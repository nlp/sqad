<p>
<s>
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
SRN	SRN	kA	SRN
<g/>
;	;	kIx,	;
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Bundesrepublik	Bundesrepublika	k1gFnPc2	Bundesrepublika
Deutschland	Deutschland	k1gInSc1	Deutschland
<g/>
,	,	kIx,	,
neoficiální	neoficiální	k2eAgFnSc1d1	neoficiální
německá	německý	k2eAgFnSc1d1	německá
zkratka	zkratka	k1gFnSc1	zkratka
BRD	Brdy	k1gInPc2	Brdy
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
rozdělený	rozdělený	k2eAgInSc4d1	rozdělený
na	na	k7c4	na
16	[number]	k4	16
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
sousedí	sousedit	k5eAaImIp3nS	sousedit
Německo	Německo	k1gNnSc1	Německo
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Lucemburskem	Lucembursko	k1gNnSc7	Lucembursko
<g/>
,	,	kIx,	,
Belgií	Belgie	k1gFnSc7	Belgie
a	a	k8xC	a
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc7	jeho
sousedem	soused	k1gMnSc7	soused
Dánsko	Dánsko	k1gNnSc4	Dánsko
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
břehy	břeh	k1gInPc1	břeh
jsou	být	k5eAaImIp3nP	být
omývány	omývat	k5eAaImNgInP	omývat
Severním	severní	k2eAgNnSc7d1	severní
a	a	k8xC	a
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
je	být	k5eAaImIp3nS	být
sousedem	soused	k1gMnSc7	soused
dvou	dva	k4xCgFnPc6	dva
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
a	a	k8xC	a
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
Německa	Německo	k1gNnSc2	Německo
je	být	k5eAaImIp3nS	být
357023	[number]	k4	357023
km2	km2	k4	km2
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
území	území	k1gNnSc1	území
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
podnebním	podnební	k2eAgNnSc6d1	podnební
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
úředních	úřední	k2eAgInPc2d1	úřední
údajů	údaj	k1gInPc2	údaj
mělo	mít	k5eAaImAgNnS	mít
Německo	Německo	k1gNnSc1	Německo
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2015	[number]	k4	2015
82,2	[number]	k4	82,2
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
stát	stát	k1gInSc1	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
populace	populace	k1gFnPc1	populace
ale	ale	k8xC	ale
již	již	k9	již
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
až	až	k9	až
2010	[number]	k4	2010
klesla	klesnout	k5eAaPmAgFnS	klesnout
kvůli	kvůli	k7c3	kvůli
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc1d1	nízká
porodnosti	porodnost	k1gFnPc1	porodnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
skoro	skoro	k6eAd1	skoro
800	[number]	k4	800
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
mála	málo	k4c2	málo
západoevropských	západoevropský	k2eAgFnPc2d1	západoevropská
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
úbytkem	úbytek	k1gInSc7	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
probíhá	probíhat	k5eAaImIp3nS	probíhat
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
domovem	domov	k1gInSc7	domov
třetí	třetí	k4xOgFnSc2	třetí
největší	veliký	k2eAgFnSc2d3	veliký
populace	populace	k1gFnSc2	populace
imigrantů	imigrant	k1gMnPc2	imigrant
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
2011	[number]	k4	2011
znamenal	znamenat	k5eAaImAgInS	znamenat
snížení	snížení	k1gNnSc4	snížení
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
úředního	úřední	k2eAgInSc2d1	úřední
odhadu	odhad	k1gInSc2	odhad
stavu	stav	k1gInSc2	stav
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
1,1	[number]	k4	1,1
miliónu	milión	k4xCgInSc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
co	co	k9	co
do	do	k7c2	do
absolutního	absolutní	k2eAgInSc2d1	absolutní
počtu	počet	k1gInSc2	počet
imigrantů	imigrant	k1gMnPc2	imigrant
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
postižena	postihnout	k5eAaPmNgFnS	postihnout
současnou	současný	k2eAgFnSc7d1	současná
evropskou	evropský	k2eAgFnSc7d1	Evropská
migrační	migrační	k2eAgFnSc7d1	migrační
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
známých	známý	k2eAgInPc2d1	známý
dokumentů	dokument	k1gInPc2	dokument
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
s	s	k7c7	s
latinským	latinský	k2eAgInSc7d1	latinský
názvem	název	k1gInSc7	název
Germania	germanium	k1gNnSc2	germanium
obydleno	obydlet	k5eAaPmNgNnS	obydlet
několika	několik	k4yIc7	několik
germánskými	germánský	k2eAgInPc7d1	germánský
kmeny	kmen	k1gInPc7	kmen
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
100	[number]	k4	100
n.	n.	k?	n.
l.	l.	k?	l.
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tvořila	tvořit	k5eAaImAgFnS	tvořit
německá	německý	k2eAgFnSc1d1	německá
území	území	k1gNnSc4	území
jádro	jádro	k1gNnSc4	jádro
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
existovala	existovat	k5eAaImAgFnS	existovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
severní	severní	k2eAgFnPc1d1	severní
oblasti	oblast	k1gFnPc1	oblast
staly	stát	k5eAaPmAgFnP	stát
centrem	centr	k1gInSc7	centr
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Německa	Německo	k1gNnSc2	Německo
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
se	se	k3xPyFc4	se
Německo	Německo	k1gNnSc1	Německo
významně	významně	k6eAd1	významně
podílelo	podílet	k5eAaImAgNnS	podílet
na	na	k7c6	na
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
rozpoutalo	rozpoutat	k5eAaPmAgNnS	rozpoutat
Německo	Německo	k1gNnSc1	Německo
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
doposud	doposud	k6eAd1	doposud
největší	veliký	k2eAgFnSc7d3	veliký
válkou	válka	k1gFnSc7	válka
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
přišlo	přijít	k5eAaPmAgNnS	přijít
Německo	Německo	k1gNnSc1	Německo
o	o	k7c6	o
území	území	k1gNnSc6	území
východně	východně	k6eAd1	východně
od	od	k7c2	od
řek	řeka	k1gFnPc2	řeka
Nisy	Nisa	k1gFnSc2	Nisa
a	a	k8xC	a
Odry	Odra	k1gFnSc2	Odra
včetně	včetně	k7c2	včetně
exklávy	exkláva	k1gFnSc2	exkláva
Východní	východní	k2eAgInSc4d1	východní
Prusko	Prusko	k1gNnSc4	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zbylém	zbylý	k2eAgNnSc6d1	zbylé
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
dva	dva	k4xCgInPc4	dva
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Německá	německý	k2eAgFnSc1d1	německá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc4	Německo
(	(	kIx(	(
<g/>
SRN	SRN	kA	SRN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
politickým	politický	k2eAgInSc7d1	politický
útvarem	útvar	k1gInSc7	útvar
byl	být	k5eAaImAgInS	být
Západní	západní	k2eAgInSc1d1	západní
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ale	ale	k9	ale
měl	mít	k5eAaImAgInS	mít
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
pevné	pevný	k2eAgFnSc2d1	pevná
vazby	vazba	k1gFnSc2	vazba
na	na	k7c6	na
SRN	SRN	kA	SRN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
znovusjednocený	znovusjednocený	k2eAgInSc1d1	znovusjednocený
Berlín	Berlín	k1gInSc1	Berlín
a	a	k8xC	a
pět	pět	k4xCc1	pět
obnovených	obnovený	k2eAgFnPc2d1	obnovená
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalé	bývalý	k2eAgFnSc2d1	bývalá
NDR	NDR	kA	NDR
připojily	připojit	k5eAaPmAgInP	připojit
ke	k	k7c3	k
Spolkové	spolkový	k2eAgFnSc3d1	spolková
republice	republika	k1gFnSc3	republika
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
státem	stát	k1gInSc7	stát
Evropského	evropský	k2eAgNnSc2d1	Evropské
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Německo	Německo	k1gNnSc1	Německo
významným	významný	k2eAgInSc7d1	významný
členem	člen	k1gInSc7	člen
jak	jak	k6eAd1	jak
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EMU	emu	k1gMnSc1	emu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Německou	německý	k2eAgFnSc7d1	německá
měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
euro	euro	k1gNnSc1	euro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
německou	německý	k2eAgFnSc4d1	německá
marku	marka	k1gFnSc4	marka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgFnSc7d1	federativní
a	a	k8xC	a
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
šestnácti	šestnáct	k4xCc7	šestnáct
spolkovými	spolkový	k2eAgFnPc7d1	spolková
zeměmi	zem	k1gFnPc7	zem
(	(	kIx(	(
<g/>
Bundesländer	Bundesländer	k1gInSc1	Bundesländer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnPc3d1	hlavní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Berlín	Berlín	k1gInSc1	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
uskupení	uskupení	k1gNnSc1	uskupení
G8	G8	k1gFnSc2	G8
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
G	G	kA	G
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
a	a	k8xC	a
signatářem	signatář	k1gMnSc7	signatář
Kjótského	Kjótský	k2eAgInSc2d1	Kjótský
protokolu	protokol	k1gInSc2	protokol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
nominální	nominální	k2eAgFnSc2d1	nominální
hodnoty	hodnota	k1gFnSc2	hodnota
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
německá	německý	k2eAgFnSc1d1	německá
ekonomika	ekonomika	k1gFnSc1	ekonomika
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
největší	veliký	k2eAgFnSc4d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	on	k3xPp3gFnPc4	on
předčila	předčít	k5eAaPmAgFnS	předčít
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc4	Německo
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
exportérem	exportér	k1gMnSc7	exportér
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
poskytovatelem	poskytovatel	k1gMnSc7	poskytovatel
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
pomoci	pomoc	k1gFnSc2	pomoc
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
výdaje	výdaj	k1gInPc4	výdaj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
jsou	být	k5eAaImIp3nP	být
šesté	šestý	k4xOgInPc1	šestý
největší	veliký	k2eAgInPc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
funguje	fungovat	k5eAaImIp3nS	fungovat
velice	velice	k6eAd1	velice
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
systém	systém	k1gInSc1	systém
sociální	sociální	k2eAgFnSc2d1	sociální
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgMnPc2d1	klíčový
aktérů	aktér	k1gMnPc2	aktér
evropské	evropský	k2eAgFnSc2d1	Evropská
i	i	k8xC	i
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
vědeckých	vědecký	k2eAgInPc6d1	vědecký
a	a	k8xC	a
technologických	technologický	k2eAgInPc6d1	technologický
oborech	obor	k1gInPc6	obor
je	být	k5eAaImIp3nS	být
Německo	Německo	k1gNnSc1	Německo
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c2	za
světového	světový	k2eAgMnSc2d1	světový
lídra	lídr	k1gMnSc2	lídr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
Německa	Německo	k1gNnSc2	Německo
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
z	z	k7c2	z
dávné	dávný	k2eAgFnSc2d1	dávná
minulosti	minulost	k1gFnSc2	minulost
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
němý	němý	k2eAgInSc1d1	němý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vžilo	vžít	k5eAaPmAgNnS	vžít
jako	jako	k9	jako
označení	označení	k1gNnSc1	označení
německého	německý	k2eAgInSc2d1	německý
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
pro	pro	k7c4	pro
nedostatečnou	dostatečný	k2eNgFnSc4d1	nedostatečná
schopnost	schopnost	k1gFnSc4	schopnost
těchto	tento	k3xDgMnPc2	tento
sousedních	sousední	k2eAgMnPc2d1	sousední
cizinců	cizinec	k1gMnPc2	cizinec
se	se	k3xPyFc4	se
dorozumět	dorozumět	k5eAaPmF	dorozumět
česky	česky	k6eAd1	česky
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
k	k	k7c3	k
dorozumívání	dorozumívání	k1gNnSc3	dorozumívání
používali	používat	k5eAaImAgMnP	používat
slova	slovo	k1gNnPc4	slovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
němčina	němčina	k1gFnSc1	němčina
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
německý	německý	k2eAgInSc4d1	německý
národ	národ	k1gInSc4	národ
výraz	výraz	k1gInSc1	výraz
Deutsche	Deutsch	k1gInSc2	Deutsch
resp.	resp.	kA	resp.
die	die	k?	die
Deutschen	Deutschen	k1gInSc1	Deutschen
<g/>
,	,	kIx,	,
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Deutschland	Deutschland	k1gInSc1	Deutschland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
běžně	běžně	k6eAd1	běžně
používal	používat	k5eAaImAgMnS	používat
politický	politický	k2eAgInSc4d1	politický
název	název	k1gInSc4	název
Německá	německý	k2eAgFnSc1d1	německá
spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
NSR	NSR	kA	NSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
název	název	k1gInSc1	název
užívaný	užívaný	k2eAgInSc1d1	užívaný
z	z	k7c2	z
československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
styku	styk	k1gInSc6	styk
-	-	kIx~	-
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
SRN	SRN	kA	SRN
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
přála	přát	k5eAaImAgFnS	přát
německá	německý	k2eAgFnSc1d1	německá
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
Bundesstaat	Bundesstaat	k2eAgInSc1d1	Bundesstaat
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
překládá	překládat	k5eAaImIp3nS	překládat
jako	jako	k9	jako
federativní	federativní	k2eAgInSc1d1	federativní
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
byl	být	k5eAaImAgInS	být
také	také	k9	také
geografický	geografický	k2eAgInSc4d1	geografický
název	název	k1gInSc4	název
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
"	"	kIx"	"
<g/>
spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
<g/>
"	"	kIx"	"
Německá	německý	k2eAgFnSc1d1	německá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
geografického	geografický	k2eAgNnSc2d1	geografické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jen	jen	k9	jen
neoficiálně	oficiálně	k6eNd1	oficiálně
<g/>
,	,	kIx,	,
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Východní	východní	k2eAgNnSc4d1	východní
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
Nemecká	Nemecký	k2eAgFnSc1d1	Nemecká
spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
NSR	NSR	kA	NSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
styku	styk	k1gInSc6	styk
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
dohody	dohoda	k1gFnPc4	dohoda
<g/>
,	,	kIx,	,
smlouvy	smlouva	k1gFnPc4	smlouva
a	a	k8xC	a
podobné	podobný	k2eAgInPc4d1	podobný
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
výslovnou	výslovný	k2eAgFnSc4d1	výslovná
žádost	žádost	k1gFnSc4	žádost
německé	německý	k2eAgFnSc2d1	německá
strany	strana	k1gFnSc2	strana
název	název	k1gInSc4	název
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Nemecko	Nemecko	k1gNnSc4	Nemecko
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SRN	srna	k1gFnPc2	srna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Prehistorie	prehistorie	k1gFnSc2	prehistorie
===	===	k?	===
</s>
</p>
<p>
<s>
Nález	nález	k1gInSc1	nález
čelisti	čelist	k1gFnSc2	čelist
Mauer	Mauer	k1gInSc1	Mauer
1	[number]	k4	1
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravěcí	pravěký	k2eAgMnPc1d1	pravěký
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
přítomni	přítomen	k2eAgMnPc1d1	přítomen
už	už	k9	už
před	před	k7c7	před
600	[number]	k4	600
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
dosud	dosud	k6eAd1	dosud
nalezené	nalezený	k2eAgFnPc1d1	nalezená
kompletní	kompletní	k2eAgFnPc1d1	kompletní
lovecké	lovecký	k2eAgFnPc1d1	lovecká
zbraně	zbraň	k1gFnPc1	zbraň
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
v	v	k7c6	v
uhelném	uhelný	k2eAgInSc6d1	uhelný
dole	dol	k1gInSc6	dol
v	v	k7c6	v
Schöningenu	Schöningen	k1gInSc6	Schöningen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
380	[number]	k4	380
000	[number]	k4	000
let	léto	k1gNnPc2	léto
staré	starý	k2eAgInPc1d1	starý
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
oštěpy	oštěp	k1gInPc1	oštěp
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7,5	[number]	k4	7,5
stopy	stopa	k1gFnSc2	stopa
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Neandertal	Neandertal	k1gFnSc2	Neandertal
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
objevena	objevit	k5eAaPmNgFnS	objevit
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
fosílie	fosílie	k1gFnSc1	fosílie
nemoderního	moderní	k2eNgMnSc2d1	nemoderní
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
člověka	člověk	k1gMnSc2	člověk
byl	být	k5eAaImAgInS	být
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
Neandertálec	neandertálec	k1gMnSc1	neandertálec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
těchto	tento	k3xDgFnPc6	tento
fosíliích	fosílie	k1gFnPc6	fosílie
nazvaných	nazvaný	k2eAgFnPc6d1	nazvaná
Neandrtal	Neandrtal	k1gFnSc4	Neandrtal
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
40	[number]	k4	40
000	[number]	k4	000
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnPc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
moderních	moderní	k2eAgInPc6d1	moderní
lidech	lid	k1gInPc6	lid
podobně	podobně	k6eAd1	podobně
starých	stará	k1gFnPc2	stará
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
v	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Švábská	švábský	k2eAgFnSc1d1	Švábská
Alba	alba	k1gFnSc1	alba
nedaleko	nedaleko	k7c2	nedaleko
Ulmu	Ulmus	k1gInSc2	Ulmus
<g/>
.	.	kIx.	.
</s>
<s>
Nalezeny	nalezen	k2eAgFnPc1d1	nalezena
byly	být	k5eAaImAgFnP	být
také	také	k9	také
42	[number]	k4	42
000	[number]	k4	000
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnSc2d1	stará
flétny	flétna	k1gFnSc2	flétna
<g/>
,	,	kIx,	,
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
z	z	k7c2	z
ptačích	ptačí	k2eAgFnPc2d1	ptačí
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
mamutí	mamutí	k2eAgFnSc2d1	mamutí
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nejstaršími	starý	k2eAgInPc7d3	nejstarší
nalezenými	nalezený	k2eAgInPc7d1	nalezený
hudebními	hudební	k2eAgInPc7d1	hudební
nástroji	nástroj	k1gInPc7	nástroj
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
40	[number]	k4	40
000	[number]	k4	000
let	léto	k1gNnPc2	léto
stará	starý	k2eAgFnSc1d1	stará
soška	soška	k1gFnSc1	soška
lvího	lví	k2eAgMnSc2d1	lví
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
a	a	k8xC	a
35	[number]	k4	35
000	[number]	k4	000
let	léto	k1gNnPc2	léto
stará	starý	k2eAgFnSc1d1	stará
Venuše	Venuše	k1gFnSc1	Venuše
z	z	k7c2	z
Hohle	Hohle	k1gNnSc2	Hohle
Fels	Felsa	k1gFnPc2	Felsa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
nejstarším	starý	k2eAgNnSc7d3	nejstarší
objeveným	objevený	k2eAgNnSc7d1	objevené
sochařským	sochařský	k2eAgNnSc7d1	sochařské
uměním	umění	k1gNnSc7	umění
<g/>
.	.	kIx.	.
</s>
<s>
Disk	disk	k1gInSc1	disk
z	z	k7c2	z
Nebry	Nebra	k1gFnSc2	Nebra
je	být	k5eAaImIp3nS	být
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
a	a	k8xC	a
nalezený	nalezený	k2eAgInSc1d1	nalezený
poblíž	poblíž	k6eAd1	poblíž
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
název	název	k1gInSc1	název
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
Nebry	Nebra	k1gFnPc1	Nebra
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
Sasko-Anhaltsko	Sasko-Anhaltsko	k1gNnSc1	Sasko-Anhaltsko
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
registr	registr	k1gInSc1	registr
programu	program	k1gInSc2	program
Paměť	paměť	k1gFnSc4	paměť
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
)	)	kIx)	)
tento	tento	k3xDgInSc4	tento
nález	nález	k1gInSc4	nález
nazval	nazvat	k5eAaPmAgMnS	nazvat
"	"	kIx"	"
<g/>
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
a	a	k8xC	a
Franská	franský	k2eAgFnSc1d1	Franská
říše	říše	k1gFnSc1	říše
===	===	k?	===
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
germánských	germánský	k2eAgMnPc2d1	germánský
kmenů	kmen	k1gInPc2	kmen
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
severské	severský	k2eAgFnSc2d1	severská
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
nebo	nebo	k8xC	nebo
předřímské	předřímský	k2eAgFnSc2d1	předřímský
doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnSc2d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
a	a	k8xC	a
severního	severní	k2eAgNnSc2d1	severní
Německa	Německo	k1gNnSc2	Německo
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
postupovali	postupovat	k5eAaImAgMnP	postupovat
Germáni	Germán	k1gMnPc1	Germán
jižním	jižní	k2eAgInSc7d1	jižní
<g/>
,	,	kIx,	,
východním	východní	k2eAgInSc7d1	východní
a	a	k8xC	a
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
keltskými	keltský	k2eAgInPc7d1	keltský
kmeny	kmen	k1gInPc7	kmen
Galů	Gal	k1gMnPc2	Gal
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
i	i	k9	i
s	s	k7c7	s
íránskými	íránský	k2eAgInPc7d1	íránský
<g/>
,	,	kIx,	,
baltskými	baltský	k2eAgInPc7d1	baltský
a	a	k8xC	a
slovanskými	slovanský	k2eAgInPc7d1	slovanský
kmeny	kmen	k1gInPc7	kmen
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
císaře	císař	k1gMnSc2	císař
Augusta	August	k1gMnSc2	August
začalo	začít	k5eAaPmAgNnS	začít
Římské	římský	k2eAgNnSc4d1	římské
impérium	impérium	k1gNnSc4	impérium
území	území	k1gNnSc2	území
Germánie	Germánie	k1gFnSc2	Germánie
napadat	napadat	k5eAaBmF	napadat
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc4	oblast
rozprostírající	rozprostírající	k2eAgFnSc4d1	rozprostírající
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
Rýna	Rýn	k1gInSc2	Rýn
po	po	k7c4	po
Ural	Ural	k1gInSc4	Ural
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
9	[number]	k4	9
n.	n.	k?	n.
l.	l.	k?	l.
byly	být	k5eAaImAgFnP	být
tři	tři	k4xCgFnPc4	tři
římské	římský	k2eAgFnPc4d1	římská
legie	legie	k1gFnPc4	legie
vedené	vedený	k2eAgFnPc4d1	vedená
Publiem	Publium	k1gNnSc7	Publium
Quinctiliem	Quinctilium	k1gNnSc7	Quinctilium
Varem	var	k1gInSc7	var
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Teutoburském	Teutoburský	k2eAgInSc6d1	Teutoburský
lese	les	k1gInSc6	les
poraženy	poražen	k2eAgFnPc1d1	poražena
cheruským	cheruský	k2eAgMnSc7d1	cheruský
vůdcem	vůdce	k1gMnSc7	vůdce
Arminiem	Arminium	k1gNnSc7	Arminium
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
100	[number]	k4	100
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Tacitus	Tacitus	k1gInSc1	Tacitus
napsal	napsat	k5eAaBmAgInS	napsat
knihu	kniha	k1gFnSc4	kniha
Germania	germanium	k1gNnSc2	germanium
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
usadily	usadit	k5eAaPmAgInP	usadit
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
Rýn	Rýn	k1gInSc1	Rýn
a	a	k8xC	a
Dunaj	Dunaj	k1gInSc1	Dunaj
(	(	kIx(	(
<g/>
Limes	Limes	k1gMnSc1	Limes
Germanicus	Germanicus	k1gMnSc1	Germanicus
<g/>
)	)	kIx)	)
a	a	k8xC	a
zabíraly	zabírat	k5eAaImAgFnP	zabírat
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
jižního	jižní	k2eAgNnSc2d1	jižní
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
a	a	k8xC	a
západního	západní	k2eAgNnSc2d1	západní
Porýní	Porýní	k1gNnSc2	Porýní
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgNnPc1	tento
území	území	k1gNnPc1	území
spadala	spadat	k5eAaPmAgNnP	spadat
pod	pod	k7c4	pod
Řím	Řím	k1gInSc4	Řím
jako	jako	k8xS	jako
provincie	provincie	k1gFnPc4	provincie
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
řada	řada	k1gFnSc1	řada
západních	západní	k2eAgInPc2d1	západní
germánských	germánský	k2eAgInPc2d1	germánský
kmenů	kmen	k1gInPc2	kmen
<g/>
:	:	kIx,	:
Alamani	Alaman	k1gMnPc1	Alaman
<g/>
,	,	kIx,	,
Frankové	Frank	k1gMnPc1	Frank
<g/>
,	,	kIx,	,
Chattové	Chatt	k1gMnPc1	Chatt
<g/>
,	,	kIx,	,
Sasové	Sas	k1gMnPc1	Sas
<g/>
,	,	kIx,	,
Frísové	Frís	k1gMnPc1	Frís
a	a	k8xC	a
Durynkové	Durynk	k1gMnPc1	Durynk
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
260	[number]	k4	260
začali	začít	k5eAaPmAgMnP	začít
Germáni	Germán	k1gMnPc1	Germán
nájezdy	nájezd	k1gInPc4	nájezd
na	na	k7c4	na
římská	římský	k2eAgNnPc4d1	římské
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
invazi	invaze	k1gFnSc6	invaze
Hunů	Hun	k1gMnPc2	Hun
v	v	k7c6	v
roce	rok	k1gInSc6	rok
375	[number]	k4	375
a	a	k8xC	a
s	s	k7c7	s
úpadkem	úpadek	k1gInSc7	úpadek
Říma	Řím	k1gInSc2	Řím
od	od	k7c2	od
roku	rok	k1gInSc2	rok
395	[number]	k4	395
se	se	k3xPyFc4	se
germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
přesunuly	přesunout	k5eAaPmAgInP	přesunout
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
několik	několik	k4yIc1	několik
velkých	velký	k2eAgInPc2d1	velký
kmenů	kmen	k1gInPc2	kmen
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
zhruba	zhruba	k6eAd1	zhruba
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
vysídlilo	vysídlit	k5eAaPmAgNnS	vysídlit
menší	malý	k2eAgNnSc1d2	menší
germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
území	území	k1gNnPc1	území
(	(	kIx(	(
<g/>
známá	známá	k1gFnSc1	známá
od	od	k7c2	od
merovejského	merovejský	k2eAgNnSc2d1	merovejský
období	období	k1gNnSc2	období
jako	jako	k8xS	jako
Austrasie	Austrasie	k1gFnSc2	Austrasie
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
Franky	Frank	k1gMnPc7	Frank
a	a	k8xC	a
severní	severní	k2eAgNnSc1d1	severní
Německo	Německo	k1gNnSc1	Německo
bylo	být	k5eAaImAgNnS	být
ovládáno	ovládat	k5eAaImNgNnS	ovládat
Sasy	Sas	k1gMnPc7	Sas
a	a	k8xC	a
Slovany	Slovan	k1gMnPc7	Slovan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
800	[number]	k4	800
byl	být	k5eAaImAgMnS	být
franský	franský	k2eAgMnSc1d1	franský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
korunován	korunován	k2eAgMnSc1d1	korunován
na	na	k7c4	na
císaře	císař	k1gMnSc4	císař
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
Karolínskou	karolínský	k2eAgFnSc4d1	karolínská
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
843	[number]	k4	843
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
Karlova	Karlův	k2eAgNnSc2d1	Karlovo
původního	původní	k2eAgNnSc2d1	původní
království	království	k1gNnSc2	království
a	a	k8xC	a
ukázala	ukázat	k5eAaPmAgFnS	ukázat
se	se	k3xPyFc4	se
jako	jako	k9	jako
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
území	území	k1gNnSc4	území
se	se	k3xPyFc4	se
táhla	táhnout	k5eAaImAgFnS	táhnout
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Eider	Eidra	k1gFnPc2	Eidra
na	na	k7c6	na
severu	sever	k1gInSc6	sever
ke	k	k7c3	k
Středozemnímu	středozemní	k2eAgNnSc3d1	středozemní
moři	moře	k1gNnSc3	moře
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
ottonských	ottonský	k2eAgMnPc2d1	ottonský
císařů	císař	k1gMnPc2	císař
(	(	kIx(	(
<g/>
919	[number]	k4	919
<g/>
-	-	kIx~	-
<g/>
1024	[number]	k4	1024
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
sjednoceno	sjednotit	k5eAaPmNgNnS	sjednotit
několik	několik	k4yIc1	několik
významných	významný	k2eAgNnPc2d1	významné
vévodství	vévodství	k1gNnPc2	vévodství
a	a	k8xC	a
německý	německý	k2eAgMnSc1d1	německý
král	král	k1gMnSc1	král
Ota	Ota	k1gMnSc1	Ota
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
962	[number]	k4	962
korunován	korunovat	k5eAaBmNgMnS	korunovat
na	na	k7c4	na
císaře	císař	k1gMnSc4	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
996	[number]	k4	996
se	se	k3xPyFc4	se
Řehoř	Řehoř	k1gMnSc1	Řehoř
V.	V.	kA	V.
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
německým	německý	k2eAgMnSc7d1	německý
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
ho	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnSc1	jeho
bratranec	bratranec	k1gMnSc1	bratranec
Ota	Ota	k1gMnSc1	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
korunován	korunovat	k5eAaBmNgMnS	korunovat
císařem	císař	k1gMnSc7	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
sálské	sálský	k2eAgFnSc2d1	sálská
dynastie	dynastie	k1gFnSc2	dynastie
(	(	kIx(	(
<g/>
1024	[number]	k4	1024
<g/>
-	-	kIx~	-
<g/>
1125	[number]	k4	1125
<g/>
)	)	kIx)	)
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
pohltila	pohltit	k5eAaPmAgFnS	pohltit
severní	severní	k2eAgFnSc4d1	severní
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
Burgundsko	Burgundsko	k1gNnSc4	Burgundsko
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
císaři	císař	k1gMnPc1	císař
ztratili	ztratit	k5eAaPmAgMnP	ztratit
kvůli	kvůli	k7c3	kvůli
boji	boj	k1gInSc3	boj
o	o	k7c4	o
investituru	investitura	k1gFnSc4	investitura
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Štaufů	Štauf	k1gInPc2	Štauf
(	(	kIx(	(
<g/>
1138	[number]	k4	1138
<g/>
-	-	kIx~	-
<g/>
1254	[number]	k4	1254
<g/>
)	)	kIx)	)
posílila	posílit	k5eAaPmAgFnS	posílit
německá	německý	k2eAgFnSc1d1	německá
knížata	kníže	k1gMnPc1wR	kníže
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
na	na	k7c4	na
východ	východ	k1gInSc4	východ
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
obývaných	obývaný	k2eAgInPc2d1	obývaný
Slovany	Slovan	k1gInPc7	Slovan
<g/>
,	,	kIx,	,
předcházejíc	předcházet	k5eAaImSgFnS	předcházet
německému	německý	k2eAgNnSc3d1	německé
osídlení	osídlení	k1gNnSc3	osídlení
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
(	(	kIx(	(
<g/>
Ostsiedlung	Ostsiedlung	k1gInSc4	Ostsiedlung
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
německá	německý	k2eAgFnSc1d1	německá
města	město	k1gNnSc2	město
rostla	růst	k5eAaImAgFnS	růst
a	a	k8xC	a
prosperovala	prosperovat	k5eAaImAgFnS	prosperovat
jako	jako	k9	jako
členové	člen	k1gMnPc1	člen
Hanzy	hanza	k1gFnSc2	hanza
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
velkým	velký	k2eAgInSc7d1	velký
hladomorem	hladomor	k1gInSc7	hladomor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1315	[number]	k4	1315
a	a	k8xC	a
konče	konče	k7c7	konče
černou	černý	k2eAgFnSc7d1	černá
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1348-1350	[number]	k4	1348-1350
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Německa	Německo	k1gNnSc2	Německo
snížil	snížit	k5eAaPmAgInS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1356	[number]	k4	1356
zajistila	zajistit	k5eAaPmAgFnS	zajistit
základní	základní	k2eAgFnSc4d1	základní
ústavu	ústava	k1gFnSc4	ústava
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
kodifikovala	kodifikovat	k5eAaBmAgFnS	kodifikovat
volbu	volba	k1gFnSc4	volba
císaře	císař	k1gMnSc2	císař
sedmi	sedm	k4xCc2	sedm
kurfiřty	kurfiřt	k1gMnPc4	kurfiřt
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vládli	vládnout	k5eAaImAgMnP	vládnout
některému	některý	k3yIgInSc3	některý
z	z	k7c2	z
nejmocnějších	mocný	k2eAgNnPc2d3	nejmocnější
knížectví	knížectví	k1gNnPc2	knížectví
a	a	k8xC	a
arcibiskupství	arcibiskupství	k1gNnPc2	arcibiskupství
<g/>
.	.	kIx.	.
<g/>
Martin	Martin	k1gInSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1517	[number]	k4	1517
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
napadal	napadat	k5eAaPmAgInS	napadat
římskokatolickou	římskokatolický	k2eAgFnSc4d1	Římskokatolická
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgInS	zahájit
protestantskou	protestantský	k2eAgFnSc4d1	protestantská
reformaci	reformace	k1gFnSc4	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
luteránská	luteránský	k2eAgFnSc1d1	luteránská
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1530	[number]	k4	1530
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
spolkových	spolkový	k2eAgFnPc6d1	spolková
zemích	zem	k1gFnPc6	zem
stala	stát	k5eAaPmAgFnS	stát
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Náboženský	náboženský	k2eAgInSc1d1	náboženský
konflikt	konflikt	k1gInSc1	konflikt
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
třicetileté	třicetiletý	k2eAgFnSc3d1	třicetiletá
válce	válka	k1gFnSc3	válka
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
-	-	kIx~	-
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
německé	německý	k2eAgFnPc4d1	německá
země	zem	k1gFnPc4	zem
zpustošila	zpustošit	k5eAaPmAgFnS	zpustošit
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
německých	německý	k2eAgFnPc2d1	německá
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
asi	asi	k9	asi
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vestfálský	vestfálský	k2eAgInSc1d1	vestfálský
mír	mír	k1gInSc1	mír
(	(	kIx(	(
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
německými	německý	k2eAgFnPc7d1	německá
zeměmi	zem	k1gFnPc7	zem
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
válku	válka	k1gFnSc4	válka
ukončil	ukončit	k5eAaPmAgMnS	ukončit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
de	de	k?	de
facto	facto	k1gNnSc1	facto
rozdělena	rozdělen	k2eAgFnSc1d1	rozdělena
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
nezávislých	závislý	k2eNgNnPc2d1	nezávislé
knížectví	knížectví	k1gNnPc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
800	[number]	k4	800
takových	takový	k3xDgNnPc2	takový
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
<g/>
Pruské	pruský	k2eAgNnSc1d1	pruské
království	království	k1gNnSc1	království
byl	být	k5eAaImAgInS	být
největší	veliký	k2eAgInSc1d3	veliký
německý	německý	k2eAgInSc1d1	německý
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
existoval	existovat	k5eAaImAgInS	existovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1701	[number]	k4	1701
<g/>
.	.	kIx.	.
</s>
<s>
Pruské	pruský	k2eAgNnSc1d1	pruské
království	království	k1gNnSc1	království
čím	čí	k3xOyQgMnPc3	čí
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
sílilo	sílit	k5eAaImAgNnS	sílit
a	a	k8xC	a
tak	tak	k6eAd1	tak
ve	v	k7c6	v
slezských	slezský	k2eAgFnPc6d1	Slezská
válkách	válka	k1gFnPc6	válka
v	v	k7c6	v
letech	let	k1gInPc6	let
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1745	[number]	k4	1745
dobylo	dobýt	k5eAaPmAgNnS	dobýt
od	od	k7c2	od
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
část	část	k1gFnSc1	část
Země	zem	k1gFnSc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
-	-	kIx~	-
většinu	většina	k1gFnSc4	většina
Slezska	Slezsko	k1gNnSc2	Slezsko
i	i	k9	i
s	s	k7c7	s
Kladskem	Kladsko	k1gNnSc7	Kladsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
byla	být	k5eAaImAgFnS	být
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
dobyta	dobyt	k2eAgFnSc1d1	dobyta
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Německý	německý	k2eAgInSc1d1	německý
spolek	spolek	k1gInSc1	spolek
a	a	k8xC	a
císařství	císařství	k1gNnSc1	císařství
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Napoleona	Napoleon	k1gMnSc2	Napoleon
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
svolán	svolán	k2eAgInSc1d1	svolán
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
kongres	kongres	k1gInSc1	kongres
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
německý	německý	k2eAgInSc1d1	německý
spolek	spolek	k1gInSc1	spolek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Deutscher	Deutschra	k1gFnPc2	Deutschra
Bund	bund	k1gInSc1	bund
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
volná	volný	k2eAgFnSc1d1	volná
liga	liga	k1gFnSc1	liga
39	[number]	k4	39
svrchovaných	svrchovaný	k2eAgInPc2d1	svrchovaný
německých	německý	k2eAgInPc2d1	německý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlas	nesouhlas	k1gInSc1	nesouhlas
s	s	k7c7	s
restaurátorskou	restaurátorský	k2eAgFnSc7d1	restaurátorská
politikou	politika	k1gFnSc7	politika
částečně	částečně	k6eAd1	částečně
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
liberálních	liberální	k2eAgFnPc2d1	liberální
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
následovaly	následovat	k5eAaImAgFnP	následovat
nové	nový	k2eAgFnPc4d1	nová
represe	represe	k1gFnPc4	represe
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rakouského	rakouský	k2eAgMnSc2d1	rakouský
státníka	státník	k1gMnSc2	státník
knížete	kníže	k1gMnSc2	kníže
Metternicha	Metternich	k1gMnSc2	Metternich
<g/>
.	.	kIx.	.
</s>
<s>
Celní	celní	k2eAgFnSc1d1	celní
unie	unie	k1gFnSc1	unie
Zollverein	Zollvereina	k1gFnPc2	Zollvereina
podporovala	podporovat	k5eAaImAgFnS	podporovat
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
jednotu	jednota	k1gFnSc4	jednota
v	v	k7c6	v
německých	německý	k2eAgInPc6d1	německý
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
<g/>
Národní	národní	k2eAgInSc1d1	národní
a	a	k8xC	a
liberální	liberální	k2eAgInSc1d1	liberální
ideály	ideál	k1gInPc7	ideál
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
získaly	získat	k5eAaPmAgFnP	získat
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
podporu	podpora	k1gFnSc4	podpora
mezi	mezi	k7c7	mezi
mnoha	mnoho	k4c3	mnoho
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
mladými	mladý	k2eAgMnPc7d1	mladý
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
Hambach	Hambacha	k1gFnPc2	Hambacha
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1832	[number]	k4	1832
hlavní	hlavní	k2eAgFnSc7d1	hlavní
událostí	událost	k1gFnSc7	událost
v	v	k7c6	v
podpoře	podpora	k1gFnSc6	podpora
sjednocení	sjednocení	k1gNnSc2	sjednocení
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
revolucí	revoluce	k1gFnPc2	revoluce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pomohly	pomoct	k5eAaPmAgFnP	pomoct
založit	založit	k5eAaPmF	založit
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
intelektuálové	intelektuál	k1gMnPc1	intelektuál
i	i	k8xC	i
prostí	prostý	k2eAgMnPc1d1	prostý
občané	občan	k1gMnPc1	občan
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Králi	Král	k1gMnSc3	Král
Fridrichu	Fridrich	k1gMnSc3	Fridrich
Vilémovi	Vilém	k1gMnSc3	Vilém
IV	IV	kA	IV
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
nabídnut	nabídnout	k5eAaPmNgInS	nabídnout
titul	titul	k1gInSc4	titul
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
moci	moct	k5eAaImF	moct
<g/>
;	;	kIx,	;
korunu	koruna	k1gFnSc4	koruna
a	a	k8xC	a
navrhované	navrhovaný	k2eAgFnPc4d1	navrhovaná
stanovy	stanova	k1gFnPc4	stanova
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pro	pro	k7c4	pro
hnutí	hnutí	k1gNnSc4	hnutí
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dočasnému	dočasný	k2eAgInSc3d1	dočasný
zádrhelu	zádrhel	k1gInSc3	zádrhel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
Vilém	Vilém	k1gMnSc1	Vilém
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Otto	Otto	k1gMnSc1	Otto
von	von	k1gInSc4	von
Bismarcka	Bismarcka	k1gFnSc1	Bismarcka
novým	nový	k2eAgMnSc7d1	nový
pruským	pruský	k2eAgMnSc7d1	pruský
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
úspěšně	úspěšně	k6eAd1	úspěšně
vedl	vést	k5eAaImAgMnS	vést
druhou	druhý	k4xOgFnSc4	druhý
šlesvickou	šlesvický	k2eAgFnSc4d1	šlesvický
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
a	a	k8xC	a
pruské	pruský	k2eAgNnSc4d1	pruské
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
prusko-rakouské	pruskoakouský	k2eAgFnSc6d1	prusko-rakouská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vytvořit	vytvořit	k5eAaPmF	vytvořit
severoněmecký	severoněmecký	k2eAgInSc4d1	severoněmecký
spolek	spolek	k1gInSc4	spolek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Norddeutscher	Norddeutschra	k1gFnPc2	Norddeutschra
Bund	bund	k1gInSc1	bund
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
záležitosti	záležitost	k1gFnSc2	záležitost
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
francouzské	francouzský	k2eAgFnSc6d1	francouzská
prohře	prohra	k1gFnSc6	prohra
v	v	k7c6	v
Prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
vyhlášeno	vyhlášen	k2eAgNnSc1d1	vyhlášeno
německé	německý	k2eAgNnSc1d1	německé
císařství	císařství	k1gNnSc1	císařství
<g/>
,	,	kIx,	,
sdružující	sdružující	k2eAgFnSc1d1	sdružující
všechny	všechen	k3xTgFnPc4	všechen
rozptýlené	rozptýlený	k2eAgFnPc4d1	rozptýlená
části	část	k1gFnPc4	část
Německa	Německo	k1gNnSc2	Německo
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Prusko	Prusko	k1gNnSc1	Prusko
bylo	být	k5eAaImAgNnS	být
dominující	dominující	k2eAgFnSc7d1	dominující
složkou	složka	k1gFnSc7	složka
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
pruský	pruský	k2eAgMnSc1d1	pruský
král	král	k1gMnSc1	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Hohenzollernů	Hohenzollern	k1gMnPc2	Hohenzollern
vládl	vládnout	k5eAaImAgInS	vládnout
současně	současně	k6eAd1	současně
jako	jako	k9	jako
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
hlavní	hlavní	k2eAgInSc1d1	hlavní
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Berlín	Berlín	k1gInSc4	Berlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
Gründerzeit	Gründerzeita	k1gFnPc2	Gründerzeita
po	po	k7c6	po
sjednocení	sjednocení	k1gNnSc6	sjednocení
Německa	Německo	k1gNnSc2	Německo
zaručila	zaručit	k5eAaPmAgFnS	zaručit
Bismarckova	Bismarckův	k2eAgFnSc1d1	Bismarckova
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
(	(	kIx(	(
<g/>
Bismarck	Bismarck	k1gMnSc1	Bismarck
se	se	k3xPyFc4	se
za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Viléma	Vilém	k1gMnSc2	Vilém
stal	stát	k5eAaPmAgMnS	stát
německým	německý	k2eAgMnSc7d1	německý
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
)	)	kIx)	)
Německu	Německo	k1gNnSc3	Německo
postavení	postavení	k1gNnSc2	postavení
jako	jako	k8xS	jako
velikému	veliký	k2eAgInSc3d1	veliký
národu	národ	k1gInSc3	národ
<g/>
,	,	kIx,	,
diplomatickou	diplomatický	k2eAgFnSc7d1	diplomatická
cestou	cesta	k1gFnSc7	cesta
izolovala	izolovat	k5eAaBmAgFnS	izolovat
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
vyhýbala	vyhýbat	k5eAaImAgFnS	vyhýbat
se	se	k3xPyFc4	se
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
Německo	Německo	k1gNnSc1	Německo
nárokovalo	nárokovat	k5eAaImAgNnS	nárokovat
několik	několik	k4yIc4	několik
kolonií	kolonie	k1gFnPc2	kolonie
včetně	včetně	k7c2	včetně
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Toga	Togo	k1gNnSc2	Togo
a	a	k8xC	a
Kamerunu	Kamerun	k1gInSc2	Kamerun
<g/>
.	.	kIx.	.
<g/>
Pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
Viléma	Vilém	k1gMnSc2	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
evropské	evropský	k2eAgFnPc1d1	Evropská
mocnosti	mocnost	k1gFnPc1	mocnost
<g/>
,	,	kIx,	,
ubralo	ubrat	k5eAaPmAgNnS	ubrat
novoimperialistickým	novoimperialistický	k2eAgInSc7d1	novoimperialistický
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
třenicím	třenice	k1gFnPc3	třenice
se	s	k7c7	s
sousedními	sousední	k2eAgFnPc7d1	sousední
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
aliancí	aliance	k1gFnSc7	aliance
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
dříve	dříve	k6eAd2	dříve
členem	člen	k1gMnSc7	člen
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
a	a	k8xC	a
nové	nový	k2eAgFnPc1d1	nová
aliance	aliance	k1gFnPc1	aliance
ho	on	k3xPp3gNnSc2	on
nikam	nikam	k6eAd1	nikam
"	"	kIx"	"
<g/>
nepustily	pustit	k5eNaPmAgInP	pustit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Františka	František	k1gMnSc4	František
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
spustil	spustit	k5eAaPmAgInS	spustit
první	první	k4xOgFnSc4	první
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
ústředních	ústřední	k2eAgFnPc2d1	ústřední
mocností	mocnost	k1gFnPc2	mocnost
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
od	od	k7c2	od
států	stát	k1gInPc2	stát
Dohody	dohoda	k1gFnSc2	dohoda
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc1	dva
miliony	milion	k4xCgInPc1	milion
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
padlo	padnout	k5eAaPmAgNnS	padnout
<g/>
.	.	kIx.	.
<g/>
Německá	německý	k2eAgFnSc1d1	německá
revoluce	revoluce	k1gFnSc1	revoluce
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1918	[number]	k4	1918
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
se	s	k7c7	s
všemi	všecek	k3xTgNnPc7	všecek
vládnoucími	vládnoucí	k2eAgNnPc7d1	vládnoucí
knížaty	kníže	k1gNnPc7	kníže
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
<g/>
.	.	kIx.	.
</s>
<s>
Příměří	příměří	k1gNnSc1	příměří
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc3	listopad
válku	válka	k1gFnSc4	válka
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1919	[number]	k4	1919
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
versailleskou	versailleský	k2eAgFnSc4d1	Versailleská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
,	,	kIx,	,
a	a	k8xC	a
muselo	muset	k5eAaImAgNnS	muset
vrátit	vrátit	k5eAaPmF	vrátit
Francii	Francie	k1gFnSc4	Francie
Alsasko	Alsasko	k1gNnSc4	Alsasko
a	a	k8xC	a
Lotrinsko	Lotrinsko	k1gNnSc4	Lotrinsko
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
smlouvu	smlouva	k1gFnSc4	smlouva
vnímali	vnímat	k5eAaImAgMnP	vnímat
jako	jako	k9	jako
ponižující	ponižující	k2eAgFnSc4d1	ponižující
a	a	k8xC	a
nespravedlivou	spravedlivý	k2eNgFnSc4d1	nespravedlivá
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
měla	mít	k5eAaImAgFnS	mít
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
historiků	historik	k1gMnPc2	historik
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Hitlerův	Hitlerův	k2eAgInSc4d1	Hitlerův
vzestup	vzestup	k1gInSc4	vzestup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výmarská	výmarský	k2eAgFnSc1d1	Výmarská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
===	===	k?	===
</s>
</p>
<p>
<s>
Listopadová	listopadový	k2eAgFnSc1d1	listopadová
revoluce	revoluce	k1gFnSc1	revoluce
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
učinila	učinit	k5eAaImAgFnS	učinit
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
parlamentní	parlamentní	k2eAgFnSc6d1	parlamentní
demokracii	demokracie	k1gFnSc6	demokracie
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
boj	boj	k1gInSc1	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
mezi	mezi	k7c7	mezi
levicovými	levicový	k2eAgFnPc7d1	levicová
až	až	k8xS	až
krajně	krajně	k6eAd1	krajně
levicovými	levicový	k2eAgFnPc7d1	levicová
silami	síla	k1gFnPc7	síla
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
středovými	středový	k2eAgFnPc7d1	středová
a	a	k8xC	a
pravicovými	pravicový	k2eAgFnPc7d1	pravicová
silami	síla	k1gFnPc7	síla
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
komunisté	komunista	k1gMnPc1	komunista
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
ruských	ruský	k2eAgMnPc2d1	ruský
bolševiků	bolševik	k1gMnPc2	bolševik
krátkodobou	krátkodobý	k2eAgFnSc4d1	krátkodobá
Bavorskou	bavorský	k2eAgFnSc4d1	bavorská
republiku	republika	k1gFnSc4	republika
rad	rada	k1gFnPc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgInPc1d1	revoluční
pokusy	pokus	k1gInPc1	pokus
skončily	skončit	k5eAaPmAgInP	skončit
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
říšský	říšský	k2eAgMnSc1d1	říšský
prezident	prezident	k1gMnSc1	prezident
Friedrich	Friedrich	k1gMnSc1	Friedrich
Ebert	Ebert	k1gInSc4	Ebert
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
významný	významný	k2eAgMnSc1d1	významný
člen	člen	k1gMnSc1	člen
Sociálnědemokratické	sociálnědemokratický	k2eAgFnSc2d1	sociálnědemokratická
strany	strana	k1gFnSc2	strana
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
SPD	SPD	kA	SPD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podepsal	podepsat	k5eAaPmAgInS	podepsat
Výmarskou	výmarský	k2eAgFnSc4d1	Výmarská
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
zůstal	zůstat	k5eAaPmAgInS	zůstat
Německá	německý	k2eAgFnSc1d1	německá
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
novou	nový	k2eAgFnSc7d1	nová
ústavou	ústava	k1gFnSc7	ústava
však	však	k9	však
byl	být	k5eAaImAgMnS	být
konstituován	konstituován	k2eAgInSc4d1	konstituován
demokratický	demokratický	k2eAgInSc4d1	demokratický
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
historiky	historik	k1gMnPc4	historik
nazýván	nazýván	k2eAgInSc1d1	nazýván
Výmarská	výmarský	k2eAgFnSc1d1	Výmarská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
<g/>
Německo	Německo	k1gNnSc1	Německo
ovšem	ovšem	k9	ovšem
zažilo	zažít	k5eAaPmAgNnS	zažít
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
bouřlivé	bouřlivý	k2eAgNnSc1d1	bouřlivé
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
probíhala	probíhat	k5eAaImAgFnS	probíhat
okupace	okupace	k1gFnSc1	okupace
Porúří	Porúří	k1gNnSc2	Porúří
francouzskými	francouzský	k2eAgNnPc7d1	francouzské
a	a	k8xC	a
belgickými	belgický	k2eAgNnPc7d1	Belgické
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tíživých	tíživý	k2eAgFnPc2d1	tíživá
reparací	reparace	k1gFnPc2	reparace
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
vítězných	vítězný	k2eAgFnPc2d1	vítězná
mocností	mocnost	k1gFnPc2	mocnost
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obrovském	obrovský	k2eAgNnSc6d1	obrovské
vzestupu	vzestup	k1gInSc3	vzestup
inflace	inflace	k1gFnSc2	inflace
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyústil	vyústit	k5eAaPmAgInS	vyústit
až	až	k9	až
v	v	k7c4	v
katastrofální	katastrofální	k2eAgFnSc4d1	katastrofální
hyperinflaci	hyperinflace	k1gFnSc4	hyperinflace
let	léto	k1gNnPc2	léto
1922	[number]	k4	1922
a	a	k8xC	a
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
plán	plán	k1gInSc4	plán
na	na	k7c4	na
restrukturalizaci	restrukturalizace	k1gFnSc4	restrukturalizace
reparací	reparace	k1gFnPc2	reparace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Dawesův	Dawesův	k2eAgInSc1d1	Dawesův
plán	plán	k1gInSc1	plán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
nové	nový	k2eAgFnSc2d1	nová
měny	měna	k1gFnSc2	měna
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Rentenmark	Rentenmark	k1gInSc1	Rentenmark
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
začala	začít	k5eAaPmAgFnS	začít
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
dvacátá	dvacátý	k4xOgNnPc4	dvacátý
léta	léto	k1gNnPc4	léto
<g/>
"	"	kIx"	"
s	s	k7c7	s
postupnou	postupný	k2eAgFnSc7d1	postupná
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
prosperitou	prosperita	k1gFnSc7	prosperita
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastalo	nastat	k5eAaPmAgNnS	nastat
obnovování	obnovování	k1gNnSc1	obnovování
německého	německý	k2eAgNnSc2d1	německé
národního	národní	k2eAgNnSc2d1	národní
sebevědomí	sebevědomí	k1gNnSc2	sebevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Rozkvetl	rozkvetnout	k5eAaPmAgInS	rozkvetnout
liberální	liberální	k2eAgInSc1d1	liberální
kulturní	kulturní	k2eAgInSc1d1	kulturní
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
Mnichově	Mnichov	k1gInSc6	Mnichov
a	a	k8xC	a
jiných	jiný	k2eAgNnPc6d1	jiné
velkoměstech	velkoměsto	k1gNnPc6	velkoměsto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
soustředily	soustředit	k5eAaPmAgFnP	soustředit
tvůrčí	tvůrčí	k2eAgFnPc1d1	tvůrčí
síly	síla	k1gFnPc1	síla
směřující	směřující	k2eAgFnPc1d1	směřující
k	k	k7c3	k
modernímu	moderní	k2eAgInSc3d1	moderní
uměleckému	umělecký	k2eAgInSc3d1	umělecký
výrazu	výraz	k1gInSc3	výraz
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
škola	škola	k1gFnSc1	škola
architektury	architektura	k1gFnSc2	architektura
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Bauhaus	Bauhaus	k1gMnSc1	Bauhaus
v	v	k7c6	v
saském	saský	k2eAgNnSc6d1	Saské
městě	město	k1gNnSc6	město
Dessau	Dessaus	k1gInSc2	Dessaus
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Německo	Německo	k1gNnSc1	Německo
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
po	po	k7c6	po
politické	politický	k2eAgFnSc6d1	politická
stránce	stránka	k1gFnSc6	stránka
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
David	David	k1gMnSc1	David
Williamson	Williamson	k1gMnSc1	Williamson
popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	on	k3xPp3gInSc4	on
stav	stav	k1gInSc4	stav
v	v	k7c4	v
období	období	k1gNnSc4	období
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1924	[number]	k4	1924
a	a	k8xC	a
1929	[number]	k4	1929
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
jen	jen	k6eAd1	jen
částečnou	částečný	k2eAgFnSc4d1	částečná
stabilizaci	stabilizace	k1gFnSc4	stabilizace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
Německo	Německo	k1gNnSc4	Německo
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstoupení	odstoupení	k1gNnSc6	odstoupení
vlády	vláda	k1gFnSc2	vláda
kancléře	kancléř	k1gMnSc2	kancléř
Hermanna	Hermann	k1gMnSc2	Hermann
Müllera	Müller	k1gMnSc2	Müller
a	a	k8xC	a
rozpadu	rozpad	k1gInSc2	rozpad
velké	velký	k2eAgFnSc2d1	velká
koalice	koalice	k1gFnSc2	koalice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepodařilo	podařit	k5eNaPmAgNnS	podařit
sestavit	sestavit	k5eAaPmF	sestavit
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
získala	získat	k5eAaPmAgFnS	získat
většinovou	většinový	k2eAgFnSc4d1	většinová
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
Říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
vlády	vláda	k1gFnPc1	vláda
Heinricha	Heinrich	k1gMnSc2	Heinrich
Brüninga	Brüning	k1gMnSc2	Brüning
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Franze	Franze	k1gFnSc1	Franze
von	von	k1gInSc1	von
Papena	Papena	k1gFnSc1	Papena
<g/>
,	,	kIx,	,
Kurta	kurta	k1gFnSc1	kurta
von	von	k1gInSc1	von
Schleichera	Schleichera	k1gFnSc1	Schleichera
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
byly	být	k5eAaImAgFnP	být
jmenovány	jmenován	k2eAgFnPc1d1	jmenována
a	a	k8xC	a
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
uvedeny	uvést	k5eAaPmNgInP	uvést
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
parlament	parlament	k1gInSc1	parlament
říšským	říšský	k2eAgMnSc7d1	říšský
prezidentem	prezident	k1gMnSc7	prezident
Paulem	Paul	k1gMnSc7	Paul
von	von	k1gInSc1	von
Hindenburgem	Hindenburg	k1gInSc7	Hindenburg
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nouzových	nouzový	k2eAgNnPc2d1	nouzové
ustanovení	ustanovení	k1gNnPc2	ustanovení
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
48	[number]	k4	48
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Brüningova	Brüningův	k2eAgFnSc1d1	Brüningův
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
fiskálních	fiskální	k2eAgFnPc2d1	fiskální
úspor	úspora	k1gFnPc2	úspora
a	a	k8xC	a
ponechala	ponechat	k5eAaPmAgFnS	ponechat
prostor	prostor	k1gInSc4	prostor
deflaci	deflace	k1gFnSc3	deflace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
vysokou	vysoký	k2eAgFnSc4d1	vysoká
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
a	a	k8xC	a
zanechala	zanechat	k5eAaPmAgFnS	zanechat
mnohé	mnohé	k1gNnSc4	mnohé
Němce	Němec	k1gMnSc2	Němec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
nezaměstnané	nezaměstnaný	k1gMnPc4	nezaměstnaný
<g/>
,	,	kIx,	,
na	na	k7c6	na
pouhém	pouhý	k2eAgNnSc6d1	pouhé
minimu	minimum	k1gNnSc6	minimum
mezd	mzda	k1gFnPc2	mzda
či	či	k8xC	či
sociálních	sociální	k2eAgFnPc2d1	sociální
dávek	dávka	k1gFnPc2	dávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
%	%	kIx~	%
práceschopných	práceschopný	k2eAgMnPc2d1	práceschopný
Němců	Němec	k1gMnPc2	Němec
bylo	být	k5eAaImAgNnS	být
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
získala	získat	k5eAaPmAgFnS	získat
NSDAP	NSDAP	kA	NSDAP
37	[number]	k4	37
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohla	moct	k5eNaImAgFnS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
koaliční	koaliční	k2eAgFnSc4d1	koaliční
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sérii	série	k1gFnSc6	série
neúspěšných	úspěšný	k2eNgInPc2d1	neúspěšný
kabinetů	kabinet	k1gInPc2	kabinet
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
ke	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1933	[number]	k4	1933
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
Rakušana	Rakušan	k1gMnSc2	Rakušan
<g/>
,	,	kIx,	,
novým	nový	k2eAgMnSc7d1	nový
kancléřem	kancléř	k1gMnSc7	kancléř
Německa	Německo	k1gNnSc2	Německo
<g/>
.27	.27	k4	.27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1933	[number]	k4	1933
zachvátil	zachvátit	k5eAaPmAgMnS	zachvátit
budovu	budova	k1gFnSc4	budova
Reichstagu	Reichstag	k1gInSc2	Reichstag
požár	požár	k1gInSc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
dekret	dekret	k1gInSc1	dekret
o	o	k7c6	o
požáru	požár	k1gInSc6	požár
Říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
zrušila	zrušit	k5eAaPmAgNnP	zrušit
základní	základní	k2eAgNnPc1d1	základní
občanská	občanský	k2eAgNnPc1d1	občanské
práva	právo	k1gNnPc1	právo
<g/>
,	,	kIx,	,
a	a	k8xC	a
zmocňovací	zmocňovací	k2eAgInSc1d1	zmocňovací
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dal	dát	k5eAaPmAgMnS	dát
Hitlerovi	Hitler	k1gMnSc3	Hitler
neomezenou	omezený	k2eNgFnSc4d1	neomezená
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
nazývat	nazývat	k5eAaImF	nazývat
"	"	kIx"	"
<g/>
vůdcem	vůdce	k1gMnSc7	vůdce
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Führer	Führer	k1gInSc1	Führer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
pomocníci	pomocník	k1gMnPc1	pomocník
<g/>
,	,	kIx,	,
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
a	a	k8xC	a
Hermann	Hermann	k1gMnSc1	Hermann
Göring	Göring	k1gInSc4	Göring
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
přeměnili	přeměnit	k5eAaPmAgMnP	přeměnit
Německo	Německo	k1gNnSc4	Německo
v	v	k7c4	v
centralizovaný	centralizovaný	k2eAgInSc4d1	centralizovaný
totalitní	totalitní	k2eAgInSc4d1	totalitní
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Začali	začít	k5eAaPmAgMnP	začít
systematicky	systematicky	k6eAd1	systematicky
potlačovat	potlačovat	k5eAaImF	potlačovat
politickou	politický	k2eAgFnSc4d1	politická
opozici	opozice	k1gFnSc4	opozice
a	a	k8xC	a
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
židovské	židovský	k2eAgMnPc4d1	židovský
občany	občan	k1gMnPc4	občan
a	a	k8xC	a
národnostní	národnostní	k2eAgFnPc4d1	národnostní
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
menšiny	menšina	k1gFnPc4	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1933	[number]	k4	1933
byly	být	k5eAaImAgInP	být
zřízeny	zřízen	k2eAgInPc1d1	zřízen
první	první	k4xOgInPc1	první
německé	německý	k2eAgInPc1d1	německý
koncentrační	koncentrační	k2eAgInPc1d1	koncentrační
tábory	tábor	k1gInPc1	tábor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Německo	Německo	k1gNnSc1	Německo
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
ze	z	k7c2	z
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaImF	věnovat
znovuvyzbrojování	znovuvyzbrojování	k1gNnSc4	znovuvyzbrojování
země	zem	k1gFnSc2	zem
a	a	k8xC	a
budování	budování	k1gNnSc2	budování
početně	početně	k6eAd1	početně
silného	silný	k2eAgNnSc2d1	silné
vojska	vojsko	k1gNnSc2	vojsko
(	(	kIx(	(
<g/>
Wehrmacht	wehrmacht	k1gFnSc1	wehrmacht
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
organizace	organizace	k1gFnSc1	organizace
Sicherheitsstandarte	Sicherheitsstandart	k1gInSc5	Sicherheitsstandart
(	(	kIx(	(
<g/>
SS	SS	kA	SS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
nacistický	nacistický	k2eAgInSc1d1	nacistický
režim	režim	k1gInSc1	režim
znovu	znovu	k6eAd1	znovu
zavedl	zavést	k5eAaPmAgInS	zavést
povinnou	povinný	k2eAgFnSc4d1	povinná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
od	od	k7c2	od
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
rasistické	rasistický	k2eAgInPc4d1	rasistický
Norimberské	norimberský	k2eAgInPc4d1	norimberský
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
především	především	k6eAd1	především
proti	proti	k7c3	proti
Židům	Žid	k1gMnPc3	Žid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
dalším	další	k2eAgFnPc3d1	další
skupinám	skupina	k1gFnPc3	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
opět	opět	k6eAd1	opět
získalo	získat	k5eAaPmAgNnS	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Sárským	sárský	k2eAgNnSc7d1	sárský
teritoriem	teritorium	k1gNnSc7	teritorium
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
poslalo	poslat	k5eAaPmAgNnS	poslat
vojáky	voják	k1gMnPc4	voják
do	do	k7c2	do
Porýní	Porýní	k1gNnSc2	Porýní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
Versailleskou	versailleský	k2eAgFnSc7d1	Versailleská
smlouvou	smlouva	k1gFnSc7	smlouva
původně	původně	k6eAd1	původně
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
<g/>
.	.	kIx.	.
<g/>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
republika	republika	k1gFnSc1	republika
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Německu	Německo	k1gNnSc6	Německo
připojena	připojit	k5eAaPmNgFnS	připojit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1938	[number]	k4	1938
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Anschluss	Anschluss	k1gInSc1	Anschluss
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Rakousku	Rakousko	k1gNnSc6	Rakousko
obrátil	obrátit	k5eAaPmAgMnS	obrátit
Hitler	Hitler	k1gMnSc1	Hitler
svůj	svůj	k3xOyFgInSc4	svůj
zrak	zrak	k1gInSc4	zrak
na	na	k7c4	na
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
národnostní	národnostní	k2eAgFnSc1d1	národnostní
menšina	menšina	k1gFnSc1	menšina
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
silná	silný	k2eAgFnSc1d1	silná
počtem	počet	k1gInSc7	počet
3,3	[number]	k4	3,3
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
dožadovala	dožadovat	k5eAaImAgFnS	dožadovat
nejen	nejen	k6eAd1	nejen
autonomie	autonomie	k1gFnSc1	autonomie
jako	jako	k8xC	jako
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
přímo	přímo	k6eAd1	přímo
odtržení	odtržení	k1gNnSc4	odtržení
a	a	k8xC	a
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
pod	pod	k7c7	pod
heslem	heslo	k1gNnSc7	heslo
Heim	Heim	k1gInSc1	Heim
ins	ins	k?	ins
Reich	Reich	k?	Reich
(	(	kIx(	(
<g/>
Domů	dům	k1gInPc2	dům
do	do	k7c2	do
Říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1938	[number]	k4	1938
se	se	k3xPyFc4	se
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
představitelé	představitel	k1gMnPc1	představitel
čtyř	čtyři	k4xCgFnPc2	čtyři
mocností	mocnost	k1gFnPc2	mocnost
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
(	(	kIx(	(
<g/>
Německá	německý	k2eAgFnSc1d1	německá
říše	říše	k1gFnSc1	říše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Benito	Benit	k2eAgNnSc1d1	Benito
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neville	Neville	k1gInSc1	Neville
Chamberlain	Chamberlain	k2eAgInSc1d1	Chamberlain
(	(	kIx(	(
<g/>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
a	a	k8xC	a
Édouard	Édouard	k1gMnSc1	Édouard
Daladier	Daladier	k1gMnSc1	Daladier
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
Mnichovském	mnichovský	k2eAgInSc6d1	mnichovský
diktátu	diktát	k1gInSc6	diktát
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
přinuceno	přinucen	k2eAgNnSc1d1	přinuceno
odstoupit	odstoupit	k5eAaPmF	odstoupit
své	svůj	k3xOyFgNnSc4	svůj
tzv.	tzv.	kA	tzv.
sudetské	sudetský	k2eAgNnSc1d1	sudetské
pohraničí	pohraničí	k1gNnSc1	pohraničí
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
využil	využít	k5eAaPmAgMnS	využít
Hitler	Hitler	k1gMnSc1	Hitler
rozporů	rozpor	k1gInPc2	rozpor
mezi	mezi	k7c7	mezi
politickými	politický	k2eAgMnPc7d1	politický
představiteli	představitel	k1gMnPc7	představitel
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
jako	jako	k8xS	jako
záminku	záminka	k1gFnSc4	záminka
k	k	k7c3	k
rozbití	rozbití	k1gNnSc3	rozbití
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
k	k	k7c3	k
obsazení	obsazení	k1gNnSc3	obsazení
historických	historický	k2eAgFnPc2d1	historická
Českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
přeměně	přeměna	k1gFnSc3	přeměna
v	v	k7c4	v
Protektorát	protektorát	k1gInSc4	protektorát
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
jemu	on	k3xPp3gMnSc3	on
poddajného	poddajný	k2eAgInSc2d1	poddajný
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
měsíci	měsíc	k1gInSc6	měsíc
obsadila	obsadit	k5eAaPmAgFnS	obsadit
německá	německý	k2eAgFnSc1d1	německá
vojska	vojsko	k1gNnPc4	vojsko
také	také	k9	také
přístav	přístav	k1gInSc1	přístav
Memel	Memela	k1gFnPc2	Memela
v	v	k7c6	v
Litevské	litevský	k2eAgFnSc6d1	Litevská
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
politika	politika	k1gFnSc1	politika
tzv.	tzv.	kA	tzv.
appeasementu	appeasement	k1gInSc2	appeasement
tak	tak	k9	tak
zcela	zcela	k6eAd1	zcela
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
tzv.	tzv.	kA	tzv.
Velkoněmecké	velkoněmecký	k2eAgFnSc2d1	Velkoněmecká
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
připravila	připravit	k5eAaPmAgFnS	připravit
na	na	k7c4	na
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
podpisem	podpis	k1gInSc7	podpis
paktu	pakt	k1gInSc2	pakt
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc1	Ribbentrop-Molotov
a	a	k8xC	a
naplánovala	naplánovat	k5eAaBmAgFnS	naplánovat
tzv.	tzv.	kA	tzv.
operaci	operace	k1gFnSc4	operace
Himmler	Himmler	k1gInSc4	Himmler
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
falešný	falešný	k2eAgInSc1d1	falešný
polský	polský	k2eAgInSc1d1	polský
útok	útok	k1gInSc1	útok
na	na	k7c4	na
říšskou	říšský	k2eAgFnSc4d1	říšská
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
vysílač	vysílač	k1gInSc4	vysílač
Hlivice	Hlivice	k1gFnSc2	Hlivice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Reichssender	Reichssender	k1gMnSc1	Reichssender
Gleiwitz	Gleiwitz	k1gMnSc1	Gleiwitz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1939	[number]	k4	1939
německý	německý	k2eAgInSc1d1	německý
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
zahájil	zahájit	k5eAaPmAgInS	zahájit
invazi	invaze	k1gFnSc4	invaze
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
obsadil	obsadit	k5eAaPmAgMnS	obsadit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
opačné	opačný	k2eAgFnSc2d1	opačná
světové	světový	k2eAgFnSc2d1	světová
strany	strana	k1gFnSc2	strana
začala	začít	k5eAaPmAgFnS	začít
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
východní	východní	k2eAgFnSc1d1	východní
polská	polský	k2eAgFnSc1d1	polská
území	území	k1gNnSc1	území
obsazovat	obsazovat	k5eAaImF	obsazovat
sovětská	sovětský	k2eAgFnSc1d1	sovětská
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
reagovaly	reagovat	k5eAaBmAgFnP	reagovat
na	na	k7c4	na
invazi	invaze	k1gFnSc4	invaze
Polska	Polsko	k1gNnSc2	Polsko
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
války	válka	k1gFnSc2	válka
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
formálně	formálně	k6eAd1	formálně
začala	začít	k5eAaPmAgFnS	začít
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
Francie	Francie	k1gFnSc1	Francie
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
podepsala	podepsat	k5eAaPmAgFnS	podepsat
příměří	příměří	k1gNnSc4	příměří
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
nacistická	nacistický	k2eAgNnPc4d1	nacistické
vojska	vojsko	k1gNnPc4	vojsko
obsadila	obsadit	k5eAaPmAgFnS	obsadit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
francouzského	francouzský	k2eAgNnSc2d1	francouzské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
však	však	k9	však
německé	německý	k2eAgInPc4d1	německý
vzdušné	vzdušný	k2eAgInPc4d1	vzdušný
útoky	útok	k1gInPc4	útok
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
úspěšně	úspěšně	k6eAd1	úspěšně
odrazili	odrazit	k5eAaPmAgMnP	odrazit
v	v	k7c6	v
leteckých	letecký	k2eAgInPc6d1	letecký
bojích	boj	k1gInPc6	boj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
jako	jako	k8xS	jako
bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
jediným	jediný	k2eAgMnSc7d1	jediný
vážným	vážný	k2eAgMnSc7d1	vážný
účastníkem	účastník	k1gMnSc7	účastník
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
mocnostem	mocnost	k1gFnPc3	mocnost
Osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
Německo	Německo	k1gNnSc1	Německo
vypovědělo	vypovědět	k5eAaPmAgNnS	vypovědět
pakt	pakt	k1gInSc4	pakt
Molotov-Ribbentrop	Molotov-Ribbentrop	k1gInSc1	Molotov-Ribbentrop
a	a	k8xC	a
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
mělo	mít	k5eAaImAgNnS	mít
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
mocnosti	mocnost	k1gFnPc1	mocnost
Osy	osa	k1gFnSc2	osa
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
většinu	většina	k1gFnSc4	většina
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
postupovala	postupovat	k5eAaImAgNnP	postupovat
motorizovaná	motorizovaný	k2eAgNnPc1d1	motorizované
německá	německý	k2eAgNnPc1d1	německé
vojska	vojsko	k1gNnPc1	vojsko
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Blitzkrieg	Blitzkrieg	k1gInSc1	Blitzkrieg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
na	na	k7c4	na
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
900	[number]	k4	900
dní	den	k1gInPc2	den
oblehla	oblehnout	k5eAaPmAgFnS	oblehnout
Leningrad	Leningrad	k1gInSc4	Leningrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
začala	začít	k5eAaPmAgNnP	začít
německá	německý	k2eAgNnPc1d1	německé
vojska	vojsko	k1gNnPc1	vojsko
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
ustupovat	ustupovat	k5eAaImF	ustupovat
z	z	k7c2	z
dobytých	dobytý	k2eAgNnPc2d1	dobyté
území	území	k1gNnPc2	území
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
další	další	k2eAgFnPc1d1	další
porážky	porážka	k1gFnPc1	porážka
v	v	k7c6	v
těžkých	těžký	k2eAgFnPc6d1	těžká
bitvách	bitva	k1gFnPc6	bitva
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
definitivnímu	definitivní	k2eAgInSc3d1	definitivní
obratu	obrat	k1gInSc3	obrat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
září	září	k1gNnSc6	září
1943	[number]	k4	1943
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
německý	německý	k2eAgMnSc1d1	německý
spojenec	spojenec	k1gMnSc1	spojenec
<g/>
,	,	kIx,	,
Italské	italský	k2eAgNnSc1d1	italské
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
proti	proti	k7c3	proti
nastupujícím	nastupující	k2eAgFnPc3d1	nastupující
spojeneckým	spojenecký	k2eAgFnPc3d1	spojenecká
silám	síla	k1gFnPc3	síla
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
velké	velký	k2eAgFnPc1d1	velká
jednotky	jednotka	k1gFnPc1	jednotka
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
a	a	k8xC	a
Waffen	Waffen	k1gInSc4	Waffen
SS	SS	kA	SS
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pak	pak	k6eAd1	pak
chyběly	chybět	k5eAaImAgInP	chybět
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
bojištích	bojiště	k1gNnPc6	bojiště
včetně	včetně	k7c2	včetně
východní	východní	k2eAgFnSc2d1	východní
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgMnPc1d1	západní
spojenci	spojenec	k1gMnPc1	spojenec
připravili	připravit	k5eAaPmAgMnP	připravit
a	a	k8xC	a
provedli	provést	k5eAaPmAgMnP	provést
vylodění	vylodění	k1gNnSc4	vylodění
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
rozsahu	rozsah	k1gInSc6	rozsah
otevřela	otevřít	k5eAaPmAgFnS	otevřít
západní	západní	k2eAgFnSc1d1	západní
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
německý	německý	k2eAgInSc4d1	německý
protiútok	protiútok	k1gInSc4	protiútok
v	v	k7c6	v
Ardenách	Ardeny	k1gFnPc6	Ardeny
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
síly	síla	k1gFnSc2	síla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
Hilterovu	Hilterův	k2eAgFnSc4d1	Hilterův
sebevraždu	sebevražda	k1gFnSc4	sebevražda
a	a	k8xC	a
bitvu	bitva	k1gFnSc4	bitva
o	o	k7c4	o
Berlín	Berlín	k1gInSc4	Berlín
se	se	k3xPyFc4	se
Německo	Německo	k1gNnSc1	Německo
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
nejkrvavějším	krvavý	k2eAgInSc7d3	nejkrvavější
konfliktem	konflikt	k1gInSc7	konflikt
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
a	a	k8xC	a
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
způsobila	způsobit	k5eAaPmAgFnS	způsobit
smrt	smrt	k1gFnSc1	smrt
asi	asi	k9	asi
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
26,6	[number]	k4	26,6
miliónu	milión	k4xCgInSc2	milión
sovětských	sovětský	k2eAgMnPc2d1	sovětský
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
byly	být	k5eAaImAgFnP	být
mezi	mezi	k7c4	mezi
3	[number]	k4	3
250	[number]	k4	250
000	[number]	k4	000
a	a	k8xC	a
5	[number]	k4	5
300	[number]	k4	300
000	[number]	k4	000
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
,	,	kIx,	,
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
plošného	plošný	k2eAgNnSc2d1	plošné
bombardování	bombardování	k1gNnSc2	bombardování
měst	město	k1gNnPc2	město
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
letectvem	letectvo	k1gNnSc7	letectvo
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
přišly	přijít	k5eAaPmAgInP	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc1	dva
miliony	milion	k4xCgInPc1	milion
německých	německý	k2eAgMnPc2d1	německý
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejhrůznějších	hrůzný	k2eAgInPc2d3	nejhrůznější
jevů	jev	k1gInPc2	jev
období	období	k1gNnSc2	období
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
holokaust	holokaust	k1gInSc1	holokaust
<g/>
,	,	kIx,	,
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
prováděné	prováděný	k2eAgNnSc1d1	prováděné
systematické	systematický	k2eAgNnSc1d1	systematické
pronásledování	pronásledování	k1gNnSc1	pronásledování
a	a	k8xC	a
hromadné	hromadný	k2eAgNnSc1d1	hromadné
vyvražďování	vyvražďování	k1gNnSc1	vyvražďování
etnických	etnický	k2eAgFnPc2d1	etnická
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
velkých	velký	k2eAgFnPc2d1	velká
skupin	skupina	k1gFnPc2	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Obětmi	oběť	k1gFnPc7	oběť
holocaustu	holocaust	k1gInSc2	holocaust
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
asi	asi	k9	asi
šest	šest	k4xCc1	šest
milionů	milion	k4xCgInPc2	milion
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
220	[number]	k4	220
000	[number]	k4	000
a	a	k8xC	a
1	[number]	k4	1
500	[number]	k4	500
000	[number]	k4	000
Rómů	Róm	k1gMnPc2	Róm
<g/>
,	,	kIx,	,
275	[number]	k4	275
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
mentálním	mentální	k2eAgNnSc7d1	mentální
nebo	nebo	k8xC	nebo
zdravotním	zdravotní	k2eAgNnSc7d1	zdravotní
postižením	postižení	k1gNnSc7	postižení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Akce	akce	k1gFnSc2	akce
T	T	kA	T
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
tisíce	tisíc	k4xCgInPc1	tisíc
Svědků	svědek	k1gMnPc2	svědek
Jehovových	Jehovův	k2eAgFnPc2d1	Jehovova
a	a	k8xC	a
homosexuálů	homosexuál	k1gMnPc2	homosexuál
<g/>
.	.	kIx.	.
</s>
<s>
Státními	státní	k2eAgInPc7d1	státní
a	a	k8xC	a
vojenskými	vojenský	k2eAgInPc7d1	vojenský
útvary	útvar	k1gInPc7	útvar
byly	být	k5eAaImAgFnP	být
zavražděny	zavražděn	k2eAgFnPc4d1	zavražděna
stovky	stovka	k1gFnPc4	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
politických	politický	k2eAgMnPc2d1	politický
protivníků	protivník	k1gMnPc2	protivník
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
cca	cca	kA	cca
šest	šest	k4xCc1	šest
milionů	milion	k4xCgInPc2	milion
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
a	a	k8xC	a
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
obětí	oběť	k1gFnSc7	oběť
nelidského	lidský	k2eNgNnSc2d1	nelidské
zacházení	zacházení	k1gNnSc2	zacházení
asi	asi	k9	asi
2,8	[number]	k4	2,8
milionu	milion	k4xCgInSc2	milion
sovětských	sovětský	k2eAgMnPc2d1	sovětský
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Nacistický	nacistický	k2eAgMnSc1d1	nacistický
Generalplan	Generalplan	k1gMnSc1	Generalplan
Ost	Ost	k1gMnSc1	Ost
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
pro	pro	k7c4	pro
německé	německý	k2eAgNnSc4d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
vytvořit	vytvořit	k5eAaPmF	vytvořit
životní	životní	k2eAgInSc4d1	životní
prostor	prostor	k1gInSc4	prostor
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
chystal	chystat	k5eAaImAgMnS	chystat
po	po	k7c6	po
vítězné	vítězný	k2eAgFnSc6d1	vítězná
válce	válka	k1gFnSc6	válka
germanizaci	germanizace	k1gFnSc4	germanizace
<g/>
,	,	kIx,	,
vysídlení	vysídlení	k1gNnSc4	vysídlení
a	a	k8xC	a
likvidaci	likvidace	k1gFnSc4	likvidace
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
Rusů	Rus	k1gMnPc2	Rus
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
<g/>
Prohra	prohra	k1gFnSc1	prohra
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
pro	pro	k7c4	pro
Německo	Německo	k1gNnSc4	Německo
znamenala	znamenat	k5eAaImAgFnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
území	území	k1gNnSc2	území
východně	východně	k6eAd1	východně
od	od	k7c2	od
řek	řeka	k1gFnPc2	řeka
Nisy	Nisa	k1gFnSc2	Nisa
a	a	k8xC	a
Odry	Odra	k1gFnSc2	Odra
včetně	včetně	k7c2	včetně
exklávy	exkláva	k1gFnSc2	exkláva
Východní	východní	k2eAgNnSc4d1	východní
Prusko	Prusko	k1gNnSc4	Prusko
a	a	k8xC	a
vysídlení	vysídlení	k1gNnSc4	vysídlení
milionů	milion	k4xCgInPc2	milion
etnických	etnický	k2eAgMnPc2d1	etnický
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
dřívějších	dřívější	k2eAgNnPc2d1	dřívější
východních	východní	k2eAgNnPc2d1	východní
území	území	k1gNnPc2	území
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
z	z	k7c2	z
okupovaných	okupovaný	k2eAgFnPc2d1	okupovaná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
osvobozovaných	osvobozovaný	k2eAgFnPc2d1	osvobozovaný
od	od	k7c2	od
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masovému	masový	k2eAgNnSc3d1	masové
znásilňování	znásilňování	k1gNnSc3	znásilňování
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
bombardování	bombardování	k1gNnSc2	bombardování
během	během	k7c2	během
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
mnoho	mnoho	k4c1	mnoho
měst	město	k1gNnPc2	město
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
obrovské	obrovský	k2eAgFnPc4d1	obrovská
ztráty	ztráta	k1gFnPc4	ztráta
na	na	k7c6	na
kulturním	kulturní	k2eAgNnSc6d1	kulturní
dědictví	dědictví	k1gNnSc6	dědictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byli	být	k5eAaImAgMnP	být
zadrženi	zadržen	k2eAgMnPc1d1	zadržen
vrcholní	vrcholný	k2eAgMnPc1d1	vrcholný
nacisté	nacista	k1gMnPc1	nacista
a	a	k8xC	a
souzeni	souzen	k2eAgMnPc1d1	souzen
za	za	k7c4	za
válečné	válečný	k2eAgMnPc4d1	válečný
zločiny	zločin	k1gMnPc4	zločin
a	a	k8xC	a
genocidu	genocida	k1gFnSc4	genocida
v	v	k7c6	v
Norimberském	norimberský	k2eAgInSc6d1	norimberský
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Východní	východní	k2eAgNnSc1d1	východní
a	a	k8xC	a
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
kapitulaci	kapitulace	k1gFnSc6	kapitulace
Německa	Německo	k1gNnSc2	Německo
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
území	území	k1gNnSc1	území
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
spojenců	spojenec	k1gMnPc2	spojenec
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
okupačních	okupační	k2eAgFnPc2d1	okupační
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Berlín	Berlín	k1gInSc1	Berlín
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
čtyřmocenský	čtyřmocenský	k2eAgInSc4d1	čtyřmocenský
status	status	k1gInSc4	status
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
čtyři	čtyři	k4xCgInPc1	čtyři
sektory	sektor	k1gInPc1	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
zóny	zóna	k1gFnPc1	zóna
a	a	k8xC	a
sektory	sektor	k1gInPc1	sektor
přijaly	přijmout	k5eAaPmAgInP	přijmout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6,5	[number]	k4	6,5
milionů	milion	k4xCgInPc2	milion
etnických	etnický	k2eAgMnPc2d1	etnický
Němců	Němec	k1gMnPc2	Němec
vyhnaných	vyhnaný	k2eAgFnPc2d1	vyhnaná
z	z	k7c2	z
bývalých	bývalý	k2eAgFnPc2d1	bývalá
východních	východní	k2eAgFnPc2d1	východní
oblastí	oblast	k1gFnPc2	oblast
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
obývaných	obývaný	k2eAgInPc2d1	obývaný
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
<g/>
Západní	západní	k2eAgFnSc2d1	západní
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
ovládané	ovládaný	k2eAgInPc1d1	ovládaný
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
<g/>
,	,	kIx,	,
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
sloučeny	sloučen	k2eAgInPc1d1	sloučen
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
Spolkovou	spolkový	k2eAgFnSc4d1	spolková
republiku	republika	k1gFnSc4	republika
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
Bundesrepublik	Bundesrepublika	k1gFnPc2	Bundesrepublika
Deutschland	Deutschland	k1gInSc1	Deutschland
<g/>
,	,	kIx,	,
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
zkratka	zkratka	k1gFnSc1	zkratka
BRD	Brdy	k1gInPc2	Brdy
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
SRN	SRN	kA	SRN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
Německá	německý	k2eAgFnSc1d1	německá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
Deutsche	Deutsche	k1gInSc1	Deutsche
Demokratische	Demokratische	k1gNnSc2	Demokratische
Republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
DDR	DDR	kA	DDR
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neformálně	formálně	k6eNd1	formálně
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
státní	státní	k2eAgInPc1d1	státní
útvary	útvar	k1gInPc1	útvar
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Východní	východní	k2eAgNnSc1d1	východní
Německo	Německo	k1gNnSc1	Německo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
NDR	NDR	kA	NDR
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Východní	východní	k2eAgInSc1d1	východní
Berlín	Berlín	k1gInSc1	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc1	Německo
zvolila	zvolit	k5eAaPmAgFnS	zvolit
Bonn	Bonn	k1gInSc4	Bonn
jako	jako	k8xC	jako
prozatímní	prozatímní	k2eAgNnSc4d1	prozatímní
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
(	(	kIx(	(
<g/>
vážným	vážný	k2eAgMnSc7d1	vážný
kandidátem	kandidát	k1gMnSc7	kandidát
byl	být	k5eAaImAgMnS	být
také	také	k9	také
Frankfurt	Frankfurt	k1gInSc4	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zdůraznila	zdůraznit	k5eAaPmAgFnS	zdůraznit
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytvoření	vytvoření	k1gNnSc2	vytvoření
dvou	dva	k4xCgInPc2	dva
německých	německý	k2eAgInPc2d1	německý
států	stát	k1gInPc2	stát
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
umělé	umělý	k2eAgNnSc4d1	umělé
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
dočasné	dočasný	k2eAgNnSc4d1	dočasné
řešení	řešení	k1gNnSc4	řešení
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
Berlínu	Berlín	k1gInSc3	Berlín
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
vrátí	vrátit	k5eAaPmIp3nS	vrátit
jeho	jeho	k3xOp3gFnPc4	jeho
funkce	funkce	k1gFnPc4	funkce
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
znovusjednoceného	znovusjednocený	k2eAgNnSc2d1	znovusjednocené
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
<g/>
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
jako	jako	k8xS	jako
federální	federální	k2eAgFnSc1d1	federální
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
sociálně	sociálně	k6eAd1	sociálně
tržním	tržní	k2eAgNnSc7d1	tržní
hospodářstvím	hospodářství	k1gNnSc7	hospodářství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
soziale	soziale	k6eAd1	soziale
Marktwirtschaft	Marktwirtschaft	k2eAgInSc1d1	Marktwirtschaft
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
příjemcem	příjemce	k1gMnSc7	příjemce
finanční	finanční	k2eAgFnSc2d1	finanční
a	a	k8xC	a
materiální	materiální	k2eAgFnSc2d1	materiální
podpory	podpora	k1gFnSc2	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
při	při	k7c6	při
poválečné	poválečný	k2eAgFnSc6d1	poválečná
obnově	obnova	k1gFnSc6	obnova
<g/>
,	,	kIx,	,
poskytnuté	poskytnutý	k2eAgFnPc1d1	poskytnutá
evropským	evropský	k2eAgFnPc3d1	Evropská
zemím	zem	k1gFnPc3	zem
podle	podle	k7c2	podle
Marshallova	Marshallův	k2eAgInSc2d1	Marshallův
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
a	a	k8xC	a
využilo	využít	k5eAaPmAgNnS	využít
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
rychlé	rychlý	k2eAgFnSc3d1	rychlá
přestavbě	přestavba	k1gFnSc3	přestavba
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolkovým	spolkový	k2eAgInSc7d1	spolkový
parlamentem	parlament	k1gInSc7	parlament
SRN	SRN	kA	SRN
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
zvolen	zvolit	k5eAaPmNgInS	zvolit
Konrad	Konrad	k1gInSc1	Konrad
Adenauer	Adenaura	k1gFnPc2	Adenaura
prvním	první	k4xOgMnSc6	první
spolkovým	spolkový	k2eAgMnSc7d1	spolkový
kancléřem	kancléř	k1gMnSc7	kancléř
(	(	kIx(	(
<g/>
Bundeskanzler	Bundeskanzler	k1gMnSc1	Bundeskanzler
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úřadu	úřad	k1gInSc6	úřad
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
a	a	k8xC	a
následně	následně	k6eAd1	následně
pod	pod	k7c7	pod
Erhardovým	Erhardův	k2eAgNnSc7d1	Erhardův
vedením	vedení	k1gNnSc7	vedení
země	zem	k1gFnSc2	zem
zažila	zažít	k5eAaPmAgFnS	zažít
prudký	prudký	k2eAgInSc4d1	prudký
a	a	k8xC	a
dlouhotrvající	dlouhotrvající	k2eAgInSc4d1	dlouhotrvající
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
již	již	k6eAd1	již
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
historie	historie	k1gFnSc2	historie
vešel	vejít	k5eAaPmAgInS	vejít
jako	jako	k9	jako
"	"	kIx"	"
<g/>
německý	německý	k2eAgInSc1d1	německý
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
zázrak	zázrak	k1gInSc1	zázrak
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Wirtschaftswunder	Wirtschaftswunder	k1gInSc1	Wirtschaftswunder
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
připojilo	připojit	k5eAaPmAgNnS	připojit
k	k	k7c3	k
NATO	NATO	kA	NATO
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Evropského	evropský	k2eAgNnSc2d1	Evropské
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východní	východní	k2eAgNnSc1d1	východní
Německo	Německo	k1gNnSc1	Německo
bylo	být	k5eAaImAgNnS	být
začleněno	začlenit	k5eAaPmNgNnS	začlenit
do	do	k7c2	do
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
s	s	k7c7	s
politickou	politický	k2eAgFnSc7d1	politická
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
kontrolou	kontrola	k1gFnSc7	kontrola
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
silné	silný	k2eAgNnSc4d1	silné
sovětské	sovětský	k2eAgFnPc4d1	sovětská
vojenské	vojenský	k2eAgFnPc4d1	vojenská
jednotky	jednotka	k1gFnPc4	jednotka
a	a	k8xC	a
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Východní	východní	k2eAgNnSc1d1	východní
Německo	Německo	k1gNnSc1	Německo
prohlašovalo	prohlašovat	k5eAaImAgNnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
demokratické	demokratický	k2eAgNnSc1d1	demokratické
<g/>
,	,	kIx,	,
politická	politický	k2eAgFnSc1d1	politická
moc	moc	k1gFnSc1	moc
byla	být	k5eAaImAgFnS	být
vykonávána	vykonávat	k5eAaImNgFnS	vykonávat
pouze	pouze	k6eAd1	pouze
předními	přední	k2eAgMnPc7d1	přední
členy	člen	k1gMnPc7	člen
politbyra	politbyro	k1gNnSc2	politbyro
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
SED	sed	k1gInSc1	sed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podporované	podporovaný	k2eAgNnSc1d1	podporované
tajnou	tajný	k2eAgFnSc7d1	tajná
službou	služba	k1gFnSc7	služba
Stasi	stase	k1gFnSc4	stase
(	(	kIx(	(
<g/>
Staatssicherheit	Staatssicherheit	k1gInSc4	Staatssicherheit
<g/>
)	)	kIx)	)
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
dílčích	dílčí	k2eAgFnPc2d1	dílčí
organizací	organizace	k1gFnPc2	organizace
kontrolujících	kontrolující	k2eAgFnPc2d1	kontrolující
téměř	téměř	k6eAd1	téměř
veškeré	veškerý	k3xTgNnSc4	veškerý
dění	dění	k1gNnSc4	dění
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
ekonomika	ekonomika	k1gFnSc1	ekonomika
a	a	k8xC	a
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
RVHP	RVHP	kA	RVHP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
východoněmecká	východoněmecký	k2eAgFnSc1d1	východoněmecká
propaganda	propaganda	k1gFnSc1	propaganda
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
domnělých	domnělý	k2eAgFnPc6d1	domnělá
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
skutečných	skutečný	k2eAgFnPc6d1	skutečná
výhodách	výhoda	k1gFnPc6	výhoda
tamějších	tamější	k2eAgInPc2d1	tamější
sociálních	sociální	k2eAgInPc2d1	sociální
programů	program	k1gInPc2	program
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
hrozila	hrozit	k5eAaImAgFnS	hrozit
invazí	invaze	k1gFnSc7	invaze
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
SRN	SRN	kA	SRN
a	a	k8xC	a
západních	západní	k2eAgMnPc2d1	západní
"	"	kIx"	"
<g/>
imperialistů	imperialista	k1gMnPc2	imperialista
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc4d1	velký
počty	počet	k1gInPc4	počet
občanů	občan	k1gMnPc2	občan
NDR	NDR	kA	NDR
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgInP	snažit
uniknout	uniknout	k5eAaPmF	uniknout
do	do	k7c2	do
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
za	za	k7c7	za
svobodou	svoboda	k1gFnSc7	svoboda
a	a	k8xC	a
prosperitou	prosperita	k1gFnSc7	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
částečně	částečně	k6eAd1	částečně
možné	možný	k2eAgNnSc4d1	možné
přechodem	přechod	k1gInSc7	přechod
nebo	nebo	k8xC	nebo
přejezdem	přejezd	k1gInSc7	přejezd
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
onom	onen	k3xDgInSc6	onen
roce	rok	k1gInSc6	rok
však	však	k9	však
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prakticky	prakticky	k6eAd1	prakticky
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
východním	východní	k2eAgMnPc3d1	východní
Němcům	Němec	k1gMnPc3	Němec
útěk	útěk	k1gInSc4	útěk
do	do	k7c2	do
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zeď	zeď	k1gFnSc1	zeď
napříč	napříč	k7c7	napříč
Berlínem	Berlín	k1gInSc7	Berlín
a	a	k8xC	a
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
hranici	hranice	k1gFnSc6	hranice
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
s	s	k7c7	s
územím	území	k1gNnSc7	území
NDR	NDR	kA	NDR
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
pád	pád	k1gInSc1	pád
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
stal	stát	k5eAaPmAgInS	stát
nejen	nejen	k6eAd1	nejen
symbolem	symbol	k1gInSc7	symbol
znovusjednocení	znovusjednocení	k1gNnSc2	znovusjednocení
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
zásadních	zásadní	k2eAgFnPc2d1	zásadní
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Wende	Wend	k1gMnSc5	Wend
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
symbolem	symbol	k1gInSc7	symbol
pádu	pád	k1gInSc2	pád
komunismu	komunismus	k1gInSc2	komunismus
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
samotném	samotný	k2eAgInSc6d1	samotný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
východním	východní	k2eAgNnSc7d1	východní
a	a	k8xC	a
západním	západní	k2eAgNnSc7d1	západní
Německem	Německo	k1gNnSc7	Německo
zmírnilo	zmírnit	k5eAaPmAgNnS	zmírnit
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
měrou	míra	k1gFnSc7wR	míra
přispěla	přispět	k5eAaPmAgFnS	přispět
tzv.	tzv.	kA	tzv.
Ostpolitik	Ostpolitika	k1gFnPc2	Ostpolitika
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prosadil	prosadit	k5eAaPmAgMnS	prosadit
spolkový	spolkový	k2eAgMnSc1d1	spolkový
kancléř	kancléř	k1gMnSc1	kancléř
Willy	Willa	k1gFnSc2	Willa
Brandt	Brandt	k1gMnSc1	Brandt
a	a	k8xC	a
ve	v	k7c4	v
které	který	k3yQgInPc4	který
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
nástupce	nástupce	k1gMnSc1	nástupce
Helmut	Helmut	k1gMnSc1	Helmut
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
normalizovaly	normalizovat	k5eAaBmAgInP	normalizovat
i	i	k9	i
vztahy	vztah	k1gInPc1	vztah
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
zeměmi	zem	k1gFnPc7	zem
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
SRN	SRN	kA	SRN
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1973	[number]	k4	1973
tzv.	tzv.	kA	tzv.
Pražskou	pražský	k2eAgFnSc4d1	Pražská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgMnSc1d1	československý
prezident	prezident	k1gMnSc1	prezident
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
vykonal	vykonat	k5eAaPmAgMnS	vykonat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1978	[number]	k4	1978
oficiální	oficiální	k2eAgFnSc4d1	oficiální
návštěvu	návštěva	k1gFnSc4	návštěva
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1989	[number]	k4	1989
se	se	k3xPyFc4	se
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
železnou	železný	k2eAgFnSc4d1	železná
oponu	opona	k1gFnSc4	opona
odstranit	odstranit	k5eAaPmF	odstranit
a	a	k8xC	a
otevřít	otevřít	k5eAaPmF	otevřít
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
emigraci	emigrace	k1gFnSc4	emigrace
tisíců	tisíc	k4xCgInPc2	tisíc
východních	východní	k2eAgMnPc2d1	východní
Němců	Němec	k1gMnPc2	Němec
do	do	k7c2	do
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
přes	přes	k7c4	přes
Maďarsko	Maďarsko	k1gNnSc4	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c6	na
NDR	NDR	kA	NDR
zničující	zničující	k2eAgInPc4d1	zničující
dopady	dopad	k1gInPc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
pravidelným	pravidelný	k2eAgFnPc3d1	pravidelná
masovým	masový	k2eAgFnPc3d1	masová
demonstracím	demonstrace	k1gFnPc3	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
získávaly	získávat	k5eAaImAgFnP	získávat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
na	na	k7c6	na
podpoře	podpora	k1gFnSc6	podpora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
pádu	pád	k1gInSc2	pád
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
zmírnily	zmírnit	k5eAaPmAgInP	zmírnit
východoněmecké	východoněmecký	k2eAgInPc1d1	východoněmecký
úřady	úřad	k1gInPc1	úřad
nečekaně	nečekaně	k6eAd1	nečekaně
omezení	omezení	k1gNnSc4	omezení
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dovolilo	dovolit	k5eAaPmAgNnS	dovolit
východoněmeckým	východoněmecký	k2eAgMnPc3d1	východoněmecký
občanům	občan	k1gMnPc3	občan
cestovat	cestovat	k5eAaImF	cestovat
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
zamýšlelo	zamýšlet	k5eAaImAgNnS	zamýšlet
udržet	udržet	k5eAaPmF	udržet
Východní	východní	k2eAgNnSc1d1	východní
Německo	Německo	k1gNnSc1	Německo
jako	jako	k8xC	jako
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otevření	otevření	k1gNnSc1	otevření
hranic	hranice	k1gFnPc2	hranice
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
urychlení	urychlení	k1gNnSc3	urychlení
sjednocujícího	sjednocující	k2eAgInSc2d1	sjednocující
reformního	reformní	k2eAgInSc2d1	reformní
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
ve	v	k7c4	v
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
konečném	konečný	k2eAgNnSc6d1	konečné
uspořádání	uspořádání	k1gNnSc6	uspořádání
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
smlouva	smlouva	k1gFnSc1	smlouva
4	[number]	k4	4
+	+	kIx~	+
2	[number]	k4	2
<g/>
)	)	kIx)	)
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
čtyři	čtyři	k4xCgFnPc1	čtyři
okupační	okupační	k2eAgFnPc1d1	okupační
mocnosti	mocnost	k1gFnPc1	mocnost
vzdaly	vzdát	k5eAaPmAgFnP	vzdát
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
daných	daný	k2eAgNnPc2d1	dané
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
bezpodmínečné	bezpodmínečný	k2eAgFnSc2d1	bezpodmínečná
kapitulace	kapitulace	k1gFnSc2	kapitulace
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
vedené	vedený	k2eAgNnSc1d1	vedené
spolkovým	spolkový	k2eAgMnSc7d1	spolkový
kancléřem	kancléř	k1gMnSc7	kancléř
Helmutem	Helmut	k1gMnSc7	Helmut
Kohlem	Kohl	k1gMnSc7	Kohl
(	(	kIx(	(
<g/>
vůdce	vůdce	k1gMnSc1	vůdce
koalice	koalice	k1gFnSc2	koalice
CDU	CDU	kA	CDU
<g/>
/	/	kIx~	/
<g/>
CSU	CSU	kA	CSU
a	a	k8xC	a
FDP	FDP	kA	FDP
<g/>
,	,	kIx,	,
vicekancléřem	vicekancléř	k1gMnSc7	vicekancléř
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
byl	být	k5eAaImAgMnS	být
Hans-Dietrich	Hans-Dietrich	k1gMnSc1	Hans-Dietrich
Genscher	Genschra	k1gFnPc2	Genschra
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
prakticky	prakticky	k6eAd1	prakticky
opět	opět	k6eAd1	opět
plně	plně	k6eAd1	plně
suverénním	suverénní	k2eAgInSc7d1	suverénní
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
i	i	k8xC	i
formální	formální	k2eAgNnSc1d1	formální
znovusjednocení	znovusjednocení	k1gNnSc1	znovusjednocení
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
vstupem	vstup	k1gInSc7	vstup
pěti	pět	k4xCc2	pět
zemí	zem	k1gFnPc2	zem
bývalé	bývalý	k2eAgFnSc2d1	bývalá
NDR	NDR	kA	NDR
(	(	kIx(	(
<g/>
Neue	Neue	k1gFnSc1	Neue
Länder	Ländra	k1gFnPc2	Ländra
<g/>
)	)	kIx)	)
do	do	k7c2	do
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současné	současný	k2eAgNnSc1d1	současné
Německo	Německo	k1gNnSc1	Německo
od	od	k7c2	od
znovusjednocení	znovusjednocení	k1gNnSc2	znovusjednocení
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
smlouvy	smlouva	k1gFnPc4	smlouva
Berlín	Berlín	k1gInSc1	Berlín
<g/>
/	/	kIx~	/
<g/>
Bonn	Bonn	k1gInSc1	Bonn
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
provizorní	provizorní	k2eAgInSc1d1	provizorní
stav	stav	k1gInSc1	stav
ohledně	ohledně	k7c2	ohledně
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
změněn	změnit	k5eAaPmNgInS	změnit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
sjednoceného	sjednocený	k2eAgNnSc2d1	sjednocené
Německa	Německo	k1gNnSc2	Německo
definitivně	definitivně	k6eAd1	definitivně
stal	stát	k5eAaPmAgInS	stát
Berlín	Berlín	k1gInSc1	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Městu	město	k1gNnSc3	město
Bonnu	Bonn	k1gInSc6	Bonn
byl	být	k5eAaImAgMnS	být
přiznán	přiznán	k2eAgInSc4d1	přiznán
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
status	status	k1gInSc4	status
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
Bundesstadt	Bundesstadt	k2eAgMnSc1d1	Bundesstadt
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
spolkové	spolkový	k2eAgNnSc1d1	Spolkové
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	s	k7c7	s
zachováním	zachování	k1gNnSc7	zachování
některých	některý	k3yIgNnPc2	některý
spolkových	spolkový	k2eAgNnPc2d1	Spolkové
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
a	a	k8xC	a
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Přemístění	přemístění	k1gNnSc1	přemístění
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
dohodnutém	dohodnutý	k2eAgInSc6d1	dohodnutý
rozsahu	rozsah	k1gInSc6	rozsah
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
sociálnědemokratický	sociálnědemokratický	k2eAgMnSc1d1	sociálnědemokratický
politik	politik	k1gMnSc1	politik
Gerhard	Gerhard	k1gMnSc1	Gerhard
Schröder	Schröder	k1gMnSc1	Schröder
stal	stát	k5eAaPmAgMnS	stát
kancléřem	kancléř	k1gMnSc7	kancléř
historicky	historicky	k6eAd1	historicky
první	první	k4xOgInSc4	první
tzv.	tzv.	kA	tzv.
červeno-zelené	červenoelený	k2eAgFnSc2d1	červeno-zelená
koalice	koalice	k1gFnSc2	koalice
SPD	SPD	kA	SPD
a	a	k8xC	a
Svazu	svaz	k1gInSc2	svaz
90	[number]	k4	90
<g/>
/	/	kIx~	/
<g/>
Zelených	Zelených	k2eAgFnSc2d1	Zelených
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sjednocené	sjednocený	k2eAgNnSc1d1	sjednocené
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
politickém	politický	k2eAgNnSc6d1	politické
dění	dění	k1gNnSc6	dění
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Operace	operace	k1gFnSc2	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílely	podílet	k5eAaImAgInP	podílet
též	též	k9	též
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
skončení	skončení	k1gNnSc6	skončení
se	se	k3xPyFc4	se
Německo	Německo	k1gNnSc1	Německo
angažovalo	angažovat	k5eAaBmAgNnS	angažovat
při	při	k7c6	při
zajišťování	zajišťování	k1gNnSc6	zajišťování
stability	stabilita	k1gFnSc2	stabilita
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
Bundeswehru	bundeswehr	k1gInSc2	bundeswehr
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
mise	mise	k1gFnSc2	mise
NATO	NATO	kA	NATO
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zajistit	zajistit	k5eAaPmF	zajistit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
po	po	k7c4	po
svržení	svržení	k1gNnSc4	svržení
vlády	vláda	k1gFnSc2	vláda
Talibanu	Taliban	k1gInSc2	Taliban
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vojenská	vojenský	k2eAgNnPc1d1	vojenské
nasazení	nasazení	k1gNnPc1	nasazení
NATO	NATO	kA	NATO
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
jak	jak	k6eAd1	jak
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
samotném	samotný	k2eAgNnSc6d1	samotné
tak	tak	k8xS	tak
mezinárodně	mezinárodně	k6eAd1	mezinárodně
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
debaty	debata	k1gFnSc2	debata
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
vázáno	vázat	k5eAaImNgNnS	vázat
jak	jak	k8xC	jak
spojeneckými	spojenecký	k2eAgFnPc7d1	spojenecká
dohodami	dohoda	k1gFnPc7	dohoda
tak	tak	k6eAd1	tak
vnitrostátním	vnitrostátní	k2eAgNnSc7d1	vnitrostátní
právem	právo	k1gNnSc7	právo
k	k	k7c3	k
rozmístění	rozmístění	k1gNnSc3	rozmístění
jednotek	jednotka	k1gFnPc2	jednotka
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
vlastním	vlastní	k2eAgNnSc6d1	vlastní
území	území	k1gNnSc6	území
k	k	k7c3	k
výlučně	výlučně	k6eAd1	výlučně
obranným	obranný	k2eAgInPc3d1	obranný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
stala	stát	k5eAaPmAgFnS	stát
kancléřkou	kancléřka	k1gFnSc7	kancléřka
Německa	Německo	k1gNnSc2	Německo
Angela	Angela	k1gFnSc1	Angela
Merkelová	Merkelová	k1gFnSc1	Merkelová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
vládu	vláda	k1gFnSc4	vláda
tzv.	tzv.	kA	tzv.
velké	velký	k2eAgFnSc2d1	velká
koalice	koalice	k1gFnSc2	koalice
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
stran	strana	k1gFnPc2	strana
CDU	CDU	kA	CDU
<g/>
/	/	kIx~	/
<g/>
CSU	CSU	kA	CSU
a	a	k8xC	a
sociálně-demokratické	sociálněemokratický	k2eAgFnSc2d1	sociálně-demokratická
SPD	SPD	kA	SPD
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
druhá	druhý	k4xOgFnSc1	druhý
vláda	vláda	k1gFnSc1	vláda
kancléřky	kancléřka	k1gFnSc2	kancléřka
Merkelové	Merkelová	k1gFnSc2	Merkelová
jako	jako	k8xC	jako
koalice	koalice	k1gFnSc2	koalice
CDU	CDU	kA	CDU
<g/>
/	/	kIx~	/
<g/>
CSU	CSU	kA	CSU
a	a	k8xC	a
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
FDP	FDP	kA	FDP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
předseda	předseda	k1gMnSc1	předseda
Guido	Guido	k1gNnSc1	Guido
Westerwelle	Westerwelle	k1gInSc1	Westerwelle
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vicekancléřem	vicekancléř	k1gMnSc7	vicekancléř
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
tzv.	tzv.	kA	tzv.
velká	velký	k2eAgFnSc1d1	velká
koalice	koalice	k1gFnSc1	koalice
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
třetí	třetí	k4xOgFnSc1	třetí
vláda	vláda	k1gFnSc1	vláda
Angely	Angela	k1gFnSc2	Angela
Merkelové	Merkelová	k1gFnSc2	Merkelová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
Spolkovém	spolkový	k2eAgInSc6d1	spolkový
sněmu	sněm	k1gInSc6	sněm
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
liberální	liberální	k2eAgInSc1d1	liberální
FDP	FDP	kA	FDP
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
získává	získávat	k5eAaImIp3nS	získávat
v	v	k7c6	v
parlamentech	parlament	k1gInPc6	parlament
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
mandáty	mandát	k1gInPc1	mandát
nově	nově	k6eAd1	nově
založená	založený	k2eAgFnSc1d1	založená
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
strana	strana	k1gFnSc1	strana
Alternativa	alternativa	k1gFnSc1	alternativa
pro	pro	k7c4	pro
Německo	Německo	k1gNnSc4	Německo
(	(	kIx(	(
<g/>
AfD	AfD	k1gFnSc1	AfD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
doposud	doposud	k6eAd1	doposud
ve	v	k7c6	v
spolkovém	spolkový	k2eAgInSc6d1	spolkový
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
politické	politický	k2eAgInPc4d1	politický
plány	plán	k1gInPc4	plán
německého	německý	k2eAgNnSc2d1	německé
vedení	vedení	k1gNnSc2	vedení
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
patří	patřit	k5eAaImIp3nS	patřit
tzv.	tzv.	kA	tzv.
energetická	energetický	k2eAgFnSc1d1	energetická
revoluce	revoluce	k1gFnSc1	revoluce
(	(	kIx(	(
<g/>
Energiewende	Energiewend	k1gInSc5	Energiewend
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
udržitelných	udržitelný	k2eAgInPc2d1	udržitelný
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
solární	solární	k2eAgFnSc1d1	solární
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
větrná	větrný	k2eAgFnSc1d1	větrná
energie	energie	k1gFnSc1	energie
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
zdroje	zdroj	k1gInPc1	zdroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
brzda	brzda	k1gFnSc1	brzda
dluhu	dluh	k1gInSc2	dluh
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Schuldenbremse	Schuldenbremse	k1gFnSc1	Schuldenbremse
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zajistit	zajistit	k5eAaPmF	zajistit
vyrovnané	vyrovnaný	k2eAgInPc4d1	vyrovnaný
státní	státní	k2eAgInPc4d1	státní
a	a	k8xC	a
komunální	komunální	k2eAgInPc4d1	komunální
rozpočty	rozpočet	k1gInPc4	rozpočet
<g/>
,	,	kIx,	,
reforma	reforma	k1gFnSc1	reforma
německých	německý	k2eAgInPc2d1	německý
imigračních	imigrační	k2eAgInPc2d1	imigrační
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
právní	právní	k2eAgInPc1d1	právní
předpisy	předpis	k1gInPc1	předpis
pro	pro	k7c4	pro
obecnou	obecný	k2eAgFnSc4d1	obecná
minimální	minimální	k2eAgFnSc4d1	minimální
mzdu	mzda	k1gFnSc4	mzda
a	a	k8xC	a
high-tech	high	k1gMnPc6	high-t
strategie	strategie	k1gFnSc2	strategie
pro	pro	k7c4	pro
informatizaci	informatizace	k1gFnSc4	informatizace
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
modernizaci	modernizace	k1gFnSc4	modernizace
již	již	k9	již
tak	tak	k6eAd1	tak
velmi	velmi	k6eAd1	velmi
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
německé	německý	k2eAgFnPc1d1	německá
ekonomiky	ekonomika	k1gFnPc1	ekonomika
na	na	k7c4	na
model	model	k1gInSc4	model
zvaný	zvaný	k2eAgInSc4d1	zvaný
Industrie	industrie	k1gFnSc1	industrie
4.0	[number]	k4	4.0
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
hlediska	hledisko	k1gNnSc2	hledisko
bylo	být	k5eAaImAgNnS	být
začleňování	začleňování	k1gNnSc4	začleňování
"	"	kIx"	"
<g/>
nových	nový	k2eAgFnPc2d1	nová
zemí	zem	k1gFnPc2	zem
<g/>
"	"	kIx"	"
do	do	k7c2	do
znovusjednoceného	znovusjednocený	k2eAgNnSc2d1	znovusjednocené
Německa	Německo	k1gNnSc2	Německo
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
velmi	velmi	k6eAd1	velmi
nákladné	nákladný	k2eAgNnSc1d1	nákladné
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
krytí	krytí	k1gNnSc4	krytí
těchto	tento	k3xDgInPc2	tento
nákladů	náklad	k1gInPc2	náklad
byly	být	k5eAaImAgFnP	být
zmobilizovány	zmobilizovat	k5eAaPmNgInP	zmobilizovat
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
finanční	finanční	k2eAgInPc1d1	finanční
zdroje	zdroj	k1gInPc1	zdroj
včetně	včetně	k7c2	včetně
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
)	)	kIx)	)
5,5	[number]	k4	5,5
%	%	kIx~	%
tzv.	tzv.	kA	tzv.
solidárního	solidární	k2eAgInSc2d1	solidární
příplatku	příplatek	k1gInSc2	příplatek
(	(	kIx(	(
<g/>
Solidaritätszuschlag	Solidaritätszuschlag	k1gInSc1	Solidaritätszuschlag
<g/>
)	)	kIx)	)
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
daňových	daňový	k2eAgFnPc2d1	daňová
sazeb	sazba	k1gFnPc2	sazba
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
z	z	k7c2	z
kapitálových	kapitálový	k2eAgInPc2d1	kapitálový
výnosů	výnos	k1gInPc2	výnos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
příplatek	příplatek	k1gInSc1	příplatek
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
iniciativ	iniciativa	k1gFnPc2	iniciativa
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
zrušení	zrušení	k1gNnSc3	zrušení
či	či	k8xC	či
alespoň	alespoň	k9	alespoň
snížení	snížení	k1gNnSc4	snížení
vybírán	vybírat	k5eAaImNgMnS	vybírat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
29	[number]	k4	29
let	léto	k1gNnPc2	léto
po	po	k7c6	po
znovusjednocení	znovusjednocení	k1gNnSc6	znovusjednocení
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc4	použití
těchto	tento	k3xDgInPc2	tento
významných	významný	k2eAgInPc2d1	významný
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
není	být	k5eNaImIp3nS	být
transparentní	transparentní	k2eAgNnSc1d1	transparentní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
účelově	účelově	k6eAd1	účelově
vázáno	vázat	k5eAaImNgNnS	vázat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
prostředky	prostředek	k1gInPc4	prostředek
může	moct	k5eAaImIp3nS	moct
spolková	spolkový	k2eAgFnSc1d1	spolková
vláda	vláda	k1gFnSc1	vláda
použít	použít	k5eAaPmF	použít
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
uvážení	uvážení	k1gNnSc2	uvážení
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
značných	značný	k2eAgInPc2d1	značný
vnitropolitických	vnitropolitický	k2eAgInPc2d1	vnitropolitický
a	a	k8xC	a
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
problémů	problém	k1gInPc2	problém
vyvolaných	vyvolaný	k2eAgInPc2d1	vyvolaný
znovusjednocením	znovusjednocení	k1gNnSc7	znovusjednocení
se	se	k3xPyFc4	se
Německo	Německo	k1gNnSc4	Německo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
až	až	k9	až
2015	[number]	k4	2015
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
dočasně	dočasně	k6eAd1	dočasně
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
a	a	k8xC	a
zvětšováním	zvětšování	k1gNnSc7	zvětšování
sociální	sociální	k2eAgFnSc2d1	sociální
propasti	propast	k1gFnSc2	propast
mezi	mezi	k7c7	mezi
příjmovými	příjmový	k2eAgFnPc7d1	příjmová
skupinami	skupina	k1gFnPc7	skupina
<g/>
,	,	kIx,	,
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
porodností	porodnost	k1gFnSc7	porodnost
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývajícím	vyplývající	k2eAgNnSc7d1	vyplývající
stárnutím	stárnutí	k1gNnSc7	stárnutí
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
úbytkem	úbytek	k1gInSc7	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
vlna	vlna	k1gFnSc1	vlna
imigrace	imigrace	k1gFnSc2	imigrace
zvláště	zvláště	k6eAd1	zvláště
z	z	k7c2	z
muslimských	muslimský	k2eAgFnPc2d1	muslimská
zemí	zem	k1gFnPc2	zem
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
přinesla	přinést	k5eAaPmAgFnS	přinést
vážné	vážný	k2eAgInPc4d1	vážný
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
začleňováním	začleňování	k1gNnSc7	začleňování
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
zesílení	zesílení	k1gNnSc1	zesílení
pravicového	pravicový	k2eAgInSc2d1	pravicový
i	i	k8xC	i
levicového	levicový	k2eAgInSc2d1	levicový
extremismu	extremismus	k1gInSc2	extremismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
uznal	uznat	k5eAaPmAgInS	uznat
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
uspořádány	uspořádán	k2eAgFnPc1d1	uspořádána
protiizraelské	protiizraelský	k2eAgFnPc1d1	protiizraelská
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
byla	být	k5eAaImAgFnS	být
propalestinskými	propalestinský	k2eAgInPc7d1	propalestinský
demonstranty	demonstrant	k1gMnPc7	demonstrant
spálena	spálit	k5eAaPmNgFnS	spálit
improvizovaná	improvizovaný	k2eAgFnSc1d1	improvizovaná
izraelská	izraelský	k2eAgFnSc1d1	izraelská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
rada	rada	k1gFnSc1	rada
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
Zentralrat	Zentralrat	k1gMnSc1	Zentralrat
der	drát	k5eAaImRp2nS	drát
Juden	Juden	k1gInSc1	Juden
<g/>
)	)	kIx)	)
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
velké	velký	k2eAgNnSc4d1	velké
znepokojení	znepokojení	k1gNnSc4	znepokojení
nad	nad	k7c7	nad
antisemitskými	antisemitský	k2eAgFnPc7d1	antisemitská
tendencemi	tendence	k1gFnPc7	tendence
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
i	i	k9	i
v	v	k7c6	v
mešitách	mešita	k1gFnPc6	mešita
v	v	k7c6	v
německých	německý	k2eAgNnPc6d1	německé
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
rady	rada	k1gFnSc2	rada
Josef	Josef	k1gMnSc1	Josef
Schuster	Schuster	k1gMnSc1	Schuster
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
reprezentanti	reprezentant	k1gMnPc1	reprezentant
muslimských	muslimský	k2eAgInPc2d1	muslimský
svazů	svaz	k1gInPc2	svaz
sice	sice	k8xC	sice
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
proti	proti	k7c3	proti
antisemitismu	antisemitismus	k1gInSc3	antisemitismus
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
muslimských	muslimský	k2eAgFnPc6d1	muslimská
obcích	obec	k1gFnPc6	obec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
mešitách	mešita	k1gFnPc6	mešita
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
nic	nic	k3yNnSc1	nic
nepodniká	podnikat	k5eNaImIp3nS	podnikat
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
imámové	imám	k1gMnPc1	imám
v	v	k7c6	v
nich	on	k3xPp3gNnPc6	on
rozhlašují	rozhlašovat	k5eAaImIp3nP	rozhlašovat
výhrady	výhrada	k1gFnPc1	výhrada
proti	proti	k7c3	proti
Židům	Žid	k1gMnPc3	Žid
a	a	k8xC	a
Izraeli	Izrael	k1gMnSc3	Izrael
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
geomorfologickými	geomorfologický	k2eAgInPc7d1	geomorfologický
celky	celek	k1gInPc7	celek
Německa	Německo	k1gNnSc2	Německo
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
Severoněmecká	severoněmecký	k2eAgFnSc1d1	Severoněmecká
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
Středoněmecká	středoněmecký	k2eAgFnSc1d1	Středoněmecká
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
alpské	alpský	k2eAgNnSc1d1	alpské
předhůří	předhůří	k1gNnSc1	předhůří
a	a	k8xC	a
velehorské	velehorský	k2eAgNnSc1d1	velehorské
pásmo	pásmo	k1gNnSc1	pásmo
německých	německý	k2eAgFnPc2d1	německá
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
devíti	devět	k4xCc7	devět
státy	stát	k1gInPc7	stát
<g/>
:	:	kIx,	:
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
a	a	k8xC	a
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
nejvíce	hodně	k6eAd3	hodně
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nS	sousedit
Německo	Německo	k1gNnSc1	Německo
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
hranic	hranice	k1gFnPc2	hranice
67	[number]	k4	67
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
(	(	kIx(	(
<g/>
442	[number]	k4	442
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
(	(	kIx(	(
<g/>
811	[number]	k4	811
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
(	(	kIx(	(
<g/>
815	[number]	k4	815
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
;	;	kIx,	;
bez	bez	k7c2	bez
<g />
.	.	kIx.	.
</s>
<s>
hranice	hranice	k1gFnSc1	hranice
v	v	k7c6	v
Bodamském	bodamský	k2eAgNnSc6d1	Bodamské
jezeře	jezero	k1gNnSc6	jezero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
(	(	kIx(	(
<g/>
316	[number]	k4	316
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
;	;	kIx,	;
s	s	k7c7	s
hranicí	hranice	k1gFnSc7	hranice
exklávy	exkláva	k1gFnSc2	exkláva
Büsingen	Büsingen	k1gInSc1	Büsingen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
hranice	hranice	k1gFnSc2	hranice
v	v	k7c6	v
Bodamském	bodamský	k2eAgNnSc6d1	Bodamské
jezeře	jezero	k1gNnSc6	jezero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
(	(	kIx(	(
<g/>
448	[number]	k4	448
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Lucemburskem	Lucembursko	k1gNnSc7	Lucembursko
(	(	kIx(	(
<g/>
135	[number]	k4	135
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
Belgií	Belgie	k1gFnSc7	Belgie
(	(	kIx(	(
<g/>
156	[number]	k4	156
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
s	s	k7c7	s
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
(	(	kIx(	(
<g/>
567	[number]	k4	567
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
hranic	hranice	k1gFnPc2	hranice
činí	činit	k5eAaImIp3nS	činit
celkem	celkem	k6eAd1	celkem
3	[number]	k4	3
757	[number]	k4	757
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
v	v	k7c6	v
Bodamském	bodamský	k2eAgNnSc6d1	Bodamské
jezeře	jezero	k1gNnSc6	jezero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
často	často	k6eAd1	často
měnil	měnit	k5eAaImAgInS	měnit
i	i	k9	i
střed	střed	k1gInSc1	střed
německého	německý	k2eAgInSc2d1	německý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
eurasijské	eurasijský	k2eAgFnSc6d1	eurasijská
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
desce	deska	k1gFnSc6	deska
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
slabá	slabý	k2eAgNnPc1d1	slabé
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Porúří	Porúří	k1gNnSc6	Porúří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Geomorfologie	geomorfologie	k1gFnSc2	geomorfologie
===	===	k?	===
</s>
</p>
<p>
<s>
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
třetihorním	třetihorní	k2eAgNnSc7d1	třetihorní
zvrásněním	zvrásnění	k1gNnSc7	zvrásnění
kontinentálních	kontinentální	k2eAgFnPc2d1	kontinentální
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jedinými	jediný	k2eAgFnPc7d1	jediná
německými	německý	k2eAgFnPc7d1	německá
velehorami	velehora	k1gFnPc7	velehora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
leží	ležet	k5eAaImIp3nS	ležet
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Německa	Německo	k1gNnSc2	Německo
–	–	k?	–
Zugspitze	Zugspitze	k1gFnSc1	Zugspitze
o	o	k7c6	o
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
2962	[number]	k4	2962
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středoněmecká	středoněmecký	k2eAgFnSc1d1	Středoněmecká
vysočina	vysočina	k1gFnSc1	vysočina
nabývá	nabývat	k5eAaImIp3nS	nabývat
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
na	na	k7c6	na
výšce	výška	k1gFnSc6	výška
a	a	k8xC	a
rozloze	rozloha	k1gFnSc6	rozloha
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Schwarzwaldu	Schwarzwald	k1gInSc6	Schwarzwald
–	–	k?	–
1493	[number]	k4	1493
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc4d1	vysoký
Feldberg	Feldberg	k1gInSc4	Feldberg
<g/>
,	,	kIx,	,
následován	následován	k2eAgInSc4d1	následován
1456	[number]	k4	1456
metrů	metr	k1gInPc2	metr
vysokým	vysoký	k2eAgInPc3d1	vysoký
Großer	Großer	k1gInSc1	Großer
Arber	Arber	k1gInSc1	Arber
v	v	k7c6	v
Bavorském	bavorský	k2eAgInSc6d1	bavorský
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholy	vrchol	k1gInPc1	vrchol
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
mají	mít	k5eAaImIp3nP	mít
dále	daleko	k6eAd2	daleko
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Smrčiny	Smrčiny	k1gFnPc1	Smrčiny
<g/>
,	,	kIx,	,
Švábská	švábský	k2eAgFnSc1d1	Švábská
Alba	alba	k1gFnSc1	alba
a	a	k8xC	a
pohoří	pohoří	k1gNnSc1	pohoří
Harz	Harza	k1gFnPc2	Harza
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
izolovanou	izolovaný	k2eAgFnSc7d1	izolovaná
částí	část	k1gFnSc7	část
Středoněmecké	středoněmecký	k2eAgFnSc2d1	Středoněmecká
vysočiny	vysočina	k1gFnSc2	vysočina
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Brocken	Brockna	k1gFnPc2	Brockna
o	o	k7c6	o
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1142	[number]	k4	1142
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
kopce	kopec	k1gInPc4	kopec
vyšší	vysoký	k2eAgInPc4d2	vyšší
než	než	k8xS	než
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
je	být	k5eAaImIp3nS	být
Hagelberg	Hagelberg	k1gInSc1	Hagelberg
v	v	k7c6	v
geomorfologické	geomorfologický	k2eAgFnSc6d1	geomorfologická
části	část	k1gFnSc6	část
Flaming	Flaming	k1gInSc1	Flaming
v	v	k7c6	v
Braniborsku	Braniborsko	k1gNnSc6	Braniborsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
Německa	Německo	k1gNnSc2	Německo
je	být	k5eAaImIp3nS	být
proláklina	proláklina	k1gFnSc1	proláklina
3,54	[number]	k4	3,54
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
u	u	k7c2	u
Neuendorf-Sachsebbande	Neuendorf-Sachsebband	k1gInSc5	Neuendorf-Sachsebband
v	v	k7c6	v
Šlesvicku-Holštýnsku	Šlesvicku-Holštýnsko	k1gNnSc3	Šlesvicku-Holštýnsko
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
Šlesvicku-Holštýnsku	Šlesvicku-Holštýnsko	k1gNnSc6	Šlesvicku-Holštýnsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejhlubší	hluboký	k2eAgInSc1d3	nejhlubší
zatopený	zatopený	k2eAgInSc1d1	zatopený
bod	bod	k1gInSc1	bod
Německa	Německo	k1gNnSc2	Německo
<g/>
:	:	kIx,	:
39,10	[number]	k4	39,10
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
Hemmelsdorfer	Hemmelsdorfero	k1gNnPc2	Hemmelsdorfero
See	See	k1gFnSc2	See
severoseverovýchodně	severoseverovýchodně	k6eAd1	severoseverovýchodně
od	od	k7c2	od
Lübecku	Lübeck	k1gInSc2	Lübeck
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
umělý	umělý	k2eAgInSc1d1	umělý
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
bod	bod	k1gInSc1	bod
Německa	Německo	k1gNnSc2	Německo
je	být	k5eAaImIp3nS	být
293	[number]	k4	293
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
hnědouhelného	hnědouhelný	k2eAgInSc2d1	hnědouhelný
dolu	dol	k1gInSc2	dol
Hambach	Hambacha	k1gFnPc2	Hambacha
východně	východně	k6eAd1	východně
od	od	k7c2	od
Jülichu	Jülich	k1gInSc2	Jülich
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Porýní-Vestfálsku	Porýní-Vestfálsek	k1gInSc6	Porýní-Vestfálsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vodstvo	vodstvo	k1gNnSc1	vodstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Území	území	k1gNnSc4	území
Německa	Německo	k1gNnSc2	Německo
odvodňují	odvodňovat	k5eAaImIp3nP	odvodňovat
z	z	k7c2	z
největší	veliký	k2eAgFnSc2d3	veliký
části	část	k1gFnSc2	část
řeky	řeka	k1gFnSc2	řeka
Rýn	Rýn	k1gInSc1	Rýn
<g/>
,	,	kIx,	,
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
Labe	Labe	k1gNnSc1	Labe
<g/>
,	,	kIx,	,
Odra	Odra	k1gFnSc1	Odra
<g/>
,	,	kIx,	,
Vezera	Vezera	k1gFnSc1	Vezera
a	a	k8xC	a
Emže	Emže	k1gFnSc1	Emže
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgNnSc1d1	německé
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
odvodňováno	odvodňovat	k5eAaImNgNnS	odvodňovat
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
Černého	černé	k1gNnSc2	černé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
německé	německý	k2eAgNnSc4d1	německé
území	území	k1gNnSc4	území
vede	vést	k5eAaImIp3nS	vést
hlavní	hlavní	k2eAgNnSc4d1	hlavní
evropské	evropský	k2eAgNnSc4d1	Evropské
rozvodí	rozvodí	k1gNnSc4	rozvodí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
německou	německý	k2eAgFnSc7d1	německá
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Rýn	Rýn	k1gInSc1	Rýn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
délku	délka	k1gFnSc4	délka
865	[number]	k4	865
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Rýn	Rýn	k1gInSc1	Rýn
dominuje	dominovat	k5eAaImIp3nS	dominovat
jihozápadu	jihozápad	k1gInSc3	jihozápad
a	a	k8xC	a
západu	západ	k1gInSc3	západ
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgInPc7d3	nejdůležitější
přítoky	přítok	k1gInPc7	přítok
jsou	být	k5eAaImIp3nP	být
Neckar	Neckar	k1gMnSc1	Neckar
<g/>
,	,	kIx,	,
Mohan	Mohan	k1gInSc1	Mohan
<g/>
,	,	kIx,	,
Mosela	Mosela	k1gFnSc1	Mosela
a	a	k8xC	a
Rúr	Rúr	k1gFnSc1	Rúr
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
význam	význam	k1gInSc1	význam
Rýna	Rýn	k1gInSc2	Rýn
je	být	k5eAaImIp3nS	být
značný	značný	k2eAgInSc1d1	značný
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
řek	řeka	k1gFnPc2	řeka
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
Dunaj	Dunaj	k1gInSc1	Dunaj
délku	délka	k1gFnSc4	délka
647	[number]	k4	647
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
skoro	skoro	k6eAd1	skoro
celé	celý	k2eAgNnSc4d1	celé
předhůří	předhůří	k1gNnSc4	předhůří
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
teče	téct	k5eAaImIp3nS	téct
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc7	jeho
nejdůležitějšími	důležitý	k2eAgInPc7d3	nejdůležitější
přítoky	přítok	k1gInPc7	přítok
jsou	být	k5eAaImIp3nP	být
Iller	Iller	k1gMnSc1	Iller
<g/>
,	,	kIx,	,
Lech	Lech	k1gMnSc1	Lech
<g/>
,	,	kIx,	,
Isar	Isar	k1gMnSc1	Isar
a	a	k8xC	a
Inn	Inn	k1gMnSc1	Inn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
Německa	Německo	k1gNnSc2	Německo
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
Labe	Labe	k1gNnSc2	Labe
délku	délka	k1gFnSc4	délka
725	[number]	k4	725
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc7	jeho
nejdůležitějšími	důležitý	k2eAgInPc7d3	nejdůležitější
přítoky	přítok	k1gInPc7	přítok
jsou	být	k5eAaImIp3nP	být
Sála	Sála	k1gFnSc1	Sála
a	a	k8xC	a
Havola	Havola	k1gFnSc1	Havola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hranici	hranice	k1gFnSc4	hranice
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
Odra	Odra	k1gFnSc1	Odra
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
přítokem	přítok	k1gInSc7	přítok
je	být	k5eAaImIp3nS	být
Lužická	lužický	k2eAgFnSc1d1	Lužická
Nisa	Nisa	k1gFnSc1	Nisa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celé	celý	k2eAgNnSc1d1	celé
povodí	povodí	k1gNnSc1	povodí
Vezery	Vezera	k1gFnSc2	Vezera
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
soutokem	soutok	k1gInSc7	soutok
Werry	Werra	k1gFnSc2	Werra
a	a	k8xC	a
Fuldy	Fulda	k1gFnSc2	Fulda
a	a	k8xC	a
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Emže	Emže	k1gFnSc1	Emže
(	(	kIx(	(
<g/>
Ems	Ems	k1gFnSc1	Ems
<g/>
)	)	kIx)	)
teče	téct	k5eAaImIp3nS	téct
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přirozená	přirozený	k2eAgNnPc1d1	přirozené
jezera	jezero	k1gNnPc1	jezero
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
německá	německý	k2eAgFnSc1d1	německá
jezera	jezero	k1gNnSc2	jezero
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
předhůří	předhůří	k1gNnSc6	předhůří
Alp	Alpy	k1gFnPc2	Alpy
a	a	k8xC	a
Meklenburské	meklenburský	k2eAgFnSc6d1	Meklenburská
jezerní	jezerní	k2eAgFnSc6d1	jezerní
plošině	plošina	k1gFnSc6	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
jezerem	jezero	k1gNnSc7	jezero
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
celé	celý	k2eAgInPc1d1	celý
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Müritz	Müritz	k1gInSc1	Müritz
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
Meklenburské	meklenburský	k2eAgFnSc6d1	Meklenburská
jezerní	jezerní	k2eAgFnSc6d1	jezerní
plošině	plošina	k1gFnSc6	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
Bodamské	bodamský	k2eAgNnSc1d1	Bodamské
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
Německo	Německo	k1gNnSc1	Německo
dělí	dělit	k5eAaImIp3nS	dělit
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
zatopených	zatopený	k2eAgInPc6d1	zatopený
bývalých	bývalý	k2eAgInPc6d1	bývalý
hnědouhelných	hnědouhelný	k2eAgInPc6d1	hnědouhelný
dolech	dol	k1gInPc6	dol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostrovy	ostrov	k1gInPc1	ostrov
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
souostroví	souostroví	k1gNnSc2	souostroví
Severofríské	Severofríská	k1gFnSc2	Severofríská
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
Východofríské	Východofríský	k2eAgInPc4d1	Východofríský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
Helgoland	Helgoland	k1gInSc1	Helgoland
a	a	k8xC	a
Neuwerk	Neuwerk	k1gInSc1	Neuwerk
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
obydlené	obydlený	k2eAgFnPc1d1	obydlená
<g/>
.	.	kIx.	.
</s>
<s>
Severofríské	Severofríský	k2eAgInPc1d1	Severofríský
ostrovy	ostrov	k1gInPc1	ostrov
představují	představovat	k5eAaImIp3nP	představovat
zbytky	zbytek	k1gInPc7	zbytek
zatopené	zatopený	k2eAgFnSc2d1	zatopená
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
po	po	k7c6	po
snížení	snížení	k1gNnSc6	snížení
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
zaplavení	zaplavení	k1gNnSc6	zaplavení
<g/>
.	.	kIx.	.
</s>
<s>
Východofríské	Východofríský	k2eAgInPc1d1	Východofríský
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
bariérové	bariérový	k2eAgInPc1d1	bariérový
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
příbojem	příboj	k1gInSc7	příboj
a	a	k8xC	a
pohybem	pohyb	k1gInSc7	pohyb
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
písčin	písčina	k1gFnPc2	písčina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
německé	německý	k2eAgInPc1d1	německý
ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
)	)	kIx)	)
Fehmarn	Fehmarn	k1gMnSc1	Fehmarn
<g/>
,	,	kIx,	,
Poel	Poel	k1gMnSc1	Poel
<g/>
,	,	kIx,	,
Hiddensee	Hiddensee	k1gFnSc1	Hiddensee
<g/>
,	,	kIx,	,
Rujána	Rujána	k1gFnSc1	Rujána
a	a	k8xC	a
Usedom	Usedom	k1gInSc4	Usedom
<g/>
;	;	kIx,	;
největším	veliký	k2eAgInSc7d3	veliký
poloostrovem	poloostrov	k1gInSc7	poloostrov
je	být	k5eAaImIp3nS	být
Fischland-Darß-Zingst	Fischland-Darß-Zingst	k1gFnSc1	Fischland-Darß-Zingst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
německé	německý	k2eAgInPc1d1	německý
vnitrozemské	vnitrozemský	k2eAgInPc1d1	vnitrozemský
ostrovy	ostrov	k1gInPc1	ostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Bodamském	bodamský	k2eAgNnSc6d1	Bodamské
jezeře	jezero	k1gNnSc6	jezero
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ostrovy	ostrov	k1gInPc1	ostrov
Reichenau	Reichenaus	k1gInSc2	Reichenaus
<g/>
,	,	kIx,	,
Mainau	Mainaus	k1gInSc2	Mainaus
a	a	k8xC	a
Lindau	Lindaus	k1gInSc2	Lindaus
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ostrov	ostrov	k1gInSc1	ostrov
Herrenchiemsee	Herrenchiemse	k1gFnSc2	Herrenchiemse
v	v	k7c6	v
Chiemsee	Chiemsee	k1gFnSc6	Chiemsee
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
===	===	k?	===
</s>
</p>
<p>
<s>
Celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
Německa	Německo	k1gNnSc2	Německo
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
mírnému	mírný	k2eAgNnSc3d1	mírné
klimatickému	klimatický	k2eAgNnSc3d1	klimatické
pásmu	pásmo	k1gNnSc3	pásmo
Střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
převládajícím	převládající	k2eAgNnSc6d1	převládající
západním	západní	k2eAgNnSc6d1	západní
prouděním	proudění	k1gNnSc7	proudění
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
přechodném	přechodný	k2eAgNnSc6d1	přechodné
pásmu	pásmo	k1gNnSc6	pásmo
mezi	mezi	k7c7	mezi
oceánským	oceánský	k2eAgNnSc7d1	oceánské
a	a	k8xC	a
kontinentálním	kontinentální	k2eAgNnSc7d1	kontinentální
klimatem	klima	k1gNnSc7	klima
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
ovlivňováno	ovlivňovat	k5eAaImNgNnS	ovlivňovat
Golfským	golfský	k2eAgInSc7d1	golfský
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
celoročně	celoročně	k6eAd1	celoročně
převažují	převažovat	k5eAaImIp3nP	převažovat
příznivé	příznivý	k2eAgFnPc4d1	příznivá
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střední	střední	k2eAgInSc1d1	střední
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
700	[number]	k4	700
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgNnSc1d1	střední
měsíční	měsíční	k2eAgNnSc1d1	měsíční
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
40	[number]	k4	40
milimetry	milimetr	k1gInPc7	milimetr
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
a	a	k8xC	a
77	[number]	k4	77
milimetry	milimetr	k1gInPc7	milimetr
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
-45,9	-45,9	k4	-45,9
°	°	k?	°
<g/>
C	C	kA	C
<g/>
;	;	kIx,	;
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2001	[number]	k4	2001
v	v	k7c6	v
Funtensee	Funtensee	k1gFnSc6	Funtensee
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
registrována	registrován	k2eAgFnSc1d1	registrována
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
v	v	k7c6	v
Nennigu	Nennig	k1gInSc6	Nennig
v	v	k7c6	v
Sársku	Sársko	k1gNnSc6	Sársko
a	a	k8xC	a
činila	činit	k5eAaImAgFnS	činit
+40,3	+40,3	k4	+40,3
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnSc2	geologie
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
prvohor	prvohory	k1gFnPc2	prvohory
pochází	pocházet	k5eAaImIp3nS	pocházet
na	na	k7c4	na
území	území	k1gNnSc4	území
Německa	Německo	k1gNnSc2	Německo
krystalické	krystalický	k2eAgFnSc2d1	krystalická
horniny	hornina	k1gFnSc2	hornina
jako	jako	k8xS	jako
rula	rula	k1gFnSc1	rula
a	a	k8xC	a
žula	žula	k1gFnSc1	žula
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Středoněmecké	středoněmecký	k2eAgFnSc6d1	Středoněmecká
vysočině	vysočina	k1gFnSc6	vysočina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Harz	Harza	k1gFnPc2	Harza
<g/>
.	.	kIx.	.
</s>
<s>
Sedimenty	sediment	k1gInPc1	sediment
z	z	k7c2	z
Rheinisches	Rheinischesa	k1gFnPc2	Rheinischesa
Schiefergebirge	Schiefergebirg	k1gFnSc2	Schiefergebirg
(	(	kIx(	(
<g/>
Porýnské	porýnský	k2eAgFnSc2d1	porýnská
břidličné	břidličný	k2eAgFnSc2d1	Břidličná
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
)	)	kIx)	)
taktéž	taktéž	k?	taktéž
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
prvohor	prvohory	k1gFnPc2	prvohory
<g/>
,	,	kIx,	,
z	z	k7c2	z
devonu	devon	k1gInSc2	devon
a	a	k8xC	a
spodního	spodní	k2eAgInSc2d1	spodní
karbonu	karbon	k1gInSc2	karbon
<g/>
.	.	kIx.	.
</s>
<s>
Vyzvednutí	vyzvednutí	k1gNnSc1	vyzvednutí
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc1	vytvoření
pohoří	pohoří	k1gNnSc2	pohoří
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
pliocénu	pliocén	k1gInSc6	pliocén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
v	v	k7c6	v
Porýnské	porýnský	k2eAgFnSc6d1	porýnská
břidličné	břidličný	k2eAgFnSc6d1	Břidličná
vrchovině	vrchovina	k1gFnSc6	vrchovina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
karbonská	karbonský	k2eAgNnPc4d1	karbonské
souvrství	souvrství	k1gNnPc4	souvrství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
veliká	veliký	k2eAgNnPc1d1	veliké
ložiska	ložisko	k1gNnPc1	ložisko
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
v	v	k7c6	v
Porúří	Porúří	k1gNnSc6	Porúří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mesozoiku	mesozoikum	k1gNnSc6	mesozoikum
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhohorách	druhohory	k1gFnPc6	druhohory
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
horniny	hornina	k1gFnPc1	hornina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
převládají	převládat	k5eAaImIp3nP	převládat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Falci	Falc	k1gFnSc6	Falc
<g/>
,	,	kIx,	,
v	v	k7c6	v
Durynsku	Durynsko	k1gNnSc6	Durynsko
<g/>
,	,	kIx,	,
části	část	k1gFnSc6	část
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
a	a	k8xC	a
Saska	Sasko	k1gNnSc2	Sasko
pochází	pocházet	k5eAaImIp3nS	pocházet
podloží	podloží	k1gNnSc4	podloží
hornin	hornina	k1gFnPc2	hornina
z	z	k7c2	z
triasu	trias	k1gInSc2	trias
<g/>
,	,	kIx,	,
časného	časný	k2eAgNnSc2d1	časné
mesozoika	mesozoikum	k1gNnSc2	mesozoikum
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
Jura	jura	k1gFnSc1	jura
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
Švábská	švábský	k2eAgFnSc1d1	Švábská
a	a	k8xC	a
Franská	franský	k2eAgFnSc1d1	Franská
Alba	alba	k1gFnSc1	alba
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
jury	jura	k1gFnSc2	jura
<g/>
.	.	kIx.	.
</s>
<s>
Z	Z	kA	Z
trias	trias	k1gInSc1	trias
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yIgInSc2	který
pochází	pocházet	k5eAaImIp3nS	pocházet
většina	většina	k1gFnSc1	většina
pískovců	pískovec	k1gInPc2	pískovec
<g/>
,	,	kIx,	,
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Jury	jura	k1gFnSc2	jura
ale	ale	k8xC	ale
převažuje	převažovat	k5eAaImIp3nS	převažovat
vápenec	vápenec	k1gInSc4	vápenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
třetihorách	třetihory	k1gFnPc6	třetihory
následovala	následovat	k5eAaImAgFnS	následovat
eroze	eroze	k1gFnSc1	eroze
<g/>
,	,	kIx,	,
zarovnání	zarovnání	k1gNnSc1	zarovnání
terénu	terén	k1gInSc2	terén
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
nížin	nížina	k1gFnPc2	nížina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aktivní	aktivní	k2eAgInSc1d1	aktivní
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
pozorován	pozorován	k2eAgMnSc1d1	pozorován
<g/>
,	,	kIx,	,
vulkanické	vulkanický	k2eAgFnPc1d1	vulkanická
horniny	hornina	k1gFnPc1	hornina
ale	ale	k9	ale
dosvědčují	dosvědčovat	k5eAaImIp3nP	dosvědčovat
dřívější	dřívější	k2eAgFnSc4d1	dřívější
vulkanickou	vulkanický	k2eAgFnSc4d1	vulkanická
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Eifel	Eifela	k1gFnPc2	Eifela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Eifel	Eifela	k1gFnPc2	Eifela
jsou	být	k5eAaImIp3nP	být
prameny	pramen	k1gInPc1	pramen
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
kysličníku	kysličník	k1gInSc2	kysličník
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
gejzíry	gejzír	k1gInPc7	gejzír
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
eurasijské	eurasijský	k2eAgFnSc6d1	eurasijská
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
desce	deska	k1gFnSc6	deska
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
slabá	slabý	k2eAgNnPc1d1	slabé
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Porúří	Porúří	k1gNnSc6	Porúří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Půdy	půda	k1gFnSc2	půda
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
převládají	převládat	k5eAaImIp3nP	převládat
drnopodzolové	drnopodzol	k1gMnPc1	drnopodzol
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
lesní	lesní	k2eAgFnSc2d1	lesní
hnědozemě	hnědozem	k1gFnSc2	hnědozem
<g/>
.	.	kIx.	.
</s>
<s>
Drnopodzolové	Drnopodzolový	k2eAgFnPc1d1	Drnopodzolový
půdy	půda	k1gFnPc1	půda
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
písčitých	písčitý	k2eAgFnPc6d1	písčitá
a	a	k8xC	a
štěrkopísčitých	štěrkopísčitý	k2eAgFnPc6d1	štěrkopísčitý
ledovcových	ledovcový	k2eAgFnPc6d1	ledovcová
usazeninách	usazenina	k1gFnPc6	usazenina
v	v	k7c6	v
Severoněmecké	severoněmecký	k2eAgFnSc6d1	Severoněmecká
nížině	nížina	k1gFnSc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Lesní	lesní	k2eAgFnPc1d1	lesní
hnědozemě	hnědozem	k1gFnPc1	hnědozem
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
Středoněmecké	středoněmecký	k2eAgFnSc6d1	Středoněmecká
vysočině	vysočina	k1gFnSc6	vysočina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příroda	příroda	k1gFnSc1	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Flóra	Flóra	k1gFnSc1	Flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
klimatickém	klimatický	k2eAgNnSc6d1	klimatické
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
jeho	on	k3xPp3gNnSc2	on
území	území	k1gNnSc2	území
původně	původně	k6eAd1	původně
pokrývaly	pokrývat	k5eAaImAgInP	pokrývat
listnaté	listnatý	k2eAgInPc1d1	listnatý
a	a	k8xC	a
jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
vykáceny	vykácen	k2eAgFnPc1d1	vykácena
a	a	k8xC	a
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
uchována	uchován	k2eAgFnSc1d1	uchována
původní	původní	k2eAgFnSc1d1	původní
skladba	skladba	k1gFnSc1	skladba
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Flóra	Flóra	k1gFnSc1	Flóra
na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
geologického	geologický	k2eAgNnSc2d1	geologické
podloží	podloží	k1gNnSc2	podloží
a	a	k8xC	a
klimatických	klimatický	k2eAgFnPc2d1	klimatická
podmínek	podmínka	k1gFnPc2	podmínka
na	na	k7c6	na
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
vegetaci	vegetace	k1gFnSc4	vegetace
zejména	zejména	k9	zejména
přechod	přechod	k1gInSc4	přechod
od	od	k7c2	od
oceánského	oceánský	k2eAgNnSc2d1	oceánské
ke	k	k7c3	k
kontinentálnímu	kontinentální	k2eAgNnSc3d1	kontinentální
podnebí	podnebí	k1gNnSc3	podnebí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
buk	buk	k1gInSc1	buk
lesní	lesní	k2eAgFnSc2d1	lesní
tvořící	tvořící	k2eAgFnSc2d1	tvořící
bučiny	bučina	k1gFnSc2	bučina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
jezer	jezero	k1gNnPc2	jezero
stále	stále	k6eAd1	stále
nachází	nacházet	k5eAaImIp3nS	nacházet
řídké	řídký	k2eAgInPc4d1	řídký
nivní	nivní	k2eAgInPc4d1	nivní
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
dominuje	dominovat	k5eAaImIp3nS	dominovat
buk	buk	k1gInSc1	buk
a	a	k8xC	a
dub	dub	k1gInSc1	dub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
a	a	k8xC	a
německých	německý	k2eAgFnPc6d1	německá
středohorách	středohory	k1gFnPc6	středohory
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
lesy	les	k1gInPc1	les
i	i	k9	i
na	na	k7c6	na
pozemcích	pozemek	k1gInPc6	pozemek
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
velkým	velký	k2eAgInSc7d1	velký
sklonem	sklon	k1gInSc7	sklon
<g/>
.	.	kIx.	.
</s>
<s>
Pionýrskými	pionýrský	k2eAgFnPc7d1	Pionýrská
dřevinami	dřevina	k1gFnPc7	dřevina
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
bříza	bříza	k1gFnSc1	bříza
a	a	k8xC	a
borovice	borovice	k1gFnSc1	borovice
<g/>
.	.	kIx.	.
</s>
<s>
Jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
lesy	les	k1gInPc1	les
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
dříve	dříve	k6eAd2	dříve
velmi	velmi	k6eAd1	velmi
rozšířenými	rozšířený	k2eAgInPc7d1	rozšířený
listnatými	listnatý	k2eAgInPc7d1	listnatý
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
lidského	lidský	k2eAgInSc2d1	lidský
vlivu	vliv	k1gInSc2	vliv
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
potenciální	potenciální	k2eAgFnSc2d1	potenciální
vegetace	vegetace	k1gFnSc2	vegetace
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
klimatickém	klimatický	k2eAgNnSc6d1	klimatické
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
,	,	kIx,	,
skládala	skládat	k5eAaImAgFnS	skládat
především	především	k6eAd1	především
z	z	k7c2	z
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
vřesoviště	vřesoviště	k1gNnPc1	vřesoviště
a	a	k8xC	a
bažiny	bažina	k1gFnPc1	bažina
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
alpínská	alpínský	k2eAgNnPc4d1	alpínské
a	a	k8xC	a
subalpínská	subalpínský	k2eAgNnPc4d1	subalpínské
společenství	společenství	k1gNnPc4	společenství
z	z	k7c2	z
Bavorských	bavorský	k2eAgFnPc6d1	bavorská
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Schwarzwaldu	Schwarzwaldo	k1gNnSc6	Schwarzwaldo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Alpínská	alpínský	k2eAgNnPc1d1	alpínské
a	a	k8xC	a
subalpínská	subalpínský	k2eAgNnPc1d1	subalpínské
stanoviště	stanoviště	k1gNnPc1	stanoviště
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
chudé	chudý	k2eAgFnPc1d1	chudá
půdy	půda	k1gFnPc1	půda
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnPc1d2	nižší
teploty	teplota	k1gFnPc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
než	než	k8xS	než
dříve	dříve	k6eAd2	dříve
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
různé	různý	k2eAgFnPc1d1	různá
zavlečené	zavlečený	k2eAgFnPc1d1	zavlečená
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
akát	akát	k1gInSc1	akát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
zalesněno	zalesněn	k2eAgNnSc4d1	zalesněno
29,5	[number]	k4	29,5
%	%	kIx~	%
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
má	mít	k5eAaImIp3nS	mít
Německo	Německo	k1gNnSc4	Německo
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
ploch	plocha	k1gFnPc2	plocha
lesa	les	k1gInSc2	les
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
obdělané	obdělaný	k2eAgFnSc2d1	obdělaná
plochy	plocha	k1gFnSc2	plocha
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
pěstování	pěstování	k1gNnSc3	pěstování
obilí	obilí	k1gNnSc2	obilí
(	(	kIx(	(
<g/>
ječmene	ječmen	k1gInSc2	ječmen
<g/>
,	,	kIx,	,
ovsa	oves	k1gInSc2	oves
<g/>
,	,	kIx,	,
žita	žito	k1gNnSc2	žito
a	a	k8xC	a
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
brambory	brambora	k1gFnPc1	brambora
a	a	k8xC	a
kukuřice	kukuřice	k1gFnPc1	kukuřice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
plodiny	plodina	k1gFnPc4	plodina
dovezené	dovezený	k2eAgFnPc4d1	dovezená
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
plodin	plodina	k1gFnPc2	plodina
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
řepka	řepka	k1gFnSc1	řepka
olejná	olejný	k2eAgFnSc1d1	olejná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ovocných	ovocný	k2eAgInPc2d1	ovocný
stromů	strom	k1gInPc2	strom
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jabloně	jabloň	k1gFnPc1	jabloň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Mosely	Mosela	k1gFnSc2	Mosela
<g/>
,	,	kIx,	,
Ahru	Ahrus	k1gInSc2	Ahrus
a	a	k8xC	a
Rýna	Rýn	k1gInSc2	Rýn
jsou	být	k5eAaImIp3nP	být
provozovány	provozován	k2eAgFnPc4d1	provozována
vinice	vinice	k1gFnPc4	vinice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInPc1d1	původní
druhy	druh	k1gInPc1	druh
savců	savec	k1gMnPc2	savec
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
žijí	žít	k5eAaImIp3nP	žít
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lese	les	k1gInSc6	les
žijí	žít	k5eAaImIp3nP	žít
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
lasic	lasice	k1gFnPc2	lasice
<g/>
,	,	kIx,	,
daněk	daněk	k1gMnSc1	daněk
evropský	evropský	k2eAgMnSc1d1	evropský
<g/>
,	,	kIx,	,
jelen	jelen	k1gMnSc1	jelen
evropský	evropský	k2eAgMnSc1d1	evropský
<g/>
,	,	kIx,	,
srnec	srnec	k1gMnSc1	srnec
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
a	a	k8xC	a
liška	liška	k1gFnSc1	liška
obecná	obecná	k1gFnSc1	obecná
<g/>
.	.	kIx.	.
</s>
<s>
Bobr	bobr	k1gMnSc1	bobr
a	a	k8xC	a
vydra	vydra	k1gFnSc1	vydra
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
ale	ale	k8xC	ale
stoupají	stoupat	k5eAaImIp3nP	stoupat
jejich	jejich	k3xOp3gInPc4	jejich
stavy	stav	k1gInPc4	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
velcí	velký	k2eAgMnPc1d1	velký
savci	savec	k1gMnPc1	savec
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
vyhubeni	vyhubit	k5eAaPmNgMnP	vyhubit
<g/>
:	:	kIx,	:
pratur	pratur	k1gMnSc1	pratur
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgInSc1d1	hnědý
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
los	los	k1gInSc1	los
evropský	evropský	k2eAgInSc1d1	evropský
byl	být	k5eAaImAgInS	být
hojný	hojný	k2eAgInSc1d1	hojný
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
divoký	divoký	k2eAgMnSc1d1	divoký
kůň	kůň	k1gMnSc1	kůň
byl	být	k5eAaImAgMnS	být
vyhuben	vyhubit	k5eAaPmNgMnS	vyhubit
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zubr	zubr	k1gMnSc1	zubr
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
vlk	vlk	k1gMnSc1	vlk
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
vrací	vracet	k5eAaImIp3nP	vracet
vlci	vlk	k1gMnPc1	vlk
a	a	k8xC	a
losi	los	k1gMnPc1	los
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
kde	kde	k6eAd1	kde
stoupají	stoupat	k5eAaImIp3nP	stoupat
jejich	jejich	k3xOp3gInPc4	jejich
stavy	stav	k1gInPc4	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Populace	populace	k1gFnSc1	populace
orla	orel	k1gMnSc2	orel
mořského	mořský	k2eAgMnSc2d1	mořský
<g/>
,	,	kIx,	,
ptáka	pták	k1gMnSc2	pták
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
čítá	čítat	k5eAaImIp3nS	čítat
asi	asi	k9	asi
500	[number]	k4	500
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Braniborsku	Braniborsko	k1gNnSc6	Braniborsko
a	a	k8xC	a
Meklenbursku-Předním	Meklenbursku-Přední	k2eAgNnSc6d1	Meklenbursku-Přední
Pomořansku	Pomořansko	k1gNnSc6	Pomořansko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dravých	dravý	k2eAgMnPc2d1	dravý
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nejvíc	hodně	k6eAd3	hodně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
káně	káně	k1gFnSc1	káně
lesní	lesní	k2eAgFnSc1d1	lesní
a	a	k8xC	a
poštolka	poštolka	k1gFnSc1	poštolka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
sokol	sokol	k1gMnSc1	sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vzácnější	vzácný	k2eAgFnSc1d2	vzácnější
<g/>
.	.	kIx.	.
50	[number]	k4	50
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
luňáka	luňák	k1gMnSc2	luňák
červeného	červené	k1gNnSc2	červené
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
právě	právě	k9	právě
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
intenzifikaci	intenzifikace	k1gFnSc3	intenzifikace
zemědělství	zemědělství	k1gNnSc2	zemědělství
klesají	klesat	k5eAaImIp3nP	klesat
jeho	jeho	k3xOp3gInPc1	jeho
stavy	stav	k1gInPc1	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
žije	žít	k5eAaImIp3nS	žít
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
profitují	profitovat	k5eAaBmIp3nP	profitovat
z	z	k7c2	z
přítomnosti	přítomnost	k1gFnSc2	přítomnost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
holub	holub	k1gMnSc1	holub
hřivnáč	hřivnáč	k1gMnSc1	hřivnáč
<g/>
,	,	kIx,	,
kos	kos	k1gMnSc1	kos
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
čistě	čistě	k6eAd1	čistě
lesní	lesní	k2eAgMnSc1d1	lesní
pták	pták	k1gMnSc1	pták
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrabec	vrabec	k1gMnSc1	vrabec
a	a	k8xC	a
sýkora	sýkora	k1gFnSc1	sýkora
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
přikrmovány	přikrmovat	k5eAaImNgInP	přikrmovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
nabídka	nabídka	k1gFnSc1	nabídka
krmení	krmení	k1gNnSc2	krmení
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
havranů	havran	k1gMnPc2	havran
a	a	k8xC	a
racků	racek	k1gMnPc2	racek
se	se	k3xPyFc4	se
zase	zase	k9	zase
přiživují	přiživovat	k5eAaImIp3nP	přiživovat
na	na	k7c6	na
skládkách	skládka	k1gFnPc6	skládka
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
zcela	zcela	k6eAd1	zcela
nejsevernější	severní	k2eAgNnPc4d3	nejsevernější
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
plameňáků	plameňák	k1gMnPc2	plameňák
v	v	k7c4	v
Zwillbrocker	Zwillbrocker	k1gInSc4	Zwillbrocker
Venn	Venna	k1gFnPc2	Venna
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Porýní-Vestfálsku	Porýní-Vestfálsko	k1gNnSc6	Porýní-Vestfálsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
řekách	řeka	k1gFnPc6	řeka
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgMnPc1d1	vyskytující
lososi	losos	k1gMnPc1	losos
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
industrializace	industrializace	k1gFnSc2	industrializace
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyhubeni	vyhuben	k2eAgMnPc1d1	vyhuben
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
řekách	řeka	k1gFnPc6	řeka
objevili	objevit	k5eAaPmAgMnP	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
jeseter	jeseter	k1gMnSc1	jeseter
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
uloven	uloven	k2eAgInSc1d1	uloven
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rybnících	rybník	k1gInPc6	rybník
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
např.	např.	kA	např.
kapři	kapr	k1gMnPc1	kapr
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k9	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
Severního	severní	k2eAgNnSc2d1	severní
a	a	k8xC	a
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
žijící	žijící	k2eAgMnSc1d1	žijící
tuleň	tuleň	k1gMnSc1	tuleň
obecný	obecný	k2eAgMnSc1d1	obecný
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
téměř	téměř	k6eAd1	téměř
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
stavy	stav	k1gInPc1	stav
zvýšily	zvýšit	k5eAaPmAgInP	zvýšit
na	na	k7c4	na
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
exemplářů	exemplář	k1gInPc2	exemplář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
===	===	k?	===
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
je	být	k5eAaImIp3nS	být
zachování	zachování	k1gNnSc4	zachování
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
14	[number]	k4	14
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
19	[number]	k4	19
biosférických	biosférický	k2eAgFnPc2d1	biosférická
rezervací	rezervace	k1gFnPc2	rezervace
<g/>
,	,	kIx,	,
95	[number]	k4	95
přírodních	přírodní	k2eAgInPc2d1	přírodní
parků	park	k1gInPc2	park
a	a	k8xC	a
tisíce	tisíc	k4xCgInPc1	tisíc
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rezervací	rezervace	k1gFnPc2	rezervace
<g/>
,	,	kIx,	,
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
geografie	geografie	k1gFnSc1	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Využití	využití	k1gNnSc1	využití
území	území	k1gNnSc2	území
===	===	k?	===
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívá	využívat	k5eAaPmIp3nS	využívat
53,5	[number]	k4	53,5
%	%	kIx~	%
německého	německý	k2eAgNnSc2d1	německé
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
lesy	les	k1gInPc1	les
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
29,5	[number]	k4	29,5
%	%	kIx~	%
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
12,3	[number]	k4	12,3
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
zastavěné	zastavěný	k2eAgFnPc1d1	zastavěná
plochy	plocha	k1gFnPc1	plocha
a	a	k8xC	a
dopravní	dopravní	k2eAgFnSc1d1	dopravní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
1,8	[number]	k4	1,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgMnSc1d1	zbývající
2,4	[number]	k4	2,4
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
ostatní	ostatní	k2eAgFnPc1d1	ostatní
plochy	plocha	k1gFnPc1	plocha
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
skály	skála	k1gFnPc1	skála
a	a	k8xC	a
povrchové	povrchový	k2eAgInPc1d1	povrchový
doly	dol	k1gInPc1	dol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Aglomerace	aglomerace	k1gFnSc2	aglomerace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
80	[number]	k4	80
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
(	(	kIx(	(
<g/>
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
nad	nad	k7c7	nad
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
14	[number]	k4	14
měst	město	k1gNnPc2	město
má	mít	k5eAaImIp3nS	mít
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pět	pět	k4xCc1	pět
největších	veliký	k2eAgNnPc2d3	veliký
německých	německý	k2eAgNnPc2d1	německé
měst	město	k1gNnPc2	město
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
3,42	[number]	k4	3,42
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Hamburk	Hamburk	k1gInSc1	Hamburk
<g/>
,	,	kIx,	,
1,77	[number]	k4	1,77
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
1,31	[number]	k4	1,31
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
669	[number]	k4	669
021	[number]	k4	021
obyvatelV	obyvatelV	k?	obyvatelV
německém	německý	k2eAgNnSc6d1	německé
názvosloví	názvosloví	k1gNnSc6	názvosloví
se	se	k3xPyFc4	se
aglomerace	aglomerace	k1gFnSc1	aglomerace
často	často	k6eAd1	často
nazývají	nazývat	k5eAaImIp3nP	nazývat
Ballungsgebiete	Ballungsgebie	k1gNnSc2	Ballungsgebie
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
aglomerací	aglomerace	k1gFnSc7	aglomerace
je	být	k5eAaImIp3nS	být
hustě	hustě	k6eAd1	hustě
zalidněná	zalidněný	k2eAgFnSc1d1	zalidněná
oblast	oblast	k1gFnSc1	oblast
ve	v	k7c6	v
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Severní	severní	k2eAgNnSc4d1	severní
Porýní-Vestfálsko	Porýní-Vestfálsko	k1gNnSc4	Porýní-Vestfálsko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
Düsseldorf	Düsseldorf	k1gInSc1	Düsseldorf
<g/>
,	,	kIx,	,
Dortmund	Dortmund	k1gInSc1	Dortmund
a	a	k8xC	a
Essen	Essen	k1gInSc1	Essen
se	s	k7c7	s
všemi	všecek	k3xTgNnPc7	všecek
městy	město	k1gNnPc7	město
a	a	k8xC	a
obcemi	obec	k1gFnPc7	obec
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
spádovém	spádový	k2eAgNnSc6d1	spádové
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
tzv.	tzv.	kA	tzv.
metropolní	metropolní	k2eAgInSc1d1	metropolní
region	region	k1gInSc1	region
Rýn-Rúr	Rýn-Rúr	k1gInSc1	Rýn-Rúr
(	(	kIx(	(
<g/>
Rhein-Ruhr	Rhein-Ruhr	k1gInSc1	Rhein-Ruhr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vykázán	vykázán	k2eAgInSc1d1	vykázán
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
11,69	[number]	k4	11,69
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Region	region	k1gInSc1	region
Lipsko-Drážďany	Lipsko-Drážďana	k1gFnSc2	Lipsko-Drážďana
je	být	k5eAaImIp3nS	být
nazýván	nazýván	k2eAgMnSc1d1	nazýván
Střední	střední	k2eAgNnSc4d1	střední
Německo	Německo	k1gNnSc4	Německo
(	(	kIx(	(
<g/>
Mitteldeutschland	Mitteldeutschland	k1gInSc1	Mitteldeutschland
<g/>
)	)	kIx)	)
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
dohromady	dohromady	k6eAd1	dohromady
2,4	[number]	k4	2,4
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejlidnatější	lidnatý	k2eAgFnPc1d3	nejlidnatější
oblasti	oblast	k1gFnPc1	oblast
osídlení	osídlení	k1gNnSc2	osídlení
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Státní	státní	k2eAgNnSc1d1	státní
uspořádání	uspořádání	k1gNnSc1	uspořádání
===	===	k?	===
</s>
</p>
<p>
<s>
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc4	Německo
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgFnSc1d1	federativní
parlamentně-demokratická	parlamentněemokratický	k2eAgFnSc1d1	parlamentně-demokratický
a	a	k8xC	a
zastupitelská	zastupitelský	k2eAgFnSc1d1	zastupitelská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolkový	spolkový	k2eAgMnSc1d1	spolkový
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
Bundespräsident	Bundespräsident	k1gMnSc1	Bundespräsident
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
Frank-Walter	Frank-Walter	k1gMnSc1	Frank-Walter
Steinmeier	Steinmeier	k1gMnSc1	Steinmeier
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
především	především	k9	především
reprezentativní	reprezentativní	k2eAgFnPc4d1	reprezentativní
povinnosti	povinnost	k1gFnPc4	povinnost
a	a	k8xC	a
pravomoce	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
je	být	k5eAaImIp3nS	být
volen	volen	k2eAgInSc1d1	volen
Spolkovým	spolkový	k2eAgNnSc7d1	Spolkové
shromážděním	shromáždění	k1gNnSc7	shromáždění
-	-	kIx~	-
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
Spolkového	spolkový	k2eAgInSc2d1	spolkový
sněmu	sněm	k1gInSc2	sněm
(	(	kIx(	(
<g/>
Bundestag	Bundestag	k1gInSc1	Bundestag
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
stejného	stejný	k2eAgInSc2d1	stejný
počtu	počet	k1gInSc2	počet
volitelů	volitel	k1gMnPc2	volitel
zastupujících	zastupující	k2eAgMnPc2d1	zastupující
spolkové	spolkový	k2eAgFnPc4d1	spolková
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolkový	spolkový	k2eAgMnSc1d1	spolkový
kancléř	kancléř	k1gMnSc1	kancléř
(	(	kIx(	(
<g/>
Bundeskazler	Bundeskazler	k1gMnSc1	Bundeskazler
<g/>
)	)	kIx)	)
respektive	respektive	k9	respektive
kancléřka	kancléřka	k1gFnSc1	kancléřka
(	(	kIx(	(
<g/>
Bundeskanzlerin	Bundeskanzlerin	k1gInSc1	Bundeskanzlerin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
Angela	Angela	k1gFnSc1	Angela
Merkelová	Merkelová	k1gFnSc1	Merkelová
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
podobnou	podobný	k2eAgFnSc4d1	podobná
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
má	mít	k5eAaImIp3nS	mít
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
demokraciích	demokracie	k1gFnPc6	demokracie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
16	[number]	k4	16
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
souhrnně	souhrnně	k6eAd1	souhrnně
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xC	jako
Bundesländer	Bundesländer	k1gMnSc1	Bundesländer
<g/>
.	.	kIx.	.
<g/>
Každá	každý	k3xTgFnSc1	každý
země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
disponuje	disponovat	k5eAaBmIp3nS	disponovat
poměrně	poměrně	k6eAd1	poměrně
velkou	velký	k2eAgFnSc7d1	velká
autonomií	autonomie	k1gFnSc7	autonomie
v	v	k7c6	v
pohledu	pohled	k1gInSc6	pohled
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
vnitřnímu	vnitřní	k2eAgNnSc3d1	vnitřní
uspořádání	uspořádání	k1gNnSc3	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
spolkovou	spolkový	k2eAgFnSc7d1	spolková
zemí	zem	k1gFnSc7	zem
co	co	k3yInSc4	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
plochy	plocha	k1gFnSc2	plocha
je	být	k5eAaImIp3nS	být
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejvíce	hodně	k6eAd3	hodně
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Porýní-Vestfálsku	Porýní-Vestfálsko	k1gNnSc6	Porýní-Vestfálsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
směry	směr	k1gInPc1	směr
německé	německý	k2eAgFnSc2d1	německá
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
jsou	být	k5eAaImIp3nP	být
stabilní	stabilní	k2eAgNnSc4d1	stabilní
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
zeměmi	zem	k1gFnPc7	zem
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Westbindung	Westbindung	k1gInSc1	Westbindung
<g/>
)	)	kIx)	)
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
evropská	evropský	k2eAgFnSc1d1	Evropská
integrace	integrace	k1gFnSc1	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
hraje	hrát	k5eAaImIp3nS	hrát
Německo	Německo	k1gNnSc4	Německo
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
a	a	k8xC	a
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
roli	role	k1gFnSc4	role
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
členem	člen	k1gMnSc7	člen
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
Římské	římský	k2eAgFnPc4d1	římská
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
základy	základ	k1gInPc4	základ
pro	pro	k7c4	pro
dnešní	dnešní	k2eAgFnSc4d1	dnešní
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
prvkem	prvek	k1gInSc7	prvek
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
vyjádření	vyjádření	k1gNnSc2	vyjádření
prozápadní	prozápadní	k2eAgFnSc2d1	prozápadní
politiky	politika	k1gFnSc2	politika
je	být	k5eAaImIp3nS	být
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
země	země	k1gFnSc1	země
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
Německo	Německo	k1gNnSc1	Německo
členem	člen	k1gInSc7	člen
OECD	OECD	kA	OECD
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
MMF	MMF	kA	MMF
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
v	v	k7c6	v
EU	EU	kA	EU
také	také	k9	také
snaží	snažit	k5eAaImIp3nS	snažit
prosadit	prosadit	k5eAaPmF	prosadit
více	hodně	k6eAd2	hodně
sjednocený	sjednocený	k2eAgInSc1d1	sjednocený
politický	politický	k2eAgInSc1d1	politický
<g/>
,	,	kIx,	,
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
aparát	aparát	k1gInSc1	aparát
<g/>
.	.	kIx.	.
<g/>
Německo	Německo	k1gNnSc1	Německo
má	mít	k5eAaImIp3nS	mít
síť	síť	k1gFnSc4	síť
227	[number]	k4	227
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
diplomatických	diplomatický	k2eAgFnPc2d1	diplomatická
misí	mise	k1gFnPc2	mise
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
190	[number]	k4	190
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
největší	veliký	k2eAgFnSc4d3	veliký
přispěvatelem	přispěvatel	k1gMnSc7	přispěvatel
do	do	k7c2	do
evropského	evropský	k2eAgInSc2d1	evropský
rozpočtu	rozpočet	k1gInSc2	rozpočet
(	(	kIx(	(
<g/>
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
třetím	třetí	k4xOgMnSc7	třetí
největším	veliký	k2eAgMnSc7d3	veliký
přispěvatelem	přispěvatel	k1gMnSc7	přispěvatel
do	do	k7c2	do
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vláda	vláda	k1gFnSc1	vláda
kancléře	kancléř	k1gMnSc2	kancléř
Gerharda	Gerharda	k1gFnSc1	Gerharda
Schrödera	Schrödera	k1gFnSc1	Schrödera
vymezila	vymezit	k5eAaPmAgFnS	vymezit
nový	nový	k2eAgInSc4d1	nový
základ	základ	k1gInSc4	základ
německé	německý	k2eAgFnSc2d1	německá
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
NATO	NATO	kA	NATO
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Operace	operace	k1gFnSc2	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
byli	být	k5eAaImAgMnP	být
oficiálně	oficiálně	k6eAd1	oficiálně
posláni	poslat	k5eAaPmNgMnP	poslat
do	do	k7c2	do
boje	boj	k1gInSc2	boj
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Politické	politický	k2eAgInPc1d1	politický
subjekty	subjekt	k1gInPc1	subjekt
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
politice	politika	k1gFnSc6	politika
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
po	po	k7c6	po
sjednocení	sjednocení	k1gNnSc6	sjednocení
země	zem	k1gFnSc2	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Spolkové	spolkový	k2eAgFnSc6d1	spolková
republice	republika	k1gFnSc6	republika
dominovaly	dominovat	k5eAaImAgInP	dominovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
dvě	dva	k4xCgNnPc4	dva
velká	velký	k2eAgNnPc4d1	velké
uskupení	uskupení	k1gNnPc4	uskupení
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
středopravá	středopravý	k2eAgFnSc1d1	středopravá
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
<g/>
)	)	kIx)	)
Křesťanskodemokratická	křesťanskodemokratický	k2eAgFnSc1d1	Křesťanskodemokratická
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
CDU	CDU	kA	CDU
<g/>
)	)	kIx)	)
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
Křesťansko-sociální	křesťanskoociální	k2eAgFnSc7d1	křesťansko-sociální
unií	unie	k1gFnSc7	unie
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
(	(	kIx(	(
<g/>
CSU	CSU	kA	CSU
<g/>
)	)	kIx)	)
a	a	k8xC	a
středolevá	středolevý	k2eAgFnSc1d1	středolevá
Sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1	sociálnědemokratická
strana	strana	k1gFnSc1	strana
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
SPD	SPD	kA	SPD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
nemohla	moct	k5eNaImAgFnS	moct
mít	mít	k5eAaImF	mít
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc1	Německo
zpočátku	zpočátku	k6eAd1	zpočátku
vlastní	vlastní	k2eAgFnPc4d1	vlastní
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
korejské	korejský	k2eAgFnSc6d1	Korejská
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
sovětské	sovětský	k2eAgFnSc3d1	sovětská
politice	politika	k1gFnSc3	politika
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
bylo	být	k5eAaImAgNnS	být
Západnímu	západní	k2eAgInSc3d1	západní
Německu	Německo	k1gNnSc6	Německo
povoleno	povolen	k2eAgNnSc1d1	povoleno
vyzbrojit	vyzbrojit	k5eAaPmF	vyzbrojit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
založena	založen	k2eAgFnSc1d1	založena
polovojenská	polovojenský	k2eAgFnSc1d1	polovojenská
spolková	spolkový	k2eAgFnSc1d1	spolková
pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
stráž	stráž	k1gFnSc1	stráž
Bundesgrenzschutz	Bundesgrenzschutza	k1gFnPc2	Bundesgrenzschutza
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
se	se	k3xPyFc4	se
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
NDR	NDR	kA	NDR
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
národní	národní	k2eAgFnSc1d1	národní
lidová	lidový	k2eAgFnSc1d1	lidová
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
Bundeswehr	bundeswehr	k1gInSc1	bundeswehr
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
jako	jako	k8xS	jako
Heer	Heer	k1gInSc1	Heer
(	(	kIx(	(
<g/>
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
speciální	speciální	k2eAgFnPc1d1	speciální
jednotky	jednotka	k1gFnPc1	jednotka
KSK	KSK	kA	KSK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marine	Marin	k1gMnSc5	Marin
(	(	kIx(	(
<g/>
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Luftwaffe	Luftwaff	k1gMnSc5	Luftwaff
(	(	kIx(	(
<g/>
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ústřední	ústřední	k2eAgFnSc1d1	ústřední
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
služba	služba	k1gFnSc1	služba
a	a	k8xC	a
Streitkräftebasis	Streitkräftebasis	k1gInSc1	Streitkräftebasis
(	(	kIx(	(
<g/>
základny	základna	k1gFnPc1	základna
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byly	být	k5eAaImAgInP	být
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
odhadovány	odhadovat	k5eAaImNgFnP	odhadovat
na	na	k7c4	na
1,3	[number]	k4	1,3
%	%	kIx~	%
německého	německý	k2eAgInSc2d1	německý
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
členy	člen	k1gMnPc7	člen
NATO	nato	k6eAd1	nato
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
absolutních	absolutní	k2eAgNnPc6d1	absolutní
číslech	číslo	k1gNnPc6	číslo
jsou	být	k5eAaImIp3nP	být
německé	německý	k2eAgInPc4d1	německý
vojenské	vojenský	k2eAgInPc4d1	vojenský
výdaje	výdaj	k1gInPc4	výdaj
9	[number]	k4	9
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
je	být	k5eAaImIp3nS	být
Bundeswehr	bundeswehr	k1gInSc1	bundeswehr
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
válečného	válečný	k2eAgInSc2d1	válečný
stavu	stav	k1gInSc2	stav
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
stal	stát	k5eAaPmAgMnS	stát
kancléř	kancléř	k1gMnSc1	kancléř
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
Bundeswehru	bundeswehr	k1gInSc6	bundeswehr
sloužilo	sloužit	k5eAaImAgNnS	sloužit
183	[number]	k4	183
000	[number]	k4	000
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
17	[number]	k4	17
000	[number]	k4	000
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
vláda	vláda	k1gFnSc1	vláda
plánuje	plánovat	k5eAaImIp3nS	plánovat
snížit	snížit	k5eAaPmF	snížit
počet	počet	k1gInSc1	počet
vojáků	voják	k1gMnPc2	voják
na	na	k7c4	na
170	[number]	k4	170
000	[number]	k4	000
profesionálů	profesionál	k1gMnPc2	profesionál
a	a	k8xC	a
15	[number]	k4	15
000	[number]	k4	000
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
také	také	k9	také
záložníky	záložník	k1gMnPc7	záložník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
obraných	obraný	k2eAgInPc6d1	obraný
cvičeních	cvičení	k1gNnPc6	cvičení
a	a	k8xC	a
nasazení	nasazení	k1gNnSc1	nasazení
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
organizace	organizace	k1gFnSc2	organizace
SIPRI	SIPRI	kA	SIPRI
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
největším	veliký	k2eAgMnSc7d3	veliký
vývozcem	vývozce	k1gMnSc7	vývozce
zbraní	zbraň	k1gFnPc2	zbraň
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Makroekonomické	makroekonomický	k2eAgInPc1d1	makroekonomický
údaje	údaj	k1gInPc1	údaj
===	===	k?	===
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
surovinově	surovinově	k6eAd1	surovinově
relativně	relativně	k6eAd1	relativně
chudá	chudý	k2eAgFnSc1d1	chudá
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
hospodářství	hospodářství	k1gNnSc1	hospodářství
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
v	v	k7c6	v
sektoru	sektor	k1gInSc6	sektor
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
2	[number]	k4	2
307,2	[number]	k4	307,2
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
činil	činit	k5eAaImAgInS	činit
HDP	HDP	kA	HDP
28	[number]	k4	28
012	[number]	k4	012
€	€	k?	€
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
má	mít	k5eAaImIp3nS	mít
třetí	třetí	k4xOgNnSc4	třetí
největší	veliký	k2eAgNnSc4d3	veliký
národní	národní	k2eAgNnSc4d1	národní
hospodářství	hospodářství	k1gNnSc4	hospodářství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hrubém	hrubý	k2eAgInSc6d1	hrubý
národním	národní	k2eAgInSc6d1	národní
produktu	produkt	k1gInSc6	produkt
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
přesto	přesto	k8xC	přesto
jen	jen	k9	jen
na	na	k7c4	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sektoru	sektor	k1gInSc6	sektor
služeb	služba	k1gFnPc2	služba
pracovalo	pracovat	k5eAaImAgNnS	pracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
63,8	[number]	k4	63,8
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
33,4	[number]	k4	33,4
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
2,8	[number]	k4	2,8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc1	služba
produkovaly	produkovat	k5eAaImAgFnP	produkovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
68,5	[number]	k4	68,5
%	%	kIx~	%
národního	národní	k2eAgInSc2d1	národní
důchodu	důchod	k1gInSc2	důchod
<g/>
,	,	kIx,	,
průmysl	průmysl	k1gInSc1	průmysl
30,2	[number]	k4	30,2
%	%	kIx~	%
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc1	zemědělství
1,3	[number]	k4	1,3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
německou	německý	k2eAgFnSc4d1	německá
ekonomiku	ekonomika	k1gFnSc4	ekonomika
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
brzdí	brzdit	k5eAaImIp3nP	brzdit
nedostatek	nedostatek	k1gInSc4	nedostatek
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
způsobený	způsobený	k2eAgInSc1d1	způsobený
extrémně	extrémně	k6eAd1	extrémně
nepříznivým	příznivý	k2eNgInSc7d1	nepříznivý
demografickým	demografický	k2eAgInSc7d1	demografický
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2030	[number]	k4	2030
bude	být	k5eAaImBp3nS	být
německé	německý	k2eAgFnSc2d1	německá
ekonomice	ekonomika	k1gFnSc3	ekonomika
chybět	chybět	k5eAaImF	chybět
na	na	k7c4	na
5,2	[number]	k4	5,2
milionu	milion	k4xCgInSc2	milion
kvalifikovaných	kvalifikovaný	k2eAgFnPc2d1	kvalifikovaná
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2,4	[number]	k4	2,4
milionu	milion	k4xCgInSc2	milion
vysokoškolsky	vysokoškolsky	k6eAd1	vysokoškolsky
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
také	také	k9	také
neprozřetelným	prozřetelný	k2eNgInSc7d1	neprozřetelný
sedmileté	sedmiletý	k2eAgNnSc1d1	sedmileté
omezení	omezení	k1gNnSc1	omezení
pracovního	pracovní	k2eAgInSc2d1	pracovní
trhu	trh	k1gInSc2	trh
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
v	v	k7c4	v
účinnost	účinnost	k1gFnSc4	účinnost
po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
EU	EU	kA	EU
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
<g/>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
špičce	špička	k1gFnSc6	špička
v	v	k7c6	v
automobilovém	automobilový	k2eAgNnSc6d1	automobilové
<g/>
,	,	kIx,	,
elektrotechnickém	elektrotechnický	k2eAgNnSc6d1	elektrotechnické
<g/>
,	,	kIx,	,
strojírenském	strojírenský	k2eAgMnSc6d1	strojírenský
a	a	k8xC	a
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003-2008	[number]	k4	2003-2008
bylo	být	k5eAaImAgNnS	být
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
vývozcem	vývozce	k1gMnSc7	vývozce
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
<g/>
Vývoj	vývoj	k1gInSc1	vývoj
hospodářství	hospodářství	k1gNnSc2	hospodářství
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
po	po	k7c6	po
konstituování	konstituování	k1gNnSc6	konstituování
dvou	dva	k4xCgInPc2	dva
německých	německý	k2eAgMnPc2d1	německý
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
tři	tři	k4xCgFnPc1	tři
tradiční	tradiční	k2eAgFnPc1d1	tradiční
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
oblasti	oblast	k1gFnPc1	oblast
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
Horní	horní	k2eAgNnSc1d1	horní
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
,	,	kIx,	,
Sasko	Sasko	k1gNnSc1	Sasko
a	a	k8xC	a
Porúří	Porúří	k1gNnSc1	Porúří
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ocitly	ocitnout	k5eAaPmAgFnP	ocitnout
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
různých	různý	k2eAgInPc6d1	různý
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
NDR	NDR	kA	NDR
a	a	k8xC	a
NSR	NSR	kA	NSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
měly	mít	k5eAaImAgFnP	mít
zcela	zcela	k6eAd1	zcela
odlišné	odlišný	k2eAgFnPc1d1	odlišná
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgNnSc1d1	horní
Slezsko	Slezsko	k1gNnSc1	Slezsko
připadlo	připadnout	k5eAaPmAgNnS	připadnout
téměř	téměř	k6eAd1	téměř
celé	celá	k1gFnSc3	celá
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Saska	Sasko	k1gNnSc2	Sasko
bylo	být	k5eAaImAgNnS	být
strojové	strojový	k2eAgNnSc1d1	strojové
vybavení	vybavení	k1gNnSc1	vybavení
mnoha	mnoho	k4c2	mnoho
továren	továrna	k1gFnPc2	továrna
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
odvezeno	odvézt	k5eAaPmNgNnS	odvézt
jako	jako	k8xC	jako
reparace	reparace	k1gFnPc4	reparace
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Porúří	Porúří	k1gNnSc6	Porúří
(	(	kIx(	(
<g/>
s	s	k7c7	s
městy	město	k1gNnPc7	město
jako	jako	k8xC	jako
Dortmund	Dortmund	k1gInSc1	Dortmund
<g/>
,	,	kIx,	,
Essen	Essen	k1gInSc1	Essen
a	a	k8xC	a
Duisburg	Duisburg	k1gInSc1	Duisburg
<g/>
)	)	kIx)	)
hrály	hrát	k5eAaImAgInP	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
i	i	k9	i
nadále	nadále	k6eAd1	nadále
těžký	těžký	k2eAgInSc1d1	těžký
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
těžba	těžba	k1gFnSc1	těžba
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Německo	Německo	k1gNnSc4	Německo
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
finančně	finančně	k6eAd1	finančně
výhodnější	výhodný	k2eAgNnSc4d2	výhodnější
uhlí	uhlí	k1gNnSc4	uhlí
dovážet	dovážet	k5eAaImF	dovážet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
úplném	úplný	k2eAgInSc6d1	úplný
útlumu	útlum	k1gInSc6	útlum
těžby	těžba	k1gFnSc2	těžba
černého	černé	k1gNnSc2	černé
(	(	kIx(	(
<g/>
kamenného	kamenný	k2eAgNnSc2d1	kamenné
<g/>
)	)	kIx)	)
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
zatím	zatím	k6eAd1	zatím
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
USA	USA	kA	USA
na	na	k7c6	na
předním	přední	k2eAgNnSc6d1	přední
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
v	v	k7c6	v
těžbě	těžba	k1gFnSc6	těžba
hnědého	hnědý	k2eAgNnSc2d1	hnědé
uhlí	uhlí	k1gNnSc2	uhlí
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Východním	východní	k2eAgNnSc6d1	východní
Německu	Německo	k1gNnSc6	Německo
<g/>
)	)	kIx)	)
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
spotřebě	spotřeba	k1gFnSc6	spotřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
životní	životní	k2eAgFnSc6d1	životní
úrovni	úroveň	k1gFnSc6	úroveň
je	být	k5eAaImIp3nS	být
Německo	Německo	k1gNnSc1	Německo
dle	dle	k7c2	dle
Indexu	index	k1gInSc2	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
(	(	kIx(	(
<g/>
HDI	HDI	kA	HDI
<g/>
)	)	kIx)	)
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
a	a	k8xC	a
lesnictví	lesnictví	k1gNnSc1	lesnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Zemědělsky	zemědělsky	k6eAd1	zemědělsky
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
plochy	plocha	k1gFnSc2	plocha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
výrobních	výrobní	k2eAgFnPc2d1	výrobní
metod	metoda	k1gFnPc2	metoda
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
jen	jen	k6eAd1	jen
kolem	kolem	k7c2	kolem
2-3	[number]	k4	2-3
%	%	kIx~	%
práceschopného	práceschopný	k2eAgNnSc2d1	práceschopné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zemědělců	zemědělec	k1gMnPc2	zemědělec
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
tuto	tento	k3xDgFnSc4	tento
činnost	činnost	k1gFnSc4	činnost
vedle	vedle	k7c2	vedle
jiného	jiný	k2eAgNnSc2d1	jiné
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
představuje	představovat	k5eAaImIp3nS	představovat
34	[number]	k4	34
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
4	[number]	k4	4
%	%	kIx~	%
orné	orný	k2eAgFnSc2d1	orná
půdy	půda	k1gFnSc2	půda
jsou	být	k5eAaImIp3nP	být
zavlažovány	zavlažován	k2eAgFnPc1d1	zavlažována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
služby	služba	k1gFnPc1	služba
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
500	[number]	k4	500
největších	veliký	k2eAgFnPc2d3	veliký
firem	firma	k1gFnPc2	firma
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
37	[number]	k4	37
německých	německý	k2eAgMnPc2d1	německý
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
30	[number]	k4	30
firem	firma	k1gFnPc2	firma
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c2	za
společnosti	společnost	k1gFnSc2	společnost
zobrazené	zobrazený	k2eAgFnSc2d1	zobrazená
v	v	k7c6	v
akciovém	akciový	k2eAgInSc6d1	akciový
indexu	index	k1gInSc6	index
DAX	DAX	kA	DAX
(	(	kIx(	(
<g/>
Deutscher	Deutschra	k1gFnPc2	Deutschra
Aktienindex	Aktienindex	k1gInSc1	Aktienindex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
společnosti	společnost	k1gFnPc1	společnost
jsou	být	k5eAaImIp3nP	být
vesměs	vesměs	k6eAd1	vesměs
činné	činný	k2eAgInPc1d1	činný
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
německém	německý	k2eAgInSc6d1	německý
nebo	nebo	k8xC	nebo
evropském	evropský	k2eAgInSc6d1	evropský
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
německé	německý	k2eAgFnPc4d1	německá
firmy	firma	k1gFnPc4	firma
patří	patřit	k5eAaImIp3nP	patřit
automobilky	automobilka	k1gFnPc1	automobilka
Volkswagen	volkswagen	k1gInSc1	volkswagen
<g/>
,	,	kIx,	,
Daimler	Daimler	k1gInSc1	Daimler
AG	AG	kA	AG
a	a	k8xC	a
BMW	BMW	kA	BMW
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
programů	program	k1gInPc2	program
pro	pro	k7c4	pro
firemní	firemní	k2eAgNnSc4d1	firemní
použití	použití	k1gNnSc4	použití
SAP	sapa	k1gFnPc2	sapa
<g/>
,	,	kIx,	,
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
Allianz	Allianza	k1gFnPc2	Allianza
a	a	k8xC	a
Münchener	Münchener	k1gMnSc1	Münchener
Rück	Rück	k1gMnSc1	Rück
<g/>
,	,	kIx,	,
výrobce	výrobce	k1gMnSc1	výrobce
elektrických	elektrický	k2eAgMnPc2d1	elektrický
zařízení	zařízení	k1gNnPc1	zařízení
Siemens	siemens	k1gInSc1	siemens
AG	AG	kA	AG
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnSc2d1	chemická
firmy	firma	k1gFnSc2	firma
Bayer	Bayer	k1gMnSc1	Bayer
AG	AG	kA	AG
a	a	k8xC	a
BASF	BASF	kA	BASF
<g/>
,	,	kIx,	,
výrobci	výrobce	k1gMnPc1	výrobce
sportovního	sportovní	k2eAgNnSc2d1	sportovní
zboží	zboží	k1gNnSc2	zboží
Adidas	Adidas	k1gInSc1	Adidas
a	a	k8xC	a
Puma	puma	k1gFnSc1	puma
a	a	k8xC	a
přes	přes	k7c4	přes
různé	různý	k2eAgInPc4d1	různý
problémy	problém	k1gInPc4	problém
nadále	nadále	k6eAd1	nadále
i	i	k9	i
velká	velký	k2eAgFnSc1d1	velká
obchodní	obchodní	k2eAgFnSc1d1	obchodní
banka	banka	k1gFnSc1	banka
Deutsche	Deutsch	k1gFnSc2	Deutsch
Bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
firmy	firma	k1gFnPc4	firma
známé	známý	k2eAgFnPc4d1	známá
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
patří	patřit	k5eAaImIp3nS	patřit
výrobce	výrobce	k1gMnSc1	výrobce
pneumatik	pneumatika	k1gFnPc2	pneumatika
Continental	Continental	k1gMnSc1	Continental
(	(	kIx(	(
<g/>
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
patří	patřit	k5eAaImIp3nS	patřit
česká	český	k2eAgFnSc1d1	Česká
firma	firma	k1gFnSc1	firma
Continental	Continental	k1gMnSc1	Continental
Barum	barum	k1gInSc1	barum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sesterské	sesterský	k2eAgFnSc2d1	sesterská
farmaceutické	farmaceutický	k2eAgFnSc2d1	farmaceutická
firmy	firma	k1gFnSc2	firma
Fresenius	Fresenius	k1gMnSc1	Fresenius
a	a	k8xC	a
Fresenius	Fresenius	k1gMnSc1	Fresenius
Medical	Medical	k1gMnSc5	Medical
Care	car	k1gMnSc5	car
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Robert	Robert	k1gMnSc1	Robert
Bosch	Bosch	kA	Bosch
GmbH	GmbH	k1gMnSc1	GmbH
a	a	k8xC	a
elektrárenské	elektrárenský	k2eAgFnPc1d1	elektrárenská
společnosti	společnost	k1gFnPc1	společnost
E.	E.	kA	E.
<g/>
ON	on	k3xPp3gMnSc1	on
a	a	k8xC	a
RWE	RWE	kA	RWE
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
význam	význam	k1gInSc4	význam
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
mj.	mj.	kA	mj.
Deutsche	Deutsche	k1gNnSc4	Deutsche
Post	post	k1gInSc1	post
<g/>
,	,	kIx,	,
telekomunikační	telekomunikační	k2eAgInSc1d1	telekomunikační
koncern	koncern	k1gInSc1	koncern
Deutsche	Deutsche	k1gFnPc2	Deutsche
Telekom	Telekom	k1gInSc1	Telekom
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc1d1	obchodní
společnost	společnost	k1gFnSc1	společnost
Metro	metro	k1gNnSc4	metro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitým	důležitý	k2eAgNnSc7d1	důležité
měřítkem	měřítko	k1gNnSc7	měřítko
významu	význam	k1gInSc2	význam
německých	německý	k2eAgInPc2d1	německý
koncernů	koncern	k1gInPc2	koncern
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
tržní	tržní	k2eAgFnSc1d1	tržní
kapitalizace	kapitalizace	k1gFnSc1	kapitalizace
(	(	kIx(	(
<g/>
Marktkapitalisierung	Marktkapitalisierung	k1gInSc1	Marktkapitalisierung
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
násobením	násobení	k1gNnSc7	násobení
počtu	počet	k1gInSc2	počet
akcií	akcie	k1gFnPc2	akcie
společnosti	společnost	k1gFnSc2	společnost
jejich	jejich	k3xOp3gInSc7	jejich
aktuálním	aktuální	k2eAgInSc7d1	aktuální
kurzem	kurz	k1gInSc7	kurz
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
největší	veliký	k2eAgFnSc1d3	veliký
vykázána	vykázán	k2eAgFnSc1d1	vykázána
chemická	chemický	k2eAgFnSc1d1	chemická
společnost	společnost	k1gFnSc1	společnost
Bayer	Bayer	k1gMnSc1	Bayer
(	(	kIx(	(
<g/>
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
:	:	kIx,	:
Leverkusen	Leverkusen	k1gInSc1	Leverkusen
<g/>
)	)	kIx)	)
s	s	k7c7	s
kapitálem	kapitál	k1gInSc7	kapitál
96,0	[number]	k4	96,0
miliardy	miliarda	k4xCgFnSc2	miliarda
Eur	euro	k1gNnPc2	euro
<g/>
,	,	kIx,	,
následována	následován	k2eAgFnSc1d1	následována
koncernem	koncern	k1gInSc7	koncern
SAP	sapa	k1gFnPc2	sapa
s	s	k7c7	s
kapitálem	kapitál	k1gInSc7	kapitál
89,8	[number]	k4	89,8
mrd	mrd	k?	mrd
<g/>
.	.	kIx.	.
</s>
<s>
Eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
je	být	k5eAaImIp3nS	být
automobilka	automobilka	k1gFnSc1	automobilka
Daimler	Daimler	k1gMnSc1	Daimler
(	(	kIx(	(
<g/>
82,9	[number]	k4	82,9
mrd	mrd	k?	mrd
<g/>
.	.	kIx.	.
</s>
<s>
Eur	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
je	být	k5eAaImIp3nS	být
Siemens	siemens	k1gInSc1	siemens
(	(	kIx(	(
<g/>
79,0	[number]	k4	79,0
mrd	mrd	k?	mrd
<g/>
.	.	kIx.	.
</s>
<s>
Eur	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
a	a	k8xC	a
pátá	pátý	k4xOgFnSc1	pátý
Deutsche	Deutsche	k1gFnSc1	Deutsche
Telekom	Telekom	k1gInSc1	Telekom
(	(	kIx(	(
<g/>
76,3	[number]	k4	76,3
mrd	mrd	k?	mrd
<g/>
.	.	kIx.	.
</s>
<s>
Eur	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
devátém	devátý	k4xOgNnSc6	devátý
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
automobilka	automobilka	k1gFnSc1	automobilka
BMW	BMW	kA	BMW
(	(	kIx(	(
<g/>
Bayerische	Bayerisch	k1gMnSc2	Bayerisch
Motorenwerke	Motorenwerk	k1gMnSc2	Motorenwerk
<g/>
)	)	kIx)	)
s	s	k7c7	s
kapitalizací	kapitalizace	k1gFnSc7	kapitalizace
63,5	[number]	k4	63,5
mrd	mrd	k?	mrd
<g/>
.	.	kIx.	.
</s>
<s>
Eur	euro	k1gNnPc2	euro
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
akcií	akcie	k1gFnPc2	akcie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Stefana	Stefan	k1gMnSc2	Stefan
Quandta	Quandt	k1gMnSc2	Quandt
a	a	k8xC	a
Susanne	Susann	k1gInSc5	Susann
Klattenové	Klattenové	k2eAgMnSc2d1	Klattenové
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
a	a	k8xC	a
dcery	dcera	k1gFnSc2	dcera
průmyslníka	průmyslník	k1gMnSc4	průmyslník
Herberta	Herbert	k1gMnSc4	Herbert
Quandta	Quandt	k1gMnSc4	Quandt
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
velikosti	velikost	k1gFnSc2	velikost
společností	společnost	k1gFnPc2	společnost
z	z	k7c2	z
indexu	index	k1gInSc2	index
DAX	DAX	kA	DAX
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
automobilka	automobilka	k1gFnSc1	automobilka
Volkswagen	volkswagen	k1gInSc1	volkswagen
(	(	kIx(	(
<g/>
VW	VW	kA	VW
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
tzv.	tzv.	kA	tzv.
zvýhodněné	zvýhodněný	k2eAgFnPc4d1	zvýhodněná
akcie	akcie	k1gFnPc4	akcie
(	(	kIx(	(
<g/>
Vorzugsaktien	Vorzugsaktien	k1gInSc1	Vorzugsaktien
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Vorzüge	Vorzüge	k1gNnSc1	Vorzüge
<g/>
)	)	kIx)	)
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
70,0	[number]	k4	70,0
mrd	mrd	k?	mrd
<g/>
.	.	kIx.	.
</s>
<s>
Eur	euro	k1gNnPc2	euro
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
v	v	k7c6	v
indexu	index	k1gInSc6	index
DAX	DAX	kA	DAX
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kurzy	kurz	k1gInPc1	kurz
tzv.	tzv.	kA	tzv.
kmenových	kmenový	k2eAgFnPc2d1	kmenová
akcií	akcie	k1gFnPc2	akcie
(	(	kIx(	(
<g/>
Stammaktien	Stammaktien	k1gInSc1	Stammaktien
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Stämme	Stämme	k1gMnPc1	Stämme
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
Prime	prim	k1gInSc5	prim
Standard	standard	k1gInSc1	standard
bez	bez	k7c2	bez
tržní	tržní	k2eAgFnSc2d1	tržní
kapitalizace	kapitalizace	k1gFnSc2	kapitalizace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
všech	všecek	k3xTgFnPc2	všecek
akcií	akcie	k1gFnPc2	akcie
VW	VW	kA	VW
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Porsche	Porsche	k1gNnSc4	Porsche
Holding	holding	k1gInSc1	holding
<g/>
)	)	kIx)	)
držena	držen	k2eAgFnSc1d1	držena
dvěma	dva	k4xCgFnPc7	dva
podnikatelskými	podnikatelský	k2eAgFnPc7d1	podnikatelská
rodinami	rodina	k1gFnPc7	rodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
rodinami	rodina	k1gFnPc7	rodina
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
Porscheho	Porsche	k1gMnSc2	Porsche
a	a	k8xC	a
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Piëcha	Piëch	k1gMnSc4	Piëch
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
jsou	být	k5eAaImIp3nP	být
vnuci	vnuk	k1gMnPc1	vnuk
zakladatele	zakladatel	k1gMnSc2	zakladatel
firem	firma	k1gFnPc2	firma
Volkswagen	volkswagen	k1gInSc4	volkswagen
a	a	k8xC	a
Porsche	Porsche	k1gNnSc4	Porsche
<g/>
,	,	kIx,	,
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Porscheho	Porsche	k1gMnSc2	Porsche
<g/>
.	.	kIx.	.
</s>
<s>
Volkswagen	volkswagen	k1gInSc1	volkswagen
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
automobilek	automobilka	k1gFnPc2	automobilka
světa	svět	k1gInSc2	svět
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
jí	on	k3xPp3gFnSc3	on
mj.	mj.	kA	mj.
100	[number]	k4	100
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
Škody	škoda	k1gFnSc2	škoda
Auto	auto	k1gNnSc1	auto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
měla	mít	k5eAaImAgFnS	mít
celkově	celkově	k6eAd1	celkově
592	[number]	k4	592
600	[number]	k4	600
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společnosti	společnost	k1gFnPc1	společnost
</s>
</p>
<p>
<s>
===	===	k?	===
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
dlážděné	dlážděný	k2eAgFnPc1d1	dlážděná
silnice	silnice	k1gFnPc1	silnice
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Německa	Německo	k1gNnSc2	Německo
založili	založit	k5eAaPmAgMnP	založit
již	již	k9	již
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
státní	státní	k2eAgFnPc1d1	státní
silnice	silnice	k1gFnPc1	silnice
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
automobilismu	automobilismus	k1gInSc2	automobilismus
dal	dát	k5eAaPmAgInS	dát
výstavbě	výstavba	k1gFnSc3	výstavba
silnic	silnice	k1gFnPc2	silnice
nové	nový	k2eAgInPc1d1	nový
impulsy	impuls	k1gInPc1	impuls
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
dálnice	dálnice	k1gFnSc1	dálnice
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
AVUS	AVUS	kA	AVUS
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
přepraveného	přepravený	k2eAgInSc2d1	přepravený
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
mělo	mít	k5eAaImAgNnS	mít
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2007	[number]	k4	2007
12	[number]	k4	12
531	[number]	k4	531
kilometrů	kilometr	k1gInPc2	kilometr
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
,	,	kIx,	,
40	[number]	k4	40
711	[number]	k4	711
kilometrů	kilometr	k1gInPc2	kilometr
spolkových	spolkový	k2eAgFnPc2d1	spolková
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
86	[number]	k4	86
597	[number]	k4	597
zemských	zemský	k2eAgFnPc2d1	zemská
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
91	[number]	k4	91
520	[number]	k4	520
kilometrů	kilometr	k1gInPc2	kilometr
okresních	okresní	k2eAgFnPc2d1	okresní
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c6	na
německých	německý	k2eAgFnPc6d1	německá
silnicích	silnice	k1gFnPc6	silnice
5094	[number]	k4	5094
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
mrtvých	mrtvý	k1gMnPc2	mrtvý
má	mít	k5eAaImIp3nS	mít
klesající	klesající	k2eAgFnSc4d1	klesající
tendenci	tendence	k1gFnSc4	tendence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
velice	velice	k6eAd1	velice
časté	častý	k2eAgFnPc1d1	častá
pěší	pěší	k2eAgFnPc1d1	pěší
zóny	zóna	k1gFnPc1	zóna
<g/>
,	,	kIx,	,
zóny	zóna	k1gFnPc1	zóna
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
rychlostí	rychlost	k1gFnSc7	rychlost
30	[number]	k4	30
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
disponuje	disponovat	k5eAaBmIp3nS	disponovat
železniční	železniční	k2eAgFnSc7d1	železniční
sítí	síť	k1gFnSc7	síť
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
35	[number]	k4	35
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
je	být	k5eAaImIp3nS	být
vypraveno	vypravit	k5eAaPmNgNnS	vypravit
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
osobních	osobní	k2eAgInPc2d1	osobní
a	a	k8xC	a
nákladních	nákladní	k2eAgInPc2d1	nákladní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
reformy	reforma	k1gFnSc2	reforma
železnice	železnice	k1gFnSc2	železnice
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1994	[number]	k4	1994
sloučena	sloučen	k2eAgFnSc1d1	sloučena
Deutsche	Deutsche	k1gFnSc1	Deutsche
Bundesbahn	Bundesbahna	k1gFnPc2	Bundesbahna
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Deutsche	Deutsche	k1gFnSc1	Deutsche
Reichsbahn	Reichsbahna	k1gFnPc2	Reichsbahna
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc2d1	východní
část	část	k1gFnSc4	část
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
do	do	k7c2	do
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
Deutsche	Deutsche	k1gNnSc2	Deutsche
Bahn	Bahna	k1gFnPc2	Bahna
AG	AG	kA	AG
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
organizuje	organizovat	k5eAaBmIp3nS	organizovat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Deutsche	Deutsche	k1gNnSc2	Deutsche
Bahn	Bahna	k1gFnPc2	Bahna
AG	AG	kA	AG
jezdí	jezdit	k5eAaImIp3nP	jezdit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
asi	asi	k9	asi
350	[number]	k4	350
dalších	další	k2eAgMnPc2d1	další
menších	malý	k2eAgMnPc2d2	menší
regionálních	regionální	k2eAgMnPc2d1	regionální
železničních	železniční	k2eAgMnPc2d1	železniční
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
financuje	financovat	k5eAaBmIp3nS	financovat
údržbu	údržba	k1gFnSc4	údržba
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
dotuje	dotovat	k5eAaBmIp3nS	dotovat
regionální	regionální	k2eAgFnSc4d1	regionální
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
regionální	regionální	k2eAgFnSc6d1	regionální
dopravě	doprava	k1gFnSc6	doprava
i	i	k8xC	i
dálkové	dálkový	k2eAgFnSc6d1	dálková
dopravě	doprava	k1gFnSc6	doprava
jezdí	jezdit	k5eAaImIp3nP	jezdit
vlaky	vlak	k1gInPc1	vlak
většinou	většina	k1gFnSc7	většina
podle	podle	k7c2	podle
taktového	taktový	k2eAgInSc2d1	taktový
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dálkových	dálkový	k2eAgFnPc6d1	dálková
trasách	trasa	k1gFnPc6	trasa
jezdí	jezdit	k5eAaImIp3nP	jezdit
rychlovlaky	rychlovlak	k1gInPc1	rychlovlak
ICE	ICE	kA	ICE
na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
2000	[number]	k4	2000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
železnice	železnice	k1gFnSc2	železnice
na	na	k7c6	na
nákladní	nákladní	k2eAgFnSc6d1	nákladní
i	i	k8xC	i
osobní	osobní	k2eAgFnSc6d1	osobní
přepravě	přeprava	k1gFnSc6	přeprava
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
klesal	klesat	k5eAaImAgInS	klesat
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
má	mít	k5eAaImIp3nS	mít
opět	opět	k6eAd1	opět
stoupající	stoupající	k2eAgFnSc4d1	stoupající
tendenci	tendence	k1gFnSc4	tendence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letiště	letiště	k1gNnSc1	letiště
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
je	být	k5eAaImIp3nS	být
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
letištěm	letiště	k1gNnSc7	letiště
společnosti	společnost	k1gFnSc2	společnost
Lufthansa	Lufthansa	k1gFnSc1	Lufthansa
a	a	k8xC	a
třetím	třetí	k4xOgMnSc6	třetí
největším	veliký	k2eAgNnSc7d3	veliký
letištěm	letiště	k1gNnSc7	letiště
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
co	co	k3yQnSc1	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
odbavených	odbavený	k2eAgFnPc2d1	odbavená
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
přepraveného	přepravený	k2eAgNnSc2d1	přepravené
zboží	zboží	k1gNnSc2	zboží
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
letištěm	letiště	k1gNnSc7	letiště
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurtské	frankfurtský	k2eAgNnSc1d1	Frankfurtské
letiště	letiště	k1gNnSc1	letiště
ročně	ročně	k6eAd1	ročně
odbaví	odbavit	k5eAaPmIp3nS	odbavit
52	[number]	k4	52
miliónů	milión	k4xCgInPc2	milión
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Mnichovské	mnichovský	k2eAgNnSc1d1	mnichovské
letiště	letiště	k1gNnSc1	letiště
Franze	Franze	k1gFnSc2	Franze
Josefa	Josef	k1gMnSc2	Josef
Strausse	Strauss	k1gMnSc2	Strauss
přepraví	přepravit	k5eAaPmIp3nS	přepravit
ročně	ročně	k6eAd1	ročně
32	[number]	k4	32
miliónů	milión	k4xCgInPc2	milión
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
berlínské	berlínský	k2eAgNnSc1d1	berlínské
letiště	letiště	k1gNnSc1	letiště
bude	být	k5eAaImBp3nS	být
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
třetím	třetí	k4xOgMnSc6	třetí
největším	veliký	k2eAgNnSc7d3	veliký
německým	německý	k2eAgNnSc7d1	německé
letištěm	letiště	k1gNnSc7	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
má	mít	k5eAaImIp3nS	mít
celkově	celkově	k6eAd1	celkově
430	[number]	k4	430
letišť	letiště	k1gNnPc2	letiště
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc4d3	veliký
hustotu	hustota	k1gFnSc4	hustota
přistávacích	přistávací	k2eAgFnPc2d1	přistávací
a	a	k8xC	a
startovacích	startovací	k2eAgFnPc2d1	startovací
drah	draha	k1gFnPc2	draha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
exportérů	exportér	k1gMnPc2	exportér
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
námořní	námořní	k2eAgFnSc6d1	námořní
flotile	flotila	k1gFnSc6	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Disponuje	disponovat	k5eAaBmIp3nS	disponovat
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
moderních	moderní	k2eAgInPc2d1	moderní
přístavů	přístav	k1gInPc2	přístav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
se	se	k3xPyFc4	se
realizuje	realizovat	k5eAaBmIp3nS	realizovat
přes	přes	k7c4	přes
přístavy	přístav	k1gInPc4	přístav
v	v	k7c6	v
sousedních	sousední	k2eAgFnPc6d1	sousední
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
plánuje	plánovat	k5eAaImIp3nS	plánovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
modernizace	modernizace	k1gFnPc4	modernizace
svých	svůj	k3xOyFgInPc2	svůj
přístavů	přístav	k1gInPc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
stavět	stavět	k5eAaImF	stavět
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
kontejnerový	kontejnerový	k2eAgInSc4d1	kontejnerový
terminál	terminál	k1gInSc4	terminál
v	v	k7c4	v
Bremerhavenu	Bremerhaven	k2eAgFnSc4d1	Bremerhaven
<g/>
,	,	kIx,	,
prohlubovat	prohlubovat	k5eAaImF	prohlubovat
koryta	koryto	k1gNnPc4	koryto
řek	řeka	k1gFnPc2	řeka
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
Vezery	Vezera	k1gFnSc2	Vezera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prohloubeném	prohloubený	k2eAgInSc6d1	prohloubený
přístavu	přístav	k1gInSc6	přístav
JadeWeserPort	JadeWeserPort	k1gInSc4	JadeWeserPort
ve	v	k7c4	v
Wilhelmshavenu	Wilhelmshaven	k2eAgFnSc4d1	Wilhelmshaven
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
přistávat	přistávat	k5eAaImF	přistávat
největší	veliký	k2eAgFnPc4d3	veliký
kontejnerové	kontejnerový	k2eAgFnPc4d1	kontejnerová
lodě	loď	k1gFnPc4	loď
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
přístavy	přístav	k1gInPc7	přístav
Německa	Německo	k1gNnSc2	Německo
jsou	být	k5eAaImIp3nP	být
Hamburk	Hamburk	k1gInSc1	Hamburk
<g/>
,	,	kIx,	,
Wilhelmshaven	Wilhelmshaven	k2eAgInSc1d1	Wilhelmshaven
a	a	k8xC	a
Brémy	Brémy	k1gFnPc1	Brémy
s	s	k7c7	s
Bremerhaven	Bremerhavna	k1gFnPc2	Bremerhavna
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
přístavem	přístav	k1gInSc7	přístav
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
Lübeck	Lübeck	k1gInSc1	Lübeck
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
trasy	trasa	k1gFnPc1	trasa
námořní	námořní	k2eAgFnSc2d1	námořní
plavby	plavba	k1gFnSc2	plavba
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
dolním	dolní	k2eAgNnSc6d1	dolní
Labi	Labe	k1gNnSc6	Labe
a	a	k8xC	a
dolní	dolní	k2eAgFnSc6d1	dolní
Vezeře	Vezera	k1gFnSc6	Vezera
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
průplav	průplav	k1gInSc1	průplav
mezi	mezi	k7c7	mezi
Severním	severní	k2eAgNnSc7d1	severní
a	a	k8xC	a
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgNnSc1d1	německé
pobřeží	pobřeží	k1gNnSc1	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
Meklenburském	meklenburský	k2eAgInSc6d1	meklenburský
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejnebezpečnější	bezpečný	k2eNgFnSc7d3	nejnebezpečnější
částí	část	k1gFnSc7	část
baltského	baltský	k2eAgNnSc2d1	Baltské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
říční	říční	k2eAgFnSc1d1	říční
plavba	plavba	k1gFnSc1	plavba
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
hustou	hustý	k2eAgFnSc4d1	hustá
síť	síť	k1gFnSc4	síť
vnitrozemských	vnitrozemský	k2eAgInPc2d1	vnitrozemský
průplavů	průplav	k1gInPc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
splavné	splavný	k2eAgFnPc1d1	splavná
řeky	řeka	k1gFnPc1	řeka
jsou	být	k5eAaImIp3nP	být
Rýn	Rýn	k1gInSc4	Rýn
<g/>
,	,	kIx,	,
Mohan	Mohan	k1gInSc1	Mohan
<g/>
,	,	kIx,	,
Vezera	Vezera	k1gFnSc1	Vezera
a	a	k8xC	a
Labe	Labe	k1gNnSc1	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
vnitrozemské	vnitrozemský	k2eAgInPc1d1	vnitrozemský
říční	říční	k2eAgInPc1d1	říční
kanály	kanál	k1gInPc1	kanál
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
Středoněmecký	středoněmecký	k2eAgInSc1d1	středoněmecký
průplav	průplav	k1gInSc1	průplav
<g/>
,	,	kIx,	,
kanál	kanál	k1gInSc1	kanál
Dortmund-Ems	Dortmund-Ems	k1gInSc1	Dortmund-Ems
<g/>
,	,	kIx,	,
kanál	kanál	k1gInSc1	kanál
Rýn-Herne	Rýn-Hern	k1gInSc5	Rýn-Hern
a	a	k8xC	a
Elbe-Seitenkanal	Elbe-Seitenkanal	k1gFnPc6	Elbe-Seitenkanal
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
Dunaj-Mohan	Dunaj-Mohany	k1gInPc2	Dunaj-Mohany
překračuje	překračovat	k5eAaImIp3nS	překračovat
hlavní	hlavní	k2eAgNnSc4d1	hlavní
evropské	evropský	k2eAgNnSc4d1	Evropské
rozvodí	rozvodí	k1gNnSc4	rozvodí
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
plavbu	plavba	k1gFnSc4	plavba
ze	z	k7c2	z
Severního	severní	k2eAgNnSc2d1	severní
a	a	k8xC	a
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgInPc1d1	říční
přístavy	přístav	k1gInPc1	přístav
v	v	k7c6	v
Duisburgu	Duisburg	k1gInSc6	Duisburg
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Rýna	Rýn	k1gInSc2	Rýn
a	a	k8xC	a
řeky	řeka	k1gFnSc2	řeka
Ruhr	Ruhra	k1gFnPc2	Ruhra
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
obratem	obrat	k1gInSc7	obrat
70	[number]	k4	70
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
zboží	zboží	k1gNnSc2	zboží
ročně	ročně	k6eAd1	ročně
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
říčním	říční	k2eAgInPc3d1	říční
přístavům	přístav	k1gInPc3	přístav
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
obchod	obchod	k1gInSc1	obchod
===	===	k?	===
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
světových	světový	k2eAgMnPc2d1	světový
exportérů	exportér	k1gMnPc2	exportér
a	a	k8xC	a
importérů	importér	k1gMnPc2	importér
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
značného	značný	k2eAgInSc2d1	značný
přebytku	přebytek	k1gInSc2	přebytek
obchodní	obchodní	k2eAgFnSc2d1	obchodní
bilance	bilance	k1gFnSc2	bilance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdůležitějším	důležitý	k2eAgMnSc7d3	nejdůležitější
obchodním	obchodní	k2eAgMnSc7d1	obchodní
partnerem	partner	k1gMnSc7	partner
Německa	Německo	k1gNnSc2	Německo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
německo-čínský	německo-čínský	k2eAgInSc1d1	německo-čínský
obchod	obchod	k1gInSc1	obchod
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
objemu	objem	k1gInSc2	objem
170	[number]	k4	170
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
systém	systém	k1gInSc1	systém
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
organizován	organizován	k2eAgInSc1d1	organizován
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Volitelná	volitelný	k2eAgFnSc1d1	volitelná
je	být	k5eAaImIp3nS	být
docházka	docházka	k1gFnSc1	docházka
do	do	k7c2	do
mateřské	mateřský	k2eAgFnSc2d1	mateřská
školky	školka	k1gFnSc2	školka
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
děti	dítě	k1gFnPc4	dítě
od	od	k7c2	od
tří	tři	k4xCgInPc2	tři
do	do	k7c2	do
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
následuje	následovat	k5eAaImIp3nS	následovat
povinná	povinný	k2eAgFnSc1d1	povinná
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
alespoň	alespoň	k9	alespoň
devíti	devět	k4xCc2	devět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
školství	školství	k1gNnSc1	školství
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
technických	technický	k2eAgFnPc2d1	technická
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
jiných	jiný	k2eAgFnPc2d1	jiná
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
odborných	odborný	k2eAgFnPc2d1	odborná
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
technika	technika	k1gFnSc1	technika
==	==	k?	==
</s>
</p>
<p>
<s>
Institucemi	instituce	k1gFnPc7	instituce
vědeckého	vědecký	k2eAgInSc2d1	vědecký
výzkumu	výzkum	k1gInSc2	výzkum
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
hlavně	hlavně	k9	hlavně
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
technické	technický	k2eAgFnSc2d1	technická
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
odborné	odborný	k2eAgFnSc2d1	odborná
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
jsou	být	k5eAaImIp3nP	být
zařízení	zařízení	k1gNnPc1	zařízení
oprávněná	oprávněný	k2eAgNnPc1d1	oprávněné
vydávat	vydávat	k5eAaImF	vydávat
tituly	titul	k1gInPc4	titul
doktora	doktor	k1gMnSc2	doktor
<g/>
,	,	kIx,	,
profesora	profesor	k1gMnSc2	profesor
a	a	k8xC	a
docenta	docent	k1gMnSc2	docent
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
řada	řada	k1gFnSc1	řada
veřejnoprávních	veřejnoprávní	k2eAgFnPc2d1	veřejnoprávní
a	a	k8xC	a
soukromých	soukromý	k2eAgFnPc2d1	soukromá
výzkumných	výzkumný	k2eAgFnPc2d1	výzkumná
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
veřejnoprávní	veřejnoprávní	k2eAgFnPc4d1	veřejnoprávní
instituce	instituce	k1gFnPc4	instituce
patří	patřit	k5eAaImIp3nS	patřit
Deutsche	Deutsche	k1gInSc1	Deutsche
Forschungsgesellschaft	Forschungsgesellschaft	k2eAgInSc1d1	Forschungsgesellschaft
a	a	k8xC	a
Frauenhofer-Institut	Frauenhofer-Institut	k2eAgInSc1d1	Frauenhofer-Institut
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumem	výzkum	k1gInSc7	výzkum
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
oborech	obor	k1gInPc6	obor
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
také	také	k9	také
velké	velký	k2eAgInPc4d1	velký
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
koncerny	koncern	k1gInPc4	koncern
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
automobilky	automobilka	k1gFnPc1	automobilka
nebo	nebo	k8xC	nebo
chemické	chemický	k2eAgFnPc1d1	chemická
společnosti	společnost	k1gFnPc1	společnost
(	(	kIx(	(
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
např.	např.	kA	např.
Spolkový	spolkový	k2eAgInSc1d1	spolkový
svaz	svaz	k1gInSc1	svaz
výzkumných	výzkumný	k2eAgMnPc2d1	výzkumný
výrobců	výrobce	k1gMnPc2	výrobce
léčiv	léčivo	k1gNnPc2	léčivo
-	-	kIx~	-
Bundesverband	Bundesverband	k1gInSc1	Bundesverband
Forschender	Forschendra	k1gFnPc2	Forschendra
Arzneimittelhersteller	Arzneimittelhersteller	k1gInSc4	Arzneimittelhersteller
s	s	k7c7	s
cca	cca	kA	cca
90	[number]	k4	90
členskými	členský	k2eAgFnPc7d1	členská
firmami	firma	k1gFnPc7	firma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
vědci	vědec	k1gMnPc1	vědec
jsou	být	k5eAaImIp3nP	být
častými	častý	k2eAgMnPc7d1	častý
laureáty	laureát	k1gMnPc7	laureát
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
získali	získat	k5eAaPmAgMnP	získat
Peter	Peter	k1gMnSc1	Peter
Grünberg	Grünberg	k1gMnSc1	Grünberg
<g/>
,	,	kIx,	,
Theodor	Theodor	k1gMnSc1	Theodor
W.	W.	kA	W.
Hänsch	Hänsch	k1gMnSc1	Hänsch
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Ketterle	Ketterle	k1gFnSc2	Ketterle
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
Kroemer	Kroemer	k1gMnSc1	Kroemer
<g/>
,	,	kIx,	,
Horst	Horst	k1gMnSc1	Horst
L.	L.	kA	L.
Störmer	Störmer	k1gMnSc1	Störmer
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Georg	Georg	k1gMnSc1	Georg
Dehmelt	Dehmelt	k1gMnSc1	Dehmelt
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Steinberger	Steinberger	k1gMnSc1	Steinberger
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
Georg	Georg	k1gMnSc1	Georg
Bednorz	Bednorz	k1gMnSc1	Bednorz
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Gerd	Gerd	k1gMnSc1	Gerd
<g />
.	.	kIx.	.
</s>
<s>
Binnig	Binnig	k1gMnSc1	Binnig
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
von	von	k1gInSc1	von
Klitzing	Klitzing	k1gInSc1	Klitzing
<g/>
,	,	kIx,	,
Arno	Arno	k1gMnSc1	Arno
Allan	Allan	k1gMnSc1	Allan
Penzias	Penzias	k1gMnSc1	Penzias
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Bethe	Bethe	k1gFnSc1	Bethe
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
Göppert-Mayerová	Göppert-Mayerová	k1gFnSc1	Göppert-Mayerová
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Hans	Hans	k1gMnSc1	Hans
D.	D.	kA	D.
Jensen	Jensen	k1gInSc1	Jensen
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Ludwig	Ludwig	k1gMnSc1	Ludwig
Mössbauer	Mössbauer	k1gMnSc1	Mössbauer
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Born	Borna	k1gFnPc2	Borna
<g/>
,	,	kIx,	,
Walther	Walthra	k1gFnPc2	Walthra
Bothe	Both	k1gInSc2	Both
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Stern	sternum	k1gNnPc2	sternum
<g/>
,	,	kIx,	,
Werner	Werner	k1gMnSc1	Werner
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Franck	Franck	k1gMnSc1	Franck
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Ludwig	Ludwig	k1gMnSc1	Ludwig
Hertz	hertz	k1gInSc1	hertz
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
Stark	Stark	k1gInSc1	Stark
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Planck	Planck	k1gMnSc1	Planck
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
von	von	k1gInSc4	von
Laue	Laue	k1gNnSc2	Laue
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Wien	Wien	k1gMnSc1	Wien
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Braun	Braun	k1gMnSc1	Braun
<g/>
,	,	kIx,	,
Philipp	Philipp	k1gMnSc1	Philipp
Lenard	Lenard	k1gMnSc1	Lenard
a	a	k8xC	a
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Conrad	Conrada	k1gFnPc2	Conrada
Röntgen	Röntgen	k1gInSc1	Röntgen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Stefan	Stefan	k1gMnSc1	Stefan
Hell	Hell	k1gMnSc1	Hell
<g/>
,	,	kIx,	,
Gerhard	Gerhard	k1gMnSc1	Gerhard
Ertl	Ertl	k1gMnSc1	Ertl
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Deisenhofer	Deisenhofer	k1gMnSc1	Deisenhofer
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Huber	Huber	k1gMnSc1	Huber
<g/>
,	,	kIx,	,
Hartmut	Hartmut	k1gMnSc1	Hartmut
Michel	Michel	k1gMnSc1	Michel
<g/>
,	,	kIx,	,
Georg	Georg	k1gMnSc1	Georg
Wittig	Wittig	k1gMnSc1	Wittig
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Otto	Otto	k1gMnSc1	Otto
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Gerhard	Gerhard	k1gMnSc1	Gerhard
Herzberg	Herzberg	k1gMnSc1	Herzberg
<g/>
,	,	kIx,	,
Manfred	Manfred	k1gMnSc1	Manfred
Eigen	Eigen	k1gInSc1	Eigen
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Ziegler	Ziegler	k1gMnSc1	Ziegler
<g/>
,	,	kIx,	,
Hermann	Hermann	k1gMnSc1	Hermann
Staudinger	Staudinger	k1gMnSc1	Staudinger
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Diels	Dielsa	k1gFnPc2	Dielsa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Alder	Alder	k1gMnSc1	Alder
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Hahn	Hahn	k1gMnSc1	Hahn
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Butenandt	Butenandt	k1gMnSc1	Butenandt
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Kuhn	Kuhn	k1gMnSc1	Kuhn
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Bosch	Bosch	kA	Bosch
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Bergius	Bergius	k1gMnSc1	Bergius
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
von	von	k1gInSc4	von
Euler-Chelpin	Euler-Chelpin	k1gMnSc1	Euler-Chelpin
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Otto	Otto	k1gMnSc1	Otto
Reinhold	Reinhold	k1gMnSc1	Reinhold
Windaus	Windaus	k1gMnSc1	Windaus
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
Otto	Otto	k1gMnSc1	Otto
Wieland	Wielanda	k1gFnPc2	Wielanda
<g/>
,	,	kIx,	,
Walther	Walthra	k1gFnPc2	Walthra
Nernst	Nernst	k1gMnSc1	Nernst
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Haber	Haber	k1gMnSc1	Haber
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Willstätter	Willstätter	k1gMnSc1	Willstätter
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Wallach	Wallach	k1gMnSc1	Wallach
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Ostwald	Ostwald	k1gMnSc1	Ostwald
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Buchner	Buchner	k1gMnSc1	Buchner
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
von	von	k1gInSc4	von
Baeyer	Baeyer	k1gMnSc1	Baeyer
a	a	k8xC	a
Hermann	Hermann	k1gMnSc1	Hermann
Emil	Emil	k1gMnSc1	Emil
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
nebo	nebo	k8xC	nebo
lékařství	lékařství	k1gNnSc4	lékařství
Thomas	Thomas	k1gMnSc1	Thomas
C.	C.	kA	C.
Südhof	Südhof	k1gMnSc1	Südhof
<g/>
,	,	kIx,	,
Harald	Harald	k1gMnSc1	Harald
zur	zur	k?	zur
Hausen	Hausen	k1gInSc1	Hausen
<g/>
,	,	kIx,	,
Günter	Günter	k1gMnSc1	Günter
Blobel	Blobel	k1gMnSc1	Blobel
<g/>
,	,	kIx,	,
Christiane	Christian	k1gMnSc5	Christian
Nüsslein-Volhard	Nüsslein-Volharda	k1gFnPc2	Nüsslein-Volharda
<g/>
,	,	kIx,	,
Bert	Berta	k1gFnPc2	Berta
Sakmann	Sakmanna	k1gFnPc2	Sakmanna
<g/>
,	,	kIx,	,
Erwin	Erwin	k1gMnSc1	Erwin
Neher	Neher	k1gMnSc1	Neher
<g/>
,	,	kIx,	,
Georges	Georges	k1gMnSc1	Georges
Jean	Jean	k1gMnSc1	Jean
Franz	Franz	k1gMnSc1	Franz
Köhler	Köhler	k1gMnSc1	Köhler
<g/>
,	,	kIx,	,
Bernard	Bernard	k1gMnSc1	Bernard
Katz	Katz	k1gMnSc1	Katz
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Delbrück	Delbrück	k1gMnSc1	Delbrück
<g/>
,	,	kIx,	,
Feodor	Feodor	k1gMnSc1	Feodor
Felix	Felix	k1gMnSc1	Felix
Konrad	Konrada	k1gFnPc2	Konrada
Lynen	Lynen	k1gInSc1	Lynen
<g/>
,	,	kIx,	,
Konrad	Konrad	k1gInSc1	Konrad
<g />
.	.	kIx.	.
</s>
<s>
Bloch	Bloch	k1gMnSc1	Bloch
<g/>
,	,	kIx,	,
Werner	Werner	k1gMnSc1	Werner
Forssmann	Forssmann	k1gMnSc1	Forssmann
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Albert	Albert	k1gMnSc1	Albert
Lipmann	Lipmann	k1gMnSc1	Lipmann
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Adolf	Adolf	k1gMnSc1	Adolf
Krebs	Krebs	k1gInSc1	Krebs
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Boris	Boris	k1gMnSc1	Boris
Chain	Chain	k1gMnSc1	Chain
<g/>
,	,	kIx,	,
Gerhard	Gerhard	k1gMnSc1	Gerhard
Domagk	Domagk	k1gMnSc1	Domagk
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Spemann	Spemann	k1gMnSc1	Spemann
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Heinrich	Heinrich	k1gMnSc1	Heinrich
Warburg	Warburg	k1gMnSc1	Warburg
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Fritz	Fritz	k1gMnSc1	Fritz
Meyerhof	Meyerhof	k1gMnSc1	Meyerhof
<g/>
,	,	kIx,	,
Albrecht	Albrecht	k1gMnSc1	Albrecht
Kossel	Kossel	k1gMnSc1	Kossel
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Ehrlich	Ehrlich	k1gMnSc1	Ehrlich
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Koch	Koch	k1gMnSc1	Koch
a	a	k8xC	a
Emil	Emil	k1gMnSc1	Emil
Adolf	Adolf	k1gMnSc1	Adolf
von	von	k1gInSc4	von
Behring	Behring	k1gInSc1	Behring
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropskou	evropský	k2eAgFnSc4d1	Evropská
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
civilizaci	civilizace	k1gFnSc4	civilizace
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
ale	ale	k8xC	ale
i	i	k9	i
jiní	jiný	k2eAgMnPc1d1	jiný
badatelé	badatel	k1gMnPc1	badatel
<g/>
.	.	kIx.	.
</s>
<s>
Zásadně	zásadně	k6eAd1	zásadně
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaPmAgInS	učinit
vynález	vynález	k1gInSc4	vynález
knihtisku	knihtisk	k1gInSc2	knihtisk
Johannese	Johannese	k1gFnSc1	Johannese
Gutenberga	Gutenberga	k1gFnSc1	Gutenberga
<g/>
.	.	kIx.	.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hertz	hertz	k1gInSc4	hertz
umožnil	umožnit	k5eAaPmAgInS	umožnit
svými	svůj	k3xOyFgInPc7	svůj
objevy	objev	k1gInPc7	objev
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
elektromagnetismu	elektromagnetismus	k1gInSc2	elektromagnetismus
vznik	vznik	k1gInSc4	vznik
přístrojů	přístroj	k1gInPc2	přístroj
pro	pro	k7c4	pro
bezdrátové	bezdrátový	k2eAgNnSc4d1	bezdrátové
spojení	spojení	k1gNnSc4	spojení
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Gauss	gauss	k1gInSc4	gauss
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Eduardem	Eduard	k1gMnSc7	Eduard
Weberem	Weber	k1gMnSc7	Weber
poté	poté	k6eAd1	poté
i	i	k9	i
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
elektromagnetický	elektromagnetický	k2eAgInSc4d1	elektromagnetický
telegraf	telegraf	k1gInSc4	telegraf
<g/>
.	.	kIx.	.
</s>
<s>
Otcem	otec	k1gMnSc7	otec
moderní	moderní	k2eAgFnSc2d1	moderní
astronomie	astronomie	k1gFnSc2	astronomie
je	být	k5eAaImIp3nS	být
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Kirchhoff	Kirchhoff	k1gMnSc1	Kirchhoff
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
tvůrců	tvůrce	k1gMnPc2	tvůrce
spektrální	spektrální	k2eAgFnSc2d1	spektrální
analýzy	analýza	k1gFnSc2	analýza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
určit	určit	k5eAaPmF	určit
složení	složení	k1gNnSc4	složení
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
a	a	k8xC	a
definoval	definovat	k5eAaBmAgMnS	definovat
tzv.	tzv.	kA	tzv.
černé	černý	k2eAgNnSc1d1	černé
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Nicolaus	Nicolaus	k1gMnSc1	Nicolaus
Otto	Otto	k1gMnSc1	Otto
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
zážehový	zážehový	k2eAgInSc4d1	zážehový
čtyřtaktní	čtyřtaktní	k2eAgInSc4d1	čtyřtaktní
motor	motor	k1gInSc4	motor
(	(	kIx(	(
<g/>
benzínový	benzínový	k2eAgMnSc1d1	benzínový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Diesel	diesel	k1gInSc4	diesel
dieslový	dieslový	k2eAgInSc4d1	dieslový
motor	motor	k1gInSc4	motor
<g/>
,	,	kIx,	,
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Daimler	Daimler	k1gInSc4	Daimler
vysokorychlostní	vysokorychlostní	k2eAgInSc1d1	vysokorychlostní
benzínový	benzínový	k2eAgInSc1d1	benzínový
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Benz	Benz	k1gMnSc1	Benz
první	první	k4xOgInSc4	první
benzínový	benzínový	k2eAgInSc4d1	benzínový
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
raketové	raketový	k2eAgFnSc2d1	raketová
techniky	technika	k1gFnSc2	technika
zásadně	zásadně	k6eAd1	zásadně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
Wernher	Wernhra	k1gFnPc2	Wernhra
von	von	k1gInSc1	von
Braun	Braun	k1gMnSc1	Braun
<g/>
.	.	kIx.	.
</s>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
von	von	k1gInSc1	von
Humboldt	Humboldt	k1gMnSc1	Humboldt
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
přírodovědcům	přírodovědec	k1gMnPc3	přírodovědec
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
zakladatelům	zakladatel	k1gMnPc3	zakladatel
geografie	geografie	k1gFnSc2	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
zákony	zákon	k1gInPc1	zákon
elektřiny	elektřina	k1gFnSc2	elektřina
objevil	objevit	k5eAaPmAgMnS	objevit
Georg	Georg	k1gMnSc1	Georg
Simon	Simon	k1gMnSc1	Simon
Ohm	ohm	k1gInSc4	ohm
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Herschel	Herschela	k1gFnPc2	Herschela
objevil	objevit	k5eAaPmAgInS	objevit
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
a	a	k8xC	a
matematice	matematika	k1gFnSc6	matematika
vyvolali	vyvolat	k5eAaPmAgMnP	vyvolat
Bernhard	Bernhard	k1gMnSc1	Bernhard
Riemann	Riemann	k1gMnSc1	Riemann
a	a	k8xC	a
Georg	Georg	k1gMnSc1	Georg
Cantor	Cantor	k1gMnSc1	Cantor
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
matematikem	matematik	k1gMnSc7	matematik
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jacob	Jacoba	k1gFnPc2	Jacoba
Jacobi	Jacob	k1gFnSc2	Jacob
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Hilbert	Hilbert	k1gMnSc1	Hilbert
<g/>
,	,	kIx,	,
Leopold	Leopold	k1gMnSc1	Leopold
Kronecker	Kronecker	k1gMnSc1	Kronecker
nebo	nebo	k8xC	nebo
Hermann	Hermann	k1gMnSc1	Hermann
Weyl	Weyl	k1gMnSc1	Weyl
<g/>
.	.	kIx.	.
</s>
<s>
Evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
Ernst	Ernst	k1gMnSc1	Ernst
Haeckel	Haeckel	k1gMnSc1	Haeckel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Ernst	Ernst	k1gMnSc1	Ernst
Mayr	Mayr	k1gMnSc1	Mayr
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
tornád	tornádo	k1gNnPc2	tornádo
předložil	předložit	k5eAaPmAgMnS	předložit
meteorolog	meteorolog	k1gMnSc1	meteorolog
a	a	k8xC	a
geolog	geolog	k1gMnSc1	geolog
Alfred	Alfred	k1gMnSc1	Alfred
Wegener	Wegener	k1gMnSc1	Wegener
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Gabriel	Gabriel	k1gMnSc1	Gabriel
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
proslul	proslout	k5eAaPmAgInS	proslout
výzkumem	výzkum	k1gInSc7	výzkum
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Bunsen	Bunsen	k1gInSc4	Bunsen
vynálezem	vynález	k1gInSc7	vynález
řady	řada	k1gFnSc2	řada
laboratorních	laboratorní	k2eAgInPc2d1	laboratorní
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wöhler	Wöhler	k1gMnSc1	Wöhler
syntézou	syntéza	k1gFnSc7	syntéza
močoviny	močovina	k1gFnPc1	močovina
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
postavou	postava	k1gFnSc7	postava
fyziky	fyzika	k1gFnSc2	fyzika
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
Hermann	Hermann	k1gMnSc1	Hermann
von	von	k1gInSc4	von
Helmholtz	Helmholtza	k1gFnPc2	Helmholtza
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Clausius	Clausius	k1gMnSc1	Clausius
byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
termodynamiky	termodynamika	k1gFnSc2	termodynamika
<g/>
.	.	kIx.	.
</s>
<s>
Justus	Justus	k1gInSc1	Justus
von	von	k1gInSc1	von
Liebig	Liebig	k1gInSc1	Liebig
založil	založit	k5eAaPmAgInS	založit
výrobu	výroba	k1gFnSc4	výroba
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Olbers	Olbers	k1gInSc4	Olbers
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
zaniklé	zaniklý	k2eAgFnSc6d1	zaniklá
planetě	planeta	k1gFnSc6	planeta
Phaeton	Phaeton	k1gInSc4	Phaeton
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
už	už	k6eAd1	už
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
málokdo	málokdo	k3yInSc1	málokdo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
Olbersův	Olbersův	k2eAgInSc1d1	Olbersův
paradox	paradox	k1gInSc1	paradox
zásadně	zásadně	k6eAd1	zásadně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
moderní	moderní	k2eAgFnSc4d1	moderní
kosmologii	kosmologie	k1gFnSc4	kosmologie
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
informatiky	informatika	k1gFnSc2	informatika
byl	být	k5eAaImAgMnS	být
Konrád	Konrád	k1gMnSc1	Konrád
Zuse	Zus	k1gInSc2	Zus
<g/>
.	.	kIx.	.
</s>
<s>
Emmy	Emma	k1gFnPc1	Emma
Noetherová	Noetherová	k1gFnSc1	Noetherová
byla	být	k5eAaImAgFnS	být
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
matematičkou	matematička	k1gFnSc7	matematička
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
teorii	teorie	k1gFnSc4	teorie
čísel	číslo	k1gNnPc2	číslo
i	i	k9	i
matematickou	matematický	k2eAgFnSc4d1	matematická
statistiku	statistika	k1gFnSc4	statistika
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
Johann	Johann	k1gMnSc1	Johann
Peter	Peter	k1gMnSc1	Peter
Gustav	Gustav	k1gMnSc1	Gustav
Lejeune	Lejeun	k1gInSc5	Lejeun
Dirichlet	Dirichlet	k1gInSc1	Dirichlet
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Dedekind	Dedekind	k1gMnSc1	Dedekind
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
množinu	množina	k1gFnSc4	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
Klein	Klein	k1gMnSc1	Klein
revolučním	revoluční	k2eAgInSc7d1	revoluční
způsobem	způsob	k1gInSc7	způsob
počal	počnout	k5eAaPmAgInS	počnout
propojovat	propojovat	k5eAaImF	propojovat
geometrii	geometrie	k1gFnSc4	geometrie
s	s	k7c7	s
algebrou	algebra	k1gFnSc7	algebra
<g/>
,	,	kIx,	,
za	za	k7c2	za
otce	otec	k1gMnSc2	otec
moderní	moderní	k2eAgFnSc2d1	moderní
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
Karl	Karl	k1gInSc1	Karl
Weierstrass	Weierstrassa	k1gFnPc2	Weierstrassa
<g/>
.	.	kIx.	.
</s>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Karl	Karl	k1gMnSc1	Karl
Abbe	Abbe	k1gFnSc4	Abbe
<g/>
,	,	kIx,	,
Carl	Carl	k1gInSc4	Carl
Zeiss	Zeissa	k1gFnPc2	Zeissa
a	a	k8xC	a
Joseph	Josepha	k1gFnPc2	Josepha
von	von	k1gInSc1	von
Fraunhofer	Fraunhofer	k1gMnSc1	Fraunhofer
založili	založit	k5eAaPmAgMnP	založit
moderní	moderní	k2eAgFnSc4d1	moderní
optiku	optika	k1gFnSc4	optika
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Bessel	Bessel	k1gMnSc1	Bessel
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
změřil	změřit	k5eAaPmAgMnS	změřit
paralaxu	paralaxa	k1gFnSc4	paralaxa
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
spočítal	spočítat	k5eAaPmAgMnS	spočítat
její	její	k3xOp3gFnSc4	její
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Georgius	Georgius	k1gInSc1	Georgius
Agricola	Agricola	k1gFnSc1	Agricola
je	být	k5eAaImIp3nS	být
vnímán	vnímán	k2eAgMnSc1d1	vnímán
jako	jako	k8xC	jako
zakladatel	zakladatel	k1gMnSc1	zakladatel
mineralogie	mineralogie	k1gFnSc2	mineralogie
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
von	von	k1gInSc4	von
Guericke	Guericke	k1gNnSc2	Guericke
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
vývěvu	vývěva	k1gFnSc4	vývěva
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
August	August	k1gMnSc1	August
Kekulé	Kekulý	k2eAgFnSc2d1	Kekulý
popsal	popsat	k5eAaPmAgMnS	popsat
zákonitosti	zákonitost	k1gFnPc4	zákonitost
řetězení	řetězení	k1gNnSc2	řetězení
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Gottfried	Gottfried	k1gMnSc1	Gottfried
Galle	Gall	k1gMnSc4	Gall
objevil	objevit	k5eAaPmAgMnS	objevit
planetu	planeta	k1gFnSc4	planeta
Neptun	Neptun	k1gMnSc1	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
August	August	k1gMnSc1	August
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Möbius	Möbius	k1gMnSc1	Möbius
je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
topologie	topologie	k1gFnSc2	topologie
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
buněk	buňka	k1gFnPc2	buňka
významně	významně	k6eAd1	významně
posunuli	posunout	k5eAaPmAgMnP	posunout
kupředu	kupředu	k6eAd1	kupředu
Rudolf	Rudolf	k1gMnSc1	Rudolf
Virchow	Virchow	k1gMnSc1	Virchow
a	a	k8xC	a
Matthias	Matthias	k1gMnSc1	Matthias
Jacob	Jacoba	k1gFnPc2	Jacoba
Schleiden	Schleidna	k1gFnPc2	Schleidna
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Hahnemann	Hahnemann	k1gMnSc1	Hahnemann
založil	založit	k5eAaPmAgMnS	založit
stále	stále	k6eAd1	stále
diskutovanou	diskutovaný	k2eAgFnSc4d1	diskutovaná
homeopatii	homeopatie	k1gFnSc4	homeopatie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
praotce	praotec	k1gMnPc4	praotec
kvantové	kvantový	k2eAgFnSc2d1	kvantová
fyziky	fyzika	k1gFnSc2	fyzika
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
Arnolda	Arnold	k1gMnSc2	Arnold
Sommerfelda	Sommerfeld	k1gMnSc2	Sommerfeld
<g/>
.	.	kIx.	.
</s>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Geiger	Geiger	k1gMnSc1	Geiger
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
první	první	k4xOgMnSc1	první
měřič	měřič	k1gMnSc1	měřič
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
astrofotografie	astrofotografie	k1gFnSc2	astrofotografie
byl	být	k5eAaImAgMnS	být
Max	Max	k1gMnSc1	Max
Wolf	Wolf	k1gMnSc1	Wolf
<g/>
.	.	kIx.	.
</s>
<s>
Emile	Emil	k1gMnSc5	Emil
Berliner	Berliner	k1gMnSc1	Berliner
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
mikrofon	mikrofon	k1gInSc4	mikrofon
a	a	k8xC	a
gramofon	gramofon	k1gInSc4	gramofon
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
kartografem	kartograf	k1gMnSc7	kartograf
byl	být	k5eAaImAgMnS	být
Martin	Martin	k1gMnSc1	Martin
Waldseemüller	Waldseemüller	k1gMnSc1	Waldseemüller
<g/>
,	,	kIx,	,
všestranností	všestrannost	k1gFnSc7	všestrannost
vpravdě	vpravdě	k9	vpravdě
renesanční	renesanční	k2eAgFnSc4d1	renesanční
vynikl	vyniknout	k5eAaPmAgMnS	vyniknout
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
učenec	učenec	k1gMnSc1	učenec
Athanasius	Athanasius	k1gMnSc1	Athanasius
Kircher	Kirchra	k1gFnPc2	Kirchra
<g/>
.	.	kIx.	.
</s>
<s>
Fyzik	fyzik	k1gMnSc1	fyzik
Karl	Karl	k1gMnSc1	Karl
Schwarzschild	Schwarzschild	k1gMnSc1	Schwarzschild
sehrál	sehrát	k5eAaPmAgMnS	sehrát
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Theodor	Theodor	k1gMnSc1	Theodor
Schwann	Schwann	k1gMnSc1	Schwann
zavedl	zavést	k5eAaPmAgMnS	zavést
pojem	pojem	k1gInSc4	pojem
metabolismus	metabolismus	k1gInSc1	metabolismus
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
konstruktér	konstruktér	k1gMnSc1	konstruktér
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
vynikl	vyniknout	k5eAaPmAgMnS	vyniknout
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
von	von	k1gInSc4	von
Zeppelin	Zeppelin	k2eAgInSc4d1	Zeppelin
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
konstruktér	konstruktér	k1gMnSc1	konstruktér
automobilový	automobilový	k2eAgMnSc1d1	automobilový
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Porsche	Porsche	k1gNnSc2	Porsche
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
průmysl	průmysl	k1gInSc1	průmysl
zásadně	zásadně	k6eAd1	zásadně
pozdvihl	pozdvihnout	k5eAaPmAgInS	pozdvihnout
též	též	k9	též
Ernst	Ernst	k1gMnSc1	Ernst
Werner	Werner	k1gMnSc1	Werner
von	von	k1gInSc4	von
Siemens	siemens	k1gInSc1	siemens
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
zakladatel	zakladatel	k1gMnSc1	zakladatel
americké	americký	k2eAgFnSc2d1	americká
firmy	firma	k1gFnSc2	firma
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
džín	džíny	k1gFnPc2	džíny
Levi	Lev	k1gFnSc2	Lev
Strauss	Straussa	k1gFnPc2	Straussa
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
Lilienthal	Lilienthal	k1gMnSc1	Lilienthal
byl	být	k5eAaImAgMnS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
letectví	letectví	k1gNnSc2	letectví
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Philipp	Philipp	k1gMnSc1	Philipp
Reis	Reis	k1gInSc4	Reis
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
typů	typ	k1gInPc2	typ
telefonu	telefon	k1gInSc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Tradici	tradice	k1gFnSc4	tradice
matematiky	matematika	k1gFnSc2	matematika
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
zakládali	zakládat	k5eAaImAgMnP	zakládat
lidé	člověk	k1gMnPc1	člověk
jako	jako	k8xS	jako
Adam	Adam	k1gMnSc1	Adam
Ries	Ries	k1gInSc1	Ries
nebo	nebo	k8xC	nebo
Regiomontanus	Regiomontanus	k1gInSc1	Regiomontanus
(	(	kIx(	(
<g/>
Johannes	Johannes	k1gMnSc1	Johannes
Müller	Müller	k1gMnSc1	Müller
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prestižní	prestižní	k2eAgFnSc4d1	prestižní
Fieldsovu	Fieldsův	k2eAgFnSc4d1	Fieldsova
medaili	medaile	k1gFnSc4	medaile
získali	získat	k5eAaPmAgMnP	získat
matematici	matematik	k1gMnPc1	matematik
Gerd	Gerdo	k1gNnPc2	Gerdo
Faltings	Faltingsa	k1gFnPc2	Faltingsa
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Scholze	Scholze	k1gFnSc2	Scholze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
humanitních	humanitní	k2eAgFnPc2d1	humanitní
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
se	se	k3xPyFc4	se
Němci	Němec	k1gMnPc1	Němec
výrazně	výrazně	k6eAd1	výrazně
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
zejména	zejména	k9	zejména
ve	v	k7c4	v
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Představitelem	představitel	k1gMnSc7	představitel
metafyziky	metafyzika	k1gFnSc2	metafyzika
byl	být	k5eAaImAgMnS	být
Mistr	mistr	k1gMnSc1	mistr
Eckhart	Eckharta	k1gFnPc2	Eckharta
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
německých	německý	k2eAgMnPc2d1	německý
scholastikem	scholastik	k1gMnSc7	scholastik
byl	být	k5eAaImAgMnS	být
Albert	Albert	k1gMnSc1	Albert
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Renesančním	renesanční	k2eAgMnSc7d1	renesanční
myslitelem	myslitel	k1gMnSc7	myslitel
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Kusánský	Kusánský	k2eAgInSc1d1	Kusánský
<g/>
.	.	kIx.	.
</s>
<s>
Gottfried	Gottfried	k1gMnSc1	Gottfried
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Leibniz	Leibniz	k1gMnSc1	Leibniz
představil	představit	k5eAaPmAgMnS	představit
klasickou	klasický	k2eAgFnSc4d1	klasická
ontologii	ontologie	k1gFnSc4	ontologie
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
zhusta	zhusta	k6eAd1	zhusta
reagováno	reagován	k2eAgNnSc1d1	reagováno
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Heinrich	Heinrich	k1gMnSc1	Heinrich
Dietrich	Dietricha	k1gFnPc2	Dietricha
von	von	k1gInSc1	von
Holbach	Holbach	k1gMnSc1	Holbach
je	být	k5eAaImIp3nS	být
klasikem	klasik	k1gMnSc7	klasik
filozofického	filozofický	k2eAgInSc2d1	filozofický
materialismu	materialismus	k1gInSc2	materialismus
a	a	k8xC	a
ateismu	ateismus	k1gInSc2	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholnou	vrcholný	k2eAgFnSc7d1	vrcholná
érou	éra	k1gFnSc7	éra
německé	německý	k2eAgFnSc2d1	německá
filozofie	filozofie	k1gFnSc2	filozofie
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Nietzsche	Nietzsch	k1gFnSc2	Nietzsch
<g/>
,	,	kIx,	,
Georg	Georg	k1gMnSc1	Georg
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Friedrich	Friedrich	k1gMnSc1	Friedrich
Hegel	Hegel	k1gMnSc1	Hegel
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Schopenhauer	Schopenhauer	k1gMnSc1	Schopenhauer
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Fichte	Ficht	k1gInSc5	Ficht
<g/>
,	,	kIx,	,
Ludwig	Ludwig	k1gMnSc1	Ludwig
Feuerbach	Feuerbach	k1gMnSc1	Feuerbach
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Gottfried	Gottfried	k1gMnSc1	Gottfried
Herder	Herdra	k1gFnPc2	Herdra
<g/>
,	,	kIx,	,
Gottlob	Gottloba	k1gFnPc2	Gottloba
Frege	Freg	k1gInSc2	Freg
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Joseph	Joseph	k1gMnSc1	Joseph
Schelling	Schelling	k1gInSc1	Schelling
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Stirner	Stirner	k1gMnSc1	Stirner
nebo	nebo	k8xC	nebo
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Dilthey	Dilthea	k1gFnSc2	Dilthea
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
filozofům	filozof	k1gMnPc3	filozof
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
teoretiky	teoretik	k1gMnPc7	teoretik
socialismu	socialismus	k1gInSc2	socialismus
byli	být	k5eAaImAgMnP	být
Karl	Karl	k1gMnSc1	Karl
Marx	Marx	k1gMnSc1	Marx
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Engels	Engels	k1gMnSc1	Engels
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Lassalle	Lassalle	k1gFnSc2	Lassalle
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Bernstein	Bernstein	k1gMnSc1	Bernstein
a	a	k8xC	a
Rosa	Rosa	k1gFnSc1	Rosa
Luxemburgová	Luxemburgový	k2eAgFnSc1d1	Luxemburgová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
školou	škola	k1gFnSc7	škola
fenomenologie	fenomenologie	k1gFnSc2	fenomenologie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
Martin	Martin	k2eAgMnSc1d1	Martin
Heidegger	Heidegger	k1gMnSc1	Heidegger
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
Husserl	Husserl	k1gMnSc1	Husserl
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
ovšem	ovšem	k9	ovšem
také	také	k6eAd1	také
považovat	považovat	k5eAaImF	považovat
za	za	k7c2	za
Rakušana	Rakušan	k1gMnSc2	Rakušan
<g/>
,	,	kIx,	,
neřkuli	neřkuli	k9	neřkuli
za	za	k7c4	za
Čecha	Čech	k1gMnSc4	Čech
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Scheler	Scheler	k1gMnSc1	Scheler
či	či	k8xC	či
Hannah	Hannah	k1gMnSc1	Hannah
Arendtová	Arendtová	k1gFnSc1	Arendtová
<g/>
,	,	kIx,	,
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Frankurtská	Frankurtský	k2eAgFnSc1d1	Frankurtský
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
představovali	představovat	k5eAaImAgMnP	představovat
Jürgen	Jürgen	k1gInSc4	Jürgen
Habermas	Habermasa	k1gFnPc2	Habermasa
<g/>
,	,	kIx,	,
Theodor	Theodora	k1gFnPc2	Theodora
Adorno	Adorno	k1gNnSc1	Adorno
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
Fromm	Fromm	k1gMnSc1	Fromm
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
Benjamin	Benjamin	k1gMnSc1	Benjamin
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
Marcuse	Marcuse	k1gFnSc2	Marcuse
a	a	k8xC	a
Max	Max	k1gMnSc1	Max
Horkheimer	Horkheimer	k1gMnSc1	Horkheimer
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
existencialistům	existencialista	k1gMnPc3	existencialista
patřil	patřit	k5eAaImAgInS	patřit
Karl	Karl	k1gInSc1	Karl
Jaspers	Jaspers	k1gInSc4	Jaspers
<g/>
,	,	kIx,	,
k	k	k7c3	k
hermeneutice	hermeneutika	k1gFnSc3	hermeneutika
Hans-Georg	Hans-Georg	k1gMnSc1	Hans-Georg
Gadamer	Gadamer	k1gMnSc1	Gadamer
<g/>
,	,	kIx,	,
k	k	k7c3	k
analytické	analytický	k2eAgFnSc3d1	analytická
filozofii	filozofie	k1gFnSc3	filozofie
Rudolf	Rudolf	k1gMnSc1	Rudolf
Carnap	Carnap	k1gMnSc1	Carnap
<g/>
.	.	kIx.	.
</s>
<s>
Novokantovský	novokantovský	k2eAgInSc1d1	novokantovský
přístup	přístup	k1gInSc1	přístup
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozvíjel	rozvíjet	k5eAaImAgMnS	rozvíjet
Ernst	Ernst	k1gMnSc1	Ernst
Cassirer	Cassirer	k1gMnSc1	Cassirer
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Daniel	Daniel	k1gMnSc1	Daniel
Ernst	Ernst	k1gMnSc1	Ernst
Schleiermacher	Schleiermachra	k1gFnPc2	Schleiermachra
byl	být	k5eAaImAgMnS	být
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
teologem	teolog	k1gMnSc7	teolog
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Dietrich	Dietrich	k1gMnSc1	Dietrich
Bonhoeffer	Bonhoeffer	k1gInSc4	Bonhoeffer
století	století	k1gNnSc2	století
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
sociologům	sociolog	k1gMnPc3	sociolog
světa	svět	k1gInSc2	svět
patří	patřit	k5eAaImIp3nS	patřit
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Tönnies	Tönnies	k1gMnSc1	Tönnies
<g/>
,	,	kIx,	,
Georg	Georg	k1gMnSc1	Georg
Simmel	Simmel	k1gMnSc1	Simmel
<g/>
,	,	kIx,	,
Norbert	Norbert	k1gMnSc1	Norbert
Elias	Elias	k1gMnSc1	Elias
<g/>
,	,	kIx,	,
Werner	Werner	k1gMnSc1	Werner
Sombart	Sombart	k1gInSc1	Sombart
a	a	k8xC	a
Ulrich	Ulrich	k1gMnSc1	Ulrich
Beck	Beck	k1gMnSc1	Beck
<g/>
.	.	kIx.	.
</s>
<s>
Zakladateli	zakladatel	k1gMnSc3	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
psychologie	psychologie	k1gFnSc2	psychologie
jsou	být	k5eAaImIp3nP	být
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Wundt	Wundt	k1gMnSc1	Wundt
a	a	k8xC	a
a	a	k8xC	a
Gustav	Gustav	k1gMnSc1	Gustav
Fechner	Fechner	k1gMnSc1	Fechner
<g/>
,	,	kIx,	,
psychologii	psychologie	k1gFnSc3	psychologie
národů	národ	k1gInPc2	národ
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
rozvinout	rozvinout	k5eAaPmF	rozvinout
Erik	Erik	k1gMnSc1	Erik
Erikson	Erikson	k1gMnSc1	Erikson
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
ekonomiku	ekonomika	k1gFnSc4	ekonomika
získali	získat	k5eAaPmAgMnP	získat
Reinhard	Reinhard	k1gMnSc1	Reinhard
Selten	Selten	k2eAgMnSc1d1	Selten
a	a	k8xC	a
Wassily	Wassily	k1gMnSc1	Wassily
Leontief	Leontief	k1gMnSc1	Leontief
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gInSc1	Franz
Boas	Boas	k1gInSc1	Boas
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgMnPc2d3	nejdůležitější
kulturních	kulturní	k2eAgMnPc2d1	kulturní
antropologů	antropolog	k1gMnPc2	antropolog
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Archeolog	archeolog	k1gMnSc1	archeolog
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schliemann	Schliemann	k1gMnSc1	Schliemann
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
objevitel	objevitel	k1gMnSc1	objevitel
antické	antický	k2eAgFnSc2d1	antická
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
von	von	k1gInSc4	von
Ranke	Ranke	k1gNnSc2	Ranke
byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
německé	německý	k2eAgFnSc2d1	německá
historiografie	historiografie	k1gFnSc2	historiografie
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Oswald	Oswald	k1gMnSc1	Oswald
Spengler	Spengler	k1gMnSc1	Spengler
proslul	proslout	k5eAaPmAgMnS	proslout
svou	svůj	k3xOyFgFnSc4	svůj
knihou	kniha	k1gFnSc7	kniha
Zánik	zánik	k1gInSc4	zánik
Západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
Erwin	Erwin	k1gMnSc1	Erwin
Panofsky	Panofsky	k1gMnSc1	Panofsky
svými	svůj	k3xOyFgFnPc7	svůj
výpravami	výprava	k1gFnPc7	výprava
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zakladatelům	zakladatel	k1gMnPc3	zakladatel
indologie	indologie	k1gFnSc2	indologie
patřil	patřit	k5eAaImAgMnS	patřit
Friedrich	Friedrich	k1gMnSc1	Friedrich
Max	Max	k1gMnSc1	Max
Müller	Müller	k1gMnSc1	Müller
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvýznamnějšího	významný	k2eAgMnSc4d3	nejvýznamnější
teoretika	teoretik	k1gMnSc4	teoretik
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Carl	Carl	k1gInSc1	Carl
von	von	k1gInSc1	von
Clausewitz	Clausewitz	k1gInSc1	Clausewitz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
údajů	údaj	k1gInPc2	údaj
ke	k	k7c3	k
dni	den	k1gInSc3	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
mělo	mít	k5eAaImAgNnS	mít
Německo	Německo	k1gNnSc1	Německo
82	[number]	k4	82
175	[number]	k4	175
684	[number]	k4	684
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
činí	činit	k5eAaImIp3nS	činit
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
zemí	zem	k1gFnSc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
po	po	k7c6	po
Rusku	Rusko	k1gNnSc6	Rusko
druhou	druhý	k4xOgFnSc7	druhý
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
nejlidnatější	lidnatý	k2eAgFnSc2d3	nejlidnatější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
225	[number]	k4	225
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
čtverční	čtverční	k2eAgInSc4d1	čtverční
kilometr	kilometr	k1gInSc4	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
80,19	[number]	k4	80,19
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
77,93	[number]	k4	77,93
let	léto	k1gNnPc2	léto
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
82,58	[number]	k4	82,58
let	léto	k1gNnPc2	léto
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
činila	činit	k5eAaImAgFnS	činit
plodnost	plodnost	k1gFnSc1	plodnost
1,41	[number]	k4	1,41
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
či	či	k8xC	či
8,33	[number]	k4	8,33
narození	narození	k1gNnSc2	narození
na	na	k7c4	na
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
překonává	překonávat	k5eAaImIp3nS	překonávat
porodnost	porodnost	k1gFnSc4	porodnost
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
Německo	Německo	k1gNnSc4	Německo
svědkem	svědek	k1gMnSc7	svědek
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
porodnosti	porodnost	k1gFnSc2	porodnost
a	a	k8xC	a
míry	míra	k1gFnSc2	míra
migrace	migrace	k1gFnSc2	migrace
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
může	moct	k5eAaImIp3nS	moct
nárůst	nárůst	k1gInSc1	nárůst
počtu	počet	k1gInSc2	počet
dobře	dobře	k6eAd1	dobře
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
migrantů	migrant	k1gMnPc2	migrant
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
imigrantů	imigrant	k1gMnPc2	imigrant
přicházela	přicházet	k5eAaImAgFnS	přicházet
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
usazuje	usazovat	k5eAaImIp3nS	usazovat
se	se	k3xPyFc4	se
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
<g/>
Německo	Německo	k1gNnSc1	Německo
má	mít	k5eAaImIp3nS	mít
chronicky	chronicky	k6eAd1	chronicky
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
porodností	porodnost	k1gFnPc2	porodnost
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gNnSc7	jeho
vůbec	vůbec	k9	vůbec
nejvážnější	vážní	k2eAgInPc4d3	nejvážnější
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
velkou	velký	k2eAgFnSc7d1	velká
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
počet	počet	k1gInSc4	počet
zemřelých	zemřelý	k1gMnPc2	zemřelý
neustále	neustále	k6eAd1	neustále
překračuje	překračovat	k5eAaImIp3nS	překračovat
počet	počet	k1gInSc4	počet
narozených	narozený	k2eAgMnPc2d1	narozený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
měla	mít	k5eAaImAgFnS	mít
německá	německý	k2eAgFnSc1d1	německá
rodina	rodina	k1gFnSc1	rodina
1,37	[number]	k4	1,37
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
porodnost	porodnost	k1gFnSc4	porodnost
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
a	a	k8xC	a
narodilo	narodit	k5eAaPmAgNnS	narodit
se	se	k3xPyFc4	se
684	[number]	k4	684
862	[number]	k4	862
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
1,38	[number]	k4	1,38
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
ale	ale	k8xC	ale
dále	daleko	k6eAd2	daleko
klesala	klesat	k5eAaImAgFnS	klesat
až	až	k9	až
k	k	k7c3	k
665	[number]	k4	665
126	[number]	k4	126
živě	živě	k6eAd1	živě
narozeným	narozený	k2eAgFnPc3d1	narozená
dětem	dítě	k1gFnPc3	dítě
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
1,36	[number]	k4	1,36
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
matku	matka	k1gFnSc4	matka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
drobným	drobný	k2eAgInSc7d1	drobný
výkyvem	výkyv	k1gInSc7	výkyv
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
k	k	k7c3	k
hodnotě	hodnota	k1gFnSc3	hodnota
1,39	[number]	k4	1,39
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
677	[number]	k4	677
947	[number]	k4	947
živě	živě	k6eAd1	živě
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
opět	opět	k6eAd1	opět
spíše	spíše	k9	spíše
klesala	klesat	k5eAaImAgFnS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
rychlé	rychlý	k2eAgNnSc4d1	rychlé
stárnutí	stárnutí	k1gNnSc4	stárnutí
a	a	k8xC	a
úbytek	úbytek	k1gInSc4	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Německo	Německo	k1gNnSc1	Německo
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
počtu	počet	k1gInSc3	počet
82	[number]	k4	82
536	[number]	k4	536
680	[number]	k4	680
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ubylo	ubýt	k5eAaPmAgNnS	ubýt
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
i	i	k8xC	i
přes	přes	k7c4	přes
výrazný	výrazný	k2eAgInSc4d1	výrazný
příliv	příliv	k1gInSc4	příliv
imigrantů	imigrant	k1gMnPc2	imigrant
více	hodně	k6eAd2	hodně
než	než	k8xS	než
785	[number]	k4	785
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2007	[number]	k4	2007
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c4	na
území	území	k1gNnSc4	území
Německa	Německo	k1gNnSc2	Německo
82	[number]	k4	82
217	[number]	k4	217
800	[number]	k4	800
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
357	[number]	k4	357
104	[number]	k4	104
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hustotou	hustota	k1gFnSc7	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
především	především	k9	především
odsunu	odsunout	k5eAaPmIp1nS	odsunout
a	a	k8xC	a
emigrace	emigrace	k1gFnSc1	emigrace
etnických	etnický	k2eAgMnPc2d1	etnický
Němců	Němec	k1gMnPc2	Němec
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
75	[number]	k4	75
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
91	[number]	k4	91
%	%	kIx~	%
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
německé	německý	k2eAgNnSc4d1	německé
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
má	mít	k5eAaImIp3nS	mít
7,9	[number]	k4	7,9
miliónu	milión	k4xCgInSc2	milión
migrační	migrační	k2eAgFnSc1d1	migrační
minulost	minulost	k1gFnSc1	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
4,3	[number]	k4	4,3
miliónů	milión	k4xCgInPc2	milión
občanů	občan	k1gMnPc2	občan
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
(	(	kIx(	(
<g/>
51	[number]	k4	51
procent	procento	k1gNnPc2	procento
<g/>
)	)	kIx)	)
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
34	[number]	k4	34
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
německý	německý	k2eAgInSc4d1	německý
původ	původ	k1gInSc4	původ
a	a	k8xC	a
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Hovorově	hovorově	k6eAd1	hovorově
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
říká	říkat	k5eAaImIp3nS	říkat
ruští	ruský	k2eAgMnPc1d1	ruský
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
žijící	žijící	k2eAgMnPc1d1	žijící
cizinci	cizinec	k1gMnPc1	cizinec
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
rodinní	rodinní	k2eAgMnPc1d1	rodinní
příslušníci	příslušník	k1gMnPc1	příslušník
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gFnPc2	jejich
dětí	dítě	k1gFnPc2	dítě
narozených	narozený	k2eAgFnPc2d1	narozená
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
německé	německý	k2eAgNnSc4d1	německé
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Imigrace	imigrace	k1gFnSc2	imigrace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
probíhající	probíhající	k2eAgFnSc2d1	probíhající
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
zázrak	zázrak	k1gInSc1	zázrak
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
konce	konec	k1gInSc2	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
až	až	k9	až
po	po	k7c4	po
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
podstatně	podstatně	k6eAd1	podstatně
podpořen	podpořit	k5eAaPmNgInS	podpořit
třemi	tři	k4xCgFnPc7	tři
přistěhovaleckými	přistěhovalecký	k2eAgFnPc7d1	přistěhovalecká
vlnami	vlna	k1gFnPc7	vlna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pomohly	pomoct	k5eAaPmAgFnP	pomoct
zamezit	zamezit	k5eAaPmF	zamezit
vzrůstajícímu	vzrůstající	k2eAgInSc3d1	vzrůstající
nedostatku	nedostatek	k1gInSc2	nedostatek
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
přišlo	přijít	k5eAaPmAgNnS	přijít
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
západních	západní	k2eAgFnPc2d1	západní
okupačních	okupační	k2eAgFnPc2d1	okupační
zón	zóna	k1gFnPc2	zóna
asi	asi	k9	asi
dvanáct	dvanáct	k4xCc1	dvanáct
milionů	milion	k4xCgInPc2	milion
německých	německý	k2eAgMnPc2d1	německý
uprchlíků	uprchlík	k1gMnPc2	uprchlík
a	a	k8xC	a
vyhnanců	vyhnanec	k1gMnPc2	vyhnanec
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
obou	dva	k4xCgInPc2	dva
německých	německý	k2eAgInPc2d1	německý
států	stát	k1gInPc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
až	až	k9	až
do	do	k7c2	do
postavení	postavení	k1gNnSc2	postavení
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
se	se	k3xPyFc4	se
z	z	k7c2	z
východního	východní	k2eAgNnSc2d1	východní
Německa	Německo	k1gNnSc2	Německo
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
západního	západní	k2eAgInSc2d1	západní
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
asi	asi	k9	asi
3,1	[number]	k4	3,1
milionu	milion	k4xCgInSc2	milion
Němců	Němec	k1gMnPc2	Němec
<g/>
;	;	kIx,	;
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
400	[number]	k4	400
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
započalo	započnout	k5eAaPmAgNnS	započnout
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
rozsahu	rozsah	k1gInSc6	rozsah
zaměstnávání	zaměstnávání	k1gNnSc2	zaměstnávání
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
příchozím	příchozí	k1gMnPc3	příchozí
pracovníkům	pracovník	k1gMnPc3	pracovník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
najímáni	najímat	k5eAaImNgMnP	najímat
především	především	k9	především
na	na	k7c6	na
základě	základ	k1gInSc6	základ
bilaterálních	bilaterální	k2eAgFnPc2d1	bilaterální
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
veřejných	veřejný	k2eAgFnPc6d1	veřejná
debatách	debata	k1gFnPc6	debata
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
hostující	hostující	k2eAgMnPc1d1	hostující
dělníci	dělník	k1gMnPc1	dělník
<g/>
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
Gastarbeiter	Gastarbeiter	k1gInSc1	Gastarbeiter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dohody	dohoda	k1gFnPc1	dohoda
o	o	k7c6	o
náboru	nábor	k1gInSc6	nábor
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
a	a	k8xC	a
Řeckem	Řecko	k1gNnSc7	Řecko
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
s	s	k7c7	s
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
smlouvy	smlouva	k1gFnPc1	smlouva
byly	být	k5eAaImAgFnP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
s	s	k7c7	s
Marokem	Maroko	k1gNnSc7	Maroko
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
-	-	kIx~	-
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
Tuniskem	Tunisko	k1gNnSc7	Tunisko
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
Turků	turek	k1gInPc2	turek
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
z	z	k7c2	z
8	[number]	k4	8
700	[number]	k4	700
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
milion	milion	k4xCgInSc4	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pracovní	pracovní	k2eAgFnSc2d1	pracovní
migrace	migrace	k1gFnSc2	migrace
předcházely	předcházet	k5eAaImAgFnP	předcházet
mužské	mužský	k2eAgFnPc1d1	mužská
pracovní	pracovní	k2eAgFnPc1d1	pracovní
síly	síla	k1gFnPc1	síla
příchodu	příchod	k1gInSc2	příchod
dalších	další	k2eAgMnPc2d1	další
rodinných	rodinný	k2eAgMnPc2d1	rodinný
příslušníků	příslušník	k1gMnPc2	příslušník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
jako	jako	k8xC	jako
úředně	úředně	k6eAd1	úředně
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
a	a	k8xC	a
časově	časově	k6eAd1	časově
omezená	omezený	k2eAgFnSc1d1	omezená
pracovní	pracovní	k2eAgFnSc1d1	pracovní
migrace	migrace	k1gFnSc1	migrace
<g/>
,	,	kIx,	,
plynule	plynule	k6eAd1	plynule
přešel	přejít	k5eAaPmAgInS	přejít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
skutečné	skutečný	k2eAgFnSc2d1	skutečná
imigrační	imigrační	k2eAgFnSc2d1	imigrační
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
slabý	slabý	k2eAgInSc1d1	slabý
odliv	odliv	k1gInSc1	odliv
turecké	turecký	k2eAgFnSc2d1	turecká
menšiny	menšina	k1gFnSc2	menšina
<g/>
;	;	kIx,	;
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
imigrovalo	imigrovat	k5eAaBmAgNnS	imigrovat
asi	asi	k9	asi
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
Turků	turek	k1gInPc2	turek
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
odstěhovalo	odstěhovat	k5eAaPmAgNnS	odstěhovat
asi	asi	k9	asi
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
převážně	převážně	k6eAd1	převážně
německých	německý	k2eAgMnPc2d1	německý
občanů	občan	k1gMnPc2	občan
tureckého	turecký	k2eAgInSc2d1	turecký
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
žily	žít	k5eAaImAgFnP	žít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
asi	asi	k9	asi
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
Turků	Turek	k1gMnPc2	Turek
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
a	a	k8xC	a
2010	[number]	k4	2010
celkově	celkově	k6eAd1	celkově
Německo	Německo	k1gNnSc1	Německo
opustilo	opustit	k5eAaPmAgNnS	opustit
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
než	než	k8xS	než
kolik	kolik	k4yQc4	kolik
se	se	k3xPyFc4	se
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
81	[number]	k4	81
<g/>
milionovém	milionový	k2eAgNnSc6d1	milionové
Německu	Německo	k1gNnSc6	Německo
skoro	skoro	k6eAd1	skoro
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
jiné	jiný	k2eAgFnSc2d1	jiná
státní	státní	k2eAgFnSc2d1	státní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
.	.	kIx.	.
96	[number]	k4	96
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
(	(	kIx(	(
<g/>
celého	celý	k2eAgInSc2d1	celý
<g/>
)	)	kIx)	)
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
mělo	mít	k5eAaImAgNnS	mít
1,6	[number]	k4	1,6
miliónu	milión	k4xCgInSc2	milión
tureckou	turecký	k2eAgFnSc4d1	turecká
státní	státní	k2eAgFnSc4d1	státní
příslušnost	příslušnost	k1gFnSc4	příslušnost
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
2,1	[number]	k4	2,1
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
520	[number]	k4	520
tisíc	tisíc	k4xCgInPc2	tisíc
italské	italský	k2eAgFnSc2d1	italská
státní	státní	k2eAgFnSc2d1	státní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
,	,	kIx,	,
468	[number]	k4	468
tisíc	tisíc	k4xCgInPc2	tisíc
polské	polský	k2eAgInPc1d1	polský
<g/>
,	,	kIx,	,
283	[number]	k4	283
tisíc	tisíc	k4xCgInPc2	tisíc
řecké	řecký	k2eAgFnSc2d1	řecká
a	a	k8xC	a
38	[number]	k4	38
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
má	mít	k5eAaImIp3nS	mít
české	český	k2eAgNnSc4d1	české
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
členy	člen	k1gMnPc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
3,2	[number]	k4	3,2
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jeden	jeden	k4xCgMnSc1	jeden
milion	milion	k4xCgInSc1	milion
lidí	člověk	k1gMnPc2	člověk
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
mělo	mít	k5eAaImAgNnS	mít
přistěhovalecký	přistěhovalecký	k2eAgInSc4d1	přistěhovalecký
původ	původ	k1gInSc4	původ
22,5	[number]	k4	22,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
do	do	k7c2	do
5	[number]	k4	5
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
přistěhovaleckého	přistěhovalecký	k2eAgInSc2d1	přistěhovalecký
původu	původ	k1gInSc2	původ
36	[number]	k4	36
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
kořeny	kořen	k1gInPc7	kořen
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
51	[number]	k4	51
%	%	kIx~	%
a	a	k8xC	a
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
africkými	africký	k2eAgInPc7d1	africký
kořeny	kořen	k1gInPc7	kořen
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
46	[number]	k4	46
%	%	kIx~	%
na	na	k7c4	na
740	[number]	k4	740
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
tvoří	tvořit	k5eAaImIp3nP	tvořit
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
přistěhovaleckým	přistěhovalecký	k2eAgInSc7d1	přistěhovalecký
původem	původ	k1gInSc7	původ
51,2	[number]	k4	51,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
pobývalo	pobývat	k5eAaImAgNnS	pobývat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nelegálně	legálně	k6eNd1	legálně
na	na	k7c4	na
230	[number]	k4	230
000	[number]	k4	000
migrantů	migrant	k1gMnPc2	migrant
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
vyhoštěni	vyhostit	k5eAaPmNgMnP	vyhostit
německými	německý	k2eAgMnPc7d1	německý
úřady	úřada	k1gMnPc7	úřada
<g/>
.	.	kIx.	.
60	[number]	k4	60
<g/>
%	%	kIx~	%
migrantů	migrant	k1gMnPc2	migrant
pobíralo	pobírat	k5eAaImAgNnS	pobírat
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
dlouhodobé	dlouhodobý	k2eAgFnSc6d1	dlouhodobá
nezaměstnanosti	nezaměstnanost	k1gFnSc6	nezaměstnanost
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
původních	původní	k2eAgFnPc2d1	původní
etnických	etnický	k2eAgFnPc2d1	etnická
menšin	menšina	k1gFnPc2	menšina
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
15	[number]	k4	15
až	až	k9	až
50	[number]	k4	50
tisíc	tisíc	k4xCgInSc4	tisíc
Dánů	Dán	k1gMnPc2	Dán
(	(	kIx(	(
<g/>
zdroje	zdroj	k1gInPc1	zdroj
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
50	[number]	k4	50
až	až	k9	až
60	[number]	k4	60
tisíc	tisíc	k4xCgInPc2	tisíc
Frísů	Frís	k1gMnPc2	Frís
<g/>
,	,	kIx,	,
60	[number]	k4	60
tisíc	tisíc	k4xCgInPc2	tisíc
Lužických	lužický	k2eAgMnPc2d1	lužický
Srbů	Srb	k1gMnPc2	Srb
a	a	k8xC	a
asi	asi	k9	asi
70	[number]	k4	70
tisíc	tisíc	k4xCgInPc2	tisíc
Romů	Rom	k1gMnPc2	Rom
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
USA	USA	kA	USA
druhým	druhý	k4xOgInSc7	druhý
nejčastějším	častý	k2eAgInSc7d3	nejčastější
cílem	cíl	k1gInSc7	cíl
imigrantů	imigrant	k1gMnPc2	imigrant
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
Německo	Německo	k1gNnSc1	Německo
zmítá	zmítat	k5eAaImIp3nS	zmítat
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
migrační	migrační	k2eAgFnSc6d1	migrační
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2015	[number]	k4	2015
a	a	k8xC	a
2016	[number]	k4	2016
přišlo	přijít	k5eAaPmAgNnS	přijít
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
téměř	téměř	k6eAd1	téměř
1,2	[number]	k4	1,2
milionu	milion	k4xCgInSc2	milion
migrantů	migrant	k1gMnPc2	migrant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Němčina	němčina	k1gFnSc1	němčina
je	být	k5eAaImIp3nS	být
úřední	úřední	k2eAgInSc4d1	úřední
a	a	k8xC	a
převažující	převažující	k2eAgInSc4d1	převažující
jazyk	jazyk	k1gInSc4	jazyk
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
24	[number]	k4	24
úředních	úřední	k2eAgInPc2d1	úřední
a	a	k8xC	a
pracovních	pracovní	k2eAgInPc2d1	pracovní
jazyků	jazyk	k1gInPc2	jazyk
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
pracovních	pracovní	k2eAgInPc2d1	pracovní
jazyků	jazyk	k1gInPc2	jazyk
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
němčina	němčina	k1gFnSc1	němčina
také	také	k6eAd1	také
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
miliony	milion	k4xCgInPc4	milion
rodilými	rodilý	k2eAgMnPc7d1	rodilý
mluvčími	mluvčí	k1gMnPc7	mluvčí
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
původní	původní	k2eAgInPc4d1	původní
menšinové	menšinový	k2eAgInPc4d1	menšinový
jazyky	jazyk	k1gInPc4	jazyk
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
patří	patřit	k5eAaImIp3nS	patřit
dánština	dánština	k1gFnSc1	dánština
<g/>
,	,	kIx,	,
dolnoněmčina	dolnoněmčina	k1gFnSc1	dolnoněmčina
<g/>
,	,	kIx,	,
lužická	lužický	k2eAgFnSc1d1	Lužická
srbština	srbština	k1gFnSc1	srbština
<g/>
,	,	kIx,	,
fríština	fríština	k1gFnSc1	fríština
a	a	k8xC	a
romština	romština	k1gFnSc1	romština
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
menšinové	menšinový	k2eAgInPc1d1	menšinový
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
chráněny	chránit	k5eAaImNgFnP	chránit
evropskou	evropský	k2eAgFnSc7d1	Evropská
chartou	charta	k1gFnSc7	charta
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nejpoužívanějšími	používaný	k2eAgInPc7d3	nejpoužívanější
jazyky	jazyk	k1gInPc7	jazyk
mezi	mezi	k7c7	mezi
imigranty	imigrant	k1gMnPc7	imigrant
jsou	být	k5eAaImIp3nP	být
turečtina	turečtina	k1gFnSc1	turečtina
<g/>
,	,	kIx,	,
kurdština	kurdština	k1gFnSc1	kurdština
<g/>
,	,	kIx,	,
polština	polština	k1gFnSc1	polština
<g/>
,	,	kIx,	,
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
srbochorvatština	srbochorvatština	k1gFnSc1	srbochorvatština
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
jazyky	jazyk	k1gInPc1	jazyk
z	z	k7c2	z
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
Německa	Německo	k1gNnSc2	Německo
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
mnohojazyční	mnohojazyční	k2eAgMnPc1d1	mnohojazyční
<g/>
:	:	kIx,	:
67	[number]	k4	67
%	%	kIx~	%
německých	německý	k2eAgMnPc2d1	německý
občanů	občan	k1gMnPc2	občan
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dokáží	dokázat	k5eAaPmIp3nP	dokázat
domluvit	domluvit	k5eAaPmF	domluvit
minimálně	minimálně	k6eAd1	minimálně
jedním	jeden	k4xCgInSc7	jeden
cizím	cizí	k2eAgInSc7d1	cizí
jazykem	jazyk	k1gInSc7	jazyk
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
němčiny	němčina	k1gFnSc2	němčina
<g/>
)	)	kIx)	)
a	a	k8xC	a
27	[number]	k4	27
%	%	kIx~	%
minimálně	minimálně	k6eAd1	minimálně
dvěma	dva	k4xCgInPc7	dva
cizími	cizí	k2eAgInPc7d1	cizí
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
<g/>
Standardní	standardní	k2eAgFnSc1d1	standardní
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Hochdeutsch	Hochdeutsch	k1gInSc1	Hochdeutsch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
západogermánský	západogermánský	k2eAgInSc1d1	západogermánský
jazyk	jazyk	k1gInSc1	jazyk
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
dolnoněmčině	dolnoněmčina	k1gFnSc3	dolnoněmčina
<g/>
,	,	kIx,	,
nizozemštině	nizozemština	k1gFnSc3	nizozemština
a	a	k8xC	a
fríštině	fríština	k1gFnSc3	fríština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
je	být	k5eAaImIp3nS	být
také	také	k9	také
příbuzná	příbuzná	k1gFnSc1	příbuzná
s	s	k7c7	s
vymřelými	vymřelý	k2eAgInPc7d1	vymřelý
východogermánskými	východogermánský	k2eAgInPc7d1	východogermánský
a	a	k8xC	a
dnešními	dnešní	k2eAgInPc7d1	dnešní
severogermánskými	severogermánský	k2eAgInPc7d1	severogermánský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
německých	německý	k2eAgNnPc2d1	německé
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
z	z	k7c2	z
germánských	germánský	k2eAgFnPc2d1	germánská
větví	větev	k1gFnPc2	větev
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
menšina	menšina	k1gFnSc1	menšina
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
přibývá	přibývat	k5eAaImIp3nS	přibývat
mnoho	mnoho	k4c1	mnoho
slov	slovo	k1gNnPc2	slovo
převzatých	převzatý	k2eAgInPc2d1	převzatý
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Denglisch	Denglisch	k1gMnSc1	Denglisch
<g/>
.	.	kIx.	.
</s>
<s>
Psaná	psaný	k2eAgFnSc1d1	psaná
němčina	němčina	k1gFnSc1	němčina
používá	používat	k5eAaImIp3nS	používat
latinku	latinka	k1gFnSc4	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgInPc1d1	německý
dialekty	dialekt	k1gInPc1	dialekt
jsou	být	k5eAaImIp3nP	být
četné	četný	k2eAgInPc1d1	četný
tradiční	tradiční	k2eAgInPc1d1	tradiční
územní	územní	k2eAgInPc1d1	územní
nebo	nebo	k8xC	nebo
i	i	k9	i
lokální	lokální	k2eAgInPc4d1	lokální
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
vystopovat	vystopovat	k5eAaPmF	vystopovat
ke	k	k7c3	k
starým	starý	k2eAgInPc3d1	starý
germánským	germánský	k2eAgInPc3d1	germánský
kmenům	kmen	k1gInPc3	kmen
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Alemané	Aleman	k1gMnPc1	Aleman
<g/>
,	,	kIx,	,
Bavoři	Bavor	k1gMnPc1	Bavor
<g/>
,	,	kIx,	,
Frankové	Frank	k1gMnPc1	Frank
<g/>
,	,	kIx,	,
Chattové	Chatt	k1gMnPc1	Chatt
a	a	k8xC	a
Sasové	Sas	k1gMnPc1	Sas
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
útvarů	útvar	k1gInPc2	útvar
standardní	standardní	k2eAgFnSc2d1	standardní
němčiny	němčina	k1gFnSc2	němčina
v	v	k7c6	v
lexiku	lexikon	k1gNnSc6	lexikon
<g/>
,	,	kIx,	,
fonologii	fonologie	k1gFnSc6	fonologie
a	a	k8xC	a
syntaxi	syntax	k1gFnSc6	syntax
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
znalostí	znalost	k1gFnPc2	znalost
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
založený	založený	k2eAgInSc4d1	založený
Goethe-Institut	Goethe-Institut	k1gInSc4	Goethe-Institut
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
se	s	k7c7	s
12	[number]	k4	12
pobočkami	pobočka	k1gFnPc7	pobočka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
159	[number]	k4	159
pracovišti	pracoviště	k1gNnPc7	pracoviště
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k8xC	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Institut	institut	k1gInSc1	institut
je	být	k5eAaImIp3nS	být
nazván	nazvat	k5eAaBmNgInS	nazvat
podle	podle	k7c2	podle
básníka	básník	k1gMnSc2	básník
Johanna	Johann	k1gMnSc2	Johann
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
von	von	k1gInSc4	von
Goethe	Goethe	k1gNnSc2	Goethe
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
finančně	finančně	k6eAd1	finančně
podporován	podporován	k2eAgInSc1d1	podporován
německým	německý	k2eAgNnSc7d1	německé
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
náboženstvím	náboženství	k1gNnSc7	náboženství
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
66,8	[number]	k4	66,8
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
celkové	celkový	k2eAgFnSc3d1	celková
populaci	populace	k1gFnSc3	populace
se	s	k7c7	s
31,2	[number]	k4	31,2
%	%	kIx~	%
označilo	označit	k5eAaPmAgNnS	označit
jako	jako	k9	jako
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
<g/>
;	;	kIx,	;
30,8	[number]	k4	30,8
%	%	kIx~	%
jako	jako	k9	jako
protestanti	protestant	k1gMnPc1	protestant
zastupovaní	zastupovaný	k2eAgMnPc1d1	zastupovaný
evangelickou	evangelický	k2eAgFnSc7d1	evangelická
církví	církev	k1gFnSc7	církev
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
EKD	EKD	kA	EKD
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
4,8	[number]	k4	4,8
%	%	kIx~	%
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
jiných	jiný	k2eAgNnPc2d1	jiné
křesťanských	křesťanský	k2eAgNnPc2d1	křesťanské
vyznání	vyznání	k1gNnPc2	vyznání
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
evangelické	evangelický	k2eAgFnSc2d1	evangelická
svobodné	svobodný	k2eAgFnSc2d1	svobodná
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Evangelische	Evangelische	k1gFnSc1	Evangelische
Freikirche	Freikirch	k1gFnSc2	Freikirch
<g/>
,	,	kIx,	,
s	s	k7c7	s
0,9	[number]	k4	0,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatních	ostatní	k2eAgMnPc2d1	ostatní
protestantů	protestant	k1gMnPc2	protestant
mimo	mimo	k7c4	mimo
EKD	EKD	kA	EKD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
pravoslaví	pravoslaví	k1gNnSc3	pravoslaví
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
1,3	[number]	k4	1,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
k	k	k7c3	k
judaismu	judaismus	k1gInSc3	judaismus
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
náboženství	náboženství	k1gNnPc4	náboženství
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
2,7	[number]	k4	2,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Geograficky	geograficky	k6eAd1	geograficky
je	být	k5eAaImIp3nS	být
protestantství	protestantství	k1gNnSc1	protestantství
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
<g/>
,	,	kIx,	,
středních	střední	k2eAgFnPc6d1	střední
a	a	k8xC	a
východních	východní	k2eAgFnPc6d1	východní
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
členy	člen	k1gMnPc4	člen
EKD	EKD	kA	EKD
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
luterány	luterán	k1gMnPc4	luterán
<g/>
,	,	kIx,	,
reformované	reformovaný	k2eAgMnPc4d1	reformovaný
a	a	k8xC	a
sjednocenou	sjednocený	k2eAgFnSc4d1	sjednocená
církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
tradice	tradice	k1gFnPc1	tradice
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
k	k	k7c3	k
pruské	pruský	k2eAgFnSc3d1	pruská
unii	unie	k1gFnSc3	unie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
<g/>
.	.	kIx.	.
</s>
<s>
Katolíci	katolík	k1gMnPc1	katolík
jsou	být	k5eAaImIp3nP	být
soustředěni	soustředit	k5eAaPmNgMnP	soustředit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
počet	počet	k1gInSc1	počet
příslušníků	příslušník	k1gMnPc2	příslušník
tradičních	tradiční	k2eAgFnPc2d1	tradiční
historických	historický	k2eAgFnPc2d1	historická
církví	církev	k1gFnPc2	církev
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
souvisí	souviset	k5eAaImIp3nS	souviset
hlavně	hlavně	k9	hlavně
s	s	k7c7	s
sekularizací	sekularizace	k1gFnSc7	sekularizace
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
církevní	církevní	k2eAgFnSc7d1	církevní
daní	daň	k1gFnSc7	daň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	s	k7c7	s
33	[number]	k4	33
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
označilo	označit	k5eAaPmAgNnS	označit
jako	jako	k8xC	jako
agnostici	agnostik	k1gMnPc1	agnostik
<g/>
,	,	kIx,	,
ateisté	ateista	k1gMnPc1	ateista
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
bezbožní	bezbožný	k2eAgMnPc1d1	bezbožný
<g/>
.	.	kIx.	.
</s>
<s>
Nevěřících	nevěřící	k1gFnPc2	nevěřící
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
bývalého	bývalý	k2eAgNnSc2d1	bývalé
východního	východní	k2eAgNnSc2d1	východní
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
velkých	velký	k2eAgFnPc6d1	velká
metropolitních	metropolitní	k2eAgFnPc6d1	metropolitní
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
identifikuje	identifikovat	k5eAaBmIp3nS	identifikovat
jako	jako	k8xC	jako
ateisté	ateista	k1gMnPc1	ateista
<g/>
.	.	kIx.	.
<g/>
Islám	islám	k1gInSc1	islám
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
druhým	druhý	k4xOgNnSc7	druhý
nejpočetnějším	početní	k2eAgNnSc7d3	nejpočetnější
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
migraci	migrace	k1gFnSc6	migrace
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
tvořili	tvořit	k5eAaImAgMnP	tvořit
muslimové	muslim	k1gMnPc1	muslim
5,4	[number]	k4	5,4
až	až	k8xS	až
5,7	[number]	k4	5,7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
muslimů	muslim	k1gMnPc2	muslim
jsou	být	k5eAaImIp3nP	být
sunniti	sunniti	k?	sunniti
a	a	k8xC	a
alevité	alevita	k1gMnPc1	alevita
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itů	itů	k?	itů
<g/>
,	,	kIx,	,
ahmadíjů	ahmadíj	k1gInPc2	ahmadíj
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
podíl	podíl	k1gInSc1	podíl
muslimů	muslim	k1gMnPc2	muslim
ze	z	k7c2	z
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
nebo	nebo	k8xC	nebo
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
tureckého	turecký	k2eAgInSc2d1	turecký
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
od	od	k7c2	od
státu	stát	k1gInSc2	stát
plné	plný	k2eAgNnSc4d1	plné
oficiální	oficiální	k2eAgNnSc4d1	oficiální
uznání	uznání	k1gNnSc4	uznání
náboženské	náboženský	k2eAgFnSc2d1	náboženská
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
<g/>
Ostatní	ostatní	k2eAgNnSc1d1	ostatní
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
méně	málo	k6eAd2	málo
než	než	k8xS	než
procento	procento	k1gNnSc1	procento
německé	německý	k2eAgFnSc2d1	německá
populace	populace	k1gFnSc2	populace
jsou	být	k5eAaImIp3nP	být
buddhismus	buddhismus	k1gInSc4	buddhismus
se	s	k7c7	s
250	[number]	k4	250
000	[number]	k4	000
stoupenci	stoupenec	k1gMnPc7	stoupenec
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
hinduismus	hinduismus	k1gInSc1	hinduismus
s	s	k7c7	s
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
stoupenci	stoupenec	k1gMnPc7	stoupenec
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
náboženské	náboženský	k2eAgFnPc1d1	náboženská
komunity	komunita	k1gFnPc1	komunita
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
mají	mít	k5eAaImIp3nP	mít
pod	pod	k7c7	pod
50	[number]	k4	50
000	[number]	k4	000
stoupenců	stoupenec	k1gMnPc2	stoupenec
každá	každý	k3xTgFnSc1	každý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Úvod	úvod	k1gInSc1	úvod
===	===	k?	===
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
především	především	k9	především
společným	společný	k2eAgInSc7d1	společný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
mnoho	mnoho	k6eAd1	mnoho
představitelů	představitel	k1gMnPc2	představitel
německé	německý	k2eAgFnSc2d1	německá
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
odešlo	odejít	k5eAaPmAgNnS	odejít
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
však	však	k9	však
zůstali	zůstat	k5eAaPmAgMnP	zůstat
spjati	spjat	k2eAgMnPc1d1	spjat
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklady	příklad	k1gInPc1	příklad
emigrace	emigrace	k1gFnSc2	emigrace
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
básníka	básník	k1gMnSc2	básník
Heinricha	Heinrich	k1gMnSc2	Heinrich
Heineho	Heine	k1gMnSc2	Heine
a	a	k8xC	a
filozofa	filozof	k1gMnSc2	filozof
a	a	k8xC	a
ekonoma	ekonom	k1gMnSc2	ekonom
Karla	Karel	k1gMnSc2	Karel
Marxe	Marx	k1gMnSc2	Marx
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Německé	německý	k2eAgNnSc1d1	německé
císařství	císařství	k1gNnSc1	císařství
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nevýrazné	výrazný	k2eNgFnSc2d1	nevýrazná
společné	společný	k2eAgFnSc2d1	společná
identifikace	identifikace	k1gFnSc2	identifikace
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgMnPc2d1	mluvící
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
roztříštěnosti	roztříštěnost	k1gFnSc2	roztříštěnost
státní	státní	k2eAgFnSc2d1	státní
organizace	organizace	k1gFnSc2	organizace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Německého	německý	k2eAgInSc2d1	německý
spolku	spolek	k1gInSc2	spolek
jak	jak	k8xS	jak
německá	německý	k2eAgFnSc1d1	německá
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
věda	věda	k1gFnSc1	věda
důležitými	důležitý	k2eAgInPc7d1	důležitý
sjednocujícími	sjednocující	k2eAgInPc7d1	sjednocující
prvky	prvek	k1gInPc7	prvek
německého	německý	k2eAgNnSc2d1	německé
národního	národní	k2eAgNnSc2d1	národní
uvědomění	uvědomění	k1gNnSc2	uvědomění
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
předtím	předtím	k6eAd1	předtím
ovšem	ovšem	k9	ovšem
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
země	zem	k1gFnPc1	zem
básníků	básník	k1gMnPc2	básník
a	a	k8xC	a
myslitelů	myslitel	k1gMnPc2	myslitel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgNnSc1d1	zakládající
se	se	k3xPyFc4	se
na	na	k7c4	na
dědictví	dědictví	k1gNnSc4	dědictví
velkých	velký	k2eAgFnPc2d1	velká
osobností	osobnost	k1gFnPc2	osobnost
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
von	von	k1gInSc4	von
Goethe	Goethe	k1gNnSc2	Goethe
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
a	a	k8xC	a
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
živnou	živný	k2eAgFnSc7d1	živná
půdou	půda	k1gFnSc7	půda
německého	německý	k2eAgNnSc2d1	německé
vlastenectví	vlastenectví	k1gNnSc2	vlastenectví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
patřilo	patřit	k5eAaImAgNnS	patřit
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
Německo	Německo	k1gNnSc1	Německo
k	k	k7c3	k
předním	přední	k2eAgInPc3d1	přední
kulturním	kulturní	k2eAgInPc3d1	kulturní
a	a	k8xC	a
vědeckým	vědecký	k2eAgInPc3d1	vědecký
národům	národ	k1gInPc3	národ
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
díky	díky	k7c3	díky
německé	německý	k2eAgFnSc3d1	německá
klasické	klasický	k2eAgFnSc3d1	klasická
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
těžit	těžit	k5eAaImF	těžit
z	z	k7c2	z
odkazu	odkaz	k1gInSc2	odkaz
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
</s>
<s>
Amadea	Amadeus	k1gMnSc4	Amadeus
Mozarta	Mozart	k1gMnSc4	Mozart
<g/>
,	,	kIx,	,
Ludwiga	Ludwig	k1gMnSc2	Ludwig
van	vana	k1gFnPc2	vana
Beethovena	Beethoven	k1gMnSc2	Beethoven
<g/>
,	,	kIx,	,
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
,	,	kIx,	,
Johannese	Johannese	k1gFnSc1	Johannese
Brahmse	Brahms	k1gMnSc2	Brahms
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
velkých	velký	k2eAgMnPc2d1	velký
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
i	i	k9	i
díky	díky	k7c3	díky
německému	německý	k2eAgNnSc3d1	německé
výtvarnému	výtvarný	k2eAgNnSc3d1	výtvarné
a	a	k8xC	a
filmovému	filmový	k2eAgNnSc3d1	filmové
umění	umění	k1gNnSc3	umění
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
kultura	kultura	k1gFnSc1	kultura
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejvyspělejších	vyspělý	k2eAgFnPc2d3	nejvyspělejší
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nástup	nástup	k1gInSc1	nástup
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spojená	spojený	k2eAgFnSc1d1	spojená
početná	početný	k2eAgFnSc1d1	početná
emigrace	emigrace	k1gFnSc1	emigrace
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
německých	německý	k2eAgMnPc2d1	německý
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
mnozí	mnohý	k2eAgMnPc1d1	mnohý
byli	být	k5eAaImAgMnP	být
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
přinesly	přinést	k5eAaPmAgFnP	přinést
úpadek	úpadek	k1gInSc4	úpadek
německé	německý	k2eAgFnSc2d1	německá
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Trvalými	trvalý	k2eAgMnPc7d1	trvalý
nebo	nebo	k8xC	nebo
dočasnými	dočasný	k2eAgMnPc7d1	dočasný
emigranty	emigrant	k1gMnPc7	emigrant
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
například	například	k6eAd1	například
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Tucholsky	Tucholsky	k1gMnSc1	Tucholsky
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Heinrich	Heinrich	k1gMnSc1	Heinrich
Mann	Mann	k1gMnSc1	Mann
<g/>
,	,	kIx,	,
Bertolt	Bertolt	k1gMnSc1	Bertolt
Brecht	Brecht	k1gMnSc1	Brecht
<g/>
,	,	kIx,	,
Lion	Lion	k1gMnSc1	Lion
Feuchtwanger	Feuchtwanger	k1gMnSc1	Feuchtwanger
<g/>
,	,	kIx,	,
Hannah	Hannah	k1gMnSc1	Hannah
Arendtová	Arendtová	k1gFnSc1	Arendtová
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
literatura	literatura	k1gFnSc1	literatura
sahá	sahat	k5eAaImIp3nS	sahat
zpět	zpět	k6eAd1	zpět
až	až	k9	až
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Walther	Walthra	k1gFnPc2	Walthra
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Vogelweide	Vogelweid	k1gInSc5	Vogelweid
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
středověkých	středověký	k2eAgMnPc2d1	středověký
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
Hildegarda	Hildegarda	k1gFnSc1	Hildegarda
z	z	k7c2	z
Bingenu	Bingen	k1gInSc2	Bingen
literárně	literárně	k6eAd1	literárně
zpracovala	zpracovat	k5eAaPmAgFnS	zpracovat
své	svůj	k3xOyFgFnPc4	svůj
mystické	mystický	k2eAgFnPc4d1	mystická
vize	vize	k1gFnPc4	vize
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
německé	německý	k2eAgMnPc4d1	německý
autory	autor	k1gMnPc4	autor
patří	patřit	k5eAaImIp3nP	patřit
básníci	básník	k1gMnPc1	básník
a	a	k8xC	a
dramatikové	dramatik	k1gMnPc1	dramatik
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
von	von	k1gInSc4	von
Goethe	Goeth	k1gInSc2	Goeth
a	a	k8xC	a
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
.	.	kIx.	.
</s>
<s>
Literárně	literárně	k6eAd1	literárně
významní	významný	k2eAgMnPc1d1	významný
jsou	být	k5eAaImIp3nP	být
bratři	bratr	k1gMnPc1	bratr
Grimmové	Grimmová	k1gFnSc2	Grimmová
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
klasikům	klasik	k1gMnPc3	klasik
německé	německý	k2eAgFnSc2d1	německá
literatury	literatura	k1gFnSc2	literatura
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Heinrich	Heinrich	k1gMnSc1	Heinrich
Heine	Hein	k1gInSc5	Hein
<g/>
,	,	kIx,	,
Gotthold	Gotthold	k1gMnSc1	Gotthold
Ephraim	Ephraima	k1gFnPc2	Ephraima
Lessing	Lessing	k1gInSc1	Lessing
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
von	von	k1gInSc4	von
Kleist	Kleist	k1gMnSc1	Kleist
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Theodor	Theodor	k1gMnSc1	Theodor
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Hölderlin	Hölderlin	k1gInSc1	Hölderlin
<g/>
,	,	kIx,	,
Novalis	Novalis	k1gInSc1	Novalis
<g/>
,	,	kIx,	,
Theodor	Theodor	k1gMnSc1	Theodor
Fontane	Fontan	k1gInSc5	Fontan
či	či	k8xC	či
Georg	Georg	k1gMnSc1	Georg
Büchner	Büchner	k1gMnSc1	Büchner
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
nositeli	nositel	k1gMnPc7	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
postupně	postupně	k6eAd1	postupně
Theodor	Theodor	k1gMnSc1	Theodor
Mommsen	Mommsen	k2eAgMnSc1d1	Mommsen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Heyse	Heyse	k1gFnSc2	Heyse
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Gerhart	Gerhart	k1gInSc1	Gerhart
Hauptmann	Hauptmann	k1gInSc1	Hauptmann
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Hermann	Hermann	k1gMnSc1	Hermann
Hesse	Hesse	k1gFnSc2	Hesse
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
Böll	Böll	k1gMnSc1	Böll
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
a	a	k8xC	a
Günter	Günter	k1gInSc1	Günter
Grass	Grassa	k1gFnPc2	Grassa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Herta	Herta	k1gFnSc1	Herta
Müllerová	Müllerová	k1gFnSc1	Müllerová
<g/>
,	,	kIx,	,
německo-rumunská	německoumunský	k2eAgFnSc1d1	německo-rumunský
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Leureáty	Leureáta	k1gFnPc1	Leureáta
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
Nelly	Nella	k1gFnPc1	Nella
Sachsová	Sachsový	k2eAgFnSc1d1	Sachsový
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Mann	Mann	k1gMnSc1	Mann
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Christoph	Christoph	k1gMnSc1	Christoph
Eucken	Euckno	k1gNnPc2	Euckno
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
autorům	autor	k1gMnPc3	autor
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bývá	bývat	k5eAaImIp3nS	bývat
řazen	řadit	k5eAaImNgInS	řadit
i	i	k9	i
Bertolt	Bertolt	k2eAgMnSc1d1	Bertolt
Brecht	Brecht	k1gMnSc1	Brecht
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
Mann	Mann	k1gMnSc1	Mann
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Döblin	Döblin	k2eAgMnSc1d1	Döblin
nebo	nebo	k8xC	nebo
Patrick	Patrick	k1gMnSc1	Patrick
Süskind	Süskind	k1gMnSc1	Süskind
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
bestsellerem	bestseller	k1gInSc7	bestseller
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc4	klid
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
Erich	Erich	k1gMnSc1	Erich
Maria	Mario	k1gMnSc2	Mario
Remarque	Remarqu	k1gFnSc2	Remarqu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dětské	dětský	k2eAgFnSc2d1	dětská
literatury	literatura	k1gFnSc2	literatura
vynikl	vyniknout	k5eAaPmAgMnS	vyniknout
například	například	k6eAd1	například
Erich	Erich	k1gMnSc1	Erich
Kästner	Kästner	k1gMnSc1	Kästner
(	(	kIx(	(
<g/>
Emil	Emil	k1gMnSc1	Emil
a	a	k8xC	a
detektivové	detektiv	k1gMnPc1	detektiv
<g/>
)	)	kIx)	)
či	či	k8xC	či
Michael	Michael	k1gMnSc1	Michael
Ende	End	k1gFnSc2	End
(	(	kIx(	(
<g/>
Nekonečný	konečný	k2eNgInSc1d1	nekonečný
příběh	příběh	k1gInSc1	příběh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
populární	populární	k2eAgFnSc2d1	populární
literatury	literatura	k1gFnSc2	literatura
Karl	Karl	k1gMnSc1	Karl
May	May	k1gMnSc1	May
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Frankfurtský	frankfurtský	k2eAgInSc1d1	frankfurtský
knižní	knižní	k2eAgInSc1d1	knižní
veletrh	veletrh	k1gInSc1	veletrh
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
knižních	knižní	k2eAgInPc2d1	knižní
veletrhů	veletrh	k1gInPc2	veletrh
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
evropské	evropský	k2eAgFnSc2d1	Evropská
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
německých	německý	k2eAgMnPc2d1	německý
hudebních	hudební	k2eAgMnPc2d1	hudební
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
čelním	čelní	k2eAgMnPc3d1	čelní
tvůrcům	tvůrce	k1gMnPc3	tvůrce
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
patřili	patřit	k5eAaImAgMnP	patřit
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
,	,	kIx,	,
Georg	Georg	k1gMnSc1	Georg
Friedrich	Friedrich	k1gMnSc1	Friedrich
Händel	Händlo	k1gNnPc2	Händlo
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
později	pozdě	k6eAd2	pozdě
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ludwig	Ludwig	k1gMnSc1	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Maria	Maria	k1gFnSc1	Maria
von	von	k1gInSc1	von
Weber	weber	k1gInSc1	weber
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gMnSc1	Schumann
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
Mendelssohn	Mendelssohna	k1gFnPc2	Mendelssohna
Bartholdy	Bartholda	k1gFnSc2	Bartholda
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
Brahms	Brahms	k1gMnSc1	Brahms
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Strauss	Straussa	k1gFnPc2	Straussa
<g/>
.	.	kIx.	.
</s>
<s>
Německou	německý	k2eAgFnSc4d1	německá
hudbu	hudba	k1gFnSc4	hudba
silně	silně	k6eAd1	silně
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
i	i	k9	i
Christoph	Christoph	k1gMnSc1	Christoph
Willibald	Willibald	k1gMnSc1	Willibald
Gluck	Gluck	k1gMnSc1	Gluck
<g/>
,	,	kIx,	,
Georg	Georg	k1gMnSc1	Georg
Philipp	Philipp	k1gMnSc1	Philipp
Telemann	Telemann	k1gMnSc1	Telemann
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Orff	Orff	k1gMnSc1	Orff
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Philipp	Philipp	k1gMnSc1	Philipp
Emanuel	Emanuel	k1gMnSc1	Emanuel
Bach	Bach	k1gMnSc1	Bach
<g/>
,	,	kIx,	,
Dietrich	Dietrich	k1gMnSc1	Dietrich
Buxtehude	Buxtehud	k1gInSc5	Buxtehud
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Hindemith	Hindemith	k1gMnSc1	Hindemith
<g/>
,	,	kIx,	,
Giacomo	Giacoma	k1gFnSc5	Giacoma
Meyerbeer	Meyerbeer	k1gMnSc1	Meyerbeer
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Weill	Weill	k1gMnSc1	Weill
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schütz	Schütz	k1gMnSc1	Schütz
nebo	nebo	k8xC	nebo
Max	Max	k1gMnSc1	Max
Bruch	bruch	k1gInSc1	bruch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
má	mít	k5eAaImIp3nS	mít
silnou	silný	k2eAgFnSc4d1	silná
tradici	tradice	k1gFnSc4	tradice
sborového	sborový	k2eAgInSc2d1	sborový
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
rozvoj	rozvoj	k1gInSc4	rozvoj
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
přerušen	přerušit	k5eAaPmNgInS	přerušit
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
však	však	k9	však
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
velký	velký	k2eAgInSc1d1	velký
rozmach	rozmach	k1gInSc1	rozmach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
Německo	Německo	k1gNnSc4	Německo
téměř	téměř	k6eAd1	téměř
centrální	centrální	k2eAgFnSc4d1	centrální
důležitost	důležitost	k1gFnSc4	důležitost
v	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
životě	život	k1gInSc6	život
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
prvotřídních	prvotřídní	k2eAgFnPc2d1	prvotřídní
hudebních	hudební	k2eAgFnPc2d1	hudební
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
konzervatoří	konzervatoř	k1gFnPc2	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
také	také	k9	také
zemí	zem	k1gFnSc7	zem
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
symfonických	symfonický	k2eAgInPc2d1	symfonický
orchestrů	orchestr	k1gInPc2	orchestr
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
92	[number]	k4	92
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
patří	patřit	k5eAaImIp3nP	patřit
tzv.	tzv.	kA	tzv.
A-orchestry	Archestr	k1gInPc4	A-orchestr
<g/>
:	:	kIx,	:
Bamberger	Bamberger	k1gMnSc1	Bamberger
Symphoniker	Symphoniker	k1gMnSc1	Symphoniker
(	(	kIx(	(
<g/>
Bamberg	Bamberg	k1gMnSc1	Bamberg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Berlínští	berlínský	k2eAgMnPc1d1	berlínský
filharmonikové	filharmonik	k1gMnPc1	filharmonik
(	(	kIx(	(
<g/>
Berliner	Berliner	k1gMnSc1	Berliner
Philharmoniker	Philharmoniker	k1gMnSc1	Philharmoniker
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gewandhausorchester	Gewandhausorchester	k1gInSc1	Gewandhausorchester
(	(	kIx(	(
<g/>
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hr-Sinfonieorchester	hr-Sinfonieorchester	k1gInSc1	hr-Sinfonieorchester
(	(	kIx(	(
<g/>
rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
filharmonie	filharmonie	k1gFnSc1	filharmonie
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Münchner	Münchner	k1gMnSc1	Münchner
Philharmoniker	Philharmoniker	k1gMnSc1	Philharmoniker
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sächsische	Sächsische	k1gNnSc1	Sächsische
Staatskapelle	Staatskapelle	k1gFnSc2	Staatskapelle
Dresden	Dresdna	k1gFnPc2	Dresdna
(	(	kIx(	(
<g/>
Drážďany	Drážďany	k1gInPc1	Drážďany
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Staatskapelle	Staatskapelle	k1gNnSc1	Staatskapelle
Berlin	berlina	k1gFnPc2	berlina
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
šéfdirigentem	šéfdirigent	k1gMnSc7	šéfdirigent
Danielem	Daniel	k1gMnSc7	Daniel
Barenboimem	Barenboim	k1gMnSc7	Barenboim
<g/>
,	,	kIx,	,
Symphonieorchester	Symphonieorchester	k1gMnSc1	Symphonieorchester
des	des	k1gNnSc2	des
Bayerischen	Bayerischna	k1gFnPc2	Bayerischna
Rundfunks	Rundfunksa	k1gFnPc2	Rundfunksa
(	(	kIx(	(
<g/>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1961-1979	[number]	k4	1961-1979
Rafael	Rafael	k1gMnSc1	Rafael
Kubelík	Kubelík	k1gMnSc1	Kubelík
<g/>
)	)	kIx)	)
a	a	k8xC	a
WDR	WDR	kA	WDR
Sinfonieorchester	Sinfonieorchester	k1gMnSc1	Sinfonieorchester
Köln	Köln	k1gMnSc1	Köln
(	(	kIx(	(
<g/>
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
orchestrech	orchestr	k1gInPc6	orchestr
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgMnPc2d1	významný
dirigentů	dirigent	k1gMnPc2	dirigent
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Valerij	Valerij	k1gMnSc1	Valerij
Gergijev	Gergijev	k1gMnSc1	Gergijev
<g/>
,	,	kIx,	,
Simon	Simon	k1gMnSc1	Simon
Rattle	Rattle	k1gFnSc1	Rattle
<g/>
,	,	kIx,	,
Jukka-Pekka	Jukka-Pekka	k1gFnSc1	Jukka-Pekka
Saraste	Sarast	k1gMnSc5	Sarast
<g/>
,	,	kIx,	,
Christian	Christian	k1gMnSc1	Christian
Thielemann	Thielemann	k1gMnSc1	Thielemann
a	a	k8xC	a
Kirill	Kirill	k1gMnSc1	Kirill
Petrenko	Petrenka	k1gFnSc5	Petrenka
<g/>
.	.	kIx.	.
</s>
<s>
Symfonické	symfonický	k2eAgInPc1d1	symfonický
orchestry	orchestr	k1gInPc1	orchestr
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
velké	velká	k1gFnSc2	velká
koncertní	koncertní	k2eAgInPc1d1	koncertní
sály	sál	k1gInPc1	sál
s	s	k7c7	s
výbornou	výborný	k2eAgFnSc7d1	výborná
akustikou	akustika	k1gFnSc7	akustika
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
(	(	kIx(	(
<g/>
Gasteig	Gasteig	k1gMnSc1	Gasteig
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
byla	být	k5eAaImAgFnS	být
přestavěna	přestavěn	k2eAgFnSc1d1	přestavěna
budova	budova	k1gFnSc1	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
skladu	sklad	k1gInSc2	sklad
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
Elbphilharmonie	Elbphilharmonie	k1gFnPc4	Elbphilharmonie
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
moderním	moderní	k2eAgInSc7d1	moderní
koncertním	koncertní	k2eAgInSc7d1	koncertní
sálem	sál	k1gInSc7	sál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
vynikající	vynikající	k2eAgFnSc4d1	vynikající
akustiku	akustika	k1gFnSc4	akustika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
největším	veliký	k2eAgInSc7d3	veliký
hudebním	hudební	k2eAgInSc7d1	hudební
trhem	trh	k1gInSc7	trh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
hudebních	hudební	k2eAgMnPc2d1	hudební
sólistů	sólista	k1gMnPc2	sólista
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
instrumentalisty	instrumentalista	k1gMnPc7	instrumentalista
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
jmenovat	jmenovat	k5eAaBmF	jmenovat
zejména	zejména	k9	zejména
světově	světově	k6eAd1	světově
proslulé	proslulý	k2eAgFnPc1d1	proslulá
houslistky	houslistka	k1gFnPc1	houslistka
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Anne-Sophie	Anne-Sophie	k1gFnPc1	Anne-Sophie
Mutter	Muttra	k1gFnPc2	Muttra
<g/>
,	,	kIx,	,
Isabelle	Isabell	k1gMnPc4	Isabell
Faust	Faust	k1gFnSc4	Faust
a	a	k8xC	a
Julia	Julius	k1gMnSc4	Julius
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
renomé	renomé	k1gNnSc1	renomé
mají	mít	k5eAaImIp3nP	mít
mj.	mj.	kA	mj.
sopranistka	sopranistka	k1gFnSc1	sopranistka
Diana	Diana	k1gFnSc1	Diana
Damrauová	Damrauová	k1gFnSc1	Damrauová
a	a	k8xC	a
tenorista	tenorista	k1gMnSc1	tenorista
Jonas	Jonas	k1gMnSc1	Jonas
Kaufmann	Kaufmann	k1gMnSc1	Kaufmann
<g/>
.	.	kIx.	.
</s>
<s>
Vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
i	i	k8xC	i
pianistka	pianistka	k1gFnSc1	pianistka
Clara	Clara	k1gFnSc1	Clara
Schumannová	Schumannová	k1gFnSc1	Schumannová
či	či	k8xC	či
varhaník	varhaník	k1gMnSc1	varhaník
Johann	Johann	k1gMnSc1	Johann
Pachelbel	Pachelbel	k1gMnSc1	Pachelbel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
bohatému	bohatý	k2eAgInSc3d1	bohatý
kulturnímu	kulturní	k2eAgInSc3d1	kulturní
životu	život	k1gInSc3	život
přispívá	přispívat	k5eAaImIp3nS	přispívat
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
při	při	k7c6	při
operních	operní	k2eAgNnPc6d1	operní
představeních	představení	k1gNnPc6	představení
a	a	k8xC	a
koncertech	koncert	k1gInPc6	koncert
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
početní	početní	k2eAgFnPc1d1	početní
umělci	umělec	k1gMnPc7	umělec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filmovou	filmový	k2eAgFnSc7d1	filmová
hudbou	hudba	k1gFnSc7	hudba
proslul	proslout	k5eAaPmAgMnS	proslout
Hans	Hans	k1gMnSc1	Hans
Zimmer	Zimmer	k1gMnSc1	Zimmer
(	(	kIx(	(
<g/>
Lví	lví	k2eAgMnSc1d1	lví
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Piráti	pirát	k1gMnPc1	pirát
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
,	,	kIx,	,
Gladiátor	gladiátor	k1gMnSc1	gladiátor
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
jazzové	jazzový	k2eAgFnSc2d1	jazzová
hudby	hudba	k1gFnSc2	hudba
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgMnSc1d3	nejznámější
Adam	Adam	k1gMnSc1	Adam
Taubitz	Taubitz	k1gMnSc1	Taubitz
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Brötzmann	Brötzmann	k1gMnSc1	Brötzmann
<g/>
,	,	kIx,	,
Theo	Thea	k1gFnSc5	Thea
Jörgensmann	Jörgensmann	k1gMnSc1	Jörgensmann
<g/>
,	,	kIx,	,
Till	Till	k1gMnSc1	Till
Brönner	Brönner	k1gMnSc1	Brönner
a	a	k8xC	a
Eberhard	Eberhard	k1gMnSc1	Eberhard
Weber	Weber	k1gMnSc1	Weber
<g/>
.	.	kIx.	.
</s>
<s>
Slavným	slavný	k2eAgMnSc7d1	slavný
kapelníkem	kapelník	k1gMnSc7	kapelník
big	big	k?	big
bandu	band	k1gInSc6	band
byl	být	k5eAaImAgMnS	být
James	James	k1gMnSc1	James
Last	Last	k1gMnSc1	Last
<g/>
.	.	kIx.	.
</s>
<s>
Proslulými	proslulý	k2eAgMnPc7d1	proslulý
operními	operní	k2eAgMnPc7d1	operní
pěvci	pěvec	k1gMnPc7	pěvec
byli	být	k5eAaImAgMnP	být
Dietrich	Dietrich	k1gInSc4	Dietrich
Fischer-Dieskau	Fischer-Dieskaus	k1gInSc2	Fischer-Dieskaus
<g/>
,	,	kIx,	,
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
Schwarzkopfová	Schwarzkopfový	k2eAgFnSc1d1	Schwarzkopfová
nebo	nebo	k8xC	nebo
Christa	Christ	k1gMnSc4	Christ
Ludwigová	Ludwigový	k2eAgFnSc1d1	Ludwigová
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
odvedly	odvést	k5eAaPmAgFnP	odvést
pionýrskou	pionýrský	k2eAgFnSc4d1	Pionýrská
práci	práce	k1gFnSc4	práce
skupina	skupina	k1gFnSc1	skupina
Kraftwerk	Kraftwerk	k1gInSc4	Kraftwerk
<g/>
,	,	kIx,	,
Karlheinz	Karlheinz	k1gInSc4	Karlheinz
Stockhausen	Stockhausna	k1gFnPc2	Stockhausna
a	a	k8xC	a
Klaus	Klaus	k1gMnSc1	Klaus
Schulze	Schulz	k1gMnSc2	Schulz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
popové	popový	k2eAgFnSc6d1	popová
a	a	k8xC	a
rockové	rockový	k2eAgFnSc6d1	rocková
hudbě	hudba	k1gFnSc6	hudba
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
hranice	hranice	k1gFnPc1	hranice
Německa	Německo	k1gNnSc2	Německo
umělci	umělec	k1gMnPc7	umělec
jako	jako	k8xC	jako
Udo	Udo	k1gMnPc7	Udo
Jürgens	Jürgensa	k1gFnPc2	Jürgensa
<g/>
,	,	kIx,	,
Udo	Udo	k1gMnSc1	Udo
Lindenberg	Lindenberg	k1gMnSc1	Lindenberg
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
Grönemeyer	Grönemeyer	k1gMnSc1	Grönemeyer
<g/>
,	,	kIx,	,
Nena	Nena	k1gMnSc1	Nena
<g/>
,	,	kIx,	,
Sandra	Sandra	k1gFnSc1	Sandra
a	a	k8xC	a
Xavier	Xavier	k1gMnSc1	Xavier
Naidoo	Naidoo	k1gMnSc1	Naidoo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
Accept	Accept	k1gInSc1	Accept
<g/>
,	,	kIx,	,
Helloween	Helloween	k1gInSc1	Helloween
<g/>
,	,	kIx,	,
Gamma	Gamma	k1gFnSc1	Gamma
Ray	Ray	k1gFnSc1	Ray
<g/>
,	,	kIx,	,
Die	Die	k1gFnSc1	Die
Toten	toten	k1gInSc1	toten
Hosen	Hosen	k1gInSc1	Hosen
<g/>
,	,	kIx,	,
Die	Die	k1gMnSc5	Die
Ärzte	Ärzt	k1gMnSc5	Ärzt
<g/>
,	,	kIx,	,
Alphaville	Alphaville	k1gNnPc1	Alphaville
<g/>
,	,	kIx,	,
Scooter	Scooter	k1gInSc1	Scooter
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc1	Tokio
Hotel	hotel	k1gInSc1	hotel
<g/>
,	,	kIx,	,
Blind	Blind	k1gMnSc1	Blind
Guardian	Guardian	k1gMnSc1	Guardian
<g/>
,	,	kIx,	,
Modern	Modern	k1gMnSc1	Modern
Talking	Talking	k1gInSc1	Talking
<g/>
,	,	kIx,	,
Scorpions	Scorpions	k1gInSc1	Scorpions
a	a	k8xC	a
Rammstein	Rammstein	k1gInSc1	Rammstein
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
s	s	k7c7	s
německými	německý	k2eAgInPc7d1	německý
texty	text	k1gInPc7	text
i	i	k8xC	i
v	v	k7c6	v
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
hitparádách	hitparáda	k1gFnPc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
těší	těšit	k5eAaImIp3nS	těšit
velké	velký	k2eAgFnSc3d1	velká
popularitě	popularita	k1gFnSc3	popularita
také	také	k9	také
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
německým	německý	k2eAgMnSc7d1	německý
malířem	malíř	k1gMnSc7	malíř
je	být	k5eAaImIp3nS	být
Albrecht	Albrecht	k1gMnSc1	Albrecht
Dürer	Dürer	k1gMnSc1	Dürer
<g/>
.	.	kIx.	.
</s>
<s>
Dürerem	Dürero	k1gNnSc7	Dürero
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
inspirován	inspirován	k2eAgMnSc1d1	inspirován
i	i	k8xC	i
dřevorytec	dřevorytec	k1gMnSc1	dřevorytec
Hans	Hans	k1gMnSc1	Hans
Holbein	Holbein	k1gMnSc1	Holbein
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
renesančním	renesanční	k2eAgMnSc7d1	renesanční
malířem	malíř	k1gMnSc7	malíř
byl	být	k5eAaImAgMnS	být
Lucas	Lucas	k1gMnSc1	Lucas
Cranach	Cranach	k1gMnSc1	Cranach
starší	starší	k1gMnSc1	starší
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
řazen	řadit	k5eAaImNgMnS	řadit
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
dunajské	dunajský	k2eAgFnSc6d1	Dunajská
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Albrecht	Albrecht	k1gMnSc1	Albrecht
Altdorfer	Altdorfer	k1gMnSc1	Altdorfer
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
renesanci	renesance	k1gFnSc3	renesance
náleží	náležet	k5eAaImIp3nS	náležet
i	i	k9	i
Matthias	Matthias	k1gMnSc1	Matthias
Grünewald	Grünewald	k1gMnSc1	Grünewald
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgMnPc2d3	nejvýraznější
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
pocházejících	pocházející	k2eAgMnPc2d1	pocházející
barokních	barokní	k2eAgMnPc2d1	barokní
malířů	malíř	k1gMnPc2	malíř
je	být	k5eAaImIp3nS	být
mistr	mistr	k1gMnSc1	mistr
Cosmas	Cosmas	k1gMnSc1	Cosmas
Damian	Damian	k1gMnSc1	Damian
Asam	Asam	k1gMnSc1	Asam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významným	významný	k2eAgMnSc7d1	významný
dřevorytcem	dřevorytec	k1gMnSc7	dřevorytec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Busch	Busch	k1gMnSc1	Busch
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
romantických	romantický	k2eAgMnPc2d1	romantický
malířů	malíř	k1gMnPc2	malíř
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
Caspar	Caspar	k1gMnSc1	Caspar
David	David	k1gMnSc1	David
Friedrich	Friedrich	k1gMnSc1	Friedrich
a	a	k8xC	a
Carl	Carl	k1gMnSc1	Carl
Spitzweg	Spitzweg	k1gMnSc1	Spitzweg
<g/>
.	.	kIx.	.
</s>
<s>
Maria	Maria	k1gFnSc1	Maria
Sibylla	Sibylla	k1gFnSc1	Sibylla
Merianová	Merianová	k1gFnSc1	Merianová
proslula	proslout	k5eAaPmAgFnS	proslout
kresbami	kresba	k1gFnPc7	kresba
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Impresionistické	impresionistický	k2eAgInPc1d1	impresionistický
vlivy	vliv	k1gInPc1	vliv
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
Max	Max	k1gMnSc1	Max
Liebermann	Liebermann	k1gMnSc1	Liebermann
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
osobností	osobnost	k1gFnSc7	osobnost
avantgardy	avantgarda	k1gFnSc2	avantgarda
byl	být	k5eAaImAgMnS	být
Max	Max	k1gMnSc1	Max
Ernst	Ernst	k1gMnSc1	Ernst
či	či	k8xC	či
expresionisté	expresionista	k1gMnPc1	expresionista
Franz	Franza	k1gFnPc2	Franza
Marc	Marc	k1gInSc1	Marc
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Ludwig	Ludwig	k1gMnSc1	Ludwig
Kirchner	Kirchner	k1gMnSc1	Kirchner
a	a	k8xC	a
Käthe	Käthe	k1gFnSc1	Käthe
Kollwitzová	Kollwitzová	k1gFnSc1	Kollwitzová
<g/>
.	.	kIx.	.
</s>
<s>
Expresionismus	expresionismus	k1gInSc1	expresionismus
sehrál	sehrát	k5eAaPmAgInS	sehrát
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
umění	umění	k1gNnSc6	umění
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
roli	role	k1gFnSc4	role
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
přímo	přímo	k6eAd1	přímo
vnikl	vniknout	k5eAaPmAgInS	vniknout
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
a	a	k8xC	a
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neoexpresionismus	Neoexpresionismus	k1gInSc4	Neoexpresionismus
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
Georg	Georg	k1gMnSc1	Georg
Baselitz	Baselitz	k1gMnSc1	Baselitz
či	či	k8xC	či
Anselm	Anselm	k1gMnSc1	Anselm
Kiefer	Kiefer	k1gMnSc1	Kiefer
<g/>
.	.	kIx.	.
</s>
<s>
Hyperrealismus	hyperrealismus	k1gInSc1	hyperrealismus
a	a	k8xC	a
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
užití	užití	k1gNnSc1	užití
fotografie	fotografia	k1gFnSc2	fotografia
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
Gerharda	Gerhard	k1gMnSc4	Gerhard
Richtera	Richter	k1gMnSc4	Richter
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kapitalistický	kapitalistický	k2eAgInSc1d1	kapitalistický
realismus	realismus	k1gInSc1	realismus
<g/>
"	"	kIx"	"
Sigmara	Sigmara	k1gFnSc1	Sigmara
Polkeho	Polke	k1gMnSc2	Polke
<g/>
,	,	kIx,	,
performanční	performanční	k2eAgNnSc4d1	performanční
a	a	k8xC	a
happeningové	happeningový	k2eAgNnSc4d1	happeningové
umění	umění	k1gNnSc4	umění
Josepha	Joseph	k1gMnSc2	Joseph
Beuyse	Beuys	k1gMnSc2	Beuys
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
karikaturami	karikatura	k1gFnPc7	karikatura
prosluli	proslout	k5eAaPmAgMnP	proslout
George	Georg	k1gMnSc4	Georg
Grosz	Grosz	k1gMnSc1	Grosz
či	či	k8xC	či
Otto	Otto	k1gMnSc1	Otto
Dix	Dix	k1gMnSc1	Dix
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějšími	slavný	k2eAgMnPc7d3	nejslavnější
sochaři	sochař	k1gMnPc7	sochař
jsou	být	k5eAaImIp3nP	být
Hans	Hans	k1gMnSc1	Hans
Arp	Arp	k1gMnSc1	Arp
a	a	k8xC	a
Jörg	Jörg	k1gMnSc1	Jörg
Immendorff	Immendorff	k1gMnSc1	Immendorff
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
je	být	k5eAaImIp3nS	být
festival	festival	k1gInSc1	festival
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
documenta	documento	k1gNnSc2	documento
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každých	každý	k3xTgNnPc2	každý
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kassel	Kassel	k1gInSc1	Kassel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Architektura	architektura	k1gFnSc1	architektura
===	===	k?	===
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
má	mít	k5eAaImIp3nS	mít
bohaté	bohatý	k2eAgFnPc4d1	bohatá
a	a	k8xC	a
mnohotvárné	mnohotvárný	k2eAgFnPc4d1	mnohotvárná
dějiny	dějiny	k1gFnPc4	dějiny
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
dějinami	dějiny	k1gFnPc7	dějiny
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
sousedních	sousední	k2eAgFnPc6d1	sousední
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Architektonické	architektonický	k2eAgFnPc4d1	architektonická
památky	památka	k1gFnPc4	památka
pochází	pocházet	k5eAaImIp3nS	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
římské	římský	k2eAgFnSc2d1	římská
antiky	antika	k1gFnSc2	antika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
památky	památka	k1gFnPc1	památka
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
dochovaly	dochovat	k5eAaPmAgInP	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
památky	památka	k1gFnPc1	památka
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
románským	románský	k2eAgInSc7d1	románský
slohem	sloh	k1gInSc7	sloh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vstupní	vstupní	k2eAgInSc1d1	vstupní
objekt	objekt	k1gInSc1	objekt
do	do	k7c2	do
města	město	k1gNnSc2	město
Lorsch	Lorscha	k1gFnPc2	Lorscha
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
Torhalle	Torhalle	k1gInSc1	Torhalle
<g/>
.	.	kIx.	.
</s>
<s>
Torhalle	Torhalle	k6eAd1	Torhalle
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
vývoj	vývoj	k1gInSc1	vývoj
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
románskému	románský	k2eAgInSc3d1	románský
slohu	sloh	k1gInSc3	sloh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přišel	přijít	k5eAaPmAgInS	přijít
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1030	[number]	k4	1030
<g/>
.	.	kIx.	.
</s>
<s>
Gotika	gotika	k1gFnSc1	gotika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc1	první
stavby	stavba	k1gFnPc1	stavba
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
pochází	pocházet	k5eAaImIp3nS	pocházet
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1230	[number]	k4	1230
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Chrám	chrám	k1gInSc4	chrám
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Trevíru	Trevír	k1gInSc6	Trevír
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1520	[number]	k4	1520
přišla	přijít	k5eAaPmAgFnS	přijít
renesance	renesance	k1gFnSc1	renesance
z	z	k7c2	z
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Itálie	Itálie	k1gFnSc2	Itálie
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnPc1d1	římská
<g/>
,	,	kIx,	,
ukázka	ukázka	k1gFnSc1	ukázka
je	být	k5eAaImIp3nS	být
radnice	radnice	k1gFnSc1	radnice
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Augsburg	Augsburg	k1gInSc1	Augsburg
<g/>
.	.	kIx.	.
</s>
<s>
Baroko	baroko	k1gNnSc1	baroko
se	se	k3xPyFc4	se
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Německa	Německo	k1gNnSc2	Německo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1650	[number]	k4	1650
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
díla	dílo	k1gNnPc1	dílo
od	od	k7c2	od
Balthasara	Balthasar	k1gMnSc2	Balthasar
Neumanna	Neumann	k1gMnSc2	Neumann
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kostel	kostel	k1gInSc1	kostel
ve	v	k7c4	v
Wies	Wies	k1gInSc4	Wies
a	a	k8xC	a
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Ludwigsburgu	Ludwigsburg	k1gInSc6	Ludwigsburg
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
období	období	k1gNnSc4	období
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgFnPc7d3	nejdůležitější
stavbami	stavba	k1gFnPc7	stavba
jsou	být	k5eAaImIp3nP	být
Staré	Staré	k2eAgNnSc1d1	Staré
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Charlottenhof	Charlottenhof	k1gInSc1	Charlottenhof
a	a	k8xC	a
Braniborská	braniborský	k2eAgFnSc1d1	Braniborská
brána	brána	k1gFnSc1	brána
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
historismu	historismus	k1gInSc2	historismus
(	(	kIx(	(
<g/>
období	období	k1gNnSc2	období
1840	[number]	k4	1840
až	až	k9	až
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
zámek	zámek	k1gInSc4	zámek
Neuschwanstein	Neuschwanstein	k2eAgInSc4d1	Neuschwanstein
a	a	k8xC	a
Berlínský	berlínský	k2eAgInSc4d1	berlínský
dóm	dóm	k1gInSc4	dóm
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
architektem	architekt	k1gMnSc7	architekt
neoklasicismu	neoklasicismus	k1gInSc2	neoklasicismus
byl	být	k5eAaImAgMnS	být
Karl	Karl	k1gMnSc1	Karl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schinkel	Schinkel	k1gMnSc1	Schinkel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
němečtí	německý	k2eAgMnPc1d1	německý
architekti	architekt	k1gMnPc1	architekt
přední	přední	k2eAgMnPc1d1	přední
představitelé	představitel	k1gMnPc1	představitel
klasické	klasický	k2eAgFnSc2d1	klasická
moderny	moderna	k1gFnSc2	moderna
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
Gropius	Gropius	k1gMnSc1	Gropius
<g/>
,	,	kIx,	,
Ludwig	Ludwig	k1gMnSc1	Ludwig
Mies	Miesa	k1gFnPc2	Miesa
van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS	drát
Rohe	Rohe	k1gFnSc1	Rohe
a	a	k8xC	a
škola	škola	k1gFnSc1	škola
architektury	architektura	k1gFnSc2	architektura
Bauhaus	Bauhaus	k1gInSc1	Bauhaus
udala	udat	k5eAaPmAgFnS	udat
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
architektura	architektura	k1gFnSc1	architektura
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
celosvětově	celosvětově	k6eAd1	celosvětově
využívá	využívat	k5eAaImIp3nS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Monumentální	monumentální	k2eAgFnSc1d1	monumentální
architektura	architektura	k1gFnSc1	architektura
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nacismu	nacismus	k1gInSc2	nacismus
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
tvář	tvář	k1gFnSc1	tvář
německých	německý	k2eAgFnPc2d1	německá
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
obnovy	obnova	k1gFnSc2	obnova
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
měl	mít	k5eAaImAgInS	mít
převahu	převaha	k1gFnSc4	převaha
pragmatismus	pragmatismus	k1gInSc1	pragmatismus
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
našla	najít	k5eAaPmAgFnS	najít
německá	německý	k2eAgFnSc1d1	německá
architektura	architektura	k1gFnSc1	architektura
nové	nový	k2eAgFnSc2d1	nová
formy	forma	k1gFnSc2	forma
projevu	projev	k1gInSc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
olympijský	olympijský	k2eAgInSc1d1	olympijský
stadion	stadion	k1gInSc1	stadion
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
německá	německý	k2eAgFnSc1d1	německá
architektura	architektura	k1gFnSc1	architektura
znovu	znovu	k6eAd1	znovu
našla	najít	k5eAaPmAgFnS	najít
svůj	svůj	k3xOyFgInSc4	svůj
styl	styl	k1gInSc4	styl
</s>
<s>
<g/>
Jeho	jeho	k3xOp3gMnSc7	jeho
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Frei	Free	k1gFnSc4	Free
Paul	Paul	k1gMnSc1	Paul
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
prestižní	prestižní	k2eAgFnSc2d1	prestižní
Pritzkerovy	Pritzkerův	k2eAgFnSc2d1	Pritzkerova
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
německým	německý	k2eAgMnSc7d1	německý
laureátem	laureát	k1gMnSc7	laureát
této	tento	k3xDgFnSc2	tento
"	"	kIx"	"
<g/>
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
architekturu	architektura	k1gFnSc4	architektura
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
Gottfried	Gottfried	k1gMnSc1	Gottfried
Böhm	Böhm	k1gMnSc1	Böhm
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
brutalismu	brutalismus	k1gInSc2	brutalismus
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
řady	řada	k1gFnSc2	řada
unikátních	unikátní	k2eAgFnPc2d1	unikátní
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
radnice	radnice	k1gFnSc1	radnice
v	v	k7c6	v
Bergisch	Bergis	k1gFnPc6	Bergis
Gladbachu	Gladbach	k1gInSc2	Gladbach
nebo	nebo	k8xC	nebo
kostel	kostel	k1gInSc4	kostel
ve	v	k7c6	v
Velbertu	Velbert	k1gInSc6	Velbert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
a	a	k8xC	a
stavební	stavební	k2eAgFnPc1d1	stavební
památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
UNESCO	UNESCO	kA	UNESCO
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
německých	německý	k2eAgInPc2d1	německý
příspěvků	příspěvek	k1gInPc2	příspěvek
ke	k	k7c3	k
Světovému	světový	k2eAgNnSc3d1	světové
kulturnímu	kulturní	k2eAgNnSc3d1	kulturní
dědictví	dědictví	k1gNnSc3	dědictví
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
15	[number]	k4	15
ze	z	k7c2	z
16	[number]	k4	16
německých	německý	k2eAgFnPc2d1	německá
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
byla	být	k5eAaImAgFnS	být
katedrála	katedrála	k1gFnSc1	katedrála
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgInSc7d1	poslední
přírůstkem	přírůstek	k1gInSc7	přírůstek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
jsou	být	k5eAaImIp3nP	být
Moderní	moderní	k2eAgFnPc1d1	moderní
berlínské	berlínský	k2eAgFnPc1d1	Berlínská
stavby	stavba	k1gFnPc1	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
a	a	k8xC	a
domácích	domácí	k2eAgMnPc2d1	domácí
turistů	turist	k1gMnPc2	turist
mají	mít	k5eAaImIp3nP	mít
mnohé	mnohý	k2eAgFnPc1d1	mnohá
památky	památka	k1gFnPc1	památka
z	z	k7c2	z
období	období	k1gNnSc2	období
německé	německý	k2eAgFnSc2d1	německá
romantiky	romantika	k1gFnSc2	romantika
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc1	zámek
Neuschwanstein	Neuschwanstein	k2eAgInSc1d1	Neuschwanstein
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
město	město	k1gNnSc1	město
Heidelberg	Heidelberg	k1gInSc1	Heidelberg
<g/>
,	,	kIx,	,
vinařské	vinařský	k2eAgNnSc1d1	vinařské
středisko	středisko	k1gNnSc1	středisko
Rüdesheim	Rüdesheim	k1gInSc1	Rüdesheim
<g/>
,	,	kIx,	,
památník	památník	k1gInSc1	památník
Walhalla	Walhallo	k1gNnSc2	Walhallo
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
a	a	k8xC	a
také	také	k9	také
centra	centrum	k1gNnSc2	centrum
středověkých	středověký	k2eAgNnPc2d1	středověké
měst	město	k1gNnPc2	město
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Rothenburg	Rothenburg	k1gInSc4	Rothenburg
ob	ob	k7c4	ob
der	drát	k5eAaImRp2nS	drát
Tauber	Tauber	k1gInSc4	Tauber
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oblibě	obliba	k1gFnSc6	obliba
získávají	získávat	k5eAaImIp3nP	získávat
i	i	k9	i
nákladně	nákladně	k6eAd1	nákladně
zmodernizovaná	zmodernizovaný	k2eAgNnPc1d1	zmodernizované
městská	městský	k2eAgNnPc1d1	Městské
centra	centrum	k1gNnPc1	centrum
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
industrializace	industrializace	k1gFnSc2	industrializace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
mnoha	mnoho	k4c6	mnoho
velkoměstech	velkoměsto	k1gNnPc6	velkoměsto
Severního	severní	k2eAgNnSc2d1	severní
Porýní-Vestfálska	Porýní-Vestfálsko	k1gNnSc2	Porýní-Vestfálsko
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
bombardování	bombardování	k1gNnSc2	bombardování
v	v	k7c6	v
období	období	k1gNnSc6	období
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
ztracena	ztracen	k2eAgNnPc4d1	ztraceno
původní	původní	k2eAgNnPc4d1	původní
centra	centrum	k1gNnPc4	centrum
mnoha	mnoho	k4c2	mnoho
německých	německý	k2eAgNnPc2d1	německé
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
(	(	kIx(	(
<g/>
Brémy	Brémy	k1gFnPc1	Brémy
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Norimberk	Norimberk	k1gInSc1	Norimberk
<g/>
,	,	kIx,	,
Drážďany	Drážďany	k1gInPc1	Drážďany
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
a	a	k8xC	a
středně	středně	k6eAd1	středně
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
též	též	k9	též
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
nákladem	náklad	k1gInSc7	náklad
obnovována	obnovovat	k5eAaImNgFnS	obnovovat
a	a	k8xC	a
přestavována	přestavovat	k5eAaImNgFnS	přestavovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
malých	malý	k2eAgFnPc2d1	malá
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
starobylá	starobylý	k2eAgFnSc1d1	starobylá
centra	centrum	k1gNnPc1	centrum
měst	město	k1gNnPc2	město
zachovala	zachovat	k5eAaPmAgFnS	zachovat
často	často	k6eAd1	často
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
obnovena	obnoven	k2eAgFnSc1d1	obnovena
(	(	kIx(	(
<g/>
Dinkelsbühl	Dinkelsbühl	k1gFnSc1	Dinkelsbühl
<g/>
,	,	kIx,	,
Gosslar	Gosslar	k1gInSc1	Gosslar
<g/>
,	,	kIx,	,
Wetzlar	Wetzlar	k1gInSc1	Wetzlar
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Muzea	muzeum	k1gNnSc2	muzeum
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přes	přes	k7c4	přes
6000	[number]	k4	6000
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgInPc3	který
patří	patřit	k5eAaImIp3nP	patřit
soukromé	soukromý	k2eAgFnPc1d1	soukromá
i	i	k8xC	i
veřejné	veřejný	k2eAgFnPc1d1	veřejná
sbírky	sbírka	k1gFnPc1	sbírka
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
a	a	k8xC	a
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
a	a	k8xC	a
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
patří	patřit	k5eAaImIp3nS	patřit
Německé	německý	k2eAgNnSc1d1	německé
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
největší	veliký	k2eAgNnSc1d3	veliký
přírodovědeckotechnické	přírodovědeckotechnický	k2eAgNnSc1d1	přírodovědeckotechnický
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Germanische	Germanische	k1gInSc1	Germanische
Nationalmuseum	Nationalmuseum	k1gInSc1	Nationalmuseum
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
se	s	k7c7	s
sbírkami	sbírka	k1gFnPc7	sbírka
z	z	k7c2	z
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
od	od	k7c2	od
rané	raný	k2eAgFnSc2d1	raná
historie	historie	k1gFnSc2	historie
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
celosvětově	celosvětově	k6eAd1	celosvětově
důležitá	důležitý	k2eAgNnPc1d1	důležité
muzea	muzeum	k1gNnPc1	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Média	médium	k1gNnPc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
počátcích	počátek	k1gInPc6	počátek
němečtí	německý	k2eAgMnPc1d1	německý
vynálezci	vynálezce	k1gMnPc1	vynálezce
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
televizního	televizní	k2eAgMnSc2d1	televizní
a	a	k8xC	a
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
probíhá	probíhat	k5eAaImIp3nS	probíhat
jak	jak	k6eAd1	jak
pozemními	pozemní	k2eAgInPc7d1	pozemní
vysílači	vysílač	k1gInPc7	vysílač
<g/>
,	,	kIx,	,
tak	tak	k9	tak
přes	přes	k7c4	přes
družicové	družicový	k2eAgNnSc4d1	družicové
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
je	být	k5eAaImIp3nS	být
i	i	k9	i
kabelová	kabelový	k2eAgFnSc1d1	kabelová
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
probíhá	probíhat	k5eAaImIp3nS	probíhat
digitalizace	digitalizace	k1gFnSc1	digitalizace
televizního	televizní	k2eAgNnSc2d1	televizní
a	a	k8xC	a
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
kompletní	kompletní	k2eAgFnSc1d1	kompletní
digitalizace	digitalizace	k1gFnSc1	digitalizace
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
poskytovatelé	poskytovatel	k1gMnPc1	poskytovatel
nabízí	nabízet	k5eAaImIp3nS	nabízet
televizní	televizní	k2eAgNnSc4d1	televizní
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
vysílání	vysílání	k1gNnSc4	vysílání
i	i	k9	i
přes	přes	k7c4	přes
Internet	Internet	k1gInSc4	Internet
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
protokolu	protokol	k1gInSc2	protokol
IPTV	IPTV	kA	IPTV
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
širokopásmového	širokopásmový	k2eAgNnSc2d1	širokopásmové
připojení	připojení	k1gNnSc2	připojení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
má	mít	k5eAaImIp3nS	mít
duální	duální	k2eAgNnSc1d1	duální
rozhlasové	rozhlasový	k2eAgNnSc1d1	rozhlasové
vysílání	vysílání	k1gNnSc1	vysílání
<g/>
,	,	kIx,	,
veřejnoprávní	veřejnoprávní	k2eAgInSc1d1	veřejnoprávní
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
soukromých	soukromý	k2eAgFnPc2d1	soukromá
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
financovány	financovat	k5eAaBmNgFnP	financovat
reklamami	reklama	k1gFnPc7	reklama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
existuje	existovat	k5eAaImIp3nS	existovat
veřejnoprávní	veřejnoprávní	k2eAgFnSc1d1	veřejnoprávní
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jiným	jiné	k1gNnSc7	jiné
stanice	stanice	k1gFnSc2	stanice
ARD	ARD	kA	ARD
a	a	k8xC	a
ZDF	ZDF	kA	ZDF
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
soukromé	soukromý	k2eAgFnPc1d1	soukromá
televizní	televizní	k2eAgFnPc1d1	televizní
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
ProsiebenSat	ProsiebenSat	k1gInSc1	ProsiebenSat
<g/>
.1	.1	k4	.1
media	medium	k1gNnSc2	medium
AG	AG	kA	AG
<g/>
,	,	kIx,	,
RTL	RTL	kA	RTL
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
MTV	MTV	kA	MTV
Networks	Networks	k1gInSc1	Networks
Deutschland	Deutschland	k1gInSc1	Deutschland
<g/>
,	,	kIx,	,
NBC	NBC	kA	NBC
Universal	Universal	k1gFnSc1	Universal
Deutschland	Deutschland	k1gInSc1	Deutschland
<g/>
,	,	kIx,	,
Tele	tele	k1gNnSc1	tele
München	Münchna	k1gFnPc2	Münchna
Gruppe	Grupp	k1gMnSc5	Grupp
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
regionální	regionální	k2eAgMnPc1d1	regionální
poskytovatelé	poskytovatel	k1gMnPc1	poskytovatel
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
volně	volně	k6eAd1	volně
přijímaného	přijímaný	k2eAgNnSc2d1	přijímané
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
také	také	k9	také
digitální	digitální	k2eAgFnSc1d1	digitální
placená	placený	k2eAgFnSc1d1	placená
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
poskytovatel	poskytovatel	k1gMnSc1	poskytovatel
placeného	placený	k2eAgNnSc2d1	placené
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
je	být	k5eAaImIp3nS	být
Premiere	Premier	k1gMnSc5	Premier
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prostředí	prostředí	k1gNnSc1	prostředí
německé	německý	k2eAgFnSc2d1	německá
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
mnohotvárné	mnohotvárný	k2eAgNnSc1d1	mnohotvárné
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejčtenější	čtený	k2eAgInPc4d3	nejčtenější
deníky	deník	k1gInPc4	deník
patří	patřit	k5eAaImIp3nS	patřit
Bild	Bild	k1gInSc1	Bild
<g/>
,	,	kIx,	,
Westdeutsche	Westdeutschus	k1gMnSc5	Westdeutschus
Allgemeine	Allgemein	k1gMnSc5	Allgemein
Zeitung	Zeitung	k1gInSc4	Zeitung
a	a	k8xC	a
Süddeutsche	Süddeutsche	k1gFnSc4	Süddeutsche
Zeitung	Zeitunga	k1gFnPc2	Zeitunga
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgInPc7d3	nejznámější
týdeníky	týdeník	k1gInPc7	týdeník
jsou	být	k5eAaImIp3nP	být
Der	drát	k5eAaImRp2nS	drát
Spiegel	Spiegel	k1gMnSc1	Spiegel
a	a	k8xC	a
Focus	Focus	k1gMnSc1	Focus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
disponovalo	disponovat	k5eAaBmAgNnS	disponovat
72	[number]	k4	72
%	%	kIx~	%
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
připojením	připojení	k1gNnSc7	připojení
na	na	k7c4	na
internet	internet	k1gInSc4	internet
<g/>
;	;	kIx,	;
asi	asi	k9	asi
24	[number]	k4	24
%	%	kIx~	%
mělo	mít	k5eAaImAgNnS	mít
širokopásmové	širokopásmový	k2eAgNnSc1d1	širokopásmové
připojení	připojení	k1gNnSc1	připojení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kinematografie	kinematografie	k1gFnSc1	kinematografie
===	===	k?	===
</s>
</p>
<p>
<s>
Klasikem	klasik	k1gMnSc7	klasik
německého	německý	k2eAgInSc2d1	německý
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
režisér	režisér	k1gMnSc1	režisér
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Murnau	Murnaa	k1gFnSc4	Murnaa
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
klasického	klasický	k2eAgInSc2d1	klasický
snímku	snímek	k1gInSc2	snímek
Upír	upír	k1gMnSc1	upír
Nosferatu	Nosferat	k1gInSc2	Nosferat
<g/>
.	.	kIx.	.
</s>
<s>
Slavnou	slavný	k2eAgFnSc7d1	slavná
filmovou	filmový	k2eAgFnSc7d1	filmová
režisérkou	režisérka	k1gFnSc7	režisérka
byla	být	k5eAaImAgFnS	být
i	i	k9	i
Leni	Leni	k?	Leni
Riefenstahlová	Riefenstahlová	k1gFnSc1	Riefenstahlová
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
postavou	postava	k1gFnSc7	postava
poválečného	poválečný	k2eAgInSc2d1	poválečný
německého	německý	k2eAgInSc2d1	německý
filmu	film	k1gInSc2	film
byli	být	k5eAaImAgMnP	být
Wim	Wim	k1gMnPc1	Wim
Wenders	Wendersa	k1gFnPc2	Wendersa
<g/>
,	,	kIx,	,
Werner	Werner	k1gMnSc1	Werner
Herzog	Herzog	k1gMnSc1	Herzog
<g/>
,	,	kIx,	,
Volker	Volker	k1gMnSc1	Volker
Schlöndorff	Schlöndorff	k1gMnSc1	Schlöndorff
či	či	k8xC	či
Rainer	Rainer	k1gMnSc1	Rainer
Werner	Werner	k1gMnSc1	Werner
Fassbinder	Fassbinder	k1gMnSc1	Fassbinder
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
Roland	Roland	k1gInSc1	Roland
Emmerich	Emmerich	k1gInSc4	Emmerich
<g/>
,	,	kIx,	,
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
uspěl	uspět	k5eAaPmAgMnS	uspět
i	i	k9	i
Tom	Tom	k1gMnSc1	Tom
Tykwer	Tykwer	k1gMnSc1	Tykwer
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějšími	slavný	k2eAgMnPc7d3	nejslavnější
německými	německý	k2eAgMnPc7d1	německý
herci	herec	k1gMnPc7	herec
jsou	být	k5eAaImIp3nP	být
Marlene	Marlen	k1gInSc5	Marlen
Dietrichová	Dietrichová	k1gFnSc5	Dietrichová
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
Kinski	Kinsk	k1gFnSc2	Kinsk
a	a	k8xC	a
Nastassja	Nastassjum	k1gNnSc2	Nastassjum
Kinski	Kinsk	k1gFnSc2	Kinsk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
modelingu	modeling	k1gInSc6	modeling
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
Heidi	Heid	k1gMnPc1	Heid
Klumová	Klumová	k1gFnSc1	Klumová
a	a	k8xC	a
Claudia	Claudia	k1gFnSc1	Claudia
Schifferová	Schifferová	k1gFnSc1	Schifferová
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
módní	módní	k2eAgMnSc1d1	módní
návrhář	návrhář	k1gMnSc1	návrhář
proslul	proslout	k5eAaPmAgMnS	proslout
Karl	Karl	k1gMnSc1	Karl
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
Berlinale	Berlinale	k1gFnSc2	Berlinale
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kuchyně	kuchyně	k1gFnSc2	kuchyně
===	===	k?	===
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
mnohotvárná	mnohotvárný	k2eAgFnSc1d1	mnohotvárná
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
region	region	k1gInSc1	region
od	od	k7c2	od
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
vydatné	vydatný	k2eAgInPc4d1	vydatný
a	a	k8xC	a
těžké	těžký	k2eAgInPc4d1	těžký
pokrmy	pokrm	k1gInPc4	pokrm
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
vepřové	vepřový	k2eAgNnSc1d1	vepřové
koleno	koleno	k1gNnSc1	koleno
a	a	k8xC	a
uzené	uzený	k2eAgNnSc1d1	uzené
vepřové	vepřový	k2eAgNnSc1d1	vepřové
maso	maso	k1gNnSc1	maso
s	s	k7c7	s
kyselým	kyselý	k2eAgNnSc7d1	kyselé
zelím	zelí	k1gNnSc7	zelí
<g/>
,	,	kIx,	,
kadeřavá	kadeřavý	k2eAgFnSc1d1	kadeřavá
kapusta	kapusta	k1gFnSc1	kapusta
s	s	k7c7	s
čajovkou	čajovka	k1gFnSc7	čajovka
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
různé	různý	k2eAgInPc4d1	různý
eintopfy	eintopf	k1gInPc4	eintopf
<g/>
.	.	kIx.	.
</s>
<s>
Eintopf	Eintopf	k1gMnSc1	Eintopf
je	být	k5eAaImIp3nS	být
jídlo	jídlo	k1gNnSc4	jídlo
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
hrnce	hrnec	k1gInSc2	hrnec
<g/>
,	,	kIx,	,
v	v	k7c6	v
hrnci	hrnec	k1gInSc6	hrnec
se	se	k3xPyFc4	se
vaří	vařit	k5eAaImIp3nS	vařit
<g/>
,	,	kIx,	,
dusí	dusit	k5eAaImIp3nS	dusit
a	a	k8xC	a
peče	péct	k5eAaImIp3nS	péct
úplně	úplně	k6eAd1	úplně
všechno	všechen	k3xTgNnSc4	všechen
od	od	k7c2	od
masa	maso	k1gNnSc2	maso
přes	přes	k7c4	přes
luštěniny	luštěnina	k1gFnPc4	luštěnina
až	až	k9	až
po	po	k7c4	po
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vydatná	vydatný	k2eAgNnPc1d1	vydatné
jídla	jídlo	k1gNnPc1	jídlo
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
tradici	tradice	k1gFnSc4	tradice
díky	díky	k7c3	díky
relativně	relativně	k6eAd1	relativně
severní	severní	k2eAgFnSc3d1	severní
poloze	poloha	k1gFnSc3	poloha
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
byl	být	k5eAaImAgInS	být
nutný	nutný	k2eAgInSc1d1	nutný
vydatný	vydatný	k2eAgInSc1d1	vydatný
příjem	příjem	k1gInSc1	příjem
kalorií	kalorie	k1gFnPc2	kalorie
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
různorodá	různorodý	k2eAgFnSc1d1	různorodá
a	a	k8xC	a
ovlivňovaná	ovlivňovaný	k2eAgFnSc1d1	ovlivňovaná
kuchyněmi	kuchyně	k1gFnPc7	kuchyně
ze	z	k7c2	z
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
jako	jako	k8xS	jako
i	i	k9	i
jiných	jiný	k2eAgFnPc6d1	jiná
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
přechází	přecházet	k5eAaImIp3nS	přecházet
od	od	k7c2	od
těžkých	těžký	k2eAgInPc2d1	těžký
k	k	k7c3	k
lehčím	lehký	k2eAgInPc3d2	lehčí
pokrmům	pokrm	k1gInPc3	pokrm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Především	především	k9	především
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
různorodé	různorodý	k2eAgInPc1d1	různorodý
pokrmy	pokrm	k1gInPc1	pokrm
z	z	k7c2	z
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
knedlík	knedlík	k1gInSc1	knedlík
a	a	k8xC	a
nudle	nudle	k1gFnSc1	nudle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
k	k	k7c3	k
snídani	snídaně	k1gFnSc3	snídaně
jí	jíst	k5eAaImIp3nS	jíst
chléb	chléb	k1gInSc1	chléb
a	a	k8xC	a
pečivo	pečivo	k1gNnSc4	pečivo
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
pomazánkami	pomazánka	k1gFnPc7	pomazánka
<g/>
,	,	kIx,	,
s	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
výběrem	výběr	k1gInSc7	výběr
uzenin	uzenina	k1gFnPc2	uzenina
a	a	k8xC	a
sýrů	sýr	k1gInPc2	sýr
<g/>
,	,	kIx,	,
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vařené	vařený	k2eAgNnSc1d1	vařené
vejce	vejce	k1gNnSc1	vejce
<g/>
,	,	kIx,	,
i	i	k8xC	i
vejce	vejce	k1gNnPc4	vejce
na	na	k7c4	na
hniličku	hnilička	k1gFnSc4	hnilička
<g/>
,	,	kIx,	,
pije	pít	k5eAaImIp3nS	pít
se	se	k3xPyFc4	se
čaj	čaj	k1gInSc1	čaj
či	či	k8xC	či
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přednost	přednost	k1gFnSc4	přednost
dávají	dávat	k5eAaImIp3nP	dávat
Němci	Němec	k1gMnPc1	Němec
kávě	káva	k1gFnSc3	káva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
se	se	k3xPyFc4	se
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
teplé	teplý	k2eAgNnSc1d1	teplé
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
,	,	kIx,	,
večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
často	často	k6eAd1	často
podává	podávat	k5eAaImIp3nS	podávat
studená	studený	k2eAgFnSc1d1	studená
večeře	večeře	k1gFnSc1	večeře
formou	forma	k1gFnSc7	forma
studené	studený	k2eAgFnSc2d1	studená
mísy	mísa	k1gFnSc2	mísa
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Kalteplatte	Kalteplatt	k1gInSc5	Kalteplatt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
sladkého	sladký	k2eAgNnSc2d1	sladké
pečiva	pečivo	k1gNnSc2	pečivo
a	a	k8xC	a
koláčů	koláč	k1gInPc2	koláč
s	s	k7c7	s
ovocnou	ovocný	k2eAgFnSc7d1	ovocná
nebo	nebo	k8xC	nebo
tvarohovou	tvarohový	k2eAgFnSc7d1	tvarohová
náplní	náplň	k1gFnSc7	náplň
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
oplatek	oplatka	k1gFnPc2	oplatka
<g/>
.	.	kIx.	.
</s>
<s>
Odpolední	odpolední	k2eAgFnSc4d1	odpolední
svačinu	svačina	k1gFnSc4	svačina
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
Kaffee	Kaffee	k1gNnSc3	Kaffee
und	und	k?	und
Kuchen	Kuchen	k1gInSc1	Kuchen
(	(	kIx(	(
<g/>
káva	káva	k1gFnSc1	káva
a	a	k8xC	a
dort	dort	k1gInSc1	dort
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Sport	sport	k1gInSc1	sport
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
vysokou	vysoký	k2eAgFnSc4d1	vysoká
společenskou	společenský	k2eAgFnSc4d1	společenská
prestiž	prestiž	k1gFnSc4	prestiž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ročníku	ročník	k1gInSc6	ročník
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
bylo	být	k5eAaImAgNnS	být
přibližně	přibližně	k6eAd1	přibližně
27,6	[number]	k4	27,6
miliónu	milión	k4xCgInSc2	milión
německých	německý	k2eAgMnPc2d1	německý
obyvatel	obyvatel	k1gMnPc2	obyvatel
členy	člen	k1gInPc4	člen
91	[number]	k4	91
000	[number]	k4	000
sportovních	sportovní	k2eAgInPc2d1	sportovní
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počtu	počet	k1gInSc6	počet
medailí	medaile	k1gFnPc2	medaile
získaných	získaný	k2eAgFnPc2d1	získaná
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
sečteme	sečíst	k5eAaPmIp1nP	sečíst
olympijské	olympijský	k2eAgInPc4d1	olympijský
výsledky	výsledek	k1gInPc4	výsledek
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
východního	východní	k2eAgNnSc2d1	východní
Německa	Německo	k1gNnSc2	Německo
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
společného	společný	k2eAgInSc2d1	společný
německého	německý	k2eAgInSc2d1	německý
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
olympiád	olympiáda	k1gFnPc2	olympiáda
zúčastňoval	zúčastňovat	k5eAaImAgInS	zúčastňovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1956	[number]	k4	1956
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Německo	Německo	k1gNnSc1	Německo
třetí	třetí	k4xOgFnSc7	třetí
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
pořadatelem	pořadatel	k1gMnSc7	pořadatel
největších	veliký	k2eAgInPc2d3	veliký
sportovních	sportovní	k2eAgInPc2d1	sportovní
svátků	svátek	k1gInPc2	svátek
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgInPc2	dva
letních	letní	k2eAgInPc2d1	letní
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
1972	[number]	k4	1972
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedněch	jeden	k4xCgFnPc2	jeden
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
v	v	k7c6	v
Garmisch-Partenkirchenu	Garmisch-Partenkirchen	k1gInSc6	Garmisch-Partenkirchen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvou	dva	k4xCgNnPc2	dva
mistrovství	mistrovství	k1gNnPc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
významných	významný	k2eAgFnPc2d1	významná
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
hostí	hostit	k5eAaImIp3nS	hostit
řadu	řada	k1gFnSc4	řada
motoristických	motoristický	k2eAgInPc2d1	motoristický
závodů	závod	k1gInPc2	závod
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
úrovně	úroveň	k1gFnSc2	úroveň
včetně	včetně	k7c2	včetně
tradiční	tradiční	k2eAgFnSc2d1	tradiční
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
dosud	dosud	k6eAd1	dosud
uskutečněných	uskutečněný	k2eAgInPc2d1	uskutečněný
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Evropy	Evropa	k1gFnSc2	Evropa
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
německým	německý	k2eAgMnSc7d1	německý
sportovcem	sportovec	k1gMnSc7	sportovec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
ankety	anketa	k1gFnSc2	anketa
ZDF	ZDF	kA	ZDF
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
překonal	překonat	k5eAaPmAgInS	překonat
mnoho	mnoho	k4c4	mnoho
rekordů	rekord	k1gInPc2	rekord
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
včetně	včetně	k7c2	včetně
zisku	zisk	k1gInSc2	zisk
sedmi	sedm	k4xCc2	sedm
titulů	titul	k1gInPc2	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
ankety	anketa	k1gFnSc2	anketa
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
umístila	umístit	k5eAaPmAgFnS	umístit
kanoistka	kanoistka	k1gFnSc1	kanoistka
Birgit	Birgit	k1gInSc1	Birgit
Fischerová	Fischerová	k1gFnSc1	Fischerová
<g/>
,	,	kIx,	,
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
německá	německý	k2eAgFnSc1d1	německá
olympionička	olympionička	k1gFnSc1	olympionička
<g/>
,	,	kIx,	,
tenisté	tenista	k1gMnPc1	tenista
Steffi	Steffi	k1gFnSc1	Steffi
Grafová	Grafová	k1gFnSc1	Grafová
a	a	k8xC	a
Boris	Boris	k1gMnSc1	Boris
Becker	Becker	k1gMnSc1	Becker
<g/>
,	,	kIx,	,
cyklista	cyklista	k1gMnSc1	cyklista
Jan	Jan	k1gMnSc1	Jan
Ullrich	Ullrich	k1gMnSc1	Ullrich
<g/>
,	,	kIx,	,
plavkyně	plavkyně	k1gFnSc1	plavkyně
Franziska	Franziska	k1gFnSc1	Franziska
van	van	k1gInSc1	van
Almsicková	Almsicková	k1gFnSc1	Almsicková
<g/>
,	,	kIx,	,
legendární	legendární	k2eAgMnSc1d1	legendární
boxer	boxer	k1gMnSc1	boxer
Max	max	kA	max
Schmeling	Schmeling	k1gInSc1	Schmeling
a	a	k8xC	a
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
fotbalistů	fotbalista	k1gMnPc2	fotbalista
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Franzem	Franz	k1gMnSc7	Franz
Beckenbauerem	Beckenbauer	k1gMnSc7	Beckenbauer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
i	i	k9	i
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
sportovních	sportovní	k2eAgFnPc2d1	sportovní
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
krom	krom	k7c2	krom
legendárního	legendární	k2eAgNnSc2d1	legendární
Schumachera	Schumachero	k1gNnSc2	Schumachero
i	i	k9	i
Sebastian	Sebastian	k1gMnSc1	Sebastian
Vettel	Vettel	k1gMnSc1	Vettel
a	a	k8xC	a
Nico	Nico	k1gMnSc1	Nico
Rosberg	Rosberg	k1gMnSc1	Rosberg
<g/>
.	.	kIx.	.
</s>
<s>
Anton	anton	k1gInSc1	anton
Mang	mango	k1gNnPc2	mango
je	být	k5eAaImIp3nS	být
pětinásobným	pětinásobný	k2eAgMnSc7d1	pětinásobný
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
silničních	silniční	k2eAgInPc2d1	silniční
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Walter	Walter	k1gMnSc1	Walter
Röhrl	Röhrl	k1gMnSc1	Röhrl
je	být	k5eAaImIp3nS	být
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
rallye	rallye	k1gNnSc6	rallye
<g/>
.	.	kIx.	.
</s>
<s>
Tenisovou	tenisový	k2eAgFnSc7d1	tenisová
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
a	a	k8xC	a
vítězkou	vítězka	k1gFnSc7	vítězka
tří	tři	k4xCgInPc2	tři
grandslamů	grandslam	k1gInPc2	grandslam
byla	být	k5eAaImAgFnS	být
vedle	vedle	k7c2	vedle
Graffové	Graffový	k2eAgFnSc2d1	Graffová
i	i	k8xC	i
Angelique	Angelique	k1gFnSc1	Angelique
Kerberová	Kerberová	k1gFnSc1	Kerberová
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Stich	Stich	k1gMnSc1	Stich
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
jezdectví	jezdectví	k1gNnSc2	jezdectví
<g/>
,	,	kIx,	,
Isabell	Isabell	k1gInSc1	Isabell
Werthová	Werthová	k1gFnSc1	Werthová
si	se	k3xPyFc3	se
v	v	k7c6	v
koňském	koňský	k2eAgNnSc6d1	koňské
sedle	sedlo	k1gNnSc6	sedlo
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
šest	šest	k4xCc4	šest
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
její	její	k3xOp3gMnSc1	její
kolega	kolega	k1gMnSc1	kolega
Reiner	Reinra	k1gFnPc2	Reinra
Klimke	Klimke	k1gFnPc2	Klimke
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úspěšným	úspěšný	k2eAgMnPc3d1	úspěšný
olympijským	olympijský	k2eAgMnPc3d1	olympijský
plavcům	plavec	k1gMnPc3	plavec
patřili	patřit	k5eAaImAgMnP	patřit
i	i	k9	i
reprezentanti	reprezentant	k1gMnPc1	reprezentant
NDR	NDR	kA	NDR
Kornelia	Kornelia	k1gFnSc1	Kornelia
Enderová	Enderová	k1gFnSc1	Enderová
<g/>
,	,	kIx,	,
Roland	Roland	k1gInSc1	Roland
Matthes	Matthesa	k1gFnPc2	Matthesa
a	a	k8xC	a
Kristin	Kristina	k1gFnPc2	Kristina
Ottová	Ottová	k1gFnSc1	Ottová
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
atletkou	atletka	k1gFnSc7	atletka
je	být	k5eAaImIp3nS	být
běžkyně	běžkyně	k1gFnSc1	běžkyně
Bärbel	Bärbela	k1gFnPc2	Bärbela
Wöckelová	Wöckelová	k1gFnSc1	Wöckelová
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc4	čtyři
olympijské	olympijský	k2eAgInPc4d1	olympijský
zlaté	zlatý	k1gInPc4	zlatý
má	mít	k5eAaImIp3nS	mít
kanoistka	kanoistka	k1gFnSc1	kanoistka
Katrin	Katrin	k1gInSc4	Katrin
Wagner-Augustinová	Wagner-Augustinový	k2eAgFnSc1d1	Wagner-Augustinový
a	a	k8xC	a
veslařka	veslařka	k1gFnSc1	veslařka
Kathrin	Kathrin	k1gInSc1	Kathrin
Boronová	Boronová	k1gFnSc1	Boronová
<g/>
.	.	kIx.	.
</s>
<s>
Slavným	slavný	k2eAgMnSc7d1	slavný
šachistou	šachista	k1gMnSc7	šachista
byl	být	k5eAaImAgMnS	být
Emanuel	Emanuel	k1gMnSc1	Emanuel
Lasker	Lasker	k1gMnSc1	Lasker
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
světovou	světový	k2eAgFnSc7d1	světová
velmocí	velmoc	k1gFnSc7	velmoc
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
<g/>
,	,	kIx,	,
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c6	na
bobech	bob	k1gInPc6	bob
<g/>
,	,	kIx,	,
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
nebo	nebo	k8xC	nebo
rychlobruslení	rychlobruslení	k1gNnSc2	rychlobruslení
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
německou	německý	k2eAgFnSc7d1	německá
účastnicí	účastnice	k1gFnSc7	účastnice
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
rychlobruslařka	rychlobruslařka	k1gFnSc1	rychlobruslařka
Claudia	Claudia	k1gFnSc1	Claudia
Pechsteinová	Pechsteinová	k1gFnSc1	Pechsteinová
s	s	k7c7	s
pěti	pět	k4xCc7	pět
zlatými	zlatý	k2eAgFnPc7d1	zlatá
medailemi	medaile	k1gFnPc7	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Rychlobruslařskými	rychlobruslařský	k2eAgFnPc7d1	rychlobruslařská
legendami	legenda	k1gFnPc7	legenda
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
Gunda	Gunda	k1gFnSc1	Gunda
Niemannová-Stirnemannová	Niemannová-Stirnemannová	k1gFnSc1	Niemannová-Stirnemannová
či	či	k8xC	či
Karin	Karina	k1gFnPc2	Karina
Enkeová	Enkeový	k2eAgFnSc1d1	Enkeový
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc4	čtyři
zlaté	zlatý	k1gInPc4	zlatý
mají	mít	k5eAaImIp3nP	mít
bobisté	bobista	k1gMnPc1	bobista
Kevin	Kevina	k1gFnPc2	Kevina
Kuske	Kusk	k1gFnSc2	Kusk
a	a	k8xC	a
André	André	k1gMnSc2	André
Lange	Lang	k1gMnSc2	Lang
i	i	k9	i
sáňkaři	sáňkař	k1gMnPc1	sáňkař
Natalie	Natalie	k1gFnSc1	Natalie
Geisenbergerová	Geisenbergerová	k1gFnSc1	Geisenbergerová
<g/>
,	,	kIx,	,
Tobias	Tobias	k1gMnSc1	Tobias
Arlt	Arlt	k1gMnSc1	Arlt
a	a	k8xC	a
Tobias	Tobias	k1gMnSc1	Tobias
Wendl	Wendl	k1gMnSc1	Wendl
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnými	úspěšný	k2eAgMnPc7d1	úspěšný
biatlonisty	biatlonista	k1gMnPc7	biatlonista
byli	být	k5eAaImAgMnP	být
Sven	Sven	k1gMnSc1	Sven
Fischer	Fischer	k1gMnSc1	Fischer
či	či	k8xC	či
Laura	Laura	k1gFnSc1	Laura
Dahlmeierová	Dahlmeierová	k1gFnSc1	Dahlmeierová
<g/>
.	.	kIx.	.
</s>
<s>
Jens	Jens	k6eAd1	Jens
Weissflog	Weissflog	k1gInSc1	Weissflog
je	být	k5eAaImIp3nS	být
legendou	legenda	k1gFnSc7	legenda
skoků	skok	k1gInPc2	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
,	,	kIx,	,
Markus	Markus	k1gInSc1	Markus
Wasmeier	Wasmeira	k1gFnPc2	Wasmeira
sjezdového	sjezdový	k2eAgNnSc2d1	sjezdové
lyžování	lyžování	k1gNnSc2	lyžování
<g/>
,	,	kIx,	,
Katarina	Katarin	k2eAgNnPc4d1	Katarin
Wittová	Wittový	k2eAgNnPc4d1	Wittový
krasobruslení	krasobruslení	k1gNnPc4	krasobruslení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejoblíbenějším	oblíbený	k2eAgInSc7d3	nejoblíbenější
druhem	druh	k1gInSc7	druh
sportu	sport	k1gInSc2	sport
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
kopaná	kopaná	k1gFnSc1	kopaná
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
svaz	svaz	k1gInSc1	svaz
má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
miliónů	milión	k4xCgInPc2	milión
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
v	v	k7c6	v
soutěžích	soutěž	k1gFnPc6	soutěž
hraje	hrát	k5eAaImIp3nS	hrát
170	[number]	k4	170
tisíc	tisíc	k4xCgInPc2	tisíc
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
mužská	mužský	k2eAgFnSc1d1	mužská
reprezentace	reprezentace	k1gFnSc1	reprezentace
je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgInPc2d3	nejúspěšnější
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
titul	titul	k1gInSc4	titul
mistrů	mistr	k1gMnPc2	mistr
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
a	a	k8xC	a
třikrát	třikrát	k6eAd1	třikrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
kopané	kopaná	k1gFnSc6	kopaná
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
a	a	k8xC	a
ženské	ženský	k2eAgFnSc6d1	ženská
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
tří	tři	k4xCgFnPc2	tři
německých	německý	k2eAgFnPc2d1	německá
klubů	klub	k1gInPc2	klub
se	se	k3xPyFc4	se
radovali	radovat	k5eAaImAgMnP	radovat
z	z	k7c2	z
vítězství	vítězství	k1gNnSc2	vítězství
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
či	či	k8xC	či
jejím	její	k3xOp3gMnPc3	její
předchůdci	předchůdce	k1gMnPc1	předchůdce
Poháru	pohár	k1gInSc2	pohár
mistrů	mistr	k1gMnPc2	mistr
(	(	kIx(	(
<g/>
Bayern	Bayern	k1gInSc1	Bayern
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Hamburger	hamburger	k1gInSc1	hamburger
SV	sv	kA	sv
<g/>
,	,	kIx,	,
Borussia	Borussia	k1gFnSc1	Borussia
Dortmund	Dortmund	k1gInSc1	Dortmund
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bayern	Bayern	k1gInSc1	Bayern
Mnichov	Mnichov	k1gInSc1	Mnichov
celkem	celkem	k6eAd1	celkem
pětkrát	pětkrát	k6eAd1	pětkrát
<g/>
.	.	kIx.	.
</s>
<s>
Pohár	pohár	k1gInSc1	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
i	i	k9	i
Werder	Werder	k1gInSc1	Werder
Brémy	Brémy	k1gFnPc1	Brémy
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Magdeburg	Magdeburg	k1gInSc1	Magdeburg
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
východoněmecký	východoněmecký	k2eAgInSc1d1	východoněmecký
tým	tým	k1gInSc1	tým
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropskou	evropský	k2eAgFnSc4d1	Evropská
ligu	liga	k1gFnSc4	liga
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Pohár	pohár	k1gInSc1	pohár
UEFA	UEFA	kA	UEFA
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
Borussia	Borussia	k1gFnSc1	Borussia
Mönchengladbach	Mönchengladbach	k1gInSc1	Mönchengladbach
<g/>
,	,	kIx,	,
Eintracht	Eintracht	k2eAgInSc1d1	Eintracht
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
,	,	kIx,	,
Bayer	Bayer	k1gMnSc1	Bayer
Leverkusen	Leverkusen	k1gInSc1	Leverkusen
a	a	k8xC	a
FC	FC	kA	FC
Schalke	Schalke	k1gFnSc1	Schalke
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
fotbalistu	fotbalista	k1gMnSc4	fotbalista
Evropy	Evropa	k1gFnSc2	Evropa
získali	získat	k5eAaPmAgMnP	získat
Gerd	Gerd	k1gMnSc1	Gerd
Müller	Müller	k1gMnSc1	Müller
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Beckenbauer	Beckenbauer	k1gMnSc1	Beckenbauer
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karl-Heinz	Karl-Heinz	k1gMnSc1	Karl-Heinz
Rummenigge	Rummenigg	k1gFnSc2	Rummenigg
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lothar	Lothar	k1gMnSc1	Lothar
Matthäus	Matthäus	k1gMnSc1	Matthäus
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
a	a	k8xC	a
Matthias	Matthias	k1gMnSc1	Matthias
Sammer	Sammer	k1gMnSc1	Sammer
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oliver	Oliver	k1gMnSc1	Oliver
Kahn	Kahn	k1gMnSc1	Kahn
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
světového	světový	k2eAgInSc2d1	světový
šampionátu	šampionát	k1gInSc2	šampionát
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Klose	Klose	k1gFnSc2	Klose
vede	vést	k5eAaImIp3nS	vést
historickou	historický	k2eAgFnSc4d1	historická
tabulku	tabulka	k1gFnSc4	tabulka
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
střelců	střelec	k1gMnPc2	střelec
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
vyniká	vynikat	k5eAaImIp3nS	vynikat
také	také	k9	také
německá	německý	k2eAgFnSc1d1	německá
házená	házená	k1gFnSc1	házená
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
krát	krát	k6eAd1	krát
mistři	mistr	k1gMnPc1	mistr
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
a	a	k8xC	a
pozemní	pozemní	k2eAgInSc1d1	pozemní
hokej	hokej	k1gInSc1	hokej
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgFnPc1	čtyři
zlaté	zlatá	k1gFnPc1	zlatá
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narůstá	narůstat	k5eAaImIp3nS	narůstat
i	i	k9	i
popularita	popularita	k1gFnSc1	popularita
basketbalu	basketbal	k1gInSc2	basketbal
a	a	k8xC	a
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
německým	německý	k2eAgMnSc7d1	německý
basketbalistou	basketbalista	k1gMnSc7	basketbalista
je	být	k5eAaImIp3nS	být
Dirk	Dirk	k1gMnSc1	Dirk
Nowitzki	Nowitzk	k1gFnSc2	Nowitzk
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
světového	světový	k2eAgMnSc4d1	světový
házenkáře	házenkář	k1gMnSc4	házenkář
roku	rok	k1gInSc2	rok
mají	mít	k5eAaImIp3nP	mít
Daniel	Daniel	k1gMnSc1	Daniel
Stephan	Stephan	k1gMnSc1	Stephan
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Henning	Henning	k1gInSc1	Henning
Fritz	Fritz	k1gInSc1	Fritz
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nadine	Nadin	k1gInSc5	Nadin
Krauseová	Krauseová	k1gFnSc1	Krauseová
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
volejbalové	volejbalový	k2eAgFnSc2d1	volejbalová
federace	federace	k1gFnSc2	federace
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgMnS	uvést
Siegfried	Siegfried	k1gMnSc1	Siegfried
Schneider	Schneider	k1gMnSc1	Schneider
<g/>
.	.	kIx.	.
<g/>
Řada	řada	k1gFnSc1	řada
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
sportovců	sportovec	k1gMnPc2	sportovec
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
bývalou	bývalý	k2eAgFnSc7d1	bývalá
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k8xC	ale
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
vyšla	vyjít	k5eAaPmAgFnS	vyjít
najevo	najevo	k6eAd1	najevo
existence	existence	k1gFnSc1	existence
systému	systém	k1gInSc2	systém
státem	stát	k1gInSc7	stát
řízeného	řízený	k2eAgInSc2d1	řízený
dopingu	doping	k1gInSc2	doping
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svátky	svátek	k1gInPc4	svátek
===	===	k?	===
</s>
</p>
<p>
<s>
Státním	státní	k2eAgInSc7d1	státní
svátek	svátek	k1gInSc1	svátek
Německa	Německo	k1gNnSc2	Německo
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
<g/>
,	,	kIx,	,
Den	den	k1gInSc1	den
německé	německý	k2eAgFnSc2d1	německá
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
spolkovou	spolkový	k2eAgFnSc7d1	spolková
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
svátky	svátek	k1gInPc1	svátek
určují	určovat	k5eAaImIp3nP	určovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
spolkové	spolkový	k2eAgFnPc4d1	spolková
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jsou	být	k5eAaImIp3nP	být
svátky	svátek	k1gInPc4	svátek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
spolkových	spolkový	k2eAgFnPc6d1	spolková
zemích	zem	k1gFnPc6	zem
<g/>
:	:	kIx,	:
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
<g/>
,	,	kIx,	,
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
<g/>
,	,	kIx,	,
Nanebevstoupení	nanebevstoupení	k1gNnSc4	nanebevstoupení
páně	páně	k2eAgNnSc4d1	páně
<g/>
,	,	kIx,	,
Svatodušní	svatodušní	k2eAgNnSc4d1	svatodušní
pondělí	pondělí	k1gNnSc4	pondělí
(	(	kIx(	(
<g/>
letnice	letnice	k1gFnPc4	letnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
oba	dva	k4xCgInPc4	dva
Vánoční	vánoční	k2eAgInPc4d1	vánoční
svátky	svátek	k1gInPc4	svátek
<g/>
;	;	kIx,	;
bez	bez	k7c2	bez
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
pozadí	pozadí	k1gNnSc2	pozadí
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
a	a	k8xC	a
Svátek	svátek	k1gInSc4	svátek
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nedělí	neděle	k1gFnSc7	neděle
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
svátky	svátek	k1gInPc4	svátek
dny	den	k1gInPc7	den
pracovní	pracovní	k2eAgInSc4d1	pracovní
klidu	klid	k1gInSc3	klid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
památky	památka	k1gFnPc1	památka
(	(	kIx(	(
<g/>
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Církevní	církevní	k2eAgFnPc1d1	církevní
památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
#	#	kIx~	#
<g/>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
kostelní	kostelní	k2eAgFnSc1d1	kostelní
věž	věž	k1gFnSc1	věž
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
161,53	[number]	k4	161,53
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
stavby	stavba	k1gFnPc1	stavba
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Technika	technika	k1gFnSc1	technika
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Krajina	Krajina	k1gFnSc1	Krajina
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Germany	German	k1gInPc4	German
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FULBROOK	FULBROOK	kA	FULBROOK
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
moderního	moderní	k2eAgNnSc2d1	moderní
Německa	Německo	k1gNnSc2	Německo
:	:	kIx,	:
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
304	[number]	k4	304
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
3104	[number]	k4	3104
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MÜLLER	MÜLLER	kA	MÜLLER
<g/>
,	,	kIx,	,
Helmut	Helmut	k1gMnSc1	Helmut
<g/>
;	;	kIx,	;
KRIEGER	KRIEGER	kA	KRIEGER
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Friedrich	Friedrich	k1gMnSc1	Friedrich
<g/>
;	;	kIx,	;
VOLLRATH	VOLLRATH	kA	VOLLRATH
<g/>
,	,	kIx,	,
Hanna	Hanna	k1gFnSc1	Hanna
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
dopl	dopla	k1gFnPc2	dopla
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
609	[number]	k4	609
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
712	[number]	k4	712
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Německo	Německo	k1gNnSc4	Německo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Německo	Německo	k1gNnSc4	Německo
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Německo	Německo	k1gNnSc4	Německo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Německo	Německo	k1gNnSc4	Německo
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
Německo	Německo	k1gNnSc4	Německo
ve	v	k7c4	v
WikicestáchExterní	WikicestáchExterní	k2eAgInPc4d1	WikicestáchExterní
odkazy	odkaz	k1gInPc4	odkaz
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
instituce	instituce	k1gFnPc4	instituce
státuNěmecko	státuNěmecko	k6eAd1	státuNěmecko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Německo	Německo	k1gNnSc1	Německo
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
Spolkový	spolkový	k2eAgMnSc1d1	spolkový
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
Bundespräsident	Bundespräsident	k1gMnSc1	Bundespräsident
<g/>
)	)	kIx)	)
Frank-Walter	Frank-Walter	k1gMnSc1	Frank-Walter
Steinmeier	Steinmeier	k1gMnSc1	Steinmeier
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
Spolkový	spolkový	k2eAgInSc1d1	spolkový
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
Deutscher	Deutschra	k1gFnPc2	Deutschra
Bundestag	Bundestag	k1gInSc1	Bundestag
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
Spolková	spolkový	k2eAgFnSc1d1	spolková
vláda	vláda	k1gFnSc1	vláda
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Bundesregierung	Bundesregierung	k1gMnSc1	Bundesregierung
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
Spolková	spolkový	k2eAgFnSc1d1	spolková
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
Bundesrat	Bundesrat	k1gInSc1	Bundesrat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Spolkový	spolkový	k2eAgInSc1d1	spolkový
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
(	(	kIx(	(
<g/>
Statistiches	Statistiches	k1gInSc1	Statistiches
Bundesamt	Bundesamta	k1gFnPc2	Bundesamta
Deutschland	Deutschlanda	k1gFnPc2	Deutschlanda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Německá	německý	k2eAgFnSc1d1	německá
turistická	turistický	k2eAgFnSc1d1	turistická
centrála	centrála	k1gFnSc1	centrála
(	(	kIx(	(
<g/>
Deutsche	Deutschus	k1gMnSc5	Deutschus
Zentrale	Zentral	k1gMnSc5	Zentral
für	für	k?	für
Tourismus	Tourismus	k1gInSc1	Tourismus
<g/>
)	)	kIx)	)
<g/>
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
informace	informace	k1gFnSc2	informace
<g/>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
<g/>
www.deutschland.de	www.deutschland.de	k6eAd1	www.deutschland.de
–	–	k?	–
oficiální	oficiální	k2eAgInSc1d1	oficiální
německý	německý	k2eAgInSc1d1	německý
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
<g/>
Fakta	faktum	k1gNnPc1	faktum
o	o	k7c6	o
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
Tatsachen	Tatsachen	k2eAgInSc1d1	Tatsachen
űber	űber	k1gInSc1	űber
Deutschland	Deutschlanda	k1gFnPc2	Deutschlanda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
Spolkové	spolkový	k2eAgNnSc1d1	Spolkové
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničíExterní	zahraničíExterní	k2eAgInPc1d1	zahraničíExterní
odkazy	odkaz	k1gInPc1	odkaz
–	–	k?	–
další	další	k2eAgInPc1d1	další
informaceGermany	informaceGerman	k1gInPc1	informaceGerman
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
web	web	k1gInSc4	web
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Answers	Answersa	k1gFnPc2	Answersa
–	–	k?	–
Germany	German	k1gInPc1	German
</s>
</p>
<p>
<s>
Export	export	k1gInSc1	export
do	do	k7c2	do
SRN	srna	k1gFnPc2	srna
–	–	k?	–
aktuální	aktuální	k2eAgFnSc2d1	aktuální
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
Německu	Německo	k1gNnSc6	Německo
</s>
</p>
<p>
<s>
Germany	German	k1gInPc1	German
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Germany	German	k1gInPc1	German
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
-	-	kIx~	-
o	o	k7c6	o
českých	český	k2eAgMnPc6d1	český
krajanech	krajan	k1gMnPc6	krajan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2004-06-08	[number]	k4	2004-06-08
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Germany	German	k1gInPc4	German
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-07-01	[number]	k4	2011-07-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Germany	German	k1gInPc1	German
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Germany	German	k1gInPc7	German
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2008-04-21	[number]	k4	2008-04-21
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Německo	Německo	k1gNnSc1	Německo
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-05-18	[number]	k4	2011-05-18
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Germany	German	k1gInPc1	German
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
–	–	k?	–
internetová	internetový	k2eAgFnSc1d1	internetová
médiaPřehled	médiaPřehled	k1gInSc4	médiaPřehled
tisku	tisk	k1gInSc2	tisk
–	–	k?	–
tisk	tisk	k1gInSc1	tisk
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
mapa	mapa	k1gFnSc1	mapa
Německa	Německo	k1gNnSc2	Německo
–	–	k?	–
interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
mapa	mapa	k1gFnSc1	mapa
až	až	k9	až
do	do	k7c2	do
měřítka	měřítko	k1gNnSc2	měřítko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
000	[number]	k4	000
</s>
</p>
