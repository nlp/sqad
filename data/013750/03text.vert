<s>
Špičák	špičák	k1gInSc1
(	(	kIx(
<g/>
Oldřichovská	Oldřichovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Špičák	špičák	k1gInSc4
Vrchol	vrchol	k1gInSc1
Špičáku	špičák	k1gInSc2
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
724	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
246	#num#	k4
m	m	kA
↓	↓	k?
Oldřichovské	Oldřichovský	k2eAgNnSc4d1
sedlo	sedlo	k1gNnSc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Izolace	izolace	k1gFnPc1
</s>
<s>
3,5	3,5	k4
km	km	kA
→	→	k?
Poledník	poledník	k1gInSc1
Seznamy	seznam	k1gInPc1
</s>
<s>
Nejprominentnější	prominentní	k2eAgFnPc1d3
hory	hora	k1gFnPc1
CZ	CZ	kA
#	#	kIx~
<g/>
64	#num#	k4
<g/>
Hory	hora	k1gFnSc2
a	a	k8xC
kopce	kopec	k1gInSc2
Jizerských	jizerský	k2eAgFnPc2d1
hor	hora	k1gFnPc2
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Jizerské	jizerský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
/	/	kIx~
Jizerská	jizerský	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
/	/	kIx~
Oldřichovská	Oldřichovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
/	/	kIx~
Špičácká	Špičácký	k2eAgFnSc1d1
část	část	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
52	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
28	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Oldřichovský	Oldřichovský	k2eAgInSc1d1
Špičák	špičák	k1gInSc1
</s>
<s>
Povodí	povodí	k1gNnSc1
</s>
<s>
Lužická	lužický	k2eAgFnSc1d1
Nisa	Nisa	k1gFnSc1
→	→	k?
Odra	Odra	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Špičák	špičák	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
často	často	k6eAd1
Oldřichovský	Oldřichovský	k2eAgInSc1d1
Špičák	špičák	k1gInSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
Buschullersdorfer	Buschullersdorfer	k2eAgInSc1d1
Spitzberg	Spitzberg	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
724	#num#	k4
m	m	kA
n.	n.	kA
m.	m.	kA
vysoký	vysoký	k2eAgInSc1d1
vrchol	vrchol	k1gInSc1
s	s	k7c7
vyhlídkou	vyhlídka	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1
dominuje	dominovat	k5eAaImIp3nS
západní	západní	k2eAgFnSc3d1
části	část	k1gFnSc3
Jizerských	jizerský	k2eAgFnPc2d1
hor	hora	k1gFnPc2
nad	nad	k7c7
Oldřichovem	Oldřichovo	k1gNnSc7
v	v	k7c6
Hájích	háj	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Vrchol	vrchol	k1gInSc1
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
samostatnou	samostatný	k2eAgFnSc7d1
národní	národní	k2eAgFnSc7d1
přírodní	přírodní	k2eAgFnSc7d1
rezervací	rezervace	k1gFnSc7
Špičák	Špičák	k1gMnSc1
o	o	k7c6
rozloze	rozloha	k1gFnSc6
29,21	29,21	k4
ha	ha	kA
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
jádrovou	jádrový	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
NPR	NPR	kA
Jizerskohorské	jizerskohorský	k2eAgFnSc2d1
bučiny	bučina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdejší	zdejší	k2eAgInSc1d1
skalnatý	skalnatý	k2eAgInSc1d1
terén	terén	k1gInSc1
je	být	k5eAaImIp3nS
porostlý	porostlý	k2eAgMnSc1d1
smíšeným	smíšený	k2eAgInSc7d1
lesem	les	k1gInSc7
<g/>
,	,	kIx,
prochází	procházet	k5eAaImIp3nS
tudy	tudy	k6eAd1
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Oldřichovské	Oldřichovský	k2eAgInPc4d1
háje	háj	k1gInPc1
a	a	k8xC
skály	skála	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jihovýchodním	jihovýchodní	k2eAgInSc6d1
svahu	svah	k1gInSc6
hory	hora	k1gFnSc2
stojí	stát	k5eAaImIp3nS
starý	starý	k2eAgInSc1d1
pomníček	pomníček	k1gInSc1
dřevorubce	dřevorubec	k1gMnSc2
Geisslera	Geissler	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zde	zde	k6eAd1
v	v	k7c6
náručí	náručí	k1gNnSc6
svých	svůj	k3xOyFgMnPc2
tří	tři	k4xCgMnPc2
bratrů	bratr	k1gMnPc2
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
zástavu	zástava	k1gFnSc4
srdce	srdce	k1gNnSc2
při	při	k7c6
kácení	kácení	k1gNnSc6
stromu	strom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Bučina	bučina	k1gFnSc1
na	na	k7c6
severovýchodním	severovýchodní	k2eAgNnSc6d1
úbočí	úbočí	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oldřichovský	Oldřichovský	k2eAgInSc1d1
Špičák	špičák	k1gInSc1
na	na	k7c4
Ceskehory	Ceskehora	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Geisslerova	Geisslerův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
na	na	k7c4
Jizerpom	Jizerpom	k1gInSc4
<g/>
.	.	kIx.
<g/>
wz	wz	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Oldřichovský	Oldřichovský	k2eAgInSc4d1
Špičák	špičák	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ultrakopce	Ultrakopce	k1gFnPc1
na	na	k7c4
Ultratisicovky	Ultratisicovka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
