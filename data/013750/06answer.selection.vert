<s>
Špičák	špičák	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
často	často	k6eAd1
Oldřichovský	Oldřichovský	k2eAgInSc1d1
Špičák	špičák	k1gInSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
Buschullersdorfer	Buschullersdorfer	k2eAgInSc1d1
Spitzberg	Spitzberg	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
724	#num#	k4
m	m	kA
n.	n.	kA
m.	m.	kA
vysoký	vysoký	k2eAgInSc1d1
vrchol	vrchol	k1gInSc1
s	s	k7c7
vyhlídkou	vyhlídka	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1
dominuje	dominovat	k5eAaImIp3nS
západní	západní	k2eAgFnSc3d1
části	část	k1gFnSc3
Jizerských	jizerský	k2eAgFnPc2d1
hor	hora	k1gFnPc2
nad	nad	k7c7
Oldřichovem	Oldřichovo	k1gNnSc7
v	v	k7c6
Hájích	háj	k1gInPc6
<g/>
.	.	kIx.
</s>