<s>
Isaac	Isaac	k1gInSc1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgMnSc1d1	narozen
jako	jako	k8xS	jako
Izák	Izák	k1gMnSc1	Izák
Judovič	Judovič	k1gMnSc1	Judovič
Ozimov	Ozimov	k1gInSc1	Ozimov
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
И	И	k?	И
Ю	Ю	k?	Ю
О	О	k?	О
<g/>
)	)	kIx)	)
asi	asi	k9	asi
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Petroviči	Petrovič	k1gMnSc6	Petrovič
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
biochemik	biochemik	k1gMnSc1	biochemik
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
přes	přes	k7c4	přes
300	[number]	k4	300
vědeckých	vědecký	k2eAgInPc2d1	vědecký
<g/>
,	,	kIx,	,
vědecko-populárních	vědeckoopulární	k2eAgInPc2d1	vědecko-populární
a	a	k8xC	a
sci-fi	scii	k1gNnSc3	sci-fi
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
období	období	k1gNnSc2	období
tzv.	tzv.	kA	tzv.
Zlatého	zlatý	k1gInSc2	zlatý
věku	věk	k1gInSc2	věk
sci-fi	scii	k1gNnPc2	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
prestižních	prestižní	k2eAgFnPc2d1	prestižní
cen	cena	k1gFnPc2	cena
(	(	kIx(	(
<g/>
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
,	,	kIx,	,
Nebula	nebula	k1gFnSc1	nebula
<g/>
,	,	kIx,	,
Locus	Locus	k1gInSc1	Locus
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
tří	tři	k4xCgNnPc2	tři
zákonů	zákon	k1gInPc2	zákon
robotiky	robotika	k1gFnSc2	robotika
<g/>
.	.	kIx.	.
</s>
<s>
Isaac	Isaac	k1gInSc1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
do	do	k7c2	do
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Petroviči	Petrovič	k1gMnPc7	Petrovič
v	v	k7c6	v
Sovětském	sovětský	k2eAgNnSc6d1	sovětské
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
autobiografii	autobiografie	k1gFnSc6	autobiografie
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k9	jako
datum	datum	k1gNnSc1	datum
svého	svůj	k3xOyFgNnSc2	svůj
narození	narození	k1gNnSc2	narození
považuje	považovat	k5eAaImIp3nS	považovat
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
chybějícím	chybějící	k2eAgInPc3d1	chybějící
dokumentům	dokument	k1gInPc3	dokument
či	či	k8xC	či
případné	případný	k2eAgFnSc3d1	případná
chybě	chyba	k1gFnSc3	chyba
při	při	k7c6	při
převodu	převod	k1gInSc6	převod
z	z	k7c2	z
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
nejzazší	zadní	k2eAgNnSc1d3	nejzazší
a	a	k8xC	a
správné	správný	k2eAgNnSc1d1	správné
datum	datum	k1gNnSc1	datum
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
americkým	americký	k2eAgMnSc7d1	americký
občanem	občan	k1gMnSc7	občan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Asimov	Asimov	k1gInSc1	Asimov
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nenaučil	naučit	k5eNaPmAgInS	naučit
rusky	rusky	k6eAd1	rusky
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
mluvili	mluvit	k5eAaImAgMnP	mluvit
jidiš	jidiš	k6eAd1	jidiš
<g/>
)	)	kIx)	)
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
svou	svůj	k3xOyFgFnSc4	svůj
rodnou	rodný	k2eAgFnSc4d1	rodná
zemi	zem	k1gFnSc4	zem
nenavštívil	navštívit	k5eNaPmAgMnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Isaac	Isaac	k1gInSc1	Isaac
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
měl	mít	k5eAaImAgMnS	mít
malý	malý	k2eAgInSc4d1	malý
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
Asimov	Asimov	k1gInSc1	Asimov
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
časopisy	časopis	k1gInPc7	časopis
a	a	k8xC	a
novinami	novina	k1gFnPc7	novina
<g/>
.	.	kIx.	.
</s>
<s>
Asimov	Asimov	k1gInSc1	Asimov
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
sám	sám	k3xTgMnSc1	sám
číst	číst	k5eAaImF	číst
v	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
články	článek	k1gInPc4	článek
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
už	už	k6eAd1	už
v	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
a	a	k8xC	a
od	od	k7c2	od
devatenácti	devatenáct	k4xCc2	devatenáct
je	on	k3xPp3gFnPc4	on
dokonce	dokonce	k9	dokonce
prodával	prodávat	k5eAaImAgInS	prodávat
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vystudování	vystudování	k1gNnSc6	vystudování
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
Asimov	Asimov	k1gInSc1	Asimov
na	na	k7c4	na
Kolumbijskou	kolumbijský	k2eAgFnSc4d1	kolumbijská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
promoval	promovat	k5eAaBmAgMnS	promovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sloužil	sloužit	k5eAaImAgInS	sloužit
Asimov	Asimov	k1gInSc1	Asimov
jako	jako	k8xC	jako
civilista	civilista	k1gMnSc1	civilista
na	na	k7c6	na
námořní	námořní	k2eAgFnSc6d1	námořní
základně	základna	k1gFnSc6	základna
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
(	(	kIx(	(
<g/>
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Naval	navalit	k5eAaPmRp2nS	navalit
Shipyard	Shipyard	k1gInSc1	Shipyard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
předvolán	předvolat	k5eAaPmNgInS	předvolat
k	k	k7c3	k
armádní	armádní	k2eAgFnSc3d1	armádní
odvodové	odvodový	k2eAgFnSc3d1	odvodová
prohlídce	prohlídka	k1gFnSc3	prohlídka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
krátkozrakosti	krátkozrakost	k1gFnSc3	krátkozrakost
nebyl	být	k5eNaImAgInS	být
odveden	odvést	k5eAaPmNgInS	odvést
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vada	vada	k1gFnSc1	vada
už	už	k6eAd1	už
nebyla	být	k5eNaImAgFnS	být
armádě	armáda	k1gFnSc3	armáda
USA	USA	kA	USA
na	na	k7c4	na
obtíž	obtíž	k1gFnSc4	obtíž
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Asimov	Asimov	k1gInSc1	Asimov
obdržel	obdržet	k5eAaPmAgInS	obdržet
povolávací	povolávací	k2eAgInSc1d1	povolávací
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jejích	její	k3xOp3gFnPc2	její
řad	řada	k1gFnPc2	řada
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Prošel	projít	k5eAaPmAgInS	projít
základním	základní	k2eAgInSc7d1	základní
výcvikem	výcvik	k1gInSc7	výcvik
v	v	k7c4	v
Camp	camp	k1gInSc4	camp
Lee	Lea	k1gFnSc3	Lea
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1946	[number]	k4	1946
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
povídku	povídka	k1gFnSc4	povídka
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
jediná	jediný	k2eAgFnSc1d1	jediná
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Asimov	Asimov	k1gInSc1	Asimov
napsal	napsat	k5eAaPmAgInS	napsat
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
USA	USA	kA	USA
(	(	kIx(	(
<g/>
období	období	k1gNnSc4	období
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1945	[number]	k4	1945
do	do	k7c2	do
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
již	již	k9	již
měl	mít	k5eAaImAgInS	mít
hotovu	hotov	k2eAgFnSc4d1	hotova
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
s	s	k7c7	s
psaním	psaní	k1gNnSc7	psaní
přestal	přestat	k5eAaPmAgMnS	přestat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
operace	operace	k1gFnPc4	operace
Crossroads	Crossroadsa	k1gFnPc2	Crossroadsa
na	na	k7c6	na
atolu	atol	k1gInSc6	atol
Bikini	Bikini	k1gNnSc2	Bikini
<g/>
.	.	kIx.	.
</s>
<s>
Odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
s	s	k7c7	s
ostatními	ostatní	k1gNnPc7	ostatní
na	na	k7c4	na
Havaj	Havaj	k1gFnSc4	Havaj
do	do	k7c2	do
Honolulu	Honolulu	k1gNnSc2	Honolulu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jaderného	jaderný	k2eAgInSc2d1	jaderný
testu	test	k1gInSc2	test
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Camp	camp	k1gInSc4	camp
Lee	Lea	k1gFnSc3	Lea
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
propuštění	propuštění	k1gNnSc6	propuštění
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
kvůli	kvůli	k7c3	kvůli
doktorandské	doktorandský	k2eAgFnSc3d1	doktorandská
práci	práce	k1gFnSc3	práce
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Odcházel	odcházet	k5eAaImAgMnS	odcházet
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
desátníka	desátník	k1gMnSc2	desátník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
dokončil	dokončit	k5eAaPmAgMnS	dokončit
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
doktorem	doktor	k1gMnSc7	doktor
biochemie	biochemie	k1gFnSc2	biochemie
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
biochemie	biochemie	k1gFnSc2	biochemie
na	na	k7c6	na
Bostonské	bostonský	k2eAgFnSc6d1	Bostonská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
profesionálním	profesionální	k2eAgMnSc7d1	profesionální
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Asimov	Asimov	k1gInSc1	Asimov
byl	být	k5eAaImAgInS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
:	:	kIx,	:
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
s	s	k7c7	s
Gertrude	Gertrud	k1gInSc5	Gertrud
Blugerman	Blugerman	k1gMnSc1	Blugerman
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
měli	mít	k5eAaImAgMnP	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Davida	David	k1gMnSc2	David
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Robyn	Robyna	k1gFnPc2	Robyna
Joan	Joana	k1gFnPc2	Joana
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
postavu	postava	k1gFnSc4	postava
Wandy	Wanda	k1gFnSc2	Wanda
Seldon	Seldona	k1gFnPc2	Seldona
<g/>
.	.	kIx.	.
</s>
<s>
Wanda	Wanda	k1gFnSc1	Wanda
byla	být	k5eAaImAgFnS	být
vnučkou	vnučka	k1gFnSc7	vnučka
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
Nadaci	nadace	k1gFnSc6	nadace
Hariho	Hari	k1gMnSc2	Hari
Seldona	Seldon	k1gMnSc2	Seldon
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
a	a	k8xC	a
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Asimov	Asimov	k1gInSc1	Asimov
oženil	oženit	k5eAaPmAgInS	oženit
znovu	znovu	k6eAd1	znovu
-	-	kIx~	-
vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
americkou	americký	k2eAgFnSc4d1	americká
autorku	autorka	k1gFnSc4	autorka
sci-fi	scii	k1gNnSc2	sci-fi
Janet	Janeta	k1gFnPc2	Janeta
O.	O.	kA	O.
Jeppson	Jeppson	k1gInSc1	Jeppson
<g/>
.	.	kIx.	.
</s>
<s>
Isaac	Isaac	k1gInSc1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
jeho	jeho	k3xOp3gNnSc1	jeho
druhá	druhý	k4xOgFnSc1	druhý
žena	žena	k1gFnSc1	žena
odhalila	odhalit	k5eAaPmAgFnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nemoci	nemoc	k1gFnSc2	nemoc
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nákaze	nákaza	k1gFnSc3	nákaza
došlo	dojít	k5eAaPmAgNnS	dojít
při	při	k7c6	při
operaci	operace	k1gFnSc6	operace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Asimovovi	Asimovův	k2eAgMnPc1d1	Asimovův
podána	podat	k5eAaPmNgFnS	podat
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
HIV	HIV	kA	HIV
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
<g/>
.	.	kIx.	.
</s>
<s>
Asimov	Asimov	k1gInSc1	Asimov
trpěl	trpět	k5eAaImAgInS	trpět
klaustrofilií	klaustrofilie	k1gFnSc7	klaustrofilie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
přímý	přímý	k2eAgInSc1d1	přímý
opak	opak	k1gInSc1	opak
ke	k	k7c3	k
klaustrofobii	klaustrofobie	k1gFnSc3	klaustrofobie
<g/>
.	.	kIx.	.
</s>
<s>
Miloval	milovat	k5eAaImAgInS	milovat
malé	malý	k2eAgInPc4d1	malý
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
prostory	prostor	k1gInPc4	prostor
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
romány	román	k1gInPc1	román
Ocelové	ocelový	k2eAgFnSc2d1	ocelová
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
,	,	kIx,	,
Nahé	nahý	k2eAgNnSc1d1	nahé
slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
Roboti	robot	k1gMnPc1	robot
úsvitu	úsvit	k1gInSc2	úsvit
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
první	první	k4xOgFnSc2	první
povídky	povídka	k1gFnSc2	povídka
ze	z	k7c2	z
série	série	k1gFnSc2	série
Nadace	nadace	k1gFnSc2	nadace
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
románech	román	k1gInPc6	román
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
podzemních	podzemní	k2eAgNnPc6d1	podzemní
městech	město	k1gNnPc6	město
a	a	k8xC	a
panicky	panicky	k6eAd1	panicky
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
otevřeného	otevřený	k2eAgInSc2d1	otevřený
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
paradoxem	paradox	k1gInSc7	paradox
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Asimov	Asimov	k1gInSc1	Asimov
bál	bát	k5eAaImAgInS	bát
létat	létat	k5eAaImF	létat
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
především	především	k9	především
romány	román	k1gInPc1	román
o	o	k7c4	o
Nadaci	nadace	k1gFnSc4	nadace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plné	plný	k2eAgNnSc1d1	plné
kosmických	kosmický	k2eAgInPc2d1	kosmický
letů	let	k1gInPc2	let
a	a	k8xC	a
meziplanetárních	meziplanetární	k2eAgFnPc2d1	meziplanetární
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
Asimov	Asimov	k1gInSc4	Asimov
přitom	přitom	k6eAd1	přitom
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
letěl	letět	k5eAaImAgMnS	letět
v	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
pouze	pouze	k6eAd1	pouze
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
Asimov	Asimov	k1gInSc1	Asimov
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nenaučil	naučit	k5eNaPmAgInS	naučit
plavat	plavat	k5eAaImF	plavat
či	či	k8xC	či
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nestavěl	stavět	k5eNaImAgMnS	stavět
proti	proti	k7c3	proti
náboženství	náboženství	k1gNnSc3	náboženství
jako	jako	k9	jako
takovému	takový	k3xDgMnSc3	takový
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
proti	proti	k7c3	proti
spoléhání	spoléhání	k1gNnSc3	spoléhání
se	se	k3xPyFc4	se
na	na	k7c4	na
víru	víra	k1gFnSc4	víra
v	v	k7c6	v
neověřitelné	ověřitelný	k2eNgFnSc6d1	neověřitelná
či	či	k8xC	či
neověřené	ověřený	k2eNgFnSc6d1	neověřená
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
beletristických	beletristický	k2eAgFnPc2d1	beletristická
i	i	k8xC	i
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšel	vyjít	k5eAaPmAgMnS	vyjít
jen	jen	k9	jen
nepatrný	patrný	k2eNgInSc4d1	patrný
zlomek	zlomek	k1gInSc4	zlomek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
hlavně	hlavně	k9	hlavně
svými	svůj	k3xOyFgInPc7	svůj
díly	díl	k1gInPc7	díl
z	z	k7c2	z
vědecko-fantastického	vědeckoantastický	k2eAgInSc2d1	vědecko-fantastický
žánru	žánr	k1gInSc2	žánr
(	(	kIx(	(
<g/>
sci-fi	scii	k1gFnSc1	sci-fi
<g/>
)	)	kIx)	)
a	a	k8xC	a
populárně	populárně	k6eAd1	populárně
vědeckými	vědecký	k2eAgFnPc7d1	vědecká
knihami	kniha	k1gFnPc7	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
Asimovým	Asimův	k2eAgInSc7d1	Asimův
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
série	série	k1gFnSc1	série
Nadace	nadace	k1gFnSc1	nadace
(	(	kIx(	(
<g/>
Foundation	Foundation	k1gInSc1	Foundation
series	series	k1gInSc1	series
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
později	pozdě	k6eAd2	pozdě
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
dvěma	dva	k4xCgFnPc7	dva
sériemi	série	k1gFnPc7	série
-	-	kIx~	-
o	o	k7c6	o
galaktické	galaktický	k2eAgFnSc6d1	Galaktická
Říši	říš	k1gFnSc6	říš
(	(	kIx(	(
<g/>
Galactic	Galactice	k1gFnPc2	Galactice
Empire	empir	k1gInSc5	empir
series	seriesa	k1gFnPc2	seriesa
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c6	o
robotech	robot	k1gInPc6	robot
(	(	kIx(	(
<g/>
Robot	robot	k1gMnSc1	robot
series	series	k1gMnSc1	series
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Robotům	robot	k1gMnPc3	robot
se	se	k3xPyFc4	se
Asimov	Asimov	k1gInSc4	Asimov
věnoval	věnovat	k5eAaImAgInS	věnovat
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
povídku	povídka	k1gFnSc4	povídka
o	o	k7c6	o
robotovi	robot	k1gMnSc6	robot
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
se	s	k7c7	s
"	"	kIx"	"
<g/>
Robbie	Robbie	k1gFnPc4	Robbie
<g/>
"	"	kIx"	"
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
najít	najít	k5eAaPmF	najít
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
povídek	povídka	k1gFnPc2	povídka
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
robot	robot	k1gMnSc1	robot
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
bezpočet	bezpočet	k1gInSc1	bezpočet
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
několik	několik	k4yIc1	několik
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
problematice	problematika	k1gFnSc6	problematika
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
robot	robot	k1gMnSc1	robot
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
tzv.	tzv.	kA	tzv.
zákonů	zákon	k1gInPc2	zákon
robotiky	robotika	k1gFnSc2	robotika
<g/>
.	.	kIx.	.
</s>
<s>
Asimov	Asimov	k1gInSc4	Asimov
sám	sám	k3xTgInSc4	sám
přiznal	přiznat	k5eAaPmAgInS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovo	slovo	k1gNnSc1	slovo
robot	robota	k1gFnPc2	robota
je	být	k5eAaImIp3nS	být
přejaté	přejatý	k2eAgNnSc1d1	přejaté
z	z	k7c2	z
dramatu	drama	k1gNnSc2	drama
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
RUR	RUR	kA	RUR
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Asimov	Asimov	k1gInSc1	Asimov
toto	tento	k3xDgNnSc4	tento
drama	drama	k1gNnSc4	drama
přečetl	přečíst	k5eAaPmAgMnS	přečíst
a	a	k8xC	a
netajil	tajit	k5eNaImAgMnS	tajit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dílo	dílo	k1gNnSc4	dílo
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
také	také	k9	také
detektivky	detektivka	k1gFnPc4	detektivka
<g/>
,	,	kIx,	,
fantasy	fantas	k1gInPc4	fantas
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
"	"	kIx"	"
<g/>
ne-fantastických	antastický	k2eNgFnPc2d1	-fantastický
<g/>
"	"	kIx"	"
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
napsal	napsat	k5eAaPmAgInS	napsat
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
editor	editor	k1gInSc1	editor
upravil	upravit	k5eAaPmAgInS	upravit
kolem	kolem	k7c2	kolem
500	[number]	k4	500
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
90	[number]	k4	90
000	[number]	k4	000
dopisů	dopis	k1gInPc2	dopis
nebo	nebo	k8xC	nebo
pohlednic	pohlednice	k1gFnPc2	pohlednice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
práce	práce	k1gFnPc1	práce
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
hlavních	hlavní	k2eAgFnPc2d1	hlavní
kategorií	kategorie	k1gFnPc2	kategorie
Mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
desetinného	desetinný	k2eAgNnSc2d1	desetinné
třídění	třídění	k1gNnSc2	třídění
<g/>
,	,	kIx,	,
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnSc3d1	velká
trojce	trojka	k1gFnSc3	trojka
<g/>
"	"	kIx"	"
autorů	autor	k1gMnPc2	autor
sci-fi	scii	k1gFnSc4	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Isaac	Isaac	k1gInSc1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
byl	být	k5eAaImAgMnS	být
humanista	humanista	k1gMnSc1	humanista
a	a	k8xC	a
racionalista	racionalista	k1gMnSc1	racionalista
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
mistra	mistr	k1gMnSc4	mistr
žánru	žánr	k1gInSc2	žánr
vědecké	vědecký	k2eAgFnSc2d1	vědecká
fantastiky	fantastika	k1gFnSc2	fantastika
a	a	k8xC	a
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
společně	společně	k6eAd1	společně
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
A.	A.	kA	A.
Heinleinem	Heinlein	k1gMnSc7	Heinlein
a	a	k8xC	a
Arthurem	Arthur	k1gMnSc7	Arthur
C.	C.	kA	C.
Clarkem	Clarek	k1gMnSc7	Clarek
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
velkou	velký	k2eAgFnSc4d1	velká
trojku	trojka	k1gFnSc4	trojka
<g/>
"	"	kIx"	"
autorů	autor	k1gMnPc2	autor
sci-fi	scii	k1gFnSc4	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zákony	zákon	k1gInPc4	zákon
robotiky	robotika	k1gFnSc2	robotika
<g/>
.	.	kIx.	.
</s>
<s>
Asimov	Asimov	k1gInSc1	Asimov
je	být	k5eAaImIp3nS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
zákonů	zákon	k1gInPc2	zákon
robotiky	robotika	k1gFnSc2	robotika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
časem	čas	k1gInSc7	čas
staly	stát	k5eAaPmAgInP	stát
dogmatem	dogma	k1gNnSc7	dogma
sci-fi	scii	k1gFnSc2	sci-fi
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Popisují	popisovat	k5eAaImIp3nP	popisovat
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgNnPc7	jaký
pravidly	pravidlo	k1gNnPc7	pravidlo
se	se	k3xPyFc4	se
roboti	robot	k1gMnPc1	robot
mají	mít	k5eAaImIp3nP	mít
řídit	řídit	k5eAaImF	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
uvedeny	uvést	k5eAaPmNgFnP	uvést
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1942	[number]	k4	1942
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
"	"	kIx"	"
<g/>
Hra	hra	k1gFnSc1	hra
na	na	k7c4	na
honěnou	honěná	k1gFnSc4	honěná
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Runaround	Runaround	k1gInSc1	Runaround
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Násilí	násilí	k1gNnSc1	násilí
je	být	k5eAaImIp3nS	být
posledním	poslední	k2eAgNnSc7d1	poslední
útočištěm	útočiště	k1gNnSc7	útočiště
neschopných	schopný	k2eNgMnPc2d1	neschopný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
-	-	kIx~	-
Salvor	Salvor	k1gInSc1	Salvor
Hardin	Hardina	k1gFnPc2	Hardina
<g/>
,	,	kIx,	,
Nadace	nadace	k1gFnSc1	nadace
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
nedopusťte	dopustit	k5eNaPmRp2nP	dopustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vám	vy	k3xPp2nPc3	vy
smysl	smysl	k1gInSc1	smysl
pro	pro	k7c4	pro
morálku	morálka	k1gFnSc4	morálka
zabránil	zabránit	k5eAaPmAgInS	zabránit
udělat	udělat	k5eAaPmF	udělat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
-	-	kIx~	-
Hober	hobra	k1gFnPc2	hobra
Mallow	Mallow	k1gMnPc2	Mallow
<g/>
,	,	kIx,	,
Nadace	nadace	k1gFnSc1	nadace
7	[number]	k4	7
cen	cena	k1gFnPc2	cena
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
:	:	kIx,	:
1963	[number]	k4	1963
<g/>
:	:	kIx,	:
výběr	výběr	k1gInSc1	výběr
F	F	kA	F
<g/>
&	&	k?	&
<g/>
SF	SF	kA	SF
Science	Science	k1gFnSc1	Science
Articles	Articles	k1gMnSc1	Articles
(	(	kIx(	(
<g/>
Special	Special	k1gMnSc1	Special
Achievement	Achievement	k1gMnSc1	Achievement
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
<g/>
:	:	kIx,	:
série	série	k1gFnSc1	série
Nadace	nadace	k1gFnSc1	nadace
-	-	kIx~	-
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(	(
<g/>
Best	Best	k1gInSc1	Best
All-time	Allim	k1gInSc5	All-tim
Series	Series	k1gMnSc1	Series
<g/>
)	)	kIx)	)
1973	[number]	k4	1973
<g/>
:	:	kIx,	:
román	román	k1gInSc4	román
Ani	ani	k9	ani
sami	sám	k3xTgMnPc1	sám
<g />
.	.	kIx.	.
</s>
<s>
bohové	bůh	k1gMnPc1	bůh
-	-	kIx~	-
The	The	k1gMnSc1	The
Gods	Godsa	k1gFnPc2	Godsa
Themselves	Themselves	k1gMnSc1	Themselves
(	(	kIx(	(
<g/>
Novel	novela	k1gFnPc2	novela
<g/>
)	)	kIx)	)
1977	[number]	k4	1977
<g/>
:	:	kIx,	:
povídka	povídka	k1gFnSc1	povídka
Dvěstěletý	dvěstěletý	k2eAgMnSc1d1	dvěstěletý
člověk	člověk	k1gMnSc1	člověk
-	-	kIx~	-
The	The	k1gMnSc1	The
Bicentennial	Bicentennial	k1gMnSc1	Bicentennial
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
Novelette	Novelett	k1gInSc5	Novelett
<g/>
)	)	kIx)	)
1983	[number]	k4	1983
<g/>
:	:	kIx,	:
román	román	k1gInSc1	román
Nadace	nadace	k1gFnSc2	nadace
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
-	-	kIx~	-
Foundation	Foundation	k1gInSc4	Foundation
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Edge	Edge	k1gFnSc7	Edge
(	(	kIx(	(
<g/>
Novel	novela	k1gFnPc2	novela
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
povídka	povídka	k1gFnSc1	povídka
Zlato	zlato	k1gNnSc4	zlato
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Gold	Gold	k1gInSc4	Gold
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Novelette	Novelett	k1gMnSc5	Novelett
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
paměti	paměť	k1gFnSc2	paměť
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Asimov	Asimov	k1gInSc1	Asimov
-	-	kIx~	-
I.	I.	kA	I.
Asimov	Asimov	k1gInSc1	Asimov
<g/>
:	:	kIx,	:
A	a	k9	a
Memoir	Memoir	k1gInSc1	Memoir
(	(	kIx(	(
<g/>
Non-Fiction	Non-Fiction	k1gInSc1	Non-Fiction
<g/>
)	)	kIx)	)
2	[number]	k4	2
ceny	cena	k1gFnSc2	cena
Nebula	nebula	k1gFnSc1	nebula
<g/>
:	:	kIx,	:
1972	[number]	k4	1972
<g/>
:	:	kIx,	:
román	román	k1gInSc1	román
Ani	ani	k8xC	ani
sami	sám	k3xTgMnPc1	sám
bohové	bůh	k1gMnPc1	bůh
-	-	kIx~	-
The	The	k1gFnSc1	The
Gods	Gods	k1gInSc4	Gods
Themselves	Themselves	k1gInSc1	Themselves
1976	[number]	k4	1976
<g/>
:	:	kIx,	:
povídka	povídka	k1gFnSc1	povídka
<g />
.	.	kIx.	.
</s>
<s>
Dvousetletý	dvousetletý	k2eAgMnSc1d1	dvousetletý
člověk	člověk	k1gMnSc1	člověk
-	-	kIx~	-
The	The	k1gMnSc1	The
Bicentennial	Bicentennial	k1gMnSc1	Bicentennial
Man	Man	k1gMnSc1	Man
7	[number]	k4	7
cen	cena	k1gFnPc2	cena
Locus	Locus	k1gMnSc1	Locus
1973	[number]	k4	1973
<g/>
:	:	kIx,	:
román	román	k1gInSc1	román
Ani	ani	k8xC	ani
sami	sám	k3xTgMnPc1	sám
bohové	bůh	k1gMnPc1	bůh
-	-	kIx~	-
The	The	k1gMnSc1	The
Gods	Godsa	k1gFnPc2	Godsa
Themselves	Themselves	k1gMnSc1	Themselves
(	(	kIx(	(
<g/>
Novel	novela	k1gFnPc2	novela
<g/>
)	)	kIx)	)
1975	[number]	k4	1975
<g/>
:	:	kIx,	:
Before	Befor	k1gInSc5	Befor
the	the	k?	the
Golden	Goldna	k1gFnPc2	Goldna
Age	Age	k1gFnSc1	Age
(	(	kIx(	(
<g/>
Anthology	Antholog	k1gMnPc7	Antholog
<g/>
)	)	kIx)	)
1977	[number]	k4	1977
<g/>
:	:	kIx,	:
povídka	povídka	k1gFnSc1	povídka
Dvousetletý	dvousetletý	k2eAgMnSc1d1	dvousetletý
člověk	člověk	k1gMnSc1	člověk
-	-	kIx~	-
The	The	k1gMnSc1	The
Bicentennial	Bicentennial	k1gMnSc1	Bicentennial
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
Novelette	Novelett	k1gInSc5	Novelett
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
1981	[number]	k4	1981
<g/>
:	:	kIx,	:
In	In	k1gMnSc1	In
Joy	Joy	k1gMnSc1	Joy
Still	Still	k1gMnSc1	Still
Felt	Felt	k1gMnSc1	Felt
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Autobiography	Autobiographa	k1gFnSc2	Autobiographa
of	of	k?	of
Isaac	Isaac	k1gInSc1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
<g/>
,	,	kIx,	,
1954-1978	[number]	k4	1954-1978
(	(	kIx(	(
<g/>
related	related	k1gInSc1	related
non-fiction	noniction	k1gInSc1	non-fiction
book	book	k1gInSc1	book
<g/>
)	)	kIx)	)
1983	[number]	k4	1983
<g/>
:	:	kIx,	:
román	román	k1gInSc1	román
Nadace	nadace	k1gFnSc2	nadace
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
-	-	kIx~	-
Foundation	Foundation	k1gInSc4	Foundation
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Edge	Edge	k1gFnSc7	Edge
(	(	kIx(	(
<g/>
Novel	novela	k1gFnPc2	novela
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
povídka	povídka	k1gFnSc1	povídka
Sny	sen	k1gInPc1	sen
<g />
.	.	kIx.	.
</s>
<s>
robotů	robot	k1gMnPc2	robot
-	-	kIx~	-
Robot	robot	k1gInSc1	robot
Dreams	Dreams	k1gInSc1	Dreams
(	(	kIx(	(
<g/>
Short	Short	k1gInSc1	Short
Story	story	k1gFnSc2	story
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
paměti	paměť	k1gFnSc2	paměť
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Asimov	Asimov	k1gInSc1	Asimov
-	-	kIx~	-
I.	I.	kA	I.
Asimov	Asimov	k1gInSc1	Asimov
<g/>
:	:	kIx,	:
A	a	k9	a
Memoir	Memoir	k1gInSc1	Memoir
(	(	kIx(	(
<g/>
Non-Fiction	Non-Fiction	k1gInSc1	Non-Fiction
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
<g/>
:	:	kIx,	:
cena	cena	k1gFnSc1	cena
E.	E.	kA	E.
E.	E.	kA	E.
Smithe	Smithe	k1gFnSc1	Smithe
-	-	kIx~	-
Skylark	Skylark	k1gInSc1	Skylark
Award	Awarda	k1gFnPc2	Awarda
<g/>
,	,	kIx,	,
uděluje	udělovat	k5eAaImIp3nS	udělovat
New	New	k1gFnSc1	New
England	Englanda	k1gFnPc2	Englanda
Science	Science	k1gFnSc1	Science
Fiction	Fiction	k1gInSc1	Fiction
Association	Association	k1gInSc1	Association
1986	[number]	k4	1986
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
Velmistrem	velmistr	k1gMnSc7	velmistr
žánru	žánr	k1gInSc2	žánr
-	-	kIx~	-
SFWA	SFWA	kA	SFWA
award	awarda	k1gFnPc2	awarda
for	forum	k1gNnPc2	forum
Grand	grand	k1gMnSc1	grand
Master	master	k1gMnSc1	master
<g/>
,	,	kIx,	,
uděluje	udělovat	k5eAaImIp3nS	udělovat
Science	Science	k1gFnSc1	Science
Fiction	Fiction	k1gInSc1	Fiction
and	and	k?	and
Fantasy	fantas	k1gInPc1	fantas
Writers	Writers	k1gInSc1	Writers
of	of	k?	of
America	Americ	k1gInSc2	Americ
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
Forry	Forra	k1gFnPc1	Forra
Award	Awardo	k1gNnPc2	Awardo
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
uděluje	udělovat	k5eAaImIp3nS	udělovat
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
Science	Science	k1gFnSc2	Science
Fantasy	fantas	k1gInPc1	fantas
Society	societa	k1gFnSc2	societa
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
-	-	kIx~	-
Science	Science	k1gFnSc1	Science
Fiction	Fiction	k1gInSc1	Fiction
and	and	k?	and
Fantasy	fantas	k1gInPc1	fantas
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
,	,	kIx,	,
uděluje	udělovat	k5eAaImIp3nS	udělovat
Kansas	Kansas	k1gInSc1	Kansas
City	City	k1gFnSc2	City
Science	Science	k1gFnSc2	Science
Fiction	Fiction	k1gInSc1	Fiction
and	and	k?	and
Fantasy	fantas	k1gInPc7	fantas
Society	societa	k1gFnSc2	societa
Následuje	následovat	k5eAaImIp3nS	následovat
seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Uvedeno	uveden	k2eAgNnSc1d1	uvedeno
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
první	první	k4xOgNnSc4	první
české	český	k2eAgNnSc4d1	české
vydání	vydání	k1gNnSc4	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Základna	základna	k1gFnSc1	základna
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
samizdatové	samizdatový	k2eAgNnSc1d1	samizdatové
vydání	vydání	k1gNnSc1	vydání
prvních	první	k4xOgInPc2	první
3	[number]	k4	3
dílů	díl	k1gInPc2	díl
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
Nadace	nadace	k1gFnSc2	nadace
<g/>
,	,	kIx,	,
AG	AG	kA	AG
Kult	kult	k1gInSc4	kult
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
Nadace	nadace	k1gFnSc1	nadace
a	a	k8xC	a
Říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
AG	AG	kA	AG
Kult	kult	k1gInSc4	kult
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
Foundation	Foundation	k1gInSc1	Foundation
and	and	k?	and
Empire	empir	k1gInSc5	empir
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
Druhá	druhý	k4xOgFnSc1	druhý
Nadace	nadace	k1gFnSc1	nadace
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
AG	AG	kA	AG
Kult	kult	k1gInSc4	kult
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
Second	Seconda	k1gFnPc2	Seconda
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
Nadace	nadace	k1gFnSc1	nadace
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
<g/>
,	,	kIx,	,
AG	AG	kA	AG
Kult	kult	k1gInSc4	kult
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
Foundation	Foundation	k1gInSc1	Foundation
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Edge	Edge	k1gFnSc7	Edge
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Nadace	nadace	k1gFnSc2	nadace
a	a	k8xC	a
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
AG	AG	kA	AG
Kult	kult	k1gInSc4	kult
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
Foundation	Foundation	k1gInSc1	Foundation
and	and	k?	and
Earth	Earth	k1gInSc1	Earth
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Předehra	předehra	k1gFnSc1	předehra
k	k	k7c3	k
Nadaci	nadace	k1gFnSc3	nadace
<g/>
,	,	kIx,	,
AG	AG	kA	AG
Kult	kult	k1gInSc4	kult
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
Prelude	Prelud	k1gInSc5	Prelud
to	ten	k3xDgNnSc1	ten
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
A	a	k8xC	a
zrodí	zrodit	k5eAaPmIp3nP	zrodit
se	se	k3xPyFc4	se
Nadace	nadace	k1gFnSc2	nadace
<g/>
,	,	kIx,	,
And	Anda	k1gFnPc2	Anda
Classic	Classice	k1gInPc2	Classice
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
Forward	Forward	k1gInSc1	Forward
the	the	k?	the
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Ocelové	ocelový	k2eAgFnSc2d1	ocelová
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Caves	Caves	k1gMnSc1	Caves
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
Steel	Steel	k1gMnSc1	Steel
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Nahé	nahý	k2eAgNnSc1d1	nahé
slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Naked	Naked	k1gInSc1	Naked
Sun	Sun	kA	Sun
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Roboti	robot	k1gMnPc1	robot
úsvitu	úsvit	k1gInSc2	úsvit
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
Julese	Julese	k1gFnSc2	Julese
Vernea	Verne	k1gInSc2	Verne
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Robots	Robotsa	k1gFnPc2	Robotsa
of	of	k?	of
Dawn	Dawna	k1gFnPc2	Dawna
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Roboti	robot	k1gMnPc1	robot
a	a	k8xC	a
impérium	impérium	k1gNnSc1	impérium
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
Julese	Julese	k1gFnSc2	Julese
Vernea	Verneum	k1gNnSc2	Verneum
<g />
.	.	kIx.	.
</s>
<s>
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
Robots	Robots	k1gInSc1	Robots
and	and	k?	and
Empire	empir	k1gInSc5	empir
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Oblázek	oblázek	k1gInSc1	oblázek
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
AF	AF	kA	AF
167	[number]	k4	167
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
Pebble	Pebble	k1gFnSc1	Pebble
in	in	k?	in
the	the	k?	the
Sky	Sky	k1gMnSc1	Sky
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Hvězdy	hvězda	k1gFnPc1	hvězda
jako	jako	k8xC	jako
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
AF	AF	kA	AF
167	[number]	k4	167
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Stars	Stars	k1gInSc1	Stars
<g/>
,	,	kIx,	,
Like	Like	k1gNnSc1	Like
Dust	Dusta	k1gFnPc2	Dusta
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Kosmické	kosmický	k2eAgInPc1d1	kosmický
proudy	proud	k1gInPc1	proud
<g/>
,	,	kIx,	,
AF	AF	kA	AF
167	[number]	k4	167
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Currents	Currentsa	k1gFnPc2	Currentsa
of	of	k?	of
Space	Space	k1gMnSc1	Space
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
David	David	k1gMnSc1	David
Starr	Starr	k1gMnSc1	Starr
-	-	kIx~	-
tulák	tulák	k1gMnSc1	tulák
po	po	k7c6	po
hvězdách	hvězda	k1gFnPc6	hvězda
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Starr	Starr	k1gMnSc1	Starr
<g/>
,	,	kIx,	,
Space	Space	k1gMnSc1	Space
Ranger	Ranger	k1gMnSc1	Ranger
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
Lucky	lucky	k6eAd1	lucky
Starr	Starr	k1gMnSc1	Starr
&	&	k?	&
piráti	pirát	k1gMnPc1	pirát
z	z	k7c2	z
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
Lucky	lucky	k6eAd1	lucky
Starr	Starr	k1gMnSc1	Starr
and	and	k?	and
the	the	k?	the
Pirates	Pirates	k1gInSc1	Pirates
of	of	k?	of
the	the	k?	the
Asteroids	Asteroidsa	k1gFnPc2	Asteroidsa
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
Lucky	lucky	k6eAd1	lucky
Starr	Starr	k1gMnSc1	Starr
&	&	k?	&
oceány	oceán	k1gInPc1	oceán
Venuše	Venuše	k1gFnSc2	Venuše
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
Lucky	lucky	k6eAd1	lucky
Starr	Starr	k1gMnSc1	Starr
and	and	k?	and
the	the	k?	the
Oceans	Oceans	k1gInSc1	Oceans
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Lucky	lucky	k6eAd1	lucky
Starr	Starr	k1gMnSc1	Starr
&	&	k?	&
velké	velký	k2eAgNnSc1d1	velké
slunce	slunce	k1gNnSc1	slunce
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
<g />
.	.	kIx.	.
</s>
<s>
Železný	Železný	k1gMnSc1	Železný
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
Lucky	lucky	k6eAd1	lucky
Starr	Starr	k1gMnSc1	Starr
and	and	k?	and
the	the	k?	the
Big	Big	k1gFnSc2	Big
Sun	Sun	kA	Sun
of	of	k?	of
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
Lucky	lucky	k6eAd1	lucky
Starr	Starra	k1gFnPc2	Starra
a	a	k8xC	a
měsíce	měsíc	k1gInSc2	měsíc
Jupiterovy	Jupiterův	k2eAgFnSc2d1	Jupiterova
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
Lucky	lucky	k6eAd1	lucky
Starr	Starr	k1gMnSc1	Starr
and	and	k?	and
the	the	k?	the
Moons	Moonsa	k1gFnPc2	Moonsa
of	of	k?	of
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Lucky	lucky	k6eAd1	lucky
Starr	Starr	k1gInSc1	Starr
&	&	k?	&
prstence	prstenec	k1gInSc2	prstenec
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
<g />
.	.	kIx.	.
</s>
<s>
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
Lucky	lucky	k6eAd1	lucky
Starr	Starr	k1gMnSc1	Starr
and	and	k?	and
the	the	k?	the
Rings	Rings	k1gInSc1	Rings
of	of	k?	of
Saturn	Saturn	k1gInSc1	Saturn
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Ani	ani	k8xC	ani
sami	sám	k3xTgMnPc1	sám
bohové	bůh	k1gMnPc1	bůh
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Svoboda	Svoboda	k1gMnSc1	Svoboda
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
The	The	k1gFnSc6	The
Gods	Godsa	k1gFnPc2	Godsa
Themselves	Themselvesa	k1gFnPc2	Themselvesa
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Fantastická	fantastický	k2eAgFnSc1d1	fantastická
cesta	cesta	k1gFnSc1	cesta
II	II	kA	II
<g/>
,	,	kIx,	,
Místo	místo	k7c2	místo
určení	určení	k1gNnSc2	určení
<g/>
:	:	kIx,	:
Mozek	mozek	k1gInSc1	mozek
<g/>
,	,	kIx,	,
Slan	slano	k1gNnPc2	slano
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
Fantastic	Fantastice	k1gFnPc2	Fantastice
Voyage	Voyag	k1gInSc2	Voyag
II	II	kA	II
<g/>
,	,	kIx,	,
Destination	Destination	k1gInSc4	Destination
Brain	Braina	k1gFnPc2	Braina
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Příchod	příchod	k1gInSc1	příchod
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
Laser	laser	k1gInSc1	laser
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
Nightfall	Nightfall	k1gMnSc1	Nightfall
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Silverbergem	Silverberg	k1gMnSc7	Silverberg
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
sluncí	slunce	k1gNnPc2	slunce
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
stále	stále	k6eAd1	stále
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
zde	zde	k6eAd1	zde
žijící	žijící	k2eAgMnPc1d1	žijící
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
tmy	tma	k1gFnPc1	tma
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
2049	[number]	k4	2049
let	léto	k1gNnPc2	léto
však	však	k9	však
nastává	nastávat	k5eAaImIp3nS	nastávat
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
svítí	svítit	k5eAaImIp3nS	svítit
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zatmění	zatmění	k1gNnSc3	zatmění
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
okamžik	okamžik	k1gInSc4	okamžik
zpanikaří	zpanikařit	k5eAaPmIp3nS	zpanikařit
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc4	všechen
zapálí	zapálit	k5eAaPmIp3nP	zapálit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
rozvrácení	rozvrácení	k1gNnSc3	rozvrácení
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
zešílí	zešílet	k5eAaPmIp3nS	zešílet
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
civilizaci	civilizace	k1gFnSc4	civilizace
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
lidské	lidský	k2eAgFnSc2d1	lidská
civilizace	civilizace	k1gFnSc2	civilizace
v	v	k7c6	v
době	doba	k1gFnSc6	doba
napsání	napsání	k1gNnSc2	napsání
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
polemizují	polemizovat	k5eAaImIp3nP	polemizovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
přijde	přijít	k5eAaPmIp3nS	přijít
či	či	k8xC	či
nepřijde	přijít	k5eNaPmIp3nS	přijít
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
příchodu	příchod	k1gInSc6	příchod
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
požárům	požár	k1gInPc3	požár
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
nápravě	náprava	k1gFnSc3	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
věčnosti	věčnost	k1gFnSc2	věčnost
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
End	End	k1gFnPc2	End
of	of	k?	of
Eternity	eternit	k1gInPc1	eternit
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
Dítě	Dítě	k2eAgInSc2d1	Dítě
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
And	Anda	k1gFnPc2	Anda
Classic	Classice	k1gInPc2	Classice
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
The	The	k1gFnSc2	The
Ugly	Ugla	k1gFnSc2	Ugla
Little	Little	k1gFnSc2	Little
Boy	boa	k1gFnSc2	boa
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
-	-	kIx~	-
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Silverbergem	Silverberg	k1gMnSc7	Silverberg
Pozitronový	pozitronový	k2eAgMnSc1d1	pozitronový
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
Baronet	baronet	k1gMnSc1	baronet
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Positronic	Positronice	k1gFnPc2	Positronice
Man	Man	k1gMnSc1	Man
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
-	-	kIx~	-
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Silverbergem	Silverberg	k1gMnSc7	Silverberg
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
robot	robot	k1gMnSc1	robot
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
1981	[number]	k4	1981
(	(	kIx(	(
<g/>
I	i	k9	i
<g/>
,	,	kIx,	,
Robot	robot	k1gMnSc1	robot
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Vize	vize	k1gFnSc1	vize
robotů	robot	k1gInPc2	robot
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
Klub	klub	k1gInSc1	klub
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
Robot	robot	k1gInSc1	robot
visions	visionsa	k1gFnPc2	visionsa
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Azazel	Azazel	k1gFnSc1	Azazel
<g/>
,	,	kIx,	,
Laser	laser	k1gInSc1	laser
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
Azazel	Azazel	k1gMnSc1	Azazel
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
fantasy	fantas	k1gInPc4	fantas
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
postava	postava	k1gFnSc1	postava
skřítka	skřítek	k1gMnSc2	skřítek
Azazela	Azazela	k1gMnSc2	Azazela
<g/>
.	.	kIx.	.
</s>
<s>
Sny	sen	k1gInPc1	sen
robotů	robot	k1gInPc2	robot
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
Klub	klub	k1gInSc1	klub
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
Robot	robot	k1gInSc1	robot
Dreams	Dreamsa	k1gFnPc2	Dreamsa
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Sbohem	sbohem	k0	sbohem
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Mustang	mustang	k1gMnSc1	mustang
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
Gold	Gold	k1gMnSc1	Gold
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Nesmrtelný	nesmrtelný	k1gMnSc1	nesmrtelný
Bard	bard	k1gMnSc1	bard
<g/>
,	,	kIx,	,
Polaris	Polaris	k1gFnSc1	Polaris
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
různé	různý	k2eAgInPc1d1	různý
originály	originál	k1gInPc1	originál
<g/>
)	)	kIx)	)
Neznámý	známý	k2eNgInSc1d1	neznámý
Asimov	Asimov	k1gInSc1	Asimov
<g/>
,	,	kIx,	,
Triton	triton	k1gInSc1	triton
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Early	earl	k1gMnPc7	earl
Asimov	Asimov	k1gInSc4	Asimov
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Neznámý	známý	k2eNgInSc1d1	neznámý
Asimov	Asimov	k1gInSc1	Asimov
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Robohistorie	Robohistorie	k1gFnSc1	Robohistorie
<g/>
,	,	kIx,	,
Triton	triton	k1gInSc1	triton
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
The	The	k1gFnSc3	The
Complete	Comple	k1gNnSc2	Comple
Robot	robota	k1gFnPc2	robota
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
The	The	k1gFnPc4	The
Rest	rest	k6eAd1	rest
of	of	k?	of
the	the	k?	the
Robots	Robotsa	k1gFnPc2	Robotsa
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
Slova	slovo	k1gNnSc2	slovo
vědy	věda	k1gFnSc2	věda
<g/>
:	:	kIx,	:
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
skrývá	skrývat	k5eAaImIp3nS	skrývat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Panorama	panorama	k1gNnSc1	panorama
1978	[number]	k4	1978
(	(	kIx(	(
<g/>
Words	Words	k1gInSc1	Words
of	of	k?	of
Science	Science	k1gFnSc1	Science
and	and	k?	and
History	Histor	k1gInPc1	Histor
behind	behinda	k1gFnPc2	behinda
them	thema	k1gFnPc2	thema
a	a	k8xC	a
More	mor	k1gInSc5	mor
Words	Words	k1gInSc1	Words
of	of	k?	of
Science	Science	k1gFnSc1	Science
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Asimov	Asimov	k1gInSc1	Asimov
<g/>
:	:	kIx,	:
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
Český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
I	i	k9	i
<g/>
,	,	kIx,	,
Asimov	Asimov	k1gInSc1	Asimov
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
třetí	třetí	k4xOgFnSc1	třetí
autobiografie	autobiografie	k1gFnSc1	autobiografie
Atom	atom	k1gInSc1	atom
<g/>
:	:	kIx,	:
cesta	cesta	k1gFnSc1	cesta
subatomárním	subatomárnit	k5eAaPmIp1nS	subatomárnit
vesmírem	vesmír	k1gInSc7	vesmír
<g/>
,	,	kIx,	,
Jota	jota	k1gNnSc7	jota
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
Inside	Insid	k1gInSc5	Insid
the	the	k?	the
Atom	atom	k1gInSc1	atom
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
</s>
