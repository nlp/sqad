<s>
Los	los	k1gInSc1
Angeles	Angelesa	k1gFnPc2
Police	police	k1gFnSc2
Department	department	k1gInSc1
</s>
<s>
Los	los	k1gInSc1
Angeles	Angelesa	k1gFnPc2
Police	police	k1gFnSc2
Department	department	k1gInSc1
Motto	motto	k1gNnSc5
</s>
<s>
To	to	k9
protect	protect	k1gMnSc1
and	and	k?
to	ten	k3xDgNnSc1
serve	servat	k5eAaPmIp3nS
Vznik	vznik	k1gInSc1
</s>
<s>
1869	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
100	#num#	k4
West	West	k1gInSc1
1	#num#	k4
<g/>
st	st	kA
Street	Streeta	k1gFnPc2
</s>
<s>
Los	los	k1gInSc1
Angeles	Angelesa	k1gFnPc2
<g/>
,	,	kIx,
California	Californium	k1gNnSc2
<g/>
,	,	kIx,
U.	U.	kA
<g/>
S.	S.	kA
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
34	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
8,66	8,66	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
118	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
40,06	40,06	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
1	#num#	k4
733	#num#	k4
838	#num#	k4
124	#num#	k4
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
13	#num#	k4
010	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.lapdonline.org	www.lapdonline.org	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Los	los	k1gInSc1
Angeles	Angeles	k1gMnSc1
Police	police	k1gFnSc2
Department	department	k1gInSc1
(	(	kIx(
<g/>
LAPD	LAPD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
City	city	k1gNnSc1
of	of	k?
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Police	police	k1gFnSc2
Department	department	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
policejní	policejní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
10	#num#	k4
tisíci	tisíc	k4xCgInPc7
příslušníky	příslušník	k1gMnPc7
a	a	k8xC
třemi	tři	k4xCgMnPc7
tisíci	tisíc	k4xCgInPc7
civilními	civilní	k2eAgMnPc7d1
zaměstnanci	zaměstnanec	k1gMnPc7
je	být	k5eAaImIp3nS
třetí	třetí	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
po	po	k7c6
newyorské	newyorský	k2eAgFnSc6d1
a	a	k8xC
chicagské	chicagský	k2eAgFnSc6d1
policii	policie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útvar	útvar	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
roku	rok	k1gInSc2
1869	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
policejním	policejní	k2eAgMnSc7d1
oddělením	oddělení	k1gNnSc7
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
zřídil	zřídit	k5eAaPmAgInS
jednotku	jednotka	k1gFnSc4
SWAT	SWAT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c4
jurisdikci	jurisdikce	k1gFnSc4
LAPD	LAPD	kA
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
území	území	k1gNnSc2
města	město	k1gNnSc2
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
o	o	k7c6
rozloze	rozloha	k1gFnSc6
přibližně	přibližně	k6eAd1
1300	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
žijí	žít	k5eAaImIp3nP
zhruba	zhruba	k6eAd1
4	#num#	k4
milióny	milión	k4xCgInPc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Specifikace	specifikace	k1gFnSc1
</s>
<s>
Policisté	policista	k1gMnPc1
nosí	nosit	k5eAaImIp3nP
černou	černý	k2eAgFnSc4d1
uniformu	uniforma	k1gFnSc4
<g/>
,	,	kIx,
opatřenou	opatřený	k2eAgFnSc4d1
odznakem	odznak	k1gInSc7
na	na	k7c6
levé	levý	k2eAgFnSc6d1
náprsní	náprsní	k2eAgFnSc6d1
kapse	kapsa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
odznaku	odznak	k1gInSc6
je	být	k5eAaImIp3nS
umístěn	umístěn	k2eAgInSc1d1
nápis	nápis	k1gInSc1
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
Police	police	k1gFnSc2
a	a	k8xC
specifikace	specifikace	k1gFnSc2
útvaru	útvar	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
speciální	speciální	k2eAgInSc4d1
útvar	útvar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
řadových	řadový	k2eAgMnPc2d1
policistů	policista	k1gMnPc2
zde	zde	k6eAd1
stojí	stát	k5eAaImIp3nS
Police	police	k1gFnSc1
officer	officra	k1gFnPc2
-	-	kIx~
tedy	tedy	k9
policejní	policejní	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
odznaku	odznak	k1gInSc6
je	být	k5eAaImIp3nS
také	také	k9
umístěn	umístěn	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
:	:	kIx,
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
Founded	Founded	k1gInSc1
1781	#num#	k4
-	-	kIx~
Los	los	k1gInSc1
Angeles	Angelesa	k1gFnPc2
-	-	kIx~
založeno	založit	k5eAaPmNgNnS
1781	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xS,k8xC
vozidla	vozidlo	k1gNnPc4
používá	používat	k5eAaImIp3nS
útvar	útvar	k1gInSc1
automobily	automobil	k1gInPc7
Ford	ford	k1gInSc1
<g/>
,	,	kIx,
Chevrolet	chevrolet	k1gInSc1
nebo	nebo	k8xC
Dodge	Dodge	k1gInSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
motocykly	motocykl	k1gInPc1
zn.	zn.	kA
Harley-Davidson	Harley-Davidson	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Výzbroj	výzbroj	k1gFnSc4
tvořily	tvořit	k5eAaImAgFnP
samonabíjecí	samonabíjecí	k2eAgFnPc1d1
pistole	pistol	k1gFnPc1
Beretta	Berett	k1gInSc2
M	M	kA
<g/>
92	#num#	k4
<g/>
,	,	kIx,
avšak	avšak	k8xC
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
procesu	proces	k1gInSc2
přezbrojení	přezbrojení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brokovnice	brokovnice	k1gFnSc2
jsou	být	k5eAaImIp3nP
značky	značka	k1gFnPc1
Remington	remington	k1gInSc1
<g/>
,	,	kIx,
Mossberg	Mossberg	k1gInSc1
<g/>
,	,	kIx,
či	či	k8xC
Winchester	Winchester	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
používá	používat	k5eAaImIp3nS
zbraně	zbraň	k1gFnPc4
M	M	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
ráže	ráže	k1gFnSc1
5,56	5,56	k4
×	×	k?
45	#num#	k4
mm	mm	kA
NATO	NATO	kA
<g/>
,	,	kIx,
tak	tak	k9
ráže	ráže	k1gFnSc2
9	#num#	k4
mm	mm	kA
Luger	Lugra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dovybavení	dovybavení	k1gNnSc3
hlídkových	hlídkový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
zbraněmi	zbraň	k1gFnPc7
ráže	ráže	k1gFnSc2
5,56	5,56	k4
mm	mm	kA
došlo	dojít	k5eAaPmAgNnS
po	po	k7c6
situaci	situace	k1gFnSc6
s	s	k7c7
bankovní	bankovní	k2eAgFnSc7d1
loupeží	loupež	k1gFnSc7
v	v	k7c6
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
Nord	Norda	k1gFnPc2
Holywood	Holywooda	k1gFnPc2
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
lupiči	lupič	k1gMnPc1
vybaveni	vybavit	k5eAaPmNgMnP
i	i	k8xC
doma	doma	k6eAd1
vyrobenými	vyrobený	k2eAgFnPc7d1
balistickými	balistický	k2eAgFnPc7d1
vestami	vesta	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
zbraně	zbraň	k1gFnPc1
ráže	ráže	k1gFnSc1
9	#num#	k4
<g/>
mm	mm	kA
Luger	Lugra	k1gFnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
brokovnice	brokovnice	k1gFnPc1
nedokázaly	dokázat	k5eNaPmAgFnP
prorazit	prorazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Jednotka	jednotka	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
divize	divize	k1gFnPc4
<g/>
,	,	kIx,
důležitou	důležitý	k2eAgFnSc7d1
je	být	k5eAaImIp3nS
divize	divize	k1gFnSc1
Metro	metro	k1gNnSc4
(	(	kIx(
<g/>
metropolitní	metropolitní	k2eAgFnSc2d1
policie	policie	k1gFnSc2
<g/>
,	,	kIx,
centrální	centrální	k2eAgFnPc4d1
policejní	policejní	k2eAgNnPc4d1
oddělení	oddělení	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Divize	divize	k1gFnSc1
Metro	metro	k1gNnSc1
</s>
<s>
Divize	divize	k1gFnSc1
Metro	metro	k1gNnSc4
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
5	#num#	k4
čet	četa	k1gFnPc2
<g/>
(	(	kIx(
<g/>
Platoon	Platoon	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
MOS	MOS	kA
-	-	kIx~
Personální	personální	k2eAgMnSc1d1
a	a	k8xC
výcvikové	výcvikový	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Čety	četa	k1gFnPc1
B	B	kA
a	a	k8xC
C	C	kA
-	-	kIx~
Jsou	být	k5eAaImIp3nP
pověřené	pověřený	k2eAgFnPc1d1
bojem	boj	k1gInSc7
proti	proti	k7c3
kriminalitě	kriminalita	k1gFnSc3
<g/>
,	,	kIx,
</s>
<s>
Četa	četa	k1gFnSc1
D	D	kA
-	-	kIx~
<g/>
S.	S.	kA
<g/>
W.A.T.	W.A.T.	k1gMnSc1
</s>
<s>
Četa	četa	k1gFnSc1
E	E	kA
-	-	kIx~
Jízdní	jízdní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
Četa	četa	k1gFnSc1
K9	K9	k1gFnSc1
(	(	kIx(
<g/>
canine-pes	canine-pes	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
Četa	četa	k1gFnSc1
psovodů	psovod	k1gMnPc2
K9	K9	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
Police	police	k1gFnSc2
Department	department	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
2124535-6	2124535-6	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
9382	#num#	k4
2847	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50045520	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
137894799	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50045520	#num#	k4
</s>
