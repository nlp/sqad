<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
opera	opera	k1gFnSc1	opera
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
napsána	napsán	k2eAgFnSc1d1	napsána
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
německého	německý	k2eAgMnSc2d1	německý
básníka	básník	k1gMnSc2	básník
(	(	kIx(	(
<g/>
Karla	Karel	k1gMnSc2	Karel
<g/>
)	)	kIx)	)
Theodora	Theodor	k1gMnSc2	Theodor
Körnera	Körner	k1gMnSc2	Körner
<g/>
.	.	kIx.	.
</s>
<s>
Uvedena	uveden	k2eAgFnSc1d1	uvedena
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
a	a	k8xC	a
prozatím	prozatím	k6eAd1	prozatím
i	i	k9	i
naposledy	naposledy	k6eAd1	naposledy
<g/>
,	,	kIx,	,
až	až	k9	až
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
libretem	libreto	k1gNnSc7	libreto
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
historie	historie	k1gFnSc1	historie
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
letý	letý	k2eAgMnSc1d1	letý
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
ke	k	k7c3	k
kompozici	kompozice	k1gFnSc3	kompozice
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
opery	opera	k1gFnSc2	opera
s	s	k7c7	s
názvem	název	k1gInSc7	název
Alfred	Alfred	k1gMnSc1	Alfred
odhodlal	odhodlat	k5eAaPmAgMnS	odhodlat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
měl	mít	k5eAaImAgMnS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
nejen	nejen	k6eAd1	nejen
osmileté	osmiletý	k2eAgNnSc4d1	osmileté
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
orchestru	orchestr	k1gInSc6	orchestr
pražského	pražský	k2eAgNnSc2d1	Pražské
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
řadu	řada	k1gFnSc4	řada
kompozic	kompozice	k1gFnPc2	kompozice
včetně	včetně	k7c2	včetně
tak	tak	k6eAd1	tak
náročných	náročný	k2eAgFnPc2d1	náročná
forem	forma	k1gFnPc2	forma
jako	jako	k8xC	jako
symfonie	symfonie	k1gFnSc2	symfonie
a	a	k8xC	a
smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
kvartet	kvartet	k1gInSc4	kvartet
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
však	však	k9	však
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
nebyly	být	k5eNaImAgInP	být
provedeny	provést	k5eAaPmNgInP	provést
<g/>
)	)	kIx)	)
a	a	k8xC	a
včetně	včetně	k7c2	včetně
menších	malý	k2eAgFnPc2d2	menší
skladeb	skladba	k1gFnPc2	skladba
vokálních	vokální	k2eAgFnPc2d1	vokální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
příležitost	příležitost	k1gFnSc4	příležitost
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
německé	německý	k2eAgNnSc4d1	německé
libreto	libreto	k1gNnSc4	libreto
romantického	romantický	k2eAgMnSc2d1	romantický
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
hrdiny	hrdina	k1gMnSc2	hrdina
protinapoleonského	protinapoleonský	k2eAgInSc2d1	protinapoleonský
boje	boj	k1gInSc2	boj
Karla	Karel	k1gMnSc2	Karel
Theodora	Theodor	k1gMnSc2	Theodor
Körnera	Körner	k1gMnSc2	Körner
(	(	kIx(	(
<g/>
1791	[number]	k4	1791
<g/>
-	-	kIx~	-
<g/>
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
libreto	libreto	k1gNnSc1	libreto
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1811	[number]	k4	1811
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Alfred	Alfred	k1gMnSc1	Alfred
der	drát	k5eAaImRp2nS	drát
Große	Groß	k1gFnSc2	Groß
<g/>
,	,	kIx,	,
Alfréd	Alfréd	k1gMnSc1	Alfréd
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
námětem	námět	k1gInSc7	námět
je	být	k5eAaImIp3nS	být
vítězný	vítězný	k2eAgInSc4d1	vítězný
odpor	odpor	k1gInSc4	odpor
britských	britský	k2eAgMnPc2d1	britský
Sasů	Sas	k1gMnPc2	Sas
vedených	vedený	k2eAgFnPc2d1	vedená
králem	král	k1gMnSc7	král
Alfrédem	Alfréd	k1gMnSc7	Alfréd
Velikým	veliký	k2eAgMnSc7d1	veliký
proti	proti	k7c3	proti
dánským	dánský	k2eAgMnPc3d1	dánský
Vikingům	Viking	k1gMnPc3	Viking
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
využito	využít	k5eAaPmNgNnS	využít
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
tak	tak	k9	tak
učinil	učinit	k5eAaPmAgInS	učinit
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
Johann	Johann	k1gMnSc1	Johann
Philipp	Philipp	k1gMnSc1	Philipp
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
je	být	k5eAaImIp3nS	být
zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
Josef	Josef	k1gMnSc1	Josef
Joachim	Joachim	k1gMnSc1	Joachim
Raff	Raff	k1gMnSc1	Raff
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
třetí	třetí	k4xOgFnSc7	třetí
operu	oprat	k5eAaPmIp1nS	oprat
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1833	[number]	k4	1833
a	a	k8xC	a
1835	[number]	k4	1835
napsal	napsat	k5eAaBmAgMnS	napsat
známější	známý	k2eAgMnSc1d2	známější
skladatel	skladatel	k1gMnSc1	skladatel
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc4	von
Flotow	Flotow	k1gFnSc2	Flotow
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ta	ten	k3xDgFnSc1	ten
nebyla	být	k5eNaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dvořák	Dvořák	k1gMnSc1	Dvořák
žádné	žádný	k3yNgNnSc4	žádný
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
zpracování	zpracování	k1gNnPc2	zpracování
neznal	neznat	k5eAaImAgMnS	neznat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
dílo	dílo	k1gNnSc4	dílo
s	s	k7c7	s
osvědčenou	osvědčený	k2eAgFnSc7d1	osvědčená
literární	literární	k2eAgFnSc7d1	literární
kvalitou	kvalita	k1gFnSc7	kvalita
a	a	k8xC	a
současně	současně	k6eAd1	současně
autorsky	autorsky	k6eAd1	autorsky
volné	volný	k2eAgFnPc4d1	volná
(	(	kIx(	(
<g/>
kvalitní	kvalitní	k2eAgFnPc4d1	kvalitní
česká	český	k2eAgNnPc4d1	české
libreta	libreto	k1gNnPc4	libreto
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
prakticky	prakticky	k6eAd1	prakticky
neexistovala	existovat	k5eNaImAgFnS	existovat
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
by	by	kYmCp3nS	by
Dvořák	Dvořák	k1gMnSc1	Dvořák
nové	nový	k2eAgNnSc4d1	nové
libreto	libreto	k1gNnSc4	libreto
musel	muset	k5eAaImAgMnS	muset
zakoupit	zakoupit	k5eAaPmF	zakoupit
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
neměl	mít	k5eNaImAgMnS	mít
prostředky	prostředek	k1gInPc4	prostředek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
Körnerova	Körnerův	k2eAgFnSc1d1	Körnerova
paralela	paralela	k1gFnSc1	paralela
s	s	k7c7	s
bojem	boj	k1gInSc7	boj
Němců	Němec	k1gMnPc2	Němec
proti	proti	k7c3	proti
francouzské	francouzský	k2eAgFnSc3d1	francouzská
okupaci	okupace	k1gFnSc3	okupace
již	již	k6eAd1	již
vyvanula	vyvanout	k5eAaPmAgFnS	vyvanout
<g/>
,	,	kIx,	,
pololegendární	pololegendárnit	k5eAaPmIp3nS	pololegendárnit
hrdinský	hrdinský	k2eAgInSc4d1	hrdinský
zápas	zápas	k1gInSc4	zápas
mezi	mezi	k7c7	mezi
pohanskými	pohanský	k2eAgMnPc7d1	pohanský
Dány	Dán	k1gMnPc7	Dán
a	a	k8xC	a
křesťanskými	křesťanský	k2eAgMnPc7d1	křesťanský
Sasy	Sas	k1gMnPc7	Sas
však	však	k8xC	však
rezonoval	rezonovat	k5eAaImAgMnS	rezonovat
s	s	k7c7	s
náměty	námět	k1gInPc7	námět
děl	dělo	k1gNnPc2	dělo
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Dvořákovi	Dvořák	k1gMnSc3	Dvořák
hlavním	hlavní	k2eAgInSc7d1	hlavní
vzorem	vzor	k1gInSc7	vzor
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
původní	původní	k2eAgFnSc1d1	původní
podoba	podoba	k1gFnSc1	podoba
libreta	libreto	k1gNnSc2	libreto
s	s	k7c7	s
přísným	přísný	k2eAgNnSc7d1	přísné
rozdělením	rozdělení	k1gNnSc7	rozdělení
na	na	k7c4	na
recitativy	recitativ	k1gInPc4	recitativ
a	a	k8xC	a
tradiční	tradiční	k2eAgFnPc4d1	tradiční
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
formy	forma	k1gFnPc4	forma
(	(	kIx(	(
<g/>
árie	árie	k1gFnPc4	árie
<g/>
,	,	kIx,	,
ansámbly	ansámbl	k1gInPc4	ansámbl
<g/>
,	,	kIx,	,
sbory	sbor	k1gInPc4	sbor
<g/>
,	,	kIx,	,
tance	tanec	k1gInPc4	tanec
<g/>
)	)	kIx)	)
vyhovovala	vyhovovat	k5eAaImAgFnS	vyhovovat
Wagnerově	Wagnerův	k2eAgFnSc3d1	Wagnerova
koncepci	koncepce	k1gFnSc3	koncepce
"	"	kIx"	"
<g/>
hudebního	hudební	k2eAgNnSc2d1	hudební
dramatu	drama	k1gNnSc2	drama
<g/>
"	"	kIx"	"
jen	jen	k9	jen
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
určitého	určitý	k2eAgNnSc2d1	určité
znásilnění	znásilnění	k1gNnSc2	znásilnění
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
Dvořák	Dvořák	k1gMnSc1	Dvořák
primárně	primárně	k6eAd1	primárně
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
tuto	tento	k3xDgFnSc4	tento
operu	opera	k1gFnSc4	opera
pro	pro	k7c4	pro
německá	německý	k2eAgNnPc4d1	německé
jeviště	jeviště	k1gNnPc4	jeviště
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
nabídnout	nabídnout	k5eAaPmF	nabídnout
Alfreda	Alfred	k1gMnSc2	Alfred
Prozatímnímu	prozatímní	k2eAgNnSc3d1	prozatímní
divadlu	divadlo	k1gNnSc3	divadlo
po	po	k7c6	po
překladu	překlad	k1gInSc6	překlad
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
–	–	k?	–
takový	takový	k3xDgInSc4	takový
postup	postup	k1gInSc4	postup
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
existence	existence	k1gFnSc1	existence
stálého	stálý	k2eAgNnSc2d1	stálé
českého	český	k2eAgNnSc2d1	české
divadla	divadlo	k1gNnSc2	divadlo
neobvyklý	obvyklý	k2eNgInSc4d1	neobvyklý
–	–	k?	–
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
libretu	libreto	k1gNnSc6	libreto
učinil	učinit	k5eAaImAgMnS	učinit
skladatel	skladatel	k1gMnSc1	skladatel
jen	jen	k9	jen
drobné	drobný	k2eAgFnPc4d1	drobná
úpravy	úprava	k1gFnPc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Nejzávažnější	závažný	k2eAgNnSc1d3	nejzávažnější
je	být	k5eAaImIp3nS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
prvního	první	k4xOgNnSc2	první
dějství	dějství	k1gNnSc2	dějství
předlohy	předloha	k1gFnSc2	předloha
–	–	k?	–
které	který	k3yRgFnPc1	který
délkou	délka	k1gFnSc7	délka
textu	text	k1gInSc2	text
značně	značně	k6eAd1	značně
přesahovalo	přesahovat	k5eAaImAgNnS	přesahovat
druhé	druhý	k4xOgNnSc1	druhý
–	–	k?	–
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
obraz	obraz	k1gInSc1	obraz
(	(	kIx(	(
<g/>
V	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
Dánů	Dán	k1gMnPc2	Dán
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
oddělen	oddělit	k5eAaPmNgInS	oddělit
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
akt	akt	k1gInSc1	akt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
operu	opera	k1gFnSc4	opera
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
Josefa	Josef	k1gMnSc2	Josef
Jiránka	Jiránek	k1gMnSc2	Jiránek
předložil	předložit	k5eAaPmAgMnS	předložit
Dvořák	Dvořák	k1gMnSc1	Dvořák
Alfreda	Alfred	k1gMnSc2	Alfred
k	k	k7c3	k
posouzení	posouzení	k1gNnSc3	posouzení
Smetanovi	Smetana	k1gMnSc3	Smetana
<g/>
,	,	kIx,	,
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
šéfu	šéf	k1gMnSc3	šéf
opery	opera	k1gFnSc2	opera
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
úsudek	úsudek	k1gInSc4	úsudek
však	však	k9	však
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
Dvořák	Dvořák	k1gMnSc1	Dvořák
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
o	o	k7c6	o
uvedení	uvedení	k1gNnSc6	uvedení
této	tento	k3xDgFnSc2	tento
své	svůj	k3xOyFgFnSc2	svůj
operní	operní	k2eAgFnSc2d1	operní
prvotiny	prvotina	k1gFnSc2	prvotina
neusiloval	usilovat	k5eNaImAgInS	usilovat
a	a	k8xC	a
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
–	–	k?	–
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dalších	další	k2eAgFnPc2d1	další
skladeb	skladba	k1gFnPc2	skladba
včetně	včetně	k7c2	včetně
následující	následující	k2eAgFnSc2d1	následující
opery	opera	k1gFnSc2	opera
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
uhlíř	uhlíř	k1gMnSc1	uhlíř
–	–	k?	–
později	pozdě	k6eAd2	pozdě
nevracel	vracet	k5eNaImAgMnS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
<g/>
,	,	kIx,	,
našla	najít	k5eAaPmAgFnS	najít
se	se	k3xPyFc4	se
až	až	k9	až
ve	v	k7c6	v
Dvořákově	Dvořákův	k2eAgFnSc6d1	Dvořákova
pozůstalosti	pozůstalost	k1gFnSc6	pozůstalost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
je	být	k5eAaImIp3nS	být
Dvořákovo	Dvořákův	k2eAgNnSc4d1	Dvořákovo
rané	raný	k2eAgNnSc4d1	rané
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
odráží	odrážet	k5eAaImIp3nS	odrážet
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
skladatelovo	skladatelův	k2eAgNnSc1d1	skladatelovo
zaujetí	zaujetí	k1gNnSc1	zaujetí
pro	pro	k7c4	pro
Wagnera	Wagner	k1gMnSc4	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
prvním	první	k4xOgNnSc6	první
uvedení	uvedení	k1gNnSc6	uvedení
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
za	za	k7c4	za
"	"	kIx"	"
<g/>
wagnerovštější	wagnerovský	k2eAgInSc4d2	wagnerovský
než	než	k8xS	než
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tedy	tedy	k9	tedy
četné	četný	k2eAgInPc4d1	četný
příznačné	příznačný	k2eAgInPc4d1	příznačný
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
melodii	melodie	k1gFnSc4	melodie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dominanci	dominance	k1gFnSc4	dominance
orchestru	orchestr	k1gInSc2	orchestr
<g/>
,	,	kIx,	,
hutnou	hutný	k2eAgFnSc4d1	hutná
instrumentaci	instrumentace	k1gFnSc4	instrumentace
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
opery	opera	k1gFnSc2	opera
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
etapy	etapa	k1gFnSc2	etapa
Wagnerovy	Wagnerův	k2eAgFnSc2d1	Wagnerova
tvorby	tvorba	k1gFnSc2	tvorba
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
též	též	k9	též
opery	opera	k1gFnSc2	opera
Meyerbeerovy	Meyerbeerův	k2eAgFnSc2d1	Meyerbeerův
nebo	nebo	k8xC	nebo
Verdiho	Verdi	k1gMnSc2	Verdi
<g/>
)	)	kIx)	)
řadu	řada	k1gFnSc4	řada
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
sborů	sbor	k1gInPc2	sbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
si	se	k3xPyFc3	se
zřejmě	zřejmě	k6eAd1	zřejmě
nejvíce	nejvíce	k6eAd1	nejvíce
cenil	cenit	k5eAaImAgMnS	cenit
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
orchestrální	orchestrální	k2eAgFnSc2d1	orchestrální
předehry	předehra	k1gFnSc2	předehra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hudebně-dramaticky	hudebněramaticky	k6eAd1	hudebně-dramaticky
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
děj	děj	k1gInSc1	děj
celého	celý	k2eAgNnSc2d1	celé
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Hodlal	hodlat	k5eAaImAgMnS	hodlat
ji	on	k3xPp3gFnSc4	on
uvést	uvést	k5eAaPmF	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
až	až	k9	až
po	po	k7c6	po
skladatelově	skladatelův	k2eAgFnSc6d1	skladatelova
smrti	smrt	k1gFnSc6	smrt
Českou	český	k2eAgFnSc7d1	Česká
filharmonií	filharmonie	k1gFnSc7	filharmonie
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1905	[number]	k4	1905
jako	jako	k8xS	jako
Tragická	tragický	k2eAgFnSc1d1	tragická
ouvertura	ouvertura	k1gFnSc1	ouvertura
a	a	k8xC	a
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
byla	být	k5eAaImAgFnS	být
i	i	k9	i
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Simrock	Simrocka	k1gFnPc2	Simrocka
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
též	též	k9	též
jako	jako	k9	jako
Dramatická	dramatický	k2eAgFnSc1d1	dramatická
ouvertura	ouvertura	k1gFnSc1	ouvertura
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
samotné	samotný	k2eAgFnSc2d1	samotná
opery	opera	k1gFnSc2	opera
bývá	bývat	k5eAaImIp3nS	bývat
hrána	hrát	k5eAaImNgFnS	hrát
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
jedinou	jediný	k2eAgFnSc7d1	jediná
částí	část	k1gFnSc7	část
díla	dílo	k1gNnSc2	dílo
známou	známá	k1gFnSc7	známá
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úryvky	úryvek	k1gInPc1	úryvek
z	z	k7c2	z
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
německém	německý	k2eAgNnSc6d1	německé
znění	znění	k1gNnSc6	znění
provedl	provést	k5eAaPmAgInS	provést
poprvé	poprvé	k6eAd1	poprvé
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
dirigenta	dirigent	k1gMnSc2	dirigent
Georga	Georg	k1gMnSc2	Georg
Singera	Singero	k1gNnSc2	Singero
zpívali	zpívat	k5eAaImAgMnP	zpívat
Walter	Walter	k1gMnSc1	Walter
Windholz	Windholz	k1gMnSc1	Windholz
(	(	kIx(	(
<g/>
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fine	Fin	k1gMnSc5	Fin
Reich-Dörichová	Reich-Dörichový	k2eAgNnPc1d1	Reich-Dörichový
(	(	kIx(	(
<g/>
Alwina	Alwino	k1gNnPc1	Alwino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Kubla	Kubla	k1gMnSc1	Kubla
(	(	kIx(	(
<g/>
Harald	Harald	k1gMnSc1	Harald
<g/>
)	)	kIx)	)
a	a	k8xC	a
Julius	Julius	k1gMnSc1	Julius
Gutman	Gutman	k1gMnSc1	Gutman
(	(	kIx(	(
<g/>
Sieward	Sieward	k1gMnSc1	Sieward
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Scénického	scénický	k2eAgNnSc2d1	scénické
uvedení	uvedení	k1gNnSc2	uvedení
se	se	k3xPyFc4	se
Alfred	Alfred	k1gMnSc1	Alfred
dočkal	dočkat	k5eAaPmAgMnS	dočkat
až	až	k9	až
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1938	[number]	k4	1938
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
souboru	soubor	k1gInSc2	soubor
Českégo	Českégo	k6eAd1	Českégo
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
70	[number]	k4	70
let	léto	k1gNnPc2	léto
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
Anny	Anna	k1gFnSc2	Anna
Richterové	Richterová	k1gFnSc2	Richterová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rovněž	rovněž	k9	rovněž
zpívala	zpívat	k5eAaImAgFnS	zpívat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
ženskou	ženský	k2eAgFnSc4d1	ženská
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nebyla	být	k5eNaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvedení	uvedení	k1gNnSc1	uvedení
opery	opera	k1gFnSc2	opera
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
premiéře	premiéra	k1gFnSc6	premiéra
s	s	k7c7	s
originálním	originální	k2eAgNnSc7d1	originální
libretem	libreto	k1gNnSc7	libreto
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
<g/>
Alfred	Alfred	k1gMnSc1	Alfred
nemá	mít	k5eNaImIp3nS	mít
opusové	opusový	k2eAgNnSc4d1	opusové
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Burghauserově	Burghauserův	k2eAgInSc6d1	Burghauserův
katalogu	katalog	k1gInSc6	katalog
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc1	označení
B	B	kA	B
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Předehra	předehra	k1gFnSc1	předehra
samostatně	samostatně	k6eAd1	samostatně
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc4	označení
B	B	kA	B
<g/>
16	[number]	k4	16
<g/>
a.	a.	k?	a.
</s>
</p>
<p>
<s>
==	==	k?	==
Osoby	osoba	k1gFnPc4	osoba
a	a	k8xC	a
první	první	k4xOgNnPc4	první
obsazení	obsazení	k1gNnPc4	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	dít	k5eAaImRp2nS	dít
opery	opera	k1gFnSc2	opera
==	==	k?	==
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
opery	opera	k1gFnSc2	opera
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
roku	rok	k1gInSc2	rok
878	[number]	k4	878
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Tábor	Tábor	k1gInSc1	Tábor
Dánů	Dán	k1gMnPc2	Dán
<g/>
)	)	kIx)	)
Dánští	dánský	k2eAgMnPc1d1	dánský
vojáci	voják	k1gMnPc1	voják
porazili	porazit	k5eAaPmAgMnP	porazit
a	a	k8xC	a
rozprášili	rozprášit	k5eAaPmAgMnP	rozprášit
vojsko	vojsko	k1gNnSc4	vojsko
britského	britský	k2eAgMnSc2d1	britský
krále	král	k1gMnSc2	král
Alfreda	Alfred	k1gMnSc2	Alfred
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
vesele	vesele	k6eAd1	vesele
popíjejí	popíjet	k5eAaImIp3nP	popíjet
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
svých	svůj	k3xOyFgFnPc2	svůj
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Auf	Auf	k1gFnSc2	Auf
<g/>
,	,	kIx,	,
tapfre	tapfr	k1gInSc5	tapfr
Gesellen	Gesellen	k1gInSc1	Gesellen
<g/>
,	,	kIx,	,
zum	zum	k?	zum
Feste	Feste	k?	Feste
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
...	...	k?	...
Einsam	Einsam	k1gInSc1	Einsam
unter	unter	k1gMnSc1	unter
fremdem	fremd	k1gMnSc7	fremd
Himmel	Himmel	k1gMnSc1	Himmel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgMnSc1d2	starší
dánský	dánský	k2eAgMnSc1d1	dánský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Gothron	Gothron	k1gMnSc1	Gothron
opodál	opodál	k6eAd1	opodál
se	se	k3xPyFc4	se
veselice	veselice	k1gFnSc1	veselice
neúčastní	účastnit	k5eNaImIp3nS	účastnit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
znepokojen	znepokojit	k5eAaPmNgMnS	znepokojit
snem	sen	k1gInSc7	sen
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zjevil	zjevit	k5eAaPmAgMnS	zjevit
protivník	protivník	k1gMnSc1	protivník
Dánů	Dán	k1gMnPc2	Dán
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
král	král	k1gMnSc1	král
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadpozemské	nadpozemský	k2eAgFnSc6d1	nadpozemská
záři	zář	k1gFnSc6	zář
(	(	kIx(	(
<g/>
Doch	doch	k1gInSc1	doch
stand	stand	k?	stand
es	es	k1gNnSc1	es
nicht	nicht	k1gMnSc1	nicht
mit	mit	k?	mit
voller	voller	k1gMnSc1	voller
Kraft	Kraft	k1gMnSc1	Kraft
des	des	k1gNnSc2	des
Lebens	Lebensa	k1gFnPc2	Lebensa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
triumfálního	triumfální	k2eAgInSc2d1	triumfální
pochodu	pochod	k1gInSc2	pochod
(	(	kIx(	(
<g/>
Hoch	hoch	k1gMnSc1	hoch
töne	tön	k1gFnSc2	tön
Trompetenschmetter	Trompetenschmetter	k1gMnSc1	Trompetenschmetter
<g/>
)	)	kIx)	)
přichází	přicházet	k5eAaImIp3nS	přicházet
mladší	mladý	k2eAgMnSc1d2	mladší
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Harald	Harald	k1gMnSc1	Harald
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dánských	dánský	k2eAgFnPc2d1	dánská
rukou	ruka	k1gFnPc2	ruka
padlo	padnout	k5eAaImAgNnS	padnout
mnoho	mnoho	k4c1	mnoho
zajatců	zajatec	k1gMnPc2	zajatec
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Weh	Weh	k1gFnSc2	Weh
<g/>
!	!	kIx.	!
</s>
<s>
Was	Was	k?	Was
haben	haben	k1gInSc1	haben
wir	wir	k?	wir
verbrochen	verbrochen	k1gInSc1	verbrochen
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gothron	Gothron	k1gInSc1	Gothron
po	po	k7c6	po
straně	strana	k1gFnSc6	strana
Haraldovi	Harald	k1gMnSc3	Harald
závidí	závidět	k5eAaImIp3nS	závidět
a	a	k8xC	a
protiví	protivit	k5eAaImIp3nS	protivit
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gNnSc1	jeho
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
<g/>
,	,	kIx,	,
veřejně	veřejně	k6eAd1	veřejně
jej	on	k3xPp3gMnSc4	on
varuje	varovat	k5eAaImIp3nS	varovat
před	před	k7c7	před
podceňováním	podceňování	k1gNnSc7	podceňování
poraženého	poražený	k1gMnSc2	poražený
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Harald	Harald	k1gMnSc1	Harald
považuje	považovat	k5eAaImIp3nS	považovat
tyto	tento	k3xDgFnPc4	tento
obavy	obava	k1gFnPc4	obava
za	za	k7c4	za
bezdůvodné	bezdůvodný	k2eAgNnSc4d1	bezdůvodné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Gothron	Gothron	k1gInSc1	Gothron
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
oddílem	oddíl	k1gInSc7	oddíl
hledat	hledat	k5eAaImF	hledat
Alfreda	Alfred	k1gMnSc4	Alfred
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Harald	Harald	k6eAd1	Harald
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
zpívají	zpívat	k5eAaImIp3nP	zpívat
píseň	píseň	k1gFnSc4	píseň
o	o	k7c4	o
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yQgInPc4	který
děkují	děkovat	k5eAaImIp3nP	děkovat
pohanským	pohanský	k2eAgMnPc3d1	pohanský
bohům	bůh	k1gMnPc3	bůh
(	(	kIx(	(
<g/>
Wir	Wir	k1gFnSc1	Wir
kämpfen	kämpfen	k1gInSc1	kämpfen
mit	mit	k?	mit
dem	dem	k?	dem
Schwert	Schwert	k1gInSc1	Schwert
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
zajatci	zajatec	k1gMnPc7	zajatec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
spanilá	spanilý	k2eAgFnSc1d1	spanilá
Alvina	Alvina	k1gFnSc1	Alvina
<g/>
.	.	kIx.	.
</s>
<s>
Harald	Harald	k6eAd1	Harald
jí	on	k3xPp3gFnSc3	on
nabízí	nabízet	k5eAaImIp3nS	nabízet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
královnou	královna	k1gFnSc7	královna
<g/>
,	,	kIx,	,
Alvina	Alvin	k2eAgFnSc1d1	Alvina
však	však	k9	však
rozhodně	rozhodně	k6eAd1	rozhodně
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
zaslíbena	zaslíben	k2eAgFnSc1d1	zaslíbena
Alfredovi	Alfred	k1gMnSc6	Alfred
(	(	kIx(	(
<g/>
árie	árie	k1gFnSc2	árie
Wagst	Wagst	k1gInSc1	Wagst
Du	Du	k?	Du
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
nach	nach	k1gInSc1	nach
mir	mir	k1gInSc1	mir
die	die	k?	die
Hände	Händ	k1gInSc5	Händ
auszustrecken	auszustreckna	k1gFnPc2	auszustreckna
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
...	...	k?	...
Es	es	k1gNnSc7	es
lebt	lebt	k1gMnSc1	lebt
noch	noch	k1gMnSc1	noch
ein	ein	k?	ein
gerechter	gerechter	k1gMnSc1	gerechter
Gott	Gott	k1gMnSc1	Gott
im	im	k?	im
Himmel	Himmel	k1gMnSc1	Himmel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozzuřený	rozzuřený	k2eAgMnSc1d1	rozzuřený
Harald	Harald	k1gMnSc1	Harald
ji	on	k3xPp3gFnSc4	on
dává	dávat	k5eAaImIp3nS	dávat
uvrhnout	uvrhnout	k5eAaPmF	uvrhnout
do	do	k7c2	do
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Pustá	pustý	k2eAgFnSc1d1	pustá
lesní	lesní	k2eAgFnSc1d1	lesní
krajina	krajina	k1gFnSc1	krajina
<g/>
)	)	kIx)	)
Král	Král	k1gMnSc1	Král
Alfred	Alfred	k1gMnSc1	Alfred
je	být	k5eAaImIp3nS	být
otřesen	otřást	k5eAaPmNgMnS	otřást
vítězstvím	vítězství	k1gNnSc7	vítězství
Dánů	Dán	k1gMnPc2	Dán
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
odhodlán	odhodlán	k2eAgMnSc1d1	odhodlán
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
(	(	kIx(	(
<g/>
Wohl	Wohl	k1gMnSc1	Wohl
euch	euch	k1gMnSc1	euch
<g/>
,	,	kIx,	,
ihr	ihr	k?	ihr
tapfren	tapfrna	k1gFnPc2	tapfrna
Streiter	Streiter	k1gMnSc1	Streiter
<g/>
...	...	k?	...
Wild	Wild	k1gMnSc1	Wild
braust	braust	k1gMnSc1	braust
der	drát	k5eAaImRp2nS	drát
Sturm	Sturm	k1gInSc1	Sturm
<g/>
,	,	kIx,	,
die	die	k?	die
Donner	Donner	k1gInSc1	Donner
brüllen	brüllen	k1gInSc1	brüllen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potkává	potkávat	k5eAaImIp3nS	potkávat
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
sluhou	sluha	k1gMnSc7	sluha
Sivardem	Sivard	k1gMnSc7	Sivard
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
podává	podávat	k5eAaImIp3nS	podávat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
zajetí	zajetí	k1gNnSc6	zajetí
Alviny	Alvina	k1gFnSc2	Alvina
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
ji	on	k3xPp3gFnSc4	on
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
zachránit	zachránit	k5eAaPmF	zachránit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
pronikne	proniknout	k5eAaPmIp3nS	proniknout
do	do	k7c2	do
dánského	dánský	k2eAgInSc2d1	dánský
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
za	za	k7c4	za
harfeníka	harfeník	k1gMnSc4	harfeník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Před	před	k7c7	před
hradem	hrad	k1gInSc7	hrad
Dánů	Dán	k1gMnPc2	Dán
<g/>
)	)	kIx)	)
Gothron	Gothron	k1gMnSc1	Gothron
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
vojáky	voják	k1gMnPc7	voják
dosud	dosud	k6eAd1	dosud
bezvýsledně	bezvýsledně	k6eAd1	bezvýsledně
hledá	hledat	k5eAaImIp3nS	hledat
Alfreda	Alfred	k1gMnSc4	Alfred
(	(	kIx(	(
<g/>
Noch	Noch	k1gMnSc1	Noch
fand	fand	k1gMnSc1	fand
ich	ich	k?	ich
keine	keinout	k5eAaPmIp3nS	keinout
Spur	Spur	k1gInSc1	Spur
des	des	k1gNnSc1	des
Britenkönigs	Britenkönigs	k1gInSc1	Britenkönigs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
odejdou	odejít	k5eAaPmIp3nP	odejít
<g/>
,	,	kIx,	,
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
Alfred	Alfred	k1gMnSc1	Alfred
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
Sivarda	Sivarda	k1gFnSc1	Sivarda
a	a	k8xC	a
slyší	slyšet	k5eAaImIp3nS	slyšet
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
Alvininu	Alvinin	k2eAgFnSc4d1	Alvinin
píseň	píseň	k1gFnSc4	píseň
(	(	kIx(	(
<g/>
In	In	k1gMnSc1	In
des	des	k1gNnSc2	des
Thurmes	Thurmes	k1gMnSc1	Thurmes
Nacht	Nacht	k1gMnSc1	Nacht
gefangen	gefangen	k1gInSc1	gefangen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
poznat	poznat	k5eAaPmF	poznat
a	a	k8xC	a
slibuje	slibovat	k5eAaImIp3nS	slibovat
jí	on	k3xPp3gFnSc3	on
záchranu	záchrana	k1gFnSc4	záchrana
(	(	kIx(	(
<g/>
duet	duet	k1gInSc1	duet
Nicht	Nicht	k1gMnSc1	Nicht
länger	länger	k1gMnSc1	länger
sollst	sollst	k1gMnSc1	sollst
Du	Du	k?	Du
trostlos	trostlos	k1gMnSc1	trostlos
weinen	weinen	k2eAgMnSc1d1	weinen
<g/>
...	...	k?	...
Gewiß	Gewiß	k1gMnSc1	Gewiß
<g/>
,	,	kIx,	,
gewiß	gewiß	k?	gewiß
<g/>
,	,	kIx,	,
Du	Du	k?	Du
wirst	wirst	k1gInSc1	wirst
mich	mich	k1gInSc1	mich
retten	retten	k2eAgInSc1d1	retten
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
podezřívavý	podezřívavý	k2eAgMnSc1d1	podezřívavý
Gothron	Gothron	k1gMnSc1	Gothron
<g/>
;	;	kIx,	;
harfeníka	harfeník	k1gMnSc4	harfeník
dá	dát	k5eAaPmIp3nS	dát
odvést	odvést	k5eAaPmF	odvést
k	k	k7c3	k
poveselení	poveselení	k1gNnSc3	poveselení
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
Sivarda	Sivarda	k1gFnSc1	Sivarda
ale	ale	k8xC	ale
dá	dát	k5eAaPmIp3nS	dát
zajmout	zajmout	k5eAaPmF	zajmout
a	a	k8xC	a
uvěznit	uvěznit	k5eAaPmF	uvěznit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Noc	noc	k1gFnSc1	noc
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
<g/>
)	)	kIx)	)
Harald	Harald	k1gMnSc1	Harald
vítá	vítat	k5eAaImIp3nS	vítat
navracejícího	navracející	k2eAgMnSc4d1	navracející
se	se	k3xPyFc4	se
Gothrona	Gothrona	k1gFnSc1	Gothrona
posměšně	posměšně	k6eAd1	posměšně
<g/>
.	.	kIx.	.
</s>
<s>
Dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Alvina	Alvina	k1gFnSc1	Alvina
<g/>
,	,	kIx,	,
nedočkavši	dočkat	k5eNaPmDgFnS	dočkat
se	se	k3xPyFc4	se
osvobození	osvobození	k1gNnSc4	osvobození
Alfredem	Alfred	k1gMnSc7	Alfred
<g/>
,	,	kIx,	,
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
vlastním	vlastní	k2eAgNnSc7d1	vlastní
přičiněním	přičinění	k1gNnSc7	přičinění
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Harald	Harald	k1gMnSc1	Harald
běsní	běsnit	k5eAaImIp3nS	běsnit
<g/>
,	,	kIx,	,
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
Gothron	Gothron	k1gInSc1	Gothron
harfeníka	harfeník	k1gMnSc2	harfeník
ke	k	k7c3	k
zpěvu	zpěv	k1gInSc3	zpěv
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
dánského	dánský	k2eAgNnSc2d1	dánské
vítězství	vítězství	k1gNnSc2	vítězství
(	(	kIx(	(
<g/>
romance	romance	k1gFnSc2	romance
Des	des	k1gNnSc1	des
langen	langen	k1gInSc1	langen
Kampfes	Kampfes	k1gMnSc1	Kampfes
müde	müde	k1gInSc1	müde
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
v	v	k7c4	v
oslavu	oslava	k1gFnSc4	oslava
britského	britský	k2eAgMnSc2d1	britský
krále	král	k1gMnSc2	král
a	a	k8xC	a
hrozbu	hrozba	k1gFnSc4	hrozba
Dánům	Dán	k1gMnPc3	Dán
<g/>
.	.	kIx.	.
</s>
<s>
Rozhořčení	rozhořčený	k2eAgMnPc1d1	rozhořčený
Dánové	Dán	k1gMnPc1	Dán
se	se	k3xPyFc4	se
na	na	k7c4	na
pěvce	pěvec	k1gMnSc4	pěvec
vrhají	vrhat	k5eAaImIp3nP	vrhat
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dává	dávat	k5eAaImIp3nS	dávat
poznat	poznat	k5eAaPmF	poznat
jako	jako	k9	jako
Alfred	Alfred	k1gMnSc1	Alfred
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
majestátu	majestát	k1gInSc6	majestát
<g/>
.	.	kIx.	.
</s>
<s>
Alvina	Alvina	k1gFnSc1	Alvina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
připlížila	připlížit	k5eAaPmAgFnS	připlížit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
využije	využít	k5eAaPmIp3nS	využít
tohoto	tento	k3xDgInSc2	tento
momentu	moment	k1gInSc2	moment
překvapení	překvapení	k1gNnSc2	překvapení
a	a	k8xC	a
zhasne	zhasnout	k5eAaPmIp3nS	zhasnout
jedinou	jediný	k2eAgFnSc4d1	jediná
pochodeň	pochodeň	k1gFnSc4	pochodeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nastalé	nastalý	k2eAgFnSc6d1	nastalá
tmě	tma	k1gFnSc6	tma
a	a	k8xC	a
zmatku	zmatek	k1gInSc2	zmatek
se	se	k3xPyFc4	se
oběma	dva	k4xCgMnPc7	dva
podaří	podařit	k5eAaPmIp3nS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
je	on	k3xPp3gInPc4	on
nejdříve	dříve	k6eAd3	dříve
pronásledují	pronásledovat	k5eAaImIp3nP	pronásledovat
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
ale	ale	k9	ale
vrací	vracet	k5eAaImIp3nS	vracet
s	s	k7c7	s
nepořízenou	nepořízená	k1gFnSc7	nepořízená
<g/>
.	.	kIx.	.
</s>
<s>
Gothron	Gothron	k1gInSc1	Gothron
se	se	k3xPyFc4	se
rozpomíná	rozpomínat	k5eAaImIp3nS	rozpomínat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
sen	sen	k1gInSc4	sen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Skalnaté	skalnatý	k2eAgNnSc4d1	skalnaté
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
)	)	kIx)	)
V	v	k7c6	v
lese	les	k1gInSc6	les
se	se	k3xPyFc4	se
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
zbylí	zbylý	k2eAgMnPc1d1	zbylý
Alfredovi	Alfredův	k2eAgMnPc1d1	Alfredův
vojáci	voják	k1gMnPc1	voják
vedení	vedení	k1gNnSc2	vedení
velitelem	velitel	k1gMnSc7	velitel
Dorsetem	Dorset	k1gMnSc7	Dorset
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeskyně	jeskyně	k1gFnSc2	jeskyně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Alvina	Alvin	k2eAgFnSc1d1	Alvina
a	a	k8xC	a
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
Alffred	Alffred	k1gInSc1	Alffred
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
živu	živ	k2eAgFnSc4d1	živa
a	a	k8xC	a
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Odešel	odejít	k5eAaPmAgMnS	odejít
hledat	hledat	k5eAaImF	hledat
posily	posila	k1gFnPc4	posila
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgInS	zanechat
ji	on	k3xPp3gFnSc4	on
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
Vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
proto	proto	k8xC	proto
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ke	k	k7c3	k
králi	král	k1gMnSc3	král
připojili	připojit	k5eAaPmAgMnP	připojit
(	(	kIx(	(
<g/>
Auch	Auch	k1gInSc1	Auch
mir	mir	k1gInSc1	mir
sollt	sollt	k1gInSc1	sollt
ihr	ihr	k?	ihr
im	im	k?	im
Kampfe	Kampf	k1gInSc5	Kampf
sehen	sehet	k5eAaImNgMnS	sehet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
s	s	k7c7	s
pokřikem	pokřik	k1gInSc7	pokřik
Alfred	Alfred	k1gMnSc1	Alfred
und	und	k?	und
Sieg	Sieg	k1gInSc1	Sieg
odcházejí	odcházet	k5eAaImIp3nP	odcházet
a	a	k8xC	a
nechávají	nechávat	k5eAaImIp3nP	nechávat
Alvinu	Alvina	k1gFnSc4	Alvina
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
<g/>
,	,	kIx,	,
na	na	k7c6	na
pospas	pospas	k1gInSc1	pospas
Haraldovi	Harald	k1gMnSc3	Harald
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
právě	právě	k6eAd1	právě
přichází	přicházet	k5eAaImIp3nS	přicházet
a	a	k8xC	a
opět	opět	k6eAd1	opět
ji	on	k3xPp3gFnSc4	on
bere	brát	k5eAaImIp3nS	brát
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Opakuje	opakovat	k5eAaImIp3nS	opakovat
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Harald	Harald	k1gMnSc1	Harald
Alvině	Alvina	k1gFnSc3	Alvina
nejprve	nejprve	k6eAd1	nejprve
koří	kořit	k5eAaImIp3nS	kořit
a	a	k8xC	a
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
odmítnutí	odmítnutí	k1gNnSc6	odmítnutí
se	se	k3xPyFc4	se
rozzuří	rozzuřit	k5eAaPmIp3nS	rozzuřit
a	a	k8xC	a
odvádí	odvádět	k5eAaImIp3nS	odvádět
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
uvěznění	uvěznění	k1gNnSc3	uvěznění
(	(	kIx(	(
<g/>
duet	duet	k1gInSc4	duet
Welch	Welcha	k1gFnPc2	Welcha
ein	ein	k?	ein
Erwachen	Erwachna	k1gFnPc2	Erwachna
<g/>
!	!	kIx.	!
</s>
<s>
Ich	Ich	k?	Ich
seh	seh	k?	seh
<g/>
'	'	kIx"	'
mit	mit	k?	mit
Grauen	Grauen	k1gInSc1	Grauen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Před	před	k7c7	před
jeskyní	jeskyně	k1gFnSc7	jeskyně
<g/>
)	)	kIx)	)
Dorsetovi	Dorsetův	k2eAgMnPc1d1	Dorsetův
vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
spojili	spojit	k5eAaPmAgMnP	spojit
s	s	k7c7	s
Alfredem	Alfred	k1gMnSc7	Alfred
a	a	k8xC	a
zdraví	zdraví	k1gNnSc1	zdraví
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
Heil	Heil	k1gInSc1	Heil
unserm	unserm	k1gInSc1	unserm
König	König	k1gInSc1	König
–	–	k?	–
Alffred	Alffred	k1gInSc1	Alffred
und	und	k?	und
Sieg	Sieg	k1gInSc1	Sieg
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
je	on	k3xPp3gInPc4	on
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
ke	k	k7c3	k
statečnosti	statečnost	k1gFnSc3	statečnost
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
společnou	společný	k2eAgFnSc4d1	společná
modlitbu	modlitba	k1gFnSc4	modlitba
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
(	(	kIx(	(
<g/>
Höre	Hör	k1gInSc2	Hör
unser	unser	k1gInSc1	unser
lautes	lautes	k1gMnSc1	lautes
Flehen	Flehen	k1gInSc1	Flehen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Starý	starý	k2eAgInSc1d1	starý
kastel	kastel	k1gInSc1	kastel
<g/>
)	)	kIx)	)
Zajatí	zajatý	k2eAgMnPc1d1	zajatý
Britové	Brit	k1gMnPc1	Brit
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
Dánů	Dán	k1gMnPc2	Dán
neztrácejí	ztrácet	k5eNaImIp3nP	ztrácet
naději	naděje	k1gFnSc4	naděje
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Wir	Wir	k1gFnPc2	Wir
verschmachten	verschmachten	k2eAgMnSc1d1	verschmachten
hier	hier	k1gMnSc1	hier
in	in	k?	in
Ketten	Kettno	k1gNnPc2	Kettno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Harald	Harald	k1gMnSc1	Harald
přivádí	přivádět	k5eAaImIp3nS	přivádět
Alvinu	Alvina	k1gFnSc4	Alvina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stále	stále	k6eAd1	stále
vzpouzí	vzpouzet	k5eAaImIp3nS	vzpouzet
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
povzbuzuje	povzbuzovat	k5eAaImIp3nS	povzbuzovat
zajatce	zajatec	k1gMnPc4	zajatec
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
osvobození	osvobození	k1gNnSc1	osvobození
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
skutečně	skutečně	k6eAd1	skutečně
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
hluk	hluk	k1gInSc1	hluk
britského	britský	k2eAgNnSc2d1	Britské
vojska	vojsko	k1gNnSc2	vojsko
(	(	kIx(	(
<g/>
Welch	Welch	k1gMnSc1	Welch
ein	ein	k?	ein
Ton	Ton	k1gMnSc1	Ton
<g/>
?	?	kIx.	?
</s>
<s>
Was	Was	k?	Was
mag	mag	k?	mag
er	er	k?	er
bedeuten	bedeutno	k1gNnPc2	bedeutno
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
Alfred	Alfred	k1gMnSc1	Alfred
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
dorazí	dorazit	k5eAaPmIp3nS	dorazit
i	i	k9	i
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Gothron	Gothron	k1gMnSc1	Gothron
již	již	k6eAd1	již
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
Haraldovi	Haraldův	k2eAgMnPc1d1	Haraldův
Alfred	Alfred	k1gMnSc1	Alfred
nabízí	nabízet	k5eAaImIp3nS	nabízet
volný	volný	k2eAgInSc4d1	volný
odchod	odchod	k1gInSc4	odchod
do	do	k7c2	do
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
volí	volit	k5eAaImIp3nS	volit
raději	rád	k6eAd2	rád
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
a	a	k8xC	a
Alvina	Alvina	k1gMnSc1	Alvina
jsou	být	k5eAaImIp3nP	být
konečně	konečně	k6eAd1	konečně
spolu	spolu	k6eAd1	spolu
(	(	kIx(	(
<g/>
duet	duet	k1gInSc1	duet
O	o	k7c4	o
Glück	Glück	k1gInSc4	Glück
der	drát	k5eAaImRp2nS	drát
Liebe	Lieb	k1gMnSc5	Lieb
<g/>
,	,	kIx,	,
Götterlust	Götterlust	k1gFnSc4	Götterlust
<g/>
)	)	kIx)	)
a	a	k8xC	a
Britové	Brit	k1gMnPc1	Brit
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
vítězného	vítězný	k2eAgMnSc4d1	vítězný
panovníka	panovník	k1gMnSc4	panovník
(	(	kIx(	(
<g/>
Heil	Heil	k1gMnSc1	Heil
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
,	,	kIx,	,
heil	heil	k1gMnSc1	heil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nahrávka	nahrávka	k1gFnSc1	nahrávka
==	==	k?	==
</s>
</p>
<p>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
celé	celý	k2eAgFnSc2d1	celá
opery	opera	k1gFnSc2	opera
dosud	dosud	k6eAd1	dosud
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Komerčně	komerčně	k6eAd1	komerčně
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
nahrávka	nahrávka	k1gFnSc1	nahrávka
předehry	předehra	k1gFnSc2	předehra
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Dramatic	Dramatice	k1gFnPc2	Dramatice
(	(	kIx(	(
<g/>
Tragic	Tragice	k1gFnPc2	Tragice
<g/>
)	)	kIx)	)
Overture	Overtur	k1gMnSc5	Overtur
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
posth	posth	k1gMnSc1	posth
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
filharmonii	filharmonie	k1gFnSc4	filharmonie
diriguje	dirigovat	k5eAaImIp3nS	dirigovat
Libor	Libor	k1gMnSc1	Libor
Pešek	Pešek	k1gMnSc1	Pešek
<g/>
,	,	kIx,	,
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
(	(	kIx(	(
<g/>
Naxos	Naxos	k1gInSc1	Naxos
<g/>
)	)	kIx)	)
8.220	[number]	k4	8.220
<g/>
420	[number]	k4	420
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŠÍP	Šíp	k1gMnSc1	Šíp
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
opera	opera	k1gFnSc1	opera
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
tvůrci	tvůrce	k1gMnPc1	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
n.	n.	k?	n.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
400	[number]	k4	400
s.	s.	k?	s.
Kapitola	kapitola	k1gFnSc1	kapitola
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
s.	s.	k?	s.
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠOUREK	Šourek	k1gMnSc1	Šourek
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
HUTTER	HUTTER	kA	HUTTER
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
CHALABALA	CHALABALA	kA	CHALABALA
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
umění	umění	k1gNnSc1	umění
dramatické	dramatický	k2eAgFnSc2d1	dramatická
II	II	kA	II
-	-	kIx~	-
Zpěvohra	zpěvohra	k1gFnSc1	zpěvohra
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Šolc	Šolc	k1gMnSc1	Šolc
a	a	k8xC	a
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
101	[number]	k4	101
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WAGNER	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
Heinz	Heinz	k1gMnSc1	Heinz
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
grosse	grosse	k1gFnSc1	grosse
Handbuch	Handbuch	k1gInSc1	Handbuch
der	drát	k5eAaImRp2nS	drát
Oper	opera	k1gFnPc2	opera
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Hamburg	Hamburg	k1gInSc1	Hamburg
<g/>
:	:	kIx,	:
Nikol	nikol	k1gInSc1	nikol
Verlagsgesellschaft	Verlagsgesellschaft	k1gInSc1	Verlagsgesellschaft
mbH	mbH	k?	mbH
&	&	k?	&
Co	co	k9	co
<g/>
.	.	kIx.	.
</s>
<s>
KG	kg	kA	kg
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
1470	[number]	k4	1470
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
937872	[number]	k4	937872
<g/>
-	-	kIx~	-
<g/>
38	[number]	k4	38
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
-	-	kIx~	-
Alfred	Alfred	k1gMnSc1	Alfred
der	drát	k5eAaImRp2nS	drát
Große	Großus	k1gMnSc5	Großus
<g/>
,	,	kIx,	,
s.	s.	k?	s.
328	[number]	k4	328
<g/>
-	-	kIx~	-
<g/>
329	[number]	k4	329
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Údaje	údaj	k1gInPc1	údaj
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zčásti	zčásti	k6eAd1	zčásti
zavádějící	zavádějící	k2eAgFnSc1d1	zavádějící
a	a	k8xC	a
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
k	k	k7c3	k
jinému	jiný	k2eAgNnSc3d1	jiné
zhudebnění	zhudebnění	k1gNnSc3	zhudebnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAMINSKI	KAMINSKI	kA	KAMINSKI
<g/>
,	,	kIx,	,
Piotr	Piotr	k1gInSc1	Piotr
<g/>
.	.	kIx.	.
</s>
<s>
Mille	Mille	k1gFnSc1	Mille
et	et	k?	et
un	un	k?	un
opéras	opéras	k1gInSc1	opéras
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Librairie	Librairie	k1gFnSc1	Librairie
Arthè	Arthè	k1gMnSc1	Arthè
Fayard	Fayard	k1gMnSc1	Fayard
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
1819	[number]	k4	1819
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
213	[number]	k4	213
<g/>
-	-	kIx~	-
<g/>
60017	[number]	k4	60017
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
-	-	kIx~	-
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
,	,	kIx,	,
s.	s.	k?	s.
394	[number]	k4	394
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HOSTOMSKÁ	HOSTOMSKÁ	kA	HOSTOMSKÁ
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
–	–	k?	–
Průvodce	průvodka	k1gFnSc6	průvodka
operní	operní	k2eAgFnSc7d1	operní
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NS	NS	kA	NS
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
1466	[number]	k4	1466
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
637	[number]	k4	637
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
701	[number]	k4	701
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Šupky	Šupky	k?	Šupky
na	na	k7c4	na
OperaPlus	OperaPlus	k1gInSc4	OperaPlus
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
Antonínu	Antonín	k1gMnSc3	Antonín
Dvořákovi	Dvořák	k1gMnSc6	Dvořák
</s>
</p>
<p>
<s>
Partitura	partitura	k1gFnSc1	partitura
předehry	předehra	k1gFnSc2	předehra
k	k	k7c3	k
Alfredovi	Alfred	k1gMnSc3	Alfred
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
