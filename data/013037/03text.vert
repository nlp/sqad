<p>
<s>
Batok	Batok	k1gInSc1	Batok
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
jednotka	jednotka	k1gFnSc1	jednotka
objemu	objem	k1gInSc2	objem
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
skořápka	skořápka	k1gFnSc1	skořápka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Převodní	převodní	k2eAgInPc1d1	převodní
vztahy	vztah	k1gInPc1	vztah
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
batok	batok	k1gInSc1	batok
=	=	kIx~	=
1,072	[number]	k4	1,072
l	l	kA	l
=	=	kIx~	=
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
gantang	gantanga	k1gFnPc2	gantanga
=	=	kIx~	=
1,204	[number]	k4	1,204
<g/>
8	[number]	k4	8
kojan	kojana	k1gFnPc2	kojana
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Chvojka	chvojka	k1gFnSc1	chvojka
–	–	k?	–
J.	J.	kA	J.
Skála	Skála	k1gMnSc1	Skála
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
slovník	slovník	k1gInSc1	slovník
jednotek	jednotka	k1gFnPc2	jednotka
měření	měření	k1gNnSc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
