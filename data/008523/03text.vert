<p>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
byla	být	k5eAaImAgFnS	být
mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
vítězi	vítěz	k1gMnPc7	vítěz
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
byly	být	k5eAaImAgFnP	být
sjednány	sjednat	k5eAaPmNgFnP	sjednat
mírové	mírový	k2eAgFnPc1d1	mírová
smlouvy	smlouva	k1gFnPc1	smlouva
mezi	mezi	k7c7	mezi
vítěznými	vítězný	k2eAgInPc7d1	vítězný
státy	stát	k1gInPc7	stát
Dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
poraženými	poražený	k2eAgFnPc7d1	poražená
Centrálními	centrální	k2eAgFnPc7d1	centrální
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
byly	být	k5eAaImAgFnP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
mírové	mírový	k2eAgFnPc1d1	mírová
smlouvy	smlouva	k1gFnPc1	smlouva
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
,	,	kIx,	,
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
,	,	kIx,	,
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
a	a	k8xC	a
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řízení	řízení	k1gNnSc6	řízení
konference	konference	k1gFnSc2	konference
a	a	k8xC	a
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
měly	mít	k5eAaImAgInP	mít
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
měly	mít	k5eAaImAgFnP	mít
největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
konečném	konečný	k2eAgNnSc6d1	konečné
vítězství	vítězství	k1gNnSc6	vítězství
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Velkou	velký	k2eAgFnSc4d1	velká
pětku	pětka	k1gFnSc4	pětka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
role	role	k1gFnSc1	role
japonské	japonský	k2eAgFnSc2d1	japonská
delegace	delegace	k1gFnSc2	delegace
byla	být	k5eAaImAgFnS	být
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Konference	konference	k1gFnSc1	konference
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
a	a	k8xC	a
s	s	k7c7	s
několika	několik	k4yIc7	několik
přestávkami	přestávka	k1gFnPc7	přestávka
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Účastnili	účastnit	k5eAaImAgMnP	účastnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
představitelé	představitel	k1gMnPc1	představitel
celkem	celkem	k6eAd1	celkem
32	[number]	k4	32
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
mírového	mírový	k2eAgInSc2d1	mírový
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
Čtrnácti	čtrnáct	k4xCc2	čtrnáct
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
sepsal	sepsat	k5eAaPmAgMnS	sepsat
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
přednesl	přednést	k5eAaPmAgMnS	přednést
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
USA	USA	kA	USA
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
Woodrow	Woodrow	k1gFnPc2	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
poraženými	poražený	k2eAgNnPc7d1	poražené
státy	stát	k1gInPc1	stát
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
Pařížské	pařížský	k2eAgFnPc1d1	Pařížská
předměstské	předměstský	k2eAgFnPc1d1	předměstská
smlouvy	smlouva	k1gFnPc1	smlouva
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
jiného	jiný	k2eAgNnSc2d1	jiné
pařížského	pařížský	k2eAgNnSc2d1	pařížské
předměstí	předměstí	k1gNnSc2	předměstí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dominantními	dominantní	k2eAgFnPc7d1	dominantní
diplomatickými	diplomatický	k2eAgFnPc7d1	diplomatická
osobnostmi	osobnost	k1gFnPc7	osobnost
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Georges	Georges	k1gMnSc1	Georges
Clemenceau	Clemenceaus	k1gInSc2	Clemenceaus
<g/>
,	,	kIx,	,
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Francie	Francie	k1gFnSc2	Francie
<g/>
;	;	kIx,	;
David	David	k1gMnSc1	David
Lloyd	Lloyd	k1gMnSc1	Lloyd
George	Georg	k1gMnSc2	Georg
<g/>
,	,	kIx,	,
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
;	;	kIx,	;
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
a	a	k8xC	a
Vittorio	Vittorio	k6eAd1	Vittorio
Orlando	Orlanda	k1gFnSc5	Orlanda
<g/>
,	,	kIx,	,
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
nazývaní	nazývaný	k2eAgMnPc1d1	nazývaný
též	též	k9	též
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
čtyřka	čtyřka	k1gFnSc1	čtyřka
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Rada	rada	k1gFnSc1	rada
čtyř	čtyři	k4xCgInPc2	čtyři
<g/>
"	"	kIx"	"
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Big	Big	k1gFnSc1	Big
four	four	k1gMnSc1	four
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Organizace	organizace	k1gFnSc2	organizace
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
příměřím	příměří	k1gNnSc7	příměří
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
počet	počet	k1gInSc1	počet
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
s	s	k7c7	s
poraženým	poražený	k2eAgNnSc7d1	poražené
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
spojenci	spojenec	k1gMnPc7	spojenec
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
organizaci	organizace	k1gFnSc4	organizace
celé	celý	k2eAgFnSc2d1	celá
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Formální	formální	k2eAgNnSc1d1	formální
zahájení	zahájení	k1gNnSc1	zahájení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Francie	Francie	k1gFnSc2	Francie
za	za	k7c2	za
předsednictví	předsednictví	k1gNnSc2	předsednictví
prezidenta	prezident	k1gMnSc2	prezident
Francie	Francie	k1gFnSc2	Francie
Raymonda	Raymond	k1gMnSc2	Raymond
Poincaré	Poincarý	k2eAgNnSc4d1	Poincaré
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
hlavních	hlavní	k2eAgFnPc2d1	hlavní
velmocí	velmoc	k1gFnPc2	velmoc
určilo	určit	k5eAaPmAgNnS	určit
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
ostatní	ostatní	k2eAgInPc1d1	ostatní
státy	stát	k1gInPc1	stát
Dohody	dohoda	k1gFnSc2	dohoda
delegátů	delegát	k1gMnPc2	delegát
a	a	k8xC	a
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
účastnit	účastnit	k5eAaImF	účastnit
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
slyšení	slyšení	k1gNnSc2	slyšení
v	v	k7c6	v
plénu	plénum	k1gNnSc6	plénum
a	a	k8xC	a
v	v	k7c6	v
komisích	komise	k1gFnPc6	komise
<g/>
.	.	kIx.	.
</s>
<s>
Určující	určující	k2eAgInSc1d1	určující
byl	být	k5eAaImAgInS	být
jednací	jednací	k2eAgInSc1d1	jednací
konferenční	konferenční	k2eAgInSc1d1	konferenční
řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
reglement	reglement	k1gInSc1	reglement
de	de	k?	de
la	la	k1gNnSc7	la
conférence	conférence	k1gFnSc2	conférence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
spojené	spojený	k2eAgFnPc4d1	spojená
a	a	k8xC	a
sdružené	sdružený	k2eAgInPc1d1	sdružený
státy	stát	k1gInPc1	stát
Dohody	dohoda	k1gFnSc2	dohoda
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
skupině	skupina	k1gFnSc6	skupina
byly	být	k5eAaImAgFnP	být
mocnosti	mocnost	k1gFnPc1	mocnost
s	s	k7c7	s
obecnými	obecný	k2eAgInPc7d1	obecný
zájmy	zájem	k1gInPc7	zájem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
velmoci	velmoc	k1gFnPc1	velmoc
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
účastnit	účastnit	k5eAaImF	účastnit
všech	všecek	k3xTgFnPc2	všecek
schůzí	schůze	k1gFnPc2	schůze
vedených	vedený	k2eAgFnPc2d1	vedená
na	na	k7c6	na
konferencích	konference	k1gFnPc6	konference
a	a	k8xC	a
v	v	k7c6	v
komisích	komise	k1gFnPc6	komise
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
skupině	skupina	k1gFnSc6	skupina
byly	být	k5eAaImAgInP	být
státy	stát	k1gInPc1	stát
se	s	k7c7	s
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
zájmy	zájem	k1gInPc7	zájem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Britská	britský	k2eAgNnPc4d1	Britské
dominia	dominion	k1gNnPc4	dominion
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
,	,	kIx,	,
Haiti	Haiti	k1gNnSc2	Haiti
<g/>
,	,	kIx,	,
Hedžáz	Hedžáza	k1gFnPc2	Hedžáza
<g/>
,	,	kIx,	,
Honduras	Honduras	k1gInSc1	Honduras
<g/>
,	,	kIx,	,
Libérie	Libérie	k1gFnSc1	Libérie
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
<g/>
,	,	kIx,	,
Panama	panama	k1gNnSc1	panama
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
Siam	Siam	k1gInSc1	Siam
a	a	k8xC	a
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Delegáti	delegát	k1gMnPc1	delegát
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
účastnit	účastnit	k5eAaImF	účastnit
schůzí	schůze	k1gFnSc7	schůze
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
probíraly	probírat	k5eAaImAgFnP	probírat
záležitosti	záležitost	k1gFnPc4	záležitost
dotýkající	dotýkající	k2eAgFnPc4d1	dotýkající
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc2	jejich
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
skupině	skupina	k1gFnSc6	skupina
byly	být	k5eAaImAgInP	být
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
s	s	k7c7	s
centrálními	centrální	k2eAgFnPc7d1	centrální
mocnostmi	mocnost	k1gFnPc7	mocnost
přerušily	přerušit	k5eAaPmAgInP	přerušit
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g/>
,	,	kIx,	,
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
a	a	k8xC	a
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
.	.	kIx.	.
</s>
<s>
Delegáti	delegát	k1gMnPc1	delegát
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
také	také	k9	také
mohli	moct	k5eAaImAgMnP	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
pouze	pouze	k6eAd1	pouze
jednání	jednání	k1gNnSc4	jednání
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
probíraly	probírat	k5eAaImAgFnP	probírat
záležitosti	záležitost	k1gFnPc4	záležitost
dotýkající	dotýkající	k2eAgFnPc4d1	dotýkající
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc2	jejich
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
neutrální	neutrální	k2eAgInPc1d1	neutrální
nebo	nebo	k8xC	nebo
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nově	nově	k6eAd1	nově
tvořily	tvořit	k5eAaImAgFnP	tvořit
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
velmocemi	velmoc	k1gFnPc7	velmoc
pozvány	pozván	k2eAgFnPc4d1	pozvána
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přednesly	přednést	k5eAaPmAgFnP	přednést
ústně	ústně	k6eAd1	ústně
nebo	nebo	k8xC	nebo
písemně	písemně	k6eAd1	písemně
svá	svůj	k3xOyFgNnPc4	svůj
stanoviska	stanovisko	k1gNnPc4	stanovisko
v	v	k7c6	v
záležitostech	záležitost	k1gFnPc6	záležitost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
týkaly	týkat	k5eAaImAgInP	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc1	počet
delegátů	delegát	k1gMnPc2	delegát
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
byly	být	k5eAaImAgInP	být
určeny	určit	k5eAaPmNgInP	určit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
velmoc	velmoc	k1gFnSc1	velmoc
měla	mít	k5eAaImAgFnS	mít
5	[number]	k4	5
delegátů	delegát	k1gMnPc2	delegát
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
a	a	k8xC	a
Srbsko	Srbsko	k1gNnSc1	Srbsko
tři	tři	k4xCgMnPc4	tři
delegáty	delegát	k1gMnPc4	delegát
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgMnPc6	dva
delegátech	delegát	k1gMnPc6	delegát
měly	mít	k5eAaImAgFnP	mít
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Hedžáz	Hedžáza	k1gFnPc2	Hedžáza
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Siam	Siam	k1gInSc1	Siam
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
a	a	k8xC	a
Československo	Československo	k1gNnSc1	Československo
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Eduard	Eduard	k1gMnSc1	Eduard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc2	jeden
delegáta	delegát	k1gMnSc2	delegát
měly	mít	k5eAaImAgFnP	mít
Bolívie	Bolívie	k1gFnPc1	Bolívie
<g/>
,	,	kIx,	,
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
,	,	kIx,	,
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
,	,	kIx,	,
Haiti	Haiti	k1gNnSc1	Haiti
<g/>
,	,	kIx,	,
Honduras	Honduras	k1gInSc1	Honduras
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
Libérie	Libérie	k1gFnSc1	Libérie
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Nicaragua	Nicaragua	k1gFnSc1	Nicaragua
<g/>
,	,	kIx,	,
Panam	Panama	k1gFnPc2	Panama
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
a	a	k8xC	a
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálními	oficiální	k2eAgInPc7d1	oficiální
jazyky	jazyk	k1gInPc7	jazyk
používanými	používaný	k2eAgInPc7d1	používaný
na	na	k7c6	na
mírové	mírový	k2eAgFnSc6d1	mírová
konferenci	konference	k1gFnSc6	konference
byla	být	k5eAaImAgFnS	být
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konečná	Konečná	k1gFnSc1	Konečná
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
kompetenci	kompetence	k1gFnSc6	kompetence
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
Rada	rada	k1gFnSc1	rada
deseti	deset	k4xCc2	deset
(	(	kIx(	(
<g/>
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
již	již	k6eAd1	již
jen	jen	k9	jen
Rada	rada	k1gFnSc1	rada
čtyř	čtyři	k4xCgFnPc2	čtyři
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byli	být	k5eAaImAgMnP	být
delegáti	delegát	k1gMnPc1	delegát
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
střídavě	střídavě	k6eAd1	střídavě
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
účastnil	účastnit	k5eAaImAgMnS	účastnit
i	i	k9	i
delegát	delegát	k1gMnSc1	delegát
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
italského	italský	k2eAgMnSc2d1	italský
delegáta	delegát	k1gMnSc2	delegát
(	(	kIx(	(
<g/>
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
Rada	rada	k1gFnSc1	rada
tří	tři	k4xCgFnPc2	tři
<g/>
.	.	kIx.	.
</s>
<s>
Ministři	ministr	k1gMnPc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
po	po	k7c6	po
reorganizaci	reorganizace	k1gFnSc6	reorganizace
konference	konference	k1gFnSc2	konference
odešli	odejít	k5eAaPmAgMnP	odejít
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
Rady	rada	k1gFnSc2	rada
deseti	deset	k4xCc2	deset
a	a	k8xC	a
nezasedali	zasedat	k5eNaImAgMnP	zasedat
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
čtyř	čtyři	k4xCgMnPc2	čtyři
<g/>
,	,	kIx,	,
utvořili	utvořit	k5eAaPmAgMnP	utvořit
orgán	orgán	k1gInSc4	orgán
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Rada	rada	k1gFnSc1	rada
pěti	pět	k4xCc3	pět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
bylo	být	k5eAaImAgNnS	být
trvale	trvale	k6eAd1	trvale
zastoupeno	zastoupen	k2eAgNnSc1d1	zastoupeno
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sbor	sbor	k1gInSc1	sbor
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
menšími	malý	k2eAgInPc7d2	menší
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
naléhavě	naléhavě	k6eAd1	naléhavě
řešit	řešit	k5eAaImF	řešit
a	a	k8xC	a
ulehčoval	ulehčovat	k5eAaImAgMnS	ulehčovat
tak	tak	k6eAd1	tak
práci	práce	k1gFnSc4	práce
Radě	rada	k1gFnSc3	rada
čtyř	čtyři	k4xCgInPc2	čtyři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
plenární	plenární	k2eAgFnSc6d1	plenární
schůzi	schůze	k1gFnSc6	schůze
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
pět	pět	k4xCc1	pět
rezolucí	rezoluce	k1gFnPc2	rezoluce
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yIgFnPc2	který
byly	být	k5eAaImAgFnP	být
zřízeny	zřídit	k5eAaPmNgFnP	zřídit
následující	následující	k2eAgFnPc1d1	následující
komise	komise	k1gFnPc1	komise
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
Společnost	společnost	k1gFnSc4	společnost
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
zastoupení	zastoupení	k1gNnSc2	zastoupení
ČSR	ČSR	kA	ČSR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
původců	původce	k1gMnPc2	původce
války	válka	k1gFnSc2	válka
a	a	k8xC	a
uložení	uložení	k1gNnSc2	uložení
sankcí	sankce	k1gFnPc2	sankce
</s>
</p>
<p>
<s>
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
náhradu	náhrada	k1gFnSc4	náhrada
škod	škoda	k1gFnPc2	škoda
(	(	kIx(	(
<g/>
zastoupení	zastoupení	k1gNnSc2	zastoupení
ČSR	ČSR	kA	ČSR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
zákonodárství	zákonodárství	k1gNnSc4	zákonodárství
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
zastoupení	zastoupení	k1gNnSc4	zastoupení
ČSR	ČSR	kA	ČSR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
úpravu	úprava	k1gFnSc4	úprava
přístavů	přístav	k1gInPc2	přístav
<g/>
,	,	kIx,	,
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
železnic	železnice	k1gFnPc2	železnice
(	(	kIx(	(
<g/>
zastoupení	zastoupení	k1gNnSc2	zastoupení
ČSR	ČSR	kA	ČSR
<g/>
)	)	kIx)	)
<g/>
Podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
byly	být	k5eAaImAgFnP	být
zřízeny	zřídit	k5eAaPmNgFnP	zřídit
ještě	ještě	k9	ještě
komise	komise	k1gFnPc1	komise
další	další	k2eAgFnPc1d1	další
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
otázky	otázka	k1gFnPc4	otázka
finanční	finanční	k2eAgFnPc4d1	finanční
</s>
</p>
<p>
<s>
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
otázky	otázka	k1gFnPc4	otázka
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
</s>
</p>
<p>
<s>
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
otázky	otázka	k1gFnPc4	otázka
územní	územní	k2eAgFnPc4d1	územní
(	(	kIx(	(
<g/>
zasedaly	zasedat	k5eAaImAgFnP	zasedat
územní	územní	k2eAgFnPc1d1	územní
komise	komise	k1gFnPc1	komise
pro	pro	k7c4	pro
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
a	a	k8xC	a
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
,	,	kIx,	,
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
Řecko	Řecko	k1gNnSc4	Řecko
a	a	k8xC	a
Albánii	Albánie	k1gFnSc4	Albánie
a	a	k8xC	a
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
Belgii	Belgie	k1gFnSc4	Belgie
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc4	Dánsko
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
komise	komise	k1gFnSc1	komise
vojenská	vojenský	k2eAgFnSc1d1	vojenská
<g/>
,	,	kIx,	,
námořní	námořní	k2eAgFnSc1d1	námořní
a	a	k8xC	a
letecká	letecký	k2eAgFnSc1d1	letecká
</s>
</p>
<p>
<s>
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
nové	nový	k2eAgInPc4d1	nový
státy	stát	k1gInPc4	stát
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
menšin	menšina	k1gFnPc2	menšina
</s>
</p>
<p>
<s>
komise	komise	k1gFnPc1	komise
redakční	redakční	k2eAgFnPc1d1	redakční
(	(	kIx(	(
<g/>
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
pěti	pět	k4xCc2	pět
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
)	)	kIx)	)
<g/>
Předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
se	se	k3xPyFc4	se
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
místopředsedů	místopředseda	k1gMnPc2	místopředseda
<g/>
,	,	kIx,	,
generálního	generální	k2eAgInSc2d1	generální
sekretariátu	sekretariát	k1gInSc2	sekretariát
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
a	a	k8xC	a
verifikačním	verifikační	k2eAgInSc7d1	verifikační
výborem	výbor	k1gInSc7	výbor
a	a	k8xC	a
redakční	redakční	k2eAgFnSc7d1	redakční
komisí	komise	k1gFnSc7	komise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otázka	otázka	k1gFnSc1	otázka
zásady	zásada	k1gFnSc2	zásada
rovnosti	rovnost	k1gFnSc2	rovnost
států	stát	k1gInPc2	stát
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
usnesení	usnesení	k1gNnSc1	usnesení
platné	platný	k2eAgNnSc1d1	platné
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
hlasování	hlasování	k1gNnSc2	hlasování
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nerozhodovalo	rozhodovat	k5eNaImAgNnS	rozhodovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pouhé	pouhý	k2eAgFnSc2d1	pouhá
většiny	většina	k1gFnSc2	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
činitelem	činitel	k1gInSc7	činitel
na	na	k7c6	na
mírové	mírový	k2eAgFnSc6d1	mírová
konferenci	konference	k1gFnSc6	konference
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rada	rada	k1gFnSc1	rada
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
složení	složení	k1gNnSc6	složení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
sice	sice	k8xC	sice
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
právem	právo	k1gNnSc7	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odporovala	odporovat	k5eAaImAgFnS	odporovat
zásadě	zásada	k1gFnSc3	zásada
o	o	k7c6	o
rovnosti	rovnost	k1gFnSc6	rovnost
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ustanovení	ustanovení	k1gNnSc1	ustanovení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rada	rada	k1gFnSc1	rada
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
pro	pro	k7c4	pro
menší	malý	k2eAgFnPc4d2	menší
či	či	k8xC	či
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgInP	muset
tyto	tento	k3xDgInPc1	tento
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Převážil	převážit	k5eAaPmAgInS	převážit
však	však	k9	však
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
konsensus	konsensus	k1gInSc1	konsensus
o	o	k7c6	o
potřebě	potřeba	k1gFnSc6	potřeba
upřednostnění	upřednostnění	k1gNnSc2	upřednostnění
obecných	obecný	k2eAgInPc2d1	obecný
zájmů	zájem	k1gInPc2	zájem
před	před	k7c7	před
partikulárními	partikulární	k2eAgFnPc7d1	partikulární
a	a	k8xC	a
proto	proto	k6eAd1	proto
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
zvolená	zvolený	k2eAgFnSc1d1	zvolená
metoda	metoda	k1gFnSc1	metoda
řízení	řízení	k1gNnSc2	řízení
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
všemi	všecek	k3xTgFnPc7	všecek
stranami	strana	k1gFnPc7	strana
tolerována	tolerovat	k5eAaImNgFnS	tolerovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
předměstské	předměstský	k2eAgFnSc2d1	předměstská
smlouvy	smlouva	k1gFnSc2	smlouva
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
pařížské	pařížský	k2eAgFnPc1d1	Pařížská
mírové	mírový	k2eAgFnPc1d1	mírová
konference	konference	k1gFnPc1	konference
byly	být	k5eAaImAgFnP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
Pařížské	pařížský	k2eAgFnPc1d1	Pařížská
předměstské	předměstský	k2eAgFnPc1d1	předměstská
smlouvy	smlouva	k1gFnPc1	smlouva
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
Versailleský	versailleský	k2eAgInSc4d1	versailleský
mírový	mírový	k2eAgInSc4d1	mírový
systém	systém	k1gInSc4	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
předložena	předložit	k5eAaPmNgFnS	předložit
Německu	Německo	k1gNnSc6	Německo
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
požadováno	požadován	k2eAgNnSc1d1	požadováno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
případné	případný	k2eAgFnPc4d1	případná
připomínky	připomínka	k1gFnPc4	připomínka
německé	německý	k2eAgFnSc2d1	německá
delegace	delegace	k1gFnSc2	delegace
nebyly	být	k5eNaImAgInP	být
diskutovány	diskutovat	k5eAaImNgInP	diskutovat
ústně	ústně	k6eAd1	ústně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
podány	podat	k5eAaPmNgFnP	podat
písemně	písemně	k6eAd1	písemně
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
delegace	delegace	k1gFnSc1	delegace
předložené	předložený	k2eAgFnSc3d1	předložená
mírové	mírový	k2eAgFnSc3d1	mírová
smlouvě	smlouva	k1gFnSc3	smlouva
vytýkala	vytýkat	k5eAaImAgFnS	vytýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
odchýlila	odchýlit	k5eAaPmAgFnS	odchýlit
od	od	k7c2	od
smluv	smlouva	k1gFnPc2	smlouva
daných	daný	k2eAgFnPc2d1	daná
příměřím	příměří	k1gNnSc7	příměří
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
jejich	jejich	k3xOp3gFnPc1	jejich
námitky	námitka	k1gFnPc1	námitka
vedly	vést	k5eAaImAgFnP	vést
jen	jen	k9	jen
k	k	k7c3	k
menším	malý	k2eAgFnPc3d2	menší
změnám	změna	k1gFnPc3	změna
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
německými	německý	k2eAgMnPc7d1	německý
delegáty	delegát	k1gMnPc4	delegát
podepsána	podepsán	k2eAgFnSc1d1	podepsána
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c4	v
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
platnost	platnost	k1gFnSc4	platnost
měla	mít	k5eAaImAgFnS	mít
vstoupit	vstoupit	k5eAaPmF	vstoupit
až	až	k9	až
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
byly	být	k5eAaImAgFnP	být
podepsány	podepsán	k2eAgFnPc1d1	podepsána
smlouvy	smlouva	k1gFnPc1	smlouva
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
<g/>
,	,	kIx,	,
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
o	o	k7c6	o
pomoci	pomoc	k1gFnSc6	pomoc
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nevyvolaného	vyvolaný	k2eNgInSc2d1	nevyvolaný
útoku	útok	k1gInSc2	útok
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Belgií	Belgie	k1gFnSc7	Belgie
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
o	o	k7c6	o
vojenském	vojenský	k2eAgNnSc6d1	vojenské
obsazení	obsazení	k1gNnSc6	obsazení
Porýní	Porýní	k1gNnSc2	Porýní
a	a	k8xC	a
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgFnPc7d1	hlavní
mocnostmi	mocnost	k1gFnPc7	mocnost
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
ochrany	ochrana	k1gFnSc2	ochrana
rasových	rasový	k2eAgInPc2d1	rasový
<g/>
,	,	kIx,	,
náboženských	náboženský	k2eAgInPc2d1	náboženský
a	a	k8xC	a
jazykových	jazykový	k2eAgFnPc2d1	jazyková
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
o	o	k7c6	o
obchodních	obchodní	k2eAgInPc6d1	obchodní
stycích	styk	k1gInPc6	styk
a	a	k8xC	a
o	o	k7c6	o
přistoupení	přistoupení	k1gNnSc6	přistoupení
Polska	Polsko	k1gNnSc2	Polsko
k	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
obecným	obecný	k2eAgFnPc3d1	obecná
úmluvám	úmluva	k1gFnPc3	úmluva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
se	se	k3xPyFc4	se
Rada	rada	k1gFnSc1	rada
čtyř	čtyři	k4xCgMnPc2	čtyři
rozešla	rozejít	k5eAaPmAgFnS	rozejít
a	a	k8xC	a
přípravou	příprava	k1gFnSc7	příprava
dalších	další	k2eAgFnPc2d1	další
mírových	mírový	k2eAgFnPc2d1	mírová
smluv	smlouva	k1gFnPc2	smlouva
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
,	,	kIx,	,
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
,	,	kIx,	,
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
bylo	být	k5eAaImAgNnS	být
pověřeno	pověřit	k5eAaPmNgNnS	pověřit
pět	pět	k4xCc1	pět
ministrů	ministr	k1gMnPc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rakousku	Rakousko	k1gNnSc6	Rakousko
byly	být	k5eAaImAgFnP	být
mírové	mírový	k2eAgFnPc1d1	mírová
podmínky	podmínka	k1gFnPc1	podmínka
předloženy	předložen	k2eAgFnPc4d1	předložena
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1919	[number]	k4	1919
a	a	k8xC	a
po	po	k7c6	po
menších	malý	k2eAgFnPc6d2	menší
změnách	změna	k1gFnPc6	změna
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
týkajících	týkající	k2eAgNnPc2d1	týkající
se	se	k3xPyFc4	se
hospodářství	hospodářství	k1gNnPc2	hospodářství
a	a	k8xC	a
financí	finance	k1gFnPc2	finance
byla	být	k5eAaImAgFnS	být
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
podepsána	podepsat	k5eAaPmNgFnS	podepsat
v	v	k7c4	v
Saint-Germain-en	Saint-Germainn	k2eAgInSc4d1	Saint-Germain-en
Laye	Laye	k1gInSc4	Laye
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc7	jejich
spojenci	spojenec	k1gMnPc7	spojenec
a	a	k8xC	a
Československem	Československo	k1gNnSc7	Československo
sjednána	sjednat	k5eAaPmNgFnS	sjednat
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
rasových	rasový	k2eAgFnPc2d1	rasová
<g/>
,	,	kIx,	,
náboženských	náboženský	k2eAgFnPc2d1	náboženská
a	a	k8xC	a
jazykových	jazykový	k2eAgFnPc2d1	jazyková
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
o	o	k7c6	o
obchodních	obchodní	k2eAgInPc6d1	obchodní
stycích	styk	k1gInPc6	styk
a	a	k8xC	a
o	o	k7c6	o
přistoupení	přistoupení	k1gNnSc6	přistoupení
Československa	Československo	k1gNnSc2	Československo
k	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
obecným	obecný	k2eAgFnPc3d1	obecná
úmluvám	úmluva	k1gFnPc3	úmluva
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
obdobná	obdobný	k2eAgFnSc1d1	obdobná
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
královstvím	království	k1gNnSc7	království
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
,	,	kIx,	,
úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c4	o
revizi	revize	k1gFnSc4	revize
berlínských	berlínský	k2eAgInPc2d1	berlínský
generálních	generální	k2eAgInPc2d1	generální
aktů	akt	k1gInPc2	akt
z	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1885	[number]	k4	1885
a	a	k8xC	a
bruselských	bruselský	k2eAgInPc2d1	bruselský
generálních	generální	k2eAgInPc2d1	generální
aktů	akt	k1gInPc2	akt
a	a	k8xC	a
deklarací	deklarace	k1gFnPc2	deklarace
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
kontrole	kontrola	k1gFnSc6	kontrola
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
a	a	k8xC	a
střelivem	střelivo	k1gNnSc7	střelivo
a	a	k8xC	a
také	také	k9	také
úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
lihovinami	lihovina	k1gFnPc7	lihovina
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
podepsána	podepsán	k2eAgFnSc1d1	podepsána
úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
letectví	letectví	k1gNnSc6	letectví
<g/>
,	,	kIx,	,
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
delegáti	delegát	k1gMnPc1	delegát
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
začali	začít	k5eAaPmAgMnP	začít
zabývat	zabývat	k5eAaImF	zabývat
mírovými	mírový	k2eAgFnPc7d1	mírová
podmínkami	podmínka	k1gFnPc7	podmínka
pro	pro	k7c4	pro
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
podepsána	podepsat	k5eAaPmNgFnS	podepsat
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
v	v	k7c6	v
Neuilly-sur-Seine	Neuillyur-Sein	k1gInSc5	Neuilly-sur-Sein
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1919	[number]	k4	1919
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
se	s	k7c7	s
státy	stát	k1gInPc7	stát
Dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
spojenci	spojenec	k1gMnPc1	spojenec
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
menšin	menšina	k1gFnPc2	menšina
a	a	k8xC	a
obchodních	obchodní	k2eAgInPc6d1	obchodní
stycích	styk	k1gInPc6	styk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
bolševické	bolševický	k2eAgFnSc2d1	bolševická
diktatury	diktatura	k1gFnSc2	diktatura
Bély	Béla	k1gMnSc2	Béla
Kuna	kuna	k1gFnSc1	kuna
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
byla	být	k5eAaImAgFnS	být
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
předložena	předložen	k2eAgFnSc1d1	předložena
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
a	a	k8xC	a
podepsána	podepsán	k2eAgFnSc1d1	podepsána
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1920	[number]	k4	1920
v	v	k7c6	v
Trianonu	Trianon	k1gInSc6	Trianon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1920	[number]	k4	1920
v	v	k7c6	v
Sè	Sè	k1gFnSc6	Sè
podepsána	podepsán	k2eAgFnSc1d1	podepsána
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ovšem	ovšem	k9	ovšem
později	pozdě	k6eAd2	pozdě
nahradila	nahradit	k5eAaPmAgFnS	nahradit
lausannská	lausannský	k2eAgFnSc1d1	Lausannská
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sè	Sè	k1gFnSc6	Sè
byla	být	k5eAaImAgFnS	být
také	také	k9	také
podepsána	podepsat	k5eAaPmNgFnS	podepsat
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc7	jejich
spojenci	spojenec	k1gMnPc7	spojenec
a	a	k8xC	a
Řeckem	Řecko	k1gNnSc7	Řecko
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
menšin	menšina	k1gFnPc2	menšina
a	a	k8xC	a
obchodních	obchodní	k2eAgInPc6d1	obchodní
stycích	styk	k1gInPc6	styk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sjednání	sjednání	k1gNnSc6	sjednání
všem	všecek	k3xTgMnPc3	všecek
mírových	mírový	k2eAgFnPc2d1	mírová
smluv	smlouva	k1gFnPc2	smlouva
s	s	k7c7	s
poraženými	poražený	k2eAgInPc7d1	poražený
státy	stát	k1gInPc7	stát
se	se	k3xPyFc4	se
Rada	rada	k1gFnSc1	rada
pěti	pět	k4xCc2	pět
ministrů	ministr	k1gMnPc2	ministr
rozešla	rozejít	k5eAaPmAgFnS	rozejít
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
místo	místo	k1gNnSc4	místo
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Konference	konference	k1gFnSc1	konference
velvyslanců	velvyslanec	k1gMnPc2	velvyslanec
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
států	stát	k1gInPc2	stát
pověřených	pověřený	k2eAgInPc2d1	pověřený
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
orgán	orgán	k1gInSc1	orgán
velmocí	velmoc	k1gFnPc2	velmoc
s	s	k7c7	s
trvalou	trvalý	k2eAgFnSc7d1	trvalá
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bděl	bdít	k5eAaImAgInS	bdít
nad	nad	k7c7	nad
dodržováním	dodržování	k1gNnSc7	dodržování
mírových	mírový	k2eAgFnPc2d1	mírová
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
smlouvách	smlouva	k1gFnPc6	smlouva
jsou	být	k5eAaImIp3nP	být
obsažena	obsáhnout	k5eAaPmNgNnP	obsáhnout
i	i	k8xC	i
jim	on	k3xPp3gMnPc3	on
všem	všecek	k3xTgMnPc3	všecek
společná	společný	k2eAgNnPc1d1	společné
<g/>
,	,	kIx,	,
identická	identický	k2eAgNnPc1d1	identické
ustanovení	ustanovení	k1gNnPc1	ustanovení
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
ILO	ILO	kA	ILO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DEJMEK	Dejmek	k1gMnSc1	Dejmek
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
nové	nový	k2eAgFnSc2d1	nová
Evropy	Evropa	k1gFnSc2	Evropa
:	:	kIx,	:
Versailles	Versailles	k1gFnSc1	Versailles
<g/>
,	,	kIx,	,
St.	st.	kA	st.
<g/>
-Germain	-Germain	k1gInSc1	-Germain
<g/>
,	,	kIx,	,
Trianon	Trianon	k1gInSc1	Trianon
a	a	k8xC	a
dotváření	dotváření	k1gNnSc1	dotváření
poválečného	poválečný	k2eAgInSc2d1	poválečný
mírového	mírový	k2eAgInSc2d1	mírový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
518	[number]	k4	518
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7286	[number]	k4	7286
<g/>
-	-	kIx~	-
<g/>
188	[number]	k4	188
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACMILLAN	MACMILLAN	kA	MACMILLAN
<g/>
,	,	kIx,	,
Margaret	Margareta	k1gFnPc2	Margareta
Olwen	Olwna	k1gFnPc2	Olwna
<g/>
.	.	kIx.	.
</s>
<s>
Mírotvorci	mírotvorce	k1gMnPc1	mírotvorce
:	:	kIx,	:
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
konference	konference	k1gFnSc1	konference
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
551	[number]	k4	551
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1151	[number]	k4	1151
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PETRÁŠ	Petráš	k1gMnSc1	Petráš
<g/>
,	,	kIx,	,
René	René	k1gMnSc1	René
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodněprávní	mezinárodněprávní	k2eAgFnSc1d1	mezinárodněprávní
ochrana	ochrana	k1gFnSc1	ochrana
menšin	menšina	k1gFnPc2	menšina
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
11	[number]	k4	11
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
31	[number]	k4	31
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOUKUP	Soukup	k1gMnSc1	Soukup
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Entente	entente	k1gFnSc1	entente
Powers	Powers	k1gInSc1	Powers
and	and	k?	and
the	the	k?	the
Rhineland	Rhineland	k1gInSc1	Rhineland
Question	Question	k1gInSc1	Question
in	in	k?	in
the	the	k?	the
Armistice	Armistika	k1gFnSc3	Armistika
Period	perioda	k1gFnPc2	perioda
and	and	k?	and
during	during	k1gInSc1	during
the	the	k?	the
Paris	Paris	k1gMnSc1	Paris
Peace	Peace	k1gMnSc2	Peace
Conference	Conferenec	k1gInSc2	Conferenec
of	of	k?	of
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
s.	s.	k?	s.
227	[number]	k4	227
<g/>
-	-	kIx~	-
<g/>
240	[number]	k4	240
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
208	[number]	k4	208
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
-	-	kIx~	-
chronologie	chronologie	k1gFnSc1	chronologie
událostí	událost	k1gFnPc2	událost
</s>
</p>
<p>
<s>
Důsledky	důsledek	k1gInPc4	důsledek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
