<p>
<s>
Pyšná	pyšný	k2eAgFnSc1d1	pyšná
princezna	princezna	k1gFnSc1	princezna
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
režiséra	režisér	k1gMnSc2	režisér
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Pyšné	pyšný	k2eAgFnSc3d1	pyšná
princezně	princezna	k1gFnSc3	princezna
Krasomile	krasomil	k1gMnSc5	krasomil
(	(	kIx(	(
<g/>
Alena	Alena	k1gFnSc1	Alena
Vránová	Vránová	k1gFnSc1	Vránová
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
za	za	k7c4	za
manžela	manžel	k1gMnSc4	manžel
dost	dost	k6eAd1	dost
dobrý	dobrý	k2eAgMnSc1d1	dobrý
hodný	hodný	k2eAgMnSc1d1	hodný
a	a	k8xC	a
spravedlivý	spravedlivý	k2eAgMnSc1d1	spravedlivý
král	král	k1gMnSc1	král
sousední	sousední	k2eAgFnSc2d1	sousední
země	zem	k1gFnSc2	zem
Miroslav	Miroslav	k1gMnSc1	Miroslav
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Ráž	Ráž	k1gMnSc1	Ráž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
za	za	k7c4	za
zahradníka	zahradník	k1gMnSc4	zahradník
vydá	vydat	k5eAaPmIp3nS	vydat
udělit	udělit	k5eAaPmF	udělit
princezně	princezna	k1gFnSc3	princezna
výchovnou	výchovný	k2eAgFnSc4d1	výchovná
lekci	lekce	k1gFnSc4	lekce
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
vypěstuje	vypěstovat	k5eAaPmIp3nS	vypěstovat
zpívající	zpívající	k2eAgFnSc4d1	zpívající
květinu	květina	k1gFnSc4	květina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vydrží	vydržet	k5eAaPmIp3nS	vydržet
s	s	k7c7	s
melodií	melodie	k1gFnSc7	melodie
do	do	k7c2	do
rána	ráno	k1gNnSc2	ráno
<g/>
,	,	kIx,	,
jen	jen	k9	jen
když	když	k8xS	když
princezna	princezna	k1gFnSc1	princezna
nebude	být	k5eNaImBp3nS	být
tak	tak	k6eAd1	tak
pyšná	pyšný	k2eAgFnSc1d1	pyšná
<g/>
.	.	kIx.	.
</s>
<s>
Princezna	princezna	k1gFnSc1	princezna
se	se	k3xPyFc4	se
do	do	k7c2	do
zahradníka	zahradník	k1gMnSc2	zahradník
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
uprchne	uprchnout	k5eAaPmIp3nS	uprchnout
a	a	k8xC	a
cestou	cesta	k1gFnSc7	cesta
pozná	poznat	k5eAaPmIp3nS	poznat
život	život	k1gInSc1	život
obyčejných	obyčejný	k2eAgMnPc2d1	obyčejný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
přestane	přestat	k5eAaPmIp3nS	přestat
být	být	k5eAaImF	být
pyšná	pyšný	k2eAgFnSc1d1	pyšná
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
zahradník	zahradník	k1gMnSc1	zahradník
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
jejich	jejich	k3xOp3gInSc2	jejich
sňatku	sňatek	k1gInSc2	sňatek
nestojí	stát	k5eNaImIp3nS	stát
nic	nic	k3yNnSc1	nic
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Námět	námět	k1gInSc1	námět
<g/>
:	:	kIx,	:
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kautský	Kautský	k2eAgMnSc1d1	Kautský
<g/>
,	,	kIx,	,
Henryk	Henryk	k1gMnSc1	Henryk
Bloch	Bloch	k1gMnSc1	Bloch
</s>
</p>
<p>
<s>
Scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Henryk	Henryk	k1gMnSc1	Henryk
Bloch	Bloch	k1gMnSc1	Bloch
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kautský	Kautský	k2eAgMnSc1d1	Kautský
</s>
</p>
<p>
<s>
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Dalibor	Dalibor	k1gMnSc1	Dalibor
C.	C.	kA	C.
Vačkář	Vačkář	k1gMnSc1	Vačkář
</s>
</p>
<p>
<s>
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Roth	Roth	k1gMnSc1	Roth
</s>
</p>
<p>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Zeman	Zeman	k1gMnSc1	Zeman
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
údaje	údaj	k1gInPc1	údaj
<g/>
:	:	kIx,	:
černobílý	černobílý	k2eAgMnSc1d1	černobílý
<g/>
,	,	kIx,	,
94	[number]	k4	94
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
pohádka	pohádka	k1gFnSc1	pohádka
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
<g/>
:	:	kIx,	:
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
státní	státní	k2eAgInSc1d1	státní
film	film	k1gInSc1	film
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
</s>
</p>
<p>
<s>
==	==	k?	==
Hrají	hrát	k5eAaImIp3nP	hrát
==	==	k?	==
</s>
</p>
<p>
<s>
Alena	Alena	k1gFnSc1	Alena
Vránová	Vránová	k1gFnSc1	Vránová
(	(	kIx(	(
<g/>
princezna	princezna	k1gFnSc1	princezna
Krasomila	krasomil	k1gMnSc2	krasomil
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Ráž	Ráž	k1gMnSc1	Ráž
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Neumann	Neumann	k1gMnSc1	Neumann
(	(	kIx(	(
<g/>
starý	starý	k2eAgMnSc1d1	starý
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
princezny	princezna	k1gFnSc2	princezna
Krasomily	krasomil	k1gMnPc4	krasomil
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mária	Mária	k1gFnSc1	Mária
Sýkorová	Sýkorová	k1gFnSc1	Sýkorová
(	(	kIx(	(
<g/>
chůva	chůva	k1gFnSc1	chůva
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seník	seník	k1gInSc1	seník
(	(	kIx(	(
<g/>
ministr	ministr	k1gMnSc1	ministr
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
(	(	kIx(	(
<g/>
kancléř	kancléř	k1gMnSc1	kancléř
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Dědek	Dědek	k1gMnSc1	Dědek
(	(	kIx(	(
<g/>
ceremoniář	ceremoniář	k1gMnSc1	ceremoniář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Effa	Effa	k1gMnSc1	Effa
(	(	kIx(	(
<g/>
strážce	strážce	k1gMnSc1	strážce
pokladu	poklást	k5eAaPmIp1nS	poklást
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Heverle	Heverle	k1gFnSc2	Heverle
(	(	kIx(	(
<g/>
Vítek	Vítek	k1gMnSc1	Vítek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Hlinomaz	hlinomaz	k1gMnSc1	hlinomaz
(	(	kIx(	(
<g/>
výběrčí	výběrčí	k1gFnSc1	výběrčí
daní	daň	k1gFnPc2	daň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Luděk	Luděk	k1gMnSc1	Luděk
Mandaus	Mandaus	k1gMnSc1	Mandaus
(	(	kIx(	(
<g/>
hlasatel	hlasatel	k1gMnSc1	hlasatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Čáp	Čáp	k1gMnSc1	Čáp
(	(	kIx(	(
<g/>
švec	švec	k1gMnSc1	švec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otomar	Otomar	k1gMnSc1	Otomar
Korbelář	korbelář	k1gMnSc1	korbelář
(	(	kIx(	(
<g/>
hospodář	hospodář	k1gMnSc1	hospodář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kurandová	Kurandový	k2eAgFnSc1d1	Kurandová
(	(	kIx(	(
<g/>
hospodyně	hospodyně	k1gFnSc1	hospodyně
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Werichová	Werichová	k1gFnSc1	Werichová
(	(	kIx(	(
<g/>
Madlenka	Madlenka	k1gFnSc1	Madlenka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bejval	Bejval	k1gMnSc1	Bejval
(	(	kIx(	(
<g/>
Ivánek	Ivánek	k1gMnSc1	Ivánek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Hanus	Hanus	k1gMnSc1	Hanus
(	(	kIx(	(
<g/>
uhlíř	uhlíř	k1gMnSc1	uhlíř
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nita	Nit	k2eAgFnSc1d1	Nita
Romanečová	Romanečová	k1gFnSc1	Romanečová
(	(	kIx(	(
<g/>
uhlířova	uhlířův	k2eAgFnSc1d1	uhlířova
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Terezie	Terezie	k1gFnSc1	Terezie
Brzková	Brzková	k1gFnSc1	Brzková
(	(	kIx(	(
<g/>
mlynářka	mlynářka	k1gFnSc1	mlynářka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kovářík	kovářík	k1gMnSc1	kovářík
(	(	kIx(	(
<g/>
mlynář	mlynář	k1gMnSc1	mlynář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Branko	branka	k1gFnSc5	branka
Koreň	Koreň	k1gFnSc1	Koreň
(	(	kIx(	(
<g/>
mleč	mleč	k1gMnSc1	mleč
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Nesvadba	Nesvadba	k1gMnSc1	Nesvadba
(	(	kIx(	(
<g/>
princ	princ	k1gMnSc1	princ
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
zacházejícího	zacházející	k2eAgNnSc2d1	zacházející
slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Kupšovský	Kupšovský	k1gMnSc1	Kupšovský
(	(	kIx(	(
<g/>
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
zbrojnoš	zbrojnoš	k1gMnSc1	zbrojnoš
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Cortés	Cortés	k1gInSc1	Cortés
(	(	kIx(	(
<g/>
zpívající	zpívající	k2eAgMnSc1d1	zpívající
dřevorubec	dřevorubec	k1gMnSc1	dřevorubec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Švec	Švec	k1gMnSc1	Švec
(	(	kIx(	(
<g/>
dvořan	dvořan	k1gMnSc1	dvořan
u	u	k7c2	u
prince	princ	k1gMnSc2	princ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Orlický	orlický	k2eAgMnSc1d1	orlický
(	(	kIx(	(
<g/>
posel	posel	k1gMnSc1	posel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Kavan	Kavan	k1gMnSc1	Kavan
(	(	kIx(	(
<g/>
malíř	malíř	k1gMnSc1	malíř
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Hořánek	Hořánek	k1gMnSc1	Hořánek
(	(	kIx(	(
<g/>
sluha	sluha	k1gMnSc1	sluha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kokejl	Kokejl	k1gMnSc1	Kokejl
(	(	kIx(	(
<g/>
řezník	řezník	k1gMnSc1	řezník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
Bokrová	Bokrová	k1gFnSc1	Bokrová
(	(	kIx(	(
<g/>
košíkářka	košíkářka	k1gFnSc1	košíkářka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Zukal	Zukal	k1gMnSc1	Zukal
(	(	kIx(	(
<g/>
pomocník	pomocník	k1gMnSc1	pomocník
kováře	kovář	k1gMnSc2	kovář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Hájek	Hájek	k1gMnSc1	Hájek
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
zbrojnošů	zbrojnoš	k1gMnPc2	zbrojnoš
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zpěv	zpěv	k1gInSc1	zpěv
==	==	k?	==
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Ráž	Ráž	k1gMnSc1	Ráž
–	–	k?	–
Rozvíjej	rozvíjet	k5eAaImRp2nS	rozvíjet
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
poupátko	poupátko	k1gNnSc4	poupátko
<g/>
,	,	kIx,	,
Kdo	kdo	k3yQnSc1	kdo
má	mít	k5eAaImIp3nS	mít
milou	milá	k1gFnSc4	milá
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
Vítej	vítat	k5eAaImRp2nS	vítat
slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
ranním	ranní	k2eAgInSc6d1	ranní
lese	les	k1gInSc6	les
Už	už	k9	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
uděláno	udělat	k5eAaPmNgNnS	udělat
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hotovo	hotov	k2eAgNnSc1d1	hotovo
</s>
</p>
<p>
<s>
Alena	Alena	k1gFnSc1	Alena
Vránová	Vránová	k1gFnSc1	Vránová
–	–	k?	–
Rozvíjej	rozvíjet	k5eAaImRp2nS	rozvíjet
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
poupátko	poupátko	k1gNnSc4	poupátko
<g/>
,	,	kIx,	,
Vítej	vítat	k5eAaImRp2nS	vítat
slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
ranním	ranní	k2eAgInSc6d1	ranní
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
Už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
uděláno	udělat	k5eAaPmNgNnS	udělat
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hotovo	hotov	k2eAgNnSc1d1	hotovo
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Neumann	Neumann	k1gMnSc1	Neumann
–	–	k?	–
Rozvíjej	rozvíjet	k5eAaImRp2nS	rozvíjet
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
poupátko	poupátko	k1gNnSc4	poupátko
</s>
</p>
<p>
<s>
Kühnův	Kühnův	k2eAgInSc1d1	Kühnův
dětský	dětský	k2eAgInSc1d1	dětský
pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
–	–	k?	–
Pro	pro	k7c4	pro
pár	pár	k4xCyI	pár
stromů	strom	k1gInPc2	strom
statný	statný	k2eAgInSc4d1	statný
les	les	k1gInSc4	les
<g/>
,	,	kIx,	,
Není	být	k5eNaImIp3nS	být
větší	veliký	k2eAgNnSc4d2	veliký
potěšení	potěšení	k1gNnSc4	potěšení
nad	nad	k7c4	nad
naši	náš	k3xOp1gFnSc4	náš
krásnou	krásný	k2eAgFnSc4d1	krásná
zem	zem	k1gFnSc4	zem
<g/>
;	;	kIx,	;
<g/>
,	,	kIx,	,
Pusťte	pustit	k5eAaPmRp2nP	pustit
nás	my	k3xPp1nPc2	my
tou	ten	k3xDgFnSc7	ten
zlatou	zlatý	k2eAgFnSc7d1	zlatá
branou	brána	k1gFnSc7	brána
<g/>
,	,	kIx,	,
Zpívejme	zpívat	k5eAaImRp1nP	zpívat
a	a	k8xC	a
radujme	radovat	k5eAaImRp1nP	radovat
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Cortés	Cortésa	k1gFnPc2	Cortésa
–	–	k?	–
Pro	pro	k7c4	pro
pár	pár	k4xCyI	pár
stromů	strom	k1gInPc2	strom
statný	statný	k2eAgInSc1d1	statný
les	les	k1gInSc1	les
</s>
</p>
<p>
<s>
Luděk	Luděk	k1gMnSc1	Luděk
Mandaus	Mandaus	k1gMnSc1	Mandaus
–	–	k?	–
Kdo	kdo	k3yInSc1	kdo
má	mít	k5eAaImIp3nS	mít
milou	milá	k1gFnSc4	milá
doma	doma	k6eAd1	doma
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Čáp	Čáp	k1gMnSc1	Čáp
–	–	k?	–
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
mladej	mladej	k?	mladej
švec	švec	k1gMnSc1	švec
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Hanus	Hanus	k1gMnSc1	Hanus
–	–	k?	–
Vítej	vítat	k5eAaImRp2nS	vítat
slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
ranním	ranní	k2eAgInSc6d1	ranní
lese	les	k1gInSc6	les
</s>
</p>
<p>
<s>
Sbor	sbor	k1gInSc1	sbor
–	–	k?	–
Skončila	skončit	k5eAaPmAgFnS	skončit
se	se	k3xPyFc4	se
panská	panský	k2eAgFnSc1d1	Panská
pýcha	pýcha	k1gFnSc1	pýcha
</s>
</p>
<p>
<s>
Terezie	Terezie	k1gFnSc1	Terezie
Brzková	Brzková	k1gFnSc1	Brzková
–	–	k?	–
Už	už	k9	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
uděláno	udělat	k5eAaPmNgNnS	udělat
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hotovo	hotov	k2eAgNnSc1d1	hotovo
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kovářík	kovářík	k1gMnSc1	kovářík
–	–	k?	–
Už	už	k9	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
uděláno	udělat	k5eAaPmNgNnS	udělat
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hotovo	hotov	k2eAgNnSc1d1	hotovo
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Kupšovský	Kupšovský	k1gMnSc1	Kupšovský
–	–	k?	–
Vrána	Vrána	k1gMnSc1	Vrána
k	k	k7c3	k
vráně	vrána	k1gFnSc3	vrána
vždycky	vždycky	k6eAd1	vždycky
patří	patřit	k5eAaImIp3nS	patřit
</s>
</p>
<p>
<s>
==	==	k?	==
Výroky	výrok	k1gInPc7	výrok
z	z	k7c2	z
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zlidověly	zlidovět	k5eAaPmAgInP	zlidovět
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Našli	najít	k5eAaPmAgMnP	najít
<g/>
,	,	kIx,	,
rádcové	rádce	k1gMnPc1	rádce
moji	můj	k3xOp1gMnPc1	můj
<g/>
?	?	kIx.	?
</s>
<s>
Našli	najít	k5eAaPmAgMnP	najít
<g/>
,	,	kIx,	,
našli	najít	k5eAaPmAgMnP	najít
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Odvolávám	odvolávat	k5eAaImIp1nS	odvolávat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
jsem	být	k5eAaImIp1nS	být
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
,	,	kIx,	,
slibuji	slibovat	k5eAaImIp1nS	slibovat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
jsem	být	k5eAaImIp1nS	být
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dobrých	dobrý	k2eAgFnPc6d1	dobrá
rukou	ruka	k1gFnPc6	ruka
krále	král	k1gMnSc2	král
Miroslava	Miroslav	k1gMnSc2	Miroslav
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Vítek	Vítek	k1gMnSc1	Vítek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Pamatuj	pamatovat	k5eAaImRp2nS	pamatovat
si	se	k3xPyFc3	se
ševče	ševče	k1gNnSc4	ševče
<g/>
:	:	kIx,	:
Nad	nad	k7c4	nad
nikoho	nikdo	k3yNnSc4	nikdo
se	se	k3xPyFc4	se
nepovyšuj	povyšovat	k5eNaImRp2nS	povyšovat
a	a	k8xC	a
před	před	k7c7	před
nikým	nikdo	k3yNnSc7	nikdo
se	se	k3xPyFc4	se
neponižuj	ponižovat	k5eNaImRp2nS	ponižovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
zajiskřilo	zajiskřit	k5eAaPmAgNnS	zajiskřit
nejen	nejen	k6eAd1	nejen
mezi	mezi	k7c7	mezi
králem	král	k1gMnSc7	král
Miroslavem	Miroslav	k1gMnSc7	Miroslav
a	a	k8xC	a
princeznou	princezna	k1gFnSc7	princezna
Krasomilou	Krasomila	k1gFnSc7	Krasomila
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gMnPc7	jejich
představiteli	představitel	k1gMnPc7	představitel
<g/>
,	,	kIx,	,
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Rážem	Ráž	k1gMnSc7	Ráž
a	a	k8xC	a
Alenou	Alena	k1gFnSc7	Alena
Vránovou	Vránová	k1gFnSc7	Vránová
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
manželkou	manželka	k1gFnSc7	manželka
básníka	básník	k1gMnSc2	básník
Pavla	Pavel	k1gMnSc2	Pavel
Kohouta	Kohout	k1gMnSc2	Kohout
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
a	a	k8xC	a
za	za	k7c2	za
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Ráže	Ráž	k1gMnSc2	Ráž
provdala	provdat	k5eAaPmAgFnS	provdat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hází	házet	k5eAaImIp3nS	házet
princezna	princezna	k1gFnSc1	princezna
míč	míč	k1gInSc4	míč
přes	přes	k7c4	přes
bránu	brána	k1gFnSc4	brána
Miroslavovi	Miroslav	k1gMnSc6	Miroslav
stojí	stát	k5eAaImIp3nS	stát
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
na	na	k7c6	na
Hluboké	Hluboká	k1gFnSc6	Hluboká
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejnavštěvovanějším	navštěvovaný	k2eAgInSc7d3	nejnavštěvovanější
filmem	film	k1gInSc7	film
v	v	k7c6	v
tuzemských	tuzemský	k2eAgNnPc6d1	tuzemské
kinech	kino	k1gNnPc6	kino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
biografech	biograf	k1gMnPc6	biograf
ji	on	k3xPp3gFnSc4	on
podle	podle	k7c2	podle
Unie	unie	k1gFnSc2	unie
filmových	filmový	k2eAgMnPc2d1	filmový
distributorů	distributor	k1gMnPc2	distributor
vidělo	vidět	k5eAaImAgNnS	vidět
8,2	[number]	k4	8,2
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Místa	místo	k1gNnSc2	místo
natáčení	natáčení	k1gNnSc2	natáčení
==	==	k?	==
</s>
</p>
<p>
<s>
Tržiště	tržiště	k1gNnSc1	tržiště
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
–	–	k?	–
Piaristické	piaristický	k2eAgNnSc4d1	Piaristické
náměstí	náměstí	k1gNnSc4	náměstí
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
</s>
</p>
<p>
<s>
Obracení	obracení	k1gNnSc1	obracení
sena	seno	k1gNnSc2	seno
–	–	k?	–
louky	louka	k1gFnSc2	louka
u	u	k7c2	u
Třeboně	Třeboň	k1gFnSc2	Třeboň
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
scének	scénka	k1gFnPc2	scénka
z	z	k7c2	z
útěku	útěk	k1gInSc2	útěk
byla	být	k5eAaImAgNnP	být
natáčena	natáčet	k5eAaImNgNnP	natáčet
u	u	k7c2	u
varhan	varhany	k1gFnPc2	varhany
poblíž	poblíž	k7c2	poblíž
Kamenického	Kamenického	k2eAgInSc2d1	Kamenického
Šenova	Šenov	k1gInSc2	Šenov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
Telči	Telč	k1gFnSc6	Telč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výročí	výročí	k1gNnSc2	výročí
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
65	[number]	k4	65
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
uvedení	uvedení	k1gNnSc2	uvedení
filmu	film	k1gInSc2	film
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Telči	Telč	k1gFnSc6	Telč
sezonní	sezonní	k2eAgFnSc1d1	sezonní
výstava	výstava	k1gFnSc1	výstava
kostýmů	kostým	k1gInPc2	kostým
<g/>
,	,	kIx,	,
rekvizit	rekvizita	k1gFnPc2	rekvizita
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
předmětů	předmět	k1gInPc2	předmět
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
pohádkou	pohádka	k1gFnSc7	pohádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Pyšná	pyšný	k2eAgFnSc1d1	pyšná
princezna	princezna	k1gFnSc1	princezna
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Pyšná	pyšný	k2eAgFnSc1d1	pyšná
princezna	princezna	k1gFnSc1	princezna
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Pyšná	pyšný	k2eAgFnSc1d1	pyšná
princezna	princezna	k1gFnSc1	princezna
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Pyšná	pyšný	k2eAgFnSc1d1	pyšná
princezna	princezna	k1gFnSc1	princezna
na	na	k7c4	na
Filmová	filmový	k2eAgNnPc4d1	filmové
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
