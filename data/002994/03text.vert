<s>
Hydra	hydra	k1gFnSc1	hydra
je	být	k5eAaImIp3nS	být
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
na	na	k7c6	na
nebeském	nebeský	k2eAgInSc6d1	nebeský
rovníku	rovník	k1gInSc6	rovník
(	(	kIx(	(
<g/>
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
ze	z	k7c2	z
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
moderní	moderní	k2eAgFnSc2d1	moderní
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
48	[number]	k4	48
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
zavedených	zavedený	k2eAgFnPc2d1	zavedená
Ptolemaiem	Ptolemaios	k1gMnSc7	Ptolemaios
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
jím	on	k3xPp3gNnSc7	on
prochází	procházet	k5eAaImIp3nS	procházet
ebeský	ebeský	k2eAgInSc1d1	ebeský
rovník	rovník	k1gInSc1	rovník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
alespoň	alespoň	k9	alespoň
z	z	k7c2	z
části	část	k1gFnSc2	část
pozorovatelné	pozorovatelný	k2eAgFnSc2d1	pozorovatelná
kdekoliv	kdekoliv	k6eAd1	kdekoliv
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hydra	hydra	k1gFnSc1	hydra
je	být	k5eAaImIp3nS	být
nejužší	úzký	k2eAgFnSc1d3	nejužší
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejdelší	dlouhý	k2eAgNnPc1d3	nejdelší
souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rektascenzi	rektascenze	k1gFnSc6	rektascenze
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
šest	šest	k4xCc4	šest
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
Hydra	hydra	k1gFnSc1	hydra
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
astronomové	astronom	k1gMnPc1	astronom
John	John	k1gMnSc1	John
Flamsteed	Flamsteed	k1gMnSc1	Flamsteed
a	a	k8xC	a
Johannes	Johannes	k1gMnSc1	Johannes
Hevelius	Hevelius	k1gMnSc1	Hevelius
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgNnPc1	čtyři
menší	malý	k2eAgNnPc1d2	menší
souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
<g/>
:	:	kIx,	:
Sextant	sextant	k1gInSc1	sextant
<g/>
,	,	kIx,	,
Pohár	pohár	k1gInSc1	pohár
<g/>
,	,	kIx,	,
Havran	Havran	k1gMnSc1	Havran
a	a	k8xC	a
dnešní	dnešní	k2eAgFnSc4d1	dnešní
Hydru	hydra	k1gFnSc4	hydra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hvězdných	hvězdný	k2eAgFnPc6d1	hvězdná
mapách	mapa	k1gFnPc6	mapa
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
dokonce	dokonce	k9	dokonce
ještě	ještě	k6eAd1	ještě
jedno	jeden	k4xCgNnSc1	jeden
malé	malý	k2eAgNnSc1d1	malé
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
mezi	mezi	k7c7	mezi
Hydrou	hydra	k1gFnSc7	hydra
<g/>
,	,	kIx,	,
Vývěvou	vývěva	k1gFnSc7	vývěva
a	a	k8xC	a
Kompasem	kompas	k1gInSc7	kompas
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
nazváno	nazvat	k5eAaBmNgNnS	nazvat
Felis	Felis	k1gFnSc7	Felis
-	-	kIx~	-
Kočka	kočka	k1gFnSc1	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
však	však	k9	však
astronomickou	astronomický	k2eAgFnSc7d1	astronomická
veřejností	veřejnost	k1gFnSc7	veřejnost
přijato	přijat	k2eAgNnSc4d1	přijato
a	a	k8xC	a
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asterismus	asterismus	k1gInSc1	asterismus
Hlava	hlava	k1gFnSc1	hlava
Hydry	hydra	k1gFnPc1	hydra
<g/>
,	,	kIx,	,
obrazec	obrazec	k1gInSc1	obrazec
tvořený	tvořený	k2eAgInSc1d1	tvořený
spojnicí	spojnice	k1gFnSc7	spojnice
hvězd	hvězda	k1gFnPc2	hvězda
δ	δ	k?	δ
-	-	kIx~	-
ε	ε	k?	ε
-	-	kIx~	-
ζ	ζ	k?	ζ
-	-	kIx~	-
η	η	k?	η
-	-	kIx~	-
ρ	ρ	k?	ρ
-	-	kIx~	-
σ	σ	k?	σ
Tento	tento	k3xDgInSc1	tento
poměrně	poměrně	k6eAd1	poměrně
výrazný	výrazný	k2eAgInSc1d1	výrazný
obrazec	obrazec	k1gInSc1	obrazec
leží	ležet	k5eAaImIp3nS	ležet
východně	východně	k6eAd1	východně
od	od	k7c2	od
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
Raka	rak	k1gMnSc2	rak
<g/>
.	.	kIx.	.
</s>
<s>
Nejjasnější	jasný	k2eAgNnSc1d3	nejjasnější
(	(	kIx(	(
<g/>
prakticky	prakticky	k6eAd1	prakticky
jedinou	jediný	k2eAgFnSc7d1	jediná
jasnou	jasný	k2eAgFnSc7d1	jasná
<g/>
)	)	kIx)	)
hvězdou	hvězda	k1gFnSc7	hvězda
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
je	být	k5eAaImIp3nS	být
α	α	k?	α
s	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Alphard	Alpharda	k1gFnPc2	Alpharda
(	(	kIx(	(
<g/>
Alfa	alfa	k1gFnSc1	alfa
Hydrae	Hydra	k1gFnSc2	Hydra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hydře	hydra	k1gFnSc6	hydra
sa	sa	k?	sa
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgInPc1	tři
objekty	objekt	k1gInPc1	objekt
Messierova	Messierův	k2eAgInSc2d1	Messierův
katalogu	katalog	k1gInSc2	katalog
<g/>
:	:	kIx,	:
M	M	kA	M
48	[number]	k4	48
<g/>
,	,	kIx,	,
M	M	kA	M
68	[number]	k4	68
a	a	k8xC	a
M	M	kA	M
83	[number]	k4	83
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Alphardem	Alphard	k1gInSc7	Alphard
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
silný	silný	k2eAgInSc1d1	silný
rádiový	rádiový	k2eAgInSc1d1	rádiový
zdroj	zdroj	k1gInSc1	zdroj
Hydra	hydra	k1gFnSc1	hydra
A.	A.	kA	A.
M	M	kA	M
48	[number]	k4	48
-	-	kIx~	-
otevřená	otevřený	k2eAgFnSc1d1	otevřená
hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
M	M	kA	M
68	[number]	k4	68
-	-	kIx~	-
kulová	kulový	k2eAgFnSc1d1	kulová
hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
M	M	kA	M
83	[number]	k4	83
-	-	kIx~	-
galaxie	galaxie	k1gFnSc2	galaxie
</s>
