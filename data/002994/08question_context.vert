<s>
Hydra	hydra	k1gFnSc1	hydra
je	být	k5eAaImIp3nS	být
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
na	na	k7c6	na
nebeském	nebeský	k2eAgInSc6d1	nebeský
rovníku	rovník	k1gInSc6	rovník
(	(	kIx(	(
<g/>
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
ze	z	k7c2	z
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
moderní	moderní	k2eAgFnSc2d1	moderní
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
.	.	kIx.	.
</s>

