<s>
Argonauté	argonaut	k1gMnPc1	argonaut
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Α	Α	k?	Α
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Argonautae	Argonauta	k1gFnSc2	Argonauta
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
plavili	plavit	k5eAaImAgMnP	plavit
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Argó	Argó	k1gFnSc2	Argó
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Iásóna	Iásón	k1gMnSc2	Iásón
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
báji	báj	k1gFnSc6	báj
o	o	k7c6	o
Zlatém	zlatý	k2eAgNnSc6d1	Zlaté
rounu	rouno	k1gNnSc6	rouno
<g/>
.	.	kIx.	.
</s>
