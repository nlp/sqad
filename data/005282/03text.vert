<s>
Argonauté	argonaut	k1gMnPc1	argonaut
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Α	Α	k?	Α
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Argonautae	Argonauta	k1gFnSc2	Argonauta
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
plavili	plavit	k5eAaImAgMnP	plavit
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Argó	Argó	k1gFnSc2	Argó
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Iásóna	Iásón	k1gMnSc2	Iásón
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
báji	báj	k1gFnSc6	báj
o	o	k7c6	o
Zlatém	zlatý	k2eAgNnSc6d1	Zlaté
rounu	rouno	k1gNnSc6	rouno
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
plavbu	plavba	k1gFnSc4	plavba
z	z	k7c2	z
Iólku	Iólk	k1gInSc2	Iólk
do	do	k7c2	do
Kolchidy	Kolchida	k1gFnSc2	Kolchida
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
čekalo	čekat	k5eAaImAgNnS	čekat
na	na	k7c4	na
posádku	posádka	k1gFnSc4	posádka
mnoho	mnoho	k4c4	mnoho
nebezpečí	nebezpeč	k1gFnPc2wB	nebezpeč
a	a	k8xC	a
úkladů	úklad	k1gInPc2	úklad
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnPc1	vláda
v	v	k7c6	v
Iólku	Iólk	k1gInSc6	Iólk
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
neprávem	neprávo	k1gNnSc7	neprávo
záludný	záludný	k2eAgMnSc1d1	záludný
a	a	k8xC	a
krutý	krutý	k2eAgMnSc1d1	krutý
král	král	k1gMnSc1	král
Peliás	Peliása	k1gFnPc2	Peliása
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
právoplatný	právoplatný	k2eAgMnSc1d1	právoplatný
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
Iásón	Iásón	k1gMnSc1	Iásón
domáhal	domáhat	k5eAaImAgMnS	domáhat
jejího	její	k3xOp3gMnSc4	její
vrácení	vrácený	k2eAgMnPc1d1	vrácený
<g/>
,	,	kIx,	,
stanovil	stanovit	k5eAaPmAgMnS	stanovit
král	král	k1gMnSc1	král
Peliás	Peliás	k1gInSc4	Peliás
podmínku	podmínka	k1gFnSc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
vládu	vláda	k1gFnSc4	vláda
předá	předat	k5eAaPmIp3nS	předat
po	po	k7c4	po
získání	získání	k1gNnSc4	získání
zlatého	zlatý	k2eAgNnSc2d1	Zlaté
rouna	rouno	k1gNnSc2	rouno
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
Kolchidy	Kolchida	k1gFnSc2	Kolchida
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
zlatý	zlatý	k2eAgInSc1d1	zlatý
beran	beran	k1gInSc1	beran
přenesl	přenést	k5eAaPmAgInS	přenést
ohroženého	ohrožený	k2eAgNnSc2d1	ohrožené
Frixa	Frixum	k1gNnSc2	Frixum
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
zádech	záda	k1gNnPc6	záda
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
ke	k	k7c3	k
králi	král	k1gMnSc3	král
Aiétovi	Aiét	k1gMnSc3	Aiét
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oběti	oběť	k1gFnSc6	oběť
bohům	bůh	k1gMnPc3	bůh
daroval	darovat	k5eAaPmAgInS	darovat
Frixos	Frixos	k1gInSc1	Frixos
rouno	rouno	k1gNnSc4	rouno
králi	král	k1gMnSc3	král
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
ho	on	k3xPp3gMnSc4	on
nechal	nechat	k5eAaPmAgInS	nechat
střežit	střežit	k5eAaImF	střežit
hrozným	hrozný	k2eAgInSc7d1	hrozný
drakem	drak	k1gInSc7	drak
<g/>
.	.	kIx.	.
</s>
<s>
Zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
záruku	záruka	k1gFnSc4	záruka
svého	svůj	k3xOyFgNnSc2	svůj
vládnutí	vládnutí	k1gNnSc2	vládnutí
<g/>
.	.	kIx.	.
</s>
<s>
Iásón	Iásón	k1gMnSc1	Iásón
úkol	úkol	k1gInSc4	úkol
přinést	přinést	k5eAaPmF	přinést
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c4	na
dalekou	daleký	k2eAgFnSc4d1	daleká
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
neznámých	známý	k2eNgFnPc2d1	neznámá
končin	končina	k1gFnPc2	končina
postavit	postavit	k5eAaPmF	postavit
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
sehnal	sehnat	k5eAaPmAgInS	sehnat
statečnou	statečný	k2eAgFnSc4d1	statečná
posádku	posádka	k1gFnSc4	posádka
a	a	k8xC	a
vyplul	vyplout	k5eAaPmAgMnS	vyplout
do	do	k7c2	do
nebezpečenství	nebezpečenství	k1gNnSc2	nebezpečenství
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
nikdo	nikdo	k3yNnSc1	nikdo
nevěřil	věřit	k5eNaImAgMnS	věřit
na	na	k7c4	na
návrat	návrat	k1gInSc4	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
obrovská	obrovský	k2eAgFnSc1d1	obrovská
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
předtím	předtím	k6eAd1	předtím
nikdo	nikdo	k3yNnSc1	nikdo
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
padesát	padesát	k4xCc4	padesát
vesel	veslo	k1gNnPc2	veslo
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
veliká	veliký	k2eAgFnSc1d1	veliká
a	a	k8xC	a
pohodlná	pohodlný	k2eAgFnSc1d1	pohodlná
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
tak	tak	k6eAd1	tak
lehká	lehký	k2eAgFnSc1d1	lehká
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
plavci	plavec	k1gMnPc1	plavec
mohli	moct	k5eAaImAgMnP	moct
nést	nést	k5eAaImF	nést
na	na	k7c6	na
ramenou	rameno	k1gNnPc6	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Stavěl	stavět	k5eAaImAgMnS	stavět
ji	on	k3xPp3gFnSc4	on
vynikající	vynikající	k2eAgMnSc1d1	vynikající
mistr	mistr	k1gMnSc1	mistr
Argos	Argos	k1gMnSc1	Argos
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Argó	Argó	k1gFnSc1	Argó
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
Rychlá	rychlý	k2eAgFnSc1d1	rychlá
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
stavbou	stavba	k1gFnSc7	stavba
bděla	bdít	k5eAaImAgFnS	bdít
bohyně	bohyně	k1gFnSc1	bohyně
Héra	Héra	k1gFnSc1	Héra
<g/>
,	,	kIx,	,
také	také	k9	také
Athéna	Athéna	k1gFnSc1	Athéna
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnSc1	bohyně
všech	všecek	k3xTgNnPc2	všecek
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
všeho	všecek	k3xTgNnSc2	všecek
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
šťastnou	šťastný	k2eAgFnSc4d1	šťastná
plavbu	plavba	k1gFnSc4	plavba
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
lodního	lodní	k2eAgInSc2d1	lodní
kýlu	kýl	k1gInSc2	kýl
vložen	vložit	k5eAaPmNgInS	vložit
kus	kus	k1gInSc1	kus
dřeva	dřevo	k1gNnSc2	dřevo
z	z	k7c2	z
posvátného	posvátný	k2eAgInSc2d1	posvátný
dubu	dub	k1gInSc2	dub
z	z	k7c2	z
věštírny	věštírna	k1gFnSc2	věštírna
<g/>
.	.	kIx.	.
</s>
<s>
Posádku	posádka	k1gFnSc4	posádka
tvořili	tvořit	k5eAaImAgMnP	tvořit
význační	význačný	k2eAgMnPc1d1	význačný
<g/>
,	,	kIx,	,
udatní	udatný	k2eAgMnPc1d1	udatný
a	a	k8xC	a
slavní	slavný	k2eAgMnPc1d1	slavný
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jich	on	k3xPp3gMnPc2	on
celkem	celkem	k6eAd1	celkem
padesát	padesát	k4xCc1	padesát
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
tito	tento	k3xDgMnPc1	tento
<g/>
:	:	kIx,	:
Héraklés	Héraklésa	k1gFnPc2	Héraklésa
a	a	k8xC	a
Ankaios	Ankaiosa	k1gFnPc2	Ankaiosa
–	–	k?	–
siláci	silák	k1gMnPc1	silák
<g/>
;	;	kIx,	;
Kastór	Kastór	k1gInSc1	Kastór
a	a	k8xC	a
Polydeukés	Polydeukés	k1gInSc1	Polydeukés
ze	z	k7c2	z
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
;	;	kIx,	;
Orfeus	Orfeus	k1gMnSc1	Orfeus
–	–	k?	–
bájný	bájný	k2eAgMnSc1d1	bájný
pěvec	pěvec	k1gMnSc1	pěvec
a	a	k8xC	a
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
;	;	kIx,	;
Meleagros	Meleagrosa	k1gFnPc2	Meleagrosa
–	–	k?	–
kalydónský	kalydónský	k2eAgMnSc1d1	kalydónský
král	král	k1gMnSc1	král
<g/>
;	;	kIx,	;
Théseus	Théseus	k1gMnSc1	Théseus
–	–	k?	–
chlouba	chlouba	k1gFnSc1	chlouba
Athén	Athéna	k1gFnPc2	Athéna
<g/>
;	;	kIx,	;
Akastos	Akastos	k1gMnSc1	Akastos
–	–	k?	–
Peliův	Peliův	k2eAgMnSc1d1	Peliův
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
otce	otka	k1gFnSc3	otka
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Tífys	Tífys	k1gInSc1	Tífys
–	–	k?	–
kormidelník	kormidelník	k1gMnSc1	kormidelník
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
krále	král	k1gMnSc2	král
Lykose	Lykosa	k1gFnSc3	Lykosa
<g/>
;	;	kIx,	;
Idmón	Idmón	k1gMnSc1	Idmón
–	–	k?	–
věštec	věštec	k1gMnSc1	věštec
<g/>
,	,	kIx,	,
zahynul	zahynout	k5eAaPmAgMnS	zahynout
po	po	k7c4	po
napadení	napadení	k1gNnSc4	napadení
divokým	divoký	k2eAgMnSc7d1	divoký
kancem	kanec	k1gMnSc7	kanec
<g/>
;	;	kIx,	;
Mopsos	Mopsos	k1gMnSc1	Mopsos
–	–	k?	–
věštec	věštec	k1gMnSc1	věštec
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c4	po
šlápnutí	šlápnutí	k1gNnSc4	šlápnutí
na	na	k7c4	na
jedovatého	jedovatý	k2eAgMnSc4d1	jedovatý
hada	had	k1gMnSc4	had
<g/>
;	;	kIx,	;
Hylás	Hylás	k1gInSc1	Hylás
–	–	k?	–
přítel	přítel	k1gMnSc1	přítel
Héraklův	Héraklův	k2eAgMnSc1d1	Héraklův
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc1d1	vodní
víla	víla	k1gFnSc1	víla
ho	on	k3xPp3gMnSc4	on
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
pod	pod	k7c4	pod
hladinu	hladina	k1gFnSc4	hladina
<g/>
;	;	kIx,	;
Polyfémos	Polyfémos	k1gInSc1	Polyfémos
–	–	k?	–
Héraklův	Héraklův	k2eAgInSc1d1	Héraklův
druh	druh	k1gInSc1	druh
<g/>
;	;	kIx,	;
Kalais	Kalais	k1gInSc1	Kalais
a	a	k8xC	a
Zétés	Zétés	k1gInSc1	Zétés
–	–	k?	–
okřídlení	okřídlený	k2eAgMnPc1d1	okřídlený
synové	syn	k1gMnPc1	syn
boha	bůh	k1gMnSc4	bůh
západního	západní	k2eAgInSc2d1	západní
větru	vítr	k1gInSc2	vítr
Borea	Borea	k1gFnSc1	Borea
<g/>
;	;	kIx,	;
Idás	Idás	k1gInSc1	Idás
–	–	k?	–
probodl	probodnout	k5eAaPmAgInS	probodnout
divokého	divoký	k2eAgMnSc4d1	divoký
kance	kanec	k1gMnSc4	kanec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zabil	zabít	k5eAaPmAgMnS	zabít
Idmóse	Idmóse	k1gFnPc4	Idmóse
<g/>
;	;	kIx,	;
Oileus	Oileus	k1gMnSc1	Oileus
–	–	k?	–
lokridský	lokridský	k2eAgMnSc1d1	lokridský
král	král	k1gMnSc1	král
<g/>
;	;	kIx,	;
Peirithoos	Peirithoos	k1gMnSc1	Peirithoos
–	–	k?	–
lapithský	lapithský	k1gMnSc1	lapithský
král	král	k1gMnSc1	král
<g/>
;	;	kIx,	;
Péleus	Péleus	k1gMnSc1	Péleus
–	–	k?	–
fthíjský	fthíjský	k2eAgMnSc1d1	fthíjský
král	král	k1gMnSc1	král
<g/>
;	;	kIx,	;
Telamón	Telamón	k1gMnSc1	Telamón
–	–	k?	–
salamínský	salamínský	k2eAgMnSc1d1	salamínský
král	král	k1gMnSc1	král
<g/>
;	;	kIx,	;
Admétos	Admétos	k1gMnSc1	Admétos
<g/>
,	,	kIx,	,
Týdeus	Týdeus	k1gMnSc1	Týdeus
<g/>
,	,	kIx,	,
Lynkeus	Lynkeus	k1gMnSc1	Lynkeus
<g/>
,	,	kIx,	,
Eufémos	Eufémos	k1gMnSc1	Eufémos
<g/>
,	,	kIx,	,
Oileus	Oileus	k1gMnSc1	Oileus
–	–	k?	–
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
;	;	kIx,	;
Asklépios	Asklépios	k1gMnSc1	Asklépios
–	–	k?	–
lodní	lodní	k2eAgMnSc1d1	lodní
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
bůh	bůh	k1gMnSc1	bůh
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Kolchidy	Kolchida	k1gFnSc2	Kolchida
se	se	k3xPyFc4	se
přidali	přidat	k5eAaPmAgMnP	přidat
čtyři	čtyři	k4xCgMnPc1	čtyři
synové	syn	k1gMnPc1	syn
Frixovi	Frixův	k2eAgMnPc1d1	Frixův
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnPc1d3	nejstarší
jménem	jméno	k1gNnSc7	jméno
Argos	Argos	k1gInSc1	Argos
a	a	k8xC	a
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
Frontis	Frontis	k1gFnSc1	Frontis
<g/>
.	.	kIx.	.
</s>
<s>
Obětovali	obětovat	k5eAaBmAgMnP	obětovat
bohům	bůh	k1gMnPc3	bůh
<g/>
,	,	kIx,	,
prosili	prosit	k5eAaImAgMnP	prosit
Apollóna	Apollón	k1gMnSc4	Apollón
o	o	k7c4	o
zdar	zdar	k1gInSc4	zdar
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
šťastný	šťastný	k2eAgInSc4d1	šťastný
návrat	návrat	k1gInSc4	návrat
<g/>
,	,	kIx,	,
za	za	k7c4	za
příznivý	příznivý	k2eAgInSc4d1	příznivý
vítr	vítr	k1gInSc4	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Věštec	věštec	k1gMnSc1	věštec
Idmós	Idmós	k1gInSc1	Idmós
posádce	posádka	k1gFnSc3	posádka
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
co	co	k9	co
mu	on	k3xPp3gMnSc3	on
zjevil	zjevit	k5eAaPmAgMnS	zjevit
Apollón	Apollón	k1gMnSc1	Apollón
<g/>
:	:	kIx,	:
všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nP	vrátit
<g/>
,	,	kIx,	,
i	i	k8xC	i
se	s	k7c7	s
zlatým	zlatý	k2eAgNnSc7d1	Zlaté
rounem	rouno	k1gNnSc7	rouno
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
komu	kdo	k3yRnSc3	kdo
je	být	k5eAaImIp3nS	být
souzeno	souzen	k2eAgNnSc1d1	souzeno
zahynout	zahynout	k5eAaPmF	zahynout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
věštec	věštec	k1gMnSc1	věštec
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Noc	noc	k1gFnSc1	noc
před	před	k7c7	před
vyplutím	vyplutí	k1gNnSc7	vyplutí
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
napjati	napjat	k2eAgMnPc1d1	napjat
očekáváním	očekávání	k1gNnSc7	očekávání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
obavami	obava	k1gFnPc7	obava
<g/>
.	.	kIx.	.
</s>
<s>
Mírnil	mírnit	k5eAaImAgMnS	mírnit
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
zpěvem	zpěv	k1gInSc7	zpěv
Orfeus	Orfeus	k1gMnSc1	Orfeus
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
bylo	být	k5eAaImAgNnS	být
slunečné	slunečný	k2eAgNnSc1d1	slunečné
<g/>
,	,	kIx,	,
vál	vát	k5eAaImAgInS	vát
mírný	mírný	k2eAgInSc1d1	mírný
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Skvělá	skvělý	k2eAgFnSc1d1	skvělá
loď	loď	k1gFnSc1	loď
Argó	Argó	k1gFnSc1	Argó
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dalekou	daleký	k2eAgFnSc4d1	daleká
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
neznámých	známý	k2eNgFnPc2d1	neznámá
krajin	krajina	k1gFnPc2	krajina
<g/>
,	,	kIx,	,
vyprovázena	vyprovázen	k2eAgFnSc1d1	vyprovázen
množstvím	množství	k1gNnSc7	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
bájných	bájný	k2eAgFnPc2d1	bájná
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
den	den	k1gInSc1	den
plavby	plavba	k1gFnSc2	plavba
byl	být	k5eAaImAgInS	být
příznivý	příznivý	k2eAgInSc1d1	příznivý
<g/>
,	,	kIx,	,
k	k	k7c3	k
večeru	večer	k1gInSc3	večer
přistáli	přistát	k5eAaImAgMnP	přistát
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
u	u	k7c2	u
mohyly	mohyla	k1gFnSc2	mohyla
Dolopse	Dolops	k1gMnSc2	Dolops
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
boha	bůh	k1gMnSc2	bůh
Herma	Hermes	k1gMnSc2	Hermes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
památku	památka	k1gFnSc4	památka
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
nazváno	nazván	k2eAgNnSc1d1	nazváno
Argos	Argos	k1gInSc4	Argos
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
však	však	k9	však
vál	vát	k5eAaImAgInS	vát
nepříznivý	příznivý	k2eNgInSc1d1	nepříznivý
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
i	i	k8xC	i
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Plavci	plavec	k1gMnPc1	plavec
poslouchali	poslouchat	k5eAaImAgMnP	poslouchat
zpěv	zpěv	k1gInSc4	zpěv
Orfeův	Orfeův	k2eAgInSc4d1	Orfeův
<g/>
,	,	kIx,	,
vyprávěli	vyprávět	k5eAaImAgMnP	vyprávět
historky	historka	k1gFnPc4	historka
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
seznamovali	seznamovat	k5eAaImAgMnP	seznamovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
dny	den	k1gInPc4	den
plavba	plavba	k1gFnSc1	plavba
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
,	,	kIx,	,
viděli	vidět	k5eAaImAgMnP	vidět
nebetyčný	nebetyčný	k2eAgInSc4d1	nebetyčný
Olymp	Olymp	k1gInSc4	Olymp
<g/>
,	,	kIx,	,
obepluli	obeplout	k5eAaPmAgMnP	obeplout
ostrov	ostrov	k1gInSc4	ostrov
Athos	Athos	k1gInSc1	Athos
a	a	k8xC	a
mířili	mířit	k5eAaImAgMnP	mířit
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
Lémnos	Lémnosa	k1gFnPc2	Lémnosa
<g/>
.	.	kIx.	.
</s>
<s>
Věštec	věštec	k1gMnSc1	věštec
lodníkům	lodník	k1gMnPc3	lodník
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
nežije	žít	k5eNaImIp3nS	žít
jediný	jediný	k2eAgMnSc1d1	jediný
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
nedostatečně	dostatečně	k6eNd1	dostatečně
uctívaly	uctívat	k5eAaImAgFnP	uctívat
Afroditu	Afrodita	k1gFnSc4	Afrodita
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
je	on	k3xPp3gFnPc4	on
bohyně	bohyně	k1gFnPc4	bohyně
potrestala	potrestat	k5eAaPmAgFnS	potrestat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnPc1	jejich
mužové	muž	k1gMnPc1	muž
o	o	k7c4	o
ně	on	k3xPp3gInPc4	on
ztratili	ztratit	k5eAaPmAgMnP	ztratit
zájem	zájem	k1gInSc4	zájem
a	a	k8xC	a
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
si	se	k3xPyFc3	se
ženy	žena	k1gFnPc4	žena
z	z	k7c2	z
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
,	,	kIx,	,
přiváželi	přivážet	k5eAaImAgMnP	přivážet
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
za	za	k7c2	za
manželky	manželka	k1gFnSc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
hněv	hněv	k1gInSc4	hněv
a	a	k8xC	a
nenávist	nenávist	k1gFnSc4	nenávist
a	a	k8xC	a
ženy	žena	k1gFnPc4	žena
z	z	k7c2	z
Lémnu	Lémn	k1gInSc2	Lémn
pobily	pobít	k5eAaPmAgFnP	pobít
všechny	všechen	k3xTgMnPc4	všechen
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
se	se	k3xPyFc4	se
zachránil	zachránit	k5eAaPmAgMnS	zachránit
král	král	k1gMnSc1	král
Thoás	Thoás	k1gInSc4	Thoás
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
dcera	dcera	k1gFnSc1	dcera
Hypsipylé	Hypsipylý	k2eAgNnSc4d1	Hypsipylý
ukryla	ukrýt	k5eAaPmAgFnS	ukrýt
v	v	k7c4	v
truhlici	truhlice	k1gFnSc4	truhlice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
spustila	spustit	k5eAaPmAgFnS	spustit
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
ženy	žena	k1gFnPc1	žena
poznaly	poznat	k5eAaPmAgFnP	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
život	život	k1gInSc4	život
ještě	ještě	k6eAd1	ještě
těžší	těžký	k2eAgMnSc1d2	těžší
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
loď	loď	k1gFnSc1	loď
s	s	k7c7	s
Argonauty	argonaut	k1gMnPc7	argonaut
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc4	žena
je	on	k3xPp3gMnPc4	on
pozvaly	pozvat	k5eAaPmAgInP	pozvat
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
obletovaly	obletovat	k5eAaImAgInP	obletovat
je	on	k3xPp3gMnPc4	on
a	a	k8xC	a
prosily	prosít	k5eAaPmAgFnP	prosít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
zůstali	zůstat	k5eAaPmAgMnP	zůstat
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
Hypsipylé	Hypsipylý	k2eAgFnSc2d1	Hypsipylý
nabízela	nabízet	k5eAaImAgFnS	nabízet
předání	předání	k1gNnSc2	předání
moci	moc	k1gFnSc2	moc
Iásonovi	Iáson	k1gMnSc3	Iáson
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k9	tak
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
zdrželi	zdržet	k5eAaPmAgMnP	zdržet
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
Héraklovo	Héraklův	k2eAgNnSc4d1	Héraklův
zvedli	zvednout	k5eAaPmAgMnP	zvednout
kotvy	kotva	k1gFnPc4	kotva
a	a	k8xC	a
odplouvali	odplouvat	k5eAaImAgMnP	odplouvat
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
plula	plout	k5eAaImAgFnS	plout
Thráckým	thrácký	k2eAgNnSc7d1	thrácké
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
minula	minout	k5eAaImAgFnS	minout
ostrov	ostrov	k1gInSc4	ostrov
Imbros	Imbrosa	k1gFnPc2	Imbrosa
a	a	k8xC	a
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
se	se	k3xPyFc4	se
ke	k	k7c3	k
slavné	slavný	k2eAgFnSc3d1	slavná
Tróji	Trója	k1gFnSc3	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Plula	plout	k5eAaImAgFnS	plout
po	po	k7c6	po
hrozivých	hrozivý	k2eAgFnPc6d1	hrozivá
vlnách	vlna	k1gFnPc6	vlna
Helléspontu	Helléspont	k1gInSc2	Helléspont
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
utopila	utopit	k5eAaPmAgFnS	utopit
malá	malá	k1gFnSc1	malá
Hellé	Hellý	k2eAgNnSc4d1	Hellý
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
z	z	k7c2	z
hřbetu	hřbet	k1gInSc2	hřbet
zlatého	zlatý	k2eAgMnSc4d1	zlatý
berana	beran	k1gMnSc4	beran
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
bratra	bratr	k1gMnSc2	bratr
Frixa	Frixus	k1gMnSc2	Frixus
unášel	unášet	k5eAaImAgMnS	unášet
do	do	k7c2	do
Kolchidy	Kolchida	k1gFnSc2	Kolchida
pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
zlé	zlý	k2eAgFnSc2d1	zlá
macechy	macecha	k1gFnSc2	macecha
Ínó	Ínó	k1gFnSc2	Ínó
<g/>
.	.	kIx.	.
</s>
<s>
Argonauté	argonaut	k1gMnPc1	argonaut
vzpomněli	vzpomnít	k5eAaPmAgMnP	vzpomnít
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
také	také	k6eAd1	také
příčinou	příčina	k1gFnSc7	příčina
jejich	jejich	k3xOp3gFnSc2	jejich
cesty	cesta	k1gFnSc2	cesta
pro	pro	k7c4	pro
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
<g/>
.	.	kIx.	.
</s>
<s>
Připluli	připlout	k5eAaPmAgMnP	připlout
k	k	k7c3	k
ostrovním	ostrovní	k2eAgFnPc3d1	ostrovní
skalám	skála	k1gFnPc3	skála
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žili	žít	k5eAaImAgMnP	žít
divocí	divoký	k2eAgMnPc1d1	divoký
obři	obr	k1gMnPc1	obr
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
páry	pár	k1gInPc7	pár
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
pod	pod	k7c7	pod
horou	hora	k1gFnSc7	hora
žili	žít	k5eAaImAgMnP	žít
Dolionové	Dolionový	k2eAgFnPc1d1	Dolionový
s	s	k7c7	s
králem	král	k1gMnSc7	král
Kyzikem	Kyzik	k1gMnSc7	Kyzik
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
obři	obr	k1gMnPc1	obr
zpozorovali	zpozorovat	k5eAaPmAgMnP	zpozorovat
kotvící	kotvící	k2eAgFnSc4d1	kotvící
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
vrhali	vrhat	k5eAaImAgMnP	vrhat
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
obrovské	obrovský	k2eAgInPc4d1	obrovský
kameny	kámen	k1gInPc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgInS	postavit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
Héraklés	Héraklés	k1gInSc1	Héraklés
a	a	k8xC	a
nejprve	nejprve	k6eAd1	nejprve
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
i	i	k9	i
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
druhy	druh	k1gInPc7	druh
pobili	pobít	k5eAaPmAgMnP	pobít
obry	obr	k1gMnPc7	obr
luky	luk	k1gInPc4	luk
a	a	k8xC	a
šípy	šíp	k1gInPc4	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Dolionové	Dolionový	k2eAgNnSc1d1	Dolionový
se	se	k3xPyFc4	se
radovali	radovat	k5eAaImAgMnP	radovat
z	z	k7c2	z
pobití	pobití	k1gNnSc2	pobití
strašných	strašný	k2eAgMnPc2d1	strašný
sousedů	soused	k1gMnPc2	soused
<g/>
,	,	kIx,	,
přinášeli	přinášet	k5eAaImAgMnP	přinášet
plavcům	plavec	k1gMnPc3	plavec
bohaté	bohatý	k2eAgMnPc4d1	bohatý
dary	dar	k1gInPc4	dar
a	a	k8xC	a
pohoštění	pohoštění	k1gNnSc4	pohoštění
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
Argonauté	argonaut	k1gMnPc1	argonaut
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
silná	silný	k2eAgFnSc1d1	silná
bouře	bouře	k1gFnSc1	bouře
a	a	k8xC	a
vítr	vítr	k1gInSc1	vítr
je	on	k3xPp3gMnPc4	on
zahnaly	zahnat	k5eAaPmAgFnP	zahnat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noční	noční	k2eAgFnSc6d1	noční
tmě	tma	k1gFnSc6	tma
se	se	k3xPyFc4	se
Dolionové	Dolionový	k2eAgNnSc1d1	Dolionový
a	a	k8xC	a
Argonauté	argonaut	k1gMnPc1	argonaut
nepoznali	poznat	k5eNaPmAgMnP	poznat
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
Dolionů	Dolion	k1gInPc2	Dolion
padlo	padnout	k5eAaPmAgNnS	padnout
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
král	král	k1gMnSc1	král
Kyzikos	Kyzikos	k1gInSc4	Kyzikos
šípem	šíp	k1gInSc7	šíp
Iásonovým	Iásonův	k2eAgInSc7d1	Iásonův
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
denního	denní	k2eAgNnSc2d1	denní
světla	světlo	k1gNnSc2	světlo
všichni	všechen	k3xTgMnPc1	všechen
zděšeně	zděšeně	k6eAd1	zděšeně
zjišťovali	zjišťovat	k5eAaImAgMnP	zjišťovat
škody	škoda	k1gFnPc4	škoda
a	a	k8xC	a
společně	společně	k6eAd1	společně
truchlili	truchlit	k5eAaImAgMnP	truchlit
dalších	další	k2eAgInPc2d1	další
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
loď	loď	k1gFnSc1	loď
nemohla	moct	k5eNaImAgFnS	moct
pro	pro	k7c4	pro
nepřízeň	nepřízeň	k1gFnSc4	nepřízeň
počasí	počasí	k1gNnSc2	počasí
odplout	odplout	k5eAaPmF	odplout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
počasí	počasí	k1gNnSc1	počasí
dovolilo	dovolit	k5eAaPmAgNnS	dovolit
<g/>
,	,	kIx,	,
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
plavbě	plavba	k1gFnSc6	plavba
loď	loď	k1gFnSc1	loď
přistála	přistát	k5eAaPmAgFnS	přistát
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
Mýsie	Mýsie	k1gFnSc2	Mýsie
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
Lýdií	Lýdia	k1gFnSc7	Lýdia
a	a	k8xC	a
Frygií	Frygie	k1gFnSc7	Frygie
<g/>
)	)	kIx)	)
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
pohostinně	pohostinně	k6eAd1	pohostinně
přijati	přijmout	k5eAaPmNgMnP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
Héraklés	Héraklésa	k1gFnPc2	Héraklésa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
nové	nový	k2eAgNnSc4d1	nové
veslo	veslo	k1gNnSc4	veslo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
oblíbenec	oblíbenec	k1gMnSc1	oblíbenec
Hylás	Hylása	k1gFnPc2	Hylása
šel	jít	k5eAaImAgMnS	jít
pro	pro	k7c4	pro
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
tolik	tolik	k6eAd1	tolik
se	se	k3xPyFc4	se
zalíbil	zalíbit	k5eAaPmAgMnS	zalíbit
jedné	jeden	k4xCgFnSc6	jeden
vodní	vodní	k2eAgFnSc6d1	vodní
víle	víla	k1gFnSc6	víla
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
k	k	k7c3	k
sobě	se	k3xPyFc3	se
pod	pod	k7c4	pod
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
druhové	druh	k1gMnPc1	druh
ho	on	k3xPp3gNnSc4	on
marně	marně	k6eAd1	marně
hledali	hledat	k5eAaImAgMnP	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
o	o	k7c6	o
ztrátě	ztráta	k1gFnSc6	ztráta
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
Héraklés	Héraklés	k1gInSc4	Héraklés
<g/>
,	,	kIx,	,
volal	volat	k5eAaImAgMnS	volat
a	a	k8xC	a
hledal	hledat	k5eAaImAgMnS	hledat
ho	on	k3xPp3gNnSc4	on
ještě	ještě	k9	ještě
celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechno	všechen	k3xTgNnSc1	všechen
marně	marně	k6eAd1	marně
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
rychle	rychle	k6eAd1	rychle
opustili	opustit	k5eAaPmAgMnP	opustit
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
daleko	daleko	k6eAd1	daleko
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
Iásón	Iásón	k1gMnSc1	Iásón
s	s	k7c7	s
hrůzou	hrůza	k1gFnSc7	hrůza
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
chybí	chybit	k5eAaPmIp3nP	chybit
tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
-	-	kIx~	-
Héraklés	Héraklés	k1gInSc1	Héraklés
<g/>
,	,	kIx,	,
Polyfémos	Polyfémos	k1gInSc1	Polyfémos
a	a	k8xC	a
Hylás	Hylás	k1gInSc1	Hylás
<g/>
.	.	kIx.	.
</s>
<s>
Dočkal	dočkat	k5eAaPmAgMnS	dočkat
se	se	k3xPyFc4	se
trpkých	trpký	k2eAgFnPc2d1	trpká
výčitek	výčitka	k1gFnPc2	výčitka
Telamónových	Telamónový	k2eAgFnPc2d1	Telamónový
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
vrátit	vrátit	k5eAaPmF	vrátit
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
se	se	k3xPyFc4	se
vynořil	vynořit	k5eAaPmAgMnS	vynořit
mořský	mořský	k2eAgMnSc1d1	mořský
bůh	bůh	k1gMnSc1	bůh
Glaukos	Glaukos	k1gMnSc1	Glaukos
a	a	k8xC	a
domluvil	domluvit	k5eAaPmAgMnS	domluvit
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vše	všechen	k3xTgNnSc4	všechen
nechali	nechat	k5eAaPmAgMnP	nechat
na	na	k7c4	na
vůli	vůle	k1gFnSc4	vůle
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
usoudili	usoudit	k5eAaPmAgMnP	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hérakla	Hérakles	k1gMnSc2	Hérakles
čekají	čekat	k5eAaImIp3nP	čekat
jiné	jiný	k2eAgInPc4d1	jiný
velké	velký	k2eAgInPc4d1	velký
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
Polyfémos	Polyfémos	k1gInSc1	Polyfémos
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
usadit	usadit	k5eAaPmF	usadit
v	v	k7c6	v
Mýsii	Mýsie	k1gFnSc6	Mýsie
a	a	k8xC	a
založit	založit	k5eAaPmF	založit
tam	tam	k6eAd1	tam
slavné	slavný	k2eAgNnSc4d1	slavné
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Hylás	Hylás	k6eAd1	Hylás
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
manželem	manžel	k1gMnSc7	manžel
té	ten	k3xDgFnSc2	ten
víly	víla	k1gFnSc2	víla
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
plavce	plavec	k1gMnPc4	plavec
uklidnilo	uklidnit	k5eAaPmAgNnS	uklidnit
a	a	k8xC	a
odpustili	odpustit	k5eAaPmAgMnP	odpustit
si	se	k3xPyFc3	se
vzájemné	vzájemný	k2eAgFnPc4d1	vzájemná
výčitky	výčitka	k1gFnPc4	výčitka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
přistání	přistání	k1gNnSc1	přistání
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
surového	surový	k2eAgMnSc4d1	surový
krále	král	k1gMnSc4	král
Amyka	Amyek	k1gMnSc4	Amyek
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnPc4	vládce
národa	národ	k1gInSc2	národ
Bebryků	Bebryek	k1gMnPc2	Bebryek
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
příchozích	příchozí	k1gMnPc2	příchozí
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
s	s	k7c7	s
králem	král	k1gMnSc7	král
utkat	utkat	k5eAaPmF	utkat
v	v	k7c6	v
pěstním	pěstní	k2eAgInSc6d1	pěstní
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
již	již	k6eAd1	již
už	už	k6eAd1	už
zabil	zabít	k5eAaPmAgMnS	zabít
a	a	k8xC	a
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc2	jejich
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Argonauté	argonaut	k1gMnPc1	argonaut
byli	být	k5eAaImAgMnP	být
pobouřeni	pobouřit	k5eAaPmNgMnP	pobouřit
a	a	k8xC	a
rozhněváni	rozhněvat	k5eAaPmNgMnP	rozhněvat
a	a	k8xC	a
k	k	k7c3	k
zápasu	zápas	k1gInSc3	zápas
s	s	k7c7	s
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
Polydeukés	Polydeukés	k1gInSc4	Polydeukés
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
pěstní	pěstní	k2eAgMnSc1d1	pěstní
zápasník	zápasník	k1gMnSc1	zápasník
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
cizí	cizí	k2eAgMnSc1d1	cizí
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Polydeukés	Polydeukés	k1gInSc4	Polydeukés
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
a	a	k8xC	a
král	král	k1gMnSc1	král
padl	padnout	k5eAaImAgMnS	padnout
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
s	s	k7c7	s
rozbitou	rozbitý	k2eAgFnSc7d1	rozbitá
lebkou	lebka	k1gFnSc7	lebka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
krvavou	krvavý	k2eAgFnSc4d1	krvavá
řež	řež	k1gFnSc4	řež
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Bebrykové	Bebryková	k1gFnSc6	Bebryková
padali	padat	k5eAaImAgMnP	padat
a	a	k8xC	a
dávali	dávat	k5eAaImAgMnP	dávat
se	se	k3xPyFc4	se
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Dostali	dostat	k5eAaPmAgMnP	dostat
se	se	k3xPyFc4	se
však	však	k9	však
do	do	k7c2	do
boje	boj	k1gInSc2	boj
osmělených	osmělený	k2eAgMnPc2d1	osmělený
sousedů	soused	k1gMnPc2	soused
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
a	a	k8xC	a
plenili	plenit	k5eAaImAgMnP	plenit
území	území	k1gNnSc3	území
Bebryků	Bebryk	k1gInPc2	Bebryk
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc2	jejich
pole	pole	k1gNnSc2	pole
i	i	k8xC	i
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Argonauté	argonaut	k1gMnPc1	argonaut
zůstali	zůstat	k5eAaPmAgMnP	zůstat
všichni	všechen	k3xTgMnPc1	všechen
naživu	naživu	k6eAd1	naživu
<g/>
,	,	kIx,	,
naplnili	naplnit	k5eAaPmAgMnP	naplnit
koráb	koráb	k1gInSc4	koráb
kořistí	kořist	k1gFnPc2	kořist
a	a	k8xC	a
vypluli	vyplout	k5eAaPmAgMnP	vyplout
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Pluli	plout	k5eAaImAgMnP	plout
Bosporem	Bospor	k1gInSc7	Bospor
a	a	k8xC	a
kormidelník	kormidelník	k1gMnSc1	kormidelník
je	být	k5eAaImIp3nS	být
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
z	z	k7c2	z
nebezpečenství	nebezpečenství	k1gNnSc2	nebezpečenství
a	a	k8xC	a
uchránil	uchránit	k5eAaPmAgInS	uchránit
životy	život	k1gInPc4	život
i	i	k8xC	i
loď	loď	k1gFnSc4	loď
při	při	k7c6	při
ohrožení	ohrožení	k1gNnSc6	ohrožení
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
vlnou	vlna	k1gFnSc7	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
noční	noční	k2eAgFnSc6d1	noční
plavbě	plavba	k1gFnSc6	plavba
přistáli	přistát	k5eAaImAgMnP	přistát
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
neznámé	známý	k2eNgFnSc2d1	neznámá
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
stařec	stařec	k1gMnSc1	stařec
Fíneus	Fíneus	k1gMnSc1	Fíneus
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
měl	mít	k5eAaImAgMnS	mít
věšteckého	věštecký	k2eAgMnSc4d1	věštecký
ducha	duch	k1gMnSc4	duch
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
prozrazoval	prozrazovat	k5eAaImAgMnS	prozrazovat
lidem	člověk	k1gMnPc3	člověk
úmysly	úmysl	k1gInPc4	úmysl
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
byl	být	k5eAaImAgMnS	být
krutě	krutě	k6eAd1	krutě
potrestán	potrestat	k5eAaPmNgMnS	potrestat
<g/>
:	:	kIx,	:
byl	být	k5eAaImAgMnS	být
starý	starý	k2eAgMnSc1d1	starý
<g/>
,	,	kIx,	,
slepý	slepý	k2eAgMnSc1d1	slepý
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
hladový	hladový	k2eAgMnSc1d1	hladový
<g/>
.	.	kIx.	.
</s>
<s>
Kdykoli	kdykoli	k6eAd1	kdykoli
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
najíst	najíst	k5eAaPmF	najíst
<g/>
,	,	kIx,	,
přiletěly	přiletět	k5eAaPmAgFnP	přiletět
divoké	divoký	k2eAgFnPc4d1	divoká
dravé	dravý	k2eAgFnPc4d1	dravá
Harpyje	Harpyje	k1gFnPc4	Harpyje
<g/>
,	,	kIx,	,
napůl	napůl	k6eAd1	napůl
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
napůl	napůl	k6eAd1	napůl
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Rvaly	rvát	k5eAaImAgFnP	rvát
mu	on	k3xPp3gMnSc3	on
potravu	potrava	k1gFnSc4	potrava
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
i	i	k8xC	i
úst	ústa	k1gNnPc2	ústa
a	a	k8xC	a
ponechaly	ponechat	k5eAaPmAgInP	ponechat
mu	on	k3xPp3gMnSc3	on
jenom	jenom	k9	jenom
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nezemřel	zemřít	k5eNaPmAgMnS	zemřít
hladem	hlad	k1gInSc7	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgNnSc1d1	zbylé
jídlo	jídlo	k1gNnSc1	jídlo
pokálely	pokálet	k5eAaPmAgInP	pokálet
a	a	k8xC	a
znečistily	znečistit	k5eAaPmAgInP	znečistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gFnPc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nemohl	moct	k5eNaImAgMnS	moct
jíst	jíst	k5eAaImF	jíst
<g/>
.	.	kIx.	.
</s>
<s>
Fíneus	Fíneus	k1gMnSc1	Fíneus
Argonauty	argonaut	k1gMnPc7	argonaut
radostně	radostně	k6eAd1	radostně
přivítal	přivítat	k5eAaPmAgMnS	přivítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
věděl	vědět	k5eAaImAgMnS	vědět
z	z	k7c2	z
věšteb	věštba	k1gFnPc2	věštba
<g/>
.	.	kIx.	.
</s>
<s>
Synové	syn	k1gMnPc1	syn
boha	bůh	k1gMnSc4	bůh
severního	severní	k2eAgInSc2d1	severní
větru	vítr	k1gInSc2	vítr
Borea	Boreum	k1gNnSc2	Boreum
<g/>
,	,	kIx,	,
okřídlení	okřídlení	k1gNnSc2	okřídlení
Kalaís	Kalaísa	k1gFnPc2	Kalaísa
a	a	k8xC	a
Zétés	Zétésa	k1gFnPc2	Zétésa
<g/>
,	,	kIx,	,
pociťovali	pociťovat	k5eAaImAgMnP	pociťovat
se	s	k7c7	s
starcem	stařec	k1gMnSc7	stařec
velikou	veliký	k2eAgFnSc4d1	veliká
lítost	lítost	k1gFnSc4	lítost
<g/>
,	,	kIx,	,
připravili	připravit	k5eAaPmAgMnP	připravit
Fíneovi	Fíneův	k2eAgMnPc1d1	Fíneův
jídlo	jídlo	k1gNnSc1	jídlo
a	a	k8xC	a
jakmile	jakmile	k8xS	jakmile
přiletěly	přiletět	k5eAaPmAgFnP	přiletět
Harpyje	Harpyje	k1gFnPc1	Harpyje
<g/>
,	,	kIx,	,
vyřítili	vyřítit	k5eAaPmAgMnP	vyřítit
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
a	a	k8xC	a
chystali	chystat	k5eAaImAgMnP	chystat
se	se	k3xPyFc4	se
zahubit	zahubit	k5eAaPmF	zahubit
je	být	k5eAaImIp3nS	být
mečem	meč	k1gInSc7	meč
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
je	on	k3xPp3gFnPc4	on
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
bohyně	bohyně	k1gFnSc1	bohyně
Iris	iris	k1gFnSc2	iris
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uchránila	uchránit	k5eAaPmAgFnS	uchránit
bratry	bratr	k1gMnPc4	bratr
od	od	k7c2	od
božského	božský	k2eAgInSc2d1	božský
hněvu	hněv	k1gInSc2	hněv
<g/>
,	,	kIx,	,
zaručila	zaručit	k5eAaPmAgFnS	zaručit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
Harpyje	Harpyje	k1gFnSc1	Harpyje
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Fíneus	Fíneus	k1gMnSc1	Fíneus
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
zase	zase	k9	zase
dosyta	dosyta	k6eAd1	dosyta
najedl	najíst	k5eAaPmAgMnS	najíst
<g/>
,	,	kIx,	,
radoval	radovat	k5eAaImAgMnS	radovat
se	se	k3xPyFc4	se
z	z	k7c2	z
toho	ten	k3xDgMnSc2	ten
a	a	k8xC	a
Argonautům	argonaut	k1gMnPc3	argonaut
věštil	věštit	k5eAaImAgInS	věštit
o	o	k7c6	o
jejich	jejich	k3xOp3gFnPc6	jejich
dalších	další	k2eAgFnPc6d1	další
cestách	cesta	k1gFnPc6	cesta
a	a	k8xC	a
nástrahách	nástraha	k1gFnPc6	nástraha
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
však	však	k9	však
doplují	doplout	k5eAaPmIp3nP	doplout
do	do	k7c2	do
Kolchidy	Kolchida	k1gFnSc2	Kolchida
<g/>
,	,	kIx,	,
čeká	čekat	k5eAaImIp3nS	čekat
je	on	k3xPp3gNnSc4	on
šťastný	šťastný	k2eAgInSc1d1	šťastný
návrat	návrat	k1gInSc1	návrat
a	a	k8xC	a
sláva	sláva	k1gFnSc1	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozloučení	rozloučení	k1gNnSc6	rozloučení
s	s	k7c7	s
Fíneem	Fíneum	k1gNnSc7	Fíneum
se	se	k3xPyFc4	se
loď	loď	k1gFnSc1	loď
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
cestu	cesta	k1gFnSc4	cesta
přes	přes	k7c4	přes
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
úžinu	úžina	k1gFnSc4	úžina
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Bospor	Bospor	k1gInSc1	Bospor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
střežily	střežit	k5eAaImAgFnP	střežit
Symplégady	Symplégada	k1gFnPc1	Symplégada
<g/>
,	,	kIx,	,
obrovské	obrovský	k2eAgFnPc1d1	obrovská
skály	skála	k1gFnPc1	skála
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
za	za	k7c2	za
silného	silný	k2eAgNnSc2d1	silné
dunění	dunění	k1gNnSc2	dunění
a	a	k8xC	a
vlnobití	vlnobití	k1gNnSc2	vlnobití
hrozivě	hrozivě	k6eAd1	hrozivě
srážely	srážet	k5eAaImAgFnP	srážet
<g/>
.	.	kIx.	.
</s>
<s>
Skrytě	skrytě	k6eAd1	skrytě
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
dohlížela	dohlížet	k5eAaImAgFnS	dohlížet
patronka	patronka	k1gFnSc1	patronka
bohyně	bohyně	k1gFnSc1	bohyně
Athéna	Athéna	k1gFnSc1	Athéna
a	a	k8xC	a
plavcům	plavec	k1gMnPc3	plavec
pomohla	pomoct	k5eAaPmAgFnS	pomoct
Fíneova	Fíneův	k2eAgFnSc1d1	Fíneův
holubice	holubice	k1gFnSc1	holubice
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
rady	rada	k1gFnSc2	rada
ji	on	k3xPp3gFnSc4	on
vypustili	vypustit	k5eAaPmAgMnP	vypustit
a	a	k8xC	a
bedlivě	bedlivě	k6eAd1	bedlivě
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
úhony	úhona	k1gFnSc2	úhona
dostane	dostat	k5eAaPmIp3nS	dostat
přes	přes	k7c4	přes
úžinu	úžina	k1gFnSc4	úžina
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
potom	potom	k6eAd1	potom
prudce	prudko	k6eAd1	prudko
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
s	s	k7c7	s
lodí	loď	k1gFnSc7	loď
a	a	k8xC	a
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
sil	síla	k1gFnPc2	síla
i	i	k8xC	i
božskou	božská	k1gFnSc7	božská
pomocí	pomoc	k1gFnSc7	pomoc
úžinou	úžina	k1gFnSc7	úžina
projeli	projet	k5eAaPmAgMnP	projet
<g/>
.	.	kIx.	.
</s>
<s>
Plavba	plavba	k1gFnSc1	plavba
po	po	k7c6	po
hladině	hladina	k1gFnSc6	hladina
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
byla	být	k5eAaImAgFnS	být
klidná	klidný	k2eAgFnSc1d1	klidná
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zastavili	zastavit	k5eAaPmAgMnP	zastavit
u	u	k7c2	u
malého	malý	k2eAgInSc2d1	malý
ostrova	ostrov	k1gInSc2	ostrov
Thýnie	Thýnie	k1gFnSc2	Thýnie
<g/>
,	,	kIx,	,
spatřili	spatřit	k5eAaPmAgMnP	spatřit
zářivého	zářivý	k2eAgMnSc4d1	zářivý
boha	bůh	k1gMnSc4	bůh
Apollóna	Apollón	k1gMnSc4	Apollón
v	v	k7c6	v
lesklé	lesklý	k2eAgFnSc6d1	lesklá
zbroji	zbroj	k1gFnSc6	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
tam	tam	k6eAd1	tam
právě	právě	k6eAd1	právě
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
každoroční	každoroční	k2eAgFnSc6d1	každoroční
návštěvě	návštěva	k1gFnSc6	návštěva
u	u	k7c2	u
Hyperboreů	Hyperbore	k1gInPc2	Hyperbore
<g/>
,	,	kIx,	,
severského	severský	k2eAgInSc2d1	severský
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
<g/>
.	.	kIx.	.
</s>
<s>
Plavci	plavec	k1gMnPc1	plavec
obětovali	obětovat	k5eAaBmAgMnP	obětovat
bohům	bůh	k1gMnPc3	bůh
a	a	k8xC	a
prosili	prosít	k5eAaPmAgMnP	prosít
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
na	na	k7c6	na
dalších	další	k2eAgFnPc6d1	další
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc4	ostrov
zasvětili	zasvětit	k5eAaPmAgMnP	zasvětit
Jitřnímu	jitřní	k2eAgMnSc3d1	jitřní
Apollónovi	Apollón	k1gMnSc3	Apollón
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jehož	k3xOyRp3gInPc4	jehož
statečné	statečný	k2eAgInPc4d1	statečný
činy	čin	k1gInPc4	čin
vzpomínali	vzpomínat	k5eAaImAgMnP	vzpomínat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
plavbě	plavba	k1gFnSc6	plavba
míjeli	míjet	k5eAaImAgMnP	míjet
Acheruskou	Acheruský	k2eAgFnSc4d1	Acheruský
skálu	skála	k1gFnSc4	skála
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
blízkosti	blízkost	k1gFnSc6	blízkost
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
podsvětní	podsvětní	k2eAgFnSc1d1	podsvětní
řeka	řeka	k1gFnSc1	řeka
Acherón	Acherón	k1gInSc1	Acherón
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
proudí	proudit	k5eAaPmIp3nS	proudit
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
zemi	zem	k1gFnSc6	zem
žili	žít	k5eAaImAgMnP	žít
Maryandynové	Maryandyn	k1gMnPc1	Maryandyn
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgMnS	vládnout
jim	on	k3xPp3gMnPc3	on
král	král	k1gMnSc1	král
Lykos	Lykos	k1gInSc1	Lykos
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
král	král	k1gMnSc1	král
je	být	k5eAaImIp3nS	být
slavnostně	slavnostně	k6eAd1	slavnostně
uvítal	uvítat	k5eAaPmAgMnS	uvítat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
sláva	sláva	k1gFnSc1	sláva
je	být	k5eAaImIp3nS	být
předcházela	předcházet	k5eAaImAgFnS	předcházet
<g/>
.	.	kIx.	.
</s>
<s>
Přijetí	přijetí	k1gNnSc1	přijetí
bylo	být	k5eAaImAgNnS	být
naplněno	naplnit	k5eAaPmNgNnS	naplnit
i	i	k9	i
vděčností	vděčnost	k1gFnSc7	vděčnost
za	za	k7c4	za
porážku	porážka	k1gFnSc4	porážka
Bebryků	Bebryk	k1gInPc2	Bebryk
i	i	k9	i
jejich	jejich	k3xOp3gNnSc1	jejich
krále	král	k1gMnSc4	král
Amyka	Amyek	k1gMnSc4	Amyek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
činem	čin	k1gInSc7	čin
Polydeukovým	Polydeukový	k2eAgInSc7d1	Polydeukový
<g/>
.	.	kIx.	.
</s>
<s>
Iásón	Iásón	k1gMnSc1	Iásón
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
podrobnosti	podrobnost	k1gFnPc4	podrobnost
z	z	k7c2	z
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
cesty	cesta	k1gFnSc2	cesta
i	i	k8xC	i
očekávání	očekávání	k1gNnSc2	očekávání
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterými	který	k3yQgNnPc7	který
plují	plout	k5eAaImIp3nP	plout
do	do	k7c2	do
Kolchidy	Kolchida	k1gFnSc2	Kolchida
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Lykos	Lykos	k1gMnSc1	Lykos
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
přátelský	přátelský	k2eAgInSc4d1	přátelský
a	a	k8xC	a
nadšený	nadšený	k2eAgInSc4d1	nadšený
jejich	jejich	k3xOp3gNnPc3	jejich
dobrodružstvím	dobrodružství	k1gNnPc3	dobrodružství
<g/>
,	,	kIx,	,
že	že	k8xS	že
požádal	požádat	k5eAaPmAgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vzali	vzít	k5eAaPmAgMnP	vzít
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
cestu	cesta	k1gFnSc4	cesta
s	s	k7c7	s
sebou	se	k3xPyFc7	se
jeho	on	k3xPp3gMnSc4	on
syna	syn	k1gMnSc4	syn
Daskyla	Daskyla	k1gMnSc4	Daskyla
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
zato	zato	k6eAd1	zato
vystaví	vystavit	k5eAaPmIp3nS	vystavit
bratřím	bratřit	k5eAaImIp1nS	bratřit
Polydeukovi	Polydeuk	k1gMnSc3	Polydeuk
a	a	k8xC	a
Kastorovi	Kastor	k1gMnSc3	Kastor
vysoký	vysoký	k2eAgInSc4d1	vysoký
chrám	chrám	k1gInSc4	chrám
na	na	k7c6	na
skále	skála	k1gFnSc6	skála
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
také	také	k9	také
stala	stát	k5eAaPmAgFnS	stát
smutná	smutný	k2eAgFnSc1d1	smutná
očekávaná	očekávaný	k2eAgFnSc1d1	očekávaná
událost	událost	k1gFnSc1	událost
<g/>
:	:	kIx,	:
věštec	věštec	k1gMnSc1	věštec
Idmón	Idmón	k1gMnSc1	Idmón
zahynul	zahynout	k5eAaPmAgMnS	zahynout
nešťastně	šťastně	k6eNd1	šťastně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gNnSc4	on
napadl	napadnout	k5eAaPmAgMnS	napadnout
divoký	divoký	k2eAgMnSc1d1	divoký
kanec	kanec	k1gMnSc1	kanec
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
zle	zle	k6eAd1	zle
jej	on	k3xPp3gMnSc4	on
zranil	zranit	k5eAaPmAgMnS	zranit
a	a	k8xC	a
Idmóna	Idmón	k1gMnSc4	Idmón
usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
přispěchali	přispěchat	k5eAaPmAgMnP	přispěchat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
marně	marně	k6eAd1	marně
<g/>
.	.	kIx.	.
</s>
<s>
Divoké	divoký	k2eAgNnSc1d1	divoké
zvíře	zvíře	k1gNnSc1	zvíře
probodl	probodnout	k5eAaPmAgInS	probodnout
Idás	Idás	k1gInSc4	Idás
kopím	kopit	k5eAaImIp1nS	kopit
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
dny	den	k1gInPc1	den
a	a	k8xC	a
noci	noc	k1gFnPc1	noc
se	se	k3xPyFc4	se
s	s	k7c7	s
druhem	druh	k1gInSc7	druh
loučili	loučit	k5eAaImAgMnP	loučit
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ho	on	k3xPp3gMnSc4	on
slavnostně	slavnostně	k6eAd1	slavnostně
pochovali	pochovat	k5eAaPmAgMnP	pochovat
a	a	k8xC	a
navršili	navršit	k5eAaPmAgMnP	navršit
mu	on	k3xPp3gMnSc3	on
mohylu	mohyla	k1gFnSc4	mohyla
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
chvílích	chvíle	k1gFnPc6	chvíle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c4	po
kratičké	kratičký	k2eAgFnPc4d1	kratičká
nemoci	nemoc	k1gFnPc4	nemoc
jejich	jejich	k3xOp3gFnSc2	jejich
komidelník	komidelník	k1gInSc4	komidelník
Tífys	Tífysa	k1gFnPc2	Tífysa
<g/>
.	.	kIx.	.
</s>
<s>
Smutek	smutek	k1gInSc1	smutek
trval	trvat	k5eAaImAgInS	trvat
dalších	další	k2eAgInPc2d1	další
dvanáct	dvanáct	k4xCc4	dvanáct
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
musela	muset	k5eAaImAgFnS	muset
vstoupit	vstoupit	k5eAaPmF	vstoupit
bohyně	bohyně	k1gFnSc1	bohyně
Héra	Héra	k1gFnSc1	Héra
a	a	k8xC	a
vnukla	vnuknout	k5eAaPmAgFnS	vnuknout
Ankaiovi	Ankaius	k1gMnSc3	Ankaius
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
mužstvo	mužstvo	k1gNnSc1	mužstvo
vzchopilo	vzchopit	k5eAaPmAgNnS	vzchopit
a	a	k8xC	a
ke	k	k7c3	k
kormidlu	kormidlo	k1gNnSc3	kormidlo
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
Ankaios	Ankaios	k1gMnSc1	Ankaios
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
plavba	plavba	k1gFnSc1	plavba
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nerušená	rušený	k2eNgFnSc1d1	nerušená
<g/>
.	.	kIx.	.
</s>
<s>
Zastávka	zastávka	k1gFnSc1	zastávka
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
u	u	k7c2	u
mohyly	mohyla	k1gFnSc2	mohyla
Sthenela	Sthenel	k1gMnSc2	Sthenel
<g/>
,	,	kIx,	,
přítele	přítel	k1gMnSc2	přítel
Héraklova	Héraklův	k2eAgMnSc2d1	Héraklův
z	z	k7c2	z
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Amazonkami	Amazonka	k1gFnPc7	Amazonka
<g/>
.	.	kIx.	.
</s>
<s>
Duši	duše	k1gFnSc4	duše
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
hrdiny	hrdina	k1gMnSc2	hrdina
propustila	propustit	k5eAaPmAgFnS	propustit
z	z	k7c2	z
podsvětní	podsvětní	k2eAgFnSc2d1	podsvětní
říše	říš	k1gFnSc2	říš
královna	královna	k1gFnSc1	královna
Persefona	Persefona	k1gFnSc1	Persefona
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
setkat	setkat	k5eAaPmF	setkat
z	z	k7c2	z
krajany	krajan	k1gMnPc7	krajan
<g/>
.	.	kIx.	.
</s>
<s>
Argonauté	argonaut	k1gMnPc1	argonaut
vzdali	vzdát	k5eAaPmAgMnP	vzdát
poctu	pocta	k1gFnSc4	pocta
a	a	k8xC	a
vykonali	vykonat	k5eAaPmAgMnP	vykonat
oběť	oběť	k1gFnSc4	oběť
a	a	k8xC	a
při	při	k7c6	při
Orfeově	Orfeův	k2eAgInSc6d1	Orfeův
zpěvu	zpěv	k1gInSc6	zpěv
vzpomínali	vzpomínat	k5eAaImAgMnP	vzpomínat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
už	už	k6eAd1	už
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
při	při	k7c6	při
dobrém	dobrý	k2eAgInSc6d1	dobrý
větru	vítr	k1gInSc6	vítr
cesta	cesta	k1gFnSc1	cesta
rychle	rychle	k6eAd1	rychle
ubíhala	ubíhat	k5eAaImAgFnS	ubíhat
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
opřít	opřít	k5eAaPmF	opřít
do	do	k7c2	do
vesel	veslo	k1gNnPc2	veslo
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
kolem	kolem	k7c2	kolem
ostrova	ostrov	k1gInSc2	ostrov
Amazonek	Amazonka	k1gFnPc2	Amazonka
jim	on	k3xPp3gMnPc3	on
přál	přát	k5eAaImAgMnS	přát
silnější	silný	k2eAgInSc4d2	silnější
vítr	vítr	k1gInSc4	vítr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemuseli	muset	k5eNaImAgMnP	muset
přistávat	přistávat	k5eAaImF	přistávat
a	a	k8xC	a
riskovat	riskovat	k5eAaBmF	riskovat
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
je	on	k3xPp3gFnPc4	on
čekalo	čekat	k5eAaImAgNnS	čekat
přistání	přistání	k1gNnSc1	přistání
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Aretiás	Aretiása	k1gFnPc2	Aretiása
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlili	sídlit	k5eAaImAgMnP	sídlit
draví	dravý	k2eAgMnPc1d1	dravý
ptáci	pták	k1gMnPc1	pták
s	s	k7c7	s
kovovými	kovový	k2eAgInPc7d1	kovový
brky	brk	k1gInPc7	brk
<g/>
.	.	kIx.	.
</s>
<s>
Héraklés	Héraklés	k6eAd1	Héraklés
je	být	k5eAaImIp3nS	být
sem	sem	k6eAd1	sem
zahnal	zahnat	k5eAaPmAgMnS	zahnat
od	od	k7c2	od
Stymfálského	Stymfálský	k2eAgNnSc2d1	Stymfálský
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
než	než	k8xS	než
ukotvili	ukotvit	k5eAaPmAgMnP	ukotvit
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
svým	svůj	k3xOyFgInSc7	svůj
brkem	brk	k1gInSc7	brk
zranil	zranit	k5eAaPmAgMnS	zranit
plavce	plavka	k1gFnSc3	plavka
Oilea	Oileus	k1gMnSc2	Oileus
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
druhému	druhý	k4xOgInSc3	druhý
útoku	útok	k1gInSc3	útok
se	se	k3xPyFc4	se
už	už	k6eAd1	už
ubránili	ubránit	k5eAaPmAgMnP	ubránit
svým	svůj	k3xOyFgInSc7	svůj
ostrým	ostrý	k2eAgInSc7d1	ostrý
šípem	šíp	k1gInSc7	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Fíneovy	Fíneův	k2eAgFnSc2d1	Fíneův
věštby	věštba	k1gFnSc2	věštba
však	však	k9	však
na	na	k7c4	na
ptáky	pták	k1gMnPc4	pták
střelba	střelba	k1gFnSc1	střelba
šípy	šíp	k1gInPc4	šíp
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
zvolili	zvolit	k5eAaPmAgMnP	zvolit
jinou	jiný	k2eAgFnSc4d1	jiná
metodu	metoda	k1gFnSc4	metoda
-	-	kIx~	-
mužstvo	mužstvo	k1gNnSc1	mužstvo
se	se	k3xPyFc4	se
ochránilo	ochránit	k5eAaPmAgNnS	ochránit
štíty	štít	k1gInPc7	štít
a	a	k8xC	a
kopími	kopí	k1gNnPc7	kopí
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
ptáci	pták	k1gMnPc1	pták
přiblížili	přiblížit	k5eAaPmAgMnP	přiblížit
<g/>
,	,	kIx,	,
spustili	spustit	k5eAaPmAgMnP	spustit
hlasitý	hlasitý	k2eAgInSc4d1	hlasitý
křik	křik	k1gInSc4	křik
a	a	k8xC	a
harašili	harašit	k5eAaImAgMnP	harašit
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
ptáky	pták	k1gMnPc4	pták
tak	tak	k6eAd1	tak
vyděsilo	vyděsit	k5eAaPmAgNnS	vyděsit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odletěli	odletět	k5eAaPmAgMnP	odletět
k	k	k7c3	k
dalekým	daleký	k2eAgFnPc3d1	daleká
horám	hora	k1gFnPc3	hora
a	a	k8xC	a
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nevrátili	vrátit	k5eNaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Poslechli	poslechnout	k5eAaPmAgMnP	poslechnout
i	i	k9	i
další	další	k2eAgFnSc4d1	další
radu	rada	k1gFnSc4	rada
Fíneovu	Fíneův	k2eAgFnSc4d1	Fíneův
<g/>
,	,	kIx,	,
zůstali	zůstat	k5eAaPmAgMnP	zůstat
ještě	ještě	k6eAd1	ještě
tu	ten	k3xDgFnSc4	ten
noc	noc	k1gFnSc4	noc
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
zachránilo	zachránit	k5eAaPmAgNnS	zachránit
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přišla	přijít	k5eAaPmAgFnS	přijít
nevídaně	vídaně	k6eNd1	vídaně
silná	silný	k2eAgFnSc1d1	silná
bouře	bouře	k1gFnSc1	bouře
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
ráno	ráno	k6eAd1	ráno
klid	klid	k1gInSc4	klid
<g/>
,	,	kIx,	,
přicházeli	přicházet	k5eAaImAgMnP	přicházet
k	k	k7c3	k
lodi	loď	k1gFnSc3	loď
a	a	k8xC	a
vtom	vtom	k6eAd1	vtom
se	se	k3xPyFc4	se
vynořili	vynořit	k5eAaPmAgMnP	vynořit
z	z	k7c2	z
úkrytu	úkryt	k1gInSc2	úkryt
čtyři	čtyři	k4xCgMnPc1	čtyři
mladí	mladý	k2eAgMnPc1d1	mladý
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
bouři	bouře	k1gFnSc4	bouře
unikli	uniknout	k5eAaPmAgMnP	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
synové	syn	k1gMnPc1	syn
Frixa	Frixa	k1gMnSc1	Frixa
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
zlatý	zlatý	k2eAgInSc1d1	zlatý
beran	beran	k1gInSc1	beran
dopravil	dopravit	k5eAaPmAgInS	dopravit
do	do	k7c2	do
Kolchidy	Kolchida	k1gFnSc2	Kolchida
ke	k	k7c3	k
králi	král	k1gMnSc3	král
Aiétovi	Aiét	k1gMnSc3	Aiét
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc1	jejich
dědovi	dědův	k2eAgMnPc1d1	dědův
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nyní	nyní	k6eAd1	nyní
vlastní	vlastní	k2eAgNnSc4d1	vlastní
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
<g/>
.	.	kIx.	.
</s>
<s>
Frixos	Frixos	k1gInSc1	Frixos
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
nabádal	nabádat	k5eAaBmAgMnS	nabádat
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
řeckého	řecký	k2eAgInSc2d1	řecký
Orchomenu	Orchomen	k2eAgFnSc4d1	Orchomen
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
jmění	jmění	k1gNnSc4	jmění
po	po	k7c6	po
králi	král	k1gMnSc6	král
Athamantovi	Athamant	k1gMnSc6	Athamant
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
synové	syn	k1gMnPc1	syn
uposlechli	uposlechnout	k5eAaPmAgMnP	uposlechnout
a	a	k8xC	a
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
Chalkiopé	Chalkiopý	k2eAgNnSc1d1	Chalkiopý
právě	právě	k9	právě
prchali	prchat	k5eAaImAgMnP	prchat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
když	když	k8xS	když
silná	silný	k2eAgFnSc1d1	silná
bouře	bouře	k1gFnSc1	bouře
zničila	zničit	k5eAaPmAgFnS	zničit
jejich	jejich	k3xOp3gFnSc4	jejich
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Iásón	Iásón	k1gMnSc1	Iásón
vše	všechen	k3xTgNnSc4	všechen
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
vyslechl	vyslechnout	k5eAaPmAgInS	vyslechnout
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgInS	vyzvat
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
Arga	Argos	k1gMnSc4	Argos
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nalodili	nalodit	k5eAaPmAgMnP	nalodit
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
a	a	k8xC	a
vrátili	vrátit	k5eAaPmAgMnP	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Kolchidy	Kolchida	k1gFnSc2	Kolchida
pro	pro	k7c4	pro
rouno	rouno	k1gNnSc4	rouno
<g/>
.	.	kIx.	.
</s>
<s>
Frixovi	Frixův	k2eAgMnPc1d1	Frixův
synové	syn	k1gMnPc1	syn
jej	on	k3xPp3gMnSc4	on
varovali	varovat	k5eAaImAgMnP	varovat
před	před	k7c7	před
proradností	proradnost	k1gFnSc7	proradnost
krále	král	k1gMnSc2	král
Aiéta	Aiét	k1gMnSc2	Aiét
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc2	jeho
krutostí	krutost	k1gFnPc2	krutost
a	a	k8xC	a
bojovností	bojovnost	k1gFnPc2	bojovnost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
líčili	líčit	k5eAaImAgMnP	líčit
jako	jako	k9	jako
nemožné	možný	k2eNgNnSc1d1	nemožné
získat	získat	k5eAaPmF	získat
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
prožívali	prožívat	k5eAaImAgMnP	prožívat
všichni	všechen	k3xTgMnPc1	všechen
radost	radost	k6eAd1	radost
i	i	k8xC	i
strach	strach	k1gInSc4	strach
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
vedla	vést	k5eAaImAgFnS	vést
kolem	kolem	k7c2	kolem
vysoké	vysoký	k2eAgFnSc2d1	vysoká
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
trpěl	trpět	k5eAaImAgMnS	trpět
Prométheus	Prométheus	k1gMnSc1	Prométheus
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
daroval	darovat	k5eAaPmAgMnS	darovat
lidem	člověk	k1gMnPc3	člověk
oheň	oheň	k1gInSc4	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
co	co	k8xS	co
den	den	k1gInSc1	den
mu	on	k3xPp3gMnSc3	on
obrovský	obrovský	k2eAgMnSc1d1	obrovský
orel	orel	k1gMnSc1	orel
rozsápal	rozsápat	k5eAaPmAgMnS	rozsápat
játra	játra	k1gNnPc4	játra
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
zase	zase	k9	zase
dorostla	dorůst	k5eAaPmAgFnS	dorůst
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
již	již	k6eAd1	již
brzy	brzy	k6eAd1	brzy
dorazili	dorazit	k5eAaPmAgMnP	dorazit
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
kolchidské	kolchidský	k2eAgFnSc2d1	Kolchidská
řeky	řeka	k1gFnSc2	řeka
Fásidy	Fásida	k1gFnSc2	Fásida
<g/>
,	,	kIx,	,
do	do	k7c2	do
pohodlného	pohodlný	k2eAgInSc2d1	pohodlný
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Ukryli	ukrýt	k5eAaPmAgMnP	ukrýt
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
obětovali	obětovat	k5eAaBmAgMnP	obětovat
bohům	bůh	k1gMnPc3	bůh
za	za	k7c4	za
šťastné	šťastný	k2eAgNnSc4d1	šťastné
doplutí	doplutí	k1gNnSc4	doplutí
a	a	k8xC	a
prosili	prosit	k5eAaImAgMnP	prosit
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
přízeň	přízeň	k1gFnSc4	přízeň
a	a	k8xC	a
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Sbírali	sbírat	k5eAaImAgMnP	sbírat
odvahu	odvaha	k1gFnSc4	odvaha
a	a	k8xC	a
dělali	dělat	k5eAaImAgMnP	dělat
plány	plán	k1gInPc4	plán
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
králem	král	k1gMnSc7	král
Aiétem	Aiét	k1gMnSc7	Aiét
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
důležitá	důležitý	k2eAgFnSc1d1	důležitá
část	část	k1gFnSc1	část
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
mýtu	mýtus	k1gInSc2	mýtus
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
příběhem	příběh	k1gInSc7	příběh
Iásona	Iásona	k1gFnSc1	Iásona
a	a	k8xC	a
Médeii	Médeium	k1gNnPc7	Médeium
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgInPc3	jenž
jsou	být	k5eAaImIp3nP	být
věnovány	věnovat	k5eAaImNgInP	věnovat
samostatné	samostatný	k2eAgInPc1d1	samostatný
články	článek	k1gInPc1	článek
<g/>
.	.	kIx.	.
</s>
<s>
Zkráceně	zkráceně	k6eAd1	zkráceně
se	se	k3xPyFc4	se
události	událost	k1gFnPc1	událost
v	v	k7c6	v
Kolchidě	Kolchida	k1gFnSc6	Kolchida
vyvíjely	vyvíjet	k5eAaImAgInP	vyvíjet
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
Aiétés	Aiétés	k1gInSc4	Aiétés
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
úskočný	úskočný	k2eAgMnSc1d1	úskočný
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
obav	obava	k1gFnPc2	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
Iásón	Iásón	k1gMnSc1	Iásón
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
zbavit	zbavit	k5eAaPmF	zbavit
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
nepřívětivě	přívětivě	k6eNd1	přívětivě
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
mu	on	k3xPp3gMnSc3	on
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
za	za	k7c2	za
nesplnitelných	splnitelný	k2eNgFnPc2d1	nesplnitelná
podmínek	podmínka	k1gFnPc2	podmínka
-	-	kIx~	-
musel	muset	k5eAaImAgInS	muset
zorat	zorat	k5eAaPmF	zorat
pole	pole	k1gNnPc4	pole
s	s	k7c7	s
býky	býk	k1gMnPc7	býk
s	s	k7c7	s
kovovýma	kovový	k2eAgFnPc7d1	kovová
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
nasít	nasít	k5eAaPmF	nasít
dračí	dračí	k2eAgInPc4d1	dračí
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vzklíčí	vzklíčit	k5eAaPmIp3nP	vzklíčit
ozbrojení	ozbrojený	k2eAgMnPc1d1	ozbrojený
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
ty	ten	k3xDgMnPc4	ten
musí	muset	k5eAaImIp3nP	muset
do	do	k7c2	do
večera	večer	k1gInSc2	večer
pobít	pobít	k5eAaPmF	pobít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
již	již	k6eAd1	již
Iásón	Iásón	k1gInSc1	Iásón
měl	mít	k5eAaImAgInS	mít
silného	silný	k2eAgMnSc4d1	silný
spojence	spojenec	k1gMnSc4	spojenec
-	-	kIx~	-
Aiétovu	Aiétův	k2eAgFnSc4d1	Aiétův
dceru	dcera	k1gFnSc4	dcera
Médeiu	Médeius	k1gMnSc3	Médeius
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
Erós	erós	k1gInSc1	erós
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
srdce	srdce	k1gNnSc4	srdce
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
Iásonovi	Iáson	k1gMnSc3	Iáson
<g/>
.	.	kIx.	.
</s>
<s>
Dala	dát	k5eAaPmAgFnS	dát
Iásonovi	Iásonův	k2eAgMnPc1d1	Iásonův
zázračnou	zázračný	k2eAgFnSc4d1	zázračná
mast	mast	k1gFnSc4	mast
nezranitelnosti	nezranitelnost	k1gFnSc2	nezranitelnost
a	a	k8xC	a
poradila	poradit	k5eAaPmAgFnS	poradit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
porazit	porazit	k5eAaPmF	porazit
dračí	dračí	k2eAgNnSc4d1	dračí
vojsko	vojsko	k1gNnSc4	vojsko
-	-	kIx~	-
vhodí	vhodit	k5eAaPmIp3nS	vhodit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
kámen	kámen	k1gInSc4	kámen
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
se	se	k3xPyFc4	se
pobijí	pobít	k5eAaPmIp3nP	pobít
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Médeia	Médeia	k1gFnSc1	Médeia
měla	mít	k5eAaImAgFnS	mít
výčitky	výčitek	k1gInPc4	výčitek
ze	z	k7c2	z
zrady	zrada	k1gFnSc2	zrada
vůči	vůči	k7c3	vůči
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Iásona	Iásona	k1gFnSc1	Iásona
milovala	milovat	k5eAaImAgFnS	milovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
chtěla	chtít	k5eAaImAgFnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
sebou	se	k3xPyFc7	se
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
Iásón	Iásón	k1gInSc1	Iásón
bez	bez	k7c2	bez
váhání	váhání	k1gNnSc2	váhání
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
úkol	úkol	k1gInSc1	úkol
splněn	splnit	k5eAaPmNgInS	splnit
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Aiétés	Aiétésa	k1gFnPc2	Aiétésa
hned	hned	k6eAd1	hned
pojal	pojmout	k5eAaPmAgMnS	pojmout
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
cizí	cizí	k2eAgFnSc2d1	cizí
pomoci	pomoc	k1gFnSc2	pomoc
a	a	k8xC	a
bez	bez	k7c2	bez
váhání	váhání	k1gNnSc2	váhání
začal	začít	k5eAaPmAgInS	začít
podezřívat	podezřívat	k5eAaImF	podezřívat
některou	některý	k3yIgFnSc4	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Médeia	Médeia	k1gFnSc1	Médeia
z	z	k7c2	z
hrozného	hrozný	k2eAgInSc2d1	hrozný
strachu	strach	k1gInSc2	strach
vyhledala	vyhledat	k5eAaPmAgFnS	vyhledat
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Iásóna	Iásón	k1gMnSc2	Iásón
a	a	k8xC	a
přemlouvala	přemlouvat	k5eAaImAgFnS	přemlouvat
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
útěku	útěk	k1gInSc3	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Pomohla	pomoct	k5eAaPmAgFnS	pomoct
získat	získat	k5eAaPmF	získat
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
<g/>
,	,	kIx,	,
očarovala	očarovat	k5eAaPmAgFnS	očarovat
hrozného	hrozný	k2eAgMnSc4d1	hrozný
draka	drak	k1gMnSc4	drak
kouzelnými	kouzelný	k2eAgNnPc7d1	kouzelné
slovy	slovo	k1gNnPc7	slovo
a	a	k8xC	a
uspala	uspat	k5eAaPmAgFnS	uspat
ho	on	k3xPp3gMnSc4	on
kouzelnou	kouzelný	k2eAgFnSc7d1	kouzelná
šťávou	šťáva	k1gFnSc7	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
Iásón	Iásón	k1gMnSc1	Iásón
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
a	a	k8xC	a
prchali	prchat	k5eAaImAgMnP	prchat
k	k	k7c3	k
lodi	loď	k1gFnSc3	loď
<g/>
.	.	kIx.	.
</s>
<s>
Argonauté	argonaut	k1gMnPc1	argonaut
žasli	žasnout	k5eAaImAgMnP	žasnout
nad	nad	k7c7	nad
krásou	krása	k1gFnSc7	krása
zlatého	zlatý	k2eAgNnSc2d1	Zlaté
rouna	rouno	k1gNnSc2	rouno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Iásón	Iásón	k1gInSc1	Iásón
i	i	k9	i
Médeia	Médeia	k1gFnSc1	Médeia
je	on	k3xPp3gFnPc4	on
pobízeli	pobízet	k5eAaImAgMnP	pobízet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rychle	rychle	k6eAd1	rychle
připravili	připravit	k5eAaPmAgMnP	připravit
loď	loď	k1gFnSc4	loď
na	na	k7c4	na
odplutí	odplutí	k1gNnSc4	odplutí
a	a	k8xC	a
posádku	posádka	k1gFnSc4	posádka
k	k	k7c3	k
boji	boj	k1gInSc3	boj
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Aiétés	Aiétésa	k1gFnPc2	Aiétésa
rychle	rychle	k6eAd1	rychle
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
bojovníky	bojovník	k1gMnPc4	bojovník
a	a	k8xC	a
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
Apsyrtem	Apsyrt	k1gMnSc7	Apsyrt
dorazil	dorazit	k5eAaPmAgMnS	dorazit
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
ale	ale	k8xC	ale
Argonauté	argonaut	k1gMnPc1	argonaut
byli	být	k5eAaImAgMnP	být
daleko	daleko	k6eAd1	daleko
na	na	k7c6	na
širém	širý	k2eAgNnSc6d1	širé
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
s	s	k7c7	s
dopomocí	dopomoc	k1gFnSc7	dopomoc
bohyně	bohyně	k1gFnSc2	bohyně
Héry	Héra	k1gFnSc2	Héra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
znamení	znamení	k1gNnSc4	znamení
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
Istru	Ister	k1gInSc2	Ister
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aiétovo	Aiétův	k2eAgNnSc1d1	Aiétův
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
vydalo	vydat	k5eAaPmAgNnS	vydat
cestou	cesta	k1gFnSc7	cesta
na	na	k7c6	na
Helléspont	Helléspont	k1gMnSc1	Helléspont
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
vedená	vedený	k2eAgFnSc1d1	vedená
Apsyrtem	Apsyrt	k1gInSc7	Apsyrt
plula	plout	k5eAaImAgFnS	plout
k	k	k7c3	k
Istru	Ister	k1gInSc3	Ister
a	a	k8xC	a
tam	tam	k6eAd1	tam
na	na	k7c6	na
soutoku	soutok	k1gInSc2	soutok
dvou	dva	k4xCgNnPc2	dva
ramen	rameno	k1gNnPc2	rameno
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
lodi	loď	k1gFnPc1	loď
setkaly	setkat	k5eAaPmAgFnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
vložila	vložit	k5eAaPmAgFnS	vložit
Médeia	Médeium	k1gNnSc2	Médeium
<g/>
,	,	kIx,	,
lstí	lest	k1gFnPc2	lest
vylákala	vylákat	k5eAaPmAgFnS	vylákat
Apsyrta	Apsyrta	k1gFnSc1	Apsyrta
na	na	k7c4	na
schůzku	schůzka	k1gFnSc4	schůzka
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
<g/>
,	,	kIx,	,
vyskočil	vyskočit	k5eAaPmAgMnS	vyskočit
z	z	k7c2	z
úkrytu	úkryt	k1gInSc2	úkryt
Iásón	Iásón	k1gInSc1	Iásón
<g/>
,	,	kIx,	,
Apsyrta	Apsyrta	k1gFnSc1	Apsyrta
proklál	proklát	k5eAaPmAgInS	proklát
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
uťal	utít	k5eAaPmAgMnS	utít
mu	on	k3xPp3gMnSc3	on
všechny	všechen	k3xTgInPc4	všechen
údy	úd	k1gInPc4	úd
a	a	k8xC	a
tělo	tělo	k1gNnSc4	tělo
zahrabal	zahrabat	k5eAaPmAgMnS	zahrabat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
strašný	strašný	k2eAgInSc1d1	strašný
čin	čin	k1gInSc1	čin
viděly	vidět	k5eAaImAgFnP	vidět
Erínye	Eríny	k1gFnPc1	Eríny
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnPc1	bohyně
pomsty	pomsta	k1gFnSc2	pomsta
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
svou	svůj	k3xOyFgFnSc4	svůj
oběť	oběť	k1gFnSc4	oběť
pronásledují	pronásledovat	k5eAaImIp3nP	pronásledovat
jako	jako	k8xC	jako
štvané	štvaný	k2eAgNnSc4d1	štvané
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
si	se	k3xPyFc3	se
viník	viník	k1gMnSc1	viník
svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
neodpyká	odpykat	k5eNaPmIp3nS	odpykat
<g/>
.	.	kIx.	.
</s>
<s>
Médeia	Médeia	k1gFnSc1	Médeia
poté	poté	k6eAd1	poté
přivolala	přivolat	k5eAaPmAgFnS	přivolat
loď	loď	k1gFnSc1	loď
Argonautů	argonaut	k1gMnPc2	argonaut
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
v	v	k7c6	v
krvavé	krvavý	k2eAgFnSc6d1	krvavá
řeži	řež	k1gFnSc6	řež
vojsko	vojsko	k1gNnSc4	vojsko
Apsyrtovo	Apsyrtův	k2eAgNnSc4d1	Apsyrtův
porazili	porazit	k5eAaPmAgMnP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
poradě	porada	k1gFnSc6	porada
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Pélea	Péleus	k1gMnSc2	Péleus
odrazili	odrazit	k5eAaPmAgMnP	odrazit
s	s	k7c7	s
lodí	loď	k1gFnSc7	loď
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
plavili	plavit	k5eAaImAgMnP	plavit
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
Hylleů	Hylle	k1gInPc2	Hylle
<g/>
.	.	kIx.	.
</s>
<s>
Kolchidské	kolchidský	k2eAgNnSc1d1	kolchidský
vojsko	vojsko	k1gNnSc1	vojsko
vyrazilo	vyrazit	k5eAaPmAgNnS	vyrazit
je	on	k3xPp3gFnPc4	on
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bohyně	bohyně	k1gFnSc1	bohyně
Héra	Héra	k1gFnSc1	Héra
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
vichřici	vichřice	k1gFnSc4	vichřice
a	a	k8xC	a
bouři	bouře	k1gFnSc4	bouře
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
cesta	cesta	k1gFnSc1	cesta
pronásledovatelů	pronásledovatel	k1gMnPc2	pronásledovatel
byla	být	k5eAaImAgFnS	být
marná	marný	k2eAgFnSc1d1	marná
<g/>
.	.	kIx.	.
</s>
<s>
Vzdali	vzdát	k5eAaPmAgMnP	vzdát
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
těch	ten	k3xDgNnPc6	ten
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
do	do	k7c2	do
Kolchidy	Kolchida	k1gFnSc2	Kolchida
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevrátili	vrátit	k5eNaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Hylleů	Hylle	k1gMnPc2	Hylle
se	se	k3xPyFc4	se
Argonauté	argonaut	k1gMnPc1	argonaut
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dary	dar	k1gInPc7	dar
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Zeus	Zeus	k6eAd1	Zeus
se	se	k3xPyFc4	se
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
hněval	hněvat	k5eAaImAgMnS	hněvat
kvůli	kvůli	k7c3	kvůli
Iásonovu	Iásonův	k2eAgInSc3d1	Iásonův
zločinu	zločin	k1gInSc3	zločin
a	a	k8xC	a
za	za	k7c4	za
trest	trest	k1gInSc4	trest
jim	on	k3xPp3gMnPc3	on
určil	určit	k5eAaPmAgInS	určit
nesnáze	snadno	k6eNd2	snadno
a	a	k8xC	a
strasti	strast	k1gFnPc4	strast
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
Iásón	Iásón	k1gInSc1	Iásón
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
Apsyrta	Apsyrt	k1gMnSc2	Apsyrt
neočistí	očistit	k5eNaPmIp3nS	očistit
u	u	k7c2	u
kouzelnice	kouzelnice	k1gFnSc2	kouzelnice
Kirké	Kirký	k2eAgFnSc2d1	Kirký
<g/>
,	,	kIx,	,
Aiétovy	Aiétův	k2eAgFnSc2d1	Aiétův
sestry	sestra	k1gFnSc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
božský	božský	k2eAgInSc1d1	božský
úmysl	úmysl	k1gInSc1	úmysl
jim	on	k3xPp3gMnPc3	on
lidským	lidský	k2eAgInSc7d1	lidský
hlasem	hlas	k1gInSc7	hlas
oznámila	oznámit	k5eAaPmAgFnS	oznámit
sama	sám	k3xTgFnSc1	sám
loď	loď	k1gFnSc1	loď
Argó	Argó	k1gFnSc2	Argó
<g/>
,	,	kIx,	,
což	což	k9	což
plavce	plavec	k1gMnPc4	plavec
ještě	ještě	k6eAd1	ještě
víc	hodně	k6eAd2	hodně
vyděsilo	vyděsit	k5eAaPmAgNnS	vyděsit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
loď	loď	k1gFnSc1	loď
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
do	do	k7c2	do
proudů	proud	k1gInPc2	proud
Éridanu	Éridan	k1gInSc2	Éridan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
mohyla	mohyla	k1gFnSc1	mohyla
Faethontova	Faethontův	k2eAgFnSc1d1	Faethontův
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
zabil	zabít	k5eAaPmAgInS	zabít
Diův	Diův	k2eAgInSc1d1	Diův
blesk	blesk	k1gInSc1	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pluli	plout	k5eAaImAgMnP	plout
po	po	k7c6	po
prudkém	prudký	k2eAgInSc6d1	prudký
Rhodanu	Rhodan	k1gInSc6	Rhodan
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nehody	nehoda	k1gFnSc2	nehoda
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k9	až
k	k	k7c3	k
Aithalii	Aithalie	k1gFnSc3	Aithalie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnPc1d1	dnešní
Elba	Elbum	k1gNnPc1	Elbum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Héra	Héra	k1gFnSc1	Héra
je	být	k5eAaImIp3nS	být
hustou	hustý	k2eAgFnSc7d1	hustá
mlhou	mlha	k1gFnSc7	mlha
chránila	chránit	k5eAaImAgFnS	chránit
před	před	k7c4	před
útoky	útok	k1gInPc4	útok
místních	místní	k2eAgInPc2d1	místní
divokých	divoký	k2eAgInPc2d1	divoký
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
zdržování	zdržování	k1gNnSc2	zdržování
pluli	plout	k5eAaImAgMnP	plout
rychle	rychle	k6eAd1	rychle
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
dorazili	dorazit	k5eAaPmAgMnP	dorazit
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
Áia	Áia	k1gFnSc2	Áia
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlila	sídlit	k5eAaImAgFnS	sídlit
kouzelnice	kouzelnice	k1gFnSc1	kouzelnice
Kirké	Kirká	k1gFnSc2	Kirká
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
domu	dům	k1gInSc2	dům
Kirké	Kirká	k1gFnSc2	Kirká
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
Iásón	Iásón	k1gMnSc1	Iásón
a	a	k8xC	a
Médeia	Médeia	k1gFnSc1	Médeia
sami	sám	k3xTgMnPc1	sám
a	a	k8xC	a
podrobili	podrobit	k5eAaPmAgMnP	podrobit
se	se	k3xPyFc4	se
očistné	očistný	k2eAgFnSc3d1	očistná
oběti	oběť	k1gFnSc3	oběť
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
Kirké	Kirká	k1gFnSc2	Kirká
sňala	snít	k5eAaPmAgNnP	snít
obvinění	obvinění	k1gNnPc1	obvinění
z	z	k7c2	z
těžkého	těžký	k2eAgInSc2d1	těžký
zločinu	zločin	k1gInSc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Kirké	Kirká	k1gFnSc3	Kirká
však	však	k9	však
vyslechla	vyslechnout	k5eAaPmAgFnS	vyslechnout
Médeino	Médein	k2eAgNnSc4d1	Médein
vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c6	o
všech	všecek	k3xTgFnPc6	všecek
událostech	událost	k1gFnPc6	událost
v	v	k7c6	v
Kolchidě	Kolchida	k1gFnSc6	Kolchida
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
podvedla	podvést	k5eAaPmAgFnS	podvést
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
Aiéta	Aiét	k1gMnSc4	Aiét
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
pomohla	pomoct	k5eAaPmAgFnS	pomoct
Iásonovi	Iáson	k1gMnSc6	Iáson
s	s	k7c7	s
plněním	plnění	k1gNnSc7	plnění
všech	všecek	k3xTgInPc2	všecek
úkolů	úkol	k1gInPc2	úkol
a	a	k8xC	a
jak	jak	k8xS	jak
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
tajně	tajně	k6eAd1	tajně
opustila	opustit	k5eAaPmAgFnS	opustit
a	a	k8xC	a
utekla	utéct	k5eAaPmAgFnS	utéct
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Zamlčela	zamlčet	k5eAaPmAgFnS	zamlčet
jedině	jedině	k6eAd1	jedině
úkladnou	úkladný	k2eAgFnSc4d1	úkladná
vraždu	vražda	k1gFnSc4	vražda
Apsyrtovu	Apsyrtův	k2eAgFnSc4d1	Apsyrtův
<g/>
.	.	kIx.	.
</s>
<s>
Kirké	Kirký	k2eAgMnPc4d1	Kirký
všechno	všechen	k3xTgNnSc1	všechen
věděla	vědět	k5eAaImAgFnS	vědět
<g/>
,	,	kIx,	,
zradu	zrada	k1gFnSc4	zrada
jí	jíst	k5eAaImIp3nS	jíst
neodpustila	odpustit	k5eNaPmAgFnS	odpustit
a	a	k8xC	a
hanebný	hanebný	k2eAgInSc4d1	hanebný
útěk	útěk	k1gInSc4	útěk
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
nato	nato	k6eAd1	nato
opustili	opustit	k5eAaPmAgMnP	opustit
Kirčin	Kirčin	k2eAgInSc4d1	Kirčin
dům	dům	k1gInSc4	dům
a	a	k8xC	a
tu	tu	k6eAd1	tu
bohyně	bohyně	k1gFnSc1	bohyně
Héra	Héra	k1gFnSc1	Héra
a	a	k8xC	a
Iris	iris	k1gFnSc1	iris
zajistily	zajistit	k5eAaPmAgFnP	zajistit
jejich	jejich	k3xOp3gFnSc4	jejich
další	další	k2eAgFnSc4d1	další
hladkou	hladký	k2eAgFnSc4d1	hladká
a	a	k8xC	a
rychlou	rychlý	k2eAgFnSc4d1	rychlá
plavbu	plavba	k1gFnSc4	plavba
-	-	kIx~	-
Héfaistos	Héfaistos	k1gMnSc1	Héfaistos
zhasil	zhasit	k5eAaPmAgMnS	zhasit
svůj	svůj	k3xOyFgInSc4	svůj
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
Aiolos	Aiolos	k1gMnSc1	Aiolos
utišil	utišit	k5eAaPmAgMnS	utišit
všechny	všechen	k3xTgInPc4	všechen
protivětry	protivítr	k1gInPc4	protivítr
a	a	k8xC	a
Thetis	Thetis	k1gFnPc4	Thetis
zajistila	zajistit	k5eAaPmAgFnS	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Skylla	Skylla	k1gFnSc1	Skylla
a	a	k8xC	a
Charybdis	Charybdis	k1gFnSc1	Charybdis
loď	loď	k1gFnSc1	loď
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
dravých	dravý	k2eAgFnPc6d1	dravá
vodách	voda	k1gFnPc6	voda
nezničily	zničit	k5eNaPmAgFnP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
již	již	k9	již
Péleus	Péleus	k1gMnSc1	Péleus
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
bohyně	bohyně	k1gFnSc2	bohyně
Thetis	Thetis	k1gFnSc2	Thetis
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Achilleův	Achilleův	k2eAgMnSc1d1	Achilleův
<g/>
,	,	kIx,	,
vybídl	vybídnout	k5eAaPmAgMnS	vybídnout
plavce	plavec	k1gMnPc4	plavec
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
odplutí	odplutí	k1gNnSc3	odplutí
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
jako	jako	k9	jako
střela	střela	k1gFnSc1	střela
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
míjeli	míjet	k5eAaImAgMnP	míjet
ostrov	ostrov	k1gInSc4	ostrov
Sirén	Siréna	k1gFnPc2	Siréna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
lákaly	lákat	k5eAaImAgFnP	lákat
plavce	plavec	k1gMnPc4	plavec
tklivým	tklivý	k2eAgInSc7d1	tklivý
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
začal	začít	k5eAaPmAgMnS	začít
zpívat	zpívat	k5eAaImF	zpívat
Orfeus	Orfeus	k1gMnSc1	Orfeus
<g/>
,	,	kIx,	,
oněměly	oněmět	k5eAaPmAgInP	oněmět
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
zpěvu	zpěv	k1gInSc6	zpěv
Sirény	Siréna	k1gFnSc2	Siréna
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
tak	tak	k6eAd1	tak
nezískaly	získat	k5eNaPmAgFnP	získat
žádnou	žádný	k3yNgFnSc4	žádný
oběť	oběť	k1gFnSc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
loď	loď	k1gFnSc1	loď
u	u	k7c2	u
obludné	obludný	k2eAgFnSc2d1	obludná
Skylly	Skylla	k1gFnSc2	Skylla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
dvanáct	dvanáct	k4xCc4	dvanáct
noh	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
krků	krk	k1gInPc2	krk
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
tlam	tlama	k1gFnPc2	tlama
a	a	k8xC	a
štěkala	štěkat	k5eAaImAgFnS	štěkat
jako	jako	k8xS	jako
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
lodi	loď	k1gFnSc3	loď
připluly	připlout	k5eAaPmAgInP	připlout
mořské	mořský	k2eAgInPc1d1	mořský
víly	víla	k1gFnPc4	víla
i	i	k8xC	i
s	s	k7c7	s
mořskou	mořský	k2eAgFnSc7d1	mořská
bohyní	bohyně	k1gFnSc7	bohyně
Thetidou	Thetida	k1gFnSc7	Thetida
a	a	k8xC	a
loď	loď	k1gFnSc4	loď
protlačily	protlačit	k5eAaPmAgInP	protlačit
kolem	kolem	k7c2	kolem
obou	dva	k4xCgFnPc2	dva
příšer	příšera	k1gFnPc2	příšera
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc4	všechen
sledovaly	sledovat	k5eAaImAgInP	sledovat
z	z	k7c2	z
Olympu	Olymp	k1gInSc2	Olymp
bohyně	bohyně	k1gFnSc1	bohyně
Héra	Héra	k1gFnSc1	Héra
a	a	k8xC	a
Athéna	Athéna	k1gFnSc1	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
volném	volný	k2eAgNnSc6d1	volné
moři	moře	k1gNnSc6	moře
je	být	k5eAaImIp3nS	být
víly	víla	k1gFnPc1	víla
i	i	k8xC	i
bohyně	bohyně	k1gFnPc1	bohyně
opustily	opustit	k5eAaPmAgFnP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
plula	plout	k5eAaImAgFnS	plout
kolem	kolem	k7c2	kolem
sicilských	sicilský	k2eAgInPc2d1	sicilský
břehů	břeh	k1gInPc2	břeh
a	a	k8xC	a
dorazila	dorazit	k5eAaPmAgFnS	dorazit
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
Fajáků	Faják	k1gMnPc2	Faják
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
vládl	vládnout	k5eAaImAgMnS	vládnout
král	král	k1gMnSc1	král
Alkinoos	Alkinoos	k1gMnSc1	Alkinoos
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
však	však	k9	však
připlulo	připlout	k5eAaPmAgNnS	připlout
i	i	k9	i
vojsko	vojsko	k1gNnSc1	vojsko
krále	král	k1gMnSc2	král
Aiéta	Aiét	k1gMnSc2	Aiét
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pronásledoval	pronásledovat	k5eAaImAgMnS	pronásledovat
dceru	dcera	k1gFnSc4	dcera
Médeiu	Médeius	k1gMnSc3	Médeius
a	a	k8xC	a
žádal	žádat	k5eAaImAgInS	žádat
její	její	k3xOp3gNnSc4	její
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
hrozil	hrozit	k5eAaImAgMnS	hrozit
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
za	za	k7c4	za
Médeiu	Médeium	k1gNnSc3	Médeium
přimlouvali	přimlouvat	k5eAaImAgMnP	přimlouvat
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Alkinoos	Alkinoos	k1gMnSc1	Alkinoos
tedy	tedy	k9	tedy
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
ještě	ještě	k6eAd1	ještě
dívkou	dívka	k1gFnSc7	dívka
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
vydána	vydán	k2eAgFnSc1d1	vydána
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
však	však	k9	však
již	již	k6eAd1	již
manželkou	manželka	k1gFnSc7	manželka
Iásonovou	Iásonový	k2eAgFnSc7d1	Iásonový
<g/>
,	,	kIx,	,
zůstane	zůstat	k5eAaPmIp3nS	zůstat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
neprodleně	prodleně	k6eNd1	prodleně
konala	konat	k5eAaImAgFnS	konat
jejich	jejich	k3xOp3gFnSc1	jejich
svatba	svatba	k1gFnSc1	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Kolchidští	kolchidský	k2eAgMnPc1d1	kolchidský
vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
báli	bát	k5eAaImAgMnP	bát
trestu	trest	k1gInSc2	trest
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc4	svůj
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
raději	rád	k6eAd2	rád
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
svolení	svolení	k1gNnSc4	svolení
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c6	na
sousedním	sousední	k2eAgInSc6d1	sousední
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
Argonautů	argonaut	k1gMnPc2	argonaut
také	také	k9	také
záhy	záhy	k6eAd1	záhy
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
dny	den	k1gInPc4	den
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
nepřálo	přát	k5eNaImAgNnS	přát
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
severák	severák	k1gInSc1	severák
je	být	k5eAaImIp3nS	být
zahnal	zahnat	k5eAaPmAgMnS	zahnat
k	k	k7c3	k
písčitým	písčitý	k2eAgFnPc3d1	písčitá
Syrtám	Syrta	k1gFnPc3	Syrta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
mělčiny	mělčina	k1gFnPc1	mělčina
s	s	k7c7	s
hustou	hustý	k2eAgFnSc7d1	hustá
spletí	spleť	k1gFnSc7	spleť
řas	řasa	k1gFnPc2	řasa
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nikdo	nikdo	k3yNnSc1	nikdo
nevyvázne	vyváznout	k5eNaPmIp3nS	vyváznout
<g/>
.	.	kIx.	.
</s>
<s>
Plavci	plavec	k1gMnPc1	plavec
propadali	propadat	k5eAaImAgMnP	propadat
beznaději	beznaděje	k1gFnSc4	beznaděje
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přišly	přijít	k5eAaPmAgFnP	přijít
jim	on	k3xPp3gMnPc3	on
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
víly	víla	k1gFnSc2	víla
<g/>
,	,	kIx,	,
ochránkyně	ochránkyně	k1gFnSc2	ochránkyně
pusté	pustý	k2eAgFnSc2d1	pustá
Libye	Libye	k1gFnSc2	Libye
a	a	k8xC	a
slíbily	slíbit	k5eAaPmAgFnP	slíbit
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
obrovský	obrovský	k2eAgMnSc1d1	obrovský
kůň	kůň	k1gMnSc1	kůň
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
hřívou	hříva	k1gFnSc7	hříva
<g/>
,	,	kIx,	,
plavci	plavec	k1gMnPc1	plavec
nesli	nést	k5eAaImAgMnP	nést
svou	svůj	k3xOyFgFnSc4	svůj
loď	loď	k1gFnSc4	loď
na	na	k7c6	na
ramenou	rameno	k1gNnPc6	rameno
dvanáct	dvanáct	k4xCc1	dvanáct
dní	den	k1gInPc2	den
a	a	k8xC	a
nocí	noc	k1gFnPc2	noc
a	a	k8xC	a
kůň	kůň	k1gMnSc1	kůň
je	být	k5eAaImIp3nS	být
dovedl	dovést	k5eAaPmAgMnS	dovést
až	až	k9	až
k	k	k7c3	k
trítónské	trítónský	k2eAgFnSc3d1	trítónský
zátoce	zátoka	k1gFnSc3	zátoka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
hledali	hledat	k5eAaImAgMnP	hledat
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
dostali	dostat	k5eAaPmAgMnP	dostat
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k9	ještě
včera	včera	k6eAd1	včera
Héraklés	Héraklés	k1gInSc4	Héraklés
usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
draka	drak	k1gMnSc4	drak
hlídajícího	hlídající	k2eAgInSc2d1	hlídající
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
jablka	jablko	k1gNnPc4	jablko
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
Hesperidek	Hesperidka	k1gFnPc2	Hesperidka
<g/>
.	.	kIx.	.
</s>
<s>
Argonauti	argonaut	k1gMnPc1	argonaut
spatřili	spatřit	k5eAaPmAgMnP	spatřit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
drak	drak	k1gMnSc1	drak
ještě	ještě	k6eAd1	ještě
škubal	škubat	k5eAaImAgMnS	škubat
<g/>
!	!	kIx.	!
</s>
<s>
A	a	k9	a
vzápětí	vzápětí	k6eAd1	vzápětí
také	také	k9	také
našli	najít	k5eAaPmAgMnP	najít
pramen	pramen	k1gInSc4	pramen
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Héraklés	Héraklés	k1gInSc4	Héraklés
kopnutím	kopnutí	k1gNnSc7	kopnutí
paty	pata	k1gFnSc2	pata
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hérakla	Hérakles	k1gMnSc4	Hérakles
samotného	samotný	k2eAgMnSc4d1	samotný
však	však	k8xC	však
už	už	k6eAd1	už
nezastihli	zastihnout	k5eNaPmAgMnP	zastihnout
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
netušili	tušit	k5eNaImAgMnP	tušit
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Polyfémos	Polyfémos	k1gMnSc1	Polyfémos
po	po	k7c6	po
rozloučení	rozloučení	k1gNnSc6	rozloučení
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
po	po	k7c6	po
lodi	loď	k1gFnSc6	loď
dlouho	dlouho	k6eAd1	dlouho
pátral	pátrat	k5eAaImAgMnS	pátrat
<g/>
,	,	kIx,	,
prošel	projít	k5eAaPmAgInS	projít
daleké	daleký	k2eAgInPc4d1	daleký
kraje	kraj	k1gInPc4	kraj
a	a	k8xC	a
osud	osud	k1gInSc4	osud
ho	on	k3xPp3gInSc4	on
zahubil	zahubit	k5eAaPmAgInS	zahubit
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
zemřel	zemřít	k5eAaPmAgMnS	zemřít
také	také	k6eAd1	také
jejich	jejich	k3xOp3gMnSc1	jejich
druhý	druhý	k4xOgMnSc1	druhý
věštec	věštec	k1gMnSc1	věštec
Mopsos	Mopsos	k1gMnSc1	Mopsos
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
šlápl	šlápnout	k5eAaPmAgMnS	šlápnout
na	na	k7c4	na
jedovatého	jedovatý	k2eAgMnSc4d1	jedovatý
hada	had	k1gMnSc4	had
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
boha	bůh	k1gMnSc2	bůh
Tritóna	Tritón	k1gMnSc2	Tritón
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c4	na
hluboké	hluboký	k2eAgNnSc4d1	hluboké
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
přistáli	přistát	k5eAaImAgMnP	přistát
na	na	k7c6	na
ostrůvku	ostrůvek	k1gInSc6	ostrůvek
mezi	mezi	k7c7	mezi
Krétou	Kréta	k1gFnSc7	Kréta
a	a	k8xC	a
Rhodem	Rhodos	k1gInSc7	Rhodos
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vplouvání	vplouvání	k1gNnSc6	vplouvání
do	do	k7c2	do
krétského	krétský	k2eAgInSc2d1	krétský
přístavu	přístav	k1gInSc2	přístav
jim	on	k3xPp3gMnPc3	on
bránil	bránit	k5eAaImAgInS	bránit
kovový	kovový	k2eAgMnSc1d1	kovový
obr	obr	k1gMnSc1	obr
Talós	Talósa	k1gFnPc2	Talósa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
vrhal	vrhat	k5eAaImAgMnS	vrhat
obrovské	obrovský	k2eAgInPc4d1	obrovský
kameny	kámen	k1gInPc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Médeia	Médeia	k1gFnSc1	Médeia
obra	obr	k1gMnSc2	obr
zahubila	zahubit	k5eAaPmAgFnS	zahubit
svými	svůj	k3xOyFgNnPc7	svůj
kouzly	kouzlo	k1gNnPc7	kouzlo
<g/>
,	,	kIx,	,
omámila	omámit	k5eAaPmAgFnS	omámit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
narazil	narazit	k5eAaPmAgMnS	narazit
nohou	noha	k1gFnSc7	noha
do	do	k7c2	do
skaliska	skalisko	k1gNnSc2	skalisko
<g/>
,	,	kIx,	,
zranil	zranit	k5eAaPmAgMnS	zranit
se	se	k3xPyFc4	se
a	a	k8xC	a
z	z	k7c2	z
kotníku	kotník	k1gInSc2	kotník
mu	on	k3xPp3gMnSc3	on
vytryskla	vytrysknout	k5eAaPmAgFnS	vytrysknout
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
zřítil	zřítit	k5eAaPmAgInS	zřítit
ze	z	k7c2	z
skály	skála	k1gFnSc2	skála
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
propluli	proplout	k5eAaPmAgMnP	proplout
kolem	kolem	k7c2	kolem
Aigíny	Aigína	k1gFnSc2	Aigína
<g/>
,	,	kIx,	,
vzdali	vzdát	k5eAaPmAgMnP	vzdát
oběť	oběť	k1gFnSc4	oběť
bohům	bůh	k1gMnPc3	bůh
<g/>
,	,	kIx,	,
pluli	plout	k5eAaImAgMnP	plout
kolem	kolem	k7c2	kolem
Attiky	Attika	k1gFnSc2	Attika
se	s	k7c7	s
slavnými	slavný	k2eAgFnPc7d1	slavná
Athénami	Athéna	k1gFnPc7	Athéna
<g/>
,	,	kIx,	,
minuli	minout	k5eAaImAgMnP	minout
Aulidu	Aulida	k1gFnSc4	Aulida
a	a	k8xC	a
kolem	kolem	k7c2	kolem
eubojských	eubojský	k2eAgInPc2d1	eubojský
břehů	břeh	k1gInPc2	břeh
dopluli	doplout	k5eAaPmAgMnP	doplout
do	do	k7c2	do
Iólku	Iólk	k1gInSc2	Iólk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přístavišti	přístaviště	k1gNnSc6	přístaviště
je	být	k5eAaImIp3nS	být
vítaly	vítat	k5eAaImAgInP	vítat
obrovské	obrovský	k2eAgInPc4d1	obrovský
zástupy	zástup	k1gInPc4	zástup
jásajících	jásající	k2eAgMnPc2d1	jásající
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Iásón	Iásón	k1gInSc1	Iásón
se	se	k3xPyFc4	se
rozhlížel	rozhlížet	k5eAaImAgInS	rozhlížet
po	po	k7c6	po
svých	svůj	k3xOyFgFnPc6	svůj
starých	starý	k2eAgFnPc6d1	stará
rodičích	rodič	k1gMnPc6	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Prosil	prosít	k5eAaPmAgMnS	prosít
Médeiu	Médeius	k1gMnSc3	Médeius
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gMnSc3	jeho
otci	otec	k1gMnSc3	otec
Aisonovi	Aison	k1gMnSc3	Aison
přidala	přidat	k5eAaPmAgFnS	přidat
několik	několik	k4yIc4	několik
roků	rok	k1gInPc2	rok
života	život	k1gInSc2	život
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
slíbila	slíbit	k5eAaPmAgFnS	slíbit
<g/>
.	.	kIx.	.
</s>
<s>
Iásón	Iásón	k1gMnSc1	Iásón
přivedl	přivést	k5eAaPmAgMnS	přivést
Médeiu	Médeius	k1gMnSc3	Médeius
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
rodičům	rodič	k1gMnPc3	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
šťastni	šťasten	k2eAgMnPc1d1	šťasten
<g/>
.	.	kIx.	.
</s>
<s>
Médeia	Médeia	k1gFnSc1	Médeia
následující	následující	k2eAgFnSc1d1	následující
noc	noc	k1gFnSc1	noc
nasbírala	nasbírat	k5eAaPmAgFnS	nasbírat
čarovné	čarovný	k2eAgFnPc4d1	čarovná
byliny	bylina	k1gFnPc4	bylina
<g/>
,	,	kIx,	,
uvařila	uvařit	k5eAaPmAgFnS	uvařit
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
šťávu	šťáva	k1gFnSc4	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
Nechala	nechat	k5eAaPmAgFnS	nechat
vynést	vynést	k5eAaPmF	vynést
Aisona	Aisona	k1gFnSc1	Aisona
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
uspala	uspat	k5eAaPmAgFnS	uspat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
probodla	probodnout	k5eAaPmAgFnS	probodnout
mu	on	k3xPp3gMnSc3	on
hrdlo	hrdlo	k1gNnSc1	hrdlo
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
vytéci	vytéct	k5eAaPmF	vytéct
krev	krev	k1gFnSc1	krev
<g/>
.	.	kIx.	.
</s>
<s>
Napustila	napustit	k5eAaPmAgFnS	napustit
žíly	žíla	k1gFnSc2	žíla
kouzelnou	kouzelný	k2eAgFnSc7d1	kouzelná
šťávou	šťáva	k1gFnSc7	šťáva
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
otcovy	otcův	k2eAgInPc1d1	otcův
vlasy	vlas	k1gInPc1	vlas
nebyly	být	k5eNaImAgInP	být
šedivé	šedivý	k2eAgInPc1d1	šedivý
<g/>
,	,	kIx,	,
zmizely	zmizet	k5eAaPmAgInP	zmizet
vrásky	vrásek	k1gInPc1	vrásek
<g/>
,	,	kIx,	,
z	z	k7c2	z
Aisona	Aison	k1gMnSc2	Aison
byl	být	k5eAaImAgMnS	být
mladý	mladý	k2eAgMnSc1d1	mladý
silný	silný	k2eAgMnSc1d1	silný
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
přítomní	přítomný	k1gMnPc1	přítomný
užasli	užasnout	k5eAaPmAgMnP	užasnout
<g/>
,	,	kIx,	,
radovali	radovat	k5eAaImAgMnP	radovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pociťovali	pociťovat	k5eAaImAgMnP	pociťovat
bázeň	bázeň	k1gFnSc4	bázeň
před	před	k7c7	před
Médeiou	Médeia	k1gFnSc7	Médeia
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
oslava	oslava	k1gFnSc1	oslava
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
vzpomínali	vzpomínat	k5eAaImAgMnP	vzpomínat
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
cestu	cesta	k1gFnSc4	cesta
a	a	k8xC	a
slibovali	slibovat	k5eAaImAgMnP	slibovat
věrnost	věrnost	k1gFnSc4	věrnost
otčině	otčina	k1gFnSc3	otčina
<g/>
.	.	kIx.	.
</s>
<s>
Plavci	plavec	k1gMnPc1	plavec
z	z	k7c2	z
lodi	loď	k1gFnSc2	loď
Argó	Argó	k1gFnPc1	Argó
opouštěli	opouštět	k5eAaImAgMnP	opouštět
Aisonův	Aisonův	k2eAgInSc4d1	Aisonův
dům	dům	k1gInSc4	dům
a	a	k8xC	a
vykročili	vykročit	k5eAaPmAgMnP	vykročit
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
domovům	domov	k1gInPc3	domov
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
poté	poté	k6eAd1	poté
Iásón	Iásón	k1gMnSc1	Iásón
šel	jít	k5eAaImAgMnS	jít
za	za	k7c7	za
králem	král	k1gMnSc7	král
Peliem	Pelius	k1gMnSc7	Pelius
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
zpět	zpět	k6eAd1	zpět
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Iólkem	Iólko	k1gNnSc7	Iólko
<g/>
,	,	kIx,	,
nepochodil	nepochodil	k?	nepochodil
<g/>
.	.	kIx.	.
</s>
<s>
Peliás	Peliás	k1gInSc1	Peliás
se	se	k3xPyFc4	se
nemínil	mínit	k5eNaImAgInS	mínit
vlády	vláda	k1gFnSc2	vláda
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nechtěl	chtít	k5eNaImAgMnS	chtít
vracejícího	vracející	k2eAgMnSc4d1	vracející
se	se	k3xPyFc4	se
Iásona	Iásona	k1gFnSc1	Iásona
vůbec	vůbec	k9	vůbec
přijmout	přijmout	k5eAaPmF	přijmout
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
ukládal	ukládat	k5eAaImAgInS	ukládat
o	o	k7c4	o
život	život	k1gInSc4	život
také	také	k9	také
jeho	jeho	k3xOp3gMnPc3	jeho
rodičům	rodič	k1gMnPc3	rodič
a	a	k8xC	a
příbuzenstvu	příbuzenstvo	k1gNnSc3	příbuzenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Iásón	Iásón	k1gMnSc1	Iásón
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
ho	on	k3xPp3gNnSc2	on
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Médeii	Médeie	k1gFnSc4	Médeie
zavraždit	zavraždit	k5eAaPmF	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Médeia	Médeia	k1gFnSc1	Médeia
slíbila	slíbit	k5eAaPmAgFnS	slíbit
Peliovým	Peliový	k2eAgFnPc3d1	Peliový
dcerám	dcera	k1gFnPc3	dcera
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnPc1	jejich
otce	otec	k1gMnSc4	otec
omladí	omladit	k5eAaPmIp3nP	omladit
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
otce	otec	k1gMnPc4	otec
Iásonova	Iásonův	k2eAgFnSc1d1	Iásonova
<g/>
,	,	kIx,	,
donutila	donutit	k5eAaPmAgFnS	donutit
dcery	dcera	k1gFnSc2	dcera
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
samy	sám	k3xTgInPc1	sám
otcovi	otcův	k2eAgMnPc1d1	otcův
otevřely	otevřít	k5eAaPmAgFnP	otevřít
žíly	žíla	k1gFnPc4	žíla
<g/>
,	,	kIx,	,
vypustila	vypustit	k5eAaPmAgFnS	vypustit
mu	on	k3xPp3gMnSc3	on
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nenahradila	nahradit	k5eNaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
žádnou	žádný	k3yNgFnSc7	žádný
zázračnou	zázračný	k2eAgFnSc7d1	zázračná
šťávou	šťáva	k1gFnSc7	šťáva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odvarem	odvar	k1gInSc7	odvar
z	z	k7c2	z
neúčinných	účinný	k2eNgFnPc2d1	neúčinná
travin	travina	k1gFnPc2	travina
<g/>
.	.	kIx.	.
</s>
<s>
Peliás	Peliás	k6eAd1	Peliás
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
se	se	k3xPyFc4	se
Iásón	Iásón	k1gMnSc1	Iásón
nedočkal	dočkat	k5eNaPmAgMnS	dočkat
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odplatou	odplata	k1gFnSc7	odplata
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
Peliovu	Peliův	k2eAgFnSc4d1	Peliův
je	on	k3xPp3gNnPc4	on
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
z	z	k7c2	z
Iólku	Iólk	k1gInSc2	Iólk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
cestách	cesta	k1gFnPc6	cesta
našli	najít	k5eAaPmAgMnP	najít
útočiště	útočiště	k1gNnSc4	útočiště
u	u	k7c2	u
korinthského	korinthský	k2eAgMnSc2d1	korinthský
krále	král	k1gMnSc2	král
Kreonta	Kreont	k1gMnSc2	Kreont
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
jejich	jejich	k3xOp3gMnPc1	jejich
dva	dva	k4xCgMnPc4	dva
synové	syn	k1gMnPc1	syn
Mermeros	Mermerosa	k1gFnPc2	Mermerosa
a	a	k8xC	a
Ferétés	Ferétésa	k1gFnPc2	Ferétésa
<g/>
.	.	kIx.	.
</s>
<s>
Médeia	Médeia	k1gFnSc1	Médeia
doufala	doufat	k5eAaImAgFnS	doufat
v	v	k7c4	v
rodinné	rodinný	k2eAgNnSc4d1	rodinné
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
Iásón	Iásón	k1gMnSc1	Iásón
bažil	bažit	k5eAaImAgMnS	bažit
po	po	k7c6	po
moci	moc	k1gFnSc6	moc
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zradil	zradit	k5eAaPmAgMnS	zradit
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
zapudit	zapudit	k5eAaPmF	zapudit
Médeiu	Médeius	k1gMnSc3	Médeius
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
láska	láska	k1gFnSc1	láska
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c4	v
nenávist	nenávist	k1gFnSc4	nenávist
<g/>
.	.	kIx.	.
</s>
<s>
Zahubila	zahubit	k5eAaPmAgFnS	zahubit
svou	svůj	k3xOyFgFnSc4	svůj
sokyni	sokyně	k1gFnSc4	sokyně
Glauku	Glauk	k1gInSc2	Glauk
i	i	k8xC	i
Kreonta	Kreont	k1gInSc2	Kreont
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
své	svůj	k3xOyFgMnPc4	svůj
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
neštěstí	neštěstí	k1gNnSc1	neštěstí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
strůjcem	strůjce	k1gMnSc7	strůjce
<g/>
,	,	kIx,	,
vzalo	vzít	k5eAaPmAgNnS	vzít
Iásonovi	Iáson	k1gMnSc3	Iáson
přízeň	přízeň	k1gFnSc4	přízeň
i	i	k8xC	i
přátelství	přátelství	k1gNnSc4	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
psancem	psanec	k1gMnSc7	psanec
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
Argó	Argó	k1gFnSc2	Argó
byla	být	k5eAaImAgFnS	být
uctívána	uctíván	k2eAgFnSc1d1	uctívána
jako	jako	k8xC	jako
posvátná	posvátný	k2eAgFnSc1d1	posvátná
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
trouchnivěla	trouchnivět	k5eAaImAgFnS	trouchnivět
<g/>
,	,	kIx,	,
usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
stínu	stín	k1gInSc6	stín
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
zádí	zádí	k1gNnSc4	zádí
zřítila	zřítit	k5eAaPmAgFnS	zřítit
a	a	k8xC	a
zabila	zabít	k5eAaPmAgFnS	zabít
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
báje	báje	k1gFnSc1	báje
o	o	k7c6	o
Argonautech	argonaut	k1gMnPc6	argonaut
se	se	k3xPyFc4	se
odrazila	odrazit	k5eAaPmAgFnS	odrazit
zejména	zejména	k9	zejména
ve	v	k7c6	v
vázovém	vázový	k2eAgNnSc6d1	vázový
malířství	malířství	k1gNnSc6	malířství
<g/>
,	,	kIx,	,
freskách	freska	k1gFnPc6	freska
a	a	k8xC	a
terakotách	terakota	k1gFnPc6	terakota
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
na	na	k7c6	na
bronzové	bronzový	k2eAgFnSc6d1	bronzová
nádobě	nádoba	k1gFnSc6	nádoba
Cista	cista	k1gFnSc1	cista
Ficoroni	Ficoron	k1gMnPc1	Ficoron
z	z	k7c2	z
Praeneste	Praenest	k1gInSc5	Praenest
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
400	[number]	k4	400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
uložena	uložen	k2eAgNnPc1d1	uloženo
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
Mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c6	o
Argonautech	argonaut	k1gMnPc6	argonaut
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
starý	starý	k2eAgMnSc1d1	starý
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
epizody	epizoda	k1gFnPc1	epizoda
jsou	být	k5eAaImIp3nP	být
vyprávěny	vyprávět	k5eAaImNgFnP	vyprávět
už	už	k6eAd1	už
u	u	k7c2	u
Homéra	Homér	k1gMnSc2	Homér
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
příběh	příběh	k1gInSc1	příběh
byl	být	k5eAaImAgInS	být
vyprávěn	vyprávět	k5eAaImNgInS	vyprávět
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
verzích	verze	k1gFnPc6	verze
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgFnP	lišit
v	v	k7c4	v
líčení	líčení	k1gNnSc4	líčení
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
v	v	k7c6	v
geografických	geografický	k2eAgInPc6d1	geografický
údajích	údaj	k1gInPc6	údaj
<g/>
,	,	kIx,	,
v	v	k7c6	v
popisech	popis	k1gInPc6	popis
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
.	.	kIx.	.
první	první	k4xOgNnSc4	první
obšírné	obšírný	k2eAgNnSc4d1	obšírné
vyprávění	vyprávění	k1gNnSc4	vyprávění
osudů	osud	k1gInPc2	osud
Argonautů	argonaut	k1gMnPc2	argonaut
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
řeckého	řecký	k2eAgMnSc2d1	řecký
básníka	básník	k1gMnSc2	básník
Pindara	Pindar	k1gMnSc2	Pindar
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
5	[number]	k4	5
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
básnicky	básnicky	k6eAd1	básnicky
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
<g />
.	.	kIx.	.
</s>
<s>
souborně	souborně	k6eAd1	souborně
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
Apollonios	Apollonios	k1gInSc1	Apollonios
z	z	k7c2	z
Rhodu	Rhodos	k1gInSc2	Rhodos
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
čtyřdílném	čtyřdílný	k2eAgInSc6d1	čtyřdílný
eposu	epos	k1gInSc6	epos
Argonautika	Argonautika	k1gFnSc1	Argonautika
Gerhard	Gerhard	k1gInSc1	Gerhard
Löwe	Löwe	k1gFnSc1	Löwe
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnPc1	antika
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
237	[number]	k4	237
<g/>
-	-	kIx~	-
<g/>
3938	[number]	k4	3938
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Publius	Publius	k1gMnSc1	Publius
Ovidius	Ovidius	k1gMnSc1	Ovidius
Naso	Naso	k1gMnSc1	Naso
<g/>
,	,	kIx,	,
Proměny	proměna	k1gFnPc1	proměna
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mertlík	Mertlík	k1gInSc1	Mertlík
<g/>
,	,	kIx,	,
Starověké	starověký	k2eAgFnSc2d1	starověká
báje	báj	k1gFnSc2	báj
a	a	k8xC	a
pověsti	pověst	k1gFnSc2	pověst
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
<g/>
.	.	kIx.	.
</s>
