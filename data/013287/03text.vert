<p>
<s>
Kulička	kulička	k1gFnSc1	kulička
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Boule	boule	k1gFnSc2	boule
de	de	k?	de
Suif	Suif	k1gInSc1	Suif
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
povídka	povídka	k1gFnSc1	povídka
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Maupassanta	Maupassanta	k1gFnSc1	Maupassanta
napsaná	napsaný	k2eAgFnSc1d1	napsaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
první	první	k4xOgNnSc1	první
zveřejnění	zveřejnění	k1gNnSc1	zveřejnění
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
autor	autor	k1gMnSc1	autor
přečetl	přečíst	k5eAaPmAgMnS	přečíst
svým	svůj	k3xOyFgMnPc3	svůj
přátelům	přítel	k1gMnPc3	přítel
v	v	k7c6	v
literárním	literární	k2eAgInSc6d1	literární
kroužku	kroužek	k1gInSc6	kroužek
zvaném	zvaný	k2eAgInSc6d1	zvaný
Médan	Médany	k1gInPc2	Médany
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Les	les	k1gInSc1	les
Soirées	Soiréesa	k1gFnPc2	Soiréesa
de	de	k?	de
Médan	Médana	k1gFnPc2	Médana
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
Maupassantových	Maupassantův	k2eAgFnPc2d1	Maupassantova
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
pod	pod	k7c7	pod
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
autor	autor	k1gMnSc1	autor
publikoval	publikovat	k5eAaBmAgMnS	publikovat
i	i	k9	i
sbírku	sbírka	k1gFnSc4	sbírka
svých	svůj	k3xOyFgFnPc2	svůj
krátkých	krátký	k2eAgFnPc2d1	krátká
povídek	povídka	k1gFnPc2	povídka
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
francouzsko-pruské	francouzskoruský	k2eAgFnSc2d1	francouzsko-pruská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
==	==	k?	==
</s>
</p>
<p>
<s>
Dějištěm	dějiště	k1gNnSc7	dějiště
je	být	k5eAaImIp3nS	být
Normandie	Normandie	k1gFnSc1	Normandie
<g/>
,	,	kIx,	,
rok	rok	k1gInSc1	rok
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
období	období	k1gNnSc4	období
prusko-francouzské	pruskorancouzský	k2eAgFnSc2d1	prusko-francouzská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
už	už	k9	už
táhly	táhnout	k5eAaImAgFnP	táhnout
přes	přes	k7c4	přes
město	město	k1gNnSc4	město
trosky	troska	k1gFnSc2	troska
rozprášené	rozprášený	k2eAgFnSc2d1	rozprášená
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
nacházeli	nacházet	k5eAaImAgMnP	nacházet
útočiště	útočiště	k1gNnSc4	útočiště
v	v	k7c6	v
domech	dům	k1gInPc6	dům
nepřátelských	přátelský	k2eNgMnPc2d1	nepřátelský
poražených	poražený	k1gMnPc2	poražený
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
předstírali	předstírat	k5eAaImAgMnP	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
nechtějí	chtít	k5eNaImIp3nP	chtít
znát	znát	k5eAaImF	znát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doma	doma	k6eAd1	doma
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
byli	být	k5eAaImAgMnP	být
vcelku	vcelku	k6eAd1	vcelku
zadobře	zadobře	k6eAd1	zadobře
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
si	se	k3xPyFc3	se
dodali	dodat	k5eAaPmAgMnP	dodat
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
obchodníkům	obchodník	k1gMnPc3	obchodník
znovu	znovu	k6eAd1	znovu
ovládly	ovládnout	k5eAaPmAgInP	ovládnout
mysl	mysl	k1gFnSc4	mysl
potřeby	potřeba	k1gFnSc2	potřeba
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
sázce	sázka	k1gFnSc6	sázka
velké	velký	k2eAgInPc4d1	velký
peníze	peníz	k1gInPc4	peníz
v	v	k7c6	v
Havru	Havro	k1gNnSc6	Havro
–	–	k?	–
chtěli	chtít	k5eAaImAgMnP	chtít
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
dojet	dojet	k5eAaPmF	dojet
do	do	k7c2	do
Dieppu	Diepp	k1gInSc2	Diepp
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
do	do	k7c2	do
Havru	Havr	k1gInSc2	Havr
snad	snad	k9	snad
dostali	dostat	k5eAaPmAgMnP	dostat
lodí	loď	k1gFnSc7	loď
<g/>
.	.	kIx.	.
</s>
<s>
Použili	použít	k5eAaPmAgMnP	použít
vlivu	vliv	k1gInSc2	vliv
německých	německý	k2eAgMnPc2d1	německý
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
se	se	k3xPyFc4	se
znali	znát	k5eAaImAgMnP	znát
<g/>
,	,	kIx,	,
dostali	dostat	k5eAaPmAgMnP	dostat
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
odjezdu	odjezd	k1gInSc3	odjezd
podepsané	podepsaný	k2eAgFnSc2d1	podepsaná
velícím	velící	k2eAgMnSc7d1	velící
generálem	generál	k1gMnSc7	generál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
zima	zima	k1gFnSc1	zima
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
v	v	k7c4	v
půl	půl	k1xP	půl
páté	pátá	k1gFnSc2	pátá
ráno	ráno	k6eAd1	ráno
od	od	k7c2	od
Normandského	normandský	k2eAgInSc2d1	normandský
hotelu	hotel	k1gInSc2	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Nasedli	nasednout	k5eAaPmAgMnP	nasednout
do	do	k7c2	do
vozu	vůz	k1gInSc2	vůz
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
pomaloučku	pomaloučku	k6eAd1	pomaloučku
<g/>
,	,	kIx,	,
krok	krok	k1gInSc1	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
zámožnější	zámožný	k2eAgMnPc1d2	zámožnější
občané	občan	k1gMnPc1	občan
z	z	k7c2	z
Rouenu	Rouen	k1gInSc2	Rouen
<g/>
:	:	kIx,	:
pán	pán	k1gMnSc1	pán
a	a	k8xC	a
paní	paní	k1gFnSc4	paní
Loiseauovi	Loiseauův	k2eAgMnPc1d1	Loiseauův
<g/>
,	,	kIx,	,
majitelé	majitel	k1gMnPc1	majitel
velkoobchodu	velkoobchod	k1gInSc2	velkoobchod
s	s	k7c7	s
vínem	víno	k1gNnSc7	víno
<g/>
.	.	kIx.	.
</s>
<s>
Louiseau	Louiseau	k6eAd1	Louiseau
začínal	začínat	k5eAaImAgMnS	začínat
kdysi	kdysi	k6eAd1	kdysi
jako	jako	k8xS	jako
příručí	příručí	k1gMnPc1	příručí
u	u	k7c2	u
majitele	majitel	k1gMnSc2	majitel
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pak	pak	k6eAd1	pak
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
mizinu	mizina	k1gFnSc4	mizina
a	a	k8xC	a
Louiseau	Louiseaa	k1gFnSc4	Louiseaa
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
obchod	obchod	k1gInSc4	obchod
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
a	a	k8xC	a
vydělal	vydělat	k5eAaPmAgInS	vydělat
pěkné	pěkný	k2eAgNnSc4d1	pěkné
jmění	jmění	k1gNnSc4	jmění
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
lacino	lacino	k6eAd1	lacino
prodával	prodávat	k5eAaImAgMnS	prodávat
špatné	špatný	k2eAgNnSc4d1	špatné
víno	víno	k1gNnSc4	víno
a	a	k8xC	a
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgFnPc7	svůj
známými	známá	k1gFnPc7	známá
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
pověsti	pověst	k1gFnSc2	pověst
prohnaného	prohnaný	k2eAgMnSc2d1	prohnaný
filuty	filuta	k1gMnSc2	filuta
<g/>
,	,	kIx,	,
taškáře	taškář	k1gMnSc2	taškář
a	a	k8xC	a
především	především	k6eAd1	především
veselé	veselý	k2eAgFnPc4d1	veselá
kopy	kopa	k1gFnPc4	kopa
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
malé	malý	k2eAgFnPc4d1	malá
postavy	postava	k1gFnPc4	postava
s	s	k7c7	s
objemným	objemný	k2eAgInSc7d1	objemný
bříškem	bříšek	k1gInSc7	bříšek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
byla	být	k5eAaImAgFnS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
statná	statný	k2eAgFnSc1d1	statná
<g/>
,	,	kIx,	,
rázná	rázný	k2eAgFnSc1d1	rázná
a	a	k8xC	a
vnášela	vnášet	k5eAaImAgFnS	vnášet
řád	řád	k1gInSc4	řád
a	a	k8xC	a
ducha	duch	k1gMnSc4	duch
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
nich	on	k3xPp3gFnPc2	on
seděl	sedět	k5eAaImAgMnS	sedět
pan	pan	k1gMnSc1	pan
Carré-Lamadon	Carré-Lamadon	k1gMnSc1	Carré-Lamadon
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Carré-Lamadonovou	Carré-Lamadonová	k1gFnSc7	Carré-Lamadonová
<g/>
.	.	kIx.	.
</s>
<s>
Lamadon	Lamadon	k1gMnSc1	Lamadon
zaujímal	zaujímat	k5eAaImAgMnS	zaujímat
pevné	pevný	k2eAgNnSc4d1	pevné
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
bavlnářském	bavlnářský	k2eAgInSc6d1	bavlnářský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
důstojníkem	důstojník	k1gMnSc7	důstojník
Čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
krajské	krajský	k2eAgFnSc2d1	krajská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
–	–	k?	–
drobná	drobný	k2eAgFnSc1d1	drobná
a	a	k8xC	a
roztomiloučká	roztomiloučký	k2eAgFnSc1d1	roztomiloučká
–	–	k?	–
byla	být	k5eAaImAgFnS	být
mnohem	mnohem	k6eAd1	mnohem
mladší	mladý	k2eAgFnSc1d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Šestici	šestice	k1gFnSc4	šestice
dotvářeli	dotvářet	k5eAaImAgMnP	dotvářet
hrabě	hrabě	k1gMnSc1	hrabě
Hubert	Hubert	k1gMnSc1	Hubert
de	de	k?	de
Bréville	Bréville	k1gFnSc1	Bréville
s	s	k7c7	s
chotí	choť	k1gFnSc7	choť
<g/>
.	.	kIx.	.
</s>
<s>
Honosili	honosit	k5eAaImAgMnP	honosit
se	se	k3xPyFc4	se
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
a	a	k8xC	a
nejzvučnějších	zvučný	k2eAgNnPc2d3	nejzvučnější
šlechtických	šlechtický	k2eAgNnPc2d1	šlechtické
jmen	jméno	k1gNnPc2	jméno
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
voze	vůz	k1gInSc6	vůz
byly	být	k5eAaImAgFnP	být
také	také	k9	také
dvě	dva	k4xCgFnPc1	dva
jeptišky	jeptiška	k1gFnPc1	jeptiška
<g/>
,	,	kIx,	,
demokrat	demokrat	k1gMnSc1	demokrat
Cornudet	Cornudet	k1gMnSc1	Cornudet
–	–	k?	–
zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
již	již	k9	již
propil	propít	k5eAaPmAgMnS	propít
jmění	jmění	k1gNnSc2	jmění
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
cukráři	cukrář	k1gMnSc6	cukrář
–	–	k?	–
a	a	k8xC	a
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gInSc2	on
tělnatá	tělnatý	k2eAgFnSc1d1	tělnatá
žena	žena	k1gFnSc1	žena
lehčích	lehký	k2eAgInPc2d2	lehčí
mravů	mrav	k1gInPc2	mrav
Alžběta	Alžběta	k1gFnSc1	Alžběta
Roussetová	Roussetový	k2eAgFnSc1d1	Roussetový
<g/>
,	,	kIx,	,
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Kulička	kulička	k1gFnSc1	kulička
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
vskutku	vskutku	k9	vskutku
velmi	velmi	k6eAd1	velmi
vábná	vábný	k2eAgFnSc1d1	vábná
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
vyhledávaná	vyhledávaný	k2eAgFnSc1d1	vyhledávaná
<g/>
.	.	kIx.	.
</s>
<s>
Přítomné	přítomný	k2eAgFnPc1d1	přítomná
dámy	dáma	k1gFnPc1	dáma
si	se	k3xPyFc3	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
povídaly	povídat	k5eAaImAgFnP	povídat
jako	jako	k9	jako
o	o	k7c6	o
nevěstce	nevěstka	k1gFnSc6	nevěstka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
všem	všecek	k3xTgMnPc3	všecek
pro	pro	k7c4	pro
hanbu	hanba	k1gFnSc4	hanba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vůz	vůz	k1gInSc1	vůz
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
zapadl	zapadnout	k5eAaPmAgInS	zapadnout
<g/>
.	.	kIx.	.
</s>
<s>
Cestující	cestující	k1gMnPc1	cestující
dostali	dostat	k5eAaPmAgMnP	dostat
po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
hlad	hlad	k1gInSc4	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
Kulička	kulička	k1gFnSc1	kulička
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
s	s	k7c7	s
sebou	se	k3xPyFc7	se
měla	mít	k5eAaImAgFnS	mít
košík	košík	k1gInSc4	košík
plný	plný	k2eAgInSc4d1	plný
jídla	jídlo	k1gNnPc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
se	se	k3xPyFc4	se
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hodování	hodování	k1gNnSc1	hodování
přivedlo	přivést	k5eAaPmAgNnS	přivést
všechny	všechen	k3xTgInPc1	všechen
k	k	k7c3	k
řeči	řeč	k1gFnSc3	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
přijeli	přijet	k5eAaPmAgMnP	přijet
do	do	k7c2	do
Tôtes	Tôtesa	k1gFnPc2	Tôtesa
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
zkontrolovali	zkontrolovat	k5eAaPmAgMnP	zkontrolovat
jejich	jejich	k3xOp3gNnSc4	jejich
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
cestě	cesta	k1gFnSc3	cesta
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
ubytovat	ubytovat	k5eAaPmF	ubytovat
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německý	německý	k2eAgMnSc1d1	německý
důstojník	důstojník	k1gMnSc1	důstojník
drží	držet	k5eAaImIp3nS	držet
všechny	všechen	k3xTgFnPc4	všechen
v	v	k7c4	v
hospodě	hospodě	k?	hospodě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
Kuličku	kulička	k1gFnSc4	kulička
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ho	on	k3xPp3gMnSc4	on
vlastenecky	vlastenecky	k6eAd1	vlastenecky
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
všichni	všechen	k3xTgMnPc1	všechen
zajedno	zajedno	k6eAd1	zajedno
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jak	jak	k8xS	jak
dny	den	k1gInPc1	den
ubíhají	ubíhat	k5eAaImIp3nP	ubíhat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
soudržnost	soudržnost	k1gFnSc4	soudržnost
opadá	opadat	k5eAaBmIp3nS	opadat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Kulička	kulička	k1gFnSc1	kulička
jde	jít	k5eAaImIp3nS	jít
podívat	podívat	k5eAaImF	podívat
na	na	k7c4	na
místní	místní	k2eAgFnPc4d1	místní
křtiny	křtiny	k1gFnPc4	křtiny
(	(	kIx(	(
<g/>
sama	sám	k3xTgFnSc1	sám
má	mít	k5eAaImIp3nS	mít
dítě	dítě	k1gNnSc4	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
ostatní	ostatní	k2eAgMnPc1d1	ostatní
kout	kout	k5eAaImF	kout
plány	plán	k1gInPc4	plán
a	a	k8xC	a
pikle	pikel	k1gInPc4	pikel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
večeře	večeře	k1gFnSc2	večeře
<g/>
,	,	kIx,	,
oběda	oběd	k1gInSc2	oběd
i	i	k8xC	i
během	během	k7c2	během
dne	den	k1gInSc2	den
všichni	všechen	k3xTgMnPc1	všechen
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c6	o
ženách	žena	k1gFnPc6	žena
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
obětovaly	obětovat	k5eAaBmAgFnP	obětovat
za	za	k7c4	za
vlast	vlast	k1gFnSc4	vlast
<g/>
,	,	kIx,	,
víru	vír	k1gInSc2	vír
apod.	apod.	kA	apod.
Hostinský	hostinský	k1gMnSc1	hostinský
Follenvie	Follenvie	k1gFnSc2	Follenvie
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
večer	večer	k6eAd1	večer
objevil	objevit	k5eAaPmAgMnS	objevit
s	s	k7c7	s
dotazem	dotaz	k1gInSc7	dotaz
od	od	k7c2	od
Němce	Němec	k1gMnSc2	Němec
na	na	k7c4	na
slečnu	slečna	k1gFnSc4	slečna
Alžbětu	Alžběta	k1gFnSc4	Alžběta
Rousettovou	Rousettová	k1gFnSc4	Rousettová
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
večera	večer	k1gInSc2	večer
se	se	k3xPyFc4	se
Kulička	kulička	k1gFnSc1	kulička
omluvila	omluvit	k5eAaPmAgFnS	omluvit
z	z	k7c2	z
večeře	večeře	k1gFnSc2	večeře
–	–	k?	–
všem	všecek	k3xTgNnPc3	všecek
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
polevila	polevit	k5eAaPmAgFnS	polevit
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
oslavovat	oslavovat	k5eAaImF	oslavovat
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
až	až	k6eAd1	až
na	na	k7c4	na
Cornudeta	Cornudet	k1gMnSc4	Cornudet
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
ji	on	k3xPp3gFnSc4	on
chtěl	chtít	k5eAaImAgMnS	chtít
mít	mít	k5eAaImF	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
ho	on	k3xPp3gNnSc4	on
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ráno	ráno	k6eAd1	ráno
byl	být	k5eAaImAgInS	být
připravený	připravený	k2eAgInSc1d1	připravený
vůz	vůz	k1gInSc1	vůz
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
si	se	k3xPyFc3	se
nabalili	nabalit	k5eAaPmAgMnP	nabalit
jídlo	jídlo	k1gNnSc4	jídlo
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Kulička	kulička	k1gFnSc1	kulička
se	se	k3xPyFc4	se
cítila	cítit	k5eAaImAgFnS	cítit
zostuzená	zostuzený	k2eAgFnSc1d1	zostuzená
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
nemluvil	mluvit	k5eNaImAgMnS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
dostali	dostat	k5eAaPmAgMnP	dostat
cestující	cestující	k1gMnPc1	cestující
hlad	hlad	k1gMnSc1	hlad
–	–	k?	–
jídlo	jídlo	k1gNnSc4	jídlo
nabalené	nabalený	k2eAgMnPc4d1	nabalený
měli	mít	k5eAaImAgMnP	mít
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
počali	počnout	k5eAaPmAgMnP	počnout
hodovat	hodovat	k5eAaImF	hodovat
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
Kulička	kulička	k1gFnSc1	kulička
si	se	k3xPyFc3	se
ve	v	k7c6	v
spěchu	spěch	k1gInSc6	spěch
nestihla	stihnout	k5eNaPmAgFnS	stihnout
nic	nic	k3yNnSc4	nic
zabalit	zabalit	k5eAaPmF	zabalit
s	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
nepodělil	podělit	k5eNaPmAgMnS	podělit
<g/>
.	.	kIx.	.
</s>
<s>
Seděla	sedět	k5eAaImAgFnS	sedět
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
plná	plný	k2eAgFnSc1d1	plná
zlosti	zlost	k1gFnSc3	zlost
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	s	k7c7	s
slzami	slza	k1gFnPc7	slza
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Online	Onlin	k1gInSc5	Onlin
dostupné	dostupný	k2eAgNnSc4d1	dostupné
dílo	dílo	k1gNnSc4	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
MAUPASSANT	MAUPASSANT	kA	MAUPASSANT
<g/>
,	,	kIx,	,
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
<g/>
.	.	kIx.	.
</s>
<s>
Kulička	kulička	k1gFnSc1	kulička
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jiří	Jiří	k1gMnSc1	Jiří
Stanislav	Stanislav	k1gMnSc1	Stanislav
Guth-Jarkovský	Guth-Jarkovský	k2eAgMnSc1d1	Guth-Jarkovský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jos	Jos	k1gFnSc1	Jos
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
220	[number]	k4	220
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
