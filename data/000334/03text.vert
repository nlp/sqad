<s>
Něva	Něva	k1gFnSc1	Něva
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Н	Н	k?	Н
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Leningradské	leningradský	k2eAgFnSc6d1	Leningradská
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
74	[number]	k4	74
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
povodí	povodí	k1gNnSc2	povodí
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
povodí	povodí	k1gNnSc2	povodí
Ladožského	ladožský	k2eAgNnSc2d1	Ladožské
a	a	k8xC	a
Oněžského	oněžský	k2eAgNnSc2d1	Oněžské
jezera	jezero	k1gNnSc2	jezero
<g/>
)	)	kIx)	)
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
281	[number]	k4	281
000	[number]	k4	000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
povodí	povodí	k1gNnSc1	povodí
Něvy	Něva	k1gFnSc2	Něva
činí	činit	k5eAaImIp3nS	činit
5	[number]	k4	5
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Odtéká	odtékat	k5eAaImIp3nS	odtékat
ze	z	k7c2	z
zátoky	zátoka	k1gFnSc2	zátoka
Petrokreposť	Petrokreposť	k1gFnSc2	Petrokreposť
(	(	kIx(	(
<g/>
Tliselburské	Tliselburský	k2eAgFnSc2d1	Tliselburský
<g/>
)	)	kIx)	)
Ladožského	ladožský	k2eAgNnSc2d1	Ladožské
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
Něvské	něvský	k2eAgFnSc2d1	Něvská
zátoky	zátoka	k1gFnSc2	zátoka
Finského	finský	k2eAgInSc2d1	finský
zálivu	záliv	k1gInSc2	záliv
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
se	se	k3xPyFc4	se
větví	větvit	k5eAaImIp3nS	větvit
na	na	k7c4	na
ramena	rameno	k1gNnPc4	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
jsou	být	k5eAaImIp3nP	být
Velká	velká	k1gFnSc1	velká
a	a	k8xC	a
Malá	malý	k2eAgFnSc1d1	malá
Něva	Něva	k1gFnSc1	Něva
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
Malá	malý	k2eAgFnSc1d1	malá
Něvka	Něvka	k1gFnSc1	Něvka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k6eAd1	tak
velkou	velký	k2eAgFnSc4d1	velká
deltu	delta	k1gFnSc4	delta
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
ostrovech	ostrov	k1gInPc6	ostrov
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
odtoku	odtok	k1gInSc6	odtok
(	(	kIx(	(
<g/>
u	u	k7c2	u
města	město	k1gNnSc2	město
Šlisselburg	Šlisselburg	k1gInSc1	Šlisselburg
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
480	[number]	k4	480
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
maximální	maximální	k2eAgFnSc4d1	maximální
4	[number]	k4	4
590	[number]	k4	590
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
minimální	minimální	k2eAgInSc4d1	minimální
2	[number]	k4	2
050	[number]	k4	050
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
2	[number]	k4	2
530	[number]	k4	530
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Jihozápadní	jihozápadní	k2eAgInPc1d1	jihozápadní
a	a	k8xC	a
západní	západní	k2eAgInPc1d1	západní
větry	vítr	k1gInPc1	vítr
ženou	hnát	k5eAaImIp3nP	hnát
vodu	voda	k1gFnSc4	voda
Finským	finský	k2eAgInSc7d1	finský
zálivem	záliv	k1gInSc7	záliv
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
Něvy	Něva	k1gFnSc2	Něva
<g/>
,	,	kIx,	,
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
tak	tak	k9	tak
úroveň	úroveň	k1gFnSc4	úroveň
hladiny	hladina	k1gFnSc2	hladina
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
záplavy	záplava	k1gFnPc1	záplava
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Katastrofické	katastrofický	k2eAgFnPc1d1	katastrofická
záplavy	záplava	k1gFnPc1	záplava
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
m	m	kA	m
nad	nad	k7c7	nad
běžnou	běžný	k2eAgFnSc7d1	běžná
hladinou	hladina	k1gFnSc7	hladina
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
r	r	kA	r
1824	[number]	k4	1824
(	(	kIx(	(
<g/>
popsáno	popsat	k5eAaPmNgNnS	popsat
Puškinem	Puškin	k1gInSc7	Puškin
v	v	k7c6	v
Měděném	měděný	k2eAgInSc6d1	měděný
jezdci	jezdec	k1gInSc6	jezdec
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
r	r	kA	r
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
ledové	ledový	k2eAgFnPc4d1	ledová
zácpy	zácpa	k1gFnPc4	zácpa
<g/>
.	.	kIx.	.
</s>
<s>
Zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
a	a	k8xC	a
led	led	k1gInSc1	led
mizí	mizet	k5eAaImIp3nS	mizet
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
nebo	nebo	k8xC	nebo
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
10	[number]	k4	10
-	-	kIx~	-
15	[number]	k4	15
dnech	den	k1gInPc6	den
po	po	k7c6	po
krách	kra	k1gFnPc6	kra
uvolněných	uvolněný	k2eAgMnPc2d1	uvolněný
při	při	k7c6	při
tání	tání	k1gNnSc6	tání
Něvy	Něva	k1gFnSc2	Něva
následují	následovat	k5eAaImIp3nP	následovat
kry	kra	k1gFnPc1	kra
z	z	k7c2	z
Ladožského	ladožský	k2eAgNnSc2d1	Ladožské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
přítoky	přítok	k1gInPc1	přítok
jsou	být	k5eAaImIp3nP	být
zprava	zprava	k6eAd1	zprava
Ochta	Ocht	k1gInSc2	Ocht
a	a	k8xC	a
zleva	zleva	k6eAd1	zleva
Ižora	Ižora	k1gFnSc1	Ižora
<g/>
,	,	kIx,	,
Tosna	Tosna	k1gFnSc1	Tosna
a	a	k8xC	a
Mga.	Mga.	k1gFnSc1	Mga.
Něva	Něva	k1gFnSc1	Něva
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
bělomořsko-baltské	bělomořskoaltský	k2eAgFnSc2d1	bělomořsko-baltský
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
volžsko-baltské	volžskoaltský	k2eAgFnSc2d1	volžsko-baltský
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1242	[number]	k4	1242
odrazil	odrazit	k5eAaPmAgInS	odrazit
útok	útok	k1gInSc1	útok
Švédů	Švéd	k1gMnPc2	Švéd
během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c6	na
Něvě	Něva	k1gFnSc6	Něva
Alexandr	Alexandr	k1gMnSc1	Alexandr
Něvský	něvský	k2eAgInSc1d1	něvský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Něvě	Něva	k1gFnSc6	Něva
utopen	utopen	k2eAgMnSc1d1	utopen
Grigorij	Grigorij	k1gMnSc1	Grigorij
Jefimovič	Jefimovič	k1gMnSc1	Jefimovič
Rasputin	Rasputin	k1gMnSc1	Rasputin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941	[number]	k4	1941
až	až	k8xS	až
1943	[number]	k4	1943
se	se	k3xPyFc4	se
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
povodí	povodí	k1gNnSc6	povodí
odehrála	odehrát	k5eAaPmAgFnS	odehrát
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Leningrad	Leningrad	k1gInSc4	Leningrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Н	Н	k?	Н
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Něva	Něva	k1gFnSc1	Něva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
