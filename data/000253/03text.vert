<s>
Hry	hra	k1gFnPc1	hra
XXIX	XXIX	kA	XXIX
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnPc1	olympiáda
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
Pekingu	Peking	k1gInSc6	Peking
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
zemí	zem	k1gFnPc2	zem
bojovali	bojovat	k5eAaImAgMnP	bojovat
celkem	celkem	k6eAd1	celkem
o	o	k7c4	o
302	[number]	k4	302
sady	sada	k1gFnSc2	sada
medailí	medaile	k1gFnPc2	medaile
ve	v	k7c6	v
28	[number]	k4	28
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nově	nově	k6eAd1	nově
zařazené	zařazený	k2eAgFnPc4d1	zařazená
disciplíny	disciplína	k1gFnPc4	disciplína
patřila	patřit	k5eAaImAgFnS	patřit
například	například	k6eAd1	například
cyklistická	cyklistický	k2eAgFnSc1d1	cyklistická
disciplína	disciplína	k1gFnSc1	disciplína
BMX	BMX	kA	BMX
nebo	nebo	k8xC	nebo
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
plavecký	plavecký	k2eAgInSc4d1	plavecký
maratón	maratón	k1gInSc4	maratón
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
nevládní	vládní	k2eNgFnPc1d1	nevládní
organizace	organizace	k1gFnPc1	organizace
protestovaly	protestovat	k5eAaBmAgFnP	protestovat
proti	proti	k7c3	proti
olympijským	olympijský	k2eAgFnPc3d1	olympijská
hrám	hra	k1gFnPc3	hra
kvůli	kvůli	k7c3	kvůli
porušování	porušování	k1gNnSc3	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
stadiónem	stadión	k1gInSc7	stadión
her	hra	k1gFnPc2	hra
je	být	k5eAaImIp3nS	být
Pekingský	pekingský	k2eAgInSc1d1	pekingský
národní	národní	k2eAgInSc1d1	národní
stadion	stadion	k1gInSc1	stadion
(	(	kIx(	(
<g/>
北	北	k?	北
<g/>
;	;	kIx,	;
Běijī	Běijī	k1gMnSc1	Běijī
guójiā	guójiā	k?	guójiā
tǐ	tǐ	k?	tǐ
<g/>
;	;	kIx,	;
Pej-ťing	Pej-ťing	k1gInSc1	Pej-ťing
kuo-ťia	kuo-ťium	k1gNnSc2	kuo-ťium
tchi-jü-čchang	tchiü-čchanga	k1gFnPc2	tchi-jü-čchanga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
označovaný	označovaný	k2eAgInSc1d1	označovaný
dle	dle	k7c2	dle
své	svůj	k3xOyFgFnSc2	svůj
podoby	podoba	k1gFnSc2	podoba
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Ptačí	ptačí	k2eAgNnSc1d1	ptačí
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
鸟	鸟	k?	鸟
<g/>
;	;	kIx,	;
Niǎ	Niǎ	k1gMnSc1	Niǎ
<g/>
;	;	kIx,	;
Niao-čchao	Niao-čchao	k1gMnSc1	Niao-čchao
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mottem	motto	k1gNnSc7	motto
pekingské	pekingský	k2eAgFnSc2d1	Pekingská
olympiády	olympiáda	k1gFnSc2	olympiáda
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
sen	sen	k1gInSc1	sen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
同	同	k?	同
<g/>
,	,	kIx,	,
<g/>
同	同	k?	同
<g/>
;	;	kIx,	;
Tóng	Tóng	k1gMnSc1	Tóng
yī	yī	k?	yī
shì	shì	k?	shì
<g/>
,	,	kIx,	,
tóng	tóng	k1gInSc1	tóng
yī	yī	k?	yī
mè	mè	k?	mè
<g/>
;	;	kIx,	;
Tchung	Tchung	k1gMnSc1	Tchung
i-ke	i	k1gFnSc2	i-k
š	š	k?	š
<g/>
'	'	kIx"	'
<g/>
-ťie	-ťie	k1gInSc1	-ťie
<g/>
,	,	kIx,	,
tchung	tchung	k1gMnSc1	tchung
i-ke	i	k1gFnSc2	i-k
meng-siang	mengiang	k1gMnSc1	meng-siang
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pekingské	pekingský	k2eAgFnPc1d1	Pekingská
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
mají	mít	k5eAaImIp3nP	mít
rekordní	rekordní	k2eAgInSc4d1	rekordní
počet	počet	k1gInSc4	počet
maskotů	maskot	k1gInPc2	maskot
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
postav	postava	k1gFnPc2	postava
nesoucích	nesoucí	k2eAgFnPc2d1	nesoucí
bohatou	bohatý	k2eAgFnSc4d1	bohatá
symboliku	symbolika	k1gFnSc4	symbolika
<g/>
,	,	kIx,	,
jmenují	jmenovat	k5eAaBmIp3nP	jmenovat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
Šťastné	Šťastné	k2eAgFnSc1d1	Šťastné
děti	dítě	k1gFnPc1	dítě
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
福	福	k?	福
<g/>
;	;	kIx,	;
Fúwá	Fúwá	k1gFnSc1	Fúwá
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
kapr	kapr	k1gMnSc1	kapr
Pej-pej	Pejej	k1gMnSc1	Pej-pej
(	(	kIx(	(
<g/>
贝	贝	k?	贝
Bè	Bè	k1gFnSc1	Bè
<g/>
)	)	kIx)	)
-	-	kIx~	-
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
prosperita	prosperita	k1gFnSc1	prosperita
<g/>
,	,	kIx,	,
vznešenost	vznešenost	k1gFnSc1	vznešenost
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
čistota	čistota	k1gFnSc1	čistota
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInPc4d1	vodní
sporty	sport	k1gInPc4	sport
panda	panda	k1gFnSc1	panda
velká	velká	k1gFnSc1	velká
Ťing-Ťing	Ťing-Ťing	k1gInSc1	Ťing-Ťing
(	(	kIx(	(
<g/>
晶	晶	k?	晶
Jī	Jī	k1gFnSc1	Jī
<g/>
)	)	kIx)	)
-	-	kIx~	-
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
radost	radost	k1gFnSc1	radost
<g/>
,	,	kIx,	,
čest	čest	k1gFnSc1	čest
a	a	k8xC	a
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
silové	silový	k2eAgInPc4d1	silový
a	a	k8xC	a
úpolové	úpolový	k2eAgInPc4d1	úpolový
sporty	sport	k1gInPc4	sport
olympijský	olympijský	k2eAgInSc4d1	olympijský
oheň	oheň	k1gInSc4	oheň
Chuan-chuan	Chuanhuany	k1gInPc2	Chuan-chuany
(	(	kIx(	(
<g/>
欢	欢	k?	欢
Huā	Huā	k1gFnSc1	Huā
<g/>
)	)	kIx)	)
-	-	kIx~	-
červená	červená	k1gFnSc1	červená
<g/>
,	,	kIx,	,
vášeň	vášeň	k1gFnSc1	vášeň
<g/>
,	,	kIx,	,
nespoutanost	nespoutanost	k1gFnSc1	nespoutanost
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
míčové	míčový	k2eAgInPc4d1	míčový
sporty	sport	k1gInPc4	sport
tibetská	tibetský	k2eAgFnSc1d1	tibetská
antilopa	antilopa	k1gFnSc1	antilopa
Jing-jing	Jinging	k1gInSc1	Jing-jing
(	(	kIx(	(
<g/>
迎	迎	k?	迎
Yíngyíng	Yíngyíng	k1gInSc1	Yíngyíng
<g/>
)	)	kIx)	)
-	-	kIx~	-
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
,	,	kIx,	,
pohotovost	pohotovost	k1gFnSc1	pohotovost
a	a	k8xC	a
ohebnost	ohebnost	k1gFnSc1	ohebnost
<g/>
,	,	kIx,	,
atletika	atletika	k1gFnSc1	atletika
vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
Ni-ni	Niň	k1gFnSc6	Ni-eň
(	(	kIx(	(
<g/>
妮	妮	k?	妮
Nī	Nī	k1gFnSc1	Nī
<g/>
)	)	kIx)	)
-	-	kIx~	-
zelená	zelená	k1gFnSc1	zelená
<g/>
,	,	kIx,	,
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
nevinnost	nevinnost	k1gFnSc1	nevinnost
a	a	k8xC	a
živost	živost	k1gFnSc1	živost
<g/>
,	,	kIx,	,
gymnastika	gymnastika	k1gFnSc1	gymnastika
Tyto	tento	k3xDgFnPc4	tento
figurky	figurka	k1gFnPc4	figurka
jsou	být	k5eAaImIp3nP	být
implikovány	implikovat	k5eAaImNgFnP	implikovat
stykem	styk	k1gInSc7	styk
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
lesem	les	k1gInSc7	les
<g/>
,	,	kIx,	,
ohněm	oheň	k1gInSc7	oheň
<g/>
,	,	kIx,	,
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
oblohou	obloha	k1gFnSc7	obloha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naráží	narážet	k5eAaPmIp3nS	narážet
na	na	k7c4	na
harmonii	harmonie	k1gFnSc4	harmonie
člověka	člověk	k1gMnSc2	člověk
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
myšlence	myšlenka	k1gFnSc3	myšlenka
"	"	kIx"	"
<g/>
Zelené	Zelená	k1gFnSc2	Zelená
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc2	ten
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
figurek	figurka	k1gFnPc2	figurka
Fuwa	Fuw	k1gInSc2	Fuw
integrovány	integrován	k2eAgInPc1d1	integrován
mnohé	mnohý	k2eAgInPc1d1	mnohý
prvky	prvek	k1gInPc1	prvek
čínské	čínský	k2eAgFnSc2d1	čínská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jména	jméno	k1gNnSc2	jméno
těchto	tento	k3xDgInPc2	tento
pěti	pět	k4xCc2	pět
postaviček	postavička	k1gFnPc2	postavička
sestavená	sestavený	k2eAgFnSc1d1	sestavená
za	za	k7c7	za
sebou	se	k3xPyFc7	se
připomínají	připomínat	k5eAaImIp3nP	připomínat
čínskou	čínský	k2eAgFnSc4d1	čínská
větu	věta	k1gFnSc4	věta
"	"	kIx"	"
<g/>
Peking	Peking	k1gInSc1	Peking
tě	ty	k3xPp2nSc4	ty
vítá	vítat	k5eAaImIp3nS	vítat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
北	北	k?	北
<g/>
;	;	kIx,	;
Běijī	Běijī	k1gMnSc1	Běijī
huā	huā	k?	huā
nǐ	nǐ	k?	nǐ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
olympijských	olympijský	k2eAgFnPc2d1	olympijská
postaviček	postavička	k1gFnPc2	postavička
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
barevně	barevně	k6eAd1	barevně
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
pět	pět	k4xCc4	pět
olympijských	olympijský	k2eAgInPc2d1	olympijský
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
olympiády	olympiáda	k1gFnSc2	olympiáda
byl	být	k5eAaImAgInS	být
42	[number]	k4	42
miliard	miliarda	k4xCgFnPc2	miliarda
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
reklamu	reklama	k1gFnSc4	reklama
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
díky	díky	k7c3	díky
olympiádě	olympiáda	k1gFnSc6	olympiáda
stouply	stoupnout	k5eAaPmAgFnP	stoupnout
o	o	k7c4	o
19	[number]	k4	19
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
značky	značka	k1gFnPc1	značka
si	se	k3xPyFc3	se
od	od	k7c2	od
olympiády	olympiáda	k1gFnSc2	olympiáda
slibovaly	slibovat	k5eAaImAgInP	slibovat
velké	velký	k2eAgInPc1d1	velký
zisky	zisk	k1gInPc1	zisk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
město	město	k1gNnSc1	město
uspořádá	uspořádat	k5eAaPmIp3nS	uspořádat
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2001	[number]	k4	2001
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
MOV	MOV	kA	MOV
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Pekingem	Peking	k1gInSc7	Peking
usilovaly	usilovat	k5eAaImAgFnP	usilovat
o	o	k7c6	o
pořádání	pořádání	k1gNnSc6	pořádání
také	také	k9	také
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Istanbul	Istanbul	k1gInSc1	Istanbul
a	a	k8xC	a
Ósaka	Ósaka	k1gFnSc1	Ósaka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
hlasování	hlasování	k1gNnSc2	hlasování
dostal	dostat	k5eAaPmAgMnS	dostat
Peking	Peking	k1gInSc4	Peking
nejvíce	hodně	k6eAd3	hodně
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
absolutní	absolutní	k2eAgFnSc4d1	absolutní
většiny	většina	k1gFnPc4	většina
<g/>
,	,	kIx,	,
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
bez	bez	k7c2	bez
vyřazené	vyřazený	k2eAgFnSc2d1	vyřazená
Ósaky	Ósaka	k1gFnSc2	Ósaka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
již	již	k6eAd1	již
56	[number]	k4	56
ze	z	k7c2	z
105	[number]	k4	105
pověřených	pověřený	k2eAgInPc2d1	pověřený
členů	člen	k1gInPc2	člen
MOV	MOV	kA	MOV
dalo	dát	k5eAaPmAgNnS	dát
svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
čínské	čínský	k2eAgFnSc6d1	čínská
metropoli	metropol	k1gFnSc6	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Mohly	moct	k5eAaImAgFnP	moct
tak	tak	k6eAd1	tak
vypuknout	vypuknout	k5eAaPmF	vypuknout
mohutné	mohutný	k2eAgFnPc4d1	mohutná
oslavy	oslava	k1gFnPc4	oslava
jak	jak	k8xC	jak
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Peking	Peking	k1gInSc1	Peking
tak	tak	k9	tak
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
pokus	pokus	k1gInSc4	pokus
uspěl	uspět	k5eAaPmAgMnS	uspět
-	-	kIx~	-
při	při	k7c6	při
první	první	k4xOgFnSc6	první
kandidatuře	kandidatura	k1gFnSc6	kandidatura
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
dali	dát	k5eAaPmAgMnP	dát
delegáti	delegát	k1gMnPc1	delegát
přednost	přednost	k1gFnSc1	přednost
australskému	australský	k2eAgNnSc3d1	Australské
Sydney	Sydney	k1gNnSc3	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholní	vrcholný	k2eAgMnPc1d1	vrcholný
představitelé	představitel	k1gMnPc1	představitel
čínského	čínský	k2eAgInSc2d1	čínský
státu	stát	k1gInSc2	stát
rovněž	rovněž	k9	rovněž
dali	dát	k5eAaPmAgMnP	dát
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nebudou	být	k5eNaImBp3nP	být
opakovat	opakovat	k5eAaImF	opakovat
potíže	potíž	k1gFnPc4	potíž
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
olympijských	olympijský	k2eAgNnPc2d1	Olympijské
sportovišť	sportoviště	k1gNnPc2	sportoviště
jako	jako	k8xS	jako
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
dodrželi	dodržet	k5eAaPmAgMnP	dodržet
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
sportoviště	sportoviště	k1gNnPc1	sportoviště
byla	být	k5eAaImAgNnP	být
dokončena	dokončit	k5eAaPmNgNnP	dokončit
rok	rok	k1gInSc4	rok
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
soutěží	soutěž	k1gFnPc2	soutěž
a	a	k8xC	a
předána	předat	k5eAaPmNgFnS	předat
do	do	k7c2	do
užívání	užívání	k1gNnPc2	užívání
sportovcům	sportovec	k1gMnPc3	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
také	také	k9	také
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
testovací	testovací	k2eAgInPc1d1	testovací
předolympijské	předolympijský	k2eAgInPc1d1	předolympijský
závody	závod	k1gInPc1	závod
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
organizace	organizace	k1gFnPc1	organizace
vyzývají	vyzývat	k5eAaImIp3nP	vyzývat
k	k	k7c3	k
bojkotu	bojkot	k1gInSc3	bojkot
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
jako	jako	k8xS	jako
Studenti	student	k1gMnPc1	student
pro	pro	k7c4	pro
svobodný	svobodný	k2eAgInSc4d1	svobodný
Tibet	Tibet	k1gInSc4	Tibet
požadují	požadovat	k5eAaImIp3nP	požadovat
bojkotovat	bojkotovat	k5eAaImF	bojkotovat
hry	hra	k1gFnPc4	hra
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
čínské	čínský	k2eAgFnSc2d1	čínská
okupace	okupace	k1gFnSc2	okupace
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gFnSc4	International
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
čínskou	čínský	k2eAgFnSc4d1	čínská
angažovanost	angažovanost	k1gFnSc4	angažovanost
v	v	k7c6	v
Dárfúrské	Dárfúrský	k2eAgFnSc6d1	Dárfúrský
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
také	také	k9	také
bojuje	bojovat	k5eAaImIp3nS	bojovat
se	s	k7c7	s
silným	silný	k2eAgNnSc7d1	silné
znečištěním	znečištění	k1gNnSc7	znečištění
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
doufá	doufat	k5eAaImIp3nS	doufat
organizační	organizační	k2eAgInSc4d1	organizační
výbor	výbor	k1gInSc4	výbor
odstranit	odstranit	k5eAaPmF	odstranit
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
zaujímal	zaujímat	k5eAaImAgInS	zaujímat
zdrženlivý	zdrženlivý	k2eAgInSc1d1	zdrženlivý
postoj	postoj	k1gInSc1	postoj
v	v	k7c6	v
diskusích	diskuse	k1gFnPc6	diskuse
o	o	k7c6	o
lidských	lidský	k2eAgNnPc6d1	lidské
právech	právo	k1gNnPc6	právo
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
zatčením	zatčení	k1gNnPc3	zatčení
po	po	k7c6	po
pokusech	pokus	k1gInPc6	pokus
protestovat	protestovat	k5eAaBmF	protestovat
proti	proti	k7c3	proti
čínské	čínský	k2eAgFnSc3d1	čínská
politice	politika	k1gFnSc3	politika
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Štafeta	štafeta	k1gFnSc1	štafeta
olympijské	olympijský	k2eAgFnSc2d1	olympijská
pochodně	pochodeň	k1gFnSc2	pochodeň
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzplane	vzplanout	k5eAaPmIp3nS	vzplanout
na	na	k7c6	na
olympijském	olympijský	k2eAgInSc6d1	olympijský
stadiónu	stadión	k1gInSc6	stadión
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zahájení	zahájení	k1gNnSc6	zahájení
pekingských	pekingský	k2eAgFnPc2d1	Pekingská
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zapálen	zapálit	k5eAaPmNgInS	zapálit
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
11	[number]	k4	11
hodině	hodina	k1gFnSc6	hodina
SEČ	seč	k1gFnSc4	seč
na	na	k7c6	na
tradičním	tradiční	k2eAgNnSc6d1	tradiční
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starořecké	starořecký	k2eAgFnSc6d1	starořecká
Olympii	Olympia	k1gFnSc6	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
Čest	čest	k1gFnSc1	čest
zapálit	zapálit	k5eAaPmF	zapálit
oheň	oheň	k1gInSc4	oheň
dostala	dostat	k5eAaPmAgFnS	dostat
řecká	řecký	k2eAgFnSc1d1	řecká
herečka	herečka	k1gFnSc1	herečka
Marie	Maria	k1gFnSc2	Maria
Nafpliotu	Nafpliot	k1gInSc2	Nafpliot
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
dobrému	dobrý	k2eAgNnSc3d1	dobré
počasí	počasí	k1gNnSc3	počasí
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
olympijský	olympijský	k2eAgInSc1d1	olympijský
oheň	oheň	k1gInSc1	oheň
zapálen	zapálen	k2eAgInSc1d1	zapálen
v	v	k7c6	v
rozvalinách	rozvalina	k1gFnPc6	rozvalina
chrámu	chrám	k1gInSc2	chrám
bohyně	bohyně	k1gFnSc2	bohyně
Héry	Héra	k1gFnSc2	Héra
tradičním	tradiční	k2eAgInSc7d1	tradiční
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pomocí	pomocí	k7c2	pomocí
parabolických	parabolický	k2eAgNnPc2d1	parabolické
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
oheň	oheň	k1gInSc1	oheň
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
pouť	pouť	k1gFnSc4	pouť
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
137	[number]	k4	137
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
putoval	putovat	k5eAaImAgInS	putovat
týden	týden	k1gInSc4	týden
po	po	k7c6	po
území	území	k1gNnSc6	území
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
prvním	první	k4xOgMnSc7	první
členem	člen	k1gMnSc7	člen
štafety	štafeta	k1gFnSc2	štafeta
byl	být	k5eAaImAgMnS	být
řecký	řecký	k2eAgMnSc1d1	řecký
taekwondista	taekwondista	k1gMnSc1	taekwondista
Alexandros	Alexandrosa	k1gFnPc2	Alexandrosa
Nikolaidis	Nikolaidis	k1gFnPc2	Nikolaidis
<g/>
.	.	kIx.	.
</s>
<s>
Akt	akt	k1gInSc1	akt
zapálení	zapálení	k1gNnSc2	zapálení
ohně	oheň	k1gInSc2	oheň
byl	být	k5eAaImAgInS	být
narušen	narušit	k5eAaPmNgInS	narušit
několika	několik	k4yIc7	několik
demonstranty	demonstrant	k1gMnPc7	demonstrant
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
protestovali	protestovat	k5eAaBmAgMnP	protestovat
proti	proti	k7c3	proti
potlačování	potlačování	k1gNnSc3	potlačování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
demonstrantů	demonstrant	k1gMnPc2	demonstrant
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
až	až	k9	až
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
vedoucímu	vedoucí	k1gMnSc3	vedoucí
organizačního	organizační	k2eAgInSc2d1	organizační
výboru	výbor	k1gInSc2	výbor
Liou	Lio	k1gMnSc3	Lio
Čchi	Čch	k1gMnSc3	Čch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
právě	právě	k6eAd1	právě
vedl	vést	k5eAaImAgMnS	vést
u	u	k7c2	u
řečnického	řečnický	k2eAgInSc2d1	řečnický
pultu	pult	k1gInSc2	pult
svůj	svůj	k3xOyFgInSc4	svůj
proslov	proslov	k1gInSc4	proslov
<g/>
.	.	kIx.	.
</s>
<s>
Demonstranta	demonstrant	k1gMnSc4	demonstrant
však	však	k9	však
dokázala	dokázat	k5eAaPmAgFnS	dokázat
chytit	chytit	k5eAaPmF	chytit
ochranka	ochranka	k1gFnSc1	ochranka
<g/>
.	.	kIx.	.
</s>
<s>
Liou	Lio	k2eAgFnSc4d1	Lio
Čchi	Čche	k1gFnSc4	Čche
ale	ale	k8xC	ale
svou	svůj	k3xOyFgFnSc4	svůj
řeč	řeč	k1gFnSc4	řeč
nepřerušil	přerušit	k5eNaPmAgMnS	přerušit
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
klidný	klidný	k2eAgMnSc1d1	klidný
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
hlásal	hlásat	k5eAaImAgInS	hlásat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
oheň	oheň	k1gInSc1	oheň
bude	být	k5eAaImBp3nS	být
šířit	šířit	k5eAaImF	šířit
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
mír	mír	k1gInSc4	mír
a	a	k8xC	a
přátelství	přátelství	k1gNnSc4	přátelství
<g/>
,	,	kIx,	,
naději	naděje	k1gFnSc4	naděje
a	a	k8xC	a
sny	sen	k1gInPc1	sen
všech	všecek	k3xTgMnPc2	všecek
Číňanů	Číňan	k1gMnPc2	Číňan
i	i	k8xC	i
lidí	člověk	k1gMnPc2	člověk
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
K	k	k7c3	k
incidentu	incident	k1gInSc3	incident
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
organizace	organizace	k1gFnSc1	organizace
Reportéři	reportér	k1gMnPc1	reportér
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
agentur	agentura	k1gFnPc2	agentura
byl	být	k5eAaImAgInS	být
přímý	přímý	k2eAgInSc1d1	přímý
přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
Olympie	Olympia	k1gFnSc2	Olympia
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
televizi	televize	k1gFnSc6	televize
v	v	k7c6	v
době	doba	k1gFnSc6	doba
incidentu	incident	k1gInSc2	incident
přerušen	přerušit	k5eAaPmNgInS	přerušit
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
předtočenými	předtočený	k2eAgInPc7d1	předtočený
záběry	záběr	k1gInPc7	záběr
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
komentátorů	komentátor	k1gMnPc2	komentátor
neinformoval	informovat	k5eNaBmAgInS	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
událo	udát	k5eAaPmAgNnS	udát
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
minut	minuta	k1gFnPc2	minuta
později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
protestnímu	protestní	k2eAgInSc3d1	protestní
aktu	akt	k1gInSc3	akt
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
tibetská	tibetský	k2eAgFnSc1d1	tibetská
žena	žena	k1gFnSc1	žena
zahalená	zahalený	k2eAgFnSc1d1	zahalená
do	do	k7c2	do
červené	červený	k2eAgFnSc2d1	červená
látky	látka	k1gFnSc2	látka
lehla	lehnout	k5eAaPmAgFnS	lehnout
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
před	před	k7c4	před
běžce	běžec	k1gMnPc4	běžec
s	s	k7c7	s
pochodní	pochodeň	k1gFnSc7	pochodeň
poblíž	poblíž	k7c2	poblíž
Olympie	Olympia	k1gFnSc2	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
Jacques	Jacques	k1gMnSc1	Jacques
Rogge	Rogg	k1gMnSc2	Rogg
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
obřadu	obřad	k1gInSc6	obřad
též	též	k9	též
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
držet	držet	k5eAaImF	držet
napjatou	napjatý	k2eAgFnSc4d1	napjatá
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
mantinelech	mantinel	k1gInPc6	mantinel
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
odehrát	odehrát	k5eAaPmF	odehrát
v	v	k7c6	v
mírovém	mírový	k2eAgNnSc6d1	Mírové
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Vysíláme	vysílat	k5eAaImIp1nP	vysílat
dnes	dnes	k6eAd1	dnes
poselství	poselství	k1gNnSc1	poselství
olympijského	olympijský	k2eAgNnSc2d1	Olympijské
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
organizačního	organizační	k2eAgInSc2d1	organizační
výboru	výbor	k1gInSc2	výbor
pak	pak	k6eAd1	pak
pochodeň	pochodeň	k1gFnSc4	pochodeň
převzali	převzít	k5eAaPmAgMnP	převzít
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
metropoli	metropol	k1gFnSc6	metropol
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ji	on	k3xPp3gFnSc4	on
dopravili	dopravit	k5eAaPmAgMnP	dopravit
do	do	k7c2	do
Pekingu	Peking	k1gInSc2	Peking
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pochodeň	pochodeň	k1gFnSc1	pochodeň
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
pochodeň	pochodeň	k1gFnSc1	pochodeň
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
zamířila	zamířit	k5eAaPmAgFnS	zamířit
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
světa	svět	k1gInSc2	svět
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
vrchol	vrchol	k1gInSc4	vrchol
ji	on	k3xPp3gFnSc4	on
horolezci	horolezec	k1gMnPc1	horolezec
vynesli	vynést	k5eAaPmAgMnP	vynést
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
v	v	k7c4	v
devět	devět	k4xCc4	devět
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Kalendář	kalendář	k1gInSc1	kalendář
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
zpracován	zpracovat	k5eAaPmNgInS	zpracovat
dle	dle	k7c2	dle
programu	program	k1gInSc2	program
zveřejněného	zveřejněný	k2eAgInSc2d1	zveřejněný
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgNnPc1d1	modré
pole	pole	k1gNnPc1	pole
označují	označovat	k5eAaImIp3nP	označovat
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
probíhají	probíhat	k5eAaImIp3nP	probíhat
závody	závod	k1gInPc1	závod
v	v	k7c6	v
uvedeném	uvedený	k2eAgInSc6d1	uvedený
sportu	sport	k1gInSc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Žluté	žlutý	k2eAgNnSc1d1	žluté
pole	pole	k1gNnSc1	pole
označuje	označovat	k5eAaImIp3nS	označovat
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
boje	boj	k1gInPc1	boj
o	o	k7c4	o
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Číslice	číslice	k1gFnSc1	číslice
označuje	označovat	k5eAaImIp3nS	označovat
počet	počet	k1gInSc4	počet
disciplín	disciplína	k1gFnPc2	disciplína
daného	daný	k2eAgInSc2d1	daný
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
finále	finála	k1gFnSc3	finála
koná	konat	k5eAaImIp3nS	konat
<g/>
.	.	kIx.	.
viz	vidět	k5eAaImRp2nS	vidět
<g/>
:	:	kIx,	:
Zahajovací	zahajovací	k2eAgInSc1d1	zahajovací
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
XXIX	XXIX	kA	XXIX
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympiády	olympiáda	k1gFnSc2	olympiáda
O	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
skandálů	skandál	k1gInPc2	skandál
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
švédský	švédský	k2eAgMnSc1d1	švédský
zápasník	zápasník	k1gMnSc1	zápasník
Ara	ara	k1gMnSc1	ara
Abrahamjan	Abrahamjan	k1gMnSc1	Abrahamjan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
údajně	údajně	k6eAd1	údajně
nespravedlivému	spravedlivý	k2eNgInSc3d1	nespravedlivý
výsledku	výsledek	k1gInSc3	výsledek
svého	svůj	k3xOyFgInSc2	svůj
zápasu	zápas	k1gInSc2	zápas
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
při	při	k7c6	při
předávání	předávání	k1gNnSc6	předávání
medailí	medaile	k1gFnPc2	medaile
sestoupil	sestoupit	k5eAaPmAgInS	sestoupit
z	z	k7c2	z
pódia	pódium	k1gNnSc2	pódium
a	a	k8xC	a
položil	položit	k5eAaPmAgMnS	položit
svoji	svůj	k3xOyFgFnSc4	svůj
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
MOV	MOV	kA	MOV
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
nesportovní	sportovní	k2eNgNnSc4d1	nesportovní
chování	chování	k1gNnSc4	chování
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
z	z	k7c2	z
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Medailové	medailový	k2eAgNnSc4d1	medailové
pořadí	pořadí	k1gNnSc4	pořadí
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
205	[number]	k4	205
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
účast	účast	k1gFnSc4	účast
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
následující	následující	k2eAgFnPc1d1	následující
země	zem	k1gFnPc1	zem
<g/>
:	:	kIx,	:
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Marshallovy	Marshallův	k2eAgInPc4d1	Marshallův
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
Tuvalu	Tuvala	k1gFnSc4	Tuvala
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
udávají	udávat	k5eAaImIp3nP	udávat
počty	počet	k1gInPc1	počet
sportovců	sportovec	k1gMnPc2	sportovec
zastupujících	zastupující	k2eAgFnPc2d1	zastupující
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česko	Česko	k1gNnSc4	Česko
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
