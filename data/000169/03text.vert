<s>
Jedna	jeden	k4xCgFnSc1	jeden
ruka	ruka	k1gFnSc1	ruka
netleská	tleskat	k5eNaImIp3nS	tleskat
je	on	k3xPp3gFnPc4	on
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
režiséra	režisér	k1gMnSc2	režisér
Davida	David	k1gMnSc2	David
Ondříčka	Ondříček	k1gMnSc2	Ondříček
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc4d1	český
lev	lev	k1gInSc4	lev
2003	[number]	k4	2003
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
-	-	kIx~	-
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
P.	P.	kA	P.
Muchow	Muchow	k1gMnSc1	Muchow
nominace	nominace	k1gFnSc2	nominace
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zvuk	zvuk	k1gInSc4	zvuk
-	-	kIx~	-
Jakub	Jakub	k1gMnSc1	Jakub
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Rejholec	rejholec	k1gInSc4	rejholec
nominace	nominace	k1gFnSc2	nominace
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
filmový	filmový	k2eAgInSc4d1	filmový
plakát	plakát	k1gInSc4	plakát
-	-	kIx~	-
Zuzana	Zuzana	k1gFnSc1	Zuzana
Lednická	lednický	k2eAgFnSc1d1	Lednická
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
Najbrt	Najbrta	k1gFnPc2	Najbrta
Jedna	jeden	k4xCgFnSc1	jeden
ruka	ruka	k1gFnSc1	ruka
netleská	tleskat	k5eNaImIp3nS	tleskat
na	na	k7c6	na
webu	web	k1gInSc6	web
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Jedna	jeden	k4xCgFnSc1	jeden
ruka	ruka	k1gFnSc1	ruka
netleská	tleskat	k5eNaImIp3nS	tleskat
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Jedna	jeden	k4xCgFnSc1	jeden
ruka	ruka	k1gFnSc1	ruka
netleská	tleskat	k5eNaImIp3nS	tleskat
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Jedna	jeden	k4xCgFnSc1	jeden
ruka	ruka	k1gFnSc1	ruka
netleská	tleskat	k5eNaImIp3nS	tleskat
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Jedna	jeden	k4xCgFnSc1	jeden
ruka	ruka	k1gFnSc1	ruka
netleská	tleskat	k5eNaImIp3nS	tleskat
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databas	k1gInSc5	Databas
</s>
