<s>
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1798	[number]	k4	1798
Hodslavice	Hodslavice	k1gInPc4	Hodslavice
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1876	[number]	k4	1876
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
organizátor	organizátor	k1gMnSc1	organizátor
veřejného	veřejný	k2eAgInSc2d1	veřejný
kulturního	kulturní	k2eAgInSc2d1	kulturní
a	a	k8xC	a
vědeckého	vědecký	k2eAgInSc2d1	vědecký
života	život	k1gInSc2	život
v	v	k7c6	v
soudobé	soudobý	k2eAgFnSc6d1	soudobá
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderního	moderní	k2eAgNnSc2d1	moderní
českého	český	k2eAgNnSc2d1	české
dějepisectví	dějepisectví	k1gNnSc2	dějepisectví
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přezdívku	přezdívka	k1gFnSc4	přezdívka
Otec	otec	k1gMnSc1	otec
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Hodslavicích	Hodslavice	k1gInPc6	Hodslavice
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
Valašsko	Valašsko	k1gNnSc1	Valašsko
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
páté	pátá	k1gFnPc4	pátá
z	z	k7c2	z
12	[number]	k4	12
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
8	[number]	k4	8
se	se	k3xPyFc4	se
dožilo	dožít	k5eAaPmAgNnS	dožít
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
protestantské	protestantský	k2eAgFnSc2d1	protestantská
rodiny	rodina	k1gFnSc2	rodina
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
tradicí	tradice	k1gFnSc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Jiří	Jiří	k1gMnSc1	Jiří
Palacký	Palacký	k1gMnSc1	Palacký
(	(	kIx(	(
<g/>
*	*	kIx~	*
1768	[number]	k4	1768
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
lokálním	lokální	k2eAgInSc7d1	lokální
faktorem	faktor	k1gInSc7	faktor
domácích	domácí	k2eAgMnPc2d1	domácí
tkalců	tkadlec	k1gMnPc2	tkadlec
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Křižanová	Křižanová	k1gFnSc1	Křižanová
<g/>
,	,	kIx,	,
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
otec	otec	k1gMnSc1	otec
podruhé	podruhé	k6eAd1	podruhé
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Julianou	Juliana	k1gFnSc7	Juliana
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
bratři	bratr	k1gMnPc1	bratr
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
vzdělávali	vzdělávat	k5eAaImAgMnP	vzdělávat
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
1807	[number]	k4	1807
<g/>
-	-	kIx~	-
<g/>
1809	[number]	k4	1809
<g/>
)	)	kIx)	)
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
významné	významný	k2eAgFnSc2d1	významná
zámecké	zámecký	k2eAgFnSc2d1	zámecká
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Kunvaldu	Kunvald	k1gInSc6	Kunvald
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc4d1	dnešní
Kunín	Kunín	k1gInSc4	Kunín
<g/>
)	)	kIx)	)
u	u	k7c2	u
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1809	[number]	k4	1809
<g/>
-	-	kIx~	-
<g/>
1812	[number]	k4	1812
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
studiem	studio	k1gNnSc7	studio
latinské	latinský	k2eAgFnSc2d1	Latinská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Trenčíně	Trenčín	k1gInSc6	Trenčín
a	a	k8xC	a
následovalo	následovat	k5eAaImAgNnS	následovat
evangelické	evangelický	k2eAgNnSc1d1	evangelické
lyceum	lyceum	k1gNnSc1	lyceum
(	(	kIx(	(
<g/>
do	do	k7c2	do
r.	r.	kA	r.
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
v	v	k7c6	v
Prešpurku	Prešpurku	k?	Prešpurku
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
a	a	k8xC	a
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
s	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
Josefem	Josef	k1gMnSc7	Josef
Šafaříkem	Šafařík	k1gMnSc7	Šafařík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Trenčíně	Trenčín	k1gInSc6	Trenčín
bydlel	bydlet	k5eAaImAgMnS	bydlet
u	u	k7c2	u
otce	otec	k1gMnSc2	otec
Ľudovíta	Ľudovít	k1gMnSc2	Ľudovít
Štúra	Štúr	k1gMnSc2	Štúr
Samuela	Samuel	k1gMnSc2	Samuel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
také	také	k9	také
jeho	jeho	k3xOp3gMnSc7	jeho
učitelem	učitel	k1gMnSc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
si	se	k3xPyFc3	se
Palackého	Palackého	k2eAgMnSc1d1	Palackého
otec	otec	k1gMnSc1	otec
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
také	také	k9	také
z	z	k7c2	z
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
stal	stát	k5eAaPmAgMnS	stát
evangelický	evangelický	k2eAgMnSc1d1	evangelický
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
nepokračoval	pokračovat	k5eNaImAgMnS	pokračovat
dál	daleko	k6eAd2	daleko
ve	v	k7c6	v
studiích	studio	k1gNnPc6	studio
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1819	[number]	k4	1819
<g/>
-	-	kIx~	-
<g/>
1823	[number]	k4	1823
jako	jako	k8xS	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
do	do	k7c2	do
uherských	uherský	k2eAgFnPc2d1	uherská
šlechtických	šlechtický	k2eAgFnPc2d1	šlechtická
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
odmítnutí	odmítnutí	k1gNnSc2	odmítnutí
otcova	otcův	k2eAgNnSc2d1	otcovo
přání	přání	k1gNnSc2	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pastorem	pastor	k1gMnSc7	pastor
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
uherské	uherský	k2eAgFnSc3d1	uherská
šlechtičně	šlechtična	k1gFnSc3	šlechtična
Nině	Nina	k1gFnSc3	Nina
Zerdahelyové	Zerdahelyová	k1gFnSc3	Zerdahelyová
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc3d1	rozená
Báloghové	Báloghová	k1gFnSc3	Báloghová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
Palacký	Palacký	k1gMnSc1	Palacký
seznámil	seznámit	k5eAaPmAgMnS	seznámit
v	v	k7c6	v
Prešpurku	Prešpurku	k?	Prešpurku
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
1820	[number]	k4	1820
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
strávil	strávit	k5eAaPmAgMnS	strávit
jako	jako	k9	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
svěřenci	svěřenec	k1gMnPc7	svěřenec
Karlem	Karel	k1gMnSc7	Karel
a	a	k8xC	a
Janem	Jan	k1gMnSc7	Jan
Nepomukem	Nepomuk	k1gMnSc7	Nepomuk
Czúsovými	Czúsová	k1gFnPc7	Czúsová
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
působení	působení	k1gNnSc1	působení
ve	v	k7c6	v
vyšších	vysoký	k2eAgInPc6d2	vyšší
společenských	společenský	k2eAgInPc6d1	společenský
kruzích	kruh	k1gInPc6	kruh
mu	on	k3xPp3gMnSc3	on
jednak	jednak	k8xC	jednak
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
světa	svět	k1gInSc2	svět
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
měl	mít	k5eAaImAgMnS	mít
dost	dost	k6eAd1	dost
příležitosti	příležitost	k1gFnPc4	příležitost
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
sebevzdělávání	sebevzdělávání	k1gNnSc6	sebevzdělávání
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
filozofii	filozofie	k1gFnSc6	filozofie
a	a	k8xC	a
estetice	estetika	k1gFnSc6	estetika
(	(	kIx(	(
<g/>
ovlivněn	ovlivnit	k5eAaPmNgMnS	ovlivnit
byl	být	k5eAaImAgMnS	být
především	především	k9	především
I.	I.	kA	I.
Kantem	Kant	k1gMnSc7	Kant
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Schillerem	Schiller	k1gMnSc7	Schiller
a	a	k8xC	a
J.	J.	kA	J.
G.	G.	kA	G.
Herderem	Herder	k1gMnSc7	Herder
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
dobrý	dobrý	k2eAgInSc1d1	dobrý
přehled	přehled	k1gInSc1	přehled
o	o	k7c6	o
politice	politika	k1gFnSc6	politika
a	a	k8xC	a
politickém	politický	k2eAgNnSc6d1	politické
dění	dění	k1gNnSc6	dění
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
habsburských	habsburský	k2eAgFnPc6d1	habsburská
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
intenzívně	intenzívně	k6eAd1	intenzívně
studoval	studovat	k5eAaImAgInS	studovat
i	i	k9	i
cizí	cizí	k2eAgInPc4d1	cizí
jazyky	jazyk	k1gInPc4	jazyk
(	(	kIx(	(
<g/>
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
latinu	latina	k1gFnSc4	latina
<g/>
,	,	kIx,	,
řečtinu	řečtina	k1gFnSc4	řečtina
<g/>
,	,	kIx,	,
němčinu	němčina	k1gFnSc4	němčina
<g/>
,	,	kIx,	,
češtinu	čeština	k1gFnSc4	čeština
<g/>
,	,	kIx,	,
maďarštinu	maďarština	k1gFnSc4	maďarština
<g/>
,	,	kIx,	,
portugalštinu	portugalština	k1gFnSc4	portugalština
<g/>
,	,	kIx,	,
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
italštinu	italština	k1gFnSc4	italština
<g/>
,	,	kIx,	,
ruštinu	ruština	k1gFnSc4	ruština
a	a	k8xC	a
staroslověnštinu	staroslověnština	k1gFnSc4	staroslověnština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
pocházejí	pocházet	k5eAaImIp3nP	pocházet
časopisecké	časopisecký	k2eAgFnPc1d1	časopisecká
studie	studie	k1gFnPc1	studie
o	o	k7c6	o
estetice	estetika	k1gFnSc6	estetika
(	(	kIx(	(
<g/>
Přehled	přehled	k1gInSc1	přehled
dějin	dějiny	k1gFnPc2	dějiny
krasovědy	krasověda	k1gFnSc2	krasověda
nebo	nebo	k8xC	nebo
O	o	k7c6	o
krasovědě	krasověda	k1gFnSc6	krasověda
<g/>
)	)	kIx)	)
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Šafaříkem	Šafařík	k1gMnSc7	Šafařík
napsaní	napsaný	k2eAgMnPc1d1	napsaný
Počátkové	počátkový	k2eAgFnPc1d1	počátkový
českého	český	k2eAgNnSc2d1	české
básnictví	básnictví	k1gNnSc2	básnictví
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
prozódie	prozódie	k1gFnSc1	prozódie
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
měl	mít	k5eAaImAgMnS	mít
Palacký	Palacký	k1gMnSc1	Palacký
velké	velký	k2eAgFnSc2d1	velká
literární	literární	k2eAgFnSc2d1	literární
ambice	ambice	k1gFnSc2	ambice
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgInS	napsat
několik	několik	k4yIc4	několik
nepříliš	příliš	k6eNd1	příliš
hodnotných	hodnotný	k2eAgFnPc2d1	hodnotná
básní	báseň	k1gFnPc2	báseň
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
a	a	k8xC	a
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
časoměrným	časoměrný	k2eAgInSc7d1	časoměrný
veršem	verš	k1gInSc7	verš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
příležitostem	příležitost	k1gFnPc3	příležitost
<g/>
,	,	kIx,	,
přátelům	přítel	k1gMnPc3	přítel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
filosofickou	filosofický	k2eAgFnSc7d1	filosofická
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
ale	ale	k9	ale
velké	velký	k2eAgInPc4d1	velký
plány	plán	k1gInPc4	plán
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
tragédii	tragédie	k1gFnSc4	tragédie
o	o	k7c6	o
Kateřině	Kateřina	k1gFnSc6	Kateřina
Veliké	veliký	k2eAgFnSc6d1	veliká
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
o	o	k7c6	o
Janu	Jan	k1gMnSc6	Jan
Husovi	Hus	k1gMnSc6	Hus
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
historický	historický	k2eAgInSc4d1	historický
epos	epos	k1gInSc4	epos
o	o	k7c6	o
Napoleonovi	Napoleon	k1gMnSc6	Napoleon
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
děl	dělo	k1gNnPc2	dělo
nedokončil	dokončit	k5eNaPmAgInS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Pořídil	pořídit	k5eAaPmAgInS	pořídit
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
Ossiana	Ossian	k1gMnSc2	Ossian
<g/>
,	,	kIx,	,
podvržené	podvržený	k2eAgNnSc4d1	podvržené
básnické	básnický	k2eAgFnPc4d1	básnická
prózy	próza	k1gFnPc4	próza
z	z	k7c2	z
keltských	keltský	k2eAgFnPc2d1	keltská
dějin	dějiny	k1gFnPc2	dějiny
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1819	[number]	k4	1819
se	se	k3xPyFc4	se
Palacký	Palacký	k1gMnSc1	Palacký
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaImF	věnovat
především	především	k6eAd1	především
studiu	studio	k1gNnSc3	studio
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
kontaktoval	kontaktovat	k5eAaImAgInS	kontaktovat
Josefa	Josef	k1gMnSc4	Josef
Jungmanna	Jungmann	k1gMnSc4	Jungmann
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
žádal	žádat	k5eAaImAgMnS	žádat
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
a	a	k8xC	a
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
:	:	kIx,	:
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
základních	základní	k2eAgFnPc2d1	základní
historiografických	historiografický	k2eAgFnPc2d1	historiografická
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
zasvěcení	zasvěcení	k1gNnPc2	zasvěcení
do	do	k7c2	do
metodiky	metodika	k1gFnSc2	metodika
historikovy	historikův	k2eAgFnSc2d1	historikova
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Jungmannovi	Jungmann	k1gMnSc3	Jungmann
se	se	k3xPyFc4	se
Palackého	Palackého	k2eAgFnSc1d1	Palackého
cílevědomost	cílevědomost	k1gFnSc1	cílevědomost
<g/>
,	,	kIx,	,
koncepčnost	koncepčnost	k1gFnSc1	koncepčnost
a	a	k8xC	a
oddanost	oddanost	k1gFnSc1	oddanost
oboru	obor	k1gInSc2	obor
líbila	líbit	k5eAaImAgFnS	líbit
<g/>
,	,	kIx,	,
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
mu	on	k3xPp3gMnSc3	on
maximum	maximum	k1gNnSc4	maximum
svých	svůj	k3xOyFgFnPc2	svůj
znalostí	znalost	k1gFnPc2	znalost
i	i	k8xC	i
společenských	společenský	k2eAgInPc2d1	společenský
kontaktů	kontakt	k1gInPc2	kontakt
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
celoživotním	celoživotní	k2eAgMnSc7d1	celoživotní
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1823	[number]	k4	1823
Palacký	Palacký	k1gMnSc1	Palacký
napsal	napsat	k5eAaPmAgMnS	napsat
dopis	dopis	k1gInSc4	dopis
podobného	podobný	k2eAgNnSc2d1	podobné
znění	znění	k1gNnSc2	znění
i	i	k8xC	i
Josefu	Josef	k1gMnSc3	Josef
Dobrovskému	Dobrovský	k1gMnSc3	Dobrovský
a	a	k8xC	a
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
Palackého	Palacký	k1gMnSc2	Palacký
zaměstnal	zaměstnat	k5eAaPmAgMnS	zaměstnat
Josef	Josef	k1gMnSc1	Josef
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
František	František	k1gMnSc1	František
Šternberk-Manderscheid	Šternberk-Manderscheida	k1gFnPc2	Šternberk-Manderscheida
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodokmenů	rodokmen	k1gInPc2	rodokmen
<g/>
.	.	kIx.	.
</s>
<s>
Šternberkové	Šternberek	k1gMnPc1	Šternberek
ho	on	k3xPp3gMnSc4	on
posléze	posléze	k6eAd1	posléze
zaměstnali	zaměstnat	k5eAaPmAgMnP	zaměstnat
jako	jako	k8xS	jako
rodinného	rodinný	k2eAgMnSc4d1	rodinný
archiváře	archivář	k1gMnSc4	archivář
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Dobrovským	Dobrovský	k1gMnSc7	Dobrovský
měla	mít	k5eAaImAgFnS	mít
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
na	na	k7c4	na
Palackého	Palackého	k2eAgInSc4d1	Palackého
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
historiografickou	historiografický	k2eAgFnSc4d1	historiografická
kritickou	kritický	k2eAgFnSc4d1	kritická
metodu	metoda	k1gFnSc4	metoda
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
filologicko-kritickou	filologickoritický	k2eAgFnSc7d1	filologicko-kritický
metodou	metoda	k1gFnSc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Určité	určitý	k2eAgNnSc1d1	určité
napětí	napětí	k1gNnSc1	napětí
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
pak	pak	k6eAd1	pak
znamenal	znamenat	k5eAaImAgInS	znamenat
odlišný	odlišný	k2eAgInSc1d1	odlišný
názor	názor	k1gInSc1	názor
na	na	k7c4	na
pravost	pravost	k1gFnSc4	pravost
Rukopisu	rukopis	k1gInSc2	rukopis
zelenohorského	zelenohorský	k2eAgInSc2d1	zelenohorský
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Palacký	Palacký	k1gMnSc1	Palacký
s	s	k7c7	s
Dobrovského	Dobrovského	k2eAgFnSc7d1	Dobrovského
kritikou	kritika	k1gFnSc7	kritika
neztotožnil	ztotožnit	k5eNaPmAgMnS	ztotožnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
mezi	mezi	k7c7	mezi
vzdělanými	vzdělaný	k2eAgFnPc7d1	vzdělaná
a	a	k8xC	a
vlasteneckými	vlastenecký	k2eAgFnPc7d1	vlastenecká
kruhy	kruh	k1gInPc4	kruh
známou	známý	k2eAgFnSc7d1	známá
osobností	osobnost	k1gFnSc7	osobnost
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
výrazném	výrazný	k2eAgInSc6d1	výrazný
talentu	talent	k1gInSc6	talent
a	a	k8xC	a
intelektuálních	intelektuální	k2eAgFnPc6d1	intelektuální
kvalitách	kvalita	k1gFnPc6	kvalita
se	se	k3xPyFc4	se
nepochybovalo	pochybovat	k5eNaImAgNnS	pochybovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
Palacký	Palacký	k1gMnSc1	Palacký
začal	začít	k5eAaPmAgMnS	začít
vydávat	vydávat	k5eAaImF	vydávat
na	na	k7c4	na
nabídku	nabídka	k1gFnSc4	nabídka
hraběte	hrabě	k1gMnSc2	hrabě
Kašpara	Kašpar	k1gMnSc2	Kašpar
Šternberka	Šternberk	k1gInSc2	Šternberk
muzejní	muzejní	k2eAgInPc1d1	muzejní
časopisy	časopis	k1gInPc1	časopis
(	(	kIx(	(
<g/>
německý	německý	k2eAgInSc1d1	německý
měsíčník	měsíčník	k1gInSc1	měsíčník
Monatsschrift	Monatsschrifta	k1gFnPc2	Monatsschrifta
des	des	k1gNnSc2	des
böhmen	böhmen	k2eAgInSc4d1	böhmen
Museums	Museums	k1gInSc4	Museums
a	a	k8xC	a
český	český	k2eAgInSc4d1	český
čtvrtletník	čtvrtletník	k1gInSc4	čtvrtletník
Časopis	časopis	k1gInSc1	časopis
Společnosti	společnost	k1gFnSc2	společnost
vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
vlastenci	vlastenec	k1gMnPc7	vlastenec
bylo	být	k5eAaImAgNnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
sporů	spor	k1gInPc2	spor
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
s	s	k7c7	s
F.	F.	kA	F.
L.	L.	kA	L.
Čelakovským	Čelakovský	k2eAgMnPc3d1	Čelakovský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
nelíbila	líbit	k5eNaImAgFnS	líbit
forma	forma	k1gFnSc1	forma
a	a	k8xC	a
pravopis	pravopis	k1gInSc1	pravopis
těchto	tento	k3xDgInPc2	tento
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
sporech	spor	k1gInPc6	spor
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
projevovala	projevovat	k5eAaImAgFnS	projevovat
roztříštěnost	roztříštěnost	k1gFnSc1	roztříštěnost
českého	český	k2eAgNnSc2d1	české
vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
malý	malý	k2eAgInSc4d1	malý
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
německý	německý	k2eAgInSc4d1	německý
časopis	časopis	k1gInSc4	časopis
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
změněn	změnit	k5eAaPmNgMnS	změnit
na	na	k7c4	na
čtvrtletník	čtvrtletník	k1gInSc4	čtvrtletník
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
zrušen	zrušit	k5eAaPmNgInS	zrušit
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
začal	začít	k5eAaPmAgInS	začít
českou	český	k2eAgFnSc4d1	Česká
verzi	verze	k1gFnSc4	verze
redigovat	redigovat	k5eAaImF	redigovat
Pavel	Pavel	k1gMnSc1	Pavel
Josef	Josef	k1gMnSc1	Josef
Šafařík	Šafařík	k1gMnSc1	Šafařík
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
redaktorské	redaktorský	k2eAgFnSc2d1	redaktorská
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
na	na	k7c6	na
chodu	chod	k1gInSc6	chod
muzea	muzeum	k1gNnSc2	muzeum
podílel	podílet	k5eAaImAgInS	podílet
i	i	k9	i
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
muzejního	muzejní	k2eAgInSc2d1	muzejní
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
od	od	k7c2	od
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
1834	[number]	k4	1834
jako	jako	k8xS	jako
jednatel	jednatel	k1gMnSc1	jednatel
tohoto	tento	k3xDgInSc2	tento
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
se	se	k3xPyFc4	se
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
mladší	mladý	k2eAgFnSc7d2	mladší
dcerou	dcera	k1gFnSc7	dcera
velkostatkáře	velkostatkář	k1gMnSc2	velkostatkář
a	a	k8xC	a
advokáta	advokát	k1gMnSc2	advokát
Jana	Jan	k1gMnSc2	Jan
Měchury	Měchury	k?	Měchury
<g/>
,	,	kIx,	,
Terezií	Terezie	k1gFnSc7	Terezie
Měchurovou	Měchurův	k2eAgFnSc7d1	Měchurova
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
mu	on	k3xPp3gMnSc3	on
rodina	rodina	k1gFnSc1	rodina
i	i	k9	i
rodinný	rodinný	k2eAgMnSc1d1	rodinný
lékař	lékař	k1gMnSc1	lékař
Václav	Václav	k1gMnSc1	Václav
Staněk	Staněk	k1gMnSc1	Staněk
sňatek	sňatek	k1gInSc4	sňatek
rozmlouvali	rozmlouvat	k5eAaImAgMnP	rozmlouvat
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Terezie	Terezie	k1gFnSc1	Terezie
má	mít	k5eAaImIp3nS	mít
těžkou	těžký	k2eAgFnSc4d1	těžká
srdeční	srdeční	k2eAgFnSc4d1	srdeční
vadu	vada	k1gFnSc4	vada
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
manželství	manželství	k1gNnPc4	manželství
způsobilá	způsobilý	k2eAgFnSc1d1	způsobilá
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Terezií	Terezie	k1gFnSc7	Terezie
vychoval	vychovat	k5eAaPmAgMnS	vychovat
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
budoucího	budoucí	k2eAgMnSc2d1	budoucí
přírodovědce	přírodovědec	k1gMnSc2	přírodovědec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
provdanou	provdaný	k2eAgFnSc7d1	provdaná
za	za	k7c4	za
Františka	František	k1gMnSc4	František
Ladislava	Ladislav	k1gMnSc4	Ladislav
Riegera	Rieger	k1gMnSc4	Rieger
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
vychovatelem	vychovatel	k1gMnSc7	vychovatel
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Vladivoj	Vladivoj	k1gInSc4	Vladivoj
Tomek	Tomek	k1gMnSc1	Tomek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
Pamětech	paměť	k1gFnPc6	paměť
popsal	popsat	k5eAaPmAgMnS	popsat
těžkou	těžký	k2eAgFnSc4d1	těžká
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
měl	mít	k5eAaImAgMnS	mít
zejména	zejména	k9	zejména
s	s	k7c7	s
hyperaktivním	hyperaktivní	k2eAgMnSc7d1	hyperaktivní
a	a	k8xC	a
neposlušným	poslušný	k2eNgMnSc7d1	neposlušný
Janem	Jan	k1gMnSc7	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
dobrému	dobrý	k2eAgNnSc3d1	dobré
majetkovému	majetkový	k2eAgNnSc3d1	majetkové
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
rodiny	rodina	k1gFnSc2	rodina
Měchurových	Měchurův	k2eAgInPc2d1	Měchurův
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
rodiny	rodina	k1gFnSc2	rodina
svého	svůj	k3xOyFgMnSc2	svůj
zetě	zeť	k1gMnSc2	zeť
Františka	František	k1gMnSc2	František
Ladislava	Ladislav	k1gMnSc2	Ladislav
Riegera	Rieger	k1gMnSc2	Rieger
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
Palacký	Palacký	k1gMnSc1	Palacký
psát	psát	k5eAaImF	psát
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
rodinných	rodinný	k2eAgNnPc2d1	rodinné
panství	panství	k1gNnPc2	panství
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
ve	v	k7c6	v
víkendovém	víkendový	k2eAgInSc6d1	víkendový
Červeném	červený	k2eAgInSc6d1	červený
domě	dům	k1gInSc6	dům
(	(	kIx(	(
<g/>
též	též	k9	též
zvaném	zvaný	k2eAgInSc6d1	zvaný
Červený	červený	k2eAgInSc1d1	červený
lis	lis	k1gInSc1	lis
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
Libni	Libeň	k1gFnSc6	Libeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
zámcích	zámek	k1gInPc6	zámek
v	v	k7c6	v
Otíně	Otína	k1gFnSc6	Otína
<g/>
,	,	kIx,	,
Chocomyšli	Chocomyšle	k1gFnSc6	Chocomyšle
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Lobkovicích	Lobkovice	k1gInPc6	Lobkovice
u	u	k7c2	u
Mělníka	Mělník	k1gInSc2	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
chatrné	chatrný	k2eAgNnSc4d1	chatrné
zdraví	zdraví	k1gNnSc4	zdraví
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
(	(	kIx(	(
<g/>
měla	mít	k5eAaImAgFnS	mít
srdeční	srdeční	k2eAgFnSc4d1	srdeční
vadu	vada	k1gFnSc4	vada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Palacký	Palacký	k1gMnSc1	Palacký
velmi	velmi	k6eAd1	velmi
a	a	k8xC	a
stále	stále	k6eAd1	stále
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
jezdila	jezdit	k5eAaImAgFnS	jezdit
často	často	k6eAd1	často
do	do	k7c2	do
lázní	lázeň	k1gFnPc2	lázeň
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c4	na
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
Riviéru	Riviéra	k1gFnSc4	Riviéra
do	do	k7c2	do
Nice	Nice	k1gFnSc2	Nice
(	(	kIx(	(
<g/>
Nizza	Nizza	k1gFnSc1	Nizza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
začaly	začít	k5eAaPmAgInP	začít
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
české	český	k2eAgInPc1d1	český
stavy	stav	k1gInPc1	stav
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jej	on	k3xPp3gInSc4	on
chtěly	chtít	k5eAaImAgFnP	chtít
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Františka	František	k1gMnSc2	František
Pubičky	Pubička	k1gMnSc2	Pubička
jmenovat	jmenovat	k5eAaImF	jmenovat
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
oficiálního	oficiální	k2eAgMnSc2d1	oficiální
stavovského	stavovský	k2eAgMnSc2d1	stavovský
historiografa	historiograf	k1gMnSc2	historiograf
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
určité	určitý	k2eAgFnPc4d1	určitá
komplikace	komplikace	k1gFnPc4	komplikace
byl	být	k5eAaImAgMnS	být
opravdu	opravdu	k6eAd1	opravdu
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
až	až	k9	až
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
plat	plat	k1gInSc1	plat
ovšem	ovšem	k9	ovšem
dostával	dostávat	k5eAaImAgInS	dostávat
již	již	k6eAd1	již
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
dokončit	dokončit	k5eAaPmF	dokončit
Pubičkovy	Pubičkův	k2eAgFnPc4d1	Pubičkův
Chronologische	Chronologisch	k1gFnPc4	Chronologisch
Geschichte	Geschicht	k1gInSc5	Geschicht
Böhmens	Böhmens	k1gInSc1	Böhmens
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
metodicky	metodicky	k6eAd1	metodicky
i	i	k8xC	i
koncepčně	koncepčně	k6eAd1	koncepčně
zastaralé	zastaralý	k2eAgNnSc1d1	zastaralé
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
Palacký	Palacký	k1gMnSc1	Palacký
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
sepsat	sepsat	k5eAaPmF	sepsat
české	český	k2eAgFnPc4d1	Česká
dějiny	dějiny	k1gFnPc4	dějiny
úplně	úplně	k6eAd1	úplně
nově	nově	k6eAd1	nově
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pramenných	pramenný	k2eAgInPc2d1	pramenný
fondů	fond	k1gInPc2	fond
i	i	k8xC	i
diplomatického	diplomatický	k2eAgInSc2d1	diplomatický
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
i	i	k9	i
na	na	k7c4	na
sociální	sociální	k2eAgInSc4d1	sociální
<g/>
,	,	kIx,	,
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
<g/>
,	,	kIx,	,
náboženský	náboženský	k2eAgInSc4d1	náboženský
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc4d1	kulturní
vývoj	vývoj	k1gInSc4	vývoj
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Stavy	stav	k1gInPc1	stav
po	po	k7c6	po
usilovném	usilovný	k2eAgNnSc6d1	usilovné
přesvědčování	přesvědčování	k1gNnSc6	přesvědčování
svolily	svolit	k5eAaPmAgInP	svolit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1831	[number]	k4	1831
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
takto	takto	k6eAd1	takto
nově	nově	k6eAd1	nově
pojatých	pojatý	k2eAgFnPc2d1	pojatá
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
Geschichte	Geschicht	k1gInSc5	Geschicht
von	von	k1gInSc4	von
Böhmen	Böhmen	k1gInSc1	Böhmen
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1827	[number]	k4	1827
<g/>
-	-	kIx~	-
<g/>
1848	[number]	k4	1848
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
archivech	archiv	k1gInPc6	archiv
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vytěžil	vytěžit	k5eAaPmAgInS	vytěžit
písemné	písemný	k2eAgInPc4d1	písemný
prameny	pramen	k1gInPc4	pramen
k	k	k7c3	k
českým	český	k2eAgFnPc3d1	Česká
dějinám	dějiny	k1gFnPc3	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
archivem	archiv	k1gInSc7	archiv
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
<g/>
,	,	kIx,	,
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
,	,	kIx,	,
Lipsku	Lipsko	k1gNnSc6	Lipsko
a	a	k8xC	a
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
s	s	k7c7	s
malířem	malíř	k1gMnSc7	malíř
Josefem	Josef	k1gMnSc7	Josef
Vojtěchem	Vojtěch	k1gMnSc7	Vojtěch
Hellichem	Hellich	k1gMnSc7	Hellich
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Udine	Udin	k1gInSc5	Udin
<g/>
,	,	kIx,	,
Perugii	Perugie	k1gFnSc6	Perugie
<g/>
,	,	kIx,	,
Padově	Padova	k1gFnSc6	Padova
<g/>
,	,	kIx,	,
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
,	,	kIx,	,
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
studoval	studovat	k5eAaImAgMnS	studovat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
i	i	k8xC	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
městech	město	k1gNnPc6	město
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Přátelil	přátelit	k5eAaImAgMnS	přátelit
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
Jánem	Ján	k1gMnSc7	Ján
Kollárem	Kollár	k1gMnSc7	Kollár
<g/>
,	,	kIx,	,
Karlem	Karel	k1gMnSc7	Karel
Havlíčkem	Havlíček	k1gMnSc7	Havlíček
Borovským	Borovský	k1gMnSc7	Borovský
<g/>
,	,	kIx,	,
Egonem	Egon	k1gMnSc7	Egon
Ebertem	Ebert	k1gMnSc7	Ebert
<g/>
,	,	kIx,	,
či	či	k8xC	či
s	s	k7c7	s
lékaři	lékař	k1gMnPc7	lékař
Heldem	Held	k1gMnSc7	Held
a	a	k8xC	a
Staňkem	Staněk	k1gMnSc7	Staněk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
projekt	projekt	k1gInSc1	projekt
české	český	k2eAgFnSc2d1	Česká
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
slibném	slibný	k2eAgInSc6d1	slibný
začátku	začátek	k1gInSc6	začátek
ztroskotal	ztroskotat	k5eAaPmAgMnS	ztroskotat
a	a	k8xC	a
z	z	k7c2	z
kteréhožto	kteréhožto	k?	kteréhožto
pokusu	pokus	k1gInSc2	pokus
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
Matice	matice	k1gFnSc2	matice
česká	český	k2eAgNnPc1d1	české
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
financovat	financovat	k5eAaBmF	financovat
české	český	k2eAgFnPc4d1	Česká
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
nemohly	moct	k5eNaImAgFnP	moct
jako	jako	k9	jako
nelukrativní	lukrativní	k2eNgFnPc1d1	nelukrativní
vyjít	vyjít	k5eAaPmF	vyjít
u	u	k7c2	u
soukromých	soukromý	k2eAgMnPc2d1	soukromý
vydavatelů	vydavatel	k1gMnPc2	vydavatel
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc4d1	Česká
encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
vydal	vydat	k5eAaPmAgMnS	vydat
až	až	k9	až
jeho	jeho	k3xOp3gMnSc1	jeho
zeť	zeť	k1gMnSc1	zeť
František	František	k1gMnSc1	František
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rieger	Rieger	k1gMnSc1	Rieger
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Riegrův	Riegrův	k2eAgInSc4d1	Riegrův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
byl	být	k5eAaImAgInS	být
řádným	řádný	k2eAgInSc7d1	řádný
členem	člen	k1gInSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
nauk	nauka	k1gFnPc2	nauka
<g/>
,	,	kIx,	,
sekretářem	sekretář	k1gMnSc7	sekretář
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
-	-	kIx~	-
<g/>
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
direktorem	direktor	k1gMnSc7	direktor
(	(	kIx(	(
<g/>
předsedou	předseda	k1gMnSc7	předseda
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1835	[number]	k4	1835
<g/>
-	-	kIx~	-
<g/>
1836	[number]	k4	1836
a	a	k8xC	a
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1875	[number]	k4	1875
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
vyšel	vyjít	k5eAaPmAgInS	vyjít
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
Archivu	archiv	k1gInSc2	archiv
českého	český	k2eAgInSc2d1	český
<g/>
,	,	kIx,	,
edice	edice	k1gFnSc1	edice
česky	česky	k6eAd1	česky
psaných	psaný	k2eAgInPc2d1	psaný
historických	historický	k2eAgInPc2d1	historický
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
i	i	k9	i
podklady	podklad	k1gInPc1	podklad
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
díly	díl	k1gInPc4	díl
Geschichte	Geschicht	k1gInSc5	Geschicht
von	von	k1gInSc1	von
Böhmen	Böhmen	k2eAgMnSc1d1	Böhmen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
této	tento	k3xDgFnSc2	tento
edice	edice	k1gFnSc2	edice
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
také	také	k9	také
V.	V.	kA	V.
V.	V.	kA	V.
Tomek	Tomek	k1gMnSc1	Tomek
a	a	k8xC	a
K.	K.	kA	K.
J.	J.	kA	J.
Erben	Erben	k1gMnSc1	Erben
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
Palackým	Palacký	k1gMnSc7	Palacký
prozkoumávali	prozkoumávat	k5eAaImAgMnP	prozkoumávat
archívy	archív	k1gInPc4	archív
a	a	k8xC	a
opisovali	opisovat	k5eAaImAgMnP	opisovat
listiny	listina	k1gFnPc4	listina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Tomkem	Tomek	k1gMnSc7	Tomek
začal	začít	k5eAaPmAgMnS	začít
posléze	posléze	k6eAd1	posléze
Palacký	Palacký	k1gMnSc1	Palacký
počítat	počítat	k5eAaImF	počítat
jako	jako	k9	jako
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
zástupcem	zástupce	k1gMnSc7	zástupce
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
stavovského	stavovský	k2eAgMnSc2d1	stavovský
historiografa	historiograf	k1gMnSc2	historiograf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
připravoval	připravovat	k5eAaImAgMnS	připravovat
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
třetí	třetí	k4xOgFnSc3	třetí
díl	díl	k1gInSc4	díl
svých	svůj	k3xOyFgNnPc6	svůj
Geschichte	Geschicht	k1gInSc5	Geschicht
von	von	k1gInSc4	von
Böhmen	Böhmen	k1gInSc4	Böhmen
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgInS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
vývoj	vývoj	k1gInSc1	vývoj
státu	stát	k1gInSc2	stát
v	v	k7c6	v
době	doba	k1gFnSc6	doba
předhusitské	předhusitský	k2eAgFnSc2d1	předhusitská
a	a	k8xC	a
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
během	během	k7c2	během
publikování	publikování	k1gNnSc2	publikování
dílčích	dílčí	k2eAgFnPc2d1	dílčí
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
při	při	k7c6	při
schvalování	schvalování	k1gNnSc6	schvalování
konečné	konečný	k2eAgFnSc2d1	konečná
verze	verze	k1gFnSc2	verze
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
cenzurou	cenzura	k1gFnSc7	cenzura
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gMnSc4	on
obvinila	obvinit	k5eAaPmAgFnS	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podání	podání	k1gNnSc1	podání
počátků	počátek	k1gInPc2	počátek
husitství	husitství	k1gNnSc2	husitství
je	být	k5eAaImIp3nS	být
namířeno	namířit	k5eAaPmNgNnS	namířit
proti	proti	k7c3	proti
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
protestant	protestant	k1gMnSc1	protestant
<g/>
,	,	kIx,	,
úzkostlivě	úzkostlivě	k6eAd1	úzkostlivě
snažil	snažit	k5eAaImAgMnS	snažit
své	svůj	k3xOyFgNnSc4	svůj
náboženské	náboženský	k2eAgNnSc4d1	náboženské
cítění	cítění	k1gNnSc4	cítění
do	do	k7c2	do
vědecké	vědecký	k2eAgFnSc2d1	vědecká
práce	práce	k1gFnSc2	práce
nevměšovat	vměšovat	k5eNaImF	vměšovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
čistý	čistý	k2eAgInSc4d1	čistý
překlad	překlad	k1gInSc4	překlad
<g/>
,	,	kIx,	,
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
verzi	verze	k1gFnSc6	verze
prohloubil	prohloubit	k5eAaPmAgMnS	prohloubit
historiografickou	historiografický	k2eAgFnSc4d1	historiografická
reflexi	reflexe	k1gFnSc4	reflexe
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
kladl	klást	k5eAaImAgMnS	klást
větší	veliký	k2eAgInSc4d2	veliký
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
jako	jako	k8xC	jako
nositele	nositel	k1gMnSc2	nositel
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
vyšel	vyjít	k5eAaPmAgInS	vyjít
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
jeho	on	k3xPp3gNnSc2	on
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
díla	dílo	k1gNnSc2	dílo
Dějiny	dějiny	k1gFnPc4	dějiny
národu	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
v	v	k7c6	v
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c4	o
revoluci	revoluce	k1gFnSc4	revoluce
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1848	[number]	k4	1848
zastával	zastávat	k5eAaImAgMnS	zastávat
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
české	český	k2eAgNnSc1d1	české
národní	národní	k2eAgNnSc1d1	národní
hnutí	hnutí	k1gNnSc1	hnutí
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
mělo	mít	k5eAaImAgNnS	mít
držet	držet	k5eAaImF	držet
legálních	legální	k2eAgInPc2d1	legální
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
stavěl	stavět	k5eAaImAgMnS	stavět
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
velmi	velmi	k6eAd1	velmi
rezervovaně	rezervovaně	k6eAd1	rezervovaně
vůči	vůči	k7c3	vůči
aktivitám	aktivita	k1gFnPc3	aktivita
Svatováclavského	svatováclavský	k2eAgInSc2d1	svatováclavský
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
raději	rád	k6eAd2	rád
do	do	k7c2	do
aktivit	aktivita	k1gFnPc2	aktivita
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
guberniální	guberniální	k2eAgFnSc2d1	guberniální
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
šéf	šéf	k1gMnSc1	šéf
zemské	zemský	k2eAgFnSc2d1	zemská
správy	správa	k1gFnSc2	správa
hrabě	hrabě	k1gMnSc1	hrabě
Rudolf	Rudolf	k1gMnSc1	Rudolf
Stadion	stadion	k1gNnSc4	stadion
jako	jako	k8xS	jako
protiváhu	protiváha	k1gFnSc4	protiváha
Svatováclavského	svatováclavský	k2eAgInSc2d1	svatováclavský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Palacký	Palacký	k1gMnSc1	Palacký
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
vcelku	vcelku	k6eAd1	vcelku
neúspěšně	úspěšně	k6eNd1	úspěšně
snažil	snažit	k5eAaImAgMnS	snažit
mírnit	mírnit	k5eAaImF	mírnit
rostoucí	rostoucí	k2eAgNnSc4d1	rostoucí
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc7	Němec
a	a	k8xC	a
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
české	český	k2eAgInPc4d1	český
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přijaly	přijmout	k5eAaPmAgInP	přijmout
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
zásady	zásada	k1gFnPc4	zásada
liberálního	liberální	k2eAgInSc2d1	liberální
konstitucionalismu	konstitucionalismus	k1gInSc2	konstitucionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
si	se	k3xPyFc3	se
však	však	k9	však
Palacký	Palacký	k1gMnSc1	Palacký
získal	získat	k5eAaPmAgMnS	získat
roli	role	k1gFnSc4	role
politického	politický	k2eAgMnSc2d1	politický
vůdce	vůdce	k1gMnSc2	vůdce
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
když	když	k8xS	když
nejdříve	dříve	k6eAd3	dříve
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1848	[number]	k4	1848
anonymně	anonymně	k6eAd1	anonymně
v	v	k7c6	v
Národních	národní	k2eAgFnPc6d1	národní
novinách	novina	k1gFnPc6	novina
publikoval	publikovat	k5eAaBmAgInS	publikovat
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
Češi	Čech	k1gMnPc1	Čech
nemohou	moct	k5eNaImIp3nP	moct
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
národním	národní	k2eAgNnSc6d1	národní
sjednocení	sjednocení	k1gNnSc6	sjednocení
a	a	k8xC	a
poslat	poslat	k5eAaPmF	poslat
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
do	do	k7c2	do
frankfurtského	frankfurtský	k2eAgInSc2d1	frankfurtský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1848	[number]	k4	1848
"	"	kIx"	"
<g/>
Psaním	psaní	k1gNnSc7	psaní
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
"	"	kIx"	"
s	s	k7c7	s
obdobným	obdobný	k2eAgNnSc7d1	obdobné
zdůvodněním	zdůvodnění	k1gNnSc7	zdůvodnění
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
pozvání	pozvání	k1gNnSc4	pozvání
stát	stát	k1gInSc1	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
přípravného	přípravný	k2eAgInSc2d1	přípravný
výboru	výbor	k1gInSc2	výbor
tohoto	tento	k3xDgInSc2	tento
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Palacký	Palacký	k1gMnSc1	Palacký
v	v	k7c4	v
"	"	kIx"	"
<g/>
Psaní	psaní	k1gNnSc4	psaní
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
"	"	kIx"	"
rovněž	rovněž	k9	rovněž
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
koncept	koncept	k1gInSc1	koncept
austroslavismu	austroslavismus	k1gInSc2	austroslavismus
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
idea	idea	k1gFnSc1	idea
rakouského	rakouský	k2eAgInSc2d1	rakouský
státu	stát	k1gInSc2	stát
zastřešujícího	zastřešující	k2eAgInSc2d1	zastřešující
různorodé	různorodý	k2eAgInPc4d1	různorodý
rovnoprávné	rovnoprávný	k2eAgInPc4d1	rovnoprávný
národy	národ	k1gInPc4	národ
nejen	nejen	k6eAd1	nejen
odporuje	odporovat	k5eAaImIp3nS	odporovat
zájmům	zájem	k1gInPc3	zájem
německého	německý	k2eAgNnSc2d1	německé
sjednocovacího	sjednocovací	k2eAgNnSc2d1	sjednocovací
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
polským	polský	k2eAgMnSc7d1	polský
a	a	k8xC	a
maďarským	maďarský	k2eAgFnPc3d1	maďarská
nacionálním	nacionální	k2eAgFnPc3d1	nacionální
snahám	snaha	k1gFnPc3	snaha
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
že	že	k8xS	že
realizace	realizace	k1gFnSc1	realizace
austroslavistického	austroslavistický	k2eAgInSc2d1	austroslavistický
konceptu	koncept	k1gInSc2	koncept
by	by	kYmCp3nS	by
velmi	velmi	k6eAd1	velmi
omezila	omezit	k5eAaPmAgFnS	omezit
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
habsburské	habsburský	k2eAgFnSc2d1	habsburská
dynastie	dynastie	k1gFnSc2	dynastie
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1848	[number]	k4	1848
byla	být	k5eAaImAgFnS	být
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
guberniální	guberniální	k2eAgFnSc1d1	guberniální
komise	komise	k1gFnSc1	komise
sloučena	sloučit	k5eAaPmNgFnS	sloučit
se	s	k7c7	s
Svatováclavským	svatováclavský	k2eAgInSc7d1	svatováclavský
výborem	výbor	k1gInSc7	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gMnSc7	člen
takto	takto	k6eAd1	takto
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
i	i	k9	i
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
účastnil	účastnit	k5eAaImAgInS	účastnit
vyhroceného	vyhrocený	k2eAgNnSc2d1	vyhrocené
jednání	jednání	k1gNnSc2	jednání
s	s	k7c7	s
delegáty	delegát	k1gMnPc7	delegát
frankfurtského	frankfurtský	k2eAgInSc2d1	frankfurtský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Palacký	Palacký	k1gMnSc1	Palacký
dostal	dostat	k5eAaPmAgMnS	dostat
překvapivou	překvapivý	k2eAgFnSc4d1	překvapivá
nabídku	nabídka	k1gFnSc4	nabídka
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
ministrem	ministr	k1gMnSc7	ministr
vyučování	vyučování	k1gNnSc2	vyučování
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
odlišnému	odlišný	k2eAgInSc3d1	odlišný
názoru	názor	k1gInSc3	názor
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Franze	Franze	k1gFnSc2	Franze
Pillersdorfera	Pillersdorfero	k1gNnSc2	Pillersdorfero
na	na	k7c4	na
frankfurtský	frankfurtský	k2eAgInSc4d1	frankfurtský
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nabídku	nabídka	k1gFnSc4	nabídka
nového	nový	k2eAgMnSc4d1	nový
guberniálního	guberniální	k2eAgMnSc4d1	guberniální
prezidenta	prezident	k1gMnSc4	prezident
Lva	Lev	k1gMnSc4	Lev
Thuna	Thun	k1gMnSc4	Thun
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
provizorní	provizorní	k2eAgFnSc6d1	provizorní
zemské	zemský	k2eAgFnSc6d1	zemská
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
Palacký	Palacký	k1gMnSc1	Palacký
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
přijal	přijmout	k5eAaPmAgMnS	přijmout
jako	jako	k8xS	jako
šanci	šance	k1gFnSc4	šance
podpořit	podpořit	k5eAaPmF	podpořit
Thunovy	Thunův	k2eAgFnPc4d1	Thunova
autonomistické	autonomistický	k2eAgFnPc4d1	autonomistická
snahy	snaha	k1gFnPc4	snaha
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
však	však	k9	však
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jde	jít	k5eAaImIp3nS	jít
Thunovi	Thun	k1gMnSc3	Thun
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
členy	člen	k1gMnPc7	člen
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
o	o	k7c6	o
vymanění	vymanění	k1gNnSc6	vymanění
se	se	k3xPyFc4	se
z	z	k7c2	z
vlivu	vliv	k1gInSc2	vliv
"	"	kIx"	"
<g/>
revoluční	revoluční	k2eAgFnSc2d1	revoluční
<g/>
"	"	kIx"	"
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
základny	základna	k1gFnSc2	základna
ke	k	k7c3	k
kontrarevoluci	kontrarevoluce	k1gFnSc3	kontrarevoluce
<g/>
,	,	kIx,	,
provizorní	provizorní	k2eAgFnSc4d1	provizorní
zemskou	zemský	k2eAgFnSc4d1	zemská
vládu	vláda	k1gFnSc4	vláda
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
začal	začít	k5eAaPmAgInS	začít
připravovat	připravovat	k5eAaImF	připravovat
Slovanský	slovanský	k2eAgInSc1d1	slovanský
sjezd	sjezd	k1gInSc1	sjezd
<g/>
.	.	kIx.	.
</s>
<s>
Thun	Thun	k1gMnSc1	Thun
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
pořádáním	pořádání	k1gNnSc7	pořádání
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
Palacký	Palacký	k1gMnSc1	Palacký
zaručil	zaručit	k5eAaPmAgMnS	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
slovanské	slovanský	k2eAgInPc1d1	slovanský
národy	národ	k1gInPc1	národ
vysloví	vyslovit	k5eAaPmIp3nP	vyslovit
podporu	podpora	k1gFnSc4	podpora
habsburské	habsburský	k2eAgFnSc3d1	habsburská
monarchii	monarchie	k1gFnSc3	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Palacký	Palacký	k1gMnSc1	Palacký
však	však	k9	však
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
intenzivní	intenzivní	k2eAgFnSc6d1	intenzivní
snaze	snaha	k1gFnSc6	snaha
vtisknout	vtisknout	k5eAaPmF	vtisknout
sjezdu	sjezd	k1gInSc6	sjezd
austroslavický	austroslavický	k2eAgInSc4d1	austroslavický
ráz	ráz	k1gInSc4	ráz
příliš	příliš	k6eAd1	příliš
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
sjelo	sjet	k5eAaPmAgNnS	sjet
i	i	k9	i
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
zástupců	zástupce	k1gMnPc2	zástupce
ze	z	k7c2	z
slovanských	slovanský	k2eAgNnPc2d1	slovanské
území	území	k1gNnPc2	území
mimo	mimo	k7c4	mimo
habsburskou	habsburský	k2eAgFnSc4d1	habsburská
monarchii	monarchie	k1gFnSc4	monarchie
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
delegáti	delegát	k1gMnPc1	delegát
polské	polský	k2eAgFnSc2d1	polská
emigrace	emigrace	k1gFnSc2	emigrace
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
nebo	nebo	k8xC	nebo
ruský	ruský	k2eAgMnSc1d1	ruský
emigrant	emigrant	k1gMnSc1	emigrant
Michail	Michail	k1gMnSc1	Michail
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Bakunin	Bakunin	k2eAgMnSc1d1	Bakunin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c6	na
zachování	zachování	k1gNnSc6	zachování
monarchie	monarchie	k1gFnSc2	monarchie
neměli	mít	k5eNaImAgMnP	mít
žádný	žádný	k3yNgInSc4	žádný
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Palacký	Palacký	k1gMnSc1	Palacký
tedy	tedy	k8xC	tedy
sice	sice	k8xC	sice
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
sjezdového	sjezdový	k2eAgMnSc2d1	sjezdový
předsedy	předseda	k1gMnSc2	předseda
sjezd	sjezd	k1gInSc4	sjezd
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
zahájil	zahájit	k5eAaPmAgMnS	zahájit
<g/>
,	,	kIx,	,
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
však	však	k9	však
nabídnout	nabídnout	k5eAaPmF	nabídnout
takový	takový	k3xDgInSc4	takový
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
dokázal	dokázat	k5eAaPmAgInS	dokázat
překlenout	překlenout	k5eAaPmF	překlenout
různorodé	různorodý	k2eAgInPc4d1	různorodý
zájmy	zájem	k1gInPc4	zájem
zástupců	zástupce	k1gMnPc2	zástupce
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
byl	být	k5eAaImAgInS	být
předčasně	předčasně	k6eAd1	předčasně
ukončen	ukončit	k5eAaPmNgInS	ukončit
již	již	k9	již
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
kvůli	kvůli	k7c3	kvůli
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
Palacký	Palacký	k1gMnSc1	Palacký
snažil	snažit	k5eAaImAgMnS	snažit
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
kompromis	kompromis	k1gInSc4	kompromis
mezi	mezi	k7c7	mezi
povstalci	povstalec	k1gMnPc7	povstalec
a	a	k8xC	a
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
přiměl	přimět	k5eAaPmAgMnS	přimět
radikální	radikální	k2eAgMnPc4d1	radikální
studenty	student	k1gMnPc4	student
k	k	k7c3	k
propuštění	propuštění	k1gNnSc3	propuštění
zajatého	zajatý	k2eAgMnSc2d1	zajatý
guberniálního	guberniální	k2eAgMnSc2d1	guberniální
prezidenta	prezident	k1gMnSc2	prezident
Thuna	Thun	k1gMnSc2	Thun
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
umírněnost	umírněnost	k1gFnSc4	umírněnost
se	se	k3xPyFc4	se
Palacký	Palacký	k1gMnSc1	Palacký
nevyhnul	vyhnout	k5eNaPmAgMnS	vyhnout
nařčení	nařčení	k1gNnSc4	nařčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
prý	prý	k9	prý
byl	být	k5eAaImAgMnS	být
původcem	původce	k1gMnSc7	původce
revolučního	revoluční	k2eAgInSc2d1	revoluční
komplotu	komplot	k1gInSc2	komplot
a	a	k8xC	a
že	že	k8xS	že
Slovanský	slovanský	k2eAgInSc1d1	slovanský
sjezd	sjezd	k1gInSc1	sjezd
měl	mít	k5eAaImAgInS	mít
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
představovat	představovat	k5eAaImF	představovat
ideovou	ideový	k2eAgFnSc4d1	ideová
přípravu	příprava	k1gFnSc4	příprava
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
Slovanů	Slovan	k1gInPc2	Slovan
proti	proti	k7c3	proti
Němcům	Němec	k1gMnPc3	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
ústavodárného	ústavodárný	k2eAgInSc2d1	ústavodárný
parlamentu	parlament	k1gInSc2	parlament
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c4	za
volební	volební	k2eAgInSc4d1	volební
obvod	obvod	k1gInSc4	obvod
Praha-město	Prahaěsta	k1gFnSc5	Praha-města
II	II	kA	II
a	a	k8xC	a
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
roli	role	k1gFnSc4	role
mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
poslanci	poslanec	k1gMnPc7	poslanec
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
ústavní	ústavní	k2eAgFnSc2d1	ústavní
subkomise	subkomise	k1gFnSc2	subkomise
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
již	již	k6eAd1	již
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1848	[number]	k4	1848
návrh	návrh	k1gInSc4	návrh
rakouské	rakouský	k2eAgFnSc2d1	rakouská
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
principy	princip	k1gInPc4	princip
austroslavismu	austroslavismus	k1gInSc2	austroslavismus
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
najít	najít	k5eAaPmF	najít
rovnováhu	rovnováha	k1gFnSc4	rovnováha
mezi	mezi	k7c7	mezi
centralismem	centralismus	k1gInSc7	centralismus
a	a	k8xC	a
federalismem	federalismus	k1gInSc7	federalismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentním	parlamentní	k2eAgNnSc6d1	parlamentní
plénu	plénum	k1gNnSc6	plénum
však	však	k9	však
místo	místo	k7c2	místo
projednání	projednání	k1gNnSc2	projednání
tohoto	tento	k3xDgInSc2	tento
návrhu	návrh	k1gInSc2	návrh
bylo	být	k5eAaImAgNnS	být
upřednostněno	upřednostněn	k2eAgNnSc1d1	upřednostněno
jednání	jednání	k1gNnSc1	jednání
o	o	k7c6	o
palčivější	palčivý	k2eAgFnSc6d2	palčivější
otázce	otázka	k1gFnSc6	otázka
zrušení	zrušení	k1gNnSc1	zrušení
poddanství	poddanství	k1gNnSc2	poddanství
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
jej	on	k3xPp3gMnSc4	on
manželka	manželka	k1gFnSc1	manželka
Terezie	Terezie	k1gFnPc4	Terezie
v	v	k7c6	v
korespondenci	korespondence	k1gFnSc6	korespondence
v	v	k7c6	v
konzervativním	konzervativní	k2eAgMnSc6d1	konzervativní
duchu	duch	k1gMnSc6	duch
nabádala	nabádat	k5eAaImAgFnS	nabádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
nestal	stát	k5eNaPmAgInS	stát
přílišným	přílišný	k2eAgMnSc7d1	přílišný
demokratem	demokrat	k1gMnSc7	demokrat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
přání	přání	k1gNnSc4	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
mandátu	mandát	k1gInSc2	mandát
a	a	k8xC	a
raději	rád	k6eAd2	rád
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
bezpečnějšímu	bezpečný	k2eAgNnSc3d2	bezpečnější
povolání	povolání	k1gNnSc3	povolání
historika	historik	k1gMnSc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
těmto	tento	k3xDgNnPc3	tento
doporučením	doporučení	k1gNnPc3	doporučení
se	se	k3xPyFc4	se
Palacký	Palacký	k1gMnSc1	Palacký
ohradil	ohradit	k5eAaPmAgMnS	ohradit
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ovšem	ovšem	k9	ovšem
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
podruhé	podruhé	k6eAd1	podruhé
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
nabídku	nabídka	k1gFnSc4	nabídka
pozice	pozice	k1gFnSc2	pozice
ministra	ministr	k1gMnSc2	ministr
kultu	kult	k1gInSc2	kult
a	a	k8xC	a
vyučování	vyučování	k1gNnSc2	vyučování
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1848	[number]	k4	1848
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
další	další	k2eAgFnSc1d1	další
revoluční	revoluční	k2eAgFnSc1d1	revoluční
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
čeští	český	k2eAgMnPc1d1	český
poslanci	poslanec	k1gMnPc1	poslanec
raději	rád	k6eAd2	rád
město	město	k1gNnSc4	město
opustili	opustit	k5eAaPmAgMnP	opustit
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vydali	vydat	k5eAaPmAgMnP	vydat
deklaraci	deklarace	k1gFnSc4	deklarace
<g/>
,	,	kIx,	,
že	že	k8xS	že
usnesení	usnesení	k1gNnSc4	usnesení
parlamentu	parlament	k1gInSc2	parlament
nepovažují	považovat	k5eNaImIp3nP	považovat
za	za	k7c4	za
závazná	závazný	k2eAgNnPc4d1	závazné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
prý	prý	k9	prý
vynucena	vynutit	k5eAaPmNgFnS	vynutit
revolučním	revoluční	k2eAgInSc7d1	revoluční
terorem	teror	k1gInSc7	teror
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgNnSc4d1	stejné
tvrzení	tvrzení	k1gNnSc4	tvrzení
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
prohlášení	prohlášení	k1gNnSc6	prohlášení
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
i	i	k9	i
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
navíc	navíc	k6eAd1	navíc
požadoval	požadovat	k5eAaImAgMnS	požadovat
bezodkladné	bezodkladný	k2eAgNnSc4d1	bezodkladné
přeložení	přeložení	k1gNnSc4	přeložení
sněmu	sněm	k1gInSc2	sněm
do	do	k7c2	do
Kroměříže	Kroměříž	k1gFnSc2	Kroměříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ústavodárný	ústavodárný	k2eAgInSc1d1	ústavodárný
sněm	sněm	k1gInSc1	sněm
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1848	[number]	k4	1848
skutečně	skutečně	k6eAd1	skutečně
zahájil	zahájit	k5eAaPmAgMnS	zahájit
své	svůj	k3xOyFgNnSc4	svůj
jednání	jednání	k1gNnSc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Palacký	Palacký	k1gMnSc1	Palacký
se	se	k3xPyFc4	se
na	na	k7c6	na
obnoveném	obnovený	k2eAgInSc6d1	obnovený
sněmu	sněm	k1gInSc6	sněm
snažil	snažit	k5eAaImAgMnS	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
klub	klub	k1gInSc4	klub
slovanských	slovanský	k2eAgMnPc2d1	slovanský
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
austroslavistické	austroslavistický	k2eAgNnSc4d1	austroslavistický
zaměření	zaměření	k1gNnSc4	zaměření
však	však	k9	však
stěží	stěží	k6eAd1	stěží
mohlo	moct	k5eAaImAgNnS	moct
přilákat	přilákat	k5eAaPmF	přilákat
haličské	haličský	k2eAgInPc4d1	haličský
Poláky	polák	k1gInPc4	polák
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
posledním	poslední	k2eAgInPc3d1	poslední
revolučním	revoluční	k2eAgInPc3d1	revoluční
neúspěchům	neúspěch	k1gInPc3	neúspěch
velkoněmců	velkoněmec	k1gMnPc2	velkoněmec
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
Palacký	Palacký	k1gMnSc1	Palacký
mylně	mylně	k6eAd1	mylně
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
austroslavismus	austroslavismus	k1gInSc1	austroslavismus
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
dané	daný	k2eAgFnPc4d1	daná
situace	situace	k1gFnPc4	situace
nebývale	nebývale	k6eAd1	nebývale
velké	velký	k2eAgFnPc4d1	velká
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
prosazení	prosazení	k1gNnSc4	prosazení
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
pomalu	pomalu	k6eAd1	pomalu
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
porážka	porážka	k1gFnSc1	porážka
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
revoluce	revoluce	k1gFnSc2	revoluce
znamená	znamenat	k5eAaImIp3nS	znamenat
především	především	k9	především
vítězství	vítězství	k1gNnSc1	vítězství
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgMnSc1d1	nový
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
toleruje	tolerovat	k5eAaImIp3nS	tolerovat
jednání	jednání	k1gNnSc4	jednání
sněmu	sněm	k1gInSc2	sněm
z	z	k7c2	z
pouhé	pouhý	k2eAgFnSc2d1	pouhá
nezbytnosti	nezbytnost	k1gFnSc2	nezbytnost
a	a	k8xC	a
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
Felixe	Felix	k1gMnSc2	Felix
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
hodlá	hodlat	k5eAaImIp3nS	hodlat
jednat	jednat	k5eAaImF	jednat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
nad	nad	k7c7	nad
Palackého	Palackého	k2eAgInSc7d1	Palackého
federalickým	federalický	k2eAgInSc7d1	federalický
návrhem	návrh	k1gInSc7	návrh
ústavy	ústava	k1gFnSc2	ústava
získala	získat	k5eAaPmAgFnS	získat
navrch	navrch	k6eAd1	navrch
centralistická	centralistický	k2eAgFnSc1d1	centralistická
předloha	předloha	k1gFnSc1	předloha
Kajetana	Kajetan	k1gMnSc2	Kajetan
Mayera	Mayer	k1gMnSc2	Mayer
<g/>
,	,	kIx,	,
a	a	k8xC	a
frustrovaný	frustrovaný	k2eAgMnSc1d1	frustrovaný
Palacký	Palacký	k1gMnSc1	Palacký
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
počátkem	počátkem	k7c2	počátkem
února	únor	k1gInSc2	únor
vzdal	vzdát	k5eAaPmAgInS	vzdát
funkce	funkce	k1gFnSc2	funkce
místopředsedy	místopředseda	k1gMnSc2	místopředseda
v	v	k7c6	v
ústavním	ústavní	k2eAgInSc6d1	ústavní
výboru	výbor	k1gInSc6	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1849	[number]	k4	1849
se	se	k3xPyFc4	se
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
poslanci	poslanec	k1gMnPc1	poslanec
sešli	sejít	k5eAaPmAgMnP	sejít
s	s	k7c7	s
ministrem	ministr	k1gMnSc7	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Franzem	Franz	k1gMnSc7	Franz
Stadionem	stadion	k1gInSc7	stadion
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jim	on	k3xPp3gMnPc3	on
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
bude	být	k5eAaImBp3nS	být
kroměřížský	kroměřížský	k2eAgInSc1d1	kroměřížský
sněm	sněm	k1gInSc1	sněm
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
a	a	k8xC	a
oktrojována	oktrojován	k2eAgFnSc1d1	oktrojována
Stadionova	Stadionův	k2eAgFnSc1d1	Stadionova
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Palackého	Palackého	k2eAgInSc1d1	Palackého
tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
událostí	událost	k1gFnSc7	událost
zaskočil	zaskočit	k5eAaPmAgInS	zaskočit
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
čeští	český	k2eAgMnPc1d1	český
politici	politik	k1gMnPc1	politik
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
po	po	k7c6	po
rozpuštění	rozpuštění	k1gNnSc6	rozpuštění
sněmu	sněm	k1gInSc2	sněm
zachovat	zachovat	k5eAaPmF	zachovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
zachovat	zachovat	k5eAaPmF	zachovat
klid	klid	k1gInSc4	klid
a	a	k8xC	a
počkat	počkat	k5eAaPmF	počkat
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgInS	být
unaven	unavit	k5eAaPmNgInS	unavit
politickými	politický	k2eAgInPc7d1	politický
spory	spor	k1gInPc7	spor
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgInS	chtít
věnovat	věnovat	k5eAaImF	věnovat
své	svůj	k3xOyFgFnSc3	svůj
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
práci	práce	k1gFnSc4	práce
historika	historik	k1gMnSc2	historik
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
roce	rok	k1gInSc6	rok
zanedbával	zanedbávat	k5eAaImAgMnS	zanedbávat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
stavu	stav	k1gInSc2	stav
obležení	obležení	k1gNnSc2	obležení
na	na	k7c4	na
Prahou	Praha	k1gFnSc7	Praha
kvůli	kvůli	k7c3	kvůli
Májovému	májový	k2eAgNnSc3d1	Májové
spiknutí	spiknutí	k1gNnSc3	spiknutí
Palacký	Palacký	k1gMnSc1	Palacký
pobýval	pobývat	k5eAaImAgMnS	pobývat
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
na	na	k7c6	na
studijních	studijní	k2eAgInPc6d1	studijní
a	a	k8xC	a
léčebných	léčebný	k2eAgInPc6d1	léčebný
pobytech	pobyt	k1gInPc6	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
v	v	k7c6	v
sobě	se	k3xPyFc3	se
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
choval	chovat	k5eAaImAgMnS	chovat
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vymoženosti	vymoženost	k1gFnSc3	vymoženost
revolučních	revoluční	k2eAgNnPc2d1	revoluční
let	léto	k1gNnPc2	léto
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1849	[number]	k4	1849
podaří	podařit	k5eAaPmIp3nS	podařit
udržet	udržet	k5eAaPmF	udržet
a	a	k8xC	a
že	že	k8xS	že
represe	represe	k1gFnPc4	represe
postihnou	postihnout	k5eAaPmIp3nP	postihnout
jen	jen	k9	jen
radikální	radikální	k2eAgMnPc4d1	radikální
demokraty	demokrat	k1gMnPc4	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
dalo	dát	k5eAaPmAgNnS	dát
české	český	k2eAgNnSc1d1	české
místodržitelství	místodržitelství	k1gNnSc1	místodržitelství
Palackého	Palacký	k1gMnSc2	Palacký
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
"	"	kIx"	"
<g/>
zkompromitovaných	zkompromitovaný	k2eAgFnPc2d1	zkompromitovaná
<g/>
"	"	kIx"	"
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
policejně	policejně	k6eAd1	policejně
sledovány	sledovat	k5eAaImNgInP	sledovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1850	[number]	k4	1850
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
ještě	ještě	k6eAd1	ještě
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
pražského	pražský	k2eAgInSc2d1	pražský
širšího	široký	k2eAgInSc2d2	širší
městského	městský	k2eAgInSc2d1	městský
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1851	[number]	k4	1851
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
proti	proti	k7c3	proti
návrhu	návrh	k1gInSc3	návrh
udělit	udělit	k5eAaPmF	udělit
čestné	čestný	k2eAgNnSc4d1	čestné
občanství	občanství	k1gNnSc4	občanství
Felixi	Felix	k1gMnSc3	Felix
Schwarzenbergovi	Schwarzenberg	k1gMnSc3	Schwarzenberg
<g/>
,	,	kIx,	,
donutily	donutit	k5eAaPmAgInP	donutit
jej	on	k3xPp3gInSc4	on
úřady	úřad	k1gInPc7	úřad
odejít	odejít	k5eAaPmF	odejít
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ostatně	ostatně	k6eAd1	ostatně
vyslyšel	vyslyšet	k5eAaPmAgMnS	vyslyšet
i	i	k8xC	i
přání	přání	k1gNnSc4	přání
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
úřednímu	úřední	k2eAgInSc3d1	úřední
tlaku	tlak	k1gInSc3	tlak
se	se	k3xPyFc4	se
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1852	[number]	k4	1852
vzdal	vzdát	k5eAaPmAgMnS	vzdát
i	i	k9	i
funkce	funkce	k1gFnSc1	funkce
předsedy	předseda	k1gMnSc2	předseda
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
sbírku	sbírka	k1gFnSc4	sbírka
na	na	k7c4	na
zakoupení	zakoupení	k1gNnSc4	zakoupení
pozemku	pozemek	k1gInSc2	pozemek
pro	pro	k7c4	pro
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Palacký	Palacký	k1gMnSc1	Palacký
tak	tak	k6eAd1	tak
chtěl	chtít	k5eAaImAgMnS	chtít
svou	svůj	k3xOyFgFnSc4	svůj
energii	energie	k1gFnSc4	energie
investovat	investovat	k5eAaBmF	investovat
do	do	k7c2	do
rozvoje	rozvoj	k1gInSc2	rozvoj
činnosti	činnost	k1gFnSc2	činnost
Českého	český	k2eAgNnSc2d1	české
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
jako	jako	k8xC	jako
Šafařík	Šafařík	k1gMnSc1	Šafařík
či	či	k8xC	či
Čelakovský	Čelakovský	k2eAgInSc1d1	Čelakovský
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
naučného	naučný	k2eAgInSc2d1	naučný
slovníku	slovník	k1gInSc2	slovník
podílet	podílet	k5eAaImF	podílet
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1852	[number]	k4	1852
mj.	mj.	kA	mj.
kvůli	kvůli	k7c3	kvůli
agitaci	agitace	k1gFnSc3	agitace
Palackého	Palackého	k2eAgMnPc2d1	Palackého
odpůrců	odpůrce	k1gMnPc2	odpůrce
nebyl	být	k5eNaImAgMnS	být
znovuzvolen	znovuzvolit	k5eAaPmNgMnS	znovuzvolit
do	do	k7c2	do
muzejního	muzejní	k2eAgInSc2d1	muzejní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnPc4d1	následující
léta	léto	k1gNnPc4	léto
trávil	trávit	k5eAaImAgInS	trávit
Palacký	Palacký	k1gMnSc1	Palacký
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Lobkovice	Lobkovice	k1gInPc4	Lobkovice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
Terezie	Terezie	k1gFnSc1	Terezie
Palacká	Palacká	k1gFnSc1	Palacká
zdědila	zdědit	k5eAaPmAgFnS	zdědit
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
studijními	studijní	k2eAgInPc7d1	studijní
pobyty	pobyt	k1gInPc7	pobyt
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
byl	být	k5eAaImAgInS	být
občas	občas	k6eAd1	občas
sledován	sledovat	k5eAaImNgInS	sledovat
rakouskými	rakouský	k2eAgInPc7d1	rakouský
agenty	agens	k1gInPc7	agens
<g/>
.	.	kIx.	.
</s>
<s>
Palacký	Palacký	k1gMnSc1	Palacký
dále	daleko	k6eAd2	daleko
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
historickém	historický	k2eAgNnSc6d1	historické
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
dílech	dílo	k1gNnPc6	dílo
Geschichte	Geschicht	k1gInSc5	Geschicht
von	von	k1gInSc1	von
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
stalo	stát	k5eAaPmAgNnS	stát
zdrojem	zdroj	k1gInSc7	zdroj
sporů	spor	k1gInPc2	spor
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
zemským	zemský	k2eAgInSc7d1	zemský
výborem	výbor	k1gInSc7	výbor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
financující	financující	k2eAgFnSc2d1	financující
instituce	instituce	k1gFnSc2	instituce
postavil	postavit	k5eAaPmAgInS	postavit
proti	proti	k7c3	proti
Palackého	Palackého	k2eAgInSc3d1	Palackého
záměru	záměr	k1gInSc3	záměr
vydávat	vydávat	k5eAaImF	vydávat
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
originálně	originálně	k6eAd1	originálně
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
namísto	namísto	k7c2	namísto
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
se	se	k3xPyFc4	se
Palacký	Palacký	k1gMnSc1	Palacký
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
Riegrově	Riegrův	k2eAgInSc6d1	Riegrův
Slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
autorský	autorský	k2eAgInSc1d1	autorský
kolektiv	kolektiv	k1gInSc1	kolektiv
sestával	sestávat	k5eAaImAgInS	sestávat
i	i	k9	i
z	z	k7c2	z
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
Palackého	Palackého	k2eAgMnSc1d1	Palackého
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
stranily	stranit	k5eAaImAgFnP	stranit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Václav	Václav	k1gMnSc1	Václav
Vladivoj	Vladivoj	k1gInSc1	Vladivoj
Tomek	Tomek	k1gMnSc1	Tomek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
vydal	vydat	k5eAaPmAgInS	vydat
také	také	k9	také
několik	několik	k4yIc4	několik
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
o	o	k7c4	o
Rukopisy	rukopis	k1gInPc4	rukopis
hájil	hájit	k5eAaImAgMnS	hájit
jejich	jejich	k3xOp3gFnSc4	jejich
pravost	pravost	k1gFnSc4	pravost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jej	on	k3xPp3gNnSc2	on
názorově	názorově	k6eAd1	názorově
sbližovalo	sbližovat	k5eAaImAgNnS	sbližovat
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
členy	člen	k1gMnPc7	člen
"	"	kIx"	"
<g/>
vládní	vládní	k2eAgFnSc2d1	vládní
strany	strana	k1gFnSc2	strana
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
a	a	k8xC	a
Hermenegildem	Hermenegild	k1gMnSc7	Hermenegild
Jirečkem	Jireček	k1gMnSc7	Jireček
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
začínali	začínat	k5eAaImAgMnP	začínat
být	být	k5eAaImF	být
zklamáni	zklamat	k5eAaPmNgMnP	zklamat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
loajalitu	loajalita	k1gFnSc4	loajalita
vůči	vůči	k7c3	vůči
vládě	vláda	k1gFnSc3	vláda
nemohou	moct	k5eNaImIp3nP	moct
příliš	příliš	k6eAd1	příliš
realizovat	realizovat	k5eAaBmF	realizovat
své	svůj	k3xOyFgFnPc4	svůj
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
českém	český	k2eAgNnSc6d1	české
kulturním	kulturní	k2eAgNnSc6d1	kulturní
obrození	obrození	k1gNnSc6	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Palacký	Palacký	k1gMnSc1	Palacký
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
1860	[number]	k4	1860
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Nice	Nice	k1gFnSc6	Nice
<g/>
,	,	kIx,	,
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nevyjadřoval	vyjadřovat	k5eNaImAgMnS	vyjadřovat
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
náznakům	náznak	k1gInPc3	náznak
liberalizace	liberalizace	k1gFnSc2	liberalizace
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
byl	být	k5eAaImAgInS	být
skeptický	skeptický	k2eAgMnSc1d1	skeptický
<g/>
,	,	kIx,	,
politickou	politický	k2eAgFnSc4d1	politická
iniciativu	iniciativa	k1gFnSc4	iniciativa
přenechával	přenechávat	k5eAaImAgInS	přenechávat
svému	svůj	k3xOyFgMnSc3	svůj
zeti	zeť	k1gMnSc3	zeť
F.	F.	kA	F.
L.	L.	kA	L.
Riegerovi	Rieger	k1gMnSc3	Rieger
a	a	k8xC	a
omezil	omezit	k5eAaPmAgInS	omezit
se	se	k3xPyFc4	se
na	na	k7c6	na
spolupodepsání	spolupodepsání	k1gNnSc6	spolupodepsání
Riegrova	Riegrův	k2eAgNnSc2d1	Riegrovo
memoranda	memorandum	k1gNnSc2	memorandum
o	o	k7c6	o
potřebách	potřeba	k1gFnPc6	potřeba
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
z	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
měla	mít	k5eAaImAgFnS	mít
těžká	těžký	k2eAgFnSc1d1	těžká
nemoc	nemoc	k1gFnSc1	nemoc
Terezie	Terezie	k1gFnSc2	Terezie
Palacké	Palacká	k1gFnSc2	Palacká
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
během	během	k7c2	během
návratu	návrat	k1gInSc2	návrat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1860	[number]	k4	1860
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
období	období	k1gNnSc2	období
neoabsolutismu	neoabsolutismus	k1gInSc2	neoabsolutismus
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
vyšel	vyjít	k5eAaPmAgMnS	vyjít
podle	podle	k7c2	podle
Jiřího	Jiří	k1gMnSc2	Jiří
Štaifa	Štaif	k1gMnSc2	Štaif
jako	jako	k8xS	jako
nezpochybnitelná	zpochybnitelný	k2eNgFnSc1d1	nezpochybnitelná
morální	morální	k2eAgFnSc1d1	morální
autorita	autorita	k1gFnSc1	autorita
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
radikální	radikální	k2eAgMnPc1d1	radikální
demokraté	demokrat	k1gMnPc1	demokrat
mu	on	k3xPp3gMnSc3	on
byli	být	k5eAaImAgMnP	být
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přiznat	přiznat	k5eAaPmF	přiznat
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
Otce	otec	k1gMnSc2	otec
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
jako	jako	k9	jako
dějepisec	dějepisec	k1gMnSc1	dějepisec
neúnavně	únavně	k6eNd1	únavně
vychovával	vychovávat	k5eAaImAgMnS	vychovávat
český	český	k2eAgInSc4d1	český
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Respekt	respekt	k1gInSc1	respekt
vůči	vůči	k7c3	vůči
Palackému	Palacký	k1gMnSc3	Palacký
projevily	projevit	k5eAaPmAgInP	projevit
i	i	k9	i
mnohé	mnohý	k2eAgFnPc1d1	mnohá
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mu	on	k3xPp3gMnSc3	on
udělily	udělit	k5eAaPmAgFnP	udělit
čestné	čestný	k2eAgNnSc4d1	čestné
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
projevy	projev	k1gInPc1	projev
většinou	většinou	k6eAd1	většinou
nekritické	kritický	k2eNgFnSc2d1	nekritická
úcty	úcta	k1gFnSc2	úcta
nicméně	nicméně	k8xC	nicméně
vedly	vést	k5eAaImAgInP	vést
Palackého	Palackého	k2eAgInPc1d1	Palackého
k	k	k7c3	k
autoritativnosti	autoritativnost	k1gFnSc3	autoritativnost
<g/>
.	.	kIx.	.
</s>
<s>
Autoritu	autorita	k1gFnSc4	autorita
Palackého	Palacký	k1gMnSc2	Palacký
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
i	i	k9	i
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
jej	on	k3xPp3gMnSc4	on
jako	jako	k8xS	jako
prvního	první	k4xOgMnSc2	první
Čecha	Čech	k1gMnSc2	Čech
nešlechtického	šlechtický	k2eNgInSc2d1	šlechtický
původu	původ	k1gInSc2	původ
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1861	[number]	k4	1861
jmenoval	jmenovat	k5eAaBmAgInS	jmenovat
doživotním	doživotní	k2eAgInSc7d1	doživotní
členem	člen	k1gInSc7	člen
panské	panský	k2eAgFnSc2d1	Panská
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Palacký	Palacký	k1gMnSc1	Palacký
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
Únorovou	únorový	k2eAgFnSc4d1	únorová
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
prosazoval	prosazovat	k5eAaImAgInS	prosazovat
autonomní	autonomní	k2eAgNnPc4d1	autonomní
práva	právo	k1gNnPc4	právo
zemských	zemský	k2eAgInPc2d1	zemský
sněmů	sněm	k1gInPc2	sněm
<g/>
,	,	kIx,	,
tíživě	tíživě	k6eAd1	tíživě
však	však	k9	však
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc1	jeho
projevy	projev	k1gInPc1	projev
nemají	mít	k5eNaImIp3nP	mít
kýžený	kýžený	k2eAgInSc4d1	kýžený
ohlas	ohlas	k1gInSc4	ohlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
září	září	k1gNnSc6	září
1861	[number]	k4	1861
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
pasivní	pasivní	k2eAgFnSc4d1	pasivní
rezistenci	rezistence	k1gFnSc4	rezistence
vůči	vůči	k7c3	vůči
panské	panský	k2eAgFnSc3d1	Panská
sněmovně	sněmovna	k1gFnSc3	sněmovna
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Palacký	Palacký	k1gMnSc1	Palacký
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
zachování	zachování	k1gNnSc4	zachování
své	svůj	k3xOyFgFnSc2	svůj
vůdčí	vůdčí	k2eAgFnSc2d1	vůdčí
role	role	k1gFnSc2	role
mezi	mezi	k7c7	mezi
poslanci	poslanec	k1gMnPc7	poslanec
Českého	český	k2eAgInSc2d1	český
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgNnSc2	jenž
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
hned	hned	k6eAd1	hned
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
volebních	volební	k2eAgInPc6d1	volební
obvodech	obvod	k1gInPc6	obvod
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mandát	mandát	k1gInSc1	mandát
přijal	přijmout	k5eAaPmAgInS	přijmout
za	za	k7c4	za
volební	volební	k2eAgInSc4d1	volební
obvod	obvod	k1gInSc4	obvod
Karlín	Karlín	k1gInSc4	Karlín
v	v	k7c6	v
kurii	kurie	k1gFnSc6	kurie
venkovských	venkovský	k2eAgFnPc2d1	venkovská
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
pak	pak	k6eAd1	pak
neúspěšně	úspěšně	k6eNd1	úspěšně
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
zejména	zejména	k9	zejména
volební	volební	k2eAgFnSc4d1	volební
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
odstranit	odstranit	k5eAaPmF	odstranit
nevyváženost	nevyváženost	k1gFnSc4	nevyváženost
českojazyčných	českojazyčný	k2eAgInPc2d1	českojazyčný
a	a	k8xC	a
německojazyčných	německojazyčný	k2eAgInPc2d1	německojazyčný
volebních	volební	k2eAgInPc2d1	volební
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
také	také	k9	také
českého	český	k2eAgInSc2d1	český
spolkového	spolkový	k2eAgInSc2d1	spolkový
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
spoluzakládal	spoluzakládat	k5eAaImAgInS	spoluzakládat
spolek	spolek	k1gInSc1	spolek
Svatobor	Svatobor	k1gInSc4	Svatobor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jehož	jehož	k3xOyRp3gMnSc1	jehož
předseda	předseda	k1gMnSc1	předseda
pak	pak	k6eAd1	pak
působil	působit	k5eAaImAgMnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
svěřeno	svěřit	k5eAaPmNgNnS	svěřit
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
poklepání	poklepání	k1gNnSc1	poklepání
na	na	k7c4	na
základní	základní	k2eAgInSc4d1	základní
kámen	kámen	k1gInSc4	kámen
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
čelným	čelný	k2eAgMnPc3d1	čelný
řečníkům	řečník	k1gMnPc3	řečník
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
národních	národní	k2eAgFnPc6d1	národní
oslavách	oslava	k1gFnPc6	oslava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
přezdívce	přezdívka	k1gFnSc3	přezdívka
Otec	otec	k1gMnSc1	otec
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Palackého	Palackého	k2eAgFnPc1d1	Palackého
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
posílení	posílení	k1gNnSc4	posílení
kolektivního	kolektivní	k2eAgNnSc2d1	kolektivní
českého	český	k2eAgNnSc2d1	české
vědomí	vědomí	k1gNnSc2	vědomí
začala	začít	k5eAaPmAgFnS	začít
záhy	záhy	k6eAd1	záhy
narušovat	narušovat	k5eAaImF	narušovat
mladá	mladý	k2eAgFnSc1d1	mladá
generace	generace	k1gFnSc1	generace
českých	český	k2eAgMnPc2d1	český
liberálů	liberál	k1gMnPc2	liberál
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nehodlala	hodlat	k5eNaImAgFnS	hodlat
Palackého	Palackého	k2eAgInPc4d1	Palackého
názory	názor	k1gInPc4	názor
nekriticky	kriticky	k6eNd1	kriticky
přejímat	přejímat	k5eAaImF	přejímat
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
si	se	k3xPyFc3	se
Palacký	Palacký	k1gMnSc1	Palacký
bral	brát	k5eAaImAgMnS	brát
velmi	velmi	k6eAd1	velmi
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Rodící	rodící	k2eAgFnSc1d1	rodící
se	se	k3xPyFc4	se
mladočeská	mladočeský	k2eAgFnSc1d1	mladočeská
frakce	frakce	k1gFnSc1	frakce
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
zejména	zejména	k9	zejména
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
Palackého	Palacký	k1gMnSc2	Palacký
vůči	vůči	k7c3	vůči
polskému	polský	k2eAgNnSc3d1	polské
povstání	povstání	k1gNnSc3	povstání
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
a	a	k8xC	a
demokratizaci	demokratizace	k1gFnSc4	demokratizace
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Palacký	Palacký	k1gMnSc1	Palacký
nemohl	moct	k5eNaImAgMnS	moct
prosadit	prosadit	k5eAaPmF	prosadit
své	svůj	k3xOyFgFnPc4	svůj
vize	vize	k1gFnPc4	vize
uspořádání	uspořádání	k1gNnSc2	uspořádání
státu	stát	k1gInSc2	stát
ani	ani	k8xC	ani
na	na	k7c4	na
říšské	říšský	k2eAgNnSc4d1	říšské
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
na	na	k7c6	na
zemské	zemský	k2eAgFnSc6d1	zemská
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
sepsal	sepsat	k5eAaPmAgMnS	sepsat
je	být	k5eAaImIp3nS	být
alespoň	alespoň	k9	alespoň
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
v	v	k7c4	v
pojednání	pojednání	k1gNnSc4	pojednání
Idea	idea	k1gFnSc1	idea
státu	stát	k1gInSc2	stát
rakouského	rakouský	k2eAgInSc2d1	rakouský
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
i	i	k8xC	i
proslulou	proslulý	k2eAgFnSc4d1	proslulá
větu	věta	k1gFnSc4	věta
"	"	kIx"	"
<g/>
Byli	být	k5eAaImAgMnP	být
jsme	být	k5eAaImIp1nP	být
před	před	k7c7	před
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
,	,	kIx,	,
budeme	být	k5eAaImBp1nP	být
i	i	k9	i
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Rakousko-uherskému	rakouskoherský	k2eAgNnSc3d1	rakousko-uherské
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
ovšem	ovšem	k9	ovšem
čeští	český	k2eAgMnPc1d1	český
politici	politik	k1gMnPc1	politik
s	s	k7c7	s
Palackým	Palacký	k1gMnSc7	Palacký
a	a	k8xC	a
Riegerem	Rieger	k1gMnSc7	Rieger
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
získat	získat	k5eAaPmF	získat
účinnou	účinný	k2eAgFnSc4d1	účinná
podporu	podpora	k1gFnSc4	podpora
ani	ani	k8xC	ani
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
česká	český	k2eAgFnSc1d1	Česká
delegace	delegace	k1gFnSc1	delegace
vykonala	vykonat	k5eAaPmAgFnS	vykonat
"	"	kIx"	"
<g/>
pouť	pouť	k1gFnSc4	pouť
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
rakousko-uherské	rakouskoherský	k2eAgNnSc4d1	rakousko-uherské
vyrovnání	vyrovnání	k1gNnSc4	vyrovnání
pak	pak	k6eAd1	pak
81	[number]	k4	81
poslanců	poslanec	k1gMnPc2	poslanec
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Palackým	Palacký	k1gMnSc7	Palacký
a	a	k8xC	a
Riegerem	Rieger	k1gMnSc7	Rieger
vydalo	vydat	k5eAaPmAgNnS	vydat
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1868	[number]	k4	1868
české	český	k2eAgNnSc4d1	české
státoprávní	státoprávní	k2eAgNnSc4d1	státoprávní
prohlášení	prohlášení	k1gNnSc4	prohlášení
(	(	kIx(	(
<g/>
státoprávní	státoprávní	k2eAgFnSc2d1	státoprávní
deklarace	deklarace	k1gFnSc2	deklarace
českých	český	k2eAgMnPc2d1	český
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
Palacký	Palacký	k1gMnSc1	Palacký
opět	opět	k6eAd1	opět
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
Karlín	Karlín	k1gInSc1	Karlín
obhájil	obhájit	k5eAaPmAgInS	obhájit
mandát	mandát	k1gInSc4	mandát
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
zemském	zemský	k2eAgInSc6d1	zemský
sněmu	sněm	k1gInSc6	sněm
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
i	i	k8xC	i
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
neúčasti	neúčast	k1gFnSc3	neúčast
na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
září	září	k1gNnSc6	září
1868	[number]	k4	1868
sice	sice	k8xC	sice
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
českými	český	k2eAgMnPc7d1	český
poslanci	poslanec	k1gMnPc7	poslanec
zbaven	zbavit	k5eAaPmNgMnS	zbavit
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
1869	[number]	k4	1869
však	však	k9	však
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
řádných	řádný	k2eAgFnPc6d1	řádná
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c4	za
městskou	městský	k2eAgFnSc4d1	městská
kurii	kurie	k1gFnSc4	kurie
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
Praha-Nové	Praha-Nové	k2eAgNnSc1d1	Praha-Nové
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ale	ale	k9	ale
v	v	k7c4	v
pasivní	pasivní	k2eAgFnSc4d1	pasivní
opozici	opozice	k1gFnSc4	opozice
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
periodicky	periodicky	k6eAd1	periodicky
zbavován	zbavovat	k5eAaImNgInS	zbavovat
mandátu	mandát	k1gInSc3	mandát
a	a	k8xC	a
opětovně	opětovně	k6eAd1	opětovně
volen	volit	k5eAaImNgInS	volit
v	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
1875	[number]	k4	1875
a	a	k8xC	a
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
Palacký	Palacký	k1gMnSc1	Palacký
vydal	vydat	k5eAaPmAgMnS	vydat
poslední	poslední	k2eAgInSc4d1	poslední
díl	díl	k1gInSc4	díl
Geschichte	Geschicht	k1gInSc5	Geschicht
von	von	k1gInSc1	von
Böhmen	Böhmen	k2eAgInSc1d1	Böhmen
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tak	tak	k6eAd1	tak
dovedl	dovést	k5eAaPmAgMnS	dovést
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
ostré	ostrý	k2eAgFnSc2d1	ostrá
historiografické	historiografický	k2eAgFnSc2d1	historiografická
polemiky	polemika	k1gFnSc2	polemika
se	se	k3xPyFc4	se
spolkem	spolek	k1gInSc7	spolek
česko-německých	českoěmecký	k2eAgMnPc2d1	česko-německý
historiků	historik	k1gMnPc2	historik
(	(	kIx(	(
<g/>
Konstantin	Konstantin	k1gMnSc1	Konstantin
Höfler	Höfler	k1gMnSc1	Höfler
<g/>
,	,	kIx,	,
Ludwig	Ludwig	k1gMnSc1	Ludwig
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Lippert	Lippert	k1gMnSc1	Lippert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
ocenění	ocenění	k1gNnSc4	ocenění
od	od	k7c2	od
Matice	matice	k1gFnSc2	matice
slovenské	slovenský	k2eAgFnSc2d1	slovenská
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnSc2d1	historická
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
a	a	k8xC	a
Lublani	Lublaň	k1gFnSc6	Lublaň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
pak	pak	k6eAd1	pak
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
řád	řád	k1gInSc4	řád
Železné	železný	k2eAgFnSc2d1	železná
koruny	koruna	k1gFnSc2	koruna
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
nárokovat	nárokovat	k5eAaImF	nárokovat
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
tohoto	tento	k3xDgNnSc2	tento
práva	právo	k1gNnSc2	právo
ovšem	ovšem	k9	ovšem
nikdy	nikdy	k6eAd1	nikdy
nevyužil	využít	k5eNaPmAgMnS	využít
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1875	[number]	k4	1875
zastával	zastávat	k5eAaImAgMnS	zastávat
pozici	pozice	k1gFnSc4	pozice
prezidenta	prezident	k1gMnSc2	prezident
Královské	královský	k2eAgFnSc2d1	královská
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
nauk	nauka	k1gFnPc2	nauka
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
zemřel	zemřít	k5eAaPmAgMnS	zemřít
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
celonárodní	celonárodní	k2eAgFnSc4d1	celonárodní
tryznu	tryzna	k1gFnSc4	tryzna
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
truchlících	truchlící	k2eAgMnPc2d1	truchlící
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rodinném	rodinný	k2eAgNnSc6d1	rodinné
rozloučení	rozloučení	k1gNnSc6	rozloučení
se	se	k3xPyFc4	se
zesnulým	zesnulý	k1gMnPc3	zesnulý
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
nabalzámované	nabalzámovaný	k2eAgNnSc1d1	nabalzámovaný
tělo	tělo	k1gNnSc1	tělo
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
v	v	k7c6	v
zasedací	zasedací	k2eAgFnSc6d1	zasedací
síni	síň	k1gFnSc6	síň
Staroměstské	staroměstský	k2eAgFnSc2d1	Staroměstská
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
pak	pak	k6eAd1	pak
převezeno	převézt	k5eAaPmNgNnS	převézt
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Lobkovicích	Lobkovice	k1gInPc6	Lobkovice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
Palackého	Palackého	k2eAgNnSc1d1	Palackého
historické	historický	k2eAgNnSc1d1	historické
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
milníkem	milník	k1gInSc7	milník
české	český	k2eAgFnSc2d1	Česká
historiografie	historiografie	k1gFnSc2	historiografie
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgFnPc1d1	celá
nadcházející	nadcházející	k2eAgFnPc1d1	nadcházející
generace	generace	k1gFnPc1	generace
historiků	historik	k1gMnPc2	historik
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gNnSc3	on
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
pojetí	pojetí	k1gNnSc6	pojetí
musely	muset	k5eAaImAgInP	muset
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
vymezit	vymezit	k5eAaPmF	vymezit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
nebyla	být	k5eNaImAgFnS	být
jen	jen	k9	jen
shlukem	shluk	k1gInSc7	shluk
historických	historický	k2eAgInPc2d1	historický
faktů	fakt	k1gInPc2	fakt
<g/>
,	,	kIx,	,
chronologicky	chronologicky	k6eAd1	chronologicky
seřazených	seřazený	k2eAgNnPc2d1	seřazené
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Palackého	Palacký	k1gMnSc2	Palacký
historie	historie	k1gFnSc2	historie
představovala	představovat	k5eAaImAgFnS	představovat
organický	organický	k2eAgInSc4d1	organický
celek	celek	k1gInSc4	celek
skutečností	skutečnost	k1gFnPc2	skutečnost
protknutý	protknutý	k2eAgInSc1d1	protknutý
hlavní	hlavní	k2eAgFnSc7d1	hlavní
linií	linie	k1gFnSc7	linie
(	(	kIx(	(
<g/>
ideou	idea	k1gFnSc7	idea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
českých	český	k2eAgMnPc2d1	český
historiků	historik	k1gMnPc2	historik
detailně	detailně	k6eAd1	detailně
studoval	studovat	k5eAaImAgMnS	studovat
nejen	nejen	k6eAd1	nejen
politické	politický	k2eAgFnPc4d1	politická
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dějiny	dějiny	k1gFnPc1	dějiny
státoprávní	státoprávní	k2eAgFnPc1d1	státoprávní
(	(	kIx(	(
<g/>
vliv	vliv	k1gInSc1	vliv
polského	polský	k2eAgMnSc2d1	polský
historika	historik	k1gMnSc2	historik
W.	W.	kA	W.
A.	A.	kA	A.
Maciejowskeho	Maciejowske	k1gMnSc2	Maciejowske
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc4d1	sociální
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
vědy	věda	k1gFnPc4	věda
či	či	k8xC	či
dějiny	dějiny	k1gFnPc4	dějiny
každodennosti	každodennost	k1gFnSc2	každodennost
<g/>
.	.	kIx.	.
</s>
<s>
Palacký	Palacký	k1gMnSc1	Palacký
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Hegelovy	Hegelův	k2eAgFnSc2d1	Hegelova
dialektiky	dialektika	k1gFnSc2	dialektika
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c7	za
hlavní	hlavní	k2eAgFnSc7d1	hlavní
linií	linie	k1gFnSc7	linie
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
neustálý	neustálý	k2eAgInSc4d1	neustálý
zápas	zápas	k1gInSc4	zápas
Slovanů	Slovan	k1gMnPc2	Slovan
a	a	k8xC	a
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
dvěma	dva	k4xCgFnPc3	dva
principům	princip	k1gInPc3	princip
také	také	k9	také
přiřadil	přiřadit	k5eAaPmAgMnS	přiřadit
určité	určitý	k2eAgFnPc4d1	určitá
charakteristiky	charakteristika	k1gFnPc4	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ten	ten	k3xDgInSc4	ten
německý	německý	k2eAgInSc4d1	německý
byla	být	k5eAaImAgFnS	být
typická	typický	k2eAgFnSc1d1	typická
podnikavost	podnikavost	k1gFnSc1	podnikavost
<g/>
,	,	kIx,	,
řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
dovednost	dovednost	k1gFnSc1	dovednost
<g/>
,	,	kIx,	,
politická	politický	k2eAgFnSc1d1	politická
organizovanost	organizovanost	k1gFnSc1	organizovanost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
výbojnost	výbojnost	k1gFnSc1	výbojnost
a	a	k8xC	a
náboženská	náboženský	k2eAgFnSc1d1	náboženská
agilnost	agilnost	k1gFnSc1	agilnost
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
Slovany	Slovan	k1gMnPc4	Slovan
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
typická	typický	k2eAgFnSc1d1	typická
rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
mírumilovnost	mírumilovnost	k1gFnSc1	mírumilovnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
anarchii	anarchie	k1gFnSc3	anarchie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
úrovni	úroveň	k1gFnSc6	úroveň
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
polarita	polarita	k1gFnSc1	polarita
dvou	dva	k4xCgFnPc2	dva
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
ovlivňovaly	ovlivňovat	k5eAaImAgFnP	ovlivňovat
a	a	k8xC	a
střetávaly	střetávat	k5eAaImAgFnP	střetávat
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc1	tento
síly	síla	k1gFnPc1	síla
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyjádřeny	vyjádřit	k5eAaPmNgInP	vyjádřit
různě	různě	k6eAd1	různě
<g/>
:	:	kIx,	:
svoboda	svoboda	k1gFnSc1	svoboda
-	-	kIx~	-
autorita	autorita	k1gFnSc1	autorita
<g/>
,	,	kIx,	,
demokracie	demokracie	k1gFnSc1	demokracie
-	-	kIx~	-
feudalismus	feudalismus	k1gInSc1	feudalismus
<g/>
,	,	kIx,	,
věda	věda	k1gFnSc1	věda
-	-	kIx~	-
víra	víra	k1gFnSc1	víra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
boj	boj	k1gInSc1	boj
dvou	dva	k4xCgFnPc2	dva
sil	síla	k1gFnPc2	síla
ať	ať	k8xC	ať
již	již	k6eAd1	již
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
či	či	k8xC	či
světové	světový	k2eAgFnSc6d1	světová
úrovni	úroveň	k1gFnSc6	úroveň
Palacký	Palacký	k1gMnSc1	Palacký
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
neúrodný	úrodný	k2eNgInSc4d1	neúrodný
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
neustálá	neustálý	k2eAgFnSc1d1	neustálá
konfrontace	konfrontace	k1gFnSc1	konfrontace
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
obzor	obzor	k1gInSc4	obzor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
semeništěm	semeniště	k1gNnSc7	semeniště
nových	nový	k2eAgFnPc2d1	nová
myšlenek	myšlenka	k1gFnPc2	myšlenka
či	či	k8xC	či
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
smiřování	smiřování	k1gNnSc3	smiřování
protikladů	protiklad	k1gInPc2	protiklad
<g/>
.	.	kIx.	.
</s>
<s>
Palackého	Palackého	k2eAgInSc4d1	Palackého
výklad	výklad	k1gInSc4	výklad
si	se	k3xPyFc3	se
takto	takto	k6eAd1	takto
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podstatě	podstata	k1gFnSc6	podstata
vznášel	vznášet	k5eAaImAgInS	vznášet
nárok	nárok	k1gInSc4	nárok
i	i	k9	i
na	na	k7c4	na
pedagogické	pedagogický	k2eAgNnSc4d1	pedagogické
a	a	k8xC	a
poučné	poučný	k2eAgNnSc4d1	poučné
poslání	poslání	k1gNnSc4	poslání
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
německým	německý	k2eAgInSc7d1	německý
elementem	element	k1gInSc7	element
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
závazkem	závazek	k1gInSc7	závazek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nesl	nést	k5eAaImAgInS	nést
napříč	napříč	k7c7	napříč
generacemi	generace	k1gFnPc7	generace
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
bezesporu	bezesporu	k9	bezesporu
husitství	husitství	k1gNnPc1	husitství
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
páteř	páteř	k1gFnSc4	páteř
jeho	on	k3xPp3gNnSc2	on
hlavního	hlavní	k2eAgNnSc2d1	hlavní
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
klade	klást	k5eAaImIp3nS	klást
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
myšlenkový	myšlenkový	k2eAgMnSc1d1	myšlenkový
a	a	k8xC	a
sociální	sociální	k2eAgInSc1d1	sociální
obsah	obsah	k1gInSc1	obsah
husitství	husitství	k1gNnSc2	husitství
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
radikálního	radikální	k2eAgNnSc2d1	radikální
křídla	křídlo	k1gNnSc2	křídlo
představovaného	představovaný	k2eAgNnSc2d1	představované
Žižkou	Žižka	k1gMnSc7	Žižka
<g/>
,	,	kIx,	,
Želivským	želivský	k2eAgMnSc7d1	želivský
a	a	k8xC	a
tábority	táborita	k1gMnPc7	táborita
<g/>
.	.	kIx.	.
</s>
<s>
Staví	stavit	k5eAaImIp3nS	stavit
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
určitého	určitý	k2eAgInSc2d1	určitý
kontrastu	kontrast	k1gInSc2	kontrast
proti	proti	k7c3	proti
starší	starý	k2eAgFnSc3d2	starší
osvícenecké	osvícenecký	k2eAgFnSc3d1	osvícenecká
historiografii	historiografie	k1gFnSc3	historiografie
(	(	kIx(	(
<g/>
Jungmann	Jungmann	k1gMnSc1	Jungmann
nebo	nebo	k8xC	nebo
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
husitství	husitství	k1gNnSc3	husitství
většinou	většinou	k6eAd1	většinou
vyhrazovala	vyhrazovat	k5eAaImAgFnS	vyhrazovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
horu	hora	k1gFnSc4	hora
Radhošť	Radhošť	k1gFnSc4	Radhošť
-	-	kIx~	-
báseň	báseň	k1gFnSc4	báseň
Má	mít	k5eAaImIp3nS	mít
modlitba	modlitba	k1gFnSc1	modlitba
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1818	[number]	k4	1818
-	-	kIx~	-
hymnus	hymnus	k1gInSc1	hymnus
Ideál	ideál	k1gInSc1	ideál
říše	říše	k1gFnSc1	říše
-	-	kIx~	-
óda	óda	k1gFnSc1	óda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
Staročeský	staročeský	k2eAgInSc1d1	staročeský
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
kalendář	kalendář	k1gInSc1	kalendář
Život	život	k1gInSc1	život
Jana	Jan	k1gMnSc2	Jan
Amose	Amos	k1gMnSc2	Amos
Komenského	Komenský	k1gMnSc2	Komenský
Zběrky	Zběrka	k1gMnSc2	Zběrka
ze	z	k7c2	z
starožitnosti	starožitnost	k1gFnSc2	starožitnost
českoslovanské	českoslovanský	k2eAgFnSc2d1	českoslovanská
Počátkové	počátkový	k2eAgFnSc2d1	počátkový
českého	český	k2eAgNnSc2d1	české
básnictví	básnictví	k1gNnSc2	básnictví
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
prosodie	prosodie	k1gFnSc1	prosodie
-	-	kIx~	-
vydáno	vydat	k5eAaPmNgNnS	vydat
anonymně	anonymně	k6eAd1	anonymně
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc4	dílo
sepsal	sepsat	k5eAaPmAgInS	sepsat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
P.	P.	kA	P.
J.	J.	kA	J.
Šafaříkem	Šafařík	k1gMnSc7	Šafařík
a	a	k8xC	a
J.	J.	kA	J.
Benediktim	Benediktim	k1gInSc4	Benediktim
(	(	kIx(	(
<g/>
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
dílem	díl	k1gInSc7	díl
začíná	začínat	k5eAaImIp3nS	začínat
roztržka	roztržka	k1gFnSc1	roztržka
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
Jungmanovci	Jungmanovec	k1gMnPc7	Jungmanovec
a	a	k8xC	a
příznivci	příznivec	k1gMnPc1	příznivec
J.	J.	kA	J.
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
Dobrowský	Dobrowský	k1gMnSc1	Dobrowský
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Leben	Leben	k1gInSc1	Leben
und	und	k?	und
gelehrtes	gelehrtes	k1gInSc1	gelehrtes
Wirken	Wirken	k1gInSc1	Wirken
<g/>
.	.	kIx.	.
</s>
<s>
Prag	Prag	k1gInSc1	Prag
:	:	kIx,	:
Haase	Haase	k1gFnSc1	Haase
<g/>
,	,	kIx,	,
1833	[number]	k4	1833
--	--	k?	--
64	[number]	k4	64
s.	s.	k?	s.
Zdigitalizováno	Zdigitalizován	k2eAgNnSc4d1	Zdigitalizován
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
služby	služba	k1gFnSc2	služba
Elektronické	elektronický	k2eAgFnSc2d1	elektronická
knihy	kniha	k1gFnSc2	kniha
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
(	(	kIx(	(
<g/>
EOD	EOD	kA	EOD
<g/>
)	)	kIx)	)
Moravskou	moravský	k2eAgFnSc7d1	Moravská
zemskou	zemský	k2eAgFnSc7d1	zemská
knihovnou	knihovna	k1gFnSc7	knihovna
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Krásověda	Krásověda	k1gFnSc1	Krásověda
čili	čili	k8xC	čili
o	o	k7c6	o
kráse	krása	k1gFnSc6	krása
a	a	k8xC	a
umění	umění	k1gNnSc6	umění
knihy	kniha	k1gFnSc2	kniha
patery	patera	k1gFnSc2	patera
-	-	kIx~	-
dílo	dílo	k1gNnSc4	dílo
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
estetikou	estetika	k1gFnSc7	estetika
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
chtěl	chtít	k5eAaImAgMnS	chtít
napsat	napsat	k5eAaPmF	napsat
pět	pět	k4xCc4	pět
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgInS	napsat
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc1	dva
a	a	k8xC	a
část	část	k1gFnSc1	část
třetí	třetí	k4xOgFnSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Okus	okus	k1gInSc1	okus
české	český	k2eAgFnSc2d1	Česká
terminologie	terminologie	k1gFnSc2	terminologie
filosofické	filosofický	k2eAgNnSc1d1	filosofické
-	-	kIx~	-
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
české	český	k2eAgFnSc2d1	Česká
filosofické	filosofický	k2eAgFnSc2d1	filosofická
terminologie	terminologie	k1gFnSc2	terminologie
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
jím	jíst	k5eAaImIp1nS	jíst
navržená	navržený	k2eAgFnSc1d1	navržená
terminologie	terminologie	k1gFnSc1	terminologie
se	se	k3xPyFc4	se
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
ujala	ujmout	k5eAaPmAgFnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Staří	Starý	k1gMnPc1	Starý
letopisové	letopisový	k2eAgFnSc2d1	letopisový
čeští	český	k2eAgMnPc1d1	český
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1378	[number]	k4	1378
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
Geschichte	Geschicht	k1gInSc5	Geschicht
von	von	k1gInSc4	von
Böhmen	Böhmen	k1gInSc1	Böhmen
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc4	díl
(	(	kIx(	(
<g/>
do	do	k7c2	do
1198	[number]	k4	1198
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
1198	[number]	k4	1198
<g/>
-	-	kIx~	-
<g/>
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
1306	[number]	k4	1306
<g/>
-	-	kIx~	-
<g/>
1378	[number]	k4	1378
<g/>
)	)	kIx)	)
Dějiny	dějiny	k1gFnPc1	dějiny
národu	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
v	v	k7c6	v
Moravě	Morava	k1gFnSc6	Morava
-	-	kIx~	-
vycházelo	vycházet	k5eAaImAgNnS	vycházet
jak	jak	k6eAd1	jak
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
veliké	veliký	k2eAgNnSc1d1	veliké
rozsahem	rozsah	k1gInSc7	rozsah
<g/>
,	,	kIx,	,
Palacký	Palacký	k1gMnSc1	Palacký
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
obraz	obraz	k1gInSc4	obraz
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
pasáže	pasáž	k1gFnPc1	pasáž
(	(	kIx(	(
<g/>
především	především	k9	především
husitství	husitství	k1gNnSc2	husitství
<g/>
)	)	kIx)	)
podléhaly	podléhat	k5eAaImAgFnP	podléhat
rakouské	rakouský	k2eAgFnPc1d1	rakouská
teologické	teologický	k2eAgFnPc1d1	teologická
cenzuře	cenzura	k1gFnSc3	cenzura
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc4	díl
(	(	kIx(	(
<g/>
do	do	k7c2	do
r.	r.	kA	r.
1125	[number]	k4	1125
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
1125	[number]	k4	1125
<g/>
-	-	kIx~	-
<g/>
1403	[number]	k4	1403
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
1403	[number]	k4	1403
<g/>
-	-	kIx~	-
<g/>
1439	[number]	k4	1439
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
1439	[number]	k4	1439
<g/>
-	-	kIx~	-
<g/>
1471	[number]	k4	1471
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
1471	[number]	k4	1471
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
Pomůcky	pomůcka	k1gFnSc2	pomůcka
ku	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
řádů	řád	k1gInPc2	řád
zemských	zemský	k2eAgFnPc2d1	zemská
království	království	k1gNnPc4	království
Českého	český	k2eAgNnSc2d1	české
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
-	-	kIx~	-
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
shrnutí	shrnutí	k1gNnSc4	shrnutí
života	život	k1gInSc2	život
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pomůcky	pomůcka	k1gFnPc1	pomůcka
ku	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
staročeského	staročeský	k2eAgNnSc2d1	staročeské
práva	právo	k1gNnSc2	právo
i	i	k8xC	i
řádu	řád	k1gInSc2	řád
soudního	soudní	k2eAgInSc2d1	soudní
Přehled	přehled	k1gInSc1	přehled
současných	současný	k2eAgMnPc2d1	současný
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
ouředníků	ouředník	k1gMnPc2	ouředník
zemských	zemský	k2eAgFnPc2d1	zemská
i	i	k8xC	i
dvorských	dvorský	k2eAgFnPc2d1	dvorská
v	v	k7c6	v
královstvím	království	k1gNnSc7	království
Českém	český	k2eAgMnSc6d1	český
<g/>
,	,	kIx,	,
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
časův	časův	k2eAgInSc4d1	časův
až	až	k9	až
do	do	k7c2	do
nynějška	nynějšek	k1gInSc2	nynějšek
-	-	kIx~	-
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
i	i	k9	i
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
království	království	k1gNnSc2	království
Českého	český	k2eAgNnSc2d1	české
-	-	kIx~	-
soupis	soupis	k1gInSc4	soupis
všech	všecek	k3xTgFnPc2	všecek
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	s	k7c7	s
základními	základní	k2eAgInPc7d1	základní
údaji	údaj	k1gInPc7	údaj
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
data	datum	k1gNnPc1	datum
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stručné	stručný	k2eAgFnPc1d1	stručná
dějiny	dějiny	k1gFnPc1	dějiny
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
Skizze	Skizze	k1gFnSc1	Skizze
einer	einra	k1gFnPc2	einra
Geschichte	Geschicht	k1gInSc5	Geschicht
von	von	k1gInSc4	von
Prag	Prag	k1gInSc1	Prag
<g/>
)	)	kIx)	)
Idea	idea	k1gFnSc1	idea
státu	stát	k1gInSc2	stát
rakouského	rakouský	k2eAgInSc2d1	rakouský
-	-	kIx~	-
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
popis	popis	k1gInSc4	popis
jeho	jeho	k3xOp3gFnPc2	jeho
představ	představa	k1gFnPc2	představa
o	o	k7c6	o
federálním	federální	k2eAgNnSc6d1	federální
uspořádání	uspořádání	k1gNnSc6	uspořádání
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
přeje	přát	k5eAaImIp3nS	přát
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gNnSc4	jeho
zachování	zachování	k1gNnSc4	zachování
<g/>
.	.	kIx.	.
</s>
<s>
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
korrespondence	korrespondence	k1gFnSc1	korrespondence
a	a	k8xC	a
zápisky	zápisek	k1gInPc1	zápisek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1863	[number]	k4	1863
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
korespondence	korespondence	k1gFnSc2	korespondence
s	s	k7c7	s
významnými	významný	k2eAgFnPc7d1	významná
osobnostmi	osobnost	k1gFnPc7	osobnost
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
novinových	novinový	k2eAgInPc2d1	novinový
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
pomníky	pomník	k1gInPc1	pomník
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Palackého	Palackého	k2eAgNnSc1d1	Palackého
náměstí	náměstí	k1gNnSc1	náměstí
Stanislav	Stanislav	k1gMnSc1	Stanislav
Sucharda	Sucharda	k1gMnSc1	Sucharda
1902	[number]	k4	1902
<g/>
;	;	kIx,	;
Panteon	panteon	k1gInSc4	panteon
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
Písek	Písek	k1gInSc1	Písek
Ladislav	Ladislav	k1gMnSc1	Ladislav
Wurzel	Wurzel	k1gMnSc1	Wurzel
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hodslavice	Hodslavice	k1gInPc1	Hodslavice
<g/>
,	,	kIx,	,
Čelechovice	Čelechovice	k1gFnPc1	Čelechovice
-	-	kIx~	-
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Velkého	velký	k2eAgInSc2d1	velký
Kosíře	kosíř	k1gInSc2	kosíř
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kozí	kozí	k2eAgFnSc1d1	kozí
hora	hora	k1gFnSc1	hora
Brno-Žabovřesky	Brno-Žabovřeska	k1gFnSc2	Brno-Žabovřeska
<g/>
,	,	kIx,	,
busty	busta	k1gFnSc2	busta
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
Palackého	Palackého	k2eAgFnSc1d1	Palackého
7	[number]	k4	7
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Popp	Popp	k1gMnSc1	Popp
1898	[number]	k4	1898
<g/>
;	;	kIx,	;
Tomáš	Tomáš	k1gMnSc1	Tomáš
Seidan	Seidan	k1gMnSc1	Seidan
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Wielgus	Wielgus	k1gMnSc1	Wielgus
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtná	posmrtný	k2eAgFnSc1d1	posmrtná
maska	maska	k1gFnSc1	maska
a	a	k8xC	a
odlitek	odlitek	k1gInSc1	odlitek
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
<g/>
;	;	kIx,	;
1876	[number]	k4	1876
plakety	plaketa	k1gFnSc2	plaketa
<g/>
:	:	kIx,	:
Stanislav	Stanislav	k1gMnSc1	Stanislav
Sucharda	Sucharda	k1gMnSc1	Sucharda
1912	[number]	k4	1912
tříčtvrteční	tříčtvrteční	k2eAgFnSc1d1	tříčtvrteční
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
olejomalba	olejomalba	k1gFnSc1	olejomalba
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hellich	Hellich	k1gMnSc1	Hellich
<g/>
,	,	kIx,	,
1846	[number]	k4	1846
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
poloviční	poloviční	k2eAgFnSc1d1	poloviční
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
olejomalba	olejomalba	k1gFnSc1	olejomalba
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
Agathon	Agathon	k1gMnSc1	Agathon
Klemt	Klemt	k1gMnSc1	Klemt
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
svatební	svatební	k2eAgFnSc1d1	svatební
portrétní	portrétní	k2eAgFnSc1d1	portrétní
miniatura	miniatura	k1gFnSc1	miniatura
<g/>
,	,	kIx,	,
kvaš	kvaš	k1gFnSc1	kvaš
na	na	k7c6	na
slonovině	slonovina	k1gFnSc6	slonovina
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Schödl	Schödl	k1gMnSc1	Schödl
<g/>
,	,	kIx,	,
1827	[number]	k4	1827
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
litografie	litografie	k1gFnPc1	litografie
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Dauthage	Dauthag	k1gFnSc2	Dauthag
<g/>
,	,	kIx,	,
1855	[number]	k4	1855
Tříčtvrteční	tříčtvrteční	k2eAgFnSc1d1	tříčtvrteční
postava	postava	k1gFnSc1	postava
v	v	k7c6	v
klobouku	klobouk	k1gInSc6	klobouk
<g/>
,	,	kIx,	,
litografie	litografie	k1gFnSc1	litografie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Švabinský	Švabinský	k2eAgMnSc1d1	Švabinský
<g/>
,	,	kIx,	,
1835	[number]	k4	1835
četné	četný	k2eAgFnPc1d1	četná
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ateliér	ateliér	k1gInSc1	ateliér
Angerer	Angerer	k1gInSc1	Angerer
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
medaile	medaile	k1gFnSc1	medaile
<g/>
:	:	kIx,	:
F.	F.	kA	F.
<g/>
Seidan	Seidan	k1gInSc4	Seidan
1861	[number]	k4	1861
<g/>
;	;	kIx,	;
1876	[number]	k4	1876
<g/>
;	;	kIx,	;
Čermákové	Čermákové	k2eAgFnPc4d1	Čermákové
Hořovice	Hořovice	k1gFnPc4	Hořovice
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
Pichl	Pichl	k1gFnSc1	Pichl
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Popp	Popp	k1gInSc4	Popp
1898	[number]	k4	1898
<g/>
;	;	kIx,	;
B.	B.	kA	B.
<g/>
Vlček	vlček	k1gInSc4	vlček
1898	[number]	k4	1898
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
V.	V.	kA	V.
Šmakal	šmakat	k5eAaImAgMnS	šmakat
(	(	kIx(	(
<g/>
několik	několik	k4yIc4	několik
pro	pro	k7c4	pro
různá	různý	k2eAgNnPc4d1	různé
města	město	k1gNnPc4	město
<g/>
)	)	kIx)	)
1898-	[number]	k4	1898-
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Babka	babka	k1gFnSc1	babka
1948	[number]	k4	1948
medaile-	medaile-	k?	medaile-
insignie	insignie	k1gFnPc1	insignie
pro	pro	k7c4	pro
řetězy	řetěz	k1gInPc4	řetěz
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palackého	k2eAgFnSc2d1	Palackého
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
Ot	ot	k1gMnSc1	ot
<g/>
.	.	kIx.	.
<g/>
Španiel	Španiel	k1gMnSc1	Španiel
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Benda	Benda	k1gMnSc1	Benda
<g/>
)	)	kIx)	)
1946	[number]	k4	1946
tisícikorunová	tisícikorunový	k2eAgFnSc1d1	tisícikorunová
česká	český	k2eAgFnSc1d1	Česká
bankovka	bankovka	k1gFnSc1	bankovka
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kulhánek	Kulhánek	k1gMnSc1	Kulhánek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g />
.	.	kIx.	.
</s>
<s>
200	[number]	k4	200
korunová	korunový	k2eAgFnSc1d1	korunová
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
mince	mince	k1gFnSc1	mince
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Oppl	Oppl	k1gMnSc1	Oppl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
poštovní	poštovní	k2eAgFnSc1d1	poštovní
známka	známka	k1gFnSc1	známka
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
1998	[number]	k4	1998
Byt	byt	k1gInSc1	byt
rodin	rodina	k1gFnPc2	rodina
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
a	a	k8xC	a
Františka	František	k1gMnSc2	František
Ladislava	Ladislav	k1gMnSc2	Ladislav
Riegera	Rieger	k1gMnSc2	Rieger
v	v	k7c6	v
Palackého	Palackého	k2eAgFnSc6d1	Palackého
ulici	ulice	k1gFnSc6	ulice
č.	č.	k?	č.
7	[number]	k4	7
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
byl	být	k5eAaImAgInS	být
zásluhou	zásluha	k1gFnSc7	zásluha
Palackého	Palackého	k2eAgFnSc2d1	Palackého
vnučky	vnučka	k1gFnSc2	vnučka
Libuše	Libuše	k1gFnSc2	Libuše
Bráfové	Bráfový	k2eAgFnSc2d1	Bráfová
proměněn	proměněn	k2eAgInSc4d1	proměněn
v	v	k7c4	v
Památník	památník	k1gInSc4	památník
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
zčásti	zčásti	k6eAd1	zčásti
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
veřejnosti	veřejnost	k1gFnSc3	veřejnost
k	k	k7c3	k
občasným	občasný	k2eAgFnPc3d1	občasná
prohlídkám	prohlídka	k1gFnPc3	prohlídka
<g/>
.	.	kIx.	.
</s>
<s>
Dědicové	dědic	k1gMnPc1	dědic
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
Riegerů	Rieger	k1gMnPc2	Rieger
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
(	(	kIx(	(
<g/>
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
zkonfiskování	zkonfiskování	k1gNnSc2	zkonfiskování
nemovitosti	nemovitost	k1gFnSc2	nemovitost
čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
<g/>
)	)	kIx)	)
darovali	darovat	k5eAaPmAgMnP	darovat
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
zařízení	zařízení	k1gNnSc4	zařízení
bytu	byt	k1gInSc2	byt
včetně	včetně	k7c2	včetně
většiny	většina	k1gFnSc2	většina
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
soch	socha	k1gFnPc2	socha
Národnímu	národní	k2eAgNnSc3d1	národní
muzeu	muzeum	k1gNnSc3	muzeum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zde	zde	k6eAd1	zde
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
provozuje	provozovat	k5eAaImIp3nS	provozovat
celou	celý	k2eAgFnSc4d1	celá
expozici	expozice	k1gFnSc4	expozice
<g/>
,	,	kIx,	,
renovovanou	renovovaný	k2eAgFnSc4d1	renovovaná
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
pod	pod	k7c7	pod
okny	okno	k1gNnPc7	okno
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dalšími	další	k1gNnPc7	další
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
dvaasedmdesát	dvaasedmdesát	k4xCc4	dvaasedmdesát
jmen	jméno	k1gNnPc2	jméno
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Palackého	Palacký	k1gMnSc4	Palacký
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
upomíná	upomínat	k5eAaImIp3nS	upomínat
jednak	jednak	k8xC	jednak
památník	památník	k1gInSc1	památník
v	v	k7c6	v
rodných	rodný	k2eAgInPc6d1	rodný
Hodslavicích	Hodslavice	k1gInPc6	Hodslavice
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
také	také	k9	také
expozice	expozice	k1gFnSc1	expozice
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Kunín	Kunína	k1gFnPc2	Kunína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Palacký	Palacký	k1gMnSc1	Palacký
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1807	[number]	k4	1807
a	a	k8xC	a
1809	[number]	k4	1809
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
.	.	kIx.	.
</s>
