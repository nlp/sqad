<s>
Ewa	Ewa	k?	Ewa
Farna	Farna	k1gFnSc1	Farna
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1993	[number]	k4	1993
Třinec	Třinec	k1gInSc1	Třinec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
polské	polský	k2eAgFnSc2d1	polská
národnosti	národnost	k1gFnSc2	národnost
se	s	k7c7	s
státním	státní	k2eAgNnSc7d1	státní
občanstvím	občanství	k1gNnSc7	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
populární	populární	k2eAgFnSc2d1	populární
a	a	k8xC	a
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
zpívá	zpívat	k5eAaImIp3nS	zpívat
česky	česky	k6eAd1	česky
a	a	k8xC	a
polsky	polsky	k6eAd1	polsky
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
i	i	k8xC	i
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Vendryni	Vendryně	k1gFnSc6	Vendryně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
získala	získat	k5eAaPmAgFnS	získat
řadu	řada	k1gFnSc4	řada
českých	český	k2eAgFnPc2d1	Česká
i	i	k8xC	i
polských	polský	k2eAgNnPc2d1	polské
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Ewa	Ewa	k?	Ewa
Farna	Farna	k1gFnSc1	Farna
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
vendryňskou	vendryňský	k2eAgFnSc4d1	vendryňský
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
vyučovacím	vyučovací	k2eAgInSc6d1	vyučovací
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
ukončila	ukončit	k5eAaPmAgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
polském	polský	k2eAgNnSc6d1	polské
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Těšíně	Těšín	k1gInSc6	Těšín
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
k	k	k7c3	k
vysokoškolskému	vysokoškolský	k2eAgNnSc3d1	vysokoškolské
studiu	studio	k1gNnSc3	studio
práv	právo	k1gNnPc2	právo
do	do	k7c2	do
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k8xC	ale
po	po	k7c6	po
dokončeném	dokončený	k2eAgNnSc6d1	dokončené
prvním	první	k4xOgNnSc6	první
roce	rok	k1gInSc6	rok
studium	studium	k1gNnSc4	studium
ukončila	ukončit	k5eAaPmAgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
,	,	kIx,	,
bratra	bratr	k1gMnSc4	bratr
Adama	Adam	k1gMnSc4	Adam
(	(	kIx(	(
<g/>
*	*	kIx~	*
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
a	a	k8xC	a
sestru	sestra	k1gFnSc4	sestra
Magdalénu	Magdaléna	k1gFnSc4	Magdaléna
(	(	kIx(	(
<g/>
*	*	kIx~	*
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
po	po	k7c4	po
6	[number]	k4	6
<g/>
.	.	kIx.	.
hodině	hodina	k1gFnSc6	hodina
ráno	ráno	k6eAd1	ráno
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
mezi	mezi	k7c7	mezi
Třincem	Třinec	k1gInSc7	Třinec
a	a	k8xC	a
Vendryní	Vendryně	k1gFnSc7	Vendryně
nezvládla	zvládnout	k5eNaPmAgFnS	zvládnout
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
havarovala	havarovat	k5eAaPmAgFnS	havarovat
<g/>
.	.	kIx.	.
</s>
<s>
Nehoda	nehoda	k1gFnSc1	nehoda
se	se	k3xPyFc4	se
obešla	obejít	k5eAaPmAgFnS	obejít
bez	bez	k7c2	bez
vážnějších	vážní	k2eAgNnPc2d2	vážnější
zranění	zranění	k1gNnPc2	zranění
<g/>
,	,	kIx,	,
vůz	vůz	k1gInSc1	vůz
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
zcela	zcela	k6eAd1	zcela
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Přivolaná	přivolaný	k2eAgFnSc1d1	přivolaná
policie	policie	k1gFnSc1	policie
naměřila	naměřit	k5eAaBmAgFnS	naměřit
zpěvačce	zpěvačka	k1gFnSc3	zpěvačka
v	v	k7c6	v
dechu	dech	k1gInSc6	dech
alkohol	alkohol	k1gInSc1	alkohol
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
promile	promile	k1gNnSc2	promile
(	(	kIx(	(
<g/>
0,8	[number]	k4	0,8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
bylo	být	k5eAaImAgNnS	být
usnutí	usnutí	k1gNnSc1	usnutí
za	za	k7c7	za
volantem	volant	k1gInSc7	volant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
jí	on	k3xPp3gFnSc3	on
byl	být	k5eAaImAgMnS	být
odebrán	odebrán	k2eAgInSc4d1	odebrán
řidičský	řidičský	k2eAgInSc4d1	řidičský
průkaz	průkaz	k1gInSc4	průkaz
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
po	po	k7c6	po
patnácti	patnáct	k4xCc6	patnáct
měsících	měsíc	k1gInPc6	měsíc
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Ewu	Ewu	k?	Ewu
Farnu	Farna	k1gMnSc4	Farna
objevil	objevit	k5eAaPmAgMnS	objevit
producent	producent	k1gMnSc1	producent
Lešek	Lešek	k1gMnSc1	Lešek
Wronka	Wronka	k1gMnSc1	Wronka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
13	[number]	k4	13
letech	let	k1gInPc6	let
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
singl	singl	k1gInSc4	singl
Měls	Mělsa	k1gFnPc2	Mělsa
mě	já	k3xPp1nSc2	já
vůbec	vůbec	k9	vůbec
rád	rád	k6eAd1	rád
na	na	k7c4	na
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
koncert	koncert	k1gInSc4	koncert
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Třinci	Třinec	k1gInSc6	Třinec
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
ocenění	ocenění	k1gNnSc4	ocenění
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
Slavík	slavík	k1gInSc1	slavík
Mattoni	Mattoň	k1gFnSc3	Mattoň
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vydala	vydat	k5eAaPmAgFnS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
českou	český	k2eAgFnSc4d1	Česká
desku	deska	k1gFnSc4	deska
Ticho	ticho	k6eAd1	ticho
a	a	k8xC	a
také	také	k9	také
debutovou	debutový	k2eAgFnSc4d1	debutová
polskou	polský	k2eAgFnSc4d1	polská
desku	deska	k1gFnSc4	deska
Sam	Sam	k1gMnSc1	Sam
na	na	k7c6	na
sam	sam	k?	sam
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
Objevem	objev	k1gInSc7	objev
roku	rok	k1gInSc2	rok
Hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
Óčka	Óčk	k1gInSc2	Óčk
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
za	za	k7c4	za
obě	dva	k4xCgNnPc4	dva
česká	český	k2eAgNnPc4d1	české
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
Slavík	slavík	k1gInSc1	slavík
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
turné	turné	k1gNnSc4	turné
Blíž	blízce	k6eAd2	blízce
ke	k	k7c3	k
hvězdám	hvězda	k1gFnPc3	hvězda
čítající	čítající	k2eAgFnSc2d1	čítající
18	[number]	k4	18
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
14	[number]	k4	14
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
vůbec	vůbec	k9	vůbec
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podobné	podobný	k2eAgNnSc4d1	podobné
turné	turné	k1gNnSc4	turné
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
živé	živá	k1gFnPc1	živá
DVD	DVD	kA	DVD
&	&	k?	&
CD	CD	kA	CD
bylo	být	k5eAaImAgNnS	být
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kubou	Kuba	k1gMnSc7	Kuba
Molędou	Molęda	k1gMnSc7	Molęda
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
oficiální	oficiální	k2eAgInSc4d1	oficiální
soundtrack	soundtrack	k1gInSc4	soundtrack
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
jazyce	jazyk	k1gInSc6	jazyk
k	k	k7c3	k
filmu	film	k1gInSc3	film
Camp	camp	k1gInSc1	camp
Rock	rock	k1gInSc1	rock
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Oto	Oto	k1gMnSc1	Oto
ja	ja	k?	ja
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
soundtrack	soundtrack	k1gInSc4	soundtrack
k	k	k7c3	k
seriálu	seriál	k1gInSc2	seriál
Ošklivka	Ošklivka	k1gFnSc1	Ošklivka
Katka	Katka	k1gFnSc1	Katka
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Jaký	jaký	k9	jaký
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hostovala	hostovat	k5eAaImAgFnS	hostovat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
slovenské	slovenský	k2eAgFnSc2d1	slovenská
skupiny	skupina	k1gFnSc2	skupina
No	no	k9	no
Name	Nam	k1gFnPc1	Nam
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc4	rok
završila	završit	k5eAaPmAgFnS	završit
výhrou	výhra	k1gFnSc7	výhra
třetího	třetí	k4xOgNnSc2	třetí
místa	místo	k1gNnPc1	místo
Českého	český	k2eAgInSc2d1	český
slavíka	slavík	k1gInSc2	slavík
2008	[number]	k4	2008
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
držitelkou	držitelka	k1gFnSc7	držitelka
tohoto	tento	k3xDgNnSc2	tento
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
vyšla	vyjít	k5eAaPmAgFnS	vyjít
její	její	k3xOp3gFnSc1	její
druhá	druhý	k4xOgFnSc1	druhý
polská	polský	k2eAgFnSc1d1	polská
deska	deska	k1gFnSc1	deska
Cicho	Cic	k1gMnSc2	Cic
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
česká	český	k2eAgFnSc1d1	Česká
deska	deska	k1gFnSc1	deska
Virtuální	virtuální	k2eAgFnSc1d1	virtuální
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
této	tento	k3xDgFnSc2	tento
desky	deska	k1gFnSc2	deska
procestovala	procestovat	k5eAaPmAgFnS	procestovat
republiku	republika	k1gFnSc4	republika
se	s	k7c7	s
stejnojmenným	stejnojmenný	k2eAgNnSc7d1	stejnojmenné
turné	turné	k1gNnSc7	turné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
obhájila	obhájit	k5eAaPmAgFnS	obhájit
post	post	k1gInSc4	post
bronzové	bronzový	k2eAgFnSc2d1	bronzová
slavice	slavice	k1gFnSc2	slavice
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
také	také	k9	také
několik	několik	k4yIc4	několik
polských	polský	k2eAgFnPc2d1	polská
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
cenu	cena	k1gFnSc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
českou	český	k2eAgFnSc4d1	Česká
(	(	kIx(	(
<g/>
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Bendigem	Bendig	k1gMnSc7	Bendig
<g/>
)	)	kIx)	)
a	a	k8xC	a
polskou	polský	k2eAgFnSc4d1	polská
(	(	kIx(	(
<g/>
s	s	k7c7	s
Kubou	Kuba	k1gFnSc7	Kuba
Molędou	Molęda	k1gFnSc7	Molęda
<g/>
)	)	kIx)	)
verzi	verze	k1gFnSc4	verze
oficiálního	oficiální	k2eAgInSc2d1	oficiální
soundtracku	soundtrack	k1gInSc2	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc2	film
Camp	camp	k1gInSc4	camp
Rock	rock	k1gInSc1	rock
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vyšla	vyjít	k5eAaPmAgFnS	vyjít
jí	jíst	k5eAaImIp3nS	jíst
třetí	třetí	k4xOgFnSc1	třetí
polská	polský	k2eAgFnSc1d1	polská
deska	deska	k1gFnSc1	deska
Ewakuacja	Ewakuacja	k1gFnSc1	Ewakuacja
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
hostovala	hostovat	k5eAaImAgFnS	hostovat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
skupiny	skupina	k1gFnSc2	skupina
Čechomor	Čechomora	k1gFnPc2	Čechomora
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
obhájila	obhájit	k5eAaPmAgFnS	obhájit
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
dostalo	dostat	k5eAaPmAgNnS	dostat
ocenění	ocenění	k1gNnSc4	ocenění
i	i	k9	i
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
také	také	k9	také
moderátorkou	moderátorka	k1gFnSc7	moderátorka
cen	cena	k1gFnPc2	cena
Anděl	Anděla	k1gFnPc2	Anděla
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnSc7	její
spolumoderátorem	spolumoderátor	k1gMnSc7	spolumoderátor
byl	být	k5eAaImAgMnS	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Klus	klus	k1gInSc4	klus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
partnerem	partner	k1gMnSc7	partner
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
složila	složit	k5eAaPmAgFnS	složit
maturitní	maturitní	k2eAgFnSc4d1	maturitní
zkoušku	zkouška	k1gFnSc4	zkouška
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Těšíně	Těšín	k1gInSc6	Těšín
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
nato	nato	k6eAd1	nato
měla	mít	k5eAaImAgFnS	mít
autonehodu	autonehoda	k1gFnSc4	autonehoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Pavlom	Pavlom	k1gInSc4	Pavlom
Haberou	Habera	k1gFnSc7	Habera
a	a	k8xC	a
Ondřejem	Ondřej	k1gMnSc7	Ondřej
Soukupem	Soukup	k1gMnSc7	Soukup
vybrána	vybrat	k5eAaPmNgFnS	vybrat
jako	jako	k8xS	jako
porotkyně	porotkyně	k1gFnSc1	porotkyně
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc4	Česko
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Superstar	superstar	k1gFnSc1	superstar
<g/>
,	,	kIx,	,
castingy	casting	k1gInPc1	casting
začaly	začít	k5eAaPmAgInP	začít
již	již	k6eAd1	již
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
navštívila	navštívit	k5eAaPmAgFnS	navštívit
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
českým	český	k2eAgMnPc3d1	český
starousedlíkům	starousedlík	k1gMnPc3	starousedlík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vydává	vydávat	k5eAaPmIp3nS	vydávat
své	svůj	k3xOyFgFnSc2	svůj
polské	polský	k2eAgFnSc2d1	polská
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
řadové	řadový	k2eAgNnSc1d1	řadové
album	album	k1gNnSc1	album
a	a	k8xC	a
desáté	desátá	k1gFnPc1	desátá
celkové	celkový	k2eAgFnPc1d1	celková
s	s	k7c7	s
názvem	název	k1gInSc7	název
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
inna	inna	k1gMnSc1	inna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
té	ten	k3xDgFnSc3	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vypustila	vypustit	k5eAaPmAgFnS	vypustit
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
Znak	znak	k1gInSc4	znak
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Ulubiona	Ulubiona	k1gFnSc1	Ulubiona
rzecz	rzecz	k1gMnSc1	rzecz
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInSc4	první
"	"	kIx"	"
<g/>
dospělé	dospělý	k2eAgInPc4d1	dospělý
<g/>
"	"	kIx"	"
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
autorsky	autorsky	k6eAd1	autorsky
více	hodně	k6eAd2	hodně
než	než	k8xS	než
na	na	k7c6	na
předchozích	předchozí	k2eAgFnPc6d1	předchozí
deskách	deska	k1gFnPc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
ukončila	ukončit	k5eAaPmAgFnS	ukončit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
varšavských	varšavský	k2eAgNnPc6d1	Varšavské
právech	právo	k1gNnPc6	právo
kvůli	kvůli	k7c3	kvůli
časové	časový	k2eAgFnSc3d1	časová
tísni	tíseň	k1gFnSc3	tíseň
a	a	k8xC	a
práci	práce	k1gFnSc3	práce
<g/>
,	,	kIx,	,
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
problémům	problém	k1gInPc3	problém
a	a	k8xC	a
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Českém	český	k2eAgInSc6d1	český
slavíku	slavík	k1gInSc6	slavík
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spadla	spadnout	k5eAaPmAgFnS	spadnout
ze	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
na	na	k7c4	na
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
oblíbenosti	oblíbenost	k1gFnSc6	oblíbenost
<g/>
,	,	kIx,	,
představila	představit	k5eAaPmAgFnS	představit
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
věc	věc	k1gFnSc1	věc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ulubiona	Ulubiona	k1gFnSc1	Ulubiona
rzecz	rzecz	k1gMnSc1	rzecz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
irském	irský	k2eAgInSc6d1	irský
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
vypustila	vypustit	k5eAaPmAgFnS	vypustit
do	do	k7c2	do
éteru	éter	k1gInSc2	éter
první	první	k4xOgFnSc4	první
vlaštovku	vlaštovka	k1gFnSc4	vlaštovka
z	z	k7c2	z
nového	nový	k2eAgInSc2d1	nový
CD	CD	kA	CD
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Leporelo	leporelo	k1gNnSc1	leporelo
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předzvěstí	předzvěst	k1gFnSc7	předzvěst
nástupce	nástupce	k1gMnSc2	nástupce
pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
starého	starý	k2eAgNnSc2d1	staré
alba	album	k1gNnSc2	album
Virtuální	virtuální	k2eAgFnSc1d1	virtuální
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2014	[number]	k4	2014
představila	představit	k5eAaPmAgFnS	představit
již	již	k6eAd1	již
třetí	třetí	k4xOgInSc4	třetí
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Tajna	Tajna	k?	Tajna
misja	misja	k1gFnSc1	misja
<g/>
"	"	kIx"	"
z	z	k7c2	z
polské	polský	k2eAgFnSc2d1	polská
desky	deska	k1gFnSc2	deska
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
INNA	INNA	kA	INNA
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
akustický	akustický	k2eAgInSc4d1	akustický
koncert	koncert	k1gInSc4	koncert
na	na	k7c4	na
Óčko	Óčko	k1gNnSc4	Óčko
Music	Musice	k1gInPc2	Musice
Stage	Stage	k1gNnPc2	Stage
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
byl	být	k5eAaImAgInS	být
pořízen	pořízen	k2eAgInSc1d1	pořízen
záznam	záznam	k1gInSc1	záznam
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
vyjít	vyjít	k5eAaPmF	vyjít
na	na	k7c6	na
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
jako	jako	k8xS	jako
reprezentant	reprezentant	k1gMnSc1	reprezentant
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
nahrávání	nahrávání	k1gNnSc2	nahrávání
hymny	hymna	k1gFnSc2	hymna
pro	pro	k7c4	pro
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
umělci	umělec	k1gMnPc7	umělec
z	z	k7c2	z
několika	několik	k4yIc2	několik
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
deska	deska	k1gFnSc1	deska
Leporelo	leporelo	k1gNnSc1	leporelo
vyšla	vyjít	k5eAaPmAgFnS	vyjít
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přípravách	příprava	k1gFnPc6	příprava
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
jazzová	jazzový	k2eAgFnSc1d1	jazzová
verze	verze	k1gFnSc1	verze
desky	deska	k1gFnSc2	deska
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
INNA	INNA	kA	INNA
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
měla	mít	k5eAaImAgFnS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
televizních	televizní	k2eAgNnPc2d1	televizní
vystoupení	vystoupení	k1gNnSc2	vystoupení
<g/>
,	,	kIx,	,
odehrála	odehrát	k5eAaPmAgFnS	odehrát
přes	přes	k7c4	přes
pět	pět	k4xCc4	pět
set	set	k1gInSc4	set
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
čtyři	čtyři	k4xCgNnPc4	čtyři
samostatná	samostatný	k2eAgNnPc4d1	samostatné
turné	turné	k1gNnPc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
také	také	k9	také
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Mám	mít	k5eAaImIp1nS	mít
boky	boka	k1gFnPc4	boka
jako	jako	k8xS	jako
skříň	skříň	k1gFnSc4	skříň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ewa	Ewa	k?	Ewa
Farna	Farna	k1gFnSc1	Farna
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
Roman	Roman	k1gMnSc1	Roman
Vícha	Vích	k1gMnSc2	Vích
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
Tomáš	Tomáš	k1gMnSc1	Tomáš
Lacina	Lacina	k1gMnSc1	Lacina
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
Martin	Martin	k2eAgInSc1d1	Martin
Chobot	chobot	k1gInSc1	chobot
(	(	kIx(	(
<g/>
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Steinsdörfer	Steinsdörfer	k1gMnSc1	Steinsdörfer
(	(	kIx(	(
<g/>
kapelník	kapelník	k1gMnSc1	kapelník
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
)	)	kIx)	)
Lukáš	Lukáš	k1gMnSc1	Lukáš
Chromek	Chromek	k1gMnSc1	Chromek
(	(	kIx(	(
<g/>
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
S	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
mluví	mluvit	k5eAaImIp3nS	mluvit
těšínským	těšínský	k2eAgNnSc7d1	Těšínské
nářečím	nářečí	k1gNnSc7	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
veřejně	veřejně	k6eAd1	veřejně
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
k	k	k7c3	k
evangelickému	evangelický	k2eAgNnSc3d1	evangelické
vyznání	vyznání	k1gNnSc3	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
zpíváním	zpívání	k1gNnSc7	zpívání
na	na	k7c6	na
celostátní	celostátní	k2eAgFnSc6d1	celostátní
úrovni	úroveň	k1gFnSc6	úroveň
začínala	začínat	k5eAaImAgFnS	začínat
<g/>
,	,	kIx,	,
nosila	nosit	k5eAaImAgFnS	nosit
rovnátka	rovnátko	k1gNnSc2	rovnátko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
ankety	anketa	k1gFnSc2	anketa
Zlatý	zlatý	k2eAgInSc1d1	zlatý
Slavík	slavík	k1gInSc1	slavík
2008	[number]	k4	2008
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
duet	duet	k1gInSc4	duet
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
No	no	k9	no
Name	Name	k1gFnSc1	Name
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Prvá	prvý	k4xOgFnSc1	prvý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
tváří	tvář	k1gFnSc7	tvář
kampaně	kampaň	k1gFnSc2	kampaň
zvané	zvaný	k2eAgFnSc2d1	zvaná
Sázka	sázka	k1gFnSc1	sázka
na	na	k7c4	na
polskost	polskost	k1gFnSc4	polskost
(	(	kIx(	(
<g/>
Postaw	Postaw	k1gFnSc4	Postaw
na	na	k7c4	na
polskość	polskość	k?	polskość
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nejmladší	mladý	k2eAgFnSc4d3	nejmladší
porotkyni	porotkyně	k1gFnSc4	porotkyně
Superstar	superstar	k1gFnSc2	superstar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
autorské	autorský	k2eAgNnSc4d1	autorské
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
hraje	hrát	k5eAaImIp3nS	hrát
člen	člen	k1gMnSc1	člen
kapely	kapela	k1gFnSc2	kapela
Toxique	Toxiqu	k1gFnSc2	Toxiqu
Roman	Roman	k1gMnSc1	Roman
Vícha	Vích	k1gMnSc2	Vích
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
kapelu	kapela	k1gFnSc4	kapela
opustil	opustit	k5eAaPmAgInS	opustit
bubeník	bubeník	k1gMnSc1	bubeník
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Lukáš	Lukáš	k1gMnSc1	Lukáš
Pavlík	Pavlík	k1gMnSc1	Pavlík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
desce	deska	k1gFnSc6	deska
Leporelo	leporelo	k1gNnSc1	leporelo
je	být	k5eAaImIp3nS	být
song	song	k1gInSc4	song
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
producentovi	producent	k1gMnSc3	producent
Leškovi	Lešek	k1gMnSc3	Lešek
Wronkovi	Wronek	k1gMnSc3	Wronek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
kytarista	kytarista	k1gMnSc1	kytarista
Tomáš	Tomáš	k1gMnSc1	Tomáš
Fuchs	Fuchs	k1gMnSc1	Fuchs
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
léta	léto	k1gNnSc2	léto
2013	[number]	k4	2013
udržuje	udržovat	k5eAaImIp3nS	udržovat
partnerský	partnerský	k2eAgInSc4d1	partnerský
vztah	vztah	k1gInSc4	vztah
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
kytaristou	kytarista	k1gMnSc7	kytarista
Martinem	Martin	k1gMnSc7	Martin
Chobotem	chobot	k1gInSc7	chobot
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zazpívej	zazpívat	k5eAaPmRp2nS	zazpívat
si	se	k3xPyFc3	se
na	na	k7c4	na
Ewa	Ewa	k1gFnSc4	Ewa
Tour	Tour	k1gInSc1	Tour
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
Bud	bouda	k1gFnPc2	bouda
virtuální	virtuální	k2eAgFnSc1d1	virtuální
na	na	k7c4	na
Ewa	Ewa	k1gFnSc4	Ewa
Tour	Tour	k1gInSc1	Tour
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
EWAkuacja	EWAkuacja	k1gFnSc1	EWAkuacja
tour	tour	k1gInSc1	tour
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
Čechomor	Čechomor	k1gMnSc1	Čechomor
tour	tour	k1gMnSc1	tour
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
Narozeninové	narozeninový	k2eAgInPc1d1	narozeninový
koncerty	koncert	k1gInPc1	koncert
18	[number]	k4	18
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
Hrady	hrad	k1gInPc1	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
INNA	INNA	kA	INNA
<g/>
?	?	kIx.	?
</s>
<s>
Tour	Tour	k1gInSc1	Tour
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
V	v	k7c6	v
AKUSTICKÉM	akustický	k2eAgInSc6d1	akustický
SMĚRU	směr	k1gInSc6	směr
2015	[number]	k4	2015
On	on	k3xPp3gMnSc1	on
Stage	Stage	k1gFnSc7	Stage
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
Nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
Ewa	Ewa	k1gFnSc1	Ewa
Farna	Farna	k1gFnSc1	Farna
a	a	k8xC	a
Support	support	k1gInSc1	support
Lesbiens	Lesbiensa	k1gFnPc2	Lesbiensa
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
-	-	kIx~	-
okresní	okresní	k2eAgFnSc1d1	okresní
soutěž	soutěž	k1gFnSc1	soutěž
ve	v	k7c6	v
Frýdku-Místku	Frýdku-Místek	k1gInSc6	Frýdku-Místek
2005	[number]	k4	2005
-	-	kIx~	-
evropský	evropský	k2eAgInSc1d1	evropský
festival	festival	k1gInSc1	festival
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
2006	[number]	k4	2006
-	-	kIx~	-
televizní	televizní	k2eAgFnSc1d1	televizní
pěvecká	pěvecký	k2eAgFnSc1d1	pěvecká
soutěž	soutěž	k1gFnSc1	soutěž
Šance	šance	k1gFnSc1	šance
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
2006	[number]	k4	2006
-	-	kIx~	-
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g />
.	.	kIx.	.
</s>
<s>
Mattoni	Mattoň	k1gFnSc6	Mattoň
2006	[number]	k4	2006
-	-	kIx~	-
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
-	-	kIx~	-
RGM	RGM	kA	RGM
hudební	hudební	k2eAgFnPc4d1	hudební
ceny	cena	k1gFnPc4	cena
TV	TV	kA	TV
Óčka	Óčka	k1gFnSc1	Óčka
-	-	kIx~	-
"	"	kIx"	"
<g/>
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
2007	[number]	k4	2007
-	-	kIx~	-
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
Allianz	Allianza	k1gFnPc2	Allianza
2006	[number]	k4	2006
-	-	kIx~	-
album	album	k1gNnSc1	album
"	"	kIx"	"
<g/>
Měls	Měls	k1gInSc1	Měls
mě	já	k3xPp1nSc2	já
vůbec	vůbec	k9	vůbec
rád	rád	k6eAd1	rád
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
"	"	kIx"	"
<g/>
Nejprodávanějším	prodávaný	k2eAgInSc7d3	nejprodávanější
debutem	debut	k1gInSc7	debut
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
17	[number]	k4	17
308	[number]	k4	308
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
-	-	kIx~	-
"	"	kIx"	"
<g/>
Objev	objev	k1gInSc1	objev
Ro	Ro	k1gFnSc2	Ro
<g/>
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
ku	k	k7c3	k
<g/>
"	"	kIx"	"
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
časopisu	časopis	k1gInSc2	časopis
Rock	rock	k1gInSc1	rock
<g/>
&	&	k?	&
<g/>
Pop	pop	k1gInSc1	pop
2007	[number]	k4	2007
-	-	kIx~	-
Nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
"	"	kIx"	"
<g/>
Objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
Anděl	Anděl	k1gMnSc1	Anděl
Allianz	Allianz	k1gMnSc1	Allianz
2006	[number]	k4	2006
2007	[number]	k4	2007
-	-	kIx~	-
platinové	platinový	k2eAgNnSc1d1	platinové
album	album	k1gNnSc1	album
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
více	hodně	k6eAd2	hodně
<g />
.	.	kIx.	.
</s>
<s>
než	než	k8xS	než
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
desek	deska	k1gFnPc2	deska
2007	[number]	k4	2007
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
JETIX	JETIX	kA	JETIX
KIDS	KIDS	kA	KIDS
AWARDS	AWARDS	kA	AWARDS
2007	[number]	k4	2007
2007	[number]	k4	2007
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
Mattoni	Matton	k1gMnPc1	Matton
2007	[number]	k4	2007
2008	[number]	k4	2008
-	-	kIx~	-
Srebrne	Srebrn	k1gInSc5	Srebrn
Spinki	Spinki	k1gNnSc1	Spinki
<g/>
,	,	kIx,	,
ocenění	ocenění	k1gNnSc1	ocenění
generálního	generální	k2eAgMnSc2d1	generální
konzula	konzul	k1gMnSc2	konzul
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
za	za	k7c4	za
hudební	hudební	k2eAgInPc4d1	hudební
úspěchy	úspěch	k1gInPc4	úspěch
a	a	k8xC	a
propagaci	propagace	k1gFnSc4	propagace
polské	polský	k2eAgFnSc2d1	polská
menšiny	menšina	k1gFnSc2	menšina
z	z	k7c2	z
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
2008	[number]	k4	2008
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
roku	rok	k1gInSc2	rok
na	na	k7c4	na
udílení	udílení	k1gNnSc4	udílení
cen	cena	k1gFnPc2	cena
APH	APH	kA	APH
Anděl	Anděl	k1gMnSc1	Anděl
Alianz	Alianz	k1gMnSc1	Alianz
2007	[number]	k4	2007
2008	[number]	k4	2008
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
zpěvaček	zpěvačka	k1gFnPc2	zpěvačka
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
TýTý	TýTý	k1gFnSc1	TýTý
2007	[number]	k4	2007
2008	[number]	k4	2008
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
2008	[number]	k4	2008
2009	[number]	k4	2009
-	-	kIx~	-
Opolí	Opolí	k1gNnSc2	Opolí
-	-	kIx~	-
Superjedynky	Superjedynka	k1gFnSc2	Superjedynka
2009	[number]	k4	2009
-	-	kIx~	-
Album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Cicho	Cic	k1gMnSc2	Cic
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
polskou	polský	k2eAgFnSc4d1	polská
cenu	cena	k1gFnSc4	cena
MTV	MTV	kA	MTV
2009	[number]	k4	2009
-	-	kIx~	-
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
interpret	interpret	k1gMnSc1	interpret
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
2009	[number]	k4	2009
-	-	kIx~	-
Anděl	Anděla	k1gFnPc2	Anděla
2008	[number]	k4	2008
-	-	kIx~	-
nominace	nominace	k1gFnSc2	nominace
na	na	k7c4	na
Zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
-	-	kIx~	-
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
-	-	kIx~	-
Blíž	blízce	k6eAd2	blízce
ke	k	k7c3	k
hvězdám	hvězda	k1gFnPc3	hvězda
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejprodávanější	prodávaný	k2eAgFnSc2d3	nejprodávanější
hudební	hudební	k2eAgFnSc2d1	hudební
DVD	DVD	kA	DVD
2009	[number]	k4	2009
-	-	kIx~	-
SopotHit	SopotHit	k2eAgInSc1d1	SopotHit
Festiwal	Festiwal	k1gInSc1	Festiwal
-	-	kIx~	-
Polský	polský	k2eAgInSc1d1	polský
Hit	hit	k1gInSc1	hit
2009	[number]	k4	2009
2009	[number]	k4	2009
-	-	kIx~	-
Musiq	Musiq	k1gFnSc1	Musiq
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
awards	awards	k6eAd1	awards
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
2009	[number]	k4	2009
2010	[number]	k4	2010
-	-	kIx~	-
Viva	Viva	k1gMnSc1	Viva
Comet	Comet	k1gInSc4	Comet
2010	[number]	k4	2010
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
-	-	kIx~	-
Viva	Viva	k1gMnSc1	Viva
Comet	Comet	k1gMnSc1	Comet
2010	[number]	k4	2010
Hit	hit	k1gInSc1	hit
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
-	-	kIx~	-
Eska	eska	k1gFnSc1	eska
Music	Music	k1gMnSc1	Music
Awards	Awards	k1gInSc1	Awards
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Hit	hit	k1gInSc1	hit
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
-	-	kIx~	-
Cicho	Cic	k1gMnSc2	Cic
2010	[number]	k4	2010
-	-	kIx~	-
Musiq	Musiq	k1gFnPc2	Musiq
<g/>
1	[number]	k4	1
awards	awardsa	k1gFnPc2	awardsa
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
2010	[number]	k4	2010
2011	[number]	k4	2011
-	-	kIx~	-
Viva	Viva	k1gMnSc1	Viva
Comet	Comet	k1gMnSc1	Comet
2011	[number]	k4	2011
Album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
-	-	kIx~	-
Viva	Viva	k1gMnSc1	Viva
Comet	Comet	k1gMnSc1	Comet
2011	[number]	k4	2011
Videoklip	videoklip	k1gInSc1	videoklip
2011	[number]	k4	2011
-	-	kIx~	-
Viva	Viva	k1gFnSc1	Viva
Comet	Cometa	k1gFnPc2	Cometa
2011	[number]	k4	2011
Píseň	píseň	k1gFnSc4	píseň
roku	rok	k1gInSc2	rok
<g />
.	.	kIx.	.
</s>
<s>
2011	[number]	k4	2011
-	-	kIx~	-
Viva	Viva	k1gFnSc1	Viva
Comet	Comet	k1gInSc1	Comet
2011	[number]	k4	2011
Image	image	k1gInSc2	image
umělce	umělec	k1gMnSc2	umělec
2011	[number]	k4	2011
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
2011	[number]	k4	2011
2012	[number]	k4	2012
-	-	kIx~	-
Viva	Viva	k1gMnSc1	Viva
Comet	Comet	k1gInSc4	Comet
2012	[number]	k4	2012
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
-	-	kIx~	-
Viva	Viva	k1gMnSc1	Viva
Comet	Comet	k1gInSc4	Comet
2012	[number]	k4	2012
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
na	na	k7c4	na
viva-tv	vivav	k1gInSc4	viva-tv
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
łez	łez	k?	łez
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
-	-	kIx~	-
Viva	Viv	k1gInSc2	Viv
<g />
.	.	kIx.	.
</s>
<s>
Comet	Comet	k1gMnSc1	Comet
2012	[number]	k4	2012
Dzwonek	Dzwonek	k1gInSc1	Dzwonek
Roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
łez	łez	k?	łez
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k6eAd1	místo
Český	český	k2eAgInSc1d1	český
Slavík	slavík	k1gInSc1	slavík
Mattoni	Mattoň	k1gFnSc6	Mattoň
2013	[number]	k4	2013
-	-	kIx~	-
Nominace	nominace	k1gFnSc1	nominace
Eska	eska	k1gFnSc1	eska
music	musice	k1gInPc2	musice
awards	awards	k6eAd1	awards
2013	[number]	k4	2013
2013	[number]	k4	2013
-	-	kIx~	-
Čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Slavíku	slavík	k1gInSc6	slavík
2013	[number]	k4	2013
2014	[number]	k4	2014
-	-	kIx~	-
Superjedynki	Superjedynk	k1gFnSc6	Superjedynk
-SuperAlbum	-SuperAlbum	k1gNnSc4	-SuperAlbum
((	((	k?	((
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
INNA	INNA	kA	INNA
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
2014	[number]	k4	2014
-	-	kIx~	-
Eska	eska	k1gFnSc1	eska
Music	Music	k1gMnSc1	Music
Awards	Awards	k1gInSc4	Awards
2014	[number]	k4	2014
<g/>
-Nominace	-Nominace	k1gFnSc1	-Nominace
2014	[number]	k4	2014
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Slavíku	slavík	k1gInSc6	slavík
2015	[number]	k4	2015
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Slavíku	slavík	k1gInSc6	slavík
2016	[number]	k4	2016
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Slavíku	slavík	k1gInSc6	slavík
</s>
