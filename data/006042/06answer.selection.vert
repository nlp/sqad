<s>
Pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
skupenství	skupenství	k1gNnSc2	skupenství
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc1	pojem
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
obecnější	obecní	k2eAgMnSc1d2	obecní
než	než	k8xS	než
skupenství	skupenství	k1gNnSc1	skupenství
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
látka	látka	k1gFnSc1	látka
může	moct	k5eAaImIp3nS	moct
za	za	k7c2	za
různých	různý	k2eAgFnPc2d1	různá
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
tlaků	tlak	k1gInPc2	tlak
existovat	existovat	k5eAaImF	existovat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
<g/>
,	,	kIx,	,
lišících	lišící	k2eAgFnPc2d1	lišící
se	se	k3xPyFc4	se
např.	např.	kA	např.
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
stavbou	stavba	k1gFnSc7	stavba
<g/>
.	.	kIx.	.
</s>
