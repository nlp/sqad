<s>
Skupenství	skupenství	k1gNnSc1	skupenství
neboli	neboli	k8xC	neboli
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
forma	forma	k1gFnSc1	forma
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
charakterizovaná	charakterizovaný	k2eAgFnSc1d1	charakterizovaná
především	především	k6eAd1	především
uspořádáním	uspořádání	k1gNnSc7	uspořádání
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
látce	látka	k1gFnSc6	látka
a	a	k8xC	a
projevující	projevující	k2eAgMnSc1d1	projevující
se	se	k3xPyFc4	se
typickými	typický	k2eAgFnPc7d1	typická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
skupenství	skupenství	k1gNnSc2	skupenství
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc1	pojem
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
obecnější	obecní	k2eAgMnSc1d2	obecní
než	než	k8xS	než
skupenství	skupenství	k1gNnSc1	skupenství
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
látka	látka	k1gFnSc1	látka
může	moct	k5eAaImIp3nS	moct
za	za	k7c2	za
různých	různý	k2eAgFnPc2d1	různá
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
tlaků	tlak	k1gInPc2	tlak
existovat	existovat	k5eAaImF	existovat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
<g/>
,	,	kIx,	,
lišících	lišící	k2eAgFnPc2d1	lišící
se	se	k3xPyFc4	se
např.	např.	kA	např.
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
stavbou	stavba	k1gFnSc7	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Čistá	čistý	k2eAgFnSc1d1	čistá
látka	látka	k1gFnSc1	látka
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
rovnovážném	rovnovážný	k2eAgInSc6d1	rovnovážný
stavu	stav	k1gInSc6	stav
za	za	k7c2	za
dané	daný	k2eAgFnSc2d1	daná
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
existovat	existovat	k5eAaImF	existovat
buď	buď	k8xC	buď
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvou	dva	k4xCgMnPc6	dva
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nejvýše	vysoce	k6eAd3	vysoce
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
fázích	fáze	k1gFnPc6	fáze
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
graficky	graficky	k6eAd1	graficky
popisováno	popisovat	k5eAaImNgNnS	popisovat
fázovým	fázový	k2eAgInSc7d1	fázový
diagramem	diagram	k1gInSc7	diagram
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
též	též	k6eAd1	též
stavový	stavový	k2eAgInSc1d1	stavový
diagram	diagram	k1gInSc1	diagram
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
osu	osa	k1gFnSc4	osa
x	x	k?	x
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vynáší	vynášet	k5eAaImIp3nS	vynášet
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
na	na	k7c4	na
osu	osa	k1gFnSc4	osa
y	y	k?	y
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
oblasti	oblast	k1gFnPc1	oblast
roviny	rovina	k1gFnSc2	rovina
grafu	graf	k1gInSc2	graf
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
existenci	existence	k1gFnSc4	existence
jediné	jediný	k2eAgFnSc2d1	jediná
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
hraniční	hraniční	k2eAgFnPc4d1	hraniční
křivky	křivka	k1gFnPc4	křivka
mezi	mezi	k7c7	mezi
oblastmi	oblast	k1gFnPc7	oblast
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
koexistenci	koexistence	k1gFnSc4	koexistence
dvou	dva	k4xCgFnPc2	dva
fází	fáze	k1gFnPc2	fáze
a	a	k8xC	a
v	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
tři	tři	k4xCgFnPc1	tři
křivky	křivka	k1gFnPc1	křivka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
trojný	trojný	k2eAgInSc1d1	trojný
bod	bod	k1gInSc1	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
současně	současně	k6eAd1	současně
tři	tři	k4xCgFnPc4	tři
fáze	fáze	k1gFnPc4	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
tři	tři	k4xCgNnPc4	tři
skupenství	skupenství	k1gNnPc4	skupenství
pevné	pevný	k2eAgFnSc2d1	pevná
<g/>
,	,	kIx,	,
kapalné	kapalný	k2eAgFnSc2d1	kapalná
a	a	k8xC	a
plynné	plynný	k2eAgFnSc2d1	plynná
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jsou	být	k5eAaImIp3nP	být
běžná	běžný	k2eAgFnSc1d1	běžná
v	v	k7c6	v
našem	náš	k3xOp1gNnSc6	náš
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
skupenství	skupenství	k1gNnSc1	skupenství
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
označováno	označován	k2eAgNnSc4d1	označováno
plazma	plazma	k1gNnSc4	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Skupenství	skupenství	k1gNnSc1	skupenství
látky	látka	k1gFnSc2	látka
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
energií	energie	k1gFnSc7	energie
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
skupenství	skupenství	k1gNnSc2	skupenství
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
pevným	pevný	k2eAgMnPc3d1	pevný
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
uspořádáním	uspořádání	k1gNnSc7	uspořádání
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
z	z	k7c2	z
pevné	pevný	k2eAgFnSc2d1	pevná
látky	látka	k1gFnSc2	látka
drží	držet	k5eAaImIp3nS	držet
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
do	do	k7c2	do
nějakého	nějaký	k3yIgInSc2	nějaký
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síla	k1gFnPc1	síla
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
pevné	pevný	k2eAgFnSc2d1	pevná
látky	látka	k1gFnSc2	látka
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
silnější	silný	k2eAgFnPc1d2	silnější
než	než	k8xS	než
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
způsobily	způsobit	k5eAaPmAgFnP	způsobit
jeho	on	k3xPp3gInSc4	on
rozpad	rozpad	k1gInSc4	rozpad
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
s	s	k7c7	s
pozemskými	pozemský	k2eAgFnPc7d1	pozemská
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapalině	kapalina	k1gFnSc6	kapalina
jsou	být	k5eAaImIp3nP	být
částice	částice	k1gFnPc1	částice
látky	látka	k1gFnSc2	látka
stále	stále	k6eAd1	stále
drženy	držen	k2eAgInPc1d1	držen
pohromadě	pohromadě	k6eAd1	pohromadě
slabými	slabý	k2eAgFnPc7d1	slabá
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
pevně	pevně	k6eAd1	pevně
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
<g/>
.	.	kIx.	.
</s>
<s>
Kapaliny	kapalina	k1gFnPc1	kapalina
nejdou	jít	k5eNaImIp3nP	jít
stlačit	stlačit	k5eAaPmF	stlačit
<g/>
.	.	kIx.	.
</s>
<s>
Kapalinu	kapalina	k1gFnSc4	kapalina
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
uchovávat	uchovávat	k5eAaImF	uchovávat
v	v	k7c6	v
nádobách	nádoba	k1gFnPc6	nádoba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
udržet	udržet	k5eAaPmF	udržet
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
-	-	kIx~	-
rozlévá	rozlévat	k5eAaImIp3nS	rozlévat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapalina	kapalina	k1gFnSc1	kapalina
je	být	k5eAaImIp3nS	být
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Plyn	plyn	k1gInSc1	plyn
patří	patřit	k5eAaImIp3nS	patřit
s	s	k7c7	s
kapalinou	kapalina	k1gFnSc7	kapalina
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
plynu	plyn	k1gInSc2	plyn
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
drženy	držet	k5eAaImNgFnP	držet
pohromadě	pohromadě	k6eAd1	pohromadě
žádnými	žádný	k3yNgFnPc7	žádný
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
vzájemných	vzájemný	k2eAgFnPc6d1	vzájemná
srážkách	srážka	k1gFnPc6	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
kapalině	kapalina	k1gFnSc3	kapalina
bývá	bývat	k5eAaImIp3nS	bývat
mnohem	mnohem	k6eAd1	mnohem
snadněji	snadno	k6eAd2	snadno
stlačitelný	stlačitelný	k2eAgInSc1d1	stlačitelný
<g/>
.	.	kIx.	.
</s>
<s>
Plyn	plyn	k1gInSc1	plyn
nelze	lze	k6eNd1	lze
skladovat	skladovat	k5eAaImF	skladovat
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
ho	on	k3xPp3gInSc4	on
uzavřít	uzavřít	k5eAaPmF	uzavřít
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ačkoli	ačkoli	k8xS	ačkoli
střední	střední	k2eAgFnSc1d1	střední
volná	volný	k2eAgFnSc1d1	volná
dráha	dráha	k1gFnSc1	dráha
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
poměrně	poměrně	k6eAd1	poměrně
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
by	by	kYmCp3nS	by
z	z	k7c2	z
otevřené	otevřený	k2eAgFnSc2d1	otevřená
nádoby	nádoba	k1gFnSc2	nádoba
vyprchával	vyprchávat	k5eAaImAgInS	vyprchávat
a	a	k8xC	a
mísil	mísit	k5eAaImAgInS	mísit
se	se	k3xPyFc4	se
s	s	k7c7	s
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
skupenství	skupenství	k1gNnSc4	skupenství
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
považuje	považovat	k5eAaImIp3nS	považovat
plazma	plazma	k1gNnSc4	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
trochu	trochu	k6eAd1	trochu
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
skupenství	skupenství	k1gNnSc4	skupenství
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chemie	chemie	k1gFnSc1	chemie
již	již	k6eAd1	již
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
popsat	popsat	k5eAaPmF	popsat
chování	chování	k1gNnSc4	chování
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
něm.	něm.	k?	něm.
Z	z	k7c2	z
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
hlediska	hledisko	k1gNnSc2	hledisko
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
třeba	třeba	k6eAd1	třeba
o	o	k7c4	o
plazma	plazma	k1gNnSc4	plazma
vodíkové	vodíkový	k2eAgFnSc2d1	vodíková
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
heliové	heliový	k2eAgFnPc1d1	heliová
<g/>
.	.	kIx.	.
</s>
<s>
Podstatným	podstatný	k2eAgNnSc7d1	podstatné
kritériem	kritérion	k1gNnSc7	kritérion
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
můžeme	moct	k5eAaImIp1nP	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
plazmatem	plazma	k1gNnSc7	plazma
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc1	existence
některých	některý	k3yIgInPc2	některý
kolektivních	kolektivní	k2eAgInPc2d1	kolektivní
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
společné	společný	k2eAgFnPc4d1	společná
specifické	specifický	k2eAgFnPc4d1	specifická
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
99	[number]	k4	99
<g/>
%	%	kIx~	%
veškeré	veškerý	k3xTgFnSc2	veškerý
viditelné	viditelný	k2eAgFnSc2d1	viditelná
hmoty	hmota	k1gFnSc2	hmota
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skupenství	skupenství	k1gNnSc6	skupenství
plazmatu	plazma	k1gNnSc2	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Kvark	kvark	k1gInSc1	kvark
gluonovým	gluonův	k2eAgNnSc7d1	gluonův
plazmatem	plazma	k1gNnSc7	plazma
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
páté	pátý	k4xOgNnSc1	pátý
skupenství	skupenství	k1gNnSc1	skupenství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc6d1	vysoká
teplotě	teplota	k1gFnSc6	teplota
(	(	kIx(	(
<g/>
větší	veliký	k2eAgFnSc7d2	veliký
než	než	k8xS	než
1012	[number]	k4	1012
K	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kvarky	kvark	k1gInPc1	kvark
a	a	k8xC	a
gluony	gluon	k1gInPc1	gluon
začnou	začít	k5eAaPmIp3nP	začít
chovat	chovat	k5eAaImF	chovat
jako	jako	k9	jako
volné	volný	k2eAgFnPc1d1	volná
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
již	již	k6eAd1	již
opravdu	opravdu	k6eAd1	opravdu
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
smysl	smysl	k1gInSc4	smysl
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
prvcích	prvek	k1gInPc6	prvek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
složena	složit	k5eAaPmNgFnS	složit
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
v	v	k7c4	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
při	při	k7c6	při
srážkách	srážka	k1gFnPc6	srážka
jader	jádro	k1gNnPc2	jádro
těžkých	těžký	k2eAgInPc2d1	těžký
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
urychlovačích	urychlovač	k1gInPc6	urychlovač
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
oznámen	oznámit	k5eAaPmNgInS	oznámit
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Bose-Einsteinův	Bose-Einsteinův	k2eAgInSc1d1	Bose-Einsteinův
kondenzát	kondenzát	k1gInSc1	kondenzát
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
tvořená	tvořený	k2eAgFnSc1d1	tvořená
bosony	bosona	k1gFnPc1	bosona
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
blížící	blížící	k2eAgFnSc6d1	blížící
se	se	k3xPyFc4	se
absolutní	absolutní	k2eAgFnSc3d1	absolutní
nule	nula	k1gFnSc3	nula
(	(	kIx(	(
<g/>
0	[number]	k4	0
kelvinů	kelvin	k1gInPc2	kelvin
neboli	neboli	k8xC	neboli
-273,15	-273,15	k4	-273,15
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
takovýchto	takovýto	k3xDgFnPc2	takovýto
podmínek	podmínka	k1gFnPc2	podmínka
má	mít	k5eAaImIp3nS	mít
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
atomů	atom	k1gInPc2	atom
minimální	minimální	k2eAgFnSc4d1	minimální
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Kvantový	kvantový	k2eAgInSc1d1	kvantový
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
na	na	k7c6	na
makromolekulární	makromolekulární	k2eAgFnSc6d1	makromolekulární
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
Boseho-Einsteinovy	Boseho-Einsteinův	k2eAgFnSc2d1	Boseho-Einsteinova
kondenzace	kondenzace	k1gFnSc2	kondenzace
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
zchlazený	zchlazený	k2eAgInSc1d1	zchlazený
oblak	oblak	k1gInSc1	oblak
atomů	atom	k1gInPc2	atom
s	s	k7c7	s
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
všechny	všechen	k3xTgInPc1	všechen
atomy	atom	k1gInPc1	atom
přejdou	přejít	k5eAaPmIp3nP	přejít
do	do	k7c2	do
stejného	stejný	k2eAgInSc2d1	stejný
kvantového	kvantový	k2eAgInSc2d1	kvantový
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
makroskopicky	makroskopicky	k6eAd1	makroskopicky
pozorovatelné	pozorovatelný	k2eAgNnSc4d1	pozorovatelné
kvantové	kvantový	k2eAgNnSc4d1	kvantové
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Supratekutost	supratekutost	k1gFnSc1	supratekutost
-	-	kIx~	-
Supratekutá	Supratekutý	k2eAgFnSc1d1	Supratekutý
látka	látka	k1gFnSc1	látka
nebo	nebo	k8xC	nebo
též	též	k9	též
supratekutina	supratekutina	k1gFnSc1	supratekutina
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
supra	supra	k1gNnSc2	supra
=	=	kIx~	=
nad	nad	k7c7	nad
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kapalina	kapalina	k1gFnSc1	kapalina
s	s	k7c7	s
nulovou	nulový	k2eAgFnSc7d1	nulová
viskozitou	viskozita	k1gFnSc7	viskozita
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
supratekutost	supratekutost	k1gFnSc1	supratekutost
<g/>
.	.	kIx.	.
</s>
<s>
Donedávna	donedávna	k6eAd1	donedávna
byla	být	k5eAaImAgFnS	být
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
jen	jen	k9	jen
u	u	k7c2	u
helia	helium	k1gNnSc2	helium
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
blízkých	blízký	k2eAgFnPc6d1	blízká
absolutní	absolutní	k2eAgFnSc4d1	absolutní
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Fermiho	Fermize	k6eAd1	Fermize
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
Degenerovaný	degenerovaný	k2eAgMnSc1d1	degenerovaný
Fermiho	Fermi	k1gMnSc2	Fermi
plyn	plyn	k1gInSc1	plyn
nebo	nebo	k8xC	nebo
též	též	k9	též
Fermiho	Fermi	k1gMnSc4	Fermi
atomární	atomární	k2eAgInSc1d1	atomární
plyn	plyn	k1gInSc1	plyn
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
fermionů	fermion	k1gInPc2	fermion
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
absolutní	absolutní	k2eAgFnSc6d1	absolutní
nule	nula	k1gFnSc6	nula
fermiony	fermion	k1gInPc1	fermion
obsazují	obsazovat	k5eAaImIp3nP	obsazovat
energetické	energetický	k2eAgFnSc2d1	energetická
hladiny	hladina	k1gFnSc2	hladina
od	od	k7c2	od
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
tak	tak	k8xS	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
stavu	stav	k1gInSc6	stav
nebyl	být	k5eNaImAgMnS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
energetické	energetický	k2eAgFnSc6d1	energetická
hladině	hladina	k1gFnSc6	hladina
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
více	hodně	k6eAd2	hodně
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
dalším	další	k2eAgNnSc7d1	další
kvantovým	kvantový	k2eAgNnSc7d1	kvantové
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
obsazené	obsazený	k2eAgFnSc3d1	obsazená
energetické	energetický	k2eAgFnSc3d1	energetická
hladině	hladina	k1gFnSc3	hladina
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
Fermiho	Fermi	k1gMnSc2	Fermi
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
EF	EF	kA	EF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
zchlazené	zchlazený	k2eAgInPc1d1	zchlazený
na	na	k7c4	na
absolutní	absolutní	k2eAgFnSc4d1	absolutní
nulu	nula	k1gFnSc4	nula
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Fermionický	Fermionický	k2eAgInSc4d1	Fermionický
kondenzát	kondenzát	k1gInSc4	kondenzát
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stavu	stav	k1gInSc6	stav
ztratí	ztratit	k5eAaPmIp3nP	ztratit
veškerou	veškerý	k3xTgFnSc4	veškerý
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Látka	látka	k1gFnSc1	látka
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
skupenství	skupenství	k1gNnSc6	skupenství
přenáší	přenášet	k5eAaImIp3nS	přenášet
energii	energie	k1gFnSc4	energie
téměř	téměř	k6eAd1	téměř
beze	beze	k7c2	beze
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
hustotách	hustota	k1gFnPc6	hustota
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
energetické	energetický	k2eAgFnPc1d1	energetická
hladiny	hladina	k1gFnPc1	hladina
elektronů	elektron	k1gInPc2	elektron
obsazeny	obsazen	k2eAgFnPc1d1	obsazena
až	až	k8xS	až
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
maximální	maximální	k2eAgFnSc2d1	maximální
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
určitá	určitý	k2eAgFnSc1d1	určitá
maximální	maximální	k2eAgFnSc1d1	maximální
hybnost	hybnost	k1gFnSc1	hybnost
<g/>
;	;	kIx,	;
tomuto	tento	k3xDgInSc3	tento
stavu	stav	k1gInSc3	stav
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
degenerace	degenerace	k1gFnSc1	degenerace
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
degenerovaný	degenerovaný	k2eAgInSc4d1	degenerovaný
elektronový	elektronový	k2eAgInSc4d1	elektronový
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
další	další	k2eAgInSc1d1	další
elektron	elektron	k1gInSc1	elektron
musí	muset	k5eAaImIp3nS	muset
zaujmou	zaujmout	k5eAaPmIp3nP	zaujmout
novou	nový	k2eAgFnSc4d1	nová
vyšší	vysoký	k2eAgFnSc4d2	vyšší
energetickou	energetický	k2eAgFnSc4d1	energetická
hladinu	hladina	k1gFnSc4	hladina
a	a	k8xC	a
mít	mít	k5eAaImF	mít
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hybnost	hybnost	k1gFnSc4	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
skupenství	skupenství	k1gNnSc1	skupenství
hmoty	hmota	k1gFnSc2	hmota
je	být	k5eAaImIp3nS	být
pozorovatelné	pozorovatelný	k2eAgNnSc1d1	pozorovatelné
v	v	k7c6	v
bílých	bílý	k2eAgMnPc6d1	bílý
trpaslících	trpaslík	k1gMnPc6	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Degenerovaný	degenerovaný	k2eAgInSc1d1	degenerovaný
neutronový	neutronový	k2eAgInSc1d1	neutronový
plyn	plyn	k1gInSc1	plyn
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
obrovských	obrovský	k2eAgFnPc6d1	obrovská
hustotách	hustota	k1gFnPc6	hustota
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
elektrony	elektron	k1gInPc4	elektron
"	"	kIx"	"
<g/>
vmáčknuty	vmáčknut	k2eAgFnPc4d1	vmáčknuta
<g/>
"	"	kIx"	"
gravitací	gravitace	k1gFnSc7	gravitace
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
protony	proton	k1gInPc7	proton
přemění	přeměnit	k5eAaPmIp3nP	přeměnit
na	na	k7c4	na
neutrony	neutron	k1gInPc4	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
skupenství	skupenství	k1gNnSc6	skupenství
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
neutronových	neutronový	k2eAgFnPc2d1	neutronová
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fázový	fázový	k2eAgInSc1d1	fázový
přechod	přechod	k1gInSc1	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Přechodu	přechod	k1gInSc3	přechod
od	od	k7c2	od
pevné	pevný	k2eAgFnSc2d1	pevná
látky	látka	k1gFnSc2	látka
ke	k	k7c3	k
kapalině	kapalina	k1gFnSc3	kapalina
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
tání	tání	k1gNnSc1	tání
<g/>
.	.	kIx.	.
</s>
<s>
Opačný	opačný	k2eAgInSc1d1	opačný
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tuhnutí	tuhnutí	k1gNnSc1	tuhnutí
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
těleso	těleso	k1gNnSc1	těleso
přešlo	přejít	k5eAaPmAgNnS	přejít
z	z	k7c2	z
pevné	pevný	k2eAgFnSc2d1	pevná
fáze	fáze	k1gFnSc2	fáze
do	do	k7c2	do
kapalné	kapalný	k2eAgFnSc2d1	kapalná
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
mu	on	k3xPp3gMnSc3	on
dodat	dodat	k5eAaPmF	dodat
skupenské	skupenský	k2eAgNnSc4d1	skupenské
teplo	teplo	k1gNnSc4	teplo
tání	tání	k1gNnSc2	tání
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mikroskopické	mikroskopický	k2eAgFnSc6d1	mikroskopická
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
rovná	rovnat	k5eAaImIp3nS	rovnat
dodání	dodání	k1gNnSc4	dodání
energie	energie	k1gFnSc2	energie
částici	částice	k1gFnSc4	částice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bude	být	k5eAaImBp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
energie	energie	k1gFnSc1	energie
vazby	vazba	k1gFnSc2	vazba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
částici	částice	k1gFnSc3	částice
v	v	k7c6	v
pevné	pevný	k2eAgFnSc6d1	pevná
látce	látka	k1gFnSc6	látka
drží	držet	k5eAaImIp3nS	držet
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pevné	pevný	k2eAgNnSc1d1	pevné
těleso	těleso	k1gNnSc1	těleso
mělo	mít	k5eAaImAgNnS	mít
nějakou	nějaký	k3yIgFnSc4	nějaký
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
částice	částice	k1gFnPc1	částice
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
uvolňovaly	uvolňovat	k5eAaImAgInP	uvolňovat
do	do	k7c2	do
kapalné	kapalný	k2eAgFnSc2d1	kapalná
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
bodu	bod	k1gInSc3	bod
tání	tání	k1gNnSc4	tání
<g/>
,	,	kIx,	,
přechod	přechod	k1gInSc4	přechod
do	do	k7c2	do
kapalné	kapalný	k2eAgFnSc2d1	kapalná
fáze	fáze	k1gFnSc2	fáze
nastane	nastat	k5eAaPmIp3nS	nastat
spontánně	spontánně	k6eAd1	spontánně
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
jeho	jeho	k3xOp3gInSc2	jeho
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Přechodu	přechod	k1gInSc3	přechod
od	od	k7c2	od
kapaliny	kapalina	k1gFnSc2	kapalina
k	k	k7c3	k
plynu	plyn	k1gInSc3	plyn
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
vypařování	vypařování	k1gNnSc1	vypařování
<g/>
.	.	kIx.	.
</s>
<s>
Opačný	opačný	k2eAgInSc1d1	opačný
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zkapalnění	zkapalnění	k1gNnSc1	zkapalnění
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
těleso	těleso	k1gNnSc1	těleso
přešlo	přejít	k5eAaPmAgNnS	přejít
z	z	k7c2	z
kapalné	kapalný	k2eAgFnSc2d1	kapalná
fáze	fáze	k1gFnSc2	fáze
do	do	k7c2	do
plynné	plynný	k2eAgFnSc2d1	plynná
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
mu	on	k3xPp3gMnSc3	on
dodat	dodat	k5eAaPmF	dodat
skupenské	skupenský	k2eAgNnSc4d1	skupenské
teplo	teplo	k1gNnSc4	teplo
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mikroskopické	mikroskopický	k2eAgFnSc6d1	mikroskopická
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
rovná	rovnat	k5eAaImIp3nS	rovnat
dodání	dodání	k1gNnSc4	dodání
energie	energie	k1gFnSc2	energie
částici	částice	k1gFnSc4	částice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
energie	energie	k1gFnSc1	energie
vazby	vazba	k1gFnSc2	vazba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
částici	částice	k1gFnSc3	částice
v	v	k7c6	v
kapalině	kapalina	k1gFnSc6	kapalina
drží	držet	k5eAaImIp3nS	držet
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kapalné	kapalný	k2eAgNnSc1d1	kapalné
těleso	těleso	k1gNnSc1	těleso
mělo	mít	k5eAaImAgNnS	mít
nějakou	nějaký	k3yIgFnSc4	nějaký
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
částice	částice	k1gFnPc1	částice
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
uvolňovaly	uvolňovat	k5eAaImAgInP	uvolňovat
do	do	k7c2	do
plynné	plynný	k2eAgFnSc2d1	plynná
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
bodu	bod	k1gInSc3	bod
varu	var	k1gInSc2	var
<g/>
,	,	kIx,	,
přechod	přechod	k1gInSc1	přechod
do	do	k7c2	do
plynné	plynný	k2eAgFnSc2d1	plynná
fáze	fáze	k1gFnSc2	fáze
nastane	nastat	k5eAaPmIp3nS	nastat
spontánně	spontánně	k6eAd1	spontánně
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
jeho	jeho	k3xOp3gInSc2	jeho
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
varu	var	k1gInSc6	var
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
částici	částice	k1gFnSc4	částice
na	na	k7c6	na
mikroskopické	mikroskopický	k2eAgFnSc6d1	mikroskopická
úrovni	úroveň	k1gFnSc6	úroveň
dodáme	dodat	k5eAaPmIp1nP	dodat
tolik	tolik	k6eAd1	tolik
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přetrhne	přetrhnout	k5eAaPmIp3nS	přetrhnout
nejen	nejen	k6eAd1	nejen
vazba	vazba	k1gFnSc1	vazba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
držela	držet	k5eAaImAgFnS	držet
na	na	k7c6	na
pevném	pevný	k2eAgNnSc6d1	pevné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vazba	vazba	k1gFnSc1	vazba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
udržela	udržet	k5eAaPmAgFnS	udržet
v	v	k7c6	v
kapalině	kapalina	k1gFnSc6	kapalina
<g/>
,	,	kIx,	,
částice	částice	k1gFnSc1	částice
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
jako	jako	k9	jako
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
vhodných	vhodný	k2eAgInPc6d1	vhodný
případech	případ	k1gInPc6	případ
lze	lze	k6eAd1	lze
tento	tento	k3xDgInSc4	tento
přechod	přechod	k1gInSc4	přechod
pozorovat	pozorovat	k5eAaImF	pozorovat
i	i	k9	i
na	na	k7c6	na
makroskopické	makroskopický	k2eAgFnSc6d1	makroskopická
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
sublimace	sublimace	k1gFnSc1	sublimace
<g/>
.	.	kIx.	.
</s>
<s>
Opačný	opačný	k2eAgInSc1d1	opačný
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
desublimace	desublimace	k1gFnSc1	desublimace
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
sublimace	sublimace	k1gFnSc2	sublimace
pro	pro	k7c4	pro
oba	dva	k4xCgInPc4	dva
směry	směr	k1gInPc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
první	první	k4xOgNnSc1	první
skupenství	skupenství	k1gNnSc1	skupenství
je	být	k5eAaImIp3nS	být
plynné	plynný	k2eAgNnSc1d1	plynné
<g/>
,	,	kIx,	,
kapalné	kapalný	k2eAgNnSc1d1	kapalné
nebo	nebo	k8xC	nebo
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
.	.	kIx.	.
</s>
<s>
Látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
kvalitativně	kvalitativně	k6eAd1	kvalitativně
v	v	k7c6	v
úplně	úplně	k6eAd1	úplně
novém	nový	k2eAgInSc6d1	nový
směru	směr	k1gInSc6	směr
-	-	kIx~	-
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
část	část	k1gFnSc1	část
nebo	nebo	k8xC	nebo
všechny	všechen	k3xTgInPc4	všechen
své	svůj	k3xOyFgInPc4	svůj
elektrony	elektron	k1gInPc4	elektron
z	z	k7c2	z
atomových	atomový	k2eAgInPc2d1	atomový
obalů	obal	k1gInPc2	obal
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgMnSc1d1	rozhodující
přitom	přitom	k6eAd1	přitom
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
silná	silný	k2eAgFnSc1d1	silná
ionizace	ionizace	k1gFnSc1	ionizace
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zda	zda	k8xS	zda
tato	tento	k3xDgFnSc1	tento
ionizace	ionizace	k1gFnSc1	ionizace
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
kolektivní	kolektivní	k2eAgNnSc4d1	kolektivní
chování	chování	k1gNnSc4	chování
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
velmi	velmi	k6eAd1	velmi
slabě	slabě	k6eAd1	slabě
ionizovaná	ionizovaný	k2eAgFnSc1d1	ionizovaná
látka	látka	k1gFnSc1	látka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
plazmatem	plazma	k1gNnSc7	plazma
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ionosféra	ionosféra	k1gFnSc1	ionosféra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
třeba	třeba	k6eAd1	třeba
plamen	plamen	k1gInSc1	plamen
ohně	oheň	k1gInSc2	oheň
se	se	k3xPyFc4	se
za	za	k7c4	za
plazma	plazma	k1gNnSc4	plazma
obvykle	obvykle	k6eAd1	obvykle
nepovažuje	považovat	k5eNaImIp3nS	považovat
<g/>
.	.	kIx.	.
</s>
<s>
Ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
skupenského	skupenský	k2eAgNnSc2d1	skupenské
tepla	teplo	k1gNnSc2	teplo
zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
energie	energie	k1gFnSc1	energie
potřebná	potřebný	k2eAgFnSc1d1	potřebná
k	k	k7c3	k
ionizaci	ionizace	k1gFnSc3	ionizace
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Plazma	plazma	k1gFnSc1	plazma
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
ionizovaný	ionizovaný	k2eAgInSc1d1	ionizovaný
plyn	plyn	k1gInSc1	plyn
při	při	k7c6	při
samostatném	samostatný	k2eAgInSc6d1	samostatný
výboji	výboj	k1gInSc6	výboj
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Tento	tento	k3xDgInSc1	tento
přechod	přechod	k1gInSc1	přechod
byl	být	k5eAaImAgInS	být
zatím	zatím	k6eAd1	zatím
pozorován	pozorovat	k5eAaImNgInS	pozorovat
jen	jen	k9	jen
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
speciálních	speciální	k2eAgInPc6d1	speciální
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc1	jaký
všechny	všechen	k3xTgInPc1	všechen
děje	děj	k1gInPc1	děj
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
z	z	k7c2	z
normálního	normální	k2eAgInSc2d1	normální
stavu	stav	k1gInSc2	stav
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
existence	existence	k1gFnSc2	existence
nukleonů	nukleon	k1gInPc2	nukleon
<g/>
)	)	kIx)	)
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
kvark	kvark	k1gInSc1	kvark
gluonového	gluonový	k2eAgNnSc2d1	gluonový
plazmatu	plazma	k1gNnSc2	plazma
probíhají	probíhat	k5eAaImIp3nP	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
odpovědí	odpověď	k1gFnPc2	odpověď
nám	my	k3xPp1nPc3	my
přinese	přinést	k5eAaPmIp3nS	přinést
urychlovač	urychlovač	k1gInSc1	urychlovač
LHC	LHC	kA	LHC
(	(	kIx(	(
<g/>
Large	Larg	k1gMnSc2	Larg
Hadron	Hadron	k1gMnSc1	Hadron
Collider	Collider	k1gMnSc1	Collider
<g/>
)	)	kIx)	)
v	v	k7c6	v
CERNu	CERNus	k1gInSc6	CERNus
s	s	k7c7	s
právě	právě	k6eAd1	právě
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
dokončenou	dokončený	k2eAgFnSc7d1	dokončená
soustavou	soustava	k1gFnSc7	soustava
detektorů	detektor	k1gInPc2	detektor
pro	pro	k7c4	pro
projekt	projekt	k1gInSc4	projekt
ALICE	Alice	k1gFnSc2	Alice
(	(	kIx(	(
<g/>
A	A	kA	A
Large	Large	k1gNnSc1	Large
Ion	ion	k1gInSc1	ion
Collider	Collider	k1gMnSc1	Collider
Experiment	experiment	k1gInSc1	experiment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Látka	látka	k1gFnSc1	látka
Fáze	fáze	k1gFnSc1	fáze
Termodynamika	termodynamika	k1gFnSc1	termodynamika
</s>
