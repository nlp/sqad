<p>
<s>
Children	Childrna	k1gFnPc2	Childrna
of	of	k?	of
Bodom	Bodom	k1gInSc1	Bodom
je	být	k5eAaImIp3nS	být
finská	finský	k2eAgFnSc1d1	finská
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
z	z	k7c2	z
města	město	k1gNnSc2	město
Espoo	Espoo	k6eAd1	Espoo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
IneartheD	IneartheD	k1gFnSc2	IneartheD
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
změnila	změnit	k5eAaPmAgFnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c6	na
Children	Childrno	k1gNnPc2	Childrno
of	of	k?	of
Bodom	Bodom	k1gInSc1	Bodom
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
z	z	k7c2	z
proslulých	proslulý	k2eAgFnPc2d1	proslulá
"	"	kIx"	"
<g/>
bodomských	bodomský	k2eAgFnPc2d1	bodomský
vražd	vražda	k1gFnPc2	vražda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
udály	udát	k5eAaPmAgFnP	udát
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
Bodom	Bodom	k1gInSc4	Bodom
v	v	k7c4	v
Espoo	Espoo	k1gNnSc4	Espoo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tématem	téma	k1gNnSc7	téma
skladeb	skladba	k1gFnPc2	skladba
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
právě	právě	k6eAd1	právě
jezero	jezero	k1gNnSc1	jezero
Bodom	Bodom	k1gInSc1	Bodom
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
problémy	problém	k1gInPc1	problém
v	v	k7c6	v
osobních	osobní	k2eAgInPc6d1	osobní
vztazích	vztah	k1gInPc6	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Alexi	Alexe	k1gFnSc4	Alexe
Laiho	Lai	k1gMnSc2	Lai
užívá	užívat	k5eAaImIp3nS	užívat
chraplavě	chraplavě	k6eAd1	chraplavě
křičícího	křičící	k2eAgInSc2d1	křičící
vokálu	vokál	k1gInSc2	vokál
(	(	kIx(	(
<g/>
screaming	screaming	k1gInSc1	screaming
<g/>
)	)	kIx)	)
s	s	k7c7	s
často	často	k6eAd1	často
k	k	k7c3	k
nepoznání	nepoznání	k1gNnSc3	nepoznání
zkomolenými	zkomolený	k2eAgNnPc7d1	zkomolené
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zpěváka	zpěvák	k1gMnSc4	zpěvák
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
špatnou	špatný	k2eAgFnSc7d1	špatná
anglickou	anglický	k2eAgFnSc7d1	anglická
výslovností	výslovnost	k1gFnSc7	výslovnost
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
mluví	mluvit	k5eAaImIp3nS	mluvit
anglicky	anglicky	k6eAd1	anglicky
plynule	plynule	k6eAd1	plynule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sestava	sestava	k1gFnSc1	sestava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Současní	současný	k2eAgMnPc1d1	současný
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Alexi	Alexe	k1gFnSc4	Alexe
Laiho	Lai	k1gMnSc2	Lai
–	–	k?	–
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Freyberg	Freyberg	k1gMnSc1	Freyberg
-	-	kIx~	-
rytmická	rytmický	k2eAgFnSc1d1	rytmická
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Janne	Jannout	k5eAaImIp3nS	Jannout
Wirman	Wirman	k1gMnSc1	Wirman
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
</s>
</p>
<p>
<s>
Henkka	Henkka	k1gMnSc1	Henkka
T.	T.	kA	T.
Blacksmith	Blacksmith	k1gMnSc1	Blacksmith
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
</s>
</p>
<p>
<s>
Jaska	Jaska	k1gMnSc1	Jaska
W.	W.	kA	W.
Raatikainen	Raatikainen	k1gInSc1	Raatikainen
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
</s>
</p>
<p>
<s>
===	===	k?	===
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Kuoppala	Kuoppal	k1gMnSc2	Kuoppal
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
1993-2003	[number]	k4	1993-2003
</s>
</p>
<p>
<s>
Erna	Ern	k2eAgFnSc1d1	Erna
Siikavirta	Siikavirta	k1gFnSc1	Siikavirta
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
(	(	kIx(	(
<g/>
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kimberly	Kimberla	k1gFnPc1	Kimberla
Goss	Goss	k1gInSc1	Goss
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
(	(	kIx(	(
<g/>
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Roope	Roopat	k5eAaPmIp3nS	Roopat
Latvala	Latvala	k1gFnSc7	Latvala
–	–	k?	–
rytmická	rytmický	k2eAgFnSc1d1	rytmická
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Antti	Antti	k1gNnSc1	Antti
Wirman	Wirman	k1gMnSc1	Wirman
–	–	k?	–
rytmická	rytmický	k2eAgFnSc1d1	rytmická
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Časová	časový	k2eAgFnSc1d1	časová
osa	osa	k1gFnSc1	osa
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Something	Something	k1gInSc1	Something
Wild	Wild	k1gInSc1	Wild
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hatebreeder	Hatebreeder	k1gInSc1	Hatebreeder
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Follow	Follow	k?	Follow
the	the	k?	the
Reaper	Reaper	k1gInSc1	Reaper
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hate	Hate	k6eAd1	Hate
Crew	Crew	k1gMnSc1	Crew
Deathroll	Deathroll	k1gMnSc1	Deathroll
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Are	ar	k1gInSc5	ar
You	You	k1gFnSc2	You
Dead	Dead	k1gMnSc1	Dead
Yet	Yet	k1gMnSc1	Yet
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Blooddrunk	Blooddrunk	k1gInSc1	Blooddrunk
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Relentless	Relentless	k6eAd1	Relentless
Reckless	Reckless	k1gInSc1	Reckless
Forever	Forevra	k1gFnPc2	Forevra
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Halo	halo	k1gNnSc1	halo
of	of	k?	of
Blood	Blooda	k1gFnPc2	Blooda
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
I	i	k9	i
Worship	Worship	k1gInSc1	Worship
Chaos	chaos	k1gInSc1	chaos
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hexed	Hexed	k1gInSc1	Hexed
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
<g/>
Koncertní	koncertní	k2eAgNnSc1d1	koncertní
albaTokyo	albaTokyo	k1gNnSc1	albaTokyo
Warhearts	Warheartsa	k1gFnPc2	Warheartsa
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chaos	chaos	k1gInSc1	chaos
Ridden	Riddna	k1gFnPc2	Riddna
Years	Years	k1gInSc4	Years
–	–	k?	–
Stockholm	Stockholm	k1gInSc1	Stockholm
Knockout	Knockout	k1gMnSc1	Knockout
Live	Live	k1gInSc1	Live
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
OstatníChildren	OstatníChildrna	k1gFnPc2	OstatníChildrna
of	of	k?	of
Bodom	Bodom	k1gInSc1	Bodom
-	-	kIx~	-
split	split	k2eAgInSc1d1	split
singl	singl	k1gInSc1	singl
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bestbreeder	Bestbreeder	k1gMnSc1	Bestbreeder
from	from	k1gInSc4	from
1997	[number]	k4	1997
to	ten	k3xDgNnSc4	ten
2000	[number]	k4	2000
–	–	k?	–
best	best	k1gMnSc1	best
of	of	k?	of
<g/>
/	/	kIx~	/
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Thrashed	Thrashed	k1gMnSc1	Thrashed
<g/>
,	,	kIx,	,
Lost	Lost	k1gMnSc1	Lost
&	&	k?	&
Strungout	Strungout	k1gMnSc1	Strungout
–	–	k?	–
EP	EP	kA	EP
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skeletons	Skeletons	k1gInSc1	Skeletons
in	in	k?	in
the	the	k?	the
Closet	Closet	k1gInSc1	Closet
–	–	k?	–
kompilace	kompilace	k1gFnSc2	kompilace
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Children	Childrna	k1gFnPc2	Childrna
of	of	k?	of
Bodom	Bodom	k1gInSc1	Bodom
na	na	k7c4	na
MySpace	MySpace	k1gFnPc4	MySpace
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
fanouškovské	fanouškovský	k2eAgNnSc1d1	fanouškovské
fórum	fórum	k1gNnSc1	fórum
</s>
</p>
