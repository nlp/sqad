<p>
<s>
Bambule	bambula	k1gFnSc3	bambula
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
Sto	sto	k4xCgNnSc4	sto
zvířat	zvíře	k1gNnPc2	zvíře
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
Podpaží	podpažit	k5eAaPmIp3nS	podpažit
</s>
</p>
<p>
<s>
Parník	parník	k1gInSc1	parník
</s>
</p>
<p>
<s>
Oči	oko	k1gNnPc1	oko
skleněný	skleněný	k2eAgInSc4d1	skleněný
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
postel	postel	k1gInSc1	postel
</s>
</p>
<p>
<s>
Rozpočítadlo	rozpočítadlo	k1gNnSc1	rozpočítadlo
</s>
</p>
<p>
<s>
Chlápek	Chlápek	k?	Chlápek
</s>
</p>
<p>
<s>
Dej	dát	k5eAaPmRp2nS	dát
si	se	k3xPyFc3	se
hlavu	hlava	k1gFnSc4	hlava
na	na	k7c4	na
můj	můj	k3xOp1gInSc4	můj
polštář	polštář	k1gInSc4	polštář
</s>
</p>
<p>
<s>
Karneval	karneval	k1gInSc1	karneval
</s>
</p>
<p>
<s>
Chleba	chléb	k1gInSc2	chléb
</s>
</p>
<p>
<s>
Automat	automat	k1gInSc1	automat
</s>
</p>
<p>
<s>
Soused-ska	Sousedka	k1gFnSc1	Soused-ska
</s>
</p>
<p>
<s>
Krást	krást	k5eAaImF	krást
</s>
</p>
<p>
<s>
Dvojčata	dvojče	k1gNnPc1	dvojče
</s>
</p>
<p>
<s>
Osvobození	osvobození	k1gNnSc1	osvobození
</s>
</p>
<p>
<s>
U	u	k7c2	u
sklenice	sklenice	k1gFnSc2	sklenice
</s>
</p>
<p>
<s>
Smrtihlav	smrtihlav	k1gMnSc1	smrtihlav
</s>
</p>
<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
</s>
</p>
