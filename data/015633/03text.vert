<s>
Kobylnice	Kobylnice	k1gFnPc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Brno-venkov	Brno-venkov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kobylnice	Kobylnice	k1gFnPc1
Škola	škola	k1gFnSc1
a	a	k8xC
obecní	obecní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0643	CZ0643	k4
583219	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Šlapanice	Šlapanice	k1gFnSc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Brno-venkov	Brno-venkov	k1gInSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
643	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jihomoravský	jihomoravský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
64	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
17	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
55	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1	#num#	k4
189	#num#	k4
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
5,10	5,10	k4
km²	km²	k?
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Kobylnice	Kobylnice	k1gFnPc1
u	u	k7c2
Brna	Brno	k1gNnSc2
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
214	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
664	#num#	k4
51	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
1	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Na	na	k7c6
Budínku	Budínek	k1gInSc6
240	#num#	k4
<g/>
Kobylnice	Kobylnice	k1gFnPc4
<g/>
66451	#num#	k4
Šlapanice	Šlapanice	k1gFnSc2
u	u	k7c2
Brna	Brno	k1gNnSc2
obec@kobylnice.cz	obec@kobylnice.cz	k1gMnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Lubomír	Lubomír	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.kobylnice.cz	www.kobylnice.cz	k1gInSc1
</s>
<s>
Kobylnice	Kobylnice	k1gFnPc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
obce	obec	k1gFnPc1
</s>
<s>
583219	#num#	k4
Kód	kód	k1gInSc1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
67474	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kobylnice	Kobylnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
obec	obec	k1gFnSc4
v	v	k7c6
okrese	okres	k1gInSc6
Brno-venkov	Brno-venkov	k1gInSc1
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládají	rozkládat	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
Dyjsko-svrateckém	dyjsko-svratecký	k2eAgInSc6d1
úvalu	úval	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
katastrálním	katastrální	k2eAgInSc6d1
území	území	k1gNnSc2
Kobylnice	Kobylnice	k1gFnPc1
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
1	#num#	k4
200	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1306	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
staletí	staletí	k1gNnSc2
patřila	patřit	k5eAaImAgFnS
obec	obec	k1gFnSc1
různým	různý	k2eAgMnPc3d1
majitelům	majitel	k1gMnPc3
V	v	k7c4
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
v	v	k7c6
Kobylnicích	Kobylnice	k1gFnPc6
tvrz	tvrz	k1gFnSc1
<g/>
,	,	kIx,
dvůr	dvůr	k1gInSc1
<g/>
,	,	kIx,
pivovar	pivovar	k1gInSc1
a	a	k8xC
mlýn	mlýn	k1gInSc1
<g/>
,	,	kIx,
dva	dva	k4xCgInPc1
rybníky	rybník	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obec	obec	k1gFnSc1
patří	patřit	k5eAaImIp3nS
do	do	k7c2
šlapanické	šlapanický	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
školy	škola	k1gFnSc2
chodily	chodit	k5eAaImAgFnP
zdejší	zdejší	k2eAgFnPc1d1
děti	dítě	k1gFnPc1
do	do	k7c2
Šlapanic	Šlapanice	k1gFnPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
do	do	k7c2
Ponětovic	Ponětovice	k1gFnPc2
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
obci	obec	k1gFnSc6
postavena	postaven	k2eAgFnSc1d1
roku	rok	k1gInSc2
1853	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
dochovaná	dochovaný	k2eAgFnSc1d1
pečeť	pečeť	k1gFnSc1
s	s	k7c7
radlicí	radlice	k1gFnSc7
<g/>
,	,	kIx,
vinařským	vinařský	k2eAgInSc7d1
nožem	nůž	k1gInSc7
a	a	k8xC
hroznem	hrozen	k1gInSc7
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1648	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1986	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
byly	být	k5eAaImAgFnP
Kobylnice	Kobylnice	k1gFnPc1
součástí	součást	k1gFnPc2
Šlapanic	Šlapanice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
měla	mít	k5eAaImAgFnS
obec	obec	k1gFnSc1
25	#num#	k4
domů	dům	k1gInPc2
<g/>
,	,	kIx,
po	po	k7c6
třicetileté	třicetiletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
bylo	být	k5eAaImAgNnS
7	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
pustých	pustý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1790	#num#	k4
měla	mít	k5eAaImAgFnS
obec	obec	k1gFnSc1
41	#num#	k4
domů	dům	k1gInPc2
a	a	k8xC
206	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1834	#num#	k4
48	#num#	k4
domů	dům	k1gInPc2
a	a	k8xC
290	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
sčítání	sčítání	k1gNnSc1
lidu	lid	k1gInSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
18691880189019001910192119301950196119701980199120012011	#num#	k4
</s>
<s>
406514552569769843889845898931912848819996	#num#	k4
</s>
<s>
Společenský	společenský	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Samospráva	samospráva	k1gFnSc1
obce	obec	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
vyvěšuje	vyvěšovat	k5eAaImIp3nS
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
moravskou	moravský	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
13	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
působí	působit	k5eAaImIp3nS
v	v	k7c6
obci	obec	k1gFnSc6
spolek	spolek	k1gInSc4
Kobylnické	Kobylnický	k2eAgNnSc4d1
impérium	impérium	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Zvonice	zvonice	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Sněžné	sněžný	k2eAgFnSc2d1
</s>
<s>
Boží	boží	k2eAgFnSc1d1
muka	muka	k1gFnSc1
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Zvonice	zvonice	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Sněžné	sněžný	k2eAgFnSc2d1
na	na	k7c6
návsi	náves	k1gFnSc6
</s>
<s>
Kaple	kaple	k1gFnSc1
postavená	postavený	k2eAgFnSc1d1
k	k	k7c3
60	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
vlády	vláda	k1gFnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
</s>
<s>
Kříž	kříž	k1gInSc1
datovaný	datovaný	k2eAgInSc1d1
1835	#num#	k4
</s>
<s>
Bývalý	bývalý	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2021	.2021	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
SLAVÍK	Slavík	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
A.	A.	kA
Brněnský	brněnský	k2eAgInSc1d1
okres	okres	k1gInSc1
Vlastivěda	vlastivěda	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
GARN	GARN	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
392	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86347	#num#	k4
<g/>
-	-	kIx~
<g/>
53	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
142	#num#	k4
<g/>
-	-	kIx~
<g/>
143	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
:	:	kIx,
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abecední	abecední	k2eAgInSc1d1
přehled	přehled	k1gInSc1
obcí	obec	k1gFnPc2
a	a	k8xC
částí	část	k1gFnPc2
obcí	obec	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2015-12-21	2015-12-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SLAVÍK	Slavík	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
A.	A.	kA
Brněnský	brněnský	k2eAgInSc1d1
okres	okres	k1gInSc1
Vlastivěda	vlastivěda	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
GARN	GARN	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
392	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86347	#num#	k4
<g/>
-	-	kIx~
<g/>
53	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
141	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
:	:	kIx,
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
domů	dům	k1gInPc2
podle	podle	k7c2
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
částí	část	k1gFnPc2
obcí	obec	k1gFnPc2
a	a	k8xC
historických	historický	k2eAgFnPc2d1
osad	osada	k1gFnPc2
/	/	kIx~
lokalit	lokalita	k1gFnPc2
v	v	k7c6
letech	léto	k1gNnPc6
1869	#num#	k4
-	-	kIx~
2011	#num#	k4
:	:	kIx,
Okres	okres	k1gInSc1
Brno-venkov	Brno-venkov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2015-12-21	2015-12-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://www.facebook.com/permalink.php?story_fbid=10154086033900661&	https://www.facebook.com/permalink.php?story_fbid=10154086033900661&	k?
<g/>
↑	↑	k?
Kobylnické	Kobylnický	k2eAgNnSc4d1
impérium	impérium	k1gNnSc4
<g/>
.	.	kIx.
www.kobylnickeimperium.cz	www.kobylnickeimperium.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kobylnice	Kobylnice	k1gFnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kobylnice	Kobylnice	k1gFnPc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
<g/>
,	,	kIx,
městyse	městys	k1gInSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Brno-venkov	Brno-venkov	k1gInSc1
</s>
<s>
Babice	babice	k1gFnSc1
nad	nad	k7c7
Svitavou	Svitava	k1gFnSc7
•	•	k?
Babice	babice	k1gFnSc2
u	u	k7c2
Rosic	Rosice	k1gFnPc2
•	•	k?
Běleč	Běleč	k1gMnSc1
•	•	k?
Bílovice	Bílovice	k1gInPc1
nad	nad	k7c7
Svitavou	Svitava	k1gFnSc7
•	•	k?
Biskoupky	Biskoupka	k1gFnSc2
•	•	k?
Blažovice	Blažovice	k1gFnSc2
•	•	k?
Blučina	Blučina	k1gFnSc1
•	•	k?
Borač	Borač	k1gInSc1
•	•	k?
Borovník	borovník	k1gInSc1
•	•	k?
Braníškov	Braníškov	k1gInSc1
•	•	k?
Branišovice	Branišovice	k1gFnSc2
•	•	k?
Bratčice	Bratčice	k1gFnSc2
•	•	k?
Brumov	Brumov	k1gInSc1
•	•	k?
Březina	Březina	k1gFnSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
okres	okres	k1gInSc1
Blansko	Blansko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Březina	Březina	k1gMnSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
okres	okres	k1gInSc1
Tišnov	Tišnov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Bukovice	Bukovice	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Cvrčovice	Cvrčovice	k1gFnSc2
•	•	k?
Čebín	Čebín	k1gMnSc1
•	•	k?
Černvír	Černvír	k1gMnSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
•	•	k?
Čučice	Čučice	k1gFnSc1
•	•	k?
Deblín	Deblín	k1gInSc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Kounice	Kounice	k1gFnPc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Loučky	loučka	k1gFnPc1
•	•	k?
Domašov	Domašov	k1gInSc1
•	•	k?
Doubravník	Doubravník	k1gInSc1
•	•	k?
Drahonín	Drahonín	k1gInSc1
•	•	k?
Drásov	Drásov	k1gInSc4
•	•	k?
Hajany	Hajany	k1gInPc4
•	•	k?
Heroltice	Heroltice	k1gFnSc2
•	•	k?
Hlína	hlína	k1gFnSc1
•	•	k?
Hluboké	hluboký	k2eAgInPc1d1
Dvory	Dvůr	k1gInPc1
•	•	k?
Holasice	Holasice	k1gFnSc2
•	•	k?
Horní	horní	k2eAgFnSc2d1
Loučky	loučka	k1gFnSc2
•	•	k?
Hostěnice	hostěnice	k1gFnSc1
•	•	k?
Hradčany	Hradčany	k1gInPc1
•	•	k?
Hrušovany	Hrušovany	k1gInPc4
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Hvozdec	Hvozdec	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Chudčice	Chudčice	k1gFnSc2
•	•	k?
Ivaň	Ivaň	k1gFnSc1
•	•	k?
Ivančice	Ivančice	k1gFnPc4
•	•	k?
Javůrek	Javůrek	k1gMnSc1
•	•	k?
Jinačovice	Jinačovice	k1gFnSc2
•	•	k?
Jiříkovice	Jiříkovice	k1gFnSc2
•	•	k?
Kaly	kala	k1gFnSc2
•	•	k?
Kanice	Kanice	k1gFnPc4
•	•	k?
Katov	Katov	k1gInSc1
•	•	k?
Ketkovice	Ketkovice	k1gFnSc2
•	•	k?
Kobylnice	Kobylnice	k1gFnPc4
•	•	k?
Kovalovice	Kovalovice	k1gFnPc4
•	•	k?
Kratochvilka	Kratochvilka	k1gFnSc1
•	•	k?
Křižínkov	Křižínkov	k1gInSc1
•	•	k?
Kupařovice	Kupařovice	k1gFnSc2
•	•	k?
Kuřim	Kuřim	k1gMnSc1
•	•	k?
Kuřimská	kuřimský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Kuřimské	kuřimský	k2eAgNnSc4d1
Jestřabí	Jestřabí	k1gNnSc4
•	•	k?
Lažánky	Lažánka	k1gFnSc2
•	•	k?
Ledce	Ledce	k1gFnSc2
•	•	k?
Lelekovice	Lelekovice	k1gFnSc2
•	•	k?
Lesní	lesní	k2eAgFnPc1d1
Hluboké	Hluboká	k1gFnPc1
•	•	k?
Litostrov	Litostrov	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Loděnice	loděnice	k1gFnSc2
•	•	k?
Lomnice	Lomnice	k1gFnSc2
•	•	k?
Lomnička	Lomnička	k1gFnSc1
•	•	k?
Lubné	Lubná	k1gFnSc2
•	•	k?
Lukovany	Lukovan	k1gMnPc4
•	•	k?
Malešovice	Malešovice	k1gFnSc1
•	•	k?
Malhostovice	Malhostovice	k1gFnSc2
•	•	k?
Maršov	Maršov	k1gInSc1
•	•	k?
Medlov	Medlov	k1gInSc1
•	•	k?
Mělčany	Mělčan	k1gMnPc4
•	•	k?
Měnín	Měnín	k1gInSc1
•	•	k?
Modřice	Modřice	k1gFnSc2
•	•	k?
Mokrá-Horákov	Mokrá-Horákov	k1gInSc1
•	•	k?
Moravany	Moravan	k1gMnPc4
•	•	k?
Moravské	moravský	k2eAgFnSc2d1
Bránice	bránice	k1gFnSc2
•	•	k?
Moravské	moravský	k2eAgFnSc2d1
Knínice	Knínice	k1gFnSc2
•	•	k?
Moutnice	Moutnice	k1gFnSc2
•	•	k?
Nebovidy	Nebovida	k1gFnSc2
•	•	k?
Nedvědice	Nedvědice	k1gFnSc2
•	•	k?
Nelepeč-Žernůvka	Nelepeč-Žernůvka	k1gFnSc1
•	•	k?
Němčičky	Němčička	k1gFnSc2
•	•	k?
Neslovice	Neslovice	k1gFnSc2
•	•	k?
Nesvačilka	Nesvačilka	k1gFnSc1
•	•	k?
Níhov	Níhov	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Nosislav	Nosislav	k1gMnSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Nové	Nová	k1gFnSc2
Bránice	bránice	k1gFnSc2
•	•	k?
Odrovice	Odrovice	k1gFnSc2
•	•	k?
Ochoz	ochoz	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Ochoz	ochoz	k1gInSc1
u	u	k7c2
Tišnova	Tišnov	k1gInSc2
•	•	k?
Olší	olše	k1gFnPc2
•	•	k?
Omice	Omice	k1gMnSc1
•	•	k?
Opatovice	Opatovice	k1gFnSc2
•	•	k?
Ořechov	Ořechov	k1gInSc1
•	•	k?
Osiky	osika	k1gFnSc2
•	•	k?
Oslavany	Oslavany	k1gInPc4
•	•	k?
Ostopovice	Ostopovice	k1gFnSc2
•	•	k?
Ostrovačice	Ostrovačice	k1gFnSc2
•	•	k?
Otmarov	Otmarov	k1gInSc1
•	•	k?
Pasohlávky	Pasohlávka	k1gFnSc2
•	•	k?
Pernštejnské	pernštejnský	k2eAgNnSc4d1
Jestřabí	Jestřabí	k1gNnSc4
•	•	k?
Podolí	Podolí	k1gNnSc2
•	•	k?
Pohořelice	Pohořelice	k1gFnPc4
•	•	k?
Ponětovice	Ponětovice	k1gFnPc4
•	•	k?
Popovice	Popovice	k1gFnPc4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Popůvky	Popůvka	k1gFnSc2
•	•	k?
Pozořice	Pozořice	k1gFnSc2
•	•	k?
Prace	Prace	k1gFnSc2
•	•	k?
Pravlov	Pravlov	k1gInSc1
•	•	k?
Prštice	Prštice	k1gFnSc1
•	•	k?
Předklášteří	Předklášteří	k1gNnPc2
•	•	k?
Přibice	Přibice	k1gFnSc2
•	•	k?
Příbram	Příbram	k1gFnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
•	•	k?
Přibyslavice	Přibyslavice	k1gFnSc2
•	•	k?
Přísnotice	Přísnotice	k1gFnSc2
•	•	k?
Radostice	Radostika	k1gFnSc3
•	•	k?
Rajhrad	Rajhrad	k1gInSc1
•	•	k?
Rajhradice	Rajhradice	k1gFnSc2
•	•	k?
Rašov	Rašov	k1gInSc1
•	•	k?
Rebešovice	Rebešovice	k1gFnSc2
•	•	k?
Rohozec	Rohozec	k1gMnSc1
•	•	k?
Rojetín	Rojetín	k1gMnSc1
•	•	k?
Rosice	Rosice	k1gFnPc4
•	•	k?
Rozdrojovice	Rozdrojovice	k1gFnPc4
•	•	k?
Rudka	rudka	k1gFnSc1
•	•	k?
Řícmanice	Řícmanice	k1gFnSc1
•	•	k?
Říčany	Říčany	k1gInPc1
•	•	k?
Říčky	říčka	k1gFnSc2
•	•	k?
Řikonín	Řikonín	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Senorady	Senorada	k1gFnSc2
•	•	k?
Sentice	Sentice	k1gFnSc2
•	•	k?
Silůvky	Silůvka	k1gFnSc2
•	•	k?
Sivice	Sivice	k1gFnSc2
•	•	k?
Skalička	Skalička	k1gMnSc1
•	•	k?
Skryje	skrýt	k5eAaPmIp3nS
•	•	k?
Sobotovice	Sobotovice	k1gFnSc1
•	•	k?
Sokolnice	sokolnice	k1gFnSc1
•	•	k?
Stanoviště	stanoviště	k1gNnSc1
•	•	k?
Strhaře	strhař	k1gMnSc2
•	•	k?
Střelice	střelice	k1gFnSc2
•	•	k?
Svatoslav	Svatoslav	k1gMnSc1
•	•	k?
Synalov	Synalov	k1gInSc1
•	•	k?
Syrovice	syrovice	k1gFnSc2
•	•	k?
Šerkovice	Šerkovice	k1gFnSc2
•	•	k?
Šlapanice	Šlapanice	k1gFnSc2
•	•	k?
Štěpánovice	Štěpánovice	k1gFnSc2
•	•	k?
Šumice	Šumice	k1gFnSc2
•	•	k?
Telnice	Telnice	k1gFnSc2
•	•	k?
Těšany	Těšana	k1gFnSc2
•	•	k?
Tetčice	Tetčice	k1gFnSc2
•	•	k?
Tišnov	Tišnov	k1gInSc1
•	•	k?
Tišnovská	tišnovský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Trboušany	Trboušana	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Troskotovice	Troskotovice	k1gFnSc1
•	•	k?
Troubsko	Troubsko	k1gNnSc1
•	•	k?
Tvarožná	Tvarožný	k2eAgFnSc1d1
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Rosic	Rosice	k1gFnPc2
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Tišnova	Tišnov	k1gInSc2
•	•	k?
Unín	Unín	k1gMnSc1
•	•	k?
Unkovice	Unkovice	k1gFnSc2
•	•	k?
Úsuší	Úsuší	k2eAgFnSc1d1
•	•	k?
Velatice	Velatice	k1gFnSc1
•	•	k?
Veverská	Veverský	k2eAgFnSc1d1
Bítýška	Bítýška	k1gFnSc1
•	•	k?
Veverské	Veverský	k2eAgFnSc2d1
Knínice	Knínice	k1gFnSc2
•	•	k?
Viničné	Viničný	k2eAgFnSc2d1
Šumice	Šumice	k1gFnSc2
•	•	k?
Vlasatice	vlasatice	k1gFnSc2
•	•	k?
Vohančice	Vohančice	k1gFnSc2
•	•	k?
Vojkovice	Vojkovice	k1gFnSc2
•	•	k?
Vranov	Vranov	k1gInSc1
•	•	k?
Vranovice	Vranovice	k1gFnSc2
•	•	k?
Vratislávka	Vratislávka	k1gFnSc1
•	•	k?
Všechovice	Všechovice	k1gFnSc2
•	•	k?
Vysoké	vysoký	k2eAgFnPc1d1
Popovice	Popovice	k1gFnPc1
•	•	k?
Zakřany	Zakřan	k1gMnPc4
•	•	k?
Zálesná	Zálesný	k2eAgFnSc1d1
Zhoř	Zhoř	k1gFnSc1
•	•	k?
Zastávka	zastávka	k1gFnSc1
•	•	k?
Zbraslav	Zbraslav	k1gFnSc1
•	•	k?
Zbýšov	Zbýšov	k1gInSc1
•	•	k?
Zhoř	Zhoř	k1gInSc1
•	•	k?
Žabčice	Žabčice	k1gFnSc2
•	•	k?
Žatčany	Žatčan	k1gMnPc4
•	•	k?
Žďárec	Žďárec	k1gInSc1
•	•	k?
Želešice	Želešice	k1gFnSc1
•	•	k?
Železné	železný	k2eAgFnPc4d1
•	•	k?
Židlochovice	Židlochovice	k1gFnPc4
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
</s>
