<s>
Radhošť	Radhošť	k1gFnSc1	Radhošť
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Moravskoslezských	moravskoslezský	k2eAgInPc6d1	moravskoslezský
Beskydech	Beskyd	k1gInPc6	Beskyd
na	na	k7c6	na
závěru	závěr	k1gInSc6	závěr
výrazného	výrazný	k2eAgInSc2d1	výrazný
Pustevenského	Pustevenský	k2eAgInSc2d1	Pustevenský
hřbetu	hřbet	k1gInSc2	hřbet
3	[number]	k4	3
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Trojanovic	Trojanovice	k1gFnPc2	Trojanovice
a	a	k8xC	a
6	[number]	k4	6
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Rožnova	Rožnov	k1gInSc2	Rožnov
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
<g/>
.	.	kIx.	.
</s>
