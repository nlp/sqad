<s>
Radhošť	Radhošť	k1gFnSc1	Radhošť
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Moravskoslezských	moravskoslezský	k2eAgInPc6d1	moravskoslezský
Beskydech	Beskyd	k1gInPc6	Beskyd
na	na	k7c6	na
závěru	závěr	k1gInSc6	závěr
výrazného	výrazný	k2eAgInSc2d1	výrazný
Pustevenského	Pustevenský	k2eAgInSc2d1	Pustevenský
hřbetu	hřbet	k1gInSc2	hřbet
3	[number]	k4	3
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Trojanovic	Trojanovice	k1gFnPc2	Trojanovice
a	a	k8xC	a
6	[number]	k4	6
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Rožnova	Rožnov	k1gInSc2	Rožnov
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výškou	výška	k1gFnSc7	výška
1129	[number]	k4	1129
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sedmou	sedmý	k4xOgFnSc4	sedmý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
Moravskoslezských	moravskoslezský	k2eAgFnPc2d1	Moravskoslezská
Beskyd	Beskydy	k1gFnPc2	Beskydy
a	a	k8xC	a
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
nejzápadnější	západní	k2eAgFnSc4d3	nejzápadnější
tisícovku	tisícovka	k1gFnSc4	tisícovka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověstí	pověst	k1gFnPc2	pověst
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
Radhošti	Radhošť	k1gInSc6	Radhošť
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
sídlo	sídlo	k1gNnSc1	sídlo
slovanský	slovanský	k2eAgMnSc1d1	slovanský
bůh	bůh	k1gMnSc1	bůh
Radegast	Radegast	k1gMnSc1	Radegast
-	-	kIx~	-
Bůh	bůh	k1gMnSc1	bůh
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
války	válka	k1gFnSc2	válka
a	a	k8xC	a
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
podoba	podoba	k1gFnSc1	podoba
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
ztvárňuje	ztvárňovat	k5eAaImIp3nS	ztvárňovat
socha	socha	k1gFnSc1	socha
od	od	k7c2	od
Albína	Albín	k1gMnSc2	Albín
Poláška	Polášek	k1gMnSc2	Polášek
<g/>
,	,	kIx,	,
nepůsobí	působit	k5eNaImIp3nS	působit
zrovna	zrovna	k6eAd1	zrovna
mile	mile	k6eAd1	mile
a	a	k8xC	a
rozhněvat	rozhněvat	k5eAaPmF	rozhněvat
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
by	by	kYmCp3nS	by
asi	asi	k9	asi
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
nemuselo	muset	k5eNaImAgNnS	muset
být	být	k5eAaImF	být
příznivé	příznivý	k2eAgNnSc1d1	příznivé
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
anebo	anebo	k8xC	anebo
právě	právě	k9	právě
proto	proto	k8xC	proto
Radegasta	Radegast	k1gMnSc4	Radegast
generace	generace	k1gFnSc2	generace
lidí	člověk	k1gMnPc2	člověk
milovaly	milovat	k5eAaImAgInP	milovat
i	i	k8xC	i
zatracovaly	zatracovat	k5eAaImAgInP	zatracovat
<g/>
.	.	kIx.	.
</s>
<s>
Naši	náš	k3xOp1gMnPc1	náš
předkové	předek	k1gMnPc1	předek
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
měli	mít	k5eAaImAgMnP	mít
úctu	úcta	k1gFnSc4	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
přicházeli	přicházet	k5eAaImAgMnP	přicházet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
přinesli	přinést	k5eAaPmAgMnP	přinést
dary	dar	k1gInPc4	dar
-	-	kIx~	-
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
úrody	úroda	k1gFnSc2	úroda
<g/>
,	,	kIx,	,
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ulovili	ulovit	k5eAaPmAgMnP	ulovit
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
jara	jaro	k1gNnSc2	jaro
pak	pak	k6eAd1	pak
na	na	k7c6	na
Radhošti	Radhošť	k1gInSc6	Radhošť
staří	starý	k2eAgMnPc1d1	starý
Slované	Slovan	k1gMnPc1	Slovan
slavili	slavit	k5eAaImAgMnP	slavit
letní	letní	k2eAgInSc4d1	letní
slunovrat	slunovrat	k1gInSc4	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
nocích	noc	k1gFnPc6	noc
se	se	k3xPyFc4	se
rozzářily	rozzářit	k5eAaPmAgFnP	rozzářit
vatry	vatra	k1gFnPc1	vatra
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
tančili	tančit	k5eAaImAgMnP	tančit
a	a	k8xC	a
zpívali	zpívat	k5eAaImAgMnP	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Pohanské	pohanský	k2eAgInPc1d1	pohanský
zvyky	zvyk	k1gInPc1	zvyk
přetrvaly	přetrvat	k5eAaPmAgInP	přetrvat
i	i	k9	i
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
a	a	k8xC	a
nezabránila	zabránit	k5eNaPmAgFnS	zabránit
tomu	ten	k3xDgNnSc3	ten
ani	ani	k9	ani
pověst	pověst	k1gFnSc1	pověst
<g/>
,	,	kIx,	,
že	že	k8xS	že
modlu	modla	k1gFnSc4	modla
Radegasta	Radegast	k1gMnSc2	Radegast
prý	prý	k9	prý
strhli	strhnout	k5eAaPmAgMnP	strhnout
Cyril	Cyril	k1gMnSc1	Cyril
s	s	k7c7	s
Metodějem	Metoděj	k1gMnSc7	Metoděj
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
postavili	postavit	k5eAaPmAgMnP	postavit
kříž	kříž	k1gInSc4	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
povídání	povídání	k1gNnSc1	povídání
o	o	k7c6	o
Radegastově	Radegastův	k2eAgFnSc6d1	Radegastova
modle	modla	k1gFnSc6	modla
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
jakémsi	jakýsi	k3yIgInSc6	jakýsi
podzemním	podzemní	k2eAgInSc6d1	podzemní
radhošťském	radhošťský	k2eAgInSc6d1	radhošťský
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Zní	znět	k5eAaImIp3nS	znět
to	ten	k3xDgNnSc1	ten
sice	sice	k8xC	sice
jako	jako	k9	jako
nějaká	nějaký	k3yIgFnSc1	nějaký
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vzít	vzít	k5eAaPmF	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
nedalekých	daleký	k2eNgFnPc6d1	nedaleká
Pustevnách	Pustevna	k1gFnPc6	Pustevna
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
téměř	téměř	k6eAd1	téměř
nepřístupná	přístupný	k2eNgFnSc1d1	nepřístupná
soustava	soustava	k1gFnSc1	soustava
tzv.	tzv.	kA	tzv.
pseudokrasových	pseudokrasův	k2eAgFnPc2d1	pseudokrasův
puklin	puklina	k1gFnPc2	puklina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vedly	vést	k5eAaImAgInP	vést
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
tři	tři	k4xCgInPc4	tři
vchody	vchod	k1gInPc4	vchod
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
projít	projít	k5eAaPmF	projít
z	z	k7c2	z
Pusteven	Pusteven	k2eAgInSc4d1	Pusteven
na	na	k7c4	na
Radhošť	Radhošť	k1gFnSc4	Radhošť
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
pověstí	pověst	k1gFnPc2	pověst
o	o	k7c4	o
Sirotku	sirotka	k1gFnSc4	sirotka
z	z	k7c2	z
Radhoště	Radhošť	k1gFnSc2	Radhošť
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
středě	středa	k1gFnSc6	středa
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
sochou	socha	k1gFnSc7	socha
Radegasta	Radegast	k1gMnSc2	Radegast
a	a	k8xC	a
vrcholem	vrchol	k1gInSc7	vrchol
Radhoště	Radhošť	k1gFnSc2	Radhošť
i	i	k8xC	i
tábor	tábor	k1gInSc1	tábor
vojáků	voják	k1gMnPc2	voják
sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
"	"	kIx"	"
<g/>
dočasně	dočasně	k6eAd1	dočasně
umístěných	umístěný	k2eAgInPc2d1	umístěný
na	na	k7c6	na
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vojska	vojsko	k1gNnPc1	vojsko
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Waršavské	Waršavský	k2eAgFnSc2d1	Waršavská
smlouvy	smlouva	k1gFnSc2	smlouva
vč.	vč.	k?	vč.
vojáků	voják	k1gMnPc2	voják
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Československa	Československo	k1gNnSc2	Československo
od	od	k7c2	od
r.	r.	kA	r.
1969	[number]	k4	1969
umístěna	umístit	k5eAaPmNgFnS	umístit
jako	jako	k9	jako
protiváha	protiváha	k1gFnSc1	protiváha
zvyšujícího	zvyšující	k2eAgMnSc2d1	zvyšující
se	se	k3xPyFc4	se
počtu	počet	k1gInSc2	počet
vojáků	voják	k1gMnPc2	voják
armády	armáda	k1gFnSc2	armáda
USA	USA	kA	USA
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgFnSc2d1	umístěná
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hranic	hranice	k1gFnPc2	hranice
na	na	k7c6	na
území	území	k1gNnSc6	území
Německé	německý	k2eAgFnSc2d1	německá
spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
od	od	k7c2	od
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnPc4	druhý
sv.	sv.	kA	sv.
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
pravidelně	pravidelně	k6eAd1	pravidelně
konají	konat	k5eAaImIp3nP	konat
poutě	pouť	k1gFnPc1	pouť
na	na	k7c6	na
Radhošti	Radhošť	k1gFnSc6	Radhošť
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
účastní	účastnit	k5eAaImIp3nS	účastnit
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInSc1	tisíc
poutníků	poutník	k1gMnPc2	poutník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
několik	několik	k4yIc4	několik
zajímavých	zajímavý	k2eAgNnPc2d1	zajímavé
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
kříž	kříž	k1gInSc1	kříž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
<g/>
,	,	kIx,	,
Kaple	kaple	k1gFnPc1	kaple
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc4	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc4	Metoděj
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
98	[number]	k4	98
<g/>
,	,	kIx,	,
sousoší	sousoší	k1gNnSc1	sousoší
obou	dva	k4xCgMnPc2	dva
věrozvěstů	věrozvěst	k1gMnPc2	věrozvěst
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
a	a	k8xC	a
televizní	televizní	k2eAgInSc1d1	televizní
vysílač	vysílač	k1gInSc1	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
je	být	k5eAaImIp3nS	být
nejvýše	vysoce	k6eAd3	vysoce
položený	položený	k2eAgInSc4d1	položený
chrám	chrám	k1gInSc4	chrám
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
je	být	k5eAaImIp3nS	být
bronzová	bronzový	k2eAgFnSc1d1	bronzová
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnSc4d1	připomínající
návštěvu	návštěva	k1gFnSc4	návštěva
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
300	[number]	k4	300
m	m	kA	m
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
stojí	stát	k5eAaImIp3nS	stát
hotel	hotel	k1gInSc4	hotel
Radegast	Radegast	k1gMnSc1	Radegast
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
hotelem	hotel	k1gInSc7	hotel
a	a	k8xC	a
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
služebna	služebna	k1gFnSc1	služebna
Horské	Horské	k2eAgFnSc2d1	Horské
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
pod	pod	k7c4	pod
vrchol	vrchol	k1gInSc4	vrchol
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
západu	západ	k1gInSc2	západ
lyžařský	lyžařský	k2eAgInSc4d1	lyžařský
vlek	vlek	k1gInSc4	vlek
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
geodetický	geodetický	k2eAgInSc4d1	geodetický
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Radhošť	Radhošť	k1gFnSc1	Radhošť
(	(	kIx(	(
<g/>
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Radhošti	Radhošť	k1gInSc6	Radhošť
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Radhošť	Radhošť	k1gFnSc1	Radhošť
je	být	k5eAaImIp3nS	být
nejsnáze	snadno	k6eAd3	snadno
přístupný	přístupný	k2eAgInSc1d1	přístupný
z	z	k7c2	z
Pusteven	Pusteven	k2eAgInSc4d1	Pusteven
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
vede	vést	k5eAaImIp3nS	vést
lanovka	lanovka	k1gFnSc1	lanovka
z	z	k7c2	z
Trojanovic	Trojanovice	k1gFnPc2	Trojanovice
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
také	také	k9	také
parkoviště	parkoviště	k1gNnSc4	parkoviště
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
vede	vést	k5eAaImIp3nS	vést
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
modře	modro	k6eAd1	modro
značená	značený	k2eAgFnSc1d1	značená
hřebenovka	hřebenovka	k1gFnSc1	hřebenovka
přes	přes	k7c4	přes
vrchol	vrchol	k1gInSc4	vrchol
Radegast	Radegast	k1gMnSc1	Radegast
a	a	k8xC	a
kolem	kolem	k7c2	kolem
sochy	socha	k1gFnSc2	socha
Radegasta	Radegast	k1gMnSc2	Radegast
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
(	(	kIx(	(
<g/>
4	[number]	k4	4
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejhodnotnější	hodnotný	k2eAgInSc4d3	nejhodnotnější
výstup	výstup	k1gInSc4	výstup
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Rožnova	Rožnov	k1gInSc2	Rožnov
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
po	po	k7c6	po
červené	červený	k2eAgFnSc6d1	červená
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
(	(	kIx(	(
<g/>
7	[number]	k4	7
km	km	kA	km
s	s	k7c7	s
převýšením	převýšení	k1gNnSc7	převýšení
kolem	kolem	k7c2	kolem
800	[number]	k4	800
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
jsou	být	k5eAaImIp3nP	být
viditelné	viditelný	k2eAgInPc1d1	viditelný
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
uspořádané	uspořádaný	k2eAgInPc4d1	uspořádaný
hřbety	hřbet	k1gInPc4	hřbet
Hostýnsko-vsetínské	hostýnskosetínský	k2eAgFnSc2d1	hostýnsko-vsetínský
hornatiny	hornatina	k1gFnSc2	hornatina
<g/>
,	,	kIx,	,
Javorníků	Javorník	k1gInPc2	Javorník
a	a	k8xC	a
Bílých	bílý	k2eAgInPc2d1	bílý
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Východnímu	východní	k2eAgInSc3d1	východní
obzoru	obzor	k1gInSc3	obzor
dominují	dominovat	k5eAaImIp3nP	dominovat
beskydské	beskydský	k2eAgInPc4d1	beskydský
vrcholy	vrchol	k1gInPc4	vrchol
Lysá	lysat	k5eAaImIp3nS	lysat
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Smrk	smrk	k1gInSc1	smrk
<g/>
,	,	kIx,	,
Kněhyně	kněhyně	k1gFnSc1	kněhyně
a	a	k8xC	a
Čertův	čertův	k2eAgInSc1d1	čertův
mlýn	mlýn	k1gInSc1	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dálce	dálka	k1gFnSc6	dálka
jsou	být	k5eAaImIp3nP	být
viditelné	viditelný	k2eAgFnPc1d1	viditelná
Strážovské	Strážovská	k1gFnPc1	Strážovská
vrchy	vrch	k1gInPc4	vrch
<g/>
,	,	kIx,	,
masív	masív	k1gInSc1	masív
Malé	Malé	k2eAgFnSc2d1	Malé
Fatry	Fatra	k1gFnSc2	Fatra
a	a	k8xC	a
štíty	štít	k1gInPc4	štít
Tater	Tatra	k1gFnPc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severu	sever	k1gInSc3	sever
se	se	k3xPyFc4	se
otevírá	otevírat	k5eAaImIp3nS	otevírat
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Ostravskou	ostravský	k2eAgFnSc4d1	Ostravská
pánev	pánev	k1gFnSc4	pánev
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
částečně	částečně	k6eAd1	částečně
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
masív	masív	k1gInSc4	masív
Ondřejníku	Ondřejník	k1gInSc2	Ondřejník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
oblá	oblý	k2eAgFnSc1d1	oblá
kopule	kopule	k1gFnSc1	kopule
Velkého	velký	k2eAgInSc2d1	velký
Javorníku	Javorník	k1gInSc2	Javorník
<g/>
,	,	kIx,	,
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
v	v	k7c6	v
dálce	dálka	k1gFnSc6	dálka
Velký	velký	k2eAgInSc1d1	velký
Roudný	Roudný	k2eAgInSc1d1	Roudný
a	a	k8xC	a
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
Hrubý	hrubý	k2eAgInSc1d1	hrubý
Jeseník	Jeseník	k1gInSc1	Jeseník
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
Moravská	moravský	k2eAgFnSc1d1	Moravská
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
vyzdviženou	vyzdvižený	k2eAgFnSc7d1	vyzdvižená
plošinou	plošina	k1gFnSc7	plošina
Nízkého	nízký	k2eAgInSc2d1	nízký
Jeseníku	Jeseník	k1gInSc2	Jeseník
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výjimečně	výjimečně	k6eAd1	výjimečně
výborné	výborný	k2eAgFnPc4d1	výborná
dohlednosti	dohlednost	k1gFnPc4	dohlednost
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
i	i	k9	i
261	[number]	k4	261
km	km	kA	km
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
alpský	alpský	k2eAgInSc1d1	alpský
vrchol	vrchol	k1gInSc1	vrchol
Schneeberg	Schneeberg	k1gInSc1	Schneeberg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Radhošti	Radhošť	k1gFnSc6	Radhošť
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
vlaků	vlak	k1gInPc2	vlak
EuroCity	EuroCita	k1gFnSc2	EuroCita
společností	společnost	k1gFnPc2	společnost
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
ZSSK	ZSSK	kA	ZSSK
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Olomouc	Olomouc	k1gFnSc1	Olomouc
-	-	kIx~	-
Horní	horní	k2eAgInSc1d1	horní
Lideč	Lideč	k1gInSc1	Lideč
-	-	kIx~	-
Žilina	Žilina	k1gFnSc1	Žilina
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
MAUR	Maur	k1gMnSc1	Maur
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Paměť	paměť	k1gFnSc1	paměť
hor.	hor.	k?	hor.
Šumava	Šumava	k1gFnSc1	Šumava
-	-	kIx~	-
Říp	Říp	k1gInSc1	Říp
-	-	kIx~	-
Blaník	Blaník	k1gInSc1	Blaník
-	-	kIx~	-
Hostýn	Hostýn	k1gInSc1	Hostýn
-	-	kIx~	-
Radhošť	Radhošť	k1gInSc1	Radhošť
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Havran	Havran	k1gMnSc1	Havran
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
376	[number]	k4	376
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Krok	krok	k1gInSc1	krok
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86515	[number]	k4	86515
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Radegastova	Radegastův	k2eAgInSc2d1	Radegastův
kultu	kult	k1gInSc2	kult
k	k	k7c3	k
masové	masový	k2eAgFnSc3d1	masová
turistice	turistika	k1gFnSc3	turistika
<g/>
:	:	kIx,	:
Radhošť	Radhošť	k1gInSc1	Radhošť
<g/>
,	,	kIx,	,
s.	s.	k?	s.
313	[number]	k4	313
<g/>
-	-	kIx~	-
<g/>
342	[number]	k4	342
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Radhošť	Radhošť	k1gInSc4	Radhošť
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc1	Commons
Radhošť	Radhošť	k1gFnSc4	Radhošť
na	na	k7c6	na
VBeskydech	VBeskyd	k1gInPc6	VBeskyd
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Radhošť	Radhošť	k1gInSc1	Radhošť
na	na	k7c4	na
Frenstat	Frenstat	k1gInSc4	Frenstat
<g/>
.	.	kIx.	.
<g/>
info	info	k1gMnSc1	info
Radhošť	Radhošť	k1gMnSc1	Radhošť
na	na	k7c4	na
Tisicovky	Tisicovka	k1gFnPc4	Tisicovka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
