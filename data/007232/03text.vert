<s>
Jako	jako	k8xS	jako
otrava	otrava	k1gFnSc1	otrava
houbami	houba	k1gFnPc7	houba
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
otrava	otrava	k1gFnSc1	otrava
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
konzumace	konzumace	k1gFnSc2	konzumace
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
míní	mínit	k5eAaImIp3nS	mínit
velké	velký	k2eAgFnPc4d1	velká
houby	houba	k1gFnPc4	houba
záměrně	záměrně	k6eAd1	záměrně
konzumované	konzumovaný	k2eAgFnPc1d1	konzumovaná
<g/>
,	,	kIx,	,
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
však	však	k9	však
lze	lze	k6eAd1	lze
zahrnout	zahrnout	k5eAaPmF	zahrnout
i	i	k9	i
otravy	otrava	k1gFnPc4	otrava
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
nezáměrnou	záměrný	k2eNgFnSc7d1	nezáměrná
konzumací	konzumace	k1gFnSc7	konzumace
plísní	plíseň	k1gFnPc2	plíseň
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
menších	malý	k2eAgFnPc2d2	menší
hub	houba	k1gFnPc2	houba
při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
jimi	on	k3xPp3gFnPc7	on
zamořené	zamořený	k2eAgFnPc4d1	zamořená
stravy	strava	k1gFnSc2	strava
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
ergotismus	ergotismus	k1gInSc1	ergotismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
otrava	otrava	k1gFnSc1	otrava
z	z	k7c2	z
hub	houba	k1gFnPc2	houba
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
široká	široký	k2eAgFnSc1d1	široká
paleta	paleta	k1gFnSc1	paleta
příznaků	příznak	k1gInPc2	příznak
a	a	k8xC	a
následků	následek	k1gInPc2	následek
závisejících	závisející	k2eAgInPc2d1	závisející
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
požité	požitý	k2eAgFnSc2d1	požitá
houby	houba	k1gFnSc2	houba
<g/>
,	,	kIx,	,
okolnostech	okolnost	k1gFnPc6	okolnost
<g/>
,	,	kIx,	,
za	za	k7c2	za
kterých	který	k3yRgFnPc2	který
byla	být	k5eAaImAgFnS	být
sebrána	sebrán	k2eAgFnSc1d1	sebrána
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
této	tento	k3xDgFnSc2	tento
houby	houba	k1gFnSc2	houba
a	a	k8xC	a
způsobu	způsob	k1gInSc2	způsob
jakým	jaký	k3yIgInSc7	jaký
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
osobních	osobní	k2eAgFnPc2d1	osobní
dispozic	dispozice	k1gFnPc2	dispozice
konzumenta	konzument	k1gMnSc2	konzument
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
pouze	pouze	k6eAd1	pouze
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
zvracení	zvracení	k1gNnSc1	zvracení
(	(	kIx(	(
<g/>
po	po	k7c6	po
konzumaci	konzumace	k1gFnSc6	konzumace
nedostatečně	dostatečně	k6eNd1	dostatečně
tepelně	tepelně	k6eAd1	tepelně
upraveného	upravený	k2eAgMnSc4d1	upravený
satana	satan	k1gMnSc4	satan
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jemné	jemný	k2eAgNnSc4d1	jemné
a	a	k8xC	a
dočasné	dočasný	k2eAgNnSc4d1	dočasné
změny	změna	k1gFnPc4	změna
psychiky	psychika	k1gFnSc2	psychika
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
trvalá	trvalý	k2eAgFnSc1d1	trvalá
invalidita	invalidita	k1gFnSc1	invalidita
či	či	k8xC	či
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
při	při	k7c6	při
požití	požití	k1gNnSc6	požití
muchomůrky	muchomůrky	k?	muchomůrky
zelené	zelený	k2eAgInPc1d1	zelený
či	či	k8xC	či
pavučince	pavučinec	k1gInPc1	pavučinec
plyšového	plyšový	k2eAgInSc2d1	plyšový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
hub	houba	k1gFnPc2	houba
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
specifikovat	specifikovat	k5eAaBmF	specifikovat
množství	množství	k1gNnSc4	množství
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
už	už	k6eAd1	už
představuje	představovat	k5eAaImIp3nS	představovat
riziko	riziko	k1gNnSc4	riziko
–	–	k?	–
obsah	obsah	k1gInSc1	obsah
jedu	jet	k5eAaImIp1nS	jet
totiž	totiž	k9	totiž
zpravidla	zpravidla	k6eAd1	zpravidla
velice	velice	k6eAd1	velice
silně	silně	k6eAd1	silně
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
těžko	těžko	k6eAd1	těžko
zjistitelných	zjistitelný	k2eAgFnPc6d1	zjistitelná
okolnostech	okolnost	k1gFnPc6	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
sběr	sběr	k1gInSc1	sběr
hub	houba	k1gFnPc2	houba
velice	velice	k6eAd1	velice
populární	populární	k2eAgFnPc1d1	populární
a	a	k8xC	a
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
příležitostně	příležitostně	k6eAd1	příležitostně
praktikován	praktikován	k2eAgInSc1d1	praktikován
i	i	k8xC	i
naprostými	naprostý	k2eAgMnPc7d1	naprostý
amatéry	amatér	k1gMnPc7	amatér
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
řada	řada	k1gFnSc1	řada
velmi	velmi	k6eAd1	velmi
nebezpečných	bezpečný	k2eNgInPc2d1	nebezpečný
druhů	druh	k1gInPc2	druh
hub	houba	k1gFnPc2	houba
(	(	kIx(	(
<g/>
naštěstí	naštěstí	k6eAd1	naštěstí
relativně	relativně	k6eAd1	relativně
vzácných	vzácný	k2eAgInPc2d1	vzácný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
k	k	k7c3	k
smrtelným	smrtelný	k2eAgFnPc3d1	smrtelná
otravám	otrava	k1gFnPc3	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nejnebezpečnější	bezpečný	k2eNgMnSc1d3	nejnebezpečnější
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
jednoznačně	jednoznačně	k6eAd1	jednoznačně
profiluje	profilovat	k5eAaImIp3nS	profilovat
muchomůrka	muchomůrka	k?	muchomůrka
zelená	zelená	k1gFnSc1	zelená
<g/>
,	,	kIx,	,
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejjedovatějších	jedovatý	k2eAgFnPc2d3	nejjedovatější
a	a	k8xC	a
nejnebezpečnějších	bezpečný	k2eNgFnPc2d3	nejnebezpečnější
hub	houba	k1gFnPc2	houba
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gMnPc4	její
blízké	blízký	k2eAgMnPc4d1	blízký
příbuzné	příbuzný	k1gMnPc4	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gInPc2	on
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
další	další	k2eAgInPc4d1	další
druhy	druh	k1gInPc4	druh
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
ovšem	ovšem	k9	ovšem
vzácnější	vzácný	k2eAgFnPc1d2	vzácnější
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
rozličné	rozličný	k2eAgInPc4d1	rozličný
pavučince	pavučinec	k1gInPc4	pavučinec
(	(	kIx(	(
<g/>
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
pavučincem	pavučinec	k1gInSc7	pavučinec
plyšovým	plyšový	k2eAgInSc7d1	plyšový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vláknice	vláknice	k1gFnSc1	vláknice
(	(	kIx(	(
<g/>
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
vláknicí	vláknice	k1gFnSc7	vláknice
začervenalou	začervenalý	k2eAgFnSc7d1	začervenalá
<g/>
)	)	kIx)	)
a	a	k8xC	a
závojenky	závojenka	k1gFnSc2	závojenka
(	(	kIx(	(
<g/>
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
závojenkou	závojenka	k1gFnSc7	závojenka
olovovou	olovový	k2eAgFnSc7d1	olovová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
houby	houba	k1gFnPc1	houba
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
psychotické	psychotický	k2eAgInPc1d1	psychotický
stavy	stav	k1gInPc1	stav
a	a	k8xC	a
halucinace	halucinace	k1gFnPc1	halucinace
–	–	k?	–
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dobře	dobře	k6eAd1	dobře
známá	známý	k2eAgFnSc1d1	známá
lysohlávka	lysohlávka	k1gFnSc1	lysohlávka
česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
záměrně	záměrně	k6eAd1	záměrně
sbírány	sbírán	k2eAgMnPc4d1	sbírán
a	a	k8xC	a
konzumovány	konzumován	k2eAgMnPc4d1	konzumován
jakožto	jakožto	k8xS	jakožto
droga	droga	k1gFnSc1	droga
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
si	se	k3xPyFc3	se
ale	ale	k9	ale
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
hrozí	hrozit	k5eAaImIp3nS	hrozit
záměna	záměna	k1gFnSc1	záměna
se	s	k7c7	s
smrtelně	smrtelně	k6eAd1	smrtelně
jedovatými	jedovatý	k2eAgFnPc7d1	jedovatá
čepičatkami	čepičatka	k1gFnPc7	čepičatka
nebo	nebo	k8xC	nebo
předávkování	předávkování	k1gNnSc1	předávkování
<g/>
.	.	kIx.	.
</s>
<s>
Otrav	otrávit	k5eAaPmRp2nS	otrávit
nebo	nebo	k8xC	nebo
přiotrávení	přiotrávení	k1gNnSc1	přiotrávení
houbami	houba	k1gFnPc7	houba
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
300	[number]	k4	300
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
umírají	umírat	k5eAaImIp3nP	umírat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
úmrtí	úmrť	k1gFnSc7	úmrť
velmi	velmi	k6eAd1	velmi
sporadická	sporadický	k2eAgFnSc1d1	sporadická
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnPc1d3	nejčastější
jsou	být	k5eAaImIp3nP	být
otravy	otrava	k1gFnPc1	otrava
muchomůrkou	muchomůrkou	k?	muchomůrkou
zelenou	zelená	k1gFnSc7	zelená
a	a	k8xC	a
muchomůrkou	muchomůrkou	k?	muchomůrkou
panterovou	panterův	k2eAgFnSc7d1	panterova
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Otrava	otrava	k1gMnSc1	otrava
hřibem	hřib	k1gInSc7	hřib
satanem	satan	k1gInSc7	satan
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Otrava	otrava	k1gMnSc1	otrava
muchomůrkou	muchomůrkou	k?	muchomůrkou
zelenou	zelená	k1gFnSc4	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Těžké	těžký	k2eAgInPc1d1	těžký
kovy	kov	k1gInPc1	kov
v	v	k7c6	v
houbách	houba	k1gFnPc6	houba
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
klasických	klasický	k2eAgFnPc2d1	klasická
otrav	otrava	k1gFnPc2	otrava
požitím	požití	k1gNnSc7	požití
jedovaté	jedovatý	k2eAgFnSc2d1	jedovatá
houby	houba	k1gFnSc2	houba
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
otrava	otrava	k1gFnSc1	otrava
z	z	k7c2	z
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
primárně	primárně	k6eAd1	primárně
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přejaly	přejmout	k5eAaPmAgFnP	přejmout
nějakou	nějaký	k3yIgFnSc4	nějaký
jedovatou	jedovatý	k2eAgFnSc4d1	jedovatá
látku	látka	k1gFnSc4	látka
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
špatně	špatně	k6eAd1	špatně
prozkoumaná	prozkoumaný	k2eAgFnSc1d1	prozkoumaná
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
panuje	panovat	k5eAaImIp3nS	panovat
konsenzus	konsenzus	k1gInSc1	konsenzus
<g/>
,	,	kIx,	,
že	že	k8xS	že
sběr	sběr	k1gInSc1	sběr
a	a	k8xC	a
konzumace	konzumace	k1gFnSc1	konzumace
hub	houba	k1gFnPc2	houba
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zamořeny	zamořen	k2eAgFnPc1d1	zamořena
např.	např.	kA	např.
těžkými	těžký	k2eAgInPc7d1	těžký
kovy	kov	k1gInPc7	kov
nebo	nebo	k8xC	nebo
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
spadem	spad	k1gInSc7	spad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
a	a	k8xC	a
nerozumná	rozumný	k2eNgFnSc1d1	nerozumná
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
ojedinělých	ojedinělý	k2eAgInPc6d1	ojedinělý
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
o	o	k7c6	o
pravidelnějším	pravidelní	k2eAgInSc6d2	pravidelní
sběru	sběr	k1gInSc6	sběr
a	a	k8xC	a
konzumaci	konzumace	k1gFnSc3	konzumace
nemluvě	nemluva	k1gFnSc3	nemluva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
zásadně	zásadně	k6eAd1	zásadně
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
sběr	sběr	k1gInSc4	sběr
na	na	k7c6	na
výsypkách	výsypka	k1gFnPc6	výsypka
a	a	k8xC	a
skládkách	skládka	k1gFnPc6	skládka
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
cest	cesta	k1gFnPc2	cesta
s	s	k7c7	s
rušnou	rušný	k2eAgFnSc7d1	rušná
automobilovou	automobilový	k2eAgFnSc7d1	automobilová
dopravou	doprava	k1gFnSc7	doprava
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
na	na	k7c6	na
radioaktivních	radioaktivní	k2eAgFnPc6d1	radioaktivní
haldách	halda	k1gFnPc6	halda
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jáchymovských	jáchymovský	k2eAgInPc2d1	jáchymovský
uranových	uranový	k2eAgInPc2d1	uranový
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Konzervace	konzervace	k1gFnSc2	konzervace
a	a	k8xC	a
uchovávání	uchovávání	k1gNnSc4	uchovávání
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
otrávit	otrávit	k5eAaPmF	otrávit
nejedovatými	jedovatý	k2eNgFnPc7d1	nejedovatá
houbami	houba	k1gFnPc7	houba
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
otrava	otrava	k1gFnSc1	otrava
ze	z	k7c2	z
zkažených	zkažený	k2eAgFnPc2d1	zkažená
hub	houba	k1gFnPc2	houba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zapařených	zapařený	k2eAgInPc6d1	zapařený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otravu	otrava	k1gFnSc4	otrava
pak	pak	k6eAd1	pak
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jed	jed	k1gInSc1	jed
produkovaný	produkovaný	k2eAgInSc1d1	produkovaný
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
a	a	k8xC	a
plísněmi	plíseň	k1gFnPc7	plíseň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
nevhodně	vhodně	k6eNd1	vhodně
skladovaných	skladovaný	k2eAgFnPc6d1	skladovaná
houbách	houba	k1gFnPc6	houba
a	a	k8xC	a
houbovitých	houbovitý	k2eAgInPc6d1	houbovitý
pokrmech	pokrm	k1gInPc6	pokrm
namnožily	namnožit	k5eAaPmAgFnP	namnožit
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
jako	jako	k8xC	jako
u	u	k7c2	u
otravy	otrava	k1gFnSc2	otrava
ze	z	k7c2	z
zkaženého	zkažený	k2eAgNnSc2d1	zkažené
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
obecných	obecný	k2eAgFnPc2d1	obecná
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
člověku	člověk	k1gMnSc3	člověk
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyhne	vyhnout	k5eAaPmIp3nS	vyhnout
otravě	otrava	k1gFnSc3	otrava
houbami	houba	k1gFnPc7	houba
<g/>
.	.	kIx.	.
</s>
<s>
Sbírám	sbírat	k5eAaImIp1nS	sbírat
pouze	pouze	k6eAd1	pouze
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
dobře	dobře	k6eAd1	dobře
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nejsem	být	k5eNaImIp1nS	být
skutečný	skutečný	k2eAgMnSc1d1	skutečný
znalec	znalec	k1gMnSc1	znalec
<g/>
,	,	kIx,	,
vyhýbám	vyhýbat	k5eAaImIp1nS	vyhýbat
se	se	k3xPyFc4	se
složitým	složitý	k2eAgInSc7d1	složitý
rodům	rod	k1gInPc3	rod
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
pavučince	pavučinec	k1gInPc4	pavučinec
a	a	k8xC	a
všemu	všecek	k3xTgNnSc3	všecek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrozí	hrozit	k5eAaImIp3nS	hrozit
riziko	riziko	k1gNnSc4	riziko
záměny	záměna	k1gFnSc2	záměna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sebemenší	sebemenší	k2eAgFnSc6d1	sebemenší
pochybnosti	pochybnost	k1gFnSc6	pochybnost
nechám	nechat	k5eAaPmIp1nS	nechat
houbu	houba	k1gFnSc4	houba
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
</s>
<s>
Vyhýbám	vyhýbat	k5eAaImIp1nS	vyhýbat
se	se	k3xPyFc4	se
oblastem	oblast	k1gFnPc3	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
v	v	k7c6	v
houbách	houba	k1gFnPc6	houba
koncentrovat	koncentrovat	k5eAaBmF	koncentrovat
nebezpečné	bezpečný	k2eNgInPc4d1	nebezpečný
kontaminanty	kontaminant	k1gInPc4	kontaminant
(	(	kIx(	(
<g/>
okraje	okraj	k1gInPc4	okraj
cest	cesta	k1gFnPc2	cesta
s	s	k7c7	s
rušnou	rušný	k2eAgFnSc7d1	rušná
automobilovou	automobilový	k2eAgFnSc7d1	automobilová
dopravou	doprava	k1gFnSc7	doprava
<g/>
,	,	kIx,	,
skládky	skládka	k1gFnPc1	skládka
<g/>
,	,	kIx,	,
všelijak	všelijak	k6eAd1	všelijak
zamořená	zamořený	k2eAgNnPc1d1	zamořené
území	území	k1gNnPc1	území
<g/>
)	)	kIx)	)
Nesbírám	sbírat	k5eNaImIp1nS	sbírat
příliš	příliš	k6eAd1	příliš
staré	starý	k2eAgFnPc4d1	stará
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
už	už	k6eAd1	už
začaly	začít	k5eAaPmAgInP	začít
hnilobné	hnilobný	k2eAgInPc1d1	hnilobný
procesy	proces	k1gInPc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
houby	houba	k1gFnPc4	houba
přemrzlé	přemrzlý	k2eAgFnPc4d1	přemrzlá
<g/>
.	.	kIx.	.
</s>
<s>
Nesbírám	sbírat	k5eNaImIp1nS	sbírat
příliš	příliš	k6eAd1	příliš
mladé	mladý	k2eAgFnPc4d1	mladá
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
nejde	jít	k5eNaImIp3nS	jít
druh	druh	k1gInSc1	druh
bezpečně	bezpečně	k6eAd1	bezpečně
poznat	poznat	k5eAaPmF	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
pověr	pověra	k1gFnPc2	pověra
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
si	se	k3xPyFc3	se
amatéři	amatér	k1gMnPc1	amatér
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
a	a	k8xC	a
podle	podle	k7c2	podle
kterých	který	k3yRgMnPc2	který
houby	houby	k6eAd1	houby
sbírají	sbírat	k5eAaImIp3nP	sbírat
v	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
nehrozí	hrozit	k5eNaImIp3nS	hrozit
žádné	žádný	k3yNgNnSc4	žádný
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
jedovatých	jedovatý	k2eAgFnPc2d1	jedovatá
hub	houba	k1gFnPc2	houba
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vzácných	vzácný	k2eAgMnPc2d1	vzácný
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
jim	on	k3xPp3gMnPc3	on
jejich	jejich	k3xOp3gFnPc4	jejich
pověry	pověra	k1gFnPc4	pověra
i	i	k9	i
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vycházet	vycházet	k5eAaImF	vycházet
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
získávat	získávat	k5eAaImF	získávat
punc	punc	k1gInSc4	punc
potvrzenosti	potvrzenost	k1gFnSc2	potvrzenost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
mylné	mylný	k2eAgFnPc1d1	mylná
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
vyvrácení	vyvrácení	k1gNnSc2	vyvrácení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nečekané	čekaný	k2eNgInPc4d1	nečekaný
a	a	k8xC	a
tragické	tragický	k2eAgInPc4d1	tragický
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
seznam	seznam	k1gInSc1	seznam
určitě	určitě	k6eAd1	určitě
není	být	k5eNaImIp3nS	být
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Pověra	pověra	k1gFnSc1	pověra
<g/>
:	:	kIx,	:
Jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
houby	houba	k1gFnPc1	houba
jsou	být	k5eAaImIp3nP	být
pestře	pestro	k6eAd1	pestro
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
<g/>
.	.	kIx.	.
–	–	k?	–
Není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
smrtelně	smrtelně	k6eAd1	smrtelně
jedovatých	jedovatý	k2eAgFnPc2d1	jedovatá
hub	houba	k1gFnPc2	houba
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
nenápadnou	nápadný	k2eNgFnSc4d1	nenápadná
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
muchomůrka	muchomůrka	k?	muchomůrka
zelená	zelený	k2eAgFnSc1d1	zelená
či	či	k8xC	či
muchomůrka	muchomůrka	k?	muchomůrka
jízlivá	jízlivý	k2eAgNnPc1d1	jízlivé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pověra	pověra	k1gFnSc1	pověra
<g/>
:	:	kIx,	:
Jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
houby	houba	k1gFnPc1	houba
nejsou	být	k5eNaImIp3nP	být
ožírané	ožíraný	k2eAgMnPc4d1	ožíraný
slimáky	slimák	k1gMnPc4	slimák
a	a	k8xC	a
hmyzími	hmyzí	k2eAgFnPc7d1	hmyzí
larvami	larva	k1gFnPc7	larva
<g/>
.	.	kIx.	.
–	–	k?	–
Není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
muchomůrka	muchomůrka	k?	muchomůrka
zelená	zelená	k1gFnSc1	zelená
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
hmyz	hmyz	k1gInSc4	hmyz
neškodná	škodný	k2eNgFnSc1d1	neškodná
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
larvy	larva	k1gFnPc1	larva
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
běžně	běžně	k6eAd1	běžně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Plži	plž	k1gMnPc1	plž
je	on	k3xPp3gInPc4	on
též	též	k9	též
hojně	hojně	k6eAd1	hojně
vyhledávaná	vyhledávaný	k2eAgFnSc1d1	vyhledávaná
<g/>
.	.	kIx.	.
</s>
<s>
Pověra	pověra	k1gFnSc1	pověra
<g/>
:	:	kIx,	:
Jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
houby	houba	k1gFnPc1	houba
černají	černat	k5eAaImIp3nP	černat
při	při	k7c6	při
styku	styk	k1gInSc6	styk
se	s	k7c7	s
stříbrným	stříbrný	k2eAgNnSc7d1	stříbrné
nádobím	nádobí	k1gNnSc7	nádobí
či	či	k8xC	či
cibulí	cibule	k1gFnSc7	cibule
<g/>
.	.	kIx.	.
–	–	k?	–
Není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
hub	houba	k1gFnPc2	houba
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
spíše	spíše	k9	spíše
při	při	k7c6	při
vyschnutí	vyschnutí	k1gNnSc6	vyschnutí
a	a	k8xC	a
zvadnutí	zvadnutí	k1gNnSc6	zvadnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
cibule	cibule	k1gFnSc2	cibule
nebo	nebo	k8xC	nebo
nádobí	nádobí	k1gNnSc2	nádobí
a	a	k8xC	a
už	už	k6eAd1	už
vůbec	vůbec	k9	vůbec
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
jedovaté	jedovatý	k2eAgInPc4d1	jedovatý
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Pověra	pověra	k1gFnSc1	pověra
<g/>
:	:	kIx,	:
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
převaření	převaření	k1gNnSc1	převaření
či	či	k8xC	či
smažení	smažení	k1gNnSc1	smažení
zlikviduje	zlikvidovat	k5eAaPmIp3nS	zlikvidovat
každý	každý	k3xTgInSc1	každý
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
–	–	k?	–
Není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgFnPc1	některý
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
houby	houba	k1gFnPc1	houba
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
úpravou	úprava	k1gFnSc7	úprava
jedovatost	jedovatost	k1gFnSc1	jedovatost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hřib	hřib	k1gInSc1	hřib
satan	satan	k1gInSc1	satan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většina	většina	k1gFnSc1	většina
toxinů	toxin	k1gInPc2	toxin
je	být	k5eAaImIp3nS	být
tepelně	tepelně	k6eAd1	tepelně
vysoce	vysoce	k6eAd1	vysoce
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
var	var	k1gInSc1	var
je	on	k3xPp3gNnSc4	on
neničí	ničit	k5eNaImIp3nS	ničit
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
smrtelně	smrtelně	k6eAd1	smrtelně
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
houby	houba	k1gFnPc1	houba
vyskytující	vyskytující	k2eAgFnPc1d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
si	se	k3xPyFc3	se
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
svoji	svůj	k3xOyFgFnSc4	svůj
jedovatost	jedovatost	k1gFnSc4	jedovatost
i	i	k9	i
po	po	k7c6	po
dlouhodobé	dlouhodobý	k2eAgFnSc6d1	dlouhodobá
tepelné	tepelný	k2eAgFnSc6d1	tepelná
úpravě	úprava	k1gFnSc6	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Pověra	pověra	k1gFnSc1	pověra
<g/>
:	:	kIx,	:
Když	když	k8xS	když
se	se	k3xPyFc4	se
spletu	splést	k5eAaPmIp1nS	splést
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
udělá	udělat	k5eAaPmIp3nS	udělat
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
nebudu	být	k5eNaImBp1nS	být
váhat	váhat	k5eAaImF	váhat
a	a	k8xC	a
nechám	nechat	k5eAaPmIp1nS	nechat
si	se	k3xPyFc3	se
vypláchnout	vypláchnout	k5eAaPmF	vypláchnout
žaludek	žaludek	k1gInSc4	žaludek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
nic	nic	k3yNnSc1	nic
hrozného	hrozný	k2eAgNnSc2d1	hrozné
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
.	.	kIx.	.
–	–	k?	–
Chybný	chybný	k2eAgInSc4d1	chybný
předpoklad	předpoklad	k1gInSc4	předpoklad
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
smrtelně	smrtelně	k6eAd1	smrtelně
jedovatých	jedovatý	k2eAgInPc2d1	jedovatý
druhů	druh	k1gInPc2	druh
hub	houba	k1gFnPc2	houba
je	být	k5eAaImIp3nS	být
zákeřná	zákeřný	k2eAgFnSc1d1	zákeřná
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
otrava	otrava	k1gFnSc1	otrava
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
až	až	k9	až
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
jed	jed	k1gInSc1	jed
vstřebán	vstřebán	k2eAgInSc1d1	vstřebán
a	a	k8xC	a
napáchal	napáchat	k5eAaBmAgInS	napáchat
nevratné	vratný	k2eNgFnPc4d1	nevratná
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
například	například	k6eAd1	například
u	u	k7c2	u
muchomůrky	muchomůrky	k?	muchomůrky
zelené	zelená	k1gFnSc2	zelená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
u	u	k7c2	u
pavučince	pavučinec	k1gInSc2	pavučinec
plyšového	plyšový	k2eAgInSc2d1	plyšový
–	–	k?	–
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
se	se	k3xPyFc4	se
první	první	k4xOgInPc1	první
příznaky	příznak	k1gInPc1	příznak
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
dokonce	dokonce	k9	dokonce
až	až	k6eAd1	až
po	po	k7c6	po
3	[number]	k4	3
týdnech	týden	k1gInPc6	týden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lékaři	lékař	k1gMnPc1	lékař
mohou	moct	k5eAaImIp3nP	moct
konstatovat	konstatovat	k5eAaBmF	konstatovat
už	už	k6eAd1	už
jen	jen	k9	jen
těžké	těžký	k2eAgNnSc1d1	těžké
poškození	poškození	k1gNnSc1	poškození
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
úplné	úplný	k2eAgNnSc1d1	úplné
a	a	k8xC	a
definitivní	definitivní	k2eAgNnSc1d1	definitivní
selhání	selhání	k1gNnSc1	selhání
<g/>
)	)	kIx)	)
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
trvalou	trvalý	k2eAgFnSc4d1	trvalá
invaliditu	invalidita	k1gFnSc4	invalidita
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Pověra	pověra	k1gFnSc1	pověra
<g/>
:	:	kIx,	:
Jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
houby	houba	k1gFnPc1	houba
chutnají	chutnat	k5eAaImIp3nP	chutnat
odporně	odporně	k6eAd1	odporně
<g/>
.	.	kIx.	.
–	–	k?	–
Není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
smrtelně	smrtelně	k6eAd1	smrtelně
jedovatých	jedovatý	k2eAgFnPc2d1	jedovatá
hub	houba	k1gFnPc2	houba
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
chuťově	chuťově	k6eAd1	chuťově
vynikající	vynikající	k2eAgFnSc1d1	vynikající
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
muchomůrka	muchomůrka	k?	muchomůrka
zelená	zelený	k2eAgFnSc1d1	zelená
a	a	k8xC	a
muchomůrka	muchomůrka	k?	muchomůrka
jízlivá	jízlivý	k2eAgFnSc1d1	jízlivá
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
chutné	chutný	k2eAgFnPc4d1	chutná
houby	houba	k1gFnPc4	houba
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Smotlacha	smotlacha	k1gMnSc1	smotlacha
<g/>
,	,	kIx,	,
legenda	legenda	k1gFnSc1	legenda
české	český	k2eAgFnSc2d1	Česká
mykologie	mykologie	k1gFnSc2	mykologie
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všechny	všechen	k3xTgFnPc1	všechen
houby	houba	k1gFnPc1	houba
opravdu	opravdu	k6eAd1	opravdu
jedovaté	jedovatý	k2eAgInPc1d1	jedovatý
mají	mít	k5eAaImIp3nP	mít
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
vůni	vůně	k1gFnSc4	vůně
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
nebo	nebo	k8xC	nebo
aspoň	aspoň	k9	aspoň
nenápadnou	nápadný	k2eNgFnSc4d1	nenápadná
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
i	i	k9	i
sympatický	sympatický	k2eAgInSc4d1	sympatický
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pověra	pověra	k1gFnSc1	pověra
<g/>
:	:	kIx,	:
Co	co	k3yQnSc1	co
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
pařezu	pařez	k1gInSc6	pařez
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jedlé	jedlý	k2eAgNnSc1d1	jedlé
<g/>
.	.	kIx.	.
–	–	k?	–
Není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
smrtelně	smrtelně	k6eAd1	smrtelně
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
čepičatka	čepičatka	k1gFnSc1	čepičatka
jehličnanová	jehličnanová	k1gFnSc1	jehličnanová
běžně	běžně	k6eAd1	běžně
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
padlých	padlý	k2eAgInPc2d1	padlý
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
pařezů	pařez	k1gInPc2	pařez
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Bastien	Bastino	k1gNnPc2	Bastino
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
otrava	otrava	k1gFnSc1	otrava
houbami	houba	k1gFnPc7	houba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Jedy	jed	k1gInPc1	jed
vyšších	vysoký	k2eAgFnPc2d2	vyšší
hub	houba	k1gFnPc2	houba
na	na	k7c4	na
www.biotox.cz	www.biotox.cz	k1gInSc4	www.biotox.cz
</s>
