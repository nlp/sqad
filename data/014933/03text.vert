<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Nové	Nové	k2eAgFnPc1d1
Valteřice	Valteřice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Mariev	Mariva	k1gFnPc2
Nových	Nových	k2eAgFnPc6d1
Valteřicích	Valteřice	k1gFnPc6
Kostel	kostel	k1gInSc4
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Nových	Nových	k2eAgInPc6d1
ValteřicíchMísto	ValteřicíchMísta	k1gMnSc5
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Bruntál	Bruntál	k1gInSc1
Obec	obec	k1gFnSc1
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
Valteřice	Valteřice	k1gFnPc1
<g/>
,	,	kIx,
Moravský	moravský	k2eAgInSc1d1
Beroun	Beroun	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
29,45	29,45	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
24,54	24,54	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
moravská	moravský	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
ostravsko-opavská	ostravsko-opavský	k2eAgFnSc1d1
Děkanát	děkanát	k1gInSc1
</s>
<s>
Bruntál	Bruntál	k1gInSc1
Farnost	farnost	k1gFnSc1
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
Beroun	Beroun	k1gInSc1
Status	status	k1gInSc1
</s>
<s>
farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Užívání	užívání	k1gNnSc1
</s>
<s>
užíván	užíván	k2eAgInSc1d1
Architektonický	architektonický	k2eAgInSc1d1
popis	popis	k1gInSc1
Stavební	stavební	k2eAgInSc1d1
sloh	sloh	k1gInSc4
</s>
<s>
baroko	baroko	k1gNnSc1
<g/>
,	,	kIx,
klasicismus	klasicismus	k1gInSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1815	#num#	k4
Další	další	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Kód	kód	k1gInSc4
památky	památka	k1gFnSc2
</s>
<s>
104899	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Nových	Nových	k2eAgInPc6d1
Valteřicích	Valteřik	k1gInPc6
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
nový	nový	k2eAgInSc1d1
kostel	kostel	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1815	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
kulturní	kulturní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Na	na	k7c6
místě	místo	k1gNnSc6
dnešního	dnešní	k2eAgInSc2d1
kostela	kostel	k1gInSc2
stával	stávat	k5eAaImAgInS
v	v	k7c6
obci	obec	k1gFnSc6
Nové	Nové	k2eAgFnSc2d1
Valteřice	Valteřice	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1961	#num#	k4
součást	součást	k1gFnSc1
Moravského	moravský	k2eAgInSc2d1
Berouna	Beroun	k1gInSc2
<g/>
)	)	kIx)
nejspíše	nejspíše	k9
původní	původní	k2eAgInSc4d1
gotický	gotický	k2eAgInSc4d1
kostel	kostel	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1710	#num#	k4
barokně	barokně	k6eAd1
upraven	upravit	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1789	#num#	k4
byla	být	k5eAaImAgFnS
ke	k	k7c3
kostelu	kostel	k1gInSc3
přistavěna	přistavěn	k2eAgFnSc1d1
fara	fara	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1811	#num#	k4
stržen	strhnout	k5eAaPmNgInS
a	a	k8xC
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
místě	místo	k1gNnSc6
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
dnešní	dnešní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nová	nový	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
kostela	kostel	k1gInSc2
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1815	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
kostelem	kostel	k1gInSc7
stával	stávat	k5eAaImAgInS
hřbitov	hřbitov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
přemístěn	přemístit	k5eAaPmNgInS
mimo	mimo	k7c4
obec	obec	k1gFnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
pozůstatek	pozůstatek	k1gInSc1
zde	zde	k6eAd1
zůstal	zůstat	k5eAaPmAgInS
stát	stát	k5eAaImF,k5eAaPmF
kamenný	kamenný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1858	#num#	k4
byla	být	k5eAaImAgFnS
dostavěna	dostavěn	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Kostel	kostel	k1gInSc1
dle	dle	k7c2
stavebního	stavební	k2eAgInSc2d1
řadit	řadit	k5eAaImF
k	k	k7c3
stavbám	stavba	k1gFnPc3
pozdního	pozdní	k2eAgNnSc2d1
baroka	baroko	k1gNnSc2
a	a	k8xC
navazující	navazující	k2eAgInSc4d1
raný	raný	k2eAgInSc4d1
klasicismus	klasicismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
jednolodní	jednolodní	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
s	s	k7c7
rovným	rovný	k2eAgInSc7d1
závěrem	závěr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c4
jednoosé	jednoosý	k2eAgNnSc4d1
průčelí	průčelí	k1gNnSc4
s	s	k7c7
věží	věž	k1gFnSc7
vystupující	vystupující	k2eAgFnSc2d1
rizalitové	rizalitový	k2eAgFnSc2d1
fasády	fasáda	k1gFnSc2
je	být	k5eAaImIp3nS
předsazeno	předsazen	k2eAgNnSc1d1
schodiště	schodiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
loď	loď	k1gFnSc4
navazuje	navazovat	k5eAaImIp3nS
odsazený	odsazený	k2eAgInSc1d1
presbytář	presbytář	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
přiléhá	přiléhat	k5eAaImIp3nS
sakristie	sakristie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
vystavěn	vystavět	k5eAaPmNgInS
na	na	k7c6
kamenné	kamenný	k2eAgFnSc6d1
podezdívce	podezdívka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyrovnává	vyrovnávat	k5eAaImIp3nS
nerovný	rovný	k2eNgInSc4d1
terén	terén	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdivo	zdivo	k1gNnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
cihel	cihla	k1gFnPc2
pálených	pálená	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1
zdí	zdít	k5eAaImIp3nP
včetně	včetně	k7c2
přístavku	přístavek	k1gInSc2
lemuje	lemovat	k5eAaImIp3nS
korunní	korunní	k2eAgFnSc1d1
římsa	římsa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
pokrývá	pokrývat	k5eAaImIp3nS
plechová	plechový	k2eAgFnSc1d1
valbová	valbový	k2eAgFnSc1d1
střecha	střecha	k1gFnSc1
ostatní	ostatní	k2eAgFnSc2d1
části	část	k1gFnSc2
kostela	kostel	k1gInSc2
mají	mít	k5eAaImIp3nP
polovalbovou	polovalbový	k2eAgFnSc4d1
střechu	střecha	k1gFnSc4
<g/>
,	,	kIx,
krom	krom	k7c2
věže	věž	k1gFnSc2
ta	ten	k3xDgFnSc1
má	mít	k5eAaImIp3nS
zvonovou	zvonový	k2eAgFnSc4d1
střechu	střecha	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Interiér	interiér	k1gInSc1
</s>
<s>
Z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
zachoval	zachovat	k5eAaPmAgMnS
i	i	k9
původní	původní	k2eAgInSc4d1
mobiliář	mobiliář	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byly	být	k5eAaImAgFnP
bezdůvodně	bezdůvodně	k6eAd1
prodány	prodat	k5eAaPmNgFnP
cenné	cenný	k2eAgFnPc1d1
varhany	varhany	k1gFnPc1
od	od	k7c2
krnovského	krnovský	k2eAgMnSc2d1
stavitele	stavitel	k1gMnSc2
varhan	varhany	k1gFnPc2
Františka	František	k1gMnSc2
Riegra	Riegr	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
hlavní	hlavní	k2eAgInSc1d1
oltář	oltář	k1gInSc1
</s>
<s>
dřevěná	dřevěný	k2eAgFnSc1d1
křtitelnice	křtitelnice	k1gFnSc1
</s>
<s>
zpovědnice	zpovědnice	k1gFnSc1
</s>
<s>
dřevěná	dřevěný	k2eAgFnSc1d1
socha	socha	k1gFnSc1
Krista	Krista	k1gFnSc1
s	s	k7c7
ohnivým	ohnivý	k2eAgNnSc7d1
srdcem	srdce	k1gNnSc7
</s>
<s>
dřevěné	dřevěný	k2eAgFnPc1d1
lavice	lavice	k1gFnPc1
</s>
<s>
soubor	soubor	k1gInSc1
14	#num#	k4
obrazů	obraz	k1gInPc2
křížové	křížový	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
</s>
<s>
věčné	věčný	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
</s>
<s>
dřevěný	dřevěný	k2eAgInSc4d1
krucifix	krucifix	k1gInSc4
</s>
<s>
skříň	skříň	k1gFnSc1
na	na	k7c4
paramenta	paramenta	k1gNnPc4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zvony	zvon	k1gInPc1
</s>
<s>
V	v	k7c6
kostelní	kostelní	k2eAgFnSc6d1
věži	věž	k1gFnSc6
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgInP
2	#num#	k4
zvony	zvon	k1gInPc1
<g/>
,	,	kIx,
Ctiborius	Ctiborius	k1gMnSc1
a	a	k8xC
František	František	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
pojmenovány	pojmenovat	k5eAaPmNgFnP
po	po	k7c4
Františku	Františka	k1gFnSc4
Krumholzovi	Krumholz	k1gMnSc3
a	a	k8xC
jeho	jeho	k3xOp3gMnSc3
otci	otec	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
-	-	kIx~
Památkový	památkový	k2eAgInSc1d1
Katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
pamatkovykatalog	pamatkovykatalog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
S.	S.	kA
<g/>
R.	R.	kA
<g/>
O	O	kA
<g/>
,	,	kIx,
Jiri	Jiri	k1gNnSc1
Cizek	Cizky	k1gFnPc2
<g/>
,	,	kIx,
Hrady	hrad	k1gInPc7
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
Nové	Nové	k2eAgFnPc1d1
Valteřice	Valteřice	k1gFnPc1
<g/>
.	.	kIx.
www.hrady.cz	www.hrady.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Krásné	krásný	k2eAgNnSc1d1
Česko	Česko	k1gNnSc1
<g/>
.	.	kIx.
www.krasnecesko.cz	www.krasnecesko.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Detail	detail	k1gInSc1
dokumentu	dokument	k1gInSc2
-	-	kIx~
G	G	kA
<g/>
0	#num#	k4
<g/>
127283	#num#	k4
<g/>
.	.	kIx.
iispp	iispp	k1gInSc1
<g/>
.	.	kIx.
<g/>
npu	npu	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://ct24.ceskatelevize.cz/domaci/1421664-knez-rozprodava-vybaveni-kostelu.	https://ct24.ceskatelevize.cz/domaci/1421664-knez-rozprodava-vybaveni-kostelu.	k4
ct	ct	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
ceskatelevize	ceskatelevize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
