<s>
Jorre	Jorr	k1gMnSc5
Verstraeten	Verstraeten	k2eAgMnSc1d1
</s>
<s>
Jorre	Jorr	k1gMnSc5
VerstraetenOsobní	VerstraetenOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1997	#num#	k4
(	(	kIx(
<g/>
23	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Lovaň	Lovanit	k5eAaPmRp2nS
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc2
Stát	stát	k1gInSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
Sportovní	sportovní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Klub	klub	k1gInSc1
/	/	kIx~
Dojo	Dojo	k6eAd1
</s>
<s>
FFBJ	FFBJ	kA
(	(	kIx(
<g/>
fed	fed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
Judocentrum	Judocentrum	k1gNnSc1
Leuven	Leuvna	k1gFnPc2
(	(	kIx(
<g/>
dom	dom	k?
<g/>
.	.	kIx.
klub	klub	k1gInSc1
<g/>
)	)	kIx)
<g/>
JC	JC	kA
Koksijde	Koksijd	k1gInSc5
(	(	kIx(
<g/>
klub	klub	k1gInSc1
<g/>
)	)	kIx)
<g/>
Team	team	k1gInSc1
Belgium	Belgium	k1gNnSc1
(	(	kIx(
<g/>
vrch	vrch	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Trenéři	trenér	k1gMnPc1
</s>
<s>
Reno	Reno	k6eAd1
Rutten	Rutten	k2eAgInSc1d1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
<g/>
)	)	kIx)
<g/>
Frédéric	Frédéric	k1gMnSc1
Georgery	Georgera	k1gFnSc2
(	(	kIx(
<g/>
vrch	vrch	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
Cédric	Cédrice	k1gFnPc2
Taymans	Taymans	k1gInSc1
(	(	kIx(
<g/>
repr	repr	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Kategorie	kategorie	k1gFnPc1
</s>
<s>
lehká	lehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
Účast	účast	k1gFnSc1
na	na	k7c6
LOH	LOH	kA
</s>
<s>
bez	bez	k7c2
účasti	účast	k1gFnSc2
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgFnSc2d1
k	k	k7c3
červnu	červen	k1gInSc3
2019	#num#	k4
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
judu	judo	k1gNnSc6
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
EH	eh	k0
/	/	kIx~
ME	ME	kA
2019	#num#	k4
</s>
<s>
superlehká	superlehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
ME	ME	kA
2020	#num#	k4
</s>
<s>
superlehká	superlehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
Jorre	Jorr	k1gMnSc5
Verstraeten	Verstraeten	k2eAgMnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1997	#num#	k4
Lovaň	Lovaň	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
belgický	belgický	k2eAgMnSc1d1
zápasník	zápasník	k1gMnSc1
<g/>
–	–	k?
<g/>
judista	judista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS
v	v	k7c6
Heverlee	Heverlee	k1gFnSc6
na	na	k7c6
předměstí	předměstí	k1gNnSc6
Lovaně	Lovaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
judem	judo	k1gNnSc7
začal	začít	k5eAaPmAgMnS
v	v	k7c6
místním	místní	k2eAgNnSc6d1
Judocentru	Judocentrum	k1gNnSc6
v	v	k7c6
6	#num#	k4
letech	let	k1gInPc6
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Reno	Reno	k6eAd1
Ruttena	Rutten	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vrcholově	vrcholově	k6eAd1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
s	s	k7c7
valonským	valonský	k2eAgInSc7d1
týmem	tým	k1gInSc7
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Frédérica	Frédéricus	k1gMnSc2
Georgeryho	Georgery	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
belgické	belgický	k2eAgFnSc6d1
mužské	mužský	k2eAgFnSc6d1
reprezentaci	reprezentace	k1gFnSc6
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
v	v	k7c6
superlehké	superlehký	k2eAgFnSc6d1
váze	váha	k1gFnSc6
do	do	k7c2
60	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s>
Vítězství	vítězství	k1gNnSc1
</s>
<s>
2019	#num#	k4
–	–	k?
1	#num#	k4
<g/>
x	x	k?
světový	světový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
(	(	kIx(
<g/>
Tel	telit	k5eAaImRp2nS
Aviv	Aviv	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
Turnaj	turnaj	k1gInSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
2017	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
2019	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
muší	muší	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
superlehká	superlehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
Olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
<g/>
—	—	k?
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
<g/>
—	—	k?
<g/>
úč	úč	k?
<g/>
.	.	kIx.
</s>
<s>
Evropské	evropský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
<g/>
—	—	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
<g/>
—	—	k?
<g/>
úč	úč	k?
<g/>
.	.	kIx.
<g/>
úč	úč	k?
<g/>
.	.	kIx.
</s>
<s>
ME	ME	kA
do	do	k7c2
23	#num#	k4
let	léto	k1gNnPc2
<g/>
—	—	k?
<g/>
7.5	7.5	k4
<g/>
.	.	kIx.
<g/>
—	—	k?
</s>
<s>
MS	MS	kA
juniorů	junior	k1gMnPc2
<g/>
—	—	k?
<g/>
úč	úč	k?
<g/>
.	.	kIx.
<g/>
úč	úč	k?
<g/>
.	.	kIx.
</s>
<s>
ME	ME	kA
juniorů	junior	k1gMnPc2
<g/>
—	—	k?
<g/>
úč	úč	k?
<g/>
.	.	kIx.
<g/>
úč	úč	k?
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s>
MS	MS	kA
dorostenců	dorostenec	k1gMnPc2
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ME	ME	kA
dorostencůúč	dorostencůúč	k1gFnSc1
<g/>
.5	.5	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.nieuwsblad.be	www.nieuwsblad.bat	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Zápas	zápas	k1gInSc1
v	v	k7c6
Belgii	Belgie	k1gFnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
a	a	k8xC
novinky	novinka	k1gFnPc1
Jorre	Jorr	k1gInSc5
Verstraetena	Verstraeten	k2eAgFnSc1d1
na	na	k7c4
Judoinside	Judoinsid	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Výsledky	výsledek	k1gInPc1
a	a	k8xC
novinky	novinka	k1gFnPc1
Jorre	Jorr	k1gInSc5
Verstraetena	Verstraeten	k2eAgMnSc4d1
na	na	k7c6
Judobase	Judobasa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
