<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
méně	málo	k6eAd2	málo
rostoucím	rostoucí	k2eAgInPc3d1	rostoucí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
relativně	relativně	k6eAd1	relativně
silným	silný	k2eAgFnPc3d1	silná
středoevropským	středoevropský	k2eAgFnPc3d1	středoevropská
ekonomikám	ekonomika	k1gFnPc3	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
období	období	k1gNnSc6	období
hluboké	hluboký	k2eAgFnSc2d1	hluboká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
recese	recese	k1gFnSc2	recese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
ekonomika	ekonomika	k1gFnSc1	ekonomika
se	se	k3xPyFc4	se
ve	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
vážném	vážný	k2eAgInSc6d1	vážný
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
státního	státní	k2eAgInSc2d1	státní
bankrotu	bankrot	k1gInSc2	bankrot
<g/>
.	.	kIx.	.
</s>
<s>
Důsledky	důsledek	k1gInPc1	důsledek
života	život	k1gInSc2	život
na	na	k7c4	na
dluh	dluh	k1gInSc4	dluh
negativně	negativně	k6eAd1	negativně
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
všechny	všechen	k3xTgFnPc1	všechen
vrstvy	vrstva	k1gFnPc1	vrstva
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
doslova	doslova	k6eAd1	doslova
frustrovaná	frustrovaný	k2eAgFnSc1d1	frustrovaná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
běžné	běžný	k2eAgMnPc4d1	běžný
lidi	člověk	k1gMnPc4	člověk
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
dopadá	dopadat	k5eAaImIp3nS	dopadat
recese	recese	k1gFnSc1	recese
<g/>
,	,	kIx,	,
země	zem	k1gFnPc1	zem
zažívá	zažívat	k5eAaImIp3nS	zažívat
hromadné	hromadný	k2eAgNnSc1d1	hromadné
propouštění	propouštění	k1gNnSc1	propouštění
<g/>
,	,	kIx,	,
znehodnocuje	znehodnocovat	k5eAaImIp3nS	znehodnocovat
se	se	k3xPyFc4	se
maďarský	maďarský	k2eAgInSc4d1	maďarský
forint	forint	k1gInSc4	forint
a	a	k8xC	a
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
rodin	rodina	k1gFnPc2	rodina
jsou	být	k5eAaImIp3nP	být
neschopné	schopný	k2eNgFnPc1d1	neschopná
splácet	splácet	k5eAaImF	splácet
úvěry	úvěr	k1gInPc4	úvěr
a	a	k8xC	a
hypotéky	hypotéka	k1gFnPc4	hypotéka
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
zadlužení	zadlužení	k1gNnSc1	zadlužení
domácností	domácnost	k1gFnPc2	domácnost
překročilo	překročit	k5eAaPmAgNnS	překročit
hranici	hranice	k1gFnSc4	hranice
350	[number]	k4	350
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Zadluženost	zadluženost	k1gFnSc1	zadluženost
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c6	na
astronomických	astronomický	k2eAgInPc6d1	astronomický
81,6	[number]	k4	81,6
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
46	[number]	k4	46
%	%	kIx~	%
a	a	k8xC	a
SR	SR	kA	SR
54	[number]	k4	54
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Meziválečné	meziválečný	k2eAgNnSc1d1	meziválečné
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
===	===	k?	===
</s>
</p>
<p>
<s>
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
ekonomika	ekonomika	k1gFnSc1	ekonomika
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
vzniku	vznik	k1gInSc2	vznik
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
propadla	propadnout	k5eAaPmAgFnS	propadnout
do	do	k7c2	do
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
odtržení	odtržení	k1gNnSc1	odtržení
pohraničních	pohraniční	k2eAgNnPc2d1	pohraniční
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
<g/>
,	,	kIx,	,
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
<g/>
,	,	kIx,	,
Vojvodiny	Vojvodina	k1gFnSc2	Vojvodina
<g/>
,	,	kIx,	,
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
Burgenlandu	Burgenland	k1gInSc2	Burgenland
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpřetrhání	zpřetrhání	k1gNnSc1	zpřetrhání
dlouholetých	dlouholetý	k2eAgFnPc2d1	dlouholetá
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
vazeb	vazba	k1gFnPc2	vazba
mezi	mezi	k7c7	mezi
centrem	centrum	k1gNnSc7	centrum
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
okraji	okraj	k1gInPc7	okraj
<g/>
,	,	kIx,	,
bohatými	bohatý	k2eAgInPc7d1	bohatý
na	na	k7c4	na
suroviny	surovina	k1gFnPc4	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
nakonec	nakonec	k6eAd1	nakonec
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
autoritativního	autoritativní	k2eAgInSc2d1	autoritativní
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
například	například	k6eAd1	například
pádem	pád	k1gInSc7	pád
domácí	domácí	k2eAgFnSc2d1	domácí
měny	měna	k1gFnSc2	měna
a	a	k8xC	a
nepomohly	pomoct	k5eNaPmAgFnP	pomoct
ji	on	k3xPp3gFnSc4	on
ani	ani	k8xC	ani
špatné	špatný	k2eAgInPc1d1	špatný
vztahy	vztah	k1gInPc1	vztah
republiky	republika	k1gFnSc2	republika
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
novými	nový	k2eAgMnPc7d1	nový
sousedy	soused	k1gMnPc7	soused
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc2	inflace
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
byla	být	k5eAaImAgFnS	být
dosažena	dosáhnout	k5eAaPmNgFnS	dosáhnout
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
nominální	nominální	k2eAgFnSc1d1	nominální
hodnota	hodnota	k1gFnSc1	hodnota
vydávaných	vydávaný	k2eAgFnPc2d1	vydávaná
bankovek	bankovka	k1gFnPc2	bankovka
1	[number]	k4	1
000	[number]	k4	000
maďarských	maďarský	k2eAgInPc2d1	maďarský
pengő	pengő	k?	pengő
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
10	[number]	k4	10
000	[number]	k4	000
000	[number]	k4	000
pengő	pengő	k?	pengő
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
(	(	kIx(	(
<g/>
sto	sto	k4xCgNnSc4	sto
trilionů	trilion	k4xCgInPc2	trilion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
speciální	speciální	k2eAgFnSc2d1	speciální
měny	měna	k1gFnSc2	měna
adópengő	adópengő	k?	adópengő
<g/>
,	,	kIx,	,
určené	určený	k2eAgFnSc6d1	určená
pro	pro	k7c4	pro
daňové	daňový	k2eAgFnPc4d1	daňová
a	a	k8xC	a
poštovní	poštovní	k2eAgFnPc4d1	poštovní
platby	platba	k1gFnPc4	platba
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
adópenga	adópenga	k1gFnSc1	adópenga
byla	být	k5eAaImAgFnS	být
každým	každý	k3xTgInSc7	každý
dnem	den	k1gInSc7	den
přizpůsobována	přizpůsobovat	k5eAaImNgFnS	přizpůsobovat
současnému	současný	k2eAgInSc3d1	současný
kurzu	kurz	k1gInSc3	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1946	[number]	k4	1946
se	se	k3xPyFc4	se
jedno	jeden	k4xCgNnSc1	jeden
adópengő	adópengő	k?	adópengő
rovnalo	rovnat	k5eAaImAgNnS	rovnat
jednomu	jeden	k4xCgNnSc3	jeden
pengu	peng	k1gInSc3	peng
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
července	červenec	k1gInSc2	červenec
1946	[number]	k4	1946
se	se	k3xPyFc4	se
již	již	k6eAd1	již
rovnalo	rovnat	k5eAaImAgNnS	rovnat
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
(	(	kIx(	(
<g/>
2	[number]	k4	2
triliardám	triliarda	k4xCgFnPc3	triliarda
<g/>
)	)	kIx)	)
peng	penga	k1gFnPc2	penga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nahrazení	nahrazení	k1gNnSc3	nahrazení
maďarského	maďarský	k2eAgMnSc2d1	maďarský
penga	peng	k1gMnSc2	peng
maďarským	maďarský	k2eAgInSc7d1	maďarský
forintem	forint	k1gInSc7	forint
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hodnota	hodnota	k1gFnSc1	hodnota
všech	všecek	k3xTgFnPc2	všecek
maďarských	maďarský	k2eAgFnPc2d1	maďarská
bankovek	bankovka	k1gFnPc2	bankovka
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
rovnala	rovnat	k5eAaImAgFnS	rovnat
jedné	jeden	k4xCgFnSc6	jeden
tisícině	tisícina	k1gFnSc6	tisícina
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
zaznamenaný	zaznamenaný	k2eAgInSc4d1	zaznamenaný
případ	případ	k1gInSc4	případ
inflace	inflace	k1gFnSc2	inflace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měsíčně	měsíčně	k6eAd1	měsíčně
rostla	růst	k5eAaImAgFnS	růst
o	o	k7c4	o
1,3	[number]	k4	1,3
<g/>
×	×	k?	×
<g/>
1016	[number]	k4	1016
%	%	kIx~	%
<g/>
,	,	kIx,	,
ceny	cena	k1gFnPc1	cena
se	se	k3xPyFc4	se
tak	tak	k9	tak
každých	každý	k3xTgInPc2	každý
15	[number]	k4	15
hodin	hodina	k1gFnPc2	hodina
zdvojnásobovaly	zdvojnásobovat	k5eAaImAgFnP	zdvojnásobovat
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
dopad	dopad	k1gInSc1	dopad
hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
byl	být	k5eAaImAgInS	být
zastaven	zastavit	k5eAaPmNgInS	zastavit
až	až	k9	až
měnovou	měnový	k2eAgFnSc7d1	měnová
reformou	reforma	k1gFnSc7	reforma
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
400	[number]	k4	400
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
(	(	kIx(	(
<g/>
400	[number]	k4	400
quadriliard	quadriliarda	k1gFnPc2	quadriliarda
<g/>
)	)	kIx)	)
pengő	pengő	k?	pengő
proměněno	proměněn	k2eAgNnSc1d1	proměněno
na	na	k7c6	na
1	[number]	k4	1
forint	forint	k1gInSc1	forint
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Socialistické	socialistický	k2eAgNnSc1d1	socialistické
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
začalo	začít	k5eAaPmAgNnS	začít
postupně	postupně	k6eAd1	postupně
začleňovat	začleňovat	k5eAaImF	začleňovat
do	do	k7c2	do
východních	východní	k2eAgFnPc2d1	východní
struktur	struktura	k1gFnPc2	struktura
a	a	k8xC	a
svojí	svojit	k5eAaImIp3nS	svojit
ekonomiku	ekonomika	k1gFnSc4	ekonomika
vázat	vázat	k5eAaImF	vázat
na	na	k7c6	na
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
lidově	lidově	k6eAd1	lidově
demokratické	demokratický	k2eAgInPc1d1	demokratický
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
odsud	odsud	k6eAd1	odsud
přišlo	přijít	k5eAaPmAgNnS	přijít
mnoho	mnoho	k4c1	mnoho
investic	investice	k1gFnPc2	investice
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
vláda	vláda	k1gFnSc1	vláda
navíc	navíc	k6eAd1	navíc
podporovala	podporovat	k5eAaImAgFnS	podporovat
překotnou	překotný	k2eAgFnSc4d1	překotná
industrializaci	industrializace	k1gFnSc4	industrializace
v	v	k7c6	v
stalinistickém	stalinistický	k2eAgInSc6d1	stalinistický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Podniky	podnik	k1gInPc1	podnik
byly	být	k5eAaImAgInP	být
znárodněny	znárodněn	k2eAgInPc1d1	znárodněn
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc1	zemědělství
zkolektivizováno	zkolektivizován	k2eAgNnSc1d1	zkolektivizován
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
fungoval	fungovat	k5eAaImAgInS	fungovat
po	po	k7c6	po
celá	celý	k2eAgFnSc1d1	celá
50	[number]	k4	50
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
pootevřelo	pootevřít	k5eAaPmAgNnS	pootevřít
zahraničnímu	zahraniční	k2eAgInSc3d1	zahraniční
obchodu	obchod	k1gInSc3	obchod
a	a	k8xC	a
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
omezené	omezený	k2eAgFnSc2d1	omezená
míry	míra	k1gFnSc2	míra
fungování	fungování	k1gNnSc1	fungování
trhu	trh	k1gInSc2	trh
(	(	kIx(	(
<g/>
známé	známý	k2eAgNnSc1d1	známé
též	též	k9	též
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
gulášový	gulášový	k2eAgInSc1d1	gulášový
socialismus	socialismus	k1gInSc1	socialismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
těšila	těšit	k5eAaImAgFnS	těšit
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
životních	životní	k2eAgFnPc2d1	životní
úrovní	úroveň	k1gFnPc2	úroveň
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
RVHP	RVHP	kA	RVHP
(	(	kIx(	(
<g/>
po	po	k7c6	po
NDR	NDR	kA	NDR
a	a	k8xC	a
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
problémy	problém	k1gInPc1	problém
se	se	k3xPyFc4	se
ale	ale	k9	ale
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
a	a	k8xC	a
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začaly	začít	k5eAaPmAgFnP	začít
výrobní	výrobní	k2eAgFnPc1d1	výrobní
technologie	technologie	k1gFnPc1	technologie
zastarávat	zastarávat	k5eAaImF	zastarávat
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
též	též	k9	též
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
i	i	k9	i
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
<g/>
;	;	kIx,	;
během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
narostl	narůst	k5eAaPmAgInS	narůst
zhruba	zhruba	k6eAd1	zhruba
patnáctinásobně	patnáctinásobně	k6eAd1	patnáctinásobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
donutilo	donutit	k5eAaPmAgNnS	donutit
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
systém	systém	k1gInSc1	systém
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
upravit	upravit	k5eAaPmF	upravit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
západním	západní	k2eAgFnPc3d1	západní
ekonomikám	ekonomika	k1gFnPc3	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dvoustupňový	dvoustupňový	k2eAgInSc1d1	dvoustupňový
bankovní	bankovní	k2eAgInSc1d1	bankovní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
byly	být	k5eAaImAgFnP	být
nové	nový	k2eAgFnPc1d1	nová
daně	daň	k1gFnPc1	daň
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
daně	daň	k1gFnPc1	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
tak	tak	k9	tak
pomalu	pomalu	k6eAd1	pomalu
začala	začít	k5eAaPmAgFnS	začít
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
volného	volný	k2eAgInSc2d1	volný
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přišla	přijít	k5eAaPmAgFnS	přijít
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Postkomunistická	postkomunistický	k2eAgFnSc1d1	postkomunistická
éra	éra	k1gFnSc1	éra
a	a	k8xC	a
integrace	integrace	k1gFnSc1	integrace
do	do	k7c2	do
západních	západní	k2eAgFnPc2d1	západní
struktur	struktura	k1gFnPc2	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990	[number]	k4	1990
a	a	k8xC	a
1994	[number]	k4	1994
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
premiér	premiér	k1gMnSc1	premiér
József	József	k1gMnSc1	József
Antall	Antall	k1gMnSc1	Antall
provedl	provést	k5eAaPmAgMnS	provést
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
liberalizaci	liberalizace	k1gFnSc4	liberalizace
<g/>
,	,	kIx,	,
též	též	k9	též
byla	být	k5eAaImAgFnS	být
přepracována	přepracován	k2eAgFnSc1d1	přepracována
daňová	daňový	k2eAgFnSc1d1	daňová
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
prvních	první	k4xOgNnPc2	první
let	léto	k1gNnPc2	léto
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
s	s	k7c7	s
financování	financování	k1gNnSc2	financování
také	také	k9	také
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc1d1	měnový
fond	fond	k1gInSc1	fond
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
služby	služba	k1gFnPc1	služba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
socialistickými	socialistický	k2eAgFnPc7d1	socialistická
vládami	vláda	k1gFnPc7	vláda
dotované	dotovaný	k2eAgFnSc2d1	dotovaná
<g/>
,	,	kIx,	,
najednou	najednou	k6eAd1	najednou
být	být	k5eAaImF	být
přestaly	přestat	k5eAaPmAgFnP	přestat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
cen	cena	k1gFnPc2	cena
například	například	k6eAd1	například
za	za	k7c4	za
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
energie	energie	k1gFnPc4	energie
a	a	k8xC	a
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
podniků	podnik	k1gInPc2	podnik
bylo	být	k5eAaImAgNnS	být
privatizováno	privatizován	k2eAgNnSc1d1	privatizováno
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
se	se	k3xPyFc4	se
ale	ale	k9	ale
nepřizpůsobily	přizpůsobit	k5eNaPmAgFnP	přizpůsobit
trhu	trh	k1gInSc3	trh
a	a	k8xC	a
zkrachovaly	zkrachovat	k5eAaPmAgFnP	zkrachovat
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
musely	muset	k5eAaImAgFnP	muset
radikálně	radikálně	k6eAd1	radikálně
přeorientovat	přeorientovat	k5eAaPmF	přeorientovat
(	(	kIx(	(
<g/>
Ikarus	Ikarus	k1gMnSc1	Ikarus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ustal	ustat	k5eAaPmAgMnS	ustat
vývoz	vývoz	k1gInSc4	vývoz
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Narostla	narůst	k5eAaPmAgFnS	narůst
též	též	k9	též
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
,	,	kIx,	,
státní	státní	k2eAgInPc4d1	státní
rozpočty	rozpočet	k1gInPc4	rozpočet
končily	končit	k5eAaImAgFnP	končit
zásadně	zásadně	k6eAd1	zásadně
deficitem	deficit	k1gInSc7	deficit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
premiér	premiér	k1gMnSc1	premiér
Gyula	Gyula	k1gMnSc1	Gyula
Horn	Horn	k1gMnSc1	Horn
se	se	k3xPyFc4	se
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1995	[number]	k4	1995
pokusil	pokusit	k5eAaPmAgMnS	pokusit
celou	celý	k2eAgFnSc4d1	celá
situaci	situace	k1gFnSc4	situace
vyřešit	vyřešit	k5eAaPmF	vyřešit
velkou	velký	k2eAgFnSc7d1	velká
privatizací	privatizace	k1gFnSc7	privatizace
mnohých	mnohý	k2eAgInPc2d1	mnohý
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Klesly	klesnout	k5eAaPmAgInP	klesnout
také	také	k9	také
i	i	k9	i
výdaje	výdaj	k1gInPc1	výdaj
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
veřejného	veřejný	k2eAgInSc2d1	veřejný
sektoru	sektor	k1gInSc2	sektor
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
z	z	k7c2	z
62	[number]	k4	62
%	%	kIx~	%
na	na	k7c6	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pomalu	pomalu	k6eAd1	pomalu
nastartovalo	nastartovat	k5eAaPmAgNnS	nastartovat
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
růst	růst	k1gInSc4	růst
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
prvého	prvý	k4xOgNnSc2	prvý
desetiletí	desetiletí	k1gNnSc2	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začal	začít	k5eAaPmAgInS	začít
vzestup	vzestup	k1gInSc1	vzestup
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
posledními	poslední	k2eAgNnPc7d1	poslední
lety	léto	k1gNnPc7	léto
vskutku	vskutku	k9	vskutku
nevídaný	vídaný	k2eNgMnSc1d1	nevídaný
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
mu	on	k3xPp3gMnSc3	on
napomohl	napomoct	k5eAaPmAgMnS	napomoct
i	i	k9	i
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
EU	EU	kA	EU
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Avšak	avšak	k8xC	avšak
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
opět	opět	k6eAd1	opět
začala	začít	k5eAaPmAgFnS	začít
horšit	horšit	k5eAaImF	horšit
<g/>
,	,	kIx,	,
především	především	k9	především
díky	díky	k7c3	díky
zvyšujícímu	zvyšující	k2eAgMnSc3d1	zvyšující
se	se	k3xPyFc4	se
tempu	tempo	k1gNnSc6	tempo
zadlužování	zadlužování	k1gNnPc2	zadlužování
vlády	vláda	k1gFnSc2	vláda
premiéra	premiér	k1gMnSc2	premiér
Ference	Ferenc	k1gMnSc2	Ferenc
Gyurcsánye	Gyurcsány	k1gMnSc2	Gyurcsány
a	a	k8xC	a
též	též	k9	též
i	i	k9	i
zastavením	zastavení	k1gNnSc7	zastavení
některých	některý	k3yIgFnPc2	některý
důležitých	důležitý	k2eAgFnPc2d1	důležitá
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Odkládání	odkládání	k1gNnSc1	odkládání
nezbytných	zbytný	k2eNgFnPc2d1	zbytný
reforem	reforma	k1gFnPc2	reforma
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
propadu	propad	k1gInSc3	propad
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
problémům	problém	k1gInPc3	problém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
projevily	projevit	k5eAaPmAgInP	projevit
během	během	k7c2	během
tzv.	tzv.	kA	tzv.
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Maďarský	maďarský	k2eAgInSc1d1	maďarský
forint	forint	k1gInSc1	forint
prudce	prudko	k6eAd1	prudko
oslabil	oslabit	k5eAaPmAgInS	oslabit
a	a	k8xC	a
země	země	k1gFnSc1	země
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xS	jak
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
===	===	k?	===
</s>
</p>
<p>
<s>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
státům	stát	k1gInPc3	stát
nejvíce	hodně	k6eAd3	hodně
postiženým	postižený	k2eAgInSc7d1	postižený
světovou	světový	k2eAgFnSc7d1	světová
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
krizí	krize	k1gFnSc7	krize
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
tohoto	tento	k3xDgMnSc4	tento
je	být	k5eAaImIp3nS	být
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
<g/>
.	.	kIx.	.
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
30	[number]	k4	30
let	léto	k1gNnPc2	léto
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
neúměrně	úměrně	k6eNd1	úměrně
veliký	veliký	k2eAgInSc4d1	veliký
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
splácet	splácet	k5eAaImF	splácet
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
odhalila	odhalit	k5eAaPmAgFnS	odhalit
velice	velice	k6eAd1	velice
slabé	slabý	k2eAgInPc4d1	slabý
základy	základ	k1gInPc4	základ
maďarského	maďarský	k2eAgNnSc2d1	Maďarské
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Důsledky	důsledek	k1gInPc1	důsledek
jsou	být	k5eAaImIp3nP	být
znepokojivé	znepokojivý	k2eAgInPc1d1	znepokojivý
<g/>
.	.	kIx.	.
</s>
<s>
Zadluženost	zadluženost	k1gFnSc1	zadluženost
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
73	[number]	k4	73
procent	procento	k1gNnPc2	procento
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
30	[number]	k4	30
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Úroky	úrok	k1gInPc1	úrok
z	z	k7c2	z
dluhů	dluh	k1gInPc2	dluh
odčerpávají	odčerpávat	k5eAaImIp3nP	odčerpávat
3,5	[number]	k4	3,5
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
procenta	procento	k1gNnPc4	procento
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
suma	suma	k1gFnSc1	suma
než	než	k8xS	než
součet	součet	k1gInSc1	součet
zdrojů	zdroj	k1gInPc2	zdroj
z	z	k7c2	z
evropských	evropský	k2eAgInPc2d1	evropský
fondů	fond	k1gInPc2	fond
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
stálo	stát	k5eAaImAgNnS	stát
euro	euro	k1gNnSc1	euro
317	[number]	k4	317
forintů	forint	k1gInPc2	forint
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
230	[number]	k4	230
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
loňského	loňský	k2eAgInSc2d1	loňský
roku	rok	k1gInSc2	rok
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
zadluženost	zadluženost	k1gFnSc1	zadluženost
maďarských	maďarský	k2eAgFnPc2d1	maďarská
domácností	domácnost	k1gFnPc2	domácnost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
biliony	bilion	k4xCgInPc4	bilion
forintů	forint	k1gInPc2	forint
(	(	kIx(	(
<g/>
6,25	[number]	k4	6,25
miliardy	miliarda	k4xCgFnSc2	miliarda
eur	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
úvěrů	úvěr	k1gInPc2	úvěr
je	být	k5eAaImIp3nS	být
11,87	[number]	k4	11,87
miliardy	miliarda	k4xCgFnSc2	miliarda
eur	euro	k1gNnPc2	euro
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
bylo	být	k5eAaImAgNnS	být
téměř	téměř	k6eAd1	téměř
80	[number]	k4	80
procent	procento	k1gNnPc2	procento
poskytnuto	poskytnout	k5eAaPmNgNnS	poskytnout
v	v	k7c6	v
cizích	cizí	k2eAgFnPc6d1	cizí
měnách	měna	k1gFnPc6	měna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
loňského	loňský	k2eAgNnSc2d1	loňské
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
října	říjen	k1gInSc2	říjen
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
desetimilionové	desetimilionový	k2eAgFnSc6d1	desetimilionová
zemi	zem	k1gFnSc6	zem
představuje	představovat	k5eAaImIp3nS	představovat
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
8,6	[number]	k4	8,6
procenta	procento	k1gNnSc2	procento
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
krizi	krize	k1gFnSc3	krize
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyšplhat	vyšplhat	k5eAaPmF	vyšplhat
až	až	k9	až
na	na	k7c4	na
deset	deset	k4xCc4	deset
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
je	být	k5eAaImIp3nS	být
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
57	[number]	k4	57
<g/>
procentní	procentní	k2eAgInSc1d1	procentní
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
nad	nad	k7c7	nad
60	[number]	k4	60
procenty	procent	k1gInPc7	procent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Financovat	financovat	k5eAaBmF	financovat
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
asi	asi	k9	asi
tři	tři	k4xCgNnPc4	tři
miliony	milion	k4xCgInPc4	milion
důchodců	důchodce	k1gMnPc2	důchodce
<g/>
.	.	kIx.	.
</s>
<s>
Daňová	daňový	k2eAgFnSc1d1	daňová
zátěž	zátěž	k1gFnSc1	zátěž
je	být	k5eAaImIp3nS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hrubé	hrubý	k2eAgFnSc2d1	hrubá
mzdy	mzda	k1gFnSc2	mzda
odvádějí	odvádět	k5eAaImIp3nP	odvádět
Maďaři	Maďar	k1gMnPc1	Maďar
54	[number]	k4	54
procent	procento	k1gNnPc2	procento
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
42,9	[number]	k4	42,9
<g/>
,	,	kIx,	,
v	v	k7c6	v
SR	SR	kA	SR
38,5	[number]	k4	38,5
procenta	procento	k1gNnSc2	procento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
propad	propad	k1gInSc1	propad
početných	početný	k2eAgFnPc2d1	početná
skupin	skupina	k1gFnPc2	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
kriminalitu	kriminalita	k1gFnSc4	kriminalita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvních	první	k4xOgInPc2	první
šest	šest	k4xCc1	šest
týdnů	týden	k1gInPc2	týden
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
tolik	tolik	k4yIc1	tolik
bankovních	bankovní	k2eAgFnPc2d1	bankovní
loupeží	loupež	k1gFnPc2	loupež
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
za	za	k7c4	za
půlrok	půlrok	k1gInSc4	půlrok
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
národnostní	národnostní	k2eAgNnSc1d1	národnostní
napětí	napětí	k1gNnSc1	napětí
<g/>
,	,	kIx,	,
o	o	k7c6	o
útocích	útok	k1gInPc6	útok
na	na	k7c4	na
Romy	Rom	k1gMnPc4	Rom
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
maďarský	maďarský	k2eAgInSc1d1	maďarský
tisk	tisk	k1gInSc1	tisk
přináší	přinášet	k5eAaImIp3nS	přinášet
více	hodně	k6eAd2	hodně
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
trestných	trestný	k2eAgInPc6d1	trestný
činech	čin	k1gInPc6	čin
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
Romy	Rom	k1gMnPc4	Rom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
odhady	odhad	k1gInPc1	odhad
vývoje	vývoj	k1gInSc2	vývoj
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
propad	propad	k1gInSc4	propad
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
7-9	[number]	k4	7-9
%	%	kIx~	%
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
–	–	k?	–
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ekonomika	ekonomik	k1gMnSc2	ekonomik
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
