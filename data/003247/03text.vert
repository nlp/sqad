<s>
Symfonický	symfonický	k2eAgInSc1d1	symfonický
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
metalových	metalový	k2eAgInPc2d1	metalový
stylů	styl	k1gInPc2	styl
(	(	kIx(	(
<g/>
death	death	k1gMnSc1	death
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
heavy	heava	k1gFnPc1	heava
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
black	black	k6eAd1	black
metal	metat	k5eAaImAgMnS	metat
a	a	k8xC	a
doom	doom	k1gMnSc1	doom
metal	metat	k5eAaImAgMnS	metat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
zakomponováním	zakomponování	k1gNnSc7	zakomponování
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
hlasů	hlas	k1gInPc2	hlas
používaných	používaný	k2eAgInPc2d1	používaný
ve	v	k7c6	v
vážné	vážný	k2eAgFnSc3d1	vážná
hudbě	hudba	k1gFnSc3	hudba
(	(	kIx(	(
<g/>
housle	housle	k1gFnPc1	housle
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
dechy	dech	k1gInPc1	dech
<g/>
,	,	kIx,	,
pěvecké	pěvecký	k2eAgInPc1d1	pěvecký
sbory	sbor	k1gInPc1	sbor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zvuky	zvuk	k1gInPc1	zvuk
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
uměle	uměle	k6eAd1	uměle
nebo	nebo	k8xC	nebo
skutečným	skutečný	k2eAgNnSc7d1	skutečné
hudebním	hudební	k2eAgNnSc7d1	hudební
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Symphonic	Symphonice	k1gFnPc2	Symphonice
metal	metal	k1gInSc4	metal
hraje	hrát	k5eAaImIp3nS	hrát
mnoho	mnoho	k4c4	mnoho
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
projekty	projekt	k1gInPc4	projekt
hudebníků	hudebník	k1gMnPc2	hudebník
či	či	k8xC	či
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
experimenty	experiment	k1gInPc1	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Prvopočátky	prvopočátek	k1gInPc1	prvopočátek
tohoto	tento	k3xDgInSc2	tento
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začaly	začít	k5eAaPmAgFnP	začít
po	po	k7c6	po
boku	bok	k1gInSc6	bok
trashmetalových	trashmetalův	k2eAgMnPc2d1	trashmetalův
a	a	k8xC	a
deathmetalových	deathmetalův	k2eAgMnPc2d1	deathmetalův
velikánů	velikán	k1gMnPc2	velikán
vznikat	vznikat	k5eAaImF	vznikat
kapely	kapela	k1gFnPc4	kapela
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
začaly	začít	k5eAaPmAgFnP	začít
hrát	hrát	k5eAaImF	hrát
nový	nový	k2eAgInSc4d1	nový
a	a	k8xC	a
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
velice	velice	k6eAd1	velice
neobvyklý	obvyklý	k2eNgInSc1d1	neobvyklý
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
doom	doom	k6eAd1	doom
metal	metat	k5eAaImAgInS	metat
(	(	kIx(	(
<g/>
Paradise	Paradise	k1gFnSc1	Paradise
Lost	Lost	k1gInSc1	Lost
<g/>
,	,	kIx,	,
Anathema	anathema	k1gNnSc1	anathema
nebo	nebo	k8xC	nebo
velice	velice	k6eAd1	velice
temná	temnat	k5eAaImIp3nS	temnat
<g/>
,	,	kIx,	,
až	až	k8xS	až
smrtící	smrtící	k2eAgFnSc1d1	smrtící
doomová	doomový	k2eAgFnSc1d1	doomový
kapela	kapela	k1gFnSc1	kapela
My	my	k3xPp1nPc1	my
Dying	Dying	k1gInSc1	Dying
Bride	Brid	k1gInSc5	Brid
<g/>
;	;	kIx,	;
v	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
skladbách	skladba	k1gFnPc6	skladba
znějící	znějící	k2eAgFnPc1d1	znějící
housle	housle	k1gFnPc1	housle
přinášejí	přinášet	k5eAaImIp3nP	přinášet
velice	velice	k6eAd1	velice
tesknou	teskný	k2eAgFnSc4d1	teskná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
doom	dooma	k1gFnPc2	dooma
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
art	art	k?	art
rocku	rock	k1gInSc2	rock
začalo	začít	k5eAaPmAgNnS	začít
hojně	hojně	k6eAd1	hojně
používat	používat	k5eAaImF	používat
netradičních	tradiční	k2eNgInPc2d1	netradiční
"	"	kIx"	"
<g/>
klasických	klasický	k2eAgInPc2d1	klasický
<g/>
"	"	kIx"	"
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
tempo	tempo	k1gNnSc1	tempo
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
zpomalilo	zpomalit	k5eAaPmAgNnS	zpomalit
<g/>
,	,	kIx,	,
skladby	skladba	k1gFnPc1	skladba
nabraly	nabrat	k5eAaPmAgFnP	nabrat
baladický	baladický	k2eAgInSc4d1	baladický
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
ženské	ženský	k2eAgInPc4d1	ženský
vokály	vokál	k1gInPc4	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
moc	moc	k6eAd1	moc
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
vytlačován	vytlačovat	k5eAaImNgInS	vytlačovat
black	black	k6eAd1	black
metalem	metal	k1gInSc7	metal
<g/>
,	,	kIx,	,
power	power	k1gMnSc1	power
metalem	metal	k1gInSc7	metal
<g/>
,	,	kIx,	,
hardcorem	hardcor	k1gInSc7	hardcor
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
styly	styl	k1gInPc7	styl
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
získávaly	získávat	k5eAaImAgInP	získávat
větší	veliký	k2eAgFnSc4d2	veliký
popularitu	popularita	k1gFnSc4	popularita
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
začalo	začít	k5eAaPmAgNnS	začít
zapomínat	zapomínat	k5eAaImF	zapomínat
<g/>
.	.	kIx.	.
</s>
<s>
Doom	Doom	k6eAd1	Doom
metal	metal	k1gInSc1	metal
dal	dát	k5eAaPmAgInS	dát
základy	základ	k1gInPc4	základ
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
gothic	gothice	k1gFnPc2	gothice
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
symphonic	symphonice	k1gFnPc2	symphonice
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
symphonic	symphonice	k1gFnPc2	symphonice
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
nejspíše	nejspíše	k9	nejspíše
Therion	Therion	k1gInSc4	Therion
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
zapojili	zapojit	k5eAaPmAgMnP	zapojit
symfonický	symfonický	k2eAgInSc4d1	symfonický
orchestr	orchestr	k1gInSc4	orchestr
jako	jako	k8xS	jako
plnohodnotnou	plnohodnotný	k2eAgFnSc4d1	plnohodnotná
součást	součást	k1gFnSc4	součást
svého	svůj	k3xOyFgInSc2	svůj
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
odklonili	odklonit	k5eAaPmAgMnP	odklonit
se	se	k3xPyFc4	se
tak	tak	k9	tak
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
předchozí	předchozí	k2eAgFnSc2d1	předchozí
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgNnSc4	svůj
nejvíce	nejvíce	k6eAd1	nejvíce
experimentální	experimentální	k2eAgNnSc4d1	experimentální
album	album	k1gNnSc4	album
Theli	Thele	k1gFnSc4	Thele
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hudební	hudební	k2eAgFnSc7d1	hudební
složkou	složka	k1gFnSc7	složka
jsou	být	k5eAaImIp3nP	být
klávesy	klávesa	k1gFnPc1	klávesa
a	a	k8xC	a
sborový	sborový	k2eAgInSc1d1	sborový
zpěv	zpěv	k1gInSc1	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
páteř	páteř	k1gFnSc1	páteř
se	se	k3xPyFc4	se
neliší	lišit	k5eNaImIp3nS	lišit
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
metalového	metalový	k2eAgInSc2d1	metalový
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
symfoniky	symfonik	k1gMnPc4	symfonik
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
dosažena	dosáhnout	k5eAaPmNgFnS	dosáhnout
klávesami	klávesa	k1gFnPc7	klávesa
či	či	k8xC	či
hostováním	hostování	k1gNnSc7	hostování
klasických	klasický	k2eAgInPc2d1	klasický
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
lze	lze	k6eAd1	lze
tuto	tento	k3xDgFnSc4	tento
kombinaci	kombinace	k1gFnSc4	kombinace
slyšet	slyšet	k5eAaImF	slyšet
v	v	k7c4	v
black	black	k1gInSc4	black
metalu	metal	k1gInSc2	metal
(	(	kIx(	(
<g/>
Dimmu	Dimma	k1gFnSc4	Dimma
Borgir	Borgira	k1gFnPc2	Borgira
<g/>
,	,	kIx,	,
Limbonic	Limbonice	k1gFnPc2	Limbonice
Art	Art	k1gFnPc2	Art
<g/>
,	,	kIx,	,
Tartaros	Tartaros	k1gInSc1	Tartaros
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgFnPc1	veškerý
skladby	skladba	k1gFnPc1	skladba
jsou	být	k5eAaImIp3nP	být
hrány	hrát	k5eAaImNgFnP	hrát
hudebním	hudební	k2eAgNnSc7d1	hudební
tělesem	těleso	k1gNnSc7	těleso
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Pražský	pražský	k2eAgInSc1d1	pražský
filharmonický	filharmonický	k2eAgInSc1d1	filharmonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
a	a	k8xC	a
elektrické	elektrický	k2eAgFnPc1d1	elektrická
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnPc1d1	bicí
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
dělají	dělat	k5eAaImIp3nP	dělat
jen	jen	k9	jen
doprovod	doprovod	k1gInSc4	doprovod
(	(	kIx(	(
<g/>
Rage	Rage	k1gFnSc4	Rage
–	–	k?	–
Lingua	Lingu	k2eAgFnSc1d1	Lingua
Mortis	Mortis	k1gFnSc1	Mortis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
i	i	k8xC	i
více	hodně	k6eAd2	hodně
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
operní	operní	k2eAgInSc4d1	operní
a	a	k8xC	a
klasický	klasický	k2eAgInSc4d1	klasický
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
stylu	styl	k1gInSc2	styl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
formou	forma	k1gFnSc7	forma
"	"	kIx"	"
<g/>
dialogů	dialog	k1gInPc2	dialog
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hádky	hádka	k1gFnPc1	hádka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
anděl	anděl	k1gMnSc1	anděl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
"	"	kIx"	"
<g/>
ďábel	ďábel	k1gMnSc1	ďábel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Avantasia	Avantasia	k1gFnSc1	Avantasia
–	–	k?	–
The	The	k1gFnSc1	The
Metal	metal	k1gInSc1	metal
Opera	opera	k1gFnSc1	opera
Pt	Pt	k1gFnSc1	Pt
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
<g/>
–	–	k?	–
<g/>
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
častým	častý	k2eAgMnSc7d1	častý
a	a	k8xC	a
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
modelem	model	k1gInSc7	model
je	být	k5eAaImIp3nS	být
výrazný	výrazný	k2eAgInSc1d1	výrazný
ženský	ženský	k2eAgInSc1d1	ženský
hlas	hlas	k1gInSc1	hlas
(	(	kIx(	(
<g/>
Nightwish	Nightwish	k1gInSc1	Nightwish
<g/>
,	,	kIx,	,
Within	Within	k2eAgInSc1d1	Within
Temptation	Temptation	k1gInSc1	Temptation
<g/>
,	,	kIx,	,
Amberian	Amberian	k1gMnSc1	Amberian
Dawn	Dawn	k1gMnSc1	Dawn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
nástroje	nástroj	k1gInPc1	nástroj
jej	on	k3xPp3gMnSc4	on
podkreslují	podkreslovat	k5eAaImIp3nP	podkreslovat
a	a	k8xC	a
dodávají	dodávat	k5eAaImIp3nP	dodávat
dramatičnost	dramatičnost	k1gFnSc1	dramatičnost
<g/>
,	,	kIx,	,
mužský	mužský	k2eAgInSc1d1	mužský
hlas	hlas	k1gInSc1	hlas
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
ke	k	k7c3	k
zdrsnění	zdrsnění	k1gNnSc3	zdrsnění
ve	v	k7c6	v
dvojzpěvech	dvojzpěv	k1gInPc6	dvojzpěv
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
převzata	převzít	k5eAaPmNgFnS	převzít
z	z	k7c2	z
klasických	klasický	k2eAgFnPc2d1	klasická
oper	opera	k1gFnPc2	opera
nebo	nebo	k8xC	nebo
muzikálových	muzikálový	k2eAgNnPc2d1	muzikálové
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
upravena	upravit	k5eAaPmNgFnS	upravit
do	do	k7c2	do
daného	daný	k2eAgInSc2d1	daný
hudebního	hudební	k2eAgInSc2d1	hudební
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
asi	asi	k9	asi
nejčastěji	často	k6eAd3	často
bývá	bývat	k5eAaImIp3nS	bývat
hrán	hrát	k5eAaImNgInS	hrát
Fantom	fantom	k1gInSc1	fantom
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
používá	používat	k5eAaImIp3nS	používat
hudbu	hudba	k1gFnSc4	hudba
i	i	k9	i
jiných	jiný	k2eAgNnPc2d1	jiné
etnik	etnikum	k1gNnPc2	etnikum
–	–	k?	–
arabskou	arabský	k2eAgFnSc4d1	arabská
<g/>
,	,	kIx,	,
indickou	indický	k2eAgFnSc4d1	indická
<g/>
,	,	kIx,	,
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
nebo	nebo	k8xC	nebo
germánskou	germánský	k2eAgFnSc4d1	germánská
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Hollenthon	Hollenthon	k1gInSc1	Hollenthon
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
inspirovanou	inspirovaný	k2eAgFnSc7d1	inspirovaná
renesancí	renesance	k1gFnSc7	renesance
nebo	nebo	k8xC	nebo
barokem	baroko	k1gNnSc7	baroko
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Haggard	Haggard	k1gInSc1	Haggard
Jako	jako	k8xS	jako
nejčastější	častý	k2eAgInPc1d3	nejčastější
nástroje	nástroj	k1gInPc1	nástroj
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgFnPc4d1	používána
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
Hammondovy	Hammondův	k2eAgFnPc1d1	Hammondova
varhany	varhany	k1gFnPc1	varhany
<g/>
,	,	kIx,	,
tympány	tympán	k1gInPc1	tympán
<g/>
,	,	kIx,	,
flétna	flétna	k1gFnSc1	flétna
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgFnPc1d1	elektrická
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnPc1d1	bicí
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
skladba	skladba	k1gFnSc1	skladba
či	či	k8xC	či
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
skutečné	skutečný	k2eAgNnSc4d1	skutečné
operní	operní	k2eAgNnSc4d1	operní
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zvlášť	zvlášť	k6eAd1	zvlášť
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
sbory	sbor	k1gInPc1	sbor
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
zpěvy	zpěv	k1gInPc1	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
škálou	škála	k1gFnSc7	škála
zaměření	zaměření	k1gNnSc2	zaměření
–	–	k?	–
od	od	k7c2	od
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
příroda	příroda	k1gFnSc1	příroda
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgInPc4d1	náboženský
motivy	motiv	k1gInPc4	motiv
a	a	k8xC	a
příběhy	příběh	k1gInPc4	příběh
až	až	k9	až
po	po	k7c4	po
běžné	běžný	k2eAgInPc4d1	běžný
lidské	lidský	k2eAgInPc4d1	lidský
problémy	problém	k1gInPc4	problém
aj.	aj.	kA	aj.
Texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
psány	psát	k5eAaImNgInP	psát
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
nebo	nebo	k8xC	nebo
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
italštině	italština	k1gFnSc6	italština
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
češtině	čeština	k1gFnSc6	čeština
aj.	aj.	kA	aj.
Symfonický	symfonický	k2eAgInSc4d1	symfonický
black	black	k1gInSc4	black
metal	metat	k5eAaImAgInS	metat
Symfonický	symfonický	k2eAgInSc1d1	symfonický
power	power	k1gInSc1	power
metal	metat	k5eAaImAgInS	metat
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
symphonicmetalových	symphonicmetalův	k2eAgFnPc2d1	symphonicmetalův
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Wishmasters	Wishmasters	k1gInSc1	Wishmasters
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
Agnus	Agnus	k1gMnSc1	Agnus
Dei	Dei	k1gMnSc1	Dei
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
Avantasia	Avantasia	k1gFnSc1	Avantasia
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
Avidity	Avidita	k1gFnPc1	Avidita
for	forum	k1gNnPc2	forum
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
Almora	Almora	k1gFnSc1	Almora
(	(	kIx(	(
<g/>
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
)	)	kIx)	)
Amberian	Amberian	k1gInSc1	Amberian
Dawn	Dawn	k1gNnSc1	Dawn
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
Delain	Delain	k1gInSc1	Delain
(	(	kIx(	(
<g/>
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
)	)	kIx)	)
Edenbridge	Edenbridge	k1gFnSc1	Edenbridge
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
Epica	Epica	k1gFnSc1	Epica
(	(	kIx(	(
<g/>
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
)	)	kIx)	)
Fairyland	Fairyland	k1gInSc1	Fairyland
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
Haggard	Haggard	k1gInSc1	Haggard
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
Interitus	Interitus	k1gInSc1	Interitus
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
Lacrimosa	Lacrimosa	k1gFnSc1	Lacrimosa
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
Leaves	Leaves	k1gInSc1	Leaves
<g/>
'	'	kIx"	'
Eyes	Eyes	k1gInSc1	Eyes
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
/	/	kIx~	/
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
Luca	Luca	k1gMnSc1	Luca
Turilli	Turille	k1gFnSc4	Turille
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
Nightwish	Nightwish	k1gInSc1	Nightwish
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
Rhapsody	Rhapsoda	k1gFnPc1	Rhapsoda
of	of	k?	of
Fire	Fire	k1gFnSc1	Fire
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Rhapsody	Rhapsoda	k1gFnPc4	Rhapsoda
<g/>
)	)	kIx)	)
Secret	Secret	k1gMnSc1	Secret
Sphere	Spher	k1gInSc5	Spher
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
Tarja	Tarj	k1gInSc2	Tarj
Turunen	Turunen	k1gInSc1	Turunen
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
Therion	Therion	k1gInSc1	Therion
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
Thy	Thy	k1gFnSc1	Thy
Majestie	Majestie	k1gFnSc1	Majestie
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Victor	Victor	k1gMnSc1	Victor
Smolski	Smolsk	k1gFnSc2	Smolsk
(	(	kIx(	(
<g/>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
)	)	kIx)	)
Wuthering	Wuthering	k1gInSc1	Wuthering
Heights	Heights	k1gInSc1	Heights
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
Anette	Anett	k1gMnSc5	Anett
Olzon	Olzon	k1gNnSc1	Olzon
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
White	Whit	k1gMnSc5	Whit
Light	Light	k1gInSc1	Light
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
Within	Within	k2eAgInSc1d1	Within
Temptation	Temptation	k1gInSc1	Temptation
(	(	kIx(	(
<g/>
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
)	)	kIx)	)
Welicoruss	Welicoruss	k1gInSc1	Welicoruss
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
/	/	kIx~	/
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
symphonicmetalových	symphonicmetalův	k2eAgFnPc2d1	symphonicmetalův
skupin	skupina	k1gFnPc2	skupina
Gothic	Gothice	k1gFnPc2	Gothice
metal	metat	k5eAaImAgInS	metat
Speed	Speed	k1gInSc1	Speed
metal	metal	k1gInSc1	metal
Death	Death	k1gInSc4	Death
metal	metat	k5eAaImAgInS	metat
Power	Power	k1gInSc1	Power
metal	metal	k1gInSc1	metal
Orchestr	orchestr	k1gInSc1	orchestr
Opera	opera	k1gFnSc1	opera
Středověká	středověký	k2eAgFnSc1d1	středověká
hudba	hudba	k1gFnSc1	hudba
–	–	k?	–
obecně	obecně	k6eAd1	obecně
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
symfonický	symfonický	k2eAgInSc4d1	symfonický
metal	metal	k1gInSc4	metal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Metalová	metalový	k2eAgFnSc1d1	metalová
mapa	mapa	k1gFnSc1	mapa
Evropy	Evropa	k1gFnSc2	Evropa
Encyclopaedia	Encyclopaedium	k1gNnSc2	Encyclopaedium
Metallum	Metallum	k1gInSc1	Metallum
</s>
