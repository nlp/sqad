<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začaly	začít	k5eAaPmAgFnP	začít
po	po	k7c6	po
boku	bok	k1gInSc6	bok
trashmetalových	trashmetalův	k2eAgMnPc2d1	trashmetalův
a	a	k8xC	a
deathmetalových	deathmetalův	k2eAgMnPc2d1	deathmetalův
velikánů	velikán	k1gMnPc2	velikán
vznikat	vznikat	k5eAaImF	vznikat
kapely	kapela	k1gFnPc4	kapela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
začaly	začít	k5eAaPmAgFnP	začít
hrát	hrát	k5eAaImF	hrát
nový	nový	k2eAgInSc4d1	nový
a	a	k8xC	a
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
velice	velice	k6eAd1	velice
neobvyklý	obvyklý	k2eNgInSc1d1	neobvyklý
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
doom	doom	k6eAd1	doom
metal	metat	k5eAaImAgInS	metat
(	(	kIx(	(
<g/>
Paradise	Paradise	k1gFnSc1	Paradise
Lost	Lost	k1gInSc1	Lost
<g/>
,	,	kIx,	,
Anathema	anathema	k1gNnSc1	anathema
nebo	nebo	k8xC	nebo
velice	velice	k6eAd1	velice
temná	temnat	k5eAaImIp3nS	temnat
<g/>
,	,	kIx,	,
až	až	k8xS	až
smrtící	smrtící	k2eAgFnSc1d1	smrtící
doomová	doomový	k2eAgFnSc1d1	doomový
kapela	kapela	k1gFnSc1	kapela
My	my	k3xPp1nPc1	my
Dying	Dying	k1gInSc1	Dying
Bride	Brid	k1gInSc5	Brid
<g/>
;	;	kIx,	;
v	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
skladbách	skladba	k1gFnPc6	skladba
znějící	znějící	k2eAgFnPc1d1	znějící
housle	housle	k1gFnPc1	housle
přinášejí	přinášet	k5eAaImIp3nP	přinášet
velice	velice	k6eAd1	velice
tesknou	teskný	k2eAgFnSc4d1	teskná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
