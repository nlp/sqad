<s>
Spinosaurus	Spinosaurus	k1gInSc1	Spinosaurus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Trnitý	trnitý	k2eAgMnSc1d1	trnitý
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
obrovský	obrovský	k2eAgMnSc1d1	obrovský
masožravý	masožravý	k2eAgMnSc1d1	masožravý
dinosaurus	dinosaurus	k1gMnSc1	dinosaurus
(	(	kIx(	(
<g/>
teropod	teropod	k1gInSc1	teropod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
v	v	k7c6	v
křídových	křídový	k2eAgFnPc6d1	křídová
periodách	perioda	k1gFnPc6	perioda
albu	album	k1gNnSc6	album
a	a	k8xC	a
cenomanu	cenoman	k1gMnSc3	cenoman
před	před	k7c7	před
asi	asi	k9	asi
112	[number]	k4	112
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
