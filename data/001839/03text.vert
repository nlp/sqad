<s>
Spinosaurus	Spinosaurus	k1gInSc1	Spinosaurus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Trnitý	trnitý	k2eAgMnSc1d1	trnitý
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
obrovský	obrovský	k2eAgMnSc1d1	obrovský
masožravý	masožravý	k2eAgMnSc1d1	masožravý
dinosaurus	dinosaurus	k1gMnSc1	dinosaurus
(	(	kIx(	(
<g/>
teropod	teropod	k1gInSc1	teropod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
v	v	k7c6	v
křídových	křídový	k2eAgFnPc6d1	křídová
periodách	perioda	k1gFnPc6	perioda
albu	album	k1gNnSc6	album
a	a	k8xC	a
cenomanu	cenoman	k1gMnSc3	cenoman
před	před	k7c7	před
asi	asi	k9	asi
112	[number]	k4	112
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
největšího	veliký	k2eAgMnSc4d3	veliký
známého	známý	k1gMnSc4	známý
dravého	dravý	k2eAgMnSc2d1	dravý
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
tohoto	tento	k3xDgMnSc4	tento
teropoda	teropod	k1gMnSc4	teropod
výrazně	výrazně	k6eAd1	výrazně
proměnil	proměnit	k5eAaPmAgMnS	proměnit
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žil	žít	k5eAaImAgMnS	žít
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
netypicky	typicky	k6eNd1	typicky
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
čtyřech	čtyři	k4xCgFnPc6	čtyři
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
zadní	zadní	k2eAgFnPc4d1	zadní
končetiny	končetina	k1gFnPc4	končetina
měl	mít	k5eAaImAgInS	mít
relativně	relativně	k6eAd1	relativně
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgNnSc1d1	krátké
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
dal	dát	k5eAaPmAgMnS	dát
Sasso	Sassa	k1gFnSc5	Sassa
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
právě	právě	k9	právě
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
největším	veliký	k2eAgMnSc7d3	veliký
známým	známý	k2eAgMnSc7d1	známý
teropodním	teropodní	k2eAgMnSc7d1	teropodní
dinosaurem	dinosaurus	k1gMnSc7	dinosaurus
<g/>
,	,	kIx,	,
větším	veliký	k2eAgMnSc7d2	veliký
než	než	k8xS	než
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
držitel	držitel	k1gMnSc1	držitel
primátu	primát	k1gInSc2	primát
Giganotosaurus	Giganotosaurus	k1gMnSc1	Giganotosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
udávané	udávaný	k2eAgFnPc1d1	udávaná
rozměry	rozměra	k1gFnPc1	rozměra
tohoto	tento	k3xDgMnSc2	tento
dravého	dravý	k2eAgMnSc2d1	dravý
obra	obr	k1gMnSc2	obr
činí	činit	k5eAaImIp3nS	činit
15	[number]	k4	15
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
metrů	metr	k1gInPc2	metr
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
kolem	kolem	k7c2	kolem
9	[number]	k4	9
tun	tuna	k1gFnPc2	tuna
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
odhady	odhad	k1gInPc1	odhad
posouvají	posouvat	k5eAaImIp3nP	posouvat
hmotnost	hmotnost	k1gFnSc4	hmotnost
až	až	k9	až
k	k	k7c3	k
neuvěřitelným	uvěřitelný	k2eNgFnPc3d1	neuvěřitelná
hodnotám	hodnota	k1gFnPc3	hodnota
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
samotné	samotný	k2eAgFnSc6d1	samotná
hranici	hranice	k1gFnSc6	hranice
únosnosti	únosnost	k1gFnSc2	únosnost
pro	pro	k7c4	pro
dvounohého	dvounohý	k2eAgMnSc4d1	dvounohý
živočicha	živočich	k1gMnSc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Jenom	jenom	k9	jenom
lebka	lebka	k1gFnSc1	lebka
jednoho	jeden	k4xCgInSc2	jeden
exempláře	exemplář	k1gInSc2	exemplář
by	by	kYmCp3nS	by
prý	prý	k9	prý
v	v	k7c6	v
kompletním	kompletní	k2eAgInSc6d1	kompletní
stavu	stav	k1gInSc6	stav
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
až	až	k6eAd1	až
2,4	[number]	k4	2,4
metru	metr	k1gInSc2	metr
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
oficiální	oficiální	k2eAgInPc1d1	oficiální
odhady	odhad	k1gInPc1	odhad
spíš	spíš	k9	spíš
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
1,75	[number]	k4	1,75
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nS	by
Spinosaurus	Spinosaurus	k1gMnSc1	Spinosaurus
představoval	představovat	k5eAaImAgMnS	představovat
zdaleka	zdaleka	k6eAd1	zdaleka
největšího	veliký	k2eAgMnSc4d3	veliký
dravého	dravý	k2eAgMnSc4d1	dravý
tvora	tvor	k1gMnSc4	tvor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kdy	kdy	k6eAd1	kdy
chodil	chodit	k5eAaImAgMnS	chodit
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
(	(	kIx(	(
<g/>
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc1	jehož
fosílie	fosílie	k1gFnPc1	fosílie
již	již	k6eAd1	již
známe	znát	k5eAaImIp1nP	znát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgFnPc1d1	charakteristická
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
zádové	zádový	k2eAgInPc4d1	zádový
výběžky	výběžek	k1gInPc4	výběžek
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnPc1d1	tvořící
jakési	jakýsi	k3yIgInPc4	jakýsi
"	"	kIx"	"
<g/>
trny	trn	k1gInPc4	trn
<g/>
"	"	kIx"	"
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
až	až	k9	až
přes	přes	k7c4	přes
2	[number]	k4	2
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
za	za	k7c2	za
života	život	k1gInSc2	život
zvířete	zvíře	k1gNnSc2	zvíře
zřejmě	zřejmě	k6eAd1	zřejmě
sloužící	sloužící	k2eAgMnSc1d1	sloužící
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kožní	kožní	k2eAgFnSc2d1	kožní
plachty	plachta	k1gFnSc2	plachta
<g/>
,	,	kIx,	,
napnuté	napnutý	k2eAgFnSc2d1	napnutá
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
výběžky	výběžek	k1gInPc7	výběžek
<g/>
,	,	kIx,	,
k	k	k7c3	k
termoregulaci	termoregulace	k1gFnSc3	termoregulace
<g/>
.	.	kIx.	.
</s>
<s>
Spinosaurus	Spinosaurus	k1gMnSc1	Spinosaurus
žil	žít	k5eAaImAgMnS	žít
nejspíš	nejspíš	k9	nejspíš
na	na	k7c4	na
březích	březí	k2eAgNnPc2d1	březí
křídových	křídový	k2eAgNnPc2d1	křídové
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
potoků	potok	k1gInPc2	potok
a	a	k8xC	a
lovil	lovit	k5eAaImAgMnS	lovit
velké	velký	k2eAgFnPc4d1	velká
sladkovodní	sladkovodní	k2eAgFnPc4d1	sladkovodní
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
možná	možná	k9	možná
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
částečně	částečně	k6eAd1	částečně
i	i	k9	i
mršinami	mršina	k1gFnPc7	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
utváření	utváření	k1gNnSc1	utváření
jeho	jeho	k3xOp3gMnPc2	jeho
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
čelistí	čelist	k1gFnPc2	čelist
<g/>
,	,	kIx,	,
vhodných	vhodný	k2eAgInPc2d1	vhodný
k	k	k7c3	k
lovení	lovení	k1gNnSc3	lovení
ryb	ryba	k1gFnPc2	ryba
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Obojživelný	obojživelný	k2eAgInSc1d1	obojživelný
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
také	také	k9	také
výzkum	výzkum	k1gInSc1	výzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
samozřejmě	samozřejmě	k6eAd1	samozřejmě
vyloučit	vyloučit	k5eAaPmF	vyloučit
ani	ani	k8xC	ani
potravu	potrava	k1gFnSc4	potrava
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
živých	živý	k2eAgFnPc2d1	živá
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
velkých	velký	k2eAgMnPc2d1	velký
býložravých	býložravý	k2eAgMnPc2d1	býložravý
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
před	před	k7c7	před
95	[number]	k4	95
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
publikována	publikován	k2eAgFnSc1d1	publikována
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zcela	zcela	k6eAd1	zcela
mění	měnit	k5eAaImIp3nS	měnit
pohled	pohled	k1gInSc4	pohled
na	na	k7c6	na
ekologii	ekologie	k1gFnSc6	ekologie
tohoto	tento	k3xDgMnSc2	tento
predátora	predátor	k1gMnSc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
obojživelného	obojživelný	k2eAgMnSc4d1	obojživelný
predátora	predátor	k1gMnSc4	predátor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
chodil	chodit	k5eAaImAgMnS	chodit
jen	jen	k9	jen
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
fosilie	fosilie	k1gFnSc1	fosilie
tohoto	tento	k3xDgMnSc2	tento
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
objevené	objevený	k2eAgFnSc2d1	objevená
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
byly	být	k5eAaImAgFnP	být
ztraceny	ztratit	k5eAaPmNgFnP	ztratit
při	při	k7c6	při
válečném	válečný	k2eAgInSc6d1	válečný
bombovém	bombový	k2eAgInSc6d1	bombový
náletu	nálet	k1gInSc6	nálet
na	na	k7c4	na
mnichovské	mnichovský	k2eAgNnSc4d1	mnichovské
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Spinosaurus	Spinosaurus	k1gMnSc1	Spinosaurus
je	být	k5eAaImIp3nS	být
nejpopulárnějším	populární	k2eAgMnSc7d3	nejpopulárnější
a	a	k8xC	a
nejnebezpečnějším	bezpečný	k2eNgMnSc7d3	nejnebezpečnější
dinosaurem	dinosaurus	k1gMnSc7	dinosaurus
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
3	[number]	k4	3
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
scéně	scéna	k1gFnSc6	scéna
dokonce	dokonce	k9	dokonce
zabije	zabít	k5eAaPmIp3nS	zabít
tyranosaura	tyranosaura	k1gFnSc1	tyranosaura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizních	televizní	k2eAgInPc6d1	televizní
dokumentech	dokument	k1gInPc6	dokument
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
především	především	k9	především
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
díle	dílo	k1gNnSc6	dílo
dokumentu	dokument	k1gInSc2	dokument
Megazvířata	Megazvířat	k1gMnSc2	Megazvířat
nebo	nebo	k8xC	nebo
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
filmu	film	k1gInSc2	film
Tajuplní	tajuplný	k2eAgMnPc1d1	tajuplný
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
karcharodontosaurem	karcharodontosaur	k1gInSc7	karcharodontosaur
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
1	[number]	k4	1
<g/>
.	.	kIx.	.
dílu	dílo	k1gNnSc3	dílo
dokumentu	dokument	k1gInSc2	dokument
Planet	planeta	k1gFnPc2	planeta
Dinosaur	Dinosaur	k1gMnSc1	Dinosaur
jménem	jméno	k1gNnSc7	jméno
Ztracený	ztracený	k2eAgInSc1d1	ztracený
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
podle	podle	k7c2	podle
nejnovějších	nový	k2eAgInPc2d3	nejnovější
poznatků	poznatek	k1gInPc2	poznatek
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
obojživelný	obojživelný	k2eAgMnSc1d1	obojživelný
<g/>
"	"	kIx"	"
dinosaurus	dinosaurus	k1gMnSc1	dinosaurus
živící	živící	k2eAgMnSc1d1	živící
se	se	k3xPyFc4	se
rybami	ryba	k1gFnPc7	ryba
<g/>
.	.	kIx.	.
↑	↑	k?	↑
http://dinosaurusblog.wordpress.com/2015/09/21/dinosauri-velikostni-rekordmani/	[url]	k4	http://dinosaurusblog.wordpress.com/2015/09/21/dinosauri-velikostni-rekordmani/
↑	↑	k?	↑
http://dinosaurusblog.wordpress.com/2010/02/15/835604-obojzivelni-spinosauri/	[url]	k4	http://dinosaurusblog.wordpress.com/2010/02/15/835604-obojzivelni-spinosauri/
↑	↑	k?	↑
http://dinosaurusblog.wordpress.com/2014/09/18/predstavuje-se-novy-spinosaurus/	[url]	k4	http://dinosaurusblog.wordpress.com/2014/09/18/predstavuje-se-novy-spinosaurus/
↑	↑	k?	↑
http://dinosaurusblog.wordpress.com/2009/04/24/782233-konec-egyptskych-dinosauru/	[url]	k4	http://dinosaurusblog.wordpress.com/2009/04/24/782233-konec-egyptskych-dinosauru/
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Spinosaurus	Spinosaurus	k1gInSc4	Spinosaurus
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc1	Commons
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
spinosaurovi	spinosaur	k1gMnSc6	spinosaur
Článek	článek	k1gInSc4	článek
o	o	k7c6	o
pravěkých	pravěký	k2eAgMnPc6d1	pravěký
predátorech	predátor	k1gMnPc6	predátor
http://www.nhm.ac.uk/jdsml/nature-online/dino-directory/detail.dsml?Genus=Spinosaurus	[url]	k1gInSc1	http://www.nhm.ac.uk/jdsml/nature-online/dino-directory/detail.dsml?Genus=Spinosaurus
http://www.livescience.com/animals/060301_big_carnivores.html	[url]	k5eAaPmAgInS	http://www.livescience.com/animals/060301_big_carnivores.html
Český	český	k2eAgInSc1d1	český
článek	článek	k1gInSc1	článek
o	o	k7c6	o
největších	veliký	k2eAgInPc6d3	veliký
teropodech	teropod	k1gInPc6	teropod
(	(	kIx(	(
<g/>
Hradecký	hradecký	k2eAgInSc1d1	hradecký
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
