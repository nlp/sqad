<s>
Románské	románský	k2eAgInPc4d1	románský
jazyky	jazyk	k1gInPc4	jazyk
je	být	k5eAaImIp3nS	být
rodina	rodina	k1gFnSc1	rodina
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
latina	latina	k1gFnSc1	latina
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
také	také	k9	také
jako	jako	k9	jako
novolatinské	novolatinský	k2eAgNnSc1d1	novolatinský
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jazyky	jazyk	k1gInPc1	jazyk
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
především	především	k9	především
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
(	(	kIx(	(
<g/>
západo	západa	k1gFnSc5	západa
<g/>
)	)	kIx)	)
římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
latina	latina	k1gFnSc1	latina
byla	být	k5eAaImAgFnS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
přitom	přitom	k6eAd1	přitom
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
původními	původní	k2eAgInPc7d1	původní
jazyky	jazyk	k1gInPc7	jazyk
obsazených	obsazený	k2eAgNnPc2d1	obsazené
území	území	k1gNnPc2	území
a	a	k8xC	a
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
tedy	tedy	k9	tedy
vznikaly	vznikat	k5eAaImAgFnP	vznikat
různé	různý	k2eAgFnPc1d1	různá
pidžiny	pidžina	k1gFnPc1	pidžina
nebo	nebo	k8xC	nebo
kreolské	kreolský	k2eAgInPc1d1	kreolský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Romanskými	Romanský	k2eAgInPc7d1	Romanský
jazyky	jazyk	k1gInPc7	jazyk
mluví	mluvit	k5eAaImIp3nP	mluvit
800	[number]	k4	800
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
západorománské	západorománský	k2eAgInPc1d1	západorománský
jazyky	jazyk	k1gInPc1	jazyk
ibersko-románské	iberskoománský	k2eAgInPc1d1	ibersko-románský
jazyky	jazyk	k1gInPc1	jazyk
galicijsko-portugalské	galicijskoortugalský	k2eAgInPc1d1	galicijsko-portugalský
jazyky	jazyk	k1gInPc1	jazyk
galicijština	galicijština	k1gFnSc1	galicijština
-	-	kIx~	-
3	[number]	k4	3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluvčích	mluvčí	k1gMnPc2	mluvčí
v	v	k7c6	v
Galicii	Galicie	k1gFnSc6	Galicie
falština	falština	k1gFnSc1	falština
-	-	kIx~	-
10	[number]	k4	10
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluvčích	mluvčí	k1gMnPc2	mluvčí
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
portugalština	portugalština	k1gFnSc1	portugalština
-	-	kIx~	-
230	[number]	k4	230
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluvčích	mluvčí	k1gMnPc2	mluvčí
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
a	a	k8xC	a
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
26	[number]	k4	26
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
judeo-portugalština	judeoortugalština	k1gFnSc1	judeo-portugalština
-	-	kIx~	-
vymřelá	vymřelý	k2eAgFnSc1d1	vymřelá
aragonština	aragonština	k1gFnSc1	aragonština
-	-	kIx~	-
10	[number]	k4	10
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
v	v	k7c6	v
Aragonu	Aragon	k1gInSc6	Aragon
asturština	asturština	k1gFnSc1	asturština
(	(	kIx(	(
<g/>
mirandština	mirandština	k1gFnSc1	mirandština
<g/>
,	,	kIx,	,
leónština	leónština	k1gFnSc1	leónština
<g/>
)	)	kIx)	)
extremadurština	extremadurština	k1gFnSc1	extremadurština
-	-	kIx~	-
v	v	k7c6	v
Extremaduře	Extremadura	k1gFnSc6	Extremadura
španělština	španělština	k1gFnSc1	španělština
-	-	kIx~	-
360	[number]	k4	360
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluvčích	mluvčí	k1gMnPc2	mluvčí
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
ladino	ladin	k2eAgNnSc1d1	ladin
-	-	kIx~	-
židovský	židovský	k2eAgInSc4d1	židovský
dialekt	dialekt	k1gInSc4	dialekt
španělštiny	španělština	k1gFnSc2	španělština
<g />
.	.	kIx.	.
</s>
<s>
portuñ	portuñ	k?	portuñ
(	(	kIx(	(
<g/>
portunhol	portunhol	k1gInSc1	portunhol
<g/>
,	,	kIx,	,
fronterizo	fronteriza	k1gFnSc5	fronteriza
<g/>
)	)	kIx)	)
-	-	kIx~	-
okolo	okolo	k7c2	okolo
100	[number]	k4	100
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
mluvčích	mluvčí	k1gFnPc2	mluvčí
v	v	k7c4	v
Uruguay	Uruguay	k1gFnSc4	Uruguay
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
Brazílii	Brazílie	k1gFnSc4	Brazílie
gallo-románské	galloománský	k2eAgInPc1d1	gallo-románský
jazyky	jazyk	k1gInPc1	jazyk
okcitánsko-katalánské	okcitánskoatalánský	k2eAgInPc1d1	okcitánsko-katalánský
jazyky	jazyk	k1gInPc1	jazyk
katalánština	katalánština	k1gFnSc1	katalánština
-	-	kIx~	-
6,5	[number]	k4	6,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluvčích	mluvčí	k1gMnPc2	mluvčí
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Andorře	Andorra	k1gFnSc6	Andorra
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
okcitánština	okcitánština	k1gFnSc1	okcitánština
-	-	kIx~	-
2	[number]	k4	2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluvčích	mluvčí	k1gMnPc2	mluvčí
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Francii	Francie	k1gFnSc4	Francie
franko-provensálština	frankorovensálština	k1gFnSc1	franko-provensálština
-	-	kIx~	-
především	především	k9	především
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Lyon	Lyon	k1gInSc4	Lyon
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
rétorománské	rétorománský	k2eAgInPc4d1	rétorománský
jazyky	jazyk	k1gInPc4	jazyk
furlanština	furlanština	k1gFnSc1	furlanština
-	-	kIx~	-
Furlansko-Julské	Furlansko-Julský	k2eAgNnSc1d1	Furlansko-Julské
Benátsko	Benátsko	k1gNnSc1	Benátsko
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
atd.	atd.	kA	atd.
ladynština	ladynština	k1gFnSc1	ladynština
-	-	kIx~	-
Dolomity	Dolomity	k1gInPc1	Dolomity
rétorománština	rétorománština	k1gFnSc1	rétorománština
-	-	kIx~	-
66	[number]	k4	66
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluvčích	mluvčí	k1gMnPc2	mluvčí
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
oï	oï	k?	oï
jazyky	jazyk	k1gInPc4	jazyk
poitevin-saintongeaiština	poitevinaintongeaiština	k1gFnSc1	poitevin-saintongeaiština
burgundština	burgundština	k1gFnSc1	burgundština
champenoiština	champenoiština	k1gFnSc1	champenoiština
franko-komtoiština	frankoomtoiština	k1gFnSc1	franko-komtoiština
lotrinština	lotrinština	k1gFnSc1	lotrinština
francouzština	francouzština	k1gFnSc1	francouzština
-	-	kIx~	-
70	[number]	k4	70
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
12	[number]	k4	12
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
galština	galština	k1gFnSc1	galština
normanština	normanština	k1gFnSc1	normanština
jerseyština	jerseyština	k1gFnSc1	jerseyština
-	-	kIx~	-
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Jersey	Jersea	k1gFnSc2	Jersea
picardština	picardština	k1gFnSc1	picardština
valonština	valonština	k1gFnSc1	valonština
gallo-italské	gallotalský	k2eAgFnPc4d1	gallo-italský
jazyky	jazyk	k1gInPc4	jazyk
ligurština	ligurština	k1gFnSc1	ligurština
monégasqština	monégasqština	k1gFnSc1	monégasqština
piemontština	piemontština	k1gFnSc1	piemontština
lombardština	lombardština	k1gFnSc1	lombardština
emilijština	emilijština	k1gFnSc1	emilijština
venetština	venetština	k1gFnSc1	venetština
dalmatština	dalmatština	k1gFnSc1	dalmatština
-	-	kIx~	-
vymřelý	vymřelý	k2eAgInSc1d1	vymřelý
jazyk	jazyk	k1gInSc1	jazyk
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
východorománské	východorománský	k2eAgInPc1d1	východorománský
jazyky	jazyk	k1gInPc1	jazyk
italské	italský	k2eAgInPc1d1	italský
jazyky	jazyk	k1gInPc1	jazyk
italština	italština	k1gFnSc1	italština
-	-	kIx~	-
60	[number]	k4	60
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
korsičtina	korsičtina	k1gFnSc1	korsičtina
neapolština	neapolština	k1gFnSc1	neapolština
-	-	kIx~	-
8	[number]	k4	8
mil	míle	k1gFnPc2	míle
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
středojižní	středojižní	k2eAgFnSc6d1	středojižní
Itálii	Itálie	k1gFnSc6	Itálie
sicilština	sicilština	k1gFnSc1	sicilština
-	-	kIx~	-
5	[number]	k4	5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
romanesco	romanesco	k1gMnSc1	romanesco
istrijština	istrijština	k1gFnSc1	istrijština
judeo-italština	judeotalština	k1gFnSc1	judeo-italština
-	-	kIx~	-
4	[number]	k4	4
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
rumunština	rumunština	k1gFnSc1	rumunština
arumunština	arumunština	k1gFnSc1	arumunština
jihorománské	jihorománský	k2eAgMnPc4d1	jihorománský
jazyky	jazyk	k1gMnPc4	jazyk
africká	africký	k2eAgFnSc1d1	africká
románština	románština	k1gFnSc1	románština
-	-	kIx~	-
vymřelý	vymřelý	k2eAgInSc1d1	vymřelý
jazyk	jazyk	k1gInSc1	jazyk
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
a	a	k8xC	a
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
mozarabština	mozarabština	k1gFnSc1	mozarabština
-	-	kIx~	-
vymřelý	vymřelý	k2eAgInSc1d1	vymřelý
jazyk	jazyk	k1gInSc1	jazyk
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
sardština	sardština	k1gFnSc1	sardština
-	-	kIx~	-
300	[number]	k4	300
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
např.	např.	kA	např.
jihoitalské	jihoitalský	k2eAgInPc1d1	jihoitalský
dialekty	dialekt	k1gInPc1	dialekt
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
samostatně	samostatně	k6eAd1	samostatně
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
lingvistického	lingvistický	k2eAgNnSc2d1	lingvistické
a	a	k8xC	a
komparativního	komparativní	k2eAgNnSc2d1	komparativní
hlediska	hledisko	k1gNnSc2	hledisko
považovat	považovat	k5eAaImF	považovat
také	také	k9	také
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jako	jako	k9	jako
takové	takový	k3xDgFnPc1	takový
nejsou	být	k5eNaImIp3nP	být
uznány	uznat	k5eAaPmNgFnP	uznat
<g/>
.	.	kIx.	.
</s>
