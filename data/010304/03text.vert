<p>
<s>
Březovské	Březovský	k2eAgInPc1d1	Březovský
vodovody	vodovod	k1gInPc1	vodovod
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Březovské	Březovský	k2eAgInPc1d1	Březovský
přivaděče	přivaděč	k1gInPc1	přivaděč
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
soustava	soustava	k1gFnSc1	soustava
dvou	dva	k4xCgInPc2	dva
vodovodních	vodovodní	k2eAgMnPc2d1	vodovodní
přivaděčů	přivaděč	k1gInPc2	přivaděč
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
z	z	k7c2	z
prameniště	prameniště	k1gNnSc2	prameniště
u	u	k7c2	u
Březové	březový	k2eAgFnSc2d1	Březová
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
březovský	březovský	k2eAgInSc1d1	březovský
vodovod	vodovod	k1gInSc1	vodovod
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
letech	let	k1gInPc6	let
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
kolem	kolem	k7c2	kolem
řeky	řeka	k1gFnSc2	řeka
Svitavy	Svitava	k1gFnSc2	Svitava
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
funkční	funkční	k2eAgNnSc1d1	funkční
dosud	dosud	k6eAd1	dosud
<g/>
,	,	kIx,	,
kapacitnější	kapacitný	k2eAgInSc4d2	kapacitný
Druhý	druhý	k4xOgInSc4	druhý
březovský	březovský	k2eAgInSc4d1	březovský
vodovod	vodovod	k1gInSc4	vodovod
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
u	u	k7c2	u
Letovic	Letovice	k1gFnPc2	Letovice
odpojuje	odpojovat	k5eAaImIp3nS	odpojovat
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starý	k2eAgMnSc2d2	starší
sourozence	sourozenec	k1gMnSc2	sourozenec
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
je	být	k5eAaImIp3nS	být
trasován	trasovat	k5eAaImNgInS	trasovat
Boskovickou	boskovický	k2eAgFnSc7d1	boskovická
brázdou	brázda	k1gFnSc7	brázda
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatelem	provozovatel	k1gMnSc7	provozovatel
Březovských	Březovský	k2eAgInPc2d1	Březovský
vodovodů	vodovod	k1gInPc2	vodovod
je	být	k5eAaImIp3nS	být
firma	firma	k1gFnSc1	firma
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
vodárny	vodárna	k1gFnSc2	vodárna
a	a	k8xC	a
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
,	,	kIx,	,
majitelem	majitel	k1gMnSc7	majitel
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
potřebovalo	potřebovat	k5eAaImAgNnS	potřebovat
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
blízké	blízký	k2eAgFnPc1d1	blízká
obce	obec	k1gFnPc1	obec
dodat	dodat	k5eAaPmF	dodat
29,3	[number]	k4	29,3
milionů	milion	k4xCgInPc2	milion
m3	m3	k4	m3
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
67,5	[number]	k4	67,5
%	%	kIx~	%
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
objemu	objem	k1gInSc2	objem
zajistil	zajistit	k5eAaPmAgInS	zajistit
zdroj	zdroj	k1gInSc1	zdroj
II	II	kA	II
<g/>
.	.	kIx.	.
březovského	březovský	k2eAgInSc2d1	březovský
vodovodu	vodovod	k1gInSc2	vodovod
<g/>
,	,	kIx,	,
27,0	[number]	k4	27,0
%	%	kIx~	%
zdroj	zdroj	k1gInSc4	zdroj
I.	I.	kA	I.
březovského	březovský	k2eAgInSc2d1	březovský
vodovodu	vodovod	k1gInSc2	vodovod
a	a	k8xC	a
5,4	[number]	k4	5,4
%	%	kIx~	%
pocházelo	pocházet	k5eAaImAgNnS	pocházet
z	z	k7c2	z
Vírského	vírský	k2eAgInSc2d1	vírský
oblastního	oblastní	k2eAgInSc2d1	oblastní
vodovodu	vodovod	k1gInSc2	vodovod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
I.	I.	kA	I.
březovský	březovský	k2eAgInSc1d1	březovský
vodovod	vodovod	k1gInSc1	vodovod
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
nebylo	být	k5eNaImAgNnS	být
zásobování	zásobování	k1gNnSc4	zásobování
Brna	Brno	k1gNnSc2	Brno
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
vyhovující	vyhovující	k2eAgFnSc4d1	vyhovující
zejména	zejména	k9	zejména
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
město	město	k1gNnSc1	město
hledalo	hledat	k5eAaImAgNnS	hledat
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
náhradu	náhrada	k1gFnSc4	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgInPc1	první
návrhy	návrh	k1gInPc1	návrh
o	o	k7c4	o
využití	využití	k1gNnSc4	využití
zdrojové	zdrojový	k2eAgFnSc2d1	zdrojová
oblasti	oblast	k1gFnSc2	oblast
u	u	k7c2	u
Březové	březový	k2eAgFnSc2d1	Březová
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
brněnská	brněnský	k2eAgFnSc1d1	brněnská
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
schválila	schválit	k5eAaPmAgFnS	schválit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
na	na	k7c4	na
Banínské	Banínský	k2eAgInPc4d1	Banínský
prameny	pramen	k1gInPc4	pramen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyvěrají	vyvěrat	k5eAaImIp3nP	vyvěrat
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Svitavy	Svitava	k1gFnSc2	Svitava
poblíž	poblíž	k6eAd1	poblíž
Muzlova	Muzlův	k2eAgFnSc1d1	Muzlův
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
hygienické	hygienický	k2eAgFnSc2d1	hygienická
kontroly	kontrola	k1gFnSc2	kontrola
byly	být	k5eAaImAgInP	být
dobré	dobré	k1gNnSc4	dobré
<g/>
,	,	kIx,	,
zdejší	zdejší	k2eAgInSc4d1	zdejší
zdroj	zdroj	k1gInSc4	zdroj
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
využívat	využívat	k5eAaImF	využívat
pro	pro	k7c4	pro
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
bylo	být	k5eAaImAgNnS	být
stavebním	stavební	k2eAgInSc7d1	stavební
podnikem	podnik	k1gInSc7	podnik
Karla	Karel	k1gMnSc2	Karel
von	von	k1gInSc1	von
Schwarze	Schwarz	k1gMnSc2	Schwarz
za	za	k7c4	za
částku	částka	k1gFnSc4	částka
70	[number]	k4	70
000	[number]	k4	000
korun	koruna	k1gFnPc2	koruna
realizováno	realizovat	k5eAaBmNgNnS	realizovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
pokusné	pokusný	k2eAgFnPc4d1	pokusná
jímací	jímací	k2eAgNnPc4d1	jímací
zařízení	zařízení	k1gNnPc4	zařízení
(	(	kIx(	(
<g/>
štola	štola	k1gFnSc1	štola
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
300	[number]	k4	300
m	m	kA	m
s	s	k7c7	s
jímacími	jímací	k2eAgFnPc7d1	jímací
studnami	studna	k1gFnPc7	studna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
od	od	k7c2	od
září	září	k1gNnSc2	září
1906	[number]	k4	1906
do	do	k7c2	do
března	březen	k1gInSc2	březen
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
probíhalo	probíhat	k5eAaImAgNnS	probíhat
zkušební	zkušební	k2eAgNnSc1d1	zkušební
čerpání	čerpání	k1gNnSc1	čerpání
250	[number]	k4	250
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
získaná	získaný	k2eAgFnSc1d1	získaná
voda	voda	k1gFnSc1	voda
byla	být	k5eAaImAgFnS	být
odváděna	odvádět	k5eAaImNgFnS	odvádět
do	do	k7c2	do
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
250	[number]	k4	250
litrů	litr	k1gInPc2	litr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
budoucí	budoucí	k2eAgFnSc2d1	budoucí
potřeby	potřeba	k1gFnSc2	potřeba
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
podle	podle	k7c2	podle
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
propočtů	propočet	k1gInPc2	propočet
a	a	k8xC	a
plánovaného	plánovaný	k2eAgInSc2d1	plánovaný
růstu	růst	k1gInSc2	růst
města	město	k1gNnSc2	město
dostatečně	dostatečně	k6eAd1	dostatečně
postačovat	postačovat	k5eAaImF	postačovat
na	na	k7c4	na
delší	dlouhý	k2eAgNnSc4d2	delší
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
přivaděče	přivaděč	k1gInSc2	přivaděč
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
schválen	schválit	k5eAaPmNgInS	schválit
a	a	k8xC	a
po	po	k7c6	po
dořešení	dořešení	k1gNnSc6	dořešení
sporů	spor	k1gInPc2	spor
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
obcemi	obec	k1gFnPc7	obec
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
budováním	budování	k1gNnSc7	budování
započato	započnout	k5eAaPmNgNnS	započnout
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
;	;	kIx,	;
rozpočet	rozpočet	k1gInSc1	rozpočet
činil	činit	k5eAaImAgInS	činit
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
(	(	kIx(	(
<g/>
další	další	k2eAgInPc4d1	další
náklady	náklad	k1gInPc4	náklad
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
nucené	nucený	k2eAgNnSc1d1	nucené
přeložení	přeložení	k1gNnSc1	přeložení
řečiště	řečiště	k1gNnSc2	řečiště
Svitavy	Svitava	k1gFnSc2	Svitava
ve	v	k7c6	v
zdrojové	zdrojový	k2eAgFnSc6d1	zdrojová
oblasti	oblast	k1gFnSc6	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dodavateli	dodavatel	k1gMnPc7	dodavatel
litinových	litinový	k2eAgFnPc2d1	litinová
trub	trouba	k1gFnPc2	trouba
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
firmy	firma	k1gFnPc1	firma
<g/>
:	:	kIx,	:
Vítkovická	vítkovický	k2eAgFnSc1d1	Vítkovická
hornická	hornický	k2eAgFnSc1d1	hornická
a	a	k8xC	a
hutnická	hutnický	k2eAgFnSc1d1	Hutnická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
Pražská	pražský	k2eAgFnSc1d1	Pražská
železářská	železářský	k2eAgFnSc1d1	železářská
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
společnost	společnost	k1gFnSc1	společnost
z	z	k7c2	z
Králova	Králův	k2eAgInSc2d1	Králův
Dvora	Dvůr	k1gInSc2	Dvůr
<g/>
,	,	kIx,	,
Důlní	důlní	k2eAgFnSc1d1	důlní
a	a	k8xC	a
železářská	železářský	k2eAgFnSc1d1	železářská
společnost	společnost	k1gFnSc1	společnost
z	z	k7c2	z
Węgierske	Węgiersk	k1gFnSc2	Węgiersk
Górky	Górka	k1gFnSc2	Górka
a	a	k8xC	a
Strojírenská	strojírenský	k2eAgFnSc1d1	strojírenská
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
Breitfeld	Breitfeld	k1gMnSc1	Breitfeld
<g/>
,	,	kIx,	,
Daněk	Daněk	k1gMnSc1	Daněk
&	&	k?	&
Co	co	k9	co
<g/>
.	.	kIx.	.
z	z	k7c2	z
Blanska	Blansko	k1gNnSc2	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
trubka	trubka	k1gFnSc1	trubka
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
země	zem	k1gFnSc2	zem
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Vodoprávní	vodoprávní	k2eAgNnSc4d1	vodoprávní
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
čerpání	čerpání	k1gNnSc3	čerpání
250	[number]	k4	250
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
kolaudace	kolaudace	k1gFnSc1	kolaudace
stavby	stavba	k1gFnSc2	stavba
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
vodovod	vodovod	k1gInSc1	vodovod
"	"	kIx"	"
<g/>
Die	Die	k1gMnSc1	Die
Kaiser	Kaiser	k1gMnSc1	Kaiser
Franz	Franz	k1gMnSc1	Franz
Joseph	Joseph	k1gMnSc1	Joseph
I.	I.	kA	I.
<g/>
-Trinkwasserleitung	-Trinkwasserleitung	k1gMnSc1	-Trinkwasserleitung
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgInSc4d1	pojmenovaný
podle	podle	k7c2	podle
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
také	také	k9	také
přispěl	přispět	k5eAaPmAgMnS	přispět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
zprovozněn	zprovoznit	k5eAaPmNgInS	zprovoznit
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
používán	používat	k5eAaImNgInS	používat
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
březovský	březovský	k2eAgMnSc1d1	březovský
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
brněnský	brněnský	k2eAgInSc1d1	brněnský
vodovod	vodovod	k1gInSc1	vodovod
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
povolení	povolení	k1gNnSc1	povolení
k	k	k7c3	k
odběru	odběr	k1gInSc3	odběr
300	[number]	k4	300
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
k	k	k7c3	k
reálnému	reálný	k2eAgNnSc3d1	reálné
navýšení	navýšení	k1gNnSc3	navýšení
čerpání	čerpání	k1gNnSc2	čerpání
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgNnPc4d1	platné
doposud	doposud	k6eAd1	doposud
<g/>
.	.	kIx.	.
</s>
<s>
Vodovod	vodovod	k1gInSc1	vodovod
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
modernizován	modernizován	k2eAgInSc1d1	modernizován
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
elektrifikován	elektrifikovat	k5eAaBmNgMnS	elektrifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
manipulace	manipulace	k1gFnSc1	manipulace
se	s	k7c7	s
šoupaty	šoupě	k1gNnPc7	šoupě
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kapacita	kapacita	k1gFnSc1	kapacita
přivaděče	přivaděč	k1gInSc2	přivaděč
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
naplno	naplno	k6eAd1	naplno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stavby	stavba	k1gFnSc2	stavba
II	II	kA	II
<g/>
.	.	kIx.	.
vodovodu	vodovod	k1gInSc6	vodovod
původní	původní	k2eAgInSc1d1	původní
vodovod	vodovod	k1gInSc1	vodovod
napojen	napojen	k2eAgInSc1d1	napojen
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
společného	společný	k2eAgInSc2d1	společný
řídicího	řídicí	k2eAgInSc2d1	řídicí
vodojemu	vodojem	k1gInSc2	vodojem
v	v	k7c4	v
Březové	březový	k2eAgNnSc4d1	Březové
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
se	se	k3xPyFc4	se
však	však	k9	však
snížil	snížit	k5eAaPmAgInS	snížit
výškový	výškový	k2eAgInSc1d1	výškový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
gravitační	gravitační	k2eAgFnSc1d1	gravitační
kapacita	kapacita	k1gFnSc1	kapacita
přivaděče	přivaděč	k1gInSc2	přivaděč
byla	být	k5eAaImAgFnS	být
snížena	snížit	k5eAaPmNgFnS	snížit
na	na	k7c4	na
současnou	současný	k2eAgFnSc4d1	současná
hodnotu	hodnota	k1gFnSc4	hodnota
264	[number]	k4	264
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Podle	podle	k7c2	podle
zkoušek	zkouška	k1gFnPc2	zkouška
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
použito	použít	k5eAaPmNgNnS	použít
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
litinové	litinový	k2eAgNnSc1d1	litinové
potrubí	potrubí	k1gNnSc1	potrubí
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
další	další	k2eAgFnSc1d1	další
kontrola	kontrola	k1gFnSc1	kontrola
jeho	on	k3xPp3gInSc2	on
stavu	stav	k1gInSc2	stav
bude	být	k5eAaImBp3nS	být
potřeba	potřeba	k6eAd1	potřeba
až	až	k9	až
za	za	k7c4	za
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Popis	popis	k1gInSc4	popis
===	===	k?	===
</s>
</p>
<p>
<s>
Puklinové	puklinový	k2eAgFnPc1d1	puklinová
podzemní	podzemní	k2eAgFnPc1d1	podzemní
vody	voda	k1gFnPc1	voda
křídových	křídový	k2eAgFnPc2d1	křídová
vrstev	vrstva	k1gFnPc2	vrstva
jsou	být	k5eAaImIp3nP	být
získávány	získávat	k5eAaImNgInP	získávat
pomocí	pomocí	k7c2	pomocí
14	[number]	k4	14
vrtaných	vrtaný	k2eAgFnPc2d1	vrtaná
studní	studna	k1gFnPc2	studna
o	o	k7c6	o
hloubkách	hloubka	k1gFnPc6	hloubka
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
m	m	kA	m
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
13	[number]	k4	13
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
ve	v	k7c4	v
300	[number]	k4	300
m	m	kA	m
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
štole	štola	k1gFnSc6	štola
s	s	k7c7	s
cihelnou	cihelný	k2eAgFnSc7d1	cihelná
obezdívkou	obezdívka	k1gFnSc7	obezdívka
<g/>
,	,	kIx,	,
čtrnáctá	čtrnáctý	k4xOgFnSc1	čtrnáctý
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
samostatném	samostatný	k2eAgInSc6d1	samostatný
objektu	objekt	k1gInSc6	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
sváděna	svádět	k5eAaImNgFnS	svádět
násoskovým	násoskový	k2eAgInSc7d1	násoskový
řadem	řad	k1gInSc7	řad
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
do	do	k7c2	do
vodojemu	vodojem	k1gInSc2	vodojem
v	v	k7c4	v
Březové	březový	k2eAgMnPc4d1	březový
(	(	kIx(	(
<g/>
objem	objem	k1gInSc1	objem
5000	[number]	k4	5000
m3	m3	k4	m3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nP	mísit
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
získanou	získaný	k2eAgFnSc7d1	získaná
ve	v	k7c6	v
studních	studně	k1gFnPc6	studně
II	II	kA	II
<g/>
.	.	kIx.	.
březovského	březovský	k2eAgInSc2d1	březovský
vodovodu	vodovod	k1gInSc2	vodovod
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rezervoár	rezervoár	k1gInSc1	rezervoár
byl	být	k5eAaImAgInS	být
zprovozněn	zprovoznit	k5eAaPmNgInS	zprovoznit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
čerpaná	čerpaný	k2eAgFnSc1d1	čerpaná
voda	voda	k1gFnSc1	voda
vedena	vést	k5eAaImNgFnS	vést
přivaděčem	přivaděč	k1gInSc7	přivaděč
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
<g/>
Trasa	trasa	k1gFnSc1	trasa
I.	I.	kA	I.
březovského	březovský	k2eAgInSc2d1	březovský
vodovodu	vodovod	k1gInSc2	vodovod
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
podél	podél	k7c2	podél
Svitavy	Svitava	k1gFnSc2	Svitava
až	až	k9	až
do	do	k7c2	do
Bílovic	Bílovice	k1gInPc2	Bílovice
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
odklání	odklánět	k5eAaImIp3nP	odklánět
a	a	k8xC	a
prudkým	prudký	k2eAgNnSc7d1	prudké
stoupáním	stoupání	k1gNnSc7	stoupání
míří	mířit	k5eAaImIp3nS	mířit
do	do	k7c2	do
vodojemu	vodojem	k1gInSc2	vodojem
v	v	k7c4	v
Brno-Lesné	Brno-Lesný	k2eAgMnPc4d1	Brno-Lesný
na	na	k7c6	na
Holých	Holých	k2eAgFnPc6d1	Holých
horách	hora	k1gFnPc6	hora
(	(	kIx(	(
<g/>
objem	objem	k1gInSc4	objem
14	[number]	k4	14
669	[number]	k4	669
m3	m3	k4	m3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výškový	výškový	k2eAgInSc1d1	výškový
rozdíl	rozdíl	k1gInSc1	rozdíl
hladiny	hladina	k1gFnSc2	hladina
podzemních	podzemní	k2eAgFnPc2d1	podzemní
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
Březové	březový	k2eAgFnSc6d1	Březová
a	a	k8xC	a
vodojemu	vodojem	k1gInSc2	vodojem
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
89	[number]	k4	89
m	m	kA	m
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
místo	místo	k7c2	místo
přivaděče	přivaděč	k1gInSc2	přivaděč
(	(	kIx(	(
<g/>
220	[number]	k4	220
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Bílovicích	Bílovice	k1gInPc6	Bílovice
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
vodovodu	vodovod	k1gInSc2	vodovod
činí	činit	k5eAaImIp3nS	činit
57,496	[number]	k4	57,496
km	km	kA	km
<g/>
,	,	kIx,	,
tvořen	tvořen	k2eAgInSc1d1	tvořen
je	být	k5eAaImIp3nS	být
litinovými	litinový	k2eAgFnPc7d1	litinová
trubkami	trubka	k1gFnPc7	trubka
uloženými	uložený	k2eAgFnPc7d1	uložená
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
hloubce	hloubka	k1gFnSc6	hloubka
2	[number]	k4	2
m	m	kA	m
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
přivaděč	přivaděč	k1gInSc4	přivaděč
vyraženy	vyražen	k2eAgFnPc1d1	vyražena
mezi	mezi	k7c7	mezi
Blanskem	Blansko	k1gNnSc7	Blansko
a	a	k8xC	a
Bílovicemi	Bílovice	k1gInPc7	Bílovice
tři	tři	k4xCgFnPc4	tři
štoly	štola	k1gFnPc4	štola
o	o	k7c6	o
délkách	délka	k1gFnPc6	délka
74	[number]	k4	74
m	m	kA	m
(	(	kIx(	(
<g/>
severně	severně	k6eAd1	severně
od	od	k7c2	od
Nového	Nového	k2eAgInSc2d1	Nového
hradu	hrad	k1gInSc2	hrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
305	[number]	k4	305
m	m	kA	m
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
Novým	nový	k2eAgInSc7d1	nový
hradem	hrad	k1gInSc7	hrad
<g/>
)	)	kIx)	)
a	a	k8xC	a
614	[number]	k4	614
m	m	kA	m
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
Hrádkem	Hrádok	k1gInSc7	Hrádok
u	u	k7c2	u
Bílovic	Bílovice	k1gInPc2	Bílovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
realizováno	realizován	k2eAgNnSc1d1	realizováno
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
šest	šest	k4xCc1	šest
odlehčovacích	odlehčovací	k2eAgFnPc2d1	odlehčovací
trub	trouba	k1gFnPc2	trouba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
pro	pro	k7c4	pro
tu	ten	k3xDgFnSc4	ten
v	v	k7c6	v
Letovicích	Letovice	k1gFnPc6	Letovice
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
věž	věž	k1gFnSc1	věž
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
20	[number]	k4	20
m.	m.	k?	m.
Součástí	součást	k1gFnSc7	součást
vodovodu	vodovod	k1gInSc2	vodovod
také	také	k9	také
je	být	k5eAaImIp3nS	být
17	[number]	k4	17
sekčních	sekční	k2eAgNnPc2d1	sekční
šoupat	šoupě	k1gNnPc2	šoupě
<g/>
,	,	kIx,	,
100	[number]	k4	100
vzdušníků	vzdušník	k1gInPc2	vzdušník
a	a	k8xC	a
v	v	k7c6	v
Bílovicích	Bílovice	k1gInPc6	Bílovice
se	se	k3xPyFc4	se
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
53	[number]	k4	53
kalosvodů	kalosvod	k1gInPc2	kalosvod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přivaděče	přivaděč	k1gInSc2	přivaděč
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
odebírat	odebírat	k5eAaImF	odebírat
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
Letovicích	Letovice	k1gFnPc6	Letovice
a	a	k8xC	a
v	v	k7c6	v
Adamově	Adamov	k1gInSc6	Adamov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
II	II	kA	II
<g/>
.	.	kIx.	.
březovský	březovský	k2eAgInSc1d1	březovský
vodovod	vodovod	k1gInSc1	vodovod
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
kapacitnějšího	kapacitný	k2eAgInSc2d2	kapacitný
vodovodu	vodovod	k1gInSc2	vodovod
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
již	již	k6eAd1	již
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
průzkumy	průzkum	k1gInPc1	průzkum
oblasti	oblast	k1gFnSc2	oblast
mezi	mezi	k7c7	mezi
Letovicemi	Letovice	k1gFnPc7	Letovice
a	a	k8xC	a
Opatovem	Opatov	k1gInSc7	Opatov
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc4d1	další
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
přímo	přímo	k6eAd1	přímo
v	v	k7c4	v
Březové	březový	k2eAgFnPc4d1	Březová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
realizováno	realizovat	k5eAaBmNgNnS	realizovat
28	[number]	k4	28
studní	studna	k1gFnPc2	studna
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
také	také	k9	také
sedm	sedm	k4xCc1	sedm
jímacích	jímací	k2eAgInPc2d1	jímací
vrtů	vrt	k1gInPc2	vrt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výpočtů	výpočet	k1gInPc2	výpočet
by	by	kYmCp3nS	by
čerpání	čerpání	k1gNnSc2	čerpání
těchto	tento	k3xDgFnPc2	tento
vod	voda	k1gFnPc2	voda
snížilo	snížit	k5eAaPmAgNnS	snížit
průtok	průtok	k1gInSc4	průtok
na	na	k7c6	na
dolní	dolní	k2eAgFnSc6d1	dolní
Svitavě	Svitava	k1gFnSc6	Svitava
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jako	jako	k9	jako
zásobní	zásobní	k2eAgFnSc1d1	zásobní
ochrana	ochrana	k1gFnSc1	ochrana
pro	pro	k7c4	pro
dobu	doba	k1gFnSc4	doba
minimálních	minimální	k2eAgInPc2d1	minimální
průtoků	průtok	k1gInPc2	průtok
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vybudována	vybudován	k2eAgNnPc1d1	vybudováno
na	na	k7c6	na
svitavském	svitavský	k2eAgInSc6d1	svitavský
přítoku	přítok	k1gInSc6	přítok
Křetínce	Křetínka	k1gFnSc6	Křetínka
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Letovice	Letovice	k1gFnSc2	Letovice
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
II	II	kA	II
<g/>
.	.	kIx.	.
březovského	březovský	k2eAgInSc2d1	březovský
vodovodu	vodovod	k1gInSc2	vodovod
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
skutečné	skutečný	k2eAgFnPc1d1	skutečná
práce	práce	k1gFnPc1	práce
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1972	[number]	k4	1972
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1975	[number]	k4	1975
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
přivaděč	přivaděč	k1gInSc1	přivaděč
zprovozněn	zprovozněn	k2eAgInSc1d1	zprovozněn
<g/>
,	,	kIx,	,
plný	plný	k2eAgInSc1d1	plný
provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
objektů	objekt	k1gInPc2	objekt
sedmi	sedm	k4xCc2	sedm
hlubokých	hluboký	k2eAgMnPc2d1	hluboký
jímacích	jímací	k2eAgMnPc2d1	jímací
vrtů	vrt	k1gInPc2	vrt
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
také	také	k9	také
provedeny	proveden	k2eAgFnPc1d1	provedena
práce	práce	k1gFnPc1	práce
proti	proti	k7c3	proti
znečištění	znečištění	k1gNnSc3	znečištění
prameniště	prameniště	k1gNnSc2	prameniště
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
2,7	[number]	k4	2,7
km	km	kA	km
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
nového	nový	k2eAgNnSc2d1	nové
koryta	koryto	k1gNnSc2	koryto
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
hodnota	hodnota	k1gFnSc1	hodnota
objemu	objem	k1gInSc2	objem
čerpání	čerpání	k1gNnSc2	čerpání
vod	voda	k1gFnPc2	voda
činí	činit	k5eAaImIp3nS	činit
780	[number]	k4	780
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
trasa	trasa	k1gFnSc1	trasa
vodovodu	vodovod	k1gInSc2	vodovod
napojena	napojit	k5eAaPmNgFnS	napojit
na	na	k7c4	na
vodojem	vodojem	k1gInSc4	vodojem
v	v	k7c6	v
Čebíně	Čebína	k1gFnSc6	Čebína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
míchání	míchání	k1gNnSc3	míchání
vod	voda	k1gFnPc2	voda
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
a	a	k8xC	a
z	z	k7c2	z
Vírského	vírský	k2eAgInSc2d1	vírský
oblastního	oblastní	k2eAgInSc2d1	oblastní
vodovodu	vodovod	k1gInSc2	vodovod
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
část	část	k1gFnSc1	část
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
trasy	trasa	k1gFnSc2	trasa
II	II	kA	II
<g/>
.	.	kIx.	.
březovského	březovský	k2eAgInSc2d1	březovský
přivaděče	přivaděč	k1gInSc2	přivaděč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Popis	popis	k1gInSc4	popis
===	===	k?	===
</s>
</p>
<p>
<s>
Voda	voda	k1gFnSc1	voda
pro	pro	k7c4	pro
II	II	kA	II
<g/>
.	.	kIx.	.
březovský	březovský	k2eAgInSc1d1	březovský
vodovod	vodovod	k1gInSc1	vodovod
je	být	k5eAaImIp3nS	být
získávána	získávat	k5eAaImNgFnS	získávat
z	z	k7c2	z
28	[number]	k4	28
vrtaných	vrtaný	k2eAgFnPc2d1	vrtaná
studní	studna	k1gFnPc2	studna
o	o	k7c6	o
hloubce	hloubka	k1gFnSc6	hloubka
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
m	m	kA	m
a	a	k8xC	a
ze	z	k7c2	z
sedm	sedm	k4xCc4	sedm
jímacích	jímací	k2eAgInPc2d1	jímací
vrtů	vrt	k1gInPc2	vrt
o	o	k7c6	o
hloubce	hloubka	k1gFnSc6	hloubka
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
m	m	kA	m
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
údolní	údolní	k2eAgFnSc6d1	údolní
nivě	niva	k1gFnSc6	niva
Svitavy	Svitava	k1gFnSc2	Svitava
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
bývalého	bývalý	k2eAgInSc2d1	bývalý
Muzlova	Muzlův	k2eAgInSc2d1	Muzlův
<g/>
.	.	kIx.	.
</s>
<s>
Čerpaná	čerpaný	k2eAgFnSc1d1	čerpaná
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
svedena	svést	k5eAaPmNgFnS	svést
do	do	k7c2	do
sběrného	sběrný	k2eAgInSc2d1	sběrný
řadu	řad	k1gInSc2	řad
a	a	k8xC	a
do	do	k7c2	do
společného	společný	k2eAgInSc2d1	společný
vodojemu	vodojem	k1gInSc2	vodojem
v	v	k7c4	v
Březové	březový	k2eAgFnPc4d1	Březová
(	(	kIx(	(
<g/>
zprovozněn	zprovozněn	k2eAgInSc4d1	zprovozněn
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
II	II	kA	II
<g/>
.	.	kIx.	.
přivaděče	přivaděč	k1gInSc2	přivaděč
je	být	k5eAaImIp3nS	být
zpočátku	zpočátku	k6eAd1	zpočátku
vedena	vést	k5eAaImNgFnS	vést
společně	společně	k6eAd1	společně
s	s	k7c7	s
trasou	trasa	k1gFnSc7	trasa
I.	I.	kA	I.
vodovodu	vodovod	k1gInSc2	vodovod
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
trasy	trasa	k1gFnPc1	trasa
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Sasina	Sasino	k1gNnSc2	Sasino
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
březovský	březovský	k2eAgInSc1d1	březovský
vodovod	vodovod	k1gInSc1	vodovod
odtud	odtud	k6eAd1	odtud
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
Boskovickou	boskovický	k2eAgFnSc7d1	boskovická
brázdou	brázda	k1gFnSc7	brázda
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
Lysicemi	Lysice	k1gFnPc7	Lysice
a	a	k8xC	a
Černou	černý	k2eAgFnSc7d1	černá
Horou	hora	k1gFnSc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Rozvodí	rozvodí	k1gNnSc1	rozvodí
u	u	k7c2	u
Malé	Malé	k2eAgFnSc2d1	Malé
Lhoty	Lhota	k1gFnSc2	Lhota
podchází	podcházet	k5eAaImIp3nS	podcházet
v	v	k7c4	v
2795	[number]	k4	2795
m	m	kA	m
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
ražené	ražený	k2eAgFnSc6d1	ražená
štole	štola	k1gFnSc6	štola
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
Malhostovice	Malhostovice	k1gFnPc4	Malhostovice
<g/>
,	,	kIx,	,
Kuřim	Kuřim	k1gInSc1	Kuřim
a	a	k8xC	a
Brno-Ivanovice	Brno-Ivanovice	k1gFnSc1	Brno-Ivanovice
do	do	k7c2	do
vodojemu	vodojem	k1gInSc2	vodojem
na	na	k7c6	na
Palackého	Palackého	k2eAgInSc6d1	Palackého
vrchu	vrch	k1gInSc6	vrch
(	(	kIx(	(
<g/>
objem	objem	k1gInSc1	objem
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
17	[number]	k4	17
500	[number]	k4	500
m3	m3	k4	m3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Výškový	výškový	k2eAgInSc1d1	výškový
rozdíl	rozdíl	k1gInSc1	rozdíl
hladin	hladina	k1gFnPc2	hladina
vodojemů	vodojem	k1gInPc2	vodojem
v	v	k7c6	v
Březové	březový	k2eAgFnSc6d1	Březová
a	a	k8xC	a
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
činí	činit	k5eAaImIp3nS	činit
66,5	[number]	k4	66,5
m	m	kA	m
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgInSc4d1	maximální
průtok	průtok	k1gInSc4	průtok
přivaděčem	přivaděč	k1gInSc7	přivaděč
1140	[number]	k4	1140
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
vodovodu	vodovod	k1gInSc2	vodovod
tvořeného	tvořený	k2eAgInSc2d1	tvořený
ocelovými	ocelový	k2eAgFnPc7d1	ocelová
trubkami	trubka	k1gFnPc7	trubka
je	být	k5eAaImIp3nS	být
55,357	[number]	k4	55,357
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
také	také	k9	také
12	[number]	k4	12
sekčních	sekční	k2eAgFnPc2d1	sekční
klapek	klapka	k1gFnPc2	klapka
<g/>
,	,	kIx,	,
66	[number]	k4	66
kalosvodů	kalosvod	k1gInPc2	kalosvod
<g/>
,	,	kIx,	,
64	[number]	k4	64
automatických	automatický	k2eAgInPc2d1	automatický
vzdušníků	vzdušník	k1gInPc2	vzdušník
a	a	k8xC	a
aktivní	aktivní	k2eAgFnSc1d1	aktivní
katodová	katodový	k2eAgFnSc1d1	katodová
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
.	.	kIx.	.
<g/>
Propojení	propojení	k1gNnSc1	propojení
do	do	k7c2	do
vodojemu	vodojem	k1gInSc2	vodojem
Čebín	Čebín	k1gInSc1	Čebín
(	(	kIx(	(
<g/>
objem	objem	k1gInSc1	objem
8500	[number]	k4	8500
m3	m3	k4	m3
<g/>
)	)	kIx)	)
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
maximální	maximální	k2eAgInSc4d1	maximální
průtok	průtok	k1gInSc4	průtok
938	[number]	k4	938
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
vydatnosti	vydatnost	k1gFnSc6	vydatnost
je	být	k5eAaImIp3nS	být
přítok	přítok	k1gInSc1	přítok
do	do	k7c2	do
Čebína	Čebín	k1gInSc2	Čebín
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
mimo	mimo	k7c4	mimo
vodojem	vodojem	k1gInSc4	vodojem
přes	přes	k7c4	přes
čerpací	čerpací	k2eAgFnSc4d1	čerpací
stanici	stanice	k1gFnSc4	stanice
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Březovské	Březovský	k2eAgInPc1d1	Březovský
přivaděče	přivaděč	k1gInPc1	přivaděč
<g/>
,	,	kIx,	,
bvk	bvk	k?	bvk
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
VELEŠÍK	VELEŠÍK	kA	VELEŠÍK
<g/>
,	,	kIx,	,
Marian	Mariana	k1gFnPc2	Mariana
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
březovských	březovský	k2eAgInPc2d1	březovský
vodovodů	vodovod	k1gInPc2	vodovod
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
36	[number]	k4	36
s.	s.	k?	s.
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
–	–	k?	–
Katedra	katedra	k1gFnSc1	katedra
geografie	geografie	k1gFnSc1	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Renata	Renata	k1gFnSc1	Renata
Pavelková	Pavelková	k1gFnSc1	Pavelková
Chmelová	Chmelová	k1gFnSc1	Chmelová
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
