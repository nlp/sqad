<s>
Březovské	Březovský	k2eAgInPc1d1	Březovský
vodovody	vodovod	k1gInPc1	vodovod
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Březovské	Březovský	k2eAgInPc1d1	Březovský
přivaděče	přivaděč	k1gInPc1	přivaděč
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
soustava	soustava	k1gFnSc1	soustava
dvou	dva	k4xCgInPc2	dva
vodovodních	vodovodní	k2eAgMnPc2d1	vodovodní
přivaděčů	přivaděč	k1gInPc2	přivaděč
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
z	z	k7c2	z
prameniště	prameniště	k1gNnSc2	prameniště
u	u	k7c2	u
Březové	březový	k2eAgFnSc2d1	Březová
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
