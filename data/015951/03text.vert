<s>
Brno-Černovice	Brno-Černovice	k1gFnSc1
</s>
<s>
Brno-Černovice	Brno-Černovice	k1gFnSc1
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Brno-Černovice	Brno-Černovice	k1gFnSc2
</s>
<s>
•	•	k?
Vlevo	vlevo	k6eAd1
1	#num#	k4
<g/>
:	:	kIx,
Psychiatrická	psychiatrický	k2eAgFnSc1d1
nemocnice	nemocnice	k1gFnSc1
</s>
<s>
•	•	k?
Vlevo	vlevo	k6eAd1
2	#num#	k4
<g/>
:	:	kIx,
Řeka	řeka	k1gFnSc1
Svitava	Svitava	k1gFnSc1
</s>
<s>
•	•	k?
Vlevo	vlevo	k6eAd1
3	#num#	k4
<g/>
:	:	kIx,
Turgeněvova	Turgeněvův	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
</s>
<s>
•	•	k?
Vpravo	vpravo	k6eAd1
1	#num#	k4
<g/>
:	:	kIx,
Húskova	Húskov	k1gInSc2
ulice	ulice	k1gFnSc2
</s>
<s>
•	•	k?
Vpravo	vpravo	k6eAd1
2	#num#	k4
<g/>
:	:	kIx,
Olomoucká	olomoucký	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
</s>
<s>
•	•	k?
Vpravo	vpravo	k6eAd1
3	#num#	k4
<g/>
:	:	kIx,
Ferrerova	Ferrerův	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
</s>
<s>
•	•	k?
Vpravo	vpravo	k6eAd1
4	#num#	k4
<g/>
:	:	kIx,
Textilní	textilní	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
společnosti	společnost	k1gFnSc2
Nová	nový	k2eAgFnSc1d1
Mosilana	Mosilana	k1gFnSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Brno	Brno	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Brno-město	Brno-město	k6eAd1
Kraj	kraj	k1gInSc1
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
Historická	historický	k2eAgFnSc1d1
země	zem	k1gFnPc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
58	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
13	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
8	#num#	k4
024	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
6,29	6,29	k4
km²	km²	k?
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Černovice	Černovice	k1gFnSc1
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
200	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
618	#num#	k4
00	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
985	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
10	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
úřadu	úřad	k1gInSc2
MČ	MČ	kA
</s>
<s>
Bolzanova	Bolzanův	k2eAgFnSc1d1
763	#num#	k4
<g/>
/	/	kIx~
<g/>
1618	#num#	k4
00	#num#	k4
Brno	Brno	k1gNnSc4
info@cernovice.brno.cz	info@cernovice.brno.cz	k1gMnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Petra	Petra	k1gFnSc1
Quittová	Quittová	k1gFnSc1
(	(	kIx(
<g/>
STAN	stan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.brno-cernovice.cz	www.brno-cernovice.cz	k1gInSc1
</s>
<s>
Brno-Černovice	Brno-Černovice	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
MČ	MČ	kA
</s>
<s>
551066	#num#	k4
Kód	kód	k1gInSc1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
411752	#num#	k4
Kód	kód	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
611263	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Brno-Černovice	Brno-Černovice	k1gFnSc1
je	být	k5eAaImIp3nS
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
městskou	městský	k2eAgFnSc7d1
čtvrtí	čtvrt	k1gFnSc7
Černovice	Černovice	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Czernowitz	Czernowitz	k1gInSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
Tschernowitz	Tschernowitz	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
původně	původně	k6eAd1
samostatnou	samostatný	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
k	k	k7c3
Brnu	Brno	k1gNnSc3
připojena	připojit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
katastrální	katastrální	k2eAgNnSc1d1
území	území	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
6,29	6,29	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samosprávná	samosprávný	k2eAgFnSc1d1
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
8000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
účely	účel	k1gInPc4
senátních	senátní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
je	být	k5eAaImIp3nS
území	území	k1gNnSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-Černovice	Brno-Černovice	k1gFnPc1
zařazeno	zařadit	k5eAaPmNgNnS
do	do	k7c2
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc1
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sousedící	sousedící	k2eAgFnPc1d1
městské	městský	k2eAgFnPc1d1
části	část	k1gFnPc1
</s>
<s>
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Brno-Černovice	Brno-Černovice	k1gFnSc2
hraničí	hraničit	k5eAaImIp3nS
na	na	k7c6
východě	východ	k1gInSc6
s	s	k7c7
městskou	městský	k2eAgFnSc7d1
částí	část	k1gFnSc7
Brno-Slatina	Brno-Slatina	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
severu	sever	k1gInSc6
s	s	k7c7
městskou	městský	k2eAgFnSc7d1
částí	část	k1gFnSc7
Brno-Židenice	Brno-Židenice	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
západě	západ	k1gInSc6
s	s	k7c7
městskými	městský	k2eAgFnPc7d1
částmi	část	k1gFnPc7
Brno-střed	Brno-střed	k1gInSc1
a	a	k8xC
Brno-jih	Brno-jih	k1gInSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zde	zde	k6eAd1
její	její	k3xOp3gFnSc1
západní	západní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
po	po	k7c6
západním	západní	k2eAgInSc6d1
(	(	kIx(
<g/>
pravém	pravý	k2eAgInSc6d1
<g/>
)	)	kIx)
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Svitavy	Svitava	k1gFnSc2
(	(	kIx(
<g/>
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
ostatně	ostatně	k6eAd1
patrné	patrný	k2eAgNnSc1d1
z	z	k7c2
územního	územní	k2eAgInSc2d1
plánu	plán	k1gInSc2
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
pak	pak	k6eAd1
hraničí	hraničit	k5eAaImIp3nP
s	s	k7c7
městskou	městský	k2eAgFnSc7d1
částí	část	k1gFnSc7
Brno-Tuřany	Brno-Tuřan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Název	název	k1gInSc1
údajně	údajně	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
územím	území	k1gNnSc7
na	na	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
se	se	k3xPyFc4
čtvrť	čtvrť	k1gFnSc1
nachází	nacházet	k5eAaImIp3nS
<g/>
,	,	kIx,
oblast	oblast	k1gFnSc1
byla	být	k5eAaImAgFnS
kdysi	kdysi	k6eAd1
bažinatá	bažinatý	k2eAgFnSc1d1
<g/>
,	,	kIx,
dokládají	dokládat	k5eAaImIp3nP
to	ten	k3xDgNnSc1
i	i	k9
názvy	název	k1gInPc1
okolních	okolní	k2eAgFnPc2d1
čtvrtí	čtvrt	k1gFnPc2
<g/>
,	,	kIx,
Slatina	slatina	k1gFnSc1
a	a	k8xC
Komárov	Komárov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Černovice	Černovice	k1gFnSc1
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
černá	černý	k2eAgFnSc1d1
zem	zem	k1gFnSc1
nebo	nebo	k8xC
vzduch	vzduch	k1gInSc4
černý	černý	k2eAgInSc4d1
hejny	hejno	k1gNnPc7
komárů	komár	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
čtvrti	čtvrt	k1gFnSc2
</s>
<s>
Černovice	Černovice	k1gFnPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
menších	malý	k2eAgFnPc2d2
starých	starý	k2eAgFnPc2d1
a	a	k8xC
větších	veliký	k2eAgFnPc2d2
Nových	Nových	k2eAgFnPc2d1
Černovic	Černovice	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
kterými	který	k3yRgFnPc7,k3yIgFnPc7,k3yQgFnPc7
se	se	k3xPyFc4
nedaleko	nedaleko	k7c2
budovy	budova	k1gFnSc2
zdejšího	zdejší	k2eAgInSc2d1
textilního	textilní	k2eAgInSc2d1
kombinátu	kombinát	k1gInSc2
Nová	nový	k2eAgFnSc1d1
Mosilana	Mosilana	k1gFnSc1
nachází	nacházet	k5eAaImIp3nS
areál	areál	k1gInSc4
zdejší	zdejší	k2eAgFnSc2d1
psychiatrické	psychiatrický	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Nové	Nové	k2eAgFnPc1d1
Černovice	Černovice	k1gFnPc1
mají	mít	k5eAaImIp3nP
městský	městský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
s	s	k7c7
převážně	převážně	k6eAd1
prvorepublikovými	prvorepublikový	k2eAgInPc7d1
domy	dům	k1gInPc7
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
staré	starý	k2eAgFnPc4d1
Černovice	Černovice	k1gFnPc4
vesnický	vesnický	k2eAgInSc1d1
charakter	charakter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
převážně	převážně	k6eAd1
zemědělsky	zemědělsky	k6eAd1
a	a	k8xC
zahrádkářsky	zahrádkářsky	k6eAd1
zaměřené	zaměřený	k2eAgFnPc4d1
Černovice	Černovice	k1gFnPc4
se	se	k3xPyFc4
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
okrajových	okrajový	k2eAgFnPc6d1
částech	část	k1gFnPc6
mění	měnit	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
v	v	k7c4
průmyslovou	průmyslový	k2eAgFnSc4d1
čtvrť	čtvrť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
změna	změna	k1gFnSc1
bývalého	bývalý	k2eAgNnSc2d1
černovického	černovický	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
v	v	k7c4
průmyslovou	průmyslový	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
Černovické	Černovický	k2eAgFnSc2d1
terasy	terasa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozporuplnost	rozporuplnost	k1gFnSc1
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
dokládá	dokládat	k5eAaImIp3nS
vcelku	vcelku	k6eAd1
blízké	blízký	k2eAgNnSc4d1
sousedství	sousedství	k1gNnSc4
brněnské	brněnský	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
skládky	skládka	k1gFnSc2
Černovice	Černovice	k1gFnSc2
a	a	k8xC
malého	malý	k2eAgInSc2d1
kousku	kousek	k1gInSc2
původní	původní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
Černovický	Černovický	k2eAgInSc1d1
hájek	hájek	k1gInSc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
však	však	k9
již	již	k6eAd1
k	k	k7c3
Černovicím	Černovice	k1gFnPc3
nenáleží	náležet	k5eNaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3
historie	historie	k1gFnSc1
a	a	k8xC
urbanistický	urbanistický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Černovice	Černovice	k1gFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
před	před	k7c4
více	hodně	k6eAd2
než	než	k8xS
700	#num#	k4
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1645	#num#	k4
při	při	k7c6
obléhání	obléhání	k1gNnSc6
Brna	Brno	k1gNnSc2
švédskými	švédský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
byla	být	k5eAaImAgFnS
obec	obec	k1gFnSc1
značně	značně	k6eAd1
poškozena	poškodit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severně	severně	k6eAd1
od	od	k7c2
starých	starý	k2eAgFnPc2d1
Černovic	Černovice	k1gFnPc2
byla	být	k5eAaImAgFnS
v	v	k7c6
letech	léto	k1gNnPc6
1861-1863	1861-1863	k4
vybudována	vybudován	k2eAgFnSc1d1
zemská	zemský	k2eAgFnSc1d1
psychiatrická	psychiatrický	k2eAgFnSc1d1
léčebna	léčebna	k1gFnSc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Psychiatrická	psychiatrický	k2eAgFnSc1d1
nemocnice	nemocnice	k1gFnSc1
Brno	Brno	k1gNnSc4
<g/>
)	)	kIx)
s	s	k7c7
osově	osově	k6eAd1
komponovanými	komponovaný	k2eAgInPc7d1
novorenesančními	novorenesanční	k2eAgInPc7d1
objekty	objekt	k1gInPc7
<g/>
,	,	kIx,
ležící	ležící	k2eAgInPc1d1
v	v	k7c6
„	„	k?
<g/>
zelené	zelená	k1gFnSc6
<g/>
“	“	k?
části	část	k1gFnSc2
této	tento	k3xDgFnSc2
čtvrti	čtvrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vznikala	vznikat	k5eAaImAgFnS
zástavba	zástavba	k1gFnSc1
Nových	Nových	k2eAgFnPc2d1
Černovic	Černovice	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
dotvořena	dotvořit	k5eAaPmNgFnS
během	během	k7c2
období	období	k1gNnSc2
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
vznikla	vzniknout	k5eAaPmAgFnS
na	na	k7c6
východě	východ	k1gInSc6
tehdejšího	tehdejší	k2eAgInSc2d1
černovického	černovický	k2eAgInSc2d1
katastru	katastr	k1gInSc2
nouzová	nouzový	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
Černovičky	Černovička	k1gFnSc2
<g/>
,	,	kIx,
náležející	náležející	k2eAgFnSc2d1
od	od	k7c2
roku	rok	k1gInSc2
1969	#num#	k4
ke	k	k7c3
katastru	katastr	k1gInSc3
sousední	sousední	k2eAgFnSc2d1
Slatiny	slatina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1959	#num#	k4
začalo	začít	k5eAaPmAgNnS
na	na	k7c6
východním	východní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Nových	Nových	k2eAgFnPc2d1
Černovic	Černovice	k1gFnPc2
v	v	k7c6
prostoru	prostor	k1gInSc6
dnešní	dnešní	k2eAgFnSc2d1
Kneslovy	Kneslův	k2eAgFnSc2d1
a	a	k8xC
Krausovy	Krausův	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
vznikat	vznikat	k5eAaImF
vůbec	vůbec	k9
první	první	k4xOgNnSc4
brněnské	brněnský	k2eAgNnSc4d1
panelové	panelový	k2eAgNnSc4d1
sídliště	sídliště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Připojení	připojení	k1gNnSc4
k	k	k7c3
Brnu	Brno	k1gNnSc3
</s>
<s>
Území	území	k1gNnSc1
moderní	moderní	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-Černovice	Brno-Černovice	k1gFnSc2
bylo	být	k5eAaImAgNnS
připojeno	připojit	k5eAaPmNgNnS
k	k	k7c3
Brnu	Brno	k1gNnSc3
postupně	postupně	k6eAd1
v	v	k7c6
několika	několik	k4yIc6
fázích	fág	k1gInPc6
<g/>
:	:	kIx,
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1850	#num#	k4
se	s	k7c7
součástí	součást	k1gFnSc7
Brna	Brno	k1gNnSc2
stal	stát	k5eAaPmAgInS
blok	blok	k1gInSc1
domů	dům	k1gInPc2
mezi	mezi	k7c7
ulicemi	ulice	k1gFnPc7
Olomoucká	olomoucký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Tržní	tržní	k2eAgFnSc1d1
a	a	k8xC
Zvěřinova	Zvěřinův	k2eAgFnSc1d1
<g/>
,	,	kIx,
náležející	náležející	k2eAgFnSc1d1
tehdy	tehdy	k6eAd1
k	k	k7c3
Zábrdovicím	Zábrdovice	k1gFnPc3
</s>
<s>
roku	rok	k1gInSc2
1869	#num#	k4
byl	být	k5eAaImAgMnS
k	k	k7c3
Brnu	Brno	k1gNnSc3
připojen	připojen	k2eAgInSc4d1
areál	areál	k1gInSc4
psychiatrické	psychiatrický	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
v	v	k7c6
Černovicích	Černovice	k1gFnPc6
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
nově	nově	k6eAd1
vytvořené	vytvořený	k2eAgNnSc1d1
katastrální	katastrální	k2eAgNnSc1d1
území	území	k1gNnSc1
Zábrdovice	Zábrdovice	k1gFnPc1
II	II	kA
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
oddělené	oddělený	k2eAgInPc4d1
od	od	k7c2
zbytku	zbytek	k1gInSc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přejmenování	přejmenování	k1gNnSc3
stávajícího	stávající	k2eAgNnSc2d1
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Zábrdovice	Zábrdovice	k1gFnPc1
na	na	k7c4
Zábrdovice	Zábrdovice	k1gFnPc4
I	i	k8xC
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1884	#num#	k4
(	(	kIx(
<g/>
podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
katastrální	katastrální	k2eAgFnSc2d1
mapy	mapa	k1gFnSc2
z	z	k7c2
povinných	povinný	k2eAgInPc2d1
císařských	císařský	k2eAgInPc2d1
výtisků	výtisk	k1gInPc2
stabilního	stabilní	k2eAgInSc2d1
katastru	katastr	k1gInSc2
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
k	k	k7c3
Brnu	Brno	k1gNnSc3
připojeny	připojit	k5eAaPmNgFnP
parcely	parcela	k1gFnPc1
po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
západní	západní	k2eAgFnSc2d1
části	část	k1gFnSc2
Olomoucké	olomoucký	k2eAgFnPc1d1
ulice	ulice	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
předtím	předtím	k6eAd1
tvořily	tvořit	k5eAaImAgFnP
součást	součást	k1gFnSc4
katastru	katastr	k1gInSc2
obce	obec	k1gFnSc2
Černovice	Černovice	k1gFnSc1
(	(	kIx(
<g/>
tyto	tento	k3xDgInPc1
pozemky	pozemek	k1gInPc1
byly	být	k5eAaImAgInP
současně	současně	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
Brna	Brno	k1gNnSc2
připojeny	připojit	k5eAaPmNgInP
ke	k	k7c3
katastrálnímu	katastrální	k2eAgNnSc3d1
území	území	k1gNnSc3
Zábrdovice	Zábrdovice	k1gFnPc4
I	i	k9
<g/>
)	)	kIx)
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1898	#num#	k4
(	(	kIx(
<g/>
podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
indikační	indikační	k2eAgFnSc2d1
skici	skica	k1gFnSc2
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Černovice	Černovice	k1gFnSc2
datované	datovaný	k2eAgFnSc2d1
rokem	rok	k1gInSc7
1873	#num#	k4
-	-	kIx~
signatura	signatura	k1gFnSc1
této	tento	k3xDgFnSc2
indikační	indikační	k2eAgFnSc2d1
skici	skica	k1gFnSc2
uložené	uložený	k2eAgFnSc2d1
v	v	k7c6
Moravském	moravský	k2eAgInSc6d1
zemském	zemský	k2eAgInSc6d1
archivu	archiv	k1gInSc6
je	být	k5eAaImIp3nS
MOR	mor	k1gInSc1
<g/>
0	#num#	k4
<g/>
38118730	#num#	k4
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
na	na	k7c6
základě	základ	k1gInSc6
výnosu	výnos	k1gInSc2
zemského	zemský	k2eAgNnSc2d1
finančního	finanční	k2eAgNnSc2d1
ředitelství	ředitelství	k1gNnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
připojen	připojit	k5eAaPmNgInS
k	k	k7c3
Brnu	Brno	k1gNnSc3
území	území	k1gNnSc2
na	na	k7c6
severozápadě	severozápad	k1gInSc6
tehdejšího	tehdejší	k2eAgInSc2d1
katastru	katastr	k1gInSc2
Černovic	Černovice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
zahrnovalo	zahrnovat	k5eAaImAgNnS
část	část	k1gFnSc4
Masné	masný	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
<g/>
,	,	kIx,
krátký	krátký	k2eAgInSc4d1
úsek	úsek	k1gInSc4
ulice	ulice	k1gFnSc2
Mlýnské	mlýnský	k2eAgFnSc2d1
<g/>
,	,	kIx,
ulici	ulice	k1gFnSc6
Porážka	porážka	k1gFnSc1
a	a	k8xC
blok	blok	k1gInSc1
domů	dům	k1gInPc2
mezi	mezi	k7c7
ulicemi	ulice	k1gFnPc7
Stinná	stinný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Elišky	Eliška	k1gFnPc1
Krásnohorské	krásnohorský	k2eAgFnPc1d1
a	a	k8xC
Spojka	spojka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc4
byla	být	k5eAaImAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
Brna	Brno	k1gNnSc2
připojena	připojen	k2eAgFnSc1d1
ke	k	k7c3
katastrálnímu	katastrální	k2eAgNnSc3d1
území	území	k1gNnSc3
Trnitá	trnitý	k2eAgFnSc1d1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1919	#num#	k4
pak	pak	k9
byla	být	k5eAaImAgFnS
připojena	připojit	k5eAaPmNgFnS
k	k	k7c3
Brnu	Brno	k1gNnSc3
i	i	k8xC
samotná	samotný	k2eAgFnSc1d1
obec	obec	k1gFnSc1
Černovice	Černovice	k1gFnSc1
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
obce	obec	k1gFnPc1
Ivanovice	Ivanovice	k1gFnPc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
zvané	zvaný	k2eAgFnPc4d1
Brněnské	brněnský	k2eAgFnPc4d1
Ivanovice	Ivanovice	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
Komárov	Komárov	k1gInSc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
katastry	katastr	k1gInPc1
na	na	k7c6
území	území	k1gNnSc6
moderní	moderní	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-Černovice	Brno-Černovice	k1gFnSc2
také	také	k9
zasahovaly	zasahovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s>
Změny	změna	k1gFnPc1
katastrálních	katastrální	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
Černovic	Černovice	k1gFnPc2
</s>
<s>
Vedle	vedle	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
zmíněných	zmíněný	k2eAgFnPc2d1
změn	změna	k1gFnPc2
katastrální	katastrální	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
Černovic	Černovice	k1gFnPc2
<g/>
,	,	kIx,
souvisejících	související	k2eAgInPc2d1
s	s	k7c7
připojováním	připojování	k1gNnSc7
k	k	k7c3
Brnu	Brno	k1gNnSc3
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dalším	další	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
též	též	k9
při	při	k7c6
první	první	k4xOgFnSc6
katastrální	katastrální	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
Brna	Brno	k1gNnSc2
roku	rok	k1gInSc2
1941	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
druhé	druhý	k4xOgFnSc3
katastrální	katastrální	k2eAgFnSc3d1
reformě	reforma	k1gFnSc3
Brna	Brno	k1gNnSc2
</s>
<s>
z	z	k7c2
let	léto	k1gNnPc2
1966	#num#	k4
<g/>
-	-	kIx~
<g/>
1969	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Černovice	Černovice	k1gFnPc1
získaly	získat	k5eAaPmAgFnP
svoje	svůj	k3xOyFgFnPc4
současné	současný	k2eAgFnPc4d1
katastrální	katastrální	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1941	#num#	k4
získaly	získat	k5eAaPmAgFnP
Černovice	Černovice	k1gFnPc1
zpět	zpět	k6eAd1
malé	malý	k2eAgNnSc4d1
území	území	k1gNnSc4
s	s	k7c7
výše	vysoce	k6eAd2
uvedeným	uvedený	k2eAgInSc7d1
blokem	blok	k1gInSc7
při	při	k7c6
ulici	ulice	k1gFnSc6
Elišky	Eliška	k1gFnSc2
Krásnohorské	krásnohorský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
zcela	zcela	k6eAd1
zásadní	zásadní	k2eAgFnSc3d1
změně	změna	k1gFnSc3
katastrálních	katastrální	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
Černovic	Černovice	k1gFnPc2
však	však	k9
došlo	dojít	k5eAaPmAgNnS
až	až	k9
při	při	k7c6
druhé	druhý	k4xOgFnSc6
katastrální	katastrální	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
Brna	Brno	k1gNnSc2
z	z	k7c2
let	léto	k1gNnPc2
1966	#num#	k4
<g/>
-	-	kIx~
<g/>
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byly	být	k5eAaImAgFnP
součástí	součást	k1gFnSc7
jejich	jejich	k3xOp3gInSc3
katastru	katastr	k1gInSc3
i	i	k9
některé	některý	k3yIgInPc4
nezastavěné	zastavěný	k2eNgInPc4d1
pozemky	pozemek	k1gInPc4
ležící	ležící	k2eAgInSc4d1
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Svitavy	Svitava	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
dnes	dnes	k6eAd1
součástí	součást	k1gFnSc7
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Komárova	komárův	k2eAgNnSc2d1
<g/>
,	,	kIx,
několik	několik	k4yIc4
bloků	blok	k1gInPc2
na	na	k7c6
jihu	jih	k1gInSc6
dnešního	dnešní	k2eAgInSc2d1
katastru	katastr	k1gInSc2
Židenic	Židenice	k1gFnPc2
<g/>
,	,	kIx,
Černovický	Černovický	k2eAgInSc1d1
hájek	hájek	k1gInSc1
v	v	k7c6
současném	současný	k2eAgInSc6d1
katastru	katastr	k1gInSc6
Brněnských	brněnský	k2eAgFnPc2d1
Ivanovic	Ivanovice	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
území	území	k1gNnSc4
dnes	dnes	k6eAd1
tvořící	tvořící	k2eAgFnSc7d1
západní	západní	k2eAgFnSc7d1
<g />
.	.	kIx.
</s>
<s hack="1">
část	část	k1gFnSc1
katastru	katastr	k1gInSc2
Slatiny	slatina	k1gFnSc2
(	(	kIx(
<g/>
Černovičky	Černovička	k1gFnPc1
<g/>
,	,	kIx,
kasárny	kasárny	k1gFnPc1
<g/>
,	,	kIx,
areál	areál	k1gInSc1
Lidlu	Lidl	k1gInSc2
<g/>
,	,	kIx,
vozovna	vozovna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Černovice	Černovice	k1gFnPc4
tehdy	tehdy	k6eAd1
naopak	naopak	k6eAd1
získaly	získat	k5eAaPmAgFnP
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zábrdovické	zábrdovický	k2eAgInPc1d1
domy	dům	k1gInPc1
jižně	jižně	k6eAd1
od	od	k7c2
Olomoucké	olomoucký	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
(	(	kIx(
<g/>
část	část	k1gFnSc1
zdejších	zdejší	k2eAgInPc2d1
pozemků	pozemek	k1gInPc2
však	však	k9
náležela	náležet	k5eAaImAgFnS
k	k	k7c3
Černovicím	Černovice	k1gFnPc3
do	do	k7c2
roku	rok	k1gInSc2
1884	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
pozemky	pozemek	k1gInPc1
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
náležející	náležející	k2eAgFnPc1d1
původně	původně	k6eAd1
k	k	k7c3
Brněnským	brněnský	k2eAgFnPc3d1
Ivanovicím	Ivanovice	k1gFnPc3
a	a	k8xC
Komárovu	komárův	k2eAgInSc3d1
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
administrativní	administrativní	k2eAgFnSc2d1
příslušnosti	příslušnost	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1945-1946	1945-1946	k4
tvořil	tvořit	k5eAaImAgInS
celý	celý	k2eAgInSc1d1
tehdejší	tehdejší	k2eAgInSc1d1
černovický	černovický	k2eAgInSc1d1
katastr	katastr	k1gInSc1
samosprávnou	samosprávný	k2eAgFnSc4d1
městskou	městský	k2eAgFnSc4d1
část	část	k1gFnSc4
Brno-Černovice	Brno-Černovice	k1gFnSc2
s	s	k7c7
vlastním	vlastní	k2eAgNnSc7d1
MNV	MNV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
části	část	k1gFnPc4
moderní	moderní	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
působnosti	působnost	k1gFnSc6
MNV	MNV	kA
Komárov	Komárov	k1gInSc1
a	a	k8xC
MNV	MNV	kA
Brněnské	brněnský	k2eAgFnPc4d1
Ivanovice	Ivanovice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
účely	účel	k1gInPc4
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
se	se	k3xPyFc4
bohužel	bohužel	k9
nepodařilo	podařit	k5eNaPmAgNnS
zjistit	zjistit	k5eAaPmF
tehdejší	tehdejší	k2eAgFnSc4d1
administrativní	administrativní	k2eAgFnSc4d1
příslušnost	příslušnost	k1gFnSc4
pozemků	pozemek	k1gInPc2
náležejících	náležející	k2eAgInPc2d1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ke	k	k7c3
katastru	katastr	k1gInSc3
Zábrdovic	Zábrdovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1947-1949	1947-1949	k4
byl	být	k5eAaImAgInS
celý	celý	k2eAgInSc1d1
tehdejší	tehdejší	k2eAgInSc4d1
černovický	černovický	k2eAgInSc4d1
katastr	katastr	k1gInSc4
součástí	součást	k1gFnSc7
městského	městský	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Brno	Brno	k1gNnSc4
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
zábrdovická	zábrdovický	k2eAgFnSc1d1
část	část	k1gFnSc1
moderní	moderní	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
náležela	náležet	k5eAaImAgFnS
k	k	k7c3
městskému	městský	k2eAgInSc3d1
obvodu	obvod	k1gInSc3
Brno	Brno	k1gNnSc4
I.	I.	kA
</s>
<s>
1949-1954	1949-1954	k4
náležela	náležet	k5eAaImAgFnS
většina	většina	k1gFnSc1
území	území	k1gNnSc2
tehdejšího	tehdejší	k2eAgInSc2d1
černovického	černovický	k2eAgInSc2d1
katastru	katastr	k1gInSc2
k	k	k7c3
městskému	městský	k2eAgInSc3d1
obvodu	obvod	k1gInSc3
Brno	Brno	k1gNnSc4
X	X	kA
(	(	kIx(
<g/>
území	území	k1gNnPc1
ležící	ležící	k2eAgNnPc1d1
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Svitavy	Svitava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
západní	západní	k2eAgFnSc1d1
část	část	k1gFnSc1
katastru	katastr	k1gInSc2
Černovic	Černovice	k1gFnPc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
však	však	k9
již	již	k6eAd1
k	k	k7c3
Černovicím	Černovice	k1gFnPc3
nenáležející	náležející	k2eNgFnSc2d1
<g/>
)	)	kIx)
ležící	ležící	k2eAgFnSc2d1
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Svitavy	Svitava	k1gFnSc2
byla	být	k5eAaImAgFnS
rozdělena	rozdělit	k5eAaPmNgFnS
mezi	mezi	k7c4
městské	městský	k2eAgInPc4d1
obvody	obvod	k1gInPc4
Brno	Brno	k1gNnSc1
XI	XI	kA
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Brno	Brno	k1gNnSc1
IV	Iva	k1gFnPc2
<g/>
;	;	kIx,
součástí	součást	k1gFnPc2
Brna	Brno	k1gNnSc2
X	X	kA
byla	být	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
i	i	k9
část	část	k1gFnSc4
moderního	moderní	k2eAgInSc2d1
černovického	černovický	k2eAgInSc2d1
katastru	katastr	k1gInSc2
<g/>
,	,	kIx,
náležející	náležející	k2eAgFnSc1d1
tehdy	tehdy	k6eAd1
k	k	k7c3
Zábrdovicím	Zábrdovice	k1gFnPc3
<g/>
;	;	kIx,
parcely	parcela	k1gFnPc1
na	na	k7c6
jihu	jih	k1gInSc6
moderního	moderní	k2eAgNnSc2d1
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-Černovice	Brno-Černovice	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
v	v	k7c6
té	ten	k3xDgFnSc6
doby	doba	k1gFnPc1
tvořily	tvořit	k5eAaImAgFnP
součást	součást	k1gFnSc4
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
Komárov	Komárov	k1gInSc1
a	a	k8xC
Brněnské	brněnský	k2eAgFnPc1d1
Ivanovice	Ivanovice	k1gFnPc1
<g/>
,	,	kIx,
patřily	patřit	k5eAaImAgFnP
k	k	k7c3
městskému	městský	k2eAgInSc3d1
obvodu	obvod	k1gInSc3
Brno	Brno	k1gNnSc4
XIII	XIII	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1954-1964	1954-1964	k4
tvořila	tvořit	k5eAaImAgFnS
východní	východní	k2eAgFnPc4d1
části	část	k1gFnPc4
tehdejšího	tehdejší	k2eAgInSc2d1
černovického	černovický	k2eAgInSc2d1
katastru	katastr	k1gInSc2
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Svitavy	Svitava	k1gFnSc2
součást	součást	k1gFnSc4
městského	městský	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Brno	Brno	k1gNnSc4
VI	VI	kA
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
západní	západní	k2eAgFnSc1d1
část	část	k1gFnSc1
katasru	katasr	k1gInSc2
ležící	ležící	k2eAgFnSc1d1
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Svitavy	Svitava	k1gFnSc2
náležela	náležet	k5eAaImAgFnS
k	k	k7c3
městskému	městský	k2eAgInSc3d1
obvodu	obvod	k1gInSc3
Brno	Brno	k1gNnSc4
IV	IV	kA
<g/>
;	;	kIx,
součástí	součást	k1gFnPc2
Brna	Brno	k1gNnSc2
VI	VI	kA
byla	být	k5eAaImAgFnS
i	i	k9
tehdy	tehdy	k6eAd1
zábrdovická	zábrdovický	k2eAgFnSc1d1
část	část	k1gFnSc1
moderních	moderní	k2eAgFnPc2d1
Černovic	Černovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
moderního	moderní	k2eAgInSc2d1
černovického	černovický	k2eAgInSc2d1
katastru	katastr	k1gInSc2
<g/>
,	,	kIx,
patřící	patřící	k2eAgFnSc1d1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ke	k	k7c3
Komárovu	komárův	k2eAgInSc3d1
a	a	k8xC
Brněnským	brněnský	k2eAgFnPc3d1
Ivanovicím	Ivanovice	k1gFnPc3
<g/>
,	,	kIx,
náležely	náležet	k5eAaImAgFnP
v	v	k7c6
letech	léto	k1gNnPc6
1954-1960	1954-1960	k4
k	k	k7c3
městskému	městský	k2eAgInSc3d1
obvodu	obvod	k1gInSc3
Brno	Brno	k1gNnSc4
X-Tuřany	X-Tuřan	k1gMnPc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1971	#num#	k4
k	k	k7c3
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
Tuřany	Tuřana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
od	od	k7c2
roku	rok	k1gInSc2
1964	#num#	k4
náležel	náležet	k5eAaImAgInS
celý	celý	k2eAgInSc1d1
černovický	černovický	k2eAgInSc1d1
katastr	katastr	k1gInSc1
k	k	k7c3
městskému	městský	k2eAgInSc3d1
obvodu	obvod	k1gInSc3
Brno	Brno	k1gNnSc4
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
katastrální	katastrální	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
Brna	Brno	k1gNnSc2
pak	pak	k6eAd1
došlo	dojít	k5eAaPmAgNnS
roku	rok	k1gInSc2
1971	#num#	k4
k	k	k7c3
další	další	k2eAgFnSc3d1
správní	správní	k2eAgFnSc3d1
reformě	reforma	k1gFnSc3
a	a	k8xC
pak	pak	k6eAd1
až	až	k9
do	do	k7c2
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
tvořil	tvořit	k5eAaImAgInS
celý	celý	k2eAgInSc1d1
černovický	černovický	k2eAgInSc1d1
katastr	katastr	k1gInSc1
(	(	kIx(
<g/>
již	již	k9
v	v	k7c6
současných	současný	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
<g/>
)	)	kIx)
součást	součást	k1gFnSc1
městského	městský	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Brno	Brno	k1gNnSc4
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
od	od	k7c2
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
jsou	být	k5eAaImIp3nP
Černovice	Černovice	k1gFnPc4
pod	pod	k7c7
názvem	název	k1gInSc7
Brno-Černovice	Brno-Černovice	k1gFnSc2
jednou	jednou	k6eAd1
ze	z	k7c2
samosprávných	samosprávný	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
Brně-Černovicích	Brně-Černovice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Floriána	Florián	k1gMnSc2
–	–	k?
novogotický	novogotický	k2eAgInSc4d1
kostel	kostel	k1gInSc4
postavený	postavený	k2eAgInSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1898	#num#	k4
na	na	k7c6
návsi	náves	k1gFnSc6
(	(	kIx(
<g/>
Faměrovo	Faměrův	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zbořen	zbořen	k2eAgMnSc1d1
1960	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
gisbrno	gisbrno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
tmapserver	tmapserver	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
například	například	k6eAd1
z	z	k7c2
této	tento	k3xDgFnSc2
mapy	mapa	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1943	#num#	k4
na	na	k7c6
tomto	tento	k3xDgInSc6
webu	web	k1gInSc6
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FLODROVÁ	FLODROVÁ	kA
<g/>
,	,	kIx,
Milena	Milena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
v	v	k7c6
proměnách	proměna	k1gFnPc6
času	čas	k1gInSc2
(	(	kIx(
<g/>
Malá	malý	k2eAgNnPc1d1
zamyšlení	zamyšlení	k1gNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Šimon	Šimon	k1gMnSc1
Ryšavý	Ryšavý	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
179	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86137	#num#	k4
<g/>
-	-	kIx~
<g/>
79	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Dvojí	dvojit	k5eAaImIp3nS
Černovice	Černovice	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
104	#num#	k4
<g/>
-	-	kIx~
<g/>
106	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Černovice	Černovice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normat	k5eAaImAgInS,k5eAaPmAgInS,k5eAaBmAgInS
<g/>
}	}	kIx)
Statutární	statutární	k2eAgNnSc4d1
město	město	k1gNnSc4
Brno	Brno	k1gNnSc4
Městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
</s>
<s>
Brno-Bohunice	Brno-Bohunice	k1gFnSc1
</s>
<s>
Brno-Bosonohy	Brno-Bosonoh	k1gInPc1
</s>
<s>
Brno-Bystrc	Brno-Bystrc	k6eAd1
</s>
<s>
Brno-Černovice	Brno-Černovice	k1gFnSc1
</s>
<s>
Brno-Chrlice	Brno-Chrlice	k1gFnSc1
</s>
<s>
Brno-Ivanovice	Brno-Ivanovice	k1gFnSc1
</s>
<s>
Brno-Jehnice	Brno-Jehnice	k1gFnSc1
</s>
<s>
Brno-jih	Brno-jih	k1gMnSc1
</s>
<s>
Brno-Jundrov	Brno-Jundrov	k1gInSc1
</s>
<s>
Brno-Kníničky	Brno-Knínička	k1gFnPc1
</s>
<s>
Brno-Kohoutovice	Brno-Kohoutovice	k1gFnSc1
</s>
<s>
Brno-Komín	Brno-Komín	k1gMnSc1
</s>
<s>
Brno-Královo	Brno-Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
</s>
<s>
Brno-Líšeň	Brno-Líšeň	k1gFnSc1
</s>
<s>
Brno-Maloměřice	Brno-Maloměřice	k1gFnPc1
a	a	k8xC
Obřany	Obřana	k1gFnPc1
</s>
<s>
Brno-Medlánky	Brno-Medlánka	k1gFnPc1
</s>
<s>
Brno-Nový	Brno-Nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Brno-Ořešín	Brno-Ořešín	k1gMnSc1
</s>
<s>
Brno-Řečkovice	Brno-Řečkovice	k1gFnSc1
a	a	k8xC
Mokrá	mokrý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Brno-sever	Brno-sever	k1gMnSc1
</s>
<s>
Brno-Slatina	Brno-Slatina	k1gFnSc1
</s>
<s>
Brno-Starý	Brno-Starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Brno-střed	Brno-střed	k1gMnSc1
</s>
<s>
Brno-Tuřany	Brno-Tuřan	k1gMnPc4
</s>
<s>
Brno-Útěchov	Brno-Útěchov	k1gInSc1
</s>
<s>
Brno-Vinohrady	Brno-Vinohrada	k1gFnPc1
</s>
<s>
Brno-Žabovřesky	Brno-Žabovřesky	k6eAd1
</s>
<s>
Brno-Žebětín	Brno-Žebětín	k1gMnSc1
</s>
<s>
Brno-Židenice	Brno-Židenice	k1gFnSc1
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
akatastrální	akatastrální	k2eAgFnSc4d1
území	území	k1gNnPc2
</s>
<s>
Bohunice	Bohunice	k1gFnPc1
</s>
<s>
Bosonohy	Bosonohy	k?
</s>
<s>
Brněnské	brněnský	k2eAgFnPc4d1
Ivanovice	Ivanovice	k1gFnPc4
</s>
<s>
Brno-město	Brno-město	k6eAd1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Město	město	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Bystrc	Bystrc	k1gFnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Pole	pole	k1gFnSc1
</s>
<s>
Černovice	Černovice	k1gFnSc1
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
Heršpice	Heršpice	k1gFnPc1
</s>
<s>
Dvorska	Dvorska	k1gFnSc1
</s>
<s>
Holásky	Holásek	k1gMnPc4
</s>
<s>
Horní	horní	k2eAgFnPc1d1
Heršpice	Heršpice	k1gFnPc1
</s>
<s>
Husovice	Husovice	k1gFnPc1
</s>
<s>
Chrlice	Chrlice	k1gFnPc1
</s>
<s>
Ivanovice	Ivanovice	k1gFnPc1
</s>
<s>
Jehnice	jehnice	k1gFnSc1
</s>
<s>
Jundrov	Jundrov	k1gInSc1
</s>
<s>
Kníničky	Knínička	k1gFnPc1
</s>
<s>
Kohoutovice	Kohoutovice	k1gFnPc1
</s>
<s>
Komárov	Komárov	k1gInSc1
</s>
<s>
Komín	Komín	k1gInSc1
</s>
<s>
Královo	Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
</s>
<s>
Lesná	lesný	k2eAgFnSc1d1
</s>
<s>
Líšeň	Líšeň	k1gFnSc1
</s>
<s>
Maloměřice	Maloměřice	k1gFnPc1
</s>
<s>
Medlánky	Medlánka	k1gFnPc1
</s>
<s>
Mokrá	mokrý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Obřany	Obřana	k1gFnPc1
</s>
<s>
Ořešín	Ořešín	k1gMnSc1
</s>
<s>
Pisárky	Pisárka	k1gFnPc1
</s>
<s>
Ponava	Ponava	k1gFnSc1
</s>
<s>
Přízřenice	Přízřenice	k1gFnSc1
</s>
<s>
Řečkovice	Řečkovice	k1gFnSc1
</s>
<s>
Sadová	sadový	k2eAgFnSc1d1
</s>
<s>
Slatina	slatina	k1gFnSc1
</s>
<s>
Soběšice	Soběšice	k1gFnSc1
</s>
<s>
Staré	Staré	k2eAgNnSc1d1
Brno	Brno	k1gNnSc1
</s>
<s>
Starý	starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Stránice	Stránice	k1gFnSc1
</s>
<s>
Štýřice	Štýřice	k1gFnSc1
</s>
<s>
Trnitá	trnitý	k2eAgFnSc1d1
</s>
<s>
Tuřany	Tuřana	k1gFnPc1
</s>
<s>
Útěchov	Útěchov	k1gInSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Útěchov	Útěchov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Veveří	veveří	k2eAgNnSc4d1
</s>
<s>
Zábrdovice	Zábrdovice	k1gFnPc1
</s>
<s>
Žabovřesky	Žabovřesky	k1gFnPc1
</s>
<s>
Žebětín	Žebětín	k1gMnSc1
</s>
<s>
Židenice	Židenice	k1gFnPc1
Další	další	k2eAgFnPc1d1
čtvrtě	čtvrt	k1gFnPc1
</s>
<s>
Cacovice	Cacovice	k1gFnSc1
</s>
<s>
Černovičky	Černovička	k1gFnPc1
</s>
<s>
Divišova	Divišův	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
</s>
<s>
Jabloňová	jabloňový	k2eAgFnSc1d1
</s>
<s>
Juliánov	Juliánov	k1gInSc1
</s>
<s>
Kamechy	Kamech	k1gInPc1
</s>
<s>
Kamenná	kamenný	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1
Vrch	vrch	k1gInSc1
</s>
<s>
Kandie	Kandie	k1gFnSc1
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
Černovice	Černovice	k1gFnPc1
</s>
<s>
Nové	Nové	k2eAgMnPc4d1
Moravany	Moravan	k1gMnPc4
</s>
<s>
Nový	nový	k2eAgInSc1d1
dům	dům	k1gInSc1
</s>
<s>
Písečník	písečník	k1gInSc1
</s>
<s>
Pod	pod	k7c7
vodojemem	vodojem	k1gInSc7
</s>
<s>
Slatinka	Slatinka	k1gFnSc1
</s>
<s>
Štefánikova	Štefánikův	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
Historická	historický	k2eAgFnSc1d1
předměstí	předměstí	k1gNnSc4
</s>
<s>
Augustiniánská	augustiniánský	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Červená	červený	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
a	a	k8xC
Horní	horní	k2eAgInSc1d1
Cejl	Cejl	k1gInSc1
</s>
<s>
Josefov	Josefov	k1gInSc1
</s>
<s>
Kožená	kožený	k2eAgFnSc1d1
</s>
<s>
Křenová	křenový	k2eAgFnSc1d1
</s>
<s>
Křížová	Křížová	k1gFnSc1
</s>
<s>
Lužánky	Lužánka	k1gFnPc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Mariacela	Mariacela	k1gFnSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Na	na	k7c4
Hrázi	hráze	k1gFnSc4
a	a	k8xC
Příkop	příkop	k1gInSc4
</s>
<s>
Náhon	náhon	k1gInSc1
</s>
<s>
Nové	Nové	k2eAgInPc1d1
Sady	sad	k1gInPc1
</s>
<s>
Pekařská	pekařský	k2eAgFnSc1d1
</s>
<s>
Růžový	růžový	k2eAgInSc1d1
</s>
<s>
Silniční	silniční	k2eAgInSc1d1
</s>
<s>
Špilberk	Špilberk	k1gInSc1
</s>
<s>
Švábka	švábka	k1gFnSc1
</s>
<s>
U	u	k7c2
Svaté	svatý	k2eAgFnSc2d1
Anny	Anna	k1gFnSc2
</s>
<s>
Ugartov	Ugartov	k1gInSc1
</s>
<s>
V	v	k7c6
Jirchářích	jirchář	k1gMnPc6
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Vinohrádky	vinohrádek	k1gInPc1
Zaniklé	zaniklý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
</s>
<s>
Kníničky	Knínička	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
</s>
