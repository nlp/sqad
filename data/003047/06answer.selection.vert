<s>
Mrchožrout	mrchožrout	k1gMnSc1	mrchožrout
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
živočišný	živočišný	k2eAgInSc4d1	živočišný
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
mrtvými	mrtvý	k2eAgMnPc7d1	mrtvý
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sami	sám	k3xTgMnPc1	sám
uhynuli	uhynout	k5eAaPmAgMnP	uhynout
(	(	kIx(	(
<g/>
neloví	lovit	k5eNaImIp3nS	lovit
je	on	k3xPp3gInPc4	on
nebo	nebo	k8xC	nebo
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
je	on	k3xPp3gInPc4	on
ulovit	ulovit	k5eAaPmF	ulovit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
