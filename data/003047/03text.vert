<s>
Mrchožrout	mrchožrout	k1gMnSc1	mrchožrout
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
živočišný	živočišný	k2eAgInSc4d1	živočišný
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
mrtvými	mrtvý	k2eAgMnPc7d1	mrtvý
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sami	sám	k3xTgMnPc1	sám
uhynuli	uhynout	k5eAaPmAgMnP	uhynout
(	(	kIx(	(
<g/>
neloví	lovit	k5eNaImIp3nS	lovit
je	on	k3xPp3gInPc4	on
nebo	nebo	k8xC	nebo
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
je	on	k3xPp3gInPc4	on
ulovit	ulovit	k5eAaPmF	ulovit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mrchožrouti	mrchožrout	k1gMnPc1	mrchožrout
se	se	k3xPyFc4	se
také	také	k9	také
živí	živit	k5eAaImIp3nP	živit
na	na	k7c6	na
zbytcích	zbytek	k1gInPc6	zbytek
potravy	potrava	k1gFnSc2	potrava
po	po	k7c6	po
predátorech	predátor	k1gMnPc6	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Mrchožrouti	mrchožrout	k1gMnPc1	mrchožrout
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
saprofágy	saprofág	k1gMnPc7	saprofág
a	a	k8xC	a
jim	on	k3xPp3gMnPc3	on
je	být	k5eAaImIp3nS	být
nadřazený	nadřazený	k2eAgInSc4d1	nadřazený
termín	termín	k1gInSc4	termín
dekompozitor	dekompozitor	k1gMnSc1	dekompozitor
<g/>
.	.	kIx.	.
sup	sup	k1gMnSc1	sup
bělohlavý	bělohlavý	k2eAgMnSc1d1	bělohlavý
(	(	kIx(	(
<g/>
Gyps	gyps	k1gInSc1	gyps
fulvus	fulvus	k1gMnSc1	fulvus
<g/>
)	)	kIx)	)
šakal	šakal	k1gMnSc1	šakal
čabrakový	čabrakový	k2eAgMnSc1d1	čabrakový
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnSc1	Canis
mesomelas	mesomelas	k1gMnSc1	mesomelas
<g/>
)	)	kIx)	)
chrobák	chrobák	k1gMnSc1	chrobák
(	(	kIx(	(
<g/>
Geotrupidae	Geotrupidae	k1gFnSc1	Geotrupidae
<g/>
)	)	kIx)	)
mrchožrout	mrchožrout	k1gMnSc1	mrchožrout
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Phosphuga	Phosphuga	k1gFnSc1	Phosphuga
atrata	atrata	k1gFnSc1	atrata
<g/>
)	)	kIx)	)
S	s	k7c7	s
mrchožrouty	mrchožrout	k1gMnPc7	mrchožrout
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
asociována	asociovat	k5eAaBmNgFnS	asociovat
také	také	k9	také
hyena	hyena	k1gFnSc1	hyena
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
(	(	kIx(	(
<g/>
Crocuta	Crocut	k2eAgFnSc1d1	Crocuta
crocuta	crocut	k2eAgFnSc1d1	crocut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
ale	ale	k9	ale
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
loví	lovit	k5eAaImIp3nP	lovit
živou	živý	k2eAgFnSc4d1	živá
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
mršiny	mršina	k1gFnPc4	mršina
tvoří	tvořit	k5eAaImIp3nP	tvořit
jen	jen	k9	jen
poměrně	poměrně	k6eAd1	poměrně
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
jejího	její	k3xOp3gInSc2	její
jídelníčku	jídelníček	k1gInSc2	jídelníček
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
mrchožroutem	mrchožrout	k1gMnSc7	mrchožrout
je	být	k5eAaImIp3nS	být
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
lev	lev	k1gMnSc1	lev
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Afriky	Afrika	k1gFnSc2	Afrika
tvoří	tvořit	k5eAaImIp3nS	tvořit
mršiny	mršina	k1gFnPc4	mršina
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
<g/>
%	%	kIx~	%
jeho	jeho	k3xOp3gFnSc2	jeho
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
mrchožroutem	mrchožrout	k1gMnSc7	mrchožrout
mezi	mezi	k7c7	mezi
obratlovci	obratlovec	k1gMnPc1	obratlovec
krkavec	krkavec	k1gMnSc1	krkavec
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Corvus	Corvus	k1gInSc1	Corvus
corax	corax	k1gInSc1	corax
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mrchožroutovití	Mrchožroutovitý	k2eAgMnPc1d1	Mrchožroutovitý
(	(	kIx(	(
<g/>
Silphidae	Silphidae	k1gNnSc7	Silphidae
<g/>
)	)	kIx)	)
Mrchožrouti	mrchožrout	k1gMnPc1	mrchožrout
(	(	kIx(	(
<g/>
Silphini	Silphin	k1gMnPc1	Silphin
<g/>
)	)	kIx)	)
</s>
