<s>
Howard	Howard	k1gMnSc1	Howard
Phillips	Phillipsa	k1gFnPc2	Phillipsa
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1890	[number]	k4	1890
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
žurnalista	žurnalista	k1gMnSc1	žurnalista
věnující	věnující	k2eAgFnSc3d1	věnující
se	se	k3xPyFc4	se
literární	literární	k2eAgFnSc3d1	literární
tvorbě	tvorba	k1gFnSc3	tvorba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fantasy	fantas	k1gInPc4	fantas
<g/>
,	,	kIx,	,
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc1	fiction
a	a	k8xC	a
především	především	k9	především
hororu	horor	k1gInSc2	horor
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
kombinoval	kombinovat	k5eAaImAgMnS	kombinovat
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
oblíbeném	oblíbený	k2eAgInSc6d1	oblíbený
žánru	žánr	k1gInSc6	žánr
"	"	kIx"	"
<g/>
weird	weird	k1gInSc1	weird
fiction	fiction	k1gInSc1	fiction
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
