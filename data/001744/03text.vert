<s>
Howard	Howard	k1gMnSc1	Howard
Phillips	Phillipsa	k1gFnPc2	Phillipsa
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1890	[number]	k4	1890
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
žurnalista	žurnalista	k1gMnSc1	žurnalista
věnující	věnující	k2eAgFnSc3d1	věnující
se	se	k3xPyFc4	se
literární	literární	k2eAgFnSc3d1	literární
tvorbě	tvorba	k1gFnSc3	tvorba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fantasy	fantas	k1gInPc4	fantas
<g/>
,	,	kIx,	,
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc1	fiction
a	a	k8xC	a
především	především	k9	především
hororu	horor	k1gInSc2	horor
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
kombinoval	kombinovat	k5eAaImAgMnS	kombinovat
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
oblíbeném	oblíbený	k2eAgInSc6d1	oblíbený
žánru	žánr	k1gInSc6	žánr
"	"	kIx"	"
<g/>
weird	weird	k1gInSc1	weird
fiction	fiction	k1gInSc1	fiction
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
potýkal	potýkat	k5eAaImAgMnS	potýkat
s	s	k7c7	s
finančními	finanční	k2eAgFnPc7d1	finanční
obtížemi	obtíž	k1gFnPc7	obtíž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
řešíval	řešívat	k5eAaImAgMnS	řešívat
ghostwritingem	ghostwriting	k1gInSc7	ghostwriting
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
pověst	pověst	k1gFnSc4	pověst
výstředního	výstřední	k2eAgMnSc2d1	výstřední
samotáře	samotář	k1gMnSc2	samotář
<g/>
,	,	kIx,	,
laskavého	laskavý	k2eAgMnSc2d1	laskavý
přítele	přítel	k1gMnSc2	přítel
i	i	k9	i
nesmírně	smírně	k6eNd1	smírně
sečtělého	sečtělý	k2eAgMnSc4d1	sečtělý
učence	učenec	k1gMnSc4	učenec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
amatérským	amatérský	k2eAgNnSc7d1	amatérské
studiem	studio	k1gNnSc7	studio
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
i	i	k8xC	i
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
především	především	k6eAd1	především
svými	svůj	k3xOyFgFnPc7	svůj
povídkami	povídka	k1gFnPc7	povídka
<g/>
,	,	kIx,	,
pojednávajícími	pojednávající	k2eAgInPc7d1	pojednávající
o	o	k7c6	o
mýtu	mýtus	k1gInSc6	mýtus
Cthulhu	Cthulh	k1gInSc2	Cthulh
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgFnSc3d1	popisující
podivné	podivný	k2eAgFnPc4d1	podivná
a	a	k8xC	a
temné	temný	k2eAgFnPc4d1	temná
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obývaly	obývat	k5eAaImAgFnP	obývat
Zemi	zem	k1gFnSc4	zem
milióny	milión	k4xCgInPc4	milión
let	léto	k1gNnPc2	léto
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
nesena	nést	k5eAaImNgFnS	nést
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
tísnivého	tísnivý	k2eAgInSc2d1	tísnivý
kosmického	kosmický	k2eAgInSc2d1	kosmický
indiferentismu	indiferentismus	k1gInSc2	indiferentismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
realita	realita	k1gFnSc1	realita
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
křehká	křehký	k2eAgFnSc1d1	křehká
<g/>
,	,	kIx,	,
iluzivní	iluzivní	k2eAgFnSc1d1	iluzivní
skořápka	skořápka	k1gFnSc1	skořápka
<g/>
,	,	kIx,	,
dělící	dělící	k2eAgNnSc1d1	dělící
lidské	lidský	k2eAgNnSc1d1	lidské
vnímání	vnímání	k1gNnSc1	vnímání
od	od	k7c2	od
nesmírně	smírně	k6eNd1	smírně
bezútěšné	bezútěšný	k2eAgFnSc2d1	bezútěšná
a	a	k8xC	a
děsivé	děsivý	k2eAgFnSc2d1	děsivá
existence	existence	k1gFnSc2	existence
v	v	k7c6	v
chladném	chladný	k2eAgMnSc6d1	chladný
<g/>
,	,	kIx,	,
lhostejném	lhostejný	k2eAgInSc6d1	lhostejný
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Odhalení	odhalení	k1gNnPc1	odhalení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
hrdinové	hrdina	k1gMnPc1	hrdina
děl	dělo	k1gNnPc2	dělo
učiní	učinit	k5eAaImIp3nP	učinit
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
často	často	k6eAd1	často
ke	k	k7c3	k
tragickému	tragický	k2eAgInSc3d1	tragický
sestupu	sestup	k1gInSc3	sestup
v	v	k7c6	v
šílenství	šílenství	k1gNnSc6	šílenství
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
i	i	k9	i
autorem	autor	k1gMnSc7	autor
snového	snový	k2eAgInSc2d1	snový
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velice	velice	k6eAd1	velice
květnatým	květnatý	k2eAgInSc7d1	květnatý
a	a	k8xC	a
básnivým	básnivý	k2eAgInSc7d1	básnivý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
krás	krása	k1gFnPc2	krása
lidských	lidský	k2eAgInPc2d1	lidský
snů	sen	k1gInPc2	sen
a	a	k8xC	a
mýtů	mýtus	k1gInPc2	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
motivů	motiv	k1gInPc2	motiv
je	být	k5eAaImIp3nS	být
však	však	k9	však
pradávná	pradávný	k2eAgFnSc1d1	pradávná
kniha	kniha	k1gFnSc1	kniha
Necronomicon	Necronomicona	k1gFnPc2	Necronomicona
od	od	k7c2	od
fiktivního	fiktivní	k2eAgMnSc2d1	fiktivní
arabského	arabský	k2eAgMnSc2d1	arabský
učence	učenec	k1gMnSc2	učenec
Abdula	Abdul	k1gMnSc2	Abdul
Alhazreda	Alhazred	k1gMnSc2	Alhazred
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
promyšlená	promyšlený	k2eAgFnSc1d1	promyšlená
mystifikace	mystifikace	k1gFnSc1	mystifikace
vedla	vést	k5eAaImAgFnS	vést
často	často	k6eAd1	často
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
veřejnosti	veřejnost	k1gFnSc2	veřejnost
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
skutečné	skutečný	k2eAgFnSc6d1	skutečná
pravosti	pravost	k1gFnSc6	pravost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
čten	číst	k5eAaImNgInS	číst
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgInS	zemřít
v	v	k7c6	v
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
hrstky	hrstka	k1gFnSc2	hrstka
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
soustavně	soustavně	k6eAd1	soustavně
vydávat	vydávat	k5eAaPmF	vydávat
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
H.	H.	kA	H.
P.	P.	kA	P.
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
hororových	hororový	k2eAgMnPc2d1	hororový
autorů	autor	k1gMnPc2	autor
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
průkopníkem	průkopník	k1gMnSc7	průkopník
moderního	moderní	k2eAgInSc2d1	moderní
hororu	horor	k1gInSc2	horor
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
na	na	k7c4	na
194	[number]	k4	194
Angell	Angellum	k1gNnPc2	Angellum
Street	Streeta	k1gFnPc2	Streeta
v	v	k7c6	v
městě	město	k1gNnSc6	město
Providence	providence	k1gFnSc2	providence
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
mj.	mj.	kA	mj.
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
a	a	k8xC	a
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
svých	svůj	k3xOyFgFnPc6	svůj
básních	báseň	k1gFnPc6	báseň
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Winfielda	Winfield	k1gMnSc2	Winfield
Scotta	Scott	k1gMnSc2	Scott
Lovecrafta	Lovecraft	k1gMnSc2	Lovecraft
<g/>
,	,	kIx,	,
obchodního	obchodní	k2eAgMnSc2d1	obchodní
cestujícího	cestující	k1gMnSc2	cestující
s	s	k7c7	s
klenoty	klenot	k1gInPc7	klenot
a	a	k8xC	a
Sarah	Sarah	k1gFnSc7	Sarah
Susan	Susana	k1gFnPc2	Susana
Phillips	Phillipsa	k1gFnPc2	Phillipsa
Lovecraftové	Lovecraftová	k1gFnSc2	Lovecraftová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zajímala	zajímat	k5eAaImAgFnS	zajímat
o	o	k7c4	o
dějiny	dějiny	k1gFnPc4	dějiny
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
počínající	počínající	k2eAgFnSc2d1	počínající
Georgem	Georg	k1gMnSc7	Georg
Phillipsem	Phillips	k1gMnSc7	Phillips
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
již	již	k6eAd1	již
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1630	[number]	k4	1630
připlul	připlout	k5eAaPmAgInS	připlout
mezi	mezi	k7c7	mezi
prvními	první	k4xOgMnPc7	první
anglickými	anglický	k2eAgMnPc7d1	anglický
přistěhovalci	přistěhovalec	k1gMnPc7	přistěhovalec
na	na	k7c6	na
území	území	k1gNnSc6	území
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
byl	být	k5eAaImAgInS	být
Howard	Howard	k1gInSc1	Howard
velmi	velmi	k6eAd1	velmi
rozmazlován	rozmazlován	k2eAgInSc1d1	rozmazlován
a	a	k8xC	a
hýčkán	hýčkán	k2eAgInSc1d1	hýčkán
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vychováván	vychovávat	k5eAaImNgInS	vychovávat
v	v	k7c6	v
odloučení	odloučení	k1gNnSc6	odloučení
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
trávil	trávit	k5eAaImAgMnS	trávit
poklidnou	poklidný	k2eAgFnSc7d1	poklidná
četbou	četba	k1gFnSc7	četba
a	a	k8xC	a
denním	denní	k2eAgNnSc7d1	denní
sněním	snění	k1gNnSc7	snění
v	v	k7c6	v
prostorných	prostorný	k2eAgFnPc6d1	prostorná
<g/>
,	,	kIx,	,
tmavých	tmavý	k2eAgFnPc6d1	tmavá
chodbách	chodba	k1gFnPc6	chodba
rodného	rodný	k2eAgInSc2d1	rodný
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mnozí	mnohý	k2eAgMnPc1d1	mnohý
říkali	říkat	k5eAaImAgMnP	říkat
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
neudržovaný	udržovaný	k2eNgInSc1d1	neudržovaný
a	a	k8xC	a
zanedbaný	zanedbaný	k2eAgInSc1d1	zanedbaný
<g/>
,	,	kIx,	,
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
Howard	Howard	k1gInSc1	Howard
působil	působit	k5eAaImAgInS	působit
dojmem	dojem	k1gInSc7	dojem
chudého	chudý	k2eAgMnSc2d1	chudý
aristokrata	aristokrat	k1gMnSc2	aristokrat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
ovšem	ovšem	k9	ovšem
náhle	náhle	k6eAd1	náhle
přerušila	přerušit	k5eAaPmAgFnS	přerušit
tragédie	tragédie	k1gFnSc1	tragédie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
Lovecraftovi	Lovecraftův	k2eAgMnPc1d1	Lovecraftův
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
tehdy	tehdy	k6eAd1	tehdy
podstupoval	podstupovat	k5eAaImAgMnS	podstupovat
další	další	k2eAgFnSc4d1	další
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
obchodní	obchodní	k2eAgFnSc4d1	obchodní
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
hotelový	hotelový	k2eAgInSc4d1	hotelový
pokoj	pokoj	k1gInSc4	pokoj
však	však	k9	však
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
psychické	psychický	k2eAgNnSc4d1	psychické
zhroucení	zhroucení	k1gNnSc4	zhroucení
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgMnS	být
převezen	převézt	k5eAaPmNgMnS	převézt
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Providence	providence	k1gFnSc2	providence
<g/>
,	,	kIx,	,
do	do	k7c2	do
Butlerovy	Butlerův	k2eAgFnSc2d1	Butlerova
nemocnice	nemocnice	k1gFnSc2	nemocnice
pro	pro	k7c4	pro
duševně	duševně	k6eAd1	duševně
choré	chorý	k2eAgFnPc4d1	chorá
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraft	Lovecraft	k1gInSc1	Lovecraft
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Winfielda	Winfieldo	k1gNnSc2	Winfieldo
nicméně	nicméně	k8xC	nicméně
nenavštěvovali	navštěvovat	k5eNaImAgMnP	navštěvovat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byli	být	k5eAaImAgMnP	být
informováni	informovat	k5eAaBmNgMnP	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnSc1	jejich
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
muž	muž	k1gMnSc1	muž
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
paralyzovaný	paralyzovaný	k2eAgMnSc1d1	paralyzovaný
a	a	k8xC	a
neschopný	schopný	k2eNgMnSc1d1	neschopný
komunikace	komunikace	k1gFnPc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
vědom	vědom	k2eAgMnSc1d1	vědom
příčiny	příčina	k1gFnPc4	příčina
otcovy	otcův	k2eAgFnSc2d1	otcova
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dokládají	dokládat	k5eAaImIp3nP	dokládat
mnohé	mnohý	k2eAgInPc1d1	mnohý
dopisy	dopis	k1gInPc1	dopis
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nejspíše	nejspíše	k9	nejspíše
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
otcova	otcův	k2eAgFnSc1d1	otcova
"	"	kIx"	"
<g/>
mrtvice	mrtvice	k1gFnSc1	mrtvice
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgFnP	být
důsledkem	důsledek	k1gInSc7	důsledek
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
a	a	k8xC	a
stresu	stres	k1gInSc2	stres
z	z	k7c2	z
četných	četný	k2eAgNnPc2d1	četné
studií	studio	k1gNnPc2	studio
a	a	k8xC	a
úmorné	úmorný	k2eAgFnSc2d1	úmorná
obchodní	obchodní	k2eAgFnSc2d1	obchodní
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
W.	W.	kA	W.
S.	S.	kA	S.
Lovecraftovi	Lovecraft	k1gMnSc3	Lovecraft
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
progresivní	progresivní	k2eAgFnSc1d1	progresivní
paralýza	paralýza	k1gFnSc1	paralýza
<g/>
,	,	kIx,	,
mozková	mozkový	k2eAgFnSc1d1	mozková
forma	forma	k1gFnSc1	forma
pozdní	pozdní	k2eAgFnSc1d1	pozdní
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraftův	Lovecraftův	k2eAgMnSc1d1	Lovecraftův
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
pětiletého	pětiletý	k2eAgNnSc2d1	pětileté
dožívání	dožívání	k1gNnSc2	dožívání
projevoval	projevovat	k5eAaImAgInS	projevovat
agresivně	agresivně	k6eAd1	agresivně
a	a	k8xC	a
často	často	k6eAd1	často
celé	celý	k2eAgFnPc4d1	celá
hodiny	hodina	k1gFnPc4	hodina
hlasitě	hlasitě	k6eAd1	hlasitě
opakoval	opakovat	k5eAaImAgMnS	opakovat
nesmyslné	smyslný	k2eNgFnPc4d1	nesmyslná
útržky	útržka	k1gFnPc4	útržka
vět	věta	k1gFnPc2	věta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
pokročilé	pokročilý	k2eAgFnSc2d1	pokročilá
demence	demence	k1gFnSc2	demence
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
komplikován	komplikován	k2eAgInSc1d1	komplikován
proleženinami	proleženina	k1gFnPc7	proleženina
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
epileptického	epileptický	k2eAgInSc2d1	epileptický
záchvatu	záchvat	k1gInSc2	záchvat
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraftova	Lovecraftův	k2eAgFnSc1d1	Lovecraftova
matka	matka	k1gFnSc1	matka
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
navíc	navíc	k6eAd1	navíc
uschovávala	uschovávat	k5eAaImAgFnS	uschovávat
arzenovou	arzenový	k2eAgFnSc4d1	arzenový
tinkturu	tinktura	k1gFnSc4	tinktura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
využívala	využívat	k5eAaImAgFnS	využívat
jako	jako	k8xS	jako
preventivní	preventivní	k2eAgInSc1d1	preventivní
lék	lék	k1gInSc1	lék
proti	proti	k7c3	proti
syfilidě	syfilida	k1gFnSc3	syfilida
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jej	on	k3xPp3gMnSc4	on
tedy	tedy	k9	tedy
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
jeho	jeho	k3xOp3gFnSc1	jeho
značně	značně	k6eAd1	značně
neurotická	neurotický	k2eAgFnSc1d1	neurotická
<g/>
,	,	kIx,	,
panovačná	panovačný	k2eAgFnSc1d1	panovačná
a	a	k8xC	a
chorobně	chorobně	k6eAd1	chorobně
starostlivá	starostlivý	k2eAgFnSc1d1	starostlivá
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
tety	teta	k1gFnPc1	teta
(	(	kIx(	(
<g/>
Lillian	Lillian	k1gInSc1	Lillian
Delora	Delor	k1gMnSc2	Delor
a	a	k8xC	a
Annie	Annie	k1gFnSc2	Annie
Emeline	Emelin	k1gInSc5	Emelin
Phillipsovy	Phillipsův	k2eAgInPc4d1	Phillipsův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
snažily	snažit	k5eAaImAgFnP	snažit
vychovat	vychovat	k5eAaPmF	vychovat
pravého	pravý	k2eAgMnSc4d1	pravý
gentlemana	gentleman	k1gMnSc4	gentleman
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
dědeček	dědeček	k1gMnSc1	dědeček
Whipple	Whipple	k1gFnPc2	Whipple
Van	vana	k1gFnPc2	vana
Buren	Buren	k2eAgInSc4d1	Buren
Phillips	Phillips	k1gInSc4	Phillips
<g/>
,	,	kIx,	,
majetný	majetný	k2eAgMnSc1d1	majetný
průmyslník	průmyslník	k1gMnSc1	průmyslník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zažehl	zažehnout	k5eAaPmAgMnS	zažehnout
celoživotní	celoživotní	k2eAgFnSc4d1	celoživotní
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
tajemnu	tajemno	k1gNnSc3	tajemno
a	a	k8xC	a
E.	E.	kA	E.
A.	A.	kA	A.
Poeovi	Poeovi	k1gRnPc1	Poeovi
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
v	v	k7c4	v
rozmazlování	rozmazlování	k1gNnSc4	rozmazlování
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohl	moct	k5eAaImAgMnS	moct
jíst	jíst	k5eAaImF	jíst
cokoli	cokoli	k3yInSc4	cokoli
a	a	k8xC	a
kdykoli	kdykoli	k6eAd1	kdykoli
chtěl	chtít	k5eAaImAgMnS	chtít
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
jít	jít	k5eAaImF	jít
spát	spát	k5eAaImF	spát
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
zachtělo	zachtít	k5eAaPmAgNnS	zachtít
<g/>
.	.	kIx.	.
</s>
<s>
Chodíval	chodívat	k5eAaImAgInS	chodívat
tehdy	tehdy	k6eAd1	tehdy
spát	spát	k5eAaImF	spát
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
pozdních	pozdní	k2eAgFnPc6d1	pozdní
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Vyhýbal	vyhýbat	k5eAaImAgInS	vyhýbat
se	se	k3xPyFc4	se
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
začal	začít	k5eAaPmAgInS	začít
trpět	trpět	k5eAaImF	trpět
nočními	noční	k2eAgFnPc7d1	noční
můrami	můra	k1gFnPc7	můra
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
pavor	pavor	k1gMnSc1	pavor
nocturnus	nocturnus	k1gMnSc1	nocturnus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
matce	matka	k1gFnSc3	matka
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
rozpolcený	rozpolcený	k2eAgMnSc1d1	rozpolcený
až	až	k8xS	až
patologický	patologický	k2eAgMnSc1d1	patologický
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
přestože	přestože	k8xS	přestože
jej	on	k3xPp3gInSc4	on
jeho	jeho	k3xOp3gInSc4	jeho
matka	matka	k1gFnSc1	matka
velmi	velmi	k6eAd1	velmi
milovala	milovat	k5eAaImAgFnS	milovat
<g/>
,	,	kIx,	,
určitou	určitý	k2eAgFnSc7d1	určitá
měrou	míra	k1gFnSc7wR	míra
ho	on	k3xPp3gMnSc4	on
i	i	k9	i
nenáviděla	návidět	k5eNaImAgFnS	návidět
<g/>
.	.	kIx.	.
</s>
<s>
Obviňovala	obviňovat	k5eAaImAgFnS	obviňovat
jej	on	k3xPp3gInSc4	on
z	z	k7c2	z
otcova	otcův	k2eAgNnSc2d1	otcovo
úmrtí	úmrtí	k1gNnSc2	úmrtí
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přesvědčena	přesvědčit	k5eAaPmNgFnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
ošklivý	ošklivý	k2eAgInSc1d1	ošklivý
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraft	Lovecraft	k1gInSc1	Lovecraft
byl	být	k5eAaImAgInS	být
předčasně	předčasně	k6eAd1	předčasně
vyspělý	vyspělý	k2eAgInSc4d1	vyspělý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
číst	číst	k5eAaImF	číst
již	již	k6eAd1	již
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dětství	dětství	k1gNnSc2	dětství
i	i	k8xC	i
dospívání	dospívání	k1gNnSc2	dospívání
ale	ale	k8xC	ale
často	často	k6eAd1	často
stonal	stonat	k5eAaImAgMnS	stonat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
původ	původ	k1gInSc1	původ
těchto	tento	k3xDgFnPc2	tento
chorob	choroba	k1gFnPc2	choroba
byl	být	k5eAaImAgInS	být
částečně	částečně	k6eAd1	částečně
psychický	psychický	k2eAgInSc1d1	psychický
<g/>
.	.	kIx.	.
</s>
<s>
Povahou	povaha	k1gFnSc7	povaha
byl	být	k5eAaImAgInS	být
svárlivý	svárlivý	k2eAgMnSc1d1	svárlivý
<g/>
,	,	kIx,	,
samotářský	samotářský	k2eAgMnSc1d1	samotářský
a	a	k8xC	a
neústupný	ústupný	k2eNgMnSc1d1	neústupný
introvert	introvert	k1gMnSc1	introvert
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
také	také	k9	také
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
školu	škola	k1gFnSc4	škola
jen	jen	k9	jen
velice	velice	k6eAd1	velice
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
obcházení	obcházení	k1gNnSc1	obcházení
školní	školní	k2eAgFnSc2d1	školní
docházky	docházka	k1gFnSc2	docházka
umocnila	umocnit	k5eAaPmAgFnS	umocnit
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
občas	občas	k6eAd1	občas
i	i	k9	i
šikanován	šikanován	k2eAgMnSc1d1	šikanován
a	a	k8xC	a
odmítán	odmítán	k2eAgMnSc1d1	odmítán
jak	jak	k8xC	jak
svými	svůj	k3xOyFgMnPc7	svůj
vrstevníky	vrstevník	k1gMnPc7	vrstevník
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
sousedy	soused	k1gMnPc4	soused
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
za	za	k7c4	za
podivína	podivín	k1gMnSc4	podivín
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnSc4	jeho
nejranější	raný	k2eAgFnSc4d3	nejranější
četbu	četba	k1gFnSc4	četba
patřily	patřit	k5eAaImAgInP	patřit
Homérovy	Homérův	k2eAgInPc1d1	Homérův
eposy	epos	k1gInPc1	epos
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
dokonce	dokonce	k9	dokonce
přidružil	přidružit	k5eAaPmAgInS	přidružit
i	i	k9	i
vlastní	vlastní	k2eAgFnPc4d1	vlastní
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
či	či	k8xC	či
pohádky	pohádka	k1gFnPc4	pohádka
tisíce	tisíc	k4xCgInSc2	tisíc
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
podnítily	podnítit	k5eAaPmAgFnP	podnítit
jeho	jeho	k3xOp3gFnSc4	jeho
lásku	láska	k1gFnSc4	láska
ke	k	k7c3	k
starověké	starověký	k2eAgFnSc3d1	starověká
historii	historie	k1gFnSc3	historie
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
si	se	k3xPyFc3	se
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
také	také	k9	také
makabrózní	makabrózní	k2eAgFnSc4d1	makabrózní
literaturu	literatura	k1gFnSc4	literatura
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeus	k1gMnSc2	Poeus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
stal	stát	k5eAaPmAgInS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
literárním	literární	k2eAgInSc7d1	literární
vzorem	vzor	k1gInSc7	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zalíbily	zalíbit	k5eAaPmAgFnP	zalíbit
i	i	k9	i
přírodní	přírodní	k2eAgFnPc1d1	přírodní
vědy	věda	k1gFnPc1	věda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
obracel	obracet	k5eAaImAgMnS	obracet
k	k	k7c3	k
minulosti	minulost	k1gFnSc3	minulost
a	a	k8xC	a
vzorem	vzor	k1gInSc7	vzor
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
za	za	k7c4	za
ideály	ideál	k1gInPc4	ideál
vyspělé	vyspělý	k2eAgFnSc2d1	vyspělá
kultury	kultura	k1gFnSc2	kultura
považoval	považovat	k5eAaImAgInS	považovat
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
řecký	řecký	k2eAgInSc4d1	řecký
helénismus	helénismus	k1gInSc4	helénismus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dvanácti	dvanáct	k4xCc2	dvanáct
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc4	jeho
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
astronomii	astronomie	k1gFnSc4	astronomie
přerodil	přerodit	k5eAaPmAgMnS	přerodit
ve	v	k7c4	v
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
vášeň	vášeň	k1gFnSc4	vášeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
vzdělával	vzdělávat	k5eAaImAgMnS	vzdělávat
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
vyznal	vyznat	k5eAaBmAgMnS	vyznat
se	se	k3xPyFc4	se
v	v	k7c4	v
astronomii	astronomie	k1gFnSc4	astronomie
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
publikoval	publikovat	k5eAaBmAgMnS	publikovat
články	článek	k1gInPc7	článek
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
deníku	deník	k1gInSc6	deník
Providence	providence	k1gFnSc2	providence
Journal	Journal	k1gFnSc2	Journal
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k6eAd1	rovněž
obeznámen	obeznámit	k5eAaPmNgInS	obeznámit
s	s	k7c7	s
pracemi	práce	k1gFnPc7	práce
mnoha	mnoho	k4c2	mnoho
fyziků	fyzik	k1gMnPc2	fyzik
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
teorie	teorie	k1gFnPc4	teorie
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
nervově	nervově	k6eAd1	nervově
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jeho	jeho	k3xOp3gMnSc1	jeho
dědeček	dědeček	k1gMnSc1	dědeček
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
rodinný	rodinný	k2eAgInSc1d1	rodinný
krb	krb	k1gInSc1	krb
tím	ten	k3xDgNnSc7	ten
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraftovi	Lovecraftův	k2eAgMnPc1d1	Lovecraftův
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
čtvrti	čtvrt	k1gFnSc2	čtvrt
a	a	k8xC	a
menšího	malý	k2eAgInSc2d2	menší
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
příčinou	příčina	k1gFnSc7	příčina
jeho	jeho	k3xOp3gNnSc2	jeho
zhroucení	zhroucení	k1gNnSc2	zhroucení
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zánik	zánik	k1gInSc1	zánik
jeho	on	k3xPp3gInSc2	on
snu	sen	k1gInSc2	sen
o	o	k7c6	o
dráze	dráha	k1gFnSc6	dráha
astronoma	astronom	k1gMnSc2	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
vědu	věda	k1gFnSc4	věda
dostatečně	dostatečně	k6eAd1	dostatečně
neovládá	ovládat	k5eNaImIp3nS	ovládat
tolik	tolik	k6eAd1	tolik
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
vždy	vždy	k6eAd1	vždy
činila	činit	k5eAaImAgFnS	činit
nemalé	malý	k2eNgFnPc4d1	nemalá
obtíže	obtíž	k1gFnPc4	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
tedy	tedy	k9	tedy
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
středoškolské	středoškolský	k2eAgNnSc4d1	středoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
si	se	k3xPyFc3	se
velice	velice	k6eAd1	velice
přál	přát	k5eAaImAgMnS	přát
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Brownově	Brownův	k2eAgFnSc6d1	Brownova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
duševním	duševní	k2eAgNnSc6d1	duševní
rozpoložení	rozpoložení	k1gNnSc6	rozpoložení
strávil	strávit	k5eAaPmAgInS	strávit
celých	celý	k2eAgInPc2d1	celý
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
domu	dům	k1gInSc2	dům
vycházel	vycházet	k5eAaImAgMnS	vycházet
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
trpěl	trpět	k5eAaImAgMnS	trpět
úpornými	úporný	k2eAgFnPc7d1	úporná
bolestmi	bolest	k1gFnPc7	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
i	i	k9	i
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
tohoto	tento	k3xDgNnSc2	tento
poustevnictví	poustevnictví	k1gNnSc2	poustevnictví
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
pro	pro	k7c4	pro
redakci	redakce	k1gFnSc4	redakce
časopisu	časopis	k1gInSc2	časopis
The	The	k1gFnSc2	The
Argosy	Argos	k1gInPc4	Argos
sepsal	sepsat	k5eAaPmAgMnS	sepsat
veršovanou	veršovaný	k2eAgFnSc4d1	veršovaná
stížnost	stížnost	k1gFnSc4	stížnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
fádní	fádní	k2eAgInSc1d1	fádní
a	a	k8xC	a
komerční	komerční	k2eAgFnPc1d1	komerční
romance	romance	k1gFnPc1	romance
Freda	Fred	k1gMnSc2	Fred
Jacksona	Jackson	k1gMnSc2	Jackson
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
články	článek	k1gInPc1	článek
poté	poté	k6eAd1	poté
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
prezidenta	prezident	k1gMnSc2	prezident
UAPA	UAPA	kA	UAPA
(	(	kIx(	(
<g/>
United	United	k1gMnSc1	United
Amateur	Amateur	k1gMnSc1	Amateur
Press	Pressa	k1gFnPc2	Pressa
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
Edwarda	Edward	k1gMnSc2	Edward
F.	F.	kA	F.
Daase	Daas	k1gMnSc2	Daas
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gNnSc4	on
obratem	obratem	k6eAd1	obratem
přizval	přizvat	k5eAaPmAgMnS	přizvat
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
rovněž	rovněž	k9	rovněž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jeho	jeho	k3xOp3gFnSc4	jeho
prvotní	prvotní	k2eAgFnSc4d1	prvotní
díla	dílo	k1gNnPc4	dílo
The	The	k1gMnSc1	The
Tomb	Tomb	k1gMnSc1	Tomb
a	a	k8xC	a
Dagon	Dagon	k1gMnSc1	Dagon
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
také	také	k9	také
pěstovat	pěstovat	k5eAaImF	pěstovat
svoji	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
vášeň	vášeň	k1gFnSc4	vášeň
-	-	kIx~	-
dopisování	dopisování	k1gNnSc2	dopisování
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
byl	být	k5eAaImAgMnS	být
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
dopisovatel	dopisovatel	k1gMnSc1	dopisovatel
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
zálibou	záliba	k1gFnSc7	záliba
začal	začít	k5eAaPmAgInS	začít
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nervovém	nervový	k2eAgInSc6d1	nervový
otřesu	otřes	k1gInSc6	otřes
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
s	s	k7c7	s
bratrancem	bratranec	k1gMnSc7	bratranec
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
tzv.	tzv.	kA	tzv.
Lovecraftův	Lovecraftův	k2eAgInSc4d1	Lovecraftův
kruh	kruh	k1gInSc4	kruh
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
také	také	k9	také
datoval	datovat	k5eAaImAgMnS	datovat
své	svůj	k3xOyFgInPc4	svůj
dopisy	dopis	k1gInPc4	dopis
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
stě	sto	k4xCgFnPc1	sto
let	léto	k1gNnPc2	léto
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
před	před	k7c7	před
americkou	americký	k2eAgFnSc7d1	americká
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
korespondenty	korespondent	k1gMnPc4	korespondent
a	a	k8xC	a
přátele	přítel	k1gMnPc4	přítel
(	(	kIx(	(
<g/>
právě	právě	k9	právě
díky	díky	k7c3	díky
dopisování	dopisování	k1gNnSc3	dopisování
se	se	k3xPyFc4	se
plachý	plachý	k2eAgInSc4d1	plachý
Lovecraft	Lovecraft	k1gInSc4	Lovecraft
s	s	k7c7	s
mnohými	mnohý	k2eAgMnPc7d1	mnohý
s	s	k7c7	s
nich	on	k3xPp3gInPc2	on
setkal	setkat	k5eAaPmAgMnS	setkat
i	i	k9	i
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
pevná	pevný	k2eAgNnPc1d1	pevné
přátelství	přátelství	k1gNnPc1	přátelství
<g/>
)	)	kIx)	)
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
Robert	Robert	k1gMnSc1	Robert
E.	E.	kA	E.
Howard	Howard	k1gMnSc1	Howard
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
Conanovi	Conan	k1gMnSc6	Conan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nadaný	nadaný	k2eAgMnSc1d1	nadaný
básník	básník	k1gMnSc1	básník
Clark	Clark	k1gInSc4	Clark
Asthon	Asthon	k1gMnSc1	Asthon
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Robert	Robert	k1gMnSc1	Robert
Bloch	Bloch	k1gMnSc1	Bloch
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
příběhu	příběh	k1gInSc2	příběh
Psycho	psycha	k1gFnSc5	psycha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevyrovnala	vyrovnat	k5eNaPmAgFnS	vyrovnat
s	s	k7c7	s
duševním	duševní	k2eAgNnSc7d1	duševní
onemocněním	onemocnění	k1gNnSc7	onemocnění
a	a	k8xC	a
ztrátou	ztráta	k1gFnSc7	ztráta
svého	svůj	k3xOyFgMnSc2	svůj
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
hospitalizována	hospitalizován	k2eAgFnSc1d1	hospitalizována
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
léčebně	léčebna	k1gFnSc6	léčebna
jako	jako	k8xC	jako
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jí	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgMnS	být
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
nablízku	nablízku	k6eAd1	nablízku
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
nepovedené	povedený	k2eNgFnSc2d1	nepovedená
operace	operace	k1gFnSc2	operace
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ztráta	ztráta	k1gFnSc1	ztráta
Lovecrafta	Lovecrafta	k1gFnSc1	Lovecrafta
natolik	natolik	k6eAd1	natolik
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
opět	opět	k6eAd1	opět
postihly	postihnout	k5eAaPmAgFnP	postihnout
i	i	k9	i
sebevražedné	sebevražedný	k2eAgFnPc1d1	sebevražedná
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
zotavil	zotavit	k5eAaPmAgMnS	zotavit
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgMnS	navštívit
shromáždění	shromáždění	k1gNnSc3	shromáždění
amatérských	amatérský	k2eAgMnPc2d1	amatérský
žurnalistů	žurnalist	k1gMnPc2	žurnalist
UAPA	UAPA	kA	UAPA
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
chotí	choť	k1gFnSc7	choť
Soniou	Soniý	k2eAgFnSc7d1	Soniý
Greeneovou	Greeneův	k2eAgFnSc7d1	Greeneova
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
židovku	židovka	k1gFnSc4	židovka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
nalezla	nalézt	k5eAaBmAgFnS	nalézt
zalíbení	zalíbení	k1gNnSc4	zalíbení
a	a	k8xC	a
dvořila	dvořit	k5eAaImAgFnS	dvořit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraft	Lovecraft	k5eAaPmF	Lovecraft
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
rovněž	rovněž	k9	rovněž
zamiloval	zamilovat	k5eAaPmAgInS	zamilovat
a	a	k8xC	a
zbožňoval	zbožňovat	k5eAaImAgInS	zbožňovat
diskuze	diskuze	k1gFnPc4	diskuze
o	o	k7c6	o
filosofii	filosofie	k1gFnSc6	filosofie
a	a	k8xC	a
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
vedl	vést	k5eAaImAgMnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
cokoli	cokoli	k3yInSc4	cokoli
oznámili	oznámit	k5eAaPmAgMnP	oznámit
Howardovým	Howardův	k2eAgFnPc3d1	Howardova
upjatým	upjatý	k2eAgFnPc3d1	upjatá
tetám	teta	k1gFnPc3	teta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odstěhovali	odstěhovat	k5eAaPmAgMnP	odstěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
do	do	k7c2	do
čtvrtě	čtvrt	k1gFnSc2	čtvrt
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
pro	pro	k7c4	pro
Howarda	Howard	k1gMnSc4	Howard
zdrojem	zdroj	k1gInSc7	zdroj
velkého	velký	k2eAgInSc2d1	velký
úžasu	úžas	k1gInSc2	úžas
a	a	k8xC	a
inspirace	inspirace	k1gFnSc2	inspirace
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
však	však	k9	však
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
zanevíral	zanevírat	k5eAaImAgInS	zanevírat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
ale	ale	k9	ale
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nenaplněno	naplněn	k2eNgNnSc1d1	nenaplněno
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Lovecraft	Lovecraft	k1gInSc1	Lovecraft
měl	mít	k5eAaImAgInS	mít
značnou	značný	k2eAgFnSc4d1	značná
averzi	averze	k1gFnSc4	averze
k	k	k7c3	k
intimnímu	intimní	k2eAgInSc3d1	intimní
styku	styk	k1gInSc3	styk
(	(	kIx(	(
<g/>
Lovecraft	Lovecraft	k1gInSc1	Lovecraft
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
asexuálem	asexuále	k1gNnSc7	asexuále
<g/>
;	;	kIx,	;
jistý	jistý	k2eAgInSc4d1	jistý
vliv	vliv	k1gInSc4	vliv
zde	zde	k6eAd1	zde
také	také	k9	také
sehrála	sehrát	k5eAaPmAgFnS	sehrát
skutečnost	skutečnost	k1gFnSc1	skutečnost
silného	silný	k2eAgInSc2d1	silný
komplexu	komplex	k1gInSc2	komplex
méněcennosti	méněcennost	k1gFnSc2	méněcennost
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
vypěstovala	vypěstovat	k5eAaPmAgFnS	vypěstovat
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sonia	Sonia	k1gFnSc1	Sonia
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
klobouky	klobouk	k1gInPc7	klobouk
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
brzy	brzy	k6eAd1	brzy
zkrachoval	zkrachovat	k5eAaPmAgInS	zkrachovat
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
donucena	donutit	k5eAaPmNgFnS	donutit
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
i	i	k9	i
nadále	nadále	k6eAd1	nadále
Lovecrafta	Lovecrafta	k1gFnSc1	Lovecrafta
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
nemohl	moct	k5eNaImAgMnS	moct
nalézt	nalézt	k5eAaPmF	nalézt
žádné	žádný	k3yNgNnSc4	žádný
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
čtyřiatřicetiletého	čtyřiatřicetiletý	k2eAgMnSc4d1	čtyřiatřicetiletý
muže	muž	k1gMnSc4	muž
bez	bez	k7c2	bez
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
pracovních	pracovní	k2eAgFnPc2d1	pracovní
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
pár	pár	k1gInSc1	pár
přátelsky	přátelsky	k6eAd1	přátelsky
rozešel	rozejít	k5eAaPmAgInS	rozejít
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
úředně	úředně	k6eAd1	úředně
nerozvedli	rozvést	k5eNaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
zde	zde	k6eAd1	zde
ještě	ještě	k9	ještě
po	po	k7c4	po
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
natolik	natolik	k6eAd1	natolik
znelíbil	znelíbit	k5eAaPmAgInS	znelíbit
(	(	kIx(	(
<g/>
především	především	k9	především
z	z	k7c2	z
rasových	rasový	k2eAgInPc2d1	rasový
důvodů	důvod	k1gInPc2	důvod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
sepsal	sepsat	k5eAaPmAgMnS	sepsat
i	i	k9	i
značně	značně	k6eAd1	značně
xenofobní	xenofobní	k2eAgFnSc4d1	xenofobní
a	a	k8xC	a
rasistickou	rasistický	k2eAgFnSc4d1	rasistická
povídku	povídka	k1gFnSc4	povídka
The	The	k1gFnSc2	The
Horror	horror	k1gInSc1	horror
at	at	k?	at
Red	Red	k1gMnSc1	Red
Hook	Hook	k1gMnSc1	Hook
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
rodnou	rodný	k2eAgFnSc4d1	rodná
zemi	zem	k1gFnSc4	zem
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vydat	vydat	k5eAaPmF	vydat
až	až	k9	až
svém	svůj	k3xOyFgInSc6	svůj
zpětném	zpětný	k2eAgInSc6d1	zpětný
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Providence	providence	k1gFnSc2	providence
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
cílem	cíl	k1gInSc7	cíl
jeho	jeho	k3xOp3gFnPc2	jeho
cest	cesta	k1gFnPc2	cesta
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
především	především	k9	především
Itálie	Itálie	k1gFnSc1	Itálie
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
folklór	folklór	k1gInSc1	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
rodné	rodný	k2eAgFnSc2d1	rodná
Providence	providence	k1gFnSc2	providence
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zabydlel	zabydlet	k5eAaPmAgMnS	zabydlet
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
domě	dům	k1gInSc6	dům
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
10	[number]	k4	10
Barnes	Barnes	k1gInSc1	Barnes
Street	Street	k1gInSc4	Street
<g/>
,	,	kIx,	,
stejnou	stejný	k2eAgFnSc4d1	stejná
adresu	adresa	k1gFnSc4	adresa
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Willet	Willet	k1gInSc1	Willet
v	v	k7c6	v
románu	román	k1gInSc6	román
Podivný	podivný	k2eAgInSc1d1	podivný
případ	případ	k1gInSc1	případ
Charlese	Charles	k1gMnSc2	Charles
Dextera	Dexter	k1gMnSc2	Dexter
Warda	Ward	k1gMnSc2	Ward
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
poté	poté	k6eAd1	poté
sepsal	sepsat	k5eAaPmAgMnS	sepsat
svá	svůj	k3xOyFgNnPc4	svůj
nejznámější	známý	k2eAgNnPc4d3	nejznámější
a	a	k8xC	a
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
The	The	k1gFnSc4	The
Call	Calla	k1gFnPc2	Calla
of	of	k?	of
Cthulhu	Cthulh	k1gInSc2	Cthulh
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Shadow	Shadow	k1gFnSc2	Shadow
over	over	k1gMnSc1	over
Innsmouth	Innsmouth	k1gMnSc1	Innsmouth
<g/>
,	,	kIx,	,
At	At	k1gMnSc1	At
the	the	k?	the
Mountains	Mountains	k1gInSc1	Mountains
of	of	k?	of
Madness	Madness	k1gInSc1	Madness
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Shadow	Shadow	k1gFnSc2	Shadow
out	out	k?	out
of	of	k?	of
Time	Time	k1gFnSc1	Time
nebo	nebo	k8xC	nebo
The	The	k1gMnSc1	The
Dream-quest	Dreamuest	k1gMnSc1	Dream-quest
to	ten	k3xDgNnSc4	ten
unknown	unknown	k1gNnSc4	unknown
Kadath	Kadath	k1gInSc4	Kadath
<g/>
.	.	kIx.	.
</s>
<s>
Přijímal	přijímat	k5eAaImAgMnS	přijímat
začínající	začínající	k2eAgMnPc4d1	začínající
mladé	mladý	k2eAgMnPc4d1	mladý
spisovatele	spisovatel	k1gMnPc4	spisovatel
a	a	k8xC	a
obdivovatele	obdivovatel	k1gMnPc4	obdivovatel
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
<g/>
,	,	kIx,	,
radil	radit	k5eAaImAgInS	radit
a	a	k8xC	a
také	také	k6eAd1	také
je	být	k5eAaImIp3nS	být
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
širokého	široký	k2eAgInSc2d1	široký
epistolárního	epistolární	k2eAgInSc2d1	epistolární
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
jej	on	k3xPp3gInSc4	on
velice	velice	k6eAd1	velice
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
smrt	smrt	k1gFnSc4	smrt
jeho	on	k3xPp3gMnSc2	on
dobrého	dobrý	k2eAgMnSc2d1	dobrý
přítele	přítel	k1gMnSc2	přítel
R.	R.	kA	R.
E.	E.	kA	E.
Howarda	Howard	k1gMnSc2	Howard
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
jeho	jeho	k3xOp3gNnSc4	jeho
vlastní	vlastní	k2eAgNnSc4d1	vlastní
smrtelné	smrtelný	k2eAgNnSc4d1	smrtelné
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
když	když	k8xS	když
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
rakovina	rakovina	k1gFnSc1	rakovina
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
posílená	posílený	k2eAgFnSc1d1	posílená
navíc	navíc	k6eAd1	navíc
chronickým	chronický	k2eAgInSc7d1	chronický
zánětem	zánět	k1gInSc7	zánět
ledvin	ledvina	k1gFnPc2	ledvina
(	(	kIx(	(
<g/>
Brightova	Brightův	k2eAgFnSc1d1	Brightova
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
považoval	považovat	k5eAaImAgMnS	považovat
své	svůj	k3xOyFgFnPc4	svůj
obtíže	obtíž	k1gFnPc4	obtíž
za	za	k7c4	za
pouhou	pouhý	k2eAgFnSc4d1	pouhá
chřipkovitou	chřipkovitý	k2eAgFnSc4d1	chřipkovitý
infekci	infekce	k1gFnSc4	infekce
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jej	on	k3xPp3gMnSc4	on
sužovala	sužovat	k5eAaImAgFnS	sužovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
jediné	jediný	k2eAgFnPc1d1	jediná
co	co	k9	co
však	však	k9	však
mohli	moct	k5eAaImAgMnP	moct
lékaři	lékař	k1gMnPc1	lékař
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
podávat	podávat	k5eAaImF	podávat
nemocnému	nemocný	k1gMnSc3	nemocný
Lovecraftovi	Lovecraft	k1gMnSc3	Lovecraft
morfin	morfin	k1gInSc4	morfin
pro	pro	k7c4	pro
utišení	utišení	k1gNnSc4	utišení
bolesti	bolest	k1gFnSc2	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Básníkův	básníkův	k2eAgInSc1d1	básníkův
postoj	postoj	k1gInSc1	postoj
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
stavu	stav	k1gInSc3	stav
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
stoický	stoický	k2eAgInSc1d1	stoický
a	a	k8xC	a
odevzdaný	odevzdaný	k2eAgInSc1d1	odevzdaný
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
dojmy	dojem	k1gInPc4	dojem
zaznamenával	zaznamenávat	k5eAaImAgInS	zaznamenávat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
deníku	deník	k1gInSc2	deník
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nepřetržitých	přetržitý	k2eNgFnPc6d1	nepřetržitá
bolestech	bolest	k1gFnPc6	bolest
ještě	ještě	k6eAd1	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
hrobě	hrob	k1gInSc6	hrob
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
mu	on	k3xPp3gMnSc3	on
postavili	postavit	k5eAaPmAgMnP	postavit
hrob	hrob	k1gInSc1	hrob
vlastní	vlastní	k2eAgInSc1d1	vlastní
<g/>
,	,	kIx,	,
zdobený	zdobený	k2eAgInSc1d1	zdobený
úryvkem	úryvek	k1gInSc7	úryvek
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
básně	báseň	k1gFnSc2	báseň
<g/>
,	,	kIx,	,
znějící	znějící	k2eAgMnSc1d1	znějící
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
I	i	k9	i
AM	AM	kA	AM
PROVIDENCE	providence	k1gFnSc1	providence
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c4	v
šíření	šíření	k1gNnSc4	šíření
jeho	on	k3xPp3gInSc2	on
mýtu	mýtus	k1gInSc2	mýtus
Cthulhu	Cthulh	k1gInSc2	Cthulh
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
tiskli	tisknout	k5eAaImAgMnP	tisknout
jeho	jeho	k3xOp3gFnPc4	jeho
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
úporné	úporný	k2eAgFnSc6d1	úporná
sebekritice	sebekritika	k1gFnSc6	sebekritika
často	často	k6eAd1	často
zdráhal	zdráhat	k5eAaImAgInS	zdráhat
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
patří	patřit	k5eAaImIp3nS	patřit
výlučně	výlučně	k6eAd1	výlučně
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc4	povídka
<g/>
,	,	kIx,	,
novely	novela	k1gFnPc4	novela
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc4	esej
a	a	k8xC	a
novinové	novinový	k2eAgInPc4d1	novinový
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
vydávány	vydávat	k5eAaPmNgFnP	vydávat
pouze	pouze	k6eAd1	pouze
časopiseckou	časopisecký	k2eAgFnSc7d1	časopisecká
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
společnostmi	společnost	k1gFnPc7	společnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byly	být	k5eAaImAgInP	být
Astounding	Astounding	k1gInSc4	Astounding
stories	storiesa	k1gFnPc2	storiesa
<g/>
,	,	kIx,	,
The	The	k1gFnSc7	The
Argosy	Argos	k1gInPc7	Argos
<g/>
,	,	kIx,	,
Weird	Weird	k1gMnSc1	Weird
Tales	Tales	k1gMnSc1	Tales
<g/>
,	,	kIx,	,
Tryout	Tryout	k1gMnSc1	Tryout
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
V	v	k7c6	v
měkké	měkký	k2eAgFnSc6d1	měkká
vazbě	vazba	k1gFnSc6	vazba
vydal	vydat	k5eAaPmAgInS	vydat
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc1	dva
práce	práce	k1gFnPc1	práce
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Odstrašující	odstrašující	k2eAgInSc1d1	odstrašující
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Shunned	Shunned	k1gMnSc1	Shunned
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stín	stín	k1gInSc1	stín
nad	nad	k7c7	nad
Innsmouthem	Innsmouth	k1gInSc7	Innsmouth
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Shadow	Shadow	k1gMnSc1	Shadow
Over	Over	k1gMnSc1	Over
Innsmouth	Innsmouth	k1gMnSc1	Innsmouth
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
přímého	přímý	k2eAgMnSc4d1	přímý
následovníka	následovník	k1gMnSc4	následovník
E.	E.	kA	E.
A.	A.	kA	A.
Poea	Poea	k1gMnSc1	Poea
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
podobnosti	podobnost	k1gFnSc3	podobnost
stylu	styl	k1gInSc2	styl
i	i	k8xC	i
žánru	žánr	k1gInSc2	žánr
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
nazýván	nazývat	k5eAaImNgInS	nazývat
Poem	poema	k1gFnPc2	poema
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
motivem	motiv	k1gInSc7	motiv
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
celoživotním	celoživotní	k2eAgNnSc6d1	celoživotní
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
narušení	narušení	k1gNnSc1	narušení
nám	my	k3xPp1nPc3	my
známého	známý	k1gMnSc4	známý
trojrozměrného	trojrozměrný	k2eAgInSc2d1	trojrozměrný
světa	svět	k1gInSc2	svět
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spise	spis	k1gInSc6	spis
Nadpřirozená	nadpřirozený	k2eAgFnSc1d1	nadpřirozená
hrůza	hrůza	k1gFnSc1	hrůza
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
poté	poté	k6eAd1	poté
definoval	definovat	k5eAaBmAgMnS	definovat
hororový	hororový	k2eAgInSc4d1	hororový
žánr	žánr	k1gInSc4	žánr
a	a	k8xC	a
shrnul	shrnout	k5eAaPmAgMnS	shrnout
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Svému	svůj	k1gMnSc3	svůj
vzoru-Poeovi	vzoru-Poeus	k1gMnSc3	vzoru-Poeus
věnoval	věnovat	k5eAaPmAgMnS	věnovat
hned	hned	k6eAd1	hned
celou	celý	k2eAgFnSc4d1	celá
kapitolu	kapitola	k1gFnSc4	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraft	Lovecraft	k1gInSc1	Lovecraft
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
sebekritický	sebekritický	k2eAgInSc1d1	sebekritický
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
svých	svůj	k3xOyFgFnPc2	svůj
prací	práce	k1gFnPc2	práce
zničil	zničit	k5eAaPmAgMnS	zničit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
zavrhl	zavrhnout	k5eAaPmAgInS	zavrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
považoval	považovat	k5eAaImAgMnS	považovat
The	The	k1gMnSc2	The
Colour	Colour	k1gMnSc1	Colour
Out	Out	k1gMnSc1	Out
of	of	k?	of
Space	Space	k1gMnSc1	Space
<g/>
,	,	kIx,	,
obdobné	obdobný	k2eAgFnPc4d1	obdobná
obliby	obliba	k1gFnPc4	obliba
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
pouze	pouze	k6eAd1	pouze
několika	několik	k4yIc3	několik
dalším	další	k2eAgInPc3d1	další
dílům	díl	k1gInPc3	díl
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
povídkách	povídka	k1gFnPc6	povídka
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
objevují	objevovat	k5eAaImIp3nP	objevovat
pradávné	pradávný	k2eAgFnPc1d1	pradávná
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
rasy	rasa	k1gFnSc2	rasa
uctívající	uctívající	k2eAgNnPc4d1	uctívající
temná	temný	k2eAgNnPc4d1	temné
božstva	božstvo	k1gNnPc4	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
bohové	bůh	k1gMnPc1	bůh
i	i	k8xC	i
jejich	jejich	k3xOp3gMnPc1	jejich
stoupenci	stoupenec	k1gMnPc1	stoupenec
však	však	k9	však
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
vyhoštěni	vyhostit	k5eAaPmNgMnP	vyhostit
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
ale	ale	k9	ale
vrátit	vrátit	k5eAaPmF	vrátit
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nP	vrátit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
práce	práce	k1gFnPc4	práce
i	i	k9	i
celoživotní	celoživotní	k2eAgInSc1d1	celoživotní
postoj	postoj	k1gInSc1	postoj
vůči	vůči	k7c3	vůči
světu	svět	k1gInSc3	svět
byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
hluboce	hluboko	k6eAd1	hluboko
pesimistický	pesimistický	k2eAgInSc1d1	pesimistický
až	až	k8xS	až
cynický	cynický	k2eAgMnSc1d1	cynický
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
měl	mít	k5eAaImAgInS	mít
však	však	k9	však
mnohokrát	mnohokrát	k6eAd1	mnohokrát
uplatňoval	uplatňovat	k5eAaImAgInS	uplatňovat
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
a	a	k8xC	a
jemnou	jemný	k2eAgFnSc4d1	jemná
ironii	ironie	k1gFnSc4	ironie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
náměty	námět	k1gInPc4	námět
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
např.	např.	kA	např.
mystické	mystický	k2eAgNnSc1d1	mystické
směřování	směřování	k1gNnSc1	směřování
k	k	k7c3	k
tragickému	tragický	k2eAgInSc3d1	tragický
osudu	osud	k1gInSc3	osud
a	a	k8xC	a
nemožnost	nemožnost	k1gFnSc4	nemožnost
mu	on	k3xPp3gMnSc3	on
utéct	utéct	k5eAaPmF	utéct
-	-	kIx~	-
naopak	naopak	k6eAd1	naopak
nutkavé	nutkavý	k2eAgNnSc4d1	nutkavé
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
pokračovat	pokračovat	k5eAaImF	pokračovat
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
neznáma	neznámo	k1gNnSc2	neznámo
<g/>
,	,	kIx,	,
mimozemské	mimozemský	k2eAgFnPc1d1	mimozemská
bytosti	bytost	k1gFnPc1	bytost
z	z	k7c2	z
rozličných	rozličný	k2eAgInPc2d1	rozličný
rozměrů	rozměr	k1gInPc2	rozměr
lhostejné	lhostejný	k2eAgFnSc2d1	lhostejná
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
zneužívání	zneužívání	k1gNnSc3	zneužívání
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
existenciální	existenciální	k2eAgFnSc1d1	existenciální
úzkost	úzkost	k1gFnSc1	úzkost
<g/>
,	,	kIx,	,
strach	strach	k1gInSc1	strach
z	z	k7c2	z
rozpadu	rozpad	k1gInSc2	rozpad
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
zobrazení	zobrazení	k1gNnSc2	zobrazení
prázdnoty	prázdnota	k1gFnSc2	prázdnota
a	a	k8xC	a
kosmické	kosmický	k2eAgFnSc2d1	kosmická
nicoty	nicota	k1gFnSc2	nicota
<g/>
...	...	k?	...
H.	H.	kA	H.
P.	P.	kA	P.
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
kupříkladu	kupříkladu	k6eAd1	kupříkladu
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
vytvořením	vytvoření	k1gNnSc7	vytvoření
nukleárních	nukleární	k2eAgFnPc2d1	nukleární
zbraní	zbraň	k1gFnPc2	zbraň
i	i	k8xC	i
bezohledným	bezohledný	k2eAgInSc7d1	bezohledný
výzkumem	výzkum	k1gInSc7	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Ve	v	k7c6	v
zdech	zeď	k1gFnPc6	zeď
Eryxu	Eryx	k1gInSc2	Eryx
popsal	popsat	k5eAaPmAgMnS	popsat
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pozemské	pozemský	k2eAgFnPc1d1	pozemská
výpravy	výprava	k1gFnPc1	výprava
ve	v	k7c6	v
vesmírných	vesmírný	k2eAgFnPc6d1	vesmírná
lodích	loď	k1gFnPc6	loď
podnikají	podnikat	k5eAaImIp3nP	podnikat
nájezdy	nájezd	k1gInPc1	nájezd
<g/>
,	,	kIx,	,
Conquistu	conquista	k1gFnSc4	conquista
<g/>
,	,	kIx,	,
za	za	k7c7	za
cílem	cíl	k1gInSc7	cíl
uloupení	uloupení	k1gNnSc2	uloupení
krystalické	krystalický	k2eAgFnSc2d1	krystalická
substance	substance	k1gFnSc2	substance
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
rase	rasa	k1gFnSc3	rasa
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
známé	známý	k2eAgFnSc6d1	známá
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
zfilmované	zfilmovaný	k2eAgFnSc6d1	zfilmovaná
povídce	povídka	k1gFnSc6	povídka
Herbert	Herbert	k1gMnSc1	Herbert
West-Reanimátor	West-Reanimátor	k1gMnSc1	West-Reanimátor
se	se	k3xPyFc4	se
také	také	k9	také
objevuje	objevovat	k5eAaImIp3nS	objevovat
jeho	jeho	k3xOp3gNnSc1	jeho
poněkud	poněkud	k6eAd1	poněkud
komický	komický	k2eAgInSc1d1	komický
rasismus	rasismus	k1gInSc1	rasismus
a	a	k8xC	a
xenofobie	xenofobie	k1gFnSc1	xenofobie
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
xenofobně	xenofobně	k6eAd1	xenofobně
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
zatvrzelým	zatvrzelý	k2eAgMnSc7d1	zatvrzelý
zastáncem	zastánce	k1gMnSc7	zastánce
prezidenta	prezident	k1gMnSc2	prezident
Franklina	Franklina	k1gFnSc1	Franklina
Delano	Delana	k1gFnSc5	Delana
Roosevelta	Roosevelt	k1gMnSc4	Roosevelt
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
za	za	k7c7	za
"	"	kIx"	"
<g/>
New	New	k1gMnSc2	New
Deal	Deal	k1gMnSc1	Deal
demokrata	demokrat	k1gMnSc2	demokrat
a	a	k8xC	a
antikomunistu	antikomunista	k1gMnSc4	antikomunista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
tyto	tento	k3xDgInPc4	tento
postoje	postoj	k1gInPc4	postoj
přehodnotil	přehodnotit	k5eAaPmAgInS	přehodnotit
a	a	k8xC	a
velice	velice	k6eAd1	velice
se	se	k3xPyFc4	se
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
styděl	stydět	k5eAaImAgMnS	stydět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
rasismus	rasismus	k1gInSc1	rasismus
a	a	k8xC	a
xenofobie	xenofobie	k1gFnPc1	xenofobie
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
za	za	k7c4	za
komické	komický	k2eAgNnSc4d1	komické
<g/>
,	,	kIx,	,
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
odlišnému	odlišný	k2eAgNnSc3d1	odlišné
etniku	etnikum	k1gNnSc3	etnikum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
židovkou	židovka	k1gFnSc7	židovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
ženské	ženský	k2eAgFnPc1d1	ženská
postavy	postava	k1gFnPc1	postava
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezeně	omezeně	k6eAd1	omezeně
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nezaujímají	zaujímat	k5eNaImIp3nP	zaujímat
významné	významný	k2eAgFnPc4d1	významná
role	role	k1gFnPc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
občas	občas	k6eAd1	občas
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
pro	pro	k7c4	pro
misogynitu	misogynita	k1gFnSc4	misogynita
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
množství	množství	k1gNnSc4	množství
ženských	ženský	k2eAgFnPc2d1	ženská
postav	postava	k1gFnPc2	postava
však	však	k9	však
svědčí	svědčit	k5eAaImIp3nS	svědčit
spíše	spíše	k9	spíše
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
plachosti	plachost	k1gFnSc6	plachost
a	a	k8xC	a
nesmělosti	nesmělost	k1gFnSc3	nesmělost
vůči	vůči	k7c3	vůči
ženám	žena	k1gFnPc3	žena
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Howard	Howard	k1gMnSc1	Howard
Phillips	Phillipsa	k1gFnPc2	Phillipsa
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
oplýval	oplývat	k5eAaImAgMnS	oplývat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
,	,	kIx,	,
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
pamětí	paměť	k1gFnSc7	paměť
a	a	k8xC	a
tvořivostí	tvořivost	k1gFnSc7	tvořivost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
tichý	tichý	k2eAgInSc1d1	tichý
<g/>
,	,	kIx,	,
melancholický	melancholický	k2eAgInSc1d1	melancholický
<g/>
,	,	kIx,	,
zádumčivý	zádumčivý	k2eAgInSc1d1	zádumčivý
a	a	k8xC	a
zasněný	zasněný	k2eAgMnSc1d1	zasněný
člověk	člověk	k1gMnSc1	člověk
velmi	velmi	k6eAd1	velmi
laskavé	laskavý	k2eAgFnPc1d1	laskavá
a	a	k8xC	a
úzkostné	úzkostný	k2eAgFnPc1d1	úzkostná
povahy	povaha	k1gFnPc1	povaha
<g/>
,	,	kIx,	,
a	a	k8xC	a
ačkoli	ačkoli	k8xS	ačkoli
jeho	jeho	k3xOp3gFnSc3	jeho
tvorbě	tvorba	k1gFnSc3	tvorba
dominovala	dominovat	k5eAaImAgFnS	dominovat
především	především	k6eAd1	především
nadpřirozená	nadpřirozený	k2eAgNnPc4d1	nadpřirozené
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
byl	být	k5eAaImAgInS	být
osobou	osoba	k1gFnSc7	osoba
racionální	racionální	k2eAgInPc1d1	racionální
<g/>
.	.	kIx.	.
</s>
<s>
Netajil	tajit	k5eNaImAgMnS	tajit
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
anglofilií	anglofilie	k1gFnSc7	anglofilie
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
láskou	láska	k1gFnSc7	láska
ke	k	k7c3	k
kočkám	kočka	k1gFnPc3	kočka
či	či	k8xC	či
minulosti	minulost	k1gFnSc3	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
jej	on	k3xPp3gInSc4	on
velmi	velmi	k6eAd1	velmi
zajímaly	zajímat	k5eAaImAgInP	zajímat
jeho	jeho	k3xOp3gInPc1	jeho
sny	sen	k1gInPc1	sen
<g/>
.	.	kIx.	.
</s>
<s>
Zbožňoval	zbožňovat	k5eAaImAgMnS	zbožňovat
vycházky	vycházka	k1gFnPc4	vycházka
po	po	k7c6	po
starobylých	starobylý	k2eAgNnPc6d1	starobylé
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
křivolakých	křivolaký	k2eAgFnPc6d1	křivolaká
uličkách	ulička	k1gFnPc6	ulička
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
krajině	krajina	k1gFnSc6	krajina
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Netajil	tajit	k5eNaImAgInS	tajit
se	se	k3xPyFc4	se
ale	ale	k9	ale
také	také	k9	také
svoji	svůj	k3xOyFgFnSc4	svůj
nechutí	nechutit	k5eAaImIp3nP	nechutit
k	k	k7c3	k
odlišným	odlišný	k2eAgFnPc3d1	odlišná
rasám	rasa	k1gFnPc3	rasa
<g/>
,	,	kIx,	,
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
a	a	k8xC	a
modernímu	moderní	k2eAgInSc3d1	moderní
ezoterismu	ezoterismus	k1gInSc3	ezoterismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
filosofický	filosofický	k2eAgInSc1d1	filosofický
pohled	pohled	k1gInSc1	pohled
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
existenciální	existenciální	k2eAgInSc1d1	existenciální
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
mechanického	mechanický	k2eAgInSc2d1	mechanický
racionalismu	racionalismus	k1gInSc2	racionalismus
a	a	k8xC	a
nihilismu	nihilismus	k1gInSc2	nihilismus
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
byl	být	k5eAaImAgMnS	být
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
čtenářem	čtenář	k1gMnSc7	čtenář
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
soukromé	soukromý	k2eAgFnSc6d1	soukromá
knihovně	knihovna	k1gFnSc6	knihovna
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
kolem	kolem	k7c2	kolem
2	[number]	k4	2
000	[number]	k4	000
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
děl	dělo	k1gNnPc2	dělo
velmi	velmi	k6eAd1	velmi
starých	starý	k2eAgInPc2d1	starý
a	a	k8xC	a
cenných	cenný	k2eAgInPc2d1	cenný
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
náboženské	náboženský	k2eAgNnSc4d1	náboženské
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
R.	R.	kA	R.
E.	E.	kA	E.
Howardovi	Howardův	k2eAgMnPc1d1	Howardův
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Celkově	celkově	k6eAd1	celkově
vzato	vzít	k5eAaPmNgNnS	vzít
si	se	k3xPyFc3	se
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
existence	existence	k1gFnSc1	existence
ústřední	ústřední	k2eAgFnSc2d1	ústřední
kosmické	kosmický	k2eAgFnSc2d1	kosmická
vůle	vůle	k1gFnSc2	vůle
nebo	nebo	k8xC	nebo
věčného	věčný	k2eAgNnSc2d1	věčné
setrvání	setrvání	k1gNnSc2	setrvání
osobnosti	osobnost	k1gFnSc2	osobnost
je	být	k5eAaImIp3nS	být
zatraceně	zatraceně	k6eAd1	zatraceně
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ty	ten	k3xDgFnPc1	ten
nejsměšnější	směšný	k2eAgFnPc1d3	nejsměšnější
a	a	k8xC	a
nejnesmyslnější	smyslný	k2eNgFnPc1d3	nejnesmyslnější
věci	věc	k1gFnPc1	věc
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc4	jaký
může	moct	k5eAaImIp3nS	moct
kdokoli	kdokoli	k3yInSc1	kdokoli
říct	říct	k5eAaPmF	říct
o	o	k7c6	o
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
a	a	k8xC	a
já	já	k3xPp1nSc1	já
nejsem	být	k5eNaImIp1nS	být
tak	tak	k6eAd1	tak
slovíčkářský	slovíčkářský	k2eAgMnSc1d1	slovíčkářský
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
nepředstíral	předstírat	k5eNaImAgMnS	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
pokládám	pokládat	k5eAaImIp1nS	pokládat
za	za	k7c4	za
bludný	bludný	k2eAgInSc4d1	bludný
a	a	k8xC	a
nesmyslný	smyslný	k2eNgInSc4d1	nesmyslný
přelud	přelud	k1gInSc4	přelud
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
jsem	být	k5eAaImIp1nS	být
agnostik	agnostik	k1gMnSc1	agnostik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
vší	všecek	k3xTgFnSc7	všecek
rozhodností	rozhodnost	k1gFnSc7	rozhodnost
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
musím	muset	k5eAaImIp1nS	muset
se	se	k3xPyFc4	se
prozatímně	prozatímně	k6eAd1	prozatímně
a	a	k8xC	a
teoreticky	teoreticky	k6eAd1	teoreticky
řadit	řadit	k5eAaImF	řadit
k	k	k7c3	k
ateistům	ateista	k1gMnPc3	ateista
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Většina	většina	k1gFnSc1	většina
postav	postava	k1gFnPc2	postava
Lovecraftových	Lovecraftův	k2eAgNnPc2d1	Lovecraftovo
děl	dělo	k1gNnPc2	dělo
jsou	být	k5eAaImIp3nP	být
pozitivisticky	pozitivisticky	k6eAd1	pozitivisticky
uvažující	uvažující	k2eAgMnPc1d1	uvažující
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
věří	věřit	k5eAaImIp3nP	věřit
spíše	spíše	k9	spíše
vědě	věda	k1gFnSc3	věda
než	než	k8xS	než
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
zřejmě	zřejmě	k6eAd1	zřejmě
odráží	odrážet	k5eAaImIp3nS	odrážet
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
Lovecrafta	Lovecraft	k1gInSc2	Lovecraft
samotného	samotný	k2eAgInSc2d1	samotný
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
i	i	k9	i
tento	tento	k3xDgInSc1	tento
apriorní	apriorní	k2eAgInSc1d1	apriorní
pozitivismus	pozitivismus	k1gInSc1	pozitivismus
Lovecraft	Lovecrafta	k1gFnPc2	Lovecrafta
nezřídka	nezřídka	k6eAd1	nezřídka
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
a	a	k8xC	a
paroduje	parodovat	k5eAaImIp3nS	parodovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Herbert	Herbert	k1gMnSc1	Herbert
West	West	k1gMnSc1	West
<g/>
-	-	kIx~	-
<g/>
Reanimator	Reanimator	k1gMnSc1	Reanimator
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
fikce	fikce	k1gFnPc1	fikce
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
citovány	citován	k2eAgInPc1d1	citován
jako	jako	k8xC	jako
příklad	příklad	k1gInSc1	příklad
tzv.	tzv.	kA	tzv.
misoteismu	misoteismus	k1gInSc2	misoteismus
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
božstva	božstvo	k1gNnPc1	božstvo
sice	sice	k8xC	sice
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
zlá	zlý	k2eAgFnSc1d1	zlá
a	a	k8xC	a
vůči	vůči	k7c3	vůči
člověku	člověk	k1gMnSc3	člověk
nepřátelská	přátelský	k2eNgFnSc1d1	nepřátelská
<g/>
.	.	kIx.	.
</s>
<s>
Fiktivní	fiktivní	k2eAgNnSc1d1	fiktivní
náboženství	náboženství	k1gNnSc1	náboženství
z	z	k7c2	z
Lovecraftových	Lovecraftová	k1gFnPc2	Lovecraftová
děl	dít	k5eAaImAgMnS	dít
tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
sice	sice	k8xC	sice
implikují	implikovat	k5eAaImIp3nP	implikovat
<g/>
,	,	kIx,	,
Lovecraft	Lovecraft	k1gInSc1	Lovecraft
sám	sám	k3xTgInSc1	sám
jej	on	k3xPp3gInSc4	on
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nezastával	zastávat	k5eNaImAgMnS	zastávat
<g/>
.	.	kIx.	.
</s>
<s>
Trpěl	trpět	k5eAaImAgMnS	trpět
nesčetnými	sčetný	k2eNgFnPc7d1	nesčetná
psychosomatickými	psychosomatický	k2eAgFnPc7d1	psychosomatická
obtížemi	obtíž	k1gFnPc7	obtíž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
podepisovaly	podepisovat	k5eAaImAgFnP	podepisovat
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
astenickém	astenický	k2eAgNnSc6d1	astenický
vzezření	vzezření	k1gNnSc6	vzezření
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
byl	být	k5eAaImAgInS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
nepřetržitým	přetržitý	k2eNgFnPc3d1	nepřetržitá
obavám	obava	k1gFnPc3	obava
o	o	k7c4	o
vlastní	vlastní	k2eAgNnSc4d1	vlastní
duševní	duševní	k2eAgNnSc4d1	duševní
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
rodinné	rodinný	k2eAgFnSc3d1	rodinná
zátěži	zátěž	k1gFnSc3	zátěž
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
hysterii	hysterie	k1gFnSc3	hysterie
a	a	k8xC	a
depresím	deprese	k1gFnPc3	deprese
své	svůj	k3xOyFgFnPc4	svůj
matky	matka	k1gFnPc4	matka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
onemocnění	onemocnění	k1gNnSc1	onemocnění
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
příčina	příčina	k1gFnSc1	příčina
jeho	on	k3xPp3gInSc2	on
stavu	stav	k1gInSc2	stav
byla	být	k5eAaImAgFnS	být
organická	organický	k2eAgFnSc1d1	organická
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
Lovecraftova	Lovecraftův	k2eAgNnSc2d1	Lovecraftovo
dětství	dětství	k1gNnSc2	dětství
připravena	připravit	k5eAaPmNgFnS	připravit
ošetřit	ošetřit	k5eAaPmF	ošetřit
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
v	v	k7c6	v
případě	případ	k1gInSc6	případ
projevů	projev	k1gInPc2	projev
vrozené	vrozený	k2eAgFnPc1d1	vrozená
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
zhroucení	zhroucení	k1gNnSc3	zhroucení
v	v	k7c6	v
osmnácti	osmnáct	k4xCc6	osmnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
trpěl	trpět	k5eAaImAgMnS	trpět
i	i	k8xC	i
obličejovými	obličejový	k2eAgInPc7d1	obličejový
tiky	tik	k1gInPc7	tik
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
V	v	k7c6	v
jistém	jistý	k2eAgNnSc6d1	jisté
období	období	k1gNnSc6	období
zase	zase	k9	zase
přechodně	přechodně	k6eAd1	přechodně
téměř	téměř	k6eAd1	téměř
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
sluch	sluch	k1gInSc4	sluch
<g/>
,	,	kIx,	,
stav	stav	k1gInSc4	stav
měl	mít	k5eAaImAgInS	mít
ale	ale	k8xC	ale
krátkého	krátký	k2eAgNnSc2d1	krátké
trvání	trvání	k1gNnSc2	trvání
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jeho	jeho	k3xOp3gFnSc7	jeho
příčinou	příčina	k1gFnSc7	příčina
byla	být	k5eAaImAgFnS	být
zatvrdlá	zatvrdlý	k2eAgFnSc1d1	zatvrdlá
vrstva	vrstva	k1gFnSc1	vrstva
ušního	ušní	k2eAgInSc2d1	ušní
mazu	maz	k1gInSc2	maz
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
horší	zlý	k2eAgInPc1d2	horší
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gFnPc4	jeho
obtíže	obtíž	k1gFnPc4	obtíž
s	s	k7c7	s
termoregulačním	termoregulační	k2eAgInSc7d1	termoregulační
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
odráželo	odrážet	k5eAaImAgNnS	odrážet
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
přecitlivělosti	přecitlivělost	k1gFnSc6	přecitlivělost
na	na	k7c4	na
chlad	chlad	k1gInSc4	chlad
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
své	svůj	k3xOyFgFnSc6	svůj
procházce	procházka	k1gFnSc6	procházka
v	v	k7c6	v
dvacetistupňovém	dvacetistupňový	k2eAgInSc6d1	dvacetistupňový
mrazu	mráz	k1gInSc6	mráz
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dopisech	dopis	k1gInPc6	dopis
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
přiznával	přiznávat	k5eAaImAgMnS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
jednoduchém	jednoduchý	k2eAgInSc6d1	jednoduchý
prvku	prvek	k1gInSc6	prvek
mrazu	mráz	k1gInSc2	mráz
silný	silný	k2eAgInSc4d1	silný
zdroj	zdroj	k1gInSc4	zdroj
inspirace	inspirace	k1gFnSc2	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
Trápily	trápit	k5eAaImAgInP	trápit
jej	on	k3xPp3gNnSc4	on
i	i	k9	i
časté	častý	k2eAgFnPc1d1	častá
noční	noční	k2eAgFnPc1d1	noční
můry	můra	k1gFnPc1	můra
<g/>
,	,	kIx,	,
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
údajně	údajně	k6eAd1	údajně
i	i	k9	i
nočními	noční	k2eAgInPc7d1	noční
děsy	děs	k1gInPc7	děs
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
křehkému	křehký	k2eAgNnSc3d1	křehké
zdraví	zdraví	k1gNnSc3	zdraví
nepřispívala	přispívat	k5eNaImAgFnS	přispívat
ani	ani	k9	ani
obliba	obliba	k1gFnSc1	obliba
v	v	k7c6	v
nezdravém	zdravý	k2eNgNnSc6d1	nezdravé
jídle	jídlo	k1gNnSc6	jídlo
<g/>
;	;	kIx,	;
zbožňoval	zbožňovat	k5eAaImAgInS	zbožňovat
kávu	káva	k1gFnSc4	káva
<g/>
,	,	kIx,	,
čokoládu	čokoláda	k1gFnSc4	čokoláda
<g/>
,	,	kIx,	,
sýry	sýr	k1gInPc4	sýr
<g/>
,	,	kIx,	,
hranolky	hranolek	k1gInPc4	hranolek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pozdější	pozdní	k2eAgFnSc7d2	pozdější
nepříznivou	příznivý	k2eNgFnSc7d1	nepříznivá
finanční	finanční	k2eAgFnSc7d1	finanční
situací	situace	k1gFnSc7	situace
se	se	k3xPyFc4	se
stravoval	stravovat	k5eAaImAgInS	stravovat
nepravidelně	pravidelně	k6eNd1	pravidelně
a	a	k8xC	a
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
jeho	jeho	k3xOp3gFnPc7	jeho
pozdějšími	pozdní	k2eAgFnPc7d2	pozdější
obtížemi	obtíž	k1gFnPc7	obtíž
s	s	k7c7	s
trávením	trávení	k1gNnSc7	trávení
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
rakovinou	rakovina	k1gFnSc7	rakovina
střev	střevo	k1gNnPc2	střevo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Řazeno	řazen	k2eAgNnSc1d1	řazeno
dle	dle	k7c2	dle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
At	At	k1gFnSc1	At
the	the	k?	the
Mountains	Mountains	k1gInSc1	Mountains
of	of	k?	of
Madness	Madness	k1gInSc1	Madness
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
The	The	k1gFnPc2	The
Call	Callum	k1gNnPc2	Callum
of	of	k?	of
Cthulhu	Cthulh	k1gInSc6	Cthulh
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Case	Cas	k1gFnSc2	Cas
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Dexter	Dexter	k1gMnSc1	Dexter
Ward	Ward	k1gMnSc1	Ward
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Dream-Quest	Dream-Quest	k1gMnSc1	Dream-Quest
of	of	k?	of
Unknown	Unknown	k1gMnSc1	Unknown
Kadath	Kadath	k1gMnSc1	Kadath
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Shadow	Shadow	k1gMnSc2	Shadow
Out	Out	k1gMnSc2	Out
of	of	k?	of
Time	Tim	k1gMnSc2	Tim
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Alchemist	Alchemist	k1gMnSc1	Alchemist
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
Azathoth	Azathotha	k1gFnPc2	Azathotha
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Beast	Beast	k1gMnSc1	Beast
in	in	k?	in
the	the	k?	the
Cave	Cave	k1gInSc1	Cave
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
Beyond	Beyond	k1gMnSc1	Beyond
the	the	k?	the
Wall	Wall	k1gMnSc1	Wall
of	of	k?	of
Sleep	Sleep	k1gMnSc1	Sleep
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Book	Book	k1gMnSc1	Book
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
The	The	k1gMnSc2	The
Cats	Cats	k1gInSc1	Cats
of	of	k?	of
Ulthar	Ulthar	k1gInSc1	Ulthar
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Celephaï	Celephaï	k1gFnSc2	Celephaï
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Colour	Colour	k1gMnSc1	Colour
Out	Out	k1gMnSc1	Out
of	of	k?	of
Space	Space	k1gMnSc1	Space
(	(	kIx(	(
<g/>
Barva	barva	k1gFnSc1	barva
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
březen	březen	k1gInSc1	březen
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Povídka	povídka	k1gFnSc1	povídka
byla	být	k5eAaImAgFnS	být
dramatizována	dramatizovat	k5eAaBmNgFnS	dramatizovat
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
rozhlasu	rozhlas	k1gInSc6	rozhlas
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
jako	jako	k8xC	jako
četba	četba	k1gFnSc1	četba
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Balabán	Balabán	k1gMnSc1	Balabán
<g/>
,	,	kIx,	,
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Vlada	Vlad	k1gMnSc2	Vlad
Ruska	Rusko	k1gNnSc2	Rusko
četl	číst	k5eAaImAgMnS	číst
Lukáš	Lukáš	k1gMnSc1	Lukáš
Hlavica	Hlavica	k1gMnSc1	Hlavica
Cool	Cool	k1gMnSc1	Cool
Air	Air	k1gMnSc1	Air
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Dagon	Dagon	k1gInSc1	Dagon
(	(	kIx(	(
<g/>
July	Jula	k1gFnSc2	Jula
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Descendant	Descendant	k1gMnSc1	Descendant
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
The	The	k1gFnPc2	The
Doom	Doomo	k1gNnPc2	Doomo
That	That	k1gMnSc1	That
Came	Cam	k1gFnSc2	Cam
to	ten	k3xDgNnSc4	ten
Sarnath	Sarnath	k1gMnSc1	Sarnath
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Dreams	Dreams	k1gInSc1	Dreams
in	in	k?	in
the	the	k?	the
Witch	Witch	k1gInSc1	Witch
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Dunwich	Dunwich	k1gMnSc1	Dunwich
Horror	horror	k1gInSc1	horror
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Evil	Evil	k1gMnSc1	Evil
Clergyman	Clergyman	k1gMnSc1	Clergyman
(	(	kIx(	(
<g/>
October	October	k1gInSc1	October
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
Ex	ex	k6eAd1	ex
Oblivione	Oblivion	k1gInSc5	Oblivion
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Facts	Facts	k1gInSc1	Facts
Concerning	Concerning	k1gInSc1	Concerning
the	the	k?	the
Late	lat	k1gInSc5	lat
Arthur	Arthur	k1gMnSc1	Arthur
Jermyn	Jermyna	k1gFnPc2	Jermyna
and	and	k?	and
His	his	k1gNnSc4	his
Family	Famila	k1gFnSc2	Famila
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
From	From	k1gMnSc1	From
Beyond	Beyond	k1gMnSc1	Beyond
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
The	The	k1gFnPc4	The
Haunted	Haunted	k1gInSc4	Haunted
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
/	/	kIx~	/
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Haunter	Haunter	k1gMnSc1	Haunter
of	of	k?	of
the	the	k?	the
Dark	Dark	k1gInSc1	Dark
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
He	he	k0	he
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
Herbert	Herbert	k1gMnSc1	Herbert
West	West	k1gMnSc1	West
-	-	kIx~	-
Reanimator	Reanimator	k1gMnSc1	Reanimator
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
Necronomicon	Necronomicon	k1gInSc1	Necronomicon
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Horror	horror	k1gInSc1	horror
at	at	k?	at
Red	Red	k1gMnSc1	Red
Hook	Hook	k1gMnSc1	Hook
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Hound	Hound	k1gMnSc1	Hound
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Hypnos	hypnosa	k1gFnPc2	hypnosa
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Ibid	Ibida	k1gFnPc2	Ibida
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
In	In	k1gMnSc3	In
the	the	k?	the
Vault	Vault	k1gMnSc1	Vault
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
In	In	k1gFnSc1	In
the	the	k?	the
Walls	Walls	k1gInSc1	Walls
of	of	k?	of
Eryx	Eryx	k1gInSc1	Eryx
(	(	kIx(	(
<g/>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Kenneth	Kennetha	k1gFnPc2	Kennetha
Sterling	sterling	k1gInSc1	sterling
<g/>
;	;	kIx,	;
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
the	the	k?	the
Detective	Detectiv	k1gInSc5	Detectiv
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
/	/	kIx~	/
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
Life	Lif	k1gInSc2	Lif
and	and	k?	and
Death	Death	k1gMnSc1	Death
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
?	?	kIx.	?
</s>
<s>
<g/>
;	;	kIx,	;
Ztraceno	ztraceno	k1gNnSc1	ztraceno
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Little	Little	k1gFnSc2	Little
Glass	Glassa	k1gFnPc2	Glassa
Bottle	Bottle	k1gFnSc2	Bottle
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
The	The	k1gFnPc2	The
Lurking	Lurking	k1gInSc1	Lurking
Fear	Fear	k1gMnSc1	Fear
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Memory	Memora	k1gFnSc2	Memora
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Moon-Bog	Moon-Bog	k1gMnSc1	Moon-Bog
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Mound	Mound	k1gMnSc1	Mound
(	(	kIx(	(
<g/>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
se	se	k3xPyFc4	se
Zealia	Zealia	k1gFnSc1	Zealia
Bishop	Bishop	k1gInSc1	Bishop
<g/>
;	;	kIx,	;
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Music	Music	k1gMnSc1	Music
of	of	k?	of
Erich	Erich	k1gMnSc1	Erich
Zann	Zann	k1gMnSc1	Zann
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Mysterious	Mysterious	k1gMnSc1	Mysterious
Ship	Ship	k1gMnSc1	Ship
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Mystery	Myster	k1gInPc1	Myster
of	of	k?	of
the	the	k?	the
Grave-Yard	Grave-Yard	k1gInSc1	Grave-Yard
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
The	The	k1gFnSc2	The
Mystery	Myster	k1gInPc4	Myster
of	of	k?	of
Murdon	Murdon	k1gInSc4	Murdon
Grange	Grang	k1gMnSc4	Grang
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Nameless	Namelessa	k1gFnPc2	Namelessa
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Night	Night	k1gMnSc1	Night
Ocean	Ocean	k1gMnSc1	Ocean
(	(	kIx(	(
<g/>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
R.	R.	kA	R.
H.	H.	kA	H.
Barlow	Barlow	k1gMnSc1	Barlow
<g/>
;	;	kIx,	;
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Noble	Noble	k1gFnSc2	Noble
Eavesdropper	Eavesdropper	k1gMnSc1	Eavesdropper
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Nyarlathotep	Nyarlathotep	k1gMnSc1	Nyarlathotep
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Old	Olda	k1gFnPc2	Olda
Bugs	Bugs	k1gInSc1	Bugs
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Other	Other	k1gMnSc1	Other
Gods	Gods	k1gInSc1	Gods
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Out	Out	k1gFnSc1	Out
of	of	k?	of
the	the	k?	the
Aeons	Aeons	k1gInSc1	Aeons
(	(	kIx(	(
<g/>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Hazel	Hazel	k1gMnSc1	Hazel
Heald	Heald	k1gMnSc1	Heald
<g/>
;	;	kIx,	;
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Outsider	outsider	k1gMnSc1	outsider
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Pickman	Pickman	k1gMnSc1	Pickman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
The	The	k1gMnSc5	The
Picture	Pictur	k1gMnSc5	Pictur
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
The	The	k1gMnSc5	The
Picture	Pictur	k1gMnSc5	Pictur
in	in	k?	in
the	the	k?	the
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Poetry	Poetr	k1gInPc1	Poetr
and	and	k?	and
the	the	k?	the
Gods	Gods	k1gInSc1	Gods
(	(	kIx(	(
<g/>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Anna	Anna	k1gFnSc1	Anna
Helen	Helena	k1gFnPc2	Helena
Crofts	Croftsa	k1gFnPc2	Croftsa
<g/>
;	;	kIx,	;
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Polaris	Polaris	k1gFnSc2	Polaris
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Quest	Quest	k1gMnSc1	Quest
of	of	k?	of
Iranon	Iranon	k1gMnSc1	Iranon
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Rats	Rats	k1gInSc1	Rats
in	in	k?	in
the	the	k?	the
Walls	Walls	k1gInSc1	Walls
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
A	a	k8xC	a
Reminiscence	reminiscence	k1gFnSc1	reminiscence
of	of	k?	of
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Samuel	Samuel	k1gMnSc1	Samuel
Johnson	Johnson	k1gMnSc1	Johnson
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Secret	Secret	k1gMnSc1	Secret
of	of	k?	of
the	the	k?	the
Grave	grave	k1gNnSc4	grave
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
/	/	kIx~	/
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
The	The	k1gFnPc2	The
Secret	Secret	k1gInSc1	Secret
Cave	Cav	k1gInSc2	Cav
or	or	k?	or
John	John	k1gMnSc1	John
Lees	Lees	k1gInSc1	Lees
Adventure	Adventur	k1gMnSc5	Adventur
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Shadow	Shadow	k1gMnSc1	Shadow
Over	Over	k1gMnSc1	Over
Innsmouth	Innsmouth	k1gMnSc1	Innsmouth
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
The	The	k1gFnPc4	The
Shunned	Shunned	k1gInSc4	Shunned
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Silver	Silver	k1gMnSc1	Silver
Key	Key	k1gMnSc1	Key
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Statement	Statement	k1gMnSc1	Statement
of	of	k?	of
Randolph	Randolph	k1gMnSc1	Randolph
Carter	Carter	k1gMnSc1	Carter
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Strange	Strang	k1gFnSc2	Strang
High	Higha	k1gFnPc2	Higha
House	house	k1gNnSc1	house
in	in	k?	in
the	the	k?	the
Mist	Mist	k1gInSc1	Mist
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Street	Street	k1gMnSc1	Street
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Sweet	Sweet	k1gInSc1	Sweet
Ermengarde	Ermengard	k1gMnSc5	Ermengard
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Temple	templ	k1gInSc5	templ
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Terrible	Terrible	k1gFnSc2	Terrible
Old	Olda	k1gFnPc2	Olda
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Thing	Thing	k1gMnSc1	Thing
in	in	k?	in
the	the	k?	the
Moonlight	Moonlight	k1gMnSc1	Moonlight
(	(	kIx(	(
<g/>
Nepravý	nepravý	k1gMnSc1	nepravý
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Thing	Thing	k1gMnSc1	Thing
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Doorstep	Doorstep	k1gInSc1	Doorstep
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
Through	Through	k1gMnSc1	Through
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
of	of	k?	of
the	the	k?	the
<g />
.	.	kIx.	.
</s>
<s>
Silver	Silver	k1gMnSc1	Silver
Key	Key	k1gMnSc1	Key
(	(	kIx(	(
<g/>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
E.	E.	kA	E.
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
Price	Price	k1gMnSc1	Price
<g/>
;	;	kIx,	;
1932	[number]	k4	1932
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Tomb	Tomb	k1gMnSc1	Tomb
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Transition	Transition	k1gInSc1	Transition
of	of	k?	of
Juan	Juan	k1gMnSc1	Juan
Romero	Romero	k1gNnSc1	Romero
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Tree	Tree	k1gFnSc1	Tree
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
The	The	k1gFnSc2	The
Tree	Tre	k1gFnSc2	Tre
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Hill	Hill	k1gMnSc1	Hill
(	(	kIx(	(
<g/>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
Duane	Duan	k1gInSc5	Duan
W.	W.	kA	W.
Rimel	Rimela	k1gFnPc2	Rimela
<g/>
;	;	kIx,	;
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
Under	Under	k1gInSc1	Under
the	the	k?	the
Pyramids	Pyramids	k1gInSc1	Pyramids
(	(	kIx(	(
<g/>
Napsáno	napsat	k5eAaPmNgNnS	napsat
pro	pro	k7c4	pro
Harry	Harr	k1gMnPc4	Harr
Houdini	Houdin	k2eAgMnPc1d1	Houdin
<g/>
;	;	kIx,	;
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Unnamable	Unnamable	k1gMnSc1	Unnamable
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Very	Vera	k1gFnSc2	Vera
Old	Olda	k1gFnPc2	Olda
Folk	folk	k1gInSc1	folk
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
What	What	k1gInSc1	What
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
Brings	Brings	k1gInSc1	Brings
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Whisperer	Whisperer	k1gMnSc1	Whisperer
<g />
.	.	kIx.	.
</s>
<s>
in	in	k?	in
Darkness	Darkness	k1gInSc1	Darkness
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
The	The	k1gMnSc5	The
White	Whit	k1gMnSc5	Whit
Ship	Ship	k1gMnSc1	Ship
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Řazeno	řazen	k2eAgNnSc1d1	řazeno
dle	dle	k7c2	dle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Art	Art	k1gMnSc1	Art
of	of	k?	of
Fusion	Fusion	k1gInSc1	Fusion
<g/>
,	,	kIx,	,
Melting	Melting	k1gInSc1	Melting
<g/>
,	,	kIx,	,
Pudling	Pudling	k1gInSc1	Pudling
&	&	k?	&
Casting	Casting	k1gInSc1	Casting
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
Chemistry	Chemistr	k1gMnPc7	Chemistr
<g/>
;	;	kIx,	;
4	[number]	k4	4
extant	extant	k1gMnSc1	extant
volumes	volumes	k1gMnSc1	volumes
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Good	Good	k1gInSc1	Good
Anaesthetic	Anaesthetice	k1gFnPc2	Anaesthetice
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Scientific	Scientific	k1gMnSc1	Scientific
Gazette	Gazett	k1gInSc5	Gazett
<g/>
;	;	kIx,	;
32	[number]	k4	32
issues	issuesa	k1gFnPc2	issuesa
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
-	-	kIx~	-
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Railroad	Railroad	k1gInSc1	Railroad
Review	Review	k1gFnSc1	Review
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
My	my	k3xPp1nPc1	my
Opinion	Opinion	k1gInSc1	Opinion
as	as	k9	as
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Lunar	Lunar	k1gInSc1	Lunar
Canals	Canals	k1gInSc1	Canals
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
Journal	Journal	k1gFnSc1	Journal
of	of	k?	of
Astronomy	astronom	k1gMnPc7	astronom
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
69	[number]	k4	69
issues	issuesa	k1gFnPc2	issuesa
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
Astronomy	astronom	k1gMnPc4	astronom
<g/>
/	/	kIx~	/
<g/>
The	The	k1gMnSc1	The
Monthly	Monthly	k1gMnSc1	Monthly
Almanack	Almanack	k1gMnSc1	Almanack
<g/>
;	;	kIx,	;
9	[number]	k4	9
issues	issuesa	k1gFnPc2	issuesa
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Planet	planeta	k1gFnPc2	planeta
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
The	The	k1gFnSc2	The
R.	R.	kA	R.
<g/>
I.	I.	kA	I.
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Science	Science	k1gFnSc1	Science
&	&	k?	&
Astronomy	astronom	k1gMnPc7	astronom
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
Annals	Annals	k1gInSc1	Annals
of	of	k?	of
the	the	k?	the
Providence	providence	k1gFnSc2	providence
<g />
.	.	kIx.	.
</s>
<s>
Observatory	Observator	k1gInPc1	Observator
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Providence	providence	k1gFnSc1	providence
Observatory	Observator	k1gInPc1	Observator
<g/>
:	:	kIx,	:
Forecast	Forecast	k1gInSc1	Forecast
for	forum	k1gNnPc2	forum
Providence	providence	k1gFnSc2	providence
&	&	k?	&
Vicinity	Vicinit	k1gInPc1	Vicinit
Next	Next	k1gInSc1	Next
24	[number]	k4	24
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
The	The	k1gFnSc2	The
Science	Science	k1gFnSc2	Science
Library	Librara	k1gFnSc2	Librara
<g/>
;	;	kIx,	;
3	[number]	k4	3
extant	extant	k1gMnSc1	extant
volumes	volumes	k1gMnSc1	volumes
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
No	no	k9	no
Transit	transit	k1gInSc1	transit
of	of	k?	of
Mars	Mars	k1gInSc1	Mars
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
Trans-Neptunian	Trans-Neptunian	k1gInSc1	Trans-Neptunian
Planets	Planets	k1gInSc1	Planets
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Moon	Moon	k1gMnSc1	Moon
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
Astronomy	astronom	k1gMnPc4	astronom
Articles	Articles	k1gMnSc1	Articles
for	forum	k1gNnPc2	forum
the	the	k?	the
Pawtuxet	Pawtuxet	k1gMnSc1	Pawtuxet
Valley	Vallea	k1gFnSc2	Vallea
Gleaner	Gleaner	k1gMnSc1	Gleaner
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
Astronomy	astronom	k1gMnPc4	astronom
Articles	Articles	k1gMnSc1	Articles
for	forum	k1gNnPc2	forum
the	the	k?	the
Providence	providence	k1gFnSc2	providence
Tribune	tribun	k1gMnSc5	tribun
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Earth	Earth	k1gMnSc1	Earth
Not	nota	k1gFnPc2	nota
Hollow	Hollow	k1gMnSc1	Hollow
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
Does	Does	k1gInSc1	Does
"	"	kIx"	"
<g/>
Vulcan	Vulcan	k1gMnSc1	Vulcan
<g/>
"	"	kIx"	"
Exist	Exist	k1gMnSc1	Exist
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
Third	Third	k1gInSc1	Third
Annual	Annual	k1gInSc1	Annual
Report	report	k1gInSc1	report
of	of	k?	of
the	the	k?	the
Prov	Prov	k1gInSc1	Prov
<g/>
.	.	kIx.	.
</s>
<s>
Meteorological	Meteorologicat	k5eAaPmAgInS	Meteorologicat
Station	station	k1gInSc1	station
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
Celestial	Celestial	k1gInSc4	Celestial
Objects	Objects	k1gInSc4	Objects
for	forum	k1gNnPc2	forum
All	All	k1gMnSc2	All
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
Astronomical	Astronomical	k1gFnSc1	Astronomical
Notebook	notebook	k1gInSc1	notebook
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
Venus	Venus	k1gInSc1	Venus
and	and	k?	and
the	the	k?	the
Public	publicum	k1gNnPc2	publicum
Eye	Eye	k1gFnSc1	Eye
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
Astronomy	astronom	k1gMnPc4	astronom
Articles	Articles	k1gMnSc1	Articles
for	forum	k1gNnPc2	forum
the	the	k?	the
Providence	providence	k1gFnSc2	providence
Evening	Evening	k1gInSc1	Evening
News	News	k1gInSc1	News
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
Science	Science	k1gFnSc1	Science
<g />
.	.	kIx.	.
</s>
<s>
versus	versus	k7c1	versus
Charlatanry	Charlatanra	k1gMnSc2	Charlatanra
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Falsity	Falsit	k1gInPc1	Falsit
of	of	k?	of
Astrology	astrolog	k1gMnPc7	astrolog
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Astrology	astrolog	k1gMnPc4	astrolog
and	and	k?	and
the	the	k?	the
Future	Futur	k1gMnSc5	Futur
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Delavan	Delavan	k1gMnSc1	Delavan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Comet	Comet	k1gInSc1	Comet
and	and	k?	and
Astrology	astrolog	k1gMnPc7	astrolog
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
Astrology	astrolog	k1gMnPc7	astrolog
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Isaac	Isaac	k1gFnSc1	Isaac
Bickerstaffe	Bickerstaff	k1gInSc5	Bickerstaff
<g/>
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Reply	Repl	k1gInPc7	Repl
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Mysteries	Mysteries	k1gInSc1	Mysteries
of	of	k?	of
the	the	k?	the
Heavens	Heavens	k1gInSc1	Heavens
Revealed	Revealed	k1gInSc1	Revealed
by	by	kYmCp3nS	by
Astronomy	astronom	k1gMnPc7	astronom
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
Editor	editor	k1gInSc1	editor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Note	Note	k1gFnSc7	Note
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Irish	Irish	k1gMnSc1	Irish
and	and	k?	and
the	the	k?	the
Fairies	Fairies	k1gInSc1	Fairies
<g/>
"	"	kIx"	"
by	by	kYmCp3nS	by
Peter	Peter	k1gMnSc1	Peter
J.	J.	kA	J.
MacManus	MacManus	k1gMnSc1	MacManus
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
Brumalia	Brumalium	k1gNnSc2	Brumalium
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Truth	Truth	k1gMnSc1	Truth
About	About	k1gMnSc1	About
Mars	Mars	k1gMnSc1	Mars
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Cancer	Cancer	k1gMnSc1	Cancer
of	of	k?	of
Superstition	Superstition	k1gInSc1	Superstition
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Some	Som	k1gInSc2	Som
Backgrounds	Backgrounds	k1gInSc1	Backgrounds
of	of	k?	of
Fairyland	Fairyland	k1gInSc1	Fairyland
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
</s>
