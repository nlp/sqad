<s>
Jaroslav	Jaroslav	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
DvořákOsobní	DvořákOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1999	#num#	k4
(	(	kIx(
<g/>
22	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Výška	výška	k1gFnSc1
</s>
<s>
183	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
80	#num#	k4
kg	kg	kA
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Dvořka	Dvořka	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Současný	současný	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
Mountfield	Mountfield	k6eAd1
HK	HK	kA
Číslo	číslo	k1gNnSc1
</s>
<s>
63	#num#	k4
Pozice	pozice	k1gFnSc1
</s>
<s>
útočník	útočník	k1gMnSc1
Předchozí	předchozí	k2eAgInPc4d1
kluby	klub	k1gInPc4
</s>
<s>
Královští	královský	k2eAgMnPc1d1
lvi	lev	k1gMnPc1
Hradec	Hradec	k1gInSc4
Králové	Králové	k2eAgFnSc2d1
</s>
<s>
HC	HC	kA
Stadion	stadion	k1gInSc1
Litoměřice	Litoměřice	k1gInPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
extraliga	extraliga	k1gFnSc1
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
</s>
<s>
Mountfield	Mountfield	k1gMnSc1
HK	HK	kA
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1999	#num#	k4
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
hokejový	hokejový	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
a	a	k8xC
mládežnický	mládežnický	k2eAgMnSc1d1
reprezentant	reprezentant	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
září	září	k1gNnSc2
2016	#num#	k4
nastupující	nastupující	k2eAgFnSc1d1
za	za	k7c7
A-tým	A-té	k1gNnSc7
českého	český	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
Mountfield	Mountfielda	k1gFnPc2
HK	HK	kA
<g/>
.	.	kIx.
</s>
<s>
Hráčská	hráčský	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4
hokejovou	hokejový	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
začal	začít	k5eAaPmAgMnS
v	v	k7c6
klubu	klub	k1gInSc6
HC	HC	kA
VCES	VCES	kA
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
prošel	projít	k5eAaPmAgInS
všemi	všecek	k3xTgFnPc7
mládežnickými	mládežnický	k2eAgFnPc7d1
kategoriemi	kategorie	k1gFnPc7
a	a	k8xC
s	s	k7c7
mladším	mladý	k2eAgInSc7d2
dorostem	dorost	k1gInSc7
získal	získat	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
mistrovské	mistrovský	k2eAgInPc4d1
tituly	titul	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
premiérový	premiérový	k2eAgInSc4d1
start	start	k1gInSc4
za	za	k7c4
"	"	kIx"
<g/>
áčko	áčko	k1gNnSc4
<g/>
"	"	kIx"
v	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
soutěži	soutěž	k1gFnSc6
si	se	k3xPyFc3
připsal	připsat	k5eAaPmAgMnS
27	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
proti	proti	k7c3
Spartě	Sparta	k1gFnSc3
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nadále	nadále	k6eAd1
však	však	k9
nastupoval	nastupovat	k5eAaImAgMnS
i	i	k9
za	za	k7c4
mládež	mládež	k1gFnSc4
Hradce	Hradec	k1gInSc2
a	a	k8xC
kvůli	kvůli	k7c3
většímu	veliký	k2eAgNnSc3d2
hernímu	herní	k2eAgNnSc3d1
vytížení	vytížení	k1gNnSc3
v	v	k7c6
seniorské	seniorský	k2eAgFnSc6d1
kategorii	kategorie	k1gFnSc6
hrál	hrát	k5eAaImAgMnS
rovněž	rovněž	k9
za	za	k7c4
Stadion	stadion	k1gInSc4
Litoměřice	Litoměřice	k1gInPc4
<g/>
,	,	kIx,
prvoligovou	prvoligový	k2eAgFnSc4d1
královéhradeckou	královéhradecký	k2eAgFnSc4d1
farmu	farma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvnímu	první	k4xOgInSc3
týmu	tým	k1gInSc3
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
v	v	k7c6
sezoně	sezona	k1gFnSc6
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
částečně	částečně	k6eAd1
pomohl	pomoct	k5eAaPmAgMnS
v	v	k7c6
lize	liga	k1gFnSc6
k	k	k7c3
historickému	historický	k2eAgInSc3d1
úspěchu	úspěch	k1gInSc3
-	-	kIx~
zisku	zisk	k1gInSc2
bronzové	bronzový	k2eAgFnSc2d1
medaile	medaile	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1
sezony	sezona	k1gFnPc1
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
Královští	královský	k2eAgMnPc1d1
lvi	lev	k1gMnPc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
-	-	kIx~
mládež	mládež	k1gFnSc1
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
Královští	královský	k2eAgMnPc1d1
lvi	lev	k1gMnPc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
-	-	kIx~
mládež	mládež	k1gFnSc1
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Královští	královský	k2eAgMnPc1d1
lvi	lev	k1gMnPc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
-	-	kIx~
mládež	mládež	k1gFnSc1
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
Mountfield	Mountfield	k1gInSc1
HK	HK	kA
-	-	kIx~
mládež	mládež	k1gFnSc1
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
Mountfield	Mountfield	k1gInSc1
HK	HK	kA
-	-	kIx~
mládež	mládež	k1gFnSc1
<g/>
,	,	kIx,
A-tým	A-té	k1gNnSc7
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
extraliga	extraliga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
HC	HC	kA
Stadion	stadion	k1gInSc1
Litoměřice	Litoměřice	k1gInPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
Mountfield	Mountfield	k1gInSc1
HK	HK	kA
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
extraliga	extraliga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
HC	HC	kA
Stadion	stadion	k1gInSc1
Litoměřice	Litoměřice	k1gInPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
Mountfield	Mountfield	k1gInSc1
HK	HK	kA
ELH	ELH	kA
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Turnaj	turnaj	k1gInSc1
</s>
<s>
Místo	místo	k7c2
konání	konání	k1gNnSc2
</s>
<s>
ZGABTM	ZGABTM	kA
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
MS-18	MS-18	k4
2017	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
(	(	kIx(
<g/>
Poprad	Poprad	k1gInSc1
<g/>
,	,	kIx,
Spišská	spišský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Celkem	celkem	k6eAd1
na	na	k7c6
MS-18	MS-18	k1gFnSc6
(	(	kIx(
<g/>
1	#num#	k4
<g/>
x	x	k?
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc4
<g/>
,	,	kIx,
mountfieldhk	mountfieldhk	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
TOP	topit	k5eAaImRp2nS
5	#num#	k4
událostí	událost	k1gFnSc7
uplynulé	uplynulý	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čem	co	k3yQnSc6,k3yRnSc6,k3yInSc6
je	být	k5eAaImIp3nS
ročník	ročník	k1gInSc1
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
nezapomenutelný	zapomenutelný	k2eNgInSc1d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
mountfieldhk	mountfieldhk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
14	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c6
hclitomerice	hclitomerika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c4
mountfieldhk	mountfieldhk	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Statistiky	statistika	k1gFnPc1
hráče	hráč	k1gMnSc2
na	na	k7c4
hokej	hokej	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Statistiky	statistika	k1gFnPc1
hráče	hráč	k1gMnSc2
na	na	k7c4
eliteprospects	eliteprospects	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Statistiky	statistika	k1gFnPc1
hráče	hráč	k1gMnSc2
na	na	k7c4
hockeydb	hockeydb	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
