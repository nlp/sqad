<s>
Velká	velký	k2eAgFnSc1d1	velká
francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
dějin	dějiny	k1gFnPc2	dějiny
Francie	Francie	k1gFnSc2	Francie
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1789	[number]	k4	1789
a	a	k8xC	a
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
svolání	svolání	k1gNnSc2	svolání
generálních	generální	k2eAgInPc2d1	generální
stavů	stav	k1gInPc2	stav
králem	král	k1gMnSc7	král
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
do	do	k7c2	do
uchopení	uchopení	k1gNnSc2	uchopení
moci	moc	k1gFnSc2	moc
Napoleonem	Napoleon	k1gMnSc7	Napoleon
Bonapartem	bonapart	k1gInSc7	bonapart
<g/>
.	.	kIx.	.
</s>
