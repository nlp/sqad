<s>
Delfín	Delfín	k1gMnSc1	Delfín
kapverdský	kapverdský	k2eAgMnSc1d1	kapverdský
<g/>
,	,	kIx,	,
též	též	k9	též
delfín	delfín	k1gMnSc1	delfín
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
<g/>
,	,	kIx,	,
delfínovec	delfínovec	k1gMnSc1	delfínovec
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
nebo	nebo	k8xC	nebo
prodelfín	prodelfín	k1gInSc1	prodelfín
skvrnitý	skvrnitý	k2eAgInSc1d1	skvrnitý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
delfína	delfín	k1gMnSc2	delfín
žijícího	žijící	k2eAgMnSc2d1	žijící
v	v	k7c6	v
Atlantickém	atlantický	k2eAgInSc6d1	atlantický
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
