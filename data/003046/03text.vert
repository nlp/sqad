<s>
Delfín	Delfín	k1gMnSc1	Delfín
kapverdský	kapverdský	k2eAgMnSc1d1	kapverdský
<g/>
,	,	kIx,	,
též	též	k9	též
delfín	delfín	k1gMnSc1	delfín
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
<g/>
,	,	kIx,	,
delfínovec	delfínovec	k1gMnSc1	delfínovec
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
nebo	nebo	k8xC	nebo
prodelfín	prodelfín	k1gInSc1	prodelfín
skvrnitý	skvrnitý	k2eAgInSc1d1	skvrnitý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
delfína	delfín	k1gMnSc2	delfín
žijícího	žijící	k2eAgMnSc2d1	žijící
v	v	k7c6	v
Atlantickém	atlantický	k2eAgInSc6d1	atlantický
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podobného	podobný	k2eAgMnSc2d1	podobný
delfína	delfín	k1gMnSc2	delfín
mexickéh	mexickéh	k1gMnSc1	mexickéh
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
kratším	krátký	k2eAgInSc7d2	kratší
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
plošším	plochý	k2eAgNnSc7d2	plošší
a	a	k8xC	a
silnějším	silný	k2eAgNnSc7d2	silnější
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
světlým	světlý	k2eAgInSc7d1	světlý
pruhem	pruh	k1gInSc7	pruh
táhnoucím	táhnoucí	k2eAgInSc7d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
ramen	rameno	k1gNnPc2	rameno
až	až	k9	až
k	k	k7c3	k
ocasní	ocasní	k2eAgFnSc3d1	ocasní
ploutvi	ploutev	k1gFnSc3	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
delfín	delfín	k1gMnSc1	delfín
kapverdský	kapverdský	k2eAgMnSc1d1	kapverdský
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
až	až	k9	až
140	[number]	k4	140
kg	kg	kA	kg
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
až	až	k9	až
2,5	[number]	k4	2,5
m.	m.	k?	m.
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
beze	beze	k7c2	beze
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
věkem	věk	k1gInSc7	věk
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
na	na	k7c4	na
hřbet	hřbet	k1gInSc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
až	až	k6eAd1	až
patnáctičlenných	patnáctičlenný	k2eAgFnPc6d1	patnáctičlenná
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
sledování	sledování	k1gNnSc6	sledování
sezónně	sezónně	k6eAd1	sezónně
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
potravy	potrava	k1gFnSc2	potrava
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
shlukovat	shlukovat	k5eAaImF	shlukovat
i	i	k9	i
do	do	k7c2	do
větších	veliký	k2eAgNnPc2d2	veliký
stád	stádo	k1gNnPc2	stádo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dravý	dravý	k2eAgMnSc1d1	dravý
<g/>
,	,	kIx,	,
loví	lovit	k5eAaImIp3nP	lovit
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hladiny	hladina	k1gFnSc2	hladina
i	i	k9	i
ze	z	k7c2	z
středních	střední	k2eAgFnPc2d1	střední
hloubek	hloubka	k1gFnPc2	hloubka
<g/>
,	,	kIx,	,
především	především	k9	především
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
hlavonožce	hlavonožec	k1gMnPc4	hlavonožec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
olihně	oliheň	k1gFnPc4	oliheň
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
březí	březí	k2eAgFnSc1d1	březí
14-15	[number]	k4	14-15
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
novorozené	novorozený	k2eAgNnSc1d1	novorozené
mládě	mládě	k1gNnSc1	mládě
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
0,8	[number]	k4	0,8
-	-	kIx~	-
1,2	[number]	k4	1,2
m.	m.	k?	m.
Delfín	Delfín	k1gMnSc1	Delfín
kapverdský	kapverdský	k2eAgMnSc1d1	kapverdský
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Atlantickém	atlantický	k2eAgInSc6d1	atlantický
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Brazílie	Brazílie	k1gFnSc2	Brazílie
k	k	k7c3	k
Nové	Nové	k2eAgFnSc3d1	Nové
Anglii	Anglie	k1gFnSc3	Anglie
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
při	při	k7c6	při
africkém	africký	k2eAgNnSc6d1	africké
pobřeží	pobřeží	k1gNnSc6	pobřeží
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
