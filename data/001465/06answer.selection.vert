<s>
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
heliocentrismus	heliocentrismus	k1gInSc1	heliocentrismus
protikladem	protiklad	k1gInSc7	protiklad
geocentrismu	geocentrismus	k1gInSc2	geocentrismus
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
modernímu	moderní	k2eAgInSc3d1	moderní
geocentrismu	geocentrismus	k1gInSc3	geocentrismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
kladou	klást	k5eAaImIp3nP	klást
jako	jako	k9	jako
centrum	centrum	k1gNnSc4	centrum
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
