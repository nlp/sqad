<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Neustadt	Neustadt	k2eAgInSc1d1	Neustadt
in	in	k?	in
Mähren	Mährno	k1gNnPc2	Mährno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Žďárských	Žďárských	k2eAgInPc2d1	Žďárských
vrchů	vrch	k1gInPc2	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Založeno	založen	k2eAgNnSc1d1	založeno
bylo	být	k5eAaImAgNnS	být
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1250	[number]	k4	1250
Bočkem	bočkem	k6eAd1	bočkem
z	z	k7c2	z
Obřan	Obřana	k1gFnPc2	Obřana
<g/>
,	,	kIx,	,
zakladatelem	zakladatel	k1gMnSc7	zakladatel
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
kláštera	klášter	k1gInSc2	klášter
ve	v	k7c6	v
Žďáře	Žďár	k1gInSc6	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
rozkvětu	rozkvět	k1gInSc2	rozkvět
požívalo	požívat	k5eAaImAgNnS	požívat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
renesance	renesance	k1gFnSc2	renesance
za	za	k7c2	za
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Zachovalé	zachovalý	k2eAgNnSc1d1	zachovalé
historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
městskou	městský	k2eAgFnSc4d1	městská
památkovou	památkový	k2eAgFnSc4d1	památková
zónu	zóna	k1gFnSc4	zóna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
bohatá	bohatý	k2eAgFnSc1d1	bohatá
sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
náměstí	náměstí	k1gNnSc2	náměstí
od	od	k7c2	od
místních	místní	k2eAgMnPc2d1	místní
rodáků	rodák	k1gMnPc2	rodák
Jana	Jan	k1gMnSc2	Jan
Štursy	Štursa	k1gFnSc2	Štursa
a	a	k8xC	a
Vincence	Vincenc	k1gMnSc2	Vincenc
Makovského	Makovský	k1gMnSc2	Makovský
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
památky	památka	k1gFnPc4	památka
patří	patřit	k5eAaImIp3nS	patřit
katolický	katolický	k2eAgInSc1d1	katolický
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
staré	starý	k2eAgFnSc2d1	stará
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
významných	významný	k2eAgInPc2d1	významný
sportovních	sportovní	k2eAgInPc2d1	sportovní
happeningů	happening	k1gInPc2	happening
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
historií	historie	k1gFnSc7	historie
je	být	k5eAaImIp3nS	být
závod	závod	k1gInSc1	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
běžeckém	běžecký	k2eAgNnSc6d1	běžecké
lyžování	lyžování	k1gNnSc6	lyžování
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
lyže	lyže	k1gFnSc1	lyže
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
místo	místo	k1gNnSc4	místo
přebral	přebrat	k5eAaPmAgInS	přebrat
každoročně	každoročně	k6eAd1	každoročně
konaný	konaný	k2eAgInSc1d1	konaný
závod	závod	k1gInSc1	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konalo	konat	k5eAaImAgNnS	konat
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
i	i	k9	i
závod	závod	k1gInSc4	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
horských	horský	k2eAgNnPc2d1	horské
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
příznivce	příznivec	k1gMnPc4	příznivec
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
<g/>
,	,	kIx,	,
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
pak	pak	k6eAd1	pak
nabízí	nabízet	k5eAaImIp3nS	nabízet
nespočet	nespočet	k1gInSc1	nespočet
možností	možnost	k1gFnPc2	možnost
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
turisty	turist	k1gMnPc4	turist
i	i	k8xC	i
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
sportovec	sportovec	k1gMnSc1	sportovec
ocení	ocenit	k5eAaPmIp3nS	ocenit
především	především	k6eAd1	především
nový	nový	k2eAgInSc4d1	nový
areál	areál	k1gInSc4	areál
Vysočina	vysočina	k1gFnSc1	vysočina
arena	arena	k1gFnSc1	arena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
výše	výše	k1gFnPc1	výše
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
akce	akce	k1gFnSc2	akce
úspěšně	úspěšně	k6eAd1	úspěšně
konají	konat	k5eAaImIp3nP	konat
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
spjata	spjat	k2eAgFnSc1d1	spjata
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
žďárského	žďárský	k2eAgInSc2d1	žďárský
kláštera	klášter	k1gInSc2	klášter
založeného	založený	k2eAgInSc2d1	založený
Bočkem	boček	k1gInSc7	boček
z	z	k7c2	z
Obřan	Obřana	k1gFnPc2	Obřana
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
česko-moravské	českooravský	k2eAgFnSc2d1	česko-moravská
hranice	hranice	k1gFnSc2	hranice
pokryté	pokrytý	k2eAgNnSc1d1	pokryté
pomezním	pomezní	k2eAgInSc7d1	pomezní
pralesem	prales	k1gInSc7	prales
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1267	[number]	k4	1267
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
osada	osada	k1gFnSc1	osada
uváděna	uvádět	k5eAaImNgFnS	uvádět
ještě	ještě	k6eAd1	ještě
jako	jako	k8xC	jako
Bočkonov	Bočkonov	k1gInSc1	Bočkonov
(	(	kIx(	(
<g/>
Bočkov	Bočkov	k1gInSc1	Bočkov
<g/>
)	)	kIx)	)
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
potvrzující	potvrzující	k2eAgInSc1d1	potvrzující
Bočkův	Bočkův	k2eAgInSc1d1	Bočkův
odkaz	odkaz	k1gInSc1	odkaz
klášteru	klášter	k1gInSc3	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Nova	nova	k1gFnSc1	nova
Civitas	Civitas	k1gInSc1	Civitas
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
listině	listina	k1gFnSc6	listina
krále	král	k1gMnSc4	král
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1293	[number]	k4	1293
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
uvedeno	uvést	k5eAaPmNgNnS	uvést
jako	jako	k9	jako
městečko	městečko	k1gNnSc1	městečko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1312	[number]	k4	1312
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Smila	Smil	k1gMnSc2	Smil
z	z	k7c2	z
Obřan	Obřana	k1gFnPc2	Obřana
<g/>
,	,	kIx,	,
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
Jindřichu	Jindřich	k1gMnSc6	Jindřich
z	z	k7c2	z
Lipé	Lipé	k1gNnSc2	Lipé
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
vybudovat	vybudovat	k5eAaPmF	vybudovat
tvrz	tvrz	k1gFnSc4	tvrz
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bývala	bývat	k5eAaImAgFnS	bývat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
rozlehlost	rozlehlost	k1gFnSc1	rozlehlost
nazývána	nazývat	k5eAaImNgFnS	nazývat
hradem	hrad	k1gInSc7	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1496	[number]	k4	1496
patřilo	patřit	k5eAaImAgNnS	patřit
panství	panství	k1gNnSc1	panství
Pernštejnům	Pernštejn	k1gInPc3	Pernštejn
<g/>
,	,	kIx,	,
za	za	k7c2	za
jejichž	jejichž	k3xOyRp3gFnSc2	jejichž
doby	doba	k1gFnSc2	doba
zažívalo	zažívat	k5eAaImAgNnS	zažívat
město	město	k1gNnSc4	město
výrazný	výrazný	k2eAgInSc1d1	výrazný
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
rozmach	rozmach	k1gInSc1	rozmach
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
za	za	k7c4	za
Vratislava	Vratislav	k1gMnSc4	Vratislav
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
(	(	kIx(	(
<g/>
1561	[number]	k4	1561
<g/>
-	-	kIx~	-
<g/>
1582	[number]	k4	1582
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
bylo	být	k5eAaImAgNnS	být
panství	panství	k1gNnSc1	panství
prodáno	prodán	k2eAgNnSc1d1	prodáno
Janem	Jan	k1gMnSc7	Jan
a	a	k8xC	a
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
Vilému	Vilém	k1gMnSc3	Vilém
Dubskému	Dubský	k1gMnSc3	Dubský
z	z	k7c2	z
Třebomyslic	Třebomyslice	k1gFnPc2	Třebomyslice
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nechal	nechat	k5eAaPmAgMnS	nechat
pustou	pustý	k2eAgFnSc4d1	pustá
tvrz	tvrz	k1gFnSc4	tvrz
zbořit	zbořit	k5eAaPmF	zbořit
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
renezanční	renezanční	k2eAgInSc4d1	renezanční
zámeček	zámeček	k1gInSc4	zámeček
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
usídlil	usídlit	k5eAaPmAgMnS	usídlit
<g/>
,	,	kIx,	,
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
vrchnost	vrchnost	k1gFnSc4	vrchnost
opět	opět	k6eAd1	opět
sídlila	sídlit	k5eAaImAgFnS	sídlit
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
bylo	být	k5eAaImAgNnS	být
panství	panství	k1gNnSc1	panství
zkonfiskováno	zkonfiskovat	k5eAaPmNgNnS	zkonfiskovat
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
majitelem	majitel	k1gMnSc7	majitel
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1624	[number]	k4	1624
stal	stát	k5eAaPmAgMnS	stát
kardinál	kardinál	k1gMnSc1	kardinál
František	František	k1gMnSc1	František
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rovněž	rovněž	k9	rovněž
sousední	sousední	k2eAgNnSc1d1	sousední
klášterní	klášterní	k2eAgNnSc1d1	klášterní
panství	panství	k1gNnSc1	panství
žďárské	žďárský	k2eAgNnSc1d1	žďárské
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byla	být	k5eAaImAgFnS	být
nyní	nyní	k6eAd1	nyní
vykonávána	vykonáván	k2eAgFnSc1d1	vykonávána
správa	správa	k1gFnSc1	správa
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gNnSc2	jeho
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
bylo	být	k5eAaImAgNnS	být
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
povýšeno	povýšen	k2eAgNnSc1d1	povýšeno
na	na	k7c4	na
město	město	k1gNnSc4	město
(	(	kIx(	(
<g/>
1635	[number]	k4	1635
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
byl	být	k5eAaImAgMnS	být
městu	město	k1gNnSc3	město
obnoven	obnoven	k2eAgInSc4d1	obnoven
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1638	[number]	k4	1638
změnli	změnnout	k5eAaImAgMnP	změnnout
panství	panství	k1gNnSc4	panství
majitele	majitel	k1gMnSc2	majitel
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
stává	stávat	k5eAaImIp3nS	stávat
Šimon	Šimon	k1gMnSc1	Šimon
Kratzer	Kratzer	k1gMnSc1	Kratzer
ze	z	k7c2	z
Schönsperka	Schönsperka	k1gFnSc1	Schönsperka
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
hospodářský	hospodářský	k2eAgMnSc1d1	hospodářský
správce	správce	k1gMnSc1	správce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podporoval	podporovat	k5eAaImAgInS	podporovat
sklářskou	sklářský	k2eAgFnSc4d1	sklářská
výrobu	výroba	k1gFnSc4	výroba
v	v	k7c6	v
panství	panství	k1gNnSc6	panství
<g/>
,	,	kIx,	,
zamýšlel	zamýšlet	k5eAaImAgInS	zamýšlet
vybudovat	vybudovat	k5eAaPmF	vybudovat
také	také	k9	také
železářské	železářský	k2eAgFnPc4d1	železářská
hutě	huť	k1gFnPc4	huť
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
záměru	záměr	k1gInSc3	záměr
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
byl	být	k5eAaImAgInS	být
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
Švédy	Švéd	k1gMnPc7	Švéd
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc1	panství
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
spravoval	spravovat	k5eAaImAgInS	spravovat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1660	[number]	k4	1660
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
František	František	k1gMnSc1	František
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Kratzer	Kratzer	k1gMnSc1	Kratzer
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
záměr	záměr	k1gInSc4	záměr
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1651	[number]	k4	1651
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c6	na
Kadově	Kadov	k1gInSc6	Kadov
dvě	dva	k4xCgFnPc1	dva
železářské	železářský	k2eAgFnPc1d1	železářská
vysoké	vysoká	k1gFnPc1	vysoká
pece	pec	k1gFnSc2	pec
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
výrobou	výroba	k1gFnSc7	výroba
navazující	navazující	k2eAgInPc1d1	navazující
hamry	hamr	k1gInPc1	hamr
na	na	k7c6	na
Kuklíku	Kuklík	k1gInSc6	Kuklík
<g/>
,	,	kIx,	,
Vříšti	Vříšti	k1gFnSc6	Vříšti
a	a	k8xC	a
Líšné	Líšná	k1gFnSc6	Líšná
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
položil	položit	k5eAaPmAgInS	položit
základ	základ	k1gInSc4	základ
železářské	železářský	k2eAgFnSc2d1	železářská
výroby	výroba	k1gFnSc2	výroba
na	na	k7c4	na
Novoměstsku	Novoměstska	k1gFnSc4	Novoměstska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1691	[number]	k4	1691
na	na	k7c6	na
základě	základ	k1gInSc6	základ
soudního	soudní	k2eAgInSc2d1	soudní
výnosu	výnos	k1gInSc2	výnos
připadlo	připadnout	k5eAaPmAgNnS	připadnout
panství	panství	k1gNnSc1	panství
knížeti	kníže	k1gMnSc6	kníže
Ferdinandu	Ferdinand	k1gMnSc6	Ferdinand
Ditrichštejnovi	Ditrichštejn	k1gMnSc6	Ditrichštejn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
8	[number]	k4	8
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
prodal	prodat	k5eAaPmAgMnS	prodat
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Leopold	Leopold	k1gMnSc1	Leopold
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
světské	světský	k2eAgNnSc1d1	světské
nadaci	nadace	k1gFnSc3	nadace
šlechtičen	šlechtična	k1gFnPc2	šlechtična
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
patřil	patřit	k5eAaImAgInS	patřit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Nadačního	nadační	k2eAgInSc2d1	nadační
ústavu	ústav	k1gInSc2	ústav
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
mnoho	mnoho	k4c1	mnoho
šlechtičen	šlechtična	k1gFnPc2	šlechtična
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
jejich	jejich	k3xOp3gFnSc2	jejich
nadvlády	nadvláda	k1gFnSc2	nadvláda
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
na	na	k7c6	na
novoměstském	novoměstský	k2eAgNnSc6d1	Novoměstské
panství	panství	k1gNnSc6	panství
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tzv.	tzv.	kA	tzv.
pozdní	pozdní	k2eAgFnSc1d1	pozdní
horská	horský	k2eAgFnSc1d1	horská
kolonizace	kolonizace	k1gFnSc1	kolonizace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nejmladší	mladý	k2eAgFnPc1d3	nejmladší
vesnice	vesnice	k1gFnPc1	vesnice
jako	jako	k8xC	jako
např.	např.	kA	např.
Blatiny	Blatin	k1gInPc1	Blatin
<g/>
,	,	kIx,	,
Koníkov	Koníkov	k1gInSc1	Koníkov
<g/>
,	,	kIx,	,
Samotín	Samotín	k1gInSc1	Samotín
<g/>
,	,	kIx,	,
Krátká	krátké	k1gNnPc1	krátké
a	a	k8xC	a
Moravské	moravský	k2eAgInPc1d1	moravský
Milovy	Milov	k1gInPc1	Milov
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
hraběnky	hraběnka	k1gFnSc2	hraběnka
Hohenzollernové	Hohenzollern	k1gMnPc1	Hohenzollern
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
-	-	kIx~	-
<g/>
1745	[number]	k4	1745
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
nově	nově	k6eAd1	nově
přestavěn	přestavěn	k2eAgInSc4d1	přestavěn
katolický	katolický	k2eAgInSc4d1	katolický
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
nástupkyně	nástupkyně	k1gFnSc1	nástupkyně
baronka	baronka	k1gFnSc1	baronka
Miniati	Miniat	k2eAgMnPc1d1	Miniat
di	di	k?	di
Campoli	Campole	k1gFnSc6	Campole
(	(	kIx(	(
<g/>
1746	[number]	k4	1746
<g/>
-	-	kIx~	-
<g/>
1759	[number]	k4	1759
<g/>
)	)	kIx)	)
barokně	barokně	k6eAd1	barokně
přestavěla	přestavět	k5eAaPmAgFnS	přestavět
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
hřbitovní	hřbitovní	k2eAgInSc4d1	hřbitovní
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Zrušením	zrušení	k1gNnSc7	zrušení
poddanství	poddanství	k1gNnSc2	poddanství
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
-	-	kIx~	-
za	za	k7c2	za
baronky	baronka	k1gFnSc2	baronka
Skrbenské	Skrbenský	k2eAgFnSc2d1	Skrbenská
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
-	-	kIx~	-
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
vrchnost	vrchnost	k1gFnSc1	vrchnost
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1723	[number]	k4	1723
město	město	k1gNnSc1	město
zachvátil	zachvátit	k5eAaPmAgInS	zachvátit
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
zde	zde	k6eAd1	zde
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
podružská	podružský	k2eAgFnSc1d1	podružská
rebelie	rebelie	k1gFnSc1	rebelie
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1796	[number]	k4	1796
<g/>
-	-	kIx~	-
<g/>
1797	[number]	k4	1797
helvetská	helvetský	k2eAgFnSc1d1	helvetská
rebelie	rebelie	k1gFnSc1	rebelie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
okresního	okresní	k2eAgNnSc2d1	okresní
hejtmanství	hejtmanství	k1gNnSc2	hejtmanství
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
soudních	soudní	k2eAgInPc2d1	soudní
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
má	mít	k5eAaImIp3nS	mít
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
přívlastek	přívlastek	k1gInSc1	přívlastek
"	"	kIx"	"
<g/>
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
Nových	Nová	k1gFnPc2	Nová
Měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
bylo	být	k5eAaImAgNnS	být
sídlo	sídlo	k1gNnSc1	sídlo
okresu	okres	k1gInSc2	okres
přeneseno	přenést	k5eAaPmNgNnS	přenést
do	do	k7c2	do
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnPc1d2	novější
dějiny	dějiny	k1gFnPc1	dějiny
města	město	k1gNnSc2	město
výrazně	výrazně	k6eAd1	výrazně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
založení	založení	k1gNnSc1	založení
reálného	reálný	k2eAgNnSc2d1	reálné
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
vybudování	vybudování	k1gNnSc4	vybudování
železnice	železnice	k1gFnSc2	železnice
z	z	k7c2	z
Tišnova	Tišnov	k1gInSc2	Tišnov
do	do	k7c2	do
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc1	rozvoj
lyžování	lyžování	k1gNnSc2	lyžování
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
související	související	k2eAgFnSc1d1	související
výroba	výroba	k1gFnSc1	výroba
lyží	lyže	k1gFnPc2	lyže
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
tak	tak	k9	tak
výroba	výroba	k1gFnSc1	výroba
chirurgických	chirurgický	k2eAgInPc2d1	chirurgický
nástrojů	nástroj	k1gInPc2	nástroj
zahájená	zahájený	k2eAgFnSc1d1	zahájená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Osada	osada	k1gFnSc1	osada
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
městečko	městečko	k1gNnSc1	městečko
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
nad	nad	k7c7	nad
soutokem	soutok	k1gInSc7	soutok
říček	říčka	k1gFnPc2	říčka
Bezděčka	Bezděčko	k1gNnSc2	Bezděčko
a	a	k8xC	a
Bobrůvka	Bobrůvka	k1gFnSc1	Bobrůvka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
asi	asi	k9	asi
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Vratislavova	Vratislavův	k2eAgNnSc2d1	Vratislavovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
zástavba	zástavba	k1gFnSc1	zástavba
omezovala	omezovat	k5eAaImAgFnS	omezovat
jen	jen	k9	jen
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
;	;	kIx,	;
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Vratislavovo	Vratislavův	k2eAgNnSc4d1	Vratislavovo
<g/>
,	,	kIx,	,
Komenského	Komenského	k2eAgNnSc4d1	Komenského
a	a	k8xC	a
Palackého	Palackého	k2eAgNnSc4d1	Palackého
náměstí	náměstí	k1gNnSc4	náměstí
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
spojena	spojit	k5eAaPmNgFnS	spojit
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
náměstí	náměstí	k1gNnSc2	náměstí
vybíhaly	vybíhat	k5eAaImAgFnP	vybíhat
cesty	cesta	k1gFnPc1	cesta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
určovaly	určovat	k5eAaImAgFnP	určovat
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
na	na	k7c4	na
Bobrovou	bobrový	k2eAgFnSc4d1	Bobrová
vedla	vést	k5eAaImAgFnS	vést
Bobrovská	Bobrovský	k2eAgFnSc1d1	Bobrovská
(	(	kIx(	(
<g/>
Nečasova	Nečasův	k2eAgFnSc1d1	Nečasova
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
severozápadním	severozápadní	k2eAgMnSc7d1	severozápadní
na	na	k7c4	na
Žďár	Žďár	k1gInSc4	Žďár
Žďárská	žďárský	k2eAgFnSc1d1	žďárská
ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrem	směr	k1gInSc7	směr
vedla	vést	k5eAaImAgFnS	vést
ulice	ulice	k1gFnSc2	ulice
Nové	Nové	k2eAgInPc1d1	Nové
Domy	dům	k1gInPc1	dům
(	(	kIx(	(
<g/>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
cesta	cesta	k1gFnSc1	cesta
dělila	dělit	k5eAaImAgFnS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
větve	větev	k1gFnPc4	větev
na	na	k7c4	na
Svratku	Svratka	k1gFnSc4	Svratka
a	a	k8xC	a
na	na	k7c4	na
Jimramov	Jimramov	k1gInSc4	Jimramov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
přestavbou	přestavba	k1gFnSc7	přestavba
měšťanského	měšťanský	k2eAgInSc2d1	měšťanský
domu	dům	k1gInSc2	dům
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
náměstí	náměstí	k1gNnSc2	náměstí
vybudována	vybudován	k2eAgFnSc1d1	vybudována
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
zde	zde	k6eAd1	zde
blok	blok	k1gInSc1	blok
domů	domů	k6eAd1	domů
oddělil	oddělit	k5eAaPmAgInS	oddělit
tzv.	tzv.	kA	tzv.
Dolní	dolní	k2eAgInSc1d1	dolní
(	(	kIx(	(
<g/>
Palackého	Palackého	k2eAgNnSc1d1	Palackého
<g/>
)	)	kIx)	)
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
stavebním	stavební	k2eAgMnPc3d1	stavební
zásahům	zásah	k1gInPc3	zásah
do	do	k7c2	do
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
podoby	podoba	k1gFnSc2	podoba
městečka	městečko	k1gNnSc2	městečko
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
novoměstského	novoměstský	k2eAgNnSc2d1	Novoměstské
panství	panství	k1gNnSc2	panství
Vilém	Vilém	k1gMnSc1	Vilém
Dubský	Dubský	k1gMnSc1	Dubský
z	z	k7c2	z
Třebomyslic	Třebomyslice	k1gFnPc2	Třebomyslice
nejenže	nejenže	k6eAd1	nejenže
dal	dát	k5eAaPmAgMnS	dát
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
značně	značně	k6eAd1	značně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
panské	panský	k2eAgNnSc4d1	panské
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
renesanční	renesanční	k2eAgInSc1d1	renesanční
zámek	zámek	k1gInSc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postavení	postavení	k1gNnSc3	postavení
panského	panský	k2eAgInSc2d1	panský
pivovaru	pivovar	k1gInSc2	pivovar
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
tzv.	tzv.	kA	tzv.
Hrádku	Hrádok	k1gInSc6	Hrádok
(	(	kIx(	(
<g/>
Halina	Halina	k1gFnSc1	Halina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sídla	sídlo	k1gNnSc2	sídlo
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Zahrádecké	Zahrádecký	k2eAgFnSc2d1	Zahrádecká
ze	z	k7c2	z
Zahrádek	zahrádka	k1gFnPc2	zahrádka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
oddělil	oddělit	k5eAaPmAgInS	oddělit
prostor	prostor	k1gInSc1	prostor
dnešního	dnešní	k2eAgMnSc2d1	dnešní
Komenského	Komenský	k1gMnSc2	Komenský
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
odbočka	odbočka	k1gFnSc1	odbočka
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
Nové	Nové	k2eAgInPc7d1	Nové
Domy	dům	k1gInPc7	dům
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
východní	východní	k2eAgInSc4d1	východní
konec	konec	k1gInSc4	konec
Malé	Malé	k2eAgFnSc2d1	Malé
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
kostelíku	kostelík	k1gInSc3	kostelík
Nanabevzetí	Nanabevzetí	k1gNnSc2	Nanabevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenější	vzdálený	k2eAgFnSc1d2	vzdálenější
část	část	k1gFnSc1	část
Malé	Malé	k2eAgFnSc2d1	Malé
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
Pod	pod	k7c7	pod
kostelíčkem	kostelíček	k1gInSc7	kostelíček
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pak	pak	k6eAd1	pak
až	až	k6eAd1	až
začátkem	začátkem	k7c2	začátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
téhož	týž	k3xTgNnSc2	týž
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
o	o	k7c6	o
ulici	ulice	k1gFnSc6	ulice
Svatojánskou	svatojánský	k2eAgFnSc7d1	Svatojánská
(	(	kIx(	(
<g/>
Podstrání	podstrání	k1gNnSc1	podstrání
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
a	a	k8xC	a
Jánská	jánský	k2eAgFnSc1d1	Jánská
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
stavební	stavební	k2eAgInSc1d1	stavební
vývoj	vývoj	k1gInSc1	vývoj
výrazně	výrazně	k6eAd1	výrazně
poznamenaly	poznamenat	k5eAaPmAgFnP	poznamenat
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
ničivé	ničivý	k2eAgInPc4d1	ničivý
požáry	požár	k1gInPc4	požár
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1723	[number]	k4	1723
<g/>
,	,	kIx,	,
zničil	zničit	k5eAaPmAgMnS	zničit
domy	dům	k1gInPc4	dům
zejména	zejména	k9	zejména
na	na	k7c6	na
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
straně	strana	k1gFnSc6	strana
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
208	[number]	k4	208
domů	dům	k1gInPc2	dům
včetně	včetně	k7c2	včetně
kostela	kostel	k1gInSc2	kostel
převládla	převládnout	k5eAaPmAgFnS	převládnout
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
zděná	zděný	k2eAgFnSc1d1	zděná
výstavba	výstavba	k1gFnSc1	výstavba
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
nadále	nadále	k6eAd1	nadále
soustřeďovala	soustřeďovat	k5eAaImAgFnS	soustřeďovat
především	především	k9	především
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
třetině	třetina	k1gFnSc6	třetina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
začalo	začít	k5eAaPmAgNnS	začít
rychle	rychle	k6eAd1	rychle
zvětšovat	zvětšovat	k5eAaImF	zvětšovat
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
výstavba	výstavba	k1gFnSc1	výstavba
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c6	na
území	území	k1gNnSc6	území
zvané	zvaný	k2eAgFnPc1d1	zvaná
Niva	niva	k1gFnSc1	niva
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
vyplňovalo	vyplňovat	k5eAaImAgNnS	vyplňovat
plochu	plocha	k1gFnSc4	plocha
mezi	mezi	k7c7	mezi
dosavadním	dosavadní	k2eAgNnSc7d1	dosavadní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
Malou	malý	k2eAgFnSc7d1	malá
a	a	k8xC	a
Žďárskou	žďárský	k2eAgFnSc7d1	žďárská
ulicí	ulice	k1gFnSc7	ulice
a	a	k8xC	a
oběma	dva	k4xCgInPc7	dva
rybníky	rybník	k1gInPc7	rybník
(	(	kIx(	(
<g/>
Cihelským	Cihelský	k2eAgInPc3d1	Cihelský
a	a	k8xC	a
Klečkovským	Klečkovský	k2eAgInPc3d1	Klečkovský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
město	město	k1gNnSc1	město
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
i	i	k9	i
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Novými	nový	k2eAgInPc7d1	nový
Domy	dům	k1gInPc7	dům
je	být	k5eAaImIp3nS	být
zastavována	zastavován	k2eAgFnSc1d1	zastavována
východní	východní	k2eAgFnSc1d1	východní
strana	strana	k1gFnSc1	strana
silnice	silnice	k1gFnSc2	silnice
na	na	k7c4	na
Jimramov	Jimramov	k1gInSc4	Jimramov
<g/>
,	,	kIx,	,
čtvrti	čtvrt	k1gFnPc1	čtvrt
zde	zde	k6eAd1	zde
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
Hejkalov	Hejkalov	k1gInSc1	Hejkalov
<g/>
;	;	kIx,	;
staví	stavit	k5eAaPmIp3nS	stavit
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
vedoucí	vedoucí	k2eAgFnSc6d1	vedoucí
od	od	k7c2	od
sokolovny	sokolovna	k1gFnSc2	sokolovna
k	k	k7c3	k
nádraží	nádraží	k1gNnSc3	nádraží
(	(	kIx(	(
<g/>
Tyršova	Tyršův	k2eAgFnSc1d1	Tyršova
a	a	k8xC	a
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
u	u	k7c2	u
viaduktu	viadukt	k1gInSc2	viadukt
(	(	kIx(	(
<g/>
lokalitě	lokalita	k1gFnSc3	lokalita
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
Šanghaj	Šanghaj	k1gFnSc4	Šanghaj
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
ulice	ulice	k1gFnSc1	ulice
Mírová	mírový	k2eAgFnSc1d1	mírová
a	a	k8xC	a
Výhledy	výhled	k1gInPc7	výhled
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
domů	dům	k1gInPc2	dům
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
za	za	k7c7	za
nádražím	nádraží	k1gNnSc7	nádraží
(	(	kIx(	(
<g/>
Nezvalova	Nezvalův	k2eAgFnSc1d1	Nezvalova
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInPc1	první
domy	dům	k1gInPc1	dům
vznikají	vznikat	k5eAaImIp3nP	vznikat
nad	nad	k7c7	nad
Kazmírovým	Kazmírův	k2eAgInSc7d1	Kazmírův
rybníkem	rybník	k1gInSc7	rybník
(	(	kIx(	(
<g/>
Německého	německý	k2eAgInSc2d1	německý
ulice	ulice	k1gFnPc1	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
žďárské	žďárský	k2eAgFnSc2d1	žďárská
silnice	silnice	k1gFnSc2	silnice
vybudován	vybudován	k2eAgInSc1d1	vybudován
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
areál	areál	k1gInSc1	areál
okresní	okresní	k2eAgFnSc2d1	okresní
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
naproti	naproti	k6eAd1	naproti
přes	přes	k7c4	přes
silnici	silnice	k1gFnSc4	silnice
pak	pak	k6eAd1	pak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
první	první	k4xOgNnSc1	první
novoměstské	novoměstský	k2eAgNnSc1d1	Novoměstské
sídliště	sídliště	k1gNnSc1	sídliště
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Mohutný	mohutný	k2eAgInSc4d1	mohutný
stavební	stavební	k2eAgInSc4d1	stavební
rozvoj	rozvoj	k1gInSc4	rozvoj
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
město	město	k1gNnSc1	město
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
při	při	k7c6	při
silnici	silnice	k1gFnSc6	silnice
na	na	k7c4	na
Vlachovice	Vlachovice	k1gFnPc4	Vlachovice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postaven	postaven	k2eAgInSc1d1	postaven
podnik	podnik	k1gInSc1	podnik
Chirana	Chirana	k1gFnSc1	Chirana
(	(	kIx(	(
<g/>
Medin	Medina	k1gFnPc2	Medina
<g/>
)	)	kIx)	)
výrobce	výrobce	k1gMnSc1	výrobce
chirurgických	chirurgický	k2eAgMnPc2d1	chirurgický
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zahájil	zahájit	k5eAaPmAgMnS	zahájit
svůj	svůj	k3xOyFgInSc4	svůj
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Slonkovy	Slonkův	k2eAgFnSc2d1	Slonkova
firmy	firma	k1gFnSc2	firma
situované	situovaný	k2eAgFnPc1d1	situovaná
východně	východně	k6eAd1	východně
od	od	k7c2	od
silnice	silnice	k1gFnSc2	silnice
na	na	k7c6	na
Pohledec	Pohledec	k1gMnSc1	Pohledec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
podnik	podnik	k1gInSc1	podnik
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
lyží	lyže	k1gFnPc2	lyže
Sport	sport	k1gInSc1	sport
(	(	kIx(	(
<g/>
Sporten	Sporten	k2eAgMnSc1d1	Sporten
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
o	o	k7c4	o
20	[number]	k4	20
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
nový	nový	k2eAgInSc1d1	nový
provoz	provoz	k1gInSc1	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Bytovou	bytový	k2eAgFnSc4d1	bytová
výstavbu	výstavba	k1gFnSc4	výstavba
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
způsobem	způsob	k1gInSc7	způsob
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
těžba	těžba	k1gFnSc1	těžba
uranu	uran	k1gInSc2	uran
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Dolní	dolní	k2eAgFnSc2d1	dolní
Rožínky	Rožínka	k1gFnSc2	Rožínka
<g/>
.	.	kIx.	.
</s>
<s>
Panelová	panelový	k2eAgFnSc1d1	panelová
sídlištní	sídlištní	k2eAgFnSc1d1	sídlištní
výstavba	výstavba	k1gFnSc1	výstavba
určená	určený	k2eAgFnSc1d1	určená
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
její	její	k3xOp3gMnPc4	její
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
několika	několik	k4yIc6	několik
vlnách	vlna	k1gFnPc6	vlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
sídliště	sídliště	k1gNnSc4	sídliště
U	u	k7c2	u
nádraží	nádraží	k1gNnSc2	nádraží
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Staré	Staré	k2eAgNnSc1d1	Staré
sídliště	sídliště	k1gNnSc1	sídliště
<g/>
)	)	kIx)	)
a	a	k8xC	a
stavělo	stavět	k5eAaImAgNnS	stavět
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
Tyršově	Tyršův	k2eAgFnSc6d1	Tyršova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
i	i	k9	i
tři	tři	k4xCgInPc1	tři
věžové	věžový	k2eAgInPc1d1	věžový
domy	dům	k1gInPc1	dům
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
jejich	jejich	k3xOp3gFnSc2	jejich
fasády	fasáda	k1gFnSc2	fasáda
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
dekádě	dekáda	k1gFnSc6	dekáda
pak	pak	k6eAd1	pak
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
sídliště	sídliště	k1gNnSc4	sídliště
na	na	k7c6	na
Hornické	hornický	k2eAgFnSc6d1	hornická
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
další	další	k2eAgFnSc3d1	další
panelové	panelový	k2eAgFnSc3d1	panelová
výstavbě	výstavba	k1gFnSc3	výstavba
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zbourána	zbourán	k2eAgFnSc1d1	zbourána
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
východní	východní	k2eAgFnSc1d1	východní
strana	strana	k1gFnSc1	strana
Gottwaldovy	Gottwaldův	k2eAgFnSc2d1	Gottwaldova
(	(	kIx(	(
<g/>
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
<g/>
)	)	kIx)	)
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
zásadně	zásadně	k6eAd1	zásadně
narušen	narušit	k5eAaPmNgInS	narušit
i	i	k9	i
vzhled	vzhled	k1gInSc4	vzhled
Komenského	Komenského	k2eAgNnSc2d1	Komenského
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
novodobá	novodobý	k2eAgFnSc1d1	novodobá
budova	budova	k1gFnSc1	budova
nákupního	nákupní	k2eAgNnSc2d1	nákupní
střediska	středisko	k1gNnSc2	středisko
<g/>
,	,	kIx,	,
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
stál	stát	k5eAaImAgInS	stát
už	už	k6eAd1	už
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
moderní	moderní	k2eAgInSc4d1	moderní
obchodní	obchodní	k2eAgInSc4d1	obchodní
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Zastavěna	zastavěn	k2eAgFnSc1d1	zastavěna
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
také	také	k9	také
volná	volný	k2eAgFnSc1d1	volná
plocha	plocha	k1gFnSc1	plocha
severně	severně	k6eAd1	severně
od	od	k7c2	od
Žďárské	Žďárské	k2eAgFnSc2d1	Žďárské
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
tady	tady	k6eAd1	tady
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
sídliště	sídliště	k1gNnSc1	sídliště
Pod	pod	k7c7	pod
zastávkou	zastávka	k1gFnSc7	zastávka
a	a	k8xC	a
panelová	panelový	k2eAgFnSc1d1	panelová
výstavba	výstavba	k1gFnSc1	výstavba
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
výstavbou	výstavba	k1gFnSc7	výstavba
sídliště	sídliště	k1gNnSc2	sídliště
Pod	pod	k7c7	pod
nemocnicí	nemocnice	k1gFnSc7	nemocnice
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnPc1	ulice
Pavlovova	Pavlovův	k2eAgFnSc1d1	Pavlovova
a	a	k8xC	a
Mendlova	Mendlův	k2eAgFnSc1d1	Mendlova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
panelových	panelový	k2eAgNnPc2d1	panelové
sídlišť	sídliště	k1gNnPc2	sídliště
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
dvě	dva	k4xCgFnPc4	dva
hotelové	hotelový	k2eAgFnPc4d1	hotelová
ubytovny	ubytovna	k1gFnPc4	ubytovna
pro	pro	k7c4	pro
horníky	horník	k1gMnPc4	horník
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
Uno	Uno	k1gFnSc2	Uno
a	a	k8xC	a
Duo	duo	k1gNnSc4	duo
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
stará	starý	k2eAgFnSc1d1	stará
hotelovka	hotelovka	k1gFnSc1	hotelovka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Uno	Uno	k1gFnSc1	Uno
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
upravena	upraven	k2eAgFnSc1d1	upravena
přestavbou	přestavba	k1gFnSc7	přestavba
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
přístavbou	přístavba	k1gFnSc7	přístavba
na	na	k7c4	na
městské	městský	k2eAgInPc4d1	městský
byty	byt	k1gInPc4	byt
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
nová	nový	k2eAgFnSc1d1	nová
hotelovka	hotelovka	k1gFnSc1	hotelovka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Duo	duo	k1gNnSc4	duo
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
prodána	prodat	k5eAaPmNgFnS	prodat
realitní	realitní	k2eAgFnSc3d1	realitní
kanceláři	kancelář	k1gFnSc3	kancelář
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
jednak	jednak	k8xC	jednak
byty	byt	k1gInPc4	byt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
také	také	k9	také
využívana	využívan	k1gMnSc2	využívan
ke	k	k7c3	k
komerčním	komerční	k2eAgInPc3d1	komerční
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
rodinných	rodinný	k2eAgInPc2d1	rodinný
domků	domek	k1gInPc2	domek
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
především	především	k9	především
na	na	k7c6	na
území	území	k1gNnSc6	území
nad	nad	k7c7	nad
Kazmírovým	Kazmírův	k2eAgInSc7d1	Kazmírův
rybníkem	rybník	k1gInSc7	rybník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
samostatná	samostatný	k2eAgFnSc1d1	samostatná
městská	městský	k2eAgFnSc1d1	městská
čtvrť	čtvrť	k1gFnSc1	čtvrť
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
písmene	písmeno	k1gNnSc2	písmeno
U	u	k7c2	u
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
Betlém	Betlém	k1gInSc1	Betlém
<g/>
.	.	kIx.	.
</s>
<s>
Rodinné	rodinný	k2eAgInPc4d1	rodinný
domky	domek	k1gInPc4	domek
se	se	k3xPyFc4	se
však	však	k9	však
stavěly	stavět	k5eAaImAgInP	stavět
i	i	k9	i
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgNnPc2d1	další
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
za	za	k7c7	za
Klečkovcem	Klečkovec	k1gMnSc7	Klečkovec
a	a	k8xC	a
Cihelňákem	Cihelňák	k1gMnSc7	Cihelňák
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nazývané	nazývaný	k2eAgFnSc6d1	nazývaná
Korsika	Korsika	k1gFnSc1	Korsika
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Dukelská	dukelský	k2eAgFnSc1d1	Dukelská
a	a	k8xC	a
Veslařská	veslařský	k2eAgFnSc1d1	Veslařská
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Brožkova	Brožkův	k2eAgInSc2d1	Brožkův
kopce	kopec	k1gInSc2	kopec
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnPc1	ulice
Na	na	k7c4	na
Výsluní	výsluní	k1gNnSc4	výsluní
<g/>
,	,	kIx,	,
Zahradní	zahradní	k2eAgNnSc4d1	zahradní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
nový	nový	k2eAgInSc4d1	nový
hotel	hotel	k1gInSc4	hotel
Ski	ski	k1gFnSc2	ski
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgMnSc1d1	postavený
v	v	k7c6	v
lese	les	k1gInSc6	les
Ochoza	Ochoza	k?	Ochoza
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Harusova	Harusův	k2eAgInSc2d1	Harusův
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
bytovou	bytový	k2eAgFnSc7d1	bytová
výstavbou	výstavba	k1gFnSc7	výstavba
a	a	k8xC	a
nárůstem	nárůst	k1gInSc7	nárůst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
došlo	dojít	k5eAaPmAgNnS	dojít
od	od	k7c2	od
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
objektů	objekt	k1gInPc2	objekt
občanské	občanský	k2eAgFnSc2d1	občanská
vybavenosti	vybavenost	k1gFnSc2	vybavenost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Leandra	Leandr	k1gMnSc2	Leandr
Čecha	Čech	k1gMnSc2	Čech
<g/>
,	,	kIx,	,
západně	západně	k6eAd1	západně
od	od	k7c2	od
gymnázia	gymnázium	k1gNnSc2	gymnázium
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Klečkovskému	Klečkovský	k2eAgInSc3d1	Klečkovský
rybníku	rybník	k1gInSc3	rybník
<g/>
,	,	kIx,	,
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
nová	nový	k2eAgFnSc1d1	nová
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
<g/>
;	;	kIx,	;
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zbořené	zbořený	k2eAgFnSc2d1	zbořená
sokolovny	sokolovna	k1gFnSc2	sokolovna
na	na	k7c6	na
Tyršově	Tyršův	k2eAgFnSc6d1	Tyršova
ulici	ulice	k1gFnSc6	ulice
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
kulturní	kulturní	k2eAgInSc1d1	kulturní
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
byly	být	k5eAaImAgFnP	být
také	také	k9	také
mateřské	mateřský	k2eAgFnPc1d1	mateřská
školy	škola	k1gFnPc1	škola
(	(	kIx(	(
<g/>
na	na	k7c4	na
Drobného	drobný	k2eAgMnSc4d1	drobný
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgFnSc6d1	Malé
<g/>
,	,	kIx,	,
Tyršově	Tyršův	k2eAgFnSc6d1	Tyršova
a	a	k8xC	a
Žďárské	Žďárské	k2eAgFnSc6d1	Žďárské
ulici	ulice	k1gFnSc6	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInSc1d1	stavební
ruch	ruch	k1gInSc1	ruch
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
i	i	k9	i
areál	areál	k1gInSc1	areál
okresní	okresní	k2eAgFnSc2d1	okresní
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
podařilo	podařit	k5eAaPmAgNnS	podařit
dokončit	dokončit	k5eAaPmF	dokončit
dlouho	dlouho	k6eAd1	dlouho
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
stavbu	stavba	k1gFnSc4	stavba
gynekologicko-porodnického	gynekologickoorodnický	k2eAgInSc2d1	gynekologicko-porodnický
pavilonu	pavilon	k1gInSc2	pavilon
<g/>
;	;	kIx,	;
postupně	postupně	k6eAd1	postupně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
většiny	většina	k1gFnSc2	většina
pavilonů	pavilon	k1gInPc2	pavilon
<g/>
,	,	kIx,	,
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
centrálního	centrální	k2eAgInSc2d1	centrální
operačního	operační	k2eAgInSc2d1	operační
sálu	sál	k1gInSc2	sál
a	a	k8xC	a
heliportu	heliport	k1gInSc2	heliport
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
už	už	k6eAd1	už
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
záměr	záměr	k1gInSc1	záměr
vybudovat	vybudovat	k5eAaPmF	vybudovat
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
mimo	mimo	k7c4	mimo
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
pod	pod	k7c7	pod
zámkem	zámek	k1gInSc7	zámek
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
otevření	otevření	k1gNnSc3	otevření
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Žďárské	Žďárské	k2eAgFnSc2d1	Žďárské
ulice	ulice	k1gFnSc2	ulice
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
Dům	dům	k1gInSc1	dům
s	s	k7c7	s
pečovatelskou	pečovatelský	k2eAgFnSc7d1	pečovatelská
službou	služba	k1gFnSc7	služba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
městské	městský	k2eAgInPc1d1	městský
byty	byt	k1gInPc1	byt
<g/>
.	.	kIx.	.
</s>
<s>
Nečasovu	Nečasův	k2eAgFnSc4d1	Nečasova
ulici	ulice	k1gFnSc4	ulice
tvoří	tvořit	k5eAaImIp3nP	tvořit
domy	dům	k1gInPc1	dům
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
komunikace	komunikace	k1gFnSc2	komunikace
vycházející	vycházející	k2eAgFnSc6d1	vycházející
z	z	k7c2	z
Palackého	Palackého	k2eAgNnSc2d1	Palackého
náměstí	náměstí	k1gNnSc2	náměstí
a	a	k8xC	a
ústící	ústící	k2eAgMnSc1d1	ústící
do	do	k7c2	do
kruhového	kruhový	k2eAgInSc2d1	kruhový
objezdu	objezd	k1gInSc2	objezd
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
několik	několik	k4yIc4	několik
domů	dům	k1gInPc2	dům
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
silnice	silnice	k1gFnSc2	silnice
stoupající	stoupající	k2eAgFnSc2d1	stoupající
k	k	k7c3	k
Nové	Nové	k2eAgFnSc3d1	Nové
Vsi	ves	k1gFnSc3	ves
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ulice	ulice	k1gFnSc1	ulice
se	se	k3xPyFc4	se
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
nazývala	nazývat	k5eAaImAgFnS	nazývat
Bobrovská	Bobrovský	k2eAgFnSc1d1	Bobrovská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
starých	starý	k2eAgInPc6d1	starý
zápisech	zápis	k1gInPc6	zápis
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
název	název	k1gInSc1	název
Obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
,	,	kIx,	,
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Bobrové	bobrový	k2eAgFnSc2d1	Bobrová
říkalo	říkat	k5eAaImAgNnS	říkat
i	i	k9	i
Obrová	Obrový	k2eAgFnSc1d1	Obrový
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
zástavbu	zástavba	k1gFnSc4	zástavba
Bobrovské	Bobrovský	k2eAgFnSc2d1	Bobrovská
ulice	ulice	k1gFnSc2	ulice
tvořily	tvořit	k5eAaImAgInP	tvořit
pouze	pouze	k6eAd1	pouze
domy	dům	k1gInPc1	dům
od	od	k7c2	od
Palackého	Palackého	k2eAgFnPc2d1	Palackého
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Dolního	dolní	k2eAgNnSc2d1	dolní
<g/>
)	)	kIx)	)
náměstí	náměstí	k1gNnSc2	náměstí
k	k	k7c3	k
mostu	most	k1gInSc3	most
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
čísla	číslo	k1gNnSc2	číslo
popisná	popisný	k2eAgFnSc1d1	popisná
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
25	[number]	k4	25
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kdysi	kdysi	k6eAd1	kdysi
stával	stávat	k5eAaImAgInS	stávat
až	až	k9	až
za	za	k7c7	za
mostem	most	k1gInSc7	most
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
počítán	počítán	k2eAgMnSc1d1	počítán
k	k	k7c3	k
ulici	ulice	k1gFnSc3	ulice
Svatojánské	svatojánský	k2eAgFnSc3d1	Svatojánská
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
ústí	ústit	k5eAaImIp3nS	ústit
Bobrovské	Bobrovský	k2eAgFnPc4d1	Bobrovská
ulice	ulice	k1gFnPc4	ulice
u	u	k7c2	u
přechodu	přechod	k1gInSc2	přechod
potoka	potok	k1gInSc2	potok
Bezděčky	Bezděčka	k1gFnSc2	Bezděčka
doznalo	doznat	k5eAaPmAgNnS	doznat
oproti	oproti	k7c3	oproti
minulosti	minulost	k1gFnSc3	minulost
největších	veliký	k2eAgFnPc2d3	veliký
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
okresní	okresní	k2eAgFnSc2d1	okresní
silnice	silnice	k1gFnSc2	silnice
procházející	procházející	k2eAgFnSc2d1	procházející
Novým	nový	k2eAgNnSc7d1	nové
Městem	město	k1gNnSc7	město
byla	být	k5eAaImAgFnS	být
Bobrovská	Bobrovský	k2eAgFnSc1d1	Bobrovská
ulice	ulice	k1gFnSc1	ulice
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1938	[number]	k4	1938
a	a	k8xC	a
1939	[number]	k4	1939
vydlážděna	vydláždit	k5eAaPmNgFnS	vydláždit
kamennou	kamenný	k2eAgFnSc7d1	kamenná
dlažbou	dlažba	k1gFnSc7	dlažba
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
silnice	silnice	k1gFnSc1	silnice
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
Bobrovské	Bobrovský	k2eAgFnSc2d1	Bobrovská
ulice	ulice	k1gFnSc2	ulice
po	po	k7c6	po
přechodu	přechod	k1gInSc6	přechod
potoka	potok	k1gInSc2	potok
Bezděčky	Bezděčka	k1gFnSc2	Bezděčka
napřímena	napřímen	k2eAgFnSc1d1	napřímena
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
opisovala	opisovat	k5eAaImAgFnS	opisovat
pravý	pravý	k2eAgInSc4d1	pravý
oblouk	oblouk	k1gInSc4	oblouk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Muselo	muset	k5eAaImAgNnS	muset
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
postavení	postavení	k1gNnSc3	postavení
nového	nový	k2eAgInSc2d1	nový
mostu	most	k1gInSc2	most
a	a	k8xC	a
přeložení	přeložení	k1gNnSc2	přeložení
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
nad	nad	k7c7	nad
Jelínkovým	Jelínkův	k2eAgInSc7d1	Jelínkův
mlýnem	mlýn	k1gInSc7	mlýn
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
26	[number]	k4	26
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
nový	nový	k2eAgInSc1d1	nový
železobetonový	železobetonový	k2eAgInSc1d1	železobetonový
most	most	k1gInSc1	most
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
8	[number]	k4	8
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
také	také	k9	také
přesunut	přesunut	k2eAgInSc4d1	přesunut
kříž	kříž	k1gInSc4	kříž
stojící	stojící	k2eAgInSc4d1	stojící
ve	v	k7c6	v
větvení	větvení	k1gNnSc6	větvení
silnice	silnice	k1gFnSc2	silnice
na	na	k7c4	na
Novou	nový	k2eAgFnSc4d1	nová
Ves	ves	k1gFnSc4	ves
a	a	k8xC	a
Petrovice	Petrovice	k1gFnSc1	Petrovice
<g/>
,	,	kIx,	,
křižovatka	křižovatka	k1gFnSc1	křižovatka
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
a	a	k8xC	a
zpřehledněna	zpřehlednit	k5eAaPmNgFnS	zpřehlednit
vykácením	vykácení	k1gNnSc7	vykácení
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
křižovatka	křižovatka	k1gFnSc1	křižovatka
opět	opět	k6eAd1	opět
měnila	měnit	k5eAaImAgFnS	měnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	jíst	k5eAaImIp3nS	jíst
začala	začít	k5eAaPmAgFnS	začít
procházet	procházet	k5eAaImF	procházet
nově	nově	k6eAd1	nově
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
přeložka	přeložka	k1gFnSc1	přeložka
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
s	s	k7c7	s
kruhovým	kruhový	k2eAgInSc7d1	kruhový
objezdem	objezd	k1gInSc7	objezd
získala	získat	k5eAaPmAgFnS	získat
teprve	teprve	k9	teprve
nedávno	nedávno	k6eAd1	nedávno
<g/>
.	.	kIx.	.
</s>
<s>
Peklem	peklo	k1gNnSc7	peklo
byl	být	k5eAaImAgInS	být
nazýván	nazýván	k2eAgInSc1d1	nazýván
už	už	k6eAd1	už
ve	v	k7c6	v
starých	starý	k2eAgFnPc6d1	stará
pozemkových	pozemkový	k2eAgFnPc6d1	pozemková
knihách	kniha	k1gFnPc6	kniha
dům	dům	k1gInSc4	dům
čp.	čp.	k?	čp.
20	[number]	k4	20
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc3	jeho
majiteli	majitel	k1gMnSc3	majitel
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
říkalo	říkat	k5eAaImAgNnS	říkat
Pekelník	pekelník	k1gMnSc1	pekelník
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tu	tu	k6eAd1	tu
býval	bývat	k5eAaImAgInS	bývat
hamr	hamr	k1gInSc1	hamr
<g/>
.	.	kIx.	.
</s>
<s>
Víc	hodně	k6eAd2	hodně
než	než	k8xS	než
hamr	hamr	k1gInSc4	hamr
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
hospoda	hospoda	k?	hospoda
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Pekle	peklo	k1gNnSc6	peklo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
tu	tu	k6eAd1	tu
otevřel	otevřít	k5eAaPmAgInS	otevřít
původem	původ	k1gInSc7	původ
žďárský	žďárský	k2eAgMnSc1d1	žďárský
řezník	řezník	k1gMnSc1	řezník
Václav	Václav	k1gMnSc1	Václav
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
byla	být	k5eAaImAgFnS	být
vyhlášenou	vyhlášený	k2eAgFnSc7d1	vyhlášená
kuchařkou	kuchařka	k1gFnSc7	kuchařka
a	a	k8xC	a
tak	tak	k6eAd1	tak
hospoda	hospoda	k?	hospoda
prosperovala	prosperovat	k5eAaImAgFnS	prosperovat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Procházka	Procházka	k1gMnSc1	Procházka
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
proslavil	proslavit	k5eAaPmAgMnS	proslavit
v	v	k7c6	v
revolučním	revoluční	k2eAgInSc6d1	revoluční
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
hejtmanem	hejtman	k1gMnSc7	hejtman
a	a	k8xC	a
cvičitelem	cvičitel	k1gMnSc7	cvičitel
Národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
hostinským	hostinský	k1gMnSc7	hostinský
v	v	k7c6	v
"	"	kIx"	"
<g/>
Pekle	peklo	k1gNnSc6	peklo
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
další	další	k2eAgMnSc1d1	další
žďárský	žďárský	k2eAgMnSc1d1	žďárský
rodák	rodák	k1gMnSc1	rodák
Josef	Josef	k1gMnSc1	Josef
Zábrš	Zábrš	k1gMnSc1	Zábrš
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pekla	peklo	k1gNnSc2	peklo
můžeme	moct	k5eAaImIp1nP	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
rovnou	rovnou	k6eAd1	rovnou
do	do	k7c2	do
očistce	očistec	k1gInSc2	očistec
<g/>
.	.	kIx.	.
</s>
<s>
Skončíme	skončit	k5eAaPmIp1nP	skončit
tak	tak	k9	tak
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
hospodě	hospodě	k?	hospodě
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bývala	bývat	k5eAaImAgFnS	bývat
v	v	k7c6	v
nedávno	nedávno	k6eAd1	nedávno
zbořeném	zbořený	k2eAgInSc6d1	zbořený
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
27	[number]	k4	27
stojícím	stojící	k2eAgFnPc3d1	stojící
u	u	k7c2	u
odbočky	odbočka	k1gFnSc2	odbočka
k	k	k7c3	k
Jelínkovu	Jelínkův	k2eAgInSc3d1	Jelínkův
mlýnu	mlýn	k1gInSc3	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Říkalo	říkat	k5eAaImAgNnS	říkat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
V	v	k7c6	v
Očistci	očistec	k1gInSc6	očistec
nebo	nebo	k8xC	nebo
u	u	k7c2	u
Valíšků	Valíšek	k1gInPc2	Valíšek
<g/>
.	.	kIx.	.
</s>
<s>
Hostinským	hostinský	k1gMnPc3	hostinský
tu	tu	k6eAd1	tu
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
býval	bývat	k5eAaImAgMnS	bývat
Josef	Josef	k1gMnSc1	Josef
Vališ	Vališ	k1gMnSc1	Vališ
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gMnSc1	jeho
stejnojmenný	stejnojmenný	k2eAgMnSc1d1	stejnojmenný
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
také	také	k6eAd1	také
tkalci	tkadlec	k1gMnPc1	tkadlec
a	a	k8xC	a
kostelníky	kostelník	k1gMnPc7	kostelník
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
ml.	ml.	kA	ml.
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
hostinec	hostinec	k1gInSc1	hostinec
prodal	prodat	k5eAaPmAgInS	prodat
a	a	k8xC	a
odstěhoval	odstěhovat	k5eAaPmAgInS	odstěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
pekla	peklo	k1gNnSc2	peklo
a	a	k8xC	a
očistce	očistec	k1gInSc2	očistec
možná	možná	k9	možná
čekáte	čekat	k5eAaImIp2nP	čekat
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
půjdeme	jít	k5eAaImIp1nP	jít
rovnou	rovnou	k6eAd1	rovnou
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Zastavíme	zastavit	k5eAaPmIp1nP	zastavit
se	se	k3xPyFc4	se
v	v	k7c6	v
barvírně	barvírna	k1gFnSc6	barvírna
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
barvířský	barvířský	k2eAgInSc1d1	barvířský
závod	závod	k1gInSc1	závod
provozoval	provozovat	k5eAaImAgInS	provozovat
v	v	k7c6	v
čp.	čp.	k?	čp.
29	[number]	k4	29
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
František	František	k1gMnSc1	František
Kubík	Kubík	k1gMnSc1	Kubík
<g/>
.	.	kIx.	.
</s>
<s>
Svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc3	okolí
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
znám	znát	k5eAaImIp1nS	znát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
za	za	k7c7	za
domem	dům	k1gInSc7	dům
tekl	téct	k5eAaImAgMnS	téct
"	"	kIx"	"
<g/>
věčně	věčně	k6eAd1	věčně
modrý	modrý	k2eAgInSc1d1	modrý
potok	potok	k1gInSc1	potok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dříve	dříve	k6eAd2	dříve
tekl	téct	k5eAaImAgInS	téct
za	za	k7c4	za
domy	dům	k1gInPc4	dům
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
Bobrovské	Bobrovský	k2eAgFnSc2d1	Bobrovská
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
mlýnským	mlýnský	k2eAgInSc7d1	mlýnský
odpadem	odpad	k1gInSc7	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Tekla	téct	k5eAaImAgFnS	téct
tudy	tudy	k6eAd1	tudy
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
Kazmírova	Kazmírův	k2eAgInSc2d1	Kazmírův
mlýna	mlýn	k1gInSc2	mlýn
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
35	[number]	k4	35
<g/>
)	)	kIx)	)
a	a	k8xC	a
ústila	ústit	k5eAaImAgFnS	ústit
do	do	k7c2	do
nádrže	nádrž	k1gFnSc2	nádrž
před	před	k7c7	před
Jelínkovým	Jelínkův	k2eAgInSc7d1	Jelínkův
mlýnem	mlýn	k1gInSc7	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mlýn	mlýn	k1gInSc1	mlýn
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
popisným	popisný	k2eAgNnSc7d1	popisné
26	[number]	k4	26
stojí	stát	k5eAaImIp3nS	stát
sice	sice	k8xC	sice
trochu	trochu	k6eAd1	trochu
stranou	stranou	k6eAd1	stranou
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
můžeme	moct	k5eAaImIp1nP	moct
ho	on	k3xPp3gNnSc4	on
ještě	ještě	k9	ještě
k	k	k7c3	k
Bobrovské	Bobrovský	k2eAgFnSc3d1	Bobrovská
ulici	ulice	k1gFnSc3	ulice
počítat	počítat	k5eAaImF	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Mlýn	mlýn	k1gInSc1	mlýn
tu	tu	k6eAd1	tu
býval	bývat	k5eAaImAgInS	bývat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říkalo	říkat	k5eAaImAgNnS	říkat
Šarlův	Šarlův	k2eAgMnSc1d1	Šarlův
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ho	on	k3xPp3gMnSc4	on
koupil	koupit	k5eAaPmAgMnS	koupit
Jiřík	Jiřík	k1gMnSc1	Jiřík
Humpolecký	humpolecký	k2eAgMnSc1d1	humpolecký
z	z	k7c2	z
Rybenska	Rybensko	k1gNnSc2	Rybensko
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
mlýn	mlýn	k1gInSc1	mlýn
Humpolecký	humpolecký	k2eAgInSc1d1	humpolecký
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
mlýn	mlýn	k1gInSc1	mlýn
zpustl	zpustnout	k5eAaPmAgInS	zpustnout
a	a	k8xC	a
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
vrchnosti	vrchnost	k1gFnSc2	vrchnost
<g/>
.	.	kIx.	.
</s>
<s>
Vrchnostenskými	vrchnostenský	k2eAgMnPc7d1	vrchnostenský
mlynáři	mlynář	k1gMnPc7	mlynář
tu	tu	k6eAd1	tu
byli	být	k5eAaImAgMnP	být
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
členové	člen	k1gMnPc1	člen
mlynářského	mlynářský	k2eAgInSc2d1	mlynářský
rodu	rod	k1gInSc2	rod
Jelínků	Jelínek	k1gMnPc2	Jelínek
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
mlynářem	mlynář	k1gMnSc7	mlynář
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
ve	v	k7c6	v
století	století	k1gNnSc6	století
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
Augustin	Augustin	k1gMnSc1	Augustin
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
na	na	k7c6	na
domech	dům	k1gInPc6	dům
v	v	k7c6	v
Bobrovské	Bobrovský	k2eAgFnSc6d1	Bobrovská
ulici	ulice	k1gFnSc6	ulice
objevovat	objevovat	k5eAaImF	objevovat
pamětní	pamětní	k2eAgFnPc4d1	pamětní
desky	deska	k1gFnPc4	deska
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
významným	významný	k2eAgFnPc3d1	významná
osobnostem	osobnost	k1gFnPc3	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
se	s	k7c7	s
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1938	[number]	k4	1938
odhalovala	odhalovat	k5eAaImAgFnS	odhalovat
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
na	na	k7c6	na
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
22	[number]	k4	22
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
raného	raný	k2eAgNnSc2d1	rané
dětství	dětství	k1gNnSc2	dětství
žil	žít	k5eAaImAgMnS	žít
ruský	ruský	k2eAgMnSc1d1	ruský
legionář	legionář	k1gMnSc1	legionář
František	František	k1gMnSc1	František
Seidl	Seidl	k1gMnSc1	Seidl
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
padl	padnout	k5eAaPmAgMnS	padnout
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1918	[number]	k4	1918
při	při	k7c6	při
dobývání	dobývání	k1gNnSc6	dobývání
Kurganu	kurgan	k1gInSc2	kurgan
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
-	-	kIx~	-
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1947	[number]	k4	1947
-	-	kIx~	-
odhalil	odhalit	k5eAaPmAgMnS	odhalit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
výživy	výživa	k1gFnSc2	výživa
Václav	Václav	k1gMnSc1	Václav
Majer	Majer	k1gMnSc1	Majer
pamětní	pamětní	k2eAgFnSc4d1	pamětní
desku	deska	k1gFnSc4	deska
na	na	k7c6	na
rodném	rodný	k2eAgInSc6d1	rodný
domě	dům	k1gInSc6	dům
ing.	ing.	kA	ing.
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nečas	Nečas	k1gMnSc1	Nečas
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
31	[number]	k4	31
<g/>
,	,	kIx,	,
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ministrem	ministr	k1gMnSc7	ministr
sociální	sociální	k2eAgFnSc2d1	sociální
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
i	i	k9	i
členem	člen	k1gMnSc7	člen
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
byla	být	k5eAaImAgFnS	být
už	už	k6eAd1	už
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1945	[number]	k4	1945
Bobrovská	Bobrovský	k2eAgFnSc1d1	Bobrovská
ulice	ulice	k1gFnSc1	ulice
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
Nečasovu	Nečasův	k2eAgFnSc4d1	Nečasova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
se	se	k3xPyFc4	se
v	v	k7c6	v
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
21	[number]	k4	21
na	na	k7c6	na
Bobrovské	Bobrovský	k2eAgFnSc6d1	Bobrovská
ulici	ulice	k1gFnSc6	ulice
narodil	narodit	k5eAaPmAgMnS	narodit
Josef	Josef	k1gMnSc1	Josef
Veselka	Veselka	k1gMnSc1	Veselka
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
sbormistr	sbormistr	k1gMnSc1	sbormistr
Akademického	akademický	k2eAgNnSc2d1	akademické
pěveckého	pěvecký	k2eAgNnSc2d1	pěvecké
sdružení	sdružení	k1gNnSc2	sdružení
Moravan	Moravan	k1gMnSc1	Moravan
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
brněnské	brněnský	k2eAgFnSc2d1	brněnská
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
a	a	k8xC	a
JAMU	jam	k1gInSc2	jam
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
rodném	rodný	k2eAgInSc6d1	rodný
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
už	už	k6eAd1	už
v	v	k7c6	v
Nečasově	Nečasův	k2eAgFnSc6d1	Nečasova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1996	[number]	k4	1996
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Nečasově	Nečasův	k2eAgFnSc6d1	Nečasova
ulici	ulice	k1gFnSc6	ulice
třetí	třetí	k4xOgFnSc2	třetí
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Palackého	Palackého	k2eAgNnSc1d1	Palackého
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
poměrně	poměrně	k6eAd1	poměrně
exkluzivní	exkluzivní	k2eAgFnSc1d1	exkluzivní
adresa	adresa	k1gFnSc1	adresa
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
jen	jen	k9	jen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
čtyř	čtyři	k4xCgFnPc2	čtyři
domů	dům	k1gInPc2	dům
-	-	kIx~	-
čísel	číslo	k1gNnPc2	číslo
popisných	popisný	k2eAgMnPc2d1	popisný
16	[number]	k4	16
<g/>
,	,	kIx,	,
32	[number]	k4	32
<g/>
,	,	kIx,	,
33	[number]	k4	33
a	a	k8xC	a
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
domy	dům	k1gInPc1	dům
s	s	k7c7	s
okny	okno	k1gNnPc7	okno
do	do	k7c2	do
Palackého	Palackého	k2eAgNnSc2d1	Palackého
náměstí	náměstí	k1gNnSc2	náměstí
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
počítány	počítat	k5eAaImNgFnP	počítat
k	k	k7c3	k
Nečasově	Nečasův	k2eAgFnSc3d1	Nečasova
ulici	ulice	k1gFnSc3	ulice
nebo	nebo	k8xC	nebo
Vratislavovu	Vratislavův	k2eAgNnSc3d1	Vratislavovo
náměstí	náměstí	k1gNnSc3	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zástavbě	zástavba	k1gFnSc6	zástavba
oddělující	oddělující	k2eAgFnSc1d1	oddělující
dnes	dnes	k6eAd1	dnes
obě	dva	k4xCgNnPc4	dva
náměstí	náměstí	k1gNnPc2	náměstí
je	být	k5eAaImIp3nS	být
ukryt	ukryt	k2eAgInSc1d1	ukryt
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
97	[number]	k4	97
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
ještě	ještě	k6eAd1	ještě
nedávno	nedávno	k6eAd1	nedávno
sídlilo	sídlit	k5eAaImAgNnS	sídlit
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
původní	původní	k2eAgFnSc1d1	původní
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přinejmenším	přinejmenším	k6eAd1	přinejmenším
sídlo	sídlo	k1gNnSc1	sídlo
vrchnostenských	vrchnostenský	k2eAgMnPc2d1	vrchnostenský
úředníků	úředník	k1gMnPc2	úředník
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Nové	Nové	k2eAgNnSc4d1	Nové
Město	město	k1gNnSc4	město
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
členové	člen	k1gMnPc1	člen
mocného	mocný	k2eAgInSc2d1	mocný
rodu	rod	k1gInSc2	rod
Pernštejnů	Pernštejn	k1gInPc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
jinak	jinak	k6eAd1	jinak
si	se	k3xPyFc3	se
vyložit	vyložit	k5eAaPmF	vyložit
znamení	znamení	k1gNnSc4	znamení
zubří	zubří	k2eAgFnSc2d1	zubří
hlavy	hlava	k1gFnSc2	hlava
zdobící	zdobící	k2eAgInSc1d1	zdobící
kamenný	kamenný	k2eAgInSc1d1	kamenný
sloup	sloup	k1gInSc1	sloup
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgInSc4	jenž
je	být	k5eAaImIp3nS	být
svedena	sveden	k2eAgFnSc1d1	svedena
klenba	klenba	k1gFnSc1	klenba
přízemní	přízemní	k2eAgFnSc2d1	přízemní
místnosti	místnost	k1gFnSc2	místnost
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
97	[number]	k4	97
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
shluk	shluk	k1gInSc1	shluk
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
terénní	terénní	k2eAgInSc1d1	terénní
zlom	zlom	k1gInSc1	zlom
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Vratislavovo	Vratislavův	k2eAgNnSc4d1	Vratislavovo
a	a	k8xC	a
Palackého	Palackého	k2eAgNnSc4d1	Palackého
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
bylo	být	k5eAaImAgNnS	být
Palackého	Palackého	k2eAgNnSc1d1	Palackého
náměstí	náměstí	k1gNnSc1	náměstí
původně	původně	k6eAd1	původně
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Dolním	dolní	k2eAgNnSc7d1	dolní
náměstím	náměstí	k1gNnSc7	náměstí
či	či	k8xC	či
Dolním	dolní	k2eAgInSc7d1	dolní
rynkem	rynek	k1gInSc7	rynek
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
náměstí	náměstí	k1gNnSc2	náměstí
tvoří	tvořit	k5eAaImIp3nS	tvořit
kašna	kašna	k1gFnSc1	kašna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
podobu	podoba	k1gFnSc4	podoba
kamenného	kamenný	k2eAgNnSc2d1	kamenné
kvadrilobu	kvadriloba	k1gFnSc4	kvadriloba
(	(	kIx(	(
<g/>
čtyřlistu	čtyřlista	k1gMnSc4	čtyřlista
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
byla	být	k5eAaImAgFnS	být
osazena	osadit	k5eAaPmNgFnS	osadit
plastikou	plastika	k1gFnSc7	plastika
Jana	Jan	k1gMnSc2	Jan
Štursy	Štursa	k1gFnSc2	Štursa
Píseň	píseň	k1gFnSc1	píseň
hor	hora	k1gFnPc2	hora
zpodobňující	zpodobňující	k2eAgFnSc2d1	zpodobňující
pasáčka	pasáček	k1gMnSc2	pasáček
s	s	k7c7	s
ovcí	ovce	k1gFnSc7	ovce
na	na	k7c6	na
klíně	klín	k1gInSc6	klín
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
je	být	k5eAaImIp3nS	být
originál	originál	k1gInSc4	originál
plastiky	plastika	k1gFnSc2	plastika
z	z	k7c2	z
hořického	hořický	k2eAgInSc2d1	hořický
pískovce	pískovec	k1gInSc2	pískovec
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
Horáckém	horácký	k2eAgNnSc6d1	Horácké
muzeu	muzeum	k1gNnSc6	muzeum
a	a	k8xC	a
na	na	k7c6	na
kašně	kašna	k1gFnSc6	kašna
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
kopie	kopie	k1gFnSc1	kopie
zhotovená	zhotovený	k2eAgFnSc1d1	zhotovená
ve	v	k7c6	v
zlínské	zlínský	k2eAgFnSc6d1	zlínská
Škole	škola	k1gFnSc6	škola
umění	umění	k1gNnSc4	umění
vedené	vedený	k2eAgNnSc4d1	vedené
tehdy	tehdy	k6eAd1	tehdy
Vincencem	Vincenc	k1gMnSc7	Vincenc
Makovským	Makovský	k1gMnSc7	Makovský
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaný	takzvaný	k2eAgMnSc1d1	takzvaný
Pasáček	pasáček	k1gMnSc1	pasáček
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
jediným	jediný	k2eAgInSc7d1	jediný
sochařským	sochařský	k2eAgInSc7d1	sochařský
výtvorem	výtvor	k1gInSc7	výtvor
zdobícím	zdobící	k2eAgNnSc6d1	zdobící
Palackého	Palackého	k2eAgNnSc6d1	Palackého
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kašně	kašna	k1gFnSc6	kašna
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
umístěna	umístěn	k2eAgFnSc1d1	umístěna
socha	socha	k1gFnSc1	socha
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nakonec	nakonec	k6eAd1	nakonec
našla	najít	k5eAaPmAgFnS	najít
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
protilehlém	protilehlý	k2eAgInSc6d1	protilehlý
svahu	svah	k1gInSc6	svah
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostně	slavnostně	k6eAd1	slavnostně
byla	být	k5eAaImAgFnS	být
odhalena	odhalen	k2eAgFnSc1d1	odhalena
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
náměstí	náměstí	k1gNnSc1	náměstí
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Palackého	Palackého	k2eAgFnSc7d1	Palackého
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
změna	změna	k1gFnSc1	změna
názvu	název	k1gInSc2	název
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Okupace	okupace	k1gFnSc1	okupace
a	a	k8xC	a
zřízení	zřízení	k1gNnSc1	zřízení
protektorátu	protektorát	k1gInSc2	protektorát
však	však	k9	však
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesly	přinést	k5eAaPmAgFnP	přinést
nařízení	nařízení	k1gNnPc1	nařízení
o	o	k7c4	o
odstranění	odstranění	k1gNnSc4	odstranění
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
neodpovídalo	odpovídat	k5eNaImAgNnS	odpovídat
"	"	kIx"	"
<g/>
změněným	změněný	k2eAgInPc3d1	změněný
státoprávním	státoprávní	k2eAgInPc3d1	státoprávní
poměrům	poměr	k1gInPc3	poměr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
socha	socha	k1gFnSc1	socha
Palackého	Palackého	k2eAgFnSc1d1	Palackého
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
náměstí	náměstí	k1gNnSc1	náměstí
opět	opět	k6eAd1	opět
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Dolní	dolní	k2eAgNnSc4d1	dolní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
Palackého	Palacký	k1gMnSc2	Palacký
náměstí	náměstí	k1gNnSc2	náměstí
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
návrat	návrat	k1gInSc4	návrat
sochy	socha	k1gFnSc2	socha
si	se	k3xPyFc3	se
muselo	muset	k5eAaImAgNnS	muset
město	město	k1gNnSc1	město
počkat	počkat	k5eAaPmF	počkat
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
socha	socha	k1gFnSc1	socha
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
sporech	spor	k1gInPc6	spor
o	o	k7c4	o
její	její	k3xOp3gNnPc4	její
umístění	umístění	k1gNnPc4	umístění
znovuodhalena	znovuodhalen	k2eAgNnPc4d1	znovuodhalen
na	na	k7c6	na
původním	původní	k2eAgNnSc6d1	původní
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Píseň	píseň	k1gFnSc1	píseň
hor	hora	k1gFnPc2	hora
je	být	k5eAaImIp3nS	být
i	i	k9	i
socha	socha	k1gFnSc1	socha
Otce	otec	k1gMnSc2	otec
národa	národ	k1gInSc2	národ
dílem	dílo	k1gNnSc7	dílo
Jana	Jan	k1gMnSc2	Jan
Štursy	Štursa	k1gFnSc2	Štursa
<g/>
.	.	kIx.	.
</s>
<s>
Palackého	Palackého	k2eAgFnSc1d1	Palackého
socha	socha	k1gFnSc1	socha
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
i	i	k9	i
Štursovou	Štursová	k1gFnSc4	Štursová
první	první	k4xOgFnSc7	první
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
zakázkou	zakázka	k1gFnSc7	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
sochy	socha	k1gFnPc1	socha
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
a	a	k8xC	a
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
Josefa	Josef	k1gMnSc2	Josef
Jelínka	Jelínek	k1gMnSc2	Jelínek
<g/>
,	,	kIx,	,
Štursova	Štursův	k2eAgMnSc2d1	Štursův
poručníka	poručník	k1gMnSc2	poručník
a	a	k8xC	a
mecenáše	mecenáš	k1gMnSc2	mecenáš
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Jelínek	Jelínek	k1gMnSc1	Jelínek
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
-	-	kIx~	-
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
býval	bývat	k5eAaImAgInS	bývat
novoměstským	novoměstský	k2eAgMnSc7d1	novoměstský
koželuhem	koželuh	k1gMnSc7	koželuh
a	a	k8xC	a
majitelem	majitel	k1gMnSc7	majitel
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
32	[number]	k4	32
na	na	k7c6	na
Palackého	Palackého	k2eAgNnSc6d1	Palackého
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
dům	dům	k1gInSc1	dům
s	s	k7c7	s
věžičkou	věžička	k1gFnSc7	věžička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
u	u	k7c2	u
bohatých	bohatý	k2eAgMnPc2d1	bohatý
Jelínků	Jelínek	k1gMnPc2	Jelínek
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
veřejně	veřejně	k6eAd1	veřejně
činným	činný	k2eAgInPc3d1	činný
<g/>
,	,	kIx,	,
ať	ať	k9	ať
už	už	k9	už
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
nebo	nebo	k8xC	nebo
člen	člen	k1gMnSc1	člen
řady	řada	k1gFnSc2	řada
místních	místní	k2eAgInPc2d1	místní
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
obecního	obecní	k2eAgInSc2d1	obecní
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
zastával	zastávat	k5eAaImAgInS	zastávat
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
post	posta	k1gFnPc2	posta
starosty	starosta	k1gMnSc2	starosta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
poslancem	poslanec	k1gMnSc7	poslanec
Moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
návrh	návrh	k1gInSc4	návrh
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
kašnu	kašna	k1gFnSc4	kašna
na	na	k7c6	na
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Velkém	velký	k2eAgNnSc6d1	velké
náměstí	náměstí	k1gNnSc6	náměstí
zhotovena	zhotovit	k5eAaPmNgFnS	zhotovit
socha	socha	k1gFnSc1	socha
Vratislava	Vratislava	k1gFnSc1	Vratislava
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
bylo	být	k5eAaImAgNnS	být
Josefu	Josef	k1gMnSc3	Josef
Jelínkovi	Jelínek	k1gMnSc3	Jelínek
uděleno	udělit	k5eAaPmNgNnS	udělit
čestné	čestný	k2eAgNnSc1d1	čestné
občanství	občanství	k1gNnSc1	občanství
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otcových	otcův	k2eAgFnPc6d1	otcova
stopách	stopa	k1gFnPc6	stopa
šel	jít	k5eAaImAgMnS	jít
i	i	k9	i
Josef	Josef	k1gMnSc1	Josef
Jelínek	Jelínek	k1gMnSc1	Jelínek
mladší	mladý	k2eAgMnSc1d2	mladší
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
býval	bývat	k5eAaImAgInS	bývat
velitelem	velitel	k1gMnSc7	velitel
novoměstských	novoměstský	k2eAgMnPc2d1	novoměstský
hasičů	hasič	k1gMnPc2	hasič
či	či	k8xC	či
prvním	první	k4xOgMnSc7	první
starostou	starosta	k1gMnSc7	starosta
obnoveného	obnovený	k2eAgInSc2d1	obnovený
Sokola	Sokol	k1gMnSc4	Sokol
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
též	též	k6eAd1	též
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
majitelem	majitel	k1gMnSc7	majitel
prvního	první	k4xOgInSc2	první
automobilu	automobil	k1gInSc2	automobil
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
starostou	starosta	k1gMnSc7	starosta
města	město	k1gNnSc2	město
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jím	jíst	k5eAaImIp1nS	jíst
až	až	k9	až
do	do	k7c2	do
poválečných	poválečný	k2eAgFnPc2d1	poválečná
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
zvolení	zvolení	k1gNnSc2	zvolení
starostou	starosta	k1gMnSc7	starosta
se	se	k3xPyFc4	se
potýkal	potýkat	k5eAaImAgMnS	potýkat
s	s	k7c7	s
nepřízní	nepřízeň	k1gFnSc7	nepřízeň
části	část	k1gFnSc2	část
novoměstských	novoměstský	k2eAgMnPc2d1	novoměstský
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
té	ten	k3xDgFnSc6	ten
se	se	k3xPyFc4	se
nevyhnul	vyhnout	k5eNaPmAgMnS	vyhnout
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
prodal	prodat	k5eAaPmAgMnS	prodat
svůj	svůj	k3xOyFgInSc4	svůj
dům	dům	k1gInSc4	dům
"	"	kIx"	"
<g/>
eráru	erár	k1gInSc2	erár
<g/>
"	"	kIx"	"
a	a	k8xC	a
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
32	[number]	k4	32
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stal	stát	k5eAaPmAgMnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
finančních	finanční	k2eAgInPc2d1	finanční
úřadů	úřad	k1gInPc2	úřad
s	s	k7c7	s
okresní	okresní	k2eAgFnSc7d1	okresní
působností	působnost	k1gFnSc7	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
statut	statut	k1gInSc4	statut
okresního	okresní	k2eAgNnSc2d1	okresní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
našla	najít	k5eAaPmAgFnS	najít
v	v	k7c6	v
domě	dům	k1gInSc6	dům
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
zřízená	zřízený	k2eAgFnSc1d1	zřízená
samostatná	samostatný	k2eAgFnSc1d1	samostatná
hudební	hudební	k2eAgFnSc1d1	hudební
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
osobností	osobnost	k1gFnPc2	osobnost
najdeme	najít	k5eAaPmIp1nP	najít
i	i	k9	i
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
rohového	rohový	k2eAgInSc2d1	rohový
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1662	[number]	k4	1662
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
členů	člen	k1gInPc2	člen
rodu	rod	k1gInSc2	rod
Německých	německý	k2eAgInPc2d1	německý
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgMnS	být
Martin	Martin	k1gMnSc1	Martin
Německý	německý	k2eAgMnSc1d1	německý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Dorotou	Dorota	k1gFnSc7	Dorota
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Kalouskové	Kalouskové	k2eAgFnSc2d1	Kalouskové
zvané	zvaný	k2eAgFnSc2d1	zvaná
Šlejfrlice	Šlejfrlice	k1gFnSc2	Šlejfrlice
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
byla	být	k5eAaImAgFnS	být
majitelkou	majitelka	k1gFnSc7	majitelka
šĺajferny	šĺajferna	k1gFnSc2	šĺajferna
-	-	kIx~	-
brusírny	brusírna	k1gFnSc2	brusírna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přídomek	přídomek	k1gInSc1	přídomek
přešel	přejít	k5eAaPmAgInS	přejít
i	i	k9	i
na	na	k7c4	na
dům	dům	k1gInSc4	dům
čp.	čp.	k?	čp.
34	[number]	k4	34
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
u	u	k7c2	u
Šlejfrlíků	Šlejfrlík	k1gInPc2	Šlejfrlík
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
hudebnímu	hudební	k2eAgNnSc3d1	hudební
nadání	nadání	k1gNnSc3	nadání
svých	svůj	k3xOyFgMnPc2	svůj
obyvatel	obyvatel	k1gMnPc2	obyvatel
získal	získat	k5eAaPmAgMnS	získat
dům	dům	k1gInSc4	dům
během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jiný	jiný	k2eAgInSc4d1	jiný
přídomek	přídomek	k1gInSc4	přídomek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1752	[number]	k4	1752
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
majitelem	majitel	k1gMnSc7	majitel
domu	dům	k1gInSc2	dům
Daniel	Daniel	k1gMnSc1	Daniel
Německý	německý	k2eAgMnSc1d1	německý
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
hudebníkem	hudebník	k1gMnSc7	hudebník
<g/>
,	,	kIx,	,
městským	městský	k2eAgMnSc7d1	městský
varhaníkem	varhaník	k1gMnSc7	varhaník
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
rektorem	rektor	k1gMnSc7	rektor
městské	městský	k2eAgFnSc2d1	městská
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jeho	jeho	k3xOp3gInPc2	jeho
časů	čas	k1gInPc2	čas
se	se	k3xPyFc4	se
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
34	[number]	k4	34
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
u	u	k7c2	u
Varhaníků	varhaník	k1gMnPc2	varhaník
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
rodákem	rodák	k1gMnSc7	rodák
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
34	[number]	k4	34
však	však	k9	však
byl	být	k5eAaImAgMnS	být
Daniel	Daniel	k1gMnSc1	Daniel
Matyáš	Matyáš	k1gMnSc1	Matyáš
Německý	německý	k2eAgMnSc1d1	německý
(	(	kIx(	(
<g/>
1762	[number]	k4	1762
<g/>
-	-	kIx~	-
<g/>
1820	[number]	k4	1820
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
filozofie	filozofie	k1gFnSc1	filozofie
a	a	k8xC	a
veškerého	veškerý	k3xTgNnSc2	veškerý
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
novátor	novátor	k1gMnSc1	novátor
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
také	také	k9	také
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Německých	německý	k2eAgFnPc2d1	německá
dal	dát	k5eAaPmAgMnS	dát
světu	svět	k1gInSc3	svět
i	i	k8xC	i
slavné	slavný	k2eAgMnPc4d1	slavný
lyžaře	lyžař	k1gMnPc4	lyžař
Josefa	Josef	k1gMnSc4	Josef
a	a	k8xC	a
Otakara	Otakar	k1gMnSc4	Otakar
Němce	Němec	k1gMnSc4	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Přídomek	přídomek	k1gInSc1	přídomek
Šlejfrlík	Šlejfrlík	k1gMnSc1	Šlejfrlík
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
Varhaníka	varhaník	k1gMnSc4	varhaník
nezanikl	zaniknout	k5eNaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Přenesl	přenést	k5eAaPmAgInS	přenést
se	se	k3xPyFc4	se
ale	ale	k9	ale
na	na	k7c4	na
protější	protější	k2eAgInSc4d1	protější
rohový	rohový	k2eAgInSc4d1	rohový
dům	dům	k1gInSc4	dům
čp.	čp.	k?	čp.
101	[number]	k4	101
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
držení	držení	k1gNnSc2	držení
druhé	druhý	k4xOgFnSc2	druhý
větve	větev	k1gFnSc2	větev
rodu	rod	k1gInSc2	rod
Německých	německý	k2eAgFnPc2d1	německá
založené	založený	k2eAgFnPc4d1	založená
Václavem	Václav	k1gMnSc7	Václav
Německým	německý	k2eAgMnSc7d1	německý
<g/>
,	,	kIx,	,
vnukem	vnuk	k1gMnSc7	vnuk
Martina	Martin	k1gMnSc2	Martin
Německého-Šlejfrlíka	Německého-Šlejfrlík	k1gMnSc2	Německého-Šlejfrlík
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
"	"	kIx"	"
<g/>
Šlejfrlíků	Šlejfrlík	k1gMnPc2	Šlejfrlík
<g/>
"	"	kIx"	"
pocházel	pocházet	k5eAaImAgMnS	pocházet
i	i	k9	i
František	František	k1gMnSc1	František
Německý	německý	k2eAgMnSc1d1	německý
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
novoměstský	novoměstský	k2eAgMnSc1d1	novoměstský
hostinský	hostinský	k1gMnSc1	hostinský
a	a	k8xC	a
hoteliér	hoteliér	k1gMnSc1	hoteliér
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Hotelu	hotel	k1gInSc2	hotel
Německý	německý	k2eAgMnSc1d1	německý
stojícího	stojící	k2eAgMnSc4d1	stojící
ještě	ještě	k9	ještě
před	před	k7c4	před
30	[number]	k4	30
lety	léto	k1gNnPc7	léto
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavým	zajímavý	k2eAgNnSc7d1	zajímavé
pojítkem	pojítko	k1gNnSc7	pojítko
mezi	mezi	k7c7	mezi
domy	dům	k1gInPc7	dům
čp.	čp.	k?	čp.
34	[number]	k4	34
a	a	k8xC	a
101	[number]	k4	101
bývala	bývat	k5eAaImAgFnS	bývat
městská	městský	k2eAgFnSc1d1	městská
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kdysi	kdysi	k6eAd1	kdysi
uzavírala	uzavírat	k5eAaImAgFnS	uzavírat
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
ze	z	k7c2	z
Žďárské	Žďárské	k2eAgFnSc2d1	Žďárské
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
původu	původ	k1gInSc6	původ
jména	jméno	k1gNnSc2	jméno
Žďárské	Žďárské	k2eAgFnSc2d1	Žďárské
ulice	ulice	k1gFnSc2	ulice
není	být	k5eNaImIp3nS	být
pochyb	pochyba	k1gFnPc2	pochyba
<g/>
,	,	kIx,	,
tudy	tudy	k6eAd1	tudy
se	se	k3xPyFc4	se
jezdilo	jezdit	k5eAaImAgNnS	jezdit
z	z	k7c2	z
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
do	do	k7c2	do
Žďáru	Žďár	k1gInSc2	Žďár
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
dostala	dostat	k5eAaPmAgFnS	dostat
ulice	ulice	k1gFnSc1	ulice
do	do	k7c2	do
vínku	vínek	k1gInSc2	vínek
i	i	k9	i
oheň	oheň	k1gInSc1	oheň
-	-	kIx~	-
žďáření	žďáření	k1gNnSc1	žďáření
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1879	[number]	k4	1879
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
ve	v	k7c6	v
stodole	stodola	k1gFnSc6	stodola
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
84	[number]	k4	84
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
trvale	trvale	k6eAd1	trvale
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
silnému	silný	k2eAgInSc3d1	silný
větru	vítr	k1gInSc3	vítr
ženoucímu	ženoucí	k2eAgInSc3d1	ženoucí
oheň	oheň	k1gInSc1	oheň
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
města	město	k1gNnSc2	město
tehdy	tehdy	k6eAd1	tehdy
shořelo	shořet	k5eAaPmAgNnS	shořet
19	[number]	k4	19
obytných	obytný	k2eAgInPc2d1	obytný
a	a	k8xC	a
12	[number]	k4	12
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
stavení	stavení	k1gNnPc2	stavení
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
do	do	k7c2	do
rozsahu	rozsah	k1gInSc2	rozsah
nemohl	moct	k5eNaImAgMnS	moct
tento	tento	k3xDgInSc4	tento
požár	požár	k1gInSc4	požár
aspirovat	aspirovat	k5eAaImF	aspirovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
titul	titul	k1gInSc4	titul
<g/>
"	"	kIx"	"
největšího	veliký	k2eAgInSc2d3	veliký
požáru	požár	k1gInSc2	požár
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
historickému	historický	k2eAgNnSc3d1	historické
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
později	pozdě	k6eAd2	pozdě
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1879	[number]	k4	1879
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
založen	založen	k2eAgInSc1d1	založen
sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
.	.	kIx.	.
</s>
<s>
Žhavá	žhavý	k2eAgNnPc4d1	žhavé
témata	téma	k1gNnPc4	téma
však	však	k9	však
najdeme	najít	k5eAaPmIp1nP	najít
i	i	k9	i
u	u	k7c2	u
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Žďárské	Žďárská	k1gFnSc3	Žďárská
ulici	ulice	k1gFnSc4	ulice
patřívaly	patřívat	k5eAaImAgInP	patřívat
tři	tři	k4xCgInPc1	tři
mlýny	mlýn	k1gInPc1	mlýn
zmiňované	zmiňovaný	k2eAgInPc1d1	zmiňovaný
již	již	k6eAd1	již
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
mostu	most	k1gInSc2	most
stával	stávat	k5eAaImAgInS	stávat
Hánův	Hánův	k2eAgInSc1d1	Hánův
mlýn	mlýn	k1gInSc1	mlýn
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
81	[number]	k4	81
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mlýn	mlýn	k1gInSc1	mlýn
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
již	již	k6eAd1	již
při	při	k7c6	při
památném	památný	k2eAgInSc6d1	památný
požáru	požár	k1gInSc6	požár
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
požáru	požár	k1gInSc6	požár
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zkáze	zkáza	k1gFnSc3	zkáza
ho	on	k3xPp3gMnSc4	on
přivedli	přivést	k5eAaPmAgMnP	přivést
o	o	k7c4	o
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zdejší	zdejší	k2eAgMnPc1d1	zdejší
mlynáři	mlynář	k1gMnPc1	mlynář
Josef	Josef	k1gMnSc1	Josef
Ondra	Ondra	k1gMnSc1	Ondra
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
Jakub	Jakub	k1gMnSc1	Jakub
Žák	Žák	k1gMnSc1	Žák
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
mlýna	mlýn	k1gInSc2	mlýn
zchátraly	zchátrat	k5eAaPmAgFnP	zchátrat
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
zatíženy	zatížit	k5eAaPmNgInP	zatížit
dluhy	dluh	k1gInPc1	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Rozpadající	rozpadající	k2eAgMnSc1d1	rozpadající
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dobře	dobře	k6eAd1	dobře
pojištěný	pojištěný	k2eAgMnSc1d1	pojištěný
<g/>
,	,	kIx,	,
mlýn	mlýn	k1gInSc1	mlýn
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1907	[number]	k4	1907
do	do	k7c2	do
základů	základ	k1gInPc2	základ
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Mlýn	mlýn	k1gInSc1	mlýn
již	již	k6eAd1	již
obnoven	obnovit	k5eAaPmNgInS	obnovit
nebyl	být	k5eNaImAgInS	být
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pila	pít	k5eAaImAgFnS	pít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
na	na	k7c6	na
mlýně	mlýn	k1gInSc6	mlýn
začal	začít	k5eAaPmAgInS	začít
provozovat	provozovat	k5eAaImF	provozovat
pilařskou	pilařský	k2eAgFnSc4d1	pilařská
živnost	živnost	k1gFnSc4	živnost
Filip	Filip	k1gMnSc1	Filip
Jaroš	Jaroš	k1gMnSc1	Jaroš
<g/>
,	,	kIx,	,
a	a	k8xC	a
právě	právě	k9	právě
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
objekt	objekt	k1gInSc1	objekt
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
Jarošova	Jarošův	k2eAgFnSc1d1	Jarošova
pila	pila	k1gFnSc1	pila
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
ukončil	ukončit	k5eAaPmAgInS	ukončit
činnost	činnost	k1gFnSc4	činnost
i	i	k9	i
dalšího	další	k2eAgInSc2d1	další
mlýna	mlýn	k1gInSc2	mlýn
známého	známý	k2eAgInSc2d1	známý
jako	jako	k9	jako
Kazmírův	Kazmírův	k2eAgMnSc1d1	Kazmírův
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
mlýnu	mlýn	k1gInSc3	mlýn
a	a	k8xC	a
rybníku	rybník	k1gInSc3	rybník
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
říkalo	říkat	k5eAaImAgNnS	říkat
Škrobův	škrobův	k2eAgMnSc1d1	škrobův
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
mlynáře	mlynář	k1gMnSc2	mlynář
Kazimíra	Kazimír	k1gMnSc2	Kazimír
Štampy	Štampa	k1gFnSc2	Štampa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
mlýnu	mlýn	k1gInSc3	mlýn
a	a	k8xC	a
rybníku	rybník	k1gInSc3	rybník
Kazmírův	Kazmírův	k2eAgMnSc1d1	Kazmírův
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
mlynářem	mlynář	k1gMnSc7	mlynář
v	v	k7c6	v
Kazmírově	Kazmírův	k2eAgInSc6d1	Kazmírův
mlýně	mlýn	k1gInSc6	mlýn
byl	být	k5eAaImAgMnS	být
Alois	Alois	k1gMnSc1	Alois
Jelínek	Jelínek	k1gMnSc1	Jelínek
ze	z	k7c2	z
známého	známý	k2eAgInSc2d1	známý
fryšavského	fryšavský	k2eAgInSc2d1	fryšavský
mlynářského	mlynářský	k2eAgInSc2d1	mlynářský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Historii	historie	k1gFnSc4	historie
mlýna	mlýn	k1gInSc2	mlýn
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
z	z	k7c2	z
mlýnů	mlýn	k1gInPc2	mlýn
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
svého	svůj	k3xOyFgInSc2	svůj
zániku	zánik	k1gInSc2	zánik
i	i	k9	i
bez	bez	k7c2	bez
přispění	přispění	k1gNnSc2	přispění
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ho	on	k3xPp3gMnSc4	on
postavil	postavit	k5eAaPmAgMnS	postavit
pod	pod	k7c7	pod
tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
Sadovým	sadový	k2eAgInSc7d1	sadový
rybníkem	rybník	k1gInSc7	rybník
Jan	Jan	k1gMnSc1	Jan
Štrafa	Štraf	k1gMnSc2	Štraf
<g/>
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc1	rybník
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Klečkovský	Klečkovský	k2eAgInSc1d1	Klečkovský
po	po	k7c4	po
Janu	Jana	k1gFnSc4	Jana
Klečkovi	Klečka	k1gMnSc3	Klečka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
mlýn	mlýn	k1gInSc4	mlýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
prodal	prodat	k5eAaPmAgMnS	prodat
vrchnosti	vrchnost	k1gFnSc3	vrchnost
<g/>
.	.	kIx.	.
</s>
<s>
Mlýnu	mlýn	k1gInSc3	mlýn
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
i	i	k9	i
Ráčkův	Ráčkův	k2eAgMnSc1d1	Ráčkův
a	a	k8xC	a
rybníku	rybník	k1gInSc3	rybník
se	se	k3xPyFc4	se
tak	tak	k9	tak
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
mlynářem	mlynář	k1gMnSc7	mlynář
tu	tu	k6eAd1	tu
totiž	totiž	k9	totiž
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Ráček	Ráček	k1gMnSc1	Ráček
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
podivín	podivín	k1gMnSc1	podivín
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
mlýn	mlýn	k1gInSc4	mlýn
zchátrat	zchátrat	k5eAaPmF	zchátrat
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
od	od	k7c2	od
památkového	památkový	k2eAgInSc2d1	památkový
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Mlýn	mlýn	k1gInSc1	mlýn
nakonec	nakonec	k6eAd1	nakonec
koupil	koupit	k5eAaPmAgInS	koupit
novoměstský	novoměstský	k2eAgInSc4d1	novoměstský
velkostatek	velkostatek	k1gInSc4	velkostatek
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1935	[number]	k4	1935
ho	on	k3xPp3gMnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
zbořit	zbořit	k5eAaPmF	zbořit
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zažilo	zažít	k5eAaPmAgNnS	zažít
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
i	i	k8xC	i
válečný	válečný	k2eAgInSc1d1	válečný
požár	požár	k1gInSc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
Letadla	letadlo	k1gNnPc1	letadlo
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
ostřelovala	ostřelovat	k5eAaImAgFnS	ostřelovat
a	a	k8xC	a
bombardovala	bombardovat	k5eAaImAgFnS	bombardovat
ustupující	ustupující	k2eAgFnPc4d1	ustupující
německé	německý	k2eAgFnPc4d1	německá
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Letecký	letecký	k2eAgInSc4d1	letecký
útok	útok	k1gInSc4	útok
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
životem	život	k1gInSc7	život
i	i	k9	i
několik	několik	k4yIc1	několik
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
poškozena	poškodit	k5eAaPmNgFnS	poškodit
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
právě	právě	k9	právě
ve	v	k7c6	v
Žďárské	Žďárské	k2eAgFnSc6d1	Žďárské
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
82	[number]	k4	82
listonoše	listonoš	k1gMnSc4	listonoš
Františka	František	k1gMnSc4	František
Vyplašila	vyplašit	k5eAaPmAgFnS	vyplašit
stojící	stojící	k2eAgFnSc1d1	stojící
v	v	k7c6	v
křižovatce	křižovatka	k1gFnSc6	křižovatka
(	(	kIx(	(
<g/>
u	u	k7c2	u
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
)	)	kIx)	)
dnešních	dnešní	k2eAgFnPc2d1	dnešní
ulic	ulice	k1gFnPc2	ulice
Žďárské	Žďárská	k1gFnSc2	Žďárská
a	a	k8xC	a
Dukelské	dukelský	k2eAgNnSc1d1	Dukelské
již	již	k6eAd1	již
obnoven	obnoven	k2eAgMnSc1d1	obnoven
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Vincence	Vincenc	k1gMnSc2	Vincenc
Makovského	Makovský	k1gMnSc2	Makovský
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
kamenný	kamenný	k2eAgInSc4d1	kamenný
taras	taras	k1gInSc4	taras
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
Žďárská	žďárský	k2eAgFnSc1d1	žďárská
ulice	ulice	k1gFnSc1	ulice
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
Stalingradskou	stalingradský	k2eAgFnSc4d1	Stalingradská
<g/>
,	,	kIx,	,
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
města	město	k1gNnSc2	město
zničeného	zničený	k2eAgNnSc2d1	zničené
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
původnímu	původní	k2eAgInSc3d1	původní
názvu	název	k1gInSc3	název
se	se	k3xPyFc4	se
ulice	ulice	k1gFnSc1	ulice
vrátila	vrátit	k5eAaPmAgFnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
Nové	Nová	k1gFnSc2	Nová
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Moravy	Morava	k1gFnSc2	Morava
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
10	[number]	k4	10
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
600	[number]	k4	600
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Žďárských	Žďárských	k2eAgInPc2d1	Žďárských
vrchů	vrch	k1gInPc2	vrch
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
východní	východní	k2eAgFnSc7d1	východní
součástí	součást	k1gFnSc7	součást
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Žďárské	Žďárské	k2eAgInPc4d1	Žďárské
vrchy	vrch	k1gInPc4	vrch
<g/>
.	.	kIx.	.
</s>
<s>
CHKO	CHKO	kA	CHKO
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
a	a	k8xC	a
rozléhá	rozléhat	k5eAaImIp3nS	rozléhat
se	se	k3xPyFc4	se
na	na	k7c4	na
715	[number]	k4	715
km2	km2	k4	km2
v	v	k7c6	v
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
od	od	k7c2	od
490	[number]	k4	490
do	do	k7c2	do
836,3	[number]	k4	836,3
m	m	kA	m
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Devět	devět	k4xCc1	devět
skal	skála	k1gFnPc2	skála
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pramennou	pramenný	k2eAgFnSc7d1	pramenná
oblastí	oblast	k1gFnSc7	oblast
mnoha	mnoho	k4c2	mnoho
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
prochází	procházet	k5eAaImIp3nS	procházet
hlavní	hlavní	k2eAgFnSc1d1	hlavní
evropská	evropský	k2eAgFnSc1d1	Evropská
rozvodnice	rozvodnice	k1gFnSc1	rozvodnice
mezi	mezi	k7c7	mezi
Severním	severní	k2eAgNnSc7d1	severní
a	a	k8xC	a
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
typických	typický	k2eAgInPc2d1	typický
krajinných	krajinný	k2eAgInPc2d1	krajinný
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
rulové	rulový	k2eAgInPc1d1	rulový
skalní	skalní	k2eAgInPc1d1	skalní
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
mrazovým	mrazový	k2eAgNnSc7d1	mrazové
zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
na	na	k7c6	na
zalesněných	zalesněný	k2eAgInPc6d1	zalesněný
vrcholcích	vrcholek	k1gInPc6	vrcholek
Žďárských	Žďárských	k2eAgInPc2d1	Žďárských
vrchů	vrch	k1gInPc2	vrch
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Malinská	Malinský	k2eAgFnSc1d1	Malinská
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
Dráteničky	Drátenička	k1gFnPc1	Drátenička
<g/>
,	,	kIx,	,
Čtyři	čtyři	k4xCgFnPc1	čtyři
palice	palice	k1gFnPc1	palice
<g/>
,	,	kIx,	,
Pasecká	Pasecký	k2eAgFnSc1d1	Pasecká
či	či	k8xC	či
Lisovská	Lisovský	k2eAgFnSc1d1	Lisovská
skála	skála	k1gFnSc1	skála
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
westernové	westernový	k2eAgNnSc4d1	westernové
městečko	městečko	k1gNnSc4	městečko
Šiklův	Šiklův	k2eAgInSc1d1	Šiklův
mlýn	mlýn	k1gInSc1	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Novým	nový	k2eAgNnSc7d1	nové
Městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
největší	veliký	k2eAgFnSc1d3	veliký
sjezdovka	sjezdovka	k1gFnSc1	sjezdovka
na	na	k7c6	na
Vysočině	vysočina	k1gFnSc6	vysočina
-	-	kIx~	-
sjezdovka	sjezdovka	k1gFnSc1	sjezdovka
Harusův	Harusův	k2eAgInSc1d1	Harusův
kopec	kopec	k1gInSc1	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Sjezdovka	sjezdovka	k1gFnSc1	sjezdovka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
uměle	uměle	k6eAd1	uměle
zasněžována	zasněžovat	k5eAaImNgFnS	zasněžovat
a	a	k8xC	a
osvětlena	osvětlit	k5eAaPmNgFnS	osvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Harusův	Harusův	k2eAgInSc1d1	Harusův
kopec	kopec	k1gInSc1	kopec
-	-	kIx~	-
"	"	kIx"	"
<g/>
Harusák	Harusák	k1gInSc1	Harusák
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
již	již	k6eAd1	již
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Bohumila	Bohumil	k1gMnSc2	Bohumil
Polácha	Polách	k1gMnSc2	Polách
"	"	kIx"	"
<g/>
Srub	srub	k1gInSc1	srub
radosti	radost	k1gFnSc2	radost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc4d1	městský
znak	znak	k1gInSc4	znak
udělil	udělit	k5eAaPmAgMnS	udělit
městu	město	k1gNnSc3	město
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1635	[number]	k4	1635
kardinál	kardinál	k1gMnSc1	kardinál
František	František	k1gMnSc1	František
kníže	kníže	k1gMnSc1	kníže
Dietrichštejn	Dietrichštejn	k1gMnSc1	Dietrichštejn
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc4d1	městský
prapor	prapor	k1gInSc4	prapor
udělilo	udělit	k5eAaPmAgNnS	udělit
městu	město	k1gNnSc3	město
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
svým	svůj	k3xOyFgInSc7	svůj
926	[number]	k4	926
<g/>
.	.	kIx.	.
usnesením	usnesení	k1gNnSc7	usnesení
z	z	k7c2	z
93	[number]	k4	93
<g/>
.	.	kIx.	.
schůze	schůze	k1gFnSc2	schůze
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
tvoří	tvořit	k5eAaImIp3nS	tvořit
oválný	oválný	k2eAgInSc1d1	oválný
štít	štít	k1gInSc1	štít
nakoso	nakoso	k6eAd1	nakoso
půlený	půlený	k2eAgMnSc1d1	půlený
od	od	k7c2	od
pravé	pravý	k2eAgFnSc2d1	pravá
horní	horní	k2eAgFnSc2d1	horní
strany	strana	k1gFnSc2	strana
dolů	dol	k1gInPc2	dol
k	k	k7c3	k
levé	levý	k2eAgFnSc3d1	levá
straně	strana	k1gFnSc3	strana
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc1d1	horní
polovina	polovina	k1gFnSc1	polovina
je	být	k5eAaImIp3nS	být
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc1d1	dolní
červená	červená	k1gFnSc1	červená
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
vinařské	vinařský	k2eAgInPc1d1	vinařský
nože	nůž	k1gInPc1	nůž
se	s	k7c7	s
stříbrnou	stříbrný	k2eAgFnSc7d1	stříbrná
čepelí	čepel	k1gFnSc7	čepel
a	a	k8xC	a
střenkou	střenka	k1gFnSc7	střenka
v	v	k7c6	v
přirozené	přirozený	k2eAgFnSc6d1	přirozená
barvě	barva	k1gFnSc6	barva
stojí	stát	k5eAaImIp3nS	stát
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s>
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
-	-	kIx~	-
ohnutými	ohnutý	k2eAgFnPc7d1	ohnutá
špicemi	špice	k1gFnPc7	špice
nahoru	nahoru	k6eAd1	nahoru
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
noži	nůž	k1gInPc7	nůž
na	na	k7c6	na
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
knížecí	knížecí	k2eAgInSc1d1	knížecí
klobouk	klobouk	k1gInSc1	klobouk
<g/>
,	,	kIx,	,
dole	dole	k6eAd1	dole
mezi	mezi	k7c7	mezi
střenkami	střenka	k1gFnPc7	střenka
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
pak	pak	k6eAd1	pak
sedící	sedící	k2eAgFnSc1d1	sedící
<g/>
,	,	kIx,	,
dozadu	dozadu	k6eAd1	dozadu
se	se	k3xPyFc4	se
ohlížející	ohlížející	k2eAgInSc1d1	ohlížející
zlatý	zlatý	k2eAgInSc1d1	zlatý
lev	lev	k1gInSc1	lev
bez	bez	k7c2	bez
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
s	s	k7c7	s
pozdviženým	pozdvižený	k2eAgInSc7d1	pozdvižený
jedním	jeden	k4xCgInSc7	jeden
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
prapor	prapor	k1gInSc1	prapor
města	město	k1gNnSc2	město
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
má	mít	k5eAaImIp3nS	mít
list	list	k1gInSc1	list
nakoso	nakoso	k6eAd1	nakoso
dělený	dělený	k2eAgInSc1d1	dělený
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc6d1	horní
pole	pola	k1gFnSc6	pola
žluté	žlutý	k2eAgFnSc6d1	žlutá
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc6d1	dolní
červené	červená	k1gFnSc6	červená
<g/>
,	,	kIx,	,
na	na	k7c6	na
třetinách	třetina	k1gFnPc6	třetina
listu	list	k1gInSc2	list
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
odvrácené	odvrácený	k2eAgInPc1d1	odvrácený
vinařské	vinařský	k2eAgInPc1d1	vinařský
nože	nůž	k1gInPc1	nůž
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
čepele	čepel	k1gFnSc2	čepel
nožů	nůž	k1gInPc2	nůž
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
šířky	šířka	k1gFnSc2	šířka
listu	list	k1gInSc2	list
a	a	k8xC	a
střenky	střenka	k1gFnSc2	střenka
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
šířky	šířka	k1gFnPc4	šířka
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
čepele	čepel	k1gFnPc1	čepel
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
střenky	střenka	k1gFnPc1	střenka
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
,	,	kIx,	,
poměr	poměr	k1gInSc1	poměr
délky	délka	k1gFnSc2	délka
k	k	k7c3	k
šířce	šířka	k1gFnSc3	šířka
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
pečeť	pečeť	k1gFnSc4	pečeť
města	město	k1gNnSc2	město
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
tvoří	tvořit	k5eAaImIp3nS	tvořit
městský	městský	k2eAgInSc1d1	městský
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
Město	město	k1gNnSc1	město
-	-	kIx~	-
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
sněhovými	sněhový	k2eAgFnPc7d1	sněhová
vločkami	vločka	k1gFnPc7	vločka
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
maskotem	maskot	k1gInSc7	maskot
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
je	být	k5eAaImIp3nS	být
strašidlo	strašidlo	k1gNnSc1	strašidlo
horáckých	horácký	k2eAgInPc2d1	horácký
lesů	les	k1gInPc2	les
-	-	kIx~	-
Horácký	horácký	k2eAgMnSc1d1	horácký
hejkal	hejkal	k1gMnSc1	hejkal
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
podobu	podoba	k1gFnSc4	podoba
se	se	k3xPyFc4	se
snažilo	snažit	k5eAaImAgNnS	snažit
vystihnout	vystihnout	k5eAaPmF	vystihnout
mnoho	mnoho	k6eAd1	mnoho
novoměstských	novoměstský	k2eAgMnPc2d1	novoměstský
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
tradičním	tradiční	k2eAgInSc7d1	tradiční
vzorem	vzor	k1gInSc7	vzor
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
podoba	podoba	k1gFnSc1	podoba
ztvárněná	ztvárněný	k2eAgFnSc1d1	ztvárněná
novoměstským	novoměstský	k2eAgMnSc7d1	novoměstský
rodákem	rodák	k1gMnSc7	rodák
Karlem	Karel	k1gMnSc7	Karel
Němcem	Němec	k1gMnSc7	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Horáckého	horácký	k2eAgMnSc2d1	horácký
hejkala	hejkal	k1gMnSc2	hejkal
představuje	představovat	k5eAaImIp3nS	představovat
divého	divý	k2eAgMnSc4d1	divý
muže	muž	k1gMnSc4	muž
-	-	kIx~	-
skřeta	skřet	k1gMnSc4	skřet
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
<g/>
,	,	kIx,	,
řídkými	řídký	k2eAgInPc7d1	řídký
rozježenými	rozježený	k2eAgInPc7d1	rozježený
vlasy	vlas	k1gInPc7	vlas
a	a	k8xC	a
vousy	vous	k1gInPc7	vous
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
vlastním	vlastní	k2eAgNnSc7d1	vlastní
městem	město	k1gNnSc7	město
a	a	k8xC	a
9	[number]	k4	9
vesnicemi	vesnice	k1gFnPc7	vesnice
<g/>
:	:	kIx,	:
Hlinné	hlinný	k2eAgFnPc1d1	Hlinná
Jiříkovice	Jiříkovice	k1gFnPc1	Jiříkovice
Maršovice	Maršovice	k1gFnSc2	Maršovice
Olešná	Olešný	k2eAgFnSc1d1	Olešná
Petrovice	Petrovice	k1gFnSc1	Petrovice
Pohledec	Pohledec	k1gMnSc1	Pohledec
Rokytno	Rokytno	k6eAd1	Rokytno
Slavkovice	Slavkovice	k1gFnSc2	Slavkovice
Studnice	studnice	k1gFnSc2	studnice
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
Leandra	Leandr	k1gMnSc2	Leandr
Čecha	Čech	k1gMnSc2	Čech
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
Vratislavovo	Vratislavův	k2eAgNnSc1d1	Vratislavovo
náměstí	náměstí	k1gNnSc1	náměstí
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Vincence	Vincenc	k1gMnSc2	Vincenc
Makovského	Makovský	k1gMnSc2	Makovský
se	s	k7c7	s
sportovními	sportovní	k2eAgFnPc7d1	sportovní
třídami	třída	k1gFnPc7	třída
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
</s>
<s>
Dům	dům	k1gInSc1	dům
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
Jana	Jan	k1gMnSc2	Jan
Štursy	Štursa	k1gFnSc2	Štursa
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
sportovní	sportovní	k2eAgFnSc7d1	sportovní
událostí	událost	k1gFnSc7	událost
je	být	k5eAaImIp3nS	být
závod	závod	k1gInSc1	závod
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
-	-	kIx~	-
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
lyže	lyže	k1gFnSc1	lyže
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
konec	konec	k1gInSc1	konec
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nS	jezdit
v	v	k7c6	v
malebném	malebný	k2eAgNnSc6d1	malebné
okolí	okolí	k1gNnSc6	okolí
hotelu	hotel	k1gInSc6	hotel
Ski	ski	k1gFnPc1	ski
v	v	k7c6	v
lese	les	k1gInSc6	les
Ochoza	Ochoza	k?	Ochoza
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
součástí	součást	k1gFnPc2	součást
projektu	projekt	k1gInSc2	projekt
FIS	FIS	kA	FIS
-	-	kIx~	-
Tour	Tour	k1gMnSc1	Tour
de	de	k?	de
Ski	ski	k1gFnSc2	ski
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
závod	závod	k1gInSc1	závod
Rovner	Rovner	k1gInSc1	Rovner
Vysočina	vysočina	k1gFnSc1	vysočina
(	(	kIx(	(
<g/>
počátek	počátek	k1gInSc1	počátek
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
využíváno	využívat	k5eAaImNgNnS	využívat
také	také	k9	také
pro	pro	k7c4	pro
závody	závod	k1gInPc4	závod
v	v	k7c6	v
orientačním	orientační	k2eAgInSc6d1	orientační
běhu	běh	k1gInSc6	běh
<g/>
,	,	kIx,	,
horských	horský	k2eAgFnPc2d1	horská
kol	kola	k1gFnPc2	kola
<g/>
,	,	kIx,	,
kolečkových	kolečkový	k2eAgFnPc2d1	kolečková
lyží	lyže	k1gFnPc2	lyže
či	či	k8xC	či
triatlonu	triatlon	k1gInSc2	triatlon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konalo	konat	k5eAaImAgNnS	konat
mistrovství	mistrovství	k1gNnSc1	mistrovství
v	v	k7c6	v
orientačním	orientační	k2eAgInSc6d1	orientační
závodě	závod	k1gInSc6	závod
na	na	k7c6	na
horských	horský	k2eAgNnPc6d1	horské
kolech	kolo	k1gNnPc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konalo	konat	k5eAaImAgNnS	konat
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
horských	horský	k2eAgNnPc2d1	horské
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
první	první	k4xOgInSc1	první
závod	závod	k1gInSc1	závod
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
poprvé	poprvé	k6eAd1	poprvé
konalo	konat	k5eAaImAgNnS	konat
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
46	[number]	k4	46
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Nové	Nové	k2eAgNnSc4d1	Nové
Město	město	k1gNnSc4	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
prochází	procházet	k5eAaImIp3nS	procházet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
železniční	železniční	k2eAgFnSc1d1	železniční
Trať	trať	k1gFnSc1	trať
251	[number]	k4	251
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
-	-	kIx~	-
Tišnov	Tišnov	k1gInSc1	Tišnov
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Tišnovka	Tišnovka	k1gFnSc1	Tišnovka
<g/>
,	,	kIx,	,
nádraží	nádraží	k1gNnSc1	nádraží
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
necelý	celý	k2eNgInSc4d1	necelý
kilometr	kilometr	k1gInSc4	kilometr
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
centrálních	centrální	k2eAgNnPc2d1	centrální
náměstí	náměstí	k1gNnPc2	náměstí
<g/>
,	,	kIx,	,
zastávka	zastávka	k1gFnSc1	zastávka
Nové	Nová	k1gFnSc2	Nová
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
zastávka	zastávka	k1gFnSc1	zastávka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
městské	městský	k2eAgFnSc2d1	městská
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
nemocnice	nemocnice	k1gFnSc2	nemocnice
a	a	k8xC	a
továrny	továrna	k1gFnSc2	továrna
MEDIN	Medina	k1gFnPc2	Medina
<g/>
.	.	kIx.	.
</s>
<s>
Jižním	jižní	k2eAgInSc7d1	jižní
obchvatem	obchvat	k1gInSc7	obchvat
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
ulice	ulice	k1gFnSc1	ulice
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
<g/>
)	)	kIx)	)
prochází	procházet	k5eAaImIp3nS	procházet
západovýchodním	západovýchodní	k2eAgInSc7d1	západovýchodní
směrem	směr	k1gInSc7	směr
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Severojižním	severojižní	k2eAgInSc7d1	severojižní
směrem	směr	k1gInSc7	směr
procházejí	procházet	k5eAaImIp3nP	procházet
přes	přes	k7c4	přes
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
peáží	peáž	k1gFnPc2	peáž
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
360	[number]	k4	360
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
354	[number]	k4	354
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
vychází	vycházet	k5eAaImIp3nS	vycházet
ještě	ještě	k9	ještě
několik	několik	k4yIc1	několik
silnic	silnice	k1gFnPc2	silnice
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
36039	[number]	k4	36039
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
na	na	k7c4	na
Zubří	zubří	k2eAgInSc4d1	zubří
a	a	k8xC	a
Vojtěchov	Vojtěchov	k1gInSc4	Vojtěchov
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
35314	[number]	k4	35314
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
na	na	k7c4	na
Tři	tři	k4xCgFnPc4	tři
Studně	studně	k1gFnPc4	studně
(	(	kIx(	(
<g/>
krátkou	krátký	k2eAgFnSc7d1	krátká
spojkou	spojka	k1gFnSc7	spojka
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1844	[number]	k4	1844
napojená	napojený	k2eAgNnPc4d1	napojené
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
35315	[number]	k4	35315
na	na	k7c4	na
západ	západ	k1gInSc4	západ
do	do	k7c2	do
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Jiříkovice	Jiříkovice	k1gFnSc2	Jiříkovice
<g/>
,	,	kIx,	,
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
přirozeným	přirozený	k2eAgInSc7d1	přirozený
uzlem	uzel	k1gInSc7	uzel
regionální	regionální	k2eAgFnSc2d1	regionální
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Dominantním	dominantní	k2eAgMnSc7d1	dominantní
dopravcem	dopravce	k1gMnSc7	dopravce
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ZDAR	zdar	k1gInSc1	zdar
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
základní	základní	k2eAgFnSc2d1	základní
obslužnosti	obslužnost	k1gFnSc2	obslužnost
regionu	region	k1gInSc2	region
zde	zde	k6eAd1	zde
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
i	i	k8xC	i
dvěma	dva	k4xCgFnPc7	dva
dálkovými	dálkový	k2eAgFnPc7d1	dálková
linkami	linka	k1gFnPc7	linka
<g/>
,	,	kIx,	,
840127	[number]	k4	840127
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
-	-	kIx~	-
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
a	a	k8xC	a
840319	[number]	k4	840319
Bystřice	Bystřice	k1gFnSc1	Bystřice
nad	nad	k7c7	nad
Pernštejnem	Pernštejn	k1gInSc7	Pernštejn
<g/>
-	-	kIx~	-
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
zde	zde	k6eAd1	zde
však	však	k9	však
i	i	k9	i
dálková	dálkový	k2eAgFnSc1d1	dálková
linka	linka	k1gFnSc1	linka
dopravce	dopravce	k1gMnSc1	dopravce
Tourbus	Tourbus	k1gMnSc1	Tourbus
a.s.	a.s.	k?	a.s.
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
720265	[number]	k4	720265
Brno	Brno	k1gNnSc1	Brno
<g/>
-	-	kIx~	-
<g/>
Svratka	Svratka	k1gFnSc1	Svratka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
linka	linka	k1gFnSc1	linka
ICOM	ICOM	kA	ICOM
transport	transport	k1gInSc4	transport
760700	[number]	k4	760700
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
-	-	kIx~	-
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
z	z	k7c2	z
Poličky	Polička	k1gFnSc2	Polička
do	do	k7c2	do
města	město	k1gNnSc2	město
jezdí	jezdit	k5eAaImIp3nS	jezdit
linka	linka	k1gFnSc1	linka
840901	[number]	k4	840901
dopravce	dopravce	k1gMnSc1	dopravce
Zlatovánek	Zlatovánek	k1gMnSc1	Zlatovánek
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Autobusy	autobus	k1gInPc1	autobus
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
dva	dva	k4xCgInPc4	dva
terminály	terminál	k1gInPc4	terminál
<g/>
:	:	kIx,	:
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
i	i	k8xC	i
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
terminál	terminál	k1gInSc1	terminál
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
Dopravní	dopravní	k2eAgInSc1d1	dopravní
terminál	terminál	k1gInSc1	terminál
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
zprovozněn	zprovoznit	k5eAaPmNgInS	zprovoznit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
u	u	k7c2	u
železničního	železniční	k2eAgNnSc2d1	železniční
nádraží	nádraží	k1gNnSc2	nádraží
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
autobusových	autobusový	k2eAgNnPc2d1	autobusové
stání	stání	k1gNnPc2	stání
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
3	[number]	k4	3
krytá	krytý	k2eAgFnSc1d1	krytá
<g/>
,	,	kIx,	,
a	a	k8xC	a
33	[number]	k4	33
parkovacích	parkovací	k2eAgNnPc2d1	parkovací
míst	místo	k1gNnPc2	místo
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
R.	R.	kA	R.
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
provoz	provoz	k1gInSc4	provoz
městské	městský	k2eAgFnSc2d1	městská
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
předváděcí	předváděcí	k2eAgFnSc7d1	předváděcí
jízdou	jízda	k1gFnSc7	jízda
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
MHD	MHD	kA	MHD
je	být	k5eAaImIp3nS	být
provozována	provozován	k2eAgFnSc1d1	provozována
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
jednosměrně	jednosměrně	k6eAd1	jednosměrně
okružních	okružní	k2eAgFnPc6d1	okružní
linkách	linka	k1gFnPc6	linka
se	se	k3xPyFc4	se
závleky	závlek	k1gInPc7	závlek
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
ji	on	k3xPp3gFnSc4	on
vozem	vůz	k1gInSc7	vůz
pro	pro	k7c4	pro
25	[number]	k4	25
sedících	sedící	k2eAgMnPc2d1	sedící
cestujících	cestující	k1gMnPc2	cestující
dopravce	dopravce	k1gMnSc1	dopravce
ZDAR	zdar	k1gInSc1	zdar
a.s.	a.s.	k?	a.s.
Během	během	k7c2	během
ročního	roční	k2eAgInSc2d1	roční
zkušebního	zkušební	k2eAgInSc2d1	zkušební
provozu	provoz	k1gInSc2	provoz
chce	chtít	k5eAaImIp3nS	chtít
dopravce	dopravce	k1gMnSc1	dopravce
doladit	doladit	k5eAaPmF	doladit
jízdní	jízdní	k2eAgInPc4d1	jízdní
řády	řád	k1gInPc4	řád
podle	podle	k7c2	podle
připomínek	připomínka	k1gFnPc2	připomínka
a	a	k8xC	a
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
autobusů	autobus	k1gInPc2	autobus
a	a	k8xC	a
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
je	být	k5eAaImIp3nS	být
připraven	připraven	k2eAgInSc1d1	připraven
nasadit	nasadit	k5eAaPmF	nasadit
větší	veliký	k2eAgInSc4d2	veliký
autobus	autobus	k1gInSc4	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
linky	linka	k1gFnPc1	linka
začínají	začínat	k5eAaImIp3nP	začínat
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
dopravním	dopravní	k2eAgInSc6d1	dopravní
terminálu	terminál	k1gInSc6	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
1	[number]	k4	1
(	(	kIx(	(
<g/>
845001	[number]	k4	845001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgNnSc1d2	kratší
<g/>
,	,	kIx,	,
jede	jet	k5eAaImIp3nS	jet
od	od	k7c2	od
Maršovic	Maršovice	k1gFnPc2	Maršovice
přes	přes	k7c4	přes
terminál	terminál	k1gInSc4	terminál
kolem	kolem	k7c2	kolem
kulturního	kulturní	k2eAgInSc2d1	kulturní
domu	dům	k1gInSc2	dům
k	k	k7c3	k
nemocnici	nemocnice	k1gFnSc3	nemocnice
<g/>
,	,	kIx,	,
navazuje	navazovat	k5eAaImIp3nS	navazovat
zejména	zejména	k9	zejména
na	na	k7c4	na
vlaky	vlak	k1gInPc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
2	[number]	k4	2
(	(	kIx(	(
<g/>
845002	[number]	k4	845002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
Maršovic	Maršovice	k1gFnPc2	Maršovice
pod	pod	k7c4	pod
Brožkův	Brožkův	k2eAgInSc4d1	Brožkův
Kopec	kopec	k1gInSc4	kopec
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Dukelskou	dukelský	k2eAgFnSc4d1	Dukelská
ulici	ulice	k1gFnSc4	ulice
<g/>
,	,	kIx,	,
nemocnici	nemocnice	k1gFnSc4	nemocnice
<g/>
,	,	kIx,	,
Palackého	Palackého	k2eAgNnSc4d1	Palackého
náměstí	náměstí	k1gNnSc4	náměstí
k	k	k7c3	k
Bille	Bill	k1gMnSc5	Bill
a	a	k8xC	a
Masarykovou	Masarykův	k2eAgFnSc7d1	Masarykova
ulicí	ulice	k1gFnSc7	ulice
na	na	k7c4	na
dopravní	dopravní	k2eAgInSc4d1	dopravní
terminál	terminál	k1gInSc4	terminál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
místní	místní	k2eAgMnPc4d1	místní
školáky	školák	k1gMnPc4	školák
<g/>
.	.	kIx.	.
</s>
<s>
Jízdy	jízda	k1gFnPc1	jízda
na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
v	v	k7c4	v
sudou	sudý	k2eAgFnSc4d1	sudá
a	a	k8xC	a
lichou	lichý	k2eAgFnSc4d1	lichá
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
jezdí	jezdit	k5eAaImIp3nS	jezdit
celotýdenně	celotýdenně	k6eAd1	celotýdenně
od	od	k7c2	od
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
do	do	k7c2	do
8	[number]	k4	8
večer	večer	k6eAd1	večer
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
ranní	ranní	k2eAgInPc1d1	ranní
a	a	k8xC	a
polední	polední	k2eAgInPc1d1	polední
spoje	spoj	k1gInPc1	spoj
jezdí	jezdit	k5eAaImIp3nP	jezdit
ale	ale	k9	ale
jen	jen	k9	jen
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
přípravy	příprava	k1gFnSc2	příprava
zastávek	zastávka	k1gFnPc2	zastávka
a	a	k8xC	a
roční	roční	k2eAgFnPc1d1	roční
objednávky	objednávka	k1gFnPc1	objednávka
dopravních	dopravní	k2eAgInPc2d1	dopravní
výkonů	výkon	k1gInPc2	výkon
investovalo	investovat	k5eAaBmAgNnS	investovat
město	město	k1gNnSc1	město
asi	asi	k9	asi
milion	milion	k4xCgInSc1	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
roky	rok	k1gInPc4	rok
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
ročními	roční	k2eAgFnPc7d1	roční
dotacemi	dotace	k1gFnPc7	dotace
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
statisíců	statisíce	k1gInPc2	statisíce
<g/>
.	.	kIx.	.
</s>
<s>
Plné	plný	k2eAgNnSc4d1	plné
jízdné	jízdné	k1gNnSc4	jízdné
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
jízdu	jízda	k1gFnSc4	jízda
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
Kč	Kč	kA	Kč
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
se	se	k3xPyFc4	se
při	při	k7c6	při
nástupu	nástup	k1gInSc6	nástup
u	u	k7c2	u
řidiče	řidič	k1gInSc2	řidič
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
od	od	k7c2	od
6	[number]	k4	6
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
poloviční	poloviční	k2eAgFnSc1d1	poloviční
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
Novoměstská	novoměstský	k2eAgFnSc1d1	Novoměstská
karta	karta	k1gFnSc1	karta
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnSc1d1	základní
cena	cena	k1gFnSc1	cena
900	[number]	k4	900
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
studenty	student	k1gMnPc4	student
720	[number]	k4	720
Kč	Kč	kA	Kč
a	a	k8xC	a
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
nad	nad	k7c4	nad
70	[number]	k4	70
let	léto	k1gNnPc2	léto
480	[number]	k4	480
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
ceně	cena	k1gFnSc6	cena
je	být	k5eAaImIp3nS	být
automaticky	automaticky	k6eAd1	automaticky
zahrnuto	zahrnut	k2eAgNnSc1d1	zahrnuto
roční	roční	k2eAgNnSc1d1	roční
předplatné	předplatné	k1gNnSc1	předplatné
na	na	k7c6	na
MHD	MHD	kA	MHD
a	a	k8xC	a
za	za	k7c4	za
svoz	svoz	k1gInSc4	svoz
odpadu	odpad	k1gInSc2	odpad
a	a	k8xC	a
slevy	sleva	k1gFnSc2	sleva
v	v	k7c6	v
městských	městský	k2eAgNnPc6d1	Městské
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
dálková	dálkový	k2eAgFnSc1d1	dálková
cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
č.	č.	k?	č.
19	[number]	k4	19
(	(	kIx(	(
<g/>
souběžně	souběžně	k6eAd1	souběžně
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
a	a	k8xC	a
cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
č.	č.	k?	č.
103	[number]	k4	103
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
35314	[number]	k4	35314
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
na	na	k7c4	na
Tři	tři	k4xCgFnPc4	tři
Studně	studně	k1gFnPc4	studně
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Katolický	katolický	k2eAgInSc1d1	katolický
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
na	na	k7c6	na
Vratislavově	Vratislavův	k2eAgNnSc6d1	Vratislavovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sgrafitovou	sgrafitový	k2eAgFnSc7d1	sgrafitová
výzdobou	výzdoba	k1gFnSc7	výzdoba
(	(	kIx(	(
<g/>
jedoucí	jedoucí	k2eAgMnPc1d1	jedoucí
Sv.	sv.	kA	sv.
Václav	Václav	k1gMnSc1	Václav
<g/>
)	)	kIx)	)
od	od	k7c2	od
akademického	akademický	k2eAgMnSc2d1	akademický
malíře	malíř	k1gMnSc2	malíř
Karla	Karel	k1gMnSc2	Karel
Němce	Němec	k1gMnSc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1596	[number]	k4	1596
<g/>
.	.	kIx.	.
</s>
<s>
Novorenesanční	novorenesanční	k2eAgInSc1d1	novorenesanční
evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
kříže	kříž	k1gInPc1	kříž
postaveny	postaven	k2eAgInPc1d1	postaven
z	z	k7c2	z
vděčnosti	vděčnost	k1gFnSc2	vděčnost
za	za	k7c4	za
záchranu	záchrana	k1gFnSc4	záchrana
před	před	k7c7	před
morovou	morový	k2eAgFnSc7d1	morová
epidemií	epidemie	k1gFnSc7	epidemie
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
nezemřel	zemřít	k5eNaPmAgMnS	zemřít
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
(	(	kIx(	(
<g/>
675	[number]	k4	675
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
zalesněného	zalesněný	k2eAgInSc2d1	zalesněný
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Novoměstský	novoměstský	k2eAgInSc1d1	novoměstský
zámek	zámek	k1gInSc1	zámek
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
-	-	kIx~	-
Původní	původní	k2eAgFnSc4d1	původní
novoměstskou	novoměstský	k2eAgFnSc4d1	Novoměstská
radnici	radnice	k1gFnSc4	radnice
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
Vratislavově	Vratislavův	k2eAgNnSc6d1	Vratislavovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1555	[number]	k4	1555
přestavbou	přestavba	k1gFnSc7	přestavba
původních	původní	k2eAgInPc2d1	původní
dvou	dva	k4xCgInPc2	dva
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1723	[number]	k4	1723
ale	ale	k8xC	ale
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
a	a	k8xC	a
při	při	k7c6	při
následné	následný	k2eAgFnSc6d1	následná
obnově	obnova	k1gFnSc6	obnova
přibylo	přibýt	k5eAaPmAgNnS	přibýt
další	další	k2eAgNnSc1d1	další
poschodí	poschodí	k1gNnSc1	poschodí
a	a	k8xC	a
také	také	k9	také
věžička	věžička	k1gFnSc1	věžička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
místním	místní	k2eAgMnSc7d1	místní
rodákem	rodák	k1gMnSc7	rodák
Karlem	Karel	k1gMnSc7	Karel
Němcem	Němec	k1gMnSc7	Němec
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
sgrafitovou	sgrafitový	k2eAgFnSc4d1	sgrafitová
výzdoba	výzdoba	k1gFnSc1	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
umístěno	umístěn	k2eAgNnSc1d1	umístěno
Horácké	horácký	k2eAgNnSc1d1	Horácké
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
121	[number]	k4	121
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1608	[number]	k4	1608
zřízena	zřízen	k2eAgFnSc1d1	zřízena
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
13	[number]	k4	13
rodiny	rodina	k1gFnSc2	rodina
Bradyových	Bradyová	k1gFnPc2	Bradyová
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
připomínkou	připomínka	k1gFnSc7	připomínka
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
a	a	k8xC	a
Holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
97	[number]	k4	97
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
sídlí	sídlet	k5eAaImIp3nS	sídlet
městské	městský	k2eAgNnSc1d1	Městské
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
103	[number]	k4	103
s	s	k7c7	s
renesančními	renesanční	k2eAgInPc7d1	renesanční
základy	základ	k1gInPc7	základ
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sloužil	sloužit	k5eAaImAgInS	sloužit
zpočátku	zpočátku	k6eAd1	zpočátku
jako	jako	k9	jako
dům	dům	k1gInSc1	dům
právovárečný	právovárečný	k2eAgInSc1d1	právovárečný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
7	[number]	k4	7
-	-	kIx~	-
hotel	hotel	k1gInSc4	hotel
Panský	panský	k2eAgInSc4d1	panský
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
124	[number]	k4	124
-	-	kIx~	-
budova	budova	k1gFnSc1	budova
I.	I.	kA	I.
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
11	[number]	k4	11
-	-	kIx~	-
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
Vincence	Vincenc	k1gMnSc2	Vincenc
Makovského	Makovský	k1gMnSc2	Makovský
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
1439	[number]	k4	1439
-	-	kIx~	-
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
Tobiáše	Tobiáš	k1gMnSc2	Tobiáš
Kamenického	Kamenický	k1gMnSc2	Kamenický
CHKO	CHKO	kA	CHKO
Žďárské	Žďárské	k2eAgInPc1d1	Žďárské
vrchy	vrch	k1gInPc1	vrch
-	-	kIx~	-
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
je	být	k5eAaImIp3nS	být
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
do	do	k7c2	do
chráněné	chráněný	k2eAgFnSc2d1	chráněná
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Pernovka	Pernovka	k1gFnSc1	Pernovka
-	-	kIx~	-
rašelinná	rašelinný	k2eAgFnSc1d1	rašelinná
louka	louka	k1gFnSc1	louka
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
U	u	k7c2	u
Bezděkova	Bezděkův	k2eAgInSc2d1	Bezděkův
-	-	kIx~	-
lokalita	lokalita	k1gFnSc1	lokalita
šafránu	šafrán	k1gInSc2	šafrán
bělokvětého	bělokvětý	k2eAgInSc2d1	bělokvětý
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Blažek	Blažek	k1gMnSc1	Blažek
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
teoretik	teoretik	k1gMnSc1	teoretik
Oldřich	Oldřich	k1gMnSc1	Oldřich
Blažíček	Blažíček	k1gMnSc1	Blažíček
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
<g />
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
Brady	brada	k1gFnSc2	brada
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
židovská	židovský	k2eAgFnSc1d1	židovská
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahynula	zahynout	k5eAaPmAgFnS	zahynout
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
židovské	židovský	k2eAgFnSc2d1	židovská
perzekuce	perzekuce	k1gFnSc2	perzekuce
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Leander	Leander	k1gMnSc1	Leander
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnSc1d1	národní
buditel	buditel	k1gMnSc1	buditel
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
středoškolský	středoškolský	k2eAgMnSc1d1	středoškolský
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
Daniel	Daniel	k1gMnSc1	Daniel
Dítě	Dítě	k1gMnSc1	Dítě
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
dabér	dabér	k1gMnSc1	dabér
a	a	k8xC	a
dabingový	dabingový	k2eAgMnSc1d1	dabingový
režisér	režisér	k1gMnSc1	režisér
Věra	Věra	k1gFnSc1	Věra
Frömlová-Zezuláková	Frömlová-Zezuláková	k1gFnSc1	Frömlová-Zezuláková
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malířka	malířka	k1gFnSc1	malířka
a	a	k8xC	a
restaurátorka	restaurátorka	k1gFnSc1	restaurátorka
Jan	Jan	k1gMnSc1	Jan
Hlaváček	Hlaváček	k1gMnSc1	Hlaváček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plukovník	plukovník	k1gMnSc1	plukovník
Alois	Alois	k1gMnSc1	Alois
Chocholáč	chocholáč	k1gMnSc1	chocholáč
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
umění	umění	k1gNnSc4	umění
samorostů	samorost	k1gInPc2	samorost
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Jambor	Jambor	k1gMnSc1	Jambor
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
Radek	Radek	k1gMnSc1	Radek
Jaroš	Jaroš	k1gMnSc1	Jaroš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
horolezec	horolezec	k1gMnSc1	horolezec
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kallab	Kallab	k1gMnSc1	Kallab
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
ředitelem	ředitel	k1gMnSc7	ředitel
kriminologického	kriminologický	k2eAgInSc2d1	kriminologický
ústavu	ústav	k1gInSc2	ústav
Karel	Karel	k1gMnSc1	Karel
Kalláb	Kalláb	k1gMnSc1	Kalláb
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jan	Jan	k1gMnSc1	Jan
Kasal	Kasal	k1gMnSc1	Kasal
(	(	kIx(	(
<g/>
*	*	kIx~	*
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
čelný	čelný	k2eAgMnSc1d1	čelný
představitel	představitel	k1gMnSc1	představitel
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
Miloš	Miloš	k1gMnSc1	Miloš
Konvalinka	Konvalinka	k1gMnSc1	Konvalinka
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Pavel	Pavel	k1gMnSc1	Pavel
Kopáček	Kopáček	k1gMnSc1	Kopáček
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
krajinář	krajinář	k1gMnSc1	krajinář
Jindřich	Jindřich	k1gMnSc1	Jindřich
Mahelka	Mahelka	k1gMnSc1	Mahelka
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
Jiří	Jiří	k1gMnSc1	Jiří
Macháček	Macháček	k1gMnSc1	Macháček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
archeolog	archeolog	k1gMnSc1	archeolog
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
archeologii	archeologie	k1gFnSc4	archeologie
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
archeologii	archeologie	k1gFnSc6	archeologie
<g/>
.	.	kIx.	.
</s>
<s>
Vincenc	Vincenc	k1gMnSc1	Vincenc
Makovský	Makovský	k1gMnSc1	Makovský
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
Josef	Josef	k1gMnSc1	Josef
Vratislav	Vratislav	k1gMnSc1	Vratislav
Monse	Monse	k1gFnSc1	Monse
(	(	kIx(	(
<g/>
1733	[number]	k4	1733
<g/>
-	-	kIx~	-
<g/>
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnSc1d1	národní
buditel	buditel	k1gMnSc1	buditel
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
univerzity	univerzita	k1gFnSc2	univerzita
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nečas	Nečas	k1gMnSc1	Nečas
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
Josef	Josef	k1gMnSc1	Josef
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
<g/>
,	,	kIx,	,
poslancem	poslanec	k1gMnSc7	poslanec
za	za	k7c4	za
VV	VV	kA	VV
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Nečas	Nečas	k1gMnSc1	Nečas
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
Karel	Karel	k1gMnSc1	Karel
Němec	Němec	k1gMnSc1	Němec
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
<g/>
,	,	kIx,	,
poslankyně	poslankyně	k1gFnSc1	poslankyně
za	za	k7c4	za
ODS	ODS	kA	ODS
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
operní	operní	k2eAgMnSc1d1	operní
a	a	k8xC	a
operetní	operetní	k2eAgMnSc1d1	operetní
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
tenor	tenor	k1gInSc1	tenor
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Martina	Martina	k1gFnSc1	Martina
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
Ivan	Ivan	k1gMnSc1	Ivan
Sekanina	Sekanina	k1gMnSc1	Sekanina
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
Ivo	Ivo	k1gMnSc1	Ivo
Strejček	Strejček	k1gMnSc1	Strejček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poslanec	poslanec	k1gMnSc1	poslanec
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
ODS	ODS	kA	ODS
Karel	Karel	k1gMnSc1	Karel
Daněk	Daněk	k1gMnSc1	Daněk
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
několika	několik	k4yIc2	několik
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
lyžování	lyžování	k1gNnSc6	lyžování
a	a	k8xC	a
zdravém	zdravý	k2eAgInSc6d1	zdravý
životním	životní	k2eAgInSc6d1	životní
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
sběratel	sběratel	k1gMnSc1	sběratel
básní	báseň	k1gFnPc2	báseň
Jan	Jan	k1gMnSc1	Jan
Svítil-Karník	Svítil-Karník	k1gMnSc1	Svítil-Karník
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všestranný	všestranný	k2eAgMnSc1d1	všestranný
vlastivědný	vlastivědný	k2eAgMnSc1d1	vlastivědný
a	a	k8xC	a
národopisný	národopisný	k2eAgMnSc1d1	národopisný
pracovník	pracovník	k1gMnSc1	pracovník
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
a	a	k8xC	a
muzeolog	muzeolog	k1gMnSc1	muzeolog
Jan	Jan	k1gMnSc1	Jan
Štursa	Štursa	k1gFnSc1	Štursa
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
Josef	Josef	k1gMnSc1	Josef
Veselka	Veselka	k1gMnSc1	Veselka
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbormistr	sbormistr	k1gMnSc1	sbormistr
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
Eva	Eva	k1gFnSc1	Eva
Vítečková	Vítečková	k1gFnSc1	Vítečková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
basketbalistka	basketbalistka	k1gFnSc1	basketbalistka
a	a	k8xC	a
vicemistryně	vicemistryně	k1gFnSc1	vicemistryně
světa	svět	k1gInSc2	svět
Východně	východně	k6eAd1	východně
od	od	k7c2	od
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
vesnice	vesnice	k1gFnSc1	vesnice
Mnichov	Mnichov	k1gInSc1	Mnichov
nebo	nebo	k8xC	nebo
též	též	k9	též
Michovy	Michova	k1gFnSc2	Michova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
ji	on	k3xPp3gFnSc4	on
připomínají	připomínat	k5eAaImIp3nP	připomínat
pouze	pouze	k6eAd1	pouze
místní	místní	k2eAgInPc1d1	místní
názvy	název	k1gInPc1	název
Horní	horní	k2eAgInPc1d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgInPc1d1	dolní
Michovy	Michov	k1gInPc1	Michov
a	a	k8xC	a
Michovské	Michovský	k2eAgFnPc1d1	Michovská
hory	hora	k1gFnPc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
osadě	osada	k1gFnSc6	osada
Mnichov	Mnichov	k1gInSc1	Mnichov
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
zmiňováni	zmiňován	k2eAgMnPc1d1	zmiňován
Mikuláš	mikuláš	k1gInSc4	mikuláš
Vicher	Vicher	k?	Vicher
a	a	k8xC	a
Huňatý	huňatý	k2eAgInSc1d1	huňatý
z	z	k7c2	z
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
patřila	patřit	k5eAaImAgFnS	patřit
osada	osada	k1gFnSc1	osada
pánům	pan	k1gMnPc3	pan
z	z	k7c2	z
Lipého	Lipé	k1gNnSc2	Lipé
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1496	[number]	k4	1496
prodali	prodat	k5eAaPmAgMnP	prodat
Vilémovi	Vilémův	k2eAgMnPc1d1	Vilémův
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
začíná	začínat	k5eAaImIp3nS	začínat
postupné	postupný	k2eAgNnSc4d1	postupné
skupování	skupování	k1gNnSc4	skupování
pozemků	pozemek	k1gInPc2	pozemek
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
obyvateli	obyvatel	k1gMnPc7	obyvatel
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1540	[number]	k4	1540
byla	být	k5eAaImAgFnS	být
vesnice	vesnice	k1gFnSc1	vesnice
Janem	Jan	k1gMnSc7	Jan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
prodána	prodat	k5eAaPmNgFnS	prodat
Novému	nový	k2eAgNnSc3d1	nové
Městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1580	[number]	k4	1580
ji	on	k3xPp3gFnSc4	on
Vratislav	Vratislav	k1gFnSc4	Vratislav
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
uznává	uznávat	k5eAaImIp3nS	uznávat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
městečka	městečko	k1gNnSc2	městečko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
7	[number]	k4	7
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
osada	osada	k1gFnSc1	osada
již	již	k6eAd1	již
vylidněná	vylidněný	k2eAgFnSc1d1	vylidněná
a	a	k8xC	a
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kupní	kupní	k2eAgFnSc6d1	kupní
smlouvě	smlouva	k1gFnSc6	smlouva
uváděna	uváděn	k2eAgNnPc1d1	uváděno
jako	jako	k8xC	jako
zcela	zcela	k6eAd1	zcela
pustá	pustý	k2eAgFnSc1d1	pustá
<g/>
.	.	kIx.	.
</s>
<s>
Mnichov	Mnichov	k1gInSc1	Mnichov
možno	možno	k6eAd1	možno
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
severně	severně	k6eAd1	severně
od	od	k7c2	od
kopce	kopec	k1gInSc2	kopec
Kalvárie	Kalvárie	k1gFnSc2	Kalvárie
na	na	k7c4	na
horní	horní	k2eAgInSc4d1	horní
okraj	okraj	k1gInSc4	okraj
pramenného	pramenný	k2eAgInSc2d1	pramenný
zářezu	zářez	k1gInSc2	zářez
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
silnice	silnice	k1gFnSc2	silnice
č.	č.	k?	č.
19	[number]	k4	19
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
k	k	k7c3	k
Bystřici	Bystřice	k1gFnSc3	Bystřice
nad	nad	k7c7	nad
Pernštejnem	Pernštejn	k1gInSc7	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Waalre	Waalr	k1gMnSc5	Waalr
Ziano	Ziano	k1gNnSc4	Ziano
di	di	k?	di
Fiemme	Fiemme	k1gFnPc2	Fiemme
Nová	Nová	k1gFnSc1	Nová
Města	město	k1gNnSc2	město
Evropy	Evropa	k1gFnSc2	Evropa
</s>
