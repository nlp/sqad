<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Japonska	Japonsko	k1gNnSc2	Japonsko
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
nazývá	nazývat	k5eAaImIp3nS	nazývat
Niššóki	Niššóke	k1gFnSc4	Niššóke
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
日	日	k?	日
<g/>
;	;	kIx,	;
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Hinomaru	Hinomar	k1gInSc2	Hinomar
(	(	kIx(	(
<g/>
日	日	k?	日
<g/>
;	;	kIx,	;
Sluneční	sluneční	k2eAgInSc1d1	sluneční
kotouč	kotouč	k1gInSc1	kotouč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
šóguny	šógun	k1gMnPc7	šógun
rodu	rod	k1gInSc2	rod
Tokugawa	Tokugaw	k1gInSc2	Tokugaw
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
japonskou	japonský	k2eAgFnSc7d1	japonská
námořní	námořní	k2eAgFnSc7d1	námořní
vlajkou	vlajka	k1gFnSc7	vlajka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
i	i	k9	i
vlajkou	vlajka	k1gFnSc7	vlajka
státní	státní	k2eAgFnSc7d1	státní
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
bílým	bílý	k2eAgInSc7d1	bílý
listem	list	k1gInSc7	list
s	s	k7c7	s
červeným	červený	k2eAgNnSc7d1	červené
kruhovým	kruhový	k2eAgNnSc7d1	kruhové
polem	pole	k1gNnSc7	pole
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
vlajky	vlajka	k1gFnSc2	vlajka
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
čistotu	čistota	k1gFnSc4	čistota
a	a	k8xC	a
poctivost	poctivost	k1gFnSc4	poctivost
<g/>
,	,	kIx,	,
červená	červenat	k5eAaImIp3nS	červenat
pak	pak	k6eAd1	pak
náruživost	náruživost	k1gFnSc4	náruživost
<g/>
,	,	kIx,	,
upřímnost	upřímnost	k1gFnSc4	upřímnost
a	a	k8xC	a
nadšení	nadšení	k1gNnSc4	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
kotouč	kotouč	k1gInSc1	kotouč
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
je	být	k5eAaImIp3nS	být
Japonsko	Japonsko	k1gNnSc1	Japonsko
známé	známá	k1gFnSc2	známá
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
pojmenováni	pojmenovat	k5eAaPmNgMnP	pojmenovat
Marcem	Marce	k1gMnSc7	Marce
Polem	polem	k6eAd1	polem
jako	jako	k9	jako
Zipangu	Zipang	k1gInSc2	Zipang
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
exonymum	exonymum	k1gNnSc4	exonymum
čínského	čínský	k2eAgNnSc2d1	čínské
Ž	Ž	kA	Ž
<g/>
'	'	kIx"	'
<g/>
-pen-kuo	enuo	k1gMnSc1	-pen-kuo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Země	zem	k1gFnSc2	zem
vycházejícího	vycházející	k2eAgNnSc2d1	vycházející
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
<g/>
Japonská	japonský	k2eAgFnSc1d1	japonská
válečná	válečný	k2eAgFnSc1d1	válečná
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
vlajkou	vlajka	k1gFnSc7	vlajka
Japonských	japonský	k2eAgFnPc2d1	japonská
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sebeobranných	sebeobranný	k2eAgFnPc2d1	sebeobranná
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
námořní	námořní	k2eAgFnSc1d1	námořní
válečná	válečný	k2eAgFnSc1d1	válečná
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
vlajkou	vlajka	k1gFnSc7	vlajka
Japonských	japonský	k2eAgFnPc2d1	japonská
námořních	námořní	k2eAgFnPc2d1	námořní
sil	síla	k1gFnPc2	síla
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
od	od	k7c2	od
jejich	jejich	k3xOp3gNnSc2	jejich
založení	založení	k1gNnSc2	založení
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
<g/>
Japonská	japonský	k2eAgFnSc1d1	japonská
císařská	císařský	k2eAgFnSc1d1	císařská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
se	s	k7c7	s
stylizovaným	stylizovaný	k2eAgInSc7d1	stylizovaný
listem	list	k1gInSc7	list
chryzantémy	chryzantéma	k1gFnSc2	chryzantéma
se	s	k7c7	s
šestnácti	šestnáct	k4xCc7	šestnáct
lístky	lístek	k1gInPc7	lístek
ve	v	k7c6	v
zlaté	zlatý	k2eAgFnSc6d1	zlatá
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1926	[number]	k4	1926
japonským	japonský	k2eAgMnPc3d1	japonský
císařským	císařský	k2eAgMnPc3d1	císařský
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
vlajku	vlajka	k1gFnSc4	vlajka
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
emeritní	emeritní	k2eAgMnSc1d1	emeritní
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
vlajce	vlajka	k1gFnSc3	vlajka
panujícího	panující	k2eAgMnSc2d1	panující
císaře	císař	k1gMnSc2	císař
má	mít	k5eAaImIp3nS	mít
vlajka	vlajka	k1gFnSc1	vlajka
tmavý	tmavý	k2eAgInSc1d1	tmavý
odstín	odstín	k1gInSc1	odstín
červené	červená	k1gFnSc2	červená
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
abdikaci	abdikace	k1gFnSc6	abdikace
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
užívána	užívat	k5eAaImNgFnS	užívat
i	i	k9	i
bývalým	bývalý	k2eAgMnSc7d1	bývalý
císařem	císař	k1gMnSc7	císař
Akihitem	Akihit	k1gMnSc7	Akihit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
japonským	japonský	k2eAgMnSc7d1	japonský
císařem	císař	k1gMnSc7	císař
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
japonské	japonský	k2eAgFnSc2d1	japonská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
660	[number]	k4	660
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
legendární	legendární	k2eAgMnSc1d1	legendární
císař	císař	k1gMnSc1	císař
Džimmu	Džimm	k1gInSc2	Džimm
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
prvního	první	k4xOgInSc2	první
skutečného	skutečný	k2eAgInSc2d1	skutečný
státu	stát	k1gInSc2	stát
na	na	k7c6	na
Japonském	japonský	k2eAgNnSc6d1	Japonské
souostroví	souostroví	k1gNnSc6	souostroví
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c4	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
vznikem	vznik	k1gInSc7	vznik
Státu	stát	k1gInSc3	stát
Jamoto	Jamota	k1gFnSc5	Jamota
<g/>
.	.	kIx.	.
</s>
<s>
Japonští	japonský	k2eAgMnPc1d1	japonský
císaři	císař	k1gMnPc1	císař
měli	mít	k5eAaImAgMnP	mít
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pouze	pouze	k6eAd1	pouze
formální	formální	k2eAgFnSc1d1	formální
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
skutečná	skutečný	k2eAgFnSc1d1	skutečná
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
vojenských	vojenský	k2eAgMnPc2d1	vojenský
správců	správce	k1gMnPc2	správce
(	(	kIx(	(
<g/>
Šógunů	Šógun	k1gMnPc2	Šógun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
začali	začít	k5eAaPmAgMnP	začít
šóguni	šógun	k1gMnPc1	šógun
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Tokugawa	Tokugaw	k1gInSc2	Tokugaw
užívat	užívat	k5eAaImF	užívat
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
válečných	válečný	k2eAgFnPc6d1	válečná
lodích	loď	k1gFnPc6	loď
bílé	bílý	k2eAgInPc1d1	bílý
prapory	prapor	k1gInPc1	prapor
s	s	k7c7	s
červeným	červený	k2eAgNnSc7d1	červené
kruhovým	kruhový	k2eAgNnSc7d1	kruhové
polem	pole	k1gNnSc7	pole
<g/>
,	,	kIx,	,
základem	základ	k1gInSc7	základ
dnešní	dnešní	k2eAgFnSc2d1	dnešní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1542	[number]	k4	1542
přistála	přistát	k5eAaPmAgFnS	přistát
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
Japonska	Japonsko	k1gNnSc2	Japonsko
první	první	k4xOgFnSc7	první
evropská	evropský	k2eAgFnSc1d1	Evropská
(	(	kIx(	(
<g/>
portugalská	portugalský	k2eAgFnSc1d1	portugalská
<g/>
)	)	kIx)	)
loď	loď	k1gFnSc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
do	do	k7c2	do
země	zem	k1gFnSc2	zem
začali	začít	k5eAaPmAgMnP	začít
pronikat	pronikat	k5eAaImF	pronikat
obchodníci	obchodník	k1gMnPc1	obchodník
a	a	k8xC	a
misionáři	misionář	k1gMnPc1	misionář
i	i	k9	i
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
či	či	k8xC	či
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
vzrůstaly	vzrůstat	k5eAaImAgFnP	vzrůstat
obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
vlivu	vliv	k1gInSc2	vliv
evropských	evropský	k2eAgFnPc2d1	Evropská
námořních	námořní	k2eAgFnPc2d1	námořní
mocností	mocnost	k1gFnPc2	mocnost
<g/>
,	,	kIx,	,
přistoupil	přistoupit	k5eAaPmAgInS	přistoupit
Šógunát	Šógunát	k1gInSc1	Šógunát
Tokugawa	Tokugaw	k1gInSc2	Tokugaw
k	k	k7c3	k
izolaci	izolace	k1gFnSc3	izolace
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
let	léto	k1gNnPc2	léto
proto	proto	k8xC	proto
Japonsko	Japonsko	k1gNnSc1	Japonsko
nepotřebovalo	potřebovat	k5eNaImAgNnS	potřebovat
a	a	k8xC	a
ani	ani	k8xC	ani
neužívalo	užívat	k5eNaImAgNnS	užívat
žádnou	žádný	k3yNgFnSc4	žádný
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
obnovené	obnovený	k2eAgInPc1d1	obnovený
kontakty	kontakt	k1gInPc1	kontakt
se	s	k7c7	s
západem	západ	k1gInSc7	západ
potřebu	potřeba	k1gFnSc4	potřeba
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1853	[number]	k4	1853
<g/>
,	,	kIx,	,
při	při	k7c6	při
předávání	předávání	k1gNnSc6	předávání
poselství	poselství	k1gNnSc2	poselství
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
americkým	americký	k2eAgMnSc7d1	americký
komodorem	komodor	k1gMnSc7	komodor
Matthewem	Matthew	k1gMnSc7	Matthew
Calbraithem	Calbraith	k1gInSc7	Calbraith
Perrym	Perrym	k1gInSc1	Perrym
šógunovi	šógun	k1gMnSc3	šógun
Tokugawu	Tokugaw	k1gMnSc3	Tokugaw
Iejošimu	Iejošim	k1gMnSc3	Iejošim
byla	být	k5eAaImAgFnS	být
např.	např.	kA	např.
přítomna	přítomno	k1gNnSc2	přítomno
pouze	pouze	k6eAd1	pouze
vlajka	vlajka	k1gFnSc1	vlajka
USA	USA	kA	USA
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
japonská	japonský	k2eAgFnSc1d1	japonská
vlajka	vlajka	k1gFnSc1	vlajka
chyběla	chybět	k5eAaImAgFnS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
novou	nový	k2eAgFnSc4d1	nová
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
nařízením	nařízení	k1gNnSc7	nařízení
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1854	[number]	k4	1854
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zasadila	zasadit	k5eAaPmAgFnS	zasadit
především	především	k9	především
skupina	skupina	k1gFnSc1	skupina
Japonců	Japonec	k1gMnPc2	Japonec
vedená	vedený	k2eAgFnSc1d1	vedená
knížetem	kníže	k1gMnSc7	kníže
Nariakirou	Nariakira	k1gMnSc7	Nariakira
<g/>
.	.	kIx.	.
</s>
<s>
Schválená	schválený	k2eAgFnSc1d1	schválená
japonská	japonský	k2eAgFnSc1d1	japonská
námořní	námořní	k2eAgFnSc1d1	námořní
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
bílým	bílý	k2eAgInSc7d1	bílý
listem	list	k1gInSc7	list
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
slunečním	sluneční	k2eAgInSc7d1	sluneční
kotoučem	kotouč	k1gInSc7	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Nařízeními	nařízení	k1gNnPc7	nařízení
č.	č.	k?	č.
57	[number]	k4	57
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
a	a	k8xC	a
č.	č.	k?	č.
651	[number]	k4	651
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
se	se	k3xPyFc4	se
vlajka	vlajka	k1gFnSc1	vlajka
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
vlajkou	vlajka	k1gFnSc7	vlajka
státní	státní	k2eAgFnSc7d1	státní
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
nařízení	nařízení	k1gNnSc1	nařízení
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgFnPc4d1	obchodní
vlajky	vlajka	k1gFnPc4	vlajka
a	a	k8xC	a
stanovilo	stanovit	k5eAaPmAgNnS	stanovit
poměr	poměr	k1gInSc4	poměr
stran	stran	k7c2	stran
vlajky	vlajka	k1gFnSc2	vlajka
na	na	k7c4	na
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Stanovilo	stanovit	k5eAaPmAgNnS	stanovit
také	také	k9	také
průměr	průměr	k1gInSc4	průměr
kotouče	kotouč	k1gInSc2	kotouč
na	na	k7c4	na
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
posunut	posunout	k5eAaPmNgInS	posunout
o	o	k7c4	o
1	[number]	k4	1
<g/>
%	%	kIx~	%
k	k	k7c3	k
žerďovému	žerďový	k2eAgInSc3d1	žerďový
okraji	okraj	k1gInSc3	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
nařízení	nařízení	k1gNnSc1	nařízení
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
námořní	námořní	k2eAgFnSc4d1	námořní
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Stanovilo	stanovit	k5eAaPmAgNnS	stanovit
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
na	na	k7c4	na
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
kotouče	kotouč	k1gInSc2	kotouč
byl	být	k5eAaImAgInS	být
ponechán	ponechat	k5eAaPmNgInS	ponechat
a	a	k8xC	a
kotouč	kotouč	k1gInSc1	kotouč
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
středu	střed	k1gInSc2	střed
vlajkového	vlajkový	k2eAgInSc2d1	vlajkový
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
nebylo	být	k5eNaImAgNnS	být
přesně	přesně	k6eAd1	přesně
stanoveno	stanoven	k2eAgNnSc1d1	stanoveno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nařízení	nařízení	k1gNnSc1	nařízení
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
<g/>
Vlajka	vlajka	k1gFnSc1	vlajka
Japonského	japonský	k2eAgNnSc2d1	Japonské
císařského	císařský	k2eAgNnSc2d1	císařské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
v	v	k7c6	v
letech	let	k1gInPc6	let
1889	[number]	k4	1889
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xC	jako
současná	současný	k2eAgFnSc1d1	současná
vlajka	vlajka	k1gFnSc1	vlajka
Japonských	japonský	k2eAgFnPc2d1	japonská
námořních	námořní	k2eAgFnPc2d1	námořní
sil	síla	k1gFnPc2	síla
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
pouze	pouze	k6eAd1	pouze
odlišný	odlišný	k2eAgInSc4d1	odlišný
odstín	odstín	k1gInSc4	odstín
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Japonské	japonský	k2eAgFnSc2d1	japonská
císařské	císařský	k2eAgFnSc2d1	císařská
armády	armáda	k1gFnSc2	armáda
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vlajky	vlajka	k1gFnSc2	vlajka
císařského	císařský	k2eAgNnSc2d1	císařské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
střed	střed	k1gInSc4	střed
slunce	slunce	k1gNnSc2	slunce
uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Užívání	užívání	k1gNnSc1	užívání
japonské	japonský	k2eAgFnSc2d1	japonská
vlajky	vlajka	k1gFnSc2	vlajka
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1945	[number]	k4	1945
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1952	[number]	k4	1952
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
porážky	porážka	k1gFnSc2	porážka
ve	v	k7c6	v
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
nemohlo	moct	k5eNaImAgNnS	moct
Japonsko	Japonsko	k1gNnSc1	Japonsko
užívat	užívat	k5eAaImF	užívat
a	a	k8xC	a
jako	jako	k9	jako
obchodní	obchodní	k2eAgFnSc4d1	obchodní
vlajku	vlajka	k1gFnSc4	vlajka
místo	místo	k7c2	místo
toho	ten	k3xDgMnSc2	ten
užívalo	užívat	k5eAaImAgNnS	užívat
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
lodích	loď	k1gFnPc6	loď
modročervenou	modročervený	k2eAgFnSc4d1	modročervená
vlajku	vlajka	k1gFnSc4	vlajka
odvozenou	odvozený	k2eAgFnSc4d1	odvozená
z	z	k7c2	z
námořní	námořní	k2eAgFnSc2d1	námořní
vlakové	vlakový	k2eAgFnSc2d1	vlaková
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
písmene	písmeno	k1gNnSc2	písmeno
E	E	kA	E
však	však	k8xC	však
měla	mít	k5eAaImAgFnS	mít
dva	dva	k4xCgInPc4	dva
cípy	cíp	k1gInPc4	cíp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
nařízení	nařízení	k1gNnSc1	nařízení
č.	č.	k?	č.
127	[number]	k4	127
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
a	a	k8xC	a
hymně	hymna	k1gFnSc6	hymna
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyřešilo	vyřešit	k5eAaPmAgNnS	vyřešit
rozpory	rozpor	k1gInPc4	rozpor
v	v	k7c6	v
rozměrech	rozměr	k1gInPc6	rozměr
vlajky	vlajka	k1gFnPc1	vlajka
a	a	k8xC	a
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
druhé	druhý	k4xOgNnSc1	druhý
nařízení	nařízení	k1gNnSc1	nařízení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajka	vlajka	k1gFnSc1	vlajka
Rjúkjúského	rjúkjúský	k2eAgNnSc2d1	rjúkjúský
království	království	k1gNnSc2	království
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
existovalo	existovat	k5eAaImAgNnS	existovat
na	na	k7c6	na
souostroví	souostroví	k1gNnSc6	souostroví
Rjúkjú	Rjúkjú	k1gNnSc2	Rjúkjú
<g/>
,	,	kIx,	,
skupině	skupina	k1gFnSc3	skupina
malých	malý	k2eAgInPc2d1	malý
ostrovů	ostrov	k1gInPc2	ostrov
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
součástí	součást	k1gFnPc2	součást
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
Rjúkjúské	rjúkjúský	k2eAgNnSc1d1	rjúkjúský
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1609	[number]	k4	1609
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
souostroví	souostroví	k1gNnSc1	souostroví
součástí	součást	k1gFnPc2	součást
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
autonomie	autonomie	k1gFnSc1	autonomie
mu	on	k3xPp3gMnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
užívat	užívat	k5eAaImF	užívat
svou	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
tvořil	tvořit	k5eAaImAgInS	tvořit
list	list	k1gInSc1	list
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
bílým	bílý	k1gMnSc7	bílý
<g/>
,	,	kIx,	,
černým	černý	k2eAgMnSc7d1	černý
<g/>
,	,	kIx,	,
červeným	červený	k2eAgNnSc7d1	červené
a	a	k8xC	a
černým	černý	k2eAgNnSc7d1	černé
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
byl	být	k5eAaImAgInS	být
kulatý	kulatý	k2eAgInSc1d1	kulatý
černo-modrý	černoodrý	k2eAgInSc1d1	černo-modrý
znak	znak	k1gInSc1	znak
rodu	rod	k1gInSc2	rod
Šó	Šó	k1gFnPc2	Šó
(	(	kIx(	(
<g/>
připomínající	připomínající	k2eAgInSc1d1	připomínající
vír	vír	k1gInSc1	vír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazená	zobrazený	k2eAgFnSc1d1	zobrazená
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
vír	vír	k1gInSc4	vír
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
než	než	k8xS	než
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
změněna	změnit	k5eAaPmNgFnS	změnit
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
japonské	japonský	k2eAgFnSc2d1	japonská
vlajky	vlajka	k1gFnSc2	vlajka
na	na	k7c4	na
bílý	bílý	k2eAgInSc4d1	bílý
list	list	k1gInSc4	list
s	s	k7c7	s
uprostřed	uprostřed	k6eAd1	uprostřed
umístěným	umístěný	k2eAgInSc7d1	umístěný
tříbarevným	tříbarevný	k2eAgInSc7d1	tříbarevný
znakem	znak	k1gInSc7	znak
panovnického	panovnický	k2eAgInSc2d1	panovnický
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc4	znak
symbolizoval	symbolizovat	k5eAaImAgMnS	symbolizovat
tři	tři	k4xCgFnPc4	tři
ctnosti	ctnost	k1gFnPc4	ctnost
<g/>
:	:	kIx,	:
krásu	krása	k1gFnSc4	krása
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
bližnímu	bližní	k1gMnSc3	bližní
(	(	kIx(	(
<g/>
červená	červená	k1gFnSc1	červená
<g/>
)	)	kIx)	)
a	a	k8xC	a
něžnost	něžnost	k1gFnSc1	něžnost
(	(	kIx(	(
<g/>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
pýchu	pýcha	k1gFnSc4	pýcha
na	na	k7c4	na
neposkvrněnost	neposkvrněnost	k1gFnSc4	neposkvrněnost
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1879	[number]	k4	1879
byl	být	k5eAaImAgMnS	být
poslední	poslední	k2eAgMnSc1d1	poslední
král	král	k1gMnSc1	král
Šo	Šo	k1gMnSc1	Šo
Tai	Tai	k1gMnSc1	Tai
přinucen	přinutit	k5eAaPmNgMnS	přinutit
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
a	a	k8xC	a
vlajkou	vlajka	k1gFnSc7	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vlajka	vlajka	k1gFnSc1	vlajka
japonská	japonský	k2eAgFnSc1d1	japonská
<g/>
.	.	kIx.	.
<g/>
Obě	dva	k4xCgFnPc1	dva
zobrazené	zobrazený	k2eAgFnPc1d1	zobrazená
vlajky	vlajka	k1gFnPc1	vlajka
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
vlajek	vlajka	k1gFnPc2	vlajka
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
byly	být	k5eAaImAgFnP	být
zpochybněny	zpochybnit	k5eAaPmNgFnP	zpochybnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1945	[number]	k4	1945
byly	být	k5eAaImAgInP	být
ostrovy	ostrov	k1gInPc1	ostrov
obsazeny	obsazen	k2eAgInPc1d1	obsazen
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
úplného	úplný	k2eAgNnSc2d1	úplné
vrácení	vrácení	k1gNnSc2	vrácení
Japonsku	Japonsko	k1gNnSc6	Japonsko
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1972	[number]	k4	1972
byly	být	k5eAaImAgFnP	být
užívány	užívat	k5eAaImNgFnP	užívat
jejich	jejich	k3xOp3gFnPc1	jejich
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1967	[number]	k4	1967
užívaly	užívat	k5eAaImAgInP	užívat
ostrovy	ostrov	k1gInPc1	ostrov
jako	jako	k8xC	jako
svou	svůj	k3xOyFgFnSc4	svůj
obchodní	obchodní	k2eAgFnSc4d1	obchodní
vlajku	vlajka	k1gFnSc4	vlajka
žlutomodrou	žlutomodrý	k2eAgFnSc4d1	žlutomodrá
vlajku	vlajka	k1gFnSc4	vlajka
odvozenou	odvozený	k2eAgFnSc4d1	odvozená
z	z	k7c2	z
námořní	námořní	k2eAgFnSc2d1	námořní
vlakové	vlakový	k2eAgFnSc2d1	vlaková
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
písmene	písmeno	k1gNnSc2	písmeno
D	D	kA	D
však	však	k8xC	však
měla	mít	k5eAaImAgFnS	mít
dva	dva	k4xCgInPc4	dva
cípy	cíp	k1gInPc4	cíp
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1967	[number]	k4	1967
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
obchodní	obchodní	k2eAgFnSc1d1	obchodní
vlajka	vlajka	k1gFnSc1	vlajka
začala	začít	k5eAaPmAgFnS	začít
užívat	užívat	k5eAaImF	užívat
japonská	japonský	k2eAgFnSc1d1	japonská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
doplněná	doplněný	k2eAgFnSc1d1	doplněná
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
bílým	bílý	k2eAgInSc7d1	bílý
plamenem	plamen	k1gInSc7	plamen
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
japonským	japonský	k2eAgInSc7d1	japonský
nápisem	nápis	k1gInSc7	nápis
Rjúkjú	Rjúkjú	k1gNnSc2	Rjúkjú
(	(	kIx(	(
<g/>
琉	琉	k?	琉
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
<g/>
,	,	kIx,	,
červeně	červeně	k6eAd1	červeně
lemovanými	lemovaný	k2eAgNnPc7d1	lemované
písmeny	písmeno	k1gNnPc7	písmeno
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
RYUKYUS	RYUKYUS	kA	RYUKYUS
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vlajka	vlajka	k1gFnSc1	vlajka
Okinawy	Okinawa	k1gFnSc2	Okinawa
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Okinawských	okinawský	k2eAgMnPc2d1	okinawský
ostrovů	ostrov	k1gInPc2	ostrov
americkou	americký	k2eAgFnSc7d1	americká
armádou	armáda	k1gFnSc7	armáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
ostrov	ostrov	k1gInSc4	ostrov
Okinawa	Okinawa	k1gFnSc1	Okinawa
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
na	na	k7c6	na
Japonsku	Japonsko	k1gNnSc6	Japonsko
nezávislý	závislý	k2eNgInSc1d1	nezávislý
Stát	stát	k1gInSc1	stát
Okinawa	Okinawa	k1gFnSc1	Okinawa
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1950	[number]	k4	1950
okinawský	okinawský	k2eAgMnSc1d1	okinawský
guvernér	guvernér	k1gMnSc1	guvernér
Košin	košina	k1gFnPc2	košina
Šikija	Šikija	k1gMnSc1	Šikija
představil	představit	k5eAaPmAgInS	představit
vlajku	vlajka	k1gFnSc4	vlajka
tohoto	tento	k3xDgInSc2	tento
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
list	list	k1gInSc4	list
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
listu	list	k1gInSc2	list
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
v	v	k7c6	v
modrém	modrý	k2eAgInSc6d1	modrý
pruhu	pruh	k1gInSc6	pruh
<g/>
)	)	kIx)	)
umístěna	umístěn	k2eAgFnSc1d1	umístěna
bílá	bílý	k2eAgFnSc1d1	bílá
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
nejdříve	dříve	k6eAd3	dříve
nezávislost	nezávislost	k1gFnSc1	nezávislost
Okinawy	Okinawa	k1gFnSc2	Okinawa
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1950	[number]	k4	1950
tuto	tento	k3xDgFnSc4	tento
vlajku	vlajka	k1gFnSc4	vlajka
i	i	k8xC	i
samostatnost	samostatnost	k1gFnSc4	samostatnost
zamítly	zamítnout	k5eAaPmAgInP	zamítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc1	vlajka
japonských	japonský	k2eAgFnPc2d1	japonská
prefektur	prefektura	k1gFnPc2	prefektura
==	==	k?	==
</s>
</p>
<p>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
metropolitní	metropolitní	k2eAgInSc4d1	metropolitní
město	město	k1gNnSc1	město
to	ten	k3xDgNnSc4	ten
(	(	kIx(	(
<g/>
都	都	k?	都
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1	[number]	k4	1
správní	správní	k2eAgInSc1d1	správní
okruh	okruh	k1gInSc1	okruh
dó	dó	k?	dó
(	(	kIx(	(
<g/>
道	道	k?	道
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Hokkaidó	Hokkaidó	k1gFnSc1	Hokkaidó
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
městské	městský	k2eAgFnPc4d1	městská
prefektury	prefektura	k1gFnPc4	prefektura
fu	fu	k0	fu
(	(	kIx(	(
<g/>
府	府	k?	府
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Kjóto	Kjóto	k1gNnSc1	Kjóto
a	a	k8xC	a
Ósaka	Ósaka	k1gFnSc1	Ósaka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
43	[number]	k4	43
prefektur	prefektura	k1gFnPc2	prefektura
ken	ken	k?	ken
(	(	kIx(	(
<g/>
県	県	k?	県
<g/>
)	)	kIx)	)
<g/>
Všechny	všechen	k3xTgInPc1	všechen
administrativní	administrativní	k2eAgInPc1d1	administrativní
celky	celek	k1gInPc1	celek
užívají	užívat	k5eAaImIp3nP	užívat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Japonska	Japonsko	k1gNnSc2	Japonsko
</s>
</p>
<p>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Japonska	Japonsko	k1gNnSc2	Japonsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
japonská	japonský	k2eAgFnSc1d1	japonská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
