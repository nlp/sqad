<s>
Vincentka	Vincentka	k1gFnSc1	Vincentka
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
léčivá	léčivý	k2eAgFnSc1d1	léčivá
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
vyvěrající	vyvěrající	k2eAgFnSc1d1	vyvěrající
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
Luhačovice	Luhačovice	k1gFnPc1	Luhačovice
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
uprostřed	uprostřed	k7c2	uprostřed
kolonády	kolonáda	k1gFnSc2	kolonáda
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
říčky	říčka	k1gFnSc2	říčka
Šťávnice	Šťávnice	k1gFnSc2	Šťávnice
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInSc1d1	přírodní
zdroj	zdroj	k1gInSc1	zdroj
léčivé	léčivý	k2eAgFnSc2d1	léčivá
minerální	minerální	k2eAgFnSc2d1	minerální
uhličité	uhličitý	k2eAgFnSc2d1	uhličitá
vody	voda	k1gFnSc2	voda
Vincentka	Vincentka	k1gFnSc1	Vincentka
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
ve	v	k7c6	v
zřídelní	zřídelní	k2eAgFnSc6d1	zřídelní
struktuře	struktura	k1gFnSc6	struktura
lázní	lázeň	k1gFnPc2	lázeň
Luhačovic	Luhačovice	k1gFnPc2	Luhačovice
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
podhůří	podhůří	k1gNnSc6	podhůří
Bílých	bílý	k2eAgInPc2d1	bílý
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
,	,	kIx,	,
v	v	k7c6	v
klimaticky	klimaticky	k6eAd1	klimaticky
velmi	velmi	k6eAd1	velmi
příhodném	příhodný	k2eAgNnSc6d1	příhodné
prostředí	prostředí	k1gNnSc6	prostředí
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
lesních	lesní	k2eAgInPc2d1	lesní
komplexů	komplex	k1gInPc2	komplex
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
256	[number]	k4	256
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
zbytkového	zbytkový	k2eAgInSc2d1	zbytkový
mořského	mořský	k2eAgInSc2d1	mořský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
léčivých	léčivý	k2eAgInPc2d1	léčivý
účinků	účinek	k1gInPc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1590	[number]	k4	1590
Luhačovice	Luhačovice	k1gFnPc4	Luhačovice
připadly	připadnout	k5eAaPmAgFnP	připadnout
rodině	rodina	k1gFnSc6	rodina
Bartodějských	Bartodějský	k2eAgFnPc2d1	Bartodějský
z	z	k7c2	z
Bartoděj	Bartoděj	k1gFnSc2	Bartoděj
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pramen	pramen	k1gInSc1	pramen
zvaný	zvaný	k2eAgInSc1d1	zvaný
Bublavý	bublavý	k2eAgInSc1d1	bublavý
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Amandka	Amandek	k1gMnSc2	Amandek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
léčivých	léčivý	k2eAgFnPc2d1	léčivá
luhačovických	luhačovický	k2eAgFnPc2d1	Luhačovická
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
fyzika	fyzik	k1gMnSc2	fyzik
a	a	k8xC	a
lékaře	lékař	k1gMnSc2	lékař
Jana	Jan	k1gMnSc2	Jan
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Hertoda	Hertod	k1gMnSc2	Hertod
z	z	k7c2	z
Todtenfeldu	Todtenfeld	k1gInSc2	Todtenfeld
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1669	[number]	k4	1669
vydal	vydat	k5eAaPmAgInS	vydat
písemnou	písemný	k2eAgFnSc4d1	písemná
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
chemickém	chemický	k2eAgNnSc6d1	chemické
složení	složení	k1gNnSc6	složení
luhačovické	luhačovický	k2eAgFnSc2d1	Luhačovická
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
způsob	způsob	k1gInSc4	způsob
pitné	pitný	k2eAgFnSc2d1	pitná
léčby	léčba	k1gFnSc2	léčba
a	a	k8xC	a
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
výsledky	výsledek	k1gInPc4	výsledek
léčení	léčení	k1gNnSc2	léčení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1792	[number]	k4	1792
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
název	název	k1gInSc1	název
Vincentka	Vincentka	k1gFnSc1	Vincentka
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
prodeje	prodej	k1gInSc2	prodej
Vincentky	Vincentka	k1gFnSc2	Vincentka
v	v	k7c6	v
lahvích	lahev	k1gFnPc6	lahev
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lázeňství	lázeňství	k1gNnSc6	lázeňství
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
k	k	k7c3	k
inhalačním	inhalační	k2eAgFnPc3d1	inhalační
a	a	k8xC	a
pitným	pitný	k2eAgFnPc3d1	pitná
kúrám	kúra	k1gFnPc3	kúra
při	při	k7c6	při
nemocech	nemoc	k1gFnPc6	nemoc
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
hlasivek	hlasivka	k1gFnPc2	hlasivka
<g/>
,	,	kIx,	,
poruchách	poruch	k1gInPc6	poruch
výměny	výměna	k1gFnSc2	výměna
látkové	látkový	k2eAgFnSc2d1	látková
<g/>
,	,	kIx,	,
žaludečních	žaludeční	k2eAgInPc6d1	žaludeční
vředech	vřed	k1gInPc6	vřed
<g/>
,	,	kIx,	,
nemocech	nemoc	k1gFnPc6	nemoc
dvanáctníku	dvanáctník	k1gInSc2	dvanáctník
<g/>
,	,	kIx,	,
při	při	k7c6	při
chronické	chronický	k2eAgFnSc6d1	chronická
pankreatitidě	pankreatitida	k1gFnSc6	pankreatitida
a	a	k8xC	a
diabetu	diabetes	k1gInSc6	diabetes
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
lázní	lázeň	k1gFnPc2	lázeň
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgInS	zasloužit
šlechtický	šlechtický	k2eAgInSc1d1	šlechtický
rod	rod	k1gInSc1	rod
Serényiů	Serényi	k1gMnPc2	Serényi
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
dal	dát	k5eAaPmAgMnS	dát
hrabě	hrabě	k1gMnSc1	hrabě
Vincenc	Vincenc	k1gMnSc1	Vincenc
postavit	postavit	k5eAaPmF	postavit
první	první	k4xOgFnSc1	první
léčebná	léčebný	k2eAgFnSc1d1	léčebná
a	a	k8xC	a
ubytovací	ubytovací	k2eAgNnPc1d1	ubytovací
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
lázně	lázeň	k1gFnPc4	lázeň
zvelebovat	zvelebovat	k5eAaImF	zvelebovat
hrabě	hrabě	k1gMnSc1	hrabě
Jan	Jan	k1gMnSc1	Jan
Serényi	Serény	k1gMnSc3	Serény
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
všechny	všechen	k3xTgInPc4	všechen
tehdy	tehdy	k6eAd1	tehdy
známé	známý	k2eAgFnPc1d1	známá
vody	voda	k1gFnPc1	voda
křestními	křestní	k2eAgNnPc7d1	křestní
jmény	jméno	k1gNnPc7	jméno
příslušníků	příslušník	k1gMnPc2	příslušník
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
existují	existovat	k5eAaImIp3nP	existovat
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc4	tři
prameny	pramen	k1gInPc4	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Vincentka	Vincentka	k1gFnSc1	Vincentka
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Vincentka	Vincentka	k1gFnSc1	Vincentka
a	a	k8xC	a
Vincentka	Vincentka	k1gFnSc1	Vincentka
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
původního	původní	k2eAgInSc2d1	původní
pramene	pramen	k1gInSc2	pramen
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
jako	jako	k9	jako
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
vybudované	vybudovaný	k2eAgFnSc6d1	vybudovaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1947	[number]	k4	1947
-	-	kIx~	-
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
studená	studený	k2eAgFnSc1d1	studená
i	i	k8xC	i
ohřívaná	ohřívaný	k2eAgFnSc1d1	ohřívaná
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
pitné	pitný	k2eAgFnSc3d1	pitná
léčbě	léčba	k1gFnSc3	léčba
a	a	k8xC	a
inhalacím	inhalace	k1gFnPc3	inhalace
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vydatnosti	vydatnost	k1gFnSc3	vydatnost
10	[number]	k4	10
-	-	kIx~	-
12	[number]	k4	12
litrů	litr	k1gInPc2	litr
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tento	tento	k3xDgInSc4	tento
pramen	pramen	k1gInSc4	pramen
již	již	k6eAd1	již
nevyužívá	využívat	k5eNaImIp3nS	využívat
k	k	k7c3	k
lahvování	lahvování	k1gNnSc3	lahvování
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
pramen	pramen	k1gInSc1	pramen
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
podobného	podobný	k2eAgNnSc2d1	podobné
složení	složení	k1gNnSc2	složení
jako	jako	k8xC	jako
původní	původní	k2eAgFnSc2d1	původní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
vydatnosti	vydatnost	k1gFnSc3	vydatnost
30	[number]	k4	30
litrů	litr	k1gInPc2	litr
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
využíván	využívat	k5eAaPmNgInS	využívat
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
pro	pro	k7c4	pro
stáčírnu	stáčírna	k1gFnSc4	stáčírna
a	a	k8xC	a
plněn	plnit	k5eAaImNgInS	plnit
do	do	k7c2	do
lahví	lahev	k1gFnPc2	lahev
<g/>
.	.	kIx.	.
35	[number]	k4	35
metrů	metr	k1gInPc2	metr
hluboký	hluboký	k2eAgInSc1d1	hluboký
vrt	vrt	k1gInSc1	vrt
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
z	z	k7c2	z
pramenů	pramen	k1gInPc2	pramen
s	s	k7c7	s
vydatností	vydatnost	k1gFnSc7	vydatnost
40	[number]	k4	40
litrů	litr	k1gInPc2	litr
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
najdete	najít	k5eAaPmIp2nP	najít
západně	západně	k6eAd1	západně
od	od	k7c2	od
Jurkovičova	Jurkovičův	k2eAgInSc2d1	Jurkovičův
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
rezerva	rezerva	k1gFnSc1	rezerva
pro	pro	k7c4	pro
léčebné	léčebný	k2eAgInPc4d1	léčebný
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Vincentka	Vincentka	k1gFnSc1	Vincentka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mj.	mj.	kA	mj.
ionty	ion	k1gInPc7	ion
sodíku	sodík	k1gInSc2	sodík
<g/>
,	,	kIx,	,
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
lithia	lithium	k1gNnSc2	lithium
<g/>
,	,	kIx,	,
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
,	,	kIx,	,
fluoru	fluor	k1gInSc2	fluor
<g/>
,	,	kIx,	,
chlóru	chlór	k1gInSc2	chlór
<g/>
,	,	kIx,	,
jódu	jód	k1gInSc2	jód
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
;	;	kIx,	;
železo	železo	k1gNnSc1	železo
je	být	k5eAaImIp3nS	být
však	však	k9	však
před	před	k7c7	před
stáčením	stáčení	k1gNnSc7	stáčení
do	do	k7c2	do
lahví	lahev	k1gFnPc2	lahev
odstraňováno	odstraňovat	k5eAaImNgNnS	odstraňovat
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
celkové	celkový	k2eAgFnSc2d1	celková
mineralizace	mineralizace	k1gFnSc2	mineralizace
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
gramů	gram	k1gInPc2	gram
na	na	k7c4	na
litr	litr	k1gInSc4	litr
<g/>
.	.	kIx.	.
</s>
<s>
Osmotickým	osmotický	k2eAgInSc7d1	osmotický
tlakem	tlak	k1gInSc7	tlak
634,7	[number]	k4	634,7
kPa	kPa	k?	kPa
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
osmolaritě	osmolarita	k1gFnSc3	osmolarita
lidské	lidský	k2eAgFnSc2d1	lidská
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
intracelulárních	intracelulární	k2eAgFnPc2d1	intracelulární
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
pramenů	pramen	k1gInPc2	pramen
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
typu	typ	k1gInSc3	typ
HCO3	HCO3	k1gFnSc2	HCO3
-	-	kIx~	-
Cl	Cl	k1gMnSc1	Cl
-	-	kIx~	-
Na	na	k7c6	na
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
prvků	prvek	k1gInPc2	prvek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
stopovou	stopový	k2eAgFnSc4d1	stopová
příměs	příměs	k1gFnSc4	příměs
sirovodíku	sirovodík	k1gInSc2	sirovodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
lázní	lázeň	k1gFnPc2	lázeň
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
k	k	k7c3	k
pitným	pitný	k2eAgFnPc3d1	pitná
a	a	k8xC	a
inhalačním	inhalační	k2eAgFnPc3d1	inhalační
kúrám	kúra	k1gFnPc3	kúra
při	při	k7c6	při
<g/>
:	:	kIx,	:
onemocnění	onemocnění	k1gNnSc4	onemocnění
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
hlasivek	hlasivka	k1gFnPc2	hlasivka
chorobách	choroba	k1gFnPc6	choroba
látkové	látkový	k2eAgFnSc2d1	látková
výměny	výměna	k1gFnSc2	výměna
žaludečních	žaludeční	k2eAgInPc2d1	žaludeční
a	a	k8xC	a
dvanáctníkových	dvanáctníkový	k2eAgInPc6d1	dvanáctníkový
vředech	vřed	k1gInPc6	vřed
a	a	k8xC	a
pooperačních	pooperační	k2eAgInPc6d1	pooperační
stavech	stav	k1gInPc6	stav
vleklých	vleklý	k2eAgInPc6d1	vleklý
zduřeních	zduření	k1gNnPc6	zduření
jater	játra	k1gNnPc2	játra
diabetes	diabetes	k1gInSc4	diabetes
mellitus	mellitus	k1gInSc4	mellitus
chronické	chronický	k2eAgFnSc2d1	chronická
pankreatitis	pankreatitis	k1gFnSc1	pankreatitis
Vincentku	Vincentka	k1gFnSc4	Vincentka
lze	lze	k6eAd1	lze
užívat	užívat	k5eAaImF	užívat
preventivně	preventivně	k6eAd1	preventivně
v	v	k7c6	v
denní	denní	k2eAgFnSc6d1	denní
dávce	dávka	k1gFnSc6	dávka
200	[number]	k4	200
-	-	kIx~	-
300	[number]	k4	300
ml.	ml.	kA	ml.
Jako	jako	k8xS	jako
účinná	účinný	k2eAgFnSc1d1	účinná
pitná	pitný	k2eAgFnSc1d1	pitná
kúra	kúra	k1gFnSc1	kúra
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
2	[number]	k4	2
<g/>
×	×	k?	×
denně	denně	k6eAd1	denně
na	na	k7c6	na
lačno	lačno	k6eAd1	lačno
(	(	kIx(	(
<g/>
před	před	k7c7	před
snídaní	snídaně	k1gFnSc7	snídaně
a	a	k8xC	a
večeří	večeře	k1gFnSc7	večeře
<g/>
)	)	kIx)	)
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
250	[number]	k4	250
-	-	kIx~	-
350	[number]	k4	350
ml	ml	kA	ml
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
20	[number]	k4	20
-	-	kIx~	-
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
množství	množství	k1gNnSc6	množství
25	[number]	k4	25
-	-	kIx~	-
30	[number]	k4	30
ml	ml	kA	ml
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
používána	používat	k5eAaImNgFnS	používat
k	k	k7c3	k
úhradě	úhrada	k1gFnSc3	úhrada
denní	denní	k2eAgFnSc2d1	denní
dávky	dávka	k1gFnSc2	dávka
jódu	jód	k1gInSc2	jód
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
obsahu	obsah	k1gInSc3	obsah
minerálních	minerální	k2eAgFnPc2d1	minerální
solí	sůl	k1gFnPc2	sůl
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k9	jako
k	k	k7c3	k
regeneraci	regenerace	k1gFnSc3	regenerace
po	po	k7c6	po
velkých	velký	k2eAgFnPc6d1	velká
ztrátách	ztráta	k1gFnPc6	ztráta
potu	pot	k1gInSc2	pot
<g/>
.	.	kIx.	.
</s>
<s>
Neutralizuje	neutralizovat	k5eAaBmIp3nS	neutralizovat
překyselení	překyselení	k1gNnSc1	překyselení
žaludku	žaludek	k1gInSc2	žaludek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
po	po	k7c6	po
konzumaci	konzumace	k1gFnSc6	konzumace
kávy	káva	k1gFnSc2	káva
nebo	nebo	k8xC	nebo
vína	víno	k1gNnSc2	víno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
ke	k	k7c3	k
kloktání	kloktání	k1gNnSc3	kloktání
<g/>
,	,	kIx,	,
inhalacím	inhalace	k1gFnPc3	inhalace
a	a	k8xC	a
výplachům	výplach	k1gInPc3	výplach
nosních	nosní	k2eAgFnPc2d1	nosní
dutin	dutina	k1gFnPc2	dutina
a	a	k8xC	a
nosohltanu	nosohltan	k1gInSc2	nosohltan
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
i	i	k9	i
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
těhotné	těhotný	k2eAgFnPc4d1	těhotná
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
rekonvalescenty	rekonvalescent	k1gMnPc4	rekonvalescent
<g/>
.	.	kIx.	.
</s>
<s>
Kontraindikována	kontraindikovat	k5eAaImNgFnS	kontraindikovat
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
se	s	k7c7	s
selhávajícími	selhávající	k2eAgFnPc7d1	selhávající
ledvinami	ledvina	k1gFnPc7	ledvina
<g/>
;	;	kIx,	;
osoby	osoba	k1gFnPc1	osoba
s	s	k7c7	s
onemocněním	onemocnění	k1gNnSc7	onemocnění
ledvin	ledvina	k1gFnPc2	ledvina
nebo	nebo	k8xC	nebo
otoky	otok	k1gInPc4	otok
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
před	před	k7c7	před
použitím	použití	k1gNnSc7	použití
měly	mít	k5eAaImAgFnP	mít
poradit	poradit	k5eAaPmF	poradit
s	s	k7c7	s
lékařem	lékař	k1gMnSc7	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Vincentka	Vincentka	k1gFnSc1	Vincentka
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
lázní	lázeň	k1gFnPc2	lázeň
u	u	k7c2	u
původního	původní	k2eAgInSc2d1	původní
pramene	pramen	k1gInSc2	pramen
Vincentka	Vincentka	k1gFnSc1	Vincentka
<g/>
.	.	kIx.	.
</s>
<s>
Lahvovaná	Lahvovaný	k2eAgFnSc1d1	Lahvovaný
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pramene	pramen	k1gInSc2	pramen
Nová	nový	k2eAgFnSc1d1	nová
Vincentka	Vincentka	k1gFnSc1	Vincentka
<g/>
.	.	kIx.	.
</s>
<s>
Prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
nosních	nosní	k2eAgInPc6d1	nosní
sprejových	sprejový	k2eAgInPc6d1	sprejový
aplikátorech	aplikátor	k1gInPc6	aplikátor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přidávána	přidáván	k2eAgFnSc1d1	přidávána
do	do	k7c2	do
zubních	zubní	k2eAgFnPc2d1	zubní
past	pasta	k1gFnPc2	pasta
a	a	k8xC	a
pastilek	pastilka	k1gFnPc2	pastilka
Vincentka	Vincentka	k1gFnSc1	Vincentka
<g/>
.	.	kIx.	.
</s>
