<p>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
osvětlení	osvětlení	k1gNnSc2	osvětlení
(	(	kIx(	(
<g/>
též	též	k9	též
osvětlenost	osvětlenost	k1gFnSc1	osvětlenost
<g/>
,	,	kIx,	,
osvětlivost	osvětlivost	k1gFnSc1	osvětlivost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fotometrická	fotometrický	k2eAgFnSc1d1	fotometrická
veličina	veličina	k1gFnSc1	veličina
definovaná	definovaný	k2eAgFnSc1d1	definovaná
jako	jako	k8xC	jako
světelný	světelný	k2eAgInSc1d1	světelný
tok	tok	k1gInSc1	tok
dopadající	dopadající	k2eAgInSc1d1	dopadající
na	na	k7c4	na
jednotku	jednotka	k1gFnSc4	jednotka
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
podílem	podíl	k1gInSc7	podíl
světelného	světelný	k2eAgInSc2d1	světelný
toku	tok	k1gInSc2	tok
(	(	kIx(	(
<g/>
v	v	k7c6	v
lumenech	lumen	k1gInPc6	lumen
<g/>
)	)	kIx)	)
a	a	k8xC	a
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
v	v	k7c6	v
metrech	metr	k1gInPc6	metr
čtverečních	čtvereční	k2eAgInPc6d1	čtvereční
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
E.	E.	kA	E.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc7	její
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
lux	lux	k1gInSc1	lux
(	(	kIx(	(
<g/>
lx	lx	k?	lx
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
Φ	Φ	k?	Φ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
A	A	kA	A
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Φ	Φ	k?	Φ
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
bodového	bodový	k2eAgInSc2d1	bodový
zdroje	zdroj	k1gInSc2	zdroj
o	o	k7c6	o
svítivosti	svítivost	k1gFnSc6	svítivost
I	i	k8xC	i
a	a	k8xC	a
paprsků	paprsek	k1gInPc2	paprsek
dopadajích	dopadaj	k1gFnPc6	dopadaj
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
α	α	k?	α
k	k	k7c3	k
normále	normála	k1gFnSc3	normála
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
r	r	kA	r
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
I	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}	}	kIx)	}
</s>
</p>
<p>
<s>
Osvětlení	osvětlení	k1gNnSc1	osvětlení
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nepřímo	přímo	k6eNd1	přímo
úměrné	úměrný	k2eAgNnSc1d1	úměrné
čtverci	čtverec	k1gInPc7	čtverec
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
šikměji	šikmo	k6eAd2	šikmo
paprsky	paprsek	k1gInPc1	paprsek
dopadají	dopadat	k5eAaImIp3nP	dopadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotkou	jednotka	k1gFnSc7	jednotka
osvětlení	osvětlení	k1gNnSc2	osvětlení
je	být	k5eAaImIp3nS	být
lux	lux	k1gInSc1	lux
(	(	kIx(	(
<g/>
lx	lx	k?	lx
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
osvětlení	osvětlení	k1gNnSc1	osvětlení
způsobené	způsobený	k2eAgNnSc1d1	způsobené
světelným	světelný	k2eAgInSc7d1	světelný
tokem	tok	k1gInSc7	tok
1	[number]	k4	1
lm	lm	k?	lm
dopadajícím	dopadající	k2eAgFnPc3d1	dopadající
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
1	[number]	k4	1
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Evropské	evropský	k2eAgFnSc2d1	Evropská
normy	norma	k1gFnSc2	norma
pro	pro	k7c4	pro
osvětlení	osvětlení	k1gNnSc4	osvětlení
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
v	v	k7c6	v
prostorech	prostor	k1gInPc6	prostor
s	s	k7c7	s
trvalým	trvalý	k2eAgInSc7d1	trvalý
pobytem	pobyt	k1gInSc7	pobyt
osob	osoba	k1gFnPc2	osoba
udržovaná	udržovaný	k2eAgFnSc1d1	udržovaná
osvětlenost	osvětlenost	k1gFnSc1	osvětlenost
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
200	[number]	k4	200
lx	lx	k?	lx
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
oka	oko	k1gNnSc2	oko
je	být	k5eAaImIp3nS	být
značná	značný	k2eAgFnSc1d1	značná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
rozlišit	rozlišit	k5eAaPmF	rozlišit
předměty	předmět	k1gInPc4	předmět
již	již	k9	již
při	při	k7c6	při
osvětlení	osvětlení	k1gNnSc6	osvětlení
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
5	[number]	k4	5
lx	lx	k?	lx
.	.	kIx.	.
</s>
</p>
<p>
<s>
Běžná	běžný	k2eAgFnSc1d1	běžná
hodnota	hodnota	k1gFnSc1	hodnota
osvětlení	osvětlení	k1gNnSc2	osvětlení
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
prostorách	prostora	k1gFnPc6	prostora
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
lx	lx	k?	lx
<g/>
,	,	kIx,	,
ve	v	k7c4	v
slunečný	slunečný	k2eAgInSc4d1	slunečný
letní	letní	k2eAgInSc4d1	letní
den	den	k1gInSc4	den
na	na	k7c6	na
volném	volný	k2eAgNnSc6d1	volné
prostranství	prostranství	k1gNnSc6	prostranství
lze	lze	k6eAd1	lze
naměřit	naměřit	k5eAaBmF	naměřit
hodnoty	hodnota	k1gFnPc4	hodnota
větší	veliký	k2eAgFnPc4d2	veliký
než	než	k8xS	než
70	[number]	k4	70
000	[number]	k4	000
lx	lx	k?	lx
(	(	kIx(	(
<g/>
v	v	k7c6	v
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jasná	jasný	k2eAgFnSc1d1	jasná
měsíční	měsíční	k2eAgFnSc1d1	měsíční
noc	noc	k1gFnSc1	noc
při	při	k7c6	při
úplňku	úplněk	k1gInSc6	úplněk
představuje	představovat	k5eAaImIp3nS	představovat
osvětlenost	osvětlenost	k1gFnSc1	osvětlenost
do	do	k7c2	do
0,5	[number]	k4	0,5
lx	lx	k?	lx
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
zrak	zrak	k1gInSc1	zrak
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
adaptabilní	adaptabilní	k2eAgNnSc1d1	adaptabilní
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vnímat	vnímat	k5eAaImF	vnímat
určité	určitý	k2eAgInPc4d1	určitý
světelné	světelný	k2eAgInPc4d1	světelný
podněty	podnět	k1gInPc4	podnět
ještě	ještě	k9	ještě
při	při	k7c6	při
hladině	hladina	k1gFnSc6	hladina
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
9	[number]	k4	9
lx	lx	k?	lx
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
jakékoliv	jakýkoliv	k3yIgInPc4	jakýkoliv
předměty	předmět	k1gInPc4	předmět
<g/>
;	;	kIx,	;
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
číst	číst	k5eAaImF	číst
výrazný	výrazný	k2eAgInSc4d1	výrazný
text	text	k1gInSc4	text
při	při	k7c6	při
osvětlení	osvětlení	k1gNnSc6	osvětlení
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
8	[number]	k4	8
lx	lx	k?	lx
(	(	kIx(	(
<g/>
pochopitelně	pochopitelně	k6eAd1	pochopitelně
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
výrazného	výrazný	k2eAgNnSc2d1	výrazné
nepohodlí	nepohodlí	k1gNnSc2	nepohodlí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Odvození	odvození	k1gNnSc1	odvození
vzorců	vzorec	k1gInPc2	vzorec
</s>
</p>
