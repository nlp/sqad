<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Kořínek	Kořínek	k1gMnSc1	Kořínek
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
obránce	obránce	k1gMnSc1	obránce
a	a	k8xC	a
záložník	záložník	k1gMnSc1	záložník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
aktivní	aktivní	k2eAgFnSc2d1	aktivní
kariéry	kariéra	k1gFnSc2	kariéra
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
funkcionář	funkcionář	k1gMnSc1	funkcionář
a	a	k8xC	a
trenér	trenér	k1gMnSc1	trenér
<g/>
,	,	kIx,	,
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1982	[number]	k4	1982
<g/>
/	/	kIx~	/
<g/>
83	[number]	k4	83
byl	být	k5eAaImAgInS	být
asistentem	asistent	k1gMnSc7	asistent
Františka	František	k1gMnSc2	František
Plasse	Plass	k1gMnSc2	Plass
u	u	k7c2	u
týmu	tým	k1gInSc2	tým
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
hrál	hrát	k5eAaImAgInS	hrát
za	za	k7c4	za
SONP	SONP	kA	SONP
Kladno	Kladno	k1gNnSc4	Kladno
a	a	k8xC	a
Škodu	škoda	k1gFnSc4	škoda
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
76	[number]	k4	76
ligových	ligový	k2eAgInPc6d1	ligový
utkáních	utkání	k1gNnPc6	utkání
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
1	[number]	k4	1
ligový	ligový	k2eAgInSc4d1	ligový
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
v	v	k7c4	v
utkání	utkání	k1gNnSc4	utkání
proti	proti	k7c3	proti
FC	FC	kA	FC
Bayern	Bayern	k1gInSc1	Bayern
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ligová	ligový	k2eAgFnSc1d1	ligová
bilance	bilance	k1gFnSc1	bilance
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
Král	Král	k1gMnSc1	Král
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
našeho	náš	k3xOp1gInSc2	náš
fotbalu	fotbal	k1gInSc2	fotbal
−	−	k?	−
Libri	Libri	k1gNnSc1	Libri
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Radovan	Radovan	k1gMnSc1	Radovan
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Jenšík	Jenšík	k1gMnSc1	Jenšík
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
českého	český	k2eAgInSc2d1	český
fotbalu	fotbal	k1gInSc2	fotbal
−	−	k?	−
Radovan	Radovan	k1gMnSc1	Radovan
Jelínek	Jelínek	k1gMnSc1	Jelínek
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Ľubomír	Ľubomír	k1gMnSc1	Ľubomír
Dávid	Dávida	k1gFnPc2	Dávida
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Grünner	Grünner	k1gMnSc1	Grünner
<g/>
,	,	kIx,	,
Juraj	Juraj	k1gMnSc1	Juraj
Hrivnák	Hrivnák	k1gMnSc1	Hrivnák
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Kšiňan	Kšiňan	k1gMnSc1	Kšiňan
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Pejchar	Pejchar	k1gMnSc1	Pejchar
<g/>
:	:	kIx,	:
Futbal	Futbal	k1gInSc1	Futbal
82	[number]	k4	82
<g/>
/	/	kIx~	/
<g/>
83	[number]	k4	83
-	-	kIx~	-
ročenka	ročenka	k1gFnSc1	ročenka
</s>
</p>
<p>
<s>
Gól	gól	k1gInSc1	gól
<g/>
:	:	kIx,	:
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
a	a	k8xC	a
hokejový	hokejový	k2eAgInSc1d1	hokejový
týdeník	týdeník	k1gInSc1	týdeník
-	-	kIx~	-
ročník	ročník	k1gInSc1	ročník
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Worldfootball	Worldfootball	k1gInSc1	Worldfootball
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
</s>
</p>
<p>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
deník	deník	k1gInSc1	deník
</s>
</p>
<p>
<s>
CS	CS	kA	CS
Fotbal	fotbal	k1gInSc1	fotbal
</s>
</p>
