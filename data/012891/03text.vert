<p>
<s>
Jetel	jetel	k1gInSc1	jetel
luční	luční	k2eAgInSc1d1	luční
(	(	kIx(	(
<g/>
Trifolium	Trifolium	k1gNnSc1	Trifolium
pratense	pratense	k1gFnSc2	pratense
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
deset	deset	k4xCc1	deset
centimetrů	centimetr	k1gInPc2	centimetr
až	až	k8xS	až
metr	metr	k1gInSc4	metr
vysoká	vysoká	k1gFnSc1	vysoká
<g/>
,	,	kIx,	,
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
dvouděložní	dvouděložný	k2eAgMnPc1d1	dvouděložný
bylina	bylina	k1gFnSc1	bylina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
bobovitých	bobovitý	k2eAgMnPc2d1	bobovitý
<g/>
.	.	kIx.	.
</s>
<s>
Nesprávně	správně	k6eNd1	správně
nazýván	nazývat	k5eAaImNgInS	nazývat
jako	jako	k9	jako
jetel	jetel	k1gInSc1	jetel
červený	červený	k2eAgInSc1d1	červený
(	(	kIx(	(
<g/>
špatný	špatný	k2eAgInSc1d1	špatný
překlad	překlad	k1gInSc1	překlad
z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgNnSc1d1	latinské
jméno	jméno	k1gNnSc1	jméno
označuje	označovat	k5eAaImIp3nS	označovat
trojlístek	trojlístek	k1gInSc1	trojlístek
jeho	jeho	k3xOp3gInPc2	jeho
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
rostliny	rostlina	k1gFnSc2	rostlina
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
bylina	bylina	k1gFnSc1	bylina
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
mohutný	mohutný	k2eAgInSc1d1	mohutný
kořenový	kořenový	k2eAgInSc1d1	kořenový
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
kořen	kořen	k1gInSc1	kořen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
proniká	pronikat	k5eAaImIp3nS	pronikat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něho	on	k3xPp3gInSc2	on
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
listová	listový	k2eAgFnSc1d1	listová
růžice	růžice	k1gFnSc1	růžice
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yQgFnSc2	který
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
přímé	přímý	k2eAgFnPc1d1	přímá
větvené	větvený	k2eAgFnPc1d1	větvená
lodyhy	lodyha	k1gFnPc1	lodyha
<g/>
.	.	kIx.	.
</s>
<s>
Lodyhy	lodyha	k1gFnPc1	lodyha
nesou	nést	k5eAaImIp3nP	nést
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
střídavě	střídavě	k6eAd1	střídavě
trojčetné	trojčetný	k2eAgFnPc1d1	trojčetná
a	a	k8xC	a
dlouze	dlouho	k6eAd1	dlouho
řapíkaté	řapíkatý	k2eAgNnSc1d1	řapíkaté
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
krátce	krátce	k6eAd1	krátce
řapíkaté	řapíkatý	k2eAgFnPc1d1	řapíkatá
až	až	k8xS	až
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
až	až	k8xS	až
široce	široko	k6eAd1	široko
elipsovité	elipsovitý	k2eAgInPc1d1	elipsovitý
celokrajné	celokrajný	k2eAgInPc1d1	celokrajný
lístky	lístek	k1gInPc1	lístek
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
líci	líc	k1gInSc6	líc
výraznou	výrazný	k2eAgFnSc4d1	výrazná
bělavou	bělavý	k2eAgFnSc4d1	bělavá
nebo	nebo	k8xC	nebo
červenohnědou	červenohnědý	k2eAgFnSc4d1	červenohnědá
půlměsícovitou	půlměsícovitý	k2eAgFnSc4d1	půlměsícovitý
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rubu	rub	k1gInSc6	rub
jsou	být	k5eAaImIp3nP	být
lístky	lístek	k1gInPc1	lístek
místy	místy	k6eAd1	místy
chlupaté	chlupatý	k2eAgInPc1d1	chlupatý
a	a	k8xC	a
beze	beze	k7c2	beze
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Ostře	ostro	k6eAd1	ostro
špičaté	špičatý	k2eAgInPc1d1	špičatý
palisty	palist	k1gInPc1	palist
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
řapíkem	řapík	k1gInSc7	řapík
vysoko	vysoko	k6eAd1	vysoko
srostlé	srostlý	k2eAgNnSc1d1	srostlé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Květenství	květenství	k1gNnSc1	květenství
je	být	k5eAaImIp3nS	být
hlávkovité	hlávkovitý	k2eAgInPc4d1	hlávkovitý
tvořené	tvořený	k2eAgInPc4d1	tvořený
drobnými	drobné	k1gInPc7	drobné
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
stopkatými	stopkatý	k2eAgInPc7d1	stopkatý
<g/>
,	,	kIx,	,
souměrnými	souměrný	k2eAgInPc7d1	souměrný
oboupohlavnými	oboupohlavný	k2eAgInPc7d1	oboupohlavný
pětičetnými	pětičetný	k2eAgInPc7d1	pětičetný
květy	květ	k1gInPc7	květ
<g/>
.	.	kIx.	.
</s>
<s>
Květenství	květenství	k1gNnSc1	květenství
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Květy	Květa	k1gFnPc1	Květa
mají	mít	k5eAaImIp3nP	mít
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
narůžovělé	narůžovělý	k2eAgFnPc1d1	narůžovělá
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
bílé	bílý	k2eAgInPc1d1	bílý
korunní	korunní	k2eAgInPc1d1	korunní
lístky	lístek	k1gInPc1	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Vejčitě	vejčitě	k6eAd1	vejčitě
podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
pavéza	pavéza	k1gFnSc1	pavéza
a	a	k8xC	a
úzce	úzko	k6eAd1	úzko
podlouhlá	podlouhlý	k2eAgNnPc4d1	podlouhlé
křídla	křídlo	k1gNnPc4	křídlo
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgInPc1d2	delší
než	než	k8xS	než
tupý	tupý	k2eAgInSc1d1	tupý
člunek	člunek	k1gInSc1	člunek
<g/>
.	.	kIx.	.
</s>
<s>
Květní	květní	k2eAgFnPc1d1	květní
hlávky	hlávka	k1gFnPc1	hlávka
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
zakryté	zakrytý	k2eAgFnSc6d1	zakrytá
velkými	velký	k2eAgInPc7d1	velký
palisty	palist	k1gInPc7	palist
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
kvetení	kvetení	k1gNnSc2	kvetení
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
délku	délka	k1gFnSc4	délka
korunní	korunní	k2eAgFnSc2d1	korunní
trubky	trubka	k1gFnSc2	trubka
může	moct	k5eAaImIp3nS	moct
opylovat	opylovat	k5eAaImF	opylovat
tuto	tento	k3xDgFnSc4	tento
bylinu	bylina	k1gFnSc4	bylina
pouze	pouze	k6eAd1	pouze
hmyz	hmyz	k1gInSc1	hmyz
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
sosákem	sosák	k1gInSc7	sosák
<g/>
,	,	kIx,	,
především	především	k9	především
čmeláci	čmelák	k1gMnPc1	čmelák
nebo	nebo	k8xC	nebo
motýli	motýl	k1gMnPc1	motýl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
nepukavý	pukavý	k2eNgInSc1d1	nepukavý
jednosemenný	jednosemenný	k2eAgInSc1d1	jednosemenný
lusk	lusk	k1gInSc1	lusk
<g/>
,	,	kIx,	,
pevně	pevně	k6eAd1	pevně
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
v	v	k7c6	v
kalichu	kalich	k1gInSc6	kalich
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
nepravidelně	pravidelně	k6eNd1	pravidelně
ledvinovitá	ledvinovitý	k2eAgFnSc1d1	ledvinovitá
až	až	k9	až
2,5	[number]	k4	2,5
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obrysu	obrys	k1gInSc6	obrys
mají	mít	k5eAaImIp3nP	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
kořínek	kořínek	k1gInSc4	kořínek
vyznačený	vyznačený	k2eAgInSc4d1	vyznačený
rýhou	rýha	k1gFnSc7	rýha
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
lesklá	lesklý	k2eAgFnSc1d1	lesklá
<g/>
,	,	kIx,	,
žlutě	žlutě	k6eAd1	žlutě
až	až	k9	až
fialově	fialově	k6eAd1	fialově
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
se	se	k3xPyFc4	se
při	při	k7c6	při
výmlatu	výmlat	k1gInSc6	výmlat
z	z	k7c2	z
lusku	lusk	k1gInSc2	lusk
obtížně	obtížně	k6eAd1	obtížně
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylina	bylina	k1gFnSc1	bylina
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
loukách	louka	k1gFnPc6	louka
a	a	k8xC	a
pastvinách	pastvina	k1gFnPc6	pastvina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
přilehlých	přilehlý	k2eAgFnPc6d1	přilehlá
částech	část	k1gFnPc6	část
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
pěstovat	pěstovat	k5eAaImF	pěstovat
při	při	k7c6	při
zavedení	zavedení	k1gNnSc6	zavedení
střídavého	střídavý	k2eAgNnSc2d1	střídavé
hospodářství	hospodářství	k1gNnSc2	hospodářství
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
roste	růst	k5eAaImIp3nS	růst
skoro	skoro	k6eAd1	skoro
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podobné	podobný	k2eAgInPc1d1	podobný
druhy	druh	k1gInPc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
rostliny	rostlina	k1gFnPc4	rostlina
v	v	k7c6	v
rodě	rod	k1gInSc6	rod
jetel	jetel	k1gInSc1	jetel
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
často	často	k6eAd1	často
jetel	jetel	k1gInSc1	jetel
luční	luční	k2eAgInSc1d1	luční
zaměňuje	zaměňovat	k5eAaImIp3nS	zaměňovat
s	s	k7c7	s
jetelem	jetel	k1gInSc7	jetel
zvrhlým	zvrhlý	k2eAgInSc7d1	zvrhlý
(	(	kIx(	(
<g/>
Trifolium	Trifolium	k1gNnSc1	Trifolium
hybridum	hybridum	k1gNnSc1	hybridum
L.	L.	kA	L.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
jetel	jetel	k1gInSc4	jetel
švédský	švédský	k2eAgMnSc1d1	švédský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
listy	list	k1gInPc4	list
bez	bez	k7c2	bez
charakteristických	charakteristický	k2eAgFnPc2d1	charakteristická
půlměsícovitých	půlměsícovitý	k2eAgFnPc2d1	půlměsícovitý
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
jetelem	jetel	k1gInSc7	jetel
prostředním	prostřednět	k5eAaImIp1nS	prostřednět
(	(	kIx(	(
<g/>
Trifolium	Trifolium	k1gNnSc1	Trifolium
medium	medium	k1gNnSc1	medium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Jetel	jetel	k1gInSc1	jetel
luční	luční	k2eAgInSc1d1	luční
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
pícninu	pícnina	k1gFnSc4	pícnina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
bílkovinné	bílkovinný	k2eAgFnPc4d1	bílkovinná
a	a	k8xC	a
minerální	minerální	k2eAgFnPc4d1	minerální
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
vyséván	vysévat	k5eAaImNgInS	vysévat
pro	pro	k7c4	pro
krmení	krmení	k1gNnSc4	krmení
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osevních	osevní	k2eAgInPc6d1	osevní
sledech	sled	k1gInPc6	sled
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
při	při	k7c6	při
zúrodňování	zúrodňování	k1gNnSc6	zúrodňování
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Včelařství	včelařství	k1gNnSc2	včelařství
===	===	k?	===
</s>
</p>
<p>
<s>
Jetel	jetel	k1gInSc1	jetel
luční	luční	k2eAgFnSc2d1	luční
je	být	k5eAaImIp3nS	být
vynikající	vynikající	k2eAgFnSc1d1	vynikající
nektarodárná	ktarodárný	k2eNgFnSc1d1	nektarodárná
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Nektarium	nektarium	k1gNnSc1	nektarium
diploidního	diploidní	k2eAgInSc2d1	diploidní
jetele	jetel	k1gInSc2	jetel
lučního	luční	k2eAgInSc2d1	luční
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
0,8	[number]	k4	0,8
<g/>
–	–	k?	–
<g/>
0,9	[number]	k4	0,9
mg	mg	kA	mg
nektaru	nektar	k1gInSc2	nektar
s	s	k7c7	s
cukernatostí	cukernatost	k1gFnSc7	cukernatost
45	[number]	k4	45
<g/>
–	–	k?	–
<g/>
63	[number]	k4	63
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Cukerná	cukerný	k2eAgFnSc1d1	cukerná
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
množství	množství	k1gNnSc1	množství
cukru	cukr	k1gInSc2	cukr
vyprodukovaného	vyprodukovaný	k2eAgInSc2d1	vyprodukovaný
v	v	k7c6	v
květu	květ	k1gInSc6	květ
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
0,4	[number]	k4	0,4
mg	mg	kA	mg
<g/>
.	.	kIx.	.
<g/>
Nektaria	nektarium	k1gNnPc1	nektarium
tetraploidního	tetraploidní	k2eAgInSc2d1	tetraploidní
jetele	jetel	k1gInSc2	jetel
lučního	luční	k2eAgInSc2d1	luční
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
0,5	[number]	k4	0,5
<g/>
–	–	k?	–
<g/>
0,8	[number]	k4	0,8
mg	mg	kA	mg
nektaru	nektar	k1gInSc2	nektar
s	s	k7c7	s
cukernatostí	cukernatost	k1gFnSc7	cukernatost
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
58	[number]	k4	58
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Cukerná	cukerný	k2eAgFnSc1d1	cukerná
hodnota	hodnota	k1gFnSc1	hodnota
květů	květ	k1gInPc2	květ
je	být	k5eAaImIp3nS	být
0,57	[number]	k4	0,57
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
jetele	jetel	k1gInSc2	jetel
lučního	luční	k2eAgInSc2d1	luční
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
trubka	trubka	k1gFnSc1	trubka
květu	květ	k1gInSc2	květ
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
včelám	včela	k1gFnPc3	včela
ztěžovat	ztěžovat	k5eAaImF	ztěžovat
sběr	sběr	k1gInSc4	sběr
nektaru	nektar	k1gInSc2	nektar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pylová	pylový	k2eAgNnPc1d1	pylové
zrna	zrno	k1gNnPc1	zrno
jetele	jetel	k1gInSc2	jetel
lučního	luční	k2eAgInSc2d1	luční
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
včely	včela	k1gFnPc4	včela
velmi	velmi	k6eAd1	velmi
výživná	výživný	k2eAgFnSc1d1	výživná
<g/>
.	.	kIx.	.
</s>
<s>
Rousky	rouska	k1gFnPc1	rouska
jetelového	jetelový	k2eAgInSc2d1	jetelový
pylu	pyl	k1gInSc2	pyl
jsou	být	k5eAaImIp3nP	být
tmavě	tmavě	k6eAd1	tmavě
hnědá	hnědat	k5eAaImIp3nS	hnědat
<g/>
.	.	kIx.	.
<g/>
Druhový	druhový	k2eAgInSc4d1	druhový
med	med	k1gInSc4	med
z	z	k7c2	z
jetele	jetel	k1gInSc2	jetel
lučního	luční	k2eAgInSc2d1	luční
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgInSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnSc2	včela
jej	on	k3xPp3gMnSc4	on
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
v	v	k7c6	v
suchých	suchý	k2eAgNnPc6d1	suché
letech	léto	k1gNnPc6	léto
či	či	k8xC	či
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
seče	seč	k1gFnSc2	seč
jetele	jetel	k1gInSc2	jetel
lučního	luční	k2eAgInSc2d1	luční
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hnědě	hnědě	k6eAd1	hnědě
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
<g/>
,	,	kIx,	,
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
vůní	vůně	k1gFnSc7	vůně
a	a	k8xC	a
příjemnou	příjemný	k2eAgFnSc7d1	příjemná
chutí	chuť	k1gFnSc7	chuť
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
hrubé	hrubý	k2eAgInPc1d1	hrubý
krystaly	krystal	k1gInPc1	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Nektar	nektar	k1gInSc1	nektar
jetele	jetel	k1gInSc2	jetel
lučního	luční	k2eAgInSc2d1	luční
se	se	k3xPyFc4	se
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
stává	stávat	k5eAaImIp3nS	stávat
součástí	součást	k1gFnSc7	součást
smíšených	smíšený	k2eAgInPc2d1	smíšený
letních	letní	k2eAgInPc2d1	letní
medů	med	k1gInPc2	med
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lékařství	lékařství	k1gNnSc1	lékařství
===	===	k?	===
</s>
</p>
<p>
<s>
Jetel	jetel	k1gInSc1	jetel
luční	luční	k2eAgInSc1d1	luční
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
léčivá	léčivý	k2eAgFnSc1d1	léčivá
bylina	bylina	k1gFnSc1	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInPc3	jeho
silným	silný	k2eAgInPc3d1	silný
desinfekčním	desinfekční	k2eAgInPc3d1	desinfekční
účinkům	účinek	k1gInPc3	účinek
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
průjmů	průjem	k1gInPc2	průjem
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc2	zvracení
a	a	k8xC	a
otravy	otrava	k1gFnSc2	otrava
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
na	na	k7c6	na
detoxikaci	detoxikace	k1gFnSc6	detoxikace
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
přidáván	přidáván	k2eAgInSc1d1	přidáván
do	do	k7c2	do
čistících	čistící	k2eAgFnPc2d1	čistící
čajových	čajový	k2eAgFnPc2d1	čajová
směsí	směs	k1gFnPc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
a	a	k8xC	a
ulehčuje	ulehčovat	k5eAaImIp3nS	ulehčovat
léčbu	léčba	k1gFnSc4	léčba
kašle	kašel	k1gInSc2	kašel
<g/>
,	,	kIx,	,
rýmy	rýma	k1gFnSc2	rýma
<g/>
,	,	kIx,	,
bronchitidy	bronchitida	k1gFnSc2	bronchitida
<g/>
,	,	kIx,	,
či	či	k8xC	či
nachlazení	nachlazení	k1gNnSc1	nachlazení
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
čaje	čaj	k1gInPc4	čaj
z	z	k7c2	z
květů	květ	k1gInPc2	květ
jetele	jetel	k1gInSc2	jetel
lučního	luční	k2eAgInSc2d1	luční
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
užívat	užívat	k5eAaImF	užívat
i	i	k9	i
delší	dlouhý	k2eAgNnSc4d2	delší
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
potíže	potíž	k1gFnPc4	potíž
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
menopauzou	menopauza	k1gFnSc7	menopauza
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
nespavost	nespavost	k1gFnSc1	nespavost
<g/>
,	,	kIx,	,
návaly	nával	k1gInPc1	nával
horka	horko	k1gNnSc2	horko
<g/>
,	,	kIx,	,
pocity	pocit	k1gInPc1	pocit
nevolnosti	nevolnost	k1gFnSc2	nevolnost
apod.	apod.	kA	apod.
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
využitelnou	využitelný	k2eAgFnSc7d1	využitelná
složkou	složka	k1gFnSc7	složka
jsou	být	k5eAaImIp3nP	být
fytoestrogeny	fytoestrogen	k1gInPc1	fytoestrogen
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vázat	vázat	k5eAaImF	vázat
na	na	k7c4	na
estrogenové	estrogenový	k2eAgInPc4d1	estrogenový
receptory	receptor	k1gInPc4	receptor
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organizmu	organizmus	k1gInSc6	organizmus
a	a	k8xC	a
projevovat	projevovat	k5eAaImF	projevovat
se	se	k3xPyFc4	se
estrogenními	estrogenní	k2eAgFnPc7d1	estrogenní
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
vlastnosti	vlastnost	k1gFnSc3	vlastnost
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
doplnění	doplnění	k1gNnSc3	doplnění
scházejících	scházející	k2eAgInPc2d1	scházející
estrogenů	estrogen	k1gInPc2	estrogen
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
věkem	věk	k1gInSc7	věk
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
vlastní	vlastní	k2eAgInPc1d1	vlastní
estrogeny	estrogen	k1gInPc1	estrogen
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
estrogenů	estrogen	k1gInPc2	estrogen
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
ženy	žena	k1gFnSc2	žena
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgInPc2d1	různý
problémů	problém	k1gInPc2	problém
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
klimakterické	klimakterický	k2eAgFnPc4d1	klimakterická
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
osteoporóza	osteoporóza	k1gFnSc1	osteoporóza
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
pružnosti	pružnost	k1gFnSc2	pružnost
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
tvorba	tvorba	k1gFnSc1	tvorba
vrásek	vrásek	k1gInSc1	vrásek
<g/>
.	.	kIx.	.
<g/>
Má	mít	k5eAaImIp3nS	mít
blahodárné	blahodárný	k2eAgInPc4d1	blahodárný
účinky	účinek	k1gInPc4	účinek
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
kůži	kůže	k1gFnSc6	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Léčí	léčit	k5eAaImIp3nP	léčit
kožní	kožní	k2eAgInPc4d1	kožní
problémy	problém	k1gInPc4	problém
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
ekzém	ekzém	k1gInSc4	ekzém
<g/>
,	,	kIx,	,
akné	akné	k1gFnSc4	akné
či	či	k8xC	či
drobná	drobný	k2eAgNnPc4d1	drobné
poranění	poranění	k1gNnPc4	poranění
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
opět	opět	k6eAd1	opět
díky	díky	k7c3	díky
dezinfekčním	dezinfekční	k2eAgInPc3d1	dezinfekční
účinkům	účinek	k1gInPc3	účinek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
urychlují	urychlovat	k5eAaImIp3nP	urychlovat
hojení	hojení	k1gNnSc4	hojení
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nálevu	nálev	k1gInSc2	nálev
čerstvého	čerstvý	k2eAgInSc2d1	čerstvý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sušeného	sušený	k2eAgInSc2d1	sušený
jetele	jetel	k1gInSc2	jetel
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
připravit	připravit	k5eAaPmF	připravit
pleťová	pleťový	k2eAgFnSc1d1	pleťová
voda	voda	k1gFnSc1	voda
nebo	nebo	k8xC	nebo
koupel	koupel	k1gFnSc1	koupel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mystika	mystika	k1gFnSc1	mystika
a	a	k8xC	a
magie	magie	k1gFnSc1	magie
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
lidé	člověk	k1gMnPc1	člověk
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
jetelový	jetelový	k2eAgInSc1d1	jetelový
čtyřlístek	čtyřlístek	k1gInSc1	čtyřlístek
přinese	přinést	k5eAaPmIp3nS	přinést
štěstí	štěstí	k1gNnSc4	štěstí
či	či	k8xC	či
osobní	osobní	k2eAgFnSc4d1	osobní
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Netypická	typický	k2eNgFnSc1d1	netypická
forma	forma	k1gFnSc1	forma
–	–	k?	–
čtyřlístek	čtyřlístek	k1gInSc1	čtyřlístek
–	–	k?	–
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
symbolem	symbol	k1gInSc7	symbol
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
další	další	k2eAgFnSc1d1	další
netypická	typický	k2eNgFnSc1d1	netypická
forma	forma	k1gFnSc1	forma
–	–	k?	–
dvojlístek	dvojlístek	k1gInSc1	dvojlístek
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
starých	starý	k2eAgFnPc2d1	stará
pověr	pověra	k1gFnPc2	pověra
je	být	k5eAaImIp3nS	být
dvojlístek	dvojlístek	k1gInSc1	dvojlístek
symbolem	symbol	k1gInSc7	symbol
pro	pro	k7c4	pro
nového	nový	k2eAgMnSc4d1	nový
milence	milenec	k1gMnSc4	milenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lidové	lidový	k2eAgInPc1d1	lidový
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
lidové	lidový	k2eAgInPc4d1	lidový
názvy	název	k1gInPc4	název
patří	patřit	k5eAaImIp3nS	patřit
dětelina	dětelina	k1gFnSc1	dětelina
<g/>
,	,	kIx,	,
milostná	milostný	k2eAgFnSc1d1	milostná
zelinka	zelinka	k1gFnSc1	zelinka
<g/>
,	,	kIx,	,
jatel	jatel	k1gInSc1	jatel
či	či	k8xC	či
listnačka	listnačka	k1gFnSc1	listnačka
<g/>
,	,	kIx,	,
čtyřlístek	čtyřlístek	k1gInSc1	čtyřlístek
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HRON	Hron	k1gMnSc1	Hron
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
ZEJBRLÍK	ZEJBRLÍK	kA	ZEJBRLÍK
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
polí	pole	k1gFnPc2	pole
a	a	k8xC	a
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
192-193	[number]	k4	192-193
</s>
</p>
<p>
<s>
PILÁT	Pilát	k1gMnSc1	Pilát
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
OTTO	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Ušák	ušák	k1gMnSc1	ušák
<g/>
.	.	kIx.	.
</s>
<s>
Kapesní	kapesní	k2eAgInSc4d1	kapesní
atlas	atlas	k1gInSc4	atlas
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
107-108	[number]	k4	107-108
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jetel	jetel	k1gInSc1	jetel
plazivý	plazivý	k2eAgInSc1d1	plazivý
</s>
</p>
<p>
<s>
Jetel	jetel	k1gInSc1	jetel
rolní	rolní	k2eAgFnSc2d1	rolní
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jetel	jetel	k1gInSc1	jetel
luční	luční	k2eAgFnPc4d1	luční
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Trifolium	Trifolium	k1gNnSc1	Trifolium
pratense	pratense	k6eAd1	pratense
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Jetel	jetel	k1gInSc1	jetel
luční	luční	k2eAgInSc1d1	luční
ve	v	k7c6	v
fotoherbáři	fotoherbář	k1gInSc6	fotoherbář
</s>
</p>
<p>
<s>
Jetel	jetel	k1gInSc1	jetel
luční	luční	k2eAgInSc1d1	luční
v	v	k7c6	v
Atlasu	Atlas	k1gInSc6	Atlas
rostlin	rostlina	k1gFnPc2	rostlina
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
jetele	jetel	k1gInSc2	jetel
lučního	luční	k2eAgInSc2d1	luční
</s>
</p>
<p>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
jetele	jetel	k1gInSc2	jetel
lučního	luční	k2eAgInSc2d1	luční
dle	dle	k7c2	dle
ČNFD	ČNFD	kA	ČNFD
</s>
</p>
