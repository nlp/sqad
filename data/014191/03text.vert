<s>
Lunar	Lunar	k1gInSc1
Landing	Landing	k1gInSc1
Research	Research	k1gInSc4
Vehicle	Vehicle	k1gFnSc2
</s>
<s>
Lunar	Lunar	k1gInSc1
Landing	Landing	k1gInSc1
Research	Research	k1gInSc4
Vehicle	Vehicle	k1gFnSc2
na	na	k7c6
letištní	letištní	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
v	v	k7c6
Leteckém	letecký	k2eAgNnSc6d1
výzkumném	výzkumný	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
NASA	NASA	kA
<g/>
,	,	kIx,
1966	#num#	k4
</s>
<s>
Lunar	Lunar	k2eAgInSc1d1
Landing	Landing	k2eAgInSc1d1
Research	Research	k2eAgInSc1d1
Vehicle	Vehicle	k1gInSc1
(	(	kIx(
<g/>
LLRV	LLRV	kA
<g/>
,	,	kIx,
volně	volně	k6eAd1
„	„	kIx"	
<g/>
prostředek	prostředek	k1gInSc1
výzkumu	výzkum	k1gInSc2
přistání	přistání	k1gNnSc2
na	na	k7c6
Měsíci	měsíc	k1gInSc6
<g/>
“	“	kIx"	
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
létající	létající	k2eAgInSc1d1
trenažér	trenažér	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgNnSc6
astronauti	astronaut	k1gMnPc1
programu	program	k1gInSc2
Apollo	Apollo	k1gNnSc1
nacvičovali	nacvičovat	k5eAaImAgMnP
přistání	přistání	k1gNnSc4
na	na	k7c6
Měsíci	měsíc	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
objednávku	objednávka	k1gFnSc4
NASA	NASA	kA
ho	on	k3xPp3gMnSc4
zkonstruovala	zkonstruovat	k5eAaPmAgFnS
a	a	k8xC
vyráběla	vyrábět	k5eAaImAgFnS
firma	firma	k1gFnSc1
Bell	bell	k1gInSc1
Aerosystems	Aerosystems	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
naučit	naučit	k5eAaPmF
astronauty	astronaut	k1gMnPc4
manévrovat	manévrovat	k5eAaImF
a	a	k8xC
přistávat	přistávat	k5eAaImF
v	v	k7c6
podmínkách	podmínka	k1gFnPc6
šestinové	šestinový	k2eAgFnSc2d1
gravitace	gravitace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stroj	stroj	k1gInSc1
byl	být	k5eAaImAgInS
schopen	schopen	k2eAgInSc1d1
kolmého	kolmý	k2eAgInSc2d1
vzletu	vzlet	k1gInSc2
a	a	k8xC
přistání	přistání	k1gNnSc4
s	s	k7c7
centrálním	centrální	k2eAgInSc7d1
proudovým	proudový	k2eAgInSc7d1
motorem	motor	k1gInSc7
CF	CF	kA
700	#num#	k4
v	v	k7c4
Kardanově	kardanově	k6eAd1
závěsu	závěsa	k1gFnSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
umožnil	umožnit	k5eAaPmAgInS
motoru	motor	k1gInSc2
vždy	vždy	k6eAd1
zachovávat	zachovávat	k5eAaImF
svislou	svislý	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
tah	tah	k1gInSc1
byl	být	k5eAaImAgInS
seřízen	seřídit	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyrovnával	vyrovnávat	k5eAaImAgInS
5	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
váhy	váha	k1gFnPc4
stroje	stroj	k1gInSc2
<g/>
,	,	kIx,
takže	takže	k8xS
manévrování	manévrování	k1gNnSc1
pomocí	pomocí	k7c2
dalších	další	k2eAgInPc2d1
raketových	raketový	k2eAgInPc2d1
motorů	motor	k1gInPc2
na	na	k7c4
peroxid	peroxid	k1gInSc4
vodíku	vodík	k1gInSc2
poměrně	poměrně	k6eAd1
věrně	věrně	k6eAd1
simulovalo	simulovat	k5eAaImAgNnS
pohyb	pohyb	k1gInSc4
v	v	k7c6
podmínkách	podmínka	k1gFnPc6
nižší	nízký	k2eAgFnSc2d2
měsíční	měsíční	k2eAgFnSc2d1
gravitace	gravitace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Postaveny	postaven	k2eAgFnPc1d1
a	a	k8xC
roku	rok	k1gInSc2
1964	#num#	k4
dodány	dodat	k5eAaPmNgInP
byly	být	k5eAaImAgFnP
dva	dva	k4xCgInPc4
LLRV	LLRV	kA
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc1
zkoušky	zkouška	k1gFnPc1
probíhaly	probíhat	k5eAaImAgFnP
v	v	k7c6
Leteckém	letecký	k2eAgNnSc6d1
výzkumném	výzkumný	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
NASA	NASA	kA
na	na	k7c6
Edwardsově	Edwardsův	k2eAgFnSc6d1
základně	základna	k1gFnSc6
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jejich	jejich	k3xOp3gInSc6
úspěšném	úspěšný	k2eAgInSc6d1
průběhu	průběh	k1gInSc6
NASA	NASA	kA
roku	rok	k1gInSc2
1966	#num#	k4
objednala	objednat	k5eAaPmAgFnS
ještě	ještě	k9
tři	tři	k4xCgInPc4
další	další	k2eAgInPc4d1
<g/>
,	,	kIx,
nazvané	nazvaný	k2eAgInPc4d1
Lunar	Lunar	k1gInSc4
Landing	Landing	k1gInSc1
Training	Training	k1gInSc1
Vehicles	Vehicles	k1gInSc1
(	(	kIx(
<g/>
LLTV	LLTV	kA
<g/>
,	,	kIx,
volně	volně	k6eAd1
„	„	k?
<g/>
prostředek	prostředek	k1gInSc1
tréninku	trénink	k1gInSc2
přistání	přistání	k1gNnSc2
na	na	k7c6
Měsíci	měsíc	k1gInSc6
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výcvikové	výcvikový	k2eAgInPc1d1
lety	let	k1gInPc1
byly	být	k5eAaImAgInP
přemístěny	přemístit	k5eAaPmNgInP
na	na	k7c4
Ellingtonovo	Ellingtonův	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
v	v	k7c6
Houstonu	Houston	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgInPc1
z	z	k7c2
pěti	pět	k4xCc2
dodaných	dodaný	k2eAgInPc2d1
strojů	stroj	k1gInPc2
byly	být	k5eAaImAgFnP
během	během	k7c2
letů	let	k1gInPc2
zničeny	zničen	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
astronauti	astronaut	k1gMnPc1
se	se	k3xPyFc4
vždy	vždy	k6eAd1
bezpečně	bezpečně	k6eAd1
katapultovali	katapultovat	k5eAaBmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeživší	přeživší	k2eAgInSc1d1
LLRV	LLRV	kA
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Leteckého	letecký	k2eAgNnSc2d1
výzkumného	výzkumný	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
NASA	NASA	kA
<g/>
,	,	kIx,
jediný	jediný	k2eAgInSc1d1
zbylý	zbylý	k2eAgInSc1d1
LLTV	LLTV	kA
je	být	k5eAaImIp3nS
vystavován	vystavovat	k5eAaImNgInS
v	v	k7c6
Johnsonově	Johnsonův	k2eAgNnSc6d1
vesmírném	vesmírný	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
v	v	k7c6
Houstonu	Houston	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MATRANGA	MATRANGA	kA
<g/>
,	,	kIx,
Gene	gen	k1gInSc5
J	J	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unconventional	Unconventional	k1gFnPc2
<g/>
,	,	kIx,
Contrary	Contrara	k1gFnSc2
<g/>
,	,	kIx,
and	and	k?
Ugly	Ugla	k1gFnPc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Lunar	Lunar	k1gInSc1
Landing	Landing	k1gInSc1
Research	Research	k1gInSc4
Vehicle	Vehicle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
DC	DC	kA
<g/>
:	:	kIx,
NASA	NASA	kA
History	Histor	k1gMnPc4
Division	Division	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
xii	xii	k?
+	+	kIx~
228	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Monograph	Monograph	k1gMnSc1
is	is	k?
Aerospace	Aerospace	k1gFnSc1
History	Histor	k1gInPc4
<g/>
;	;	kIx,
sv.	sv.	kA
35	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lunar	Lunar	k1gInSc1
Landing	Landing	k1gInSc1
Research	Research	k1gInSc4
Vehicle	Vehicle	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
CURRY	CURRY	kA
<g/>
,	,	kIx,
Marty	Marta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fact	Fact	k2eAgInSc1d1
Sheets	Sheets	k1gInSc1
:	:	kIx,
Lunar	Lunar	k1gInSc1
Landing	Landing	k1gInSc1
Research	Research	k1gInSc1
Vehicle	Vehicle	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dryden	Drydna	k1gFnPc2
Flight	Flight	k1gMnSc1
Research	Research	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2008-5-7	2008-5-7	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stručně	stručně	k6eAd1
o	o	k7c6
LLRV	LLRV	kA
na	na	k7c6
stránkách	stránka	k1gFnPc6
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CURRY	CURRY	kA
<g/>
,	,	kIx,
Marty	Marta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Image	image	k1gInSc1
Gallery	Galler	k1gInPc1
:	:	kIx,
Lunar	Lunar	k1gInSc1
Landing	Landing	k1gInSc1
Research	Research	k1gInSc1
Vehicle	Vehicle	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dryden	Drydna	k1gFnPc2
Flight	Flight	k1gMnSc1
Research	Research	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2011-5-23	2011-5-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrazová	obrazový	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
LLRV	LLRV	kA
na	na	k7c6
stránkách	stránka	k1gFnPc6
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Bell	bell	k1gInSc1
Aircraft	Aircraft	k1gMnSc1
Corporation	Corporation	k1gInSc1
/	/	kIx~
Bell	bell	k1gInSc1
Helicopter	Helicopter	k1gInSc1
Textron	Textron	k1gInSc4
Bojová	bojový	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
</s>
<s>
YFM-1	YFM-1	k4
•	•	k?
P-39	P-39	k1gMnSc1
•	•	k?
P-59	P-59	k1gMnSc1
•	•	k?
P-63	P-63	k1gMnSc1
•	•	k?
XFL	XFL	kA
•	•	k?
XP-52	XP-52	k1gMnSc1
•	•	k?
XP-76	XP-76	k1gMnSc1
•	•	k?
XP-77	XP-77	k1gMnSc1
•	•	k?
XP-83	XP-83	k1gMnSc1
Experimentální	experimentální	k2eAgMnSc1d1
letadla	letadlo	k1gNnSc2
</s>
<s>
X-1	X-1	k4
•	•	k?
X-2	X-2	k1gMnSc1
•	•	k?
X-5	X-5	k1gMnSc1
•	•	k?
X-9	X-9	k1gMnSc1
•	•	k?
X-14	X-14	k1gMnSc1
•	•	k?
X-16	X-16	k1gMnSc1
•	•	k?
X-22	X-22	k1gMnSc1
•	•	k?
XV-3	XV-3	k1gMnSc1
•	•	k?
XV-15	XV-15	k1gFnSc2
•	•	k?
533	#num#	k4
•	•	k?
D-292	D-292	k1gMnSc3
•	•	k?
LLRV	LLRV	kA
Komerční	komerční	k2eAgFnSc2d1
helikoptéry	helikoptéra	k1gFnSc2
</s>
<s>
47	#num#	k4
<g/>
/	/	kIx~
<g/>
J	J	kA
•	•	k?
204	#num#	k4
•	•	k?
205	#num#	k4
•	•	k?
206	#num#	k4
•	•	k?
210	#num#	k4
•	•	k?
212	#num#	k4
•	•	k?
214	#num#	k4
•	•	k?
222	#num#	k4
•	•	k?
230	#num#	k4
•	•	k?
400	#num#	k4
•	•	k?
407	#num#	k4
•	•	k?
412	#num#	k4
•	•	k?
417	#num#	k4
•	•	k?
427	#num#	k4
•	•	k?
429	#num#	k4
•	•	k?
430	#num#	k4
•	•	k?
440	#num#	k4
•	•	k?
505	#num#	k4
•	•	k?
525	#num#	k4
Vojenské	vojenský	k2eAgFnSc2d1
helikoptéry	helikoptéra	k1gFnSc2
</s>
<s>
H-13	H-13	k4
•	•	k?
XH-15	XH-15	k1gMnSc1
•	•	k?
UH-	UH-	k1gMnSc1
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
N	N	kA
<g/>
/	/	kIx~
<g/>
Y	Y	kA
•	•	k?
207	#num#	k4
•	•	k?
209	#num#	k4
•	•	k?
AH-	AH-	k1gMnSc1
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
J	J	kA
<g/>
/	/	kIx~
<g/>
T	T	kA
<g/>
/	/	kIx~
<g/>
W	W	kA
<g/>
/	/	kIx~
<g/>
Z	z	k7c2
•	•	k?
309	#num#	k4
•	•	k?
YAH-63	YAH-63	k1gMnSc1
•	•	k?
YOH-4	YOH-4	k1gMnSc1
•	•	k?
OH-58	OH-58	k1gMnSc1
•	•	k?
ARH-70	ARH-70	k1gMnSc1
•	•	k?
CH-146	CH-146	k1gMnSc1
Konvertoplány	Konvertoplán	k2eAgInPc4d1
</s>
<s>
XV-15	XV-15	k4
•	•	k?
V-22	V-22	k1gMnSc1
•	•	k?
BA609	BA609	k1gMnSc1
•	•	k?
Eagle	Eagle	k1gInSc1
Eye	Eye	k1gMnSc1
•	•	k?
V-247	V-247	k1gMnSc1
•	•	k?
V-280	V-280	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
