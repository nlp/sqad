<s>
Lipsko	Lipsko	k1gNnSc1	Lipsko
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Leipzig	Leipzig	k1gInSc1	Leipzig
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
s	s	k7c7	s
postavením	postavení	k1gNnSc7	postavení
samostatného	samostatný	k2eAgInSc2d1	samostatný
městského	městský	k2eAgInSc2d1	městský
okresu	okres	k1gInSc2	okres
(	(	kIx(	(
<g/>
Kreisfreie	Kreisfreie	k1gFnSc1	Kreisfreie
Stadt	Stadt	k1gMnSc1	Stadt
<g/>
)	)	kIx)	)
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
německé	německý	k2eAgFnSc2d1	německá
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Sasko	Sasko	k1gNnSc1	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
535	[number]	k4	535
732	[number]	k4	732
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
Lipsko	Lipsko	k1gNnSc1	Lipsko
dvanácté	dvanáctý	k4xOgNnSc4	dvanáctý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Lipsko	Lipsko	k1gNnSc1	Lipsko
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
nejjižnější	jižní	k2eAgFnSc6d3	nejjižnější
části	část	k1gFnSc6	část
Severoněmecké	severoněmecký	k2eAgFnSc2d1	Severoněmecká
nížiny	nížina	k1gFnSc2	nížina
<g/>
,	,	kIx,	,
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
tří	tři	k4xCgFnPc2	tři
řek	řeka	k1gFnPc2	řeka
(	(	kIx(	(
<g/>
Weißer	Weißer	k1gInSc1	Weißer
Elster	Elstra	k1gFnPc2	Elstra
<g/>
,	,	kIx,	,
Pleiße	Pleiße	k1gFnPc2	Pleiße
a	a	k8xC	a
Parthe	Parthe	k1gFnPc2	Parthe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
zalesněno	zalesněn	k2eAgNnSc1d1	zalesněno
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
krajiny	krajina	k1gFnSc2	krajina
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
utvářen	utvářen	k2eAgInSc1d1	utvářen
těžbou	těžba	k1gFnSc7	těžba
hnědého	hnědý	k2eAgNnSc2d1	hnědé
uhlí	uhlí	k1gNnSc2	uhlí
v	v	k7c6	v
povrchových	povrchový	k2eAgInPc6d1	povrchový
dolech	dol	k1gInPc6	dol
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
uzavření	uzavření	k1gNnSc6	uzavření
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
postupně	postupně	k6eAd1	postupně
zavodňovány	zavodňovat	k5eAaImNgInP	zavodňovat
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
zejména	zejména	k6eAd1	zejména
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
turisticky	turisticky	k6eAd1	turisticky
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
města	město	k1gNnSc2	město
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
23,4	[number]	k4	23,4
kilometru	kilometr	k1gInSc2	kilometr
a	a	k8xC	a
z	z	k7c2	z
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
21,3	[number]	k4	21,3
kilometru	kilometr	k1gInSc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
128,7	[number]	k4	128,7
kilometru	kilometr	k1gInSc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničí	hraničit	k5eAaImIp3nS	hraničit
Lipsko	Lipsko	k1gNnSc1	Lipsko
se	s	k7c7	s
zemským	zemský	k2eAgInSc7d1	zemský
okresem	okres	k1gInSc7	okres
Severní	severní	k2eAgNnSc4d1	severní
Sasko	Sasko	k1gNnSc4	Sasko
(	(	kIx(	(
<g/>
Landkreis	Landkreis	k1gInSc1	Landkreis
Nordsachsen	Nordsachsen	k1gInSc1	Nordsachsen
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
pak	pak	k6eAd1	pak
se	s	k7c7	s
zemským	zemský	k2eAgInSc7d1	zemský
okresem	okres	k1gInSc7	okres
Lipsko	Lipsko	k1gNnSc1	Lipsko
(	(	kIx(	(
<g/>
Landkreis	Landkreis	k1gFnSc1	Landkreis
Leipzig	Leipzig	k1gMnSc1	Leipzig
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgNnSc1d3	nejbližší
velké	velký	k2eAgNnSc1d1	velké
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Halle	Halle	k1gFnSc1	Halle
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
30	[number]	k4	30
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
<g/>
.	.	kIx.	.
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
leží	ležet	k5eAaImIp3nS	ležet
pak	pak	k6eAd1	pak
zemské	zemský	k2eAgNnSc1d1	zemské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Saska-Anhaltska	Saska-Anhaltska	k1gFnSc1	Saska-Anhaltska
-	-	kIx~	-
Magdeburg	Magdeburg	k1gInSc1	Magdeburg
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Saska	Sasko	k1gNnSc2	Sasko
Drážďany	Drážďany	k1gInPc4	Drážďany
se	se	k3xPyFc4	se
nalézé	nalézé	k6eAd1	nalézé
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
.	.	kIx.	.
70	[number]	k4	70
kilometrů	kilometr	k1gInPc2	kilometr
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
město	město	k1gNnSc1	město
Chemnitz	Chemnitza	k1gFnPc2	Chemnitza
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Durynsko	Durynsko	k1gNnSc1	Durynsko
Erfurt	Erfurt	k1gInSc1	Erfurt
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnPc1d2	menší
vyvýšeniny	vyvýšenina	k1gFnPc1	vyvýšenina
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
přírodními	přírodní	k2eAgInPc7d1	přírodní
body	bod	k1gInPc7	bod
jsou	být	k5eAaImIp3nP	být
Monarchenhügel	Monarchenhügel	k1gInSc4	Monarchenhügel
(	(	kIx(	(
<g/>
Monarchův	monarchův	k2eAgInSc4d1	monarchův
vršek	vršek	k1gInSc4	vršek
<g/>
)	)	kIx)	)
se	s	k7c7	s
159	[number]	k4	159
metry	metro	k1gNnPc7	metro
a	a	k8xC	a
Galgenberg	Galgenberg	k1gInSc1	Galgenberg
(	(	kIx(	(
<g/>
Šibeniční	šibeniční	k2eAgFnSc1d1	šibeniční
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
se	s	k7c7	s
163	[number]	k4	163
metry	metro	k1gNnPc7	metro
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgMnPc1d2	vyšší
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
skládky	skládka	k1gFnPc1	skládka
Seehausen	Seehausna	k1gFnPc2	Seehausna
(	(	kIx(	(
<g/>
178	[number]	k4	178
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Liebertwolkwitz	Liebertwolkwitz	k1gMnSc1	Liebertwolkwitz
(	(	kIx(	(
<g/>
177	[number]	k4	177
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řekou	řeka	k1gFnSc7	řeka
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
průtokem	průtok	k1gInSc7	průtok
je	být	k5eAaImIp3nS	být
Weißer	Weißer	k1gMnSc1	Weißer
Elster	Elster	k1gMnSc1	Elster
<g/>
.	.	kIx.	.
</s>
<s>
Známější	známý	k2eAgFnSc1d2	známější
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
Pleiße	Pleiße	k1gNnSc4	Pleiße
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
vedlejší	vedlejší	k2eAgNnSc4d1	vedlejší
rameno	rameno	k1gNnSc4	rameno
protéká	protékat	k5eAaImIp3nS	protékat
nejblíže	blízce	k6eAd3	blízce
centru	centrum	k1gNnSc3	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Lipsko	Lipsko	k1gNnSc1	Lipsko
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
názvu	název	k1gInSc2	název
"	"	kIx"	"
<g/>
lípa	lípa	k1gFnSc1	lípa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Osada	osada	k1gFnSc1	osada
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
slovanskými	slovanský	k2eAgMnPc7d1	slovanský
osadníky	osadník	k1gMnPc7	osadník
již	již	k6eAd1	již
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1015	[number]	k4	1015
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1165	[number]	k4	1165
bylo	být	k5eAaImAgNnS	být
Lipsko	Lipsko	k1gNnSc1	Lipsko
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
a	a	k8xC	a
místní	místní	k2eAgInPc4d1	místní
trhy	trh	k1gInPc4	trh
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
významnou	významný	k2eAgFnSc7d1	významná
nadregionální	nadregionální	k2eAgFnSc7d1	nadregionální
událostí	událost	k1gFnSc7	událost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1409	[number]	k4	1409
byla	být	k5eAaImAgFnS	být
německými	německý	k2eAgMnPc7d1	německý
mistry	mistr	k1gMnPc7	mistr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
odešli	odejít	k5eAaPmAgMnP	odejít
z	z	k7c2	z
pražské	pražský	k2eAgFnSc2d1	Pražská
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
Dekret	dekret	k1gInSc1	dekret
kutnohorský	kutnohorský	k2eAgInSc1d1	kutnohorský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
Lipská	lipský	k2eAgFnSc1d1	Lipská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
zvětšil	zvětšit	k5eAaPmAgInS	zvětšit
význam	význam	k1gInSc1	význam
města	město	k1gNnSc2	město
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rychle	rychle	k6eAd1	rychle
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
řemesla	řemeslo	k1gNnPc4	řemeslo
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Lipska	Lipsko	k1gNnSc2	Lipsko
byla	být	k5eAaImAgNnP	být
ve	v	k7c6	v
dnech	den	k1gInPc6	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
až	až	k9	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1813	[number]	k4	1813
svedena	sveden	k2eAgFnSc1d1	svedena
největší	veliký	k2eAgFnSc1d3	veliký
bitva	bitva	k1gFnSc1	bitva
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Bitva	bitva	k1gFnSc1	bitva
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
protifrancouzská	protifrancouzský	k2eAgFnSc1d1	protifrancouzská
koalice	koalice	k1gFnSc1	koalice
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
rozhodujícího	rozhodující	k2eAgNnSc2d1	rozhodující
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Lipskem	Lipsko	k1gNnSc7	Lipsko
a	a	k8xC	a
Drážďany	Drážďany	k1gInPc7	Drážďany
byla	být	k5eAaImAgFnS	být
také	také	k9	také
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
německá	německý	k2eAgFnSc1d1	německá
dálková	dálkový	k2eAgFnSc1d1	dálková
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
nedlouho	dlouho	k6eNd1	dlouho
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
důležitým	důležitý	k2eAgInSc7d1	důležitý
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
narůstal	narůstat	k5eAaImAgInS	narůstat
rychle	rychle	k6eAd1	rychle
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
nad	nad	k7c7	nad
500	[number]	k4	500
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
sestěhovávali	sestěhovávat	k5eAaImAgMnP	sestěhovávat
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
za	za	k7c7	za
vidinou	vidina	k1gFnSc7	vidina
lepšího	dobrý	k2eAgInSc2d2	lepší
života	život	k1gInSc2	život
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nacistické	nacistický	k2eAgFnSc2d1	nacistická
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
Lipsku	Lipsko	k1gNnSc3	Lipsko
nevyhnula	vyhnout	k5eNaPmAgFnS	vyhnout
tzv.	tzv.	kA	tzv.
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
vypáleny	vypálen	k2eAgFnPc1d1	vypálena
synagogy	synagoga	k1gFnPc1	synagoga
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
ubito	ubít	k5eAaPmNgNnS	ubít
několik	několik	k4yIc1	několik
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
noc	noc	k1gFnSc1	noc
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
stala	stát	k5eAaPmAgFnS	stát
velkou	velký	k2eAgFnSc7d1	velká
manifestací	manifestace	k1gFnSc7	manifestace
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
NSDAP	NSDAP	kA	NSDAP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1943	[number]	k4	1943
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
letecké	letecký	k2eAgNnSc1d1	letecké
bombardování	bombardování	k1gNnSc1	bombardování
zničilo	zničit	k5eAaPmAgNnS	zničit
až	až	k9	až
60	[number]	k4	60
%	%	kIx~	%
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
okolo	okolo	k7c2	okolo
6	[number]	k4	6
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
nálety	nálet	k1gInPc1	nálet
byly	být	k5eAaImAgInP	být
provedeny	provést	k5eAaPmNgInP	provést
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1943	[number]	k4	1943
(	(	kIx(	(
<g/>
britská	britský	k2eAgFnSc1d1	britská
Royal	Royal	k1gInSc1	Royal
Air	Air	k1gMnPc3	Air
Force	force	k1gFnSc2	force
<g/>
)	)	kIx)	)
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
(	(	kIx(	(
<g/>
americká	americký	k2eAgNnPc1d1	americké
letadla	letadlo	k1gNnPc1	letadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
obsadila	obsadit	k5eAaPmAgFnS	obsadit
město	město	k1gNnSc4	město
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
z	z	k7c2	z
jaltské	jaltský	k2eAgFnSc2d1	Jaltská
konference	konference	k1gFnSc2	konference
ho	on	k3xPp3gNnSc2	on
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1945	[number]	k4	1945
předala	předat	k5eAaPmAgFnS	předat
Rudé	rudý	k2eAgFnSc2d1	rudá
armádě	armáda	k1gFnSc3	armáda
a	a	k8xC	a
Lipsko	Lipsko	k1gNnSc1	Lipsko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
NDR	NDR	kA	NDR
začala	začít	k5eAaPmAgFnS	začít
mohutná	mohutný	k2eAgFnSc1d1	mohutná
přestavba	přestavba	k1gFnSc1	přestavba
města	město	k1gNnSc2	město
podle	podle	k7c2	podle
socialistického	socialistický	k2eAgInSc2d1	socialistický
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
vybudována	vybudován	k2eAgNnPc1d1	vybudováno
velká	velký	k2eAgNnPc1d1	velké
sídliště	sídliště	k1gNnPc1	sídliště
i	i	k8xC	i
nové	nový	k2eAgInPc1d1	nový
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
podniky	podnik	k1gInPc1	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Lipsko	Lipsko	k1gNnSc1	Lipsko
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaImF	stát
"	"	kIx"	"
<g/>
perlou	perla	k1gFnSc7	perla
Německa	Německo	k1gNnSc2	Německo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
existence	existence	k1gFnSc2	existence
Východního	východní	k2eAgNnSc2d1	východní
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
a	a	k8xC	a
1989	[number]	k4	1989
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
velké	velký	k2eAgFnSc2d1	velká
demonstrace	demonstrace	k1gFnSc2	demonstrace
proti	proti	k7c3	proti
SED	sed	k1gInSc1	sed
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnSc3d1	německá
komunistické	komunistický	k2eAgFnSc3d1	komunistická
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
a	a	k8xC	a
znovusjednocení	znovusjednocení	k1gNnSc2	znovusjednocení
Německa	Německo	k1gNnSc2	Německo
dostávalo	dostávat	k5eAaImAgNnS	dostávat
město	město	k1gNnSc4	město
velkou	velký	k2eAgFnSc4d1	velká
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
proměně	proměna	k1gFnSc3	proměna
z	z	k7c2	z
šedivého	šedivý	k2eAgNnSc2d1	šedivé
socialistického	socialistický	k2eAgNnSc2d1	socialistické
města	město	k1gNnSc2	město
v	v	k7c4	v
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejmodernějších	moderní	k2eAgNnPc2d3	nejmodernější
měst	město	k1gNnPc2	město
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Německé	německý	k2eAgFnSc2d1	německá
demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Specifickou	specifický	k2eAgFnSc7d1	specifická
kapitolou	kapitola	k1gFnSc7	kapitola
dějin	dějiny	k1gFnPc2	dějiny
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Lipsko	Lipsko	k1gNnSc1	Lipsko
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
centrem	centrum	k1gNnSc7	centrum
jejich	jejich	k3xOp3gFnSc2	jejich
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
usazeno	usadit	k5eAaPmNgNnS	usadit
na	na	k7c4	na
1	[number]	k4	1
500	[number]	k4	500
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
zabývajících	zabývající	k2eAgMnPc2d1	zabývající
se	se	k3xPyFc4	se
výrobou	výroba	k1gFnSc7	výroba
a	a	k8xC	a
prodejem	prodej	k1gInSc7	prodej
všemožných	všemožný	k2eAgFnPc2d1	všemožná
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
publikací	publikace	k1gFnPc2	publikace
<g/>
.	.	kIx.	.
</s>
<s>
Nechyběla	chybět	k5eNaImAgNnP	chybět
ani	ani	k8xC	ani
nejvýznamnější	významný	k2eAgNnPc1d3	nejvýznamnější
německá	německý	k2eAgNnPc1d1	německé
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
jsou	být	k5eAaImIp3nP	být
mnohá	mnohé	k1gNnPc1	mnohé
aktivní	aktivní	k2eAgFnSc2d1	aktivní
i	i	k8xC	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
-	-	kIx~	-
Baedeker	Baedeker	k1gMnSc1	Baedeker
<g/>
,	,	kIx,	,
Brockhaus	Brockhaus	k1gMnSc1	Brockhaus
apod.	apod.	kA	apod.
Z	z	k7c2	z
přítomnosti	přítomnost	k1gFnSc2	přítomnost
mnoha	mnoho	k4c2	mnoho
tiskáren	tiskárna	k1gFnPc2	tiskárna
<g/>
,	,	kIx,	,
knihvazačství	knihvazačství	k1gNnPc2	knihvazačství
či	či	k8xC	či
grafických	grafický	k2eAgInPc2d1	grafický
ústavů	ústav	k1gInPc2	ústav
profitovala	profitovat	k5eAaBmAgFnS	profitovat
ale	ale	k8xC	ale
i	i	k9	i
jiná	jiný	k2eAgNnPc4d1	jiné
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
odvětví	odvětví	k1gNnPc4	odvětví
<g/>
,	,	kIx,	,
např.	např.	kA	např.
strojírenské	strojírenský	k2eAgInPc4d1	strojírenský
podniky	podnik	k1gInPc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Odvětví	odvětví	k1gNnSc1	odvětví
výroby	výroba	k1gFnSc2	výroba
knih	kniha	k1gFnPc2	kniha
zažívalo	zažívat	k5eAaImAgNnS	zažívat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
takový	takový	k3xDgInSc1	takový
rozkvět	rozkvět	k1gInSc1	rozkvět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
činný	činný	k2eAgInSc1d1	činný
každý	každý	k3xTgMnSc1	každý
desátý	desátý	k4xOgMnSc1	desátý
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
si	se	k3xPyFc3	se
město	město	k1gNnSc1	město
vysloužilo	vysloužit	k5eAaPmAgNnS	vysloužit
dokonce	dokonce	k9	dokonce
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
city	cit	k1gInPc1	cit
of	of	k?	of
books	books	k1gInSc1	books
<g/>
"	"	kIx"	"
-	-	kIx~	-
město	město	k1gNnSc1	město
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
trpěla	trpět	k5eAaImAgFnS	trpět
řada	řada	k1gFnSc1	řada
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
kvůli	kvůli	k7c3	kvůli
restrikcím	restrikce	k1gFnPc3	restrikce
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
Grafická	grafický	k2eAgFnSc1d1	grafická
čtvrť	čtvrť	k1gFnSc1	čtvrť
(	(	kIx(	(
<g/>
Graphisches	Graphisches	k1gMnSc1	Graphisches
Viertel	Viertel	k1gMnSc1	Viertel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
koncentrována	koncentrován	k2eAgFnSc1d1	koncentrována
většina	většina	k1gFnSc1	většina
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
zničena	zničit	k5eAaPmNgFnS	zničit
nálety	nálet	k1gInPc7	nálet
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
Německa	Německo	k1gNnSc2	Německo
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vedlo	vést	k5eAaImAgNnS	vést
i	i	k9	i
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
většiny	většina	k1gFnSc2	většina
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pak	pak	k6eAd1	pak
působily	působit	k5eAaImAgFnP	působit
na	na	k7c6	na
území	území	k1gNnSc6	území
obou	dva	k4xCgInPc2	dva
německých	německý	k2eAgMnPc2d1	německý
států	stát	k1gInPc2	stát
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
NDR	NDR	kA	NDR
zastávalo	zastávat	k5eAaImAgNnS	zastávat
Lipsko	Lipsko	k1gNnSc1	Lipsko
nadále	nadále	k6eAd1	nadále
pozici	pozice	k1gFnSc4	pozice
"	"	kIx"	"
<g/>
města	město	k1gNnPc1	město
knih	kniha	k1gFnPc2	kniha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sjednocení	sjednocení	k1gNnSc6	sjednocení
Německa	Německo	k1gNnSc2	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
navrácení	navrácení	k1gNnSc3	navrácení
lipských	lipský	k2eAgNnPc2d1	Lipské
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
původním	původní	k2eAgMnPc3d1	původní
vlastníkům	vlastník	k1gMnPc3	vlastník
<g/>
,	,	kIx,	,
sídlícím	sídlící	k2eAgNnSc6d1	sídlící
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Převzaté	převzatý	k2eAgInPc1d1	převzatý
podniky	podnik	k1gInPc1	podnik
byly	být	k5eAaImAgInP	být
dále	daleko	k6eAd2	daleko
provozovány	provozovat	k5eAaImNgInP	provozovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
pobočky	pobočka	k1gFnSc2	pobočka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
byly	být	k5eAaImAgFnP	být
mnohé	mnohý	k2eAgInPc1d1	mnohý
později	pozdě	k6eAd2	pozdě
uzavřeny	uzavřen	k2eAgInPc1d1	uzavřen
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
byla	být	k5eAaImAgNnP	být
prodána	prodán	k2eAgFnSc1d1	prodána
a	a	k8xC	a
fungují	fungovat	k5eAaImIp3nP	fungovat
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc3	Lipsko
dodnes	dodnes	k6eAd1	dodnes
(	(	kIx(	(
<g/>
E.	E.	kA	E.
A.	A.	kA	A.
Seemann	Seemann	k1gMnSc1	Seemann
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Benno	Benno	k1gNnSc1	Benno
<g/>
,	,	kIx,	,
Koehler	Koehler	k1gMnSc1	Koehler
&	&	k?	&
Amelang	Amelang	k1gMnSc1	Amelang
<g/>
,	,	kIx,	,
Buchverlag	Buchverlag	k1gMnSc1	Buchverlag
für	für	k?	für
die	die	k?	die
Frau	Fraus	k1gInSc2	Fraus
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každoročně	každoročně	k6eAd1	každoročně
knižní	knižní	k2eAgInSc1d1	knižní
veletrh	veletrh	k1gInSc1	veletrh
(	(	kIx(	(
<g/>
Leipziger	Leipziger	k1gInSc1	Leipziger
Buchmesse	Buchmesse	k1gFnSc2	Buchmesse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
veletrhu	veletrh	k1gInSc6	veletrh
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgInPc3d3	veliký
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
města	město	k1gNnSc2	město
během	běh	k1gInSc7	běh
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
velkých	velký	k2eAgNnPc6d1	velké
německých	německý	k2eAgNnPc6d1	německé
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k1gNnSc1	málo
historických	historický	k2eAgFnPc2d1	historická
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
poměrně	poměrně	k6eAd1	poměrně
kompaktní	kompaktní	k2eAgFnPc1d1	kompaktní
a	a	k8xC	a
zachovalé	zachovalý	k2eAgFnPc1d1	zachovalá
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
najít	najít	k5eAaPmF	najít
mnoho	mnoho	k4c4	mnoho
skvostných	skvostný	k2eAgInPc2d1	skvostný
domů	dům	k1gInPc2	dům
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
německého	německý	k2eAgNnSc2d1	německé
císařství	císařství	k1gNnSc2	císařství
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
rozmachu	rozmach	k1gInSc6	rozmach
a	a	k8xC	a
významu	význam	k1gInSc6	význam
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
renesanční	renesanční	k2eAgFnSc1d1	renesanční
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
(	(	kIx(	(
<g/>
Altes	Altes	k1gInSc1	Altes
Rathaus	rathaus	k1gInSc1	rathaus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
dochovaným	dochovaný	k2eAgFnPc3d1	dochovaná
renesančním	renesanční	k2eAgFnPc3d1	renesanční
stavbám	stavba	k1gFnPc3	stavba
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
její	její	k3xOp3gFnSc1	její
kapacita	kapacita	k1gFnSc1	kapacita
později	pozdě	k6eAd2	pozdě
přestala	přestat	k5eAaPmAgFnS	přestat
dostačovat	dostačovat	k5eAaImF	dostačovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postavena	postaven	k2eAgFnSc1d1	postavena
historizující	historizující	k2eAgFnSc1d1	historizující
Nová	nový	k2eAgFnSc1d1	nová
radnice	radnice	k1gFnSc1	radnice
(	(	kIx(	(
<g/>
Neues	Neues	k1gInSc1	Neues
Rathaus	rathaus	k1gInSc1	rathaus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
i	i	k9	i
budova	budova	k1gFnSc1	budova
německého	německý	k2eAgInSc2d1	německý
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
Bundesverwaltungsgericht	Bundesverwaltungsgericht	k1gInSc1	Bundesverwaltungsgericht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vystavěná	vystavěný	k2eAgFnSc1d1	vystavěná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1888	[number]	k4	1888
-	-	kIx~	-
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
architektonicky	architektonicky	k6eAd1	architektonicky
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
italskou	italský	k2eAgFnSc4d1	italská
pozdní	pozdní	k2eAgFnSc4d1	pozdní
renesanci	renesance	k1gFnSc4	renesance
a	a	k8xC	a
francouzské	francouzský	k2eAgNnSc4d1	francouzské
baroko	baroko	k1gNnSc4	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
podobá	podobat	k5eAaImIp3nS	podobat
budově	budova	k1gFnSc3	budova
Reichstagu	Reichstag	k1gInSc2	Reichstag
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
stavěn	stavit	k5eAaImNgInS	stavit
v	v	k7c6	v
obdobném	obdobný	k2eAgInSc6d1	obdobný
stylu	styl	k1gInSc6	styl
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Lipsko	Lipsko	k1gNnSc1	Lipsko
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
proslulo	proslout	k5eAaPmAgNnS	proslout
hudbou	hudba	k1gFnSc7	hudba
-	-	kIx~	-
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
skladatel	skladatel	k1gMnSc1	skladatel
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
několik	několik	k4yIc1	několik
operních	operní	k2eAgInPc2d1	operní
domů	dům	k1gInPc2	dům
-	-	kIx~	-
např.	např.	kA	např.
Gewandhaus	Gewandhaus	k1gInSc1	Gewandhaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kostele	kostel	k1gInSc6	kostel
Sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
(	(	kIx(	(
<g/>
Thomaskirche	Thomaskirche	k1gInSc1	Thomaskirche
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Bach	Bach	k1gMnSc1	Bach
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1723	[number]	k4	1723
<g/>
-	-	kIx~	-
<g/>
1750	[number]	k4	1750
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
konají	konat	k5eAaImIp3nP	konat
pravidelně	pravidelně	k6eAd1	pravidelně
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1212	[number]	k4	1212
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
i	i	k9	i
světoznámý	světoznámý	k2eAgInSc1d1	světoznámý
pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
"	"	kIx"	"
<g/>
Thomanerchor	Thomanerchor	k1gInSc1	Thomanerchor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
100	[number]	k4	100
chlapců	chlapec	k1gMnPc2	chlapec
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
9	[number]	k4	9
-	-	kIx~	-
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jej	on	k3xPp3gNnSc4	on
vedl	vést	k5eAaImAgInS	vést
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
i	i	k8xC	i
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
Kostel	kostel	k1gInSc1	kostel
Sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
(	(	kIx(	(
<g/>
Nikolaikirche	Nikolaikirche	k1gInSc1	Nikolaikirche
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vystavěný	vystavěný	k2eAgInSc1d1	vystavěný
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1165	[number]	k4	1165
v	v	k7c6	v
románském	románský	k2eAgInSc6d1	románský
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
středověku	středověk	k1gInSc6	středověk
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
gotický	gotický	k2eAgInSc4d1	gotický
halový	halový	k2eAgInSc4d1	halový
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
existence	existence	k1gFnSc2	existence
NDR	NDR	kA	NDR
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
místem	místo	k1gNnSc7	místo
mírových	mírový	k2eAgFnPc2d1	mírová
modliteb	modlitba	k1gFnPc2	modlitba
a	a	k8xC	a
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
tzv.	tzv.	kA	tzv.
pondělních	pondělní	k2eAgFnPc2d1	pondělní
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
široká	široký	k2eAgFnSc1d1	široká
veřejnost	veřejnost	k1gFnSc1	veřejnost
svou	svůj	k3xOyFgFnSc4	svůj
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
se	s	k7c7	s
stagnujícím	stagnující	k2eAgInSc7d1	stagnující
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Bitvu	bitva	k1gFnSc4	bitva
národů	národ	k1gInPc2	národ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1813	[number]	k4	1813
upomíná	upomínat	k5eAaImIp3nS	upomínat
nejmonumentálnější	monumentální	k2eAgInSc4d3	nejmonumentálnější
pomník	pomník	k1gInSc4	pomník
Německa	Německo	k1gNnSc2	Německo
-	-	kIx~	-
Pomník	pomník	k1gInSc1	pomník
Bitvy	bitva	k1gFnSc2	bitva
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
Völkerschlachtdenkmal	Völkerschlachtdenkmal	k1gInSc1	Völkerschlachtdenkmal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokončený	dokončený	k2eAgInSc1d1	dokončený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
typických	typický	k2eAgInPc2d1	typický
znaků	znak	k1gInPc2	znak
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
pomníku	pomník	k1gInSc2	pomník
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
plošina	plošina	k1gFnSc1	plošina
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
lze	lze	k6eAd1	lze
dohlédnou	dohlédnout	k5eAaPmIp3nP	dohlédnout
až	až	k9	až
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
"	"	kIx"	"
<g/>
City-Hochhaus	City-Hochhaus	k1gMnSc1	City-Hochhaus
Leipzig	Leipzig	k1gMnSc1	Leipzig
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
142	[number]	k4	142
metrů	metr	k1gInPc2	metr
pozorovat	pozorovat	k5eAaImF	pozorovat
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Lipské	lipský	k2eAgNnSc1d1	Lipské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
dokončené	dokončený	k2eAgNnSc1d1	dokončené
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
plochou	plocha	k1gFnSc7	plocha
83.640	[number]	k4	83.640
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
metrů	metr	k1gInPc2	metr
největší	veliký	k2eAgInSc1d3	veliký
hlavovou	hlavový	k2eAgFnSc7d1	hlavová
stanicí	stanice	k1gFnSc7	stanice
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
jím	on	k3xPp3gInSc7	on
projde	projít	k5eAaPmIp3nS	projít
na	na	k7c4	na
120	[number]	k4	120
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
čelní	čelní	k2eAgFnSc1d1	čelní
fasáda	fasáda	k1gFnSc1	fasáda
je	být	k5eAaImIp3nS	být
široká	široký	k2eAgFnSc1d1	široká
skoro	skoro	k6eAd1	skoro
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Lipsko	Lipsko	k1gNnSc1	Lipsko
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
pověst	pověst	k1gFnSc1	pověst
tradičního	tradiční	k2eAgNnSc2d1	tradiční
veletržního	veletržní	k2eAgNnSc2d1	veletržní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
a	a	k8xC	a
tržní	tržní	k2eAgFnSc1d1	tržní
práva	právo	k1gNnSc2	právo
dostalo	dostat	k5eAaPmAgNnS	dostat
Lipsko	Lipsko	k1gNnSc1	Lipsko
od	od	k7c2	od
markraběte	markrabě	k1gMnSc2	markrabě
Otty	Otta	k1gMnSc2	Otta
Míšeňského	míšeňský	k2eAgInSc2d1	míšeňský
roku	rok	k1gInSc2	rok
1165	[number]	k4	1165
<g/>
.	.	kIx.	.
</s>
<s>
Říšské	říšský	k2eAgNnSc1d1	říšské
tržní	tržní	k2eAgNnSc1d1	tržní
privilegium	privilegium	k1gNnSc1	privilegium
mu	on	k3xPp3gMnSc3	on
poté	poté	k6eAd1	poté
udělil	udělit	k5eAaPmAgMnS	udělit
císař	císař	k1gMnSc1	císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
1497	[number]	k4	1497
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konal	konat	k5eAaImAgInS	konat
první	první	k4xOgInSc1	první
vzorkový	vzorkový	k2eAgInSc1d1	vzorkový
veletrh	veletrh	k1gInSc1	veletrh
s	s	k7c7	s
oficiálním	oficiální	k2eAgInSc7d1	oficiální
katalogem	katalog	k1gInSc7	katalog
<g/>
.	.	kIx.	.
</s>
<s>
Lipský	lipský	k2eAgInSc1d1	lipský
veletržní	veletržní	k2eAgInSc1d1	veletržní
úřad	úřad	k1gInSc1	úřad
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
ale	ale	k8xC	ale
až	až	k6eAd1	až
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
ukončení	ukončení	k1gNnSc6	ukončení
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konal	konat	k5eAaImAgInS	konat
první	první	k4xOgInSc1	první
Lipský	lipský	k2eAgInSc1d1	lipský
jarní	jarní	k2eAgInSc1d1	jarní
veletrh	veletrh	k1gInSc1	veletrh
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konaly	konat	k5eAaImAgInP	konat
první	první	k4xOgInPc1	první
odborné	odborný	k2eAgInPc1d1	odborný
veletrhy	veletrh	k1gInPc1	veletrh
k	k	k7c3	k
tématům	téma	k1gNnPc3	téma
stavba	stavba	k1gFnSc1	stavba
a	a	k8xC	a
cestování	cestování	k1gNnSc1	cestování
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
Lipský	lipský	k2eAgInSc1d1	lipský
veletržní	veletržní	k2eAgInSc1d1	veletržní
úřad	úřad	k1gInSc1	úřad
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
znovusjednocení	znovusjednocení	k1gNnSc6	znovusjednocení
Německa	Německo	k1gNnSc2	Německo
transformován	transformovat	k5eAaBmNgInS	transformovat
ve	v	k7c4	v
společnost	společnost	k1gFnSc4	společnost
Lipský	lipský	k2eAgInSc1d1	lipský
veletrh	veletrh	k1gInSc1	veletrh
(	(	kIx(	(
<g/>
Leipziger	Leipziger	k1gInSc1	Leipziger
Messe	Messe	k1gFnSc2	Messe
<g/>
,	,	kIx,	,
zal	zal	k?	zal
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
mají	mít	k5eAaImIp3nP	mít
stát	stát	k5eAaImF	stát
Sasko	Sasko	k1gNnSc4	Sasko
a	a	k8xC	a
město	město	k1gNnSc1	město
Lipsko	Lipsko	k1gNnSc1	Lipsko
podíl	podíl	k1gInSc1	podíl
po	po	k7c6	po
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
kolem	kolem	k7c2	kolem
30	[number]	k4	30
odborných	odborný	k2eAgInPc2d1	odborný
i	i	k8xC	i
spotřebitelských	spotřebitelský	k2eAgInPc2d1	spotřebitelský
veletrhů	veletrh	k1gInPc2	veletrh
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
výstavní	výstavní	k2eAgFnPc1d1	výstavní
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zničeny	zničit	k5eAaPmNgInP	zničit
za	za	k7c2	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
starého	starý	k2eAgNnSc2d1	staré
výstaviště	výstaviště	k1gNnSc2	výstaviště
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunismu	komunismus	k1gInSc2	komunismus
nevyhovující	vyhovující	k2eNgFnSc2d1	nevyhovující
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
výstaviště	výstaviště	k1gNnSc1	výstaviště
nové	nový	k2eAgNnSc1d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
například	například	k6eAd1	například
Lipský	lipský	k2eAgInSc1d1	lipský
knižní	knižní	k2eAgInSc1d1	knižní
veletrh	veletrh	k1gInSc1	veletrh
(	(	kIx(	(
<g/>
Leipziger	Leipziger	k1gInSc1	Leipziger
Buchmesse	Buchmesse	k1gFnSc2	Buchmesse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblíben	oblíben	k2eAgInSc1d1	oblíben
je	být	k5eAaImIp3nS	být
i	i	k9	i
automobilový	automobilový	k2eAgInSc1d1	automobilový
veletrh	veletrh	k1gInSc1	veletrh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
též	též	k9	též
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
historie	historie	k1gFnSc2	historie
města	město	k1gNnSc2	město
Lipska	Lipsko	k1gNnSc2	Lipsko
(	(	kIx(	(
<g/>
Stadtgeschichtliches	Stadtgeschichtliches	k1gInSc1	Stadtgeschichtliches
Museum	museum	k1gNnSc1	museum
Leipzig	Leipzig	k1gInSc1	Leipzig
<g/>
)	)	kIx)	)
-	-	kIx~	-
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
např.	např.	kA	např.
výstava	výstava	k1gFnSc1	výstava
k	k	k7c3	k
historii	historie	k1gFnSc3	historie
města	město	k1gNnSc2	město
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Stare	star	k1gInSc5	star
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
Pomník	pomník	k1gInSc1	pomník
Bitvy	bitva	k1gFnSc2	bitva
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
muzeum	muzeum	k1gNnSc1	muzeum
kávy	káva	k1gFnSc2	káva
"	"	kIx"	"
<g/>
Coffe	Coff	k1gInSc5	Coff
Baum	Baum	k1gMnSc1	Baum
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
Památník	památník	k1gInSc1	památník
a	a	k8xC	a
muzeum	muzeum	k1gNnSc1	muzeum
"	"	kIx"	"
<g/>
Runde	Rund	k1gMnSc5	Rund
Ecke	Eckus	k1gMnSc5	Eckus
<g/>
"	"	kIx"	"
-	-	kIx~	-
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
<g />
.	.	kIx.	.
</s>
<s>
činnost	činnost	k1gFnSc1	činnost
státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
NDR	NDR	kA	NDR
Fórum	fórum	k1gNnSc1	fórum
moderní	moderní	k2eAgFnSc2d1	moderní
historie	historie	k1gFnSc2	historie
(	(	kIx(	(
<g/>
Zeitgeschichtliches	Zeitgeschichtliches	k1gInSc1	Zeitgeschichtliches
Forum	forum	k1gNnSc1	forum
Leipzig	Leipzig	k1gInSc1	Leipzig
<g/>
)	)	kIx)	)
-	-	kIx~	-
mapuje	mapovat	k5eAaImIp3nS	mapovat
historii	historie	k1gFnSc4	historie
východního	východní	k2eAgNnSc2d1	východní
Německa	Německo	k1gNnSc2	Německo
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
až	až	k9	až
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
Bachovo	Bachův	k2eAgNnSc1d1	Bachovo
muzeum	muzeum	k1gNnSc1	muzeum
Lipsko	Lipsko	k1gNnSc1	Lipsko
(	(	kIx(	(
<g/>
Bachmuseum	Bachmuseum	k1gNnSc1	Bachmuseum
Leipzig	Leipziga	k1gFnPc2	Leipziga
<g/>
)	)	kIx)	)
-	-	kIx~	-
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
život	život	k1gInSc4	život
a	a	k8xC	a
dílo	dílo	k1gNnSc4	dílo
Johanna	Johanno	k1gNnSc2	Johanno
Sebastiana	Sebastian	k1gMnSc2	Sebastian
Bacha	Bacha	k?	Bacha
Muzeum	muzeum	k1gNnSc4	muzeum
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Naturkundemuseum	Naturkundemuseum	k1gInSc4	Naturkundemuseum
<g/>
)	)	kIx)	)
Muzeum	muzeum	k1gNnSc4	muzeum
Grassi	Grasse	k1gFnSc4	Grasse
(	(	kIx(	(
<g/>
Grassimuseum	Grassimuseum	k1gInSc4	Grassimuseum
<g/>
)	)	kIx)	)
-	-	kIx~	-
tvoří	tvořit	k5eAaImIp3nS	tvořit
ho	on	k3xPp3gNnSc4	on
3	[number]	k4	3
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
muzeum	muzeum	k1gNnSc1	muzeum
aplikovaného	aplikovaný	k2eAgNnSc2d1	aplikované
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
etnologie	etnologie	k1gFnSc2	etnologie
a	a	k8xC	a
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
)	)	kIx)	)
Univerzita	univerzita	k1gFnSc1	univerzita
Lipsko	Lipsko	k1gNnSc4	Lipsko
(	(	kIx(	(
<g/>
Universität	Universität	k1gMnSc1	Universität
Leipzig	Leipzig	k1gMnSc1	Leipzig
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1409	[number]	k4	1409
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Heidelbergu	Heidelberg	k1gInSc6	Heidelberg
(	(	kIx(	(
<g/>
1386	[number]	k4	1386
<g/>
)	)	kIx)	)
druhou	druhý	k4xOgFnSc7	druhý
nejstarší	starý	k2eAgFnSc7d3	nejstarší
fungující	fungující	k2eAgFnSc7d1	fungující
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitu	univerzita	k1gFnSc4	univerzita
založili	založit	k5eAaPmAgMnP	založit
studenti	student	k1gMnPc1	student
a	a	k8xC	a
profesoři	profesor	k1gMnPc1	profesor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
Dekretu	dekret	k1gInSc2	dekret
Kutnohorského	kutnohorský	k2eAgInSc2d1	kutnohorský
opustili	opustit	k5eAaPmAgMnP	opustit
Univerzitu	univerzita	k1gFnSc4	univerzita
Karlovu	Karlův	k2eAgFnSc4d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgInSc6d1	zimní
semestru	semestr	k1gInSc6	semestr
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
26	[number]	k4	26
772	[number]	k4	772
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
knižnice	knižnice	k1gFnSc1	knižnice
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
a	a	k8xC	a
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
sloužila	sloužit	k5eAaImAgFnS	sloužit
až	až	k9	až
do	do	k7c2	do
rozdělení	rozdělení	k1gNnSc2	rozdělení
Německa	Německo	k1gNnSc2	Německo
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgInPc4d1	světový
válce	válec	k1gInPc4	válec
jako	jako	k8xS	jako
sběrné	sběrný	k2eAgNnSc4d1	sběrné
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
veškerou	veškerý	k3xTgFnSc4	veškerý
německy	německy	k6eAd1	německy
psanou	psaný	k2eAgFnSc4d1	psaná
literaturu	literatura	k1gFnSc4	literatura
vydanou	vydaný	k2eAgFnSc4d1	vydaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
knihovna	knihovna	k1gFnSc1	knihovna
včleněna	včlenit	k5eAaPmNgFnS	včlenit
do	do	k7c2	do
Německé	německý	k2eAgFnSc2d1	německá
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
součástí	součást	k1gFnSc7	součást
Německé	německý	k2eAgFnSc2d1	německá
národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
(	(	kIx(	(
<g/>
Deutsche	Deutsche	k1gFnSc1	Deutsche
Nationalbibliothek	Nationalbibliothek	k1gMnSc1	Nationalbibliothek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
založená	založený	k2eAgFnSc1d1	založená
Německá	německý	k2eAgFnSc1d1	německá
knihovna	knihovna	k1gFnSc1	knihovna
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
založený	založený	k2eAgInSc1d1	založený
Hudební	hudební	k2eAgInSc1d1	hudební
archiv	archiv	k1gInSc1	archiv
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
na	na	k7c4	na
38	[number]	k4	38
000	[number]	k4	000
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
zaregistrovaných	zaregistrovaný	k2eAgNnPc2d1	zaregistrované
u	u	k7c2	u
místní	místní	k2eAgFnSc2d1	místní
živnostenské	živnostenský	k2eAgFnSc2d1	Živnostenská
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
činných	činný	k2eAgFnPc2d1	činná
na	na	k7c6	na
5	[number]	k4	5
000	[number]	k4	000
řemeslnických	řemeslnický	k2eAgFnPc2d1	řemeslnická
firem	firma	k1gFnPc2	firma
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2014	[number]	k4	2014
9,9	[number]	k4	9,9
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
regionech	region	k1gInPc6	region
východního	východní	k2eAgNnSc2d1	východní
Německa	Německo	k1gNnSc2	Německo
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
NDR	NDR	kA	NDR
ke	k	k7c3	k
zhroucení	zhroucení	k1gNnSc3	zhroucení
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
postupně	postupně	k6eAd1	postupně
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
zaměstnavatele	zaměstnavatel	k1gMnPc4	zaměstnavatel
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Siemens	siemens	k1gInSc1	siemens
(	(	kIx(	(
<g/>
1	[number]	k4	1
700	[number]	k4	700
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Porsche	Porsche	k1gNnPc1	Porsche
(	(	kIx(	(
<g/>
2	[number]	k4	2
500	[number]	k4	500
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
u	u	k7c2	u
Porsche	Porsche	k1gNnSc2	Porsche
Leipzig	Leipziga	k1gFnPc2	Leipziga
plus	plus	k6eAd1	plus
dalších	další	k2eAgFnPc2d1	další
800	[number]	k4	800
ve	v	k7c6	v
služebních	služební	k2eAgNnPc6d1	služební
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
)	)	kIx)	)
či	či	k8xC	či
BMW	BMW	kA	BMW
(	(	kIx(	(
<g/>
3	[number]	k4	3
700	[number]	k4	700
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
partnery	partner	k1gMnPc7	partner
a	a	k8xC	a
dodavateli	dodavatel	k1gMnPc7	dodavatel
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
na	na	k7c4	na
6	[number]	k4	6
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
Lipska	Lipsko	k1gNnSc2	Lipsko
(	(	kIx(	(
<g/>
Leipziger	Leipziger	k1gInSc1	Leipziger
Verkehrsbetriebe	Verkehrsbetrieb	k1gMnSc5	Verkehrsbetrieb
<g/>
)	)	kIx)	)
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
i	i	k8xC	i
vlakové	vlakový	k2eAgFnPc1d1	vlaková
soupravy	souprava	k1gFnPc1	souprava
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
na	na	k7c4	na
2	[number]	k4	2
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
a	a	k8xC	a
letišti	letiště	k1gNnSc6	letiště
je	být	k5eAaImIp3nS	být
region	region	k1gInSc1	region
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
logistických	logistický	k2eAgNnPc2d1	logistické
center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
zřídil	zřídit	k5eAaPmAgMnS	zřídit
Amazon	amazona	k1gFnPc2	amazona
své	svůj	k3xOyFgMnPc4	svůj
druhé	druhý	k4xOgFnSc6	druhý
a	a	k8xC	a
doposud	doposud	k6eAd1	doposud
největší	veliký	k2eAgNnSc4d3	veliký
logistické	logistický	k2eAgNnSc4d1	logistické
centrum	centrum	k1gNnSc4	centrum
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
logistické	logistický	k2eAgNnSc1d1	logistické
centrum	centrum	k1gNnSc1	centrum
DHL	DHL	kA	DHL
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
S-Bahn	S-Bahna	k1gFnPc2	S-Bahna
ve	v	k7c6	v
Středním	střední	k2eAgNnSc6d1	střední
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
a	a	k8xC	a
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Lipska	Lipsko	k1gNnSc2	Lipsko
probíhá	probíhat	k5eAaImIp3nS	probíhat
několik	několik	k4yIc4	několik
dálnic	dálnice	k1gFnPc2	dálnice
-	-	kIx~	-
na	na	k7c6	na
severu	sever	k1gInSc6	sever
dálnice	dálnice	k1gFnSc2	dálnice
A	A	kA	A
14	[number]	k4	14
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
A	A	kA	A
9	[number]	k4	9
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
A	A	kA	A
38	[number]	k4	38
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dálnice	dálnice	k1gFnPc1	dálnice
tvoří	tvořit	k5eAaImIp3nP	tvořit
současně	současně	k6eAd1	současně
obchvat	obchvat	k1gInSc4	obchvat
Lipska	Lipsko	k1gNnSc2	Lipsko
a	a	k8xC	a
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
města	město	k1gNnSc2	město
Halle	Halle	k1gFnSc2	Halle
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
na	na	k7c4	na
Chemnitz	Chemnitz	k1gInSc4	Chemnitz
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
momentálně	momentálně	k6eAd1	momentálně
staví	stavit	k5eAaBmIp3nS	stavit
nová	nový	k2eAgFnSc1d1	nová
dálnice	dálnice	k1gFnSc1	dálnice
A	a	k9	a
72	[number]	k4	72
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nP	křížit
trasy	trasa	k1gFnPc1	trasa
vlaků	vlak	k1gInPc2	vlak
Intercity-Express	Intercity-Expressa	k1gFnPc2	Intercity-Expressa
(	(	kIx(	(
<g/>
Hamburg	Hamburg	k1gInSc4	Hamburg
<g/>
-	-	kIx~	-
<g/>
Berlin	berlina	k1gFnPc2	berlina
<g/>
-	-	kIx~	-
<g/>
Leipzig	Leipzig	k1gInSc1	Leipzig
<g/>
-	-	kIx~	-
<g/>
Nürnberg	Nürnberg	k1gInSc1	Nürnberg
<g/>
-	-	kIx~	-
<g/>
München	München	k1gInSc1	München
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Dresden	Dresdna	k1gFnPc2	Dresdna
<g/>
-	-	kIx~	-
<g/>
Leipzig	Leipzig	k1gInSc1	Leipzig
<g/>
-	-	kIx~	-
<g/>
Erfurt	Erfurt	k1gInSc1	Erfurt
<g/>
-	-	kIx~	-
<g/>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
am	am	k?	am
Main	Main	k1gInSc1	Main
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
Wiesbaden	Wiesbaden	k1gInSc1	Wiesbaden
<g/>
/	/	kIx~	/
<g/>
Saarbrücken	Saarbrücken	k1gInSc1	Saarbrücken
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
vlaku	vlak	k1gInSc2	vlak
Intercity	Intercit	k1gInPc1	Intercit
pro	pro	k7c4	pro
trasu	trasa	k1gFnSc4	trasa
Dresden	Dresdna	k1gFnPc2	Dresdna
<g/>
-	-	kIx~	-
<g/>
Leipzig	Leipzig	k1gInSc1	Leipzig
<g/>
-	-	kIx~	-
<g/>
Magdeburg	Magdeburg	k1gInSc1	Magdeburg
<g/>
-	-	kIx~	-
<g/>
Braunschweig	Braunschweig	k1gInSc1	Braunschweig
<g/>
-	-	kIx~	-
<g/>
Hannover	Hannover	k1gInSc1	Hannover
<g/>
-	-	kIx~	-
<g/>
Dortmund	Dortmund	k1gInSc1	Dortmund
<g/>
-	-	kIx~	-
<g/>
Köln	Köln	k1gInSc1	Köln
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
dále	daleko	k6eAd2	daleko
staví	stavit	k5eAaBmIp3nS	stavit
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
noční	noční	k2eAgFnPc4d1	noční
spoje	spoj	k1gFnPc4	spoj
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Curychu	Curych	k1gInSc2	Curych
<g/>
,	,	kIx,	,
Basileje	Basilej	k1gFnSc2	Basilej
či	či	k8xC	či
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgNnSc1d1	regionální
železniční	železniční	k2eAgFnSc7d1	železniční
dopravou	doprava	k1gFnSc7	doprava
lze	lze	k6eAd1	lze
bez	bez	k7c2	bez
přestupu	přestup	k1gInSc2	přestup
dojet	dojet	k5eAaPmF	dojet
do	do	k7c2	do
většiny	většina	k1gFnSc2	většina
velkých	velký	k2eAgInPc2d1	velký
a	a	k8xC	a
středně	středně	k6eAd1	středně
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
v	v	k7c6	v
Sasku	Sasko	k1gNnSc6	Sasko
a	a	k8xC	a
jižním	jižní	k2eAgInSc6d1	jižní
Sasku-Anhaltsku	Sasku-Anhaltsek	k1gInSc6	Sasku-Anhaltsek
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sousedním	sousední	k2eAgNnSc7d1	sousední
městem	město	k1gNnSc7	město
Halle	Halle	k1gFnSc2	Halle
je	být	k5eAaImIp3nS	být
Lipsko	Lipsko	k1gNnSc1	Lipsko
spojeno	spojit	k5eAaPmNgNnS	spojit
S-Bahnem	S-Bahn	k1gInSc7	S-Bahn
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Lipsko-Halle	Lipsko-Halle	k1gNnSc2	Lipsko-Halle
(	(	kIx(	(
<g/>
Flughafen	Flughafen	k2eAgInSc1d1	Flughafen
Leipzig	Leipzig	k1gInSc1	Leipzig
<g/>
/	/	kIx~	/
<g/>
Halle	Halle	k1gInSc1	Halle
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
letištěm	letiště	k1gNnSc7	letiště
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgFnPc4	jenž
je	být	k5eAaImIp3nS	být
napojeno	napojit	k5eAaPmNgNnS	napojit
linkami	linka	k1gFnPc7	linka
S-Bahn	S-Bahno	k1gNnPc2	S-Bahno
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
nákladové	nákladový	k2eAgNnSc4d1	nákladové
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gottfried	Gottfried	k1gMnSc1	Gottfried
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Leibniz	Leibniz	k1gMnSc1	Leibniz
(	(	kIx(	(
<g/>
1646	[number]	k4	1646
<g/>
-	-	kIx~	-
<g/>
1716	[number]	k4	1716
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
diplomat	diplomat	k1gMnSc1	diplomat
a	a	k8xC	a
matematik	matematik	k1gMnSc1	matematik
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
(	(	kIx(	(
<g/>
1685	[number]	k4	1685
<g/>
-	-	kIx~	-
<g/>
1750	[number]	k4	1750
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Lipsku	Lipsko	k1gNnSc3	Lipsko
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
kantor	kantor	k1gMnSc1	kantor
chrámového	chrámový	k2eAgInSc2d1	chrámový
sboru	sbor	k1gInSc2	sbor
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Goethe	Goethe	k1gFnSc1	Goethe
(	(	kIx(	(
<g/>
1749	[number]	k4	1749
<g/>
-	-	kIx~	-
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
studoval	studovat	k5eAaImAgMnS	studovat
a	a	k8xC	a
situoval	situovat	k5eAaBmAgMnS	situovat
sem	sem	k6eAd1	sem
i	i	k9	i
děj	děj	k1gInSc1	děj
své	svůj	k3xOyFgFnSc2	svůj
veršované	veršovaný	k2eAgFnSc2d1	veršovaná
tragédie	tragédie	k1gFnSc2	tragédie
"	"	kIx"	"
<g/>
Faust	Faust	k1gMnSc1	Faust
<g/>
"	"	kIx"	"
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
(	(	kIx(	(
<g/>
1759	[number]	k4	1759
<g/>
-	-	kIx~	-
<g/>
1805	[number]	k4	1805
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
složil	složit	k5eAaPmAgMnS	složit
zde	zde	k6eAd1	zde
i	i	k9	i
svou	svůj	k3xOyFgFnSc4	svůj
slavnou	slavný	k2eAgFnSc4d1	slavná
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Óda	óda	k1gFnSc1	óda
na	na	k7c4	na
radost	radost	k1gFnSc4	radost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zhudebněnou	zhudebněný	k2eAgFnSc4d1	zhudebněná
L.	L.	kA	L.
van	van	k1gInSc1	van
Beethovenem	Beethoven	k1gMnSc7	Beethoven
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
jeho	on	k3xPp3gNnSc2	on
9	[number]	k4	9
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnSc1	symfonie
<g/>
)	)	kIx)	)
Felix	Felix	k1gMnSc1	Felix
Mendelssohn-Bartholdy	Mendelssohn-Bartholda	k1gMnSc2	Mendelssohn-Bartholda
(	(	kIx(	(
<g/>
1809	[number]	k4	1809
<g/>
-	-	kIx~	-
<g/>
1847	[number]	k4	1847
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
německých	německý	k2eAgMnPc2d1	německý
romantických	romantický	k2eAgMnPc2d1	romantický
skladatelů	skladatel	k1gMnPc2	skladatel
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
-	-	kIx~	-
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Emil	Emil	k1gMnSc1	Emil
Jellinek	Jellinka	k1gFnPc2	Jellinka
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
automobilismu	automobilismus	k1gInSc2	automobilismus
<g />
.	.	kIx.	.
</s>
<s>
Karl	Karl	k1gMnSc1	Karl
Liebknecht	Liebknecht	k1gMnSc1	Liebknecht
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
socialista	socialista	k1gMnSc1	socialista
<g/>
)	)	kIx)	)
Werner	Werner	k1gMnSc1	Werner
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
Angela	Angela	k1gFnSc1	Angela
Merkelová	Merkelová	k1gFnSc1	Merkelová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
kancléřka	kancléřka	k1gFnSc1	kancléřka
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
zdejší	zdejší	k2eAgFnSc6d1	zdejší
univerzitě	univerzita	k1gFnSc6	univerzita
Bill	Bill	k1gMnSc1	Bill
Kaulitz	Kaulitz	k1gMnSc1	Kaulitz
a	a	k8xC	a
Tom	Tom	k1gMnSc1	Tom
Kaulitz	Kaulitz	k1gMnSc1	Kaulitz
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvojčata	dvojče	k1gNnPc1	dvojče
z	z	k7c2	z
populární	populární	k2eAgFnSc2d1	populární
německé	německý	k2eAgFnSc2d1	německá
skupiny	skupina	k1gFnSc2	skupina
Tokio	Tokio	k1gNnSc1	Tokio
Hotel	hotel	k1gInSc1	hotel
</s>
