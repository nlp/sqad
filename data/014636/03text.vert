<s>
Jan	Jan	k1gMnSc1
Grois	Grois	k1gFnSc2
</s>
<s>
Jan	Jan	k1gMnSc1
Grois	Grois	k1gFnSc2
<g/>
,	,	kIx,
MBA	MBA	kA
</s>
<s>
Zastupitel	zastupitel	k1gMnSc1
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2016	#num#	k4
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
</s>
<s>
Starosta	Starosta	k1gMnSc1
města	město	k1gNnSc2
Znojmo	Znojmo	k1gNnSc1
Úřadující	úřadující	k2eAgFnSc2d1
</s>
<s>
Ve	v	k7c4
funkci	funkce	k1gFnSc4
od	od	k7c2
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2018	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Vlastimil	Vlastimil	k1gMnSc1
Gabrhel	Gabrhel	k1gMnSc1
</s>
<s>
Zastupitel	zastupitel	k1gMnSc1
města	město	k1gNnSc2
Znojmo	Znojmo	k1gNnSc1
<g/>
(	(	kIx(
<g/>
v	v	k7c6
letech	let	k1gInPc6
2010	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
také	také	k9
místostarosta	místostarosta	k1gMnSc1
města	město	k1gNnSc2
<g/>
)	)	kIx)
Úřadující	úřadující	k2eAgInSc1d1
</s>
<s>
Ve	v	k7c4
funkci	funkce	k1gFnSc4
od	od	k7c2
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2010	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1980	#num#	k4
(	(	kIx(
<g/>
40	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
ZnojmoČeskoslovensko	ZnojmoČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Choť	choť	k1gMnSc1
</s>
<s>
ženatý	ženatý	k2eAgMnSc1d1
Děti	dítě	k1gFnPc1
</s>
<s>
dva	dva	k4xCgMnPc1
synové	syn	k1gMnPc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Znojmo	Znojmo	k1gNnSc1
Profese	profes	k1gFnSc2
</s>
<s>
politik	politik	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Grois	Grois	k1gMnSc1xF
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1980	#num#	k4
Znojmo	Znojmo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
politik	politik	k1gMnSc1
a	a	k8xC
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
2016	#num#	k4
až	až	k6
2020	#num#	k4
zastupitel	zastupitel	k1gMnSc1
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
února	únor	k1gInSc2
2018	#num#	k4
starosta	starosta	k1gMnSc1
města	město	k1gNnSc2
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
2010	#num#	k4
až	až	k6
2018	#num#	k4
místostarosta	místostarosta	k1gMnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
ČSSD	ČSSD	kA
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Po	po	k7c6
maturitě	maturita	k1gFnSc6
začal	začít	k5eAaPmAgInS
podnikat	podnikat	k5eAaImF
v	v	k7c6
oblasti	oblast	k1gFnSc6
gastronomie	gastronomie	k1gFnSc2
a	a	k8xC
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
své	svůj	k3xOyFgNnSc4
vzdělání	vzdělání	k1gNnSc4
si	se	k3xPyFc3
později	pozdě	k6eAd2
doplnil	doplnit	k5eAaPmAgMnS
o	o	k7c4
titul	titul	k1gInSc4
MBA	MBA	kA
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Grois	Grois	k1gFnSc2
žije	žít	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
Znojmo	Znojmo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mládí	mládí	k1gNnSc6
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
folklóru	folklór	k1gInSc2
(	(	kIx(
<g/>
byl	být	k5eAaImAgInS
členem	člen	k1gInSc7
spolku	spolek	k1gInSc2
Dyjavánek	Dyjavánka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gNnSc4
záliby	zálib	k1gInPc1
patří	patřit	k5eAaImIp3nP
sport	sport	k1gInSc4
(	(	kIx(
<g/>
vodní	vodní	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
,	,	kIx,
běhání	běhání	k1gNnSc4
<g/>
,	,	kIx,
funkční	funkční	k2eAgInSc4d1
trénink	trénink	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
turistika	turistika	k1gFnSc1
a	a	k8xC
cestování	cestování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Politické	politický	k2eAgNnSc1d1
působení	působení	k1gNnSc1
</s>
<s>
V	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
letech	léto	k1gNnPc6
2002	#num#	k4
a	a	k8xC
2006	#num#	k4
kandidoval	kandidovat	k5eAaImAgInS
za	za	k7c4
ČSSD	ČSSD	kA
do	do	k7c2
Zastupitelstva	zastupitelstvo	k1gNnSc2
města	město	k1gNnSc2
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
neuspěl	uspět	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zvolen	Zvolen	k1gInSc1
byl	být	k5eAaImAgInS
až	až	k9
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
listopadu	listopad	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
2010	#num#	k4
se	se	k3xPyFc4
navíc	navíc	k6eAd1
stal	stát	k5eAaPmAgMnS
místostarostou	místostarosta	k1gMnSc7
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
byl	být	k5eAaImAgMnS
opět	opět	k6eAd1
zvolen	zvolit	k5eAaPmNgMnS
zastupitelem	zastupitel	k1gMnSc7
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
ve	v	k7c6
městě	město	k1gNnSc6
volby	volba	k1gFnSc2
vyhrála	vyhrát	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
získala	získat	k5eAaPmAgFnS
více	hodně	k6eAd2
než	než	k8xS
31	#num#	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
11	#num#	k4
z	z	k7c2
31	#num#	k4
mandátů	mandát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSSD	ČSSD	kA
pak	pak	k6eAd1
uzavřela	uzavřít	k5eAaPmAgFnS
koalici	koalice	k1gFnSc4
s	s	k7c7
hnutím	hnutí	k1gNnSc7
ANO	ano	k9
2011	#num#	k4
za	za	k7c2
podpory	podpora	k1gFnSc2
jednoho	jeden	k4xCgMnSc2
zastupitele	zastupitel	k1gMnSc2
zvoleného	zvolený	k2eAgMnSc2d1
za	za	k7c7
KSČM	KSČM	kA
a	a	k8xC
Grois	Grois	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
listopadu	listopad	k1gInSc6
2014	#num#	k4
opět	opět	k6eAd1
zvolen	zvolit	k5eAaPmNgMnS
místostarostou	místostarosta	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
dosavadní	dosavadní	k2eAgMnSc1d1
starosta	starosta	k1gMnSc1
Vlastimil	Vlastimil	k1gMnSc1
Gabrhel	Gabrhel	k1gMnSc1
na	na	k7c4
funkci	funkce	k1gFnSc4
starosty	starosta	k1gMnSc2
rezignoval	rezignovat	k5eAaBmAgMnS
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
na	na	k7c6
následném	následný	k2eAgNnSc6d1
zasedání	zasedání	k1gNnSc6
městského	městský	k2eAgNnSc2d1
zastupitelstva	zastupitelstvo	k1gNnSc2
zvolen	zvolit	k5eAaPmNgMnS
starostou	starosta	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
byla	být	k5eAaImAgFnS
utvořena	utvořen	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
ze	z	k7c2
sociální	sociální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
,	,	kIx,
ANO	ano	k9
2011	#num#	k4
a	a	k8xC
občanských	občanský	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grois	Grois	k1gInSc1
se	se	k3xPyFc4
opětovně	opětovně	k6eAd1
stal	stát	k5eAaPmAgMnS
starostou	starosta	k1gMnSc7
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
krajských	krajský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
kandidoval	kandidovat	k5eAaImAgMnS
za	za	k7c2
ČSSD	ČSSD	kA
do	do	k7c2
Zastupitelstva	zastupitelstvo	k1gNnSc2
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
neuspěl	uspět	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Zvolen	Zvolen	k1gInSc1
byl	být	k5eAaImAgInS
až	až	k9
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
již	již	k6eAd1
nekandidoval	kandidovat	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Senátu	senát	k1gInSc2
PČR	PČR	kA
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
kandidoval	kandidovat	k5eAaImAgMnS
v	v	k7c6
obvodu	obvod	k1gInSc6
č.	č.	k?
54	#num#	k4
–	–	k?
Znojmo	Znojmo	k1gNnSc1
jako	jako	k8xC,k8xS
člen	člen	k1gInSc1
ČSSD	ČSSD	kA
za	za	k7c4
hnutí	hnutí	k1gNnSc4
Starostové	Starostová	k1gFnSc2
a	a	k8xC
osobnosti	osobnost	k1gFnSc2
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
(	(	kIx(
<g/>
SOM	soma	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
prvním	první	k4xOgInSc6
kole	kolo	k1gNnSc6
získal	získat	k5eAaPmAgMnS
25,75	25,75	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
postoupil	postoupit	k5eAaPmAgMnS
tak	tak	k6eAd1
ze	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnPc4
do	do	k7c2
druhého	druhý	k4xOgNnSc2
kola	kolo	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
prohrál	prohrát	k5eAaPmAgMnS
s	s	k7c7
kandidátem	kandidát	k1gMnSc7
ODS	ODS	kA
<g/>
,	,	kIx,
STAN	stan	k1gInSc1
a	a	k8xC
TOP	topit	k5eAaImRp2nS
09	#num#	k4
Tomášem	Tomáš	k1gMnSc7
Třetinou	třetina	k1gFnSc7
poměrem	poměr	k1gInSc7
hlasů	hlas	k1gInPc2
45,42	45,42	k4
%	%	kIx~
:	:	kIx,
54,57	54,57	k4
%	%	kIx~
a	a	k8xC
senátorem	senátor	k1gMnSc7
se	se	k3xPyFc4
tak	tak	k6eAd1
nestal	stát	k5eNaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2020	#num#	k4
plánovalo	plánovat	k5eAaImAgNnS
zastupitelstvo	zastupitelstvo	k1gNnSc1
jednat	jednat	k5eAaImF
o	o	k7c6
jeho	jeho	k3xOp3gNnSc6
odvolání	odvolání	k1gNnSc6
z	z	k7c2
funkce	funkce	k1gFnSc2
starosty	starosta	k1gMnSc2
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
však	však	k9
zastupitelé	zastupitel	k1gMnPc1
za	za	k7c2
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
to	ten	k3xDgNnSc4
původně	původně	k6eAd1
iniciovali	iniciovat	k5eAaBmAgMnP
<g/>
,	,	kIx,
otočili	otočit	k5eAaPmAgMnP
a	a	k8xC
tato	tento	k3xDgFnSc1
otázka	otázka	k1gFnSc1
se	se	k3xPyFc4
vůbec	vůbec	k9
nedostala	dostat	k5eNaPmAgFnS
na	na	k7c4
program	program	k1gInSc4
jednání	jednání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Novým	nový	k2eAgMnSc7d1
starostou	starosta	k1gMnSc7
Znojma	Znojmo	k1gNnSc2
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Grois	Grois	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastupitelé	zastupitel	k1gMnPc1
zvolili	zvolit	k5eAaPmAgMnP
i	i	k9
nového	nový	k2eAgMnSc4d1
místostarostu	místostarosta	k1gMnSc4
a	a	k8xC
předsedu	předseda	k1gMnSc4
finančního	finanční	k2eAgInSc2d1
výboru	výbor	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Znojmo	Znojmo	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
<g/>
:	:	kIx,
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
str	str	kA
<g/>
.	.	kIx.
<g/>
sociálně	sociálně	k6eAd1
demokrat	demokrat	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
<g/>
:	:	kIx,
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
str	str	kA
<g/>
.	.	kIx.
<g/>
sociálně	sociálně	k6eAd1
demokrat	demokrat	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
<g/>
:	:	kIx,
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
str	str	kA
<g/>
.	.	kIx.
<g/>
sociálně	sociálně	k6eAd1
demokrat	demokrat	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Usnesení	usnesení	k1gNnSc1
zastupitelstva	zastupitelstvo	k1gNnSc2
města	město	k1gNnSc2
Znojma	Znojmo	k1gNnSc2
č.	č.	k?
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
0	#num#	k4
<g/>
8.11	8.11	k4
<g/>
.2010	.2010	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
10.10	10.10	k4
<g/>
.	.	kIx.
-	-	kIx~
11.10	11.10	k4
<g/>
.2014	.2014	k4
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
<g/>
:	:	kIx,
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
str	str	kA
<g/>
.	.	kIx.
<g/>
sociálně	sociálně	k6eAd1
demokrat	demokrat	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SMOLA	Smola	k1gMnSc1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
;	;	kIx,
MOŠTĚK	Moštěk	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojmo	Znojmo	k1gNnSc1
má	mít	k5eAaImIp3nS
nové	nový	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
radnice	radnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starostou	Starosta	k1gMnSc7
bude	být	k5eAaImBp3nS
opět	opět	k6eAd1
Gabrhel	Gabrhel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemský	znojemský	k2eAgInSc1d1
deník	deník	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-11-10	2014-11-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Zuzana	Zuzana	k1gFnSc1
Pastrňáková	Pastrňáková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojmo	Znojmo	k1gNnSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
nového	nový	k2eAgMnSc4d1
starostu	starosta	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současný	současný	k2eAgInSc1d1
starosta	starosta	k1gMnSc1
ve	v	k7c6
funkci	funkce	k1gFnSc6
skončil	skončit	k5eAaPmAgMnS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Znojmo	Znojmo	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
redakce	redakce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	nový	k2eAgMnSc7d1
starostou	starosta	k1gMnSc7
Znojma	Znojmo	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
Grois	Grois	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemský	znojemský	k2eAgInSc1d1
týden	týden	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zastupitelé	zastupitel	k1gMnPc1
zvolili	zvolit	k5eAaPmAgMnP
nové	nový	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starostou	Starosta	k1gMnSc7
zůstává	zůstávat	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Grois	Grois	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Znojmo	Znojmo	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
<g/>
:	:	kIx,
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
str	str	kA
<g/>
.	.	kIx.
<g/>
sociálně	sociálně	k6eAd1
demokrat	demokrat	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
12.10	12.10	k4
<g/>
.	.	kIx.
–	–	k?
13.10	13.10	k4
<g/>
.2012	.2012	k4
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc1
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
7.10	7.10	k4
<g/>
.	.	kIx.
–	–	k?
8.10	8.10	k4
<g/>
.2016	.2016	k4
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc1
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Seznam	seznam	k1gInSc4
kandidátů	kandidát	k1gMnPc2
pro	pro	k7c4
podzimní	podzimní	k2eAgFnPc4d1
senátní	senátní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-06-18	2020-06-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Znojemský	znojemský	k2eAgMnSc1d1
starosta	starosta	k1gMnSc1
Grois	Grois	k1gFnSc2
se	se	k3xPyFc4
popere	poprat	k5eAaPmIp3nS
o	o	k7c4
Senát	senát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítl	odmítnout	k5eAaPmAgMnS
kandidaturu	kandidatura	k1gFnSc4
za	za	k7c4
ČSSD	ČSSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemský	znojemský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-07-23	2020-07-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jan	Jan	k1gMnSc1
Grois	Grois	k1gFnSc2
-	-	kIx~
starosta	starosta	k1gMnSc1
města	město	k1gNnSc2
Znojma	Znojmo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Facebook	Facebook	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2020-07-23	2020-07-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Senátu	senát	k1gInSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
2.10	2.10	k4
<g/>
.	.	kIx.
–	–	k?
3.10	3.10	k4
<g/>
.2020	.2020	k4
<g/>
,	,	kIx,
Výsledky	výsledek	k1gInPc1
hlasování	hlasování	k1gNnSc2
<g/>
,	,	kIx,
Obvod	obvod	k1gInSc1
<g/>
:	:	kIx,
54	#num#	k4
–	–	k?
Znojmo	Znojmo	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2020	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DUŠKOVÁ	Dušková	k1gFnSc1
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezničitelný	zničitelný	k2eNgInSc4d1
Grois	Grois	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemského	znojemský	k2eAgMnSc4d1
starostu	starosta	k1gMnSc4
zachránil	zachránit	k5eAaPmAgMnS
od	od	k7c2
převratu	převrat	k1gInSc2
přemet	přemet	k1gInSc4
ANO	ano	k9
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-12-09	2020-12-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Osobní	osobní	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Morava	Morava	k1gFnSc1
</s>
