<p>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
draselný	draselný	k2eAgInSc1d1	draselný
(	(	kIx(	(
<g/>
triviálně	triviálně	k6eAd1	triviálně
cyankáli	cyankáli	k1gNnSc1	cyankáli
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
draselná	draselný	k2eAgFnSc1d1	draselná
sůl	sůl	k1gFnSc1	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
kyanovodíkové	kyanovodíkový	k2eAgFnSc2d1	kyanovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
je	být	k5eAaImIp3nS	být
KCN	KCN	kA	KCN
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
látka	látka	k1gFnSc1	látka
reagující	reagující	k2eAgFnSc1d1	reagující
silně	silně	k6eAd1	silně
alkalicky	alkalicky	k6eAd1	alkalicky
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnSc7	reakce
se	s	k7c7	s
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
postupně	postupně	k6eAd1	postupně
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
uhličitan	uhličitan	k1gInSc4	uhličitan
draselný	draselný	k2eAgInSc4d1	draselný
(	(	kIx(	(
<g/>
potaš	potaš	k1gInSc4	potaš
<g/>
)	)	kIx)	)
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
plynného	plynný	k2eAgInSc2d1	plynný
kyanovodíku	kyanovodík	k1gInSc2	kyanovodík
(	(	kIx(	(
<g/>
HCN	HCN	kA	HCN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
páchne	páchnout	k5eAaImIp3nS	páchnout
po	po	k7c6	po
hořkých	hořký	k2eAgFnPc6d1	hořká
mandlích	mandle	k1gFnPc6	mandle
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gNnSc4	on
rozeznat	rozeznat	k5eAaPmF	rozeznat
čichem	čich	k1gInSc7	čich
při	při	k7c6	při
koncentraci	koncentrace	k1gFnSc6	koncentrace
od	od	k7c2	od
6	[number]	k4	6
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
asi	asi	k9	asi
4,65	[number]	k4	4,65
hmotnostních	hmotnostní	k2eAgNnPc2d1	hmotnostní
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
KCN	KCN	kA	KCN
+	+	kIx~	+
CO2	CO2	k1gMnSc1	CO2
+	+	kIx~	+
H2O	H2O	k1gMnSc1	H2O
(	(	kIx(	(
<g/>
vzd	vzd	k?	vzd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
→	→	k?	→
K2CO3	K2CO3	k1gFnSc1	K2CO3
+	+	kIx~	+
2	[number]	k4	2
HCN	HCN	kA	HCN
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
draselný	draselný	k2eAgInSc1d1	draselný
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
reakcí	reakce	k1gFnSc7	reakce
kyseliny	kyselina	k1gFnSc2	kyselina
kyanovodíkové	kyanovodíkový	k2eAgFnSc2d1	kyanovodíková
a	a	k8xC	a
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HCN	HCN	kA	HCN
+	+	kIx~	+
KOH	KOH	kA	KOH
→	→	k?	→
KCN	KCN	kA	KCN
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
ONebo	ONeba	k1gFnSc5	ONeba
reakcí	reakce	k1gFnPc2	reakce
formamidu	formamida	k1gFnSc4	formamida
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
draselným	draselný	k2eAgInSc7d1	draselný
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
HCONH2	HCONH2	k4	HCONH2
+	+	kIx~	+
KOH	KOH	kA	KOH
→	→	k?	→
KCN	KCN	kA	KCN
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
</s>
</p>
<p>
<s>
==	==	k?	==
Fyziologické	fyziologický	k2eAgInPc1d1	fyziologický
účinky	účinek	k1gInPc1	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
draselný	draselný	k2eAgInSc1d1	draselný
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
jed	jed	k1gInSc1	jed
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
intoxikuje	intoxikovat	k5eAaImIp3nS	intoxikovat
tělo	tělo	k1gNnSc4	tělo
požitím	požití	k1gNnSc7	požití
<g/>
,	,	kIx,	,
inhalací	inhalace	k1gFnSc7	inhalace
prachu	prach	k1gInSc2	prach
či	či	k8xC	či
vstřebáním	vstřebání	k1gNnSc7	vstřebání
kůží	kůže	k1gFnPc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žaludku	žaludek	k1gInSc6	žaludek
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
působením	působení	k1gNnSc7	působení
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
(	(	kIx(	(
<g/>
HCl	HCl	k1gFnSc2	HCl
<g/>
)	)	kIx)	)
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
prudce	prudko	k6eAd1	prudko
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
kyanovodík	kyanovodík	k1gInSc4	kyanovodík
<g/>
.	.	kIx.	.
</s>
<s>
Smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
KCN	KCN	kA	KCN
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
200	[number]	k4	200
mg	mg	kA	mg
–	–	k?	–
300	[number]	k4	300
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Otravy	otrava	k1gMnPc4	otrava
zažívacím	zažívací	k2eAgInSc7d1	zažívací
traktem	trakt	k1gInSc7	trakt
trvají	trvat	k5eAaImIp3nP	trvat
při	při	k7c6	při
plném	plný	k2eAgInSc6d1	plný
žaludku	žaludek	k1gInSc6	žaludek
i	i	k9	i
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
dávce	dávka	k1gFnSc6	dávka
uvolněného	uvolněný	k2eAgInSc2d1	uvolněný
kyanovodíku	kyanovodík	k1gInSc2	kyanovodík
(	(	kIx(	(
<g/>
či	či	k8xC	či
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
vdechování	vdechování	k1gNnSc6	vdechování
<g/>
)	)	kIx)	)
nastává	nastávat	k5eAaImIp3nS	nastávat
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
několika	několik	k4yIc6	několik
sekundách	sekunda	k1gFnPc6	sekunda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toxický	toxický	k2eAgInSc1d1	toxický
účinek	účinek	k1gInSc1	účinek
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
blokování	blokování	k1gNnSc6	blokování
enzymů	enzym	k1gInPc2	enzym
tkáňového	tkáňový	k2eAgNnSc2d1	tkáňové
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
inhibice	inhibice	k1gFnSc1	inhibice
cytochromoxidázy	cytochromoxidáza	k1gFnSc2	cytochromoxidáza
–	–	k?	–
CN	CN	kA	CN
<g/>
−	−	k?	−
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
přítomné	přítomný	k2eAgNnSc4d1	přítomné
trojmocné	trojmocný	k2eAgNnSc4d1	trojmocné
železo	železo	k1gNnSc4	železo
Fe	Fe	k1gFnSc2	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
blokádě	blokáda	k1gFnSc3	blokáda
buněčného	buněčný	k2eAgNnSc2d1	buněčné
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následek	k1gInSc7	následek
je	být	k5eAaImIp3nS	být
dušení	dušení	k1gNnSc1	dušení
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
některých	některý	k3yIgNnPc2	některý
mozkových	mozkový	k2eAgNnPc2d1	mozkové
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Transport	transport	k1gInSc1	transport
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
je	být	k5eAaImIp3nS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
železo	železo	k1gNnSc1	železo
v	v	k7c6	v
krevním	krevní	k2eAgNnSc6d1	krevní
barvivu	barvivo	k1gNnSc6	barvivo
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
dvojmocné	dvojmocný	k2eAgFnSc6d1	dvojmocná
formě	forma	k1gFnSc6	forma
(	(	kIx(	(
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
==	==	k?	==
</s>
</p>
<p>
<s>
Nastupují	nastupovat	k5eAaImIp3nP	nastupovat
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
po	po	k7c6	po
minutách	minuta	k1gFnPc6	minuta
<g/>
,	,	kIx,	,
nejvýše	vysoce	k6eAd3	vysoce
po	po	k7c6	po
desítkách	desítka	k1gFnPc6	desítka
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
náplně	náplň	k1gFnSc2	náplň
žaludku	žaludek	k1gInSc2	žaludek
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
(	(	kIx(	(
<g/>
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
v	v	k7c6	v
srdeční	srdeční	k2eAgFnSc6d1	srdeční
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc4	pocit
horka	horko	k1gNnSc2	horko
ve	v	k7c6	v
tvářích	tvář	k1gFnPc6	tvář
<g/>
,	,	kIx,	,
hučení	hučení	k1gNnSc3	hučení
v	v	k7c6	v
uších	ucho	k1gNnPc6	ucho
<g/>
,	,	kIx,	,
závratě	závrať	k1gFnPc1	závrať
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
nedostatek	nedostatek	k1gInSc1	nedostatek
kyslíku	kyslík	k1gInSc2	kyslík
ve	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
(	(	kIx(	(
<g/>
tkáňová	tkáňový	k2eAgFnSc1d1	tkáňová
hypoxie	hypoxie	k1gFnSc1	hypoxie
<g/>
)	)	kIx)	)
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
prohloubenému	prohloubený	k2eAgNnSc3d1	prohloubené
dýchání	dýchání	k1gNnSc3	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Postižený	postižený	k1gMnSc1	postižený
zpravidla	zpravidla	k6eAd1	zpravidla
zvrací	zvracet	k5eAaImIp3nS	zvracet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nutkání	nutkání	k1gNnSc4	nutkání
na	na	k7c6	na
stolici	stolice	k1gFnSc6	stolice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
malátný	malátný	k2eAgInSc1d1	malátný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
fázi	fáze	k1gFnSc6	fáze
otravy	otrava	k1gFnSc2	otrava
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
závažné	závažný	k2eAgFnPc1d1	závažná
poruchy	porucha	k1gFnPc1	porucha
dýchání	dýchání	k1gNnSc2	dýchání
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
několik	několik	k4yIc1	několik
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
zástavě	zástava	k1gFnSc6	zástava
dechu	dech	k1gInSc2	dech
však	však	k9	však
funguje	fungovat	k5eAaImIp3nS	fungovat
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
umělým	umělý	k2eAgNnSc7d1	umělé
dýcháním	dýchání	k1gNnSc7	dýchání
a	a	k8xC	a
podáním	podání	k1gNnSc7	podání
protijedu	protijed	k1gInSc2	protijed
dotyčného	dotyčný	k2eAgMnSc2d1	dotyčný
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
umělém	umělý	k2eAgNnSc6d1	umělé
dýchání	dýchání	k1gNnSc6	dýchání
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
je	být	k5eAaImIp3nS	být
zachránce	zachránce	k1gMnPc4	zachránce
ohrožen	ohrožen	k2eAgInSc1d1	ohrožen
vydechovaným	vydechovaný	k2eAgInSc7d1	vydechovaný
kyanovodíkem	kyanovodík	k1gInSc7	kyanovodík
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
vhodné	vhodný	k2eAgNnSc1d1	vhodné
použít	použít	k5eAaPmF	použít
masku	maska	k1gFnSc4	maska
nebo	nebo	k8xC	nebo
roušku	rouška	k1gFnSc4	rouška
s	s	k7c7	s
ventilem	ventil	k1gInSc7	ventil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgFnSc4	první
pomoc	pomoc	k1gFnSc4	pomoc
==	==	k?	==
</s>
</p>
<p>
<s>
Postiženého	postižený	k1gMnSc4	postižený
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ihned	ihned	k6eAd1	ihned
odnést	odnést	k5eAaPmF	odnést
ze	z	k7c2	z
zamořeného	zamořený	k2eAgNnSc2d1	zamořené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
-li	i	k?	-li
ke	k	k7c3	k
kontaktu	kontakt	k1gInSc3	kontakt
jedu	jed	k1gInSc2	jed
s	s	k7c7	s
pokožkou	pokožka	k1gFnSc7	pokožka
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zasažené	zasažený	k2eAgNnSc4d1	zasažené
místo	místo	k1gNnSc4	místo
důkladně	důkladně	k6eAd1	důkladně
omyto	omyt	k2eAgNnSc4d1	omyto
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
<g/>
-li	i	k?	-li
jed	jed	k1gInSc1	jed
požit	požít	k5eAaPmNgInS	požít
a	a	k8xC	a
postižený	postižený	k2eAgMnSc1d1	postižený
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
při	při	k7c6	při
vědomí	vědomí	k1gNnSc6	vědomí
<g/>
,	,	kIx,	,
ihned	ihned	k6eAd1	ihned
vyvoláme	vyvolat	k5eAaPmIp1nP	vyvolat
zvracení	zvracení	k1gNnSc4	zvracení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
požitím	požití	k1gNnSc7	požití
slané	slaný	k2eAgFnSc2d1	slaná
vody	voda	k1gFnSc2	voda
či	či	k8xC	či
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
drážděním	dráždění	k1gNnSc7	dráždění
hrdla	hrdlo	k1gNnSc2	hrdlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
voláme	volat	k5eAaImIp1nP	volat
lékaře	lékař	k1gMnPc4	lékař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejméně	málo	k6eAd3	málo
rizikovým	rizikový	k2eAgInSc7d1	rizikový
postupem	postup	k1gInSc7	postup
lékařské	lékařský	k2eAgFnSc2d1	lékařská
první	první	k4xOgFnSc2	první
pomoci	pomoc	k1gFnSc2	pomoc
je	být	k5eAaImIp3nS	být
nitrožilní	nitrožilní	k2eAgNnSc1d1	nitrožilní
podání	podání	k1gNnSc1	podání
hydroxykobalaminu	hydroxykobalamin	k1gInSc2	hydroxykobalamin
(	(	kIx(	(
<g/>
derivát	derivát	k1gInSc1	derivát
vitamínu	vitamín	k1gInSc2	vitamín
B	B	kA	B
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
dávce	dávka	k1gFnSc6	dávka
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
gramy	gram	k1gInPc1	gram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výměnou	výměna	k1gFnSc7	výměna
hydroxidové	hydroxidový	k2eAgFnSc2d1	hydroxidový
skupiny	skupina	k1gFnSc2	skupina
vzniká	vznikat	k5eAaImIp3nS	vznikat
nejedovatý	jedovatý	k2eNgInSc1d1	nejedovatý
kyanokobalamin	kyanokobalamin	k1gInSc1	kyanokobalamin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
postupy	postup	k1gInPc1	postup
využívají	využívat	k5eAaPmIp3nP	využívat
vazbu	vazba	k1gFnSc4	vazba
kyanidového	kyanidový	k2eAgInSc2d1	kyanidový
aniontu	anion	k1gInSc2	anion
na	na	k7c4	na
krevní	krevní	k2eAgNnSc4d1	krevní
barvivo	barvivo	k1gNnSc4	barvivo
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
následné	následný	k2eAgNnSc1d1	následné
převedení	převedení	k1gNnSc1	převedení
na	na	k7c4	na
netoxické	toxický	k2eNgFnPc4d1	netoxická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
HCN	HCN	kA	HCN
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
krevní	krevní	k2eAgNnSc4d1	krevní
barvivo	barvivo	k1gNnSc4	barvivo
hemoglobin	hemoglobin	k1gInSc4	hemoglobin
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jeho	on	k3xPp3gNnSc2	on
převedení	převedení	k1gNnSc2	převedení
na	na	k7c4	na
trojmocnou	trojmocný	k2eAgFnSc4d1	trojmocná
formu	forma	k1gFnSc4	forma
(	(	kIx(	(
<g/>
methemoglobin	methemoglobin	k2eAgMnSc1d1	methemoglobin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
kyanhemoglobin	kyanhemoglobin	k2eAgMnSc1d1	kyanhemoglobin
HbCN	HbCN	k1gMnSc1	HbCN
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Běžně	běžně	k6eAd1	běžně
používaným	používaný	k2eAgInSc7d1	používaný
protijedem	protijed	k1gInSc7	protijed
laické	laický	k2eAgFnSc2d1	laická
první	první	k4xOgFnSc2	první
pomoci	pomoc	k1gFnSc2	pomoc
je	být	k5eAaImIp3nS	být
amylnitrit	amylnitrit	k1gInSc1	amylnitrit
(	(	kIx(	(
<g/>
další	další	k2eAgInPc1d1	další
názvy	název	k1gInPc1	název
<g/>
:	:	kIx,	:
isoamylnitrit	isoamylnitrit	k1gInSc1	isoamylnitrit
<g/>
,	,	kIx,	,
isoamylester	isoamylester	k1gInSc1	isoamylester
kyseliny	kyselina	k1gFnSc2	kyselina
dusité	dusitý	k2eAgFnSc2d1	dusitá
<g/>
,	,	kIx,	,
dusitan	dusitan	k1gInSc1	dusitan
amylnatý	amylnatý	k2eAgInSc1d1	amylnatý
<g/>
,	,	kIx,	,
isopentylum	isopentylum	k1gInSc1	isopentylum
nitrosum	nitrosum	k1gInSc1	nitrosum
<g/>
,	,	kIx,	,
amylium	amylium	k1gNnSc1	amylium
nitrosum	nitrosum	k1gInSc1	nitrosum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
páry	pára	k1gFnPc1	pára
otrávený	otrávený	k2eAgMnSc1d1	otrávený
vdechuje	vdechovat	k5eAaImIp3nS	vdechovat
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
převodu	převod	k1gInSc3	převod
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
(	(	kIx(	(
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
na	na	k7c4	na
methemoglobin	methemoglobin	k2eAgInSc4d1	methemoglobin
(	(	kIx(	(
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
a	a	k8xC	a
poklesu	pokles	k1gInSc3	pokles
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
nejlépe	dobře	k6eAd3	dobře
vdechovat	vdechovat	k5eAaImF	vdechovat
v	v	k7c6	v
sedě	sedě	k6eAd1	sedě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vdechnutí	vdechnutí	k1gNnSc6	vdechnutí
postižený	postižený	k1gMnSc1	postižený
zrudne	zrudnout	k5eAaPmIp3nS	zrudnout
v	v	k7c6	v
obličeji	obličej	k1gInSc6	obličej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
barva	barva	k1gFnSc1	barva
v	v	k7c6	v
obličeji	obličej	k1gInSc6	obličej
vrátí	vrátit	k5eAaPmIp3nS	vrátit
k	k	k7c3	k
normálu	normál	k1gInSc3	normál
<g/>
,	,	kIx,	,
vdechnutí	vdechnutí	k1gNnSc6	vdechnutí
amylnitritu	amylnitrit	k1gInSc2	amylnitrit
opakujeme	opakovat	k5eAaImIp1nP	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
léky	lék	k1gInPc1	lék
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
nitrátů	nitrát	k1gInPc2	nitrát
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
efekt	efekt	k1gInSc4	efekt
(	(	kIx(	(
<g/>
potenciálně	potenciálně	k6eAd1	potenciálně
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
methemoglobinemii	methemoglobinemie	k1gFnSc4	methemoglobinemie
a	a	k8xC	a
pokles	pokles	k1gInSc4	pokles
tlaku	tlak	k1gInSc2	tlak
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lékař	lékař	k1gMnSc1	lékař
dále	daleko	k6eAd2	daleko
podává	podávat	k5eAaImIp3nS	podávat
sloučeniny	sloučenina	k1gFnPc4	sloučenina
síry	síra	k1gFnSc2	síra
(	(	kIx(	(
<g/>
thiosíran	thiosíran	k1gInSc1	thiosíran
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
netoxické	toxický	k2eNgInPc1d1	netoxický
thiokyanáty	thiokyanát	k1gInPc1	thiokyanát
(	(	kIx(	(
<g/>
rhodanidy	rhodanida	k1gFnPc1	rhodanida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
močí	moč	k1gFnSc7	moč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Minimálně	minimálně	k6eAd1	minimálně
dvě	dva	k4xCgFnPc1	dva
hodiny	hodina	k1gFnPc1	hodina
po	po	k7c6	po
otravě	otrava	k1gFnSc6	otrava
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pacient	pacient	k1gMnSc1	pacient
pod	pod	k7c7	pod
lékařským	lékařský	k2eAgInSc7d1	lékařský
dohledem	dohled	k1gInSc7	dohled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
uváděné	uváděný	k2eAgNnSc1d1	uváděné
podávání	podávání	k1gNnSc1	podávání
glukózy	glukóza	k1gFnSc2	glukóza
je	být	k5eAaImIp3nS	být
mýtem	mýtus	k1gInSc7	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rasputinova	Rasputinův	k2eAgFnSc1d1	Rasputinova
odolnost	odolnost	k1gFnSc1	odolnost
vůči	vůči	k7c3	vůči
jedu	jed	k1gInSc3	jed
neměla	mít	k5eNaImAgFnS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
nadpřirozenými	nadpřirozený	k2eAgFnPc7d1	nadpřirozená
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plynula	plynout	k5eAaImAgFnS	plynout
z	z	k7c2	z
neznalosti	neznalost	k1gFnSc2	neznalost
jeho	jeho	k3xOp3gMnPc2	jeho
vrahů	vrah	k1gMnPc2	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
mu	on	k3xPp3gMnSc3	on
jed	jed	k1gInSc4	jed
podávali	podávat	k5eAaImAgMnP	podávat
ve	v	k7c6	v
sladkých	sladký	k2eAgInPc6d1	sladký
moučnících	moučník	k1gInPc6	moučník
a	a	k8xC	a
madeirském	madeirský	k2eAgNnSc6d1	Madeirské
víně	víno	k1gNnSc6	víno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
cukr	cukr	k1gInSc4	cukr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
potravinách	potravina	k1gFnPc6	potravina
se	se	k3xPyFc4	se
ale	ale	k9	ale
jedná	jednat	k5eAaImIp3nS	jednat
spíše	spíše	k9	spíše
o	o	k7c4	o
sacharózu	sacharóza	k1gFnSc4	sacharóza
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
glukózu	glukóza	k1gFnSc4	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Blokáda	blokáda	k1gFnSc1	blokáda
dýchacích	dýchací	k2eAgInPc2d1	dýchací
enzymů	enzym	k1gInPc2	enzym
není	být	k5eNaImIp3nS	být
glukózou	glukóza	k1gFnSc7	glukóza
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
spekulovat	spekulovat	k5eAaImF	spekulovat
o	o	k7c6	o
pomalém	pomalý	k2eAgNnSc6d1	pomalé
uvolňování	uvolňování	k1gNnSc6	uvolňování
jedu	jed	k1gInSc2	jed
z	z	k7c2	z
naplněného	naplněný	k2eAgInSc2d1	naplněný
žaludku	žaludek	k1gInSc2	žaludek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
kyanid	kyanid	k1gInSc1	kyanid
draselný	draselný	k2eAgInSc1d1	draselný
přeměnu	přeměna	k1gFnSc4	přeměna
zlata	zlato	k1gNnSc2	zlato
v	v	k7c4	v
rozpustné	rozpustný	k2eAgFnPc4d1	rozpustná
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
klenotnictví	klenotnictví	k1gNnSc6	klenotnictví
při	při	k7c6	při
pozlacování	pozlacování	k1gNnSc6	pozlacování
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
je	být	k5eAaImIp3nS	být
užíván	užívat	k5eAaImNgInS	užívat
při	při	k7c6	při
získávání	získávání	k1gNnSc6	získávání
zlata	zlato	k1gNnSc2	zlato
z	z	k7c2	z
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
používá	používat	k5eAaImIp3nS	používat
jeho	on	k3xPp3gInSc4	on
příbuzný	příbuzný	k2eAgInSc4d1	příbuzný
–	–	k?	–
kyanid	kyanid	k1gInSc4	kyanid
sodný	sodný	k2eAgInSc4d1	sodný
(	(	kIx(	(
<g/>
NaCN	NaCN	k1gFnSc4	NaCN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Louhování	louhování	k1gNnSc1	louhování
zlata	zlato	k1gNnSc2	zlato
kyanidovou	kyanidový	k2eAgFnSc7d1	kyanidová
metodou	metoda	k1gFnSc7	metoda
je	být	k5eAaImIp3nS	být
problematické	problematický	k2eAgNnSc1d1	problematické
z	z	k7c2	z
ekologických	ekologický	k2eAgInPc2d1	ekologický
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
draselný	draselný	k2eAgInSc1d1	draselný
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
kyanidy	kyanid	k1gInPc7	kyanid
(	(	kIx(	(
<g/>
kyanid	kyanid	k1gInSc4	kyanid
sodný	sodný	k2eAgInSc4d1	sodný
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
lázní	lázeň	k1gFnPc2	lázeň
pro	pro	k7c4	pro
galvanotechniku	galvanotechnika	k1gFnSc4	galvanotechnika
(	(	kIx(	(
<g/>
galvanické	galvanický	k2eAgNnSc1d1	galvanické
pokovování	pokovování	k1gNnSc1	pokovování
<g/>
)	)	kIx)	)
-	-	kIx~	-
zinkování	zinkování	k1gNnSc1	zinkování
stříbření	stříbření	k1gNnPc2	stříbření
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
u	u	k7c2	u
autorů	autor	k1gMnPc2	autor
detektivních	detektivní	k2eAgInPc2d1	detektivní
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
bylo	být	k5eAaImAgNnS	být
cyankáli	cyankáli	k1gNnSc1	cyankáli
používaným	používaný	k2eAgInSc7d1	používaný
jedem	jed	k1gInSc7	jed
pro	pro	k7c4	pro
sebevraždy	sebevražda	k1gFnPc4	sebevražda
špiček	špička	k1gFnPc2	špička
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Ampulku	ampulka	k1gFnSc4	ampulka
s	s	k7c7	s
jedem	jed	k1gInSc7	jed
použili	použít	k5eAaPmAgMnP	použít
například	například	k6eAd1	například
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Braunová	Braunová	k1gFnSc1	Braunová
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
nebo	nebo	k8xC	nebo
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
odsouzený	odsouzený	k2eAgMnSc1d1	odsouzený
Hermann	Hermann	k1gMnSc1	Hermann
Göring	Göring	k1gInSc4	Göring
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
bývalý	bývalý	k2eAgMnSc1d1	bývalý
generál	generál	k1gMnSc1	generál
Slobodan	Slobodan	k1gMnSc1	Slobodan
Praljak	Praljak	k1gMnSc1	Praljak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paleček	Paleček	k1gMnSc1	Paleček
<g/>
,	,	kIx,	,
Linhart	Linhart	k1gMnSc1	Linhart
<g/>
,	,	kIx,	,
Horák	Horák	k1gMnSc1	Horák
<g/>
:	:	kIx,	:
Toxikologie	toxikologie	k1gFnSc1	toxikologie
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
<s>
VŠCHT	VŠCHT	kA	VŠCHT
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Škuta	škuta	k1gFnSc1	škuta
<g/>
:	:	kIx,	:
Jedy	jed	k1gInPc1	jed
a	a	k8xC	a
žíraviny	žíravina	k1gFnPc1	žíravina
<g/>
.	.	kIx.	.
</s>
<s>
AKS	AKS	kA	AKS
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wichterle	Wichterle	k1gFnSc1	Wichterle
<g/>
,	,	kIx,	,
Petrů	Petrů	k1gFnSc1	Petrů
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
.	.	kIx.	.
</s>
<s>
ČSAV	ČSAV	kA	ČSAV
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
</p>
