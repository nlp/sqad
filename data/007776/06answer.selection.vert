<s>
Chelonoidis	Chelonoidis	k1gFnSc7	Chelonoidis
nigra	nigr	k1gMnSc2	nigr
Harlan	Harlana	k1gFnPc2	Harlana
<g/>
,	,	kIx,	,
1827	[number]	k4	1827
Želva	želva	k1gFnSc1	želva
sloní	sloň	k1gFnPc2	sloň
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
1,2	[number]	k4	1,2
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
200	[number]	k4	200
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
