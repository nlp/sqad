<s>
LORAN	LORAN	kA
</s>
<s>
LORAN	LORAN	kA
(	(	kIx(
<g/>
LOng	LOng	k1gInSc1
RAnge	RAnge	k1gNnSc1
Navigation	Navigation	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pozemní	pozemní	k2eAgInSc1d1
rádiový	rádiový	k2eAgInSc1d1
navigační	navigační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
určený	určený	k2eAgInSc1d1
především	především	k9
pro	pro	k7c4
leteckou	letecký	k2eAgFnSc4d1
a	a	k8xC
námořní	námořní	k2eAgFnSc4d1
navigaci	navigace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňuje	umožňovat	k5eAaImIp3nS
určit	určit	k5eAaPmF
polohu	poloha	k1gFnSc4
a	a	k8xC
rychlost	rychlost	k1gFnSc4
dopravního	dopravní	k2eAgInSc2d1
prostředku	prostředek	k1gInSc2
pomocí	pomocí	k7c2
přijímání	přijímání	k1gNnSc2
nízkofrekvenčních	nízkofrekvenční	k2eAgInPc2d1
signálů	signál	k1gInPc2
ze	z	k7c2
synchronizovaných	synchronizovaný	k2eAgMnPc2d1
pozemních	pozemní	k2eAgMnPc2d1
vysílačů	vysílač	k1gMnPc2
(	(	kIx(
<g/>
patří	patřit	k5eAaImIp3nS
tedy	tedy	k9
mezi	mezi	k7c7
systémy	systém	k1gInPc7
hyperbolické	hyperbolický	k2eAgFnSc2d1
navigace	navigace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
probíhá	probíhat	k5eAaImIp3nS
nad	nad	k7c7
budoucností	budoucnost	k1gFnSc7
tohoto	tento	k3xDgInSc2
systém	systém	k1gInSc1
diskuse	diskuse	k1gFnSc1
–	–	k?
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
masovým	masový	k2eAgNnSc7d1
rozšířením	rozšíření	k1gNnSc7
družicových	družicový	k2eAgInPc2d1
navigačních	navigační	k2eAgInPc2d1
systémů	systém	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
GPS	GPS	kA
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4221125-6	4221125-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85078342	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85078342	#num#	k4
</s>
