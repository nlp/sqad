<s>
MMS	MMS	kA
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Miracle	Miracle	k1gNnSc7
Mineral	Mineral	k1gMnSc1
Supplement	Supplement	k1gMnSc1
–	–	k?
údajně	údajně	k6eAd1
léčivý	léčivý	k2eAgInSc4d1
prostředek	prostředek	k1gInSc4
na	na	k7c6
bázi	báze	k1gFnSc6
chemického	chemický	k2eAgNnSc2d1
bělidla	bělidlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
MMS	MMS	kA
zpráva	zpráva	k1gFnSc1
na	na	k7c6
displeji	displej	k1gInSc6
telefonu	telefon	k1gInSc2
Sony	Sony	kA
Ericsson	Ericsson	kA
<g/>
.	.	kIx.
</s>
<s>
Multimediální	multimediální	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
MMS	MMS	kA
z	z	k7c2
anglického	anglický	k2eAgNnSc2d1
Multimedia	multimedium	k1gNnSc2
Messaging	Messaging	k1gInSc1
Service	Service	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Multimediální	multimediální	k2eAgFnSc1d1
paralela	paralela	k1gFnSc1
k	k	k7c3
SMS	SMS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
MMS	MMS	kA
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
posílat	posílat	k5eAaImF
kromě	kromě	k7c2
textu	text	k1gInSc2
i	i	k8xC
obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
audio	audio	k2eAgInPc4d1
a	a	k8xC
videoklipy	videoklip	k1gInPc1
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
e-mailem	e-mail	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
MMS	MMS	kA
zprávy	zpráva	k1gFnPc1
jsou	být	k5eAaImIp3nP
přenášeny	přenášen	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
datový	datový	k2eAgInSc1d1
tok	tok	k1gInSc1
prostřednictvím	prostřednictvím	k7c2
datové	datový	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
GPRS	GPRS	kA
v	v	k7c6
GSM	GSM	kA
a	a	k8xC
pokročilejších	pokročilý	k2eAgFnPc6d2
<g/>
,	,	kIx,
jakým	jaký	k3yRgNnSc7,k3yIgNnSc7,k3yQgNnSc7
jsou	být	k5eAaImIp3nP
např.	např.	kA
CDMA	CDMA	kA
u	u	k7c2
UMTS	UMTS	kA
nebo	nebo	k8xC
EDGE	EDGE	kA
<g/>
.	.	kIx.
</s>
<s>
MMS	MMS	kA
zprávu	zpráva	k1gFnSc4
lze	lze	k6eAd1
v	v	k7c6
praxi	praxe	k1gFnSc6
posílat	posílat	k5eAaImF
z	z	k7c2
telefonu	telefon	k1gInSc2
na	na	k7c4
telefon	telefon	k1gInSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
z	z	k7c2
telefonu	telefon	k1gInSc2
do	do	k7c2
e-mailové	e-mailové	k2eAgFnSc2d1
schránky	schránka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
buď	buď	k8xC
okamžitým	okamžitý	k2eAgNnSc7d1
doručením	doručení	k1gNnSc7
nebo	nebo	k8xC
doručením	doručení	k1gNnSc7
s	s	k7c7
možností	možnost	k1gFnSc7
vyzvednutí	vyzvednutí	k1gNnSc2
zprávy	zpráva	k1gFnSc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
v	v	k7c6
určitém	určitý	k2eAgInSc6d1
časovém	časový	k2eAgInSc6d1
intervalu	interval	k1gInSc6
(	(	kIx(
<g/>
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
službách	služba	k1gFnPc6
telefonního	telefonní	k2eAgMnSc2d1
operátora	operátor	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7
MMS	MMS	kA
byla	být	k5eAaImAgFnS
technologie	technologie	k1gFnSc1
Sha-Mail	Sha-Maila	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
umožňovala	umožňovat	k5eAaImAgFnS
zasílání	zasílání	k1gNnSc4
fotografií	fotografia	k1gFnPc2
z	z	k7c2
jednoho	jeden	k4xCgInSc2
mobilního	mobilní	k2eAgInSc2d1
telefonu	telefon	k1gInSc2
na	na	k7c4
druhý	druhý	k4xOgInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
japonskou	japonský	k2eAgFnSc7d1
firmou	firma	k1gFnSc7
J-Phone	J-Phon	k1gInSc5
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Formáty	formát	k1gInPc1
</s>
<s>
Pro	pro	k7c4
přenos	přenos	k1gInSc4
obrázků	obrázek	k1gInPc2
se	se	k3xPyFc4
u	u	k7c2
MMS	MMS	kA
používají	používat	k5eAaImIp3nP
nejčastěji	často	k6eAd3
formáty	formát	k1gInPc1
GIF	GIF	kA
<g/>
,	,	kIx,
PNG	PNG	kA
<g/>
,	,	kIx,
JPG	JPG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
přenos	přenos	k1gInSc4
zvuku	zvuk	k1gInSc2
AMR	AMR	kA
nebo	nebo	k8xC
WAV	WAV	kA
a	a	k8xC
pro	pro	k7c4
video	video	k1gNnSc4
MPEG-	MPEG-	k1gFnSc2
<g/>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
3GPP	3GPP	k4
TS	ts	k0
26.140	26.140	k4
<g/>
:	:	kIx,
3	#num#	k4
<g/>
rd	rd	k?
Generation	Generation	k1gInSc1
Partnership	Partnership	k1gMnSc1
Project	Project	k1gMnSc1
<g/>
;	;	kIx,
Technical	Technical	k1gFnSc1
Specification	Specification	k1gInSc4
Group	Group	k1gInSc1
Services	Services	k1gInSc1
and	and	k?
System	Systo	k1gNnSc7
Aspects	Aspectsa	k1gFnPc2
<g/>
;	;	kIx,
Multimedia	multimedium	k1gNnSc2
Messaging	Messaging	k1gInSc4
Service	Service	k1gFnSc2
(	(	kIx(
<g/>
MMS	MMS	kA
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Media	medium	k1gNnPc4
formats	formats	k6eAd1
and	and	k?
codecs	codecs	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
GPP	GPP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
MMS	MMS	kA
architektura	architektura	k1gFnSc1
</s>
<s>
Středisko	středisko	k1gNnSc1
multimediálních	multimediální	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
</s>
<s>
Enhanced	Enhanced	k1gInSc1
Messaging	Messaging	k1gInSc1
Service	Service	k1gFnSc1
(	(	kIx(
<g/>
EMS	EMS	kA
<g/>
)	)	kIx)
</s>
<s>
Synchronized	Synchronized	k1gMnSc1
Multimedia	multimedium	k1gNnSc2
Integration	Integration	k1gInSc1
Language	language	k1gFnSc4
</s>
<s>
Mobilní	mobilní	k2eAgInSc1d1
marketing	marketing	k1gInSc1
</s>
<s>
Premium	Premium	k1gNnSc1
SMS	SMS	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
MMS	MMS	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
MMS	MMS	kA
u	u	k7c2
O	O	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
MMS	MMS	kA
u	u	k7c2
T-Mobile	T-Mobila	k1gFnSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
MMS	MMS	kA
u	u	k7c2
Vodafone	Vodafon	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
