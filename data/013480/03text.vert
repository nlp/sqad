<s>
Joinvillea	Joinvillea	k6eAd1
</s>
<s>
Joinvillea	Joinville	k2eAgFnSc1d1
Joinvillea	Joinvillea	k1gFnSc1
ascendens	ascendens	k6eAd1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
jednoděložné	jednoděložná	k1gFnPc1
(	(	kIx(
<g/>
Liliopsida	Liliopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
lipnicotvaré	lipnicotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Poales	Poales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
Joinvilleaceae	Joinvilleaceae	k6eAd1
Rod	rod	k1gInSc1
</s>
<s>
JoinvilleaGaudich	JoinvilleaGaudich	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1861	#num#	k4
druhy	druh	k1gInPc1
</s>
<s>
Joinvillea	Joinvillea	k6eAd1
ascendens	ascendens	k6eAd1
</s>
<s>
Joinvillea	Joinville	k2eAgFnSc1d1
borneensis	borneensis	k1gFnSc1
</s>
<s>
Joinvillea	Joinvillea	k6eAd1
bryanii	bryanie	k1gFnSc4
</s>
<s>
Joinvillea	Joinville	k2eAgFnSc1d1
plicata	plicata	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Joinvillea	Joinvillea	k1gNnSc1
je	být	k5eAaImIp3nS
rod	rod	k1gInSc1
jednoděložných	jednoděložný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
Joinvilleaceae	Joinvilleaceae	k1gFnSc2
<g/>
,	,	kIx,
řádu	řád	k1gInSc2
lipnicotvaré	lipnicotvarý	k2eAgMnPc4d1
(	(	kIx(
<g/>
Poales	Poales	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeleď	čeleď	k1gFnSc1
Joinvilleaceae	Joinvilleacea	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
jediný	jediný	k2eAgInSc1d1
rod	rod	k1gInSc1
Joinvillea	Joinville	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
starších	starý	k2eAgInPc6d2
taxonomických	taxonomický	k2eAgInPc6d1
systémech	systém	k1gInPc6
sem	sem	k6eAd1
byly	být	k5eAaImAgInP
někdy	někdy	k6eAd1
řazen	řadit	k5eAaImNgMnS
do	do	k7c2
čeledi	čeleď	k1gFnSc2
Flagellariaceae	Flagellariacea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
celkem	celkem	k6eAd1
robustní	robustní	k2eAgFnPc1d1
vytrvalé	vytrvalý	k2eAgFnPc1d1
byliny	bylina	k1gFnPc1
s	s	k7c7
oddenky	oddenek	k1gInPc1
připomínající	připomínající	k2eAgInSc1d1
rákos	rákos	k1gInSc4
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
dosahovat	dosahovat	k5eAaImF
až	až	k9
5	#num#	k4
m	m	kA
výšky	výška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
jsou	být	k5eAaImIp3nP
jednoduché	jednoduchý	k2eAgFnPc1d1
<g/>
,	,	kIx,
střídavé	střídavý	k2eAgFnPc1d1
<g/>
,	,	kIx,
přisedlé	přisedlý	k2eAgFnPc1d1
<g/>
,	,	kIx,
dvouřadě	dvouřadě	k6eAd1
uspořádané	uspořádaný	k2eAgFnPc1d1
<g/>
,	,	kIx,
s	s	k7c7
listovými	listový	k2eAgFnPc7d1
pochvami	pochva	k1gFnPc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
dosti	dosti	k6eAd1
velké	velký	k2eAgInPc1d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
dosahují	dosahovat	k5eAaImIp3nP
až	až	k9
1	#num#	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čepel	čepel	k1gFnSc1
je	být	k5eAaImIp3nS
čárkovitá	čárkovitý	k2eAgFnSc1d1
až	až	k6eAd1
kopinatá	kopinatý	k2eAgFnSc1d1
<g/>
,	,	kIx,
celokrajná	celokrajný	k2eAgFnSc1d1
<g/>
,	,	kIx,
žilnatina	žilnatina	k1gFnSc1
je	být	k5eAaImIp3nS
souběžná	souběžný	k2eAgFnSc1d1
až	až	k6eAd1
dlanitá	dlanitý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
jednodomé	jednodomý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
s	s	k7c7
oboupohlavnými	oboupohlavný	k2eAgInPc7d1
květy	květ	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
jsou	být	k5eAaImIp3nP
malé	malý	k2eAgMnPc4d1
<g/>
,	,	kIx,
uspořádány	uspořádán	k2eAgMnPc4d1
v	v	k7c6
květenstvích	květenství	k1gNnPc6
<g/>
,	,	kIx,
v	v	k7c6
latách	lata	k1gFnPc6
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
v	v	k7c6
kláscích	klásek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okvětních	okvětní	k2eAgMnPc2d1
lístků	lístek	k1gInPc2
je	být	k5eAaImIp3nS
6	#num#	k4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
volné	volný	k2eAgFnPc4d1
nebo	nebo	k8xC
srostlé	srostlý	k2eAgFnPc4d1
<g/>
,	,	kIx,
zelenavé	zelenavý	k2eAgFnPc4d1
až	až	k8xS
krémové	krémový	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyčinek	tyčinka	k1gFnPc2
je	být	k5eAaImIp3nS
6	#num#	k4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
volné	volný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gynecum	Gynecum	k1gInSc1
je	být	k5eAaImIp3nS
srostlé	srostlý	k2eAgNnSc1d1
ze	z	k7c2
3	#num#	k4
plodolistů	plodolist	k1gInPc2
<g/>
,	,	kIx,
je	on	k3xPp3gInPc4
synkarpní	synkarpnit	k5eAaPmIp3nP
<g/>
,	,	kIx,
semeník	semeník	k1gInSc1
je	být	k5eAaImIp3nS
svrchní	svrchní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
peckovice	peckovice	k1gFnPc4
žluté	žlutý	k2eAgFnPc4d1
<g/>
,	,	kIx,
červené	červený	k2eAgFnPc4d1
nebo	nebo	k8xC
černé	černý	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
Jsou	být	k5eAaImIp3nP
známy	znám	k2eAgFnPc1d1
4	#num#	k4
druhy	druh	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
rozšířeny	rozšířit	k5eAaPmNgInP
v	v	k7c6
tropech	trop	k1gInPc6
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
a	a	k8xC
na	na	k7c6
ostrovech	ostrov	k1gInPc6
v	v	k7c6
Tichém	tichý	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
http://www.mobot.org/MOBOT/Research/apweb/	http://www.mobot.org/MOBOT/Research/apweb/	k?
<g/>
↑	↑	k?
http://delta-intkey.com/angio/www/joinvill.htm	http://delta-intkey.com/angio/www/joinvill.htm	k6eAd1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Joinvillea	Joinville	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Joinvillea	Joinville	k1gInSc2
(	(	kIx(
<g/>
Plantae	Plantae	k1gInSc1
<g/>
)	)	kIx)
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
