<s>
Twilight	Twilight	k1gInSc1
sága	sága	k1gFnSc1
<g/>
:	:	kIx,
Zatmění	zatmění	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Twilight	Twilight	k1gInSc1
sága	sága	k1gFnSc1
<g/>
:	:	kIx,
Zatmění	zatmění	k1gNnSc1
Základní	základní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
The	The	k?
Twilight	Twilight	k1gMnSc1
Saga	Saga	k1gMnSc1
<g/>
:	:	kIx,
Eclipse	Eclipse	k1gFnSc1
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
124	#num#	k4
minut	minuta	k1gFnPc2
Žánry	žánr	k1gInPc1
</s>
<s>
romantický	romantický	k2eAgInSc4d1
filmfantasy	filmfantas	k1gInPc4
filmteenagerský	filmteenagerský	k2eAgInSc1d1
filmvampire	filmvampir	k1gInSc5
filmfilmové	filmfilmové	k2eAgInSc1d1
dramafilm	dramafilm	k1gInSc4
natočený	natočený	k2eAgInSc4d1
podle	podle	k7c2
knihy	kniha	k1gFnSc2
Předloha	předloha	k1gFnSc1
</s>
<s>
Zatmenie	Zatmenie	k1gFnSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Melissa	Melissa	k1gFnSc1
Rosenberg	Rosenberg	k1gInSc1
Režie	režie	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Slade	slad	k1gInSc5
Obsazení	obsazení	k1gNnSc1
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Kristen	Kristen	k2eAgInSc1d1
StewartRobert	StewartRobert	k1gInSc1
PattinsonTaylor	PattinsonTaylora	k1gFnPc2
LautnerAshley	LautnerAshlea	k1gFnSc2
GreeneBryce	GreeneBryce	k1gFnPc1
Dallas	Dallas	k1gInSc4
HowardBilly	HowardBilla	k1gFnSc2
BurkeDakota	BurkeDakota	k1gFnSc1
Fanning	Fanning	k1gInSc1
Produkce	produkce	k1gFnSc1
</s>
<s>
Wyck	Wyck	k1gInSc1
Godfrey	Godfrea	k1gFnSc2
<g/>
,	,	kIx,
Karen	Karen	k2eAgInSc1d1
Rosenfelt	Rosenfelt	k1gInSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
Howard	Howard	k1gMnSc1
Shore	Shor	k1gInSc5
Kamera	kamera	k1gFnSc1
</s>
<s>
Javier	Javirat	k5eAaPmRp2nS
Aguirresarobe	Aguirresarob	k1gMnSc5
Střih	střih	k1gInSc4
</s>
<s>
Nancy	Nancy	k1gNnSc1
Richardson	Richardson	k1gMnSc1
<g/>
,	,	kIx,
Art	Art	k1gMnSc1
Jones	Jones	k1gMnSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2010	#num#	k4
Produkční	produkční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Summit	summit	k1gInSc1
EntertainmentMaverick	EntertainmentMaverick	k1gInSc1
Films	Films	k1gInSc4
Distribuce	distribuce	k1gFnSc2
</s>
<s>
Summit	summit	k1gInSc1
Entertainment	Entertainment	k1gInSc1
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
$	$	kIx~
<g/>
68	#num#	k4
milionů	milion	k4xCgInPc2
Tržby	tržba	k1gFnPc1
</s>
<s>
$	$	kIx~
<g/>
698,491,347	698,491,347	k4
Předchozí	předchozí	k2eAgInSc4d1
a	a	k8xC
následující	následující	k2eAgInSc4d1
díl	díl	k1gInSc4
</s>
<s>
Twilight	Twilight	k1gInSc1
sága	sága	k1gFnSc1
<g/>
:	:	kIx,
Nový	nový	k2eAgInSc1d1
měsíc	měsíc	k1gInSc1
</s>
<s>
Twilight	Twilight	k1gInSc1
sága	sága	k1gFnSc1
<g/>
:	:	kIx,
Rozbřesk	rozbřesk	k1gInSc1
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
</s>
<s>
Twilight	Twilight	k1gInSc1
sága	sága	k1gFnSc1
<g/>
:	:	kIx,
Zatmění	zatmění	k1gNnSc1
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Twilight	Twilight	k1gInSc1
sága	sága	k1gFnSc1
<g/>
:	:	kIx,
Zatmění	zatmění	k1gNnSc1
je	být	k5eAaImIp3nS
americký	americký	k2eAgInSc4d1
romantický	romantický	k2eAgInSc4d1
fantasy	fantas	k1gInPc4
thriller	thriller	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc1
díl	díl	k1gInSc1
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
krátce	krátce	k6eAd1
po	po	k7c6
druhém	druhý	k4xOgNnSc6
tedy	tedy	k9
Novém	nový	k2eAgInSc6d1
měsíci	měsíc	k1gInSc6
kvůli	kvůli	k7c3
zachování	zachování	k1gNnSc3
sedmnáctiletého	sedmnáctiletý	k2eAgInSc2d1
vzhledu	vzhled	k1gInSc2
Roberta	Robert	k1gMnSc4
Pattinsona	Pattinson	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiéra	premiéra	k1gFnSc1
byla	být	k5eAaImAgFnS
stanovena	stanovit	k5eAaPmNgFnS
na	na	k7c4
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
režíroval	režírovat	k5eAaImAgMnS
David	David	k1gMnSc1
Slade	Slade	k1MnSc1
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Xavier	Xavier	k1gMnSc1
Samuel	Samuel	k1gMnSc1
</s>
<s>
Riley	Rilea	k1gFnPc1
</s>
<s>
Robert	Robert	k1gMnSc1
Pattinson	Pattinson	k1gMnSc1
</s>
<s>
Edward	Edward	k1gMnSc1
</s>
<s>
Kristen	Kristen	k2eAgInSc1d1
Stewartová	Stewartová	k1gFnSc5
</s>
<s>
Bella	Bella	k1gMnSc1
</s>
<s>
Bryce	Bryce	k1gMnSc1
Dallas	Dallas	k1gMnSc1
Howard	Howard	k1gMnSc1
</s>
<s>
Victoria	Victorium	k1gNnPc1
</s>
<s>
Catalina	Catalina	k1gFnSc1
Sandino	Sandin	k2eAgNnSc1d1
Moreno	Moren	k2eAgNnSc1d1
</s>
<s>
Maria	Maria	k1gFnSc1
</s>
<s>
Jack	Jack	k6eAd1
Huston	Huston	k1gInSc1
</s>
<s>
Royce	Royce	k1gMnSc1
King	King	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Julia	Julius	k1gMnSc2
Jones	Jones	k1gMnSc1
</s>
<s>
Leah	Leah	k1gMnSc1
Clearwater	Clearwater	k1gMnSc1
</s>
<s>
Boo	boa	k1gFnSc5
Boo	boa	k1gFnSc5
Stewart	Stewart	k1gMnSc1
</s>
<s>
Seth	Seth	k1gMnSc1
Clearwater	Clearwater	k1gMnSc1
</s>
<s>
Jodelle	Jodelle	k1gFnSc1
Ferland	Ferlanda	k1gFnPc2
</s>
<s>
Bree	Bree	k1gFnSc1
Tanner	Tannra	k1gFnPc2
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Bella	Bella	k1gMnSc1
tráví	trávit	k5eAaImIp3nS
s	s	k7c7
Edwardem	Edward	k1gMnSc7
více	hodně	k6eAd2
a	a	k8xC
více	hodně	k6eAd2
času	čas	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
s	s	k7c7
ní	on	k3xPp3gFnSc7
Jacob	Jacoba	k1gFnPc2
Black	Black	k1gInSc4
nemluví	mluvit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc4
se	se	k3xPyFc4
ale	ale	k9
změní	změnit	k5eAaPmIp3nS
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
Seattlu	Seattl	k1gInSc6
začínají	začínat	k5eAaImIp3nP
podivné	podivný	k2eAgFnPc1d1
vraždy	vražda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečná	konečný	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
s	s	k7c7
Victorií	Victorie	k1gFnSc7
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Soundtrack	soundtrack	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CD	CD	kA
</s>
<s>
Metric	Metric	k1gMnSc1
–	–	k?
Eclipse	Eclipse	k1gFnSc1
(	(	kIx(
<g/>
All	All	k1gFnSc1
Yours	Yoursa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
MUSE	Musa	k1gFnSc3
–	–	k?
Neutron	neutron	k1gInSc1
Star	Star	kA
Collision	Collision	k1gInSc1
(	(	kIx(
<g/>
Love	lov	k1gInSc5
Is	Is	k1gFnPc3
Forever	Forever	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Bravery	Braver	k1gInPc1
–	–	k?
Ours	Ours	k1gInSc1
</s>
<s>
Sia	Sia	k?
–	–	k?
My	my	k3xPp1nPc1
Love	lov	k1gInSc5
</s>
<s>
Fanfarlo	Fanfarlo	k1gNnSc4
–	–	k?
Atlas	Atlas	k1gInSc1
</s>
<s>
The	The	k?
Black	Black	k1gInSc1
Keys	Keys	k1gInSc1
–	–	k?
Chop	chop	k1gInSc1
And	Anda	k1gFnPc2
Change	change	k1gFnSc2
</s>
<s>
The	The	k?
Dead	Dead	k1gInSc1
Weather	Weathra	k1gFnPc2
–	–	k?
Rolling	Rolling	k1gInSc1
In	In	k1gMnSc1
On	on	k3xPp3gMnSc1
A	a	k9
Burning	Burning	k1gInSc4
Tire	Tir	k1gInSc2
</s>
<s>
Beck	Beck	k6eAd1
and	and	k?
Bat	Bat	k1gFnSc1
For	forum	k1gNnPc2
Lashes	Lashes	k1gInSc1
–	–	k?
Let	let	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Get	Get	k1gFnSc7
Lost	Losta	k1gFnPc2
</s>
<s>
Vampire	Vampir	k1gMnSc5
Weekend	weekend	k1gInSc1
–	–	k?
Jonathan	Jonathan	k1gMnSc1
Low	Low	k1gMnSc1
</s>
<s>
UNKLE	UNKLE	kA
–	–	k?
With	With	k1gMnSc1
You	You	k1gMnSc1
In	In	k1gMnSc1
My	my	k3xPp1nPc1
Head	Head	k1gMnSc1
(	(	kIx(
<g/>
Feat	Feat	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Black	Blacka	k1gFnPc2
Angels	Angelsa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Eastern	Eastern	k1gInSc1
Conference	Conference	k1gFnSc2
Champions	Championsa	k1gFnPc2
–	–	k?
A	a	k8xC
Million	Million	k1gInSc1
Miles	Miles	k1gMnSc1
An	An	k1gMnSc1
Hour	Hour	k1gMnSc1
</s>
<s>
Band	band	k1gInSc1
of	of	k?
Horses	Horses	k1gInSc1
–	–	k?
Life	Lif	k1gFnSc2
On	on	k3xPp3gMnSc1
Earth	Earth	k1gMnSc1
</s>
<s>
Cee	Cee	k?
Lo	Lo	k1gMnSc1
Green	Green	k2eAgMnSc1d1
–	–	k?
What	What	k2eAgInSc1d1
Part	part	k1gInSc1
of	of	k?
Forever	Forever	k1gInSc1
</s>
<s>
Howard	Howard	k1gMnSc1
Shore	Shor	k1gInSc5
–	–	k?
Jacob	Jacoba	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Theme	Them	k1gMnSc5
</s>
<s>
MiMi	Mimi	k1gFnSc1
–	–	k?
Don	dona	k1gFnPc2
<g/>
'	'	kIx"
<g/>
t	t	k?
You	You	k1gMnSc1
Mourn	Mourn	k1gMnSc1
The	The	k1gMnSc1
Sun	Sun	kA
(	(	kIx(
<g/>
Bonustrack	Bonustrack	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CD	CD	kA
</s>
<s>
The	The	k?
Score	Scor	k1gMnSc5
</s>
<s>
Instrumentální	instrumentální	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
ke	k	k7c3
třetímu	třetí	k4xOgInSc3
dílu	díl	k1gInSc3
složil	složit	k5eAaPmAgMnS
Howard	Howard	k1gMnSc1
Shore	Shor	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
skladbě	skladba	k1gFnSc6
Weeding	Weeding	k1gInSc1
Plans	Plansa	k1gFnPc2
se	se	k3xPyFc4
podíleli	podílet	k5eAaImAgMnP
Metric	Metric	k1gMnSc1
na	na	k7c4
instrumetální	instrumetální	k2eAgInSc4d1
Weeding	Weeding	k1gInSc4
Plans	Plans	k1gInSc1
navazuje	navazovat	k5eAaImIp3nS
píseň	píseň	k1gFnSc4
Eclipse	Eclipse	k1gFnSc2
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
<g/>
CD	CD	kA
.	.	kIx.
</s>
<s>
Riley	Rilea	k1gFnPc1
</s>
<s>
Compromise	Compromise	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Bella	Bella	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Theme	Them	k1gMnSc5
</s>
<s>
Bella	Bella	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Truck	truck	k1gInSc1
<g/>
/	/	kIx~
<g/>
Florida	Florida	k1gFnSc1
</s>
<s>
Victoria	Victorium	k1gNnPc1
</s>
<s>
Imprinting	Imprinting	k1gInSc1
</s>
<s>
The	The	k?
Cullens	Cullens	k1gInSc1
Plan	plan	k1gInSc1
</s>
<s>
First	First	k1gFnSc1
Kiss	Kissa	k1gFnPc2
</s>
<s>
Rosalie	Rosalie	k1gFnSc1
Listen	listen	k1gInSc1
</s>
<s>
Decisions	Decisions	k1gInSc1
<g/>
,	,	kIx,
Decisions	Decisions	k1gInSc1
<g/>
…	…	k?
</s>
<s>
They	Thea	k1gFnPc1
<g/>
'	'	kIx"
<g/>
re	re	k9
Coming	Coming	k1gInSc1
Here	Her	k1gMnSc2
</s>
<s>
Jacob	Jacoba	k1gFnPc2
Black	Blacka	k1gFnPc2
</s>
<s>
Jasper	Jasper	k1gInSc1
Listen	listen	k1gInSc1
</s>
<s>
Wolf	Wolf	k1gMnSc1
Scent	Scent	k1gMnSc1
</s>
<s>
Mountain	Mountain	k2eAgInSc1d1
Peak	Peak	k1gInSc1
</s>
<s>
The	The	k?
Kiss	Kiss	k1gInSc1
</s>
<s>
The	The	k?
Battle	Battle	k1gFnPc1
<g/>
/	/	kIx~
<g/>
Victoria	Victorium	k1gNnPc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edward	Edward	k1gMnSc1
</s>
<s>
Jane	Jan	k1gMnSc5
</s>
<s>
As	as	k1gNnSc1
Easy	Easa	k1gFnSc2
as	as	k9
Breathing	Breathing	k1gInSc4
</s>
<s>
Wedding	Wedding	k1gInSc1
Plans	Plansa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
