je	být	k5eAaImIp3nS
cena	cena	k1gFnSc1
většinou	většinou	k6eAd1
používaná	používaný	k2eAgFnSc1d1
pro	pro	k7c4
účely	účel	k1gInPc4
soukromoprávních	soukromoprávní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
,	,	kIx,
občanského	občanský	k2eAgNnSc2d1
soudního	soudní	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
včetně	včetně	k7c2
dědického	dědický	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
daňového	daňový	k2eAgNnSc2d1
nebo	nebo	k8xC
exekučního	exekuční	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
apod.	apod.	kA
