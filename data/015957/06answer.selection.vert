<s>
Obvyklá	obvyklý	k2eAgFnSc1d1
cena	cena	k1gFnSc1
nebo	nebo	k8xC
cena	cena	k1gFnSc1
obvyklá	obvyklý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
cena	cena	k1gFnSc1
většinou	většinou	k6eAd1
používaná	používaný	k2eAgFnSc1d1
pro	pro	k7c4
účely	účel	k1gInPc4
soukromoprávních	soukromoprávní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
,	,	kIx,
občanského	občanský	k2eAgNnSc2d1
soudního	soudní	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
včetně	včetně	k7c2
dědického	dědický	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
daňového	daňový	k2eAgNnSc2d1
nebo	nebo	k8xC
exekučního	exekuční	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
apod.	apod.	kA
V	v	k7c6
minulosti	minulost	k1gFnSc6
se	se	k3xPyFc4
pro	pro	k7c4
označení	označení	k1gNnSc4
téhož	týž	k3xTgInSc2
používal	používat	k5eAaImAgInS
pojem	pojem	k1gInSc1
obecná	obecná	k1gFnSc1
cena	cena	k1gFnSc1
<g/>
.	.	kIx.
</s>