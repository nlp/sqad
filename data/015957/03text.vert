<s>
Obvyklá	obvyklý	k2eAgFnSc1d1
cena	cena	k1gFnSc1
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1
cena	cena	k1gFnSc1
nebo	nebo	k8xC
cena	cena	k1gFnSc1
obvyklá	obvyklý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
cena	cena	k1gFnSc1
většinou	většinou	k6eAd1
používaná	používaný	k2eAgFnSc1d1
pro	pro	k7c4
účely	účel	k1gInPc4
soukromoprávních	soukromoprávní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
,	,	kIx,
občanského	občanský	k2eAgNnSc2d1
soudního	soudní	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
včetně	včetně	k7c2
dědického	dědický	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
daňového	daňový	k2eAgNnSc2d1
nebo	nebo	k8xC
exekučního	exekuční	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
apod.	apod.	kA
V	v	k7c6
minulosti	minulost	k1gFnSc6
se	se	k3xPyFc4
pro	pro	k7c4
označení	označení	k1gNnSc4
téhož	týž	k3xTgInSc2
používal	používat	k5eAaImAgInS
pojem	pojem	k1gInSc1
obecná	obecná	k1gFnSc1
cena	cena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Obvyklou	obvyklý	k2eAgFnSc7d1
cenou	cena	k1gFnSc7
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
dosažena	dosáhnout	k5eAaPmNgFnS
při	při	k7c6
prodeji	prodej	k1gInSc6
stejného	stejný	k2eAgNnSc2d1
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
obdobného	obdobný	k2eAgInSc2d1
majetku	majetek	k1gInSc2
nebo	nebo	k8xC
při	při	k7c6
poskytování	poskytování	k1gNnSc6
stejné	stejný	k2eAgFnPc1d1
nebo	nebo	k8xC
obdobné	obdobný	k2eAgFnPc1d1
služby	služba	k1gFnPc1
v	v	k7c6
obvyklém	obvyklý	k2eAgInSc6d1
obchodním	obchodní	k2eAgInSc6d1
styku	styk	k1gInSc6
v	v	k7c6
tuzemsku	tuzemsko	k1gNnSc6
ke	k	k7c3
dni	den	k1gInSc3
ocenění	ocenění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
cenu	cena	k1gFnSc4
stanovenou	stanovený	k2eAgFnSc4d1
bez	bez	k7c2
přihlédnutí	přihlédnutí	k1gNnSc2
k	k	k7c3
vlivu	vliv	k1gInSc3
zvláštních	zvláštní	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
(	(	kIx(
<g/>
mimořádných	mimořádný	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
na	na	k7c6
trhu	trh	k1gInSc6
<g/>
,	,	kIx,
osobních	osobní	k2eAgInPc2d1
poměrů	poměr	k1gInPc2
prodávajícího	prodávající	k2eAgNnSc2d1
nebo	nebo	k8xC
kupujícího	kupující	k1gMnSc4
apod.	apod.	kA
<g/>
)	)	kIx)
či	či	k8xC
vlivu	vliv	k1gInSc2
zvláštní	zvláštní	k2eAgFnSc2d1
obliby	obliba	k1gFnSc2
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
by	by	kYmCp3nP
šlo	jít	k5eAaImAgNnS
o	o	k7c4
tzv.	tzv.	kA
mimořádnou	mimořádný	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1
cena	cena	k1gFnSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
zjednodušeně	zjednodušeně	k6eAd1
ztotožňována	ztotožňován	k2eAgFnSc1d1
s	s	k7c7
cenou	cena	k1gFnSc7
tržní	tržní	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
však	však	k9
myslitelná	myslitelný	k2eAgFnSc1d1
jen	jen	k9
v	v	k7c6
případě	případ	k1gInSc6
uskutečněného	uskutečněný	k2eAgInSc2d1
prodeje	prodej	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
obvyklá	obvyklý	k2eAgFnSc1d1
cena	cena	k1gFnSc1
není	být	k5eNaImIp3nS
stanovována	stanovovat	k5eAaImNgFnS
pro	pro	k7c4
účely	účel	k1gInPc4
prodeje	prodej	k1gInSc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
pro	pro	k7c4
účely	účel	k1gInPc4
zjištění	zjištění	k1gNnSc2
relativně	relativně	k6eAd1
objektivní	objektivní	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
oceňované	oceňovaný	k2eAgFnSc2d1
věci	věc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
