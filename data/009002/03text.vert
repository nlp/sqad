<p>
<s>
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc4	cyklus
dvaceti	dvacet	k4xCc2	dvacet
velkoformátových	velkoformátový	k2eAgInPc2d1	velkoformátový
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
malíř	malíř	k1gMnSc1	malíř
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
shrnul	shrnout	k5eAaPmAgMnS	shrnout
dějiny	dějiny	k1gFnPc4	dějiny
slovanských	slovanský	k2eAgMnPc2d1	slovanský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
vznikal	vznikat	k5eAaImAgInS	vznikat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Zbiroh	Zbiroh	k1gInSc4	Zbiroh
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
od	od	k7c2	od
Charlese	Charles	k1gMnSc2	Charles
R.	R.	kA	R.
Crana	Cran	k1gMnSc2	Cran
<g/>
.	.	kIx.	.
</s>
<s>
Mucha	Mucha	k1gMnSc1	Mucha
jednotlivě	jednotlivě	k6eAd1	jednotlivě
dokončované	dokončovaný	k2eAgInPc4d1	dokončovaný
obrazy	obraz	k1gInPc4	obraz
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
postupně	postupně	k6eAd1	postupně
podmínečně	podmínečně	k6eAd1	podmínečně
předával	předávat	k5eAaImAgMnS	předávat
městu	město	k1gNnSc3	město
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
zachytit	zachytit	k5eAaPmF	zachytit
dějiny	dějiny	k1gFnPc4	dějiny
Slovanstva	Slovanstvo	k1gNnSc2	Slovanstvo
v	v	k7c6	v
kolekci	kolekce	k1gFnSc6	kolekce
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
jejich	jejich	k3xOp3gFnSc4	jejich
cestu	cesta	k1gFnSc4	cesta
dějinami	dějiny	k1gFnPc7	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
vytvoření	vytvoření	k1gNnSc2	vytvoření
Epopeje	epopej	k1gFnPc4	epopej
si	se	k3xPyFc3	se
pohrával	pohrávat	k5eAaImAgInS	pohrávat
již	již	k6eAd1	již
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
Světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
financí	finance	k1gFnPc2	finance
musel	muset	k5eAaImAgInS	muset
myšlenku	myšlenka	k1gFnSc4	myšlenka
odložit	odložit	k5eAaPmF	odložit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Chytilovou	Chytilová	k1gFnSc7	Chytilová
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
se	se	k3xPyFc4	se
potkal	potkat	k5eAaPmAgInS	potkat
s	s	k7c7	s
průmyslníkem	průmyslník	k1gMnSc7	průmyslník
Charlesem	Charles	k1gMnSc7	Charles
R.	R.	kA	R.
Cranem	Cran	k1gMnSc7	Cran
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
ochoten	ochoten	k2eAgInSc1d1	ochoten
nést	nést	k5eAaImF	nést
veškeré	veškerý	k3xTgInPc4	veškerý
náklady	náklad	k1gInPc4	náklad
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
vytvořením	vytvoření	k1gNnSc7	vytvoření
Slovanské	slovanský	k2eAgFnSc2d1	Slovanská
epopeje	epopej	k1gFnSc2	epopej
<g/>
.	.	kIx.	.
</s>
<s>
Mucha	Mucha	k1gMnSc1	Mucha
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
rodné	rodný	k2eAgFnSc2d1	rodná
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
pronajal	pronajmout	k5eAaPmAgMnS	pronajmout
část	část	k1gFnSc4	část
zámku	zámek	k1gInSc2	zámek
na	na	k7c6	na
Zbirohu	Zbiroh	k1gInSc6	Zbiroh
a	a	k8xC	a
zde	zde	k6eAd1	zde
tvořil	tvořit	k5eAaImAgInS	tvořit
Slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
epopej	epopej	k1gFnSc4	epopej
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mu	on	k3xPp3gMnSc3	on
dohromady	dohromady	k6eAd1	dohromady
zabrala	zabrat	k5eAaPmAgFnS	zabrat
18	[number]	k4	18
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dokončil	dokončit	k5eAaPmAgMnS	dokončit
ji	on	k3xPp3gFnSc4	on
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Charlesem	Charles	k1gMnSc7	Charles
Cranem	Cran	k1gMnSc7	Cran
předal	předat	k5eAaPmAgMnS	předat
Praze	Praha	k1gFnSc3	Praha
a	a	k8xC	a
veškerému	veškerý	k3xTgInSc3	veškerý
československému	československý	k2eAgInSc3d1	československý
lidu	lid	k1gInSc3	lid
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
byly	být	k5eAaImAgFnP	být
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
uloženy	uložit	k5eAaPmNgInP	uložit
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1943	[number]	k4	1943
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
obrazy	obraz	k1gInPc7	obraz
Václava	Václav	k1gMnSc2	Václav
Brožíka	Brožík	k1gMnSc2	Brožík
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
ve	v	k7c6	v
Slatiňanech	Slatiňany	k1gInPc6	Slatiňany
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
nehrozilo	hrozit	k5eNaImAgNnS	hrozit
poškození	poškození	k1gNnSc1	poškození
případným	případný	k2eAgNnSc7d1	případné
bombardováním	bombardování	k1gNnSc7	bombardování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Moravského	moravský	k2eAgInSc2d1	moravský
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
místních	místní	k2eAgMnPc2d1	místní
občanů	občan	k1gMnPc2	občan
restaurovány	restaurován	k2eAgInPc1d1	restaurován
a	a	k8xC	a
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1963	[number]	k4	1963
byly	být	k5eAaImAgFnP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
na	na	k7c6	na
zdejším	zdejší	k2eAgInSc6d1	zdejší
zámku	zámek	k1gInSc6	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc4	obraz
sem	sem	k6eAd1	sem
zapůjčila	zapůjčit	k5eAaPmAgFnS	zapůjčit
Galerie	galerie	k1gFnSc1	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
správcem	správce	k1gMnSc7	správce
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
se	se	k3xPyFc4	se
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
stala	stát	k5eAaPmAgFnS	stát
movitou	movitý	k2eAgFnSc7d1	movitá
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vystavování	vystavování	k1gNnSc3	vystavování
Slovanské	slovanský	k2eAgFnSc2d1	Slovanská
epopeje	epopej	k1gFnSc2	epopej
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
obrazy	obraz	k1gInPc1	obraz
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
vystaveny	vystavit	k5eAaPmNgInP	vystavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
v	v	k7c6	v
Klementinu	Klementinum	k1gNnSc6	Klementinum
<g/>
,	,	kIx,	,
jedenáct	jedenáct	k4xCc1	jedenáct
pláten	plátno	k1gNnPc2	plátno
bylo	být	k5eAaImAgNnS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
(	(	kIx(	(
<g/>
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
výstavu	výstava	k1gFnSc4	výstava
50	[number]	k4	50
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Celek	celek	k1gInSc1	celek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
předveden	předveden	k2eAgInSc1d1	předveden
české	český	k2eAgFnSc3d1	Česká
veřejnosti	veřejnost	k1gFnSc3	veřejnost
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
novém	nový	k2eAgInSc6d1	nový
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1930	[number]	k4	1930
byla	být	k5eAaImAgFnS	být
Epopej	epopej	k1gFnSc1	epopej
vystavena	vystavit	k5eAaPmNgFnS	vystavit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
několik	několik	k4yIc4	několik
pláten	plátno	k1gNnPc2	plátno
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1963	[number]	k4	1963
až	až	k9	až
2011	[number]	k4	2011
byly	být	k5eAaImAgInP	být
obrazy	obraz	k1gInPc1	obraz
vystaveny	vystaven	k2eAgInPc1d1	vystaven
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
sporech	spor	k1gInPc6	spor
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
pět	pět	k4xCc4	pět
pláten	plátno	k1gNnPc2	plátno
cyklu	cyklus	k1gInSc2	cyklus
umístěno	umístit	k5eAaPmNgNnS	umístit
ve	v	k7c6	v
dvoraně	dvorana	k1gFnSc6	dvorana
Veletržního	veletržní	k2eAgInSc2d1	veletržní
paláce	palác	k1gInSc2	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc1d1	celý
cyklus	cyklus	k1gInSc1	cyklus
byl	být	k5eAaImAgInS	být
tamtéž	tamtéž	k6eAd1	tamtéž
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výstava	výstava	k1gFnSc1	výstava
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
cyklus	cyklus	k1gInSc1	cyklus
poprvé	poprvé	k6eAd1	poprvé
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Výstava	výstava	k1gFnSc1	výstava
byla	být	k5eAaImAgFnS	být
nebývale	nebývale	k6eAd1	nebývale
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
výstavu	výstav	k1gInSc2	výstav
shlédlo	shlédnout	k5eAaPmAgNnS	shlédnout
přes	přes	k7c4	přes
650	[number]	k4	650
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejnavštěvovanější	navštěvovaný	k2eAgFnSc7d3	nejnavštěvovanější
výstavou	výstava	k1gFnSc7	výstava
umělce	umělec	k1gMnSc2	umělec
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc4	několik
vybraných	vybraný	k2eAgNnPc2d1	vybrané
pláten	plátno	k1gNnPc2	plátno
Epopeje	epopeje	k1gFnSc1	epopeje
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
na	na	k7c6	na
Festivalu	festival	k1gInSc6	festival
RE	re	k9	re
<g/>
:	:	kIx,	:
<g/>
PUBLIKA	publikum	k1gNnPc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
výstavy	výstava	k1gFnSc2	výstava
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
nejen	nejen	k6eAd1	nejen
plátna	plátno	k1gNnPc1	plátno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sbírka	sbírka	k1gFnSc1	sbírka
Muchovy	Muchův	k2eAgFnSc2d1	Muchova
plakátové	plakátový	k2eAgFnSc2d1	plakátová
tvorby	tvorba	k1gFnSc2	tvorba
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
Ivana	Ivan	k1gMnSc2	Ivan
Lendla	Lendla	k1gMnSc2	Lendla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
epopej	epopej	k1gFnSc4	epopej
==	==	k?	==
</s>
</p>
<p>
<s>
Majitelem	majitel	k1gMnSc7	majitel
obrazů	obraz	k1gInPc2	obraz
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
Mucha	Mucha	k1gMnSc1	Mucha
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Cranem	Cran	k1gInSc7	Cran
<g/>
)	)	kIx)	)
obrazy	obraz	k1gInPc1	obraz
daroval	darovat	k5eAaPmAgMnS	darovat
"	"	kIx"	"
<g/>
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
umístí	umístit	k5eAaPmIp3nS	umístit
hotové	hotový	k2eAgInPc4d1	hotový
obrazy	obraz	k1gInPc4	obraz
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
výslovně	výslovně	k6eAd1	výslovně
vystavěné	vystavěný	k2eAgNnSc1d1	vystavěné
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nestanovil	stanovit	k5eNaPmAgMnS	stanovit
však	však	k9	však
pro	pro	k7c4	pro
postavení	postavení	k1gNnSc4	postavení
budovy	budova	k1gFnSc2	budova
žádnou	žádný	k3yNgFnSc4	žádný
lhůtu	lhůta	k1gFnSc4	lhůta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podmínka	podmínka	k1gFnSc1	podmínka
nebyla	být	k5eNaImAgFnS	být
dosud	dosud	k6eAd1	dosud
splněna	splnit	k5eAaPmNgFnS	splnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
soubor	soubor	k1gInSc1	soubor
přesunut	přesunout	k5eAaPmNgInS	přesunout
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
dědicové	dědic	k1gMnPc1	dědic
Alfonse	Alfons	k1gMnSc2	Alfons
Muchy	Mucha	k1gMnSc2	Mucha
však	však	k9	však
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svého	svůj	k3xOyFgMnSc2	svůj
právního	právní	k2eAgMnSc2d1	právní
zástupce	zástupce	k1gMnSc2	zástupce
podali	podat	k5eAaPmAgMnP	podat
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
vydání	vydání	k1gNnSc4	vydání
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
pokojného	pokojný	k2eAgInSc2d1	pokojný
stavu	stav	k1gInSc2	stav
podle	podle	k7c2	podle
§	§	k?	§
5	[number]	k4	5
občanského	občanský	k2eAgInSc2d1	občanský
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
,	,	kIx,	,
krumlovský	krumlovský	k2eAgInSc1d1	krumlovský
úřad	úřad	k1gInSc1	úřad
jejich	jejich	k3xOp3gInSc4	jejich
argument	argument	k1gInSc4	argument
uznal	uznat	k5eAaPmAgMnS	uznat
a	a	k8xC	a
zakázal	zakázat	k5eAaPmAgMnS	zakázat
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
Praze	Praha	k1gFnSc3	Praha
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
soud	soud	k1gInSc1	soud
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2010	[number]	k4	2010
určil	určit	k5eAaPmAgInS	určit
předběžným	předběžný	k2eAgNnSc7d1	předběžné
opatřením	opatření	k1gNnSc7	opatření
<g/>
,	,	kIx,	,
že	že	k8xS	že
Krumlov	Krumlov	k1gInSc1	Krumlov
musí	muset	k5eAaImIp3nS	muset
umožnit	umožnit	k5eAaPmF	umožnit
převezení	převezení	k1gNnSc4	převezení
obrazů	obraz	k1gInPc2	obraz
na	na	k7c4	na
připravovanou	připravovaný	k2eAgFnSc4d1	připravovaná
výstavu	výstava	k1gFnSc4	výstava
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgNnPc2	dva
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
právní	právní	k2eAgFnSc6d1	právní
veřejnosti	veřejnost	k1gFnSc6	veřejnost
bohatě	bohatě	k6eAd1	bohatě
diskutovaný	diskutovaný	k2eAgMnSc1d1	diskutovaný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2010	[number]	k4	2010
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
uklidnila	uklidnit	k5eAaPmAgFnS	uklidnit
a	a	k8xC	a
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2011	[number]	k4	2011
Galerie	galerie	k1gFnSc1	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
převezla	převézt	k5eAaPmAgFnS	převézt
pět	pět	k4xCc4	pět
pláten	plátno	k1gNnPc2	plátno
cyklu	cyklus	k1gInSc2	cyklus
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
převzala	převzít	k5eAaPmAgFnS	převzít
správu	správa	k1gFnSc4	správa
výstavy	výstava	k1gFnSc2	výstava
ostatních	ostatní	k2eAgFnPc2d1	ostatní
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
pět	pět	k4xCc4	pět
pláten	plátno	k1gNnPc2	plátno
cyklu	cyklus	k1gInSc2	cyklus
umístěno	umístit	k5eAaPmNgNnS	umístit
ve	v	k7c6	v
dvoraně	dvorana	k1gFnSc6	dvorana
Veletržního	veletržní	k2eAgInSc2d1	veletržní
paláce	palác	k1gInSc2	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
ostatních	ostatní	k2eAgMnPc2d1	ostatní
patnáct	patnáct	k4xCc1	patnáct
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2011	[number]	k4	2011
začaly	začít	k5eAaPmAgFnP	začít
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
přepravě	přeprava	k1gFnSc6	přeprava
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
cyklu	cyklus	k1gInSc2	cyklus
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
celý	celý	k2eAgInSc1d1	celý
cyklus	cyklus	k1gInSc1	cyklus
vystaven	vystavit	k5eAaPmNgInS	vystavit
ve	v	k7c6	v
dvoraně	dvorana	k1gFnSc6	dvorana
Veletržního	veletržní	k2eAgInSc2d1	veletržní
paláce	palác	k1gInSc2	palác
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
výstava	výstava	k1gFnSc1	výstava
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
otevřena	otevřít	k5eAaPmNgFnS	otevřít
až	až	k9	až
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
Rada	rada	k1gFnSc1	rada
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
schválila	schválit	k5eAaPmAgFnS	schválit
záměr	záměr	k1gInSc4	záměr
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
cyklus	cyklus	k1gInSc1	cyklus
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
nově	nově	k6eAd1	nově
postavené	postavený	k2eAgFnSc6d1	postavená
přístavbě	přístavba	k1gFnSc6	přístavba
Lapidária	lapidárium	k1gNnSc2	lapidárium
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Výstavišti	výstaviště	k1gNnSc6	výstaviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uzavření	uzavření	k1gNnSc4	uzavření
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Muchou	Mucha	k1gMnSc7	Mucha
===	===	k?	===
</s>
</p>
<p>
<s>
Dědic	dědic	k1gMnSc1	dědic
malíře	malíř	k1gMnSc2	malíř
Alfonse	Alfons	k1gMnSc2	Alfons
Muchy	Mucha	k1gMnSc2	Mucha
John	John	k1gMnSc1	John
Mucha	Mucha	k1gMnSc1	Mucha
žádal	žádat	k5eAaImAgMnS	žádat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
vrácení	vrácení	k1gNnSc2	vrácení
cyklu	cyklus	k1gInSc2	cyklus
obrazů	obraz	k1gInPc2	obraz
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
<g/>
.	.	kIx.	.
</s>
<s>
Argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Praha	Praha	k1gFnSc1	Praha
stále	stále	k6eAd1	stále
nesplnila	splnit	k5eNaPmAgFnS	splnit
podmínku	podmínka	k1gFnSc4	podmínka
daru	dar	k1gInSc2	dar
a	a	k8xC	a
nepostavila	postavit	k5eNaPmAgFnS	postavit
pro	pro	k7c4	pro
epopej	epopej	k1gFnSc4	epopej
vlastní	vlastní	k2eAgFnSc4d1	vlastní
galerii	galerie	k1gFnSc4	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Vadilo	vadit	k5eAaImAgNnS	vadit
mu	on	k3xPp3gMnSc3	on
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dvacítka	dvacítka	k1gFnSc1	dvacítka
velkoformátových	velkoformátový	k2eAgNnPc2d1	velkoformátové
pláten	plátno	k1gNnPc2	plátno
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vycestovat	vycestovat	k5eAaPmF	vycestovat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Muchy	Mucha	k1gMnSc2	Mucha
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
může	moct	k5eAaImIp3nS	moct
Epopej	epopej	k1gFnSc4	epopej
poškodit	poškodit	k5eAaPmF	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgInSc1d1	soudní
spor	spor	k1gInSc1	spor
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Muchou	Mucha	k1gMnSc7	Mucha
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2017	[number]	k4	2017
pravomocným	pravomocný	k2eAgInSc7d1	pravomocný
rozsudkem	rozsudek	k1gInSc7	rozsudek
Městský	městský	k2eAgInSc1d1	městský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
patří	patřit	k5eAaImIp3nS	patřit
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
doposud	doposud	k6eAd1	doposud
nebyl	být	k5eNaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
soudem	soud	k1gInSc7	soud
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
vrácení	vrácení	k1gNnSc6	vrácení
kauzy	kauza	k1gFnSc2	kauza
zpět	zpět	k6eAd1	zpět
Městskému	městský	k2eAgInSc3d1	městský
soudu	soud	k1gInSc3	soud
pro	pro	k7c4	pro
nedostatečné	dostatečný	k2eNgNnSc4d1	nedostatečné
zdůvodnění	zdůvodnění	k1gNnSc4	zdůvodnění
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
2019	[number]	k4	2019
byl	být	k5eAaImAgInS	být
spor	spor	k1gInSc1	spor
odložen	odložit	k5eAaPmNgInS	odložit
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
o	o	k7c6	o
důstojném	důstojný	k2eAgNnSc6d1	důstojné
trvalém	trvalý	k2eAgNnSc6d1	trvalé
umístnění	umístnění	k1gNnSc6	umístnění
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInSc1	jeho
případný	případný	k2eAgInSc1d1	případný
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
autorova	autorův	k2eAgInSc2d1	autorův
rodného	rodný	k2eAgInSc2d1	rodný
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
díky	díky	k7c3	díky
občanům	občan	k1gMnPc3	občan
autorova	autorův	k2eAgNnSc2d1	autorovo
rodiště	rodiště	k1gNnSc2	rodiště
byla	být	k5eAaImAgFnS	být
zachráněna	zachránit	k5eAaPmNgFnS	zachránit
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
mravenčí	mravenčí	k2eAgFnSc3d1	mravenčí
práci	práce	k1gFnSc3	práce
moravských	moravský	k2eAgMnPc2d1	moravský
restaurátorů	restaurátor	k1gMnPc2	restaurátor
<g/>
,	,	kIx,	,
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
a	a	k8xC	a
občanů	občan	k1gMnPc2	občan
Moravského	moravský	k2eAgInSc2d1	moravský
Krumlova	Krumlov	k1gInSc2	Krumlov
může	moct	k5eAaImIp3nS	moct
svět	svět	k1gInSc1	svět
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
obdivovat	obdivovat	k5eAaImF	obdivovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
obrazů	obraz	k1gInPc2	obraz
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MUCHA	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
Alfons	Alfons	k1gMnSc1	Alfons
<g/>
,	,	kIx,	,
BYDŽOVSKÁ	Bydžovská	k1gFnSc1	Bydžovská
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
a	a	k8xC	a
SRP	srp	k1gInSc1	srp
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
–	–	k?	–
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Galerie	galerie	k1gFnSc1	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
383	[number]	k4	383
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7010	[number]	k4	7010
<g/>
-	-	kIx~	-
<g/>
102	[number]	k4	102
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
mytologie	mytologie	k1gFnSc1	mytologie
</s>
</p>
<p>
<s>
Panslavismus	panslavismus	k1gInSc1	panslavismus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Slovanské	slovanský	k2eAgFnPc1d1	Slovanská
epopeji	epopej	k1gFnSc3	epopej
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
projektu	projekt	k1gInSc2	projekt
digitalizace	digitalizace	k1gFnSc2	digitalizace
Slovanské	slovanský	k2eAgFnSc2d1	Slovanská
epopeje	epopej	k1gFnSc2	epopej
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
výstavě	výstava	k1gFnSc6	výstava
Slovanské	slovanský	k2eAgFnSc2d1	Slovanská
epopeje	epopej	k1gFnSc2	epopej
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
Dostupné	dostupný	k2eAgFnSc2d1	dostupná
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
Moravský	moravský	k2eAgInSc4d1	moravský
Krumlov	Krumlov	k1gInSc4	Krumlov
ke	k	k7c3	k
Slovanské	slovanský	k2eAgFnSc3d1	Slovanská
epopeji	epopej	k1gFnSc3	epopej
(	(	kIx(	(
<g/>
Dostupné	dostupný	k2eAgFnPc4d1	dostupná
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
"	"	kIx"	"
<g/>
Epopeje	epopeje	k1gFnSc1	epopeje
Slovanstva	Slovanstvo	k1gNnSc2	Slovanstvo
<g/>
"	"	kIx"	"
včetně	včetně	k7c2	včetně
překladu	překlad	k1gInSc2	překlad
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Muchou	Mucha	k1gMnSc7	Mucha
a	a	k8xC	a
Prahou	Praha	k1gFnSc7	Praha
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
epopej	epopej	k1gFnSc1	epopej
-	-	kIx~	-
velkoformátové	velkoformátový	k2eAgFnPc1d1	velkoformátová
fotografie	fotografia	k1gFnPc1	fotografia
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
poselství	poselství	k1gNnSc2	poselství
Alfonse	Alfons	k1gMnSc2	Alfons
Muchy	Mucha	k1gMnSc2	Mucha
</s>
</p>
