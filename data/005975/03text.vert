<s>
Tengwar	Tengwar	k1gMnSc1	Tengwar
je	být	k5eAaImIp3nS	být
písmo	písmo	k1gNnSc4	písmo
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkienem	Tolkien	k1gInSc7	Tolkien
jako	jako	k8xC	jako
obohacení	obohacení	k1gNnSc1	obohacení
jeho	jeho	k3xOp3gFnPc2	jeho
literárních	literární	k2eAgFnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Budeme	být	k5eAaImBp1nP	být
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
držet	držet	k5eAaImF	držet
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Tengwar	Tengwar	k1gInSc4	Tengwar
starobylé	starobylý	k2eAgInPc4d1	starobylý
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
moudrý	moudrý	k2eAgMnSc1d1	moudrý
noldorský	noldorský	k2eAgMnSc1d1	noldorský
elf	elf	k1gMnSc1	elf
jménem	jméno	k1gNnSc7	jméno
Fëanor	Fëanor	k1gMnSc1	Fëanor
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
tohoto	tento	k3xDgNnSc2	tento
písma	písmo	k1gNnSc2	písmo
byly	být	k5eAaImAgInP	být
Rúmilovy	Rúmilův	k2eAgInPc1d1	Rúmilův
Sarati	Sarat	k1gMnPc5	Sarat
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
Tengwaru	Tengwar	k1gInSc2	Tengwar
nejsou	být	k5eNaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
jen	jen	k9	jen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existuje	existovat	k5eAaImIp3nS	existovat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
řada	řada	k1gFnSc1	řada
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
mezi	mezi	k7c7	mezi
prvky	prvek	k1gInPc7	prvek
v	v	k7c6	v
Mendělejevově	Mendělejevův	k2eAgFnSc6d1	Mendělejevova
tabulce	tabulka	k1gFnSc6	tabulka
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
podrobněji	podrobně	k6eAd2	podrobně
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
dodatku	dodatek	k1gInSc6	dodatek
knihy	kniha	k1gFnSc2	kniha
Návratu	návrat	k1gInSc2	návrat
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
Tengwar	Tengwar	k1gInSc4	Tengwar
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
souhlásky	souhláska	k1gFnPc4	souhláska
<g/>
,	,	kIx,	,
a	a	k8xC	a
Tehtar	Tehtar	k1gInSc1	Tehtar
<g/>
,	,	kIx,	,
zastupující	zastupující	k2eAgFnPc1d1	zastupující
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
,	,	kIx,	,
přídech	přídech	k1gInSc1	přídech
<g/>
,	,	kIx,	,
sykavky	sykavka	k1gFnPc1	sykavka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
slovo	slovo	k1gNnSc1	slovo
tengwar	tengwara	k1gFnPc2	tengwara
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
quenijštině	quenijština	k1gFnSc6	quenijština
"	"	kIx"	"
<g/>
písmena	písmeno	k1gNnPc4	písmeno
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
množné	množný	k2eAgNnSc4d1	množné
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
jednotné	jednotný	k2eAgNnSc4d1	jednotné
číslo	číslo	k1gNnSc4	číslo
je	být	k5eAaImIp3nS	být
tengwa	tengwa	k6eAd1	tengwa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
tengwu	tengwa	k1gFnSc4	tengwa
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnPc4d1	základní
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
telco	telco	k1gMnSc1	telco
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nožička	nožička	k1gFnSc1	nožička
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
telcor	telcor	k1gInSc1	telcor
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
nebo	nebo	k8xC	nebo
dva	dva	k4xCgInPc1	dva
lúvar	lúvar	k1gInSc1	lúvar
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
obloučky	oblouček	k1gInPc1	oblouček
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
sg.	sg.	k?	sg.
lúva	lúva	k1gMnSc1	lúva
<g/>
)	)	kIx)	)
Tehtar	Tehtara	k1gFnPc2	Tehtara
jsou	být	k5eAaImIp3nP	být
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
nad	nad	k7c7	nad
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
Tengwar	Tengwara	k1gFnPc2	Tengwara
(	(	kIx(	(
<g/>
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
jiný	jiný	k2eAgInSc4d1	jiný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
jako	jako	k8xC	jako
quenijština	quenijština	k1gFnSc1	quenijština
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
většina	většina	k1gFnSc1	většina
slov	slovo	k1gNnPc2	slovo
končí	končit	k5eAaImIp3nS	končit
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tehta	tehta	k1gFnSc1	tehta
kladla	klást	k5eAaImAgFnS	klást
nad	nad	k7c4	nad
předcházející	předcházející	k2eAgFnSc4d1	předcházející
souhlásku	souhláska	k1gFnSc4	souhláska
<g/>
,	,	kIx,	,
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
jako	jako	k8xS	jako
sindarština	sindarština	k1gFnSc1	sindarština
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
většina	většina	k1gFnSc1	většina
slov	slovo	k1gNnPc2	slovo
končila	končit	k5eAaImAgFnS	končit
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kladla	klást	k5eAaImAgFnS	klást
nad	nad	k7c4	nad
následující	následující	k2eAgFnSc4d1	následující
souhlásku	souhláska	k1gFnSc4	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
<g/>
-li	i	k?	-li
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc4	ten
jinak	jinak	k6eAd1	jinak
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
psát	psát	k5eAaImF	psát
samohlásky	samohláska	k1gFnPc4	samohláska
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
píšeme	psát	k5eAaImIp1nP	psát
krátké	krátký	k2eAgFnPc4d1	krátká
samohlásky	samohláska	k1gFnPc4	samohláska
nad	nad	k7c4	nad
krátké	krátký	k2eAgInPc4d1	krátký
nosiče	nosič	k1gInPc4	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
samohlásky	samohláska	k1gFnPc1	samohláska
byly	být	k5eAaImAgFnP	být
obvykle	obvykle	k6eAd1	obvykle
znázorněny	znázornit	k5eAaPmNgInP	znázornit
položením	položení	k1gNnSc7	položení
tehty	tehta	k1gFnSc2	tehta
nad	nad	k7c4	nad
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
nosič	nosič	k1gInSc4	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Stejnému	stejný	k2eAgInSc3d1	stejný
účelu	účel	k1gInSc3	účel
však	však	k9	však
sloužila	sloužit	k5eAaImAgFnS	sloužit
i	i	k9	i
zdvojená	zdvojený	k2eAgFnSc1d1	zdvojená
tehta	tehta	k1gFnSc1	tehta
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
časté	častý	k2eAgNnSc1d1	časté
jen	jen	k9	jen
u	u	k7c2	u
kudrlinek	kudrlinka	k1gFnPc2	kudrlinka
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
u	u	k7c2	u
akutu	akut	k1gInSc2	akut
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tengwar	Tengwara	k1gFnPc2	Tengwara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Podrobné	podrobný	k2eAgFnSc2d1	podrobná
informace	informace	k1gFnSc2	informace
o	o	k7c4	o
tengwar	tengwar	k1gInSc4	tengwar
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Angrenost	Angrenost	k1gFnSc4	Angrenost
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Amanyë	Amanyë	k1gFnSc1	Amanyë
Tenceli	Tenceli	k1gFnSc2	Tenceli
</s>
