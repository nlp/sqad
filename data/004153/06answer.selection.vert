<s>
Prvním	první	k4xOgInSc7	první
filmem	film	k1gInSc7	film
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
mimo	mimo	k6eAd1	mimo
státní	státní	k2eAgInSc1d1	státní
monopol	monopol	k1gInSc1	monopol
byl	být	k5eAaImAgInS	být
Tankový	tankový	k2eAgInSc1d1	tankový
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vít	Vít	k1gMnSc1	Vít
Olmer	Olmer	k1gMnSc1	Olmer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
produkovala	produkovat	k5eAaImAgFnS	produkovat
společnost	společnost	k1gFnSc1	společnost
Bontonfilm	Bontonfilma	k1gFnPc2	Bontonfilma
<g/>
.	.	kIx.	.
</s>
