<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
tehdy	tehdy	k6eAd1	tehdy
zbrusu	zbrusu	k6eAd1	zbrusu
novému	nový	k2eAgInSc3d1	nový
druhu	druh	k1gInSc3	druh
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
se	se	k3xPyFc4	se
rodící	rodící	k2eAgFnSc3d1	rodící
kinematografii	kinematografie	k1gFnSc3	kinematografie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgInS	začít
natáčet	natáčet	k5eAaImF	natáčet
grotesky	groteska	k1gFnSc2	groteska
pro	pro	k7c4	pro
známou	známý	k2eAgFnSc4d1	známá
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
filmovou	filmový	k2eAgFnSc4d1	filmová
společnost	společnost	k1gFnSc4	společnost
Pathé	Pathý	k2eAgFnPc4d1	Pathý
<g/>
.	.	kIx.	.
</s>
