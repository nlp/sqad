<p>
<s>
Max	Max	k1gMnSc1	Max
Linder	Linder	k1gMnSc1	Linder
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Gabriel	Gabriel	k1gMnSc1	Gabriel
Maxmilien	Maxmilien	k2eAgInSc1d1	Maxmilien
Leuvielle	Leuvielle	k1gInSc1	Leuvielle
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
Saint-Loubè	Saint-Loubè	k1gFnSc1	Saint-Loubè
<g/>
,	,	kIx,	,
Gironde	Girond	k1gMnSc5	Girond
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
herec-komik	herecomik	k1gMnSc1	herec-komik
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
průkopník	průkopník	k1gMnSc1	průkopník
éry	éra	k1gFnSc2	éra
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Začátky	začátek	k1gInPc1	začátek
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
francouzského	francouzský	k2eAgNnSc2d1	francouzské
města	město	k1gNnSc2	město
Bordeaux	Bordeaux	k1gNnSc2	Bordeaux
a	a	k8xC	a
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
katolické	katolický	k2eAgFnSc2d1	katolická
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
sedmnácti	sedmnáct	k4xCc2	sedmnáct
let	léto	k1gNnPc2	léto
osamostatnil	osamostatnit	k5eAaPmAgMnS	osamostatnit
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
divadelním	divadelní	k2eAgMnSc7d1	divadelní
hercem	herec	k1gMnSc7	herec
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
tehdy	tehdy	k6eAd1	tehdy
zbrusu	zbrusu	k6eAd1	zbrusu
novému	nový	k2eAgInSc3d1	nový
druhu	druh	k1gInSc3	druh
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
se	se	k3xPyFc4	se
rodící	rodící	k2eAgFnSc3d1	rodící
kinematografii	kinematografie	k1gFnSc3	kinematografie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgInS	začít
natáčet	natáčet	k5eAaImF	natáčet
grotesky	groteska	k1gFnSc2	groteska
pro	pro	k7c4	pro
známou	známý	k2eAgFnSc4d1	známá
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
filmovou	filmový	k2eAgFnSc4d1	filmová
společnost	společnost	k1gFnSc4	společnost
Pathé	Pathý	k2eAgFnPc4d1	Pathý
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
známým	známý	k2eAgMnSc7d1	známý
a	a	k8xC	a
velice	velice	k6eAd1	velice
populárním	populární	k2eAgNnSc7d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
údajně	údajně	k6eAd1	údajně
natočil	natočit	k5eAaBmAgMnS	natočit
nejméně	málo	k6eAd3	málo
200	[number]	k4	200
krátkých	krátká	k1gFnPc2	krátká
filmových	filmový	k2eAgFnPc2d1	filmová
grotesek	groteska	k1gFnPc2	groteska
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešních	dnešní	k2eAgFnPc2d1	dnešní
dob	doba	k1gFnPc2	doba
dochoval	dochovat	k5eAaPmAgInS	dochovat
pouze	pouze	k6eAd1	pouze
zlomek	zlomek	k1gInSc1	zlomek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
svých	svůj	k3xOyFgInPc6	svůj
němých	němý	k2eAgInPc6d1	němý
filmech	film	k1gInPc6	film
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
filmovou	filmový	k2eAgFnSc4d1	filmová
postavu	postava	k1gFnSc4	postava
šviháka	švihák	k1gMnSc2	švihák
Maxe	Max	k1gMnSc2	Max
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
oblečeného	oblečený	k2eAgMnSc4d1	oblečený
smutného	smutný	k2eAgMnSc4d1	smutný
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
první	první	k4xOgFnSc7	první
velkou	velký	k2eAgFnSc7d1	velká
filmovou	filmový	k2eAgFnSc7d1	filmová
hvězdou	hvězda	k1gFnSc7	hvězda
éry	éra	k1gFnSc2	éra
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
vládl	vládnout	k5eAaImAgInS	vládnout
filmovému	filmový	k2eAgInSc3d1	filmový
žánru	žánr	k1gInSc3	žánr
nejen	nejen	k6eAd1	nejen
evropské	evropský	k2eAgFnSc3d1	Evropská
ale	ale	k8xC	ale
i	i	k9	i
světové	světový	k2eAgFnSc2d1	světová
grotesky	groteska	k1gFnSc2	groteska
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
také	také	k9	také
velkou	velký	k2eAgFnSc7d1	velká
inspirací	inspirace	k1gFnSc7	inspirace
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
nejznámějšího	známý	k2eAgMnSc2d3	nejznámější
a	a	k8xC	a
nejproslulejšího	proslulý	k2eAgMnSc2d3	nejproslulejší
následovníka	následovník	k1gMnSc2	následovník
Charlese	Charles	k1gMnSc2	Charles
Chaplina	Chaplin	k2eAgMnSc2d1	Chaplin
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
si	se	k3xPyFc3	se
velice	velice	k6eAd1	velice
vážil	vážit	k5eAaImAgMnS	vážit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poválečné	poválečný	k2eAgNnSc1d1	poválečné
období	období	k1gNnSc1	období
===	===	k?	===
</s>
</p>
<p>
<s>
Zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kariéře	kariéra	k1gFnSc6	kariéra
představovala	představovat	k5eAaImAgFnS	představovat
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
bojoval	bojovat	k5eAaImAgMnS	bojovat
jako	jako	k8xS	jako
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yIgFnSc1	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
znamenala	znamenat	k5eAaImAgFnS	znamenat
velký	velký	k2eAgInSc4d1	velký
fyzický	fyzický	k2eAgInSc4d1	fyzický
i	i	k8xC	i
psychický	psychický	k2eAgInSc4d1	psychický
otřes	otřes	k1gInSc4	otřes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zranění	zranění	k1gNnSc6	zranění
a	a	k8xC	a
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
odejel	odejet	k5eAaPmAgMnS	odejet
na	na	k7c4	na
čas	čas	k1gInSc4	čas
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
exceloval	excelovat	k5eAaImAgMnS	excelovat
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplin	k1gInSc4	Chaplin
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
filmovou	filmový	k2eAgFnSc7d1	filmová
postavou	postava	k1gFnSc7	postava
tuláka	tulák	k1gMnSc2	tulák
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Linder	Linder	k1gMnSc1	Linder
však	však	k9	však
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
nemoc	nemoc	k1gFnSc4	nemoc
a	a	k8xC	a
deprese	deprese	k1gFnPc4	deprese
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
svůj	svůj	k3xOyFgInSc4	svůj
předválečný	předválečný	k2eAgInSc4d1	předválečný
úspěch	úspěch	k1gInSc4	úspěch
zopakovat	zopakovat	k5eAaPmF	zopakovat
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
postupně	postupně	k6eAd1	postupně
upadala	upadat	k5eAaImAgFnS	upadat
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
do	do	k7c2	do
USA	USA	kA	USA
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
vrátil	vrátit	k5eAaPmAgMnS	vrátit
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
sám	sám	k3xTgMnSc1	sám
točit	točit	k5eAaImF	točit
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
produkoval	produkovat	k5eAaImAgMnS	produkovat
i	i	k8xC	i
režíroval	režírovat	k5eAaImAgMnS	režírovat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Charlese	Charles	k1gMnSc2	Charles
Chaplina	Chaplina	k1gFnSc1	Chaplina
<g/>
,	,	kIx,	,
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
i	i	k9	i
Harold	Harold	k1gMnSc1	Harold
Lloyd	Lloyd	k1gMnSc1	Lloyd
a	a	k8xC	a
Buster	Buster	k1gMnSc1	Buster
Keaton	Keaton	k1gInSc1	Keaton
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
jeho	jeho	k3xOp3gInPc1	jeho
filmy	film	k1gInPc1	film
také	také	k9	také
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
dobře	dobře	k6eAd1	dobře
konkurovat	konkurovat	k5eAaImF	konkurovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
společně	společně	k6eAd1	společně
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
pařížském	pařížský	k2eAgInSc6d1	pařížský
hotelu	hotel	k1gInSc6	hotel
Phrygien	Phrygien	k1gInSc1	Phrygien
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybraná	vybraný	k2eAgFnSc1d1	vybraná
filmografie	filmografie	k1gFnSc1	filmografie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Skater	Skater	k1gInSc1	Skater
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Debut	debut	k1gInSc1	debut
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
and	and	k?	and
His	his	k1gNnSc4	his
Mother-in-Law	Mothern-Law	k1gFnSc2	Mother-in-Law
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Takes	Takes	k1gMnSc1	Takes
Tonics	Tonicsa	k1gFnPc2	Tonicsa
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
and	and	k?	and
His	his	k1gNnSc2	his
Dog	doga	k1gFnPc2	doga
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Max	max	kA	max
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hat	hat	k0	hat
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
and	and	k?	and
the	the	k?	the
Jealous	Jealous	k1gInSc1	Jealous
Husband	Husband	k1gInSc1	Husband
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
in	in	k?	in
America	America	k1gMnSc1	America
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
in	in	k?	in
a	a	k8xC	a
Taxi	taxi	k1gNnPc2	taxi
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Wants	Wants	k1gInSc1	Wants
a	a	k8xC	a
Divorce	Divorka	k1gFnSc3	Divorka
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seven	Seven	k2eAgInSc1d1	Seven
Years	Years	k1gInSc1	Years
Bad	Bad	k1gMnSc1	Bad
Luck	Luck	k1gMnSc1	Luck
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
neštěstí	neštěstí	k1gNnSc2	neštěstí
</s>
</p>
<p>
<s>
Be	Be	k?	Be
my	my	k3xPp1nPc1	my
wife	wife	k1gNnPc1	wife
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Pro	pro	k7c4	pro
pět	pět	k4xCc4	pět
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
mojí	můj	k3xOp1gFnSc7	můj
ženuškou	ženuška	k1gFnSc7	ženuška
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
The	The	k?	The
Three	Three	k1gInSc1	Three
Must-Get-Theres	Must-Get-Theres	k1gInSc1	Must-Get-Theres
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Tři	tři	k4xCgMnPc1	tři
mušketýři	mušketýr	k1gMnPc1	mušketýr
</s>
</p>
<p>
<s>
Au	au	k0	au
Secours	Secoursa	k1gFnPc2	Secoursa
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Help	help	k1gInSc1	help
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Max	max	kA	max
Linder	Lindero	k1gNnPc2	Lindero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Linder	Linder	k1gMnSc1	Linder
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Max	Max	k1gMnSc1	Max
Linder	Linder	k1gMnSc1	Linder
at	at	k?	at
Golden	Goldno	k1gNnPc2	Goldno
Silents	Silents	k1gInSc1	Silents
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Photographs	Photographs	k1gInSc1	Photographs
and	and	k?	and
literature	literatur	k1gMnSc5	literatur
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Institut	institut	k1gInSc1	institut
Max	Max	k1gMnSc1	Max
Linder	Linder	k1gMnSc1	Linder
</s>
</p>
