<p>
<s>
Lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
jen	jen	k9	jen
hokej	hokej	k1gInSc4	hokej
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
týmový	týmový	k2eAgInSc1d1	týmový
sport	sport	k1gInSc1	sport
hraný	hraný	k2eAgInSc1d1	hraný
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejrychlejších	rychlý	k2eAgInPc2d3	nejrychlejší
sportů	sport	k1gInPc2	sport
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
na	na	k7c6	na
bruslích	brusle	k1gFnPc6	brusle
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vysokých	vysoký	k2eAgFnPc2d1	vysoká
rychlostí	rychlost	k1gFnPc2	rychlost
a	a	k8xC	a
vystřelený	vystřelený	k2eAgInSc1d1	vystřelený
puk	puk	k1gInSc1	puk
někdy	někdy	k6eAd1	někdy
přesáhne	přesáhnout	k5eAaPmIp3nS	přesáhnout
i	i	k9	i
rychlost	rychlost	k1gFnSc1	rychlost
175	[number]	k4	175
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hokej	hokej	k1gInSc1	hokej
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
koncem	koncem	k7c2	koncem
19.	[number]	k4	19.
století	století	k1gNnSc2	století
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
pravidla	pravidlo	k1gNnPc1	pravidlo
byla	být	k5eAaImAgNnP	být
vytvořena	vytvořit	k5eAaPmNgNnP	vytvořit
v	v	k7c6	v
Montréalu	Montréal	k1gInSc6	Montréal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
částečně	částečně	k6eAd1	částečně
i	i	k9	i
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
záznam	záznam	k1gInSc1	záznam
o	o	k7c6	o
hokejovém	hokejový	k2eAgNnSc6d1	hokejové
utkání	utkání	k1gNnSc6	utkání
<g/>
,	,	kIx,	,
konaném	konaný	k2eAgNnSc6d1	konané
na	na	k7c6	na
známém	známý	k2eAgNnSc6d1	známé
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
ve	v	k7c4	v
známý	známý	k2eAgInSc4d1	známý
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
se	s	k7c7	s
zaznamenaným	zaznamenaný	k2eAgInSc7d1	zaznamenaný
výsledkem	výsledek	k1gInSc7	výsledek
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
týmy	tým	k1gInPc7	tým
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
3.	[number]	k4	3.
března	březen	k1gInSc2	březen
1875	[number]	k4	1875
v	v	k7c6	v
Montreálu	Montreál	k1gInSc6	Montreál
na	na	k7c4	na
Victoria	Victorium	k1gNnSc2	Victorium
Skating	Skating	k1gInSc1	Skating
Rink	Rinko	k1gNnPc2	Rinko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Hra	hra	k1gFnSc1	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
3	[number]	k4	3
třetiny	třetina	k1gFnSc2	třetina
po	po	k7c6	po
20	[number]	k4	20
minutách	minuta	k1gFnPc6	minuta
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
čistého	čistý	k2eAgInSc2d1	čistý
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
při	při	k7c6	při
přerušení	přerušení	k1gNnSc6	přerušení
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
třetinami	třetina	k1gFnPc7	třetina
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
přestávky	přestávka	k1gFnPc1	přestávka
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
trvá	trvat	k5eAaImIp3nS	trvat
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
<g/>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
na	na	k7c6	na
hokejovém	hokejový	k2eAgNnSc6d1	hokejové
hřišti	hřiště	k1gNnSc6	hřiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nastoupí	nastoupit	k5eAaPmIp3nS	nastoupit
na	na	k7c4	na
lední	lední	k2eAgFnSc4d1	lední
plochu	plocha	k1gFnSc4	plocha
šest	šest	k4xCc1	šest
hráčů	hráč	k1gMnPc2	hráč
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
hráči	hráč	k1gMnPc1	hráč
mají	mít	k5eAaImIp3nP	mít
brusle	brusle	k1gFnPc4	brusle
a	a	k8xC	a
hokejky	hokejka	k1gFnPc4	hokejka
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
vstřelit	vstřelit	k5eAaPmF	vstřelit
více	hodně	k6eAd2	hodně
gólů	gól	k1gInPc2	gól
než	než	k8xS	než
soupeř	soupeř	k1gMnSc1	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
malým	malý	k2eAgInSc7d1	malý
<g/>
,	,	kIx,	,
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
gumovým	gumový	k2eAgInSc7d1	gumový
kotoučem	kotouč	k1gInSc7	kotouč
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
puk	puk	k1gInSc1	puk
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
puk	puk	k1gInSc4	puk
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
holemi	hole	k1gFnPc7	hole
s	s	k7c7	s
čepelí	čepel	k1gFnPc2	čepel
tzv.	tzv.	kA	tzv.
hokejkami	hokejka	k1gFnPc7	hokejka
<g/>
.	.	kIx.	.
</s>
<s>
Hokejka	hokejka	k1gFnSc1	hokejka
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
dolním	dolní	k2eAgInSc6d1	dolní
konci	konec	k1gInSc6	konec
zahnutá	zahnutý	k2eAgFnSc1d1	zahnutá
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
měnit	měnit	k5eAaImF	měnit
směr	směr	k1gInSc4	směr
pohybu	pohyb	k1gInSc2	pohyb
puku	puk	k1gInSc2	puk
svým	svůj	k3xOyFgNnSc7	svůj
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
rukou	ruka	k1gFnPc2	ruka
či	či	k8xC	či
bruslí	brusle	k1gFnPc2	brusle
<g/>
.	.	kIx.	.
</s>
<s>
Platné	platný	k2eAgFnPc1d1	platná
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
zákaz	zákaz	k1gInSc1	zákaz
přihrát	přihrát	k5eAaPmF	přihrát
rukou	ruka	k1gFnSc7	ruka
puk	puk	k1gInSc1	puk
svému	svůj	k3xOyFgInSc3	svůj
spoluhráči	spoluhráč	k1gMnPc1	spoluhráč
mimo	mimo	k7c4	mimo
obranné	obranný	k2eAgNnSc4d1	obranné
pásmo	pásmo	k1gNnSc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
nesmějí	smát	k5eNaImIp3nP	smát
vstřelit	vstřelit	k5eAaPmF	vstřelit
puk	puk	k1gInSc4	puk
do	do	k7c2	do
branky	branka	k1gFnSc2	branka
úmyslně	úmyslně	k6eAd1	úmyslně
čímkoliv	cokoliv	k3yInSc7	cokoliv
jiným	jiný	k2eAgNnSc7d1	jiné
než	než	k8xS	než
hokejkou	hokejka	k1gFnSc7	hokejka
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
šesti	šest	k4xCc2	šest
hráčů	hráč	k1gMnPc2	hráč
je	být	k5eAaImIp3nS	být
brankář	brankář	k1gMnSc1	brankář
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
nepustit	pustit	k5eNaPmF	pustit
puk	puk	k1gInSc1	puk
do	do	k7c2	do
branky	branka	k1gFnSc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Brankář	brankář	k1gMnSc1	brankář
má	mít	k5eAaImIp3nS	mít
speciální	speciální	k2eAgNnSc4d1	speciální
vybavení	vybavení	k1gNnSc4	vybavení
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgNnPc4d1	zvláštní
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
přikrýt	přikrýt	k5eAaPmF	přikrýt
puk	puk	k1gInSc4	puk
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zastavit	zastavit	k5eAaPmF	zastavit
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
tým	tým	k1gInSc1	tým
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
zápise	zápis	k1gInSc6	zápis
(	(	kIx(	(
<g/>
na	na	k7c6	na
soupisce	soupiska	k1gFnSc6	soupiska
jich	on	k3xPp3gInPc2	on
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
ovšem	ovšem	k9	ovšem
neomezeně	omezeně	k6eNd1	omezeně
<g/>
)	)	kIx)	)
maximálně	maximálně	k6eAd1	maximálně
22	[number]	k4	22
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
brankáři	brankář	k1gMnPc1	brankář
<g/>
.	.	kIx.	.
</s>
<s>
Střídání	střídání	k1gNnSc1	střídání
hráčů	hráč	k1gMnPc2	hráč
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
kdykoliv	kdykoliv	k6eAd1	kdykoliv
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Mužstvo	mužstvo	k1gNnSc1	mužstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyhodí	vyhodit	k5eAaPmIp3nS	vyhodit
puk	puk	k1gInSc1	puk
na	na	k7c4	na
zakázané	zakázaný	k2eAgNnSc4d1	zakázané
uvolnění	uvolnění	k1gNnSc4	uvolnění
neboli	neboli	k8xC	neboli
icing	icing	k1gInSc1	icing
nesmí	smět	k5eNaImIp3nS	smět
před	před	k7c7	před
opětovným	opětovný	k2eAgNnSc7d1	opětovné
zahájením	zahájení	k1gNnSc7	zahájení
hry	hra	k1gFnSc2	hra
vystřídat	vystřídat	k5eAaPmF	vystřídat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbylých	zbylý	k2eAgMnPc2d1	zbylý
pět	pět	k4xCc1	pět
hráčů	hráč	k1gMnPc2	hráč
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgMnPc4	tři
útočníky	útočník	k1gMnPc4	útočník
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
obránce	obránce	k1gMnPc4	obránce
<g/>
.	.	kIx.	.
</s>
<s>
Útočnické	útočnický	k2eAgFnPc1d1	útočnický
pozice	pozice	k1gFnPc1	pozice
jsou	být	k5eAaImIp3nP	být
levé	levý	k2eAgNnSc4d1	levé
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
střední	střední	k2eAgMnSc1d1	střední
útočník	útočník	k1gMnSc1	útočník
a	a	k8xC	a
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Útočníci	útočník	k1gMnPc1	útočník
spolu	spolu	k6eAd1	spolu
většinou	většinou	k6eAd1	většinou
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c6	v
útočných	útočný	k2eAgFnPc6d1	útočná
řadách	řada	k1gFnPc6	řada
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
lajnách	lajna	k1gFnPc6	lajna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
tři	tři	k4xCgMnPc1	tři
útočníci	útočník	k1gMnPc1	útočník
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyměňuje	vyměňovat	k5eAaImIp3nS	vyměňovat
celá	celý	k2eAgFnSc1d1	celá
pětka	pětka	k1gFnSc1	pětka
hráčů	hráč	k1gMnPc2	hráč
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Optimální	optimální	k2eAgFnSc1d1	optimální
délka	délka	k1gFnSc1	délka
jednoho	jeden	k4xCgNnSc2	jeden
střídání	střídání	k1gNnSc2	střídání
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Většina	většina	k1gFnSc1	většina
týmů	tým	k1gInPc2	tým
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
útočné	útočný	k2eAgFnPc4d1	útočná
řady	řada	k1gFnPc4	řada
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
obranné	obranný	k2eAgFnPc4d1	obranná
dvojice	dvojice	k1gFnPc4	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
hry	hra	k1gFnSc2	hra
jsou	být	k5eAaImIp3nP	být
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
čtyři	čtyři	k4xCgFnPc1	čtyři
–	–	k?	–
dva	dva	k4xCgMnPc1	dva
hlavní	hlavní	k2eAgMnPc1d1	hlavní
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
čároví	čárový	k2eAgMnPc1d1	čárový
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
regulérnost	regulérnost	k1gFnSc4	regulérnost
hry	hra	k1gFnSc2	hra
–	–	k?	–
posuzují	posuzovat	k5eAaImIp3nP	posuzovat
fauly	faul	k1gInPc4	faul
a	a	k8xC	a
udělují	udělovat	k5eAaImIp3nP	udělovat
tresty	trest	k1gInPc1	trest
<g/>
.	.	kIx.	.
<g/>
Menší	malý	k2eAgInSc1d2	menší
trest	trest	k1gInSc1	trest
trvá	trvat	k5eAaImIp3nS	trvat
2	[number]	k4	2
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc4d2	veliký
trest	trest	k1gInSc4	trest
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
osobní	osobní	k2eAgInSc4d1	osobní
trest	trest	k1gInSc4	trest
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Tresty	trest	k1gInPc4	trest
lze	lze	k6eAd1	lze
kombinovat	kombinovat	k5eAaImF	kombinovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
2+5,5	[number]	k4	2+5,5
<g/>
+	+	kIx~	+
<g/>
do	do	k7c2	do
konce	konec	k1gInSc2	konec
utkání	utkání	k1gNnSc2	utkání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mantinely	mantinel	k1gInPc1	mantinel
okolo	okolo	k7c2	okolo
hřiště	hřiště	k1gNnSc2	hřiště
napomáhají	napomáhat	k5eAaImIp3nP	napomáhat
udržet	udržet	k5eAaPmF	udržet
puk	puk	k1gInSc4	puk
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
i	i	k9	i
díky	díky	k7c3	díky
nim	on	k3xPp3gMnPc3	on
hra	hra	k1gFnSc1	hra
často	často	k6eAd1	často
trvá	trvat	k5eAaImIp3nS	trvat
několik	několik	k4yIc1	několik
minut	minuta	k1gFnPc2	minuta
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
se	s	k7c7	s
vhazováním	vhazování	k1gNnSc7	vhazování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hokeji	hokej	k1gInSc6	hokej
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgNnPc4	tři
základní	základní	k2eAgNnPc4d1	základní
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
omezují	omezovat	k5eAaImIp3nP	omezovat
pohyb	pohyb	k1gInSc4	pohyb
puku	puk	k1gInSc2	puk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
zakázané	zakázaný	k2eAgNnSc4d1	zakázané
uvolnění	uvolnění	k1gNnSc4	uvolnění
<g/>
,	,	kIx,	,
vyhození	vyhození	k1gNnSc4	vyhození
kotouče	kotouč	k1gInSc2	kotouč
mimo	mimo	k7c4	mimo
ledovou	ledový	k2eAgFnSc4d1	ledová
plochu	plocha	k1gFnSc4	plocha
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
mimo	mimo	k7c4	mimo
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dodržování	dodržování	k1gNnSc3	dodržování
těchto	tento	k3xDgNnPc2	tento
pravidel	pravidlo	k1gNnPc2	pravidlo
dohlížejí	dohlížet	k5eAaImIp3nP	dohlížet
čároví	čárový	k2eAgMnPc1d1	čárový
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbývající	zbývající	k2eAgFnPc1d1	zbývající
vlastnosti	vlastnost	k1gFnPc1	vlastnost
hry	hra	k1gFnSc2	hra
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
závislé	závislý	k2eAgFnSc2d1	závislá
na	na	k7c6	na
přesných	přesný	k2eAgNnPc6d1	přesné
pravidlech	pravidlo	k1gNnPc6	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
použita	použit	k2eAgNnPc1d1	použito
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hokejové	hokejový	k2eAgFnSc2d1	hokejová
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
IIHF	IIHF	kA	IIHF
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
profesionální	profesionální	k2eAgFnSc4d1	profesionální
ligu	liga	k1gFnSc4	liga
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
českého	český	k2eAgInSc2d1	český
hokeje	hokej	k1gInSc2	hokej
jsou	být	k5eAaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
shodná	shodný	k2eAgNnPc1d1	shodné
s	s	k7c7	s
pravidly	pravidlo	k1gNnPc7	pravidlo
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Hřiště	hřiště	k1gNnSc1	hřiště
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Hokejové	hokejový	k2eAgNnSc1d1	hokejové
hřiště	hřiště	k1gNnSc1	hřiště
je	být	k5eAaImIp3nS	být
ledová	ledový	k2eAgFnSc1d1	ledová
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgInPc1d1	maximální
rozměry	rozměr	k1gInPc1	rozměr
61	[number]	k4	61
x	x	k?	x
30	[number]	k4	30
m	m	kA	m
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgMnSc1d1	minimální
56	[number]	k4	56
x	x	k?	x
26	[number]	k4	26
m	m	kA	m
<g/>
,	,	kIx,	,
rohy	roh	k1gInPc7	roh
hřiště	hřiště	k1gNnSc2	hřiště
zaobleny	zaoblen	k2eAgFnPc1d1	zaoblena
hrazením	hrazení	k1gNnSc7	hrazení
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
7	[number]	k4	7
až	až	k9	až
8,5	[number]	k4	8,5
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
úpravu	úprava	k1gFnSc4	úprava
ledové	ledový	k2eAgFnSc2d1	ledová
plochy	plocha	k1gFnSc2	plocha
se	se	k3xPyFc4	se
od	od	k7c2	od
50.	[number]	k4	50.
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
používají	používat	k5eAaImIp3nP	používat
rolby	rolba	k1gFnPc1	rolba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Puk	puk	k0	puk
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Puk	puk	k1gInSc1	puk
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
touš	touš	k1gInSc1	touš
či	či	k8xC	či
prostě	prostě	k9	prostě
kotouč	kotouč	k1gInSc1	kotouč
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drobný	drobný	k2eAgInSc4d1	drobný
předmět	předmět	k1gInSc4	předmět
tvaru	tvar	k1gInSc2	tvar
velmi	velmi	k6eAd1	velmi
plochého	plochý	k2eAgInSc2d1	plochý
válce	válec	k1gInSc2	válec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
při	při	k7c6	při
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Puk	puk	k1gInSc1	puk
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
průměr	průměr	k1gInSc1	průměr
76,2	[number]	k4	76,2
milimetrů	milimetr	k1gInPc2	milimetr
(	(	kIx(	(
<g/>
3	[number]	k4	3
palce	palec	k1gInSc2	palec
anglické	anglický	k2eAgFnSc2d1	anglická
míry	míra	k1gFnSc2	míra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výšku	výška	k1gFnSc4	výška
25,4	[number]	k4	25,4
mm	mm	kA	mm
(	(	kIx(	(
<g/>
1	[number]	k4	1
palec	palec	k1gInSc1	palec
anglické	anglický	k2eAgFnSc2d1	anglická
míry	míra	k1gFnSc2	míra
<g/>
)	)	kIx)	)
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
od	od	k7c2	od
156	[number]	k4	156
do	do	k7c2	do
170	[number]	k4	170
gramů	gram	k1gInPc2	gram
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
tyto	tento	k3xDgInPc1	tento
parametry	parametr	k1gInPc1	parametr
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mírně	mírně	k6eAd1	mírně
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
třeba	třeba	k6eAd1	třeba
pro	pro	k7c4	pro
žákovské	žákovský	k2eAgFnPc4d1	žákovská
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Hokejové	hokejový	k2eAgFnSc2d1	hokejová
ligy	liga	k1gFnSc2	liga
a	a	k8xC	a
turnaje	turnaj	k1gInSc2	turnaj
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
National	Nationat	k5eAaPmAgMnS	Nationat
Hockey	Hockea	k1gFnPc4	Hockea
League	Leagu	k1gInSc2	Leagu
(	(	kIx(	(
<g/>
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejprestižnější	prestižní	k2eAgFnSc1d3	nejprestižnější
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
týmy	tým	k1gInPc1	tým
z	z	k7c2	z
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
KHL	KHL	kA	KHL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
hokejovou	hokejový	k2eAgFnSc7d1	hokejová
ligou	liga	k1gFnSc7	liga
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
zúčastňují	zúčastňovat	k5eAaImIp3nP	zúčastňovat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
týmy	tým	k1gInPc7	tým
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
</s>
</p>
<p>
<s>
Euro	euro	k1gNnSc1	euro
Hockey	Hockea	k1gFnSc2	Hockea
Tour	Toura	k1gFnPc2	Toura
je	být	k5eAaImIp3nS	být
série	série	k1gFnSc1	série
hokejových	hokejový	k2eAgMnPc2d1	hokejový
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
reprezentační	reprezentační	k2eAgInPc4d1	reprezentační
výběry	výběr	k1gInPc4	výběr
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
</s>
</p>
<p>
<s>
Czech	Czech	k1gMnSc1	Czech
Hockey	Hockea	k1gFnSc2	Hockea
Games	Games	k1gInSc1	Games
je	být	k5eAaImIp3nS	být
hokejový	hokejový	k2eAgInSc1d1	hokejový
turnaj	turnaj	k1gInSc1	turnaj
konající	konající	k2eAgFnSc2d1	konající
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
pod	pod	k7c4	pod
Euro	euro	k1gNnSc4	euro
Hockey	Hockea	k1gFnSc2	Hockea
Tour	Toura	k1gFnPc2	Toura
</s>
</p>
<p>
<s>
Karjala	Karnout	k5eAaPmAgFnS	Karnout
Cup	cup	k1gInSc4	cup
je	být	k5eAaImIp3nS	být
hokejový	hokejový	k2eAgInSc1d1	hokejový
turnaj	turnaj	k1gInSc1	turnaj
konající	konající	k2eAgFnSc2d1	konající
se	se	k3xPyFc4	se
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
pod	pod	k7c4	pod
Euro	euro	k1gNnSc4	euro
Hockey	Hockea	k1gFnSc2	Hockea
Tour	Toura	k1gFnPc2	Toura
</s>
</p>
<p>
<s>
Channel	Channel	k1gMnSc1	Channel
One	One	k1gFnSc2	One
Cup	cup	k1gInSc1	cup
je	být	k5eAaImIp3nS	být
hokejový	hokejový	k2eAgInSc1d1	hokejový
turnaj	turnaj	k1gInSc1	turnaj
konající	konající	k2eAgFnSc2d1	konající
se	se	k3xPyFc4	se
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
pod	pod	k7c4	pod
Euro	euro	k1gNnSc4	euro
Hockey	Hockea	k1gFnSc2	Hockea
Tour	Toura	k1gFnPc2	Toura
</s>
</p>
<p>
<s>
Oddset	Oddset	k1gMnSc1	Oddset
Hockey	Hockea	k1gFnSc2	Hockea
Games	Games	k1gMnSc1	Games
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
LG	LG	kA	LG
Hockey	Hockey	k1gInPc1	Hockey
Games	Games	k1gInSc1	Games
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hokejový	hokejový	k2eAgInSc1d1	hokejový
turnaj	turnaj	k1gInSc1	turnaj
konající	konající	k2eAgFnSc2d1	konající
se	se	k3xPyFc4	se
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
pod	pod	k7c4	pod
Euro	euro	k1gNnSc4	euro
Hockey	Hockea	k1gFnSc2	Hockea
Tour	Toura	k1gFnPc2	Toura
</s>
</p>
<p>
<s>
Hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
je	být	k5eAaImIp3nS	být
turnaj	turnaj	k1gInSc1	turnaj
týmů	tým	k1gInPc2	tým
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
evropských	evropský	k2eAgFnPc2d1	Evropská
lig	liga	k1gFnPc2	liga
<g/>
,	,	kIx,	,
předchůdcem	předchůdce	k1gMnSc7	předchůdce
této	tento	k3xDgFnSc2	tento
ligy	liga	k1gFnSc2	liga
byl	být	k5eAaImAgInS	být
European	European	k1gInSc1	European
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
znovuobnoven	znovuobnovit	k5eAaPmNgInS	znovuobnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016.	[number]	k4	2016.
</s>
</p>
<p>
<s>
MHL	MHL	kA	MHL
<g/>
,	,	kIx,	,
Juniorská	juniorský	k2eAgFnSc1d1	juniorská
verze	verze	k1gFnSc1	verze
KHL	KHL	kA	KHL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hokej	hokej	k1gInSc4	hokej
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
hokejová	hokejový	k2eAgFnSc1d1	hokejová
extraliga	extraliga	k1gFnSc1	extraliga
–	–	k?	–
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
profesionální	profesionální	k2eAgFnSc4d1	profesionální
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
1.	[number]	k4	1.
národní	národní	k2eAgFnSc1d1	národní
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
–	–	k?	–
Chance	Chanec	k1gInPc1	Chanec
liga	liga	k1gFnSc1	liga
-	-	kIx~	-
2.	[number]	k4	2.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hokejová	hokejový	k2eAgFnSc1d1	hokejová
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
2.	[number]	k4	2.
národní	národní	k2eAgFnSc1d1	národní
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgInPc1d1	krajský
hokejové	hokejový	k2eAgInPc1d1	hokejový
přebory	přebor	k1gInPc1	přebor
</s>
</p>
<p>
<s>
Okresní	okresní	k2eAgInPc1d1	okresní
hokejové	hokejový	k2eAgInPc1d1	hokejový
přebory	přebor	k1gInPc1	přebor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Pozemní	pozemní	k2eAgInSc4d1	pozemní
hokej	hokej	k1gInSc4	hokej
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
klubů	klub	k1gInPc2	klub
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Trest	trest	k1gInSc1	trest
(	(	kIx(	(
<g/>
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
lední	lední	k2eAgFnSc1d1	lední
hokej	hokej	k1gInSc4	hokej
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.cslh.cz	www.cslh.cz	k1gInSc1	www.cslh.cz
–	–	k?	–
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
</s>
</p>
<p>
<s>
www.hokej.cz	www.hokej.cz	k1gInSc1	www.hokej.cz
–	–	k?	–
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
českého	český	k2eAgInSc2d1	český
hokeje	hokej	k1gInSc2	hokej
</s>
</p>
<p>
<s>
www.iihf.com	www.iihf.com	k1gInSc1	www.iihf.com
–	–	k?	–
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
IIHF	IIHF	kA	IIHF
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
hokejová	hokejový	k2eAgFnSc1d1	hokejová
federace	federace	k1gFnSc1	federace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
www.nhl.com	www.nhl.com	k1gInSc1	www.nhl.com
–	–	k?	–
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
o	o	k7c4	o
kanadsko	kanadsko	k1gNnSc4	kanadsko
–	–	k?	–
americké	americký	k2eAgFnSc3d1	americká
národní	národní	k2eAgFnSc3d1	národní
hokejové	hokejový	k2eAgFnSc3d1	hokejová
lize	liga	k1gFnSc3	liga
</s>
</p>
<p>
<s>
www.eht.cz	www.eht.cz	k1gInSc1	www.eht.cz
–	–	k?	–
Stránky	stránka	k1gFnSc2	stránka
o	o	k7c4	o
Euro	euro	k1gNnSc4	euro
Hockey	Hockea	k1gFnSc2	Hockea
Tour	Toura	k1gFnPc2	Toura
</s>
</p>
<p>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
of	of	k?	of
Ice	Ice	k1gMnSc2	Ice
Hockey	Hockea	k1gMnSc2	Hockea
</s>
</p>
<p>
<s>
hokej.idnes.cz	hokej.idnes.cz	k1gInSc1	hokej.idnes.cz
–	–	k?	–
Hokej	hokej	k1gInSc1	hokej
na	na	k7c4	na
iDnes.cz	iDnes.cz	k1gInSc4	iDnes.cz
</s>
</p>
<p>
<s>
hokej.ihned.cz	hokej.ihned.cz	k1gInSc1	hokej.ihned.cz
–	–	k?	–
Hokej	hokej	k1gInSc1	hokej
na	na	k7c4	na
iHned.cz	iHned.cz	k1gInSc4	iHned.cz
</s>
</p>
<p>
<s>
hokejportal.cz	hokejportal.cz	k1gInSc1	hokejportal.cz
–	–	k?	–
Hokejový	hokejový	k2eAgInSc1d1	hokejový
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
Hokej	hokej	k1gInSc1	hokej
není	být	k5eNaImIp3nS	být
jenom	jenom	k9	jenom
sport	sport	k1gInSc1	sport
(	(	kIx(	(
<g/>
Historie	historie	k1gFnSc1	historie
<g/>
.	.	kIx.	.
<g/>
cs	cs	k?	cs
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
www.hokejbonus.estranky.cz	www.hokejbonus.estranky.cz	k1gInSc1	www.hokejbonus.estranky.cz
–	–	k?	–
Historie	historie	k1gFnSc1	historie
velkých	velký	k2eAgInPc2d1	velký
hokejových	hokejový	k2eAgInPc2d1	hokejový
turnajů	turnaj	k1gInPc2	turnaj
</s>
</p>
