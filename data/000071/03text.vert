<s>
Zrní	zrní	k1gNnSc1	zrní
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
Voní	vonět	k5eAaImIp3nS	vonět
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
Hrdina	Hrdina	k1gMnSc1	Hrdina
počítačový	počítačový	k2eAgMnSc1d1	počítačový
hry	hra	k1gFnPc4	hra
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
světa	svět	k1gInSc2	svět
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
písničkářky	písničkářka	k1gFnSc2	písničkářka
Radůzy	Radůza	k1gFnSc2	Radůza
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
labelu	label	k1gInSc6	label
Radůza	Radůza	k1gFnSc1	Radůza
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
album	album	k1gNnSc4	album
Soundtrack	soundtrack	k1gInSc1	soundtrack
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yIgInPc4	který
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Vinyla	Vinyla	k1gFnSc2	Vinyla
i	i	k9	i
Apollo	Apollo	k1gNnSc4	Apollo
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
desku	deska	k1gFnSc4	deska
roku	rok	k1gInSc2	rok
a	a	k8xC	a
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
kategoriích	kategorie	k1gFnPc6	kategorie
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
odnesla	odnést	k5eAaPmAgFnS	odnést
tu	tu	k6eAd1	tu
za	za	k7c4	za
Objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
své	svůj	k3xOyFgNnSc4	svůj
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgNnSc4d1	poslední
album	album	k1gNnSc4	album
Následuj	následovat	k5eAaImRp2nS	následovat
kojota	kojot	k1gMnSc2	kojot
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
album	album	k1gNnSc4	album
roku	rok	k1gInSc2	rok
v	v	k7c6	v
cenách	cena	k1gFnPc6	cena
Apollo	Apollo	k1gNnSc1	Apollo
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
nominace	nominace	k1gFnPc1	nominace
na	na	k7c4	na
ceny	cena	k1gFnPc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
festival	festival	k1gInSc4	festival
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
2015	[number]	k4	2015
skupina	skupina	k1gFnSc1	skupina
připravila	připravit	k5eAaPmAgFnS	připravit
vystoupení	vystoupení	k1gNnSc4	vystoupení
s	s	k7c7	s
členy	člen	k1gMnPc7	člen
Janáčkovy	Janáčkův	k2eAgFnSc2d1	Janáčkova
filharmonie	filharmonie	k1gFnSc2	filharmonie
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Orchestraci	orchestrace	k1gFnSc4	orchestrace
písniček	písnička	k1gFnPc2	písnička
napsal	napsat	k5eAaBmAgMnS	napsat
Stano	Stano	k6eAd1	Stano
Palúch	Palúch	k1gMnSc1	Palúch
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Unger	Unger	k1gMnSc1	Unger
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
flétna	flétna	k1gFnSc1	flétna
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Fišer	Fišer	k1gMnSc1	Fišer
(	(	kIx(	(
<g/>
housle	housle	k1gFnPc4	housle
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Juklík	Juklík	k1gMnSc1	Juklík
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Caithaml	Caithaml	k1gMnSc1	Caithaml
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
Ondřej	Ondřej	k1gMnSc1	Ondřej
Slavík	Slavík	k1gMnSc1	Slavík
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
akordeon	akordeon	k1gInSc1	akordeon
<g/>
,	,	kIx,	,
beatbox	beatbox	k1gInSc1	beatbox
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Voní	vonět	k5eAaImIp3nP	vonět
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Hrdina	Hrdina	k1gMnSc1	Hrdina
počítačový	počítačový	k2eAgMnSc1d1	počítačový
hry	hra	k1gFnPc4	hra
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Soundtrack	soundtrack	k1gInSc1	soundtrack
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Kolik	kolika	k1gFnPc2	kolika
váží	vážit	k5eAaImIp3nS	vážit
vaše	váš	k3xOp2gFnSc1	váš
touha	touha	k1gFnSc1	touha
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
-	-	kIx~	-
album	album	k1gNnSc4	album
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
tanečnímu	taneční	k2eAgNnSc3d1	taneční
představení	představení	k1gNnSc3	představení
skupiny	skupina	k1gFnSc2	skupina
VerTeDance	VerTeDance	k1gFnSc2	VerTeDance
Následuj	následovat	k5eAaImRp2nS	následovat
kojota	kojot	k1gMnSc2	kojot
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
Čtyři	čtyři	k4xCgInPc1	čtyři
Honzové	Honzové	k2eAgMnPc2d1	Honzové
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
Ondřej	Ondřej	k1gMnSc1	Ondřej
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Střeleckého	střelecký	k2eAgInSc2d1	střelecký
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
-	-	kIx~	-
živě	živě	k6eAd1	živě
<g/>
,	,	kIx,	,
akusticky	akusticky	k6eAd1	akusticky
Kapela	kapela	k1gFnSc1	kapela
také	také	k9	také
hostuje	hostovat	k5eAaImIp3nS	hostovat
na	na	k7c6	na
Rodinném	rodinný	k2eAgNnSc6d1	rodinné
albu	album	k1gNnSc6	album
Jakuba	Jakub	k1gMnSc2	Jakub
Čermáka	Čermák	k1gMnSc2	Čermák
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
