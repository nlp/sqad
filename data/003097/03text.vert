<s>
Hermafrodit	hermafrodit	k1gMnSc1	hermafrodit
(	(	kIx(	(
<g/>
též	též	k9	též
obojetník	obojetník	k1gMnSc1	obojetník
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
živočich	živočich	k1gMnSc1	živočich
schopný	schopný	k2eAgMnSc1d1	schopný
produkovat	produkovat	k5eAaImF	produkovat
současně	současně	k6eAd1	současně
vajíčka	vajíčko	k1gNnPc4	vajíčko
i	i	k8xC	i
spermie	spermie	k1gFnPc4	spermie
<g/>
.	.	kIx.	.
</s>
<s>
Hermafrodit	hermafrodit	k1gMnSc1	hermafrodit
má	mít	k5eAaImIp3nS	mít
buďto	buďto	k8xC	buďto
varlata	varle	k1gNnPc1	varle
i	i	k8xC	i
vaječníky	vaječník	k1gInPc1	vaječník
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
tyto	tento	k3xDgInPc1	tento
orgány	orgán	k1gInPc1	orgán
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
tzv.	tzv.	kA	tzv.
obojetnou	obojetný	k2eAgFnSc7d1	obojetná
pohlavní	pohlavní	k2eAgFnSc7d1	pohlavní
žlázou	žláza	k1gFnSc7	žláza
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hermafroditům	hermafrodit	k1gMnPc3	hermafrodit
patří	patřit	k5eAaImIp3nP	patřit
mnozí	mnohý	k2eAgMnPc1d1	mnohý
měkkýši	měkkýš	k1gMnPc1	měkkýš
(	(	kIx(	(
<g/>
mimo	mimo	k6eAd1	mimo
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
a	a	k8xC	a
mlžů	mlž	k1gMnPc2	mlž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ploštěnci	ploštěnec	k1gMnPc1	ploštěnec
<g/>
,	,	kIx,	,
kroužkovci	kroužkovec	k1gMnPc1	kroužkovec
<g/>
,	,	kIx,	,
pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
třeba	třeba	k6eAd1	třeba
některé	některý	k3yIgFnPc4	některý
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
Hermafrodítos	Hermafrodítos	k1gMnSc1	Hermafrodítos
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
bohů	bůh	k1gMnPc2	bůh
Herma	Hermes	k1gMnSc2	Hermes
a	a	k8xC	a
Afrodity	Afrodita	k1gFnSc2	Afrodita
<g/>
,	,	kIx,	,
srostl	srůst	k5eAaPmAgMnS	srůst
s	s	k7c7	s
nymfou	nymfa	k1gFnSc7	nymfa
Salmakis	Salmakis	k1gFnSc2	Salmakis
v	v	k7c4	v
jedinou	jediný	k2eAgFnSc4d1	jediná
oboupohlavní	oboupohlavní	k2eAgFnSc4d1	oboupohlavní
bytost	bytost	k1gFnSc4	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
gonochorismem	gonochorismus	k1gInSc7	gonochorismus
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedinec	jedinec	k1gMnSc1	jedinec
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
změní	změnit	k5eAaPmIp3nS	změnit
nebo	nebo	k8xC	nebo
střídá	střídat	k5eAaImIp3nS	střídat
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
ryb	ryba	k1gFnPc2	ryba
s	s	k7c7	s
výrazným	výrazný	k2eAgNnSc7d1	výrazné
teritoriálním	teritoriální	k2eAgNnSc7d1	teritoriální
chováním	chování	k1gNnSc7	chování
samců	samec	k1gInPc2	samec
-	-	kIx~	-
mladé	mladý	k2eAgFnPc4d1	mladá
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgFnP	mít
šanci	šance	k1gFnSc4	šance
se	se	k3xPyFc4	se
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
mají	mít	k5eAaImIp3nP	mít
samičí	samičí	k2eAgNnPc4d1	samičí
pohlaví	pohlaví	k1gNnPc4	pohlaví
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
až	až	k9	až
když	když	k8xS	když
vyrostou	vyrůst	k5eAaPmIp3nP	vyrůst
<g/>
,	,	kIx,	,
přejdou	přejít	k5eAaPmIp3nP	přejít
na	na	k7c4	na
pohlaví	pohlaví	k1gNnPc4	pohlaví
samčí	samčí	k2eAgFnPc1d1	samčí
<g/>
.	.	kIx.	.
</s>
<s>
Sebeoplození	Sebeoplození	k1gNnSc1	Sebeoplození
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
případ	případ	k1gInSc4	případ
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
hermafroditů	hermafrodit	k1gMnPc2	hermafrodit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vzácné	vzácný	k2eAgNnSc1d1	vzácné
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
především	především	k9	především
u	u	k7c2	u
takových	takový	k3xDgInPc2	takový
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
nebyly	být	k5eNaImAgInP	být
kvůli	kvůli	k7c3	kvůli
svému	své	k1gNnSc3	své
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
schopny	schopen	k2eAgFnPc1d1	schopna
sehnat	sehnat	k5eAaPmF	sehnat
partnera	partner	k1gMnSc4	partner
k	k	k7c3	k
páření	páření	k1gNnSc3	páření
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
endoparazité	endoparazit	k1gMnPc1	endoparazit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tasemnice	tasemnice	k1gFnSc1	tasemnice
bezbranná	bezbranný	k2eAgFnSc1d1	bezbranná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sebeoplození	sebeoplození	k1gNnSc3	sebeoplození
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
u	u	k7c2	u
samosprašných	samosprašný	k2eAgFnPc2d1	samosprašná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hermafrodit	hermafrodit	k1gMnSc1	hermafrodit
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
označuje	označovat	k5eAaImIp3nS	označovat
defektní	defektní	k2eAgMnSc1d1	defektní
jedinec	jedinec	k1gMnSc1	jedinec
gonochoristických	gonochoristický	k2eAgInPc2d1	gonochoristický
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
základy	základ	k1gInPc4	základ
obou	dva	k4xCgInPc2	dva
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
ženských	ženský	k2eAgFnPc2d1	ženská
i	i	k8xC	i
mužských	mužský	k2eAgFnPc2d1	mužská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
funkční	funkční	k2eAgFnPc1d1	funkční
obě	dva	k4xCgFnPc1	dva
-	-	kIx~	-
buďto	buďto	k8xC	buďto
funguje	fungovat	k5eAaImIp3nS	fungovat
jenom	jenom	k9	jenom
jedna	jeden	k4xCgFnSc1	jeden
nebo	nebo	k8xC	nebo
žádná	žádný	k3yNgFnSc1	žádný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
nebyl	být	k5eNaImAgMnS	být
hermafrodit	hermafrodit	k1gMnSc1	hermafrodit
s	s	k7c7	s
oběma	dva	k4xCgFnPc7	dva
funkčními	funkční	k2eAgFnPc7d1	funkční
soustavami	soustava	k1gFnPc7	soustava
zaznamenán	zaznamenán	k2eAgInSc4d1	zaznamenán
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgInPc4d1	zbylý
dva	dva	k4xCgInPc4	dva
případy	případ	k1gInPc4	případ
ano	ano	k9	ano
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nejvíce	hodně	k6eAd3	hodně
popularizovaným	popularizovaný	k2eAgInSc7d1	popularizovaný
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
hermafroditem	hermafrodit	k1gMnSc7	hermafrodit
je	být	k5eAaImIp3nS	být
jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
atletka	atletka	k1gFnSc1	atletka
Caster	Castra	k1gFnPc2	Castra
Semenyaová	Semenyaová	k1gFnSc1	Semenyaová
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
australského	australský	k2eAgInSc2d1	australský
listu	list	k1gInSc2	list
Sydney	Sydney	k1gNnSc1	Sydney
Morning	Morning	k1gInSc1	Morning
Herold	herold	k1gMnSc1	herold
má	mít	k5eAaImIp3nS	mít
mužské	mužský	k2eAgInPc4d1	mužský
i	i	k8xC	i
ženské	ženský	k2eAgInPc4d1	ženský
pohlavní	pohlavní	k2eAgInPc4d1	pohlavní
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Testy	test	k1gInPc1	test
prý	prý	k9	prý
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
prokázaly	prokázat	k5eAaPmAgInP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
místo	místo	k1gNnSc4	místo
vaječníků	vaječník	k1gInPc2	vaječník
varlata	varle	k1gNnPc1	varle
<g/>
.	.	kIx.	.
</s>
<s>
Intersexualita	Intersexualita	k1gFnSc1	Intersexualita
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
narodí	narodit	k5eAaPmIp3nS	narodit
s	s	k7c7	s
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
nejednoznačným	jednoznačný	k2eNgNnSc7d1	nejednoznačné
pohlavím	pohlaví	k1gNnSc7	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
též	též	k9	též
hermafroditismus	hermafroditismus	k1gInSc1	hermafroditismus
<g/>
.	.	kIx.	.
</s>
<s>
Intersexualita	Intersexualita	k1gFnSc1	Intersexualita
se	se	k3xPyFc4	se
jako	jako	k9	jako
anomálie	anomálie	k1gFnSc1	anomálie
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
prasat	prase	k1gNnPc2	prase
či	či	k8xC	či
u	u	k7c2	u
raků	rak	k1gMnPc2	rak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
botanice	botanika	k1gFnSc6	botanika
je	být	k5eAaImIp3nS	být
obdobou	obdoba	k1gFnSc7	obdoba
hermafrodita	hermafrodit	k1gMnSc2	hermafrodit
jednodomá	jednodomý	k2eAgFnSc1d1	jednodomá
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vyšší	vysoký	k2eAgFnSc1d2	vyšší
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
buďto	buďto	k8xC	buďto
oba	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
květů	květ	k1gInPc2	květ
(	(	kIx(	(
<g/>
samčí	samčí	k2eAgMnSc1d1	samčí
i	i	k8xC	i
samičí	samičí	k2eAgMnSc1d1	samičí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
květy	květ	k1gInPc4	květ
oboupohlavní	oboupohlavní	k2eAgInPc4d1	oboupohlavní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říši	říš	k1gFnSc6	říš
rostlin	rostlina	k1gFnPc2	rostlina
toto	tento	k3xDgNnSc4	tento
uspořádání	uspořádání	k1gNnSc4	uspořádání
převládá	převládat	k5eAaImIp3nS	převládat
<g/>
.	.	kIx.	.
</s>
