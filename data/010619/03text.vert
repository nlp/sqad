<p>
<s>
Ferrari	Ferrari	k1gMnSc7	Ferrari
je	být	k5eAaImIp3nS	být
italská	italský	k2eAgFnSc1d1	italská
automobilová	automobilový	k2eAgFnSc1d1	automobilová
značka	značka	k1gFnSc1	značka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
proslula	proslout	k5eAaPmAgFnS	proslout
především	především	k9	především
svými	svůj	k3xOyFgInPc7	svůj
sportovními	sportovní	k2eAgInPc7d1	sportovní
vozy	vůz	k1gInPc7	vůz
a	a	k8xC	a
účastí	účast	k1gFnSc7	účast
v	v	k7c6	v
automobilových	automobilový	k2eAgInPc6d1	automobilový
závodech	závod	k1gInPc6	závod
(	(	kIx(	(
<g/>
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
především	především	k9	především
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Firmu	firma	k1gFnSc4	firma
založil	založit	k5eAaPmAgMnS	založit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
Enzo	Enzo	k1gMnSc1	Enzo
Ferrari	Ferrari	k1gMnSc1	Ferrari
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
koncernu	koncern	k1gInSc2	koncern
Fiat	fiat	k1gInSc1	fiat
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
městě	město	k1gNnSc6	město
Maranello	Maranello	k1gNnSc1	Maranello
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
činila	činit	k5eAaImAgFnS	činit
produkce	produkce	k1gFnSc1	produkce
přes	přes	k7c4	přes
8	[number]	k4	8
000	[number]	k4	000
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
firma	firma	k1gFnSc1	firma
za	za	k7c4	za
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
existenci	existence	k1gFnSc4	existence
více	hodně	k6eAd2	hodně
než	než	k8xS	než
190	[number]	k4	190
000	[number]	k4	000
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
zakládal	zakládat	k5eAaImAgMnS	zakládat
Enzo	Enzo	k1gMnSc1	Enzo
Ferrari	Ferrari	k1gMnSc1	Ferrari
v	v	k7c6	v
Modeně	Modena	k1gFnSc6	Modena
společnost	společnost	k1gFnSc1	společnost
Scuderia	Scuderium	k1gNnSc2	Scuderium
Ferrari	Ferrari	k1gMnSc2	Ferrari
určenou	určený	k2eAgFnSc4d1	určená
k	k	k7c3	k
sponzorování	sponzorování	k1gNnSc3	sponzorování
amatérských	amatérský	k2eAgMnPc2d1	amatérský
automobilových	automobilový	k2eAgMnPc2d1	automobilový
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
neměl	mít	k5eNaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
vyrábět	vyrábět	k5eAaImF	vyrábět
silniční	silniční	k2eAgInPc4d1	silniční
vozy	vůz	k1gInPc4	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Ferrari	Ferrari	k1gMnSc1	Ferrari
připravoval	připravovat	k5eAaImAgMnS	připravovat
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
v	v	k7c6	v
závodění	závodění	k1gNnSc6	závodění
mnoha	mnoho	k4c2	mnoho
jezdcům	jezdec	k1gInPc3	jezdec
firmy	firma	k1gFnSc2	firma
Alfa	alfa	k1gFnSc1	alfa
Romeo	Romeo	k1gMnSc1	Romeo
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
jako	jako	k8xS	jako
šéf	šéf	k1gMnSc1	šéf
závodního	závodní	k2eAgInSc2d1	závodní
týmu	tým	k1gInSc2	tým
Alfy	alfa	k1gFnSc2	alfa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
Alfu	alfa	k1gFnSc4	alfa
Romeo	Romeo	k1gMnSc1	Romeo
částečně	částečně	k6eAd1	částečně
převzala	převzít	k5eAaPmAgFnS	převzít
italská	italský	k2eAgFnSc1d1	italská
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
válečné	válečný	k2eAgFnSc2d1	válečná
zbrojní	zbrojní	k2eAgFnSc2d1	zbrojní
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
řídil	řídit	k5eAaImAgMnS	řídit
Ferrari	Ferrari	k1gMnSc7	Ferrari
byla	být	k5eAaImAgFnS	být
však	však	k9	však
tak	tak	k6eAd1	tak
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
nedotkla	dotknout	k5eNaPmAgFnS	dotknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
Ferrarimu	Ferrarima	k1gFnSc4	Ferrarima
závodní	závodní	k2eAgFnSc4d1	závodní
činnost	činnost	k1gFnSc4	činnost
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
zakázána	zakázán	k2eAgFnSc1d1	zakázána
ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
s	s	k7c7	s
Alfou	alfa	k1gFnSc7	alfa
<g/>
,	,	kIx,	,
Scuderia	Scuderium	k1gNnPc1	Scuderium
se	se	k3xPyFc4	se
přeměnila	přeměnit	k5eAaPmAgNnP	přeměnit
na	na	k7c4	na
Auto	auto	k1gNnSc4	auto
Avio	Avio	k6eAd1	Avio
Costruzioni	Costruzion	k1gMnPc1	Costruzion
Ferrari	Ferrari	k1gMnSc1	Ferrari
<g/>
,	,	kIx,	,
vyrábějící	vyrábějící	k2eAgInPc4d1	vyrábějící
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
díly	díl	k1gInPc4	díl
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
SEFAC	SEFAC	kA	SEFAC
(	(	kIx(	(
<g/>
Scuderia	Scuderium	k1gNnSc2	Scuderium
Enzo	Enzo	k6eAd1	Enzo
Ferrari	ferrari	k1gNnSc2	ferrari
Auto	auto	k1gNnSc1	auto
Corse	Corse	k1gFnSc2	Corse
<g/>
)	)	kIx)	)
Ferrari	ferrari	k1gNnSc1	ferrari
během	během	k7c2	během
"	"	kIx"	"
<g/>
nezávodního	závodní	k2eNgNnSc2d1	nezávodní
<g/>
"	"	kIx"	"
období	období	k1gNnSc2	období
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
jediný	jediný	k2eAgInSc1d1	jediný
typ	typ	k1gInSc1	typ
závodního	závodní	k2eAgInSc2d1	závodní
vozu	vůz	k1gInSc2	vůz
Tipo	Tipo	k6eAd1	Tipo
815	[number]	k4	815
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
vlastně	vlastně	k9	vlastně
první	první	k4xOgInSc1	první
automobil	automobil	k1gInSc1	automobil
Ferrari	Ferrari	k1gMnSc2	Ferrari
<g/>
.	.	kIx.	.
</s>
<s>
Představil	představit	k5eAaPmAgInS	představit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
Mille	Mill	k1gMnSc2	Mill
Miglia	Miglius	k1gMnSc2	Miglius
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jediné	jediný	k2eAgInPc4d1	jediný
dva	dva	k4xCgInPc4	dva
prototypy	prototyp	k1gInPc4	prototyp
řídili	řídit	k5eAaImAgMnP	řídit
Lotario	Lotario	k6eAd1	Lotario
Rangoni	Rangon	k1gMnPc1	Rangon
a	a	k8xC	a
Alberto	Alberta	k1gFnSc5	Alberta
Ascari	Ascar	k1gInSc3	Ascar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
válce	válka	k1gFnSc3	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
závodě	závod	k1gInSc6	závod
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
konkurencí	konkurence	k1gFnSc7	konkurence
poměřit	poměřit	k5eAaPmF	poměřit
nemohli	moct	k5eNaImAgMnP	moct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
Ferrari	Ferrari	k1gMnSc1	Ferrari
přemístil	přemístit	k5eAaPmAgMnS	přemístit
továrnu	továrna	k1gFnSc4	továrna
do	do	k7c2	do
Maranella	Maranello	k1gNnSc2	Maranello
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působí	působit	k5eAaImIp3nS	působit
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Továrna	továrna	k1gFnSc1	továrna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
bombardována	bombardován	k2eAgFnSc1d1	bombardována
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
a	a	k8xC	a
přibyly	přibýt	k5eAaPmAgFnP	přibýt
i	i	k9	i
nové	nový	k2eAgInPc4d1	nový
provozy	provoz	k1gInPc4	provoz
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
silničních	silniční	k2eAgInPc2d1	silniční
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
===	===	k?	===
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
skutečným	skutečný	k2eAgInSc7d1	skutečný
závodním	závodní	k2eAgInSc7d1	závodní
vozem	vůz	k1gInSc7	vůz
Ferrari	ferrari	k1gNnSc2	ferrari
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
stal	stát	k5eAaPmAgInS	stát
125	[number]	k4	125
S	s	k7c7	s
<g/>
,	,	kIx,	,
poháněný	poháněný	k2eAgInSc4d1	poháněný
dvanáctiválcovým	dvanáctiválcový	k2eAgInSc7d1	dvanáctiválcový
vidlicovým	vidlicový	k2eAgInSc7d1	vidlicový
motorem	motor	k1gInSc7	motor
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
1,5	[number]	k4	1,5
litru	litr	k1gInSc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Enzo	Enzo	k6eAd1	Enzo
celkem	celkem	k6eAd1	celkem
nerad	nerad	k2eAgMnSc1d1	nerad
stavěl	stavět	k5eAaImAgMnS	stavět
a	a	k8xC	a
prodával	prodávat	k5eAaImAgMnS	prodávat
automobily	automobil	k1gInPc4	automobil
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musel	muset	k5eAaImAgMnS	muset
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
dále	daleko	k6eAd2	daleko
podporovat	podporovat	k5eAaImF	podporovat
tým	tým	k1gInSc4	tým
Scuderia	Scuderium	k1gNnSc2	Scuderium
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc7	jeho
životní	životní	k2eAgFnSc7d1	životní
láskou	láska	k1gFnSc7	láska
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
závodění	závodění	k1gNnSc1	závodění
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
si	se	k3xPyFc3	se
však	však	k9	však
jeho	jeho	k3xOp3gInPc4	jeho
vozy	vůz	k1gInPc4	vůz
rychle	rychle	k6eAd1	rychle
získaly	získat	k5eAaPmAgInP	získat
pověst	pověst	k1gFnSc4	pověst
výjimečných	výjimečný	k2eAgInPc2d1	výjimečný
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Enzo	Enzo	k1gMnSc1	Enzo
byl	být	k5eAaImAgMnS	být
proslulý	proslulý	k2eAgMnSc1d1	proslulý
svou	svůj	k3xOyFgFnSc7	svůj
"	"	kIx"	"
<g/>
nelibostí	nelibost	k1gFnSc7	nelibost
<g/>
"	"	kIx"	"
balzak	balzak	k6eAd1	balzak
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většinu	většina	k1gFnSc4	většina
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gInSc1	jeho
vozy	vůz	k1gInPc1	vůz
kupují	kupovat	k5eAaImIp3nP	kupovat
kvůli	kvůli	k7c3	kvůli
prestiži	prestiž	k1gFnSc3	prestiž
a	a	k8xC	a
ne	ne	k9	ne
kvůli	kvůli	k7c3	kvůli
technickým	technický	k2eAgInPc3d1	technický
a	a	k8xC	a
sportovním	sportovní	k2eAgInPc3d1	sportovní
výkonům	výkon	k1gInPc3	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Výkony	výkon	k1gInPc1	výkon
jeho	jeho	k3xOp3gInPc2	jeho
automobilů	automobil	k1gInPc2	automobil
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
převyšovaly	převyšovat	k5eAaImAgInP	převyšovat
jen	jen	k6eAd1	jen
vozy	vůz	k1gInPc1	vůz
španělské	španělský	k2eAgFnSc2d1	španělská
firmy	firma	k1gFnSc2	firma
Pegaso	Pegasa	k1gFnSc5	Pegasa
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
však	však	k9	však
později	pozdě	k6eAd2	pozdě
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Silniční	silniční	k2eAgInPc1d1	silniční
vozy	vůz	k1gInPc1	vůz
Ferrari	Ferrari	k1gMnSc2	Ferrari
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc1d1	známý
také	také	k9	také
svým	svůj	k3xOyFgInSc7	svůj
pozoruhodným	pozoruhodný	k2eAgInSc7d1	pozoruhodný
designem	design	k1gInSc7	design
<g/>
,	,	kIx,	,
navrhovala	navrhovat	k5eAaImAgNnP	navrhovat
nejlepší	dobrý	k2eAgNnPc1d3	nejlepší
návrhářská	návrhářský	k2eAgNnPc1d1	návrhářské
studia	studio	k1gNnPc1	studio
jako	jako	k8xC	jako
Pininfarina	Pininfarin	k2eAgFnSc1d1	Pininfarina
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nezbytným	zbytný	k2eNgInSc7d1	zbytný
doplňkem	doplněk	k1gInSc7	doplněk
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
návrzích	návrh	k1gInPc6	návrh
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
i	i	k8xC	i
firmy	firma	k1gFnSc2	firma
Scaglietti	Scaglietť	k1gFnSc2	Scaglietť
<g/>
,	,	kIx,	,
Bertone	Berton	k1gInSc5	Berton
<g/>
,	,	kIx,	,
Touring	Touring	k1gInSc1	Touring
<g/>
,	,	kIx,	,
Ghia	Ghia	k1gFnSc1	Ghia
<g/>
,	,	kIx,	,
a	a	k8xC	a
Vignale	Vignale	k1gFnSc1	Vignale
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
Fiat	fiat	k1gInSc4	fiat
Group	Group	k1gInSc1	Group
85	[number]	k4	85
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
Ferrari	Ferrari	k1gMnSc2	Ferrari
<g/>
,	,	kIx,	,
Mubadala	Mubadal	k1gMnSc2	Mubadal
Development	Development	k1gInSc4	Development
Company	Compana	k1gFnPc4	Compana
5	[number]	k4	5
<g/>
%	%	kIx~	%
a	a	k8xC	a
Enzův	Enzův	k2eAgMnSc1d1	Enzův
syn	syn	k1gMnSc1	syn
Piero	Piero	k1gNnSc4	Piero
Ferrari	Ferrari	k1gMnSc2	Ferrari
10	[number]	k4	10
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sériové	sériový	k2eAgInPc1d1	sériový
modely	model	k1gInPc1	model
==	==	k?	==
</s>
</p>
<p>
<s>
Pojmenování	pojmenování	k1gNnSc4	pojmenování
modelů	model	k1gInPc2	model
Ferrari	Ferrari	k1gMnPc2	Ferrari
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
měnilo	měnit	k5eAaImAgNnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
trojmístné	trojmístný	k2eAgNnSc1d1	trojmístné
číslo	číslo	k1gNnSc1	číslo
značilo	značit	k5eAaImAgNnS	značit
objem	objem	k1gInSc1	objem
jednoho	jeden	k4xCgInSc2	jeden
válce	válec	k1gInSc2	válec
motoru	motor	k1gInSc2	motor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
modely	model	k1gInPc1	model
250	[number]	k4	250
<g/>
,	,	kIx,	,
365	[number]	k4	365
nebo	nebo	k8xC	nebo
nejnověji	nově	k6eAd3	nově
456	[number]	k4	456
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
označování	označování	k1gNnSc2	označování
se	se	k3xPyFc4	se
u	u	k7c2	u
dvanáctiválcových	dvanáctiválcový	k2eAgInPc2d1	dvanáctiválcový
modelů	model	k1gInPc2	model
udržel	udržet	k5eAaPmAgInS	udržet
až	až	k6eAd1	až
do	do	k7c2	do
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
také	také	k9	také
začíná	začínat	k5eAaImIp3nS	začínat
používat	používat	k5eAaImF	používat
trojmístné	trojmístný	k2eAgNnSc1d1	trojmístné
označení	označení	k1gNnSc1	označení
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
objemu	objem	k1gInSc2	objem
motoru	motor	k1gInSc2	motor
a	a	k8xC	a
počtu	počet	k1gInSc2	počet
válců	válec	k1gInPc2	válec
–	–	k?	–
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
Dino	Dino	k1gMnSc1	Dino
Ferrari	Ferrari	k1gMnSc1	Ferrari
206	[number]	k4	206
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
objem	objem	k1gInSc1	objem
dva	dva	k4xCgInPc4	dva
litry	litr	k1gInPc4	litr
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
válců	válec	k1gInPc2	válec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
např.	např.	kA	např.
512	[number]	k4	512
BB	BB	kA	BB
(	(	kIx(	(
<g/>
pět	pět	k4xCc4	pět
litrů	litr	k1gInPc2	litr
<g/>
,	,	kIx,	,
dvanáct	dvanáct	k4xCc4	dvanáct
válců	válec	k1gInPc2	válec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
pojmenován	pojmenován	k2eAgInSc4d1	pojmenován
model	model	k1gInSc4	model
612	[number]	k4	612
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
moderní	moderní	k2eAgMnPc1d1	moderní
Ferrari	Ferrari	k1gMnPc1	Ferrari
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
číselném	číselný	k2eAgNnSc6d1	číselné
označení	označení	k1gNnSc6	označení
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
pouze	pouze	k6eAd1	pouze
objem	objem	k1gInSc1	objem
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
GT	GT	kA	GT
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
vpředu	vpředu	k6eAd1	vpředu
===	===	k?	===
</s>
</p>
<p>
<s>
250	[number]	k4	250
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
330	[number]	k4	330
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
275	[number]	k4	275
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
365	[number]	k4	365
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
365	[number]	k4	365
Daytona	Daytona	k1gFnSc1	Daytona
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
400	[number]	k4	400
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
412	[number]	k4	412
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
456	[number]	k4	456
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
550	[number]	k4	550
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
575M	[number]	k4	575M
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
612	[number]	k4	612
Scaglietti	Scaglietti	k1gNnPc1	Scaglietti
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
599	[number]	k4	599
GTB	GTB	kA	GTB
Fiorano	Fiorana	k1gFnSc5	Fiorana
(	(	kIx(	(
<g/>
od	od	k7c2	od
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
California	Californium	k1gNnPc1	Californium
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ferrari	Ferrari	k1gMnSc1	Ferrari
FF	ff	kA	ff
(	(	kIx(	(
<g/>
od	od	k7c2	od
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ferrari	Ferrari	k1gMnSc1	Ferrari
F	F	kA	F
<g/>
12	[number]	k4	12
<g/>
berlinetta	berlinett	k1gInSc2	berlinett
(	(	kIx(	(
<g/>
od	od	k7c2	od
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Šesti-	Šesti-	k1gFnPc4	Šesti-
a	a	k8xC	a
osmiválcové	osmiválcový	k2eAgNnSc4d1	osmiválcové
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
uprostřed	uprostřed	k6eAd1	uprostřed
===	===	k?	===
</s>
</p>
<p>
<s>
Dino	Dino	k6eAd1	Dino
206	[number]	k4	206
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dino	Dino	k6eAd1	Dino
246	[number]	k4	246
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
308	[number]	k4	308
GT4	GT4	k1gFnSc1	GT4
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
208	[number]	k4	208
GT4	GT4	k1gFnSc1	GT4
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
308	[number]	k4	308
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
208	[number]	k4	208
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mondial	Mondial	k1gInSc1	Mondial
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
328	[number]	k4	328
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
348	[number]	k4	348
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
F355	F355	k4	F355
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
360	[number]	k4	360
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
F430	F430	k4	F430
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
458	[number]	k4	458
Italia	Italium	k1gNnPc1	Italium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
488	[number]	k4	488
GTB	GTB	kA	GTB
(	(	kIx(	(
<g/>
od	od	k7c2	od
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dvanáctiválcové	dvanáctiválcový	k2eAgFnPc4d1	dvanáctiválcová
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
uprostřed	uprostřed	k6eAd1	uprostřed
===	===	k?	===
</s>
</p>
<p>
<s>
365	[number]	k4	365
GT4	GT4	k1gFnSc1	GT4
BB	BB	kA	BB
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
512	[number]	k4	512
BB	BB	kA	BB
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Testarossa	Testarossa	k1gFnSc1	Testarossa
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
512	[number]	k4	512
TR	TR	kA	TR
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ferrari	Ferrari	k1gMnSc1	Ferrari
F12	F12	k1gMnSc1	F12
Berlinetta	Berlinetta	k1gMnSc1	Berlinetta
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ferrari	Ferrari	k1gMnSc1	Ferrari
812	[number]	k4	812
Superfast	Superfast	k1gInSc1	Superfast
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Supersporty	supersport	k1gInPc4	supersport
===	===	k?	===
</s>
</p>
<p>
<s>
250	[number]	k4	250
GTO	GTO	kA	GTO
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
288	[number]	k4	288
GTO	GTO	kA	GTO
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
F40	F40	k4	F40
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
F50	F50	k4	F50
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Enzo	Enzo	k1gNnSc1	Enzo
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FXX	FXX	kA	FXX
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
30	[number]	k4	30
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LaFerrari	LaFerrari	k1gNnSc1	LaFerrari
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FXX	FXX	kA	FXX
K	K	kA	K
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
jezdci	jezdec	k1gMnPc1	jezdec
Ferrari	ferrari	k1gNnSc1	ferrari
==	==	k?	==
</s>
</p>
<p>
<s>
Tazio	Tazio	k1gNnSc1	Tazio
Nuvolari	Nuvolar	k1gFnSc2	Nuvolar
</s>
</p>
<p>
<s>
Juan	Juan	k1gMnSc1	Juan
Manuel	Manuel	k1gMnSc1	Manuel
Fangio	Fangio	k1gMnSc1	Fangio
</s>
</p>
<p>
<s>
Luigi	Luigi	k1gNnSc1	Luigi
Chinetti	Chinetť	k1gFnSc2	Chinetť
</s>
</p>
<p>
<s>
Alberto	Alberta	k1gFnSc5	Alberta
Ascari	Ascar	k1gFnSc5	Ascar
</s>
</p>
<p>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
von	von	k1gInSc4	von
Trips	Trips	k1gInSc1	Trips
</s>
</p>
<p>
<s>
Phil	Phil	k1gMnSc1	Phil
Hill	Hill	k1gMnSc1	Hill
</s>
</p>
<p>
<s>
Olivier	Olivier	k1gMnSc1	Olivier
Gendebien	Gendebina	k1gFnPc2	Gendebina
</s>
</p>
<p>
<s>
Mike	Mike	k1gFnSc1	Mike
Hawthorn	Hawthorna	k1gFnPc2	Hawthorna
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Collins	Collinsa	k1gFnPc2	Collinsa
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Surtees	Surtees	k1gMnSc1	Surtees
</s>
</p>
<p>
<s>
Jacky	Jacky	k6eAd1	Jacky
Ickx	Ickx	k1gInSc1	Ickx
</s>
</p>
<p>
<s>
Mario	Mario	k1gMnSc1	Mario
Andretti	Andretť	k1gFnSc2	Andretť
</s>
</p>
<p>
<s>
Niki	Niki	k6eAd1	Niki
Lauda	Lauda	k1gFnSc1	Lauda
</s>
</p>
<p>
<s>
Carlos	Carlos	k1gMnSc1	Carlos
Reutemann	Reutemann	k1gMnSc1	Reutemann
</s>
</p>
<p>
<s>
Jody	jod	k1gInPc1	jod
Scheckter	Schecktra	k1gFnPc2	Schecktra
</s>
</p>
<p>
<s>
Gilles	Gilles	k1gInSc1	Gilles
Villeneuve	Villeneuev	k1gFnSc2	Villeneuev
</s>
</p>
<p>
<s>
Didier	Didier	k1gInSc4	Didier
Pironi	Piron	k1gMnPc1	Piron
</s>
</p>
<p>
<s>
Michele	Michele	k6eAd1	Michele
Alboreto	Alboret	k2eAgNnSc1d1	Alboret
</s>
</p>
<p>
<s>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Berger	Berger	k1gMnSc1	Berger
</s>
</p>
<p>
<s>
Nigel	Nigel	k1gMnSc1	Nigel
Mansell	Mansell	k1gMnSc1	Mansell
</s>
</p>
<p>
<s>
Alain	Alain	k2eAgInSc1d1	Alain
Prost	prost	k2eAgInSc1d1	prost
</s>
</p>
<p>
<s>
Jean	Jean	k1gMnSc1	Jean
Alesi	Ales	k1gMnSc3	Ales
</s>
</p>
<p>
<s>
Rubens	Rubens	k6eAd1	Rubens
Barrichello	Barrichello	k1gNnSc1	Barrichello
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
</s>
</p>
<p>
<s>
Felipe	Felipat	k5eAaPmIp3nS	Felipat
Massa	Massa	k1gFnSc1	Massa
</s>
</p>
<p>
<s>
Kimi	Kimi	k6eAd1	Kimi
Raikkonen	Raikkonen	k2eAgInSc1d1	Raikkonen
</s>
</p>
<p>
<s>
Giancarlo	Giancarlo	k1gNnSc1	Giancarlo
Fisichella	Fisichello	k1gNnSc2	Fisichello
</s>
</p>
<p>
<s>
Fernando	Fernanda	k1gFnSc5	Fernanda
Alonso	Alonsa	k1gFnSc5	Alonsa
</s>
</p>
<p>
<s>
Sebastian	Sebastian	k1gMnSc1	Sebastian
Vettel	Vettel	k1gMnSc1	Vettel
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
značek	značka	k1gFnPc2	značka
automobilů	automobil	k1gInPc2	automobil
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ferrari	ferrari	k1gNnSc2	ferrari
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
