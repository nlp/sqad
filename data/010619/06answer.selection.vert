<s>
Ferrari	Ferrari	k1gMnSc7	Ferrari
je	být	k5eAaImIp3nS	být
italská	italský	k2eAgFnSc1d1	italská
automobilová	automobilový	k2eAgFnSc1d1	automobilová
značka	značka	k1gFnSc1	značka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proslula	proslout	k5eAaPmAgFnS	proslout
především	především	k9	především
svými	svůj	k3xOyFgInPc7	svůj
sportovními	sportovní	k2eAgInPc7d1	sportovní
vozy	vůz	k1gInPc7	vůz
a	a	k8xC	a
účastí	účast	k1gFnSc7	účast
v	v	k7c6	v
automobilových	automobilový	k2eAgInPc6d1	automobilový
závodech	závod	k1gInPc6	závod
(	(	kIx(	(
<g/>
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
především	především	k9	především
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
