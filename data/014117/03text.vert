<s>
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
</s>
<s>
MUDr.	MUDr.	kA
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
<g/>
,	,	kIx,
MBA	MBA	kA
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
ministr	ministr	k1gMnSc1
zdravotnictví	zdravotnictví	k1gNnSc2
ČR	ČR	kA
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2006	#num#	k4
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Mirek	Mirek	k1gMnSc1
Topolánek	Topolánek	k1gMnSc1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
David	David	k1gMnSc1
Rath	Rath	k1gMnSc1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Daniela	Daniela	k1gFnSc1
Filipiová	Filipiový	k2eAgFnSc1d1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
6	#num#	k4
<g/>
.	.	kIx.
předseda	předseda	k1gMnSc1
Senátorského	senátorský	k2eAgInSc2d1
klubu	klub	k1gInSc2
ODS	ODS	kA
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2004	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2007	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Liška	Liška	k1gMnSc1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Stříteský	Stříteský	k2eAgMnSc1d1
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Stříteský	Stříteský	k2eAgMnSc1d1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
senátor	senátor	k1gMnSc1
za	za	k7c4
obvod	obvod	k1gInSc4
č.	č.	k?
55	#num#	k4
–	–	k?
Brno-město	Brno-města	k1gMnSc5
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1998	#num#	k4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Vlasta	Vlasta	k1gMnSc1
Svobodová	Svobodová	k1gFnSc1
Nástupce	nástupce	k1gMnSc2
</s>
<s>
Jan	Jan	k1gMnSc1
Žaloudík	Žaloudík	k1gMnSc1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
ODS	ODS	kA
(	(	kIx(
<g/>
od	od	k7c2
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1956	#num#	k4
(	(	kIx(
<g/>
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Brno	Brno	k1gNnSc1
Československo	Československo	k1gNnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Choť	choť	k1gFnSc1
</s>
<s>
ženatý	ženatý	k2eAgMnSc1d1
Děti	dítě	k1gFnPc1
</s>
<s>
dva	dva	k4xCgMnPc1
synové	syn	k1gMnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
UJEP	UJEP	kA
v	v	k7c6
Brně	Brno	k1gNnSc6
Profese	profes	k1gFnSc2
</s>
<s>
lékař	lékař	k1gMnSc1
Webová	webový	k2eAgNnPc5d1
stránka	stránka	k1gFnSc1
</s>
<s>
Archív	archív	k1gInSc1
Commons	Commonsa	k1gFnPc2
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1956	#num#	k4
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
za	za	k7c4
ODS	ODS	kA
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
2006	#num#	k4
až	až	k9
2009	#num#	k4
byl	být	k5eAaImAgMnS
ministrem	ministr	k1gMnSc7
zdravotnictví	zdravotnictví	k1gNnSc2
ČR	ČR	kA
v	v	k7c6
první	první	k4xOgFnSc6
a	a	k8xC
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
vládě	vláda	k1gFnSc6
Mirka	Mirka	k1gFnSc1
Topolánka	Topolánka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
Masarykovu	Masarykův	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
(	(	kIx(
<g/>
obor	obor	k1gInSc4
všeobecné	všeobecný	k2eAgNnSc4d1
lékařství	lékařství	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
MBA	MBA	kA
na	na	k7c4
Brno	Brno	k1gNnSc4
International	International	k1gMnPc2
Business	business	k1gInSc1
School	Schoola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
působil	působit	k5eAaImAgMnS
jako	jako	k9
lékař	lékař	k1gMnSc1
ve	v	k7c6
Svitavách	Svitava	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Ivančicích	Ivančice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
ODS	ODS	kA
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
senátor	senátor	k1gMnSc1
<g/>
,	,	kIx,
ve	v	k7c6
volbách	volba	k1gFnPc6
2010	#num#	k4
svůj	svůj	k3xOyFgInSc4
mandát	mandát	k1gInSc4
neobhájil	obhájit	k5eNaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
jej	on	k3xPp3gMnSc4
v	v	k7c6
obou	dva	k4xCgNnPc6
kolech	kolo	k1gNnPc6
porazil	porazit	k5eAaPmAgMnS
nestraník	nestraník	k1gMnSc1
kandidující	kandidující	k2eAgMnSc1d1
za	za	k7c4
sociální	sociální	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
Jan	Jan	k1gMnSc1
Žaloudík	Žaloudík	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
říjnu	říjen	k1gInSc3
2012	#num#	k4
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgMnS
jako	jako	k9
poradce	poradce	k1gMnSc1
premiéra	premiér	k1gMnSc2
Petra	Petr	k1gMnSc2
Nečase	Nečas	k1gMnSc2
pro	pro	k7c4
zdravotnictví	zdravotnictví	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spolu	spolu	k6eAd1
s	s	k7c7
některými	některý	k3yIgInPc7
členy	člen	k1gInPc4
svého	svůj	k3xOyFgInSc2
týmu	tým	k1gInSc2
působí	působit	k5eAaImIp3nS
ve	v	k7c6
sdružení	sdružení	k1gNnSc6
Reforma	reforma	k1gFnSc1
zdravotnictví-forum	zdravotnictví-forum	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
mj.	mj.	kA
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
nadnárodní	nadnárodní	k2eAgFnSc7d1
poradenskou	poradenský	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
Kaiser	Kaiser	k1gMnSc1
Permanente	permanent	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Činnost	činnost	k1gFnSc1
ve	v	k7c6
funkci	funkce	k1gFnSc6
ministra	ministr	k1gMnSc2
zdravotnictví	zdravotnictví	k1gNnSc2
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
pohled	pohled	k1gInSc1
</s>
<s>
Jako	jako	k8xC,k8xS
stoupenec	stoupenec	k1gMnSc1
neoliberální	neoliberální	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
a	a	k8xC
sociální	sociální	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
prosazoval	prosazovat	k5eAaImAgMnS
širokou	široký	k2eAgFnSc4d1
otevřenost	otevřenost	k1gFnSc4
zdravotnictví	zdravotnictví	k1gNnSc2
pro	pro	k7c4
vstup	vstup	k1gInSc4
soukromého	soukromý	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
a	a	k8xC
tvorbu	tvorba	k1gFnSc4
privátního	privátní	k2eAgInSc2d1
zisku	zisk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravoval	připravovat	k5eAaImAgMnS
individuální	individuální	k2eAgFnSc4d1
kapitalizaci	kapitalizace	k1gFnSc4
zdravotního	zdravotní	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
záměr	záměr	k1gInSc1
transformovat	transformovat	k5eAaBmF
fakultní	fakultní	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
a	a	k8xC
zdravotní	zdravotní	k2eAgFnSc2d1
pojišťovny	pojišťovna	k1gFnSc2
do	do	k7c2
podoby	podoba	k1gFnSc2
akciových	akciový	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
vyvolal	vyvolat	k5eAaPmAgInS
protesty	protest	k1gInPc1
četných	četný	k2eAgNnPc2d1
sdružení	sdružení	k1gNnSc2
pacientů	pacient	k1gMnPc2
a	a	k8xC
lékařů	lékař	k1gMnPc2
<g/>
,	,	kIx,
většiny	většina	k1gFnSc2
univerzit	univerzita	k1gFnPc2
<g/>
,	,	kIx,
studentů	student	k1gMnPc2
lékařských	lékařský	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
i	i	k8xC
odborových	odborový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpůrci	odpůrce	k1gMnPc1
Julínkových	Julínkových	k2eAgInPc2d1
záměrů	záměr	k1gInPc2
poukazují	poukazovat	k5eAaImIp3nP
na	na	k7c4
nevratnost	nevratnost	k1gFnSc4
transformačních	transformační	k2eAgInPc2d1
kroků	krok	k1gInPc2
<g/>
,	,	kIx,
neprůhlednost	neprůhlednost	k1gFnSc1
vlastnických	vlastnický	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
podnikatelských	podnikatelský	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
ve	v	k7c6
zdravotnictví	zdravotnictví	k1gNnSc6
a	a	k8xC
neochotu	neochota	k1gFnSc4
vzít	vzít	k5eAaPmF
v	v	k7c4
potaz	potaz	k1gInSc4
zahraniční	zahraniční	k2eAgFnSc2d1
zkušenosti	zkušenost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
plánům	plán	k1gInPc3
Julínkova	Julínkův	k2eAgNnSc2d1
vedení	vedení	k1gNnSc2
ministerstva	ministerstvo	k1gNnSc2
postupně	postupně	k6eAd1
začaly	začít	k5eAaPmAgFnP
vystupovat	vystupovat	k5eAaImF
obě	dva	k4xCgFnPc1
menší	malý	k2eAgFnPc1d2
koaliční	koaliční	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnPc1
i	i	k8xC
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
<g/>
,	,	kIx,
a	a	k8xC
nakonec	nakonec	k6eAd1
i	i	k9
část	část	k1gFnSc1
regionálních	regionální	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
ODS	ODS	kA
(	(	kIx(
<g/>
někteří	některý	k3yIgMnPc1
z	z	k7c2
kandidátů	kandidát	k1gMnPc2
v	v	k7c6
podzimních	podzimní	k2eAgFnPc6d1
krajských	krajský	k2eAgFnPc6d1
a	a	k8xC
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2008	#num#	k4
byl	být	k5eAaImAgMnS
proto	proto	k8xC
Julínek	Julínek	k1gMnSc1
donucen	donutit	k5eAaPmNgMnS
stáhnout	stáhnout	k5eAaPmF
nejkontroverznější	kontroverzní	k2eAgFnSc4d3
část	část	k1gFnSc4
připravovaných	připravovaný	k2eAgInPc2d1
zdravotnických	zdravotnický	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
funkce	funkce	k1gFnSc2
byl	být	k5eAaImAgMnS
odvolán	odvolán	k2eAgMnSc1d1
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
při	při	k7c6
hromadné	hromadný	k2eAgFnSc6d1
rekonstrukci	rekonstrukce	k1gFnSc6
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Regulační	regulační	k2eAgInPc1d1
poplatky	poplatek	k1gInPc1
</s>
<s>
Jednou	jeden	k4xCgFnSc7
ze	z	k7c2
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
se	se	k3xPyFc4
Julínkovu	Julínkův	k2eAgInSc3d1
týmu	tým	k1gInSc3
podařilo	podařit	k5eAaPmAgNnS
prosadit	prosadit	k5eAaPmF
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
přijetí	přijetí	k1gNnSc1
zákona	zákon	k1gInSc2
o	o	k7c6
regulačních	regulační	k2eAgInPc6d1
poplatcích	poplatek	k1gInPc6
(	(	kIx(
<g/>
též	též	k9
poplatky	poplatek	k1gInPc4
za	za	k7c4
návštěvy	návštěva	k1gFnPc4
u	u	k7c2
lékaře	lékař	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
vstoupil	vstoupit	k5eAaPmAgInS
v	v	k7c4
účinnost	účinnost	k1gFnSc4
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2008	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
svými	svůj	k3xOyFgMnPc7
tvůrci	tvůrce	k1gMnPc7
obhajován	obhajován	k2eAgInSc4d1
jako	jako	k9
prostředek	prostředek	k1gInSc4
k	k	k7c3
zamezení	zamezení	k1gNnSc3
nadbytečných	nadbytečný	k2eAgFnPc2d1
návštěv	návštěva	k1gFnPc2
zdravotnických	zdravotnický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
Ústavní	ústavní	k2eAgInSc1d1
soud	soud	k1gInSc1
ČR	ČR	kA
rozhodl	rozhodnout	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
regulační	regulační	k2eAgInPc1d1
poplatky	poplatek	k1gInPc1
nejsou	být	k5eNaImIp3nP
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
Ústavou	ústava	k1gFnSc7
ČR	ČR	kA
<g/>
,	,	kIx,
sedm	sedm	k4xCc1
ústavních	ústavní	k2eAgMnPc2d1
soudců	soudce	k1gMnPc2
včetně	včetně	k7c2
předsedy	předseda	k1gMnSc2
a	a	k8xC
místopředsedkyně	místopředsedkyně	k1gFnSc2
ÚS	ÚS	kA
zveřejnilo	zveřejnit	k5eAaPmAgNnS
proti	proti	k7c3
tomuto	tento	k3xDgInSc3
nálezu	nález	k1gInSc3
odlišné	odlišný	k2eAgNnSc1d1
stanovisko	stanovisko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červnu	červen	k1gInSc6
2008	#num#	k4
vláda	vláda	k1gFnSc1
Mirka	Mirka	k1gFnSc1
Topolánka	Topolánka	k1gFnSc1
zrušila	zrušit	k5eAaPmAgFnS
regulační	regulační	k2eAgInPc4d1
poplatky	poplatek	k1gInPc4
pro	pro	k7c4
novorozence	novorozenec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Řízená	řízený	k2eAgFnSc1d1
péče	péče	k1gFnSc1
</s>
<s>
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
představil	představit	k5eAaPmAgInS
tzv.	tzv.	kA
řízenou	řízený	k2eAgFnSc4d1
péči	péče	k1gFnSc4
–	–	k?
koncept	koncept	k1gInSc4
zdravotní	zdravotní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
praktikovaný	praktikovaný	k2eAgInSc4d1
například	například	k6eAd1
v	v	k7c6
americkém	americký	k2eAgNnSc6d1
zdravotnictví	zdravotnictví	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
Spor	spor	k1gInSc1
se	s	k7c7
senátním	senátní	k2eAgMnSc7d1
kandidátem	kandidát	k1gMnSc7
ODS	ODS	kA
Zdeňkem	Zdeněk	k1gMnSc7
Schwarzem	Schwarz	k1gMnSc7
</s>
<s>
Julínek	Julínek	k1gMnSc1
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
pro	pro	k7c4
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
označil	označit	k5eAaPmAgInS
šéfa	šéf	k1gMnSc4
pražské	pražský	k2eAgFnSc2d1
záchranky	záchranka	k1gFnSc2
Zdeňka	Zdeněk	k1gMnSc2
Schwarze	Schwarz	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
za	za	k7c2
ODS	ODS	kA
kandiduje	kandidovat	k5eAaImIp3nS
do	do	k7c2
Senátu	senát	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c4
„	„	k?
<g/>
kandidáta	kandidát	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
popírá	popírat	k5eAaImIp3nS
jednak	jednak	k8xC
ideje	idea	k1gFnPc1
<g/>
,	,	kIx,
jednak	jednak	k8xC
značku	značka	k1gFnSc4
ODS	ODS	kA
<g/>
“	“	k?
a	a	k8xC
naznačil	naznačit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mu	on	k3xPp3gInSc3
jeho	jeho	k3xOp3gNnSc1
prohra	prohra	k1gFnSc1
ve	v	k7c6
volbách	volba	k1gFnPc6
nevadila	vadit	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
výrokem	výrok	k1gInSc7
popudil	popudit	k5eAaPmAgMnS
pražskou	pražský	k2eAgFnSc4d1
ODS	ODS	kA
a	a	k8xC
následně	následně	k6eAd1
v	v	k7c6
deníku	deník	k1gInSc6
Právo	právo	k1gNnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gNnSc4
vyjádření	vyjádření	k1gNnSc4
údajně	údajně	k6eAd1
„	„	k?
<g/>
byla	být	k5eAaImAgFnS
vytržená	vytržený	k2eAgFnSc1d1
z	z	k7c2
kontextu	kontext	k1gInSc2
<g/>
“	“	k?
a	a	k8xC
že	že	k8xS
„	„	k?
<g/>
odpovídal	odpovídat	k5eAaImAgMnS
na	na	k7c4
obecný	obecný	k2eAgInSc4d1
dotaz	dotaz	k1gInSc4
novináře	novinář	k1gMnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
dívá	dívat	k5eAaImIp3nS
na	na	k7c4
nezávislé	závislý	k2eNgMnPc4d1
kandidáty	kandidát	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
jasně	jasně	k6eAd1
neprezentují	prezentovat	k5eNaBmIp3nP
pod	pod	k7c7
hlavičkou	hlavička	k1gFnSc7
strany	strana	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
HN	HN	kA
nicméně	nicméně	k8xC
doložily	doložit	k5eAaPmAgFnP
zvukovou	zvukový	k2eAgFnSc7d1
nahrávkou	nahrávka	k1gFnSc7
rozhovoru	rozhovor	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
Julínek	Julínek	k1gMnSc1
byl	být	k5eAaImAgMnS
jednoznačně	jednoznačně	k6eAd1
a	a	k8xC
přímo	přímo	k6eAd1
dotazován	dotazovat	k5eAaImNgMnS
na	na	k7c4
kandidáta	kandidát	k1gMnSc4
ODS	ODS	kA
Zdeňka	Zdeněk	k1gMnSc4
Schwarze	Schwarz	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
Julínka	Julínek	k1gMnSc2
kritizoval	kritizovat	k5eAaImAgMnS
za	za	k7c4
zákon	zákon	k1gInSc4
o	o	k7c6
záchranné	záchranný	k2eAgFnSc6d1
službě	služba	k1gFnSc6
a	a	k8xC
za	za	k7c4
snahu	snaha	k1gFnSc4
udělat	udělat	k5eAaPmF
z	z	k7c2
fakultních	fakultní	k2eAgFnPc2d1
nemocnic	nemocnice	k1gFnPc2
akciové	akciový	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Schwarz	Schwarz	k1gMnSc1
v	v	k7c6
následné	následný	k2eAgFnSc6d1
reakci	reakce	k1gFnSc6
obvinil	obvinit	k5eAaPmAgMnS
Julínka	Julínek	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
mluvčího	mluvčí	k1gMnSc2
Tomáše	Tomáš	k1gMnSc2
Cikrta	Cikrt	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
škodí	škodit	k5eAaImIp3nP
ODS	ODS	kA
<g/>
,	,	kIx,
„	„	k?
<g/>
selhali	selhat	k5eAaPmAgMnP
a	a	k8xC
udělali	udělat	k5eAaPmAgMnP
zásadní	zásadní	k2eAgFnSc4d1
politickou	politický	k2eAgFnSc4d1
chybu	chyba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
by	by	kYmCp3nP
měl	mít	k5eAaImAgMnS
ministr	ministr	k1gMnSc1
Julínek	Julínek	k1gMnSc1
i	i	k8xC
jeho	jeho	k3xOp3gMnSc1
mluvčí	mluvčí	k1gMnSc1
Cikrt	Cikrta	k1gFnPc2
odstoupit	odstoupit	k5eAaPmF
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
kritice	kritika	k1gFnSc3
Julínka	Julínek	k1gMnSc2
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgMnS
také	také	k9
první	první	k4xOgMnSc1
místopředseda	místopředseda	k1gMnSc1
ODS	ODS	kA
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
ODS	ODS	kA
a	a	k8xC
pražský	pražský	k2eAgMnSc1d1
primátor	primátor	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Bém	Bém	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výzvy	výzev	k1gInPc1
k	k	k7c3
náhradě	náhrada	k1gFnSc3
v	v	k7c6
ministerské	ministerský	k2eAgFnSc6d1
funkci	funkce	k1gFnSc6
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
2008	#num#	k4
krátce	krátce	k6eAd1
pro	pro	k7c4
prohře	prohra	k1gFnSc6
ODS	ODS	kA
v	v	k7c6
krajských	krajský	k2eAgFnPc6d1
a	a	k8xC
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
vyzval	vyzvat	k5eAaPmAgMnS
k	k	k7c3
náhradě	náhrada	k1gFnSc3
Julínka	Julínek	k1gMnSc2
ve	v	k7c6
funkci	funkce	k1gFnSc6
místopředseda	místopředseda	k1gMnSc1
strany	strana	k1gFnSc2
Petr	Petr	k1gMnSc1
Bendl	Bendl	k1gMnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
Julínek	Julínek	k1gMnSc1
ve	v	k7c6
vedení	vedení	k1gNnSc6
ministerstva	ministerstvo	k1gNnSc2
významně	významně	k6eAd1
přispěly	přispět	k5eAaPmAgFnP
k	k	k7c3
volební	volební	k2eAgFnSc3d1
porážce	porážka	k1gFnSc3
ODS	ODS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
odstoupení	odstoupení	k1gNnSc3
Julínka	Julínek	k1gMnSc2
vyzvali	vyzvat	k5eAaPmAgMnP
i	i	k9
poslanci	poslanec	k1gMnPc1
Boris	Boris	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
(	(	kIx(
<g/>
ODS	ODS	kA
<g/>
)	)	kIx)
a	a	k8xC
Kateřina	Kateřina	k1gFnSc1
Jacques	Jacques	k1gMnSc1
(	(	kIx(
<g/>
SZ	SZ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
politických	politický	k2eAgInPc6d1
kuloárech	kuloár	k1gInPc6
se	se	k3xPyFc4
pak	pak	k6eAd1
objevily	objevit	k5eAaPmAgFnP
spekulace	spekulace	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Julínka	Julínek	k1gMnSc2
mohl	moct	k5eAaImAgMnS
nahradit	nahradit	k5eAaPmF
bývalý	bývalý	k2eAgMnSc1d1
hejtman	hejtman	k1gMnSc1
Plzeňského	plzeňský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
Petr	Petr	k1gMnSc1
Zimmermann	Zimmermann	k1gMnSc1
(	(	kIx(
<g/>
ODS	ODS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
řediteli	ředitel	k1gMnSc3
Fakultní	fakultní	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Leoš	Leoš	k1gMnSc1
Heger	Heger	k1gMnSc1
nebo	nebo	k8xC
poslanec	poslanec	k1gMnSc1
Boris	Boris	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
svém	svůj	k3xOyFgInSc6
sjezdu	sjezd	k1gInSc6
vyzvala	vyzvat	k5eAaPmAgFnS
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2008	#num#	k4
k	k	k7c3
odvolání	odvolání	k1gNnSc3
Julínka	Julínek	k1gMnSc2
z	z	k7c2
ministerské	ministerský	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
také	také	k9
Česká	český	k2eAgFnSc1d1
lékařská	lékařský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
KDU-ČSL	KDU-ČSL	k1gFnSc2
</s>
<s>
Předseda	předseda	k1gMnSc1
zdravotní	zdravotní	k2eAgFnSc2d1
komise	komise	k1gFnSc2
KDU-ČSL	KDU-ČSL	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Carbol	Carbol	k1gInSc4
se	s	k7c7
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
v	v	k7c6
oficiálním	oficiální	k2eAgNnSc6d1
prohlášení	prohlášení	k1gNnSc6
zaslaném	zaslaný	k2eAgNnSc6d1
médiím	médium	k1gNnPc3
velmi	velmi	k6eAd1
kriticky	kriticky	k6eAd1
vyjádřil	vyjádřit	k5eAaPmAgMnS
k	k	k7c3
reformě	reforma	k1gFnSc3
zdravotnictví	zdravotnictví	k1gNnSc2
Tomáše	Tomáš	k1gMnSc2
Julínka	Julínek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Carbola	Carbola	k1gFnSc1
ministerstvo	ministerstvo	k1gNnSc1
vedené	vedený	k2eAgNnSc1d1
Julínkem	Julínek	k1gMnSc7
„	„	k?
<g/>
dlouhodobě	dlouhodobě	k6eAd1
podceňuje	podceňovat	k5eAaImIp3nS
nedobrou	dobrý	k2eNgFnSc4d1
situaci	situace	k1gFnSc4
v	v	k7c6
nemocnicích	nemocnice	k1gFnPc6
a	a	k8xC
léčebnách	léčebna	k1gFnPc6
dlouhodobě	dlouhodobě	k6eAd1
nemocných	nemocný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
na	na	k7c6
účtech	účet	k1gInPc6
pojišťoven	pojišťovna	k1gFnPc2
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
40	#num#	k4
miliard	miliarda	k4xCgFnPc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgNnPc6
zařízeních	zařízení	k1gNnPc6
nemají	mít	k5eNaImIp3nP
dostatek	dostatek	k1gInSc4
peněz	peníze	k1gInPc2
na	na	k7c4
provoz	provoz	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
principiálním	principiální	k2eAgMnPc3d1
kritikům	kritik	k1gMnPc3
Julínkem	Julínek	k1gMnSc7
zamýšlených	zamýšlený	k2eAgFnPc2d1
reforem	reforma	k1gFnPc2
z	z	k7c2
řad	řada	k1gFnPc2
představitelů	představitel	k1gMnPc2
KDU-ČSL	KDU-ČSL	k1gFnSc2
patří	patřit	k5eAaImIp3nS
poslanec	poslanec	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hovorka	Hovorka	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obvinění	obviněný	k1gMnPc1
ze	z	k7c2
sabotování	sabotování	k1gNnSc2
reforem	reforma	k1gFnPc2
členy	člen	k1gMnPc7
ODS	ODS	kA
</s>
<s>
Na	na	k7c6
stranickém	stranický	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
ODS	ODS	kA
počátkem	počátkem	k7c2
prosince	prosinec	k1gInSc2
2008	#num#	k4
Julínek	Julínek	k1gMnSc1
obvinil	obvinit	k5eAaPmAgMnS
z	z	k7c2
údajného	údajný	k2eAgNnSc2d1
sabotování	sabotování	k1gNnSc2
zdravotnických	zdravotnický	k2eAgFnPc2d1
reforem	reforma	k1gFnPc2
starosty	starosta	k1gMnSc2
za	za	k7c4
ODS	ODS	kA
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
začali	začít	k5eAaPmAgMnP
vracet	vracet	k5eAaImF
poplatky	poplatek	k1gInPc4
zavedené	zavedený	k2eAgInPc4d1
jeho	jeho	k3xOp3gNnSc7
ministerstvem	ministerstvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Julínka	Julínek	k1gMnSc2
se	se	k3xPyFc4
někteří	některý	k3yIgMnPc1
kandidáti	kandidát	k1gMnPc1
ODS	ODS	kA
před	před	k7c7
volbami	volba	k1gFnPc7
„	„	k?
<g/>
svou	svůj	k3xOyFgFnSc7
kampaní	kampaň	k1gFnSc7
nelišili	lišit	k5eNaImAgMnP
od	od	k7c2
svých	svůj	k3xOyFgFnPc2
komunistických	komunistický	k2eAgFnPc2d1
a	a	k8xC
sociálně	sociálně	k6eAd1
demokratických	demokratický	k2eAgMnPc2d1
kandidátů	kandidát	k1gMnPc2
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
vlastní	vlastní	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
a	a	k8xC
jednoho	jeden	k4xCgMnSc4
vnuka	vnuk	k1gMnSc4
Jakuba	Jakub	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Volby	volba	k1gFnPc1
senát	senát	k1gInSc1
2010	#num#	k4
<g/>
↑	↑	k?
Poradní	poradní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
předsedy	předseda	k1gMnSc2
vlády	vláda	k1gFnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
www.vlada.cz	www.vlada.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.reformazdravotnictvi.cz	www.reformazdravotnictvi.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HOLUB	Holub	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Julínek	Julínek	k1gMnSc1
otvírá	otvírat	k5eAaImIp3nS
zdravotnictví	zdravotnictví	k1gNnSc4
byznysu	byznys	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firmy	firma	k1gFnPc1
se	se	k3xPyFc4
slétají	slétat	k5eAaPmIp3nP,k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2008-04-28	2008-04-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WIRNITZER	WIRNITZER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
POLÁKOVÁ	Poláková	k1gFnSc1
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
;	;	kIx,
BARTOŠ	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
B.	B.	kA
Klaus	Klaus	k1gMnSc1
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
nové	nový	k2eAgMnPc4d1
ministry	ministr	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemáte	mít	k5eNaImIp2nP
sto	sto	k4xCgNnSc4
dnů	den	k1gInPc2
hájení	hájení	k1gNnSc2
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
jim	on	k3xPp3gMnPc3
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2009-01-23	2009-01-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rozhodnutí	rozhodnutí	k1gNnSc1
Ústavního	ústavní	k2eAgInSc2d1
soudu	soud	k1gInSc2
ČR	ČR	kA
z	z	k7c2
20	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
↑	↑	k?
HOLUB	Holub	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leden	leden	k1gInSc1
2009	#num#	k4
<g/>
:	:	kIx,
Připravte	připravit	k5eAaPmRp2nP
se	se	k3xPyFc4
na	na	k7c4
řízenou	řízený	k2eAgFnSc4d1
péči	péče	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2008-05-10	2008-05-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LEINERT	LEINERT	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
přesně	přesně	k6eAd1
řekl	říct	k5eAaPmAgMnS
Julínek	Julínek	k1gMnSc1
o	o	k7c4
kandidátovi	kandidát	k1gMnSc6
ODS	ODS	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
Poslechněte	poslechnout	k5eAaPmRp2nP
si	se	k3xPyFc3
audio	audio	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2008-10-08	2008-10-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VAŠEK	Vašek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kandidát	kandidát	k1gMnSc1
ODS	ODS	kA
Schwarz	Schwarz	k1gMnSc1
<g/>
:	:	kIx,
Julínek	Julínek	k1gMnSc1
s	s	k7c7
Cikrtem	Cikrt	k1gInSc7
by	by	kYmCp3nP
měli	mít	k5eAaImAgMnP
odstoupit	odstoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2008-10-10	2008-10-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VAŠEK	Vašek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bém	Bém	k1gMnSc1
<g/>
:	:	kIx,
Julínek	Julínek	k1gMnSc1
a	a	k8xC
Cikrt	Cikrt	k1gInSc1
před	před	k7c7
volbami	volba	k1gFnPc7
poškozují	poškozovat	k5eAaImIp3nP
ODS	ODS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2008-10-10	2008-10-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vyměňme	vyměnit	k5eAaPmRp1nP
Julínka	Julínek	k1gMnSc4
<g/>
,	,	kIx,
navrhuje	navrhovat	k5eAaImIp3nS
Bendl	Bendl	k1gFnSc3
kolegům	kolega	k1gMnPc3
z	z	k7c2
ODS	ODS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2008-10-21	2008-10-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Julínek	Julínek	k1gMnSc1
je	být	k5eAaImIp3nS
nejohroženějším	ohrožený	k2eAgMnSc7d3
členem	člen	k1gMnSc7
kabinetu	kabinet	k1gInSc2
<g/>
,	,	kIx,
odstupovat	odstupovat	k5eAaImF
nehodlá	hodlat	k5eNaImIp3nS
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
toh	toh	k?
<g/>
,	,	kIx,
Novinky	novinka	k1gFnPc1
<g/>
;	;	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
lékařská	lékařský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
vyzvala	vyzvat	k5eAaPmAgFnS
k	k	k7c3
sesazení	sesazení	k1gNnSc3
Julínka	Julínek	k1gMnSc2
<g/>
,	,	kIx,
premiér	premiér	k1gMnSc1
mu	on	k3xPp3gMnSc3
věří	věřit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
2008-11-09	2008-11-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LEINERT	LEINERT	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovci	lidovec	k1gMnPc1
po	po	k7c6
volbách	volba	k1gFnPc6
odsoudili	odsoudit	k5eAaPmAgMnP
zdravotní	zdravotní	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
<g/>
:	:	kIx,
Julínek	Julínek	k1gMnSc1
ji	on	k3xPp3gFnSc4
nezvládl	zvládnout	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2008-10-26	2008-10-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Andrej	Andrej	k1gMnSc1
Bažant	Bažant	k1gMnSc1
Josef	Josef	k1gMnSc1
Lux	Lux	k1gMnSc1
by	by	kYmCp3nS
z	z	k7c2
vlády	vláda	k1gFnSc2
dávno	dávno	k6eAd1
odešel	odejít	k5eAaPmAgMnS
<g/>
,	,	kIx,
Literární	literární	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
43	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
22	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
:	:	kIx,
Julínek	Julínek	k1gMnSc1
viní	vinit	k5eAaImIp3nS
kolegy	kolega	k1gMnPc4
z	z	k7c2
ODS	ODS	kA
ze	z	k7c2
sabotování	sabotování	k1gNnSc2
reforem	reforma	k1gFnPc2
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2008	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
7	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Archivované	archivovaný	k2eAgFnPc1d1
oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
fungovaly	fungovat	k5eAaImAgFnP
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
v	v	k7c6
pořadu	pořad	k1gInSc6
Impulsy	impuls	k1gInPc4
Václava	Václav	k1gMnSc4
Moravce	Moravec	k1gMnSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Audiozáznam	Audiozáznam	k1gInSc1
rozhovoru	rozhovor	k1gInSc2
Tomáše	Tomáš	k1gMnSc2
Julínka	Julínek	k1gMnSc2
s	s	k7c7
redaktorem	redaktor	k1gMnSc7
Hospodářských	hospodářský	k2eAgFnPc2d1
novin	novina	k1gFnPc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Julínkova	Julínkův	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
přivedla	přivést	k5eAaPmAgFnS
lidi	člověk	k1gMnPc4
ke	k	k7c3
drahým	drahý	k2eAgInPc3d1
lékům	lék	k1gInPc3
<g/>
,	,	kIx,
Aktuálně	aktuálně	k6eAd1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2009	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
První	první	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Mirka	Mirek	k1gMnSc4
Topolánka	Topolánek	k1gMnSc4
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Mirek	Mirek	k1gMnSc1
Topolánek	Topolánek	k1gMnSc1
(	(	kIx(
<g/>
jmenován	jmenován	k2eAgMnSc1d1
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
Členové	člen	k1gMnPc1
v	v	k7c4
den	den	k1gInSc4
jmenování	jmenování	k1gNnSc2
vlády	vláda	k1gFnSc2
<g/>
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2006	#num#	k4
</s>
<s>
Petr	Petr	k1gMnSc1
Nečas	Nečas	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
práce	práce	k1gFnSc2
a	a	k8xC
sociální	sociální	k2eAgFnSc2d1
věci	věc	k1gFnSc2
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Ivan	Ivan	k1gMnSc1
Langer	Langer	k1gMnSc1
(	(	kIx(
<g/>
vnitro	vnitro	k1gNnSc1
<g/>
,	,	kIx,
informatika	informatika	k1gFnSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Vondra	Vondra	k1gMnSc1
(	(	kIx(
<g/>
zahraniční	zahraniční	k2eAgFnPc1d1
věci	věc	k1gFnPc1
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Jiří	Jiří	k1gMnSc1
Šedivý	Šedivý	k1gMnSc1
(	(	kIx(
<g/>
obrana	obrana	k1gFnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
nestraník	nestraník	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Vlastimil	Vlastimil	k1gMnSc1
Tlustý	tlustý	k2eAgMnSc1d1
(	(	kIx(
<g/>
finance	finance	k1gFnPc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Aleš	Aleš	k1gMnSc1
Řebíček	řebíček	k1gInSc1
(	(	kIx(
<g/>
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Martin	Martin	k1gMnSc1
Říman	Říman	k1gMnSc1
(	(	kIx(
<g/>
průmysl	průmysl	k1gInSc1
a	a	k8xC
obchod	obchod	k1gInSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Milena	Milena	k1gFnSc1
Vicenová	Vicenová	k1gFnSc1
(	(	kIx(
<g/>
zemědělství	zemědělství	k1gNnSc1
<g/>
,	,	kIx,
nestranička	nestranička	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
(	(	kIx(
<g/>
zdravotnictví	zdravotnictví	k1gNnSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Miroslava	Miroslava	k1gFnSc1
Kopicová	Kopicová	k1gFnSc1
(	(	kIx(
<g/>
školství	školství	k1gNnSc1
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc1
a	a	k8xC
tělovýchova	tělovýchova	k1gFnSc1
<g/>
,	,	kIx,
nestranička	nestranička	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Petr	Petr	k1gMnSc1
Kalaš	Kalaš	k1gMnSc1
(	(	kIx(
<g/>
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Martin	Martin	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
(	(	kIx(
<g/>
kultura	kultura	k1gFnSc1
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Petr	Petr	k1gMnSc1
Gandalovič	Gandalovič	k1gMnSc1
(	(	kIx(
<g/>
místní	místní	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Jiří	Jiří	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
(	(	kIx(
<g/>
spravedlnost	spravedlnost	k1gFnSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
</s>
<s>
Druhá	druhý	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Mirka	Mirek	k1gMnSc4
Topolánka	Topolánek	k1gMnSc4
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Mirek	Mirek	k1gMnSc1
Topolánek	Topolánek	k1gMnSc1
(	(	kIx(
<g/>
jmenován	jmenován	k2eAgMnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
Členové	člen	k1gMnPc1
v	v	k7c4
den	den	k1gInSc4
jmenování	jmenování	k1gNnSc2
vlády	vláda	k1gFnSc2
<g/>
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Čunek	Čunek	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
místní	místní	k2eAgFnSc2d1
rozvoj	rozvoj	k1gInSc4
–	–	k?
demise	demise	k1gFnSc2
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Martin	Martin	k1gMnSc1
Bursík	Bursík	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
,	,	kIx,
školství	školství	k1gNnSc1
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc1
a	a	k8xC
tělovýchova	tělovýchova	k1gFnSc1
–	–	k?
pověřen	pověřen	k2eAgInSc1d1
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2007	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
odvolán	odvolán	k2eAgInSc4d1
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
SZ	SZ	kA
<g/>
)	)	kIx)
•	•	k?
Petr	Petr	k1gMnSc1
Nečas	Nečas	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
práce	práce	k1gFnSc2
a	a	k8xC
sociální	sociální	k2eAgFnSc2d1
věci	věc	k1gFnSc2
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Vondra	Vondra	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
pro	pro	k7c4
evropské	evropský	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Vlasta	Vlasta	k1gFnSc1
Parkanová	Parkanová	k1gFnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
obrana	obrana	k1gFnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
vlády	vláda	k1gFnSc2
–	–	k?
od	od	k7c2
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Ivan	Ivan	k1gMnSc1
Langer	Langer	k1gMnSc1
(	(	kIx(
<g/>
vnitro	vnitro	k1gNnSc1
<g/>
,	,	kIx,
informatika	informatika	k1gFnSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
Schwarzenberg	Schwarzenberg	k1gMnSc1
(	(	kIx(
<g/>
zahraniční	zahraniční	k2eAgFnPc1d1
věci	věc	k1gFnPc1
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
za	za	k7c4
SZ	SZ	kA
<g/>
)	)	kIx)
•	•	k?
Martin	Martin	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Říman	Říman	k1gMnSc1
(	(	kIx(
<g/>
průmysl	průmysl	k1gInSc1
a	a	k8xC
obchod	obchod	k1gInSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
(	(	kIx(
<g/>
zdravotnictví	zdravotnictví	k1gNnSc1
–	–	k?
odvolán	odvolán	k2eAgMnSc1d1
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Jiří	Jiří	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
(	(	kIx(
<g/>
spravedlnost	spravedlnost	k1gFnSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Kalousek	Kalousek	k1gMnSc1
(	(	kIx(
<g/>
finance	finance	k1gFnPc1
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Dana	Dana	k1gFnSc1
Kuchtová	Kuchtová	k1gFnSc1
(	(	kIx(
<g/>
školství	školství	k1gNnSc1
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc1
a	a	k8xC
tělovýchova	tělovýchova	k1gFnSc1
–	–	k?
demise	demise	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
SZ	SZ	kA
<g/>
)	)	kIx)
•	•	k?
Aleš	Aleš	k1gMnSc1
Řebíček	řebíček	k1gInSc1
(	(	kIx(
<g/>
doprava	doprava	k1gFnSc1
–	–	k?
odvolán	odvolán	k2eAgInSc1d1
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Petr	Petr	k1gMnSc1
Gandalovič	Gandalovič	k1gMnSc1
(	(	kIx(
<g/>
zemědělství	zemědělství	k1gNnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Helena	Helena	k1gFnSc1
Třeštíková	Třeštíková	k1gFnSc1
(	(	kIx(
<g/>
kultura	kultura	k1gFnSc1
–	–	k?
demise	demise	k1gFnSc1
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
nestranička	nestranička	k1gFnSc1
za	za	k7c4
KDU-ČSL	KDU-ČSL	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
Cyril	Cyril	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
(	(	kIx(
<g/>
předseda	předseda	k1gMnSc1
Legislativní	legislativní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
vlády	vláda	k1gFnSc2
–	–	k?
odvolán	odvolán	k2eAgInSc1d1
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
místní	místní	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
–	–	k?
pověřen	pověřen	k2eAgInSc1d1
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Džamila	Džamila	k1gFnSc1
Stehlíková	Stehlíková	k1gFnSc1
(	(	kIx(
<g/>
lidská	lidský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
a	a	k8xC
národnostní	národnostní	k2eAgFnPc1d1
menšiny	menšina	k1gFnPc1
–	–	k?
odvolána	odvolán	k2eAgFnSc1d1
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
SZ	SZ	kA
<g/>
)	)	kIx)
Členové	člen	k1gMnPc1
jmenovaní	jmenovaný	k2eAgMnPc1d1
později	pozdě	k6eAd2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
</s>
<s>
Václav	Václav	k1gMnSc1
Jehlička	jehlička	k1gFnSc1
(	(	kIx(
<g/>
kultura	kultura	k1gFnSc1
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2007	#num#	k4
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
(	(	kIx(
<g/>
školství	školství	k1gNnSc1
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc1
a	a	k8xC
tělovýchova	tělovýchova	k1gFnSc1
<g/>
,	,	kIx,
SZ	SZ	kA
<g/>
)	)	kIx)
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2008	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Čunek	Čunek	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
místní	místní	k2eAgFnSc2d1
rozvoj	rozvoj	k1gInSc4
–	–	k?
demise	demise	k1gFnSc2
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
</s>
<s>
Daniela	Daniela	k1gFnSc1
Filipiová	Filipiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
zdravotnictví	zdravotnictví	k1gNnSc2
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Petr	Petr	k1gMnSc1
Bendl	Bendl	k1gMnSc1
(	(	kIx(
<g/>
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Pavel	Pavel	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
(	(	kIx(
<g/>
předseda	předseda	k1gMnSc1
Legislativní	legislativní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Michael	Michael	k1gMnSc1
Kocáb	Kocáb	k1gMnSc1
(	(	kIx(
<g/>
lidská	lidský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
a	a	k8xC
národnostní	národnostní	k2eAgFnPc1d1
menšiny	menšina	k1gFnPc1
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
za	za	k7c4
SZ	SZ	kA
<g/>
)	)	kIx)
</s>
<s>
Ministři	ministr	k1gMnPc1
zdravotnictví	zdravotnictví	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
19992000	#num#	k4
<g/>
–	–	k?
<g/>
20062006	#num#	k4
<g/>
–	–	k?
<g/>
20162016	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
</s>
<s>
1993	#num#	k4
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Lom	lom	k1gInSc4
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
:	:	kIx,
Luděk	Luděk	k1gMnSc1
Rubáš	rubat	k5eAaImIp2nS
</s>
<s>
1995	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Stráský	Stráský	k1gMnSc1
</s>
<s>
1998	#num#	k4
<g/>
:	:	kIx,
Zuzana	Zuzana	k1gFnSc1
Roithová	Roithová	k1gFnSc1
</s>
<s>
1998	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
:	:	kIx,
Ivan	Ivan	k1gMnSc1
David	David	k1gMnSc1
</s>
<s>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
:	:	kIx,
Vladimír	Vladimír	k1gMnSc1
Špidla	Špidla	k1gMnSc1
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
:	:	kIx,
Bohumil	Bohumil	k1gMnSc1
Fišer	Fišer	k1gMnSc1
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
:	:	kIx,
Marie	Marie	k1gFnSc1
Součková	Součková	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
:	:	kIx,
Jozef	Jozef	k1gMnSc1
Kubinyi	Kubiny	k1gFnSc2
</s>
<s>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
:	:	kIx,
Milada	Milada	k1gFnSc1
Emmerová	Emmerová	k1gFnSc1
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Škromach	Škromach	k1gMnSc1
</s>
<s>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
:	:	kIx,
David	David	k1gMnSc1
Rath	Rath	k1gMnSc1
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
:	:	kIx,
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
Daniela	Daniela	k1gFnSc1
Filipiová	Filipiový	k2eAgFnSc1d1
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
:	:	kIx,
Dana	Dana	k1gFnSc1
Jurásková	Jurásková	k1gFnSc1
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
:	:	kIx,
Leoš	Leoš	k1gMnSc1
Heger	Heger	k1gMnSc1
</s>
<s>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
:	:	kIx,
Martin	Martin	k1gMnSc1
Holcát	Holcát	k1gMnSc1
</s>
<s>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
:	:	kIx,
Svatopluk	Svatopluk	k1gMnSc1
Němeček	Němeček	k1gMnSc1
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
:	:	kIx,
Miloslav	Miloslav	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
</s>
<s>
2017	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
:	:	kIx,
Adam	Adam	k1gMnSc1
Vojtěch	Vojtěch	k1gMnSc1
</s>
<s>
2020	#num#	k4
<g/>
:	:	kIx,
Roman	Roman	k1gMnSc1
Prymula	Prymul	k1gMnSc2
</s>
<s>
2020	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Blatný	blatný	k2eAgMnSc1d1
</s>
<s>
od	od	k7c2
r.	r.	kA
2021	#num#	k4
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Arenberger	Arenberger	k1gMnSc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
zdravotnictví	zdravotnictví	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Senátor	senátor	k1gMnSc1
za	za	k7c4
obvod	obvod	k1gInSc4
č.	č.	k?
55	#num#	k4
–	–	k?
Brno-město	Brno-města	k1gMnSc5
</s>
<s>
1996	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
:	:	kIx,
Vlasta	Vlasta	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
•	•	k?
1998	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
:	:	kIx,
Tomáš	Tomáš	k1gMnSc1
Julínek	Julínek	k1gMnSc1
•	•	k?
2010	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Žaloudík	Žaloudík	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
nlk	nlk	k?
<g/>
20020125267	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84840440	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
</s>
