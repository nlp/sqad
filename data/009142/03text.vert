<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
či	či	k8xC	či
levhart	levhart	k1gMnSc1	levhart
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
čínský	čínský	k2eAgMnSc1d1	čínský
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
pardus	pardus	k1gInSc1	pardus
japonensis	japonensis	k1gFnSc1	japonensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poddruh	poddruh	k1gInSc4	poddruh
či	či	k8xC	či
nejnověji	nově	k6eAd3	nově
pouhá	pouhý	k2eAgFnSc1d1	pouhá
forma	forma	k1gFnSc1	forma
(	(	kIx(	(
<g/>
taxonomická	taxonomický	k2eAgFnSc1d1	Taxonomická
revize	revize	k1gFnSc1	revize
vydaná	vydaný	k2eAgFnSc1d1	vydaná
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
ho	on	k3xPp3gMnSc4	on
řadí	řadit	k5eAaImIp3nP	řadit
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
levhart	levhart	k1gMnSc1	levhart
mandžuský	mandžuský	k2eAgMnSc1d1	mandžuský
<g/>
)	)	kIx)	)
levharta	levhart	k1gMnSc4	levhart
skvrnitého	skvrnitý	k2eAgMnSc4d1	skvrnitý
žijící	žijící	k2eAgFnSc7d1	žijící
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Podobá	podobat	k5eAaImIp3nS	podobat
se	se	k3xPyFc4	se
levhartu	levhart	k1gMnSc3	levhart
mandžuskému	mandžuský	k2eAgMnSc3d1	mandžuský
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
tmavší	tmavý	k2eAgNnSc1d2	tmavší
oranžovo	oranžovo	k1gNnSc1	oranžovo
<g/>
–	–	k?	–
<g/>
cihlové	cihlový	k2eAgNnSc4d1	cihlové
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
především	především	k9	především
savci	savec	k1gMnPc1	savec
malé	malý	k2eAgFnSc2d1	malá
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
jeho	on	k3xPp3gInSc2	on
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
fragmentovaný	fragmentovaný	k2eAgInSc1d1	fragmentovaný
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
probádán	probádán	k2eAgInSc1d1	probádán
a	a	k8xC	a
IUCN	IUCN	kA	IUCN
tento	tento	k3xDgInSc4	tento
poddruh	poddruh	k1gInSc4	poddruh
podle	podle	k7c2	podle
stupně	stupeň	k1gInSc2	stupeň
ohrožení	ohrožení	k1gNnSc2	ohrožení
nevyhodnocuje	vyhodnocovat	k5eNaImIp3nS	vyhodnocovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnPc4	taxonomie
a	a	k8xC	a
popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Britský	britský	k2eAgMnSc1d1	britský
zoolog	zoolog	k1gMnSc1	zoolog
John	John	k1gMnSc1	John
Edward	Edward	k1gMnSc1	Edward
Gray	Graa	k1gFnSc2	Graa
popsal	popsat	k5eAaPmAgMnS	popsat
levharta	levhart	k1gMnSc4	levhart
čínského	čínský	k2eAgMnSc4d1	čínský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jedné	jeden	k4xCgFnSc2	jeden
kožešiny	kožešina	k1gFnSc2	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Nazval	nazvat	k5eAaPmAgMnS	nazvat
ho	on	k3xPp3gNnSc4	on
nesprávně	správně	k6eNd1	správně
levhartem	levhart	k1gMnSc7	levhart
japonským	japonský	k2eAgNnSc7d1	Japonské
(	(	kIx(	(
<g/>
Leopardus	Leopardus	k1gInSc1	Leopardus
japonensis	japonensis	k1gFnSc2	japonensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kožešina	kožešina	k1gFnSc1	kožešina
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc4	pojmenování
binomické	binomický	k2eAgFnSc2d1	binomická
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
trinomické	trinomický	k2eAgNnSc4d1	trinomický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jméno	jméno	k1gNnSc1	jméno
japonensis	japonensis	k1gFnPc2	japonensis
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
nebylo	být	k5eNaImAgNnS	být
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
alternativní	alternativní	k2eAgInPc1d1	alternativní
názvy	název	k1gInPc1	název
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
šelma	šelma	k1gFnSc1	šelma
nazývána	nazýván	k2eAgFnSc1d1	nazývána
华	华	k?	华
nebo	nebo	k8xC	nebo
中	中	k?	中
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
pin-yin	pinina	k1gFnPc2	pin-yina
přepisu	přepis	k1gInSc2	přepis
huáběi	huábě	k1gFnSc2	huábě
bà	bà	k?	bà
a	a	k8xC	a
zhō	zhō	k?	zhō
bà	bà	k?	bà
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
čínský	čínský	k2eAgMnSc1d1	čínský
levhart	levhart	k1gMnSc1	levhart
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
severočínský	severočínský	k2eAgMnSc1d1	severočínský
levhart	levhart	k1gMnSc1	levhart
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
zlato-mincový	zlatoincový	k2eAgMnSc1d1	zlato-mincový
levhart	levhart	k1gMnSc1	levhart
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnSc1	jeho
barva	barva	k1gFnSc1	barva
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
zlatavý	zlatavý	k2eAgInSc1d1	zlatavý
odstín	odstín	k1gInSc1	odstín
a	a	k8xC	a
rozety	rozeta	k1gFnPc1	rozeta
připomínají	připomínat	k5eAaImIp3nP	připomínat
mince	mince	k1gFnPc1	mince
<g/>
.	.	kIx.	.
<g/>
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
taxonomie	taxonomie	k1gFnSc1	taxonomie
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
svazu	svaz	k1gInSc2	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
ho	on	k3xPp3gMnSc4	on
nevyděluje	vydělovat	k5eNaImIp3nS	vydělovat
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
poddruh	poddruh	k1gInSc1	poddruh
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
ho	on	k3xPp3gNnSc4	on
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
k	k	k7c3	k
poddruhu	poddruh	k1gInSc3	poddruh
Panthera	Panthero	k1gNnSc2	Panthero
pardus	pardus	k1gInSc4	pardus
orientalis	orientalis	k1gFnSc2	orientalis
neboli	neboli	k8xC	neboli
k	k	k7c3	k
levhartu	levhart	k1gMnSc3	levhart
mandžuskému	mandžuský	k2eAgInSc3d1	mandžuský
<g/>
.	.	kIx.	.
</s>
<s>
Levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
se	se	k3xPyFc4	se
zbarvením	zbarvení	k1gNnSc7	zbarvení
své	svůj	k3xOyFgFnSc2	svůj
srsti	srst	k1gFnSc2	srst
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
velmi	velmi	k6eAd1	velmi
tmavou	tmavý	k2eAgFnSc4d1	tmavá
podkladovou	podkladový	k2eAgFnSc4d1	podkladová
barvu	barva	k1gFnSc4	barva
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
a	a	k8xC	a
hřbetu	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
okrová	okrový	k2eAgFnSc1d1	okrová
<g/>
,	,	kIx,	,
žlutohnědá	žlutohnědý	k2eAgFnSc1d1	žlutohnědá
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
až	až	k9	až
cihlově	cihlově	k6eAd1	cihlově
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
bývá	bývat	k5eAaImIp3nS	bývat
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
světlá	světlý	k2eAgFnSc1d1	světlá
<g/>
.	.	kIx.	.
</s>
<s>
Rozety	rozeta	k1gFnPc1	rozeta
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgFnPc1d1	velká
a	a	k8xC	a
uvnitř	uvnitř	k7c2	uvnitř
nich	on	k3xPp3gMnPc2	on
místy	místy	k6eAd1	místy
bývají	bývat	k5eAaImIp3nP	bývat
černé	černý	k2eAgFnPc4d1	černá
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
podobá	podobat	k5eAaImIp3nS	podobat
jaguárovi	jaguár	k1gMnSc3	jaguár
<g/>
.	.	kIx.	.
</s>
<s>
Rozměry	rozměra	k1gFnPc4	rozměra
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
průměrný	průměrný	k2eAgInSc4d1	průměrný
až	až	k8xS	až
mírně	mírně	k6eAd1	mírně
nadprůměrný	nadprůměrný	k2eAgInSc4d1	nadprůměrný
poddruh	poddruh	k1gInSc4	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
1,8	[number]	k4	1,8
<g/>
–	–	k?	–
<g/>
2,2	[number]	k4	2,2
metru	metro	k1gNnSc6	metro
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
osciluje	oscilovat	k5eAaImIp3nS	oscilovat
mezi	mezi	k7c4	mezi
22	[number]	k4	22
kg	kg	kA	kg
(	(	kIx(	(
<g/>
malé	malý	k2eAgFnPc1d1	malá
samice	samice	k1gFnPc1	samice
<g/>
)	)	kIx)	)
až	až	k9	až
75	[number]	k4	75
kg	kg	kA	kg
(	(	kIx(	(
<g/>
velcí	velký	k2eAgMnPc1d1	velký
samci	samec	k1gMnPc1	samec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
<g/>
,	,	kIx,	,
biotop	biotop	k1gInSc1	biotop
<g/>
,	,	kIx,	,
populace	populace	k1gFnSc1	populace
==	==	k?	==
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
území	území	k1gNnSc6	území
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ostrůvkovitě	ostrůvkovitě	k6eAd1	ostrůvkovitě
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
provinciích	provincie	k1gFnPc6	provincie
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
,	,	kIx,	,
severovýchodě	severovýchod	k1gInSc6	severovýchod
a	a	k8xC	a
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
provincie	provincie	k1gFnSc1	provincie
Šan	Šan	k1gFnSc1	Šan
<g/>
–	–	k?	–
<g/>
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
nejpříhodnějším	příhodný	k2eAgNnSc7d3	nejpříhodnější
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
výskyt	výskyt	k1gInSc4	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
přichází	přicházet	k5eAaImIp3nS	přicházet
jeho	on	k3xPp3gNnSc2	on
území	území	k1gNnSc2	území
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
územím	území	k1gNnSc7	území
levharta	levhart	k1gMnSc2	levhart
indočínského	indočínský	k2eAgMnSc2d1	indočínský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
provinciích	provincie	k1gFnPc6	provincie
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
potkával	potkávat	k5eAaImAgMnS	potkávat
s	s	k7c7	s
levhartem	levhart	k1gMnSc7	levhart
mandžuským	mandžuský	k2eAgMnSc7d1	mandžuský
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
areál	areál	k1gInSc1	areál
výskytu	výskyt	k1gInSc2	výskyt
se	se	k3xPyFc4	se
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
tato	tento	k3xDgNnPc1	tento
setkání	setkání	k1gNnPc1	setkání
zcela	zcela	k6eAd1	zcela
výjimečná	výjimečný	k2eAgNnPc1d1	výjimečné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
obývá	obývat	k5eAaImIp3nS	obývat
především	především	k9	především
vrchovinné	vrchovinný	k2eAgNnSc1d1	vrchovinný
až	až	k9	až
hornaté	hornatý	k2eAgFnPc4d1	hornatá
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
hustých	hustý	k2eAgInPc2d1	hustý
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
i	i	k9	i
ve	v	k7c6	v
stepích	step	k1gFnPc6	step
a	a	k8xC	a
polopouštích	polopoušť	k1gFnPc6	polopoušť
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
těžko	těžko	k6eAd1	těžko
kvantifikovatelná	kvantifikovatelný	k2eAgFnSc1d1	kvantifikovatelná
a	a	k8xC	a
i	i	k9	i
kvalifikované	kvalifikovaný	k2eAgInPc1d1	kvalifikovaný
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
žije	žít	k5eAaImIp3nS	žít
méně	málo	k6eAd2	málo
než	než	k8xS	než
4	[number]	k4	4
000	[number]	k4	000
levhartů	levhart	k1gMnPc2	levhart
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
poddruhů	poddruh	k1gInPc2	poddruh
(	(	kIx(	(
<g/>
l.	l.	k?	l.
čínský	čínský	k2eAgInSc1d1	čínský
<g/>
,	,	kIx,	,
l.	l.	k?	l.
indočínský	indočínský	k2eAgInSc1d1	indočínský
<g/>
,	,	kIx,	,
l.	l.	k?	l.
mandžuský	mandžuský	k2eAgInSc1d1	mandžuský
a	a	k8xC	a
l.	l.	k?	l.
indický	indický	k2eAgInSc4d1	indický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc1d2	novější
průzkumy	průzkum	k1gInPc1	průzkum
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
množství	množství	k1gNnSc1	množství
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
možná	možný	k2eAgFnSc1d1	možná
jen	jen	k9	jen
okolo	okolo	k7c2	okolo
1	[number]	k4	1
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
pak	pak	k6eAd1	pak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
kusů	kus	k1gInPc2	kus
levharta	levhart	k1gMnSc4	levhart
čínského	čínský	k2eAgMnSc4d1	čínský
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
osciluje	oscilovat	k5eAaImIp3nS	oscilovat
mezi	mezi	k7c7	mezi
174	[number]	k4	174
<g/>
-	-	kIx~	-
<g/>
348	[number]	k4	348
<g/>
,	,	kIx,	,
a	a	k8xC	a
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
proto	proto	k8xC	proto
IUCN	IUCN	kA	IUCN
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zavedla	zavést	k5eAaPmAgFnS	zavést
samostatné	samostatný	k2eAgNnSc4d1	samostatné
ohodnocení	ohodnocení	k1gNnSc4	ohodnocení
poddruhu	poddruh	k1gInSc2	poddruh
jako	jako	k8xC	jako
kriticky	kriticky	k6eAd1	kriticky
ohroženého	ohrožený	k2eAgNnSc2d1	ohrožené
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
článek	článek	k1gInSc1	článek
uveřejněný	uveřejněný	k2eAgInSc1d1	uveřejněný
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
China	China	k1gFnSc1	China
Scenic	Scenice	k1gFnPc2	Scenice
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
méně	málo	k6eAd2	málo
než	než	k8xS	než
2	[number]	k4	2
000	[number]	k4	000
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
vágní	vágní	k2eAgInSc1d1	vágní
odhad	odhad	k1gInSc1	odhad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologie	biologie	k1gFnSc2	biologie
==	==	k?	==
</s>
</p>
<p>
<s>
Levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
plachý	plachý	k2eAgInSc1d1	plachý
<g/>
,	,	kIx,	,
obtížně	obtížně	k6eAd1	obtížně
pozorovatelný	pozorovatelný	k2eAgMnSc1d1	pozorovatelný
živočich	živočich	k1gMnSc1	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
dosud	dosud	k6eAd1	dosud
nebyl	být	k5eNaImAgInS	být
detailně	detailně	k6eAd1	detailně
studován	studovat	k5eAaImNgInS	studovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
například	například	k6eAd1	například
afrických	africký	k2eAgMnPc2d1	africký
nebo	nebo	k8xC	nebo
indických	indický	k2eAgMnPc2d1	indický
levhartů	levhart	k1gMnPc2	levhart
<g/>
,	,	kIx,	,
jen	jen	k9	jen
minimum	minimum	k1gNnSc4	minimum
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
samotářské	samotářský	k2eAgNnSc4d1	samotářské
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
teritorium	teritorium	k1gNnSc1	teritorium
zabírá	zabírat	k5eAaImIp3nS	zabírat
mnoho	mnoho	k4c4	mnoho
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
<g/>
Loví	lovit	k5eAaImIp3nS	lovit
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc4d1	velký
druhy	druh	k1gInPc4	druh
kopytníků	kopytník	k1gInPc2	kopytník
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
goralové	goral	k1gMnPc1	goral
sečuánští	sečuánský	k2eAgMnPc1d1	sečuánský
<g/>
,	,	kIx,	,
muntžaci	muntžak	k1gMnPc1	muntžak
chocholatí	chocholatý	k2eAgMnPc1d1	chocholatý
<g/>
,	,	kIx,	,
divoká	divoký	k2eAgNnPc4d1	divoké
prasata	prase	k1gNnPc4	prase
<g/>
,	,	kIx,	,
srnci	srnec	k1gMnPc1	srnec
a	a	k8xC	a
jeleni	jelen	k1gMnPc1	jelen
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
různé	různý	k2eAgMnPc4d1	různý
hlodavce	hlodavec	k1gMnPc4	hlodavec
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
hlodouni	hlodoun	k1gMnPc1	hlodoun
čínští	čínský	k2eAgMnPc1d1	čínský
<g/>
,	,	kIx,	,
zajíci	zajíc	k1gMnPc1	zajíc
a	a	k8xC	a
krysy	krysa	k1gFnPc1	krysa
<g/>
.	.	kIx.	.
</s>
<s>
Nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
ani	ani	k8xC	ani
jezevci	jezevec	k1gMnPc7	jezevec
<g/>
,	,	kIx,	,
makaky	makak	k1gMnPc7	makak
<g/>
,	,	kIx,	,
cibetkami	cibetka	k1gFnPc7	cibetka
<g/>
,	,	kIx,	,
ptáky	pták	k1gMnPc4	pták
a	a	k8xC	a
v	v	k7c6	v
nouzi	nouze	k1gFnSc6	nouze
i	i	k8xC	i
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
útokům	útok	k1gInPc3	útok
často	často	k6eAd1	často
využívá	využívat	k5eAaPmIp3nS	využívat
velké	velký	k2eAgInPc4d1	velký
kameny	kámen	k1gInPc4	kámen
a	a	k8xC	a
skalní	skalní	k2eAgInPc1d1	skalní
převisy	převis	k1gInPc1	převis
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
napadá	napadat	k5eAaBmIp3nS	napadat
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
překvapivým	překvapivý	k2eAgInSc7d1	překvapivý
skokem	skok	k1gInSc7	skok
<g/>
.	.	kIx.	.
</s>
<s>
Zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
také	také	k9	také
lidmi	člověk	k1gMnPc7	člověk
chovaný	chovaný	k2eAgInSc4d1	chovaný
dobytek	dobytek	k1gInSc4	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
přirozeného	přirozený	k2eAgMnSc2d1	přirozený
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
příležitostně	příležitostně	k6eAd1	příležitostně
zabit	zabít	k5eAaPmNgMnS	zabít
tygrem	tygr	k1gMnSc7	tygr
čínským	čínský	k2eAgMnSc7d1	čínský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
už	už	k6eAd1	už
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
nežije	žít	k5eNaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
aktivita	aktivita	k1gFnSc1	aktivita
více	hodně	k6eAd2	hodně
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
po	po	k7c6	po
soumraku	soumrak	k1gInSc6	soumrak
a	a	k8xC	a
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Doba	doba	k1gFnSc1	doba
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
není	být	k5eNaImIp3nS	být
striktně	striktně	k6eAd1	striktně
daná	daný	k2eAgFnSc1d1	daná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
leden	leden	k1gInSc4	leden
a	a	k8xC	a
únor	únor	k1gInSc4	únor
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
105	[number]	k4	105
<g/>
–	–	k?	–
<g/>
110	[number]	k4	110
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
nejčastěji	často	k6eAd3	často
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
samice	samice	k1gFnPc1	samice
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
zakládají	zakládat	k5eAaImIp3nP	zakládat
teritorium	teritorium	k1gNnSc4	teritorium
poblíž	poblíž	k7c2	poblíž
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
odcházejí	odcházet	k5eAaImIp3nP	odcházet
hledat	hledat	k5eAaImF	hledat
si	se	k3xPyFc3	se
území	území	k1gNnSc4	území
podstatně	podstatně	k6eAd1	podstatně
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hrozby	hrozba	k1gFnSc2	hrozba
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
pro	pro	k7c4	pro
levharta	levhart	k1gMnSc2	levhart
čínského	čínský	k2eAgMnSc2d1	čínský
byl	být	k5eAaImAgInS	být
a	a	k8xC	a
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
poddruh	poddruh	k1gInSc1	poddruh
poměrně	poměrně	k6eAd1	poměrně
hojný	hojný	k2eAgMnSc1d1	hojný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
tzv.	tzv.	kA	tzv.
škůdců	škůdce	k1gMnPc2	škůdce
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
stavy	stav	k1gInPc1	stav
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
rapidně	rapidně	k6eAd1	rapidně
snižovat	snižovat	k5eAaImF	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
zabíjeno	zabíjet	k5eAaImNgNnS	zabíjet
2	[number]	k4	2
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
000	[number]	k4	000
levhartů	levhart	k1gMnPc2	levhart
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
muselo	muset	k5eAaImAgNnS	muset
nějak	nějak	k6eAd1	nějak
projevit	projevit	k5eAaPmF	projevit
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
pronásledování	pronásledování	k1gNnSc4	pronásledování
jako	jako	k8xC	jako
škůdců	škůdce	k1gMnPc2	škůdce
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zvířat	zvíře	k1gNnPc2	zvíře
využívaných	využívaný	k2eAgNnPc2d1	využívané
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
medicíně	medicína	k1gFnSc6	medicína
jim	on	k3xPp3gInPc3	on
škodil	škodit	k5eAaImAgInS	škodit
i	i	k8xC	i
úbytek	úbytek	k1gInSc1	úbytek
přirozené	přirozený	k2eAgFnSc2d1	přirozená
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
fragmentace	fragmentace	k1gFnSc1	fragmentace
životního	životní	k2eAgInSc2d1	životní
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
rapidní	rapidní	k2eAgInSc4d1	rapidní
úbytek	úbytek	k1gInSc4	úbytek
je	být	k5eAaImIp3nS	být
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nepotkal	potkat	k5eNaPmAgInS	potkat
osud	osud	k1gInSc1	osud
v	v	k7c6	v
divoké	divoký	k2eAgFnSc6d1	divoká
přírodě	příroda	k1gFnSc6	příroda
již	již	k6eAd1	již
vyhubených	vyhubený	k2eAgMnPc2d1	vyhubený
tygrů	tygr	k1gMnPc2	tygr
čínských	čínský	k2eAgMnPc2d1	čínský
<g/>
.	.	kIx.	.
</s>
<s>
Důvodů	důvod	k1gInPc2	důvod
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
leopard	leopard	k1gMnSc1	leopard
je	být	k5eAaImIp3nS	být
adaptabilnější	adaptabilní	k2eAgMnSc1d2	adaptabilnější
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
širší	široký	k2eAgFnSc4d2	širší
škálu	škála	k1gFnSc4	škála
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
skrývat	skrývat	k5eAaImF	skrývat
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
přece	přece	k9	přece
jen	jen	k9	jen
tak	tak	k6eAd1	tak
atraktivní	atraktivní	k2eAgMnSc1d1	atraktivní
pro	pro	k7c4	pro
tradiční	tradiční	k2eAgFnSc4d1	tradiční
čínskou	čínský	k2eAgFnSc4d1	čínská
medicínu	medicína	k1gFnSc4	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
není	být	k5eNaImIp3nS	být
důvod	důvod	k1gInSc4	důvod
k	k	k7c3	k
optimismu	optimismus	k1gInSc3	optimismus
a	a	k8xC	a
stavy	stav	k1gInPc1	stav
levhartů	levhart	k1gMnPc2	levhart
jsou	být	k5eAaImIp3nP	být
státními	státní	k2eAgInPc7d1	státní
úřady	úřad	k1gInPc7	úřad
stále	stále	k6eAd1	stále
poněkud	poněkud	k6eAd1	poněkud
nadhodnocovány	nadhodnocován	k2eAgFnPc1d1	nadhodnocována
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
chráněn	chráněn	k2eAgMnSc1d1	chráněn
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Class	Classa	k1gFnPc2	Classa
I	I	kA	I
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrozba	hrozba	k1gFnSc1	hrozba
vyhubení	vyhubení	k1gNnSc2	vyhubení
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
stále	stále	k6eAd1	stále
visí	viset	k5eAaImIp3nS	viset
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zemědělci	zemědělec	k1gMnPc1	zemědělec
se	se	k3xPyFc4	se
často	často	k6eAd1	často
mstí	mstít	k5eAaImIp3nS	mstít
<g/>
,	,	kIx,	,
když	když	k8xS	když
levharti	levhart	k1gMnPc1	levhart
zabijí	zabít	k5eAaPmIp3nP	zabít
jejich	jejich	k3xOp3gInSc4	jejich
dobytek	dobytek	k1gInSc4	dobytek
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
otrávené	otrávený	k2eAgFnPc4d1	otrávená
návnady	návnada	k1gFnPc4	návnada
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
i	i	k9	i
izolace	izolace	k1gFnSc1	izolace
malých	malý	k2eAgFnPc2d1	malá
populací	populace	k1gFnPc2	populace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
hrozí	hrozit	k5eAaImIp3nS	hrozit
příbuzenským	příbuzenský	k2eAgNnSc7d1	příbuzenské
křížením	křížení	k1gNnSc7	křížení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
<g/>
,	,	kIx,	,
chov	chov	k1gInSc1	chov
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
==	==	k?	==
</s>
</p>
<p>
<s>
Chinese	Chinést	k5eAaPmIp3nS	Chinést
Felid	Felid	k1gInSc1	Felid
Conservation	Conservation	k1gInSc1	Conservation
Alliance	Alliance	k1gFnSc1	Alliance
(	(	kIx(	(
<g/>
CFCA	CFCA	kA	CFCA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malá	malá	k1gFnSc1	malá
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
měla	mít	k5eAaImAgFnS	mít
12	[number]	k4	12
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
monitorováním	monitorování	k1gNnSc7	monitorování
a	a	k8xC	a
ochranou	ochrana	k1gFnSc7	ochrana
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k9	pak
levharta	levhart	k1gMnSc4	levhart
čínského	čínský	k2eAgMnSc4d1	čínský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
rozmisťováním	rozmisťování	k1gNnSc7	rozmisťování
fotopastí	fotopast	k1gFnPc2	fotopast
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Šan-si	Šane	k1gFnSc6	Šan-se
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
jimi	on	k3xPp3gFnPc7	on
pokryla	pokrýt	k5eAaPmAgFnS	pokrýt
území	území	k1gNnSc4	území
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgInPc2d1	jiný
záběrů	záběr	k1gInPc2	záběr
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
zachytit	zachytit	k5eAaPmF	zachytit
samice	samice	k1gFnSc2	samice
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
různými	různý	k2eAgInPc7d1	různý
vrhy	vrh	k1gInPc7	vrh
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
organizace	organizace	k1gFnSc2	organizace
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
vesnice	vesnice	k1gFnSc1	vesnice
a	a	k8xC	a
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
informace	informace	k1gFnPc4	informace
od	od	k7c2	od
místních	místní	k2eAgMnPc2d1	místní
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
šířit	šířit	k5eAaImF	šířit
osvětu	osvěta	k1gFnSc4	osvěta
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
i	i	k8xC	i
kompenzační	kompenzační	k2eAgInSc1d1	kompenzační
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
zemědělci	zemědělec	k1gMnPc1	zemědělec
odškodňováni	odškodňován	k2eAgMnPc1d1	odškodňován
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
když	když	k8xS	když
jejich	jejich	k3xOp3gInSc4	jejich
dobytek	dobytek	k1gInSc4	dobytek
je	být	k5eAaImIp3nS	být
usmrcen	usmrtit	k5eAaPmNgMnS	usmrtit
některým	některý	k3yIgMnSc7	některý
levhartem	levhart	k1gMnSc7	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
800	[number]	k4	800
až	až	k9	až
3000	[number]	k4	3000
jüanů	jüan	k1gInPc2	jüan
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
S	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-čchuan	-čchuan	k1gMnSc1	-čchuan
vyšetřeno	vyšetřen	k2eAgNnSc4d1	vyšetřeno
a	a	k8xC	a
odškodněno	odškodněn	k2eAgNnSc4d1	odškodněn
zabití	zabití	k1gNnSc4	zabití
40	[number]	k4	40
kusů	kus	k1gInPc2	kus
dobytka	dobytek	k1gInSc2	dobytek
levharty	levhart	k1gMnPc7	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Kompenzace	kompenzace	k1gFnPc1	kompenzace
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
dlouhodobým	dlouhodobý	k2eAgNnPc3d1	dlouhodobé
řešením	řešení	k1gNnPc3	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
CFCA	CFCA	kA	CFCA
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
místní	místní	k2eAgInPc4d1	místní
úřady	úřad	k1gInPc4	úřad
a	a	k8xC	a
zemědělce	zemědělec	k1gMnPc4	zemědělec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nenechávali	nechávat	k5eNaImAgMnP	nechávat
dobytek	dobytek	k1gInSc4	dobytek
volně	volně	k6eAd1	volně
pást	pást	k5eAaImF	pást
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
ohrazovali	ohrazovat	k5eAaImAgMnP	ohrazovat
mu	on	k3xPp3gMnSc3	on
pastviny	pastvina	k1gFnPc4	pastvina
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
různých	různý	k2eAgMnPc2d1	různý
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
co	co	k3yInSc4	co
nejčastěji	často	k6eAd3	často
patrolovat	patrolovat	k5eAaImF	patrolovat
a	a	k8xC	a
odhalovat	odhalovat	k5eAaImF	odhalovat
či	či	k8xC	či
alespoň	alespoň	k9	alespoň
odrazovat	odrazovat	k5eAaImF	odrazovat
pytláky	pytlák	k1gMnPc4	pytlák
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
je	být	k5eAaImIp3nS	být
chováno	chovat	k5eAaImNgNnS	chovat
asi	asi	k9	asi
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
těchto	tento	k3xDgFnPc2	tento
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Liberec	Liberec	k1gInSc1	Liberec
<g/>
;	;	kIx,	;
v	v	k7c6	v
Zoo	zoo	k1gNnSc6	zoo
Plzeň	Plzeň	k1gFnSc1	Plzeň
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc4	jeden
kus	kus	k1gInSc4	kus
chován	chován	k2eAgInSc4d1	chován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
ho	on	k3xPp3gMnSc4	on
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
华	华	k?	华
na	na	k7c6	na
čínské	čínský	k2eAgFnSc6d1	čínská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DOBRORUKA	DOBRORUKA	kA	DOBRORUKA
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
J.	J.	kA	J.
Über	Über	k1gMnSc1	Über
nordchinesische	nordchinesisch	k1gInSc2	nordchinesisch
Leoparden	Leopardna	k1gFnPc2	Leopardna
<g/>
,	,	kIx,	,
besonders	besonders	k6eAd1	besonders
über	über	k1gInSc1	über
den	den	k1gInSc1	den
Namen	Namna	k1gFnPc2	Namna
Panthera	Panthero	k1gNnSc2	Panthero
pardus	pardus	k1gInSc4	pardus
japonensis	japonensis	k1gFnSc2	japonensis
(	(	kIx(	(
<g/>
Gray	Graa	k1gFnSc2	Graa
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zoologischer	Zoologischra	k1gFnPc2	Zoologischra
Anzeiger	Anzeigra	k1gFnPc2	Anzeigra
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
170	[number]	k4	170
<g/>
,	,	kIx,	,
s.	s.	k?	s.
164	[number]	k4	164
<g/>
-	-	kIx~	-
<g/>
171	[number]	k4	171
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GRAY	GRAY	kA	GRAY
<g/>
,	,	kIx,	,
J.	J.	kA	J.
E.	E.	kA	E.
Description	Description	k1gInSc1	Description
of	of	k?	of
some	somat	k5eAaPmIp3nS	somat
new	new	k?	new
species	species	k1gFnSc1	species
of	of	k?	of
Mammalia	Mammalia	k1gFnSc1	Mammalia
<g/>
.	.	kIx.	.
</s>
<s>
Proceedings	Proceedings	k1gInSc1	Proceedings
of	of	k?	of
the	the	k?	the
Scientific	Scientific	k1gMnSc1	Scientific
Meetings	Meetingsa	k1gFnPc2	Meetingsa
of	of	k?	of
the	the	k?	the
Zoological	Zoological	k1gMnSc1	Zoological
Society	societa	k1gFnSc2	societa
of	of	k?	of
London	London	k1gMnSc1	London
for	forum	k1gNnPc2	forum
the	the	k?	the
Year	Year	k1gMnSc1	Year
1862	[number]	k4	1862
<g/>
.	.	kIx.	.
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
s.	s.	k?	s.
262	[number]	k4	262
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JUTZELER	JUTZELER	kA	JUTZELER
Eva	Eva	k1gFnSc1	Eva
<g/>
,	,	kIx,	,
WU	WU	kA	WU
ZHIGANG	ZHIGANG	kA	ZHIGANG
<g/>
,	,	kIx,	,
LIU	LIU	kA	LIU
WEISHI	WEISHI	kA	WEISHI
and	and	k?	and
BREITENMOSER	BREITENMOSER	kA	BREITENMOSER
Urs	Urs	k1gFnSc2	Urs
<g/>
.	.	kIx.	.
</s>
<s>
Leopard	leopard	k1gMnSc1	leopard
Panthera	Panthero	k1gNnSc2	Panthero
pardus	pardus	k1gInSc1	pardus
<g/>
.	.	kIx.	.
</s>
<s>
Cat	Cat	k?	Cat
News	News	k1gInSc1	News
Spacial	Spacial	k1gInSc1	Spacial
Issue	Issue	k1gFnSc1	Issue
5	[number]	k4	5
<g/>
:	:	kIx,	:
Cats	Cats	k1gInSc1	Cats
in	in	k?	in
China	China	k1gFnSc1	China
<g/>
.	.	kIx.	.
</s>
<s>
Autumn	Autumn	k1gInSc1	Autumn
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
s.	s.	k?	s.
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LU	LU	kA	LU
JUN	jun	k1gMnSc1	jun
<g/>
,	,	kIx,	,
HU	hu	k0	hu
DEFU	DEFU	kA	DEFU
and	and	k?	and
YANG	YANG	kA	YANG
LIANGLIANG	LIANGLIANG	kA	LIANGLIANG
<g/>
.	.	kIx.	.
</s>
<s>
Legal	Legal	k1gInSc1	Legal
status	status	k1gInSc1	status
and	and	k?	and
conservation	conservation	k1gInSc1	conservation
of	of	k?	of
cat	cat	k?	cat
species	species	k1gFnSc2	species
in	in	k?	in
China	China	k1gFnSc1	China
<g/>
.	.	kIx.	.
</s>
<s>
Cat	Cat	k?	Cat
News	News	k1gInSc1	News
Spacial	Spacial	k1gInSc1	Spacial
Issue	Issue	k1gFnSc1	Issue
5	[number]	k4	5
<g/>
:	:	kIx,	:
Cats	Cats	k1gInSc1	Cats
in	in	k?	in
China	China	k1gFnSc1	China
<g/>
.	.	kIx.	.
</s>
<s>
Autumn	Autumn	k1gInSc1	Autumn
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
s.	s.	k?	s.
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SUNQUIST	SUNQUIST	kA	SUNQUIST
<g/>
,	,	kIx,	,
Mel	mlít	k5eAaImRp2nS	mlít
<g/>
;	;	kIx,	;
SUNQUIST	SUNQUIST	kA	SUNQUIST
<g/>
,	,	kIx,	,
Fiona	Fiono	k1gNnSc2	Fiono
<g/>
.	.	kIx.	.
</s>
<s>
Wild	Wild	k6eAd1	Wild
Cats	Cats	k1gInSc1	Cats
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Chicago	Chicago	k1gNnSc1	Chicago
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
318	[number]	k4	318
<g/>
-	-	kIx~	-
<g/>
342	[number]	k4	342
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Panthera	Panthero	k1gNnSc2	Panthero
pardus	pardus	k1gInSc1	pardus
japonensis	japonensis	k1gFnSc4	japonensis
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
ZOO	zoo	k1gFnSc1	zoo
Liberec	Liberec	k1gInSc1	Liberec
<g/>
:	:	kIx,	:
Levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
</s>
</p>
<p>
<s>
ZOO	zoo	k1gFnSc1	zoo
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Levhart	levhart	k1gMnSc1	levhart
čínský	čínský	k2eAgMnSc1d1	čínský
</s>
</p>
<p>
<s>
EAZA	EAZA	kA	EAZA
Yearbook	Yearbook	k1gInSc4	Yearbook
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
The	The	k?	The
Cat	Cat	k1gMnPc1	Cat
House	house	k1gNnSc1	house
<g/>
:	:	kIx,	:
North	North	k1gMnSc1	North
Chinese	Chinese	k1gFnSc2	Chinese
Leopard	leopard	k1gMnSc1	leopard
</s>
</p>
<p>
<s>
Vzácné	vzácný	k2eAgInPc1d1	vzácný
záběry	záběr	k1gInPc1	záběr
z	z	k7c2	z
kamerových	kamerový	k2eAgFnPc2d1	kamerová
pastí	past	k1gFnPc2	past
z	z	k7c2	z
provincie	provincie	k1gFnSc2	provincie
Šan-si	Šane	k1gFnSc4	Šan-se
(	(	kIx(	(
<g/>
video	video	k1gNnSc4	video
<g/>
)	)	kIx)	)
</s>
</p>
