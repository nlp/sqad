<s>
Pyramida	pyramida	k1gFnSc1	pyramida
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Pyramide	pyramid	k1gInSc5	pyramid
du	du	k?	du
Louvre	Louvre	k1gInSc1	Louvre
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prosklená	prosklený	k2eAgFnSc1d1	prosklená
pyramida	pyramida	k1gFnSc1	pyramida
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádvoří	nádvoří	k1gNnSc6	nádvoří
Paláce	palác	k1gInSc2	palác
Louvre	Louvre	k1gInSc1	Louvre
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
Louvre	Louvre	k1gInSc1	Louvre
<g/>
.	.	kIx.	.
</s>
