<s>
Pyramida	pyramida	k1gFnSc1	pyramida
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Pyramide	pyramid	k1gInSc5	pyramid
du	du	k?	du
Louvre	Louvre	k1gInSc1	Louvre
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prosklená	prosklený	k2eAgFnSc1d1	prosklená
pyramida	pyramida	k1gFnSc1	pyramida
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádvoří	nádvoří	k1gNnSc6	nádvoří
Paláce	palác	k1gInSc2	palác
Louvre	Louvre	k1gInSc1	Louvre
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
Louvre	Louvre	k1gInSc1	Louvre
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
stavby	stavba	k1gFnSc2	stavba
dokončené	dokončený	k2eAgFnPc1d1	dokončená
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
je	být	k5eAaImIp3nS	být
architekt	architekt	k1gMnSc1	architekt
Ieoh	Ieoh	k1gMnSc1	Ieoh
Ming	Ming	k1gMnSc1	Ming
Pei	Pei	k1gMnSc1	Pei
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ji	on	k3xPp3gFnSc4	on
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
francouzského	francouzský	k2eAgMnSc2d1	francouzský
prezidenta	prezident	k1gMnSc2	prezident
Mitterranda	Mitterrando	k1gNnSc2	Mitterrando
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
otevření	otevření	k1gNnSc1	otevření
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
200	[number]	k4	200
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Pyramida	pyramida	k1gFnSc1	pyramida
má	mít	k5eAaImIp3nS	mít
čtvercovou	čtvercový	k2eAgFnSc4d1	čtvercová
základnu	základna	k1gFnSc4	základna
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
hrany	hrana	k1gFnSc2	hrana
35	[number]	k4	35
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
výšku	výška	k1gFnSc4	výška
20,6	[number]	k4	20,6
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
603	[number]	k4	603
kosočtvercových	kosočtvercový	k2eAgMnPc2d1	kosočtvercový
a	a	k8xC	a
70	[number]	k4	70
trojúhelníkových	trojúhelníkový	k2eAgInPc2d1	trojúhelníkový
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Pyramida	pyramida	k1gFnSc1	pyramida
jakož	jakož	k8xC	jakož
i	i	k9	i
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
podzemní	podzemní	k2eAgFnSc1d1	podzemní
vstupní	vstupní	k2eAgFnSc1d1	vstupní
hala	hala	k1gFnSc1	hala
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgInSc1d1	původní
hlavní	hlavní	k2eAgInSc1d1	hlavní
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
bočním	boční	k2eAgNnSc6d1	boční
křídle	křídlo	k1gNnSc6	křídlo
již	již	k6eAd1	již
kapacitně	kapacitně	k6eAd1	kapacitně
nedostačoval	dostačovat	k5eNaImAgMnS	dostačovat
množství	množství	k1gNnSc3	množství
každodenních	každodenní	k2eAgMnPc2d1	každodenní
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Pyramida	pyramida	k1gFnSc1	pyramida
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
přirozené	přirozený	k2eAgNnSc1d1	přirozené
osvětlení	osvětlení	k1gNnSc1	osvětlení
centrálního	centrální	k2eAgInSc2d1	centrální
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
,	,	kIx,	,
informační	informační	k2eAgFnSc2d1	informační
přepážky	přepážka	k1gFnSc2	přepážka
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
vstupy	vstup	k1gInPc1	vstup
do	do	k7c2	do
křídel	křídlo	k1gNnPc2	křídlo
paláce	palác	k1gInSc2	palác
se	s	k7c7	s
samotnými	samotný	k2eAgInPc7d1	samotný
výstavními	výstavní	k2eAgInPc7d1	výstavní
prostory	prostor	k1gInPc7	prostor
a	a	k8xC	a
průchod	průchod	k1gInSc1	průchod
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
podzemních	podzemní	k2eAgFnPc2d1	podzemní
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
obchody	obchod	k1gInPc4	obchod
<g/>
,	,	kIx,	,
stanice	stanice	k1gFnPc4	stanice
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
podzemní	podzemní	k2eAgNnSc4d1	podzemní
parkoviště	parkoviště	k1gNnSc4	parkoviště
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
koncept	koncept	k1gInSc4	koncept
využilo	využít	k5eAaPmAgNnS	využít
později	pozdě	k6eAd2	pozdě
i	i	k9	i
několik	několik	k4yIc4	několik
jiných	jiný	k2eAgNnPc2d1	jiné
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Muzeum	muzeum	k1gNnSc4	muzeum
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
pyramidy	pyramida	k1gFnSc2	pyramida
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
částečným	částečný	k2eAgInSc7d1	částečný
odporem	odpor	k1gInSc7	odpor
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
futuristický	futuristický	k2eAgInSc4d1	futuristický
vzhled	vzhled	k1gInSc4	vzhled
kontrastující	kontrastující	k2eAgFnSc2d1	kontrastující
s	s	k7c7	s
architekturou	architektura	k1gFnSc7	architektura
Paláce	palác	k1gInSc2	palác
Louvre	Louvre	k1gInSc1	Louvre
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
rozměry	rozměr	k1gInPc1	rozměr
pyramidy	pyramida	k1gFnSc2	pyramida
nepřesahují	přesahovat	k5eNaImIp3nP	přesahovat
historickou	historický	k2eAgFnSc4d1	historická
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
nijak	nijak	k6eAd1	nijak
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
celistvosti	celistvost	k1gFnSc2	celistvost
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
stavby	stavba	k1gFnSc2	stavba
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
městská	městský	k2eAgFnSc1d1	městská
legenda	legenda	k1gFnSc1	legenda
<g/>
,	,	kIx,	,
že	že	k8xS	že
pyramida	pyramida	k1gFnSc1	pyramida
je	být	k5eAaImIp3nS	být
projektována	projektovat	k5eAaBmNgFnS	projektovat
ze	z	k7c2	z
666	[number]	k4	666
skleněných	skleněný	k2eAgFnPc2d1	skleněná
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
stavby	stavba	k1gFnSc2	stavba
správa	správa	k1gFnSc1	správa
muzea	muzeum	k1gNnSc2	muzeum
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pyramida	pyramida	k1gFnSc1	pyramida
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
673	[number]	k4	673
tabulek	tabulka	k1gFnPc2	tabulka
(	(	kIx(	(
<g/>
603	[number]	k4	603
kosočtverců	kosočtverec	k1gInPc2	kosočtverec
a	a	k8xC	a
70	[number]	k4	70
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
znovu	znovu	k6eAd1	znovu
oživil	oživit	k5eAaPmAgInS	oživit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
spisovatel	spisovatel	k1gMnSc1	spisovatel
Dan	Dan	k1gMnSc1	Dan
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
když	když	k8xS	když
tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
uvedl	uvést	k5eAaPmAgMnS	uvést
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Šifra	šifra	k1gFnSc1	šifra
mistra	mistr	k1gMnSc2	mistr
Leonarda	Leonardo	k1gMnSc2	Leonardo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Palais	Palais	k1gFnSc3	Palais
du	du	k?	du
Louvre	Louvre	k1gInSc1	Louvre
Louvre	Louvre	k1gInSc1	Louvre
Obrácená	obrácený	k2eAgFnSc1d1	obrácená
pyramida	pyramida	k1gFnSc1	pyramida
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Pyramide	pyramid	k1gInSc5	pyramid
du	du	k?	du
Louvre	Louvre	k1gInSc4	Louvre
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pyramida	pyramida	k1gFnSc1	pyramida
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fotografie	fotografia	k1gFnPc1	fotografia
pyramidy	pyramida	k1gFnPc1	pyramida
</s>
