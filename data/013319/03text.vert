<p>
<s>
Zoofobie	Zoofobie	k1gFnSc1	Zoofobie
je	být	k5eAaImIp3nS	být
panický	panický	k2eAgInSc4d1	panický
strach	strach	k1gInSc4	strach
ze	z	k7c2	z
zvířat	zvíře	k1gNnPc2	zvíře
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejčastější	častý	k2eAgInSc4d3	nejčastější
druh	druh	k1gInSc4	druh
fobie	fobie	k1gFnSc2	fobie
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
než	než	k8xS	než
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
postiženého	postižený	k1gMnSc2	postižený
vede	vést	k5eAaImIp3nS	vést
strach	strach	k1gInSc1	strach
ze	z	k7c2	z
zvířete	zvíře	k1gNnSc2	zvíře
až	až	k9	až
k	k	k7c3	k
mohutnému	mohutný	k2eAgInSc3d1	mohutný
stresu	stres	k1gInSc3	stres
a	a	k8xC	a
k	k	k7c3	k
celkově	celkově	k6eAd1	celkově
vyhýbavému	vyhýbavý	k2eAgNnSc3d1	vyhýbavé
chování	chování	k1gNnSc3	chování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
normální	normální	k2eAgMnSc1d1	normální
se	se	k3xPyFc4	se
bát	bát	k1gInSc1	bát
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
agresivního	agresivní	k2eAgMnSc4d1	agresivní
psa	pes	k1gMnSc4	pes
či	či	k8xC	či
se	se	k3xPyFc4	se
štítit	štítit	k5eAaImF	štítit
pavouka	pavouk	k1gMnSc4	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zoofobie	zoofobie	k1gFnSc2	zoofobie
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
tento	tento	k3xDgInSc1	tento
strach	strach	k1gInSc1	strach
iracionální	iracionální	k2eAgInSc1d1	iracionální
-	-	kIx~	-
kynofobik	kynofobik	k1gInSc1	kynofobik
se	se	k3xPyFc4	se
psa	pes	k1gMnSc4	pes
bojí	bát	k5eAaImIp3nP	bát
ať	ať	k8xC	ať
je	být	k5eAaImIp3nS	být
pes	pes	k1gMnSc1	pes
klidný	klidný	k2eAgMnSc1d1	klidný
nebo	nebo	k8xC	nebo
agresivní	agresivní	k2eAgMnSc1d1	agresivní
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
má	mít	k5eAaImIp3nS	mít
náhubek	náhubek	k1gInSc4	náhubek
a	a	k8xC	a
vodítko	vodítko	k1gNnSc4	vodítko
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Arachnofobik	Arachnofobik	k1gInSc1	Arachnofobik
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
pavouka	pavouk	k1gMnSc4	pavouk
i	i	k9	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
každého	každý	k3xTgInSc2	každý
druhu	druh	k1gInSc2	druh
zoofobie	zoofobie	k1gFnSc2	zoofobie
může	moct	k5eAaImIp3nS	moct
postižený	postižený	k2eAgMnSc1d1	postižený
panikařit	panikařit	k5eAaImF	panikařit
už	už	k6eAd1	už
jen	jen	k9	jen
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
fotografii	fotografia	k1gFnSc4	fotografia
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
nesmyslnost	nesmyslnost	k1gFnSc4	nesmyslnost
těchto	tento	k3xDgInPc2	tento
pocitů	pocit	k1gInPc2	pocit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
to	ten	k3xDgNnSc4	ten
ovládnout	ovládnout	k5eAaPmF	ovládnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnPc4	příčina
==	==	k?	==
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgFnPc1d1	různá
příčiny	příčina	k1gFnPc1	příčina
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
vrozený	vrozený	k2eAgInSc4d1	vrozený
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
strach	strach	k1gInSc1	strach
způsobený	způsobený	k2eAgInSc1d1	způsobený
nepříjemným	příjemný	k2eNgInSc7d1	nepříjemný
zážitkem	zážitek	k1gInSc7	zážitek
<g/>
,	,	kIx,	,
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
byl	být	k5eAaImAgMnS	být
svědkem	svědek	k1gMnSc7	svědek
napadení	napadení	k1gNnSc2	napadení
nebo	nebo	k8xC	nebo
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
slyšel	slyšet	k5eAaImAgMnS	slyšet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příznaky	příznak	k1gInPc4	příznak
==	==	k?	==
</s>
</p>
<p>
<s>
Záchvat	záchvat	k1gInSc1	záchvat
je	být	k5eAaImIp3nS	být
vyvolán	vyvolat	k5eAaPmNgInS	vyvolat
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
i	i	k9	i
když	když	k8xS	když
zoofobik	zoofobik	k1gMnSc1	zoofobik
vidí	vidět	k5eAaImIp3nS	vidět
zvíře	zvíře	k1gNnSc4	zvíře
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
na	na	k7c6	na
fotografii	fotografia	k1gFnSc6	fotografia
<g/>
,	,	kIx,	,
či	či	k8xC	či
při	při	k7c6	při
pomyšlení	pomyšlení	k1gNnSc6	pomyšlení
na	na	k7c4	na
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
záchvatu	záchvat	k1gInSc6	záchvat
fobie	fobie	k1gFnSc2	fobie
se	se	k3xPyFc4	se
u	u	k7c2	u
dotyčného	dotyčný	k2eAgNnSc2d1	dotyčné
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tyto	tento	k3xDgInPc4	tento
příznaky	příznak	k1gInPc4	příznak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nervozita	nervozita	k1gFnSc1	nervozita
</s>
</p>
<p>
<s>
panika	panika	k1gFnSc1	panika
<g/>
,	,	kIx,	,
záchvat	záchvat	k1gInSc1	záchvat
úzkosti	úzkost	k1gFnSc2	úzkost
</s>
</p>
<p>
<s>
strach	strach	k1gInSc1	strach
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
</s>
</p>
<p>
<s>
touha	touha	k1gFnSc1	touha
uniknout	uniknout	k5eAaPmF	uniknout
</s>
</p>
<p>
<s>
dušnost	dušnost	k1gFnSc1	dušnost
<g/>
,	,	kIx,	,
potíže	potíž	k1gFnPc1	potíž
s	s	k7c7	s
dýcháním	dýchání	k1gNnSc7	dýchání
</s>
</p>
<p>
<s>
sucho	sucho	k1gNnSc1	sucho
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
</s>
</p>
<p>
<s>
zrychlený	zrychlený	k2eAgInSc1d1	zrychlený
tep	tep	k1gInSc1	tep
</s>
</p>
<p>
<s>
pocení	pocení	k1gNnSc1	pocení
</s>
</p>
<p>
<s>
pocit	pocit	k1gInSc4	pocit
na	na	k7c4	na
zvracení	zvracení	k1gNnSc4	zvracení
</s>
</p>
<p>
<s>
třes	třes	k1gInSc1	třes
</s>
</p>
<p>
<s>
ztuhnutí	ztuhnutí	k1gNnSc4	ztuhnutí
<g/>
,	,	kIx,	,
neschopnost	neschopnost	k1gFnSc4	neschopnost
mluvit	mluvit	k5eAaImF	mluvit
</s>
</p>
<p>
<s>
==	==	k?	==
Fobie	fobie	k1gFnPc4	fobie
z	z	k7c2	z
určitých	určitý	k2eAgNnPc2d1	určité
zvířat	zvíře	k1gNnPc2	zvíře
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgFnPc4d1	související
stránky	stránka	k1gFnPc4	stránka
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
fobií	fobie	k1gFnPc2	fobie
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
https://web.archive.org/web/20090809102606/http://constricted.pise.cz/94408-druhy-strachu-nebo-vedecky-fobie.html	[url]	k1gMnSc1	https://web.archive.org/web/20090809102606/http://constricted.pise.cz/94408-druhy-strachu-nebo-vedecky-fobie.html
</s>
</p>
<p>
<s>
http://jung.sneznik.cz/forum/read.php?3,19089	[url]	k4	http://jung.sneznik.cz/forum/read.php?3,19089
</s>
</p>
<p>
<s>
http://ivet-k.blog.cz/0703/druhy-fobie	[url]	k1gFnSc1	http://ivet-k.blog.cz/0703/druhy-fobie
</s>
</p>
<p>
<s>
http://naposledy.blog.cz/0901/druhy-fobii	[url]	k1gFnSc4	http://naposledy.blog.cz/0901/druhy-fobii
</s>
</p>
<p>
<s>
http://wisegeek.com/how-common-is-a-fear-of-animals.htm	[url]	k6eAd1	http://wisegeek.com/how-common-is-a-fear-of-animals.htm
</s>
</p>
