<s>
Zoofobie	Zoofobie	k1gFnSc1	Zoofobie
je	být	k5eAaImIp3nS	být
panický	panický	k2eAgInSc4d1	panický
strach	strach	k1gInSc4	strach
ze	z	k7c2	z
zvířat	zvíře	k1gNnPc2	zvíře
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejčastější	častý	k2eAgInSc4d3	nejčastější
druh	druh	k1gInSc4	druh
fobie	fobie	k1gFnSc2	fobie
<g/>
.	.	kIx.	.
</s>
