<s>
Počítač	počítač	k1gInSc1	počítač
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
výpočetní	výpočetní	k2eAgFnSc1d1	výpočetní
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
data	datum	k1gNnPc4	datum
pomocí	pomocí	k7c2	pomocí
předem	předem	k6eAd1	předem
vytvořeného	vytvořený	k2eAgInSc2d1	vytvořený
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
počítač	počítač	k1gInSc1	počítač
je	být	k5eAaImIp3nS	být
elektronický	elektronický	k2eAgInSc1d1	elektronický
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
hardwaru	hardware	k1gInSc2	hardware
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
fyzické	fyzický	k2eAgFnPc4d1	fyzická
části	část	k1gFnPc4	část
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
<g/>
,	,	kIx,	,
klávesnice	klávesnice	k1gFnSc1	klávesnice
<g/>
,	,	kIx,	,
monitor	monitor	k1gInSc1	monitor
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
ze	z	k7c2	z
softwaru	software	k1gInSc2	software
(	(	kIx(	(
<g/>
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
a	a	k8xC	a
programy	program	k1gInPc1	program
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gInSc1	počítač
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
ovládán	ovládat	k5eAaImNgInS	ovládat
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
počítači	počítač	k1gInSc3	počítač
data	datum	k1gNnPc4	datum
ke	k	k7c3	k
zpracování	zpracování	k1gNnSc3	zpracování
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jeho	jeho	k3xOp3gNnPc2	jeho
vstupních	vstupní	k2eAgNnPc2d1	vstupní
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
počítač	počítač	k1gInSc4	počítač
výsledky	výsledek	k1gInPc7	výsledek
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
pomocí	pomocí	k7c2	pomocí
výstupních	výstupní	k2eAgNnPc2d1	výstupní
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
počítače	počítač	k1gInPc1	počítač
využívány	využívat	k5eAaPmNgInP	využívat
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
oborech	obor	k1gInPc6	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
první	první	k4xOgMnSc1	první
počítač	počítač	k1gMnSc1	počítač
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
měnil	měnit	k5eAaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
slovo	slovo	k1gNnSc1	slovo
počítač	počítač	k1gInSc1	počítač
označovalo	označovat	k5eAaImAgNnS	označovat
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prováděl	provádět	k5eAaImAgMnS	provádět
výpočty	výpočet	k1gInPc7	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
významu	význam	k1gInSc2	význam
slova	slovo	k1gNnSc2	slovo
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
označení	označení	k1gNnSc4	označení
hardwaru	hardware	k1gInSc2	hardware
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
projektu	projekt	k1gInSc2	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
(	(	kIx(	(
<g/>
vývoj	vývoj	k1gInSc1	vývoj
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
předchůdce	předchůdce	k1gMnSc4	předchůdce
počítače	počítač	k1gMnSc4	počítač
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
mechanické	mechanický	k2eAgNnSc4d1	mechanické
počitadlo	počitadlo	k1gNnSc4	počitadlo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
abakus	abakus	k1gInSc1	abakus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
již	již	k6eAd1	již
v	v	k7c6	v
Babylonii	Babylonie	k1gFnSc6	Babylonie
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
třetího	třetí	k4xOgNnSc2	třetí
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vynálezce	vynálezce	k1gMnPc4	vynálezce
dnešních	dnešní	k2eAgInPc2d1	dnešní
počítačů	počítač	k1gInPc2	počítač
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Charles	Charles	k1gMnSc1	Charles
Babbage	Babbag	k1gInSc2	Babbag
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
základní	základní	k2eAgInPc4d1	základní
principy	princip	k1gInPc4	princip
fungování	fungování	k1gNnSc2	fungování
mechanického	mechanický	k2eAgInSc2d1	mechanický
stroje	stroj	k1gInSc2	stroj
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
složitých	složitý	k2eAgInPc2d1	složitý
výpočtů	výpočet	k1gInPc2	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
prvního	první	k4xOgMnSc2	první
přímého	přímý	k2eAgMnSc2d1	přímý
předchůdce	předchůdce	k1gMnSc2	předchůdce
současných	současný	k2eAgMnPc2d1	současný
elektronických	elektronický	k2eAgMnPc2d1	elektronický
počítačů	počítač	k1gMnPc2	počítač
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
elektronkový	elektronkový	k2eAgInSc4d1	elektronkový
ENIAC	ENIAC	kA	ENIAC
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
počítač	počítač	k1gInSc1	počítač
si	se	k3xPyFc3	se
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
představí	představit	k5eAaPmIp3nS	představit
buď	buď	k8xC	buď
notebook	notebook	k1gInSc4	notebook
nebo	nebo	k8xC	nebo
PC	PC	kA	PC
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
osobní	osobní	k2eAgInSc1d1	osobní
počítač	počítač	k1gInSc1	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
daleko	daleko	k6eAd1	daleko
širší	široký	k2eAgInSc1d2	širší
<g/>
.	.	kIx.	.
</s>
<s>
Počítače	počítač	k1gInPc4	počítač
řídí	řídit	k5eAaImIp3nS	řídit
činnosti	činnost	k1gFnSc3	činnost
nejrůznějších	různý	k2eAgNnPc2d3	nejrůznější
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
všude	všude	k6eAd1	všude
kolem	kolem	k7c2	kolem
nás	my	k3xPp1nPc2	my
–	–	k?	–
v	v	k7c6	v
automobilech	automobil	k1gInPc6	automobil
<g/>
,	,	kIx,	,
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
<g/>
,	,	kIx,	,
automatických	automatický	k2eAgFnPc6d1	automatická
pračkách	pračka	k1gFnPc6	pračka
<g/>
,	,	kIx,	,
mikrovlnných	mikrovlnný	k2eAgFnPc6d1	mikrovlnná
troubách	trouba	k1gFnPc6	trouba
<g/>
,	,	kIx,	,
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
robotech	robot	k1gInPc6	robot
<g/>
,	,	kIx,	,
letadlech	letadlo	k1gNnPc6	letadlo
<g/>
,	,	kIx,	,
digitálních	digitální	k2eAgInPc6d1	digitální
fotoaparátech	fotoaparát	k1gInPc6	fotoaparát
<g/>
,	,	kIx,	,
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
přehrávačích	přehrávač	k1gInPc6	přehrávač
<g/>
,	,	kIx,	,
záchodových	záchodový	k2eAgNnPc6d1	záchodové
splachovadlech	splachovadlo	k1gNnPc6	splachovadlo
<g/>
,	,	kIx,	,
klikách	klika	k1gFnPc6	klika
<g />
.	.	kIx.	.
</s>
<s>
od	od	k7c2	od
dveří	dveře	k1gFnPc2	dveře
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
zámcích	zámek	k1gInPc6	zámek
na	na	k7c4	na
karty	karta	k1gFnPc4	karta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
dětských	dětský	k2eAgFnPc6d1	dětská
hračkách	hračka	k1gFnPc6	hračka
<g/>
,	,	kIx,	,
...	...	k?	...
Princip	princip	k1gInSc1	princip
činnosti	činnost	k1gFnSc2	činnost
počítače	počítač	k1gInSc2	počítač
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dvojí	dvojí	k4xRgFnSc4	dvojí
<g/>
:	:	kIx,	:
analogový	analogový	k2eAgInSc1d1	analogový
počítač	počítač	k1gInSc1	počítač
–	–	k?	–
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
analogovém	analogový	k2eAgInSc6d1	analogový
principu	princip	k1gInSc6	princip
(	(	kIx(	(
<g/>
data	datum	k1gNnSc2	datum
jsou	být	k5eAaImIp3nP	být
reprezentovana	reprezentovan	k1gMnSc2	reprezentovan
např	např	kA	např
velikostí	velikost	k1gFnSc7	velikost
proudu	proud	k1gInSc2	proud
<g/>
)	)	kIx)	)
číslicový	číslicový	k2eAgInSc1d1	číslicový
počítač	počítač	k1gInSc1	počítač
–	–	k?	–
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
digitální	digitální	k2eAgInSc1d1	digitální
data	datum	k1gNnSc2	datum
Analogové	analogový	k2eAgInPc1d1	analogový
počítače	počítač	k1gInPc1	počítač
bývají	bývat	k5eAaImIp3nP	bývat
úzce	úzko	k6eAd1	úzko
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
úlohu	úloha	k1gFnSc4	úloha
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
třídu	třída	k1gFnSc4	třída
úloh	úloha	k1gFnPc2	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgMnSc3	ten
číslicové	číslicový	k2eAgInPc1d1	číslicový
počítače	počítač	k1gInPc1	počítač
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
zkonstruovat	zkonstruovat	k5eAaPmF	zkonstruovat
coby	coby	k?	coby
univerzální	univerzální	k2eAgInPc1d1	univerzální
(	(	kIx(	(
<g/>
ne	ne	k9	ne
všechny	všechen	k3xTgInPc1	všechen
číslicové	číslicový	k2eAgInPc1d1	číslicový
počítače	počítač	k1gInPc1	počítač
ovšem	ovšem	k9	ovšem
zcela	zcela	k6eAd1	zcela
univerzální	univerzální	k2eAgMnPc1d1	univerzální
jsou	být	k5eAaImIp3nP	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Church-Turingovy	Church-Turingův	k2eAgFnSc2d1	Church-Turingův
teze	teze	k1gFnSc2	teze
je	být	k5eAaImIp3nS	být
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
číslicový	číslicový	k2eAgInSc1d1	číslicový
počítač	počítač	k1gInSc1	počítač
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
minimálními	minimální	k2eAgFnPc7d1	minimální
schopnostmi	schopnost	k1gFnPc7	schopnost
schopný	schopný	k2eAgInSc4d1	schopný
provést	provést	k5eAaPmF	provést
v	v	k7c6	v
principu	princip	k1gInSc6	princip
totéž	týž	k3xTgNnSc1	týž
jako	jako	k8xC	jako
libovolný	libovolný	k2eAgInSc4d1	libovolný
jiný	jiný	k2eAgInSc4d1	jiný
počítač	počítač	k1gInSc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
univerzalitě	univerzalita	k1gFnSc3	univerzalita
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
převážně	převážně	k6eAd1	převážně
používány	používat	k5eAaImNgInP	používat
i	i	k9	i
konstruovány	konstruován	k2eAgInPc1d1	konstruován
číslicové	číslicový	k2eAgInPc1d1	číslicový
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
a	a	k8xC	a
přesnější	přesný	k2eAgMnSc1d2	přesnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
analogové	analogový	k2eAgInPc1d1	analogový
počítače	počítač	k1gInPc1	počítač
zpracovávající	zpracovávající	k2eAgFnSc2d1	zpracovávající
analogové	analogový	k2eAgFnSc2d1	analogová
úlohy	úloha	k1gFnSc2	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
základní	základní	k2eAgFnPc1d1	základní
koncepce	koncepce	k1gFnPc1	koncepce
konstrukce	konstrukce	k1gFnSc2	konstrukce
číslicového	číslicový	k2eAgInSc2d1	číslicový
počítače	počítač	k1gInSc2	počítač
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
von	von	k1gInSc4	von
Neumannovo	Neumannův	k2eAgNnSc4d1	Neumannovo
schéma	schéma	k1gNnSc4	schéma
počítače	počítač	k1gInSc2	počítač
–	–	k?	–
Neumannovo	Neumannův	k2eAgNnSc1d1	Neumannovo
používá	používat	k5eAaImIp3nS	používat
jednu	jeden	k4xCgFnSc4	jeden
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
paměť	paměť	k1gFnSc4	paměť
pro	pro	k7c4	pro
program	program	k1gInSc4	program
i	i	k9	i
pro	pro	k7c4	pro
data	datum	k1gNnPc4	datum
Harvardská	harvardský	k2eAgFnSc1d1	Harvardská
architektura	architektura	k1gFnSc1	architektura
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
oddělenou	oddělený	k2eAgFnSc4d1	oddělená
paměť	paměť	k1gFnSc4	paměť
pro	pro	k7c4	pro
data	datum	k1gNnPc4	datum
a	a	k8xC	a
pro	pro	k7c4	pro
program	program	k1gInSc4	program
Současné	současný	k2eAgInPc1d1	současný
počítače	počítač	k1gInPc1	počítač
nejsou	být	k5eNaImIp3nP	být
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
důsledně	důsledně	k6eAd1	důsledně
ani	ani	k8xC	ani
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgNnPc2	dva
základních	základní	k2eAgNnPc2d1	základní
schémat	schéma	k1gNnPc2	schéma
<g/>
.	.	kIx.	.
</s>
<s>
Univerzální	univerzální	k2eAgInPc1d1	univerzální
osobní	osobní	k2eAgInPc1d1	osobní
počítače	počítač	k1gInPc1	počítač
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
programy	program	k1gInPc1	program
i	i	k8xC	i
zpracovávaná	zpracovávaný	k2eAgNnPc1d1	zpracovávané
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
paměť	paměť	k1gFnSc4	paměť
obsahující	obsahující	k2eAgInSc4d1	obsahující
program	program	k1gInSc4	program
označit	označit	k5eAaPmF	označit
jen	jen	k9	jen
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
část	část	k1gFnSc1	část
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
data	datum	k1gNnPc4	datum
označit	označit	k5eAaPmF	označit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelze	lze	k6eNd1	lze
vykonávat	vykonávat	k5eAaImF	vykonávat
strojové	strojový	k2eAgFnPc4d1	strojová
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uloženy	uložit	k5eAaPmNgInP	uložit
<g/>
.	.	kIx.	.
</s>
<s>
Harvardské	harvardský	k2eAgNnSc1d1	Harvardské
schéma	schéma	k1gNnSc1	schéma
s	s	k7c7	s
oddělenou	oddělený	k2eAgFnSc7d1	oddělená
pamětí	paměť	k1gFnSc7	paměť
pro	pro	k7c4	pro
program	program	k1gInSc4	program
a	a	k8xC	a
data	datum	k1gNnPc1	datum
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
u	u	k7c2	u
jednočipových	jednočipový	k2eAgInPc2d1	jednočipový
počítačů	počítač	k1gInPc2	počítač
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
malých	malý	k2eAgInPc2d1	malý
vestavěných	vestavěný	k2eAgInPc2d1	vestavěný
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
PDA	PDA	kA	PDA
<g/>
,	,	kIx,	,
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
a	a	k8xC	a
především	především	k9	především
u	u	k7c2	u
signálových	signálový	k2eAgInPc2d1	signálový
procesorů	procesor	k1gInPc2	procesor
(	(	kIx(	(
<g/>
DSP	DSP	kA	DSP
<g/>
)	)	kIx)	)
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
velké	velký	k2eAgFnSc3d1	velká
rychlosti	rychlost	k1gFnSc3	rychlost
zpracování	zpracování	k1gNnSc4	zpracování
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Počítače	počítač	k1gInPc1	počítač
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
základních	základní	k2eAgInPc2d1	základní
druhů	druh	k1gInPc2	druh
komponentů	komponent	k1gInPc2	komponent
<g/>
:	:	kIx,	:
software	software	k1gInSc1	software
–	–	k?	–
programové	programový	k2eAgNnSc4d1	programové
vybavení	vybavení	k1gNnSc4	vybavení
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
řada	řada	k1gFnSc1	řada
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
jsou	být	k5eAaImIp3nP	být
počítačem	počítač	k1gInSc7	počítač
postupně	postupně	k6eAd1	postupně
provedeny	provést	k5eAaPmNgFnP	provést
hardware	hardware	k1gInSc4	hardware
–	–	k?	–
technické	technický	k2eAgNnSc4d1	technické
vybavení	vybavení	k1gNnSc4	vybavení
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
fyzické	fyzický	k2eAgFnPc1d1	fyzická
části	část	k1gFnPc1	část
(	(	kIx(	(
<g/>
slangově	slangově	k6eAd1	slangově
železo	železo	k1gNnSc1	železo
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
nejen	nejen	k6eAd1	nejen
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
pomocné	pomocný	k2eAgInPc4d1	pomocný
programy	program	k1gInPc4	program
a	a	k8xC	a
aplikační	aplikační	k2eAgInSc4d1	aplikační
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
napevno	napevno	k6eAd1	napevno
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
BIOSu	BIOSus	k1gInSc6	BIOSus
nebo	nebo	k8xC	nebo
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
vstupně-výstupních	vstupněýstupní	k2eAgNnPc6d1	vstupně-výstupní
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
je	být	k5eAaImIp3nS	být
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
řeší	řešit	k5eAaImIp3nP	řešit
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
úlohy	úloha	k1gFnPc4	úloha
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
programování	programování	k1gNnSc1	programování
jako	jako	k8xS	jako
zápis	zápis	k1gInSc1	zápis
algoritmu	algoritmus	k1gInSc2	algoritmus
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
programovacím	programovací	k2eAgInSc6d1	programovací
jazyku	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Spuštěný	spuštěný	k2eAgInSc4d1	spuštěný
program	program	k1gInSc4	program
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Hardware	hardware	k1gInSc1	hardware
je	být	k5eAaImIp3nS	být
technické	technický	k2eAgNnSc4d1	technické
vybavení	vybavení	k1gNnSc4	vybavení
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnujeme	zahrnovat	k5eAaImIp1nP	zahrnovat
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
všechny	všechen	k3xTgFnPc4	všechen
fyzické	fyzický	k2eAgFnPc4d1	fyzická
součásti	součást	k1gFnPc4	součást
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hardware	hardware	k1gInSc1	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
počítač	počítač	k1gInSc1	počítač
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
součástí	součást	k1gFnPc2	součást
<g/>
:	:	kIx,	:
počítačová	počítačový	k2eAgFnSc1d1	počítačová
skříň	skříň	k1gFnSc1	skříň
–	–	k?	–
skříň	skříň	k1gFnSc1	skříň
z	z	k7c2	z
plechu	plech	k1gInSc2	plech
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
materiálů	materiál	k1gInPc2	materiál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
též	též	k9	též
součástí	součást	k1gFnSc7	součást
monitoru	monitor	k1gInSc2	monitor
(	(	kIx(	(
<g/>
iMac	iMac	k1gFnSc1	iMac
<g/>
)	)	kIx)	)
základní	základní	k2eAgFnSc1d1	základní
deska	deska	k1gFnSc1	deska
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
většinu	většina	k1gFnSc4	většina
elektronických	elektronický	k2eAgFnPc2d1	elektronická
částí	část	k1gFnPc2	část
počítače	počítač	k1gInSc2	počítač
mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
–	–	k?	–
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
strojové	strojový	k2eAgFnPc4d1	strojová
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
,	,	kIx,	,
ze	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
kterých	který	k3yRgFnPc2	který
jsou	být	k5eAaImIp3nP	být
složeny	složen	k2eAgInPc1d1	složen
programy	program	k1gInPc1	program
operační	operační	k2eAgFnSc4d1	operační
paměť	paměť	k1gFnSc4	paměť
–	–	k?	–
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
počítače	počítač	k1gInSc2	počítač
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
programy	program	k1gInPc4	program
a	a	k8xC	a
data	datum	k1gNnPc4	datum
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
elektronická	elektronický	k2eAgFnSc1d1	elektronická
paměť	paměť	k1gFnSc1	paměť
<g/>
)	)	kIx)	)
sběrnice	sběrnice	k1gFnSc1	sběrnice
–	–	k?	–
propojuje	propojovat	k5eAaImIp3nS	propojovat
vstupně-výstupní	vstupněýstupní	k2eAgNnPc4d1	vstupně-výstupní
zařízení	zařízení	k1gNnPc4	zařízení
s	s	k7c7	s
procesorem	procesor	k1gInSc7	procesor
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
připojení	připojení	k1gNnSc4	připojení
rozšiřujících	rozšiřující	k2eAgFnPc2d1	rozšiřující
karet	kareta	k1gFnPc2	kareta
grafická	grafický	k2eAgFnSc1d1	grafická
karta	karta	k1gFnSc1	karta
–	–	k?	–
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytvořit	vytvořit	k5eAaPmF	vytvořit
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
zvuková	zvukový	k2eAgFnSc1d1	zvuková
karta	karta	k1gFnSc1	karta
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
reproduktoru	reproduktor	k1gInSc6	reproduktor
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
zvuk	zvuk	k1gInSc4	zvuk
síťová	síťový	k2eAgFnSc1d1	síťová
karta	karta	k1gFnSc1	karta
–	–	k?	–
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
počítačové	počítačový	k2eAgFnSc3d1	počítačová
síti	síť	k1gFnSc3	síť
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
rozšiřující	rozšiřující	k2eAgNnPc4d1	rozšiřující
zařízení	zařízení	k1gNnPc4	zařízení
pevný	pevný	k2eAgInSc4d1	pevný
disk	disk	k1gInSc4	disk
–	–	k?	–
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
programy	program	k1gInPc4	program
i	i	k8xC	i
data	datum	k1gNnPc4	datum
i	i	k9	i
po	po	k7c6	po
vypnutí	vypnutí	k1gNnSc6	vypnutí
počítače	počítač	k1gInSc2	počítač
elektrický	elektrický	k2eAgInSc4d1	elektrický
zdroj	zdroj	k1gInSc4	zdroj
–	–	k?	–
mění	měnit	k5eAaImIp3nS	měnit
síťové	síťový	k2eAgNnSc1d1	síťové
střídavé	střídavý	k2eAgNnSc1d1	střídavé
napětí	napětí	k1gNnSc1	napětí
230	[number]	k4	230
V	V	kA	V
na	na	k7c4	na
nižší	nízký	k2eAgNnSc4d2	nižší
stejnosměrné	stejnosměrný	k2eAgNnSc4d1	stejnosměrné
napětí	napětí	k1gNnSc4	napětí
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
obvykle	obvykle	k6eAd1	obvykle
12	[number]	k4	12
<g/>
,	,	kIx,	,
5	[number]	k4	5
a	a	k8xC	a
3,3	[number]	k4	3,3
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
napájení	napájení	k1gNnSc4	napájení
komponent	komponenta	k1gFnPc2	komponenta
počítače	počítač	k1gInSc2	počítač
monitor	monitor	k1gInSc1	monitor
–	–	k?	–
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
informace	informace	k1gFnPc4	informace
uživateli	uživatel	k1gMnSc3	uživatel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
ke	k	k7c3	k
grafické	grafický	k2eAgFnSc3d1	grafická
kartě	karta	k1gFnSc3	karta
počítačová	počítačový	k2eAgFnSc1d1	počítačová
klávesnice	klávesnice	k1gFnSc1	klávesnice
–	–	k?	–
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
alfanumerický	alfanumerický	k2eAgInSc1d1	alfanumerický
vstup	vstup	k1gInSc1	vstup
od	od	k7c2	od
uživatele	uživatel	k1gMnSc2	uživatel
počítačová	počítačový	k2eAgFnSc1d1	počítačová
myš	myš	k1gFnSc1	myš
–	–	k?	–
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pohybovat	pohybovat	k5eAaImF	pohybovat
kurzorem	kurzor	k1gInSc7	kurzor
myši	myš	k1gFnSc2	myš
a	a	k8xC	a
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
události	událost	k1gFnPc4	událost
stiskem	stisk	k1gInSc7	stisk
tlačítka	tlačítko	k1gNnSc2	tlačítko
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
vstupní-výstupní	vstupníýstupní	k2eAgNnPc1d1	vstupní-výstupní
zařízení	zařízení	k1gNnPc1	zařízení
(	(	kIx(	(
<g/>
počítačová	počítačový	k2eAgFnSc1d1	počítačová
tiskárna	tiskárna	k1gFnSc1	tiskárna
<g/>
,	,	kIx,	,
scanner	scanner	k1gInSc1	scanner
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
Počítače	počítač	k1gInPc1	počítač
již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
téměř	téměř	k6eAd1	téměř
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
lidských	lidský	k2eAgFnPc2d1	lidská
činností	činnost	k1gFnPc2	činnost
i	i	k9	i
do	do	k7c2	do
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
vliv	vliv	k1gInSc1	vliv
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
nadále	nadále	k6eAd1	nadále
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
lidem	lid	k1gInSc7	lid
poskytovat	poskytovat	k5eAaImF	poskytovat
komfortnější	komfortní	k2eAgFnPc4d2	komfortnější
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Počítače	počítač	k1gInPc1	počítač
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
propojovány	propojovat	k5eAaImNgInP	propojovat
pomocí	pomocí	k7c2	pomocí
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
využívají	využívat	k5eAaPmIp3nP	využívat
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
síť	síť	k1gFnSc4	síť
Internet	Internet	k1gInSc1	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Počítačové	počítačový	k2eAgFnPc1d1	počítačová
sítě	síť	k1gFnPc1	síť
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
sdílení	sdílení	k1gNnSc4	sdílení
zdrojů	zdroj	k1gInPc2	zdroj
(	(	kIx(	(
<g/>
soubory	soubor	k1gInPc1	soubor
<g/>
,	,	kIx,	,
tiskárny	tiskárna	k1gFnPc1	tiskárna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
moderních	moderní	k2eAgInPc2d1	moderní
komunikačních	komunikační	k2eAgInPc2d1	komunikační
nástrojů	nástroj	k1gInPc2	nástroj
informační	informační	k2eAgFnSc2d1	informační
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Cloud	Cloud	k6eAd1	Cloud
computing	computing	k1gInSc1	computing
tak	tak	k6eAd1	tak
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
nezávislost	nezávislost	k1gFnSc4	nezávislost
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
a	a	k8xC	a
centralizace	centralizace	k1gFnSc1	centralizace
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
informacím	informace	k1gFnPc3	informace
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšovaná	zvyšovaný	k2eAgFnSc1d1	zvyšovaná
počítačová	počítačový	k2eAgFnSc1d1	počítačová
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
a	a	k8xC	a
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
propojení	propojení	k1gNnSc6	propojení
povedou	povést	k5eAaPmIp3nP	povést
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
v	v	k7c6	v
tlaku	tlak	k1gInSc6	tlak
na	na	k7c4	na
důvěryhodné	důvěryhodný	k2eAgInPc4d1	důvěryhodný
počítače	počítač	k1gInPc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
již	již	k9	již
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
omezování	omezování	k1gNnSc3	omezování
práv	právo	k1gNnPc2	právo
uživatelů	uživatel	k1gMnPc2	uživatel
počítačů	počítač	k1gInPc2	počítač
i	i	k8xC	i
mobilních	mobilní	k2eAgNnPc2d1	mobilní
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Trusted	Trusted	k1gInSc1	Trusted
Platform	Platform	k1gInSc1	Platform
Module	modul	k1gInSc5	modul
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
UEFI	UEFI	kA	UEFI
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
z	z	k7c2	z
App	App	k1gFnSc2	App
Store	Stor	k1gMnSc5	Stor
či	či	k8xC	či
Google	Googl	k1gMnSc5	Googl
Play	play	k0	play
<g/>
)	)	kIx)	)
brání	bránit	k5eAaImIp3nP	bránit
spouštění	spouštění	k1gNnSc4	spouštění
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
datům	datum	k1gNnPc3	datum
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
neschválí	schválit	k5eNaPmIp3nS	schválit
výrobce	výrobce	k1gMnSc1	výrobce
či	či	k8xC	či
majitel	majitel	k1gMnSc1	majitel
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
