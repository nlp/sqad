<s>
Hořčík	hořčík	k1gInSc1	hořčík
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Magnesium	magnesium	k1gNnSc1	magnesium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
stříbrolesklý	stříbrolesklý	k2eAgInSc1d1	stříbrolesklý
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
