<s>
Samotný	samotný	k2eAgInSc1d1
znak	znak	k1gInSc1
tabulátoru	tabulátor	k1gInSc2
je	být	k5eAaImIp3nS
tvořen	tvořen	k2eAgInSc1d1
ASCII	ascii	kA
kódem	kód	k1gInSc7
9	#num#	k4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
hexa	hexa	k1gFnSc1
0	#num#	k4
<g/>
x	x	k?
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
odborné	odborný	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
a	a	k8xC
samotných	samotný	k2eAgInPc6d1
technických	technický	k2eAgInPc6d1
prostředcích	prostředek	k1gInPc6
pro	pro	k7c4
zpracování	zpracování	k1gNnSc4
textu	text	k1gInSc2
často	často	k6eAd1
zastoupen	zastoupit	k5eAaPmNgMnS
dvojicí	dvojice	k1gFnSc7
znaků	znak	k1gInPc2
\	\	kIx~
<g/>
t.	t.	k?
V	v	k7c6
laické	laický	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
se	s	k7c7
zkratkou	zkratka	k1gFnSc7
Tab	tab	kA
pro	pro	k7c4
klávesu	klávesa	k1gFnSc4
a	a	k8xC
<tab>
pro	pro	k7c4
samotný	samotný	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
.	.	kIx.
</s>