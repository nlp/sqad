<s>
Tabulátor	tabulátor	k1gInSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Tab	tab	kA
<g/>
.	.	kIx.
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
základním	základní	k2eAgInSc6d1
významu	význam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
městě	město	k1gNnSc6
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Tab	tab	kA
(	(	kIx(
<g/>
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Klávesa	klávesa	k1gFnSc1
tabulátoru	tabulátor	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c4
klávesnici	klávesnice	k1gFnSc4
PC	PC	kA
obvykle	obvykle	k6eAd1
vedle	vedle	k7c2
písmena	písmeno	k1gNnSc2
Q.	Q.	kA
</s>
<s>
Tabulátor	tabulátor	k1gInSc1
je	být	k5eAaImIp3nS
speciální	speciální	k2eAgFnSc1d1
klávesa	klávesa	k1gFnSc1
na	na	k7c4
klávesnici	klávesnice	k1gFnSc4
počítačů	počítač	k1gInPc2
a	a	k8xC
psacích	psací	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňuje	umožňovat	k5eAaImIp3nS
snadný	snadný	k2eAgInSc1d1
zápis	zápis	k1gInSc1
údajů	údaj	k1gInPc2
do	do	k7c2
sloupců	sloupec	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
zarovnány	zarovnán	k2eAgInPc1d1
od	od	k7c2
jednotné	jednotný	k2eAgFnSc2d1
svislice	svislice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
psacích	psací	k2eAgInPc6d1
strojích	stroj	k1gInPc6
býval	bývat	k5eAaImAgInS
kromě	kromě	k7c2
klávesy	klávesa	k1gFnSc2
tabulátoru	tabulátor	k1gInSc2
i	i	k9
dekadický	dekadický	k2eAgInSc4d1
tabulátor	tabulátor	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
tvořen	tvořit	k5eAaImNgInS
několika	několik	k4yIc7
klávesami	klávesa	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
tabulátor	tabulátor	k1gInSc4
umožňoval	umožňovat	k5eAaImAgInS
zarovnávat	zarovnávat	k5eAaImF
číselné	číselný	k2eAgInPc4d1
údaje	údaj	k1gInPc4
do	do	k7c2
sloupců	sloupec	k1gInPc2
na	na	k7c4
desetinný	desetinný	k2eAgInSc4d1
oddělovač	oddělovač	k1gInSc4
(	(	kIx(
<g/>
čárku	čárka	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
běžném	běžný	k2eAgInSc6d1
hovoru	hovor	k1gInSc6
a	a	k8xC
textu	text	k1gInSc2
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
rozlišovat	rozlišovat	k5eAaImF
mezi	mezi	k7c7
klávesou	klávesa	k1gFnSc7
tabulátoru	tabulátor	k1gInSc2
<g/>
,	,	kIx,
stiskem	stisk	k1gInSc7
klávesy	klávesa	k1gFnSc2
tabulátoru	tabulátor	k1gInSc2
a	a	k8xC
samotným	samotný	k2eAgInSc7d1
znakem	znak	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
obdrží	obdržet	k5eAaPmIp3nS
program	program	k1gInSc4
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ke	k	k7c3
stisku	stisk	k1gInSc3
klávesy	klávesa	k1gFnSc2
dojde	dojít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc1
tabulátoru	tabulátor	k1gInSc2
</s>
<s>
Po	po	k7c6
stisku	stisk	k1gInSc6
klávesy	klávesa	k1gFnSc2
tabulátoru	tabulátor	k1gInSc2
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
zaslání	zaslání	k1gNnSc3
kódu	kód	k1gInSc2
stisknuté	stisknutý	k2eAgFnSc2d1
klávesy	klávesa	k1gFnSc2
operačnímu	operační	k2eAgInSc3d1
systému	systém	k1gInSc3
počítače	počítač	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
kód	kód	k1gInSc1
přeloží	přeložit	k5eAaPmIp3nS
pomocí	pomocí	k7c2
ovladače	ovladač	k1gInSc2
vstupních	vstupní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
na	na	k7c4
samotný	samotný	k2eAgInSc4d1
znak	znak	k1gInSc4
tabulátoru	tabulátor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
tvoří	tvořit	k5eAaImIp3nP
spolu	spolu	k6eAd1
se	s	k7c7
standardní	standardní	k2eAgFnSc7d1
mezerou	mezera	k1gFnSc7
a	a	k8xC
kódem	kód	k1gInSc7
nového	nový	k2eAgInSc2d1
řádku	řádek	k1gInSc2
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejčastěji	často	k6eAd3
používaných	používaný	k2eAgInPc2d1
bílých	bílý	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jsou	být	k5eAaImIp3nP
znaky	znak	k1gInPc1
v	v	k7c6
běžné	běžný	k2eAgFnSc6d1
reprezentaci	reprezentace	k1gFnSc6
neviditelné	viditelný	k2eNgFnSc6d1
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yQgMnPc2,k3yIgMnPc2,k3yRgMnPc2
pozorujeme	pozorovat	k5eAaImIp1nP
pouze	pouze	k6eAd1
jejich	jejich	k3xOp3gInPc1
projevy	projev	k1gInPc1
(	(	kIx(
<g/>
například	například	k6eAd1
nový	nový	k2eAgInSc4d1
řádek	řádek	k1gInSc4
<g/>
,	,	kIx,
či	či	k8xC
odsazení	odsazení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1
znak	znak	k1gInSc1
tabulátoru	tabulátor	k1gInSc2
je	být	k5eAaImIp3nS
tvořen	tvořen	k2eAgInSc1d1
ASCII	ascii	kA
kódem	kód	k1gInSc7
9	#num#	k4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
hexa	hexa	k1gFnSc1
0	#num#	k4
<g/>
x	x	k?
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
odborné	odborný	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
a	a	k8xC
samotných	samotný	k2eAgInPc6d1
technických	technický	k2eAgInPc6d1
prostředcích	prostředek	k1gInPc6
pro	pro	k7c4
zpracování	zpracování	k1gNnSc4
textu	text	k1gInSc2
často	často	k6eAd1
zastoupen	zastoupit	k5eAaPmNgMnS
dvojicí	dvojice	k1gFnSc7
znaků	znak	k1gInPc2
\	\	kIx~
<g/>
t.	t.	k?
V	v	k7c6
laické	laický	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
se	s	k7c7
zkratkou	zkratka	k1gFnSc7
Tab	tab	kA
pro	pro	k7c4
klávesu	klávesa	k1gFnSc4
a	a	k8xC
<tab>
pro	pro	k7c4
samotný	samotný	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Ukázka	ukázka	k1gFnSc1
odsazování	odsazování	k1gNnSc2
a	a	k8xC
zobrazování	zobrazování	k1gNnSc2
jinak	jinak	k6eAd1
neviditelných	viditelný	k2eNgInPc2d1
znaků	znak	k1gInPc2
v	v	k7c6
programátorském	programátorský	k2eAgInSc6d1
editoru	editor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současném	současný	k2eAgInSc6d1
světě	svět	k1gInSc6
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
je	být	k5eAaImIp3nS
tabulátor	tabulátor	k1gInSc1
laiky	laik	k1gMnPc7
využíván	využíván	k2eAgInSc4d1
převážně	převážně	k6eAd1
k	k	k7c3
zarovnávání	zarovnávání	k1gNnSc3
bloků	blok	k1gInPc2
textu	text	k1gInSc2
<g/>
,	,	kIx,
programátory	programátor	k1gInPc1
pak	pak	k6eAd1
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
pak	pak	k6eAd1
také	také	k9
jako	jako	k9
oddělovač	oddělovač	k1gInSc4
při	při	k7c6
ukládání	ukládání	k1gNnSc6
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
TSV	TSV	kA
(	(	kIx(
<g/>
datový	datový	k2eAgInSc4d1
formát	formát	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
použití	použití	k1gNnSc6
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
rozeznávat	rozeznávat	k5eAaImF
stisk	stisk	k1gInSc4
klávesy	klávesa	k1gFnSc2
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
nemusí	muset	k5eNaImIp3nS
mít	mít	k5eAaImF
za	za	k7c4
důsledek	důsledek	k1gInSc4
očekávanou	očekávaný	k2eAgFnSc4d1
akci	akce	k1gFnSc4
<g/>
)	)	kIx)
od	od	k7c2
vložení	vložení	k1gNnSc2
znaku	znak	k1gInSc2
do	do	k7c2
proudu	proud	k1gInSc2
bajtů	bajt	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
je	být	k5eAaImIp3nS
uložen	uložen	k2eAgInSc1d1
soubor	soubor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Klávesa	klávesa	k1gFnSc1
tabulátoru	tabulátor	k1gInSc2
hraje	hrát	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
programování	programování	k1gNnSc6
<g/>
,	,	kIx,
při	při	k7c6
zarovnávání	zarovnávání	k1gNnSc6
bloků	blok	k1gInPc2
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohé	k1gNnSc1
programátorské	programátorský	k2eAgInPc1d1
editory	editor	k1gInPc1
umožňují	umožňovat	k5eAaImIp3nP
při	při	k7c6
vybrání	vybrání	k1gNnSc6
textu	text	k1gInSc2
posouvat	posouvat	k5eAaImF
stiskem	stisk	k1gInSc7
klávesy	klávesa	k1gFnSc2
tabulátoru	tabulátor	k1gInSc2
celý	celý	k2eAgInSc4d1
vybraný	vybraný	k2eAgInSc4d1
blok	blok	k1gInSc4
o	o	k7c4
patřičné	patřičný	k2eAgNnSc4d1
odsazení	odsazení	k1gNnSc4
doleva	doleva	k6eAd1
<g/>
,	,	kIx,
či	či	k8xC
doprava	doprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
podstatné	podstatný	k2eAgNnSc1d1
například	například	k6eAd1
v	v	k7c6
Pythonu	Python	k1gMnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
odsazení	odsazení	k1gNnSc4
silný	silný	k2eAgInSc1d1
vliv	vliv	k1gInSc1
na	na	k7c4
vykonávání	vykonávání	k1gNnSc4
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c4
nastavení	nastavení	k1gNnSc4
pokročilého	pokročilý	k2eAgInSc2d1
editoru	editor	k1gInSc2
muže	muž	k1gMnSc2
po	po	k7c6
stisku	stisk	k1gInSc6
klávesy	klávesa	k1gFnSc2
docházet	docházet	k5eAaImF
jednak	jednak	k8xC
k	k	k7c3
vložení	vložení	k1gNnSc3
znaku	znak	k1gInSc2
tabulátoru	tabulátor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
poté	poté	k6eAd1
zobrazen	zobrazit	k5eAaPmNgInS
dle	dle	k7c2
nastavení	nastavení	k1gNnSc2
editoru	editor	k1gInSc2
<g/>
,	,	kIx,
či	či	k8xC
k	k	k7c3
vložení	vložení	k1gNnSc4
několika	několik	k4yIc2
znaků	znak	k1gInPc2
mezer	mezera	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konkrétní	konkrétní	k2eAgFnSc1d1
preference	preference	k1gFnSc1
znaků	znak	k1gInPc2
či	či	k8xC
mezer	mezera	k1gFnPc2
programátory	programátor	k1gMnPc7
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
projekt	projekt	k1gInSc1
od	od	k7c2
projektu	projekt	k1gInSc2
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
jazyky	jazyk	k1gInPc1
také	také	k9
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
standardech	standard	k1gInPc6
vyžadují	vyžadovat	k5eAaImIp3nP
používání	používání	k1gNnSc4
mezer	mezera	k1gFnPc2
<g/>
,	,	kIx,
namísto	namísto	k7c2
odsazení	odsazení	k1gNnSc2
znakem	znak	k1gInSc7
\	\	kIx~
<g/>
t.	t.	k?
</s>
<s>
Některé	některý	k3yIgInPc1
textové	textový	k2eAgInPc1d1
editory	editor	k1gInPc1
jsou	být	k5eAaImIp3nP
schopné	schopný	k2eAgInPc1d1
zobrazovat	zobrazovat	k5eAaImF
neviditelné	viditelný	k2eNgInPc4d1
bílé	bílý	k2eAgInPc4d1
znaky	znak	k1gInPc4
pomocí	pomocí	k7c2
znaků	znak	k1gInPc2
zástupných	zástupný	k2eAgInPc2d1
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
přiložený	přiložený	k2eAgInSc4d1
obrázek	obrázek	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
pozorovat	pozorovat	k5eAaImF
různá	různý	k2eAgNnPc4d1
nastavení	nastavení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
tři	tři	k4xCgFnPc1
ukázky	ukázka	k1gFnPc1
jsou	být	k5eAaImIp3nP
projevem	projev	k1gInSc7
stisku	stisk	k1gInSc2
stejné	stejný	k2eAgFnSc2d1
klávesy	klávesa	k1gFnSc2
tabulátoru	tabulátor	k1gInSc2
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
chování	chování	k1gNnSc1
je	být	k5eAaImIp3nS
pokaždé	pokaždé	k6eAd1
jiné	jiný	k2eAgNnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c4
nastavení	nastavení	k1gNnSc4
editoru	editor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Formulářové	formulářový	k2eAgInPc1d1
dialogy	dialog	k1gInPc1
</s>
<s>
Speciální	speciální	k2eAgNnSc1d1
použití	použití	k1gNnSc1
klávesy	klávesa	k1gFnSc2
tab	tab	kA
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
nalézt	nalézt	k5eAaPmF,k5eAaBmF
v	v	k7c6
programech	program	k1gInPc6
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
zobrazují	zobrazovat	k5eAaImIp3nP
formuláře	formulář	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historicky	historicky	k6eAd1
zde	zde	k6eAd1
existuje	existovat	k5eAaImIp3nS
konvence	konvence	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
klávesa	klávesa	k1gFnSc1
tabulátoru	tabulátor	k1gInSc2
nevkládá	vkládat	k5eNaImIp3nS
znak	znak	k1gInSc1
tabulátoru	tabulátor	k1gInSc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
způsobuje	způsobovat	k5eAaImIp3nS
přeskočení	přeskočení	k1gNnSc4
na	na	k7c4
další	další	k2eAgInSc4d1
prvek	prvek	k1gInSc4
formuláře	formulář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
chování	chování	k1gNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
pozorovat	pozorovat	k5eAaImF
i	i	k9
ve	v	k7c6
webových	webový	k2eAgInPc6d1
prohlížečích	prohlížeč	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
při	při	k7c6
stisknutí	stisknutí	k1gNnSc6
klávesy	klávesa	k1gFnSc2
nedojde	dojít	k5eNaPmIp3nS
k	k	k7c3
vložení	vložení	k1gNnSc3
znaku	znak	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
přeskoku	přeskok	k1gInSc3
na	na	k7c4
další	další	k2eAgInSc4d1
aktivní	aktivní	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
odkaz	odkaz	k1gInSc1
<g/>
,	,	kIx,
vstupní	vstupní	k2eAgNnSc1d1
pole	pole	k1gNnSc1
<g/>
,	,	kIx,
či	či	k8xC
tlačítko	tlačítko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Ukázka	ukázka	k1gFnSc1
zarovnaného	zarovnaný	k2eAgInSc2d1
textu	text	k1gInSc2
</s>
<s>
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
příklad	příklad	k1gInSc4
tabulky	tabulka	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
vložením	vložení	k1gNnSc7
znaku	znak	k1gInSc2
tabulátoru	tabulátor	k1gInSc2
za	za	k7c4
různě	různě	k6eAd1
dlouhé	dlouhý	k2eAgInPc4d1
řetězce	řetězec	k1gInPc4
znaků	znak	k1gInPc2
na	na	k7c6
začátku	začátek	k1gInSc6
řádku	řádek	k1gInSc2
(	(	kIx(
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
kbd	kbd	k?
<g/>
.	.	kIx.
<g/>
Sablona__Klavesa	Sablona__Klavesa	k1gFnSc1
<g/>
{	{	kIx(
<g/>
background-color	background-color	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
<g/>
background-image	background-imag	k1gInSc2
<g/>
:	:	kIx,
<g/>
linear-gradient	linear-gradient	k1gMnSc1
<g/>
(	(	kIx(
<g/>
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
255,255,255	255,255,255	k4
<g/>
,	,	kIx,
<g/>
.4	.4	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
0,0	0,0	k4
<g/>
,0	,0	k4
<g/>
,	,	kIx,
<g/>
.1	.1	k4
<g/>
))	))	k?
<g/>
;	;	kIx,
<g/>
border	border	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
;	;	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
border-color	border-color	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
DDD	DDD	kA
#	#	kIx~
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
b	b	k?
<g/>
1	#num#	k4
#	#	kIx~
<g/>
888	#num#	k4
#	#	kIx~
<g/>
CCC	CCC	kA
<g/>
;	;	kIx,
<g/>
border-radius	border-radius	k1gMnSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
.4	.4	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
text-shadow	text-shadow	k?
<g/>
:	:	kIx,
<g/>
0	#num#	k4
1	#num#	k4
<g/>
px	px	k?
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
255,255,255	255,255,255	k4
<g/>
,	,	kIx,
<g/>
.5	.5	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gMnSc1
<g/>
}	}	kIx)
<g/>
Tab	tab	kA
↹	↹	k?
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
x	x	k?
y	y	k?
</s>
<s>
xx	xx	k?
y	y	k?
</s>
<s>
xxxy	xxxa	k1gFnPc1
</s>
<s>
xxxxy	xxxx	k1gInPc1
</s>
<s>
Jak	jak	k6eAd1
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
na	na	k7c6
začátku	začátek	k1gInSc6
řádku	řádek	k1gInSc2
nachází	nacházet	k5eAaImIp3nS
rozdílný	rozdílný	k2eAgInSc1d1
počet	počet	k1gInSc1
znaků	znak	k1gInPc2
x	x	k?
<g/>
,	,	kIx,
text	text	k1gInSc1
oddělený	oddělený	k2eAgInSc1d1
znakem	znak	k1gInSc7
tabulátoru	tabulátor	k1gInSc2
se	se	k3xPyFc4
zobrazuje	zobrazovat	k5eAaImIp3nS
(	(	kIx(
<g/>
alespoň	alespoň	k9
při	při	k7c6
standardním	standardní	k2eAgNnSc6d1
nastavení	nastavení	k1gNnSc6
<g/>
)	)	kIx)
stejně	stejně	k6eAd1
zarovnaný	zarovnaný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Nezaměňovat	zaměňovat	k5eNaImF
s	s	k7c7
tabelátorem	tabelátor	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
stroj	stroj	k1gInSc4
na	na	k7c4
snímání	snímání	k1gNnSc4
děrných	děrný	k2eAgInPc2d1
štítků	štítek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
počítačová	počítačový	k2eAgFnSc1d1
klávesnice	klávesnice	k1gFnSc1
QWERTZ	QWERTZ	kA
</s>
<s>
Esc	Esc	k?
</s>
<s>
F1	F1	k4
</s>
<s>
F2	F2	k4
</s>
<s>
F3	F3	k4
</s>
<s>
F4	F4	k4
</s>
<s>
F5	F5	k4
</s>
<s>
F6	F6	k4
</s>
<s>
F7	F7	k4
</s>
<s>
F8	F8	k4
</s>
<s>
F9	F9	k4
</s>
<s>
F10	F10	k4
</s>
<s>
F11	F11	k4
</s>
<s>
F12	F12	k4
</s>
<s>
PrtScSysRq	PrtScSysRq	k?
</s>
<s>
ScrLk	ScrLk	k6eAd1
</s>
<s>
Pause	pausa	k1gFnSc3
</s>
<s>
Ins	Ins	k?
</s>
<s>
Home	Home	k6eAd1
</s>
<s>
PgUp	PgUp	k1gMnSc1
</s>
<s>
NumLk	NumLk	k6eAd1
</s>
<s>
/	/	kIx~
</s>
<s>
*	*	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
Del	Del	k?
</s>
<s>
End	End	k?
</s>
<s>
PgDn	PgDn	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
↑	↑	k?
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Ent	Ent	k?
</s>
<s>
←	←	k?
</s>
<s>
↓	↓	k?
</s>
<s>
→	→	k?
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Slovník	slovník	k1gInSc1
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
–	–	k?
tabelátor	tabelátor	k1gInSc1
</s>
