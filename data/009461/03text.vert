<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Velcl	Velcl	k1gMnSc1	Velcl
nebo	nebo	k8xC	nebo
Jan	Jan	k1gMnSc1	Jan
Eskymo	eskymo	k1gNnSc1	eskymo
Welzl	Welzl	k1gMnSc1	Welzl
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1868	[number]	k4	1868
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1948	[number]	k4	1948
Dawson	Dawsona	k1gFnPc2	Dawsona
City	City	k1gFnSc1	City
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
Čech	Čech	k1gMnSc1	Čech
moravské	moravský	k2eAgFnSc2d1	Moravská
národnosti	národnost	k1gFnSc2	národnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
<g/>
,	,	kIx,	,
lovec	lovec	k1gMnSc1	lovec
<g/>
,	,	kIx,	,
zlatokop	zlatokop	k1gMnSc1	zlatokop
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
soudce	soudce	k1gMnSc1	soudce
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Sibiři	Sibiř	k1gFnSc6	Sibiř
a	a	k8xC	a
vypravěč	vypravěč	k1gMnSc1	vypravěč
uvedený	uvedený	k2eAgMnSc1d1	uvedený
jako	jako	k8xS	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Eskymo	eskymo	k1gNnSc1	eskymo
Welzl	Welzl	k1gFnSc2	Welzl
nebo	nebo	k8xC	nebo
také	také	k9	také
Arctic	Arctice	k1gFnPc2	Arctice
Bismarck	Bismarck	k1gMnSc1	Bismarck
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
zámečníkem	zámečník	k1gMnSc7	zámečník
ve	v	k7c6	v
Zvoli	Zvole	k1gFnSc6	Zvole
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tovaryšem	tovaryš	k1gMnSc7	tovaryš
<g/>
,	,	kIx,	,
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
pěšky	pěšky	k6eAd1	pěšky
procestoval	procestovat	k5eAaPmAgMnS	procestovat
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
narukoval	narukovat	k5eAaPmAgMnS	narukovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vojny	vojna	k1gFnSc2	vojna
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Janova	Janov	k1gInSc2	Janov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
místo	místo	k7c2	místo
topiče	topič	k1gInSc2	topič
na	na	k7c6	na
zámořské	zámořský	k2eAgFnSc6d1	zámořská
lodi	loď	k1gFnSc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
dělník	dělník	k1gMnSc1	dělník
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
Port	porta	k1gFnPc2	porta
Arthur	Arthura	k1gFnPc2	Arthura
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Irkutska	Irkutsk	k1gInSc2	Irkutsk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
transsibiřské	transsibiřský	k2eAgFnSc2d1	Transsibiřská
magistrály	magistrála	k1gFnSc2	magistrála
<g/>
,	,	kIx,	,
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
ale	ale	k8xC	ale
stavbu	stavba	k1gFnSc4	stavba
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
putoval	putovat	k5eAaImAgMnS	putovat
pěšky	pěšky	k6eAd1	pěšky
přes	přes	k7c4	přes
Jakutsk	Jakutsk	k1gInSc4	Jakutsk
<g/>
,	,	kIx,	,
Verchojansk	Verchojansk	k1gInSc4	Verchojansk
a	a	k8xC	a
Nižněkolymsk	Nižněkolymsk	k1gInSc4	Nižněkolymsk
na	na	k7c4	na
Novosibiřské	Novosibiřský	k2eAgInPc4d1	Novosibiřský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
údajně	údajně	k6eAd1	údajně
prožil	prožít	k5eAaPmAgMnS	prožít
následujících	následující	k2eAgNnPc2d1	následující
28	[number]	k4	28
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novosibirských	novosibirský	k2eAgInPc6d1	novosibirský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
Welzl	Welzl	k1gFnSc2	Welzl
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
lovec	lovec	k1gMnSc1	lovec
kožešin	kožešina	k1gFnPc2	kožešina
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
neoficiální	oficiální	k2eNgMnSc1d1	oficiální
smírčí	smírčí	k1gMnSc1	smírčí
soudce	soudce	k1gMnSc1	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
soudcovské	soudcovský	k2eAgFnPc1d1	soudcovská
aktivity	aktivita	k1gFnPc1	aktivita
při	při	k7c6	při
potírání	potírání	k1gNnSc6	potírání
kriminality	kriminalita	k1gFnSc2	kriminalita
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
loupeží	loupežit	k5eAaImIp3nS	loupežit
mezi	mezi	k7c7	mezi
lovci	lovec	k1gMnPc7	lovec
kožešin	kožešina	k1gFnPc2	kožešina
a	a	k8xC	a
prodeje	prodej	k1gInSc2	prodej
pančovaného	pančovaný	k2eAgInSc2d1	pančovaný
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
)	)	kIx)	)
však	však	k9	však
byly	být	k5eAaImAgInP	být
spíše	spíše	k9	spíše
vigilantismem	vigilantismus	k1gInSc7	vigilantismus
a	a	k8xC	a
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
dokonce	dokonce	k9	dokonce
lynčování	lynčování	k1gNnSc1	lynčování
provinilců	provinilec	k1gMnPc2	provinilec
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
jeho	jeho	k3xOp3gFnPc2	jeho
obchodních	obchodní	k2eAgFnPc2d1	obchodní
aktivit	aktivita	k1gFnPc2	aktivita
prý	prý	k9	prý
sahala	sahat	k5eAaImAgFnS	sahat
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
až	až	k9	až
po	po	k7c4	po
severní	severní	k2eAgFnSc4d1	severní
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zásoboval	zásobovat	k5eAaImAgInS	zásobovat
lovce	lovec	k1gMnSc4	lovec
<g/>
,	,	kIx,	,
zlatokopy	zlatokop	k1gMnPc4	zlatokop
a	a	k8xC	a
polární	polární	k2eAgMnPc4d1	polární
badatele	badatel	k1gMnPc4	badatel
potravinami	potravina	k1gFnPc7	potravina
<g/>
,	,	kIx,	,
léky	lék	k1gInPc7	lék
<g/>
,	,	kIx,	,
střelivem	střelivo	k1gNnSc7	střelivo
a	a	k8xC	a
dalším	další	k2eAgNnSc7d1	další
potřebným	potřebný	k2eAgNnSc7d1	potřebné
zbožím	zboží	k1gNnSc7	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Rozvážel	rozvážet	k5eAaImAgMnS	rozvážet
s	s	k7c7	s
psím	psí	k2eAgNnSc7d1	psí
spřežením	spřežení	k1gNnSc7	spřežení
také	také	k9	také
poštu	pošta	k1gFnSc4	pošta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
domorodým	domorodý	k2eAgInPc3d1	domorodý
Inuitům	Inuit	k1gInPc3	Inuit
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
zastával	zastávat	k5eAaImAgMnS	zastávat
přátelský	přátelský	k2eAgInSc4d1	přátelský
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
bez	bez	k7c2	bez
předsudků	předsudek	k1gInPc2	předsudek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
sobě	se	k3xPyFc3	se
rovnými	rovný	k2eAgFnPc7d1	rovná
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
Pojídač	pojídač	k1gMnSc1	pojídač
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Welzl	Welznout	k5eAaPmAgMnS	Welznout
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
zvolili	zvolit	k5eAaPmAgMnP	zvolit
náčelníkem	náčelník	k1gInSc7	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
i	i	k9	i
další	další	k2eAgInPc4d1	další
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
kontaktech	kontakt	k1gInPc6	kontakt
s	s	k7c7	s
domorodými	domorodý	k2eAgMnPc7d1	domorodý
obyvateli	obyvatel	k1gMnPc7	obyvatel
dalekého	daleký	k2eAgInSc2d1	daleký
severu	sever	k1gInSc2	sever
je	být	k5eAaImIp3nS	být
však	však	k9	však
obtížné	obtížný	k2eAgNnSc1d1	obtížné
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
Welzl	Welzl	k1gMnSc1	Welzl
inuitsky	inuitsky	k6eAd1	inuitsky
uměl	umět	k5eAaImAgMnS	umět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ukázky	ukázka	k1gFnPc1	ukázka
"	"	kIx"	"
<g/>
eskymáčtiny	eskymáčtina	k1gFnPc1	eskymáčtina
<g/>
"	"	kIx"	"
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
knihách	kniha	k1gFnPc6	kniha
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
lingvistického	lingvistický	k2eAgNnSc2d1	lingvistické
hlediska	hledisko	k1gNnSc2	hledisko
pochybné	pochybný	k2eAgNnSc1d1	pochybné
až	až	k8xS	až
nesmyslné	smyslný	k2eNgNnSc1d1	nesmyslné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
Welzl	Welzl	k1gFnSc2	Welzl
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
u	u	k7c2	u
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
USA	USA	kA	USA
s	s	k7c7	s
lodí	loď	k1gFnSc7	loď
Seven	Seven	k2eAgInSc1d1	Seven
Sisters	Sisters	k1gInSc1	Sisters
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zadržen	zadržet	k5eAaPmNgInS	zadržet
americkými	americký	k2eAgInPc7d1	americký
úřady	úřad	k1gInPc7	úřad
a	a	k8xC	a
vyhoštěn	vyhostit	k5eAaPmNgMnS	vyhostit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
živořil	živořit	k5eAaImAgMnS	živořit
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
poslal	poslat	k5eAaPmAgMnS	poslat
několik	několik	k4yIc4	několik
dopisů	dopis	k1gInPc2	dopis
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
základě	základ	k1gInSc6	základ
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Welzlova	Welzlův	k2eAgFnSc1d1	Welzlova
korespondence	korespondence	k1gFnSc1	korespondence
s	s	k7c7	s
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Těsnohlídkem	Těsnohlídek	k1gMnSc7	Těsnohlídek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
posléze	posléze	k6eAd1	posléze
obsah	obsah	k1gInSc4	obsah
Welzlových	Welzlův	k2eAgInPc2d1	Welzlův
dopisů	dopis	k1gInPc2	dopis
zpracoval	zpracovat	k5eAaPmAgInS	zpracovat
do	do	k7c2	do
knihy	kniha	k1gFnSc2	kniha
Eskymo	eskymo	k1gNnSc1	eskymo
Welzl	Welzl	k1gMnPc2	Welzl
<g/>
,	,	kIx,	,
s	s	k7c7	s
Welzlem	Welzl	k1gInSc7	Welzl
se	se	k3xPyFc4	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nesetkal	setkat	k5eNaPmAgMnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
návštěvě	návštěva	k1gFnSc6	návštěva
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
Welzl	Welzl	k1gMnSc1	Welzl
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	s	k7c7	s
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1928	[number]	k4	1928
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Masarykem	Masaryk	k1gMnSc7	Masaryk
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
rodného	rodný	k2eAgInSc2d1	rodný
Zábřehu	Zábřeh	k1gInSc2	Zábřeh
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
kontaktoval	kontaktovat	k5eAaImAgInS	kontaktovat
redakci	redakce	k1gFnSc4	redakce
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předtím	předtím	k6eAd1	předtím
pracoval	pracovat	k5eAaImAgMnS	pracovat
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
Welzla	Welzla	k1gFnSc6	Welzla
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
redaktoři	redaktor	k1gMnPc1	redaktor
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
a	a	k8xC	a
Bedřich	Bedřich	k1gMnSc1	Bedřich
Golombek	Golombek	k1gMnSc1	Golombek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
pozvali	pozvat	k5eAaPmAgMnP	pozvat
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
Welzl	Welzl	k1gMnSc1	Welzl
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
především	především	k9	především
Valentovi	Valentův	k2eAgMnPc1d1	Valentův
své	svůj	k3xOyFgInPc4	svůj
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
Arktidy	Arktida	k1gFnSc2	Arktida
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
základě	základ	k1gInSc6	základ
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
knihy	kniha	k1gFnPc1	kniha
Třicet	třicet	k4xCc1	třicet
let	léto	k1gNnPc2	léto
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Trampoty	trampota	k1gFnPc1	trampota
eskymáckého	eskymácký	k2eAgMnSc2d1	eskymácký
náčelníka	náčelník	k1gMnSc2	náčelník
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1929	[number]	k4	1929
odjel	odjet	k5eAaPmAgMnS	odjet
přes	přes	k7c4	přes
Hamburk	Hamburk	k1gInSc4	Hamburk
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Dawson	Dawsona	k1gFnPc2	Dawsona
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Toužil	toužit	k5eAaImAgMnS	toužit
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
Novosibiřské	Novosibiřský	k2eAgInPc4d1	Novosibiřský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
nepovolila	povolit	k5eNaPmAgFnS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c4	v
Dawson	Dawson	k1gNnSc4	Dawson
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
jeho	jeho	k3xOp3gFnSc1	jeho
socha	socha	k1gFnSc1	socha
od	od	k7c2	od
Stanislava	Stanislav	k1gMnSc2	Stanislav
Lacha	Lach	k1gMnSc2	Lach
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nejdříve	dříve	k6eAd3	dříve
zaujímala	zaujímat	k5eAaImAgFnS	zaujímat
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
u	u	k7c2	u
III	III	kA	III
<g/>
.	.	kIx.	.
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Zábřehu	Zábřeh	k1gInSc6	Zábřeh
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
na	na	k7c4	na
vlakové	vlakový	k2eAgNnSc4d1	vlakové
nádraží	nádraží	k1gNnSc4	nádraží
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
zdejším	zdejší	k2eAgNnSc6d1	zdejší
muzeu	muzeum	k1gNnSc6	muzeum
je	být	k5eAaImIp3nS	být
Welzlovi	Welzl	k1gMnSc3	Welzl
věnována	věnován	k2eAgFnSc1d1	věnována
místnost	místnost	k1gFnSc1	místnost
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
artefakty	artefakt	k1gInPc7	artefakt
<g/>
,	,	kIx,	,
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
v	v	k7c6	v
Zábřehu	Zábřeh	k1gInSc6	Zábřeh
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
i	i	k8xC	i
ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umělecký	umělecký	k2eAgInSc4d1	umělecký
význam	význam	k1gInSc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
<g/>
,	,	kIx,	,
rekonstruovaná	rekonstruovaný	k2eAgNnPc1d1	rekonstruované
na	na	k7c6	na
základě	základ	k1gInSc6	základ
korespondence	korespondence	k1gFnSc2	korespondence
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
jako	jako	k8xS	jako
první	první	k4xOgFnPc1	první
pod	pod	k7c7	pod
titulem	titul	k1gInSc7	titul
Eskymo	eskymo	k1gNnSc1	eskymo
Welzl	Welzl	k1gFnSc2	Welzl
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Welzl	Welznout	k5eAaPmAgMnS	Welznout
a	a	k8xC	a
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
se	se	k3xPyFc4	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
naživo	naživo	k1gNnSc4	naživo
nesetkali	setkat	k5eNaPmAgMnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
další	další	k2eAgNnPc4d1	další
pokračování	pokračování	k1gNnPc4	pokračování
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Těsnohlídka	Těsnohlídka	k1gFnSc1	Těsnohlídka
pokusil	pokusit	k5eAaPmAgMnS	pokusit
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
práci	práce	k1gFnSc4	práce
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
dílo	dílo	k1gNnSc1	dílo
dotáhli	dotáhnout	k5eAaPmAgMnP	dotáhnout
do	do	k7c2	do
konce	konec	k1gInSc2	konec
Bedřich	Bedřich	k1gMnSc1	Bedřich
Golombek	Golombek	k1gMnSc1	Golombek
a	a	k8xC	a
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
s	s	k7c7	s
Welzlem	Welzl	k1gInSc7	Welzl
osobně	osobně	k6eAd1	osobně
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
severu	sever	k1gInSc6	sever
měla	mít	k5eAaImAgFnS	mít
nesmírný	smírný	k2eNgInSc4d1	nesmírný
úspěch	úspěch	k1gInSc4	úspěch
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokonce	dokonce	k9	dokonce
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
podezření	podezření	k1gNnSc3	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k1gMnSc1	žádný
Eskymo	eskymo	k1gNnSc1	eskymo
Welzl	Welzl	k1gMnSc1	Welzl
neexistuje	existovat	k5eNaImIp3nS	existovat
a	a	k8xC	a
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
napsal	napsat	k5eAaBmAgMnS	napsat
k	k	k7c3	k
zahraničnímu	zahraniční	k2eAgInSc3d1	zahraniční
vydání	vydání	k1gNnSc2	vydání
předmluvu	předmluva	k1gFnSc4	předmluva
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
mimořádně	mimořádně	k6eAd1	mimořádně
živě	živě	k6eAd1	živě
a	a	k8xC	a
poutavě	poutavě	k6eAd1	poutavě
napsané	napsaný	k2eAgFnPc4d1	napsaná
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
cesty	cesta	k1gFnPc4	cesta
po	po	k7c6	po
Sibiři	Sibiř	k1gFnSc6	Sibiř
a	a	k8xC	a
autorovo	autorův	k2eAgNnSc4d1	autorovo
působení	působení	k1gNnSc4	působení
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
však	však	k9	však
závažné	závažný	k2eAgFnPc4d1	závažná
nepřesnosti	nepřesnost	k1gFnPc4	nepřesnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
etnologie	etnologie	k1gFnSc2	etnologie
a	a	k8xC	a
zoologie	zoologie	k1gFnSc2	zoologie
<g/>
.	.	kIx.	.
</s>
<s>
Nejzávažnějším	závažný	k2eAgInSc7d3	nejzávažnější
omylem	omyl	k1gInSc7	omyl
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
Welzlovo	Welzlův	k2eAgNnSc1d1	Welzlovo
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Novosibiřských	Novosibiřský	k2eAgInPc6d1	Novosibiřský
ostrovech	ostrov	k1gInPc6	ostrov
žili	žít	k5eAaImAgMnP	žít
Eskymáci	Eskymák	k1gMnPc1	Eskymák
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
sousostroví	sousostrový	k2eAgMnPc1d1	sousostrový
původní	původní	k2eAgMnSc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
nežili	žít	k5eNaImAgMnP	žít
a	a	k8xC	a
veškeré	veškerý	k3xTgNnSc4	veškerý
osídlení	osídlení	k1gNnSc4	osídlení
tvořilo	tvořit	k5eAaImAgNnS	tvořit
několik	několik	k4yIc1	několik
ruských	ruský	k2eAgMnPc2d1	ruský
lovců	lovec	k1gMnPc2	lovec
<g/>
,	,	kIx,	,
rybářů	rybář	k1gMnPc2	rybář
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
měli	mít	k5eAaImAgMnP	mít
jakutské	jakutský	k2eAgMnPc4d1	jakutský
pomocníky	pomocník	k1gMnPc4	pomocník
<g/>
.	.	kIx.	.
</s>
<s>
Welzl	Welznout	k5eAaPmAgMnS	Welznout
však	však	k9	však
podrobně	podrobně	k6eAd1	podrobně
popsal	popsat	k5eAaPmAgMnS	popsat
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
zvyky	zvyk	k1gInPc4	zvyk
novosibiřských	novosibiřský	k2eAgInPc2d1	novosibiřský
Eskymámků	Eskymámek	k1gInPc2	Eskymámek
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
ukázky	ukázka	k1gFnPc4	ukázka
jejich	jejich	k3xOp3gInSc2	jejich
údajného	údajný	k2eAgInSc2d1	údajný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
jednoznačně	jednoznačně	k6eAd1	jednoznačně
smyšlené	smyšlený	k2eAgNnSc1d1	smyšlené
a	a	k8xC	a
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
autor	autor	k1gMnSc1	autor
žádný	žádný	k1gMnSc1	žádný
z	z	k7c2	z
eskymáckých	eskymácký	k2eAgInPc2d1	eskymácký
dialektů	dialekt	k1gInPc2	dialekt
neovládal	ovládat	k5eNaImAgInS	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
rovněž	rovněž	k9	rovněž
popisuje	popisovat	k5eAaImIp3nS	popisovat
nesmyslné	smyslný	k2eNgFnPc4d1	nesmyslná
metody	metoda	k1gFnPc4	metoda
lovu	lov	k1gInSc2	lov
velryb	velryba	k1gFnPc2	velryba
nebo	nebo	k8xC	nebo
fantastická	fantastický	k2eAgNnPc1d1	fantastické
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
"	"	kIx"	"
<g/>
Ďáblovy	ďáblův	k2eAgFnSc2d1	Ďáblova
ryby	ryba	k1gFnSc2	ryba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
masožravé	masožravý	k2eAgFnPc1d1	masožravá
nestvůry	nestvůra	k1gFnPc1	nestvůra
<g/>
,	,	kIx,	,
schopné	schopný	k2eAgFnPc1d1	schopná
vylézt	vylézt	k5eAaPmF	vylézt
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
měl	mít	k5eAaImAgInS	mít
Welzl	Welzl	k1gFnSc4	Welzl
usmrtit	usmrtit	k5eAaPmF	usmrtit
pomocí	pomocí	k7c2	pomocí
dynamitu	dynamit	k1gInSc2	dynamit
<g/>
.	.	kIx.	.
</s>
<s>
Welzl	Welznout	k5eAaPmAgMnS	Welznout
o	o	k7c4	o
sobě	se	k3xPyFc3	se
rovněž	rovněž	k9	rovněž
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
povedlo	povést	k5eAaPmAgNnS	povést
sestrojit	sestrojit	k5eAaPmF	sestrojit
perpetuum	perpetuum	k1gNnSc4	perpetuum
mobile	mobile	k1gNnSc4	mobile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkým	velký	k2eAgMnSc7d1	velký
kritikem	kritik	k1gMnSc7	kritik
Welzlova	Welzlův	k2eAgNnSc2d1	Welzlovo
díla	dílo	k1gNnSc2	dílo
byl	být	k5eAaImAgMnS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
polární	polární	k2eAgMnSc1d1	polární
badatel	badatel	k1gMnSc1	badatel
islandského	islandský	k2eAgInSc2d1	islandský
původu	původ	k1gInSc2	původ
Vilhjalmur	Vilhjalmur	k1gMnSc1	Vilhjalmur
Stefansson	Stefansson	k1gMnSc1	Stefansson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Welzla	Welzla	k1gMnSc1	Welzla
obviňoval	obviňovat	k5eAaImAgMnS	obviňovat
ze	z	k7c2	z
lhaní	lhaní	k1gNnSc2	lhaní
a	a	k8xC	a
falšování	falšování	k1gNnSc2	falšování
údajů	údaj	k1gInPc2	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Welzl	Welznout	k5eAaPmAgInS	Welznout
se	se	k3xPyFc4	se
většiny	většina	k1gFnSc2	většina
nepřesností	nepřesnost	k1gFnPc2	nepřesnost
patrně	patrně	k6eAd1	patrně
nedopustil	dopustit	k5eNaPmAgMnS	dopustit
úmyslně	úmyslně	k6eAd1	úmyslně
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
nerozlišoval	rozlišovat	k5eNaImAgMnS	rozlišovat
mezi	mezi	k7c7	mezi
vlastními	vlastní	k2eAgInPc7d1	vlastní
zážitky	zážitek	k1gInPc7	zážitek
a	a	k8xC	a
zprávami	zpráva	k1gFnPc7	zpráva
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
nepřesnosti	nepřesnost	k1gFnPc1	nepřesnost
a	a	k8xC	a
omyly	omyl	k1gInPc1	omyl
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
během	během	k7c2	během
úprav	úprava	k1gFnPc2	úprava
a	a	k8xC	a
přepisů	přepis	k1gInPc2	přepis
jeho	jeho	k3xOp3gNnSc4	jeho
vyprávění	vyprávění	k1gNnSc4	vyprávění
a	a	k8xC	a
dopisů	dopis	k1gInPc2	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
zapomínat	zapomínat	k5eAaImF	zapomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečnými	skutečný	k2eAgMnPc7d1	skutečný
autory	autor	k1gMnPc7	autor
Třiceti	třicet	k4xCc3	třicet
let	léto	k1gNnPc2	léto
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
severu	sever	k1gInSc6	sever
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
"	"	kIx"	"
<g/>
Welzlových	Welzlová	k1gFnPc6	Welzlová
<g/>
"	"	kIx"	"
knih	kniha	k1gFnPc2	kniha
jsou	být	k5eAaImIp3nP	být
B.	B.	kA	B.
Golombek	Golombek	k1gInSc4	Golombek
a	a	k8xC	a
E.	E.	kA	E.
Valenta	Valenta	k1gMnSc1	Valenta
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
R.	R.	kA	R.
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jim	on	k3xPp3gMnPc3	on
vtiskli	vtisknout	k5eAaPmAgMnP	vtisknout
svou	svůj	k3xOyFgFnSc4	svůj
autorskou	autorský	k2eAgFnSc4d1	autorská
invenci	invence	k1gFnSc4	invence
a	a	k8xC	a
četná	četný	k2eAgNnPc4d1	četné
fakta	faktum	k1gNnPc4	faktum
značně	značně	k6eAd1	značně
upravili	upravit	k5eAaPmAgMnP	upravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Welzlovo	Welzlův	k2eAgNnSc1d1	Welzlovo
dílo	dílo	k1gNnSc1	dílo
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
také	také	k6eAd1	také
Svěráka	Svěrák	k1gMnSc2	Svěrák
a	a	k8xC	a
Smoljaka	Smoljak	k1gMnSc2	Smoljak
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
postavy	postava	k1gFnSc2	postava
všeumělce	všeumělec	k1gMnSc2	všeumělec
<g/>
,	,	kIx,	,
legendárního	legendární	k2eAgMnSc2d1	legendární
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k6eAd1	vedle
Těsnohlídka	Těsnohlídka	k1gFnSc1	Těsnohlídka
<g/>
,	,	kIx,	,
Golombka	Golombka	k1gFnSc1	Golombka
a	a	k8xC	a
Valenty	Valenta	k1gMnPc4	Valenta
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
Welzlovým	Welzlův	k2eAgNnSc7d1	Welzlovo
jménem	jméno	k1gNnSc7	jméno
spojeni	spojit	k5eAaPmNgMnP	spojit
také	také	k9	také
bratři	bratr	k1gMnPc1	bratr
Čapkové	Čapkové	k2eAgMnPc1d1	Čapkové
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
první	první	k4xOgMnPc1	první
vydání	vydání	k1gNnSc6	vydání
Třiceti	třicet	k4xCc2	třicet
let	léto	k1gNnPc2	léto
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
severu	sever	k1gInSc6	sever
opatřili	opatřit	k5eAaPmAgMnP	opatřit
předmluvou	předmluva	k1gFnSc7	předmluva
<g/>
.	.	kIx.	.
</s>
<s>
Američany	Američan	k1gMnPc4	Američan
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Welzl	Welzl	k1gFnSc2	Welzl
uchvátil	uchvátit	k5eAaPmAgMnS	uchvátit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
kniha	kniha	k1gFnSc1	kniha
Třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
severu	sever	k1gInSc6	sever
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
peněžní	peněžní	k2eAgInSc4d1	peněžní
ocenění	ocenění	k1gNnSc1	ocenění
10	[number]	k4	10
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
J.	J.	kA	J.
E.	E.	kA	E.
Welzl	Welzl	k1gMnSc7	Welzl
ale	ale	k8xC	ale
ani	ani	k8xC	ani
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Ohlasy	ohlas	k1gInPc1	ohlas
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
uvedlo	uvést	k5eAaPmAgNnS	uvést
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1976	[number]	k4	1976
podle	podle	k7c2	podle
života	život	k1gInSc2	život
J.	J.	kA	J.
E.	E.	kA	E.
Welzla	Welzla	k1gMnSc2	Welzla
premiéru	premiéra	k1gFnSc4	premiéra
inscenace	inscenace	k1gFnSc2	inscenace
Velký	velký	k2eAgInSc1d1	velký
vandr	vandr	k1gInSc1	vandr
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Peter	Peter	k1gMnSc1	Peter
Scherhaufer	Scherhaufer	k1gMnSc1	Scherhaufer
</s>
</p>
<p>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Požírač	požírač	k1gMnSc1	požírač
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
laskavá	laskavý	k2eAgFnSc1d1	laskavá
komedie	komedie	k1gFnSc1	komedie
ze	z	k7c2	z
života	život	k1gInSc2	život
polárníka	polárník	k1gMnSc2	polárník
<g/>
,	,	kIx,	,
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
pábitele	pábitel	k1gMnSc2	pábitel
J.	J.	kA	J.
E.	E.	kA	E.
Welzla	Welzla	k1gMnSc1	Welzla
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Satinský	Satinský	k2eAgMnSc1d1	Satinský
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Javorský	Javorský	k1gMnSc1	Javorský
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Polívka	polívka	k1gFnSc1	polívka
<g/>
,	,	kIx,	,
D.	D.	kA	D.
Kaplanová	Kaplanová	k1gFnSc1	Kaplanová
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Lasica	Lasica	k1gFnSc1	Lasica
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Kamera	kamera	k1gFnSc1	kamera
V.	V.	kA	V.
Myslivec	Myslivec	k1gMnSc1	Myslivec
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
P.	P.	kA	P.
Jandourek	Jandourka	k1gFnPc2	Jandourka
<g/>
.	.	kIx.	.
</s>
<s>
Odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c6	na
ČSFD	ČSFD	kA	ČSFD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
15425	[number]	k4	15425
objevená	objevený	k2eAgFnSc1d1	objevená
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
aneb	aneb	k?	aneb
Perpetuum	perpetuum	k1gNnSc1	perpetuum
mobile	mobile	k1gNnSc2	mobile
Eskymo	eskymo	k1gNnSc1	eskymo
Welzl	Welzl	k1gFnSc2	Welzl
–	–	k?	–
koprodukční	koprodukční	k2eAgInSc4d1	koprodukční
dokument	dokument	k1gInSc4	dokument
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
a	a	k8xC	a
ms	ms	k?	ms
video	video	k1gNnSc1	video
Martin	Martin	k1gMnSc1	Martin
Strouhal	Strouhal	k1gMnSc1	Strouhal
o	o	k7c6	o
Janu	Jan	k1gMnSc6	Jan
Welzlovi	Welzl	k1gMnSc6	Welzl
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Šumperk	Šumperk	k1gInSc1	Šumperk
uvedlo	uvést	k5eAaPmAgNnS	uvést
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
premiéru	premiéra	k1gFnSc4	premiéra
hry	hra	k1gFnSc2	hra
Eskymo	eskymo	k1gNnSc4	eskymo
je	být	k5eAaImIp3nS	být
Welzl	Welzl	k1gMnSc1	Welzl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dejvické	dejvický	k2eAgNnSc1d1	Dejvické
divadlo	divadlo	k1gNnSc1	divadlo
uvedlo	uvést	k5eAaPmAgNnS	uvést
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
premiéru	premiéra	k1gFnSc4	premiéra
hry	hra	k1gFnSc2	hra
Wanted	Wanted	k1gMnSc1	Wanted
Welzl	Welzl	k1gMnSc1	Welzl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plzeňská	plzeňská	k1gFnSc1	plzeňská
filharmonie	filharmonie	k1gFnSc2	filharmonie
uvádí	uvádět	k5eAaImIp3nS	uvádět
interaktivní	interaktivní	k2eAgInSc1d1	interaktivní
hudebně-zábavný	hudebněábavný	k2eAgInSc1d1	hudebně-zábavný
pořad	pořad	k1gInSc1	pořad
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
s	s	k7c7	s
názvem	název	k1gInSc7	název
Jsem	být	k5eAaImIp1nS	být
Mojook	Mojook	k1gInSc1	Mojook
Ojaak	Ojaak	k1gInSc1	Ojaak
<g/>
,	,	kIx,	,
Pojídač	pojídač	k1gMnSc1	pojídač
medvědů	medvěd	k1gMnPc2	medvěd
popisující	popisující	k2eAgInSc4d1	popisující
životní	životní	k2eAgInSc4d1	životní
příběh	příběh	k1gInSc4	příběh
Jan	Jan	k1gMnSc1	Jan
Welzla	Welzla	k1gMnSc1	Welzla
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc4	scénář
připravil	připravit	k5eAaPmAgMnS	připravit
Martin	Martin	k1gMnSc1	Martin
Pášma	Pášma	k1gNnSc4	Pášma
a	a	k8xC	a
hudbu	hudba	k1gFnSc4	hudba
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
Tomáš	Tomáš	k1gMnSc1	Tomáš
Ille	Ill	k1gInSc2	Ill
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
podle	podle	k7c2	podle
Welzlových	Welzlův	k2eAgInPc2d1	Welzlův
zápisků	zápisek	k1gInPc2	zápisek
a	a	k8xC	a
dopisů	dopis	k1gInPc2	dopis
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
<g/>
:	:	kIx,	:
Paměti	paměť	k1gFnPc1	paměť
českého	český	k2eAgMnSc2d1	český
polárního	polární	k2eAgMnSc2d1	polární
lovce	lovec	k1gMnSc2	lovec
a	a	k8xC	a
zlatokopa	zlatokop	k1gMnSc2	zlatokop
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Golombek	Golombek	k1gMnSc1	Golombek
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyprávění	vyprávění	k1gNnSc2	vyprávění
Jana	Jan	k1gMnSc2	Jan
Welzla	Welzla	k1gMnSc2	Welzla
<g/>
:	:	kIx,	:
Třicet	třicet	k4xCc1	třicet
let	léto	k1gNnPc2	léto
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Golombek	Golombek	k1gMnSc1	Golombek
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyprávění	vyprávění	k1gNnSc2	vyprávění
Jana	Jan	k1gMnSc2	Jan
Welzla	Welzla	k1gMnSc2	Welzla
<g/>
:	:	kIx,	:
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
polárních	polární	k2eAgInPc2d1	polární
pokladů	poklad	k1gInPc2	poklad
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Golombek	Golombek	k1gMnSc1	Golombek
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
<g/>
:	:	kIx,	:
Trampoty	trampota	k1gFnPc1	trampota
eskymáckého	eskymácký	k2eAgMnSc2d1	eskymácký
náčelníka	náčelník	k1gMnSc2	náčelník
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Golombek	Golombek	k1gMnSc1	Golombek
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyprávění	vyprávění	k1gNnSc2	vyprávění
Jana	Jan	k1gMnSc2	Jan
Welzla	Welzla	k1gMnSc2	Welzla
<g/>
:	:	kIx,	:
Ledové	ledový	k2eAgFnSc2d1	ledová
povídky	povídka	k1gFnSc2	povídka
Eskymo	eskymo	k1gNnSc1	eskymo
Welzla	Welzla	k1gMnPc2	Welzla
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1898	[number]	k4	1898
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Welzl	Welzl	k1gFnSc2	Welzl
ISBN	ISBN	kA	ISBN
80-7185-132-9	[number]	k4	80-7185-132-9
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Sís	Sís	k1gMnSc1	Sís
<g/>
,	,	kIx,	,
Podivuhodný	podivuhodný	k2eAgInSc1d1	podivuhodný
příběh	příběh	k1gInSc1	příběh
Eskymo	eskymo	k1gNnSc1	eskymo
Welzla	Welzla	k1gFnPc2	Welzla
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7185-022-5	[number]	k4	80-7185-022-5
</s>
</p>
<p>
<s>
Svatava	Svatava	k1gFnSc1	Svatava
Morávková	Morávková	k1gFnSc1	Morávková
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
Čtení	čtení	k1gNnSc1	čtení
o	o	k7c6	o
neobyčejných	obyčejný	k2eNgFnPc6d1	neobyčejná
cestách	cesta	k1gFnPc6	cesta
Jana	Jan	k1gMnSc2	Jan
Eskymo	eskymo	k1gNnSc1	eskymo
Welzla	Welzla	k1gMnSc1	Welzla
<g/>
.	.	kIx.	.
</s>
<s>
Nakl	Nakl	k1gMnSc1	Nakl
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7185-628-2	[number]	k4	80-7185-628-2
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Welzl	Welzl	k1gMnSc1	Welzl
<g/>
,	,	kIx,	,
Hrdinové	Hrdina	k1gMnPc1	Hrdina
ledového	ledový	k2eAgNnSc2d1	ledové
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86362-42-6	[number]	k4	80-86362-42-6
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vecka	Vecka	k1gMnSc1	Vecka
<g/>
,	,	kIx,	,
Tulák	tulák	k1gMnSc1	tulák
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
trampa	tramp	k1gMnSc2	tramp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
hledači	hledač	k1gInSc6	hledač
absolutní	absolutní	k2eAgFnSc2d1	absolutní
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86362-78-7	[number]	k4	80-86362-78-7
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Strouhal	Strouhal	k1gMnSc1	Strouhal
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
–	–	k?	–
Příběhy	příběh	k1gInPc1	příběh
a	a	k8xC	a
záhady	záhada	k1gFnPc1	záhada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
zanechal	zanechat	k5eAaPmAgMnS	zanechat
největší	veliký	k2eAgMnSc1d3	veliký
český	český	k2eAgMnSc1d1	český
polárník	polárník	k1gMnSc1	polárník
Jan	Jan	k1gMnSc1	Jan
Eskymo	eskymo	k1gNnSc4	eskymo
Welzl	Welzl	k1gMnSc2	Welzl
I.	I.	kA	I.
vydání	vydání	k1gNnSc6	vydání
2009	[number]	k4	2009
ISBN	ISBN	kA	ISBN
978-80-86438-28-3	[number]	k4	978-80-86438-28-3
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Strouhal	Strouhal	k1gMnSc1	Strouhal
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
–	–	k?	–
Příběhy	příběh	k1gInPc1	příběh
a	a	k8xC	a
záhady	záhada	k1gFnPc1	záhada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
zanechal	zanechat	k5eAaPmAgMnS	zanechat
největší	veliký	k2eAgMnSc1d3	veliký
český	český	k2eAgMnSc1d1	český
polárník	polárník	k1gMnSc1	polárník
Jan	Jan	k1gMnSc1	Jan
Eskymo	eskymo	k1gNnSc4	eskymo
Welzl	Welzl	k1gMnSc2	Welzl
II	II	kA	II
<g/>
.	.	kIx.	.
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
a	a	k8xC	a
upravené	upravený	k2eAgNnSc1d1	upravené
vydání	vydání	k1gNnSc1	vydání
2014	[number]	k4	2014
ISBN	ISBN	kA	ISBN
978-80-905367-2-2	[number]	k4	978-80-905367-2-2
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jan	Jan	k1gMnSc1	Jan
Eskymo	eskymo	k1gNnSc4	eskymo
Welzl	Welzl	k1gMnSc2	Welzl
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Eskymo	eskymo	k1gNnSc4	eskymo
Welzl	Welzl	k1gMnSc2	Welzl
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
Welzlovi	Welzl	k1gMnSc6	Welzl
</s>
</p>
<p>
<s>
Dokumenty	dokument	k1gInPc1	dokument
o	o	k7c4	o
Janu	Jana	k1gFnSc4	Jana
Eskymo	eskymo	k1gNnSc1	eskymo
Welzlovi	Welzl	k1gMnSc6	Welzl
</s>
</p>
<p>
<s>
Welzl	Welznout	k5eAaPmAgMnS	Welznout
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
planetka	planetka	k1gFnSc1	planetka
</s>
</p>
<p>
<s>
J.	J.	kA	J.
E.	E.	kA	E.
Welzl	Welzl	k1gMnSc1	Welzl
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Toulavá	toulavý	k2eAgFnSc1d1	Toulavá
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
iVysílání	iVysílání	k1gNnSc1	iVysílání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Procházka	Procházka	k1gMnSc1	Procházka
rodným	rodný	k2eAgInSc7d1	rodný
městem	město	k1gNnSc7	město
</s>
</p>
