<s>
Lament	Lament	k?
(	(	kIx(
<g/>
báseň	báseň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	kIx~
<g/>
Lament	Lament	k1gInSc1
<g/>
“	“	kIx~
je	být	k5eAaImIp3nS
báseň	báseň	k1gFnSc1
velšského	velšský	k2eAgMnSc2d1
autora	autor	k1gMnSc2
Dylana	Dylan	k1gMnSc2
Thomase	Thomas	k1gMnSc2xF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
jí	on	k3xPp3gFnSc3
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
domě	dům	k1gInSc6
v	v	k7c6
jihovelšském	jihovelšský	k2eAgInSc6d1
Laugharne	Laugharn	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Velšský	velšský	k2eAgMnSc1d1
hudebník	hudebník	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
John	John	k1gMnSc1
Cale	Cale	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
zhudebnil	zhudebnit	k5eAaPmAgMnS
již	již	k9
několik	několik	k4yIc4
Thomasových	Thomasův	k2eAgFnPc2d1
básní	báseň	k1gFnPc2
<g/>
,	,	kIx,
zhudebnil	zhudebnit	k5eAaPmAgMnS
tuto	tento	k3xDgFnSc4
báseň	báseň	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tom	ten	k3xDgInSc6
roce	rok	k1gInSc6
jí	jíst	k5eAaImIp3nS
hrál	hrát	k5eAaImAgMnS
při	při	k7c6
koncertech	koncert	k1gInPc6
se	s	k7c7
skupinou	skupina	k1gFnSc7
The	The	k1gFnSc1
Creatures	Creatures	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
jí	on	k3xPp3gFnSc3
představil	představit	k5eAaPmAgMnS
například	například	k6eAd1
při	při	k7c6
koncertu	koncert	k1gInSc6
v	v	k7c6
Edinburghu	Edinburgh	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
toho	ten	k3xDgInSc2
dne	den	k1gInSc2
představil	představit	k5eAaPmAgInS
také	také	k9
dvě	dva	k4xCgFnPc4
další	další	k2eAgFnPc4d1
zhudebněné	zhudebněný	k2eAgFnPc4d1
Thomasovy	Thomasův	k2eAgFnPc4d1
básně	báseň	k1gFnPc4
<g/>
:	:	kIx,
„	„	k?
<g/>
If	If	k1gFnSc1
I	i	k8xC
Were	Were	k1gFnSc1
Tickled	Tickled	k1gInSc1
by	by	kYmCp3nS
the	the	k?
Rub	roubit	k5eAaImRp2nS
of	of	k?
Love	lov	k1gInSc5
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
In	In	k1gFnSc1
My	my	k3xPp1nPc1
Craft	Craft	k2eAgInSc4d1
or	or	k?
Sullen	Sullen	k1gInSc4
Art	Art	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
jí	jíst	k5eAaImIp3nS
hrál	hrát	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.dylanthomas.com/dylan/1950s-dylans-death/	http://www.dylanthomas.com/dylan/1950s-dylans-death/	k4
<g/>
↑	↑	k?
http://articles.mcall.com/1998-08-08/entertainment/3213654_1_john-cale-velvet-underground-black	http://articles.mcall.com/1998-08-08/entertainment/3213654_1_john-cale-velvet-underground-black	k1gMnSc1
<g/>
↑	↑	k?
MITCHELL	MITCHELL	kA
<g/>
,	,	kIx,
Tim	Tim	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedition	Sedition	k1gInSc1
and	and	k?
Alchemy	Alchema	k1gFnSc2
<g/>
:	:	kIx,
A	a	k9
Biography	Biographa	k1gMnSc2
of	of	k?
John	John	k1gMnSc1
Cale	Cal	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrace	ilustrace	k1gFnSc1
Dave	Dav	k1gInSc5
McKean	McKeany	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Peter	Peter	k1gMnSc1
Owen	Owen	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
239	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
7206	#num#	k4
11326	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
200	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Informace	informace	k1gFnPc1
o	o	k7c6
básni	báseň	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
