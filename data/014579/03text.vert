<s>
Paralelní	paralelní	k2eAgFnSc1d1
polis	polis	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
konceptu	koncept	k1gInSc6
paralelní	paralelní	k2eAgFnSc2d1
polis	polis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
projektu	projekt	k1gInSc6
Paralelní	paralelní	k2eAgFnSc2d1
polis	polis	k1gFnSc2
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Ztohoven	Ztohoven	k2eAgInSc1d1
<g/>
#	#	kIx~
<g/>
Paralelní_polis	Paralelní_polis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Politologicko-společenský	politologicko-společenský	k2eAgInSc1d1
koncept	koncept	k1gInSc1
paralelní	paralelní	k2eAgFnSc1d1
polis	polis	k1gFnSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
českým	český	k2eAgMnSc7d1
disidentem	disident	k1gMnSc7
a	a	k8xC
politickým	politický	k2eAgMnSc7d1
myslitelem	myslitel	k1gMnSc7
Václavem	Václav	k1gMnSc7
Bendou	Benda	k1gMnSc7
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
hnutím	hnutí	k1gNnSc7
Charta	charta	k1gFnSc1
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k6eAd1
dalších	další	k2eAgMnPc2d1
angažovaných	angažovaný	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
se	se	k3xPyFc4
i	i	k9
Benda	Benda	k1gMnSc1
snažil	snažit	k5eAaImAgMnS
o	o	k7c4
teoretické	teoretický	k2eAgNnSc4d1
uchopení	uchopení	k1gNnSc4
společenského	společenský	k2eAgNnSc2d1
dění	dění	k1gNnSc2
v	v	k7c6
kruzích	kruh	k1gInPc6
disentu	disent	k1gInSc2
–	–	k?
tzv.	tzv.	kA
druhé	druhý	k4xOgFnSc2
kultury	kultura	k1gFnSc2
neboli	neboli	k8xC
undergroundu	underground	k1gInSc2
–	–	k?
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
poprvé	poprvé	k6eAd1
popsal	popsat	k5eAaPmAgMnS
v	v	k7c6
textu	text	k1gInSc6
Paralelní	paralelní	k2eAgFnSc2d1
polis	polis	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Benda	Benda	k1gMnSc1
si	se	k3xPyFc3
jako	jako	k9
politolog	politolog	k1gMnSc1
všiml	všimnout	k5eAaPmAgMnS
organického	organický	k2eAgNnSc2d1
vznikání	vznikání	k1gNnSc2
nové	nový	k2eAgFnSc2d1
společenské	společenský	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
rodit	rodit	k5eAaImF
v	v	k7c6
uměleckých	umělecký	k2eAgInPc6d1
a	a	k8xC
intelektuálních	intelektuální	k2eAgInPc6d1
kruzích	kruh	k1gInPc6
jako	jako	k8xS,k8xC
nástroj	nástroj	k1gInSc1
vymanění	vymanění	k1gNnSc2
se	se	k3xPyFc4
z	z	k7c2
totalitního	totalitní	k2eAgInSc2d1
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpoznal	rozpoznat	k5eAaPmAgInS
následující	následující	k2eAgInPc4d1
základní	základní	k2eAgInPc4d1
pilíře	pilíř	k1gInPc4
této	tento	k3xDgFnSc2
nové	nový	k2eAgFnSc2d1
„	„	k?
<g/>
obce	obec	k1gFnSc2
<g/>
“	“	k?
<g/>
:	:	kIx,
</s>
<s>
neustálé	neustálý	k2eAgNnSc4d1
hlídání	hlídání	k1gNnSc4
a	a	k8xC
ověřování	ověřování	k1gNnSc4
míry	míra	k1gFnSc2
občanských	občanský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
svobod	svoboda	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
má	mít	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
tendenci	tendence	k1gFnSc4
omezovat	omezovat	k5eAaImF
–	–	k?
paralelní	paralelní	k2eAgInSc4d1
polis	polis	k1gInSc4
tvoří	tvořit	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
zasazují	zasazovat	k5eAaImIp3nP
za	za	k7c4
svá	svůj	k3xOyFgNnPc4
práva	právo	k1gNnPc4
a	a	k8xC
chrání	chránit	k5eAaImIp3nS
je	on	k3xPp3gMnPc4
<g/>
,	,	kIx,
</s>
<s>
paralelní	paralelní	k2eAgInSc1d1
„	„	k?
<g/>
druhá	druhý	k4xOgFnSc1
<g/>
“	“	k?
kultura	kultura	k1gFnSc1
–	–	k?
underground	underground	k1gInSc1
<g/>
,	,	kIx,
nezávislá	závislý	k2eNgFnSc1d1
kultura	kultura	k1gFnSc1
a	a	k8xC
různé	různý	k2eAgFnPc1d1
sféry	sféra	k1gFnPc1
umění	umění	k1gNnSc2
jsou	být	k5eAaImIp3nP
provozovány	provozovat	k5eAaImNgFnP
a	a	k8xC
rozvíjeny	rozvíjet	k5eAaImNgFnP
bez	bez	k7c2
povolení	povolení	k1gNnSc2
a	a	k8xC
podpory	podpora	k1gFnSc2
ze	z	k7c2
strany	strana	k1gFnSc2
veřejné	veřejný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
paralelní	paralelní	k2eAgNnSc1d1
školství	školství	k1gNnSc1
a	a	k8xC
věda	věda	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
představují	představovat	k5eAaImIp3nP
realizaci	realizace	k1gFnSc4
práva	právo	k1gNnSc2
na	na	k7c4
svobodné	svobodný	k2eAgNnSc4d1
vzdělávání	vzdělávání	k1gNnSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
vědeckého	vědecký	k2eAgNnSc2d1
bádání	bádání	k1gNnSc2
(	(	kIx(
<g/>
bytové	bytový	k2eAgInPc4d1
semináře	seminář	k1gInPc4
<g/>
,	,	kIx,
různé	různý	k2eAgInPc4d1
vzdělávací	vzdělávací	k2eAgInPc4d1
spolky	spolek	k1gInPc4
<g/>
,	,	kIx,
akademie	akademie	k1gFnPc4
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
paralelní	paralelní	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
jako	jako	k8xC,k8xS
výraz	výraz	k1gInSc1
práva	právo	k1gNnSc2
na	na	k7c4
svobodné	svobodný	k2eAgNnSc4d1
šíření	šíření	k1gNnSc4
informací	informace	k1gFnPc2
(	(	kIx(
<g/>
samizdatové	samizdatový	k2eAgNnSc1d1
vydávání	vydávání	k1gNnSc1
tiskovin	tiskovina	k1gFnPc2
<g/>
,	,	kIx,
neoficiální	neoficiální	k2eAgInPc1d1,k2eNgInPc1d1
časopisy	časopis	k1gInPc1
<g/>
,	,	kIx,
sborníky	sborník	k1gInPc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
paralelní	paralelní	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Politická	politický	k2eAgFnSc1d1
moc	moc	k1gFnSc1
pokládá	pokládat	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
oblast	oblast	k1gFnSc4
za	za	k7c4
rozhodující	rozhodující	k2eAgInSc4d1
prostředek	prostředek	k1gInSc4
pro	pro	k7c4
svévolné	svévolný	k2eAgNnSc4d1
ovládání	ovládání	k1gNnSc4
občanů	občan	k1gMnPc2
a	a	k8xC
současně	současně	k6eAd1
ji	on	k3xPp3gFnSc4
co	co	k9
nejpřísněji	přísně	k6eAd3
reglementuje	reglementovat	k5eAaBmIp3nS
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Systém	systém	k1gInSc1
ekonomiky	ekonomika	k1gFnSc2
disentu	disent	k1gInSc2
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
na	na	k7c6
vzájemnosti	vzájemnost	k1gFnSc6
a	a	k8xC
důvěře	důvěra	k1gFnSc6
v	v	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
<g/>
,	,	kIx,
tzn.	tzn.	kA
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
přirozený	přirozený	k2eAgInSc4d1
zárodek	zárodek	k1gInSc4
reputačního	reputační	k2eAgInSc2d1
principu	princip	k1gInSc2
a	a	k8xC
hledání	hledání	k1gNnSc2
směnných	směnný	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
nebudou	být	k5eNaImBp3nP
závislé	závislý	k2eAgInPc1d1
na	na	k7c6
centrálně	centrálně	k6eAd1
kontrolovaných	kontrolovaný	k2eAgInPc6d1
měnových	měnový	k2eAgInPc6d1
nástrojích	nástroj	k1gInPc6
<g/>
,	,	kIx,
</s>
<s>
vytváření	vytváření	k1gNnSc1
paralelních	paralelní	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
a	a	k8xC
podporování	podporování	k1gNnSc4
jejich	jejich	k3xOp3gInSc2
rozvoje	rozvoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stínové	stínový	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
v	v	k7c6
inkubačním	inkubační	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
paralelní	paralelní	k2eAgInSc4d1
polis	polis	k1gInSc4
vyvíjeny	vyvíjen	k2eAgInPc4d1
do	do	k7c2
takové	takový	k3xDgFnSc2
funkční	funkční	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
schopny	schopen	k2eAgFnPc1d1
nahradit	nahradit	k5eAaPmF
vládnoucí	vládnoucí	k2eAgInSc4d1
autoritářský	autoritářský	k2eAgInSc4d1
režim	režim	k1gInSc4
<g/>
,	,	kIx,
</s>
<s>
paralelní	paralelní	k2eAgFnSc1d1
zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
nástrojem	nástroj	k1gInSc7
paralelní	paralelní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
pro	pro	k7c4
stabilizaci	stabilizace	k1gFnSc4
a	a	k8xC
zakotvení	zakotvení	k1gNnSc4
hnutí	hnutí	k1gNnSc2
v	v	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
a	a	k8xC
hledání	hledání	k1gNnSc6
potenciálních	potenciální	k2eAgInPc2d1
materiálních	materiální	k2eAgInPc2d1
i	i	k8xC
myšlenkových	myšlenkový	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgFnPc1
dílčí	dílčí	k2eAgFnPc1d1
paralelní	paralelní	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
však	však	k9
nejsou	být	k5eNaImIp3nP
uzavřenou	uzavřený	k2eAgFnSc7d1
množinou	množina	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
naopak	naopak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
vznikat	vznikat	k5eAaImF
další	další	k2eAgInPc1d1
na	na	k7c6
všech	všecek	k3xTgFnPc6
frontách	fronta	k1gFnPc6
odporu	odpor	k1gInSc2
proti	proti	k7c3
autoritářskému	autoritářský	k2eAgInSc3d1
státu	stát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
paralelní	paralelní	k2eAgFnSc2d1
polis	polis	k1gFnSc2
by	by	kYmCp3nS
podle	podle	k7c2
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
<g/>
,	,	kIx,
Ivana	Ivan	k1gMnSc2
M.	M.	kA
Jirouse	Jirouse	k1gFnSc2
<g/>
,	,	kIx,
Milana	Milana	k1gFnSc1
Šimečky	Šimeček	k1gMnPc4
a	a	k8xC
dalších	další	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
disentu	disent	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
o	o	k7c6
konceptu	koncept	k1gInSc6
diskutovaly	diskutovat	k5eAaImAgFnP
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
tzv.	tzv.	kA
nezávislá	závislý	k2eNgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
myšlena	myšlen	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
není	být	k5eNaImIp3nS
pokořena	pokořen	k2eAgFnSc1d1
zákony	zákon	k1gInPc7
a	a	k8xC
rozhodnutími	rozhodnutí	k1gNnPc7
zástupců	zástupce	k1gMnPc2
veřejné	veřejný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
založená	založený	k2eAgFnSc1d1
na	na	k7c6
vlastních	vlastní	k2eAgFnPc6d1
hodnotách	hodnota	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jí	on	k3xPp3gFnSc7
nejsou	být	k5eNaImIp3nP
vnucovány	vnucován	k2eAgInPc1d1
centrální	centrální	k2eAgFnSc7d1
autoritou	autorita	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
podle	podle	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
konstituovaná	konstituovaný	k2eAgFnSc1d1
a	a	k8xC
organizovaná	organizovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezávislá	závislý	k2eNgFnSc1d1
společnost	společnost	k1gFnSc1
je	být	k5eAaImIp3nS
vůči	vůči	k7c3
státní	státní	k2eAgFnSc3d1
moci	moc	k1gFnSc3
v	v	k7c6
rovném	rovný	k2eAgNnSc6d1
postavení	postavení	k1gNnSc6
a	a	k8xC
má	mít	k5eAaImIp3nS
před	před	k7c7
státní	státní	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
přednost	přednost	k1gFnSc4
při	při	k7c6
sebeorganizaci	sebeorganizace	k1gFnSc6
a	a	k8xC
sebezajištění	sebezajištění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Dalo	dát	k5eAaPmAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
dokonce	dokonce	k9
říct	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
skutečnou	skutečný	k2eAgFnSc4d1
a	a	k8xC
nejdůležitější	důležitý	k2eAgFnSc4d3
‘	‘	k?
<g/>
paralelní	paralelní	k2eAgFnSc2d1
polis	polis	k1gFnSc2
<g/>
’	’	k?
dnes	dnes	k6eAd1
nepředstavuje	představovat	k5eNaImIp3nS
‘	‘	k?
<g/>
disidentský	disidentský	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
’	’	k?
<g/>
,	,	kIx,
ale	ale	k8xC
svět	svět	k1gInSc1
smýšlení	smýšlení	k1gNnSc2
a	a	k8xC
privátních	privátní	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
celé	celý	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
sice	sice	k8xC
jednou	jeden	k4xCgFnSc7
rukou	ruka	k1gFnSc7
dává	dávat	k5eAaImIp3nS
totalitní	totalitní	k2eAgFnSc1d1
moci	moct	k5eAaImF
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
na	na	k7c6
ní	on	k3xPp3gFnSc6
bezpodmínečně	bezpodmínečně	k6eAd1
vyžaduje	vyžadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
druhou	druhý	k4xOgFnSc4
si	se	k3xPyFc3
zároveň	zároveň	k6eAd1
dělá	dělat	k5eAaImIp3nS
vše	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
sama	sám	k3xTgMnSc4
chce	chtít	k5eAaImIp3nS
a	a	k8xC
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
nemá	mít	k5eNaImIp3nS
s	s	k7c7
vůlí	vůle	k1gFnSc7
této	tento	k3xDgFnSc2
moci	moc	k1gFnSc2
pranic	pranic	k6eAd1
společného	společný	k2eAgInSc2d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
revoluci	revoluce	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
nebyla	být	k5eNaImAgFnS
tomuto	tento	k3xDgInSc3
konceptu	koncept	k1gInSc6
věnována	věnovat	k5eAaPmNgFnS,k5eAaImNgFnS
přílišná	přílišný	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Benda	Benda	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
:	:	kIx,
Paralelní	paralelní	k2eAgFnSc1d1
polis	polis	k1gFnSc1
<g/>
,	,	kIx,
in	in	k?
Noční	noční	k2eAgInSc1d1
kádrový	kádrový	k2eAgInSc1d1
dotazník	dotazník	k1gInSc1
a	a	k8xC
jiné	jiný	k2eAgInPc1d1
boje	boj	k1gInPc1
<g/>
,	,	kIx,
FRA	FRA	kA
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
63	#num#	k4
<g/>
↑	↑	k?
Parallel	Parallel	k1gFnSc1
Polis	Polis	k1gFnSc1
<g/>
,	,	kIx,
or	or	k?
an	an	k?
Independent	independent	k1gMnSc1
Society	societa	k1gFnSc2
In	In	k1gMnSc1
Central	Central	k1gMnSc1
and	and	k?
Eastern	Eastern	k1gMnSc1
Europe	Europ	k1gInSc5
<g/>
↑	↑	k?
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
<g/>
:	:	kIx,
Důvody	důvod	k1gInPc1
ke	k	k7c3
skepsi	skepse	k1gFnSc3
a	a	k8xC
zdroje	zdroj	k1gInPc4
naděje	naděje	k1gFnSc2
-	-	kIx~
1988	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ztohoven	Ztohoven	k2eAgInSc1d1
<g/>
#	#	kIx~
<g/>
Paralelní	paralelní	k2eAgInSc1d1
polis	polis	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Paralelní	paralelní	k2eAgInSc1d1
polis	polis	k1gInSc1
–	–	k?
projekt	projekt	k1gInSc1
skupiny	skupina	k1gFnSc2
Ztohoven	Ztohovna	k1gFnPc2
<g/>
,	,	kIx,
navazující	navazující	k2eAgFnPc4d1
na	na	k7c4
myšlenku	myšlenka	k1gFnSc4
Václava	Václav	k1gMnSc2
Bendy	Benda	k1gMnSc2
</s>
<s>
Jsem	být	k5eAaImIp1nS
striktní	striktní	k2eAgMnSc1d1
anarchista	anarchista	k1gMnSc1
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
Roman	Roman	k1gMnSc1
Týc	Týc	k1gMnSc1
ze	z	k7c2
skupiny	skupina	k1gFnSc2
Ztohoven	Ztohovna	k1gFnPc2
–	–	k?
rozhovor	rozhovor	k1gInSc1
mj.	mj.	kA
na	na	k7c4
téma	téma	k1gNnSc4
paralelní	paralelní	k2eAgFnSc2d1
polis	polis	k1gFnSc2
</s>
<s>
Paralelní	paralelní	k2eAgFnSc1d1
Polis	Polis	k1gFnSc1
<g/>
:	:	kIx,
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
nesmí	smět	k5eNaImIp3nS
EET	EET	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
bitcoin	bitcoin	k2eAgInSc1d1
je	být	k5eAaImIp3nS
tam	tam	k6eAd1
doma	doma	k6eAd1
–	–	k?
přehled	přehled	k1gInSc4
aktuálních	aktuální	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
,	,	kIx,
postoje	postoj	k1gInPc4
ke	k	k7c3
kryptoměnách	kryptoměna	k1gFnPc6
a	a	k8xC
informace	informace	k1gFnSc2
o	o	k7c4
financování	financování	k1gNnSc4
holešovického	holešovický	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
Paralelní	paralelní	k2eAgFnSc2d1
Polis	Polis	k1gFnSc2
</s>
