<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
rozvrat	rozvrat	k1gInSc1	rozvrat
země	zem	k1gFnSc2	zem
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
a	a	k8xC	a
již	již	k6eAd1	již
se	se	k3xPyFc4	se
projevující	projevující	k2eAgFnPc1d1	projevující
expanzionistické	expanzionistický	k2eAgFnPc1d1	expanzionistická
tendence	tendence	k1gFnPc1	tendence
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vzestup	vzestup	k1gInSc4	vzestup
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc2	vůdce
nacionálních	nacionální	k2eAgMnPc2d1	nacionální
socialistů	socialist	k1gMnPc2	socialist
(	(	kIx(	(
<g/>
NSDAP	NSDAP	kA	NSDAP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sliboval	slibovat	k5eAaImAgMnS	slibovat
německému	německý	k2eAgInSc3d1	německý
národu	národ	k1gInSc3	národ
zrušení	zrušení	k1gNnSc2	zrušení
versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
zajištění	zajištění	k1gNnSc2	zajištění
"	"	kIx"	"
<g/>
životního	životní	k2eAgInSc2d1	životní
prostoru	prostor	k1gInSc2	prostor
<g/>
"	"	kIx"	"
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
(	(	kIx(	(
<g/>
Lebensraum	Lebensraum	k1gNnSc1	Lebensraum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
byl	být	k5eAaImAgMnS	být
Hitler	Hitler	k1gMnSc1	Hitler
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
říšským	říšský	k2eAgMnSc7d1	říšský
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
během	během	k7c2	během
několika	několik	k4yIc2	několik
málo	málo	k4c4	málo
měsíců	měsíc	k1gInPc2	měsíc
realizoval	realizovat	k5eAaBmAgMnS	realizovat
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
nacistickou	nacistický	k2eAgFnSc4d1	nacistická
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
