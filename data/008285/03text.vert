<p>
<s>
Chobotnice	chobotnice	k1gFnSc1	chobotnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
(	(	kIx(	(
<g/>
Hapalochlaena	Hapalochlaen	k2eAgFnSc1d1	Hapalochlaena
maculosa	maculosa	k1gFnSc1	maculosa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
chobotnice	chobotnice	k1gFnSc2	chobotnice
patřící	patřící	k2eAgInSc4d1	patřící
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
chobotnicovití	chobotnicovitý	k2eAgMnPc1d1	chobotnicovitý
(	(	kIx(	(
<g/>
Octopodidae	Octopodidae	k1gNnSc7	Octopodidae
<g/>
)	)	kIx)	)
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
Hapalochlaena	Hapalochlaeno	k1gNnSc2	Hapalochlaeno
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
jižní	jižní	k2eAgFnSc2d1	jižní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
chobotnicemi	chobotnice	k1gFnPc7	chobotnice
je	být	k5eAaImIp3nS	být
malým	malý	k2eAgInSc7d1	malý
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
měřícím	měřící	k2eAgInSc7d1	měřící
okolo	okolo	k7c2	okolo
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
má	mít	k5eAaImIp3nS	mít
tmavohnědé	tmavohnědý	k2eAgInPc1d1	tmavohnědý
až	až	k6eAd1	až
tmavožluté	tmavožlutý	k2eAgInPc1d1	tmavožlutý
s	s	k7c7	s
nahnědlými	nahnědlý	k2eAgFnPc7d1	nahnědlá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
chobotnice	chobotnice	k1gFnSc1	chobotnice
podrážděná	podrážděný	k2eAgFnSc1d1	podrážděná
<g/>
,	,	kIx,	,
skvrny	skvrna	k1gFnPc1	skvrna
ztmavnou	ztmavnout	k5eAaPmIp3nP	ztmavnout
a	a	k8xC	a
uvnitř	uvnitř	k6eAd1	uvnitř
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
jasně	jasně	k6eAd1	jasně
modré	modrý	k2eAgInPc4d1	modrý
kruhy	kruh	k1gInPc4	kruh
či	či	k8xC	či
kruhovité	kruhovitý	k2eAgInPc4d1	kruhovitý
obrazce	obrazec	k1gInPc4	obrazec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
navíc	navíc	k6eAd1	navíc
mohou	moct	k5eAaImIp3nP	moct
působit	působit	k5eAaImF	působit
pulzujícím	pulzující	k2eAgInSc7d1	pulzující
dojmem	dojem	k1gInSc7	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
formu	forma	k1gFnSc4	forma
aposematismu	aposematismus	k1gInSc2	aposematismus
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
upozornění	upozornění	k1gNnSc1	upozornění
živočichů	živočich	k1gMnPc2	živočich
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
jedovatost	jedovatost	k1gFnSc4	jedovatost
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
v	v	k7c6	v
zadních	zadní	k2eAgFnPc6d1	zadní
slinných	slinný	k2eAgFnPc6d1	slinná
žlázách	žláza	k1gFnPc6	žláza
chobotnice	chobotnice	k1gFnSc2	chobotnice
produkují	produkovat	k5eAaImIp3nP	produkovat
tetrodotoxin	tetrodotoxin	k2eAgMnSc1d1	tetrodotoxin
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
jedy	jed	k1gInPc1	jed
paralyzující	paralyzující	k2eAgFnSc4d1	paralyzující
činnost	činnost	k1gFnSc4	činnost
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Otrava	otrava	k1gFnSc1	otrava
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
jak	jak	k6eAd1	jak
při	při	k7c6	při
kousnutí	kousnutí	k1gNnSc6	kousnutí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
při	při	k7c6	při
případné	případný	k2eAgFnSc6d1	případná
konzumaci	konzumace	k1gFnSc6	konzumace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jedovaté	jedovatý	k2eAgInPc1d1	jedovatý
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
další	další	k2eAgFnPc4d1	další
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Otrava	otrava	k1gFnSc1	otrava
je	být	k5eAaImIp3nS	být
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
i	i	k9	i
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
korýši	korýš	k1gMnPc1	korýš
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
krabi	krab	k1gMnPc1	krab
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
jedem	jed	k1gInSc7	jed
<g/>
,	,	kIx,	,
lovit	lovit	k5eAaImF	lovit
může	moct	k5eAaImIp3nS	moct
i	i	k8xC	i
měkkýše	měkkýš	k1gMnPc4	měkkýš
nebo	nebo	k8xC	nebo
malé	malý	k2eAgFnPc4d1	malá
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Chobotnice	chobotnice	k1gFnPc1	chobotnice
skvrnité	skvrnitý	k2eAgFnPc1d1	skvrnitá
jsou	být	k5eAaImIp3nP	být
samotářsky	samotářsky	k6eAd1	samotářsky
žijící	žijící	k2eAgMnPc1d1	žijící
tvorové	tvor	k1gMnPc1	tvor
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
jedince	jedinko	k6eAd1	jedinko
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
samec	samec	k1gMnSc1	samec
po	po	k7c6	po
předehře	předehra	k1gFnSc6	předehra
vloží	vložit	k5eAaPmIp3nS	vložit
samici	samice	k1gFnSc4	samice
přeměněným	přeměněný	k2eAgNnSc7d1	přeměněné
chapadlem	chapadlo	k1gNnSc7	chapadlo
do	do	k7c2	do
vejcovodu	vejcovod	k1gInSc2	vejcovod
spermie	spermie	k1gFnSc2	spermie
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
sesbírá	sesbírat	k5eAaPmIp3nS	sesbírat
sperma	sperma	k1gNnSc4	sperma
od	od	k7c2	od
více	hodně	k6eAd2	hodně
samců	samec	k1gMnPc2	samec
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
naklade	naklást	k5eAaPmIp3nS	naklást
50	[number]	k4	50
až	až	k9	až
60	[number]	k4	60
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
o	o	k7c4	o
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
okolo	okolo	k7c2	okolo
šesti	šest	k4xCc2	šest
až	až	k9	až
sedmi	sedm	k4xCc2	sedm
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vajíčka	vajíčko	k1gNnSc2	vajíčko
neustále	neustále	k6eAd1	neustále
chrání	chránit	k5eAaImIp3nS	chránit
<g/>
,	,	kIx,	,
nevyhledává	vyhledávat	k5eNaImIp3nS	vyhledávat
aktivně	aktivně	k6eAd1	aktivně
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
během	během	k7c2	během
líhnutí	líhnutí	k1gNnSc2	líhnutí
mláďat	mládě	k1gNnPc2	mládě
uhyne	uhynout	k5eAaPmIp3nS	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
loví	lovit	k5eAaImIp3nP	lovit
od	od	k7c2	od
čtyř	čtyři	k4xCgInPc2	čtyři
týdnů	týden	k1gInPc2	týden
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
dospělosti	dospělost	k1gFnPc1	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
ničením	ničení	k1gNnSc7	ničení
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
možné	možný	k2eAgNnSc1d1	možné
ohrožení	ohrožení	k1gNnSc1	ohrožení
představují	představovat	k5eAaImIp3nP	představovat
také	také	k9	také
odchyt	odchyt	k1gInSc4	odchyt
do	do	k7c2	do
akvárií	akvárium	k1gNnPc2	akvárium
<g/>
,	,	kIx,	,
zabíjení	zabíjení	k1gNnSc2	zabíjení
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
aktivity	aktivita	k1gFnPc4	aktivita
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1	lidská
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
druhu	druh	k1gInSc2	druh
spočívají	spočívat	k5eAaImIp3nP	spočívat
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
mořských	mořský	k2eAgFnPc2d1	mořská
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
zjištění	zjištění	k1gNnSc1	zjištění
velikosti	velikost	k1gFnSc2	velikost
populací	populace	k1gFnPc2	populace
a	a	k8xC	a
v	v	k7c6	v
případném	případný	k2eAgNnSc6d1	případné
omezení	omezení	k1gNnSc6	omezení
vývozu	vývoz	k1gInSc2	vývoz
<g/>
,	,	kIx,	,
důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Chobotnice	chobotnice	k1gFnSc1	chobotnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
příbuzní	příbuzný	k1gMnPc1	příbuzný
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
chobotnicovitých	chobotnicovitý	k2eAgFnPc2d1	chobotnicovitý
(	(	kIx(	(
<g/>
Octopodidae	Octopodidae	k1gFnPc2	Octopodidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Hapalochlaena	Hapalochlaeno	k1gNnSc2	Hapalochlaeno
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deset	deset	k4xCc4	deset
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jen	jen	k9	jen
čtyři	čtyři	k4xCgFnPc1	čtyři
byly	být	k5eAaImAgFnP	být
formálně	formálně	k6eAd1	formálně
popsány	popsat	k5eAaPmNgFnP	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
znaky	znak	k1gInPc7	znak
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fylogenetických	fylogenetický	k2eAgNnPc2d1	fylogenetické
studií	studio	k1gNnPc2	studio
vychází	vycházet	k5eAaImIp3nS	vycházet
jako	jako	k9	jako
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
rod	rod	k1gInSc1	rod
Amphioctopus	Amphioctopus	k1gInSc1	Amphioctopus
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tvoří	tvořit	k5eAaImIp3nS	tvořit
rod	rod	k1gInSc1	rod
Hapalochlaena	Hapalochlaeno	k1gNnSc2	Hapalochlaeno
společný	společný	k2eAgInSc1d1	společný
klad	klad	k1gInSc1	klad
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
rody	rod	k1gInPc1	rod
pak	pak	k6eAd1	pak
společně	společně	k6eAd1	společně
s	s	k7c7	s
rody	rod	k1gInPc7	rod
Abdopus	Abdopus	k1gMnSc1	Abdopus
<g/>
,	,	kIx,	,
Ameloctopus	Ameloctopus	k1gMnSc1	Ameloctopus
<g/>
,	,	kIx,	,
Cistopus	Cistopus	k1gMnSc1	Cistopus
a	a	k8xC	a
Octopus	Octopus	k1gMnSc1	Octopus
tvoří	tvořit	k5eAaImIp3nS	tvořit
širší	široký	k2eAgInSc4d2	širší
klad	klad	k1gInSc4	klad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
obsáhlejší	obsáhlý	k2eAgFnSc2d2	obsáhlejší
skupiny	skupina	k1gFnSc2	skupina
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
sdružující	sdružující	k2eAgFnSc2d1	sdružující
chobotnice	chobotnice	k1gFnSc2	chobotnice
s	s	k7c7	s
dvouřadým	dvouřadý	k2eAgNnSc7d1	dvouřadé
uspořádáním	uspořádání	k1gNnSc7	uspořádání
přísavek	přísavka	k1gFnPc2	přísavka
a	a	k8xC	a
s	s	k7c7	s
inkoustovým	inkoustový	k2eAgInSc7d1	inkoustový
váčkem	váček	k1gInSc7	váček
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
podčeleď	podčeleď	k1gFnSc1	podčeleď
Octopodinae	Octopodinae	k1gFnSc1	Octopodinae
<g/>
.	.	kIx.	.
<g/>
Odborným	odborný	k2eAgNnSc7d1	odborné
jménem	jméno	k1gNnSc7	jméno
chobotnice	chobotnice	k1gFnSc2	chobotnice
skvrnité	skvrnitý	k2eAgFnSc2d1	skvrnitá
je	být	k5eAaImIp3nS	být
Hapalochlaena	Hapalochlaen	k2eAgFnSc1d1	Hapalochlaena
maculosa	maculosa	k1gFnSc1	maculosa
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yIgInSc7	který
ji	on	k3xPp3gFnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
britský	britský	k2eAgMnSc1d1	britský
zoolog	zoolog	k1gMnSc1	zoolog
William	William	k1gInSc4	William
Evans	Evans	k1gInSc4	Evans
Hoyle	Hoyle	k1gFnSc2	Hoyle
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
</s>
<s>
Vědeckými	vědecký	k2eAgNnPc7d1	vědecké
synonymy	synonymum	k1gNnPc7	synonymum
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
Octopus	Octopus	k1gMnSc1	Octopus
maculosus	maculosus	k1gMnSc1	maculosus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
rovněž	rovněž	k9	rovněž
Hoyle	Hoyle	k1gInSc1	Hoyle
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Octopus	Octopus	k1gMnSc1	Octopus
pictus	pictus	k1gMnSc1	pictus
a	a	k8xC	a
Octopus	Octopus	k1gMnSc1	Octopus
robustus	robustus	k1gMnSc1	robustus
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
určil	určit	k5eAaPmAgInS	určit
Brock	Brock	k1gInSc1	Brock
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1883	[number]	k4	1883
a	a	k8xC	a
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
stanoviště	stanoviště	k1gNnPc1	stanoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Chobotnice	chobotnice	k1gFnSc1	chobotnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
je	být	k5eAaImIp3nS	být
živočichem	živočich	k1gMnSc7	živočich
Indo-Pacifiku	Indo-Pacifik	k1gInSc2	Indo-Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
zbývající	zbývající	k2eAgInPc1d1	zbývající
druhy	druh	k1gInPc1	druh
rodu	rod	k1gInSc2	rod
Hapalochlaena	Hapalochlaeno	k1gNnSc2	Hapalochlaeno
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obývá	obývat	k5eAaImIp3nS	obývat
její	její	k3xOp3gInSc4	její
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
od	od	k7c2	od
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
kontinentu	kontinent	k1gInSc2	kontinent
až	až	k9	až
po	po	k7c4	po
východ	východ	k1gInSc4	východ
státu	stát	k1gInSc2	stát
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
z	z	k7c2	z
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
dle	dle	k7c2	dle
analýzy	analýza	k1gFnSc2	analýza
DNA	DNA	kA	DNA
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
populací	populace	k1gFnPc2	populace
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
většího	veliký	k2eAgInSc2d2	veliký
rozptýlení	rozptýlení	k1gNnPc2	rozptýlení
do	do	k7c2	do
vzdálenějších	vzdálený	k2eAgFnPc2d2	vzdálenější
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
<g/>
Chobotnici	chobotnice	k1gFnSc4	chobotnice
skvrnitou	skvrnitý	k2eAgFnSc4d1	skvrnitá
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
až	až	k9	až
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
50	[number]	k4	50
m	m	kA	m
<g/>
,	,	kIx,	,
obyčejně	obyčejně	k6eAd1	obyčejně
se	se	k3xPyFc4	se
však	však	k9	však
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mělké	mělký	k2eAgFnSc6d1	mělká
vodě	voda	k1gFnSc6	voda
v	v	k7c6	v
lagunách	laguna	k1gFnPc6	laguna
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
korály	korál	k1gMnPc7	korál
<g/>
,	,	kIx,	,
žít	žít	k5eAaImF	žít
může	moct	k5eAaImIp3nS	moct
rovněž	rovněž	k9	rovněž
mezi	mezi	k7c7	mezi
kameny	kámen	k1gInPc7	kámen
či	či	k8xC	či
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
sumek	sumka	k1gFnPc2	sumka
(	(	kIx(	(
<g/>
Ascidiacea	Ascidiacea	k1gFnSc1	Ascidiacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
potkat	potkat	k5eAaPmF	potkat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
štěrbinách	štěrbina	k1gFnPc6	štěrbina
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
měkkému	měkký	k2eAgNnSc3d1	měkké
tělu	tělo	k1gNnSc3	tělo
a	a	k8xC	a
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
před	před	k7c7	před
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
úkrytem	úkryt	k1gInSc7	úkryt
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
schránky	schránka	k1gFnPc1	schránka
mlžů	mlž	k1gMnPc2	mlž
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
si	se	k3xPyFc3	se
útočiště	útočiště	k1gNnSc1	útočiště
vyhrabe	vyhrabat	k5eAaPmIp3nS	vyhrabat
v	v	k7c6	v
písku	písek	k1gInSc6	písek
či	či	k8xC	či
bahnu	bahno	k1gNnSc6	bahno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
přirozeném	přirozený	k2eAgNnSc6d1	přirozené
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
ji	on	k3xPp3gFnSc4	on
spatřit	spatřit	k5eAaPmF	spatřit
zvláště	zvláště	k6eAd1	zvláště
po	po	k7c6	po
bouřkách	bouřka	k1gFnPc6	bouřka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgNnSc1d1	aktivní
její	její	k3xOp3gFnSc4	její
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Chobotnice	chobotnice	k1gFnSc1	chobotnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
malé	malý	k2eAgInPc4d1	malý
druhy	druh	k1gInPc4	druh
chobotnic	chobotnice	k1gFnPc2	chobotnice
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
20	[number]	k4	20
cm	cm	kA	cm
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
délka	délka	k1gFnSc1	délka
chapadel	chapadlo	k1gNnPc2	chapadlo
činí	činit	k5eAaImIp3nS	činit
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
12	[number]	k4	12
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
špičatý	špičatý	k2eAgInSc1d1	špičatý
a	a	k8xC	a
obyčejně	obyčejně	k6eAd1	obyčejně
není	být	k5eNaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
5	[number]	k4	5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc4	hmotnost
jedinců	jedinec	k1gMnPc2	jedinec
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
38	[number]	k4	38
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
plavání	plavání	k1gNnSc3	plavání
používá	používat	k5eAaImIp3nS	používat
jak	jak	k6eAd1	jak
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
chapadla	chapadlo	k1gNnSc2	chapadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
chobotnice	chobotnice	k1gFnSc2	chobotnice
skvrnité	skvrnitý	k2eAgNnSc1d1	skvrnité
je	být	k5eAaImIp3nS	být
zvrásněné	zvrásněný	k2eAgNnSc1d1	zvrásněné
a	a	k8xC	a
hrubé	hrubý	k2eAgNnSc1d1	hrubé
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvena	zbarven	k2eAgFnSc1d1	zbarvena
je	být	k5eAaImIp3nS	být
šedě	šedě	k6eAd1	šedě
až	až	k9	až
hnědě	hnědě	k6eAd1	hnědě
<g/>
,	,	kIx,	,
s	s	k7c7	s
nahnědlými	nahnědlý	k2eAgFnPc7d1	nahnědlá
kruhovými	kruhový	k2eAgFnPc7d1	kruhová
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
maskování	maskování	k1gNnSc1	maskování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
rameni	rameno	k1gNnSc6	rameno
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
okolo	okolo	k7c2	okolo
deseti	deset	k4xCc2	deset
<g/>
,	,	kIx,	,
deset	deset	k4xCc4	deset
jich	on	k3xPp3gNnPc2	on
je	být	k5eAaImIp3nS	být
i	i	k9	i
na	na	k7c6	na
zbytku	zbytek	k1gInSc6	zbytek
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
chobotnice	chobotnice	k1gFnSc1	chobotnice
podrážděná	podrážděný	k2eAgFnSc1d1	podrážděná
<g/>
,	,	kIx,	,
skvrny	skvrna	k1gFnPc1	skvrna
ztmavnou	ztmavnout	k5eAaPmIp3nP	ztmavnout
a	a	k8xC	a
uvnitř	uvnitř	k6eAd1	uvnitř
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
jasně	jasně	k6eAd1	jasně
modré	modrý	k2eAgInPc4d1	modrý
kruhy	kruh	k1gInPc4	kruh
či	či	k8xC	či
kruhovité	kruhovitý	k2eAgInPc4d1	kruhovitý
obrazce	obrazec	k1gInPc4	obrazec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
navíc	navíc	k6eAd1	navíc
mohou	moct	k5eAaImIp3nP	moct
působit	působit	k5eAaImF	působit
pulzujícím	pulzující	k2eAgInSc7d1	pulzující
dojmem	dojem	k1gInSc7	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
reakcí	reakce	k1gFnSc7	reakce
chromatoforů	chromatofor	k1gInPc2	chromatofor
<g/>
.	.	kIx.	.
</s>
<s>
Chobotnicím	chobotnice	k1gFnPc3	chobotnice
skvrnitým	skvrnitý	k2eAgMnPc3d1	skvrnitý
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
výstražné	výstražný	k2eAgNnSc1d1	výstražné
zbarvení	zbarvení	k1gNnSc1	zbarvení
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
týdnu	týden	k1gInSc6	týden
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
očí	oko	k1gNnPc2	oko
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
růžové	růžový	k2eAgNnSc4d1	růžové
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výrazné	výrazný	k2eAgNnSc1d1	výrazné
zbarvení	zbarvení	k1gNnSc1	zbarvení
objevující	objevující	k2eAgMnSc1d1	objevující
se	se	k3xPyFc4	se
při	při	k7c6	při
podráždění	podráždění	k1gNnSc6	podráždění
je	být	k5eAaImIp3nS	být
formou	forma	k1gFnSc7	forma
aposematismu	aposematismus	k1gInSc2	aposematismus
a	a	k8xC	a
varuje	varovat	k5eAaImIp3nS	varovat
před	před	k7c7	před
jedovatostí	jedovatost	k1gFnSc7	jedovatost
nositele	nositel	k1gMnSc2	nositel
<g/>
;	;	kIx,	;
toxické	toxický	k2eAgInPc1d1	toxický
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
rodů	rod	k1gInPc2	rod
Alteromonas	Alteromonasa	k1gFnPc2	Alteromonasa
<g/>
,	,	kIx,	,
Bacillus	Bacillus	k1gInSc1	Bacillus
<g/>
,	,	kIx,	,
Pseudomonas	Pseudomonas	k1gInSc1	Pseudomonas
a	a	k8xC	a
Vibrio	vibrio	k1gNnSc1	vibrio
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
zadních	zadní	k2eAgFnPc6d1	zadní
slinných	slinný	k2eAgFnPc6d1	slinná
žlázách	žláza	k1gFnPc6	žláza
chobotnice	chobotnice	k1gFnSc2	chobotnice
jed	jed	k1gInSc1	jed
tetrodotoxin	tetrodotoxin	k1gMnSc1	tetrodotoxin
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
mimochodem	mimochodem	k9	mimochodem
znám	znám	k2eAgMnSc1d1	znám
například	například	k6eAd1	například
i	i	k8xC	i
u	u	k7c2	u
čtverzubců	čtverzubec	k1gMnPc2	čtverzubec
rodu	rod	k1gInSc2	rod
Takifugu	Takifug	k1gInSc2	Takifug
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
další	další	k2eAgFnPc4d1	další
toxické	toxický	k2eAgFnPc4d1	toxická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
žlázy	žláza	k1gFnPc1	žláza
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
střevním	střevní	k2eAgInSc6d1	střevní
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
0,15	[number]	k4	0,15
g.	g.	k?	g.
</s>
<s>
Chobotnice	chobotnice	k1gFnSc1	chobotnice
používá	používat	k5eAaImIp3nS	používat
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc1	typ
jedů	jed	k1gInPc2	jed
<g/>
:	:	kIx,	:
jeden	jeden	k4xCgMnSc1	jeden
na	na	k7c4	na
zabíjení	zabíjení	k1gNnSc4	zabíjení
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
<g/>
,	,	kIx,	,
silnější	silný	k2eAgInSc1d2	silnější
<g/>
,	,	kIx,	,
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
sekret	sekret	k1gInSc1	sekret
se	se	k3xPyFc4	se
před	před	k7c7	před
kousnutím	kousnutí	k1gNnSc7	kousnutí
promíchá	promíchat	k5eAaPmIp3nS	promíchat
se	s	k7c7	s
slinami	slina	k1gFnPc7	slina
<g/>
,	,	kIx,	,
obsahujícími	obsahující	k2eAgFnPc7d1	obsahující
antikoagulant	antikoagulant	k1gMnSc1	antikoagulant
bránící	bránící	k2eAgNnSc4d1	bránící
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
toxické	toxický	k2eAgFnPc4d1	toxická
i	i	k8xC	i
ostatní	ostatní	k2eAgFnPc4d1	ostatní
měkké	měkký	k2eAgFnPc4d1	měkká
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Jedovatá	jedovatý	k2eAgNnPc1d1	jedovaté
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
vajíčka	vajíčko	k1gNnPc4	vajíčko
chobotnic	chobotnice	k1gFnPc2	chobotnice
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
toxická	toxický	k2eAgNnPc1d1	toxické
již	již	k6eAd1	již
od	od	k7c2	od
vyklubání	vyklubání	k1gNnSc2	vyklubání
<g/>
.	.	kIx.	.
</s>
<s>
Jed	jed	k1gInSc1	jed
má	mít	k5eAaImIp3nS	mít
neurotoxické	urotoxický	k2eNgInPc4d1	neurotoxický
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
blokuje	blokovat	k5eAaImIp3nS	blokovat
přenos	přenos	k1gInSc1	přenos
nervových	nervový	k2eAgInPc2d1	nervový
impulsů	impuls	k1gInPc2	impuls
do	do	k7c2	do
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
otravu	otrava	k1gFnSc4	otrava
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
dýchací	dýchací	k2eAgFnPc4d1	dýchací
potíže	potíž	k1gFnPc4	potíž
spojené	spojený	k2eAgFnPc4d1	spojená
se	s	k7c7	s
špatnou	špatný	k2eAgFnSc7d1	špatná
řečí	řeč	k1gFnSc7	řeč
a	a	k8xC	a
polykáním	polykání	k1gNnSc7	polykání
<g/>
,	,	kIx,	,
rozostřené	rozostřený	k2eAgNnSc1d1	rozostřené
vidění	vidění	k1gNnSc1	vidění
<g/>
,	,	kIx,	,
znecitlivění	znecitlivění	k1gNnSc1	znecitlivění
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
paralýza	paralýza	k1gFnSc1	paralýza
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
u	u	k7c2	u
kočky	kočka	k1gFnSc2	kočka
nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
podání	podání	k1gNnSc6	podání
osminy	osmina	k1gFnSc2	osmina
jedové	jedový	k2eAgFnSc2d1	jedová
žlázy	žláza	k1gFnSc2	žláza
do	do	k7c2	do
patnácti	patnáct	k4xCc2	patnáct
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
až	až	k9	až
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Otrava	otrava	k1gFnSc1	otrava
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
jak	jak	k6eAd1	jak
při	při	k7c6	při
kousnutí	kousnutí	k1gNnSc6	kousnutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
bezbolestné	bezbolestný	k2eAgNnSc1d1	bezbolestné
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
měkkých	měkký	k2eAgFnPc2d1	měkká
tkání	tkáň	k1gFnPc2	tkáň
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
tetrodotoxin	tetrodotoxina	k1gFnPc2	tetrodotoxina
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
není	být	k5eNaImIp3nS	být
agresivní	agresivní	k2eAgMnSc1d1	agresivní
a	a	k8xC	a
místo	místo	k7c2	místo
útoku	útok	k1gInSc2	útok
se	se	k3xPyFc4	se
raději	rád	k6eAd2	rád
zamaskuje	zamaskovat	k5eAaPmIp3nS	zamaskovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Záměna	záměna	k1gFnSc1	záměna
===	===	k?	===
</s>
</p>
<p>
<s>
Chobotnice	chobotnice	k1gFnSc1	chobotnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
podobá	podobat	k5eAaImIp3nS	podobat
dalším	další	k2eAgMnPc3d1	další
dvěma	dva	k4xCgInPc3	dva
druhům	druh	k1gInPc3	druh
rodu	rod	k1gInSc2	rod
Hapalochlaena	Hapalochlaen	k2eAgNnPc1d1	Hapalochlaen
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
chobotnici	chobotnice	k1gFnSc4	chobotnice
kroužkovanou	kroužkovaný	k2eAgFnSc4d1	kroužkovaná
(	(	kIx(	(
<g/>
Hapalochlaena	Hapalochlaen	k2eAgNnPc1d1	Hapalochlaen
lunulata	lunule	k1gNnPc1	lunule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnPc4d2	veliký
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
,	,	kIx,	,
a	a	k8xC	a
chobotnici	chobotnice	k1gFnSc4	chobotnice
pruhovanou	pruhovaný	k2eAgFnSc4d1	pruhovaná
(	(	kIx(	(
<g/>
Hapalochlaena	Hapalochlaen	k2eAgNnPc1d1	Hapalochlaen
fasciata	fascie	k1gNnPc1	fascie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
obranné	obranný	k2eAgNnSc1d1	obranné
zbarvení	zbarvení	k1gNnSc1	zbarvení
tvoří	tvořit	k5eAaImIp3nS	tvořit
pruhovaný	pruhovaný	k2eAgInSc4d1	pruhovaný
vzor	vzor	k1gInSc4	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
druhy	druh	k1gInPc1	druh
chobotnic	chobotnice	k1gFnPc2	chobotnice
mají	mít	k5eAaImIp3nP	mít
navíc	navíc	k6eAd1	navíc
odlišný	odlišný	k2eAgInSc4d1	odlišný
areál	areál	k1gInSc4	areál
výskytu	výskyt	k1gInSc2	výskyt
<g/>
;	;	kIx,	;
chobotnice	chobotnice	k1gFnPc1	chobotnice
kroužkované	kroužkovaný	k2eAgFnPc1d1	kroužkovaná
obývají	obývat	k5eAaImIp3nP	obývat
severní	severní	k2eAgFnPc1d1	severní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
chobotnice	chobotnice	k1gFnSc2	chobotnice
pruhované	pruhovaný	k2eAgNnSc4d1	pruhované
pobřežní	pobřežní	k2eAgNnSc4d1	pobřežní
oblast	oblast	k1gFnSc1	oblast
okolo	okolo	k7c2	okolo
Nového	Nového	k2eAgInSc2d1	Nového
Jižního	jižní	k2eAgInSc2d1	jižní
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Chobotnice	chobotnice	k1gFnSc1	chobotnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
je	být	k5eAaImIp3nS	být
lovec	lovec	k1gMnSc1	lovec
s	s	k7c7	s
noční	noční	k2eAgFnSc7d1	noční
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gFnSc4	její
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
potravu	potrava	k1gFnSc4	potrava
patří	patřit	k5eAaImIp3nP	patřit
korýši	korýš	k1gMnPc1	korýš
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
krabi	krab	k1gMnPc1	krab
<g/>
.	.	kIx.	.
</s>
<s>
Loveckou	lovecký	k2eAgFnSc7d1	lovecká
strategií	strategie	k1gFnSc7	strategie
obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
vyloučení	vyloučení	k1gNnSc4	vyloučení
dávky	dávka	k1gFnSc2	dávka
jedu	jed	k1gInSc2	jed
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
nad	nad	k7c7	nad
krabem	krab	k1gInSc7	krab
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
účinky	účinek	k1gInPc1	účinek
začnou	začít	k5eAaPmIp3nP	začít
na	na	k7c4	na
oběť	oběť	k1gFnSc4	oběť
působit	působit	k5eAaImF	působit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Krab	krab	k1gInSc1	krab
následně	následně	k6eAd1	následně
upadne	upadnout	k5eAaPmIp3nS	upadnout
do	do	k7c2	do
paralýzy	paralýza	k1gFnSc2	paralýza
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
chobotnice	chobotnice	k1gFnSc2	chobotnice
znehybní	znehybnit	k5eAaPmIp3nS	znehybnit
přímo	přímo	k6eAd1	přímo
jedovatým	jedovatý	k2eAgNnSc7d1	jedovaté
kousnutím	kousnutí	k1gNnSc7	kousnutí
či	či	k8xC	či
ji	on	k3xPp3gFnSc4	on
prý	prý	k9	prý
pevně	pevně	k6eAd1	pevně
obalí	obalit	k5eAaPmIp3nP	obalit
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
vylučovaný	vylučovaný	k2eAgInSc1d1	vylučovaný
jed	jed	k1gInSc1	jed
kraba	krab	k1gMnSc2	krab
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
průnikem	průnik	k1gInSc7	průnik
do	do	k7c2	do
dýchacího	dýchací	k2eAgInSc2d1	dýchací
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ochrnutí	ochrnutí	k1gNnSc6	ochrnutí
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
chobotnice	chobotnice	k1gFnSc1	chobotnice
trhá	trhat	k5eAaImIp3nS	trhat
kusy	kus	k1gInPc4	kus
svalstva	svalstvo	k1gNnSc2	svalstvo
<g/>
;	;	kIx,	;
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
však	však	k9	však
hladová	hladový	k2eAgFnSc1d1	hladová
<g/>
,	,	kIx,	,
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
agresivněji	agresivně	k6eAd2	agresivně
a	a	k8xC	a
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
i	i	k9	i
pokud	pokud	k8xS	pokud
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
paralyzována	paralyzován	k2eAgFnSc1d1	paralyzována
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
korýšů	korýš	k1gMnPc2	korýš
může	moct	k5eAaImIp3nS	moct
chobotnice	chobotnice	k1gFnSc1	chobotnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
lovit	lovit	k5eAaImF	lovit
i	i	k8xC	i
měkkýše	měkkýš	k1gMnPc4	měkkýš
nebo	nebo	k8xC	nebo
drobné	drobný	k2eAgMnPc4d1	drobný
druhy	druh	k1gMnPc4	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
ochrnutí	ochrnutí	k1gNnSc6	ochrnutí
kořisti	kořist	k1gFnSc2	kořist
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
chobotnici	chobotnice	k1gFnSc4	chobotnice
tep	tep	k1gInSc1	tep
ze	z	k7c2	z
45	[number]	k4	45
na	na	k7c4	na
75	[number]	k4	75
úderů	úder	k1gInPc2	úder
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
připravuje	připravovat	k5eAaImIp3nS	připravovat
na	na	k7c6	na
konzumaci	konzumace	k1gFnSc6	konzumace
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnPc1	rozmnožování
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Chobotnice	chobotnice	k1gFnPc1	chobotnice
skvrnité	skvrnitý	k2eAgFnPc1d1	skvrnitá
jsou	být	k5eAaImIp3nP	být
samotářsky	samotářsky	k6eAd1	samotářsky
žijící	žijící	k2eAgMnPc1d1	žijící
tvorové	tvor	k1gMnPc1	tvor
<g/>
,	,	kIx,	,
setkání	setkání	k1gNnPc1	setkání
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
sezónně	sezónně	k6eAd1	sezónně
během	během	k7c2	během
února	únor	k1gInSc2	únor
a	a	k8xC	a
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
začne	začít	k5eAaPmIp3nS	začít
lákat	lákat	k5eAaImF	lákat
samce	samec	k1gMnSc4	samec
svým	svůj	k3xOyFgNnSc7	svůj
zbarvením	zbarvení	k1gNnSc7	zbarvení
a	a	k8xC	a
pohyby	pohyb	k1gInPc7	pohyb
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
společná	společný	k2eAgFnSc1d1	společná
předehra	předehra	k1gFnSc1	předehra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
těly	tělo	k1gNnPc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
samec	samec	k1gInSc1	samec
vloží	vložit	k5eAaPmIp3nS	vložit
přeměněným	přeměněný	k2eAgNnSc7d1	přeměněné
chapadlem	chapadlo	k1gNnSc7	chapadlo
(	(	kIx(	(
<g/>
hektokotylus	hektokotylus	k1gMnSc1	hektokotylus
<g/>
)	)	kIx)	)
spermie	spermie	k1gFnSc1	spermie
do	do	k7c2	do
vejcovodu	vejcovod	k1gInSc2	vejcovod
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
posbírá	posbírat	k5eAaPmIp3nS	posbírat
spermie	spermie	k1gFnPc4	spermie
od	od	k7c2	od
více	hodně	k6eAd2	hodně
samců	samec	k1gMnPc2	samec
a	a	k8xC	a
naklade	naklást	k5eAaPmIp3nS	naklást
50	[number]	k4	50
až	až	k9	až
60	[number]	k4	60
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
o	o	k7c4	o
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
kolem	kolem	k7c2	kolem
šesti	šest	k4xCc2	šest
až	až	k9	až
sedmi	sedm	k4xCc2	sedm
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Chrání	chránit	k5eAaImIp3nS	chránit
je	být	k5eAaImIp3nS	být
vlastním	vlastní	k2eAgNnSc7d1	vlastní
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
ukrytá	ukrytý	k2eAgFnSc1d1	ukrytá
ve	v	k7c6	v
strnulé	strnulý	k2eAgFnSc6d1	strnulá
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nepohybuje	pohybovat	k5eNaImIp3nS	pohybovat
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
chapadly	chapadlo	k1gNnPc7	chapadlo
<g/>
.	.	kIx.	.
</s>
<s>
Hýbá	hýbat	k5eAaImIp3nS	hýbat
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vyrušení	vyrušení	k1gNnSc2	vyrušení
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
pohyb	pohyb	k1gInSc1	pohyb
nevyhnutelný	vyhnutelný	k2eNgInSc1d1	nevyhnutelný
<g/>
,	,	kIx,	,
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
jej	on	k3xPp3gInSc4	on
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgNnPc2	dva
chapadel	chapadlo	k1gNnPc2	chapadlo
a	a	k8xC	a
ostatními	ostatní	k2eAgMnPc7d1	ostatní
chrání	chránit	k5eAaImIp3nP	chránit
nevyklubané	vyklubaný	k2eNgNnSc4d1	vyklubaný
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
samice	samice	k1gFnSc2	samice
aktivně	aktivně	k6eAd1	aktivně
nevyhledává	vyhledávat	k5eNaImIp3nS	vyhledávat
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
živí	živit	k5eAaImIp3nP	živit
některými	některý	k3yIgNnPc7	některý
vajíčky	vajíčko	k1gNnPc7	vajíčko
<g/>
,	,	kIx,	,
ztráty	ztráta	k1gFnPc1	ztráta
mohou	moct	k5eAaImIp3nP	moct
činit	činit	k5eAaImF	činit
až	až	k9	až
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Chobotnice	chobotnice	k1gFnPc1	chobotnice
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vajíčcích	vajíček	k1gInPc6	vajíček
zpočátku	zpočátku	k6eAd1	zpočátku
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
s	s	k7c7	s
málo	málo	k4c7	málo
znatelnými	znatelný	k2eAgNnPc7d1	znatelné
chapadly	chapadlo	k1gNnPc7	chapadlo
a	a	k8xC	a
beze	beze	k7c2	beze
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
za	za	k7c4	za
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
stádia	stádium	k1gNnSc2	stádium
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
již	již	k6eAd1	již
začínají	začínat	k5eAaImIp3nP	začínat
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
pigmentové	pigmentový	k2eAgFnPc4d1	pigmentová
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
ve	v	k7c6	v
vajíčku	vajíčko	k1gNnSc6	vajíčko
otočí	otočit	k5eAaPmIp3nP	otočit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
zhruba	zhruba	k6eAd1	zhruba
šest	šest	k4xCc4	šest
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
vyklubou	vyklubat	k5eAaPmIp3nP	vyklubat
<g/>
;	;	kIx,	;
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
během	během	k7c2	během
konce	konec	k1gInSc2	konec
října	říjen	k1gInSc2	říjen
až	až	k8xS	až
začátku	začátek	k1gInSc2	začátek
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
líhnutí	líhnutí	k1gNnSc6	líhnutí
roztahují	roztahovat	k5eAaImIp3nP	roztahovat
a	a	k8xC	a
smršťují	smršťovat	k5eAaImIp3nP	smršťovat
svůj	svůj	k3xOyFgInSc4	svůj
plášť	plášť	k1gInSc4	plášť
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
vajíčko	vajíčko	k1gNnSc1	vajíčko
postupně	postupně	k6eAd1	postupně
rozpíná	rozpínat	k5eAaImIp3nS	rozpínat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
praskne	prasknout	k5eAaPmIp3nS	prasknout
<g/>
.	.	kIx.	.
</s>
<s>
Několikadenní	několikadenní	k2eAgNnPc1d1	několikadenní
mláďata	mládě	k1gNnPc1	mládě
měří	měřit	k5eAaImIp3nP	měřit
asi	asi	k9	asi
4	[number]	k4	4
mm	mm	kA	mm
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc4	čtyři
až	až	k9	až
šest	šest	k4xCc4	šest
hodin	hodina	k1gFnPc2	hodina
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nesou	nést	k5eAaImIp3nP	nést
žloutek	žloutek	k1gInSc1	žloutek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgMnSc2	který
získávají	získávat	k5eAaImIp3nP	získávat
živiny	živina	k1gFnPc1	živina
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
žloutek	žloutek	k1gInSc1	žloutek
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
,	,	kIx,	,
uhynou	uhynout	k5eAaPmIp3nP	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
Konzumovat	konzumovat	k5eAaBmF	konzumovat
potravu	potrava	k1gFnSc4	potrava
začnou	začít	k5eAaPmIp3nP	začít
asi	asi	k9	asi
za	za	k7c4	za
týden	týden	k1gInSc4	týden
a	a	k8xC	a
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
týdnech	týden	k1gInPc6	týden
začnou	začít	k5eAaPmIp3nP	začít
lovit	lovit	k5eAaImF	lovit
kraby	krab	k1gInPc4	krab
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
si	se	k3xPyFc3	se
nejprve	nejprve	k6eAd1	nejprve
volí	volit	k5eAaImIp3nS	volit
menší	malý	k2eAgMnPc4d2	menší
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
se	se	k3xPyFc4	se
volně	volně	k6eAd1	volně
vznáší	vznášet	k5eAaImIp3nS	vznášet
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
s	s	k7c7	s
planktonem	plankton	k1gInSc7	plankton
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přesunou	přesunout	k5eAaPmIp3nP	přesunout
na	na	k7c4	na
mořské	mořský	k2eAgNnSc4d1	mořské
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
dospělosti	dospělost	k1gFnPc1	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
uhyne	uhynout	k5eAaPmIp3nS	uhynout
během	během	k7c2	během
líhnutí	líhnutí	k1gNnSc2	líhnutí
mláďat	mládě	k1gNnPc2	mládě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
12	[number]	k4	12
až	až	k9	až
13	[number]	k4	13
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
zahynou	zahynout	k5eAaPmIp3nP	zahynout
vyčerpáním	vyčerpání	k1gNnPc3	vyčerpání
po	po	k7c6	po
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
6	[number]	k4	6
až	až	k9	až
8	[number]	k4	8
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Otravy	otrava	k1gFnSc2	otrava
===	===	k?	===
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
nebylo	být	k5eNaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
jed	jed	k1gInSc4	jed
některé	některý	k3yIgFnSc2	některý
chobotnice	chobotnice	k1gFnSc2	chobotnice
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
o	o	k7c6	o
otravě	otrava	k1gFnSc6	otrava
korýšů	korýš	k1gMnPc2	korýš
se	se	k3xPyFc4	se
již	již	k6eAd1	již
vědělo	vědět	k5eAaImAgNnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
však	však	k9	však
potápěč	potápěč	k1gMnSc1	potápěč
při	při	k7c6	při
sběru	sběr	k1gInSc6	sběr
chobotnic	chobotnice	k1gFnPc2	chobotnice
chytil	chytit	k5eAaPmAgMnS	chytit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
chobotnici	chobotnice	k1gFnSc4	chobotnice
skvrnitou	skvrnitý	k2eAgFnSc4d1	skvrnitá
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
svým	svůj	k3xOyFgNnPc3	svůj
zbarvením	zbarvení	k1gNnPc3	zbarvení
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
projít	projít	k5eAaPmF	projít
po	po	k7c6	po
ramenech	rameno	k1gNnPc6	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
začaly	začít	k5eAaPmAgInP	začít
projevovat	projevovat	k5eAaImF	projevovat
příznaky	příznak	k1gInPc4	příznak
otravy	otrava	k1gFnSc2	otrava
a	a	k8xC	a
za	za	k7c4	za
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
i	i	k9	i
přes	přes	k7c4	přes
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
chobotnice	chobotnice	k1gFnSc1	chobotnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
smrtelně	smrtelně	k6eAd1	smrtelně
pokousala	pokousat	k5eAaPmAgFnS	pokousat
dva	dva	k4xCgMnPc4	dva
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
je	být	k5eAaImIp3nS	být
datováno	datovat	k5eAaImNgNnS	datovat
další	další	k2eAgNnSc1d1	další
doložené	doložený	k2eAgNnSc1d1	doložené
pokousání	pokousání	k1gNnSc1	pokousání
s	s	k7c7	s
následkem	následek	k1gInSc7	následek
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
voják	voják	k1gMnSc1	voják
sebral	sebrat	k5eAaPmAgMnS	sebrat
jednoho	jeden	k4xCgMnSc4	jeden
zástupce	zástupce	k1gMnPc4	zástupce
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Pokousání	pokousání	k1gNnSc1	pokousání
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c6	v
selhání	selhání	k1gNnSc6	selhání
dechu	dech	k1gInSc2	dech
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pomoc	pomoc	k1gFnSc1	pomoc
po	po	k7c6	po
kousnutí	kousnutí	k1gNnSc6	kousnutí
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vydezinfikování	vydezinfikování	k1gNnSc6	vydezinfikování
a	a	k8xC	a
chlazení	chlazení	k1gNnSc6	chlazení
pokousaného	pokousaný	k2eAgNnSc2d1	pokousané
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
postižený	postižený	k2eAgMnSc1d1	postižený
nedýchá	dýchat	k5eNaImIp3nS	dýchat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zahájit	zahájit	k5eAaPmF	zahájit
úkony	úkon	k1gInPc4	úkon
resuscitace	resuscitace	k1gFnSc2	resuscitace
<g/>
.	.	kIx.	.
</s>
<s>
Nezbytný	zbytný	k2eNgMnSc1d1	zbytný
je	být	k5eAaImIp3nS	být
rychlý	rychlý	k2eAgInSc1d1	rychlý
převoz	převoz	k1gInSc1	převoz
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
umělé	umělý	k2eAgNnSc1d1	umělé
dýchání	dýchání	k1gNnSc1	dýchání
pomocí	pomocí	k7c2	pomocí
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
srdce	srdce	k1gNnSc2	srdce
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
odbourávání	odbourávání	k1gNnSc2	odbourávání
jedu	jed	k1gInSc2	jed
<g/>
;	;	kIx,	;
antidotum	antidotum	k1gNnSc1	antidotum
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
správném	správný	k2eAgInSc6d1	správný
postupu	postup	k1gInSc6	postup
otrava	otrava	k1gFnSc1	otrava
nezanechá	zanechat	k5eNaPmIp3nS	zanechat
trvalé	trvalý	k2eAgInPc4d1	trvalý
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ohrožení	ohrožení	k1gNnSc1	ohrožení
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c4	o
ohrožení	ohrožení	k1gNnSc4	ohrožení
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
nebylo	být	k5eNaImAgNnS	být
získáno	získán	k2eAgNnSc4d1	získáno
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
populace	populace	k1gFnSc2	populace
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
sběr	sběr	k1gInSc4	sběr
chobotnic	chobotnice	k1gFnPc2	chobotnice
z	z	k7c2	z
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
oblíbeného	oblíbený	k2eAgMnSc4d1	oblíbený
chovance	chovanec	k1gMnSc4	chovanec
v	v	k7c6	v
akváriích	akvárium	k1gNnPc6	akvárium
i	i	k8xC	i
přes	přes	k7c4	přes
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
jedovatého	jedovatý	k2eAgNnSc2d1	jedovaté
kousnutí	kousnutí	k1gNnSc2	kousnutí
<g/>
,	,	kIx,	,
o	o	k7c6	o
skutečné	skutečný	k2eAgFnSc6d1	skutečná
hrozbě	hrozba	k1gFnSc6	hrozba
prý	prý	k9	prý
navíc	navíc	k6eAd1	navíc
nemusejí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
lidé	člověk	k1gMnPc1	člověk
ani	ani	k8xC	ani
řádně	řádně	k6eAd1	řádně
informováni	informovat	k5eAaBmNgMnP	informovat
<g/>
.	.	kIx.	.
</s>
<s>
Ohrožujícím	ohrožující	k2eAgInSc7d1	ohrožující
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
také	také	k9	také
ničení	ničení	k1gNnSc4	ničení
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
možné	možný	k2eAgFnPc4d1	možná
hrozby	hrozba	k1gFnPc4	hrozba
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
přítomnost	přítomnost	k1gFnSc1	přítomnost
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
turisté	turist	k1gMnPc1	turist
<g/>
,	,	kIx,	,
potápěči	potápěč	k1gMnPc1	potápěč
<g/>
)	)	kIx)	)
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
navíc	navíc	k6eAd1	navíc
kvůli	kvůli	k7c3	kvůli
jedu	jet	k5eAaImIp1nS	jet
zvířata	zvíře	k1gNnPc1	zvíře
často	často	k6eAd1	často
preventivně	preventivně	k6eAd1	preventivně
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zachování	zachování	k1gNnSc3	zachování
druhu	druh	k1gInSc2	druh
může	moct	k5eAaImIp3nS	moct
přispět	přispět	k5eAaPmF	přispět
ochrana	ochrana	k1gFnSc1	ochrana
mořských	mořský	k2eAgFnPc2d1	mořská
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
zjištění	zjištění	k1gNnSc1	zjištění
velikosti	velikost	k1gFnSc2	velikost
populací	populace	k1gFnPc2	populace
a	a	k8xC	a
případné	případný	k2eAgNnSc4d1	případné
omezení	omezení	k1gNnSc4	omezení
vývozu	vývoz	k1gInSc2	vývoz
<g/>
,	,	kIx,	,
důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Význam	význam	k1gInSc1	význam
===	===	k?	===
</s>
</p>
<p>
<s>
Chobotnice	chobotnice	k1gFnSc1	chobotnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
redukuje	redukovat	k5eAaBmIp3nS	redukovat
stavy	stav	k1gInPc4	stav
invazních	invazní	k2eAgInPc2d1	invazní
druhů	druh	k1gInPc2	druh
korýšů	korýš	k1gMnPc2	korýš
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
jedů	jed	k1gInPc2	jed
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yQgInPc7	který
hraje	hrát	k5eAaImIp3nS	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
tetrodotoxin	tetrodotoxina	k1gFnPc2	tetrodotoxina
z	z	k7c2	z
chobotnice	chobotnice	k1gFnSc2	chobotnice
skvrnité	skvrnitý	k2eAgFnPc1d1	skvrnitá
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
farmaceutického	farmaceutický	k2eAgInSc2d1	farmaceutický
průmyslu	průmysl	k1gInSc2	průmysl
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jed	jed	k1gInSc1	jed
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využíván	využívat	k5eAaPmNgInS	využívat
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SHEPHERD	SHEPHERD	kA	SHEPHERD
<g/>
,	,	kIx,	,
Scoresby	Scoresba	k1gFnSc2	Scoresba
<g/>
;	;	kIx,	;
EDGAR	Edgar	k1gMnSc1	Edgar
<g/>
,	,	kIx,	,
Graham	Graham	k1gMnSc1	Graham
<g/>
.	.	kIx.	.
</s>
<s>
Ecology	Ecologa	k1gFnPc1	Ecologa
of	of	k?	of
Australian	Australian	k1gMnSc1	Australian
temperate	temperat	k1gInSc5	temperat
reefs	reefsit	k5eAaPmRp2nS	reefsit
:	:	kIx,	:
the	the	k?	the
unique	uniquat	k5eAaPmIp3nS	uniquat
South	South	k1gInSc1	South
<g/>
.	.	kIx.	.
</s>
<s>
Collingwood	Collingwood	k1gInSc1	Collingwood
<g/>
:	:	kIx,	:
Csiro	Csiro	k1gNnSc1	Csiro
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
521	[number]	k4	521
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9781486300099	[number]	k4	9781486300099
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
225	[number]	k4	225
<g/>
−	−	k?	−
<g/>
227	[number]	k4	227
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chobotnice	chobotnice	k1gFnSc1	chobotnice
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Hapalochlaena	Hapalochlaen	k2eAgFnSc1d1	Hapalochlaena
maculosa	maculosa	k1gFnSc1	maculosa
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Southern	Southern	k1gMnSc1	Southern
blue-ringed	blueinged	k1gMnSc1	blue-ringed
octopus	octopus	k1gMnSc1	octopus
videos	videos	k1gMnSc1	videos
<g/>
,	,	kIx,	,
photos	photos	k1gMnSc1	photos
and	and	k?	and
facts	facts	k1gInSc1	facts
-	-	kIx~	-
Hapalochlaena	Hapalochlaen	k2eAgFnSc1d1	Hapalochlaena
maculosa	maculosa	k1gFnSc1	maculosa
<g/>
.	.	kIx.	.
</s>
<s>
ARKive	ARKivat	k5eAaPmIp3nS	ARKivat
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MACCONNELL	MACCONNELL	kA	MACCONNELL
<g/>
,	,	kIx,	,
Ashleigh	Ashleigh	k1gInSc1	Ashleigh
<g/>
.	.	kIx.	.
</s>
<s>
Hapalochlaena	Hapalochlaen	k2eAgFnSc1d1	Hapalochlaena
maculosa	maculosa	k1gFnSc1	maculosa
<g/>
.	.	kIx.	.
</s>
<s>
Animal	animal	k1gMnSc1	animal
Diversity	Diversit	k1gInPc4	Diversit
Web	web	k1gInSc1	web
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
