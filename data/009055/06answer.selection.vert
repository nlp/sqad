<s>
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
pohoří	pohořet	k5eAaPmIp3nS
Gilboa	Gilboa	k1gFnSc1
<g/>
,	,	kIx,
cca	cca	kA
13	[number]	k4
kilometrů	kilometr	k1gInPc2
západně	západně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Bejt	Bejt	k?
Še	Še	k1gFnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
a	a	k8xC
2	[number]	k4
kilometry	kilometr	k1gInPc7
východně	východně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Gan	Gan	k1gMnSc2
Ner	Ner	k1gMnSc2
<g/>
.	.	kIx.
</s>