<p>
<s>
Har	Har	k?	Har
Giborim	Giborim	k1gInSc1	Giborim
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ה	ה	k?	ה
ג	ג	k?	ג
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
o	o	k7c6	o
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
cca	cca	kA	cca
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Gilboa	Gilbo	k1gInSc2	Gilbo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Gilboa	Gilboa	k1gFnSc1	Gilboa
<g/>
,	,	kIx,	,
cca	cca	kA	cca
13	[number]	k4	13
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
Bejt	Bejt	k?	Bejt
Še	Še	k1gFnSc1	Še
<g/>
'	'	kIx"	'
<g/>
an	an	k?	an
a	a	k8xC	a
2	[number]	k4	2
kilometry	kilometr	k1gInPc7	kilometr
východně	východně	k6eAd1	východně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Gan	Gan	k1gMnSc2	Gan
Ner	Ner	k1gMnSc2	Ner
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
výrazného	výrazný	k2eAgNnSc2d1	výrazné
návrší	návrší	k1gNnSc2	návrší
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
odlesněnou	odlesněný	k2eAgFnSc7d1	odlesněná
vrcholovou	vrcholový	k2eAgFnSc7d1	vrcholová
partií	partie	k1gFnSc7	partie
ale	ale	k8xC	ale
hustě	hustě	k6eAd1	hustě
zalesněnými	zalesněný	k2eAgInPc7d1	zalesněný
východními	východní	k2eAgInPc7d1	východní
svahy	svah	k1gInPc7	svah
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
vrcholku	vrcholek	k1gInSc2	vrcholek
vede	vést	k5eAaImIp3nS	vést
lokální	lokální	k2eAgFnSc1d1	lokální
silnice	silnice	k1gFnSc1	silnice
667	[number]	k4	667
<g/>
.	.	kIx.	.
</s>
<s>
Západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
terén	terén	k1gInSc1	terén
klesá	klesat	k5eAaImIp3nS	klesat
do	do	k7c2	do
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívaného	využívaný	k2eAgInSc2d1	využívaný
Jizre	Jizr	k1gInSc5	Jizr
<g/>
'	'	kIx"	'
<g/>
elského	elský	k2eAgNnSc2d1	elský
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
stéká	stékat	k5eAaImIp3nS	stékat
vádí	vádí	k1gNnSc4	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Gilboa	Gilboa	k1gMnSc1	Gilboa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadních	severozápadní	k2eAgInPc6d1	severozápadní
svazích	svah	k1gInPc6	svah
hory	hora	k1gFnSc2	hora
leží	ležet	k5eAaImIp3nS	ležet
výšina	výšina	k1gFnSc1	výšina
Ma	Ma	k1gFnSc1	Ma
<g/>
'	'	kIx"	'
<g/>
ale	ale	k8xC	ale
Nurit	Nurit	k1gInSc1	Nurit
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
severu	sever	k1gInSc2	sever
vádí	vádí	k1gNnSc2	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Nurit	Nurit	k1gMnSc1	Nurit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
odtud	odtud	k6eAd1	odtud
stojí	stát	k5eAaImIp3nS	stát
sousední	sousední	k2eAgInSc4d1	sousední
vrchol	vrchol	k1gInSc4	vrchol
Giv	Giv	k1gFnSc2	Giv
<g/>
'	'	kIx"	'
<g/>
at	at	k?	at
Chochit	Chochit	k1gInSc1	Chochit
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vrch	vrch	k1gInSc4	vrch
Har	Har	k1gMnSc2	Har
Ša	Ša	k1gMnSc2	Ša
<g/>
'	'	kIx"	'
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Po	po	k7c6	po
jižních	jižní	k2eAgInPc6d1	jižní
svazích	svah	k1gInPc6	svah
hory	hora	k1gFnSc2	hora
vede	vést	k5eAaImIp3nS	vést
izraelská	izraelský	k2eAgFnSc1d1	izraelská
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
bariéra	bariéra	k1gFnSc1	bariéra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
přilehlý	přilehlý	k2eAgInSc1d1	přilehlý
Západní	západní	k2eAgInSc1d1	západní
břeh	břeh	k1gInSc1	břeh
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
leží	ležet	k5eAaImIp3nS	ležet
palestinská	palestinský	k2eAgFnSc1d1	palestinská
vesnice	vesnice	k1gFnSc1	vesnice
Arabuna	Arabuna	k1gFnSc1	Arabuna
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
směrem	směr	k1gInSc7	směr
stéká	stékat	k5eAaImIp3nS	stékat
vádí	vádí	k1gNnSc4	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Chochit	Chochit	k1gMnSc1	Chochit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bejtše	Bejtše	k1gFnSc1	Bejtše
<g/>
'	'	kIx"	'
<g/>
anské	anský	k2eAgNnSc1d1	anské
údolí	údolí	k1gNnSc1	údolí
</s>
</p>
