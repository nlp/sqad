<p>
<s>
Otrokářství	otrokářství	k1gNnSc1	otrokářství
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
práce	práce	k1gFnSc1	práce
nesvobodných	svobodný	k2eNgMnPc2d1	nesvobodný
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
otroků	otrok	k1gMnPc2	otrok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
právní	právní	k2eAgFnSc6d1	právní
i	i	k8xC	i
faktické	faktický	k2eAgFnSc6d1	faktická
stránce	stránka	k1gFnSc6	stránka
obchodovatelným	obchodovatelný	k2eAgInSc7d1	obchodovatelný
majetkem	majetek	k1gInSc7	majetek
zbaveným	zbavený	k2eAgInSc7d1	zbavený
osobní	osobní	k2eAgFnPc4d1	osobní
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
otroctví	otroctví	k1gNnSc6	otroctví
i	i	k8xC	i
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
osoba	osoba	k1gFnSc1	osoba
na	na	k7c4	na
druhé	druhý	k4xOgMnPc4	druhý
natolik	natolik	k6eAd1	natolik
závislá	závislý	k2eAgFnSc1d1	závislá
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nucena	nutit	k5eAaImNgFnS	nutit
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
pracovat	pracovat	k5eAaImF	pracovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
zbavena	zbaven	k2eAgFnSc1d1	zbavena
osobní	osobní	k2eAgFnSc1d1	osobní
svobody	svoboda	k1gFnPc1	svoboda
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
teoreticky	teoreticky	k6eAd1	teoreticky
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
stav	stav	k1gInSc4	stav
protiprávní	protiprávní	k2eAgFnSc1d1	protiprávní
nebo	nebo	k8xC	nebo
právem	právem	k6eAd1	právem
přímo	přímo	k6eAd1	přímo
definovaný	definovaný	k2eAgInSc1d1	definovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neolitická	neolitický	k2eAgFnSc1d1	neolitická
revoluce	revoluce	k1gFnSc1	revoluce
je	být	k5eAaImIp3nS	být
spjata	spjat	k2eAgFnSc1d1	spjata
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
otrokářství	otrokářství	k1gNnSc2	otrokářství
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
bylo	být	k5eAaImAgNnS	být
legálně	legálně	k6eAd1	legálně
využívané	využívaný	k2eAgNnSc1d1	využívané
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
normální	normální	k2eAgFnSc4d1	normální
(	(	kIx(	(
<g/>
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
<g/>
)	)	kIx)	)
součást	součást	k1gFnSc4	součást
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
společenský	společenský	k2eAgInSc1d1	společenský
žebříček	žebříček	k1gInSc1	žebříček
a	a	k8xC	a
služba	služba	k1gFnSc1	služba
bohům	bůh	k1gMnPc3	bůh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starověké	starověký	k2eAgNnSc1d1	starověké
Řecko	Řecko	k1gNnSc1	Řecko
či	či	k8xC	či
starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
byli	být	k5eAaImAgMnP	být
existenčně	existenčně	k6eAd1	existenčně
závislé	závislý	k2eAgInPc4d1	závislý
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
hrálo	hrát	k5eAaImAgNnS	hrát
ještě	ještě	k9	ještě
při	při	k7c6	při
produkci	produkce	k1gFnSc6	produkce
americké	americký	k2eAgFnSc2d1	americká
bavlny	bavlna	k1gFnSc2	bavlna
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
práce	práce	k1gFnSc1	práce
otroků	otrok	k1gMnPc2	otrok
značně	značně	k6eAd1	značně
zlevňovala	zlevňovat	k5eAaImAgFnS	zlevňovat
její	její	k3xOp3gFnSc4	její
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
zvýhodňovala	zvýhodňovat	k5eAaImAgFnS	zvýhodňovat
ji	on	k3xPp3gFnSc4	on
tak	tak	k9	tak
na	na	k7c6	na
evropských	evropský	k2eAgInPc6d1	evropský
trzích	trh	k1gInPc6	trh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
zakázané	zakázaný	k2eAgNnSc1d1	zakázané
a	a	k8xC	a
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
stíhá	stíhat	k5eAaImIp3nS	stíhat
i	i	k9	i
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
právo	právo	k1gNnSc1	právo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
otroctví	otroctví	k1gNnSc6	otroctví
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
slovanských	slovanský	k2eAgFnPc6d1	Slovanská
zemích	zem	k1gFnPc6	zem
navíc	navíc	k6eAd1	navíc
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
otrok	otrok	k1gMnSc1	otrok
<g/>
"	"	kIx"	"
mělo	mít	k5eAaImAgNnS	mít
(	(	kIx(	(
<g/>
a	a	k8xC	a
někde	někde	k6eAd1	někde
i	i	k9	i
dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
<g/>
)	)	kIx)	)
význam	význam	k1gInSc4	význam
jiný	jiný	k2eAgInSc4d1	jiný
než	než	k8xS	než
"	"	kIx"	"
<g/>
sluha	sluha	k1gMnSc1	sluha
<g/>
"	"	kIx"	"
–	–	k?	–
nedospělé	dospělý	k2eNgFnPc4d1	nedospělá
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
dítě	dítě	k1gNnSc1	dítě
–	–	k?	–
"	"	kIx"	"
<g/>
otroče	otročit	k5eAaImSgMnS	otročit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
mladík	mladík	k1gMnSc1	mladík
<g/>
/	/	kIx~	/
<g/>
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
otrok	otrok	k1gMnSc1	otrok
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
či	či	k8xC	či
dívka	dívka	k1gFnSc1	dívka
<g/>
/	/	kIx~	/
<g/>
dcerka	dcerka	k1gFnSc1	dcerka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
otroczyca	otroczyca	k6eAd1	otroczyca
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
Velkomoravské	velkomoravský	k2eAgFnSc6d1	Velkomoravská
říši	říš	k1gFnSc6	říš
vybírala	vybírat	k5eAaImAgFnS	vybírat
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
zákazům	zákaz	k1gInPc3	zákaz
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
otroctvím	otroctví	k1gNnSc7	otroctví
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
civilizovaném	civilizovaný	k2eAgInSc6d1	civilizovaný
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
různé	různý	k2eAgInPc1d1	různý
nezákonné	zákonný	k2eNgInPc1d1	nezákonný
případy	případ	k1gInPc1	případ
naplňující	naplňující	k2eAgFnSc4d1	naplňující
širší	široký	k2eAgFnSc4d2	širší
definici	definice	k1gFnSc4	definice
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
faktickém	faktický	k2eAgNnSc6d1	faktické
otroctví	otroctví	k1gNnSc6	otroctví
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
drženy	držet	k5eAaImNgFnP	držet
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
obětí	oběť	k1gFnPc2	oběť
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
bílým	bílý	k2eAgNnSc7d1	bílé
masem	maso	k1gNnSc7	maso
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nuceny	nutit	k5eAaImNgFnP	nutit
k	k	k7c3	k
prostituci	prostituce	k1gFnSc3	prostituce
(	(	kIx(	(
<g/>
prakticky	prakticky	k6eAd1	prakticky
všude	všude	k6eAd1	všude
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
zatčena	zatčen	k2eAgFnSc1d1	zatčena
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
organizovala	organizovat	k5eAaBmAgFnS	organizovat
otrockou	otrocký	k2eAgFnSc4d1	otrocká
práci	práce	k1gFnSc4	práce
mentálně	mentálně	k6eAd1	mentálně
postižených	postižený	k1gMnPc2	postižený
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
otroctvím	otroctví	k1gNnSc7	otroctví
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
definici	definice	k1gFnSc3	definice
otroctví	otroctví	k1gNnSc2	otroctví
jako	jako	k8xS	jako
nesvobodné	svobodný	k2eNgFnSc2d1	nesvobodná
práce	práce	k1gFnSc2	práce
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
práce	práce	k1gFnSc1	práce
vězňů	vězeň	k1gMnPc2	vězeň
v	v	k7c6	v
nápravných	nápravný	k2eAgFnPc6d1	nápravná
institucích	instituce	k1gFnPc6	instituce
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nucená	nucený	k2eAgFnSc1d1	nucená
práce	práce	k1gFnSc1	práce
vězňů	vězeň	k1gMnPc2	vězeň
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
využívala	využívat	k5eAaPmAgFnS	využívat
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
nutilo	nutit	k5eAaImAgNnS	nutit
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
okupovaných	okupovaný	k2eAgFnPc2d1	okupovaná
zemí	zem	k1gFnPc2	zem
vykonávat	vykonávat	k5eAaImF	vykonávat
nucené	nucený	k2eAgFnPc4d1	nucená
práce	práce	k1gFnPc4	práce
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
válečném	válečný	k2eAgInSc6d1	válečný
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Obrovského	obrovský	k2eAgInSc2d1	obrovský
rozsahu	rozsah	k1gInSc2	rozsah
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
systém	systém	k1gInSc1	systém
pracovních	pracovní	k2eAgInPc2d1	pracovní
táborů	tábor	k1gInPc2	tábor
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
vzor	vzor	k1gInSc1	vzor
následoval	následovat	k5eAaImAgInS	následovat
i	i	k9	i
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rovněž	rovněž	k9	rovněž
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
síť	síť	k1gFnSc4	síť
táborů	tábor	k1gInPc2	tábor
nucené	nucený	k2eAgFnSc2d1	nucená
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Prací	práce	k1gFnSc7	práce
vězňů	vězeň	k1gMnPc2	vězeň
získávají	získávat	k5eAaImIp3nP	získávat
nezanedbatelné	zanedbatelný	k2eNgInPc1d1	nezanedbatelný
prostředky	prostředek	k1gInPc1	prostředek
firmy	firma	k1gFnSc2	firma
vlastnící	vlastnící	k2eAgNnPc4d1	vlastnící
nápravná	nápravný	k2eAgNnPc4d1	nápravné
zařízení	zařízení	k1gNnPc4	zařízení
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
jsou	být	k5eAaImIp3nP	být
za	za	k7c4	za
využívání	využívání	k1gNnSc4	využívání
nucené	nucený	k2eAgFnSc2d1	nucená
práce	práce	k1gFnSc2	práce
vězňů	vězeň	k1gMnPc2	vězeň
kritizovány	kritizován	k2eAgFnPc1d1	kritizována
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
Podle	podle	k7c2	podle
tiskové	tiskový	k2eAgFnSc2d1	tisková
zprávy	zpráva	k1gFnSc2	zpráva
ČTK	ČTK	kA	ČTK
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
táborech	tábor	k1gInPc6	tábor
pro	pro	k7c4	pro
politické	politický	k2eAgMnPc4d1	politický
vězně	vězeň	k1gMnPc4	vězeň
v	v	k7c4	v
KLDR	KLDR	kA	KLDR
drženo	držen	k2eAgNnSc1d1	drženo
na	na	k7c4	na
150	[number]	k4	150
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
150	[number]	k4	150
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Severokorejští	severokorejský	k2eAgMnPc1d1	severokorejský
vězňové	vězeň	k1gMnPc1	vězeň
jsou	být	k5eAaImIp3nP	být
zbaveni	zbavit	k5eAaPmNgMnP	zbavit
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
nuceni	nucen	k2eAgMnPc1d1	nucen
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
tvrdě	tvrdě	k6eAd1	tvrdě
pracovat	pracovat	k5eAaImF	pracovat
zejména	zejména	k9	zejména
v	v	k7c6	v
uhelných	uhelný	k2eAgInPc6d1	uhelný
dolech	dol	k1gInPc6	dol
<g/>
,	,	kIx,	,
při	při	k7c6	při
těžbě	těžba	k1gFnSc6	těžba
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
africkém	africký	k2eAgInSc6d1	africký
státě	stát	k1gInSc6	stát
Niger	Niger	k1gInSc4	Niger
bylo	být	k5eAaImAgNnS	být
otroctví	otroctví	k1gNnSc1	otroctví
postaveno	postavit	k5eAaPmNgNnS	postavit
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
humanitární	humanitární	k2eAgFnPc1d1	humanitární
organizace	organizace	k1gFnPc1	organizace
ale	ale	k9	ale
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
otroctví	otroctví	k1gNnSc6	otroctví
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
až	až	k9	až
8	[number]	k4	8
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Nigeru	Niger	k1gInSc2	Niger
<g/>
.	.	kIx.	.
</s>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
i	i	k9	i
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc1	Mali
a	a	k8xC	a
Súdán	Súdán	k1gInSc1	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
arabské	arabský	k2eAgFnSc2d1	arabská
a	a	k8xC	a
černé	černý	k2eAgFnSc2d1	černá
(	(	kIx(	(
<g/>
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
<g/>
)	)	kIx)	)
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Otroci	otrok	k1gMnPc1	otrok
jsou	být	k5eAaImIp3nP	být
příslušníci	příslušník	k1gMnPc1	příslušník
různých	různý	k2eAgNnPc2d1	různé
černošských	černošský	k2eAgNnPc2d1	černošské
etnik	etnikum	k1gNnPc2	etnikum
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
otroctví	otroctví	k1gNnSc6	otroctví
již	již	k9	již
po	po	k7c4	po
celé	celý	k2eAgFnPc4d1	celá
generace	generace	k1gFnPc4	generace
a	a	k8xC	a
přijali	přijmout	k5eAaPmAgMnP	přijmout
identitu	identita	k1gFnSc4	identita
svých	svůj	k3xOyFgMnPc2	svůj
pánů	pan	k1gMnPc2	pan
(	(	kIx(	(
<g/>
např.	např.	kA	např.
poarabštělí	poarabštělý	k2eAgMnPc1d1	poarabštělý
Haratínové	Haratín	k1gMnPc1	Haratín
černé	černý	k2eAgFnSc2d1	černá
pleti	pleť	k1gFnSc2	pleť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Majiteli	majitel	k1gMnPc7	majitel
černých	černý	k2eAgMnPc2d1	černý
otroků	otrok	k1gMnPc2	otrok
jsou	být	k5eAaImIp3nP	být
nomádské	nomádský	k2eAgInPc1d1	nomádský
kmeny	kmen	k1gInPc1	kmen
žijící	žijící	k2eAgInPc1d1	žijící
na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sahelu	Sahel	k1gInSc2	Sahel
-	-	kIx~	-
Arabové	Arab	k1gMnPc1	Arab
(	(	kIx(	(
<g/>
bílí	bílý	k1gMnPc1	bílý
Maurové	Maurové	k?	Maurové
v	v	k7c6	v
Mauritánii	Mauritánie	k1gFnSc6	Mauritánie
nebo	nebo	k8xC	nebo
baggarské	baggarský	k2eAgInPc1d1	baggarský
kmeny	kmen	k1gInPc1	kmen
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
berberští	berberský	k2eAgMnPc1d1	berberský
Tuaregové	Tuareg	k1gMnPc1	Tuareg
a	a	k8xC	a
Fulbové	Fulb	k1gMnPc1	Fulb
<g/>
.	.	kIx.	.
<g/>
Otrokářství	otrokářství	k1gNnSc1	otrokářství
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vnímáno	vnímat	k5eAaImNgNnS	vnímat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
typů	typ	k1gInPc2	typ
společenského	společenský	k2eAgInSc2d1	společenský
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
ze	z	k7c2	z
sociopsychologického	sociopsychologický	k2eAgInSc2d1	sociopsychologický
pohledu	pohled	k1gInSc2	pohled
představuje	představovat	k5eAaImIp3nS	představovat
extrémní	extrémní	k2eAgInSc1d1	extrémní
způsob	způsob	k1gInSc1	způsob
stratifikace	stratifikace	k1gFnSc1	stratifikace
(	(	kIx(	(
<g/>
rozvrstvení	rozvrstvení	k1gNnSc1	rozvrstvení
<g/>
)	)	kIx)	)
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
otevírá	otevírat	k5eAaImIp3nS	otevírat
závažné	závažný	k2eAgFnPc4d1	závažná
etické	etický	k2eAgFnPc4d1	etická
otázky	otázka	k1gFnPc4	otázka
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
otrokáře	otrokář	k1gMnSc2	otrokář
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
aktuální	aktuální	k2eAgInSc4d1	aktuální
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
tento	tento	k3xDgInSc4	tento
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
nějaký	nějaký	k3yIgInSc1	nějaký
hodnotový	hodnotový	k2eAgInSc1d1	hodnotový
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
náboženský	náboženský	k2eAgMnSc1d1	náboženský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dnes	dnes	k6eAd1	dnes
panuje	panovat	k5eAaImIp3nS	panovat
obecná	obecný	k2eAgFnSc1d1	obecná
shoda	shoda	k1gFnSc1	shoda
ohledně	ohledně	k7c2	ohledně
etiky	etika	k1gFnSc2	etika
otrokářství	otrokářství	k1gNnSc2	otrokářství
<g/>
,	,	kIx,	,
případy	případ	k1gInPc1	případ
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
zejména	zejména	k9	zejména
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
psychologie	psychologie	k1gFnSc1	psychologie
lidského	lidský	k2eAgMnSc2d1	lidský
jedince	jedinec	k1gMnSc2	jedinec
patrně	patrně	k6eAd1	patrně
neprošla	projít	k5eNaPmAgFnS	projít
nějakým	nějaký	k3yIgMnSc7	nějaký
zásadním	zásadní	k2eAgInSc7d1	zásadní
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
určité	určitý	k2eAgFnPc4d1	určitá
státní	státní	k2eAgFnPc4d1	státní
formy	forma	k1gFnPc4	forma
otrokářství	otrokářství	k1gNnSc2	otrokářství
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
zbavení	zbavení	k1gNnSc2	zbavení
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
určitých	určitý	k2eAgFnPc2d1	určitá
sociálních	sociální	k2eAgFnPc2d1	sociální
skupin	skupina	k1gFnPc2	skupina
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Současné	současný	k2eAgNnSc1d1	současné
otroctví	otroctví	k1gNnSc1	otroctví
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
otroctví	otroctví	k1gNnSc4	otroctví
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
zemi	zem	k1gFnSc6	zem
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
počet	počet	k1gInSc1	počet
otroků	otrok	k1gMnPc2	otrok
odhadován	odhadován	k2eAgMnSc1d1	odhadován
mezi	mezi	k7c7	mezi
12	[number]	k4	12
miliony	milion	k4xCgInPc7	milion
a	a	k8xC	a
29,8	[number]	k4	29,8
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
organizace	organizace	k1gFnSc2	organizace
Walk	Walk	k1gMnSc1	Walk
Free	Free	k1gFnPc2	Free
Foundation	Foundation	k1gInSc4	Foundation
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
otroku	otrok	k1gMnSc3	otrok
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
2,9	[number]	k4	2,9
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
(	(	kIx(	(
<g/>
2,1	[number]	k4	2,1
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nigérie	Nigérie	k1gFnSc1	Nigérie
<g/>
,	,	kIx,	,
Ethiopie	Ethiopie	k1gFnSc1	Ethiopie
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Myanmar	Myanmar	k1gInSc1	Myanmar
a	a	k8xC	a
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
<g/>
;	;	kIx,	;
zatímco	zatímco	k8xS	zatímco
mezi	mezi	k7c7	mezi
zeměmi	zem	k1gFnPc7	zem
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
podílem	podíl	k1gInSc7	podíl
otroků	otrok	k1gMnPc2	otrok
byla	být	k5eAaImAgFnS	být
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
<g/>
,	,	kIx,	,
Haiti	Haiti	k1gNnSc1	Haiti
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
a	a	k8xC	a
Nepál	Nepál	k1gInSc1	Nepál
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
vydalo	vydat	k5eAaPmAgNnS	vydat
americké	americký	k2eAgNnSc1d1	americké
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc1	zahraničí
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c4	o
otroctví	otroctví	k1gNnSc4	otroctví
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zařadila	zařadit	k5eAaPmAgFnS	zařadit
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
Čínu	Čína	k1gFnSc4	Čína
a	a	k8xC	a
Uzbekistán	Uzbekistán	k1gInSc4	Uzbekistán
na	na	k7c4	na
nejhorší	zlý	k2eAgNnPc4d3	nejhorší
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Súdán	Súdán	k1gInSc1	Súdán
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
a	a	k8xC	a
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
se	se	k3xPyFc4	se
ocitly	ocitnout	k5eAaPmAgFnP	ocitnout
také	také	k9	také
na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
i	i	k9	i
Alžírsko	Alžírsko	k1gNnSc4	Alžírsko
<g/>
,	,	kIx,	,
Libyi	Libye	k1gFnSc4	Libye
<g/>
,	,	kIx,	,
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
a	a	k8xC	a
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
mezi	mezi	k7c7	mezi
celkem	celek	k1gInSc7	celek
21	[number]	k4	21
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Otroctví	otroctví	k1gNnPc4	otroctví
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
==	==	k?	==
</s>
</p>
<p>
<s>
Slovanské	slovanský	k2eAgNnSc1d1	slovanské
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
otrok	otrok	k1gMnSc1	otrok
<g/>
"	"	kIx"	"
původně	původně	k6eAd1	původně
značilo	značit	k5eAaImAgNnS	značit
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
neměl	mít	k5eNaImAgMnS	mít
právo	právo	k1gNnSc4	právo
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
-	-	kIx~	-
tj.	tj.	kA	tj.
sněmu	sněm	k1gInSc2	sněm
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
pro	pro	k7c4	pro
nízký	nízký	k2eAgInSc4d1	nízký
věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
příčin	příčina	k1gFnPc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
otrok	otrok	k1gMnSc1	otrok
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
prostě	prostě	k9	prostě
"	"	kIx"	"
<g/>
dítě	dítě	k1gNnSc1	dítě
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Praha	Praha	k1gFnSc1	Praha
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
velkých	velký	k2eAgNnPc2d1	velké
center	centrum	k1gNnPc2	centrum
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
ovšem	ovšem	k9	ovšem
i	i	k9	i
doklady	doklad	k1gInPc1	doklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
otroctví	otroctví	k1gNnSc1	otroctví
existovalo	existovat	k5eAaImAgNnS	existovat
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
nálezy	nález	k1gInPc1	nález
okovů	okov	k1gInPc2	okov
<g/>
,	,	kIx,	,
arabské	arabský	k2eAgFnSc2d1	arabská
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
trhu	trh	k1gInSc6	trh
Moravanů	Moravan	k1gMnPc2	Moravan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Karpatské	karpatský	k2eAgFnSc2d1	Karpatská
kotliny	kotlina	k1gFnSc2	kotlina
Maďary	Maďar	k1gMnPc7	Maďar
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přesunu	přesun	k1gInSc3	přesun
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
obchodní	obchodní	k2eAgFnSc2d1	obchodní
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spojovala	spojovat	k5eAaImAgFnS	spojovat
Cordóbský	Cordóbský	k2eAgInSc4d1	Cordóbský
chalífát	chalífát	k1gInSc4	chalífát
s	s	k7c7	s
Kyjevem	Kyjev	k1gInSc7	Kyjev
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
chazarskými	chazarský	k2eAgInPc7d1	chazarský
trhy	trh	k1gInPc7	trh
na	na	k7c6	na
Volze	Volha	k1gFnSc6	Volha
<g/>
,	,	kIx,	,
na	na	k7c4	na
sever	sever	k1gInSc4	sever
-	-	kIx~	-
vedla	vést	k5eAaImAgFnS	vést
tak	tak	k6eAd1	tak
nyní	nyní	k6eAd1	nyní
přes	přes	k7c4	přes
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
Krakov	Krakov	k1gInSc4	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
trh	trh	k1gInSc1	trh
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
velmi	velmi	k6eAd1	velmi
výhodného	výhodný	k2eAgNnSc2d1	výhodné
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
využil	využít	k5eAaPmAgMnS	využít
ještě	ještě	k9	ještě
dále	daleko	k6eAd2	daleko
k	k	k7c3	k
východní	východní	k2eAgFnSc3d1	východní
expanzi	expanze	k1gFnSc3	expanze
podél	podél	k7c2	podél
této	tento	k3xDgFnSc2	tento
obchodní	obchodní	k2eAgFnSc2d1	obchodní
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
proudícím	proudící	k2eAgNnSc7d1	proudící
zbožím	zboží	k1gNnSc7	zboží
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
získat	získat	k5eAaPmF	získat
pohanské	pohanský	k2eAgMnPc4d1	pohanský
otroky	otrok	k1gMnPc4	otrok
k	k	k7c3	k
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
prodeji	prodej	k1gInSc3	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc1	období
právě	právě	k6eAd1	právě
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
míru	mír	k1gInSc2	mír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vybraná	vybraná	k1gFnSc1	vybraná
od	od	k7c2	od
svobodných	svobodný	k2eAgMnPc2d1	svobodný
domácích	domácí	k2eAgMnPc2d1	domácí
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
rok	rok	k1gInSc4	rok
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
ceně	cena	k1gFnSc3	cena
asi	asi	k9	asi
90	[number]	k4	90
otroků	otrok	k1gMnPc2	otrok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolaps	kolaps	k1gInSc1	kolaps
této	tento	k3xDgFnSc2	tento
politiky	politika	k1gFnSc2	politika
nadešel	nadejít	k5eAaPmAgInS	nadejít
až	až	k9	až
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nové	nový	k2eAgInPc4d1	nový
zdroje	zdroj	k1gInPc4	zdroj
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
lov	lov	k1gInSc4	lov
<g/>
"	"	kIx"	"
pohanských	pohanský	k2eAgInPc2d1	pohanský
otroků	otrok	k1gMnPc2	otrok
nebyly	být	k5eNaImAgInP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc6d1	východní
oblasti	oblast	k1gFnSc6	oblast
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
státu	stát	k1gInSc2	stát
připadly	připadnout	k5eAaPmAgInP	připadnout
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
státům	stát	k1gInPc3	stát
(	(	kIx(	(
<g/>
Kyjevské	kyjevský	k2eAgFnSc6d1	Kyjevská
Rusi	Rus	k1gFnSc6	Rus
a	a	k8xC	a
Krakovsko	Krakovsko	k1gNnSc4	Krakovsko
Polsku	Polska	k1gFnSc4	Polska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
snaha	snaha	k1gFnSc1	snaha
prodávat	prodávat	k5eAaImF	prodávat
do	do	k7c2	do
muslimských	muslimský	k2eAgFnPc2d1	muslimská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
židovských	židovský	k2eAgMnPc2d1	židovský
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
otroky	otrok	k1gMnPc4	otrok
i	i	k8xC	i
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
církev	církev	k1gFnSc1	církev
rozhodně	rozhodně	k6eAd1	rozhodně
nevítala	vítat	k5eNaImAgFnS	vítat
–	–	k?	–
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
to	ten	k3xDgNnSc1	ten
spor	spor	k1gInSc1	spor
o	o	k7c4	o
mezi	mezi	k7c7	mezi
Slavníkovcem	Slavníkovec	k1gMnSc7	Slavníkovec
sv.	sv.	kA	sv.
Vojtěchem	Vojtěch	k1gMnSc7	Vojtěch
a	a	k8xC	a
Boleslavem	Boleslav	k1gMnSc7	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
totiž	totiž	k9	totiž
proti	proti	k7c3	proti
prodeji	prodej	k1gInSc3	prodej
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
otroků	otrok	k1gMnPc2	otrok
jinověrcům	jinověrec	k1gMnPc3	jinověrec
protestoval	protestovat	k5eAaBmAgMnS	protestovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
u	u	k7c2	u
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
elitních	elitní	k2eAgFnPc2d1	elitní
vrstev	vrstva	k1gFnPc2	vrstva
nesetkalo	setkat	k5eNaPmAgNnS	setkat
s	s	k7c7	s
pochopením	pochopení	k1gNnSc7	pochopení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
nemoci	nemoc	k1gFnSc2	nemoc
knížete	kníže	k1gMnSc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gFnSc1	jeho
družina	družina	k1gFnSc1	družina
rod	rod	k1gInSc4	rod
Slavníkovců	Slavníkovec	k1gMnPc2	Slavníkovec
vyvraždila	vyvraždit	k5eAaPmAgFnS	vyvraždit
<g/>
.	.	kIx.	.
<g/>
Existence	existence	k1gFnSc1	existence
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ne	ne	k9	ne
jako	jako	k8xS	jako
hybné	hybný	k2eAgFnPc1d1	hybná
síly	síla	k1gFnPc1	síla
ekonomiky	ekonomika	k1gFnSc2	ekonomika
raně	raně	k6eAd1	raně
středověkého	středověký	k2eAgInSc2d1	středověký
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
doložena	doložen	k2eAgFnSc1d1	doložena
v	v	k7c6	v
písemných	písemný	k2eAgInPc6d1	písemný
pramenech	pramen	k1gInPc6	pramen
až	až	k9	až
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
byl	být	k5eAaImAgInS	být
institut	institut	k1gInSc1	institut
otroctví	otroctví	k1gNnSc2	otroctví
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
státě	stát	k1gInSc6	stát
právně	právně	k6eAd1	právně
ošetřen	ošetřen	k2eAgInSc1d1	ošetřen
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc4d1	známo
datum	datum	k1gNnSc4	datum
jeho	jeho	k3xOp3gNnSc2	jeho
legislativního	legislativní	k2eAgNnSc2d1	legislativní
zrušení	zrušení	k1gNnSc2	zrušení
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Komunismus	komunismus	k1gInSc1	komunismus
a	a	k8xC	a
otroctví	otroctví	k1gNnSc1	otroctví
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
Komunistickým	komunistický	k2eAgInSc7d1	komunistický
státem	stát	k1gInSc7	stát
zřízené	zřízený	k2eAgFnPc1d1	zřízená
nápravné	nápravný	k2eAgFnPc1d1	nápravná
pracovní	pracovní	k2eAgFnPc1d1	pracovní
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
tábory	tábor	k1gInPc4	tábor
nucených	nucený	k2eAgFnPc2d1	nucená
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
pomocné	pomocný	k2eAgInPc4d1	pomocný
technické	technický	k2eAgInPc4d1	technický
prapory	prapor	k1gInPc4	prapor
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
věznice	věznice	k1gFnPc4	věznice
<g/>
,	,	kIx,	,
představovaly	představovat	k5eAaImAgInP	představovat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
novodobé	novodobý	k2eAgFnSc2d1	novodobá
otrokářské	otrokářský	k2eAgFnSc2d1	otrokářská
instituce	instituce	k1gFnSc2	instituce
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byli	být	k5eAaImAgMnP	být
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
a	a	k8xC	a
"	"	kIx"	"
<g/>
politicky	politicky	k6eAd1	politicky
nespolehlivých	spolehlivý	k2eNgMnPc2d1	nespolehlivý
<g/>
"	"	kIx"	"
občanů	občan	k1gMnPc2	občan
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kulaků	kulak	k1gMnPc2	kulak
<g/>
,	,	kIx,	,
členů	člen	k1gMnPc2	člen
nekomunistických	komunistický	k2eNgFnPc2d1	nekomunistická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
,	,	kIx,	,
hrdinů	hrdina	k1gMnPc2	hrdina
západního	západní	k2eAgInSc2d1	západní
odboje	odboj	k1gInSc2	odboj
a	a	k8xC	a
další	další	k1gNnSc1	další
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
(	(	kIx(	(
<g/>
předpokládaným	předpokládaný	k2eAgMnSc7d1	předpokládaný
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
nepřátelským	přátelský	k2eNgInSc7d1	nepřátelský
poměrem	poměr	k1gInSc7	poměr
k	k	k7c3	k
socialistickému	socialistický	k2eAgNnSc3d1	socialistické
zřízení	zřízení	k1gNnSc3	zřízení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nuceny	nutit	k5eAaImNgFnP	nutit
k	k	k7c3	k
otrocké	otrocký	k2eAgFnSc3d1	otrocká
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
<g/>
Pracovalo	pracovat	k5eAaImAgNnS	pracovat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
v	v	k7c6	v
nelidských	lidský	k2eNgFnPc6d1	nelidská
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
s	s	k7c7	s
nesplnitelnými	splnitelný	k2eNgFnPc7d1	nesplnitelná
normami	norma	k1gFnPc7	norma
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
nenaplnění	nenaplnění	k1gNnSc1	nenaplnění
bylo	být	k5eAaImAgNnS	být
tvrdě	tvrdě	k6eAd1	tvrdě
postihováno	postihovat	k5eAaImNgNnS	postihovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
odvětvích	odvětví	k1gNnPc6	odvětví
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
počtu	počet	k1gInSc3	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
či	či	k8xC	či
doživotnímu	doživotní	k2eAgNnSc3d1	doživotní
zmrzačení	zmrzačení	k1gNnSc3	zmrzačení
vězňů	vězeň	k1gMnPc2	vězeň
a	a	k8xC	a
napravovaných	napravovaný	k2eAgMnPc2d1	napravovaný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dodržování	dodržování	k1gNnSc1	dodržování
běžných	běžný	k2eAgFnPc2d1	běžná
pravidel	pravidlo	k1gNnPc2	pravidlo
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
práce	práce	k1gFnSc2	práce
bylo	být	k5eAaImAgNnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
luxus	luxus	k1gInSc4	luxus
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
"	"	kIx"	"
<g/>
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
pracujících	pracující	k1gMnPc2	pracující
<g/>
"	"	kIx"	"
nemá	mít	k5eNaImIp3nS	mít
nárok	nárok	k1gInSc4	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
skupinou	skupina	k1gFnSc7	skupina
podrobenou	podrobený	k2eAgFnSc7d1	podrobená
nezákonné	zákonný	k2eNgFnPc4d1	nezákonná
internaci	internace	k1gFnSc4	internace
a	a	k8xC	a
využívanou	využívaný	k2eAgFnSc4d1	využívaná
jako	jako	k8xS	jako
levná	levný	k2eAgFnSc1d1	levná
pracovní	pracovní	k2eAgFnSc1d1	pracovní
síla	síla	k1gFnSc1	síla
představovaly	představovat	k5eAaImAgFnP	představovat
řeholní	řeholní	k2eAgFnPc4d1	řeholní
sestry	sestra	k1gFnPc4	sestra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sexuální	sexuální	k2eAgNnSc4d1	sexuální
otroctví	otroctví	k1gNnSc4	otroctví
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Také	také	k9	také
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
určité	určitý	k2eAgFnPc4d1	určitá
formy	forma	k1gFnPc4	forma
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
opakovaně	opakovaně	k6eAd1	opakovaně
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
nevládními	vládní	k2eNgFnPc7d1	nevládní
organizacemi	organizace	k1gFnPc7	organizace
za	za	k7c4	za
laxní	laxní	k2eAgInSc4d1	laxní
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
obchodu	obchod	k1gInSc3	obchod
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
souvisejícím	související	k2eAgNnSc7d1	související
sexuálním	sexuální	k2eAgNnSc7d1	sexuální
otroctvím	otroctví	k1gNnSc7	otroctví
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
je	být	k5eAaImIp3nS	být
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
rakouském	rakouský	k2eAgNnSc6d1	rakouské
a	a	k8xC	a
německém	německý	k2eAgNnSc6d1	německé
pohraničí	pohraničí	k1gNnSc6	pohraničí
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
trestní	trestní	k2eAgNnSc1d1	trestní
právo	právo	k1gNnSc1	právo
otroctví	otroctví	k1gNnSc2	otroctví
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
jako	jako	k9	jako
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
zbavení	zbavení	k1gNnSc2	zbavení
osobní	osobní	k2eAgFnSc2d1	osobní
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgFnSc2d1	historická
podoby	podoba	k1gFnSc2	podoba
otrokářství	otrokářství	k1gNnSc2	otrokářství
==	==	k?	==
</s>
</p>
<p>
<s>
Neolitická	neolitický	k2eAgFnSc1d1	neolitická
revoluce	revoluce	k1gFnSc1	revoluce
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
otrokářství	otrokářství	k1gNnSc2	otrokářství
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
předchozí	předchozí	k2eAgMnPc1d1	předchozí
lovci	lovec	k1gMnPc1	lovec
a	a	k8xC	a
sběrači	sběrač	k1gMnPc1	sběrač
byli	být	k5eAaImAgMnP	být
společností	společnost	k1gFnSc7	společnost
rovnostářskou	rovnostářský	k2eAgFnSc7d1	rovnostářská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
většinou	většinou	k6eAd1	většinou
polygamní	polygamní	k2eAgFnSc1d1	polygamní
<g/>
,	,	kIx,	,
neznaje	neznat	k5eAaImSgInS	neznat
otcovství	otcovství	k1gNnSc2	otcovství
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc1	majetek
či	či	k8xC	či
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otroctví	otroctví	k1gNnPc4	otroctví
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
velmi	velmi	k6eAd1	velmi
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
,	,	kIx,	,
zmiňováno	zmiňován	k2eAgNnSc1d1	zmiňováno
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
v	v	k7c6	v
nejstarších	starý	k2eAgFnPc6d3	nejstarší
literárních	literární	k2eAgFnPc6d1	literární
památkách	památka	k1gFnPc6	památka
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Chammurapiho	Chammurapi	k1gMnSc4	Chammurapi
zákoník	zákoník	k1gInSc1	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
bylo	být	k5eAaImAgNnS	být
postavení	postavení	k1gNnSc1	postavení
otroka	otrok	k1gMnSc2	otrok
buď	buď	k8xC	buď
jako	jako	k9	jako
věci	věc	k1gFnSc2	věc
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
luxusní	luxusní	k2eAgNnSc1d1	luxusní
a	a	k8xC	a
méně	málo	k6eAd2	málo
časté	častý	k2eAgNnSc1d1	časté
<g/>
)	)	kIx)	)
či	či	k8xC	či
otroka	otrok	k1gMnSc4	otrok
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
splácel	splácet	k5eAaImAgMnS	splácet
svůj	svůj	k3xOyFgInSc4	svůj
dluh	dluh	k1gInSc4	dluh
(	(	kIx(	(
<g/>
častější	častý	k2eAgFnSc1d2	častější
varianta	varianta	k1gFnSc1	varianta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohl	moct	k5eNaImAgInS	moct
býti	být	k5eAaImF	být
prodán	prodat	k5eAaPmNgInS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
se	se	k3xPyFc4	se
rozmáhá	rozmáhat	k5eAaImIp3nS	rozmáhat
během	během	k7c2	během
Střední	střední	k2eAgFnSc2d1	střední
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
otroci	otrok	k1gMnPc1	otrok
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
značkováni	značkován	k2eAgMnPc1d1	značkován
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
útěku	útěk	k1gInSc2	útěk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otroctví	otroctví	k1gNnPc4	otroctví
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
mělo	mít	k5eAaImAgNnS	mít
otroctví	otroctví	k1gNnSc4	otroctví
nejprve	nejprve	k6eAd1	nejprve
patriarchální	patriarchální	k2eAgFnSc4d1	patriarchální
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
státu	stát	k1gInSc2	stát
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
chápání	chápání	k1gNnSc2	chápání
postavení	postavení	k1gNnSc2	postavení
otroka	otrok	k1gMnSc2	otrok
z	z	k7c2	z
neplnoprávného	plnoprávný	k2eNgMnSc2d1	plnoprávný
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
živou	živý	k2eAgFnSc4d1	živá
věc	věc	k1gFnSc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
otroka	otrok	k1gMnSc2	otrok
římské	římský	k2eAgNnSc1d1	římské
právo	právo	k1gNnSc1	právo
definovalo	definovat	k5eAaBmAgNnS	definovat
zásadou	zásada	k1gFnSc7	zásada
servus	servus	k?	servus
nullum	nullum	k1gInSc4	nullum
caput	caput	k2eAgInSc4d1	caput
habet	habet	k1gInSc4	habet
(	(	kIx(	(
<g/>
otrok	otrok	k1gMnSc1	otrok
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
"	"	kIx"	"
<g/>
hlavu	hlava	k1gFnSc4	hlava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
právní	právní	k2eAgFnSc1d1	právní
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
)	)	kIx)	)
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
však	však	k9	však
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
případech	případ	k1gInPc6	případ
slevováno	slevován	k2eAgNnSc4d1	slevován
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
počet	počet	k1gInSc1	počet
státních	státní	k2eAgMnPc2d1	státní
i	i	k8xC	i
soukromých	soukromý	k2eAgMnPc2d1	soukromý
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
otroky	otrok	k1gMnPc7	otrok
stávali	stávat	k5eAaImAgMnP	stávat
jako	jako	k9	jako
potomci	potomek	k1gMnPc1	potomek
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
váleční	váleční	k2eAgMnPc1d1	váleční
zajatci	zajatec	k1gMnPc1	zajatec
či	či	k8xC	či
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
důvodu	důvod	k1gInSc2	důvod
(	(	kIx(	(
<g/>
zotročení	zotročení	k1gNnSc1	zotročení
pro	pro	k7c4	pro
nevděk	nevděk	k1gInSc4	nevděk
<g/>
,	,	kIx,	,
zotročení	zotročení	k1gNnSc4	zotročení
pro	pro	k7c4	pro
dluhy	dluh	k1gInPc4	dluh
<g/>
,	,	kIx,	,
podvodný	podvodný	k2eAgInSc4d1	podvodný
prodej	prodej	k1gInSc4	prodej
svobodného	svobodný	k2eAgMnSc2d1	svobodný
občana	občan	k1gMnSc2	občan
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
prodávaný	prodávaný	k2eAgMnSc1d1	prodávaný
dobrovolně	dobrovolně	k6eAd1	dobrovolně
účastnil	účastnit	k5eAaImAgMnS	účastnit
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nesměl	smět	k5eNaImAgMnS	smět
být	být	k5eAaImF	být
svobodný	svobodný	k2eAgMnSc1d1	svobodný
člověk	člověk	k1gMnSc1	člověk
bezdůvodně	bezdůvodně	k6eAd1	bezdůvodně
zotročen	zotročen	k2eAgMnSc1d1	zotročen
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
k	k	k7c3	k
takovým	takový	k3xDgFnPc3	takový
porušením	porušení	k1gNnSc7	porušení
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
fázích	fáze	k1gFnPc6	fáze
dějin	dějiny	k1gFnPc2	dějiny
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
často	často	k6eAd1	často
docházelo	docházet	k5eAaImAgNnS	docházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
otroctví	otroctví	k1gNnSc2	otroctví
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
člověk	člověk	k1gMnSc1	člověk
propuštěn	propustit	k5eAaPmNgMnS	propustit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
dokonce	dokonce	k9	dokonce
pojil	pojit	k5eAaImAgInS	pojit
zákon	zákon	k1gInSc1	zákon
propuštění	propuštění	k1gNnSc2	propuštění
z	z	k7c2	z
otroctví	otroctví	k1gNnSc2	otroctví
s	s	k7c7	s
vykonáním	vykonání	k1gNnSc7	vykonání
nějakého	nějaký	k3yIgInSc2	nějaký
skutku	skutek	k1gInSc2	skutek
či	či	k8xC	či
dlouhodobější	dlouhodobý	k2eAgNnSc4d2	dlouhodobější
jednání	jednání	k1gNnSc4	jednání
(	(	kIx(	(
<g/>
např.	např.	kA	např.
záchrana	záchrana	k1gFnSc1	záchrana
života	život	k1gInSc2	život
císaři	císař	k1gMnSc3	císař
či	či	k8xC	či
pracování	pracování	k1gNnSc2	pracování
určitý	určitý	k2eAgInSc4d1	určitý
počet	počet	k1gInSc4	počet
let	léto	k1gNnPc2	léto
v	v	k7c6	v
nějakém	nějaký	k3yIgNnSc6	nějaký
povolání	povolání	k1gNnSc6	povolání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
císařství	císařství	k1gNnSc2	císařství
bylo	být	k5eAaImAgNnS	být
propouštění	propouštění	k1gNnSc1	propouštění
otroků	otrok	k1gMnPc2	otrok
natolik	natolik	k6eAd1	natolik
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
vydány	vydán	k2eAgInPc1d1	vydán
zákony	zákon	k1gInPc1	zákon
(	(	kIx(	(
<g/>
Lex	Lex	k1gFnSc1	Lex
Fufia	Fufium	k1gNnSc2	Fufium
Caninia	Caninium	k1gNnSc2	Caninium
a	a	k8xC	a
Lex	Lex	k1gMnSc2	Lex
Aelia	Aelius	k1gMnSc2	Aelius
Sentia	Sentius	k1gMnSc2	Sentius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
propouštění	propouštění	k1gNnSc4	propouštění
omezovaly	omezovat	k5eAaImAgFnP	omezovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
fázi	fáze	k1gFnSc6	fáze
dějin	dějiny	k1gFnPc2	dějiny
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
často	často	k6eAd1	často
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
splývání	splývání	k1gNnSc3	splývání
postavení	postavení	k1gNnSc2	postavení
kolónů	kolón	k1gInPc2	kolón
a	a	k8xC	a
otroků	otrok	k1gMnPc2	otrok
a	a	k8xC	a
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
křesťanství	křesťanství	k1gNnSc2	křesťanství
i	i	k8xC	i
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
chápání	chápání	k1gNnSc2	chápání
osoby	osoba	k1gFnSc2	osoba
otroka	otrok	k1gMnSc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
přešel	přejít	k5eAaPmAgInS	přejít
otrokářský	otrokářský	k2eAgInSc4d1	otrokářský
systém	systém	k1gInSc4	systém
plynule	plynule	k6eAd1	plynule
do	do	k7c2	do
systému	systém	k1gInSc2	systém
feudálního	feudální	k2eAgInSc2d1	feudální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otroctví	otroctví	k1gNnPc4	otroctví
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Izraeli	Izrael	k1gInSc6	Izrael
===	===	k?	===
</s>
</p>
<p>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Izraeli	Izrael	k1gInSc6	Izrael
se	se	k3xPyFc4	se
odlišovalo	odlišovat	k5eAaImAgNnS	odlišovat
od	od	k7c2	od
institutu	institut	k1gInSc2	institut
otroctví	otroctví	k1gNnSc2	otroctví
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
starověkých	starověký	k2eAgFnPc6d1	starověká
společnostech	společnost	k1gFnPc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
vyplývala	vyplývat	k5eAaImAgFnS	vyplývat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
náboženství	náboženství	k1gNnSc1	náboženství
starého	starý	k2eAgInSc2d1	starý
Izraele	Izrael	k1gInSc2	Izrael
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
také	také	k9	také
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
ojedinělé	ojedinělý	k2eAgFnSc2d1	ojedinělá
etické	etický	k2eAgFnSc2d1	etická
učení	učení	k1gNnSc3	učení
a	a	k8xC	a
právní	právní	k2eAgInSc4d1	právní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byly	být	k5eAaImAgFnP	být
silné	silný	k2eAgInPc4d1	silný
důrazy	důraz	k1gInPc4	důraz
na	na	k7c4	na
sociální	sociální	k2eAgNnSc4d1	sociální
cítění	cítění	k1gNnSc4	cítění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
židovském	židovský	k2eAgNnSc6d1	Židovské
právu	právo	k1gNnSc6	právo
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
otrok	otrok	k1gMnSc1	otrok
lidskou	lidský	k2eAgFnSc4d1	lidská
bytostí	bytost	k1gFnSc7	bytost
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
subjektem	subjekt	k1gInSc7	subjekt
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
byl	být	k5eAaImAgInS	být
nositelem	nositel	k1gMnSc7	nositel
práv	práv	k2eAgMnSc1d1	práv
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíšův	Mojžíšův	k2eAgInSc1d1	Mojžíšův
zákon	zákon	k1gInSc1	zákon
chránil	chránit	k5eAaImAgMnS	chránit
otroka	otrok	k1gMnSc4	otrok
proti	proti	k7c3	proti
svévolnému	svévolný	k2eAgNnSc3d1	svévolné
mrzačení	mrzačení	k1gNnSc3	mrzačení
a	a	k8xC	a
stíhal	stíhat	k5eAaImAgMnS	stíhat
jeho	jeho	k3xOp3gNnSc4	jeho
usmrcení	usmrcení	k1gNnSc4	usmrcení
otrokovým	otrokův	k2eAgMnSc7d1	otrokův
pánem	pán	k1gMnSc7	pán
<g/>
.	.	kIx.	.
</s>
<s>
Otrokův	otrokův	k2eAgInSc1d1	otrokův
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k9	jako
život	život	k1gInSc4	život
kteréhokoli	kterýkoli	k3yIgMnSc2	kterýkoli
jiného	jiný	k2eAgMnSc2d1	jiný
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Deuteronomium	Deuteronomium	k1gNnSc1	Deuteronomium
(	(	kIx(	(
<g/>
23,15	[number]	k4	23,15
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
dokonce	dokonce	k9	dokonce
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
uprchlým	uprchlý	k2eAgMnPc3d1	uprchlý
otrokům	otrok	k1gMnPc3	otrok
azyl	azyl	k1gInSc4	azyl
a	a	k8xC	a
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
jejich	jejich	k3xOp3gNnSc1	jejich
vydání	vydání	k1gNnSc1	vydání
jejich	jejich	k3xOp3gMnPc3	jejich
pánům	pan	k1gMnPc3	pan
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ustanovení	ustanovení	k1gNnSc1	ustanovení
ostře	ostro	k6eAd1	ostro
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
z	z	k7c2	z
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
starověkého	starověký	k2eAgInSc2d1	starověký
právního	právní	k2eAgInSc2d1	právní
systému	systém	k1gInSc2	systém
a	a	k8xC	a
zejména	zejména	k9	zejména
z	z	k7c2	z
práva	právo	k1gNnSc2	právo
římského	římský	k2eAgNnSc2d1	římské
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
izraelských	izraelský	k2eAgMnPc2d1	izraelský
otroků	otrok	k1gMnPc2	otrok
existovala	existovat	k5eAaImAgFnS	existovat
ustanovení	ustanovení	k1gNnSc4	ustanovení
o	o	k7c6	o
sobotním	sobotní	k2eAgInSc6d1	sobotní
odpočinku	odpočinek	k1gInSc6	odpočinek
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
20,10	[number]	k4	20,10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ustanovení	ustanovení	k1gNnPc1	ustanovení
o	o	k7c6	o
účasti	účast	k1gFnSc6	účast
na	na	k7c6	na
slavení	slavení	k1gNnSc6	slavení
pesachu	pesach	k1gInSc2	pesach
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
12,44	[number]	k4	12,44
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svátků	svátek	k1gInPc2	svátek
týdnů	týden	k1gInPc2	týden
(	(	kIx(	(
<g/>
Dt	Dt	k1gFnPc2	Dt
16,11	[number]	k4	16,11
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ustanovení	ustanovení	k1gNnPc2	ustanovení
časově	časově	k6eAd1	časově
omezující	omezující	k2eAgNnPc1d1	omezující
otroctví	otroctví	k1gNnPc1	otroctví
otroků	otrok	k1gMnPc2	otrok
izraelského	izraelský	k2eAgInSc2d1	izraelský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
21,2	[number]	k4	21,2
<g/>
;	;	kIx,	;
Dt	Dt	k1gFnSc1	Dt
15,12	[number]	k4	15,12
<g/>
)	)	kIx)	)
a	a	k8xC	a
chránící	chránící	k2eAgInPc1d1	chránící
zájmy	zájem	k1gInPc1	zájem
otrokyň	otrokyně	k1gFnPc2	otrokyně
prodaných	prodaný	k2eAgFnPc2d1	prodaná
za	za	k7c2	za
družky	družka	k1gFnSc2	družka
svých	svůj	k3xOyFgMnPc2	svůj
pánů	pan	k1gMnPc2	pan
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
21,7	[number]	k4	21,7
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Arabský	arabský	k2eAgInSc1d1	arabský
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
===	===	k?	===
</s>
</p>
<p>
<s>
Třetina	třetina	k1gFnSc1	třetina
populace	populace	k1gFnSc2	populace
raných	raný	k2eAgInPc2d1	raný
islámských	islámský	k2eAgInPc2d1	islámský
statů	status	k1gInPc2	status
západního	západní	k2eAgInSc2d1	západní
Súdánu	Súdán	k1gInSc2	Súdán
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Západní	západní	k2eAgFnSc1d1	západní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Ghana	Ghana	k1gFnSc1	Ghana
(	(	kIx(	(
<g/>
750	[number]	k4	750
<g/>
–	–	k?	–
<g/>
1076	[number]	k4	1076
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc1	Mali
(	(	kIx(	(
<g/>
1235	[number]	k4	1235
<g/>
–	–	k?	–
<g/>
1645	[number]	k4	1645
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Segou	Segá	k1gFnSc4	Segá
(	(	kIx(	(
<g/>
1712	[number]	k4	1712
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
a	a	k8xC	a
Songhaj	Songhaj	k1gMnSc1	Songhaj
(	(	kIx(	(
<g/>
1275	[number]	k4	1275
<g/>
–	–	k?	–
<g/>
1591	[number]	k4	1591
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
zotročena	zotročen	k2eAgFnSc1d1	zotročena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otroci	otrok	k1gMnPc1	otrok
byli	být	k5eAaImAgMnP	být
zakoupeni	zakoupit	k5eAaPmNgMnP	zakoupit
nebo	nebo	k8xC	nebo
odchyceni	odchytit	k5eAaPmNgMnP	odchytit
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
islámského	islámský	k2eAgInSc2d1	islámský
světa	svět	k1gInSc2	svět
a	a	k8xC	a
poté	poté	k6eAd1	poté
importováni	importovat	k5eAaBmNgMnP	importovat
do	do	k7c2	do
hlavních	hlavní	k2eAgFnPc2d1	hlavní
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhaly	probíhat	k5eAaImAgInP	probíhat
trhy	trh	k1gInPc1	trh
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
odkud	odkud	k6eAd1	odkud
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
distribuováni	distribuován	k2eAgMnPc1d1	distribuován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
černí	černý	k2eAgMnPc1d1	černý
otroci	otrok	k1gMnPc1	otrok
mohli	moct	k5eAaImAgMnP	moct
představovat	představovat	k5eAaImF	představovat
alespoň	alespoň	k9	alespoň
polovinu	polovina	k1gFnSc4	polovina
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
dolního	dolní	k2eAgInSc2d1	dolní
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
mnoho	mnoho	k6eAd1	mnoho
otroků	otrok	k1gMnPc2	otrok
v	v	k7c6	v
regionu	region	k1gInSc6	region
také	také	k9	také
pocházelo	pocházet	k5eAaImAgNnS	pocházet
ze	z	k7c2	z
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
otroků	otrok	k1gMnPc2	otrok
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
pořízeno	pořídit	k5eAaPmNgNnS	pořídit
za	za	k7c2	za
válek	válka	k1gFnPc2	válka
s	s	k7c7	s
křesťanskými	křesťanský	k2eAgInPc7d1	křesťanský
národy	národ	k1gInPc7	národ
středověké	středověký	k2eAgFnSc2d1	středověká
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otroctví	otroctví	k1gNnSc3	otroctví
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
Josef	Josef	k1gMnSc1	Josef
Žemlička	Žemlička	k1gMnSc1	Žemlička
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Čechy	Čech	k1gMnPc7	Čech
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgNnSc4d1	knížecí
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
provozovali	provozovat	k5eAaImAgMnP	provozovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
raný	raný	k2eAgInSc4d1	raný
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
vrcholný	vrcholný	k2eAgInSc4d1	vrcholný
středověk	středověk	k1gInSc4	středověk
většinou	většinou	k6eAd1	většinou
židé	žid	k1gMnPc1	žid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Praha	Praha	k1gFnSc1	Praha
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
hlavním	hlavní	k2eAgFnPc3d1	hlavní
evropským	evropský	k2eAgFnPc3d1	Evropská
překladištím	překladiště	k1gNnPc3	překladiště
</s>
</p>
<p>
<s>
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
západních	západní	k2eAgMnPc2d1	západní
i	i	k8xC	i
východních	východní	k2eAgMnPc2d1	východní
Slovanů	Slovan	k1gMnPc2	Slovan
mířily	mířit	k5eAaImAgInP	mířit
otrocké	otrocký	k2eAgInPc1d1	otrocký
transporty	transport	k1gInPc1	transport
na	na	k7c4	na
trhy	trh	k1gInPc4	trh
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
orientu	orient	k1gInSc6	orient
<g/>
.	.	kIx.	.
</s>
<s>
Židovští	židovský	k2eAgMnPc1d1	židovský
obchodníci	obchodník	k1gMnPc1	obchodník
také	také	k9	také
dopravovali	dopravovat	k5eAaImAgMnP	dopravovat
kolem	kolem	k7c2	kolem
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
na	na	k7c4	na
uherské	uherský	k2eAgInPc4d1	uherský
trhy	trh	k1gInPc4	trh
jak	jak	k6eAd1	jak
cizí	cizí	k2eAgMnPc4d1	cizí
zajatce	zajatec	k1gMnPc4	zajatec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
část	část	k1gFnSc4	část
zotročeného	zotročený	k2eAgInSc2d1	zotročený
domácího	domácí	k2eAgInSc2d1	domácí
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
Evropané	Evropan	k1gMnPc1	Evropan
využívali	využívat	k5eAaImAgMnP	využívat
otroky	otrok	k1gMnPc7	otrok
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
se	se	k3xPyFc4	se
přepočítávala	přepočítávat	k5eAaImAgFnS	přepočítávat
na	na	k7c4	na
životy	život	k1gInPc4	život
otroků	otrok	k1gMnPc2	otrok
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Nicméně	nicméně	k8xC	nicméně
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
za	za	k7c4	za
nelegální	legální	k2eNgInSc4d1	nelegální
veškerý	veškerý	k3xTgInSc4	veškerý
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zákaz	zákaz	k1gInSc4	zákaz
následovali	následovat	k5eAaImAgMnP	následovat
Dánové	Dán	k1gMnPc1	Dán
<g/>
,	,	kIx,	,
Švédové	Švéd	k1gMnPc1	Švéd
<g/>
,	,	kIx,	,
a	a	k8xC	a
dovoz	dovoz	k1gInSc1	dovoz
otroků	otrok	k1gMnPc2	otrok
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
zakázaly	zakázat	k5eAaPmAgInP	zakázat
také	také	k9	také
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1823	[number]	k4	1823
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
ukončili	ukončit	k5eAaPmAgMnP	ukončit
Portugalci	Portugalec	k1gMnPc1	Portugalec
a	a	k8xC	a
Holanďané	Holanďan	k1gMnPc1	Holanďan
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
námořní	námořní	k2eAgMnPc1d1	námořní
kapitáni	kapitán	k1gMnPc1	kapitán
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
transatlantickém	transatlantický	k2eAgInSc6d1	transatlantický
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
lví	lví	k2eAgInSc4d1	lví
podíl	podíl	k1gInSc4	podíl
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
zákonem	zákon	k1gInSc7	zákon
zakázali	zakázat	k5eAaPmAgMnP	zakázat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otroctví	otroctví	k1gNnSc3	otroctví
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
===	===	k?	===
</s>
</p>
<p>
<s>
Atlantický	atlantický	k2eAgInSc1d1	atlantický
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1441	[number]	k4	1441
<g/>
,	,	kIx,	,
když	když	k8xS	když
portugalský	portugalský	k2eAgMnSc1d1	portugalský
kapitán	kapitán	k1gMnSc1	kapitán
Antam	Antam	k1gInSc1	Antam
Gonçalvez	Gonçalvez	k1gInSc1	Gonçalvez
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Sahary	Sahara	k1gFnSc2	Sahara
získal	získat	k5eAaPmAgInS	získat
dva	dva	k4xCgMnPc4	dva
černé	černý	k2eAgMnPc4d1	černý
otroky	otrok	k1gMnPc4	otrok
<g/>
,	,	kIx,	,
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodal	dodat	k5eAaPmAgMnS	dodat
je	být	k5eAaImIp3nS	být
Jindřichu	Jindřich	k1gMnSc3	Jindřich
Mořeplavci	mořeplavec	k1gMnSc3	mořeplavec
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
rytířského	rytířský	k2eAgInSc2d1	rytířský
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
Portugalci	Portugalec	k1gMnPc1	Portugalec
postavili	postavit	k5eAaPmAgMnP	postavit
pevnost	pevnost	k1gFnSc4	pevnost
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Arguin	Arguina	k1gFnPc2	Arguina
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Mauretánie	Mauretánie	k1gFnSc2	Mauretánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
vzkvétal	vzkvétat	k5eAaImAgMnS	vzkvétat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středomoří	středomoří	k1gNnPc2	středomoří
(	(	kIx(	(
<g/>
Evropané	Evropan	k1gMnPc1	Evropan
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
naučili	naučit	k5eAaPmAgMnP	naučit
vyrábět	vyrábět	k5eAaImF	vyrábět
cukr	cukr	k1gInSc4	cukr
od	od	k7c2	od
Arabů	Arab	k1gMnPc2	Arab
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Křížových	Křížových	k2eAgFnPc2d1	Křížových
výprav	výprava	k1gFnPc2	výprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zisku	zisk	k1gInSc2	zisk
otroků	otrok	k1gMnPc2	otrok
ovšem	ovšem	k9	ovšem
Portugalci	Portugalec	k1gMnPc1	Portugalec
chtěli	chtít	k5eAaImAgMnP	chtít
získat	získat	k5eAaPmF	získat
i	i	k9	i
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
trh	trh	k1gInSc4	trh
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1415	[number]	k4	1415
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
udržet	udržet	k5eAaPmF	udržet
marockou	marocký	k2eAgFnSc4d1	marocká
Ceutu	Ceuta	k1gFnSc4	Ceuta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
podíl	podíl	k1gInSc4	podíl
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
komoditou	komodita	k1gFnSc7	komodita
nemalý	malý	k2eNgInSc4d1	nemalý
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Arguin	Arguin	k1gInSc1	Arguin
tak	tak	k9	tak
měl	mít	k5eAaImAgInS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
obchodní	obchodní	k2eAgNnSc4d1	obchodní
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
karavany	karavana	k1gFnPc4	karavana
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
mířící	mířící	k2eAgFnSc4d1	mířící
jinak	jinak	k6eAd1	jinak
do	do	k7c2	do
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otrokářství	otrokářství	k1gNnSc1	otrokářství
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
běžné	běžný	k2eAgNnSc1d1	běžné
i	i	k9	i
v	v	k7c6	v
samotných	samotný	k2eAgFnPc6d1	samotná
afrických	africký	k2eAgFnPc6d1	africká
společnostech	společnost	k1gFnPc6	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
řídce	řídce	k6eAd1	řídce
osídlených	osídlený	k2eAgFnPc6d1	osídlená
oblastech	oblast	k1gFnPc6	oblast
trpěly	trpět	k5eAaImAgFnP	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
pracovní	pracovní	k2eAgFnPc4d1	pracovní
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Portugalci	Portugalec	k1gMnPc1	Portugalec
tak	tak	k6eAd1	tak
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
získali	získat	k5eAaPmAgMnP	získat
řadu	řada	k1gFnSc4	řada
obchodních	obchodní	k2eAgMnPc2d1	obchodní
partnerů	partner	k1gMnPc2	partner
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
zapojení	zapojení	k1gNnSc2	zapojení
již	jenž	k3xRgFnSc4	jenž
dříve	dříve	k6eAd2	dříve
–	–	k?	–
například	například	k6eAd1	například
Maurové	Maurové	k?	Maurové
či	či	k8xC	či
království	království	k1gNnSc1	království
Wolofů	Wolof	k1gInPc2	Wolof
(	(	kIx(	(
<g/>
tamní	tamní	k2eAgMnSc1d1	tamní
král	král	k1gMnSc1	král
neváhal	váhat	k5eNaImAgMnS	váhat
Maurům	Maur	k1gMnPc3	Maur
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
Portugalcům	Portugalec	k1gMnPc3	Portugalec
prodat	prodat	k5eAaPmF	prodat
i	i	k9	i
své	svůj	k3xOyFgFnPc4	svůj
poddané	poddaná	k1gFnPc4	poddaná
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
koně	kůň	k1gMnPc4	kůň
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
devět	devět	k4xCc4	devět
až	až	k9	až
čtrnáct	čtrnáct	k4xCc4	čtrnáct
otroků	otrok	k1gMnPc2	otrok
získal	získat	k5eAaPmAgMnS	získat
král	král	k1gMnSc1	král
jednoho	jeden	k4xCgMnSc4	jeden
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgMnPc2	jenž
pořádal	pořádat	k5eAaImAgMnS	pořádat
razie	razie	k1gFnSc2	razie
na	na	k7c4	na
další	další	k2eAgMnPc4d1	další
otroky	otrok	k1gMnPc4	otrok
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
tak	tak	k6eAd1	tak
z	z	k7c2	z
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
kmenů	kmen	k1gInPc2	kmen
ovšem	ovšem	k9	ovšem
se	se	k3xPyFc4	se
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
obchodu	obchod	k1gInSc2	obchod
odmítala	odmítat	k5eAaImAgFnS	odmítat
zapojit	zapojit	k5eAaPmF	zapojit
(	(	kIx(	(
<g/>
do	do	k7c2	do
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vyhýbal	vyhýbat	k5eAaImAgInS	vyhýbat
lid	lid	k1gInSc1	lid
Jola	jola	k1gFnSc1	jola
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc2	on
nezúčastnil	zúčastnit	k5eNaPmAgInS	zúčastnit
lid	lid	k1gInSc1	lid
Baga	Baga	k?	Baga
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Guinea	guinea	k1gFnSc2	guinea
<g/>
)	)	kIx)	)
či	či	k8xC	či
Kru	kra	k1gFnSc4	kra
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Libérie	Libérie	k1gFnSc1	Libérie
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
příslušníci	příslušník	k1gMnPc1	příslušník
těchto	tento	k3xDgInPc2	tento
kmenů	kmen	k1gInPc2	kmen
kladli	klást	k5eAaImAgMnP	klást
v	v	k7c4	v
otroctví	otroctví	k1gNnPc4	otroctví
velký	velký	k2eAgInSc4d1	velký
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
zabít	zabít	k5eAaPmF	zabít
své	svůj	k3xOyFgMnPc4	svůj
pány	pan	k1gMnPc4	pan
či	či	k8xC	či
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nakonec	nakonec	k6eAd1	nakonec
Evropany	Evropan	k1gMnPc4	Evropan
zcela	zcela	k6eAd1	zcela
odradilo	odradit	k5eAaPmAgNnS	odradit
od	od	k7c2	od
jejich	jejich	k3xOp3gNnPc2	jejich
zotročování	zotročování	k1gNnPc2	zotročování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1482	[number]	k4	1482
Portugalci	Portugalec	k1gMnPc1	Portugalec
založili	založit	k5eAaPmAgMnP	založit
pevnost	pevnost	k1gFnSc4	pevnost
Elmínu	Elmín	k1gInSc2	Elmín
na	na	k7c6	na
Zlatém	zlatý	k2eAgNnSc6d1	Zlaté
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
obešli	obejít	k5eAaPmAgMnP	obejít
transsaharský	transsaharský	k2eAgInSc4d1	transsaharský
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
přístup	přístup	k1gInSc4	přístup
přímo	přímo	k6eAd1	přímo
ke	k	k7c3	k
zdroji	zdroj	k1gInSc3	zdroj
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1506	[number]	k4	1506
již	již	k6eAd1	již
tvořily	tvořit	k5eAaImAgInP	tvořit
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
příjmů	příjem	k1gInPc2	příjem
portugalské	portugalský	k2eAgFnSc2d1	portugalská
koruny	koruna	k1gFnSc2	koruna
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1700	[number]	k4	1700
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
výnosnosti	výnosnost	k1gFnSc6	výnosnost
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
otroci	otrok	k1gMnPc1	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Akanů	Akan	k1gInPc2	Akan
na	na	k7c6	na
Zlatém	zlatý	k2eAgNnSc6d1	Zlaté
pobřeží	pobřeží	k1gNnSc6	pobřeží
bylo	být	k5eAaImAgNnS	být
zlato	zlato	k1gNnSc1	zlato
kupováno	kupovat	k5eAaImNgNnS	kupovat
za	za	k7c4	za
otroky	otrok	k1gMnPc4	otrok
dovážené	dovážený	k2eAgMnPc4d1	dovážený
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Koně	kůň	k1gMnPc1	kůň
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
podmínkách	podmínka	k1gFnPc6	podmínka
totiž	totiž	k9	totiž
neměli	mít	k5eNaImAgMnP	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
životnost	životnost	k1gFnSc4	životnost
a	a	k8xC	a
dřívější	dřívější	k2eAgFnPc4d1	dřívější
dodávky	dodávka	k1gFnPc4	dodávka
palných	palný	k2eAgFnPc2d1	palná
zbraní	zbraň	k1gFnPc2	zbraň
zakázal	zakázat	k5eAaPmAgMnS	zakázat
papež	papež	k1gMnSc1	papež
v	v	k7c6	v
obavě	obava	k1gFnSc6	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Otroky	otrok	k1gMnPc4	otrok
sem	sem	k6eAd1	sem
Portugalci	Portugalec	k1gMnPc1	Portugalec
dováželi	dovážet	k5eAaImAgMnP	dovážet
především	především	k6eAd1	především
z	z	k7c2	z
Beninu	Benin	k1gInSc2	Benin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mohl	moct	k5eAaImAgInS	moct
po	po	k7c6	po
územních	územní	k2eAgFnPc6d1	územní
expanzích	expanze	k1gFnPc6	expanze
prodávat	prodávat	k5eAaImF	prodávat
zajatce	zajatec	k1gMnPc4	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
strachu	strach	k1gInSc6	strach
ze	z	k7c2	z
ztráty	ztráta	k1gFnSc2	ztráta
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
ovšem	ovšem	k9	ovšem
místní	místní	k2eAgMnPc4d1	místní
vládce	vládce	k1gMnPc4	vládce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1516	[number]	k4	1516
vývoz	vývoz	k1gInSc4	vývoz
otroků	otrok	k1gMnPc2	otrok
zakázal	zakázat	k5eAaPmAgMnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Portugalci	Portugalec	k1gMnPc1	Portugalec
tak	tak	k6eAd1	tak
Akanům	Akano	k1gNnPc3	Akano
dodávali	dodávat	k5eAaImAgMnP	dodávat
otroky	otrok	k1gMnPc4	otrok
z	z	k7c2	z
delty	delta	k1gFnSc2	delta
Nigeru	Niger	k1gInSc2	Niger
a	a	k8xC	a
Iby	Iby	k1gMnSc1	Iby
žijící	žijící	k2eAgMnSc1d1	žijící
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
začali	začít	k5eAaPmAgMnP	začít
Portugalci	Portugalec	k1gMnPc1	Portugalec
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
pěstovat	pěstovat	k5eAaImF	pěstovat
cukrovou	cukrový	k2eAgFnSc4d1	cukrová
třtinu	třtina	k1gFnSc4	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Pracovní	pracovní	k2eAgFnSc4d1	pracovní
sílu	síla	k1gFnSc4	síla
získávali	získávat	k5eAaImAgMnP	získávat
v	v	k7c6	v
nedalekém	daleký	k2eNgNnSc6d1	nedaleké
Kongu	Kongo	k1gNnSc6	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Konžský	konžský	k2eAgMnSc1d1	konžský
král	král	k1gMnSc1	král
Afonso	Afonsa	k1gFnSc5	Afonsa
Mbemba	Mbemba	k1gMnSc1	Mbemba
Nzinga	Nzing	k1gMnSc4	Nzing
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1506	[number]	k4	1506
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Portugalců	Portugalec	k1gMnPc2	Portugalec
přijal	přijmout	k5eAaPmAgMnS	přijmout
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
technologie	technologie	k1gFnSc1	technologie
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
obchodoval	obchodovat	k5eAaImAgMnS	obchodovat
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgMnS	být
křesťanem	křesťan	k1gMnSc7	křesťan
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
značný	značný	k2eAgInSc4d1	značný
úpadek	úpadek	k1gInSc4	úpadek
a	a	k8xC	a
vylidnění	vylidnění	k1gNnSc4	vylidnění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
obchodem	obchod	k1gInSc7	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
–	–	k?	–
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Konga	Kongo	k1gNnSc2	Kongo
se	se	k3xPyFc4	se
zotročovali	zotročovat	k5eAaImAgMnP	zotročovat
navzájem	navzájem	k6eAd1	navzájem
v	v	k7c6	v
touze	touha	k1gFnSc6	touha
po	po	k7c6	po
portugalském	portugalský	k2eAgNnSc6d1	portugalské
zboží	zboží	k1gNnSc6	zboží
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1526	[number]	k4	1526
Portugalci	Portugalec	k1gMnPc1	Portugalec
z	z	k7c2	z
Konga	Kongo	k1gNnSc2	Kongo
odváželi	odvážet	k5eAaImAgMnP	odvážet
dva	dva	k4xCgMnPc1	dva
až	až	k6eAd1	až
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
otroků	otrok	k1gMnPc2	otrok
ročně	ročně	k6eAd1	ročně
–	–	k?	–
Afonso	Afonsa	k1gFnSc5	Afonsa
ovšem	ovšem	k9	ovšem
obchod	obchod	k1gInSc1	obchod
omezil	omezit	k5eAaPmAgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
království	království	k1gNnSc1	království
nadále	nadále	k6eAd1	nadále
rozmohlo	rozmoct	k5eAaPmAgNnS	rozmoct
a	a	k8xC	a
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
až	až	k6eAd1	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Portugalci	Portugalec	k1gMnPc1	Portugalec
následně	následně	k6eAd1	následně
založili	založit	k5eAaPmAgMnP	založit
významné	významný	k2eAgNnSc4d1	významné
středisko	středisko	k1gNnSc4	středisko
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
v	v	k7c6	v
Luandě	Luanda	k1gFnSc6	Luanda
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1576	[number]	k4	1576
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otroky	otrok	k1gMnPc4	otrok
odtud	odtud	k6eAd1	odtud
vozili	vozit	k5eAaImAgMnP	vozit
do	do	k7c2	do
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
na	na	k7c4	na
Madeiru	Madeira	k1gFnSc4	Madeira
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1532	[number]	k4	1532
též	též	k9	též
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
domorodé	domorodý	k2eAgNnSc1d1	domorodé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
decimováno	decimovat	k5eAaBmNgNnS	decimovat
epidemiemi	epidemie	k1gFnPc7	epidemie
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1540	[number]	k4	1540
se	se	k3xPyFc4	se
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
i	i	k9	i
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
vyšší	vysoký	k2eAgFnSc3d2	vyšší
imunitě	imunita	k1gFnSc3	imunita
se	se	k3xPyFc4	se
afričtí	africký	k2eAgMnPc1d1	africký
otroci	otrok	k1gMnPc1	otrok
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
osvědčili	osvědčit	k5eAaPmAgMnP	osvědčit
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tam	tam	k6eAd1	tam
již	již	k6eAd1	již
mířilo	mířit	k5eAaImAgNnS	mířit
80	[number]	k4	80
<g/>
%	%	kIx~	%
afrického	africký	k2eAgInSc2d1	africký
vývozu	vývoz	k1gInSc2	vývoz
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výsadní	výsadní	k2eAgFnSc4d1	výsadní
pozici	pozice	k1gFnSc4	pozice
Portugalců	Portugalec	k1gMnPc2	Portugalec
zničili	zničit	k5eAaPmAgMnP	zničit
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
severní	severní	k2eAgFnPc4d1	severní
Brazílie	Brazílie	k1gFnPc4	Brazílie
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1630	[number]	k4	1630
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Elmíny	Elmín	k1gInPc1	Elmín
(	(	kIx(	(
<g/>
1637	[number]	k4	1637
<g/>
)	)	kIx)	)
a	a	k8xC	a
přechodně	přechodně	k6eAd1	přechodně
i	i	k9	i
Luandy	Luanda	k1gFnSc2	Luanda
(	(	kIx(	(
<g/>
1641	[number]	k4	1641
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
velice	velice	k6eAd1	velice
nízké	nízký	k2eAgFnPc4d1	nízká
ceny	cena	k1gFnPc4	cena
pak	pak	k6eAd1	pak
otroky	otrok	k1gMnPc7	otrok
vyváželi	vyvážet	k5eAaImAgMnP	vyvážet
hlavně	hlavně	k9	hlavně
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
pozice	pozice	k1gFnSc1	pozice
posléze	posléze	k6eAd1	posléze
převzali	převzít	k5eAaPmAgMnP	převzít
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
společností	společnost	k1gFnPc2	společnost
se	s	k7c7	s
zvláštními	zvláštní	k2eAgNnPc7d1	zvláštní
privilegii	privilegium	k1gNnPc7	privilegium
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Royal	Royal	k1gMnSc1	Royal
African	African	k1gMnSc1	African
Company	Compana	k1gFnSc2	Compana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1672	[number]	k4	1672
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
soukromí	soukromý	k2eAgMnPc1d1	soukromý
obchodníci	obchodník	k1gMnPc1	obchodník
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
-	-	kIx~	-
jejich	jejich	k3xOp3gFnSc2	jejich
společnosti	společnost	k1gFnSc2	společnost
sídlili	sídlit	k5eAaImAgMnP	sídlit
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
a	a	k8xC	a
Nantes	Nantes	k1gMnSc1	Nantes
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
na	na	k7c4	na
někdejší	někdejší	k2eAgFnSc4d1	někdejší
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
kolonii	kolonie	k1gFnSc4	kolonie
Haiti	Haiti	k1gNnSc2	Haiti
byl	být	k5eAaImAgInS	být
dovezen	dovezen	k2eAgInSc1d1	dovezen
necelý	celý	k2eNgInSc1d1	necelý
milion	milion	k4xCgInSc4	milion
Afričanů	Afričan	k1gMnPc2	Afričan
(	(	kIx(	(
<g/>
ti	ten	k3xDgMnPc1	ten
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
vzpouru	vzpoura	k1gFnSc4	vzpoura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nebyli	být	k5eNaImAgMnP	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
střízlivých	střízlivý	k2eAgInPc2d1	střízlivý
odhadů	odhad	k1gInPc2	odhad
arabští	arabský	k2eAgMnPc1d1	arabský
otrokáři	otrokář	k1gMnPc1	otrokář
v	v	k7c6	v
osmnáctém	osmnáctý	k4xOgInSc6	osmnáctý
a	a	k8xC	a
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
století	století	k1gNnSc2	století
odvlekli	odvléct	k5eAaPmAgMnP	odvléct
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
deset	deset	k4xCc4	deset
až	až	k6eAd1	až
osmnáct	osmnáct	k4xCc4	osmnáct
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Arabští	arabský	k2eAgMnPc1d1	arabský
otrokáři	otrokář	k1gMnPc1	otrokář
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgMnPc1d1	aktivní
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otrokářství	otrokářství	k1gNnSc1	otrokářství
mělo	mít	k5eAaImAgNnS	mít
jednoznačně	jednoznačně	k6eAd1	jednoznačně
záporné	záporný	k2eAgInPc4d1	záporný
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
růst	růst	k1gInSc4	růst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
Afriky	Afrika	k1gFnSc2	Afrika
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
reálné	reálný	k2eAgInPc4d1	reálný
dopady	dopad	k1gInPc4	dopad
přítomnosti	přítomnost	k1gFnSc2	přítomnost
Evropanů	Evropan	k1gMnPc2	Evropan
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
vzít	vzít	k5eAaPmF	vzít
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
Evropané	Evropan	k1gMnPc1	Evropan
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
zavlekli	zavleknout	k5eAaPmAgMnP	zavleknout
některé	některý	k3yIgFnPc4	některý
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
byla	být	k5eAaImAgFnS	být
území	území	k1gNnPc4	území
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
dříve	dříve	k6eAd2	dříve
ušetřena	ušetřen	k2eAgFnSc1d1	ušetřena
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
izolaci	izolace	k1gFnSc4	izolace
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
sem	sem	k6eAd1	sem
Evropané	Evropan	k1gMnPc1	Evropan
přinesli	přinést	k5eAaPmAgMnP	přinést
vysokokalorické	vysokokalorický	k2eAgFnPc4d1	vysokokalorická
plodiny	plodina	k1gFnPc4	plodina
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
–	–	k?	–
manihot	manihot	k1gInSc1	manihot
<g/>
(	(	kIx(	(
<g/>
kasava	kasava	k1gFnSc1	kasava
<g/>
)	)	kIx)	)
a	a	k8xC	a
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Demografické	demografický	k2eAgInPc4d1	demografický
dopady	dopad	k1gInPc4	dopad
těchto	tento	k3xDgFnPc2	tento
změn	změna	k1gFnPc2	změna
byly	být	k5eAaImAgInP	být
jistě	jistě	k9	jistě
také	také	k9	také
významné	významný	k2eAgNnSc1d1	významné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otroctví	otroctví	k1gNnSc3	otroctví
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
počátek	počátek	k1gInSc4	počátek
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kolonizace	kolonizace	k1gFnSc2	kolonizace
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
otroctvím	otroctví	k1gNnSc7	otroctví
samotným	samotný	k2eAgNnSc7d1	samotné
začal	začít	k5eAaPmAgInS	začít
Francisco	Francisco	k6eAd1	Francisco
Pizarro	Pizarro	k1gNnSc4	Pizarro
<g/>
,	,	kIx,	,
když	když	k8xS	když
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
pracovat	pracovat	k5eAaImF	pracovat
místní	místní	k2eAgMnPc1d1	místní
Inkové	Ink	k1gMnPc1	Ink
<g/>
,	,	kIx,	,
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
míře	míra	k1gFnSc6	míra
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
byli	být	k5eAaImAgMnP	být
závislí	závislý	k2eAgMnPc1d1	závislý
i	i	k9	i
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otrokářství	otrokářství	k1gNnSc1	otrokářství
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
využívání	využívání	k1gNnSc1	využívání
místních	místní	k2eAgMnPc2d1	místní
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
Španělů	Španěl	k1gMnPc2	Španěl
spíše	spíše	k9	spíše
výjimkou	výjimka	k1gFnSc7	výjimka
než	než	k8xS	než
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
,	,	kIx,	,
rozšířenější	rozšířený	k2eAgInSc1d2	rozšířenější
byl	být	k5eAaImAgInS	být
dovoz	dovoz	k1gInSc1	dovoz
černých	černý	k2eAgMnPc2d1	černý
otroků	otrok	k1gMnPc2	otrok
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Portugalci	Portugalec	k1gMnPc1	Portugalec
zpočátku	zpočátku	k6eAd1	zpočátku
využívali	využívat	k5eAaPmAgMnP	využívat
jihoamerické	jihoamerický	k2eAgMnPc4d1	jihoamerický
indiány	indián	k1gMnPc4	indián
častěji	často	k6eAd2	často
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
podnikali	podnikat	k5eAaImAgMnP	podnikat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
otrokářské	otrokářský	k2eAgFnPc4d1	otrokářská
výpravy	výprava	k1gFnPc4	výprava
i	i	k9	i
do	do	k7c2	do
španělských	španělský	k2eAgFnPc2d1	španělská
držav	država	k1gFnPc2	država
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
jezuitských	jezuitský	k2eAgFnPc2d1	jezuitská
redukcí	redukce	k1gFnPc2	redukce
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
praxi	praxe	k1gFnSc4	praxe
ukončily	ukončit	k5eAaPmAgInP	ukončit
až	až	k9	až
drtivé	drtivý	k2eAgFnPc1d1	drtivá
porážky	porážka	k1gFnPc1	porážka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
u	u	k7c2	u
Caazapá	Caazapý	k2eAgFnSc1d1	Caazapý
Guazú	Guazú	k1gFnSc1	Guazú
(	(	kIx(	(
<g/>
1639	[number]	k4	1639
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Mbororé	Mbororý	k2eAgNnSc1d1	Mbororý
(	(	kIx(	(
<g/>
1641	[number]	k4	1641
<g/>
)	)	kIx)	)
připravily	připravit	k5eAaPmAgInP	připravit
jezuity	jezuita	k1gMnSc2	jezuita
vyzbrojené	vyzbrojený	k2eAgFnSc2d1	vyzbrojená
a	a	k8xC	a
vedené	vedený	k2eAgFnSc2d1	vedená
indiánské	indiánský	k2eAgFnSc2d1	indiánská
milice	milice	k1gFnSc2	milice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konec	konec	k1gInSc1	konec
otroctví	otroctví	k1gNnSc2	otroctví
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
přišel	přijít	k5eAaPmAgInS	přijít
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podpisem	podpis	k1gInSc7	podpis
princezny-regentky	princeznyegentka	k1gFnSc2	princezny-regentka
Isabely	Isabela	k1gFnSc2	Isabela
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
tzv.	tzv.	kA	tzv.
Zlatý	zlatý	k2eAgInSc1d1	zlatý
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
důsledcích	důsledek	k1gInPc6	důsledek
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
puč	puč	k1gInSc1	puč
zorganizovaný	zorganizovaný	k2eAgInSc1d1	zorganizovaný
rozzuřenými	rozzuřený	k2eAgMnPc7d1	rozzuřený
plantážníky	plantážník	k1gMnPc7	plantážník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
přidali	přidat	k5eAaPmAgMnP	přidat
k	k	k7c3	k
republikánům	republikán	k1gMnPc3	republikán
<g/>
.	.	kIx.	.
</s>
<s>
Obnovit	obnovit	k5eAaPmF	obnovit
otroctví	otroctví	k1gNnSc4	otroctví
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
už	už	k6eAd1	už
ale	ale	k8xC	ale
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
republiky	republika	k1gFnSc2	republika
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Otroctví	otroctví	k1gNnSc3	otroctví
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
===	===	k?	===
</s>
</p>
<p>
<s>
Megasthenés	Megasthenés	k6eAd1	Megasthenés
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Indika	Indikum	k1gNnSc2	Indikum
otroky	otrok	k1gMnPc4	otrok
mezi	mezi	k7c7	mezi
sedmi	sedm	k4xCc7	sedm
popisovanými	popisovaný	k2eAgFnPc7d1	popisovaná
skupinami	skupina	k1gFnPc7	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
první	první	k4xOgInSc4	první
známý	známý	k2eAgInSc4d1	známý
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vladař	vladař	k1gMnSc1	vladař
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
postavení	postavení	k1gNnSc4	postavení
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Maurjovské	Maurjovský	k2eAgFnSc2d1	Maurjovský
říše	říš	k1gFnSc2	říš
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ašóky	Ašóka	k1gFnSc2	Ašóka
(	(	kIx(	(
<g/>
304	[number]	k4	304
<g/>
–	–	k?	–
<g/>
232	[number]	k4	232
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Otroctví	otroctví	k1gNnSc3	otroctví
v	v	k7c6	v
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
===	===	k?	===
</s>
</p>
<p>
<s>
Krymští	krymský	k2eAgMnPc1d1	krymský
Tataři	Tatar	k1gMnPc1	Tatar
a	a	k8xC	a
Nogajci	Nogajce	k1gMnPc1	Nogajce
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
vládli	vládnout	k5eAaImAgMnP	vládnout
krymští	krymský	k2eAgMnPc1d1	krymský
chánové	chán	k1gMnPc1	chán
<g/>
,	,	kIx,	,
potomci	potomek	k1gMnPc1	potomek
Čingischána	Čingischán	k1gMnSc2	Čingischán
a	a	k8xC	a
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
vazalové	vazal	k1gMnPc1	vazal
osmanského	osmanský	k2eAgMnSc2d1	osmanský
sultána	sultán	k1gMnSc2	sultán
<g/>
,	,	kIx,	,
podnikali	podnikat	k5eAaImAgMnP	podnikat
téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
loupeživé	loupeživý	k2eAgInPc4d1	loupeživý
vpády	vpád	k1gInPc4	vpád
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
byla	být	k5eAaImAgFnS	být
vylidněna	vylidnit	k5eAaPmNgFnS	vylidnit
prakticky	prakticky	k6eAd1	prakticky
celá	celý	k2eAgFnSc1d1	celá
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
nájezdech	nájezd	k1gInPc6	nájezd
obávané	obávaný	k2eAgFnSc2d1	obávaná
krymskotatarské	krymskotatarský	k2eAgFnSc2d1	krymskotatarský
jízdy	jízda	k1gFnSc2	jízda
tzv.	tzv.	kA	tzv.
sklízení	sklízení	k1gNnSc1	sklízení
stepí	step	k1gFnSc7	step
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
odvlečeno	odvlečen	k2eAgNnSc1d1	odvlečen
a	a	k8xC	a
prodáno	prodán	k2eAgNnSc1d1	prodáno
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
v	v	k7c6	v
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
až	až	k9	až
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
především	především	k9	především
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
Bělorusů	Bělorus	k1gMnPc2	Bělorus
a	a	k8xC	a
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1571	[number]	k4	1571
byla	být	k5eAaImAgFnS	být
krymskými	krymský	k2eAgMnPc7d1	krymský
Tatary	Tatar	k1gMnPc7	Tatar
vypálena	vypálen	k2eAgFnSc1d1	vypálena
dokonce	dokonce	k9	dokonce
i	i	k8xC	i
Moskva	Moskva	k1gFnSc1	Moskva
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
odvlečeno	odvlečen	k2eAgNnSc4d1	odvlečen
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1663	[number]	k4	1663
podnikli	podniknout	k5eAaPmAgMnP	podniknout
krymští	krymský	k2eAgMnPc1d1	krymský
Tataři	Tatar	k1gMnPc1	Tatar
společně	společně	k6eAd1	společně
s	s	k7c7	s
Turky	Turek	k1gMnPc7	Turek
sérii	série	k1gFnSc4	série
krutých	krutý	k2eAgInPc2d1	krutý
vpádů	vpád	k1gInPc2	vpád
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
bylo	být	k5eAaImAgNnS	být
uneseno	unést	k5eAaPmNgNnS	unést
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
na	na	k7c4	na
12	[number]	k4	12
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
především	především	k9	především
mladých	mladý	k2eAgFnPc2d1	mladá
dívek	dívka	k1gFnPc2	dívka
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
..	..	k?	..
"	"	kIx"	"
<g/>
Čoskoro	Čoskora	k1gFnSc5	Čoskora
sa	sa	k?	sa
začalo	začít	k5eAaPmAgNnS	začít
ohavné	ohavný	k2eAgFnPc4d1	ohavná
neslýchané	slýchaný	k2eNgFnPc4d1	neslýchaná
prznenie	prznenie	k1gFnPc4	prznenie
a	a	k8xC	a
znásilňovanie	znásilňovanie	k1gFnPc4	znásilňovanie
žien	žieno	k1gNnPc2	žieno
a	a	k8xC	a
panien	panina	k1gFnPc2	panina
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
píše	psát	k5eAaImIp3nS	psát
očitý	očitý	k2eAgMnSc1d1	očitý
svědek	svědek	k1gMnSc1	svědek
evangelický	evangelický	k2eAgMnSc1d1	evangelický
farář	farář	k1gMnSc1	farář
Štefan	Štefan	k1gMnSc1	Štefan
Pilárik	Pilárik	k1gMnSc1	Pilárik
o	o	k7c6	o
ženách	žena	k1gFnPc6	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Tataři	Tatar	k1gMnPc1	Tatar
odvlekli	odvléct	k5eAaPmAgMnP	odvléct
<g/>
.	.	kIx.	.
</s>
<s>
Otrokářské	otrokářský	k2eAgInPc4d1	otrokářský
nájezdy	nájezd	k1gInPc4	nájezd
Tatarů	Tatar	k1gMnPc2	Tatar
ukončila	ukončit	k5eAaPmAgFnS	ukončit
až	až	k9	až
ruská	ruský	k2eAgFnSc1d1	ruská
carevna	carevna	k1gFnSc1	carevna
Kateřina	Kateřina	k1gFnSc1	Kateřina
Veliká	veliký	k2eAgFnSc1d1	veliká
ovládnutím	ovládnutí	k1gNnSc7	ovládnutí
Krymského	krymský	k2eAgInSc2d1	krymský
chanátu	chanát	k1gInSc2	chanát
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
nájezdech	nájezd	k1gInPc6	nájezd
berberských	berberský	k2eAgInPc2d1	berberský
(	(	kIx(	(
<g/>
barbarských	barbarský	k2eAgInPc2d1	barbarský
<g/>
)	)	kIx)	)
pirátů	pirát	k1gMnPc2	pirát
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
bylo	být	k5eAaImAgNnS	být
odvlečeno	odvléct	k5eAaPmNgNnS	odvléct
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
přes	přes	k7c4	přes
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
severoafrických	severoafrický	k2eAgMnPc2d1	severoafrický
pirátů	pirát	k1gMnPc2	pirát
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spojili	spojit	k5eAaPmAgMnP	spojit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
s	s	k7c7	s
Osmanskými	osmanský	k2eAgInPc7d1	osmanský
Turky	turek	k1gInPc7	turek
a	a	k8xC	a
řádili	řádit	k5eAaImAgMnP	řádit
až	až	k9	až
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
severoafrických	severoafrický	k2eAgMnPc2d1	severoafrický
pirátů	pirát	k1gMnPc2	pirát
se	se	k3xPyFc4	se
počátkem	počátkem	k7c2	počátkem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
postavil	postavit	k5eAaPmAgMnS	postavit
obávaný	obávaný	k2eAgMnSc1d1	obávaný
turecký	turecký	k2eAgMnSc1d1	turecký
pirát	pirát	k1gMnSc1	pirát
Chajruddín	Chajruddín	k1gMnSc1	Chajruddín
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1571	[number]	k4	1571
byla	být	k5eAaImAgFnS	být
spojená	spojený	k2eAgFnSc1d1	spojená
turecko-alžírská	tureckolžírský	k2eAgFnSc1d1	turecko-alžírský
flotila	flotila	k1gFnSc1	flotila
poražena	porazit	k5eAaPmNgFnS	porazit
Svatou	svatý	k2eAgFnSc7d1	svatá
Ligou	liga	k1gFnSc7	liga
v	v	k7c6	v
krvavé	krvavý	k2eAgFnSc6d1	krvavá
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lepanta	Lepant	k1gMnSc2	Lepant
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
získali	získat	k5eAaPmAgMnP	získat
piráti	pirát	k1gMnPc1	pirát
svojí	svojit	k5eAaImIp3nP	svojit
převahu	převaha	k1gFnSc4	převaha
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
středomořských	středomořský	k2eAgInPc2d1	středomořský
ostrovů	ostrov	k1gInPc2	ostrov
bylo	být	k5eAaImAgNnS	být
prakticky	prakticky	k6eAd1	prakticky
vylidněno	vylidnit	k5eAaPmNgNnS	vylidnit
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
odvlečeni	odvléct	k5eAaPmNgMnP	odvléct
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
nebo	nebo	k8xC	nebo
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
byl	být	k5eAaImAgMnS	být
unesen	unést	k5eAaPmNgMnS	unést
i	i	k8xC	i
mladý	mladý	k2eAgMnSc1d1	mladý
Miguel	Miguel	k1gMnSc1	Miguel
de	de	k?	de
Cervantes	Cervantes	k1gMnSc1	Cervantes
a	a	k8xC	a
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgMnS	žít
jako	jako	k9	jako
otrok	otrok	k1gMnSc1	otrok
v	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
vykoupen	vykoupen	k2eAgMnSc1d1	vykoupen
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgInSc1d1	definitivní
konec	konec	k1gInSc1	konec
pirátství	pirátství	k1gNnPc2	pirátství
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Alžíru	Alžír	k1gInSc2	Alžír
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sovětský	sovětský	k2eAgInSc1d1	sovětský
Gulag	gulag	k1gInSc1	gulag
===	===	k?	===
</s>
</p>
<p>
<s>
Gulag	gulag	k1gInSc1	gulag
byl	být	k5eAaImAgInS	být
sítí	síť	k1gFnSc7	síť
sovětských	sovětský	k2eAgInPc2d1	sovětský
pracovních	pracovní	k2eAgInPc2d1	pracovní
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgMnPc2	který
byli	být	k5eAaImAgMnP	být
umísťováni	umísťován	k2eAgMnPc1d1	umísťován
politicky	politicky	k6eAd1	politicky
nespolehliví	spolehlivý	k2eNgMnPc1d1	nespolehlivý
jedinci	jedinec	k1gMnPc1	jedinec
a	a	k8xC	a
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Vězni	vězeň	k1gMnPc1	vězeň
pracovali	pracovat	k5eAaImAgMnP	pracovat
v	v	k7c6	v
nuzných	nuzný	k2eAgFnPc6d1	nuzná
podmínkách	podmínka	k1gFnPc6	podmínka
často	často	k6eAd1	často
nedostatečně	dostatečně	k6eNd1	dostatečně
vybavení	vybavení	k1gNnSc1	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
práci	práce	k1gFnSc4	práce
dostávali	dostávat	k5eAaImAgMnP	dostávat
pouze	pouze	k6eAd1	pouze
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
Gulagu	gulag	k1gInSc2	gulag
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
-	-	kIx~	-
Solženicin	Solženicin	k2eAgInSc1d1	Solženicin
uvádí	uvádět	k5eAaImIp3nS	uvádět
počty	počet	k1gInPc4	počet
násobně	násobně	k6eAd1	násobně
větší	veliký	k2eAgInPc4d2	veliký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sexuální	sexuální	k2eAgNnPc1d1	sexuální
otroctví	otroctví	k1gNnPc1	otroctví
==	==	k?	==
</s>
</p>
<p>
<s>
Sexuální	sexuální	k2eAgNnSc1d1	sexuální
otroctví	otroctví	k1gNnSc1	otroctví
je	být	k5eAaImIp3nS	být
datováno	datovat	k5eAaImNgNnS	datovat
už	už	k6eAd1	už
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
souviselo	souviset	k5eAaImAgNnS	souviset
s	s	k7c7	s
mnohoženstvím	mnohoženství	k1gNnSc7	mnohoženství
nebo	nebo	k8xC	nebo
právem	právem	k6eAd1	právem
vlastnit	vlastnit	k5eAaImF	vlastnit
otroky	otrok	k1gMnPc4	otrok
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
bylo	být	k5eAaImAgNnS	být
dědičné	dědičný	k2eAgNnSc1d1	dědičné
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
např.	např.	kA	např.
zneužívali	zneužívat	k5eAaImAgMnP	zneužívat
japonští	japonský	k2eAgMnPc1d1	japonský
vojáci	voják	k1gMnPc1	voják
zadržené	zadržený	k2eAgFnSc2d1	zadržená
jihokorejské	jihokorejský	k2eAgFnSc2d1	jihokorejská
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
využívali	využívat	k5eAaPmAgMnP	využívat
jako	jako	k9	jako
svoje	svůj	k3xOyFgFnPc4	svůj
sexuální	sexuální	k2eAgFnPc4d1	sexuální
otrokyně	otrokyně	k1gFnPc4	otrokyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Naděje	naděje	k1gFnPc1	naděje
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
===	===	k?	===
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
otroctví	otroctví	k1gNnSc1	otroctví
a	a	k8xC	a
zneužívání	zneužívání	k1gNnSc1	zneužívání
žen	žena	k1gFnPc2	žena
oficiálně	oficiálně	k6eAd1	oficiálně
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
misie	misie	k1gFnSc2	misie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
uprchlým	uprchlý	k2eAgFnPc3d1	uprchlá
obětem	oběť	k1gFnPc3	oběť
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
neziskové	ziskový	k2eNgFnPc1d1	nezisková
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
organizace	organizace	k1gFnSc1	organizace
Česko	Česko	k1gNnSc1	Česko
proti	proti	k7c3	proti
chudobě	chudoba	k1gFnSc3	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
činnost	činnost	k1gFnSc4	činnost
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
především	především	k9	především
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dodnes	dodnes	k6eAd1	dodnes
působí	působit	k5eAaImIp3nP	působit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gFnPc4	on
kdysi	kdysi	k6eAd1	kdysi
přivedli	přivést	k5eAaPmAgMnP	přivést
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
aby	aby	k9	aby
místní	místní	k2eAgMnPc4d1	místní
obyvatele	obyvatel	k1gMnPc4	obyvatel
převedli	převést	k5eAaPmAgMnP	převést
na	na	k7c4	na
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
arabských	arabský	k2eAgFnPc6d1	arabská
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
vyvíjen	vyvíjen	k2eAgInSc1d1	vyvíjen
tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
poskytnutí	poskytnutí	k1gNnSc6	poskytnutí
větších	veliký	k2eAgNnPc2d2	veliký
práv	právo	k1gNnPc2	právo
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nemohou	moct	k5eNaImIp3nP	moct
vzdělávat	vzdělávat	k5eAaImF	vzdělávat
a	a	k8xC	a
manžel	manžel	k1gMnSc1	manžel
je	on	k3xPp3gNnPc4	on
může	moct	k5eAaImIp3nS	moct
prodat	prodat	k5eAaPmF	prodat
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Problémem	problém	k1gInSc7	problém
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
otroctví	otroctví	k1gNnSc2	otroctví
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
chudoby	chudoba	k1gFnSc2	chudoba
a	a	k8xC	a
klesání	klesání	k1gNnSc2	klesání
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
organizace	organizace	k1gFnSc1	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
řeší	řešit	k5eAaImIp3nS	řešit
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
hromadnými	hromadný	k2eAgInPc7d1	hromadný
odchody	odchod	k1gInPc7	odchod
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
věří	věřit	k5eAaImIp3nP	věřit
dealerům	dealer	k1gMnPc3	dealer
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
obětí	oběť	k1gFnSc7	oběť
obchodníků	obchodník	k1gMnPc2	obchodník
s	s	k7c7	s
lidskými	lidský	k2eAgInPc7d1	lidský
orgány	orgán	k1gInPc7	orgán
a	a	k8xC	a
majitelů	majitel	k1gMnPc2	majitel
nevěstinců	nevěstinec	k1gInPc2	nevěstinec
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
mnoho	mnoho	k6eAd1	mnoho
se	se	k3xPyFc4	se
zlepšilo	zlepšit	k5eAaPmAgNnS	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vrcholu	vrchol	k1gInSc6	vrchol
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
způsobeném	způsobený	k2eAgInSc6d1	způsobený
pozvolným	pozvolný	k2eAgNnSc7d1	pozvolné
stoupáním	stoupání	k1gNnSc7	stoupání
počtu	počet	k1gInSc2	počet
otroků	otrok	k1gMnPc2	otrok
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
počet	počet	k1gInSc1	počet
otroků	otrok	k1gMnPc2	otrok
na	na	k7c6	na
světě	svět	k1gInSc6	svět
celkově	celkově	k6eAd1	celkově
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
zajatců	zajatec	k1gMnPc2	zajatec
v	v	k7c6	v
občanských	občanský	k2eAgFnPc6d1	občanská
válkách	válka	k1gFnPc6	válka
také	také	k9	také
ubývá	ubývat	k5eAaImIp3nS	ubývat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ženy	žena	k1gFnPc1	žena
získaly	získat	k5eAaPmAgFnP	získat
více	hodně	k6eAd2	hodně
příležitostí	příležitost	k1gFnPc2	příležitost
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
normální	normální	k2eAgInSc4d1	normální
život	život	k1gInSc4	život
a	a	k8xC	a
minimálně	minimálně	k6eAd1	minimálně
základní	základní	k2eAgNnSc4d1	základní
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zákaz	zákaz	k1gInSc1	zákaz
otroctví	otroctví	k1gNnPc2	otroctví
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
==	==	k?	==
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
odmítavou	odmítavý	k2eAgFnSc4d1	odmítavá
a	a	k8xC	a
závaznou	závazný	k2eAgFnSc4d1	závazná
oficiální	oficiální	k2eAgFnSc4d1	oficiální
reakci	reakce	k1gFnSc4	reakce
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
na	na	k7c6	na
otrokářství	otrokářství	k1gNnSc6	otrokářství
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
encyklika	encyklika	k1gFnSc1	encyklika
Sicut	Sicut	k1gInSc1	Sicut
Dudum	Dudum	k?	Dudum
papeže	papež	k1gMnSc2	papež
Evžena	Evžen	k1gMnSc2	Evžen
IV	IV	kA	IV
<g/>
.	.	kIx.	.
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1434	[number]	k4	1434
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
zotročování	zotročování	k1gNnSc4	zotročování
domorodců	domorodec	k1gMnPc2	domorodec
na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
španělskými	španělský	k2eAgMnPc7d1	španělský
obchodníky	obchodník	k1gMnPc7	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
nařídil	nařídit	k5eAaPmAgMnS	nařídit
všechny	všechen	k3xTgMnPc4	všechen
otroky	otrok	k1gMnPc4	otrok
propustit	propustit	k5eAaPmF	propustit
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
do	do	k7c2	do
15	[number]	k4	15
dnů	den	k1gInPc2	den
od	od	k7c2	od
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
encykliky	encyklika	k1gFnSc2	encyklika
a	a	k8xC	a
neuposlechnutí	neuposlechnutí	k1gNnSc1	neuposlechnutí
jeho	jeho	k3xOp3gInSc2	jeho
příkazu	příkaz	k1gInSc2	příkaz
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
další	další	k2eAgNnPc4d1	další
pokračování	pokračování	k1gNnPc4	pokračování
v	v	k7c6	v
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
trestal	trestat	k5eAaImAgMnS	trestat
tehdy	tehdy	k6eAd1	tehdy
nejtěžším	těžký	k2eAgInSc7d3	nejtěžší
možným	možný	k2eAgInSc7d1	možný
trestem	trest	k1gInSc7	trest
pro	pro	k7c4	pro
křesťana	křesťan	k1gMnSc4	křesťan
<g/>
,	,	kIx,	,
automatickou	automatický	k2eAgFnSc7d1	automatická
exkomunikací	exkomunikace	k1gFnSc7	exkomunikace
<g/>
.	.	kIx.	.
</s>
<s>
Španělští	španělský	k2eAgMnPc1d1	španělský
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
považovali	považovat	k5eAaImAgMnP	považovat
domorodce	domorodec	k1gMnSc2	domorodec
za	za	k7c4	za
druh	druh	k1gInSc4	druh
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
za	za	k7c4	za
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nařízení	nařízení	k1gNnSc4	nařízení
neuposlechli	uposlechnout	k5eNaPmAgMnP	uposlechnout
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
ho	on	k3xPp3gMnSc4	on
ignorovali	ignorovat	k5eAaImAgMnP	ignorovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1462	[number]	k4	1462
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
II	II	kA	II
<g/>
.	.	kIx.	.
označil	označit	k5eAaPmAgInS	označit
otrokářství	otrokářství	k1gNnSc4	otrokářství
jako	jako	k8xC	jako
těžký	těžký	k2eAgInSc4d1	těžký
hřích	hřích	k1gInSc4	hřích
(	(	kIx(	(
<g/>
magnum	magnum	k1gInSc1	magnum
scelus	scelus	k1gInSc1	scelus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1537	[number]	k4	1537
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
III	III	kA	III
<g/>
.	.	kIx.	.
opětovně	opětovně	k6eAd1	opětovně
zakázal	zakázat	k5eAaPmAgMnS	zakázat
zotročování	zotročování	k1gNnSc4	zotročování
Indiánů	Indián	k1gMnPc2	Indián
a	a	k8xC	a
ostatních	ostatní	k2eAgMnPc2d1	ostatní
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
papežské	papežský	k2eAgFnSc6d1	Papežská
bule	bula	k1gFnSc6	bula
Sublimus	Sublimus	k1gMnSc1	Sublimus
Dei	Dei	k1gMnSc1	Dei
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
i	i	k9	i
Papež	Papež	k1gMnSc1	Papež
Urban	Urban	k1gMnSc1	Urban
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1639	[number]	k4	1639
při	při	k7c6	při
schvalování	schvalování	k1gNnSc6	schvalování
Jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
mise	mise	k1gFnSc2	mise
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1741	[number]	k4	1741
v	v	k7c6	v
bule	bula	k1gFnSc6	bula
Immensa	Immens	k1gMnSc2	Immens
Pastorum	Pastorum	k?	Pastorum
principis	principis	k1gFnSc2	principis
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
otrokářství	otrokářství	k1gNnSc3	otrokářství
jako	jako	k8xS	jako
takové	takový	k3xDgNnSc1	takový
a	a	k8xC	a
která	který	k3yIgFnSc1	který
výslovně	výslovně	k6eAd1	výslovně
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
otrokářství	otrokářství	k1gNnSc4	otrokářství
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Afričtí	africký	k2eAgMnPc1d1	africký
otroci	otrok	k1gMnPc1	otrok
v	v	k7c6	v
USA	USA	kA	USA
==	==	k?	==
</s>
</p>
<p>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
černých	černý	k2eAgMnPc2d1	černý
otroků	otrok	k1gMnPc2	otrok
provází	provázet	k5eAaImIp3nS	provázet
kolonizaci	kolonizace	k1gFnSc4	kolonizace
Amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1619	[number]	k4	1619
přistála	přistát	k5eAaPmAgFnS	přistát
první	první	k4xOgFnSc1	první
loď	loď	k1gFnSc1	loď
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
v	v	k7c6	v
Jamestownu	Jamestown	k1gInSc6	Jamestown
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počáteční	počáteční	k2eAgInSc1d1	počáteční
status	status	k1gInSc1	status
je	být	k5eAaImIp3nS	být
však	však	k9	však
podobný	podobný	k2eAgInSc1d1	podobný
ostatním	ostatní	k2eAgMnPc3d1	ostatní
kolonizátorům	kolonizátor	k1gMnPc3	kolonizátor
-	-	kIx~	-
jejich	jejich	k3xOp3gInSc4	jejich
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
majiteli	majitel	k1gMnSc3	majitel
je	být	k5eAaImIp3nS	být
zajištěn	zajištěn	k2eAgInSc1d1	zajištěn
smluvně	smluvně	k6eAd1	smluvně
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
zvykem	zvyk	k1gInSc7	zvyk
pokřtěné	pokřtěný	k2eAgFnSc2d1	pokřtěná
otroky	otrok	k1gMnPc4	otrok
propouštět	propouštět	k5eAaImF	propouštět
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
význam	význam	k1gInSc1	význam
otroctví	otroctví	k1gNnPc2	otroctví
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
narůstal	narůstat	k5eAaImAgInS	narůstat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
jejich	jejich	k3xOp3gFnSc1	jejich
situace	situace	k1gFnSc1	situace
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
a	a	k8xC	a
nové	nový	k2eAgMnPc4d1	nový
zákoníky	zákoník	k1gMnPc4	zákoník
"	"	kIx"	"
<g/>
Slave	Slaev	k1gFnSc2	Slaev
codes	codes	k1gInSc1	codes
<g/>
"	"	kIx"	"
jim	on	k3xPp3gMnPc3	on
vymezovaly	vymezovat	k5eAaImAgFnP	vymezovat
postavení	postavení	k1gNnSc3	postavení
téměř	téměř	k6eAd1	téměř
bezprávného	bezprávný	k2eAgInSc2d1	bezprávný
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
USA	USA	kA	USA
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
prezidentem	prezident	k1gMnSc7	prezident
A.	A.	kA	A.
Lincolnem	Lincoln	k1gMnSc7	Lincoln
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
-	-	kIx~	-
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
území	území	k1gNnSc6	území
Unií	unie	k1gFnPc2	unie
neovládaných	ovládaný	k2eNgInPc2d1	neovládaný
"	"	kIx"	"
<g/>
povstaleckých	povstalecký	k2eAgInPc2d1	povstalecký
států	stát	k1gInPc2	stát
<g/>
"	"	kIx"	"
a	a	k8xC	a
z	z	k7c2	z
propagačních	propagační	k2eAgInPc2d1	propagační
důvodů	důvod	k1gInPc2	důvod
(	(	kIx(	(
<g/>
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
zabránění	zabránění	k1gNnSc4	zabránění
uznání	uznání	k1gNnSc2	uznání
Konfederace	konfederace	k1gFnSc2	konfederace
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
v	v	k7c6	v
celých	celá	k1gFnPc6	celá
USA	USA	kA	USA
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
až	až	k6eAd1	až
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
formálního	formální	k2eAgNnSc2d1	formální
hlediska	hledisko	k1gNnSc2	hledisko
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
na	na	k7c4	na
území	území	k1gNnSc4	území
USA	USA	kA	USA
až	až	k6eAd1	až
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jako	jako	k9	jako
poslední	poslední	k2eAgInSc4d1	poslední
americký	americký	k2eAgInSc4d1	americký
stát	stát	k1gInSc4	stát
dokončilo	dokončit	k5eAaPmAgNnS	dokončit
ratifikaci	ratifikace	k1gFnSc4	ratifikace
třináctého	třináctý	k4xOgInSc2	třináctý
dodatku	dodatek	k1gInSc2	dodatek
ústavy	ústava	k1gFnSc2	ústava
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chronologický	chronologický	k2eAgInSc4d1	chronologický
přehled	přehled	k1gInSc4	přehled
zrušení	zrušení	k1gNnSc4	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
achaimenovské	achaimenovský	k2eAgFnSc6d1	achaimenovský
Perské	perský	k2eAgFnSc6d1	perská
říši	říš	k1gFnSc6	říš
bylo	být	k5eAaImAgNnS	být
otroctví	otroctví	k1gNnSc1	otroctví
zrušeno	zrušen	k2eAgNnSc1d1	zrušeno
s	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
věrouky	věrouka	k1gFnSc2	věrouka
zoroastrismu	zoroastrismus	k1gInSc2	zoroastrismus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
institut	institut	k1gInSc1	institut
otroctví	otroctví	k1gNnSc2	otroctví
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
<g/>
,	,	kIx,	,
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
vrstvou	vrstva	k1gFnSc7	vrstva
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
například	například	k6eAd1	například
k	k	k7c3	k
propuštění	propuštění	k1gNnSc3	propuštění
Židů	Žid	k1gMnPc2	Žid
z	z	k7c2	z
babylonského	babylonský	k2eAgNnSc2d1	babylonské
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
bylo	být	k5eAaImAgNnS	být
otroctví	otroctví	k1gNnSc1	otroctví
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
:	:	kIx,	:
1335	[number]	k4	1335
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
až	až	k9	až
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
St.	st.	kA	st.
Barthélemy	Barthélem	k1gInPc4	Barthélem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
:	:	kIx,	:
1761	[number]	k4	1761
</s>
</p>
<p>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
a	a	k8xC	a
Wales	Wales	k1gInSc1	Wales
<g/>
:	:	kIx,	:
1772	[number]	k4	1772
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
Somersettova	Somersettův	k2eAgInSc2d1	Somersettův
případu	případ	k1gInSc2	případ
</s>
</p>
<p>
<s>
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
:	:	kIx,	:
1776	[number]	k4	1776
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
Wedderburneského	Wedderburneský	k2eAgInSc2d1	Wedderburneský
případu	případ	k1gInSc2	případ
</s>
</p>
<p>
<s>
Haiti	Haiti	k1gNnSc1	Haiti
<g/>
:	:	kIx,	:
1791	[number]	k4	1791
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
povstání	povstání	k1gNnSc2	povstání
asi	asi	k9	asi
půl	půl	k1xP	půl
miliónu	milión	k4xCgInSc2	milión
otroků	otrok	k1gMnPc2	otrok
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1794	[number]	k4	1794
<g/>
–	–	k?	–
<g/>
1802	[number]	k4	1802
<g/>
,	,	kIx,	,
týkalo	týkat	k5eAaImAgNnS	týkat
se	se	k3xPyFc4	se
všech	všecek	k3xTgFnPc2	všecek
kolonií	kolonie	k1gFnPc2	kolonie
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
:	:	kIx,	:
1813	[number]	k4	1813
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
(	(	kIx(	(
<g/>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
Panama	Panama	k1gFnSc1	Panama
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1821	[number]	k4	1821
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
postupného	postupný	k2eAgInSc2d1	postupný
plánu	plán	k1gInSc2	plán
osvobození	osvobození	k1gNnSc2	osvobození
(	(	kIx(	(
<g/>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
v	v	k7c6	v
r.	r.	kA	r.
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
v	v	k7c6	v
r.	r.	kA	r.
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chile	Chile	k1gNnSc1	Chile
<g/>
:	:	kIx,	:
1823	[number]	k4	1823
</s>
</p>
<p>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
:	:	kIx,	:
1829	[number]	k4	1829
</s>
</p>
<p>
<s>
Britské	britský	k2eAgNnSc1d1	Britské
impérium	impérium	k1gNnSc1	impérium
<g/>
:	:	kIx,	:
1833	[number]	k4	1833
<g/>
,	,	kIx,	,
týkalo	týkat	k5eAaImAgNnS	týkat
se	se	k3xPyFc4	se
všech	všecek	k3xTgFnPc2	všecek
kolonií	kolonie	k1gFnPc2	kolonie
(	(	kIx(	(
<g/>
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1834	[number]	k4	1834
<g/>
;	;	kIx,	;
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Indii	Indie	k1gFnSc6	Indie
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mauricius	Mauricius	k1gInSc1	Mauricius
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
britskou	britský	k2eAgFnSc7d1	britská
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
státním	státní	k2eAgInSc7d1	státní
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
:	:	kIx,	:
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
týkalo	týkat	k5eAaImAgNnS	týkat
se	se	k3xPyFc4	se
všech	všecek	k3xTgFnPc2	všecek
kolonií	kolonie	k1gFnPc2	kolonie
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
podruhé	podruhé	k6eAd1	podruhé
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
týkalo	týkat	k5eAaImAgNnS	týkat
se	se	k3xPyFc4	se
všech	všecek	k3xTgFnPc2	všecek
kolonií	kolonie	k1gFnPc2	kolonie
</s>
</p>
<p>
<s>
Peru	Peru	k1gNnSc1	Peru
<g/>
:	:	kIx,	:
1851	[number]	k4	1851
</s>
</p>
<p>
<s>
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
:	:	kIx,	:
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
týkalo	týkat	k5eAaImAgNnS	týkat
se	se	k3xPyFc4	se
všech	všecek	k3xTgFnPc2	všecek
kolonií	kolonie	k1gFnPc2	kolonie
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
<g/>
:	:	kIx,	:
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
již	již	k9	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Puerto	Puerta	k1gFnSc5	Puerta
Rico	Rica	k1gFnSc5	Rica
1873	[number]	k4	1873
a	a	k8xC	a
Kuba	Kuba	k1gMnSc1	Kuba
<g/>
:	:	kIx,	:
1880	[number]	k4	1880
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
kolonie	kolonie	k1gFnPc1	kolonie
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
:	:	kIx,	:
1888	[number]	k4	1888
</s>
</p>
<p>
<s>
Korea	Korea	k1gFnSc1	Korea
<g/>
:	:	kIx,	:
1894	[number]	k4	1894
(	(	kIx(	(
<g/>
dědičné	dědičný	k2eAgNnSc1d1	dědičné
otroctví	otroctví	k1gNnSc1	otroctví
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
<g/>
:	:	kIx,	:
1897	[number]	k4	1897
(	(	kIx(	(
<g/>
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
zakázán	zakázat	k5eAaPmNgInS	zakázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čína	Čína	k1gFnSc1	Čína
<g/>
:	:	kIx,	:
1910	[number]	k4	1910
</s>
</p>
<p>
<s>
Myanmar	Myanmar	k1gInSc1	Myanmar
<g/>
:	:	kIx,	:
1929	[number]	k4	1929
</s>
</p>
<p>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
:	:	kIx,	:
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
nařízením	nařízení	k1gNnSc7	nařízení
italských	italský	k2eAgFnPc2d1	italská
okupačních	okupační	k2eAgFnPc2d1	okupační
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
znovuzískání	znovuzískání	k1gNnSc6	znovuzískání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
císař	císař	k1gMnSc1	císař
Haile	Hail	k1gMnSc2	Hail
Selassie	Selassie	k1gFnSc2	Selassie
otroctví	otroctví	k1gNnPc2	otroctví
neobnovil	obnovit	k5eNaPmAgInS	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tibet	Tibet	k1gInSc1	Tibet
<g/>
:	:	kIx,	:
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
nařízením	nařízení	k1gNnSc7	nařízení
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
:	:	kIx,	:
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
<g/>
:	:	kIx,	:
červenec	červenec	k1gInSc1	červenec
1980	[number]	k4	1980
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
formálně	formálně	k6eAd1	formálně
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
francouzskými	francouzský	k2eAgInPc7d1	francouzský
orgány	orgán	k1gInPc7	orgán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
mlčky	mlčky	k6eAd1	mlčky
(	(	kIx(	(
<g/>
nepřímo	přímo	k6eNd1	přímo
<g/>
)	)	kIx)	)
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
ústavě	ústava	k1gFnSc6	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
a	a	k8xC	a
výslovně	výslovně	k6eAd1	výslovně
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
země	země	k1gFnSc1	země
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otrokářství	otrokářství	k1gNnSc1	otrokářství
nebylo	být	k5eNaImAgNnS	být
trestné	trestný	k2eAgNnSc1d1	trestné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kvalifikuje	kvalifikovat	k5eAaBmIp3nS	kvalifikovat
otrokářství	otrokářství	k1gNnSc4	otrokářství
jako	jako	k8xC	jako
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
a	a	k8xC	a
hrozí	hrozit	k5eAaImIp3nS	hrozit
za	za	k7c2	za
něj	on	k3xPp3gNnSc2	on
až	až	k9	až
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
otrokářství	otrokářství	k1gNnSc1	otrokářství
v	v	k7c6	v
Mauritánii	Mauritánie	k1gFnSc6	Mauritánie
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GRAUS	GRAUS	kA	GRAUS
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
venkovského	venkovský	k2eAgInSc2d1	venkovský
lidu	lid	k1gInSc2	lid
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HANZLÍČKOVÁ	HANZLÍČKOVÁ	kA	HANZLÍČKOVÁ
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgNnPc1d1	kulturní
studia	studio	k1gNnPc1	studio
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
</s>
<s>
V.	V.	kA	V.
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
s.	s.	k?	s.
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
2336	[number]	k4	2336
<g/>
-	-	kIx~	-
<g/>
2766	[number]	k4	2766
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.716	[number]	k4	10.716
<g/>
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
ks	ks	kA	ks
<g/>
.2017	.2017	k4	.2017
<g/>
.080101	.080101	k4	.080101
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Veliké	veliký	k2eAgNnSc1d1	veliké
město	město	k1gNnSc1	město
Slovanů	Slovan	k1gInPc2	Slovan
jménem	jméno	k1gNnSc7	jméno
Praha	Praha	k1gFnSc1	Praha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
a	a	k8xC	a
otroci	otrok	k1gMnPc1	otrok
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Přemyslovský	přemyslovský	k2eAgInSc1d1	přemyslovský
stát	stát	k1gInSc1	stát
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Luboš	Luboš	k1gMnSc1	Luboš
Polanský	Polanský	k1gMnSc1	Polanský
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Sláma	Sláma	k1gMnSc1	Sláma
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
Třeštík	Třeštík	k1gMnSc1	Třeštík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
s.	s.	k?	s.
49	[number]	k4	49
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
ve	v	k7c6	v
starozákoním	starozákonit	k5eAaImIp1nS	starozákonit
Izraeli	Izrael	k1gInSc6	Izrael
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Energetický	energetický	k2eAgMnSc1d1	energetický
otrok	otrok	k1gMnSc1	otrok
</s>
</p>
<p>
<s>
Námezdní	námezdní	k2eAgNnSc1d1	námezdní
otroctví	otroctví	k1gNnSc1	otroctví
</s>
</p>
<p>
<s>
Dlužní	dlužní	k2eAgNnSc1d1	dlužní
otroctví	otroctví	k1gNnSc1	otroctví
</s>
</p>
<p>
<s>
Neokolonialismus	neokolonialismus	k1gInSc1	neokolonialismus
</s>
</p>
<p>
<s>
Pán	pán	k1gMnSc1	pán
</s>
</p>
<p>
<s>
Poddanství	poddanství	k1gNnSc1	poddanství
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
otrokářství	otrokářství	k1gNnSc2	otrokářství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Otroctví	otroctví	k1gNnSc2	otroctví
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
