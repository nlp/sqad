<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Nizozemci	Nizozemec	k1gMnPc7	Nizozemec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1625	[number]	k4	1625
jako	jako	k8xC	jako
Nový	nový	k2eAgInSc1d1	nový
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Angličanů	Angličan	k1gMnPc2	Angličan
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
roku	rok	k1gInSc2	rok
1664	[number]	k4	1664
<g/>
.	.	kIx.	.
</s>
