<s>
Titan	titan	k1gInSc1	titan
(	(	kIx(	(
<g/>
Saturn	Saturn	k1gInSc1	Saturn
VI	VI	kA	VI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
62	[number]	k4	62
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
objevených	objevený	k2eAgInPc2d1	objevený
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
měsícem	měsíc	k1gInSc7	měsíc
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
silná	silný	k2eAgFnSc1d1	silná
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
a	a	k8xC	a
kromě	kromě	k7c2	kromě
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
objektem	objekt	k1gInSc7	objekt
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
ověřena	ověřen	k2eAgFnSc1d1	ověřena
přítomnost	přítomnost	k1gFnSc1	přítomnost
stálých	stálý	k2eAgFnPc2d1	stálá
kapalných	kapalný	k2eAgFnPc2d1	kapalná
struktur	struktura	k1gFnPc2	struktura
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
větší	veliký	k2eAgFnSc2d2	veliký
a	a	k8xC	a
80	[number]	k4	80
%	%	kIx~	%
hmotnější	hmotný	k2eAgInSc1d2	hmotnější
než	než	k8xS	než
zemský	zemský	k2eAgInSc1d1	zemský
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
po	po	k7c6	po
Ganymedu	Ganymed	k1gMnSc6	Ganymed
druhý	druhý	k4xOgInSc4	druhý
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
planetární	planetární	k2eAgFnSc6d1	planetární
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
nejmenší	malý	k2eAgFnSc1d3	nejmenší
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
však	však	k9	však
jen	jen	k9	jen
40	[number]	k4	40
%	%	kIx~	%
Merkurovy	Merkurův	k2eAgFnSc2d1	Merkurova
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
jej	on	k3xPp3gNnSc4	on
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
astronom	astronom	k1gMnSc1	astronom
Christiaan	Christiaan	k1gMnSc1	Christiaan
Huygens	Huygens	k1gInSc4	Huygens
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1655	[number]	k4	1655
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
objevený	objevený	k2eAgInSc1d1	objevený
měsíc	měsíc	k1gInSc1	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
šestá	šestý	k4xOgFnSc1	šestý
objevená	objevený	k2eAgFnSc1d1	objevená
oběžnice	oběžnice	k1gFnSc1	oběžnice
planety	planeta	k1gFnSc2	planeta
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
čtyřech	čtyři	k4xCgInPc6	čtyři
Galileových	Galileův	k2eAgInPc6d1	Galileův
měsících	měsíc	k1gInPc6	měsíc
obíhajících	obíhající	k2eAgInPc6d1	obíhající
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
od	od	k7c2	od
Saturnu	Saturn	k1gInSc2	Saturn
šestý	šestý	k4xOgInSc4	šestý
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
elipsoidního	elipsoidní	k2eAgInSc2d1	elipsoidní
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Planetu	planeta	k1gFnSc4	planeta
obíhá	obíhat	k5eAaImIp3nS	obíhat
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
20	[number]	k4	20
poloměrů	poloměr	k1gInPc2	poloměr
Saturnu	Saturn	k1gInSc2	Saturn
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1	[number]	k4	1
200	[number]	k4	200
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
povrchu	povrch	k1gInSc2	povrch
by	by	kYmCp3nS	by
mateřská	mateřský	k2eAgFnSc1d1	mateřská
planeta	planeta	k1gFnSc1	planeta
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
11	[number]	k4	11
<g/>
×	×	k?	×
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
Měsíc	měsíc	k1gInSc1	měsíc
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zabírala	zabírat	k5eAaImAgFnS	zabírat
by	by	kYmCp3nS	by
úhel	úhel	k1gInSc4	úhel
5,09	[number]	k4	5,09
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
Titánech	Titán	k1gMnPc6	Titán
<g/>
,	,	kIx,	,
dětech	dítě	k1gFnPc6	dítě
Úrana	Úrano	k1gNnSc2	Úrano
<g/>
,	,	kIx,	,
boha	bůh	k1gMnSc2	bůh
nebes	nebesa	k1gNnPc2	nebesa
<g/>
,	,	kIx,	,
a	a	k8xC	a
Gaie	Gaie	k1gFnSc1	Gaie
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnSc1	bohyně
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kamenného	kamenný	k2eAgInSc2d1	kamenný
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vesmírnými	vesmírný	k2eAgNnPc7d1	vesmírné
lety	léto	k1gNnPc7	léto
se	se	k3xPyFc4	se
o	o	k7c6	o
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
vědělo	vědět	k5eAaImAgNnS	vědět
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
Venuše	Venuše	k1gFnSc2	Venuše
kvůli	kvůli	k7c3	kvůli
husté	hustý	k2eAgFnSc3d1	hustá
neprůhledné	průhledný	k2eNgFnSc3d1	neprůhledná
atmosféře	atmosféra	k1gFnSc3	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
získala	získat	k5eAaPmAgFnS	získat
až	až	k9	až
sonda	sonda	k1gFnSc1	sonda
Cassini-Huygens	Cassini-Huygensa	k1gFnPc2	Cassini-Huygensa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
k	k	k7c3	k
Saturnu	Saturn	k1gInSc2	Saturn
dorazila	dorazit	k5eAaPmAgFnS	dorazit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
studie	studie	k1gFnSc2	studie
jeho	jeho	k3xOp3gFnSc2	jeho
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
satelitů	satelit	k1gMnPc2	satelit
a	a	k8xC	a
která	který	k3yIgFnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Titanových	titanový	k2eAgInPc2d1	titanový
pólů	pól	k1gInPc2	pól
jezera	jezero	k1gNnSc2	jezero
kapalných	kapalný	k2eAgInPc2d1	kapalný
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
mladý	mladý	k2eAgMnSc1d1	mladý
a	a	k8xC	a
hladký	hladký	k2eAgMnSc1d1	hladký
<g/>
,	,	kIx,	,
nalezeno	nalezen	k2eAgNnSc1d1	Nalezeno
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
několik	několik	k4yIc1	několik
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
pohoří	pohoří	k1gNnPc2	pohoří
a	a	k8xC	a
kryovulkánů	kryovulkán	k1gInPc2	kryovulkán
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
atmosféry	atmosféra	k1gFnSc2	atmosféra
Titanu	titan	k1gInSc2	titan
je	být	k5eAaImIp3nS	být
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
minoritními	minoritní	k2eAgFnPc7d1	minoritní
složkami	složka	k1gFnPc7	složka
jsou	být	k5eAaImIp3nP	být
methan	methan	k1gInSc4	methan
a	a	k8xC	a
ethan	ethan	k1gInSc4	ethan
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
oblačnost	oblačnost	k1gFnSc1	oblačnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
dusíkem	dusík	k1gInSc7	dusík
obohacený	obohacený	k2eAgInSc4d1	obohacený
organický	organický	k2eAgInSc4d1	organický
smog	smog	k1gInSc4	smog
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
větrnost	větrnost	k1gFnSc1	větrnost
a	a	k8xC	a
kapalné	kapalný	k2eAgFnPc1d1	kapalná
srážky	srážka	k1gFnPc1	srážka
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
útvary	útvar	k1gInPc4	útvar
podobné	podobný	k2eAgInPc4d1	podobný
pozemským	pozemský	k2eAgInPc3d1	pozemský
–	–	k?	–
duny	duna	k1gFnSc2	duna
<g/>
,	,	kIx,	,
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
z	z	k7c2	z
kapalného	kapalný	k2eAgInSc2d1	kapalný
methanu	methan	k1gInSc2	methan
<g/>
)	)	kIx)	)
a	a	k8xC	a
delty	delta	k1gFnSc2	delta
<g/>
,	,	kIx,	,
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
sezónní	sezónní	k2eAgFnPc4d1	sezónní
změny	změna	k1gFnPc4	změna
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Methan	methan	k1gInSc1	methan
se	se	k3xPyFc4	se
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
i	i	k9	i
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
kapalné	kapalný	k2eAgFnSc6d1	kapalná
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
do	do	k7c2	do
dusíkové	dusíkový	k2eAgFnSc2d1	dusíková
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
opět	opět	k6eAd1	opět
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
srážek	srážka	k1gFnPc2	srážka
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
–	–	k?	–
tento	tento	k3xDgInSc4	tento
methanový	methanový	k2eAgInSc4d1	methanový
cyklus	cyklus	k1gInSc4	cyklus
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
koloběhu	koloběh	k1gInSc3	koloběh
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
však	však	k9	však
za	za	k7c2	za
výrazně	výrazně	k6eAd1	výrazně
nižších	nízký	k2eAgFnPc2d2	nižší
teplot	teplota	k1gFnPc2	teplota
okolo	okolo	k7c2	okolo
94	[number]	k4	94
K	K	kA	K
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
179,2	[number]	k4	179,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
existenci	existence	k1gFnSc3	existence
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
přítomnosti	přítomnost	k1gFnSc2	přítomnost
organického	organický	k2eAgInSc2d1	organický
materiálu	materiál	k1gInSc2	materiál
existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgFnPc1d1	různá
teorie	teorie	k1gFnPc1	teorie
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	se	k3xPyFc4	se
možností	možnost	k1gFnSc7	možnost
existence	existence	k1gFnSc2	existence
života	život	k1gInSc2	život
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
jsou	být	k5eAaImIp3nP	být
obdobné	obdobný	k2eAgFnPc1d1	obdobná
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc1	jaký
panovaly	panovat	k5eAaImAgFnP	panovat
na	na	k7c6	na
pravěké	pravěký	k2eAgFnSc6d1	pravěká
Zemi	zem	k1gFnSc6	zem
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
však	však	k9	však
značné	značný	k2eAgFnPc4d1	značná
překážky	překážka	k1gFnPc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
dopadá	dopadat	k5eAaImIp3nS	dopadat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k1gNnSc1	málo
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
panují	panovat	k5eAaImIp3nP	panovat
zde	zde	k6eAd1	zde
trvale	trvale	k6eAd1	trvale
teploty	teplota	k1gFnSc2	teplota
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
v	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Organismy	organismus	k1gInPc1	organismus
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
by	by	kYmCp3nP	by
však	však	k9	však
mohly	moct	k5eAaImAgFnP	moct
využívat	využívat	k5eAaImF	využívat
místo	místo	k1gNnSc4	místo
vody	voda	k1gFnSc2	voda
kapalné	kapalný	k2eAgInPc1d1	kapalný
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
,	,	kIx,	,
např.	např.	kA	např.
methan	methan	k1gInSc1	methan
nebo	nebo	k8xC	nebo
ethan	ethan	k1gInSc1	ethan
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčivé	přesvědčivý	k2eAgInPc1d1	přesvědčivý
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
existenci	existence	k1gFnSc4	existence
života	život	k1gInSc2	život
zatím	zatím	k6eAd1	zatím
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
byl	být	k5eAaImAgInS	být
zkoumán	zkoumat	k5eAaImNgInS	zkoumat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
několika	několik	k4yIc2	několik
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
misí	mise	k1gFnPc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
snímky	snímek	k1gInPc4	snímek
pořidila	pořidit	k5eAaPmAgFnS	pořidit
při	pře	k1gFnSc4	pře
průletu	průlet	k1gInSc2	průlet
sonda	sonda	k1gFnSc1	sonda
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
výzkumnou	výzkumný	k2eAgFnSc7d1	výzkumná
sondou	sonda	k1gFnSc7	sonda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
vědců	vědec	k1gMnPc2	vědec
cíleně	cíleně	k6eAd1	cíleně
změnil	změnit	k5eAaPmAgMnS	změnit
trajektorii	trajektorie	k1gFnSc4	trajektorie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
kolem	kolem	k7c2	kolem
Titanu	titan	k1gInSc2	titan
provést	provést	k5eAaPmF	provést
těsný	těsný	k2eAgInSc4d1	těsný
průlet	průlet	k1gInSc4	průlet
a	a	k8xC	a
provést	provést	k5eAaPmF	provést
první	první	k4xOgNnSc4	první
měření	měření	k1gNnSc4	měření
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
několik	několik	k4yIc4	několik
snímků	snímek	k1gInPc2	snímek
pořídil	pořídit	k5eAaPmAgMnS	pořídit
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
následovník	následovník	k1gMnSc1	následovník
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Uranu	Uran	k1gInSc3	Uran
a	a	k8xC	a
Neptunu	Neptun	k1gInSc3	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3	nejrozsáhlejší
výzkum	výzkum	k1gInSc1	výzkum
provedla	provést	k5eAaPmAgFnS	provést
již	již	k6eAd1	již
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
sonda	sonda	k1gFnSc1	sonda
Cassini-Huygens	Cassini-Huygensa	k1gFnPc2	Cassini-Huygensa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Saturnu	Saturn	k1gInSc2	Saturn
operovala	operovat	k5eAaImAgFnS	operovat
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2004	[number]	k4	2004
a	a	k8xC	a
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
127	[number]	k4	127
<g/>
krát	krát	k6eAd1	krát
proletěla	proletět	k5eAaPmAgNnP	proletět
kolem	kolem	k7c2	kolem
Titanu	titan	k1gInSc2	titan
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
vypustila	vypustit	k5eAaPmAgFnS	vypustit
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
přistávací	přistávací	k2eAgInSc1d1	přistávací
modul	modul	k1gInSc1	modul
Huygens	Huygens	k1gInSc1	Huygens
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
přistál	přistát	k5eAaImAgInS	přistát
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
pořídil	pořídit	k5eAaPmAgMnS	pořídit
několik	několik	k4yIc4	několik
fotografií	fotografia	k1gFnPc2	fotografia
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
Titan	titan	k1gInSc1	titan
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1655	[number]	k4	1655
nizozemským	nizozemský	k2eAgMnSc7d1	nizozemský
astronomem	astronom	k1gMnSc7	astronom
a	a	k8xC	a
fyzikem	fyzik	k1gMnSc7	fyzik
Christiaanem	Christiaan	k1gMnSc7	Christiaan
Huygensem	Huygens	k1gMnSc7	Huygens
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
Galileovými	Galileův	k2eAgMnPc7d1	Galileův
objevy	objev	k1gInPc4	objev
čtyř	čtyři	k4xCgInPc2	čtyři
největších	veliký	k2eAgInPc2d3	veliký
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
inovacemi	inovace	k1gFnPc7	inovace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
technologie	technologie	k1gFnPc1	technologie
teleskopů	teleskop	k1gInPc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Constantinem	Constantin	k1gMnSc7	Constantin
Huygensem	Huygens	k1gMnSc7	Huygens
začali	začít	k5eAaPmAgMnP	začít
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1650	[number]	k4	1650
stavět	stavět	k5eAaImF	stavět
vlastní	vlastní	k2eAgInSc4d1	vlastní
teleskopy	teleskop	k1gInPc4	teleskop
a	a	k8xC	a
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
následně	následně	k6eAd1	následně
Christiaan	Christiaan	k1gInSc4	Christiaan
poprvé	poprvé	k6eAd1	poprvé
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
planetu	planeta	k1gFnSc4	planeta
Saturn	Saturn	k1gMnSc1	Saturn
a	a	k8xC	a
objevil	objevit	k5eAaPmAgInS	objevit
měsíc	měsíc	k1gInSc1	měsíc
Titan	titan	k1gInSc4	titan
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
šestou	šestý	k4xOgFnSc4	šestý
oběžnici	oběžnice	k1gFnSc4	oběžnice
planety	planeta	k1gFnSc2	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Huygens	Huygens	k6eAd1	Huygens
objevené	objevený	k2eAgNnSc1d1	objevené
těleso	těleso	k1gNnSc1	těleso
nazval	nazvat	k5eAaPmAgInS	nazvat
Luna	luna	k1gFnSc1	luna
Saturni	Saturni	k1gMnPc3	Saturni
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Saturni	Saturni	k1gFnSc1	Saturni
Luna	luna	k1gFnSc1	luna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
latinsky	latinsky	k6eAd1	latinsky
Saturnův	Saturnův	k2eAgInSc4d1	Saturnův
měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
publikoval	publikovat	k5eAaBmAgMnS	publikovat
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1655	[number]	k4	1655
ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
De	De	k?	De
Saturni	Saturni	k1gFnPc2	Saturni
Luna	luna	k1gFnSc1	luna
Observatio	Observatio	k6eAd1	Observatio
Nova	novum	k1gNnSc2	novum
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Giovanni	Giovann	k1gMnPc1	Giovann
Domenico	Domenico	k6eAd1	Domenico
Cassini	Cassin	k2eAgMnPc1d1	Cassin
publikoval	publikovat	k5eAaBmAgMnS	publikovat
své	svůj	k3xOyFgInPc4	svůj
objevy	objev	k1gInPc4	objev
čtyř	čtyři	k4xCgInPc2	čtyři
dalších	další	k2eAgInPc2d1	další
Saturnových	Saturnův	k2eAgInPc2d1	Saturnův
měsíců	měsíc	k1gInPc2	měsíc
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1673	[number]	k4	1673
a	a	k8xC	a
1686	[number]	k4	1686
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
astronomy	astronom	k1gMnPc7	astronom
tyto	tento	k3xDgFnPc4	tento
Cassiniho	Cassini	k1gMnSc2	Cassini
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Titan	titan	k1gInSc1	titan
označovány	označován	k2eAgInPc1d1	označován
číselně	číselně	k6eAd1	číselně
Saturn	Saturn	k1gInSc4	Saturn
I	i	k9	i
až	až	k9	až
V	V	kA	V
(	(	kIx(	(
<g/>
Titan	titan	k1gInSc1	titan
nesl	nést	k5eAaImAgInS	nést
tehdy	tehdy	k6eAd1	tehdy
označení	označení	k1gNnSc4	označení
IV	Iva	k1gFnPc2	Iva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
přídomkem	přídomek	k1gInSc7	přídomek
pro	pro	k7c4	pro
Titan	titan	k1gInSc4	titan
bylo	být	k5eAaImAgNnS	být
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
satelit	satelit	k1gInSc1	satelit
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevech	objev	k1gInPc6	objev
dalších	další	k2eAgInPc2d1	další
měsíců	měsíc	k1gInPc2	měsíc
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
bylo	být	k5eAaImAgNnS	být
Titanu	titan	k1gInSc6	titan
oficiálně	oficiálně	k6eAd1	oficiálně
přiděleno	přidělen	k2eAgNnSc1d1	přiděleno
stálé	stálý	k2eAgNnSc1d1	stálé
označení	označení	k1gNnSc1	označení
Saturn	Saturn	k1gInSc1	Saturn
VI	VI	kA	VI
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyřešily	vyřešit	k5eAaPmAgFnP	vyřešit
nejasnosti	nejasnost	k1gFnPc1	nejasnost
ve	v	k7c6	v
značení	značení	k1gNnSc6	značení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
měsíc	měsíc	k1gInSc1	měsíc
označován	označovat	k5eAaImNgInS	označovat
čísly	číslo	k1gNnPc7	číslo
II	II	kA	II
<g/>
,	,	kIx,	,
IV	IV	kA	IV
i	i	k8xC	i
VI	VI	kA	VI
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byly	být	k5eAaImAgInP	být
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
domluvě	domluva	k1gFnSc3	domluva
nalezeny	nalézt	k5eAaBmNgInP	nalézt
další	další	k2eAgInPc1d1	další
bližší	blízký	k2eAgInPc1d2	bližší
měsíce	měsíc	k1gInPc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Titan	titan	k1gInSc1	titan
měsíci	měsíc	k1gInPc7	měsíc
udělil	udělit	k5eAaPmAgMnS	udělit
John	John	k1gMnSc1	John
Herschel	Herschel	k1gMnSc1	Herschel
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
Williama	William	k1gMnSc2	William
Herschela	Herschel	k1gMnSc2	Herschel
<g/>
,	,	kIx,	,
objevitele	objevitel	k1gMnSc2	objevitel
měsíců	měsíc	k1gInPc2	měsíc
Mimas	Mimas	k1gInSc1	Mimas
a	a	k8xC	a
Enceladus	Enceladus	k1gInSc1	Enceladus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
publikaci	publikace	k1gFnSc6	publikace
Results	Resultsa	k1gFnPc2	Resultsa
of	of	k?	of
Astronomical	Astronomical	k1gFnSc2	Astronomical
Observations	Observationsa	k1gFnPc2	Observationsa
Made	Mad	k1gFnSc2	Mad
during	during	k1gInSc1	during
the	the	k?	the
Years	Years	k1gInSc1	Years
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
at	at	k?	at
the	the	k?	the
Cape	capat	k5eAaImIp3nS	capat
of	of	k?	of
Good	Good	k1gInSc1	Good
Hope	Hop	k1gFnSc2	Hop
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
práci	práce	k1gFnSc6	práce
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
názvy	název	k1gInPc4	název
pro	pro	k7c4	pro
všech	všecek	k3xTgNnPc2	všecek
sedm	sedm	k4xCc4	sedm
tehdy	tehdy	k6eAd1	tehdy
známých	známý	k2eAgInPc2d1	známý
měsíců	měsíc	k1gInPc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
po	po	k7c6	po
mýtických	mýtický	k2eAgMnPc6d1	mýtický
Titánech	Titán	k1gMnPc6	Titán
<g/>
,	,	kIx,	,
bratrech	bratr	k1gMnPc6	bratr
a	a	k8xC	a
sestrách	sestra	k1gFnPc6	sestra
Krona	Kron	k1gMnSc4	Kron
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
řecký	řecký	k2eAgInSc1d1	řecký
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
boha	bůh	k1gMnSc2	bůh
Saturna	Saturn	k1gMnSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Titánové	Titán	k1gMnPc1	Titán
byli	být	k5eAaImAgMnP	být
potomky	potomek	k1gMnPc7	potomek
bohyně	bohyně	k1gFnSc2	bohyně
země	zem	k1gFnSc2	zem
Gaii	Gai	k1gFnSc2	Gai
a	a	k8xC	a
boha	bůh	k1gMnSc2	bůh
nebes	nebesa	k1gNnPc2	nebesa
Úrana	Úrano	k1gNnSc2	Úrano
a	a	k8xC	a
vládli	vládnout	k5eAaImAgMnP	vládnout
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
během	během	k7c2	během
zlatého	zlatý	k2eAgInSc2d1	zlatý
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
planetu	planeta	k1gFnSc4	planeta
Saturn	Saturn	k1gInSc1	Saturn
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
15	[number]	k4	15
pozemských	pozemský	k2eAgInPc2d1	pozemský
dní	den	k1gInPc2	den
a	a	k8xC	a
22	[number]	k4	22
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
poloosa	poloosa	k1gFnSc1	poloosa
jeho	jeho	k3xOp3gFnSc2	jeho
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
221	[number]	k4	221
870	[number]	k4	870
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Titan	titan	k1gInSc4	titan
staví	stavit	k5eAaImIp3nS	stavit
na	na	k7c4	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
dosud	dosud	k6eAd1	dosud
objevenými	objevený	k2eAgInPc7d1	objevený
měsíci	měsíc	k1gInPc7	měsíc
(	(	kIx(	(
<g/>
počítáno	počítat	k5eAaImNgNnS	počítat
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
velkými	velký	k2eAgInPc7d1	velký
měsíci	měsíc	k1gInPc7	měsíc
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
hmotnosti	hmotnost	k1gFnSc3	hmotnost
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
elipsoidního	elipsoidní	k2eAgInSc2d1	elipsoidní
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
satelity	satelit	k1gInPc1	satelit
velkých	velký	k2eAgFnPc2d1	velká
planet	planeta	k1gFnPc2	planeta
obíhá	obíhat	k5eAaImIp3nS	obíhat
ve	v	k7c6	v
vázané	vázaný	k2eAgFnSc6d1	vázaná
rotaci	rotace	k1gFnSc6	rotace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
rotační	rotační	k2eAgFnSc1d1	rotační
perioda	perioda	k1gFnSc1	perioda
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
přikloněn	přikloněn	k2eAgMnSc1d1	přikloněn
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc7d1	stejná
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
polokouli	polokoule	k1gFnSc6	polokoule
Titanu	titan	k1gInSc2	titan
Saturn	Saturn	k1gInSc1	Saturn
viditelný	viditelný	k2eAgInSc1d1	viditelný
neustále	neustále	k6eAd1	neustále
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
ho	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
stále	stále	k6eAd1	stále
pozorovat	pozorovat	k5eAaImF	pozorovat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
zenitu	zenit	k1gInSc6	zenit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c4	na
opačné	opačný	k2eAgNnSc4d1	opačné
ho	on	k3xPp3gNnSc4	on
nelze	lze	k6eNd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc1d1	zeměpisná
délky	délka	k1gFnPc1	délka
jsou	být	k5eAaImIp3nP	být
počítány	počítat	k5eAaImNgFnP	počítat
západně	západně	k6eAd1	západně
od	od	k7c2	od
poledníku	poledník	k1gInSc2	poledník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
tímto	tento	k3xDgNnSc7	tento
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Excentricita	excentricita	k1gFnSc1	excentricita
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
0,0288	[number]	k4	0,0288
a	a	k8xC	a
rovina	rovina	k1gFnSc1	rovina
oběhu	oběh	k1gInSc2	oběh
je	být	k5eAaImIp3nS	být
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
Saturnova	Saturnův	k2eAgInSc2d1	Saturnův
rovníku	rovník	k1gInSc2	rovník
o	o	k7c4	o
0,348	[number]	k4	0,348
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
Titan	titan	k1gInSc1	titan
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
jako	jako	k9	jako
kotouček	kotouček	k1gInSc1	kotouček
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
0,8	[number]	k4	0,8
úhlových	úhlový	k2eAgFnPc2d1	úhlová
vteřin	vteřina	k1gFnPc2	vteřina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Saturnu	Saturn	k1gInSc2	Saturn
vzdálen	vzdáleno	k1gNnPc2	vzdáleno
až	až	k8xS	až
3	[number]	k4	3
<g/>
'	'	kIx"	'
14,29	[number]	k4	14,29
<g/>
''	''	k?	''
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
měsíc	měsíc	k1gInSc1	měsíc
Hyperion	Hyperion	k1gInSc1	Hyperion
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
s	s	k7c7	s
Titanem	titan	k1gInSc7	titan
v	v	k7c6	v
dráhové	dráhový	k2eAgFnSc6d1	dráhová
rezonanci	rezonance	k1gFnSc6	rezonance
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pomalý	pomalý	k2eAgInSc4d1	pomalý
a	a	k8xC	a
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
"	"	kIx"	"
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
této	tento	k3xDgFnSc2	tento
rezonanční	rezonanční	k2eAgFnSc2d1	rezonanční
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
Hyperion	Hyperion	k1gInSc1	Hyperion
migroval	migrovat	k5eAaImAgInS	migrovat
z	z	k7c2	z
chaotické	chaotický	k2eAgFnSc2d1	chaotická
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
podle	podle	k7c2	podle
modelů	model	k1gInPc2	model
za	za	k7c4	za
nepravděpodobný	pravděpodobný	k2eNgInSc4d1	nepravděpodobný
<g/>
.	.	kIx.	.
</s>
<s>
Hyperion	Hyperion	k1gInSc1	Hyperion
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
zformoval	zformovat	k5eAaPmAgMnS	zformovat
na	na	k7c6	na
stabilní	stabilní	k2eAgFnSc6d1	stabilní
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
velký	velký	k2eAgInSc1d1	velký
Titan	titan	k1gInSc1	titan
přitahoval	přitahovat	k5eAaImAgInS	přitahovat
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
vystřeloval	vystřelovat	k5eAaImAgInS	vystřelovat
tělesa	těleso	k1gNnPc4	těleso
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gInSc4	Saturn
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
největší	veliký	k2eAgInSc4d3	veliký
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
5	[number]	k4	5
151	[number]	k4	151
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
1,06	[number]	k4	1,06
<g/>
krát	krát	k6eAd1	krát
rozměr	rozměr	k1gInSc4	rozměr
planety	planeta	k1gFnSc2	planeta
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
1,48	[number]	k4	1,48
<g/>
krát	krát	k6eAd1	krát
rozměr	rozměr	k1gInSc4	rozměr
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
0,40	[number]	k4	0,40
<g/>
krát	krát	k6eAd1	krát
rozměr	rozměr	k1gInSc4	rozměr
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgMnPc3d1	ostatní
satelitům	satelit	k1gMnPc3	satelit
Saturnovy	Saturnův	k2eAgFnSc2d1	Saturnova
rodiny	rodina	k1gFnSc2	rodina
je	být	k5eAaImIp3nS	být
bezkonkurenčně	bezkonkurenčně	k6eAd1	bezkonkurenčně
nejmohutnější	mohutný	k2eAgFnSc1d3	nejmohutnější
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejtěžší	těžký	k2eAgFnSc1d3	nejtěžší
Rhea	Rhea	k1gFnSc1	Rhea
je	být	k5eAaImIp3nS	být
skoro	skoro	k6eAd1	skoro
60	[number]	k4	60
<g/>
×	×	k?	×
lehčí	lehčit	k5eAaImIp3nS	lehčit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
byl	být	k5eAaImAgInS	být
Titan	titan	k1gInSc1	titan
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
–	–	k?	–
dokonce	dokonce	k9	dokonce
byl	být	k5eAaImAgInS	být
mylně	mylně	k6eAd1	mylně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
Ganymed	Ganymed	k1gMnSc1	Ganymed
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
626	[number]	k4	626
km	km	kA	km
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
chyba	chyba	k1gFnSc1	chyba
byla	být	k5eAaImAgFnS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
velmi	velmi	k6eAd1	velmi
vysokou	vysoká	k1gFnSc7	vysoká
a	a	k8xC	a
hustou	hustý	k2eAgFnSc7d1	hustá
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
jevil	jevit	k5eAaImAgInS	jevit
Titan	titan	k1gInSc1	titan
větší	veliký	k2eAgInSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
míru	míra	k1gFnSc4	míra
to	ten	k3xDgNnSc1	ten
uvedla	uvést	k5eAaPmAgFnS	uvést
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
uvedeném	uvedený	k2eAgInSc6d1	uvedený
roce	rok	k1gInSc6	rok
kolem	kolem	k7c2	kolem
Titanu	titan	k1gInSc2	titan
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
<g/>
.	.	kIx.	.
</s>
<s>
Rozměry	rozměra	k1gFnPc1	rozměra
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
Titanu	titan	k1gInSc2	titan
jsou	být	k5eAaImIp3nP	být
srovnatelné	srovnatelný	k2eAgInPc1d1	srovnatelný
s	s	k7c7	s
Jupiterovými	Jupiterův	k2eAgInPc7d1	Jupiterův
měsíci	měsíc	k1gInPc7	měsíc
Ganymed	Ganymed	k1gMnSc1	Ganymed
a	a	k8xC	a
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnSc1d2	nižší
hustota	hustota	k1gFnSc1	hustota
1,88	[number]	k4	1,88
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
40	[number]	k4	40
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
nízká	nízký	k2eAgFnSc1d1	nízká
hustota	hustota	k1gFnSc1	hustota
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
přes	přes	k7c4	přes
velké	velký	k2eAgInPc4d1	velký
rozměry	rozměr	k1gInPc4	rozměr
je	být	k5eAaImIp3nS	být
gravitační	gravitační	k2eAgNnSc1d1	gravitační
zrychlení	zrychlení	k1gNnSc1	zrychlení
u	u	k7c2	u
povrchu	povrch	k1gInSc2	povrch
spíše	spíše	k9	spíše
malé	malý	k2eAgNnSc1d1	malé
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
Titan	titan	k1gInSc4	titan
přibližně	přibližně	k6eAd1	přibližně
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
hmotný	hmotný	k2eAgMnSc1d1	hmotný
oproti	oproti	k7c3	oproti
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
gravitační	gravitační	k2eAgNnSc1d1	gravitační
zrychlení	zrychlení	k1gNnSc1	zrychlení
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgNnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
získané	získaný	k2eAgFnSc2d1	získaná
hodnoty	hodnota	k1gFnSc2	hodnota
hustoty	hustota	k1gFnSc2	hustota
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Titan	titan	k1gInSc1	titan
skládá	skládat	k5eAaImIp3nS	skládat
napůl	napůl	k6eAd1	napůl
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
a	a	k8xC	a
napůl	napůl	k6eAd1	napůl
z	z	k7c2	z
kamenného	kamenný	k2eAgInSc2d1	kamenný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
toto	tento	k3xDgNnSc4	tento
složení	složení	k1gNnSc4	složení
celkem	celkem	k6eAd1	celkem
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
měsícům	měsíc	k1gInPc3	měsíc
Dione	Dion	k1gInSc5	Dion
a	a	k8xC	a
Enceladus	Enceladus	k1gInSc4	Enceladus
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
gravitační	gravitační	k2eAgFnSc3d1	gravitační
kompresi	komprese	k1gFnSc3	komprese
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
Titanu	titan	k1gInSc2	titan
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Titanu	titan	k1gInSc2	titan
ku	k	k7c3	k
hmotnosti	hmotnost	k1gFnSc3	hmotnost
Saturnu	Saturn	k1gInSc2	Saturn
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
mezi	mezi	k7c7	mezi
plynnými	plynný	k2eAgFnPc7d1	plynná
obry	obr	k1gMnPc4	obr
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4226	[number]	k4	4226
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
relativní	relativní	k2eAgInSc1d1	relativní
rozměr	rozměr	k1gInSc1	rozměr
Titanu	titan	k1gInSc2	titan
vůči	vůči	k7c3	vůči
rozměrům	rozměr	k1gInPc3	rozměr
Saturnu	Saturn	k1gInSc2	Saturn
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
hodnotě	hodnota	k1gFnSc6	hodnota
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
609	[number]	k4	609
druhý	druhý	k4xOgInSc4	druhý
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
mezi	mezi	k7c7	mezi
plynnými	plynný	k2eAgMnPc7d1	plynný
obry	obr	k1gMnPc7	obr
<g/>
..	..	k?	..
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Titanu	titan	k1gInSc2	titan
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kamenné	kamenný	k2eAgNnSc1d1	kamenné
jádro	jádro	k1gNnSc1	jádro
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
~	~	kIx~	~
<g/>
3	[number]	k4	3
740	[number]	k4	740
km	km	kA	km
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
uváděny	uvádět	k5eAaImNgInP	uvádět
údaje	údaj	k1gInPc1	údaj
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
3	[number]	k4	3
340	[number]	k4	340
až	až	k9	až
4	[number]	k4	4
000	[number]	k4	000
km	km	kA	km
<g/>
..	..	k?	..
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
diferenciace	diferenciace	k1gFnPc4	diferenciace
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
kovové	kovový	k2eAgNnSc1d1	kovové
jadérko	jadérko	k1gNnSc1	jadérko
obklopené	obklopený	k2eAgNnSc1d1	obklopené
silikátovým	silikátový	k2eAgInSc7d1	silikátový
pláštěm	plášť	k1gInSc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
obepíná	obepínat	k5eAaImIp3nS	obepínat
až	až	k9	až
700	[number]	k4	700
km	km	kA	km
tlustá	tlustý	k2eAgFnSc1d1	tlustá
vrstva	vrstva	k1gFnSc1	vrstva
ledového	ledový	k2eAgInSc2d1	ledový
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgMnS	tvořit
vrstvami	vrstva	k1gFnPc7	vrstva
různých	různý	k2eAgFnPc2d1	různá
forem	forma	k1gFnPc2	forma
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
uvnitř	uvnitř	k7c2	uvnitř
tělesa	těleso	k1gNnSc2	těleso
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
natolik	natolik	k6eAd1	natolik
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vznik	vznik	k1gInSc4	vznik
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
uvězněn	uvězněn	k2eAgInSc1d1	uvězněn
mezi	mezi	k7c7	mezi
kůrou	kůra	k1gFnSc7	kůra
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
běžným	běžný	k2eAgInSc7d1	běžný
šesterečným	šesterečný	k2eAgInSc7d1	šesterečný
ledem	led	k1gInSc7	led
(	(	kIx(	(
<g/>
led	led	k1gInSc1	led
Ih	Ih	k1gFnSc2	Ih
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vnitřními	vnitřní	k2eAgFnPc7d1	vnitřní
vrstvami	vrstva	k1gFnPc7	vrstva
exotických	exotický	k2eAgFnPc2d1	exotická
vysokotlakých	vysokotlaký	k2eAgFnPc2d1	vysokotlaká
forem	forma	k1gFnPc2	forma
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
přítomnost	přítomnost	k1gFnSc1	přítomnost
amoniaku	amoniak	k1gInSc2	amoniak
umožní	umožnit	k5eAaPmIp3nS	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
eutektickou	eutektický	k2eAgFnSc4d1	eutektická
směs	směs	k1gFnSc4	směs
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
kapalná	kapalný	k2eAgFnSc1d1	kapalná
i	i	k9	i
za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
176	[number]	k4	176
K	k	k7c3	k
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
vrstevnaté	vrstevnatý	k2eAgFnSc6d1	vrstevnatá
stavbě	stavba	k1gFnSc6	stavba
tělesa	těleso	k1gNnSc2	těleso
přinesla	přinést	k5eAaPmAgFnS	přinést
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
pozorovala	pozorovat	k5eAaImAgNnP	pozorovat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Titanu	titan	k1gInSc2	titan
extrémně	extrémně	k6eAd1	extrémně
nízkofrekvenční	nízkofrekvenční	k2eAgFnPc1d1	nízkofrekvenční
rádiové	rádiový	k2eAgFnPc1d1	rádiová
vlny	vlna	k1gFnPc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
odráží	odrážet	k5eAaImIp3nS	odrážet
tyto	tento	k3xDgFnPc4	tento
vlny	vlna	k1gFnPc4	vlna
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
slabě	slabě	k6eAd1	slabě
<g/>
,	,	kIx,	,
a	a	k8xC	a
odezvy	odezva	k1gFnPc1	odezva
jsou	být	k5eAaImIp3nP	být
vytvářeny	vytvářet	k5eAaImNgFnP	vytvářet
odrazem	odraz	k1gInSc7	odraz
od	od	k7c2	od
hranice	hranice	k1gFnSc2	hranice
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
ledové	ledový	k2eAgFnSc2d1	ledová
krusty	krusta	k1gFnSc2	krusta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
říjnem	říjen	k1gInSc7	říjen
2005	[number]	k4	2005
a	a	k8xC	a
květnem	květno	k1gNnSc7	květno
2007	[number]	k4	2007
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
pomalé	pomalý	k2eAgNnSc4d1	pomalé
systematické	systematický	k2eAgNnSc4d1	systematické
posunování	posunování	k1gNnSc4	posunování
některých	některý	k3yIgInPc2	některý
povrchových	povrchový	k2eAgInPc2d1	povrchový
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
o	o	k7c4	o
30	[number]	k4	30
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
povrchová	povrchový	k2eAgFnSc1d1	povrchová
kůra	kůra	k1gFnSc1	kůra
nějak	nějak	k6eAd1	nějak
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
rovněž	rovněž	k9	rovněž
podporuje	podporovat	k5eAaImIp3nS	podporovat
teorii	teorie	k1gFnSc4	teorie
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
oddělení	oddělení	k1gNnSc6	oddělení
ledové	ledový	k2eAgFnSc2d1	ledová
krusty	krusta	k1gFnSc2	krusta
od	od	k7c2	od
pevného	pevný	k2eAgNnSc2d1	pevné
jádra	jádro	k1gNnSc2	jádro
kapalným	kapalný	k2eAgInSc7d1	kapalný
oceánem	oceán	k1gInSc7	oceán
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
měření	měření	k1gNnSc4	měření
změn	změna	k1gFnPc2	změna
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
oběhu	oběh	k1gInSc2	oběh
Titanu	titan	k1gInSc2	titan
kolem	kolem	k7c2	kolem
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Srovnání	srovnání	k1gNnSc1	srovnání
výsledků	výsledek	k1gInPc2	výsledek
měření	měření	k1gNnSc2	měření
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
radarového	radarový	k2eAgNnSc2d1	radarové
zkoumání	zkoumání	k1gNnSc2	zkoumání
povrchu	povrch	k1gInSc2	povrch
naznačilo	naznačit	k5eAaPmAgNnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ledová	ledový	k2eAgFnSc1d1	ledová
vrstva	vrstva	k1gFnSc1	vrstva
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíce	měsíc	k1gInPc1	měsíc
Jupitera	Jupiter	k1gMnSc2	Jupiter
i	i	k8xC	i
Saturnu	Saturn	k1gInSc6	Saturn
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
souběžnou	souběžný	k2eAgFnSc7d1	souběžná
akrecí	akrece	k1gFnSc7	akrece
z	z	k7c2	z
akrečního	akreční	k2eAgInSc2d1	akreční
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
okolo	okolo	k7c2	okolo
mladých	mladý	k2eAgMnPc2d1	mladý
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
a	a	k8xC	a
sestával	sestávat	k5eAaImAgMnS	sestávat
ze	z	k7c2	z
zbytkového	zbytkový	k2eAgInSc2d1	zbytkový
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
podobným	podobný	k2eAgInSc7d1	podobný
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInPc3	jaký
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
samotná	samotný	k2eAgFnSc1d1	samotná
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
kolem	kolem	k7c2	kolem
Jupitera	Jupiter	k1gMnSc2	Jupiter
obíhají	obíhat	k5eAaImIp3nP	obíhat
čtyři	čtyři	k4xCgInPc4	čtyři
velké	velký	k2eAgInPc4d1	velký
satelity	satelit	k1gInPc4	satelit
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
pravidelnými	pravidelný	k2eAgFnPc7d1	pravidelná
oběžnými	oběžný	k2eAgFnPc7d1	oběžná
drahami	draha	k1gFnPc7	draha
podobnými	podobný	k2eAgFnPc7d1	podobná
planetárním	planetární	k2eAgNnPc3d1	planetární
<g/>
,	,	kIx,	,
Titan	titan	k1gInSc1	titan
mezi	mezi	k7c7	mezi
Saturnovými	Saturnův	k2eAgInPc7d1	Saturnův
měsíci	měsíc	k1gInPc7	měsíc
dominuje	dominovat	k5eAaImIp3nS	dominovat
a	a	k8xC	a
obíhá	obíhat	k5eAaImIp3nS	obíhat
po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
excentrické	excentrický	k2eAgFnSc6d1	excentrická
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
pouze	pouze	k6eAd1	pouze
teorií	teorie	k1gFnSc7	teorie
současného	současný	k2eAgInSc2d1	současný
vzniku	vznik	k1gInSc2	vznik
těles	těleso	k1gNnPc2	těleso
na	na	k7c6	na
stávajících	stávající	k2eAgFnPc6d1	stávající
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
Uznávaný	uznávaný	k2eAgInSc1d1	uznávaný
model	model	k1gInSc1	model
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
měl	mít	k5eAaImAgInS	mít
Saturn	Saturn	k1gInSc1	Saturn
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
skupinu	skupina	k1gFnSc4	skupina
velkých	velký	k2eAgInPc2d1	velký
satelitů	satelit	k1gInPc2	satelit
podobných	podobný	k2eAgInPc2d1	podobný
Jupiterovým	Jupiterův	k2eAgNnSc7d1	Jupiterovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnPc1	jejich
oběžné	oběžný	k2eAgFnPc1d1	oběžná
dráhy	dráha	k1gFnPc1	dráha
byly	být	k5eAaImAgFnP	být
narušeny	narušit	k5eAaPmNgFnP	narušit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
teorie	teorie	k1gFnSc2	teorie
mohl	moct	k5eAaImAgInS	moct
okolo	okolo	k7c2	okolo
Saturnu	Saturn	k1gInSc2	Saturn
obíhat	obíhat	k5eAaImF	obíhat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Titanem	titan	k1gInSc7	titan
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc1	jeden
měsíc	měsíc	k1gInSc1	měsíc
podobné	podobný	k2eAgFnSc2d1	podobná
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vlivem	vlivem	k7c2	vlivem
těchto	tento	k3xDgFnPc2	tento
změn	změna	k1gFnPc2	změna
zkolaboval	zkolabovat	k5eAaPmAgInS	zkolabovat
do	do	k7c2	do
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
působením	působení	k1gNnSc7	působení
gravitace	gravitace	k1gFnSc2	gravitace
Saturnu	Saturn	k1gInSc2	Saturn
se	se	k3xPyFc4	se
před	před	k7c7	před
dopadem	dopad	k1gInSc7	dopad
roztříštil	roztříštit	k5eAaPmAgInS	roztříštit
<g/>
,	,	kIx,	,
zbytky	zbytek	k1gInPc1	zbytek
kamenného	kamenný	k2eAgNnSc2d1	kamenné
jádra	jádro	k1gNnSc2	jádro
shořely	shořet	k5eAaPmAgFnP	shořet
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
ledový	ledový	k2eAgInSc1d1	ledový
plášť	plášť	k1gInSc1	plášť
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Saturnovy	Saturnův	k2eAgInPc4d1	Saturnův
prstence	prstenec	k1gInPc4	prstenec
a	a	k8xC	a
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc4d1	velký
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenější	vzdálený	k2eAgInSc1d2	vzdálenější
Titan	titan	k1gInSc1	titan
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
stávající	stávající	k2eAgFnSc4d1	stávající
excentrickou	excentrický	k2eAgFnSc4d1	excentrická
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
teorie	teorie	k1gFnSc1	teorie
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
narušení	narušení	k1gNnSc1	narušení
drah	draha	k1gFnPc2	draha
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
srážku	srážka	k1gFnSc4	srážka
dvou	dva	k4xCgInPc2	dva
velkých	velký	k2eAgInPc2d1	velký
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
materiálu	materiál	k1gInSc2	materiál
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
během	během	k7c2	během
impaktu	impakt	k1gInSc2	impakt
mohly	moct	k5eAaImAgFnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
některé	některý	k3yIgInPc4	některý
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc4d1	velký
měsíce	měsíc	k1gInPc4	měsíc
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Iapetus	Iapetus	k1gMnSc1	Iapetus
a	a	k8xC	a
Rhea	Rhea	k1gMnSc1	Rhea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc4	model
dramatického	dramatický	k2eAgInSc2d1	dramatický
vzniku	vznik	k1gInSc2	vznik
by	by	kYmCp3nS	by
rovněž	rovněž	k9	rovněž
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
excentricitu	excentricita	k1gFnSc4	excentricita
Titanovy	Titanovy	k?	Titanovy
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
dusík	dusík	k1gInSc1	dusík
nepochází	pocházet	k5eNaImIp3nS	pocházet
z	z	k7c2	z
materiálu	materiál	k1gInSc2	materiál
akrečního	akreční	k2eAgInSc2d1	akreční
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
materiálu	materiál	k1gInSc3	materiál
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
kometách	kometa	k1gFnPc6	kometa
přilétajících	přilétající	k2eAgFnPc6d1	přilétající
z	z	k7c2	z
Oortova	Oortův	k2eAgInSc2d1	Oortův
oblaku	oblak	k1gInSc2	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Titanu	titan	k1gInSc2	titan
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
přes	přes	k7c4	přes
600	[number]	k4	600
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc1d1	dolní
hranice	hranice	k1gFnSc1	hranice
exosféry	exosféra	k1gFnSc2	exosféra
bývá	bývat	k5eAaImIp3nS	bývat
udávána	udávat	k5eAaImNgFnS	udávat
v	v	k7c6	v
1	[number]	k4	1
500	[number]	k4	500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc4d1	jediný
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
takto	takto	k6eAd1	takto
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nižší	nízký	k2eAgFnSc3d2	nižší
gravitaci	gravitace	k1gFnSc3	gravitace
Titanu	titan	k1gInSc2	titan
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
atmosféra	atmosféra	k1gFnSc1	atmosféra
do	do	k7c2	do
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc2d2	veliký
výšky	výška	k1gFnSc2	výška
nad	nad	k7c4	nad
povrch	povrch	k1gInSc4	povrch
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
pozemské	pozemský	k2eAgNnSc4d1	pozemské
také	také	k9	také
jediná	jediný	k2eAgFnSc1d1	jediná
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
plynný	plynný	k2eAgInSc1d1	plynný
molekulární	molekulární	k2eAgInSc1d1	molekulární
dusík	dusík	k1gInSc1	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
minoritní	minoritní	k2eAgFnSc7d1	minoritní
složkou	složka	k1gFnSc7	složka
je	být	k5eAaImIp3nS	být
methan	methan	k1gInSc1	methan
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
povrchu	povrch	k1gInSc2	povrch
je	být	k5eAaImIp3nS	být
dusík	dusík	k1gInSc1	dusík
zastoupen	zastoupen	k2eAgInSc1d1	zastoupen
95	[number]	k4	95
%	%	kIx~	%
<g/>
,	,	kIx,	,
methan	methan	k1gInSc1	methan
představuje	představovat	k5eAaImIp3nS	představovat
4,9	[number]	k4	4,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zbylá	zbylý	k2eAgFnSc1d1	zbylá
část	část	k1gFnSc1	část
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
ostatní	ostatní	k2eAgInPc4d1	ostatní
plyny	plyn	k1gInPc4	plyn
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
nejvíce	hodně	k6eAd3	hodně
zastoupen	zastoupen	k2eAgInSc1d1	zastoupen
vodík	vodík	k1gInSc1	vodík
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
–	–	k?	–
0,2	[number]	k4	0,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vzrůstající	vzrůstající	k2eAgFnSc7d1	vzrůstající
výškou	výška	k1gFnSc7	výška
klesá	klesat	k5eAaImIp3nS	klesat
procentuální	procentuální	k2eAgNnSc4d1	procentuální
zastoupení	zastoupení	k1gNnSc4	zastoupení
methanu	methan	k1gInSc2	methan
i	i	k8xC	i
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
vodík	vodík	k1gInSc1	vodík
úplně	úplně	k6eAd1	úplně
mizí	mizet	k5eAaImIp3nS	mizet
a	a	k8xC	a
methan	methan	k1gInSc1	methan
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
pouze	pouze	k6eAd1	pouze
1,4	[number]	k4	1,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
stopová	stopová	k1gFnSc1	stopová
množství	množství	k1gNnSc2	množství
dalších	další	k2eAgInPc2d1	další
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
butadiinu	butadiin	k2eAgFnSc4d1	butadiin
<g/>
,	,	kIx,	,
propynu	propyn	k1gInSc6	propyn
<g/>
,	,	kIx,	,
ethynu	ethyn	k1gInSc6	ethyn
<g/>
,	,	kIx,	,
ethanu	ethan	k1gInSc6	ethan
<g/>
,	,	kIx,	,
propanu	propan	k1gInSc6	propan
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
plynů	plyn	k1gInPc2	plyn
–	–	k?	–
kyanoacetylenu	kyanoacetylen	k1gInSc2	kyanoacetylen
<g/>
,	,	kIx,	,
kyanovodíku	kyanovodík	k1gInSc2	kyanovodík
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
<g/>
,	,	kIx,	,
dikyanu	dikyan	k1gInSc2	dikyan
<g/>
,	,	kIx,	,
argonu	argon	k1gInSc2	argon
a	a	k8xC	a
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
vznikají	vznikat	k5eAaImIp3nP	vznikat
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
Titanovy	Titanovy	k?	Titanovy
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
štěpí	štěpit	k5eAaImIp3nS	štěpit
methan	methan	k1gInSc1	methan
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
širokou	široký	k2eAgFnSc4d1	široká
vrstvu	vrstva	k1gFnSc4	vrstva
oranžového	oranžový	k2eAgInSc2d1	oranžový
smogu	smog	k1gInSc2	smog
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
polycyklických	polycyklický	k2eAgInPc2d1	polycyklický
aromatických	aromatický	k2eAgInPc2d1	aromatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dávají	dávat	k5eAaImIp3nP	dávat
měsíci	měsíc	k1gInSc6	měsíc
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
oranžové	oranžový	k2eAgNnSc1d1	oranžové
zabarvení	zabarvení	k1gNnSc1	zabarvení
<g/>
,	,	kIx,	,
dalších	další	k2eAgInPc2d1	další
polymerů	polymer	k1gInPc2	polymer
a	a	k8xC	a
nitrilů	nitril	k1gInPc2	nitril
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
atmosféra	atmosféra	k1gFnSc1	atmosféra
neprůhledná	průhledný	k2eNgFnSc1d1	neprůhledná
pro	pro	k7c4	pro
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
z	z	k7c2	z
orbity	orbita	k1gFnSc2	orbita
získat	získat	k5eAaPmF	získat
kompletní	kompletní	k2eAgNnSc4d1	kompletní
spektrum	spektrum	k1gNnSc4	spektrum
odrazivosti	odrazivost	k1gFnSc2	odrazivost
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
nebyly	být	k5eNaImAgFnP	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
žádné	žádný	k3yNgInPc4	žádný
snímky	snímek	k1gInPc4	snímek
povrchu	povrch	k1gInSc2	povrch
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
pořídila	pořídit	k5eAaPmAgFnS	pořídit
až	až	k9	až
sonda	sonda	k1gFnSc1	sonda
Cassini-Huygens	Cassini-Huygensa	k1gFnPc2	Cassini-Huygensa
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
tráví	trávit	k5eAaImIp3nS	trávit
95	[number]	k4	95
%	%	kIx~	%
času	čas	k1gInSc2	čas
v	v	k7c6	v
magnetosféře	magnetosféra	k1gFnSc6	magnetosféra
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
dopadajícím	dopadající	k2eAgInSc7d1	dopadající
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
by	by	kYmCp3nS	by
sluneční	sluneční	k2eAgFnSc1d1	sluneční
energie	energie	k1gFnSc1	energie
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
schopna	schopen	k2eAgFnSc1d1	schopna
veškerý	veškerý	k3xTgInSc4	veškerý
methan	methan	k1gInSc4	methan
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
složitější	složitý	k2eAgInPc4d2	složitější
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
během	během	k7c2	během
geologicky	geologicky	k6eAd1	geologicky
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
stáří	stáří	k1gNnSc3	stáří
soustavy	soustava	k1gFnSc2	soustava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
stálá	stálý	k2eAgFnSc1d1	stálá
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
<g/>
,	,	kIx,	,
musejí	muset	k5eAaImIp3nP	muset
nacházet	nacházet	k5eAaImF	nacházet
zásobárny	zásobárna	k1gFnPc4	zásobárna
methanu	methan	k1gInSc2	methan
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
methan	methan	k1gInSc1	methan
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Prapůvodní	prapůvodní	k2eAgFnPc1d1	prapůvodní
zásoby	zásoba	k1gFnPc1	zásoba
methanu	methan	k1gInSc2	methan
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
nacházet	nacházet	k5eAaImF	nacházet
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
dostaly	dostat	k5eAaPmAgInP	dostat
během	během	k7c2	během
erupcí	erupce	k1gFnPc2	erupce
kryovulkánů	kryovulkán	k1gMnPc2	kryovulkán
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
oznámili	oznámit	k5eAaPmAgMnP	oznámit
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
že	že	k8xS	že
dle	dle	k7c2	dle
jejich	jejich	k3xOp3gNnPc2	jejich
studií	studio	k1gNnPc2	studio
simulací	simulace	k1gFnPc2	simulace
Titanovy	Titanovy	k?	Titanovy
atmosféry	atmosféra	k1gFnSc2	atmosféra
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
i	i	k9	i
komplexní	komplexní	k2eAgFnPc1d1	komplexní
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
ohlásili	ohlásit	k5eAaPmAgMnP	ohlásit
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Andaluského	andaluský	k2eAgInSc2d1	andaluský
astronomického	astronomický	k2eAgInSc2d1	astronomický
insitutu	insitut	k1gInSc2	insitut
(	(	kIx(	(
<g/>
IAA-CSIC	IAA-CSIC	k1gFnSc1	IAA-CSIC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vrchních	vrchní	k2eAgFnPc6d1	vrchní
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
detekovali	detekovat	k5eAaImAgMnP	detekovat
polycyklické	polycyklický	k2eAgInPc4d1	polycyklický
aromatické	aromatický	k2eAgInPc4d1	aromatický
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
sondě	sonda	k1gFnSc3	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
pomocí	pomocí	k7c2	pomocí
kombinovaného	kombinovaný	k2eAgInSc2d1	kombinovaný
infračerveného	infračervený	k2eAgInSc2d1	infračervený
spektrometru	spektrometr	k1gInSc2	spektrometr
(	(	kIx(	(
<g/>
CIRS	CIRS	kA	CIRS
<g/>
)	)	kIx)	)
detekovat	detekovat	k5eAaImF	detekovat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
propen	propen	k1gInSc4	propen
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
úplně	úplně	k6eAd1	úplně
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
co	co	k9	co
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
uhlovodík	uhlovodík	k1gInSc1	uhlovodík
objeven	objevit	k5eAaPmNgInS	objevit
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
tělese	těleso	k1gNnSc6	těleso
než	než	k8xS	než
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
první	první	k4xOgFnSc1	první
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
CIRS	CIRS	kA	CIRS
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
nalezl	nalézt	k5eAaBmAgMnS	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
provedla	provést	k5eAaPmAgFnS	provést
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
oranžovohnědou	oranžovohnědý	k2eAgFnSc4d1	oranžovohnědá
mlhu	mlha	k1gFnSc4	mlha
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
tvoří	tvořit	k5eAaImIp3nP	tvořit
právě	právě	k9	právě
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
rekombinací	rekombinace	k1gFnSc7	rekombinace
radikálů	radikál	k1gMnPc2	radikál
vzniklých	vzniklý	k2eAgMnPc2d1	vzniklý
fotolýzou	fotolýza	k1gFnSc7	fotolýza
slunečními	sluneční	k2eAgInPc7d1	sluneční
paprsky	paprsek	k1gInPc7	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
Titanu	titan	k1gInSc2	titan
pozorována	pozorován	k2eAgNnPc1d1	pozorováno
perleťová	perleťový	k2eAgNnPc1d1	perleťové
oblaka	oblaka	k1gNnPc1	oblaka
methanu	methan	k1gInSc2	methan
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Titan	titan	k1gInSc1	titan
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
planeta	planeta	k1gFnSc1	planeta
Venuše	Venuše	k1gFnSc2	Venuše
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
super	super	k2eAgInPc1d1	super
rotátory	rotátor	k1gInPc1	rotátor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
atmosféra	atmosféra	k1gFnSc1	atmosféra
rotuje	rotovat	k5eAaImIp3nS	rotovat
významně	významně	k6eAd1	významně
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
superrotace	superrotace	k1gFnSc2	superrotace
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
patrný	patrný	k2eAgInSc1d1	patrný
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rychlost	rychlost	k1gFnSc1	rychlost
větrů	vítr	k1gInPc2	vítr
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
720	[number]	k4	720
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
troposféry	troposféra	k1gFnSc2	troposféra
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
superrotace	superrotace	k1gFnSc2	superrotace
západní	západní	k2eAgInPc4d1	západní
větry	vítr	k1gInPc7	vítr
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
okolo	okolo	k7c2	okolo
120	[number]	k4	120
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
vzniku	vznik	k1gInSc2	vznik
superrotace	superrotace	k1gFnPc1	superrotace
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
vysvětleny	vysvětlen	k2eAgInPc1d1	vysvětlen
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
vlastnost	vlastnost	k1gFnSc4	vlastnost
pomalu	pomalu	k6eAd1	pomalu
rotujících	rotující	k2eAgNnPc2d1	rotující
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Přesnější	přesný	k2eAgFnPc1d2	přesnější
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
větrů	vítr	k1gInPc2	vítr
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
přistávací	přistávací	k2eAgInSc1d1	přistávací
modul	modul	k1gInSc1	modul
Huygens	Huygensa	k1gFnPc2	Huygensa
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
sestupu	sestup	k1gInSc6	sestup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
klesání	klesání	k1gNnSc2	klesání
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
od	od	k7c2	od
140	[number]	k4	140
do	do	k7c2	do
120	[number]	k4	120
km	km	kA	km
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
turbulentní	turbulentní	k2eAgInSc4d1	turbulentní
západní	západní	k2eAgInSc4d1	západní
vichr	vichr	k1gInSc4	vichr
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
450	[number]	k4	450
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
S	s	k7c7	s
postupně	postupně	k6eAd1	postupně
klesající	klesající	k2eAgFnSc7d1	klesající
výškou	výška	k1gFnSc7	výška
vítr	vítr	k1gInSc1	vítr
slábl	slábnout	k5eAaImAgInS	slábnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
70	[number]	k4	70
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
byl	být	k5eAaImAgInS	být
celkem	celkem	k6eAd1	celkem
slabý	slabý	k2eAgInSc1d1	slabý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c4	mezi
100	[number]	k4	100
až	až	k9	až
60	[number]	k4	60
km	km	kA	km
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
turbulence	turbulence	k1gFnPc1	turbulence
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
65	[number]	k4	65
a	a	k8xC	a
55	[number]	k4	55
km	km	kA	km
vítr	vítr	k1gInSc1	vítr
znovu	znovu	k6eAd1	znovu
zesílil	zesílit	k5eAaPmAgInS	zesílit
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
opět	opět	k6eAd1	opět
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
průběhu	průběh	k1gInSc6	průběh
klesání	klesání	k1gNnSc1	klesání
slábl	slábnout	k5eAaImAgMnS	slábnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
se	se	k3xPyFc4	se
směr	směr	k1gInSc1	směr
větru	vítr	k1gInSc2	vítr
najednou	najednou	k6eAd1	najednou
otočil	otočit	k5eAaPmAgInS	otočit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
východní	východní	k2eAgInSc1d1	východní
vítr	vítr	k1gInSc1	vítr
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
U	u	k7c2	u
povrchu	povrch	k1gInSc2	povrch
Titanu	titan	k1gInSc2	titan
rychlost	rychlost	k1gFnSc4	rychlost
větru	vítr	k1gInSc2	vítr
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
1	[number]	k4	1
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
vane	vanout	k5eAaImIp3nS	vanout
od	od	k7c2	od
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
15	[number]	k4	15
let	léto	k1gNnPc2	léto
v	v	k7c6	v
období	období	k1gNnSc6	období
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
se	se	k3xPyFc4	se
však	však	k9	však
vyskytnou	vyskytnout	k5eAaPmIp3nP	vyskytnout
vzácné	vzácný	k2eAgInPc1d1	vzácný
bouřkové	bouřkový	k2eAgInPc1d1	bouřkový
západní	západní	k2eAgInPc1d1	západní
větry	vítr	k1gInPc1	vítr
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
až	až	k9	až
10	[number]	k4	10
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
36	[number]	k4	36
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
východozápadního	východozápadní	k2eAgNnSc2d1	východozápadní
proudění	proudění	k1gNnSc2	proudění
existují	existovat	k5eAaImIp3nP	existovat
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
tzv.	tzv.	kA	tzv.
slapové	slapový	k2eAgInPc4d1	slapový
větry	vítr	k1gInPc4	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc4	ten
vznikají	vznikat	k5eAaImIp3nP	vznikat
interakcemi	interakce	k1gFnPc7	interakce
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	s	k7c7	s
slapovými	slapový	k2eAgFnPc7d1	slapová
silami	síla	k1gFnPc7	síla
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
400	[number]	k4	400
<g/>
krát	krát	k6eAd1	krát
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
jakými	jaký	k3yIgFnPc7	jaký
silami	síla	k1gFnPc7	síla
působí	působit	k5eAaImIp3nS	působit
Měsíc	měsíc	k1gInSc1	měsíc
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
směrovat	směrovat	k5eAaImF	směrovat
pohyb	pohyb	k1gInSc4	pohyb
vzduchu	vzduch	k1gInSc2	vzduch
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
rovníku	rovník	k1gInSc3	rovník
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
těchto	tento	k3xDgInPc2	tento
větrů	vítr	k1gInPc2	vítr
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
0,5	[number]	k4	0,5
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Pozorování	pozorování	k1gNnSc2	pozorování
provedená	provedený	k2eAgFnSc1d1	provedená
sondami	sonda	k1gFnPc7	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
atmosféra	atmosféra	k1gFnSc1	atmosféra
Titanu	titan	k1gInSc2	titan
je	být	k5eAaImIp3nS	být
hustší	hustý	k2eAgFnSc1d2	hustší
než	než	k8xS	než
zemská	zemský	k2eAgFnSc1d1	zemská
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1,45	[number]	k4	1,45
atm	atm	k?	atm
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
sonda	sonda	k1gFnSc1	sonda
Huygens	Huygensa	k1gFnPc2	Huygensa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
při	při	k7c6	při
přistání	přistání	k1gNnSc6	přistání
naměřila	naměřit	k5eAaBmAgFnS	naměřit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
tlak	tlak	k1gInSc1	tlak
146,7	[number]	k4	146,7
kPa	kPa	k?	kPa
<g/>
.	.	kIx.	.
</s>
<s>
Naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
povrchu	povrch	k1gInSc2	povrch
byla	být	k5eAaImAgFnS	být
-179,5	-179,5	k4	-179,5
°	°	k?	°
<g/>
C.	C.	kA	C.
Neprůhledné	průhledný	k2eNgFnPc4d1	neprůhledná
vrstvy	vrstva	k1gFnPc4	vrstva
mlhy	mlha	k1gFnSc2	mlha
zamezují	zamezovat	k5eAaImIp3nP	zamezovat
prostupu	prostup	k1gInSc3	prostup
většiny	většina	k1gFnSc2	většina
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
zakrývají	zakrývat	k5eAaImIp3nP	zakrývat
tak	tak	k6eAd1	tak
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
povrchové	povrchový	k2eAgInPc4d1	povrchový
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
zřetelný	zřetelný	k2eAgInSc4d1	zřetelný
od	od	k7c2	od
44	[number]	k4	44
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
tropopauzy	tropopauza	k1gFnSc2	tropopauza
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
změřen	změřen	k2eAgInSc1d1	změřen
tlak	tlak	k1gInSc1	tlak
11,5	[number]	k4	11,5
kPa	kPa	k?	kPa
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
-202,72	-202,72	k4	-202,72
°	°	k?	°
<g/>
C.	C.	kA	C.
Teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
94	[number]	k4	94
K	K	kA	K
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
179,2	[number]	k4	179,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
nízké	nízký	k2eAgFnSc6d1	nízká
teplotě	teplota	k1gFnSc6	teplota
má	mít	k5eAaImIp3nS	mít
vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
extrémně	extrémně	k6eAd1	extrémně
nízkou	nízký	k2eAgFnSc4d1	nízká
tenzi	tenze	k1gFnSc4	tenze
par	para	k1gFnPc2	para
a	a	k8xC	a
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgNnSc1d1	omezené
množství	množství	k1gNnSc1	množství
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
přijímá	přijímat	k5eAaImIp3nS	přijímat
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
%	%	kIx~	%
množství	množství	k1gNnSc6	množství
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
90	[number]	k4	90
%	%	kIx~	%
absorbováno	absorbován	k2eAgNnSc1d1	absorbováno
silnou	silný	k2eAgFnSc7d1	silná
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
;	;	kIx,	;
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Titanu	titan	k1gInSc2	titan
dopadá	dopadat	k5eAaImIp3nS	dopadat
jen	jen	k9	jen
asi	asi	k9	asi
0,1	[number]	k4	0,1
%	%	kIx~	%
světla	světlo	k1gNnSc2	světlo
oproti	oproti	k7c3	oproti
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
methan	methan	k1gInSc1	methan
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
něhož	jenž	k3xRgNnSc2	jenž
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc1d2	nižší
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
opar	opar	k1gInSc1	opar
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
antiskleníkový	antiskleníkový	k2eAgInSc1d1	antiskleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
sluneční	sluneční	k2eAgInPc4d1	sluneční
paprsky	paprsek	k1gInPc4	paprsek
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
účinky	účinek	k1gInPc4	účinek
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Mraky	mrak	k1gInPc1	mrak
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
methanem	methan	k1gInSc7	methan
<g/>
,	,	kIx,	,
ethanem	ethan	k1gInSc7	ethan
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
organickými	organický	k2eAgFnPc7d1	organická
sloučeninami	sloučenina	k1gFnPc7	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Mraky	mrak	k1gInPc1	mrak
jsou	být	k5eAaImIp3nP	být
rozptýlené	rozptýlený	k2eAgInPc1d1	rozptýlený
a	a	k8xC	a
různorodé	různorodý	k2eAgInPc1d1	různorodý
a	a	k8xC	a
narušují	narušovat	k5eAaImIp3nP	narušovat
jinak	jinak	k6eAd1	jinak
celistvý	celistvý	k2eAgInSc4d1	celistvý
opar	opar	k1gInSc4	opar
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nálezů	nález	k1gInPc2	nález
sondy	sonda	k1gFnSc2	sonda
Huygens	Huygensa	k1gFnPc2	Huygensa
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
pravidelně	pravidelně	k6eAd1	pravidelně
prší	pršet	k5eAaImIp3nS	pršet
kapalný	kapalný	k2eAgInSc4d1	kapalný
methan	methan	k1gInSc4	methan
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
podnebné	podnebný	k2eAgFnPc4d1	podnebná
podmínky	podmínka	k1gFnPc4	podmínka
ovlivněny	ovlivněn	k2eAgFnPc4d1	ovlivněna
ročními	roční	k2eAgNnPc7d1	roční
obdobími	období	k1gNnPc7	období
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
oběhu	oběh	k1gInSc2	oběh
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Slunce	slunce	k1gNnSc4	slunce
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
přibližně	přibližně	k6eAd1	přibližně
za	za	k7c4	za
29,5	[number]	k4	29,5
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
každé	každý	k3xTgNnSc1	každý
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
přes	přes	k7c4	přes
sedm	sedm	k4xCc4	sedm
pozemských	pozemský	k2eAgNnPc2d1	pozemské
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oblačnost	oblačnost	k1gFnSc1	oblačnost
většinou	většina	k1gFnSc7	většina
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
okolo	okolo	k7c2	okolo
1	[number]	k4	1
%	%	kIx~	%
povrchu	povrch	k1gInSc3	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
byly	být	k5eAaImAgFnP	být
pozorovány	pozorovat	k5eAaImNgFnP	pozorovat
i	i	k9	i
extrémní	extrémní	k2eAgFnPc1d1	extrémní
situace	situace	k1gFnPc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mraky	mrak	k1gInPc1	mrak
pokryly	pokrýt	k5eAaPmAgInP	pokrýt
okolo	okolo	k7c2	okolo
8	[number]	k4	8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
hypotézy	hypotéza	k1gFnSc2	hypotéza
se	se	k3xPyFc4	se
jižní	jižní	k2eAgInPc1d1	jižní
mraky	mrak	k1gInPc1	mrak
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
během	během	k7c2	během
letního	letní	k2eAgNnSc2d1	letní
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
když	když	k8xS	když
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
příjem	příjem	k1gInSc1	příjem
slunečních	sluneční	k2eAgInPc2d1	sluneční
paprsků	paprsek	k1gInPc2	paprsek
zapříčiní	zapříčinit	k5eAaPmIp3nS	zapříčinit
vznik	vznik	k1gInSc1	vznik
stoupavých	stoupavý	k2eAgInPc2d1	stoupavý
proudů	proud	k1gInPc2	proud
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
následkem	následek	k1gInSc7	následek
je	být	k5eAaImIp3nS	být
konvekce	konvekce	k1gFnSc1	konvekce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
však	však	k9	však
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvorba	tvorba	k1gFnSc1	tvorba
mraků	mrak	k1gInPc2	mrak
ethanu	ethan	k1gInSc2	ethan
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
nejen	nejen	k6eAd1	nejen
po	po	k7c6	po
jižním	jižní	k2eAgInSc6d1	jižní
letním	letní	k2eAgInSc6d1	letní
slunovratu	slunovrat	k1gInSc6	slunovrat
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
v	v	k7c6	v
období	období	k1gNnSc6	období
uprostřed	uprostřed	k7c2	uprostřed
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
methanová	methanový	k2eAgFnSc1d1	methanový
vlhkost	vlhkost	k1gFnSc1	vlhkost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
zřejmě	zřejmě	k6eAd1	zřejmě
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zvětšení	zvětšení	k1gNnSc3	zvětšení
oblaků	oblak	k1gInPc2	oblak
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
Titanu	titan	k1gInSc2	titan
bylo	být	k5eAaImAgNnS	být
letní	letní	k2eAgNnSc4d1	letní
období	období	k1gNnSc4	období
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Saturn	Saturn	k1gMnSc1	Saturn
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
přesunul	přesunout	k5eAaPmAgMnS	přesunout
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
začalo	začít	k5eAaPmAgNnS	začít
svítit	svítit	k5eAaImF	svítit
Slunce	slunce	k1gNnSc1	slunce
více	hodně	k6eAd2	hodně
na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
na	na	k7c6	na
severu	sever	k1gInSc6	sever
tedy	tedy	k9	tedy
nastalo	nastat	k5eAaPmAgNnS	nastat
jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
podzim	podzim	k1gInSc1	podzim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
období	období	k1gNnSc2	období
oblaka	oblaka	k1gNnPc1	oblaka
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
začnou	začít	k5eAaPmIp3nP	začít
kondenzovat	kondenzovat	k5eAaImF	kondenzovat
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
systému	systém	k1gInSc6	systém
Saturnu	Saturn	k1gInSc2	Saturn
letní	letní	k2eAgInSc1d1	letní
slunovrat	slunovrat	k1gInSc1	slunovrat
a	a	k8xC	a
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
následně	následně	k6eAd1	následně
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2017	[number]	k4	2017
pozorovala	pozorovat	k5eAaImAgNnP	pozorovat
světlá	světlý	k2eAgNnPc1d1	světlé
oblaka	oblaka	k1gNnPc1	oblaka
nad	nad	k7c7	nad
severními	severní	k2eAgNnPc7d1	severní
jezery	jezero	k1gNnPc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Titanu	titan	k1gInSc2	titan
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
jako	jako	k8xS	jako
rozmanitý	rozmanitý	k2eAgInSc1d1	rozmanitý
<g/>
,	,	kIx,	,
tekutinami	tekutina	k1gFnPc7	tekutina
erodovaný	erodovaný	k2eAgInSc1d1	erodovaný
a	a	k8xC	a
geologicky	geologicky	k6eAd1	geologicky
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Titan	titan	k1gInSc1	titan
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
nachází	nacházet	k5eAaImIp3nS	nacházet
již	již	k9	již
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc4	stáří
povrchu	povrch	k1gInSc2	povrch
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c6	na
rozmezí	rozmezí	k1gNnSc6	rozmezí
mezi	mezi	k7c7	mezi
100	[number]	k4	100
miliony	milion	k4xCgInPc7	milion
až	až	k8xS	až
miliardou	miliarda	k4xCgFnSc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Geologické	geologický	k2eAgInPc1d1	geologický
procesy	proces	k1gInPc1	proces
mohly	moct	k5eAaImAgInP	moct
povrch	povrch	k7c2wR	povrch
planety	planeta	k1gFnSc2	planeta
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Titanu	titan	k1gInSc2	titan
je	být	k5eAaImIp3nS	být
dvakrát	dvakrát	k6eAd1	dvakrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
zemská	zemský	k2eAgFnSc1d1	zemská
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
získat	získat	k5eAaPmF	získat
pomocí	pomocí	k7c2	pomocí
astronomických	astronomický	k2eAgInPc2d1	astronomický
přístrojů	přístroj	k1gInPc2	přístroj
mapu	mapa	k1gFnSc4	mapa
povrchu	povrch	k1gInSc2	povrch
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
infračervených	infračervený	k2eAgInPc2d1	infračervený
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
radarového	radarový	k2eAgInSc2d1	radarový
výškoměru	výškoměr	k1gInSc2	výškoměr
a	a	k8xC	a
radaru	radar	k1gInSc2	radar
se	s	k7c7	s
syntetickou	syntetický	k2eAgFnSc7d1	syntetická
aperturou	apertura	k1gFnSc7	apertura
(	(	kIx(	(
<g/>
SAR	SAR	kA	SAR
<g/>
)	)	kIx)	)
nafotit	nafotit	k5eAaPmF	nafotit
mapy	mapa	k1gFnSc2	mapa
částí	část	k1gFnPc2	část
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
když	když	k8xS	když
zrovna	zrovna	k6eAd1	zrovna
prolétala	prolétat	k5eAaPmAgFnS	prolétat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
obrázky	obrázek	k1gInPc1	obrázek
ukázaly	ukázat	k5eAaPmAgInP	ukázat
rozličnou	rozličný	k2eAgFnSc4d1	rozličná
geologii	geologie	k1gFnSc4	geologie
s	s	k7c7	s
nerovnými	rovný	k2eNgFnPc7d1	nerovná
i	i	k8xC	i
velmi	velmi	k6eAd1	velmi
hladkými	hladký	k2eAgFnPc7d1	hladká
plochami	plocha	k1gFnPc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
valila	valit	k5eAaImAgFnS	valit
voda	voda	k1gFnSc1	voda
s	s	k7c7	s
amoniakem	amoniak	k1gInSc7	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
byly	být	k5eAaImAgFnP	být
měřením	měření	k1gNnSc7	měření
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
objeveny	objevit	k5eAaPmNgInP	objevit
i	i	k9	i
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
ledová	ledový	k2eAgFnSc1d1	ledová
vrstva	vrstva	k1gFnSc1	vrstva
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc3d1	nízká
geologické	geologický	k2eAgFnSc3d1	geologická
aktivitě	aktivita	k1gFnSc3	aktivita
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Objeveny	objeven	k2eAgInPc1d1	objeven
byly	být	k5eAaImAgInP	být
i	i	k9	i
pruhovité	pruhovitý	k2eAgInPc1d1	pruhovitý
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
stovky	stovka	k1gFnPc1	stovka
kilometrů	kilometr	k1gInPc2	kilometr
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
zřejmě	zřejmě	k6eAd1	zřejmě
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
větrnou	větrný	k2eAgFnSc7d1	větrná
erozí	eroze	k1gFnSc7	eroze
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
zdrsnělých	zdrsnělý	k2eAgInPc2d1	zdrsnělý
útvarů	útvar	k1gInPc2	útvar
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
erozí	eroze	k1gFnPc2	eroze
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc1	povrch
Titanu	titan	k1gInSc2	titan
hladký	hladký	k2eAgMnSc1d1	hladký
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
impaktní	impaktní	k2eAgInPc1d1	impaktní
krátery	kráter	k1gInPc1	kráter
byly	být	k5eAaImAgInP	být
povětšinou	povětšinou	k6eAd1	povětšinou
zaplněny	zaplněn	k2eAgInPc4d1	zaplněn
kapalnými	kapalný	k2eAgInPc7d1	kapalný
uhlovodíky	uhlovodík	k1gInPc7	uhlovodík
ze	z	k7c2	z
srážek	srážka	k1gFnPc2	srážka
či	či	k8xC	či
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Radarový	radarový	k2eAgInSc1d1	radarový
výškoměr	výškoměr	k1gInSc1	výškoměr
naznačil	naznačit	k5eAaPmAgInS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
Titanu	titan	k1gInSc2	titan
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
rovinatý	rovinatý	k2eAgMnSc1d1	rovinatý
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
výkyvy	výkyv	k1gInPc1	výkyv
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
povrchu	povrch	k1gInSc2	povrch
nepřesahují	přesahovat	k5eNaImIp3nP	přesahovat
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
též	též	k9	též
výkyvy	výkyv	k1gInPc4	výkyv
nad	nad	k7c4	nad
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
pohoří	pohořet	k5eAaPmIp3nS	pohořet
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hory	hora	k1gFnPc1	hora
mívají	mívat	k5eAaImIp3nP	mívat
vrcholy	vrchol	k1gInPc4	vrchol
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
od	od	k7c2	od
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
až	až	k6eAd1	až
nad	nad	k7c4	nad
jeden	jeden	k4xCgInSc4	jeden
kilometr	kilometr	k1gInSc4	kilometr
nad	nad	k7c4	nad
okolní	okolní	k2eAgFnSc4d1	okolní
krajinu	krajina	k1gFnSc4	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Titanu	titan	k1gInSc2	titan
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
širokými	široký	k2eAgFnPc7d1	široká
oblastmi	oblast	k1gFnPc7	oblast
světlého	světlý	k2eAgInSc2d1	světlý
a	a	k8xC	a
tmavého	tmavý	k2eAgInSc2d1	tmavý
terénu	terén	k1gInSc2	terén
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
světlé	světlý	k2eAgFnPc4d1	světlá
oblasti	oblast	k1gFnPc4	oblast
patří	patřit	k5eAaImIp3nS	patřit
Xanadu	Xanada	k1gFnSc4	Xanada
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
odrazivá	odrazivý	k2eAgFnSc1d1	odrazivá
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
identifikována	identifikován	k2eAgFnSc1d1	identifikována
na	na	k7c6	na
infračervených	infračervený	k2eAgInPc6d1	infračervený
snímcích	snímek	k1gInPc6	snímek
z	z	k7c2	z
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
teleskopu	teleskop	k1gInSc2	teleskop
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc5d1	Cassin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
členité	členitý	k2eAgFnSc6d1	členitá
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
kopce	kopec	k1gInPc1	kopec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
odděleny	oddělit	k5eAaPmNgInP	oddělit
údolími	údolí	k1gNnPc7	údolí
a	a	k8xC	a
propastmi	propast	k1gFnPc7	propast
<g/>
.	.	kIx.	.
</s>
<s>
Xanadu	Xanada	k1gFnSc4	Xanada
křižují	křižovat	k5eAaImIp3nP	křižovat
tmavé	tmavý	k2eAgInPc1d1	tmavý
čárovité	čárovitý	k2eAgInPc1d1	čárovitý
topografické	topografický	k2eAgInPc1d1	topografický
útvary	útvar	k1gInPc1	útvar
–	–	k?	–
hřebeny	hřeben	k1gInPc4	hřeben
a	a	k8xC	a
trhliny	trhlina	k1gFnPc4	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
důsledkem	důsledek	k1gInSc7	důsledek
tektonické	tektonický	k2eAgFnPc4d1	tektonická
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblast	oblast	k1gFnSc1	oblast
Xanadu	Xanad	k1gInSc2	Xanad
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgNnSc2d1	jiné
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
mohly	moct	k5eAaImAgInP	moct
tyto	tento	k3xDgFnPc4	tento
tmavé	tmavý	k2eAgFnPc4d1	tmavá
oblasti	oblast	k1gFnPc4	oblast
vzniknout	vzniknout	k5eAaPmF	vzniknout
působením	působení	k1gNnSc7	působení
proudu	proud	k1gInSc2	proud
kapaliny	kapalina	k1gFnSc2	kapalina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rozdrásala	rozdrásat	k5eAaPmAgFnS	rozdrásat
starý	starý	k2eAgInSc4d1	starý
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
byly	být	k5eAaImAgFnP	být
také	také	k9	také
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
tmavé	tmavý	k2eAgFnPc1d1	tmavá
plochy	plocha	k1gFnPc1	plocha
podobné	podobný	k2eAgFnSc2d1	podobná
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ligeia	Ligeia	k1gFnSc1	Ligeia
Mare	Mar	k1gFnSc2	Mar
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
tmavá	tmavý	k2eAgFnSc1d1	tmavá
oblast	oblast	k1gFnSc1	oblast
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
moře	moře	k1gNnSc1	moře
čistého	čistý	k2eAgInSc2d1	čistý
kapalného	kapalný	k2eAgInSc2d1	kapalný
methanu	methan	k1gInSc2	methan
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
nacházejí	nacházet	k5eAaImIp3nP	nacházet
uhlovodíková	uhlovodíkový	k2eAgNnPc1d1	uhlovodíkové
moře	moře	k1gNnPc1	moře
<g/>
,	,	kIx,	,
přinesla	přinést	k5eAaPmAgNnP	přinést
již	již	k9	již
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pořídily	pořídit	k5eAaPmAgFnP	pořídit
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc4	Voyager
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgNnPc2	tento
dat	datum	k1gNnPc2	datum
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
přibližně	přibližně	k6eAd1	přibližně
správná	správný	k2eAgFnSc1d1	správná
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
složení	složení	k1gNnSc1	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
přímé	přímý	k2eAgInPc1d1	přímý
důkazy	důkaz	k1gInPc1	důkaz
však	však	k9	však
chyběly	chybět	k5eAaImAgInP	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
až	až	k6eAd1	až
data	datum	k1gNnPc1	datum
z	z	k7c2	z
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
teleskopu	teleskop	k1gInSc2	teleskop
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
pozorování	pozorování	k1gNnPc2	pozorování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vyplývala	vyplývat	k5eAaImAgFnS	vyplývat
přítomnost	přítomnost	k1gFnSc1	přítomnost
kapalného	kapalný	k2eAgInSc2d1	kapalný
methanu	methan	k1gInSc2	methan
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
již	již	k9	již
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
oddělených	oddělený	k2eAgFnPc6d1	oddělená
nádržích	nádrž	k1gFnPc6	nádrž
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
oceánech	oceán	k1gInPc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
tuto	tento	k3xDgFnSc4	tento
hypotézu	hypotéza	k1gFnSc4	hypotéza
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
misi	mise	k1gFnSc6	mise
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
doletěla	doletět	k5eAaPmAgFnS	doletět
k	k	k7c3	k
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
očekávalo	očekávat	k5eAaImAgNnS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zachytí	zachytit	k5eAaPmIp3nP	zachytit
sluneční	sluneční	k2eAgInPc1d1	sluneční
paprsky	paprsek	k1gInPc1	paprsek
odražené	odražený	k2eAgInPc1d1	odražený
od	od	k7c2	od
uhlovodíkových	uhlovodíkový	k2eAgNnPc2d1	uhlovodíkové
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zpočátku	zpočátku	k6eAd1	zpočátku
žádné	žádný	k3yNgInPc4	žádný
takovéto	takovýto	k3xDgInPc4	takovýto
přímé	přímý	k2eAgInPc4d1	přímý
odrazy	odraz	k1gInPc4	odraz
pozorovány	pozorován	k2eAgFnPc1d1	pozorována
nebyly	být	k5eNaImAgFnP	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Titanova	Titanův	k2eAgInSc2d1	Titanův
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
záhadný	záhadný	k2eAgInSc1d1	záhadný
tmavý	tmavý	k2eAgInSc1d1	tmavý
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
nazván	nazvat	k5eAaPmNgInS	nazvat
Ontario	Ontario	k1gNnSc4	Ontario
Lacus	Lacus	k1gInSc4	Lacus
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jezero	jezero	k1gNnSc4	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Radarovým	radarový	k2eAgNnSc7d1	radarové
mapováním	mapování	k1gNnSc7	mapování
byly	být	k5eAaImAgFnP	být
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
i	i	k8xC	i
možné	možný	k2eAgFnPc1d1	možná
pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
linie	linie	k1gFnPc1	linie
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
radarem	radar	k1gInSc7	radar
zmapována	zmapován	k2eAgFnSc1d1	zmapována
severní	severní	k2eAgFnSc1d1	severní
polokoule	polokoule	k1gFnSc1	polokoule
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ocitala	ocitat	k5eAaImAgFnS	ocitat
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
několik	několik	k4yIc1	několik
rozlehlých	rozlehlý	k2eAgInPc2d1	rozlehlý
hladkých	hladký	k2eAgInPc2d1	hladký
terénních	terénní	k2eAgInPc2d1	terénní
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
velmi	velmi	k6eAd1	velmi
malými	malý	k2eAgFnPc7d1	malá
a	a	k8xC	a
homogenními	homogenní	k2eAgFnPc7d1	homogenní
odezvami	odezva	k1gFnPc7	odezva
na	na	k7c4	na
radarový	radarový	k2eAgInSc4d1	radarový
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
na	na	k7c6	na
radaru	radar	k1gInSc6	radar
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tmavých	tmavý	k2eAgFnPc2d1	tmavá
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
blízké	blízký	k2eAgFnSc6d1	blízká
severnímu	severní	k2eAgMnSc3d1	severní
pólu	pól	k1gInSc3	pól
kropenatý	kropenatý	k2eAgInSc1d1	kropenatý
vzor	vzor	k1gInSc1	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnPc2	pozorování
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2007	[number]	k4	2007
vědci	vědec	k1gMnPc1	vědec
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
získány	získán	k2eAgInPc1d1	získán
"	"	kIx"	"
<g/>
nezvratné	zvratný	k2eNgInPc1d1	nezvratný
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
jezerech	jezero	k1gNnPc6	jezero
naplněných	naplněný	k2eAgFnPc2d1	naplněná
kapalným	kapalný	k2eAgInSc7d1	kapalný
methanem	methan	k1gInSc7	methan
na	na	k7c6	na
Saturnově	Saturnův	k2eAgInSc6d1	Saturnův
měsíci	měsíc	k1gInSc6	měsíc
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tým	tým	k1gInSc1	tým
okolo	okolo	k7c2	okolo
mise	mise	k1gFnSc2	mise
Cassini-Huygens	Cassini-Huygens	k1gInSc1	Cassini-Huygens
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyobrazené	vyobrazený	k2eAgFnPc1d1	vyobrazená
plochy	plocha	k1gFnPc1	plocha
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
určitě	určitě	k6eAd1	určitě
dlouho	dlouho	k6eAd1	dlouho
hledaná	hledaný	k2eAgNnPc1d1	hledané
uhlovodíková	uhlovodíkový	k2eAgNnPc1d1	uhlovodíkové
jezera	jezero	k1gNnPc1	jezero
<g/>
,	,	kIx,	,
první	první	k4xOgInPc4	první
stabilní	stabilní	k2eAgInPc4d1	stabilní
kapalné	kapalný	k2eAgInPc4d1	kapalný
útvary	útvar	k1gInPc4	útvar
nalezené	nalezený	k2eAgInPc4d1	nalezený
mimo	mimo	k7c4	mimo
planetu	planeta	k1gFnSc4	planeta
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
tato	tento	k3xDgNnPc1	tento
jezera	jezero	k1gNnPc4	jezero
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
proláklinách	proláklina	k1gFnPc6	proláklina
a	a	k8xC	a
vypadá	vypadat	k5eAaPmIp3nS	vypadat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
napájena	napájen	k2eAgFnSc1d1	napájena
methanovými	methanův	k2eAgFnPc7d1	methanův
řekami	řeka	k1gFnPc7	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
po	po	k7c6	po
erozi	eroze	k1gFnSc6	eroze
kapalinou	kapalina	k1gFnSc7	kapalina
se	se	k3xPyFc4	se
jevily	jevit	k5eAaImAgFnP	jevit
vytvořeny	vytvořen	k2eAgInPc1d1	vytvořen
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
eroze	eroze	k1gFnSc2	eroze
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
kanálech	kanál	k1gInPc6	kanál
byl	být	k5eAaImAgInS	být
překvapivě	překvapivě	k6eAd1	překvapivě
velice	velice	k6eAd1	velice
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
buď	buď	k8xC	buď
je	být	k5eAaImIp3nS	být
eroze	eroze	k1gFnSc1	eroze
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
velice	velice	k6eAd1	velice
pomalá	pomalý	k2eAgFnSc1d1	pomalá
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
byla	být	k5eAaImAgNnP	být
starší	starý	k2eAgNnPc1d2	starší
říční	říční	k2eAgNnPc1d1	říční
koryta	koryto	k1gNnPc1	koryto
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
jiným	jiný	k2eAgInSc7d1	jiný
přírodním	přírodní	k2eAgInSc7d1	přírodní
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
provedla	provést	k5eAaPmAgFnS	provést
<g/>
,	,	kIx,	,
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezera	jezero	k1gNnSc2	jezero
zabírají	zabírat	k5eAaImIp3nP	zabírat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
procent	procento	k1gNnPc2	procento
povrchu	povrch	k1gInSc2	povrch
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
sušší	suchý	k2eAgNnSc4d2	sušší
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jezer	jezero	k1gNnPc2	jezero
je	být	k5eAaImIp3nS	být
koncentrována	koncentrovat	k5eAaBmNgFnS	koncentrovat
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
pólů	pól	k1gInPc2	pól
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
nižší	nízký	k2eAgInSc1d2	nižší
příjem	příjem	k1gInSc1	příjem
slunečních	sluneční	k2eAgInPc2d1	sluneční
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
menší	malý	k2eAgInSc1d2	menší
výpar	výpar	k1gInSc1	výpar
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
však	však	k9	však
objevena	objevit	k5eAaPmNgFnS	objevit
i	i	k8xC	i
moře	moře	k1gNnSc1	moře
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
pouštních	pouštní	k2eAgFnPc6d1	pouštní
oblastech	oblast	k1gFnPc6	oblast
u	u	k7c2	u
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jednoho	jeden	k4xCgNnSc2	jeden
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
místa	místo	k1gNnSc2	místo
přistání	přistání	k1gNnSc2	přistání
modulu	modul	k1gInSc2	modul
Huygens	Huygensa	k1gFnPc2	Huygensa
v	v	k7c6	v
regionu	region	k1gInSc6	region
Shangri-La	Shangri-L	k1gInSc2	Shangri-L
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
2400	[number]	k4	2400
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
hloubce	hloubka	k1gFnSc6	hloubka
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Pouštní	pouštní	k2eAgNnPc1d1	pouštní
jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
obdobou	obdoba	k1gFnSc7	obdoba
oáz	oáza	k1gFnPc2	oáza
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
napájena	napájen	k2eAgFnSc1d1	napájena
z	z	k7c2	z
podzemních	podzemní	k2eAgFnPc2d1	podzemní
zásobáren	zásobárna	k1gFnPc2	zásobárna
methanu	methan	k1gInSc2	methan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
vizuální	vizuální	k2eAgMnSc1d1	vizuální
a	a	k8xC	a
infračervený	infračervený	k2eAgInSc1d1	infračervený
spektrometr	spektrometr	k1gInSc1	spektrometr
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Cassini	Cassin	k2eAgMnPc1d1	Cassin
bezpochybný	bezpochybný	k2eAgInSc1d1	bezpochybný
výskyt	výskyt	k1gInSc4	výskyt
kapalného	kapalný	k2eAgInSc2d1	kapalný
ethanu	ethan	k1gInSc2	ethan
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ontario	Ontario	k1gNnSc1	Ontario
Lacus	Lacus	k1gInSc1	Lacus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
proletěla	proletět	k5eAaPmAgFnS	proletět
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
jezerem	jezero	k1gNnSc7	jezero
a	a	k8xC	a
spatřila	spatřit	k5eAaPmAgFnS	spatřit
přímý	přímý	k2eAgInSc4d1	přímý
odraz	odraz	k1gInSc4	odraz
paprsků	paprsek	k1gInPc2	paprsek
na	na	k7c6	na
radaru	radar	k1gInSc6	radar
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
signálu	signál	k1gInSc2	signál
odražených	odražený	k2eAgInPc2d1	odražený
paprsků	paprsek	k1gInPc2	paprsek
saturovala	saturovat	k5eAaBmAgFnS	saturovat
přijímač	přijímač	k1gInSc4	přijímač
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hladina	hladina	k1gFnSc1	hladina
nevlnila	vlnit	k5eNaImAgFnS	vlnit
o	o	k7c4	o
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
3	[number]	k4	3
milimetry	milimetr	k1gInPc7	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
bezvětří	bezvětří	k1gNnSc2	bezvětří
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
kapalina	kapalina	k1gFnSc1	kapalina
v	v	k7c6	v
jezerech	jezero	k1gNnPc6	jezero
značně	značně	k6eAd1	značně
viskózní	viskózní	k2eAgFnSc1d1	viskózní
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgInPc1d1	přímý
odrazy	odraz	k1gInPc1	odraz
jsou	být	k5eAaImIp3nP	být
důkazem	důkaz	k1gInSc7	důkaz
pro	pro	k7c4	pro
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
,	,	kIx,	,
zrcadlící	zrcadlící	k2eAgInSc4d1	zrcadlící
se	se	k3xPyFc4	se
terén	terén	k1gInSc4	terén
a	a	k8xC	a
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
tak	tak	k9	tak
domněnku	domněnka	k1gFnSc4	domněnka
rozlehlých	rozlehlý	k2eAgFnPc2d1	rozlehlá
kapalných	kapalný	k2eAgFnPc2d1	kapalná
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
předtím	předtím	k6eAd1	předtím
vycházela	vycházet	k5eAaImAgFnS	vycházet
jen	jen	k9	jen
z	z	k7c2	z
radarových	radarový	k2eAgNnPc2d1	radarové
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
tato	tento	k3xDgNnPc1	tento
měření	měření	k1gNnPc1	měření
proběhla	proběhnout	k5eAaPmAgNnP	proběhnout
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
po	po	k7c6	po
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc4	oblast
zalila	zalít	k5eAaPmAgFnS	zalít
slunečními	sluneční	k2eAgInPc7d1	sluneční
paprsky	paprsek	k1gInPc7	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
takto	takto	k6eAd1	takto
potvrzených	potvrzený	k2eAgNnPc2d1	potvrzené
jezer	jezero	k1gNnPc2	jezero
bylo	být	k5eAaImAgNnS	být
Jingpo	Jingpa	k1gFnSc5	Jingpa
Lacus	Lacus	k1gInSc1	Lacus
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
radarová	radarový	k2eAgNnPc1d1	radarové
měření	měření	k1gNnPc1	měření
hloubky	hloubka	k1gFnSc2	hloubka
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2009	[number]	k4	2009
a	a	k8xC	a
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ontario	Ontario	k1gNnSc1	Ontario
Lacus	Lacus	k1gMnSc1	Lacus
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
mělké	mělký	k2eAgNnSc1d1	mělké
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
naměřená	naměřený	k2eAgFnSc1d1	naměřená
hloubka	hloubka	k1gFnSc1	hloubka
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
0,4	[number]	k4	0,4
do	do	k7c2	do
3	[number]	k4	3
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
nejhlubší	hluboký	k2eAgNnPc4d3	nejhlubší
místa	místo	k1gNnPc4	místo
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
jezero	jezero	k1gNnSc1	jezero
Ligeia	Ligeius	k1gMnSc2	Ligeius
Mare	Mar	k1gMnSc2	Mar
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
pólu	pól	k1gInSc6	pól
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
dle	dle	k7c2	dle
prvních	první	k4xOgNnPc2	první
měření	měření	k1gNnPc2	měření
hloubku	hloubka	k1gFnSc4	hloubka
přes	přes	k7c4	přes
osm	osm	k4xCc4	osm
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
maximální	maximální	k2eAgFnSc1d1	maximální
hloubka	hloubka	k1gFnSc1	hloubka
rozpoznatelná	rozpoznatelný	k2eAgFnSc1d1	rozpoznatelná
tehdejšími	tehdejší	k2eAgInPc7d1	tehdejší
technickými	technický	k2eAgInPc7d1	technický
a	a	k8xC	a
analytickými	analytický	k2eAgInPc7d1	analytický
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
pořízena	pořídit	k5eAaPmNgFnS	pořídit
radarovými	radarový	k2eAgInPc7d1	radarový
odrazy	odraz	k1gInPc7	odraz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ligeia	Ligeia	k1gFnSc1	Ligeia
Mare	Mar	k1gFnSc2	Mar
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hloubky	hloubka	k1gFnSc2	hloubka
160	[number]	k4	160
–	–	k?	–
170	[number]	k4	170
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
jezerem	jezero	k1gNnSc7	jezero
Titanu	titan	k1gInSc2	titan
je	být	k5eAaImIp3nS	být
Kraken	Kraken	k2eAgInSc1d1	Kraken
Mare	Mare	k1gInSc1	Mare
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
přes	přes	k7c4	přes
400	[number]	k4	400
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgNnSc2	tento
jezera	jezero	k1gNnSc2	jezero
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
danou	daný	k2eAgFnSc7d1	daná
metodou	metoda	k1gFnSc7	metoda
radarových	radarový	k2eAgInPc2d1	radarový
odrazů	odraz	k1gInPc2	odraz
zachytit	zachytit	k5eAaPmF	zachytit
dno	dno	k1gNnSc4	dno
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
m	m	kA	m
hluboké	hluboký	k2eAgNnSc1d1	hluboké
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
možným	možný	k2eAgNnSc7d1	možné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
kapalina	kapalina	k1gFnSc1	kapalina
v	v	k7c6	v
Kraken	Krakna	k1gFnPc2	Krakna
Mare	Mar	k1gInSc2	Mar
absorbovala	absorbovat	k5eAaBmAgFnS	absorbovat
záření	záření	k1gNnSc4	záření
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
Ligeia	Ligeius	k1gMnSc2	Ligeius
Mare	Mare	k1gFnSc6	Mare
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
byly	být	k5eAaImAgInP	být
v	v	k7c4	v
Kraken	Kraken	k1gInSc4	Kraken
Mare	Mar	k1gInSc2	Mar
pozorovány	pozorován	k2eAgFnPc4d1	pozorována
mělčí	mělký	k2eAgFnPc4d2	mělčí
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
rozmezím	rozmezí	k1gNnSc7	rozmezí
hloubek	hloubka	k1gFnPc2	hloubka
od	od	k7c2	od
20	[number]	k4	20
do	do	k7c2	do
35	[number]	k4	35
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
přeletu	přelet	k1gInSc2	přelet
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
objevila	objevit	k5eAaPmAgFnS	objevit
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
pólu	pól	k1gInSc6	pól
přírodní	přírodní	k2eAgInSc4d1	přírodní
útvar	útvar	k1gInSc4	útvar
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
400	[number]	k4	400
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
řekou	řeka	k1gFnSc7	řeka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Ligeia	Ligeium	k1gNnSc2	Ligeium
Mare	Mar	k1gFnSc2	Mar
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
identifikována	identifikován	k2eAgFnSc1d1	identifikována
celá	celý	k2eAgFnSc1d1	celá
síť	síť	k1gFnSc1	síť
říčních	říční	k2eAgInPc2d1	říční
kanálů	kanál	k1gInPc2	kanál
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
Vid	vid	k1gInSc4	vid
Flumina	Flumin	k2eAgFnSc1d1	Flumin
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
analýzy	analýza	k1gFnSc2	analýza
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
radarového	radarový	k2eAgInSc2d1	radarový
výškoměru	výškoměr	k1gInSc2	výškoměr
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
řeky	řeka	k1gFnPc1	řeka
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
hlubokých	hluboký	k2eAgInPc6d1	hluboký
sevřených	sevřený	k2eAgInPc6d1	sevřený
kaňonech	kaňon	k1gInPc6	kaňon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
hluboké	hluboký	k2eAgInPc1d1	hluboký
až	až	k9	až
570	[number]	k4	570
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
kilometr	kilometr	k1gInSc1	kilometr
široké	široký	k2eAgFnSc2d1	široká
a	a	k8xC	a
se	s	k7c7	s
svahy	svah	k1gInPc7	svah
skloněnými	skloněný	k2eAgInPc7d1	skloněný
až	až	k6eAd1	až
do	do	k7c2	do
40	[number]	k4	40
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgInPc1d1	silný
přímé	přímý	k2eAgInPc1d1	přímý
odrazy	odraz	k1gInPc1	odraz
naznačily	naznačit	k5eAaPmAgInP	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
kaňony	kaňon	k1gInPc1	kaňon
plné	plný	k2eAgFnSc2d1	plná
kapaliny	kapalina	k1gFnSc2	kapalina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hladina	hladina	k1gFnSc1	hladina
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
výšce	výška	k1gFnSc6	výška
jako	jako	k9	jako
hladina	hladina	k1gFnSc1	hladina
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
představě	představa	k1gFnSc3	představa
zapuštěných	zapuštěný	k2eAgNnPc2d1	zapuštěné
říčních	říční	k2eAgNnPc2d1	říční
údolí	údolí	k1gNnPc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Odrazy	odraz	k1gInPc1	odraz
od	od	k7c2	od
hladiny	hladina	k1gFnSc2	hladina
byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
i	i	k9	i
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
výškách	výška	k1gFnPc6	výška
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
hladiny	hladina	k1gFnSc2	hladina
Liegia	Liegius	k1gMnSc2	Liegius
Mare	Mar	k1gMnSc2	Mar
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
toky	tok	k1gInPc4	tok
nižšího	nízký	k2eAgInSc2d2	nižší
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
napájí	napájet	k5eAaImIp3nP	napájet
primární	primární	k2eAgFnSc4d1	primární
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
představě	představa	k1gFnSc3	představa
rozsáhlejšího	rozsáhlý	k2eAgNnSc2d2	rozsáhlejší
úmoří	úmoří	k1gNnSc2	úmoří
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
eroze	eroze	k1gFnSc2	eroze
těchto	tento	k3xDgInPc2	tento
útvarů	útvar	k1gInPc2	útvar
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
Grand	grand	k1gMnSc1	grand
Canyon	Canyon	k1gMnSc1	Canyon
při	při	k7c6	při
zdvihu	zdvih	k1gInSc6	zdvih
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
nebo	nebo	k8xC	nebo
při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kombinací	kombinace	k1gFnSc7	kombinace
obou	dva	k4xCgInPc2	dva
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2006	[number]	k4	2006
a	a	k8xC	a
2017	[number]	k4	2017
nasbírala	nasbírat	k5eAaPmAgFnS	nasbírat
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
radiometrická	radiometrický	k2eAgNnPc1d1	radiometrické
a	a	k8xC	a
optická	optický	k2eAgNnPc1d1	optické
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
tvarové	tvarový	k2eAgFnSc2d1	tvarová
proměny	proměna	k1gFnSc2	proměna
na	na	k7c6	na
měsíčním	měsíční	k2eAgInSc6d1	měsíční
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
hustoty	hustota	k1gFnSc2	hustota
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Titan	titan	k1gInSc1	titan
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
60	[number]	k4	60
%	%	kIx~	%
z	z	k7c2	z
kamenného	kamenný	k2eAgInSc2d1	kamenný
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
40	[number]	k4	40
%	%	kIx~	%
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
dat	datum	k1gNnPc2	datum
tvarových	tvarový	k2eAgFnPc2d1	tvarová
změn	změna	k1gFnPc2	změna
naznačila	naznačit	k5eAaPmAgFnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
povrch	povrch	k7c2wR	povrch
měsíce	měsíc	k1gInSc2	měsíc
během	během	k7c2	během
každého	každý	k3xTgInSc2	každý
oběhu	oběh	k1gInSc2	oběh
kolem	kolem	k7c2	kolem
Saturnu	Saturn	k1gInSc2	Saturn
periodicky	periodicky	k6eAd1	periodicky
zvedá	zvedat	k5eAaImIp3nS	zvedat
a	a	k8xC	a
zase	zase	k9	zase
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
o	o	k7c4	o
deset	deset	k4xCc4	deset
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
odchylek	odchylka	k1gFnPc2	odchylka
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřek	vnitřek	k1gInSc1	vnitřek
měsíce	měsíc	k1gInSc2	měsíc
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nějakým	nějaký	k3yIgMnSc7	nějaký
způsoben	způsoben	k2eAgInSc4d1	způsoben
deformovatelný	deformovatelný	k2eAgInSc4d1	deformovatelný
a	a	k8xC	a
nejpravděpodobnějším	pravděpodobný	k2eAgNnSc7d3	nejpravděpodobnější
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
silná	silný	k2eAgFnSc1d1	silná
ledová	ledový	k2eAgFnSc1d1	ledová
měsíční	měsíční	k2eAgFnSc1d1	měsíční
kůra	kůra	k1gFnSc1	kůra
plave	plavat	k5eAaImIp3nS	plavat
na	na	k7c6	na
podpovrchovém	podpovrchový	k2eAgInSc6d1	podpovrchový
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
týmu	tým	k1gInSc2	tým
vědců	vědec	k1gMnPc2	vědec
mise	mise	k1gFnSc2	mise
Cassini	Cassin	k2eAgMnPc1d1	Cassin
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
oceán	oceán	k1gInSc1	oceán
neleží	ležet	k5eNaImIp3nS	ležet
než	než	k8xS	než
jak	jak	k6eAd1	jak
100	[number]	k4	100
km	km	kA	km
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
hloubka	hloubka	k1gFnSc1	hloubka
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
až	až	k9	až
na	na	k7c4	na
300	[number]	k4	300
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
oceánech	oceán	k1gInPc6	oceán
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
slanější	slaný	k2eAgFnSc1d2	slanější
než	než	k8xS	než
v	v	k7c6	v
Mrtvém	mrtvý	k2eAgNnSc6d1	mrtvé
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
vědci	vědec	k1gMnPc1	vědec
NASA	NASA	kA	NASA
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
studie	studie	k1gFnPc4	studie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
by	by	kYmCp3nS	by
methanový	methanový	k2eAgInSc1d1	methanový
déšť	déšť	k1gInSc1	déšť
mohl	moct	k5eAaImAgInS	moct
reagovat	reagovat	k5eAaBmF	reagovat
s	s	k7c7	s
podpovrchovou	podpovrchový	k2eAgFnSc7d1	podpovrchová
vrstvou	vrstva	k1gFnSc7	vrstva
ledových	ledový	k2eAgInPc2d1	ledový
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
"	"	kIx"	"
<g/>
alkanofery	alkanofera	k1gFnPc1	alkanofera
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
reakcích	reakce	k1gFnPc6	reakce
by	by	k9	by
vznikal	vznikat	k5eAaImAgInS	vznikat
ethan	ethan	k1gInSc1	ethan
nebo	nebo	k8xC	nebo
propan	propan	k1gInSc1	propan
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
následně	následně	k6eAd1	následně
napájel	napájet	k5eAaImAgMnS	napájet
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
z	z	k7c2	z
mapování	mapování	k1gNnSc2	mapování
<g/>
,	,	kIx,	,
měření	měření	k1gNnSc2	měření
SAR	SAR	kA	SAR
a	a	k8xC	a
radarem	radar	k1gInSc7	radar
pořízená	pořízený	k2eAgFnSc1d1	pořízená
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
odhalila	odhalit	k5eAaPmAgFnS	odhalit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
několik	několik	k4yIc4	několik
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
stářím	stáří	k1gNnSc7	stáří
měsíce	měsíc	k1gInSc2	měsíc
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
impakty	impakt	k1gInPc4	impakt
mladé	mladý	k2eAgInPc4d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nalezených	nalezený	k2eAgInPc2d1	nalezený
kráterů	kráter	k1gInPc2	kráter
je	být	k5eAaImIp3nS	být
440	[number]	k4	440
km	km	kA	km
široký	široký	k2eAgInSc1d1	široký
Menrva	Menrvo	k1gNnSc2	Menrvo
s	s	k7c7	s
dvojčetnou	dvojčetný	k2eAgFnSc7d1	dvojčetná
prohlubní	prohlubeň	k1gFnSc7	prohlubeň
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
menší	malý	k2eAgInPc1d2	menší
krátery	kráter	k1gInPc1	kráter
jsou	být	k5eAaImIp3nP	být
Sinlap	Sinlap	k1gInSc4	Sinlap
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc1d1	široký
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
60	[number]	k4	60
km	km	kA	km
a	a	k8xC	a
rovinatý	rovinatý	k2eAgMnSc1d1	rovinatý
<g/>
,	,	kIx,	,
a	a	k8xC	a
30	[number]	k4	30
km	km	kA	km
široký	široký	k2eAgInSc4d1	široký
kráter	kráter	k1gInSc4	kráter
Ksa	Ksa	k1gFnSc2	Ksa
se	s	k7c7	s
středovou	středový	k2eAgFnSc7d1	středová
vyvýšeninou	vyvýšenina	k1gFnSc7	vyvýšenina
<g/>
.	.	kIx.	.
</s>
<s>
Radarová	radarový	k2eAgNnPc1d1	radarové
měření	měření	k1gNnPc1	měření
a	a	k8xC	a
fotografie	fotografia	k1gFnPc1	fotografia
pořízené	pořízený	k2eAgFnPc1d1	pořízená
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
objevily	objevit	k5eAaPmAgInP	objevit
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chybí	chybět	k5eAaImIp3nS	chybět
zde	zde	k6eAd1	zde
specifické	specifický	k2eAgInPc4d1	specifický
útvary	útvar	k1gInPc4	útvar
pro	pro	k7c4	pro
jistou	jistý	k2eAgFnSc4d1	jistá
identifikaci	identifikace	k1gFnSc4	identifikace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
90	[number]	k4	90
km	km	kA	km
široký	široký	k2eAgInSc4d1	široký
prstenec	prstenec	k1gInSc4	prstenec
zvrásněného	zvrásněný	k2eAgInSc2d1	zvrásněný
materiálu	materiál	k1gInSc2	materiál
nazvaný	nazvaný	k2eAgMnSc1d1	nazvaný
Guabonito	Guabonit	k2eAgNnSc4d1	Guabonit
<g/>
,	,	kIx,	,
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kruhovitý	kruhovitý	k2eAgInSc4d1	kruhovitý
impaktní	impaktní	k2eAgInSc4d1	impaktní
kráter	kráter	k1gInSc4	kráter
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jehož	k3xOyRp3gFnSc2	jehož
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
části	část	k1gFnSc2	část
kruhu	kruh	k1gInSc2	kruh
byl	být	k5eAaImAgMnS	být
větrem	vítr	k1gInSc7	vítr
nafoukán	nafoukán	k2eAgInSc4d1	nafoukán
sediment	sediment	k1gInSc4	sediment
tmavé	tmavý	k2eAgFnSc2d1	tmavá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
útvary	útvar	k1gInPc1	útvar
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
i	i	k9	i
v	v	k7c6	v
tmavých	tmavý	k2eAgInPc6d1	tmavý
regionech	region	k1gInPc6	region
Shangri-La	Shangri-Lum	k1gNnSc2	Shangri-Lum
a	a	k8xC	a
Aaru	Aarus	k1gInSc2	Aarus
<g/>
.	.	kIx.	.
</s>
<s>
Kruhovité	kruhovitý	k2eAgInPc1d1	kruhovitý
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
impakty	impakt	k1gInPc1	impakt
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
spatřeny	spatřit	k5eAaPmNgFnP	spatřit
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Xanadu	Xanad	k1gInSc2	Xanad
při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
sondy	sonda	k1gFnSc2	sonda
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
kráterů	kráter	k1gInPc2	kráter
a	a	k8xC	a
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
by	by	kYmCp3nP	by
jimi	on	k3xPp3gMnPc7	on
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c6	o
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
erozi	eroze	k1gFnSc6	eroze
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
útvary	útvar	k1gInPc1	útvar
byly	být	k5eAaImAgInP	být
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
modifikovány	modifikovat	k5eAaBmNgFnP	modifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
velkých	velký	k2eAgInPc2d1	velký
kráterů	kráter	k1gInPc2	kráter
má	mít	k5eAaImIp3nS	mít
porušený	porušený	k2eAgInSc4d1	porušený
nebo	nebo	k8xC	nebo
neúplný	úplný	k2eNgInSc4d1	neúplný
okolní	okolní	k2eAgInSc4d1	okolní
lem	lem	k1gInSc4	lem
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
lemů	lem	k1gInPc2	lem
jsou	být	k5eAaImIp3nP	být
nejsilnější	silný	k2eAgFnPc1d3	nejsilnější
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
ledových	ledový	k2eAgInPc2d1	ledový
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
zaznamenána	zaznamenán	k2eAgFnSc1d1	zaznamenána
tvorba	tvorba	k1gFnSc1	tvorba
pamplisestů	pamplisest	k1gInPc2	pamplisest
<g/>
,	,	kIx,	,
starých	starý	k2eAgInPc2d1	starý
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
reliéf	reliéf	k1gInSc1	reliéf
byl	být	k5eAaImAgInS	být
vlivem	vlivem	k7c2	vlivem
viskoelastické	viskoelastický	k2eAgFnSc2d1	viskoelastická
relaxace	relaxace	k1gFnSc2	relaxace
zahlazen	zahladit	k5eAaPmNgMnS	zahladit
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
jen	jen	k6eAd1	jen
světlý	světlý	k2eAgInSc1d1	světlý
ledový	ledový	k2eAgInSc1d1	ledový
kruh	kruh	k1gInSc1	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
většinou	většinou	k6eAd1	většinou
postrádají	postrádat	k5eAaImIp3nP	postrádat
centrální	centrální	k2eAgInSc4d1	centrální
kopec	kopec	k1gInSc4	kopec
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
rovná	rovný	k2eAgFnSc1d1	rovná
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
působením	působení	k1gNnSc7	působení
kryovulkanické	kryovulkanický	k2eAgFnSc2d1	kryovulkanický
lávy	láva	k1gFnSc2	láva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
vylila	vylít	k5eAaPmAgFnS	vylít
buď	buď	k8xC	buď
během	během	k7c2	během
nárazu	náraz	k1gInSc2	náraz
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
erupcích	erupce	k1gFnPc6	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Geologické	geologický	k2eAgFnPc1d1	geologická
změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
jen	jen	k6eAd1	jen
jedním	jeden	k4xCgInSc7	jeden
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nejsou	být	k5eNaImIp3nP	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
větší	veliký	k2eAgNnPc1d2	veliký
množství	množství	k1gNnSc4	množství
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
hustá	hustý	k2eAgFnSc1d1	hustá
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
snižuje	snižovat	k5eAaImIp3nS	snižovat
množství	množství	k1gNnSc1	množství
kráterů	kráter	k1gInPc2	kráter
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
dva	dva	k4xCgInPc4	dva
řády	řád	k1gInPc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
radarových	radarový	k2eAgFnPc6d1	radarová
mapách	mapa	k1gFnPc6	mapa
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
získány	získat	k5eAaPmNgFnP	získat
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
pro	pro	k7c4	pro
omezenou	omezený	k2eAgFnSc4d1	omezená
část	část	k1gFnSc4	část
měsíčního	měsíční	k2eAgInSc2d1	měsíční
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
nerovnoměrnost	nerovnoměrnost	k1gFnSc1	nerovnoměrnost
v	v	k7c6	v
rozložení	rozložení	k1gNnSc6	rozložení
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Xanadu	Xanad	k1gInSc2	Xanad
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
2	[number]	k4	2
až	až	k9	až
9	[number]	k4	9
<g/>
krát	krát	k6eAd1	krát
víc	hodně	k6eAd2	hodně
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
než	než	k8xS	než
ostatní	ostatní	k2eAgInSc4d1	ostatní
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
je	být	k5eAaImIp3nS	být
i	i	k9	i
zastoupení	zastoupení	k1gNnSc4	zastoupení
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
a	a	k8xC	a
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
polokoule	polokoule	k1gFnSc2	polokoule
obrácená	obrácený	k2eAgFnSc1d1	obrácená
čelem	čelo	k1gNnSc7	čelo
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
více	hodně	k6eAd2	hodně
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
hustota	hustota	k1gFnSc1	hustota
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
rovníkových	rovníkový	k2eAgFnPc2d1	Rovníková
dun	duna	k1gFnPc2	duna
a	a	k8xC	a
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
nejobvyklejší	obvyklý	k2eAgNnPc4d3	nejobvyklejší
jezera	jezero	k1gNnPc4	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
dopadových	dopadový	k2eAgFnPc2d1	dopadová
drah	draha	k1gFnPc2	draha
a	a	k8xC	a
úhlů	úhel	k1gInPc2	úhel
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
před	před	k7c7	před
zkoumaním	zkoumaní	k2eAgMnSc7d1	zkoumaní
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
,	,	kIx,	,
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dopadu	dopad	k1gInSc2	dopad
mohla	moct	k5eAaImAgFnS	moct
vyvrhnout	vyvrhnout	k5eAaPmF	vyvrhnout
tekutá	tekutý	k2eAgFnSc1d1	tekutá
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
zde	zde	k6eAd1	zde
mohla	moct	k5eAaImAgFnS	moct
vydržet	vydržet	k5eAaPmF	vydržet
i	i	k9	i
po	po	k7c4	po
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
<g/>
,	,	kIx,	,
dostatečně	dostatečně	k6eAd1	dostatečně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
"	"	kIx"	"
<g/>
syntetizovaly	syntetizovat	k5eAaImAgFnP	syntetizovat
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
prekurzorové	prekurzorový	k2eAgFnPc1d1	prekurzorový
molekuly	molekula	k1gFnPc1	molekula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
vědci	vědec	k1gMnPc1	vědec
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podmínky	podmínka	k1gFnPc1	podmínka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
podmínkám	podmínka	k1gFnPc3	podmínka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
vzniku	vznik	k1gInSc6	vznik
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
výrazně	výrazně	k6eAd1	výrazně
nižších	nízký	k2eAgFnPc2d2	nižší
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Detekce	detekce	k1gFnSc1	detekce
argonu-	argonu-	k?	argonu-
<g/>
40	[number]	k4	40
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
indikovala	indikovat	k5eAaBmAgFnS	indikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
sopkami	sopka	k1gFnPc7	sopka
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
výstupy	výstup	k1gInPc4	výstup
kryolávy	kryoláva	k1gFnPc1	kryoláva
složené	složený	k2eAgFnPc1d1	složená
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Četnost	četnost	k1gFnSc1	četnost
methanových	methanův	k2eAgNnPc2d1	methanův
jezer	jezero	k1gNnPc2	jezero
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
není	být	k5eNaImIp3nS	být
podle	podle	k7c2	podle
pořízených	pořízený	k2eAgFnPc2d1	pořízená
map	mapa	k1gFnPc2	mapa
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
současné	současný	k2eAgNnSc4d1	současné
množství	množství	k1gNnSc4	množství
plynného	plynný	k2eAgInSc2d1	plynný
methanu	methan	k1gInSc2	methan
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
objeveno	objevit	k5eAaPmNgNnS	objevit
jen	jen	k6eAd1	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgInPc6	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
prohlásit	prohlásit	k5eAaPmF	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sopky	sopka	k1gFnPc4	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
kryovulkánem	kryovulkán	k1gMnSc7	kryovulkán
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Ganesa	Ganesa	k1gFnSc1	Ganesa
Macula	Macul	k1gMnSc2	Macul
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
připomínal	připomínat	k5eAaImAgMnS	připomínat
nezvyklé	zvyklý	k2eNgInPc4d1	nezvyklý
vulkanické	vulkanický	k2eAgInPc4d1	vulkanický
útvary	útvar	k1gInPc4	útvar
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
sopečné	sopečný	k2eAgInPc1d1	sopečný
dómy	dóm	k1gInPc1	dóm
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
domněnka	domněnka	k1gFnSc1	domněnka
však	však	k9	však
byla	být	k5eAaImAgFnS	být
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
na	na	k7c6	na
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
sjezdu	sjezd	k1gInSc6	sjezd
American	American	k1gMnSc1	American
Geophysical	Geophysical	k1gFnSc2	Geophysical
Union	union	k1gInSc1	union
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
bylo	být	k5eAaImAgNnS	být
předneseno	přednesen	k2eAgNnSc4d1	předneseno
zjištění	zjištění	k1gNnSc4	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
útvar	útvar	k1gInSc1	útvar
totiž	totiž	k9	totiž
není	být	k5eNaImIp3nS	být
žádným	žádný	k3yNgInSc7	žádný
dómem	dóm	k1gInSc7	dóm
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
představu	představa	k1gFnSc4	představa
způsobila	způsobit	k5eAaPmAgFnS	způsobit
pouze	pouze	k6eAd1	pouze
náhodná	náhodný	k2eAgFnSc1d1	náhodná
kombinace	kombinace	k1gFnSc1	kombinace
světlých	světlý	k2eAgNnPc2d1	světlé
a	a	k8xC	a
tmavých	tmavý	k2eAgNnPc2d1	tmavé
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
objevila	objevit	k5eAaPmAgNnP	objevit
nezvykle	zvykle	k6eNd1	zvykle
světlé	světlý	k2eAgNnSc4d1	světlé
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
Tortola	Tortola	k1gFnSc1	Tortola
Facula	Facul	k1gMnSc2	Facul
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
identifikováno	identifikovat	k5eAaBmNgNnS	identifikovat
jako	jako	k8xC	jako
kryovulkanický	kryovulkanický	k2eAgInSc1d1	kryovulkanický
dóm	dóm	k1gInSc1	dóm
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
nebyly	být	k5eNaImAgFnP	být
detekovány	detekovat	k5eAaImNgInP	detekovat
žádné	žádný	k3yNgInPc1	žádný
další	další	k2eAgInPc1d1	další
útvary	útvar	k1gInPc1	útvar
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
astronomové	astronom	k1gMnPc1	astronom
oznámili	oznámit	k5eAaPmAgMnP	oznámit
nález	nález	k1gInSc4	nález
dvou	dva	k4xCgFnPc2	dva
velmi	velmi	k6eAd1	velmi
světlých	světlý	k2eAgFnPc2d1	světlá
skvrn	skvrna	k1gFnPc2	skvrna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
pouze	pouze	k6eAd1	pouze
přechodný	přechodný	k2eAgInSc4d1	přechodný
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
příliš	příliš	k6eAd1	příliš
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
běžnými	běžný	k2eAgInPc7d1	běžný
výkyvy	výkyv	k1gInPc7	výkyv
počasí	počasí	k1gNnSc2	počasí
–	–	k?	–
možným	možný	k2eAgNnSc7d1	možné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
byl	být	k5eAaImAgInS	být
původ	původ	k1gInSc1	původ
ve	v	k7c6	v
vulkanické	vulkanický	k2eAgFnSc6d1	vulkanická
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byly	být	k5eAaImAgFnP	být
spatřeny	spatřen	k2eAgFnPc1d1	spatřena
fluktuace	fluktuace	k1gFnPc1	fluktuace
v	v	k7c6	v
jasu	jas	k1gInSc6	jas
oblasti	oblast	k1gFnSc2	oblast
zvané	zvaný	k2eAgFnSc2d1	zvaná
Hotei	Hote	k1gFnSc2	Hote
Arcus	Arcus	k1gMnSc1	Arcus
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
struktury	struktura	k1gFnPc1	struktura
podobající	podobající	k2eAgFnPc1d1	podobající
se	se	k3xPyFc4	se
lávovým	lávový	k2eAgMnPc3d1	lávový
proudům	proud	k1gInPc3	proud
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byla	být	k5eAaImAgNnP	být
poskytnuta	poskytnut	k2eAgNnPc1d1	poskytnuto
různá	různý	k2eAgNnPc1d1	různé
vysvětlení	vysvětlení	k1gNnPc1	vysvětlení
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
při	při	k7c6	při
předpokladu	předpoklad	k1gInSc6	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
lávové	lávový	k2eAgInPc4d1	lávový
proudy	proud	k1gInPc4	proud
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stoupají	stoupat	k5eAaImIp3nP	stoupat
až	až	k6eAd1	až
dvě	dva	k4xCgFnPc1	dva
stě	sto	k4xCgFnPc1	sto
metrů	metr	k1gInPc2	metr
nad	nad	k7c4	nad
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
představou	představa	k1gFnSc7	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
byly	být	k5eAaImAgInP	být
vyvrženy	vyvrhnout	k5eAaPmNgInP	vyvrhnout
zpod	zpod	k7c2	zpod
povrchu	povrch	k1gInSc2	povrch
během	během	k7c2	během
erupce	erupce	k1gFnSc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
objevila	objevit	k5eAaPmAgFnS	objevit
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
150	[number]	k4	150
km	km	kA	km
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
široké	široký	k2eAgNnSc1d1	široké
30	[number]	k4	30
km	km	kA	km
a	a	k8xC	a
1,5	[number]	k4	1,5
km	km	kA	km
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
zřejmě	zřejmě	k6eAd1	zřejmě
ledovými	ledový	k2eAgInPc7d1	ledový
útvary	útvar	k1gInPc7	útvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
methanovým	methanův	k2eAgInSc7d1	methanův
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
byl	být	k5eAaImAgInS	být
masiv	masiv	k1gInSc1	masiv
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
mohl	moct	k5eAaImAgInS	moct
dostat	dostat	k5eAaPmF	dostat
mezerou	mezera	k1gFnSc7	mezera
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
při	pře	k1gFnSc4	pře
pohybu	pohyb	k1gInSc2	pohyb
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
pohyb	pohyb	k1gInSc1	pohyb
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
dopadem	dopad	k1gInSc7	dopad
meteoritu	meteorit	k1gInSc2	meteorit
do	do	k7c2	do
blízkého	blízký	k2eAgNnSc2d1	blízké
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
těmito	tento	k3xDgInPc7	tento
objevy	objev	k1gInPc4	objev
vědci	vědec	k1gMnPc1	vědec
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
ráz	ráz	k1gInSc4	ráz
krajiny	krajina	k1gFnSc2	krajina
utvářen	utvářen	k2eAgInSc1d1	utvářen
především	především	k9	především
nárazy	náraz	k1gInPc1	náraz
vnějších	vnější	k2eAgNnPc2d1	vnější
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
objevy	objev	k1gInPc4	objev
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
ale	ale	k9	ale
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
pohoří	pohoří	k1gNnSc1	pohoří
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
geologickými	geologický	k2eAgInPc7d1	geologický
procesy	proces	k1gInPc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
identifikovali	identifikovat	k5eAaBmAgMnP	identifikovat
vědci	vědec	k1gMnPc1	vědec
horu	hora	k1gFnSc4	hora
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
Sotra	Sotr	k1gInSc2	Sotr
Patera	Patera	k1gMnSc1	Patera
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
nejméně	málo	k6eAd3	málo
dvou	dva	k4xCgFnPc2	dva
dalších	další	k2eAgFnPc2d1	další
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vysoké	vysoká	k1gFnPc4	vysoká
od	od	k7c2	od
1000	[number]	k4	1000
do	do	k7c2	do
1500	[number]	k4	1500
m	m	kA	m
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
vrchol	vrchol	k1gInSc1	vrchol
tvoří	tvořit	k5eAaImIp3nS	tvořit
kráter	kráter	k1gInSc4	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
úpatí	úpatí	k1gNnSc1	úpatí
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
být	být	k5eAaImF	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
zmrzlou	zmrzlý	k2eAgFnSc7d1	zmrzlá
lávou	láva	k1gFnSc7	láva
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
pohoří	pohoří	k1gNnSc1	pohoří
Titanu	titan	k1gInSc2	titan
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
pásmové	pásmový	k2eAgInPc4d1	pásmový
hřbety	hřbet	k1gInPc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Himálaj	Himálaj	k1gFnSc1	Himálaj
či	či	k8xC	či
Alpy	Alpy	k1gFnPc1	Alpy
byly	být	k5eAaImAgFnP	být
vyvrásněny	vyvrásnit	k5eAaPmNgFnP	vyvrásnit
při	při	k7c6	při
kolizi	kolize	k1gFnSc6	kolize
nebo	nebo	k8xC	nebo
podsouvání	podsouvání	k1gNnSc6	podsouvání
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
možným	možný	k2eAgInSc7d1	možný
mechanismem	mechanismus	k1gInSc7	mechanismus
je	být	k5eAaImIp3nS	být
vytvoření	vytvoření	k1gNnSc1	vytvoření
hřbetu	hřbet	k1gInSc2	hřbet
slapovými	slapový	k2eAgFnPc7d1	slapová
silami	síla	k1gFnPc7	síla
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Viskozita	viskozita	k1gFnSc1	viskozita
ledového	ledový	k2eAgInSc2d1	ledový
pláště	plášť	k1gInSc2	plášť
Titanu	titan	k1gInSc2	titan
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgNnSc4d2	nižší
a	a	k8xC	a
horské	horský	k2eAgNnSc4d1	horské
podloží	podloží	k1gNnSc4	podloží
méně	málo	k6eAd2	málo
pevné	pevný	k2eAgFnSc2d1	pevná
než	než	k8xS	než
zemské	zemský	k2eAgFnSc2d1	zemská
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nemohou	moct	k5eNaImIp3nP	moct
hory	hora	k1gFnPc1	hora
dosahovat	dosahovat	k5eAaImF	dosahovat
takových	takový	k3xDgFnPc2	takový
výšek	výška	k1gFnPc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
tým	tým	k1gInSc1	tým
projektu	projekt	k1gInSc2	projekt
Cassini	Cassin	k2eAgMnPc1d1	Cassin
oznámil	oznámit	k5eAaPmAgInS	oznámit
objev	objev	k1gInSc4	objev
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
doposud	doposud	k6eAd1	doposud
známé	známý	k2eAgFnSc2d1	známá
hory	hora	k1gFnSc2	hora
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
vrchol	vrchol	k1gInSc1	vrchol
ční	čnět	k5eAaImIp3nS	čnět
3	[number]	k4	3
337	[number]	k4	337
m	m	kA	m
vysoko	vysoko	k6eAd1	vysoko
a	a	k8xC	a
nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Mithrim	Mithrim	k1gMnSc1	Mithrim
Montes	Montes	k1gMnSc1	Montes
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hypotéza	hypotéza	k1gFnSc1	hypotéza
o	o	k7c6	o
vulkanické	vulkanický	k2eAgFnSc6d1	vulkanická
činnosti	činnost	k1gFnSc6	činnost
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
správná	správný	k2eAgFnSc1d1	správná
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
napájen	napájet	k5eAaImNgMnS	napájet
energií	energie	k1gFnSc7	energie
z	z	k7c2	z
rozpadu	rozpad	k1gInSc2	rozpad
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
prvků	prvek	k1gInPc2	prvek
uvnitř	uvnitř	k7c2	uvnitř
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Zemské	zemský	k2eAgNnSc1d1	zemské
magma	magma	k1gNnSc1	magma
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
roztavenými	roztavený	k2eAgFnPc7d1	roztavená
horninami	hornina	k1gFnPc7	hornina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
pevná	pevný	k2eAgFnSc1d1	pevná
kůra	kůra	k1gFnSc1	kůra
nad	nad	k7c7	nad
nimi	on	k3xPp3gFnPc7	on
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
kapalná	kapalný	k2eAgFnSc1d1	kapalná
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
eutektické	eutektický	k2eAgFnSc6d1	eutektická
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
amoniakem	amoniak	k1gInSc7	amoniak
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hustší	hustý	k2eAgInSc1d2	hustší
než	než	k8xS	než
vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
pro	pro	k7c4	pro
spuštění	spuštění	k1gNnSc4	spuštění
kryovulkanických	kryovulkanický	k2eAgInPc2d1	kryovulkanický
procesů	proces	k1gInPc2	proces
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
potřeba	potřeba	k1gFnSc1	potřeba
další	další	k2eAgFnSc2d1	další
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ze	z	k7c2	z
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
blízkého	blízký	k2eAgInSc2d1	blízký
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
může	moct	k5eAaImIp3nS	moct
ve	v	k7c6	v
vulkanické	vulkanický	k2eAgFnSc6d1	vulkanická
činnosti	činnost	k1gFnSc6	činnost
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
přítomnost	přítomnost	k1gFnSc1	přítomnost
síranu	síran	k1gInSc2	síran
amonného	amonný	k2eAgInSc2d1	amonný
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
společně	společně	k6eAd1	společně
nestabilní	stabilní	k2eNgInSc1d1	nestabilní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
může	moct	k5eAaImIp3nS	moct
explodovat	explodovat	k5eAaBmF	explodovat
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
zrnitého	zrnitý	k2eAgInSc2d1	zrnitý
ledu	led	k1gInSc2	led
a	a	k8xC	a
popela	popel	k1gInSc2	popel
ze	z	k7c2	z
síranu	síran	k1gInSc2	síran
amonného	amonný	k2eAgInSc2d1	amonný
tvoří	tvořit	k5eAaImIp3nS	tvořit
písečný	písečný	k2eAgInSc1d1	písečný
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
tvarován	tvarovat	k5eAaImNgMnS	tvarovat
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
přednesl	přednést	k5eAaPmAgMnS	přednést
Jeffrey	Jeffre	k2eAgFnPc4d1	Jeffre
Moore	Moor	k1gInSc5	Moor
<g/>
,	,	kIx,	,
planetární	planetární	k2eAgMnSc1d1	planetární
geolog	geolog	k1gMnSc1	geolog
z	z	k7c2	z
Ames	Amesa	k1gFnPc2	Amesa
Research	Research	k1gInSc1	Research
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
Amesova	Amesův	k2eAgNnSc2d1	Amesovo
výzkumného	výzkumný	k2eAgNnSc2d1	výzkumné
centra	centrum	k1gNnSc2	centrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc4d1	vlastní
teorii	teorie	k1gFnSc4	teorie
o	o	k7c4	o
geologii	geologie	k1gFnSc4	geologie
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebyly	být	k5eNaImAgFnP	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
s	s	k7c7	s
určitostí	určitost	k1gFnSc7	určitost
identifikovány	identifikován	k2eAgInPc4d1	identifikován
žádné	žádný	k3yNgInPc4	žádný
útvary	útvar	k1gInPc4	útvar
sopečného	sopečný	k2eAgInSc2d1	sopečný
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Moore	Moor	k1gInSc5	Moor
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Titan	titan	k1gInSc4	titan
za	za	k7c4	za
geologicky	geologicky	k6eAd1	geologicky
mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
hypotézy	hypotéza	k1gFnSc2	hypotéza
byl	být	k5eAaImAgInS	být
ráz	ráz	k1gInSc1	ráz
povrchu	povrch	k1gInSc2	povrch
utvářen	utvářen	k2eAgInSc1d1	utvářen
jen	jen	k6eAd1	jen
větrnou	větrný	k2eAgFnSc7d1	větrná
a	a	k8xC	a
kapalinovou	kapalinový	k2eAgFnSc7d1	kapalinová
erozí	eroze	k1gFnSc7	eroze
<g/>
,	,	kIx,	,
přesunem	přesun	k1gInSc7	přesun
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
vnějšími	vnější	k2eAgInPc7d1	vnější
vlivy	vliv	k1gInPc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Methan	methan	k1gInSc1	methan
se	se	k3xPyFc4	se
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
nedostává	dostávat	k5eNaImIp3nS	dostávat
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pomalu	pomalu	k6eAd1	pomalu
difunduje	difundovat	k5eAaImIp3nS	difundovat
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ganesa	Ganesa	k1gFnSc1	Ganesa
Macula	Macula	k1gFnSc1	Macula
může	moct	k5eAaImIp3nS	moct
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
být	být	k5eAaImF	být
starý	starý	k2eAgInSc4d1	starý
impaktní	impaktní	k2eAgInSc4d1	impaktní
kráter	kráter	k1gInSc4	kráter
s	s	k7c7	s
tmavou	tmavý	k2eAgFnSc7d1	tmavá
dunou	duna	k1gFnSc7	duna
uprostřed	uprostřed	k7c2	uprostřed
<g/>
;	;	kIx,	;
horské	horský	k2eAgInPc1d1	horský
masivy	masiv	k1gInPc1	masiv
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
degradací	degradace	k1gFnSc7	degradace
lemů	lem	k1gInPc2	lem
(	(	kIx(	(
<g/>
i	i	k8xC	i
vícečetných	vícečetný	k2eAgInPc2d1	vícečetný
<g/>
)	)	kIx)	)
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
nebo	nebo	k8xC	nebo
kontrakcí	kontrakce	k1gFnSc7	kontrakce
tělesa	těleso	k1gNnSc2	těleso
při	při	k7c6	při
ochlazování	ochlazování	k1gNnSc6	ochlazování
vnitřku	vnitřek	k1gInSc2	vnitřek
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Xanadu	Xanad	k1gInSc6	Xanad
připodobnil	připodobnit	k5eAaPmAgMnS	připodobnit
k	k	k7c3	k
útvarům	útvar	k1gInPc3	útvar
nalezeným	nalezený	k2eAgInPc3d1	nalezený
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
Callisto	Callista	k1gMnSc5	Callista
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
porušené	porušený	k2eAgInPc4d1	porušený
<g/>
;	;	kIx,	;
obecně	obecně	k6eAd1	obecně
podle	podle	k7c2	podle
Moorea	Mooreus	k1gMnSc2	Mooreus
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
Callisto	Callista	k1gMnSc5	Callista
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
model	model	k1gInSc4	model
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
geologie	geologie	k1gFnSc2	geologie
Titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc4	měsíc
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
nazýval	nazývat	k5eAaImAgInS	nazývat
Callisto	Callista	k1gMnSc5	Callista
s	s	k7c7	s
počasím	počasí	k1gNnSc7	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
astronomická	astronomický	k2eAgFnSc1d1	astronomická
unie	unie	k1gFnSc1	unie
přidělila	přidělit	k5eAaPmAgFnS	přidělit
nejvyšším	vysoký	k2eAgFnPc3d3	nejvyšší
horám	hora	k1gFnPc3	hora
a	a	k8xC	a
pohořím	pohořet	k5eAaPmIp1nS	pohořet
oficiální	oficiální	k2eAgInPc4d1	oficiální
názvy	název	k1gInPc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
dle	dle	k7c2	dle
dohody	dohoda	k1gFnSc2	dohoda
po	po	k7c6	po
fiktivních	fiktivní	k2eAgFnPc6d1	fiktivní
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
pohořích	pohoří	k1gNnPc6	pohoří
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
postavách	postava	k1gFnPc6	postava
(	(	kIx(	(
<g/>
u	u	k7c2	u
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
)	)	kIx)	)
z	z	k7c2	z
příběhů	příběh	k1gInPc2	příběh
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
o	o	k7c6	o
Středozemi	Středozem	k1gFnSc6	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvních	první	k4xOgInPc6	první
snímcích	snímek	k1gInPc6	snímek
pořízených	pořízený	k2eAgFnPc2d1	pořízená
pozemskými	pozemský	k2eAgInPc7d1	pozemský
teleskopy	teleskop	k1gInPc7	teleskop
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
byly	být	k5eAaImAgFnP	být
zobrazeny	zobrazit	k5eAaPmNgFnP	zobrazit
rozlehlé	rozlehlý	k2eAgFnPc1d1	rozlehlá
tmavé	tmavý	k2eAgFnPc1d1	tmavá
oblasti	oblast	k1gFnPc1	oblast
okolo	okolo	k7c2	okolo
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příletem	přílet	k1gInSc7	přílet
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
byly	být	k5eAaImAgInP	být
mylně	mylně	k6eAd1	mylně
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
uhlovodíková	uhlovodíkový	k2eAgNnPc4d1	uhlovodíkové
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Radarové	radarový	k2eAgInPc4d1	radarový
snímky	snímek	k1gInPc4	snímek
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejde	jít	k5eNaImIp3nS	jít
ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
o	o	k7c4	o
moře	moře	k1gNnSc4	moře
nějaké	nějaký	k3yIgFnSc2	nějaký
kapaliny	kapalina	k1gFnSc2	kapalina
jako	jako	k9	jako
o	o	k7c4	o
moře	moře	k1gNnSc4	moře
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rovníkové	rovníkový	k2eAgFnPc1d1	Rovníková
oblasti	oblast	k1gFnPc1	oblast
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
být	být	k5eAaImF	být
rozlehlými	rozlehlý	k2eAgFnPc7d1	rozlehlá
pouštěmi	poušť	k1gFnPc7	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
písečných	písečný	k2eAgInPc2d1	písečný
oceánů	oceán	k1gInPc2	oceán
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
Belet	Beleta	k1gFnPc2	Beleta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
planinách	planina	k1gFnPc6	planina
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mohutné	mohutný	k2eAgFnPc1d1	mohutná
podélné	podélný	k2eAgFnPc1d1	podélná
duny	duna	k1gFnPc1	duna
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
takové	takový	k3xDgFnPc1	takový
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
hřeben	hřeben	k1gInSc1	hřeben
je	být	k5eAaImIp3nS	být
rovnoběžný	rovnoběžný	k2eAgInSc1d1	rovnoběžný
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
duny	duna	k1gFnPc1	duna
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
kilometr	kilometr	k1gInSc1	kilometr
široké	široký	k2eAgFnSc2d1	široká
a	a	k8xC	a
desítky	desítka	k1gFnSc2	desítka
až	až	k9	až
stovky	stovka	k1gFnPc1	stovka
kilometrů	kilometr	k1gInPc2	kilometr
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
směřují	směřovat	k5eAaImIp3nP	směřovat
od	od	k7c2	od
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vítr	vítr	k1gInSc1	vítr
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
terénní	terénní	k2eAgFnSc4d1	terénní
bariéru	bariéra	k1gFnSc4	bariéra
a	a	k8xC	a
obtékáji	obtékáje	k1gFnSc4	obtékáje
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
duny	duna	k1gFnPc1	duna
odkloněny	odklonit	k5eAaPmNgFnP	odklonit
od	od	k7c2	od
převládajícího	převládající	k2eAgInSc2d1	převládající
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
terén	terén	k1gInSc4	terén
směr	směr	k1gInSc1	směr
větru	vítr	k1gInSc2	vítr
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vznikající	vznikající	k2eAgFnPc1d1	vznikající
duny	duna	k1gFnPc1	duna
mění	měnit	k5eAaImIp3nP	měnit
z	z	k7c2	z
podélných	podélný	k2eAgMnPc2d1	podélný
na	na	k7c4	na
příčné	příčný	k2eAgInPc4d1	příčný
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
takové	takový	k3xDgFnPc1	takový
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
hřeben	hřeben	k1gInSc1	hřeben
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
směr	směr	k1gInSc4	směr
větru	vítr	k1gInSc2	vítr
kolmý	kolmý	k2eAgInSc4d1	kolmý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
duny	duna	k1gFnPc1	duna
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
například	například	k6eAd1	například
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
vysočiny	vysočina	k1gFnSc2	vysočina
Adiri	Adir	k1gFnSc2	Adir
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
duny	duna	k1gFnPc1	duna
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
větrech	vítr	k1gInPc6	vítr
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sice	sice	k8xC	sice
vanou	vanout	k5eAaImIp3nP	vanout
převážně	převážně	k6eAd1	převážně
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
duny	duna	k1gFnPc1	duna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
směr	směr	k1gInSc1	směr
i	i	k8xC	i
síla	síla	k1gFnSc1	síla
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
.	.	kIx.	.
</s>
<s>
Potřebná	potřebný	k2eAgFnSc1d1	potřebná
odhadovaná	odhadovaný	k2eAgFnSc1d1	odhadovaná
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
podélné	podélný	k2eAgFnSc2d1	podélná
duny	duna	k1gFnSc2	duna
je	být	k5eAaImIp3nS	být
0,5	[number]	k4	0,5
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
2	[number]	k4	2
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slabý	slabý	k2eAgInSc1d1	slabý
povrchový	povrchový	k2eAgInSc1d1	povrchový
východní	východní	k2eAgInSc1d1	východní
vítr	vítr	k1gInSc1	vítr
by	by	kYmCp3nS	by
dostačoval	dostačovat	k5eAaImAgInS	dostačovat
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgNnPc1d1	následné
pozorování	pozorování	k1gNnPc1	pozorování
ovšem	ovšem	k9	ovšem
ukázala	ukázat	k5eAaPmAgNnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podélné	podélný	k2eAgFnPc1d1	podélná
duny	duna	k1gFnPc1	duna
směřují	směřovat	k5eAaImIp3nP	směřovat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
vítr	vítr	k1gInSc1	vítr
vane	vanout	k5eAaImIp3nS	vanout
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledních	poslední	k2eAgFnPc2d1	poslední
počítačových	počítačový	k2eAgFnPc2d1	počítačová
simulací	simulace	k1gFnPc2	simulace
pořízených	pořízený	k2eAgFnPc2d1	pořízená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
duny	duna	k1gFnSc2	duna
vznikají	vznikat	k5eAaImIp3nP	vznikat
během	během	k7c2	během
vzácných	vzácný	k2eAgInPc2d1	vzácný
západních	západní	k2eAgInPc2d1	západní
bouřkových	bouřkový	k2eAgInPc2d1	bouřkový
větrů	vítr	k1gInPc2	vítr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vanou	vanout	k5eAaImIp3nP	vanout
každých	každý	k3xTgNnPc2	každý
15	[number]	k4	15
let	léto	k1gNnPc2	léto
v	v	k7c6	v
období	období	k1gNnSc6	období
měsíční	měsíční	k2eAgFnSc2d1	měsíční
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
rychlostí	rychlost	k1gFnSc7	rychlost
do	do	k7c2	do
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
při	při	k7c6	při
formování	formování	k1gNnSc6	formování
dun	duna	k1gFnPc2	duna
výrazně	výrazně	k6eAd1	výrazně
dominují	dominovat	k5eAaImIp3nP	dominovat
nad	nad	k7c7	nad
běžným	běžný	k2eAgInSc7d1	běžný
větrem	vítr	k1gInSc7	vítr
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Dunová	dunový	k2eAgNnPc1d1	dunový
pole	pole	k1gNnPc1	pole
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
zabírají	zabírat	k5eAaImIp3nP	zabírat
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
ohraničeném	ohraničený	k2eAgInSc6d1	ohraničený
rovnoběžkami	rovnoběžka	k1gFnPc7	rovnoběžka
30	[number]	k4	30
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
a	a	k8xC	a
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
duny	duna	k1gFnPc1	duna
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ve	v	k7c6	v
vyšší	vysoký	k2eAgFnSc6d2	vyšší
vlhkosti	vlhkost	k1gFnSc6	vlhkost
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
nebo	nebo	k8xC	nebo
příliš	příliš	k6eAd1	příliš
slabém	slabý	k2eAgInSc6d1	slabý
větru	vítr	k1gInSc6	vítr
<g/>
,	,	kIx,	,
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
převáží	převážit	k5eAaPmIp3nS	převážit
směrovost	směrovost	k1gFnSc4	směrovost
slapových	slapový	k2eAgInPc2d1	slapový
větrů	vítr	k1gInPc2	vítr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
písek	písek	k1gInSc4	písek
přenesou	přenést	k5eAaPmIp3nP	přenést
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
rovníku	rovník	k1gInSc3	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Titanský	Titanský	k2eAgInSc1d1	Titanský
písek	písek	k1gInSc1	písek
není	být	k5eNaImIp3nS	být
složením	složení	k1gNnSc7	složení
vůbec	vůbec	k9	vůbec
podobný	podobný	k2eAgInSc1d1	podobný
pozemskému	pozemský	k2eAgInSc3d1	pozemský
<g/>
,	,	kIx,	,
netvoří	tvořit	k5eNaImIp3nS	tvořit
ho	on	k3xPp3gMnSc4	on
silikátová	silikátový	k2eAgNnPc1d1	silikátové
zrníčka	zrníčko	k1gNnPc1	zrníčko
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
spíše	spíše	k9	spíše
vodní	vodní	k2eAgInSc4d1	vodní
led	led	k1gInSc4	led
z	z	k7c2	z
podloží	podloží	k1gNnSc2	podloží
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yRgInSc2	který
písečná	písečný	k2eAgFnSc1d1	písečná
hmota	hmota	k1gFnSc1	hmota
vzniká	vznikat	k5eAaImIp3nS	vznikat
erozí	eroze	k1gFnSc7	eroze
během	během	k7c2	během
bleskových	bleskový	k2eAgFnPc2d1	blesková
záplav	záplava	k1gFnPc2	záplava
kapalného	kapalný	k2eAgInSc2d1	kapalný
methanu	methan	k1gInSc2	methan
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
písek	písek	k1gInSc1	písek
tvoří	tvořit	k5eAaImIp3nP	tvořit
pevné	pevný	k2eAgFnPc4d1	pevná
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
fotochemickou	fotochemický	k2eAgFnSc7d1	fotochemická
reakcí	reakce	k1gFnSc7	reakce
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
tholiny	tholin	k1gInPc7	tholin
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
i	i	k9	i
studie	studie	k1gFnPc1	studie
složení	složení	k1gNnSc2	složení
písečných	písečný	k2eAgFnPc2d1	písečná
dun	duna	k1gFnPc2	duna
optickou	optický	k2eAgFnSc7d1	optická
a	a	k8xC	a
spektrální	spektrální	k2eAgFnSc7d1	spektrální
analýzou	analýza	k1gFnSc7	analýza
<g/>
,	,	kIx,	,
provedené	provedený	k2eAgInPc1d1	provedený
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
závěrem	závěr	k1gInSc7	závěr
bylo	být	k5eAaImAgNnS	být
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgNnSc4d2	nižší
zastoupení	zastoupení	k1gNnSc4	zastoupení
vody	voda	k1gFnSc2	voda
než	než	k8xS	než
jinde	jinde	k6eAd1	jinde
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
v	v	k7c6	v
dunách	duna	k1gFnPc6	duna
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nejspíše	nejspíše	k9	nejspíše
tvořen	tvořit	k5eAaImNgInS	tvořit
sazemi	saze	k1gFnPc7	saze
organických	organický	k2eAgInPc2d1	organický
polymerů	polymer	k1gInPc2	polymer
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
shlukly	shluknout	k5eAaPmAgInP	shluknout
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
však	však	k8xC	však
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
písku	písek	k1gInSc2	písek
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
je	být	k5eAaImIp3nS	být
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
třetinu	třetina	k1gFnSc4	třetina
hustoty	hustota	k1gFnSc2	hustota
písku	písek	k1gInSc2	písek
terestrického	terestrický	k2eAgInSc2d1	terestrický
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc4	měsíc
Titan	titan	k1gInSc4	titan
nelze	lze	k6eNd1	lze
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
spatřit	spatřit	k5eAaPmF	spatřit
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
Saturnu	Saturn	k1gInSc2	Saturn
menším	malý	k2eAgInSc7d2	menší
teleskopem	teleskop	k1gInSc7	teleskop
či	či	k8xC	či
větším	veliký	k2eAgInSc7d2	veliký
dalekohledem	dalekohled	k1gInSc7	dalekohled
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
rozeznatelný	rozeznatelný	k2eAgInSc1d1	rozeznatelný
<g/>
.	.	kIx.	.
</s>
<s>
Amatérské	amatérský	k2eAgNnSc1d1	amatérské
pozorování	pozorování	k1gNnSc1	pozorování
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
blízkost	blízkost	k1gFnSc4	blízkost
kotoučku	kotouček	k1gInSc2	kotouček
měsíce	měsíc	k1gInSc2	měsíc
k	k	k7c3	k
přesvětlenému	přesvětlený	k2eAgInSc3d1	přesvětlený
disku	disk	k1gInSc3	disk
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gInSc1	Saturn
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Prohlížení	prohlížený	k2eAgMnPc1d1	prohlížený
lze	lze	k6eAd1	lze
vylepšit	vylepšit	k5eAaPmF	vylepšit
přidáním	přidání	k1gNnSc7	přidání
clony	clona	k1gFnSc2	clona
nebo	nebo	k8xC	nebo
světelného	světelný	k2eAgInSc2d1	světelný
filtru	filtr	k1gInSc2	filtr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odstíní	odstínit	k5eAaPmIp3nS	odstínit
světlo	světlo	k1gNnSc4	světlo
odražené	odražený	k2eAgNnSc4d1	odražené
od	od	k7c2	od
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
Titanu	titan	k1gInSc2	titan
je	být	k5eAaImIp3nS	být
+8,2	+8,2	k4	+8,2
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
hodnota	hodnota	k1gFnSc1	hodnota
při	při	k7c6	při
postavení	postavení	k1gNnSc6	postavení
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
je	být	k5eAaImIp3nS	být
+8,4	+8,4	k4	+8,4
mag	mag	k?	mag
<g/>
;	;	kIx,	;
v	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
obdobích	období	k1gNnPc6	období
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
hodnota	hodnota	k1gFnSc1	hodnota
snížit	snížit	k5eAaPmF	snížit
až	až	k9	až
k	k	k7c3	k
9,0	[number]	k4	9,0
<g/>
m.	m.	k?	m.
Podobně	podobně	k6eAd1	podobně
velký	velký	k2eAgInSc4d1	velký
měsíc	měsíc	k1gInSc4	měsíc
Ganymed	Ganymed	k1gMnSc1	Ganymed
má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgMnSc3	ten
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
4,6	[number]	k4	4,6
mag	mag	k?	mag
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
obdobím	období	k1gNnSc7	období
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
sondážních	sondážní	k2eAgInPc2d1	sondážní
letů	let	k1gInPc2	let
byly	být	k5eAaImAgFnP	být
možnosti	možnost	k1gFnPc1	možnost
zkoumání	zkoumání	k1gNnSc2	zkoumání
Titanu	titan	k1gInSc2	titan
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgFnPc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
španělský	španělský	k2eAgMnSc1d1	španělský
astronom	astronom	k1gMnSc1	astronom
Josep	Josep	k1gMnSc1	Josep
Comas	Comas	k1gMnSc1	Comas
i	i	k8xC	i
Solà	Solà	k1gMnSc1	Solà
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
okrajové	okrajový	k2eAgNnSc4d1	okrajové
ztemnění	ztemnění	k1gNnSc4	ztemnění
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
těleso	těleso	k1gNnSc1	těleso
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
;	;	kIx,	;
Gerard	Gerard	k1gMnSc1	Gerard
Kuiper	Kuiper	k1gMnSc1	Kuiper
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
použil	použít	k5eAaPmAgMnS	použít
spektroskopickou	spektroskopický	k2eAgFnSc4d1	spektroskopická
techniku	technika	k1gFnSc4	technika
k	k	k7c3	k
identifikaci	identifikace	k1gFnSc3	identifikace
methanu	methan	k1gInSc2	methan
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
sondou	sonda	k1gFnSc7	sonda
vyslanou	vyslaný	k2eAgFnSc7d1	vyslaná
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
byl	být	k5eAaImAgInS	být
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
k	k	k7c3	k
soustavě	soustava	k1gFnSc3	soustava
dorazil	dorazit	k5eAaPmAgMnS	dorazit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	on	k3xPp3gNnSc2	on
odhalení	odhalení	k1gNnSc2	odhalení
byl	být	k5eAaImAgInS	být
Titan	titan	k1gInSc1	titan
příliš	příliš	k6eAd1	příliš
chladný	chladný	k2eAgInSc1d1	chladný
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
mohl	moct	k5eAaImAgMnS	moct
existovat	existovat	k5eAaImF	existovat
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Pioneer	Pioneer	kA	Pioneer
pořídil	pořídit	k5eAaPmAgInS	pořídit
několik	několik	k4yIc4	několik
snímků	snímek	k1gInPc2	snímek
Titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
fotografie	fotografia	k1gFnSc2	fotografia
zachycující	zachycující	k2eAgInSc4d1	zachycující
měsíc	měsíc	k1gInSc4	měsíc
s	s	k7c7	s
planetou	planeta	k1gFnSc7	planeta
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc4	obrázek
pořízené	pořízený	k2eAgInPc4d1	pořízený
později	pozdě	k6eAd2	pozdě
dvěma	dva	k4xCgInPc7	dva
Voyagery	Voyagero	k1gNnPc7	Voyagero
byly	být	k5eAaImAgFnP	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnPc1d2	vyšší
kvality	kvalita	k1gFnPc1	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1980	[number]	k4	1980
a	a	k8xC	a
1981	[number]	k4	1981
byl	být	k5eAaImAgInS	být
Titan	titan	k1gInSc1	titan
zkoumán	zkoumán	k2eAgInSc1d1	zkoumán
sondami	sonda	k1gFnPc7	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
1	[number]	k4	1
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Trajektorie	trajektorie	k1gFnSc1	trajektorie
letu	let	k1gInSc2	let
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc4	Voyager
1	[number]	k4	1
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kolem	kolem	k7c2	kolem
měsíce	měsíc	k1gInSc2	měsíc
těsně	těsně	k6eAd1	těsně
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
<g/>
;	;	kIx,	;
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
sonda	sonda	k1gFnSc1	sonda
získala	získat	k5eAaPmAgFnS	získat
hodnoty	hodnota	k1gFnPc4	hodnota
hustoty	hustota	k1gFnSc2	hustota
<g/>
,	,	kIx,	,
složení	složení	k1gNnSc2	složení
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
změřila	změřit	k5eAaPmAgFnS	změřit
hmotnost	hmotnost	k1gFnSc4	hmotnost
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
záložně	záložně	k6eAd1	záložně
přesměrován	přesměrovat	k5eAaPmNgInS	přesměrovat
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
těsného	těsný	k2eAgInSc2d1	těsný
průletu	průlet	k1gInSc2	průlet
kolem	kolem	k7c2	kolem
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
kolem	kolem	k7c2	kolem
Titanu	titan	k1gInSc2	titan
neproletěl	proletět	k5eNaPmAgInS	proletět
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dál	daleko	k6eAd2	daleko
k	k	k7c3	k
planetám	planeta	k1gFnPc3	planeta
Uran	Uran	k1gInSc4	Uran
a	a	k8xC	a
Neptun	Neptun	k1gInSc4	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Opar	opar	k1gInSc1	opar
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
zabránil	zabránit	k5eAaPmAgInS	zabránit
sondě	sonda	k1gFnSc3	sonda
zmapovat	zmapovat	k5eAaPmF	zmapovat
terén	terén	k1gInSc4	terén
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
digitalizaci	digitalizace	k1gFnSc6	digitalizace
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
<g/>
,	,	kIx,	,
pořízených	pořízený	k2eAgFnPc6d1	pořízená
sondou	sonda	k1gFnSc7	sonda
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
přes	přes	k7c4	přes
vestavěný	vestavěný	k2eAgInSc4d1	vestavěný
oranžový	oranžový	k2eAgInSc4d1	oranžový
filtr	filtr	k1gInSc4	filtr
<g/>
,	,	kIx,	,
viditelné	viditelný	k2eAgFnPc1d1	viditelná
světlé	světlý	k2eAgFnPc1d1	světlá
a	a	k8xC	a
tmavé	tmavý	k2eAgFnPc1d1	tmavá
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
známé	známý	k2eAgInPc4d1	známý
pod	pod	k7c7	pod
názvy	název	k1gInPc7	název
Xanadu	Xanad	k1gInSc2	Xanad
a	a	k8xC	a
Shangri-La	Shangri-L	k1gInSc2	Shangri-L
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
prvně	prvně	k?	prvně
zpozorovány	zpozorován	k2eAgMnPc4d1	zpozorován
Hubbleovým	Hubbleův	k2eAgInSc7d1	Hubbleův
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
teleskopem	teleskop	k1gInSc7	teleskop
při	při	k7c6	při
snímání	snímání	k1gNnSc6	snímání
v	v	k7c6	v
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
spektru	spektrum	k1gNnSc6	spektrum
již	již	k6eAd1	již
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
a	a	k8xC	a
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
totiž	totiž	k9	totiž
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
jinak	jinak	k6eAd1	jinak
neprůhledné	průhledný	k2eNgFnSc6d1	neprůhledná
atmosféře	atmosféra	k1gFnSc6	atmosféra
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
určitá	určitý	k2eAgNnPc4d1	určité
spektrální	spektrální	k2eAgNnPc4d1	spektrální
okna	okno	k1gNnPc4	okno
a	a	k8xC	a
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
záření	záření	k1gNnSc2	záření
specifické	specifický	k2eAgFnPc4d1	specifická
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
lze	lze	k6eAd1	lze
atmosféru	atmosféra	k1gFnSc4	atmosféra
prozářit	prozářit	k5eAaPmF	prozářit
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
bylo	být	k5eAaImAgNnS	být
použito	použit	k2eAgNnSc1d1	použito
záření	záření	k1gNnSc1	záření
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
938	[number]	k4	938
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
a	a	k8xC	a
Huygens	Huygensa	k1gFnPc2	Huygensa
(	(	kIx(	(
<g/>
sonda	sonda	k1gFnSc1	sonda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
data	datum	k1gNnPc4	datum
získaná	získaný	k2eAgFnSc1d1	získaná
sondami	sonda	k1gFnPc7	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
zůstal	zůstat	k5eAaPmAgInS	zůstat
Titan	titan	k1gInSc1	titan
předmětem	předmět	k1gInSc7	předmět
záhad	záhada	k1gFnPc2	záhada
–	–	k?	–
velký	velký	k2eAgInSc1d1	velký
měsíc	měsíc	k1gInSc1	měsíc
zakrytý	zakrytý	k2eAgInSc1d1	zakrytý
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
detailní	detailní	k2eAgNnSc4d1	detailní
zmapování	zmapování	k1gNnSc4	zmapování
<g/>
.	.	kIx.	.
</s>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Titan	titan	k1gInSc4	titan
obklopovala	obklopovat	k5eAaImAgFnS	obklopovat
již	již	k6eAd1	již
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
objeven	objevit	k5eAaPmNgInS	objevit
Christiaanem	Christiaan	k1gInSc7	Christiaan
Huygensem	Huygens	k1gInSc7	Huygens
a	a	k8xC	a
Giovannim	Giovanni	k1gNnSc7	Giovanni
Cassinim	Cassinima	k1gFnPc2	Cassinima
<g/>
,	,	kIx,	,
odhalila	odhalit	k5eAaPmAgFnS	odhalit
až	až	k9	až
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
po	po	k7c6	po
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
<g/>
.	.	kIx.	.
</s>
<s>
Společná	společný	k2eAgFnSc1d1	společná
mise	mise	k1gFnSc1	mise
Evropské	evropský	k2eAgFnSc2d1	Evropská
kosmické	kosmický	k2eAgFnSc2d1	kosmická
agentury	agentura	k1gFnSc2	agentura
(	(	kIx(	(
<g/>
ESA	eso	k1gNnSc2	eso
<g/>
)	)	kIx)	)
a	a	k8xC	a
NASA	NASA	kA	NASA
Cassini-Huygens	Cassini-Huygens	k1gInSc1	Cassini-Huygens
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
dorazila	dorazit	k5eAaPmAgFnS	dorazit
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2004	[number]	k4	2004
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
radarem	radar	k1gInSc7	radar
mapovat	mapovat	k5eAaImF	mapovat
planetární	planetární	k2eAgInSc4d1	planetární
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
pouhých	pouhý	k2eAgInPc2d1	pouhý
1	[number]	k4	1
200	[number]	k4	200
kilometrů	kilometr	k1gInPc2	kilometr
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
pořídila	pořídit	k5eAaPmAgFnS	pořídit
snímky	snímka	k1gFnPc4	snímka
Titanu	titan	k1gInSc2	titan
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
vyšším	vysoký	k2eAgNnSc7d2	vyšší
než	než	k8xS	než
kdy	kdy	k6eAd1	kdy
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Zachytila	zachytit	k5eAaPmAgFnS	zachytit
tmavé	tmavý	k2eAgFnPc4d1	tmavá
a	a	k8xC	a
světlé	světlý	k2eAgFnPc4d1	světlá
oblasti	oblast	k1gFnPc4	oblast
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
by	by	kYmCp3nP	by
lidskému	lidský	k2eAgInSc3d1	lidský
oku	oko	k1gNnSc6	oko
zůstaly	zůstat	k5eAaPmAgInP	zůstat
skryty	skryt	k1gInPc1	skryt
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
podnikla	podniknout	k5eAaPmAgFnS	podniknout
první	první	k4xOgFnSc1	první
cílený	cílený	k2eAgInSc1d1	cílený
velmi	velmi	k6eAd1	velmi
blízký	blízký	k2eAgInSc1d1	blízký
průlet	průlet	k1gInSc1	průlet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
950	[number]	k4	950
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
<g/>
;	;	kIx,	;
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
průlet	průlet	k1gInSc4	průlet
vůbec	vůbec	k9	vůbec
podnikla	podniknout	k5eAaPmAgFnS	podniknout
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
880	[number]	k4	880
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
provedla	provést	k5eAaPmAgFnS	provést
sonda	sonda	k1gFnSc1	sonda
vetší	vetšet	k5eAaImIp3nS	vetšet
množství	množství	k1gNnSc4	množství
průletů	průlet	k1gInPc2	průlet
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
podnikla	podniknout	k5eAaPmAgFnS	podniknout
Cassini	Cassin	k2eAgMnPc1d1	Cassin
svoje	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgMnSc1d1	poslední
<g/>
,	,	kIx,	,
127	[number]	k4	127
<g/>
.	.	kIx.	.
přiblížení	přiblížení	k1gNnSc6	přiblížení
se	se	k3xPyFc4	se
k	k	k7c3	k
Titanu	titan	k1gInSc3	titan
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
něho	on	k3xPp3gMnSc2	on
získávala	získávat	k5eAaImAgFnS	získávat
radarovým	radarový	k2eAgNnSc7d1	radarové
měřením	měření	k1gNnSc7	měření
data	datum	k1gNnSc2	datum
o	o	k7c6	o
hloubce	hloubka	k1gFnSc6	hloubka
menších	malý	k2eAgNnPc2d2	menší
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
sledovala	sledovat	k5eAaImAgFnS	sledovat
změny	změna	k1gFnPc4	změna
na	na	k7c6	na
ostatních	ostatní	k2eAgNnPc6d1	ostatní
jezerech	jezero	k1gNnPc6	jezero
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
prohlédla	prohlédnout	k5eAaPmAgFnS	prohlédnout
tzv.	tzv.	kA	tzv.
kouzelný	kouzelný	k2eAgInSc4d1	kouzelný
ostrov	ostrov	k1gInSc4	ostrov
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
Ligeia	Ligeius	k1gMnSc2	Ligeius
Mare	Mar	k1gMnSc2	Mar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
sledovala	sledovat	k5eAaImAgFnS	sledovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
sonda	sonda	k1gFnSc1	sonda
využije	využít	k5eAaPmIp3nS	využít
Titanovy	Titanovy	k?	Titanovy
gravitace	gravitace	k1gFnSc1	gravitace
a	a	k8xC	a
nasměruje	nasměrovat	k5eAaPmIp3nS	nasměrovat
se	se	k3xPyFc4	se
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
směřující	směřující	k2eAgNnSc1d1	směřující
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
mezi	mezi	k7c7	mezi
horními	horní	k2eAgFnPc7d1	horní
vrstvami	vrstva	k1gFnPc7	vrstva
jeho	jeho	k3xOp3gFnSc2	jeho
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
prstenci	prstenec	k1gInSc6	prstenec
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ještě	ještě	k6eAd1	ještě
žádná	žádný	k3yNgFnSc1	žádný
sonda	sonda	k1gFnSc1	sonda
nezkoumala	zkoumat	k5eNaImAgFnS	zkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gInSc1	Saturn
takto	takto	k6eAd1	takto
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
22	[number]	k4	22
<g/>
krát	krát	k6eAd1	krát
<g/>
,	,	kIx,	,
aby	aby	k9	aby
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2017	[number]	k4	2017
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
misi	mise	k1gFnSc4	mise
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
hladce	hladko	k6eAd1	hladko
přistála	přistát	k5eAaPmAgFnS	přistát
sonda	sonda	k1gFnSc1	sonda
Huygens	Huygensa	k1gFnPc2	Huygensa
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
ji	on	k3xPp3gFnSc4	on
nesla	nést	k5eAaImAgFnS	nést
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
Titan	titan	k1gInSc1	titan
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
stal	stát	k5eAaPmAgInS	stát
nejvzdálenějším	vzdálený	k2eAgInSc7d3	nejvzdálenější
objektem	objekt	k1gInSc7	objekt
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
přistála	přistát	k5eAaPmAgFnS	přistát
lidmi	člověk	k1gMnPc7	člověk
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
operace	operace	k1gFnSc1	operace
probíhala	probíhat	k5eAaImAgFnS	probíhat
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
rovněž	rovněž	k9	rovněž
první	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
přistávací	přistávací	k2eAgInSc4d1	přistávací
manévr	manévr	k1gInSc4	manévr
ve	v	k7c6	v
vnější	vnější	k2eAgFnSc6d1	vnější
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
přistání	přistání	k1gNnSc6	přistání
sonda	sonda	k1gFnSc1	sonda
neobjevila	objevit	k5eNaPmAgFnS	objevit
žádný	žádný	k3yNgInSc4	žádný
důkaz	důkaz	k1gInSc4	důkaz
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
kapaliny	kapalina	k1gFnSc2	kapalina
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
temná	temný	k2eAgFnSc1d1	temná
planina	planina	k1gFnSc1	planina
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přistála	přistát	k5eAaImAgFnS	přistát
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
zatopena	zatopen	k2eAgFnSc1d1	zatopena
<g/>
.	.	kIx.	.
</s>
<s>
Přistávací	přistávací	k2eAgInSc1d1	přistávací
modul	modul	k1gInSc1	modul
Huygens	Huygensa	k1gFnPc2	Huygensa
dosedl	dosednout	k5eAaPmAgInS	dosednout
pod	pod	k7c4	pod
nejvýchodnější	východní	k2eAgInSc4d3	nejvýchodnější
cíp	cíp	k1gInSc4	cíp
světlé	světlý	k2eAgFnSc2d1	světlá
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
Adiri	Adiri	k1gNnSc7	Adiri
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
192,4	[number]	k4	192,4
<g/>
°	°	k?	°
západní	západní	k2eAgFnSc2d1	západní
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
10,2	[number]	k4	10,2
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
přistání	přistání	k1gNnSc2	přistání
vyfotografovala	vyfotografovat	k5eAaPmAgFnS	vyfotografovat
bílé	bílý	k2eAgInPc4d1	bílý
kopce	kopec	k1gInPc4	kopec
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
úbočí	úbočí	k1gNnSc6	úbočí
se	se	k3xPyFc4	se
dolů	dolů	k6eAd1	dolů
na	na	k7c4	na
temnou	temný	k2eAgFnSc4d1	temná
planinu	planina	k1gFnSc4	planina
svažovaly	svažovat	k5eAaImAgInP	svažovat
tmavé	tmavý	k2eAgInPc1d1	tmavý
čárové	čárový	k2eAgInPc1d1	čárový
útvary	útvar	k1gInPc1	útvar
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
řeky	řeka	k1gFnSc2	řeka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
primárně	primárně	k6eAd1	primárně
o	o	k7c4	o
atmosférickou	atmosférický	k2eAgFnSc4d1	atmosférická
sondu	sonda	k1gFnSc4	sonda
<g/>
,	,	kIx,	,
pracovala	pracovat	k5eAaImAgFnS	pracovat
ještě	ještě	k9	ještě
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
hodinu	hodina	k1gFnSc4	hodina
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
předávala	předávat	k5eAaImAgFnS	předávat
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
sondu	sonda	k1gFnSc4	sonda
Zemi	zem	k1gFnSc4	zem
informace	informace	k1gFnSc2	informace
a	a	k8xC	a
data	datum	k1gNnSc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
kopce	kopec	k1gInPc1	kopec
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
vyfotila	vyfotit	k5eAaPmAgFnS	vyfotit
snímek	snímek	k1gInSc4	snímek
tmavé	tmavý	k2eAgFnSc2d1	tmavá
planiny	planina	k1gFnSc2	planina
Shangri-La	Shangri-L	k1gInSc2	Shangri-L
<g/>
,	,	kIx,	,
pokryté	pokrytý	k2eAgInPc1d1	pokrytý
malými	malý	k2eAgInPc7d1	malý
kameny	kámen	k1gInPc7	kámen
a	a	k8xC	a
oblázky	oblázek	k1gInPc7	oblázek
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
kvůli	kvůli	k7c3	kvůli
softwarové	softwarový	k2eAgFnSc3d1	softwarová
chybě	chyba	k1gFnSc3	chyba
se	se	k3xPyFc4	se
ztratila	ztratit	k5eAaPmAgNnP	ztratit
data	datum	k1gNnPc1	datum
vysílaná	vysílaný	k2eAgNnPc1d1	vysílané
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
vysílačů	vysílač	k1gInPc2	vysílač
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
především	především	k9	především
o	o	k7c4	o
část	část	k1gFnSc4	část
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
větru	vítr	k1gInSc2	vítr
při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
vědci	vědec	k1gMnPc1	vědec
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
ESA	eso	k1gNnSc2	eso
a	a	k8xC	a
COSPAR	COSPAR	kA	COSPAR
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
a	a	k8xC	a
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
místo	místo	k7c2	místo
přistání	přistání	k1gNnSc2	přistání
sondy	sonda	k1gFnSc2	sonda
po	po	k7c6	po
zemřelém	zemřelý	k2eAgMnSc6d1	zemřelý
prezidentovi	prezident	k1gMnSc6	prezident
ESA	eso	k1gNnSc2	eso
Hubertu	Hubert	k1gMnSc3	Hubert
Curienovi	Curien	k1gMnSc3	Curien
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byly	být	k5eAaImAgFnP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
ESA	eso	k1gNnSc2	eso
i	i	k8xC	i
JPL	JPL	kA	JPL
koncepty	koncept	k1gInPc7	koncept
misí	mise	k1gFnPc2	mise
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dopravení	dopravení	k1gNnSc4	dopravení
další	další	k2eAgNnSc4d1	další
robotické	robotický	k2eAgFnPc4d1	robotická
sondy	sonda	k1gFnPc4	sonda
k	k	k7c3	k
Titanu	titan	k1gInSc3	titan
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
návrhů	návrh	k1gInPc2	návrh
však	však	k9	však
nezískal	získat	k5eNaPmAgInS	získat
schválení	schválení	k1gNnSc4	schválení
finančně	finančně	k6eAd1	finančně
podporované	podporovaný	k2eAgFnSc2d1	podporovaná
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
Saturn	Saturn	k1gInSc1	Saturn
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
(	(	kIx(	(
<g/>
TSSM	TSSM	kA	TSSM
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
návrh	návrh	k1gInSc1	návrh
společné	společný	k2eAgFnSc2d1	společná
mise	mise	k1gFnSc2	mise
NASA	NASA	kA	NASA
a	a	k8xC	a
ESA	eso	k1gNnSc2	eso
na	na	k7c4	na
průzkum	průzkum	k1gInSc4	průzkum
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gInSc1	Saturn
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
měsíců	měsíc	k1gInPc2	měsíc
Titan	titan	k1gInSc4	titan
a	a	k8xC	a
Enceladus	Enceladus	k1gInSc4	Enceladus
<g/>
.	.	kIx.	.
</s>
<s>
Vizí	vize	k1gFnSc7	vize
projektu	projekt	k1gInSc2	projekt
byla	být	k5eAaImAgFnS	být
i	i	k9	i
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
na	na	k7c6	na
horkovzdušném	horkovzdušný	k2eAgInSc6d1	horkovzdušný
balónu	balón	k1gInSc6	balón
šest	šest	k4xCc1	šest
měsíců	měsíc	k1gInPc2	měsíc
studovala	studovat	k5eAaImAgFnS	studovat
atmosféru	atmosféra	k1gFnSc4	atmosféra
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
však	však	k9	však
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
konkurenční	konkurenční	k2eAgFnSc1d1	konkurenční
mise	mise	k1gFnSc1	mise
Europa	Europa	k1gFnSc1	Europa
Jupiter	Jupiter	k1gInSc4	Jupiter
System	Syst	k1gMnSc7	Syst
Mission	Mission	k1gInSc4	Mission
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
výzkum	výzkum	k1gInSc1	výzkum
měsíčního	měsíční	k2eAgInSc2d1	měsíční
systému	systém	k1gInSc2	systém
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
a	a	k8xC	a
TSSM	TSSM	kA	TSSM
byla	být	k5eAaImAgFnS	být
odložena	odložit	k5eAaPmNgFnS	odložit
na	na	k7c4	na
neurčito	neurčito	k1gNnSc4	neurčito
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
navrhovanou	navrhovaný	k2eAgFnSc7d1	navrhovaná
misí	mise	k1gFnSc7	mise
bylo	být	k5eAaImAgNnS	být
vyslání	vyslání	k1gNnSc1	vyslání
přistávacího	přistávací	k2eAgInSc2d1	přistávací
modulu	modul	k1gInSc2	modul
Titan	titan	k1gInSc1	titan
Mare	Mare	k1gFnSc1	Mare
Explorer	Explorer	k1gMnSc1	Explorer
(	(	kIx(	(
<g/>
TiME	TiME	k1gMnSc1	TiME
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
přistál	přistát	k5eAaImAgInS	přistát
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
některého	některý	k3yIgNnSc2	některý
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
by	by	kYmCp3nS	by
studoval	studovat	k5eAaImAgMnS	studovat
jeho	jeho	k3xOp3gNnSc4	jeho
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Jason	Jason	k1gMnSc1	Jason
Barnes	Barnes	k1gMnSc1	Barnes
z	z	k7c2	z
University	universita	k1gFnSc2	universita
of	of	k?	of
Idaho	Ida	k1gMnSc4	Ida
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
grant	grant	k1gInSc4	grant
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
715	[number]	k4	715
milionu	milion	k4xCgInSc2	milion
amerických	americký	k2eAgMnPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
projekt	projekt	k1gInSc4	projekt
dronu	dron	k1gInSc2	dron
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
pořídil	pořídit	k5eAaPmAgInS	pořídit
snímky	snímek	k1gInPc4	snímek
povrchu	povrch	k1gInSc2	povrch
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
ovšem	ovšem	k9	ovšem
podporu	podpora	k1gFnSc4	podpora
této	tento	k3xDgFnSc3	tento
misi	mise	k1gFnSc3	mise
neschválila	schválit	k5eNaPmAgFnS	schválit
a	a	k8xC	a
osud	osud	k1gInSc1	osud
projektu	projekt	k1gInSc2	projekt
zůstal	zůstat	k5eAaPmAgInS	zůstat
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
společný	společný	k2eAgInSc1d1	společný
projekt	projekt	k1gInSc1	projekt
španělské	španělský	k2eAgFnSc2d1	španělská
firmy	firma	k1gFnSc2	firma
SENER	SENER	kA	SENER
a	a	k8xC	a
Centra	centrum	k1gNnSc2	centrum
astrobiologie	astrobiologie	k1gFnSc2	astrobiologie
(	(	kIx(	(
<g/>
Centro	Centro	k1gNnSc1	Centro
de	de	k?	de
Astrobiología	Astrobiologí	k1gInSc2	Astrobiologí
<g/>
)	)	kIx)	)
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
na	na	k7c6	na
vyslání	vyslání	k1gNnSc6	vyslání
přistávacího	přistávací	k2eAgInSc2d1	přistávací
modulu	modul	k1gInSc2	modul
Titan	titan	k1gInSc1	titan
Lake	Lak	k1gFnSc2	Lak
In-situ	Init	k1gInSc2	In-sit
Sampling	Sampling	k1gInSc1	Sampling
Propelled	Propelled	k1gMnSc1	Propelled
Explorer	Explorer	k1gMnSc1	Explorer
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
rovněž	rovněž	k9	rovněž
dosednout	dosednout	k5eAaPmF	dosednout
na	na	k7c4	na
jezero	jezero	k1gNnSc4	jezero
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
návrhu	návrh	k1gInSc3	návrh
TiME	TiME	k1gFnSc2	TiME
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
vlastním	vlastní	k2eAgInSc7d1	vlastní
pohonným	pohonný	k2eAgInSc7d1	pohonný
systémem	systém	k1gInSc7	systém
a	a	k8xC	a
nebyl	být	k5eNaImAgMnS	být
by	by	kYmCp3nS	by
odkázán	odkázán	k2eAgMnSc1d1	odkázán
na	na	k7c4	na
nekontrolované	kontrolovaný	k2eNgNnSc4d1	nekontrolované
plachtění	plachtění	k1gNnSc4	plachtění
po	po	k7c6	po
hladině	hladina	k1gFnSc6	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
program	program	k1gInSc1	program
Journey	Journea	k1gFnSc2	Journea
to	ten	k3xDgNnSc1	ten
Enceladus	Enceladus	k1gInSc1	Enceladus
and	and	k?	and
Titan	titan	k1gInSc1	titan
(	(	kIx(	(
<g/>
JET	jet	k2eAgInSc1d1	jet
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
na	na	k7c6	na
vyslání	vyslání	k1gNnSc6	vyslání
astrobiologické	astrobiologický	k2eAgFnSc2d1	astrobiologický
sondy	sonda	k1gFnSc2	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
zkoumat	zkoumat	k5eAaImF	zkoumat
možné	možný	k2eAgFnPc4d1	možná
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
život	život	k1gInSc4	život
a	a	k8xC	a
osídlení	osídlení	k1gNnSc4	osídlení
obou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
navržen	navržen	k2eAgInSc1d1	navržen
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
coby	coby	k?	coby
13	[number]	k4	13
<g/>
.	.	kIx.	.
mise	mise	k1gFnSc2	mise
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nízkorozpočtového	nízkorozpočtový	k2eAgInSc2d1	nízkorozpočtový
programu	program	k1gInSc2	program
Discovery	Discovera	k1gFnSc2	Discovera
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
se	se	k3xPyFc4	se
ale	ale	k9	ale
nedostal	dostat	k5eNaPmAgInS	dostat
ani	ani	k8xC	ani
do	do	k7c2	do
užšího	úzký	k2eAgInSc2d2	užší
výběru	výběr	k1gInSc2	výběr
pěti	pět	k4xCc3	pět
misí	mise	k1gFnPc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
grant	grant	k1gInSc1	grant
NASA	NASA	kA	NASA
<g/>
/	/	kIx~	/
<g/>
Phase	Phas	k1gMnSc2	Phas
II	II	kA	II
pro	pro	k7c4	pro
návrh	návrh	k1gInSc4	návrh
projektu	projekt	k1gInSc2	projekt
ponorky	ponorka	k1gFnSc2	ponorka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgNnP	mít
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
jezera	jezero	k1gNnPc1	jezero
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	k9	by
součástí	součást	k1gFnSc7	součást
mise	mise	k1gFnSc2	mise
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
dopravení	dopravení	k1gNnSc1	dopravení
ponorky	ponorka	k1gFnSc2	ponorka
a	a	k8xC	a
plovoucího	plovoucí	k2eAgNnSc2d1	plovoucí
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
navrhovaný	navrhovaný	k2eAgMnSc1d1	navrhovaný
TiME	TiME	k1gMnSc1	TiME
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostředí	prostředí	k1gNnSc1	prostředí
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
biogenních	biogenní	k2eAgFnPc2d1	biogenní
komplexních	komplexní	k2eAgFnPc2d1	komplexní
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
podzemní	podzemní	k2eAgInSc1d1	podzemní
oceán	oceán	k1gInSc1	oceán
vody	voda	k1gFnSc2	voda
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
vhodný	vhodný	k2eAgInSc4d1	vhodný
ekosystém	ekosystém	k1gInSc4	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini-Huygens	Cassini-Huygensa	k1gFnPc2	Cassini-Huygensa
nebyla	být	k5eNaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
technickými	technický	k2eAgInPc7d1	technický
prostředky	prostředek	k1gInPc7	prostředek
pro	pro	k7c4	pro
detekci	detekce	k1gFnSc4	detekce
látek	látka	k1gFnPc2	látka
dokazujících	dokazující	k2eAgInPc2d1	dokazující
přítomnost	přítomnost	k1gFnSc4	přítomnost
života	život	k1gInSc2	život
nebo	nebo	k8xC	nebo
složitějších	složitý	k2eAgFnPc2d2	složitější
organických	organický	k2eAgFnPc2d1	organická
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
výsledky	výsledek	k1gInPc4	výsledek
jejích	její	k3xOp3gNnPc2	její
pozorování	pozorování	k1gNnPc2	pozorování
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podmínky	podmínka	k1gFnPc1	podmínka
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
jsou	být	k5eAaImIp3nP	být
obdobné	obdobný	k2eAgFnPc1d1	obdobná
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc1	jaký
panovaly	panovat	k5eAaImAgFnP	panovat
na	na	k7c6	na
pravěké	pravěký	k2eAgFnSc6d1	pravěká
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
atmosféra	atmosféra	k1gFnSc1	atmosféra
Titanu	titan	k1gInSc2	titan
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
prvotní	prvotní	k2eAgFnSc3d1	prvotní
atmosféře	atmosféra	k1gFnSc3	atmosféra
Země	zem	k1gFnSc2	zem
s	s	k7c7	s
jediným	jediný	k2eAgInSc7d1	jediný
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
chybějící	chybějící	k2eAgFnSc1d1	chybějící
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
.	.	kIx.	.
</s>
<s>
Millerův-Ureyův	Millerův-Ureyův	k2eAgInSc1d1	Millerův-Ureyův
experiment	experiment	k1gInSc1	experiment
a	a	k8xC	a
pokusy	pokus	k1gInPc1	pokus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
vycházely	vycházet	k5eAaImAgFnP	vycházet
<g/>
,	,	kIx,	,
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
podobné	podobný	k2eAgFnSc6d1	podobná
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
disponuje	disponovat	k5eAaBmIp3nS	disponovat
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vlivem	vliv	k1gInSc7	vliv
UV	UV	kA	UV
záření	záření	k1gNnSc2	záření
vyvolány	vyvolán	k2eAgFnPc1d1	vyvolána
reakce	reakce	k1gFnPc1	reakce
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
produktem	produkt	k1gInSc7	produkt
je	být	k5eAaImIp3nS	být
syntéza	syntéza	k1gFnSc1	syntéza
složitějších	složitý	k2eAgFnPc2d2	složitější
či	či	k8xC	či
polymerních	polymerní	k2eAgFnPc2d1	polymerní
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tholinů	tholin	k1gInPc2	tholin
<g/>
.	.	kIx.	.
</s>
<s>
Spouštěčem	spouštěč	k1gInSc7	spouštěč
reakce	reakce	k1gFnSc2	reakce
je	být	k5eAaImIp3nS	být
disociace	disociace	k1gFnSc1	disociace
molekul	molekula	k1gFnPc2	molekula
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
methanu	methan	k1gInSc2	methan
na	na	k7c4	na
radikály	radikál	k1gInPc4	radikál
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vznikají	vznikat	k5eAaImIp3nP	vznikat
kyanovodík	kyanovodík	k1gInSc1	kyanovodík
a	a	k8xC	a
acetylen	acetylen	k1gInSc1	acetylen
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgFnPc1d1	následná
reakce	reakce	k1gFnPc1	reakce
byly	být	k5eAaImAgFnP	být
podrobeny	podroben	k2eAgInPc1d1	podroben
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
studiím	studio	k1gNnPc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc1	směs
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Titanově	Titanův	k2eAgFnSc6d1	Titanova
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
vystavena	vystaven	k2eAgNnPc1d1	vystaveno
dávkám	dávka	k1gFnPc3	dávka
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
produktem	produkt	k1gInSc7	produkt
reakcí	reakce	k1gFnPc2	reakce
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgInPc2	všecek
pěti	pět	k4xCc2	pět
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
stavební	stavební	k2eAgInPc1d1	stavební
kameny	kámen	k1gInPc1	kámen
RNA	RNA	kA	RNA
a	a	k8xC	a
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
(	(	kIx(	(
<g/>
základní	základní	k2eAgInPc1d1	základní
stavební	stavební	k2eAgInPc1d1	stavební
prvky	prvek	k1gInPc1	prvek
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
pokusech	pokus	k1gInPc6	pokus
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
připraveny	připravit	k5eAaPmNgFnP	připravit
tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
za	za	k7c4	za
nepřitomnosti	nepřitomnost	k1gFnPc4	nepřitomnost
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
oznámili	oznámit	k5eAaPmAgMnP	oznámit
vědci	vědec	k1gMnPc1	vědec
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
že	že	k8xS	že
dle	dle	k7c2	dle
simulací	simulace	k1gFnSc7	simulace
atmosféry	atmosféra	k1gFnSc2	atmosféra
Titanu	titan	k1gInSc2	titan
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
mohly	moct	k5eAaImAgFnP	moct
nacházet	nacházet	k5eAaImF	nacházet
složitější	složitý	k2eAgFnPc1d2	složitější
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
laboratorních	laboratorní	k2eAgFnPc2d1	laboratorní
simulací	simulace	k1gFnPc2	simulace
se	se	k3xPyFc4	se
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
nachází	nacházet	k5eAaImIp3nS	nacházet
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
organického	organický	k2eAgInSc2d1	organický
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
započata	započat	k2eAgFnSc1d1	započata
chemická	chemický	k2eAgFnSc1d1	chemická
evoluce	evoluce	k1gFnSc1	evoluce
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
stála	stát	k5eAaImAgFnS	stát
za	za	k7c7	za
vznikem	vznik	k1gInSc7	vznik
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Předpokladem	předpoklad	k1gInSc7	předpoklad
této	tento	k3xDgFnSc2	tento
analogie	analogie	k1gFnSc2	analogie
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
pod	pod	k7c7	pod
vrstvami	vrstva	k1gFnPc7	vrstva
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zamrzla	zamrznout	k5eAaPmAgFnS	zamrznout
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
impaktního	impaktní	k2eAgNnSc2d1	impaktní
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
teorie	teorie	k1gFnSc1	teorie
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
oceánu	oceán	k1gInSc2	oceán
kapalného	kapalný	k2eAgInSc2d1	kapalný
amoniaku	amoniak	k1gInSc2	amoniak
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
až	až	k9	až
200	[number]	k4	200
km	km	kA	km
oceán	oceán	k1gInSc1	oceán
se	s	k7c7	s
směsí	směs	k1gFnSc7	směs
amoniaku	amoniak	k1gInSc2	amoniak
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
oceánech	oceán	k1gInPc6	oceán
mohl	moct	k5eAaImAgMnS	moct
existovat	existovat	k5eAaImF	existovat
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
by	by	kYmCp3nP	by
takové	takový	k3xDgFnPc1	takový
podmínky	podmínka	k1gFnPc1	podmínka
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
extrémní	extrémní	k2eAgFnPc4d1	extrémní
<g/>
.	.	kIx.	.
</s>
<s>
Přestup	přestup	k1gInSc1	přestup
tepla	teplo	k1gNnSc2	teplo
mezi	mezi	k7c7	mezi
vnitřkem	vnitřek	k1gInSc7	vnitřek
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
vnějšími	vnější	k2eAgFnPc7d1	vnější
vrstvami	vrstva	k1gFnPc7	vrstva
by	by	k9	by
mohl	moct	k5eAaImAgInS	moct
představovat	představovat	k5eAaImF	představovat
zásadní	zásadní	k2eAgFnSc4d1	zásadní
podmínku	podmínka	k1gFnSc4	podmínka
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Detekce	detekce	k1gFnSc1	detekce
mikrobiálního	mikrobiální	k2eAgInSc2d1	mikrobiální
života	život	k1gInSc2	život
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
biogenním	biogenní	k2eAgNnSc6d1	biogenní
působení	působení	k1gNnSc6	působení
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zkoumáno	zkoumán	k2eAgNnSc1d1	zkoumáno
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
methan	methan	k1gInSc1	methan
a	a	k8xC	a
dusík	dusík	k1gInSc1	dusík
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
nemohly	moct	k5eNaImAgFnP	moct
být	být	k5eAaImF	být
biologického	biologický	k2eAgInSc2d1	biologický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejnit	k5eAaPmNgFnP	zveřejnit
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
informovaly	informovat	k5eAaBmAgFnP	informovat
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
existence	existence	k1gFnSc2	existence
života	život	k1gInSc2	život
v	v	k7c6	v
methanových	methanův	k2eAgNnPc6d1	methanův
jezerech	jezero	k1gNnPc6	jezero
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
,	,	kIx,	,
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c6	na
alternativních	alternativní	k2eAgInPc6d1	alternativní
biochemických	biochemický	k2eAgInPc6d1	biochemický
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
analogických	analogický	k2eAgFnPc6d1	analogická
životu	život	k1gInSc3	život
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
organismy	organismus	k1gInPc1	organismus
by	by	kYmCp3nP	by
místo	místo	k7c2	místo
kyslíku	kyslík	k1gInSc2	kyslík
dýchaly	dýchat	k5eAaImAgFnP	dýchat
vodík	vodík	k1gInSc4	vodík
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nP	by
místo	místo	k1gNnSc4	místo
do	do	k7c2	do
glukózy	glukóza	k1gFnSc2	glukóza
metabolizovaly	metabolizovat	k5eAaImAgInP	metabolizovat
na	na	k7c4	na
acetylen	acetylen	k1gInSc4	acetylen
<g/>
,	,	kIx,	,
a	a	k8xC	a
vydechovaly	vydechovat	k5eAaImAgInP	vydechovat
methan	methan	k1gInSc4	methan
namísto	namísto	k7c2	namísto
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
živé	živý	k2eAgFnPc1d1	živá
formy	forma	k1gFnPc1	forma
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
rozpouštědlo	rozpouštědlo	k1gNnSc4	rozpouštědlo
vodu	voda	k1gFnSc4	voda
<g/>
;	;	kIx,	;
organismy	organismus	k1gInPc1	organismus
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
využít	využít	k5eAaPmF	využít
kapalné	kapalný	k2eAgInPc1d1	kapalný
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
,	,	kIx,	,
např.	např.	kA	např.
methan	methan	k1gInSc1	methan
nebo	nebo	k8xC	nebo
ethan	ethan	k1gInSc1	ethan
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
silnější	silný	k2eAgNnSc4d2	silnější
rozpouštědlo	rozpouštědlo	k1gNnSc4	rozpouštědlo
než	než	k8xS	než
methan	methan	k1gInSc4	methan
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
chemicky	chemicky	k6eAd1	chemicky
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
hydrolýzou	hydrolýza	k1gFnSc7	hydrolýza
rozbít	rozbít	k5eAaPmF	rozbít
velké	velký	k2eAgFnPc4d1	velká
organické	organický	k2eAgFnPc4d1	organická
molekuly	molekula	k1gFnPc4	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Forma	forma	k1gFnSc1	forma
života	život	k1gInSc2	život
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
uhlovodíkovém	uhlovodíkový	k2eAgNnSc6d1	uhlovodíkové
rozpouštědle	rozpouštědlo	k1gNnSc6	rozpouštědlo
by	by	kYmCp3nS	by
těmto	tento	k3xDgNnPc3	tento
problémům	problém	k1gInPc3	problém
rozpadu	rozpad	k1gInSc2	rozpad
biomolekul	biomolekula	k1gFnPc2	biomolekula
nemusela	muset	k5eNaImAgFnS	muset
čelit	čelit	k5eAaImF	čelit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
astrobiolog	astrobiolog	k1gMnSc1	astrobiolog
Chris	Chris	k1gFnSc4	Chris
McKay	McKaa	k1gFnSc2	McKaa
přednesl	přednést	k5eAaPmAgMnS	přednést
argument	argument	k1gInSc4	argument
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
pokud	pokud	k6eAd1	pokud
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
přítomna	přítomno	k1gNnSc2	přítomno
nějaká	nějaký	k3yIgFnSc1	nějaký
forma	forma	k1gFnSc1	forma
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
mít	mít	k5eAaImF	mít
měřitelný	měřitelný	k2eAgInSc4d1	měřitelný
efekt	efekt	k1gInSc4	efekt
na	na	k7c4	na
složení	složení	k1gNnSc4	složení
troposféry	troposféra	k1gFnSc2	troposféra
měsíce	měsíc	k1gInSc2	měsíc
<g/>
;	;	kIx,	;
koncentrace	koncentrace	k1gFnSc1	koncentrace
molekulárního	molekulární	k2eAgInSc2d1	molekulární
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
acetylenu	acetylen	k1gInSc2	acetylen
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
objevil	objevit	k5eAaPmAgMnS	objevit
Darrell	Darrell	k1gMnSc1	Darrell
Strobel	Strobel	k1gMnSc1	Strobel
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
z	z	k7c2	z
Univerzity	univerzita	k1gFnSc2	univerzita
Johnse	Johns	k1gMnSc2	Johns
Hopkinse	Hopkins	k1gMnSc2	Hopkins
<g/>
,	,	kIx,	,
nadbytek	nadbytek	k1gInSc1	nadbytek
molekulárního	molekulární	k2eAgInSc2d1	molekulární
vodíku	vodík	k1gInSc2	vodík
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
oproti	oproti	k7c3	oproti
vrstvám	vrstva	k1gFnPc3	vrstva
nižším	nízký	k2eAgFnPc3d2	nižší
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
argument	argument	k1gInSc1	argument
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
klesavého	klesavý	k2eAgInSc2d1	klesavý
difuzního	difuzní	k2eAgInSc2d1	difuzní
proudu	proud	k1gInSc2	proud
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
zhruba	zhruba	k6eAd1	zhruba
1028	[number]	k4	1028
molekul	molekula	k1gFnPc2	molekula
vodíku	vodík	k1gInSc2	vodík
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
však	však	k9	však
u	u	k7c2	u
povrchu	povrch	k1gInSc2	povrch
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Strobela	Strobel	k1gMnSc2	Strobel
tyto	tento	k3xDgInPc1	tento
nálezy	nález	k1gInPc1	nález
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
s	s	k7c7	s
McKayovými	McKayův	k2eAgFnPc7d1	McKayův
představami	představa	k1gFnPc7	představa
odezvy	odezva	k1gFnSc2	odezva
přítomných	přítomný	k2eAgFnPc2d1	přítomná
methanogenních	methanogenní	k2eAgFnPc2d1	methanogenní
životních	životní	k2eAgFnPc2d1	životní
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
studie	studie	k1gFnSc1	studie
vydaná	vydaný	k2eAgFnSc1d1	vydaná
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
ukázala	ukázat	k5eAaPmAgFnS	ukázat
nízké	nízký	k2eAgFnPc4d1	nízká
koncentrace	koncentrace	k1gFnPc4	koncentrace
acetylenu	acetylen	k1gInSc2	acetylen
u	u	k7c2	u
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
dle	dle	k7c2	dle
McKayovy	McKayův	k2eAgFnSc2d1	McKayův
interpretace	interpretace	k1gFnSc2	interpretace
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
konzistentní	konzistentní	k2eAgNnSc1d1	konzistentní
s	s	k7c7	s
představou	představa	k1gFnSc7	představa
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
se	se	k3xPyFc4	se
uhlovodíky	uhlovodík	k1gInPc7	uhlovodík
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
živí	živit	k5eAaImIp3nP	živit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
opakované	opakovaný	k2eAgNnSc4d1	opakované
vyjádření	vyjádření	k1gNnSc4	vyjádření
biologické	biologický	k2eAgFnSc2d1	biologická
hypotézy	hypotéza	k1gFnSc2	hypotéza
McKay	McKaa	k1gFnSc2	McKaa
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
možná	možná	k9	možná
i	i	k9	i
jiná	jiný	k2eAgNnPc4d1	jiné
a	a	k8xC	a
pravděpodobnější	pravděpodobný	k2eAgNnSc4d2	pravděpodobnější
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
těchto	tento	k3xDgInPc2	tento
nálezů	nález	k1gInPc2	nález
<g/>
:	:	kIx,	:
dosud	dosud	k6eAd1	dosud
neznámé	známý	k2eNgInPc1d1	neznámý
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
nebo	nebo	k8xC	nebo
chemické	chemický	k2eAgInPc1d1	chemický
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
spotřebovávají	spotřebovávat	k5eAaImIp3nP	spotřebovávat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nebiologické	biologický	k2eNgFnPc1d1	nebiologická
katalytické	katalytický	k2eAgFnPc1d1	katalytická
reakce	reakce	k1gFnPc1	reakce
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
i	i	k9	i
možné	možný	k2eAgFnPc4d1	možná
chyby	chyba	k1gFnPc4	chyba
při	při	k7c6	při
simulacích	simulace	k1gFnPc6	simulace
toku	tok	k1gInSc2	tok
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
acetylenu	acetylen	k1gInSc2	acetylen
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
funkčního	funkční	k2eAgInSc2d1	funkční
nebiologického	biologický	k2eNgInSc2d1	nebiologický
katalytického	katalytický	k2eAgInSc2d1	katalytický
procesu	proces	k1gInSc2	proces
při	při	k7c6	při
95	[number]	k4	95
K	K	kA	K
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
180	[number]	k4	180
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
něho	on	k3xPp3gInSc2	on
byl	být	k5eAaImAgMnS	být
i	i	k9	i
tak	tak	k6eAd1	tak
významný	významný	k2eAgMnSc1d1	významný
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
by	by	kYmCp3nS	by
jistě	jistě	k6eAd1	jistě
nevyvolal	vyvolat	k5eNaPmAgMnS	vyvolat
takovou	takový	k3xDgFnSc4	takový
senzaci	senzace	k1gFnSc4	senzace
jako	jako	k8xS	jako
proces	proces	k1gInSc4	proces
biologický	biologický	k2eAgInSc4d1	biologický
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vyjádření	vyjádření	k1gNnSc2	vyjádření
NASA	NASA	kA	NASA
v	v	k7c6	v
článku	článek	k1gInSc6	článek
z	z	k7c2	z
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
nebyly	být	k5eNaImAgFnP	být
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
datu	datum	k1gNnSc3	datum
nikde	nikde	k6eAd1	nikde
objeveny	objevit	k5eAaPmNgFnP	objevit
formy	forma	k1gFnPc1	forma
života	život	k1gInSc2	život
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
methanu	methan	k1gInSc6	methan
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
hypotetický	hypotetický	k2eAgMnSc1d1	hypotetický
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
vědců	vědec	k1gMnPc2	vědec
tyto	tento	k3xDgFnPc4	tento
chemické	chemický	k2eAgFnPc4d1	chemická
stopy	stopa	k1gFnPc4	stopa
podporují	podporovat	k5eAaImIp3nP	podporovat
argumenty	argument	k1gInPc1	argument
pro	pro	k7c4	pro
výskyt	výskyt	k1gInSc4	výskyt
primitvních	primitvní	k2eAgInPc2d1	primitvní
organismů	organismus	k1gInPc2	organismus
či	či	k8xC	či
předchůdců	předchůdce	k1gMnPc2	předchůdce
živých	živý	k2eAgFnPc2d1	živá
forem	forma	k1gFnPc2	forma
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
vymodelována	vymodelován	k2eAgFnSc1d1	vymodelována
hypotetická	hypotetický	k2eAgFnSc1d1	hypotetická
buněčná	buněčný	k2eAgFnSc1d1	buněčná
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
funkční	funkční	k2eAgFnPc4d1	funkční
v	v	k7c6	v
methanovém	methanový	k2eAgNnSc6d1	methanový
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Sestávala	sestávat	k5eAaImAgFnS	sestávat
by	by	kYmCp3nS	by
malých	malý	k2eAgFnPc2d1	malá
molekul	molekula	k1gFnPc2	molekula
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
vodík	vodík	k1gInSc4	vodík
<g/>
,	,	kIx,	,
uhlík	uhlík	k1gInSc4	uhlík
a	a	k8xC	a
dusík	dusík	k1gInSc4	dusík
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
stabilitu	stabilita	k1gFnSc4	stabilita
jako	jako	k8xS	jako
buněčná	buněčný	k2eAgFnSc1d1	buněčná
membrána	membrána	k1gFnSc1	membrána
tvořená	tvořený	k2eAgFnSc1d1	tvořená
fosfolipidy	fosfolipid	k1gInPc7	fosfolipid
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorietická	teorietický	k2eAgFnSc1d1	teorietický
membrána	membrána	k1gFnSc1	membrána
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
azotozom	azotozom	k1gInSc1	azotozom
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
složením	složení	k1gNnSc7	složení
francouzského	francouzský	k2eAgNnSc2d1	francouzské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
azote	azote	k5eAaPmIp2nP	azote
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
dusík	dusík	k1gInSc1	dusík
<g/>
)	)	kIx)	)
a	a	k8xC	a
termínu	termín	k1gInSc2	termín
lipozom	lipozom	k1gInSc4	lipozom
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byly	být	k5eAaImAgFnP	být
vysloveny	vyslovit	k5eAaPmNgFnP	vyslovit
hypotézy	hypotéza	k1gFnPc1	hypotéza
o	o	k7c6	o
biologických	biologický	k2eAgInPc6d1	biologický
pochodech	pochod	k1gInPc6	pochod
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
značné	značný	k2eAgFnSc2d1	značná
překážky	překážka	k1gFnSc2	překážka
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
života	život	k1gInSc2	život
a	a	k8xC	a
analogie	analogie	k1gFnSc2	analogie
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
je	být	k5eAaImIp3nS	být
nepřesná	přesný	k2eNgFnSc1d1	nepřesná
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
zmrzlým	zmrzlý	k2eAgInSc7d1	zmrzlý
světem	svět	k1gInSc7	svět
v	v	k7c4	v
nesmírné	smírný	k2eNgFnPc4d1	nesmírná
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
atmosféra	atmosféra	k1gFnSc1	atmosféra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
v	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
potížím	potíž	k1gFnPc3	potíž
považují	považovat	k5eAaImIp3nP	považovat
někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Jonathan	Jonathan	k1gMnSc1	Jonathan
Lunine	Lunin	k1gInSc5	Lunin
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc1	měsíc
Titan	titan	k1gInSc4	titan
za	za	k7c4	za
neobyvatelný	obyvatelný	k2eNgInSc4d1	neobyvatelný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vhodný	vhodný	k2eAgInSc1d1	vhodný
a	a	k8xC	a
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
objekt	objekt	k1gInSc1	objekt
pro	pro	k7c4	pro
experimentální	experimentální	k2eAgInSc4d1	experimentální
výzkum	výzkum	k1gInSc4	výzkum
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
panovaly	panovat	k5eAaImAgFnP	panovat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
života	život	k1gInSc2	život
a	a	k8xC	a
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
vznikem	vznik	k1gInSc7	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
dopad	dopad	k1gInSc4	dopad
většího	veliký	k2eAgNnSc2d2	veliký
tělesa	těleso	k1gNnSc2	těleso
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
mohl	moct	k5eAaImAgMnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
vyvržení	vyvržení	k1gNnSc4	vyvržení
kamenného	kamenný	k2eAgInSc2d1	kamenný
materiálu	materiál	k1gInSc2	materiál
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
mikrospór	mikrospór	k1gMnSc1	mikrospór
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
mohl	moct	k5eAaImAgMnS	moct
dopadnout	dopadnout	k5eAaPmF	dopadnout
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
tělesa	těleso	k1gNnPc4	těleso
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
včetně	včetně	k7c2	včetně
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tak	tak	k9	tak
mohl	moct	k5eAaImAgInS	moct
rozšířit	rozšířit	k5eAaPmF	rozšířit
panspermaticky	panspermaticky	k6eAd1	panspermaticky
<g/>
.	.	kIx.	.
</s>
<s>
Lunine	Luninout	k5eAaPmIp3nS	Luninout
však	však	k9	však
oponoval	oponovat	k5eAaImAgMnS	oponovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
organismy	organismus	k1gInPc1	organismus
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
uhlovodíkových	uhlovodíkový	k2eAgNnPc6d1	uhlovodíkové
jezerech	jezero	k1gNnPc6	jezero
Titanu	titan	k1gInSc2	titan
by	by	kYmCp3nP	by
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
natolik	natolik	k6eAd1	natolik
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
pozemských	pozemský	k2eAgInPc2d1	pozemský
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
druhého	druhý	k4xOgInSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
život	život	k1gInSc4	život
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
mohly	moct	k5eAaImAgInP	moct
vylepšit	vylepšit	k5eAaPmF	vylepšit
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
za	za	k7c4	za
pět	pět	k4xCc4	pět
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
až	až	k9	až
se	se	k3xPyFc4	se
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
stane	stanout	k5eAaPmIp3nS	stanout
červený	červený	k2eAgMnSc1d1	červený
obr	obr	k1gMnSc1	obr
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
mohla	moct	k5eAaImAgFnS	moct
stoupnout	stoupnout	k5eAaPmF	stoupnout
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
umožnila	umožnit	k5eAaPmAgFnS	umožnit
výskyt	výskyt	k1gInSc4	výskyt
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
snížení	snížení	k1gNnSc2	snížení
množství	množství	k1gNnSc2	množství
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
opadnout	opadnout	k5eAaPmF	opadnout
uhlovodíkový	uhlovodíkový	k2eAgInSc1d1	uhlovodíkový
opar	opar	k1gInSc1	opar
a	a	k8xC	a
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
způsobený	způsobený	k2eAgInSc1d1	způsobený
methanem	methan	k1gInSc7	methan
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
hrát	hrát	k5eAaImF	hrát
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc4d2	veliký
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
podmínkách	podmínka	k1gFnPc6	podmínka
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
uchytit	uchytit	k5eAaPmF	uchytit
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
přežít	přežít	k5eAaPmF	přežít
i	i	k8xC	i
stovky	stovka	k1gFnPc4	stovka
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
dostatečná	dostatečná	k1gFnSc1	dostatečná
doba	doba	k1gFnSc1	doba
pro	pro	k7c4	pro
usazení	usazení	k1gNnSc4	usazení
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
amoniaku	amoniak	k1gInSc2	amoniak
by	by	kYmCp3nS	by
však	však	k9	však
proces	proces	k1gInSc4	proces
vzniku	vznik	k1gInSc2	vznik
života	život	k1gInSc2	život
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
mohla	moct	k5eAaImAgFnS	moct
zpomalit	zpomalit	k5eAaPmF	zpomalit
<g/>
.	.	kIx.	.
</s>
