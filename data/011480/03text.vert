<p>
<s>
Anonymní	anonymní	k2eAgMnPc1d1	anonymní
alkoholici	alkoholik	k1gMnPc1	alkoholik
(	(	kIx(	(
<g/>
AA	AA	kA	AA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
svépomocná	svépomocný	k2eAgFnSc1d1	svépomocná
skupina	skupina	k1gFnSc1	skupina
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
postižené	postižený	k2eAgFnSc2d1	postižená
alkoholismem	alkoholismus	k1gInSc7	alkoholismus
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
konci	konec	k1gInSc6	konec
prohibice	prohibice	k1gFnSc2	prohibice
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Billem	Bill	k1gMnSc7	Bill
Wilsonem	Wilson	k1gMnSc7	Wilson
a	a	k8xC	a
Bobem	Bob	k1gMnSc7	Bob
Smithem	Smith	k1gInSc7	Smith
v	v	k7c6	v
Akronu	Akron	k1gInSc6	Akron
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
problém	problém	k1gInSc4	problém
s	s	k7c7	s
užíváním	užívání	k1gNnSc7	užívání
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
společenství	společenství	k1gNnSc4	společenství
AA	AA	kA	AA
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
skupin	skupina	k1gFnPc2	skupina
ve	v	k7c6	v
41	[number]	k4	41
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Brně	Brno	k1gNnSc6	Brno
probíhají	probíhat	k5eAaImIp3nP	probíhat
mítinky	mítink	k1gInPc1	mítink
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Preambule	preambule	k1gFnSc2	preambule
==	==	k?	==
</s>
</p>
<p>
<s>
Anonymní	anonymní	k2eAgMnPc1d1	anonymní
alkoholici	alkoholik	k1gMnPc1	alkoholik
je	on	k3xPp3gNnSc4	on
společenství	společenství	k1gNnSc4	společenství
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
spolu	spolu	k6eAd1	spolu
sdílejí	sdílet	k5eAaImIp3nP	sdílet
své	svůj	k3xOyFgFnPc4	svůj
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
dovedou	dovést	k5eAaPmIp3nP	dovést
vyřešit	vyřešit	k5eAaPmF	vyřešit
svůj	svůj	k3xOyFgInSc4	svůj
společný	společný	k2eAgInSc4d1	společný
problém	problém	k1gInSc4	problém
a	a	k8xC	a
pomoci	pomoct	k5eAaPmF	pomoct
ostatním	ostatní	k1gNnSc7	ostatní
k	k	k7c3	k
uzdravení	uzdravení	k1gNnSc3	uzdravení
z	z	k7c2	z
alkoholizmu	alkoholizmus	k1gInSc2	alkoholizmus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
požadavkem	požadavek	k1gInSc7	požadavek
pro	pro	k7c4	pro
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
AA	AA	kA	AA
je	být	k5eAaImIp3nS	být
touha	touha	k1gFnSc1	touha
přestat	přestat	k5eAaPmF	přestat
pít	pít	k5eAaImF	pít
<g/>
.	.	kIx.	.
</s>
<s>
Nemáme	mít	k5eNaImIp1nP	mít
žádné	žádný	k3yNgInPc4	žádný
povinné	povinný	k2eAgInPc4d1	povinný
poplatky	poplatek	k1gInPc4	poplatek
ani	ani	k8xC	ani
vstupné	vstupné	k1gNnSc4	vstupné
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
soběstační	soběstačný	k2eAgMnPc1d1	soběstačný
díky	díky	k7c3	díky
vlastním	vlastní	k2eAgMnPc3d1	vlastní
dobrovolným	dobrovolný	k2eAgMnPc3d1	dobrovolný
příspěvkům	příspěvek	k1gInPc3	příspěvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anonymní	anonymní	k2eAgMnPc1d1	anonymní
alkoholici	alkoholik	k1gMnPc1	alkoholik
nejsou	být	k5eNaImIp3nP	být
spojeni	spojit	k5eAaPmNgMnP	spojit
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
sektou	sekta	k1gFnSc7	sekta
<g/>
,	,	kIx,	,
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
politickou	politický	k2eAgFnSc7d1	politická
organizací	organizace	k1gFnSc7	organizace
či	či	k8xC	či
jakoukoliv	jakýkoliv	k3yIgFnSc7	jakýkoliv
jinou	jiný	k2eAgFnSc7d1	jiná
institucí	instituce	k1gFnSc7	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Nepřejí	přejíst	k5eNaPmIp3nS	přejíst
si	se	k3xPyFc3	se
zaplést	zaplést	k5eAaPmF	zaplést
se	se	k3xPyFc4	se
do	do	k7c2	do
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
rozepře	rozepře	k1gFnSc2	rozepře
<g/>
,	,	kIx,	,
neodporují	odporovat	k5eNaImIp3nP	odporovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
nepodporují	podporovat	k5eNaImIp3nP	podporovat
žádné	žádný	k3yNgInPc4	žádný
vnější	vnější	k2eAgInPc4d1	vnější
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Našim	náš	k3xOp1gMnPc3	náš
prvotním	prvotní	k2eAgMnPc3d1	prvotní
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
zůstat	zůstat	k5eAaPmF	zůstat
střízliví	střízlivět	k5eAaImIp3nS	střízlivět
a	a	k8xC	a
pomáhat	pomáhat	k5eAaImF	pomáhat
ostatním	ostatní	k2eAgMnPc3d1	ostatní
alkoholikům	alkoholik	k1gMnPc3	alkoholik
střízlivosti	střízlivost	k1gFnSc2	střízlivost
dosahovat	dosahovat	k5eAaImF	dosahovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Alcoholics	Alcoholics	k1gInSc1	Alcoholics
Anonymous	Anonymous	k1gInSc1	Anonymous
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Anonymní	anonymní	k2eAgFnSc2d1	anonymní
alkoholici	alkoholik	k1gMnPc1	alkoholik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
agnostických	agnostický	k2eAgFnPc2d1	agnostická
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
AA	AA	kA	AA
</s>
</p>
<p>
<s>
http://www.anonymnialkoholici.cz/	[url]	k?	http://www.anonymnialkoholici.cz/
</s>
</p>
