šlechtice	šlechtic	k1gMnSc4
z	z	k7c2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
identifikovali	identifikovat	k5eAaBmAgMnP
s	s	k7c7
českým	český	k2eAgNnSc7d1
státním	státní	k2eAgNnSc7d1
právem	právo	k1gNnSc7
<g/>
,	,	kIx,
respektive	respektive	k9
s	s	k7c7
historickými	historický	k2eAgNnPc7d1
zemskými	zemský	k2eAgNnPc7d1
právy	právo	k1gNnPc7
<g/>
,	,	kIx,
a	a	k8xC
odmítali	odmítat	k5eAaImAgMnP
centralistickou	centralistický	k2eAgFnSc4d1
koncepci	koncepce	k1gFnSc4
rakouského	rakouský	k2eAgInSc2d1
státu	stát	k1gInSc2
