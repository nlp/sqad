<s>
Strana	strana	k1gFnSc1
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
</s>
<s>
Strana	strana	k1gFnSc1
konzervativního	konzervativní	k2eAgInSc2d1
velkostatkuKonservativer	velkostatkuKonservativer	k1gMnSc1
Großgrundbesitz	Großgrundbesitz	k1gMnSc1
Datum	datum	k1gInSc4
založení	založení	k1gNnSc2
</s>
<s>
1860	#num#	k4
Datum	datum	k1gInSc1
rozpuštění	rozpuštění	k1gNnSc2
</s>
<s>
1918	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Clam-Martinic	Clam-Martinice	k1gFnPc2
Ideologie	ideologie	k1gFnSc2
</s>
<s>
KonzervatismusFederalismusZemský	KonzervatismusFederalismusZemský	k2eAgInSc1d1
patriotismus	patriotismus	k1gInSc1
Politická	politický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
pravice	pravice	k1gFnSc1
Barvy	barva	k1gFnSc2
</s>
<s>
tmavě	tmavě	k6eAd1
zelená	zelenat	k5eAaImIp3nS
Volební	volební	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
</s>
<s>
49	#num#	k4
<g/>
/	/	kIx~
<g/>
242	#num#	k4
</s>
<s>
Český	český	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
,	,	kIx,
1908	#num#	k4
</s>
<s>
Strana	strana	k1gFnSc1
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Konservativer	Konservativer	k1gInSc1
Großgrundbesitz	Großgrundbesitza	k1gFnPc2
nebo	nebo	k8xC
Böhmischer	Böhmischra	k1gFnPc2
konservativer	konservativer	k1gMnSc1
Großgrundbesitz	Großgrundbesitz	k1gMnSc1
či	či	k8xC
Böhmisch-Konservativer	Böhmisch-Konservativer	k1gMnSc1
Großgrundbesitz	Großgrundbesitz	k1gMnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
konzervativní	konzervativní	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
reprezentující	reprezentující	k2eAgFnSc1d1
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
století	století	k1gNnPc2
na	na	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
i	i	k8xC
na	na	k7c6
zemských	zemský	k2eAgInPc6d1
sněmech	sněm	k1gInPc6
šlechtice	šlechtic	k1gMnSc4
z	z	k7c2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
identifikovali	identifikovat	k5eAaBmAgMnP
s	s	k7c7
českým	český	k2eAgNnSc7d1
státním	státní	k2eAgNnSc7d1
právem	právo	k1gNnSc7
<g/>
,	,	kIx,
respektive	respektive	k9
s	s	k7c7
historickými	historický	k2eAgNnPc7d1
zemskými	zemský	k2eAgNnPc7d1
právy	právo	k1gNnPc7
<g/>
,	,	kIx,
a	a	k8xC
odmítali	odmítat	k5eAaImAgMnP
centralistickou	centralistický	k2eAgFnSc4d1
koncepci	koncepce	k1gFnSc4
rakouského	rakouský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Konzervativní	konzervativní	k2eAgMnPc1d1
velkostatkáři	velkostatkář	k1gMnPc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
nazývaní	nazývaný	k2eAgMnPc1d1
též	též	k9
státoprávní	státoprávní	k2eAgFnSc1d1
šlechta	šlechta	k1gFnSc1
<g/>
,	,	kIx,
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
rakouské	rakouský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
zároveň	zároveň	k6eAd1
s	s	k7c7
obnovením	obnovení	k1gNnSc7
ústavního	ústavní	k2eAgInSc2d1
způsobu	způsob	k1gInSc2
vlády	vláda	k1gFnSc2
počátkem	počátkem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
četní	četný	k2eAgMnPc1d1
protagonisté	protagonista	k1gMnPc1
tohoto	tento	k3xDgInSc2
politického	politický	k2eAgInSc2d1
směru	směr	k1gInSc2
se	se	k3xPyFc4
angažovali	angažovat	k5eAaBmAgMnP
již	již	k9
během	během	k7c2
předchozího	předchozí	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říjnový	říjnový	k2eAgInSc4d1
diplom	diplom	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1860	#num#	k4
otevřel	otevřít	k5eAaPmAgMnS
cestu	cesta	k1gFnSc4
k	k	k7c3
zavedení	zavedení	k1gNnSc3
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
založené	založený	k2eAgFnSc2d1
na	na	k7c6
respektování	respektování	k1gNnSc6
historických	historický	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc2
sněmů	sněm	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1861	#num#	k4
se	se	k3xPyFc4
předák	předák	k1gMnSc1
Strany	strana	k1gFnSc2
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
Jindřich	Jindřich	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Clam-Martinic	Clam-Martinice	k1gFnPc2
sešel	sejít	k5eAaPmAgMnS
s	s	k7c7
předákem	předák	k1gMnSc7
českých	český	k2eAgMnPc2d1
liberálů	liberál	k1gMnPc2
Františkem	František	k1gMnSc7
Ladislavem	Ladislav	k1gMnSc7
Riegerem	Rieger	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
(	(	kIx(
<g/>
s	s	k7c7
malými	malý	k2eAgFnPc7d1
přestávkami	přestávka	k1gFnPc7
<g/>
)	)	kIx)
postupovaly	postupovat	k5eAaImAgInP
oba	dva	k4xCgInPc1
politické	politický	k2eAgInPc1d1
proudy	proud	k1gInPc1
ve	v	k7c6
vzájemné	vzájemný	k2eAgFnSc6d1
koordinaci	koordinace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
inspirovaly	inspirovat	k5eAaBmAgFnP
podobnou	podobný	k2eAgFnSc7d1
aliancí	aliance	k1gFnSc7
šlechtických	šlechtický	k2eAgMnPc2d1
a	a	k8xC
občanských	občanský	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
v	v	k7c6
Uhersku	Uhersko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojoval	spojovat	k5eAaImAgMnS
je	být	k5eAaImIp3nS
zájem	zájem	k1gInSc4
na	na	k7c6
respektování	respektování	k1gNnSc6
českého	český	k2eAgNnSc2d1
státního	státní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
a	a	k8xC
odmítání	odmítání	k1gNnSc2
centralistického	centralistický	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
rakouského	rakouský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgMnSc7d1
politickým	politický	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
Strany	strana	k1gFnSc2
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
byla	být	k5eAaImAgFnS
Strana	strana	k1gFnSc1
ústavověrného	ústavověrný	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
naopak	naopak	k6eAd1
sdružovala	sdružovat	k5eAaImAgFnS
šlechtice	šlechtic	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
podporovali	podporovat	k5eAaImAgMnP
centralistickou	centralistický	k2eAgFnSc4d1
a	a	k8xC
provídeňskou	provídeňský	k2eAgFnSc4d1
linii	linie	k1gFnSc4
rakouské	rakouský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
a	a	k8xC
spolupracovali	spolupracovat	k5eAaImAgMnP
s	s	k7c7
německými	německý	k2eAgMnPc7d1
liberály	liberál	k1gMnPc7
(	(	kIx(
<g/>
tzv.	tzv.	kA
Ústavní	ústavní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Naopak	naopak	k6eAd1
spojencem	spojenec	k1gMnSc7
české	český	k2eAgFnSc2d1
státoprávní	státoprávní	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
byli	být	k5eAaImAgMnP
německorakouští	německorakouský	k2eAgMnPc1d1
konzervativní	konzervativní	k2eAgMnPc1d1
politici	politik	k1gMnPc1
z	z	k7c2
alpských	alpský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
rovněž	rovněž	k9
odmítali	odmítat	k5eAaImAgMnP
centralistické	centralistický	k2eAgFnPc4d1
a	a	k8xC
sekulární	sekulární	k2eAgFnPc4d1
tendence	tendence	k1gFnPc4
německých	německý	k2eAgMnPc2d1
liberálů	liberál	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
českých	český	k2eAgMnPc2d1
občanských	občanský	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
vedených	vedený	k2eAgFnPc2d1
Riegrem	Riegrma	k1gFnPc2
se	se	k3xPyFc4
ovšem	ovšem	k9
konzervativní	konzervativní	k2eAgMnPc1d1
velkostatkáři	velkostatkář	k1gMnPc1
často	často	k6eAd1
identifikovali	identifikovat	k5eAaBmAgMnP
se	s	k7c7
zemským	zemský	k2eAgNnSc7d1
vlastenectvím	vlastenectví	k1gNnSc7
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
s	s	k7c7
výlučně	výlučně	k6eAd1
etnickým	etnický	k2eAgNnSc7d1
češstvím	češství	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Například	například	k6eAd1
Franz	Franz	k1gMnSc1
Thun	Thun	k1gMnSc1
und	und	k?
Hohenstein	Hohenstein	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
koncem	koncem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
krátce	krátce	k6eAd1
zastával	zastávat	k5eAaImAgMnS
i	i	k9
funkci	funkce	k1gFnSc4
předsedy	předseda	k1gMnSc2
vlády	vláda	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Předlitavska	Předlitavsko	k1gNnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
jazykově	jazykově	k6eAd1
orientován	orientovat	k5eAaBmNgInS
německy	německy	k6eAd1
a	a	k8xC
neovládal	ovládat	k5eNaImAgMnS
češtinu	čeština	k1gFnSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
hlásil	hlásit	k5eAaImAgMnS
se	se	k3xPyFc4
k	k	k7c3
českému	český	k2eAgNnSc3d1
státnímu	státní	k2eAgNnSc3d1
právu	právo	k1gNnSc3
a	a	k8xC
v	v	k7c6
rámci	rámec	k1gInSc6
zemského	zemský	k2eAgNnSc2d1
vlastenectví	vlastenectví	k1gNnSc2
podporoval	podporovat	k5eAaImAgInS
plnou	plný	k2eAgFnSc4d1
rovnoprávnost	rovnoprávnost	k1gFnSc4
obou	dva	k4xCgNnPc2
zemských	zemský	k2eAgNnPc2d1
etnik	etnikum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zejména	zejména	k9
na	na	k7c6
Moravě	Morava	k1gFnSc6
se	se	k3xPyFc4
ještě	ještě	k6eAd1
během	během	k7c2
2	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zformoval	zformovat	k5eAaPmAgMnS
mezi	mezi	k7c7
šlechtou	šlechta	k1gFnSc7
další	další	k2eAgInSc1d1
politický	politický	k2eAgInSc1d1
proud	proud	k1gInSc1
<g/>
,	,	kIx,
nazývaný	nazývaný	k2eAgInSc1d1
Strana	strana	k1gFnSc1
středního	střední	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
vyhnout	vyhnout	k5eAaPmF
volbě	volba	k1gFnSc3
mezi	mezi	k7c7
stále	stále	k6eAd1
vyostřenějším	vyostřený	k2eAgInSc7d2
pročeským	pročeský	k2eAgInSc7d1
nebo	nebo	k8xC
proněmeckým	proněmecký	k2eAgInSc7d1
táborem	tábor	k1gInSc7
zaujetím	zaujetí	k1gNnSc7
centristické	centristický	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejími	její	k3xOp3gMnPc7
představiteli	představitel	k1gMnPc7
byli	být	k5eAaImAgMnP
například	například	k6eAd1
Moritz	moritz	k1gInSc4
Vetter-Lilie	Vetter-Lilie	k1gFnSc2
nebo	nebo	k8xC
Alfred	Alfred	k1gMnSc1
Skene	Sken	k1gInSc5
II	II	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Politický	politický	k2eAgInSc1d1
vliv	vliv	k1gInSc1
Strany	strana	k1gFnSc2
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
byl	být	k5eAaImAgInS
z	z	k7c2
podstatné	podstatný	k2eAgFnSc2d1
části	část	k1gFnSc2
založen	založit	k5eAaPmNgInS
na	na	k7c6
existenci	existence	k1gFnSc6
kuriového	kuriový	k2eAgInSc2d1
volebního	volební	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
velkostatkáři	velkostatkář	k1gMnPc1
odděleně	odděleně	k6eAd1
hlasovali	hlasovat	k5eAaImAgMnP
a	a	k8xC
volili	volit	k5eAaImAgMnP
své	svůj	k3xOyFgMnPc4
zástupce	zástupce	k1gMnPc4
na	na	k7c6
zemských	zemský	k2eAgInPc6d1
sněmech	sněm	k1gInPc6
i	i	k8xC
v	v	k7c6
celostátním	celostátní	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
i	i	k9
relativně	relativně	k6eAd1
nepočetná	početný	k2eNgFnSc1d1
skupina	skupina	k1gFnSc1
populace	populace	k1gFnSc1
disponovala	disponovat	k5eAaBmAgFnS
značným	značný	k2eAgInSc7d1
podílem	podíl	k1gInSc7
přidělovaných	přidělovaný	k2eAgInPc2d1
mandátů	mandát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Českém	český	k2eAgInSc6d1
zemském	zemský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
disponovala	disponovat	k5eAaBmAgFnS
kurie	kurie	k1gFnSc1
velkostatkářů	velkostatkář	k1gMnPc2
70	#num#	k4
mandáty	mandát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
relativně	relativně	k6eAd1
vyrovnaných	vyrovnaný	k2eAgInPc6d1
silových	silový	k2eAgInPc6d1
poměrech	poměr	k1gInPc6
mezi	mezi	k7c7
blokem	blok	k1gInSc7
českých	český	k2eAgMnPc2d1
a	a	k8xC
(	(	kIx(
<g/>
sudeto	sudeto	k1gNnSc1
<g/>
)	)	kIx)
<g/>
německých	německý	k2eAgFnPc2d1
občanských	občanský	k2eAgFnPc2d1
stran	strana	k1gFnPc2
byla	být	k5eAaImAgFnS
poslanecká	poslanecký	k2eAgFnSc1d1
křesla	křesnout	k5eAaPmAgFnS
ve	v	k7c6
velkostatkářské	velkostatkářský	k2eAgFnSc6d1
kurii	kurie	k1gFnSc6
mimořádně	mimořádně	k6eAd1
důležitá	důležitý	k2eAgFnSc1d1
pro	pro	k7c4
celkové	celkový	k2eAgNnSc4d1
směřování	směřování	k1gNnSc4
sněmu	sněm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
zemských	zemský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
lednu	leden	k1gInSc6
1867	#num#	k4
ovládli	ovládnout	k5eAaPmAgMnP
tuto	tento	k3xDgFnSc4
kurii	kurie	k1gFnSc4
kandidáti	kandidát	k1gMnPc1
Strany	strana	k1gFnPc1
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
sněm	sněm	k1gInSc1
získal	získat	k5eAaPmAgInS
federalistickou	federalistický	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ostře	ostro	k6eAd1
odmítla	odmítnout	k5eAaPmAgFnS
rakousko-uherské	rakousko-uherský	k2eAgNnSc4d1
vyrovnání	vyrovnání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovaly	následovat	k5eAaImAgFnP
opakované	opakovaný	k2eAgFnPc1d1
zemské	zemský	k2eAgFnPc1d1
volby	volba	k1gFnPc1
v	v	k7c6
březnu	březen	k1gInSc6
1867	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
díky	díky	k7c3
agitaci	agitace	k1gFnSc3
z	z	k7c2
Vídně	Vídeň	k1gFnSc2
naopak	naopak	k6eAd1
zcela	zcela	k6eAd1
převážila	převážit	k5eAaPmAgFnS
Strana	strana	k1gFnSc1
ústavověrného	ústavověrný	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
a	a	k8xC
český	český	k2eAgInSc1d1
státoprávní	státoprávní	k2eAgInSc1d1
blok	blok	k1gInSc1
se	se	k3xPyFc4
ocitl	ocitnout	k5eAaPmAgInS
v	v	k7c6
menšině	menšina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
se	se	k3xPyFc4
tak	tak	k6eAd1
stalo	stát	k5eAaPmAgNnS
v	v	k7c6
zemských	zemský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1872	#num#	k4
<g/>
,	,	kIx,
provázených	provázený	k2eAgFnPc2d1
masivními	masivní	k2eAgFnPc7d1
majetkovými	majetkový	k2eAgFnPc7d1
machinacemi	machinace	k1gFnPc7
s	s	k7c7
cílem	cíl	k1gInSc7
uměle	uměle	k6eAd1
zvýšit	zvýšit	k5eAaPmF
počet	počet	k1gInSc4
oprávněných	oprávněný	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
ve	v	k7c6
velkostatkářské	velkostatkářský	k2eAgFnSc6d1
kurii	kurie	k1gFnSc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
chabrusové	chabrusový	k2eAgFnPc4d1
volby	volba	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzervativní	konzervativní	k2eAgFnSc1d1
šlechta	šlechta	k1gFnSc1
převážila	převážit	k5eAaPmAgFnS
v	v	k7c6
této	tento	k3xDgFnSc6
kurii	kurie	k1gFnSc6
znovu	znovu	k6eAd1
až	až	k9
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1883	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tiskovým	tiskový	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
Strany	strana	k1gFnSc2
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
byl	být	k5eAaImAgMnS
deník	deník	k1gInSc4
Das	Das	k1gFnSc3
Vaterland	vaterland	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vycházel	vycházet	k5eAaImAgInS
německy	německy	k6eAd1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
a	a	k8xC
měl	mít	k5eAaImAgInS
celostátní	celostátní	k2eAgInSc1d1
záběr	záběr	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnSc4
linii	linie	k1gFnSc4
výrazně	výrazně	k6eAd1
ovlivňovali	ovlivňovat	k5eAaImAgMnP
konzervativní	konzervativní	k2eAgMnPc1d1
šlechtici	šlechtic	k1gMnPc1
z	z	k7c2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předákem	předák	k1gInSc7
konzervativní	konzervativní	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
byl	být	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Jindřich	Jindřich	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Clam-Martinic	Clam-Martinice	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
Egbert	Egbert	k1gInSc4
Belcredi	Belcred	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
dominoval	dominovat	k5eAaImAgMnS
této	tento	k3xDgFnSc6
politické	politický	k2eAgFnSc6d1
straně	strana	k1gFnSc6
na	na	k7c6
Moravském	moravský	k2eAgInSc6d1
zemském	zemský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
Otto	Otto	k1gMnSc1
Serényi	Serény	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
výrazné	výrazný	k2eAgFnPc4d1
politiky	politik	k1gMnPc7
státoprávní	státoprávní	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
patřili	patřit	k5eAaImAgMnP
i	i	k9
Karel	Karel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schwarzenberg	Schwarzenberg	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schwarzenberg	Schwarzenberg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecký	poslanecký	k2eAgInSc1d1
klub	klub	k1gInSc1
Strany	strana	k1gFnSc2
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
na	na	k7c6
Českém	český	k2eAgInSc6d1
zemském	zemský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
vedl	vést	k5eAaImAgInS
na	na	k7c6
přelomu	přelom	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
Alfred	Alfred	k1gMnSc1
August	August	k1gMnSc1
Windischgrätz	Windischgrätz	k1gMnSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
ho	on	k3xPp3gMnSc4
nahradil	nahradit	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
Buquoy-Longueval	Buquoy-Longueval	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výrazným	výrazný	k2eAgInSc7d1
předělem	předěl	k1gInSc7
ve	v	k7c4
fungování	fungování	k1gNnSc4
Strany	strana	k1gFnSc2
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
v	v	k7c6
politickém	politický	k2eAgInSc6d1
systému	systém	k1gInSc6
byl	být	k5eAaImAgInS
rok	rok	k1gInSc1
1879	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1879	#num#	k4
opustila	opustit	k5eAaPmAgFnS
tehdy	tehdy	k6eAd1
česká	český	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
politiku	politika	k1gFnSc4
pasivní	pasivní	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
neúčasti	neúčast	k1gFnSc2
na	na	k7c4
práci	práce	k1gFnSc4
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
vídeňského	vídeňský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
utvořil	utvořit	k5eAaPmAgInS
Český	český	k2eAgInSc1d1
klub	klub	k1gInSc1
jako	jako	k8xC,k8xS
střechová	střechový	k2eAgFnSc1d1
parlamentní	parlamentní	k2eAgFnSc1d1
frakce	frakce	k1gFnSc1
<g/>
,	,	kIx,
sdružující	sdružující	k2eAgFnSc1d1
</s>
<s>
všechny	všechen	k3xTgInPc1
čtyři	čtyři	k4xCgInPc1
proudy	proud	k1gInPc1
české	český	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
:	:	kIx,
staročechy	staročech	k1gMnPc4
<g/>
,	,	kIx,
mladočechy	mladočech	k1gMnPc4
<g/>
,	,	kIx,
moravské	moravský	k2eAgMnPc4d1
národní	národní	k2eAgMnPc4d1
poslance	poslanec	k1gMnPc4
a	a	k8xC
Stranu	strana	k1gFnSc4
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedou	předseda	k1gMnSc7
klubu	klub	k1gInSc2
byl	být	k5eAaImAgMnS
František	František	k1gMnSc1
Ladislav	Ladislav	k1gMnSc1
Rieger	Rieger	k1gMnSc1
<g/>
,	,	kIx,
jedním	jeden	k4xCgMnSc7
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
místopředsedů	místopředseda	k1gMnPc2
byl	být	k5eAaImAgInS
za	za	k7c4
konzervativní	konzervativní	k2eAgMnPc4d1
velkostatkáře	velkostatkář	k1gMnPc4
Jiří	Jiří	k1gMnSc1
Kristián	Kristián	k1gMnSc1
Lobkowicz	Lobkowicz	k1gMnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Jindřich	Jindřich	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Clam-Martinic	Clam-Martinice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
vystoupil	vystoupit	k5eAaPmAgInS
Český	český	k2eAgInSc1d1
klub	klub	k1gInSc1
se	s	k7c7
státoprávním	státoprávní	k2eAgNnSc7d1
ohrazením	ohrazení	k1gNnSc7
<g/>
,	,	kIx,
tedy	tedy	k9
deklarací	deklarace	k1gFnSc7
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
že	že	k8xS
návrat	návrat	k1gInSc4
do	do	k7c2
vídeňského	vídeňský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
neznamená	znamenat	k5eNaImIp3nS
přijetí	přijetí	k1gNnSc4
ústavních	ústavní	k2eAgInPc2d1
poměrů	poměr	k1gInPc2
v	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
ani	ani	k8xC
rezignaci	rezignace	k1gFnSc6
na	na	k7c4
české	český	k2eAgNnSc4d1
státní	státní	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Český	český	k2eAgInSc1d1
klub	klub	k1gInSc1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1879	#num#	k4
stal	stát	k5eAaPmAgMnS
součástí	součást	k1gFnSc7
vlády	vláda	k1gFnSc2
Eduarda	Eduard	k1gMnSc2
Taaffeho	Taaffe	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
jeho	jeho	k3xOp3gMnPc7
koaličními	koaliční	k2eAgMnPc7d1
partnery	partner	k1gMnPc7
byl	být	k5eAaImAgInS
Polský	polský	k2eAgInSc1d1
klub	klub	k1gInSc1
a	a	k8xC
Hohenwartův	Hohenwartův	k2eAgInSc1d1
klub	klub	k1gInSc1
(	(	kIx(
<g/>
převážně	převážně	k6eAd1
německorakouští	německorakouský	k2eAgMnPc1d1
konzervativci	konzervativec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
mocenské	mocenský	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
ukázalo	ukázat	k5eAaPmAgNnS
jako	jako	k9
velmi	velmi	k6eAd1
trvanlivé	trvanlivý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
klub	klub	k1gInSc1
se	se	k3xPyFc4
rozpadl	rozpadnout	k5eAaPmAgInS
koncem	koncem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
kvůli	kvůli	k7c3
vyostření	vyostření	k1gNnSc3
sporů	spor	k1gInPc2
mezi	mezi	k7c4
staročechy	staročech	k1gMnPc4
a	a	k8xC
mladočechy	mladočech	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
měla	mít	k5eAaImAgFnS
blíže	blízce	k6eAd2
k	k	k7c3
staročechům	staročech	k1gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
názorový	názorový	k2eAgInSc1d1
mainstream	mainstream	k1gInSc1
české	český	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
se	se	k3xPyFc4
přikláněl	přiklánět	k5eAaImAgInS
k	k	k7c3
mladočeskému	mladočeský	k2eAgInSc3d1
proudu	proud	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příznačná	příznačný	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
polemika	polemika	k1gFnSc1
z	z	k7c2
konce	konec	k1gInSc2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
na	na	k7c6
Českém	český	k2eAgInSc6d1
zemském	zemský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schwarzenberg	Schwarzenberg	k1gInSc1
v	v	k7c6
diskuzi	diskuze	k1gFnSc6
ohledně	ohledně	k7c2
umístění	umístění	k1gNnSc2
plakety	plaketa	k1gFnSc2
s	s	k7c7
podobiznou	podobizna	k1gFnSc7
Jana	Jan	k1gMnSc2
Husa	Hus	k1gMnSc2
na	na	k7c6
právě	právě	k9
dokončované	dokončovaný	k2eAgFnSc6d1
budově	budova	k1gFnSc6
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
označil	označit	k5eAaPmAgMnS
husity	husita	k1gMnSc2
za	za	k7c2
„	„	k?
<g/>
komunismus	komunismus	k1gInSc1
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
“	“	k?
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Mezi	mezi	k7c4
husity	husita	k1gMnPc4
bylo	být	k5eAaImAgNnS
na	na	k7c6
počátku	počátek	k1gInSc6
onoho	onen	k3xDgNnSc2
hnutí	hnutí	k1gNnSc2
mnoho	mnoho	k6eAd1
charakterů	charakter	k1gInPc2
ctihodných	ctihodný	k2eAgInPc2d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
husité	husita	k1gMnPc1
bohužel	bohužel	k9
zvrhli	zvrhnout	k5eAaPmAgMnP
se	se	k3xPyFc4
brzy	brzy	k6eAd1
v	v	k7c6
tlupu	tlup	k1gInSc6
lupičů	lupič	k1gMnPc2
a	a	k8xC
žhářů	žhář	k1gMnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Výrok	výrok	k1gInSc1
vyvolal	vyvolat	k5eAaPmAgInS
ostrou	ostrý	k2eAgFnSc4d1
mladočeskou	mladočeský	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzervativní	konzervativní	k2eAgMnPc1d1
statkáři	statkář	k1gMnPc1
se	se	k3xPyFc4
také	také	k9
ocitali	ocitat	k5eAaImAgMnP
v	v	k7c4
izolaci	izolace	k1gFnSc4
kvůli	kvůli	k7c3
jejich	jejich	k3xOp3gInSc3
odporu	odpor	k1gInSc3
proti	proti	k7c3
demokratizaci	demokratizace	k1gFnSc3
volebního	volební	k2eAgNnSc2d1
práva	právo	k1gNnSc2
a	a	k8xC
zdrženlivému	zdrženlivý	k2eAgInSc3d1
postoji	postoj	k1gInSc3
k	k	k7c3
ryze	ryze	k6eAd1
etnické	etnický	k2eAgFnSc3d1
definici	definice	k1gFnSc3
češství	češství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Riegrova	Riegrův	k2eAgFnSc1d1
staročeská	staročeský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
prakticky	prakticky	k6eAd1
eliminována	eliminovat	k5eAaBmNgFnS
po	po	k7c6
mladočeském	mladočeský	k2eAgInSc6d1
volebním	volební	k2eAgInSc6d1
triumfu	triumf	k1gInSc6
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1891	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
proto	proto	k8xC
po	po	k7c6
těchto	tento	k3xDgFnPc6
volbách	volba	k1gFnPc6
přistoupila	přistoupit	k5eAaPmAgFnS
do	do	k7c2
konzervativního	konzervativní	k2eAgInSc2d1
Hohenwartova	Hohenwartův	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1897	#num#	k4
měla	mít	k5eAaImAgFnS
Strana	strana	k1gFnSc1
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
na	na	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
svůj	svůj	k3xOyFgInSc1
samostatný	samostatný	k2eAgInSc1d1
poslanecký	poslanecký	k2eAgInSc1d1
klub	klub	k1gInSc1
s	s	k7c7
19	#num#	k4
členy	člen	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
předsedou	předseda	k1gMnSc7
byl	být	k5eAaImAgMnS
Eduard	Eduard	k1gMnSc1
Pálffy	Pálff	k1gInPc1
z	z	k7c2
Erdödu	Erdöd	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ale	ale	k8xC
vliv	vliv	k1gInSc1
Strany	strana	k1gFnSc2
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
slábl	slábnout	k5eAaImAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
díky	díky	k7c3
reformám	reforma	k1gFnPc3
volebního	volební	k2eAgNnSc2d1
práva	právo	k1gNnSc2
rozšiřoval	rozšiřovat	k5eAaImAgInS
okruh	okruh	k1gInSc1
oprávněných	oprávněný	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1907	#num#	k4
bylo	být	k5eAaImAgNnS
pak	pak	k6eAd1
zavedeno	zaveden	k2eAgNnSc4d1
všeobecné	všeobecný	k2eAgNnSc4d1
a	a	k8xC
rovné	rovný	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zrušení	zrušení	k1gNnSc3
kuriového	kuriový	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
zániku	zánik	k1gInSc2
separátního	separátní	k2eAgNnSc2d1
parlamentního	parlamentní	k2eAgNnSc2d1
zastoupení	zastoupení	k1gNnSc2
velkostatkářů	velkostatkář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
ovšem	ovšem	k9
dál	daleko	k6eAd2
zasedali	zasedat	k5eAaImAgMnP
v	v	k7c6
Panské	panský	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
(	(	kIx(
<g/>
horní	horní	k2eAgFnSc1d1
nevolená	volený	k2eNgFnSc1d1
komora	komora	k1gFnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
na	na	k7c6
zemském	zemský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
v	v	k7c6
Čechách	Čechy	k1gFnPc6
a	a	k8xC
na	na	k7c6
Moravě	Morava	k1gFnSc6
zůstaly	zůstat	k5eAaPmAgFnP
zachovány	zachován	k2eAgFnPc1d1
kurie	kurie	k1gFnPc1
a	a	k8xC
tedy	tedy	k9
i	i	k9
samostatná	samostatný	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
síla	síla	k1gFnSc1
šlechty	šlechta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
GEORGIEV	GEORGIEV	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
Československu	Československo	k1gNnSc6
1861	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Období	období	k1gNnSc1
1861	#num#	k4
<g/>
-	-	kIx~
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
178	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
59	#num#	k4
<g/>
-	-	kIx~
<g/>
86	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
František	František	k1gMnSc1
kníže	kníže	k1gMnSc1
Thun-Hohenstein	Thun-Hohenstein	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
politika	politika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listopad	listopad	k1gInSc1
1916	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
34	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
304	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Österreichisches	Österreichisches	k1gInSc1
Biographisches	Biographischesa	k1gFnPc2
Lexikon	lexikon	k1gNnSc1
1815	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bd	Bd	k1gFnSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wien	Wien	k1gNnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
7001	#num#	k4
<g/>
-	-	kIx~
<g/>
3213	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Skene	Sken	k1gInSc5
<g/>
,	,	kIx,
Alfred	Alfred	k1gMnSc1
(	(	kIx(
<g/>
II	II	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Frh	Frh	k1gFnSc1
<g/>
.	.	kIx.
von	von	k1gInSc1
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
-	-	kIx~
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Politiker	Politiker	k1gMnSc1
<g/>
,	,	kIx,
Fabrikant	fabrikant	k1gMnSc1
und	und	k?
Großgrundbesitzer	Großgrundbesitzer	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
320	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Československé	československý	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
178	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
316	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Österreichisches	Österreichisches	k1gInSc1
Biographisches	Biographischesa	k1gFnPc2
Lexikon	lexikon	k1gNnSc1
1815	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bd	Bd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wien	Wien	k1gNnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
7001	#num#	k4
<g/>
-	-	kIx~
<g/>
3213	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Belcredi	Belcred	k1gMnPc1
<g/>
,	,	kIx,
Ekbert	Ekbert	k1gMnSc1
Gf	Gf	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1816	#num#	k4
<g/>
-	-	kIx~
<g/>
1894	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Politiker	Politiker	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
66	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Österreichisches	Österreichisches	k1gInSc1
Biographisches	Biographischesa	k1gFnPc2
Lexikon	lexikon	k1gNnSc1
1815	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bd	Bd	k1gFnSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wien	Wien	k1gNnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
7001	#num#	k4
<g/>
-	-	kIx~
<g/>
3213	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Serényi	Serény	k1gFnSc2
von	von	k1gInSc1
Kis-Serény	Kis-Seréna	k1gFnSc2
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
Johann	Johann	k1gMnSc1
Gf	Gf	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1855	#num#	k4
<g/>
-	-	kIx~
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Politiker	Politiker	k1gMnSc1
und	und	k?
Großgrundbesitzer	Großgrundbesitzer	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
187	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hrabě	Hrabě	k1gMnSc4
Karel	Karel	k1gMnSc1
Buquoy	Buquoa	k1gFnSc2
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
politika	politika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srpen	srpen	k1gInSc1
1911	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
29	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
219	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
URBAN	Urban	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
1848	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
328	#num#	k4
<g/>
-	-	kIx~
<g/>
329	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Československé	československý	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
178	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
314	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
Československu	Československo	k1gNnSc6
1861	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Období	období	k1gNnSc1
1861	#num#	k4
<g/>
-	-	kIx~
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
178	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
164	#num#	k4
<g/>
-	-	kIx~
<g/>
165	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
URBAN	Urban	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
1848	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
391	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
URBAN	Urban	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
1848	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
410	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vorarlberger	Vorarlberger	k1gMnSc1
Landes-Zeitung	Landes-Zeitung	k1gMnSc1
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1897	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Strana	strana	k1gFnSc1
ústavověrného	ústavověrný	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
</s>
<s>
Strana	strana	k1gFnSc1
středního	střední	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rakousko	Rakousko	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
