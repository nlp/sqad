<s>
Strana	strana	k1gFnSc1
konzervativního	konzervativní	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Konservativer	Konservativer	k1gInSc1
Großgrundbesitz	Großgrundbesitza	k1gFnPc2
nebo	nebo	k8xC
Böhmischer	Böhmischra	k1gFnPc2
konservativer	konservativer	k1gMnSc1
Großgrundbesitz	Großgrundbesitz	k1gMnSc1
či	či	k8xC
Böhmisch-Konservativer	Böhmisch-Konservativer	k1gMnSc1
Großgrundbesitz	Großgrundbesitz	k1gMnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
konzervativní	konzervativní	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
reprezentující	reprezentující	k2eAgFnSc1d1
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
století	století	k1gNnPc2
na	na	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
i	i	k8xC
na	na	k7c6
zemských	zemský	k2eAgInPc6d1
sněmech	sněm	k1gInPc6
šlechtice	šlechtic	k1gMnSc4
z	z	k7c2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
identifikovali	identifikovat	k5eAaBmAgMnP
s	s	k7c7
českým	český	k2eAgNnSc7d1
státním	státní	k2eAgNnSc7d1
právem	právo	k1gNnSc7
<g/>
,	,	kIx,
respektive	respektive	k9
s	s	k7c7
historickými	historický	k2eAgNnPc7d1
zemskými	zemský	k2eAgNnPc7d1
právy	právo	k1gNnPc7
<g/>
,	,	kIx,
a	a	k8xC
odmítali	odmítat	k5eAaImAgMnP
centralistickou	centralistický	k2eAgFnSc4d1
koncepci	koncepce	k1gFnSc4
rakouského	rakouský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>