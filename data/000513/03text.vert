<s>
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
29	[number]	k4	29
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
zhruba	zhruba	k6eAd1	zhruba
uprostřed	uprostřed	k7c2	uprostřed
města	město	k1gNnSc2	město
po	po	k7c6	po
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
západně	západně	k6eAd1	západně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
1503	[number]	k4	1503
ha	ha	kA	ha
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
územím	území	k1gNnSc7	území
se	se	k3xPyFc4	se
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
územím	území	k1gNnSc7	území
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
celá	celý	k2eAgNnPc4d1	celé
katastrální	katastrální	k2eAgNnPc4d1	katastrální
území	území	k1gNnPc4	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Štýřice	Štýřice	k1gFnPc1	Štýřice
<g/>
,	,	kIx,	,
Veveří	veveří	k2eAgFnPc1d1	veveří
a	a	k8xC	a
Stránice	Stránice	k1gFnPc1	Stránice
a	a	k8xC	a
části	část	k1gFnPc1	část
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
Černá	černý	k2eAgFnSc1d1	černá
Pole	pole	k1gFnSc1	pole
<g/>
,	,	kIx,	,
Pisárky	Pisárek	k1gInPc1	Pisárek
<g/>
,	,	kIx,	,
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
a	a	k8xC	a
Zábrdovice	Zábrdovice	k1gFnPc1	Zábrdovice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
senátních	senátní	k2eAgFnPc2d1	senátní
voleb	volba	k1gFnPc2	volba
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc1	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
volebního	volební	k2eAgInSc2d1	volební
obvodu	obvod	k1gInSc2	obvod
číslo	číslo	k1gNnSc1	číslo
59	[number]	k4	59
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nesourodým	sourodý	k2eNgInSc7d1	nesourodý
celkem	celek	k1gInSc7	celek
řady	řada	k1gFnSc2	řada
čtvrtí	čtvrtit	k5eAaImIp3nS	čtvrtit
s	s	k7c7	s
hustou	hustý	k2eAgFnSc7d1	hustá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
různě	různě	k6eAd1	různě
starou	starý	k2eAgFnSc7d1	stará
zástavbou	zástavba	k1gFnSc7	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
nejstarší	starý	k2eAgFnSc2d3	nejstarší
brněnské	brněnský	k2eAgFnSc2d1	brněnská
zástavby	zástavba	k1gFnSc2	zástavba
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
moderní	moderní	k2eAgFnPc4d1	moderní
výškové	výškový	k2eAgFnPc4d1	výšková
budovy	budova	k1gFnPc4	budova
i	i	k8xC	i
panelová	panelový	k2eAgNnPc4d1	panelové
sídliště	sídliště	k1gNnSc4	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Dominanty	dominanta	k1gFnPc1	dominanta
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
představují	představovat	k5eAaImIp3nP	představovat
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
známá	známá	k1gFnSc1	známá
také	také	k9	také
jako	jako	k9	jako
Petrov	Petrov	k1gInSc1	Petrov
<g/>
)	)	kIx)	)
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
Petrově	Petrov	k1gInSc6	Petrov
a	a	k8xC	a
hrad	hrad	k1gInSc1	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
brněnský	brněnský	k2eAgInSc1d1	brněnský
park	park	k1gInSc1	park
Lužánky	Lužánka	k1gFnSc2	Lužánka
i	i	k8xC	i
ústřední	ústřední	k2eAgInSc1d1	ústřední
hřbitov	hřbitov	k1gInSc1	hřbitov
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svojí	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
především	především	k9	především
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
původní	původní	k2eAgNnSc4d1	původní
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
administrativním	administrativní	k2eAgNnSc7d1	administrativní
<g/>
,	,	kIx,	,
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
tradičně	tradičně	k6eAd1	tradičně
sídlí	sídlet	k5eAaImIp3nS	sídlet
řada	řada	k1gFnSc1	řada
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
základních	základní	k2eAgFnPc2d1	základní
<g/>
,	,	kIx,	,
středních	střední	k2eAgFnPc2d1	střední
i	i	k8xC	i
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
firem	firma	k1gFnPc2	firma
i	i	k8xC	i
obchodů	obchod	k1gInPc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
důležitý	důležitý	k2eAgInSc1d1	důležitý
dopravní	dopravní	k2eAgInSc1d1	dopravní
uzel	uzel	k1gInSc1	uzel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
čtvrti	čtvrt	k1gFnSc6	čtvrt
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc4	Brno
sídlí	sídlet	k5eAaImIp3nS	sídlet
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
úřad	úřad	k1gInSc1	úřad
Veřejného	veřejný	k2eAgMnSc2d1	veřejný
ochránce	ochránce	k1gMnSc2	ochránce
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
magistrát	magistrát	k1gInSc1	magistrát
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Městský	městský	k2eAgInSc1d1	městský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Brněnské	brněnský	k2eAgNnSc1d1	brněnské
biskupství	biskupství	k1gNnSc1	biskupství
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Veveří	veveří	k2eAgFnSc6d1	veveří
sídlí	sídlet	k5eAaImIp3nS	sídlet
krajský	krajský	k2eAgInSc1d1	krajský
úřad	úřad	k1gInSc1	úřad
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pisárkách	Pisárka	k1gFnPc6	Pisárka
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
i	i	k8xC	i
Ruský	ruský	k2eAgInSc1d1	ruský
konzulát	konzulát	k1gInSc1	konzulát
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
představuje	představovat	k5eAaImIp3nS	představovat
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svojí	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Brna	Brno	k1gNnSc2	Brno
významný	významný	k2eAgInSc4d1	významný
dopravní	dopravní	k2eAgInSc4d1	dopravní
uzel	uzel	k1gInSc4	uzel
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgNnSc2	jenž
směřuje	směřovat	k5eAaImIp3nS	směřovat
nejen	nejen	k6eAd1	nejen
řada	řada	k1gFnSc1	řada
autobusových	autobusový	k2eAgFnPc2d1	autobusová
<g/>
,	,	kIx,	,
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
i	i	k8xC	i
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
linek	linka	k1gFnPc2	linka
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
řada	řada	k1gFnSc1	řada
linek	linka	k1gFnPc2	linka
vnitrostátní	vnitrostátní	k2eAgFnSc2d1	vnitrostátní
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
autobusové	autobusový	k2eAgFnSc2d1	autobusová
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc2d1	železniční
i	i	k8xC	i
nákladní	nákladní	k2eAgFnSc2d1	nákladní
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
městské	městský	k2eAgFnSc6d1	městská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hlavní	hlavní	k2eAgNnSc4d1	hlavní
brněnské	brněnský	k2eAgNnSc4d1	brněnské
železniční	železniční	k2eAgNnSc4d1	železniční
nádraží	nádraží	k1gNnSc4	nádraží
a	a	k8xC	a
staré	starý	k2eAgNnSc4d1	staré
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
například	například	k6eAd1	například
spoje	spoj	k1gInPc1	spoj
společnosti	společnost	k1gFnSc2	společnost
Student	student	k1gMnSc1	student
Agency	Agenca	k1gMnSc2	Agenca
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
zase	zase	k9	zase
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
autobusové	autobusový	k2eAgNnSc1d1	autobusové
nádraží	nádraží	k1gNnSc1	nádraží
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
i	i	k9	i
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
autobusové	autobusový	k2eAgInPc4d1	autobusový
spoje	spoj	k1gInPc4	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Územím	území	k1gNnSc7	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
také	také	k9	také
prochází	procházet	k5eAaImIp3nS	procházet
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
trasy	trasa	k1gFnSc2	trasa
velkého	velký	k2eAgInSc2d1	velký
městského	městský	k2eAgInSc2d1	městský
okruhu	okruh	k1gInSc2	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
je	být	k5eAaImIp3nS	být
však	však	k9	však
také	také	k9	také
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
srdcem	srdce	k1gNnSc7	srdce
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
velkých	velký	k2eAgFnPc2d1	velká
<g/>
,	,	kIx,	,
středních	střední	k2eAgFnPc2d1	střední
i	i	k8xC	i
malých	malý	k2eAgFnPc2d1	malá
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
také	také	k9	také
mnoho	mnoho	k4c4	mnoho
obchodů	obchod	k1gInPc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
zdejší	zdejší	k2eAgFnSc7d1	zdejší
společností	společnost	k1gFnSc7	společnost
jsou	být	k5eAaImIp3nP	být
Brněnské	brněnský	k2eAgInPc1d1	brněnský
veletrhy	veletrh	k1gInPc1	veletrh
a	a	k8xC	a
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Pisárkách	Pisárka	k1gFnPc6	Pisárka
světoznámé	světoznámý	k2eAgNnSc1d1	světoznámé
výstaviště	výstaviště	k1gNnSc1	výstaviště
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obchodů	obchod	k1gInPc2	obchod
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgInPc1d1	významný
například	například	k6eAd1	například
hypermarket	hypermarket	k1gInSc4	hypermarket
Interspar	Interspara	k1gFnPc2	Interspara
na	na	k7c4	na
Vídeňské	vídeňský	k2eAgNnSc4d1	Vídeňské
<g/>
,	,	kIx,	,
vícepatrová	vícepatrový	k2eAgFnSc1d1	vícepatrová
budova	budova	k1gFnSc1	budova
obchodního	obchodní	k2eAgInSc2d1	obchodní
domu	dům	k1gInSc2	dům
Tesco	Tesco	k6eAd1	Tesco
na	na	k7c4	na
Dornychu	Dornycha	k1gFnSc4	Dornycha
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
Vaňkovka	Vaňkovka	k1gFnSc1	Vaňkovka
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Tescem	Tesec	k1gInSc7	Tesec
a	a	k8xC	a
Ústředním	ústřední	k2eAgNnSc7d1	ústřední
autobusovým	autobusový	k2eAgNnSc7d1	autobusové
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
velké	velký	k2eAgNnSc4d1	velké
hobby	hobby	k1gNnSc4	hobby
markety	market	k1gInPc1	market
Hornbach	Hornbach	k1gInSc1	Hornbach
a	a	k8xC	a
Bauhaus	Bauhaus	k1gInSc1	Bauhaus
na	na	k7c6	na
Heršpické	heršpický	k2eAgFnSc6d1	Heršpická
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
čtvrti	čtvrt	k1gFnSc6	čtvrt
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc4	Brno
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnSc2	svůj
pobočky	pobočka	k1gFnSc2	pobočka
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
bank	banka	k1gFnPc2	banka
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
či	či	k8xC	či
Komerční	komerční	k2eAgFnSc1d1	komerční
banka	banka	k1gFnSc1	banka
aj.	aj.	kA	aj.
V	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
i	i	k8xC	i
významné	významný	k2eAgFnPc4d1	významná
kulturní	kulturní	k2eAgFnPc4d1	kulturní
instituce	instituce	k1gFnPc4	instituce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Moravská	moravský	k2eAgFnSc1d1	Moravská
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
či	či	k8xC	či
divadla	divadlo	k1gNnSc2	divadlo
-	-	kIx~	-
Janáčkovo	Janáčkův	k2eAgNnSc1d1	Janáčkovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Mahenovo	Mahenův	k2eAgNnSc1d1	Mahenovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Hadivadlo	Hadivadlo	k1gNnSc1	Hadivadlo
<g/>
,	,	kIx,	,
Reduta	reduta	k1gFnSc1	reduta
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
Hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
a	a	k8xC	a
planetárium	planetárium	k1gNnSc1	planetárium
,	,	kIx,	,
Moravská	moravský	k2eAgFnSc1d1	Moravská
zemská	zemský	k2eAgFnSc1d1	zemská
knihovna	knihovna	k1gFnSc1	knihovna
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
velké	velký	k2eAgInPc4d1	velký
problémy	problém	k1gInPc4	problém
MČ	MČ	kA	MČ
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
patří	patřit	k5eAaImIp3nS	patřit
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Kamennou	kamenný	k2eAgFnSc4d1	kamenná
čtvrť	čtvrť	k1gFnSc4	čtvrť
<g/>
)	)	kIx)	)
nedostatečné	dostatečný	k2eNgNnSc4d1	nedostatečné
pokrytí	pokrytí	k1gNnSc4	pokrytí
kanalizační	kanalizační	k2eAgFnSc7d1	kanalizační
sítí	síť	k1gFnSc7	síť
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
nevyhovující	vyhovující	k2eNgMnSc1d1	nevyhovující
jak	jak	k8xS	jak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
hygieny	hygiena	k1gFnSc2	hygiena
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
doklady	doklad	k1gInPc1	doklad
(	(	kIx(	(
<g/>
žárová	žárový	k2eAgNnPc1d1	žárové
pohřebiště	pohřebiště	k1gNnPc1	pohřebiště
<g/>
)	)	kIx)	)
slovanského	slovanský	k2eAgNnSc2d1	slovanské
osídlení	osídlení	k1gNnSc2	osídlení
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Pisárek	Pisárka	k1gFnPc2	Pisárka
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
čtvrť	čtvrtit	k5eAaImRp2nS	čtvrtit
Brno-město	Brnoěsta	k1gMnSc5	Brno-města
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejstarší	starý	k2eAgFnSc1d3	nejstarší
zástavba	zástavba	k1gFnSc1	zástavba
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
vznikala	vznikat	k5eAaImAgFnS	vznikat
už	už	k6eAd1	už
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
území	území	k1gNnSc6	území
Starého	Starého	k2eAgNnSc2d1	Starého
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Štýřic	Štýřice	k1gFnPc2	Štýřice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
původně	původně	k6eAd1	původně
ke	k	k7c3	k
Starému	starý	k2eAgNnSc3d1	staré
Brnu	Brno	k1gNnSc3	Brno
také	také	k9	také
zčásti	zčásti	k6eAd1	zčásti
náležely	náležet	k5eAaImAgInP	náležet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
připojení	připojení	k1gNnSc6	připojení
Moravy	Morava	k1gFnSc2	Morava
k	k	k7c3	k
Českému	český	k2eAgNnSc3d1	české
knížectví	knížectví	k1gNnSc3	knížectví
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c4	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
území	území	k1gNnSc6	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brněnský	brněnský	k2eAgInSc1d1	brněnský
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
poloze	poloha	k1gFnSc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
Starého	Starého	k2eAgNnSc2d1	Starého
Brna	Brno	k1gNnSc2	Brno
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
rameny	rameno	k1gNnPc7	rameno
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
dříve	dříve	k6eAd2	dříve
předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
Petrově	Petrov	k1gInSc6	Petrov
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
současných	současný	k2eAgInPc2d1	současný
výzkumů	výzkum	k1gInPc2	výzkum
nezdá	zdát	k5eNaImIp3nS	zdát
příliš	příliš	k6eAd1	příliš
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1091	[number]	k4	1091
pochází	pocházet	k5eAaImIp3nS	pocházet
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
brněnském	brněnský	k2eAgInSc6d1	brněnský
hradu	hrad	k1gInSc6	hrad
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1223	[number]	k4	1223
pochází	pocházet	k5eAaImIp3nS	pocházet
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
na	na	k7c6	na
Petrově	Petrov	k1gInSc6	Petrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přílivu	příliv	k1gInSc3	příliv
cizích	cizí	k2eAgMnPc2d1	cizí
kolonistů	kolonista	k1gMnPc2	kolonista
a	a	k8xC	a
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
osídlení	osídlení	k1gNnSc2	osídlení
ze	z	k7c2	z
Starého	Starého	k2eAgNnSc2d1	Starého
Brna	Brno	k1gNnSc2	Brno
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
trhové	trhový	k2eAgFnSc2d1	trhová
vsi	ves	k1gFnSc2	ves
kolem	kolem	k7c2	kolem
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Náměstí	náměstí	k1gNnSc2	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
území	území	k1gNnSc6	území
čtvrti	čtvrt	k1gFnSc2	čtvrt
Brno-město	Brnoěsta	k1gMnSc5	Brno-města
několik	několik	k4yIc1	několik
osad	osada	k1gFnPc2	osada
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
různého	různý	k2eAgInSc2d1	různý
etnického	etnický	k2eAgInSc2d1	etnický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
první	první	k4xOgFnSc2	první
třetiny	třetina	k1gFnSc2	třetina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
spojily	spojit	k5eAaPmAgFnP	spojit
a	a	k8xC	a
daly	dát	k5eAaPmAgFnP	dát
tak	tak	k6eAd1	tak
vzniknout	vzniknout	k5eAaPmF	vzniknout
středověkému	středověký	k2eAgNnSc3d1	středověké
Brnu	Brno	k1gNnSc3	Brno
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
území	území	k1gNnSc4	území
nyní	nyní	k6eAd1	nyní
tvoří	tvořit	k5eAaImIp3nP	tvořit
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
této	tento	k3xDgFnSc2	tento
čtvrtě	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Brno	Brno	k1gNnSc1	Brno
stalo	stát	k5eAaPmAgNnS	stát
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
právní	právní	k2eAgInSc1d1	právní
řád	řád	k1gInSc1	řád
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
rakouských	rakouský	k2eAgNnPc2d1	rakouské
měst	město	k1gNnPc2	město
Enže	Enž	k1gInSc2	Enž
a	a	k8xC	a
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1243	[number]	k4	1243
udělil	udělit	k5eAaPmAgMnS	udělit
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
a	a	k8xC	a
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
Brnu	Brno	k1gNnSc3	Brno
městská	městský	k2eAgFnSc1d1	městská
privilegia	privilegium	k1gNnPc4	privilegium
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
královským	královský	k2eAgNnSc7d1	královské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
alespoň	alespoň	k9	alespoň
zčásti	zčásti	k6eAd1	zčásti
obehnáno	obehnat	k5eAaPmNgNnS	obehnat
městskými	městský	k2eAgFnPc7d1	městská
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
západně	západně	k6eAd1	západně
od	od	k7c2	od
původního	původní	k2eAgNnSc2d1	původní
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
založen	založit	k5eAaPmNgInS	založit
hrad	hrad	k1gInSc1	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
zeměpanským	zeměpanský	k2eAgInSc7d1	zeměpanský
hradem	hrad	k1gInSc7	hrad
<g/>
,	,	kIx,	,
nepatřícím	patřící	k2eNgInSc6d1	nepatřící
k	k	k7c3	k
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
městskými	městský	k2eAgFnPc7d1	městská
hradbami	hradba	k1gFnPc7	hradba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
menší	malý	k2eAgFnSc1d2	menší
zástavba	zástavba	k1gFnSc1	zástavba
4	[number]	k4	4
předměstských	předměstský	k2eAgFnPc2d1	předměstská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
moderního	moderní	k2eAgNnSc2d1	moderní
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gFnPc2	on
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
území	území	k1gNnSc6	území
Brna-středu	Brnatředa	k1gMnSc4	Brna-středa
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
směřujících	směřující	k2eAgFnPc6d1	směřující
k	k	k7c3	k
městu	město	k1gNnSc3	město
řada	řada	k1gFnSc1	řada
předměstí	předměstí	k1gNnSc4	předměstí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Brno	Brno	k1gNnSc4	Brno
obklopovaly	obklopovat	k5eAaImAgFnP	obklopovat
<g/>
:	:	kIx,	:
Cejl	Cejl	k1gInSc1	Cejl
<g/>
,	,	kIx,	,
Dornych	Dornych	k1gInSc1	Dornych
<g/>
,	,	kIx,	,
Křenová	křenový	k2eAgFnSc1d1	Křenová
<g/>
,	,	kIx,	,
Křídlovice	Křídlovice	k1gFnSc1	Křídlovice
(	(	kIx(	(
<g/>
podél	podél	k7c2	podél
Křídlovické	Křídlovický	k2eAgFnSc2d1	Křídlovická
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
Sady	sad	k1gInPc1	sad
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Pekařská	pekařský	k2eAgFnSc1d1	Pekařská
(	(	kIx(	(
<g/>
podél	podél	k7c2	podél
ulice	ulice	k1gFnSc2	ulice
Kopečné	Kopečné	k2eAgFnSc6d1	Kopečné
a	a	k8xC	a
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
ulice	ulice	k1gFnSc2	ulice
Pekařské	pekařský	k2eAgFnSc2d1	Pekařská
<g/>
)	)	kIx)	)
,	,	kIx,	,
Radlas	Radlas	k1gInSc1	Radlas
<g/>
,	,	kIx,	,
Statek	statek	k1gInSc1	statek
Kostela	kostel	k1gInSc2	kostel
Všech	všecek	k3xTgFnPc2	všecek
Svatých	svatá	k1gFnPc2	svatá
(	(	kIx(	(
<g/>
západně	západně	k6eAd1	západně
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
ulice	ulice	k1gFnSc2	ulice
Kopečné	Kopečná	k1gFnSc2	Kopečná
do	do	k7c2	do
ulice	ulice	k1gFnSc2	ulice
Pekařské	pekařský	k2eAgFnSc2d1	Pekařská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švábka	švábka	k1gFnSc1	švábka
(	(	kIx(	(
<g/>
podél	podél	k7c2	podél
dolní	dolní	k2eAgFnSc2d1	dolní
části	část	k1gFnSc2	část
Údolní	údolní	k2eAgFnSc2d1	údolní
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
(	(	kIx(	(
<g/>
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Lidické	lidický	k2eAgFnSc2d1	Lidická
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
těchto	tento	k3xDgInPc2	tento
předměstí	předměstí	k1gNnPc1	předměstí
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
postupně	postupně	k6eAd1	postupně
ustavily	ustavit	k5eAaPmAgFnP	ustavit
územní	územní	k2eAgFnPc1d1	územní
jednotky	jednotka	k1gFnPc1	jednotka
se	s	k7c7	s
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
správou	správa	k1gFnSc7	správa
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nelze	lze	k6eNd1	lze
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
předměstí	předměstí	k1gNnSc4	předměstí
<g/>
:	:	kIx,	:
Křížovnické	křížovnický	k2eAgNnSc1d1	Křížovnické
Území	území	k1gNnSc1	území
(	(	kIx(	(
<g/>
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
dolní	dolní	k2eAgFnSc4d1	dolní
část	část	k1gFnSc4	část
Pekařské	pekařský	k2eAgFnSc2d1	Pekařská
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
severozápad	severozápad	k1gInSc1	severozápad
území	území	k1gNnSc2	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Území	území	k1gNnSc1	území
Svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
areál	areál	k1gInSc1	areál
dnešní	dnešní	k2eAgFnSc2d1	dnešní
fakultní	fakultní	k2eAgFnSc2d1	fakultní
nemocnice	nemocnice	k1gFnSc2	nemocnice
U	u	k7c2	u
Svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
celků	celek	k1gInPc2	celek
podléhaly	podléhat	k5eAaImAgFnP	podléhat
Brnu	Brno	k1gNnSc3	Brno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpravidla	zpravidla	k6eAd1	zpravidla
měly	mít	k5eAaImAgFnP	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
správu	správa	k1gFnSc4	správa
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
brněnská	brněnský	k2eAgFnSc1d1	brněnská
magistrátní	magistrátní	k2eAgNnSc4d1	magistrátní
předměstí	předměstí	k1gNnSc4	předměstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
Brnu	Brno	k1gNnSc6	Brno
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
brněnský	brněnský	k2eAgInSc4d1	brněnský
sídelní	sídelní	k2eAgInSc4d1	sídelní
celek	celek	k1gInSc4	celek
Staré	Staré	k2eAgNnSc4d1	Staré
Brno	Brno	k1gNnSc4	Brno
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
městysem	městys	k1gInSc7	městys
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1366	[number]	k4	1366
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
západě	západ	k1gInSc6	západ
území	území	k1gNnSc2	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
v	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Pisárky	Pisárka	k1gFnSc2	Pisárka
doložena	doložit	k5eAaPmNgFnS	doložit
existence	existence	k1gFnSc1	existence
vodního	vodní	k2eAgInSc2d1	vodní
mlýna	mlýn	k1gInSc2	mlýn
zvaného	zvaný	k2eAgMnSc2d1	zvaný
Kamenný	kamenný	k2eAgInSc4d1	kamenný
mlýn	mlýn	k1gInSc4	mlýn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
pozemky	pozemek	k1gInPc1	pozemek
náležely	náležet	k5eAaImAgInP	náležet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
k	k	k7c3	k
Žabovřeskám	Žabovřesky	k1gFnPc3	Žabovřesky
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
nechalo	nechat	k5eAaPmAgNnS	nechat
město	město	k1gNnSc1	město
preventivně	preventivně	k6eAd1	preventivně
spálit	spálit	k5eAaPmF	spálit
některé	některý	k3yIgInPc4	některý
domy	dům	k1gInPc4	dům
v	v	k7c6	v
předměstských	předměstský	k2eAgFnPc6d1	předměstská
čtvrtích	čtvrt	k1gFnPc6	čtvrt
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
je	on	k3xPp3gInPc4	on
mohli	moct	k5eAaImAgMnP	moct
využít	využít	k5eAaPmF	využít
husité	husita	k1gMnPc1	husita
při	při	k7c6	při
případném	případný	k2eAgNnSc6d1	případné
obléhání	obléhání	k1gNnSc6	obléhání
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
pak	pak	k6eAd1	pak
město	město	k1gNnSc4	město
neúspěšně	úspěšně	k6eNd1	úspěšně
obléhali	obléhat	k5eAaImAgMnP	obléhat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1428	[number]	k4	1428
a	a	k8xC	a
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1430	[number]	k4	1430
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
pevnou	pevný	k2eAgFnSc7d1	pevná
baštou	bašta	k1gFnSc7	bašta
katolictví	katolictví	k1gNnSc2	katolictví
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
přispěl	přispět	k5eAaPmAgMnS	přispět
jeho	jeho	k3xOp3gInSc4	jeho
převážně	převážně	k6eAd1	převážně
německý	německý	k2eAgInSc4d1	německý
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
město	město	k1gNnSc4	město
nedobyli	dobýt	k5eNaPmAgMnP	dobýt
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
však	však	k9	však
poškodili	poškodit	k5eAaPmAgMnP	poškodit
Staré	Staré	k2eAgNnSc4d1	Staré
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1429	[number]	k4	1429
<g/>
)	)	kIx)	)
či	či	k8xC	či
Švábku	švábka	k1gFnSc4	švábka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1440-1453	[number]	k4	1440-1453
byl	být	k5eAaImAgInS	být
Špilberk	Špilberk	k1gInSc1	Špilberk
poprvé	poprvé	k6eAd1	poprvé
součástí	součást	k1gFnSc7	součást
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
pak	pak	k6eAd1	pak
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1560	[number]	k4	1560
<g/>
-	-	kIx~	-
<g/>
1620	[number]	k4	1620
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1454	[number]	k4	1454
vypověděl	vypovědět	k5eAaPmAgInS	vypovědět
židy	žid	k1gMnPc4	žid
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
král	král	k1gMnSc1	král
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pohrobek	pohrobek	k1gMnSc1	pohrobek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1467-1468	[number]	k4	1467-1468
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
ostřelováno	ostřelovat	k5eAaImNgNnS	ostřelovat
špilberskou	špilberský	k2eAgFnSc7d1	špilberská
posádkou	posádka	k1gFnSc7	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
města	město	k1gNnSc2	město
uvedeni	uvést	k5eAaPmNgMnP	uvést
kapucíni	kapucín	k1gMnPc1	kapucín
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
prozatímně	prozatímně	k6eAd1	prozatímně
přidělila	přidělit	k5eAaPmAgFnS	přidělit
kostel	kostel	k1gInSc4	kostel
Sv.	sv.	kA	sv.
Máří	Máří	k?	Máří
Magdalény	Magdaléna	k1gFnSc2	Magdaléna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1619	[number]	k4	1619
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
přidalo	přidat	k5eAaPmAgNnS	přidat
ke	k	k7c3	k
stavovskému	stavovský	k2eAgNnSc3d1	Stavovské
povstání	povstání	k1gNnSc3	povstání
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
svojí	svůj	k3xOyFgFnSc6	svůj
kapitulaci	kapitulace	k1gFnSc6	kapitulace
roku	rok	k1gInSc2	rok
1621	[number]	k4	1621
potrestáno	potrestán	k2eAgNnSc4d1	potrestáno
a	a	k8xC	a
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
Špilberk	Špilberk	k1gInSc4	Špilberk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1619	[number]	k4	1619
<g/>
,	,	kIx,	,
1622	[number]	k4	1622
<g/>
,	,	kIx,	,
1625	[number]	k4	1625
<g/>
,	,	kIx,	,
1643	[number]	k4	1643
<g/>
,	,	kIx,	,
1646	[number]	k4	1646
a	a	k8xC	a
1648	[number]	k4	1648
řádil	řádit	k5eAaImAgInS	řádit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
mor	mora	k1gFnPc2	mora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1641	[number]	k4	1641
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Olomouce	Olomouc	k1gFnSc2	Olomouc
švédskými	švédský	k2eAgNnPc7d1	švédské
vojsky	vojsko	k1gNnPc7	vojsko
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
stalo	stát	k5eAaPmAgNnS	stát
faktickým	faktický	k2eAgNnSc7d1	faktické
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Moravy	Morava	k1gFnSc2	Morava
mj.	mj.	kA	mj.
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
sem	sem	k6eAd1	sem
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
narychlo	narychlo	k6eAd1	narychlo
přemístěny	přemístěn	k2eAgFnPc4d1	přemístěna
Zemské	zemský	k2eAgFnPc4d1	zemská
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
sporu	spor	k1gInSc2	spor
obou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
učinil	učinit	k5eAaImAgMnS	učinit
však	však	k9	však
až	až	k9	až
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
markrabě	markrabě	k1gMnSc1	markrabě
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přiznal	přiznat	k5eAaPmAgMnS	přiznat
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
označení	označení	k1gNnSc4	označení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
definitivně	definitivně	k6eAd1	definitivně
Brnu	Brno	k1gNnSc3	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1643	[number]	k4	1643
a	a	k8xC	a
1645	[number]	k4	1645
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
neúspěšně	úspěšně	k6eNd1	úspěšně
obléháno	obléhat	k5eAaImNgNnS	obléhat
osmnáctitisícovým	osmnáctitisícový	k2eAgNnSc7d1	osmnáctitisícové
švédským	švédský	k2eAgNnSc7d1	švédské
vojskem	vojsko	k1gNnSc7	vojsko
generála	generál	k1gMnSc2	generál
Torstensona	Torstenson	k1gMnSc2	Torstenson
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
Brno	Brno	k1gNnSc4	Brno
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
základnu	základna	k1gFnSc4	základna
pro	pro	k7c4	pro
finální	finální	k2eAgInSc4d1	finální
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
švédském	švédský	k2eAgNnSc6d1	švédské
obléhání	obléhání	k1gNnSc6	obléhání
Brna	Brno	k1gNnSc2	Brno
zapálili	zapálit	k5eAaPmAgMnP	zapálit
jeho	jeho	k3xOp3gMnPc7	jeho
obránci	obránce	k1gMnPc7	obránce
klášter	klášter	k1gInSc1	klášter
františkánů	františkán	k1gMnPc2	františkán
pod	pod	k7c7	pod
Petrovem	Petrov	k1gInSc7	Petrov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zabránily	zabránit	k5eAaPmAgFnP	zabránit
Švédům	Švéd	k1gMnPc3	Švéd
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
případném	případný	k2eAgNnSc6d1	případné
využití	využití	k1gNnSc6	využití
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
na	na	k7c4	na
klášter	klášter	k1gInSc4	klášter
františkánek	františkánka	k1gFnPc2	františkánka
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc4	Josef
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
i	i	k9	i
katedrálu	katedrála	k1gFnSc4	katedrála
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zničil	zničit	k5eAaPmAgInS	zničit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
knihovnu	knihovna	k1gFnSc4	knihovna
a	a	k8xC	a
archiv	archiv	k1gInSc4	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Sami	sám	k3xTgMnPc1	sám
Švédové	Švéd	k1gMnPc1	Švéd
tehdy	tehdy	k6eAd1	tehdy
velké	velký	k2eAgFnPc4d1	velká
škody	škoda	k1gFnPc4	škoda
nenapáchali	napáchat	k5eNaBmAgMnP	napáchat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
švédské	švédský	k2eAgNnSc4d1	švédské
obléhání	obléhání	k1gNnSc4	obléhání
Brna	Brno	k1gNnSc2	Brno
nechalo	nechat	k5eAaPmAgNnS	nechat
město	město	k1gNnSc4	město
preventivně	preventivně	k6eAd1	preventivně
vypálit	vypálit	k5eAaPmF	vypálit
předměstí	předměstí	k1gNnSc4	předměstí
Cejl	Cejla	k1gFnPc2	Cejla
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
Sady	sad	k1gInPc1	sad
<g/>
,	,	kIx,	,
Trnitou	trnitý	k2eAgFnSc7d1	trnitá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
Švédové	Švéd	k1gMnPc1	Švéd
nemohli	moct	k5eNaImAgMnP	moct
využít	využít	k5eAaPmF	využít
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhém	druhý	k4xOgNnSc6	druhý
obléhání	obléhání	k1gNnSc6	obléhání
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
Torstensonovu	Torstensonův	k2eAgNnSc3d1	Torstensonův
vojsku	vojsko	k1gNnSc3	vojsko
přidalo	přidat	k5eAaPmAgNnS	přidat
ještě	ještě	k9	ještě
desetitisícové	desetitisícový	k2eAgNnSc1d1	desetitisícové
vojsko	vojsko	k1gNnSc1	vojsko
sedmihradského	sedmihradský	k2eAgMnSc2d1	sedmihradský
knížete	kníže	k1gMnSc2	kníže
Jiřího	Jiří	k1gMnSc2	Jiří
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákócziho	Rákóczize	k6eAd1	Rákóczize
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
část	část	k1gFnSc1	část
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
zanedlouho	zanedlouho	k6eAd1	zanedlouho
odvolána	odvolat	k5eAaPmNgFnS	odvolat
k	k	k7c3	k
Lednici	Lednice	k1gFnSc3	Lednice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
obléhání	obléhání	k1gNnSc1	obléhání
město	město	k1gNnSc4	město
bránilo	bránit	k5eAaImAgNnS	bránit
pouze	pouze	k6eAd1	pouze
1476	[number]	k4	1476
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vojáci	voják	k1gMnPc1	voják
tvořili	tvořit	k5eAaImAgMnP	tvořit
necelou	celý	k2eNgFnSc4d1	necelá
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tvrdosti	tvrdost	k1gFnSc3	tvrdost
a	a	k8xC	a
nasazení	nasazení	k1gNnSc4	nasazení
obránců	obránce	k1gMnPc2	obránce
a	a	k8xC	a
geniální	geniální	k2eAgFnSc4d1	geniální
organizaci	organizace	k1gFnSc4	organizace
obrany	obrana	k1gFnSc2	obrana
Raduitem	Raduit	k1gInSc7	Raduit
de	de	k?	de
Souches	Souchesa	k1gFnPc2	Souchesa
však	však	k9	však
Švédové	Švéd	k1gMnPc1	Švéd
neuspěli	uspět	k5eNaPmAgMnP	uspět
a	a	k8xC	a
s	s	k7c7	s
osmitisícovými	osmitisícový	k2eAgFnPc7d1	osmitisícová
ztrátami	ztráta	k1gFnPc7	ztráta
byli	být	k5eAaImAgMnP	být
donuceni	donutit	k5eAaPmNgMnP	donutit
obléhání	obléhání	k1gNnSc1	obléhání
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
obléhání	obléhání	k1gNnSc6	obléhání
však	však	k9	však
těžce	těžce	k6eAd1	těžce
poškodili	poškodit	k5eAaPmAgMnP	poškodit
klášterní	klášterní	k2eAgInSc4d1	klášterní
areál	areál	k1gInSc4	areál
na	na	k7c4	na
Území	území	k1gNnSc4	území
Svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc4	kostel
Všech	všecek	k3xTgMnPc2	všecek
Svatých	svatý	k1gMnPc2	svatý
ve	v	k7c6	v
Statku	statek	k1gInSc6	statek
Kostela	kostel	k1gInSc2	kostel
Všech	všecek	k3xTgMnPc2	všecek
Svatých	svatý	k1gMnPc2	svatý
<g/>
,	,	kIx,	,
a	a	k8xC	a
zapálili	zapálit	k5eAaPmAgMnP	zapálit
opuštěné	opuštěný	k2eAgInPc4d1	opuštěný
domy	dům	k1gInPc4	dům
obyvatel	obyvatel	k1gMnPc2	obyvatel
Starého	Starého	k2eAgNnSc2d1	Starého
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
města	město	k1gNnSc2	město
a	a	k8xC	a
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
obraně	obrana	k1gFnSc6	obrana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1643	[number]	k4	1643
a	a	k8xC	a
1645	[number]	k4	1645
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
zástavba	zástavba	k1gFnSc1	zástavba
středověkých	středověký	k2eAgFnPc2d1	středověká
předměstských	předměstský	k2eAgFnPc2d1	předměstská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
město	město	k1gNnSc1	město
nechalo	nechat	k5eAaPmAgNnS	nechat
také	také	k9	také
preventivně	preventivně	k6eAd1	preventivně
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhém	druhý	k4xOgNnSc6	druhý
švédském	švédský	k2eAgNnSc6d1	švédské
obléhání	obléhání	k1gNnSc6	obléhání
zůstala	zůstat	k5eAaPmAgFnS	zůstat
částečně	částečně	k6eAd1	částečně
poškozena	poškodit	k5eAaPmNgFnS	poškodit
i	i	k8xC	i
předměstí	předměstí	k1gNnSc6	předměstí
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
Pekařská	pekařský	k2eAgFnSc1d1	Pekařská
<g/>
,	,	kIx,	,
Švábka	švábka	k1gFnSc1	švábka
a	a	k8xC	a
nevelká	velký	k2eNgFnSc1d1	nevelká
zástavba	zástavba	k1gFnSc1	zástavba
Křížovnického	křížovnický	k2eAgNnSc2d1	Křížovnické
Území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Klášterní	klášterní	k2eAgFnSc1d1	klášterní
zástavba	zástavba	k1gFnSc1	zástavba
Území	území	k1gNnSc2	území
Svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
i	i	k8xC	i
zástavba	zástavba	k1gFnSc1	zástavba
předměstí	předměstí	k1gNnSc2	předměstí
Cejl	Cejla	k1gFnPc2	Cejla
<g/>
,	,	kIx,	,
Nových	Nových	k2eAgInPc2d1	Nových
Sadů	sad	k1gInPc2	sad
<g/>
,	,	kIx,	,
Švábky	švábka	k1gFnSc2	švábka
<g/>
,	,	kIx,	,
Trnité	trnitý	k2eAgFnSc2d1	trnitá
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
obnovení	obnovení	k1gNnSc4	obnovení
a	a	k8xC	a
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
Statek	statek	k1gInSc4	statek
Kostela	kostel	k1gInSc2	kostel
Všech	všecek	k3xTgFnPc2	všecek
Svatých	svatá	k1gFnPc2	svatá
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
magistrátnímu	magistrátní	k2eAgInSc3d1	magistrátní
předměstí	předměstí	k1gNnPc1	předměstí
Pekařská	pekařský	k2eAgFnSc1d1	Pekařská
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
přeměněno	přeměnit	k5eAaPmNgNnS	přeměnit
v	v	k7c4	v
barokní	barokní	k2eAgFnSc4d1	barokní
pevnost	pevnost	k1gFnSc4	pevnost
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
bastionů	bastion	k1gInPc2	bastion
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
konci	konec	k1gInSc6	konec
ulice	ulice	k1gFnSc2	ulice
Veveří	veveří	k2eAgNnSc4d1	veveří
nové	nový	k2eAgNnSc4d1	nové
předměstí	předměstí	k1gNnSc4	předměstí
Malá	malý	k2eAgFnSc1d1	malá
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
navázalo	navázat	k5eAaPmAgNnS	navázat
na	na	k7c4	na
zaniklé	zaniklý	k2eAgNnSc4d1	zaniklé
osídlení	osídlení	k1gNnSc4	osídlení
někdejší	někdejší	k2eAgFnSc2d1	někdejší
Hartlůvky	Hartlůvka	k1gFnSc2	Hartlůvka
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
středověké	středověký	k2eAgFnSc2d1	středověká
IV	IV	kA	IV
<g/>
.	.	kIx.	.
předměstské	předměstský	k2eAgFnSc2d1	předměstská
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zaniklé	zaniklý	k2eAgMnPc4d1	zaniklý
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1643	[number]	k4	1643
a	a	k8xC	a
1645	[number]	k4	1645
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
škodám	škoda	k1gFnPc3	škoda
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
město	město	k1gNnSc4	město
neúspěšně	úspěšně	k6eNd1	úspěšně
obléhali	obléhat	k5eAaImAgMnP	obléhat
Prusové	Prus	k1gMnPc1	Prus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
centrem	centr	k1gInSc7	centr
nově	nově	k6eAd1	nově
založeného	založený	k2eAgNnSc2d1	založené
biskupství	biskupství	k1gNnSc2	biskupství
<g/>
,	,	kIx,	,
podléhajícího	podléhající	k2eAgNnSc2d1	podléhající
však	však	k9	však
Olomouckému	olomoucký	k2eAgNnSc3d1	olomoucké
arcibiskupství	arcibiskupství	k1gNnSc3	arcibiskupství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
území	území	k1gNnSc6	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
třetině	třetina	k1gFnSc6	třetina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nových	nový	k2eAgMnPc2d1	nový
tzv.	tzv.	kA	tzv.
Josefinských	Josefinský	k2eAgMnPc2d1	Josefinský
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
:	:	kIx,	:
Horní	horní	k2eAgInSc1d1	horní
Cejl	Cejl	k1gInSc1	Cejl
<g/>
,	,	kIx,	,
Hráze	hráze	k1gFnSc1	hráze
<g/>
,	,	kIx,	,
Jircháře	jirchář	k1gMnPc4	jirchář
<g/>
,	,	kIx,	,
Josefov	Josefov	k1gInSc1	Josefov
<g/>
,	,	kIx,	,
Kožená	kožený	k2eAgFnSc1d1	kožená
<g/>
,	,	kIx,	,
Příkop	příkop	k1gInSc1	příkop
<g/>
,	,	kIx,	,
Silniční	silniční	k2eAgInSc1d1	silniční
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1786	[number]	k4	1786
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
založen	založen	k2eAgMnSc1d1	založen
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
<g/>
,	,	kIx,	,
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Velká	velký	k2eAgFnSc1d1	velká
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgInSc1d3	veliký
brněnský	brněnský	k2eAgInSc1d1	brněnský
park	park	k1gInSc1	park
Lužánky	Lužánka	k1gFnSc2	Lužánka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1805	[number]	k4	1805
a	a	k8xC	a
1809	[number]	k4	1809
město	město	k1gNnSc1	město
obsadil	obsadit	k5eAaPmAgMnS	obsadit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jenž	k3xRgInSc2	jenž
druhého	druhý	k4xOgInSc2	druhý
pobytu	pobyt	k1gInSc2	pobyt
započala	započnout	k5eAaPmAgFnS	započnout
likvidace	likvidace	k1gFnSc1	likvidace
bastionů	bastion	k1gInPc2	bastion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
bylo	být	k5eAaImAgNnS	být
císařským	císařský	k2eAgInSc7d1	císařský
dekretem	dekret	k1gInSc7	dekret
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
založeno	založit	k5eAaPmNgNnS	založit
Františkovo	Františkův	k2eAgNnSc1d1	Františkovo
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
druhé	druhý	k4xOgNnSc4	druhý
nejstarší	starý	k2eAgNnSc4d3	nejstarší
(	(	kIx(	(
<g/>
po	po	k7c6	po
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
)	)	kIx)	)
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
západě	západ	k1gInSc6	západ
území	území	k1gNnSc2	území
Brna-středu	Brnatředa	k1gMnSc4	Brna-středa
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejjižnější	jižní	k2eAgFnSc6d3	nejjižnější
části	část	k1gFnSc6	část
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnPc1	obec
Žabovřesky	Žabovřesky	k1gFnPc1	Žabovřesky
<g/>
,	,	kIx,	,
osada	osada	k1gFnSc1	osada
Kamenný	kamenný	k2eAgInSc4d1	kamenný
Mlýn	mlýn	k1gInSc4	mlýn
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
stala	stát	k5eAaPmAgFnS	stát
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
obcí	obec	k1gFnSc7	obec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Křenové	křenový	k2eAgFnSc2d1	Křenová
ulice	ulice	k1gFnSc2	ulice
nové	nový	k2eAgNnSc4d1	nové
předměstí	předměstí	k1gNnSc4	předměstí
Olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
1839	[number]	k4	1839
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
Vídní	Vídeň	k1gFnSc7	Vídeň
železnicí	železnice	k1gFnSc7	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
veřejné	veřejný	k2eAgNnSc1d1	veřejné
osvětlení	osvětlení	k1gNnSc1	osvětlení
plynovými	plynový	k2eAgFnPc7d1	plynová
lampami	lampa	k1gFnPc7	lampa
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
zaveden	zavést	k5eAaPmNgInS	zavést
telegraf	telegraf	k1gInSc1	telegraf
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1850	[number]	k4	1850
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
Brna	Brno	k1gNnSc2	Brno
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
o	o	k7c4	o
19	[number]	k4	19
dalších	další	k2eAgNnPc2d1	další
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgInSc1d1	dolní
a	a	k8xC	a
Horní	horní	k2eAgInSc1d1	horní
Cejl	Cejl	k1gInSc1	Cejl
<g/>
,	,	kIx,	,
Josefov	Josefov	k1gInSc1	Josefov
<g/>
,	,	kIx,	,
Kožená	kožený	k2eAgFnSc1d1	kožená
<g/>
,	,	kIx,	,
Křenová	křenový	k2eAgFnSc1d1	Křenová
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Křížová	Křížová	k1gFnSc1	Křížová
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
Náhon	náhon	k1gInSc1	náhon
<g/>
,	,	kIx,	,
Na	na	k7c4	na
Hrázi	hráze	k1gFnSc4	hráze
a	a	k8xC	a
Příkop	příkop	k1gInSc4	příkop
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
Sady	sad	k1gInPc1	sad
<g/>
,	,	kIx,	,
Pekařská	pekařský	k2eAgNnPc1d1	pekařské
<g/>
,	,	kIx,	,
Silniční	silniční	k2eAgNnPc1d1	silniční
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
Švábka	švábka	k1gFnSc1	švábka
<g/>
,	,	kIx,	,
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
<g/>
,	,	kIx,	,
U	u	k7c2	u
Svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
V	v	k7c6	v
Jirchářích	jirchář	k1gMnPc6	jirchář
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
a	a	k8xC	a
Červená	Červená	k1gFnSc1	Červená
<g/>
,	,	kIx,	,
Zábrdovice	Zábrdovice	k1gFnPc1	Zábrdovice
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
se	se	k3xPyFc4	se
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
na	na	k7c4	na
1	[number]	k4	1
732	[number]	k4	732
hektarů	hektar	k1gInPc2	hektar
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc4	jeho
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
území	území	k1gNnSc1	území
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zhruba	zhruba	k6eAd1	zhruba
krylo	krýt	k5eAaImAgNnS	krýt
s	s	k7c7	s
moderní	moderní	k2eAgFnSc7d1	moderní
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
rozšířením	rozšíření	k1gNnSc7	rozšíření
zároveň	zároveň	k6eAd1	zároveň
započalo	započnout	k5eAaPmAgNnS	započnout
připojování	připojování	k1gNnSc4	připojování
pozemků	pozemek	k1gInPc2	pozemek
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
původní	původní	k2eAgNnSc1d1	původní
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sousedním	sousední	k2eAgNnSc7d1	sousední
katastrálním	katastrální	k2eAgNnSc7d1	katastrální
územím	území	k1gNnSc7	území
Špilberku	Špilberk	k1gInSc2	Špilberk
stalo	stát	k5eAaPmAgNnS	stát
I.	I.	kA	I.
ze	z	k7c2	z
4	[number]	k4	4
nově	nova	k1gFnSc3	nova
zřízených	zřízený	k2eAgInPc2d1	zřízený
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
okresů	okres	k1gInPc2	okres
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stály	stát	k5eAaImAgInP	stát
okresní	okresní	k2eAgInPc1d1	okresní
výbory	výbor	k1gInPc1	výbor
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
představeným	představený	k1gMnSc7	představený
a	a	k8xC	a
náměstkem	náměstek	k1gMnSc7	náměstek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1859	[number]	k4	1859
-	-	kIx~	-
1864	[number]	k4	1864
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
zbořeno	zbořit	k5eAaPmNgNnS	zbořit
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc1d1	celé
městské	městský	k2eAgNnSc1d1	Městské
opevnění	opevnění	k1gNnSc1	opevnění
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
zachovaly	zachovat	k5eAaPmAgInP	zachovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přestavěnou	přestavěný	k2eAgFnSc7d1	přestavěná
Měnínskou	Měnínský	k2eAgFnSc7d1	Měnínská
bránou	brána	k1gFnSc7	brána
jen	jen	k9	jen
krátké	krátký	k2eAgInPc1d1	krátký
úseky	úsek	k1gInPc1	úsek
<g/>
,	,	kIx,	,
přiléhající	přiléhající	k2eAgMnSc1d1	přiléhající
k	k	k7c3	k
ulicím	ulice	k1gFnPc3	ulice
Husova	Husův	k2eAgNnSc2d1	Husovo
<g/>
,	,	kIx,	,
Bašty	bašta	k1gFnPc1	bašta
<g/>
,	,	kIx,	,
k	k	k7c3	k
Denisovým	Denisový	k2eAgInPc3d1	Denisový
Sadům	sad	k1gInPc3	sad
a	a	k8xC	a
ve	v	k7c6	v
dvoře	dvůr	k1gInSc6	dvůr
domu	dům	k1gInSc2	dům
Jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zboření	zboření	k1gNnSc6	zboření
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
započala	započnout	k5eAaPmAgFnS	započnout
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
výstavba	výstavba	k1gFnSc1	výstavba
nové	nový	k2eAgFnSc2d1	nová
zástavby	zástavba	k1gFnSc2	zástavba
a	a	k8xC	a
kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
čtvrti	čtvrt	k1gFnSc6	čtvrt
několik	několik	k4yIc4	několik
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
průmyslovému	průmyslový	k2eAgInSc3d1	průmyslový
rozvoji	rozvoj	k1gInSc3	rozvoj
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1869	[number]	k4	1869
zahájila	zahájit	k5eAaPmAgFnS	zahájit
provoz	provoz	k1gInSc4	provoz
koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1873	[number]	k4	1873
přinesl	přinést	k5eAaPmAgInS	přinést
první	první	k4xOgFnSc4	první
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
technický	technický	k2eAgInSc1d1	technický
institut	institut	k1gInSc1	institut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1881	[number]	k4	1881
a	a	k8xC	a
1882	[number]	k4	1882
bylo	být	k5eAaImAgNnS	být
nově	nově	k6eAd1	nově
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
osvětleno	osvětlen	k2eAgNnSc1d1	osvětleno
jako	jako	k8xS	jako
první	první	k4xOgNnSc1	první
evropské	evropský	k2eAgNnSc1d1	Evropské
divadlo	divadlo	k1gNnSc1	divadlo
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
žárovkami	žárovka	k1gFnPc7	žárovka
T.	T.	kA	T.
A.	A.	kA	A.
Edisona	Edison	k1gMnSc2	Edison
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
první	první	k4xOgNnSc4	první
české	český	k2eAgNnSc4d1	české
knihkupectví	knihkupectví	k1gNnSc4	knihkupectví
Joži	Joža	k1gMnSc2	Joža
Barviče	barvič	k1gMnSc2	barvič
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1896-1916	[number]	k4	1896-1916
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
provedena	proveden	k2eAgFnSc1d1	provedena
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
asanace	asanace	k1gFnSc1	asanace
(	(	kIx(	(
<g/>
přestavba	přestavba	k1gFnSc1	přestavba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
zbořeno	zbořit	k5eAaPmNgNnS	zbořit
238	[number]	k4	238
starých	starý	k2eAgInPc2d1	starý
domů	dům	k1gInPc2	dům
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
budov	budova	k1gFnPc2	budova
včetně	včetně	k7c2	včetně
Královské	královský	k2eAgFnSc2d1	královská
kaple	kaple	k1gFnSc2	kaple
(	(	kIx(	(
<g/>
zbořena	zbořen	k2eAgFnSc1d1	zbořena
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
původní	původní	k2eAgInSc1d1	původní
záměr	záměr	k1gInSc1	záměr
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
429	[number]	k4	429
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Asanaci	asanace	k1gFnSc4	asanace
předcházelo	předcházet	k5eAaImAgNnS	předcházet
roku	rok	k1gInSc3	rok
1870	[number]	k4	1870
zboření	zboření	k1gNnSc2	zboření
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
třetí	třetí	k4xOgFnSc2	třetí
třetiny	třetina	k1gFnSc2	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
území	území	k1gNnSc6	území
Brna-středu	Brnatředa	k1gMnSc4	Brna-středa
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
zástavby	zástavba	k1gFnSc2	zástavba
nových	nový	k2eAgFnPc2d1	nová
čtvrtí	čtvrt	k1gFnPc2	čtvrt
Veveří	veveří	k2eAgFnPc1d1	veveří
<g/>
,	,	kIx,	,
Lužánky	Lužánek	k1gInPc1	Lužánek
<g/>
,	,	kIx,	,
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
čtvrť	čtvrť	k1gFnSc1	čtvrť
a	a	k8xC	a
k	k	k7c3	k
zahušťování	zahušťování	k1gNnSc3	zahušťování
zástavby	zástavba	k1gFnSc2	zástavba
předměstí	předměstí	k1gNnSc2	předměstí
Křenová	křenový	k2eAgFnSc1d1	Křenová
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgInSc1d1	dolní
Cejl	Cejl	k1gInSc1	Cejl
a	a	k8xC	a
Horní	horní	k2eAgInSc1d1	horní
Cejl	Cejl	k1gInSc1	Cejl
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
původně	původně	k6eAd1	původně
hornoheršpických	hornoheršpický	k2eAgInPc6d1	hornoheršpický
pozemcích	pozemek	k1gInPc6	pozemek
založen	založen	k2eAgInSc1d1	založen
brněnský	brněnský	k2eAgInSc1d1	brněnský
ústřední	ústřední	k2eAgInSc1d1	ústřední
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
území	území	k1gNnSc1	území
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
stalo	stát	k5eAaPmAgNnS	stát
novým	nový	k2eAgNnSc7d1	nové
katastrálním	katastrální	k2eAgNnSc7d1	katastrální
územím	území	k1gNnSc7	území
Staré	Staré	k2eAgFnSc2d1	Staré
Brno	Brno	k1gNnSc1	Brno
II	II	kA	II
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
odděleno	oddělit	k5eAaPmNgNnS	oddělit
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
Brna	Brno	k1gNnSc2	Brno
<g/>
;	;	kIx,	;
zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Staré	Staré	k2eAgNnSc4d1	Staré
Brno	Brno	k1gNnSc4	Brno
I.	I.	kA	I.
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1898	[number]	k4	1898
a	a	k8xC	a
1903	[number]	k4	1903
bylo	být	k5eAaImAgNnS	být
k.	k.	k?	k.
ú.	ú.	k?	ú.
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
II	II	kA	II
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozšiřováním	rozšiřování	k1gNnSc7	rozšiřování
hřbitova	hřbitov	k1gInSc2	hřbitov
rozšiřováno	rozšiřovat	k5eAaImNgNnS	rozšiřovat
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
původně	původně	k6eAd1	původně
hornoheršpické	hornoheršpický	k2eAgInPc4d1	hornoheršpický
pozemky	pozemek	k1gInPc4	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
indikační	indikační	k2eAgFnSc2d1	indikační
skici	skica	k1gFnSc2	skica
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Černovice	Černovice	k1gFnSc2	Černovice
datovanou	datovaný	k2eAgFnSc4d1	datovaná
rokem	rok	k1gInSc7	rok
1873	[number]	k4	1873
(	(	kIx(	(
<g/>
mapa	mapa	k1gFnSc1	mapa
má	mít	k5eAaImIp3nS	mít
signaturu	signatura	k1gFnSc4	signatura
MOR038118730	MOR038118730	k1gFnPc2	MOR038118730
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
najít	najít	k5eAaPmF	najít
online	onlinout	k5eAaPmIp3nS	onlinout
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
http://www.mza.cz/indikacniskici/#	[url]	k?	http://www.mza.cz/indikacniskici/#
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1898	[number]	k4	1898
připojeny	připojit	k5eAaPmNgInP	připojit
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
pozemky	pozemka	k1gFnSc2	pozemka
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
obce	obec	k1gFnSc2	obec
Černovice	Černovice	k1gFnSc2	Černovice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
nyní	nyní	k6eAd1	nyní
tvoří	tvořit	k5eAaImIp3nS	tvořit
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
městské	městský	k2eAgFnSc2d1	městská
částí	část	k1gFnSc7	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
a	a	k8xC	a
severovýchod	severovýchod	k1gInSc1	severovýchod
sousední	sousední	k2eAgFnSc2d1	sousední
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-jih	Brnoih	k1gMnSc1	Brno-jih
(	(	kIx(	(
<g/>
na	na	k7c6	na
území	území	k1gNnSc6	území
Brna-středu	Brnatředa	k1gMnSc4	Brna-středa
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
Hladíkovu	Hladíkův	k2eAgFnSc4d1	Hladíkova
ulici	ulice	k1gFnSc4	ulice
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
úsek	úsek	k1gInSc4	úsek
ulice	ulice	k1gFnSc2	ulice
Mlýnská	mlýnský	k2eAgNnPc1d1	mlýnské
a	a	k8xC	a
části	část	k1gFnSc6	část
ulic	ulice	k1gFnPc2	ulice
Zvonařka	Zvonařka	k1gFnSc1	Zvonařka
a	a	k8xC	a
Masná	masný	k2eAgFnSc1d1	Masná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
regotizaci	regotizace	k1gFnSc3	regotizace
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
byly	být	k5eAaImAgInP	být
brněnské	brněnský	k2eAgInPc1d1	brněnský
samosprávné	samosprávný	k2eAgInPc1d1	samosprávný
okresy	okres	k1gInPc1	okres
zrušeny	zrušit	k5eAaPmNgInP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
vzniká	vznikat	k5eAaImIp3nS	vznikat
nové	nový	k2eAgNnSc4d1	nové
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Vídeňka	Vídeňka	k1gFnSc1	Vídeňka
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
obou	dva	k4xCgInPc2	dva
stávajících	stávající	k2eAgInPc2d1	stávající
starobrněnských	starobrněnský	k2eAgInPc2d1	starobrněnský
katastrů	katastr	k1gInPc2	katastr
staly	stát	k5eAaPmAgFnP	stát
další	další	k2eAgFnPc1d1	další
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hornoheršpické	hornoheršpický	k2eAgInPc1d1	hornoheršpický
pozemky	pozemek	k1gInPc1	pozemek
severně	severně	k6eAd1	severně
a	a	k8xC	a
východně	východně	k6eAd1	východně
od	od	k7c2	od
ústředního	ústřední	k2eAgInSc2d1	ústřední
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
rozšíření	rozšíření	k1gNnSc3	rozšíření
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
už	už	k6eAd1	už
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
náležely	náležet	k5eAaImAgInP	náležet
všechny	všechen	k3xTgInPc1	všechen
pozemky	pozemek	k1gInPc1	pozemek
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
získané	získaný	k2eAgInPc1d1	získaný
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
mimobrněnské	mimobrněnská	k1gFnPc1	mimobrněnská
<g/>
,	,	kIx,	,
pozemky	pozemek	k1gInPc1	pozemek
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
náležely	náležet	k5eAaImAgFnP	náležet
tehdy	tehdy	k6eAd1	tehdy
ke	k	k7c3	k
katastrům	katastr	k1gInPc3	katastr
těchto	tento	k3xDgFnPc2	tento
připojených	připojený	k2eAgFnPc2d1	připojená
obcí	obec	k1gFnPc2	obec
<g/>
:	:	kIx,	:
Bohunice	Bohunice	k1gFnPc1	Bohunice
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnPc1d1	horní
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
,	,	kIx,	,
Kamenný	kamenný	k2eAgInSc1d1	kamenný
Mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
Komárov	Komárov	k1gInSc1	Komárov
a	a	k8xC	a
Žabovřesky	Žabovřesky	k1gFnPc1	Žabovřesky
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
Brněnské	brněnský	k2eAgNnSc1d1	brněnské
výstaviště	výstaviště	k1gNnSc1	výstaviště
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
rozšiřované	rozšiřovaný	k2eAgNnSc1d1	rozšiřované
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939-1941	[number]	k4	1939-1941
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudován	k2eAgNnSc1d1	vybudováno
prodloužení	prodloužení	k1gNnSc1	prodloužení
Husovy	Husův	k2eAgFnSc2d1	Husova
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
propojení	propojení	k1gNnSc4	propojení
s	s	k7c7	s
Nádražní	nádražní	k2eAgFnSc7d1	nádražní
ulicí	ulice	k1gFnSc7	ulice
přes	přes	k7c4	přes
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
areál	areál	k1gInSc4	areál
Denisových	Denisový	k2eAgInPc2d1	Denisový
sadů	sad	k1gInPc2	sad
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
tento	tento	k3xDgInSc1	tento
park	park	k1gInSc1	park
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
a	a	k8xC	a
tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
park	park	k1gInSc4	park
Studánka	studánka	k1gFnSc1	studánka
na	na	k7c6	na
moderním	moderní	k2eAgInSc6d1	moderní
katastru	katastr	k1gInSc6	katastr
Starého	Starého	k2eAgNnSc2d1	Starého
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
komunismu	komunismus	k1gInSc2	komunismus
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
území	území	k1gNnSc6	území
Brna-středu	Brnatředa	k1gMnSc4	Brna-středa
k	k	k7c3	k
zásadním	zásadní	k2eAgFnPc3d1	zásadní
urbanistickým	urbanistický	k2eAgFnPc3d1	urbanistická
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
chápány	chápat	k5eAaImNgInP	chápat
negativně	negativně	k6eAd1	negativně
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
postavení	postavení	k1gNnSc4	postavení
socialistické	socialistický	k2eAgFnSc2d1	socialistická
tržnice	tržnice	k1gFnSc2	tržnice
na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
nebo	nebo	k8xC	nebo
přestavbu	přestavba	k1gFnSc4	přestavba
Starého	Starého	k2eAgNnSc2d1	Starého
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1962	[number]	k4	1962
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgInP	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
zasypány	zasypán	k2eAgFnPc4d1	zasypána
poslední	poslední	k2eAgFnPc4d1	poslední
viditelné	viditelný	k2eAgFnPc4d1	viditelná
části	část	k1gFnPc4	část
Svrateckého	svratecký	k2eAgInSc2d1	svratecký
náhonu	náhon	k1gInSc2	náhon
<g/>
,	,	kIx,	,
zbořena	zbořen	k2eAgFnSc1d1	zbořena
řada	řada	k1gFnSc1	řada
starších	starý	k2eAgFnPc2d2	starší
budov	budova	k1gFnPc2	budova
včetně	včetně	k7c2	včetně
někdejší	někdejší	k2eAgFnSc2d1	někdejší
starobrněnské	starobrněnský	k2eAgFnSc2d1	starobrněnský
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
postaveno	postaven	k2eAgNnSc4d1	postaveno
panelové	panelový	k2eAgNnSc4d1	panelové
sídliště	sídliště	k1gNnSc4	sídliště
Staré	Staré	k2eAgFnPc2d1	Staré
Brno-sever	Brnoevra	k1gFnPc2	Brno-sevra
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1958-1965	[number]	k4	1958-1965
postaveno	postavit	k5eAaPmNgNnS	postavit
Janáčkovo	Janáčkův	k2eAgNnSc1d1	Janáčkovo
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
hala	hala	k1gFnSc1	hala
Rondo	rondo	k1gNnSc4	rondo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
území	území	k1gNnSc6	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
k	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
stavebním	stavební	k2eAgMnPc3d1	stavební
zásahům	zásah	k1gInPc3	zásah
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
pokládány	pokládat	k5eAaImNgFnP	pokládat
za	za	k7c2	za
značně	značně	k6eAd1	značně
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
-	-	kIx~	-
například	například	k6eAd1	například
vybudování	vybudování	k1gNnSc1	vybudování
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
Velký	velký	k2eAgInSc1d1	velký
Špalíček	špalíček	k1gInSc1	špalíček
či	či	k8xC	či
pseudogotická	pseudogotický	k2eAgFnSc1d1	pseudogotická
puristická	puristický	k2eAgFnSc1d1	puristická
přestavba	přestavba	k1gFnSc1	přestavba
Špilberku	Špilberk	k1gInSc2	Špilberk
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
i	i	k9	i
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
rekonstrukcí	rekonstrukce	k1gFnPc2	rekonstrukce
za	za	k7c2	za
komunismu	komunismus	k1gInSc2	komunismus
chátrajících	chátrající	k2eAgFnPc2d1	chátrající
budov	budova	k1gFnPc2	budova
-	-	kIx~	-
například	například	k6eAd1	například
Dům	dům	k1gInSc1	dům
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
<g/>
,	,	kIx,	,
Petřvaldský	petřvaldský	k2eAgInSc4d1	petřvaldský
dům	dům	k1gInSc4	dům
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Obchodní	obchodní	k2eAgFnPc1d1	obchodní
galerie	galerie	k1gFnPc1	galerie
Orlí	orlí	k2eAgFnPc1d1	orlí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kleinův	Kleinův	k2eAgInSc1d1	Kleinův
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
či	či	k8xC	či
Městský	městský	k2eAgInSc1d1	městský
dvůr	dvůr	k1gInSc1	dvůr
na	na	k7c6	na
Šilingrově	Šilingrův	k2eAgNnSc6d1	Šilingrovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
území	území	k1gNnSc2	území
moderní	moderní	k2eAgFnPc1d1	moderní
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
staly	stát	k5eAaPmAgFnP	stát
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
první	první	k4xOgFnSc7	první
katastrální	katastrální	k2eAgFnSc7d1	katastrální
reformou	reforma	k1gFnSc7	reforma
Brna	Brno	k1gNnSc2	Brno
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
existovala	existovat	k5eAaImAgFnS	existovat
na	na	k7c6	na
území	území	k1gNnSc6	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
celá	celý	k2eAgFnSc1d1	celá
původní	původní	k2eAgFnSc1d1	původní
katastrální	katastrální	k2eAgFnSc1d1	katastrální
území	území	k1gNnSc4	území
Josefov	Josefov	k1gInSc1	Josefov
<g/>
,	,	kIx,	,
Kožená	kožený	k2eAgFnSc1d1	kožená
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Náhon	náhon	k1gInSc1	náhon
<g/>
,	,	kIx,	,
Na	na	k7c4	na
Hrázi	hráze	k1gFnSc4	hráze
a	a	k8xC	a
Příkop	příkop	k1gInSc4	příkop
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
Sady	sad	k1gInPc1	sad
<g/>
,	,	kIx,	,
Pekařská	pekařský	k2eAgFnSc1d1	Pekařská
<g/>
,	,	kIx,	,
Silniční	silniční	k2eAgFnSc1d1	silniční
<g/>
,	,	kIx,	,
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
Švábka	švábka	k1gFnSc1	švábka
<g/>
,	,	kIx,	,
U	u	k7c2	u
Svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
V	v	k7c6	v
Jirchářích	jirchář	k1gMnPc6	jirchář
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
části	část	k1gFnPc1	část
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
Bohunice	Bohunice	k1gFnPc1	Bohunice
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgFnPc1d1	dolní
a	a	k8xC	a
Horní	horní	k2eAgInSc1d1	horní
Cejl	Cejl	k1gInSc1	Cejl
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnPc1d1	horní
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
,	,	kIx,	,
Komárov	Komárov	k1gInSc1	Komárov
<g/>
,	,	kIx,	,
Křenová	křenový	k2eAgFnSc1d1	Křenová
<g/>
,	,	kIx,	,
Křížová	křížový	k2eAgFnSc1d1	křížová
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Vídeňka	Vídeňka	k1gFnSc1	Vídeňka
<g/>
,	,	kIx,	,
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
a	a	k8xC	a
Červená	Červená	k1gFnSc1	Červená
<g/>
,	,	kIx,	,
Zábrdovice	Zábrdovice	k1gFnPc1	Zábrdovice
<g/>
,	,	kIx,	,
Žabovřesky	Žabovřesky	k1gFnPc1	Žabovřesky
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
části	část	k1gFnSc2	část
Kamenného	kamenný	k2eAgInSc2d1	kamenný
Mlýna	mlýn	k1gInSc2	mlýn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
první	první	k4xOgFnSc6	první
katastrální	katastrální	k2eAgFnSc6d1	katastrální
reformě	reforma	k1gFnSc6	reforma
Brna	Brno	k1gNnSc2	Brno
řada	řada	k1gFnSc1	řada
malých	malý	k2eAgNnPc2d1	malé
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
rozdělením	rozdělení	k1gNnSc7	rozdělení
či	či	k8xC	či
přičleněním	přičlenění	k1gNnSc7	přičlenění
k	k	k7c3	k
větším	veliký	k2eAgNnPc3d2	veliký
katastrálním	katastrální	k2eAgNnPc3d1	katastrální
územím	území	k1gNnPc3	území
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
úpravám	úprava	k1gFnPc3	úprava
hranic	hranice	k1gFnPc2	hranice
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
existujících	existující	k2eAgInPc2d1	existující
katastrů	katastr	k1gInPc2	katastr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c6	na
území	území	k1gNnSc6	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
poté	poté	k6eAd1	poté
existovala	existovat	k5eAaImAgNnP	existovat
celá	celý	k2eAgNnPc1d1	celé
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
Sady	sad	k1gInPc1	sad
a	a	k8xC	a
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
části	část	k1gFnPc1	část
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
Bohunice	Bohunice	k1gFnPc1	Bohunice
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgFnPc1d1	dolní
a	a	k8xC	a
Horní	horní	k2eAgInSc1d1	horní
Cejl	Cejl	k1gInSc1	Cejl
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnPc1d1	horní
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
,	,	kIx,	,
Komárov	Komárov	k1gInSc1	Komárov
<g/>
,	,	kIx,	,
Křížová	Křížová	k1gFnSc1	Křížová
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Vídeňka	Vídeňka	k1gFnSc1	Vídeňka
<g/>
,	,	kIx,	,
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
a	a	k8xC	a
Červená	Červená	k1gFnSc1	Červená
<g/>
,	,	kIx,	,
Zábrdovice	Zábrdovice	k1gFnPc1	Zábrdovice
a	a	k8xC	a
Žabovřesky	Žabovřesky	k1gFnPc1	Žabovřesky
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
druhé	druhý	k4xOgFnSc6	druhý
katastrální	katastrální	k2eAgFnSc6d1	katastrální
reformě	reforma	k1gFnSc6	reforma
z	z	k7c2	z
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
značným	značný	k2eAgFnPc3d1	značná
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
nových	nový	k2eAgInPc2d1	nový
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
staré	stará	k1gFnPc1	stará
se	se	k3xPyFc4	se
rušily	rušit	k5eAaImAgFnP	rušit
nebo	nebo	k8xC	nebo
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
jejich	jejich	k3xOp3gFnPc2	jejich
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
členilo	členit	k5eAaImAgNnS	členit
na	na	k7c4	na
celá	celý	k2eAgNnPc4d1	celé
katastrální	katastrální	k2eAgNnPc4d1	katastrální
území	území	k1gNnPc4	území
Lužánky	Lužánka	k1gFnSc2	Lužánka
<g/>
,	,	kIx,	,
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Veveří	veveří	k2eAgFnSc1d1	veveří
<g/>
,	,	kIx,	,
Stránice	Stránice	k1gFnSc1	Stránice
<g/>
,	,	kIx,	,
Štýřice	Štýřice	k1gFnSc1	Štýřice
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
části	část	k1gFnSc2	část
katastrálních	katastrální	k2eAgFnPc2d1	katastrální
území	území	k1gNnPc2	území
Pisárky	Pisárka	k1gFnSc2	Pisárka
<g/>
,	,	kIx,	,
Ponava	Ponava	k1gFnSc1	Ponava
<g/>
,	,	kIx,	,
Trnitá	trnitý	k2eAgFnSc1d1	trnitá
a	a	k8xC	a
Zábrdovice	Zábrdovice	k1gFnPc1	Zábrdovice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
když	když	k8xS	když
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Lužánky	Lužánka	k1gFnSc2	Lužánka
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
rozdělení	rozdělení	k1gNnSc1	rozdělení
mezi	mezi	k7c4	mezi
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Veveří	veveří	k2eAgFnSc1d1	veveří
a	a	k8xC	a
Černá	černý	k2eAgFnSc1d1	černá
Pole	pole	k1gFnSc1	pole
a	a	k8xC	a
k	k	k7c3	k
přičlenění	přičlenění	k1gNnSc3	přičlenění
zdejší	zdejší	k2eAgFnSc2d1	zdejší
části	část	k1gFnSc2	část
Ponavy	Ponava	k1gFnSc2	Ponava
ke	k	k7c3	k
katastrálnímu	katastrální	k2eAgNnSc3d1	katastrální
území	území	k1gNnSc3	území
Veveří	veveří	k2eAgFnPc1d1	veveří
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
moderní	moderní	k2eAgFnSc2d1	moderní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
prodělalo	prodělat	k5eAaPmAgNnS	prodělat
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
době	doba	k1gFnSc6	doba
komunismu	komunismus	k1gInSc2	komunismus
velice	velice	k6eAd1	velice
dynamický	dynamický	k2eAgInSc1d1	dynamický
správní	správní	k2eAgInSc1d1	správní
vývoj	vývoj	k1gInSc1	vývoj
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Brna	Brno	k1gNnSc2	Brno
měnila	měnit	k5eAaImAgFnS	měnit
správní	správní	k2eAgFnSc1d1	správní
příslušnost	příslušnost	k1gFnSc1	příslušnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
moderní	moderní	k2eAgFnSc1d1	moderní
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
ke	k	k7c3	k
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
a	a	k8xC	a
původně	původně	k6eAd1	původně
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
větší	veliký	k2eAgNnSc4d2	veliký
území	území	k1gNnSc4	území
než	než	k8xS	než
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
:	:	kIx,	:
na	na	k7c6	na
západě	západ	k1gInSc6	západ
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Pisárky	Pisárka	k1gFnSc2	Pisárka
vedla	vést	k5eAaImAgFnS	vést
část	část	k1gFnSc1	část
její	její	k3xOp3gFnSc2	její
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-Kohoutovice	Brno-Kohoutovice	k1gFnSc2	Brno-Kohoutovice
ulicí	ulice	k1gFnSc7	ulice
Veslařskou	veslařský	k2eAgFnSc7d1	Veslařská
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
pak	pak	k6eAd1	pak
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
celou	celý	k2eAgFnSc4d1	celá
pravobřežní	pravobřežní	k2eAgFnSc4d1	pravobřežní
část	část	k1gFnSc4	část
moderního	moderní	k2eAgNnSc2d1	moderní
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Zábrdovice	Zábrdovice	k1gFnPc1	Zábrdovice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
zmenšení	zmenšení	k1gNnSc2	zmenšení
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nabyla	nabýt	k5eAaPmAgFnS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
hranice	hranice	k1gFnSc2	hranice
se	s	k7c7	s
sousední	sousední	k2eAgFnSc7d1	sousední
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-Jundrov	Brno-Jundrovo	k1gNnPc2	Brno-Jundrovo
<g/>
,	,	kIx,	,
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
dohody	dohoda	k1gFnSc2	dohoda
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
předala	předat	k5eAaPmAgFnS	předat
část	část	k1gFnSc1	část
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Pisárky	Pisárka	k1gFnSc2	Pisárka
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Jundrov	Brno-Jundrov	k1gInSc1	Brno-Jundrov
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
zmenšení	zmenšení	k1gNnSc3	zmenšení
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc7d1	poslední
změnou	změna	k1gFnSc7	změna
jejího	její	k3xOp3gNnSc2	její
vymezení	vymezení	k1gNnSc2	vymezení
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc3	květen
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nabyla	nabýt	k5eAaPmAgFnS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
hranice	hranice	k1gFnSc2	hranice
se	s	k7c7	s
sousední	sousední	k2eAgFnSc7d1	sousední
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-sever	Brnoevra	k1gFnPc2	Brno-sevra
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgInPc1	který
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
předala	předat	k5eAaPmAgFnS	předat
výše	výše	k1gFnSc1	výše
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
sousední	sousední	k2eAgFnSc2d1	sousední
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
část	část	k1gFnSc4	část
katastrálního	katastrální	k2eAgInSc2d1	katastrální
území	území	k1gNnSc4	území
Zábrdovice	Zábrdovice	k1gFnPc4	Zábrdovice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nová	nový	k2eAgFnSc1d1	nová
hranice	hranice	k1gFnSc1	hranice
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
linií	linie	k1gFnSc7	linie
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
ulicemi	ulice	k1gFnPc7	ulice
Francouzskou	francouzský	k2eAgFnSc7d1	francouzská
<g/>
,	,	kIx,	,
Hvězdovou	Hvězdův	k2eAgFnSc7d1	Hvězdova
<g/>
,	,	kIx,	,
Bratislavskou	bratislavský	k2eAgFnSc7d1	Bratislavská
<g/>
,	,	kIx,	,
Soudní	soudní	k2eAgFnSc7d1	soudní
<g/>
,	,	kIx,	,
Cejlem	Cejl	k1gInSc7	Cejl
a	a	k8xC	a
Tkalcovskou	tkalcovský	k2eAgFnSc7d1	tkalcovská
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pešek	Pešek	k1gMnSc1	Pešek
</s>
