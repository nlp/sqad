<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
(	(	kIx(	(
<g/>
samostatných	samostatný	k2eAgFnPc2d1	samostatná
i	i	k9	i
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
druhy	druh	k1gInPc7	druh
rybáků	rybák	k1gMnPc2	rybák
nebo	nebo	k8xC	nebo
racků	racek	k1gMnPc2	racek
<g/>
)	)	kIx)	)
na	na	k7c6	na
mořských	mořský	k2eAgInPc6d1	mořský
pobřežích	pobřeží	k1gNnPc6	pobřeží
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
u	u	k7c2	u
velkých	velký	k2eAgFnPc2d1	velká
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
i	i	k8xC	i
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
