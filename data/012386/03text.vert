<p>
<s>
Rybák	Rybák	k1gMnSc1	Rybák
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Sterna	sternum	k1gNnSc2	sternum
hirundo	hirundo	k6eAd1	hirundo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
rybáka	rybák	k1gMnSc2	rybák
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
černou	černý	k2eAgFnSc4d1	černá
čepičku	čepička	k1gFnSc4	čepička
a	a	k8xC	a
červený	červený	k2eAgInSc4d1	červený
zobák	zobák	k1gInSc4	zobák
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
špičkou	špička	k1gFnSc7	špička
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
hluboce	hluboko	k6eAd1	hluboko
vykrojený	vykrojený	k2eAgInSc4d1	vykrojený
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
<g/>
:	:	kIx,	:
Délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
34	[number]	k4	34
<g/>
–	–	k?	–
<g/>
37	[number]	k4	37
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
:	:	kIx,	:
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
90	[number]	k4	90
<g/>
–	–	k?	–
<g/>
175	[number]	k4	175
g.	g.	k?	g.
</s>
</p>
<p>
<s>
Šat	šat	k1gInSc1	šat
svatební	svatební	k2eAgFnSc2d1	svatební
–	–	k?	–
černá	černý	k2eAgFnSc1d1	černá
čepička	čepička	k1gFnSc1	čepička
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
od	od	k7c2	od
čela	čelo	k1gNnSc2	čelo
na	na	k7c6	na
šíji	šíj	k1gFnSc6	šíj
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
pod	pod	k7c4	pod
oko	oko	k1gNnSc4	oko
a	a	k8xC	a
do	do	k7c2	do
příuší	příuše	k1gFnPc2	příuše
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc1d1	dolní
část	část	k1gFnSc1	část
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
strany	strana	k1gFnSc2	strana
krku	krk	k1gInSc2	krk
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
spodina	spodina	k1gFnSc1	spodina
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
,	,	kIx,	,
vrch	vrch	k1gInSc1	vrch
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
kostřec	kostřec	k1gInSc4	kostřec
šedé	šedá	k1gFnSc2	šedá
<g/>
.	.	kIx.	.
</s>
<s>
Ocasní	ocasní	k2eAgFnPc4d1	ocasní
krovky	krovka	k1gFnPc4	krovka
a	a	k8xC	a
ocas	ocas	k1gInSc4	ocas
bílé	bílé	k1gNnSc4	bílé
<g/>
,	,	kIx,	,
pera	pero	k1gNnPc1	pero
na	na	k7c6	na
ocase	ocas	k1gInSc6	ocas
s	s	k7c7	s
šedým	šedý	k2eAgInSc7d1	šedý
vnějším	vnější	k2eAgInSc7d1	vnější
praporem	prapor	k1gInSc7	prapor
<g/>
,	,	kIx,	,
krajní	krajní	k2eAgMnPc1d1	krajní
s	s	k7c7	s
černým	černý	k1gMnSc7	černý
<g/>
.	.	kIx.	.
</s>
<s>
Krajní	krajní	k2eAgInPc1d1	krajní
ruční	ruční	k2eAgInPc1d1	ruční
letky	letek	k1gInPc1	letek
tmavošedé	tmavošedý	k2eAgInPc1d1	tmavošedý
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
do	do	k7c2	do
černa	černo	k1gNnSc2	černo
<g/>
,	,	kIx,	,
na	na	k7c6	na
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
praporu	prapor	k1gInSc6	prapor
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc1	zobák
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgInSc1d1	červený
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
špicí	špice	k1gFnSc7	špice
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
jedinců	jedinec	k1gMnPc2	jedinec
chybět	chybět	k5eAaImF	chybět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nohy	noha	k1gFnPc4	noha
červené	červený	k2eAgFnPc4d1	červená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šat	šat	k1gInSc1	šat
prostý	prostý	k2eAgInSc1d1	prostý
–	–	k?	–
jako	jako	k9	jako
svatební	svatební	k2eAgNnSc4d1	svatební
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bílé	bílý	k2eAgNnSc1d1	bílé
čelo	čelo	k1gNnSc1	čelo
<g/>
,	,	kIx,	,
na	na	k7c6	na
zbytku	zbytek	k1gInSc6	zbytek
čepičky	čepička	k1gFnSc2	čepička
někdy	někdy	k6eAd1	někdy
bílé	bílý	k2eAgFnSc2d1	bílá
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Tmavší	tmavý	k2eAgFnPc1d2	tmavší
přední	přední	k2eAgFnPc1d1	přední
křídelní	křídelní	k2eAgFnPc1d1	křídelní
krovky	krovka	k1gFnPc1	krovka
tvoří	tvořit	k5eAaImIp3nP	tvořit
nevýraznou	výrazný	k2eNgFnSc4d1	nevýrazná
pásku	páska	k1gFnSc4	páska
<g/>
,	,	kIx,	,
svrchní	svrchní	k2eAgFnPc4d1	svrchní
křídelní	křídelní	k2eAgFnPc4d1	křídelní
krovky	krovka	k1gFnPc4	krovka
a	a	k8xC	a
ocas	ocas	k1gInSc4	ocas
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
do	do	k7c2	do
šeda	šedo	k1gNnSc2	šedo
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc1	zobák
černý	černý	k2eAgInSc1d1	černý
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
ploškou	ploška	k1gFnSc7	ploška
pod	pod	k7c7	pod
nozdrami	nozdra	k1gFnPc7	nozdra
a	a	k8xC	a
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
celý	celý	k2eAgInSc1d1	celý
černý	černý	k2eAgInSc1d1	černý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nohy	noha	k1gFnPc1	noha
tmavěji	tmavě	k6eAd2	tmavě
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šat	šata	k1gFnPc2	šata
mláďat	mládě	k1gNnPc2	mládě
–	–	k?	–
jako	jako	k8xS	jako
šat	šat	k1gInSc1	šat
prostý	prostý	k2eAgInSc1d1	prostý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čelo	čelo	k1gNnSc1	čelo
s	s	k7c7	s
hnědým	hnědý	k2eAgNnSc7d1	hnědé
skvrněním	skvrnění	k1gNnSc7	skvrnění
<g/>
,	,	kIx,	,
i	i	k8xC	i
zbytek	zbytek	k1gInSc1	zbytek
čepičky	čepička	k1gFnSc2	čepička
promísen	promísit	k5eAaPmNgMnS	promísit
s	s	k7c7	s
hnědou	hnědý	k2eAgFnSc7d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Pera	pero	k1gNnPc1	pero
hřbetu	hřbet	k1gInSc2	hřbet
a	a	k8xC	a
krovky	krovka	k1gFnSc2	krovka
s	s	k7c7	s
hnědo-rezavými	hnědoezavý	k2eAgFnPc7d1	hnědo-rezavý
koncovými	koncový	k2eAgFnPc7d1	koncová
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
přední	přední	k2eAgFnPc1d1	přední
krovky	krovka	k1gFnPc1	krovka
téměř	téměř	k6eAd1	téměř
černé	černý	k2eAgFnSc2d1	černá
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nP	tvořit
výraznou	výrazný	k2eAgFnSc4d1	výrazná
pásku	páska	k1gFnSc4	páska
na	na	k7c6	na
předním	přední	k2eAgInSc6d1	přední
okraji	okraj	k1gInSc6	okraj
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Letky	letek	k1gInPc1	letek
tmavošedé	tmavošedý	k2eAgInPc1d1	tmavošedý
<g/>
,	,	kIx,	,
loketní	loketní	k2eAgInPc1d1	loketní
se	s	k7c7	s
světlejšími	světlý	k2eAgFnPc7d2	světlejší
špičkami	špička	k1gFnPc7	špička
<g/>
,	,	kIx,	,
kostřec	kostřec	k1gInSc4	kostřec
a	a	k8xC	a
ocas	ocas	k1gInSc4	ocas
šedavé	šedavý	k2eAgFnPc1d1	šedavá
<g/>
,	,	kIx,	,
rýdovací	rýdovací	k2eAgNnPc4d1	rýdovací
pera	pero	k1gNnPc4	pero
mají	mít	k5eAaImIp3nP	mít
tmavší	tmavý	k2eAgInPc1d2	tmavší
okraje	okraj	k1gInPc1	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc4	zobák
masový	masový	k2eAgInSc4d1	masový
nebo	nebo	k8xC	nebo
žlutooranžový	žlutooranžový	k2eAgInSc4d1	žlutooranžový
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
špičkou	špička	k1gFnSc7	špička
(	(	kIx(	(
<g/>
postupně	postupně	k6eAd1	postupně
tmavne	tmavnout	k5eAaImIp3nS	tmavnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nohy	noha	k1gFnPc4	noha
růžové	růžový	k2eAgFnPc4d1	růžová
až	až	k6eAd1	až
žlutooranžové	žlutooranžový	k2eAgFnPc1d1	žlutooranžová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šat	šat	k1gInSc1	šat
1	[number]	k4	1
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
–	–	k?	–
jako	jako	k8xC	jako
šat	šat	k1gInSc1	šat
prostý	prostý	k2eAgInSc1d1	prostý
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
těla	tělo	k1gNnSc2	tělo
ještě	ještě	k9	ještě
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
některá	některý	k3yIgNnPc1	některý
pera	pero	k1gNnPc1	pero
z	z	k7c2	z
šatu	šat	k1gInSc2	šat
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
pásmu	pásmo	k1gNnSc6	pásmo
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
mimo	mimo	k7c4	mimo
západní	západní	k2eAgFnSc4d1	západní
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
;	;	kIx,	;
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
také	také	k9	také
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
táhne	táhnout	k5eAaImIp3nS	táhnout
většina	většina	k1gFnSc1	většina
ptáků	pták	k1gMnPc2	pták
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
na	na	k7c4	na
jih	jih	k1gInSc4	jih
po	po	k7c4	po
Argentinu	Argentina	k1gFnSc4	Argentina
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc4d1	jižní
Afriku	Afrika	k1gFnSc4	Afrika
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc4	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
především	především	k9	především
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
na	na	k7c6	na
ostrůvcích	ostrůvek	k1gInPc6	ostrůvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
(	(	kIx(	(
<g/>
samostatných	samostatný	k2eAgFnPc2d1	samostatná
i	i	k9	i
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
druhy	druh	k1gInPc7	druh
rybáků	rybák	k1gMnPc2	rybák
nebo	nebo	k8xC	nebo
racků	racek	k1gMnPc2	racek
<g/>
)	)	kIx)	)
na	na	k7c6	na
mořských	mořský	k2eAgInPc6d1	mořský
pobřežích	pobřeží	k1gNnPc6	pobřeží
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
u	u	k7c2	u
velkých	velký	k2eAgFnPc2d1	velká
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
i	i	k8xC	i
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
přilétá	přilétat	k5eAaImIp3nS	přilétat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
každoročně	každoročně	k6eAd1	každoročně
hnízdní	hnízdní	k2eAgFnSc1d1	hnízdní
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
hnízdění	hnízdění	k1gNnSc3	hnízdění
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
štěrkové	štěrkový	k2eAgInPc4d1	štěrkový
ostrovy	ostrov	k1gInPc4	ostrov
či	či	k8xC	či
lavice	lavice	k1gFnPc4	lavice
v	v	k7c6	v
korytech	koryto	k1gNnPc6	koryto
větších	veliký	k2eAgFnPc2d2	veliký
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
jiné	jiný	k2eAgFnPc1d1	jiná
holé	holý	k2eAgFnPc1d1	holá
plochy	plocha	k1gFnPc1	plocha
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
velmi	velmi	k6eAd1	velmi
společenským	společenský	k2eAgInSc7d1	společenský
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
shání	shánět	k5eAaImIp3nS	shánět
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
hejnech	hejno	k1gNnPc6	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Hnízda	hnízdo	k1gNnPc1	hnízdo
staví	stavit	k5eAaPmIp3nP	stavit
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
málo	málo	k6eAd1	málo
zarostlých	zarostlý	k2eAgNnPc6d1	zarostlé
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jen	jen	k9	jen
na	na	k7c6	na
naplaveném	naplavený	k2eAgInSc6d1	naplavený
štěrku	štěrk	k1gInSc6	štěrk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
snáší	snášet	k5eAaImIp3nS	snášet
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
vejce	vejce	k1gNnPc4	vejce
<g/>
,	,	kIx,	,
sedí	sedit	k5eAaImIp3nP	sedit
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
ze	z	k7c2	z
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
času	čas	k1gInSc2	čas
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
při	při	k7c6	při
častém	častý	k2eAgNnSc6d1	časté
rušení	rušení	k1gNnSc6	rušení
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
sezení	sezení	k1gNnSc1	sezení
protáhnout	protáhnout	k5eAaPmF	protáhnout
až	až	k9	až
na	na	k7c4	na
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
dny	den	k1gInPc7	den
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
týden	týden	k1gInSc4	týden
jsou	být	k5eAaImIp3nP	být
zahřívána	zahříván	k2eAgFnSc1d1	zahřívána
samicí	samice	k1gFnSc7	samice
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
rodič	rodič	k1gMnSc1	rodič
přináší	přinášet	k5eAaImIp3nS	přinášet
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
krmí	krmit	k5eAaImIp3nP	krmit
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Vzletná	vzletný	k2eAgFnSc1d1	vzletná
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
22	[number]	k4	22
<g/>
–	–	k?	–
<g/>
28	[number]	k4	28
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
další	další	k2eAgInSc1d1	další
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
měsíce	měsíc	k1gInSc2	měsíc
jsou	být	k5eAaImIp3nP	být
krmena	krmen	k2eAgNnPc1d1	krmeno
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdiště	hnízdiště	k1gNnPc1	hnízdiště
opouštějí	opouštět	k5eAaImIp3nP	opouštět
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnPc1d1	poslední
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc4	my
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
většina	většina	k1gFnSc1	většina
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
věk	věk	k1gInSc1	věk
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nP	tvořit
především	především	k9	především
malé	malý	k2eAgFnPc4d1	malá
rybky	rybka	k1gFnPc4	rybka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
loví	lovit	k5eAaImIp3nP	lovit
při	při	k7c6	při
třepotavém	třepotavý	k2eAgInSc6d1	třepotavý
letu	let	k1gInSc6	let
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
střemhlav	střemhlav	k6eAd1	střemhlav
vrhá	vrhat	k5eAaImIp3nS	vrhat
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
potravy	potrava	k1gFnSc2	potrava
tvoří	tvořit	k5eAaImIp3nS	tvořit
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
korýši	korýš	k1gMnPc1	korýš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Common	Common	k1gMnSc1	Common
tern	tern	k1gMnSc1	tern
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SVENSSON	SVENSSON	kA	SVENSSON
<g/>
,	,	kIx,	,
Lars	Larsa	k1gFnPc2	Larsa
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Ševčík	Ševčík	k1gMnSc1	Ševčík
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7291	[number]	k4	7291
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
201	[number]	k4	201
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rybák	rybák	k1gMnSc1	rybák
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Sterna	sternum	k1gNnSc2	sternum
hirundo	hirundo	k6eAd1	hirundo
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
