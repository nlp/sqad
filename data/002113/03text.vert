<s>
Abidžan	Abidžan	k1gInSc1	Abidžan
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Abidjan	Abidjan	k1gInSc1	Abidjan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
(	(	kIx(	(
<g/>
Côte	Côte	k1gInSc1	Côte
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ivoire	Ivoir	k1gMnSc5	Ivoir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
"	"	kIx"	"
<g/>
Perla	perla	k1gFnSc1	perla
laguny	laguna	k1gFnSc2	laguna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
politickým	politický	k2eAgInSc7d1	politický
<g/>
,	,	kIx,	,
správním	správní	k2eAgInSc7d1	správní
<g/>
,	,	kIx,	,
obchodním	obchodní	k2eAgInSc7d1	obchodní
a	a	k8xC	a
finančním	finanční	k2eAgInSc7d1	finanční
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
frankofonní	frankofonní	k2eAgFnSc7d1	frankofonní
městskou	městský	k2eAgFnSc7d1	městská
aglomerací	aglomerace	k1gFnSc7	aglomerace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
po	po	k7c6	po
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
Kinshase	Kinshasa	k1gFnSc6	Kinshasa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
Montreal	Montreal	k1gInSc1	Montreal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
mělo	mít	k5eAaImAgNnS	mít
samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
3	[number]	k4	3
796	[number]	k4	796
677	[number]	k4	677
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
frankofonní	frankofonní	k2eAgNnSc4d1	frankofonní
město	město	k1gNnSc4	město
<g/>
)	)	kIx)	)
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
aglomerace	aglomerace	k1gFnSc1	aglomerace
5	[number]	k4	5
068	[number]	k4	068
858	[number]	k4	858
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
pětinu	pětina	k1gFnSc4	pětina
obyvatel	obyvatel	k1gMnSc1	obyvatel
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgNnSc1d1	kulturní
centrum	centrum	k1gNnSc1	centrum
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
úrovní	úroveň	k1gFnSc7	úroveň
industrializace	industrializace	k1gFnSc2	industrializace
a	a	k8xC	a
urbanizace	urbanizace	k1gFnSc2	urbanizace
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
laguně	laguna	k1gFnSc6	laguna
Ébrié	Ébriá	k1gFnSc2	Ébriá
na	na	k7c6	na
poloostrovech	poloostrov	k1gInPc6	poloostrov
a	a	k8xC	a
ostrovech	ostrov	k1gInPc6	ostrov
spojených	spojený	k2eAgFnPc2d1	spojená
mosty	most	k1gInPc1	most
a	a	k8xC	a
lodní	lodní	k2eAgFnSc7d1	lodní
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Laguna	laguna	k1gFnSc1	laguna
Ébrié	Ébriá	k1gFnSc2	Ébriá
je	být	k5eAaImIp3nS	být
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
Guinejského	guinejský	k2eAgInSc2d1	guinejský
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
písečnou	písečný	k2eAgFnSc7d1	písečná
kosou	kosa	k1gFnSc7	kosa
pláže	pláž	k1gFnSc2	pláž
Vridi	Vrid	k1gMnPc1	Vrid
a	a	k8xC	a
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
stejnojmenným	stejnojmenný	k2eAgInSc7d1	stejnojmenný
kanálem	kanál	k1gInSc7	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
abidžanský	abidžanský	k2eAgInSc1d1	abidžanský
námořní	námořní	k2eAgInSc1d1	námořní
přístav	přístav	k1gInSc1	přístav
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
vývozem	vývoz	k1gInSc7	vývoz
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc1	kakao
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
ananasy	ananas	k1gInPc1	ananas
a	a	k8xC	a
manga	mango	k1gNnPc1	mango
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Grand-Bassamu	Grand-Bassam	k1gInSc6	Grand-Bassam
a	a	k8xC	a
Bingervillu	Bingervill	k1gInSc6	Bingervill
byl	být	k5eAaImAgInS	být
Abidžan	Abidžan	k1gInSc1	Abidžan
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetí	třetí	k4xOgNnSc4	třetí
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Yamoussoukro	Yamoussoukro	k1gNnSc1	Yamoussoukro
a	a	k8xC	a
Bingerville	Bingerville	k1gNnSc1	Bingerville
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc4	část
předměstí	předměstí	k1gNnSc2	předměstí
Abidžanu	Abidžan	k1gInSc2	Abidžan
<g/>
.	.	kIx.	.
</s>
<s>
Bingerville	Bingerville	k1gNnSc1	Bingerville
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
epidemii	epidemie	k1gFnSc3	epidemie
žluté	žlutý	k2eAgFnSc2d1	žlutá
zimnice	zimnice	k1gFnSc2	zimnice
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
usídlení	usídlený	k2eAgMnPc1d1	usídlený
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Grand	grand	k1gMnSc1	grand
Bassamu	Bassam	k1gInSc3	Bassam
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
do	do	k7c2	do
Bingerville	Bingerville	k1gFnSc2	Bingerville
<g/>
.	.	kIx.	.
</s>
<s>
Doufali	doufat	k5eAaImAgMnP	doufat
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
příznivější	příznivý	k2eAgNnSc4d2	příznivější
ovzduší	ovzduší	k1gNnSc4	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
Abidžan	Abidžan	k1gInSc1	Abidžan
ležel	ležet	k5eAaImAgInS	ležet
blízko	blízko	k6eAd1	blízko
a	a	k8xC	a
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
obchodu	obchod	k1gInSc2	obchod
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc2	jeho
umístění	umístění	k1gNnSc2	umístění
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
upřednostňován	upřednostňovat	k5eAaImNgInS	upřednostňovat
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
přístav	přístav	k1gInSc1	přístav
Petit-Bassam	Petit-Bassam	k1gInSc1	Petit-Bassam
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Port	port	k1gInSc4	port
Bouët	Bouëta	k1gFnPc2	Bouëta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
a	a	k8xC	a
překonal	překonat	k5eAaPmAgInS	překonat
přístav	přístav	k1gInSc1	přístav
Grand	grand	k1gMnSc1	grand
Bassam	Bassam	k1gInSc1	Bassam
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
měl	mít	k5eAaImAgInS	mít
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vedoucí	vedoucí	k2eAgNnPc4d1	vedoucí
postavení	postavení	k1gNnPc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
dokončený	dokončený	k2eAgInSc1d1	dokončený
rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
plán	plán	k1gInSc1	plán
Bingerville	Bingerville	k1gFnSc1	Bingerville
<g/>
,	,	kIx,	,
Abidžan	Abidžan	k1gInSc1	Abidžan
převzal	převzít	k5eAaPmAgInS	převzít
roli	role	k1gFnSc4	role
hlavního	hlavní	k2eAgInSc2d1	hlavní
přístavu	přístav	k1gInSc2	přístav
celé	celý	k2eAgFnSc2d1	celá
kolonie	kolonie	k1gFnSc2	kolonie
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
evropský	evropský	k2eAgInSc4d1	evropský
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dopravu	doprava	k1gFnSc4	doprava
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
velmi	velmi	k6eAd1	velmi
zjednodušilo	zjednodušit	k5eAaPmAgNnS	zjednodušit
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1933	[number]	k4	1933
bylo	být	k5eAaImAgNnS	být
dekretem	dekret	k1gInSc7	dekret
přemístěno	přemístěn	k2eAgNnSc1d1	přemístěno
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
z	z	k7c2	z
Bingervillu	Bingervill	k1gInSc2	Bingervill
do	do	k7c2	do
Abidžanu	Abidžan	k1gInSc2	Abidžan
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
také	také	k9	také
přesun	přesun	k1gInSc1	přesun
mnoha	mnoho	k4c2	mnoho
vesnic	vesnice	k1gFnPc2	vesnice
mluvících	mluvící	k2eAgFnPc2d1	mluvící
jazykem	jazyk	k1gInSc7	jazyk
tchaman	tchaman	k1gMnSc1	tchaman
(	(	kIx(	(
<g/>
fr	fr	k0	fr
<g/>
:	:	kIx,	:
<g/>
Tchaman	Tchaman	k1gMnSc1	Tchaman
<g/>
)	)	kIx)	)
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
komuny	komuna	k1gFnSc2	komuna
Adjame	Adjam	k1gInSc5	Adjam
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Plateau	Plateaus	k1gInSc2	Plateaus
-	-	kIx~	-
středu	středa	k1gFnSc4	středa
Abidžanu	Abidžan	k1gInSc2	Abidžan
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
prostorem	prostor	k1gInSc7	prostor
tchamanské	tchamanský	k2eAgFnSc2d1	tchamanský
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
posvátného	posvátný	k2eAgNnSc2d1	posvátné
místa	místo	k1gNnSc2	místo
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základním	základní	k2eAgInSc7d1	základní
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
vydírání	vydírání	k1gNnSc4	vydírání
tchamanské	tchamanský	k2eAgFnSc2d1	tchamanský
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
nucené	nucený	k2eAgFnSc2d1	nucená
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
železnice	železnice	k1gFnSc2	železnice
Abidžan	Abidžan	k1gInSc1	Abidžan
-	-	kIx~	-
Niger	Niger	k1gInSc1	Niger
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
centrum	centrum	k1gNnSc1	centrum
Abidžanu	Abidžan	k1gInSc2	Abidžan
-	-	kIx~	-
jih	jih	k1gInSc1	jih
dnešního	dnešní	k2eAgInSc2d1	dnešní
Plateau	Plateaus	k1gInSc2	Plateaus
-	-	kIx~	-
základnou	základna	k1gFnSc7	základna
vesnice	vesnice	k1gFnSc2	vesnice
Dugbeyo	Dugbeyo	k1gNnSc4	Dugbeyo
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
však	však	k9	však
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
do	do	k7c2	do
čtvrti	čtvrt	k1gFnSc2	čtvrt
Anoumabo	Anoumaba	k1gFnSc5	Anoumaba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
laguny	laguna	k1gFnSc2	laguna
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
jež	jenž	k3xRgNnSc4	jenž
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
přejmenován	přejmenovat	k5eAaPmNgMnS	přejmenovat
na	na	k7c6	na
Treichville	Treichville	k1gNnPc6	Treichville
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
Marcela	Marcel	k1gMnSc2	Marcel
Treich-Laplè	Treich-Laplè	k1gMnSc2	Treich-Laplè
<g/>
,	,	kIx,	,
považovaného	považovaný	k2eAgMnSc2d1	považovaný
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
někdejší	někdejší	k2eAgFnSc2d1	někdejší
vesnice	vesnice	k1gFnSc2	vesnice
Dugbeyo	Dugbeyo	k6eAd1	Dugbeyo
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
"	"	kIx"	"
<g/>
Gare	Gare	k1gInSc1	Gare
Sud	sud	k1gInSc1	sud
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
trajektový	trajektový	k2eAgInSc1d1	trajektový
terminál	terminál	k1gInSc1	terminál
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
také	také	k9	také
část	část	k1gFnSc4	část
obchodní	obchodní	k2eAgFnSc2d1	obchodní
třídy	třída	k1gFnSc2	třída
"	"	kIx"	"
<g/>
Boulevard	Boulevard	k1gInSc1	Boulevard
de	de	k?	de
Gaulle	Gaulle	k1gInSc1	Gaulle
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
kolonisté	kolonista	k1gMnPc1	kolonista
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
Plateau	Plateaum	k1gNnSc6	Plateaum
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
populace	populace	k1gFnSc1	populace
a	a	k8xC	a
kolonizované	kolonizovaný	k2eAgNnSc1d1	kolonizované
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
se	se	k3xPyFc4	se
usazovali	usazovat	k5eAaImAgMnP	usazovat
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
oblasti	oblast	k1gFnPc1	oblast
byly	být	k5eAaImAgFnP	být
odděleny	oddělit	k5eAaPmNgFnP	oddělit
Gallieniho	Gallieniha	k1gFnSc5	Gallieniha
kasárnami	kasárny	k1gFnPc7	kasárny
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
budovy	budova	k1gFnSc2	budova
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Plateau	Plateau	k6eAd1	Plateau
a	a	k8xC	a
Treichville	Treichvillo	k1gNnSc6	Treichvillo
byly	být	k5eAaImAgFnP	být
spojeny	spojen	k2eAgMnPc4d1	spojen
pontonovým	pontonový	k2eAgInSc7d1	pontonový
mostem	most	k1gInSc7	most
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
most	most	k1gInSc1	most
Houphouët	Houphouëta	k1gFnPc2	Houphouëta
Boigny	Boigna	k1gFnSc2	Boigna
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
dostal	dostat	k5eAaPmAgMnS	dostat
Abidžan	Abidžan	k1gMnSc1	Abidžan
první	první	k4xOgFnSc2	první
uliční	uliční	k2eAgFnSc2d1	uliční
adresy	adresa	k1gFnSc2	adresa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
starosty	starosta	k1gMnSc2	starosta
Conana	Conan	k1gMnSc2	Conan
Kanga	Kang	k1gMnSc2	Kang
změněny	změněn	k2eAgInPc1d1	změněn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
kanál	kanál	k1gInSc1	kanál
Vridi	Vrid	k1gMnPc1	Vrid
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
laguny	laguna	k1gFnSc2	laguna
Ébrié	Ébriá	k1gFnSc2	Ébriá
<g/>
.	.	kIx.	.
</s>
<s>
Abidžanský	Abidžanský	k2eAgInSc1d1	Abidžanský
přístav	přístav	k1gInSc1	přístav
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
snadno	snadno	k6eAd1	snadno
přístupný	přístupný	k2eAgInSc1d1	přístupný
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
to	ten	k3xDgNnSc1	ten
však	však	k9	však
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
teploty	teplota	k1gFnSc2	teplota
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
laguně	laguna	k1gFnSc6	laguna
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
země	země	k1gFnSc1	země
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
Abidžan	Abidžan	k1gInSc1	Abidžan
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
rychle	rychle	k6eAd1	rychle
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Yamoussoukro	Yamoussoukro	k1gNnSc1	Yamoussoukro
ve	v	k7c4	v
středu	středa	k1gFnSc4	středa
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vrcholné	vrcholný	k2eAgInPc1d1	vrcholný
úřady	úřad	k1gInPc1	úřad
zůstaly	zůstat	k5eAaPmAgInP	zůstat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
Abidžanu	Abidžan	k1gInSc6	Abidžan
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatel	obyvatel	k1gMnSc1	obyvatel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
přibývalo	přibývat	k5eAaImAgNnS	přibývat
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
budovat	budovat	k5eAaImF	budovat
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
a	a	k8xC	a
obytné	obytný	k2eAgFnPc4d1	obytná
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozrůstaly	rozrůstat	k5eAaImAgFnP	rozrůstat
chudinské	chudinský	k2eAgFnPc1d1	chudinská
čtvrti	čtvrt	k1gFnPc1	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
Plateau	Plateaus	k1gInSc6	Plateaus
vyrostly	vyrůst	k5eAaPmAgInP	vyrůst
mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
a	a	k8xC	a
posílilo	posílit	k5eAaPmAgNnS	posílit
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
centra	centrum	k1gNnSc2	centrum
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
státní	státní	k2eAgInSc1d1	státní
převrat	převrat	k1gInSc1	převrat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
začala	začít	k5eAaPmAgFnS	začít
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
města	město	k1gNnSc2	město
citelně	citelně	k6eAd1	citelně
dotkla	dotknout	k5eAaPmAgFnS	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgInPc1d1	následovaný
dalšími	další	k2eAgInPc7d1	další
nepokoji	nepokoj	k1gInPc7	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Abidžanu	Abidžan	k1gInSc2	Abidžan
převzal	převzít	k5eAaPmAgMnS	převzít
moc	moc	k6eAd1	moc
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
Alassane	Alassan	k1gMnSc5	Alassan
Ouattara	Ouattara	k1gFnSc1	Ouattara
a	a	k8xC	a
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
zklidňovat	zklidňovat	k5eAaImF	zklidňovat
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
Abidžan	Abidžana	k1gFnPc2	Abidžana
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
na	na	k7c6	na
laguně	laguna	k1gFnSc6	laguna
Ébrié	Ébriá	k1gFnSc2	Ébriá
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
severní	severní	k2eAgFnPc1d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgInSc1d1	jižní
Abidžan	Abidžan	k1gInSc1	Abidžan
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgNnSc1d1	oddělené
vodní	vodní	k2eAgFnSc7d1	vodní
plochou	plocha	k1gFnSc7	plocha
laguny	laguna	k1gFnSc2	laguna
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Abidžan	Abidžany	k1gInPc2	Abidžany
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
čtvrtí	čtvrt	k1gFnPc2	čtvrt
-	-	kIx~	-
komun	komuna	k1gFnPc2	komuna
(	(	kIx(	(
<g/>
commune	commun	k1gMnSc5	commun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
mají	mít	k5eAaImIp3nP	mít
každá	každý	k3xTgFnSc1	každý
vlastního	vlastní	k2eAgMnSc4d1	vlastní
starostu	starosta	k1gMnSc4	starosta
a	a	k8xC	a
zastupitelstvo	zastupitelstvo	k1gNnSc4	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Plateau	Plateau	k5eAaPmIp1nS	Plateau
-	-	kIx~	-
hlavní	hlavní	k2eAgMnSc1d1	hlavní
administrativní	administrativní	k2eAgNnSc4d1	administrativní
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgNnSc4d1	obchodní
a	a	k8xC	a
finanční	finanční	k2eAgNnSc4d1	finanční
centrum	centrum	k1gNnSc4	centrum
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
a	a	k8xC	a
Abidžanu	Abidžan	k1gInSc2	Abidžan
s	s	k7c7	s
výškovými	výškový	k2eAgFnPc7d1	výšková
budovami	budova	k1gFnPc7	budova
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
do	do	k7c2	do
Yamoussoukra	Yamoussoukr	k1gInSc2	Yamoussoukr
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
státní	státní	k2eAgInPc1d1	státní
úřady	úřad	k1gInPc1	úřad
jako	jako	k8xS	jako
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
úřad	úřad	k1gInSc1	úřad
a	a	k8xC	a
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
Plateau	Plateaus	k1gInSc6	Plateaus
<g/>
.	.	kIx.	.
</s>
<s>
Adjamé	Adjamý	k2eAgNnSc1d1	Adjamý
-	-	kIx~	-
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
leží	ležet	k5eAaImIp3nS	ležet
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
míří	mířit	k5eAaImIp3nS	mířit
linky	linka	k1gFnPc4	linka
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
i	i	k9	i
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Cocody	Cocoda	k1gFnPc1	Cocoda
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
lycea	lyceum	k1gNnSc2	lyceum
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
ale	ale	k8xC	ale
tvoří	tvořit	k5eAaImIp3nP	tvořit
obytné	obytný	k2eAgFnPc4d1	obytná
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Abobo	Aboba	k1gMnSc5	Aboba
Attécoubé	Attécoubý	k2eAgFnSc2d1	Attécoubý
-	-	kIx~	-
součástí	součást	k1gFnPc2	součást
čtvrti	čtvrt	k1gFnSc2	čtvrt
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Banco	Banco	k6eAd1	Banco
<g/>
.	.	kIx.	.
</s>
<s>
Yopougon	Yopougon	k1gNnSc1	Yopougon
-	-	kIx~	-
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
komuna	komuna	k1gFnSc1	komuna
Abidžanu	Abidžan	k1gInSc2	Abidžan
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
částečně	částečně	k6eAd1	částečně
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Abidžanu	Abidžan	k1gInSc6	Abidžan
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
laguny	laguna	k1gFnSc2	laguna
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Abidžanu	Abidžan	k1gInSc6	Abidžan
<g/>
.	.	kIx.	.
</s>
<s>
Treichville	Treichville	k1gNnSc1	Treichville
-	-	kIx~	-
autonomní	autonomní	k2eAgInSc1d1	autonomní
abidžanský	abidžanský	k2eAgInSc1d1	abidžanský
přístav	přístav	k1gInSc1	přístav
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Marcory	Marcora	k1gFnPc4	Marcora
Koumassi	Koumasse	k1gFnSc4	Koumasse
-	-	kIx~	-
komuna	komuna	k1gFnSc1	komuna
s	s	k7c7	s
důležitými	důležitý	k2eAgInPc7d1	důležitý
průmyslovými	průmyslový	k2eAgInPc7d1	průmyslový
areály	areál	k1gInPc7	areál
<g/>
.	.	kIx.	.
</s>
<s>
Port-Bouët	Port-Bouët	k1gInSc1	Port-Bouët
-	-	kIx~	-
rafinerie	rafinerie	k1gFnPc1	rafinerie
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Félixe	Félixe	k1gFnSc2	Félixe
Houphouët-Boigny	Houphouët-Boigna	k1gFnSc2	Houphouët-Boigna
<g/>
.	.	kIx.	.
</s>
<s>
Starostou	Starosta	k1gMnSc7	Starosta
Abidžanu	Abidžan	k1gInSc2	Abidžan
je	být	k5eAaImIp3nS	být
guvernér	guvernér	k1gMnSc1	guvernér
distriktu	distrikt	k1gInSc2	distrikt
<g/>
,	,	kIx,	,
jmenovaný	jmenovaný	k2eAgInSc4d1	jmenovaný
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
monzunů	monzun	k1gInPc2	monzun
Köppenovy	Köppenův	k2eAgFnSc2d1	Köppenova
klasifikace	klasifikace	k1gFnSc2	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
období	období	k1gNnSc1	období
dešťů	dešť	k1gInPc2	dešť
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
krátké	krátká	k1gFnPc1	krátká
od	od	k7c2	od
září	září	k1gNnSc2	září
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
se	se	k3xPyFc4	se
však	však	k9	však
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
v	v	k7c6	v
období	období	k1gNnSc6	období
sucha	sucho	k1gNnSc2	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
bývá	bývat	k5eAaImIp3nS	bývat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
80	[number]	k4	80
i	i	k8xC	i
více	hodně	k6eAd2	hodně
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
000	[number]	k4	000
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
celoročně	celoročně	k6eAd1	celoročně
vyrovnaná	vyrovnaný	k2eAgFnSc1d1	vyrovnaná
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
výkyvů	výkyv	k1gInPc2	výkyv
kolem	kolem	k7c2	kolem
27	[number]	k4	27
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
Abidžanu	Abidžan	k1gInSc6	Abidžan
žije	žít	k5eAaImIp3nS	žít
okolo	okolo	k7c2	okolo
jedné	jeden	k4xCgFnSc2	jeden
pětiny	pětina	k1gFnSc2	pětina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činí	činit	k5eAaImIp3nS	činit
3	[number]	k4	3
796	[number]	k4	796
677	[number]	k4	677
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
aglomerace	aglomerace	k1gFnSc1	aglomerace
pak	pak	k6eAd1	pak
dokonce	dokonce	k9	dokonce
5	[number]	k4	5
068	[number]	k4	068
858	[number]	k4	858
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nepravidelné	pravidelný	k2eNgNnSc1d1	nepravidelné
rozložení	rozložení	k1gNnSc1	rozložení
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
přistěhovalectvím	přistěhovalectví	k1gNnSc7	přistěhovalectví
venkovského	venkovský	k2eAgNnSc2d1	venkovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zde	zde	k6eAd1	zde
hledá	hledat	k5eAaImIp3nS	hledat
lepší	dobrý	k2eAgFnPc4d2	lepší
životní	životní	k2eAgFnPc4d1	životní
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
každých	každý	k3xTgNnPc2	každý
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
zdvojnásobuje	zdvojnásobovat	k5eAaImIp3nS	zdvojnásobovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
růst	růst	k1gInSc4	růst
obyvatel	obyvatel	k1gMnPc2	obyvatel
zmírnil	zmírnit	k5eAaPmAgMnS	zmírnit
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
se	se	k3xPyFc4	se
srovnávat	srovnávat	k5eAaImF	srovnávat
s	s	k7c7	s
lety	let	k1gInPc7	let
1960	[number]	k4	1960
-	-	kIx~	-
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ročně	ročně	k6eAd1	ročně
činil	činit	k5eAaImAgInS	činit
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
činí	činit	k5eAaImIp3nS	činit
4,5	[number]	k4	4,5
%	%	kIx~	%
a	a	k8xC	a
i	i	k9	i
podíl	podíl	k1gInSc1	podíl
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgMnS	snížit
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1960	[number]	k4	1960
-	-	kIx~	-
1990	[number]	k4	1990
na	na	k7c4	na
současnou	současný	k2eAgFnSc4d1	současná
třetinu	třetina	k1gFnSc4	třetina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
však	však	k9	však
populace	populace	k1gFnSc1	populace
Abidžanu	Abidžan	k1gInSc2	Abidžan
příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
porovnáme	porovnat	k5eAaPmIp1nP	porovnat
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
městy	město	k1gNnPc7	město
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
jako	jako	k8xS	jako
Yamoussoukro	Yamoussoukro	k1gNnSc4	Yamoussoukro
(	(	kIx(	(
<g/>
250	[number]	k4	250
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Bouaké	Bouaký	k2eAgNnSc1d1	Bouaké
(	(	kIx(	(
<g/>
660	[number]	k4	660
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Abidžan	Abidžan	k1gInSc1	Abidžan
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
původně	původně	k6eAd1	původně
z	z	k7c2	z
rybářské	rybářský	k2eAgFnSc2d1	rybářská
vesnice	vesnice	k1gFnSc2	vesnice
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
rozvoj	rozvoj	k1gInSc4	rozvoj
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
vybudování	vybudování	k1gNnSc1	vybudování
železničního	železniční	k2eAgNnSc2d1	železniční
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
a	a	k8xC	a
zejména	zejména	k9	zejména
vybudování	vybudování	k1gNnSc3	vybudování
kanálu	kanál	k1gInSc2	kanál
Vridi	Vrid	k1gMnPc1	Vrid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spojil	spojit	k5eAaPmAgInS	spojit
přístav	přístav	k1gInSc4	přístav
a	a	k8xC	a
lagunu	laguna	k1gFnSc4	laguna
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
umožnil	umožnit	k5eAaPmAgInS	umožnit
rozvoj	rozvoj	k1gInSc1	rozvoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
z	z	k7c2	z
Abidžanu	Abidžan	k1gInSc2	Abidžan
udělal	udělat	k5eAaPmAgInS	udělat
veliký	veliký	k2eAgInSc1d1	veliký
námořní	námořní	k2eAgInSc1d1	námořní
přístav	přístav	k1gInSc1	přístav
pro	pro	k7c4	pro
zaoceánský	zaoceánský	k2eAgInSc4d1	zaoceánský
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
zbožím	zboží	k1gNnSc7	zboží
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
zdroje	zdroj	k1gInPc1	zdroj
příjmů	příjem	k1gInPc2	příjem
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
plynou	plynout	k5eAaImIp3nP	plynout
z	z	k7c2	z
vývozu	vývoz	k1gInSc2	vývoz
produktů	produkt	k1gInPc2	produkt
zejména	zejména	k9	zejména
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
kakaa	kakao	k1gNnSc2	kakao
<g/>
,	,	kIx,	,
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
sucho	sucho	k1gNnSc1	sucho
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1983	[number]	k4	1983
a	a	k8xC	a
1984	[number]	k4	1984
a	a	k8xC	a
pokles	pokles	k1gInSc1	pokles
cen	cena	k1gFnPc2	cena
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
produktů	produkt	k1gInPc2	produkt
vedl	vést	k5eAaImAgMnS	vést
zemi	zem	k1gFnSc4	zem
k	k	k7c3	k
ekonomickým	ekonomický	k2eAgInPc3d1	ekonomický
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
snahu	snaha	k1gFnSc4	snaha
diverzifikovat	diverzifikovat	k5eAaImF	diverzifikovat
zdroje	zdroj	k1gInPc4	zdroj
však	však	k9	však
je	být	k5eAaImIp3nS	být
země	země	k1gFnSc1	země
stále	stále	k6eAd1	stále
výrazně	výrazně	k6eAd1	výrazně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Neustávající	ustávající	k2eNgInSc1d1	neustávající
silný	silný	k2eAgInSc1d1	silný
proud	proud	k1gInSc1	proud
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
zejména	zejména	k9	zejména
ze	z	k7c2	z
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
chudých	chudý	k2eAgFnPc2d1	chudá
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
neumožní	umožnit	k5eNaPmIp3nS	umožnit
městu	město	k1gNnSc3	město
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
svoji	svůj	k3xOyFgFnSc4	svůj
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Abidžan	Abidžan	k1gInSc1	Abidžan
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
laguně	laguna	k1gFnSc6	laguna
Ébrié	Ébriá	k1gFnSc2	Ébriá
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemá	mít	k5eNaImIp3nS	mít
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
zásobováním	zásobování	k1gNnSc7	zásobování
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odpadní	odpadní	k2eAgFnSc1d1	odpadní
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
kanalizace	kanalizace	k1gFnSc1	kanalizace
lagunu	laguna	k1gFnSc4	laguna
znečišťuje	znečišťovat	k5eAaImIp3nS	znečišťovat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
kanalizační	kanalizační	k2eAgInSc1d1	kanalizační
systém	systém	k1gInSc1	systém
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgInSc4d1	dobrý
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
udržet	udržet	k5eAaPmF	udržet
krok	krok	k1gInSc4	krok
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
si	se	k3xPyFc3	se
však	však	k9	však
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
každoroční	každoroční	k2eAgInSc4d1	každoroční
přírůstek	přírůstek	k1gInSc4	přírůstek
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
střechu	střecha	k1gFnSc4	střecha
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
i	i	k9	i
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
omezení	omezení	k1gNnSc1	omezení
tlaku	tlak	k1gInSc2	tlak
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
program	program	k1gInSc1	program
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
decentralizaci	decentralizace	k1gFnSc4	decentralizace
země	zem	k1gFnSc2	zem
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
výstavbu	výstavba	k1gFnSc4	výstavba
měst	město	k1gNnPc2	město
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
s	s	k7c7	s
dobrým	dobrý	k2eAgNnSc7d1	dobré
vybavením	vybavení	k1gNnSc7	vybavení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přilákaly	přilákat	k5eAaPmAgFnP	přilákat
mladé	mladá	k1gFnPc1	mladá
z	z	k7c2	z
Abidžanu	Abidžan	k1gInSc2	Abidžan
<g/>
.	.	kIx.	.
</s>
<s>
Přemístění	přemístění	k1gNnSc1	přemístění
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
do	do	k7c2	do
Yamoussoukra	Yamoussoukr	k1gInSc2	Yamoussoukr
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgInSc2	tento
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
<g/>
,	,	kIx,	,
plán	plán	k1gInSc1	plán
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
plně	plně	k6eAd1	plně
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
a	a	k8xC	a
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
i	i	k8xC	i
obchodní	obchodní	k2eAgFnSc1d1	obchodní
důležitost	důležitost	k1gFnSc1	důležitost
Abidžanu	Abidžan	k1gInSc2	Abidžan
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
význam	význam	k1gInSc1	význam
Yamoussoukra	Yamoussoukr	k1gInSc2	Yamoussoukr
<g/>
.	.	kIx.	.
</s>
<s>
Silnou	silný	k2eAgFnSc7d1	silná
stránkou	stránka	k1gFnSc7	stránka
Abidžanu	Abidžan	k1gInSc2	Abidžan
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jeho	jeho	k3xOp3gInSc1	jeho
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
s	s	k7c7	s
kvalitní	kvalitní	k2eAgFnSc7d1	kvalitní
sítí	síť	k1gFnSc7	síť
silnic	silnice	k1gFnPc2	silnice
udržovaných	udržovaný	k2eAgFnPc2d1	udržovaná
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
komuny	komuna	k1gFnPc1	komuna
města	město	k1gNnSc2	město
spojují	spojovat	k5eAaImIp3nP	spojovat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
asfaltové	asfaltový	k2eAgInPc1d1	asfaltový
silnice	silnice	k1gFnPc4	silnice
a	a	k8xC	a
široké	široký	k2eAgFnPc1d1	široká
třídy	třída	k1gFnPc1	třída
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
síť	síť	k1gFnSc4	síť
obchvatů	obchvat	k1gInPc2	obchvat
pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
a	a	k8xC	a
urychlení	urychlení	k1gNnSc4	urychlení
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Abidžanu	Abidžan	k1gInSc2	Abidžan
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
dvěma	dva	k4xCgInPc7	dva
mosty	most	k1gInPc7	most
mezi	mezi	k7c7	mezi
komunami	komuna	k1gFnPc7	komuna
Plateau	Plateaus	k1gInSc2	Plateaus
a	a	k8xC	a
Treichville	Treichville	k1gNnSc2	Treichville
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
svojí	svůj	k3xOyFgFnSc7	svůj
kapacitou	kapacita	k1gFnSc7	kapacita
nestačí	stačit	k5eNaBmIp3nS	stačit
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
buduje	budovat	k5eAaImIp3nS	budovat
most	most	k1gInSc1	most
třetí	třetí	k4xOgFnSc1	třetí
mezi	mezi	k7c4	mezi
Cocody	Cocoda	k1gFnPc4	Cocoda
a	a	k8xC	a
Marcory	Marcora	k1gFnPc4	Marcora
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Individuální	individuální	k2eAgFnSc1d1	individuální
doprava	doprava	k1gFnSc1	doprava
po	po	k7c6	po
laguně	laguna	k1gFnSc6	laguna
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
zejména	zejména	k9	zejména
na	na	k7c6	na
malých	malý	k2eAgInPc6d1	malý
člunech	člun	k1gInPc6	člun
-	-	kIx~	-
pirogách	piroga	k1gFnPc6	piroga
<g/>
.	.	kIx.	.
</s>
<s>
Hromadnou	hromadný	k2eAgFnSc4d1	hromadná
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
komunami	komuna	k1gFnPc7	komuna
po	po	k7c6	po
vodě	voda	k1gFnSc6	voda
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
větší	veliký	k2eAgFnPc1d2	veliký
lodě	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
dle	dle	k7c2	dle
rozvojových	rozvojový	k2eAgInPc2d1	rozvojový
plánů	plán	k1gInPc2	plán
města	město	k1gNnSc2	město
nabývat	nabývat	k5eAaImF	nabývat
na	na	k7c6	na
důležitosti	důležitost	k1gFnSc6	důležitost
<g/>
.	.	kIx.	.
</s>
<s>
Abidžan	Abidžan	k1gInSc1	Abidžan
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
dopravní	dopravní	k2eAgFnSc7d1	dopravní
křižovatkou	křižovatka	k1gFnSc7	křižovatka
nejen	nejen	k6eAd1	nejen
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celé	celý	k2eAgFnSc2d1	celá
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
bývalé	bývalý	k2eAgFnPc4d1	bývalá
francouzské	francouzský	k2eAgFnPc4d1	francouzská
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
má	mít	k5eAaImIp3nS	mít
historické	historický	k2eAgFnSc2d1	historická
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
autobusového	autobusový	k2eAgNnSc2d1	autobusové
nádraží	nádraží	k1gNnSc2	nádraží
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Adjamé	Adjamý	k2eAgFnSc6d1	Adjamý
je	být	k5eAaImIp3nS	být
přímé	přímý	k2eAgNnSc4d1	přímé
spojení	spojení	k1gNnSc4	spojení
nejen	nejen	k6eAd1	nejen
do	do	k7c2	do
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
do	do	k7c2	do
sousedních	sousední	k2eAgInPc2d1	sousední
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Abidžanu	Abidžan	k1gInSc2	Abidžan
vede	vést	k5eAaImIp3nS	vést
železnice	železnice	k1gFnSc1	železnice
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
Ouagadougou	Ouagadouga	k1gFnSc7	Ouagadouga
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Burkiny	Burkina	k1gFnSc2	Burkina
Faso	Faso	k1gMnSc1	Faso
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgNnPc7	dva
městy	město	k1gNnPc7	město
trvá	trvat	k5eAaImIp3nS	trvat
40	[number]	k4	40
hodina	hodina	k1gFnSc1	hodina
a	a	k8xC	a
trať	trať	k1gFnSc1	trať
je	být	k5eAaImIp3nS	být
obsluhována	obsluhovat	k5eAaImNgFnS	obsluhovat
dvěma	dva	k4xCgInPc7	dva
vlaky	vlak	k1gInPc7	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Abidžanské	Abidžanský	k2eAgNnSc1d1	Abidžanský
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Félixe	Félixe	k1gFnSc2	Félixe
Houphouët-Boignye	Houphouët-Boigny	k1gFnSc2	Houphouët-Boigny
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
též	též	k9	též
Bouët	Bouët	k1gInSc4	Bouët
podle	podle	k7c2	podle
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
region	region	k1gInSc4	region
<g/>
.	.	kIx.	.
</s>
<s>
Letadla	letadlo	k1gNnPc1	letadlo
odtud	odtud	k6eAd1	odtud
létají	létat	k5eAaImIp3nP	létat
na	na	k7c6	na
mezikontinentálních	mezikontinentální	k2eAgFnPc6d1	mezikontinentální
linkách	linka	k1gFnPc6	linka
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
na	na	k7c4	na
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
uzpůsobené	uzpůsobený	k2eAgNnSc1d1	uzpůsobené
i	i	k8xC	i
pro	pro	k7c4	pro
velká	velký	k2eAgNnPc4d1	velké
dopravní	dopravní	k2eAgNnPc4d1	dopravní
letadla	letadlo	k1gNnPc4	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Abidžanský	Abidžanský	k2eAgInSc1d1	Abidžanský
námořní	námořní	k2eAgInSc1d1	námořní
přístav	přístav	k1gInSc1	přístav
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
vybudováním	vybudování	k1gNnSc7	vybudování
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
hlubokého	hluboký	k2eAgInSc2d1	hluboký
kanálu	kanál	k1gInSc2	kanál
Vridi	Vrid	k1gMnPc1	Vrid
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
zde	zde	k6eAd1	zde
tak	tak	k6eAd1	tak
kotvit	kotvit	k5eAaImF	kotvit
i	i	k9	i
lodě	loď	k1gFnPc4	loď
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
ponorem	ponor	k1gInSc7	ponor
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
slouží	sloužit	k5eAaImIp3nS	sloužit
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
Pobřeží	pobřeží	k1gNnSc4	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
využívá	využívat	k5eAaImIp3nS	využívat
ho	on	k3xPp3gMnSc4	on
hojně	hojně	k6eAd1	hojně
i	i	k9	i
Mali	Mali	k1gNnSc1	Mali
<g/>
,	,	kIx,	,
Burkina	Burkina	k1gFnSc1	Burkina
Faso	Faso	k1gNnSc1	Faso
a	a	k8xC	a
Niger	Niger	k1gInSc1	Niger
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
využitelnost	využitelnost	k1gFnSc1	využitelnost
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
jednak	jednak	k8xC	jednak
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
a	a	k8xC	a
tedy	tedy	k9	tedy
přístupnosti	přístupnost	k1gFnSc2	přístupnost
pro	pro	k7c4	pro
lodě	loď	k1gFnPc4	loď
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
ponorem	ponor	k1gInSc7	ponor
a	a	k8xC	a
jednak	jednak	k8xC	jednak
z	z	k7c2	z
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc2	uspořádání
dopravní	dopravní	k2eAgFnSc2d1	dopravní
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yIgInPc4	který
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
míří	mířit	k5eAaImIp3nS	mířit
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
jen	jen	k9	jen
samotný	samotný	k2eAgInSc1d1	samotný
Abidžan	Abidžan	k1gInSc1	Abidžan
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
60	[number]	k4	60
%	%	kIx~	%
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
země	zem	k1gFnSc2	zem
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
využíván	využívat	k5eAaImNgInS	využívat
místními	místní	k2eAgFnPc7d1	místní
továrnami	továrna	k1gFnPc7	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Výsadní	výsadní	k2eAgNnSc1d1	výsadní
postavení	postavení	k1gNnSc1	postavení
má	mít	k5eAaImIp3nS	mít
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
v	v	k7c6	v
šelfu	šelf	k1gInSc6	šelf
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zpracovávána	zpracovávat	k5eAaImNgFnS	zpracovávat
v	v	k7c6	v
rafinerii	rafinerie	k1gFnSc6	rafinerie
u	u	k7c2	u
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
tvořily	tvořit	k5eAaImAgInP	tvořit
ropné	ropný	k2eAgInPc1d1	ropný
produkty	produkt	k1gInPc1	produkt
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
z	z	k7c2	z
tonáže	tonáž	k1gFnSc2	tonáž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prošla	projít	k5eAaPmAgFnS	projít
přístavem	přístav	k1gInSc7	přístav
(	(	kIx(	(
<g/>
5,5	[number]	k4	5,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
z	z	k7c2	z
12	[number]	k4	12
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
celkem	celkem	k6eAd1	celkem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
Abidžanská	Abidžanský	k2eAgFnSc1d1	Abidžanský
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
sbírky	sbírka	k1gFnPc1	sbírka
umění	umění	k1gNnSc2	umění
domorodců	domorodec	k1gMnPc2	domorodec
(	(	kIx(	(
<g/>
masky	maska	k1gFnPc1	maska
<g/>
,	,	kIx,	,
šperky	šperk	k1gInPc1	šperk
<g/>
,	,	kIx,	,
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
bubny	buben	k1gInPc1	buben
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgInPc1d1	hudební
nástroje	nástroj	k1gInPc1	nástroj
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
částí	část	k1gFnPc2	část
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
v	v	k7c6	v
Plateau	Plateaus	k1gInSc6	Plateaus
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
širokých	široký	k2eAgFnPc2d1	široká
tříd	třída	k1gFnPc2	třída
a	a	k8xC	a
rozlehlých	rozlehlý	k2eAgInPc2d1	rozlehlý
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
knihoven	knihovna	k1gFnPc2	knihovna
a	a	k8xC	a
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
ústavů	ústav	k1gInPc2	ústav
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
obchodů	obchod	k1gInPc2	obchod
s	s	k7c7	s
místními	místní	k2eAgInPc7d1	místní
uměleckými	umělecký	k2eAgInPc7d1	umělecký
a	a	k8xC	a
řemeslnými	řemeslný	k2eAgInPc7d1	řemeslný
výrobky	výrobek	k1gInPc7	výrobek
a	a	k8xC	a
nejrůznějšími	různý	k2eAgInPc7d3	nejrůznější
produkty	produkt	k1gInPc7	produkt
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
se	s	k7c7	s
západním	západní	k2eAgNnSc7d1	západní
zbožím	zboží	k1gNnSc7	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
místní	místní	k2eAgFnPc4d1	místní
pamětihodnosti	pamětihodnost	k1gFnPc4	pamětihodnost
patří	patřit	k5eAaImIp3nS	patřit
moderní	moderní	k2eAgFnSc1d1	moderní
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Alda	Ald	k2eAgFnSc1d1	Alda
Spirita	Spirita	k?	Spirita
vysvěcená	vysvěcený	k2eAgFnSc1d1	vysvěcená
papežem	papež	k1gMnSc7	papež
Janem	Jan	k1gMnSc7	Jan
Pavlem	Pavel	k1gMnSc7	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
zapadá	zapadat	k5eAaImIp3nS	zapadat
dobře	dobře	k6eAd1	dobře
svojí	svůj	k3xOyFgFnSc7	svůj
architekturou	architektura	k1gFnSc7	architektura
mezi	mezi	k7c4	mezi
mrakodrapy	mrakodrap	k1gInPc4	mrakodrap
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Plateau	Plateaus	k1gInSc2	Plateaus
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
i	i	k9	i
Cocodské	Cocodský	k2eAgNnSc1d1	Cocodský
městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
čtvrti	čtvrt	k1gFnSc6	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Banco	Banco	k6eAd1	Banco
je	být	k5eAaImIp3nS	být
rezervace	rezervace	k1gFnSc2	rezervace
původního	původní	k2eAgInSc2d1	původní
tropického	tropický	k2eAgInSc2d1	tropický
deštného	deštný	k2eAgInSc2d1	deštný
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
původně	původně	k6eAd1	původně
obklopoval	obklopovat	k5eAaImAgInS	obklopovat
celou	celý	k2eAgFnSc4d1	celá
lagunu	laguna	k1gFnSc4	laguna
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
běžný	běžný	k2eAgInSc4d1	běžný
podél	podél	k7c2	podél
Guinejského	guinejský	k2eAgInSc2d1	guinejský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
30	[number]	k4	30
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
sevřený	sevřený	k2eAgInSc1d1	sevřený
městskou	městský	k2eAgFnSc7d1	městská
aglomerací	aglomerace	k1gFnSc7	aglomerace
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
množství	množství	k1gNnSc1	množství
ptactva	ptactvo	k1gNnSc2	ptactvo
a	a	k8xC	a
opic	opice	k1gFnPc2	opice
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
vzácné	vzácný	k2eAgFnPc1d1	vzácná
dřeviny	dřevina	k1gFnPc1	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
upravované	upravovaný	k2eAgFnPc1d1	upravovaná
cesty	cesta	k1gFnPc1	cesta
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
návštěvníky	návštěvník	k1gMnPc4	návštěvník
a	a	k8xC	a
park	park	k1gInSc1	park
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k6eAd1	především
jako	jako	k9	jako
rekreační	rekreační	k2eAgFnSc1d1	rekreační
oblast	oblast	k1gFnSc1	oblast
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
Abidžanu	Abidžana	k1gFnSc4	Abidžana
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
arboretum	arboretum	k1gNnSc1	arboretum
se	s	k7c7	s
stromy	strom	k1gInPc7	strom
a	a	k8xC	a
keři	keř	k1gInPc7	keř
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Banco	Banco	k6eAd1	Banco
je	být	k5eAaImIp3nS	být
říční	říční	k2eAgFnSc1d1	říční
prádelna	prádelna	k1gFnSc1	prádelna
<g/>
.	.	kIx.	.
</s>
<s>
Pradláci	pradlák	k1gMnPc1	pradlák
a	a	k8xC	a
pradleny	pradlena	k1gFnPc1	pradlena
zde	zde	k6eAd1	zde
perou	prát	k5eAaImIp3nP	prát
prádlo	prádlo	k1gNnSc4	prádlo
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
a	a	k8xC	a
vydělávají	vydělávat	k5eAaImIp3nP	vydělávat
si	se	k3xPyFc3	se
tak	tak	k9	tak
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
<g/>
.	.	kIx.	.
</s>
<s>
Komuna	komuna	k1gFnSc1	komuna
Plateau	Plateaus	k1gInSc2	Plateaus
je	být	k5eAaImIp3nS	být
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
svými	svůj	k3xOyFgFnPc7	svůj
výškovými	výškový	k2eAgFnPc7d1	výšková
budovami	budova	k1gFnPc7	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
neobvyklými	obvyklý	k2eNgInPc7d1	neobvyklý
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obchody	obchod	k1gInPc7	obchod
a	a	k8xC	a
venkovními	venkovní	k2eAgFnPc7d1	venkovní
kavárnami	kavárna	k1gFnPc7	kavárna
je	být	k5eAaImIp3nS	být
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgMnPc4d1	obchodní
cestující	cestující	k1gMnPc4	cestující
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
místních	místní	k2eAgFnPc2d1	místní
kanceláří	kancelář	k1gFnSc7	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
několik	několik	k4yIc4	několik
filmových	filmový	k2eAgInPc2d1	filmový
festivalů	festival	k1gInPc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Abidžan	Abidžan	k1gInSc1	Abidžan
je	být	k5eAaImIp3nS	být
však	však	k9	však
především	především	k6eAd1	především
centrem	centrum	k1gNnSc7	centrum
hudby	hudba	k1gFnSc2	hudba
ve	v	k7c6	v
frankofonní	frankofonní	k2eAgFnSc6d1	frankofonní
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
barů	bar	k1gInPc2	bar
s	s	k7c7	s
živou	živý	k2eAgFnSc7d1	živá
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrají	hrát	k5eAaImIp3nP	hrát
hudebníci	hudebník	k1gMnPc1	hudebník
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
centrum	centrum	k1gNnSc4	centrum
afrického	africký	k2eAgInSc2d1	africký
reggae	regga	k1gInSc2	regga
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
africké	africký	k2eAgFnSc6d1	africká
taneční	taneční	k2eAgFnSc6d1	taneční
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
hudebních	hudební	k2eAgInPc2d1	hudební
festivalů	festival	k1gInPc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
řada	řada	k1gFnSc1	řada
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgMnPc1d3	nejúspěšnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
ASEC	ASEC	kA	ASEC
Mimomasas	Mimomasas	k1gInSc1	Mimomasas
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
národním	národní	k2eAgInSc6d1	národní
stadionu	stadion	k1gInSc6	stadion
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
65	[number]	k4	65
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
)	)	kIx)	)
a	a	k8xC	a
Africa	Africa	k1gFnSc1	Africa
Sports	Sportsa	k1gFnPc2	Sportsa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnSc3d1	velká
oblibě	obliba	k1gFnSc3	obliba
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
fotbalu	fotbal	k1gInSc2	fotbal
těší	těšit	k5eAaImIp3nS	těšit
např.	např.	kA	např.
basketbal	basketbal	k1gInSc4	basketbal
a	a	k8xC	a
ragby	ragby	k1gNnSc4	ragby
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vládních	vládní	k2eAgInPc2d1	vládní
úřadů	úřad	k1gInPc2	úřad
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
Abidžanu	Abidžan	k1gInSc6	Abidžan
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k8xC	i
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kancelář	kancelář	k1gFnSc1	kancelář
<g/>
,	,	kIx,	,
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
Ústavní	ústavní	k2eAgFnSc1d1	ústavní
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Port	port	k1gInSc4	port
Bouët	Bouët	k2eAgInSc4d1	Bouët
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
letiště	letiště	k1gNnSc2	letiště
sídlí	sídlet	k5eAaImIp3nS	sídlet
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
francouzských	francouzský	k2eAgFnPc2d1	francouzská
vojenských	vojenský	k2eAgFnPc2d1	vojenská
základen	základna	k1gFnPc2	základna
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
základna	základna	k1gFnSc1	základna
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
obsazená	obsazený	k2eAgFnSc1d1	obsazená
<g/>
.	.	kIx.	.
</s>
<s>
Abidžan	Abidžan	k1gInSc1	Abidžan
je	být	k5eAaImIp3nS	být
oblastním	oblastní	k2eAgNnSc7d1	oblastní
sídlem	sídlo	k1gNnSc7	sídlo
významných	významný	k2eAgInPc2d1	významný
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
úřadů	úřad	k1gInPc2	úřad
<g/>
:	:	kIx,	:
UNICEF	UNICEF	kA	UNICEF
<g/>
,	,	kIx,	,
Rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
program	program	k1gInSc1	program
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Světový	světový	k2eAgInSc1d1	světový
potravinový	potravinový	k2eAgInSc1d1	potravinový
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
servisní	servisní	k2eAgNnSc4d1	servisní
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
Populační	populační	k2eAgInSc1d1	populační
fond	fond	k1gInSc1	fond
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc1d1	měnový
fond	fond	k1gInSc1	fond
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
