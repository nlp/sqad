<s>
Publius	Publius	k1gMnSc1	Publius
Septimius	Septimius	k1gMnSc1	Septimius
Geta	Geta	k1gMnSc1	Geta
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
či	či	k8xC	či
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
189	[number]	k4	189
Mediolanum	Mediolanum	k1gInSc1	Mediolanum
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
či	či	k8xC	či
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
211	[number]	k4	211
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
vládnoucí	vládnoucí	k2eAgMnSc1d1	vládnoucí
od	od	k7c2	od
září	září	k1gNnSc2	září
/	/	kIx~	/
října	říjen	k1gInSc2	říjen
209	[number]	k4	209
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
211	[number]	k4	211
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
Septimiem	Septimius	k1gMnSc7	Septimius
Severem	sever	k1gInSc7	sever
a	a	k8xC	a
bratrem	bratr	k1gMnSc7	bratr
Caracallou	Caracalla	k1gMnSc7	Caracalla
<g/>
,	,	kIx,	,
od	od	k7c2	od
února	únor	k1gInSc2	únor
211	[number]	k4	211
pak	pak	k6eAd1	pak
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
Caracallou	Caracalla	k1gFnSc7	Caracalla
<g/>
.	.	kIx.	.
</s>
