<s>
Brynzové	brynzový	k2eAgFnPc1d1	brynzová
halušky	haluška	k1gFnPc1	haluška
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
bryndzové	bryndzový	k2eAgFnPc1d1	bryndzový
halušky	haluška	k1gFnPc1	haluška
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
slovenské	slovenský	k2eAgNnSc4d1	slovenské
národní	národní	k2eAgNnSc4d1	národní
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Připravují	připravovat	k5eAaImIp3nP	připravovat
se	se	k3xPyFc4	se
z	z	k7c2	z
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
brynzy	brynza	k1gFnSc2	brynza
a	a	k8xC	a
slaniny	slanina	k1gFnSc2	slanina
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
brynzových	brynzový	k2eAgFnPc2d1	brynzová
halušek	haluška	k1gFnPc2	haluška
zřejmě	zřejmě	k6eAd1	zřejmě
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
tradičním	tradiční	k2eAgInSc7d1	tradiční
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
slovenského	slovenský	k2eAgNnSc2d1	slovenské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
tradicí	tradice	k1gFnSc7	tradice
chovu	chov	k1gInSc2	chov
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
výrobou	výroba	k1gFnSc7	výroba
výrobků	výrobek	k1gInPc2	výrobek
z	z	k7c2	z
ovčího	ovčí	k2eAgNnSc2d1	ovčí
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
brynzy	brynza	k1gFnSc2	brynza
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
etnoložky	etnoložka	k1gFnSc2	etnoložka
Kataríny	Katarína	k1gFnPc4	Katarína
Nádaské	Nádaský	k2eAgFnPc4d1	Nádaský
se	se	k3xPyFc4	se
brynzové	brynzový	k2eAgFnSc2d1	brynzová
halušky	haluška	k1gFnSc2	haluška
ve	v	k7c6	v
slovenské	slovenský	k2eAgFnSc6d1	slovenská
kuchyni	kuchyně	k1gFnSc6	kuchyně
objevily	objevit	k5eAaPmAgInP	objevit
až	až	k6eAd1	až
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c7	pod
rumunským	rumunský	k2eAgInSc7d1	rumunský
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
