<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Elizabeth	Elizabeth	k1gFnSc7	Elizabeth
II	II	kA	II
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
Alexandra	Alexandra	k1gFnSc1	Alexandra
Mary	Mary	k1gFnSc1	Mary
<g/>
;	;	kIx,	;
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1926	[number]	k4	1926
Mayfair	Mayfair	k1gInSc1	Mayfair
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
královna	královna	k1gFnSc1	královna
šestnácti	šestnáct	k4xCc2	šestnáct
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
označovaných	označovaný	k2eAgNnPc2d1	označované
jako	jako	k8xS	jako
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
realm	realm	k1gMnSc1	realm
<g/>
.	.	kIx.	.
</s>
