<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Elizabeth	Elizabeth	k1gFnSc7	Elizabeth
II	II	kA	II
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
Alexandra	Alexandra	k1gFnSc1	Alexandra
Mary	Mary	k1gFnSc1	Mary
<g/>
;	;	kIx,	;
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1926	[number]	k4	1926
Mayfair	Mayfair	k1gInSc1	Mayfair
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
královna	královna	k1gFnSc1	královna
šestnácti	šestnáct	k4xCc2	šestnáct
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
označovaných	označovaný	k2eAgNnPc2d1	označované
jako	jako	k8xS	jako
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
realm	realm	k1gMnSc1	realm
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
státy	stát	k1gInPc4	stát
Antigua	Antiguum	k1gNnSc2	Antiguum
a	a	k8xC	a
Barbuda	Barbudo	k1gNnSc2	Barbudo
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Bahamy	Bahamy	k1gFnPc1	Bahamy
<g/>
,	,	kIx,	,
Barbados	Barbados	k1gInSc1	Barbados
<g/>
,	,	kIx,	,
Belize	Belize	k1gFnSc1	Belize
<g/>
,	,	kIx,	,
Grenada	Grenada	k1gFnSc1	Grenada
<g/>
,	,	kIx,	,
Jamajka	Jamajka	k1gFnSc1	Jamajka
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Papua-Nová	Papua-Nový	k2eAgFnSc1d1	Papua-Nová
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
Nevis	viset	k5eNaImRp2nS	viset
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
a	a	k8xC	a
Grenadiny	grenadina	k1gFnPc1	grenadina
<g/>
,	,	kIx,	,
Šalamounovy	Šalamounův	k2eAgInPc1d1	Šalamounův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Tuvalu	Tuval	k1gInSc2	Tuval
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
panovnici	panovnice	k1gFnSc4	panovnice
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
a	a	k8xC	a
plnění	plnění	k1gNnSc2	plnění
královských	královský	k2eAgFnPc2d1	královská
povinností	povinnost	k1gFnPc2	povinnost
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
zemi	zem	k1gFnSc4	zem
je	být	k5eAaImIp3nS	být
oddělené	oddělený	k2eAgNnSc1d1	oddělené
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
představitelkou	představitelka	k1gFnSc7	představitelka
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
funkcí	funkce	k1gFnPc2	funkce
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zvykovým	zvykový	k2eAgNnSc7d1	zvykové
právem	právo	k1gNnSc7	právo
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Britskou	britský	k2eAgFnSc7d1	britská
královnou	královna	k1gFnSc7	královna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Jiřího	Jiří	k1gMnSc2	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
období	období	k1gNnSc6	období
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
již	již	k9	již
za	za	k7c4	za
jejího	její	k3xOp3gMnSc4	její
otce	otec	k1gMnSc4	otec
<g/>
)	)	kIx)	)
a	a	k8xC	a
následnému	následný	k2eAgInSc3d1	následný
vzniku	vznik	k1gInSc3	vznik
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
britské	britský	k2eAgFnPc1d1	britská
kolonie	kolonie	k1gFnPc1	kolonie
získávaly	získávat	k5eAaImAgFnP	získávat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
královnou	královna	k1gFnSc7	královna
několika	několik	k4yIc2	několik
nově	nově	k6eAd1	nově
osamostatněných	osamostatněný	k2eAgFnPc2d1	osamostatněná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
později	pozdě	k6eAd2	pozdě
změnily	změnit	k5eAaPmAgFnP	změnit
svůj	svůj	k3xOyFgInSc4	svůj
status	status	k1gInSc4	status
na	na	k7c4	na
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
překonala	překonat	k5eAaPmAgFnS	překonat
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
vlády	vláda	k1gFnSc2	vláda
svoji	svůj	k3xOyFgFnSc4	svůj
praprababičku	praprababička	k1gFnSc4	praprababička
Viktorii	Viktoria	k1gFnSc4	Viktoria
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
nejdéle	dlouho	k6eAd3	dlouho
vládnoucím	vládnoucí	k2eAgMnSc7d1	vládnoucí
panovníkem	panovník	k1gMnSc7	panovník
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
windsorské	windsorský	k2eAgFnSc2d1	Windsorská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
Philipa	Philip	k1gMnSc4	Philip
<g/>
,	,	kIx,	,
vévodu	vévoda	k1gMnSc4	vévoda
z	z	k7c2	z
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgMnPc4	čtyři
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
také	také	k9	také
pětinásobnou	pětinásobný	k2eAgFnSc7d1	pětinásobná
prababičkou	prababička	k1gFnSc7	prababička
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	s	k7c7	s
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1926	[number]	k4	1926
jako	jako	k8xS	jako
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dcera	dcera	k1gFnSc1	dcera
prince	princ	k1gMnSc2	princ
Alberta	Albert	k1gMnSc2	Albert
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
jediného	jediný	k2eAgMnSc4d1	jediný
sourozence	sourozenec	k1gMnSc4	sourozenec
<g/>
,	,	kIx,	,
princeznu	princezna	k1gFnSc4	princezna
Margaretu	Margareta	k1gFnSc4	Margareta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgFnSc1	třetí
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
strýci	strýc	k1gMnSc6	strýc
Eduardovi	Eduard	k1gMnSc6	Eduard
a	a	k8xC	a
svém	svůj	k1gMnSc6	svůj
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Nezdálo	zdát	k5eNaImAgNnS	zdát
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nástupcem	nástupce	k1gMnSc7	nástupce
stane	stanout	k5eAaPmIp3nS	stanout
její	její	k3xOp3gMnSc1	její
strýc	strýc	k1gMnSc1	strýc
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ožení	oženit	k5eAaPmIp3nS	oženit
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jejího	její	k3xOp3gMnSc2	její
dědečka	dědeček	k1gMnSc2	dědeček
Jiřího	Jiří	k1gMnSc2	Jiří
V.	V.	kA	V.
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
stal	stát	k5eAaPmAgMnS	stát
panovníkem	panovník	k1gMnSc7	panovník
její	její	k3xOp3gInSc4	její
strýc	strýc	k1gMnSc1	strýc
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
kvůli	kvůli	k7c3	kvůli
snaze	snaha	k1gFnSc3	snaha
oženit	oženit	k5eAaPmF	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
dvakrát	dvakrát	k6eAd1	dvakrát
rozvedenou	rozvedený	k2eAgFnSc7d1	rozvedená
ženou	žena	k1gFnSc7	žena
abdikoval	abdikovat	k5eAaBmAgInS	abdikovat
a	a	k8xC	a
britským	britský	k2eAgMnSc7d1	britský
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
a	a	k8xC	a
Alžbětin	Alžbětin	k2eAgMnSc1d1	Alžbětin
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
aktem	akt	k1gInSc7	akt
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
postavení	postavení	k1gNnSc2	postavení
následnice	následnice	k1gFnSc2	následnice
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
ústavní	ústavní	k2eAgFnSc4d1	ústavní
historii	historie	k1gFnSc4	historie
na	na	k7c4	na
Eton	Eton	k1gNnSc4	Eton
College	College	k1gNnSc1	College
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
u	u	k7c2	u
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
canterburského	canterburský	k2eAgMnSc2d1	canterburský
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Učila	učít	k5eAaPmAgFnS	učít
se	se	k3xPyFc4	se
také	také	k9	také
moderní	moderní	k2eAgInPc1d1	moderní
jazyky	jazyk	k1gInPc1	jazyk
a	a	k8xC	a
stále	stále	k6eAd1	stále
plynule	plynule	k6eAd1	plynule
hovoří	hovořit	k5eAaImIp3nS	hovořit
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
stýkat	stýkat	k5eAaImF	stýkat
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
děvčaty	děvče	k1gNnPc7	děvče
svého	svůj	k3xOyFgInSc2	svůj
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
i	i	k9	i
dívčí	dívčí	k2eAgFnSc1d1	dívčí
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
principu	princip	k1gInSc6	princip
podobném	podobný	k2eAgInSc6d1	podobný
skautskému	skautský	k2eAgNnSc3d1	skautské
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
)	)	kIx)	)
organizovaná	organizovaný	k2eAgNnPc4d1	organizované
Buckinghamským	buckinghamský	k2eAgInSc7d1	buckinghamský
palácem	palác	k1gInSc7	palác
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
učila	učít	k5eAaPmAgFnS	učít
přednášet	přednášet	k5eAaImF	přednášet
<g/>
,	,	kIx,	,
plavat	plavat	k5eAaImF	plavat
<g/>
,	,	kIx,	,
tančit	tančit	k5eAaImF	tančit
<g/>
,	,	kIx,	,
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
vařit	vařit	k5eAaImF	vařit
<g/>
,	,	kIx,	,
starat	starat	k5eAaImF	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
vyšívat	vyšívat	k5eAaImF	vyšívat
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c4	o
skautské	skautský	k2eAgNnSc4d1	skautské
hnutí	hnutí	k1gNnSc4	hnutí
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
je	být	k5eAaImIp3nS	být
patronem	patron	k1gInSc7	patron
Skautské	skautský	k2eAgFnSc2d1	skautská
asociace	asociace	k1gFnSc2	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
zřejmě	zřejmě	k6eAd1	zřejmě
poprvé	poprvé	k6eAd1	poprvé
potkala	potkat	k5eAaPmAgFnS	potkat
svého	svůj	k3xOyFgMnSc4	svůj
budoucího	budoucí	k2eAgMnSc4d1	budoucí
manžela	manžel	k1gMnSc4	manžel
<g/>
,	,	kIx,	,
Philipa	Philip	k1gMnSc4	Philip
<g/>
,	,	kIx,	,
prince	princ	k1gMnSc2	princ
řeckého	řecký	k2eAgMnSc2d1	řecký
a	a	k8xC	a
dánského	dánský	k2eAgNnSc2d1	dánské
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1934	[number]	k4	1934
až	až	k9	až
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
královského	královský	k2eAgNnSc2d1	královské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
v	v	k7c6	v
Dartmouthu	Dartmouth	k1gInSc6	Dartmouth
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1939	[number]	k4	1939
se	se	k3xPyFc4	se
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
si	se	k3xPyFc3	se
začali	začít	k5eAaPmAgMnP	začít
posílat	posílat	k5eAaImF	posílat
psaní	psaní	k1gNnSc4	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
dožila	dožít	k5eAaPmAgFnS	dožít
101	[number]	k4	101
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
Vánoc	Vánoce	k1gFnPc2	Vánoce
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pobývala	pobývat	k5eAaImAgFnS	pobývat
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Balmoral	Balmoral	k1gFnSc2	Balmoral
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přesunuly	přesunout	k5eAaPmAgInP	přesunout
do	do	k7c2	do
Sandringham	Sandringham	k1gInSc4	Sandringham
House	house	k1gNnSc1	house
v	v	k7c6	v
Norfolku	Norfolek	k1gInSc6	Norfolek
a	a	k8xC	a
od	od	k7c2	od
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
bydlely	bydlet	k5eAaImAgInP	bydlet
na	na	k7c6	na
Windsorském	windsorský	k2eAgInSc6d1	windsorský
hradu	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgFnP	existovat
plány	plán	k1gInPc4	plán
pro	pro	k7c4	pro
evakuaci	evakuace	k1gFnSc4	evakuace
obou	dva	k4xCgFnPc2	dva
princezen	princezna	k1gFnPc2	princezna
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jejich	jejich	k3xOp3gMnPc7	jejich
rodiči	rodič	k1gMnPc7	rodič
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
jejich	jejich	k3xOp3gFnSc1	jejich
matka	matka	k1gFnSc1	matka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mé	můj	k3xOp1gFnPc1	můj
děti	dítě	k1gFnPc1	dítě
nikdy	nikdy	k6eAd1	nikdy
neodcestují	odcestovat	k5eNaPmIp3nP	odcestovat
beze	beze	k7c2	beze
mě	já	k3xPp1nSc2	já
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
nikdy	nikdy	k6eAd1	nikdy
neodejdu	odejít	k5eNaPmIp1nS	odejít
bez	bez	k7c2	bez
svého	svůj	k3xOyFgMnSc2	svůj
manžela	manžel	k1gMnSc2	manžel
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
král	král	k1gMnSc1	král
zemi	zem	k1gFnSc4	zem
neopustí	opustit	k5eNaPmIp3nS	opustit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
Commonwealthu	Commonwealth	k1gInSc6	Commonwealth
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
byl	být	k5eAaImAgInS	být
připravován	připravován	k2eAgInSc1d1	připravován
oddělený	oddělený	k2eAgInSc1d1	oddělený
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
přehlídka	přehlídka	k1gFnSc1	přehlídka
kanadských	kanadský	k2eAgFnPc2d1	kanadská
pilotek	pilotka	k1gFnPc2	pilotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1945	[number]	k4	1945
(	(	kIx(	(
<g/>
v	v	k7c6	v
necelých	celý	k2eNgNnPc6d1	necelé
19	[number]	k4	19
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
ženskému	ženský	k2eAgInSc3d1	ženský
pomocnému	pomocný	k2eAgInSc3d1	pomocný
sboru	sbor	k1gInSc3	sbor
jako	jako	k8xC	jako
nižší	nízký	k2eAgFnSc2d2	nižší
důstojnice	důstojnice	k1gFnSc2	důstojnice
Alžběta	Alžběta	k1gFnSc1	Alžběta
Windsorová	Windsorová	k1gFnSc1	Windsorová
<g/>
.	.	kIx.	.
</s>
<s>
Cvičila	cvičit	k5eAaImAgFnS	cvičit
se	se	k3xPyFc4	se
v	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
a	a	k8xC	a
údržbě	údržba	k1gFnSc3	údržba
vojenských	vojenský	k2eAgNnPc2d1	vojenské
nákladních	nákladní	k2eAgNnPc2d1	nákladní
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
funkce	funkce	k1gFnSc1	funkce
mladší	mladý	k2eAgFnSc2d2	mladší
velitelky	velitelka	k1gFnSc2	velitelka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rumunským	rumunský	k2eAgMnSc7d1	rumunský
králem	král	k1gMnSc7	král
Michalem	Michal	k1gMnSc7	Michal
I.	I.	kA	I.
jediným	jediný	k2eAgMnSc7d1	jediný
žijícím	žijící	k2eAgMnSc7d1	žijící
panovníkem	panovník	k1gMnSc7	panovník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
uniformě	uniforma	k1gFnSc6	uniforma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
při	při	k7c6	při
oslavách	oslava	k1gFnPc6	oslava
jejich	jejich	k3xOp3gInSc2	jejich
konce	konec	k1gInSc2	konec
se	se	k3xPyFc4	se
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
anonymně	anonymně	k6eAd1	anonymně
připojily	připojit	k5eAaPmAgFnP	připojit
k	k	k7c3	k
oslavujícím	oslavující	k2eAgInPc3d1	oslavující
davům	dav	k1gInPc3	dav
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
vzdáleného	vzdálený	k2eAgMnSc4d1	vzdálený
příbuzného	příbuzný	k1gMnSc4	příbuzný
(	(	kIx(	(
<g/>
prapravnuka	prapravnuk	k1gMnSc4	prapravnuk
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
<g/>
)	)	kIx)	)
prince	princ	k1gMnSc2	princ
Philipa	Philip	k1gMnSc2	Philip
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Philip	Philip	k1gMnSc1	Philip
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
svých	svůj	k3xOyFgInPc2	svůj
řeckých	řecký	k2eAgInPc2d1	řecký
a	a	k8xC	a
dánských	dánský	k2eAgInPc2d1	dánský
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
přijal	přijmout	k5eAaPmAgMnS	přijmout
titul	titul	k1gInSc4	titul
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
<g/>
.	.	kIx.	.
</s>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
se	se	k3xPyFc4	se
nevyhnul	vyhnout	k5eNaPmAgInS	vyhnout
kritice	kritika	k1gFnSc3	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Philip	Philip	k1gInSc1	Philip
byl	být	k5eAaImAgInS	být
jiného	jiný	k2eAgNnSc2d1	jiné
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
bez	bez	k7c2	bez
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
sestry	sestra	k1gFnPc1	sestra
se	se	k3xPyFc4	se
vdaly	vdát	k5eAaPmAgFnP	vdát
za	za	k7c4	za
německé	německý	k2eAgMnPc4d1	německý
šlechtice	šlechtic	k1gMnPc4	šlechtic
s	s	k7c7	s
vazbou	vazba	k1gFnSc7	vazba
na	na	k7c4	na
nacistické	nacistický	k2eAgNnSc4d1	nacistické
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
úplně	úplně	k6eAd1	úplně
nevzpamatovaly	vzpamatovat	k5eNaPmAgInP	vzpamatovat
ze	z	k7c2	z
zpustošení	zpustošení	k1gNnSc2	zpustošení
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
byl	být	k5eAaImAgInS	být
uplatňován	uplatňován	k2eAgInSc1d1	uplatňován
přídělový	přídělový	k2eAgInSc1d1	přídělový
systém	systém	k1gInSc1	systém
a	a	k8xC	a
Alžběta	Alžběta	k1gFnSc1	Alžběta
musela	muset	k5eAaImAgFnS	muset
ušetřit	ušetřit	k5eAaPmF	ušetřit
z	z	k7c2	z
přídělových	přídělový	k2eAgInPc2d1	přídělový
kupónů	kupón	k1gInPc2	kupón
na	na	k7c4	na
látku	látka	k1gFnSc4	látka
pro	pro	k7c4	pro
svatební	svatební	k2eAgInPc4d1	svatební
šaty	šat	k1gInPc4	šat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
sňatek	sňatek	k1gInSc1	sňatek
byl	být	k5eAaImAgInS	být
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k9	jako
první	první	k4xOgInSc1	první
záblesk	záblesk	k1gInSc1	záblesk
obnovy	obnova	k1gFnSc2	obnova
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
a	a	k8xC	a
Philip	Philip	k1gMnSc1	Philip
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
asi	asi	k9	asi
2500	[number]	k4	2500
svatebních	svatební	k2eAgInPc2d1	svatební
darů	dar	k1gInPc2	dar
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
potomek	potomek	k1gMnSc1	potomek
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
si	se	k3xPyFc3	se
pár	pár	k4xCyI	pár
pronajal	pronajmout	k5eAaPmAgInS	pronajmout
dům	dům	k1gInSc1	dům
Windlesham	Windlesham	k1gInSc1	Windlesham
Moor	Moora	k1gFnPc2	Moora
a	a	k8xC	a
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1949	[number]	k4	1949
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
Clarence	Clarenka	k1gFnSc6	Clarenka
House	house	k1gNnSc1	house
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
až	až	k9	až
1951	[number]	k4	1951
trávil	trávit	k5eAaImAgInS	trávit
Philip	Philip	k1gInSc4	Philip
určitá	určitý	k2eAgNnPc4d1	určité
období	období	k1gNnSc4	období
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
námořní	námořní	k2eAgMnSc1d1	námořní
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
Alžběta	Alžběta	k1gFnSc1	Alžběta
tam	tam	k6eAd1	tam
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
často	často	k6eAd1	často
pobývala	pobývat	k5eAaImAgFnS	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Philipova	Philipův	k2eAgMnSc2d1	Philipův
strýce	strýc	k1gMnSc2	strýc
lorda	lord	k1gMnSc2	lord
Moutbattena	Moutbatten	k2eAgFnSc1d1	Moutbatten
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
děti	dítě	k1gFnPc1	dítě
zůstavaly	zůstavat	k5eAaImAgFnP	zůstavat
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Zdraví	zdraví	k1gNnSc1	zdraví
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
a	a	k8xC	a
Alžběta	Alžběta	k1gFnSc1	Alžběta
ho	on	k3xPp3gInSc4	on
často	často	k6eAd1	často
zastupovala	zastupovat	k5eAaImAgFnS	zastupovat
na	na	k7c6	na
společenských	společenský	k2eAgFnPc6d1	společenská
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
cestovala	cestovat	k5eAaImAgFnS	cestovat
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
navštívila	navštívit	k5eAaPmAgFnS	navštívit
i	i	k9	i
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Harryho	Harry	k1gMnSc2	Harry
Trumana	Truman	k1gMnSc2	Truman
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
s	s	k7c7	s
plánovanou	plánovaný	k2eAgFnSc7d1	plánovaná
zastávkou	zastávka	k1gFnSc7	zastávka
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
ji	on	k3xPp3gFnSc4	on
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c4	o
úmrtí	úmrtí	k1gNnSc4	úmrtí
jejího	její	k3xOp3gMnSc2	její
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
královnou	královna	k1gFnSc7	královna
a	a	k8xC	a
pár	pár	k1gInSc1	pár
přerušil	přerušit	k5eAaPmAgInS	přerušit
svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
úmrtí	úmrtí	k1gNnSc3	úmrtí
královniny	královnin	k2eAgFnSc2d1	Královnina
babičky	babička	k1gFnSc2	babička
Marie	Maria	k1gFnSc2	Maria
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
se	s	k7c7	s
<g/>
,	,	kIx,	,
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Mariiným	Mariin	k2eAgNnSc7d1	Mariino
přáním	přání	k1gNnSc7	přání
<g/>
,	,	kIx,	,
konala	konat	k5eAaImAgFnS	konat
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
korunovace	korunovace	k1gFnSc2	korunovace
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
korunovace	korunovace	k1gFnSc2	korunovace
sledovalo	sledovat	k5eAaImAgNnS	sledovat
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
asi	asi	k9	asi
12	[number]	k4	12
miliónů	milión	k4xCgInPc2	milión
naslouchalo	naslouchat	k5eAaImAgNnS	naslouchat
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Alžbětina	Alžbětin	k2eAgNnSc2d1	Alžbětino
dospívání	dospívání	k1gNnSc2	dospívání
probíhala	probíhat	k5eAaImAgFnS	probíhat
transformace	transformace	k1gFnSc1	transformace
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
moderní	moderní	k2eAgFnSc2d1	moderní
formy	forma	k1gFnSc2	forma
společenství	společenství	k1gNnSc2	společenství
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
transformace	transformace	k1gFnSc1	transformace
ukončena	ukončit	k5eAaPmNgFnS	ukončit
a	a	k8xC	a
Alžběta	Alžběta	k1gFnSc1	Alžběta
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
konsolidované	konsolidovaný	k2eAgFnSc6d1	konsolidovaná
pozici	pozice	k1gFnSc6	pozice
formální	formální	k2eAgFnSc2d1	formální
hlavy	hlava	k1gFnSc2	hlava
mnoha	mnoho	k4c2	mnoho
samostatných	samostatný	k2eAgInPc2d1	samostatný
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
až	až	k9	až
1954	[number]	k4	1954
podnikla	podniknout	k5eAaPmAgFnS	podniknout
královna	královna	k1gFnSc1	královna
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
šestiměsíční	šestiměsíční	k2eAgFnSc4d1	šestiměsíční
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgMnSc6	první
panovníkem	panovník	k1gMnSc7	panovník
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
její	její	k3xOp3gFnSc6	její
návštěvě	návštěva	k1gFnSc6	návštěva
viděly	vidět	k5eAaImAgFnP	vidět
asi	asi	k9	asi
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
obyvatel	obyvatel	k1gMnPc2	obyvatel
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1956	[number]	k4	1956
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
Suezské	suezský	k2eAgFnSc3d1	Suezská
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Izrael	Izrael	k1gInSc1	Izrael
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusily	pokusit	k5eAaPmAgFnP	pokusit
získat	získat	k5eAaPmF	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Suezským	suezský	k2eAgInSc7d1	suezský
průplavem	průplav	k1gInSc7	průplav
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
britský	britský	k2eAgMnSc1d1	britský
premiér	premiér	k1gMnSc1	premiér
Anthony	Anthona	k1gFnSc2	Anthona
Eden	Eden	k1gInSc1	Eden
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konzervativní	konzervativní	k2eAgFnSc6d1	konzervativní
straně	strana	k1gFnSc6	strana
neexistoval	existovat	k5eNaImAgInS	existovat
mechanismus	mechanismus	k1gInSc1	mechanismus
pro	pro	k7c4	pro
volbu	volba	k1gFnSc4	volba
nového	nový	k2eAgMnSc2d1	nový
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Alžbětě	Alžběta	k1gFnSc6	Alžběta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vybrala	vybrat	k5eAaPmAgFnS	vybrat
člena	člen	k1gMnSc4	člen
konzervativců	konzervativec	k1gMnPc2	konzervativec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
sestavil	sestavit	k5eAaPmAgInS	sestavit
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konzultacích	konzultace	k1gFnPc6	konzultace
s	s	k7c7	s
důležitými	důležitý	k2eAgMnPc7d1	důležitý
členy	člen	k1gMnPc7	člen
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgInS	pověřit
sestavením	sestavení	k1gNnSc7	sestavení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
Harold	Harold	k1gMnSc1	Harold
Macmillan	Macmillan	k1gMnSc1	Macmillan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
Macmillan	Macmillany	k1gInPc2	Macmillany
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
a	a	k8xC	a
doporučil	doporučit	k5eAaPmAgInS	doporučit
královně	královna	k1gFnSc3	královna
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
Aleca	Alecus	k1gMnSc4	Alecus
Douglase-Homeho	Douglase-Home	k1gMnSc4	Douglase-Home
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
musela	muset	k5eAaImAgFnS	muset
Alžběta	Alžběta	k1gFnSc1	Alžběta
čelit	čelit	k5eAaImF	čelit
výtkám	výtka	k1gFnPc3	výtka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
s	s	k7c7	s
pouze	pouze	k6eAd1	pouze
malou	malý	k2eAgFnSc7d1	malá
skupinou	skupina	k1gFnSc7	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
přijala	přijmout	k5eAaPmAgFnS	přijmout
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
strana	strana	k1gFnSc1	strana
vnitrostranický	vnitrostranický	k2eAgInSc1d1	vnitrostranický
mechanismus	mechanismus	k1gInSc1	mechanismus
volby	volba	k1gFnSc2	volba
svého	svůj	k3xOyFgMnSc2	svůj
nového	nový	k2eAgMnSc2d1	nový
předsedy	předseda	k1gMnSc2	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
Suezská	suezský	k2eAgFnSc1d1	Suezská
krize	krize	k1gFnSc1	krize
a	a	k8xC	a
jmenování	jmenování	k1gNnSc1	jmenování
Edenova	Edenův	k2eAgMnSc2d1	Edenův
nástupce	nástupce	k1gMnSc2	nástupce
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
první	první	k4xOgFnSc3	první
osobní	osobní	k2eAgFnSc3d1	osobní
kritice	kritika	k1gFnSc3	kritika
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Grigg	Grigg	k1gMnSc1	Grigg
<g/>
,	,	kIx,	,
baron	baron	k1gMnSc1	baron
z	z	k7c2	z
Altrinchamu	Altrincham	k1gInSc2	Altrincham
<g/>
,	,	kIx,	,
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vlastním	vlastní	k2eAgInSc6d1	vlastní
časopise	časopis	k1gInSc6	časopis
kritické	kritický	k2eAgInPc4d1	kritický
články	článek	k1gInPc4	článek
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ale	ale	k9	ale
veřejností	veřejnost	k1gFnSc7	veřejnost
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
Alžběta	Alžběta	k1gFnSc1	Alžběta
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
státní	státní	k2eAgFnSc4d1	státní
návštěvu	návštěva	k1gFnSc4	návštěva
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
navštívila	navštívit	k5eAaPmAgFnS	navštívit
též	též	k9	též
sídlo	sídlo	k1gNnSc4	sídlo
OSN	OSN	kA	OSN
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
pronesla	pronést	k5eAaPmAgFnS	pronést
projev	projev	k1gInSc4	projev
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
Valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
zahájila	zahájit	k5eAaPmAgFnS	zahájit
také	také	k9	také
jednání	jednání	k1gNnSc4	jednání
kanadského	kanadský	k2eAgInSc2d1	kanadský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
kanadský	kanadský	k2eAgMnSc1d1	kanadský
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaPmAgMnS	učinit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Kanadu	Kanada	k1gFnSc4	Kanada
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
procestovala	procestovat	k5eAaPmAgFnS	procestovat
Kypr	Kypr	k1gInSc4	Kypr
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc4	Indie
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc4	Pákistán
<g/>
,	,	kIx,	,
Nepál	Nepál	k1gInSc1	Nepál
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Ghanu	Ghana	k1gFnSc4	Ghana
<g/>
,	,	kIx,	,
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
obavy	obava	k1gFnPc4	obava
o	o	k7c4	o
vlastní	vlastní	k2eAgFnSc4d1	vlastní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
její	její	k3xOp3gMnSc1	její
hostitel	hostitel	k1gMnSc1	hostitel
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Kwame	Kwam	k1gInSc5	Kwam
Nkrumah	Nkrumah	k1gInSc4	Nkrumah
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nedlouho	dlouho	k6eNd1	dlouho
předtím	předtím	k6eAd1	předtím
terčem	terč	k1gInSc7	terč
atentátníků	atentátník	k1gMnPc2	atentátník
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
těhotenství	těhotenství	k1gNnSc1	těhotenství
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1959	[number]	k4	1959
a	a	k8xC	a
1963	[number]	k4	1963
byly	být	k5eAaImAgInP	být
jediné	jediný	k2eAgInPc1d1	jediný
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
formálně	formálně	k6eAd1	formálně
nezahájila	zahájit	k5eNaPmAgFnS	zahájit
jednání	jednání	k1gNnSc3	jednání
britského	britský	k2eAgInSc2d1	britský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zahájením	zahájení	k1gNnSc7	zahájení
jednání	jednání	k1gNnSc1	jednání
pověřila	pověřit	k5eAaPmAgFnS	pověřit
lorda	lord	k1gMnSc4	lord
kancléře	kancléř	k1gMnSc4	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
slavnostně	slavnostně	k6eAd1	slavnostně
zahájila	zahájit	k5eAaPmAgFnS	zahájit
provoz	provoz	k1gInSc4	provoz
transatlantického	transatlantický	k2eAgNnSc2d1	transatlantické
telefonického	telefonický	k2eAgNnSc2d1	telefonické
spojení	spojení	k1gNnSc2	spojení
rozhovorem	rozhovor	k1gInSc7	rozhovor
s	s	k7c7	s
kanadským	kanadský	k2eAgMnSc7d1	kanadský
premiérem	premiér	k1gMnSc7	premiér
Johnem	John	k1gMnSc7	John
Difenbakerem	Difenbaker	k1gMnSc7	Difenbaker
<g/>
.	.	kIx.	.
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
zrychlující	zrychlující	k2eAgFnSc2d1	zrychlující
se	se	k3xPyFc4	se
dekolonizace	dekolonizace	k1gFnSc2	dekolonizace
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
přes	přes	k7c4	přes
20	[number]	k4	20
zemí	zem	k1gFnPc2	zem
získalo	získat	k5eAaPmAgNnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Británii	Británie	k1gFnSc6	Británie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
dohod	dohoda	k1gFnPc2	dohoda
<g/>
,	,	kIx,	,
následovaných	následovaný	k2eAgFnPc2d1	následovaná
plánovanými	plánovaný	k2eAgInPc7d1	plánovaný
přechody	přechod	k1gInPc7	přechod
k	k	k7c3	k
vlastním	vlastní	k2eAgFnPc3d1	vlastní
samosprávám	samospráva	k1gFnPc3	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
ovšem	ovšem	k9	ovšem
rhodeský	rhodeský	k2eAgMnSc1d1	rhodeský
premiér	premiér	k1gMnSc1	premiér
Ian	Ian	k1gMnSc1	Ian
Smith	Smith	k1gMnSc1	Smith
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
jednostranně	jednostranně	k6eAd1	jednostranně
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
královna	královna	k1gFnSc1	královna
jeho	jeho	k3xOp3gFnSc4	jeho
deklaraci	deklarace	k1gFnSc4	deklarace
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
společenství	společenství	k1gNnSc1	společenství
uplatnilo	uplatnit	k5eAaPmAgNnS	uplatnit
sankce	sankce	k1gFnPc4	sankce
<g/>
,	,	kIx,	,
udržel	udržet	k5eAaPmAgInS	udržet
se	se	k3xPyFc4	se
Smithův	Smithův	k2eAgInSc1d1	Smithův
režim	režim	k1gInSc1	režim
dalších	další	k2eAgNnPc2d1	další
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1974	[number]	k4	1974
byly	být	k5eAaImAgFnP	být
nerozhodné	rozhodný	k2eNgFnPc1d1	nerozhodná
a	a	k8xC	a
znamenaly	znamenat	k5eAaImAgFnP	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
původní	původní	k2eAgMnSc1d1	původní
premiér	premiér	k1gMnSc1	premiér
Edward	Edward	k1gMnSc1	Edward
Heath	Heath	k1gMnSc1	Heath
mohl	moct	k5eAaImAgMnS	moct
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
sestavit	sestavit	k5eAaPmF	sestavit
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
Liberální	liberální	k2eAgFnSc7d1	liberální
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
aby	aby	kYmCp3nS	aby
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Heath	Heath	k1gMnSc1	Heath
tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
a	a	k8xC	a
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
až	až	k6eAd1	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
neuspěl	uspět	k5eNaPmAgMnS	uspět
se	s	k7c7	s
sestavením	sestavení	k1gNnSc7	sestavení
koaliční	koaliční	k2eAgFnSc2d1	koaliční
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
pak	pak	k6eAd1	pak
pověřila	pověřit	k5eAaPmAgFnS	pověřit
vytvořením	vytvoření	k1gNnSc7	vytvoření
vlády	vláda	k1gFnSc2	vláda
opozičního	opoziční	k2eAgMnSc2d1	opoziční
vůdce	vůdce	k1gMnSc2	vůdce
Harolda	Harold	k1gMnSc2	Harold
Wilsona	Wilson	k1gMnSc2	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
australské	australský	k2eAgFnSc2d1	australská
ústavní	ústavní	k2eAgFnSc2d1	ústavní
krize	krize	k1gFnSc2	krize
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
odvolal	odvolat	k5eAaPmAgMnS	odvolat
britský	britský	k2eAgMnSc1d1	britský
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
John	John	k1gMnSc1	John
Kerr	Kerr	k1gMnSc1	Kerr
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
australského	australský	k2eAgMnSc2d1	australský
premiéra	premiér	k1gMnSc2	premiér
Gougha	Gough	k1gMnSc2	Gough
Whitlama	Whitlam	k1gMnSc2	Whitlam
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
získat	získat	k5eAaPmF	získat
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
rozpočtové	rozpočtový	k2eAgInPc4d1	rozpočtový
návrhy	návrh	k1gInPc4	návrh
v	v	k7c6	v
opozicí	opozice	k1gFnSc7	opozice
kontrolovaném	kontrolovaný	k2eAgInSc6d1	kontrolovaný
australském	australský	k2eAgInSc6d1	australský
Senátu	senát	k1gInSc6	senát
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Whitlam	Whitlam	k1gInSc1	Whitlam
měl	mít	k5eAaImAgInS	mít
většinu	většina	k1gFnSc4	většina
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
,	,	kIx,	,
mluvčí	mluvčí	k1gMnSc1	mluvčí
Sněmovny	sněmovna	k1gFnSc2	sněmovna
požádal	požádat	k5eAaPmAgMnS	požádat
královnu	královna	k1gFnSc4	královna
<g/>
,	,	kIx,	,
aby	aby	k9	aby
Kerrovo	Kerrův	k2eAgNnSc4d1	Kerrův
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zvrátila	zvrátit	k5eAaPmAgFnS	zvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
to	ten	k3xDgNnSc4	ten
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
kompetencí	kompetence	k1gFnPc2	kompetence
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
australská	australský	k2eAgFnSc1d1	australská
ústava	ústava	k1gFnSc1	ústava
vyhrazuje	vyhrazovat	k5eAaImIp3nS	vyhrazovat
generálnímu	generální	k2eAgMnSc3d1	generální
guvernérovi	guvernér	k1gMnSc3	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
výrazně	výrazně	k6eAd1	výrazně
zvedla	zvednout	k5eAaPmAgFnS	zvednout
popularitu	popularita	k1gFnSc4	popularita
australských	australský	k2eAgMnPc2d1	australský
republikánů	republikán	k1gMnPc2	republikán
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
oslavila	oslavit	k5eAaPmAgFnS	oslavit
stříbrné	stříbrný	k2eAgNnSc4d1	stříbrné
jubileum	jubileum	k1gNnSc4	jubileum
svého	svůj	k3xOyFgInSc2	svůj
nástupu	nástup	k1gInSc2	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Oslavy	oslava	k1gFnPc1	oslava
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
a	a	k8xC	a
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
díkuvzdání	díkuvzdání	k1gNnSc2	díkuvzdání
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
hodnostářů	hodnostář	k1gMnPc2	hodnostář
a	a	k8xC	a
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
představitelů	představitel	k1gMnPc2	představitel
mnoha	mnoho	k4c2	mnoho
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Oslavy	oslava	k1gFnPc1	oslava
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
velkou	velký	k2eAgFnSc7d1	velká
slavností	slavnost	k1gFnSc7	slavnost
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
i	i	k9	i
trasa	trasa	k1gFnSc1	trasa
Jubilee	Jubile	k1gInSc2	Jubile
Line	linout	k5eAaImIp3nS	linout
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
otevřená	otevřený	k2eAgFnSc1d1	otevřená
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
přijala	přijmout	k5eAaPmAgFnS	přijmout
jako	jako	k9	jako
státní	státní	k2eAgFnSc4d1	státní
návštěvu	návštěva	k1gFnSc4	návštěva
rumunského	rumunský	k2eAgMnSc2d1	rumunský
komunistického	komunistický	k2eAgMnSc2d1	komunistický
diktátora	diktátor	k1gMnSc2	diktátor
Nicolae	Nicola	k1gMnSc2	Nicola
Ceauș	Ceauș	k1gMnSc2	Ceauș
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
soukromě	soukromě	k6eAd1	soukromě
si	se	k3xPyFc3	se
myslela	myslet	k5eAaImAgFnS	myslet
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
"	"	kIx"	"
<g/>
krev	krev	k1gFnSc1	krev
na	na	k7c6	na
rukou	ruka	k1gFnPc6	ruka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1979	[number]	k4	1979
jí	jíst	k5eAaImIp3nS	jíst
přinesl	přinést	k5eAaPmAgInS	přinést
dvě	dva	k4xCgFnPc4	dva
těžké	těžký	k2eAgFnPc4d1	těžká
rány	rána	k1gFnPc4	rána
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
správce	správce	k1gMnSc1	správce
jejích	její	k3xOp3gFnPc2	její
obrazových	obrazový	k2eAgFnPc2d1	obrazová
sbírek	sbírka	k1gFnPc2	sbírka
Anthony	Anthona	k1gFnSc2	Anthona
Blunt	Blunt	k1gInSc1	Blunt
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
jako	jako	k8xC	jako
komunistický	komunistický	k2eAgMnSc1d1	komunistický
špion	špion	k1gMnSc1	špion
a	a	k8xC	a
Prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
irská	irský	k2eAgFnSc1d1	irská
republikánská	republikánský	k2eAgFnSc1d1	republikánská
armáda	armáda	k1gFnSc1	armáda
zavraždila	zavraždit	k5eAaPmAgFnS	zavraždit
jejího	její	k3xOp3gMnSc4	její
vzdáleného	vzdálený	k2eAgMnSc4d1	vzdálený
příbuzného	příbuzný	k1gMnSc4	příbuzný
a	a	k8xC	a
strýce	strýc	k1gMnSc4	strýc
jejího	její	k3xOp3gMnSc4	její
manžela	manžel	k1gMnSc4	manžel
Philipa	Philip	k1gMnSc4	Philip
lorda	lord	k1gMnSc4	lord
Mountbattena	Mountbatten	k2eAgFnSc1d1	Mountbattena
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Paula	Paul	k1gMnSc2	Paul
Martina	Martin	k1gMnSc2	Martin
se	se	k3xPyFc4	se
královna	královna	k1gFnSc1	královna
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
obávala	obávat	k5eAaImAgFnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kanadský	kanadský	k2eAgMnSc1d1	kanadský
premiér	premiér	k1gMnSc1	premiér
Pierre	Pierr	k1gInSc5	Pierr
Trudeau	Trudeaus	k1gInSc3	Trudeaus
nemá	mít	k5eNaImIp3nS	mít
valné	valný	k2eAgNnSc1d1	Valné
mínění	mínění	k1gNnSc1	mínění
o	o	k7c6	o
královském	královský	k2eAgInSc6d1	královský
majestátu	majestát	k1gInSc6	majestát
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
chování	chování	k1gNnSc1	chování
v	v	k7c6	v
Buckinghamském	buckinghamský	k2eAgInSc6d1	buckinghamský
paláci	palác	k1gInSc6	palác
a	a	k8xC	a
odstranění	odstranění	k1gNnSc6	odstranění
některých	některý	k3yIgInPc2	některý
kanadských	kanadský	k2eAgInPc2d1	kanadský
královských	královský	k2eAgInPc2d1	královský
symbolů	symbol	k1gInPc2	symbol
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gNnSc2	jeho
úřadování	úřadování	k1gNnSc2	úřadování
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
byl	být	k5eAaImAgMnS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
vyslán	vyslat	k5eAaPmNgInS	vyslat
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
projednali	projednat	k5eAaPmAgMnP	projednat
změny	změna	k1gFnPc4	změna
kanadské	kanadský	k2eAgFnSc2d1	kanadská
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zajímala	zajímat	k5eAaImAgFnS	zajímat
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
ústavní	ústavní	k2eAgFnSc4d1	ústavní
debatu	debata	k1gFnSc4	debata
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
lépe	dobře	k6eAd2	dobře
informována	informovat	k5eAaBmNgFnS	informovat
než	než	k8xS	než
někteří	některý	k3yIgMnPc1	některý
kanadští	kanadský	k2eAgMnPc1d1	kanadský
politici	politik	k1gMnPc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
zrušení	zrušení	k1gNnSc1	zrušení
vlivu	vliv	k1gInSc2	vliv
britského	britský	k2eAgInSc2d1	britský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vliv	vliv	k1gInSc1	vliv
monarchie	monarchie	k1gFnSc2	monarchie
zůstal	zůstat	k5eAaPmAgInS	zůstat
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
odvaha	odvaha	k1gFnSc1	odvaha
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
její	její	k3xOp3gFnPc1	její
jezdecké	jezdecký	k2eAgFnPc1d1	jezdecká
schopnosti	schopnost	k1gFnPc1	schopnost
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgFnP	projevit
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
střídání	střídání	k1gNnSc6	střídání
stráží	stráž	k1gFnPc2	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
po	po	k7c6	po
ulici	ulice	k1gFnSc6	ulice
Mall	Mall	k1gInSc1	Mall
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
bylo	být	k5eAaImAgNnS	být
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
šest	šest	k4xCc1	šest
střel	střela	k1gFnPc2	střela
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
svého	svůj	k3xOyFgMnSc2	svůj
koně	kůň	k1gMnSc2	kůň
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
jízdě	jízda	k1gFnSc6	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
královna	královna	k1gFnSc1	královna
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
riskantní	riskantní	k2eAgFnSc6d1	riskantní
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
probudila	probudit	k5eAaPmAgFnS	probudit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
ložnici	ložnice	k1gFnSc6	ložnice
v	v	k7c6	v
Buckinghamském	buckinghamský	k2eAgInSc6d1	buckinghamský
paláci	palác	k1gInSc6	palác
a	a	k8xC	a
spatřila	spatřit	k5eAaPmAgFnS	spatřit
cizího	cizí	k2eAgMnSc4d1	cizí
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
zůstala	zůstat	k5eAaPmAgFnS	zůstat
klidná	klidný	k2eAgFnSc1d1	klidná
a	a	k8xC	a
asi	asi	k9	asi
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
do	do	k7c2	do
příjezdu	příjezd	k1gInSc2	příjezd
policie	policie	k1gFnSc2	policie
s	s	k7c7	s
mužem	muž	k1gMnSc7	muž
mluvila	mluvit	k5eAaImAgFnS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
hostila	hostit	k5eAaImAgFnS	hostit
amerického	americký	k2eAgMnSc4d1	americký
prezidenta	prezident	k1gMnSc4	prezident
Ronalda	Ronald	k1gMnSc4	Ronald
Reagana	Reagan	k1gMnSc4	Reagan
na	na	k7c6	na
Windsorském	windsorský	k2eAgInSc6d1	windsorský
hradu	hrad	k1gInSc6	hrad
a	a	k8xC	a
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
navštívila	navštívit	k5eAaPmAgFnS	navštívit
jeho	jeho	k3xOp3gFnSc1	jeho
ranč	ranč	k1gFnSc1	ranč
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
rozzlobena	rozzlobit	k5eAaPmNgFnS	rozzlobit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
administrativa	administrativa	k1gFnSc1	administrativa
nařídila	nařídit	k5eAaPmAgFnS	nařídit
invazi	invaze	k1gFnSc4	invaze
na	na	k7c4	na
Grenadu	Grenada	k1gFnSc4	Grenada
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gNnPc2	její
dominií	dominion	k1gNnPc2	dominion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
britskou	britský	k2eAgFnSc7d1	britská
premiérkou	premiérka	k1gFnSc7	premiérka
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
uváděno	uvádět	k5eAaImNgNnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
královna	královna	k1gFnSc1	královna
obávala	obávat	k5eAaImAgFnS	obávat
její	její	k3xOp3gFnPc4	její
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
politiky	politika	k1gFnPc4	politika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
rozdělovala	rozdělovat	k5eAaImAgFnS	rozdělovat
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
znepokojena	znepokojit	k5eAaPmNgFnS	znepokojit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
,	,	kIx,	,
řadou	řada	k1gFnSc7	řada
nepokojů	nepokoj	k1gInPc2	nepokoj
<g/>
,	,	kIx,	,
násilím	násilí	k1gNnSc7	násilí
při	při	k7c6	při
stávkách	stávka	k1gFnPc6	stávka
horníků	horník	k1gMnPc2	horník
a	a	k8xC	a
neochotou	neochota	k1gFnSc7	neochota
Thatcherové	Thatcherová	k1gFnSc2	Thatcherová
uplatnit	uplatnit	k5eAaPmF	uplatnit
sankce	sankce	k1gFnPc4	sankce
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
uplatňujícímu	uplatňující	k2eAgNnSc3d1	uplatňující
politiku	politikum	k1gNnSc3	politikum
apartheidu	apartheid	k1gInSc2	apartheid
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
obou	dva	k4xCgFnPc2	dva
žen	žena	k1gFnPc2	žena
byly	být	k5eAaImAgInP	být
občas	občas	k6eAd1	občas
popisovány	popisovat	k5eAaImNgInP	popisovat
jako	jako	k8xS	jako
upřímný	upřímný	k2eAgInSc1d1	upřímný
odpor	odpor	k1gInSc1	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
těmto	tento	k3xDgFnPc3	tento
spekulacím	spekulace	k1gFnPc3	spekulace
označila	označit	k5eAaPmAgFnS	označit
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
Alžbětu	Alžběta	k1gFnSc4	Alžběta
za	za	k7c4	za
úžasnou	úžasný	k2eAgFnSc4d1	úžasná
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vždy	vždy	k6eAd1	vždy
ví	vědět	k5eAaImIp3nS	vědět
co	co	k3yQnSc1	co
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
vztahovalo	vztahovat	k5eAaImAgNnS	vztahovat
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
poslednímu	poslední	k2eAgNnSc3d1	poslední
setkání	setkání	k1gNnSc3	setkání
s	s	k7c7	s
královnou	královna	k1gFnSc7	královna
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
premiérky	premiérka	k1gFnSc2	premiérka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
odešla	odejít	k5eAaPmAgFnS	odejít
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
udělila	udělit	k5eAaPmAgFnS	udělit
jí	jíst	k5eAaImIp3nS	jíst
královna	královna	k1gFnSc1	královna
řád	řád	k1gInSc4	řád
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
a	a	k8xC	a
Podvazkový	podvazkový	k2eAgInSc4d1	podvazkový
řád	řád	k1gInSc4	řád
a	a	k8xC	a
oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
Philipem	Philip	k1gMnSc7	Philip
účastnili	účastnit	k5eAaImAgMnP	účastnit
oslav	oslava	k1gFnPc2	oslava
80	[number]	k4	80
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
Margarety	Margareta	k1gFnSc2	Margareta
Thatcherové	Thatcherová	k1gFnSc2	Thatcherová
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgInSc7	první
britským	britský	k2eAgInSc7d1	britský
panovníkem	panovník	k1gMnSc7	panovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pronesl	pronést	k5eAaPmAgMnS	pronést
projev	projev	k1gInSc4	projev
před	před	k7c7	před
Kongresem	kongres	k1gInSc7	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
zabránit	zabránit	k5eAaPmF	zabránit
rozpadu	rozpad	k1gInSc3	rozpad
manželství	manželství	k1gNnSc2	manželství
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Charlese	Charles	k1gMnSc2	Charles
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
Dianou	Diana	k1gFnSc7	Diana
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
snaha	snaha	k1gFnSc1	snaha
nebyla	být	k5eNaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
dvojice	dvojice	k1gFnSc1	dvojice
se	se	k3xPyFc4	se
formálně	formálně	k6eAd1	formálně
rozešla	rozejít	k5eAaPmAgFnS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
nazvala	nazvat	k5eAaBmAgFnS	nazvat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1992	[number]	k4	1992
tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
příšerným	příšerný	k2eAgMnSc7d1	příšerný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
syn	syn	k1gMnSc1	syn
se	se	k3xPyFc4	se
formálně	formálně	k6eAd1	formálně
rozešel	rozejít	k5eAaPmAgMnS	rozejít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
manželství	manželství	k1gNnSc1	manželství
druhého	druhý	k4xOgInSc2	druhý
procházelo	procházet	k5eAaImAgNnS	procházet
krizí	krize	k1gFnSc7	krize
<g/>
.	.	kIx.	.
</s>
<s>
Windsorský	windsorský	k2eAgInSc1d1	windsorský
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
poničen	poničit	k5eAaPmNgInS	poničit
požárem	požár	k1gInSc7	požár
a	a	k8xC	a
monarchie	monarchie	k1gFnSc1	monarchie
byla	být	k5eAaImAgFnS	být
vystavována	vystavovat	k5eAaImNgFnS	vystavovat
narůstající	narůstající	k2eAgFnSc3d1	narůstající
kritice	kritika	k1gFnSc3	kritika
a	a	k8xC	a
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
přetřásání	přetřásání	k1gNnSc3	přetřásání
jejích	její	k3xOp3gFnPc2	její
potíží	potíž	k1gFnPc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neobvykle	obvykle	k6eNd1	obvykle
osobním	osobní	k2eAgInSc6d1	osobní
projevu	projev	k1gInSc6	projev
připustila	připustit	k5eAaPmAgFnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
instituce	instituce	k1gFnSc1	instituce
musí	muset	k5eAaImIp3nS	muset
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přála	přát	k5eAaImAgFnS	přát
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
činěno	činit	k5eAaImNgNnS	činit
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
taktem	takt	k1gInSc7	takt
<g/>
,	,	kIx,	,
porozuměním	porozumění	k1gNnSc7	porozumění
a	a	k8xC	a
humorem	humor	k1gInSc7	humor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
veřejné	veřejný	k2eAgNnSc1d1	veřejné
propírání	propírání	k1gNnSc1	propírání
manželství	manželství	k1gNnSc2	manželství
jejího	její	k3xOp3gMnSc2	její
syna	syn	k1gMnSc2	syn
(	(	kIx(	(
<g/>
a	a	k8xC	a
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
)	)	kIx)	)
Charlese	Charles	k1gMnSc2	Charles
a	a	k8xC	a
Diany	Diana	k1gFnSc2	Diana
Spencerové	Spencerová	k1gFnSc2	Spencerová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
Johnem	John	k1gMnSc7	John
Majorem	major	k1gMnSc7	major
<g/>
,	,	kIx,	,
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
canterburským	canterburský	k2eAgMnSc7d1	canterburský
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgMnSc7	svůj
soukromým	soukromý	k2eAgMnSc7d1	soukromý
tajemníkem	tajemník	k1gMnSc7	tajemník
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
napsala	napsat	k5eAaBmAgFnS	napsat
Charlesovi	Charles	k1gMnSc6	Charles
a	a	k8xC	a
Dianě	Diana	k1gFnSc6	Diana
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
označila	označit	k5eAaPmAgFnS	označit
jejich	jejich	k3xOp3gInSc4	jejich
rozvod	rozvod	k1gInSc4	rozvod
za	za	k7c4	za
žádoucí	žádoucí	k2eAgInSc4d1	žádoucí
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Diana	Diana	k1gFnSc1	Diana
při	při	k7c6	při
autonehodě	autonehoda	k1gFnSc6	autonehoda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
královna	královna	k1gFnSc1	královna
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
vnuky	vnuk	k1gMnPc7	vnuk
na	na	k7c6	na
Balmoralském	Balmoralský	k2eAgInSc6d1	Balmoralský
zámku	zámek	k1gInSc6	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
chtěli	chtít	k5eAaImAgMnP	chtít
navštívit	navštívit	k5eAaPmF	navštívit
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
oba	dva	k4xCgMnPc4	dva
prarodiče	prarodič	k1gMnPc4	prarodič
navštívili	navštívit	k5eAaPmAgMnP	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Královně	královna	k1gFnSc3	královna
a	a	k8xC	a
jejímu	její	k3xOp3gMnSc3	její
manželovi	manžel	k1gMnSc3	manžel
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
držet	držet	k5eAaImF	držet
oba	dva	k4xCgInPc1	dva
Dianiny	Dianin	k2eAgInPc1d1	Dianin
syny	syn	k1gMnPc7	syn
na	na	k7c4	na
Balmoralu	Balmorala	k1gFnSc4	Balmorala
mimo	mimo	k7c4	mimo
pozornost	pozornost	k1gFnSc4	pozornost
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Odtažitost	odtažitost	k1gFnSc1	odtažitost
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
způsobila	způsobit	k5eAaPmAgFnS	způsobit
pohoršení	pohoršení	k1gNnSc4	pohoršení
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
premiéra	premiér	k1gMnSc2	premiér
Tonyho	Tony	k1gMnSc2	Tony
Blaira	Blair	k1gMnSc2	Blair
<g/>
,	,	kIx,	,
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
Alžběta	Alžběta	k1gFnSc1	Alžběta
s	s	k7c7	s
veřejným	veřejný	k2eAgNnSc7d1	veřejné
vystoupením	vystoupení	k1gNnSc7	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vyjádřila	vyjádřit	k5eAaPmAgNnP	vyjádřit
Dianě	Diana	k1gFnSc6	Diana
svůj	svůj	k3xOyFgInSc4	svůj
obdiv	obdiv	k1gInSc4	obdiv
jako	jako	k8xS	jako
matce	matka	k1gFnSc6	matka
svých	svůj	k3xOyFgMnPc2	svůj
vnuků	vnuk	k1gMnPc2	vnuk
<g/>
,	,	kIx,	,
princů	princ	k1gMnPc2	princ
Williama	William	k1gMnSc2	William
a	a	k8xC	a
Harryho	Harry	k1gMnSc2	Harry
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
vystoupení	vystoupení	k1gNnSc6	vystoupení
se	se	k3xPyFc4	se
nálada	nálada	k1gFnSc1	nálada
veřejnosti	veřejnost	k1gFnSc2	veřejnost
z	z	k7c2	z
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
proměnila	proměnit	k5eAaPmAgFnS	proměnit
na	na	k7c4	na
respekt	respekt	k1gInSc4	respekt
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
vztah	vztah	k1gInSc1	vztah
královny	královna	k1gFnSc2	královna
a	a	k8xC	a
Tonyho	Tony	k1gMnSc2	Tony
Blaira	Blair	k1gMnSc2	Blair
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
období	období	k1gNnSc6	období
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
až	až	k9	až
2002	[number]	k4	2002
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
některé	některý	k3yIgFnPc1	některý
skutečnosti	skutečnost	k1gFnPc1	skutečnost
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
byl	být	k5eAaImAgInS	být
napjatý	napjatý	k2eAgInSc1d1	napjatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
Alžběta	Alžběta	k1gFnSc1	Alžběta
se	se	k3xPyFc4	se
cítila	cítit	k5eAaImAgFnS	cítit
podrážděná	podrážděný	k2eAgFnSc1d1	podrážděná
a	a	k8xC	a
frustrovaná	frustrovaný	k2eAgFnSc1d1	frustrovaná
Blairovými	Blairův	k2eAgFnPc7d1	Blairova
aktivitami	aktivita	k1gFnPc7	aktivita
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
příliš	příliš	k6eAd1	příliš
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
venkovským	venkovský	k2eAgNnSc7d1	venkovské
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k9	že
jí	on	k3xPp3gFnSc3	on
nabízel	nabízet	k5eAaImAgMnS	nabízet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
oslovovala	oslovovat	k5eAaImAgFnS	oslovovat
Tony	Tony	k1gFnSc1	Tony
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
znepokojena	znepokojit	k5eAaPmNgFnS	znepokojit
neúspěchy	neúspěch	k1gInPc4	neúspěch
britského	britský	k2eAgNnSc2d1	Britské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
překvapena	překvapit	k5eAaPmNgFnS	překvapit
přesunutím	přesunutí	k1gNnSc7	přesunutí
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
schůzek	schůzka	k1gFnPc2	schůzka
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
z	z	k7c2	z
úterý	úterý	k1gNnSc2	úterý
na	na	k7c4	na
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tonymu	Tony	k1gMnSc3	Tony
Blairovi	Blair	k1gMnSc3	Blair
tuto	tento	k3xDgFnSc4	tento
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
dávala	dávat	k5eAaImAgFnS	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
veřejně	veřejně	k6eAd1	veřejně
nevyjádřila	vyjádřit	k5eNaPmAgFnS	vyjádřit
k	k	k7c3	k
Iráku	Irák	k1gInSc3	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
nicméně	nicméně	k8xC	nicméně
oceňovala	oceňovat	k5eAaImAgFnS	oceňovat
Blairův	Blairův	k2eAgInSc4d1	Blairův
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
dosažení	dosažení	k1gNnSc6	dosažení
míru	mír	k1gInSc2	mír
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
oslavila	oslavit	k5eAaPmAgFnS	oslavit
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
jubileum	jubileum	k1gNnSc4	jubileum
svého	svůj	k3xOyFgInSc2	svůj
nástupu	nástup	k1gInSc2	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
svých	svůj	k3xOyFgFnPc6	svůj
dominiích	dominie	k1gFnPc6	dominie
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
královna	královna	k1gFnSc1	královna
označila	označit	k5eAaPmAgFnS	označit
uvítací	uvítací	k2eAgFnSc4d1	uvítací
oslavu	oslava	k1gFnSc4	oslava
za	za	k7c4	za
pozoruhodnou	pozoruhodný	k2eAgFnSc4d1	pozoruhodná
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
výpadek	výpadek	k1gInSc1	výpadek
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
ponořil	ponořit	k5eAaPmAgMnS	ponořit
oficiální	oficiální	k2eAgFnSc4d1	oficiální
guvernérovu	guvernérův	k2eAgFnSc4d1	guvernérova
rezidenci	rezidence	k1gFnSc4	rezidence
do	do	k7c2	do
tmy	tma	k1gFnSc2	tma
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
probíhaly	probíhat	k5eAaImAgFnP	probíhat
veřejné	veřejný	k2eAgFnPc1d1	veřejná
oslavy	oslava	k1gFnPc1	oslava
tohoto	tento	k3xDgNnSc2	tento
výročí	výročí	k1gNnSc2	výročí
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
skromnější	skromný	k2eAgFnPc1d2	skromnější
než	než	k8xS	než
u	u	k7c2	u
předchozího	předchozí	k2eAgNnSc2d1	předchozí
(	(	kIx(	(
<g/>
stříbrného	stříbrný	k2eAgNnSc2d1	stříbrné
<g/>
)	)	kIx)	)
jubilea	jubileum	k1gNnSc2	jubileum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
úmrtí	úmrtí	k1gNnSc2	úmrtí
královniny	královnin	k2eAgFnSc2d1	Královnina
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
sestry	sestra	k1gFnSc2	sestra
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
obdržela	obdržet	k5eAaPmAgFnS	obdržet
královna	královna	k1gFnSc1	královna
mnoho	mnoho	k6eAd1	mnoho
darů	dar	k1gInPc2	dar
a	a	k8xC	a
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
tohoto	tento	k3xDgNnSc2	tento
výročí	výročí	k1gNnSc2	výročí
byly	být	k5eAaImAgInP	být
pojmenovány	pojmenován	k2eAgInPc1d1	pojmenován
některé	některý	k3yIgInPc1	některý
památníky	památník	k1gInPc1	památník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgMnSc7	první
monarchou	monarcha	k1gMnSc7	monarcha
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pronesl	pronést	k5eAaPmAgMnS	pronést
projev	projev	k1gInSc4	projev
před	před	k7c7	před
zákonodárným	zákonodárný	k2eAgNnSc7d1	zákonodárné
shromážděním	shromáždění	k1gNnSc7	shromáždění
kanadské	kanadský	k2eAgFnSc2d1	kanadská
provincie	provincie	k1gFnSc2	provincie
Alberta	Albert	k1gMnSc2	Albert
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
oslavili	oslavit	k5eAaPmAgMnP	oslavit
60	[number]	k4	60
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
své	svůj	k3xOyFgFnSc2	svůj
svatby	svatba	k1gFnSc2	svatba
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
bohoslužbou	bohoslužba	k1gFnSc7	bohoslužba
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
,	,	kIx,	,
soukromou	soukromý	k2eAgFnSc7d1	soukromá
večeří	večeře	k1gFnSc7	večeře
pořádanou	pořádaný	k2eAgFnSc7d1	pořádaná
jejich	jejich	k3xOp3gMnSc7	jejich
synem	syn	k1gMnSc7	syn
Charlesem	Charles	k1gMnSc7	Charles
a	a	k8xC	a
večírkem	večírek	k1gInSc7	večírek
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
byli	být	k5eAaImAgMnP	být
pozvaní	pozvaný	k2eAgMnPc1d1	pozvaný
i	i	k8xC	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
předchozí	předchozí	k2eAgMnSc1d1	předchozí
a	a	k8xC	a
současný	současný	k2eAgMnSc1d1	současný
premiér	premiér	k1gMnSc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Diamantové	diamantový	k2eAgNnSc1d1	diamantové
jubileum	jubileum	k1gNnSc1	jubileum
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
60	[number]	k4	60
let	léto	k1gNnPc2	léto
od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
oslavila	oslavit	k5eAaPmAgFnS	oslavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poselství	poselství	k1gNnSc6	poselství
proneseném	pronesený	k2eAgNnSc6d1	pronesené
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
jubilea	jubileum	k1gNnSc2	jubileum
pronesla	pronést	k5eAaPmAgFnS	pronést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tento	tento	k3xDgInSc1	tento
speciální	speciální	k2eAgInSc1d1	speciální
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
zasvěcuji	zasvěcovat	k5eAaImIp1nS	zasvěcovat
službě	služba	k1gFnSc3	služba
Vám	vy	k3xPp2nPc3	vy
všem	všecek	k3xTgMnPc3	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
všichni	všechen	k3xTgMnPc1	všechen
připomeneme	připomenout	k5eAaPmIp1nP	připomenout
sílu	síla	k1gFnSc4	síla
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
a	a	k8xC	a
spojující	spojující	k2eAgFnSc4d1	spojující
sílu	síla	k1gFnSc4	síla
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
přátelství	přátelství	k1gNnSc2	přátelství
a	a	k8xC	a
dobrého	dobrý	k2eAgNnSc2d1	dobré
sousedství	sousedství	k1gNnSc2	sousedství
<g/>
...	...	k?	...
Také	také	k9	také
doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
jubilejní	jubilejní	k2eAgInSc1d1	jubilejní
rok	rok	k1gInSc1	rok
bude	být	k5eAaImBp3nS	být
časem	časem	k6eAd1	časem
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
díků	dík	k1gInPc2	dík
za	za	k7c4	za
velký	velký	k2eAgInSc4d1	velký
pokrok	pokrok	k1gInSc4	pokrok
dosažený	dosažený	k2eAgInSc4d1	dosažený
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
a	a	k8xC	a
časem	časem	k6eAd1	časem
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
jasnou	jasný	k2eAgFnSc7d1	jasná
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
otevřeným	otevřený	k2eAgNnSc7d1	otevřené
srdcem	srdce	k1gNnSc7	srdce
pohlédneme	pohlédnout	k5eAaPmIp1nP	pohlédnout
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
oficiálně	oficiálně	k6eAd1	oficiálně
otevřela	otevřít	k5eAaPmAgFnS	otevřít
XXI	XXI	kA	XXI
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
kanadském	kanadský	k2eAgInSc6d1	kanadský
Montréalu	Montréal	k1gInSc6	Montréal
a	a	k8xC	a
nechyběla	chybět	k5eNaImAgFnS	chybět
ani	ani	k8xC	ani
u	u	k7c2	u
zahájení	zahájení	k1gNnSc2	zahájení
XXX	XXX	kA	XXX
<g/>
.	.	kIx.	.
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2012	[number]	k4	2012
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
první	první	k4xOgFnSc7	první
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
oficiálně	oficiálně	k6eAd1	oficiálně
zahájila	zahájit	k5eAaPmAgFnS	zahájit
dvoje	dvoje	k4xRgFnPc4	dvoje
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
různých	různý	k2eAgInPc6d1	různý
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zahájení	zahájení	k1gNnSc6	zahájení
londýnské	londýnský	k2eAgFnSc2d1	londýnská
olympiády	olympiáda	k1gFnSc2	olympiáda
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
samu	sám	k3xTgFnSc4	sám
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
krátkem	krátek	k1gInSc7	krátek
filmu	film	k1gInSc2	film
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Daniela	Daniela	k1gFnSc1	Daniela
Craga	Craga	k1gFnSc1	Craga
jakožto	jakožto	k8xS	jakožto
Jamese	Jamese	k1gFnSc1	Jamese
Bonda	Bonda	k1gFnSc1	Bonda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
vystoupení	vystoupení	k1gNnSc4	vystoupení
obdržela	obdržet	k5eAaPmAgFnS	obdržet
čestnou	čestný	k2eAgFnSc4d1	čestná
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
za	za	k7c4	za
"	"	kIx"	"
<g/>
patronaci	patronace	k1gFnSc4	patronace
nad	nad	k7c7	nad
britským	britský	k2eAgInSc7d1	britský
filmovým	filmový	k2eAgInSc7d1	filmový
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
britský	britský	k2eAgMnSc1d1	britský
panovník	panovník	k1gMnSc1	panovník
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
a	a	k8xC	a
doby	doba	k1gFnPc1	doba
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
se	s	k7c7	s
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
zasedání	zasedání	k1gNnSc4	zasedání
britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
s	s	k7c7	s
příznaky	příznak	k1gInPc7	příznak
gastroenteritidy	gastroenteritis	k1gFnSc2	gastroenteritis
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
byla	být	k5eAaImAgFnS	být
propuštěna	propustit	k5eAaPmNgFnS	propustit
do	do	k7c2	do
domácího	domácí	k2eAgNnSc2d1	domácí
ošetření	ošetření	k1gNnSc2	ošetření
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
pokročilému	pokročilý	k2eAgInSc3d1	pokročilý
věku	věk	k1gInSc3	věk
a	a	k8xC	a
obtížnému	obtížný	k2eAgNnSc3d1	obtížné
cestování	cestování	k1gNnSc3	cestování
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
nezúčastnila	zúčastnit	k5eNaPmAgFnS	zúčastnit
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
bienálního	bienální	k2eAgNnSc2d1	bienální
setkání	setkání	k1gNnSc2	setkání
hlav	hlava	k1gFnPc2	hlava
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc2	lance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejstarším	starý	k2eAgMnSc7d3	nejstarší
britským	britský	k2eAgMnSc7d1	britský
panovníkem	panovník	k1gMnSc7	panovník
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejdéle	dlouho	k6eAd3	dlouho
vládnoucím	vládnoucí	k2eAgMnSc7d1	vládnoucí
britským	britský	k2eAgMnSc7d1	britský
panovníkem	panovník	k1gMnSc7	panovník
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
překonala	překonat	k5eAaPmAgFnS	překonat
svou	svůj	k3xOyFgFnSc4	svůj
praprababičku	praprababička	k1gFnSc4	praprababička
královnu	královna	k1gFnSc4	královna
Viktorii	Viktoria	k1gFnSc4	Viktoria
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejdéle	dlouho	k6eAd3	dlouho
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
zaznamenané	zaznamenaný	k2eAgFnSc6d1	zaznamenaná
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
thajského	thajský	k2eAgMnSc4d1	thajský
krále	král	k1gMnSc4	král
Pchúmipchona	Pchúmipchon	k1gMnSc2	Pchúmipchon
Adunjadéta	Adunjadét	k1gMnSc2	Adunjadét
(	(	kIx(	(
<g/>
Rámy	Ráma	k1gMnSc2	Ráma
IX	IX	kA	IX
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejdéle	dlouho	k6eAd3	dlouho
vládnoucím	vládnoucí	k2eAgMnSc7d1	vládnoucí
monarchou	monarcha	k1gMnSc7	monarcha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nemíní	mínit	k5eNaImIp3nS	mínit
abdikovat	abdikovat	k5eAaBmF	abdikovat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
od	od	k7c2	od
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
prince	princ	k1gMnSc2	princ
Charlese	Charles	k1gMnSc2	Charles
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
oslavila	oslavit	k5eAaPmAgFnS	oslavit
90	[number]	k4	90
<g/>
.	.	kIx.	.
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
přebere	přebrat	k5eAaPmIp3nS	přebrat
část	část	k1gFnSc4	část
jejích	její	k3xOp3gFnPc2	její
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
povinností	povinnost	k1gFnPc2	povinnost
<g/>
.	.	kIx.	.
</s>
