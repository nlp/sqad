<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
<g/>
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
DPMB	DPMB	kA	DPMB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
dopravcem	dopravce	k1gMnSc7	dopravce
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
hromadné	hromadný	k2eAgFnSc6d1	hromadná
dopravě	doprava	k1gFnSc6	doprava
na	na	k7c6	na
území	území	k1gNnSc6	území
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
jediným	jediný	k2eAgMnSc7d1	jediný
akcionářem	akcionář	k1gMnSc7	akcionář
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
provozované	provozovaný	k2eAgFnPc1d1	provozovaná
linky	linka	k1gFnPc1	linka
jsou	být	k5eAaImIp3nP	být
zapojeny	zapojit	k5eAaPmNgFnP	zapojit
do	do	k7c2	do
Integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
IDS	IDS	kA	IDS
JMK	JMK	kA	JMK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
jádro	jádro	k1gNnSc1	jádro
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
právě	právě	k6eAd1	právě
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
provozoval	provozovat	k5eAaImAgInS	provozovat
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c4	na
11	[number]	k4	11
tramvajových	tramvajový	k2eAgMnPc2d1	tramvajový
<g/>
,	,	kIx,	,
55	[number]	k4	55
městských	městský	k2eAgFnPc6d1	městská
autobusových	autobusový	k2eAgFnPc6d1	autobusová
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
34	[number]	k4	34
běžných	běžný	k2eAgMnPc2d1	běžný
denních	denní	k2eAgMnPc2d1	denní
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
expresní	expresní	k2eAgMnPc1d1	expresní
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
určené	určený	k2eAgInPc4d1	určený
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
tělesně	tělesně	k6eAd1	tělesně
postižené	postižený	k2eAgMnPc4d1	postižený
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgMnPc1	čtyři
školní	školní	k2eAgMnPc1d1	školní
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
bezplatná	bezplatný	k2eAgFnSc1d1	bezplatná
k	k	k7c3	k
nákupnímu	nákupní	k2eAgNnSc3d1	nákupní
centru	centrum	k1gNnSc3	centrum
Avion	avion	k1gInSc4	avion
a	a	k8xC	a
11	[number]	k4	11
nočních	noční	k2eAgFnPc2d1	noční
<g/>
)	)	kIx)	)
a	a	k8xC	a
13	[number]	k4	13
trolejbusových	trolejbusový	k2eAgFnPc6d1	trolejbusová
pravidelných	pravidelný	k2eAgFnPc6d1	pravidelná
linkách	linka	k1gFnPc6	linka
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
nejbližším	blízký	k2eAgNnSc6d3	nejbližší
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
provozovatelem	provozovatel	k1gMnSc7	provozovatel
jedné	jeden	k4xCgFnSc2	jeden
linky	linka	k1gFnSc2	linka
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
,	,	kIx,	,
dvou	dva	k4xCgFnPc6	dva
sezónních	sezónní	k2eAgFnPc2d1	sezónní
historických	historický	k2eAgFnPc2d1	historická
(	(	kIx(	(
<g/>
nostalgických	nostalgický	k2eAgFnPc2d1	nostalgická
<g/>
)	)	kIx)	)
linek	linka	k1gFnPc2	linka
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
seniorbusu	seniorbus	k1gInSc2	seniorbus
a	a	k8xC	a
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
vyhlídkových	vyhlídkový	k2eAgFnPc2d1	vyhlídková
jízd	jízda	k1gFnPc2	jízda
turistického	turistický	k2eAgInSc2d1	turistický
minibusu	minibus	k1gInSc2	minibus
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
městské	městský	k2eAgFnPc1d1	městská
linky	linka	k1gFnPc1	linka
DPMB	DPMB	kA	DPMB
zajížděly	zajíždět	k5eAaImAgFnP	zajíždět
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
i	i	k8xC	i
do	do	k7c2	do
některých	některý	k3yIgNnPc2	některý
okolních	okolní	k2eAgNnPc2d1	okolní
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
Kuřim	Kuřim	k1gInSc1	Kuřim
<g/>
,	,	kIx,	,
Modřice	Modřice	k1gFnSc1	Modřice
a	a	k8xC	a
Šlapanice	Šlapanice	k1gFnSc1	Šlapanice
<g/>
)	)	kIx)	)
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
Bílovice	Bílovice	k1gInPc1	Bílovice
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Kobylnice	Kobylnice	k1gFnPc1	Kobylnice
<g/>
,	,	kIx,	,
Lelekovice	Lelekovice	k1gFnPc1	Lelekovice
<g/>
,	,	kIx,	,
Prace	Praec	k1gInPc1	Praec
<g/>
,	,	kIx,	,
Sokolnice	sokolnice	k1gFnPc1	sokolnice
a	a	k8xC	a
Vranov	Vranov	k1gInSc1	Vranov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příměstská	příměstský	k2eAgFnSc1d1	příměstská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
regionální	regionální	k2eAgFnSc2d1	regionální
linky	linka	k1gFnSc2	linka
IDS	IDS	kA	IDS
JMK	JMK	kA	JMK
<g/>
)	)	kIx)	)
zajišťovaná	zajišťovaný	k2eAgFnSc1d1	zajišťovaná
DPMB	DPMB	kA	DPMB
obsluhovala	obsluhovat	k5eAaImAgFnS	obsluhovat
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Hvozdec	Hvozdec	k1gMnSc1	Hvozdec
<g/>
,	,	kIx,	,
Veverskou	Veverský	k2eAgFnSc4d1	Veverská
Bítýšku	Bítýška	k1gFnSc4	Bítýška
nebo	nebo	k8xC	nebo
Ostopovice	Ostopovice	k1gFnPc4	Ostopovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgNnSc7	první
městem	město	k1gNnSc7	město
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
tehdy	tehdy	k6eAd1	tehdy
koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
brněnská	brněnský	k2eAgFnSc1d1	brněnská
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
po	po	k7c6	po
pražské	pražský	k2eAgFnSc6d1	Pražská
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
přeměnou	přeměna	k1gFnSc7	přeměna
z	z	k7c2	z
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
státního	státní	k2eAgInSc2d1	státní
podniku	podnik	k1gInSc2	podnik
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
bez	bez	k7c2	bez
likvidace	likvidace	k1gFnSc2	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
DPMB	DPMB	kA	DPMB
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
řídil	řídit	k5eAaImAgMnS	řídit
stanovami	stanova	k1gFnPc7	stanova
upravenými	upravený	k2eAgFnPc7d1	upravená
a	a	k8xC	a
schválenými	schválený	k2eAgFnPc7d1	schválená
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
jediného	jediný	k2eAgMnSc2d1	jediný
akcionáře	akcionář	k1gMnSc2	akcionář
v	v	k7c6	v
působnosti	působnost	k1gFnSc6	působnost
valné	valný	k2eAgFnSc2d1	valná
hromady	hromada	k1gFnSc2	hromada
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
zavedl	zavést	k5eAaPmAgInS	zavést
DPMB	DPMB	kA	DPMB
prodej	prodej	k1gInSc4	prodej
SMS	SMS	kA	SMS
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
roku	rok	k1gInSc2	rok
a	a	k8xC	a
půl	půl	k6eAd1	půl
oproti	oproti	k7c3	oproti
původnímu	původní	k2eAgInSc3d1	původní
plánu	plán	k1gInSc3	plán
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejího	její	k3xOp3gMnSc2	její
provozovatele	provozovatel	k1gMnSc2	provozovatel
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vybrat	vybrat	k5eAaPmF	vybrat
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
výběrovém	výběrový	k2eAgNnSc6d1	výběrové
řízení	řízení	k1gNnSc6	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
zavedl	zavést	k5eAaPmAgInS	zavést
DPMB	DPMB	kA	DPMB
elektronické	elektronický	k2eAgNnSc1d1	elektronické
odbavování	odbavování	k1gNnSc1	odbavování
cestujících	cestující	k1gMnPc2	cestující
s	s	k7c7	s
předplatními	předplatní	k2eAgFnPc7d1	předplatní
jízdenkami	jízdenka	k1gFnPc7	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Jízdní	jízdní	k2eAgInSc1d1	jízdní
doklad	doklad	k1gInSc1	doklad
je	být	k5eAaImIp3nS	být
nahrán	nahrát	k5eAaBmNgInS	nahrát
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
platební	platební	k2eAgFnSc6d1	platební
kartě	karta	k1gFnSc6	karta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc4	rozdíl
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgNnPc3d1	jiné
městům	město	k1gNnPc3	město
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ODISka	ODISka	k1gFnSc1	ODISka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Lítačka	lítačka	k1gFnSc1	lítačka
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
:	:	kIx,	:
Ing.	ing.	kA	ing.
Miloš	Miloš	k1gMnSc1	Miloš
Havránek	Havránek	k1gMnSc1	Havránek
(	(	kIx(	(
<g/>
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Bedřich	Bedřich	k1gMnSc1	Bedřich
Prokeš	Prokeš	k1gMnSc1	Prokeš
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Technický	technický	k2eAgMnSc1d1	technický
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
:	:	kIx,	:
Ing.	ing.	kA	ing.
Jaromír	Jaromír	k1gMnSc1	Jaromír
Holec	holec	k1gMnSc1	holec
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Rudolf	Rudolf	k1gMnSc1	Rudolf
John	John	k1gMnSc1	John
(	(	kIx(	(
<g/>
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Provozní	provozní	k2eAgMnSc1d1	provozní
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
:	:	kIx,	:
Ing.	ing.	kA	ing.
Jiří	Jiří	k1gMnSc1	Jiří
Valníček	valníček	k1gInSc1	valníček
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
ředitelka	ředitelka	k1gFnSc1	ředitelka
<g/>
:	:	kIx,	:
Ing.	ing.	kA	ing.
Zuzana	Zuzana	k1gFnSc1	Zuzana
Ondroušková	Ondroušková	k1gFnSc1	Ondroušková
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Hana	Hana	k1gFnSc1	Hana
Černochová	Černochová	k1gFnSc1	Černochová
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Marhanová	Marhanová	k1gFnSc1	Marhanová
(	(	kIx(	(
<g/>
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
bylo	být	k5eAaImAgNnS	být
pátým	pátý	k4xOgNnSc7	pátý
městem	město	k1gNnSc7	město
v	v	k7c6	v
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc6	Rakousku-Uhersko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zavedlo	zavést	k5eAaPmAgNnS	zavést
koněspřežnou	koněspřežný	k2eAgFnSc4d1	koněspřežná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
před	před	k7c7	před
Brnem	Brno	k1gNnSc7	Brno
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
Pešti	Pešť	k1gFnSc6	Pešť
<g/>
,	,	kIx,	,
Budíně	Budín	k1gInSc6	Budín
a	a	k8xC	a
Temešváru	Temešvár	k1gInSc6	Temešvár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
první	první	k4xOgInSc1	první
úsek	úsek	k1gInSc1	úsek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vedl	vést	k5eAaImAgInS	vést
od	od	k7c2	od
Kiosku	kiosek	k1gInSc2	kiosek
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
do	do	k7c2	do
Kartouz	kartouza	k1gFnPc2	kartouza
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Semilasso	Semilassa	k1gFnSc5	Semilassa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
síť	síť	k1gFnSc1	síť
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
délky	délka	k1gFnPc4	délka
14,5	[number]	k4	14,5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
kvůli	kvůli	k7c3	kvůli
nerentabilitě	nerentabilita	k1gFnSc3	nerentabilita
zrušen	zrušen	k2eAgInSc1d1	zrušen
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
města	město	k1gNnSc2	město
ovšem	ovšem	k9	ovšem
chtěli	chtít	k5eAaImAgMnP	chtít
udržet	udržet	k5eAaPmF	udržet
pouliční	pouliční	k2eAgFnSc4d1	pouliční
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jednali	jednat	k5eAaImAgMnP	jednat
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
zájemci	zájemce	k1gMnPc7	zájemce
o	o	k7c4	o
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
obnoven	obnovit	k5eAaPmNgInS	obnovit
letní	letní	k2eAgInSc1d1	letní
provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
trati	trať	k1gFnSc6	trať
z	z	k7c2	z
Pisárek	Pisárka	k1gFnPc2	Pisárka
do	do	k7c2	do
Králova	Králův	k2eAgNnSc2d1	Královo
Pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
finančních	finanční	k2eAgFnPc2d1	finanční
byla	být	k5eAaImAgFnS	být
dráha	dráha	k1gFnSc1	dráha
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
inzerce	inzerce	k1gFnSc2	inzerce
byl	být	k5eAaImAgInS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
další	další	k2eAgMnSc1d1	další
zájemce	zájemce	k1gMnSc1	zájemce
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
zavedl	zavést	k5eAaPmAgMnS	zavést
místo	místo	k1gNnSc4	místo
koňky	koňka	k1gFnSc2	koňka
parní	parní	k2eAgFnSc4d1	parní
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
opět	opět	k6eAd1	opět
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Pisárky	Pisárka	k1gFnSc2	Pisárka
-	-	kIx~	-
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
a	a	k8xC	a
nově	nově	k6eAd1	nově
i	i	k9	i
na	na	k7c6	na
odbočce	odbočka	k1gFnSc6	odbočka
k	k	k7c3	k
Ústřednímu	ústřední	k2eAgInSc3d1	ústřední
hřbitovu	hřbitov	k1gInSc3	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
začaly	začít	k5eAaPmAgFnP	začít
neshody	neshoda	k1gFnPc1	neshoda
mezi	mezi	k7c7	mezi
městem	město	k1gNnSc7	město
a	a	k8xC	a
provozovatelem	provozovatel	k1gMnSc7	provozovatel
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
celý	celý	k2eAgInSc4d1	celý
podnik	podnik	k1gInSc4	podnik
prodal	prodat	k5eAaPmAgMnS	prodat
majitel	majitel	k1gMnSc1	majitel
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
elektrárenské	elektrárenský	k2eAgFnSc2d1	elektrárenská
společnosti	společnost	k1gFnSc2	společnost
Union	union	k1gInSc1	union
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
správce	správce	k1gMnSc1	správce
tratí	trať	k1gFnPc2	trať
přebudoval	přebudovat	k5eAaPmAgMnS	přebudovat
tratě	trať	k1gFnPc4	trať
na	na	k7c4	na
elektrické	elektrický	k2eAgMnPc4d1	elektrický
(	(	kIx(	(
<g/>
provoz	provoz	k1gInSc1	provoz
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
třetí	třetí	k4xOgFnSc1	třetí
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
náměstí	náměstí	k1gNnSc2	náměstí
Svobody	Svoboda	k1gMnSc2	Svoboda
do	do	k7c2	do
Zábrdovic	Zábrdovice	k1gFnPc2	Zábrdovice
<g/>
,	,	kIx,	,
linky	linka	k1gFnPc1	linka
byly	být	k5eAaImAgFnP	být
tehdy	tehdy	k6eAd1	tehdy
označeny	označen	k2eAgFnPc1d1	označena
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
také	také	k9	také
Společnost	společnost	k1gFnSc1	společnost
brněnských	brněnský	k2eAgFnPc2d1	brněnská
elektrických	elektrický	k2eAgFnPc2d1	elektrická
pouličních	pouliční	k2eAgFnPc2d1	pouliční
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
přímý	přímý	k2eAgMnSc1d1	přímý
předchůdce	předchůdce	k1gMnSc1	předchůdce
dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
převzat	převzít	k5eAaPmNgInS	převzít
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
další	další	k2eAgInPc1d1	další
úseky	úsek	k1gInPc1	úsek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
síť	síť	k1gFnSc1	síť
dokončena	dokončit	k5eAaPmNgFnS	dokončit
a	a	k8xC	a
bez	bez	k7c2	bez
výraznějších	výrazný	k2eAgFnPc2d2	výraznější
změn	změna	k1gFnPc2	změna
vydržela	vydržet	k5eAaPmAgFnS	vydržet
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
byly	být	k5eAaImAgFnP	být
linky	linka	k1gFnPc1	linka
označeny	označen	k2eAgFnPc1d1	označena
čísly	číslo	k1gNnPc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
odešli	odejít	k5eAaPmAgMnP	odejít
rakouští	rakouský	k2eAgMnPc1d1	rakouský
pracovníci	pracovník	k1gMnPc1	pracovník
dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
změněny	změněn	k2eAgInPc1d1	změněn
názvy	název	k1gInPc1	název
mnoha	mnoho	k4c2	mnoho
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
s	s	k7c7	s
prodlužováním	prodlužování	k1gNnSc7	prodlužování
tratí	trať	k1gFnPc2	trať
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
zdvoukolejňováním	zdvoukolejňování	k1gNnSc7	zdvoukolejňování
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
rozšíření	rozšíření	k1gNnSc1	rozšíření
sítě	síť	k1gFnSc2	síť
linek	linka	k1gFnPc2	linka
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
doprava	doprava	k1gFnSc1	doprava
do	do	k7c2	do
Maloměřic	Maloměřice	k1gFnPc2	Maloměřice
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Řečkovic	Řečkovice	k1gFnPc2	Řečkovice
<g/>
.	.	kIx.	.
</s>
<s>
Padesátá	padesátý	k4xOgNnPc4	padesátý
léta	léto	k1gNnPc4	léto
přinesla	přinést	k5eAaPmAgFnS	přinést
do	do	k7c2	do
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
spoustu	spousta	k1gFnSc4	spousta
inovací	inovace	k1gFnPc2	inovace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
regulovaný	regulovaný	k2eAgInSc4d1	regulovaný
nástup	nástup	k1gInSc4	nástup
a	a	k8xC	a
výstup	výstup	k1gInSc4	výstup
cestujících	cestující	k1gFnPc2	cestující
a	a	k8xC	a
dálkové	dálkový	k2eAgNnSc4d1	dálkové
ovládání	ovládání	k1gNnSc4	ovládání
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
zamezilo	zamezit	k5eAaPmAgNnS	zamezit
vyskakování	vyskakování	k1gNnSc1	vyskakování
za	za	k7c2	za
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
tarifních	tarifní	k2eAgNnPc2d1	tarifní
pásem	pásmo	k1gNnPc2	pásmo
a	a	k8xC	a
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
cenu	cena	k1gFnSc4	cena
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
jet	jet	k5eAaImF	jet
buď	buď	k8xC	buď
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
zastávku	zastávka	k1gFnSc4	zastávka
nebo	nebo	k8xC	nebo
až	až	k9	až
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
konec	konec	k1gInSc4	konec
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
byl	být	k5eAaImAgInS	být
zahájen	zahájen	k2eAgInSc1d1	zahájen
provoz	provoz	k1gInSc1	provoz
nových	nový	k2eAgInPc2d1	nový
vozů	vůz	k1gInPc2	vůz
typu	typ	k1gInSc2	typ
Tatra	Tatra	k1gFnSc1	Tatra
T	T	kA	T
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgMnPc4	který
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
vozovna	vozovna	k1gFnSc1	vozovna
v	v	k7c6	v
Medlánkách	Medlánka	k1gFnPc6	Medlánka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgNnP	být
zrušena	zrušit	k5eAaPmNgNnP	zrušit
doprava	doprava	k6eAd1	doprava
ulicí	ulice	k1gFnSc7	ulice
Kobližnou	kobližný	k2eAgFnSc7d1	Kobližná
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
tratí	trať	k1gFnPc2	trať
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc1	jejich
prodlužování	prodlužování	k1gNnSc1	prodlužování
ale	ale	k9	ale
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
probíhala	probíhat	k5eAaImAgFnS	probíhat
stavba	stavba	k1gFnSc1	stavba
tratí	trať	k1gFnPc2	trať
rychlodrážního	rychlodrážní	k2eAgInSc2d1	rychlodrážní
charakteru	charakter	k1gInSc2	charakter
do	do	k7c2	do
sídlišť	sídliště	k1gNnPc2	sídliště
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Bohunic	Bohunice	k1gFnPc2	Bohunice
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
do	do	k7c2	do
Bystrce	Bystrc	k1gFnSc2	Bystrc
<g/>
,	,	kIx,	,
do	do	k7c2	do
Líšně	Líšeň	k1gFnSc2	Líšeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
byly	být	k5eAaImAgFnP	být
dodávány	dodávat	k5eAaImNgInP	dodávat
nové	nový	k2eAgInPc1d1	nový
vozy	vůz	k1gInPc1	vůz
Tatra	Tatra	k1gFnSc1	Tatra
T3	T3	k1gFnSc3	T3
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
kloubové	kloubový	k2eAgFnSc2d1	kloubová
dvoučlánkové	dvoučlánkový	k2eAgFnSc2d1	dvoučlánková
tramvaje	tramvaj	k1gFnSc2	tramvaj
Tatra	Tatra	k1gFnSc1	Tatra
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
stále	stále	k6eAd1	stále
tvoří	tvořit	k5eAaImIp3nP	tvořit
pilíř	pilíř	k1gInSc4	pilíř
vozového	vozový	k2eAgInSc2d1	vozový
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
tramvaje	tramvaj	k1gFnSc2	tramvaj
Tatra	Tatra	k1gFnSc1	Tatra
T3	T3	k1gFnSc1	T3
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
modernizace	modernizace	k1gFnSc1	modernizace
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
T	T	kA	T
<g/>
6	[number]	k4	6
<g/>
A	a	k9	a
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
K2	K2	k1gFnSc1	K2
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
modernizace	modernizace	k1gFnSc1	modernizace
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
KT8D5	KT8D5	k1gFnSc1	KT8D5
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
modernizace	modernizace	k1gFnSc1	modernizace
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
13	[number]	k4	13
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
K	k	k7c3	k
<g/>
3	[number]	k4	3
<g/>
R-N	R-N	k1gFnSc2	R-N
<g/>
,	,	kIx,	,
VarioLF	VarioLF	k1gMnSc1	VarioLF
a	a	k8xC	a
VarioLF	VarioLF	k1gMnSc1	VarioLF
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
objevily	objevit	k5eAaPmAgInP	objevit
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
zprovozněno	zprovoznit	k5eAaPmNgNnS	zprovoznit
7	[number]	k4	7
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
označených	označený	k2eAgInPc2d1	označený
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
ztrátový	ztrátový	k2eAgInSc1d1	ztrátový
<g/>
.	.	kIx.	.
</s>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
ale	ale	k8xC	ale
až	až	k6eAd1	až
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
spíše	spíše	k9	spíše
doplňkový	doplňkový	k2eAgInSc4d1	doplňkový
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
však	však	k8xC	však
autobusy	autobus	k1gInPc1	autobus
nabyly	nabýt	k5eAaPmAgInP	nabýt
na	na	k7c6	na
významu	význam	k1gInSc6	význam
<g/>
.	.	kIx.	.
</s>
<s>
Zabezpečovaly	zabezpečovat	k5eAaImAgFnP	zabezpečovat
dopravu	doprava	k1gFnSc4	doprava
do	do	k7c2	do
okrajových	okrajový	k2eAgFnPc2d1	okrajová
částí	část	k1gFnPc2	část
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
do	do	k7c2	do
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
do	do	k7c2	do
líšeňského	líšeňský	k2eAgInSc2d1	líšeňský
Zetoru	zetor	k1gInSc2	zetor
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
měly	mít	k5eAaImAgFnP	mít
i	i	k8xC	i
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
velkých	velký	k2eAgInPc2d1	velký
sídlištních	sídlištní	k2eAgInPc2d1	sídlištní
celků	celek	k1gInPc2	celek
-	-	kIx~	-
jejich	jejich	k3xOp3gFnSc1	jejich
trasa	trasa	k1gFnSc1	trasa
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
měněna	měnit	k5eAaImNgFnS	měnit
spolu	spolu	k6eAd1	spolu
osídlováním	osídlování	k1gNnSc7	osídlování
<g/>
.	.	kIx.	.
</s>
<s>
DPMB	DPMB	kA	DPMB
provozuje	provozovat	k5eAaImIp3nS	provozovat
autobusy	autobus	k1gInPc4	autobus
typů	typ	k1gInPc2	typ
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
931	[number]	k4	931
<g/>
,	,	kIx,	,
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941	[number]	k4	941
<g/>
,	,	kIx,	,
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
961	[number]	k4	961
<g/>
,	,	kIx,	,
Irisbus	Irisbus	k1gInSc1	Irisbus
Citelis	Citelis	k1gFnSc1	Citelis
12	[number]	k4	12
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
Irisbus	Irisbus	k1gInSc1	Irisbus
Citelis	Citelis	k1gFnSc1	Citelis
18	[number]	k4	18
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
Irisbus	Irisbus	k1gMnSc1	Irisbus
Crossway	Crosswaa	k1gFnSc2	Crosswaa
LE	LE	kA	LE
a	a	k8xC	a
minibusy	minibus	k1gInPc1	minibus
Mave	Mav	k1gFnSc2	Mav
CiBus	CiBus	k1gMnSc1	CiBus
ENA	ENA	kA	ENA
a	a	k8xC	a
SKD	SKD	kA	SKD
Stratos	Stratos	k1gInSc1	Stratos
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
přibylo	přibýt	k5eAaPmAgNnS	přibýt
36	[number]	k4	36
ks	ks	kA	ks
vozů	vůz	k1gInPc2	vůz
Solaris	Solaris	k1gFnSc2	Solaris
Urbino	Urbino	k1gNnSc1	Urbino
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
a	a	k8xC	a
2015	[number]	k4	2015
také	také	k9	také
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
masivní	masivní	k2eAgFnSc1d1	masivní
modernizace	modernizace	k1gFnSc1	modernizace
vozového	vozový	k2eAgInSc2d1	vozový
parku	park	k1gInSc2	park
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
nákupem	nákup	k1gInSc7	nákup
100	[number]	k4	100
ks	ks	kA	ks
vozidel	vozidlo	k1gNnPc2	vozidlo
na	na	k7c4	na
stlačený	stlačený	k2eAgInSc4d1	stlačený
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
pořízeny	pořízen	k2eAgInPc4d1	pořízen
vozy	vůz	k1gInPc4	vůz
Irisbus	Irisbus	k1gMnSc1	Irisbus
Citelis	Citelis	k1gInSc4	Citelis
12M	[number]	k4	12M
CNG	CNG	kA	CNG
<g/>
,	,	kIx,	,
SOR	SOR	kA	SOR
NBG	NBG	kA	NBG
12	[number]	k4	12
a	a	k8xC	a
Iveco	Iveco	k1gMnSc1	Iveco
Urbanway	Urbanwaa	k1gFnSc2	Urbanwaa
12M	[number]	k4	12M
CNG	CNG	kA	CNG
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
tehdy	tehdy	k6eAd1	tehdy
moderní	moderní	k2eAgInSc1d1	moderní
trolejbus	trolejbus	k1gInSc1	trolejbus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
linka	linka	k1gFnSc1	linka
vedla	vést	k5eAaImAgFnS	vést
od	od	k7c2	od
Hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
do	do	k7c2	do
Slatiny	slatina	k1gFnSc2	slatina
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byly	být	k5eAaImAgFnP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
linky	linka	k1gFnPc1	linka
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
náměstí	náměstí	k1gNnSc1	náměstí
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
do	do	k7c2	do
Králova	Králův	k2eAgInSc2d1	Králův
Pole	pola	k1gFnSc3	pola
ulicí	ulice	k1gFnSc7	ulice
Botanickou	botanický	k2eAgFnSc7d1	botanická
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
od	od	k7c2	od
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
smyčky	smyčka	k1gFnSc2	smyčka
Komárově	komárově	k6eAd1	komárově
do	do	k7c2	do
Tuřan	Tuřany	k1gInPc2	Tuřany
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
23	[number]	k4	23
k	k	k7c3	k
tuřanské	tuřanský	k2eAgFnSc3d1	tuřanská
točně	točna	k1gFnSc3	točna
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
linka	linka	k1gFnSc1	linka
21	[number]	k4	21
do	do	k7c2	do
Slatiny	slatina	k1gFnSc2	slatina
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
až	až	k9	až
do	do	k7c2	do
Šlapanic	Šlapanice	k1gFnPc2	Šlapanice
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
bylo	být	k5eAaImAgNnS	být
jediné	jediný	k2eAgNnSc1d1	jediné
město	město	k1gNnSc1	město
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
budována	budován	k2eAgFnSc1d1	budována
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
síť	síť	k1gFnSc1	síť
jako	jako	k8xC	jako
doplněk	doplněk	k1gInSc1	doplněk
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
<g/>
.	.	kIx.	.
</s>
<s>
Trolejbusy	trolejbus	k1gInPc1	trolejbus
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
zavádět	zavádět	k5eAaImF	zavádět
do	do	k7c2	do
nově	nově	k6eAd1	nově
vystavěných	vystavěný	k2eAgNnPc2d1	vystavěné
sídlišť	sídliště	k1gNnPc2	sídliště
(	(	kIx(	(
<g/>
Kohoutovice	Kohoutovice	k1gFnPc1	Kohoutovice
<g/>
,	,	kIx,	,
Slatina	slatina	k1gFnSc1	slatina
<g/>
,	,	kIx,	,
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
,	,	kIx,	,
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vozech	vůz	k1gInPc6	vůz
Škoda	škoda	k6eAd1	škoda
6	[number]	k4	6
<g/>
Tr	Tr	k1gFnPc2	Tr
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
Tr	Tr	k1gFnPc2	Tr
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
Tr	Tr	k1gMnPc2	Tr
a	a	k8xC	a
9	[number]	k4	9
<g/>
Tr	Tr	k1gFnPc2	Tr
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
uvedeny	uvést	k5eAaPmNgInP	uvést
koncepčně	koncepčně	k6eAd1	koncepčně
odlišné	odlišný	k2eAgInPc1d1	odlišný
vozy	vůz	k1gInPc1	vůz
od	od	k7c2	od
stejného	stejné	k1gNnSc2	stejné
výrobce	výrobce	k1gMnSc2	výrobce
14	[number]	k4	14
<g/>
Tr	Tr	k1gFnSc2	Tr
a	a	k8xC	a
jim	on	k3xPp3gFnPc3	on
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
kloubová	kloubový	k2eAgFnSc1d1	kloubová
verze	verze	k1gFnSc1	verze
15	[number]	k4	15
<g/>
Tr	Tr	k1gFnPc2	Tr
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
jezdí	jezdit	k5eAaImIp3nP	jezdit
trolejbusy	trolejbus	k1gInPc1	trolejbus
Škoda	škoda	k6eAd1	škoda
21	[number]	k4	21
<g/>
Tr	Tr	k1gFnPc2	Tr
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
22	[number]	k4	22
<g/>
Tr	Tr	k1gFnSc1	Tr
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
25	[number]	k4	25
<g/>
Tr	Tr	k1gMnSc1	Tr
Irisbus	Irisbus	k1gMnSc1	Irisbus
a	a	k8xC	a
Škoda	Škoda	k1gMnSc1	Škoda
31	[number]	k4	31
<g/>
Tr	Tr	k1gFnSc2	Tr
SOR	SOR	kA	SOR
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
byli	být	k5eAaImAgMnP	být
v	v	k7c4	v
brněnské	brněnský	k2eAgNnSc4d1	brněnské
MHD	MHD	kA	MHD
(	(	kIx(	(
<g/>
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
trakcích	trakce	k1gFnPc6	trakce
<g/>
)	)	kIx)	)
zrušeni	zrušen	k2eAgMnPc1d1	zrušen
průvodčí	průvodčí	k1gMnPc1	průvodčí
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
samostatné	samostatný	k2eAgNnSc1d1	samostatné
odbavování	odbavování	k1gNnSc1	odbavování
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
DPMB	DPMB	kA	DPMB
provozuje	provozovat	k5eAaImIp3nS	provozovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
vlastní	vlastní	k2eAgFnSc4d1	vlastní
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
používá	používat	k5eAaImIp3nS	používat
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc1	pět
větších	veliký	k2eAgInPc2d2	veliký
dvoupalubových	dvoupalubový	k2eAgInPc2d1	dvoupalubový
(	(	kIx(	(
<g/>
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Utrecht	Utrecht	k1gInSc1	Utrecht
<g/>
,	,	kIx,	,
Dallas	Dallas	k1gInSc1	Dallas
a	a	k8xC	a
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
menší	malý	k2eAgFnSc4d2	menší
jednopalubovou	jednopalubový	k2eAgFnSc4d1	jednopalubový
loď	loď	k1gFnSc4	loď
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
jsou	být	k5eAaImIp3nP	být
poháněny	pohánět	k5eAaImNgFnP	pohánět
elektromotorem	elektromotor	k1gInSc7	elektromotor
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc1	loď
jezdí	jezdit	k5eAaImIp3nP	jezdit
každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
