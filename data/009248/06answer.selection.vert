<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Švédska	Švédsko	k1gNnSc2	Švédsko
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
modrým	modrý	k2eAgInSc7d1	modrý
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
se	s	k7c7	s
žlutým	žlutý	k2eAgInSc7d1	žlutý
<g/>
,	,	kIx,	,
skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc2	jenž
ramena	rameno	k1gNnSc2	rameno
mají	mít	k5eAaImIp3nP	mít
šířku	šířka	k1gFnSc4	šířka
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
na	na	k7c4	na
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
