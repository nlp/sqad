<s>
Kaple	kaple	k1gFnSc1	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
hrobkou	hrobka	k1gFnSc7	hrobka
rodiny	rodina	k1gFnSc2	rodina
Rüklů	Rükl	k1gMnPc2	Rükl
v	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Bradle	bradlo	k1gNnSc6	bradlo
(	(	kIx(	(
<g/>
části	část	k1gFnSc2	část
obce	obec	k1gFnSc2	obec
Horní	horní	k2eAgNnSc4d1	horní
Bradlo	bradlo	k1gNnSc4	bradlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sakrální	sakrální	k2eAgFnSc7d1	sakrální
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
pseudogotickém	pseudogotický	k2eAgInSc6d1	pseudogotický
slohu	sloh	k1gInSc6	sloh
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
