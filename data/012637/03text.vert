<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
hrobkou	hrobka	k1gFnSc7	hrobka
rodiny	rodina	k1gFnSc2	rodina
Rüklů	Rükl	k1gMnPc2	Rükl
v	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Bradle	bradlo	k1gNnSc6	bradlo
(	(	kIx(	(
<g/>
části	část	k1gFnSc2	část
obce	obec	k1gFnSc2	obec
Horní	horní	k2eAgNnSc4d1	horní
Bradlo	bradlo	k1gNnSc4	bradlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sakrální	sakrální	k2eAgFnSc7d1	sakrální
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
pseudogotickém	pseudogotický	k2eAgInSc6d1	pseudogotický
slohu	sloh	k1gInSc6	sloh
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
území	území	k1gNnSc6	území
farnosti	farnost	k1gFnSc2	farnost
Chotěboř	Chotěboř	k1gFnSc1	Chotěboř
v	v	k7c6	v
havlíčkobrodském	havlíčkobrodský	k2eAgInSc6d1	havlíčkobrodský
vikariátu	vikariát	k1gInSc6	vikariát
Královéhradecké	královéhradecký	k2eAgFnSc2d1	Královéhradecká
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
diecézním	diecézní	k2eAgInSc6d1	diecézní
schématismu	schématismus	k1gInSc6	schématismus
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
filiálního	filiální	k2eAgInSc2d1	filiální
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Výstavbu	výstavba	k1gFnSc4	výstavba
kaple	kaple	k1gFnSc2	kaple
svěřila	svěřit	k5eAaPmAgFnS	svěřit
paní	paní	k1gFnSc1	paní
Růžena	Růžena	k1gFnSc1	Růžena
Rüklová	Rüklový	k2eAgFnSc1d1	Rüklový
manželka	manželka	k1gFnSc1	manželka
majitele	majitel	k1gMnSc2	majitel
bradelské	bradelský	k2eAgFnPc1d1	bradelský
sklárny	sklárna	k1gFnPc1	sklárna
Františku	františek	k1gInSc2	františek
a	a	k8xC	a
Janu	Jan	k1gMnSc3	Jan
Schmoranzovým	Schmoranzův	k2eAgMnSc7d1	Schmoranzův
ze	z	k7c2	z
Slatiňan	Slatiňany	k1gInPc2	Slatiňany
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1894	[number]	k4	1894
byly	být	k5eAaImAgFnP	být
požehnány	požehnat	k5eAaPmNgInP	požehnat
zvony	zvon	k1gInPc1	zvon
pro	pro	k7c4	pro
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kaple	kaple	k1gFnSc2	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
ve	v	k7c6	v
schématismu	schématismus	k1gInSc6	schématismus
Královéhradecké	královéhradecký	k2eAgFnSc2d1	Královéhradecká
diecéze	diecéze	k1gFnSc2	diecéze
</s>
</p>
