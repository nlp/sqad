<s>
Supremum	Supremum	k1gNnSc1	Supremum
je	být	k5eAaImIp3nS	být
zaváděno	zavádět	k5eAaImNgNnS	zavádět
jako	jako	k8xS	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
pojmu	pojem	k1gInSc3	pojem
největší	veliký	k2eAgInSc1d3	veliký
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
největšímu	veliký	k2eAgInSc3d3	veliký
prvku	prvek	k1gInSc3	prvek
je	být	k5eAaImIp3nS	být
však	však	k9	však
dohledatelné	dohledatelný	k2eAgNnSc1d1	dohledatelné
u	u	k7c2	u
více	hodně	k6eAd2	hodně
množin	množina	k1gFnPc2	množina
–	–	k?	–
například	například	k6eAd1	například
omezené	omezený	k2eAgInPc1d1	omezený
otevřené	otevřený	k2eAgInPc1d1	otevřený
intervaly	interval	k1gInPc1	interval
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
nemají	mít	k5eNaImIp3nP	mít
největší	veliký	k2eAgInSc4d3	veliký
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
supremum	supremum	k1gInSc4	supremum
<g/>
.	.	kIx.	.
</s>
