<s>
Supremum	Supremum	k1gNnSc1	Supremum
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
spojení	spojení	k1gNnSc4	spojení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
matematický	matematický	k2eAgInSc1d1	matematický
pojem	pojem	k1gInSc1	pojem
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
teorie	teorie	k1gFnSc2	teorie
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
především	především	k9	především
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
vlastností	vlastnost	k1gFnPc2	vlastnost
reálných	reálný	k2eAgFnPc2d1	reálná
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Supremum	Supremum	k1gNnSc1	Supremum
je	být	k5eAaImIp3nS	být
zaváděno	zavádět	k5eAaImNgNnS	zavádět
jako	jako	k8xS	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
pojmu	pojem	k1gInSc3	pojem
největší	veliký	k2eAgInSc1d3	veliký
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
největšímu	veliký	k2eAgInSc3d3	veliký
prvku	prvek	k1gInSc3	prvek
je	být	k5eAaImIp3nS	být
však	však	k9	však
dohledatelné	dohledatelný	k2eAgNnSc1d1	dohledatelné
u	u	k7c2	u
více	hodně	k6eAd2	hodně
množin	množina	k1gFnPc2	množina
–	–	k?	–
například	například	k6eAd1	například
omezené	omezený	k2eAgInPc1d1	omezený
otevřené	otevřený	k2eAgInPc1d1	otevřený
intervaly	interval	k1gInPc1	interval
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
nemají	mít	k5eNaImIp3nP	mít
největší	veliký	k2eAgInSc4d3	veliký
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
supremum	supremum	k1gInSc4	supremum
<g/>
.	.	kIx.	.
</s>
<s>
Duálním	duální	k2eAgInSc7d1	duální
pojmem	pojem	k1gInSc7	pojem
(	(	kIx(	(
<g/>
opakem	opak	k1gInSc7	opak
<g/>
)	)	kIx)	)
suprema	suprema	k1gFnSc1	suprema
je	být	k5eAaImIp3nS	být
infimum	infimum	k1gInSc4	infimum
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
relací	relace	k1gFnSc7	relace
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
prvku	prvek	k1gInSc6	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
∈	∈	k?	∈
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
X	X	kA	X
<g/>
}	}	kIx)	}
řekneme	říct	k5eAaPmIp1nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
supremum	supremum	k1gNnSc1	supremum
podmnožiny	podmnožina	k1gFnSc2	podmnožina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
⊆	⊆	k?	⊆
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
X	X	kA	X
<g/>
}	}	kIx)	}
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejmenší	malý	k2eAgInSc1d3	nejmenší
prvek	prvek	k1gInSc1	prvek
množiny	množina	k1gFnSc2	množina
všech	všecek	k3xTgFnPc2	všecek
horních	horní	k2eAgFnPc2d1	horní
závor	závora	k1gFnPc2	závora
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
=	=	kIx~	=
:	:	kIx,	:
sup	sup	k1gMnSc1	sup
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
Y	Y	kA	Y
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sup	sup	k1gMnSc1	sup
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Supremum	Supremum	k1gNnSc1	Supremum
má	mít	k5eAaImIp3nS	mít
každá	každý	k3xTgFnSc1	každý
shora	shora	k6eAd1	shora
omezená	omezený	k2eAgFnSc1d1	omezená
množina	množina	k1gFnSc1	množina
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
maximum	maximum	k1gNnSc4	maximum
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
prvek	prvek	k1gInSc1	prvek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
otevřený	otevřený	k2eAgInSc4d1	otevřený
interval	interval	k1gInSc4	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
=	=	kIx~	=
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
maximum	maximum	k1gNnSc1	maximum
nemá	mít	k5eNaImIp3nS	mít
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
∈	∈	k?	∈
I	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
I	i	k9	i
<g/>
}	}	kIx)	}
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
c	c	k0	c
<	<	kIx(	<
d	d	k?	d
<	<	kIx(	<
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
:	:	kIx,	:
<g/>
c	c	k0	c
<g/>
<	<	kIx(	<
<g/>
d	d	k?	d
<g/>
<	<	kIx(	<
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gInSc7	jeho
supremem	suprem	k1gInSc7	suprem
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejmenší	malý	k2eAgFnSc4d3	nejmenší
horní	horní	k2eAgFnSc4d1	horní
závoru	závora	k1gFnSc4	závora
a	a	k8xC	a
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
větší	veliký	k2eAgNnSc4d2	veliký
číslo	číslo	k1gNnSc4	číslo
již	již	k6eAd1	již
nejmenší	malý	k2eAgFnSc7d3	nejmenší
horní	horní	k2eAgFnSc7d1	horní
závorou	závora	k1gFnSc7	závora
není	být	k5eNaImIp3nS	být
–	–	k?	–
lze	lze	k6eAd1	lze
argumentovat	argumentovat	k5eAaImF	argumentovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
maxima	maximum	k1gNnSc2	maximum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Shora	shora	k6eAd1	shora
neomezené	omezený	k2eNgFnPc1d1	neomezená
množiny	množina	k1gFnPc1	množina
supremum	supremum	k1gInSc1	supremum
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
otevřený	otevřený	k2eAgInSc4d1	otevřený
interval	interval	k1gInSc4	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
=	=	kIx~	=
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
+	+	kIx~	+
∞	∞	k?	∞
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
)	)	kIx)	)
<g/>
}	}	kIx)	}
nemá	mít	k5eNaImIp3nS	mít
supremum	supremum	k1gInSc4	supremum
v	v	k7c6	v
množině	množina	k1gFnSc6	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
všech	všecek	k3xTgNnPc2	všecek
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
množina	množina	k1gFnSc1	množina
maximum	maximum	k1gNnSc4	maximum
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
supremum	supremum	k1gInSc1	supremum
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
}	}	kIx)	}
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
=	=	kIx~	=
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obecně	obecně	k6eAd1	obecně
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
množinách	množina	k1gFnPc6	množina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
supremum	supremum	k1gInSc1	supremum
zobecněním	zobecnění	k1gNnSc7	zobecnění
pojmu	pojem	k1gInSc2	pojem
největšího	veliký	k2eAgInSc2d3	veliký
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
množina	množina	k1gFnSc1	množina
největší	veliký	k2eAgFnSc1d3	veliký
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
největší	veliký	k2eAgInSc1d3	veliký
prvek	prvek	k1gInSc1	prvek
zároveň	zároveň	k6eAd1	zároveň
jejím	její	k3xOp3gInSc7	její
supremem	suprem	k1gInSc7	suprem
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
to	ten	k3xDgNnSc1	ten
však	však	k9	však
platit	platit	k5eAaImF	platit
nemusí	muset	k5eNaImIp3nP	muset
–	–	k?	–
prvním	první	k4xOgMnSc6	první
takovým	takový	k3xDgInSc7	takový
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
výše	vysoce	k6eAd2	vysoce
uvedený	uvedený	k2eAgInSc4d1	uvedený
shora	shora	k6eAd1	shora
omezený	omezený	k2eAgInSc4d1	omezený
otevřený	otevřený	k2eAgInSc4d1	otevřený
interval	interval	k1gInSc4	interval
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
supremum	supremum	k1gInSc1	supremum
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
jednoznačně	jednoznačně	k6eAd1	jednoznačně
–	–	k?	–
množina	množina	k1gFnSc1	množina
nemůže	moct	k5eNaImIp3nS	moct
mít	mít	k5eAaImF	mít
dvě	dva	k4xCgNnPc4	dva
různá	různý	k2eAgNnPc4d1	různé
suprema	supremum	k1gNnPc4	supremum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejmenší	malý	k2eAgInSc4d3	nejmenší
prvek	prvek	k1gInSc4	prvek
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
nejmenší	malý	k2eAgInSc4d3	nejmenší
prvek	prvek	k1gInSc4	prvek
množiny	množina	k1gFnSc2	množina
horních	horní	k2eAgFnPc2d1	horní
závor	závora	k1gFnPc2	závora
–	–	k?	–
supremum	supremum	k1gNnSc4	supremum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určen	určen	k2eAgInSc1d1	určen
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
o	o	k7c6	o
množině	množina	k1gFnSc6	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
všech	všecek	k3xTgNnPc2	všecek
kladných	kladný	k2eAgNnPc2d1	kladné
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
relaci	relace	k1gFnSc6	relace
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
}	}	kIx)	}
danou	daný	k2eAgFnSc4d1	daná
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
⇔	⇔	k?	⇔
a	a	k8xC	a
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
Leftrightarrow	Leftrightarrow	k1gMnSc1	Leftrightarrow
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
|	|	kIx~	|
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
je	on	k3xPp3gFnPc4	on
menší	malý	k2eAgFnPc4d2	menší
nebo	nebo	k8xC	nebo
rovné	rovný	k2eAgFnPc4d1	rovná
číslu	číslo	k1gNnSc3	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
podle	podle	k7c2	podle
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
}	}	kIx)	}
,	,	kIx,	,
pokud	pokud	k8xS	pokud
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
dělí	dělit	k5eAaImIp3nP	dělit
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
konečná	konečný	k2eAgFnSc1d1	konečná
podmnožina	podmnožina	k1gFnSc1	podmnožina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
má	mít	k5eAaImIp3nS	mít
supremum	supremum	k1gInSc4	supremum
<g/>
.	.	kIx.	.
</s>
<s>
Supremem	Suprem	k1gInSc7	Suprem
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
nejmenší	malý	k2eAgInSc4d3	nejmenší
společný	společný	k2eAgInSc4d1	společný
násobek	násobek	k1gInSc4	násobek
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
množina	množina	k1gFnSc1	množina
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
největší	veliký	k2eAgInSc1d3	veliký
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
4	[number]	k4	4
,	,	kIx,	,
6	[number]	k4	6
,	,	kIx,	,
8	[number]	k4	8
}	}	kIx)	}
⊆	⊆	k?	⊆
:	:	kIx,	:
:	:	kIx,	:
Z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
4,6	[number]	k4	4,6
<g/>
,8	,8	k4	,8
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
subseteq	subseteq	k?	subseteq
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
nemá	mít	k5eNaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neplatí	platit	k5eNaImIp3nS	platit
ani	ani	k8xC	ani
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
8	[number]	k4	8
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
8	[number]	k4	8
<g/>
}	}	kIx)	}
,	,	kIx,	,
ani	ani	k8xC	ani
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
8	[number]	k4	8
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
8	[number]	k4	8
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
ale	ale	k9	ale
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sup	sup	k1gMnSc1	sup
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
4	[number]	k4	4
,	,	kIx,	,
6	[number]	k4	6
,	,	kIx,	,
8	[number]	k4	8
}	}	kIx)	}
=	=	kIx~	=
24	[number]	k4	24
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sup	sup	k1gMnSc1	sup
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
4,6	[number]	k4	4,6
<g/>
,8	,8	k4	,8
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
24	[number]	k4	24
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
každá	každý	k3xTgFnSc1	každý
shora	shora	k6eAd1	shora
omezená	omezený	k2eAgFnSc1d1	omezená
množina	množina	k1gFnSc1	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
supremum	supremum	k1gInSc1	supremum
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
}	}	kIx)	}
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
množině	množina	k1gFnSc3	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
hodně	hodně	k6eAd1	hodně
podobná	podobný	k2eAgFnSc1d1	podobná
–	–	k?	–
je	být	k5eAaImIp3nS	být
také	také	k9	také
hustě	hustě	k6eAd1	hustě
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
shora	shora	k6eAd1	shora
omezené	omezený	k2eAgFnPc1d1	omezená
množiny	množina	k1gFnPc1	množina
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
(	(	kIx(	(
<g/>
v	v	k7c6	v
množině	množina	k1gFnSc6	množina
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
supremum	supremum	k1gInSc1	supremum
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takové	takový	k3xDgFnSc2	takový
množiny	množina	k1gFnSc2	množina
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
x	x	k?	x
∈	∈	k?	∈
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
2	[number]	k4	2
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
:	:	kIx,	:
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
množině	množina	k1gFnSc6	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
}	}	kIx)	}
nemá	mít	k5eNaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
množina	množina	k1gFnSc1	množina
supremum	supremum	k1gInSc4	supremum
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
o	o	k7c6	o
supremu	suprem	k1gInSc6	suprem
této	tento	k3xDgFnSc2	tento
množiny	množina	k1gFnSc2	množina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
všech	všecek	k3xTgNnPc2	všecek
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
něco	něco	k3yInSc4	něco
lépe	dobře	k6eAd2	dobře
–	–	k?	–
supremem	suprem	k1gInSc7	suprem
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
odmocnina	odmocnina	k1gFnSc1	odmocnina
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
o	o	k7c6	o
třídě	třída	k1gFnSc6	třída
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
O	O	kA	O
<g/>
}	}	kIx)	}
n	n	k0	n
<g/>
}	}	kIx)	}
všech	všecek	k3xTgNnPc2	všecek
ordinálních	ordinální	k2eAgNnPc2d1	ordinální
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Ordinální	ordinální	k2eAgNnPc1d1	ordinální
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
–	–	k?	–
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
podmnožina	podmnožina	k1gFnSc1	podmnožina
má	mít	k5eAaImIp3nS	mít
nejmenší	malý	k2eAgInSc4d3	nejmenší
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k9	i
infimum	infimum	k1gInSc4	infimum
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavější	zajímavý	k2eAgMnSc1d2	zajímavější
a	a	k8xC	a
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
ne	ne	k9	ne
tak	tak	k6eAd1	tak
zjevné	zjevný	k2eAgNnSc1d1	zjevné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
shora	shora	k6eAd1	shora
omezená	omezený	k2eAgFnSc1d1	omezená
podtřída	podtřída	k1gFnSc1	podtřída
třídy	třída	k1gFnSc2	třída
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
O	O	kA	O
<g/>
}	}	kIx)	}
n	n	k0	n
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
shora	shora	k6eAd1	shora
omezená	omezený	k2eAgFnSc1d1	omezená
třída	třída	k1gFnSc1	třída
ordinálních	ordinální	k2eAgNnPc2d1	ordinální
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
množina	množina	k1gFnSc1	množina
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
supremum	supremum	k1gInSc4	supremum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
největší	veliký	k2eAgInSc4d3	veliký
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
množina	množina	k1gFnSc1	množina
konečných	konečný	k2eAgNnPc2d1	konečné
ordinálních	ordinální	k2eAgNnPc2d1	ordinální
čísel	číslo	k1gNnPc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
2	[number]	k4	2
,	,	kIx,	,
...	...	k?	...
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
,2	,2	k4	,2
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k6eAd1	ldots
\	\	kIx~	\
<g/>
}}	}}	k?	}}
nemá	mít	k5eNaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sup	sup	k1gMnSc1	sup
{	{	kIx(	{
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
2	[number]	k4	2
,	,	kIx,	,
...	...	k?	...
}	}	kIx)	}
=	=	kIx~	=
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sup	sup	k1gMnSc1	sup
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
,2	,2	k4	,2
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k6eAd1	ldots
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
prostor	prostor	k1gInSc4	prostor
s	s	k7c7	s
mírou	míra	k1gFnSc7	míra
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
X	X	kA	X
,	,	kIx,	,
Σ	Σ	k?	Σ
,	,	kIx,	,
μ	μ	k?	μ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
Sigma	sigma	k1gNnSc2	sigma
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
-měřitelnou	ěřitelný	k2eAgFnSc4d1	-měřitelný
reálnou	reálný	k2eAgFnSc4d1	reálná
funkci	funkce	k1gFnSc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
f	f	k?	f
<g/>
}	}	kIx)	}
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
X	X	kA	X
<g/>
}	}	kIx)	}
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
X	X	kA	X
→	→	k?	→
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
f	f	k?	f
<g/>
:	:	kIx,	:
<g/>
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Esenciální	esenciální	k2eAgInSc4d1	esenciální
supremum	supremum	k1gInSc4	supremum
funkce	funkce	k1gFnSc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
f	f	k?	f
<g/>
}	}	kIx)	}
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
X	X	kA	X
<g/>
}	}	kIx)	}
pak	pak	k6eAd1	pak
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
ess	ess	k?	ess
sup	sup	k1gMnSc1	sup
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
ess	ess	k?	ess
sup	sup	k1gMnSc1	sup
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
a	a	k8xC	a
definujeme	definovat	k5eAaBmIp1nP	definovat
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
ess	ess	k?	ess
sup	sup	k1gMnSc1	sup
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
=	=	kIx~	=
inf	inf	k?	inf
{	{	kIx(	{
C	C	kA	C
∈	∈	k?	∈
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
≤	≤	k?	≤
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
-skoro	kora	k1gFnSc5	-skora
všechna	všechen	k3xTgNnPc4	všechen
:	:	kIx,	:
x	x	k?	x
∈	∈	k?	∈
X	X	kA	X
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
ess	ess	k?	ess
sup	sup	k1gMnSc1	sup
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
inf	inf	k?	inf
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
|	|	kIx~	|
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
C	C	kA	C
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
pro	pro	k7c4	pro
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
-skoro	kora	k1gFnSc5	-skora
všechna	všechen	k3xTgNnPc4	všechen
}}	}}	k?	}}
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
X	X	kA	X
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Esenciální	esenciální	k2eAgInSc1d1	esenciální
supremum	supremum	k1gInSc1	supremum
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
infimum	infimum	k1gInSc4	infimum
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
čísel	číslo	k1gNnPc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
c	c	k0	c
<g/>
}	}	kIx)	}
takových	takový	k3xDgInPc6	takový
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
když	když	k8xS	když
vezmeme	vzít	k5eAaPmIp1nP	vzít
množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgFnPc2	všecek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
∈	∈	k?	∈
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
X	X	kA	X
<g/>
}	}	kIx)	}
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
nabývá	nabývat	k5eAaImIp3nS	nabývat
funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
f	f	k?	f
<g/>
}	}	kIx)	}
hodnoty	hodnota	k1gFnPc4	hodnota
větší	veliký	k2eAgFnPc4d2	veliký
než	než	k8xS	než
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
c	c	k0	c
<g/>
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k8xC	tak
tato	tento	k3xDgFnSc1	tento
množina	množina	k1gFnSc1	množina
bude	být	k5eAaImBp3nS	být
míry	míra	k1gFnSc2	míra
nula	nula	k1gFnSc1	nula
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Infimum	Infimum	k1gInSc1	Infimum
Největší	veliký	k2eAgInSc4d3	veliký
prvek	prvek	k1gInSc4	prvek
Maximální	maximální	k2eAgInSc4d1	maximální
prvek	prvek	k1gInSc4	prvek
Dedekindův	Dedekindův	k2eAgInSc1d1	Dedekindův
řez	řez	k1gInSc1	řez
Svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
matematika	matematika	k1gFnSc1	matematika
<g/>
)	)	kIx)	)
Reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
Ordinální	ordinální	k2eAgNnPc1d1	ordinální
čísla	číslo	k1gNnPc1	číslo
</s>
