<s>
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc7d1	národní
barvou	barva	k1gFnSc7	barva
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tamější	tamější	k2eAgFnSc1d1	tamější
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
dynastie	dynastie	k1gFnSc1	dynastie
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
počátky	počátek	k1gInPc4	počátek
v	v	k7c6	v
nasavsko-oranžském	nasavskoranžský	k2eAgInSc6d1	nasavsko-oranžský
rodu	rod	k1gInSc6	rod
s	s	k7c7	s
původním	původní	k2eAgNnSc7d1	původní
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
městě	město	k1gNnSc6	město
Orange	Orang	k1gFnSc2	Orang
<g/>
.	.	kIx.	.
</s>
