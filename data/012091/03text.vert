<p>
<s>
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
spektru	spektrum	k1gNnSc6	spektrum
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c4	mezi
červenou	červený	k2eAgFnSc4d1	červená
a	a	k8xC	a
žlutou	žlutý	k2eAgFnSc4d1	žlutá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
přibližně	přibližně	k6eAd1	přibližně
585	[number]	k4	585
<g/>
–	–	k?	–
<g/>
620	[number]	k4	620
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
subtraktivním	subtraktivní	k2eAgNnSc6d1	subtraktivní
míchání	míchání	k1gNnSc6	míchání
barev	barva	k1gFnPc2	barva
oranžová	oranžový	k2eAgFnSc1d1	oranžová
vzniká	vznikat	k5eAaImIp3nS	vznikat
smícháním	smíchání	k1gNnSc7	smíchání
žluté	žlutý	k2eAgFnSc2d1	žlutá
s	s	k7c7	s
magentou	magenta	k1gFnSc7	magenta
(	(	kIx(	(
<g/>
purpurovou	purpurový	k2eAgFnSc7d1	purpurová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
francouzského	francouzský	k2eAgInSc2d1	francouzský
orange	orang	k1gInSc2	orang
<g/>
,	,	kIx,	,
pomeranč	pomeranč	k1gInSc1	pomeranč
<g/>
,	,	kIx,	,
převzatého	převzatý	k2eAgMnSc4d1	převzatý
z	z	k7c2	z
arabštiny	arabština	k1gFnSc2	arabština
(	(	kIx(	(
<g/>
narandž	narandž	k6eAd1	narandž
<g/>
)	)	kIx)	)
východního	východní	k2eAgInSc2d1	východní
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
Persie	Persie	k1gFnSc1	Persie
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
a	a	k8xC	a
symbolika	symbolika	k1gFnSc1	symbolika
oranžové	oranžový	k2eAgFnSc2d1	oranžová
barvy	barva	k1gFnSc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
varování	varování	k1gNnSc1	varování
či	či	k8xC	či
zvýraznění	zvýraznění	k1gNnSc1	zvýraznění
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc7d1	národní
barvou	barva	k1gFnSc7	barva
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tamější	tamější	k2eAgFnSc1d1	tamější
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
dynastie	dynastie	k1gFnSc1	dynastie
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
počátky	počátek	k1gInPc4	počátek
v	v	k7c6	v
nasavsko-oranžském	nasavskoranžský	k2eAgInSc6d1	nasavsko-oranžský
rodu	rod	k1gInSc6	rod
s	s	k7c7	s
původním	původní	k2eAgNnSc7d1	původní
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
městě	město	k1gNnSc6	město
Orange	Orang	k1gFnSc2	Orang
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nizozemská	nizozemský	k2eAgNnPc1d1	Nizozemské
národní	národní	k2eAgNnPc1d1	národní
mužstva	mužstvo	k1gNnPc1	mužstvo
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
fotbalové	fotbalový	k2eAgNnSc1d1	fotbalové
<g/>
)	)	kIx)	)
používají	používat	k5eAaImIp3nP	používat
oranžové	oranžový	k2eAgInPc4d1	oranžový
dresy	dres	k1gInPc4	dres
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
přezdívána	přezdíván	k2eAgFnSc1d1	přezdívána
Oranje	Oranje	k1gFnSc1	Oranje
–	–	k?	–
nizozemsky	nizozemsky	k6eAd1	nizozemsky
"	"	kIx"	"
<g/>
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
se	se	k3xPyFc4	se
oranžovou	oranžový	k2eAgFnSc7d1	oranžová
barvou	barva	k1gFnSc7	barva
označují	označovat	k5eAaImIp3nP	označovat
protestanté	protestant	k1gMnPc1	protestant
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Oranžský	oranžský	k2eAgInSc1d1	oranžský
řád	řád	k1gInSc1	řád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
barva	barva	k1gFnSc1	barva
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
symbolem	symbol	k1gInSc7	symbol
tzv.	tzv.	kA	tzv.
oranžové	oranžový	k2eAgFnSc2d1	oranžová
revoluce	revoluce	k1gFnSc2	revoluce
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
oranžová	oranžový	k2eAgFnSc1d1	oranžová
barva	barva	k1gFnSc1	barva
označuje	označovat	k5eAaImIp3nS	označovat
hinduismus	hinduismus	k1gInSc4	hinduismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oranžovou	oranžový	k2eAgFnSc4d1	oranžová
barvu	barva	k1gFnSc4	barva
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
symbol	symbol	k1gInSc4	symbol
židovští	židovský	k2eAgMnPc1d1	židovský
osadníci	osadník	k1gMnPc1	osadník
v	v	k7c6	v
Pásmu	pásmo	k1gNnSc6	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
značení	značení	k1gNnSc6	značení
odporů	odpor	k1gInPc2	odpor
znamená	znamenat	k5eAaImIp3nS	znamenat
oranžová	oranžový	k2eAgFnSc1d1	oranžová
barva	barva	k1gFnSc1	barva
číslici	číslice	k1gFnSc4	číslice
3	[number]	k4	3
</s>
</p>
<p>
<s>
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
barvou	barva	k1gFnSc7	barva
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
oranžová	oranžový	k2eAgFnSc1d1	oranžová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
oranžový	oranžový	k2eAgMnSc1d1	oranžový
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
