<s>
Milan	Milan	k1gMnSc1	Milan
Kundera	Kundero	k1gNnSc2	Kundero
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1929	[number]	k4	1929
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
texty	text	k1gInPc4	text
psal	psát	k5eAaImAgInS	psát
nejdříve	dříve	k6eAd3	dříve
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byl	být	k5eAaImAgMnS	být
zbaven	zbavit	k5eAaPmNgMnS	zbavit
československého	československý	k2eAgNnSc2d1	Československé
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
až	až	k9	až
do	do	k7c2	do
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
zakázáno	zakázán	k2eAgNnSc4d1	zakázáno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
dramat	drama	k1gNnPc2	drama
<g/>
,	,	kIx,	,
prózy	próza	k1gFnSc2	próza
a	a	k8xC	a
esejí	esej	k1gFnPc2	esej
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
texty	text	k1gInPc1	text
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejčastěji	často	k6eAd3	často
překládaná	překládaný	k2eAgNnPc4d1	překládané
česká	český	k2eAgNnPc4d1	české
díla	dílo	k1gNnPc4	dílo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Kundera	Kundero	k1gNnSc2	Kundero
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
v	v	k7c6	v
Purkyňově	Purkyňův	k2eAgFnSc6d1	Purkyňova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Kundera	Kundero	k1gNnSc2	Kundero
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
klavírista	klavírista	k1gMnSc1	klavírista
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
JAMU	jam	k1gInSc2	jam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
se	se	k3xPyFc4	se
Milan	Milan	k1gMnSc1	Milan
Kundera	Kunder	k1gMnSc4	Kunder
učil	učít	k5eAaPmAgMnS	učít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
prominentům	prominent	k1gMnPc3	prominent
oficiální	oficiální	k2eAgFnSc2d1	oficiální
literatury	literatura	k1gFnSc2	literatura
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
stalinismu	stalinismus	k1gInSc2	stalinismus
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
příslušníkem	příslušník	k1gMnSc7	příslušník
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
literárního	literární	k2eAgInSc2d1	literární
života	život	k1gInSc2	život
brzy	brzy	k6eAd1	brzy
po	po	k7c4	po
uchopení	uchopení	k1gNnSc4	uchopení
moci	moct	k5eAaImF	moct
komunisty	komunista	k1gMnPc7	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
maturoval	maturovat	k5eAaBmAgMnS	maturovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
UK	UK	kA	UK
literární	literární	k2eAgFnSc4d1	literární
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
estetiku	estetika	k1gFnSc4	estetika
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
semestrech	semestr	k1gInPc6	semestr
však	však	k9	však
přestoupil	přestoupit	k5eAaPmAgInS	přestoupit
na	na	k7c6	na
FAMU	FAMU	kA	FAMU
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zprvu	zprvu	k6eAd1	zprvu
studoval	studovat	k5eAaImAgMnS	studovat
filmovou	filmový	k2eAgFnSc4d1	filmová
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
poté	poté	k6eAd1	poté
scenáristiku	scenáristika	k1gFnSc4	scenáristika
u	u	k7c2	u
M.	M.	kA	M.
<g/>
V.	V.	kA	V.
Kratochvíla	Kratochvíl	k1gMnSc2	Kratochvíl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
studia	studio	k1gNnSc2	studio
začal	začít	k5eAaPmAgInS	začít
na	na	k7c6	na
FAMU	FAMU	kA	FAMU
vyučovat	vyučovat	k5eAaImF	vyučovat
světovou	světový	k2eAgFnSc4d1	světová
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
jako	jako	k8xC	jako
odborný	odborný	k2eAgMnSc1d1	odborný
asistent	asistent	k1gMnSc1	asistent
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
již	již	k6eAd1	již
jako	jako	k9	jako
docent	docent	k1gMnSc1	docent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
však	však	k9	však
vyloučen	vyloučen	k2eAgMnSc1d1	vyloučen
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
mu	on	k3xPp3gNnSc3	on
bylo	být	k5eAaImAgNnS	být
členství	členství	k1gNnSc1	členství
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgInS	stát
významným	významný	k2eAgMnSc7d1	významný
aktérem	aktér	k1gMnSc7	aktér
snah	snaha	k1gFnPc2	snaha
o	o	k7c6	o
liberalizaci	liberalizace	k1gFnSc6	liberalizace
kulturních	kulturní	k2eAgInPc2d1	kulturní
poměrů	poměr	k1gInPc2	poměr
z	z	k7c2	z
reformně	reformně	k6eAd1	reformně
komunistických	komunistický	k2eAgFnPc2d1	komunistická
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
byla	být	k5eAaImAgFnS	být
Olga	Olga	k1gFnSc1	Olga
Haasová	Haasová	k1gFnSc1	Haasová
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
skladatele	skladatel	k1gMnSc2	skladatel
Pavla	Pavel	k1gMnSc2	Pavel
Haase	Haas	k1gMnSc2	Haas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
IV	IV	kA	IV
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
Svazu	svaz	k1gInSc2	svaz
československých	československý	k2eAgMnPc2d1	československý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
s	s	k7c7	s
úvodním	úvodní	k2eAgInSc7d1	úvodní
referátem	referát	k1gInSc7	referát
Nesamozřejmost	Nesamozřejmost	k1gFnSc1	Nesamozřejmost
existence	existence	k1gFnSc1	existence
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
mezi	mezi	k7c7	mezi
jiným	jiný	k2eAgMnSc7d1	jiný
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
pokus	pokus	k1gInSc1	pokus
zakázat	zakázat	k5eAaPmF	zakázat
filmy	film	k1gInPc4	film
Věry	Věra	k1gFnSc2	Věra
Chytilové	Chytilová	k1gFnSc2	Chytilová
a	a	k8xC	a
osud	osud	k1gInSc1	osud
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
životně	životně	k6eAd1	životně
odvislý	odvislý	k2eAgInSc4d1	odvislý
na	na	k7c6	na
míře	míra	k1gFnSc6	míra
duchovní	duchovní	k2eAgFnSc2d1	duchovní
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
srpnové	srpnový	k2eAgFnSc6d1	srpnová
intervenci	intervence	k1gFnSc6	intervence
zahájil	zahájit	k5eAaPmAgMnS	zahájit
svým	svůj	k3xOyFgInSc7	svůj
článkem	článek	k1gInSc7	článek
Český	český	k2eAgInSc1d1	český
úděl	úděl	k1gInSc1	úděl
v	v	k7c6	v
týdeníku	týdeník	k1gInSc6	týdeník
Listy	lista	k1gFnSc2	lista
diskusi	diskuse	k1gFnSc4	diskuse
o	o	k7c6	o
českém	český	k2eAgInSc6d1	český
údělu	úděl	k1gInSc6	úděl
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Tvář	tvář	k1gFnSc1	tvář
(	(	kIx(	(
<g/>
č.	č.	k?	č.
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diskuse	diskuse	k1gFnSc1	diskuse
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
Plamen	plamen	k1gInSc1	plamen
a	a	k8xC	a
Host	host	k1gMnSc1	host
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Kundera	Kundera	k1gFnSc1	Kundera
až	až	k6eAd1	až
na	na	k7c4	na
ojedinělou	ojedinělý	k2eAgFnSc4d1	ojedinělá
výjimku	výjimka	k1gFnSc4	výjimka
(	(	kIx(	(
<g/>
esej	esej	k1gFnSc1	esej
Únos	únos	k1gInSc1	únos
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
exilovém	exilový	k2eAgInSc6d1	exilový
časopise	časopis	k1gInSc6	časopis
150	[number]	k4	150
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
k	k	k7c3	k
politickým	politický	k2eAgFnPc3d1	politická
otázkám	otázka	k1gFnPc3	otázka
přímo	přímo	k6eAd1	přímo
nevyjadřuje	vyjadřovat	k5eNaImIp3nS	vyjadřovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zprvu	zprvu	k6eAd1	zprvu
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Rennes	Rennes	k1gInSc4	Rennes
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
knihy	kniha	k1gFnSc2	kniha
Kniha	kniha	k1gFnSc1	kniha
smíchu	smích	k1gInSc2	smích
a	a	k8xC	a
zapomnění	zapomnění	k1gNnSc2	zapomnění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
odebráno	odebrat	k5eAaPmNgNnS	odebrat
československé	československý	k2eAgNnSc1d1	Československé
státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
získal	získat	k5eAaPmAgInS	získat
občanství	občanství	k1gNnSc4	občanství
francouzské	francouzský	k2eAgNnSc4d1	francouzské
<g/>
.	.	kIx.	.
</s>
<s>
Udržuje	udržovat	k5eAaImIp3nS	udržovat
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
okruhem	okruh	k1gInSc7	okruh
přátel	přítel	k1gMnPc2	přítel
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zřídka	zřídka	k6eAd1	zřídka
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
inkognito	inkognito	k6eAd1	inkognito
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
reviduje	revidovat	k5eAaImIp3nS	revidovat
překlady	překlad	k1gInPc4	překlad
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
napsaných	napsaný	k2eAgNnPc2d1	napsané
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
české	český	k2eAgInPc4d1	český
překlady	překlad	k1gInPc4	překlad
téměř	téměř	k6eAd1	téměř
nevydává	vydávat	k5eNaImIp3nS	vydávat
a	a	k8xC	a
odmítá	odmítat	k5eAaImIp3nS	odmítat
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
svěřit	svěřit	k5eAaPmF	svěřit
jiným	jiný	k2eAgMnPc3d1	jiný
překladatelům	překladatel	k1gMnPc3	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zatčení	zatčení	k1gNnSc2	zatčení
Miroslava	Miroslav	k1gMnSc2	Miroslav
Dvořáčka	Dvořáček	k1gMnSc2	Dvořáček
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Respekt	respekt	k1gInSc1	respekt
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
přinesl	přinést	k5eAaPmAgInS	přinést
článek	článek	k1gInSc1	článek
Adama	Adam	k1gMnSc2	Adam
Hradilka	Hradilka	k1gFnSc1	Hradilka
z	z	k7c2	z
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
<g/>
,	,	kIx,	,
uvádějící	uvádějící	k2eAgMnSc1d1	uvádějící
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Hradilkem	Hradilko	k1gNnSc7	Hradilko
objeveného	objevený	k2eAgInSc2d1	objevený
policejního	policejní	k2eAgInSc2d1	policejní
záznamu	záznam	k1gInSc2	záznam
<g/>
,	,	kIx,	,
že	že	k8xS	že
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1950	[number]	k4	1950
se	se	k3xPyFc4	se
Kundera	Kundero	k1gNnSc2	Kundero
dostavil	dostavit	k5eAaPmAgMnS	dostavit
na	na	k7c4	na
policii	policie	k1gFnSc4	policie
a	a	k8xC	a
udal	udat	k5eAaPmAgMnS	udat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nějaký	nějaký	k3yIgInSc4	nějaký
muž	muž	k1gMnSc1	muž
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
úschově	úschova	k1gFnSc6	úschova
kufr	kufr	k1gInSc4	kufr
u	u	k7c2	u
jedné	jeden	k4xCgFnSc2	jeden
Kunderovy	Kunderův	k2eAgFnSc2d1	Kunderova
spolubydlící	spolubydlící	k1gFnSc2	spolubydlící
ze	z	k7c2	z
studentské	studentský	k2eAgFnSc2d1	studentská
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
kufru	kufr	k1gInSc2	kufr
<g/>
,	,	kIx,	,
západní	západní	k2eAgMnSc1d1	západní
agent	agent	k1gMnSc1	agent
Miroslav	Miroslav	k1gMnSc1	Miroslav
Dvořáček	Dvořáček	k1gMnSc1	Dvořáček
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
poté	poté	k6eAd1	poté
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
22	[number]	k4	22
letům	let	k1gInPc3	let
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
strávil	strávit	k5eAaPmAgMnS	strávit
téměř	téměř	k6eAd1	téměř
14	[number]	k4	14
v	v	k7c6	v
těžkých	těžký	k2eAgFnPc6d1	těžká
podmínkách	podmínka	k1gFnPc6	podmínka
pracovních	pracovní	k2eAgInPc2d1	pracovní
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Kundera	Kunder	k1gMnSc4	Kunder
označil	označit	k5eAaPmAgMnS	označit
tvrzení	tvrzení	k1gNnSc4	tvrzení
za	za	k7c4	za
lež	lež	k1gFnSc4	lež
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
do	do	k7c2	do
záznamu	záznam	k1gInSc2	záznam
dostalo	dostat	k5eAaPmAgNnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
ani	ani	k8xC	ani
manželé	manžel	k1gMnPc1	manžel
Dvořáčkovi	Dvořáčkův	k2eAgMnPc1d1	Dvořáčkův
však	však	k9	však
o	o	k7c6	o
pravosti	pravost	k1gFnSc6	pravost
materiálů	materiál	k1gInPc2	materiál
nepochybují	pochybovat	k5eNaImIp3nP	pochybovat
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
Dvořáčková	Dvořáčková	k1gFnSc1	Dvořáčková
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dodala	dodat	k5eAaPmAgFnS	dodat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
údaje	údaj	k1gInPc4	údaj
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
mého	můj	k3xOp1gMnSc2	můj
manžela	manžel	k1gMnSc2	manžel
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
účastnil	účastnit	k5eAaImAgMnS	účastnit
a	a	k8xC	a
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
<g/>
,	,	kIx,	,
nenašli	najít	k5eNaPmAgMnP	najít
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
dokumentech	dokument	k1gInPc6	dokument
nic	nic	k6eAd1	nic
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nS	by
neodpovídalo	odpovídat	k5eNaImAgNnS	odpovídat
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
literárního	literární	k2eAgMnSc2d1	literární
historika	historik	k1gMnSc2	historik
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Pešata	Pešat	k1gMnSc2	Pešat
Miroslava	Miroslav	k1gMnSc2	Miroslav
Dvořáčka	Dvořáček	k1gMnSc2	Dvořáček
udal	udat	k5eAaPmAgMnS	udat
Kunderův	Kunderův	k2eAgMnSc1d1	Kunderův
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
přítel	přítel	k1gMnSc1	přítel
Miroslav	Miroslav	k1gMnSc1	Miroslav
Dlask	Dlask	k1gMnSc1	Dlask
(	(	kIx(	(
<g/>
řekl	říct	k5eAaPmAgMnS	říct
o	o	k7c6	o
tom	ten	k3xDgMnSc6	ten
Pešatovi	Pešat	k1gMnSc6	Pešat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
pobytu	pobyt	k1gInSc6	pobyt
Miroslava	Miroslav	k1gMnSc2	Miroslav
Dvořáčka	Dvořáček	k1gMnSc2	Dvořáček
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Ivy	Iva	k1gFnSc2	Iva
Militké	Militká	k1gFnSc2	Militká
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yRgFnSc2	který
Dvořáček	Dvořáček	k1gMnSc1	Dvořáček
plánoval	plánovat	k5eAaImAgMnS	plánovat
přenocovat	přenocovat	k5eAaPmF	přenocovat
<g/>
.	.	kIx.	.
</s>
<s>
Iva	Iva	k1gFnSc1	Iva
Militká-Dlasková	Militká-Dlaskový	k2eAgFnSc1d1	Militká-Dlaskový
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagovala	reagovat	k5eAaBmAgFnS	reagovat
úvahou	úvaha	k1gFnSc7	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dlask	Dlask	k1gMnSc1	Dlask
mohl	moct	k5eAaImAgMnS	moct
požádat	požádat	k5eAaPmF	požádat
Kunderu	Kundera	k1gFnSc4	Kundera
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
bývalého	bývalý	k2eAgMnSc2d1	bývalý
kriminalisty	kriminalista	k1gMnSc2	kriminalista
Aloise	Alois	k1gMnSc2	Alois
Paška	Pašek	k1gMnSc2	Pašek
však	však	k8xC	však
Kundera	Kunder	k1gMnSc2	Kunder
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
uveden	uvést	k5eAaPmNgInS	uvést
jenom	jenom	k9	jenom
namátkou	namátkou	k6eAd1	namátkou
jako	jako	k9	jako
zástupce	zástupce	k1gMnSc1	zástupce
studentů	student	k1gMnPc2	student
a	a	k8xC	a
oznámení	oznámení	k1gNnSc1	oznámení
na	na	k7c6	na
policii	policie	k1gFnSc6	policie
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
motivováno	motivován	k2eAgNnSc1d1	motivováno
hledáním	hledání	k1gNnSc7	hledání
pachatele	pachatel	k1gMnPc4	pachatel
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
kriminální	kriminální	k2eAgFnSc2d1	kriminální
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
na	na	k7c6	na
studentské	studentský	k2eAgFnSc6d1	studentská
koleji	kolej	k1gFnSc6	kolej
skrýval	skrývat	k5eAaImAgInS	skrývat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Miroslav	Miroslav	k1gMnSc1	Miroslav
Dvořáček	Dvořáček	k1gMnSc1	Dvořáček
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
odhalen	odhalit	k5eAaPmNgMnS	odhalit
náhodně	náhodně	k6eAd1	náhodně
a	a	k8xC	a
bez	bez	k7c2	bez
úmyslného	úmyslný	k2eAgInSc2d1	úmyslný
zásahu	zásah	k1gInSc2	zásah
Kundery	Kundera	k1gFnSc2	Kundera
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
jako	jako	k9	jako
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
od	od	k7c2	od
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
sbírky	sbírka	k1gFnSc2	sbírka
Člověk	člověk	k1gMnSc1	člověk
zahrada	zahrada	k1gFnSc1	zahrada
širá	širý	k2eAgFnSc1d1	širá
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
k	k	k7c3	k
intimní	intimní	k2eAgFnSc3d1	intimní
lyrice	lyrika	k1gFnSc3	lyrika
(	(	kIx(	(
<g/>
Monology	monolog	k1gInPc4	monolog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dobového	dobový	k2eAgInSc2d1	dobový
schematismu	schematismus	k1gInSc2	schematismus
se	se	k3xPyFc4	se
vymanil	vymanit	k5eAaPmAgMnS	vymanit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
tvorbou	tvorba	k1gFnSc7	tvorba
dramatickou	dramatický	k2eAgFnSc7d1	dramatická
(	(	kIx(	(
<g/>
Majitelé	majitel	k1gMnPc1	majitel
klíčů	klíč	k1gInPc2	klíč
<g/>
,	,	kIx,	,
Ptákovina	ptákovina	k1gFnSc1	ptákovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
nejvíce	hodně	k6eAd3	hodně
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prózy	próza	k1gFnSc2	próza
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
souborem	soubor	k1gInSc7	soubor
povídek	povídka	k1gFnPc2	povídka
o	o	k7c6	o
vztazích	vztah	k1gInPc6	vztah
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
Směšné	směšný	k2eAgFnSc2d1	směšná
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
dle	dle	k7c2	dle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
odpočíval	odpočívat	k5eAaImAgMnS	odpočívat
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
Majitelů	majitel	k1gMnPc2	majitel
klíčů	klíč	k1gInPc2	klíč
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Povídky	povídka	k1gFnPc1	povídka
již	již	k6eAd1	již
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
základní	základní	k2eAgInPc4d1	základní
rysy	rys	k1gInPc4	rys
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
Kunderových	Kunderová	k1gFnPc2	Kunderová
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vydal	vydat	k5eAaPmAgMnS	vydat
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
okamžitě	okamžitě	k6eAd1	okamžitě
učinil	učinit	k5eAaImAgMnS	učinit
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
představitelů	představitel	k1gMnPc2	představitel
české	český	k2eAgFnSc2d1	Česká
literární	literární	k2eAgFnSc2d1	literární
scény	scéna	k1gFnSc2	scéna
-	-	kIx~	-
Žert	žert	k1gInSc1	žert
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
jednak	jednak	k8xC	jednak
vyrovnával	vyrovnávat	k5eAaImAgMnS	vyrovnávat
s	s	k7c7	s
traumatem	trauma	k1gNnSc7	trauma
zločinů	zločin	k1gInPc2	zločin
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
s	s	k7c7	s
trpkou	trpký	k2eAgFnSc7d1	trpká
ironií	ironie	k1gFnSc7	ironie
zobrazoval	zobrazovat	k5eAaImAgInS	zobrazovat
i	i	k9	i
kulturní	kulturní	k2eAgInSc1d1	kulturní
lídry	lídr	k1gMnPc7	lídr
let	léto	k1gNnPc2	léto
šedesátých	šedesátý	k4xOgInPc2	šedesátý
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
bývalé	bývalý	k2eAgMnPc4d1	bývalý
stalinistické	stalinistický	k2eAgMnPc4d1	stalinistický
básníky	básník	k1gMnPc4	básník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
náhle	náhle	k6eAd1	náhle
konvertovali	konvertovat	k5eAaBmAgMnP	konvertovat
k	k	k7c3	k
existencialismu	existencialismus	k1gInSc3	existencialismus
a	a	k8xC	a
jiným	jiný	k2eAgInPc3d1	jiný
dobovým	dobový	k2eAgInPc3d1	dobový
intelektuálním	intelektuální	k2eAgInPc3d1	intelektuální
směrům	směr	k1gInPc3	směr
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgFnSc1d1	hlavní
negativní	negativní	k2eAgFnSc1d1	negativní
postava	postava	k1gFnSc1	postava
Žertu	žert	k1gInSc3	žert
Pavel	Pavel	k1gMnSc1	Pavel
Zemánek	Zemánek	k1gMnSc1	Zemánek
podle	podle	k7c2	podle
většiny	většina	k1gFnSc2	většina
interpretů	interpret	k1gMnPc2	interpret
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
Pavlu	Pavel	k1gMnSc3	Pavel
Kohoutovi	Kohout	k1gMnSc3	Kohout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
docela	docela	k6eAd1	docela
dobře	dobře	k6eAd1	dobře
i	i	k9	i
o	o	k7c6	o
Kunderu	Kunder	k1gInSc6	Kunder
samotného	samotný	k2eAgNnSc2d1	samotné
<g/>
,	,	kIx,	,
zobrazeného	zobrazený	k2eAgNnSc2d1	zobrazené
se	s	k7c7	s
zničující	zničující	k2eAgFnSc7d1	zničující
sebeironií	sebeironie	k1gFnSc7	sebeironie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vedle	vedle	k7c2	vedle
politické	politický	k2eAgFnSc2d1	politická
roviny	rovina	k1gFnSc2	rovina
byla	být	k5eAaImAgFnS	být
výrazně	výrazně	k6eAd1	výrazně
recipována	recipován	k2eAgFnSc1d1	recipována
i	i	k8xC	i
samotná	samotný	k2eAgFnSc1d1	samotná
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Kundera	Kundero	k1gNnPc4	Kundero
zvolil	zvolit	k5eAaPmAgInS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
hlavního	hlavní	k2eAgInSc2d1	hlavní
příběhu	příběh	k1gInSc2	příběh
totiž	totiž	k9	totiž
do	do	k7c2	do
románu	román	k1gInSc2	román
zapracoval	zapracovat	k5eAaPmAgInS	zapracovat
i	i	k9	i
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
esej	esej	k1gInSc1	esej
o	o	k7c6	o
folklóru	folklór	k1gInSc6	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
metodu	metoda	k1gFnSc4	metoda
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
neopustil	opustit	k5eNaPmAgMnS	opustit
a	a	k8xC	a
i	i	k9	i
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
řazen	řadit	k5eAaImNgMnS	řadit
k	k	k7c3	k
představitelům	představitel	k1gMnPc3	představitel
literární	literární	k2eAgFnSc2d1	literární
postmoderny	postmoderna	k1gFnSc2	postmoderna
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
po	po	k7c4	po
bok	bok	k1gInSc4	bok
Umberta	Umbert	k1gMnSc2	Umbert
Eca	Eca	k1gMnSc2	Eca
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
on	on	k3xPp3gMnSc1	on
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
svých	svůj	k3xOyFgInPc2	svůj
textů	text	k1gInPc2	text
integruje	integrovat	k5eAaBmIp3nS	integrovat
nějakou	nějaký	k3yIgFnSc4	nějaký
"	"	kIx"	"
<g/>
encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
<g/>
"	"	kIx"	"
či	či	k8xC	či
filozofický	filozofický	k2eAgInSc4d1	filozofický
esej	esej	k1gInSc4	esej
<g/>
.	.	kIx.	.
</s>
<s>
Žert	žert	k1gInSc1	žert
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
;	;	kIx,	;
po	po	k7c6	po
sovětské	sovětský	k2eAgFnSc6d1	sovětská
invazi	invaze	k1gFnSc6	invaze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
Kundera	Kundero	k1gNnSc2	Kundero
ztratil	ztratit	k5eAaPmAgMnS	ztratit
možnost	možnost	k1gFnSc4	možnost
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
ještě	ještě	k6eAd1	ještě
vyšly	vyjít	k5eAaPmAgFnP	vyjít
Směšné	směšný	k2eAgFnPc1d1	směšná
lásky	láska	k1gFnPc1	láska
v	v	k7c6	v
jednosvazkovém	jednosvazkový	k2eAgNnSc6d1	jednosvazkové
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vycházely	vycházet	k5eAaImAgFnP	vycházet
Kunderovy	Kunderův	k2eAgFnPc1d1	Kunderova
knihy	kniha	k1gFnPc1	kniha
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
Milan	Milan	k1gMnSc1	Milan
Kundera	Kunder	k1gMnSc4	Kunder
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
exil	exil	k1gInSc4	exil
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgInS	napsat
však	však	k9	však
doma	doma	k6eAd1	doma
dva	dva	k4xCgInPc1	dva
další	další	k2eAgInPc1d1	další
romány	román	k1gInPc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
jinde	jinde	k6eAd1	jinde
znovu	znovu	k6eAd1	znovu
účtuje	účtovat	k5eAaImIp3nS	účtovat
s	s	k7c7	s
50	[number]	k4	50
<g/>
.	.	kIx.	.
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
obecnější	obecní	k2eAgFnSc6d2	obecní
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Kundera	Kundera	k1gFnSc1	Kundera
zde	zde	k6eAd1	zde
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
téma	téma	k1gNnSc4	téma
lyrismu	lyrismus	k1gInSc2	lyrismus
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
násilí	násilí	k1gNnSc3	násilí
a	a	k8xC	a
totalitě	totalita	k1gFnSc3	totalita
<g/>
.	.	kIx.	.
</s>
<s>
Valčík	valčík	k1gInSc1	valčík
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
nese	nést	k5eAaImIp3nS	nést
rysy	rys	k1gInPc4	rys
grotesky	groteska	k1gFnSc2	groteska
s	s	k7c7	s
tragickými	tragický	k2eAgInPc7d1	tragický
aspekty	aspekt	k1gInPc7	aspekt
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
dává	dávat	k5eAaImIp3nS	dávat
též	též	k9	též
hořké	hořký	k2eAgInPc4d1	hořký
poslední	poslední	k2eAgInPc4d1	poslední
sbohem	sbohem	k0	sbohem
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
malosti	malost	k1gFnSc2	malost
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
se	se	k3xPyFc4	se
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
pro	pro	k7c4	pro
emigraci	emigrace	k1gFnSc4	emigrace
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Kundera	Kunder	k1gMnSc4	Kunder
text	text	k1gInSc1	text
dopisoval	dopisovat	k5eAaImAgInS	dopisovat
již	již	k6eAd1	již
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
romány	román	k1gInPc4	román
napsal	napsat	k5eAaBmAgMnS	napsat
Kundera	Kundero	k1gNnSc2	Kundero
ještě	ještě	k6eAd1	ještě
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
překladu	překlad	k1gInSc3	překlad
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
smíchu	smích	k1gInSc2	smích
a	a	k8xC	a
zapomnění	zapomnění	k1gNnSc4	zapomnění
opět	opět	k6eAd1	opět
kriticky	kriticky	k6eAd1	kriticky
účtuje	účtovat	k5eAaImIp3nS	účtovat
s	s	k7c7	s
domovinou	domovina	k1gFnSc7	domovina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
postav	postava	k1gFnPc2	postava
Gustáva	Gustáv	k1gMnSc2	Gustáv
Husáka	Husák	k1gMnSc2	Husák
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
prezidenta	prezident	k1gMnSc2	prezident
zapomnění	zapomnění	k1gNnSc2	zapomnění
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Karla	Karel	k1gMnSc4	Karel
Gotta	Gott	k1gMnSc4	Gott
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
idiota	idiot	k1gMnSc2	idiot
hudby	hudba	k1gFnSc2	hudba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
text	text	k1gInSc4	text
plný	plný	k2eAgInSc4d1	plný
filozofických	filozofický	k2eAgFnPc2d1	filozofická
úvah	úvaha	k1gFnPc2	úvaha
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dělí	dělit	k5eAaImIp3nS	dělit
smích	smích	k1gInSc1	smích
na	na	k7c4	na
andělský	andělský	k2eAgInSc4d1	andělský
(	(	kIx(	(
<g/>
všeobjímající	všeobjímající	k2eAgInSc4d1	všeobjímající
<g/>
,	,	kIx,	,
přitakávající	přitakávající	k2eAgInSc4d1	přitakávající
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
dokáží	dokázat	k5eAaPmIp3nP	dokázat
využívat	využívat	k5eAaImF	využívat
totality	totalita	k1gFnPc4	totalita
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
<g/>
)	)	kIx)	)
a	a	k8xC	a
ďábelský	ďábelský	k2eAgInSc4d1	ďábelský
(	(	kIx(	(
<g/>
ironický	ironický	k2eAgInSc4d1	ironický
<g/>
,	,	kIx,	,
zpochybňující	zpochybňující	k2eAgInSc4d1	zpochybňující
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vždy	vždy	k6eAd1	vždy
podvrací	podvracet	k5eAaImIp3nS	podvracet
a	a	k8xC	a
oživuje	oživovat	k5eAaImIp3nS	oživovat
<g/>
)	)	kIx)	)
-	-	kIx~	-
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
variaci	variace	k1gFnSc4	variace
na	na	k7c4	na
základní	základní	k2eAgNnSc4d1	základní
téma	téma	k1gNnSc4	téma
lyrismu	lyrismus	k1gInSc2	lyrismus
otevřené	otevřený	k2eAgNnSc1d1	otevřené
již	již	k6eAd1	již
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
románu	román	k1gInSc6	román
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Esencí	esence	k1gFnSc7	esence
ďábelského	ďábelský	k2eAgInSc2d1	ďábelský
smíchu	smích	k1gInSc2	smích
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
Kundery	Kundera	k1gFnSc2	Kundera
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Schéma	schéma	k1gNnSc4	schéma
znovu	znovu	k6eAd1	znovu
obměnil	obměnit	k5eAaPmAgInS	obměnit
v	v	k7c6	v
Nesnesitelné	snesitelný	k2eNgFnSc6d1	nesnesitelná
lehkosti	lehkost	k1gFnSc6	lehkost
bytí	bytí	k1gNnSc2	bytí
<g/>
.	.	kIx.	.
</s>
<s>
Polaritu	polarita	k1gFnSc4	polarita
realismus-lyrismus	realismusyrismus	k1gInSc1	realismus-lyrismus
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
andělský-ďábelský	andělský-ďábelský	k2eAgInSc4d1	andělský-ďábelský
smích	smích	k1gInSc4	smích
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
polarita	polarita	k1gFnSc1	polarita
lehkosti	lehkost	k1gFnSc2	lehkost
a	a	k8xC	a
idealismu	idealismus	k1gInSc2	idealismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
Kundery	Kundera	k1gFnSc2	Kundera
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
zastřenou	zastřený	k2eAgFnSc7d1	zastřená
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
recepce	recepce	k1gFnSc2	recepce
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
<g/>
,	,	kIx,	,
že	že	k8xS	že
román	román	k1gInSc4	román
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
i	i	k9	i
disidentská	disidentský	k2eAgFnSc1d1	disidentská
kritika	kritika	k1gFnSc1	kritika
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Milan	Milan	k1gMnSc1	Milan
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
viděla	vidět	k5eAaImAgFnS	vidět
i	i	k9	i
útok	útok	k1gInSc4	útok
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
i	i	k9	i
disidenti	disident	k1gMnPc1	disident
často	často	k6eAd1	často
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c4	v
ideály	ideál	k1gInPc4	ideál
a	a	k8xC	a
nasazovali	nasazovat	k5eAaImAgMnP	nasazovat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Nesnesitelná	snesitelný	k2eNgFnSc1d1	nesnesitelná
lehkost	lehkost	k1gFnSc1	lehkost
bytí	bytí	k1gNnSc2	bytí
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1984	[number]	k4	1984
a	a	k8xC	a
z	z	k7c2	z
Kundery	Kundera	k1gFnSc2	Kundera
udělala	udělat	k5eAaPmAgFnS	udělat
hvězdu	hvězda	k1gFnSc4	hvězda
světové	světový	k2eAgFnSc2d1	světová
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
když	když	k8xS	když
se	se	k3xPyFc4	se
otevírala	otevírat	k5eAaImAgFnS	otevírat
možnost	možnost	k1gFnSc1	možnost
jeho	jeho	k3xOp3gInSc2	jeho
návratu	návrat	k1gInSc2	návrat
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
kultury	kultura	k1gFnSc2	kultura
i	i	k8xC	i
vlasti	vlast	k1gFnSc2	vlast
(	(	kIx(	(
<g/>
perestrojka	perestrojka	k1gFnSc1	perestrojka
<g/>
,	,	kIx,	,
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kundera	Kundera	k1gFnSc1	Kundera
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
češtinu	čeština	k1gFnSc4	čeština
(	(	kIx(	(
<g/>
i	i	k8xC	i
českou	český	k2eAgFnSc4d1	Česká
tematiku	tematika	k1gFnSc4	tematika
<g/>
)	)	kIx)	)
definitivně	definitivně	k6eAd1	definitivně
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
Nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgInSc7d1	poslední
titulem	titul	k1gInSc7	titul
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napsal	napsat	k5eAaBmAgInS	napsat
česky	česky	k6eAd1	česky
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
vyšel	vyjít	k5eAaPmAgInS	vyjít
nejprve	nejprve	k6eAd1	nejprve
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Petra	Petr	k1gMnSc2	Petr
Kussiho	Kussi	k1gMnSc2	Kussi
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
překládal	překládat	k5eAaImAgMnS	překládat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
Kunderova	Kunderův	k2eAgInSc2d1	Kunderův
rukopisu	rukopis	k1gInSc2	rukopis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgInSc4	první
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
česká	český	k2eAgFnSc1d1	Česká
tematika	tematika	k1gFnSc1	tematika
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
opětovně	opětovně	k6eAd1	opětovně
s	s	k7c7	s
románem	román	k1gInSc7	román
slavil	slavit	k5eAaImAgInS	slavit
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Kundera	Kundera	k1gFnSc1	Kundera
tentokrát	tentokrát	k6eAd1	tentokrát
obrátil	obrátit	k5eAaPmAgInS	obrátit
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
k	k	k7c3	k
Západu	západ	k1gInSc3	západ
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
analyzovat	analyzovat	k5eAaImF	analyzovat
jeho	jeho	k3xOp3gFnPc4	jeho
stinné	stinný	k2eAgFnPc4d1	stinná
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
dosud	dosud	k6eAd1	dosud
se	se	k3xPyFc4	se
západní	západní	k2eAgMnSc1d1	západní
čtenář	čtenář	k1gMnSc1	čtenář
mohl	moct	k5eAaImAgMnS	moct
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
koneckonců	koneckonců	k9	koneckonců
mluví	mluvit	k5eAaImIp3nS	mluvit
hlavně	hlavně	k9	hlavně
o	o	k7c6	o
totalitě	totalita	k1gFnSc6	totalita
za	za	k7c7	za
železnou	železný	k2eAgFnSc7d1	železná
oponou	opona	k1gFnSc7	opona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
pojmů	pojem	k1gInPc2	pojem
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
imagologie	imagologie	k1gFnSc1	imagologie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
moc	moc	k1gFnSc1	moc
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
nahradila	nahradit	k5eAaPmAgFnS	nahradit
moc	moc	k1gFnSc4	moc
ideologií	ideologie	k1gFnPc2	ideologie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
stejně	stejně	k6eAd1	stejně
manipulující	manipulující	k2eAgFnSc1d1	manipulující
a	a	k8xC	a
ničivá	ničivý	k2eAgFnSc1d1	ničivá
<g/>
)	)	kIx)	)
Kundera	Kunder	k1gMnSc4	Kunder
vystoupal	vystoupat	k5eAaPmAgMnS	vystoupat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
románový	románový	k2eAgInSc4d1	románový
i	i	k8xC	i
filozofický	filozofický	k2eAgInSc4d1	filozofický
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
románech	román	k1gInPc6	román
znovu	znovu	k6eAd1	znovu
varioval	variovat	k5eAaBmAgInS	variovat
některá	některý	k3yIgNnPc1	některý
svá	svůj	k3xOyFgNnPc4	svůj
základní	základní	k2eAgNnPc4d1	základní
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
skepsi	skepse	k1gFnSc3	skepse
<g/>
,	,	kIx,	,
ďábelskému	ďábelský	k2eAgInSc3d1	ďábelský
smíchu	smích	k1gInSc3	smích
<g/>
,	,	kIx,	,
lehkosti	lehkost	k1gFnSc2	lehkost
a	a	k8xC	a
románu	román	k1gInSc2	román
přidal	přidat	k5eAaPmAgInS	přidat
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
románu	román	k1gInSc6	román
motiv	motiv	k1gInSc1	motiv
pomalosti	pomalost	k1gFnSc2	pomalost
(	(	kIx(	(
<g/>
Pomalost	pomalost	k1gFnSc1	pomalost
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Totožnosti	totožnost	k1gFnSc6	totožnost
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
starému	starý	k2eAgNnSc3d1	staré
tématu	téma	k1gNnSc3	téma
křehkosti	křehkost	k1gFnSc2	křehkost
identity	identita	k1gFnSc2	identita
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
naléhavost	naléhavost	k1gFnSc4	naléhavost
zesílily	zesílit	k5eAaPmAgFnP	zesílit
nové	nový	k2eAgFnPc1d1	nová
komunikační	komunikační	k2eAgFnPc1d1	komunikační
technologie	technologie	k1gFnPc1	technologie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
anonymita	anonymita	k1gFnSc1	anonymita
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zatím	zatím	k6eAd1	zatím
posledními	poslední	k2eAgInPc7d1	poslední
texty	text	k1gInPc7	text
jsou	být	k5eAaImIp3nP	být
Nevědění	věděný	k2eNgMnPc1d1	věděný
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
Oslava	oslava	k1gFnSc1	oslava
bezvýznamnosti	bezvýznamnost	k1gFnSc2	bezvýznamnost
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
románů	román	k1gInPc2	román
vydal	vydat	k5eAaPmAgMnS	vydat
i	i	k9	i
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
esejů	esej	k1gInPc2	esej
<g/>
,	,	kIx,	,
nejslavnější	slavný	k2eAgInSc4d3	nejslavnější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
Umění	umění	k1gNnSc1	umění
románu	román	k1gInSc2	román
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
-	-	kIx~	-
Státní	státní	k2eAgFnSc1d1	státní
cena	cena	k1gFnSc1	cena
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
za	za	k7c4	za
drama	drama	k1gNnSc4	drama
Majitelé	majitel	k1gMnPc1	majitel
klíčů	klíč	k1gInPc2	klíč
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Svazu	svaz	k1gInSc2	svaz
československých	československý	k2eAgMnPc2d1	československý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
1973	[number]	k4	1973
-	-	kIx~	-
Prix	Prix	k1gInSc1	Prix
Médicis	Médicis	k1gFnSc2	Médicis
-	-	kIx~	-
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
román	román	k1gInSc4	román
za	za	k7c4	za
román	román	k1gInSc4	román
<g />
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
1978	[number]	k4	1978
-	-	kIx~	-
Premio	Premio	k1gMnSc1	Premio
Mondello	Mondello	k1gNnSc4	Mondello
za	za	k7c4	za
román	román	k1gInSc4	román
Valčík	valčík	k1gInSc4	valčík
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
<g/>
.	.	kIx.	.
1981	[number]	k4	1981
-	-	kIx~	-
American	American	k1gMnSc1	American
Common	Common	k1gMnSc1	Common
Wealth	Wealth	k1gMnSc1	Wealth
Award	Award	k1gMnSc1	Award
za	za	k7c4	za
Knihu	kniha	k1gFnSc4	kniha
smíchu	smích	k1gInSc2	smích
a	a	k8xC	a
zapomnění	zapomnění	k1gNnSc4	zapomnění
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
-	-	kIx~	-
The	The	k1gFnSc1	The
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
Prize	Prize	k1gFnSc1	Prize
for	forum	k1gNnPc2	forum
the	the	k?	the
Freedom	Freedom	k1gInSc1	Freedom
of	of	k?	of
the	the	k?	the
Individual	Individual	k1gInSc1	Individual
in	in	k?	in
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
-	-	kIx~	-
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
státní	státní	k2eAgFnSc1d1	státní
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s>
evropskou	evropský	k2eAgFnSc4d1	Evropská
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Nelly	Nella	k1gFnSc2	Nella
Sachsové	Sachsová	k1gFnSc2	Sachsová
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
kritiků	kritik	k1gMnPc2	kritik
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
za	za	k7c4	za
esej	esej	k1gInSc4	esej
Umění	umění	k1gNnSc1	umění
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
-	-	kIx~	-
Řád	řád	k1gInSc1	řád
Čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
britského	britský	k2eAgInSc2d1	britský
deníku	deník	k1gInSc2	deník
The	The	k1gMnSc1	The
Independent	independent	k1gMnSc1	independent
(	(	kIx(	(
<g/>
Independent	independent	k1gMnSc1	independent
Foreign	Foreigna	k1gFnPc2	Foreigna
Fiction	Fiction	k1gInSc1	Fiction
Award	Award	k1gMnSc1	Award
<g/>
)	)	kIx)	)
za	za	k7c4	za
Nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Vilenice	Vilenice	k1gFnSc1	Vilenice
(	(	kIx(	(
<g/>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
-	-	kIx~	-
novinářská	novinářský	k2eAgFnSc1d1	novinářská
cena	cena	k1gFnSc1	cena
Prix	Prix	k1gInSc4	Prix
Aujourd	Aujourd	k1gInSc1	Aujourd
<g/>
'	'	kIx"	'
<g/>
hui	hui	k?	hui
za	za	k7c4	za
Les	les	k1gInSc4	les
testaments	testaments	k1gInSc1	testaments
trahis	trahis	k1gFnSc1	trahis
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Seiferta	Seifert	k1gMnSc2	Seifert
za	za	k7c4	za
román	román	k1gInSc4	román
Nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
-	-	kIx~	-
Medaile	medaile	k1gFnSc2	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
-	-	kIx~	-
I.	I.	kA	I.
stupeň	stupeň	k1gInSc1	stupeň
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
křepelek	křepelka	k1gFnPc2	křepelka
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
2000	[number]	k4	2000
-	-	kIx~	-
Herderova	Herderův	k2eAgFnSc1d1	Herderova
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
literární	literární	k2eAgFnSc4d1	literární
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
-	-	kIx~	-
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
-	-	kIx~	-
Státní	státní	k2eAgFnSc1d1	státní
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
román	román	k1gInSc4	román
Nesnesitelná	snesitelný	k2eNgFnSc1d1	nesnesitelná
lehkost	lehkost	k1gFnSc1	lehkost
bytí	bytí	k1gNnSc2	bytí
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
dosavadní	dosavadní	k2eAgFnSc3d1	dosavadní
prozaické	prozaický	k2eAgFnSc3d1	prozaická
a	a	k8xC	a
esejistické	esejistický	k2eAgFnSc3d1	esejistická
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
-	-	kIx~	-
Světová	světový	k2eAgFnSc1d1	světová
cena	cena	k1gFnSc1	cena
Nadace	nadace	k1gFnSc2	nadace
Simone	Simon	k1gMnSc5	Simon
a	a	k8xC	a
Cino	Cina	k1gMnSc5	Cina
del	del	k?	del
Duca	Duc	k2eAgMnSc4d1	Duc
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
-	-	kIx~	-
Čestné	čestný	k2eAgNnSc1d1	čestné
občanství	občanství	k1gNnSc1	občanství
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
jako	jako	k8xC	jako
brněnský	brněnský	k2eAgMnSc1d1	brněnský
rodák	rodák	k1gMnSc1	rodák
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
zahrada	zahrada	k1gFnSc1	zahrada
širá	širý	k2eAgFnSc1d1	širá
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
Poslední	poslední	k2eAgFnSc1d1	poslední
máj	máj	k1gFnSc1	máj
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
-	-	kIx~	-
oslava	oslava	k1gFnSc1	oslava
Julia	Julius	k1gMnSc2	Julius
Fučíka	Fučík	k1gMnSc2	Fučík
<g/>
.	.	kIx.	.
</s>
<s>
Monology	monolog	k1gInPc1	monolog
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Majitelé	majitel	k1gMnPc1	majitel
klíčů	klíč	k1gInPc2	klíč
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
-	-	kIx~	-
premiéra	premiéra	k1gFnSc1	premiéra
29.4	[number]	k4	29.4
<g/>
.1962	.1962	k4	.1962
Ptákovina	ptákovina	k1gFnSc1	ptákovina
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
-	-	kIx~	-
rozmnoženo	rozmnožit	k5eAaPmNgNnS	rozmnožit
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Dvě	dva	k4xCgFnPc1	dva
uši	ucho	k1gNnPc4	ucho
dvě	dva	k4xCgFnPc4	dva
svatby	svatba	k1gFnSc2	svatba
Jakub	Jakub	k1gMnSc1	Jakub
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
pán	pán	k1gMnSc1	pán
<g/>
:	:	kIx,	:
Pocta	pocta	k1gFnSc1	pocta
Denisu	Denisa	k1gFnSc4	Denisa
Diderotovi	Diderot	k1gMnSc6	Diderot
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
-	-	kIx~	-
dokončena	dokončen	k2eAgFnSc1d1	dokončena
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
1975	[number]	k4	1975
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Evalda	Evald	k1gMnSc2	Evald
Schorma	Schorm	k1gMnSc2	Schorm
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
tiskem	tisk	k1gInSc7	tisk
1992	[number]	k4	1992
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
romány	román	k1gInPc4	román
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Směšných	směšný	k2eAgFnPc2d1	směšná
lásek	láska	k1gFnPc2	láska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
jsou	být	k5eAaImIp3nP	být
souborem	soubor	k1gInSc7	soubor
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Žert	žert	k1gInSc1	žert
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Směšné	směšný	k2eAgFnSc2d1	směšná
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
povídky	povídka	k1gFnPc1	povídka
Já	já	k3xPp1nSc1	já
truchlivý	truchlivý	k2eAgMnSc1d1	truchlivý
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
Sestřičko	sestřička	k1gFnSc5	sestřička
mých	můj	k3xOp1gFnPc2	můj
sestřiček	sestřička	k1gFnPc2	sestřička
a	a	k8xC	a
Zvěstovatel	zvěstovatel	k1gMnSc1	zvěstovatel
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
<g/>
:	:	kIx,	:
Směšné	směšný	k2eAgFnSc2d1	směšná
lásky	láska	k1gFnSc2	láska
<g/>
:	:	kIx,	:
tři	tři	k4xCgFnPc1	tři
melancholické	melancholický	k2eAgFnPc1d1	melancholická
anekdoty	anekdota	k1gFnPc1	anekdota
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Já	já	k3xPp1nSc1	já
truchlivý	truchlivý	k2eAgMnSc1d1	truchlivý
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
Sestřičko	sestřička	k1gFnSc5	sestřička
mých	můj	k3xOp1gFnPc2	můj
sestřiček	sestřička	k1gFnPc2	sestřička
<g/>
,	,	kIx,	,
Nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
smát	smát	k5eAaImF	smát
<g/>
)	)	kIx)	)
Druhý	druhý	k4xOgInSc1	druhý
sešit	sešit	k1gInSc1	sešit
směšných	směšný	k2eAgFnPc2d1	směšná
lásek	láska	k1gFnPc2	láska
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Zlaté	zlatý	k2eAgNnSc1d1	Zlaté
jablko	jablko	k1gNnSc1	jablko
věčné	věčný	k2eAgFnSc2d1	věčná
touhy	touha	k1gFnSc2	touha
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Zvěstovatel	zvěstovatel	k1gMnSc1	zvěstovatel
<g/>
,	,	kIx,	,
Falešný	falešný	k2eAgInSc1d1	falešný
autostop	autostop	k1gInSc1	autostop
<g/>
)	)	kIx)	)
Třetí	třetí	k4xOgInSc1	třetí
sešit	sešit	k1gInSc1	sešit
směšných	směšný	k2eAgFnPc2d1	směšná
lásek	láska	k1gFnPc2	láska
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Symposion	symposion	k1gNnSc1	symposion
<g/>
,	,	kIx,	,
Ať	ať	k8xC	ať
ustoupí	ustoupit	k5eAaPmIp3nP	ustoupit
staří	starý	k2eAgMnPc1d1	starý
mrtví	mrtvý	k1gMnPc1	mrtvý
mladým	mladý	k2eAgMnPc3d1	mladý
mrtvým	mrtvý	k1gMnPc3	mrtvý
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
a	a	k8xC	a
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
Doktor	doktor	k1gMnSc1	doktor
Havel	Havel	k1gMnSc1	Havel
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
Valčík	valčík	k1gInSc1	valčík
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Život	život	k1gInSc1	život
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
jinde	jinde	k6eAd1	jinde
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Kniha	kniha	k1gFnSc1	kniha
smíchu	smích	k1gInSc2	smích
a	a	k8xC	a
zapomnění	zapomnění	k1gNnSc4	zapomnění
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Nesnesitelná	snesitelný	k2eNgFnSc1d1	nesnesitelná
lehkost	lehkost	k1gFnSc1	lehkost
bytí	bytí	k1gNnSc2	bytí
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
poslední	poslední	k2eAgInSc4d1	poslední
psaný	psaný	k2eAgInSc4d1	psaný
česky	česky	k6eAd1	česky
Pomalost	pomalost	k1gFnSc4	pomalost
(	(	kIx(	(
<g/>
La	la	k1gNnPc1	la
Lenteur	Lenteura	k1gFnPc2	Lenteura
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Totožnost	totožnost	k1gFnSc1	totožnost
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Identité	Identitý	k2eAgInPc1d1	Identitý
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Nevědomost	nevědomost	k1gFnSc1	nevědomost
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Ignorance	ignorance	k1gFnSc1	ignorance
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Oslava	oslava	k1gFnSc1	oslava
bezvýznamnosti	bezvýznamnost	k1gFnSc2	bezvýznamnost
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Fê	Fê	k1gFnSc2	Fê
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
insignifiance	insignifianec	k1gMnSc2	insignifianec
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
O	o	k7c6	o
sporech	spor	k1gInPc6	spor
dědických	dědický	k2eAgInPc2d1	dědický
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
Umění	umění	k1gNnSc1	umění
románu	román	k1gInSc2	román
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
Vladislava	Vladislav	k1gMnSc2	Vladislav
Vančury	Vančura	k1gMnSc2	Vančura
za	za	k7c7	za
velkou	velký	k2eAgFnSc7d1	velká
epikou	epika	k1gFnSc7	epika
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Český	český	k2eAgInSc1d1	český
úděl	úděl	k1gInSc1	úděl
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Radikalismus	radikalismus	k1gInSc1	radikalismus
a	a	k8xC	a
exhibicionismus	exhibicionismus	k1gInSc1	exhibicionismus
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Únos	únos	k1gInSc1	únos
západu	západ	k1gInSc2	západ
aneb	aneb	k?	aneb
Tragédie	tragédie	k1gFnSc2	tragédie
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
du	du	k?	du
<g />
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
(	(	kIx(	(
<g/>
Umění	umění	k1gNnSc1	umění
románu	román	k1gInSc2	román
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Les	les	k1gInSc1	les
testaments	testaments	k1gInSc1	testaments
trahis	trahis	k1gFnSc1	trahis
(	(	kIx(	(
<g/>
Zrazené	zrazený	k2eAgInPc1d1	zrazený
testamenty	testament	k1gInPc1	testament
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
en	en	k?	en
bas	bas	k1gInSc1	bas
tu	tu	k6eAd1	tu
humeras	humeras	k1gInSc1	humeras
des	des	k1gNnSc2	des
roses	rosesa	k1gFnPc2	rosesa
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
ilustrovaná	ilustrovaný	k2eAgFnSc1d1	ilustrovaná
Ernestem	Ernest	k1gMnSc7	Ernest
Breleuerem	Breleuer	k1gMnSc7	Breleuer
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Můj	můj	k3xOp1gMnSc1	můj
Janáček	Janáček	k1gMnSc1	Janáček
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Zneuznávané	zneuznávaný	k2eAgNnSc1d1	Zneuznávané
dědictví	dědictví	k1gNnSc1	dědictví
Cervantesovo	Cervantesův	k2eAgNnSc1d1	Cervantesovo
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Le	Le	k1gFnSc2	Le
Rideau	Rideaus	k1gInSc2	Rideaus
(	(	kIx(	(
<g/>
Opona	opona	k1gFnSc1	opona
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Kastrující	kastrující	k2eAgInSc1d1	kastrující
stín	stín	k1gInSc1	stín
svatého	svatý	k2eAgMnSc2d1	svatý
Garty	Garta	k1gMnSc2	Garta
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
části	část	k1gFnSc2	část
Les	les	k1gInSc4	les
testaments	testaments	k6eAd1	testaments
trahis	trahis	k1gFnSc2	trahis
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Nechovejte	chovat	k5eNaImRp2nP	chovat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
jako	jako	k9	jako
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
příteli	přítel	k1gMnSc3	přítel
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Na	na	k7c6	na
minimální	minimální	k2eAgFnSc6d1	minimální
ploše	plocha	k1gFnSc6	plocha
maximum	maximum	k1gNnSc1	maximum
různosti	různost	k1gFnSc2	různost
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Une	Une	k1gFnSc1	Une
rencontre	rencontre	k?	rencontre
(	(	kIx(	(
<g/>
Setkání	setkání	k1gNnSc1	setkání
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
pojmy	pojem	k1gInPc1	pojem
<g/>
,	,	kIx,	,
situace	situace	k1gFnSc1	situace
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Zahradou	zahrada	k1gFnSc7	zahrada
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
mám	mít	k5eAaImIp1nS	mít
rád	rád	k2eAgMnSc1d1	rád
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
O	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
románu	román	k1gInSc2	román
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
smát	smát	k5eAaImF	smát
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Hynek	Hynek	k1gMnSc1	Hynek
Bočan	bočan	k1gMnSc1	bočan
<g/>
)	)	kIx)	)
Žert	žert	k1gInSc1	žert
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jaromil	Jaromil	k1gMnSc1	Jaromil
Jireš	Jireš	k1gMnSc1	Jireš
<g/>
)	)	kIx)	)
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
truchlivý	truchlivý	k2eAgMnSc1d1	truchlivý
bůh	bůh	k1gMnSc1	bůh
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
Kachlík	Kachlík	k1gMnSc1	Kachlík
<g/>
)	)	kIx)	)
Nesnesitelná	snesitelný	k2eNgFnSc1d1	nesnesitelná
lehkost	lehkost	k1gFnSc1	lehkost
bytí	bytí	k1gNnSc2	bytí
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Philip	Philip	k1gMnSc1	Philip
Kaufman	Kaufman	k1gMnSc1	Kaufman
<g/>
)	)	kIx)	)
</s>
