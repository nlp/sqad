<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schlacht	Schlacht	k2eAgInSc1d1	Schlacht
am	am	k?	am
Weißen	Weißen	k2eAgInSc1d1	Weißen
Berg	Berg	k1gInSc1	Berg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svedená	svedený	k2eAgFnSc1d1	svedená
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1620	[number]	k4	1620
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
bitva	bitva	k1gFnSc1	bitva
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
se	se	k3xPyFc4	se
střetly	střetnout	k5eAaPmAgFnP	střetnout
česká	český	k2eAgFnSc1d1	Česká
stavovská	stavovský	k2eAgFnSc1d1	stavovská
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
armády	armáda	k1gFnPc1	armáda
katolické	katolický	k2eAgFnPc1d1	katolická
<g/>
,	,	kIx,	,
armáda	armáda	k1gFnSc1	armáda
císaře	císař	k1gMnSc2	císař
Svaté	svatá	k1gFnSc2	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štýrského	štýrský	k2eAgMnSc2d1	štýrský
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
německé	německý	k2eAgFnSc2d1	německá
Katolické	katolický	k2eAgFnSc2d1	katolická
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Katolické	katolický	k2eAgFnPc1d1	katolická
armády	armáda	k1gFnPc1	armáda
po	po	k7c6	po
jedné	jeden	k4xCgFnSc3	jeden
až	až	k9	až
dvou	dva	k4xCgMnPc6	dva
hodinách	hodina	k1gFnPc6	hodina
zdolaly	zdolat	k5eAaPmAgFnP	zdolat
méně	málo	k6eAd2	málo
početnou	početný	k2eAgFnSc4d1	početná
stavovskou	stavovský	k2eAgFnSc4d1	stavovská
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
zpečetila	zpečetit	k5eAaPmAgFnS	zpečetit
osud	osud	k1gInSc4	osud
českého	český	k2eAgNnSc2d1	české
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
300	[number]	k4	300
let	léto	k1gNnPc2	léto
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
osud	osud	k1gInSc4	osud
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Situace	situace	k1gFnSc1	situace
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
Ligistická	ligistický	k2eAgFnSc1d1	ligistická
a	a	k8xC	a
stavovská	stavovský	k2eAgFnSc1d1	stavovská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
setkaly	setkat	k5eAaPmAgFnP	setkat
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
ligisté	ligista	k1gMnPc1	ligista
dále	daleko	k6eAd2	daleko
nepostupovali	postupovat	k5eNaImAgMnP	postupovat
a	a	k8xC	a
čekali	čekat	k5eAaImAgMnP	čekat
na	na	k7c4	na
císařské	císařský	k2eAgNnSc4d1	císařské
až	až	k9	až
do	do	k7c2	do
setmění	setmění	k1gNnSc2	setmění
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
doneslo	donést	k5eAaPmAgNnS	donést
katolické	katolický	k2eAgFnSc3d1	katolická
armádě	armáda	k1gFnSc3	armáda
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotky	jednotka	k1gFnPc1	jednotka
povstalců	povstalec	k1gMnPc2	povstalec
ustoupily	ustoupit	k5eAaPmAgFnP	ustoupit
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
21	[number]	k4	21
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnPc1	hodina
začaly	začít	k5eAaPmAgFnP	začít
zvedat	zvedat	k5eAaImF	zvedat
i	i	k9	i
bavorské	bavorský	k2eAgFnPc4d1	bavorská
voje	voj	k1gFnPc4	voj
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
první	první	k4xOgFnSc1	první
bojůvka	bojůvka	k1gFnSc1	bojůvka
mezi	mezi	k7c7	mezi
císařským	císařský	k2eAgMnSc7d1	císařský
plukovníkem	plukovník	k1gMnSc7	plukovník
Nicolou	Nicola	k1gMnSc7	Nicola
de	de	k?	de
Gauchierem	Gauchier	k1gMnSc7	Gauchier
a	a	k8xC	a
předsunutým	předsunutý	k2eAgMnSc7d1	předsunutý
táborem	tábor	k1gMnSc7	tábor
lehké	lehký	k2eAgFnSc2d1	lehká
uherské	uherský	k2eAgFnSc2d1	uherská
jízdy	jízda	k1gFnSc2	jízda
u	u	k7c2	u
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
<g/>
.	.	kIx.	.
</s>
<s>
Gauchier	Gauchier	k1gInSc1	Gauchier
velel	velet	k5eAaImAgInS	velet
půlce	půlka	k1gFnSc3	půlka
pluku	pluk	k1gInSc2	pluk
valonské	valonský	k2eAgFnSc2d1	valonská
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
připojilo	připojit	k5eAaPmAgNnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
ještě	ještě	k9	ještě
200	[number]	k4	200
jezdců	jezdec	k1gMnPc2	jezdec
od	od	k7c2	od
ligistů	ligista	k1gMnPc2	ligista
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
praporce	praporec	k1gInPc1	praporec
kozáků	kozák	k1gInPc2	kozák
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
součtu	součet	k1gInSc6	součet
činilo	činit	k5eAaImAgNnS	činit
800	[number]	k4	800
až	až	k8xS	až
1000	[number]	k4	1000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
střet	střet	k1gInSc1	střet
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
Gauchier	Gauchier	k1gMnSc1	Gauchier
napadl	napadnout	k5eAaPmAgMnS	napadnout
připravený	připravený	k2eAgInSc4d1	připravený
tábor	tábor	k1gInSc4	tábor
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgMnS	způsobit
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
napadl	napadnout	k5eAaPmAgMnS	napadnout
lehkomyslné	lehkomyslný	k2eAgFnPc4d1	lehkomyslná
Uhry	Uhry	k1gFnPc4	Uhry
nepřipravené	připravený	k2eNgFnPc4d1	nepřipravená
a	a	k8xC	a
nezpůsobil	způsobit	k5eNaPmAgMnS	způsobit
velké	velký	k2eAgFnPc4d1	velká
škody	škoda	k1gFnPc4	škoda
a	a	k8xC	a
Uhrové	Uhrová	k1gFnPc4	Uhrová
prchli	prchnout	k5eAaPmAgMnP	prchnout
patrně	patrně	k6eAd1	patrně
do	do	k7c2	do
Liboce	Liboc	k1gFnSc2	Liboc
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
odehnal	odehnat	k5eAaPmAgInS	odehnat
nejméně	málo	k6eAd3	málo
1	[number]	k4	1
000	[number]	k4	000
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
postup	postup	k1gInSc4	postup
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
postavení	postavení	k1gNnSc4	postavení
obou	dva	k4xCgFnPc2	dva
armád	armáda	k1gFnPc2	armáda
není	být	k5eNaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
popsat	popsat	k5eAaPmF	popsat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jediné	jediný	k2eAgInPc1d1	jediný
dochované	dochovaný	k2eAgInPc1d1	dochovaný
dobové	dobový	k2eAgInPc1d1	dobový
záznamy	záznam	k1gInPc1	záznam
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
nečeských	český	k2eNgMnPc2d1	nečeský
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
neznali	znát	k5eNaImAgMnP	znát
místní	místní	k2eAgInPc4d1	místní
názvy	název	k1gInPc4	název
a	a	k8xC	a
ani	ani	k8xC	ani
příliš	příliš	k6eAd1	příliš
nekladli	klást	k5eNaImAgMnP	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
podrobný	podrobný	k2eAgInSc4d1	podrobný
popis	popis	k1gInSc4	popis
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
podrobnější	podrobný	k2eAgInSc4d2	podrobnější
popis	popis	k1gInSc4	popis
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
například	například	k6eAd1	například
Jindřich	Jindřich	k1gMnSc1	Jindřich
Fitzsimon	Fitzsimon	k1gMnSc1	Fitzsimon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
viděl	vidět	k5eAaImAgMnS	vidět
krajinu	krajina	k1gFnSc4	krajina
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
svoje	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
okolo	okolo	k7c2	okolo
půlnoci	půlnoc	k1gFnSc2	půlnoc
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Ligistická	ligistický	k2eAgFnSc1d1	ligistická
armáda	armáda	k1gFnSc1	armáda
začala	začít	k5eAaPmAgFnS	začít
zaujímat	zaujímat	k5eAaImF	zaujímat
svoje	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
kolem	kolem	k7c2	kolem
5	[number]	k4	5
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnPc1	hodina
ranní	ranní	k2eAgFnSc1d1	ranní
a	a	k8xC	a
císařská	císařský	k2eAgFnSc1d1	císařská
dorazila	dorazit	k5eAaPmAgFnS	dorazit
s	s	k7c7	s
určitým	určitý	k2eAgNnSc7d1	určité
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
,	,	kIx,	,
snad	snad	k9	snad
kolem	kolem	k7c2	kolem
10	[number]	k4	10
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozestavění	rozestavění	k1gNnPc1	rozestavění
armád	armáda	k1gFnPc2	armáda
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Stavovská	stavovský	k2eAgFnSc1d1	stavovská
armáda	armáda	k1gFnSc1	armáda
====	====	k?	====
</s>
</p>
<p>
<s>
O	o	k7c6	o
vlastním	vlastní	k2eAgNnSc6d1	vlastní
rozestavění	rozestavění	k1gNnSc6	rozestavění
česko-moravské	českooravský	k2eAgFnSc2d1	česko-moravská
armády	armáda	k1gFnSc2	armáda
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
majoritní	majoritní	k2eAgInPc1d1	majoritní
popisy	popis	k1gInPc1	popis
<g/>
,	,	kIx,	,
od	od	k7c2	od
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
Kristiána	Kristián	k1gMnSc2	Kristián
I.	I.	kA	I.
Anhaltského	Anhaltský	k2eAgInSc2d1	Anhaltský
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1621	[number]	k4	1621
a	a	k8xC	a
Fitzsimonův	Fitzsimonův	k2eAgMnSc1d1	Fitzsimonův
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
poněkud	poněkud	k6eAd1	poněkud
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
samozřejmě	samozřejmě	k6eAd1	samozřejmě
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgFnPc2d1	různá
dalších	další	k2eAgFnPc2d1	další
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
významných	významný	k2eAgFnPc2d1	významná
soudobých	soudobý	k2eAgFnPc2d1	soudobá
teorií	teorie	k1gFnPc2	teorie
o	o	k7c6	o
bitvě	bitva	k1gFnSc6	bitva
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Dušana	Dušan	k1gMnSc2	Dušan
Uhlíře	Uhlíř	k1gMnSc2	Uhlíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
rozložilo	rozložit	k5eAaPmAgNnS	rozložit
nedaleko	nedaleko	k7c2	nedaleko
pražských	pražský	k2eAgFnPc2d1	Pražská
hradeb	hradba	k1gFnPc2	hradba
na	na	k7c4	na
návrší	návrší	k1gNnPc4	návrší
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
k	k	k7c3	k
severu	sever	k1gInSc3	sever
a	a	k8xC	a
západu	západ	k1gInSc3	západ
prudce	prudko	k6eAd1	prudko
klesá	klesat	k5eAaImIp3nS	klesat
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
50	[number]	k4	50
výškových	výškový	k2eAgInPc2d1	výškový
metrů	metr	k1gInPc2	metr
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Litovického	Litovický	k2eAgInSc2d1	Litovický
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Pravé	pravý	k2eAgNnSc1d1	pravé
stavovské	stavovský	k2eAgNnSc1d1	Stavovské
křídlo	křídlo	k1gNnSc1	křídlo
se	se	k3xPyFc4	se
opíralo	opírat	k5eAaImAgNnS	opírat
o	o	k7c4	o
vysokou	vysoký	k2eAgFnSc4d1	vysoká
zeď	zeď	k1gFnSc4	zeď
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
obory	obora	k1gFnSc2	obora
u	u	k7c2	u
letohrádku	letohrádek	k1gInSc2	letohrádek
Hvězda	hvězda	k1gFnSc1	hvězda
a	a	k8xC	a
levé	levý	k2eAgNnSc1d1	levé
křídlo	křídlo	k1gNnSc1	křídlo
bylo	být	k5eAaImAgNnS	být
posíleno	posílit	k5eAaPmNgNnS	posílit
dělostřelectvem	dělostřelectvo	k1gNnSc7	dělostřelectvo
<g/>
.	.	kIx.	.
</s>
<s>
Koncepce	koncepce	k1gFnSc1	koncepce
bojové	bojový	k2eAgFnSc2d1	bojová
formace	formace	k1gFnSc2	formace
kterou	který	k3yIgFnSc7	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Anhalt	Anhalt	k1gInSc1	Anhalt
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
pružné	pružný	k2eAgFnSc3d1	pružná
šachovnici	šachovnice	k1gFnSc3	šachovnice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rozestavil	rozestavit	k5eAaPmAgMnS	rozestavit
svoji	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
řad	řada	k1gFnPc2	řada
s	s	k7c7	s
mezerami	mezera	k1gFnPc7	mezera
tak	tak	k9	tak
aby	aby	kYmCp3nP	aby
šiky	šik	k1gInPc1	šik
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
řady	řada	k1gFnSc2	řada
mohly	moct	k5eAaImAgInP	moct
prostoupit	prostoupit	k5eAaPmF	prostoupit
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
potom	potom	k6eAd1	potom
umístil	umístit	k5eAaPmAgMnS	umístit
uherskou	uherský	k2eAgFnSc4d1	uherská
jízdu	jízda	k1gFnSc4	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
praporců	praporec	k1gInPc2	praporec
jeho	jeho	k3xOp3gFnSc2	jeho
jízdy	jízda	k1gFnSc2	jízda
pak	pak	k6eAd1	pak
mělo	mít	k5eAaImAgNnS	mít
předsunutou	předsunutý	k2eAgFnSc4d1	předsunutá
pozici	pozice	k1gFnSc4	pozice
kvůli	kvůli	k7c3	kvůli
pohybu	pohyb	k1gInSc3	pohyb
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
byli	být	k5eAaImAgMnP	být
děleni	dělit	k5eAaImNgMnP	dělit
po	po	k7c6	po
praporcích	praporec	k1gInPc6	praporec
čili	čili	k8xC	čili
kornetech	kornet	k1gInPc6	kornet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tabulkových	tabulkový	k2eAgInPc2d1	tabulkový
počtů	počet	k1gInPc2	počet
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
jízdní	jízdní	k2eAgInSc1d1	jízdní
praporec	praporec	k1gInSc1	praporec
po	po	k7c6	po
100	[number]	k4	100
mužích	muž	k1gMnPc6	muž
a	a	k8xC	a
pěší	pěší	k2eAgInSc4d1	pěší
praporec	praporec	k1gInSc4	praporec
třikrát	třikrát	k6eAd1	třikrát
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císařsko-ligistická	císařskoigistický	k2eAgFnSc1d1	císařsko-ligistický
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
šikovat	šikovat	k5eAaImF	šikovat
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
poněkud	poněkud	k6eAd1	poněkud
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Císařská	císařský	k2eAgFnSc1d1	císařská
armáda	armáda	k1gFnSc1	armáda
====	====	k?	====
</s>
</p>
<p>
<s>
Císařská	císařský	k2eAgFnSc1d1	císařská
část	část	k1gFnSc1	část
armády	armáda	k1gFnSc2	armáda
tvořila	tvořit	k5eAaImAgFnS	tvořit
pravou	pravý	k2eAgFnSc4d1	pravá
část	část	k1gFnSc4	část
uspořádání	uspořádání	k1gNnSc2	uspořádání
a	a	k8xC	a
skládala	skládat	k5eAaImAgFnS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
šiků	šik	k1gInPc2	šik
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
tvořily	tvořit	k5eAaImAgInP	tvořit
dvě	dva	k4xCgFnPc4	dva
pěší	pěší	k2eAgFnPc4d1	pěší
tercie	tercie	k1gFnPc4	tercie
(	(	kIx(	(
<g/>
reálný	reálný	k2eAgInSc1d1	reálný
počet	počet	k1gInSc1	počet
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
kolem	kolem	k7c2	kolem
4000	[number]	k4	4000
<g/>
)	)	kIx)	)
a	a	k8xC	a
napravo	napravo	k6eAd1	napravo
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
valonské	valonský	k2eAgFnPc1d1	valonská
pluky	pluk	k1gInPc4	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
šik	šik	k1gInSc1	šik
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
doplněn	doplnit	k5eAaPmNgInS	doplnit
třemi	tři	k4xCgNnPc7	tři
jízdními	jízdní	k2eAgNnPc7d1	jízdní
křídly	křídlo	k1gNnPc7	křídlo
po	po	k7c4	po
500	[number]	k4	500
mužích	muž	k1gMnPc6	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
řadě	řada	k1gFnSc6	řada
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
tercie	tercie	k1gFnSc1	tercie
"	"	kIx"	"
<g/>
Neapolitánců	Neapolitánec	k1gMnPc2	Neapolitánec
<g/>
"	"	kIx"	"
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
2	[number]	k4	2
500	[number]	k4	500
muži	muž	k1gMnPc7	muž
a	a	k8xC	a
jezdecká	jezdecký	k2eAgNnPc1d1	jezdecké
křídla	křídlo	k1gNnPc1	křídlo
<g/>
:	:	kIx,	:
nalevo	nalevo	k6eAd1	nalevo
550	[number]	k4	550
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
napravo	napravo	k6eAd1	napravo
400	[number]	k4	400
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
posledního	poslední	k2eAgInSc2d1	poslední
šiku	šik	k1gInSc2	šik
tvořily	tvořit	k5eAaImAgFnP	tvořit
opět	opět	k6eAd1	opět
dvě	dva	k4xCgFnPc4	dva
tercie	tercie	k1gFnPc4	tercie
a	a	k8xC	a
jízda	jízda	k1gFnSc1	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prvním	první	k4xOgInSc7	první
až	až	k8xS	až
třetím	třetí	k4xOgInSc7	třetí
šikem	šik	k1gInSc7	šik
se	se	k3xPyFc4	se
pohybovalo	pohybovat	k5eAaImAgNnS	pohybovat
asi	asi	k9	asi
800	[number]	k4	800
kozáků	kozák	k1gInPc2	kozák
<g/>
.	.	kIx.	.
</s>
<s>
Císařské	císařský	k2eAgFnSc3d1	císařská
armádě	armáda	k1gFnSc3	armáda
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
též	též	k9	též
čtyři	čtyři	k4xCgNnPc1	čtyři
děla	dělo	k1gNnPc1	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Šikování	šikování	k1gNnSc4	šikování
provedl	provést	k5eAaPmAgMnS	provést
generál	generál	k1gMnSc1	generál
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
Maxmilián	Maxmiliána	k1gFnPc2	Maxmiliána
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
a	a	k8xC	a
o	o	k7c6	o
velení	velení	k1gNnSc6	velení
se	se	k3xPyFc4	se
dělil	dělit	k5eAaImAgMnS	dělit
s	s	k7c7	s
plukovníkem	plukovník	k1gMnSc7	plukovník
Tiefenbachem	Tiefenbach	k1gMnSc7	Tiefenbach
<g/>
.	.	kIx.	.
</s>
<s>
Císařské	císařský	k2eAgFnPc1d1	císařská
síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
odhadovány	odhadovat	k5eAaImNgFnP	odhadovat
na	na	k7c4	na
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Ligistická	ligistický	k2eAgFnSc1d1	ligistická
armáda	armáda	k1gFnSc1	armáda
====	====	k?	====
</s>
</p>
<p>
<s>
Ligistická	ligistický	k2eAgFnSc1d1	ligistická
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ležela	ležet	k5eAaImAgFnS	ležet
nalevo	nalevo	k6eAd1	nalevo
od	od	k7c2	od
císařské	císařský	k2eAgFnSc2d1	císařská
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
seřazena	seřadit	k5eAaPmNgNnP	seřadit
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
šiků	šik	k1gInPc2	šik
<g/>
.	.	kIx.	.
</s>
<s>
Přáním	přání	k1gNnSc7	přání
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
celé	celý	k2eAgFnSc2d1	celá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc2	Karel
Buquoye	Buquoy	k1gMnSc2	Buquoy
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
první	první	k4xOgInSc1	první
byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgFnPc7	dva
terciemi	tercie	k1gFnPc7	tercie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
první	první	k4xOgFnSc4	první
linii	linie	k1gFnSc4	linie
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
šik	šik	k1gInSc1	šik
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
patnáct	patnáct	k4xCc4	patnáct
kornet	korneta	k1gFnPc2	korneta
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
skládal	skládat	k5eAaImAgMnS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
tercií	tercie	k1gFnPc2	tercie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
pěti	pět	k4xCc7	pět
pluky	pluk	k1gInPc4	pluk
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
šik	šik	k2eAgMnSc1d1	šik
byl	být	k5eAaImAgMnS	být
zbytek	zbytek	k1gInSc4	zbytek
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Ligisté	ligista	k1gMnPc1	ligista
měli	mít	k5eAaImAgMnP	mít
přibližně	přibližně	k6eAd1	přibližně
šest	šest	k4xCc4	šest
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
ligistických	ligistický	k2eAgInPc2d1	ligistický
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
kolem	kolem	k7c2	kolem
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
formace	formace	k1gFnSc1	formace
byla	být	k5eAaImAgFnS	být
podřízena	podřízen	k2eAgFnSc1d1	podřízena
Janu	Jan	k1gMnSc3	Jan
Tserclaesovi	Tserclaes	k1gMnSc3	Tserclaes
Tillymu	Tillym	k1gInSc3	Tillym
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
zajímavostí	zajímavost	k1gFnSc7	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejspíše	nejspíše	k9	nejspíše
této	tento	k3xDgFnSc2	tento
bitvy	bitva	k1gFnSc2	bitva
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
na	na	k7c6	na
katolické	katolický	k2eAgFnSc6d1	katolická
straně	strana	k1gFnSc6	strana
i	i	k8xC	i
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filosof	filosof	k1gMnSc1	filosof
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
úloha	úloha	k1gFnSc1	úloha
však	však	k9	však
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
nijak	nijak	k6eAd1	nijak
významná	významný	k2eAgFnSc1d1	významná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poslední	poslední	k2eAgFnPc1d1	poslední
přípravy	příprava	k1gFnPc1	příprava
===	===	k?	===
</s>
</p>
<p>
<s>
Porady	porada	k1gFnPc1	porada
v	v	k7c6	v
katolickém	katolický	k2eAgInSc6d1	katolický
generálním	generální	k2eAgInSc6d1	generální
štábu	štáb	k1gInSc6	štáb
vedl	vést	k5eAaImAgMnS	vést
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
<g/>
,	,	kIx,	,
od	od	k7c2	od
něho	on	k3xPp3gNnSc2	on
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
počáteční	počáteční	k2eAgFnPc4d1	počáteční
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
dobré	dobrý	k2eAgFnSc2d1	dobrá
pozice	pozice	k1gFnSc2	pozice
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
udělat	udělat	k5eAaPmF	udělat
z	z	k7c2	z
budoucího	budoucí	k2eAgInSc2d1	budoucí
střetu	střet	k1gInSc2	střet
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
bitvu	bitva	k1gFnSc4	bitva
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k0	la
Motte	Mott	k1gMnSc5	Mott
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
z	z	k7c2	z
terénního	terénní	k2eAgInSc2d1	terénní
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgMnS	označit
Anhaltovy	Anhaltův	k2eAgFnPc4d1	Anhaltův
pozice	pozice	k1gFnPc4	pozice
za	za	k7c4	za
slabé	slabý	k2eAgNnSc4d1	slabé
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dodalo	dodat	k5eAaPmAgNnS	dodat
generálnímu	generální	k2eAgInSc3d1	generální
štábu	štáb	k1gInSc3	štáb
nemalé	malý	k2eNgNnSc1d1	nemalé
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
ve	v	k7c6	v
strategicky	strategicky	k6eAd1	strategicky
výhodné	výhodný	k2eAgFnSc6d1	výhodná
pozici	pozice	k1gFnSc6	pozice
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
povstalecké	povstalecký	k2eAgFnSc6d1	povstalecká
armádě	armáda	k1gFnSc6	armáda
panovaly	panovat	k5eAaImAgFnP	panovat
neshody	neshoda	k1gFnPc1	neshoda
a	a	k8xC	a
kompetence	kompetence	k1gFnPc1	kompetence
ve	v	k7c6	v
velení	velení	k1gNnSc6	velení
byly	být	k5eAaImAgInP	být
vymezeny	vymezit	k5eAaPmNgInP	vymezit
nejasně	jasně	k6eNd1	jasně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
také	také	k9	také
bylo	být	k5eAaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
nějak	nějak	k6eAd1	nějak
bitvu	bitva	k1gFnSc4	bitva
nazvat	nazvat	k5eAaPmF	nazvat
a	a	k8xC	a
vybrat	vybrat	k5eAaPmF	vybrat
motto	motto	k1gNnSc4	motto
<g/>
.	.	kIx.	.
</s>
<s>
Katolické	katolický	k2eAgNnSc4d1	katolické
označení	označení	k1gNnSc4	označení
bitvy	bitva	k1gFnSc2	bitva
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Carlo	Carlo	k1gNnSc4	Carlo
Spinelli	Spinelle	k1gFnSc3	Spinelle
a	a	k8xC	a
zní	znět	k5eAaImIp3nS	znět
"	"	kIx"	"
<g/>
veliká	veliký	k2eAgFnSc1d1	veliká
srážka	srážka	k1gFnSc1	srážka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
diskuze	diskuze	k1gFnSc2	diskuze
o	o	k7c6	o
mottu	motto	k1gNnSc6	motto
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
mnich	mnich	k1gMnSc1	mnich
a	a	k8xC	a
bělohorská	bělohorský	k2eAgFnSc1d1	Bělohorská
legenda	legenda	k1gFnSc1	legenda
<g/>
,	,	kIx,	,
karmelitán	karmelitán	k1gMnSc1	karmelitán
Dominik	Dominik	k1gMnSc1	Dominik
à	à	k?	à
Jesu	Jesa	k1gMnSc4	Jesa
Maria	Mario	k1gMnSc4	Mario
a	a	k8xC	a
ukončil	ukončit	k5eAaPmAgMnS	ukončit
ji	on	k3xPp3gFnSc4	on
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Spolehněte	spolehnout	k5eAaPmRp2nP	spolehnout
se	se	k3xPyFc4	se
na	na	k7c4	na
Boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
udeřte	udeřit	k5eAaPmRp2nP	udeřit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
katolické	katolický	k2eAgNnSc1d1	katolické
vojsko	vojsko	k1gNnSc1	vojsko
připravovalo	připravovat	k5eAaImAgNnS	připravovat
na	na	k7c4	na
bitvu	bitva	k1gFnSc4	bitva
jídlem	jídlo	k1gNnSc7	jídlo
a	a	k8xC	a
pitím	pití	k1gNnSc7	pití
<g/>
;	;	kIx,	;
někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
hlasitě	hlasitě	k6eAd1	hlasitě
modlili	modlit	k5eAaImAgMnP	modlit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přijímali	přijímat	k5eAaImAgMnP	přijímat
svátost	svátost	k1gFnSc4	svátost
smíření	smíření	k1gNnSc2	smíření
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Dominik	Dominik	k1gMnSc1	Dominik
jezdil	jezdit	k5eAaImAgMnS	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
a	a	k8xC	a
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
morálku	morálka	k1gFnSc4	morálka
mužů	muž	k1gMnPc2	muž
svoji	svůj	k3xOyFgFnSc4	svůj
ohnivou	ohnivý	k2eAgFnSc4d1	ohnivá
řečí	řeč	k1gFnSc7	řeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
tábor	tábor	k1gInSc1	tábor
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
byl	být	k5eAaImAgMnS	být
tichý	tichý	k2eAgMnSc1d1	tichý
<g/>
,	,	kIx,	,
probíhalo	probíhat	k5eAaImAgNnS	probíhat
zde	zde	k6eAd1	zde
dostavování	dostavování	k1gNnSc1	dostavování
náspů	násep	k1gInPc2	násep
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
postupně	postupně	k6eAd1	postupně
přicházeli	přicházet	k5eAaImAgMnP	přicházet
poslední	poslední	k2eAgMnPc1d1	poslední
velitelé	velitel	k1gMnPc1	velitel
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mnoho	mnoho	k4c1	mnoho
nedorazilo	dorazit	k5eNaPmAgNnS	dorazit
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
z	z	k7c2	z
těch	ten	k3xDgFnPc2	ten
nejprominentnějších	prominentní	k2eAgFnPc2d3	nejprominentnější
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
Fridrich	Fridrich	k1gMnSc1	Fridrich
Falcký	falcký	k2eAgMnSc1d1	falcký
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ani	ani	k9	ani
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bitva	bitva	k1gFnSc1	bitva
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
a	a	k8xC	a
zasedl	zasednout	k5eAaPmAgMnS	zasednout
k	k	k7c3	k
obědu	oběd	k1gInSc3	oběd
s	s	k7c7	s
anglickými	anglický	k2eAgMnPc7d1	anglický
vyslanci	vyslanec	k1gMnPc7	vyslanec
Richardem	Richard	k1gMnSc7	Richard
Westonem	Weston	k1gInSc7	Weston
a	a	k8xC	a
Edwardem	Edward	k1gMnSc7	Edward
Conwayem	Conway	k1gMnSc7	Conway
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
obědě	oběd	k1gInSc6	oběd
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
bojiště	bojiště	k1gNnSc4	bojiště
navštívit	navštívit	k5eAaPmF	navštívit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
již	již	k6eAd1	již
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
ustupující	ustupující	k2eAgFnSc7d1	ustupující
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
bitvy	bitva	k1gFnSc2	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
před	před	k7c7	před
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
bitvou	bitva	k1gFnSc7	bitva
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Buquoy	Buquo	k1gMnPc4	Buquo
jízdu	jízda	k1gFnSc4	jízda
proti	proti	k7c3	proti
uherským	uherský	k2eAgMnPc3d1	uherský
vojákům	voják	k1gMnPc3	voják
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
bezstarostně	bezstarostně	k6eAd1	bezstarostně
spali	spát	k5eAaImAgMnP	spát
<g/>
,	,	kIx,	,
a	a	k8xC	a
nezmohli	zmoct	k5eNaPmAgMnP	zmoct
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
výrazně	výrazně	k6eAd1	výrazně
pozvedlo	pozvednout	k5eAaPmAgNnS	pozvednout
bojovou	bojový	k2eAgFnSc4d1	bojová
morálku	morálka	k1gFnSc4	morálka
císařsko-ligistického	císařskoigistický	k2eAgNnSc2d1	císařsko-ligistický
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
demoralizovalo	demoralizovat	k5eAaBmAgNnS	demoralizovat
vojsko	vojsko	k1gNnSc4	vojsko
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pád	Pád	k1gInSc4	Pád
levého	levý	k2eAgNnSc2d1	levé
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
křídla	křídlo	k1gNnSc2	křídlo
===	===	k?	===
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
začala	začít	k5eAaPmAgFnS	začít
kolem	kolem	k7c2	kolem
poledne	poledne	k1gNnSc2	poledne
<g/>
,	,	kIx,	,
nejspíš	nejspíš	k9	nejspíš
v	v	k7c4	v
půl	půl	k1xP	půl
dvanácté	dvanáctý	k4xOgFnSc2	dvanáctý
<g/>
.	.	kIx.	.
</s>
<s>
Započalo	započnout	k5eAaPmAgNnS	započnout
ji	on	k3xPp3gFnSc4	on
pravé	pravý	k2eAgNnSc1d1	pravé
křídlo	křídlo	k1gNnSc1	křídlo
císařského	císařský	k2eAgInSc2d1	císařský
šiku	šik	k1gInSc2	šik
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
veleli	velet	k5eAaImAgMnP	velet
Gauchier	Gauchier	k1gInSc4	Gauchier
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
Motte	Mott	k1gInSc5	Mott
a	a	k8xC	a
de	de	k?	de
Lacroix	Lacroix	k1gInSc1	Lacroix
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
odraženo	odrazit	k5eAaPmNgNnS	odrazit
Thurnovým	Thurnův	k2eAgInSc7d1	Thurnův
jízdním	jízdní	k2eAgInSc7d1	jízdní
plukem	pluk	k1gInSc7	pluk
a	a	k8xC	a
částí	část	k1gFnSc7	část
Bubnova	Bubnův	k2eAgInSc2d1	Bubnův
jízdního	jízdní	k2eAgInSc2d1	jízdní
pluku	pluk	k1gInSc2	pluk
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Iselteina	Iselteino	k1gNnSc2	Iselteino
<g/>
.	.	kIx.	.
</s>
<s>
Stavovská	stavovský	k2eAgFnSc1d1	stavovská
jízda	jízda	k1gFnSc1	jízda
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
dále	daleko	k6eAd2	daleko
a	a	k8xC	a
těžce	těžce	k6eAd1	těžce
udeřila	udeřit	k5eAaPmAgFnS	udeřit
na	na	k7c4	na
tercii	tercie	k1gFnSc4	tercie
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
však	však	k9	však
selhaly	selhat	k5eAaPmAgFnP	selhat
okolní	okolní	k2eAgFnPc1d1	okolní
stavovské	stavovský	k2eAgFnPc1d1	stavovská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
útok	útok	k1gInSc1	útok
nepodpořily	podpořit	k5eNaPmAgFnP	podpořit
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
opět	opět	k6eAd1	opět
využili	využít	k5eAaPmAgMnP	využít
císařští	císařský	k2eAgMnPc1d1	císařský
<g/>
,	,	kIx,	,
posíleni	posílen	k2eAgMnPc1d1	posílen
jízdním	jízdní	k2eAgInSc7d1	jízdní
plukem	pluk	k1gInSc7	pluk
pana	pan	k1gMnSc2	pan
z	z	k7c2	z
Meggau	Meggaus	k1gInSc2	Meggaus
a	a	k8xC	a
valonskou	valonský	k2eAgFnSc7d1	valonská
jízdou	jízda	k1gFnSc7	jízda
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
napadli	napadnout	k5eAaPmAgMnP	napadnout
levé	levý	k2eAgNnSc4d1	levé
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
přispěchal	přispěchat	k5eAaPmAgMnS	přispěchat
Streif	Streif	k1gMnSc1	Streif
vedoucí	vedoucí	k1gMnSc1	vedoucí
Anhaltovu	Anhaltův	k2eAgFnSc4d1	Anhaltův
jízdu	jízda	k1gFnSc4	jízda
a	a	k8xC	a
baron	baron	k1gMnSc1	baron
Hofkirch	Hofkirch	k1gMnSc1	Hofkirch
vedoucí	vedoucí	k1gMnSc1	vedoucí
dolnorakouskou	dolnorakouský	k2eAgFnSc4d1	Dolnorakouská
jízdu	jízda	k1gFnSc4	jízda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
co	co	k3yQnSc4	co
byl	být	k5eAaImAgInS	být
Hofkirch	Hofkirch	k1gInSc1	Hofkirch
zabit	zabít	k5eAaPmNgInS	zabít
<g/>
,	,	kIx,	,
dali	dát	k5eAaPmAgMnP	dát
se	se	k3xPyFc4	se
Rakušané	Rakušan	k1gMnPc1	Rakušan
na	na	k7c4	na
ústup	ústup	k1gInSc4	ústup
a	a	k8xC	a
strhli	strhnout	k5eAaPmAgMnP	strhnout
spolu	spolu	k6eAd1	spolu
i	i	k8xC	i
Anhaltovce	Anhaltovka	k1gFnSc3	Anhaltovka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
boje	boj	k1gInSc2	boj
také	také	k9	také
nastoupily	nastoupit	k5eAaPmAgInP	nastoupit
čtyři	čtyři	k4xCgInPc1	čtyři
kornety	kornet	k1gInPc1	kornet
slezské	slezský	k2eAgFnSc2d1	Slezská
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svojí	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
od	od	k7c2	od
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
nenabyly	nabýt	k5eNaPmAgFnP	nabýt
významu	význam	k1gInSc3	význam
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začala	začít	k5eAaPmAgFnS	začít
ustupovat	ustupovat	k5eAaImF	ustupovat
i	i	k9	i
Anhaltova	Anhaltův	k2eAgFnSc1d1	Anhaltův
pěchota	pěchota	k1gFnSc1	pěchota
a	a	k8xC	a
ve	v	k7c6	v
čtvrt	čtvrt	k1xP	čtvrt
hodině	hodina	k1gFnSc6	hodina
bylo	být	k5eAaImAgNnS	být
levé	levý	k2eAgNnSc4d1	levé
křídlo	křídlo	k1gNnSc4	křídlo
první	první	k4xOgFnSc2	první
řady	řada	k1gFnSc2	řada
poraženo	poražen	k2eAgNnSc1d1	poraženo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
císařská	císařský	k2eAgFnSc1d1	císařská
pěchota	pěchota	k1gFnSc1	pěchota
na	na	k7c6	na
pochodu	pochod	k1gInSc6	pochod
podporována	podporovat	k5eAaImNgFnS	podporovat
Dampierrovou	Dampierrův	k2eAgFnSc7d1	Dampierrův
jízdou	jízda	k1gFnSc7	jízda
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
řady	řada	k1gFnSc2	řada
a	a	k8xC	a
volnou	volný	k2eAgFnSc7d1	volná
jednotkou	jednotka	k1gFnSc7	jednotka
kozáků	kozák	k1gInPc2	kozák
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
uherská	uherský	k2eAgFnSc1d1	uherská
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosud	dosud	k6eAd1	dosud
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
a	a	k8xC	a
do	do	k7c2	do
boje	boj	k1gInSc2	boj
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
ustupovat	ustupovat	k5eAaImF	ustupovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
pád	pád	k1gInSc4	pád
celého	celý	k2eAgNnSc2d1	celé
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
levého	levý	k2eAgNnSc2d1	levé
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
akce	akce	k1gFnSc1	akce
Kristiána	Kristián	k1gMnSc2	Kristián
mladšího	mladý	k2eAgMnSc2d2	mladší
Anhaltského	Anhaltský	k2eAgMnSc2d1	Anhaltský
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
–	–	k?	–
podle	podle	k7c2	podle
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
tradičních	tradiční	k2eAgFnPc2d1	tradiční
doktrín	doktrína	k1gFnPc2	doktrína
znamenala	znamenat	k5eAaImAgFnS	znamenat
prohranou	prohraný	k2eAgFnSc4d1	prohraná
bitvu	bitva	k1gFnSc4	bitva
–	–	k?	–
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
podařilo	podařit	k5eAaPmAgNnS	podařit
obrátit	obrátit	k5eAaPmF	obrátit
kartu	karta	k1gFnSc4	karta
mladému	mladý	k1gMnSc3	mladý
princi	princ	k1gMnSc3	princ
Kristiánu	Kristián	k1gMnSc3	Kristián
Anhaltskému	Anhaltský	k2eAgMnSc3d1	Anhaltský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
velel	velet	k5eAaImAgMnS	velet
devíti	devět	k4xCc3	devět
kornetám	korneta	k1gFnPc3	korneta
pluku	pluk	k1gInSc2	pluk
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
druhé	druhý	k4xOgFnSc2	druhý
řady	řada	k1gFnSc2	řada
a	a	k8xC	a
který	který	k3yRgInSc1	který
velel	velet	k5eAaImAgInS	velet
k	k	k7c3	k
protiútoku	protiútok	k1gInSc3	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
pokusu	pokus	k1gInSc6	pokus
byl	být	k5eAaImAgInS	být
podpořen	podpořit	k5eAaPmNgInS	podpořit
Kaplířovým	Kaplířův	k2eAgInSc7d1	Kaplířův
plukem	pluk	k1gInSc7	pluk
<g/>
,	,	kIx,	,
Stubenvollovou	Stubenvollův	k2eAgFnSc7d1	Stubenvollův
moravskou	moravský	k2eAgFnSc7d1	Moravská
jízdou	jízda	k1gFnSc7	jízda
a	a	k8xC	a
Mansfeldovou	Mansfeldův	k2eAgFnSc7d1	Mansfeldova
jízdou	jízda	k1gFnSc7	jízda
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
pluku	pluk	k1gInSc2	pluk
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
plukovníka	plukovník	k1gMnSc2	plukovník
Styruma	Styrum	k1gMnSc2	Styrum
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
veden	vést	k5eAaImNgInS	vést
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
tzv.	tzv.	kA	tzv.
německou	německý	k2eAgFnSc4d1	německá
císařskou	císařský	k2eAgFnSc4d1	císařská
tercii	tercie	k1gFnSc4	tercie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
předchozích	předchozí	k2eAgFnPc6d1	předchozí
akcích	akce	k1gFnPc6	akce
odhalená	odhalený	k2eAgFnSc1d1	odhalená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
zprvu	zprvu	k6eAd1	zprvu
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
,	,	kIx,	,
ukořistili	ukořistit	k5eAaPmAgMnP	ukořistit
praporec	praporec	k1gInSc4	praporec
z	z	k7c2	z
pravého	pravý	k2eAgNnSc2d1	pravé
jízdního	jízdní	k2eAgNnSc2d1	jízdní
křídla	křídlo	k1gNnSc2	křídlo
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
tercie	tercie	k1gFnSc2	tercie
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
zajali	zajmout	k5eAaPmAgMnP	zajmout
jejího	její	k3xOp3gMnSc4	její
velitele	velitel	k1gMnSc4	velitel
plukovníka	plukovník	k1gMnSc4	plukovník
Breunera	Breuner	k1gMnSc4	Breuner
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
útok	útok	k1gInSc1	útok
mladého	mladý	k2eAgMnSc2d1	mladý
prince	princ	k1gMnSc2	princ
na	na	k7c4	na
citlivé	citlivý	k2eAgNnSc4d1	citlivé
místo	místo	k1gNnSc4	místo
při	při	k7c6	při
styku	styk	k1gInSc6	styk
císařské	císařský	k2eAgFnSc2d1	císařská
a	a	k8xC	a
ligistické	ligistický	k2eAgFnSc2d1	ligistická
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
před	před	k7c7	před
sebou	se	k3xPyFc7	se
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
Löblovu	Löblův	k2eAgFnSc4d1	Löblův
jízdu	jízda	k1gFnSc4	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
mu	on	k3xPp3gNnSc3	on
také	také	k9	také
pomohly	pomoct	k5eAaPmAgFnP	pomoct
uherské	uherský	k2eAgFnPc1d1	uherská
jednotky	jednotka	k1gFnPc1	jednotka
z	z	k7c2	z
pravého	pravý	k2eAgNnSc2d1	pravé
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
křídla	křídlo	k1gNnSc2	křídlo
a	a	k8xC	a
situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
vážná	vážný	k2eAgFnSc1d1	vážná
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
zraněný	zraněný	k1gMnSc1	zraněný
Buquoy	Buquoa	k1gMnSc2	Buquoa
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
sedlat	sedlat	k5eAaImF	sedlat
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
to	ten	k3xDgNnSc1	ten
již	již	k9	již
začínal	začínat	k5eAaImAgInS	začínat
Anhalt	Anhalt	k1gInSc1	Anhalt
potřebovat	potřebovat	k5eAaImF	potřebovat
pomoc	pomoc	k1gFnSc4	pomoc
pěchoty	pěchota	k1gFnSc2	pěchota
a	a	k8xC	a
císařské	císařský	k2eAgFnPc1d1	císařská
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
navracely	navracet	k5eAaBmAgFnP	navracet
z	z	k7c2	z
pronásledování	pronásledování	k1gNnSc2	pronásledování
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Anhalta	Anhalt	k1gMnSc4	Anhalt
začala	začít	k5eAaPmAgFnS	začít
dotírat	dotírat	k5eAaImF	dotírat
Dampierrova	Dampierrův	k2eAgFnSc1d1	Dampierrův
i	i	k8xC	i
Löblova	Löblův	k2eAgFnSc1d1	Löblův
jízda	jízda	k1gFnSc1	jízda
<g/>
,	,	kIx,	,
Cratz	Cratz	k1gInSc1	Cratz
s	s	k7c7	s
bavorským	bavorský	k2eAgInSc7d1	bavorský
plukem	pluk	k1gInSc7	pluk
a	a	k8xC	a
Verdugo	Verdugo	k1gMnSc1	Verdugo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Hohenlohe	Hohenlohat	k5eAaPmIp3nS	Hohenlohat
i	i	k9	i
starý	starý	k2eAgInSc1d1	starý
Anhalt	Anhalt	k1gInSc1	Anhalt
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
dostat	dostat	k5eAaPmF	dostat
svoji	svůj	k3xOyFgFnSc4	svůj
pěchotu	pěchota	k1gFnSc4	pěchota
k	k	k7c3	k
mladému	mladý	k2eAgMnSc3d1	mladý
Anhaltovi	Anhalt	k1gMnSc3	Anhalt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc4	tento
poslední	poslední	k2eAgInSc4d1	poslední
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
zvrácení	zvrácení	k1gNnSc4	zvrácení
vítězství	vítězství	k1gNnSc2	vítězství
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
stavům	stav	k1gInPc3	stav
nevyšel	vyjít	k5eNaPmAgInS	vyjít
–	–	k?	–
Anhalt	Anhalt	k1gInSc4	Anhalt
mladší	mladý	k2eAgFnSc2d2	mladší
byl	být	k5eAaImAgMnS	být
obklíčen	obklíčit	k5eAaPmNgMnS	obklíčit
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
zajat	zajat	k2eAgMnSc1d1	zajat
dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
pěchota	pěchota	k1gFnSc1	pěchota
dorazila	dorazit	k5eAaPmAgFnS	dorazit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Závěr	závěr	k1gInSc1	závěr
===	===	k?	===
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nejvíce	nejvíce	k6eAd1	nejvíce
diskutovanou	diskutovaný	k2eAgFnSc7d1	diskutovaná
částí	část	k1gFnSc7	část
bitvy	bitva	k1gFnSc2	bitva
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
díky	díky	k7c3	díky
Jindřichu	Jindřich	k1gMnSc3	Jindřich
Fitzsimonovi	Fitzsimon	k1gMnSc3	Fitzsimon
nadcházející	nadcházející	k2eAgFnSc1d1	nadcházející
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
část	část	k1gFnSc1	část
útoku	útok	k1gInSc2	útok
katolických	katolický	k2eAgFnPc2d1	katolická
sil	síla	k1gFnPc2	síla
na	na	k7c4	na
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
stavovské	stavovský	k2eAgFnSc2d1	stavovská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Buquoyův	Buquoyův	k2eAgMnSc1d1	Buquoyův
irský	irský	k2eAgMnSc1d1	irský
zpovědník	zpovědník	k1gMnSc1	zpovědník
totiž	totiž	k9	totiž
tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
popsal	popsat	k5eAaPmAgMnS	popsat
mírně	mírně	k6eAd1	mírně
tendenčně	tendenčně	k6eAd1	tendenčně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
katolické	katolický	k2eAgNnSc4d1	katolické
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
také	také	k9	také
známa	známo	k1gNnSc2	známo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
především	především	k6eAd1	především
jednotky	jednotka	k1gFnPc1	jednotka
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
zemí	zem	k1gFnPc2	zem
najaté	najatý	k2eAgInPc4d1	najatý
za	za	k7c4	za
peníze	peníz	k1gInPc4	peníz
moravských	moravský	k2eAgInPc2d1	moravský
stavů	stav	k1gInPc2	stav
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Šlika	šlika	k1gFnSc1	šlika
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
částí	část	k1gFnSc7	část
Thurnova	Thurnův	k2eAgInSc2d1	Thurnův
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Františka	František	k1gMnSc2	František
Bernarda	Bernard	k1gMnSc2	Bernard
z	z	k7c2	z
Thurnu	Thurn	k1gInSc2	Thurn
<g/>
.	.	kIx.	.
</s>
<s>
Thurnovi	Thurn	k1gMnSc3	Thurn
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Šlikovi	Šlikův	k2eAgMnPc1d1	Šlikův
již	již	k6eAd1	již
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fitzsimon	Fitzsimon	k1gMnSc1	Fitzsimon
popsal	popsat	k5eAaPmAgMnS	popsat
střet	střet	k1gInSc4	střet
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vedl	vést	k5eAaImAgMnS	vést
Spinelliho	Spinelli	k1gMnSc4	Spinelli
tercii	tercie	k1gFnSc4	tercie
<g/>
,	,	kIx,	,
Löblovu	Löblův	k2eAgFnSc4d1	Löblův
jízdu	jízda	k1gFnSc4	jízda
a	a	k8xC	a
Marradasův	Marradasův	k2eAgInSc4d1	Marradasův
pluk	pluk	k1gInSc4	pluk
proti	proti	k7c3	proti
pravému	pravý	k2eAgNnSc3d1	pravé
křídlu	křídlo	k1gNnSc3	křídlo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hrdinné	hrdinný	k2eAgNnSc1d1	hrdinné
obětování	obětování	k1gNnSc1	obětování
statečných	statečný	k2eAgMnPc2d1	statečný
Moravanů	Moravan	k1gMnPc2	Moravan
u	u	k7c2	u
hradební	hradební	k2eAgFnSc2d1	hradební
zdi	zeď	k1gFnSc2	zeď
do	do	k7c2	do
posledního	poslední	k2eAgMnSc2d1	poslední
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jako	jako	k9	jako
těžce	těžce	k6eAd1	těžce
vybojované	vybojovaný	k2eAgNnSc4d1	vybojované
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Pravdou	pravda	k1gFnSc7	pravda
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
moravské	moravský	k2eAgInPc1d1	moravský
pluky	pluk	k1gInPc1	pluk
měly	mít	k5eAaImAgInP	mít
poněkud	poněkud	k6eAd1	poněkud
větší	veliký	k2eAgFnPc1d2	veliký
ztráty	ztráta	k1gFnPc1	ztráta
než	než	k8xS	než
jiné	jiný	k2eAgInPc1d1	jiný
oddíly	oddíl	k1gInPc1	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc1	kolik
mužů	muž	k1gMnPc2	muž
vlastně	vlastně	k9	vlastně
padlo	padnout	k5eAaPmAgNnS	padnout
a	a	k8xC	a
kolik	kolik	k4yRc1	kolik
jich	on	k3xPp3gMnPc2	on
kapitulovalo	kapitulovat	k5eAaBmAgNnS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
hrabě	hrabě	k1gMnSc1	hrabě
Šlik	šlika	k1gFnPc2	šlika
byl	být	k5eAaImAgMnS	být
zajat	zajat	k2eAgMnSc1d1	zajat
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
pluk	pluk	k1gInSc1	pluk
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgFnSc1d1	další
zvláštnost	zvláštnost	k1gFnSc1	zvláštnost
z	z	k7c2	z
konce	konec	k1gInSc2	konec
bitvy	bitva	k1gFnSc2	bitva
je	být	k5eAaImIp3nS	být
osud	osud	k1gInSc1	osud
stavovských	stavovský	k2eAgFnPc2d1	stavovská
záloh	záloha	k1gFnPc2	záloha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
v	v	k7c6	v
oboře	obora	k1gFnSc6	obora
letohrádku	letohrádek	k1gInSc2	letohrádek
Hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Zálohy	záloha	k1gFnPc1	záloha
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
Anhaltovu	Anhaltův	k2eAgFnSc4d1	Anhaltův
pěchotu	pěchota	k1gFnSc4	pěchota
<g/>
,	,	kIx,	,
výmarský	výmarský	k2eAgInSc4d1	výmarský
půlpluk	půlpluk	k1gInSc4	půlpluk
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
praporce	praporec	k1gInPc4	praporec
královské	královský	k2eAgFnSc2d1	královská
gardy	garda	k1gFnSc2	garda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
záloha	záloha	k1gFnSc1	záloha
vyčkávala	vyčkávat	k5eAaImAgFnS	vyčkávat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
bitvy	bitva	k1gFnSc2	bitva
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
když	když	k8xS	když
boje	boj	k1gInPc1	boj
probíhaly	probíhat	k5eAaImAgInP	probíhat
před	před	k7c7	před
zdmi	zeď	k1gFnPc7	zeď
obory	obora	k1gFnSc2	obora
–	–	k?	–
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
úplně	úplně	k6eAd1	úplně
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
zakončení	zakončení	k1gNnSc1	zakončení
bitvy	bitva	k1gFnSc2	bitva
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
první	první	k4xOgFnSc3	první
hodině	hodina	k1gFnSc3	hodina
odpolední	odpolední	k1gNnSc2	odpolední
bylo	být	k5eAaImAgNnS	být
dobojováno	dobojován	k2eAgNnSc1d1	dobojováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ztráty	ztráta	k1gFnSc2	ztráta
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
padlo	padnout	k5eAaImAgNnS	padnout
v	v	k7c6	v
řádném	řádný	k2eAgInSc6d1	řádný
boji	boj	k1gInSc6	boj
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k4c1	málo
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
vojska	vojsko	k1gNnSc2	vojsko
však	však	k8xC	však
bylo	být	k5eAaImAgNnS	být
pobito	pobít	k5eAaPmNgNnS	pobít
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
pěchoty	pěchota	k1gFnSc2	pěchota
a	a	k8xC	a
jízdy	jízda	k1gFnSc2	jízda
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
u	u	k7c2	u
letohrádku	letohrádek	k1gInSc2	letohrádek
Hvězda	Hvězda	k1gMnSc1	Hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
padlých	padlý	k1gMnPc2	padlý
mohl	moct	k5eAaImAgInS	moct
tedy	tedy	k9	tedy
být	být	k5eAaImF	být
kolem	kolem	k7c2	kolem
5	[number]	k4	5
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
dal	dát	k5eAaPmAgInS	dát
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
mrtvoly	mrtvola	k1gFnSc2	mrtvola
pochovat	pochovat	k5eAaPmF	pochovat
<g/>
,	,	kIx,	,
napočítal	napočítat	k5eAaPmAgMnS	napočítat
jen	jen	k9	jen
1	[number]	k4	1
600	[number]	k4	600
padlých	padlý	k1gMnPc2	padlý
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Fitzsimon	Fitzsimon	k1gInSc1	Fitzsimon
<g/>
,	,	kIx,	,
však	však	k9	však
udávají	udávat	k5eAaImIp3nP	udávat
počet	počet	k1gInSc4	počet
zabitých	zabitý	k2eAgFnPc2d1	zabitá
až	až	k9	až
na	na	k7c4	na
9	[number]	k4	9
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
1	[number]	k4	1
600	[number]	k4	600
je	být	k5eAaImIp3nS	být
zajisté	zajisté	k9	zajisté
příliš	příliš	k6eAd1	příliš
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
uváží	uvážet	k5eAaImIp3nS	uvážet
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc1	kolik
vojáků	voják	k1gMnPc2	voják
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
a	a	k8xC	a
co	co	k9	co
jich	on	k3xPp3gInPc2	on
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
u	u	k7c2	u
Hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
nepočítajíc	nepočítajíc	k7c4	nepočítajíc
utopené	utopený	k1gMnPc4	utopený
ve	v	k7c6	v
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
ztratili	ztratit	k5eAaPmAgMnP	ztratit
všechna	všechen	k3xTgNnPc4	všechen
svá	svůj	k3xOyFgNnPc4	svůj
děla	dělo	k1gNnPc4	dělo
a	a	k8xC	a
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
prchající	prchající	k2eAgMnPc1d1	prchající
žoldnéři	žoldnér	k1gMnPc1	žoldnér
své	svůj	k3xOyFgFnPc4	svůj
zbraně	zbraň	k1gFnPc4	zbraň
zahazovali	zahazovat	k5eAaImAgMnP	zahazovat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
asi	asi	k9	asi
100	[number]	k4	100
praporců	praporec	k1gInPc2	praporec
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc2	množství
střeliva	střelivo	k1gNnSc2	střelivo
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
svých	svůj	k3xOyFgNnPc2	svůj
zavazadel	zavazadlo	k1gNnPc2	zavazadlo
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
zásobovací	zásobovací	k2eAgInPc1d1	zásobovací
vozy	vůz	k1gInPc1	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
ukořistili	ukořistit	k5eAaPmAgMnP	ukořistit
také	také	k9	také
asi	asi	k9	asi
5	[number]	k4	5
000	[number]	k4	000
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přípravy	příprava	k1gFnSc2	příprava
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
byly	být	k5eAaImAgFnP	být
vykonány	vykonat	k5eAaPmNgFnP	vykonat
ve	v	k7c6	v
spěchu	spěch	k1gInSc6	spěch
<g/>
,	,	kIx,	,
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
potom	potom	k6eAd1	potom
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
získali	získat	k5eAaPmAgMnP	získat
mnoho	mnoho	k6eAd1	mnoho
velmi	velmi	k6eAd1	velmi
cenné	cenný	k2eAgFnPc4d1	cenná
kořisti	kořist	k1gFnPc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
prestižní	prestižní	k2eAgFnSc4d1	prestižní
kořist	kořist	k1gFnSc4	kořist
byly	být	k5eAaImAgFnP	být
zdobené	zdobený	k2eAgFnPc1d1	zdobená
insignie	insignie	k1gFnPc1	insignie
Podvazkového	podvazkový	k2eAgInSc2d1	podvazkový
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
král	král	k1gMnSc1	král
Fridrich	Fridrich	k1gMnSc1	Fridrich
obdržel	obdržet	k5eAaPmAgInS	obdržet
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
tchána	tchán	k1gMnSc2	tchán
krále	král	k1gMnSc4	král
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
Možná	možná	k9	možná
ještě	ještě	k9	ještě
významnější	významný	k2eAgMnSc1d2	významnější
byl	být	k5eAaImAgInS	být
nález	nález	k1gInSc1	nález
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
královského	královský	k2eAgInSc2d1	královský
archivu	archiv	k1gInSc2	archiv
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spěchu	spěch	k1gInSc6	spěch
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
málem	málem	k6eAd1	málem
zapomněla	zapomnět	k5eAaImAgFnS	zapomnět
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
malého	malý	k2eAgMnSc4d1	malý
syna	syn	k1gMnSc4	syn
Ruprechta	Ruprecht	k1gMnSc4	Ruprecht
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
Fitzsimon	Fitzsimon	k1gMnSc1	Fitzsimon
nadsadil	nadsadit	k5eAaPmAgMnS	nadsadit
počet	počet	k1gInSc4	počet
padlých	padlý	k1gMnPc2	padlý
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
katolické	katolický	k2eAgFnSc6d1	katolická
straně	strana	k1gFnSc6	strana
tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
zase	zase	k9	zase
podhodnotil	podhodnotit	k5eAaPmAgInS	podhodnotit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
nejlépe	dobře	k6eAd3	dobře
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
když	když	k8xS	když
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
císařské	císařský	k2eAgNnSc1d1	císařské
a	a	k8xC	a
ligistické	ligistický	k2eAgNnSc1d1	ligistický
padlo	padnout	k5eAaImAgNnS	padnout
pouze	pouze	k6eAd1	pouze
250	[number]	k4	250
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
zprávě	zpráva	k1gFnSc6	zpráva
o	o	k7c6	o
bitvě	bitva	k1gFnSc6	bitva
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
zaznamenána	zaznamenán	k2eAgFnSc1d1	zaznamenána
podrobnost	podrobnost	k1gFnSc1	podrobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
Breunerova	Breunerův	k2eAgInSc2d1	Breunerův
pluku	pluk	k1gInSc2	pluk
padl	padnout	k5eAaPmAgMnS	padnout
praporečník	praporečník	k1gMnSc1	praporečník
Platte	Platt	k1gInSc5	Platt
a	a	k8xC	a
500	[number]	k4	500
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
zpráva	zpráva	k1gFnSc1	zpráva
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
císařské	císařský	k2eAgNnSc1d1	císařské
a	a	k8xC	a
ligistické	ligistický	k2eAgNnSc1d1	ligistický
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
2	[number]	k4	2
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
věrohodnější	věrohodný	k2eAgFnSc1d2	věrohodnější
než	než	k8xS	než
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
ztrátě	ztráta	k1gFnSc6	ztráta
250	[number]	k4	250
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Události	událost	k1gFnPc1	událost
===	===	k?	===
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
Fridrich	Fridrich	k1gMnSc1	Fridrich
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
se	s	k7c7	s
svoji	svůj	k3xOyFgFnSc4	svůj
družinou	družin	k2eAgFnSc7d1	družina
na	na	k7c4	na
bojiště	bojiště	k1gNnSc4	bojiště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
domu	dům	k1gInSc2	dům
U	u	k7c2	u
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
koule	koule	k1gFnSc2	koule
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
knížetem	kníže	k1gMnSc7	kníže
Kristiánem	Kristián	k1gMnSc7	Kristián
z	z	k7c2	z
Anhaltu	Anhalt	k1gInSc2	Anhalt
<g/>
,	,	kIx,	,
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Matyášem	Matyáš	k1gMnSc7	Matyáš
Thurnem	Thurno	k1gNnSc7	Thurno
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Fridrichem	Fridrich	k1gMnSc7	Fridrich
z	z	k7c2	z
Hohenlohe	Hohenloh	k1gInSc2	Hohenloh
na	na	k7c6	na
uřícených	uřícený	k2eAgMnPc6d1	uřícený
koních	kůň	k1gMnPc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
zadrželi	zadržet	k5eAaPmAgMnP	zadržet
královskou	královský	k2eAgFnSc4d1	královská
družinu	družina	k1gFnSc4	družina
a	a	k8xC	a
sdělili	sdělit	k5eAaPmAgMnP	sdělit
smutnou	smutný	k2eAgFnSc4d1	smutná
novinu	novina	k1gFnSc4	novina
o	o	k7c6	o
prohrané	prohraný	k2eAgFnSc6d1	prohraná
bitvě	bitva	k1gFnSc6	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
dojel	dojet	k5eAaPmAgMnS	dojet
k	k	k7c3	k
bráně	brána	k1gFnSc3	brána
a	a	k8xC	a
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
hradby	hradba	k1gFnPc1	hradba
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jízda	jízda	k1gFnSc1	jízda
uháněla	uhánět	k5eAaImAgFnS	uhánět
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
k	k	k7c3	k
vrchu	vrch	k1gInSc3	vrch
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc4	Vavřinec
a	a	k8xC	a
níže	nízce	k6eAd2	nízce
přes	přes	k7c4	přes
vinohrady	vinohrad	k1gInPc4	vinohrad
<g/>
,	,	kIx,	,
vozová	vozový	k2eAgFnSc1d1	vozová
cesta	cesta	k1gFnSc1	cesta
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
od	od	k7c2	od
Bílé	bílý	k2eAgFnSc2d1	bílá
hory	hora	k1gFnSc2	hora
ke	k	k7c3	k
Strahovské	strahovský	k2eAgFnSc3d1	Strahovská
bráně	brána	k1gFnSc3	brána
na	na	k7c6	na
Pohořelci	Pohořelec	k1gInSc6	Pohořelec
byla	být	k5eAaImAgFnS	být
ujíždějícími	ujíždějící	k2eAgInPc7d1	ujíždějící
a	a	k8xC	a
převrácenými	převrácený	k2eAgInPc7d1	převrácený
zásobními	zásobní	k2eAgInPc7d1	zásobní
vozy	vůz	k1gInPc7	vůz
zatarasena	zatarasit	k5eAaPmNgFnS	zatarasit
<g/>
;	;	kIx,	;
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
utíkalo	utíkat	k5eAaImAgNnS	utíkat
pěší	pěší	k2eAgNnSc1d1	pěší
vojsko	vojsko	k1gNnSc1	vojsko
s	s	k7c7	s
pobíhajícími	pobíhající	k2eAgMnPc7d1	pobíhající
koňmi	kůň	k1gMnPc7	kůň
po	po	k7c6	po
širém	širý	k2eAgNnSc6d1	širé
poli	pole	k1gNnSc6	pole
jako	jako	k8xC	jako
o	o	k7c4	o
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
hnala	hnát	k5eAaImAgFnS	hnát
pronásledující	pronásledující	k2eAgFnSc1d1	pronásledující
císařská	císařský	k2eAgFnSc1d1	císařská
jízda	jízda	k1gFnSc1	jízda
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
co	co	k3yInSc4	co
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
hradbám	hradba	k1gFnPc3	hradba
se	se	k3xPyFc4	se
je	on	k3xPp3gNnSc4	on
snažili	snažit	k5eAaImAgMnP	snažit
přelézt	přelézt	k5eAaPmF	přelézt
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bitva	bitva	k1gFnSc1	bitva
je	být	k5eAaImIp3nS	být
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Ondřeje	Ondřej	k1gMnSc2	Ondřej
z	z	k7c2	z
Habernfeldu	Habernfeld	k1gInSc2	Habernfeld
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
krále	král	k1gMnSc2	král
otevřena	otevřen	k2eAgFnSc1d1	otevřena
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
prchající	prchající	k2eAgMnPc1d1	prchající
mohli	moct	k5eAaImAgMnP	moct
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Kristián	Kristián	k1gMnSc1	Kristián
starší	starší	k1gMnSc1	starší
z	z	k7c2	z
Anhaltu	Anhalt	k1gInSc2	Anhalt
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
prchající	prchající	k2eAgMnPc4d1	prchající
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uposlechlo	uposlechnout	k5eAaPmAgNnS	uposlechnout
ho	on	k3xPp3gMnSc4	on
jen	jen	k9	jen
šest	šest	k4xCc1	šest
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
král	král	k1gMnSc1	král
poslal	poslat	k5eAaPmAgMnS	poslat
ihned	ihned	k6eAd1	ihned
svého	svůj	k3xOyFgMnSc4	svůj
štalmistra	štalmistr	k1gMnSc4	štalmistr
Obentrauta	Obentraut	k1gMnSc4	Obentraut
ke	k	k7c3	k
královně	královna	k1gFnSc3	královna
Alžbětě	Alžběta	k1gFnSc3	Alžběta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odebrala	odebrat	k5eAaPmAgFnS	odebrat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
také	také	k9	také
jel	jet	k5eAaImAgMnS	jet
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
nemohla	moct	k5eNaImAgFnS	moct
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
najednou	najednou	k6eAd1	najednou
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
;	;	kIx,	;
když	když	k8xS	když
však	však	k9	však
viděla	vidět	k5eAaImAgFnS	vidět
po	po	k7c6	po
chvilce	chvilka	k1gFnSc6	chvilka
přijíždět	přijíždět	k5eAaImF	přijíždět
krále	král	k1gMnPc4	král
samého	samý	k3xTgInSc2	samý
s	s	k7c7	s
předními	přední	k2eAgMnPc7d1	přední
veliteli	velitel	k1gMnPc7	velitel
a	a	k8xC	a
slyšela	slyšet	k5eAaImAgFnS	slyšet
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
porážce	porážka	k1gFnSc6	porážka
<g/>
,	,	kIx,	,
pocítila	pocítit	k5eAaPmAgFnS	pocítit
při	při	k7c6	při
vší	všecek	k3xTgFnSc6	všecek
své	svůj	k3xOyFgFnSc6	svůj
srdnatosti	srdnatost	k1gFnSc6	srdnatost
tíhu	tíha	k1gFnSc4	tíha
celého	celý	k2eAgNnSc2d1	celé
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spěchu	spěch	k1gInSc6	spěch
sbalili	sbalit	k5eAaPmAgMnP	sbalit
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
klenoty	klenot	k1gInPc4	klenot
a	a	k8xC	a
svatováclavský	svatováclavský	k2eAgInSc4d1	svatováclavský
archiv	archiv	k1gInSc4	archiv
<g/>
,	,	kIx,	,
a	a	k8xC	a
král	král	k1gMnSc1	král
vsedl	vsednout	k5eAaPmAgMnS	vsednout
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
královnou	královna	k1gFnSc7	královna
i	i	k8xC	i
dvořanstvem	dvořanstvo	k1gNnSc7	dvořanstvo
do	do	k7c2	do
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
přes	přes	k7c4	přes
most	most	k1gInSc4	most
na	na	k7c4	na
Staré	Staré	k2eAgNnSc4d1	Staré
Město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Ubytoval	ubytovat	k5eAaPmAgInS	ubytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nárožním	nárožní	k2eAgInSc6d1	nárožní
Langenbrukově	Langenbrukův	k2eAgInSc6d1	Langenbrukův
domě	dům	k1gInSc6	dům
naproti	naproti	k7c3	naproti
jezuitskému	jezuitský	k2eAgInSc3d1	jezuitský
kostelu	kostel	k1gInSc3	kostel
u	u	k7c2	u
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
se	se	k3xPyFc4	se
však	však	k9	však
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ani	ani	k8xC	ani
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
za	za	k7c7	za
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
,	,	kIx,	,
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
se	se	k3xPyFc4	se
k	k	k7c3	k
primátoru	primátor	k1gMnSc3	primátor
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
Valentinu	Valentin	k1gMnSc3	Valentin
Kirchmayerovi	Kirchmayer	k1gMnSc3	Kirchmayer
<g/>
.	.	kIx.	.
</s>
<s>
Korunu	koruna	k1gFnSc4	koruna
s	s	k7c7	s
klenoty	klenot	k1gInPc7	klenot
a	a	k8xC	a
archivem	archiv	k1gInSc7	archiv
nechal	nechat	k5eAaPmAgMnS	nechat
složit	složit	k5eAaPmF	složit
na	na	k7c6	na
Staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
radnici	radnice	k1gFnSc6	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
nešťastně	šťastně	k6eNd1	šťastně
svedené	svedený	k2eAgFnSc6d1	svedená
bitvě	bitva	k1gFnSc6	bitva
ohromila	ohromit	k5eAaPmAgFnS	ohromit
skoro	skoro	k6eAd1	skoro
veškeré	veškerý	k3xTgFnPc4	veškerý
třídy	třída	k1gFnPc4	třída
pražského	pražský	k2eAgNnSc2d1	Pražské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
odjezd	odjezd	k1gInSc1	odjezd
Fridricha	Fridrich	k1gMnSc2	Fridrich
Falckého	falcký	k2eAgMnSc2d1	falcký
z	z	k7c2	z
královského	královský	k2eAgNnSc2d1	královské
sídla	sídlo	k1gNnSc2	sídlo
způsobil	způsobit	k5eAaPmAgInS	způsobit
naprostý	naprostý	k2eAgInSc1d1	naprostý
zmatek	zmatek	k1gInSc1	zmatek
a	a	k8xC	a
zděšení	zděšení	k1gNnSc1	zděšení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
radil	radit	k5eAaImAgMnS	radit
s	s	k7c7	s
Anhaltem	Anhalt	k1gInSc7	Anhalt
<g/>
,	,	kIx,	,
Hohenlohem	Hohenloh	k1gInSc7	Hohenloh
<g/>
,	,	kIx,	,
Thurnem	Thurn	k1gInSc7	Thurn
a	a	k8xC	a
jinými	jiný	k2eAgMnPc7d1	jiný
plukovníky	plukovník	k1gMnPc7	plukovník
a	a	k8xC	a
vojenskými	vojenský	k2eAgMnPc7d1	vojenský
rady	rada	k1gMnPc7	rada
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
být	být	k5eAaImF	být
Praha	Praha	k1gFnSc1	Praha
bráněna	bráněn	k2eAgFnSc1d1	bráněna
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
ji	on	k3xPp3gFnSc4	on
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
dvorem	dvůr	k1gInSc7	dvůr
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
odjel	odjet	k5eAaPmAgMnS	odjet
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
po	po	k7c6	po
deváté	devátý	k4xOgFnSc6	devátý
hodině	hodina	k1gFnSc6	hodina
ranní	ranní	k1gFnSc2	ranní
bylo	být	k5eAaImAgNnS	být
zapřaháno	zapřahat	k5eAaImNgNnS	zapřahat
do	do	k7c2	do
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
s	s	k7c7	s
malým	malý	k1gMnSc7	malý
synem	syn	k1gMnSc7	syn
v	v	k7c6	v
náruči	náruč	k1gFnSc6	náruč
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
vsedl	vsednout	k5eAaPmAgMnS	vsednout
na	na	k7c4	na
koně	kůň	k1gMnSc4	kůň
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
řada	řada	k1gFnSc1	řada
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
bylo	být	k5eAaImAgNnS	být
naloženo	naložit	k5eAaPmNgNnS	naložit
stříbro	stříbro	k1gNnSc1	stříbro
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
dražší	drahý	k2eAgFnPc1d2	dražší
věci	věc	k1gFnPc1	věc
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
průvodu	průvod	k1gInSc3	průvod
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
několik	několik	k4yIc1	několik
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
zemských	zemský	k2eAgMnPc2d1	zemský
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
cizích	cizí	k2eAgMnPc2d1	cizí
šlechticů	šlechtic	k1gMnPc2	šlechtic
a	a	k8xC	a
průvod	průvod	k1gInSc1	průvod
tří	tři	k4xCgFnPc2	tři
stovek	stovka	k1gFnPc2	stovka
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
k	k	k7c3	k
horské	horský	k2eAgFnSc3d1	horská
bráně	brána	k1gFnSc3	brána
<g/>
.	.	kIx.	.
</s>
<s>
Brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
zavřena	zavřít	k5eAaPmNgFnS	zavřít
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
posláno	poslat	k5eAaPmNgNnS	poslat
pro	pro	k7c4	pro
městského	městský	k2eAgMnSc4d1	městský
hejtmana	hejtman	k1gMnSc4	hejtman
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bránu	brána	k1gFnSc4	brána
otevřel	otevřít	k5eAaPmAgMnS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hejtmanem	hejtman	k1gMnSc7	hejtman
přibyla	přibýt	k5eAaPmAgFnS	přibýt
i	i	k9	i
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
loučil	loučit	k5eAaImAgMnS	loučit
krátkými	krátký	k2eAgFnPc7d1	krátká
slovy	slovo	k1gNnPc7	slovo
s	s	k7c7	s
městskými	městský	k2eAgMnPc7d1	městský
zástupci	zástupce	k1gMnPc7	zástupce
<g/>
,	,	kIx,	,
smutně	smutně	k6eAd1	smutně
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
pohlížejícími	pohlížející	k2eAgInPc7d1	pohlížející
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
u	u	k7c2	u
brány	brána	k1gFnSc2	brána
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
prostí	prostý	k2eAgMnPc1d1	prostý
Pražané	Pražan	k1gMnPc1	Pražan
<g/>
:	:	kIx,	:
někteří	některý	k3yIgMnPc1	některý
hořekovali	hořekovat	k5eAaImAgMnP	hořekovat
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
láli	lát	k5eAaImAgMnP	lát
<g/>
,	,	kIx,	,
vyhrožovali	vyhrožovat	k5eAaImAgMnP	vyhrožovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
s	s	k7c7	s
útěkem	útěk	k1gInSc7	útěk
u	u	k7c2	u
brány	brána	k1gFnSc2	brána
o	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
zpozdil	zpozdit	k5eAaPmAgMnS	zpozdit
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
by	by	kYmCp3nP	by
ho	on	k3xPp3gNnSc4	on
snad	snad	k9	snad
Pražané	Pražan	k1gMnPc1	Pražan
ani	ani	k8xC	ani
nepustili	pustit	k5eNaPmAgMnP	pustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
zemští	zemský	k2eAgMnPc1d1	zemský
úředníci	úředník	k1gMnPc1	úředník
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Roupova	roupův	k2eAgInSc2d1	roupův
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
uklidnit	uklidnit	k5eAaPmF	uklidnit
lid	lid	k1gInSc4	lid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
ho	on	k3xPp3gNnSc4	on
s	s	k7c7	s
králem	král	k1gMnSc7	král
též	též	k9	též
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
chvíli	chvíle	k1gFnSc4	chvíle
vyjížděl	vyjíždět	k5eAaImAgInS	vyjíždět
smutný	smutný	k2eAgInSc1d1	smutný
průvod	průvod	k1gInSc1	průvod
z	z	k7c2	z
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
zapomněl	zapomenout	k5eAaPmAgMnS	zapomenout
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
královskou	královský	k2eAgFnSc4d1	královská
povinnost	povinnost	k1gFnSc4	povinnost
a	a	k8xC	a
jako	jako	k8xC	jako
psanec	psanec	k1gMnSc1	psanec
utíkal	utíkat	k5eAaImAgMnS	utíkat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
vtrhlo	vtrhnout	k5eAaPmAgNnS	vtrhnout
císařské	císařský	k2eAgNnSc1d1	císařské
vojsko	vojsko	k1gNnSc1	vojsko
o	o	k7c4	o
11	[number]	k4	11
<g/>
.	.	kIx.	.
hodině	hodina	k1gFnSc6	hodina
dopolední	dopolední	k2eAgFnSc6d1	dopolední
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
stavy	stav	k1gInPc1	stav
neoficiálně	neoficiálně	k6eAd1	neoficiálně
kapitulovaly	kapitulovat	k5eAaBmAgInP	kapitulovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
některých	některý	k3yIgMnPc2	některý
zástupců	zástupce	k1gMnPc2	zástupce
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oficiální	oficiální	k2eAgFnSc3d1	oficiální
kapitulaci	kapitulace	k1gFnSc3	kapitulace
a	a	k8xC	a
odevzdání	odevzdání	k1gNnSc4	odevzdání
konfederačních	konfederační	k2eAgFnPc2d1	konfederační
smluv	smlouva	k1gFnPc2	smlouva
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
vítězů	vítěz	k1gMnPc2	vítěz
až	až	k6eAd1	až
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
skončilo	skončit	k5eAaPmAgNnS	skončit
období	období	k1gNnSc1	období
konfederace	konfederace	k1gFnSc2	konfederace
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
konfederace	konfederace	k1gFnSc1	konfederace
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
s	s	k7c7	s
Uherskem	Uhersko	k1gNnSc7	Uhersko
<g/>
,	,	kIx,	,
Rakousy	Rakousy	k1gInPc7	Rakousy
<g/>
,	,	kIx,	,
Lužicemi	Lužice	k1gFnPc7	Lužice
<g/>
,	,	kIx,	,
Slezskem	Slezsko	k1gNnSc7	Slezsko
a	a	k8xC	a
Moravou	Morava	k1gFnSc7	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kulturní	kulturní	k2eAgInSc1d1	kulturní
a	a	k8xC	a
společenský	společenský	k2eAgInSc1d1	společenský
odkaz	odkaz	k1gInSc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Jan	Jan	k1gMnSc1	Jan
Lohelius	Lohelius	k1gMnSc1	Lohelius
veřejným	veřejný	k2eAgInSc7d1	veřejný
listem	list	k1gInSc7	list
ke	k	k7c3	k
sbírání	sbírání	k1gNnSc3	sbírání
příspěvků	příspěvek	k1gInPc2	příspěvek
na	na	k7c6	na
vystavění	vystavění	k1gNnSc6	vystavění
památné	památný	k2eAgFnSc2d1	památná
kaple	kaple	k1gFnSc2	kaple
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
při	při	k7c6	při
hlavní	hlavní	k2eAgFnSc6d1	hlavní
silnici	silnice	k1gFnSc6	silnice
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
vedla	vést	k5eAaImAgFnS	vést
poutní	poutní	k2eAgFnSc1d1	poutní
cesta	cesta	k1gFnSc1	cesta
od	od	k7c2	od
pražské	pražský	k2eAgFnSc2d1	Pražská
Lorety	loreta	k1gFnSc2	loreta
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Hájku	hájek	k1gInSc6	hájek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1624	[number]	k4	1624
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc4d1	známo
první	první	k4xOgNnSc4	první
procesí	procesí	k1gNnSc4	procesí
z	z	k7c2	z
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
do	do	k7c2	do
kaple	kaple	k1gFnSc2	kaple
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
kázáno	kázat	k5eAaImNgNnS	kázat
česky	česky	k6eAd1	česky
i	i	k8xC	i
německy	německy	k6eAd1	německy
a	a	k8xC	a
děkováno	děkován	k2eAgNnSc1d1	děkováno
Bohu	bůh	k1gMnSc3	bůh
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
rozšíření	rozšíření	k1gNnSc6	rozšíření
kultu	kult	k1gInSc2	kult
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Vítězné	vítězný	k2eAgFnSc2d1	vítězná
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
potom	potom	k6eAd1	potom
o	o	k7c6	o
zviditelnění	zviditelnění	k1gNnSc6	zviditelnění
bitvy	bitva	k1gFnSc2	bitva
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celého	celý	k2eAgInSc2d1	celý
katolického	katolický	k2eAgInSc2d1	katolický
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
již	již	k6eAd1	již
zmiňovaný	zmiňovaný	k2eAgMnSc1d1	zmiňovaný
karmelitán	karmelitán	k1gMnSc1	karmelitán
Dominik	Dominik	k1gMnSc1	Dominik
à	à	k?	à
Jesu	Jesa	k1gMnSc4	Jesa
Maria	Mario	k1gMnSc4	Mario
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1704	[number]	k4	1704
až	až	k9	až
1730	[number]	k4	1730
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
výstavným	výstavný	k2eAgInSc7d1	výstavný
poutním	poutní	k2eAgInSc7d1	poutní
kostelem	kostel	k1gInSc7	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Vítězné	vítězný	k2eAgFnSc2d1	vítězná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
380	[number]	k4	380
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vyvýšenina	vyvýšenina	k1gFnSc1	vyvýšenina
při	při	k7c6	při
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
souvislé	souvislý	k2eAgFnSc2d1	souvislá
pražské	pražský	k2eAgFnSc2d1	Pražská
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
bitevního	bitevní	k2eAgNnSc2d1	bitevní
pole	pole	k1gNnSc2	pole
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
vily	vila	k1gFnPc4	vila
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
vlastní	vlastní	k2eAgNnSc1d1	vlastní
temeno	temeno	k1gNnSc1	temeno
návrší	návrší	k1gNnSc2	návrší
představuje	představovat	k5eAaImIp3nS	představovat
otevřené	otevřený	k2eAgNnSc1d1	otevřené
pole	pole	k1gNnSc1	pole
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
mohylou	mohyla	k1gFnSc7	mohyla
a	a	k8xC	a
pomníčkem	pomníček	k1gInSc7	pomníček
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
třístého	třístý	k2eAgNnSc2d1	třístý
výročí	výročí	k1gNnSc2	výročí
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vilové	vilový	k2eAgFnSc6d1	vilová
části	část	k1gFnSc6	část
sousední	sousední	k2eAgFnSc2d1	sousední
čtvrti	čtvrt	k1gFnSc2	čtvrt
Břevnov	Břevnov	k1gInSc1	Břevnov
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
bitvy	bitva	k1gFnSc2	bitva
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
místních	místní	k2eAgFnPc2d1	místní
ulic	ulice	k1gFnPc2	ulice
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
jako	jako	k8xC	jako
ulice	ulice	k1gFnSc1	ulice
Osmého	osmý	k4xOgInSc2	osmý
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgMnSc1d1	zimní
král	král	k1gMnSc1	král
aneb	aneb	k?	aneb
české	český	k2eAgNnSc1d1	české
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
Fridricha	Fridrich	k1gMnSc2	Fridrich
Falckého	falcký	k2eAgMnSc2d1	falcký
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rybka	rybka	k1gFnSc1	rybka
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
380	[number]	k4	380
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86182	[number]	k4	86182
<g/>
-	-	kIx~	-
<g/>
79	[number]	k4	79
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
s.	s.	k?	s.
287	[number]	k4	287
<g/>
–	–	k?	–
<g/>
306	[number]	k4	306
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
MIKULEC	Mikulec	k1gMnSc1	Mikulec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
1618	[number]	k4	1618
<g/>
-	-	kIx~	-
<g/>
1683	[number]	k4	1683
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
947	[number]	k4	947
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHALINE	CHALINE	kA	CHALINE
<g/>
,	,	kIx,	,
Olivier	Olivier	k1gMnSc1	Olivier
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
563	[number]	k4	563
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
2396	[number]	k4	2396
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAVKA	Kavka	k1gMnSc1	Kavka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
a	a	k8xC	a
české	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
politické	politický	k2eAgFnSc2d1	politická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
269	[number]	k4	269
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KUČERA	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
P.	P.	kA	P.
8	[number]	k4	8
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1620	[number]	k4	1620
-	-	kIx~	-
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
:	:	kIx,	:
o	o	k7c6	o
potracení	potracení	k1gNnSc6	potracení
starobylé	starobylý	k2eAgFnSc2d1	starobylá
slávy	sláva	k1gFnSc2	sláva
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Havran	Havran	k1gMnSc1	Havran
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
179	[number]	k4	179
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86515	[number]	k4	86515
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PERNES	PERNES	kA	PERNES
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
FUČÍK	Fučík	k1gMnSc1	Fučík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
HAVEL	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
císařským	císařský	k2eAgInSc7d1	císařský
praporem	prapor	k1gInSc7	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
habsburské	habsburský	k2eAgFnSc2d1	habsburská
armády	armáda	k1gFnSc2	armáda
1526	[number]	k4	1526
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Elka	Elka	k1gFnSc1	Elka
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
555	[number]	k4	555
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902745	[number]	k4	902745
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POLIŠENSKÝ	POLIŠENSKÝ	kA	POLIŠENSKÝ
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
politika	politika	k1gFnSc1	politika
a	a	k8xC	a
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
357	[number]	k4	357
s.	s.	k?	s.
</s>
</p>
<p>
<s>
TAPIÉ	TAPIÉ	kA	TAPIÉ
<g/>
,	,	kIx,	,
Victor	Victor	k1gMnSc1	Victor
L.	L.	kA	L.
Bílá	bílý	k2eAgFnSc1d1	bílá
Hora	hora	k1gFnSc1	hora
a	a	k8xC	a
francouzská	francouzský	k2eAgFnSc1d1	francouzská
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
499	[number]	k4	499
s.	s.	k?	s.
</s>
</p>
<p>
<s>
TEIGE	TEIGE	kA	TEIGE
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
KUFFNER	KUFFNER	kA	KUFFNER
<g/>
,	,	kIx,	,
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
;	;	kIx,	;
HERAIN	HERAIN	kA	HERAIN
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
Hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
A.	A.	kA	A.
B.	B.	kA	B.
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
87	[number]	k4	87
s.	s.	k?	s.
</s>
</p>
<p>
<s>
UHLÍŘ	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
den	den	k1gInSc1	den
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
:	:	kIx,	:
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1620	[number]	k4	1620
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
AVE	ave	k1gNnSc1	ave
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
169	[number]	k4	169
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902242	[number]	k4	902242
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
Hora	hora	k1gFnSc1	hora
:	:	kIx,	:
Almanach	almanach	k1gInSc1	almanach
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Družstvo	družstvo	k1gNnSc1	družstvo
Máje	máj	k1gFnSc2	máj
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
43	[number]	k4	43
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLEISNER	KLEISNER	kA	KLEISNER
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Medaile	medaile	k1gFnSc1	medaile
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
od	od	k7c2	od
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
Pietra	Pietrum	k1gNnSc2	Pietrum
de	de	k?	de
Pomis	pomísit	k5eAaPmRp2nS	pomísit
<g/>
.	.	kIx.	.
</s>
<s>
Folia	folio	k1gNnPc1	folio
numismatica	numismatic	k2eAgNnPc1d1	numismatic
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
57	[number]	k4	57
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
bitev	bitva	k1gFnPc2	bitva
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Bitvy	bitva	k1gFnPc1	bitva
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
</s>
</p>
<p>
<s>
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
exekuce	exekuce	k1gFnSc1	exekuce
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Bělohorská	bělohorský	k2eAgFnSc1d1	Bělohorská
bitva	bitva	k1gFnSc1	bitva
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
</s>
</p>
<p>
<s>
Téma	téma	k1gFnSc1	téma
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
Jak	jak	k8xC	jak
temné	temný	k2eAgNnSc1d1	temné
bylo	být	k5eAaImAgNnS	být
temno	temno	k1gNnSc1	temno
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
detailně	detailně	k6eAd1	detailně
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
interpretacemi	interpretace	k1gFnPc7	interpretace
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c4	na
Bílé	bílý	k2eAgNnSc4d1	bílé
hoře	hoře	k1gNnSc4	hoře
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
některými	některý	k3yIgInPc7	některý
málo	málo	k6eAd1	málo
známými	známý	k2eAgInPc7d1	známý
aspekty	aspekt	k1gInPc7	aspekt
celé	celý	k2eAgFnSc2d1	celá
historické	historický	k2eAgFnSc2d1	historická
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bellum	Bellum	k1gNnSc1	Bellum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1620	[number]	k4	1620
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c4	na
plánku	plánka	k1gFnSc4	plánka
</s>
</p>
<p>
<s>
Kolekce	kolekce	k1gFnSc1	kolekce
fotografií	fotografia	k1gFnPc2	fotografia
z	z	k7c2	z
dobové	dobový	k2eAgFnSc2d1	dobová
ukázky	ukázka	k1gFnSc2	ukázka
konané	konaný	k2eAgFnSc2d1	konaná
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
on-line	onin	k1gInSc5	on-lin
reportáž	reportáž	k1gFnSc4	reportáž
z	z	k7c2	z
bitvy	bitva	k1gFnSc2	bitva
</s>
</p>
