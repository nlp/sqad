<p>
<s>
Phoenix	Phoenix	k1gInSc1	Phoenix
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc4	první
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
amerického	americký	k2eAgMnSc2d1	americký
písničkáře	písničkář	k1gMnSc2	písničkář
Vince	Vince	k?	Vince
Bella	Bella	k1gMnSc1	Bella
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgInPc4d1	vydaný
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1994	[number]	k4	1994
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Watermelon	Watermelon	k1gInSc1	Watermelon
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
produkoval	produkovat	k5eAaImAgMnS	produkovat
Bob	Bob	k1gMnSc1	Bob
Neuwirth	Neuwirth	k1gMnSc1	Neuwirth
a	a	k8xC	a
podílelo	podílet	k5eAaImAgNnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
více	hodně	k6eAd2	hodně
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Geoff	Geoff	k1gMnSc1	Geoff
Muldaur	Muldaur	k1gMnSc1	Muldaur
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Cale	Cal	k1gFnSc2	Cal
a	a	k8xC	a
Mickey	Mickea	k1gFnSc2	Mickea
Raphael	Raphaela	k1gFnPc2	Raphaela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Frankenstein	Frankenstein	k2eAgMnSc1d1	Frankenstein
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Beast	Beast	k1gMnSc1	Beast
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Hard	Hard	k1gMnSc1	Hard
Road	Road	k1gMnSc1	Road
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Troubletown	Troubletowno	k1gNnPc2	Troubletowno
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Sun	Sun	kA	Sun
&	&	k?	&
Moon	Moon	k1gInSc1	Moon
&	&	k?	&
Stars	Stars	k1gInSc1	Stars
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Mirror	Mirror	k1gMnSc1	Mirror
<g/>
,	,	kIx,	,
Mirror	Mirror	k1gMnSc1	Mirror
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Had	had	k1gMnSc1	had
Enough	Enough	k1gInSc1	Enough
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Girl	girl	k1gFnSc2	girl
Who	Who	k1gMnSc1	Who
Never	Never	k1gMnSc1	Never
Saw	Saw	k1gMnSc1	Saw
a	a	k8xC	a
Mountain	Mountain	k1gMnSc1	Mountain
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Woman	Woman	k1gInSc1	Woman
of	of	k?	of
the	the	k?	the
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
Because	Because	k1gFnSc1	Because
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
No	no	k9	no
Tomorrow	Tomorrow	k1gFnSc1	Tomorrow
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Vince	Vince	k?	Vince
Bell	bell	k1gInSc1	bell
−	−	k?	−
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Geoff	Geoff	k1gMnSc1	Geoff
Muldaur	Muldaur	k1gMnSc1	Muldaur
−	−	k?	−
mandolína	mandolína	k1gFnSc1	mandolína
<g/>
,	,	kIx,	,
banjo	banjo	k1gNnSc1	banjo
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Fritz	Fritz	k1gMnSc1	Fritz
Richmond	Richmond	k1gMnSc1	Richmond
−	−	k?	−
washtub	washtub	k1gMnSc1	washtub
bass	bass	k6eAd1	bass
</s>
</p>
<p>
<s>
Bill	Bill	k1gMnSc1	Bill
Rich	Rich	k1gMnSc1	Rich
−	−	k?	−
baskytara	baskytara	k1gFnSc1	baskytara
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Mansfield	Mansfield	k1gMnSc1	Mansfield
−	−	k?	−
housle	housle	k1gFnPc4	housle
</s>
</p>
<p>
<s>
Mickey	Mickey	k1gInPc1	Mickey
Raphael	Raphaela	k1gFnPc2	Raphaela
−	−	k?	−
harmonika	harmonik	k1gMnSc2	harmonik
</s>
</p>
<p>
<s>
Stephen	Stephen	k2eAgInSc1d1	Stephen
Bruton	Bruton	k1gInSc1	Bruton
−	−	k?	−
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
mandolína	mandolína	k1gFnSc1	mandolína
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Cale	Cale	k1gNnSc2	Cale
−	−	k?	−
klavír	klavír	k1gInSc4	klavír
</s>
</p>
<p>
<s>
Jim	on	k3xPp3gMnPc3	on
Justice	justice	k1gFnPc4	justice
−	−	k?	−
housle	housle	k1gFnPc4	housle
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Logan	Logan	k1gMnSc1	Logan
−	−	k?	−
baskytara	baskytara	k1gFnSc1	baskytara
</s>
</p>
<p>
<s>
Victoria	Victorium	k1gNnPc4	Victorium
Williams	Williams	k1gInSc1	Williams
−	−	k?	−
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Lyle	Lyle	k6eAd1	Lyle
Lovett	Lovett	k2eAgInSc1d1	Lovett
−	−	k?	−
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
