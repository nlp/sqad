<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
filologie	filologie	k1gFnSc2	filologie
je	být	k5eAaImIp3nS	být
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
φ	φ	k?	φ
"	"	kIx"	"
<g/>
přítel	přítel	k1gMnSc1	přítel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
λ	λ	k?	λ
"	"	kIx"	"
<g/>
slovo	slovo	k1gNnSc1	slovo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
přátelství	přátelství	k1gNnSc4	přátelství
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
naukou	nauka	k1gFnSc7	nauka
aj.	aj.	kA	aj.
Dnes	dnes	k6eAd1	dnes
tímto	tento	k3xDgInSc7	tento
pojmem	pojem	k1gInSc7	pojem
rozumíme	rozumět	k5eAaImIp1nP	rozumět
nauku	nauka	k1gFnSc4	nauka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
daný	daný	k2eAgInSc4d1	daný
jazyk	jazyk	k1gInSc4	jazyk
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
literaturou	literatura	k1gFnSc7	literatura
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
psanou	psaný	k2eAgFnSc7d1	psaná
a	a	k8xC	a
s	s	k7c7	s
kulturním	kulturní	k2eAgInSc7d1	kulturní
a	a	k8xC	a
historickým	historický	k2eAgInSc7d1	historický
kontextem	kontext	k1gInSc7	kontext
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filologie	filologie	k1gFnSc1	filologie
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
věda	věda	k1gFnSc1	věda
zkoumající	zkoumající	k2eAgMnPc1d1	zkoumající
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
ústní	ústní	k2eAgFnSc4d1	ústní
lidovou	lidový	k2eAgFnSc4d1	lidová
slovesnost	slovesnost	k1gFnSc4	slovesnost
některého	některý	k3yIgInSc2	některý
národa	národ	k1gInSc2	národ
(	(	kIx(	(
<g/>
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
literatur	literatura	k1gFnPc2	literatura
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
kulturněhistorických	kulturněhistorický	k2eAgNnPc2d1	kulturněhistorické
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
památek	památka	k1gFnPc2	památka
<g/>
;	;	kIx,	;
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
zkoumáním	zkoumání	k1gNnSc7	zkoumání
jazykového	jazykový	k2eAgInSc2d1	jazykový
materiálu	materiál	k1gInSc2	materiál
v	v	k7c6	v
literárních	literární	k2eAgInPc6d1	literární
textech	text	k1gInPc6	text
<g/>
,	,	kIx,	,
výkladech	výklad	k1gInPc6	výklad
a	a	k8xC	a
edicích	edice	k1gFnPc6	edice
textů	text	k1gInPc2	text
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
filologií	filologie	k1gFnPc2	filologie
==	==	k?	==
</s>
</p>
<p>
<s>
Filologie	filologie	k1gFnSc1	filologie
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
dílčích	dílčí	k2eAgInPc2d1	dílčí
oborů	obor	k1gInPc2	obor
vztahujících	vztahující	k2eAgFnPc2d1	vztahující
se	se	k3xPyFc4	se
na	na	k7c4	na
dané	daný	k2eAgInPc4d1	daný
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
jazykové	jazykový	k2eAgFnSc2d1	jazyková
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zdokonalení	zdokonalení	k1gNnSc1	zdokonalení
znalosti	znalost	k1gFnSc2	znalost
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
praktickými	praktický	k2eAgNnPc7d1	praktické
cvičeními	cvičení	k1gNnPc7	cvičení
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
deskriptivní	deskriptivní	k2eAgFnSc1d1	deskriptivní
jazykověda	jazykověda	k1gFnSc1	jazykověda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obeznamuje	obeznamovat	k5eAaImIp3nS	obeznamovat
jak	jak	k6eAd1	jak
se	s	k7c7	s
základními	základní	k2eAgInPc7d1	základní
lingvistickými	lingvistický	k2eAgInPc7d1	lingvistický
pojmy	pojem	k1gInPc7	pojem
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
strukturou	struktura	k1gFnSc7	struktura
a	a	k8xC	a
postavením	postavení	k1gNnSc7	postavení
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
historická	historický	k2eAgFnSc1d1	historická
jazykověda	jazykověda	k1gFnSc1	jazykověda
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
historickým	historický	k2eAgInSc7d1	historický
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
rozvojem	rozvoj	k1gInSc7	rozvoj
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
gramatického	gramatický	k2eAgNnSc2d1	gramatické
<g/>
,	,	kIx,	,
tak	tak	k9	tak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
fonetického	fonetický	k2eAgNnSc2d1	fonetické
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
historická	historický	k2eAgFnSc1d1	historická
literární	literární	k2eAgFnSc1d1	literární
věda	věda	k1gFnSc1	věda
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
začátky	začátek	k1gInPc4	začátek
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
písemností	písemnost	k1gFnPc2	písemnost
vyhotovených	vyhotovený	k2eAgFnPc2d1	vyhotovená
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
jazyce	jazyk	k1gInSc6	jazyk
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
současná	současný	k2eAgFnSc1d1	současná
literární	literární	k2eAgFnSc1d1	literární
věda	věda	k1gFnSc1	věda
klade	klást	k5eAaImIp3nS	klást
zřetel	zřetel	k1gInSc4	zřetel
na	na	k7c6	na
současné	současný	k2eAgFnSc6d1	současná
činnosti	činnost	k1gFnSc6	činnost
v	v	k7c6	v
literárním	literární	k2eAgInSc6d1	literární
světě	svět	k1gInSc6	svět
společnosti	společnost	k1gFnSc2	společnost
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
reálie	reálie	k1gFnPc1	reálie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
veškeré	veškerý	k3xTgFnPc4	veškerý
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
dotyčných	dotyčný	k2eAgFnPc2d1	dotyčná
zemí	zem	k1gFnPc2	zem
<g/>
/	/	kIx~	/
<g/>
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
daného	daný	k2eAgInSc2d1	daný
kulturního	kulturní	k2eAgInSc2d1	kulturní
okruhu	okruh	k1gInSc2	okruh
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kultury	kultura	k1gFnSc2	kultura
anebo	anebo	k8xC	anebo
všedního	všední	k2eAgInSc2d1	všední
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
obory	obor	k1gInPc1	obor
==	==	k?	==
</s>
</p>
<p>
<s>
Filologie	filologie	k1gFnSc1	filologie
se	se	k3xPyFc4	se
prolíná	prolínat	k5eAaImIp3nS	prolínat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
vědami	věda	k1gFnPc7	věda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
historie	historie	k1gFnSc1	historie
</s>
</p>
<p>
<s>
etnologie	etnologie	k1gFnSc1	etnologie
</s>
</p>
<p>
<s>
geografie	geografie	k1gFnSc1	geografie
</s>
</p>
<p>
<s>
srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
literární	literární	k2eAgFnSc1d1	literární
věda	věda	k1gFnSc1	věda
</s>
</p>
<p>
<s>
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
jazykověda	jazykověda	k1gFnSc1	jazykověda
</s>
</p>
<p>
<s>
srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
jazykověda	jazykověda	k1gFnSc1	jazykověda
</s>
</p>
<p>
<s>
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Specializované	specializovaný	k2eAgInPc1d1	specializovaný
filologické	filologický	k2eAgInPc1d1	filologický
obory	obor	k1gInPc1	obor
podle	podle	k7c2	podle
jazyka	jazyk	k1gInSc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInSc1d1	následující
seznam	seznam	k1gInSc1	seznam
není	být	k5eNaImIp3nS	být
úplný	úplný	k2eAgInSc1d1	úplný
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
uvedeny	uvést	k5eAaPmNgInP	uvést
nejběžnější	běžný	k2eAgInPc1d3	nejběžnější
filologické	filologický	k2eAgInPc1d1	filologický
obory	obor	k1gInPc1	obor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slavistika	slavistika	k1gFnSc1	slavistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
literaturách	literatura	k1gFnPc6	literatura
</s>
</p>
<p>
<s>
bělorusistika	bělorusistika	k1gFnSc1	bělorusistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
běloruském	běloruský	k2eAgInSc6d1	běloruský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
bohemistika	bohemistika	k1gFnSc1	bohemistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
bulharistika	bulharistika	k1gFnSc1	bulharistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
bulharském	bulharský	k2eAgInSc6d1	bulharský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
kroatistika	kroatistika	k1gFnSc1	kroatistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
chorvatském	chorvatský	k2eAgInSc6d1	chorvatský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
makedonistika	makedonistika	k1gFnSc1	makedonistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
makedonském	makedonský	k2eAgInSc6d1	makedonský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
paleoslovenistika	paleoslovenistika	k1gFnSc1	paleoslovenistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
staroslověnském	staroslověnský	k2eAgInSc6d1	staroslověnský
a	a	k8xC	a
církevněslovanském	církevněslovanský	k2eAgInSc6d1	církevněslovanský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
písemnictví	písemnictví	k1gNnSc4	písemnictví
</s>
</p>
<p>
<s>
polonistika	polonistika	k1gFnSc1	polonistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
polském	polský	k2eAgInSc6d1	polský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
rusistika	rusistika	k1gFnSc1	rusistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
ruském	ruský	k2eAgInSc6d1	ruský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
serbistika	serbistika	k1gFnSc1	serbistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
srbském	srbský	k2eAgInSc6d1	srbský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
slovakistika	slovakistika	k1gFnSc1	slovakistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
slovenském	slovenský	k2eAgInSc6d1	slovenský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
slovenistika	slovenistika	k1gFnSc1	slovenistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
slovinském	slovinský	k2eAgInSc6d1	slovinský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
sorabistika	sorabistika	k1gFnSc1	sorabistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
lužické	lužický	k2eAgFnSc6d1	Lužická
srbštině	srbština	k1gFnSc6	srbština
a	a	k8xC	a
literatuře	literatura	k1gFnSc6	literatura
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
psané	psaný	k2eAgFnSc6d1	psaná
</s>
</p>
<p>
<s>
ukrajinistika	ukrajinistika	k1gFnSc1	ukrajinistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
ukrajinském	ukrajinský	k2eAgInSc6d1	ukrajinský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
literatuřeGermanistika	literatuřeGermanistika	k1gFnSc1	literatuřeGermanistika
(	(	kIx(	(
<g/>
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
)	)	kIx)	)
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
germánských	germánský	k2eAgInPc6d1	germánský
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
literaturách	literatura	k1gFnPc6	literatura
</s>
</p>
<p>
<s>
germanistika	germanistika	k1gFnSc1	germanistika
(	(	kIx(	(
<g/>
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
)	)	kIx)	)
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
německém	německý	k2eAgInSc6d1	německý
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
(	(	kIx(	(
<g/>
Některé	některý	k3yIgFnPc1	některý
univerzity	univerzita	k1gFnPc1	univerzita
se	se	k3xPyFc4	se
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
názvu	název	k1gInSc3	název
"	"	kIx"	"
<g/>
germanistika	germanistika	k1gFnSc1	germanistika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nazývajíce	nazývat	k5eAaImSgFnP	nazývat
obor	obor	k1gInSc1	obor
"	"	kIx"	"
<g/>
německá	německý	k2eAgFnSc1d1	německá
filologie	filologie	k1gFnSc1	filologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nederlandistika	nederlandistika	k1gFnSc1	nederlandistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
skandinavistika	skandinavistika	k1gFnSc1	skandinavistika
<g/>
/	/	kIx~	/
<g/>
nordistika	nordistika	k1gFnSc1	nordistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
severogermánských	severogermánský	k2eAgInPc6d1	severogermánský
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
skandinávských	skandinávský	k2eAgInPc6d1	skandinávský
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
severských	severský	k2eAgFnPc2d1	severská
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
literaturách	literatura	k1gFnPc6	literatura
</s>
</p>
<p>
<s>
anglistika	anglistika	k1gFnSc1	anglistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
amerikanistika	amerikanistika	k1gFnSc1	amerikanistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
americké	americký	k2eAgFnSc6d1	americká
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
jidistika	jidistika	k1gFnSc1	jidistika
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Jiddistik	Jiddistika	k1gFnPc2	Jiddistika
<g/>
)	)	kIx)	)
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
jazyce	jazyk	k1gInSc6	jazyk
jidiš	jidiš	k1gNnSc2	jidiš
a	a	k8xC	a
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
psané	psaný	k2eAgFnPc1d1	psaná
literatuřeRomanistika	literatuřeRomanistika	k1gFnSc1	literatuřeRomanistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
literaturách	literatura	k1gFnPc6	literatura
</s>
</p>
<p>
<s>
francistika	francistika	k1gFnSc1	francistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
francouzštině	francouzština	k1gFnSc6	francouzština
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc6d1	francouzská
literatuře	literatura	k1gFnSc6	literatura
(	(	kIx(	(
<g/>
Na	na	k7c6	na
většině	většina	k1gFnSc6	většina
univerzit	univerzita	k1gFnPc2	univerzita
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
spíše	spíše	k9	spíše
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
francouzská	francouzský	k2eAgFnSc1d1	francouzská
filologie	filologie	k1gFnSc1	filologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
italianistika	italianistika	k1gFnSc1	italianistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
italštině	italština	k1gFnSc6	italština
a	a	k8xC	a
italské	italský	k2eAgFnSc3d1	italská
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
hispanistika	hispanistika	k1gFnSc1	hispanistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
španělštině	španělština	k1gFnSc6	španělština
a	a	k8xC	a
španělské	španělský	k2eAgFnSc6d1	španělská
literatuře	literatura	k1gFnSc6	literatura
</s>
</p>
<p>
<s>
lusitanistika	lusitanistika	k1gFnSc1	lusitanistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
portugalském	portugalský	k2eAgInSc6d1	portugalský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
rumunistika	rumunistika	k1gFnSc1	rumunistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
rumunštině	rumunština	k1gFnSc6	rumunština
a	a	k8xC	a
rumunské	rumunský	k2eAgFnPc1d1	rumunská
literatuřeBaltistika	literatuřeBaltistika	k1gFnSc1	literatuřeBaltistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
baltských	baltský	k2eAgInPc6d1	baltský
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
literaturách	literatura	k1gFnPc6	literatura
</s>
</p>
<p>
<s>
let	let	k1gInSc1	let
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
onistika	onistika	k1gFnSc1	onistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
lotyštině	lotyština	k1gFnSc6	lotyština
a	a	k8xC	a
lotyšské	lotyšský	k2eAgFnSc3d1	lotyšská
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
lit	lit	k1gInSc1	lit
<g/>
(	(	kIx(	(
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
uanistika	uanistika	k1gFnSc1	uanistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
litevštině	litevština	k1gFnSc6	litevština
a	a	k8xC	a
litevské	litevský	k2eAgFnPc1d1	Litevská
literatuřeOrientalistika	literatuřeOrientalistika	k1gFnSc1	literatuřeOrientalistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
orientálních	orientální	k2eAgInPc6d1	orientální
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
literaturách	literatura	k1gFnPc6	literatura
</s>
</p>
<p>
<s>
arabistika	arabistika	k1gFnSc1	arabistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
arabském	arabský	k2eAgInSc6d1	arabský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
turkologie	turkologie	k1gFnSc1	turkologie
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
turkických	turkický	k2eAgInPc6d1	turkický
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
literaturách	literatura	k1gFnPc6	literatura
</s>
</p>
<p>
<s>
Íránistika	íránistika	k1gFnSc1	íránistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
íránských	íránský	k2eAgInPc6d1	íránský
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
literaturách	literatura	k1gFnPc6	literatura
</s>
</p>
<p>
<s>
japanologie	japanologie	k1gFnSc1	japanologie
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
japonštině	japonština	k1gFnSc6	japonština
a	a	k8xC	a
japonské	japonský	k2eAgFnSc3d1	japonská
literatuře	literatura	k1gFnSc3	literatura
</s>
</p>
<p>
<s>
koreanistika	koreanistika	k1gFnSc1	koreanistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
korejštině	korejština	k1gFnSc6	korejština
a	a	k8xC	a
korejské	korejský	k2eAgFnPc1d1	Korejská
literatuřeSinologie	literatuřeSinologie	k1gFnPc1	literatuřeSinologie
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
čínských	čínský	k2eAgInPc6d1	čínský
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
literaturách	literatura	k1gFnPc6	literatura
</s>
</p>
<p>
<s>
Uralistika	Uralistika	k1gFnSc1	Uralistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
uralských	uralský	k2eAgInPc6d1	uralský
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
literaturách	literatura	k1gFnPc6	literatura
</s>
</p>
<p>
<s>
ugrofinistika	ugrofinistika	k1gFnSc1	ugrofinistika
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
ugrofinských	ugrofinský	k2eAgInPc6d1	ugrofinský
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
literaturách	literatura	k1gFnPc6	literatura
</s>
</p>
<p>
<s>
===	===	k?	===
Přesahující	přesahující	k2eAgFnSc2d1	přesahující
filologie	filologie	k1gFnSc2	filologie
===	===	k?	===
</s>
</p>
<p>
<s>
indoevropeistika	indoevropeistika	k1gFnSc1	indoevropeistika
</s>
</p>
<p>
<s>
srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
jazykověda	jazykověda	k1gFnSc1	jazykověda
</s>
</p>
<p>
<s>
klasická	klasický	k2eAgFnSc1d1	klasická
filologie	filologie	k1gFnSc1	filologie
</s>
</p>
<p>
<s>
==	==	k?	==
"	"	kIx"	"
<g/>
Menší	malý	k2eAgInPc4d2	menší
<g/>
"	"	kIx"	"
jazyky	jazyk	k1gInPc4	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
"	"	kIx"	"
<g/>
malé	malý	k2eAgInPc1d1	malý
<g/>
"	"	kIx"	"
jazyky	jazyk	k1gInPc1	jazyk
–	–	k?	–
dosud	dosud	k6eAd1	dosud
–	–	k?	–
netvoří	tvořit	k5eNaImIp3nS	tvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
filologii	filologie	k1gFnSc4	filologie
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jsou	být	k5eAaImIp3nP	být
pojednávány	pojednávat	k5eAaImNgInP	pojednávat
pod	pod	k7c7	pod
filologií	filologie	k1gFnSc7	filologie
zastřešujícího	zastřešující	k2eAgInSc2d1	zastřešující
"	"	kIx"	"
<g/>
většího	veliký	k2eAgInSc2d2	veliký
<g/>
"	"	kIx"	"
jazyka	jazyk	k1gInSc2	jazyk
anebo	anebo	k8xC	anebo
srovnávací	srovnávací	k2eAgFnSc7d1	srovnávací
jazykovědou	jazykověda	k1gFnSc7	jazykověda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
například	například	k6eAd1	například
týče	týkat	k5eAaImIp3nS	týkat
baskického	baskický	k2eAgInSc2d1	baskický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
figuruje	figurovat	k5eAaImIp3nS	figurovat
na	na	k7c6	na
mimošpanělských	mimošpanělský	k2eAgFnPc6d1	mimošpanělský
univerzitách	univerzita	k1gFnPc6	univerzita
pod	pod	k7c7	pod
hispanistikou	hispanistika	k1gFnSc7	hispanistika
<g/>
,	,	kIx,	,
a	a	k8xC	a
indiánských	indiánský	k2eAgInPc2d1	indiánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
najdeme	najít	k5eAaPmIp1nP	najít
buď	buď	k8xC	buď
též	též	k9	též
pod	pod	k7c7	pod
hispanistikou	hispanistika	k1gFnSc7	hispanistika
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jazyky	jazyk	k1gInPc4	jazyk
domorodců	domorodec	k1gMnPc2	domorodec
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
pod	pod	k7c7	pod
amerikanistikou	amerikanistika	k1gFnSc7	amerikanistika
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
indiánské	indiánský	k2eAgInPc4d1	indiánský
jazyky	jazyk	k1gInPc4	jazyk
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
univerzitách	univerzita	k1gFnPc6	univerzita
figurují	figurovat	k5eAaImIp3nP	figurovat
africké	africký	k2eAgInPc1d1	africký
jazyky	jazyk	k1gInPc1	jazyk
pod	pod	k7c7	pod
etnologií	etnologie	k1gFnSc7	etnologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PETRÁČKOVÁ	Petráčková	k1gFnSc1	Petráčková
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
<g/>
;	;	kIx,	;
KRAUS	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Akademický	akademický	k2eAgInSc1d1	akademický
slovník	slovník	k1gInSc1	slovník
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
834	[number]	k4	834
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
607	[number]	k4	607
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Lingvistika	lingvistika	k1gFnSc1	lingvistika
</s>
</p>
<p>
<s>
Literatura	literatura	k1gFnSc1	literatura
</s>
</p>
