<s>
Filologie	filologie	k1gFnSc1	filologie
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
věda	věda	k1gFnSc1	věda
zkoumající	zkoumající	k2eAgMnPc1d1	zkoumající
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
ústní	ústní	k2eAgFnSc4d1	ústní
lidovou	lidový	k2eAgFnSc4d1	lidová
slovesnost	slovesnost	k1gFnSc4	slovesnost
některého	některý	k3yIgInSc2	některý
národa	národ	k1gInSc2	národ
(	(	kIx(	(
<g/>
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
literatur	literatura	k1gFnPc2	literatura
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
kulturněhistorických	kulturněhistorický	k2eAgNnPc2d1	kulturněhistorické
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
památek	památka	k1gFnPc2	památka
<g/>
;	;	kIx,	;
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
zkoumáním	zkoumání	k1gNnSc7	zkoumání
jazykového	jazykový	k2eAgInSc2d1	jazykový
materiálu	materiál	k1gInSc2	materiál
v	v	k7c6	v
literárních	literární	k2eAgInPc6d1	literární
textech	text	k1gInPc6	text
<g/>
,	,	kIx,	,
výkladech	výklad	k1gInPc6	výklad
a	a	k8xC	a
edicích	edice	k1gFnPc6	edice
textů	text	k1gInPc2	text
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
