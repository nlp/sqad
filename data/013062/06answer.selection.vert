<s>
Titan	titan	k1gInSc1	titan
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ti	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Titanium	Titanium	k1gNnSc1	Titanium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc1d1	šedý
až	až	k9	až
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
zastoupený	zastoupený	k2eAgInSc1d1	zastoupený
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
