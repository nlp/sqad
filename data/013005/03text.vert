<p>
<s>
Holmium	holmium	k1gNnSc1	holmium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Holmium	holmium	k1gNnSc1	holmium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
přechodný	přechodný	k2eAgInSc1d1	přechodný
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
člen	člen	k1gInSc1	člen
skupiny	skupina	k1gFnSc2	skupina
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nP	nacházet
využití	využití	k1gNnSc4	využití
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
silných	silný	k2eAgInPc2d1	silný
permanentních	permanentní	k2eAgInPc2d1	permanentní
magnetů	magnet	k1gInPc2	magnet
<g/>
,	,	kIx,	,
speciálních	speciální	k2eAgFnPc2d1	speciální
slitin	slitina	k1gFnPc2	slitina
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energetiku	energetika	k1gFnSc4	energetika
a	a	k8xC	a
laserů	laser	k1gInPc2	laser
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Holmium	holmium	k1gNnSc1	holmium
je	být	k5eAaImIp3nS	být
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
měkký	měkký	k2eAgInSc1d1	měkký
přechodný	přechodný	k2eAgInSc1d1	přechodný
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
holmium	holmium	k1gNnSc4	holmium
méně	málo	k6eAd2	málo
reaktivní	reaktivní	k2eAgInPc4d1	reaktivní
než	než	k8xS	než
předchozí	předchozí	k2eAgInPc4d1	předchozí
prvky	prvek	k1gInPc4	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
suchém	suchý	k2eAgInSc6d1	suchý
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
oxidu	oxid	k1gInSc2	oxid
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
minerálních	minerální	k2eAgFnPc6d1	minerální
kyselinách	kyselina	k1gFnPc6	kyselina
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Ho	on	k3xPp3gNnSc2	on
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
Ho	on	k3xPp3gNnSc2	on
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
podobné	podobný	k2eAgFnPc4d1	podobná
sloučeninám	sloučenina	k1gFnPc3	sloučenina
ostatních	ostatní	k2eAgInPc2d1	ostatní
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
a	a	k8xC	a
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
například	například	k6eAd1	například
vysoce	vysoce	k6eAd1	vysoce
stabilní	stabilní	k2eAgInPc1d1	stabilní
oxidy	oxid	k1gInPc1	oxid
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nereagují	reagovat	k5eNaBmIp3nP	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
se	se	k3xPyFc4	se
redukují	redukovat	k5eAaBmIp3nP	redukovat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
solí	sůl	k1gFnPc2	sůl
anorganických	anorganický	k2eAgFnPc2d1	anorganická
kyselin	kyselina	k1gFnPc2	kyselina
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
především	především	k9	především
fluoridy	fluorid	k1gInPc1	fluorid
a	a	k8xC	a
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
nerozpustnost	nerozpustnost	k1gFnSc1	nerozpustnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
separaci	separace	k1gFnSc3	separace
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
kovových	kovový	k2eAgInPc2d1	kovový
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Holmité	Holmitý	k2eAgFnPc1d1	Holmitý
soli	sůl	k1gFnPc1	sůl
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
žlutou	žlutý	k2eAgFnSc4d1	žlutá
nebo	nebo	k8xC	nebo
narůžovělou	narůžovělý	k2eAgFnSc4d1	narůžovělá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Holmium	holmium	k1gNnSc1	holmium
objevili	objevit	k5eAaPmAgMnP	objevit
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
současně	současně	k6eAd1	současně
Marc	Marc	k1gFnSc4	Marc
Delafontaine	Delafontain	k1gInSc5	Delafontain
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
Louis	Louis	k1gMnSc1	Louis
Soret	Soret	k1gMnSc1	Soret
a	a	k8xC	a
Per	pero	k1gNnPc2	pero
Teodor	Teodora	k1gFnPc2	Teodora
Cleve	Cleev	k1gFnPc4	Cleev
jako	jako	k8xC	jako
nečistotu	nečistota	k1gFnSc4	nečistota
ve	v	k7c6	v
zkoumaném	zkoumaný	k2eAgInSc6d1	zkoumaný
oxidu	oxid	k1gInSc6	oxid
erbitém	erbitý	k2eAgInSc6d1	erbitý
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
získal	získat	k5eAaPmAgInS	získat
prvek	prvek	k1gInSc4	prvek
po	po	k7c6	po
starém	starý	k2eAgInSc6d1	starý
latinském	latinský	k2eAgInSc6d1	latinský
názvu	název	k1gInSc6	název
města	město	k1gNnSc2	město
Stockholmu	Stockholm	k1gInSc2	Stockholm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Holmium	holmium	k1gNnSc1	holmium
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
přibližně	přibližně	k6eAd1	přibližně
1,2	[number]	k4	1,2
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
obsahu	obsah	k1gInSc6	obsah
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
údaje	údaj	k1gInPc1	údaj
chybí	chybit	k5eAaPmIp3nP	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
holmia	holmium	k1gNnSc2	holmium
na	na	k7c4	na
500	[number]	k4	500
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
holmium	holmium	k1gNnSc1	holmium
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
však	však	k9	však
ani	ani	k9	ani
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
lanthanoidy	lanthanoida	k1gFnPc1	lanthanoida
(	(	kIx(	(
<g/>
prvky	prvek	k1gInPc1	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
)	)	kIx)	)
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
minerály	minerál	k1gInPc4	minerál
směsné	směsný	k2eAgInPc4d1	směsný
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc1	všechen
prvky	prvek	k1gInPc1	prvek
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejznámější	známý	k2eAgFnSc7d3	nejznámější
patří	patřit	k5eAaImIp3nP	patřit
monazity	monazit	k1gInPc1	monazit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
,	,	kIx,	,
Th	Th	k1gFnSc1	Th
<g/>
,	,	kIx,	,
Nd	Nd	k1gFnSc1	Nd
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
a	a	k8xC	a
xenotim	xenotim	k1gInSc1	xenotim
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
bastnäsity	bastnäsit	k1gInPc1	bastnäsit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
CO	co	k8xS	co
<g/>
<g />
.	.	kIx.	.
</s>
<s>
3	[number]	k4	3
<g/>
F	F	kA	F
<g/>
–	–	k?	–
směsné	směsný	k2eAgInPc4d1	směsný
flourouhličitany	flourouhličitan	k1gInPc4	flourouhličitan
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
a	a	k8xC	a
např.	např.	kA	např.
minerál	minerál	k1gInSc1	minerál
euxenit	euxenit	k5eAaPmF	euxenit
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
<g/>
Ce	Ce	k1gMnSc1	Ce
<g/>
,	,	kIx,	,
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
<g/>
Th	Th	k1gFnSc1	Th
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
Nb	Nb	k1gFnSc1	Nb
<g/>
,	,	kIx,	,
<g/>
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
<g/>
Ti	ty	k3xPp2nSc3	ty
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgNnPc1d1	velké
ložiska	ložisko	k1gNnPc1	ložisko
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
ve	v	k7c4	v
Skandinávii	Skandinávie	k1gFnSc4	Skandinávie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
fosfátové	fosfátový	k2eAgFnPc1d1	fosfátová
suroviny	surovina	k1gFnPc1	surovina
–	–	k?	–
apatity	apatit	k1gInPc1	apatit
z	z	k7c2	z
poloostrova	poloostrov	k1gInSc2	poloostrov
Kola	kolo	k1gNnSc2	kolo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
rudy	ruda	k1gFnPc1	ruda
nejprve	nejprve	k6eAd1	nejprve
louží	loužit	k5eAaImIp3nP	loužit
směsí	směs	k1gFnSc7	směs
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
a	a	k8xC	a
ze	z	k7c2	z
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
roztoku	roztok	k1gInSc2	roztok
solí	solit	k5eAaImIp3nS	solit
se	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
vysráží	vysrážet	k5eAaPmIp3nS	vysrážet
hydroxidy	hydroxid	k1gInPc7	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Separace	separace	k1gFnSc1	separace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
řadou	řada	k1gFnSc7	řada
různých	různý	k2eAgInPc2d1	různý
postupů	postup	k1gInPc2	postup
–	–	k?	–
kapalinovou	kapalinový	k2eAgFnSc7d1	kapalinová
extrakcí	extrakce	k1gFnSc7	extrakce
<g/>
,	,	kIx,	,
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
ionexových	ionexový	k2eAgFnPc2d1	ionexová
kolon	kolona	k1gFnPc2	kolona
nebo	nebo	k8xC	nebo
selektivním	selektivní	k2eAgNnSc7d1	selektivní
srážením	srážení	k1gNnSc7	srážení
nerozpustných	rozpustný	k2eNgFnPc2d1	nerozpustná
komplexních	komplexní	k2eAgFnPc2d1	komplexní
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příprava	příprava	k1gFnSc1	příprava
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
holmitého	holmitý	k2eAgInSc2d1	holmitý
Ho	on	k3xPp3gNnSc2	on
<g/>
2	[number]	k4	2
<g/>
O	o	k7c6	o
<g/>
3	[number]	k4	3
elementárním	elementární	k2eAgInSc7d1	elementární
vápníkem	vápník	k1gInSc7	vápník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ho	on	k3xPp3gNnSc2	on
<g/>
2	[number]	k4	2
<g/>
O	o	k7c6	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
Ca	ca	kA	ca
→	→	k?	→
2	[number]	k4	2
Ho	ho	k0	ho
+	+	kIx~	+
3	[number]	k4	3
CaO	CaO	k1gFnPc2	CaO
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
a	a	k8xC	a
sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
gadolinium	gadolinium	k1gNnSc4	gadolinium
<g/>
,	,	kIx,	,
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
holmium	holmium	k1gNnSc4	holmium
vysoký	vysoký	k2eAgInSc1d1	vysoký
účinný	účinný	k2eAgInSc1d1	účinný
průřez	průřez	k1gInSc1	průřez
pro	pro	k7c4	pro
záchyt	záchyt	k1gInSc4	záchyt
tepelných	tepelný	k2eAgInPc2d1	tepelný
neutronů	neutron	k1gInPc2	neutron
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
slitiny	slitina	k1gFnSc2	slitina
jsou	být	k5eAaImIp3nP	být
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
moderátorových	moderátorův	k2eAgFnPc2d1	moderátorova
tyčí	tyč	k1gFnPc2	tyč
v	v	k7c6	v
jaderných	jaderný	k2eAgInPc6d1	jaderný
reaktorech	reaktor	k1gInPc6	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Zasunutím	zasunutí	k1gNnSc7	zasunutí
těchto	tento	k3xDgFnPc2	tento
tyčí	tyč	k1gFnPc2	tyč
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
reaktoru	reaktor	k1gInSc2	reaktor
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
neutronového	neutronový	k2eAgInSc2d1	neutronový
toku	tok	k1gInSc2	tok
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zpomalení	zpomalení	k1gNnSc4	zpomalení
štěpné	štěpný	k2eAgFnSc2d1	štěpná
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Holmium	holmium	k1gNnSc1	holmium
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
velmi	velmi	k6eAd1	velmi
silných	silný	k2eAgInPc2d1	silný
umělých	umělý	k2eAgInPc2d1	umělý
magnetů	magnet	k1gInPc2	magnet
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
pro	pro	k7c4	pro
fokusaci	fokusace	k1gFnSc4	fokusace
magnetických	magnetický	k2eAgFnPc2d1	magnetická
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Holmium	holmium	k1gNnSc1	holmium
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
laserů	laser	k1gInPc2	laser
na	na	k7c4	na
bázi	báze	k1gFnSc4	báze
granátů	granát	k1gInPc2	granát
yttria	yttrium	k1gNnSc2	yttrium
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
nebo	nebo	k8xC	nebo
fluoridů	fluorid	k1gInPc2	fluorid
yttria	yttrium	k1gNnSc2	yttrium
a	a	k8xC	a
lanthanu	lanthan	k1gInSc2	lanthan
<g/>
.	.	kIx.	.
</s>
<s>
Lasery	laser	k1gInPc1	laser
uvedených	uvedený	k2eAgInPc2d1	uvedený
typů	typ	k1gInPc2	typ
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
vyzařování	vyzařování	k1gNnSc4	vyzařování
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
holmia	holmium	k1gNnSc2	holmium
se	se	k3xPyFc4	se
ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
užívají	užívat	k5eAaImIp3nP	užívat
pro	pro	k7c4	pro
barvení	barvení	k1gNnSc4	barvení
skloviny	sklovina	k1gFnSc2	sklovina
do	do	k7c2	do
žluta	žluto	k1gNnSc2	žluto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
Greenwood	Greenwood	k1gInSc1	Greenwood
N.	N.	kA	N.
<g/>
N.	N.	kA	N.
<g/>
,	,	kIx,	,
Earnshaw	Earnshaw	k1gMnSc1	Earnshaw
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
II	II	kA	II
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
holmium	holmium	k1gNnSc4	holmium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
holmium	holmium	k1gNnSc4	holmium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
