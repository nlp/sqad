<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Universitas	Universitas	k1gInSc1	Universitas
Palackiana	Palackian	k1gMnSc2	Palackian
Olomucensis	Olomucensis	k1gFnSc2	Olomucensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
tradicí	tradice	k1gFnSc7	tradice
po	po	k7c6	po
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
druhá	druhý	k4xOgFnSc1	druhý
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
pak	pak	k6eAd1	pak
nejstarší	starý	k2eAgFnSc1d3	nejstarší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1573	[number]	k4	1573
udělením	udělení	k1gNnSc7	udělení
příslušných	příslušný	k2eAgNnPc2d1	příslušné
práv	právo	k1gNnPc2	právo
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
olomoucké	olomoucký	k2eAgFnSc6d1	olomoucká
koleji	kolej	k1gFnSc6	kolej
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgFnSc1d1	Ježíšova
<g/>
.	.	kIx.	.
</s>
<s>
Vysokoškolská	vysokoškolský	k2eAgFnSc1d1	vysokoškolská
výuka	výuka	k1gFnSc1	výuka
začala	začít	k5eAaPmAgFnS	začít
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
UPOL	UPOL	kA	UPOL
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
univerzita	univerzita	k1gFnSc1	univerzita
na	na	k7c4	na
čas	čas	k1gInSc4	čas
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgNnP	být
obnovena	obnovit	k5eAaPmNgNnP	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1621	[number]	k4	1621
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
byla	být	k5eAaImAgFnS	být
moravská	moravský	k2eAgFnSc1d1	Moravská
univerzita	univerzita	k1gFnSc1	univerzita
na	na	k7c4	na
čas	čas	k1gInSc4	čas
přestěhována	přestěhovat	k5eAaPmNgFnS	přestěhovat
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
vydal	vydat	k5eAaPmAgMnS	vydat
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
dekret	dekret	k1gInSc4	dekret
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
univerzita	univerzita	k1gFnSc1	univerzita
přenesla	přenést	k5eAaPmAgFnS	přenést
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
byla	být	k5eAaImAgFnS	být
degradována	degradovat	k5eAaBmNgFnS	degradovat
na	na	k7c4	na
akademické	akademický	k2eAgNnSc4d1	akademické
lyceum	lyceum	k1gNnSc4	lyceum
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
dekretu	dekret	k1gInSc2	dekret
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
univerzity	univerzita	k1gFnPc1	univerzita
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
bylo	být	k5eAaImAgNnS	být
učení	učení	k1gNnSc1	učení
znovu	znovu	k6eAd1	znovu
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
C.	C.	kA	C.
k.	k.	k?	k.
Františkova	Františkův	k2eAgFnSc1d1	Františkova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
dekretem	dekret	k1gInSc7	dekret
univerzitu	univerzita	k1gFnSc4	univerzita
zrušil	zrušit	k5eAaPmAgInS	zrušit
<g/>
,	,	kIx,	,
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
zůstala	zůstat	k5eAaPmAgFnS	zůstat
kromě	kromě	k7c2	kromě
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
knihovny	knihovna	k1gFnSc2	knihovna
pouze	pouze	k6eAd1	pouze
samostatná	samostatný	k2eAgFnSc1d1	samostatná
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pojítkem	pojítko	k1gNnSc7	pojítko
mezi	mezi	k7c7	mezi
starou	starý	k2eAgFnSc7d1	stará
univerzitou	univerzita	k1gFnSc7	univerzita
a	a	k8xC	a
univerzitou	univerzita	k1gFnSc7	univerzita
dnešní	dnešní	k2eAgFnSc7d1	dnešní
<g/>
,	,	kIx,	,
obnovenou	obnovený	k2eAgFnSc7d1	obnovená
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
takto	takto	k6eAd1	takto
přerušenému	přerušený	k2eAgInSc3d1	přerušený
olomouckému	olomoucký	k2eAgInSc3d1	olomoucký
univerzitnímu	univerzitní	k2eAgInSc3d1	univerzitní
vývoji	vývoj	k1gInSc3	vývoj
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
při	při	k7c6	při
založení	založení	k1gNnSc6	založení
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
50	[number]	k4	50
<g/>
/	/	kIx~	/
<g/>
1919	[number]	k4	1919
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
z.	z.	k?	z.
<g/>
n.	n.	k?	n.
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
byla	být	k5eAaImAgFnS	být
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
českou	český	k2eAgFnSc4d1	Česká
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Insignie	insignie	k1gFnPc4	insignie
===	===	k?	===
</s>
</p>
<p>
<s>
Revoluční	revoluční	k2eAgNnSc1d1	revoluční
hnutí	hnutí	k1gNnSc1	hnutí
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
nalezlo	naleznout	k5eAaPmAgNnS	naleznout
na	na	k7c6	na
olomoucké	olomoucký	k2eAgFnSc6d1	olomoucká
univerzitě	univerzita	k1gFnSc6	univerzita
živnou	živný	k2eAgFnSc4d1	živná
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
ozbrojená	ozbrojený	k2eAgFnSc1d1	ozbrojená
Akademická	akademický	k2eAgFnSc1d1	akademická
legie	legie	k1gFnSc1	legie
o	o	k7c4	o
celkem	celkem	k6eAd1	celkem
382	[number]	k4	382
mužích	muž	k1gMnPc6	muž
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
rota	rota	k1gFnSc1	rota
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
ze	z	k7c2	z
studentů	student	k1gMnPc2	student
práva	právo	k1gNnSc2	právo
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
studenta	student	k1gMnSc2	student
Appela	Appel	k1gMnSc2	Appel
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
ze	z	k7c2	z
studentů	student	k1gMnPc2	student
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
medicíny	medicína	k1gFnSc2	medicína
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
studenta	student	k1gMnSc2	student
Arnošta	Arnošt	k1gMnSc2	Arnošt
Förchtgotta	Förchtgott	k1gMnSc2	Förchtgott
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
legionářů	legionář	k1gMnPc2	legionář
pak	pak	k6eAd1	pak
podpořila	podpořit	k5eAaPmAgFnS	podpořit
akce	akce	k1gFnSc1	akce
revolučního	revoluční	k2eAgNnSc2d1	revoluční
studentstva	studentstvo	k1gNnSc2	studentstvo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Profesoři	profesor	k1gMnPc1	profesor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ignác	Ignác	k1gMnSc1	Ignác
Jan	Jan	k1gMnSc1	Jan
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Helcelet	Helcelet	k1gInSc1	Helcelet
<g/>
,	,	kIx,	,
Andreas	Andreas	k1gMnSc1	Andreas
Ludwig	Ludwig	k1gMnSc1	Ludwig
Jeitteles	Jeitteles	k1gMnSc1	Jeitteles
<g/>
)	)	kIx)	)
a	a	k8xC	a
studenti	student	k1gMnPc1	student
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
činnosti	činnost	k1gFnPc4	činnost
politických	politický	k2eAgInPc2d1	politický
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
podnítili	podnítit	k5eAaPmAgMnP	podnítit
vznik	vznik	k1gInSc4	vznik
německých	německý	k2eAgFnPc2d1	německá
i	i	k8xC	i
českých	český	k2eAgFnPc2d1	Česká
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
aktivity	aktivita	k1gFnPc4	aktivita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
univerzita	univerzita	k1gFnSc1	univerzita
upadla	upadnout	k5eAaPmAgFnS	upadnout
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
habsburský	habsburský	k2eAgInSc1d1	habsburský
režim	režim	k1gInSc1	režim
nabyl	nabýt	k5eAaPmAgInS	nabýt
zpět	zpět	k6eAd1	zpět
sebevědomí	sebevědomí	k1gNnSc4	sebevědomí
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
postupně	postupně	k6eAd1	postupně
likvidována	likvidovat	k5eAaBmNgFnS	likvidovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zrušena	zrušit	k5eAaPmNgFnS	zrušit
dekretem	dekret	k1gInSc7	dekret
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
(	(	kIx(	(
<g/>
dekret	dekret	k1gInSc4	dekret
přečkaly	přečkat	k5eAaPmAgFnP	přečkat
fakulta	fakulta	k1gFnSc1	fakulta
teologie	teologie	k1gFnSc1	teologie
a	a	k8xC	a
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
lékařsko-chirurgické	lékařskohirurgický	k2eAgNnSc1d1	lékařsko-chirurgický
studium	studium	k1gNnSc1	studium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitní	univerzitní	k2eAgFnPc4d1	univerzitní
regálie	regálie	k1gFnPc4	regálie
následně	následně	k6eAd1	následně
získala	získat	k5eAaPmAgFnS	získat
Univerzita	univerzita	k1gFnSc1	univerzita
Innsbruck	Innsbrucka	k1gFnPc2	Innsbrucka
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
akademický	akademický	k2eAgInSc1d1	akademický
řetěz	řetěz	k1gInSc1	řetěz
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
rektora	rektor	k1gMnSc2	rektor
zhotovený	zhotovený	k2eAgInSc1d1	zhotovený
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1566	[number]	k4	1566
<g/>
–	–	k?	–
<g/>
1573	[number]	k4	1573
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nyní	nyní	k6eAd1	nyní
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
řetěz	řetěz	k1gInSc1	řetěz
rektora	rektor	k1gMnSc2	rektor
innsbrucké	innsbrucký	k2eAgFnSc2d1	innsbrucká
univerzity	univerzita	k1gFnSc2	univerzita
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
akademické	akademický	k2eAgNnSc4d1	akademické
žezlo	žezlo	k1gNnSc4	žezlo
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
rektora	rektor	k1gMnSc2	rektor
zhotovené	zhotovený	k2eAgFnSc2d1	zhotovená
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1572	[number]	k4	1572
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nyní	nyní	k6eAd1	nyní
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
žezlo	žezlo	k1gNnSc1	žezlo
děkana	děkan	k1gMnSc2	děkan
innsbrucké	innsbrucký	k2eAgFnSc2d1	innsbrucká
fakulty	fakulta	k1gFnSc2	fakulta
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
akademické	akademický	k2eAgNnSc1d1	akademické
žezlo	žezlo	k1gNnSc1	žezlo
děkana	děkan	k1gMnSc2	děkan
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
filosofické	filosofický	k2eAgFnSc2d1	filosofická
fakulty	fakulta	k1gFnSc2	fakulta
zhotovené	zhotovený	k2eAgFnSc2d1	zhotovená
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1588	[number]	k4	1588
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nyní	nyní	k6eAd1	nyní
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
žezlo	žezlo	k1gNnSc1	žezlo
rektora	rektor	k1gMnSc2	rektor
innsbrucké	innsbrucký	k2eAgFnSc2d1	innsbrucká
univerzity	univerzita	k1gFnSc2	univerzita
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
a	a	k8xC	a
</s>
</p>
<p>
<s>
akademické	akademický	k2eAgNnSc1d1	akademické
žezlo	žezlo	k1gNnSc1	žezlo
děkana	děkan	k1gMnSc2	děkan
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
zhotovené	zhotovený	k2eAgFnSc2d1	zhotovená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nyní	nyní	k6eAd1	nyní
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
žezlo	žezlo	k1gNnSc1	žezlo
děkana	děkan	k1gMnSc2	děkan
innsbrucké	innsbrucký	k2eAgFnSc2d1	innsbrucká
fakulty	fakulta	k1gFnSc2	fakulta
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
<g/>
Snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
navrácení	navrácení	k1gNnSc4	navrácení
olomouckých	olomoucký	k2eAgFnPc2d1	olomoucká
regálií	regálie	k1gFnPc2	regálie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
započaly	započnout	k5eAaPmAgFnP	započnout
již	již	k6eAd1	již
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
doposud	doposud	k6eAd1	doposud
marné	marný	k2eAgFnPc1d1	marná
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
obdržení	obdržení	k1gNnSc1	obdržení
kopie	kopie	k1gFnSc2	kopie
rektorského	rektorský	k2eAgNnSc2d1	rektorské
žezla	žezlo	k1gNnSc2	žezlo
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Univerzita	univerzita	k1gFnSc1	univerzita
Innsbruck	Innsbruck	k1gMnSc1	Innsbruck
darovala	darovat	k5eAaPmAgFnS	darovat
Univerzitě	univerzita	k1gFnSc6	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obnovení	obnovení	k1gNnSc6	obnovení
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
tak	tak	k6eAd1	tak
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
zhotoveny	zhotoven	k2eAgFnPc4d1	zhotovena
nové	nový	k2eAgFnPc4d1	nová
regálie	regálie	k1gFnPc4	regálie
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
novodobého	novodobý	k2eAgInSc2d1	novodobý
znaku	znak	k1gInSc2	znak
UP	UP	kA	UP
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
UP	UP	kA	UP
a	a	k8xC	a
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
patriot	patriot	k1gMnSc1	patriot
Aljo	Aljo	k1gMnSc1	Aljo
Beran	Beran	k1gMnSc1	Beran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
názvu	název	k1gInSc2	název
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
univerzity	univerzita	k1gFnSc2	univerzita
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
oficiálního	oficiální	k2eAgInSc2d1	oficiální
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
prvních	první	k4xOgFnPc6	první
dvou	dva	k4xCgFnPc6	dva
třetinách	třetina	k1gFnPc6	třetina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
doložitelný	doložitelný	k2eAgInSc1d1	doložitelný
i	i	k9	i
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
asi	asi	k9	asi
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
plně	plně	k6eAd1	plně
ustálen	ustálit	k5eAaPmNgInS	ustálit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historických	historický	k2eAgInPc2d1	historický
dokladů	doklad	k1gInPc2	doklad
a	a	k8xC	a
zejm.	zejm.	k?	zejm.
pečetí	pečeť	k1gFnSc7	pečeť
univerzity	univerzita	k1gFnSc2	univerzita
lze	lze	k6eAd1	lze
stanovit	stanovit	k5eAaPmF	stanovit
zhruba	zhruba	k6eAd1	zhruba
tento	tento	k3xDgInSc4	tento
vývoj	vývoj	k1gInSc4	vývoj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
akademie	akademie	k1gFnSc1	akademie
a	a	k8xC	a
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
1573	[number]	k4	1573
<g/>
–	–	k?	–
<g/>
1762	[number]	k4	1762
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
akademie	akademie	k1gFnSc1	akademie
biskupská	biskupský	k2eAgFnSc1d1	biskupská
koleje	kolej	k1gFnSc2	kolej
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgFnSc1d1	Ježíšova
(	(	kIx(	(
<g/>
Alma	alma	k1gFnSc1	alma
Olomucensis	Olomucensis	k1gFnSc1	Olomucensis
academia	academia	k1gFnSc1	academia
collegii	collegie	k1gFnSc3	collegie
episcopalis	episcopalis	k1gFnSc4	episcopalis
Societatis	Societatis	k1gFnSc2	Societatis
Jesu	Jesus	k1gInSc2	Jesus
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
bylo	být	k5eAaImAgNnS	být
olomoucké	olomoucký	k2eAgNnSc1d1	olomoucké
vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
akademie	akademie	k1gFnSc1	akademie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císařsko-královská	císařskorálovský	k2eAgFnSc1d1	císařsko-královská
a	a	k8xC	a
biskupská	biskupský	k2eAgFnSc1d1	biskupská
univerzita	univerzita	k1gFnSc1	univerzita
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
Alma	alma	k1gFnSc1	alma
caesarea	caesarea	k1gFnSc1	caesarea
regia	regia	k1gFnSc1	regia
ac	ac	k?	ac
episcopalis	episcopalis	k1gInSc1	episcopalis
universitas	universitas	k1gInSc1	universitas
Societatis	Societatis	k1gInSc1	Societatis
Jesu	Jes	k2eAgFnSc4d1	Jes
Olomucensis	Olomucensis	k1gFnSc4	Olomucensis
<g/>
)	)	kIx)	)
–	–	k?	–
označení	označení	k1gNnSc2	označení
univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vydělením	vydělení	k1gNnSc7	vydělení
fakult	fakulta	k1gFnPc2	fakulta
filosofické	filosofický	k2eAgFnSc2d1	filosofická
a	a	k8xC	a
teologické	teologický	k2eAgFnSc2d1	teologická
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1630	[number]	k4	1630
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
státní	státní	k2eAgFnSc1d1	státní
a	a	k8xC	a
biskupská	biskupský	k2eAgFnSc1d1	biskupská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
1762	[number]	k4	1762
<g/>
–	–	k?	–
<g/>
1782	[number]	k4	1782
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1762	[number]	k4	1762
<g/>
–	–	k?	–
<g/>
1777	[number]	k4	1777
<g/>
:	:	kIx,	:
Císařsko-královská	císařskorálovský	k2eAgFnSc1d1	císařsko-královská
a	a	k8xC	a
biskupská	biskupský	k2eAgFnSc1d1	biskupská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
–	–	k?	–
roku	rok	k1gInSc2	rok
1762	[number]	k4	1762
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
používat	používat	k5eAaImF	používat
přívlastku	přívlastek	k1gInSc3	přívlastek
"	"	kIx"	"
<g/>
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
i	i	k8xC	i
starý	starý	k2eAgInSc1d1	starý
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
zrušení	zrušení	k1gNnSc2	zrušení
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
řádu	řád	k1gInSc2	řád
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1777	[number]	k4	1777
<g/>
–	–	k?	–
<g/>
1778	[number]	k4	1778
<g/>
:	:	kIx,	:
Císařsko-královská	císařskorálovský	k2eAgFnSc1d1	císařsko-královská
a	a	k8xC	a
arcibiskupská	arcibiskupský	k2eAgFnSc1d1	arcibiskupská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
</s>
</p>
<p>
<s>
1778	[number]	k4	1778
<g/>
–	–	k?	–
<g/>
1782	[number]	k4	1782
<g/>
:	:	kIx,	:
Císařsko-královská	císařskorálovský	k2eAgFnSc1d1	císařsko-královská
a	a	k8xC	a
arcibiskupská	arcibiskupský	k2eAgFnSc1d1	arcibiskupská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
olomoucké	olomoucký	k2eAgNnSc1d1	olomoucké
lyceum	lyceum	k1gNnSc1	lyceum
(	(	kIx(	(
<g/>
1782	[number]	k4	1782
<g/>
–	–	k?	–
<g/>
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Císařsko-královské	císařskorálovský	k2eAgNnSc1d1	císařsko-královské
lyceum	lyceum	k1gNnSc1	lyceum
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
</s>
</p>
<p>
<s>
Františkova	Františkův	k2eAgFnSc1d1	Františkova
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
1827	[number]	k4	1827
<g/>
–	–	k?	–
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Císařsko-královská	císařskorálovský	k2eAgFnSc1d1	císařsko-královská
Františkova	Františkův	k2eAgFnSc1d1	Františkova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
Caesareo-regia	Caesareoegia	k1gFnSc1	Caesareo-regia
universitas	universitasa	k1gFnPc2	universitasa
Franciscea	Franciscea	k1gFnSc1	Franciscea
Olomoucensis	Olomoucensis	k1gFnSc1	Olomoucensis
<g/>
,	,	kIx,	,
K.	K.	kA	K.
K.	K.	kA	K.
Franzens-Universität	Franzens-Universität	k1gMnSc1	Franzens-Universität
in	in	k?	in
Olmütz	Olmütz	k1gMnSc1	Olmütz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
samostatná	samostatný	k2eAgFnSc1d1	samostatná
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
:	:	kIx,	:
Císařsko-královská	císařskorálovský	k2eAgFnSc1d1	císařsko-královská
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
–	–	k?	–
jakožto	jakožto	k8xS	jakožto
samostatná	samostatný	k2eAgFnSc1d1	samostatná
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
promočním	promoční	k2eAgNnSc7d1	promoční
</s>
</p>
<p>
<s>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
:	:	kIx,	:
Cyrilometodějská	cyrilometodějský	k2eAgFnSc1d1	Cyrilometodějská
bohoslovecká	bohoslovecký	k2eAgFnSc1d1	bohoslovecká
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
–	–	k?	–
jakožto	jakožto	k8xS	jakožto
samostatná	samostatný	k2eAgFnSc1d1	samostatná
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
promočním	promoční	k2eAgNnSc7d1	promoční
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
univerzity	univerzita	k1gFnSc2	univerzita
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
"	"	kIx"	"
<g/>
Univerzitu	univerzita	k1gFnSc4	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
dodatek	dodatek	k1gInSc4	dodatek
"	"	kIx"	"
<g/>
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
názvu	název	k1gInSc2	název
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Fakulty	fakulta	k1gFnSc2	fakulta
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Cyrilometodějská	cyrilometodějský	k2eAgFnSc1d1	Cyrilometodějská
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
===	===	k?	===
</s>
</p>
<p>
<s>
Cyrilometodějská	cyrilometodějský	k2eAgFnSc1d1	Cyrilometodějská
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
fakultou	fakulta	k1gFnSc7	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
–	–	k?	–
bohoslovecká	bohoslovecký	k2eAgNnPc1d1	bohoslovecké
studia	studio	k1gNnPc1	studio
probíhala	probíhat	k5eAaImAgNnP	probíhat
již	již	k9	již
při	při	k7c6	při
povýšení	povýšení	k1gNnSc6	povýšení
Jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
koleje	kolej	k1gFnSc2	kolej
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1573	[number]	k4	1573
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1782	[number]	k4	1782
<g/>
–	–	k?	–
<g/>
1827	[number]	k4	1827
univerzita	univerzita	k1gFnSc1	univerzita
degradována	degradovat	k5eAaBmNgFnS	degradovat
na	na	k7c4	na
lyceum	lyceum	k1gNnSc4	lyceum
<g/>
,	,	kIx,	,
zachovala	zachovat	k5eAaPmAgFnS	zachovat
si	se	k3xPyFc3	se
tato	tento	k3xDgFnSc1	tento
fakulta	fakulta	k1gFnSc1	fakulta
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
status	status	k1gInSc4	status
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jí	on	k3xPp3gFnSc3	on
zůstal	zůstat	k5eAaPmAgInS	zůstat
i	i	k9	i
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
ostatních	ostatní	k2eAgFnPc2d1	ostatní
fakult	fakulta	k1gFnPc2	fakulta
a	a	k8xC	a
zrušení	zrušení	k1gNnSc4	zrušení
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
profesorů	profesor	k1gMnPc2	profesor
fakulty	fakulta	k1gFnSc2	fakulta
se	se	k3xPyFc4	se
distancovala	distancovat	k5eAaBmAgFnS	distancovat
od	od	k7c2	od
revoluce	revoluce	k1gFnSc2	revoluce
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
změnila	změnit	k5eAaPmAgFnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Cyrilometodějskou	cyrilometodějský	k2eAgFnSc4d1	Cyrilometodějská
bohosloveckou	bohoslovecký	k2eAgFnSc4d1	bohoslovecká
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
nacisty	nacista	k1gMnPc7	nacista
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
obnovené	obnovený	k2eAgFnSc2d1	obnovená
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
komunisty	komunista	k1gMnPc7	komunista
<g/>
,	,	kIx,	,
nakrátko	nakrátko	k6eAd1	nakrátko
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
v	v	k7c6	v
letech	let	k1gInPc6	let
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
obnovila	obnovit	k5eAaPmAgFnS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
přípravy	příprava	k1gFnSc2	příprava
bohoslovců	bohoslovec	k1gMnPc2	bohoslovec
zde	zde	k6eAd1	zde
studují	studovat	k5eAaImIp3nP	studovat
i	i	k9	i
laičtí	laický	k2eAgMnPc1d1	laický
posluchači	posluchač	k1gMnPc1	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Vyšší	vysoký	k2eAgFnSc7d2	vyšší
odbornou	odborný	k2eAgFnSc7d1	odborná
školou	škola	k1gFnSc7	škola
sociální	sociální	k2eAgInSc1d1	sociální
Caritas	Caritas	k1gInSc1	Caritas
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
nabízí	nabízet	k5eAaImIp3nS	nabízet
také	také	k9	také
bakalářské	bakalářský	k2eAgInPc4d1	bakalářský
obory	obor	k1gInPc4	obor
charitativní	charitativní	k2eAgFnSc2d1	charitativní
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
a	a	k8xC	a
humanitární	humanitární	k2eAgFnSc2d1	humanitární
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
samostatně	samostatně	k6eAd1	samostatně
navazující	navazující	k2eAgNnSc4d1	navazující
magisterské	magisterský	k2eAgNnSc4d1	magisterské
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
===	===	k?	===
</s>
</p>
<p>
<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejstarší	starý	k2eAgFnSc7d3	nejstarší
fakultou	fakulta	k1gFnSc7	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Vyučování	vyučování	k1gNnSc1	vyučování
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Angličan	Angličan	k1gMnSc1	Angličan
George	Georg	k1gFnSc2	Georg
Warr	Warr	k1gInSc1	Warr
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1576	[number]	k4	1576
přednáškou	přednáška	k1gFnSc7	přednáška
z	z	k7c2	z
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
fakulta	fakulta	k1gFnSc1	fakulta
potlačena	potlačen	k2eAgFnSc1d1	potlačena
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
<g/>
.	.	kIx.	.
</s>
<s>
Obnovena	obnoven	k2eAgFnSc1d1	obnovena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
fakulta	fakulta	k1gFnSc1	fakulta
nabízí	nabízet	k5eAaImIp3nS	nabízet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
600	[number]	k4	600
kombinací	kombinace	k1gFnPc2	kombinace
širokého	široký	k2eAgNnSc2d1	široké
spektra	spektrum	k1gNnSc2	spektrum
humanitních	humanitní	k2eAgInPc2d1	humanitní
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgInPc2d1	sociální
<g/>
,	,	kIx,	,
lingvistických	lingvistický	k2eAgInPc2d1	lingvistický
a	a	k8xC	a
uměnovědných	uměnovědný	k2eAgInPc2d1	uměnovědný
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
===	===	k?	===
</s>
</p>
<p>
<s>
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc7	třetí
nejstarší	starý	k2eAgFnSc7d3	nejstarší
fakultou	fakulta	k1gFnSc7	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1679	[number]	k4	1679
byla	být	k5eAaImAgFnS	být
dekretem	dekret	k1gInSc7	dekret
císaře	císař	k1gMnSc2	císař
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
ustavena	ustaven	k2eAgFnSc1d1	ustavena
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
právnická	právnický	k2eAgFnSc1d1	právnická
profesura	profesura	k1gFnSc1	profesura
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
značné	značný	k2eAgFnSc3d1	značná
nelibosti	nelibost	k1gFnSc3	nelibost
jezuitů	jezuita	k1gMnPc2	jezuita
vedoucích	vedoucí	k1gMnPc2	vedoucí
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
centrem	centrum	k1gNnSc7	centrum
osvícenství	osvícenství	k1gNnSc2	osvícenství
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Profesura	profesura	k1gFnSc1	profesura
byla	být	k5eAaImAgFnS	být
povýšena	povýšit	k5eAaPmNgFnS	povýšit
na	na	k7c4	na
direktorium	direktorium	k1gNnSc4	direktorium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
a	a	k8xC	a
fakultou	fakulta	k1gFnSc7	fakulta
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1784	[number]	k4	1784
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
studenti	student	k1gMnPc1	student
a	a	k8xC	a
profesoři	profesor	k1gMnPc1	profesor
byli	být	k5eAaImAgMnP	být
nejaktivnějšími	aktivní	k2eAgMnPc7d3	nejaktivnější
příznivci	příznivec	k1gMnPc7	příznivec
revoluce	revoluce	k1gFnSc2	revoluce
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
její	její	k3xOp3gNnSc1	její
uzavření	uzavření	k1gNnSc1	uzavření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
obnovil	obnovit	k5eAaPmAgInS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
i	i	k8xC	i
právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
faktickému	faktický	k2eAgNnSc3d1	faktické
znovuotevření	znovuotevření	k1gNnSc3	znovuotevření
ale	ale	k8xC	ale
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedinou	jediný	k2eAgFnSc4d1	jediná
v	v	k7c6	v
ČR	ČR	kA	ČR
poskytující	poskytující	k2eAgFnSc1d1	poskytující
klinické	klinický	k2eAgNnSc4d1	klinické
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
studentům	student	k1gMnPc3	student
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
bezplatné	bezplatný	k2eAgFnSc2d1	bezplatná
právní	právní	k2eAgFnSc2d1	právní
pomoci	pomoc	k1gFnSc2	pomoc
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
===	===	k?	===
</s>
</p>
<p>
<s>
Vyučování	vyučování	k1gNnSc1	vyučování
lékařství	lékařství	k1gNnSc2	lékařství
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
na	na	k7c6	na
Filosofické	filosofický	k2eAgFnSc6d1	filosofická
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1753	[number]	k4	1753
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přestěhováním	přestěhování	k1gNnSc7	přestěhování
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
při	při	k7c6	při
Filosofické	filosofický	k2eAgFnSc6d1	filosofická
fakultě	fakulta	k1gFnSc6	fakulta
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Katedra	katedra	k1gFnSc1	katedra
ranhojičství	ranhojičství	k1gNnSc2	ranhojičství
a	a	k8xC	a
babictví	babictví	k1gNnSc2	babictví
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
stala	stát	k5eAaPmAgFnS	stát
Direktoriem	direktorium	k1gNnSc7	direktorium
ranhojičství	ranhojičství	k1gNnSc2	ranhojičství
a	a	k8xC	a
babictví	babictví	k1gNnSc2	babictví
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
probíhal	probíhat	k5eAaImAgInS	probíhat
dvouletý	dvouletý	k2eAgInSc1d1	dvouletý
lékařský	lékařský	k2eAgInSc1d1	lékařský
kurz	kurz	k1gInSc1	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
lékařství	lékařství	k1gNnSc2	lékařství
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
zrušena	zrušit	k5eAaPmNgFnS	zrušit
byla	být	k5eAaImAgFnS	být
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
fakultou	fakulta	k1gFnSc7	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyčlenila	vyčlenit	k5eAaPmAgFnS	vyčlenit
nová	nový	k2eAgFnSc1d1	nová
Fakulta	fakulta	k1gFnSc1	fakulta
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
věd	věda	k1gFnPc2	věda
UP	UP	kA	UP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
===	===	k?	===
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
čtyři	čtyři	k4xCgFnPc1	čtyři
starobylé	starobylý	k2eAgFnPc1d1	starobylá
fakulty	fakulta	k1gFnPc1	fakulta
byly	být	k5eAaImAgFnP	být
ustaveny	ustaven	k2eAgFnPc4d1	ustavena
zákonem	zákon	k1gInSc7	zákon
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
100	[number]	k4	100
<g/>
/	/	kIx~	/
<g/>
1946	[number]	k4	1946
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Po	po	k7c6	po
reformě	reforma	k1gFnSc6	reforma
učitelského	učitelský	k2eAgNnSc2d1	učitelské
studia	studio	k1gNnSc2	studio
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
na	na	k7c4	na
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
<g/>
,	,	kIx,	,
přeměněnou	přeměněný	k2eAgFnSc4d1	přeměněná
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
na	na	k7c4	na
Pedagogický	pedagogický	k2eAgInSc4d1	pedagogický
institut	institut	k1gInSc4	institut
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
včleněn	včleněn	k2eAgInSc4d1	včleněn
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tak	tak	k6eAd1	tak
opět	opět	k6eAd1	opět
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyčlenila	vyčlenit	k5eAaPmAgFnS	vyčlenit
nová	nový	k2eAgFnSc1d1	nová
Fakulta	fakulta	k1gFnSc1	fakulta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
kultury	kultura	k1gFnSc2	kultura
UP	UP	kA	UP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
založena	založen	k2eAgFnSc1d1	založena
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
s	s	k7c7	s
Fakultou	fakulta	k1gFnSc7	fakulta
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
Fakultou	fakulta	k1gFnSc7	fakulta
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
druhá	druhý	k4xOgFnSc1	druhý
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
začleněna	začlenit	k5eAaPmNgFnS	začlenit
do	do	k7c2	do
svazku	svazek	k1gInSc2	svazek
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fakulta	fakulta	k1gFnSc1	fakulta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
kultury	kultura	k1gFnSc2	kultura
===	===	k?	===
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
kultury	kultura	k1gFnSc2	kultura
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
UP	UP	kA	UP
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
iniciativy	iniciativa	k1gFnSc2	iniciativa
pracovníků	pracovník	k1gMnPc2	pracovník
Katedry	katedra	k1gFnSc2	katedra
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
Pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
UP	UP	kA	UP
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
zahájila	zahájit	k5eAaPmAgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fakulta	fakulta	k1gFnSc1	fakulta
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
věd	věda	k1gFnPc2	věda
===	===	k?	===
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
věd	věda	k1gFnPc2	věda
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
jako	jako	k9	jako
osmá	osmý	k4xOgFnSc1	osmý
fakulta	fakulta	k1gFnSc1	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
UP	UP	kA	UP
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
osamostatněním	osamostatnění	k1gNnSc7	osamostatnění
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
nelékařských	lékařský	k2eNgInPc2d1	nelékařský
oborů	obor	k1gInPc2	obor
od	od	k7c2	od
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Univerzitní	univerzitní	k2eAgNnPc1d1	univerzitní
zařízení	zařízení	k1gNnPc1	zařízení
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
zařízení	zařízení	k1gNnSc4	zařízení
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
zvaná	zvaný	k2eAgFnSc1d1	zvaná
také	také	k6eAd1	také
"	"	kIx"	"
<g/>
centrální	centrální	k2eAgFnSc2d1	centrální
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
"	"	kIx"	"
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pracoviště	pracoviště	k1gNnSc1	pracoviště
UPRektorát	UPRektorát	k1gInSc1	UPRektorát
(	(	kIx(	(
<g/>
RUP	rup	k0	rup
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Centrum	centrum	k1gNnSc1	centrum
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
(	(	kIx(	(
<g/>
CVT	CVT	kA	CVT
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
(	(	kIx(	(
<g/>
KUP	kup	k1gInSc1	kup
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Projektový	projektový	k2eAgInSc1d1	projektový
servis	servis	k1gInSc1	servis
(	(	kIx(	(
<g/>
PS	PS	kA	PS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vědeckotechnický	vědeckotechnický	k2eAgInSc1d1	vědeckotechnický
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
VTP	VTP	kA	VTP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
(	(	kIx(	(
<g/>
VUP	VUP	kA	VUP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Centrum	centrum	k1gNnSc1	centrum
podpory	podpora	k1gFnSc2	podpora
studentů	student	k1gMnPc2	student
se	s	k7c7	s
specifickými	specifický	k2eAgFnPc7d1	specifická
potřebami	potřeba	k1gFnPc7	potřeba
(	(	kIx(	(
<g/>
CPS	CPS	kA	CPS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Konfuciův	Konfuciův	k2eAgInSc1d1	Konfuciův
institut	institut	k1gInSc1	institut
</s>
</p>
<p>
<s>
Akademik	akademik	k1gMnSc1	akademik
sport	sport	k1gInSc4	sport
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
ASC	ASC	kA	ASC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Správa	správa	k1gFnSc1	správa
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
menz	menza	k1gFnPc2	menza
(	(	kIx(	(
<g/>
SKM	SKM	kA	SKM
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
hala	hala	k1gFnSc1	hala
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
univerzitní	univerzitní	k2eAgFnSc6d1	univerzitní
hale	hala	k1gFnSc6	hala
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
U	u	k7c2	u
sportovní	sportovní	k2eAgFnSc2d1	sportovní
haly	hala	k1gFnSc2	hala
2	[number]	k4	2
<g/>
,	,	kIx,	,
Olomouc-Lazce	Olomouc-Lazka	k1gFnSc6	Olomouc-Lazka
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nP	sídlet
tři	tři	k4xCgInPc1	tři
sportovní	sportovní	k2eAgInPc1d1	sportovní
celky	celek	k1gInPc1	celek
SK	Sk	kA	Sk
UP	UP	kA	UP
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
SK	Sk	kA	Sk
UP-BCM	UP-BCM	k1gFnSc1	UP-BCM
Olomouc	Olomouc	k1gFnSc1	Olomouc
a	a	k8xC	a
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
loděnice	loděnice	k1gFnSc2	loděnice
Akademik	akademik	k1gMnSc1	akademik
sport	sport	k1gInSc1	sport
centrum	centrum	k1gNnSc4	centrum
UP	UP	kA	UP
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
víceúčelovou	víceúčelový	k2eAgFnSc4d1	víceúčelová
halu	hala	k1gFnSc4	hala
s	s	k7c7	s
komplexem	komplex	k1gInSc7	komplex
venkovních	venkovní	k2eAgNnPc2d1	venkovní
hřišť	hřiště	k1gNnPc2	hřiště
a	a	k8xC	a
loděnicí	loděnice	k1gFnSc7	loděnice
sloužící	sloužící	k2eAgMnSc1d1	sloužící
především	především	k6eAd1	především
pro	pro	k7c4	pro
odbornou	odborný	k2eAgFnSc4d1	odborná
výuku	výuka	k1gFnSc4	výuka
studentů	student	k1gMnPc2	student
Fakulty	fakulta	k1gFnSc2	fakulta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
kultury	kultura	k1gFnSc2	kultura
UP	UP	kA	UP
<g/>
,	,	kIx,	,
zájmovou	zájmový	k2eAgFnSc4d1	zájmová
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
výuku	výuka	k1gFnSc4	výuka
studentů	student	k1gMnPc2	student
všech	všecek	k3xTgMnPc2	všecek
fakult	fakulta	k1gFnPc2	fakulta
UP	UP	kA	UP
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
tréninky	trénink	k1gInPc4	trénink
a	a	k8xC	a
mistrovská	mistrovský	k2eAgNnPc4d1	mistrovské
utkání	utkání	k1gNnPc4	utkání
oddílů	oddíl	k1gInPc2	oddíl
Sportovního	sportovní	k2eAgInSc2d1	sportovní
klubu	klub	k1gInSc2	klub
UP	UP	kA	UP
a	a	k8xC	a
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgFnPc4d1	sportovní
či	či	k8xC	či
kulturní	kulturní	k2eAgFnPc4d1	kulturní
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hala	halo	k1gNnSc2	halo
svým	svůj	k3xOyFgNnSc7	svůj
vybavením	vybavení	k1gNnSc7	vybavení
splňuje	splňovat	k5eAaImIp3nS	splňovat
veškeré	veškerý	k3xTgInPc4	veškerý
parametry	parametr	k1gInPc4	parametr
pro	pro	k7c4	pro
vrcholový	vrcholový	k2eAgInSc4d1	vrcholový
sport	sport	k1gInSc4	sport
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Hrací	hrací	k2eAgFnSc1d1	hrací
plocha	plocha	k1gFnSc1	plocha
splňuje	splňovat	k5eAaImIp3nS	splňovat
podle	podle	k7c2	podle
certifikátu	certifikát	k1gInSc2	certifikát
DIN	din	k1gInSc1	din
(	(	kIx(	(
<g/>
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
federací	federace	k1gFnPc2	federace
volejbalu	volejbal	k1gInSc2	volejbal
<g/>
,	,	kIx,	,
basketbalu	basketbal	k1gInSc2	basketbal
a	a	k8xC	a
házené	házená	k1gFnSc2	házená
<g/>
)	)	kIx)	)
požadavky	požadavek	k1gInPc1	požadavek
pro	pro	k7c4	pro
konání	konání	k1gNnSc4	konání
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
na	na	k7c6	na
olympijské	olympijský	k2eAgFnSc6d1	olympijská
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dvě	dva	k4xCgFnPc4	dva
samostatné	samostatný	k2eAgFnPc4d1	samostatná
hrací	hrací	k2eAgFnPc4d1	hrací
plochy	plocha	k1gFnPc4	plocha
umožňující	umožňující	k2eAgFnSc4d1	umožňující
organizaci	organizace	k1gFnSc4	organizace
soustředění	soustředění	k1gNnPc2	soustředění
<g/>
,	,	kIx,	,
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
organizaci	organizace	k1gFnSc4	organizace
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
utkání	utkání	k1gNnPc2	utkání
a	a	k8xC	a
pořádání	pořádání	k1gNnSc6	pořádání
sportovních	sportovní	k2eAgFnPc2d1	sportovní
akcí	akce	k1gFnPc2	akce
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
i	i	k8xC	i
světové	světový	k2eAgFnSc6d1	světová
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prostory	prostor	k1gInPc7	prostor
haly	hala	k1gFnSc2	hala
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
víceúčelové	víceúčelový	k2eAgNnSc1d1	víceúčelové
centrum	centrum	k1gNnSc1	centrum
</s>
</p>
<p>
<s>
basketbalové	basketbalový	k2eAgNnSc1d1	basketbalové
hřiště	hřiště	k1gNnSc1	hřiště
</s>
</p>
<p>
<s>
volejbalové	volejbalový	k2eAgNnSc1d1	volejbalové
hřiště	hřiště	k1gNnSc1	hřiště
</s>
</p>
<p>
<s>
baseballové	baseballový	k2eAgNnSc1d1	baseballové
hřiště	hřiště	k1gNnSc1	hřiště
</s>
</p>
<p>
<s>
softbalové	softbalový	k2eAgNnSc1d1	softbalové
hřiště	hřiště	k1gNnSc1	hřiště
</s>
</p>
<p>
<s>
fotbalové	fotbalový	k2eAgNnSc4d1	fotbalové
hřiště	hřiště	k1gNnSc4	hřiště
</s>
</p>
<p>
<s>
vodní	vodní	k2eAgInPc4d1	vodní
sporty	sport	k1gInPc4	sport
</s>
</p>
<p>
<s>
==	==	k?	==
Studentské	studentský	k2eAgInPc1d1	studentský
spolky	spolek	k1gInPc1	spolek
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
spolkem	spolek	k1gInSc7	spolek
olomouckých	olomoucký	k2eAgMnPc2d1	olomoucký
studentů	student	k1gMnPc2	student
byla	být	k5eAaImAgFnS	být
patrně	patrně	k6eAd1	patrně
Mariánská	mariánský	k2eAgFnSc1d1	Mariánská
sodalita	sodalita	k1gFnSc1	sodalita
(	(	kIx(	(
<g/>
bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
spolku	spolek	k1gInSc2	spolek
ustaveného	ustavený	k2eAgInSc2d1	ustavený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1563	[number]	k4	1563
na	na	k7c6	na
Collegiu	Collegium	k1gNnSc6	Collegium
Romanu	Romana	k1gFnSc4	Romana
<g/>
)	)	kIx)	)
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Regina	Regina	k1gFnSc1	Regina
Angelorum	Angelorum	k1gInSc1	Angelorum
<g/>
,	,	kIx,	,
činná	činný	k2eAgFnSc1d1	činná
v	v	k7c6	v
letech	let	k1gInPc6	let
1625	[number]	k4	1625
<g/>
–	–	k?	–
<g/>
1778	[number]	k4	1778
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
působí	působit	k5eAaImIp3nS	působit
či	či	k8xC	či
působily	působit	k5eAaImAgInP	působit
různé	různý	k2eAgInPc1d1	různý
studentské	studentský	k2eAgInPc1d1	studentský
spolky	spolek	k1gInPc1	spolek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
<g />
.	.	kIx.	.
</s>
<s>
ESN	ESN	kA	ESN
UP	UP	kA	UP
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
sekce	sekce	k1gFnSc1	sekce
Erasmus	Erasmus	k1gInSc1	Erasmus
Student	student	k1gMnSc1	student
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
AIESEC	AIESEC	kA	AIESEC
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Asociace	asociace	k1gFnSc1	asociace
studentů	student	k1gMnPc2	student
speciální	speciální	k2eAgFnSc2d1	speciální
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
,	,	kIx,	,
Cech	cech	k1gInSc1	cech
studentů	student	k1gMnPc2	student
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
asociace	asociace	k1gFnSc1	asociace
studentů	student	k1gMnPc2	student
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
UP	UP	kA	UP
Crowd	Crowd	k1gInSc1	Crowd
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Konvikt	konvikt	k1gInSc1	konvikt
<g/>
,	,	kIx,	,
Liga	liga	k1gFnSc1	liga
posluchačů	posluchač	k1gMnPc2	posluchač
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
UP	UP	kA	UP
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
neúplný	úplný	k2eNgInSc1d1	neúplný
seznam	seznam	k1gInSc1	seznam
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
spjatých	spjatý	k2eAgFnPc2d1	spjatá
s	s	k7c7	s
olomouckou	olomoucký	k2eAgFnSc7d1	olomoucká
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Přednášející	přednášející	k1gMnSc1	přednášející
===	===	k?	===
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Kresa	Kresa	k1gFnSc1	Kresa
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
lingvista	lingvista	k1gMnSc1	lingvista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Tesánek	Tesánek	k1gMnSc1	Tesánek
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Irmler	Irmler	k1gMnSc1	Irmler
–	–	k?	–
PF	PF	kA	PF
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Heinrich	Heinrich	k1gMnSc1	Heinrich
Bösenselle	Bösenselle	k1gFnSc2	Bösenselle
–	–	k?	–
PF	PF	kA	PF
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Vratislav	Vratislav	k1gMnSc1	Vratislav
Monse	Monse	k1gFnSc2	Monse
–	–	k?	–
PF	PF	kA	PF
(	(	kIx(	(
<g/>
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
moravské	moravský	k2eAgFnSc2d1	Moravská
historiografie	historiografie	k1gFnSc2	historiografie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Samuel	Samuel	k1gMnSc1	Samuel
Karpe	Karp	k1gInSc5	Karp
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
slovinský	slovinský	k2eAgMnSc1d1	slovinský
filosof	filosof	k1gMnSc1	filosof
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ignaz	Ignaz	k1gInSc1	Ignaz
Feigerle	Feigerle	k1gFnSc2	Feigerle
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Fischer	Fischer	k1gMnSc1	Fischer
–	–	k?	–
PF	PF	kA	PF
(	(	kIx(	(
<g/>
komercionalista	komercionalista	k1gMnSc1	komercionalista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Adalbert	Adalbert	k1gMnSc1	Adalbert
Theodor	Theodor	k1gMnSc1	Theodor
Michel	Michel	k1gMnSc1	Michel
–	–	k?	–
PF	PF	kA	PF
</s>
</p>
<p>
<s>
Theodor	Theodor	k1gMnSc1	Theodor
von	von	k1gInSc4	von
Pachmann	Pachmann	k1gInSc1	Pachmann
–	–	k?	–
PF	PF	kA	PF
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Jan	Jan	k1gMnSc1	Jan
Mošner	Mošner	k1gMnSc1	Mošner
–	–	k?	–
LF	LF	kA	LF
(	(	kIx(	(
<g/>
porodník	porodník	k1gMnSc1	porodník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Schulz	Schulz	k1gMnSc1	Schulz
von	von	k1gInSc4	von
Straßnitzki	Straßnitzk	k1gFnSc2	Straßnitzk
–	–	k?	–
PF	PF	kA	PF
(	(	kIx(	(
<g/>
kameralista	kameralista	k1gMnSc1	kameralista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Franz	Franz	k1gMnSc1	Franz
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
daguerrotypista	daguerrotypista	k1gMnSc1	daguerrotypista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Koppel	Koppel	k1gMnSc1	Koppel
–	–	k?	–
PF	PF	kA	PF
(	(	kIx(	(
<g/>
revolucionář	revolucionář	k1gMnSc1	revolucionář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ignác	Ignác	k1gMnSc1	Ignác
Jan	Jan	k1gMnSc1	Jan
Hanuš	Hanuš	k1gMnSc1	Hanuš
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
revolucionář	revolucionář	k1gMnSc1	revolucionář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Helcelet	Helcelet	k1gInSc1	Helcelet
–	–	k?	–
LF	LF	kA	LF
<g/>
,	,	kIx,	,
PF	PF	kA	PF
(	(	kIx(	(
<g/>
revolucionář	revolucionář	k1gMnSc1	revolucionář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Andreas	Andreas	k1gMnSc1	Andreas
Ludwig	Ludwig	k1gMnSc1	Ludwig
Jeitteles	Jeitteles	k1gMnSc1	Jeitteles
–	–	k?	–
LF	LF	kA	LF
(	(	kIx(	(
<g/>
revolucionář	revolucionář	k1gMnSc1	revolucionář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Trost	Trost	k1gMnSc1	Trost
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
lingvista	lingvista	k1gMnSc1	lingvista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Poldauf	Poldauf	k1gMnSc1	Poldauf
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
lingvista	lingvista	k1gMnSc1	lingvista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Procházka	Procházka	k1gMnSc1	Procházka
–	–	k?	–
PF	PF	kA	PF
(	(	kIx(	(
<g/>
ústavní	ústavní	k2eAgMnPc4d1	ústavní
soudce	soudce	k1gMnPc4	soudce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Bělič	bělič	k1gMnSc1	bělič
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
dialektolog	dialektolog	k1gMnSc1	dialektolog
<g/>
,	,	kIx,	,
lingvista	lingvista	k1gMnSc1	lingvista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Vašica	Vašica	k1gMnSc1	Vašica
–	–	k?	–
TF	tf	k0wR	tf
(	(	kIx(	(
<g/>
zakladatel	zakladatel	k1gMnSc1	zakladatel
stolice	stolice	k1gFnSc2	stolice
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aljo	Aljo	k1gMnSc1	Aljo
Beran	Beran	k1gMnSc1	Beran
(	(	kIx(	(
<g/>
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rusek	Ruska	k1gFnPc2	Ruska
–	–	k?	–
PF	PF	kA	PF
(	(	kIx(	(
<g/>
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
skaut	skaut	k1gMnSc1	skaut
<g/>
,	,	kIx,	,
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
David	David	k1gMnSc1	David
–	–	k?	–
PF	PF	kA	PF
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Jařab	Jařab	k1gMnSc1	Jařab
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
amerikanista	amerikanista	k1gMnSc1	amerikanista
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Posluchači	posluchač	k1gMnPc5	posluchač
===	===	k?	===
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Sarkander	Sarkander	k1gMnSc1	Sarkander
(	(	kIx(	(
<g/>
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Ogilvie	Ogilvie	k1gFnSc2	Ogilvie
(	(	kIx(	(
<g/>
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
(	(	kIx(	(
<g/>
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wenceslas	Wenceslas	k1gMnSc1	Wenceslas
Pantaleon	Pantaleon	k1gMnSc1	Pantaleon
Kirwitzer	Kirwitzer	k1gMnSc1	Kirwitzer
(	(	kIx(	(
<g/>
astronom	astronom	k1gMnSc1	astronom
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Slavíček	Slavíček	k1gMnSc1	Slavíček
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
matematik	matematik	k1gMnSc1	matematik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Marcus	Marcus	k1gMnSc1	Marcus
Marci	Marek	k1gMnPc1	Marek
(	(	kIx(	(
<g/>
lékař	lékař	k1gMnSc1	lékař
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
(	(	kIx(	(
<g/>
literát	literát	k1gMnSc1	literát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Retz	Retz	k1gMnSc1	Retz
(	(	kIx(	(
<g/>
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
generál	generál	k1gMnSc1	generál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Petrasch	Petrasch	k1gMnSc1	Petrasch
(	(	kIx(	(
<g/>
učenec	učenec	k1gMnSc1	učenec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Vranický	Vranický	k2eAgMnSc1d1	Vranický
(	(	kIx(	(
<g/>
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Karl	Karl	k1gMnSc1	Karl
Nestler	Nestler	k1gMnSc1	Nestler
–	–	k?	–
FF	ff	kA	ff
<g/>
,	,	kIx,	,
TF	tf	k0wR	tf
<g/>
,	,	kIx,	,
PF	PF	kA	PF
(	(	kIx(	(
<g/>
polní	polní	k2eAgMnSc1d1	polní
hospodář	hospodář	k1gMnSc1	hospodář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
genetiky	genetika	k1gFnSc2	genetika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Eitelberger	Eitelbergra	k1gFnPc2	Eitelbergra
von	von	k1gInSc1	von
Edelberg	Edelberg	k1gMnSc1	Edelberg
–	–	k?	–
PF	PF	kA	PF
(	(	kIx(	(
<g/>
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Ritter	Rittra	k1gFnPc2	Rittra
von	von	k1gInSc1	von
Rittinger	Rittinger	k1gMnSc1	Rittinger
–	–	k?	–
PF	PF	kA	PF
<g/>
,	,	kIx,	,
FF	ff	kA	ff
</s>
</p>
<p>
<s>
Edmund	Edmund	k1gMnSc1	Edmund
Schebek	Schebek	k1gMnSc1	Schebek
–	–	k?	–
PF	PF	kA	PF
(	(	kIx(	(
<g/>
historik	historik	k1gMnSc1	historik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Schön	Schön	k1gMnSc1	Schön
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Förchtgott	Förchtgott	k1gMnSc1	Förchtgott
–	–	k?	–
FF	ff	kA	ff
<g/>
,	,	kIx,	,
PF	PF	kA	PF
(	(	kIx(	(
<g/>
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beda	Beda	k1gMnSc1	Beda
Dudík	Dudík	k1gMnSc1	Dudík
(	(	kIx(	(
<g/>
historiograf	historiograf	k1gMnSc1	historiograf
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Musil	Musil	k1gMnSc1	Musil
–	–	k?	–
TF	tf	k0wR	tf
(	(	kIx(	(
<g/>
orientalista	orientalista	k1gMnSc1	orientalista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Prečan	Prečan	k1gMnSc1	Prečan
–	–	k?	–
TF	tf	k0wR	tf
(	(	kIx(	(
<g/>
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Gorazd	Gorazd	k1gMnSc1	Gorazd
II	II	kA	II
<g/>
.	.	kIx.	.
–	–	k?	–
TF	tf	k0wR	tf
(	(	kIx(	(
<g/>
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Opelík	Opelík	k1gMnSc1	Opelík
(	(	kIx(	(
<g/>
kritik	kritik	k1gMnSc1	kritik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Werner	Werner	k1gMnSc1	Werner
(	(	kIx(	(
<g/>
indolog	indolog	k1gMnSc1	indolog
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
orientalista	orientalista	k1gMnSc1	orientalista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Tomášek	Tomášek	k1gMnSc1	Tomášek
–	–	k?	–
TF	tf	k0wR	tf
(	(	kIx(	(
<g/>
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Veřovský	Veřovský	k1gMnSc1	Veřovský
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
politik	politik	k1gMnSc1	politik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lumír	Lumír	k1gMnSc1	Lumír
Ondřej	Ondřej	k1gMnSc1	Ondřej
Hanuš	Hanuš	k1gMnSc1	Hanuš
–	–	k?	–
PřF	PřF	k1gMnSc1	PřF
(	(	kIx(	(
<g/>
chemik	chemik	k1gMnSc1	chemik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Stibor	Stibor	k1gMnSc1	Stibor
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Helena	Helena	k1gFnSc1	Helena
Havlíčková	Havlíčková	k1gFnSc1	Havlíčková
–	–	k?	–
PdF	PdF	k1gFnSc1	PdF
(	(	kIx(	(
<g/>
antikomunistka	antikomunistka	k1gFnSc1	antikomunistka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Štreit	Štreit	k1gMnSc1	Štreit
–	–	k?	–
PdF	PdF	k1gMnSc1	PdF
(	(	kIx(	(
<g/>
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
Švácha	Švácha	k1gMnSc1	Švácha
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
historik	historik	k1gMnSc1	historik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jozef	Jozef	k1gMnSc1	Jozef
Kubinyi	Kubiny	k1gFnSc2	Kubiny
–	–	k?	–
LF	LF	kA	LF
(	(	kIx(	(
<g/>
politik	politik	k1gMnSc1	politik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Balabán	Balabán	k1gMnSc1	Balabán
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Langer	Langer	k1gMnSc1	Langer
–	–	k?	–
LF	LF	kA	LF
a	a	k8xC	a
PF	PF	kA	PF
(	(	kIx(	(
<g/>
politik	politik	k1gMnSc1	politik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Viklický	Viklický	k2eAgMnSc1d1	Viklický
–	–	k?	–
PřF	PřF	k1gMnSc1	PřF
(	(	kIx(	(
<g/>
jazzman	jazzman	k1gMnSc1	jazzman
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Korytář	korytář	k1gMnSc1	korytář
–	–	k?	–
PřF	PřF	k1gMnSc1	PřF
(	(	kIx(	(
<g/>
ekolog	ekolog	k1gMnSc1	ekolog
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bogdan	Bogdan	k1gMnSc1	Bogdan
Trojak	Trojak	k1gMnSc1	Trojak
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
básník	básník	k1gMnSc1	básník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Dubská	Dubská	k1gFnSc1	Dubská
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
politička	politička	k1gFnSc1	politička
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
Tomáš	Tomáš	k1gMnSc1	Tomáš
Mohelník	Mohelník	k1gMnSc1	Mohelník
–	–	k?	–
TF	tf	k0wR	tf
(	(	kIx(	(
<g/>
teolog	teolog	k1gMnSc1	teolog
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Siostrzonek	Siostrzonek	k1gInSc1	Siostrzonek
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
sociolog	sociolog	k1gMnSc1	sociolog
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Věra	Věra	k1gFnSc1	Věra
Beranová	Beranová	k1gFnSc1	Beranová
–	–	k?	–
(	(	kIx(	(
<g/>
kunsthistorička	kunsthistorička	k1gFnSc1	kunsthistorička
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bohdan	Bohdan	k1gMnSc1	Bohdan
Pomahač	pomahač	k1gMnSc1	pomahač
–	–	k?	–
LF	LF	kA	LF
(	(	kIx(	(
<g/>
chirurg	chirurg	k1gMnSc1	chirurg
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Čada	Čada	k1gMnSc1	Čada
–	–	k?	–
(	(	kIx(	(
<g/>
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ivana	Ivana	k1gFnSc1	Ivana
Recmanová	Recmanová	k1gFnSc1	Recmanová
–	–	k?	–
FF	ff	kA	ff
(	(	kIx(	(
<g/>
publicistka	publicistka	k1gFnSc1	publicistka
<g/>
,	,	kIx,	,
aktivistka	aktivistka	k1gFnSc1	aktivistka
<g/>
,	,	kIx,	,
umělkyně	umělkyně	k1gFnSc1	umělkyně
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Rektoři	rektor	k1gMnPc1	rektor
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
rektorem	rektor	k1gMnSc7	rektor
univerzity	univerzita	k1gFnSc2	univerzita
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1573	[number]	k4	1573
stal	stát	k5eAaPmAgMnS	stát
Hurtado	Hurtada	k1gFnSc5	Hurtada
Pérez	Pérez	k1gMnSc1	Pérez
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1765	[number]	k4	1765
připadala	připadat	k5eAaPmAgFnS	připadat
pozice	pozice	k1gFnSc1	pozice
Rectora	Rector	k1gMnSc2	Rector
Magnifica	Magnificus	k1gMnSc2	Magnificus
automaticky	automaticky	k6eAd1	automaticky
rektorovi	rektor	k1gMnSc3	rektor
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
světským	světský	k2eAgMnSc7d1	světský
rektorem	rektor	k1gMnSc7	rektor
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1766	[number]	k4	1766
stal	stát	k5eAaPmAgMnS	stát
Johann	Johann	k1gMnSc1	Johann
Heinrich	Heinrich	k1gMnSc1	Heinrich
Bösenselle	Bösenselle	k1gFnSc1	Bösenselle
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
rektorům	rektor	k1gMnPc3	rektor
univerzity	univerzita	k1gFnSc2	univerzita
patří	patřit	k5eAaImIp3nS	patřit
osvícenec	osvícenec	k1gMnSc1	osvícenec
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
moravské	moravský	k2eAgFnSc2d1	Moravská
historiografie	historiografie	k1gFnSc2	historiografie
Josef	Josef	k1gMnSc1	Josef
Vratislav	Vratislav	k1gMnSc1	Vratislav
Monse	Monse	k1gFnSc1	Monse
<g/>
,	,	kIx,	,
slovinský	slovinský	k2eAgMnSc1d1	slovinský
filosof	filosof	k1gMnSc1	filosof
Franz	Franz	k1gMnSc1	Franz
Samuel	Samuel	k1gMnSc1	Samuel
Karpe	Karp	k1gInSc5	Karp
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
rektor	rektor	k1gMnSc1	rektor
obnovené	obnovený	k2eAgFnSc2d1	obnovená
univerzity	univerzita	k1gFnSc2	univerzita
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
filosof	filosof	k1gMnSc1	filosof
J.	J.	kA	J.
L.	L.	kA	L.
Fischer	Fischer	k1gMnSc1	Fischer
a	a	k8xC	a
první	první	k4xOgMnSc1	první
porevoluční	porevoluční	k2eAgMnSc1d1	porevoluční
rektor	rektor	k1gMnSc1	rektor
<g/>
,	,	kIx,	,
amerikanista	amerikanista	k1gMnSc1	amerikanista
Josef	Josef	k1gMnSc1	Josef
Jařab	Jařab	k1gMnSc1	Jařab
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
na	na	k7c6	na
postu	post	k1gInSc6	post
rektora	rektor	k1gMnSc4	rektor
české	český	k2eAgFnSc2d1	Česká
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
Jiřina	Jiřina	k1gFnSc1	Jiřina
Popelová	Popelová	k1gFnSc1	Popelová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
rektorem	rektor	k1gMnSc7	rektor
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
zastává	zastávat	k5eAaImIp3nS	zastávat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FIALA	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
1573	[number]	k4	1573
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
181	[number]	k4	181
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
244	[number]	k4	244
<g/>
-	-	kIx~	-
<g/>
2227	[number]	k4	2227
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Regionální	regionální	k2eAgNnSc1d1	regionální
centrum	centrum	k1gNnSc1	centrum
pokročilých	pokročilý	k2eAgFnPc2d1	pokročilá
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
materiálů	materiál	k1gInPc2	materiál
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palackého	k2eAgFnSc1d1	Palackého
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
</s>
</p>
<p>
<s>
Žurnál	žurnál	k1gInSc1	žurnál
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
–	–	k?	–
online	onlinout	k5eAaPmIp3nS	onlinout
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
</s>
</p>
<p>
<s>
Studentský	studentský	k2eAgInSc4d1	studentský
časopis	časopis	k1gInSc4	časopis
Helena	Helena	k1gFnSc1	Helena
v	v	k7c6	v
krabici	krabice	k1gFnSc6	krabice
</s>
</p>
