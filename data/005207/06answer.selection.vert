<s>
Clive	Clivat	k5eAaPmIp3nS	Clivat
Staples	Staples	k1gInSc1	Staples
Lewis	Lewis	k1gFnSc2	Lewis
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1898	[number]	k4	1898
v	v	k7c6	v
Belfastu	Belfast	k1gInSc6	Belfast
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
syn	syn	k1gMnSc1	syn
advokáta	advokát	k1gMnSc2	advokát
Alberta	Albert	k1gMnSc2	Albert
Jamese	Jamese	k1gFnSc1	Jamese
Lewise	Lewise	k1gFnSc1	Lewise
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
otec	otec	k1gMnSc1	otec
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
a	a	k8xC	a
Flory	Flora	k1gFnPc1	Flora
Augusty	Augusta	k1gMnSc2	Augusta
Lewisové	Lewisový	k2eAgFnSc2d1	Lewisová
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
irského	irský	k2eAgMnSc2d1	irský
anglikánského	anglikánský	k2eAgMnSc2d1	anglikánský
pastora	pastor	k1gMnSc2	pastor
<g/>
.	.	kIx.	.
</s>
