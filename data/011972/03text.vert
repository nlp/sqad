<p>
<s>
Sojové	sojový	k2eAgNnSc1d1	sojové
mléko	mléko	k1gNnSc1	mléko
či	či	k8xC	či
sojový	sojový	k2eAgInSc1d1	sojový
nápoj	nápoj	k1gInSc1	nápoj
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
nápoj	nápoj	k1gInSc4	nápoj
vyráběný	vyráběný	k2eAgInSc4d1	vyráběný
ze	z	k7c2	z
sóji	sója	k1gFnSc2	sója
namáčením	namáčení	k1gNnSc7	namáčení
<g/>
,	,	kIx,	,
drcením	drcení	k1gNnSc7	drcení
<g/>
,	,	kIx,	,
vařením	vaření	k1gNnSc7	vaření
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
přecezením	přecezení	k1gNnSc7	přecezení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sojové	sojový	k2eAgNnSc1d1	sojové
mléko	mléko	k1gNnSc1	mléko
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
mlékem	mléko	k1gNnSc7	mléko
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
tak	tak	k9	tak
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
jako	jako	k9	jako
náhražka	náhražka	k1gFnSc1	náhražka
mléka	mléko	k1gNnSc2	mléko
například	například	k6eAd1	například
ve	v	k7c6	v
veganské	veganský	k2eAgFnSc6d1	veganská
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
produktem	produkt	k1gInSc7	produkt
výroby	výroba	k1gFnSc2	výroba
sojového	sojový	k2eAgNnSc2d1	sojové
mléka	mléko	k1gNnSc2	mléko
je	být	k5eAaImIp3nS	být
okara	okara	k6eAd1	okara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Sojové	sojový	k2eAgNnSc1d1	sojové
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
především	především	k9	především
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
tam	tam	k6eAd1	tam
takového	takový	k3xDgNnSc2	takový
rozšíření	rozšíření	k1gNnSc2	rozšíření
jako	jako	k9	jako
mléko	mléko	k1gNnSc1	mléko
kravské	kravský	k2eAgNnSc1d1	kravské
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
především	především	k9	především
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
tofu	tofu	k1gNnSc2	tofu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
i	i	k9	i
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
nástroj	nástroj	k1gInSc1	nástroj
zdravého	zdravý	k2eAgInSc2d1	zdravý
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
k	k	k7c3	k
vaření	vaření	k1gNnSc3	vaření
a	a	k8xC	a
pečení	pečení	k1gNnSc3	pečení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
kauza	kauza	k1gFnSc1	kauza
ohledně	ohledně	k7c2	ohledně
genetické	genetický	k2eAgFnSc2d1	genetická
modifikace	modifikace	k1gFnSc2	modifikace
sojových	sojový	k2eAgInPc2d1	sojový
bobů	bob	k1gInPc2	bob
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dodavatelé	dodavatel	k1gMnPc1	dodavatel
sojových	sojový	k2eAgInPc2d1	sojový
produktů	produkt	k1gInPc2	produkt
proto	proto	k8xC	proto
označují	označovat	k5eAaImIp3nP	označovat
nemodifikované	modifikovaný	k2eNgInPc1d1	nemodifikovaný
produkty	produkt	k1gInPc1	produkt
značkou	značka	k1gFnSc7	značka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sojové	sojový	k2eAgFnSc2d1	sojová
mléko	mléko	k1gNnSc4	mléko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
