<s>
Sojové	sojový	k2eAgNnSc1d1	sojové
mléko	mléko	k1gNnSc1	mléko
či	či	k8xC	či
sojový	sojový	k2eAgInSc1d1	sojový
nápoj	nápoj	k1gInSc1	nápoj
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
nápoj	nápoj	k1gInSc4	nápoj
vyráběný	vyráběný	k2eAgInSc4d1	vyráběný
ze	z	k7c2	z
sóji	sója	k1gFnSc2	sója
namáčením	namáčení	k1gNnSc7	namáčení
<g/>
,	,	kIx,	,
drcením	drcení	k1gNnSc7	drcení
<g/>
,	,	kIx,	,
vařením	vaření	k1gNnSc7	vaření
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
přecezením	přecezení	k1gNnSc7	přecezení
<g/>
.	.	kIx.	.
</s>
