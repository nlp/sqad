<s>
Hlavnička	hlavnička	k1gFnSc1	hlavnička
(	(	kIx(	(
<g/>
také	také	k9	také
zhoubná	zhoubný	k2eAgFnSc1d1	zhoubná
katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
coryza	coryza	k1gFnSc1	coryza
gangraenosa	gangraenosa	k1gFnSc1	gangraenosa
bovum	bovum	k1gInSc1	bovum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
virové	virový	k2eAgNnSc1d1	virové
<g/>
,	,	kIx,	,
akutní	akutní	k2eAgNnSc1d1	akutní
onemocnění	onemocnění	k1gNnSc1	onemocnění
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
buvolů	buvol	k1gMnPc2	buvol
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
fibrinózním	fibrinózní	k2eAgInSc7d1	fibrinózní
zánětem	zánět	k1gInSc7	zánět
sliznic	sliznice	k1gFnPc2	sliznice
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
patologickými	patologický	k2eAgFnPc7d1	patologická
změnami	změna	k1gFnPc7	změna
na	na	k7c6	na
očích	oko	k1gNnPc6	oko
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
nervovými	nervový	k2eAgInPc7d1	nervový
příznaky	příznak	k1gInPc7	příznak
<g/>
.	.	kIx.	.
</s>

