<s>
Hlavnička	hlavnička	k1gFnSc1	hlavnička
(	(	kIx(	(
<g/>
také	také	k9	také
zhoubná	zhoubný	k2eAgFnSc1d1	zhoubná
katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
coryza	coryza	k1gFnSc1	coryza
gangraenosa	gangraenosa	k1gFnSc1	gangraenosa
bovum	bovum	k1gInSc1	bovum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
virové	virový	k2eAgNnSc1d1	virové
<g/>
,	,	kIx,	,
akutní	akutní	k2eAgNnSc1d1	akutní
onemocnění	onemocnění	k1gNnSc1	onemocnění
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
buvolů	buvol	k1gMnPc2	buvol
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
fibrinózním	fibrinózní	k2eAgInSc7d1	fibrinózní
zánětem	zánět	k1gInSc7	zánět
sliznic	sliznice	k1gFnPc2	sliznice
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
patologickými	patologický	k2eAgFnPc7d1	patologická
změnami	změna	k1gFnPc7	změna
na	na	k7c6	na
očích	oko	k1gNnPc6	oko
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
nervovými	nervový	k2eAgInPc7d1	nervový
příznaky	příznak	k1gInPc7	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
charakter	charakter	k1gInSc4	charakter
hromadného	hromadný	k2eAgNnSc2d1	hromadné
nakažlivého	nakažlivý	k2eAgNnSc2d1	nakažlivé
onemocnění	onemocnění	k1gNnSc2	onemocnění
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
spíše	spíše	k9	spíše
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
DNA	dna	k1gFnSc1	dna
virus	virus	k1gInSc4	virus
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Herpesviridae	Herpesvirida	k1gFnSc2	Herpesvirida
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dáván	dávat	k5eAaImNgMnS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
přenosem	přenos	k1gInSc7	přenos
viru	vir	k1gInSc2	vir
od	od	k7c2	od
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
infekce	infekce	k1gFnSc1	infekce
bez	bez	k7c2	bez
příznaků	příznak	k1gInPc2	příznak
<g/>
.	.	kIx.	.
perakutní	perakutní	k2eAgFnSc1d1	perakutní
forma	forma	k1gFnSc1	forma
–	–	k?	–
horečka	horečka	k1gFnSc1	horečka
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
42	[number]	k4	42
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
krváceniny	krvácenin	k2eAgInPc1d1	krvácenin
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
sliznicích	sliznice	k1gFnPc6	sliznice
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc4	zánět
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
náhlý	náhlý	k2eAgInSc4d1	náhlý
úhyn	úhyn	k1gInSc4	úhyn
hlavová	hlavový	k2eAgFnSc1d1	hlavová
a	a	k8xC	a
oční	oční	k2eAgFnSc1d1	oční
forma	forma	k1gFnSc1	forma
–	–	k?	–
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
léze	léze	k1gFnPc1	léze
na	na	k7c4	na
rohovce	rohovec	k1gInPc4	rohovec
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc4	zánět
rohovky	rohovka	k1gFnSc2	rohovka
přechází	přecházet	k5eAaImIp3nS	přecházet
až	až	k9	až
ve	v	k7c4	v
vředy	vřed	k1gInPc4	vřed
na	na	k7c6	na
rohovce	rohovka	k1gFnSc6	rohovka
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hlenohnisavému	hlenohnisavý	k2eAgInSc3d1	hlenohnisavý
zánětu	zánět	k1gInSc3	zánět
sliznic	sliznice	k1gFnPc2	sliznice
nosu	nos	k1gInSc2	nos
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
výtokem	výtok	k1gInSc7	výtok
nosu	nos	k1gInSc2	nos
<g/>
.	.	kIx.	.
</s>
<s>
Zánět	zánět	k1gInSc1	zánět
může	moct	k5eAaImIp3nS	moct
postihnout	postihnout	k5eAaPmF	postihnout
i	i	k9	i
čelní	čelní	k2eAgFnSc4d1	čelní
kost	kost	k1gFnSc4	kost
a	a	k8xC	a
kostní	kostní	k2eAgInSc4d1	kostní
základ	základ	k1gInSc4	základ
rohů	roh	k1gInPc2	roh
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
otoku	otok	k1gInSc3	otok
hrtanu	hrtan	k1gInSc2	hrtan
a	a	k8xC	a
zánětu	zánět	k1gInSc3	zánět
sliznic	sliznice	k1gFnPc2	sliznice
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
a	a	k8xC	a
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
.	.	kIx.	.
střevní	střevní	k2eAgFnSc1d1	střevní
forma	forma	k1gFnSc1	forma
–	–	k?	–
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	s	k7c7	s
průjmem	průjem	k1gInSc7	průjem
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
krvavým	krvavý	k2eAgMnPc3d1	krvavý
kožní	kožní	k2eAgFnSc1d1	kožní
forma	forma	k1gFnSc1	forma
–	–	k?	–
vezikulární	vezikulární	k2eAgFnPc4d1	vezikulární
léze	léze	k1gFnPc4	léze
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
vemene	vemeno	k1gNnSc2	vemeno
kolem	kolem	k7c2	kolem
paznehtů	pazneht	k1gInPc2	pazneht
nervová	nervový	k2eAgFnSc1d1	nervová
forma	forma	k1gFnSc1	forma
–	–	k?	–
křeče	křeč	k1gFnSc2	křeč
<g/>
,	,	kIx,	,
deprese	deprese	k1gFnSc2	deprese
<g/>
,	,	kIx,	,
svalový	svalový	k2eAgInSc1d1	svalový
třes	třes	k1gInSc1	třes
<g/>
,	,	kIx,	,
skřípaní	skřípaný	k2eAgMnPc1d1	skřípaný
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
parézy	paréza	k1gFnSc2	paréza
až	až	k8xS	až
paralýzy	paralýza	k1gFnSc2	paralýza
Všechny	všechen	k3xTgFnPc4	všechen
formy	forma	k1gFnPc4	forma
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vzájemně	vzájemně	k6eAd1	vzájemně
prolínat	prolínat	k5eAaImF	prolínat
<g/>
.	.	kIx.	.
</s>
<s>
ŠTĚRBA	štěrba	k1gFnSc1	štěrba
O.	O.	kA	O.
Virové	virový	k2eAgFnSc2d1	virová
choroby	choroba	k1gFnSc2	choroba
spárkaté	spárkatý	k2eAgFnSc2d1	spárkatá
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
z	z	k7c2	z
epizootologie	epizootologie	k1gFnSc2	epizootologie
<g/>
,	,	kIx,	,
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc4	Brno
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hlavnička	hlavnička	k1gFnSc1	hlavnička
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
