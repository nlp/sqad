<s>
Tančící	tančící	k2eAgInSc1d1	tančící
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
Ginger	Ginger	k1gMnSc1	Ginger
and	and	k?	and
Fred	Fred	k1gMnSc1	Fred
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
jménem	jméno	k1gNnSc7	jméno
původně	původně	k6eAd1	původně
Nationale	Nationale	k1gMnSc1	Nationale
Nederlanden	Nederlandna	k1gFnPc2	Nederlandna
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
dokončený	dokončený	k2eAgInSc1d1	dokončený
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Vltavy	Vltava	k1gFnSc2	Vltava
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
Rašínova	Rašínův	k2eAgNnSc2d1	Rašínovo
nábřeží	nábřeží	k1gNnSc2	nábřeží
a	a	k8xC	a
Jiráskova	Jiráskův	k2eAgNnSc2d1	Jiráskovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
svých	svůj	k3xOyFgFnPc2	svůj
dvou	dva	k4xCgFnPc2	dva
nárožních	nárožní	k2eAgFnPc2d1	nárožní
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
inspirovaných	inspirovaný	k2eAgInPc2d1	inspirovaný
slavným	slavný	k2eAgInSc7d1	slavný
meziválečným	meziválečný	k2eAgInSc7d1	meziválečný
tanečním	taneční	k2eAgInSc7d1	taneční
párem	pár	k1gInSc7	pár
Freda	Freda	k1gMnSc1	Freda
Astaira	Astaira	k1gMnSc1	Astaira
a	a	k8xC	a
Ginger	Ginger	k1gMnSc1	Ginger
Rogersové	Rogersová	k1gFnSc2	Rogersová
<g/>
.	.	kIx.	.
</s>
<s>
Tančící	tančící	k2eAgInSc1d1	tančící
dům	dům	k1gInSc1	dům
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Vlado	Vlado	k1gNnSc4	Vlado
Milunić	Milunić	k1gFnSc2	Milunić
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Frankem	frank	k1gInSc7	frank
O.	O.	kA	O.
Gehrym	Gehrym	k1gInSc1	Gehrym
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
k	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
přizval	přizvat	k5eAaPmAgMnS	přizvat
investor	investor	k1gMnSc1	investor
<g/>
.	.	kIx.	.
</s>
<s>
Interiéry	interiér	k1gInPc1	interiér
kanceláří	kancelář	k1gFnPc2	kancelář
investora	investor	k1gMnSc4	investor
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
části	část	k1gFnSc2	část
svěřeny	svěřit	k5eAaPmNgFnP	svěřit
britské	britský	k2eAgFnSc6d1	britská
architektce	architektka	k1gFnSc6	architektka
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Evě	Eva	k1gFnSc3	Eva
Jiřičné	Jiřičný	k2eAgFnPc1d1	Jiřičná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
stával	stávat	k5eAaImAgInS	stávat
činžovní	činžovní	k2eAgInSc1d1	činžovní
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
omylem	omylem	k6eAd1	omylem
zničen	zničit	k5eAaPmNgInS	zničit
zásahem	zásah	k1gInSc7	zásah
americké	americký	k2eAgFnPc4d1	americká
bomby	bomba	k1gFnPc4	bomba
při	při	k7c6	při
leteckém	letecký	k2eAgNnSc6d1	letecké
bombardování	bombardování	k1gNnSc6	bombardování
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
zástavbě	zástavba	k1gFnSc6	zástavba
proluky	proluka	k1gFnSc2	proluka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
proluka	proluka	k1gFnSc1	proluka
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
myšlenku	myšlenka	k1gFnSc4	myšlenka
zástavby	zástavba	k1gFnSc2	zástavba
obnovil	obnovit	k5eAaPmAgInS	obnovit
Vlado	Vlado	k1gNnSc4	Vlado
Milunić	Milunić	k1gFnSc2	Milunić
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Havlem	Havel	k1gMnSc7	Havel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
sousedním	sousední	k2eAgInSc6d1	sousední
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Havlem	Havel	k1gMnSc7	Havel
přišel	přijít	k5eAaPmAgMnS	přijít
Milunić	Milunić	k1gMnSc1	Milunić
do	do	k7c2	do
styku	styk	k1gInSc2	styk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
rozdělení	rozdělení	k1gNnSc2	rozdělení
bytu	byt	k1gInSc2	byt
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
bavili	bavit	k5eAaImAgMnP	bavit
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
dům	dům	k1gInSc4	dům
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaImF	stát
vedle	vedle	k6eAd1	vedle
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
idea	idea	k1gFnSc1	idea
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
vznik	vznik	k1gInSc4	vznik
domu	dům	k1gInSc2	dům
s	s	k7c7	s
knihovnou	knihovna	k1gFnSc7	knihovna
<g/>
,	,	kIx,	,
divadlem	divadlo	k1gNnSc7	divadlo
a	a	k8xC	a
kavárnou	kavárna	k1gFnSc7	kavárna
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgInS	zařadit
do	do	k7c2	do
kulturní	kulturní	k2eAgFnSc2d1	kulturní
linie	linie	k1gFnSc2	linie
od	od	k7c2	od
Rudolfina	Rudolfinum	k1gNnSc2	Rudolfinum
přes	přes	k7c4	přes
Národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
k	k	k7c3	k
Mánesu	Mánes	k1gMnSc3	Mánes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
záměr	záměr	k1gInSc4	záměr
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
najít	najít	k5eAaPmF	najít
investora	investor	k1gMnSc4	investor
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
námětech	námět	k1gInPc6	námět
Milunić	Milunić	k1gFnSc2	Milunić
počítal	počítat	k5eAaImAgMnS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
budova	budova	k1gFnSc1	budova
měla	mít	k5eAaImAgFnS	mít
vyklánět	vyklánět	k5eAaImF	vyklánět
nad	nad	k7c4	nad
křižovatku	křižovatka	k1gFnSc4	křižovatka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
stav	stav	k1gInSc4	stav
československé	československý	k2eAgFnSc2d1	Československá
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
totalitní	totalitní	k2eAgFnSc2d1	totalitní
strnulosti	strnulost	k1gFnSc2	strnulost
dala	dát	k5eAaPmAgFnS	dát
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
vizí	vize	k1gFnSc7	vize
statické	statický	k2eAgFnSc2d1	statická
věže	věž	k1gFnSc2	věž
vzadu	vzadu	k6eAd1	vzadu
a	a	k8xC	a
dynamické	dynamický	k2eAgInPc1d1	dynamický
vpředu	vpředu	k6eAd1	vpředu
<g/>
.	.	kIx.	.
</s>
<s>
Milunić	Milunić	k?	Milunić
nejprve	nejprve	k6eAd1	nejprve
chtěl	chtít	k5eAaImAgMnS	chtít
získat	získat	k5eAaPmF	získat
Jeana	Jean	k1gMnSc2	Jean
Nouvela	Nouvel	k1gMnSc2	Nouvel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jehož	jenž	k3xRgInSc2	jenž
návrhu	návrh	k1gInSc2	návrh
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
postaven	postavit	k5eAaPmNgInS	postavit
stejným	stejný	k2eAgMnSc7d1	stejný
investorem	investor	k1gMnSc7	investor
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
Anděl	Anděl	k1gMnSc1	Anděl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
Gehry	Gehra	k1gFnSc2	Gehra
prý	prý	k9	prý
nabídku	nabídka	k1gFnSc4	nabídka
přijal	přijmout	k5eAaPmAgMnS	přijmout
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dala	dát	k5eAaPmAgFnS	dát
Americe	Amerika	k1gFnSc6	Amerika
Jaromíra	Jaromír	k1gMnSc4	Jaromír
Jágra	Jágr	k1gMnSc4	Jágr
<g/>
,	,	kIx,	,
udělá	udělat	k5eAaPmIp3nS	udělat
cokoli	cokoli	k3yInSc1	cokoli
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
prosadit	prosadit	k5eAaPmF	prosadit
takovouto	takovýto	k3xDgFnSc4	takovýto
stavbu	stavba	k1gFnSc4	stavba
přičítá	přičítat	k5eAaImIp3nS	přičítat
Milunić	Milunić	k1gFnSc1	Milunić
porevoluční	porevoluční	k2eAgFnSc1d1	porevoluční
euforii	euforie	k1gFnSc3	euforie
<g/>
,	,	kIx,	,
Václavu	Václav	k1gMnSc3	Václav
Havlovi	Havel	k1gMnSc3	Havel
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
ředitelce	ředitelka	k1gFnSc3	ředitelka
památkářů	památkář	k1gMnPc2	památkář
Věře	Věra	k1gFnSc3	Věra
Millerové	Millerové	k2eAgMnPc1d1	Millerové
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzácné	vzácný	k2eAgFnSc6d1	vzácná
souhře	souhra	k1gFnSc6	souhra
náhod	náhoda	k1gFnPc2	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
pozemek	pozemka	k1gFnPc2	pozemka
koupila	koupit	k5eAaPmAgFnS	koupit
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
Nationale	Nationale	k1gFnSc2	Nationale
Nederlanden	Nederlandna	k1gFnPc2	Nederlandna
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1994	[number]	k4	1994
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
budovy	budova	k1gFnSc2	budova
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Investorem	investor	k1gMnSc7	investor
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
Nationale	Nationale	k1gFnSc2	Nationale
Nederlanden	Nederlandna	k1gFnPc2	Nederlandna
Real	Real	k1gInSc1	Real
estate	estat	k1gMnSc5	estat
<g/>
,	,	kIx,	,
v.	v.	k?	v.
o.	o.	k?	o.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
generálním	generální	k2eAgMnSc7d1	generální
projektantem	projektant	k1gMnSc7	projektant
ATIPA	ATIPA	kA	ATIPA
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgMnSc7d1	hlavní
dodavatelem	dodavatel	k1gMnSc7	dodavatel
belgická	belgický	k2eAgFnSc1d1	belgická
společnost	společnost	k1gFnSc1	společnost
BESIX	BESIX	kA	BESIX
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
železobetonové	železobetonový	k2eAgFnSc6d1	železobetonová
desce	deska	k1gFnSc6	deska
podporované	podporovaný	k2eAgFnSc2d1	podporovaná
soustavou	soustava	k1gFnSc7	soustava
vrtaných	vrtaný	k2eAgFnPc2d1	vrtaná
pilot	pilota	k1gFnPc2	pilota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
železobetonové	železobetonový	k2eAgFnSc6d1	železobetonová
konstrukci	konstrukce	k1gFnSc6	konstrukce
je	být	k5eAaImIp3nS	být
připevněno	připevnit	k5eAaPmNgNnS	připevnit
99	[number]	k4	99
originálních	originální	k2eAgInPc2d1	originální
fasádních	fasádní	k2eAgInPc2d1	fasádní
panelů	panel	k1gInPc2	panel
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnPc1	průčelí
budovy	budova	k1gFnSc2	budova
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
připomínající	připomínající	k2eAgFnPc1d1	připomínající
taneční	taneční	k1gFnPc1	taneční
pár	pár	k4xCyI	pár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
věže	věž	k1gFnSc2	věž
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Fred	Fred	k1gMnSc1	Fred
je	on	k3xPp3gFnPc4	on
kopule	kopule	k1gFnPc4	kopule
s	s	k7c7	s
konstrukcí	konstrukce	k1gFnSc7	konstrukce
z	z	k7c2	z
trubek	trubka	k1gFnPc2	trubka
potažená	potažený	k2eAgFnSc1d1	potažená
nerezovou	rezový	k2eNgFnSc7d1	nerezová
síťovinou	síťovina	k1gFnSc7	síťovina
-	-	kIx~	-
hlava	hlava	k1gFnSc1	hlava
medúzy	medúza	k1gFnSc2	medúza
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
má	mít	k5eAaImIp3nS	mít
devět	devět	k4xCc4	devět
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
,	,	kIx,	,
místnosti	místnost	k1gFnPc1	místnost
jsou	být	k5eAaImIp3nP	být
nesymetrické	symetrický	k2eNgFnPc1d1	nesymetrická
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
stěny	stěna	k1gFnPc1	stěna
šikmé	šikmý	k2eAgFnPc1d1	šikmá
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
z	z	k7c2	z
uliční	uliční	k2eAgFnSc2d1	uliční
čáry	čára	k1gFnSc2	čára
do	do	k7c2	do
linie	linie	k1gFnSc2	linie
chodníku	chodník	k1gInSc2	chodník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
6	[number]	k4	6
podlažích	podlaží	k1gNnPc6	podlaží
2965	[number]	k4	2965
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgFnPc2d1	čtvereční
využitelných	využitelný	k2eAgFnPc2d1	využitelná
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
patře	patro	k1gNnSc6	patro
je	být	k5eAaImIp3nS	být
restaurace	restaurace	k1gFnSc1	restaurace
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
679	[number]	k4	679
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
,	,	kIx,	,
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
a	a	k8xC	a
suterénu	suterén	k1gInSc6	suterén
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
zamýšlených	zamýšlený	k2eAgInPc6d1	zamýšlený
jako	jako	k8xS	jako
prodejní	prodejní	k2eAgFnSc2d1	prodejní
plochy	plocha	k1gFnSc2	plocha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
konferenční	konferenční	k2eAgNnSc1d1	konferenční
středisko	středisko	k1gNnSc1	středisko
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
prostory	prostor	k1gInPc1	prostor
kongresového	kongresový	k2eAgNnSc2d1	Kongresové
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
galerie	galerie	k1gFnPc1	galerie
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
také	také	k9	také
přeměněny	přeměnit	k5eAaPmNgInP	přeměnit
na	na	k7c4	na
kanceláře	kancelář	k1gFnPc4	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Přízemí	přízemí	k1gNnSc1	přízemí
je	být	k5eAaImIp3nS	být
řešeno	řešit	k5eAaImNgNnS	řešit
jako	jako	k8xC	jako
velkoprostor	velkoprostor	k1gInSc1	velkoprostor
s	s	k7c7	s
přístupem	přístup	k1gInSc7	přístup
od	od	k7c2	od
hlavní	hlavní	k2eAgFnSc2d1	hlavní
recepce	recepce	k1gFnSc2	recepce
a	a	k8xC	a
z	z	k7c2	z
venku	venek	k1gInSc2	venek
od	od	k7c2	od
nábřeží	nábřeží	k1gNnSc2	nábřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kavárna	kavárna	k1gFnSc1	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
Švácha	Švácha	k1gMnSc1	Švácha
citoval	citovat	k5eAaBmAgMnS	citovat
hlasy	hlas	k1gInPc4	hlas
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
houpavých	houpavý	k2eAgFnPc2d1	houpavá
a	a	k8xC	a
swingujících	swingující	k2eAgFnPc2d1	swingující
linií	linie	k1gFnPc2	linie
domu	dům	k1gInSc2	dům
prý	prý	k9	prý
oba	dva	k4xCgMnPc1	dva
autoři	autor	k1gMnPc1	autor
zakódovali	zakódovat	k5eAaPmAgMnP	zakódovat
gesto	gesto	k1gNnSc4	gesto
rozloučení	rozloučení	k1gNnSc2	rozloučení
s	s	k7c7	s
panelákovým	panelákový	k2eAgNnSc7d1	panelákové
stavebnictvím	stavebnictví	k1gNnSc7	stavebnictví
Husákovy	Husákův	k2eAgFnSc2d1	Husákova
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
Milunić	Milunić	k1gMnSc1	Milunić
po	po	k7c6	po
letech	let	k1gInPc6	let
litoval	litovat	k5eAaImAgMnS	litovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
původně	původně	k6eAd1	původně
navrhovaný	navrhovaný	k2eAgInSc1d1	navrhovaný
tvar	tvar	k1gInSc1	tvar
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
hezčí	hezký	k2eAgMnSc1d2	hezčí
a	a	k8xC	a
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Medúza	Medúza	k1gFnSc1	Medúza
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
Fred	Fred	k1gMnSc1	Fred
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
řešení	řešení	k1gNnSc4	řešení
patové	patový	k2eAgFnSc2d1	patová
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
Gehry	Gehra	k1gFnSc2	Gehra
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
prý	prý	k9	prý
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
původní	původní	k2eAgInSc4d1	původní
návrh	návrh	k1gInSc4	návrh
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
medúzy	medúza	k1gFnSc2	medúza
je	být	k5eAaImIp3nS	být
Gehry	Gehra	k1gFnPc4	Gehra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Milunić	Milunić	k1gFnSc1	Milunić
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
jeho	on	k3xPp3gInSc2	on
návrhu	návrh	k1gInSc2	návrh
na	na	k7c6	na
ukončení	ukončení	k1gNnSc6	ukončení
věže	věž	k1gFnSc2	věž
prosklenou	prosklený	k2eAgFnSc7d1	prosklená
kupolí	kupole	k1gFnSc7	kupole
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
na	na	k7c4	na
zeměkouli	zeměkoule	k1gFnSc4	zeměkoule
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
na	na	k7c6	na
sousedním	sousední	k2eAgInSc6d1	sousední
Havlově	Havlův	k2eAgInSc6d1	Havlův
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Medúza	Medúza	k1gFnSc1	Medúza
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
nakloněna	nakloněn	k2eAgFnSc1d1	nakloněna
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
úhlu	úhel	k1gInSc6	úhel
a	a	k8xC	a
ze	z	k7c2	z
křižovatky	křižovatka	k1gFnSc2	křižovatka
nevypadá	vypadat	k5eNaPmIp3nS	vypadat
jako	jako	k9	jako
koule	koule	k1gFnSc1	koule
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
něco	něco	k3yInSc1	něco
divně	divně	k6eAd1	divně
placatého	placatý	k2eAgNnSc2d1	placaté
<g/>
.	.	kIx.	.
</s>
<s>
Milunićovi	Milunića	k1gMnSc3	Milunića
se	se	k3xPyFc4	se
nelíbil	líbit	k5eNaImAgMnS	líbit
bar	bar	k1gInSc4	bar
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnPc4	restaurace
v	v	k7c6	v
horním	horní	k2eAgNnSc6d1	horní
patře	patro	k1gNnSc6	patro
ani	ani	k8xC	ani
školicí	školicí	k2eAgNnSc1d1	školicí
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
ocenil	ocenit	k5eAaPmAgMnS	ocenit
patra	patro	k1gNnSc2	patro
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Evy	Eva	k1gFnSc2	Eva
Jiřičné	Jiřičný	k2eAgFnSc2d1	Jiřičná
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
chyby	chyba	k1gFnPc4	chyba
budovy	budova	k1gFnSc2	budova
považuje	považovat	k5eAaImIp3nS	považovat
Milunić	Milunić	k1gMnSc7	Milunić
například	například	k6eAd1	například
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
vršek	vršek	k1gInSc4	vršek
od	od	k7c2	od
Palackého	Palackého	k2eAgInSc2d1	Palackého
mostu	most	k1gInSc2	most
a	a	k8xC	a
trochu	trochu	k6eAd1	trochu
i	i	k9	i
pohled	pohled	k1gInSc4	pohled
shora	shora	k6eAd1	shora
od	od	k7c2	od
Karlova	Karlův	k2eAgNnSc2d1	Karlovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
vlastním	vlastní	k2eAgInSc6d1	vlastní
původním	původní	k2eAgInSc6d1	původní
návrhu	návrh	k1gInSc6	návrh
má	mít	k5eAaImIp3nS	mít
dosud	dosud	k6eAd1	dosud
to	ten	k3xDgNnSc4	ten
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
mínění	mínění	k1gNnSc4	mínění
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Frankem	frank	k1gInSc7	frank
Gehrym	Gehrym	k1gInSc1	Gehrym
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
úžasné	úžasný	k2eAgNnSc4d1	úžasné
transoceánské	transoceánský	k2eAgNnSc4d1	transoceánský
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
financovala	financovat	k5eAaBmAgFnS	financovat
Nationale	Nationale	k1gFnSc7	Nationale
Nederlanden	Nederlandna	k1gFnPc2	Nederlandna
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
ING.	ing.	kA	ing.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
Tančící	tančící	k2eAgInSc1d1	tančící
dům	dům	k1gInSc1	dům
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Zlatým	zlatý	k2eAgMnSc7d1	zlatý
Andělem	Anděl	k1gMnSc7	Anděl
vložen	vložen	k2eAgInSc4d1	vložen
jako	jako	k8xC	jako
základní	základní	k2eAgNnSc4d1	základní
portfolio	portfolio	k1gNnSc4	portfolio
do	do	k7c2	do
investičního	investiční	k2eAgInSc2d1	investiční
fondu	fond	k1gInSc2	fond
ING	ing	kA	ing
Property	Propert	k1gInPc1	Propert
Fund	fund	k1gInSc1	fund
Central	Central	k1gMnSc5	Central
Europe	Europ	k1gMnSc5	Europ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vlastník	vlastník	k1gMnSc1	vlastník
plánoval	plánovat	k5eAaImAgMnS	plánovat
přestavbu	přestavba	k1gFnSc4	přestavba
a	a	k8xC	a
redesign	redesign	k1gInSc4	redesign
recepčního	recepční	k2eAgInSc2d1	recepční
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Vlado	Vlado	k1gNnSc1	Vlado
Milunić	Milunić	k1gFnPc2	Milunić
ale	ale	k8xC	ale
původní	původní	k2eAgFnSc4d1	původní
podobu	podoba	k1gFnSc4	podoba
recepce	recepce	k1gFnSc2	recepce
hájil	hájit	k5eAaImAgMnS	hájit
jako	jako	k9	jako
výrazně	výrazně	k6eAd1	výrazně
autorský	autorský	k2eAgInSc1d1	autorský
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nese	nést	k5eAaImIp3nS	nést
výraznou	výrazný	k2eAgFnSc4d1	výrazná
stopu	stopa	k1gFnSc4	stopa
Gehryho	Gehry	k1gMnSc2	Gehry
rukopisu	rukopis	k1gInSc2	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Správce	správce	k1gMnSc1	správce
budovy	budova	k1gFnSc2	budova
přesto	přesto	k8xC	přesto
objednal	objednat	k5eAaPmAgMnS	objednat
modernizaci	modernizace	k1gFnSc4	modernizace
u	u	k7c2	u
architekta	architekt	k1gMnSc2	architekt
M.	M.	kA	M.
Ranného	ranný	k2eAgInSc2d1	ranný
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
návrh	návrh	k1gInSc1	návrh
považoval	považovat	k5eAaImAgInS	považovat
Milunić	Milunić	k1gFnSc4	Milunić
za	za	k7c4	za
totální	totální	k2eAgNnSc4d1	totální
nepochopení	nepochopení	k1gNnSc4	nepochopení
konceptu	koncept	k1gInSc2	koncept
současné	současný	k2eAgFnSc2d1	současná
recepce	recepce	k1gFnSc2	recepce
<g/>
,	,	kIx,	,
za	za	k7c4	za
tuctovou	tuctový	k2eAgFnSc4d1	tuctová
bezcennou	bezcenný	k2eAgFnSc4d1	bezcenná
kulisu	kulisa	k1gFnSc4	kulisa
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
známe	znát	k5eAaImIp1nP	znát
z	z	k7c2	z
desítky	desítka	k1gFnSc2	desítka
nových	nový	k2eAgFnPc2d1	nová
administrativních	administrativní	k2eAgFnPc2d1	administrativní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
dosavadní	dosavadní	k2eAgMnPc1d1	dosavadní
vlastník	vlastník	k1gMnSc1	vlastník
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnSc2	společnost
CBRE	CBRE	kA	CBRE
Global	globat	k5eAaImAgMnS	globat
Investors	Investors	k1gInSc4	Investors
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
budovu	budova	k1gFnSc4	budova
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Prodej	prodej	k1gFnSc1	prodej
organizovala	organizovat	k5eAaBmAgFnS	organizovat
poradenská	poradenský	k2eAgFnSc1d1	poradenská
společnost	společnost	k1gFnSc1	společnost
Jones	Jones	k1gMnSc1	Jones
Lang	Lang	k1gMnSc1	Lang
LaSalle	LaSalle	k1gFnSc1	LaSalle
<g/>
.	.	kIx.	.
</s>
<s>
Developer	developer	k1gMnSc1	developer
Central	Central	k1gMnSc1	Central
Group	Group	k1gMnSc1	Group
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
za	za	k7c4	za
budovu	budova	k1gFnSc4	budova
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
250	[number]	k4	250
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
nabídku	nabídka	k1gFnSc4	nabídka
mírně	mírně	k6eAd1	mírně
navýšil	navýšit	k5eAaPmAgInS	navýšit
na	na	k7c4	na
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
výběrovém	výběrový	k2eAgNnSc6d1	výběrové
řízení	řízení	k1gNnSc6	řízení
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
zájemce	zájemce	k1gMnSc1	zájemce
zde	zde	k6eAd1	zde
chtěl	chtít	k5eAaImAgMnS	chtít
otevřít	otevřít	k5eAaPmF	otevřít
muzeum	muzeum	k1gNnSc4	muzeum
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
designu	design	k1gInSc2	design
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
autoři	autor	k1gMnPc1	autor
budovy	budova	k1gFnSc2	budova
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
budovu	budova	k1gFnSc4	budova
za	za	k7c4	za
13,35	[number]	k4	13,35
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
360	[number]	k4	360
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
koupila	koupit	k5eAaPmAgFnS	koupit
Pražská	pražský	k2eAgFnSc1d1	Pražská
správa	správa	k1gFnSc1	správa
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
realitní	realitní	k2eAgMnSc1d1	realitní
magnát	magnát	k1gMnSc1	magnát
Václav	Václav	k1gMnSc1	Václav
Skála	Skála	k1gMnSc1	Skála
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
vlastník	vlastník	k1gMnSc1	vlastník
chce	chtít	k5eAaImIp3nS	chtít
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
ponechat	ponechat	k5eAaPmF	ponechat
kanceláře	kancelář	k1gFnPc4	kancelář
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zvažuje	zvažovat	k5eAaImIp3nS	zvažovat
využití	využití	k1gNnSc1	využití
části	část	k1gFnSc2	část
budovy	budova	k1gFnSc2	budova
pro	pro	k7c4	pro
nekomerční	komerční	k2eNgInPc4d1	nekomerční
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
nájemníky	nájemník	k1gMnPc7	nájemník
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
prostor	prostora	k1gFnPc2	prostora
jsou	být	k5eAaImIp3nP	být
společnosti	společnost	k1gFnSc2	společnost
CBRE	CBRE	kA	CBRE
Global	globat	k5eAaImAgMnS	globat
Investors	Investors	k1gInSc4	Investors
a	a	k8xC	a
Accenture	Accentur	k1gInSc5	Accentur
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
diskuzi	diskuze	k1gFnSc4	diskuze
o	o	k7c6	o
architektuře	architektura	k1gFnSc6	architektura
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
dům	dům	k1gInSc4	dům
oceňovali	oceňovat	k5eAaImAgMnP	oceňovat
jako	jako	k9	jako
moderní	moderní	k2eAgFnSc4d1	moderní
architekturu	architektura	k1gFnSc4	architektura
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgFnSc1	jaký
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
,	,	kIx,	,
odpůrci	odpůrce	k1gMnPc1	odpůrce
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
historické	historický	k2eAgFnSc2d1	historická
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
budova	budova	k1gFnSc1	budova
nehodí	hodit	k5eNaPmIp3nS	hodit
a	a	k8xC	a
narušuje	narušovat	k5eAaImIp3nS	narušovat
městské	městský	k2eAgNnSc1d1	Městské
panorama	panorama	k1gNnSc1	panorama
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
první	první	k4xOgFnSc4	první
pražskou	pražský	k2eAgFnSc4d1	Pražská
porevoluční	porevoluční	k2eAgFnSc4d1	porevoluční
stavbu	stavba	k1gFnSc4	stavba
špičkových	špičkový	k2eAgMnPc2d1	špičkový
světových	světový	k2eAgMnPc2d1	světový
architektů	architekt	k1gMnPc2	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
města	město	k1gNnSc2	město
či	či	k8xC	či
za	za	k7c4	za
turistický	turistický	k2eAgInSc4d1	turistický
magnet	magnet	k1gInSc4	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Rostislava	Rostislav	k1gMnSc2	Rostislav
Šváchy	Švácha	k1gMnSc2	Švácha
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
laickou	laický	k2eAgFnSc4d1	laická
veřejnost	veřejnost	k1gFnSc4	veřejnost
dům	dům	k1gInSc1	dům
stal	stát	k5eAaPmAgInS	stát
ikonou	ikona	k1gFnSc7	ikona
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
a	a	k8xC	a
dal	dát	k5eAaPmAgInS	dát
éře	éra	k1gFnSc3	éra
po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
architektonickou	architektonický	k2eAgFnSc4d1	architektonická
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Hubáčkova	Hubáčkův	k2eAgFnSc1d1	Hubáčkova
věž	věž	k1gFnSc1	věž
na	na	k7c6	na
Ještědu	Ještěd	k1gInSc6	Ještěd
60	[number]	k4	60
<g/>
.	.	kIx.	.
letům	let	k1gInPc3	let
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
návštěvníky	návštěvník	k1gMnPc4	návštěvník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ikonou	ikona	k1gFnSc7	ikona
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
známou	známá	k1gFnSc4	známá
jako	jako	k8xC	jako
Karlův	Karlův	k2eAgInSc4d1	Karlův
most	most	k1gInSc4	most
či	či	k8xC	či
Svatovítská	svatovítský	k2eAgFnSc1d1	Svatovítská
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
architekti	architekt	k1gMnPc1	architekt
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
dům	dům	k1gInSc4	dům
podle	podle	k7c2	podle
Šváchy	Švácha	k1gFnSc2	Švácha
s	s	k7c7	s
takovým	takový	k3xDgNnSc7	takový
nadšením	nadšení	k1gNnSc7	nadšení
nedívali	dívat	k5eNaImAgMnP	dívat
a	a	k8xC	a
na	na	k7c4	na
organickou	organický	k2eAgFnSc4d1	organická
architekturu	architektura	k1gFnSc4	architektura
se	se	k3xPyFc4	se
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
začali	začít	k5eAaPmAgMnP	začít
ohlížet	ohlížet	k5eAaImF	ohlížet
až	až	k9	až
o	o	k7c4	o
desetiletí	desetiletí	k1gNnPc4	desetiletí
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Tančící	tančící	k2eAgInSc1d1	tančící
dům	dům	k1gInSc1	dům
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
ocenění	ocenění	k1gNnSc2	ocenění
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
designu	design	k1gInSc2	design
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
amerického	americký	k2eAgInSc2d1	americký
Time	Tim	k1gInSc2	Tim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
časopisu	časopis	k1gInSc6	časopis
Architekt	architekt	k1gMnSc1	architekt
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
mezi	mezi	k7c4	mezi
pět	pět	k4xCc4	pět
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
českých	český	k2eAgFnPc2d1	Česká
staveb	stavba	k1gFnPc2	stavba
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
vybrala	vybrat	k5eAaPmAgFnS	vybrat
Tančící	tančící	k2eAgInSc4d1	tančící
dům	dům	k1gInSc4	dům
jako	jako	k8xS	jako
příklad	příklad	k1gInSc4	příklad
současné	současný	k2eAgFnSc2d1	současná
budovy	budova	k1gFnSc2	budova
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
deseti	deset	k4xCc2	deset
mincí	mince	k1gFnPc2	mince
cyklu	cyklus	k1gInSc2	cyklus
Deset	deset	k4xCc1	deset
století	století	k1gNnPc2	století
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
prohlášením	prohlášení	k1gNnSc7	prohlášení
obchodního	obchodní	k2eAgInSc2d1	obchodní
domu	dům	k1gInSc2	dům
Máj	Mája	k1gFnPc2	Mája
za	za	k7c4	za
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgInSc1d1	hlavní
konzervátor	konzervátor	k1gInSc1	konzervátor
Národního	národní	k2eAgInSc2d1	národní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
Josef	Josefa	k1gFnPc2	Josefa
Štulc	Štulc	k1gFnSc1	Štulc
pro	pro	k7c4	pro
Lidovky	Lidovky	k1gFnPc4	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
teoreticky	teoreticky	k6eAd1	teoreticky
uvítal	uvítat	k5eAaPmAgMnS	uvítat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jako	jako	k8xC	jako
památka	památka	k1gFnSc1	památka
byl	být	k5eAaImAgInS	být
zapsán	zapsán	k2eAgMnSc1d1	zapsán
i	i	k8xC	i
Tančící	tančící	k2eAgInSc1d1	tančící
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Vlado	Vlado	k1gNnSc4	Vlado
Milunič	Milunič	k1gFnSc2	Milunič
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
se	s	k7c7	s
správcem	správce	k1gMnSc7	správce
budovy	budova	k1gFnSc2	budova
o	o	k7c6	o
přestavbě	přestavba	k1gFnSc6	přestavba
recepce	recepce	k1gFnSc2	recepce
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dům	dům	k1gInSc1	dům
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stane	stanout	k5eAaPmIp3nS	stanout
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tančící	tančící	k2eAgInSc4d1	tančící
dům	dům	k1gInSc4	dům
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Stránky	stránka	k1gFnSc2	stránka
Galerie	galerie	k1gFnSc2	galerie
Tančící	tančící	k2eAgInSc4d1	tančící
dům	dům	k1gInSc4	dům
Tančící	tančící	k2eAgInSc1d1	tančící
dům	dům	k1gInSc1	dům
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Archiweb	Archiwba	k1gFnPc2	Archiwba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
