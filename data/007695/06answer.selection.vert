<s>
Tančící	tančící	k2eAgInSc1d1	tančící
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
Ginger	Ginger	k1gMnSc1	Ginger
and	and	k?	and
Fred	Fred	k1gMnSc1	Fred
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
jménem	jméno	k1gNnSc7	jméno
původně	původně	k6eAd1	původně
Nationale	Nationale	k1gMnSc1	Nationale
Nederlanden	Nederlandna	k1gFnPc2	Nederlandna
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
dokončený	dokončený	k2eAgInSc1d1	dokončený
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Vltavy	Vltava	k1gFnSc2	Vltava
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
Rašínova	Rašínův	k2eAgNnSc2d1	Rašínovo
nábřeží	nábřeží	k1gNnSc2	nábřeží
a	a	k8xC	a
Jiráskova	Jiráskův	k2eAgNnSc2d1	Jiráskovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
