<s>
Chaloupky	chaloupka	k1gFnPc1
(	(	kIx(
<g/>
Přebuz	Přebuz	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Chaloupky	Chaloupka	k1gMnSc2
Pohled	pohled	k1gInSc1
na	na	k7c4
místo	místo	k1gNnSc4
zaniklé	zaniklý	k2eAgNnSc4d1
obceLokalita	obceLokalita	k1gFnSc1
Charakter	charakter	k1gInSc1
</s>
<s>
zaniklá	zaniklý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Přebuz	Přebuz	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Sokolov	Sokolov	k1gInSc1
Kraj	kraj	k1gInSc1
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
13	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Chaloupky	chaloupka	k1gFnPc1
u	u	k7c2
Přebuze	Přebuze	k1gFnSc2
(	(	kIx(
<g/>
4,31	4,31	k4
km²	km²	k?
<g/>
)	)	kIx)
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
830	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
Chaloupky	chaloupka	k1gFnPc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Zaniklé	zaniklý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
304	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chaloupky	chaloupka	k1gFnPc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Neuhaus	Neuhaus	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
zaniklou	zaniklý	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
v	v	k7c6
Krušných	krušný	k2eAgFnPc6d1
horách	hora	k1gFnPc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
přibližně	přibližně	k6eAd1
2,5	2,5	k4
kilometru	kilometr	k1gInSc2
východně	východně	k6eAd1
od	od	k7c2
Přebuzi	Přebuze	k1gFnSc6
v	v	k7c6
okrese	okres	k1gInSc6
Sokolov	Sokolov	k1gInSc1
v	v	k7c6
Karlovarském	karlovarský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obec	obec	k1gFnSc1
se	se	k3xPyFc4
rozprostírala	rozprostírat	k5eAaImAgFnS
po	po	k7c6
obou	dva	k4xCgInPc6
březích	břeh	k1gInPc6
horního	horní	k2eAgInSc2d1
toku	tok	k1gInSc2
Rolavy	Rolava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katastrální	katastrální	k2eAgInPc1d1
území	území	k1gNnSc4
o	o	k7c6
výměře	výměra	k1gFnSc6
4,31	4,31	k4
km²	km²	k?
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Chaloupky	chaloupka	k1gFnPc1
u	u	k7c2
Přebuze	Přebuze	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
osady	osada	k1gFnSc2
byl	být	k5eAaImAgInS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
německého	německý	k2eAgInSc2d1
názvu	název	k1gInSc2
nově	nově	k6eAd1
postaveného	postavený	k2eAgInSc2d1
domu	dům	k1gInSc2
Neuhaus	Neuhaus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterým	který	k3yRgInPc3,k3yIgInPc3,k3yQgInPc3
byla	být	k5eAaImAgFnS
kovárna	kovárna	k1gFnSc1
pro	pro	k7c4
horníky	horník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
název	název	k1gInSc1
Chaloupky	chaloupka	k1gFnSc2
byl	být	k5eAaImAgInS
stanoven	stanovit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
a	a	k8xC
vycházel	vycházet	k5eAaImAgMnS
z	z	k7c2
charakteru	charakter	k1gInSc2
zástavby	zástavba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Říčka	říčka	k1gFnSc1
Rolava	Rolava	k1gFnSc1
v	v	k7c6
místě	místo	k1gNnSc6
zaniklé	zaniklý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
</s>
<s>
V	v	k7c6
údolí	údolí	k1gNnSc6
říčky	říčka	k1gFnSc2
Rolavy	Rolava	k1gFnSc2
hledali	hledat	k5eAaImAgMnP
první	první	k4xOgMnPc1
prospektoři	prospektor	k1gMnPc1
cínovou	cínový	k2eAgFnSc4d1
rudu	ruda	k1gFnSc4
již	již	k6eAd1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
místech	místo	k1gNnPc6
ještě	ještě	k9
neexistujících	existující	k2eNgFnPc2d1
Chaloupek	chaloupka	k1gFnPc2
bylo	být	k5eAaImAgNnS
rýžování	rýžování	k1gNnSc4
cínové	cínový	k2eAgFnSc2d1
rudy	ruda	k1gFnSc2
v	v	k7c6
plném	plný	k2eAgInSc6d1
rozkvětu	rozkvět	k1gInSc6
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1400	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trvalo	trvat	k5eAaImAgNnS
však	však	k9
ještě	ještě	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
300	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
než	než	k8xS
si	se	k3xPyFc3
zde	zde	k6eAd1
lidé	člověk	k1gMnPc1
založili	založit	k5eAaPmAgMnP
osadu	osada	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
okolí	okolí	k1gNnSc6
Chaloupek	chaloupka	k1gFnPc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
plošné	plošný	k2eAgInPc1d1
pozůstatky	pozůstatek	k1gInPc1
rýžování	rýžování	k1gNnSc2
cínové	cínový	k2eAgFnSc2d1
rudy	ruda	k1gFnSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3
v	v	k7c6
celých	celý	k2eAgFnPc6d1
Krušných	krušný	k2eAgFnPc6d1
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rýžovalo	rýžovat	k5eAaImAgNnS
se	se	k3xPyFc4
nejen	nejen	k6eAd1
v	v	k7c6
okolí	okolí	k1gNnSc6
říčky	říčka	k1gFnSc2
Rolavy	Rolava	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
vysoko	vysoko	k6eAd1
na	na	k7c6
svazích	svah	k1gInPc6
a	a	k8xC
temenech	temeno	k1gNnPc6
okolních	okolní	k2eAgInPc2d1
kopců	kopec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
rýžovištím	rýžoviště	k1gNnPc3
byla	být	k5eAaImAgFnS
přiváděna	přiváděn	k2eAgFnSc1d1
voda	voda	k1gFnSc1
umělými	umělý	k2eAgInPc7d1
vodními	vodní	k2eAgInPc7d1
kanály	kanál	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
Chaloupkách	chaloupka	k1gFnPc6
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1757	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
součástí	součást	k1gFnSc7
nejdeckého	nejdecký	k2eAgNnSc2d1
panství	panství	k1gNnSc2
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgInSc3,k3yQgInSc3,k3yIgInSc3
patřily	patřit	k5eAaImAgFnP
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1850	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1850	#num#	k4
<g/>
–	–	k?
<g/>
1904	#num#	k4
(	(	kIx(
<g/>
jiný	jiný	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
1869	#num#	k4
<g/>
–	–	k?
<g/>
1890	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
osadou	osada	k1gFnSc7
obce	obec	k1gFnSc2
Rolava	Rolava	k1gFnSc1
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1904	#num#	k4
samostatnou	samostatný	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
v	v	k7c6
okrese	okres	k1gInSc6
Kraslice	kraslice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
stálo	stát	k5eAaImAgNnS
v	v	k7c6
obci	obec	k1gFnSc6
88	#num#	k4
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1910	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
byly	být	k5eAaImAgFnP
Chaloupky	chaloupka	k1gFnPc1
obcí	obec	k1gFnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Nejdek	Nejdek	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
osadou	osada	k1gFnSc7
obce	obec	k1gFnSc2
Přebuz	Přebuz	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1898	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Chaloupkách	chaloupka	k1gFnPc6
dvojtřídní	dvojtřídní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
vedle	vedle	k6eAd1
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
postaven	postavit	k5eAaPmNgInS
pomník	pomník	k1gInSc1
obětem	oběť	k1gFnPc3
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
významným	významný	k2eAgMnPc3d1
provozům	provoz	k1gInPc3
v	v	k7c6
obci	obec	k1gFnSc6
patřila	patřit	k5eAaImAgFnS
Leopoldova	Leopoldův	k2eAgFnSc1d1
parní	parní	k2eAgFnSc1d1
pila	pila	k1gFnSc1
a	a	k8xC
bednárna	bednárna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
byla	být	k5eAaImAgNnP
v	v	k7c6
provozu	provoz	k1gInSc6
ještě	ještě	k6eAd1
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
i	i	k9
když	když	k8xS
prosperovala	prosperovat	k5eAaImAgFnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
úřednickým	úřednický	k2eAgNnSc7d1
rozhodnutím	rozhodnutí	k1gNnSc7
ministerstva	ministerstvo	k1gNnSc2
průmyslu	průmysl	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
zrušena	zrušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
odsunu	odsun	k1gInSc6
německého	německý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
obec	obec	k1gFnSc1
postupně	postupně	k6eAd1
vylidňovala	vylidňovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úplný	úplný	k2eAgInSc1d1
zánik	zánik	k1gInSc1
pro	pro	k7c4
obec	obec	k1gFnSc4
znamenalo	znamenat	k5eAaImAgNnS
její	její	k3xOp3gFnSc4
začlenění	začlenění	k1gNnSc1
do	do	k7c2
hraničního	hraniční	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
zde	zde	k6eAd1
stály	stát	k5eAaImAgInP
3	#num#	k4
domy	dům	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc1
byla	být	k5eAaImAgNnP
všechna	všechen	k3xTgNnPc1
stavení	stavení	k1gNnPc1
zbourána	zbourán	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Celé	celý	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
zatopeno	zatopen	k2eAgNnSc4d1
<g/>
,	,	kIx,
naštěstí	naštěstí	k6eAd1
k	k	k7c3
tomu	ten	k3xDgNnSc3
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Při	při	k7c6
sčítání	sčítání	k1gNnSc6
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
440	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
198	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
byl	být	k5eAaImAgMnS
jeden	jeden	k4xCgMnSc1
Čechoslovák	Čechoslovák	k1gMnSc1
a	a	k8xC
439	#num#	k4
Němců	Němec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
jednoho	jeden	k4xCgMnSc2
člena	člen	k1gMnSc2
církve	církev	k1gFnSc2
československé	československý	k2eAgFnSc2d1
a	a	k8xC
jednoho	jeden	k4xCgMnSc2
člena	člen	k1gMnSc2
nezjišťovaných	zjišťovaný	k2eNgFnPc2d1
církví	církev	k1gFnPc2
byli	být	k5eAaImAgMnP
římskými	římský	k2eAgMnPc7d1
katolíky	katolík	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
měla	mít	k5eAaImAgFnS
vesnice	vesnice	k1gFnSc1
359	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
:	:	kIx,
jednoho	jeden	k4xCgMnSc2
Čechoslováka	Čechoslovák	k1gMnSc2
<g/>
,	,	kIx,
352	#num#	k4
Němců	Němec	k1gMnPc2
a	a	k8xC
šest	šest	k4xCc1
cizinců	cizinec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
vyznání	vyznání	k1gNnPc2
bylo	být	k5eAaImAgNnS
27	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
pět	pět	k4xCc1
jich	on	k3xPp3gMnPc2
patřilo	patřit	k5eAaImAgNnS
k	k	k7c3
nezjišťovaným	zjišťovaný	k2eNgFnPc3d1
církvím	církev	k1gFnPc3
a	a	k8xC
ostatní	ostatní	k2eAgMnPc1d1
se	se	k3xPyFc4
hlásili	hlásit	k5eAaImAgMnP
k	k	k7c3
římskokatolické	římskokatolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
domů	dům	k1gInPc2
Chaloupek	chaloupka	k1gFnPc2
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
<g/>
18691880189019001910192119301950196119701980199120012011	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
54853354556551544035913000000	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
domů	dům	k1gInPc2
</s>
<s>
6382808878767982000000	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
900	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
2394	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
280	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sbírka	sbírka	k1gFnSc1
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výnos	výnos	k1gInSc1
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
č.	č.	k?
494	#num#	k4
<g/>
/	/	kIx~
<g/>
1947	#num#	k4
Ú.l.	Ú.l.	k1gFnPc2
<g/>
,	,	kIx,
o	o	k7c4
stanovení	stanovení	k1gNnSc4
nových	nový	k2eAgInPc2d1
úředních	úřední	k2eAgInPc2d1
názvů	název	k1gInPc2
míst	místo	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1947-05-03	1947-05-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Územně	územně	k6eAd1
identifikační	identifikační	k2eAgInSc4d1
registr	registr	k1gInSc4
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Územně	územně	k6eAd1
identifikační	identifikační	k2eAgInSc1d1
registr	registr	k1gInSc1
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
BERANOVÁ	Beranová	k1gFnSc1
VAICOVÁ	VAICOVÁ	kA
<g/>
,	,	kIx,
Romana	Romana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaniklé	zaniklý	k2eAgFnPc4d1
obce	obec	k1gFnPc4
na	na	k7c6
Sokolovsku	Sokolovsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sokolov	Sokolov	k1gInSc1
<g/>
:	:	kIx,
Krajské	krajský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
Sokolov	Sokolov	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86630	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
193	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ROJÍK	rojík	k1gInSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
cínového	cínový	k2eAgNnSc2d1
dobývání	dobývání	k1gNnSc2
v	v	k7c6
západním	západní	k2eAgNnSc6d1
Krušnohoří	Krušnohoří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sokolov	Sokolov	k1gInSc1
<g/>
:	:	kIx,
Muzeum	muzeum	k1gNnSc1
Sokolov	Sokolovo	k1gNnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
238	#num#	k4
<g/>
-	-	kIx~
<g/>
6095	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
118	#num#	k4
<g/>
-	-	kIx~
<g/>
120	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PROKOP	Prokop	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
SMOLA	Smola	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sokolovsko	Sokolovsko	k1gNnSc1
<g/>
:	:	kIx,
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
památky	památka	k1gFnPc1
a	a	k8xC
umělci	umělec	k1gMnPc1
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sokolov	Sokolov	k1gInSc1
<g/>
:	:	kIx,
AZUS	AZUS	kA
Březová	březový	k2eAgFnSc1d1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
svazky	svazek	k1gInPc4
(	(	kIx(
<g/>
878	#num#	k4
s.	s.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
905485	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904960	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
597	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
624	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
1310	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
185	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kolektiv	kolektivum	k1gNnPc2
autorů	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmizelé	zmizelý	k2eAgFnPc4d1
Sudety	Sudety	k1gFnPc4
=	=	kIx~
Das	Das	k1gMnSc5
verschwundene	verschwunden	k1gMnSc5
Sudetenland	Sudetenlanda	k1gFnPc2
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Antikomplex	Antikomplex	k1gInSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
727	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
906198	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Krušné	krušný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
326	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
Republice	republika	k1gFnSc6
Československé	československý	k2eAgNnSc1d1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
I.	I.	kA
Čechy	Čechy	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
statistický	statistický	k2eAgInSc1d1
<g/>
,	,	kIx,
1924	#num#	k4
<g/>
.	.	kIx.
596	#num#	k4
s.	s.	k?
S.	S.	kA
253	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
Republice	republika	k1gFnSc6
Československé	československý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
I.	I.	kA
Země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
statistický	statistický	k2eAgInSc1d1
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
.	.	kIx.
614	#num#	k4
s.	s.	k?
S.	S.	kA
233	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
Okres	okres	k1gInSc1
Sokolov	Sokolov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Chaloupky	chaloupka	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
katastru	katastr	k1gInSc2
Chaloupky	chaloupka	k1gFnSc2
u	u	k7c2
Přebuze	Přebuze	k1gFnSc2
na	na	k7c6
webu	web	k1gInSc6
ČÚZK	ČÚZK	kA
</s>
<s>
Neuhaus	Neuhaus	k1gInSc1
im	im	k?
Erzgebirge	Erzgebirge	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Chaloupky	chaloupka	k1gFnPc1
na	na	k7c6
zanikleobce	zanikleobka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnPc1
města	město	k1gNnSc2
Přebuz	Přebuza	k1gFnPc2
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
Přebuz	Přebuz	k1gMnSc1
Ostatní	ostatní	k2eAgMnSc1d1
</s>
<s>
ChaloupkyRolava	ChaloupkyRolava	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
