<s>
Divadlo	divadlo	k1gNnSc1
Mír	Míra	k1gFnPc2
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Mír	mír	k1gInSc1
Divadlo	divadlo	k1gNnSc1
Mír	mír	k1gInSc1
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
Vítkovicích	Vítkovice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čelní	čelní	k2eAgMnPc1d1
stranaZákladní	stranaZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Místo	místo	k7c2
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
Typ	typ	k1gInSc1
divadla	divadlo	k1gNnSc2
</s>
<s>
profesionální	profesionální	k2eAgNnSc4d1
divadlo	divadlo	k1gNnSc4
Zaměření	zaměření	k1gNnSc2
</s>
<s>
činohra	činohra	k1gFnSc1
Zřizovatel	zřizovatel	k1gMnSc1
</s>
<s>
soukromá	soukromý	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Budova	budova	k1gFnSc1
Otevření	otevření	k1gNnSc5
</s>
<s>
2017	#num#	k4
Osobnosti	osobnost	k1gFnPc1
Ředitel	ředitel	k1gMnSc1
</s>
<s>
Albert	Albert	k1gMnSc1
Čuba	čuba	k1gFnSc1
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
44,14	44,14	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
16	#num#	k4
<g/>
′	′	k?
<g/>
10,29	10,29	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Adresa	adresa	k1gFnSc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
MírHalasova	MírHalasův	k2eAgNnSc2d1
19703	#num#	k4
00	#num#	k4
Ostrava-Vítkovice	Ostrava-Vítkovice	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
http://www.divadlomir.cz/	http://www.divadlomir.cz/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Mír	Míra	k1gFnPc2
je	být	k5eAaImIp3nS
nejmladší	mladý	k2eAgNnSc4d3
ostravské	ostravský	k2eAgNnSc4d1
profesionální	profesionální	k2eAgNnSc4d1
divadlo	divadlo	k1gNnSc4
<g/>
,	,	kIx,
zaměřující	zaměřující	k2eAgFnSc4d1
se	se	k3xPyFc4
především	především	k9
na	na	k7c4
činohru	činohra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divadlo	divadlo	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
Vítkovice	Vítkovice	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
budovy	budova	k1gFnSc2
</s>
<s>
Budova	budova	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
divadlo	divadlo	k1gNnSc4
sídlí	sídlet	k5eAaImIp3nP
<g/>
,	,	kIx,
byla	být	k5eAaImAgNnP
postavena	postavit	k5eAaPmNgNnP
na	na	k7c6
začátku	začátek	k1gInSc6
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
jako	jako	k8xC,k8xS
sokolský	sokolský	k2eAgInSc1d1
biograf	biograf	k1gInSc1
pod	pod	k7c7
názvem	název	k1gInSc7
Bio	Bio	k?
Rekord	rekord	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
výstavbu	výstavba	k1gFnSc4
se	se	k3xPyFc4
zasloužil	zasloužit	k5eAaPmAgMnS
zejména	zejména	k9
pplk.	pplk.	kA
MUDr.	MUDr.	kA
Ludvík	Ludvík	k1gMnSc1
Klega	Klega	k1gFnSc1
<g/>
,	,	kIx,
legionář	legionář	k1gMnSc1
a	a	k8xC
starosta	starosta	k1gMnSc1
Sokola	Sokol	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
tzv.	tzv.	kA
první	první	k4xOgFnPc1
republiky	republika	k1gFnPc1
nemohly	moct	k5eNaImAgFnP
získat	získat	k5eAaPmF
koncesi	koncese	k1gFnSc4
k	k	k7c3
provozování	provozování	k1gNnSc3
biografu	biograf	k1gInSc2
soukromé	soukromý	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
obecně	obecně	k6eAd1
prospěšné	prospěšný	k2eAgInPc1d1
spolky	spolek	k1gInPc1
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
právě	právě	k9
Sokol	Sokol	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Návrh	návrh	k1gInSc1
a	a	k8xC
realizaci	realizace	k1gFnSc3
stavby	stavba	k1gFnSc2
měla	mít	k5eAaImAgFnS
na	na	k7c6
starosti	starost	k1gFnSc6
projekční	projekční	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
Kolář	Kolář	k1gMnSc1
&	&	k?
Rubý	Rubý	k1gMnSc1
–	–	k?
architekti	architekt	k1gMnPc1
František	František	k1gMnSc1
Kolář	Kolář	k1gMnSc1
a	a	k8xC
Jan	Jan	k1gMnSc1
Rubý	Rubý	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
podíleli	podílet	k5eAaImAgMnP
také	také	k9
na	na	k7c6
budově	budova	k1gFnSc6
Revírní	revírní	k2eAgFnSc2d1
bratrské	bratrský	k2eAgFnSc2d1
pokladny	pokladna	k1gFnSc2
na	na	k7c4
ulici	ulice	k1gFnSc4
Českobratrská	českobratrský	k2eAgFnSc1d1
<g/>
,	,	kIx,
dnešního	dnešní	k2eAgInSc2d1
hotelu	hotel	k1gInSc2
Mercure	Mercur	k1gMnSc5
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
spolupracovali	spolupracovat	k5eAaImAgMnP
na	na	k7c6
projektu	projekt	k1gInSc6
ostravské	ostravský	k2eAgFnSc2d1
Nové	Nové	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
také	také	k9
projektanti	projektant	k1gMnPc1
známého	známý	k2eAgNnSc2d1
ostravského	ostravský	k2eAgNnSc2d1
kina	kino	k1gNnSc2
Vesmír	vesmír	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
vyprávění	vyprávění	k1gNnPc2
pamětníků	pamětník	k1gMnPc2
byla	být	k5eAaImAgFnS
budova	budova	k1gFnSc1
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
zasažena	zasažen	k2eAgFnSc1d1
bombou	bomba	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
však	však	k9
nevybuchla	vybuchnout	k5eNaPmAgFnS
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
kinetickou	kinetický	k2eAgFnSc7d1
silou	síla	k1gFnSc7
zdemolovala	zdemolovat	k5eAaPmAgFnS
střechu	střecha	k1gFnSc4
a	a	k8xC
hlediště	hlediště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
byla	být	k5eAaImAgFnS
budova	budova	k1gFnSc1
opravena	opravit	k5eAaPmNgFnS
a	a	k8xC
kino	kino	k1gNnSc1
opět	opět	k6eAd1
uvedeno	uvést	k5eAaPmNgNnS
do	do	k7c2
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
komunistickém	komunistický	k2eAgInSc6d1
převratu	převrat	k1gInSc6
se	se	k3xPyFc4
kino	kino	k1gNnSc1
přejmenovalo	přejmenovat	k5eAaPmAgNnS
na	na	k7c4
Kino	kino	k1gNnSc4
Mír	Míra	k1gFnPc2
a	a	k8xC
pod	pod	k7c7
tímto	tento	k3xDgInSc7
názvem	název	k1gInSc7
promítalo	promítat	k5eAaImAgNnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
poslední	poslední	k2eAgFnSc3d1
projekci	projekce	k1gFnSc3
a	a	k8xC
následně	následně	k6eAd1
byl	být	k5eAaImAgInS
provoz	provoz	k1gInSc1
kina	kino	k1gNnSc2
ukončen	ukončit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Budovu	budova	k1gFnSc4
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
užíval	užívat	k5eAaImAgInS
gymnastický	gymnastický	k2eAgInSc1d1
klub	klub	k1gInSc1
jako	jako	k9
tělocvičnu	tělocvična	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
následně	následně	k6eAd1
i	i	k8xC
ten	ten	k3xDgMnSc1
budovu	budova	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
opustil	opustit	k5eAaPmAgInS
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
stavba	stavba	k1gFnSc1
chátrat	chátrat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
tehdejší	tehdejší	k2eAgMnPc1d1
vlastník	vlastník	k1gMnSc1
<g/>
,	,	kIx,
statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
budovu	budova	k1gFnSc4
prodal	prodat	k5eAaPmAgInS
soukromé	soukromý	k2eAgFnSc3d1
osobě	osoba	k1gFnSc3
a	a	k8xC
následně	následně	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
několika	několik	k4yIc3
dalším	další	k2eAgMnPc3d1
přeprodejům	přeprodej	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kolem	kolem	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
začala	začít	k5eAaPmAgFnS
rekonstrukce	rekonstrukce	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
prováděli	provádět	k5eAaImAgMnP
vietnamští	vietnamský	k2eAgMnPc1d1
majitelé	majitel	k1gMnPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
budovu	budova	k1gFnSc4
získali	získat	k5eAaPmAgMnP
do	do	k7c2
vlastnictví	vlastnictví	k1gNnSc2
a	a	k8xC
chtěli	chtít	k5eAaImAgMnP
v	v	k7c6
ní	on	k3xPp3gFnSc6
provozovat	provozovat	k5eAaImF
prodejnu	prodejna	k1gFnSc4
s	s	k7c7
typickým	typický	k2eAgNnSc7d1
asijským	asijský	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
rekonstrukcí	rekonstrukce	k1gFnSc7
byla	být	k5eAaImAgFnS
budova	budova	k1gFnSc1
bývalého	bývalý	k2eAgNnSc2d1
kina	kino	k1gNnSc2
Mír	mír	k1gInSc4
zachráněna	zachránit	k5eAaPmNgFnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
stav	stav	k1gInSc1
budovy	budova	k1gFnSc2
byl	být	k5eAaImAgInS
již	již	k6eAd1
v	v	k7c6
katastrofálním	katastrofální	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodejna	prodejna	k1gFnSc1
pak	pak	k6eAd1
ovšem	ovšem	k9
fungovala	fungovat	k5eAaImAgFnS
pouze	pouze	k6eAd1
chvíli	chvíle	k1gFnSc4
<g/>
,	,	kIx,
budova	budova	k1gFnSc1
opět	opět	k6eAd1
změnila	změnit	k5eAaPmAgFnS
majitele	majitel	k1gMnSc4
a	a	k8xC
nakonec	nakonec	k6eAd1
zůstala	zůstat	k5eAaPmAgFnS
prázdná	prázdný	k2eAgFnSc1d1
a	a	k8xC
nevyužívaná	využívaný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
divadla	divadlo	k1gNnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
získával	získávat	k5eAaImAgMnS
budovu	budova	k1gFnSc4
do	do	k7c2
pronájmu	pronájem	k1gInSc2
ostravský	ostravský	k2eAgMnSc1d1
divadelník	divadelník	k1gMnSc1
Albert	Albert	k1gMnSc1
Čuba	čuba	k1gFnSc1
a	a	k8xC
začal	začít	k5eAaPmAgMnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
budovat	budovat	k5eAaImF
novou	nový	k2eAgFnSc4d1
ostravskou	ostravský	k2eAgFnSc4d1
divadelní	divadelní	k2eAgFnSc4d1
scénu	scéna	k1gFnSc4
pod	pod	k7c7
názvem	název	k1gInSc7
Divadlo	divadlo	k1gNnSc1
Mír	mír	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc7
oficiální	oficiální	k2eAgFnSc7d1
premiérou	premiéra	k1gFnSc7
v	v	k7c6
produkci	produkce	k1gFnSc6
Divadla	divadlo	k1gNnSc2
Mír	Míra	k1gFnPc2
byla	být	k5eAaImAgFnS
komedie	komedie	k1gFnSc1
Dva	dva	k4xCgMnPc1
úplně	úplně	k6eAd1
nazí	nahý	k2eAgMnPc1d1
muži	muž	k1gMnPc1
francouzského	francouzský	k2eAgMnSc2d1
herce	herec	k1gMnSc2
a	a	k8xC
dramatika	dramatik	k1gMnSc2
Sébastiena	Sébastien	k1gMnSc2
Thiéryho	Thiéry	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiéra	premiéra	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Uvedené	uvedený	k2eAgFnPc1d1
inscenace	inscenace	k1gFnPc1
</s>
<s>
Dva	dva	k4xCgMnPc1
úplně	úplně	k6eAd1
nazí	nahý	k2eAgMnPc1d1
muži	muž	k1gMnPc1
<g/>
,	,	kIx,
Sébastien	Sébastien	k1gInSc1
Thiéry	Thiéra	k1gFnSc2
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
25	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Václav	Václav	k1gMnSc1
Klemens	Klemens	k1gInSc1
</s>
<s>
Drahoušku	drahoušek	k1gMnSc5
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
chce	chtít	k5eAaImIp3nS
drink	drink	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
29	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Peter	Peter	k1gMnSc1
Gábor	Gábor	k1gMnSc1
</s>
<s>
Bůh	bůh	k1gMnSc1
masakru	masakr	k1gInSc2
<g/>
,	,	kIx,
Yasmina	Yasmin	k2eAgFnSc1d1
Reza	Reza	k1gFnSc1
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
18	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Dušan	Dušan	k1gMnSc1
Urban	Urban	k1gMnSc1
</s>
<s>
Mátový	mátový	k2eAgMnSc1d1
nebo	nebo	k8xC
citron	citron	k1gInSc1
<g/>
,	,	kIx,
Danielle	Danielle	k1gFnSc1
Navarro-Haudecœ	Navarro-Haudecœ	k1gMnSc1
a	a	k8xC
Patrick	Patrick	k1gMnSc1
Haudecœ	Haudecœ	k1gMnSc1
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Janusz	Janusz	k1gInSc1
Klimsza	Klimsza	k1gFnSc1
</s>
<s>
Jak	jak	k6eAd1
důležité	důležitý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
mít	mít	k5eAaImF
Filipa	Filip	k1gMnSc4
<g/>
,	,	kIx,
Oscar	Oscar	k1gMnSc1
Wilde	Wild	k1gInSc5
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
17	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Vít	Vít	k1gMnSc1
Vencl	Vencl	k1gMnSc1
</s>
<s>
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
pan	pan	k1gMnSc1
Schmitt	Schmitt	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Sébastien	Sébastien	k1gInSc1
Thiéry	Thiéra	k1gFnSc2
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
9	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Václav	Václav	k1gMnSc1
Klemens	Klemens	k1gInSc1
</s>
<s>
Ráno	ráno	k6eAd1
po	po	k7c6
tom	ten	k3xDgInSc6
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Quilter	Quilter	k1gMnSc1
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
6	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Grzegorz	Grzegorz	k1gMnSc1
Kempinsky	Kempinsky	k1gMnSc1
</s>
<s>
Bull	bulla	k1gFnPc2
<g/>
,	,	kIx,
Mike	Mike	k1gFnSc1
Bartlett	Bartlett	k1gInSc1
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Albert	Alberta	k1gFnPc2
Čuba	čuba	k1gFnSc1
</s>
<s>
Lakomec	lakomec	k1gMnSc1
<g/>
,	,	kIx,
Moliè	Moliè	k1gMnSc5
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Pavel	Pavel	k1gMnSc1
Šimák	Šimák	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Pplk.	pplk.	kA
MUDr.	MUDr.	kA
Ludvík	Ludvík	k1gMnSc1
Klega	Klega	k1gFnSc1
<g/>
,	,	kIx,
lékař	lékař	k1gMnSc1
<g/>
,	,	kIx,
legionář	legionář	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statutární	statutární	k2eAgInSc4d1
město	město	k1gNnSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
městský	městský	k2eAgInSc1d1
obvod	obvod	k1gInSc1
Vítkovice	Vítkovice	k1gInPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HAVLÍČKOVÁ	Havlíčková	k1gFnSc1
<g/>
,	,	kIx,
Iva	Iva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalé	bývalý	k2eAgNnSc1d1
kino	kino	k1gNnSc4
Mír	Míra	k1gFnPc2
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
na	na	k7c4
vietnamský	vietnamský	k2eAgInSc4d1
krámek	krámek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
UHLÁŘ	uhlář	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nová	k1gFnSc6
Divadlo	divadlo	k1gNnSc1
Mír	Míra	k1gFnPc2
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
otevřou	otevřít	k5eAaPmIp3nP
Dva	dva	k4xCgMnPc1
úplně	úplně	k6eAd1
nazí	nahý	k2eAgMnPc1d1
muži	muž	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ostrava	Ostrava	k1gFnSc1
|	|	kIx~
Kultura	kultura	k1gFnSc1
</s>
