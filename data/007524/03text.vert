<s>
Kašna	kašna	k1gFnSc1	kašna
Parnas	Parnas	k1gInSc1	Parnas
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgFnSc1d1	barokní
kašna	kašna	k1gFnSc1	kašna
uprostřed	uprostřed	k7c2	uprostřed
Zelného	zelný	k2eAgInSc2d1	zelný
trhu	trh	k1gInSc2	trh
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Johanna	Johann	k1gMnSc2	Johann
Bernharda	Bernhard	k1gMnSc2	Bernhard
Fischera	Fischer	k1gMnSc2	Fischer
z	z	k7c2	z
Erlachu	Erlach	k1gInSc2	Erlach
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1690	[number]	k4	1690
<g/>
–	–	k?	–
<g/>
1696	[number]	k4	1696
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněna	chráněn	k2eAgFnSc1d1	chráněna
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
byly	být	k5eAaImAgFnP	být
kašny	kašna	k1gFnPc1	kašna
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Doložena	doložen	k2eAgFnSc1d1	doložena
je	být	k5eAaImIp3nS	být
renesanční	renesanční	k2eAgFnSc1d1	renesanční
kašna	kašna	k1gFnSc1	kašna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1597	[number]	k4	1597
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
orla	orel	k1gMnSc2	orel
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatky	pozůstatek	k1gInPc1	pozůstatek
starší	starý	k2eAgFnSc2d2	starší
kruhové	kruhový	k2eAgFnSc2d1	kruhová
kašny	kašna	k1gFnSc2	kašna
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
magistrát	magistrát	k1gInSc1	magistrát
se	se	k3xPyFc4	se
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
nahradit	nahradit	k5eAaPmF	nahradit
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
nevyhovující	vyhovující	k2eNgFnSc4d1	nevyhovující
kašnu	kašna	k1gFnSc4	kašna
na	na	k7c6	na
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Horním	horní	k2eAgNnSc6d1	horní
náměstí	náměstí	k1gNnSc6	náměstí
za	za	k7c4	za
kašnu	kašna	k1gFnSc4	kašna
novou	nový	k2eAgFnSc4d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
architekt	architekt	k1gMnSc1	architekt
Johann	Johann	k1gMnSc1	Johann
Bernhard	Bernhard	k1gMnSc1	Bernhard
Fischer	Fischer	k1gMnSc1	Fischer
z	z	k7c2	z
Erlachu	Erlach	k1gInSc2	Erlach
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Pavla	Pavel	k1gMnSc2	Pavel
Suchánka	Suchánek	k1gMnSc2	Suchánek
z	z	k7c2	z
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgInP	dochovat
dva	dva	k4xCgInPc1	dva
jeho	jeho	k3xOp3gInPc4	jeho
původní	původní	k2eAgInSc4d1	původní
návrhy	návrh	k1gInPc4	návrh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
ve	v	k7c6	v
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
galerii	galerie	k1gFnSc6	galerie
Albertina	Albertin	k2eAgFnSc1d1	Albertina
a	a	k8xC	a
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
<g/>
.	.	kIx.	.
</s>
<s>
Vzorem	vzor	k1gInSc7	vzor
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
římská	římský	k2eAgFnSc1d1	římská
fontána	fontána	k1gFnSc1	fontána
čtyř	čtyři	k4xCgFnPc2	čtyři
řek	řeka	k1gFnPc2	řeka
jeho	on	k3xPp3gMnSc2	on
učitele	učitel	k1gMnSc2	učitel
Berniniho	Bernini	k1gMnSc2	Bernini
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
návrhů	návrh	k1gInPc2	návrh
se	se	k3xPyFc4	se
výsledný	výsledný	k2eAgInSc1d1	výsledný
projekt	projekt	k1gInSc1	projekt
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
zvýrazněním	zvýraznění	k1gNnSc7	zvýraznění
trojbokého	trojboký	k2eAgInSc2d1	trojboký
skalního	skalní	k2eAgInSc2d1	skalní
útesu	útes	k1gInSc2	útes
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
nižší	nízký	k2eAgFnSc1d2	nižší
a	a	k8xC	a
dominantní	dominantní	k2eAgFnSc1d1	dominantní
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
spíše	spíše	k9	spíše
sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
Brna	Brno	k1gNnSc2	Brno
si	se	k3xPyFc3	se
však	však	k9	však
tehdy	tehdy	k6eAd1	tehdy
prosadili	prosadit	k5eAaPmAgMnP	prosadit
nynější	nynější	k2eAgFnSc4d1	nynější
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
organizoval	organizovat	k5eAaBmAgMnS	organizovat
především	především	k9	především
brněnský	brněnský	k2eAgMnSc1d1	brněnský
radní	radní	k1gMnSc1	radní
Johann	Johann	k1gMnSc1	Johann
Ignaz	Ignaz	k1gInSc4	Ignaz
Dechau	Dechaus	k1gInSc2	Dechaus
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Fischer	Fischer	k1gMnSc1	Fischer
byl	být	k5eAaImAgMnS	být
zaneprázdněn	zaneprázdnit	k5eAaPmNgMnS	zaneprázdnit
i	i	k9	i
jinými	jiný	k2eAgFnPc7d1	jiná
zakázkami	zakázka	k1gFnPc7	zakázka
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1694	[number]	k4	1694
nakonec	nakonec	k6eAd1	nakonec
pro	pro	k7c4	pro
samotné	samotný	k2eAgNnSc4d1	samotné
vyhotovení	vyhotovení	k1gNnSc4	vyhotovení
kašny	kašna	k1gFnSc2	kašna
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vídeňského	vídeňský	k2eAgMnSc4d1	vídeňský
sochaře	sochař	k1gMnSc4	sochař
Tobiáše	Tobiáš	k1gMnSc4	Tobiáš
Krackera	Kracker	k1gMnSc4	Kracker
(	(	kIx(	(
<g/>
1658	[number]	k4	1658
<g/>
–	–	k?	–
<g/>
1736	[number]	k4	1736
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
sochařské	sochařský	k2eAgFnSc2d1	sochařská
výzdoby	výzdoba	k1gFnSc2	výzdoba
nejsou	být	k5eNaImIp3nP	být
přesně	přesně	k6eAd1	přesně
známi	znám	k2eAgMnPc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Dobové	dobový	k2eAgInPc1d1	dobový
záznamy	záznam	k1gInPc1	záznam
se	se	k3xPyFc4	se
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
o	o	k7c6	o
italském	italský	k2eAgMnSc6d1	italský
sochaři	sochař	k1gMnSc6	sochař
<g/>
.	.	kIx.	.
</s>
<s>
Počátků	počátek	k1gInPc2	počátek
stavby	stavba	k1gFnSc2	stavba
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
kameník	kameník	k1gMnSc1	kameník
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Steinböck	Steinböck	k1gMnSc1	Steinböck
z	z	k7c2	z
Eggenburgu	Eggenburg	k1gInSc2	Eggenburg
(	(	kIx(	(
<g/>
1650	[number]	k4	1650
<g/>
–	–	k?	–
<g/>
1708	[number]	k4	1708
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
kameníkem	kameník	k1gMnSc7	kameník
Bernardem	Bernard	k1gMnSc7	Bernard
Högerem	Höger	k1gMnSc7	Höger
a	a	k8xC	a
sochařem	sochař	k1gMnSc7	sochař
Antonínem	Antonín	k1gMnSc7	Antonín
Rigou	Riga	k1gFnSc7	Riga
na	na	k7c4	na
nespecifikované	specifikovaný	k2eNgFnPc4d1	nespecifikovaná
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
nádrž	nádrž	k1gFnSc1	nádrž
s	s	k7c7	s
půdorysem	půdorys	k1gInSc7	půdorys
šesticípé	šesticípý	k2eAgFnSc2d1	šesticípá
hvězdy	hvězda	k1gFnSc2	hvězda
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
kompletně	kompletně	k6eAd1	kompletně
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byli	být	k5eAaImAgMnP	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
i	i	k9	i
tři	tři	k4xCgInPc1	tři
kamenné	kamenný	k2eAgInPc1d1	kamenný
chrliče	chrlič	k1gInPc1	chrlič
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
delfínů	delfín	k1gMnPc2	delfín
za	za	k7c4	za
chrliče	chrlič	k1gInPc4	chrlič
litinové	litinový	k2eAgInPc4d1	litinový
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
umění	umění	k1gNnSc2	umění
Tomáše	Tomáš	k1gMnSc2	Tomáš
Jeřábka	Jeřábek	k1gMnSc2	Jeřábek
z	z	k7c2	z
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
a	a	k8xC	a
Pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
název	název	k1gInSc1	název
Parnas	Parnas	k1gInSc1	Parnas
omylem	omyl	k1gInSc7	omyl
<g/>
.	.	kIx.	.
</s>
<s>
Kašnu	kašna	k1gFnSc4	kašna
tak	tak	k6eAd1	tak
začali	začít	k5eAaPmAgMnP	začít
nazývat	nazývat	k5eAaImF	nazývat
studenti	student	k1gMnPc1	student
brněnské	brněnský	k2eAgFnSc2d1	brněnská
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
kašny	kašna	k1gFnSc2	kašna
považovali	považovat	k5eAaImAgMnP	považovat
ženské	ženský	k2eAgFnPc4d1	ženská
sochy	socha	k1gFnPc4	socha
za	za	k7c4	za
múzy	múza	k1gFnPc4	múza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sídlily	sídlit	k5eAaImAgFnP	sídlit
v	v	k7c6	v
řeckém	řecký	k2eAgNnSc6d1	řecké
pohoří	pohoří	k1gNnSc6	pohoří
Parnas	Parnas	k1gInSc1	Parnas
<g/>
.	.	kIx.	.
</s>
<s>
Kašna	kašna	k1gFnSc1	kašna
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
vandalů	vandal	k1gMnPc2	vandal
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemně	písemně	k6eAd1	písemně
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
krádež	krádež	k1gFnSc1	krádež
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
<g/>
.	.	kIx.	.
</s>
<s>
Žezla	žezlo	k1gNnPc1	žezlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postavy	postava	k1gFnPc4	postava
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
pozlacena	pozlacen	k2eAgFnSc1d1	pozlacena
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
vyměněna	vyměnit	k5eAaPmNgFnS	vyměnit
za	za	k7c4	za
měděná	měděný	k2eAgNnPc4d1	měděné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
imitace	imitace	k1gFnSc2	imitace
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
kašny	kašna	k1gFnSc2	kašna
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vápencová	vápencový	k2eAgFnSc1d1	vápencová
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
jeskyně	jeskyně	k1gFnSc1	jeskyně
otevřená	otevřený	k2eAgFnSc1d1	otevřená
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
Hérakla	Hérakles	k1gMnSc2	Hérakles
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
oděn	odět	k5eAaPmNgInS	odět
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
nemejského	mejský	k2eNgInSc2d1	mejský
lva	lev	k1gInSc2	lev
<g/>
,	,	kIx,	,
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
drží	držet	k5eAaImIp3nS	držet
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
kyj	kyj	k1gInSc1	kyj
a	a	k8xC	a
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
na	na	k7c6	na
řetězu	řetěz	k1gInSc6	řetěz
trojhlavého	trojhlavý	k2eAgMnSc2d1	trojhlavý
psa	pes	k1gMnSc2	pes
Kerbera	Kerber	k1gMnSc2	Kerber
<g/>
,	,	kIx,	,
strážce	strážce	k1gMnSc2	strážce
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
skály	skála	k1gFnSc2	skála
stojí	stát	k5eAaImIp3nS	stát
alegorie	alegorie	k1gFnSc1	alegorie
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
Svatou	svatý	k2eAgFnSc4d1	svatá
říši	říše	k1gFnSc4	říše
římskou	římský	k2eAgFnSc4d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
drží	držet	k5eAaImIp3nP	držet
žezlo	žezlo	k1gNnSc4	žezlo
a	a	k8xC	a
triumfálně	triumfálně	k6eAd1	triumfálně
stojí	stát	k5eAaImIp3nS	stát
nad	nad	k7c7	nad
přemoženým	přemožený	k2eAgInSc7d1	přemožený
drakem	drak	k1gInSc7	drak
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
kamenné	kamenný	k2eAgFnPc4d1	kamenná
desky	deska	k1gFnPc4	deska
s	s	k7c7	s
latinskými	latinský	k2eAgInPc7d1	latinský
nápisy	nápis	k1gInPc7	nápis
s	s	k7c7	s
chronogramy	chronogram	k1gInPc7	chronogram
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgInSc1d1	připomínající
rok	rok	k1gInSc1	rok
výstavby	výstavba	k1gFnSc2	výstavba
a	a	k8xC	a
renovace	renovace	k1gFnSc2	renovace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
skály	skála	k1gFnSc2	skála
jsou	být	k5eAaImIp3nP	být
alegorické	alegorický	k2eAgFnPc1d1	alegorická
sochy	socha	k1gFnPc1	socha
tří	tři	k4xCgFnPc2	tři
sedících	sedící	k2eAgFnPc2d1	sedící
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgFnPc1d1	symbolizující
tři	tři	k4xCgFnPc1	tři
starověké	starověký	k2eAgFnPc1d1	starověká
říše	říš	k1gFnPc1	říš
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
drží	držet	k5eAaImIp3nP	držet
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
žezlo	žezlo	k1gNnSc4	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
Řecka	Řecko	k1gNnSc2	Řecko
opírající	opírající	k2eAgFnSc1d1	opírající
se	se	k3xPyFc4	se
o	o	k7c4	o
toulec	toulec	k1gInSc4	toulec
se	s	k7c7	s
šípy	šíp	k1gInPc7	šíp
a	a	k8xC	a
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
u	u	k7c2	u
levé	levý	k2eAgFnSc2d1	levá
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
níž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
okřídlený	okřídlený	k2eAgInSc1d1	okřídlený
drak	drak	k1gInSc1	drak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
Babylonie	Babylonie	k1gFnSc1	Babylonie
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
u	u	k7c2	u
pravé	pravý	k2eAgFnSc2d1	pravá
nohy	noha	k1gFnSc2	noha
a	a	k8xC	a
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
je	být	k5eAaImIp3nS	být
okřídlený	okřídlený	k2eAgInSc1d1	okřídlený
lev	lev	k1gInSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
sedí	sedit	k5eAaImIp3nS	sedit
postava	postava	k1gFnSc1	postava
Persie	Persie	k1gFnSc1	Persie
s	s	k7c7	s
rohem	roh	k1gInSc7	roh
hojnosti	hojnost	k1gFnSc2	hojnost
<g/>
,	,	kIx,	,
korunou	koruna	k1gFnSc7	koruna
u	u	k7c2	u
levé	levý	k2eAgFnSc2d1	levá
nohy	noha	k1gFnSc2	noha
a	a	k8xC	a
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
ze	z	k7c2	z
skály	skála	k1gFnSc2	skála
vylézá	vylézat	k5eAaImIp3nS	vylézat
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
jsou	být	k5eAaImIp3nP	být
obráceny	obrátit	k5eAaPmNgFnP	obrátit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
městské	městský	k2eAgFnSc3d1	městská
radnici	radnice	k1gFnSc3	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
skále	skála	k1gFnSc6	skála
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
drobné	drobná	k1gFnSc2	drobná
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
dráčci	dráček	k1gMnPc1	dráček
a	a	k8xC	a
drobní	drobný	k2eAgMnPc1d1	drobný
živočichové	živočich	k1gMnPc1	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
kašny	kašna	k1gFnSc2	kašna
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
kamenných	kamenný	k2eAgInPc2d1	kamenný
sloupků	sloupek	k1gInPc2	sloupek
propojených	propojený	k2eAgInPc2d1	propojený
řetězy	řetěz	k1gInPc7	řetěz
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
kašně	kašna	k1gFnSc3	kašna
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kašnou	kašna	k1gFnSc7	kašna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
štola	štola	k1gFnSc1	štola
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
vystavěné	vystavěný	k2eAgFnPc1d1	vystavěná
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc7d1	západní
štolou	štola	k1gFnSc7	štola
byla	být	k5eAaImAgFnS	být
přiváděna	přiváděn	k2eAgFnSc1d1	přiváděna
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
vodojemu	vodojem	k1gInSc2	vodojem
na	na	k7c6	na
Petrově	Petrov	k1gInSc6	Petrov
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byla	být	k5eAaImAgFnS	být
čerpána	čerpán	k2eAgFnSc1d1	čerpána
voda	voda	k1gFnSc1	voda
ze	z	k7c2	z
Svratky	Svratka	k1gFnSc2	Svratka
přes	přes	k7c4	přes
dnešní	dnešní	k2eAgInPc4d1	dnešní
Denisovy	Denisův	k2eAgInPc4d1	Denisův
sady	sad	k1gInPc4	sad
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
štola	štola	k1gFnSc1	štola
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
ulicí	ulice	k1gFnSc7	ulice
Starobrněnskou	Starobrněnský	k2eAgFnSc7d1	Starobrněnská
a	a	k8xC	a
Peroutkovou	Peroutková	k1gFnSc4	Peroutková
kolem	kolem	k7c2	kolem
sloupu	sloup	k1gInSc2	sloup
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
pod	pod	k7c4	pod
dno	dno	k1gNnSc4	dno
kašny	kašna	k1gFnSc2	kašna
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
štola	štola	k1gFnSc1	štola
odváděla	odvádět	k5eAaImAgFnS	odvádět
odpadní	odpadní	k2eAgFnSc4d1	odpadní
vodu	voda	k1gFnSc4	voda
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Divadlu	divadlo	k1gNnSc3	divadlo
Reduta	reduta	k1gFnSc1	reduta
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
napojena	napojit	k5eAaPmNgFnS	napojit
na	na	k7c4	na
odpadní	odpadní	k2eAgFnSc4d1	odpadní
stoku	stoka	k1gFnSc4	stoka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
štoly	štola	k1gFnPc1	štola
vyčištěny	vyčištěn	k2eAgFnPc1d1	vyčištěna
<g/>
.	.	kIx.	.
</s>
