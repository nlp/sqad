<s>
Kašna	kašna	k1gFnSc1	kašna
Parnas	Parnas	k1gInSc1	Parnas
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgFnSc1d1	barokní
kašna	kašna	k1gFnSc1	kašna
uprostřed	uprostřed	k7c2	uprostřed
Zelného	zelný	k2eAgInSc2d1	zelný
trhu	trh	k1gInSc2	trh
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Johanna	Johann	k1gMnSc2	Johann
Bernharda	Bernhard	k1gMnSc2	Bernhard
Fischera	Fischer	k1gMnSc2	Fischer
z	z	k7c2	z
Erlachu	Erlach	k1gInSc2	Erlach
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1690	[number]	k4	1690
<g/>
–	–	k?	–
<g/>
1696	[number]	k4	1696
<g/>
.	.	kIx.	.
</s>
