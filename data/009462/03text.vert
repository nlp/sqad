<p>
<s>
Plavuník	Plavuník	k1gMnSc1	Plavuník
Isslerův	Isslerův	k2eAgMnSc1d1	Isslerův
(	(	kIx(	(
<g/>
Diphasiastrum	Diphasiastrum	k1gNnSc1	Diphasiastrum
issleri	issler	k1gFnSc2	issler
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
chráněné	chráněný	k2eAgInPc4d1	chráněný
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
víceletá	víceletý	k2eAgFnSc1d1	víceletá
plavuňovitá	plavuňovitý	k2eAgFnSc1d1	plavuňovitý
rostlina	rostlina	k1gFnSc1	rostlina
s	s	k7c7	s
plazivými	plazivý	k2eAgFnPc7d1	plazivá
lodyhami	lodyha	k1gFnPc7	lodyha
a	a	k8xC	a
nadzemními	nadzemní	k2eAgInPc7d1	nadzemní
oddenky	oddenek	k1gInPc7	oddenek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Plavuník	Plavuník	k1gInSc1	Plavuník
Isslerův	Isslerův	k2eAgInSc1d1	Isslerův
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
plavuňovité	plavuňovitý	k2eAgFnPc4d1	plavuňovitý
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Větve	větev	k1gFnPc1	větev
s	s	k7c7	s
listy	list	k1gInPc7	list
jsou	být	k5eAaImIp3nP	být
zřetelně	zřetelně	k6eAd1	zřetelně
zploštělé	zploštělý	k2eAgFnPc1d1	zploštělá
<g/>
,	,	kIx,	,
vzpřímené	vzpřímený	k2eAgFnPc1d1	vzpřímená
a	a	k8xC	a
značně	značně	k6eAd1	značně
větvené	větvený	k2eAgNnSc1d1	větvené
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
větviček	větvička	k1gFnPc2	větvička
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Břišní	břišní	k2eAgInPc4d1	břišní
sterilní	sterilní	k2eAgInPc4d1	sterilní
listy	list	k1gInPc4	list
nacházíme	nacházet	k5eAaImIp1nP	nacházet
přisedlé	přisedlý	k2eAgInPc4d1	přisedlý
a	a	k8xC	a
úzce	úzko	k6eAd1	úzko
čárkovité	čárkovitý	k2eAgFnPc4d1	čárkovitá
<g/>
.	.	kIx.	.
</s>
<s>
Srpkovitě	srpkovitě	k6eAd1	srpkovitě
zahnuté	zahnutý	k2eAgFnPc4d1	zahnutá
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
nesbíhavé	sbíhavý	k2eNgInPc4d1	nesbíhavý
postranní	postranní	k2eAgInPc4d1	postranní
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Hřbetní	hřbetní	k2eAgInPc1d1	hřbetní
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
úzce	úzko	k6eAd1	úzko
kopinaté	kopinatý	k2eAgFnPc1d1	kopinatá
a	a	k8xC	a
vypouklé	vypouklý	k2eAgFnPc1d1	vypouklá
(	(	kIx(	(
<g/>
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
trojúhelníkovitý	trojúhelníkovitý	k2eAgInSc4d1	trojúhelníkovitý
tvar	tvar	k1gInSc4	tvar
na	na	k7c6	na
průřezu	průřez	k1gInSc6	průřez
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
loňských	loňský	k2eAgFnPc2d1	loňská
větví	větev	k1gFnPc2	větev
s	s	k7c7	s
listy	list	k1gInPc7	list
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
plavuníku	plavuník	k1gInSc6	plavuník
jednotlivě	jednotlivě	k6eAd1	jednotlivě
(	(	kIx(	(
<g/>
vzácně	vzácně	k6eAd1	vzácně
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
<g/>
)	)	kIx)	)
výtrusnicové	výtrusnicový	k2eAgFnSc2d1	výtrusnicový
klasy	klasa	k1gFnSc2	klasa
<g/>
.	.	kIx.	.
</s>
<s>
Lodyžní	lodyžní	k2eAgInPc1d1	lodyžní
listy	list	k1gInPc1	list
mají	mít	k5eAaImIp3nP	mít
šedozelenou	šedozelený	k2eAgFnSc4d1	šedozelená
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
nalézáme	nalézat	k5eAaImIp1nP	nalézat
je	on	k3xPp3gFnPc4	on
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
podélných	podélný	k2eAgFnPc6d1	podélná
řadách	řada	k1gFnPc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Výtrusy	výtrus	k1gInPc1	výtrus
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Tomuto	tento	k3xDgInSc3	tento
druhu	druh	k1gInSc3	druh
prospívají	prospívat	k5eAaImIp3nP	prospívat
převážně	převážně	k6eAd1	převážně
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
rostou	růst	k5eAaImIp3nP	růst
na	na	k7c6	na
vřesovištích	vřesoviště	k1gNnPc6	vřesoviště
<g/>
,	,	kIx,	,
kyselých	kyselý	k2eAgFnPc6d1	kyselá
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
světlých	světlý	k2eAgInPc6d1	světlý
smrkových	smrkový	k2eAgInPc6d1	smrkový
a	a	k8xC	a
borových	borový	k2eAgInPc6d1	borový
lesích	les	k1gInPc6	les
a	a	k8xC	a
na	na	k7c6	na
půdách	půda	k1gFnPc6	půda
chudých	chudý	k2eAgFnPc6d1	chudá
na	na	k7c4	na
živiny	živina	k1gFnPc4	živina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
travnatých	travnatý	k2eAgInPc2d1	travnatý
a	a	k8xC	a
keříčkovitých	keříčkovitý	k2eAgNnPc6d1	keříčkovitý
společenstvech	společenstvo	k1gNnPc6	společenstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Plavuník	Plavuník	k1gMnSc1	Plavuník
Isslerův	Isslerův	k2eAgMnSc1d1	Isslerův
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
polohách	poloha	k1gFnPc6	poloha
hor	hora	k1gFnPc2	hora
(	(	kIx(	(
<g/>
Schwarzwald	Schwarzwald	k1gInSc1	Schwarzwald
<g/>
,	,	kIx,	,
Vogézy	Vogézy	k1gFnPc1	Vogézy
<g/>
,	,	kIx,	,
Bavorský	bavorský	k2eAgInSc1d1	bavorský
les	les	k1gInSc1	les
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
JOSEF	Josef	k1gMnSc1	Josef
DOSTÁL	Dostál	k1gMnSc1	Dostál
<g/>
.	.	kIx.	.
</s>
<s>
Klíč	klíč	k1gInSc1	klíč
k	k	k7c3	k
úplné	úplný	k2eAgFnSc3d1	úplná
květeně	květena	k1gFnSc3	květena
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
982	[number]	k4	982
s.	s.	k?	s.
</s>
</p>
<p>
<s>
BOHUMIL	Bohumil	k1gMnSc1	Bohumil
SLAVÍK	Slavík	k1gMnSc1	Slavík
<g/>
,	,	kIx,	,
SLAVOMIL	Slavomil	k1gMnSc1	Slavomil
HEJNÝ	HEJNÝ	kA	HEJNÝ
<g/>
.	.	kIx.	.
</s>
<s>
Květena	květena	k1gFnSc1	květena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Akademia	Akademia	k1gFnSc1	Akademia
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
ISBN	ISBN	kA	ISBN
80-200-0643-5	[number]	k4	80-200-0643-5
</s>
</p>
<p>
<s>
KREMER	KREMER	kA	KREMER
<g/>
,	,	kIx,	,
<g/>
MUHLE	MUHLE	kA	MUHLE
<g/>
.	.	kIx.	.
</s>
<s>
Lišejníky	lišejník	k1gInPc1	lišejník
<g/>
,	,	kIx,	,
mechorosty	mechorost	k1gInPc1	mechorost
<g/>
,	,	kIx,	,
kapraďorosty	kapraďorost	k1gInPc1	kapraďorost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Ikar	Ikara	k1gFnPc2	Ikara
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
ISBN	ISBN	kA	ISBN
80-7176-804-9	[number]	k4	80-7176-804-9
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
plavuník	plavuník	k1gMnSc1	plavuník
Isslerův	Isslerův	k2eAgMnSc1d1	Isslerův
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Diphasiastrum	Diphasiastrum	k1gNnSc1	Diphasiastrum
×	×	k?	×
issleri	issleri	k1gNnSc1	issleri
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Botany	Botana	k1gFnPc1	Botana
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Enviport	Enviport	k1gInSc1	Enviport
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
