<s>
Cena	cena	k1gFnSc1	cena
Akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Academy	Academ	k1gInPc4	Academ
Award	Awardo	k1gNnPc2	Awardo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
Oscar	Oscar	k1gInSc1	Oscar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
cena	cena	k1gFnSc1	cena
každoročně	každoročně	k6eAd1	každoročně
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
americkou	americký	k2eAgFnSc7d1	americká
Akademií	akademie	k1gFnSc7	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Oscar	Oscar	k1gInSc1	Oscar
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejprestižnější	prestižní	k2eAgFnSc4d3	nejprestižnější
filmovou	filmový	k2eAgFnSc4d1	filmová
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
především	především	k6eAd1	především
americké	americký	k2eAgFnSc2d1	americká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
kulturním	kulturní	k2eAgFnPc3d1	kulturní
událostem	událost	k1gFnPc3	událost
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Oscaři	Oscař	k1gMnPc1	Oscař
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
udělují	udělovat	k5eAaImIp3nP	udělovat
v	v	k7c6	v
Dolby	Dolba	k1gFnSc2	Dolba
Theatre	Theatr	k1gInSc5	Theatr
(	(	kIx(	(
<g/>
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
název	název	k1gInSc1	název
Kodak	Kodak	kA	Kodak
Theatre	Theatr	k1gInSc5	Theatr
<g/>
)	)	kIx)	)
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Akademii	akademie	k1gFnSc4	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
studia	studio	k1gNnSc2	studio
MGM	MGM	kA	MGM
Louis	Louis	k1gMnSc1	Louis
B.	B.	kA	B.
Mayer	Mayer	k1gMnSc1	Mayer
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
pětatřiceti	pětatřicet	k4xCc3	pětatřicet
osobnostmi	osobnost	k1gFnPc7	osobnost
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
činností	činnost	k1gFnPc2	činnost
Akademie	akademie	k1gFnSc2	akademie
je	být	k5eAaImIp3nS	být
udělování	udělování	k1gNnSc1	udělování
Cen	cena	k1gFnPc2	cena
Akademie	akademie	k1gFnSc2	akademie
nejlepším	dobrý	k2eAgInPc3d3	nejlepší
počinům	počin	k1gInPc3	počin
předešlého	předešlý	k2eAgInSc2d1	předešlý
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
cena	cena	k1gFnSc1	cena
udělena	udělen	k2eAgFnSc1d1	udělena
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1929	[number]	k4	1929
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
předávání	předávání	k1gNnSc1	předávání
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
v	v	k7c4	v
Dolby	Dolb	k1gInPc4	Dolb
Theatre	Theatr	k1gInSc5	Theatr
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
88	[number]	k4	88
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
předání	předání	k1gNnSc1	předání
ceny	cena	k1gFnSc2	cena
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1929	[number]	k4	1929
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
oceněny	oceněn	k2eAgFnPc1d1	oceněna
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
kvality	kvalita	k1gFnPc1	kvalita
filmů	film	k1gInPc2	film
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1927	[number]	k4	1927
a	a	k8xC	a
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Předávání	předávání	k1gNnSc1	předávání
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
jako	jako	k9	jako
banket	banket	k1gInSc4	banket
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
asi	asi	k9	asi
250	[number]	k4	250
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Akademie	akademie	k1gFnSc2	akademie
měli	mít	k5eAaImAgMnP	mít
vstup	vstup	k1gInSc4	vstup
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
si	se	k3xPyFc3	se
museli	muset	k5eAaImAgMnP	muset
zakoupit	zakoupit	k5eAaPmF	zakoupit
vstupenku	vstupenka	k1gFnSc4	vstupenka
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
5	[number]	k4	5
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Držitelé	držitel	k1gMnPc1	držitel
cen	cena	k1gFnPc2	cena
byli	být	k5eAaImAgMnP	být
ohlášeni	ohlášen	k2eAgMnPc1d1	ohlášen
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
předem	předem	k6eAd1	předem
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
médií	médium	k1gNnPc2	médium
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
minimální	minimální	k2eAgFnPc1d1	minimální
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
však	však	k9	však
vysílal	vysílat	k5eAaImAgInS	vysílat
přímý	přímý	k2eAgInSc1d1	přímý
rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
přenos	přenos	k1gInSc1	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
dostaly	dostat	k5eAaPmAgFnP	dostat
noviny	novina	k1gFnPc1	novina
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
vydání	vydání	k1gNnPc4	vydání
v	v	k7c6	v
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
však	však	k9	však
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
noviny	novina	k1gFnPc1	novina
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Times	Times	k1gMnSc1	Times
vydaly	vydat	k5eAaPmAgFnP	vydat
seznam	seznam	k1gInSc1	seznam
vítězů	vítěz	k1gMnPc2	vítěz
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příštím	příští	k2eAgInSc6d1	příští
roce	rok	k1gInSc6	rok
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
metoda	metoda	k1gFnSc1	metoda
zapečetěných	zapečetěný	k2eAgFnPc2d1	zapečetěná
obálek	obálka	k1gFnPc2	obálka
<g/>
,	,	kIx,	,
otevíraných	otevíraný	k2eAgFnPc2d1	otevíraná
až	až	k9	až
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
předávání	předávání	k1gNnSc2	předávání
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
laureátů	laureát	k1gMnPc2	laureát
nejsou	být	k5eNaImIp3nP	být
známá	známý	k2eAgFnSc1d1	známá
až	až	k6eAd1	až
do	do	k7c2	do
okamžiku	okamžik	k1gInSc2	okamžik
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
na	na	k7c6	na
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
předáváním	předávání	k1gNnSc7	předávání
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
schované	schovaná	k1gFnPc1	schovaná
v	v	k7c6	v
obálkách	obálka	k1gFnPc6	obálka
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
PricewaterhouseCoopers	PricewaterhouseCoopersa	k1gFnPc2	PricewaterhouseCoopersa
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
probíhalo	probíhat	k5eAaImAgNnS	probíhat
předávání	předávání	k1gNnSc1	předávání
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
banketu	banket	k1gInSc2	banket
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
ocenění	oceněný	k2eAgMnPc1d1	oceněný
udělují	udělovat	k5eAaImIp3nP	udělovat
na	na	k7c6	na
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
večeru	večer	k1gInSc6	večer
pořádaném	pořádaný	k2eAgInSc6d1	pořádaný
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
večer	večer	k1gInSc1	večer
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
zvané	zvaný	k2eAgMnPc4d1	zvaný
hosty	host	k1gMnPc4	host
<g/>
,	,	kIx,	,
vstupenky	vstupenka	k1gFnPc4	vstupenka
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
prodeji	prodej	k1gInSc6	prodej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
prvního	první	k4xOgInSc2	první
večera	večer	k1gInSc2	večer
bylo	být	k5eAaImAgNnS	být
rozdaných	rozdaný	k2eAgMnPc2d1	rozdaný
celkem	celkem	k6eAd1	celkem
dvanáct	dvanáct	k4xCc4	dvanáct
sošek	soška	k1gFnPc2	soška
a	a	k8xC	a
dvacet	dvacet	k4xCc4	dvacet
certifikátů	certifikát	k1gInPc2	certifikát
ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
základních	základní	k2eAgFnPc6d1	základní
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
speciálních	speciální	k2eAgFnPc2d1	speciální
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Herec	herec	k1gMnSc1	herec
Emil	Emil	k1gMnSc1	Emil
Jannings	Janningsa	k1gFnPc2	Janningsa
dokonce	dokonce	k9	dokonce
získal	získat	k5eAaPmAgInS	získat
úplně	úplně	k6eAd1	úplně
prvního	první	k4xOgMnSc4	první
Oscara	Oscar	k1gMnSc4	Oscar
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
ceremoniálem	ceremoniál	k1gInSc7	ceremoniál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
průběhu	průběh	k1gInSc6	průběh
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
na	na	k7c6	na
natáčení	natáčení	k1gNnSc6	natáčení
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc2	akademie
tomuto	tento	k3xDgInSc3	tento
požadavku	požadavek	k1gInSc3	požadavek
vyhověla	vyhovět	k5eAaPmAgFnS	vyhovět
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
bankety	banket	k1gInPc7	banket
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
hotelech	hotel	k1gInPc6	hotel
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
Ambassador	Ambassador	k1gMnSc1	Ambassador
a	a	k8xC	a
Biltmore	Biltmor	k1gMnSc5	Biltmor
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
zájem	zájem	k1gInSc1	zájem
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
novinářů	novinář	k1gMnPc2	novinář
i	i	k8xC	i
fanoušků	fanoušek	k1gMnPc2	fanoušek
rostl	růst	k5eAaImAgInS	růst
<g/>
,	,	kIx,	,
Akademie	akademie	k1gFnSc1	akademie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
při	při	k7c6	při
svém	své	k1gNnSc6	své
16	[number]	k4	16
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
přesunula	přesunout	k5eAaPmAgFnS	přesunout
udílení	udílení	k1gNnSc4	udílení
cen	cena	k1gFnPc2	cena
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
Graumanova	Graumanův	k2eAgNnSc2d1	Graumanův
čínského	čínský	k2eAgNnSc2d1	čínské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
1953	[number]	k4	1953
byl	být	k5eAaImAgMnS	být
rokem	rok	k1gInSc7	rok
průkopnickým	průkopnický	k2eAgInSc7d1	průkopnický
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
poprvé	poprvé	k6eAd1	poprvé
Oscaři	Oscař	k1gMnPc1	Oscař
pro	pro	k7c4	pro
diváky	divák	k1gMnPc4	divák
Spojených	spojený	k2eAgInPc2d1	spojený
Států	stát	k1gInPc2	stát
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
vysílali	vysílat	k5eAaImAgMnP	vysílat
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
se	se	k3xPyFc4	se
Oscar	Oscar	k1gInSc1	Oscar
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
měl	mít	k5eAaImAgInS	mít
televizní	televizní	k2eAgInSc1d1	televizní
přenos	přenos	k1gInSc1	přenos
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
rozměry	rozměra	k1gFnSc2	rozměra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
udílení	udílení	k1gNnSc2	udílení
cen	cena	k1gFnPc2	cena
možný	možný	k2eAgMnSc1d1	možný
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvou	dva	k4xCgNnPc2	dva
stech	sto	k4xCgNnPc6	sto
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
cen	cena	k1gFnPc2	cena
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
kategorií	kategorie	k1gFnPc2	kategorie
i	i	k8xC	i
pravidla	pravidlo	k1gNnSc2	pravidlo
značně	značně	k6eAd1	značně
měnily	měnit	k5eAaImAgFnP	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
mezníky	mezník	k1gInPc4	mezník
považuje	považovat	k5eAaImIp3nS	považovat
Akademie	akademie	k1gFnSc1	akademie
tyto	tento	k3xDgInPc4	tento
<g/>
:	:	kIx,	:
První	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
-	-	kIx~	-
První	první	k4xOgNnPc4	první
udílení	udílení	k1gNnPc4	udílení
Oscarů	Oscar	k1gMnPc2	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
udělit	udělit	k5eAaPmF	udělit
speciální	speciální	k2eAgNnSc1d1	speciální
ocenění	ocenění	k1gNnSc1	ocenění
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nevešli	vejít	k5eNaPmAgMnP	vejít
do	do	k7c2	do
dvanácti	dvanáct	k4xCc2	dvanáct
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
získalo	získat	k5eAaPmAgNnS	získat
studio	studio	k1gNnSc1	studio
Warner	Warnra	k1gFnPc2	Warnra
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
za	za	k7c4	za
průkopnický	průkopnický	k2eAgInSc4d1	průkopnický
snímek	snímek	k1gInSc4	snímek
Jazzový	jazzový	k2eAgMnSc1d1	jazzový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
Oscara	Oscar	k1gMnSc4	Oscar
si	se	k3xPyFc3	se
odnesl	odnést	k5eAaPmAgMnS	odnést
Charles	Charles	k1gMnSc1	Charles
Chaplin	Chaplina	k1gFnPc2	Chaplina
za	za	k7c4	za
produkci	produkce	k1gFnSc4	produkce
<g/>
,	,	kIx,	,
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc4	scénář
a	a	k8xC	a
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
filmu	film	k1gInSc6	film
Cirkus	cirkus	k1gInSc1	cirkus
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
-	-	kIx~	-
Počet	počet	k1gInSc1	počet
kategorií	kategorie	k1gFnPc2	kategorie
klesl	klesnout	k5eAaPmAgInS	klesnout
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
(	(	kIx(	(
<g/>
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
kamera	kamera	k1gFnSc1	kamera
a	a	k8xC	a
výprava	výprava	k1gFnSc1	výprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sedmý	sedmý	k4xOgInSc1	sedmý
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
-	-	kIx~	-
Byly	být	k5eAaImAgFnP	být
přidané	přidaný	k2eAgFnPc1d1	přidaná
kategorie	kategorie	k1gFnPc1	kategorie
jako	jako	k8xS	jako
střih	střih	k1gInSc1	střih
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
filmová	filmový	k2eAgFnSc1d1	filmová
píseň	píseň	k1gFnSc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
roce	rok	k1gInSc6	rok
taky	taky	k6eAd1	taky
začala	začít	k5eAaPmAgFnS	začít
firma	firma	k1gFnSc1	firma
Price	Price	k1gFnSc1	Price
Waterhouse	Waterhouse	k1gFnSc1	Waterhouse
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
PricewaterhouseCoopers	PricewaterhouseCoopers	k1gInSc1	PricewaterhouseCoopers
<g/>
)	)	kIx)	)
počítat	počítat	k5eAaImF	počítat
hlasovací	hlasovací	k2eAgInPc4d1	hlasovací
lístky	lístek	k1gInPc4	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
role	role	k1gFnSc1	role
ji	on	k3xPp3gFnSc4	on
zůstala	zůstat	k5eAaPmAgFnS	zůstat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Devátý	devátý	k4xOgInSc1	devátý
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
Poprvé	poprvé	k6eAd1	poprvé
byly	být	k5eAaImAgFnP	být
uděleny	udělen	k2eAgFnPc1d1	udělena
ceny	cena	k1gFnPc1	cena
ve	v	k7c6	v
vedlejších	vedlejší	k2eAgFnPc6d1	vedlejší
hereckých	herecký	k2eAgFnPc6d1	herecká
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Desátý	desátý	k4xOgInSc4	desátý
ročník	ročník	k1gInSc4	ročník
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
cena	cena	k1gFnSc1	cena
Irvinga	Irvinga	k1gFnSc1	Irvinga
G.	G.	kA	G.
Thalberga	Thalberga	k1gFnSc1	Thalberga
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Irving	Irving	k1gInSc1	Irving
G.	G.	kA	G.
Thalberg	Thalberg	k1gInSc1	Thalberg
Memorial	Memorial	k1gInSc1	Memorial
Award	Award	k1gInSc1	Award
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáctý	dvanáctý	k4xOgInSc1	dvanáctý
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
Udělen	udělen	k2eAgInSc1d1	udělen
Oscar	Oscar	k1gInSc1	Oscar
za	za	k7c4	za
speciální	speciální	k2eAgInPc4d1	speciální
efekty	efekt	k1gInPc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnáctý	čtrnáctý	k4xOgInSc1	čtrnáctý
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
Udělen	udělen	k2eAgInSc1d1	udělen
Oscar	Oscar	k1gInSc1	Oscar
za	za	k7c4	za
dokument	dokument	k1gInSc4	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Dvacátý	dvacátý	k4xOgInSc1	dvacátý
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
-	-	kIx~	-
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
udělila	udělit	k5eAaPmAgFnS	udělit
cena	cena	k1gFnSc1	cena
pro	pro	k7c4	pro
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
získal	získat	k5eAaPmAgInS	získat
de	de	k?	de
Sicův	Sicův	k2eAgInSc4d1	Sicův
film	film	k1gInSc4	film
Děti	dítě	k1gFnPc1	dítě
ulice	ulice	k1gFnSc2	ulice
(	(	kIx(	(
<g/>
v	v	k7c6	v
originálu	originál	k1gInSc6	originál
Sciuscià	Sciuscià	k1gFnSc2	Sciuscià
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
samostatná	samostatný	k2eAgFnSc1d1	samostatná
kategorii	kategorie	k1gFnSc6	kategorie
pro	pro	k7c4	pro
filmy	film	k1gInPc4	film
v	v	k7c6	v
jiném	jiný	k2eAgMnSc6d1	jiný
<g/>
,	,	kIx,	,
než	než	k8xS	než
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Akademie	akademie	k1gFnSc1	akademie
rozdala	rozdat	k5eAaPmAgFnS	rozdat
ještě	ještě	k9	ještě
sedm	sedm	k4xCc4	sedm
dalších	další	k2eAgFnPc2d1	další
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Dvacátý	dvacátý	k4xOgInSc4	dvacátý
první	první	k4xOgInSc4	první
ročník	ročník	k1gInSc4	ročník
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
Oscar	Oscar	k1gInSc1	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
kostýmy	kostým	k1gInPc4	kostým
<g/>
.	.	kIx.	.
</s>
<s>
Dvacátý	dvacátý	k4xOgInSc4	dvacátý
pátý	pátý	k4xOgInSc4	pátý
ročník	ročník	k1gInSc4	ročník
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
-	-	kIx~	-
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
večer	večer	k1gInSc4	večer
přenášen	přenášen	k2eAgInSc4d1	přenášen
televizí	televize	k1gFnSc7	televize
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
dva	dva	k4xCgInPc1	dva
ceremoniály	ceremoniál	k1gInPc1	ceremoniál
<g/>
,	,	kIx,	,
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
a	a	k8xC	a
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Hostitelem	hostitel	k1gMnSc7	hostitel
hollywoodského	hollywoodský	k2eAgInSc2d1	hollywoodský
byl	být	k5eAaImAgMnS	být
Bob	Bob	k1gMnSc1	Bob
Hope	Hop	k1gInSc2	Hop
<g/>
,	,	kIx,	,
newyorského	newyorský	k2eAgNnSc2d1	newyorské
Conrad	Conrada	k1gFnPc2	Conrada
Nagel	Naglo	k1gNnPc2	Naglo
<g/>
.	.	kIx.	.
</s>
<s>
Dvacátý	dvacátý	k4xOgInSc4	dvacátý
devátý	devátý	k4xOgInSc4	devátý
ročník	ročník	k1gInSc4	ročník
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
-	-	kIx~	-
Humanitární	humanitární	k2eAgFnSc1d1	humanitární
cena	cena	k1gFnSc1	cena
Jeana	Jean	k1gMnSc2	Jean
Hersholta	Hersholt	k1gMnSc2	Hersholt
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Jean	Jean	k1gMnSc1	Jean
Hersholt	Hersholt	k1gMnSc1	Hersholt
Humanitarian	Humanitarian	k1gMnSc1	Humanitarian
Award	Award	k1gMnSc1	Award
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
udělena	udělit	k5eAaPmNgFnS	udělit
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
prvním	první	k4xOgMnSc7	první
nositelem	nositel	k1gMnSc7	nositel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Y.	Y.	kA	Y.
Frank	Frank	k1gMnSc1	Frank
Freeman	Freeman	k1gMnSc1	Freeman
<g/>
.	.	kIx.	.
</s>
<s>
Třicátý	třicátý	k4xOgInSc4	třicátý
šestý	šestý	k4xOgInSc4	šestý
ročník	ročník	k1gInSc4	ročník
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
-	-	kIx~	-
Speciální	speciální	k2eAgInPc1d1	speciální
efekty	efekt	k1gInPc1	efekt
se	se	k3xPyFc4	se
rozdělily	rozdělit	k5eAaPmAgInP	rozdělit
na	na	k7c6	na
zvukové	zvukový	k2eAgFnSc6d1	zvuková
a	a	k8xC	a
vizuální	vizuální	k2eAgFnSc6d1	vizuální
<g/>
.	.	kIx.	.
</s>
<s>
Třicátý	třicátý	k4xOgInSc4	třicátý
osmý	osmý	k4xOgInSc4	osmý
ročník	ročník	k1gInSc4	ročník
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
-	-	kIx~	-
Konal	konat	k5eAaImAgInS	konat
se	se	k3xPyFc4	se
první	první	k4xOgInSc1	první
barevný	barevný	k2eAgInSc1d1	barevný
přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřicátý	čtyřicátý	k4xOgInSc4	čtyřicátý
první	první	k4xOgInSc4	první
ročník	ročník	k1gInSc4	ročník
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
-	-	kIx~	-
Slavností	slavnost	k1gFnSc7	slavnost
večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
právě	právě	k6eAd1	právě
dokončeném	dokončený	k2eAgInSc6d1	dokončený
pavilónu	pavilón	k1gInSc6	pavilón
Dorothy	Dorotha	k1gMnSc2	Dorotha
Chandler	Chandler	k1gMnSc1	Chandler
v	v	k7c6	v
hudebním	hudební	k2eAgNnSc6d1	hudební
centru	centrum	k1gNnSc6	centrum
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Padesátý	padesátý	k4xOgInSc4	padesátý
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
ročník	ročník	k1gInSc4	ročník
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
-	-	kIx~	-
Byla	být	k5eAaImAgFnS	být
přidána	přidat	k5eAaPmNgFnS	přidat
kategorie	kategorie	k1gFnSc1	kategorie
masky	maska	k1gFnSc2	maska
<g/>
.	.	kIx.	.
</s>
<s>
Taky	taky	k6eAd1	taky
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vyhlášen	vyhlášen	k2eAgMnSc1d1	vyhlášen
laureát	laureát	k1gMnSc1	laureát
technické	technický	k2eAgFnSc2d1	technická
Ceny	cena	k1gFnSc2	cena
Gordona	Gordon	k1gMnSc2	Gordon
E.	E.	kA	E.
Sawyera	Sawyer	k1gMnSc2	Sawyer
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Gordon	Gordon	k1gMnSc1	Gordon
E.	E.	kA	E.
Sawyer	Sawyer	k1gMnSc1	Sawyer
Award	Award	k1gMnSc1	Award
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sedmdesátý	sedmdesátý	k4xOgInSc4	sedmdesátý
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
ročník	ročník	k1gInSc4	ročník
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
-	-	kIx~	-
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
Shrek	Shrek	k6eAd1	Shrek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
cen	cena	k1gFnPc2	cena
se	se	k3xPyFc4	se
třikrát	třikrát	k6eAd1	třikrát
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
termín	termín	k1gInSc1	termín
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
předání	předání	k1gNnSc2	předání
této	tento	k3xDgFnSc2	tento
ceny	cena	k1gFnSc2	cena
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
přesunut	přesunut	k2eAgMnSc1d1	přesunut
<g/>
:	:	kIx,	:
Desátý	desátý	k4xOgInSc1	desátý
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Velké	velký	k2eAgFnPc1d1	velká
povodně	povodeň	k1gFnPc1	povodeň
v	v	k7c6	v
roku	rok	k1gInSc6	rok
1938	[number]	k4	1938
posunuly	posunout	k5eAaPmAgFnP	posunout
udílení	udílení	k1gNnSc1	udílení
Oscarů	Oscar	k1gInPc2	Oscar
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřicátý	čtyřicátý	k4xOgInSc1	čtyřicátý
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
úmrtí	úmrtí	k1gNnSc3	úmrtí
Martina	Martin	k1gMnSc2	Martin
Luthera	Luther	k1gMnSc2	Luther
Kinga	King	k1gMnSc2	King
<g/>
,	,	kIx,	,
Jr	Jr	k1gFnSc2	Jr
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
přesunut	přesunout	k5eAaPmNgInS	přesunout
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
King	King	k1gInSc1	King
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
jen	jen	k9	jen
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
před	před	k7c7	před
plánovaným	plánovaný	k2eAgNnSc7d1	plánované
odevzdáváním	odevzdávání	k1gNnSc7	odevzdávání
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
konat	konat	k5eAaImF	konat
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
organizátoři	organizátor	k1gMnPc1	organizátor
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
přesunutí	přesunutí	k1gNnSc4	přesunutí
<g/>
.	.	kIx.	.
</s>
<s>
Padesátý	padesátý	k4xOgInSc4	padesátý
třetí	třetí	k4xOgInSc4	třetí
ročník	ročník	k1gInSc4	ročník
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kvůli	kvůli	k7c3	kvůli
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
tehdejšího	tehdejší	k2eAgMnSc4d1	tehdejší
amerického	americký	k2eAgMnSc4d1	americký
prezidenta	prezident	k1gMnSc4	prezident
Ronalda	Ronald	k1gMnSc4	Ronald
Reagana	Reagan	k1gMnSc4	Reagan
přesunula	přesunout	k5eAaPmAgFnS	přesunout
Akademie	akademie	k1gFnSc1	akademie
udílení	udílení	k1gNnSc2	udílení
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
ceny	cena	k1gFnSc2	cena
je	být	k5eAaImIp3nS	být
Cena	cena	k1gFnSc1	cena
Akademie	akademie	k1gFnSc2	akademie
za	za	k7c2	za
zásluhy	zásluha	k1gFnSc2	zásluha
(	(	kIx(	(
<g/>
Academy	Academa	k1gFnSc2	Academa
Award	Award	k1gMnSc1	Award
of	of	k?	of
Merit	meritum	k1gNnPc2	meritum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc1	označení
Oscar	Oscara	k1gFnPc2	Oscara
je	být	k5eAaImIp3nS	být
přezdívka	přezdívka	k1gFnSc1	přezdívka
sošky	soška	k1gFnSc2	soška
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
uchytil	uchytit	k5eAaPmAgInS	uchytit
podle	podle	k7c2	podle
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgFnPc4d1	jasná
verze	verze	k1gFnPc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
totiž	totiž	k9	totiž
tři	tři	k4xCgInPc4	tři
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
jména	jméno	k1gNnSc2	jméno
Oscar	Oscara	k1gFnPc2	Oscara
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
verze	verze	k1gFnSc2	verze
knihovnice	knihovnice	k1gFnSc2	knihovnice
Akademie	akademie	k1gFnSc2	akademie
Margaret	Margareta	k1gFnPc2	Margareta
Herricková	Herrickový	k2eAgFnSc1d1	Herrickový
při	při	k7c6	při
spatření	spatření	k1gNnSc6	spatření
sošky	soška	k1gFnSc2	soška
zvolala	zvolat	k5eAaPmAgFnS	zvolat
"	"	kIx"	"
<g/>
vypadá	vypadat	k5eAaPmIp3nS	vypadat
úplně	úplně	k6eAd1	úplně
jako	jako	k9	jako
strýček	strýček	k1gMnSc1	strýček
Oscar	Oscar	k1gMnSc1	Oscar
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přezdívku	přezdívka	k1gFnSc4	přezdívka
vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
někdejší	někdejší	k2eAgFnSc1d1	někdejší
ředitelka	ředitelka	k1gFnSc1	ředitelka
Akademie	akademie	k1gFnSc2	akademie
Bette	Bett	k1gInSc5	Bett
Davis	Davis	k1gFnSc4	Davis
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
soška	soška	k1gFnSc1	soška
ji	on	k3xPp3gFnSc4	on
připomínala	připomínat	k5eAaImAgFnS	připomínat
manžela	manžel	k1gMnSc4	manžel
Harmona	Harmon	k1gMnSc4	Harmon
Oscara	Oscar	k1gMnSc4	Oscar
Nelsona	Nelson	k1gMnSc4	Nelson
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
však	však	k9	však
název	název	k1gInSc1	název
Oscar	Oscar	k1gInSc1	Oscar
použil	použít	k5eAaPmAgMnS	použít
losangeleský	losangeleský	k2eAgMnSc1d1	losangeleský
novinář	novinář	k1gMnSc1	novinář
Sidney	Sidnea	k1gFnSc2	Sidnea
Skolsky	Skolsko	k1gNnPc7	Skolsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
když	když	k8xS	když
glosoval	glosovat	k5eAaBmAgMnS	glosovat
úspěch	úspěch	k1gInSc4	úspěch
Katharine	Katharin	k1gInSc5	Katharin
Hepburn	Hepburn	k1gInSc4	Hepburn
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
totiž	totiž	k9	totiž
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
prvního	první	k4xOgMnSc2	první
Oscara	Oscar	k1gMnSc2	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
Akademie	akademie	k1gFnSc2	akademie
tuto	tento	k3xDgFnSc4	tento
přezdívku	přezdívka	k1gFnSc4	přezdívka
oficiálně	oficiálně	k6eAd1	oficiálně
uznala	uznat	k5eAaPmAgFnS	uznat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
ceny	cena	k1gFnSc2	cena
je	být	k5eAaImIp3nS	být
zlatá	zlatý	k2eAgFnSc1d1	zlatá
soška	soška	k1gFnSc1	soška
rytíře	rytíř	k1gMnSc2	rytíř
držícího	držící	k2eAgInSc2d1	držící
meč	meč	k1gInSc1	meč
a	a	k8xC	a
stojícího	stojící	k2eAgMnSc4d1	stojící
na	na	k7c6	na
cívce	cívka	k1gFnSc6	cívka
filmového	filmový	k2eAgInSc2d1	filmový
pásu	pás	k1gInSc2	pás
s	s	k7c7	s
pěti	pět	k4xCc7	pět
kruhy	kruh	k1gInPc7	kruh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
představují	představovat	k5eAaImIp3nP	představovat
pět	pět	k4xCc4	pět
původních	původní	k2eAgFnPc2d1	původní
profesních	profesní	k2eAgFnPc2d1	profesní
sekcí	sekce	k1gFnPc2	sekce
Akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
herci	herec	k1gMnPc1	herec
<g/>
,	,	kIx,	,
režiséři	režisér	k1gMnPc1	režisér
<g/>
,	,	kIx,	,
producenti	producent	k1gMnPc1	producent
<g/>
,	,	kIx,	,
technici	technik	k1gMnPc1	technik
a	a	k8xC	a
scenáristé	scenárista	k1gMnPc1	scenárista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sošku	soška	k1gFnSc4	soška
stvořili	stvořit	k5eAaPmAgMnP	stvořit
výtvarník	výtvarník	k1gMnSc1	výtvarník
Cedric	Cedric	k1gMnSc1	Cedric
Gibbons	Gibbons	k1gInSc1	Gibbons
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
George	Georg	k1gMnSc2	Georg
Stanley	Stanlea	k1gFnSc2	Stanlea
<g/>
.	.	kIx.	.
</s>
<s>
Podstavec	podstavec	k1gInSc1	podstavec
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
soška	soška	k1gFnSc1	soška
rytíře	rytíř	k1gMnPc4	rytíř
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
měnil	měnit	k5eAaImAgMnS	měnit
svou	svůj	k3xOyFgFnSc4	svůj
podobu	podoba	k1gFnSc4	podoba
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
změnami	změna	k1gFnPc7	změna
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
sošky	soška	k1gFnSc2	soška
je	být	k5eAaImIp3nS	být
34,5	[number]	k4	34,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
podstavce	podstavec	k1gInSc2	podstavec
7,5	[number]	k4	7,5
cm	cm	kA	cm
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
průměr	průměr	k1gInSc1	průměr
13,5	[number]	k4	13,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
Oscar	Oscar	k1gInSc1	Oscar
z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
a	a	k8xC	a
pozlacený	pozlacený	k2eAgInSc1d1	pozlacený
24	[number]	k4	24
<g/>
-karátovým	arátův	k2eAgNnSc7d1	-karátův
zlatem	zlato	k1gNnSc7	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
antimonu	antimon	k1gInSc2	antimon
a	a	k8xC	a
pokrytý	pokrytý	k2eAgInSc4d1	pokrytý
leštěnými	leštěný	k2eAgFnPc7d1	leštěná
vrstvami	vrstva	k1gFnPc7	vrstva
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
sošky	soška	k1gFnPc1	soška
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
kovových	kovový	k2eAgInPc2d1	kovový
materiálů	materiál	k1gInPc2	materiál
vyrobeny	vyrobit	k5eAaPmNgFnP	vyrobit
ze	z	k7c2	z
sádry	sádra	k1gFnSc2	sádra
<g/>
,	,	kIx,	,
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
majitelům	majitel	k1gMnPc3	majitel
vyměněny	vyměnit	k5eAaPmNgInP	vyměnit
za	za	k7c4	za
kovové	kovový	k2eAgNnSc4d1	kovové
<g/>
.	.	kIx.	.
</s>
<s>
Podstavec	podstavec	k1gInSc1	podstavec
býval	bývat	k5eAaImAgInS	bývat
z	z	k7c2	z
černého	černý	k2eAgInSc2d1	černý
mramoru	mramor	k1gInSc2	mramor
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
je	být	k5eAaImIp3nS	být
kovový	kovový	k2eAgInSc1d1	kovový
<g/>
.	.	kIx.	.
</s>
<s>
Oscar	Oscar	k1gInSc1	Oscar
váží	vážit	k5eAaImIp3nS	vážit
3,85	[number]	k4	3,85
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
sobě	se	k3xPyFc3	se
každá	každý	k3xTgFnSc1	každý
soška	soška	k1gFnSc1	soška
jedinečné	jedinečný	k2eAgNnSc4d1	jedinečné
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
číslování	číslování	k1gNnSc1	číslování
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
501	[number]	k4	501
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
sošky	soška	k1gFnSc2	soška
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
chicagská	chicagský	k2eAgFnSc1d1	Chicagská
společnost	společnost	k1gFnSc1	společnost
R.	R.	kA	R.
S.	S.	kA	S.
Owens	Owens	k1gInSc1	Owens
&	&	k?	&
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
padesát	padesát	k4xCc1	padesát
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
to	ten	k3xDgNnSc1	ten
tři	tři	k4xCgInPc4	tři
až	až	k9	až
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Odlití	odlití	k1gNnSc1	odlití
každého	každý	k3xTgInSc2	každý
kusu	kus	k1gInSc2	kus
zaměstná	zaměstnat	k5eAaPmIp3nS	zaměstnat
dvanáct	dvanáct	k4xCc1	dvanáct
pracovníků	pracovník	k1gMnPc2	pracovník
na	na	k7c4	na
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Sošky	soška	k1gFnPc1	soška
jsou	být	k5eAaImIp3nP	být
nejdřív	dříve	k6eAd3	dříve
odlité	odlitý	k2eAgInPc1d1	odlitý
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vytvarované	vytvarovaný	k2eAgInPc1d1	vytvarovaný
<g/>
,	,	kIx,	,
obroušené	obroušený	k2eAgInPc1d1	obroušený
a	a	k8xC	a
vyleštěné	vyleštěný	k2eAgInPc1d1	vyleštěný
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgFnSc1d1	výrobní
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
$	$	kIx~	$
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
symbolická	symbolický	k2eAgFnSc1d1	symbolická
je	být	k5eAaImIp3nS	být
nevyčíslitelná	vyčíslitelný	k2eNgFnSc1d1	nevyčíslitelná
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
začíná	začínat	k5eAaImIp3nS	začínat
volební	volební	k2eAgFnSc4d1	volební
kampaň	kampaň	k1gFnSc4	kampaň
Akademie	akademie	k1gFnSc2	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Udílení	udílení	k1gNnSc1	udílení
Oscarů	Oscar	k1gInPc2	Oscar
předchází	předcházet	k5eAaImIp3nS	předcházet
hlasovací	hlasovací	k2eAgInSc1d1	hlasovací
proces	proces	k1gInSc1	proces
s	s	k7c7	s
volbou	volba	k1gFnSc7	volba
nominaci	nominace	k1gFnSc4	nominace
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
aktivní	aktivní	k2eAgMnSc1d1	aktivní
člen	člen	k1gMnSc1	člen
s	s	k7c7	s
hlasovacím	hlasovací	k2eAgNnSc7d1	hlasovací
právem	právo	k1gNnSc7	právo
získá	získat	k5eAaPmIp3nS	získat
od	od	k7c2	od
Akademie	akademie	k1gFnSc2	akademie
hlasovací	hlasovací	k2eAgInSc4d1	hlasovací
lístek	lístek	k1gInSc4	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
členů	člen	k1gInPc2	člen
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
šest	šest	k4xCc4	šest
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
pobočce	pobočka	k1gFnSc6	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
v	v	k7c6	v
herecké	herecký	k2eAgFnSc6d1	herecká
<g/>
,	,	kIx,	,
režiséři	režisér	k1gMnPc1	režisér
v	v	k7c4	v
režisérské	režisérský	k2eAgNnSc4d1	režisérské
<g/>
,	,	kIx,	,
skladatelé	skladatel	k1gMnPc1	skladatel
v	v	k7c4	v
skladatelské	skladatelský	k2eAgNnSc4d1	skladatelské
<g/>
,	,	kIx,	,
střihači	střihač	k1gMnSc3	střihač
v	v	k7c4	v
střihačské	střihačský	k2eAgFnPc4d1	střihačská
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Poboček	pobočka	k1gFnPc2	pobočka
má	mít	k5eAaImIp3nS	mít
Akademie	akademie	k1gFnSc1	akademie
patnáct	patnáct	k4xCc4	patnáct
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
uplácení	uplácení	k1gNnSc1	uplácení
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnSc1	organizace
nezveřejňuje	zveřejňovat	k5eNaImIp3nS	zveřejňovat
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
jména	jméno	k1gNnPc4	jméno
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Hlasujícím	hlasující	k2eAgMnPc3d1	hlasující
členům	člen	k1gMnPc3	člen
jsou	být	k5eAaImIp3nP	být
poskytnuty	poskytnout	k5eAaPmNgInP	poskytnout
veřejná	veřejný	k2eAgNnPc1d1	veřejné
promítání	promítání	k1gNnSc4	promítání
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
vstupy	vstup	k1gInPc4	vstup
zdarma	zdarma	k6eAd1	zdarma
na	na	k7c4	na
komerční	komerční	k2eAgFnPc4d1	komerční
projekce	projekce	k1gFnPc4	projekce
a	a	k8xC	a
DVD	DVD	kA	DVD
nosiče	nosič	k1gInPc1	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jim	on	k3xPp3gMnPc3	on
zasílají	zasílat	k5eAaImIp3nP	zasílat
různá	různý	k2eAgNnPc1d1	různé
studia	studio	k1gNnPc1	studio
a	a	k8xC	a
distribuční	distribuční	k2eAgFnPc1d1	distribuční
firmy	firma	k1gFnPc1	firma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
prosince	prosinec	k1gInSc2	prosinec
dostane	dostat	k5eAaPmIp3nS	dostat
každý	každý	k3xTgMnSc1	každý
aktivní	aktivní	k2eAgMnSc1d1	aktivní
člen	člen	k1gMnSc1	člen
hlasovací	hlasovací	k2eAgInSc4d1	hlasovací
lístek	lístek	k1gInSc4	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
vyplnění	vyplnění	k1gNnSc1	vyplnění
ho	on	k3xPp3gMnSc4	on
nejpozději	pozdě	k6eAd3	pozdě
odešle	odeslat	k5eAaPmIp3nS	odeslat
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
PricewaterhouseCoopers	PricewaterhouseCoopers	k1gInSc1	PricewaterhouseCoopers
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
sečíst	sečíst	k5eAaPmF	sečíst
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Hlasování	hlasování	k1gNnSc1	hlasování
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
pětadvaceti	pětadvacet	k4xCc6	pětadvacet
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
pobočky	pobočka	k1gFnSc2	pobočka
hlasují	hlasovat	k5eAaImIp3nP	hlasovat
za	za	k7c4	za
počiny	počin	k1gInPc4	počin
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
herci	herec	k1gMnPc1	herec
odevzdávají	odevzdávat	k5eAaImIp3nP	odevzdávat
hlasy	hlas	k1gInPc4	hlas
herců	herc	k1gInPc2	herc
<g/>
,	,	kIx,	,
režiséři	režisér	k1gMnPc1	režisér
režisérům	režisér	k1gMnPc3	režisér
<g/>
,	,	kIx,	,
kameramani	kameraman	k1gMnPc1	kameraman
kameramanů	kameraman	k1gMnPc2	kameraman
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnPc4	kategorie
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
a	a	k8xC	a
cizojazyčný	cizojazyčný	k2eAgInSc1d1	cizojazyčný
film	film	k1gInSc1	film
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
speciální	speciální	k2eAgFnSc2d1	speciální
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
hlasují	hlasovat	k5eAaImIp3nP	hlasovat
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
s	s	k7c7	s
hlasovacím	hlasovací	k2eAgNnSc7d1	hlasovací
právem	právo	k1gNnSc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
prvního	první	k4xOgNnSc2	první
nominačního	nominační	k2eAgNnSc2d1	nominační
kola	kolo	k1gNnSc2	kolo
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
ve	v	k7c4	v
třetí	třetí	k4xOgInSc4	třetí
lednový	lednový	k2eAgInSc4d1	lednový
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
prezident	prezident	k1gMnSc1	prezident
Akademie	akademie	k1gFnSc2	akademie
vyhlásí	vyhlásit	k5eAaPmIp3nS	vyhlásit
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Samuela	Samuel	k1gMnSc2	Samuel
Goldwyna	Goldwyn	k1gMnSc2	Goldwyn
v	v	k7c4	v
Beverly	Beverla	k1gFnPc4	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
je	být	k5eAaImIp3nS	být
členům	člen	k1gMnPc3	člen
zaslán	zaslán	k2eAgInSc4d1	zaslán
finální	finální	k2eAgInSc4d1	finální
hlasovací	hlasovací	k2eAgInSc4d1	hlasovací
lístek	lístek	k1gInSc4	lístek
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
nominovaných	nominovaný	k2eAgMnPc2d1	nominovaný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Konečné	Konečné	k2eAgInPc1d1	Konečné
hlasy	hlas	k1gInPc1	hlas
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
do	do	k7c2	do
sídla	sídlo	k1gNnSc2	sídlo
PricewaterhouseCoopers	PricewaterhouseCoopersa	k1gFnPc2	PricewaterhouseCoopersa
dorazit	dorazit	k5eAaPmF	dorazit
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
před	před	k7c7	před
udílením	udílení	k1gNnSc7	udílení
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
už	už	k6eAd1	už
tradičně	tradičně	k6eAd1	tradičně
koná	konat	k5eAaImIp3nS	konat
poslední	poslední	k2eAgFnSc4d1	poslední
únorovou	únorový	k2eAgFnSc4d1	únorová
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
kategorií	kategorie	k1gFnSc7	kategorie
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc1d1	krátký
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc1d1	krátký
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc1d1	krátký
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
a	a	k8xC	a
cizojazyčný	cizojazyčný	k2eAgInSc1d1	cizojazyčný
film	film	k1gInSc1	film
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
hlasovat	hlasovat	k5eAaImF	hlasovat
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
můžou	můžou	k?	můžou
členové	člen	k1gMnPc1	člen
až	až	k6eAd1	až
po	po	k7c6	po
předložení	předložení	k1gNnSc6	předložení
potvrzení	potvrzení	k1gNnSc2	potvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc7	jejich
projekcí	projekce	k1gFnSc7	projekce
účastnili	účastnit	k5eAaImAgMnP	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sčítaní	sčítaný	k2eAgMnPc1d1	sčítaný
hlasů	hlas	k1gInPc2	hlas
znají	znát	k5eAaImIp3nP	znát
jejich	jejich	k3xOp3gInPc1	jejich
výsledky	výsledek	k1gInPc1	výsledek
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc1	dva
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
agentury	agentura	k1gFnSc2	agentura
PricewaterhouseCoopers	PricewaterhouseCoopers	k1gInSc1	PricewaterhouseCoopers
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zapečetěna	zapečetit	k5eAaPmNgNnP	zapečetit
do	do	k7c2	do
obálek	obálka	k1gFnPc2	obálka
a	a	k8xC	a
uložena	uložen	k2eAgFnSc1d1	uložena
až	až	k9	až
do	do	k7c2	do
předávání	předávání	k1gNnSc2	předávání
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přihlášen	přihlásit	k5eAaPmNgInS	přihlásit
každý	každý	k3xTgInSc4	každý
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
(	(	kIx(	(
<g/>
nejméně	málo	k6eAd3	málo
čtyřicetiminutový	čtyřicetiminutový	k2eAgInSc1d1	čtyřicetiminutový
<g/>
)	)	kIx)	)
film	film	k1gInSc1	film
zveřejněný	zveřejněný	k2eAgInSc1d1	zveřejněný
na	na	k7c6	na
formátu	formát	k1gInSc6	formát
35	[number]	k4	35
nebo	nebo	k8xC	nebo
70	[number]	k4	70
mm	mm	kA	mm
pro	pro	k7c4	pro
klasický	klasický	k2eAgInSc4d1	klasický
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
Digital	Digital	kA	Digital
Cinema	Cinemum	k1gNnSc2	Cinemum
pro	pro	k7c4	pro
film	film	k1gInSc4	film
natáčený	natáčený	k2eAgInSc4d1	natáčený
digitální	digitální	k2eAgInSc4d1	digitální
kamerou	kamera	k1gFnSc7	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
promítán	promítat	k5eAaImNgInS	promítat
běžným	běžný	k2eAgInSc7d1	běžný
způsobem	způsob	k1gInSc7	způsob
v	v	k7c6	v
některém	některý	k3yIgNnSc6	některý
kině	kino	k1gNnSc6	kino
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
nejméně	málo	k6eAd3	málo
7	[number]	k4	7
dní	den	k1gInPc2	den
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
premiéra	premiéra	k1gFnSc1	premiéra
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
daného	daný	k2eAgInSc2d1	daný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
uvedením	uvedení	k1gNnSc7	uvedení
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
uváděn	uvádět	k5eAaImNgMnS	uvádět
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
médiu	médium	k1gNnSc6	médium
(	(	kIx(	(
<g/>
kabelová	kabelový	k2eAgFnSc1d1	kabelová
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
Internet	Internet	k1gInSc1	Internet
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kategoriích	kategorie	k1gFnPc6	kategorie
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
některých	některý	k3yIgNnPc2	některý
pravidel	pravidlo	k1gNnPc2	pravidlo
povoleny	povolen	k2eAgFnPc4d1	povolena
výjimky	výjimka	k1gFnPc4	výjimka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
cizojazyčný	cizojazyčný	k2eAgInSc1d1	cizojazyčný
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
profesní	profesní	k2eAgFnPc1d1	profesní
sekce	sekce	k1gFnPc1	sekce
Akademie	akademie	k1gFnSc2	akademie
nominují	nominovat	k5eAaBmIp3nP	nominovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
oboru	obor	k1gInSc6	obor
vždy	vždy	k6eAd1	vždy
pět	pět	k4xCc4	pět
kandidátů	kandidát	k1gMnPc2	kandidát
pro	pro	k7c4	pro
finále	finále	k1gNnSc4	finále
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
Akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
cca	cca	kA	cca
6000	[number]	k4	6000
<g/>
)	)	kIx)	)
nominuje	nominovat	k5eAaBmIp3nS	nominovat
pět	pět	k4xCc4	pět
snímků	snímek	k1gInPc2	snímek
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tajném	tajný	k2eAgNnSc6d1	tajné
hlasování	hlasování	k1gNnSc6	hlasování
opět	opět	k6eAd1	opět
všichni	všechen	k3xTgMnPc1	všechen
určí	určit	k5eAaPmIp3nP	určit
vítěze	vítěz	k1gMnPc4	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Vyhodnocení	vyhodnocení	k1gNnPc1	vyhodnocení
hlasů	hlas	k1gInPc2	hlas
provedou	provést	k5eAaPmIp3nP	provést
tři	tři	k4xCgMnPc1	tři
notáři	notár	k1gMnPc1	notár
advokátní	advokátní	k2eAgFnSc2d1	advokátní
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
vítězů	vítěz	k1gMnPc2	vítěz
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
tajná	tajný	k2eAgFnSc1d1	tajná
až	až	k6eAd1	až
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc7	člen
Americké	americký	k2eAgFnSc2d1	americká
akademie	akademie	k1gFnSc2	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
se	se	k3xPyFc4	se
automaticky	automaticky	k6eAd1	automaticky
stávají	stávat	k5eAaImIp3nP	stávat
všichni	všechen	k3xTgMnPc1	všechen
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
alespoň	alespoň	k9	alespoň
jedenkrát	jedenkrát	k6eAd1	jedenkrát
nominováni	nominován	k2eAgMnPc1d1	nominován
na	na	k7c4	na
Cenu	cena	k1gFnSc4	cena
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
tisíc	tisíc	k4xCgInPc2	tisíc
členů	člen	k1gMnPc2	člen
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
její	její	k3xOp3gMnPc4	její
členy	člen	k1gMnPc4	člen
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
patřili	patřit	k5eAaImAgMnP	patřit
nebo	nebo	k8xC	nebo
doposud	doposud	k6eAd1	doposud
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Franz	Franz	k1gMnSc1	Franz
Planer	Planer	k1gMnSc1	Planer
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Jandl	Jandl	k1gMnSc1	Jandl
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Pinkava	Pinkava	k?	Pinkava
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Ondříček	Ondříček	k1gMnSc1	Ondříček
<g/>
,	,	kIx,	,
Theodor	Theodor	k1gMnSc1	Theodor
Pištěk	Pištěk	k1gMnSc1	Pištěk
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
<g />
.	.	kIx.	.
</s>
<s>
roli	role	k1gFnSc4	role
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
původní	původní	k2eAgInSc1d1	původní
scénář	scénář	k1gInSc1	scénář
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
adaptovaný	adaptovaný	k2eAgInSc4d1	adaptovaný
scénář	scénář	k1gInSc4	scénář
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
střih	střih	k1gInSc4	střih
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
výprava	výprava	k1gFnSc1	výprava
a	a	k8xC	a
dekorace	dekorace	k1gFnSc1	dekorace
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
návrh	návrh	k1gInSc4	návrh
kostýmů	kostým	k1gInPc2	kostým
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
píseň	píseň	k1gFnSc1	píseň
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g />
.	.	kIx.	.
</s>
<s>
zvuk	zvuk	k1gInSc1	zvuk
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
masky	maska	k1gFnPc1	maska
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
střih	střih	k1gInSc1	střih
zvuku	zvuk	k1gInSc2	zvuk
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
celovečerní	celovečerní	k2eAgInSc1d1	celovečerní
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc1d1	cizojazyčný
<g />
.	.	kIx.	.
</s>
<s>
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
krátký	krátký	k2eAgInSc1d1	krátký
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátký	krátký	k2eAgInSc4d1	krátký
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Vzpomínková	vzpomínkový	k2eAgFnSc1d1	vzpomínková
cena	cena	k1gFnSc1	cena
Irvinga	Irvinga	k1gFnSc1	Irvinga
G.	G.	kA	G.
Thalberga	Thalberga	k1gFnSc1	Thalberga
-	-	kIx~	-
pro	pro	k7c4	pro
producenta	producent	k1gMnSc4	producent
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
práce	práce	k1gFnSc1	práce
trvale	trvale	k6eAd1	trvale
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
Humanitární	humanitární	k2eAgFnSc1d1	humanitární
cena	cena	k1gFnSc1	cena
Jeana	Jean	k1gMnSc2	Jean
Hersholta	Hersholt	k1gMnSc2	Hersholt
-	-	kIx~	-
pro	pro	k7c4	pro
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
humanitární	humanitární	k2eAgNnSc4d1	humanitární
úsilí	úsilí	k1gNnSc4	úsilí
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
věhlasu	věhlas	k1gInSc3	věhlas
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
<g />
.	.	kIx.	.
</s>
<s>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Čestná	čestný	k2eAgFnSc1d1	čestná
cena	cena	k1gFnSc1	cena
-	-	kIx~	-
ocenění	ocenění	k1gNnSc1	ocenění
mimořádného	mimořádný	k2eAgNnSc2d1	mimořádné
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
výjimečného	výjimečný	k2eAgNnSc2d1	výjimečné
přispění	přispění	k1gNnSc2	přispění
filmovému	filmový	k2eAgNnSc3d1	filmové
umění	umění	k1gNnSc3	umění
a	a	k8xC	a
vědám	věda	k1gFnPc3	věda
nebo	nebo	k8xC	nebo
vynikající	vynikající	k2eAgFnSc6d1	vynikající
službě	služba	k1gFnSc6	služba
Akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
vědecký	vědecký	k2eAgInSc4d1	vědecký
a	a	k8xC	a
technický	technický	k2eAgInSc4d1	technický
přínos	přínos	k1gInSc4	přínos
-	-	kIx~	-
za	za	k7c4	za
objev	objev	k1gInSc4	objev
či	či	k8xC	či
vynález	vynález	k1gInSc1	vynález
přínosný	přínosný	k2eAgInSc1d1	přínosný
filmovému	filmový	k2eAgInSc3d1	filmový
průmyslu	průmysl	k1gInSc3	průmysl
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
Cena	cena	k1gFnSc1	cena
Gordona	Gordon	k1gMnSc2	Gordon
E.	E.	kA	E.
Sawyera	Sawyer	k1gMnSc2	Sawyer
-	-	kIx~	-
pro	pro	k7c4	pro
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
technologický	technologický	k2eAgInSc1d1	technologický
přínos	přínos	k1gInSc1	přínos
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
věhlasu	věhlas	k1gInSc3	věhlas
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Pochvalná	pochvalný	k2eAgFnSc1d1	pochvalná
medaile	medaile	k1gFnSc1	medaile
Johna	John	k1gMnSc2	John
A.	A.	kA	A.
Bonnera	Bonner	k1gMnSc2	Bonner
-	-	kIx~	-
ocenění	ocenění	k1gNnSc1	ocenění
vynikajících	vynikající	k2eAgFnPc2d1	vynikající
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
vysoké	vysoký	k2eAgFnSc2d1	vysoká
úrovně	úroveň	k1gFnSc2	úroveň
Akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
umělecká	umělecký	k2eAgFnSc1d1	umělecká
kvalita	kvalita	k1gFnSc1	kvalita
produkce	produkce	k1gFnSc2	produkce
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
titulky	titulek	k1gInPc1	titulek
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
mezititulky	mezititulek	k1gInPc1	mezititulek
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
technické	technický	k2eAgInPc1d1	technický
efekty	efekt	k1gInPc1	efekt
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
námět	námět	k1gInSc4	námět
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
až	až	k9	až
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátký	krátký	k2eAgInSc4d1	krátký
dramatický	dramatický	k2eAgInSc4d1	dramatický
film	film	k1gInSc4	film
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1933	[number]	k4	1933
až	až	k9	až
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
asistent	asistent	k1gMnSc1	asistent
režie	režie	k1gFnSc2	režie
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934	[number]	k4	1934
až	až	k9	až
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
Speciální	speciální	k2eAgFnSc1d1	speciální
cena	cena	k1gFnSc1	cena
Akademie	akademie	k1gFnSc2	akademie
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
až	až	k9	až
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
tanečních	taneční	k2eAgFnPc2d1	taneční
scén	scéna	k1gFnPc2	scéna
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1936	[number]	k4	1936
až	až	k9	až
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátký	krátký	k2eAgInSc4d1	krátký
barevný	barevný	k2eAgInSc4d1	barevný
<g />
.	.	kIx.	.
</s>
<s>
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
a	a	k8xC	a
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátký	krátký	k2eAgInSc4d1	krátký
dvou	dva	k4xCgMnPc6	dva
cívkový	cívkový	k2eAgInSc1d1	cívkový
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
až	až	k9	až
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
-	-	kIx~	-
adaptace	adaptace	k1gFnSc1	adaptace
nebo	nebo	k8xC	nebo
přepracování	přepracování	k1gNnSc1	přepracování
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1963	[number]	k4	1963
až	až	k9	až
1970	[number]	k4	1970
<g/>
;	;	kIx,	;
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
přínos	přínos	k1gInSc4	přínos
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
za	za	k7c4	za
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
příslušnému	příslušný	k2eAgInSc3d1	příslušný
filmu	film	k1gInSc6	film
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
přínos	přínos	k1gInSc4	přínos
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
který	který	k3yRgInSc4	který
neexistuje	existovat	k5eNaImIp3nS	existovat
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
kategorie	kategorie	k1gFnSc1	kategorie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1973	[number]	k4	1973
až	až	k9	až
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
ke	k	k7c3	k
komedii	komedie	k1gFnSc3	komedie
anebo	anebo	k8xC	anebo
muzikálu	muzikál	k1gInSc3	muzikál
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
až	až	k9	až
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
První	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1929	[number]	k4	1929
na	na	k7c6	na
banketu	banket	k1gInSc6	banket
v	v	k7c6	v
sálu	sál	k1gInSc2	sál
Blossom	Blossom	k1gInSc1	Blossom
hollywoodského	hollywoodský	k2eAgInSc2d1	hollywoodský
hotelu	hotel	k1gInSc2	hotel
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
hodnotila	hodnotit	k5eAaImAgFnS	hodnotit
filmy	film	k1gInPc4	film
natočení	natočení	k1gNnSc2	natočení
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpnem	srpen	k1gInSc7	srpen
1927	[number]	k4	1927
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpnem	srpen	k1gInSc7	srpen
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
udílení	udílení	k1gNnSc1	udílení
trvalo	trvat	k5eAaImAgNnS	trvat
asi	asi	k9	asi
patnáct	patnáct	k4xCc1	patnáct
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
2	[number]	k4	2
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
se	se	k3xPyFc4	se
pozornost	pozornost	k1gFnSc1	pozornost
médii	médium	k1gNnPc7	médium
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
a	a	k8xC	a
večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
přenášel	přenášet	k5eAaImAgInS	přenášet
rozhlasem	rozhlas	k1gInSc7	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
ceny	cena	k1gFnPc1	cena
střídavě	střídavě	k6eAd1	střídavě
udělovaly	udělovat	k5eAaImAgFnP	udělovat
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Ambassador	Ambassadora	k1gFnPc2	Ambassadora
a	a	k8xC	a
hotelu	hotel	k1gInSc2	hotel
Biltmore	Biltmor	k1gInSc5	Biltmor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
přesunut	přesunout	k5eAaPmNgInS	přesunout
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
prostorů	prostor	k1gInPc2	prostor
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
Graumanova	Graumanův	k2eAgNnSc2d1	Graumanův
čínského	čínský	k2eAgNnSc2d1	čínské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnPc1d1	následující
léta	léto	k1gNnPc1	léto
se	se	k3xPyFc4	se
Oscar	Oscar	k1gMnSc1	Oscar
stěhoval	stěhovat	k5eAaImAgMnS	stěhovat
ze	z	k7c2	z
Shrine	Shrin	k1gInSc5	Shrin
Auditoria	auditorium	k1gNnSc2	auditorium
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
RKO	RKO	kA	RKO
Pantages	Pantages	k1gInSc1	Pantages
<g/>
.	.	kIx.	.
</s>
<s>
Odsud	odsud	k6eAd1	odsud
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
taky	taky	k6eAd1	taky
vysílal	vysílat	k5eAaImAgInS	vysílat
televizní	televizní	k2eAgInSc1d1	televizní
přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
udílení	udílení	k1gNnSc2	udílení
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
společností	společnost	k1gFnSc7	společnost
NBC	NBC	kA	NBC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
bylo	být	k5eAaImAgNnS	být
předávání	předávání	k1gNnSc1	předávání
cen	cena	k1gFnPc2	cena
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
Santa	Santo	k1gNnSc2	Santo
Monica	Monic	k1gInSc2	Monic
Civic	Civice	k1gInPc2	Civice
Auditorium	auditorium	k1gNnSc1	auditorium
<g/>
.	.	kIx.	.
</s>
<s>
Práva	práv	k2eAgNnPc1d1	právo
na	na	k7c4	na
televizní	televizní	k2eAgInSc4d1	televizní
přenos	přenos	k1gInSc4	přenos
tehdy	tehdy	k6eAd1	tehdy
získala	získat	k5eAaPmAgFnS	získat
společnost	společnost	k1gFnSc1	společnost
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
<g/>
,	,	kIx,	,
až	až	k8xS	až
na	na	k7c4	na
roky	rok	k1gInPc4	rok
1971	[number]	k4	1971
až	až	k8xS	až
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
vysílá	vysílat	k5eAaImIp3nS	vysílat
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
gala	gala	k1gNnSc1	gala
večer	večer	k6eAd1	večer
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
vlastní	vlastní	k2eAgFnSc4d1	vlastní
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1969	[number]	k4	1969
až	až	k8xS	až
1987	[number]	k4	1987
se	se	k3xPyFc4	se
ceny	cena	k1gFnPc1	cena
udělovaly	udělovat	k5eAaImAgFnP	udělovat
v	v	k7c6	v
losangeleském	losangeleský	k2eAgNnSc6d1	losangeleské
hudebním	hudební	k2eAgNnSc6d1	hudební
centru	centrum	k1gNnSc6	centrum
v	v	k7c6	v
pavilónu	pavilón	k1gInSc6	pavilón
Dorothy	Dorotha	k1gFnSc2	Dorotha
Chandler	Chandler	k1gMnSc1	Chandler
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
Oscar	Oscar	k1gMnSc1	Oscar
opět	opět	k6eAd1	opět
stěhoval	stěhovat	k5eAaImAgMnS	stěhovat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mezi	mezi	k7c7	mezi
hudebním	hudební	k2eAgInSc7d1	hudební
centre	centr	k1gInSc5	centr
a	a	k8xC	a
Shrine	Shrin	k1gInSc5	Shrin
Auditoriem	auditorium	k1gNnSc7	auditorium
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgNnPc7	tento
dvěma	dva	k4xCgNnPc7	dva
středisky	středisko	k1gNnPc7	středisko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
sedadel	sedadlo	k1gNnPc2	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Pavilón	pavilón	k1gInSc1	pavilón
disponuje	disponovat	k5eAaBmIp3nS	disponovat
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
tisícem	tisíc	k4xCgInSc7	tisíc
sedaček	sedačka	k1gFnPc2	sedačka
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Auditorium	auditorium	k1gNnSc1	auditorium
až	až	k6eAd1	až
šesti	šest	k4xCc7	šest
tisíci	tisíc	k4xCgInPc7	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
Kodak	Kodak	kA	Kodak
Theatre	Theatr	k1gInSc5	Theatr
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
sedadel	sedadlo	k1gNnPc2	sedadlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
změnilo	změnit	k5eAaPmAgNnS	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
Dolby	Dolb	k1gInPc4	Dolb
Theatre	Theatr	k1gInSc5	Theatr
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
ceny	cena	k1gFnPc1	cena
udělovaly	udělovat	k5eAaImAgFnP	udělovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
začátkem	začátkem	k7c2	začátkem
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Akademie	akademie	k1gFnSc1	akademie
udílení	udílení	k1gNnSc2	udílení
přesunula	přesunout	k5eAaPmAgFnS	přesunout
z	z	k7c2	z
pondělí	pondělí	k1gNnSc2	pondělí
na	na	k7c4	na
neděli	neděle	k1gFnSc4	neděle
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
koná	konat	k5eAaImIp3nS	konat
poslední	poslední	k2eAgFnSc4d1	poslední
únorovou	únorový	k2eAgFnSc4d1	únorová
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
Akademie	akademie	k1gFnSc1	akademie
udílení	udílení	k1gNnSc2	udílení
posouvá	posouvat	k5eAaImIp3nS	posouvat
na	na	k7c4	na
první	první	k4xOgFnSc4	první
březnovou	březnový	k2eAgFnSc4d1	březnová
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
Oscar	Oscar	k1gMnSc1	Oscar
předával	předávat	k5eAaImAgMnS	předávat
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
tahounem	tahoun	k1gInSc7	tahoun
byl	být	k5eAaImAgMnS	být
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplin	k1gInSc4	Chaplin
s	s	k7c7	s
filmem	film	k1gInSc7	film
Cirkus	cirkus	k1gInSc1	cirkus
<g/>
.	.	kIx.	.
</s>
<s>
Chaplin	Chaplin	k1gInSc1	Chaplin
však	však	k9	však
dostal	dostat	k5eAaPmAgInS	dostat
jednoho	jeden	k4xCgMnSc4	jeden
"	"	kIx"	"
<g/>
souhrnného	souhrnný	k2eAgMnSc4d1	souhrnný
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dalšího	další	k1gNnSc2	další
Chaplin	Chaplina	k1gFnPc2	Chaplina
získá	získat	k5eAaPmIp3nS	získat
za	za	k7c4	za
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
43	[number]	k4	43
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázel	doprovázet	k5eAaImAgInS	doprovázet
ho	on	k3xPp3gNnSc4	on
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
potlesk	potlesk	k1gInSc1	potlesk
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
tohoto	tento	k3xDgNnSc2	tento
udílení	udílení	k1gNnSc2	udílení
<g/>
.	.	kIx.	.
</s>
<s>
Trval	trvat	k5eAaImAgInS	trvat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
Oscar	Oscar	k1gMnSc1	Oscar
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
Wings	Wingsa	k1gFnPc2	Wingsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
začala	začít	k5eAaPmAgFnS	začít
éra	éra	k1gFnSc1	éra
zvukových	zvukový	k2eAgInPc2d1	zvukový
filmů	film	k1gInPc2	film
a	a	k8xC	a
žádný	žádný	k3yNgInSc1	žádný
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
již	jenž	k3xRgFnSc4	jenž
sošku	soška	k1gFnSc4	soška
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Kuriozitou	kuriozita	k1gFnSc7	kuriozita
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
Umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
film	film	k1gInSc1	film
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
hold	hold	k1gInSc1	hold
němému	němý	k2eAgInSc3d1	němý
filmu	film	k1gInSc3	film
a	a	k8xC	a
jako	jako	k9	jako
takový	takový	k3xDgInSc1	takový
je	být	k5eAaImIp3nS	být
též	též	k9	též
koncipován	koncipovat	k5eAaBmNgInS	koncipovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
Oscarů	Oscar	k1gInPc2	Oscar
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
proměnily	proměnit	k5eAaPmAgInP	proměnit
tři	tři	k4xCgInPc1	tři
filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
Ben-Hur	Ben-Hur	k1gMnSc1	Ben-Hur
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Titanic	Titanic	k1gInSc1	Titanic
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
měl	mít	k5eAaImAgInS	mít
současně	současně	k6eAd1	současně
největší	veliký	k2eAgFnSc4d3	veliký
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byl	být	k5eAaImAgMnS	být
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnSc1d3	veliký
smolař	smolař	k1gMnSc1	smolař
co	co	k9	co
se	se	k3xPyFc4	se
proměňování	proměňování	k1gNnSc1	proměňování
oscarových	oscarový	k2eAgFnPc2d1	oscarová
nominací	nominace	k1gFnPc2	nominace
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zvukař	zvukař	k1gMnSc1	zvukař
Kevin	Kevin	k1gMnSc1	Kevin
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Connell	Connell	k1gInSc4	Connell
(	(	kIx(	(
<g/>
20	[number]	k4	20
neproměněných	proměněný	k2eNgFnPc2d1	neproměněná
nominací	nominace	k1gFnPc2	nominace
<g/>
)	)	kIx)	)
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
Victor	Victor	k1gMnSc1	Victor
Young	Young	k1gMnSc1	Young
(	(	kIx(	(
<g/>
21	[number]	k4	21
nominací	nominace	k1gFnPc2	nominace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
nominací	nominace	k1gFnPc2	nominace
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgInP	mít
filmy	film	k1gInPc1	film
All	All	k1gFnSc2	All
about	about	k1gMnSc1	about
Eve	Eve	k1gMnSc1	Eve
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
a	a	k8xC	a
Titanic	Titanic	k1gInSc1	Titanic
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc1	tři
filmy	film	k1gInPc1	film
získaly	získat	k5eAaPmAgInP	získat
pětici	pětice	k1gFnSc4	pětice
Oscarů	Oscar	k1gMnPc2	Oscar
v	v	k7c6	v
nejuznávanějších	uznávaný	k2eAgFnPc6d3	nejuznávanější
kategoriích	kategorie	k1gFnPc6	kategorie
(	(	kIx(	(
<g/>
též	též	k6eAd1	též
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Big	Big	k1gFnSc4	Big
Five	Fiv	k1gFnSc2	Fiv
-	-	kIx~	-
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
<g/>
:	:	kIx,	:
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
jedné	jeden	k4xCgFnSc6	jeden
noci	noc	k1gFnSc6	noc
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Přelet	přelet	k1gInSc1	přelet
nad	nad	k7c7	nad
kukaččím	kukaččí	k2eAgNnSc7d1	kukaččí
hnízdem	hnízdo	k1gNnSc7	hnízdo
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mlčení	mlčení	k1gNnSc1	mlčení
jehňátek	jehňátko	k1gNnPc2	jehňátko
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
Oscarů	Oscar	k1gMnPc2	Oscar
získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
filmy	film	k1gInPc4	film
(	(	kIx(	(
<g/>
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
Mickey	Micke	k2eAgFnPc4d1	Micke
Mouse	Mouse	k1gFnPc4	Mouse
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
celkem	celkem	k6eAd1	celkem
-	-	kIx~	-
22	[number]	k4	22
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1931	[number]	k4	1931
<g/>
/	/	kIx~	/
<g/>
1932	[number]	k4	1932
až	až	k9	až
1939	[number]	k4	1939
osm	osm	k4xCc4	osm
sošek	soška	k1gFnPc2	soška
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
(	(	kIx(	(
<g/>
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
kategoriemi	kategorie	k1gFnPc7	kategorie
deset	deset	k4xCc4	deset
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
drží	držet	k5eAaImIp3nP	držet
další	další	k2eAgMnPc1d1	další
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1950	[number]	k4	1950
a	a	k8xC	a
1954	[number]	k4	1954
získal	získat	k5eAaPmAgInS	získat
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
Oscary	Oscar	k1gInPc4	Oscar
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
krátké	krátký	k2eAgInPc4d1	krátký
filmy	film	k1gInPc4	film
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953-1954	[number]	k4	1953-1954
dva	dva	k4xCgInPc1	dva
Oscary	Oscar	k1gInPc1	Oscar
dokumentární	dokumentární	k2eAgInPc1d1	dokumentární
filmy	film	k1gInPc1	film
The	The	k1gFnSc1	The
Living	Living	k1gInSc1	Living
Desert	desert	k1gInSc1	desert
a	a	k8xC	a
The	The	k1gFnSc1	The
Vanishing	Vanishing	k1gInSc4	Vanishing
Prairie	Prairie	k1gFnSc2	Prairie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
předávání	předávání	k1gNnSc2	předávání
cen	cena	k1gFnPc2	cena
byl	být	k5eAaImAgMnS	být
nejvícekrát	jvícekrát	k6eNd1	jvícekrát
nominován	nominovat	k5eAaBmNgMnS	nominovat
právě	právě	k9	právě
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
(	(	kIx(	(
<g/>
59	[number]	k4	59
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
žijících	žijící	k2eAgFnPc2d1	žijící
osobností	osobnost	k1gFnPc2	osobnost
pak	pak	k6eAd1	pak
skladatel	skladatel	k1gMnSc1	skladatel
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
John	John	k1gMnSc1	John
Williams	Williams	k1gInSc1	Williams
(	(	kIx(	(
<g/>
48	[number]	k4	48
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvěma	dva	k4xCgFnPc7	dva
hercům	herc	k1gInPc3	herc
a	a	k8xC	a
dvěma	dva	k4xCgFnPc3	dva
herečkám	herečka	k1gFnPc3	herečka
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
dvakrát	dvakrát	k6eAd1	dvakrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hereček	herečka	k1gFnPc2	herečka
to	ten	k3xDgNnSc4	ten
byly	být	k5eAaImAgFnP	být
Luise	Luisa	k1gFnSc6	Luisa
Rainerová	Rainerová	k1gFnSc1	Rainerová
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
a	a	k8xC	a
Katharine	Katharin	k1gInSc5	Katharin
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
herců	herc	k1gInPc2	herc
Spencer	Spencer	k1gInSc4	Spencer
Tracy	Traca	k1gFnSc2	Traca
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanksa	k1gFnPc2	Hanksa
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
nejvíce	hodně	k6eAd3	hodně
Oscarů	Oscar	k1gMnPc2	Oscar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kostýmní	kostýmní	k2eAgFnSc1d1	kostýmní
výtvarnice	výtvarnice	k1gFnSc1	výtvarnice
Edit	Edita	k1gFnPc2	Edita
Head	Heada	k1gFnPc2	Heada
-	-	kIx~	-
odnesla	odnést	k5eAaPmAgFnS	odnést
si	se	k3xPyFc3	se
jich	on	k3xPp3gMnPc2	on
8	[number]	k4	8
<g/>
,	,	kIx,	,
nominací	nominace	k1gFnPc2	nominace
měla	mít	k5eAaImAgFnS	mít
dokonce	dokonce	k9	dokonce
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
Oscarů	Oscar	k1gInPc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
makeup	makeup	k1gInSc4	makeup
-	-	kIx~	-
celkem	celkem	k6eAd1	celkem
7	[number]	k4	7
-	-	kIx~	-
získal	získat	k5eAaPmAgMnS	získat
Rick	Rick	k1gMnSc1	Rick
Baker	Baker	k1gMnSc1	Baker
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
naposled	naposled	k6eAd1	naposled
stala	stát	k5eAaPmAgFnS	stát
remíza	remíza	k1gFnSc1	remíza
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
Oscara	Oscara	k1gFnSc1	Oscara
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
herečku	herečka	k1gFnSc4	herečka
získaly	získat	k5eAaPmAgFnP	získat
herečky	herečka	k1gFnPc1	herečka
Katharine	Katharin	k1gInSc5	Katharin
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
a	a	k8xC	a
Barbra	Barbra	k1gFnSc1	Barbra
Streisandová	Streisandový	k2eAgFnSc1d1	Streisandová
<g/>
.	.	kIx.	.
</s>
<s>
Katharine	Katharin	k1gInSc5	Katharin
Hepburnová	Hepburnový	k2eAgFnSc1d1	Hepburnová
je	být	k5eAaImIp3nS	být
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
herečka	herečka	k1gFnSc1	herečka
-	-	kIx~	-
získala	získat	k5eAaPmAgFnS	získat
4	[number]	k4	4
Oscary	Oscara	k1gFnSc2	Oscara
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
herečku	herečka	k1gFnSc4	herečka
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
Oscarů	Oscar	k1gInPc2	Oscar
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
získal	získat	k5eAaPmAgMnS	získat
John	John	k1gMnSc1	John
Ford	ford	k1gInSc1	ford
-	-	kIx~	-
čtyři	čtyři	k4xCgMnPc1	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
černé	černý	k2eAgFnSc2d1	černá
pleti	pleť	k1gFnSc2	pleť
mají	mít	k5eAaImIp3nP	mít
zástupce	zástupce	k1gMnSc1	zástupce
např.	např.	kA	např.
v	v	k7c4	v
Hattie	Hattie	k1gFnPc4	Hattie
McDanielové	McDanielová	k1gFnSc2	McDanielová
(	(	kIx(	(
<g/>
Jih	jih	k1gInSc1	jih
proti	proti	k7c3	proti
severu	sever	k1gInSc3	sever
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Sidney	Sidney	k1gInPc1	Sidney
Poitier	Poitira	k1gFnPc2	Poitira
(	(	kIx(	(
<g/>
Lilies	Lilies	k1gInSc1	Lilies
of	of	k?	of
the	the	k?	the
Field	Field	k1gMnSc1	Field
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
či	či	k8xC	či
Jennifer	Jennifer	k1gMnSc1	Jennifer
Hudsonová	Hudsonová	k1gFnSc1	Hudsonová
(	(	kIx(	(
<g/>
Dreamgirls	Dreamgirls	k1gInSc1	Dreamgirls
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
jak	jak	k8xS	jak
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
tak	tak	k8xC	tak
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
kategorii	kategorie	k1gFnSc6	kategorie
poprvé	poprvé	k6eAd1	poprvé
triumfovali	triumfovat	k5eAaBmAgMnP	triumfovat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
-	-	kIx~	-
tehdy	tehdy	k6eAd1	tehdy
sošku	soška	k1gFnSc4	soška
získali	získat	k5eAaPmAgMnP	získat
Halle	Hall	k1gMnSc4	Hall
Berry	Berra	k1gMnSc2	Berra
(	(	kIx(	(
<g/>
Monster	monstrum	k1gNnPc2	monstrum
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ball	Balla	k1gFnPc2	Balla
<g/>
)	)	kIx)	)
a	a	k8xC	a
Denzel	Denzel	k1gFnSc1	Denzel
Washington	Washington	k1gInSc1	Washington
(	(	kIx(	(
<g/>
Training	Training	k1gInSc1	Training
Day	Day	k1gFnSc2	Day
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Meryl	Meryl	k1gInSc1	Meryl
Streepová	Streepová	k1gFnSc1	Streepová
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
počet	počet	k1gInSc4	počet
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proměnila	proměnit	k5eAaPmAgFnS	proměnit
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
jen	jen	k9	jen
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
Čechem	Čech	k1gMnSc7	Čech
je	být	k5eAaImIp3nS	být
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
-	-	kIx~	-
získal	získat	k5eAaPmAgMnS	získat
2	[number]	k4	2
Oscary	Oscar	k1gInPc4	Oscar
(	(	kIx(	(
<g/>
Přelet	přelet	k1gInSc4	přelet
nad	nad	k7c7	nad
kukaččím	kukaččí	k2eAgNnSc7d1	kukaččí
hnízdem	hnízdo	k1gNnSc7	hnízdo
a	a	k8xC	a
Amadeus	Amadeus	k1gMnSc1	Amadeus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
nikdy	nikdy	k6eAd1	nikdy
Oscara	Oscar	k1gMnSc2	Oscar
nezískal	získat	k5eNaPmAgInS	získat
<g/>
,	,	kIx,	,
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Oscar	Oscar	k1gInSc1	Oscar
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Okomentoval	okomentovat	k5eAaPmAgMnS	okomentovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Thank	Thank	k1gInSc1	Thank
you	you	k?	you
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Steven	Steven	k2eAgInSc1d1	Steven
Spielberg	Spielberg	k1gInSc1	Spielberg
byl	být	k5eAaImAgInS	být
20	[number]	k4	20
let	léto	k1gNnPc2	léto
brán	brán	k2eAgMnSc1d1	brán
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Hollywoodu	Hollywood	k1gInSc2	Hollywood
(	(	kIx(	(
<g/>
jen	jen	k9	jen
za	za	k7c4	za
produkci	produkce	k1gFnSc4	produkce
byl	být	k5eAaImAgMnS	být
nominován	nominován	k2eAgMnSc1d1	nominován
8	[number]	k4	8
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
získal	získat	k5eAaPmAgMnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
Schindlerův	Schindlerův	k2eAgInSc4d1	Schindlerův
seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Scorsese	Scorsese	k1gFnSc2	Scorsese
5	[number]	k4	5
<g/>
×	×	k?	×
prohrál	prohrát	k5eAaPmAgInS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Oscara	Oscara	k1gFnSc1	Oscara
získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
film	film	k1gInSc4	film
Skrytá	skrytý	k2eAgFnSc1d1	skrytá
identita	identita	k1gFnSc1	identita
<g/>
.	.	kIx.	.
</s>
<s>
Roberto	Roberta	k1gFnSc5	Roberta
Benigni	Benigeň	k1gFnSc5	Benigeň
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
Oscara	Oscara	k1gFnSc1	Oscara
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
a	a	k8xC	a
také	také	k9	také
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Benigni	Benign	k1gMnPc1	Benign
to	ten	k3xDgNnSc4	ten
okomentoval	okomentovat	k5eAaPmAgMnS	okomentovat
"	"	kIx"	"
<g/>
I	i	k9	i
use	usus	k1gInSc5	usus
up	up	k?	up
all	all	k?	all
of	of	k?	of
my	my	k3xPp1nPc1	my
English	English	k1gInSc4	English
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vypotřeboval	vypotřebovat	k5eAaPmAgMnS	vypotřebovat
jsem	být	k5eAaImIp1nS	být
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
vydáním	vydání	k1gNnSc7	vydání
filmu	film	k1gInSc2	film
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
ocenění	ocenění	k1gNnSc2	ocenění
soškou	soška	k1gFnSc7	soška
filmové	filmový	k2eAgFnSc2d1	filmová
akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
celých	celý	k2eAgNnPc2d1	celé
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
případ	případ	k1gInSc1	případ
filmu	film	k1gInSc2	film
Světla	světlo	k1gNnSc2	světlo
ramp	rampa	k1gFnPc2	rampa
s	s	k7c7	s
Charlie	Charlie	k1gMnSc1	Charlie
Chaplinem	Chaplin	k1gInSc7	Chaplin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vydání	vydání	k1gNnSc2	vydání
filmu	film	k1gInSc2	film
byl	být	k5eAaImAgInS	být
kontroverzně	kontroverzně	k6eAd1	kontroverzně
vyšetřován	vyšetřován	k2eAgInSc1d1	vyšetřován
Výborem	výbor	k1gInSc7	výbor
pro	pro	k7c4	pro
neamerickou	americký	k2eNgFnSc4d1	neamerická
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
záležitost	záležitost	k1gFnSc1	záležitost
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
oceněných	oceněný	k2eAgMnPc2d1	oceněný
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
děkovné	děkovný	k2eAgFnSc6d1	děkovná
řeči	řeč	k1gFnSc6	řeč
poděkovala	poděkovat	k5eAaPmAgFnS	poděkovat
kromě	kromě	k7c2	kromě
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
spolupracovníkům	spolupracovník	k1gMnPc3	spolupracovník
také	také	k9	také
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Woody	Woody	k6eAd1	Woody
Allen	Allen	k1gMnSc1	Allen
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
nominován	nominován	k2eAgMnSc1d1	nominován
15	[number]	k4	15
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
nominaci	nominace	k1gFnSc4	nominace
proměnil	proměnit	k5eAaPmAgInS	proměnit
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
žádnému	žádný	k3yNgMnSc3	žádný
scenáristovi	scenárista	k1gMnSc3	scenárista
se	se	k3xPyFc4	se
ale	ale	k9	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
Oscara	Oscar	k1gMnSc4	Oscar
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
žádné	žádný	k3yNgFnSc3	žádný
herečce	herečka	k1gFnSc3	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
cen	cena	k1gFnPc2	cena
akademie	akademie	k1gFnSc2	akademie
získali	získat	k5eAaPmAgMnP	získat
dva	dva	k4xCgMnPc1	dva
herci	herec	k1gMnPc1	herec
Oscara	Oscar	k1gMnSc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
ztvárňující	ztvárňující	k2eAgInSc4d1	ztvárňující
jednu	jeden	k4xCgFnSc4	jeden
a	a	k8xC	a
tutéž	týž	k3xTgFnSc4	týž
postavu	postava	k1gFnSc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
Marlon	Marlon	k1gInSc1	Marlon
Brando	Brando	k6eAd1	Brando
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
De	De	k?	De
Niro	Niro	k1gMnSc1	Niro
-	-	kIx~	-
Kmotr	kmotr	k1gMnSc1	kmotr
a	a	k8xC	a
Kmotr	kmotr	k1gMnSc1	kmotr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
