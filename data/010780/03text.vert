<p>
<s>
Mikojan-Gurevič	Mikojan-Gurevič	k1gInSc1	Mikojan-Gurevič
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
(	(	kIx(	(
<g/>
V	v	k7c6	v
kódu	kód	k1gInSc6	kód
NATO	NATO	kA	NATO
"	"	kIx"	"
<g/>
Fishbed	Fishbed	k1gMnSc1	Fishbed
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sovětská	sovětský	k2eAgFnSc1d1	sovětská
nadzvuková	nadzvukový	k2eAgFnSc1d1	nadzvuková
víceúčelová	víceúčelový	k2eAgFnSc1d1	víceúčelová
stíhačka	stíhačka	k1gFnSc1	stíhačka
používaná	používaný	k2eAgFnSc1d1	používaná
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
letectvy	letectv	k1gInPc7	letectv
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
v	v	k7c6	v
několika	několik	k4yIc6	několik
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jich	on	k3xPp3gMnPc2	on
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
přes	přes	k7c4	přes
10	[number]	k4	10
000	[number]	k4	000
ks	ks	kA	ks
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nP	činit
nejvíce	nejvíce	k6eAd1	nejvíce
a	a	k8xC	a
nejdéle	dlouho	k6eAd3	dlouho
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
nadzvukový	nadzvukový	k2eAgInSc1d1	nadzvukový
letoun	letoun	k1gInSc1	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
se	se	k3xPyFc4	se
mnoha	mnoho	k4c2	mnoho
ozbrojených	ozbrojený	k2eAgInPc2d1	ozbrojený
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
v	v	k7c6	v
arabsko-izraelských	arabskozraelský	k2eAgFnPc6d1	arabsko-izraelská
válkách	válka	k1gFnPc6	válka
či	či	k8xC	či
válce	válka	k1gFnSc6	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zavedením	zavedení	k1gNnSc7	zavedení
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
vývoj	vývoj	k1gInSc1	vývoj
letounu	letoun	k1gInSc2	letoun
neskončil	skončit	k5eNaPmAgInS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
změny	změna	k1gFnPc1	změna
konstrukce	konstrukce	k1gFnSc2	konstrukce
byly	být	k5eAaImAgInP	být
mj.	mj.	kA	mj.
ověřeny	ověřen	k2eAgInPc1d1	ověřen
na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
Je-	Je-	k1gFnSc2	Je-
<g/>
66	[number]	k4	66
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letouny	letoun	k1gInPc1	letoun
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
byly	být	k5eAaImAgFnP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
armád	armáda	k1gFnPc2	armáda
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesáti	padesát	k4xCc2	padesát
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
byly	být	k5eAaImAgFnP	být
i	i	k9	i
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
Československé	československý	k2eAgFnSc2d1	Československá
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
poté	poté	k6eAd1	poté
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Licenčně	licenčně	k6eAd1	licenčně
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
1963-1972	[number]	k4	1963-1972
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
Aero	aero	k1gNnSc4	aero
Vodochody	Vodochod	k1gInPc5	Vodochod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
generace	generace	k1gFnSc1	generace
===	===	k?	===
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
konstrukci	konstrukce	k1gFnSc6	konstrukce
experimentálních	experimentální	k2eAgInPc2d1	experimentální
strojů	stroj	k1gInPc2	stroj
Je-	Je-	k1gFnSc2	Je-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Je-	Je-	k1gFnSc1	Je-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Je-	Je-	k1gFnSc1	Je-
<g/>
2	[number]	k4	2
<g/>
A	A	kA	A
a	a	k8xC	a
Je-	Je-	k1gMnPc1	Je-
<g/>
5	[number]	k4	5
odvozených	odvozený	k2eAgInPc2d1	odvozený
od	od	k7c2	od
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
let	léto	k1gNnPc2	léto
prototypu	prototyp	k1gInSc2	prototyp
Je-	Je-	k1gFnSc2	Je-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
s	s	k7c7	s
pilotem	pilot	k1gMnSc7	pilot
Sedovem	Sedov	k1gInSc7	Sedov
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Konstruktéři	konstruktér	k1gMnPc1	konstruktér
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
aplikovali	aplikovat	k5eAaBmAgMnP	aplikovat
trojúhelníkové	trojúhelníkový	k2eAgNnSc4d1	trojúhelníkové
delta	delta	k1gNnSc4	delta
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zapadalo	zapadat	k5eAaImAgNnS	zapadat
do	do	k7c2	do
koncepce	koncepce	k1gFnSc2	koncepce
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
při	při	k7c6	při
stoupání	stoupání	k1gNnSc6	stoupání
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zkušeným	zkušený	k2eAgMnSc7d1	zkušený
pilotem	pilot	k1gMnSc7	pilot
a	a	k8xC	a
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
výzbrojí	výzbroj	k1gFnSc7	výzbroj
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
pro	pro	k7c4	pro
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
moderní	moderní	k2eAgFnSc4d1	moderní
stíhačku	stíhačka	k1gFnSc4	stíhačka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
i	i	k9	i
prototyp	prototyp	k1gInSc1	prototyp
Je-	Je-	k1gFnSc2	Je-
<g/>
6	[number]	k4	6
<g/>
;	;	kIx,	;
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zkoušek	zkouška	k1gFnPc2	zkouška
zničený	zničený	k2eAgMnSc1d1	zničený
při	při	k7c6	při
havárii	havárie	k1gFnSc6	havárie
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
zahynul	zahynout	k5eAaPmAgMnS	zahynout
i	i	k8xC	i
zkušební	zkušební	k2eAgMnSc1d1	zkušební
pilot	pilot	k1gMnSc1	pilot
Vladimir	Vladimir	k1gMnSc1	Vladimir
Nefedov	Nefedov	k1gInSc4	Nefedov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
položil	položit	k5eAaPmAgInS	položit
základy	základ	k1gInPc4	základ
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nového	nový	k2eAgMnSc2d1	nový
Je-	Je-	k1gMnSc2	Je-
<g/>
66	[number]	k4	66
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
dost	dost	k6eAd1	dost
rekordů	rekord	k1gInPc2	rekord
potvrzených	potvrzený	k2eAgInPc2d1	potvrzený
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
organizací	organizace	k1gFnSc7	organizace
FAI	FAI	kA	FAI
<g/>
.	.	kIx.	.
</s>
<s>
Sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
letounů	letoun	k1gInPc2	letoun
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
během	během	k7c2	během
výroby	výroba	k1gFnSc2	výroba
upravovány	upravovat	k5eAaImNgFnP	upravovat
a	a	k8xC	a
modernizovány	modernizovat	k5eAaBmNgFnP	modernizovat
pro	pro	k7c4	pro
aktuální	aktuální	k2eAgFnPc4d1	aktuální
potřeby	potřeba	k1gFnPc4	potřeba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
MiGy-	MiGy-	k?	MiGy-
<g/>
21	[number]	k4	21
se	se	k3xPyFc4	se
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
Gorkém	Gorkij	k1gMnSc6	Gorkij
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Nižnij	Nižnij	k1gFnSc1	Nižnij
Novgorod	Novgorod	k1gInSc1	Novgorod
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
sovětský	sovětský	k2eAgInSc1d1	sovětský
stíhací	stíhací	k2eAgInSc1d1	stíhací
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
porovnatelný	porovnatelný	k2eAgInSc1d1	porovnatelný
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
F-104	F-104	k1gMnSc7	F-104
Starfighter	Starfightra	k1gFnPc2	Starfightra
<g/>
,	,	kIx,	,
či	či	k8xC	či
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
Dassault	Dassault	k1gMnSc1	Dassault
Mirage	Mirage	k1gFnSc3	Mirage
III	III	kA	III
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
první	první	k4xOgFnSc7	první
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
skloubila	skloubit	k5eAaPmAgFnS	skloubit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
klasického	klasický	k2eAgMnSc2d1	klasický
se	s	k7c7	s
záchytným	záchytný	k2eAgMnSc7d1	záchytný
stíhačem	stíhač	k1gMnSc7	stíhač
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
mohly	moct	k5eAaImAgInP	moct
nést	nést	k5eAaImF	nést
řízené	řízený	k2eAgInPc1d1	řízený
střely	střel	k1gInPc1	střel
Vympel	Vympela	k1gFnPc2	Vympela
K-	K-	k1gFnPc2	K-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
kopií	kopie	k1gFnSc7	kopie
amerických	americký	k2eAgFnPc2d1	americká
AIM-9	AIM-9	k1gFnPc2	AIM-9
Sidewinder	Sidewinder	k1gMnSc1	Sidewinder
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
však	však	k9	však
nové	nový	k2eAgInPc1d1	nový
MiGy	mig	k1gInPc1	mig
nebyly	být	k5eNaImAgInP	být
zprvu	zprvu	k6eAd1	zprvu
příliš	příliš	k6eAd1	příliš
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Rakety	raketa	k1gFnPc1	raketa
K-13	K-13	k1gFnSc2	K-13
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgInP	ukázat
být	být	k5eAaImF	být
lehce	lehko	k6eAd1	lehko
vymanévrovatelné	vymanévrovatelný	k2eAgFnPc4d1	vymanévrovatelný
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
hlavně	hlavně	k9	hlavně
během	během	k7c2	během
bojů	boj	k1gInPc2	boj
na	na	k7c6	na
blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
první	první	k4xOgFnSc1	první
sériová	sériový	k2eAgFnSc1d1	sériová
verze	verze	k1gFnSc1	verze
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
neměla	mít	k5eNaImAgFnS	mít
zabudovaný	zabudovaný	k2eAgInSc4d1	zabudovaný
radar	radar	k1gInSc4	radar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
lety	léto	k1gNnPc7	léto
jen	jen	k9	jen
během	během	k7c2	během
dne	den	k1gInSc2	den
a	a	k8xC	a
za	za	k7c2	za
dobrého	dobrý	k2eAgNnSc2d1	dobré
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
modernizacím	modernizace	k1gFnPc3	modernizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jiná	jiný	k2eAgNnPc1d1	jiné
letadla	letadlo	k1gNnPc1	letadlo
konstruovaná	konstruovaný	k2eAgNnPc1d1	konstruované
jako	jako	k8xC	jako
záchytné	záchytný	k2eAgMnPc4d1	záchytný
stíhače	stíhač	k1gMnPc4	stíhač
i	i	k8xC	i
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
měl	mít	k5eAaImAgInS	mít
krátký	krátký	k2eAgInSc1d1	krátký
dolet	dolet	k1gInSc1	dolet
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
měl	mít	k5eAaImAgInS	mít
navíc	navíc	k6eAd1	navíc
zprvu	zprvu	k6eAd1	zprvu
vestavěnou	vestavěný	k2eAgFnSc4d1	vestavěná
konstrukční	konstrukční	k2eAgFnSc4d1	konstrukční
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
těžiště	těžiště	k1gNnSc1	těžiště
letadla	letadlo	k1gNnSc2	letadlo
posunulo	posunout	k5eAaPmAgNnS	posunout
dozadu	dozadu	k6eAd1	dozadu
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
snižovalo	snižovat	k5eAaImAgNnS	snižovat
jeho	jeho	k3xOp3gFnSc4	jeho
ovládatelnost	ovládatelnost	k1gFnSc4	ovládatelnost
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
i	i	k9	i
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
výdrž	výdrž	k1gFnSc4	výdrž
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
za	za	k7c2	za
dobrých	dobrý	k2eAgFnPc2d1	dobrá
podmínek	podmínka	k1gFnPc2	podmínka
nepřesáhla	přesáhnout	k5eNaPmAgFnS	přesáhnout
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Delta	delta	k1gFnSc1	delta
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sice	sice	k8xC	sice
zlepšovalo	zlepšovat	k5eAaImAgNnS	zlepšovat
stoupavost	stoupavost	k1gFnSc4	stoupavost
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
menší	malý	k2eAgFnSc2d2	menší
manévrovací	manévrovací	k2eAgFnSc2d1	manévrovací
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
,	,	kIx,	,
až	až	k9	až
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
odstraněn	odstranit	k5eAaPmNgInS	odstranit
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
SPS	SPS	kA	SPS
(	(	kIx(	(
<g/>
sduva	sduva	k6eAd1	sduva
pograničnovo	pograničnův	k2eAgNnSc1d1	pograničnův
sloja	sloja	k6eAd1	sloja
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
ofukování	ofukování	k1gNnSc1	ofukování
klapek	klapka	k1gFnPc2	klapka
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
vztlaku	vztlak	k1gInSc2	vztlak
křídla	křídlo	k1gNnSc2	křídlo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pomalý	pomalý	k2eAgInSc1d1	pomalý
pohyb	pohyb	k1gInSc1	pohyb
vpřed	vpřed	k6eAd1	vpřed
nevytváří	vytvářit	k5eNaPmIp3nS	vytvářit
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
vztlak	vztlak	k1gInSc4	vztlak
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
dobrou	dobrý	k2eAgFnSc4d1	dobrá
ovládatelnost	ovládatelnost	k1gFnSc4	ovládatelnost
stroje	stroj	k1gInSc2	stroj
i	i	k9	i
při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
snahy	snaha	k1gFnSc2	snaha
vyrobit	vyrobit	k5eAaPmF	vyrobit
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
druhé	druhý	k4xOgFnSc2	druhý
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
určený	určený	k2eAgMnSc1d1	určený
k	k	k7c3	k
vybojování	vybojování	k1gNnSc3	vybojování
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
nadvlády	nadvláda	k1gFnSc2	nadvláda
za	za	k7c2	za
dobrého	dobrý	k2eAgNnSc2d1	dobré
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zkonstruovány	zkonstruován	k2eAgInPc1d1	zkonstruován
dva	dva	k4xCgInPc1	dva
nové	nový	k2eAgInPc1d1	nový
prototypy	prototyp	k1gInPc1	prototyp
<g/>
:	:	kIx,	:
Je-	Je-	k1gFnSc1	Je-
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
a	a	k8xC	a
Je-	Je-	k1gMnSc3	Je-
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
poprvé	poprvé	k6eAd1	poprvé
vznesly	vznést	k5eAaPmAgFnP	vznést
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1958	[number]	k4	1958
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
přímými	přímý	k2eAgMnPc7d1	přímý
pokračovateli	pokračovatel	k1gMnPc7	pokračovatel
stroje	stroj	k1gInSc2	stroj
Je-	Je-	k1gFnSc1	Je-
<g/>
6	[number]	k4	6
<g/>
T	T	kA	T
a	a	k8xC	a
také	také	k9	také
je	být	k5eAaImIp3nS	být
poháněl	pohánět	k5eAaImAgMnS	pohánět
motor	motor	k1gInSc4	motor
Tumanskij	Tumanskij	k1gMnSc2	Tumanskij
R-	R-	k1gFnSc2	R-
<g/>
11	[number]	k4	11
<g/>
F-	F-	k1gFnSc1	F-
<g/>
300	[number]	k4	300
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgFnP	lišit
větším	veliký	k2eAgInSc7d2	veliký
zádovým	zádový	k2eAgInSc7d1	zádový
hřebenem	hřeben	k1gInSc7	hřeben
a	a	k8xC	a
delším	dlouhý	k2eAgInSc7d2	delší
<g/>
,	,	kIx,	,
o	o	k7c4	o
16	[number]	k4	16
cm	cm	kA	cm
širší	široký	k2eAgInSc4d2	širší
trupem	trup	k1gInSc7	trup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
nový	nový	k2eAgInSc1d1	nový
radar	radar	k1gInSc1	radar
TSD-30T	TSD-30T	k1gFnSc1	TSD-30T
(	(	kIx(	(
<g/>
RP	RP	kA	RP
-21M	-21M	k4	-21M
Safír	safír	k1gInSc4	safír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pitotova	Pitotův	k2eAgFnSc1d1	Pitotova
trubice	trubice	k1gFnSc1	trubice
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
na	na	k7c4	na
vrchní	vrchní	k2eAgFnSc4d1	vrchní
část	část	k1gFnSc4	část
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
stíhačka	stíhačka	k1gFnSc1	stíhačka
vzlétat	vzlétat	k5eAaImF	vzlétat
i	i	k9	i
z	z	k7c2	z
travnatých	travnatý	k2eAgFnPc2d1	travnatá
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
většími	veliký	k2eAgInPc7d2	veliký
koly	kolo	k1gNnPc7	kolo
a	a	k8xC	a
upevňovacími	upevňovací	k2eAgInPc7d1	upevňovací
body	bod	k1gInPc7	bod
pro	pro	k7c4	pro
pomocné	pomocný	k2eAgInPc4d1	pomocný
raketové	raketový	k2eAgInPc4d1	raketový
motory	motor	k1gInPc4	motor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
usnadňovaly	usnadňovat	k5eAaImAgInP	usnadňovat
vzlet	vzlet	k1gInSc4	vzlet
a	a	k8xC	a
po	po	k7c6	po
startu	start	k1gInSc6	start
byly	být	k5eAaImAgFnP	být
odhazovány	odhazovat	k5eAaImNgFnP	odhazovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
neměly	mít	k5eNaImAgInP	mít
instalovány	instalovat	k5eAaBmNgInP	instalovat
žádné	žádný	k3yNgFnPc4	žádný
palubní	palubní	k2eAgFnPc4d1	palubní
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Je-	Je-	k1gFnSc4	Je-
<g/>
7	[number]	k4	7
mohl	moct	k5eAaImAgInS	moct
nést	nést	k5eAaImF	nést
2	[number]	k4	2
rakety	raketa	k1gFnPc4	raketa
K-13	K-13	k1gFnSc2	K-13
(	(	kIx(	(
<g/>
AA-	AA-	k1gFnPc2	AA-
<g/>
2	[number]	k4	2
Atoll	Atolla	k1gFnPc2	Atolla
<g/>
)	)	kIx)	)
a	a	k8xC	a
osazen	osadit	k5eAaPmNgInS	osadit
optickým	optický	k2eAgInSc7d1	optický
zaměřovačem	zaměřovač	k1gInSc7	zaměřovač
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
použití	použití	k1gNnSc4	použití
neřízených	řízený	k2eNgFnPc2d1	neřízená
raket	raketa	k1gFnPc2	raketa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sérii	série	k1gFnSc6	série
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
začít	začít	k5eAaPmF	začít
sériovou	sériový	k2eAgFnSc4d1	sériová
výrobu	výroba	k1gFnSc4	výroba
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
P	P	kA	P
(	(	kIx(	(
<g/>
Fishbed-D	Fishbed-D	k1gMnSc1	Fishbed-D
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
P	P	kA	P
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
v	v	k7c6	v
Gorkém	Gorkij	k1gMnSc6	Gorkij
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
představitelem	představitel	k1gMnSc7	představitel
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
druhé	druhý	k4xOgFnSc2	druhý
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
(	(	kIx(	(
<g/>
Fishbed-D	Fishbed-D	k1gMnSc1	Fishbed-D
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
zlepšení	zlepšení	k1gNnSc1	zlepšení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kapacity	kapacita	k1gFnSc2	kapacita
palivové	palivový	k2eAgFnSc2d1	palivová
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
nést	nést	k5eAaImF	nést
širší	široký	k2eAgFnSc4d2	širší
paletu	paleta	k1gFnSc4	paleta
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
vylepšenou	vylepšený	k2eAgFnSc4d1	vylepšená
avioniku	avionika	k1gFnSc4	avionika
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
byl	být	k5eAaImAgMnS	být
vyráběn	vyráběn	k2eAgMnSc1d1	vyráběn
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
množstvích	množství	k1gNnPc6	množství
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
v	v	k7c6	v
Gorkém	Gorkij	k1gMnSc6	Gorkij
a	a	k8xC	a
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Různá	různý	k2eAgNnPc1d1	různé
vylepšení	vylepšení	k1gNnPc1	vylepšení
se	se	k3xPyFc4	se
promítla	promítnout	k5eAaPmAgFnS	promítnout
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
verze	verze	k1gFnSc1	verze
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
(	(	kIx(	(
<g/>
Fishbed-F	Fishbed-F	k1gMnSc1	Fishbed-F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Směrové	směrový	k2eAgNnSc1d1	směrové
kormidlo	kormidlo	k1gNnSc1	kormidlo
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
modelu	model	k1gInSc6	model
zvětšeno	zvětšen	k2eAgNnSc1d1	zvětšeno
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zvýšit	zvýšit	k5eAaPmF	zvýšit
podélnou	podélný	k2eAgFnSc4d1	podélná
stabilitu	stabilita	k1gFnSc4	stabilita
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
přepracováno	přepracován	k2eAgNnSc1d1	přepracováno
umístění	umístění	k1gNnSc1	umístění
brzdícího	brzdící	k2eAgInSc2d1	brzdící
padáku	padák	k1gInSc2	padák
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
kořeni	kořen	k1gInSc6	kořen
směrového	směrový	k2eAgNnSc2d1	směrové
kormidla	kormidlo	k1gNnSc2	kormidlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
úpravou	úprava	k1gFnSc7	úprava
bylo	být	k5eAaImAgNnS	být
přidání	přidání	k1gNnSc1	přidání
systému	systém	k1gInSc2	systém
SPS	SPS	kA	SPS
(	(	kIx(	(
<g/>
sduva	sduva	k6eAd1	sduva
pograničnovo	pograničnův	k2eAgNnSc1d1	pograničnův
sloje	sloj	k1gInPc4	sloj
-	-	kIx~	-
ofukování	ofukování	k1gNnSc3	ofukování
mezní	mezní	k2eAgFnSc2d1	mezní
vrstvy	vrstva	k1gFnSc2	vrstva
vzduchem	vzduch	k1gInSc7	vzduch
odebíraným	odebíraný	k2eAgInSc7d1	odebíraný
od	od	k7c2	od
kompresoru	kompresor	k1gInSc2	kompresor
motoru	motor	k1gInSc2	motor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
u	u	k7c2	u
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
.	.	kIx.	.
</s>
<s>
Přepracován	přepracovat	k5eAaPmNgInS	přepracovat
byl	být	k5eAaImAgInS	být
i	i	k9	i
překryt	překrýt	k5eAaPmNgInS	překrýt
kabiny	kabina	k1gFnSc2	kabina
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
nyní	nyní	k6eAd1	nyní
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
a	a	k8xC	a
otvíral	otvírat	k5eAaImAgInS	otvírat
se	se	k3xPyFc4	se
do	do	k7c2	do
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
instalace	instalace	k1gFnSc2	instalace
nového	nový	k2eAgNnSc2d1	nové
vystřelovacího	vystřelovací	k2eAgNnSc2d1	vystřelovací
sedadla	sedadlo	k1gNnSc2	sedadlo
KM-	KM-	k1gFnSc2	KM-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
výzbroje	výzbroj	k1gFnSc2	výzbroj
sestávající	sestávající	k2eAgFnSc2d1	sestávající
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
raket	raketa	k1gFnPc2	raketa
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
na	na	k7c4	na
podtrupové	podtrupový	k2eAgInPc4d1	podtrupový
závěsníky	závěsník	k1gInPc4	závěsník
upevněné	upevněný	k2eAgInPc4d1	upevněný
kontejnery	kontejner	k1gInPc4	kontejner
s	s	k7c7	s
kanónem	kanón	k1gInSc7	kanón
GS-	GS-	k1gFnSc1	GS-
<g/>
23	[number]	k4	23
<g/>
,	,	kIx,	,
ráže	ráže	k1gFnSc1	ráže
23	[number]	k4	23
mm	mm	kA	mm
a	a	k8xC	a
se	s	k7c7	s
zásobou	zásoba	k1gFnSc7	zásoba
200	[number]	k4	200
nábojů	náboj	k1gInPc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
MiGy-	MiGy-	k?	MiGy-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
v	v	k7c6	v
Gorkém	Gorkij	k1gMnSc6	Gorkij
av	av	k?	av
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
byla	být	k5eAaImAgFnS	být
směrována	směrovat	k5eAaImNgFnS	směrovat
pro	pro	k7c4	pro
sovětské	sovětský	k2eAgNnSc4d1	sovětské
letectvo	letectvo	k1gNnSc4	letectvo
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
export	export	k1gInSc4	export
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
v	v	k7c6	v
licenci	licence	k1gFnSc6	licence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
FL	FL	kA	FL
byla	být	k5eAaImAgFnS	být
verze	verze	k1gFnSc1	verze
vyráběna	vyráběn	k2eAgFnSc1d1	vyráběna
v	v	k7c6	v
licenci	licence	k1gFnSc6	licence
indickou	indický	k2eAgFnSc7d1	indická
společností	společnost	k1gFnSc7	společnost
HAL	hala	k1gFnPc2	hala
v	v	k7c6	v
letech	let	k1gInPc6	let
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
MiG-	MiG-	k1gFnSc6	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
s	s	k7c7	s
méně	málo	k6eAd2	málo
výkonným	výkonný	k2eAgInSc7d1	výkonný
radarem	radar	k1gInSc7	radar
R-	R-	k1gFnSc1	R-
<g/>
2	[number]	k4	2
<g/>
L.	L.	kA	L.
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
mohly	moct	k5eAaImAgInP	moct
používat	používat	k5eAaImF	používat
i	i	k9	i
rakety	raketa	k1gFnPc1	raketa
západní	západní	k2eAgFnSc2d1	západní
výroby	výroba	k1gFnSc2	výroba
jako	jako	k9	jako
např.	např.	kA	např.
francouzská	francouzský	k2eAgFnSc1d1	francouzská
"	"	kIx"	"
<g/>
Matra	Matra	k1gFnSc1	Matra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
zlepšení	zlepšení	k1gNnPc1	zlepšení
vyústila	vyústit	k5eAaPmAgNnP	vyústit
do	do	k7c2	do
modelu	model	k1gInSc2	model
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
PFMA	PFMA	kA	PFMA
(	(	kIx(	(
<g/>
Fishbed-J	Fishbed-J	k1gMnSc1	Fishbed-J
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
hlavním	hlavní	k2eAgNnSc7d1	hlavní
zlepšením	zlepšení	k1gNnSc7	zlepšení
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
radar	radar	k1gInSc1	radar
RP-22	RP-22	k1gFnSc2	RP-22
a	a	k8xC	a
vystřelovací	vystřelovací	k2eAgFnSc1d1	vystřelovací
sedačka	sedačka	k1gFnSc1	sedačka
s	s	k7c7	s
charakteristikami	charakteristika	k1gFnPc7	charakteristika
0-0	[number]	k4	0-0
(	(	kIx(	(
<g/>
možnost	možnost	k1gFnSc4	možnost
opustit	opustit	k5eAaPmF	opustit
letadlo	letadlo	k1gNnSc4	letadlo
v	v	k7c6	v
nulové	nulový	k2eAgFnSc6d1	nulová
výši	výše	k1gFnSc6	výše
při	při	k7c6	při
nulové	nulový	k2eAgFnSc6d1	nulová
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zásoba	zásoba	k1gFnSc1	zásoba
neseného	nesený	k2eAgNnSc2d1	nesené
paliva	palivo	k1gNnSc2	palivo
byla	být	k5eAaImAgFnS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohla	moct	k5eAaImAgFnS	moct
nést	nést	k5eAaImF	nést
přídavné	přídavný	k2eAgFnSc2d1	přídavná
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
,	,	kIx,	,
Pitotova	Pitotův	k2eAgFnSc1d1	Pitotova
trubice	trubice	k1gFnSc1	trubice
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
mírně	mírně	k6eAd1	mírně
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1	výzbroj
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
raket	raketa	k1gFnPc2	raketa
R-3	R-3	k1gFnSc2	R-3
a	a	k8xC	a
na	na	k7c4	na
podtrupové	podtrupový	k2eAgInPc4d1	podtrupový
závěsníky	závěsník	k1gInPc4	závěsník
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
instalován	instalován	k2eAgInSc4d1	instalován
kanón	kanón	k1gInSc4	kanón
místo	místo	k7c2	místo
přídavné	přídavný	k2eAgFnSc2d1	přídavná
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Průzkumná	průzkumný	k2eAgFnSc1d1	průzkumná
verze	verze	k1gFnSc1	verze
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
(	(	kIx(	(
<g/>
Fishbed-H	Fishbed-H	k1gMnSc1	Fishbed-H
<g/>
)	)	kIx)	)
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
průzkumné	průzkumný	k2eAgNnSc4d1	průzkumné
vybavení	vybavení	k1gNnSc4	vybavení
nesla	nést	k5eAaImAgFnS	nést
v	v	k7c6	v
kontejneru	kontejner	k1gInSc6	kontejner
umístěném	umístěný	k2eAgInSc6d1	umístěný
na	na	k7c6	na
podtrupovém	podtrupový	k2eAgInSc6d1	podtrupový
závěsníku	závěsník	k1gInSc6	závěsník
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vlastní	vlastní	k2eAgFnSc3d1	vlastní
obraně	obrana	k1gFnSc3	obrana
nesl	nést	k5eAaImAgMnS	nést
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
pár	pár	k4xCyI	pár
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
instalovanou	instalovaný	k2eAgFnSc4d1	instalovaná
zvětšenou	zvětšený	k2eAgFnSc4d1	zvětšená
palivovou	palivový	k2eAgFnSc4d1	palivová
nádrž	nádrž	k1gFnSc4	nádrž
a	a	k8xC	a
dodatečnou	dodatečný	k2eAgFnSc4d1	dodatečná
avioniku	avionika	k1gFnSc4	avionika
v	v	k7c6	v
zádové	zádový	k2eAgFnSc6d1	zádová
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
táhla	táhnout	k5eAaImAgFnS	táhnout
od	od	k7c2	od
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
kabiny	kabina	k1gFnSc2	kabina
až	až	k9	až
po	po	k7c4	po
začátek	začátek	k1gInSc4	začátek
směrového	směrový	k2eAgNnSc2d1	směrové
kormidla	kormidlo	k1gNnSc2	kormidlo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
křídla	křídlo	k1gNnPc4	křídlo
byly	být	k5eAaImAgInP	být
přidány	přidat	k5eAaPmNgInP	přidat
další	další	k2eAgInPc4d1	další
2	[number]	k4	2
pylony	pylon	k1gInPc4	pylon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
umístěny	umístit	k5eAaPmNgFnP	umístit
přídavné	přídavný	k2eAgFnPc1d1	přídavná
palivové	palivový	k2eAgFnPc1d1	palivová
nádrže	nádrž	k1gFnPc1	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Výbava	výbava	k1gFnSc1	výbava
průzkumného	průzkumný	k2eAgInSc2d1	průzkumný
kontejneru	kontejner	k1gInSc2	kontejner
závisela	záviset	k5eAaImAgFnS	záviset
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
misi	mise	k1gFnSc6	mise
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
byl	být	k5eAaImAgMnS	být
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
nasazený	nasazený	k2eAgMnSc1d1	nasazený
<g/>
.	.	kIx.	.
</s>
<s>
Mohla	moct	k5eAaImAgFnS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
fotografické	fotografický	k2eAgNnSc4d1	fotografické
nebo	nebo	k8xC	nebo
elektronické	elektronický	k2eAgNnSc4d1	elektronické
sledovací	sledovací	k2eAgNnSc4d1	sledovací
zařízení	zařízení	k1gNnSc4	zařízení
více	hodně	k6eAd2	hodně
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
vyráběn	vyrábět	k5eAaImNgMnS	vyrábět
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
množstvích	množství	k1gNnPc6	množství
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
v	v	k7c6	v
Gorkém	Gorkij	k1gMnSc6	Gorkij
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgFnSc1	třetí
generace	generace	k1gFnSc1	generace
===	===	k?	===
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
začala	začít	k5eAaPmAgFnS	začít
produkcí	produkce	k1gFnSc7	produkce
verze	verze	k1gFnSc1	verze
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
s.	s.	k?	s.
Modely	model	k1gInPc4	model
třetí	třetí	k4xOgFnSc2	třetí
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
přebraly	přebrat	k5eAaPmAgInP	přebrat
většinu	většina	k1gFnSc4	většina
zlepšení	zlepšení	k1gNnSc2	zlepšení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
na	na	k7c6	na
předešlých	předešlý	k2eAgFnPc6d1	předešlá
verzích	verze	k1gFnPc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
instalaci	instalace	k1gFnSc4	instalace
palivové	palivový	k2eAgFnSc2d1	palivová
nádrže	nádrž	k1gFnSc2	nádrž
za	za	k7c7	za
kabinou	kabina	k1gFnSc7	kabina
(	(	kIx(	(
<g/>
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
340	[number]	k4	340
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překryt	překrýt	k5eAaPmNgInS	překrýt
kabiny	kabina	k1gFnSc2	kabina
otevírající	otevírající	k2eAgFnSc2d1	otevírající
se	se	k3xPyFc4	se
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
pylony	pylon	k1gInPc4	pylon
pod	pod	k7c7	pod
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
určenými	určený	k2eAgNnPc7d1	určené
k	k	k7c3	k
instalaci	instalace	k1gFnSc3	instalace
dvou	dva	k4xCgMnPc6	dva
490	[number]	k4	490
l	l	kA	l
odhazovatelných	odhazovatelný	k2eAgFnPc2d1	odhazovatelný
nádrží	nádrž	k1gFnPc2	nádrž
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
protiletadlových	protiletadlový	k2eAgFnPc2d1	protiletadlová
raket	raketa	k1gFnPc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
instalována	instalovat	k5eAaBmNgFnS	instalovat
nová	nový	k2eAgFnSc1d1	nová
pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
-	-	kIx~	-
motor	motor	k1gInSc1	motor
R-11F2S	R-11F2S	k1gFnSc2	R-11F2S
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
SPS	SPS	kA	SPS
a	a	k8xC	a
autopilot	autopilot	k1gMnSc1	autopilot
AP-	AP-	k1gFnSc2	AP-
<g/>
155	[number]	k4	155
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
nový	nový	k2eAgInSc1d1	nový
radar	radar	k1gInSc1	radar
RP-22S	RP-22S	k1gFnPc2	RP-22S
Safír	safír	k1gInSc1	safír
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
optickým	optický	k2eAgInSc7d1	optický
zaměřovačem	zaměřovač	k1gInSc7	zaměřovač
ASP-PF	ASP-PF	k1gMnSc2	ASP-PF
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nahradil	nahradit	k5eAaPmAgMnS	nahradit
starší	starší	k1gMnPc4	starší
PKI-	PKI-	k1gFnSc2	PKI-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
<g/>
SM	SM	kA	SM
(	(	kIx(	(
<g/>
M-modernizovaný	Modernizovaný	k2eAgMnSc1d1	M-modernizovaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
poháněn	pohánět	k5eAaImNgInS	pohánět
motorem	motor	k1gInSc7	motor
Tumanskij	Tumanskij	k1gMnPc2	Tumanskij
R-13-300	R-13-300	k1gMnSc2	R-13-300
a	a	k8xC	a
vybaven	vybavit	k5eAaPmNgInS	vybavit
palubním	palubní	k2eAgInSc7d1	palubní
kanónem	kanón	k1gInSc7	kanón
GŠ-	GŠ-	k1gFnSc1	GŠ-
<g/>
23	[number]	k4	23
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
trupu	trup	k1gInSc6	trup
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předešlých	předešlý	k2eAgFnPc2d1	předešlá
verzí	verze	k1gFnPc2	verze
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ho	on	k3xPp3gMnSc4	on
měly	mít	k5eAaImAgInP	mít
uložené	uložený	k2eAgInPc1d1	uložený
ve	v	k7c6	v
výstupku	výstupek	k1gInSc6	výstupek
po	po	k7c6	po
trupem	trup	k1gMnSc7	trup
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
především	především	k6eAd1	především
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zkušeností	zkušenost	k1gFnPc2	zkušenost
získaných	získaný	k2eAgFnPc2d1	získaná
během	během	k7c2	během
bojů	boj	k1gInPc2	boj
na	na	k7c6	na
blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
MiGy-	MiGy-	k1gFnSc1	MiGy-
<g/>
21	[number]	k4	21
vybavené	vybavený	k2eAgInPc1d1	vybavený
pouze	pouze	k6eAd1	pouze
raketovými	raketový	k2eAgFnPc7d1	raketová
střelami	střela	k1gFnPc7	střela
neosvědčily	osvědčit	k5eNaPmAgFnP	osvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
nový	nový	k2eAgInSc1d1	nový
zaměřovač	zaměřovač	k1gInSc1	zaměřovač
ASP-PDF	ASP-PDF	k1gMnPc2	ASP-PDF
<g/>
,	,	kIx,	,
se	s	k7c7	s
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
zabudovaným	zabudovaný	k2eAgFnPc3d1	zabudovaná
do	do	k7c2	do
krytu	kryt	k1gInSc2	kryt
kabiny	kabina	k1gFnSc2	kabina
<g/>
.	.	kIx.	.
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1	výzbroj
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
raket	raketa	k1gFnPc2	raketa
R-3T	R-3T	k1gFnSc2	R-3T
nebo	nebo	k8xC	nebo
R-	R-	k1gFnSc7	R-
<g/>
3	[number]	k4	3
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
bomb	bomba	k1gFnPc2	bomba
a	a	k8xC	a
neřízených	řízený	k2eNgFnPc2d1	neřízená
raket	raketa	k1gFnPc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Zásoba	zásoba	k1gFnSc1	zásoba
paliva	palivo	k1gNnSc2	palivo
byla	být	k5eAaImAgFnS	být
snížena	snížit	k5eAaPmNgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
SM	SM	kA	SM
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
pro	pro	k7c4	pro
sovětské	sovětský	k2eAgNnSc4d1	sovětské
letectvo	letectvo	k1gNnSc4	letectvo
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
v	v	k7c6	v
Gorkém	Gorkij	k1gMnSc6	Gorkij
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
do	do	k7c2	do
1974	[number]	k4	1974
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
množstvích	množství	k1gNnPc6	množství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
byl	být	k5eAaImAgInS	být
verzí	verze	k1gFnSc7	verze
odvozenou	odvozený	k2eAgFnSc7d1	odvozená
od	od	k7c2	od
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
SM	SM	kA	SM
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
slabším	slabý	k2eAgInSc7d2	slabší
radarem	radar	k1gInSc7	radar
RP-	RP-	k1gFnSc1	RP-
<g/>
21	[number]	k4	21
<g/>
MA	MA	kA	MA
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
výkonnými	výkonný	k2eAgInPc7d1	výkonný
motory	motor	k1gInPc7	motor
R-	R-	k1gMnSc2	R-
<g/>
11	[number]	k4	11
<g/>
F	F	kA	F
<g/>
2	[number]	k4	2
<g/>
S-	S-	k1gFnSc2	S-
<g/>
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
<s>
Kanón	kanón	k1gInSc1	kanón
byl	být	k5eAaImAgInS	být
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
zabudován	zabudovat	k5eAaPmNgInS	zabudovat
do	do	k7c2	do
trupu	trup	k1gInSc2	trup
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
pod	pod	k7c7	pod
trupem	trup	k1gInSc7	trup
nesena	nesen	k2eAgFnSc1d1	nesena
přídavná	přídavný	k2eAgFnSc1d1	přídavná
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
jen	jen	k9	jen
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
"	"	kIx"	"
<g/>
Znamja	Znamja	k1gFnSc1	Znamja
Trudy	trud	k1gInPc4	trud
<g/>
"	"	kIx"	"
v	v	k7c6	v
letech	let	k1gInPc6	let
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
získala	získat	k5eAaPmAgFnS	získat
Indie	Indie	k1gFnSc1	Indie
licenci	licence	k1gFnSc4	licence
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgInSc4	první
sériový	sériový	k2eAgInSc4d1	sériový
stroj	stroj	k1gInSc4	stroj
dodali	dodat	k5eAaPmAgMnP	dodat
indickém	indický	k2eAgNnSc6d1	indické
letectvu	letectvo	k1gNnSc6	letectvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
HAL	hala	k1gFnPc2	hala
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
(	(	kIx(	(
<g/>
Fishbed-J	Fishbed-J	k1gMnSc1	Fishbed-J
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
verze	verze	k1gFnSc1	verze
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
motorem	motor	k1gInSc7	motor
Tumanskij	Tumanskij	k1gMnSc2	Tumanskij
R-13-300	R-13-300	k1gMnSc2	R-13-300
a	a	k8xC	a
radarem	radar	k1gInSc7	radar
R-22	R-22	k1gFnSc2	R-22
Safír	safír	k1gInSc1	safír
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
neseného	nesený	k2eAgNnSc2d1	nesené
paliva	palivo	k1gNnSc2	palivo
bylo	být	k5eAaImAgNnS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohl	moct	k5eAaImAgInS	moct
nést	nést	k5eAaImF	nést
přídavné	přídavný	k2eAgFnPc4d1	přídavná
nádrže	nádrž	k1gFnPc4	nádrž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
prodloužily	prodloužit	k5eAaPmAgFnP	prodloužit
dolet	dolet	k1gInSc4	dolet
<g/>
.	.	kIx.	.
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1	výzbroj
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
kanónu	kanón	k1gInSc2	kanón
GS-23L	GS-23L	k1gFnPc2	GS-23L
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
bomb	bomba	k1gFnPc2	bomba
<g/>
,	,	kIx,	,
neřízených	řízený	k2eNgFnPc2d1	neřízená
a	a	k8xC	a
řízených	řízený	k2eAgFnPc2d1	řízená
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
raketových	raketový	k2eAgInPc2d1	raketový
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
převáženy	převážet	k5eAaImNgInP	převážet
na	na	k7c6	na
čtyřech	čtyři	k4xCgInPc6	čtyři
pylonech	pylon	k1gInPc6	pylon
po	po	k7c6	po
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
se	se	k3xPyFc4	se
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
hlavně	hlavně	k6eAd1	hlavně
k	k	k7c3	k
exportu	export	k1gInSc3	export
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Gorkém	Gorkij	k1gMnSc6	Gorkij
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
sloužila	sloužit	k5eAaImAgFnS	sloužit
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
počtech	počet	k1gInPc6	počet
i	i	k8xC	i
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
sériově	sériově	k6eAd1	sériově
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
firmou	firma	k1gFnSc7	firma
HAL	hala	k1gFnPc2	hala
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
MT	MT	kA	MT
byl	být	k5eAaImAgMnS	být
upravenou	upravený	k2eAgFnSc7d1	upravená
verzí	verze	k1gFnSc7	verze
MiGu-	MiGu-	k1gFnSc7	MiGu-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
větší	veliký	k2eAgFnSc7d2	veliký
zásobou	zásoba	k1gFnSc7	zásoba
neseného	nesený	k2eAgNnSc2d1	nesené
paliva	palivo	k1gNnSc2	palivo
v	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
za	za	k7c7	za
pilotní	pilotní	k2eAgFnSc7d1	pilotní
kabinou	kabina	k1gFnSc7	kabina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moskevském	moskevský	k2eAgInSc6d1	moskevský
závodě	závod	k1gInSc6	závod
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
celkem	celkem	k6eAd1	celkem
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
jen	jen	k9	jen
15	[number]	k4	15
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
MT	MT	kA	MT
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
typ	typ	k1gInSc1	typ
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
novými	nový	k2eAgInPc7d1	nový
stroji	stroj	k1gInPc7	stroj
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
23	[number]	k4	23
a	a	k8xC	a
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
27	[number]	k4	27
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
univerzálnější	univerzální	k2eAgMnPc4d2	univerzálnější
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
až	až	k9	až
příchod	příchod	k1gInSc4	příchod
typu	typ	k1gInSc2	typ
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
29	[number]	k4	29
znamenal	znamenat	k5eAaImAgInS	znamenat
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
náhradu	náhrada	k1gFnSc4	náhrada
manévrovatelné	manévrovatelný	k2eAgFnSc2d1	manévrovatelná
stíhačky	stíhačka	k1gFnSc2	stíhačka
za	za	k7c4	za
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
porovnávat	porovnávat	k5eAaImF	porovnávat
s	s	k7c7	s
americkými	americký	k2eAgInPc7d1	americký
typy	typ	k1gInPc7	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poslední	poslední	k2eAgFnPc1d1	poslední
generace	generace	k1gFnPc1	generace
===	===	k?	===
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
SMT	SMT	kA	SMT
(	(	kIx(	(
<g/>
Fishbed-K	Fishbed-K	k1gMnSc1	Fishbed-K
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
první	první	k4xOgFnSc7	první
verzí	verze	k1gFnSc7	verze
této	tento	k3xDgFnSc2	tento
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
křížence	kříženka	k1gFnSc6	kříženka
modelů	model	k1gInPc2	model
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
a	a	k8xC	a
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
MT	MT	kA	MT
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
MiGu-	MiGu-	k1gMnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
trup	trup	k1gInSc1	trup
a	a	k8xC	a
výzbroj	výzbroj	k1gFnSc1	výzbroj
<g/>
,	,	kIx,	,
z	z	k7c2	z
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
MT	MT	kA	MT
motor	motor	k1gInSc1	motor
R-13F-300	R-13F-300	k1gFnSc1	R-13F-300
a	a	k8xC	a
identická	identický	k2eAgFnSc1d1	identická
kapacita	kapacita	k1gFnSc1	kapacita
palivových	palivový	k2eAgFnPc2d1	palivová
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
rozměrná	rozměrný	k2eAgFnSc1d1	rozměrná
palivová	palivový	k2eAgFnSc1d1	palivová
nádrž	nádrž	k1gFnSc1	nádrž
za	za	k7c7	za
kabinou	kabina	k1gFnSc7	kabina
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
nestabilitu	nestabilita	k1gFnSc4	nestabilita
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc1	její
objem	objem	k1gInSc1	objem
zmenšen	zmenšit	k5eAaPmNgInS	zmenšit
na	na	k7c4	na
300	[number]	k4	300
l	l	kA	l
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgMnS	snížit
i	i	k9	i
dolet	dolet	k1gInSc4	dolet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
lehčí	lehký	k2eAgFnSc1d2	lehčí
díky	díky	k7c3	díky
použití	použití	k1gNnSc3	použití
titanových	titanový	k2eAgFnPc2d1	titanová
součástí	součást	k1gFnPc2	součást
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
verzi	verze	k1gFnSc4	verze
dokud	dokud	k8xS	dokud
nebude	být	k5eNaImBp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
motor	motor	k1gInSc1	motor
Tumanskij	Tumanskij	k1gMnSc2	Tumanskij
R-	R-	k1gMnSc2	R-
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběl	vyrábět	k5eAaImAgInS	vyrábět
se	se	k3xPyFc4	se
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Gorkém	Gorkij	k1gMnSc6	Gorkij
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
bojů	boj	k1gInPc2	boj
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
Východě	východ	k1gInSc6	východ
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
boje	boj	k1gInPc4	boj
odehrály	odehrát	k5eAaPmAgFnP	odehrát
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
výškách	výška	k1gFnPc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
zapotřebí	zapotřebí	k6eAd1	zapotřebí
silná	silný	k2eAgNnPc1d1	silné
<g/>
,	,	kIx,	,
obratná	obratný	k2eAgNnPc1d1	obratné
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
vyzbrojená	vyzbrojený	k2eAgFnSc1d1	vyzbrojená
stíhačka	stíhačka	k1gFnSc1	stíhačka
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
zásobou	zásoba	k1gFnSc7	zásoba
neseného	nesený	k2eAgNnSc2d1	nesené
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
požadavky	požadavek	k1gInPc1	požadavek
vyústily	vyústit	k5eAaPmAgInP	vyústit
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
určené	určený	k2eAgNnSc1d1	určené
k	k	k7c3	k
boji	boj	k1gInSc3	boj
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
výškách	výška	k1gFnPc6	výška
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
bis	bis	k?	bis
(	(	kIx(	(
<g/>
Fishbed-L	Fishbed-L	k1gMnSc1	Fishbed-L
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
novým	nový	k2eAgInSc7d1	nový
motorem	motor	k1gInSc7	motor
R-	R-	k1gFnSc1	R-
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
se	se	k3xPyFc4	se
dotkly	dotknout	k5eAaPmAgFnP	dotknout
i	i	k9	i
avioniky	avionika	k1gFnPc1	avionika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byl	být	k5eAaImAgInS	být
nainstalován	nainstalován	k2eAgInSc1d1	nainstalován
nový	nový	k2eAgInSc1d1	nový
radar	radar	k1gInSc1	radar
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
letadlo	letadlo	k1gNnSc1	letadlo
získalo	získat	k5eAaPmAgNnS	získat
schopnost	schopnost	k1gFnSc4	schopnost
působit	působit	k5eAaImF	působit
i	i	k9	i
za	za	k7c2	za
nepříznivého	příznivý	k2eNgNnSc2d1	nepříznivé
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1	výzbroj
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
kanónu	kanón	k1gInSc2	kanón
GŠ-	GŠ-	k1gFnSc2	GŠ-
<g/>
23	[number]	k4	23
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
raket	raketa	k1gFnPc2	raketa
R-55	R-55	k1gFnPc2	R-55
(	(	kIx(	(
<g/>
AA-	AA-	k1gFnPc2	AA-
<g/>
1	[number]	k4	1
Alkali	Alkali	k1gFnPc2	Alkali
<g/>
)	)	kIx)	)
a	a	k8xC	a
R-60M	R-60M	k1gMnSc1	R-60M
(	(	kIx(	(
<g/>
AA-	AA-	k1gMnSc1	AA-
<g/>
8	[number]	k4	8
Aphid	Aphid	k1gInSc1	Aphid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
dvojnásobný	dvojnásobný	k2eAgInSc4d1	dvojnásobný
dolet	dolet	k1gInSc4	dolet
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
dříve	dříve	k6eAd2	dříve
používanou	používaný	k2eAgFnSc7d1	používaná
R-	R-	k1gFnSc7	R-
<g/>
3	[number]	k4	3
<g/>
S.	S.	kA	S.
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
se	se	k3xPyFc4	se
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
Gorkém	Gorkij	k1gMnSc6	Gorkij
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
2	[number]	k4	2
030	[number]	k4	030
ks	ks	kA	ks
a	a	k8xC	a
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
zařazováním	zařazování	k1gNnSc7	zařazování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
firmou	firma	k1gFnSc7	firma
HAL	hala	k1gFnPc2	hala
a	a	k8xC	a
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
200	[number]	k4	200
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Existovala	existovat	k5eAaImAgFnS	existovat
i	i	k9	i
upravená	upravený	k2eAgFnSc1d1	upravená
verze	verze	k1gFnSc1	verze
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
Fishbed-N	Fishbed-N	k1gMnSc1	Fishbed-N
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
zabudovaný	zabudovaný	k2eAgInSc4d1	zabudovaný
motor	motor	k1gInSc4	motor
R-25	R-25	k1gFnSc2	R-25
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
spotřebou	spotřeba	k1gFnSc7	spotřeba
a	a	k8xC	a
vylepšenou	vylepšený	k2eAgFnSc7d1	vylepšená
avionikou	avionika	k1gFnSc7	avionika
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
byl	být	k5eAaImAgMnS	být
i	i	k9	i
palubní	palubní	k2eAgInSc4d1	palubní
systém	systém	k1gInSc4	systém
přistávání	přistávání	k1gNnSc2	přistávání
podle	podle	k7c2	podle
přístrojů	přístroj	k1gInPc2	přístroj
(	(	kIx(	(
<g/>
ILS	ILS	kA	ILS
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Poljot	Poljot	k1gInSc1	Poljot
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dvoumístné	dvoumístný	k2eAgInPc1d1	dvoumístný
modely	model	k1gInPc1	model
===	===	k?	===
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
se	se	k3xPyFc4	se
radikálně	radikálně	k6eAd1	radikálně
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
od	od	k7c2	od
všech	všecek	k3xTgInPc2	všecek
předešlých	předešlý	k2eAgInPc2d1	předešlý
dlouho	dlouho	k6eAd1	dlouho
používaných	používaný	k2eAgInPc2d1	používaný
strojů	stroj	k1gInPc2	stroj
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
silná	silný	k2eAgFnSc1d1	silná
potřeba	potřeba	k1gFnSc1	potřeba
vyvinout	vyvinout	k5eAaPmF	vyvinout
dvousedadlovou	dvousedadlový	k2eAgFnSc4d1	dvousedadlová
verzi	verze	k1gFnSc4	verze
pro	pro	k7c4	pro
výcvik	výcvik	k1gInSc4	výcvik
nových	nový	k2eAgMnPc2d1	nový
pilotů	pilot	k1gMnPc2	pilot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1959	[number]	k4	1959
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
sériových	sériový	k2eAgMnPc2d1	sériový
MiGů-	MiGů-	k1gMnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gMnPc2	F-
<g/>
13	[number]	k4	13
upraven	upravit	k5eAaPmNgInS	upravit
přidáním	přidání	k1gNnSc7	přidání
druhého	druhý	k4xOgNnSc2	druhý
sedadla	sedadlo	k1gNnSc2	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Kokpit	kokpit	k1gInSc1	kokpit
nového	nový	k2eAgNnSc2d1	nové
letadla	letadlo	k1gNnSc2	letadlo
byl	být	k5eAaImAgInS	být
chráněn	chránit	k5eAaImNgInS	chránit
celistvým	celistvý	k2eAgInSc7d1	celistvý
překrytem	překryt	k1gInSc7	překryt
kabiny	kabina	k1gFnSc2	kabina
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
otevíral	otevírat	k5eAaImAgInS	otevírat
do	do	k7c2	do
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
první	první	k4xOgFnSc1	první
dvousedadlový	dvousedadlový	k2eAgInSc1d1	dvousedadlový
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
(	(	kIx(	(
<g/>
Mongol-A	Mongol-A	k1gMnSc1	Mongol-A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
poháněna	pohánět	k5eAaImNgFnS	pohánět
motorem	motor	k1gInSc7	motor
R-	R-	k1gFnSc2	R-
<g/>
11	[number]	k4	11
<g/>
F-	F-	k1gFnSc1	F-
<g/>
300	[number]	k4	300
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměla	mít	k5eNaImAgFnS	mít
žádný	žádný	k3yNgInSc4	žádný
radar	radar	k1gInSc4	radar
ani	ani	k8xC	ani
pevnou	pevný	k2eAgFnSc4d1	pevná
výzbroj	výzbroj	k1gFnSc4	výzbroj
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ale	ale	k8xC	ale
existovala	existovat	k5eAaImAgFnS	existovat
možnost	možnost	k1gFnSc4	možnost
umístit	umístit	k5eAaPmF	umístit
pod	pod	k7c4	pod
trup	trup	k1gInSc4	trup
závěsníky	závěsník	k1gInPc4	závěsník
s	s	k7c7	s
kulometem	kulomet	k1gInSc7	kulomet
ráže	ráže	k1gFnSc1	ráže
12,7	[number]	k4	12,7
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prototyp	prototyp	k1gInSc1	prototyp
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1960	[number]	k4	1960
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
množstvích	množství	k1gNnPc6	množství
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1962	[number]	k4	1962
<g/>
-	-	kIx~	-
<g/>
66	[number]	k4	66
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc4	stroj
pro	pro	k7c4	pro
export	export	k1gInSc4	export
byly	být	k5eAaImAgFnP	být
vyráběny	vyráběn	k2eAgFnPc1d1	vyráběna
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jednosedadlových	jednosedadlový	k2eAgFnPc2d1	jednosedadlová
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
i	i	k9	i
dvousedadlový	dvousedadlový	k2eAgMnSc1d1	dvousedadlový
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
postupně	postupně	k6eAd1	postupně
upravován	upravovat	k5eAaImNgInS	upravovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
piloti	pilot	k1gMnPc1	pilot
zvykli	zvyknout	k5eAaPmAgMnP	zvyknout
na	na	k7c4	na
rostoucí	rostoucí	k2eAgInPc4d1	rostoucí
výkony	výkon	k1gInPc4	výkon
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
lišila	lišit	k5eAaImAgFnS	lišit
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
zásadních	zásadní	k2eAgInPc6d1	zásadní
ohledech	ohled	k1gInPc6	ohled
<g/>
:	:	kIx,	:
bylo	být	k5eAaImAgNnS	být
zvětšeno	zvětšit	k5eAaPmNgNnS	zvětšit
směrové	směrový	k2eAgNnSc1d1	směrové
kormidlo	kormidlo	k1gNnSc1	kormidlo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
přesunutí	přesunutí	k1gNnSc4	přesunutí
brzdícího	brzdící	k2eAgInSc2d1	brzdící
padáku	padák	k1gInSc2	padák
do	do	k7c2	do
kořene	kořen	k1gInSc2	kořen
směrového	směrový	k2eAgNnSc2d1	směrové
kormidla	kormidlo	k1gNnSc2	kormidlo
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
nový	nový	k2eAgInSc1d1	nový
motor	motor	k1gInSc1	motor
R-	R-	k1gFnSc2	R-
<g/>
112	[number]	k4	112
<g/>
S-	S-	k1gFnSc1	S-
<g/>
300	[number]	k4	300
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
instalovaný	instalovaný	k2eAgInSc1d1	instalovaný
systém	systém	k1gInSc1	systém
SPS	SPS	kA	SPS
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
předtím	předtím	k6eAd1	předtím
nebyl	být	k5eNaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
žádný	žádný	k3yNgInSc1	žádný
radar	radar	k1gInSc1	radar
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
jako	jako	k8xS	jako
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
US	US	kA	US
(	(	kIx(	(
<g/>
Mongol-B	Mongol-B	k1gMnSc1	Mongol-B
<g/>
)	)	kIx)	)
a	a	k8xC	a
oproti	oproti	k7c3	oproti
svému	svůj	k3xOyFgMnSc3	svůj
předchůdci	předchůdce	k1gMnSc3	předchůdce
také	také	k6eAd1	také
nesl	nést	k5eAaImAgMnS	nést
větší	veliký	k2eAgFnSc4d2	veliký
zásobu	zásoba	k1gFnSc4	zásoba
paliva	palivo	k1gNnSc2	palivo
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
nové	nový	k2eAgFnPc4d1	nová
vystřelovací	vystřelovací	k2eAgFnPc4d1	vystřelovací
sedačky	sedačka	k1gFnPc4	sedačka
KM-	KM-	k1gFnSc2	KM-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
kabiny	kabina	k1gFnSc2	kabina
byl	být	k5eAaImAgInS	být
sklopný	sklopný	k2eAgInSc1d1	sklopný
periskop	periskop	k1gInSc1	periskop
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc1d1	umožňující
učiteli	učitel	k1gMnPc7	učitel
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
činnost	činnost	k1gFnSc4	činnost
žáka	žák	k1gMnSc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
US	US	kA	US
se	se	k3xPyFc4	se
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
pro	pro	k7c4	pro
SSSR	SSSR	kA	SSSR
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
export	export	k1gInSc4	export
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
v	v	k7c6	v
letech	let	k1gInPc6	let
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
modelem	model	k1gInSc7	model
dvoumístných	dvoumístný	k2eAgInPc2d1	dvoumístný
strojů	stroj	k1gInPc2	stroj
byl	být	k5eAaImAgInS	být
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zvenčí	zvenčí	k6eAd1	zvenčí
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
totožný	totožný	k2eAgInSc1d1	totožný
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
předchůdci	předchůdce	k1gMnPc7	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
se	se	k3xPyFc4	se
dotkly	dotknout	k5eAaPmAgFnP	dotknout
vybavení	vybavení	k1gNnSc4	vybavení
a	a	k8xC	a
avioniky	avionika	k1gFnSc2	avionika
(	(	kIx(	(
<g/>
nový	nový	k2eAgMnSc1d1	nový
autopilot	autopilot	k1gMnSc1	autopilot
a	a	k8xC	a
palebný	palebný	k2eAgInSc1d1	palebný
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
křídla	křídlo	k1gNnPc4	křídlo
byly	být	k5eAaImAgInP	být
instalovány	instalovat	k5eAaBmNgInP	instalovat
4	[number]	k4	4
závěsníky	závěsník	k1gInPc1	závěsník
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběl	vyrábět	k5eAaImAgInS	vyrábět
se	se	k3xPyFc4	se
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
nahradil	nahradit	k5eAaPmAgMnS	nahradit
předešlou	předešlý	k2eAgFnSc4d1	předešlá
dvoumístnou	dvoumístný	k2eAgFnSc4d1	dvoumístná
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Modernizační	modernizační	k2eAgInPc1d1	modernizační
programy	program	k1gInPc1	program
===	===	k?	===
</s>
</p>
<p>
<s>
MiGy-	MiGy-	k?	MiGy-
<g/>
21	[number]	k4	21
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
stáří	stáří	k1gNnSc4	stáří
stále	stále	k6eAd1	stále
všestrannými	všestranný	k2eAgNnPc7d1	všestranné
letadly	letadlo	k1gNnPc7	letadlo
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc1	omezení
jejich	jejich	k3xOp3gNnSc2	jejich
použití	použití	k1gNnSc2	použití
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
leteckém	letecký	k2eAgInSc6d1	letecký
boji	boj	k1gInSc6	boj
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
zejména	zejména	k9	zejména
zastaráním	zastarání	k1gNnSc7	zastarání
jejich	jejich	k3xOp3gFnSc2	jejich
palubní	palubní	k2eAgFnSc2d1	palubní
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
okolo	okolo	k7c2	okolo
3	[number]	k4	3
300	[number]	k4	300
kusů	kus	k1gInPc2	kus
těchto	tento	k3xDgNnPc2	tento
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
existují	existovat	k5eAaImIp3nP	existovat
asi	asi	k9	asi
ve	v	k7c6	v
30	[number]	k4	30
variantách	varianta	k1gFnPc6	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
vlastní	vlastní	k2eAgMnSc1d1	vlastní
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
modernizaci	modernizace	k1gFnSc4	modernizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
93	[number]	k4	93
<g/>
:	:	kIx,	:
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
instalaci	instalace	k1gFnSc6	instalace
stejného	stejný	k2eAgInSc2d1	stejný
dopplerovského	dopplerovský	k2eAgInSc2d1	dopplerovský
radaru	radar	k1gInSc2	radar
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
novému	nový	k2eAgInSc3d1	nový
radaru	radar	k1gInSc3	radar
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
avioniky	avionika	k1gFnSc2	avionika
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
neseny	nesen	k2eAgFnPc1d1	nesena
moderní	moderní	k2eAgFnPc1d1	moderní
zbraně	zbraň	k1gFnPc1	zbraň
a	a	k8xC	a
rakety	raketa	k1gFnPc1	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Novinkou	novinka	k1gFnSc7	novinka
je	být	k5eAaImIp3nS	být
i	i	k9	i
instalace	instalace	k1gFnSc1	instalace
inovovaného	inovovaný	k2eAgNnSc2d1	inovované
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
head-up	headp	k1gMnSc1	head-up
displeje	displej	k1gFnSc2	displej
a	a	k8xC	a
přilbového	přilbový	k2eAgInSc2d1	přilbový
zaměřovače	zaměřovač	k1gInSc2	zaměřovač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
<g/>
:	:	kIx,	:
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgFnPc4d1	stejná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xS	jako
předešlá	předešlý	k2eAgFnSc1d1	předešlá
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
má	mít	k5eAaImIp3nS	mít
instalovaný	instalovaný	k2eAgInSc1d1	instalovaný
motor	motor	k1gInSc1	motor
Klimov	Klimov	k1gInSc4	Klimov
RD-33	RD-33	k1gFnSc2	RD-33
se	s	k7c7	s
zlepšenými	zlepšený	k2eAgFnPc7d1	zlepšená
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
LanceR	LanceR	k1gFnPc2	LanceR
<g/>
:	:	kIx,	:
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
rumunskou	rumunský	k2eAgFnSc7d1	rumunská
společností	společnost	k1gFnSc7	společnost
Aerostar	Aerostara	k1gFnPc2	Aerostara
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
firmou	firma	k1gFnSc7	firma
Elbit	Elbit	k2eAgInSc1d1	Elbit
Systems	Systems	k1gInSc1	Systems
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
pozemní	pozemní	k2eAgInPc4d1	pozemní
cíle	cíl	k1gInPc4	cíl
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
naváděných	naváděný	k2eAgFnPc2d1	naváděná
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
řízené	řízený	k2eAgFnSc2d1	řízená
munice	munice	k1gFnSc2	munice
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
dvou	dva	k4xCgFnPc6	dva
verzích	verze	k1gFnPc6	verze
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
k	k	k7c3	k
vybojování	vybojování	k1gNnSc3	vybojování
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
nadvlády	nadvláda	k1gFnSc2	nadvláda
(	(	kIx(	(
<g/>
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
LanceR	LanceR	k1gMnSc2	LanceR
C	C	kA	C
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
cvičně-bojové	cvičněojový	k2eAgFnSc6d1	cvičně-bojová
verzi	verze	k1gFnSc6	verze
(	(	kIx(	(
<g/>
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
LanceR	LanceR	k1gMnSc2	LanceR
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
změnou	změna	k1gFnSc7	změna
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc1d1	nový
radar	radar	k1gInSc1	radar
Elta	Elt	k1gInSc2	Elt
EL	Ela	k1gFnPc2	Ela
<g/>
/	/	kIx~	/
<g/>
M-	M-	k1gFnPc2	M-
<g/>
2032	[number]	k4	2032
<g/>
,	,	kIx,	,
přilbový	přilbový	k2eAgInSc1d1	přilbový
zaměřovač	zaměřovač	k1gInSc1	zaměřovač
Elbit	Elbita	k1gFnPc2	Elbita
DASH	DASH	kA	DASH
<g/>
,	,	kIx,	,
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
nesení	nesení	k1gNnSc2	nesení
střel	střela	k1gFnPc2	střela
Rafael	Rafael	k1gMnSc1	Rafael
Python	Python	k1gMnSc1	Python
III	III	kA	III
a	a	k8xC	a
Mica	Mica	k1gFnSc1	Mica
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
společností	společnost	k1gFnSc7	společnost
IAI	IAI	kA	IAI
(	(	kIx(	(
<g/>
Israel	Israel	k1gMnSc1	Israel
Aerospace	Aerospace	k1gFnSc2	Aerospace
Industries	Industries	k1gMnSc1	Industries
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
skleněného	skleněný	k2eAgInSc2d1	skleněný
kokpitu	kokpit	k1gInSc2	kokpit
<g/>
,	,	kIx,	,
nové	nový	k2eAgFnSc2d1	nová
avioniky	avionika	k1gFnSc2	avionika
a	a	k8xC	a
výzbroje	výzbroj	k1gFnSc2	výzbroj
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgMnPc3	který
nezaostává	zaostávat	k5eNaImIp3nS	zaostávat
za	za	k7c7	za
západními	západní	k2eAgInPc7d1	západní
typy	typ	k1gInPc7	typ
stíhaček	stíhačka	k1gFnPc2	stíhačka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
Bison	Bison	k1gInSc1	Bison
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
verzí	verze	k1gFnSc7	verze
modernizace	modernizace	k1gFnSc2	modernizace
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
radarem	radar	k1gInSc7	radar
"	"	kIx"	"
<g/>
Kopyo	Kopyo	k6eAd1	Kopyo
<g/>
"	"	kIx"	"
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Fazotron	Fazotron	k1gInSc1	Fazotron
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
sledovat	sledovat	k5eAaImF	sledovat
8	[number]	k4	8
cílů	cíl	k1gInPc2	cíl
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
výrobce	výrobce	k1gMnSc1	výrobce
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
-	-	kIx~	-
společnosti	společnost	k1gFnSc2	společnost
"	"	kIx"	"
<g/>
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
"	"	kIx"	"
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
Bison	Bison	k1gInSc1	Bison
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
prvními	první	k4xOgInPc7	první
modely	model	k1gInPc7	model
amerických	americký	k2eAgFnPc2d1	americká
stíhaček	stíhačka	k1gFnPc2	stíhačka
F-	F-	k1gFnSc4	F-
<g/>
16	[number]	k4	16
<g/>
A.	A.	kA	A.
</s>
</p>
<p>
<s>
==	==	k?	==
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
==	==	k?	==
</s>
</p>
<p>
<s>
Stíhačka	stíhačka	k1gFnSc1	stíhačka
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
se	se	k3xPyFc4	se
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
československého	československý	k2eAgNnSc2d1	Československé
letectva	letectvo	k1gNnSc2	letectvo
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
plánu	plán	k1gInSc2	plán
vývoje	vývoj	k1gInSc2	vývoj
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
vybrán	vybrat	k5eAaPmNgMnS	vybrat
jako	jako	k9	jako
protiváha	protiváha	k1gFnSc1	protiváha
stíhačky	stíhačka	k1gFnSc2	stíhačka
F-104	F-104	k1gMnSc1	F-104
Starfighter	Starfighter	k1gMnSc1	Starfighter
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
německá	německý	k2eAgFnSc1d1	německá
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
začátkem	začátek	k1gInSc7	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
plánovala	plánovat	k5eAaImAgFnS	plánovat
zavést	zavést	k5eAaPmF	zavést
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oboustranné	oboustranný	k2eAgFnSc6d1	oboustranná
dohodě	dohoda	k1gFnSc6	dohoda
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
začala	začít	k5eAaPmAgFnS	začít
sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
verze	verze	k1gFnSc1	verze
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
Aero	aero	k1gNnSc4	aero
Vodochody	Vodochod	k1gInPc5	Vodochod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dohody	dohoda	k1gFnSc2	dohoda
byly	být	k5eAaImAgInP	být
ze	z	k7c2	z
SSSR	SSSR	kA	SSSR
dodány	dodán	k2eAgInPc1d1	dodán
4	[number]	k4	4
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
v	v	k7c6	v
rozebraném	rozebraný	k2eAgInSc6d1	rozebraný
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
vzorový	vzorový	k2eAgInSc4d1	vzorový
kus	kus	k1gInSc4	kus
v	v	k7c6	v
celku	celek	k1gInSc6	celek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
měly	mít	k5eAaImAgInP	mít
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
potřebných	potřebný	k2eAgFnPc2d1	potřebná
zkušeností	zkušenost	k1gFnPc2	zkušenost
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
výrobními	výrobní	k2eAgInPc7d1	výrobní
postupy	postup	k1gInPc7	postup
a	a	k8xC	a
údržbou	údržba	k1gFnSc7	údržba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
československý	československý	k2eAgInSc1d1	československý
sériový	sériový	k2eAgInSc1d1	sériový
kus	kus	k1gInSc1	kus
měl	mít	k5eAaImAgInS	mít
výrobní	výrobní	k2eAgNnSc4d1	výrobní
číslo	číslo	k1gNnSc4	číslo
360101	[number]	k4	360101
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Letadlo	letadlo	k1gNnSc1	letadlo
bylo	být	k5eAaImAgNnS	být
přiděleno	přidělit	k5eAaPmNgNnS	přidělit
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
Mladá	mladá	k1gFnSc1	mladá
u	u	k7c2	u
Milovic	Milovice	k1gFnPc2	Milovice
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Vodochodech	Vodochod	k1gInPc6	Vodochod
ukončena	ukončen	k2eAgFnSc1d1	ukončena
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zalétán	zalétán	k2eAgInSc1d1	zalétán
poslední	poslední	k2eAgInSc1d1	poslední
stroj	stroj	k1gInSc1	stroj
s	s	k7c7	s
výrobním	výrobní	k2eAgNnSc7d1	výrobní
číslem	číslo	k1gNnSc7	číslo
261114	[number]	k4	261114
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
sériové	sériový	k2eAgFnSc2d1	sériová
výroby	výroba	k1gFnSc2	výroba
podnik	podnik	k1gInSc1	podnik
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
195	[number]	k4	195
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nasazení	nasazení	k1gNnSc1	nasazení
==	==	k?	==
</s>
</p>
<p>
<s>
Výrobcem	výrobce	k1gMnSc7	výrobce
a	a	k8xC	a
hlavním	hlavní	k2eAgMnSc7d1	hlavní
uživatelem	uživatel	k1gMnSc7	uživatel
MiGů-	MiGů-	k1gFnSc2	MiGů-
<g/>
21	[number]	k4	21
byl	být	k5eAaImAgInS	být
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
nástupnické	nástupnický	k2eAgInPc1d1	nástupnický
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
SSSR	SSSR	kA	SSSR
nasadil	nasadit	k5eAaPmAgMnS	nasadit
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
21	[number]	k4	21
do	do	k7c2	do
boje	boj	k1gInSc2	boj
jen	jen	k9	jen
během	během	k7c2	během
intervence	intervence	k1gFnSc2	intervence
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
===	===	k?	===
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
byly	být	k5eAaImAgFnP	být
nové	nový	k2eAgFnPc4d1	nová
MiGy-	MiGy-	k1gFnPc4	MiGy-
<g/>
21	[number]	k4	21
vyvezeny	vyvezen	k2eAgInPc1d1	vyvezen
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
Severní	severní	k2eAgInSc1d1	severní
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
SSSR	SSSR	kA	SSSR
první	první	k4xOgFnSc1	první
možnost	možnost	k1gFnSc1	možnost
skutečného	skutečný	k2eAgNnSc2d1	skutečné
otestování	otestování	k1gNnSc2	otestování
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
dosud	dosud	k6eAd1	dosud
zamlžovaný	zamlžovaný	k2eAgInSc1d1	zamlžovaný
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
utajení	utajení	k1gNnSc2	utajení
létali	létat	k5eAaImAgMnP	létat
sovětští	sovětský	k2eAgMnPc1d1	sovětský
piloti	pilot	k1gMnPc1	pilot
na	na	k7c6	na
strojích	stroj	k1gInPc6	stroj
s	s	k7c7	s
vietnamskými	vietnamský	k2eAgInPc7d1	vietnamský
výsostnými	výsostný	k2eAgInPc7d1	výsostný
znaky	znak	k1gInPc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
vietnamským	vietnamský	k2eAgNnSc7d1	vietnamské
územím	území	k1gNnSc7	území
byla	být	k5eAaImAgFnS	být
vedená	vedený	k2eAgFnSc1d1	vedená
nejintenzivnější	intenzivní	k2eAgFnSc1d3	nejintenzivnější
letecká	letecký	k2eAgFnSc1d1	letecká
válka	válka	k1gFnSc1	válka
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
<g/>
.	.	kIx.	.
</s>
<s>
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
pilotované	pilotovaný	k2eAgMnPc4d1	pilotovaný
MiGy	mig	k1gInPc4	mig
tu	tu	k6eAd1	tu
sice	sice	k8xC	sice
proti	proti	k7c3	proti
americké	americký	k2eAgFnSc3d1	americká
přesile	přesila	k1gFnSc3	přesila
a	a	k8xC	a
technické	technický	k2eAgFnSc3d1	technická
nadřazenosti	nadřazenost	k1gFnSc3	nadřazenost
měly	mít	k5eAaImAgFnP	mít
jen	jen	k9	jen
omezené	omezený	k2eAgFnPc1d1	omezená
možnosti	možnost	k1gFnPc1	možnost
nasazení	nasazení	k1gNnSc2	nasazení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
i	i	k9	i
úspěchy	úspěch	k1gInPc1	úspěch
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
proti	proti	k7c3	proti
svazům	svaz	k1gInPc3	svaz
amerických	americký	k2eAgInPc2d1	americký
těžkých	těžký	k2eAgInPc2d1	těžký
bombardérů	bombardér	k1gInPc2	bombardér
B-	B-	k1gFnSc2	B-
<g/>
52	[number]	k4	52
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
proti	proti	k7c3	proti
stíhacím	stíhací	k2eAgMnPc3d1	stíhací
bombardérům	bombardér	k1gMnPc3	bombardér
F-105	F-105	k1gFnSc2	F-105
Thunderchief	Thunderchief	k1gInSc1	Thunderchief
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
Američané	Američan	k1gMnPc1	Američan
často	často	k6eAd1	často
nechávali	nechávat	k5eAaImAgMnP	nechávat
letět	letět	k5eAaImF	letět
bez	bez	k7c2	bez
krytí	krytí	k1gNnSc2	krytí
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
plně	plně	k6eAd1	plně
zatížené	zatížený	k2eAgFnPc1d1	zatížená
municí	munice	k1gFnSc7	munice
<g/>
.	.	kIx.	.
</s>
<s>
Vietnamský	vietnamský	k2eAgMnSc1d1	vietnamský
letec	letec	k1gMnSc1	letec
Nguyen	Nguyen	k2eAgInSc4d1	Nguyen
Van	van	k1gInSc4	van
Coc	Coc	k1gMnSc1	Coc
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
na	na	k7c6	na
MiGu-	MiGu-	k1gFnSc6	MiGu-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
9	[number]	k4	9
sestřelů	sestřel	k1gInPc2	sestřel
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
výhody	výhoda	k1gFnPc1	výhoda
i	i	k8xC	i
slabosti	slabost	k1gFnPc1	slabost
MiGů	mig	k1gInPc2	mig
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zlepšování	zlepšování	k1gNnSc2	zlepšování
avioniky	avionika	k1gFnSc2	avionika
či	či	k8xC	či
doletu	dolet	k1gInSc2	dolet
se	se	k3xPyFc4	se
Sověti	Sovět	k1gMnPc1	Sovět
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
opět	opět	k6eAd1	opět
na	na	k7c4	na
letadlo	letadlo	k1gNnSc4	letadlo
namontovat	namontovat	k5eAaPmF	namontovat
organickou	organický	k2eAgFnSc4d1	organická
výzbroj	výzbroj	k1gFnSc4	výzbroj
-	-	kIx~	-
nejprve	nejprve	k6eAd1	nejprve
30	[number]	k4	30
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
23	[number]	k4	23
mm	mm	kA	mm
dvouhlavňový	dvouhlavňový	k2eAgInSc4d1	dvouhlavňový
kanón	kanón	k1gInSc4	kanón
GŠ-	GŠ-	k1gMnPc2	GŠ-
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
střetu	střet	k1gInSc2	střet
s	s	k7c7	s
vietnamským	vietnamský	k2eAgNnSc7d1	vietnamské
letectvem	letectvo	k1gNnSc7	letectvo
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
Sověti	Sovět	k1gMnPc1	Sovět
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
<g/>
,	,	kIx,	,
zřídili	zřídit	k5eAaPmAgMnP	zřídit
leteckou	letecký	k2eAgFnSc4d1	letecká
školu	škola	k1gFnSc4	škola
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
boje	boj	k1gInSc2	boj
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Top	topit	k5eAaImRp2nS	topit
Gun	Gun	k1gFnSc2	Gun
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Šestidenní	šestidenní	k2eAgFnSc1d1	šestidenní
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1967	[number]	k4	1967
měl	mít	k5eAaImAgInS	mít
Egypt	Egypt	k1gInSc1	Egypt
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
stovku	stovka	k1gFnSc4	stovka
stíhaček	stíhačka	k1gFnPc2	stíhačka
MiG-	MiG-	k1gMnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
egyptských	egyptský	k2eAgFnPc2d1	egyptská
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdušných	vzdušný	k2eAgInPc6d1	vzdušný
soubojích	souboj	k1gInPc6	souboj
izraelští	izraelský	k2eAgMnPc1d1	izraelský
piloti	pilot	k1gMnPc1	pilot
sestřelili	sestřelit	k5eAaPmAgMnP	sestřelit
11	[number]	k4	11
ks	ks	kA	ks
egyptských	egyptský	k2eAgMnPc2d1	egyptský
<g/>
,	,	kIx,	,
10	[number]	k4	10
ks	ks	kA	ks
syrských	syrský	k2eAgInPc2d1	syrský
a	a	k8xC	a
1	[number]	k4	1
iráckou	irácký	k2eAgFnSc4d1	irácká
stíhačku	stíhačka	k1gFnSc4	stíhačka
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Izraelcům	Izraelec	k1gMnPc3	Izraelec
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
významně	významně	k6eAd1	významně
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
irácký	irácký	k2eAgMnSc1d1	irácký
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
přeletěl	přeletět	k5eAaPmAgMnS	přeletět
pilot	pilot	k1gMnSc1	pilot
Munír	Munír	k1gMnSc1	Munír
Rúfá	Rúfá	k1gFnSc1	Rúfá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opotřebovací	opotřebovací	k2eAgFnSc1d1	opotřebovací
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1970	[number]	k4	1970
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
nilskou	nilský	k2eAgFnSc7d1	nilská
deltou	delta	k1gFnSc7	delta
rozpoutal	rozpoutat	k5eAaPmAgInS	rozpoutat
letecký	letecký	k2eAgInSc1d1	letecký
souboj	souboj	k1gInSc1	souboj
egyptských	egyptský	k2eAgFnPc2d1	egyptská
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
proti	proti	k7c3	proti
izraelským	izraelský	k2eAgMnPc3d1	izraelský
F-4	F-4	k1gMnPc3	F-4
Phantom	Phantom	k1gInSc4	Phantom
II	II	kA	II
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
byly	být	k5eAaImAgFnP	být
sestřeleny	sestřelen	k2eAgFnPc1d1	sestřelena
2	[number]	k4	2
stíhačky	stíhačka	k1gFnPc4	stíhačka
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
F-4	F-4	k1gMnSc1	F-4
Phantom	Phantom	k1gInSc4	Phantom
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
den	den	k1gInSc1	den
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
sestřelil	sestřelit	k5eAaPmAgInS	sestřelit
stíhačku	stíhačka	k1gFnSc4	stíhačka
Mirage	Mirag	k1gInSc2	Mirag
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
i	i	k9	i
během	během	k7c2	během
dalších	další	k2eAgInPc2d1	další
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenán	k2eAgFnPc1d1	zaznamenána
ztráty	ztráta	k1gFnPc1	ztráta
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jomkipurská	Jomkipurský	k2eAgFnSc1d1	Jomkipurská
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1973	[number]	k4	1973
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
jomkipurská	jomkipurský	k2eAgFnSc1d1	jomkipurská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
21	[number]	k4	21
vyslány	vyslán	k2eAgInPc1d1	vyslán
jako	jako	k8xC	jako
ochrana	ochrana	k1gFnSc1	ochrana
stíhacích	stíhací	k2eAgInPc2d1	stíhací
bombardérů	bombardér	k1gInPc2	bombardér
před	před	k7c7	před
izraelskými	izraelský	k2eAgInPc7d1	izraelský
útoky	útok	k1gInPc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úvodních	úvodní	k2eAgInPc6d1	úvodní
arabských	arabský	k2eAgInPc6d1	arabský
útocích	útok	k1gInPc6	útok
bylo	být	k5eAaImAgNnS	být
MiGům-	MiGům-	k1gFnSc7	MiGům-
<g/>
21	[number]	k4	21
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
chránit	chránit	k5eAaImF	chránit
egyptská	egyptský	k2eAgNnPc4d1	egyptské
letiště	letiště	k1gNnPc4	letiště
v	v	k7c6	v
očekávání	očekávání	k1gNnSc6	očekávání
protiútoku	protiútok	k1gInSc2	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc6	konec
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
přišel	přijít	k5eAaPmAgInS	přijít
Egypt	Egypt	k1gInSc1	Egypt
o	o	k7c4	o
5	[number]	k4	5
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
zachytila	zachytit	k5eAaPmAgFnS	zachytit
formace	formace	k1gFnSc1	formace
60	[number]	k4	60
stíhaček	stíhačka	k1gFnPc2	stíhačka
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
formaci	formace	k1gFnSc4	formace
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
letadel	letadlo	k1gNnPc2	letadlo
nad	nad	k7c7	nad
nilskou	nilský	k2eAgFnSc7d1	nilská
deltou	delta	k1gFnSc7	delta
a	a	k8xC	a
5	[number]	k4	5
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
sestřelili	sestřelit	k5eAaPmAgMnP	sestřelit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
7	[number]	k4	7
MiGů	mig	k1gInPc2	mig
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1973	[number]	k4	1973
podepsána	podepsán	k2eAgFnSc1d1	podepsána
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
ztratil	ztratit	k5eAaPmAgInS	ztratit
Egypt	Egypt	k1gInSc1	Egypt
73	[number]	k4	73
stíhaček	stíhačka	k1gFnPc2	stíhačka
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
a	a	k8xC	a
zničil	zničit	k5eAaPmAgInS	zničit
27	[number]	k4	27
izraelských	izraelský	k2eAgFnPc2d1	izraelská
stíhaček	stíhačka	k1gFnPc2	stíhačka
ve	v	k7c6	v
vzdušných	vzdušný	k2eAgInPc6d1	vzdušný
soubojích	souboj	k1gInPc6	souboj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgInPc1d1	jiný
konflikty	konflikt	k1gInPc1	konflikt
===	===	k?	===
</s>
</p>
<p>
<s>
Použití	použití	k1gNnSc1	použití
MiGů-	MiGů-	k1gFnSc2	MiGů-
<g/>
21	[number]	k4	21
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
konfliktech	konflikt	k1gInPc6	konflikt
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
nebylo	být	k5eNaImAgNnS	být
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
může	moct	k5eAaImIp3nS	moct
hlavně	hlavně	k6eAd1	hlavně
nedostatečný	dostatečný	k2eNgInSc1d1	nedostatečný
výcvik	výcvik	k1gInSc1	výcvik
jejich	jejich	k3xOp3gMnPc2	jejich
pilotů	pilot	k1gMnPc2	pilot
<g/>
.	.	kIx.	.
</s>
<s>
MiGy-	MiGy-	k?	MiGy-
<g/>
21	[number]	k4	21
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgInP	použít
jednak	jednak	k8xC	jednak
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
čelit	čelit	k5eAaImF	čelit
vynikajícím	vynikající	k2eAgMnPc3d1	vynikající
izraelským	izraelský	k2eAgMnPc3d1	izraelský
letcům	letec	k1gMnPc3	letec
a	a	k8xC	a
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
ani	ani	k8xC	ani
nové	nový	k2eAgFnSc6d1	nová
technice	technika	k1gFnSc6	technika
-	-	kIx~	-
strojům	stroj	k1gInPc3	stroj
F-15	F-15	k1gFnPc2	F-15
a	a	k8xC	a
F-	F-	k1gFnSc7	F-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Trochu	trochu	k6eAd1	trochu
úspěšnější	úspěšný	k2eAgNnSc1d2	úspěšnější
bylo	být	k5eAaImAgNnS	být
použití	použití	k1gNnSc1	použití
egyptských	egyptský	k2eAgFnPc2d1	egyptská
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
proti	proti	k7c3	proti
libyjským	libyjský	k2eAgMnPc3d1	libyjský
MiGům-	MiGům-	k1gMnPc3	MiGům-
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
indicko-pákistánské	indickoákistánský	k2eAgFnSc6d1	indicko-pákistánská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
znamenala	znamenat	k5eAaImAgFnS	znamenat
účast	účast	k1gFnSc1	účast
indických	indický	k2eAgInPc2d1	indický
MiGů	mig	k1gInPc2	mig
porážku	porážka	k1gFnSc4	porážka
pákistánského	pákistánský	k2eAgNnSc2d1	pákistánské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc1	stroj
se	se	k3xPyFc4	se
uplatnily	uplatnit	k5eAaPmAgInP	uplatnit
i	i	k9	i
při	při	k7c6	při
pozdějších	pozdní	k2eAgInPc6d2	pozdější
konfliktech	konflikt	k1gInPc6	konflikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
byly	být	k5eAaImAgInP	být
místní	místní	k2eAgInPc1d1	místní
MiGy	mig	k1gInPc1	mig
použity	použit	k2eAgInPc1d1	použit
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
pozemní	pozemní	k2eAgInPc4d1	pozemní
cíle	cíl	k1gInPc4	cíl
<g/>
;	;	kIx,	;
s	s	k7c7	s
nepřítelem	nepřítel	k1gMnSc7	nepřítel
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
střetly	střetnout	k5eAaPmAgFnP	střetnout
jen	jen	k9	jen
během	během	k7c2	během
intervence	intervence	k1gFnSc2	intervence
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
24	[number]	k4	24
chorvatských	chorvatský	k2eAgInPc2d1	chorvatský
a	a	k8xC	a
bosenských	bosenský	k2eAgInPc2d1	bosenský
strojů	stroj	k1gInPc2	stroj
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
21	[number]	k4	21
vyváženy	vyvážen	k2eAgInPc1d1	vyvážen
i	i	k8xC	i
do	do	k7c2	do
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
se	se	k3xPyFc4	se
angolské	angolský	k2eAgFnSc2d1	angolská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
angolského	angolský	k2eAgNnSc2d1	angolské
i	i	k8xC	i
kubánského	kubánský	k2eAgNnSc2d1	kubánské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejkurióznější	kuriózní	k2eAgNnSc4d3	nejkurióznější
nasazení	nasazení	k1gNnSc4	nasazení
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
americké	americký	k2eAgInPc1d1	americký
úřady	úřad	k1gInPc1	úřad
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
objevily	objevit	k5eAaPmAgInP	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
zločinecké	zločinecký	k2eAgFnPc1d1	zločinecká
organizace	organizace	k1gFnPc1	organizace
pašující	pašující	k2eAgFnPc1d1	pašující
narkotika	narkotikon	k1gNnPc4	narkotikon
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
použily	použít	k5eAaPmAgFnP	použít
upravené	upravený	k2eAgFnPc1d1	upravená
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
na	na	k7c4	na
jednorázový	jednorázový	k2eAgInSc4d1	jednorázový
převoz	převoz	k1gInSc4	převoz
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
drog	droga	k1gFnPc2	droga
přes	přes	k7c4	přes
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Použité	použitý	k2eAgInPc1d1	použitý
MiGy	mig	k1gInPc1	mig
byly	být	k5eAaImAgInP	být
zakoupeny	zakoupit	k5eAaPmNgInP	zakoupit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vymontovány	vymontovat	k5eAaPmNgFnP	vymontovat
všechny	všechen	k3xTgFnPc1	všechen
zbraně	zbraň	k1gFnPc1	zbraň
a	a	k8xC	a
zbytečná	zbytečný	k2eAgFnSc1d1	zbytečná
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
,	,	kIx,	,
letadla	letadlo	k1gNnSc2	letadlo
následně	následně	k6eAd1	následně
naplnila	naplnit	k5eAaPmAgFnS	naplnit
drogami	droga	k1gFnPc7	droga
a	a	k8xC	a
letem	let	k1gInSc7	let
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
rychlosti	rychlost	k1gFnSc6	rychlost
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
zpozorování	zpozorování	k1gNnSc2	zpozorování
pohraniční	pohraniční	k2eAgFnSc7d1	pohraniční
stráží	stráž	k1gFnSc7	stráž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Verze	verze	k1gFnSc1	verze
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sloužící	sloužící	k2eAgFnSc7d1	sloužící
ČSLA	ČSLA	kA	ČSLA
a	a	k8xC	a
AČR	AČR	kA	AČR
===	===	k?	===
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
<g/>
:	:	kIx,	:
v	v	k7c6	v
ČSLA	ČSLA	kA	ČSLA
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
radiolokátorem	radiolokátor	k1gInSc7	radiolokátor
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
pár	pár	k4xCyI	pár
podvěsníků	podvěsník	k1gMnPc2	podvěsník
<g/>
,	,	kIx,	,
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
i	i	k9	i
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Z-159	Z-159	k1gFnSc2	Z-159
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
:	:	kIx,	:
modernizovaný	modernizovaný	k2eAgInSc1d1	modernizovaný
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
vybavený	vybavený	k2eAgInSc1d1	vybavený
radiolokátorem	radiolokátor	k1gInSc7	radiolokátor
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
hlavňové	hlavňový	k2eAgFnSc2d1	hlavňová
výzbroje	výzbroj	k1gFnSc2	výzbroj
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
protivzdušnou	protivzdušný	k2eAgFnSc4d1	protivzdušná
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
po	po	k7c6	po
odpálení	odpálení	k1gNnSc6	odpálení
raket	raketa	k1gFnPc2	raketa
ztratil	ztratit	k5eAaPmAgInS	ztratit
prakticky	prakticky	k6eAd1	prakticky
bojeschopnost	bojeschopnost	k1gFnSc4	bojeschopnost
(	(	kIx(	(
<g/>
měl	mít	k5eAaImAgMnS	mít
proto	proto	k8xC	proto
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Holubice	holubice	k1gFnSc1	holubice
míru	mír	k1gInSc2	mír
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
<g/>
:	:	kIx,	:
vylepšená	vylepšený	k2eAgFnSc1d1	vylepšená
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
významné	významný	k2eAgNnSc1d1	významné
snížení	snížení	k1gNnSc1	snížení
přistávací	přistávací	k2eAgFnSc2d1	přistávací
rychlosti	rychlost	k1gFnSc2	rychlost
z	z	k7c2	z
340	[number]	k4	340
na	na	k7c4	na
300	[number]	k4	300
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
<g/>
:	:	kIx,	:
průzkumná	průzkumný	k2eAgFnSc1d1	průzkumná
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc2d2	veliký
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
,	,	kIx,	,
2	[number]	k4	2
páry	pára	k1gFnSc2	pára
podvěsníků	podvěsník	k1gInPc2	podvěsník
pod	pod	k7c7	pod
křídly	křídlo	k1gNnPc7	křídlo
(	(	kIx(	(
<g/>
mohou	moct	k5eAaImIp3nP	moct
nést	nést	k5eAaImF	nést
kromě	kromě	k7c2	kromě
bojových	bojový	k2eAgFnPc2d1	bojová
i	i	k8xC	i
různé	různý	k2eAgInPc1d1	různý
průzkumné	průzkumný	k2eAgInPc1d1	průzkumný
prostředky	prostředek	k1gInPc1	prostředek
a	a	k8xC	a
přídavné	přídavný	k2eAgFnPc1d1	přídavná
nádrže	nádrž	k1gFnPc1	nádrž
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
<g/>
:	:	kIx,	:
víceúčelový	víceúčelový	k2eAgInSc1d1	víceúčelový
letoun	letoun	k1gInSc1	letoun
<g/>
,	,	kIx,	,
obdobný	obdobný	k2eAgInSc1d1	obdobný
verzi	verze	k1gFnSc4	verze
R	R	kA	R
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
:	:	kIx,	:
Nejmodernější	moderní	k2eAgFnSc1d3	nejmodernější
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgNnSc1d1	sloužící
v	v	k7c6	v
ČSLA	ČSLA	kA	ČSLA
a	a	k8xC	a
AČR	AČR	kA	AČR
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
upravená	upravený	k2eAgNnPc1d1	upravené
pro	pro	k7c4	pro
součinnost	součinnost	k1gFnSc4	součinnost
s	s	k7c7	s
vojsky	vojsko	k1gNnPc7	vojsko
NATO	NATO	kA	NATO
na	na	k7c4	na
verzi	verze	k1gFnSc4	verze
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k9	jako
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
MFN	MFN	kA	MFN
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
<g/>
:	:	kIx,	:
Výcviková	výcvikový	k2eAgFnSc1d1	výcviková
dvoumístná	dvoumístný	k2eAgFnSc1d1	dvoumístná
verze	verze	k1gFnSc1	verze
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
spárka	spárka	k1gFnSc1	spárka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obdobná	obdobný	k2eAgNnPc1d1	obdobné
typu	typ	k1gInSc2	typ
F	F	kA	F
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
US	US	kA	US
<g/>
:	:	kIx,	:
Dvoumístná	dvoumístný	k2eAgFnSc1d1	dvoumístná
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
obdobná	obdobný	k2eAgFnSc1d1	obdobná
typu	typ	k1gInSc2	typ
PFM	PFM	kA	PFM
<g/>
,	,	kIx,	,
vybavená	vybavený	k2eAgFnSc1d1	vybavená
periskopem	periskop	k1gInSc7	periskop
pro	pro	k7c4	pro
instruktora	instruktor	k1gMnSc4	instruktor
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	um	k1gInSc1	um
<g/>
:	:	kIx,	:
Dvoumístná	dvoumístný	k2eAgFnSc1d1	dvoumístná
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
obdobná	obdobný	k2eAgFnSc1d1	obdobná
typu	typ	k1gInSc2	typ
MF	MF	kA	MF
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc1d1	další
verze	verze	k1gFnPc1	verze
===	===	k?	===
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
existuje	existovat	k5eAaImIp3nS	existovat
vice	vika	k1gFnSc3	vika
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
desítky	desítka	k1gFnPc4	desítka
variant	varianta	k1gFnPc2	varianta
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
předurčené	předurčený	k2eAgInPc1d1	předurčený
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
zemi	zem	k1gFnSc4	zem
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
F	F	kA	F
-	-	kIx~	-
první	první	k4xOgFnSc1	první
sériová	sériový	k2eAgFnSc1d1	sériová
verze	verze	k1gFnSc1	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
-	-	kIx~	-
Verze	verze	k1gFnSc1	verze
schopna	schopen	k2eAgFnSc1d1	schopna
nést	nést	k5eAaImF	nést
teplem	teplo	k1gNnSc7	teplo
naváděné	naváděný	k2eAgFnSc2d1	naváděná
rakety	raketa	k1gFnSc2	raketa
K-13	K-13	k1gFnPc2	K-13
(	(	kIx(	(
<g/>
AA-	AA-	k1gFnPc2	AA-
<g/>
2	[number]	k4	2
Atoll	Atolla	k1gFnPc2	Atolla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
P	P	kA	P
-	-	kIx~	-
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
vylepšením	vylepšení	k1gNnSc7	vylepšení
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
zabudovaný	zabudovaný	k2eAgInSc1d1	zabudovaný
radar	radar	k1gInSc1	radar
TSD-30	TSD-30	k1gFnSc1	TSD-30
"	"	kIx"	"
<g/>
Spin	spin	k1gInSc1	spin
scan	scan	k1gInSc1	scan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
radar	radar	k1gInSc1	radar
RP-	RP-	k1gMnPc2	RP-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
FL	FL	kA	FL
-	-	kIx~	-
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
verze	verze	k1gFnSc2	verze
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
PFV	PFV	kA	PFV
-	-	kIx~	-
Verze	verze	k1gFnSc1	verze
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
FL	FL	kA	FL
<g/>
,	,	kIx,	,
uzpůsobena	uzpůsobit	k5eAaPmNgFnS	uzpůsobit
pro	pro	k7c4	pro
vlhké	vlhký	k2eAgNnSc4d1	vlhké
podnebí	podnebí	k1gNnSc4	podnebí
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
-	-	kIx~	-
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zlepšením	zlepšení	k1gNnSc7	zlepšení
předchozích	předchozí	k2eAgFnPc2d1	předchozí
verzí	verze	k1gFnPc2	verze
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nesené	nesený	k2eAgFnSc2d1	nesená
výzbroje	výzbroj	k1gFnSc2	výzbroj
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
měl	mít	k5eAaImAgMnS	mít
také	také	k6eAd1	také
dvoudílný	dvoudílný	k2eAgInSc4d1	dvoudílný
překryt	překryt	k2eAgInSc4d1	překryt
kokpitu	kokpit	k1gInSc3	kokpit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
-	-	kIx~	-
Cvičná	cvičný	k2eAgFnSc1d1	cvičná
verze	verze	k1gFnSc1	verze
odvozená	odvozený	k2eAgFnSc1d1	odvozená
od	od	k7c2	od
MiGu-	MiGu-	k1gMnPc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gMnPc2	F-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgMnS	vzlétnout
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
-	-	kIx~	-
Speciální	speciální	k2eAgFnSc1d1	speciální
verze	verze	k1gFnSc1	verze
určená	určený	k2eAgFnSc1d1	určená
na	na	k7c4	na
taktický	taktický	k2eAgInSc4d1	taktický
průzkum	průzkum	k1gInSc4	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Mohla	moct	k5eAaImAgFnS	moct
nést	nést	k5eAaImF	nést
výzbroj	výzbroj	k1gFnSc4	výzbroj
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
řízených	řízený	k2eAgFnPc2d1	řízená
raket	raketa	k1gFnPc2	raketa
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgFnSc4d1	vlastní
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
I	I	kA	I
-	-	kIx~	-
Experimentální	experimentální	k2eAgInSc1d1	experimentální
letoun	letoun	k1gInSc1	letoun
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
ověřoval	ověřovat	k5eAaImAgMnS	ověřovat
tvar	tvar	k1gInSc4	tvar
křídla	křídlo	k1gNnSc2	křídlo
pro	pro	k7c4	pro
Tu-	Tu-	k1gFnSc4	Tu-
<g/>
144	[number]	k4	144
<g/>
,	,	kIx,	,
přistávací	přistávací	k2eAgFnSc1d1	přistávací
rychlost	rychlost	k1gFnSc1	rychlost
cca	cca	kA	cca
220	[number]	k4	220
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
vyrobeny	vyroben	k2eAgFnPc1d1	vyrobena
2	[number]	k4	2
<g/>
ks	ks	kA	ks
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
-	-	kIx~	-
Nejmodernější	moderní	k2eAgFnSc1d3	nejmodernější
vyráběna	vyráběn	k2eAgFnSc1d1	vyráběna
verze	verze	k1gFnSc1	verze
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Uživatelé	uživatel	k1gMnPc1	uživatel
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Současní	současný	k2eAgMnPc1d1	současný
uživatelé	uživatel	k1gMnPc1	uživatel
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
seznam	seznam	k1gInSc1	seznam
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
provozovatele	provozovatel	k1gMnSc2	provozovatel
čínských	čínský	k2eAgFnPc2d1	čínská
kopií	kopie	k1gFnPc2	kopie
MiGu-	MiGu-	k1gFnSc4	MiGu-
<g/>
21	[number]	k4	21
-	-	kIx~	-
licenčně	licenčně	k6eAd1	licenčně
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
verze	verze	k1gFnPc1	verze
Chengdu	Chengd	k1gInSc2	Chengd
J-	J-	k1gFnSc2	J-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c4	na
Mig-	Mig-	k1gFnSc4	Mig-
<g/>
21	[number]	k4	21
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Angola	Angola	k1gFnSc1	Angola
AngolaAngola	AngolaAngola	k1gFnSc1	AngolaAngola
získala	získat	k5eAaPmAgFnS	získat
první	první	k4xOgFnSc1	první
MiGy-	MiGy-	k1gFnSc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
dodal	dodat	k5eAaPmAgMnS	dodat
SSSR	SSSR	kA	SSSR
8	[number]	k4	8
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
a	a	k8xC	a
2	[number]	k4	2
cvičné	cvičný	k2eAgFnSc6d1	cvičná
MiG-	MiG-	k1gFnSc6	MiG-
<g/>
21	[number]	k4	21
<g/>
US	US	kA	US
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
12	[number]	k4	12
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
US	US	kA	US
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
dodávka	dodávka	k1gFnSc1	dodávka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
15	[number]	k4	15
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
US	US	kA	US
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgFnSc1d1	následovaná
30	[number]	k4	30
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1985	[number]	k4	1985
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgInPc2	první
bojů	boj	k1gInPc2	boj
se	se	k3xPyFc4	se
angolské	angolský	k2eAgFnSc3d1	angolská
MiGy-	MiGy-	k1gFnSc3	MiGy-
<g/>
21	[number]	k4	21
zúčastnily	zúčastnit	k5eAaPmAgInP	zúčastnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
proti	proti	k7c3	proti
rebelům	rebel	k1gMnPc3	rebel
z	z	k7c2	z
oraganizace	oraganizace	k1gFnSc2	oraganizace
UNITA	unita	k1gMnSc1	unita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
jihoafrickými	jihoafrický	k2eAgFnPc7d1	Jihoafrická
stíhačkami	stíhačka	k1gFnPc7	stíhačka
Mirage	Mirag	k1gInSc2	Mirag
F1	F1	k1gFnPc2	F1
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
nejméně	málo	k6eAd3	málo
5	[number]	k4	5
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
Ázerbájdžán	Ázerbájdžán	k1gInSc4	Ázerbájdžán
ÁzerbájdžánÁzerbájdžánské	ÁzerbájdžánÁzerbájdžánský	k2eAgNnSc1d1	ÁzerbájdžánÁzerbájdžánský
letectvo	letectvo	k1gNnSc1	letectvo
provozuje	provozovat	k5eAaImIp3nS	provozovat
pět	pět	k4xCc4	pět
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
Egypt	Egypt	k1gInSc4	Egypt
EgyptEgyptské	EgyptEgyptský	k2eAgNnSc1d1	EgyptEgyptský
letectvo	letectvo	k1gNnSc1	letectvo
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
několik	několik	k4yIc1	několik
stíhaček	stíhačka	k1gFnPc2	stíhačka
MiG-	MiG-	k1gMnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gMnPc2	F-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
následovaných	následovaný	k2eAgFnPc2d1	následovaná
40	[number]	k4	40
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
a	a	k8xC	a
stejným	stejný	k2eAgNnSc7d1	stejné
množstvím	množství	k1gNnSc7	množství
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
disponoval	disponovat	k5eAaBmAgInS	disponovat
Egypt	Egypt	k1gInSc1	Egypt
230	[number]	k4	230
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
75	[number]	k4	75
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
MiG-	MiG-	k1gMnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
skončila	skončit	k5eAaPmAgFnS	skončit
Egyptu	Egypt	k1gInSc3	Egypt
vojenská	vojenský	k2eAgFnSc1d1	vojenská
pomoc	pomoc	k1gFnSc1	pomoc
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
dokoupeno	dokoupit	k5eAaPmNgNnS	dokoupit
80	[number]	k4	80
ks	ks	kA	ks
čínských	čínský	k2eAgFnPc2d1	čínská
kopií	kopie	k1gFnPc2	kopie
MiGu-	MiGu-	k1gFnSc4	MiGu-
<g/>
21	[number]	k4	21
-	-	kIx~	-
stíhaček	stíhačka	k1gFnPc2	stíhačka
F-	F-	k1gFnSc4	F-
<g/>
7	[number]	k4	7
<g/>
B.	B.	kA	B.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
měl	mít	k5eAaImAgInS	mít
Egypt	Egypt	k1gInSc1	Egypt
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
3	[number]	k4	3
eskadry	eskadra	k1gFnPc4	eskadra
MiGů-	MiGů-	k1gFnSc2	MiGů-
<g/>
21	[number]	k4	21
a	a	k8xC	a
2	[number]	k4	2
eskadry	eskadra	k1gFnPc4	eskadra
F-	F-	k1gFnSc2	F-
<g/>
7	[number]	k4	7
<g/>
B.	B.	kA	B.
<g/>
Guinea	guinea	k1gFnSc4	guinea
GuineaGuinejské	GuineaGuinejský	k2eAgFnSc2d1	GuineaGuinejský
<g />
.	.	kIx.	.
</s>
<s>
letectvo	letectvo	k1gNnSc1	letectvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
získalo	získat	k5eAaPmAgNnS	získat
8	[number]	k4	8
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
1	[number]	k4	1
dvoumístný	dvoumístný	k2eAgInSc4d1	dvoumístný
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
<g/>
U.	U.	kA	U.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
5	[number]	k4	5
strojů	stroj	k1gInPc2	stroj
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
uvedených	uvedený	k2eAgInPc2d1	uvedený
do	do	k7c2	do
letuschopného	letuschopný	k2eAgInSc2d1	letuschopný
stavu	stav	k1gInSc2	stav
ruskými	ruský	k2eAgMnPc7d1	ruský
techniky	technik	k1gMnPc7	technik
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
se	se	k3xPyFc4	se
guinejský	guinejský	k2eAgMnSc1d1	guinejský
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
zřítil	zřítit	k5eAaPmAgInS	zřítit
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
<g/>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
ChorvatskoChorvatské	chorvatskochorvatský	k2eAgNnSc1d1	chorvatskochorvatský
letectvo	letectvo	k1gNnSc1	letectvo
a	a	k8xC	a
protivzdušná	protivzdušný	k2eAgFnSc1d1	protivzdušná
obrana	obrana	k1gFnSc1	obrana
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
samostatnosti	samostatnost	k1gFnSc2	samostatnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
vlastnilo	vlastnit	k5eAaImAgNnS	vlastnit
několik	několik	k4yIc1	několik
jednomístných	jednomístný	k2eAgFnPc2d1	jednomístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
bylo	být	k5eAaImAgNnS	být
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
umožněno	umožněn	k2eAgNnSc1d1	umožněno
zakoupit	zakoupit	k5eAaPmF	zakoupit
přes	přes	k7c4	přes
40	[number]	k4	40
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
bylo	být	k5eAaImAgNnS	být
16	[number]	k4	16
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
4	[number]	k4	4
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
navráceny	navrátit	k5eAaPmNgFnP	navrátit
do	do	k7c2	do
letuschopného	letuschopný	k2eAgInSc2d1	letuschopný
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
strojů	stroj	k1gInPc2	stroj
byl	být	k5eAaImAgInS	být
uložen	uložit	k5eAaPmNgInS	uložit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
8	[number]	k4	8
ks	ks	kA	ks
Mig-	Mig-	k1gFnSc1	Mig-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
4	[number]	k4	4
ks	ks	kA	ks
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
modernizováno	modernizovat	k5eAaBmNgNnS	modernizovat
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
zakoupeny	zakoupen	k2eAgFnPc1d1	zakoupena
další	další	k2eAgFnPc1d1	další
4	[number]	k4	4
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
mají	mít	k5eAaImIp3nP	mít
tato	tento	k3xDgNnPc1	tento
letadla	letadlo	k1gNnPc1	letadlo
základny	základna	k1gFnSc2	základna
na	na	k7c6	na
letištích	letiště	k1gNnPc6	letiště
Pula	Pulum	k1gNnSc2	Pulum
a	a	k8xC	a
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
.	.	kIx.	.
<g/>
Indie	Indie	k1gFnSc1	Indie
Indie	Indie	k1gFnSc2	Indie
</s>
</p>
<p>
<s>
Indické	indický	k2eAgNnSc1d1	indické
letectvo	letectvo	k1gNnSc1	letectvo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
největším	veliký	k2eAgMnSc7d3	veliký
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
uživatelem	uživatel	k1gMnSc7	uživatel
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
všech	všecek	k3xTgFnPc2	všecek
verzí	verze	k1gFnPc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
10	[number]	k4	10
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnPc2	F-
<g/>
13	[number]	k4	13
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
následovaných	následovaný	k2eAgFnPc2d1	následovaná
dalšími	další	k2eAgFnPc7d1	další
4	[number]	k4	4
ks	ks	kA	ks
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
se	se	k3xPyFc4	se
první	první	k4xOgFnPc1	první
indo-pákistánské	indoákistánský	k2eAgFnPc1d1	indo-pákistánský
války	válka	k1gFnPc1	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
pilotní	pilotní	k2eAgInSc1d1	pilotní
výcvik	výcvik	k1gInSc1	výcvik
nebyl	být	k5eNaImAgInS	být
dostatečný	dostatečný	k2eAgMnSc1d1	dostatečný
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
piloti	pilot	k1gMnPc1	pilot
na	na	k7c6	na
MiGu-	MiGu-	k1gFnSc6	MiGu-
<g/>
21	[number]	k4	21
mohl	moct	k5eAaImAgMnS	moct
provádět	provádět	k5eAaImF	provádět
bojové	bojový	k2eAgFnSc2d1	bojová
mise	mise	k1gFnSc2	mise
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
nehráli	hrát	k5eNaImAgMnP	hrát
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
konfliktu	konflikt	k1gInSc6	konflikt
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
byly	být	k5eAaImAgInP	být
dodány	dodán	k2eAgInPc1d1	dodán
2	[number]	k4	2
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1965	[number]	k4	1965
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
pákistánskými	pákistánský	k2eAgInPc7d1	pákistánský
F-86	F-86	k1gMnSc5	F-86
Sabre	Sabr	k1gMnSc5	Sabr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
40	[number]	k4	40
ks	ks	kA	ks
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
U.	U.	kA	U.
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
sériové	sériový	k2eAgFnSc6d1	sériová
výrobě	výroba	k1gFnSc6	výroba
druhé	druhý	k4xOgFnSc2	druhý
generace	generace	k1gFnSc2	generace
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
značné	značný	k2eAgNnSc1d1	značné
zpoždění	zpoždění	k1gNnSc1	zpoždění
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
modelu	model	k1gInSc2	model
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
FL	FL	kA	FL
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
dovezeno	dovézt	k5eAaPmNgNnS	dovézt
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
MiGy-	MiGy-	k1gFnSc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
FL	FL	kA	FL
domácí	domácí	k2eAgFnSc2d1	domácí
produkce	produkce	k1gFnSc2	produkce
byly	být	k5eAaImAgInP	být
dodávány	dodávat	k5eAaImNgInP	dodávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
205	[number]	k4	205
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
FL	FL	kA	FL
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1971	[number]	k4	1971
během	během	k7c2	během
třetí	třetí	k4xOgFnSc2	třetí
indo-pákistánské	indoákistánský	k2eAgFnSc2d1	indo-pákistánský
války	válka	k1gFnSc2	válka
už	už	k6eAd1	už
byli	být	k5eAaImAgMnP	být
indičtí	indický	k2eAgMnPc1d1	indický
piloti	pilot	k1gMnPc1	pilot
kvalitně	kvalitně	k6eAd1	kvalitně
vycvičeni	vycvičit	k5eAaPmNgMnP	vycvičit
a	a	k8xC	a
zapojili	zapojit	k5eAaPmAgMnP	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
proti	proti	k7c3	proti
pákistánským	pákistánský	k2eAgFnPc3d1	pákistánská
F-	F-	k1gFnPc3	F-
<g/>
104	[number]	k4	104
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zničili	zničit	k5eAaPmAgMnP	zničit
nejméně	málo	k6eAd3	málo
3	[number]	k4	3
tyto	tento	k3xDgInPc4	tento
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
výroba	výroba	k1gFnSc1	výroba
modelu	model	k1gInSc2	model
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
opozdila	opozdit	k5eAaPmAgFnS	opozdit
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
nutné	nutný	k2eAgNnSc1d1	nutné
dodávat	dodávat	k5eAaImF	dodávat
potřebný	potřebný	k2eAgInSc4d1	potřebný
počet	počet	k1gInSc4	počet
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
takto	takto	k6eAd1	takto
Indie	Indie	k1gFnSc1	Indie
obstarala	obstarat	k5eAaPmAgFnS	obstarat
několik	několik	k4yIc4	několik
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
byly	být	k5eAaImAgFnP	být
vyzbrojeny	vyzbrojit	k5eAaPmNgInP	vyzbrojit
2	[number]	k4	2
eskadry	eskadra	k1gFnPc4	eskadra
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
dodával	dodávat	k5eAaImAgMnS	dodávat
další	další	k2eAgFnSc4d1	další
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
už	už	k6eAd1	už
indický	indický	k2eAgInSc4d1	indický
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
SSSR	SSSR	kA	SSSR
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
dodáno	dodat	k5eAaPmNgNnS	dodat
několik	několik	k4yIc1	několik
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
domácí	domácí	k2eAgInSc1d1	domácí
průmysl	průmysl	k1gInSc1	průmysl
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
tyto	tento	k3xDgInPc4	tento
stroje	stroj	k1gInPc4	stroj
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1977	[number]	k4	1977
-	-	kIx~	-
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
zakoupeno	zakoupit	k5eAaPmNgNnS	zakoupit
70	[number]	k4	70
ks	ks	kA	ks
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dalších	další	k2eAgFnPc2d1	další
21	[number]	k4	21
ks	ks	kA	ks
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
různými	různý	k2eAgFnPc7d1	různá
zeměmi	zem	k1gFnPc7	zem
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
začal	začít	k5eAaPmAgInS	začít
program	program	k1gInSc1	program
modernizace	modernizace	k1gFnSc2	modernizace
více	hodně	k6eAd2	hodně
než	než	k8xS	než
stovky	stovka	k1gFnPc4	stovka
MiGů-	MiGů-	k1gFnSc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
ruskými	ruský	k2eAgFnPc7d1	ruská
a	a	k8xC	a
západními	západní	k2eAgFnPc7d1	západní
firmami	firma	k1gFnPc7	firma
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
zajistit	zajistit	k5eAaPmF	zajistit
jejich	jejich	k3xOp3gInSc4	jejich
provoz	provoz	k1gInSc4	provoz
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
prototyp	prototyp	k1gInSc1	prototyp
modernizovaného	modernizovaný	k2eAgInSc2d1	modernizovaný
MiGu-	MiGu-	k1gMnSc4	MiGu-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1998	[number]	k4	1998
a	a	k8xC	a
první	první	k4xOgInPc1	první
sériové	sériový	k2eAgInPc1d1	sériový
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
modernizoval	modernizovat	k5eAaBmAgInS	modernizovat
indický	indický	k2eAgInSc1d1	indický
letecký	letecký	k2eAgInSc1d1	letecký
průmysl	průmysl	k1gInSc1	průmysl
94	[number]	k4	94
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
označeny	označit	k5eAaPmNgInP	označit
zpočátku	zpočátku	k6eAd1	zpočátku
jako	jako	k8xS	jako
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
UPG	UPG	kA	UPG
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
MiG-	MiG-	k1gFnSc3	MiG-
<g/>
21	[number]	k4	21
Bison	Bisona	k1gFnPc2	Bisona
<g/>
.	.	kIx.	.
<g/>
Jemen	Jemen	k1gInSc4	Jemen
JemenJemenské	JemenJemenský	k2eAgNnSc1d1	JemenJemenský
letectvo	letectvo	k1gNnSc1	letectvo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
sjednocením	sjednocení	k1gNnSc7	sjednocení
Severního	severní	k2eAgInSc2d1	severní
a	a	k8xC	a
Jižního	jižní	k2eAgInSc2d1	jižní
Jemenu	Jemen	k1gInSc2	Jemen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
dostaly	dostat	k5eAaPmAgInP	dostat
i	i	k9	i
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
v	v	k7c6	v
letectvu	letectvo	k1gNnSc6	letectvo
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
sjednocení	sjednocení	k1gNnSc6	sjednocení
zavedeno	zavést	k5eAaPmNgNnS	zavést
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
nového	nový	k2eAgNnSc2d1	nové
letectva	letectvo	k1gNnSc2	letectvo
47	[number]	k4	47
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
8	[number]	k4	8
ks	ks	kA	ks
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byly	být	k5eAaImAgInP	být
dodány	dodán	k2eAgInPc1d1	dodán
stroje	stroj	k1gInPc1	stroj
F-7M	F-7M	k1gFnSc2	F-7M
z	z	k7c2	z
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
letuschopných	letuschopný	k2eAgFnPc2d1	letuschopná
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
neznámý	známý	k2eNgInSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
Jemen	Jemen	k1gInSc1	Jemen
získal	získat	k5eAaPmAgInS	získat
první	první	k4xOgFnSc4	první
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
ze	z	k7c2	z
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
Jižním	jižní	k2eAgInSc7d1	jižní
Jemenem	Jemen	k1gInSc7	Jemen
<g/>
.	.	kIx.	.
</s>
<s>
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vyzbrojoval	vyzbrojovat	k5eAaImAgMnS	vyzbrojovat
obě	dva	k4xCgFnPc4	dva
země	zem	k1gFnPc4	zem
a	a	k8xC	a
do	do	k7c2	do
Severního	severní	k2eAgInSc2d1	severní
Jemenu	Jemen	k1gInSc2	Jemen
dodal	dodat	k5eAaPmAgMnS	dodat
celkem	celkem	k6eAd1	celkem
45	[number]	k4	45
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
MiGů-	MiGů-	k1gFnSc7	MiGů-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1979	[number]	k4	1979
-	-	kIx~	-
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Jižního	jižní	k2eAgNnSc2d1	jižní
Jemen	Jemen	k1gInSc1	Jemen
získal	získat	k5eAaPmAgInS	získat
své	své	k1gNnSc4	své
první	první	k4xOgFnSc2	první
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
východních	východní	k2eAgFnPc2d1	východní
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
dodaných	dodaný	k2eAgInPc2d1	dodaný
strojů	stroj	k1gInPc2	stroj
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
Severním	severní	k2eAgInSc7d1	severní
a	a	k8xC	a
Jižním	jižní	k2eAgInSc7d1	jižní
Jemenem	Jemen	k1gInSc7	Jemen
<g/>
.	.	kIx.	.
<g/>
Kuba	Kuba	k1gFnSc1	Kuba
Kuba	Kuba	k1gFnSc1	Kuba
</s>
</p>
<p>
<s>
Kubánské	kubánský	k2eAgNnSc1d1	kubánské
revoluční	revoluční	k2eAgNnSc1d1	revoluční
letectvo	letectvo	k1gNnSc1	letectvo
a	a	k8xC	a
protivzdušná	protivzdušný	k2eAgFnSc1d1	protivzdušná
obrana	obrana	k1gFnSc1	obrana
si	se	k3xPyFc3	se
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
kubánské	kubánský	k2eAgFnSc2d1	kubánská
krize	krize	k1gFnSc2	krize
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
46	[number]	k4	46
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
a	a	k8xC	a
2	[number]	k4	2
dvoumístné	dvoumístný	k2eAgFnSc2d1	dvoumístná
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
U.	U.	kA	U.
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964	[number]	k4	1964
získala	získat	k5eAaPmAgFnS	získat
Kuba	Kuba	k1gFnSc1	Kuba
několik	několik	k4yIc4	několik
modelů	model	k1gInPc2	model
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
,	,	kIx,	,
následovaných	následovaný	k2eAgFnPc2d1	následovaná
30	[number]	k4	30
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1966	[number]	k4	1966
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
modelem	model	k1gInSc7	model
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
12	[number]	k4	12
ks	ks	kA	ks
průzkumných	průzkumný	k2eAgFnPc2d1	průzkumná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
R.	R.	kA	R.
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1972	[number]	k4	1972
-	-	kIx~	-
1974	[number]	k4	1974
doručeno	doručit	k5eAaPmNgNnS	doručit
60	[number]	k4	60
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
vyslala	vyslat	k5eAaPmAgFnS	vyslat
Kuba	Kuba	k1gFnSc1	Kuba
2	[number]	k4	2
eskadry	eskadra	k1gFnPc4	eskadra
vybavené	vybavený	k2eAgFnSc2d1	vybavená
stroji	stroj	k1gInSc6	stroj
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
do	do	k7c2	do
Angoly	Angola	k1gFnSc2	Angola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
občanské	občanský	k2eAgFnPc1d1	občanská
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
byly	být	k5eAaImAgFnP	být
dodány	dodat	k5eAaPmNgFnP	dodat
první	první	k4xOgFnPc1	první
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
270	[number]	k4	270
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
ve	v	k7c6	v
vzdušných	vzdušný	k2eAgFnPc6d1	vzdušná
silách	síla	k1gFnPc6	síla
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
2	[number]	k4	2
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
a	a	k8xC	a
12	[number]	k4	12
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
.	.	kIx.	.
<g/>
Libye	Libye	k1gFnSc1	Libye
LibyeLibyjské	LibyeLibyjský	k2eAgNnSc1d1	LibyeLibyjský
letectvo	letectvo	k1gNnSc1	letectvo
nakoupilo	nakoupit	k5eAaPmAgNnS	nakoupit
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
sovětské	sovětský	k2eAgFnSc2d1	sovětská
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc2d1	francouzská
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
i	i	k8xC	i
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
dodal	dodat	k5eAaPmAgMnS	dodat
SSSR	SSSR	kA	SSSR
několik	několik	k4yIc4	několik
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
kolem	kolem	k7c2	kolem
50	[number]	k4	50
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nahrazení	nahrazení	k1gNnSc4	nahrazení
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Sýrie	Sýrie	k1gFnSc1	Sýrie
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
během	během	k7c2	během
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
,	,	kIx,	,
předala	předat	k5eAaPmAgFnS	předat
Libye	Libye	k1gFnSc1	Libye
30	[number]	k4	30
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
syrskému	syrský	k2eAgNnSc3d1	syrské
letectvu	letectvo	k1gNnSc3	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pohraničního	pohraniční	k2eAgInSc2d1	pohraniční
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
byly	být	k5eAaImAgInP	být
oběma	dva	k4xCgMnPc7	dva
stranami	strana	k1gFnPc7	strana
nasazeny	nasazen	k2eAgFnPc1d1	nasazena
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
měly	mít	k5eAaImAgFnP	mít
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
v	v	k7c6	v
provozu	provoz	k1gInSc2	provoz
jednu	jeden	k4xCgFnSc4	jeden
eskadru	eskadra	k1gFnSc4	eskadra
MiGů-	MiGů-	k1gFnSc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	um	k1gInSc1	um
<g/>
.	.	kIx.	.
<g/>
Mali	Mali	k1gNnSc1	Mali
MaliMali	MaliMali	k1gFnSc2	MaliMali
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
získalo	získat	k5eAaPmAgNnS	získat
12	[number]	k4	12
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
další	další	k2eAgInSc4d1	další
2	[number]	k4	2
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
1	[number]	k4	1
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
MiGy-	MiGy-	k1gFnSc1	MiGy-
<g/>
21	[number]	k4	21
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
Mali	Mali	k1gNnSc2	Mali
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Bamako	Bamako	k1gNnSc1	Bamako
<g/>
.	.	kIx.	.
<g/>
Mosambik	Mosambik	k1gInSc1	Mosambik
MosambikMosambik	MosambikMosambika	k1gFnPc2	MosambikMosambika
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
48	[number]	k4	48
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kubánskými	kubánský	k2eAgInPc7d1	kubánský
piloty	pilot	k1gInPc7	pilot
a	a	k8xC	a
personálem	personál	k1gInSc7	personál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
smrskl	smrsknout	k5eAaPmAgInS	smrsknout
na	na	k7c4	na
18	[number]	k4	18
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
i	i	k9	i
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
bez	bez	k7c2	bez
náležité	náležitý	k2eAgFnSc2d1	náležitá
údržby	údržba	k1gFnSc2	údržba
postupně	postupně	k6eAd1	postupně
vyřazovány	vyřazován	k2eAgFnPc1d1	vyřazována
<g/>
.	.	kIx.	.
</s>
<s>
Mosambik	Mosambik	k1gInSc4	Mosambik
získal	získat	k5eAaPmAgMnS	získat
dalších	další	k2eAgFnPc2d1	další
6	[number]	k4	6
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
+	+	kIx~	+
2	[number]	k4	2
MiGy-	MiGy-	k1gFnPc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
z	z	k7c2	z
Rumuska	Rumusek	k1gMnSc2	Rumusek
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
firma	firma	k1gFnSc1	firma
Aerostar	Aerostar	k1gInSc1	Aerostar
<g/>
.	.	kIx.	.
<g/>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
</s>
</p>
<p>
<s>
Rumunské	rumunský	k2eAgNnSc1d1	rumunské
letectvo	letectvo	k1gNnSc1	letectvo
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1962	[number]	k4	1962
-	-	kIx~	-
1963	[number]	k4	1963
dostalo	dostat	k5eAaPmAgNnS	dostat
24	[number]	k4	24
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
dalších	další	k2eAgInPc2d1	další
38	[number]	k4	38
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
až	až	k6eAd1	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
dvoumístné	dvoumístný	k2eAgFnPc1d1	dvoumístná
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
se	se	k3xPyFc4	se
do	do	k7c2	do
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
dostaly	dostat	k5eAaPmAgFnP	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
4	[number]	k4	4
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
bylo	být	k5eAaImAgNnS	být
pořízeno	pořídit	k5eAaPmNgNnS	pořídit
29	[number]	k4	29
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
nést	nést	k5eAaImF	nést
nukleární	nukleární	k2eAgFnSc2d1	nukleární
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Vyřazeny	vyřazen	k2eAgFnPc1d1	vyřazena
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
počtech	počet	k1gInPc6	počet
konvenčních	konvenční	k2eAgFnPc2d1	konvenční
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
-	-	kIx~	-
1969	[number]	k4	1969
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
11	[number]	k4	11
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
a	a	k8xC	a
60	[number]	k4	60
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
byly	být	k5eAaImAgFnP	být
také	také	k9	také
dodány	dodán	k2eAgFnPc1d1	dodána
další	další	k2eAgFnPc1d1	další
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
a	a	k8xC	a
MiGy-	MiGy-	k1gFnSc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
US	US	kA	US
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
3	[number]	k4	3
ks	ks	kA	ks
a	a	k8xC	a
14	[number]	k4	14
ks	ks	kA	ks
respektive	respektive	k9	respektive
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1972	[number]	k4	1972
-	-	kIx~	-
1980	[number]	k4	1980
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
31	[number]	k4	31
ks	ks	kA	ks
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
14	[number]	k4	14
ks	ks	kA	ks
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
modernizovaných	modernizovaný	k2eAgMnPc2d1	modernizovaný
na	na	k7c6	na
verzi	verze	k1gFnSc6	verze
MiG-	MiG-	k1gMnSc2	MiG-
<g/>
21	[number]	k4	21
Lancer-	Lancer-	k1gFnSc2	Lancer-
<g/>
B.	B.	kA	B.
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
letecká	letecký	k2eAgFnSc1d1	letecká
technika	technika	k1gFnSc1	technika
bude	být	k5eAaImBp3nS	být
modernizována	modernizovat	k5eAaBmNgFnS	modernizovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
kompatibilní	kompatibilní	k2eAgFnSc1d1	kompatibilní
s	s	k7c7	s
technikou	technika	k1gFnSc7	technika
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
usilovalo	usilovat	k5eAaImAgNnS	usilovat
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
organizaci	organizace	k1gFnSc6	organizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
nové	nový	k2eAgInPc4d1	nový
stroje	stroj	k1gInPc4	stroj
nebyly	být	k5eNaImAgFnP	být
finance	finance	k1gFnPc1	finance
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
podrobit	podrobit	k5eAaPmF	podrobit
se	se	k3xPyFc4	se
modernizaci	modernizace	k1gFnSc3	modernizace
padlo	padnout	k5eAaPmAgNnS	padnout
na	na	k7c6	na
MiGy-	MiGy-	k1gFnSc6	MiGy-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
a	a	k8xC	a
MiGy-	MiGy-	k1gFnSc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
firmou	firma	k1gFnSc7	firma
Elbit	Elbita	k1gFnPc2	Elbita
upravila	upravit	k5eAaPmAgFnS	upravit
společnost	společnost	k1gFnSc1	společnost
Aerostar	Aerostara	k1gFnPc2	Aerostara
přes	přes	k7c4	přes
100	[number]	k4	100
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
osazením	osazení	k1gNnSc7	osazení
západní	západní	k2eAgFnSc2d1	západní
avioniky	avionika	k1gFnSc2	avionika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dodávky	dodávka	k1gFnPc4	dodávka
těchto	tento	k3xDgNnPc2	tento
letadel	letadlo	k1gNnPc2	letadlo
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
MiGy-	MiGy-	k?	MiGy-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
byly	být	k5eAaImAgInP	být
nově	nově	k6eAd1	nově
označeny	označit	k5eAaPmNgInP	označit
na	na	k7c4	na
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
Lancer-A	Lancer-A	k1gFnPc2	Lancer-A
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byly	být	k5eAaImAgInP	být
uzpůsobeny	uzpůsobit	k5eAaPmNgInP	uzpůsobit
na	na	k7c4	na
roli	role	k1gFnSc4	role
stíhacího	stíhací	k2eAgInSc2d1	stíhací
bombardéru	bombardér	k1gInSc2	bombardér
<g/>
;	;	kIx,	;
z	z	k7c2	z
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
Lancer-C	Lancer-C	k1gFnPc2	Lancer-C
<g/>
,	,	kIx,	,
uzpůsobené	uzpůsobený	k2eAgFnSc2d1	uzpůsobená
ke	k	k7c3	k
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
verze	verze	k1gFnPc4	verze
MiGu-	MiGu-	k1gFnSc4	MiGu-
<g/>
21	[number]	k4	21
Lancer	Lancra	k1gFnPc2	Lancra
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
<g/>
.	.	kIx.	.
<g/>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
Severní	severní	k2eAgFnSc1d1	severní
KoreaSeverkorejské	KoreaSeverkorejský	k2eAgNnSc4d1	KoreaSeverkorejský
letectvo	letectvo	k1gNnSc4	letectvo
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
14	[number]	k4	14
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1966	[number]	k4	1966
-	-	kIx~	-
1967	[number]	k4	1967
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
dalších	další	k2eAgInPc2d1	další
80	[number]	k4	80
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gMnPc2	F-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1969	[number]	k4	1969
disponovalo	disponovat	k5eAaBmAgNnS	disponovat
letectvo	letectvo	k1gNnSc1	letectvo
96	[number]	k4	96
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
46	[number]	k4	46
ks	ks	kA	ks
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
MiG-	MiG-	k1gMnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
a	a	k8xC	a
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
přestal	přestat	k5eAaPmAgInS	přestat
s	s	k7c7	s
dodávkami	dodávka	k1gFnPc7	dodávka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
150	[number]	k4	150
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
-	-	kIx~	-
1991	[number]	k4	1991
dodáno	dodat	k5eAaPmNgNnS	dodat
30	[number]	k4	30
ks	ks	kA	ks
modelu	model	k1gInSc2	model
F-	F-	k1gFnSc2	F-
<g/>
7	[number]	k4	7
<g/>
B.	B.	kA	B.
Poslední	poslední	k2eAgFnSc2d1	poslední
dodávky	dodávka	k1gFnSc2	dodávka
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
z	z	k7c2	z
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
pořízeno	pořídit	k5eAaPmNgNnS	pořídit
38	[number]	k4	38
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Špatná	špatný	k2eAgFnSc1d1	špatná
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
však	však	k9	však
podepsala	podepsat	k5eAaPmAgFnS	podepsat
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
letuschopnosti	letuschopnost	k1gFnSc6	letuschopnost
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgFnP	provozovat
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
<g/>
Srbsko	Srbsko	k1gNnSc1	Srbsko
Srbsko	Srbsko	k1gNnSc1	Srbsko
</s>
</p>
<p>
<s>
Srbské	srbský	k2eAgNnSc1d1	srbské
letectvo	letectvo	k1gNnSc1	letectvo
a	a	k8xC	a
protivzdušná	protivzdušný	k2eAgFnSc1d1	protivzdušná
obrana	obrana	k1gFnSc1	obrana
získalo	získat	k5eAaPmAgNnS	získat
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
většinu	většina	k1gFnSc4	většina
techniky	technika	k1gFnSc2	technika
z	z	k7c2	z
výzbroje	výzbroj	k1gFnSc2	výzbroj
bývalé	bývalý	k2eAgFnSc2d1	bývalá
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
časných	časný	k2eAgFnPc2d1	časná
verzí	verze	k1gFnPc2	verze
byla	být	k5eAaImAgFnS	být
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
a	a	k8xC	a
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
zůstaly	zůstat	k5eAaPmAgInP	zůstat
jen	jen	k9	jen
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
MiGy-	MiGy-	k1gFnSc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
MiGy-	MiGy-	k1gFnSc7	MiGy-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bojích	boj	k1gInPc6	boj
nad	nad	k7c7	nad
západním	západní	k2eAgNnSc7d1	západní
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
a	a	k8xC	a
nad	nad	k7c7	nad
srbskou	srbský	k2eAgFnSc7d1	Srbská
Krajinou	Krajina	k1gFnSc7	Krajina
bylo	být	k5eAaImAgNnS	být
sestřeleno	sestřelit	k5eAaPmNgNnS	sestřelit
několik	několik	k4yIc1	několik
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
MiGů-	MiGů-	k1gFnSc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
Srbsko	Srbsko	k1gNnSc1	Srbsko
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
ks	ks	kA	ks
jednomístných	jednomístný	k2eAgFnPc2d1	jednomístná
a	a	k8xC	a
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
Súdán	Súdán	k1gInSc1	Súdán
SúdánSúdán	SúdánSúdán	k2eAgMnSc1d1	SúdánSúdán
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
18	[number]	k4	18
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
a	a	k8xC	a
4	[number]	k4	4
ks	ks	kA	ks
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
výzbroji	výzbroj	k1gInSc6	výzbroj
už	už	k6eAd1	už
jen	jen	k9	jen
7	[number]	k4	7
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
letuschopná	letuschopný	k2eAgFnSc1d1	letuschopná
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
podpory	podpora	k1gFnSc2	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
SSSR	SSSR	kA	SSSR
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
Súdán	Súdán	k1gInSc4	Súdán
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Íránu	Írán	k1gInSc2	Írán
15	[number]	k4	15
ks	ks	kA	ks
čínských	čínský	k2eAgFnPc2d1	čínská
kopií	kopie	k1gFnPc2	kopie
Chengdu	Chengd	k1gInSc2	Chengd
F-7B	F-7B	k1gFnSc2	F-7B
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
;	;	kIx,	;
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
6	[number]	k4	6
ks	ks	kA	ks
F-7B	F-7B	k1gFnPc2	F-7B
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
FT-7	FT-7	k1gFnSc2	FT-7
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
nahrazovány	nahrazován	k2eAgInPc1d1	nahrazován
stíhačkami	stíhačka	k1gFnPc7	stíhačka
MiG-	MiG-	k1gMnSc7	MiG-
<g/>
29	[number]	k4	29
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
už	už	k9	už
jen	jen	k9	jen
28	[number]	k4	28
ks	ks	kA	ks
F-	F-	k1gFnSc2	F-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
mezitím	mezitím	k6eAd1	mezitím
obdržel	obdržet	k5eAaPmAgInS	obdržet
12	[number]	k4	12
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
<g/>
Sýrie	Sýrie	k1gFnSc1	Sýrie
SýrieSyrské	SýrieSyrský	k2eAgFnSc2d1	SýrieSyrský
arabské	arabský	k2eAgFnSc2d1	arabská
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
síly	síla	k1gFnSc2	síla
získaly	získat	k5eAaPmAgFnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
36	[number]	k4	36
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
dalšími	další	k2eAgMnPc7d1	další
45	[number]	k4	45
ks	ks	kA	ks
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
a	a	k8xC	a
15	[number]	k4	15
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
FL	FL	kA	FL
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
dvoumístné	dvoumístný	k2eAgFnPc1d1	dvoumístná
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
6	[number]	k4	6
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
během	během	k7c2	během
šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
dodal	dodat	k5eAaPmAgMnS	dodat
SSSR	SSSR	kA	SSSR
24	[number]	k4	24
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgFnP	mít
nahradit	nahradit	k5eAaPmF	nahradit
utrpěné	utrpěný	k2eAgFnPc4d1	utrpěná
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byly	být	k5eAaImAgFnP	být
dodány	dodán	k2eAgFnPc1d1	dodána
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
40	[number]	k4	40
ks	ks	kA	ks
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
60	[number]	k4	60
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
získala	získat	k5eAaPmAgFnS	získat
Sýrie	Sýrie	k1gFnSc1	Sýrie
6	[number]	k4	6
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
byly	být	k5eAaImAgFnP	být
dodány	dodán	k2eAgFnPc1d1	dodána
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
21	[number]	k4	21
ks	ks	kA	ks
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
dalšími	další	k2eAgMnPc7d1	další
40	[number]	k4	40
ks	ks	kA	ks
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
byly	být	k5eAaImAgFnP	být
dodány	dodat	k5eAaPmNgInP	dodat
i	i	k9	i
dvoumístné	dvoumístný	k2eAgFnPc1d1	dvoumístná
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
20	[number]	k4	20
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
šestidenní	šestidenní	k2eAgFnSc7d1	šestidenní
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
jomkipurskou	jomkipurský	k2eAgFnSc7d1	jomkipurská
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
ztratila	ztratit	k5eAaPmAgFnS	ztratit
Sýrie	Sýrie	k1gFnPc4	Sýrie
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
SSSR	SSSR	kA	SSSR
k	k	k7c3	k
dodání	dodání	k1gNnSc3	dodání
75	[number]	k4	75
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgFnP	mít
nahradit	nahradit	k5eAaPmF	nahradit
ztráty	ztráta	k1gFnPc1	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jomkipurské	jomkipurský	k2eAgFnSc2d1	jomkipurská
války	válka	k1gFnSc2	válka
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
Sýrie	Sýrie	k1gFnSc1	Sýrie
od	od	k7c2	od
NDR	NDR	kA	NDR
12	[number]	k4	12
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
podobné	podobný	k2eAgInPc4d1	podobný
verzi	verze	k1gFnSc6	verze
MF	MF	kA	MF
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
NDR	NDR	kA	NDR
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
dodávaly	dodávat	k5eAaImAgInP	dodávat
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
21	[number]	k4	21
i	i	k8xC	i
jiné	jiný	k2eAgFnPc1d1	jiná
země	zem	k1gFnPc1	zem
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
ČSSR	ČSSR	kA	ČSSR
a	a	k8xC	a
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
byl	být	k5eAaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
zničen	zničit	k5eAaPmNgInS	zničit
izraelskými	izraelský	k2eAgFnPc7d1	izraelská
vzdušnými	vzdušný	k2eAgFnPc7d1	vzdušná
silami	síla	k1gFnPc7	síla
nad	nad	k7c7	nad
Libanonem	Libanon	k1gInSc7	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
SSSR	SSSR	kA	SSSR
opět	opět	k6eAd1	opět
prodal	prodat	k5eAaPmAgMnS	prodat
Sýrii	Sýrie	k1gFnSc4	Sýrie
198	[number]	k4	198
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
starších	starý	k2eAgFnPc2d2	starší
61	[number]	k4	61
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
uloženo	uložit	k5eAaPmNgNnS	uložit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
MiG-	MiG-	k1gMnPc3	MiG-
<g/>
21	[number]	k4	21
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
stíhačkami	stíhačka	k1gFnPc7	stíhačka
MiG-	MiG-	k1gMnPc7	MiG-
<g/>
29	[number]	k4	29
a	a	k8xC	a
Su-	Su-	k1gMnSc1	Su-
<g/>
27	[number]	k4	27
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
letuschopných	letuschopný	k2eAgFnPc2d1	letuschopná
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
snížil	snížit	k5eAaPmAgInS	snížit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
na	na	k7c4	na
100	[number]	k4	100
ks	ks	kA	ks
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
byla	být	k5eAaImAgFnS	být
uzpůsobena	uzpůsobit	k5eAaPmNgFnS	uzpůsobit
na	na	k7c4	na
fotografický	fotografický	k2eAgInSc4d1	fotografický
průzkum	průzkum	k1gInSc4	průzkum
<g/>
.	.	kIx.	.
<g/>
Uganda	Uganda	k1gFnSc1	Uganda
UgandaUganda	UgandaUganda	k1gFnSc1	UgandaUganda
získala	získat	k5eAaPmAgFnS	získat
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
18	[number]	k4	18
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
U.	U.	kA	U.
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1976	[number]	k4	1976
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
zásahu	zásah	k1gInSc2	zásah
izraelského	izraelský	k2eAgNnSc2d1	izraelské
komanda	komando	k1gNnSc2	komando
v	v	k7c6	v
Entebbe	Entebb	k1gInSc5	Entebb
zničeno	zničen	k2eAgNnSc1d1	zničeno
7	[number]	k4	7
MiGů-	MiGů-	k1gMnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
pořídila	pořídit	k5eAaPmAgFnS	pořídit
Uganda	Uganda	k1gFnSc1	Uganda
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Izraele	Izrael	k1gInSc2	Izrael
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
6	[number]	k4	6
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
už	už	k6eAd1	už
jen	jen	k9	jen
několik	několik	k4yIc4	několik
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
<g/>
Zambie	Zambie	k1gFnSc1	Zambie
ZambieZambijské	ZambieZambijský	k2eAgNnSc1d1	ZambieZambijský
letectvo	letectvo	k1gNnSc1	letectvo
pořídilo	pořídit	k5eAaPmAgNnS	pořídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
14	[number]	k4	14
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
stroje	stroj	k1gInPc1	stroj
kromě	kromě	k7c2	kromě
jednoho	jeden	k4xCgInSc2	jeden
dvoumístného	dvoumístný	k2eAgInSc2d1	dvoumístný
MiGu-	MiGu-	k1gFnPc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
-	-	kIx~	-
1998	[number]	k4	1998
modernizovány	modernizován	k2eAgFnPc1d1	modernizována
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
stav	stav	k1gInSc1	stav
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
Zambie	Zambie	k1gFnSc2	Zambie
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
uživatelé	uživatel	k1gMnPc1	uživatel
===	===	k?	===
</s>
</p>
<p>
<s>
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
</s>
</p>
<p>
<s>
Afghánské	afghánský	k2eAgFnPc1d1	afghánská
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
získaly	získat	k5eAaPmAgInP	získat
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
zemí	zem	k1gFnPc2	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
přes	přes	k7c4	přes
40	[number]	k4	40
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
sovětské	sovětský	k2eAgFnSc2d1	sovětská
invaze	invaze	k1gFnSc2	invaze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byly	být	k5eAaImAgFnP	být
afghánské	afghánský	k2eAgFnPc1d1	afghánská
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
posíleny	posílen	k2eAgFnPc1d1	posílena
70	[number]	k4	70
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
,	,	kIx,	,
50	[number]	k4	50
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
6	[number]	k4	6
cvičnými	cvičný	k2eAgInPc7d1	cvičný
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
konfliktu	konflikt	k1gInSc2	konflikt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
nastal	nastat	k5eAaPmAgInS	nastat
boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
mudžahedíny	mudžahedín	k1gMnPc7	mudžahedín
a	a	k8xC	a
vládními	vládní	k2eAgFnPc7d1	vládní
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vítězstvím	vítězství	k1gNnSc7	vítězství
islámských	islámský	k2eAgMnPc2d1	islámský
radikálů	radikál	k1gMnPc2	radikál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
už	už	k6eAd1	už
jen	jen	k9	jen
málo	málo	k6eAd1	málo
MiGů-	MiGů-	k1gFnSc3	MiGů-
<g/>
21	[number]	k4	21
schopných	schopný	k2eAgMnPc2d1	schopný
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
letuschopných	letuschopný	k2eAgMnPc2d1	letuschopný
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
během	během	k7c2	během
americké	americký	k2eAgFnSc2d1	americká
invaze	invaze	k1gFnSc2	invaze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
a	a	k8xC	a
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
bojů	boj	k1gInPc2	boj
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
<g/>
.	.	kIx.	.
<g/>
Albánie	Albánie	k1gFnSc2	Albánie
AlbánieAlbánské	AlbánieAlbánský	k2eAgNnSc4d1	AlbánieAlbánský
vzdušné	vzdušný	k2eAgNnSc4d1	vzdušné
sílyAlžírsko	sílyAlžírsko	k1gNnSc4	sílyAlžírsko
AlžírskoAlžírské	alžírskoalžírský	k2eAgNnSc1d1	alžírskoalžírský
letectvo	letectvo	k1gNnSc1	letectvo
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
prvních	první	k4xOgInPc2	první
6	[number]	k4	6
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yIgMnPc6	který
následovala	následovat	k5eAaImAgFnS	následovat
dodávka	dodávka	k1gFnSc1	dodávka
přes	přes	k7c4	přes
40	[number]	k4	40
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
vyslalo	vyslat	k5eAaPmAgNnS	vyslat
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
31	[number]	k4	31
strojů	stroj	k1gInPc2	stroj
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
6	[number]	k4	6
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
omylem	omylem	k6eAd1	omylem
přistálo	přistát	k5eAaImAgNnS	přistát
na	na	k7c6	na
egyptské	egyptský	k2eAgFnSc6d1	egyptská
letecké	letecký	k2eAgFnSc6d1	letecká
základně	základna	k1gFnSc6	základna
na	na	k7c6	na
Sinaji	Sinaj	k1gInSc6	Sinaj
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
právě	právě	k6eAd1	právě
dobyl	dobýt	k5eAaPmAgInS	dobýt
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
letadel	letadlo	k1gNnPc2	letadlo
v	v	k7c6	v
letuschopném	letuschopný	k2eAgInSc6d1	letuschopný
stavu	stav	k1gInSc6	stav
byla	být	k5eAaImAgFnS	být
odeslána	odeslat	k5eAaPmNgFnS	odeslat
do	do	k7c2	do
USA	USA	kA	USA
na	na	k7c4	na
analýzu	analýza	k1gFnSc4	analýza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1966	[number]	k4	1966
-	-	kIx~	-
7	[number]	k4	7
dostalo	dostat	k5eAaPmAgNnS	dostat
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
dalších	další	k2eAgInPc2d1	další
30	[number]	k4	30
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
,	,	kIx,	,
následovaných	následovaný	k2eAgInPc2d1	následovaný
dodávkou	dodávka	k1gFnSc7	dodávka
40	[number]	k4	40
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
zakoupeno	zakoupit	k5eAaPmNgNnS	zakoupit
několik	několik	k4yIc1	několik
stíhaček	stíhačka	k1gFnPc2	stíhačka
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
,	,	kIx,	,
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
sovětské	sovětský	k2eAgFnSc2d1	sovětská
pomoci	pomoc	k1gFnSc2	pomoc
byly	být	k5eAaImAgFnP	být
stíhačky	stíhačka	k1gFnPc1	stíhačka
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
postupně	postupně	k6eAd1	postupně
vyřazovány	vyřazován	k2eAgInPc1d1	vyřazován
a	a	k8xC	a
nahrazovány	nahrazován	k2eAgInPc4d1	nahrazován
modernějšími	moderní	k2eAgMnPc7d2	modernější
MiG-	MiG-	k1gMnPc7	MiG-
<g/>
29	[number]	k4	29
a	a	k8xC	a
Su-	Su-	k1gMnSc1	Su-
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
alžírských	alžírský	k2eAgFnPc6d1	alžírská
vzdušných	vzdušný	k2eAgFnPc6d1	vzdušná
silách	síla	k1gFnPc6	síla
sloužilo	sloužit	k5eAaImAgNnS	sloužit
kolem	kolem	k6eAd1	kolem
120	[number]	k4	120
stíhaček	stíhačka	k1gFnPc2	stíhačka
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poslední	poslední	k2eAgInSc4d1	poslední
let	let	k1gInSc4	let
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
<g/>
Bangladéš	Bangladéš	k1gInSc4	Bangladéš
BangladéšBangladéšské	BangladéšBangladéšský	k2eAgNnSc1d1	BangladéšBangladéšský
letectvo	letectvo	k1gNnSc1	letectvo
používalo	používat	k5eAaImAgNnS	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
6	[number]	k4	6
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
<g/>
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
BěloruskoBěloruské	běloruskoběloruský	k2eAgNnSc4d1	běloruskoběloruský
letectvoBulharsko	letectvoBulharsko	k6eAd1	letectvoBulharsko
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
</s>
</p>
<p>
<s>
Bulharské	bulharský	k2eAgNnSc1d1	bulharské
letectvo	letectvo	k1gNnSc1	letectvo
dostalo	dostat	k5eAaPmAgNnS	dostat
první	první	k4xOgFnPc4	první
MiGy-	MiGy-	k1gFnPc4	MiGy-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc2	F-
<g/>
13	[number]	k4	13
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1963	[number]	k4	1963
a	a	k8xC	a
nahradily	nahradit	k5eAaPmAgFnP	nahradit
starší	starý	k2eAgFnPc1d2	starší
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
stroje	stroj	k1gInPc1	stroj
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
přeřazeny	přeřazen	k2eAgInPc1d1	přeřazen
k	k	k7c3	k
průzkumné	průzkumný	k2eAgFnSc3d1	průzkumná
jednotce	jednotka	k1gFnSc3	jednotka
a	a	k8xC	a
vybaveny	vybaven	k2eAgFnPc1d1	vybavena
kamerami	kamera	k1gFnPc7	kamera
a	a	k8xC	a
fotoaparáty	fotoaparát	k1gInPc7	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
12	[number]	k4	12
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
a	a	k8xC	a
také	také	k9	také
první	první	k4xOgInPc1	první
stroje	stroj	k1gInPc1	stroj
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
byly	být	k5eAaImAgInP	být
průzkumné	průzkumný	k2eAgInPc1d1	průzkumný
eskadře	eskadra	k1gFnSc6	eskadra
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
26	[number]	k4	26
přiřazeny	přiřazen	k2eAgFnPc1d1	přiřazena
první	první	k4xOgFnPc4	první
MiGy-	MiGy-	k1gFnPc4	MiGy-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
byly	být	k5eAaImAgFnP	být
dodány	dodán	k2eAgFnPc1d1	dodána
i	i	k9	i
12	[number]	k4	12
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Graf	graf	k1gInSc1	graf
Ignatiev	Ignativa	k1gFnPc2	Ignativa
nahradily	nahradit	k5eAaPmAgFnP	nahradit
zastaralý	zastaralý	k2eAgInSc4d1	zastaralý
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
je	on	k3xPp3gFnPc4	on
následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
stroje	stroj	k1gInPc1	stroj
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
sloužících	sloužící	k1gFnPc2	sloužící
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
bulharském	bulharský	k2eAgNnSc6d1	bulharské
letectvu	letectvo	k1gNnSc6	letectvo
nejkratší	krátký	k2eAgFnSc4d3	nejkratší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ji	on	k3xPp3gFnSc4	on
vyřadili	vyřadit	k5eAaPmAgMnP	vyřadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
pořízeny	pořídit	k5eAaPmNgFnP	pořídit
první	první	k4xOgFnPc1	první
dvoumístné	dvoumístný	k2eAgFnPc1d1	dvoumístná
MiG-	MiG-	k1gFnPc1	MiG-
<g/>
21	[number]	k4	21
<g/>
US	US	kA	US
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1974	[number]	k4	1974
-	-	kIx~	-
1982	[number]	k4	1982
doplnily	doplnit	k5eAaPmAgFnP	doplnit
vylepšené	vylepšený	k2eAgFnPc1d1	vylepšená
MiG-	MiG-	k1gFnPc1	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1983	[number]	k4	1983
-	-	kIx~	-
1985	[number]	k4	1985
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
zaváděn	zavádět	k5eAaImNgInS	zavádět
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
SSSR	SSSR	kA	SSSR
dodal	dodat	k5eAaPmAgMnS	dodat
dalších	další	k2eAgFnPc2d1	další
36	[number]	k4	36
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
celkem	celkem	k6eAd1	celkem
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
150	[number]	k4	150
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
omezeních	omezení	k1gNnPc6	omezení
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřazen	k2eAgFnPc1d1	vyřazena
verze	verze	k1gFnPc1	verze
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
,	,	kIx,	,
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
a	a	k8xC	a
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
M.	M.	kA	M.
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
regulující	regulující	k2eAgNnSc1d1	regulující
konvenční	konvenční	k2eAgNnSc1d1	konvenční
vojsko	vojsko	k1gNnSc1	vojsko
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byly	být	k5eAaImAgInP	být
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
zbaveny	zbaven	k2eAgFnPc4d1	zbavena
možnosti	možnost	k1gFnPc4	možnost
nést	nést	k5eAaImF	nést
nukleární	nukleární	k2eAgFnPc4d1	nukleární
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
MiGy-	MiGy-	k1gFnPc4	MiGy-
<g/>
21	[number]	k4	21
byly	být	k5eAaImAgFnP	být
odzbrojeny	odzbrojit	k5eAaPmNgInP	odzbrojit
a	a	k8xC	a
označeny	označit	k5eAaPmNgInP	označit
jako	jako	k8xC	jako
MiG-	MiG-	k1gFnPc1	MiG-
<g/>
21	[number]	k4	21
<g/>
-UM-	-UM-	k?	-UM-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
nemělo	mít	k5eNaImAgNnS	mít
finance	finance	k1gFnPc4	finance
na	na	k7c4	na
pořízení	pořízení	k1gNnSc4	pořízení
nových	nový	k2eAgFnPc2d1	nová
stíhaček	stíhačka	k1gFnPc2	stíhačka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
modely	model	k1gInPc4	model
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
ponechány	ponechat	k5eAaPmNgFnP	ponechat
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
modernizována	modernizován	k2eAgFnSc1d1	modernizována
instalací	instalace	k1gFnSc7	instalace
GPS	GPS	kA	GPS
přijímačů	přijímač	k1gMnPc2	přijímač
<g/>
,	,	kIx,	,
transpondéry	transpondér	k1gInPc7	transpondér
a	a	k8xC	a
záznamovými	záznamový	k2eAgNnPc7d1	záznamové
zařízeními	zařízení	k1gNnPc7	zařízení
letových	letový	k2eAgInPc2d1	letový
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
byly	být	k5eAaImAgFnP	být
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
21	[number]	k4	21
vyřazeny	vyřazen	k2eAgInPc1d1	vyřazen
<g/>
.	.	kIx.	.
</s>
<s>
Burkina	Burkina	k1gFnSc1	Burkina
Faso	Faso	k6eAd1	Faso
Burkina	Burkin	k2eAgFnSc1d1	Burkina
FasoBurkina	FasoBurkina	k1gFnSc1	FasoBurkina
Faso	Faso	k6eAd1	Faso
získala	získat	k5eAaPmAgFnS	získat
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
8	[number]	k4	8
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
je	být	k5eAaImIp3nS	být
nasadila	nasadit	k5eAaPmAgFnS	nasadit
v	v	k7c6	v
pohraničním	pohraniční	k2eAgInSc6d1	pohraniční
konfliktu	konflikt	k1gInSc6	konflikt
s	s	k7c7	s
Mali	Mali	k1gNnSc7	Mali
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
už	už	k6eAd1	už
nebyly	být	k5eNaImAgInP	být
schopné	schopný	k2eAgInPc1d1	schopný
provozu	provoz	k1gInSc6	provoz
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Ouagadougou	Ouagadouga	k1gFnSc7	Ouagadouga
<g/>
.	.	kIx.	.
<g/>
Čad	Čad	k1gInSc1	Čad
Čad	Čad	k1gInSc1	Čad
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
Československo	Československo	k1gNnSc1	Československo
</s>
</p>
<p>
<s>
Československé	československý	k2eAgNnSc1d1	Československé
letectvo	letectvo	k1gNnSc1	letectvo
provozovalo	provozovat	k5eAaImAgNnS	provozovat
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
21	[number]	k4	21
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
byla	být	k5eAaImAgFnS	být
licenční	licenční	k2eAgFnSc1d1	licenční
výroba	výroba	k1gFnSc1	výroba
typu	typ	k1gInSc2	typ
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
Aero	aero	k1gNnSc4	aero
Vodochody	Vodochod	k1gInPc4	Vodochod
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1963	[number]	k4	1963
-	-	kIx~	-
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
194	[number]	k4	194
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xS	jako
S-	S-	k1gFnSc1	S-
<g/>
106	[number]	k4	106
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
přešly	přejít	k5eAaPmAgInP	přejít
na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
název	název	k1gInSc4	název
MiG-	MiG-	k1gMnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
zdrojem	zdroj	k1gInSc7	zdroj
byly	být	k5eAaImAgFnP	být
dodávky	dodávka	k1gFnPc1	dodávka
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byly	být	k5eAaImAgFnP	být
dodány	dodán	k2eAgFnPc1d1	dodána
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vyřazeny	vyřazen	k2eAgFnPc1d1	vyřazena
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1965	[number]	k4	1965
-	-	kIx~	-
1966	[number]	k4	1966
byly	být	k5eAaImAgFnP	být
dodány	dodán	k2eAgFnPc1d1	dodána
dvoumístné	dvoumístný	k2eAgFnPc1d1	dvoumístná
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgFnSc2d1	následovaná
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
US	US	kA	US
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
-	-	kIx~	-
1970	[number]	k4	1970
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
MiGy-	MiGy-	k1gFnSc3	MiGy-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1971	[number]	k4	1971
-	-	kIx~	-
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
do	do	k7c2	do
1969	[number]	k4	1969
probíhaly	probíhat	k5eAaImAgFnP	probíhat
dodávky	dodávka	k1gFnPc1	dodávka
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
poslední	poslední	k2eAgFnPc1d1	poslední
kusy	kus	k1gInPc4	kus
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
a	a	k8xC	a
1973	[number]	k4	1973
dodala	dodat	k5eAaPmAgFnS	dodat
ČSSR	ČSSR	kA	ČSSR
několik	několik	k4yIc4	několik
MiGů-	MiGů-	k1gMnSc4	MiGů-
<g/>
21	[number]	k4	21
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měly	mít	k5eAaImAgFnP	mít
nahradit	nahradit	k5eAaPmF	nahradit
válečné	válečný	k2eAgFnPc1d1	válečná
ztráty	ztráta	k1gFnPc1	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1969	[number]	k4	1969
-	-	kIx~	-
1972	[number]	k4	1972
byl	být	k5eAaImAgMnS	být
dodán	dodán	k2eAgMnSc1d1	dodán
průzkumný	průzkumný	k2eAgMnSc1d1	průzkumný
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
do	do	k7c2	do
rozdělení	rozdělení	k1gNnSc2	rozdělení
federace	federace	k1gFnSc2	federace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
a	a	k8xC	a
následně	následně	k6eAd1	následně
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
letectvu	letectvo	k1gNnSc6	letectvo
Česka	Česko	k1gNnSc2	Česko
i	i	k8xC	i
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
i	i	k9	i
první	první	k4xOgFnPc1	první
dodávky	dodávka	k1gFnPc1	dodávka
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
(	(	kIx(	(
<g/>
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
MA	MA	kA	MA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rovněž	rovněž	k9	rovněž
sloužily	sloužit	k5eAaImAgFnP	sloužit
až	až	k9	až
do	do	k7c2	do
rozpadu	rozpad	k1gInSc2	rozpad
ČSSR	ČSSR	kA	ČSSR
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c6	v
nástupnických	nástupnický	k2eAgFnPc6d1	nástupnická
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1971	[number]	k4	1971
-	-	kIx~	-
1975	[number]	k4	1975
získalo	získat	k5eAaPmAgNnS	získat
Československo	Československo	k1gNnSc1	Československo
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pak	pak	k6eAd1	pak
tvořily	tvořit	k5eAaImAgFnP	tvořit
páteř	páteř	k1gFnSc4	páteř
stíhacích	stíhací	k2eAgFnPc2d1	stíhací
jednotek	jednotka	k1gFnPc2	jednotka
našeho	náš	k3xOp1gNnSc2	náš
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
<g/>
Česko	Česko	k1gNnSc1	Česko
ČeskoVzdušné	českovzdušný	k2eAgFnSc2d1	českovzdušný
síly	síla	k1gFnSc2	síla
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
vyřadily	vyřadit	k5eAaPmAgFnP	vyřadit
poslední	poslední	k2eAgFnSc4d1	poslední
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
21	[number]	k4	21
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
<g/>
Čína	Čína	k1gFnSc1	Čína
ČínaLetectvo	ČínaLetectvo	k1gNnSc1	ČínaLetectvo
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
získalo	získat	k5eAaPmAgNnS	získat
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
výzbroje	výzbroj	k1gFnSc2	výzbroj
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
licenční	licenční	k2eAgFnSc2d1	licenční
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Jednomístné	jednomístný	k2eAgFnSc2d1	jednomístná
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
Čcheng-tu	Čcheng	k1gInSc2	Čcheng-t
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
J-7	J-7	k1gFnSc2	J-7
a	a	k8xC	a
dvoumístné	dvoumístný	k2eAgFnSc2d1	dvoumístná
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
továrna	továrna	k1gFnSc1	továrna
Guizhou	Guizha	k1gFnSc7	Guizha
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
JJ-	JJ-	k1gFnSc2	JJ-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
stroje	stroj	k1gInPc1	stroj
J-7L	J-7L	k1gFnPc2	J-7L
se	se	k3xPyFc4	se
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
dostaly	dostat	k5eAaPmAgInP	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
Čína	Čína	k1gFnSc1	Čína
1	[number]	k4	1
575	[number]	k4	575
ks	ks	kA	ks
stíhaček	stíhačka	k1gFnPc2	stíhačka
J-7	J-7	k1gFnPc2	J-7
různých	různý	k2eAgInPc2d1	různý
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
920	[number]	k4	920
ks	ks	kA	ks
operačně	operačně	k6eAd1	operačně
nasaditelná	nasaditelný	k2eAgFnSc1d1	nasaditelná
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
J-7	J-7	k1gFnSc1	J-7
početně	početně	k6eAd1	početně
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
strojem	stroj	k1gInSc7	stroj
v	v	k7c6	v
čínském	čínský	k2eAgNnSc6d1	čínské
letectvu	letectvo	k1gNnSc6	letectvo
<g/>
.	.	kIx.	.
<g/>
Kongo	Kongo	k1gNnSc1	Kongo
(	(	kIx(	(
<g/>
Kinshasa	Kinshasa	k1gFnSc1	Kinshasa
<g/>
)	)	kIx)	)
Kongo	Kongo	k1gNnSc1	Kongo
(	(	kIx(	(
<g/>
Kinshasa	Kinshasa	k1gFnSc1	Kinshasa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Východní	východní	k2eAgNnSc1d1	východní
Německo	Německo	k1gNnSc1	Německo
Východní	východní	k2eAgNnSc1d1	východní
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
Národní	národní	k2eAgFnSc2d1	národní
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
získali	získat	k5eAaPmAgMnP	získat
první	první	k4xOgMnPc1	první
MiGy-	MiGy-	k1gMnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc2	F-
<g/>
13	[number]	k4	13
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
k	k	k7c3	k
Jagdgeschwader	Jagdgeschwadero	k1gNnPc2	Jagdgeschwadero
8	[number]	k4	8
"	"	kIx"	"
<g/>
Hermann	Hermann	k1gMnSc1	Hermann
Matern	Matern	k1gMnSc1	Matern
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
spolu	spolu	k6eAd1	spolu
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
dodáno	dodat	k5eAaPmNgNnS	dodat
76	[number]	k4	76
ks	ks	kA	ks
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1964	[number]	k4	1964
byla	být	k5eAaImAgFnS	být
dodána	dodat	k5eAaPmNgFnS	dodat
zásilka	zásilka	k1gFnSc1	zásilka
54	[number]	k4	54
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgFnSc1d1	následovaná
MiGy-	MiGy-	k1gFnSc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964	[number]	k4	1964
-	-	kIx~	-
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
k	k	k7c3	k
Jagdgeschwader	Jagdgeschwader	k1gMnSc1	Jagdgeschwader
1	[number]	k4	1
a	a	k8xC	a
8	[number]	k4	8
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
několik	několik	k4yIc1	několik
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
ik	ik	k?	ik
Jagdgeschwader	Jagdgeschwader	k1gInSc4	Jagdgeschwader
2	[number]	k4	2
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
byl	být	k5eAaImAgMnS	být
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
následovaný	následovaný	k2eAgMnSc1d1	následovaný
MiGem-	MiGem-	k1gMnSc1	MiGem-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1965	[number]	k4	1965
byly	být	k5eAaImAgFnP	být
dodány	dodat	k5eAaPmNgFnP	dodat
první	první	k4xOgFnPc1	první
dvoumístné	dvoumístný	k2eAgFnPc1d1	dvoumístná
MiG-	MiG-	k1gFnPc1	MiG-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
Rothenburg-Oberlausitz	Rothenburg-Oberlausitza	k1gFnPc2	Rothenburg-Oberlausitza
a	a	k8xC	a
dodávky	dodávka	k1gFnPc4	dodávka
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
počet	počet	k1gInSc4	počet
45	[number]	k4	45
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Modernější	moderní	k2eAgFnSc3d2	modernější
dvoumístné	dvoumístný	k2eAgFnSc3d1	dvoumístná
MiGy-	MiGy-	k1gFnSc3	MiGy-
<g/>
21	[number]	k4	21
<g/>
UM	um	k1gInSc1	um
byly	být	k5eAaImAgFnP	být
dodány	dodat	k5eAaPmNgInP	dodat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1970	[number]	k4	1970
-	-	kIx~	-
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byly	být	k5eAaImAgInP	být
doobjednané	doobjednaný	k2eAgInPc1d1	doobjednaný
3	[number]	k4	3
ks	ks	kA	ks
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Skoro	skoro	k6eAd1	skoro
všechny	všechen	k3xTgFnPc1	všechen
dvoumístné	dvoumístný	k2eAgFnPc1d1	dvoumístná
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
<g/>
/	/	kIx~	/
<g/>
UM	uma	k1gFnPc2	uma
byly	být	k5eAaImAgInP	být
vyřazeny	vyřadit	k5eAaPmNgInP	vyřadit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1968	[number]	k4	1968
začaly	začít	k5eAaPmAgFnP	začít
dodávky	dodávka	k1gFnPc1	dodávka
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
trvaly	trvat	k5eAaImAgInP	trvat
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
87	[number]	k4	87
ks	ks	kA	ks
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
bylo	být	k5eAaImAgNnS	být
12	[number]	k4	12
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
odprodáno	odprodat	k5eAaPmNgNnS	odprodat
Sýrii	Sýrie	k1gFnSc3	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1972	[number]	k4	1972
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
50	[number]	k4	50
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
následované	následovaný	k2eAgNnSc1d1	následované
dalšími	další	k2eAgMnPc7d1	další
12	[number]	k4	12
ks	ks	kA	ks
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
začaly	začít	k5eAaPmAgInP	začít
dodavky	dodavek	k1gInPc1	dodavek
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
14	[number]	k4	14
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgInPc2d1	poslední
23	[number]	k4	23
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
přes	přes	k7c4	přes
250	[number]	k4	250
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
různých	různý	k2eAgFnPc2d1	různá
verzí	verze	k1gFnPc2	verze
převedených	převedený	k2eAgFnPc2d1	převedená
do	do	k7c2	do
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
<g/>
.	.	kIx.	.
<g/>
Eritrea	Eritrea	k1gFnSc1	Eritrea
Eritrea	Eritrea	k1gFnSc1	Eritrea
</s>
</p>
<p>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
EtiopieEtiopské	EtiopieEtiopský	k2eAgNnSc1d1	EtiopieEtiopský
letectvo	letectvo	k1gNnSc1	letectvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
získalo	získat	k5eAaPmAgNnS	získat
48	[number]	k4	48
ks	ks	kA	ks
jednomístných	jednomístný	k2eAgFnPc2d1	jednomístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
několik	několik	k4yIc4	několik
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
byly	být	k5eAaImAgFnP	být
etiopské	etiopský	k2eAgFnPc1d1	etiopská
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
dvěma	dva	k4xCgFnPc7	dva
eskadry	eskadra	k1gFnPc4	eskadra
MiGů-	MiGů-	k1gFnSc4	MiGů-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stroje	stroj	k1gInPc1	stroj
a	a	k8xC	a
piloti	pilot	k1gMnPc1	pilot
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
eskader	eskadra	k1gFnPc2	eskadra
byli	být	k5eAaImAgMnP	být
vysláni	vyslat	k5eAaPmNgMnP	vyslat
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
stíhačky	stíhačka	k1gFnPc1	stíhačka
byly	být	k5eAaImAgFnP	být
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
se	s	k7c7	s
Somálskem	Somálsko	k1gNnSc7	Somálsko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
obě	dva	k4xCgFnPc1	dva
bojující	bojující	k2eAgFnPc1d1	bojující
strany	strana	k1gFnPc1	strana
použily	použít	k5eAaPmAgFnP	použít
své	svůj	k3xOyFgFnPc1	svůj
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dodávky	dodávka	k1gFnPc1	dodávka
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1982	[number]	k4	1982
-	-	kIx~	-
3	[number]	k4	3
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
přes	přes	k7c4	přes
100	[number]	k4	100
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
,	,	kIx,	,
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
a	a	k8xC	a
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
dodávky	dodávka	k1gFnPc1	dodávka
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1986	[number]	k4	1986
-	-	kIx~	-
8	[number]	k4	8
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
30	[number]	k4	30
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
bylo	být	k5eAaImAgNnS	být
10	[number]	k4	10
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
poslaných	poslaný	k2eAgMnPc2d1	poslaný
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
podrobit	podrobit	k5eAaPmF	podrobit
modernizaci	modernizace	k1gFnSc4	modernizace
na	na	k7c4	na
verzi	verze	k1gFnSc4	verze
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
Lancer	Lancra	k1gFnPc2	Lancra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedostatek	nedostatek	k1gInSc1	nedostatek
financí	finance	k1gFnPc2	finance
to	ten	k3xDgNnSc4	ten
nakonec	nakonec	k6eAd1	nakonec
neumožnil	umožnit	k5eNaPmAgInS	umožnit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
měla	mít	k5eAaImAgFnS	mít
Etiopie	Etiopie	k1gFnSc1	Etiopie
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
2	[number]	k4	2
regimenty	regiment	k1gInPc1	regiment
vybaveny	vybaven	k2eAgInPc1d1	vybaven
18	[number]	k4	18
ks	ks	kA	ks
jednomístných	jednomístný	k2eAgInPc2d1	jednomístný
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
a	a	k8xC	a
6	[number]	k4	6
ks	ks	kA	ks
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
Finsko	Finsko	k1gNnSc1	Finsko
</s>
</p>
<p>
<s>
Finské	finský	k2eAgNnSc1d1	finské
letectvo	letectvo	k1gNnSc1	letectvo
počátkem	počátkem	k7c2	počátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
stroje	stroj	k1gInPc4	stroj
De	De	k?	De
Havilland	Havilland	k1gInSc1	Havilland
Vampire	Vampir	k1gInSc5	Vampir
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
MiGy-	MiGy-	k1gFnPc7	MiGy-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgFnSc7	první
nekomunistickou	komunistický	k2eNgFnSc7d1	nekomunistická
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
získala	získat	k5eAaPmAgFnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
10	[number]	k4	10
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnPc2	F-
<g/>
13	[number]	k4	13
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
další	další	k2eAgFnSc7d1	další
dodávkou	dodávka	k1gFnSc7	dodávka
11	[number]	k4	11
ks	ks	kA	ks
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
dodávek	dodávka	k1gFnPc2	dodávka
Finové	Fin	k1gMnPc1	Fin
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objednaný	objednaný	k2eAgInSc1d1	objednaný
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
nadbytečný	nadbytečný	k2eAgInSc1d1	nadbytečný
a	a	k8xC	a
proto	proto	k8xC	proto
několik	několik	k4yIc1	několik
MiGů-	MiGů-	k1gMnPc2	MiGů-
<g/>
21	[number]	k4	21
uložili	uložit	k5eAaPmAgMnP	uložit
na	na	k7c4	na
základně	základně	k6eAd1	základně
Rovaniemi	Rovanie	k1gFnPc7	Rovanie
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
novějších	nový	k2eAgInPc2d2	novější
strojů	stroj	k1gInPc2	stroj
Saab	Saab	k1gInSc1	Saab
35BS	[number]	k4	35BS
Draken	Drakna	k1gFnPc2	Drakna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvousedadlové	dvousedadlový	k2eAgFnPc4d1	dvousedadlová
MiGy-	MiGy-	k1gFnPc4	MiGy-
<g/>
21	[number]	k4	21
<g/>
U	u	k7c2	u
dorazily	dorazit	k5eAaPmAgInP	dorazit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
modernizovanými	modernizovaný	k2eAgMnPc7d1	modernizovaný
dvoumístnými	dvoumístný	k2eAgMnPc7d1	dvoumístný
MiG-	MiG-	k1gMnPc7	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	um	k1gInSc4	um
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1971	[number]	k4	1971
-	-	kIx~	-
1973	[number]	k4	1973
bylo	být	k5eAaImAgNnS	být
9	[number]	k4	9
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
překonstruováno	překonstruovat	k5eAaPmNgNnS	překonstruovat
na	na	k7c4	na
průzkumnou	průzkumný	k2eAgFnSc4d1	průzkumná
verzi	verze	k1gFnSc4	verze
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poslední	poslední	k2eAgInSc4d1	poslední
let	let	k1gInSc4	let
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
ve	v	k7c6	v
finských	finský	k2eAgFnPc6d1	finská
službách	služba	k1gFnPc6	služba
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
HävLLv	HävLLv	k1gInSc1	HävLLv
31	[number]	k4	31
začala	začít	k5eAaPmAgFnS	začít
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
své	svůj	k3xOyFgFnPc4	svůj
MiGy-	MiGy-	k1gFnPc4	MiGy-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnPc2	F-
<g/>
13	[number]	k4	13
verzí	verze	k1gFnPc2	verze
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dodávky	dodávka	k1gFnPc1	dodávka
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
Finsko	Finsko	k1gNnSc1	Finsko
získalo	získat	k5eAaPmAgNnS	získat
26	[number]	k4	26
ks	ks	kA	ks
modelu	model	k1gInSc2	model
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
upravených	upravený	k2eAgFnPc2d1	upravená
na	na	k7c4	na
průzkumnou	průzkumný	k2eAgFnSc4d1	průzkumná
verzi	verze	k1gFnSc4	verze
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bisT	bisT	k?	bisT
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
byl	být	k5eAaImAgInS	být
dodán	dodat	k5eAaPmNgMnS	dodat
i	i	k9	i
jeden	jeden	k4xCgMnSc1	jeden
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
finské	finský	k2eAgFnPc1d1	finská
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
byly	být	k5eAaImAgInP	být
upraveny	upravit	k5eAaPmNgInP	upravit
instalací	instalace	k1gFnSc7	instalace
západní	západní	k2eAgFnSc2d1	západní
avioniky	avionika	k1gFnSc2	avionika
<g/>
,	,	kIx,	,
navigačního	navigační	k2eAgInSc2d1	navigační
systému	systém	k1gInSc2	systém
a	a	k8xC	a
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
kariéry	kariéra	k1gFnSc2	kariéra
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
ve	v	k7c6	v
finských	finský	k2eAgFnPc6d1	finská
službách	služba	k1gFnPc6	služba
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
nákupu	nákup	k1gInSc6	nákup
stíhaček	stíhačka	k1gFnPc2	stíhačka
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
A-	A-	k1gFnSc1	A-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Kuopio-Rissala	Kuopio-Rissala	k1gFnSc2	Kuopio-Rissala
uspořádána	uspořádán	k2eAgFnSc1d1	uspořádána
velká	velký	k2eAgFnSc1d1	velká
rozlučková	rozlučkový	k2eAgFnSc1d1	rozlučková
oslava	oslava	k1gFnSc1	oslava
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
byl	být	k5eAaImAgInS	být
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
exemplářů	exemplář	k1gInPc2	exemplář
pro	pro	k7c4	pro
muzea	muzeum	k1gNnPc4	muzeum
sešrotován	sešrotován	k2eAgInSc4d1	sešrotován
<g/>
.	.	kIx.	.
<g/>
Gruzie	Gruzie	k1gFnSc1	Gruzie
Gruzie	Gruzie	k1gFnSc2	Gruzie
</s>
</p>
<p>
<s>
Guinea-Bissau	Guinea-Bissa	k2eAgFnSc4d1	Guinea-Bissa
Guinea-Bissau	Guinea-Bissa	k2eAgFnSc4d1	Guinea-Bissa
Guinea-BissauGuinea-Bissau	Guinea-BissauGuinea-Bissaa	k1gFnSc4	Guinea-BissauGuinea-Bissaa
získala	získat	k5eAaPmAgFnS	získat
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c2	za
staré	starý	k2eAgFnSc2d1	stará
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
dodán	dodán	k2eAgMnSc1d1	dodán
jeden	jeden	k4xCgMnSc1	jeden
dvoumístný	dvoumístný	k2eAgMnSc1d1	dvoumístný
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	um	k1gInSc1	um
<g/>
.	.	kIx.	.
<g/>
Indonésie	Indonésie	k1gFnSc1	Indonésie
Indonésie	Indonésie	k1gFnSc1	Indonésie
</s>
</p>
<p>
<s>
Indonéské	indonéský	k2eAgNnSc1d1	indonéské
letectvo	letectvo	k1gNnSc1	letectvo
se	se	k3xPyFc4	se
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
na	na	k7c6	na
SSSR	SSSR	kA	SSSR
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
na	na	k7c4	na
dodávky	dodávka	k1gFnPc4	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
dodáno	dodat	k5eAaPmNgNnS	dodat
20	[number]	k4	20
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gMnPc2	F-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
nasazeny	nasadit	k5eAaPmNgInP	nasadit
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
konfliktech	konflikt	k1gInPc6	konflikt
ve	v	k7c6	v
kterých	který	k3yIgFnPc2	který
Indonésie	Indonésie	k1gFnSc1	Indonésie
bojovala	bojovat	k5eAaImAgFnS	bojovat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Malajsií	Malajsie	k1gFnSc7	Malajsie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
začala	začít	k5eAaPmAgFnS	začít
Indonésie	Indonésie	k1gFnSc1	Indonésie
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
malá	malý	k2eAgFnSc1d1	malá
flotila	flotila	k1gFnSc1	flotila
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
náhradních	náhradní	k2eAgInPc2d1	náhradní
dílů	díl	k1gInPc2	díl
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
mimo	mimo	k7c4	mimo
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
<g/>
Írán	Írán	k1gInSc4	Írán
ÍránÍránské	ÍránÍránský	k2eAgNnSc1d1	ÍránÍránský
vojenské	vojenský	k2eAgNnSc1d1	vojenské
letectvo	letectvo	k1gNnSc1	letectvo
zakoupilo	zakoupit	k5eAaPmAgNnS	zakoupit
12	[number]	k4	12
východoněmeckých	východoněmecký	k2eAgFnPc2d1	východoněmecká
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
plus	plus	k6eAd1	plus
čtyři	čtyři	k4xCgFnPc4	čtyři
MiGy-	MiGy-	k1gFnPc4	MiGy-
<g/>
21	[number]	k4	21
<g/>
U.	U.	kA	U.
Nicméně	nicméně	k8xC	nicméně
byly	být	k5eAaImAgFnP	být
dodány	dodat	k5eAaPmNgInP	dodat
jen	jen	k9	jen
dva	dva	k4xCgInPc1	dva
MiGy-	MiGy-	k1gFnPc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
po	po	k7c6	po
znovusjednocení	znovusjednocení	k1gNnSc6	znovusjednocení
Německa	Německo	k1gNnSc2	Německo
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
ostatní	ostatní	k2eAgInPc4d1	ostatní
stroje	stroj	k1gInPc4	stroj
uvaleno	uvalen	k2eAgNnSc4d1	uvaleno
embargo	embargo	k1gNnSc4	embargo
<g/>
.	.	kIx.	.
<g/>
Irák	Irák	k1gInSc1	Irák
Irák	Irák	k1gInSc1	Irák
</s>
</p>
<p>
<s>
Irácké	irácký	k2eAgNnSc1d1	irácké
letectvo	letectvo	k1gNnSc1	letectvo
získalo	získat	k5eAaPmAgNnS	získat
první	první	k4xOgFnSc4	první
dodávku	dodávka	k1gFnSc4	dodávka
35	[number]	k4	35
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1966	[number]	k4	1966
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
dezertoval	dezertovat	k5eAaBmAgInS	dezertovat
kpt.	kpt.	k?	kpt.
Munír	Munír	k1gInSc1	Munír
Rúfá	Rúfá	k1gFnSc1	Rúfá
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgInPc1d1	následovaný
dalšími	další	k2eAgInPc7d1	další
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
měl	mít	k5eAaImAgInS	mít
Irák	Irák	k1gInSc1	Irák
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
90	[number]	k4	90
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
Irák	Irák	k1gInSc1	Irák
55	[number]	k4	55
ks	ks	kA	ks
stíhaček	stíhačka	k1gFnPc2	stíhačka
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
předáno	předat	k5eAaPmNgNnS	předat
Egyptu	Egypt	k1gInSc3	Egypt
a	a	k8xC	a
Sýrii	Sýrie	k1gFnSc3	Sýrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahradily	nahradit	k5eAaPmAgFnP	nahradit
ztráty	ztráta	k1gFnPc4	ztráta
utrpěné	utrpěný	k2eAgFnPc4d1	utrpěná
během	běh	k1gInSc7	běh
šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jomkipurské	jomkipurský	k2eAgFnSc2d1	jomkipurská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
bylo	být	k5eAaImAgNnS	být
18	[number]	k4	18
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
odesláno	odeslán	k2eAgNnSc1d1	odesláno
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
Al-Mezzah	Al-Mezzaha	k1gFnPc2	Al-Mezzaha
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
40	[number]	k4	40
ks	ks	kA	ks
modelu	model	k1gInSc2	model
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
první	první	k4xOgInPc1	první
kusy	kus	k1gInPc1	kus
byly	být	k5eAaImAgInP	být
předány	předat	k5eAaPmNgInP	předat
Sýrii	Sýrie	k1gFnSc3	Sýrie
a	a	k8xC	a
Egyptu	Egypt	k1gInSc3	Egypt
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
utrpěných	utrpěný	k2eAgFnPc2d1	utrpěná
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
bylo	být	k5eAaImAgNnS	být
zakoupených	zakoupený	k2eAgInPc2d1	zakoupený
15	[number]	k4	15
ks	ks	kA	ks
průzkumných	průzkumný	k2eAgFnPc2d1	průzkumná
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
MiGem-	MiGem-	k1gFnSc7	MiGem-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
zúčastnily	zúčastnit	k5eAaPmAgInP	zúčastnit
války	válek	k1gInPc1	válek
s	s	k7c7	s
Íránem	Írán	k1gInSc7	Írán
a	a	k8xC	a
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
dostal	dostat	k5eAaPmAgInS	dostat
Irák	Irák	k1gInSc1	Irák
61	[number]	k4	61
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
íránsko-irácké	íránskorácký	k2eAgFnPc1d1	íránsko-irácká
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
irácké	irácký	k2eAgFnSc6d1	irácká
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
operaci	operace	k1gFnSc6	operace
Pouštní	pouštní	k2eAgFnSc2d1	pouštní
bouře	bouř	k1gFnSc2	bouř
bylo	být	k5eAaImAgNnS	být
uvaleno	uvalit	k5eAaPmNgNnS	uvalit
na	na	k7c4	na
Irák	Irák	k1gInSc4	Irák
zbraňové	zbraňový	k2eAgNnSc1d1	zbraňové
embargo	embargo	k1gNnSc1	embargo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
bránilo	bránit	k5eAaImAgNnS	bránit
v	v	k7c6	v
nákupu	nákup	k1gInSc6	nákup
dalších	další	k2eAgInPc2d1	další
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mělo	mít	k5eAaImAgNnS	mít
irácké	irácký	k2eAgNnSc1d1	irácké
letectvo	letectvo	k1gNnSc1	letectvo
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
kolem	kolem	k7c2	kolem
100	[number]	k4	100
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
a	a	k8xC	a
čínských	čínský	k2eAgFnPc6d1	čínská
F-	F-	k1gFnPc6	F-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přežily	přežít	k5eAaPmAgFnP	přežít
tuto	tento	k3xDgFnSc4	tento
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
během	během	k7c2	během
americké	americký	k2eAgFnSc2d1	americká
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
JugoslávieJugoslávské	JugoslávieJugoslávský	k2eAgNnSc1d1	JugoslávieJugoslávský
letectvo	letectvo	k1gNnSc1	letectvo
získalo	získat	k5eAaPmAgNnS	získat
své	své	k1gNnSc4	své
první	první	k4xOgFnSc2	první
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
vyřazení	vyřazení	k1gNnSc6	vyřazení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
zbývající	zbývající	k2eAgFnPc1d1	zbývající
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
začleněny	začleněn	k2eAgFnPc4d1	začleněna
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
federální	federální	k2eAgFnSc2d1	federální
republiky	republika	k1gFnSc2	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
odtrhlo	odtrhnout	k5eAaPmAgNnS	odtrhnout
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
a	a	k8xC	a
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
dvoumístné	dvoumístný	k2eAgFnPc1d1	dvoumístná
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
modernějšími	moderní	k2eAgFnPc7d2	modernější
MiGy-	MiGy-	k1gFnPc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Vyřazení	vyřazení	k1gNnSc1	vyřazení
MiGů-	MiGů-	k1gFnSc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
zařazen	zařazen	k2eAgInSc4d1	zařazen
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
průzkumným	průzkumný	k2eAgMnSc7d1	průzkumný
MiGem-	MiGem-	k1gMnSc7	MiGem-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vyřazen	vyřazen	k2eAgMnSc1d1	vyřazen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
zaveden	zavést	k5eAaPmNgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
a	a	k8xC	a
model	model	k1gInSc1	model
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
obstarala	obstarat	k5eAaPmAgFnS	obstarat
přes	přes	k7c4	přes
260	[number]	k4	260
ks	ks	kA	ks
různých	různý	k2eAgFnPc2d1	různá
verzí	verze	k1gFnPc2	verze
MiGu-	MiGu-	k1gMnPc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
společenství	společenství	k1gNnSc6	společenství
států	stát	k1gInPc2	stát
Srbsko	Srbsko	k1gNnSc1	Srbsko
a	a	k8xC	a
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
na	na	k7c4	na
Srbsko	Srbsko	k1gNnSc4	Srbsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
si	se	k3xPyFc3	se
zároveň	zároveň	k6eAd1	zároveň
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
zbývající	zbývající	k2eAgFnPc4d1	zbývající
MiGy-	MiGy-	k1gFnPc4	MiGy-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
Kambodža	Kambodža	k1gFnSc1	Kambodža
KambodžaKambodžské	KambodžaKambodžský	k2eAgNnSc1d1	KambodžaKambodžský
královské	královský	k2eAgNnSc1d1	královské
letectvo	letectvo	k1gNnSc1	letectvo
získalo	získat	k5eAaPmAgNnS	získat
19	[number]	k4	19
ks	ks	kA	ks
používaných	používaný	k2eAgFnPc2d1	používaná
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
3	[number]	k4	3
ks	ks	kA	ks
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatečné	dostatečný	k2eNgFnSc3d1	nedostatečná
údržbě	údržba	k1gFnSc3	údržba
a	a	k8xC	a
nedostatku	nedostatek	k1gInSc2	nedostatek
náhradních	náhradní	k2eAgInPc2d1	náhradní
dílů	díl	k1gInPc2	díl
tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
často	často	k6eAd1	často
nelétaly	létat	k5eNaImAgInP	létat
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1992	[number]	k4	1992
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
plánu	plán	k1gInSc2	plán
OSN	OSN	kA	OSN
na	na	k7c6	na
odzbrojení	odzbrojení	k1gNnSc6	odzbrojení
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
režimu	režim	k1gInSc2	režim
Rudých	rudý	k2eAgMnPc2d1	rudý
Khmerů	Khmer	k1gMnPc2	Khmer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
podepsala	podepsat	k5eAaPmAgFnS	podepsat
vláda	vláda	k1gFnSc1	vláda
Kambodže	Kambodža	k1gFnSc2	Kambodža
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c4	o
modernizaci	modernizace	k1gFnSc4	modernizace
12	[number]	k4	12
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
na	na	k7c4	na
verzi	verze	k1gFnSc4	verze
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
kvůli	kvůli	k7c3	kvůli
insolvenčnosti	insolvenčnost	k1gFnSc3	insolvenčnost
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
.	.	kIx.	.
<g/>
Kongo	Kongo	k1gNnSc1	Kongo
(	(	kIx(	(
<g/>
Brazzaville	Brazzaville	k1gInSc1	Brazzaville
<g/>
)	)	kIx)	)
Kongo	Kongo	k1gNnSc1	Kongo
(	(	kIx(	(
<g/>
Brazzaville	Brazzaville	k1gInSc1	Brazzaville
<g/>
)	)	kIx)	)
<g/>
Konžské	konžský	k2eAgNnSc1d1	konžské
letectvo	letectvo	k1gNnSc1	letectvo
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
několik	několik	k4yIc4	několik
MiGů-	MiGů-	k1gMnSc7	MiGů-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
následovaných	následovaný	k2eAgFnPc2d1	následovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
dvěma	dva	k4xCgInPc7	dva
dvoumístnými	dvoumístný	k2eAgInPc7d1	dvoumístný
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
mělo	mít	k5eAaImAgNnS	mít
letectvo	letectvo	k1gNnSc1	letectvo
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
16	[number]	k4	16
jednomístných	jednomístný	k2eAgMnPc2d1	jednomístný
a	a	k8xC	a
2	[number]	k4	2
dvoumístné	dvoumístný	k2eAgFnSc2d1	dvoumístná
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
však	však	k9	však
již	již	k6eAd1	již
nebyly	být	k5eNaImAgFnP	být
letuschopné	letuschopný	k2eAgFnPc1d1	letuschopná
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Pointe	Pointe	k?	Pointe
Noire	Noir	k1gInSc5	Noir
<g/>
.	.	kIx.	.
<g/>
Kyrgyzstán	Kyrgyzstán	k2eAgInSc1d1	Kyrgyzstán
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
</s>
</p>
<p>
<s>
Laos	Laos	k1gInSc1	Laos
LaosLaos	LaosLaosa	k1gFnPc2	LaosLaosa
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
několik	několik	k4yIc4	několik
MiGů-	MiGů-	k1gMnSc7	MiGů-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
pomoci	pomoc	k1gFnSc2	pomoc
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc2	Vietnam
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začala	začít	k5eAaPmAgFnS	začít
letuschopnost	letuschopnost	k1gFnSc1	letuschopnost
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
rychle	rychle	k6eAd1	rychle
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
modernizačních	modernizační	k2eAgInPc2d1	modernizační
programů	program	k1gInPc2	program
indické	indický	k2eAgFnSc2d1	indická
firmy	firma	k1gFnSc2	firma
HAL	hala	k1gFnPc2	hala
selhalo	selhat	k5eAaPmAgNnS	selhat
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
olétanosti	olétanost	k1gFnSc6	olétanost
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
<g/>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
MadagaskarMadagaskar	MadagaskarMadagaskara	k1gFnPc2	MadagaskarMadagaskara
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
8	[number]	k4	8
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
a	a	k8xC	a
1	[number]	k4	1
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
U.	U.	kA	U.
Později	pozdě	k6eAd2	pozdě
SSSR	SSSR	kA	SSSR
dodal	dodat	k5eAaPmAgInS	dodat
12	[number]	k4	12
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byly	být	k5eAaImAgFnP	být
madagaskarské	madagaskarský	k2eAgFnPc1d1	madagaskarská
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
uložené	uložený	k2eAgFnSc2d1	uložená
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Ivato	Ivato	k1gNnSc4	Ivato
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Antananarivo	Antananarivo	k1gNnSc1	Antananarivo
<g/>
.	.	kIx.	.
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
</s>
</p>
<p>
<s>
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
letectvo	letectvo	k1gNnSc1	letectvo
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgFnSc7	první
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
získaly	získat	k5eAaPmAgInP	získat
12	[number]	k4	12
ks	ks	kA	ks
modelu	model	k1gInSc2	model
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gMnPc2	F-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
dalších	další	k2eAgInPc2d1	další
68	[number]	k4	68
ks	ks	kA	ks
stejného	stejný	k2eAgInSc2d1	stejný
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvoumístné	dvoumístný	k2eAgFnPc4d1	dvoumístná
MiG-	MiG-	k1gFnPc4	MiG-
<g/>
21	[number]	k4	21
<g/>
U	u	k7c2	u
dorazily	dorazit	k5eAaPmAgInP	dorazit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
a	a	k8xC	a
dodávky	dodávka	k1gFnPc1	dodávka
včetně	včetně	k7c2	včetně
vylepšených	vylepšený	k2eAgFnPc2d1	vylepšená
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
všechny	všechen	k3xTgFnPc4	všechen
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
24	[number]	k4	24
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poslední	poslední	k2eAgInPc4d1	poslední
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
modelů	model	k1gInPc2	model
byl	být	k5eAaImAgInS	být
vyřazen	vyřadit	k5eAaPmNgInS	vyřadit
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgInPc1	první
stroje	stroj	k1gInPc1	stroj
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
staly	stát	k5eAaPmAgFnP	stát
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
verzí	verze	k1gFnSc7	verze
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
v	v	k7c6	v
maďarských	maďarský	k2eAgFnPc6d1	maďarská
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
dislokované	dislokovaný	k2eAgFnPc1d1	dislokovaná
na	na	k7c6	na
základnách	základna	k1gFnPc6	základna
Taszar	Taszar	k1gInSc1	Taszar
a	a	k8xC	a
Pápa	pápa	k1gMnSc1	pápa
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
novějších	nový	k2eAgFnPc2d2	novější
stíhaček	stíhačka	k1gFnPc2	stíhačka
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
byly	být	k5eAaImAgInP	být
starší	starý	k2eAgInPc1d2	starší
modely	model	k1gInPc1	model
uloženy	uložen	k2eAgInPc1d1	uložen
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Kecskemét	Kecskeméta	k1gFnPc2	Kecskeméta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
stále	stále	k6eAd1	stále
disponovalo	disponovat	k5eAaBmAgNnS	disponovat
velkou	velký	k2eAgFnSc7d1	velká
flotilou	flotila	k1gFnSc7	flotila
MiGů-	MiGů-	k1gFnSc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zanedlouho	zanedlouho	k6eAd1	zanedlouho
začalo	začít	k5eAaPmAgNnS	začít
čelit	čelit	k5eAaImF	čelit
vážným	vážný	k2eAgInPc3d1	vážný
finančním	finanční	k2eAgInPc3d1	finanční
problémům	problém	k1gInPc3	problém
a	a	k8xC	a
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
počty	počet	k1gInPc1	počet
stíhaček	stíhačka	k1gFnPc2	stíhačka
drasticky	drasticky	k6eAd1	drasticky
redukovány	redukován	k2eAgFnPc1d1	redukována
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
sešrotovat	sešrotovat	k5eAaPmF	sešrotovat
několik	několik	k4yIc4	několik
MiGů-	MiGů-	k1gFnSc2	MiGů-
<g/>
21	[number]	k4	21
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
přesunout	přesunout	k5eAaPmF	přesunout
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
Pápa	pápa	k1gMnSc1	pápa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
i	i	k9	i
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
MiGy-	MiGy-	k1gFnSc1	MiGy-
<g/>
21	[number]	k4	21
byly	být	k5eAaImAgInP	být
vyřazeny	vyřazen	k2eAgInPc1d1	vyřazen
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
letuschopných	letuschopný	k2eAgMnPc2d1	letuschopný
ještě	ještě	k6eAd1	ještě
12	[number]	k4	12
ks	ks	kA	ks
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
10	[number]	k4	10
ks	ks	kA	ks
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
U.	U.	kA	U.
<g/>
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
MongolskoMongolské	mongolskomongolský	k2eAgNnSc1d1	mongolskomongolský
letectvo	letectvo	k1gNnSc1	letectvo
získalo	získat	k5eAaPmAgNnS	získat
koncem	konec	k1gInSc7	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
zhruba	zhruba	k6eAd1	zhruba
12	[number]	k4	12
ks	ks	kA	ks
jednomístných	jednomístný	k2eAgFnPc2d1	jednomístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
a	a	k8xC	a
několik	několik	k4yIc4	několik
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
U.	U.	kA	U.
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
podpory	podpora	k1gFnSc2	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
uzemněny	uzemněn	k2eAgInPc1d1	uzemněn
a	a	k8xC	a
uskladněny	uskladněn	k2eAgInPc1d1	uskladněn
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
zakoupilo	zakoupit	k5eAaPmAgNnS	zakoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
jisté	jistý	k2eAgNnSc1d1	jisté
množství	množství	k1gNnSc1	množství
náhradních	náhradní	k2eAgInPc2d1	náhradní
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
znamenat	znamenat	k5eAaImF	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mongolské	mongolský	k2eAgFnSc2d1	mongolská
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
jsou	být	k5eAaImIp3nP	být
částečně	částečně	k6eAd1	částečně
letuschopné	letuschopný	k2eAgFnPc4d1	letuschopná
<g/>
.	.	kIx.	.
<g/>
Namibie	Namibie	k1gFnPc4	Namibie
NamibieNamibijské	NamibieNamibijský	k2eAgFnSc2d1	NamibieNamibijský
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
síly	síla	k1gFnSc2	síla
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
první	první	k4xOgFnSc4	první
2	[number]	k4	2
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
1	[number]	k4	1
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
byly	být	k5eAaImAgFnP	být
doručeny	doručit	k5eAaPmNgFnP	doručit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
po	po	k7c6	po
důkladné	důkladný	k2eAgFnSc6d1	důkladná
opravě	oprava	k1gFnSc6	oprava
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
firmou	firma	k1gFnSc7	firma
IAI	IAI	kA	IAI
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
navíc	navíc	k6eAd1	navíc
pořízeno	pořídit	k5eAaPmNgNnS	pořídit
12	[number]	k4	12
ks	ks	kA	ks
F-	F-	k1gFnSc2	F-
<g/>
7	[number]	k4	7
<g/>
Nm	Nm	k1gMnPc2	Nm
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
FT-7NG	FT-7NG	k1gFnPc2	FT-7NG
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
<g/>
Německo	Německo	k1gNnSc1	Německo
NěmeckoLuftwaffe	NěmeckoLuftwaff	k1gInSc5	NěmeckoLuftwaff
ještě	ještě	k9	ještě
před	před	k7c7	před
sjednocením	sjednocení	k1gNnSc7	sjednocení
Německa	Německo	k1gNnSc2	Německo
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
používat	používat	k5eAaImF	používat
žádné	žádný	k3yNgFnPc4	žádný
MiGy-	MiGy-	k1gFnPc4	MiGy-
<g/>
21	[number]	k4	21
z	z	k7c2	z
výzbroje	výzbroj	k1gFnSc2	výzbroj
letectva	letectvo	k1gNnSc2	letectvo
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
šrotování	šrotování	k1gNnSc1	šrotování
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
září	září	k1gNnSc6	září
1992	[number]	k4	1992
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
zachovány	zachovat	k5eAaPmNgInP	zachovat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
prodány	prodat	k5eAaPmNgFnP	prodat
soukromým	soukromý	k2eAgFnPc3d1	soukromá
osobám	osoba	k1gFnPc3	osoba
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
zahraničí	zahraničí	k1gNnSc6	zahraničí
nebo	nebo	k8xC	nebo
do	do	k7c2	do
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
.	.	kIx.	.
<g/>
Nigérie	Nigérie	k1gFnSc1	Nigérie
NigérieNigerijské	NigérieNigerijský	k2eAgFnSc2d1	NigérieNigerijský
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
síly	síla	k1gFnSc2	síla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
zakoupily	zakoupit	k5eAaPmAgInP	zakoupit
25	[number]	k4	25
ks	ks	kA	ks
MiGů-	MiGů-	k1gFnSc1	MiGů-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
6	[number]	k4	6
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byly	být	k5eAaImAgInP	být
zveřejněny	zveřejněn	k2eAgInPc1d1	zveřejněn
plány	plán	k1gInPc1	plán
modernizace	modernizace	k1gFnSc2	modernizace
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
podobného	podobný	k2eAgMnSc2d1	podobný
neudálo	udát	k5eNaPmAgNnS	udát
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
proto	proto	k6eAd1	proto
uloženy	uložit	k5eAaPmNgInP	uložit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
zakoupily	zakoupit	k5eAaPmAgFnP	zakoupit
nigerijské	nigerijský	k2eAgFnPc1d1	nigerijská
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
12	[number]	k4	12
ks	ks	kA	ks
F-7NI	F-7NI	k1gFnPc2	F-7NI
a	a	k8xC	a
3	[number]	k4	3
ks	ks	kA	ks
FT-7NI	FT-7NI	k1gFnPc2	FT-7NI
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
Polské	polský	k2eAgNnSc1d1	polské
letectvo	letectvo	k1gNnSc1	letectvo
získalo	získat	k5eAaPmAgNnS	získat
své	své	k1gNnSc4	své
první	první	k4xOgFnSc2	první
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trvalo	trvat	k5eAaImAgNnS	trvat
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
než	než	k8xS	než
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
první	první	k4xOgFnPc1	první
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnPc2	F-
<g/>
13	[number]	k4	13
dislokovány	dislokovat	k5eAaBmNgInP	dislokovat
na	na	k7c4	na
jedinou	jediný	k2eAgFnSc4d1	jediná
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
vyřazení	vyřazení	k1gNnSc2	vyřazení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgInPc2d1	poslední
12	[number]	k4	12
ks	ks	kA	ks
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
prodáno	prodat	k5eAaPmNgNnS	prodat
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahradily	nahradit	k5eAaPmAgFnP	nahradit
ztráty	ztráta	k1gFnPc4	ztráta
utrpěné	utrpěný	k2eAgFnPc4d1	utrpěná
během	během	k7c2	během
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
začaly	začít	k5eAaPmAgFnP	začít
dodávky	dodávka	k1gFnPc1	dodávka
modelu	model	k1gInSc2	model
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
trvaly	trvat	k5eAaImAgInP	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
a	a	k8xC	a
poslední	poslední	k2eAgInPc4d1	poslední
kusy	kus	k1gInPc4	kus
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
11	[number]	k4	11
ks	ks	kA	ks
dvoumístných	dvoumístný	k2eAgFnPc2d1	dvoumístná
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vyřazeny	vyřazen	k2eAgFnPc1d1	vyřazena
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1966	[number]	k4	1966
-	-	kIx~	-
1968	[number]	k4	1968
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
162	[number]	k4	162
ks	ks	kA	ks
modelu	model	k1gInSc2	model
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgMnSc1	první
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
vyřazovány	vyřazovat	k5eAaImNgInP	vyřazovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
byl	být	k5eAaImAgInS	být
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1968	[number]	k4	1968
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
12	[number]	k4	12
ks	ks	kA	ks
průzkumných	průzkumný	k2eAgFnPc2d1	průzkumná
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
MiGy-	MiGy-	k1gFnSc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dvoumístnými	dvoumístný	k2eAgMnPc7d1	dvoumístný
cvičnými	cvičný	k2eAgMnPc7d1	cvičný
MiG-	MiG-	k1gMnPc7	MiG-
<g/>
21	[number]	k4	21
<g/>
US	US	kA	US
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
cvičné	cvičný	k2eAgInPc1d1	cvičný
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
vyřazeny	vyřadit	k5eAaPmNgInP	vyřadit
počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
M	M	kA	M
sloužil	sloužit	k5eAaImAgInS	sloužit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Nejmodernější	moderní	k2eAgMnSc1d3	nejmodernější
cvičný	cvičný	k2eAgMnSc1d1	cvičný
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	um	k1gInSc1	um
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
-	-	kIx~	-
1981	[number]	k4	1981
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
získalo	získat	k5eAaPmAgNnS	získat
polské	polský	k2eAgNnSc1d1	polské
letectvo	letectvo	k1gNnSc1	letectvo
54	[number]	k4	54
ks	ks	kA	ks
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Vyřazené	vyřazený	k2eAgFnPc1d1	vyřazená
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972	[number]	k4	1972
-	-	kIx~	-
1975	[number]	k4	1975
byly	být	k5eAaImAgFnP	být
dodány	dodán	k2eAgFnPc1d1	dodána
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nahradily	nahradit	k5eAaPmAgFnP	nahradit
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
Polsko	Polsko	k1gNnSc1	Polsko
nuceno	nucen	k2eAgNnSc1d1	nuceno
zredukovat	zredukovat	k5eAaPmF	zredukovat
své	svůj	k3xOyFgNnSc4	svůj
letectvo	letectvo	k1gNnSc4	letectvo
a	a	k8xC	a
zavřít	zavřít	k5eAaPmF	zavřít
několik	několik	k4yIc4	několik
základen	základna	k1gFnPc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc4	několik
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
a	a	k8xC	a
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
prodáno	prodat	k5eAaPmNgNnS	prodat
do	do	k7c2	do
Ugandy	Uganda	k1gFnSc2	Uganda
a	a	k8xC	a
také	také	k9	také
soukromým	soukromý	k2eAgMnPc3d1	soukromý
sběratelům	sběratel	k1gMnPc3	sběratel
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
konec	konec	k1gInSc4	konec
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
americkou	americký	k2eAgFnSc7d1	americká
F-	F-	k1gFnSc7	F-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
Rusko	Rusko	k1gNnSc1	Rusko
</s>
</p>
<p>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
letectvo	letectvo	k1gNnSc1	letectvo
vyřadilo	vyřadit	k5eAaPmAgNnS	vyřadit
své	své	k1gNnSc4	své
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
letadel	letadlo	k1gNnPc2	letadlo
bylo	být	k5eAaImAgNnS	být
přestavěno	přestavět	k5eAaPmNgNnS	přestavět
na	na	k7c4	na
cvičné	cvičný	k2eAgFnPc4d1	cvičná
terče	terč	k1gFnPc4	terč
<g/>
.	.	kIx.	.
<g/>
Slovensko	Slovensko	k1gNnSc4	Slovensko
SlovenskoVzdušné	slovenskovzdušný	k2eAgFnSc2d1	slovenskovzdušný
síly	síla	k1gFnSc2	síla
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
ČSSR	ČSSR	kA	ČSSR
získaly	získat	k5eAaPmAgInP	získat
70	[number]	k4	70
ks	ks	kA	ks
různých	různý	k2eAgFnPc2d1	různá
verzí	verze	k1gFnPc2	verze
MiGů-	MiGů-	k1gMnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
bylo	být	k5eAaImAgNnS	být
21	[number]	k4	21
ks	ks	kA	ks
vyřazených	vyřazený	k2eAgInPc2d1	vyřazený
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
evropskými	evropský	k2eAgFnPc7d1	Evropská
dohodami	dohoda	k1gFnPc7	dohoda
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
stav	stav	k1gInSc1	stav
MiGů-	MiGů-	k1gFnSc4	MiGů-
<g/>
21	[number]	k4	21
upadal	upadat	k5eAaPmAgInS	upadat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
letuschopných	letuschopný	k2eAgMnPc2d1	letuschopný
jen	jen	k6eAd1	jen
8	[number]	k4	8
strojů	stroj	k1gInPc2	stroj
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
3	[number]	k4	3
cvičné	cvičný	k2eAgFnSc6d1	cvičná
MiG-	MiG-	k1gFnSc6	MiG-
<g/>
21	[number]	k4	21
<g/>
US	US	kA	US
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
oficiálně	oficiálně	k6eAd1	oficiálně
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
z	z	k7c2	z
výzbroje	výzbroj	k1gFnSc2	výzbroj
<g/>
.	.	kIx.	.
<g/>
Somálsko	Somálsko	k1gNnSc1	Somálsko
SomálskoSomálské	somálskosomálský	k2eAgFnSc2d1	somálskosomálský
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
síly	síla	k1gFnSc2	síla
získaly	získat	k5eAaPmAgFnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
přátelství	přátelství	k1gNnSc6	přátelství
s	s	k7c7	s
SSSR	SSSR	kA	SSSR
10	[number]	k4	10
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
4	[number]	k4	4
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
UM	uma	k1gFnPc2	uma
<g/>
.	.	kIx.	.
</s>
<s>
Hlavního	hlavní	k2eAgNnSc2d1	hlavní
nasazení	nasazení	k1gNnSc2	nasazení
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
dočkaly	dočkat	k5eAaPmAgInP	dočkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
během	během	k7c2	během
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Etiopií	Etiopie	k1gFnSc7	Etiopie
o	o	k7c4	o
Ogaden	Ogadno	k1gNnPc2	Ogadno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Somálsko	Somálsko	k1gNnSc4	Somálsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
skončila	skončit	k5eAaPmAgFnS	skončit
naprostou	naprostý	k2eAgFnSc7d1	naprostá
katastrofou	katastrofa	k1gFnSc7	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
MiGů-	MiGů-	k1gFnSc2	MiGů-
<g/>
21	[number]	k4	21
byla	být	k5eAaImAgFnS	být
opuštěna	opustit	k5eAaPmNgFnS	opustit
a	a	k8xC	a
ponechána	ponechat	k5eAaPmNgFnS	ponechat
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
zničila	zničit	k5eAaPmAgFnS	zničit
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
i	i	k9	i
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Somálské	somálský	k2eAgFnSc2d1	Somálská
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
již	již	k9	již
jen	jen	k9	jen
vraky	vrak	k1gInPc4	vrak
roztroušené	roztroušený	k2eAgInPc4d1	roztroušený
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
letišti	letiště	k1gNnSc6	letiště
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
<g/>
.	.	kIx.	.
<g/>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svazSovětské	svazSovětský	k2eAgNnSc1d1	svazSovětský
letectvo	letectvo	k1gNnSc1	letectvo
získalo	získat	k5eAaPmAgNnS	získat
první	první	k4xOgFnPc4	první
sériové	sériový	k2eAgFnPc4d1	sériová
MiGy-	MiGy-	k1gFnPc4	MiGy-
<g/>
21	[number]	k4	21
<g/>
F	F	kA	F
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
verzích	verze	k1gFnPc6	verze
zůstal	zůstat	k5eAaPmAgInS	zůstat
ve	v	k7c6	v
výzbroji	výzbroj	k1gInSc6	výzbroj
letectva	letectvo	k1gNnSc2	letectvo
SSSR	SSSR	kA	SSSR
až	až	k6eAd1	až
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
rozpadu	rozpad	k1gInSc2	rozpad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
postupné	postupný	k2eAgNnSc4d1	postupné
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
MiGy-	MiGy-	k1gFnSc7	MiGy-
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
přeletu	přelet	k1gInSc2	přelet
špionážního	špionážní	k2eAgInSc2d1	špionážní
letounu	letoun	k1gInSc2	letoun
U-2	U-2	k1gMnPc2	U-2
s	s	k7c7	s
Garry	Garr	k1gMnPc7	Garr
Powersem	Powers	k1gMnSc7	Powers
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
pohotovosti	pohotovost	k1gFnSc6	pohotovost
MiGy-	MiGy-	k1gMnSc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
F	F	kA	F
s	s	k7c7	s
neřízenými	řízený	k2eNgFnPc7d1	neřízená
raketami	raketa	k1gFnPc7	raketa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
nevzlétly	vzlétnout	k5eNaPmAgFnP	vzlétnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1962	[number]	k4	1962
vyslal	vyslat	k5eAaPmAgMnS	vyslat
SSSR	SSSR	kA	SSSR
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
pluk	pluk	k1gInSc1	pluk
vybavený	vybavený	k2eAgInSc1d1	vybavený
modelem	model	k1gInSc7	model
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
Kubě	Kuba	k1gFnSc3	Kuba
zajistit	zajistit	k5eAaPmF	zajistit
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
sovětské	sovětský	k2eAgFnPc1d1	sovětská
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
rozmístěné	rozmístěný	k2eAgFnSc2d1	rozmístěná
v	v	k7c6	v
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
staženy	stáhnout	k5eAaPmNgFnP	stáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1968	[number]	k4	1968
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
několik	několik	k4yIc1	několik
verzí	verze	k1gFnPc2	verze
MiGu-	MiGu-	k1gMnPc2	MiGu-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
sovětští	sovětský	k2eAgMnPc1d1	sovětský
piloti	pilot	k1gMnPc1	pilot
vysláni	vyslat	k5eAaPmNgMnP	vyslat
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
boku	bok	k1gInSc6	bok
egyptských	egyptský	k2eAgMnPc2d1	egyptský
pilotů	pilot	k1gMnPc2	pilot
bojovali	bojovat	k5eAaImAgMnP	bojovat
proti	proti	k7c3	proti
izraelským	izraelský	k2eAgFnPc3d1	izraelská
stíhačkám	stíhačka	k1gFnPc3	stíhačka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1970	[number]	k4	1970
byl	být	k5eAaImAgInS	být
sovětským	sovětský	k2eAgInSc7d1	sovětský
MiGem-	MiGem-	k1gMnSc2	MiGem-
<g/>
21	[number]	k4	21
sestřelen	sestřelen	k2eAgInSc1d1	sestřelen
izraelský	izraelský	k2eAgInSc1d1	izraelský
A-4	A-4	k1gFnSc7	A-4
Skyhawk	Skyhawka	k1gFnPc2	Skyhawka
nad	nad	k7c4	nad
Ismá	Ismé	k1gNnPc4	Ismé
<g/>
'	'	kIx"	'
<g/>
ílíjou	ílíjá	k1gFnSc4	ílíjá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
měl	mít	k5eAaImAgInS	mít
SSSR	SSSR	kA	SSSR
více	hodně	k6eAd2	hodně
MiGů-	MiGů-	k1gFnPc1	MiGů-
<g/>
21	[number]	k4	21
než	než	k8xS	než
celé	celý	k2eAgNnSc4d1	celé
NATO	nato	k6eAd1	nato
všech	všecek	k3xTgFnPc2	všecek
stíhaček	stíhačka	k1gFnPc2	stíhačka
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgNnSc7d1	poslední
velkým	velký	k2eAgNnSc7d1	velké
nasazením	nasazení	k1gNnSc7	nasazení
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
letectva	letectvo	k1gNnSc2	letectvo
SSSR	SSSR	kA	SSSR
byla	být	k5eAaImAgFnS	být
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
nasazeny	nasazen	k2eAgFnPc1d1	nasazena
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
R	R	kA	R
a	a	k8xC	a
MiGy-	MiGy-	k1gFnSc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgFnPc1d1	sovětská
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
byly	být	k5eAaImAgFnP	být
vyřazovány	vyřazovat	k5eAaImNgFnP	vyřazovat
počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
TanzanieTanzanské	TanzanieTanzanský	k2eAgNnSc1d1	TanzanieTanzanský
letectvo	letectvo	k1gNnSc1	letectvo
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
14	[number]	k4	14
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
U.	U.	kA	U.
Během	během	k7c2	během
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Ugandou	Uganda	k1gFnSc7	Uganda
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
ukořistěno	ukořistit	k5eAaPmNgNnS	ukořistit
7	[number]	k4	7
ks	ks	kA	ks
ugandských	ugandský	k2eAgFnPc2d1	ugandská
stíhaček	stíhačka	k1gFnPc2	stíhačka
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
U.	U.	kA	U.
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
zahrnuty	zahrnout	k5eAaPmNgInP	zahrnout
do	do	k7c2	do
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
a	a	k8xC	a
sloužily	sloužit	k5eAaImAgInP	sloužit
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Mwanza	Mwanz	k1gMnSc2	Mwanz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
bylo	být	k5eAaImAgNnS	být
objednáno	objednat	k5eAaPmNgNnS	objednat
10	[number]	k4	10
ks	ks	kA	ks
F-7B	F-7B	k1gFnPc2	F-7B
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
FT-7	FT-7	k1gFnPc2	FT-7
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
pořízeny	pořízen	k2eAgInPc1d1	pořízen
4	[number]	k4	4
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgInPc4	všechen
stroje	stroj	k1gInPc4	stroj
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
už	už	k6eAd1	už
jen	jen	k6eAd1	jen
stíhačky	stíhačka	k1gFnPc4	stíhačka
F-	F-	k1gFnSc4	F-
<g/>
7	[number]	k4	7
<g/>
A.	A.	kA	A.
<g/>
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
</s>
</p>
<p>
<s>
USA	USA	kA	USA
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
UkrajinaUkrajinské	UkrajinaUkrajinský	k2eAgNnSc1d1	UkrajinaUkrajinský
letectvo	letectvo	k1gNnSc1	letectvo
získalo	získat	k5eAaPmAgNnS	získat
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
letecké	letecký	k2eAgFnSc2d1	letecká
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
i	i	k9	i
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
MiGů-	MiGů-	k1gFnPc2	MiGů-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nebyly	být	k5eNaImAgFnP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
do	do	k7c2	do
operační	operační	k2eAgFnSc2d1	operační
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nabízeny	nabízet	k5eAaImNgFnP	nabízet
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
nebo	nebo	k8xC	nebo
modernizaci	modernizace	k1gFnSc3	modernizace
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
prodala	prodat	k5eAaPmAgFnS	prodat
MiGy-	MiGy-	k1gFnSc4	MiGy-
<g/>
21	[number]	k4	21
do	do	k7c2	do
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Angoly	Angola	k1gFnSc2	Angola
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
existovaly	existovat	k5eAaImAgInP	existovat
různé	různý	k2eAgInPc1d1	různý
modernizační	modernizační	k2eAgInPc1d1	modernizační
programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgFnP	mít
upravit	upravit	k5eAaPmF	upravit
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
na	na	k7c4	na
současné	současný	k2eAgInPc4d1	současný
standardy	standard	k1gInPc4	standard
<g/>
.	.	kIx.	.
<g/>
Vietnam	Vietnam	k1gInSc1	Vietnam
Vietnam	Vietnam	k1gInSc1	Vietnam
</s>
</p>
<p>
<s>
Letectvo	letectvo	k1gNnSc1	letectvo
Vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
získalo	získat	k5eAaPmAgNnS	získat
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
MiGy-	MiGy-	k1gFnSc2	MiGy-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
boje	boj	k1gInPc1	boj
s	s	k7c7	s
americkými	americký	k2eAgFnPc7d1	americká
stíhačkami	stíhačka	k1gFnPc7	stíhačka
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
ve	v	k7c6	v
vietnamských	vietnamský	k2eAgFnPc6d1	vietnamská
službách	služba	k1gFnPc6	služba
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
oběťmi	oběť	k1gFnPc7	oběť
vietnamských	vietnamský	k2eAgMnPc2d1	vietnamský
MiGů-	MiGů-	k1gMnPc2	MiGů-
<g/>
21	[number]	k4	21
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
bezpilotní	bezpilotní	k2eAgInPc1d1	bezpilotní
aparáty	aparát	k1gInPc1	aparát
Ryan	Ryana	k1gFnPc2	Ryana
Firebee	Firebe	k1gInSc2	Firebe
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1966	[number]	k4	1966
padly	padnout	k5eAaImAgFnP	padnout
MiGům-	MiGům-	k1gFnSc4	MiGům-
<g/>
21	[number]	k4	21
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
první	první	k4xOgFnSc1	první
2	[number]	k4	2
stíhačky	stíhačka	k1gFnSc2	stíhačka
F-4	F-4	k1gFnSc1	F-4
Phantom	Phantom	k1gInSc1	Phantom
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
přes	přes	k7c4	přes
30	[number]	k4	30
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
PF	PF	kA	PF
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
PFM	PFM	kA	PFM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
získalo	získat	k5eAaPmAgNnS	získat
vietnamské	vietnamský	k2eAgNnSc1d1	vietnamské
letectvo	letectvo	k1gNnSc1	letectvo
60	[number]	k4	60
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
<g/>
MF	MF	kA	MF
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
verze	verze	k1gFnPc1	verze
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
i	i	k8xC	i
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
MiGmi-	MiGmi-	k1gMnPc2	MiGmi-
<g/>
21	[number]	k4	21
vietnamského	vietnamský	k2eAgNnSc2d1	vietnamské
letectva	letectvo	k1gNnSc2	letectvo
sestřeleno	sestřelit	k5eAaPmNgNnS	sestřelit
47	[number]	k4	47
amerických	americký	k2eAgFnPc2d1	americká
stíhaček	stíhačka	k1gFnPc2	stíhačka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
USAF	USAF	kA	USAF
a	a	k8xC	a
námořní	námořní	k2eAgNnSc1d1	námořní
letectvo	letectvo	k1gNnSc1	letectvo
USA	USA	kA	USA
sestřelilo	sestřelit	k5eAaPmAgNnS	sestřelit
67	[number]	k4	67
ks	ks	kA	ks
a	a	k8xC	a
18	[number]	k4	18
ks	ks	kA	ks
MiGu-	MiGu-	k1gFnSc1	MiGu-
<g/>
21	[number]	k4	21
respektive	respektive	k9	respektive
<g/>
.	.	kIx.	.
</s>
<s>
Třináct	třináct	k4xCc1	třináct
pilotů	pilot	k1gMnPc2	pilot
vietnamského	vietnamský	k2eAgNnSc2d1	vietnamské
letectva	letectvo	k1gNnSc2	letectvo
získalo	získat	k5eAaPmAgNnS	získat
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
eso	eso	k1gNnSc1	eso
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1967	[number]	k4	1967
a	a	k8xC	a
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byly	být	k5eAaImAgFnP	být
dodány	dodán	k2eAgFnPc1d1	dodána
MiGy-	MiGy-	k1gFnPc1	MiGy-
<g/>
21	[number]	k4	21
<g/>
bis	bis	k?	bis
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgInPc1d1	následovaný
dalšími	další	k2eAgFnPc7d1	další
dodávkami	dodávka	k1gFnPc7	dodávka
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
mělo	mít	k5eAaImAgNnS	mít
MiG-	MiG-	k1gFnSc7	MiG-
<g/>
21	[number]	k4	21
ve	v	k7c6	v
výzbroji	výzbroj	k1gInSc6	výzbroj
7	[number]	k4	7
pluků	pluk	k1gInPc2	pluk
vietnamského	vietnamský	k2eAgNnSc2d1	vietnamské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
vyřazen	vyřadit	k5eAaPmNgInS	vyřadit
ze	z	k7c2	z
službyZair	službyZaira	k1gFnPc2	službyZaira
Zair	Zaira	k1gFnPc2	Zaira
</s>
</p>
<p>
<s>
==	==	k?	==
MiG-	MiG-	k1gFnPc2	MiG-
<g/>
21	[number]	k4	21
<g/>
F-	F-	k1gFnPc2	F-
<g/>
13	[number]	k4	13
(	(	kIx(	(
<g/>
Fishbed-C	Fishbed-C	k1gFnSc1	Fishbed-C
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgInPc1d1	hlavní
technické	technický	k2eAgInPc1d1	technický
údaje	údaj	k1gInPc1	údaj
===	===	k?	===
</s>
</p>
<p>
<s>
Osádka	osádka	k1gFnSc1	osádka
<g/>
:	:	kIx,	:
1	[number]	k4	1
(	(	kIx(	(
<g/>
pilot	pilot	k1gMnSc1	pilot
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
<g/>
:	:	kIx,	:
7,150	[number]	k4	7,150
m	m	kA	m
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
trupu	trup	k1gInSc2	trup
<g/>
:	:	kIx,	:
13,460	[number]	k4	13,460
m	m	kA	m
(	(	kIx(	(
<g/>
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
trubice	trubice	k1gFnSc2	trubice
PVD	PVD	kA	PVD
<g/>
,	,	kIx,	,
15,760	[number]	k4	15,760
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
4,100	[number]	k4	4,100
m	m	kA	m
</s>
</p>
<p>
<s>
Nosná	nosný	k2eAgFnSc1d1	nosná
plocha	plocha	k1gFnSc1	plocha
<g/>
:	:	kIx,	:
23,00	[number]	k4	23,00
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
prázdného	prázdný	k2eAgInSc2d1	prázdný
letounu	letoun	k1gInSc2	letoun
<g/>
:	:	kIx,	:
4980	[number]	k4	4980
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
7370	[number]	k4	7370
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
<g/>
.	.	kIx.	.
vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
8625	[number]	k4	8625
kg	kg	kA	kg
(	(	kIx(	(
<g/>
s	s	k7c7	s
přídavnou	přídavný	k2eAgFnSc7d1	přídavná
nádrží	nádrž	k1gFnSc7	nádrž
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
250	[number]	k4	250
<g/>
kg	kg	kA	kg
pumami	puma	k1gFnPc7	puma
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
1	[number]	k4	1
x	x	k?	x
dvourotorový	dvourotorový	k2eAgInSc1d1	dvourotorový
turbokompresorový	turbokompresorový	k2eAgInSc1d1	turbokompresorový
motor	motor	k1gInSc1	motor
Tumanskij	Tumanskij	k1gMnSc2	Tumanskij
R-11F-300	R-11F-300	k1gMnSc2	R-11F-300
</s>
</p>
<p>
<s>
Tah	tah	k1gInSc1	tah
motoru	motor	k1gInSc2	motor
<g/>
:	:	kIx,	:
3900	[number]	k4	3900
kp	kp	k?	kp
(	(	kIx(	(
<g/>
38,25	[number]	k4	38,25
kN	kN	k?	kN
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tah	tah	k1gInSc1	tah
motoru	motor	k1gInSc2	motor
<g/>
:	:	kIx,	:
5750	[number]	k4	5750
kp	kp	k?	kp
(	(	kIx(	(
<g/>
56,39	[number]	k4	56,39
kN	kN	k?	kN
<g/>
)	)	kIx)	)
s	s	k7c7	s
přídavným	přídavný	k2eAgNnSc7d1	přídavné
spalováním	spalování	k1gNnSc7	spalování
</s>
</p>
<p>
<s>
===	===	k?	===
Výkony	výkon	k1gInPc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
<g/>
:	:	kIx,	:
2125	[number]	k4	2125
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
M	M	kA	M
=	=	kIx~	=
2,05	[number]	k4	2,05
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
930	[number]	k4	930
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
11	[number]	k4	11
000	[number]	k4	000
m	m	kA	m
</s>
</p>
<p>
<s>
Operační	operační	k2eAgInSc1d1	operační
dostup	dostup	k1gInSc1	dostup
<g/>
:	:	kIx,	:
17	[number]	k4	17
500	[number]	k4	500
m	m	kA	m
</s>
</p>
<p>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
stoupavost	stoupavost	k1gFnSc1	stoupavost
<g/>
:	:	kIx,	:
140	[number]	k4	140
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
Čas	čas	k1gInSc1	čas
výstupu	výstup	k1gInSc2	výstup
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
10	[number]	k4	10
000	[number]	k4	000
m	m	kA	m
<g/>
:	:	kIx,	:
3,20	[number]	k4	3,20
minuty	minuta	k1gFnPc4	minuta
</s>
</p>
<p>
<s>
Dolet	dolet	k1gInSc1	dolet
(	(	kIx(	(
<g/>
maximální	maximální	k2eAgInSc4d1	maximální
technický	technický	k2eAgInSc4d1	technický
dolet	dolet	k1gInSc4	dolet
<g/>
,	,	kIx,	,
s	s	k7c7	s
příd	příd	k?	příd
<g/>
.	.	kIx.	.
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
670	[number]	k4	670
km	km	kA	km
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
11	[number]	k4	11
000	[number]	k4	000
m	m	kA	m
</s>
</p>
<p>
<s>
===	===	k?	===
Výzbroj	výzbroj	k1gInSc4	výzbroj
===	===	k?	===
</s>
</p>
<p>
<s>
jeden	jeden	k4xCgInSc1	jeden
kanón	kanón	k1gInSc1	kanón
NR-30	NR-30	k1gFnSc2	NR-30
ráže	ráže	k1gFnSc2	ráže
30	[number]	k4	30
mmjeden	mmjeden	k2eAgInSc4d1	mmjeden
dvouhlavňový	dvouhlavňový	k2eAgInSc4d1	dvouhlavňový
kanón	kanón	k1gInSc4	kanón
GŠ-23	GŠ-23	k1gFnSc2	GŠ-23
ráže	ráže	k1gFnSc2	ráže
23	[number]	k4	23
mm	mm	kA	mm
<g/>
,	,	kIx,	,
200	[number]	k4	200
nábojů	náboj	k1gInPc2	náboj
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc2	verze
PFM	PFM	kA	PFM
<g/>
,	,	kIx,	,
MF	MF	kA	MF
<g/>
,	,	kIx,	,
SMT	SMT	kA	SMT
a	a	k8xC	a
bis	bis	k?	bis
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jeden	jeden	k4xCgInSc1	jeden
30	[number]	k4	30
<g/>
mm	mm	kA	mm
kanón	kanón	k1gInSc4	kanón
NR-30	NR-30	k1gFnSc2	NR-30
s	s	k7c7	s
60	[number]	k4	60
náboji	náboj	k1gInPc7	náboj
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
F-	F-	k1gFnSc1	F-
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
výzbroj	výzbroj	k1gFnSc1	výzbroj
nebo	nebo	k8xC	nebo
výstroj	výstroj	k1gInSc1	výstroj
na	na	k7c4	na
5	[number]	k4	5
<g/>
×	×	k?	×
podvěsech	podvěs	k1gInPc6	podvěs
do	do	k7c2	do
hmotnosti	hmotnost	k1gFnSc2	hmotnost
2	[number]	k4	2
000	[number]	k4	000
kg	kg	kA	kg
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
×	×	k?	×
Vympel	Vympel	k1gMnSc1	Vympel
K-13	K-13	k1gMnSc1	K-13
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
×	×	k?	×
Molnija	Molnij	k2eAgFnSc1d1	Molnij
R-60	R-60	k1gFnSc1	R-60
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
×	×	k?	×
přídavná	přídavný	k2eAgFnSc1d1	přídavná
800	[number]	k4	800
l	l	kA	l
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c6	na
trupovém	trupový	k2eAgInSc6d1	trupový
závěsníku	závěsník	k1gInSc6	závěsník
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
×	×	k?	×
přídavné	přídavný	k2eAgFnSc2d1	přídavná
nádrže	nádrž	k1gFnSc2	nádrž
nádrže	nádrž	k1gFnSc2	nádrž
po	po	k7c4	po
490	[number]	k4	490
<g/>
l	l	kA	l
na	na	k7c6	na
křídelních	křídelní	k2eAgInPc6d1	křídelní
závěsnících	závěsník	k1gInPc6	závěsník
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
×	×	k?	×
zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
fotografický	fotografický	k2eAgInSc4d1	fotografický
nebo	nebo	k8xC	nebo
radarový	radarový	k2eAgInSc4d1	radarový
průzkum	průzkum	k1gInSc4	průzkum
na	na	k7c6	na
trupovém	trupový	k2eAgInSc6d1	trupový
závěsníku	závěsník	k1gInSc6	závěsník
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c4	o
létání	létání	k1gNnSc4	létání
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
typu	typ	k1gInSc6	typ
letounu	letoun	k1gInSc2	letoun
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
také	také	k6eAd1	také
československý	československý	k2eAgInSc1d1	československý
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
Pod	pod	k7c7	pod
nohama	noha	k1gFnPc7	noha
nebe	nebe	k1gNnSc2	nebe
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Námětem	námět	k1gInSc7	námět
filmu	film	k1gInSc2	film
Steal	Steal	k1gInSc1	Steal
the	the	k?	the
Sky	Sky	k1gFnSc2	Sky
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
je	být	k5eAaImIp3nS	být
únos	únos	k1gInSc4	únos
MiGu	mig	k1gInSc2	mig
21	[number]	k4	21
z	z	k7c2	z
Iráku	Irák	k1gInSc2	Irák
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
pilotoval	pilotovat	k5eAaImAgMnS	pilotovat
Iráčan	Iráčan	k1gMnSc1	Iráčan
Munír	Munír	k1gMnSc1	Munír
Rúfá	Rúfá	k1gFnSc1	Rúfá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Létat	létat	k5eAaImF	létat
je	on	k3xPp3gInPc4	on
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
také	také	k9	také
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
filmu	film	k1gInSc6	film
...	...	k?	...
<g/>
a	a	k8xC	a
stoupej	stoupat	k5eAaImRp2nS	stoupat
do	do	k7c2	do
slunce	slunce	k1gNnSc2	slunce
<g/>
!	!	kIx.	!
</s>
<s>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Mikojan-Gurevič	Mikojan-Gurevič	k1gInSc1	Mikojan-Gurevič
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
MiG-	MiG-	k1gFnSc2	MiG-
<g/>
21	[number]	k4	21
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
czechairspotters	czechairspotters	k1gInSc1	czechairspotters
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
military	militara	k1gFnPc1	militara
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
forum	forum	k1gNnSc1	forum
<g/>
.	.	kIx.	.
<g/>
valka	valka	k1gFnSc1	valka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
techmag	techmag	k1gInSc1	techmag
<g/>
.	.	kIx.	.
<g/>
valka	valka	k1gFnSc1	valka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
palba	palba	k1gFnSc1	palba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
de	de	k?	de
</s>
</p>
<p>
<s>
Video	video	k1gNnSc1	video
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
českým	český	k2eAgMnPc3d1	český
Migům	mig	k1gInPc3	mig
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
