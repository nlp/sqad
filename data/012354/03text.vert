<p>
<s>
Dudek	Dudek	k1gMnSc1	Dudek
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
(	(	kIx(	(
<g/>
Upupa	Upupa	k1gFnSc1	Upupa
epops	epops	k1gInSc1	epops
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
zoborožců	zoborožec	k1gMnPc2	zoborožec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
národním	národní	k2eAgMnSc7d1	národní
ptákem	pták	k1gMnSc7	pták
státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Dudek	Dudek	k1gMnSc1	Dudek
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
žijícím	žijící	k2eAgMnSc7d1	žijící
zástupcem	zástupce	k1gMnSc7	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
dudkovitých	dudkovitý	k2eAgNnPc2d1	dudkovitý
(	(	kIx(	(
<g/>
Upupidae	Upupidae	k1gNnPc2	Upupidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
dudek	dudek	k1gMnSc1	dudek
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Upupa	Upupa	k1gFnSc1	Upupa
antaios	antaios	k1gMnSc1	antaios
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Svatá	svatý	k2eAgFnSc1d1	svatá
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
během	běh	k1gInSc7	běh
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poddruhy	poddruh	k1gInPc4	poddruh
===	===	k?	===
</s>
</p>
<p>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	s	k7c7	s
9	[number]	k4	9
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
Sibiř	Sibiř	k1gFnSc4	Sibiř
obývá	obývat	k5eAaImIp3nS	obývat
dudek	dudek	k1gMnSc1	dudek
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
e.	e.	k?	e.
epops	epops	k1gInSc1	epops
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
území	území	k1gNnPc4	území
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
d.	d.	k?	d.
ch	ch	k0	ch
<g/>
.	.	kIx.	.
asijský	asijský	k2eAgMnSc1d1	asijský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
e.	e.	k?	e.
saturata	saturata	k1gFnSc1	saturata
<g/>
)	)	kIx)	)
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
od	od	k7c2	od
Suezu	Suez	k1gInSc2	Suez
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
d.	d.	k?	d.
ch	ch	k0	ch
<g/>
.	.	kIx.	.
egyptský	egyptský	k2eAgMnSc1d1	egyptský
(	(	kIx(	(
<g/>
U.	U.	kA	U.
e.	e.	k?	e.
major	major	k1gMnSc1	major
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
6	[number]	k4	6
poddruhů	poddruh	k1gInPc2	poddruh
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velikosti	velikost	k1gFnPc1	velikost
hrdličky	hrdlička	k1gFnSc2	hrdlička
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
44	[number]	k4	44
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
nezaměnitelný	zaměnitelný	k2eNgInSc1d1	nezaměnitelný
<g/>
,	,	kIx,	,
s	s	k7c7	s
širokými	široký	k2eAgNnPc7d1	široké
zakulacenými	zakulacený	k2eAgNnPc7d1	zakulacené
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
výraznou	výrazný	k2eAgFnSc7d1	výrazná
vztyčitelnou	vztyčitelný	k2eAgFnSc7d1	vztyčitelný
chocholkou	chocholka	k1gFnSc7	chocholka
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
<g/>
,	,	kIx,	,
tenkým	tenký	k2eAgInSc7d1	tenký
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
dolů	dolů	k6eAd1	dolů
zahnutým	zahnutý	k2eAgInSc7d1	zahnutý
zobákem	zobák	k1gInSc7	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
žlutohnědě	žlutohnědě	k6eAd1	žlutohnědě
růžové	růžový	k2eAgNnSc1d1	růžové
s	s	k7c7	s
černými	černý	k2eAgInPc7d1	černý
a	a	k8xC	a
bílými	bílý	k2eAgInPc7d1	bílý
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
s	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
bílým	bílý	k2eAgInSc7d1	bílý
pruhem	pruh	k1gInSc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
se	s	k7c7	s
zbarvením	zbarvení	k1gNnSc7	zbarvení
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Létá	létat	k5eAaImIp3nS	létat
vlnovitě	vlnovitě	k6eAd1	vlnovitě
a	a	k8xC	a
dost	dost	k6eAd1	dost
vratce	vratko	k6eAd1	vratko
<g/>
,	,	kIx,	,
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
chováním	chování	k1gNnSc7	chování
připomíná	připomínat	k5eAaImIp3nS	připomínat
špačka	špaček	k1gMnSc4	špaček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Dudek	Dudek	k1gMnSc1	Dudek
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
tažný	tažný	k2eAgInSc1d1	tažný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
se	s	k7c7	s
zimovišti	zimoviště	k1gNnPc7	zimoviště
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
mapka	mapka	k1gFnSc1	mapka
rozšíření	rozšíření	k1gNnSc1	rozšíření
níže	níže	k1gFnSc2	níže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
890	[number]	k4	890
000	[number]	k4	000
párů	pár	k1gInPc2	pár
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
mírně	mírně	k6eAd1	mírně
klesající	klesající	k2eAgFnSc4d1	klesající
tendenci	tendence	k1gFnSc4	tendence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdil	hnízdit	k5eAaImAgMnS	hnízdit
dříve	dříve	k6eAd2	dříve
pravidelně	pravidelně	k6eAd1	pravidelně
a	a	k8xC	a
místy	místo	k1gNnPc7	místo
zcela	zcela	k6eAd1	zcela
běžně	běžně	k6eAd1	běžně
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
však	však	k9	však
téměř	téměř	k6eAd1	téměř
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jen	jen	k9	jen
ojediněle	ojediněle	k6eAd1	ojediněle
a	a	k8xC	a
nepravidelně	pravidelně	k6eNd1	pravidelně
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
a	a	k8xC	a
pahorkatinách	pahorkatina	k1gFnPc6	pahorkatina
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
na	na	k7c6	na
Hodonínsku	Hodonínsko	k1gNnSc6	Hodonínsko
<g/>
,	,	kIx,	,
Břeclavsku	Břeclavsko	k1gNnSc6	Břeclavsko
a	a	k8xC	a
Znojemsku	Znojemsko	k1gNnSc6	Znojemsko
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1985-89	[number]	k4	1985-89
odhadnuta	odhadnout	k5eAaPmNgFnS	odhadnout
na	na	k7c4	na
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
opakovaně	opakovaně	k6eAd1	opakovaně
pozorován	pozorovat	k5eAaImNgInS	pozorovat
v	v	k7c6	v
oboře	obora	k1gFnSc6	obora
divokých	divoký	k2eAgMnPc2d1	divoký
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
praturů	pratur	k1gMnPc2	pratur
v	v	k7c6	v
Milovicích	Milovice	k1gFnPc6	Milovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prostředí	prostředí	k1gNnSc2	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
Vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
travnatou	travnatý	k2eAgFnSc4d1	travnatá
krajinu	krajina	k1gFnSc4	krajina
s	s	k7c7	s
lesíky	lesík	k1gInPc7	lesík
<g/>
,	,	kIx,	,
živými	živý	k2eAgInPc7d1	živý
ploty	plot	k1gInPc7	plot
a	a	k8xC	a
křovinami	křovina	k1gFnPc7	křovina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
vinohradech	vinohrad	k1gInPc6	vinohrad
a	a	k8xC	a
sadech	sad	k1gInPc6	sad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jednotlivě	jednotlivě	k6eAd1	jednotlivě
a	a	k8xC	a
teritoriálně	teritoriálně	k6eAd1	teritoriálně
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
stromové	stromový	k2eAgFnPc1d1	stromová
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
vystýlky	vystýlka	k1gFnSc2	vystýlka
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
přináší	přinášet	k5eAaImIp3nS	přinášet
stonky	stonka	k1gFnPc4	stonka
a	a	k8xC	a
stébla	stéblo	k1gNnPc4	stéblo
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
kousky	kousek	k1gInPc4	kousek
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
mívá	mívat	k5eAaImIp3nS	mívat
jednu	jeden	k4xCgFnSc4	jeden
snůšku	snůška	k1gFnSc4	snůška
po	po	k7c6	po
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
bílých	bílý	k2eAgFnPc2d1	bílá
nebo	nebo	k8xC	nebo
nažloutlých	nažloutlý	k2eAgNnPc6d1	nažloutlé
vejcích	vejce	k1gNnPc6	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
26,0	[number]	k4	26,0
×	×	k?	×
17,9	[number]	k4	17,9
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
inkubace	inkubace	k1gFnSc2	inkubace
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
sedí	sedit	k5eAaImIp3nP	sedit
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
samec	samec	k1gInSc1	samec
přináší	přinášet	k5eAaImIp3nS	přinášet
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
zpočátku	zpočátku	k6eAd1	zpočátku
krmena	krmit	k5eAaImNgFnS	krmit
pouze	pouze	k6eAd1	pouze
samicí	samice	k1gFnSc7	samice
<g/>
,	,	kIx,	,
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
oběma	dva	k4xCgMnPc7	dva
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
hnízdo	hnízdo	k1gNnSc1	hnízdo
opouštějí	opouštět	k5eAaImIp3nP	opouštět
po	po	k7c6	po
26	[number]	k4	26
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
jsou	být	k5eAaImIp3nP	být
krmena	krmit	k5eAaImNgNnP	krmit
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
a	a	k8xC	a
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
1	[number]	k4	1
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
převažuje	převažovat	k5eAaImIp3nS	převažovat
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
larvy	larva	k1gFnPc4	larva
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
brouci	brouk	k1gMnPc1	brouk
<g/>
,	,	kIx,	,
dvoukřídlí	dvoukřídlí	k1gMnPc1	dvoukřídlí
<g/>
,	,	kIx,	,
blanokřídlí	blanokřídlí	k1gMnPc1	blanokřídlí
<g/>
,	,	kIx,	,
krtonožky	krtonožka	k1gFnPc1	krtonožka
a	a	k8xC	a
stonožky	stonožka	k1gFnPc1	stonožka
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pavouci	pavouk	k1gMnPc1	pavouk
a	a	k8xC	a
žížaly	žížala	k1gFnPc1	žížala
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
hledá	hledat	k5eAaImIp3nS	hledat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hoopoe	Hoopo	k1gInSc2	Hoopo
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Dungel	Dungel	k1gInSc1	Dungel
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Hudec	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
K.	K.	kA	K.
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
ptáků	pták	k1gMnPc2	pták
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
;	;	kIx,	;
str	str	kA	str
<g/>
.	.	kIx.	.
154	[number]	k4	154
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-200-0927-2	[number]	k4	978-80-200-0927-2
</s>
</p>
<p>
<s>
Bezzel	Bezzet	k5eAaImAgMnS	Bezzet
<g/>
,	,	kIx,	,
E.	E.	kA	E.
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ptáci	pták	k1gMnPc1	pták
<g/>
;	;	kIx,	;
str	str	kA	str
<g/>
.	.	kIx.	.
230	[number]	k4	230
<g/>
.	.	kIx.	.
</s>
<s>
Rebo	Rebo	k1gNnSc1	Rebo
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7234-292-1	[number]	k4	978-80-7234-292-1
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dudek	dudek	k1gMnSc1	dudek
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
dudek	dudek	k1gMnSc1	dudek
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Upupa	Upup	k1gMnSc4	Upup
epops	epops	k6eAd1	epops
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Eretz	Eretz	k1gInSc1	Eretz
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Izraelským	izraelský	k2eAgMnSc7d1	izraelský
národním	národní	k2eAgMnSc7d1	národní
ptákem	pták	k1gMnSc7	pták
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
dudek	dudek	k1gMnSc1	dudek
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
–	–	k?	–
Hlas	hlas	k1gInSc1	hlas
<g/>
:	:	kIx,	:
Dudek	Dudek	k1gMnSc1	Dudek
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
</s>
</p>
<p>
<s>
Naturephoto	Naturephota	k1gFnSc5	Naturephota
–	–	k?	–
Dudek	Dudek	k1gMnSc1	Dudek
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
(	(	kIx(	(
<g/>
Upupa	Upupa	k1gFnSc1	Upupa
epops	epopsa	k1gFnPc2	epopsa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přiroda	Přiroda	k1gMnSc1	Přiroda
<g/>
.	.	kIx.	.
<g/>
info	info	k1gMnSc1	info
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Hlas	hlas	k1gInSc1	hlas
Dudka	Dudek	k1gMnSc2	Dudek
chocholatého	chocholatý	k2eAgMnSc2d1	chocholatý
</s>
</p>
