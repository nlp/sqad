<s>
Chilská	chilský	k2eAgFnSc1d1
rallye	rallye	k1gFnSc1
2019	#num#	k4
</s>
<s>
Chilská	chilský	k2eAgFnSc1d1
rallye	rallye	k1gFnSc1
2019	#num#	k4
(	(	kIx(
<g/>
oficiálně	oficiálně	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copec	Copec	k1gInSc1
Rally	Rally	k1gInSc1
Chile	Chile	k1gNnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
6	#num#	k4
<g/>
.	.	kIx.
podnik	podnik	k1gInSc1
Mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
v	v	k7c6
rallye	rallye	k1gNnSc6
2019	#num#	k4
(	(	kIx(
<g/>
WRC	WRC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1
konal	konat	k5eAaImAgInS
v	v	k7c6
Chile	Chile	k1gNnSc6
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
až	až	k9
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolutním	absolutní	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
posádka	posádka	k1gFnSc1
Ott	Ott	k1gMnSc1
Tänak	Tänak	k1gMnSc1
a	a	k8xC
Martin	Martin	k1gMnSc1
Järveoja	Järveoja	k1gMnSc1
(	(	kIx(
<g/>
Toyota	toyota	k1gFnSc1
GAZOO	GAZOO	kA
Racing	Racing	k1gInSc4
WRT	WRT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závod	závod	k1gInSc1
mělo	mít	k5eAaImAgNnS
délce	délka	k1gFnSc6
304,81	304,81	k4
km	km	kA
se	se	k3xPyFc4
jel	jet	k5eAaImAgMnS
na	na	k7c6
šotolině	šotolina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Situace	situace	k1gFnSc1
před	před	k7c7
závodem	závod	k1gInSc7
</s>
<s>
Před	před	k7c7
Chilskou	chilský	k2eAgFnSc7d1
rallye	rallye	k1gFnSc7
posádka	posádka	k1gFnSc1
Thierry	Thierra	k1gMnSc2
Neuville	Neuvill	k1gMnSc2
a	a	k8xC
Nicolas	Nicolas	k1gInSc4
Gilsoul	Gilsoula	k1gFnPc2
vedla	vést	k5eAaImAgFnS
celkové	celkový	k2eAgNnSc4d1
pořadí	pořadí	k1gNnSc4
jezdců	jezdec	k1gMnPc2
i	i	k8xC
spolujezdců	spolujezdec	k1gMnPc2
o	o	k7c4
10	#num#	k4
bodů	bod	k1gInPc2
před	před	k7c7
šestinásobným	šestinásobný	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
Sébastienem	Sébastien	k1gMnSc7
Ogierem	Ogier	k1gMnSc7
a	a	k8xC
Julienem	Julien	k1gMnSc7
Ingrassiou	Ingrassia	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ott	Ott	k1gMnSc1
Tänak	Tänak	k1gMnSc1
a	a	k8xC
Martin	Martin	k1gMnSc1
Järveoja	Järveoja	k1gMnSc1
byli	být	k5eAaImAgMnP
třetí	třetí	k4xOgMnPc1
a	a	k8xC
zaostávali	zaostávat	k5eAaImAgMnP
za	za	k7c7
druhou	druhý	k4xOgFnSc7
posádkou	posádka	k1gFnSc7
o	o	k7c4
18	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
konstruktéry	konstruktér	k1gMnPc4
vedl	vést	k5eAaImAgInS
tým	tým	k1gInSc1
Hyundai	Hyunda	k1gFnSc2
Shell	Shella	k1gFnPc2
Mobis	Mobis	k1gFnSc2
WRT	WRT	kA
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
vedl	vést	k5eAaImAgMnS
nad	nad	k7c7
týmem	tým	k1gInSc7
Toyota	toyota	k1gFnSc1
Gazoo	Gazoo	k1gMnSc1
Racing	Racing	k1gInSc1
WRT	WRT	kA
37	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Kategorii	kategorie	k1gFnSc6
WRC	WRC	kA
2	#num#	k4
Pro	pro	k7c4
vedla	vést	k5eAaImAgFnS
polská	polský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
Łukasz	Łukasz	k1gMnSc1
Pieniążek	Pieniążek	k1gMnSc1
a	a	k8xC
Kamil	Kamil	k1gMnSc1
Heller	Heller	k1gMnSc1
před	před	k7c7
Gusem	Gus	k1gInSc7
Greensmithem	Greensmith	k1gInSc7
a	a	k8xC
Elliottem	Elliott	k1gMnSc7
Edmondsonem	Edmondson	k1gMnSc7
o	o	k7c4
4	#num#	k4
body	bod	k1gInPc4
v	v	k7c6
pořadí	pořadí	k1gNnSc6
jezdců	jezdec	k1gMnPc2
a	a	k8xC
spolujezdců	spolujezdec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mads	Madsa	k1gFnPc2
Ø	Ø	k1gInSc4
a	a	k8xC
Torstein	Torstein	k2eAgInSc4d1
Eriksen	Eriksen	k1gInSc4
byli	být	k5eAaImAgMnP
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
8	#num#	k4
bodů	bod	k1gInPc2
za	za	k7c7
britskou	britský	k2eAgFnSc7d1
posádkou	posádka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohár	pohár	k1gInSc1
konstruktérů	konstruktér	k1gMnPc2
vedl	vést	k5eAaImAgInS
tým	tým	k1gInSc1
M-Sport	M-Sport	k1gInSc1
Ford	ford	k1gInSc4
WRT	WRT	kA
před	před	k7c7
týmem	tým	k1gInSc7
Citroën	Citroën	k1gMnSc1
Total	totat	k5eAaImAgMnS
o	o	k7c4
70	#num#	k4
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
byla	být	k5eAaImAgFnS
Škoda	škoda	k1gFnSc1
Motorsport	Motorsport	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
WRC	WRC	kA
2	#num#	k4
vedl	vést	k5eAaImAgInS
Benito	Benit	k2eAgNnSc4d1
Guerra	Guerr	k1gMnSc2
a	a	k8xC
Jaime	Jaim	k1gInSc5
Zapata	Zapat	k1gMnSc2
3	#num#	k4
body	bod	k1gInPc4
před	před	k7c7
posádkou	posádka	k1gFnSc7
Ole	Ola	k1gFnSc6
Christian	Christian	k1gMnSc1
Veiby	Veiba	k1gFnSc2
a	a	k8xC
Jonas	Jonas	k1gMnSc1
Andersson	Andersson	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
byla	být	k5eAaImAgFnS
dvojice	dvojice	k1gFnSc1
Nikolaj	Nikolaj	k1gMnSc1
Grjazin	Grjazin	k1gMnSc1
a	a	k8xC
Jaroslav	Jaroslav	k1gMnSc1
Fedorov	Fedorov	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Průběh	průběh	k1gInSc1
závodu	závod	k1gInSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
etapa	etapa	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
etapa	etapa	k1gFnSc1
Chilské	chilský	k2eAgFnSc2d1
rallye	rallye	k1gFnSc2
měla	mít	k5eAaImAgFnS
celkem	celkem	k6eAd1
6	#num#	k4
rychlostních	rychlostní	k2eAgFnPc2d1
zkoušek	zkouška	k1gFnPc2
o	o	k7c6
délce	délka	k1gFnSc6
125,27	125,27	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
erzeta	erzeta	k1gFnSc1
El	Ela	k1gFnPc2
Pinar	Pinara	k1gFnPc2
měřila	měřit	k5eAaImAgFnS
17,11	17,11	k4
km	km	kA
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
a	a	k8xC
čtvrtá	čtvrtá	k1gFnSc1
El	Ela	k1gFnPc2
Puma	puma	k2eAgFnPc2d1
30,72	30,72	k4
km	km	kA
a	a	k8xC
třetí	třetí	k4xOgFnSc1
a	a	k8xC
pátá	pátý	k4xOgFnSc1
Espigado	Espigada	k1gFnSc5
měla	mít	k5eAaImAgFnS
22,26	22,26	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
a	a	k8xC
zároveň	zároveň	k6eAd1
nejkratší	krátký	k2eAgMnSc1d3
RZ	RZ	kA
celého	celý	k2eAgInSc2d1
dne	den	k1gInSc2
měla	mít	k5eAaImAgFnS
2,20	2,20	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepší	dobrý	k2eAgInSc4d3
čas	čas	k1gInSc4
v	v	k7c6
první	první	k4xOgFnSc6
šotolinové	šotolinový	k2eAgFnSc6d1
rychlostní	rychlostní	k2eAgFnSc6d1
zkoušce	zkouška	k1gFnSc6
zaznamenaly	zaznamenat	k5eAaPmAgFnP
2	#num#	k4
posádky	posádka	k1gFnPc1
<g/>
:	:	kIx,
Kris	kris	k1gInSc1
Meeke	Meek	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Sebastian	Sebastian	k1gMnSc1
Marshall	Marshall	k1gMnSc1
a	a	k8xC
Jari-Matti	Jari-Matt	k2eAgMnPc1d1
Latvala	Latvala	k1gMnPc1
<g/>
/	/	kIx~
<g/>
Miikka	Miikka	k1gMnSc1
Anttila	Anttil	k1gMnSc2
<g/>
,	,	kIx,
2,1	2,1	k4
s	s	k7c7
za	za	k7c7
nimi	on	k3xPp3gMnPc7
dojel	dojet	k5eAaPmAgMnS
Elfyn	Elfyn	k1gMnSc1
Evans	Evansa	k1gFnPc2
a	a	k8xC
Scott	Scott	k1gMnSc1
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc4
erzetu	erzeta	k1gFnSc4
vyhrála	vyhrát	k5eAaPmAgFnS
posádka	posádka	k1gFnSc1
Ott	Ott	k1gMnSc1
Tänak	Tänak	k1gMnSc1
a	a	k8xC
Martin	Martin	k1gMnSc1
Järveoja	Järveoja	k1gMnSc1
s	s	k7c7
vozem	vůz	k1gInSc7
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc1
WRC	WRC	kA
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
díky	díky	k7c3
rychlému	rychlý	k2eAgInSc3d1
času	čas	k1gInSc3
posunul	posunout	k5eAaPmAgInS
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
4	#num#	k4
<g/>
.	.	kIx.
pozice	pozice	k1gFnSc1
se	se	k3xPyFc4
na	na	k7c4
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
posunul	posunout	k5eAaPmAgMnS
Sébastien	Sébastien	k2eAgMnSc1d1
Ogier	Ogier	k1gMnSc1
a	a	k8xC
Julien	Julien	k1gInSc1
Ingrassia	Ingrassium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posun	posun	k1gInSc1
zaznamenal	zaznamenat	k5eAaPmAgInS
také	také	k9
Thierry	Thierr	k1gInPc4
Neuville	Neuville	k1gFnSc2
a	a	k8xC
Nicolas	Nicolasa	k1gFnPc2
Gilsoul	Gilsoula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
velký	velký	k2eAgInSc1d1
propad	propad	k1gInSc1
v	v	k7c6
pořadí	pořadí	k1gNnSc6
zaznamenaly	zaznamenat	k5eAaPmAgFnP
3	#num#	k4
posádky	posádka	k1gFnPc1
<g/>
:	:	kIx,
Kris	kris	k1gInSc1
Meeke	Meek	k1gFnSc2
a	a	k8xC
Sebastian	Sebastian	k1gMnSc1
Marshall	Marshall	k1gMnSc1
(	(	kIx(
<g/>
+	+	kIx~
15,3	15,3	k4
s	s	k7c7
<g/>
;	;	kIx,
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnPc4
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jari-Matti	Jari-Matti	k1gNnSc4
Latvala	Latvala	k1gMnSc2
a	a	k8xC
Miikka	Miikek	k1gMnSc2
Anttila	Anttil	k1gMnSc2
(	(	kIx(
<g/>
+	+	kIx~
16,3	16,3	k4
s	s	k7c7
<g/>
;	;	kIx,
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnPc4
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
Elfyn	Elfyn	k1gMnSc1
Evans	Evans	k1gInSc4
s	s	k7c7
Scottem	Scott	k1gMnSc7
Martinem	Martin	k1gMnSc7
(	(	kIx(
<g/>
+	+	kIx~
16,7	16,7	k4
s	s	k7c7
<g/>
;	;	kIx,
z	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnPc4
na	na	k7c4
6	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepší	dobrý	k2eAgInSc4d3
čas	čas	k1gInSc4
na	na	k7c6
třetí	třetí	k4xOgFnSc6
erzetě	erzeta	k1gFnSc6
měl	mít	k5eAaImAgMnS
Thierry	Thierra	k1gFnPc4
Neuville	Neuville	k1gNnPc2
a	a	k8xC
Nicolas	Nicolasa	k1gFnPc2
Gilsoul	Gilsoula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k6eAd1
se	se	k3xPyFc4
posunul	posunout	k5eAaPmAgInS
Jari-Matti	Jari-Matť	k1gFnSc2
Latvala	Latvala	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
vyměnil	vyměnit	k5eAaPmAgMnS
pozici	pozice	k1gFnSc4
s	s	k7c7
Krisem	kris	k1gInSc7
Meekem	Meeek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgMnSc1
rychlostní	rychlostní	k2eAgMnSc1d1
test	test	k1gMnSc1
vyhrál	vyhrát	k5eAaPmAgMnS
opět	opět	k6eAd1
Ott	Ott	k1gMnSc1
Tänak	Tänak	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
navýšil	navýšit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
náskok	náskok	k1gInSc4
na	na	k7c4
druhého	druhý	k4xOgMnSc4
Ogiera	Ogier	k1gMnSc4
na	na	k7c4
17,3	17,3	k4
s.	s.	k?
Ogier	Ogier	k1gMnSc1
ztratil	ztratit	k5eAaPmAgMnS
na	na	k7c4
Tänak	Tänak	k1gInSc4
ve	v	k7c6
4	#num#	k4
<g/>
.	.	kIx.
erzetě	erzetě	k6eAd1
celkem	celek	k1gInSc7
11,2	11,2	k4
s.	s.	k?
Jari-Matti	Jari-Matti	k1gNnSc1
Latvala	Latvala	k1gMnSc1
se	se	k3xPyFc4
díky	díky	k7c3
pomalému	pomalý	k2eAgInSc3d1
času	čas	k1gInSc3
Neuvilla	Neuvillo	k1gNnSc2
posunul	posunout	k5eAaPmAgInS
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
celkovém	celkový	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patý	Patý	k1gMnSc1
test	test	k1gMnSc1
opět	opět	k6eAd1
patřil	patřit	k5eAaImAgMnS
Tänakovi	Tänak	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
svůj	svůj	k3xOyFgInSc4
náskok	náskok	k1gInSc4
na	na	k7c4
Ogiera	Ogiero	k1gNnPc4
navýšil	navýšit	k5eAaPmAgInS
o	o	k7c4
5,8	5,8	k4
s	s	k7c7
<g/>
;	;	kIx,
celková	celkový	k2eAgFnSc1d1
výhoda	výhoda	k1gFnSc1
Tänaka	Tänak	k1gMnSc2
na	na	k7c4
Ogiera	Ogier	k1gMnSc4
byla	být	k5eAaImAgFnS
po	po	k7c6
4	#num#	k4
<g/>
.	.	kIx.
erzetě	erzetě	k6eAd1
celkem	celek	k1gInSc7
23,1	23,1	k4
s.	s.	k?
Šestý	šestý	k4xOgInSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
poslední	poslední	k2eAgInSc4d1
test	test	k1gInSc4
dne	den	k1gInSc2
vyhrál	vyhrát	k5eAaPmAgMnS
Sébastien	Sébastien	k2eAgMnSc1d1
Loeb	Loeb	k1gMnSc1
a	a	k8xC
Daniel	Daniel	k1gMnSc1
Elena	Elena	k1gFnSc1
s	s	k7c7
vozem	vůz	k1gInSc7
Hyundai	Hyunda	k1gFnSc2
i	i	k9
<g/>
20	#num#	k4
Coupe	coup	k1gInSc5
WRC	WRC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
0,6	0,6	k4
s	s	k7c7
horší	zlý	k2eAgInPc1d2
čas	čas	k1gInSc1
zaznamenal	zaznamenat	k5eAaPmAgInS
Thierry	Thierr	k1gInPc4
Neuville	Neuville	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInPc4
čas	čas	k1gInSc4
se	se	k3xPyFc4
ztrátou	ztráta	k1gFnSc7
2,0	2,0	k4
s	s	k7c7
měl	mít	k5eAaImAgInS
Kris	kris	k1gInSc4
Meeke	Meek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pořadí	pořadí	k1gNnSc1
po	po	k7c6
1	#num#	k4
<g/>
.	.	kIx.
etapě	etapa	k1gFnSc6
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
Ott	Ott	k1gFnSc1
Tänak	Tänak	k1gInSc1
Toyota	toyota	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
12,86	12,86	k4
Sébastien	Sébastien	k2eAgInSc1d1
Loeb	Loeb	k1gInSc1
Hyundai	Hyunda	k1gFnSc2
<g/>
+	+	kIx~
48,7	48,7	k4
</s>
<s>
2	#num#	k4
Sébastien	Sébastien	k2eAgInSc1d1
Ogier	Ogier	k1gInSc1
Citroën	Citroëna	k1gFnPc2
<g/>
+	+	kIx~
22,47	22,47	k4
Elfyn	Elfyna	k1gFnPc2
Evans	Evansa	k1gFnPc2
Ford	ford	k1gInSc1
<g/>
+	+	kIx~
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1,4	1,4	k4
</s>
<s>
3	#num#	k4
Jari-Matti	Jari-Matti	k1gNnSc7
Latvala	Latvala	k1gFnSc2
Toyota	toyota	k1gFnSc1
<g/>
+	+	kIx~
28,88	28,88	k4
Andreas	Andreas	k1gInSc1
Mikkelsen	Mikkelsen	k1gInSc4
Hyundai	Hyunda	k1gFnSc2
<g/>
+	+	kIx~
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8,1	8,1	k4
</s>
<s>
4	#num#	k4
Thierry	Thierra	k1gMnSc2
Neuville	Neuville	k1gInSc4
Hyundai	Hyundai	k1gNnSc2
<g/>
+	+	kIx~
29,59	29,59	k4
Teemu	Teem	k1gInSc2
Suninen	Suninen	k2eAgInSc1d1
Ford	ford	k1gInSc1
<g/>
+	+	kIx~
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9,1	9,1	k4
</s>
<s>
5	#num#	k4
Kris	kris	k1gInSc1
Meeke	Meeke	k1gNnSc2
Toyota	toyota	k1gFnSc1
<g/>
+	+	kIx~
46,510	46,510	k4
Esapekka	Esapekek	k1gInSc2
Lappi	Lappe	k1gFnSc4
Citroën	Citroën	k1gNnSc1
<g/>
+	+	kIx~
2	#num#	k4
<g/>
:	:	kIx,
<g/>
18,3	18,3	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
etapa	etapa	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
etapa	etapa	k1gFnSc1
měla	mít	k5eAaImAgFnS
také	také	k9
6	#num#	k4
rychlostních	rychlostní	k2eAgFnPc2d1
zkoušek	zkouška	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
dohromady	dohromady	k6eAd1
měly	mít	k5eAaImAgInP
129,16	129,16	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedmá	sedmý	k4xOgFnSc1
erzeta	erzet	k2eAgFnSc1d1
Rio	Rio	k1gFnSc1
Lia	Lia	k1gMnPc2
měřila	měřit	k5eAaImAgFnS
24,90	24,90	k4
km	km	kA
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
Maria	Maria	k1gFnSc1
Las	laso	k1gNnPc2
Cruces	Cruces	k1gMnSc1
23,09	23,09	k4
km	km	kA
a	a	k8xC
třetí	třetí	k4xOgInSc4
Pelun	Pelun	k1gInSc4
měla	mít	k5eAaImAgFnS
16,59	16,59	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
z	z	k7c2
erzet	erzeta	k1gFnPc2
se	se	k3xPyFc4
jela	jet	k5eAaImAgFnS
dvakrát	dvakrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedmou	sedmý	k4xOgFnSc4
rychlostní	rychlostní	k2eAgFnSc4d1
zkoušku	zkouška	k1gFnSc4
vyhrál	vyhrát	k5eAaPmAgInS
Neuville	Neuville	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
posunul	posunout	k5eAaPmAgInS
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
celkovém	celkový	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
a	a	k8xC
Latvalu	Latvala	k1gFnSc4
odsunul	odsunout	k5eAaPmAgInS
na	na	k7c4
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc1d1
jezdec	jezdec	k1gInSc1
Kris	kris	k1gInSc1
Meeke	Meeke	k1gInSc1
šel	jít	k5eAaImAgInS
přes	přes	k7c4
střechu	střecha	k1gFnSc4
a	a	k8xC
erzetu	erzeta	k1gFnSc4
dokončil	dokončit	k5eAaPmAgMnS
se	s	k7c7
ztrátou	ztráta	k1gFnSc7
6	#num#	k4
<g/>
:	:	kIx,
<g/>
26,1	26,1	k4
s	s	k7c7
na	na	k7c4
Neuvilla	Neuvillo	k1gNnPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamenalo	znamenat	k5eAaImAgNnS
propad	propad	k1gInSc4
o	o	k7c4
9	#num#	k4
místo	místo	k1gNnSc4
na	na	k7c6
14	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meeke	Meek	k1gInSc2
v	v	k7c6
cíli	cíl	k1gInSc6
poté	poté	k6eAd1
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
V	v	k7c6
pomalé	pomalý	k2eAgFnSc6d1
zatáčce	zatáčka	k1gFnSc6
jsme	být	k5eAaImIp1nP
byli	být	k5eAaImAgMnP
moc	moc	k6eAd1
širocí	široký	k2eAgMnPc1d1
a	a	k8xC
o	o	k7c4
stromy	strom	k1gInPc4
poškodili	poškodit	k5eAaPmAgMnP
auto	auto	k1gNnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
Evans	Evans	k1gInSc1
na	na	k7c6
erzetě	erzeta	k1gFnSc6
zaznamenal	zaznamenat	k5eAaPmAgMnS
krizi	krize	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
okomentoval	okomentovat	k5eAaPmAgMnS
takto	takto	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Měl	mít	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
velkou	velký	k2eAgFnSc4d1
krizi	krize	k1gFnSc4
a	a	k8xC
byl	být	k5eAaImAgInS
úplně	úplně	k6eAd1
mimo	mimo	k7c4
trať	trať	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
autu	aut	k1gInSc2
nic	nic	k3yNnSc1
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
“	“	k?
V	v	k7c6
osmé	osmý	k4xOgFnSc6
rychlostní	rychlostní	k2eAgFnSc6d1
zkoušce	zkouška	k1gFnSc6
měl	mít	k5eAaImAgMnS
Neuville	Neuville	k1gInSc4
havárii	havárie	k1gFnSc3
a	a	k8xC
ze	z	k7c2
soutěže	soutěž	k1gFnSc2
odstoupil	odstoupit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
nehodě	nehoda	k1gFnSc3
Neuvilla	Neuvill	k1gMnSc2
se	se	k3xPyFc4
Latvala	Latvala	k1gMnSc1
posunul	posunout	k5eAaPmAgInS
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Devátou	devátý	k4xOgFnSc4
rychlostní	rychlostní	k2eAgFnSc4d1
zkoušku	zkouška	k1gFnSc4
znovu	znovu	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
Ott	Ott	k1gMnSc1
Tänak	Tänak	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
nadále	nadále	k6eAd1
zvyšoval	zvyšovat	k5eAaImAgInS
náskok	náskok	k1gInSc4
na	na	k7c4
ostatní	ostatní	k2eAgMnPc4d1
jezdce	jezdec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstup	odstup	k1gInSc1
Ogiera	Ogiero	k1gNnSc2
na	na	k7c4
Tänak	Tänak	k1gInSc4
činil	činit	k5eAaImAgMnS
34,6	34,6	k4
s	s	k7c7
<g/>
,	,	kIx,
týmový	týmový	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
Latvala	Latvala	k1gFnSc2
38,5	38,5	k4
s	s	k7c7
a	a	k8xC
čtvrtý	čtvrtý	k4xOgInSc1
Loeb	Loeb	k1gInSc1
52,1	52,1	k4
s.	s.	k?
</s>
<s>
Desátá	desátý	k4xOgFnSc1
rychlostní	rychlostní	k2eAgFnSc1d1
zkouška	zkouška	k1gFnSc1
žádnou	žádný	k3yNgFnSc4
změnu	změna	k1gFnSc4
v	v	k7c6
TOP	topit	k5eAaImRp2nS
10	#num#	k4
nepřinesla	přinést	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězem	vítěz	k1gMnSc7
erzety	erzet	k1gMnPc4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Ogier	Ogier	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
najel	najet	k5eAaPmAgMnS
na	na	k7c4
Loeba	Loeb	k1gMnSc4
0,4	0,4	k4
s.	s.	k?
Třetí	třetí	k4xOgMnSc1
dokončil	dokončit	k5eAaPmAgMnS
zkoušku	zkouška	k1gFnSc4
Meeke	Meek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tänak	Tänak	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
zajel	zajet	k5eAaPmAgMnS
4	#num#	k4
<g/>
.	.	kIx.
čas	čas	k1gInSc4
<g/>
,	,	kIx,
komentoval	komentovat	k5eAaBmAgMnS
ztrátu	ztráta	k1gFnSc4
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Ztratil	ztratit	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
tu	tu	k6eAd1
4	#num#	k4
vteřiny	vteřina	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
tak	tak	k6eAd1
hrozné	hrozný	k2eAgNnSc1d1
vzhledem	vzhledem	k7c3
k	k	k7c3
volbě	volba	k1gFnSc3
pneu	pneu	k1gNnSc2
na	na	k7c4
další	další	k2eAgNnSc4d1
RZ	RZ	kA
<g/>
.	.	kIx.
<g/>
“	“	k?
Jedenáctou	jedenáctý	k4xOgFnSc4
erzetě	erzeta	k1gFnSc6
vyhrál	vyhrát	k5eAaPmAgMnS
právě	právě	k9
Tänak	Tänak	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
si	se	k3xPyFc3
dále	daleko	k6eAd2
upevňoval	upevňovat	k5eAaImAgMnS
náskok	náskok	k1gInSc4
na	na	k7c4
Ogiera	Ogier	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
dvanácté	dvanáctý	k4xOgFnSc6
erzetě	erzeta	k1gFnSc6
kraloval	kralovat	k5eAaImAgMnS
Loeb	Loeb	k1gInSc4
<g/>
;	;	kIx,
do	do	k7c2
10	#num#	k4
sekund	sekunda	k1gFnPc2
se	se	k3xPyFc4
vešli	vejít	k5eAaPmAgMnP
jen	jen	k9
2	#num#	k4
jezdci	jezdec	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
Evanse	Evanse	k1gFnSc1
(	(	kIx(
<g/>
+	+	kIx~
0,7	0,7	k4
s	s	k7c7
<g/>
)	)	kIx)
a	a	k8xC
Lappi	Lappe	k1gFnSc4
(	(	kIx(
<g/>
+	+	kIx~
<g/>
4,7	4,7	k4
s	s	k7c7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tänak	Tänak	k1gMnSc1
dojel	dojet	k5eAaPmAgMnS
do	do	k7c2
cíle	cíl	k1gInSc2
se	s	k7c7
ztrátou	ztráta	k1gFnSc7
13,9	13,9	k4
na	na	k7c4
Loeba	Loeb	k1gMnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
i	i	k9
před	před	k7c4
tuto	tento	k3xDgFnSc4
ztrátu	ztráta	k1gFnSc4
měl	mít	k5eAaImAgMnS
na	na	k7c4
Ogiera	Ogier	k1gMnSc4
stále	stále	k6eAd1
30,3	30,3	k4
s.	s.	k?
Tänak	Tänak	k1gMnSc1
v	v	k7c6
cíli	cíl	k1gInSc6
těžké	těžký	k2eAgFnSc2d1
podmínky	podmínka	k1gFnSc2
okomentoval	okomentovat	k5eAaPmAgMnS
takto	takto	k6eAd1
„	„	k?
<g/>
"	"	kIx"
<g/>
Ztratil	ztratit	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
čas	čas	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
prší	pršet	k5eAaImIp3nS
a	a	k8xC
také	také	k9
tam	tam	k6eAd1
byla	být	k5eAaImAgFnS
velká	velký	k2eAgFnSc1d1
mlha	mlha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsem	být	k5eAaImIp1nS
rád	rád	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
mám	mít	k5eAaImIp1nS
teď	teď	k6eAd1
větší	veliký	k2eAgInSc4d2
náskok	náskok	k1gInSc4
než	než	k8xS
ráno	ráno	k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
Dobrému	dobrý	k2eAgInSc3d1
výsledku	výsledek	k1gInSc3
nepřáli	přát	k5eNaImAgMnP
technické	technický	k2eAgInPc4d1
problémy	problém	k1gInPc4
Latvaly	Latval	k1gInPc4
<g/>
,	,	kIx,
když	když	k8xS
ho	on	k3xPp3gInSc4
postihla	postihnout	k5eAaPmAgFnS
technická	technický	k2eAgFnSc1d1
závada	závada	k1gFnSc1
a	a	k8xC
přidělených	přidělený	k2eAgFnPc2d1
10	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamenalo	znamenat	k5eAaImAgNnS
propad	propad	k1gInSc4
z	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnPc4
na	na	k7c4
13	#num#	k4
<g/>
.	.	kIx.
pozici	pozice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pořadí	pořadí	k1gNnSc1
po	po	k7c6
2	#num#	k4
<g/>
.	.	kIx.
etapě	etapa	k1gFnSc6
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
Ott	Ott	k1gFnSc1
Tänak	Tänak	k1gInSc1
Toyota	toyota	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
41	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5,56	5,56	k4
Esapekka	Esapekka	k1gMnSc1
Lappi	Lappe	k1gFnSc4
Citroën	Citroëna	k1gFnPc2
<g/>
+	+	kIx~
3	#num#	k4
<g/>
:	:	kIx,
<g/>
13,3	13,3	k4
</s>
<s>
2	#num#	k4
Sébastien	Sébastien	k2eAgInSc1d1
Ogier	Ogier	k1gInSc1
Citroën	Citroëna	k1gFnPc2
<g/>
+	+	kIx~
30,37	30,37	k4
Andreas	Andreas	k1gInSc1
Mikkelsen	Mikkelsen	k1gInSc4
Hyundai	Hyunda	k1gFnSc2
<g/>
+	+	kIx~
3	#num#	k4
<g/>
:	:	kIx,
<g/>
43,4	43,4	k4
</s>
<s>
3	#num#	k4
Sébastien	Sébastien	k2eAgInSc1d1
Loeb	Loeb	k1gInSc1
Hyundai	Hyunda	k1gFnSc2
<g/>
+	+	kIx~
35,48	35,48	k4
Kalle	Kalle	k1gNnPc2
Rovanperä	Rovanperä	k1gFnSc1
Škoda	škoda	k1gFnSc1
<g/>
+	+	kIx~
6	#num#	k4
<g/>
:	:	kIx,
<g/>
21,5	21,5	k4
</s>
<s>
4	#num#	k4
Elfyn	Elfyna	k1gFnPc2
Evans	Evansa	k1gFnPc2
Ford	ford	k1gInSc1
<g/>
+	+	kIx~
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6,39	6,39	k4
Mads	Madsa	k1gFnPc2
Ø	Ø	k1gMnSc1
Citroën	Citroën	k1gMnSc1
<g/>
+	+	kIx~
6	#num#	k4
<g/>
:	:	kIx,
<g/>
55,4	55,4	k4
</s>
<s>
5	#num#	k4
Teemu	Teem	k1gInSc2
Suninen	Suninen	k2eAgInSc1d1
Ford	ford	k1gInSc1
<g/>
+	+	kIx~
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3,010	3,010	k4
Kris	kris	k1gInSc1
Meeke	Meeke	k1gNnSc2
Toyota	toyota	k1gFnSc1
<g/>
+	+	kIx~
7	#num#	k4
<g/>
:	:	kIx,
<g/>
21,9	21,9	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
etapa	etapa	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
etapa	etapa	k1gFnSc1
nabídla	nabídnout	k5eAaPmAgFnS
celkem	celkem	k6eAd1
4	#num#	k4
rychlostní	rychlostní	k2eAgFnSc2d1
zkoušky	zkouška	k1gFnSc2
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
62,16	62,16	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bio	Bio	k?
Bio	Bio	k?
měla	mít	k5eAaImAgFnS
14,41	14,41	k4
km	km	kA
<g/>
,	,	kIx,
Lircay	Lirca	k1gMnPc4
byla	být	k5eAaImAgFnS
nejdelší	dlouhý	k2eAgFnSc1d3
a	a	k8xC
měřila	měřit	k5eAaImAgFnS
18,06	18,06	k4
<g/>
,	,	kIx,
San	San	k1gFnSc1
Nicolà	Nicolà	k1gFnPc2
měla	mít	k5eAaImAgFnS
15,28	15,28	k4
a	a	k8xC
jako	jako	k8xS,k8xC
powerstage	powerstage	k1gFnSc1
se	se	k3xPyFc4
jela	jet	k5eAaImAgFnS
podruhé	podruhé	k6eAd1
erzeta	erzet	k5eAaPmNgFnS
Bio	Bio	k?
Bio	Bio	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Najlepší	Najlepší	k2eAgInSc1d1
čas	čas	k1gInSc1
měl	mít	k5eAaImAgInS
na	na	k7c6
zkoušce	zkouška	k1gFnSc6
Bio	Bio	k?
Bio	Bio	k?
1	#num#	k4
Brit	Brit	k1gMnSc1
Kris	kris	k1gInSc4
Meeke	Meek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
za	za	k7c7
sebou	se	k3xPyFc7
nechal	nechat	k5eAaPmAgMnS
Latvalu	Latvala	k1gFnSc4
(	(	kIx(
<g/>
0,4	0,4	k4
s	s	k7c7
<g/>
)	)	kIx)
a	a	k8xC
Loeba	Loeba	k1gMnSc1
(	(	kIx(
<g/>
1,2	1,2	k4
s	s	k7c7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tänak	Tänak	k1gMnSc1
byl	být	k5eAaImAgMnS
šestý	šestý	k4xOgMnSc1
se	s	k7c7
ztrátou	ztráta	k1gFnSc7
5,7	5,7	k4
s.	s.	k?
Čtrnáctá	čtrnáctý	k4xOgFnSc1
erzeta	erzeta	k1gFnSc1
změny	změna	k1gFnSc2
v	v	k7c6
nejlepší	dobrý	k2eAgFnSc6d3
desítce	desítka	k1gFnSc6
nepřinesla	přinést	k5eNaPmAgFnS
<g/>
;	;	kIx,
vítězem	vítěz	k1gMnSc7
testu	test	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Sébastien	Sébastien	k2eAgMnSc1d1
Ogier	Ogier	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
najel	najet	k5eAaPmAgMnS
na	na	k7c4
druhého	druhý	k4xOgMnSc4
Sébastien	Sébastien	k1gInSc1
Loeba	Loeba	k1gFnSc1
4,2	4,2	k4
s	s	k7c7
a	a	k8xC
třetího	třetí	k4xOgNnSc2
Otta	Otta	k1gMnSc1
Tänaka	Tänak	k1gMnSc4
5,2	5,2	k4
s.	s.	k?
V	v	k7c6
cíli	cíl	k1gInSc6
Tänak	Tänak	k1gMnSc1
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Oba	dva	k4xCgMnPc1
Francouzi	Francouz	k1gMnPc1
jsou	být	k5eAaImIp3nP
rychlí	rychlý	k2eAgMnPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
spolu	spolu	k6eAd1
bojují	bojovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Já	já	k3xPp1nSc1
jedu	jet	k5eAaImIp1nS
v	v	k7c6
tempu	tempo	k1gNnSc6
a	a	k8xC
trochu	trochu	k6eAd1
šetřím	šetřit	k5eAaImIp1nS
pneu	pneu	k1gFnSc4
na	na	k7c4
PS	PS	kA
<g/>
.	.	kIx.
<g/>
“	“	k?
Podobná	podobný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
také	také	k9
v	v	k7c6
následující	následující	k2eAgFnSc6d1
patnácté	patnáctý	k4xOgFnSc6
erzetě	erzeta	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
před	před	k7c4
sebe	sebe	k3xPyFc4
pustil	pustit	k5eAaPmAgMnS
opět	opět	k6eAd1
dvojici	dvojice	k1gFnSc4
Francouzů	Francouz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězem	vítěz	k1gMnSc7
patnácté	patnáctý	k4xOgFnSc2
erzety	erzeta	k1gFnSc2
tedy	tedy	k9
byl	být	k5eAaImAgMnS
Sébastien	Sébastien	k2eAgMnSc1d1
Loeb	Loeb	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
najel	najet	k5eAaPmAgMnS
na	na	k7c4
Ogiera	Ogier	k1gMnSc4
0,7	0,7	k4
s	s	k7c7
a	a	k8xC
na	na	k7c4
Tänaka	Tänak	k1gMnSc4
3,5	3,5	k4
s.	s.	k?
Šestnáctou	šestnáctý	k4xOgFnSc4
rychlostní	rychlostní	k2eAgFnSc4d1
zkoušku	zkouška	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
zároveň	zároveň	k6eAd1
jela	jet	k5eAaImAgFnS
i	i	k9
jako	jako	k9
powestage	powestage	k6eAd1
<g/>
,	,	kIx,
vyhrál	vyhrát	k5eAaPmAgMnS
Ott	Ott	k1gMnSc1
Tänak	Tänak	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
bodů	bod	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
najel	najet	k5eAaPmAgMnS
na	na	k7c4
Ogiera	Ogier	k1gMnSc4
1,3	1,3	k4
s	s	k7c7
(	(	kIx(
<g/>
4	#num#	k4
body	bod	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Latvalu	Latval	k1gInSc2
3,1	3,1	k4
s	s	k7c7
(	(	kIx(
<g/>
3	#num#	k4
body	bod	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Loeba	Loeba	k1gFnSc1
3,8	3,8	k4
s	s	k7c7
(	(	kIx(
<g/>
2	#num#	k4
body	bod	k1gInPc4
<g/>
)	)	kIx)
a	a	k8xC
Meeka	Meeka	k1gFnSc1
4,0	4,0	k4
s	s	k7c7
(	(	kIx(
<g/>
1	#num#	k4
bod	bod	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Své	svůj	k3xOyFgNnSc1
premiérové	premiérový	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
v	v	k7c6
Chile	Chile	k1gNnSc6
okomentoval	okomentovat	k5eAaPmAgInS
takto	takto	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
skvělé	skvělý	k2eAgNnSc1d1
tu	ten	k3xDgFnSc4
při	při	k7c6
premiéře	premiéra	k1gFnSc6
vyhrát	vyhrát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
náročný	náročný	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
,	,	kIx,
vyžadoval	vyžadovat	k5eAaImAgMnS
hodně	hodně	k6eAd1
koncentrace	koncentrace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztratili	ztratit	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
v	v	k7c6
předchozích	předchozí	k2eAgFnPc6d1
dvou	dva	k4xCgFnPc6
soutěžích	soutěž	k1gFnPc6
vedení	vedení	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
jsem	být	k5eAaImIp1nS
rád	rád	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
konečně	konečně	k6eAd1
povedlo	povést	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
“	“	k?
Ogier	Ogier	k1gMnSc1
hodnotil	hodnotit	k5eAaImAgMnS
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k6eAd1
vzhledem	vzhledem	k7c3
k	k	k7c3
situaci	situace	k1gFnSc3
pozitivně	pozitivně	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Explodoval	explodovat	k5eAaBmAgMnS
mně	já	k3xPp1nSc3
v	v	k7c6
autě	auto	k1gNnSc6
hasičák	hasičák	k1gInSc4
a	a	k8xC
pak	pak	k6eAd1
tu	ten	k3xDgFnSc4
byl	být	k5eAaImAgInS
hrozný	hrozný	k2eAgInSc1d1
smrad	smrad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgNnSc1
místo	místo	k1gNnSc1
po	po	k7c6
boji	boj	k1gInSc6
s	s	k7c7
Loebem	Loeb	k1gInSc7
je	být	k5eAaImIp3nS
dobré	dobrý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
Loeb	Loeb	k1gMnSc1
poděkoval	poděkovat	k5eAaPmAgMnS
svému	svůj	k3xOyFgInSc3
týmu	tým	k1gInSc3
za	za	k7c4
příležitost	příležitost	k1gFnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Hyundai	Hyunda	k1gFnSc2
odvedl	odvést	k5eAaPmAgMnS
skvělou	skvělý	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
skvělou	skvělý	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
<g/>
,	,	kIx,
hlavně	hlavně	k6eAd1
poslední	poslední	k2eAgInPc4d1
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
Celkové	celkový	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
</s>
<s>
#	#	kIx~
</s>
<s>
Nr	Nr	k?
<g/>
.	.	kIx.
</s>
<s>
Jezdec	jezdec	k1gMnSc1
</s>
<s>
Navigátor	navigátor	k1gMnSc1
</s>
<s>
Vůz	vůz	k1gInSc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
Ztráta	ztráta	k1gFnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
Absolutní	absolutní	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Ott	Ott	k?
Tänak	Tänak	k1gInSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Järveoja	Järveoja	k1gMnSc1
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
Toyota	toyota	k1gFnSc1
Gazoo	Gazoo	k6eAd1
Racing	Racing	k1gInSc1
WRT	WRT	kA
</s>
<s>
WRC	WRC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
53.8	53.8	k4
</s>
<s>
25	#num#	k4
<g/>
+	+	kIx~
<g/>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Ogier	Ogier	k1gInSc1
</s>
<s>
Julien	Julien	k1gInSc1
Ingrassia	Ingrassium	k1gNnSc2
</s>
<s>
Citroën	Citroën	k1gMnSc1
C3	C3	k1gFnSc2
WRC	WRC	kA
</s>
<s>
Citroën	Citroën	k1gInSc1
Total	totat	k5eAaImAgInS
WRT	WRT	kA
</s>
<s>
WRC	WRC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
16.9	16.9	k4
</s>
<s>
+23.1	+23.1	k4
</s>
<s>
18	#num#	k4
<g/>
+	+	kIx~
<g/>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Loeb	Loeb	k1gInSc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Elena	Elena	k1gFnSc1
</s>
<s>
Hyundai	Hyundai	k6eAd1
i	i	k9
<g/>
20	#num#	k4
Coupe	coup	k1gInSc5
WRC	WRC	kA
</s>
<s>
Hyundai	Hyundai	k6eAd1
Shell	Shell	k1gMnSc1
Mobis	Mobis	k1gFnSc2
WRT	WRT	kA
</s>
<s>
WRC	WRC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
24.0	24.0	k4
</s>
<s>
+30.2	+30.2	k4
</s>
<s>
15	#num#	k4
<g/>
+	+	kIx~
<g/>
2	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
Elfyn	Elfyn	k1gNnSc1
Evans	Evansa	k1gFnPc2
</s>
<s>
Scott	Scott	k1gMnSc1
Martin	Martin	k1gMnSc1
</s>
<s>
Ford	ford	k1gInSc1
Fiesta	fiesta	k1gFnSc1
WRC	WRC	kA
</s>
<s>
M-Sport	M-Sport	k1gInSc1
Ford	ford	k1gInSc1
WRT	WRT	kA
</s>
<s>
WRC	WRC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
30.5	30.5	k4
</s>
<s>
+1	+1	k4
<g/>
:	:	kIx,
<g/>
36.7	36.7	k4
</s>
<s>
12	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Teemu	Teemat	k5eAaPmIp1nS
Suninen	Suninen	k2eAgMnSc1d1
</s>
<s>
Marko	Marko	k1gMnSc1
Salminen	Salminna	k1gFnPc2
</s>
<s>
Ford	ford	k1gInSc1
Fiesta	fiesta	k1gFnSc1
WRC	WRC	kA
</s>
<s>
M-Sport	M-Sport	k1gInSc1
Ford	ford	k1gInSc1
WRT	WRT	kA
</s>
<s>
WRC	WRC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9.4	9.4	k4
</s>
<s>
+3	+3	k4
<g/>
:	:	kIx,
<g/>
15.6	15.6	k4
</s>
<s>
10	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Esapekka	Esapekka	k1gFnSc1
Lappi	Lapp	k1gFnSc2
</s>
<s>
Janne	Jannout	k5eAaPmIp3nS,k5eAaImIp3nS
Ferm	Ferm	k1gInSc1
</s>
<s>
Citroën	Citroën	k1gMnSc1
C3	C3	k1gFnSc2
WRC	WRC	kA
</s>
<s>
Citroën	Citroën	k1gInSc1
Total	totat	k5eAaImAgInS
WRT	WRT	kA
</s>
<s>
WRC	WRC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
39.2	39.2	k4
</s>
<s>
+3	+3	k4
<g/>
:	:	kIx,
<g/>
45.4	45.4	k4
</s>
<s>
8	#num#	k4
<g/>
+	+	kIx~
<g/>
2	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
89	#num#	k4
</s>
<s>
Andreas	Andreas	k1gInSc1
Mikkelsen	Mikkelsna	k1gFnPc2
</s>
<s>
Anders	Anders	k6eAd1
Jæ	Jæ	k1gInSc1
</s>
<s>
Hyundai	Hyundai	k6eAd1
i	i	k9
<g/>
20	#num#	k4
Coupe	coup	k1gInSc5
WRC	WRC	kA
</s>
<s>
Hyundai	Hyundai	k6eAd1
Shell	Shell	k1gMnSc1
Mobis	Mobis	k1gFnSc2
WRT	WRT	kA
</s>
<s>
WRC	WRC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
32.8	32.8	k4
</s>
<s>
+4	+4	k4
<g/>
:	:	kIx,
<g/>
39.0	39.0	k4
</s>
<s>
6	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
Kalle	Kalle	k1gFnSc1
Rovanperä	Rovanperä	k1gFnSc2
</s>
<s>
Jonne	Jonnout	k5eAaPmIp3nS,k5eAaImIp3nS
Halttunen	Halttunen	k2eAgMnSc1d1
</s>
<s>
Škoda	škoda	k1gFnSc1
Fabia	fabia	k1gFnSc1
R5	R5	k1gFnSc1
</s>
<s>
Škoda	škoda	k1gFnSc1
Motorsport	Motorsport	k1gInSc1
</s>
<s>
WRC-2	WRC-2	k4
Pro	pro	k7c4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
46.3	46.3	k4
</s>
<s>
+7	+7	k4
<g/>
:	:	kIx,
<g/>
52.5	52.5	k4
</s>
<s>
4	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
Mads	Mads	k6eAd1
Ø	Ø	k1gInSc1
</s>
<s>
Torstein	Torstein	k2eAgInSc1d1
Eriksen	Eriksen	k1gInSc1
</s>
<s>
Citroën	Citroën	k1gMnSc1
C3	C3	k1gMnSc1
R5	R5	k1gMnSc1
</s>
<s>
Citroën	Citroën	k1gMnSc1
Total	totat	k5eAaImAgMnS
</s>
<s>
WRC-2	WRC-2	k4
Pro	pro	k7c4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9.9	9.9	k4
</s>
<s>
+8	+8	k4
<g/>
:	:	kIx,
<g/>
16.1	16.1	k4
</s>
<s>
2	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Kris	kris	k1gInSc1
Meeke	Meek	k1gFnSc2
</s>
<s>
Sebastian	Sebastian	k1gMnSc1
Marshall	Marshall	k1gMnSc1
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
Toyota	toyota	k1gFnSc1
Gazoo	Gazoo	k6eAd1
Racing	Racing	k1gInSc1
WRT	WRT	kA
</s>
<s>
WRC	WRC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
27.2	27.2	k4
</s>
<s>
+8	+8	k4
<g/>
:	:	kIx,
<g/>
33.4	33.4	k4
</s>
<s>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
</s>
<s>
WRC	WRC	kA
2	#num#	k4
Pro	pro	k7c4
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
22	#num#	k4
</s>
<s>
Kalle	Kalle	k1gFnSc1
Rovanperä	Rovanperä	k1gFnSc2
</s>
<s>
Jonne	Jonnout	k5eAaImIp3nS,k5eAaPmIp3nS
Halttunen	Halttunen	k2eAgMnSc1d1
</s>
<s>
Škoda	škoda	k1gFnSc1
Fabia	fabia	k1gFnSc1
R5	R5	k1gFnSc1
</s>
<s>
Škoda	škoda	k1gFnSc1
Motorsport	Motorsport	k1gInSc1
</s>
<s>
WRC-2	WRC-2	k4
Pro	pro	k7c4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
46.3	46.3	k4
</s>
<s>
25	#num#	k4
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
22	#num#	k4
</s>
<s>
Mads	Mads	k6eAd1
Ø	Ø	k1gInSc1
</s>
<s>
Torstein	Torstein	k2eAgInSc1d1
Eriksen	Eriksen	k1gInSc1
</s>
<s>
Citroën	Citroën	k1gMnSc1
C3	C3	k1gMnSc1
R5	R5	k1gMnSc1
</s>
<s>
Citroën	Citroën	k1gMnSc1
Total	totat	k5eAaImAgMnS
</s>
<s>
WRC-2	WRC-2	k4
Pro	pro	k7c4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9.9	9.9	k4
</s>
<s>
+23.6	+23.6	k4
</s>
<s>
18	#num#	k4
</s>
<s>
3	#num#	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
21	#num#	k4
</s>
<s>
Gus	Gus	k?
Greensmith	Greensmith	k1gInSc1
</s>
<s>
Elliott	Elliott	k2eAgInSc1d1
Edmondson	Edmondson	k1gInSc1
</s>
<s>
Ford	ford	k1gInSc1
Fiesta	fiesta	k1gFnSc1
R5	R5	k1gFnSc1
</s>
<s>
M-Sport	M-Sport	k1gInSc1
Ford	ford	k1gInSc1
WRT	WRT	kA
</s>
<s>
WRC-2	WRC-2	k4
Pro	pro	k7c4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
<g/>
:	:	kIx,
<g/>
35.3	35.3	k4
</s>
<s>
+3	+3	k4
<g/>
:	:	kIx,
<g/>
49.0	49.0	k4
</s>
<s>
15	#num#	k4
</s>
<s>
4	#num#	k4
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
24	#num#	k4
</s>
<s>
Marco	Marco	k1gMnSc1
Bulacia	Bulacium	k1gNnSc2
Wilkinson	Wilkinson	k1gMnSc1
</s>
<s>
Fabian	Fabian	k1gMnSc1
Cretu	Cret	k1gInSc2
</s>
<s>
Škoda	škoda	k1gFnSc1
Fabia	fabia	k1gFnSc1
R5	R5	k1gFnSc1
</s>
<s>
Škoda	škoda	k1gFnSc1
Motorsport	Motorsport	k1gInSc1
</s>
<s>
WRC-2	WRC-2	k4
Pro	pro	k7c4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
29	#num#	k4
<g/>
:	:	kIx,
<g/>
28.6	28.6	k4
</s>
<s>
+5	+5	k4
<g/>
:	:	kIx,
<g/>
42.3	42.3	k4
</s>
<s>
12	#num#	k4
</s>
<s>
WRC	WRC	kA
2	#num#	k4
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
44	#num#	k4
</s>
<s>
Takamoto	Takamota	k1gFnSc5
Katsuta	Katsut	k2eAgNnPc4d1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Barritt	Barritt	k1gMnSc1
</s>
<s>
Ford	ford	k1gInSc1
Fiesta	fiesta	k1gFnSc1
R5	R5	k1gMnPc2
</s>
<s>
Takamoto	Takamota	k1gFnSc5
Katsuta	Katsut	k1gMnSc2
</s>
<s>
WRC-2	WRC-2	k4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
29	#num#	k4
<g/>
:	:	kIx,
<g/>
26.7	26.7	k4
</s>
<s>
25	#num#	k4
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
41	#num#	k4
</s>
<s>
Benito	Benit	k2eAgNnSc1d1
Guerra	Guerro	k1gNnPc1
Jr	Jr	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jaime	Jaimat	k5eAaPmIp3nS
Zapata	Zapata	k1gFnSc1
</s>
<s>
Škoda	škoda	k1gFnSc1
Fabia	fabia	k1gFnSc1
R5	R5	k1gFnSc1
</s>
<s>
Benito	Benit	k2eAgNnSc1d1
Guerra	Guerro	k1gNnPc1
Jr	Jr	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
WRC-2	WRC-2	k4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
32.8	32.8	k4
</s>
<s>
+3	+3	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6.1	6.1	k4
</s>
<s>
18	#num#	k4
</s>
<s>
3	#num#	k4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
52	#num#	k4
</s>
<s>
Alejandro	Alejandro	k6eAd1
Cancio	Cancio	k6eAd1
</s>
<s>
Santiago	Santiago	k1gNnSc1
Garcia	Garcium	k1gNnSc2
</s>
<s>
Škoda	škoda	k1gFnSc1
Fabia	fabia	k1gFnSc1
R5	R5	k1gFnSc1
</s>
<s>
Alejandro	Alejandro	k6eAd1
Cancio	Cancio	k6eAd1
</s>
<s>
WRC-2	WRC-2	k4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
:	:	kIx,
<g/>
33.9	33.9	k4
</s>
<s>
+5	+5	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7.2	7.2	k4
</s>
<s>
15	#num#	k4
</s>
<s>
4	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
53	#num#	k4
</s>
<s>
Cristóbal	Cristóbal	k1gInSc1
Vidaurre	Vidaurr	k1gInSc5
</s>
<s>
FRuben	FRubna	k1gFnPc2
Garcia	Garcium	k1gNnSc2
</s>
<s>
Škoda	škoda	k1gFnSc1
Fabia	fabia	k1gFnSc1
R5	R5	k1gFnSc1
</s>
<s>
Cristóbal	Cristóbal	k1gInSc1
Vidaurre	Vidaurr	k1gInSc5
</s>
<s>
WRC-2	WRC-2	k4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
:	:	kIx,
<g/>
54.0	54.0	k4
</s>
<s>
+5	+5	k4
<g/>
:	:	kIx,
<g/>
27.3	27.3	k4
</s>
<s>
12	#num#	k4
</s>
<s>
5	#num#	k4
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
50	#num#	k4
</s>
<s>
Samuel	Samuel	k1gMnSc1
Israel	Israel	k1gMnSc1
</s>
<s>
Nicolas	Nicolas	k1gInSc1
Garcia	Garcium	k1gNnSc2
</s>
<s>
Citroën	Citroën	k1gMnSc1
C3	C3	k1gMnSc1
R5	R5	k1gMnSc1
</s>
<s>
Samuel	Samuel	k1gMnSc1
Israel	Israel	k1gMnSc1
</s>
<s>
WRC-2	WRC-2	k4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
36	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4.4	4.4	k4
</s>
<s>
+6	+6	k4
<g/>
:	:	kIx,
<g/>
37.7	37.7	k4
</s>
<s>
10	#num#	k4
</s>
<s>
6	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
49	#num#	k4
</s>
<s>
Vicente	Vicent	k1gMnSc5
Israel	Israel	k1gMnSc1
</s>
<s>
Matias	Matias	k1gMnSc1
Ramos	Ramos	k1gMnSc1
</s>
<s>
Citroën	Citroën	k1gMnSc1
C3	C3	k1gMnSc1
R5	R5	k1gMnSc1
</s>
<s>
Vicente	Vicent	k1gMnSc5
Israel	Israel	k1gMnSc1
</s>
<s>
WRC-2	WRC-2	k4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
37	#num#	k4
<g/>
:	:	kIx,
<g/>
17.9	17.9	k4
</s>
<s>
+7	+7	k4
<g/>
:	:	kIx,
<g/>
51.2	51.2	k4
</s>
<s>
8	#num#	k4
</s>
<s>
7	#num#	k4
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
46	#num#	k4
</s>
<s>
Paulo	Paula	k1gFnSc5
Nobre	Nobr	k1gInSc5
</s>
<s>
Gabriel	Gabriel	k1gMnSc1
Morales	Morales	k1gMnSc1
</s>
<s>
Škoda	škoda	k1gFnSc1
Fabia	fabia	k1gFnSc1
R5	R5	k1gFnSc1
</s>
<s>
Paulo	Paula	k1gFnSc5
Nobre	Nobr	k1gInSc5
</s>
<s>
WRC-2	WRC-2	k4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
<g/>
:	:	kIx,
<g/>
57.2	57.2	k4
</s>
<s>
+15	+15	k4
<g/>
:	:	kIx,
<g/>
30.5	30.5	k4
</s>
<s>
6	#num#	k4
</s>
<s>
8	#num#	k4
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
57	#num#	k4
</s>
<s>
Eduardo	Eduarda	k1gMnSc5
Castro	Castra	k1gMnSc5
</s>
<s>
Julio	Julio	k1gMnSc1
Echazu	Echaz	k1gInSc2
</s>
<s>
Citroën	Citroën	k1gMnSc1
C3	C3	k1gMnSc1
R5	R5	k1gMnSc1
</s>
<s>
Paulo	Paula	k1gFnSc5
Nobre	Nobr	k1gInSc5
</s>
<s>
WRC-2	WRC-2	k4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
48	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1.7	1.7	k4
</s>
<s>
+18	+18	k4
<g/>
:	:	kIx,
<g/>
35.0	35.0	k4
</s>
<s>
4	#num#	k4
</s>
<s>
9	#num#	k4
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
55	#num#	k4
</s>
<s>
Francisco	Francisco	k1gMnSc1
Lopez	Lopez	k1gMnSc1
</s>
<s>
Nicolas	Nicolas	k1gInSc1
Levalle	Levalle	k1gFnSc2
</s>
<s>
Peugeot	peugeot	k1gInSc1
208	#num#	k4
T16	T16	k1gFnPc2
</s>
<s>
Francisco	Francisco	k1gMnSc1
Lopez	Lopez	k1gMnSc1
</s>
<s>
WRC-2	WRC-2	k4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
<g/>
:	:	kIx,
<g/>
42.0	42.0	k4
</s>
<s>
+27	+27	k4
<g/>
:	:	kIx,
<g/>
15.3	15.3	k4
</s>
<s>
2	#num#	k4
</s>
<s>
10	#num#	k4
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
45	#num#	k4
</s>
<s>
Pedro	Pedro	k6eAd1
Heller	Heller	k1gInSc1
</s>
<s>
Marc	Marc	k1gFnSc1
Martí	Marť	k1gFnPc2
</s>
<s>
Ford	ford	k1gInSc1
Fiesta	fiesta	k1gFnSc1
R5	R5	k1gMnSc2
</s>
<s>
Pedro	Pedro	k6eAd1
Heller	Heller	k1gInSc1
</s>
<s>
WRC-2	WRC-2	k4
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
21.7	21.7	k4
</s>
<s>
+45	+45	k4
<g/>
:	:	kIx,
<g/>
55.0	55.0	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Rychlostní	rychlostní	k2eAgFnPc1d1
zkoušky	zkouška	k1gFnPc1
</s>
<s>
Den	den	k1gInSc1
</s>
<s>
Nr	Nr	k?
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
RZ	RZ	kA
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
RZ	RZ	kA
</s>
<s>
Vůz	vůz	k1gInSc1
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
Leader	leader	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
V	v	k7c6
</s>
<s>
-	-	kIx~
</s>
<s>
Shakedown	Shakedown	k1gMnSc1
</s>
<s>
6.45	6.45	k4
km	km	kA
</s>
<s>
Andreas	Andreas	k1gInSc1
Mikkelsen	Mikkelsna	k1gFnPc2
</s>
<s>
Hyundai	Hyundai	k6eAd1
i	i	k9
<g/>
20	#num#	k4
Coupe	coup	k1gInSc5
WRC	WRC	kA
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
46.4	46.4	k4
</s>
<s>
-	-	kIx~
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
V	v	k7c6
</s>
<s>
OS1	OS1	k4
</s>
<s>
El	Ela	k1gFnPc2
Pinar	Pinara	k1gFnPc2
1	#num#	k4
</s>
<s>
17,11	17,11	k4
km	km	kA
</s>
<s>
Kris	kris	k1gInSc1
Meeke	Meek	k1gFnSc2
Jari-Matti	Jari-Matť	k1gFnSc2
Latvala	Latvala	k1gFnSc2
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
35,9	35,9	k4
</s>
<s>
Kris	kris	k1gInSc1
Meeke	Meek	k1gFnSc2
Jari-Matti	Jari-Matť	k1gFnSc2
Latvala	Latvala	k1gFnSc2
</s>
<s>
OS2	OS2	k4
</s>
<s>
El	Ela	k1gFnPc2
Puma	puma	k2eAgFnPc2d1
1	#num#	k4
</s>
<s>
30,72	30,72	k4
km	km	kA
</s>
<s>
Thierry	Thierra	k1gFnPc1
Neuville	Neuville	k1gFnSc2
</s>
<s>
Hyundai	Hyundai	k6eAd1
i	i	k9
<g/>
20	#num#	k4
Coupe	coup	k1gInSc5
WRC	WRC	kA
</s>
<s>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
10,4	10,4	k4
</s>
<s>
Ott	Ott	k?
Tänak	Tänak	k1gInSc1
</s>
<s>
OS3	OS3	k4
</s>
<s>
Espigado	Espigada	k1gFnSc5
1	#num#	k4
</s>
<s>
22,26	22,26	k4
km	km	kA
</s>
<s>
Ott	Ott	k?
Tänak	Tänak	k1gInSc1
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
23.7	23.7	k4
</s>
<s>
OS4	OS4	k4
</s>
<s>
El	Ela	k1gFnPc2
Puma	puma	k2eAgFnPc2d1
2	#num#	k4
</s>
<s>
30,72	30,72	k4
km	km	kA
</s>
<s>
Ott	Ott	k?
Tänak	Tänak	k1gInSc1
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
46,0	46,0	k4
</s>
<s>
OS5	OS5	k4
</s>
<s>
Espigado	Espigada	k1gFnSc5
2	#num#	k4
</s>
<s>
22,26	22,26	k4
km	km	kA
</s>
<s>
Ott	Ott	k?
Tänak	Tänak	k1gInSc1
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3,4	3,4	k4
</s>
<s>
OS6	OS6	k4
</s>
<s>
Concepcion	Concepcion	k1gInSc1
-	-	kIx~
Bicentenario	Bicentenario	k6eAd1
</s>
<s>
2,20	2,20	k4
km	km	kA
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Loeb	Loeb	k1gInSc1
</s>
<s>
Hyundai	Hyundai	k6eAd1
i	i	k9
<g/>
20	#num#	k4
Coupe	coup	k1gInSc5
WRC	WRC	kA
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6,9	6,9	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
V	v	k7c6
</s>
<s>
OS7	OS7	k4
</s>
<s>
Rio	Rio	k?
Lia	Lia	k1gFnSc1
1	#num#	k4
</s>
<s>
20,90	20,90	k4
km	km	kA
</s>
<s>
Ott	Ott	k?
Tänak	Tänak	k1gInSc1
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
55,5	55,5	k4
</s>
<s>
OS8	OS8	k4
</s>
<s>
Maria	Maria	k1gFnSc1
Las	laso	k1gNnPc2
Cruces	Cruces	k1gInSc4
1	#num#	k4
</s>
<s>
23,09	23,09	k4
km	km	kA
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Loeb	Loeb	k1gInSc1
</s>
<s>
Hyundai	Hyundai	k6eAd1
i	i	k9
<g/>
20	#num#	k4
Coupe	coup	k1gInSc5
WRC	WRC	kA
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
28,3	28,3	k4
</s>
<s>
OS9	OS9	k4
</s>
<s>
Pelun	Pelun	k1gInSc1
1	#num#	k4
</s>
<s>
16,59	16,59	k4
km	km	kA
</s>
<s>
Ott	Ott	k?
Tänak	Tänak	k1gInSc1
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8,9	8,9	k4
</s>
<s>
OS10	OS10	k4
</s>
<s>
Rio	Rio	k?
Lia	Lia	k1gFnSc1
2	#num#	k4
</s>
<s>
20,90	20,90	k4
km	km	kA
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Ogier	Ogier	k1gInSc1
</s>
<s>
Citroën	Citroën	k1gMnSc1
C3	C3	k1gFnSc2
WRC	WRC	kA
</s>
<s>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
45,1	45,1	k4
</s>
<s>
OS11	OS11	k4
</s>
<s>
Maria	Maria	k1gFnSc1
Las	laso	k1gNnPc2
Cruces	Cruces	k1gMnSc1
2	#num#	k4
</s>
<s>
23,09	23,09	k4
km	km	kA
</s>
<s>
Ott	Ott	k?
Tänak	Tänak	k1gInSc1
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
14,7	14,7	k4
</s>
<s>
OS12	OS12	k4
</s>
<s>
Pelun	Pelun	k1gInSc1
2	#num#	k4
</s>
<s>
16,59	16,59	k4
km	km	kA
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Loeb	Loeb	k1gInSc1
</s>
<s>
Hyundai	Hyundai	k6eAd1
i	i	k9
<g/>
20	#num#	k4
Coupe	coup	k1gInSc5
WRC	WRC	kA
</s>
<s>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0,6	0,6	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
V	v	k7c6
</s>
<s>
OS13	OS13	k4
</s>
<s>
Bio	Bio	k?
Bio	Bio	k?
1	#num#	k4
</s>
<s>
12,52	12,52	k4
km	km	kA
</s>
<s>
Kris	kris	k1gInSc1
Meeke	Meek	k1gFnSc2
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
14,5	14,5	k4
</s>
<s>
OS14	OS14	k4
</s>
<s>
Lircay	Lircaa	k1gFnPc1
</s>
<s>
18,06	18,06	k4
km	km	kA
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Ogier	Ogier	k1gInSc1
</s>
<s>
Citroën	Citroën	k1gMnSc1
C3	C3	k1gFnSc2
WRC	WRC	kA
</s>
<s>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
13,5	13,5	k4
</s>
<s>
OS15	OS15	k4
</s>
<s>
San	San	k?
Nicolà	Nicolà	k1gInSc1
</s>
<s>
15,28	15,28	k4
km	km	kA
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Loeb	Loeb	k1gInSc1
</s>
<s>
Hyundai	Hyundai	k6eAd1
i	i	k9
<g/>
20	#num#	k4
Coupe	coup	k1gInSc5
WRC	WRC	kA
</s>
<s>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8,6	8,6	k4
</s>
<s>
OS16	OS16	k4
</s>
<s>
Bio	Bio	k?
Bio	Bio	k?
2	#num#	k4
[	[	kIx(
<g/>
Powerstage	Powerstag	k1gInSc2
<g/>
]	]	kIx)
</s>
<s>
12,52	12,52	k4
km	km	kA
</s>
<s>
Ott	Ott	k?
Tänak	Tänak	k1gInSc1
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
57,3	57,3	k4
</s>
<s>
Powerstage	Powerstage	k6eAd1
</s>
<s>
Místo	místo	k7c2
</s>
<s>
Jezdec	jezdec	k1gMnSc1
</s>
<s>
Navigátor	navigátor	k1gMnSc1
</s>
<s>
Vůz	vůz	k1gInSc1
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
Ztráta	ztráta	k1gFnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
1	#num#	k4
</s>
<s>
Ott	Ott	k?
Tänak	Tänak	k1gInSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Järveoja	Järveoja	k1gMnSc1
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
57,3	57,3	k4
</s>
<s>
-	-	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Ogier	Ogier	k1gInSc1
</s>
<s>
Julien	Julien	k1gInSc1
Ingrassia	Ingrassium	k1gNnSc2
</s>
<s>
Citroën	Citroën	k1gMnSc1
C3	C3	k1gFnSc2
WRC	WRC	kA
</s>
<s>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
58,6	58,6	k4
</s>
<s>
+1,3	+1,3	k4
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Jari-Matti	Jari-Matt	k2eAgMnPc1d1
Latvala	Latvala	k1gMnPc1
</s>
<s>
Miikka	Miikka	k1gFnSc1
Anttila	Anttil	k1gMnSc2
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0,4	0,4	k4
</s>
<s>
+3,1	+3,1	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Loeb	Loeb	k1gInSc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Elena	Elena	k1gFnSc1
</s>
<s>
Hyundai	Hyundai	k6eAd1
i	i	k9
<g/>
20	#num#	k4
Coupe	coup	k1gInSc5
WRC	WRC	kA
</s>
<s>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1,1	1,1	k4
</s>
<s>
+3,8	+3,8	k4
</s>
<s>
2	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Kris	kris	k1gInSc1
Meeke	Meek	k1gFnSc2
</s>
<s>
Sebastian	Sebastian	k1gMnSc1
Marshall	Marshall	k1gMnSc1
</s>
<s>
Toyota	toyota	k1gFnSc1
Yaris	Yaris	k1gFnSc2
WRC	WRC	kA
</s>
<s>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1,3	1,3	k4
</s>
<s>
+4,0	+4,0	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Výsledky	výsledek	k1gInPc1
po	po	k7c6
Chilské	chilský	k2eAgFnSc6d1
rallye	rallye	k1gFnSc6
</s>
<s>
Kategorie	kategorie	k1gFnPc1
WRC	WRC	kA
</s>
<s>
Pos	Pos	k?
<g/>
.	.	kIx.
</s>
<s>
Pohár	pohár	k1gInSc1
jezdců	jezdec	k1gMnPc2
</s>
<s>
Pohár	pohár	k1gInSc1
navigátorů	navigátor	k1gMnPc2
</s>
<s>
Pohár	pohár	k1gInSc1
konstruktéru	konstruktér	k1gMnSc6
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Jezdec	jezdec	k1gMnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Navigátor	navigátor	k1gMnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Konstruktér	konstruktér	k1gMnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Ogier	Ogier	k1gInSc1
</s>
<s>
122	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Julien	Julien	k1gInSc1
Ingrassia	Ingrassium	k1gNnSc2
</s>
<s>
122	#num#	k4
</s>
<s>
Hyundai	Hyundai	k6eAd1
Shell	Shell	k1gMnSc1
Mobis	Mobis	k1gFnSc2
WRT	WRT	kA
</s>
<s>
178	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Ott	Ott	k?
Tänak	Tänak	k1gInSc1
</s>
<s>
112	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Martin	Martin	k1gMnSc1
Järveoja	Järveoja	k1gMnSc1
</s>
<s>
112	#num#	k4
</s>
<s>
Toyota	toyota	k1gFnSc1
Gazoo	Gazoo	k6eAd1
Racing	Racing	k1gInSc1
WRT	WRT	kA
</s>
<s>
149	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Thierry	Thierra	k1gFnPc1
Neuville	Neuville	k1gFnSc2
</s>
<s>
110	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Nicolas	Nicolas	k1gInSc1
Gilsoul	Gilsoula	k1gFnPc2
</s>
<s>
110	#num#	k4
</s>
<s>
Citroën	Citroën	k1gInSc1
Total	totat	k5eAaImAgInS
WRT	WRT	kA
</s>
<s>
143	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Kris	kris	k1gInSc1
Meeke	Meek	k1gFnSc2
</s>
<s>
56	#num#	k4
</s>
<s>
Sebastian	Sebastian	k1gMnSc1
Marshall	Marshall	k1gMnSc1
</s>
<s>
56	#num#	k4
</s>
<s>
M-Sport	M-Sport	k1gInSc1
Ford	ford	k1gInSc1
WRT	WRT	kA
</s>
<s>
100	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Elfyn	Elfyn	k1gNnSc1
Evans	Evansa	k1gFnPc2
</s>
<s>
55	#num#	k4
</s>
<s>
Scott	Scott	k1gMnSc1
Martin	Martin	k1gMnSc1
</s>
<s>
55	#num#	k4
</s>
<s>
Kategorie	kategorie	k1gFnPc1
WRC	WRC	kA
Pro	pro	k7c4
</s>
<s>
Pos	Pos	k?
<g/>
.	.	kIx.
</s>
<s>
Pohár	pohár	k1gInSc1
jezdců	jezdec	k1gMnPc2
</s>
<s>
Pohár	pohár	k1gInSc1
navigátorů	navigátor	k1gMnPc2
</s>
<s>
Pohár	pohár	k1gInSc1
konstruktéru	konstruktér	k1gMnSc6
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Jezdec	jezdec	k1gMnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Navigátor	navigátor	k1gMnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Konstruktér	konstruktér	k1gMnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Gus	Gus	k?
Greensmith	Greensmith	k1gInSc1
</s>
<s>
73	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Elliott	Elliott	k2eAgInSc1d1
Edmondson	Edmondson	k1gInSc1
</s>
<s>
73	#num#	k4
</s>
<s>
M-Sport	M-Sport	k1gInSc1
Ford	ford	k1gInSc1
WRT	WRT	kA
</s>
<s>
135	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Mads	Mads	k6eAd1
Ø	Ø	k1gInSc1
</s>
<s>
68	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Torstein	Torstein	k2eAgInSc1d1
Eriksen	Eriksen	k1gInSc1
</s>
<s>
68	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Škoda	škoda	k1gFnSc1
Motorsport	Motorsport	k1gInSc1
</s>
<s>
73	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Pieniążek	Pieniążek	k1gMnSc1
</s>
<s>
62	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Kamil	Kamil	k1gMnSc1
Heller	Heller	k1gMnSc1
</s>
<s>
62	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Citroën	Citroën	k1gMnSc1
Total	totat	k5eAaImAgMnS
</s>
<s>
68	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Kalle	Kalle	k1gFnSc1
Rovanperä	Rovanperä	k1gFnSc2
</s>
<s>
61	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Jonne	Jonnout	k5eAaPmIp3nS,k5eAaImIp3nS
Halttunen	Halttunen	k2eAgMnSc1d1
</s>
<s>
61	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Marco	Marco	k1gMnSc1
Bulacia	Bulacium	k1gNnSc2
Wilkinson	Wilkinson	k1gMnSc1
</s>
<s>
12	#num#	k4
</s>
<s>
Fabian	Fabian	k1gMnSc1
Cretu	Cret	k1gInSc2
</s>
<s>
12	#num#	k4
</s>
<s>
Kategorie	kategorie	k1gFnPc1
WRC	WRC	kA
2	#num#	k4
</s>
<s>
Pos	Pos	k?
<g/>
.	.	kIx.
</s>
<s>
Pohár	pohár	k1gInSc1
jezdců	jezdec	k1gMnPc2
</s>
<s>
Pohár	pohár	k1gInSc1
navigátorů	navigátor	k1gMnPc2
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Jezdec	jezdec	k1gMnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Navigátor	navigátor	k1gMnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
1	#num#	k4
</s>
<s>
Benito	Benit	k2eAgNnSc1d1
Guerra	Guerro	k1gNnPc1
</s>
<s>
61	#num#	k4
</s>
<s>
Jaime	Jaimat	k5eAaPmIp3nS
Zapata	Zapata	k1gFnSc1
</s>
<s>
61	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Takamoto	Takamota	k1gFnSc5
Katsuta	Katsut	k2eAgFnSc1d1
</s>
<s>
47	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Daniel	Daniel	k1gMnSc1
Barritt	Barritt	k1gMnSc1
</s>
<s>
47	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Ole	Ola	k1gFnSc3
Christian	Christian	k1gMnSc1
Veiby	Veiba	k1gFnPc1
</s>
<s>
40	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Jonas	Jonas	k1gMnSc1
Andersson	Andersson	k1gMnSc1
</s>
<s>
40	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Nikolaj	Nikolaj	k1gMnSc1
Grjazin	Grjazin	k1gMnSc1
</s>
<s>
28	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Fedorov	Fedorov	k1gInSc4
</s>
<s>
28	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Alberto	Alberta	k1gFnSc5
Heller	Heller	k1gMnSc1
</s>
<s>
27	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
José	José	k6eAd1
Díaz	Díaz	k1gInSc1
</s>
<s>
27	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Sunday	Sundaa	k1gFnPc4
in	in	k?
Argentina	Argentina	k1gFnSc1
<g/>
:	:	kIx,
Neuville	Neuville	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
double	double	k2eAgMnPc7d1
<g/>
.	.	kIx.
wrc	wrc	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
WRC	WRC	kA
<g/>
,	,	kIx,
28	#num#	k4
April	April	k1gInSc1
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
April	April	k1gInSc1
2019	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Sunday	Sundaa	k1gFnSc2
in	in	k?
WRC	WRC	kA
2	#num#	k4
<g/>
:	:	kIx,
Ø	Ø	k1gInSc1
wins	wins	k6eAd1
Pro	pro	k7c4
<g/>
.	.	kIx.
wrc	wrc	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
WRC	WRC	kA
<g/>
,	,	kIx,
28	#num#	k4
April	April	k1gInSc1
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
April	April	k1gInSc1
2019	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ott	Ott	k1gMnSc1
Tänak	Tänak	k1gMnSc1
–	–	k?
Martin	Martin	k1gMnSc1
Järveoja	Järveoja	k1gMnSc1
domineerisid	domineerisid	k1gInSc4
Tšiili	Tšiili	k1gMnSc3
ralli	rall	k1gMnSc3
esimest	esimest	k1gInSc1
võ	võ	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
auto	auto	k1gNnSc1
<g/>
.	.	kIx.
<g/>
pub	pub	k?
<g/>
,	,	kIx,
11.05	11.05	k4
<g/>
.2019	.2019	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
estonsky	estonsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Thierry	Thierra	k1gMnSc2
Neuville	Neuvill	k1gMnSc2
&	&	k?
Nicolas	Nicolas	k1gInSc1
Gilsoul	Gilsoul	k1gInSc1
Crash	Crash	k1gInSc4
Rally	Ralla	k1gFnSc2
Chile	Chile	k1gNnSc2
2019	#num#	k4
<g/>
↑	↑	k?
MICKOVÁ	MICKOVÁ	kA
<g/>
,	,	kIx,
Andrea	Andrea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tänak	Tänak	k1gInSc1
má	mít	k5eAaImIp3nS
po	po	k7c6
druhé	druhý	k4xOgFnSc6
etapě	etapa	k1gFnSc6
Chilské	chilský	k2eAgFnSc2d1
rallye	rallye	k1gFnSc2
půlminutový	půlminutový	k2eAgInSc4d1
náskok	náskok	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copec	Copec	k1gInSc1
Rally	Ralla	k1gFnSc2
Chile	Chile	k1gNnSc2
2019	#num#	k4
OS16	OS16	k1gFnSc2
Bio	Bio	k?
Bio	Bio	k?
2	#num#	k4
[	[	kIx(
<g/>
Power	Power	k1gInSc1
Stage	Stag	k1gInSc2
-	-	kIx~
12.52	12.52	k4
km	km	kA
-	-	kIx~
(	(	kIx(
<g/>
GMT	GMT	kA
<g/>
+	+	kIx~
<g/>
1	#num#	k4
18	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
-	-	kIx~
12	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
]	]	kIx)
.	.	kIx.
www.ewrc-results.com	www.ewrc-results.com	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2019	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://www.ewrc-results.com/results/54463-copec-rally-chile-2019/?s=215122	https://www.ewrc-results.com/results/54463-copec-rally-chile-2019/?s=215122	k4
</s>
