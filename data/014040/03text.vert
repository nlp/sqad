<s>
Lepton	Lepton	k1gInSc1
</s>
<s>
Ve	v	k7c6
fyzice	fyzika	k1gFnSc6
je	být	k5eAaImIp3nS
lepton	lepton	k1gInSc4
částice	částice	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
nepůsobí	působit	k5eNaImIp3nS
silná	silný	k2eAgFnSc1d1
jaderná	jaderný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
(	(	kIx(
<g/>
silná	silný	k2eAgFnSc1d1
interakce	interakce	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc4
„	„	k?
<g/>
lepton	lepton	k1gInSc1
<g/>
“	“	k?
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
řečtiny	řečtina	k1gFnSc2
a	a	k8xC
znamená	znamenat	k5eAaImIp3nS
lehký	lehký	k2eAgInSc1d1
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
někdy	někdy	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
pod	pod	k7c7
pojmem	pojem	k1gInSc7
lehká	lehký	k2eAgFnSc1d1
částice	částice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
po	po	k7c4
zavedení	zavedení	k1gNnSc4
tohoto	tento	k3xDgInSc2
pojmu	pojem	k1gInSc2
byl	být	k5eAaImAgInS
objeven	objevit	k5eAaPmNgInS
tauon	tauon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
dvakrát	dvakrát	k6eAd1
hmotnější	hmotný	k2eAgInSc1d2
než	než	k8xS
proton	proton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Leptony	Lepton	k1gInPc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
nabité	nabitý	k2eAgInPc4d1
leptony	lepton	k1gInPc4
(	(	kIx(
<g/>
elektron	elektron	k1gInSc1
<g/>
,	,	kIx,
mion	mion	k1gInSc1
a	a	k8xC
tauon	tauon	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
neutrina	neutrino	k1gNnSc2
(	(	kIx(
<g/>
elektronové	elektronový	k2eAgFnSc2d1
<g/>
,	,	kIx,
mionové	mionový	k2eAgFnSc2d1
a	a	k8xC
tauonové	tauonový	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
U	u	k7c2
leptonů	lepton	k1gInPc2
nebyla	být	k5eNaImAgFnS
zjištěna	zjistit	k5eAaPmNgFnS
žádná	žádný	k3yNgFnSc1
vnitřní	vnitřní	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
a	a	k8xC
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
dále	daleko	k6eAd2
nedělitelné	dělitelný	k2eNgFnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgInPc1
leptony	lepton	k1gInPc1
mají	mít	k5eAaImIp3nP
spin	spin	k1gInSc4
½	½	k?
a	a	k8xC
patří	patřit	k5eAaImIp3nS
tedy	tedy	k9
mezi	mezi	k7c7
fermiony	fermion	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leptony	Lepton	k1gInPc7
mají	mít	k5eAaImIp3nP
šest	šest	k4xCc4
vůní	vůně	k1gFnPc2
<g/>
:	:	kIx,
e	e	k0
<g/>
−	−	k?
<g/>
,	,	kIx,
μ	μ	k?
<g/>
−	−	k?
<g/>
,	,	kIx,
τ	τ	k?
<g/>
−	−	k?
<g/>
,	,	kIx,
ν	ν	k1gInSc1
<g/>
,	,	kIx,
ν	ν	k?
<g/>
,	,	kIx,
ν	ν	k?
<g/>
.	.	kIx.
</s>
<s>
Známe	znát	k5eAaImIp1nP
12	#num#	k4
leptonů	lepton	k1gInPc2
<g/>
:	:	kIx,
3	#num#	k4
nabité	nabitý	k2eAgFnSc2d1
částice	částice	k1gFnSc2
(	(	kIx(
<g/>
elektron	elektron	k1gInSc4
<g/>
,	,	kIx,
mion	mion	k1gNnSc4
a	a	k8xC
tauon	tauon	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
odpovídající	odpovídající	k2eAgNnSc4d1
neutrina	neutrino	k1gNnPc4
<g/>
,	,	kIx,
a	a	k8xC
6	#num#	k4
jejich	jejich	k3xOp3gFnPc2
antičástic	antičástice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antičástice	antičástice	k1gFnSc1
mají	mít	k5eAaImIp3nP
stejnou	stejný	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
jako	jako	k8xS,k8xC
částice	částice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmotnost	hmotnost	k1gFnSc1
neutrin	neutrino	k1gNnPc2
nebyla	být	k5eNaImAgFnS
zatím	zatím	k6eAd1
stanovena	stanovit	k5eAaPmNgFnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
však	však	k9
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
nabitými	nabitý	k2eAgInPc7d1
leptony	lepton	k1gInPc7
velmi	velmi	k6eAd1
malá	malý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Nabité	nabitý	k2eAgInPc1d1
leptony	lepton	k1gInPc1
a	a	k8xC
jim	on	k3xPp3gMnPc3
odpovídající	odpovídající	k2eAgNnPc4d1
neutrina	neutrino	k1gNnPc4
tvoří	tvořit	k5eAaImIp3nS
tzv.	tzv.	kA
generace	generace	k1gFnSc1
(	(	kIx(
<g/>
řazení	řazení	k1gNnSc1
do	do	k7c2
generací	generace	k1gFnPc2
je	být	k5eAaImIp3nS
obdobné	obdobný	k2eAgFnSc3d1
jako	jako	k8xS,k8xC
u	u	k7c2
kvarků	kvark	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
generaci	generace	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
elektron	elektron	k1gInSc1
s	s	k7c7
elektronovým	elektronový	k2eAgNnSc7d1
neutrinem	neutrino	k1gNnSc7
<g/>
,	,	kIx,
druhou	druhý	k4xOgFnSc4
generaci	generace	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
mion	mion	k1gInSc1
s	s	k7c7
mionovým	mionův	k2eAgNnSc7d1
neutrinem	neutrino	k1gNnSc7
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnSc4
generaci	generace	k1gFnSc4
tauon	tauona	k1gFnPc2
s	s	k7c7
tauonovým	tauonův	k2eAgNnSc7d1
neutrinem	neutrino	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
daných	daný	k2eAgFnPc2d1
generací	generace	k1gFnPc2
patří	patřit	k5eAaImIp3nS
i	i	k9
odpovídající	odpovídající	k2eAgFnPc4d1
antičástice	antičástice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Každé	každý	k3xTgFnSc3
částici	částice	k1gFnSc3
lze	lze	k6eAd1
přiřadit	přiřadit	k5eAaPmF
tzv.	tzv.	kA
leptonové	leptonový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
–	–	k?
elektronové	elektronový	k2eAgFnSc2d1
<g/>
,	,	kIx,
mionové	mionová	k1gFnSc2
a	a	k8xC
tauonové	tauonová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částice	částice	k1gFnPc1
a	a	k8xC
neutrino	neutrino	k1gNnSc1
příslušné	příslušný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
je	být	k5eAaImIp3nS
má	mít	k5eAaImIp3nS
1	#num#	k4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInPc1
antičástice	antičástice	k1gFnSc1
-1	-1	k4
<g/>
,	,	kIx,
zbývajících	zbývající	k2eAgInPc2d1
8	#num#	k4
leptonů	lepton	k1gInPc2
je	on	k3xPp3gMnPc4
má	mít	k5eAaImIp3nS
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Leptony	Lepton	k1gInPc1
</s>
<s>
Částiceelektron	Částiceelektron	k1gInSc1
(	(	kIx(
<g/>
negatron	negatron	k1gInSc1
<g/>
)	)	kIx)
<g/>
miontauon	miontauon	k1gInSc1
</s>
<s>
Symbole	symbol	k1gInSc5
<g/>
−	−	k?
<g/>
μ	μ	k?
<g/>
−	−	k?
<g/>
τ	τ	k?
<g/>
−	−	k?
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
(	(	kIx(
<g/>
MeV	MeV	k1gFnSc1
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
0,511105	0,511105	k4
<g/>
,71777	,71777	k4
</s>
<s>
Náboj-	Náboj-	k?
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
</s>
<s>
Antičásticeantielektron	Antičásticeantielektron	k1gInSc1
(	(	kIx(
<g/>
pozitron	pozitron	k1gInSc1
<g/>
)	)	kIx)
<g/>
antimionantitauon	antimionantitauon	k1gInSc1
</s>
<s>
Symbole	symbol	k1gInSc5
<g/>
+	+	kIx~
<g/>
μ	μ	k?
<g/>
+	+	kIx~
<g/>
τ	τ	k?
<g/>
+	+	kIx~
</s>
<s>
Náboj	náboj	k1gInSc1
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
</s>
<s>
Neutrinoelektronovémionovétauonové	Neutrinoelektronovémionovétauonové	k2eAgFnSc1d1
</s>
<s>
Symbolν	Symbolν	k?
</s>
<s>
Náboj	náboj	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Antineutrinoelektronovémionovétauonové	Antineutrinoelektronovémionovétauonové	k2eAgFnSc1d1
</s>
<s>
Symbol	symbol	k1gInSc1
</s>
<s>
ν	ν	k?
</s>
<s>
e	e	k0
</s>
<s>
¯	¯	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
overline	overlin	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
nu	nu	k9
_	_	kIx~
<g/>
{	{	kIx(
<g/>
e	e	k0
<g/>
}}}}	}}}}	k?
</s>
<s>
ν	ν	k?
</s>
<s>
μ	μ	k?
</s>
<s>
¯	¯	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
overline	overlin	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
nu	nu	k9
_	_	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mu	on	k3xPp3gMnSc3
}}}}	}}}}	k?
</s>
<s>
ν	ν	k?
</s>
<s>
τ	τ	k?
</s>
<s>
¯	¯	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
overline	overlin	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
nu	nu	k9
_	_	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
tau	tau	k1gNnSc1
}}}}	}}}}	k?
</s>
<s>
Náboj	náboj	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Elementární	elementární	k2eAgFnPc1d1
částice	částice	k1gFnPc1
</s>
<s>
Fermion	fermion	k1gInSc1
</s>
<s>
Leptonové	Leptonový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Částice	částice	k1gFnSc1
Elementární	elementární	k2eAgFnSc2d1
částice	částice	k1gFnSc2
</s>
<s>
částice	částice	k1gFnSc1
hmoty	hmota	k1gFnSc2
<g/>
(	(	kIx(
<g/>
fermiony	fermion	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
kvarky	kvark	k1gInPc1
</s>
<s>
kvark	kvark	k1gInSc1
u	u	k7c2
·	·	k?
antikvark	antikvark	k1gInSc1
u	u	k7c2
·	·	k?
kvark	kvark	k1gInSc1
d	d	k?
·	·	k?
antikvark	antikvark	k1gInSc1
d	d	k?
·	·	k?
kvark	kvark	k1gInSc1
s	s	k7c7
·	·	k?
antikvark	antikvark	k1gInSc1
s	s	k7c7
·	·	k?
kvark	kvark	k1gInSc1
c	c	k0
·	·	k?
antikvark	antikvark	k1gInSc1
c	c	k0
·	·	k?
kvark	kvark	k1gInSc1
t	t	k?
·	·	k?
antikvark	antikvark	k1gInSc1
t	t	k?
·	·	k?
kvark	kvark	k1gInSc1
b	b	k?
·	·	k?
antikvark	antikvark	k1gInSc4
b	b	k?
leptony	lepton	k1gInPc4
</s>
<s>
elektron	elektron	k1gInSc1
·	·	k?
pozitron	pozitron	k1gInSc1
·	·	k?
mion	mion	k1gInSc1
·	·	k?
antimion	antimion	k1gInSc1
·	·	k?
tauon	tauon	k1gMnSc1
·	·	k?
antitauon	antitauon	k1gMnSc1
·	·	k?
elektronové	elektronový	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
elektronové	elektronový	k2eAgNnSc4d1
antineutrino	antineutrino	k1gNnSc4
·	·	k?
mionové	mionový	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
mionové	mionový	k2eAgNnSc4d1
antineutrino	antineutrino	k1gNnSc4
·	·	k?
tauonové	tauonový	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
tauonové	tauonová	k1gFnSc2
antineutrino	antineutrin	k2eAgNnSc1d1
</s>
<s>
částice	částice	k1gFnSc1
interakcí	interakce	k1gFnPc2
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
foton	foton	k1gInSc1
·	·	k?
gluon	gluon	k1gInSc1
·	·	k?
bosony	bosona	k1gFnSc2
W	W	kA
a	a	k8xC
Z	z	k7c2
·	·	k?
Higgsův	Higgsův	k2eAgMnSc1d1
boson	boson	k1gMnSc1
hypotetické	hypotetický	k2eAgFnSc2d1
</s>
<s>
částice	částice	k1gFnSc1
interakcí	interakce	k1gFnPc2
</s>
<s>
graviton	graviton	k1gInSc1
·	·	k?
bosony	bosona	k1gFnSc2
X	X	kA
a	a	k8xC
Y	Y	kA
·	·	k?
bosony	bosona	k1gFnPc4
W	W	kA
<g/>
'	'	kIx"
a	a	k8xC
Z	z	k7c2
<g/>
'	'	kIx"
·	·	k?
majoron	majoron	k1gInSc1
·	·	k?
duální	duální	k2eAgInSc1d1
graviton	graviton	k1gInSc1
superpartneři	superpartner	k1gMnPc1
</s>
<s>
gluino	gluino	k1gNnSc1
·	·	k?
gravitino	gravitin	k2eAgNnSc1d1
·	·	k?
chargino	chargino	k1gNnSc1
·	·	k?
neutralino	utralin	k2eNgNnSc1d1
·	·	k?
(	(	kIx(
<g/>
fotino	fotino	k1gNnSc1
·	·	k?
higgsino	higgsin	k2eAgNnSc1d1
·	·	k?
wino	wino	k1gNnSc1
·	·	k?
zino	zino	k1gMnSc1
<g/>
)	)	kIx)
·	·	k?
slepton	slepton	k1gInSc1
·	·	k?
skvark	skvark	k1gInSc1
·	·	k?
axino	axino	k6eAd1
ostatní	ostatní	k2eAgFnPc1d1
</s>
<s>
axion	axion	k1gInSc1
·	·	k?
dilaton	dilaton	k1gInSc1
·	·	k?
magnetický	magnetický	k2eAgInSc1d1
monopól	monopól	k1gInSc1
·	·	k?
Planckova	Planckov	k1gInSc2
částice	částice	k1gFnSc2
·	·	k?
preon	preon	k1gMnSc1
·	·	k?
sterilní	sterilní	k2eAgNnSc1d1
neutrino	neutrino	k1gNnSc1
·	·	k?
tachyon	tachyon	k1gMnSc1
</s>
<s>
Složené	složený	k2eAgFnPc1d1
částice	částice	k1gFnPc1
</s>
<s>
hadrony	hadron	k1gInPc4
</s>
<s>
baryony	baryon	k1gInPc1
<g/>
(	(	kIx(
<g/>
fermiony	fermion	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
nukleony	nukleon	k1gInPc1
</s>
<s>
proton	proton	k1gInSc1
·	·	k?
antiproton	antiproton	k1gInSc1
·	·	k?
neutron	neutron	k1gInSc1
·	·	k?
antineutron	antineutron	k1gInSc1
hyperony	hyperon	k1gMnPc7
</s>
<s>
Δ	Δ	k?
·	·	k?
Λ	Λ	k?
·	·	k?
Σ	Σ	k?
<g/>
,	,	kIx,
Σ	Σ	k?
<g/>
*	*	kIx~
·	·	k?
Ξ	Ξ	k?
<g/>
,	,	kIx,
Ξ	Ξ	k?
<g/>
*	*	kIx~
·	·	k?
Ω	Ω	k?
ostatní	ostatní	k2eAgFnSc2d1
baryonové	baryonový	k2eAgFnSc2d1
rezonance	rezonance	k1gFnSc2
</s>
<s>
N	N	kA
·	·	k?
Δ	Δ	k?
·	·	k?
Λ	Λ	k?
<g/>
,	,	kIx,
Λ	Λ	k1gInSc1
<g/>
,	,	kIx,
Λ	Λ	k1gInSc1
·	·	k?
Σ	Σ	k?
<g/>
,	,	kIx,
Σ	Σ	k1gInSc1
<g/>
,	,	kIx,
Σ	Σ	k1gInSc1
·	·	k?
Ξ	Ξ	k?
<g/>
,	,	kIx,
Ξ	Ξ	k1gInSc1
<g/>
,	,	kIx,
Ξ	Ξ	k1gInSc1
<g/>
,	,	kIx,
Ξ	Ξ	k1gInSc1
·	·	k?
Ω	Ω	k?
<g/>
,	,	kIx,
Ω	Ω	k1gInSc1
<g/>
,	,	kIx,
Ω	Ω	k1gInSc1
</s>
<s>
mezony	mezon	k1gInPc1
<g/>
/	/	kIx~
<g/>
kvarkonia	kvarkonium	k1gNnPc1
<g/>
(	(	kIx(
<g/>
bosony	boson	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
pion	pion	k1gInSc1
·	·	k?
kaon	kaon	k1gInSc1
·	·	k?
ρ	ρ	k?
·	·	k?
η	η	k?
·	·	k?
φ	φ	k?
·	·	k?
ω	ω	k?
·	·	k?
J	J	kA
<g/>
/	/	kIx~
<g/>
ψ	ψ	k?
·	·	k?
ϒ	ϒ	k?
·	·	k?
θ	θ	k?
·	·	k?
B	B	kA
·	·	k?
D	D	kA
·	·	k?
T	T	kA
exotické	exotický	k2eAgInPc4d1
hadrony	hadron	k1gInPc4
</s>
<s>
tetrakvarky	tetrakvarka	k1gFnPc1
<g/>
/	/	kIx~
<g/>
dvoumezonové	dvoumezonový	k2eAgFnPc1d1
molekuly	molekula	k1gFnPc1
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
X	X	kA
<g/>
(	(	kIx(
<g/>
2900	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
3872	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
4140	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
s	s	k7c7
excitacemi	excitace	k1gFnPc7
X	X	kA
<g/>
(	(	kIx(
<g/>
4274	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
X	X	kA
<g/>
(	(	kIx(
<g/>
4500	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
X	X	kA
<g/>
(	(	kIx(
<g/>
4700	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
))	))	k?
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
4630	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
4685	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
5568	#num#	k4
<g/>
)	)	kIx)
·	·	k?
X	X	kA
<g/>
(	(	kIx(
<g/>
6900	#num#	k4
<g/>
)	)	kIx)
<g/>
Zc	Zc	k1gFnSc2
<g/>
(	(	kIx(
<g/>
3900	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Zc	Zc	k1gFnSc1
<g/>
(	(	kIx(
<g/>
4430	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Zcs	Zcs	k1gFnSc1
<g/>
(	(	kIx(
<g/>
4000	#num#	k4
<g/>
)	)	kIx)
<g/>
+	+	kIx~
·	·	k?
Zcs	Zcs	k1gFnSc1
<g/>
(	(	kIx(
<g/>
4220	#num#	k4
<g/>
)	)	kIx)
<g/>
+	+	kIx~
pentakvarky	pentakvark	k1gInPc1
<g/>
(	(	kIx(
<g/>
fermiony	fermion	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Pc	Pc	k?
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4312	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Pc	Pc	k1gFnSc1
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4380	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Pc	Pc	k1gFnSc1
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4440	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Pc	Pc	k1gFnSc1
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
4457	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
hexakvarky	hexakvarka	k1gFnPc1
<g/>
/	/	kIx~
<g/>
dibaryony	dibaryona	k1gFnPc1
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
d	d	k?
<g/>
*	*	kIx~
<g/>
(	(	kIx(
<g/>
2380	#num#	k4
<g/>
)	)	kIx)
GLUEBALLs	GLUEBALLs	k1gInSc1
<g/>
(	(	kIx(
<g/>
bosony	bosona	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
odderon	odderon	k1gMnSc1
·	·	k?
pomeron	pomeron	k1gMnSc1
(	(	kIx(
<g/>
hypotetický	hypotetický	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
další	další	k2eAgFnPc1d1
částice	částice	k1gFnPc1
</s>
<s>
atomové	atomový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
(	(	kIx(
<g/>
deuteron	deuteron	k1gInSc1
·	·	k?
triton	triton	k1gInSc1
·	·	k?
helion	helion	k1gInSc1
·	·	k?
částice	částice	k1gFnSc1
alfa	alfa	k1gFnSc1
·	·	k?
hyperjádro	hyperjádro	k6eAd1
<g/>
)	)	kIx)
·	·	k?
atom	atom	k1gInSc1
·	·	k?
superatom	superatom	k1gInSc1
·	·	k?
exotické	exotický	k2eAgInPc1d1
atomy	atom	k1gInPc1
(	(	kIx(
<g/>
pozitronium	pozitronium	k1gNnSc1
·	·	k?
mionium	mionium	k1gNnSc1
·	·	k?
onium	onium	k1gNnSc1
<g/>
)	)	kIx)
·	·	k?
molekula	molekula	k1gFnSc1
hypotetické	hypotetický	k2eAgFnPc1d1
</s>
<s>
mezony	mezon	k1gInPc1
<g/>
/	/	kIx~
<g/>
kvarkonia	kvarkonium	k1gNnPc1
</s>
<s>
mezon	mezon	k1gInSc1
théta	théta	k1gNnSc2
·	·	k?
mezon	mezon	k1gInSc1
T	T	kA
exotické	exotický	k2eAgInPc1d1
mezony	mezon	k1gInPc1
</s>
<s>
kvark-antikvark-gluonové	kvark-antikvark-gluonové	k2eAgNnPc7d1
kompozity	kompozitum	k1gNnPc7
ostatní	ostatní	k2eAgMnPc4d1
</s>
<s>
dikvarky	dikvarka	k1gFnPc1
·	·	k?
leptokvarky	leptokvarka	k1gFnSc2
·	·	k?
pomeron	pomeron	k1gMnSc1
</s>
<s>
Kvazičástice	Kvazičástika	k1gFnSc3
</s>
<s>
Davydovův	Davydovův	k2eAgInSc1d1
soliton	soliton	k1gInSc1
·	·	k?
dripleton	dripleton	k1gInSc1
·	·	k?
elektronová	elektronový	k2eAgFnSc1d1
díra	díra	k1gFnSc1
·	·	k?
exciton	exciton	k1gInSc1
·	·	k?
fonon	fonon	k1gMnSc1
·	·	k?
magnon	magnon	k1gMnSc1
·	·	k?
plazmaron	plazmaron	k1gMnSc1
·	·	k?
plazmon	plazmon	k1gMnSc1
·	·	k?
polariton	polariton	k1gInSc1
·	·	k?
polaron	polaron	k1gInSc1
·	·	k?
roton	roton	k1gInSc1
·	·	k?
skyrmion	skyrmion	k1gInSc1
·	·	k?
trion	trion	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4035396-5	4035396-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
3698	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85076124	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85076124	#num#	k4
</s>
