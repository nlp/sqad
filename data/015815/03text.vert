<s>
Juan	Juan	k1gMnSc1
Rulfo	Rulfo	k1gMnSc1
</s>
<s>
Juan	Juan	k1gMnSc1
Nepomuceno	Nepomucen	k2eAgNnSc1d1
Carlos	Carlos	k1gMnSc1
Pérez	Péreza	k1gFnPc2
Rulfo	Rulfo	k1gMnSc1
Vizcaíno	Vizcaína	k1gFnSc5
Rodné	rodný	k2eAgMnPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Juan	Juan	k1gMnSc1
Nepomuceno	Nepomucen	k2eAgNnSc1d1
Carlos	Carlos	k1gMnSc1
Pérez	Péreza	k1gFnPc2
Rulfo	Rulfo	k1gMnSc1
Vizcaíno	Vizcaína	k1gFnSc5
Narození	narození	k1gNnPc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1917	#num#	k4
San	San	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Municipality	municipalita	k1gFnSc2
Úmrtí	úmrtí	k1gNnPc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1986	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
68	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
karcinom	karcinom	k1gInSc1
plic	plíce	k1gFnPc2
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
scenárista	scenárista	k1gMnSc1
<g/>
,	,	kIx,
fotograf	fotograf	k1gMnSc1
a	a	k8xC
romanopisec	romanopisec	k1gMnSc1
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
El	Ela	k1gFnPc2
Llano	Llano	k6eAd1
en	en	k?
llamasPedro	llamasPedro	k6eAd1
Páramo	Párama	k1gFnSc5
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Premio	Premio	k6eAd1
Xavier	Xavier	k1gInSc1
Villaurrutia	Villaurrutium	k1gNnSc2
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
Premio	Premio	k1gMnSc1
Nacional	Nacional	k1gMnSc1
de	de	k?
Ciencias	Ciencias	k1gMnSc1
y	y	k?
Artes	Artes	k1gMnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
Literární	literární	k2eAgFnSc1d1
cena	cena	k1gFnSc1
prince	princ	k1gMnSc2
asturského	asturský	k2eAgInSc2d1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
Premio	Premio	k6eAd1
Alfaguara	Alfaguara	k1gFnSc1
de	de	k?
Novela	novela	k1gFnSc1
Vlivy	vliv	k1gInPc4
</s>
<s>
William	William	k6eAd1
Faulkner	Faulkner	k1gInSc1
</s>
<s>
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Juan	Juan	k1gMnSc1
Rulfo	Rulfo	k1gMnSc1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1917	#num#	k4
<g/>
,	,	kIx,
Sayula	Sayula	k1gFnSc1
<g/>
,	,	kIx,
Jalisco	Jalisco	k1gNnSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1986	#num#	k4
<g/>
,	,	kIx,
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgMnS
mexický	mexický	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
fotograf	fotograf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c4
opravdového	opravdový	k2eAgMnSc4d1
tvůrce	tvůrce	k1gMnSc4
magického	magický	k2eAgInSc2d1
realismu	realismus	k1gInSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
román	román	k1gInSc1
Pedro	Pedro	k1gNnSc1
Páramo	Párama	k1gFnSc5
velmi	velmi	k6eAd1
ovlivnil	ovlivnit	k5eAaPmAgInS
tvorbu	tvorba	k1gFnSc4
Gabriela	Gabriel	k1gMnSc2
Garcíi	Garcí	k1gFnSc2
Márqueze	Márqueze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Juan	Juan	k1gMnSc1
Rulfo	Rulfo	k1gMnSc1
je	být	k5eAaImIp3nS
kromě	kromě	k7c2
románu	román	k1gInSc2
Pedro	Pedro	k1gMnSc1
Páramo	Párama	k1gMnSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
autorem	autor	k1gMnSc7
sbírky	sbírka	k1gFnSc2
povídek	povídka	k1gFnPc2
El	Ela	k1gFnPc2
Llano	Llano	k6eAd1
en	en	k?
llamas	llamas	k1gInSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
Llano	Llano	k6eAd1
v	v	k7c6
plamenech	plamen	k1gInPc6
<g/>
,1964	,1964	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Juan	Juan	k1gMnSc1
Rulfo	Rulfo	k1gMnSc1
-	-	kIx~
Página	Página	k1gMnSc1
oficial	oficial	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
juan-rulfo	juan-rulfo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Juan	Juan	k1gMnSc1
Rulfo	Rulfo	k6eAd1
a	a	k8xC
30	#num#	k4
añ	añ	k1gMnSc1
de	de	k?
su	su	k?
muerte	muert	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
El	Ela	k1gFnPc2
Universal	Universal	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
mexických	mexický	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Juan	Juan	k1gMnSc1
Nepomuceno	Nepomucen	k2eAgNnSc1d1
Carlos	Carlos	k1gMnSc1
Pérez	Péreza	k1gFnPc2
Rulfo	Rulfo	k1gMnSc1
Vizcaíno	Vizcaína	k1gFnSc5
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Juan	Juan	k1gMnSc1
Rulfo	Rulfo	k1gMnSc1
</s>
<s>
Heslo	heslo	k1gNnSc1
'	'	kIx"
<g/>
Juan	Juan	k1gMnSc1
Rulfo	Rulfo	k1gMnSc1
<g/>
'	'	kIx"
na	na	k7c6
stránce	stránka	k1gFnSc6
Databazeknih	Databazekniha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Heslo	heslo	k1gNnSc1
'	'	kIx"
<g/>
Juan	Juan	k1gMnSc1
Rulfo	Rulfo	k1gMnSc1
<g/>
'	'	kIx"
na	na	k7c6
stránce	stránka	k1gFnSc6
Britannica	Britannic	k1gInSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000701545	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118750348	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2099	#num#	k4
0118	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50022117	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500288263	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
9853151	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50022117	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
|	|	kIx~
Fotografie	fotografie	k1gFnSc1
|	|	kIx~
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
a	a	k8xC
Karibik	Karibik	k1gInSc1
</s>
