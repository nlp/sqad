<s>
Infekce	infekce	k1gFnSc1	infekce
Mycoplasma	Mycoplasma	k1gFnSc1	Mycoplasma
meleagridis	meleagridis	k1gFnSc1	meleagridis
(	(	kIx(	(
<g/>
MM	mm	kA	mm
<g/>
)	)	kIx)	)
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
hromadného	hromadný	k2eAgNnSc2d1	hromadné
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zánětem	zánět	k1gInSc7	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
,	,	kIx,	,
deformacemi	deformace	k1gFnPc7	deformace
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
snižováním	snižování	k1gNnSc7	snižování
hmotnostních	hmotnostní	k2eAgMnPc2d1	hmotnostní
přírůstků	přírůstek	k1gInPc2	přírůstek
<g/>
,	,	kIx,	,
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
embryonální	embryonální	k2eAgFnSc7d1	embryonální
mortalitou	mortalita	k1gFnSc7	mortalita
v	v	k7c6	v
líhních	líheň	k1gFnPc6	líheň
<g/>
.	.	kIx.	.
</s>
<s>
Adler	Adler	k1gMnSc1	Adler
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
první	první	k4xOgMnPc1	první
prokázali	prokázat	k5eAaPmAgMnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
aerosakulitida	aerosakulitida	k1gFnSc1	aerosakulitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
)	)	kIx)	)
u	u	k7c2	u
1	[number]	k4	1
<g/>
denních	denní	k2eAgNnPc2d1	denní
krůťat	krůtě	k1gNnPc2	krůtě
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
způsobena	způsobit	k5eAaPmNgFnS	způsobit
jinou	jiný	k2eAgFnSc7d1	jiná
mykoplasmou	mykoplasma	k1gFnSc7	mykoplasma
než	než	k8xS	než
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc1	gallisepticum
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
M.	M.	kA	M.
meleagridis	meleagridis	k1gFnPc1	meleagridis
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ubikvitárně	ubikvitárně	k6eAd1	ubikvitárně
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
také	také	k9	také
frekvence	frekvence	k1gFnSc1	frekvence
MM-aerosakulitid	MMerosakulitis	k1gFnPc2	MM-aerosakulitis
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
častá	častý	k2eAgFnSc1d1	častá
v	v	k7c6	v
komerčních	komerční	k2eAgInPc6d1	komerční
chovech	chov	k1gInPc6	chov
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
vyspělejších	vyspělý	k2eAgFnPc6d2	vyspělejší
zemích	zem	k1gFnPc6	zem
však	však	k9	však
již	již	k6eAd1	již
existují	existovat	k5eAaImIp3nP	existovat
chovy	chov	k1gInPc1	chov
prosté	prostý	k2eAgInPc1d1	prostý
této	tento	k3xDgFnSc2	tento
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
Mycoplasma	Mycoplasma	k1gFnSc1	Mycoplasma
meleagridis	meleagridis	k1gFnSc1	meleagridis
(	(	kIx(	(
<g/>
MM	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obarvení	obarvení	k1gNnSc6	obarvení
Geimsou	Geimsa	k1gFnSc7	Geimsa
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
kokoidní	kokoidní	k2eAgNnPc4d1	kokoidní
tělíska	tělísko	k1gNnPc4	tělísko
velikosti	velikost	k1gFnSc2	velikost
0,4	[number]	k4	0,4
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Ribosomy	Ribosom	k1gInPc1	Ribosom
jsou	být	k5eAaImIp3nP	být
rozloženy	rozložit	k5eAaPmNgFnP	rozložit
kolem	kolem	k7c2	kolem
buněčné	buněčný	k2eAgFnSc2d1	buněčná
periferie	periferie	k1gFnSc2	periferie
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
morfologickým	morfologický	k2eAgInSc7d1	morfologický
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
sférická	sférický	k2eAgFnSc1d1	sférická
forma	forma	k1gFnSc1	forma
velikosti	velikost	k1gFnSc2	velikost
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
700	[number]	k4	700
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Replikuje	replikovat	k5eAaImIp3nS	replikovat
se	se	k3xPyFc4	se
binárním	binární	k2eAgNnSc7d1	binární
dělením	dělení	k1gNnSc7	dělení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
DNA	dno	k1gNnSc2	dno
kmene	kmen	k1gInSc2	kmen
17529	[number]	k4	17529
je	být	k5eAaImIp3nS	být
složení	složení	k1gNnSc2	složení
bází	báze	k1gFnPc2	báze
G	G	kA	G
<g/>
+	+	kIx~	+
<g/>
C	C	kA	C
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
%	%	kIx~	%
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
genomu	genom	k1gInSc2	genom
4,2	[number]	k4	4,2
±	±	k?	±
0,5	[number]	k4	0,5
<g/>
×	×	k?	×
<g/>
108	[number]	k4	108
daltonu	dalton	k1gInSc2	dalton
<g/>
.	.	kIx.	.
</s>
<s>
MM	mm	kA	mm
je	být	k5eAaImIp3nS	být
fakultativní	fakultativní	k2eAgMnSc1d1	fakultativní
anaerob	anaerob	k1gMnSc1	anaerob
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
optimálně	optimálně	k6eAd1	optimálně
na	na	k7c6	na
agaru	agar	k1gInSc6	agar
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
38	[number]	k4	38
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
kolonie	kolonie	k1gFnSc1	kolonie
vznikající	vznikající	k2eAgFnSc1d1	vznikající
během	během	k7c2	během
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
dní	den	k1gInPc2	den
inkubace	inkubace	k1gFnSc2	inkubace
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnPc1	velikost
0,04	[number]	k4	0,04
<g/>
-	-	kIx~	-
<g/>
0,2	[number]	k4	0,2
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
médií	médium	k1gNnPc2	médium
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgInPc1d1	vysoký
<g/>
,	,	kIx,	,
nezbytné	zbytný	k2eNgInPc1d1	zbytný
jsou	být	k5eAaImIp3nP	být
kvasničný	kvasničný	k2eAgInSc4d1	kvasničný
autolyzát	autolyzát	k1gInSc4	autolyzát
a	a	k8xC	a
prasečí	prasečí	k2eAgNnSc4d1	prasečí
nebo	nebo	k8xC	nebo
koňské	koňský	k2eAgNnSc4d1	koňské
sérum	sérum	k1gNnSc4	sérum
<g/>
.	.	kIx.	.
</s>
<s>
Drůbeží	drůbeží	k2eAgNnPc1d1	drůbeží
séra	sérum	k1gNnPc1	sérum
nejsou	být	k5eNaImIp3nP	být
vhodná	vhodný	k2eAgNnPc1d1	vhodné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bujón	bujón	k1gInSc4	bujón
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
izolátů	izolát	k1gInPc2	izolát
špatně	špatně	k6eAd1	špatně
adaptuje	adaptovat	k5eAaBmIp3nS	adaptovat
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
meleagridis	meleagridis	k1gFnSc1	meleagridis
nefermentuje	fermentovat	k5eNaBmIp3nS	fermentovat
dextrózu	dextróza	k1gFnSc4	dextróza
ani	ani	k8xC	ani
jiné	jiný	k2eAgInPc4d1	jiný
cukry	cukr	k1gInPc4	cukr
<g/>
,	,	kIx,	,
neredukuje	redukovat	k5eNaBmIp3nS	redukovat
tetrazoliové	tetrazoliový	k2eAgFnPc4d1	tetrazoliový
soli	sůl	k1gFnPc4	sůl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
argininovou	argininový	k2eAgFnSc4d1	argininový
a	a	k8xC	a
fosfatázovou	fosfatázový	k2eAgFnSc4d1	fosfatázový
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Hemolyzuje	Hemolyzovat	k5eAaBmIp3nS	Hemolyzovat
koňské	koňský	k2eAgInPc4d1	koňský
erytrocyty	erytrocyt	k1gInPc4	erytrocyt
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obvykle	obvykle	k6eAd1	obvykle
používaných	používaný	k2eAgInPc2d1	používaný
dezinfekčních	dezinfekční	k2eAgInPc2d1	dezinfekční
prostředků	prostředek	k1gInPc2	prostředek
je	být	k5eAaImIp3nS	být
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
médiu	médium	k1gNnSc6	médium
při	při	k7c6	při
pH	ph	kA	ph
8,4	[number]	k4	8,4
<g/>
-	-	kIx~	-
<g/>
8,7	[number]	k4	8,7
přežívá	přežívat	k5eAaImIp3nS	přežívat
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
dní	den	k1gInPc2	den
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
titru	titr	k1gInSc6	titr
<g/>
,	,	kIx,	,
čerstvé	čerstvý	k2eAgFnPc1d1	čerstvá
agarové	agarový	k2eAgFnPc1d1	agarový
kultury	kultura	k1gFnPc1	kultura
přežívají	přežívat	k5eAaImIp3nP	přežívat
nejméně	málo	k6eAd3	málo
6	[number]	k4	6
dní	den	k1gInPc2	den
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Teplotou	teplota	k1gFnSc7	teplota
45	[number]	k4	45
°	°	k?	°
<g/>
C	C	kA	C
je	být	k5eAaImIp3nS	být
inaktivována	inaktivovat	k5eAaBmNgFnS	inaktivovat
během	během	k7c2	během
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
teplotou	teplota	k1gFnSc7	teplota
47	[number]	k4	47
°	°	k?	°
<g/>
C	C	kA	C
již	již	k6eAd1	již
během	během	k7c2	během
40	[number]	k4	40
<g/>
-	-	kIx~	-
<g/>
120	[number]	k4	120
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Lyofilizované	Lyofilizovaný	k2eAgFnSc2d1	Lyofilizovaná
kultury	kultura	k1gFnSc2	kultura
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
životné	životný	k2eAgNnSc1d1	životné
nejméně	málo	k6eAd3	málo
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
meleagridis	meleagridis	k1gFnSc1	meleagridis
dobře	dobře	k6eAd1	dobře
přežívá	přežívat	k5eAaImIp3nS	přežívat
bez	bez	k7c2	bez
podstatného	podstatný	k2eAgInSc2d1	podstatný
poklesu	pokles	k1gInSc2	pokles
životnosti	životnost	k1gFnSc2	životnost
ve	v	k7c6	v
zmraženém	zmražený	k2eAgNnSc6d1	zmražené
krůtím	krůtí	k2eAgNnSc6d1	krůtí
semenu	semeno	k1gNnSc6	semeno
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
meleagridis	meleagridis	k1gInSc1	meleagridis
se	se	k3xPyFc4	se
antigenně	antigenně	k6eAd1	antigenně
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
aviárních	aviární	k2eAgNnPc2d1	aviární
mykoplasmat	mykoplasma	k1gNnPc2	mykoplasma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
typizaci	typizace	k1gFnSc3	typizace
MM	mm	kA	mm
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
aglutinace	aglutinace	k1gFnPc1	aglutinace
<g/>
,	,	kIx,	,
imunofluorescence	imunofluorescence	k1gFnPc1	imunofluorescence
<g/>
,	,	kIx,	,
testy	test	k1gInPc1	test
inhibice	inhibice	k1gFnSc2	inhibice
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
metabolismu	metabolismus	k1gInSc2	metabolismus
a	a	k8xC	a
komplement	komplement	k1gInSc4	komplement
fixační	fixační	k2eAgInSc1d1	fixační
test	test	k1gInSc1	test
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
izoláty	izolát	k1gInPc1	izolát
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
hemaglutinační	hemaglutinační	k2eAgFnSc4d1	hemaglutinační
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k9	ale
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
virulencí	virulence	k1gFnSc7	virulence
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
patogenní	patogenní	k2eAgInPc4d1	patogenní
i	i	k8xC	i
nepatogenní	patogenní	k2eNgInPc4d1	nepatogenní
kmeny	kmen	k1gInPc4	kmen
M.	M.	kA	M.
meleagridis	meleagridis	k1gFnSc1	meleagridis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klinické	klinický	k2eAgFnSc6d1	klinická
manifestaci	manifestace	k1gFnSc6	manifestace
nemoci	nemoc	k1gFnSc2	nemoc
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
i	i	k9	i
kmenová	kmenový	k2eAgFnSc1d1	kmenová
variabilita	variabilita	k1gFnSc1	variabilita
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
meleagridis	meleagridis	k1gInSc1	meleagridis
je	být	k5eAaImIp3nS	být
specifický	specifický	k2eAgInSc1d1	specifický
patogen	patogen	k1gInSc1	patogen
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
krůtakrůty	krůtakrůta	k1gFnPc4	krůtakrůta
<g/>
;	;	kIx,	;
kur	kur	k1gMnSc1	kur
domácí	domácí	k1gMnSc1	domácí
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
nevnímavý	vnímavý	k2eNgInSc1d1	nevnímavý
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
meleagridis	meleagridis	k1gInSc1	meleagridis
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
infekciozitou	infekciozita	k1gFnSc7	infekciozita
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nS	přenášet
se	se	k3xPyFc4	se
vertikálně	vertikálně	k6eAd1	vertikálně
i	i	k9	i
horizontálně	horizontálně	k6eAd1	horizontálně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přenášena	přenášet	k5eAaImNgFnS	přenášet
mezi	mezi	k7c7	mezi
krůtami	krůta	k1gFnPc7	krůta
v	v	k7c6	v
kterémkoliv	kterýkoliv	k3yIgNnSc6	kterýkoliv
věku	věk	k1gInSc2	věk
přímým	přímý	k2eAgInSc7d1	přímý
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
infikovanými	infikovaný	k2eAgMnPc7d1	infikovaný
jedinci	jedinec	k1gMnPc7	jedinec
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
přes	přes	k7c4	přes
kontaminované	kontaminovaný	k2eAgNnSc4d1	kontaminované
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přímému	přímý	k2eAgInSc3d1	přímý
přenosu	přenos	k1gInSc3	přenos
infekce	infekce	k1gFnSc2	infekce
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
líhních	líheň	k1gFnPc6	líheň
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hejna	hejno	k1gNnSc2	hejno
nebo	nebo	k8xC	nebo
i	i	k9	i
mezi	mezi	k7c7	mezi
hejny	hejno	k1gNnPc7	hejno
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímý	přímý	k2eNgInSc1d1	nepřímý
přenos	přenos	k1gInSc1	přenos
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
podmínkách	podmínka	k1gFnPc6	podmínka
ošetřování	ošetřování	k1gNnSc2	ošetřování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
krůt	krůta	k1gFnPc2	krůta
má	mít	k5eAaImIp3nS	mít
horizontální	horizontální	k2eAgInSc1d1	horizontální
přenos	přenos	k1gInSc1	přenos
jen	jen	k9	jen
malý	malý	k2eAgInSc4d1	malý
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Vertikální	vertikální	k2eAgInSc1d1	vertikální
přenos	přenos	k1gInSc1	přenos
M.	M.	kA	M.
meleagridis	meleagridis	k1gInSc1	meleagridis
představuje	představovat	k5eAaImIp3nS	představovat
nejzávažnější	závažný	k2eAgInSc4d3	nejzávažnější
způsob	způsob	k1gInSc4	způsob
šíření	šíření	k1gNnSc2	šíření
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
reprodukčního	reprodukční	k2eAgInSc2d1	reprodukční
traktu	trakt	k1gInSc2	trakt
krůt	krůta	k1gFnPc2	krůta
dochází	docházet	k5eAaImIp3nS	docházet
primárně	primárně	k6eAd1	primárně
po	po	k7c6	po
inseminaci	inseminace	k1gFnSc6	inseminace
infekčním	infekční	k2eAgNnSc7d1	infekční
semenem	semeno	k1gNnSc7	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
vejce	vejce	k1gNnSc2	vejce
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
to	ten	k3xDgNnSc4	ten
nejsou	být	k5eNaImIp3nP	být
vaječníky	vaječník	k1gInPc1	vaječník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
nepodařilo	podařit	k5eNaPmAgNnS	podařit
MM	mm	kA	mm
prokázat	prokázat	k5eAaPmF	prokázat
ani	ani	k8xC	ani
u	u	k7c2	u
vylučujících	vylučující	k2eAgFnPc2d1	vylučující
nosnic	nosnice	k1gFnPc2	nosnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vejcovod	vejcovod	k1gInSc1	vejcovod
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
přenosu	přenos	k1gInSc2	přenos
známý	známý	k2eAgMnSc1d1	známý
u	u	k7c2	u
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc1	gallisepticum
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
kontaktem	kontakt	k1gInSc7	kontakt
infikovaného	infikovaný	k2eAgInSc2d1	infikovaný
abdominálního	abdominální	k2eAgInSc2d1	abdominální
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
vaku	vak	k1gInSc2	vak
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
ovariálního	ovariální	k2eAgInSc2d1	ovariální
folikulu	folikul	k1gInSc2	folikul
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
MM	mm	kA	mm
se	se	k3xPyFc4	se
lokalizuje	lokalizovat	k5eAaBmIp3nS	lokalizovat
v	v	k7c6	v
pohlavním	pohlavní	k2eAgInSc6d1	pohlavní
aparátu	aparát	k1gInSc6	aparát
krůt	krůta	k1gFnPc2	krůta
i	i	k8xC	i
krocanů	krocan	k1gMnPc2	krocan
(	(	kIx(	(
<g/>
kloaka	kloaka	k1gFnSc1	kloaka
<g/>
,	,	kIx,	,
penis	penis	k1gInSc1	penis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
perzistovat	perzistovat	k5eAaPmF	perzistovat
až	až	k9	až
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
vertikálně	vertikálně	k6eAd1	vertikálně
dochází	docházet	k5eAaImIp3nP	docházet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
embryonálního	embryonální	k2eAgInSc2d1	embryonální
vývoje	vývoj	k1gInSc2	vývoj
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
k	k	k7c3	k
imunologické	imunologický	k2eAgFnSc3d1	imunologická
toleranci	tolerance	k1gFnSc3	tolerance
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
M.	M.	kA	M.
meleagridis	meleagridis	k1gInSc1	meleagridis
perzistuje	perzistovat	k5eAaPmIp3nS	perzistovat
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
ve	v	k7c6	v
vejcovodu	vejcovod	k1gInSc6	vejcovod
mnohem	mnohem	k6eAd1	mnohem
déle	dlouho	k6eAd2	dlouho
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
krůtami	krůta	k1gFnPc7	krůta
infikovanými	infikovaný	k2eAgFnPc7d1	infikovaná
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
infikovaných	infikovaný	k2eAgNnPc2d1	infikované
násadových	násadový	k2eAgNnPc2d1	násadový
vajec	vejce	k1gNnPc2	vejce
je	být	k5eAaImIp3nS	být
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
embryonální	embryonální	k2eAgFnSc1d1	embryonální
mortalita	mortalita	k1gFnSc1	mortalita
a	a	k8xC	a
krůťata	krůtě	k1gNnPc4	krůtě
líhnutá	líhnutý	k2eAgNnPc4d1	líhnutý
z	z	k7c2	z
takových	takový	k3xDgNnPc2	takový
vajec	vejce	k1gNnPc2	vejce
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
týdnech	týden	k1gInPc6	týden
života	život	k1gInSc2	život
snížení	snížení	k1gNnSc1	snížení
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
přírůstků	přírůstek	k1gInPc2	přírůstek
i	i	k8xC	i
životaschopnosti	životaschopnost	k1gFnSc2	životaschopnost
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc4	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
(	(	kIx(	(
<g/>
aerosakulitidy	aerosakulitis	k1gFnSc2	aerosakulitis
<g/>
)	)	kIx)	)
a	a	k8xC	a
kostní	kostní	k2eAgFnSc2d1	kostní
abnormality	abnormalita	k1gFnSc2	abnormalita
<g/>
.	.	kIx.	.
</s>
<s>
Klinický	klinický	k2eAgInSc1d1	klinický
obraz	obraz	k1gInSc1	obraz
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
virulenci	virulence	k1gFnSc6	virulence
M.	M.	kA	M.
meleagridis	meleagridis	k1gFnPc1	meleagridis
<g/>
,	,	kIx,	,
přítomnosti	přítomnost	k1gFnPc1	přítomnost
stresujících	stresující	k2eAgInPc2d1	stresující
faktorů	faktor	k1gInPc2	faktor
a	a	k8xC	a
kontaminujících	kontaminující	k2eAgFnPc2d1	kontaminující
infekcí	infekce	k1gFnPc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Klinicky	klinicky	k6eAd1	klinicky
zjevné	zjevný	k2eAgInPc1d1	zjevný
respirační	respirační	k2eAgInPc1d1	respirační
příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
při	při	k7c6	při
infekci	infekce	k1gFnSc6	infekce
samotné	samotný	k2eAgFnSc6d1	samotná
M.	M.	kA	M.
meleagridis	meleagridis	k1gFnPc1	meleagridis
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
poměrně	poměrně	k6eAd1	poměrně
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
poměrně	poměrně	k6eAd1	poměrně
častý	častý	k2eAgInSc4d1	častý
pitevní	pitevní	k2eAgInSc4d1	pitevní
nález	nález	k1gInSc4	nález
aerosakulitid	aerosakulitis	k1gFnPc2	aerosakulitis
u	u	k7c2	u
krůťat	krůtě	k1gNnPc2	krůtě
vertikálně	vertikálně	k6eAd1	vertikálně
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
<g/>
.	.	kIx.	.
</s>
<s>
Nestálým	stálý	k2eNgInSc7d1	nestálý
rysem	rys	k1gInSc7	rys
nemoci	nemoc	k1gFnPc4	nemoc
u	u	k7c2	u
takto	takto	k6eAd1	takto
infikovaných	infikovaný	k2eAgNnPc2d1	infikované
krůťat	krůtě	k1gNnPc2	krůtě
je	být	k5eAaImIp3nS	být
také	také	k9	také
výskyt	výskyt	k1gInSc1	výskyt
kostních	kostní	k2eAgFnPc2d1	kostní
abnormalit	abnormalita	k1gFnPc2	abnormalita
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgInPc4d1	projevující
se	s	k7c7	s
prohnutím	prohnutí	k1gNnSc7	prohnutí
<g/>
,	,	kIx,	,
zkroucením	zkroucení	k1gNnSc7	zkroucení
nebo	nebo	k8xC	nebo
zkrácením	zkrácení	k1gNnSc7	zkrácení
tarzometatarzálních	tarzometatarzální	k2eAgFnPc2d1	tarzometatarzální
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
zduřením	zduření	k1gNnSc7	zduření
kloubu	kloub	k1gInSc2	kloub
hlezna	hlezno	k1gNnSc2	hlezno
a	a	k8xC	a
deformací	deformace	k1gFnPc2	deformace
krčních	krční	k2eAgInPc2d1	krční
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zakrslost	zakrslost	k1gFnSc1	zakrslost
či	či	k8xC	či
abnormality	abnormalita	k1gFnPc1	abnormalita
v	v	k7c6	v
opeření	opeření	k1gNnSc6	opeření
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úhynům	úhyn	k1gInPc3	úhyn
dochází	docházet	k5eAaImIp3nS	docházet
spíše	spíše	k9	spíše
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kanibalismu	kanibalismus	k1gInSc2	kanibalismus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
respiračnímu	respirační	k2eAgNnSc3d1	respirační
onemocnění	onemocnění	k1gNnSc3	onemocnění
s	s	k7c7	s
typickými	typický	k2eAgInPc7d1	typický
příznaky	příznak	k1gInPc7	příznak
většinou	většinou	k6eAd1	většinou
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
simultánních	simultánní	k2eAgFnPc6d1	simultánní
infekcích	infekce	k1gFnPc6	infekce
dalšímimykoplasmaty	dalšímimykoplasma	k1gNnPc7	dalšímimykoplasma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
synergicky	synergicky	k?	synergicky
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
infekce	infekce	k1gFnSc1	infekce
MM	mm	kA	mm
s	s	k7c7	s
M.	M.	kA	M.
iowae	iowae	k6eAd1	iowae
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
těžké	těžký	k2eAgInPc4d1	těžký
záněty	zánět	k1gInPc4	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
,	,	kIx,	,
s	s	k7c7	s
M.	M.	kA	M.
synoviae	synoviaat	k5eAaPmIp3nS	synoviaat
pak	pak	k6eAd1	pak
sinusitidy	sinusitida	k1gFnPc4	sinusitida
<g/>
.	.	kIx.	.
</s>
<s>
Horizontální	horizontální	k2eAgFnSc1d1	horizontální
infekce	infekce	k1gFnSc1	infekce
krůt	krůta	k1gFnPc2	krůta
probíhá	probíhat	k5eAaImIp3nS	probíhat
zpravidla	zpravidla	k6eAd1	zpravidla
subklinicky	subklinicky	k6eAd1	subklinicky
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
meleagridis	meleagridis	k1gFnSc1	meleagridis
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
snášku	snáška	k1gFnSc4	snáška
ani	ani	k8xC	ani
oplozenost	oplozenost	k1gFnSc4	oplozenost
násadových	násadový	k2eAgNnPc2d1	násadový
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
mortalitu	mortalita	k1gFnSc4	mortalita
embryí	embryo	k1gNnPc2	embryo
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
fázi	fáze	k1gFnSc6	fáze
inkubace	inkubace	k1gFnSc2	inkubace
<g/>
.	.	kIx.	.
</s>
<s>
Negativně	negativně	k6eAd1	negativně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
růst	růst	k1gInSc4	růst
i	i	k8xC	i
hmotnost	hmotnost	k1gFnSc4	hmotnost
mladé	mladý	k2eAgFnSc2d1	mladá
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
počet	počet	k1gInSc1	počet
brakovaných	brakovaný	k2eAgInPc2d1	brakovaný
a	a	k8xC	a
na	na	k7c6	na
porážce	porážka	k1gFnSc6	porážka
konfiskovaných	konfiskovaný	k2eAgNnPc2d1	konfiskované
zvířat	zvíře	k1gNnPc2	zvíře
pro	pro	k7c4	pro
zánět	zánět	k1gInSc4	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
jsou	být	k5eAaImIp3nP	být
charakterizovány	charakterizovat	k5eAaBmNgInP	charakterizovat
zesílením	zesílení	k1gNnSc7	zesílení
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
nažloutlým	nažloutlý	k2eAgInSc7d1	nažloutlý
exsudátem	exsudát	k1gInSc7	exsudát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
na	na	k7c6	na
sousedících	sousedící	k2eAgFnPc6d1	sousedící
tkáních	tkáň	k1gFnPc6	tkáň
a	a	k8xC	a
občasnou	občasný	k2eAgFnSc7d1	občasná
přítomností	přítomnost	k1gFnSc7	přítomnost
různě	různě	k6eAd1	různě
velikých	veliký	k2eAgFnPc2d1	veliká
hrudek	hrudka	k1gFnPc2	hrudka
kaseózního	kaseózní	k2eAgInSc2d1	kaseózní
(	(	kIx(	(
<g/>
sýrovitého	sýrovitý	k2eAgInSc2d1	sýrovitý
<g/>
)	)	kIx)	)
materiálu	materiál	k1gInSc2	materiál
volně	volně	k6eAd1	volně
uloženého	uložený	k2eAgInSc2d1	uložený
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
tělní	tělní	k2eAgFnSc6d1	tělní
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
vyvolávané	vyvolávaný	k2eAgFnPc1d1	vyvolávaná
samotnou	samotný	k2eAgFnSc7d1	samotná
MM	mm	kA	mm
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
intenzivní	intenzivní	k2eAgInSc4d1	intenzivní
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
simultánní	simultánní	k2eAgFnSc7d1	simultánní
infekcí	infekce	k1gFnSc7	infekce
s	s	k7c7	s
M.	M.	kA	M.
iowae	iowae	k1gFnSc7	iowae
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
kostních	kostní	k2eAgFnPc2d1	kostní
změn	změna	k1gFnPc2	změna
obvykle	obvykle	k6eAd1	obvykle
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
aerosakulitid	aerosakulitis	k1gFnPc2	aerosakulitis
<g/>
.	.	kIx.	.
</s>
<s>
Mateřské	mateřský	k2eAgFnPc1d1	mateřská
protilátky	protilátka	k1gFnPc1	protilátka
(	(	kIx(	(
<g/>
aglutininy	aglutinin	k1gInPc1	aglutinin
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
detekovatelné	detekovatelný	k2eAgFnPc1d1	detekovatelná
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
procentu	procent	k1gInSc6	procent
u	u	k7c2	u
krůťat	krůtě	k1gNnPc2	krůtě
od	od	k7c2	od
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
krůt	krůta	k1gFnPc2	krůta
<g/>
;	;	kIx,	;
perzistují	perzistovat	k5eAaPmIp3nP	perzistovat
asi	asi	k9	asi
2	[number]	k4	2
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
<g/>
.	.	kIx.	.
</s>
<s>
Nechrání	chránit	k5eNaImIp3nS	chránit
ale	ale	k9	ale
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
u	u	k7c2	u
infikovaných	infikovaný	k2eAgNnPc2d1	infikované
embryí	embryo	k1gNnPc2	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Perzistující	Perzistující	k2eAgFnPc1d1	Perzistující
infekce	infekce	k1gFnPc1	infekce
u	u	k7c2	u
vertikálně	vertikálně	k6eAd1	vertikálně
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
krůt	krůta	k1gFnPc2	krůta
je	být	k5eAaImIp3nS	být
vysvětlována	vysvětlovat	k5eAaImNgFnS	vysvětlovat
imunologickou	imunologický	k2eAgFnSc7d1	imunologická
tolerancí	tolerance	k1gFnSc7	tolerance
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
imunita	imunita	k1gFnSc1	imunita
při	při	k7c6	při
infekci	infekce	k1gFnSc6	infekce
M.	M.	kA	M.
meleagridis	meleagridis	k1gFnSc4	meleagridis
není	být	k5eNaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
Diagnostika	diagnostika	k1gFnSc1	diagnostika
zánětu	zánět	k1gInSc2	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
krůt	krůta	k1gFnPc2	krůta
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
stejných	stejný	k2eAgInPc2d1	stejný
postupů	postup	k1gInPc2	postup
jako	jako	k8xC	jako
při	při	k7c6	při
ostatních	ostatní	k2eAgFnPc6d1	ostatní
mykoplasmových	mykoplasmův	k2eAgFnPc6d1	mykoplasmův
infekcích	infekce	k1gFnPc6	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Záněty	zánět	k1gInPc1	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
M.	M.	kA	M.
meleagridis	meleagridis	k1gInSc1	meleagridis
nutno	nutno	k6eAd1	nutno
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
aerosakulitid	aerosakulitis	k1gFnPc2	aerosakulitis
vyvolávaných	vyvolávaný	k2eAgFnPc2d1	vyvolávaná
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc4	gallisepticum
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgNnPc7d1	jiné
mykoplasmaty	mykoplasma	k1gNnPc7	mykoplasma
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dalšími	další	k2eAgNnPc7d1	další
agens	agens	k1gNnPc7	agens
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
smíšenou	smíšený	k2eAgFnSc4d1	smíšená
infekci	infekce	k1gFnSc4	infekce
MM	mm	kA	mm
s	s	k7c7	s
M.	M.	kA	M.
synoviae	synoviae	k1gFnSc7	synoviae
nebo	nebo	k8xC	nebo
M.	M.	kA	M.
iowae	iowae	k6eAd1	iowae
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
embryonální	embryonální	k2eAgFnSc1d1	embryonální
mortalita	mortalita	k1gFnSc1	mortalita
<g/>
,	,	kIx,	,
sinusitidy	sinusitida	k1gFnPc1	sinusitida
a	a	k8xC	a
aerosakulitidy	aerosakulitis	k1gFnPc1	aerosakulitis
<g/>
.	.	kIx.	.
</s>
<s>
Kostní	kostní	k2eAgFnPc1d1	kostní
abnormality	abnormalita	k1gFnPc1	abnormalita
kromě	kromě	k7c2	kromě
MM	mm	kA	mm
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
také	také	k9	také
M.	M.	kA	M.
iowae	iowae	k1gInSc1	iowae
anebo	anebo	k8xC	anebo
dietetické	dietetický	k2eAgFnPc1d1	dietetická
závady	závada	k1gFnPc1	závada
<g/>
.	.	kIx.	.
</s>
<s>
Léčebná	léčebný	k2eAgNnPc1d1	léčebné
i	i	k8xC	i
preventivní	preventivní	k2eAgNnPc1d1	preventivní
opatření	opatření	k1gNnPc1	opatření
jsou	být	k5eAaImIp3nP	být
podobná	podobný	k2eAgNnPc1d1	podobné
jako	jako	k9	jako
při	při	k7c6	při
infekci	infekce	k1gFnSc6	infekce
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc1	gallisepticum
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
však	však	k9	však
pozorována	pozorován	k2eAgFnSc1d1	pozorována
nižší	nízký	k2eAgFnSc1d2	nižší
účinnost	účinnost	k1gFnSc1	účinnost
erytromycinu	erytromycin	k1gInSc2	erytromycin
u	u	k7c2	u
několika	několik	k4yIc2	několik
kmenů	kmen	k1gInPc2	kmen
M.	M.	kA	M.
meleagridis	meleagridis	k1gInSc1	meleagridis
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
frekvence	frekvence	k1gFnSc2	frekvence
vertikálního	vertikální	k2eAgInSc2d1	vertikální
přenosu	přenos	k1gInSc2	přenos
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
ani	ani	k8xC	ani
po	po	k7c4	po
injekční	injekční	k2eAgFnSc4d1	injekční
(	(	kIx(	(
<g/>
perorální	perorální	k2eAgFnSc4d1	perorální
<g/>
)	)	kIx)	)
aplikaci	aplikace	k1gFnSc4	aplikace
tylosinu	tylosin	k2eAgFnSc4d1	tylosin
v	v	k7c6	v
pitné	pitný	k2eAgFnSc6d1	pitná
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Lepších	dobrý	k2eAgInPc2d2	lepší
výsledků	výsledek	k1gInPc2	výsledek
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
ošetřením	ošetření	k1gNnSc7	ošetření
násadových	násadový	k2eAgNnPc2d1	násadový
vajec	vejce	k1gNnPc2	vejce
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
-	-	kIx~	-
omezení	omezení	k1gNnSc1	omezení
zánětu	zánět	k1gInSc2	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
,	,	kIx,	,
zlepšení	zlepšení	k1gNnSc3	zlepšení
líhnivosti	líhnivost	k1gFnSc2	líhnivost
<g/>
,	,	kIx,	,
užitkovosti	užitkovost	k1gFnSc2	užitkovost
<g/>
,	,	kIx,	,
redukce	redukce	k1gFnSc1	redukce
výskytu	výskyt	k1gInSc2	výskyt
kostních	kostní	k2eAgFnPc2d1	kostní
abnormalit	abnormalita	k1gFnPc2	abnormalita
a	a	k8xC	a
snížení	snížení	k1gNnSc1	snížení
konfiskací	konfiskace	k1gFnPc2	konfiskace
na	na	k7c6	na
porážce	porážka	k1gFnSc6	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
preventivních	preventivní	k2eAgNnPc2d1	preventivní
opatření	opatření	k1gNnPc2	opatření
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
největší	veliký	k2eAgInSc1d3	veliký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
provádění	provádění	k1gNnSc4	provádění
umělé	umělý	k2eAgFnSc2d1	umělá
inseminace	inseminace	k1gFnSc2	inseminace
a	a	k8xC	a
používání	používání	k1gNnSc2	používání
semene	semeno	k1gNnSc2	semeno
od	od	k7c2	od
zdravých	zdravý	k2eAgMnPc2d1	zdravý
neinfikovaných	infikovaný	k2eNgMnPc2d1	neinfikovaný
krocanů	krocan	k1gMnPc2	krocan
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgFnPc4d1	vhodná
vakcíny	vakcína	k1gFnPc4	vakcína
proti	proti	k7c3	proti
M.	M.	kA	M.
meleagridis	meleagridis	k1gFnPc1	meleagridis
nebyly	být	k5eNaImAgFnP	být
zatím	zatím	k6eAd1	zatím
připraveny	připravit	k5eAaPmNgFnP	připravit
<g/>
.	.	kIx.	.
</s>
