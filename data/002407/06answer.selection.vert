<s>
Infekce	infekce	k1gFnSc1	infekce
Mycoplasma	Mycoplasma	k1gFnSc1	Mycoplasma
meleagridis	meleagridis	k1gFnSc1	meleagridis
(	(	kIx(	(
<g/>
MM	mm	kA	mm
<g/>
)	)	kIx)	)
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
hromadného	hromadný	k2eAgNnSc2d1	hromadné
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zánětem	zánět	k1gInSc7	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
,	,	kIx,	,
deformacemi	deformace	k1gFnPc7	deformace
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
snižováním	snižování	k1gNnSc7	snižování
hmotnostních	hmotnostní	k2eAgMnPc2d1	hmotnostní
přírůstků	přírůstek	k1gInPc2	přírůstek
<g/>
,	,	kIx,	,
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
embryonální	embryonální	k2eAgFnSc7d1	embryonální
mortalitou	mortalita	k1gFnSc7	mortalita
v	v	k7c6	v
líhních	líheň	k1gFnPc6	líheň
<g/>
.	.	kIx.	.
</s>
