<s>
Reveal	Reveal	k1gMnSc1	Reveal
je	být	k5eAaImIp3nS	být
album	album	k1gNnSc4	album
kapely	kapela	k1gFnSc2	kapela
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
Imitation	Imitation	k1gInSc1	Imitation
of	of	k?	of
Life	Lif	k1gInSc2	Lif
se	se	k3xPyFc4	se
na	na	k7c6	na
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
až	až	k9	až
na	na	k7c4	na
83	[number]	k4	83
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
singly	singl	k1gInPc7	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
jsou	být	k5eAaImIp3nP	být
All	All	k1gFnSc7	All
the	the	k?	the
Way	Way	k1gFnSc2	Way
to	ten	k3xDgNnSc1	ten
Reno	Reno	k1gNnSc1	Reno
(	(	kIx(	(
<g/>
You	You	k1gFnSc1	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Gonna	Gonen	k2eAgFnSc1d1	Gonna
Be	Be	k1gFnSc1	Be
a	a	k8xC	a
Star	Star	kA	Star
<g/>
)	)	kIx)	)
a	a	k8xC	a
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Take	Take	k1gInSc1	Take
the	the	k?	the
Rain	Rain	k1gInSc1	Rain
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
kritiky	kritika	k1gFnSc2	kritika
na	na	k7c4	na
Reveal	Reveal	k1gInSc4	Reveal
byly	být	k5eAaImAgFnP	být
pozitivnější	pozitivní	k2eAgFnPc1d2	pozitivnější
než	než	k8xS	než
na	na	k7c4	na
album	album	k1gNnSc4	album
Up	Up	k1gFnSc2	Up
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
Reveal	Reveal	k1gInSc4	Reveal
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
šestém	šestý	k4xOgInSc6	šestý
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vydalo	vydat	k5eAaPmAgNnS	vydat
Warner	Warner	k1gInSc4	Warner
Brothers	Brothers	k1gInSc1	Brothers
Records	Records	k1gInSc1	Records
dvoudiskovou	dvoudiskový	k2eAgFnSc4d1	dvoudisková
edici	edice	k1gFnSc4	edice
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
CD	CD	kA	CD
<g/>
,	,	kIx,	,
DVD-Audio	DVD-Audio	k6eAd1	DVD-Audio
se	s	k7c7	s
zvukem	zvuk	k1gInSc7	zvuk
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
5.1	[number]	k4	5.1
a	a	k8xC	a
originální	originální	k2eAgInPc1d1	originální
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
<g/>
,	,	kIx,	,
booklet	booklet	k1gInSc1	booklet
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
Peter	Peter	k1gMnSc1	Peter
Buck	Buck	k1gMnSc1	Buck
<g/>
,	,	kIx,	,
Mike	Mike	k1gFnSc1	Mike
Mills	Millsa	k1gFnPc2	Millsa
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Stipe	Stip	k1gInSc5	Stip
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Lifting	Lifting	k1gInSc1	Lifting
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c4	v
Been	Been	k1gInSc4	Been
High	High	k1gInSc4	High
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
the	the	k?	the
Way	Way	k1gFnSc1	Way
to	ten	k3xDgNnSc4	ten
Reno	Reno	k1gNnSc4	Reno
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
You	You	k1gFnSc1	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Gonna	Gonen	k2eAgFnSc1d1	Gonna
Be	Be	k1gFnSc1	Be
a	a	k8xC	a
Star	Star	kA	Star
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
43	[number]	k4	43
(	(	kIx(	(
<g/>
Videoklip	videoklip	k1gInSc1	videoklip
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
She	She	k1gFnSc1	She
Just	just	k6eAd1	just
Wants	Wants	k1gInSc4	Wants
To	to	k9	to
Be	Be	k1gFnSc1	Be
<g/>
"	"	kIx"	"
-	-	kIx~	-
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
"	"	kIx"	"
<g/>
Disappear	Disappear	k1gInSc1	Disappear
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
<g />
.	.	kIx.	.
</s>
<s>
11	[number]	k4	11
"	"	kIx"	"
<g/>
Saturn	Saturn	k1gMnSc1	Saturn
return	return	k1gMnSc1	return
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
"	"	kIx"	"
<g/>
Beat	beat	k1gInSc1	beat
a	a	k8xC	a
Drum	Drum	k1gInSc1	Drum
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
21	[number]	k4	21
"	"	kIx"	"
<g/>
Imitation	Imitation	k1gInSc1	Imitation
of	of	k?	of
Life	Life	k1gInSc1	Life
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
"	"	kIx"	"
<g/>
Summer	Summer	k1gInSc1	Summer
Turns	Turnsa	k1gFnPc2	Turnsa
to	ten	k3xDgNnSc4	ten
High	High	k1gMnSc1	High
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
<g/>
31	[number]	k4	31
"	"	kIx"	"
<g/>
Chorus	chorus	k1gInSc1	chorus
and	and	k?	and
the	the	k?	the
Ring	ring	k1gInSc1	ring
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
31	[number]	k4	31
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Take	Take	k1gInSc1	Take
the	the	k?	the
Rain	Rain	k1gInSc1	Rain
<g/>
"	"	kIx"	"
-	-	kIx~	-
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
"	"	kIx"	"
<g/>
Beachball	Beachball	k1gInSc1	Beachball
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
Původní	původní	k2eAgFnSc1d1	původní
verze	verze	k1gFnSc1	verze
z	z	k7c2	z
února	únor	k1gInSc2	únor
2001	[number]	k4	2001
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
z	z	k7c2	z
března	březen	k1gInSc2	březen
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stala	stát	k5eAaPmAgFnS	stát
finální	finální	k2eAgFnSc7d1	finální
verzí	verze	k1gFnSc7	verze
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
kopie	kopie	k1gFnPc1	kopie
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
oficiální	oficiální	k2eAgFnSc7d1	oficiální
verzí	verze	k1gFnSc7	verze
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
verze	verze	k1gFnSc1	verze
Reveal	Reveal	k1gInSc1	Reveal
1.0	[number]	k4	1.0
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
přezdíváno	přezdíván	k2eAgNnSc4d1	přezdíváno
v	v	k7c6	v
několika	několik	k4yIc6	několik
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Dvě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
se	se	k3xPyFc4	se
nedostaly	dostat	k5eNaPmAgFnP	dostat
do	do	k7c2	do
konečného	konečný	k2eAgInSc2d1	konečný
výběru	výběr	k1gInSc2	výběr
<g/>
,	,	kIx,	,
Fascinating	Fascinating	k1gInSc1	Fascinating
a	a	k8xC	a
Free	Free	k1gFnSc1	Free
Form	Form	k1gInSc4	Form
Jazz	jazz	k1gInSc1	jazz
Jam	jam	k1gInSc1	jam
<g/>
.	.	kIx.	.
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
verze	verze	k1gFnSc1	verze
písně	píseň	k1gFnSc2	píseň
Beat	beat	k1gInSc1	beat
a	a	k8xC	a
Drum	Drum	k1gInSc1	Drum
s	s	k7c7	s
názvem	název	k1gInSc7	název
All	All	k1gFnPc2	All
I	i	k8xC	i
Want	Wanta	k1gFnPc2	Wanta
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgFnSc1d2	delší
verze	verze	k1gFnSc1	verze
Imitation	Imitation	k1gInSc1	Imitation
of	of	k?	of
Life	Life	k1gInSc1	Life
<g/>
.	.	kIx.	.
</s>
<s>
All	All	k?	All
The	The	k1gMnSc1	The
Way	Way	k1gMnSc1	Way
To	ten	k3xDgNnSc4	ten
Reno	Reno	k1gNnSc4	Reno
(	(	kIx(	(
<g/>
You	You	k1gMnPc2	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Gonna	Gonen	k2eAgFnSc1d1	Gonna
Be	Be	k1gFnSc1	Be
A	a	k8xC	a
Star	Star	kA	Star
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
odlišný	odlišný	k2eAgInSc4d1	odlišný
závěr	závěr	k1gInSc4	závěr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nazvána	nazván	k2eAgFnSc1d1	nazvána
jen	jen	k9	jen
Reno	Reno	k6eAd1	Reno
<g/>
.	.	kIx.	.
</s>
<s>
Odlišné	odlišný	k2eAgNnSc1d1	odlišné
namixování	namixování	k1gNnSc1	namixování
písní	píseň	k1gFnPc2	píseň
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c4	v
Been	Been	k1gInSc4	Been
High	Higha	k1gFnPc2	Higha
a	a	k8xC	a
She	She	k1gFnPc2	She
Just	just	k6eAd1	just
Wants	Wants	k1gInSc4	Wants
To	to	k9	to
Be	Be	k1gMnPc1	Be
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
neuveřejněných	uveřejněný	k2eNgFnPc2d1	neuveřejněná
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
alternativních	alternativní	k2eAgInPc2d1	alternativní
remixů	remix	k1gInPc2	remix
nebyla	být	k5eNaImAgNnP	být
později	pozdě	k6eAd2	pozdě
vydána	vydat	k5eAaPmNgNnP	vydat
komerčně	komerčně	k6eAd1	komerčně
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
kapela	kapela	k1gFnSc1	kapela
povolila	povolit	k5eAaPmAgFnS	povolit
fanouškovskému	fanouškovský	k2eAgInSc3d1	fanouškovský
serveru	server	k1gInSc2	server
murmurs	murmursa	k1gFnPc2	murmursa
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
uveřejnit	uveřejnit	k5eAaPmF	uveřejnit
píseň	píseň	k1gFnSc1	píseň
Fascinating	Fascinating	k1gInSc4	Fascinating
<g/>
.	.	kIx.	.
</s>
