<p>
<s>
Spartan	Spartan	k?	Spartan
Race	Race	k1gFnSc1	Race
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgInPc2d3	nejpopulárnější
závodů	závod	k1gInPc2	závod
překážkového	překážkový	k2eAgNnSc2d1	překážkové
běhání	běhání	k1gNnSc2	běhání
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kombinace	kombinace	k1gFnSc1	kombinace
běhu	běh	k1gInSc2	běh
a	a	k8xC	a
zdolávání	zdolávání	k1gNnSc2	zdolávání
překážek	překážka	k1gFnPc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Běží	běžet	k5eAaImIp3nS	běžet
se	se	k3xPyFc4	se
za	za	k7c2	za
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
terén	terén	k1gInSc1	terén
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgMnS	založit
jej	on	k3xPp3gNnSc4	on
Joe	Joe	k1gMnSc1	Joe
De	De	k?	De
Sena	sena	k1gFnSc1	sena
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgMnPc2d1	americký
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
Death	Death	k1gInSc4	Death
Race	Race	k1gFnPc2	Race
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
spíše	spíše	k9	spíše
vytrvalostní	vytrvalostní	k2eAgFnSc1d1	vytrvalostní
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skupina	skupina	k1gFnSc1	skupina
účastníků	účastník	k1gMnPc2	účastník
plnila	plnit	k5eAaImAgFnS	plnit
různé	různý	k2eAgInPc4d1	různý
úkoly	úkol	k1gInPc4	úkol
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
nebylo	být	k5eNaImAgNnS	být
90	[number]	k4	90
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
vyřazeno	vyřazen	k2eAgNnSc1d1	vyřazeno
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
náročnou	náročný	k2eAgFnSc4d1	náročná
akci	akce	k1gFnSc4	akce
určenou	určený	k2eAgFnSc4d1	určená
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
nejodolnější	odolný	k2eAgMnPc4d3	nejodolnější
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
něčem	něco	k3yInSc6	něco
podobném	podobný	k2eAgNnSc6d1	podobné
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Joe	Joe	k1gFnSc1	Joe
De	De	k?	De
Sena	sena	k1gFnSc1	sena
připravil	připravit	k5eAaPmAgInS	připravit
závod	závod	k1gInSc1	závod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
již	již	k6eAd1	již
nesl	nést	k5eAaImAgInS	nést
jméno	jméno	k1gNnSc4	jméno
Spartan	Spartan	k?	Spartan
Race	Race	k1gInSc1	Race
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
parametry	parametr	k1gInPc4	parametr
překážkového	překážkový	k2eAgInSc2d1	překážkový
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Death	Death	k1gInSc1	Death
Race	Rac	k1gFnSc2	Rac
se	se	k3xPyFc4	se
ale	ale	k9	ale
zachoval	zachovat	k5eAaPmAgInS	zachovat
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Agoge	Agog	k1gFnSc2	Agog
<g/>
.	.	kIx.	.
</s>
<s>
Spartan	Spartan	k?	Spartan
Race	Race	k1gFnSc1	Race
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pořádá	pořádat	k5eAaImIp3nS	pořádat
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
akcí	akce	k1gFnPc2	akce
–	–	k?	–
klasické	klasický	k2eAgInPc4d1	klasický
překážkové	překážkový	k2eAgInPc4d1	překážkový
závody	závod	k1gInPc4	závod
a	a	k8xC	a
také	také	k9	také
Endurance	Endurance	k1gFnSc1	Endurance
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
vytrvalostní	vytrvalostní	k2eAgFnSc4d1	vytrvalostní
sérii	série	k1gFnSc4	série
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
každém	každý	k3xTgInSc6	každý
úspěšně	úspěšně	k6eAd1	úspěšně
absolvovaném	absolvovaný	k2eAgInSc6d1	absolvovaný
závodu	závod	k1gInSc6	závod
získá	získat	k5eAaPmIp3nS	získat
závodník	závodník	k1gMnSc1	závodník
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
tričko	tričko	k1gNnSc4	tričko
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
i	i	k9	i
finanční	finanční	k2eAgFnSc4d1	finanční
odměnu	odměna	k1gFnSc4	odměna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
začaly	začít	k5eAaPmAgInP	začít
závody	závod	k1gInPc1	závod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
spontánně	spontánně	k6eAd1	spontánně
vznikat	vznikat	k5eAaImF	vznikat
ve	v	k7c6	v
spoustě	spousta	k1gFnSc6	spousta
měst	město	k1gNnPc2	město
tzv.	tzv.	kA	tzv.
Spartan	Spartan	k?	Spartan
Race	Race	k1gInSc1	Race
Training	Training	k1gInSc1	Training
Groups	Groups	k1gInSc1	Groups
(	(	kIx(	(
<g/>
SRTG	SRTG	kA	SRTG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
sportovce	sportovec	k1gMnPc4	sportovec
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
překážkové	překážkový	k2eAgNnSc4d1	překážkové
běhání	běhání	k1gNnSc4	běhání
a	a	k8xC	a
kontinuálně	kontinuálně	k6eAd1	kontinuálně
se	se	k3xPyFc4	se
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
sport	sport	k1gInSc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
i	i	k8xC	i
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
závodů	závod	k1gInPc2	závod
Spartan	Spartan	k?	Spartan
Race	Race	k1gInSc1	Race
==	==	k?	==
</s>
</p>
<p>
<s>
Běží	běžet	k5eAaImIp3nS	běžet
se	se	k3xPyFc4	se
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
vlnách	vlna	k1gFnPc6	vlna
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
vlna	vlna	k1gFnSc1	vlna
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
250	[number]	k4	250
běžců	běžec	k1gMnPc2	běžec
<g/>
)	)	kIx)	)
a	a	k8xC	a
startuje	startovat	k5eAaBmIp3nS	startovat
se	se	k3xPyFc4	se
každých	každý	k3xTgFnPc2	každý
30	[number]	k4	30
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Můžete	moct	k5eAaImIp2nP	moct
si	se	k3xPyFc3	se
vybrat	vybrat	k5eAaPmF	vybrat
z	z	k7c2	z
následujících	následující	k2eAgFnPc2d1	následující
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
elita	elita	k1gFnSc1	elita
muži	muž	k1gMnSc3	muž
</s>
</p>
<p>
<s>
elita	elita	k1gFnSc1	elita
ženy	žena	k1gFnSc2	žena
</s>
</p>
<p>
<s>
srtg	srtg	k1gInSc1	srtg
(	(	kIx(	(
<g/>
tréninkové	tréninkový	k2eAgFnSc2d1	tréninková
skupiny	skupina	k1gFnSc2	skupina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
open	open	k1gInSc4	open
muži	muž	k1gMnPc1	muž
</s>
</p>
<p>
<s>
open	open	k1gInSc1	open
ženy	žena	k1gFnSc2	žena
</s>
</p>
<p>
<s>
kids	kids	k1gInSc1	kids
–	–	k?	–
samostatný	samostatný	k2eAgInSc1d1	samostatný
závod	závod	k1gInSc1	závod
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
kratší	krátký	k2eAgFnSc1d2	kratší
trasa	trasa	k1gFnSc1	trasa
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
charity	charita	k1gFnPc1	charita
–	–	k?	–
samostatný	samostatný	k2eAgInSc4d1	samostatný
závod	závod	k1gInSc4	závod
s	s	k7c7	s
výtěžkem	výtěžek	k1gInSc7	výtěžek
určeným	určený	k2eAgInSc7d1	určený
na	na	k7c4	na
charitativní	charitativní	k2eAgInPc4d1	charitativní
účely	účel	k1gFnPc4	účel
</s>
<s>
Spartan	Spartan	k1gInSc1	Spartan
Race	Rac	k1gFnSc2	Rac
nenabízí	nabízet	k5eNaImIp3nS	nabízet
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
náročnost	náročnost	k1gFnSc4	náročnost
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
možností	možnost	k1gFnPc2	možnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
závodník	závodník	k1gMnSc1	závodník
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
splní	splnit	k5eAaPmIp3nP	splnit
celou	celý	k2eAgFnSc4d1	celá
sérii	série	k1gFnSc4	série
základních	základní	k2eAgInPc2d1	základní
běhů	běh	k1gInPc2	běh
Sprint	sprint	k1gInSc1	sprint
<g/>
,	,	kIx,	,
Super	super	k6eAd1	super
i	i	k9	i
Beast	Beast	k1gFnSc1	Beast
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
Trifecta	Trifect	k1gInSc2	Trifect
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
i	i	k9	i
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
USA	USA	kA	USA
i	i	k8xC	i
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sprint	sprint	k1gInSc4	sprint
===	===	k?	===
</s>
</p>
<p>
<s>
Nejkratší	krátký	k2eAgFnSc1d3	nejkratší
verze	verze	k1gFnSc1	verze
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
měří	měřit	k5eAaImIp3nS	měřit
6	[number]	k4	6
a	a	k8xC	a
více	hodně	k6eAd2	hodně
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
nejméně	málo	k6eAd3	málo
20	[number]	k4	20
překážek	překážka	k1gFnPc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Závodníci	Závodník	k1gMnPc1	Závodník
trať	trať	k1gFnSc4	trať
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
až	až	k8xS	až
2	[number]	k4	2
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
profilu	profil	k1gInSc2	profil
trati	trať	k1gFnSc2	trať
a	a	k8xC	a
kondici	kondice	k1gFnSc4	kondice
závodníka	závodník	k1gMnSc2	závodník
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Super	super	k2eAgNnSc1d1	super
===	===	k?	===
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
má	mít	k5eAaImIp3nS	mít
minimálně	minimálně	k6eAd1	minimálně
13	[number]	k4	13
kilometrů	kilometr	k1gInPc2	kilometr
s	s	k7c7	s
minimálně	minimálně	k6eAd1	minimálně
21	[number]	k4	21
překážkami	překážka	k1gFnPc7	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Časy	čas	k1gInPc1	čas
absolventů	absolvent	k1gMnPc2	absolvent
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
80	[number]	k4	80
minutami	minuta	k1gFnPc7	minuta
a	a	k8xC	a
3	[number]	k4	3
hodinami	hodina	k1gFnPc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
úroveň	úroveň	k1gFnSc1	úroveň
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
modrou	modrý	k2eAgFnSc7d1	modrá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Beast	Beast	k1gInSc4	Beast
===	===	k?	===
</s>
</p>
<p>
<s>
Nejobtížnější	obtížný	k2eAgFnSc1d3	nejobtížnější
varianta	varianta	k1gFnSc1	varianta
závodu	závod	k1gInSc2	závod
ze	z	k7c2	z
základní	základní	k2eAgFnSc2d1	základní
trojice	trojice	k1gFnSc2	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
minimálně	minimálně	k6eAd1	minimálně
20	[number]	k4	20
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
závodníci	závodník	k1gMnPc1	závodník
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
překonávají	překonávat	k5eAaImIp3nP	překonávat
minimálně	minimálně	k6eAd1	minimálně
26	[number]	k4	26
překážek	překážka	k1gFnPc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Časy	čas	k1gInPc1	čas
absolventů	absolvent	k1gMnPc2	absolvent
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
3	[number]	k4	3
až	až	k9	až
5,5	[number]	k4	5,5
hodinami	hodina	k1gFnPc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
tohoto	tento	k3xDgInSc2	tento
závodu	závod	k1gInSc2	závod
je	být	k5eAaImIp3nS	být
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ultra	ultra	k2eAgInSc1d1	ultra
Beast	Beast	k1gInSc1	Beast
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
závod	závod	k1gInSc1	závod
je	být	k5eAaImIp3nS	být
překážkový	překážkový	k2eAgInSc1d1	překážkový
maraton	maraton	k1gInSc1	maraton
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
42	[number]	k4	42
km	km	kA	km
s	s	k7c7	s
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
50	[number]	k4	50
překážkami	překážka	k1gFnPc7	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dělá	dělat	k5eAaImIp3nS	dělat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
běží	běžet	k5eAaImIp3nS	běžet
dvě	dva	k4xCgNnPc4	dva
kola	kolo	k1gNnPc4	kolo
úrovně	úroveň	k1gFnSc2	úroveň
Beast	Beast	k1gFnSc1	Beast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
tato	tento	k3xDgFnSc1	tento
obtížnost	obtížnost	k1gFnSc1	obtížnost
také	také	k9	také
na	na	k7c6	na
jediném	jediný	k2eAgInSc6d1	jediný
originálním	originální	k2eAgInSc6d1	originální
okruhu	okruh	k1gInSc6	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Ultra	ultra	k2eAgInSc4d1	ultra
Beast	Beast	k1gInSc4	Beast
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
úrovní	úroveň	k1gFnPc2	úroveň
časové	časový	k2eAgFnSc2d1	časová
limity	limita	k1gFnSc2	limita
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
závodník	závodník	k1gMnSc1	závodník
nedoběhne	doběhnout	k5eNaPmIp3nS	doběhnout
v	v	k7c6	v
předem	předem	k6eAd1	předem
určeném	určený	k2eAgInSc6d1	určený
čase	čas	k1gInSc6	čas
na	na	k7c4	na
měřené	měřený	k2eAgNnSc4d1	měřené
stanoviště	stanoviště	k1gNnSc4	stanoviště
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
diskvalifikován	diskvalifikovat	k5eAaBmNgMnS	diskvalifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
stanoviště	stanoviště	k1gNnPc1	stanoviště
bývají	bývat	k5eAaImIp3nP	bývat
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgFnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Závodníci	Závodník	k1gMnPc1	Závodník
tuto	tento	k3xDgFnSc4	tento
trať	trať	k1gFnSc4	trať
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
časovém	časový	k2eAgNnSc6d1	časové
rozmezí	rozmezí	k1gNnSc6	rozmezí
mezi	mezi	k7c7	mezi
6-12	[number]	k4	6-12
hodinami	hodina	k1gFnPc7	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hurricane	Hurrican	k1gMnSc5	Hurrican
Heat	Heat	k1gInSc4	Heat
===	===	k?	===
</s>
</p>
<p>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
klasický	klasický	k2eAgInSc4d1	klasický
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
výzvu	výzva	k1gFnSc4	výzva
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
akce	akce	k1gFnSc2	akce
vedeni	vést	k5eAaImNgMnP	vést
tzv.	tzv.	kA	tzv.
Drillmastery	Drillmaster	k1gMnPc4	Drillmaster
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jim	on	k3xPp3gMnPc3	on
dávají	dávat	k5eAaImIp3nP	dávat
různé	různý	k2eAgInPc4d1	různý
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
prověřit	prověřit	k5eAaPmF	prověřit
jejich	jejich	k3xOp3gFnSc4	jejich
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
i	i	k8xC	i
psychickou	psychický	k2eAgFnSc4d1	psychická
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
týmovou	týmový	k2eAgFnSc4d1	týmová
akci	akce	k1gFnSc4	akce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
kladen	klást	k5eAaImNgInS	klást
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
schopnost	schopnost	k1gFnSc4	schopnost
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Hurricane	Hurricanout	k5eAaPmIp3nS	Hurricanout
Heat	Heat	k1gInSc1	Heat
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
úrovně	úroveň	k1gFnPc4	úroveň
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
bude	být	k5eAaImBp3nS	být
minimálně	minimálně	k6eAd1	minimálně
trvat	trvat	k5eAaImF	trvat
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
akce	akce	k1gFnSc1	akce
je	být	k5eAaImIp3nS	být
vojenského	vojenský	k2eAgNnSc2d1	vojenské
ražení	ražení	k1gNnSc2	ražení
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
účastníci	účastník	k1gMnPc1	účastník
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
absolvování	absolvování	k1gNnSc6	absolvování
nedostanou	dostat	k5eNaPmIp3nP	dostat
klasickou	klasický	k2eAgFnSc4d1	klasická
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
známku	známka	k1gFnSc4	známka
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
běžet	běžet	k5eAaImF	běžet
klasický	klasický	k2eAgInSc4d1	klasický
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trať	trať	k1gFnSc1	trať
i	i	k8xC	i
zázemí	zázemí	k1gNnSc1	zázemí
závodu	závod	k1gInSc2	závod
bylo	být	k5eAaImAgNnS	být
poničeno	poničit	k5eAaPmNgNnS	poničit
hurikánem	hurikán	k1gInSc7	hurikán
Irene	Iren	k1gMnSc5	Iren
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pořadatelé	pořadatel	k1gMnPc1	pořadatel
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
závod	závod	k1gInSc4	závod
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
přijelo	přijet	k5eAaPmAgNnS	přijet
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
nadšenců	nadšenec	k1gMnPc2	nadšenec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
se	s	k7c7	s
zrušením	zrušení	k1gNnSc7	zrušení
závodu	závod	k1gInSc2	závod
smířit	smířit	k5eAaPmF	smířit
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
trať	trať	k1gFnSc4	trať
a	a	k8xC	a
závod	závod	k1gInSc4	závod
společně	společně	k6eAd1	společně
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
motivu	motiv	k1gInSc2	motiv
později	pozdě	k6eAd2	pozdě
vzešel	vzejít	k5eAaPmAgInS	vzejít
nápad	nápad	k1gInSc1	nápad
na	na	k7c4	na
Hurricane	Hurrican	k1gMnSc5	Hurrican
Heat	Heatum	k1gNnPc2	Heatum
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
právě	právě	k9	právě
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
akce	akce	k1gFnSc2	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Agoge	Agoge	k1gNnPc3	Agoge
===	===	k?	===
</s>
</p>
<p>
<s>
Nejobtížnější	obtížný	k2eAgFnSc1d3	nejobtížnější
varianta	varianta	k1gFnSc1	varianta
akce	akce	k1gFnSc2	akce
pořádané	pořádaný	k2eAgFnSc2d1	pořádaná
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Spartan	Spartan	k?	Spartan
Race	Rac	k1gFnSc2	Rac
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Death	Death	k1gMnSc1	Death
Race	Rac	k1gFnSc2	Rac
a	a	k8xC	a
předcházel	předcházet	k5eAaImAgInS	předcházet
všem	všecek	k3xTgInPc3	všecek
ostatním	ostatní	k2eAgInPc3d1	ostatní
závodům	závod	k1gInPc3	závod
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
trvá	trvat	k5eAaImIp3nS	trvat
přes	přes	k7c4	přes
60	[number]	k4	60
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
závod	závod	k1gInSc1	závod
dokončí	dokončit	k5eAaPmIp3nS	dokončit
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
%	%	kIx~	%
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Trifecta	Trifecto	k1gNnSc2	Trifecto
===	===	k?	===
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgNnSc1d1	speciální
ocenění	ocenění	k1gNnSc1	ocenění
pro	pro	k7c4	pro
závodníka	závodník	k1gMnSc4	závodník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
kalendářním	kalendářní	k2eAgInSc6d1	kalendářní
roce	rok	k1gInSc6	rok
dokončit	dokončit	k5eAaPmF	dokončit
tři	tři	k4xCgFnPc4	tři
úrovně	úroveň	k1gFnPc4	úroveň
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cíli	cíl	k1gInSc6	cíl
každého	každý	k3xTgInSc2	každý
závodu	závod	k1gInSc2	závod
dostane	dostat	k5eAaPmIp3nS	dostat
závodník	závodník	k1gMnSc1	závodník
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
medailí	medaile	k1gFnSc7	medaile
také	také	k9	také
díl	díl	k1gInSc1	díl
do	do	k7c2	do
trifecty	trifecta	k1gFnSc2	trifecta
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
velké	velký	k2eAgFnSc2d1	velká
kulaté	kulatý	k2eAgFnPc4d1	kulatá
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Závodní	závodní	k2eAgNnPc1d1	závodní
trifecta	trifecto	k1gNnPc1	trifecto
====	====	k?	====
</s>
</p>
<p>
<s>
Ocenění	ocenění	k1gNnSc1	ocenění
pro	pro	k7c4	pro
závodníka	závodník	k1gMnSc4	závodník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
dokončí	dokončit	k5eAaPmIp3nS	dokončit
tratě	trata	k1gFnSc3	trata
Sprint	sprint	k1gInSc4	sprint
<g/>
,	,	kIx,	,
Super	super	k1gInSc4	super
a	a	k8xC	a
Beast	Beast	k1gInSc4	Beast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Endurance	Enduranec	k1gMnSc4	Enduranec
trifecta	trifect	k1gMnSc4	trifect
====	====	k?	====
</s>
</p>
<p>
<s>
Ocenění	ocenění	k1gNnSc1	ocenění
pro	pro	k7c4	pro
závodníka	závodník	k1gMnSc4	závodník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
dokončí	dokončit	k5eAaPmIp3nS	dokončit
vytrvalostní	vytrvalostní	k2eAgFnSc4d1	vytrvalostní
sérii	série	k1gFnSc4	série
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
Hurricane	Hurrican	k1gMnSc5	Hurrican
Heat	Heat	k2eAgInSc1d1	Heat
6	[number]	k4	6
a	a	k8xC	a
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
získat	získat	k5eAaPmF	získat
z	z	k7c2	z
Ultra	ultra	k2eAgFnSc1d1	ultra
Beast	Beast	k1gFnSc1	Beast
<g/>
,	,	kIx,	,
Hurricane	Hurrican	k1gMnSc5	Hurrican
Heat	Heat	k2eAgInSc4d1	Heat
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
Agoge	Agog	k1gFnSc2	Agog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Multitrifecta	Multitrifecto	k1gNnSc2	Multitrifecto
====	====	k?	====
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
závodník	závodník	k1gMnSc1	závodník
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
úspěšně	úspěšně	k6eAd1	úspěšně
dokončit	dokončit	k5eAaPmF	dokončit
více	hodně	k6eAd2	hodně
sérií	série	k1gFnSc7	série
klasických	klasický	k2eAgInPc2d1	klasický
závodů	závod	k1gInPc2	závod
(	(	kIx(	(
<g/>
Sprint	sprint	k1gInSc1	sprint
<g/>
,	,	kIx,	,
Super	super	k1gInSc1	super
<g/>
,	,	kIx,	,
Beast	Beast	k1gInSc1	Beast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
tzv.	tzv.	kA	tzv.
mulitrifectu	mulitrifecta	k1gFnSc4	mulitrifecta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgFnPc4d1	speciální
medaile	medaile	k1gFnPc4	medaile
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
úspěšně	úspěšně	k6eAd1	úspěšně
dokončených	dokončený	k2eAgFnPc2d1	dokončená
sérií	série	k1gFnPc2	série
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
rekord	rekord	k1gInSc1	rekord
21	[number]	k4	21
splněných	splněný	k2eAgFnPc2d1	splněná
sérií	série	k1gFnPc2	série
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Registrace	registrace	k1gFnSc1	registrace
==	==	k?	==
</s>
</p>
<p>
<s>
Registrace	registrace	k1gFnSc1	registrace
na	na	k7c4	na
závody	závod	k1gInPc4	závod
se	se	k3xPyFc4	se
otvírá	otvírat	k5eAaImIp3nS	otvírat
cca	cca	kA	cca
12	[number]	k4	12
až	až	k9	až
20	[number]	k4	20
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
závodech	závod	k1gInPc6	závod
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
komerční	komerční	k2eAgInSc4d1	komerční
závod	závod	k1gInSc4	závod
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zaregistrujete	zaregistrovat	k5eAaPmIp2nP	zaregistrovat
včas	včas	k6eAd1	včas
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
registrace	registrace	k1gFnSc1	registrace
levnější	levný	k2eAgFnSc1d2	levnější
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1100	[number]	k4	1100
Kč	Kč	kA	Kč
až	až	k9	až
1600	[number]	k4	1600
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
o	o	k7c4	o
jaký	jaký	k3yQgInSc4	jaký
typ	typ	k1gInSc4	typ
závodu	závod	k1gInSc2	závod
máte	mít	k5eAaImIp2nP	mít
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
blíže	blízce	k6eAd2	blízce
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
závodu	závod	k1gInSc2	závod
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
dražší	drahý	k2eAgFnSc1d2	dražší
je	být	k5eAaImIp3nS	být
registrace	registrace	k1gFnSc1	registrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Překážky	překážka	k1gFnSc2	překážka
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
překážku	překážka	k1gFnSc4	překážka
nepodaří	podařit	k5eNaPmIp3nS	podařit
překonat	překonat	k5eAaPmF	překonat
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
trest	trest	k1gInSc4	trest
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
30	[number]	k4	30
angličáků	angličák	k1gInPc2	angličák
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
burpees	burpees	k1gInSc1	burpees
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
splnění	splnění	k1gNnSc1	splnění
je	být	k5eAaImIp3nS	být
hlídáno	hlídán	k2eAgNnSc1d1	hlídáno
dobrovolníky	dobrovolník	k1gMnPc7	dobrovolník
<g/>
.	.	kIx.	.
<g/>
Jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
překážky	překážka	k1gFnPc1	překážka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zdolat	zdolat	k5eAaPmF	zdolat
musí	muset	k5eAaImIp3nS	muset
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
následuje	následovat	k5eAaImIp3nS	následovat
diskvalifikace	diskvalifikace	k1gFnSc1	diskvalifikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Atlasovy	Atlasovy	k?	Atlasovy
kameny	kámen	k1gInPc4	kámen
(	(	kIx(	(
<g/>
Atlas	Atlas	k1gMnSc1	Atlas
Cary	car	k1gMnPc7	car
<g/>
)	)	kIx)	)
-	-	kIx~	-
přenášení	přenášení	k1gNnSc4	přenášení
těžkých	těžký	k2eAgInPc2d1	těžký
kamenů	kámen	k1gInPc2	kámen
či	či	k8xC	či
kamenných	kamenný	k2eAgFnPc2d1	kamenná
koulí	koule	k1gFnPc2	koule
</s>
</p>
<p>
<s>
Ostnatý	ostnatý	k2eAgInSc1d1	ostnatý
drát	drát	k1gInSc1	drát
(	(	kIx(	(
<g/>
Barbed	Barbed	k1gInSc1	Barbed
Wire	Wire	k1gNnSc1	Wire
Crawl	Crawl	k1gInSc1	Crawl
<g/>
)	)	kIx)	)
-	-	kIx~	-
plazení	plazení	k1gNnSc1	plazení
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
ostnatým	ostnatý	k2eAgInSc7d1	ostnatý
drátem	drát	k1gInSc7	drát
</s>
</p>
<p>
<s>
Kyblíky	Kyblíky	k?	Kyblíky
(	(	kIx(	(
<g/>
Bucket	Bucket	k1gInSc1	Bucket
Bridge	Bridg	k1gInSc2	Bridg
<g/>
)	)	kIx)	)
-	-	kIx~	-
naplnit	naplnit	k5eAaPmF	naplnit
kýbl	kýbl	k?	kýbl
štěrkem	štěrk	k1gInSc7	štěrk
a	a	k8xC	a
vylézt	vylézt	k5eAaPmF	vylézt
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
do	do	k7c2	do
kopce	kopec	k1gInSc2	kopec
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
můžete	moct	k5eAaImIp2nP	moct
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
nošením	nošení	k1gNnSc7	nošení
pytlů	pytel	k1gInPc2	pytel
plných	plný	k2eAgInPc2d1	plný
pískuHerkulův	pískuHerkulův	k2eAgInSc4d1	pískuHerkulův
výtah	výtah	k1gInSc4	výtah
(	(	kIx(	(
<g/>
Hercules	Hercules	k1gMnSc1	Hercules
Hoist	Hoist	k1gMnSc1	Hoist
<g/>
)	)	kIx)	)
-	-	kIx~	-
pytel	pytel	k1gInSc1	pytel
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
kladka	kladka	k1gFnSc1	kladka
a	a	k8xC	a
lanoOpičí	lanoOpičit	k5eAaPmIp3nS	lanoOpičit
síť	síť	k1gFnSc4	síť
(	(	kIx(	(
<g/>
Monkey	Monke	k2eAgFnPc4d1	Monke
Net	Net	k1gFnPc4	Net
<g/>
)	)	kIx)	)
-	-	kIx~	-
přelézání	přelézání	k1gNnSc1	přelézání
či	či	k8xC	či
ručkování	ručkování	k1gNnSc1	ručkování
po	po	k7c6	po
provazové	provazový	k2eAgFnSc6d1	provazová
síti	síť	k1gFnSc6	síť
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
a	a	k8xC	a
skrz	skrz	k7c4	skrz
(	(	kIx(	(
<g/>
Over	Over	k1gMnSc1	Over
Under	Under	k1gMnSc1	Under
Through	Through	k1gMnSc1	Through
<g/>
)	)	kIx)	)
-	-	kIx~	-
sada	sada	k1gFnSc1	sada
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
překážek	překážka	k1gFnPc2	překážka
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
proskočíte	proskočit	k5eAaPmIp2nP	proskočit
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc7	druhý
překonáte	překonat	k5eAaPmIp2nP	překonat
vrchem	vrchem	k6eAd1	vrchem
a	a	k8xC	a
třetí	třetí	k4xOgNnSc4	třetí
spodemŠplhání	spodemŠplhání	k1gNnSc4	spodemŠplhání
po	po	k7c6	po
provaze	provaz	k1gInSc6	provaz
(	(	kIx(	(
<g/>
Rope	Rope	k1gFnSc1	Rope
Climb	Climb	k1gMnSc1	Climb
<g/>
)	)	kIx)	)
-	-	kIx~	-
klasické	klasický	k2eAgNnSc4d1	klasické
šplhání	šplhání	k1gNnSc4	šplhání
po	po	k7c6	po
provaze	provaz	k1gInSc6	provaz
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
je	být	k5eAaImIp3nS	být
jáma	jáma	k1gFnSc1	jáma
plná	plný	k2eAgFnSc1d1	plná
vodyHod	vodyHod	k1gInSc4	vodyHod
oštěpem	oštěp	k1gInSc7	oštěp
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Spear	Spear	k1gMnSc1	Spear
Throw	Throw	k1gMnSc1	Throw
<g/>
)	)	kIx)	)
–	–	k?	–
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
,	,	kIx,	,
<g/>
aby	aby	kYmCp3nS	aby
oštěp	oštěp	k1gInSc4	oštěp
zůstal	zůstat	k5eAaPmAgInS	zůstat
zabodnutý	zabodnutý	k2eAgMnSc1d1	zabodnutý
ve	v	k7c6	v
slaměném	slaměný	k2eAgNnSc6d1	slaměné
terčiPřevracení	terčiPřevracení	k1gNnSc6	terčiPřevracení
pneumatiky	pneumatika	k1gFnSc2	pneumatika
(	(	kIx(	(
<g/>
Tire	Tire	k1gFnSc1	Tire
Drag	Drag	k1gMnSc1	Drag
<g/>
)	)	kIx)	)
-	-	kIx~	-
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
pneumatiku	pneumatika	k1gFnSc4	pneumatika
dvakrát	dvakrát	k6eAd1	dvakrát
převrátitHorolezecká	převrátitHorolezecký	k2eAgFnSc1d1	převrátitHorolezecký
stěna	stěna	k1gFnSc1	stěna
(	(	kIx(	(
<g/>
Traverse	travers	k1gInSc5	travers
Wall	Wall	k1gInSc4	Wall
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
plno	plno	k6eAd1	plno
bláta	bláto	k1gNnPc4	bláto
a	a	k8xC	a
úchyty	úchyt	k1gInPc4	úchyt
kloužou	klouzat	k5eAaImIp3nP	klouzat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
<g />
.	.	kIx.	.
</s>
<s>
mít	mít	k5eAaImF	mít
silné	silný	k2eAgNnSc4d1	silné
prstyPřelezení	prstyPřelezení	k1gNnSc4	prstyPřelezení
stěny	stěna	k1gFnSc2	stěna
(	(	kIx(	(
<g/>
Wall	Wall	k1gMnSc1	Wall
Jump	Jump	k1gMnSc1	Jump
<g/>
)	)	kIx)	)
-	-	kIx~	-
vysoká	vysoký	k2eAgFnSc1d1	vysoká
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
stěna	stěna	k1gFnSc1	stěna
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc1	její
výška	výška	k1gFnSc1	výška
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyskočit	vyskočit	k5eAaPmF	vyskočit
<g/>
,	,	kIx,	,
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
se	se	k3xPyFc4	se
a	a	k8xC	a
přeskočit	přeskočit	k5eAaPmF	přeskočit
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranuPodplavání	stranuPodplavánět	k5eAaImIp3nS	stranuPodplavánět
stěny	stěna	k1gFnPc4	stěna
(	(	kIx(	(
<g/>
Dunk	Dunk	k1gMnSc1	Dunk
Wall	Wall	k1gMnSc1	Wall
<g/>
)	)	kIx)	)
-	-	kIx~	-
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
podplavat	podplavat	k5eAaPmF	podplavat
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
stěnu	stěna	k1gFnSc4	stěna
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
ledová	ledový	k2eAgFnSc1d1	ledová
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
barvou	barva	k1gFnSc7	barva
připomíná	připomínat	k5eAaImIp3nS	připomínat
řeku	řeka	k1gFnSc4	řeka
GanguTest	GanguTest	k1gFnSc4	GanguTest
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
Memory	Memora	k1gFnSc2	Memora
Test	test	k1gInSc1	test
<g/>
)	)	kIx)	)
-	-	kIx~	-
musíte	muset	k5eAaImIp2nP	muset
si	se	k3xPyFc3	se
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
skupinu	skupina	k1gFnSc4	skupina
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
myslet	myslet	k5eAaImF	myslet
na	na	k7c4	na
běh	běh	k1gInSc4	běh
<g/>
,	,	kIx,	,
přelézání	přelézání	k1gNnSc4	přelézání
<g/>
,	,	kIx,	,
podlézání	podlézání	k1gNnSc4	podlézání
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc4d1	ostatní
fyzické	fyzický	k2eAgInPc4d1	fyzický
aspekty	aspekt	k1gInPc4	aspekt
závoduKladina	závoduKladina	k1gFnSc1	závoduKladina
-	-	kIx~	-
balanční	balanční	k2eAgNnSc1d1	balanční
překážkaRučkování	překážkaRučkování	k1gNnSc1	překážkaRučkování
(	(	kIx(	(
<g/>
Monkey	Monkey	k1gInPc4	Monkey
Bar	bar	k1gInSc1	bar
<g/>
)	)	kIx)	)
-	-	kIx~	-
úsek	úsek	k1gInSc1	úsek
lešení	lešení	k1gNnSc2	lešení
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
přeručkovat	přeručkovat	k5eAaPmF	přeručkovat
<g/>
,	,	kIx,	,
minulý	minulý	k2eAgInSc4d1	minulý
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
varianta	varianta	k1gFnSc1	varianta
tricepsové	tricepsové	k2eAgNnSc2d1	tricepsové
ručkování	ručkování	k1gNnSc2	ručkování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ručkujete	ručkovat	k5eAaImIp2nP	ručkovat
po	po	k7c6	po
závěsechSkluzavka	závěsechSkluzavka	k1gFnSc1	závěsechSkluzavka
(	(	kIx(	(
<g/>
Slide	slide	k1gInSc1	slide
Wall	Wall	k1gInSc1	Wall
<g/>
)	)	kIx)	)
-	-	kIx~	-
kopec	kopec	k1gInSc1	kopec
<g/>
,	,	kIx,	,
spousta	spousta	k1gFnSc1	spousta
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
klouzačka	klouzačka	k1gFnSc1	klouzačka
z	z	k7c2	z
pevného	pevný	k2eAgInSc2d1	pevný
igelitu	igelit	k1gInSc2	igelit
<g/>
,	,	kIx,	,
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
sjet	sjet	k5eAaPmF	sjet
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
spadnout	spadnout	k5eAaPmF	spadnout
do	do	k7c2	do
jámy	jáma	k1gFnSc2	jáma
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
pryč	pryč	k6eAd1	pryč
</s>
</p>
<p>
<s>
Šikmá	šikmý	k2eAgFnSc1d1	šikmá
stěna	stěna	k1gFnSc1	stěna
s	s	k7c7	s
provazem	provaz	k1gInSc7	provaz
–	–	k?	–
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
šplháte	šplhat	k5eAaImIp2nP	šplhat
po	po	k7c6	po
šikmé	šikmý	k2eAgFnSc6d1	šikmá
stěně	stěna	k1gFnSc6	stěna
a	a	k8xC	a
následně	následně	k6eAd1	následně
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
slézáte	slézat	k5eAaImIp2nP	slézat
jako	jako	k9	jako
ze	z	k7c2	z
žebříkuPřeskok	žebříkuPřeskok	k1gInSc1	žebříkuPřeskok
ohně	oheň	k1gInSc2	oheň
(	(	kIx(	(
<g/>
Fire	Fire	k1gFnSc1	Fire
Jump	Jump	k1gMnSc1	Jump
<g/>
)	)	kIx)	)
-	-	kIx~	-
poslední	poslední	k2eAgFnSc1d1	poslední
překážka	překážka	k1gFnSc1	překážka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
==	==	k?	==
Mistrovství	mistrovství	k1gNnSc1	mistrovství
ve	v	k7c6	v
Spartan	Spartan	k?	Spartan
Race	Race	k1gFnPc6	Race
==	==	k?	==
</s>
</p>
<p>
<s>
Vždy	vždy	k6eAd1	vždy
začátkem	začátkem	k7c2	začátkem
podzimu	podzim	k1gInSc2	podzim
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
světový	světový	k2eAgInSc1d1	světový
a	a	k8xC	a
evropský	evropský	k2eAgInSc1d1	evropský
šampionát	šampionát	k1gInSc1	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Závodníci	Závodník	k1gMnPc1	Závodník
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
světa	svět	k1gInSc2	svět
zde	zde	k6eAd1	zde
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
speciálně	speciálně	k6eAd1	speciálně
upravenou	upravený	k2eAgFnSc4d1	upravená
trať	trať	k1gFnSc4	trať
úrovně	úroveň	k1gFnSc2	úroveň
Beast	Beast	k1gInSc1	Beast
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
závodníků	závodník	k1gMnPc2	závodník
zde	zde	k6eAd1	zde
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
bývalá	bývalý	k2eAgFnSc1d1	bývalá
běžecká	běžecký	k2eAgFnSc1d1	běžecká
lyžařka	lyžařka	k1gFnSc1	lyžařka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Kocumová	Kocumová	k1gFnSc1	Kocumová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
Světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2015	[number]	k4	2015
a	a	k8xC	a
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
doběhla	doběhnout	k5eAaPmAgFnS	doběhnout
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
získala	získat	k5eAaPmAgFnS	získat
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2016	[number]	k4	2016
a	a	k8xC	a
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zázemí	zázemí	k1gNnSc2	zázemí
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
zázemí	zázemí	k1gNnSc6	zázemí
na	na	k7c6	na
závodě	závod	k1gInSc6	závod
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
postaráno	postarán	k2eAgNnSc1d1	postaráno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
připravena	připravit	k5eAaPmNgFnS	připravit
úschovna	úschovna	k1gFnSc1	úschovna
osobních	osobní	k2eAgFnPc2d1	osobní
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
kiosky	kiosek	k1gInPc1	kiosek
s	s	k7c7	s
občerstvením	občerstvení	k1gNnSc7	občerstvení
<g/>
,	,	kIx,	,
WC	WC	kA	WC
<g/>
,	,	kIx,	,
spartánské	spartánský	k2eAgFnPc1d1	spartánská
(	(	kIx(	(
<g/>
ledové	ledový	k2eAgFnPc1d1	ledová
<g/>
)	)	kIx)	)
sprchy	sprcha	k1gFnPc1	sprcha
<g/>
,	,	kIx,	,
stanové	stanový	k2eAgNnSc1d1	stanové
městečko	městečko	k1gNnSc1	městečko
pro	pro	k7c4	pro
nocležníky	nocležník	k1gMnPc4	nocležník
<g/>
,	,	kIx,	,
nákupní	nákupní	k2eAgFnSc1d1	nákupní
zóna	zóna	k1gFnSc1	zóna
(	(	kIx(	(
<g/>
sportovní	sportovní	k2eAgNnSc1d1	sportovní
vybavení	vybavení	k1gNnSc1	vybavení
<g/>
,	,	kIx,	,
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
masáže	masáž	k1gFnPc4	masáž
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
závodu	závod	k1gInSc2	závod
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Organizační	organizační	k2eAgInSc1d1	organizační
tým	tým	k1gInSc1	tým
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
poznáte	poznat	k5eAaPmIp2nP	poznat
podle	podle	k7c2	podle
červených	červený	k2eAgNnPc2d1	červené
triček	tričko	k1gNnPc2	tričko
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
nich	on	k3xPp3gFnPc2	on
by	by	kYmCp3nS	by
závod	závod	k1gInSc1	závod
nemohl	moct	k5eNaImAgInS	moct
fungovat	fungovat	k5eAaImF	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Starají	starat	k5eAaImIp3nP	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
dodržovaní	dodržovaný	k2eAgMnPc1d1	dodržovaný
pravidel	pravidlo	k1gNnPc2	pravidlo
i	i	k9	i
dbají	dbát	k5eAaImIp3nP	dbát
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
závodníků	závodník	k1gMnPc2	závodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Spartan	Spartan	k?	Spartan
race	race	k1gInSc1	race
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
