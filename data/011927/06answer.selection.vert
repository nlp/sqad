<s>
Typickým	typický	k2eAgNnSc7d1	typické
životním	životní	k2eAgNnSc7d1	životní
prostředím	prostředí	k1gNnSc7	prostředí
rysa	rys	k1gMnSc2	rys
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
přírodních	přírodní	k2eAgFnPc6d1	přírodní
podmínkách	podmínka	k1gFnPc6	podmínka
oblasti	oblast	k1gFnSc2	oblast
smíšených	smíšený	k2eAgInPc2d1	smíšený
a	a	k8xC	a
jehličnatých	jehličnatý	k2eAgInPc2d1	jehličnatý
lesů	les	k1gInPc2	les
středních	střední	k2eAgFnPc2d1	střední
a	a	k8xC	a
vyšších	vysoký	k2eAgFnPc2d2	vyšší
poloh	poloha	k1gFnPc2	poloha
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
podrostem	podrost	k1gInSc7	podrost
a	a	k8xC	a
skalními	skalní	k2eAgInPc7d1	skalní
útvary	útvar	k1gInPc7	útvar
<g/>
.	.	kIx.	.
</s>
