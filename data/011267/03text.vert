<p>
<s>
Obec	obec	k1gFnSc1	obec
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Bohaslawitz	Bohaslawitz	k1gInSc1	Bohaslawitz
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
a	a	k8xC	a
železniční	železniční	k2eAgFnPc1d1	železniční
stanice	stanice	k1gFnPc1	stanice
nesou	nést	k5eAaImIp3nP	nést
název	název	k1gInSc4	název
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Náchod	Náchod	k1gInSc1	Náchod
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
Bohuslavicích	Bohuslavice	k1gFnPc6	Bohuslavice
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c4	v
nadaci	nadace	k1gFnSc4	nadace
kostela	kostel	k1gInSc2	kostel
dobrušského	dobrušský	k2eAgInSc2d1	dobrušský
z	z	k7c2	z
r.	r.	kA	r.
1361	[number]	k4	1361
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1371	[number]	k4	1371
držel	držet	k5eAaImAgMnS	držet
Bohuslavice	Bohuslavice	k1gFnPc4	Bohuslavice
Sezima	Sezimum	k1gNnSc2	Sezimum
z	z	k7c2	z
Dobrušky	Dobruška	k1gFnSc2	Dobruška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1409	[number]	k4	1409
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
pana	pan	k1gMnSc2	pan
Vítka	Vítek	k1gMnSc2	Vítek
z	z	k7c2	z
Černčic	Černčice	k1gFnPc2	Černčice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1503	[number]	k4	1503
se	se	k3xPyFc4	se
za	za	k7c4	za
Jana	Jan	k1gMnSc4	Jan
Černčického	Černčický	k2eAgMnSc4d1	Černčický
z	z	k7c2	z
Kácova	Kácov	k1gInSc2	Kácov
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
dostaly	dostat	k5eAaPmAgFnP	dostat
k	k	k7c3	k
Novému	nový	k2eAgNnSc3d1	nové
Městu	město	k1gNnSc3	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1638	[number]	k4	1638
byly	být	k5eAaImAgInP	být
darovány	darován	k2eAgInPc1d1	darován
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
část	část	k1gFnSc1	část
zboží	zboží	k1gNnSc2	zboží
novoměstského	novoměstský	k2eAgNnSc2d1	Novoměstské
<g/>
"	"	kIx"	"
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Valtrovi	Valtrův	k2eAgMnPc1d1	Valtrův
z	z	k7c2	z
Leslie	Leslie	k1gFnSc2	Leslie
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Bohuslavice	Bohuslavice	k1gFnPc4	Bohuslavice
vedla	vést	k5eAaImAgFnS	vést
starobylá	starobylý	k2eAgFnSc1d1	starobylá
stezka	stezka	k1gFnSc1	stezka
zemská	zemský	k2eAgFnSc1d1	zemská
od	od	k7c2	od
Krčína	Krčín	k1gInSc2	Krčín
k	k	k7c3	k
Opočnu	Opočno	k1gNnSc3	Opočno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
okresu	okres	k1gInSc2	okres
Náchod	Náchod	k1gInSc1	Náchod
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
geograficky	geograficky	k6eAd1	geograficky
a	a	k8xC	a
turisticky	turisticky	k6eAd1	turisticky
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
Kladského	kladský	k2eAgNnSc2d1	Kladské
pomezí	pomezí	k1gNnSc2	pomezí
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Orlických	orlický	k2eAgMnPc2d1	orlický
hor.	hor.	k?	hor.
Jejich	jejich	k3xOp3gFnSc7	jejich
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
činí	činit	k5eAaImIp3nS	činit
284	[number]	k4	284
<g/>
m.	m.	k?	m.
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnPc1	okolí
obce	obec	k1gFnSc2	obec
lemují	lemovat	k5eAaImIp3nP	lemovat
lesy	les	k1gInPc1	les
Tuří	tuří	k2eAgInPc1d1	tuří
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
rybníkem	rybník	k1gInSc7	rybník
<g/>
,	,	kIx,	,
Horka	horka	k1gFnSc1	horka
a	a	k8xC	a
Halín	Halín	k1gInSc1	Halín
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
výskytem	výskyt	k1gInSc7	výskyt
teplomilné	teplomilný	k2eAgFnSc2d1	teplomilná
hajní	hajní	k2eAgFnSc2d1	hajní
květeny	květena	k1gFnSc2	květena
<g/>
.	.	kIx.	.
</s>
<s>
Ojedinělou	ojedinělý	k2eAgFnSc7d1	ojedinělá
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
Čechách	Čechy	k1gFnPc6	Čechy
jsou	být	k5eAaImIp3nP	být
Zbytka	Zbytka	k1gMnSc1	Zbytka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roste	růst	k5eAaImIp3nS	růst
vzácná	vzácný	k2eAgFnSc1d1	vzácná
chráněná	chráněný	k2eAgFnSc1d1	chráněná
květena	květena	k1gFnSc1	květena
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
i	i	k9	i
známé	známý	k2eAgInPc4d1	známý
a	a	k8xC	a
pověstmi	pověst	k1gFnPc7	pověst
opředené	opředený	k2eAgNnSc1d1	opředené
bezedné	bezedný	k2eAgNnSc1d1	bezedné
jezírko	jezírko	k1gNnSc1	jezírko
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
románu	román	k1gInSc6	román
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
"	"	kIx"	"
Temno	temno	k6eAd1	temno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
dávající	dávající	k2eAgInPc1d1	dávající
cca	cca	kA	cca
100	[number]	k4	100
litrů	litr	k1gInPc2	litr
<g/>
/	/	kIx~	/
<g/>
sec	sec	k1gInPc1	sec
křišťálové	křišťálový	k2eAgFnSc2d1	Křišťálová
vody	voda	k1gFnSc2	voda
se	s	k7c7	s
stálou	stálý	k2eAgFnSc7d1	stálá
teplotou	teplota	k1gFnSc7	teplota
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
vodovodu	vodovod	k1gInSc2	vodovod
pro	pro	k7c4	pro
Hradec	Hradec	k1gInSc4	Hradec
Králové	Králová	k1gFnSc2	Králová
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bohuslavicemi	Bohuslavice	k1gFnPc7	Bohuslavice
protéká	protékat	k5eAaImIp3nS	protékat
Bohuslavický	Bohuslavický	k2eAgInSc4d1	Bohuslavický
potok	potok	k1gInSc4	potok
s	s	k7c7	s
kaskádou	kaskáda	k1gFnSc7	kaskáda
rybníků	rybník	k1gInPc2	rybník
–	–	k?	–
Šuryt	Šuryt	k1gInSc1	Šuryt
<g/>
,	,	kIx,	,
Štíp	štíp	k1gInSc1	štíp
<g/>
,	,	kIx,	,
Tláskal	Tláskal	k1gMnSc1	Tláskal
<g/>
,	,	kIx,	,
Věžiště	Věžiště	k1gNnSc1	Věžiště
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
hospodou	hospodou	k?	hospodou
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
pastouškou	pastouška	k1gFnSc7	pastouška
a	a	k8xC	a
Dolejší	Dolejší	k2eAgInSc1d1	Dolejší
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgInSc1d1	připomínající
vznikem	vznik	k1gInSc7	vznik
přelom	přelom	k1gInSc4	přelom
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
chovali	chovat	k5eAaImAgMnP	chovat
kapři	kapr	k1gMnPc1	kapr
a	a	k8xC	a
naše	náš	k3xOp1gFnSc1	náš
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
ničem	nic	k3yNnSc6	nic
nezaostávala	zaostávat	k5eNaImAgFnS	zaostávat
za	za	k7c7	za
slávou	sláva	k1gFnSc7	sláva
rybníkářství	rybníkářství	k1gNnPc2	rybníkářství
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
provozuje	provozovat	k5eAaImIp3nS	provozovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Občerstvení	občerstvení	k1gNnSc1	občerstvení
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
pohostinstvích	pohostinství	k1gNnPc6	pohostinství
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Nakoupit	nakoupit	k5eAaPmF	nakoupit
lze	lze	k6eAd1	lze
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
prodejnách	prodejna	k1gFnPc6	prodejna
<g/>
,	,	kIx,	,
v	v	k7c6	v
Konzumu	konzum	k1gInSc6	konzum
<g/>
,	,	kIx,	,
v	v	k7c6	v
prodejně	prodejna	k1gFnSc6	prodejna
ZEPA	ZEPA	kA	ZEPA
a	a	k8xC	a
v	v	k7c6	v
krámku	krámek	k1gInSc6	krámek
U	u	k7c2	u
Krupičků	Krupička	k1gMnPc2	Krupička
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
buduje	budovat	k5eAaImIp3nS	budovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
tlakovou	tlakový	k2eAgFnSc4d1	tlaková
kanalizaci	kanalizace	k1gFnSc4	kanalizace
<g/>
,	,	kIx,	,
na	na	k7c6	na
lukách	luka	k1gNnPc6	luka
pod	pod	k7c7	pod
obcí	obec	k1gFnSc7	obec
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
čistírna	čistírna	k1gFnSc1	čistírna
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
hasičskou	hasičský	k2eAgFnSc7d1	hasičská
zbrojnicí	zbrojnice	k1gFnSc7	zbrojnice
a	a	k8xC	a
Orelnou	Orelna	k1gFnSc7	Orelna
prodejna	prodejna	k1gFnSc1	prodejna
potravin	potravina	k1gFnPc2	potravina
Konzum	konzum	k1gInSc1	konzum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecní	obecní	k2eAgFnSc1d1	obecní
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
sídelní	sídelní	k2eAgFnPc4d1	sídelní
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
:	:	kIx,	:
Bohuslavice	Bohuslavice	k1gFnPc4	Bohuslavice
a	a	k8xC	a
Zbytka	Zbytka	k1gFnSc1	Zbytka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Ochranitelky	ochranitelka	k1gFnSc2	ochranitelka
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Zbytka	Zbytek	k1gMnSc2	Zbytek
–	–	k?	–
lužní	lužní	k2eAgInSc4d1	lužní
les	les	k1gInSc4	les
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Bohuslavicích	Bohuslavice	k1gFnPc6	Bohuslavice
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Josef	Josef	k1gMnSc1	Josef
Drahorád	Drahorád	k1gMnSc1	Drahorád
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
<g/>
–	–	k?	–
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
Paka	Paka	k1gFnSc1	Paka
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Pieszyce	Pieszyce	k1gFnSc1	Pieszyce
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Bohuslavice	Bohuslavice	k1gFnPc4	Bohuslavice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
