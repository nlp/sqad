<s>
Obec	obec	k1gFnSc1	obec
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Bohaslawitz	Bohaslawitz	k1gInSc1	Bohaslawitz
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
a	a	k8xC	a
železniční	železniční	k2eAgFnPc1d1	železniční
stanice	stanice	k1gFnPc1	stanice
nesou	nést	k5eAaImIp3nP	nést
název	název	k1gInSc4	název
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Náchod	Náchod	k1gInSc1	Náchod
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
