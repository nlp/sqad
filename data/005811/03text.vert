<s>
PhDr.	PhDr.	kA	PhDr.
Lucie	Lucie	k1gFnSc1	Lucie
Plekancová	Plekancový	k2eAgFnSc1d1	Plekancová
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Lucie	Lucie	k1gFnSc1	Lucie
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1980	[number]	k4	1980
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
dabérka	dabérka	k1gFnSc1	dabérka
a	a	k8xC	a
textařka	textařka	k1gFnSc1	textařka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vdaná	vdaný	k2eAgFnSc1d1	vdaná
za	za	k7c4	za
českého	český	k2eAgMnSc4d1	český
hokejistu	hokejista	k1gMnSc4	hokejista
Tomáše	Tomáš	k1gMnSc4	Tomáš
Plekance	Plekanec	k1gMnSc4	Plekanec
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
má	mít	k5eAaImIp3nS	mít
syny	syn	k1gMnPc4	syn
Matyáše	Matyáš	k1gMnSc4	Matyáš
a	a	k8xC	a
Adama	Adam	k1gMnSc4	Adam
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
muzikantské	muzikantský	k2eAgFnSc2d1	muzikantská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
otec	otec	k1gMnSc1	otec
Jiří	Jiří	k1gMnSc1	Jiří
Vondráček	Vondráček	k1gMnSc1	Vondráček
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Hana	Hana	k1gFnSc1	Hana
Sorrosová	Sorrosový	k2eAgFnSc1d1	Sorrosový
je	být	k5eAaImIp3nS	být
textařka	textařka	k1gFnSc1	textařka
<g/>
,	,	kIx,	,
teta	teta	k1gFnSc1	teta
Helena	Helena	k1gFnSc1	Helena
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
české	český	k2eAgFnPc1d1	Česká
pop	pop	k1gInSc4	pop
music	musice	k1gFnPc2	musice
<g/>
.	.	kIx.	.
</s>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
coby	coby	k?	coby
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
vydala	vydat	k5eAaPmAgFnS	vydat
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
deset	deset	k4xCc4	deset
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
nebo	nebo	k8xC	nebo
i	i	k9	i
Platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
jejich	jejich	k3xOp3gInSc4	jejich
prodej	prodej	k1gInSc4	prodej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
trojici	trojice	k1gFnSc6	trojice
interpretek	interpretka	k1gFnPc2	interpretka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
letech	let	k1gInPc6	let
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
hudebně-dramatický	hudebněramatický	k2eAgInSc4d1	hudebně-dramatický
obor	obor	k1gInSc4	obor
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
zakončila	zakončit	k5eAaPmAgFnS	zakončit
i	i	k9	i
studium	studium	k1gNnSc4	studium
kulturologie	kulturologie	k1gFnSc2	kulturologie
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
UK	UK	kA	UK
diplomovou	diplomový	k2eAgFnSc7d1	Diplomová
prací	práce	k1gFnSc7	práce
"	"	kIx"	"
<g/>
Tradice	tradice	k1gFnSc1	tradice
české	český	k2eAgFnSc2d1	Česká
pohádky	pohádka	k1gFnSc2	pohádka
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
kinematografii	kinematografie	k1gFnSc6	kinematografie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
doktor	doktor	k1gMnSc1	doktor
filozofie	filozofie	k1gFnSc1	filozofie
získala	získat	k5eAaPmAgFnS	získat
obhajobou	obhajoba	k1gFnSc7	obhajoba
práce	práce	k1gFnSc2	práce
"	"	kIx"	"
<g/>
Pohádky	pohádka	k1gFnSc2	pohádka
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
kinematografii	kinematografie	k1gFnSc6	kinematografie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
PhDr.	PhDr.	kA	PhDr.
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Kláry	Klára	k1gFnSc2	Klára
nad	nad	k7c7	nad
vinicí	vinice	k1gFnSc7	vinice
v	v	k7c6	v
Troji	troje	k4xRgMnPc1	troje
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Botanické	botanický	k2eAgFnSc2d1	botanická
zahrady	zahrada	k1gFnSc2	zahrada
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
hokejistu	hokejista	k1gMnSc4	hokejista
Tomáše	Tomáš	k1gMnSc4	Tomáš
Plekance	Plekanec	k1gMnSc4	Plekanec
<g/>
.	.	kIx.	.
a	a	k8xC	a
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
své	svůj	k3xOyFgNnSc4	svůj
příjmení	příjmení	k1gNnSc4	příjmení
dle	dle	k7c2	dle
příjmení	příjmení	k1gNnSc2	příjmení
manžela	manžel	k1gMnSc2	manžel
na	na	k7c4	na
Lucie	Lucie	k1gFnPc4	Lucie
Plekancová	Plekancový	k2eAgFnSc1d1	Plekancová
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
se	se	k3xPyFc4	se
novomanželům	novomanžel	k1gMnPc3	novomanžel
v	v	k7c6	v
kanadském	kanadský	k2eAgInSc6d1	kanadský
Montrealu	Montreal	k1gInSc6	Montreal
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Adam	Adam	k1gMnSc1	Adam
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
před	před	k7c7	před
kamerou	kamera	k1gFnSc7	kamera
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Ráži	Ráž	k1gMnPc7	Ráž
Území	území	k1gNnSc2	území
bílých	bílý	k2eAgMnPc2d1	bílý
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Václava	Václav	k1gMnSc2	Václav
Vorlíčka	Vorlíček	k1gMnSc2	Vorlíček
Arabela	Arabel	k1gMnSc2	Arabel
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
aneb	aneb	k?	aneb
Rumburak	Rumburak	k1gInSc1	Rumburak
králem	král	k1gMnSc7	král
Říše	říš	k1gFnSc2	říš
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
moderátorkou	moderátorka	k1gFnSc7	moderátorka
pořadu	pořad	k1gInSc2	pořad
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc1	mládež
Marmeláda	marmeláda	k1gFnSc1	marmeláda
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
album	album	k1gNnSc4	album
Marmeláda	marmeláda	k1gFnSc1	marmeláda
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
filmech	film	k1gInPc6	film
režiséra	režisér	k1gMnSc2	režisér
Juraje	Juraj	k1gInSc2	Juraj
Jakubiska	Jakubiska	k1gFnSc1	Jakubiska
–	–	k?	–
Nejasná	jasný	k2eNgFnSc1d1	nejasná
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Bathory	Bathora	k1gFnSc2	Bathora
(	(	kIx(	(
<g/>
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
postava	postava	k1gFnSc1	postava
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Lucie	Lucie	k1gFnPc4	Lucie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Post	post	k1gInSc1	post
coitum	coitum	k1gNnSc4	coitum
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Labyrint	labyrint	k1gInSc1	labyrint
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
filmem	film	k1gInSc7	film
jsou	být	k5eAaImIp3nP	být
zatím	zatím	k6eAd1	zatím
Babovřesky	Babovřeska	k1gFnPc1	Babovřeska
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnPc1	komedie
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Trošky	troška	k1gFnSc2	troška
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obsazována	obsazován	k2eAgFnSc1d1	obsazována
i	i	k9	i
do	do	k7c2	do
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
komedii	komedie	k1gFnSc4	komedie
Last	Lasta	k1gFnPc2	Lasta
Holidays	Holidaysa	k1gFnPc2	Holidaysa
a	a	k8xC	a
dokumentu	dokument	k1gInSc2	dokument
Joan	Joan	k1gMnSc1	Joan
of	of	k?	of
Arc	Arc	k1gMnSc1	Arc
<g/>
.	.	kIx.	.
</s>
<s>
Zlomovým	zlomový	k2eAgInSc7d1	zlomový
rokem	rok	k1gInSc7	rok
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
herecké	herecký	k2eAgFnSc6d1	herecká
kariéře	kariéra	k1gFnSc6	kariéra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
rok	rok	k1gInSc1	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dostala	dostat	k5eAaPmAgFnS	dostat
malou	malý	k2eAgFnSc4d1	malá
roli	role	k1gFnSc4	role
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Saténový	saténový	k2eAgInSc4d1	saténový
střevíček	střevíček	k1gInSc4	střevíček
s	s	k7c7	s
Libuší	Libuše	k1gFnSc7	Libuše
Šafránkovou	Šafránková	k1gFnSc7	Šafránková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Rokoko	rokoko	k1gNnSc4	rokoko
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
během	během	k7c2	během
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Shakespearovu	Shakespearův	k2eAgFnSc4d1	Shakespearova
Julii	Julie	k1gFnSc4	Julie
<g/>
,	,	kIx,	,
Aňu	Aňu	k1gFnSc4	Aňu
ve	v	k7c6	v
Višňovém	višňový	k2eAgInSc6d1	višňový
sadu	sad	k1gInSc6	sad
<g/>
,	,	kIx,	,
Johanku	Johanka	k1gFnSc4	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
ve	v	k7c6	v
Skřivánkovi	Skřivánek	k1gMnSc6	Skřivánek
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc7	Mary
Warrenovou	Warrenový	k2eAgFnSc7d1	Warrenový
v	v	k7c6	v
Čarodějkách	čarodějka	k1gFnPc6	čarodějka
ze	z	k7c2	z
Salemu	Salem	k1gInSc2	Salem
<g/>
,	,	kIx,	,
Alici	Alice	k1gFnSc4	Alice
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divadelním	divadelní	k2eAgInSc6d1	divadelní
spolku	spolek	k1gInSc6	spolek
Kašpar	Kašpar	k1gMnSc1	Kašpar
hrála	hrát	k5eAaImAgFnS	hrát
trojroli	trojrole	k1gFnSc4	trojrole
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
královny	královna	k1gFnSc2	královna
Markéty	Markéta	k1gFnSc2	Markéta
a	a	k8xC	a
písaře	písař	k1gMnPc4	písař
v	v	k7c4	v
Richardu	Richarda	k1gFnSc4	Richarda
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
muzikálu	muzikál	k1gInSc2	muzikál
Excalibur	Excalibura	k1gFnPc2	Excalibura
ztělesňovala	ztělesňovat	k5eAaImAgFnS	ztělesňovat
Morganu	morgan	k1gInSc3	morgan
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
muzikálovou	muzikálový	k2eAgFnSc7d1	muzikálová
rolí	role	k1gFnSc7	role
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
i	i	k9	i
Hanka	Hanka	k1gFnSc1	Hanka
ve	v	k7c6	v
Starcích	stařec	k1gMnPc6	stařec
na	na	k7c6	na
chmelu	chmel	k1gInSc6	chmel
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Milénium	milénium	k1gNnSc1	milénium
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Karin	Karina	k1gFnPc2	Karina
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Touha	touha	k1gFnSc1	touha
–	–	k?	–
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Kalich	kalich	k1gInSc1	kalich
<g/>
)	)	kIx)	)
Věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
i	i	k9	i
dabingu	dabing	k1gInSc3	dabing
a	a	k8xC	a
práci	práce	k1gFnSc3	práce
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
(	(	kIx(	(
<g/>
Polepšovna	polepšovna	k1gFnSc1	polepšovna
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
Hra	hra	k1gFnSc1	hra
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
Bouřka	bouřka	k1gFnSc1	bouřka
<g/>
,	,	kIx,	,
O	o	k7c6	o
Rubáškovi	Rubášek	k1gMnSc6	Rubášek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterou	který	k3yRgFnSc7	který
získala	získat	k5eAaPmAgFnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
Neviditelný	viditelný	k2eNgMnSc1d1	Neviditelný
herec	herec	k1gMnSc1	herec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
divácké	divácký	k2eAgFnSc6d1	divácká
anketě	anketa	k1gFnSc6	anketa
TýTý	TýTý	k1gFnSc2	TýTý
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
květin	květina	k1gFnPc2	květina
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Nebeský	nebeský	k2eAgInSc1d1	nebeský
pláč	pláč	k1gInSc1	pláč
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Hrad	hrad	k1gInSc1	hrad
z	z	k7c2	z
písku	písek	k1gInSc2	písek
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Nejasná	jasný	k2eNgFnSc1d1	nejasná
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Šmankote	Šmankote	k?	Šmankote
<g/>
,	,	kIx,	,
babičko	babička	k1gFnSc5	babička
<g/>
,	,	kIx,	,
čaruj	čarovat	k5eAaImRp2nS	čarovat
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Baječná	Baječný	k2eAgNnPc1d1	Baječný
show	show	k1gNnPc1	show
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Kožené	kožený	k2eAgNnSc1d1	kožené
slunce	slunce	k1gNnSc1	slunce
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Ta	ten	k3xDgFnSc1	ten
třetí	třetí	k4xOgFnSc2	třetí
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Snowboarďáci	Snowboarďák	k1gMnPc1	Snowboarďák
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Post	post	k1gInSc1	post
Coitum	Coitum	k1gNnSc1	Coitum
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Umění	umění	k1gNnSc1	umění
milovat	milovat	k5eAaImF	milovat
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Poslední	poslední	k2eAgFnPc1d1	poslední
prázdniny	prázdniny	k1gFnPc1	prázdniny
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Kvaska	Kvask	k1gInSc2	Kvask
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Bathory	Bathora	k1gFnSc2	Bathora
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Rodinka	rodinka	k1gFnSc1	rodinka
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Labyrint	labyrint	k1gInSc1	labyrint
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Babovřesky	Babovřeska	k1gFnSc2	Babovřeska
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Babovřesky	Babovřeska	k1gFnSc2	Babovřeska
2	[number]	k4	2
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Babovřesky	Babovřeska	k1gFnSc2	Babovřeska
3	[number]	k4	3
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Křesadlo	křesadlo	k1gNnSc1	křesadlo
(	(	kIx(	(
<g/>
princezna	princezna	k1gFnSc1	princezna
Astrid	Astrid	k1gInSc1	Astrid
<g/>
)	)	kIx)	)
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
(	(	kIx(	(
<g/>
Disney	Disney	k1gInPc1	Disney
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
–	–	k?	–
Alenka	Alenka	k1gFnSc1	Alenka
Helena	Helena	k1gFnSc1	Helena
Trojská	trojský	k2eAgFnSc1d1	Trojská
(	(	kIx(	(
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
</s>
<s>
Helen	Helena	k1gFnPc2	Helena
of	of	k?	of
Troy	Troa	k1gMnSc2	Troa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
–	–	k?	–
Helena	Helena	k1gFnSc1	Helena
(	(	kIx(	(
<g/>
Sienna	Sienna	k1gFnSc1	Sienna
Guillory	Guillora	k1gFnSc2	Guillora
<g/>
)	)	kIx)	)
Johanka	Johanka	k1gFnSc1	Johanka
z	z	k7c2	z
A	A	kA	A
<g/>
''	''	k?	''
<g/>
rku	rku	k?	rku
(	(	kIx(	(
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
</s>
<s>
Joan	Joano	k1gNnPc2	Joano
of	of	k?	of
Arc	Arc	k1gFnSc2	Arc
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
–	–	k?	–
Johanka	Johanka	k1gFnSc1	Johanka
Hrdinové	Hrdinová	k1gFnSc2	Hrdinová
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc4	seriál
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Dexter	Dextra	k1gFnPc2	Dextra
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc4	seriál
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Ladíme	ladit	k5eAaImIp1nP	ladit
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
</s>
<s>
Pitch	Pitch	k1gMnSc1	Pitch
Perfect	Perfect	k1gMnSc1	Perfect
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
–	–	k?	–
Aubrey	Aubrey	k1gInPc1	Aubrey
Posen	Posen	k1gInSc1	Posen
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
Camp	camp	k1gInSc1	camp
<g/>
)	)	kIx)	)
Gossip	Gossip	k1gInSc1	Gossip
Girl	girl	k1gFnSc2	girl
–	–	k?	–
Blair	Blair	k1gMnSc1	Blair
Waldorf	Waldorf	k1gMnSc1	Waldorf
Krok	krok	k1gInSc4	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
–	–	k?	–
Christine	Christin	k1gInSc5	Christin
Lakin	Lakina	k1gFnPc2	Lakina
Pravěk	pravěk	k1gInSc4	pravěk
útočí	útočit	k5eAaImIp3nS	útočit
–	–	k?	–
Hannah	Hannah	k1gMnSc1	Hannah
Spearitt	Spearitt	k1gMnSc1	Spearitt
–	–	k?	–
Abby	Abba	k1gFnSc2	Abba
G.	G.	kA	G.
<g/>
Jedna	jeden	k4xCgFnSc1	jeden
<g/>
:	:	kIx,	:
První	první	k4xOgFnSc1	první
stávka	stávka	k1gFnSc1	stávka
(	(	kIx(	(
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
</s>
<s>
Ra	ra	k0	ra
<g/>
.	.	kIx.	.
<g/>
One	One	k1gFnSc6	One
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
–	–	k?	–
Sonia	Sonia	k1gFnSc1	Sonia
(	(	kIx(	(
<g/>
ČT	ČT	kA	ČT
<g/>
)	)	kIx)	)
Alenka	Alenka	k1gFnSc1	Alenka
–	–	k?	–
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nestane	stanout	k5eNaPmIp3nS	stanout
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
–	–	k?	–
Phlos	Phlos	k1gInSc1	Phlos
Čarodějův	čarodějův	k2eAgMnSc1d1	čarodějův
učeň	učeň	k1gMnSc1	učeň
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
–	–	k?	–
Becky	Becka	k1gFnSc2	Becka
(	(	kIx(	(
<g/>
Teresa	Teresa	k1gFnSc1	Teresa
Palmer	Palmer	k1gMnSc1	Palmer
<g/>
)	)	kIx)	)
MARMELÁDA	marmeláda	k1gFnSc1	marmeláda
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
ROK	rok	k1gInSc1	rok
2060	[number]	k4	2060
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Láska	láska	k1gFnSc1	láska
na	na	k7c4	na
100	[number]	k4	100
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Atlantida	Atlantida	k1gFnSc1	Atlantida
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Marmeláda	marmeláda	k1gFnSc1	marmeláda
video	video	k1gNnSc4	video
<g/>
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
CD-ROM	CD-ROM	k1gFnSc3	CD-ROM
Lucka	Lucko	k1gNnSc2	Lucko
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
best	best	k1gMnSc1	best
of	of	k?	of
English	English	k1gInSc1	English
version	version	k1gInSc1	version
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
MANON	MANON	kA	MANON
singl	singl	k1gInSc1	singl
<g/>
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
MANON	MANON	kA	MANON
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Mayday	Maydaa	k1gFnSc2	Maydaa
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
BOOMERANG	BOOMERANG	kA	BOOMERANG
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
DANCE	Danka	k1gFnSc6	Danka
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Pelmel	pelmel	k6eAd1	pelmel
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Fénix	fénix	k1gMnSc1	fénix
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Dárek	dárek	k1gInSc1	dárek
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Oheň	oheň	k1gInSc1	oheň
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Duety	duet	k1gInPc1	duet
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
Hit	hit	k1gInSc1	hit
Tour	Tour	k1gInSc1	Tour
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Anděl	Anděla	k1gFnPc2	Anděla
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
:	:	kIx,	:
2010	[number]	k4	2010
Ceny	cena	k1gFnSc2	cena
Óčko	Óčko	k6eAd1	Óčko
(	(	kIx(	(
<g/>
Pop	pop	k1gInSc1	pop
<g/>
&	&	k?	&
<g/>
Dance	Danka	k1gFnSc6	Danka
<g/>
)	)	kIx)	)
–	–	k?	–
2009	[number]	k4	2009
Neviditelný	viditelný	k2eNgMnSc1d1	Neviditelný
herec	herec	k1gMnSc1	herec
–	–	k?	–
2011	[number]	k4	2011
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
Otto	Otto	k1gMnSc1	Otto
–	–	k?	–
2007	[number]	k4	2007
Stříbrný	stříbrný	k1gInSc1	stříbrný
<g/>
:	:	kIx,	:
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
Bronzový	bronzový	k2eAgMnSc1d1	bronzový
<g/>
:	:	kIx,	:
2007	[number]	k4	2007
2007	[number]	k4	2007
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2008	[number]	k4	2008
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2009	[number]	k4	2009
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2010	[number]	k4	2010
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2011	[number]	k4	2011
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2012	[number]	k4	2012
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2013	[number]	k4	2013
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2014	[number]	k4	2014
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
CD	CD	kA	CD
–	–	k?	–
Marmeláda	marmeláda	k1gFnSc1	marmeláda
*	*	kIx~	*
Rok	rok	k1gInSc1	rok
2060	[number]	k4	2060
*	*	kIx~	*
Atlantida	Atlantida	k1gFnSc1	Atlantida
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
*	*	kIx~	*
Mayday	Mayda	k2eAgInPc1d1	Mayda
*	*	kIx~	*
Boomerang	Boomerang	k1gInSc1	Boomerang
*	*	kIx~	*
Pelmel	pelmel	k6eAd1	pelmel
*	*	kIx~	*
Fénix	fénix	k1gMnSc1	fénix
*	*	kIx~	*
Dárek	dárek	k1gInSc1	dárek
*	*	kIx~	*
Oheň	oheň	k1gInSc1	oheň
CD	CD	kA	CD
–	–	k?	–
Marmeláda	marmeláda	k1gFnSc1	marmeláda
*	*	kIx~	*
Atlantida	Atlantida	k1gFnSc1	Atlantida
*	*	kIx~	*
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
*	*	kIx~	*
Boomerang	Boomerang	k1gInSc1	Boomerang
*	*	kIx~	*
Pelmel	pelmel	k6eAd1	pelmel
*	*	kIx~	*
Fénix	fénix	k1gMnSc1	fénix
*	*	kIx~	*
Dárek	dárek	k1gInSc1	dárek
*	*	kIx~	*
Oheň	oheň	k1gInSc1	oheň
Ocenění	ocenění	k1gNnSc1	ocenění
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Tommü	Tommü	k1gMnPc2	Tommü
Records	Recordsa	k1gFnPc2	Recordsa
za	za	k7c4	za
300.000	[number]	k4	300.000
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
u	u	k7c2	u
tohoto	tento	k3xDgNnSc2	tento
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Hit	hit	k1gInSc1	hit
Tour	Tour	k1gInSc1	Tour
2013	[number]	k4	2013
Lucka	Lucka	k1gFnSc1	Lucka
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
–	–	k?	–
konalo	konat	k5eAaImAgNnS	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
10	[number]	k4	10
městech	město	k1gNnPc6	město
ČR	ČR	kA	ČR
od	od	k7c2	od
konce	konec	k1gInSc2	konec
května	květen	k1gInSc2	květen
až	až	k9	až
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
červen	červen	k1gInSc4	červen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Kousek	kousek	k6eAd1	kousek
štěští	štěštit	k5eAaPmIp3nS	štěštit
tour	tour	k1gInSc4	tour
2016	[number]	k4	2016
Muzikál	muzikál	k1gInSc1	muzikál
Excalibur	Excalibur	k1gMnSc1	Excalibur
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Muzikál	muzikál	k1gInSc1	muzikál
Tajemství	tajemství	k1gNnSc1	tajemství
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Muzikál	muzikál	k1gInSc1	muzikál
Touha	touha	k1gFnSc1	touha
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Ať	ať	k8xC	ať
žije	žít	k5eAaImIp3nS	žít
rokenrol	rokenrol	k1gInSc1	rokenrol
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
činoherní	činoherní	k2eAgNnSc4d1	činoherní
představení	představení	k1gNnSc4	představení
a	a	k8xC	a
muzikál	muzikál	k1gInSc4	muzikál
Starci	stařec	k1gMnPc1	stařec
na	na	k7c6	na
chmelu	chmel	k1gInSc6	chmel
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
Lucie	Lucie	k1gFnSc2	Lucie
Vondráčkové	Vondráčkové	k2eAgInSc4d1	Vondráčkové
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
<g />
.	.	kIx.	.
</s>
<s>
katalogu	katalog	k1gInSc3	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Lucie	Lucie	k1gFnSc1	Lucie
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lucie	Lucie	k1gFnSc2	Lucie
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Lucie	Lucie	k1gFnSc2	Lucie
Vondráčkové	Vondráčková	k1gFnSc2	Vondráčková
Oficiální	oficiální	k2eAgInSc4d1	oficiální
fanklub	fanklub	k1gInSc4	fanklub
Lucie	Lucie	k1gFnSc2	Lucie
Vondráčkové	Vondráčková	k1gFnSc2	Vondráčková
Lucie	Lucie	k1gFnSc1	Lucie
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
–	–	k?	–
český	český	k2eAgMnSc1d1	český
fan	fana	k1gFnPc2	fana
web	web	k1gInSc4	web
Lucie	Lucie	k1gFnSc1	Lucie
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
v	v	k7c6	v
Impulsech	impuls	k1gInPc6	impuls
Václava	Václav	k1gMnSc2	Václav
Moravce	Moravec	k1gMnSc2	Moravec
Lucie	Lucie	k1gFnSc1	Lucie
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
