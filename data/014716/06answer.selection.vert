<s>
Plutonium	plutonium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Pu	Pu	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
šestý	šestý	k4xOgMnSc1
člen	člen	k1gMnSc1
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
transuran	transuran	k1gInSc1
<g/>
,	,	kIx,
radioaktivní	radioaktivní	k2eAgInSc1d1
<g/>
,	,	kIx,
řetězovou	řetězový	k2eAgFnSc7d1
reakcí	reakce	k1gFnSc7
štěpitelný	štěpitelný	k2eAgInSc1d1
<g/>
,	,	kIx,
toxický	toxický	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
bombardováním	bombardování	k1gNnSc7
uranu	uran	k1gInSc2
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
<g/>
,	,	kIx,
především	především	k9
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
atomových	atomový	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
<g/>
.	.	kIx.
</s>