<p>
<s>
Plch	Plch	k1gMnSc1	Plch
zahradní	zahradní	k2eAgMnSc1d1	zahradní
(	(	kIx(	(
<g/>
Eliomys	Eliomys	k1gInSc1	Eliomys
quercinus	quercinus	k1gInSc1	quercinus
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
našich	náš	k3xOp1gMnPc2	náš
plchů	plch	k1gMnPc2	plch
nejnápadnější	nápadní	k2eAgNnSc1d3	nápadní
zabarvení	zabarvení	k1gNnSc1	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
barva	barva	k1gFnSc1	barva
hřbetu	hřbet	k1gInSc2	hřbet
a	a	k8xC	a
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
šedohnědá	šedohnědý	k2eAgFnSc1d1	šedohnědá
až	až	k6eAd1	až
hnědočervená	hnědočervený	k2eAgFnSc1d1	hnědočervená
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
strana	strana	k1gFnSc1	strana
těla	tělo	k1gNnSc2	tělo
bývá	bývat	k5eAaImIp3nS	bývat
naopak	naopak	k6eAd1	naopak
čistě	čistě	k6eAd1	čistě
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ostře	ostro	k6eAd1	ostro
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hlavy	hlava	k1gFnSc2	hlava
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
oka	oko	k1gNnSc2	oko
výrazný	výrazný	k2eAgInSc4d1	výrazný
tmavý	tmavý	k2eAgInSc4d1	tmavý
pruh	pruh	k1gInSc4	pruh
<g/>
,	,	kIx,	,
zasahující	zasahující	k2eAgFnSc4d1	zasahující
až	až	k9	až
ke	k	k7c3	k
krku	krk	k1gInSc3	krk
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
tělo	tělo	k1gNnSc1	tělo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
krátkou	krátký	k2eAgFnSc7d1	krátká
tmavohnědou	tmavohnědý	k2eAgFnSc7d1	tmavohnědá
srstí	srst	k1gFnSc7	srst
a	a	k8xC	a
zakončený	zakončený	k2eAgInSc1d1	zakončený
černobílou	černobílý	k2eAgFnSc7d1	černobílá
štětičkou	štětička	k1gFnSc7	štětička
prodloužených	prodloužený	k2eAgInPc2d1	prodloužený
chlupů	chlup	k1gInPc2	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc4	oko
i	i	k8xC	i
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgFnPc1d1	velká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
110	[number]	k4	110
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
mm	mm	kA	mm
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
mm	mm	kA	mm
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
i	i	k8xC	i
s	s	k7c7	s
ocasem	ocas	k1gInSc7	ocas
přibližně	přibližně	k6eAd1	přibližně
26	[number]	k4	26
cm	cm	kA	cm
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
140	[number]	k4	140
g.	g.	k?	g.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
hlavně	hlavně	k9	hlavně
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
pohraničí	pohraničí	k1gNnSc2	pohraničí
(	(	kIx(	(
<g/>
západní	západní	k2eAgNnSc1d1	západní
Pošumaví	Pošumaví	k1gNnSc1	Pošumaví
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
okolí	okolí	k1gNnSc1	okolí
Karlových	Karlův	k2eAgInPc2d1	Karlův
Var	Vary	k1gInPc2	Vary
<g/>
,	,	kIx,	,
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
Lužické	lužický	k2eAgFnPc1d1	Lužická
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
ze	z	k7c2	z
západního	západní	k2eAgInSc2d1	západní
okraje	okraj	k1gInSc2	okraj
Brd	Brdy	k1gInPc2	Brdy
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
nebyl	být	k5eNaImAgInS	být
zatím	zatím	k6eAd1	zatím
výskyt	výskyt	k1gInSc1	výskyt
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
s	s	k7c7	s
určitostí	určitost	k1gFnSc7	určitost
prokázán	prokázat	k5eAaPmNgMnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInPc1d2	starší
údaje	údaj	k1gInPc1	údaj
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dokládají	dokládat	k5eAaImIp3nP	dokládat
jeho	jeho	k3xOp3gInSc4	jeho
někdejší	někdejší	k2eAgInSc4d1	někdejší
výskyt	výskyt	k1gInSc4	výskyt
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
okolí	okolí	k1gNnSc6	okolí
Davle	Davle	k1gNnSc2	Davle
<g/>
,	,	kIx,	,
Karlštejna	Karlštejn	k1gInSc2	Karlštejn
<g/>
,	,	kIx,	,
Dobříše	Dobříš	k1gFnSc2	Dobříš
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
populace	populace	k1gFnSc1	populace
plcha	plch	k1gMnSc2	plch
zahradního	zahradní	k2eAgInSc2d1	zahradní
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
a	a	k8xC	a
v	v	k7c6	v
Labských	labský	k2eAgInPc6d1	labský
pískovcích	pískovec	k1gInPc6	pískovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ostrůvkovitě	ostrůvkovitě	k6eAd1	ostrůvkovitě
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
po	po	k7c4	po
Ural	Ural	k1gInSc4	Ural
<g/>
)	)	kIx)	)
i	i	k9	i
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
chybí	chybět	k5eAaImIp3nS	chybět
na	na	k7c6	na
britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
:	:	kIx,	:
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgNnSc1d1	jižní
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
střední	střední	k2eAgNnSc1d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgNnSc1d1	jižní
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
fauny	fauna	k1gFnSc2	fauna
teplejších	teplý	k2eAgNnPc2d2	teplejší
období	období	k1gNnPc2	období
holocénu	holocén	k1gInSc2	holocén
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
ostrůvkovité	ostrůvkovitý	k2eAgNnSc1d1	ostrůvkovité
rozšíření	rozšíření	k1gNnSc1	rozšíření
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
rozčleněním	rozčlenění	k1gNnSc7	rozčlenění
souvislých	souvislý	k2eAgInPc2d1	souvislý
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
plchů	plch	k1gMnPc2	plch
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
než	než	k8xS	než
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
masožravý	masožravý	k2eAgInSc1d1	masožravý
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
složení	složení	k1gNnSc1	složení
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
80	[number]	k4	80
%	%	kIx~	%
živočišná	živočišný	k2eAgFnSc1d1	živočišná
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
bezobratlými	bezobratlý	k2eAgMnPc7d1	bezobratlý
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
drobnými	drobný	k2eAgMnPc7d1	drobný
savci	savec	k1gMnPc1	savec
i	i	k8xC	i
ptačími	ptačí	k2eAgNnPc7d1	ptačí
mláďaty	mládě	k1gNnPc7	mládě
a	a	k8xC	a
vejci	vejce	k1gNnPc7	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
potravy	potrava	k1gFnSc2	potrava
převažují	převažovat	k5eAaImIp3nP	převažovat
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
semena	semeno	k1gNnSc2	semeno
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
pupeny	pupen	k1gInPc1	pupen
a	a	k8xC	a
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
teprve	teprve	k6eAd1	teprve
k	k	k7c3	k
podzimu	podzim	k1gInSc2	podzim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
našich	náš	k3xOp1gMnPc2	náš
plchů	plch	k1gMnPc2	plch
je	být	k5eAaImIp3nS	být
plch	plch	k1gMnSc1	plch
zahradní	zahradní	k2eAgMnSc1d1	zahradní
nejméně	málo	k6eAd3	málo
vázaný	vázaný	k2eAgInSc1d1	vázaný
na	na	k7c4	na
lesní	lesní	k2eAgInSc4d1	lesní
biotop	biotop	k1gInSc4	biotop
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
skalnatým	skalnatý	k2eAgInPc3d1	skalnatý
a	a	k8xC	a
suťovým	suťový	k2eAgInPc3d1	suťový
terénům	terén	k1gInPc3	terén
(	(	kIx(	(
<g/>
kamenné	kamenný	k2eAgFnPc4d1	kamenná
zídky	zídka	k1gFnPc4	zídka
<g/>
,	,	kIx,	,
zbořeniště	zbořeniště	k1gNnSc1	zbořeniště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
kolem	kolem	k7c2	kolem
lesních	lesní	k2eAgFnPc2d1	lesní
chat	chata	k1gFnPc2	chata
<g/>
,	,	kIx,	,
seníků	seník	k1gInPc2	seník
<g/>
,	,	kIx,	,
krmelců	krmelec	k1gInPc2	krmelec
i	i	k8xC	i
osamělých	osamělý	k2eAgNnPc2d1	osamělé
stavení	stavení	k1gNnPc2	stavení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
stanoviště	stanoviště	k1gNnPc1	stanoviště
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
lesní	lesní	k2eAgFnPc4d1	lesní
paseky	paseka	k1gFnPc4	paseka
<g/>
,	,	kIx,	,
sady	sada	k1gFnPc4	sada
<g/>
,	,	kIx,	,
zahrady	zahrada	k1gFnPc4	zahrada
nebo	nebo	k8xC	nebo
vinice	vinice	k1gFnPc4	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
chladných	chladný	k2eAgFnPc6d1	chladná
a	a	k8xC	a
vlhkých	vlhký	k2eAgNnPc6d1	vlhké
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
vhodných	vhodný	k2eAgInPc6d1	vhodný
biotopech	biotop	k1gInPc6	biotop
žijí	žít	k5eAaImIp3nP	žít
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
zvířata	zvíře	k1gNnPc1	zvíře
(	(	kIx(	(
<g/>
po	po	k7c6	po
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
mláďat	mládě	k1gNnPc2	mládě
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
až	až	k9	až
šest	šest	k4xCc4	šest
<g/>
)	)	kIx)	)
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
jednoho	jeden	k4xCgInSc2	jeden
hektaru	hektar	k1gInSc2	hektar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiní	jiný	k2eAgMnPc1d1	jiný
plši	plch	k1gMnPc1	plch
je	být	k5eAaImIp3nS	být
tvorem	tvor	k1gMnSc7	tvor
výhradně	výhradně	k6eAd1	výhradně
nočním	noční	k2eAgMnSc6d1	noční
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
aktivity	aktivita	k1gFnSc2	aktivita
mezi	mezi	k7c7	mezi
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
hodinou	hodina	k1gFnSc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Orientuje	orientovat	k5eAaBmIp3nS	orientovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
sluchem	sluch	k1gInSc7	sluch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
se	se	k3xPyFc4	se
ozývají	ozývat	k5eAaImIp3nP	ozývat
jasným	jasný	k2eAgInSc7d1	jasný
"	"	kIx"	"
<g/>
cú	cú	k?	cú
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
opět	opět	k6eAd1	opět
charakteristickým	charakteristický	k2eAgNnSc7d1	charakteristické
vrčením	vrčení	k1gNnSc7	vrčení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Doba	doba	k1gFnSc1	doba
páření	páření	k1gNnSc2	páření
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
nastává	nastávat	k5eAaImIp3nS	nastávat
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
ze	z	k7c2	z
zimního	zimní	k2eAgInSc2d1	zimní
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
březosti	březost	k1gFnSc6	březost
trvající	trvající	k2eAgFnSc1d1	trvající
21	[number]	k4	21
až	až	k9	až
23	[number]	k4	23
dní	den	k1gInPc2	den
samice	samice	k1gFnSc1	samice
vrhá	vrhat	k5eAaImIp3nS	vrhat
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
června	červen	k1gInSc2	červen
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
3	[number]	k4	3
až	až	k9	až
6	[number]	k4	6
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Mláďatům	mládě	k1gNnPc3	mládě
se	se	k3xPyFc4	se
po	po	k7c6	po
19	[number]	k4	19
dnech	den	k1gInPc6	den
otevírají	otevírat	k5eAaImIp3nP	otevírat
oči	oko	k1gNnPc1	oko
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
kojena	kojen	k2eAgNnPc1d1	kojeno
4	[number]	k4	4
týdny	týden	k1gInPc4	týden
a	a	k8xC	a
po	po	k7c4	po
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
osamostatňují	osamostatňovat	k5eAaImIp3nP	osamostatňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
až	až	k6eAd1	až
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
až	až	k9	až
po	po	k7c6	po
přezimování	přezimování	k1gNnSc6	přezimování
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
věk	věk	k1gInSc1	věk
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
věku	věk	k1gInSc6	věk
4	[number]	k4	4
let	léto	k1gNnPc2	léto
dožije	dožít	k5eAaPmIp3nS	dožít
jen	jen	k9	jen
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přezimování	přezimování	k1gNnSc2	přezimování
==	==	k?	==
</s>
</p>
<p>
<s>
Zimní	zimní	k2eAgNnPc1d1	zimní
hnízda	hnízdo	k1gNnPc1	hnízdo
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgNnPc1d2	veliký
<g/>
,	,	kIx,	,
suchá	suchý	k2eAgNnPc1d1	suché
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
vystlaná	vystlaný	k2eAgFnSc1d1	vystlaná
trávou	tráva	k1gFnSc7	tráva
a	a	k8xC	a
listím	listí	k1gNnSc7	listí
<g/>
,	,	kIx,	,
dostatečně	dostatečně	k6eAd1	dostatečně
chráněných	chráněný	k2eAgMnPc2d1	chráněný
před	před	k7c7	před
velkými	velký	k2eAgInPc7d1	velký
mrazy	mráz	k1gInPc7	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zimní	zimní	k2eAgInSc4d1	zimní
spánek	spánek	k1gInSc4	spánek
si	se	k3xPyFc3	se
vybírají	vybírat	k5eAaImIp3nP	vybírat
dutiny	dutina	k1gFnPc4	dutina
ve	v	k7c6	v
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
skalách	skála	k1gFnPc6	skála
<g/>
,	,	kIx,	,
podzemních	podzemní	k2eAgFnPc6d1	podzemní
norách	nora	k1gFnPc6	nora
jiných	jiný	k2eAgMnPc2d1	jiný
hlodavců	hlodavec	k1gMnPc2	hlodavec
či	či	k8xC	či
krtků	krtek	k1gMnPc2	krtek
nebo	nebo	k8xC	nebo
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
skrýších	skrýš	k1gFnPc6	skrýš
v	v	k7c6	v
chatách	chata	k1gFnPc6	chata
a	a	k8xC	a
srubech	srub	k1gInPc6	srub
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
hnízdě	hnízdo	k1gNnSc6	hnízdo
přezimuje	přezimovat	k5eAaBmIp3nS	přezimovat
i	i	k9	i
více	hodně	k6eAd2	hodně
jedinců	jedinec	k1gMnPc2	jedinec
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hibernace	hibernace	k1gFnSc2	hibernace
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
plši	plch	k1gMnPc1	plch
40	[number]	k4	40
až	až	k8xS	až
50	[number]	k4	50
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
těla	tělo	k1gNnSc2	tělo
klesá	klesat	k5eAaImIp3nS	klesat
na	na	k7c4	na
5	[number]	k4	5
°	°	k?	°
<g/>
C.	C.	kA	C.
K	k	k7c3	k
zimnímu	zimní	k2eAgInSc3d1	zimní
spánku	spánek	k1gInSc3	spánek
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
nebo	nebo	k8xC	nebo
až	až	k6eAd1	až
počátkem	počátkem	k7c2	počátkem
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
probouzejí	probouzet	k5eAaImIp3nP	probouzet
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgMnPc1d1	aktivní
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
staví	stavit	k5eAaBmIp3nS	stavit
letní	letní	k2eAgNnPc4d1	letní
hnízda	hnízdo	k1gNnPc4	hnízdo
z	z	k7c2	z
větviček	větvička	k1gFnPc2	větvička
<g/>
,	,	kIx,	,
trávy	tráva	k1gFnSc2	tráva
a	a	k8xC	a
listí	listí	k1gNnSc1	listí
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
v	v	k7c6	v
senících	seník	k1gInPc6	seník
nebo	nebo	k8xC	nebo
osamělých	osamělý	k2eAgNnPc6d1	osamělé
staveních	stavení	k1gNnPc6	stavení
<g/>
.	.	kIx.	.
</s>
<s>
Zavděk	zavděk	k6eAd1	zavděk
vezmou	vzít	k5eAaPmIp3nP	vzít
též	též	k9	též
stromovou	stromový	k2eAgFnSc4d1	stromová
a	a	k8xC	a
skalní	skalní	k2eAgFnSc4d1	skalní
dutinu	dutina	k1gFnSc4	dutina
či	či	k8xC	či
hnízdní	hnízdní	k2eAgFnSc4d1	hnízdní
budku	budka	k1gFnSc4	budka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
odolnější	odolný	k2eAgMnSc1d2	odolnější
vůči	vůči	k7c3	vůči
zmijímu	zmijí	k2eAgInSc3d1	zmijí
jedu	jed	k1gInSc3	jed
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
naši	náš	k3xOp1gMnPc1	náš
hlodavci	hlodavec	k1gMnPc1	hlodavec
(	(	kIx(	(
<g/>
se	s	k7c7	s
zmijí	zmije	k1gFnSc7	zmije
často	často	k6eAd1	často
sdílí	sdílet	k5eAaImIp3nS	sdílet
stejná	stejný	k2eAgNnPc4d1	stejné
kamenitá	kamenitý	k2eAgNnPc4d1	kamenité
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
živočišný	živočišný	k2eAgInSc1d1	živočišný
druh	druh	k1gInSc1	druh
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
hlodavců	hlodavec	k1gMnPc2	hlodavec
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
lokální	lokální	k2eAgFnSc1d1	lokální
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
početností	početnost	k1gFnSc7	početnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zdejších	zdejší	k2eAgMnPc2d1	zdejší
nejvzácnějších	vzácný	k2eAgMnPc2d3	nejvzácnější
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ještě	ještě	k9	ještě
plch	plch	k1gMnSc1	plch
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Dryomys	Dryomys	k1gInSc1	Dryomys
nitedula	nitedulum	k1gNnSc2	nitedulum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Početnost	početnost	k1gFnSc1	početnost
plcha	plch	k1gMnSc2	plch
zahradního	zahradní	k2eAgMnSc2d1	zahradní
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
v	v	k7c6	v
Červené	Červené	k2eAgFnSc6d1	Červené
knize	kniha	k1gFnSc6	kniha
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
v	v	k7c6	v
textu	text	k1gInSc6	text
Bernské	bernský	k2eAgFnSc2d1	Bernská
konvence	konvence	k1gFnSc2	konvence
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
chráněných	chráněný	k2eAgInPc2d1	chráněný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Plchů	Plch	k1gMnPc2	Plch
zahradních	zahradní	k2eAgFnPc2d1	zahradní
ubývá	ubývat	k5eAaImIp3nS	ubývat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ubývá	ubývat	k5eAaImIp3nS	ubývat
i	i	k9	i
jejich	jejich	k3xOp3gNnSc4	jejich
přirozené	přirozený	k2eAgNnSc4d1	přirozené
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
znamená	znamenat	k5eAaImIp3nS	znamenat
hlavně	hlavně	k9	hlavně
důsledně	důsledně	k6eAd1	důsledně
uchovat	uchovat	k5eAaPmF	uchovat
vhodná	vhodný	k2eAgNnPc4d1	vhodné
a	a	k8xC	a
osídlená	osídlený	k2eAgNnPc4d1	osídlené
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
populace	populace	k1gFnSc2	populace
lze	lze	k6eAd1	lze
vyvěšovat	vyvěšovat	k5eAaImF	vyvěšovat
budky	budka	k1gFnPc4	budka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
mohou	moct	k5eAaImIp3nP	moct
plchové	plch	k1gMnPc1	plch
využít	využít	k5eAaPmF	využít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
plch	plch	k1gMnSc1	plch
zahradní	zahradní	k2eAgMnSc1d1	zahradní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
