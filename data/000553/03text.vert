<s>
Září	září	k1gNnSc1	září
je	být	k5eAaImIp3nS	být
devátý	devátý	k4xOgInSc4	devátý
měsíc	měsíc	k1gInSc4	měsíc
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
podzim	podzim	k1gInSc4	podzim
(	(	kIx(	(
<g/>
podzimní	podzimní	k2eAgFnSc4d1	podzimní
rovnodennost	rovnodennost	k1gFnSc4	rovnodennost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Staročeské	staročeský	k2eAgNnSc1d1	staročeské
označení	označení	k1gNnSc1	označení
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
bylo	být	k5eAaImAgNnS	být
zářuj	zářovat	k5eAaImRp2nS	zářovat
nebo	nebo	k8xC	nebo
zářij	zářít	k5eAaPmRp2nS	zářít
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
za	za	k7c2	za
říje	říje	k1gFnSc2	říje
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
odvozování	odvozování	k1gNnSc1	odvozování
od	od	k7c2	od
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
lidovou	lidový	k2eAgFnSc7d1	lidová
etymologií	etymologie	k1gFnSc7	etymologie
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
většiny	většina	k1gFnSc2	většina
slovanských	slovanský	k2eAgMnPc2d1	slovanský
<g/>
)	)	kIx)	)
převzalo	převzít	k5eAaPmAgNnS	převzít
jeho	jeho	k3xOp3gInSc4	jeho
latinský	latinský	k2eAgInSc4d1	latinský
název	název	k1gInSc4	název
september	septembra	k1gFnPc2	septembra
(	(	kIx(	(
<g/>
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
sedmý	sedmý	k4xOgInSc4	sedmý
měsíc	měsíc	k1gInSc4	měsíc
před	před	k7c7	před
reformou	reforma	k1gFnSc7	reforma
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
začíná	začínat	k5eAaImIp3nS	začínat
období	období	k1gNnSc4	období
podzimní	podzimní	k2eAgFnSc2d1	podzimní
sklizně	sklizeň	k1gFnSc2	sklizeň
a	a	k8xC	a
školní	školní	k2eAgInSc4d1	školní
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
brambor	brambora	k1gFnPc2	brambora
dozrává	dozrávat	k5eAaImIp3nS	dozrávat
také	také	k9	také
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
a	a	k8xC	a
chmel	chmel	k1gInSc1	chmel
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
tradici	tradice	k1gFnSc6	tradice
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
měsíci	měsíc	k1gInSc6	měsíc
tzv.	tzv.	kA	tzv.
sklizňové	sklizňový	k2eAgFnSc6d1	sklizňová
slavnosti	slavnost	k1gFnSc6	slavnost
-	-	kIx~	-
vinobraní	vinobraní	k1gNnSc1	vinobraní
<g/>
,	,	kIx,	,
dočesná	dočesná	k1gFnSc1	dočesná
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
jižní	jižní	k2eAgFnPc4d1	jižní
Čechy	Čechy	k1gFnPc4	Čechy
typická	typický	k2eAgFnSc1d1	typická
konopická	konopický	k2eAgFnSc1d1	konopická
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
je	být	k5eAaImIp3nS	být
den	den	k1gInSc1	den
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
státního	státní	k2eAgInSc2d1	státní
svátku	svátek	k1gInSc2	svátek
<g/>
,	,	kIx,	,
do	do	k7c2	do
nedávné	dávný	k2eNgFnSc2d1	nedávná
minulosti	minulost	k1gFnSc2	minulost
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
hornatých	hornatý	k2eAgInPc6d1	hornatý
krajích	kraj	k1gInPc6	kraj
sváděl	svádět	k5eAaImAgInS	svádět
dobytek	dobytek	k1gInSc1	dobytek
z	z	k7c2	z
letních	letní	k2eAgFnPc2d1	letní
pastvin	pastvina	k1gFnPc2	pastvina
do	do	k7c2	do
vsí	ves	k1gFnPc2	ves
k	k	k7c3	k
přezimování	přezimování	k1gNnSc3	přezimování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
také	také	k9	také
končila	končit	k5eAaImAgFnS	končit
roční	roční	k2eAgFnSc1d1	roční
služba	služba	k1gFnSc1	služba
čeledi	čeleď	k1gFnSc2	čeleď
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
uzavíraly	uzavírat	k5eAaImAgFnP	uzavírat
smlouvy	smlouva	k1gFnPc1	smlouva
s	s	k7c7	s
hospodáři	hospodář	k1gMnPc7	hospodář
na	na	k7c4	na
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Plzeňsku	Plzeňsko	k1gNnSc6	Plzeňsko
si	se	k3xPyFc3	se
čeleď	čeleď	k1gFnSc1	čeleď
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
volila	volit	k5eAaImAgFnS	volit
chudého	chudý	k2eAgMnSc4d1	chudý
krále	král	k1gMnSc4	král
a	a	k8xC	a
královnu	královna	k1gFnSc4	královna
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pak	pak	k6eAd1	pak
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
"	"	kIx"	"
<g/>
dvořany	dvořan	k1gMnPc4	dvořan
<g/>
"	"	kIx"	"
obcházeli	obcházet	k5eAaImAgMnP	obcházet
ves	ves	k1gFnSc4	ves
a	a	k8xC	a
se	s	k7c7	s
zpěvem	zpěv	k1gInSc7	zpěv
prosili	prosít	k5eAaPmAgMnP	prosít
o	o	k7c4	o
dary	dar	k1gInPc4	dar
(	(	kIx(	(
<g/>
vejce	vejce	k1gNnSc1	vejce
<g/>
,	,	kIx,	,
pečivo	pečivo	k1gNnSc1	pečivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
hospodě	hospodě	k?	hospodě
konala	konat	k5eAaImAgFnS	konat
z	z	k7c2	z
výslužky	výslužka	k1gFnSc2	výslužka
hostina	hostina	k1gFnSc1	hostina
<g/>
.	.	kIx.	.
</s>
