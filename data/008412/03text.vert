<p>
<s>
Kamil	Kamil	k1gMnSc1	Kamil
Berdych	Berdych	k1gMnSc1	Berdych
(	(	kIx(	(
<g/>
křtěn	křtěn	k2eAgMnSc1d1	křtěn
Václav	Václav	k1gMnSc1	Václav
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1885	[number]	k4	1885
Nýřany	Nýřana	k1gFnSc2	Nýřana
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1914	[number]	k4	1914
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
vinohradská	vinohradský	k2eAgFnSc1d1	Vinohradská
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
anarchistický	anarchistický	k2eAgMnSc1d1	anarchistický
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
Nýřan	Nýřana	k1gFnPc2	Nýřana
<g/>
,	,	kIx,	,
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
neúplné	úplný	k2eNgFnSc6d1	neúplná
rodině	rodina	k1gFnSc6	rodina
a	a	k8xC	a
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
neprovdanou	provdaný	k2eNgFnSc7d1	neprovdaná
matkou	matka	k1gFnSc7	matka
Annou	Anna	k1gFnSc7	Anna
Berdychovou	Berdychová	k1gFnSc7	Berdychová
ve	v	k7c6	v
nuzných	nuzný	k2eAgInPc6d1	nuzný
poměrech	poměr	k1gInPc6	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
muž	muž	k1gMnSc1	muž
z	z	k7c2	z
"	"	kIx"	"
<g/>
lepší	dobrý	k2eAgFnSc2d2	lepší
společnosti	společnost	k1gFnSc2	společnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
Berdych	Berdych	k1gInSc1	Berdych
pohrdal	pohrdat	k5eAaImAgInS	pohrdat
a	a	k8xC	a
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
pohrdání	pohrdání	k1gNnSc4	pohrdání
i	i	k8xC	i
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
verších	verš	k1gInPc6	verš
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
knihvazač	knihvazač	k1gMnSc1	knihvazač
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
horník	horník	k1gMnSc1	horník
v	v	k7c6	v
kamenouhelných	kamenouhelný	k2eAgInPc6d1	kamenouhelný
dolech	dol	k1gInPc6	dol
v	v	k7c6	v
Nýřanech	Nýřan	k1gInPc6	Nýřan
a	a	k8xC	a
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
skládal	skládat	k5eAaImAgMnS	skládat
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
plzeňském	plzeňský	k2eAgInSc6d1	plzeňský
literárním	literární	k2eAgInSc6d1	literární
časopisu	časopis	k1gInSc6	časopis
Červen	červen	k1gInSc1	červen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spoluzakládal	spoluzakládat	k5eAaImAgInS	spoluzakládat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
sehnat	sehnat	k5eAaPmF	sehnat
místo	místo	k1gNnSc1	místo
kulisáka	kulisák	k1gMnSc2	kulisák
v	v	k7c6	v
ve	v	k7c6	v
vinohradském	vinohradský	k2eAgNnSc6d1	Vinohradské
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
knižně	knižně	k6eAd1	knižně
vydané	vydaný	k2eAgInPc4d1	vydaný
verše	verš	k1gInPc4	verš
Z	z	k7c2	z
melodií	melodie	k1gFnPc2	melodie
bastarda	bastard	k1gMnSc2	bastard
byly	být	k5eAaImAgFnP	být
kritikou	kritika	k1gFnSc7	kritika
příznivě	příznivě	k6eAd1	příznivě
přijaty	přijmout	k5eAaPmNgFnP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
politika	politika	k1gFnSc1	politika
ocenila	ocenit	k5eAaPmAgFnS	ocenit
<g/>
,	,	kIx,	,
že	že	k8xS	že
–	–	k?	–
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vrstevníků	vrstevník	k1gMnPc2	vrstevník
–	–	k?	–
nejsou	být	k5eNaImIp3nP	být
Berdychovy	Berdychův	k2eAgInPc1d1	Berdychův
verše	verš	k1gInPc1	verš
mnohomluvné	mnohomluvný	k2eAgInPc1d1	mnohomluvný
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
sedmadvacetiletý	sedmadvacetiletý	k2eAgMnSc1d1	sedmadvacetiletý
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
zemřel	zemřít	k5eAaPmAgInS	zemřít
týden	týden	k1gInSc1	týden
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Posmrtný	posmrtný	k2eAgInSc1d1	posmrtný
osud	osud	k1gInSc1	osud
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
zapomenut	zapomenout	k5eAaPmNgInS	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
připomněl	připomnět	k5eAaPmAgMnS	připomnět
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
v	v	k7c6	v
Kritickém	kritický	k2eAgInSc6d1	kritický
měsíčníku	měsíčník	k1gInSc6	měsíčník
Kamill	Kamill	k1gMnSc1	Kamill
Resler	Resler	k1gMnSc1	Resler
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
básníka	básník	k1gMnSc4	básník
znal	znát	k5eAaImAgMnS	znát
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
vzpomněl	vzpomnít	k5eAaPmAgMnS	vzpomnít
v	v	k7c6	v
Literárních	literární	k2eAgFnPc6d1	literární
novinách	novina	k1gFnPc6	novina
Kamila	Kamila	k1gFnSc1	Kamila
Berdycha	Berdycha	k1gFnSc1	Berdycha
v	v	k7c6	v
rozsáhlem	rozsáhl	k1gMnSc7	rozsáhl
článku	článek	k1gInSc2	článek
s	s	k7c7	s
ukázkami	ukázka	k1gFnPc7	ukázka
veršů	verš	k1gInPc2	verš
Michal	Michal	k1gMnSc1	Michal
Mareš	Mareš	k1gMnSc1	Mareš
<g/>
;	;	kIx,	;
dodatek	dodatek	k1gInSc1	dodatek
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
článku	článek	k1gInSc3	článek
uvedl	uvést	k5eAaPmAgMnS	uvést
Kamill	Kamill	k1gMnSc1	Kamill
Resler	Resler	k1gMnSc1	Resler
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
čísle	číslo	k1gNnSc6	číslo
časopisu	časopis	k1gInSc2	časopis
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
znovu	znovu	k6eAd1	znovu
objeven	objevit	k5eAaPmNgInS	objevit
a	a	k8xC	a
doceněn	doceněn	k2eAgInSc1d1	doceněn
literárními	literární	k2eAgMnPc7d1	literární
historiky	historik	k1gMnPc7	historik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Lexikonu	lexikon	k1gInSc2	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
byl	být	k5eAaImAgInS	být
autorův	autorův	k2eAgInSc1d1	autorův
postoj	postoj	k1gInSc1	postoj
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
smutně	smutně	k6eAd1	smutně
ironickým	ironický	k2eAgInSc7d1	ironický
protestem	protest	k1gInSc7	protest
a	a	k8xC	a
revoltou	revolta	k1gFnSc7	revolta
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
vyníká	vyníkat	k5eAaPmIp3nS	vyníkat
podmanivou	podmanivý	k2eAgFnSc7d1	podmanivá
hudebností	hudebnost	k1gFnSc7	hudebnost
i	i	k8xC	i
výrazovou	výrazový	k2eAgFnSc7d1	výrazová
přesností	přesnost	k1gFnSc7	přesnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
BERDYCH	BERDYCH	kA	BERDYCH
<g/>
,	,	kIx,	,
Kamil	Kamil	k1gMnSc1	Kamil
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
melodií	melodie	k1gFnPc2	melodie
bastarda	bastard	k1gMnSc2	bastard
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1907	[number]	k4	1907
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
47	[number]	k4	47
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
562561	[number]	k4	562561
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BERDYCH	BERDYCH	kA	BERDYCH
<g/>
,	,	kIx,	,
Kamil	Kamil	k1gMnSc1	Kamil
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Jairova	Jairov	k1gInSc2	Jairov
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Klub	klub	k1gInSc1	klub
přátel	přítel	k1gMnPc2	přítel
Tvaru	tvar	k1gInSc2	tvar
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
32	[number]	k4	32
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Epická	epický	k2eAgFnSc1d1	epická
báseň	báseň	k1gFnSc1	báseň
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
díla	dílo	k1gNnPc1	dílo
zůstala	zůstat	k5eAaPmAgNnP	zůstat
v	v	k7c6	v
rukopisech	rukopis	k1gInPc6	rukopis
a	a	k8xC	a
po	po	k7c6	po
básníkově	básníkův	k2eAgFnSc6d1	básníkova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
nezachovala	zachovat	k5eNaPmAgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FORST	FORST	kA	FORST
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
Osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
A-	A-	k1gFnPc1	A-
<g/>
G.	G.	kA	G.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
797	[number]	k4	797
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Kamil	Kamil	k1gMnSc1	Kamil
Berdych	Berdych	k1gMnSc1	Berdych
<g/>
,	,	kIx,	,
s.	s.	k?	s.
206	[number]	k4	206
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kamil	Kamil	k1gMnSc1	Kamil
Berdych	Berdycha	k1gFnPc2	Berdycha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Kamil	Kamil	k1gMnSc1	Kamil
Berdych	Berdych	k1gMnSc1	Berdych
</s>
</p>
<p>
<s>
Nýřany	Nýřana	k1gFnPc1	Nýřana
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Kamil	Kamil	k1gMnSc1	Kamil
Berdych	Berdych	k1gMnSc1	Berdych
-	-	kIx~	-
životopis	životopis	k1gInSc1	životopis
</s>
</p>
