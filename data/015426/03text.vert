<s>
Micubiši	Micubiš	k1gMnPc1
Kinsei	Kinse	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Micubiši	Micubiš	k1gMnPc1
Kinsei	Kinse	k1gFnSc2
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
金	金	k?
<g/>
,	,	kIx,
Venuše	Venuše	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
14	#num#	k4
<g/>
válcový	válcový	k2eAgInSc1d1
<g/>
,	,	kIx,
vzduchem	vzduch	k1gInSc7
chlazený	chlazený	k2eAgInSc1d1
<g/>
,	,	kIx,
dvouhvězdicový	dvouhvězdicový	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
motor	motor	k1gInSc1
používaný	používaný	k2eAgInSc1d1
v	v	k7c6
japonských	japonský	k2eAgNnPc6d1
letadlech	letadlo	k1gNnPc6
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkon	výkon	k1gInSc1
motoru	motor	k1gInSc2
se	se	k3xPyFc4
pohyboval	pohybovat	k5eAaImAgInS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
750	#num#	k4
kW	kW	kA
až	až	k9
1300	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
1000	#num#	k4
hp	hp	k?
-	-	kIx~
1700	#num#	k4
hp	hp	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Motor	motor	k1gInSc1
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
např.	např.	kA
v	v	k7c6
těchto	tento	k3xDgInPc6
typech	typ	k1gInPc6
letounů	letoun	k1gInPc2
<g/>
:	:	kIx,
Micubiši	Micubiš	k1gMnPc1
A6M	A6M	k1gMnSc1
„	„	k?
<g/>
Zero	Zero	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
jen	jen	k9
prototypy	prototyp	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Aiči	Aiči	k1gNnSc1
D3A	D3A	k1gFnSc2
„	„	k?
<g/>
Val	val	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
Kawaniši	Kawaniše	k1gFnSc6
H	H	kA
<g/>
6	#num#	k4
<g/>
K.	K.	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
