<p>
<s>
Puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
(	(	kIx(	(
<g/>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gInSc1	uralensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
noční	noční	k2eAgMnSc1d1	noční
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
puštíkovití	puštíkovitý	k2eAgMnPc1d1	puštíkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgFnPc4d1	vzácná
sovy	sova	k1gFnPc4	sova
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
počty	počet	k1gInPc1	počet
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
výru	výr	k1gMnSc6	výr
velkém	velký	k2eAgNnSc6d1	velké
naše	náš	k3xOp1gFnSc1	náš
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
sova	sova	k1gFnSc1	sova
dobře	dobře	k6eAd1	dobře
poznatelná	poznatelný	k2eAgFnSc1d1	poznatelná
podle	podle	k7c2	podle
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
ocasních	ocasní	k2eAgNnPc2d1	ocasní
per	pero	k1gNnPc2	pero
a	a	k8xC	a
charakteristického	charakteristický	k2eAgNnSc2d1	charakteristické
světlého	světlý	k2eAgNnSc2d1	světlé
zbarvení	zbarvení	k1gNnSc2	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
během	během	k7c2	během
dne	den	k1gInSc2	den
nebo	nebo	k8xC	nebo
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jarních	jarní	k2eAgInPc6d1	jarní
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
samec	samec	k1gInSc1	samec
ozývá	ozývat	k5eAaImIp3nS	ozývat
často	často	k6eAd1	často
už	už	k6eAd1	už
před	před	k7c7	před
setměním	setmění	k1gNnSc7	setmění
typickým	typický	k2eAgNnSc7d1	typické
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
houkáním	houkání	k1gNnSc7	houkání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nese	nést	k5eAaImIp3nS	nést
až	až	k9	až
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
si	se	k3xPyFc3	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hnízdění	hnízdění	k1gNnSc2	hnízdění
dokáže	dokázat	k5eAaPmIp3nS	dokázat
své	svůj	k3xOyFgNnSc4	svůj
teritorium	teritorium	k1gNnSc4	teritorium
srdnatě	srdnatě	k6eAd1	srdnatě
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
i	i	k8xC	i
vlhčích	vlhký	k2eAgInPc6d2	vlhčí
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Pobaltí	Pobaltí	k1gNnSc1	Pobaltí
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
severní	severní	k2eAgFnSc4d1	severní
Koreu	Korea	k1gFnSc4	Korea
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Izolované	izolovaný	k2eAgFnPc1d1	izolovaná
populace	populace	k1gFnPc1	populace
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
Karpatech	Karpaty	k1gInPc6	Karpaty
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc6	jejich
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
Alp	Alpy	k1gFnPc2	Alpy
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
předhůří	předhůří	k1gNnSc2	předhůří
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
oblastech	oblast	k1gFnPc6	oblast
<g/>
:	:	kIx,	:
jednak	jednak	k8xC	jednak
v	v	k7c6	v
pralesních	pralesní	k2eAgFnPc6d1	pralesní
rezervacích	rezervace	k1gFnPc6	rezervace
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
jeho	jeho	k3xOp3gNnSc1	jeho
hnízdění	hnízdění	k1gNnSc1	hnízdění
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Šumavy	Šumava	k1gFnSc2	Šumava
a	a	k8xC	a
Českého	český	k2eAgInSc2d1	český
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
vyhynulý	vyhynulý	k2eAgInSc4d1	vyhynulý
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
výsledek	výsledek	k1gInSc1	výsledek
bavorského	bavorský	k2eAgMnSc2d1	bavorský
i	i	k8xC	i
českého	český	k2eAgInSc2d1	český
reintrodukčního	reintrodukční	k2eAgInSc2d1	reintrodukční
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
se	se	k3xPyFc4	se
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Šumava	Šumava	k1gFnSc1	Šumava
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
odhadoval	odhadovat	k5eAaImAgMnS	odhadovat
na	na	k7c6	na
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
současný	současný	k2eAgInSc1d1	současný
stav	stav	k1gInSc1	stav
ovšem	ovšem	k9	ovšem
bude	být	k5eAaImBp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stanoviště	stanoviště	k1gNnSc2	stanoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
hnízdění	hnízdění	k1gNnSc4	hnízdění
této	tento	k3xDgFnSc2	tento
sovy	sova	k1gFnSc2	sova
mají	mít	k5eAaImIp3nP	mít
nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
roli	role	k1gFnSc4	role
staré	starý	k2eAgInPc4d1	starý
a	a	k8xC	a
přestárlé	přestárlý	k2eAgInPc4d1	přestárlý
stromy	strom	k1gInPc4	strom
i	i	k8xC	i
jejich	jejich	k3xOp3gNnPc4	jejich
polorozpadlá	polorozpadlý	k2eAgNnPc4d1	polorozpadlé
torza	torzo	k1gNnPc4	torzo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nepříznivých	příznivý	k2eNgNnPc6d1	nepříznivé
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
nabídka	nabídka	k1gFnSc1	nabídka
vhodné	vhodný	k2eAgFnSc2d1	vhodná
kořisti	kořist	k1gFnSc2	kořist
minimální	minimální	k2eAgFnPc1d1	minimální
<g/>
,	,	kIx,	,
páry	pár	k1gInPc1	pár
nemusejí	muset	k5eNaImIp3nP	muset
zahnízdit	zahnízdit	k5eAaPmF	zahnízdit
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středoevropských	středoevropský	k2eAgFnPc6d1	středoevropská
podmínkách	podmínka	k1gFnPc6	podmínka
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
lesích	les	k1gInPc6	les
nad	nad	k7c7	nad
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
má	mít	k5eAaImIp3nS	mít
zejména	zejména	k9	zejména
staré	starý	k2eAgFnPc4d1	stará
bučiny	bučina	k1gFnPc4	bučina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgMnSc1d2	veliký
<g/>
,	,	kIx,	,
mohutnější	mohutný	k2eAgMnSc1d2	mohutnější
a	a	k8xC	a
světlejší	světlý	k2eAgMnSc1d2	světlejší
puštík	puštík	k1gMnSc1	puštík
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
asi	asi	k9	asi
60	[number]	k4	60
cm	cm	kA	cm
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
zřetelně	zřetelně	k6eAd1	zřetelně
delší	dlouhý	k2eAgInSc1d2	delší
ocas	ocas	k1gInSc1	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
obvykle	obvykle	k6eAd1	obvykle
okolo	okolo	k7c2	okolo
120	[number]	k4	120
cm	cm	kA	cm
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
170	[number]	k4	170
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Zbarven	zbarven	k2eAgMnSc1d1	zbarven
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
nápadně	nápadně	k6eAd1	nápadně
světle	světle	k6eAd1	světle
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
tmavá	tmavý	k2eAgFnSc1d1	tmavá
varianta	varianta	k1gFnSc1	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Obličej	obličej	k1gInSc1	obličej
je	být	k5eAaImIp3nS	být
okrouhlý	okrouhlý	k2eAgInSc1d1	okrouhlý
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
černohnědé	černohnědý	k2eAgFnPc1d1	černohnědá
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
však	však	k9	však
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
větší	veliký	k2eAgFnPc1d2	veliký
mohutnosti	mohutnost	k1gFnPc1	mohutnost
(	(	kIx(	(
<g/>
samec	samec	k1gInSc1	samec
540	[number]	k4	540
<g/>
–	–	k?	–
<g/>
730	[number]	k4	730
g	g	kA	g
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
720	[number]	k4	720
<g/>
–	–	k?	–
<g/>
1200	[number]	k4	1200
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sezónní	sezónní	k2eAgFnPc1d1	sezónní
varianty	varianta	k1gFnPc1	varianta
zbarvení	zbarvení	k1gNnSc2	zbarvení
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
občas	občas	k6eAd1	občas
loví	lovit	k5eAaImIp3nP	lovit
i	i	k8xC	i
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
hlasový	hlasový	k2eAgInSc1d1	hlasový
projev	projev	k1gInSc1	projev
<g/>
:	:	kIx,	:
divoké	divoký	k2eAgNnSc1d1	divoké
"	"	kIx"	"
<g/>
hu-hauhauhau	huauhauhau	k6eAd1	hu-hauhauhau
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
vůhu-hu-hu	vůhuua	k1gFnSc4	vůhu-hu-ha
<g/>
"	"	kIx"	"
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
první	první	k4xOgFnSc4	první
slabiku	slabika	k1gFnSc4	slabika
(	(	kIx(	(
<g/>
trochu	trochu	k6eAd1	trochu
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
projevu	projev	k1gInSc2	projev
holuba	holub	k1gMnSc2	holub
hřivnáče	hřivnáč	k1gMnSc2	hřivnáč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
reaguje	reagovat	k5eAaBmIp3nS	reagovat
výkřiky	výkřik	k1gInPc4	výkřik
"	"	kIx"	"
<g/>
hé	hé	k0	hé
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
ve	v	k7c4	v
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
(	(	kIx(	(
<g/>
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
chrú-chru-chru	chrúhruhra	k1gFnSc4	chrú-chru-chra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nP	tvořit
hlodavci	hlodavec	k1gMnPc1	hlodavec
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
drobní	drobný	k2eAgMnPc1d1	drobný
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sova	sova	k1gFnSc1	sova
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nebojácná	bojácný	k2eNgFnSc1d1	nebojácná
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
urputně	urputně	k6eAd1	urputně
a	a	k8xC	a
agresivně	agresivně	k6eAd1	agresivně
bránit	bránit	k5eAaImF	bránit
své	svůj	k3xOyFgNnSc4	svůj
hnízdo	hnízdo	k1gNnSc4	hnízdo
před	před	k7c7	před
jakýmkoli	jakýkoli	k3yIgMnSc7	jakýkoli
vetřelcem	vetřelec	k1gMnSc7	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
dostal	dostat	k5eAaPmAgMnS	dostat
puštík	puštík	k1gMnSc1	puštík
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
povahový	povahový	k2eAgInSc4d1	povahový
rys	rys	k1gInSc4	rys
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
slaugula	slaugula	k1gFnSc1	slaugula
<g/>
"	"	kIx"	"
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
útočná	útočný	k2eAgFnSc1d1	útočná
sova	sova	k1gFnSc1	sova
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
nebo	nebo	k8xC	nebo
starých	starý	k2eAgNnPc6d1	staré
hnízdech	hnízdo	k1gNnPc6	hnízdo
dravců	dravec	k1gMnPc2	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
bílá	bílý	k2eAgNnPc4d1	bílé
vejce	vejce	k1gNnPc4	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgFnPc6	který
sedí	sedit	k5eAaImIp3nP	sedit
sama	sám	k3xTgMnSc4	sám
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
27	[number]	k4	27
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
ve	v	k7c6	v
středoevropských	středoevropský	k2eAgFnPc6d1	středoevropská
podmínkách	podmínka	k1gFnPc6	podmínka
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
polovině	polovina	k1gFnSc6	polovina
rozptylu	rozptyl	k1gInSc2	rozptyl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
krmí	krmit	k5eAaImIp3nP	krmit
rodiče	rodič	k1gMnPc4	rodič
asi	asi	k9	asi
5	[number]	k4	5
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
po	po	k7c6	po
vylétnutí	vylétnutí	k1gNnSc6	vylétnutí
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
asi	asi	k9	asi
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
přikrmují	přikrmovat	k5eAaImIp3nP	přikrmovat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
dalších	další	k2eAgMnPc2d1	další
puštíků	puštík	k1gMnPc2	puštík
rodiče	rodič	k1gMnPc1	rodič
své	svůj	k3xOyFgNnSc4	svůj
hnízdo	hnízdo	k1gNnSc4	hnízdo
urputně	urputně	k6eAd1	urputně
brání	bránit	k5eAaImIp3nP	bránit
před	před	k7c7	před
vetřelci	vetřelec	k1gMnPc7	vetřelec
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
napadnou	napadnout	k5eAaPmIp3nP	napadnout
i	i	k9	i
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruh	poddruh	k1gInSc1	poddruh
puštíka	puštík	k1gMnSc2	puštík
bělavého	bělavý	k2eAgMnSc2d1	bělavý
==	==	k?	==
</s>
</p>
<p>
<s>
Biologická	biologický	k2eAgFnSc1d1	biologická
systematika	systematika	k1gFnSc1	systematika
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
7	[number]	k4	7
až	až	k9	až
10	[number]	k4	10
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gFnSc2	uralensis
daurica	daurica	k6eAd1	daurica
</s>
</p>
<p>
<s>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gFnSc2	uralensis
fuscescens	fuscescens	k6eAd1	fuscescens
</s>
</p>
<p>
<s>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gFnSc2	uralensis
hondoensis	hondoensis	k1gFnSc2	hondoensis
</s>
</p>
<p>
<s>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gFnSc2	uralensis
japonica	japonica	k6eAd1	japonica
</s>
</p>
<p>
<s>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gInSc1	uralensis
liturata	liturata	k1gFnSc1	liturata
-	-	kIx~	-
puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
severoevropský	severoevropský	k2eAgMnSc5d1	severoevropský
</s>
</p>
<p>
<s>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gInSc1	uralensis
macroura	macroura	k1gFnSc1	macroura
-	-	kIx~	-
puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
</s>
</p>
<p>
<s>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gFnSc2	uralensis
momiyamae	momiyamaat	k5eAaPmIp3nS	momiyamaat
</s>
</p>
<p>
<s>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gFnSc2	uralensis
nikolskii	nikolskie	k1gFnSc3	nikolskie
</s>
</p>
<p>
<s>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gFnSc2	uralensis
uralensis	uralensis	k1gFnSc2	uralensis
-	-	kIx~	-
puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
západosibiřský	západosibiřský	k2eAgInSc1d1	západosibiřský
</s>
</p>
<p>
<s>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gFnSc2	uralensis
yenisseensis	yenisseensis	k1gFnSc2	yenisseensis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
severoevropský	severoevropský	k2eAgMnSc1d1	severoevropský
(	(	kIx(	(
<g/>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gInSc1	uralensis
liturata	liturata	k1gFnSc1	liturata
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
chován	chovat	k5eAaImNgInS	chovat
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Zoo	zoo	k1gFnSc1	zoo
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
uváděn	uvádět	k5eAaImNgMnS	uvádět
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgInSc1d1	bělavý
pobaltský	pobaltský	k2eAgMnSc1d1	pobaltský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
středoevropský	středoevropský	k2eAgMnSc1d1	středoevropský
(	(	kIx(	(
<g/>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gInSc1	uralensis
macroura	macroura	k1gFnSc1	macroura
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Centrum	centrum	k1gNnSc1	centrum
rozšíření	rozšíření	k1gNnSc2	rozšíření
tohoto	tento	k3xDgInSc2	tento
poddruhu	poddruh	k1gInSc2	poddruh
puštíka	puštík	k1gMnSc2	puštík
bělavého	bělavý	k2eAgMnSc2d1	bělavý
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
karpatském	karpatský	k2eAgInSc6d1	karpatský
oblouku	oblouk	k1gInSc6	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
areál	areál	k1gInSc1	areál
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
západním	západní	k2eAgInSc7d1	západní
výběžkem	výběžek	k1gInSc7	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
stavy	stav	k1gInPc4	stav
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
mírně	mírně	k6eAd1	mírně
stoupají	stoupat	k5eAaImIp3nP	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
populace	populace	k1gFnSc2	populace
slouží	sloužit	k5eAaImIp3nS	sloužit
program	program	k1gInSc4	program
zastřešený	zastřešený	k2eAgInSc4d1	zastřešený
Správou	správa	k1gFnSc7	správa
CHKO	CHKO	kA	CHKO
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
a	a	k8xC	a
Zoo	zoo	k1gFnSc1	zoo
Hluboká	Hluboká	k1gFnSc1	Hluboká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
i	i	k9	i
další	další	k2eAgFnSc2d1	další
zoologické	zoologický	k2eAgFnSc2d1	zoologická
stanice	stanice	k1gFnSc2	stanice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Zoo	zoo	k1gFnSc1	zoo
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
ZOO	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnPc1	zoo
Bojnice	Bojnice	k1gFnPc1	Bojnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
subjekty	subjekt	k1gInPc1	subjekt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Zoopark	zoopark	k1gInSc1	zoopark
Chomutov	Chomutov	k1gInSc1	Chomutov
a	a	k8xC	a
Stanice	stanice	k1gFnSc1	stanice
Pavlov	Pavlov	k1gInSc1	Pavlov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
západosibiřský	západosibiřský	k2eAgMnSc1d1	západosibiřský
(	(	kIx(	(
<g/>
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gFnSc2	uralensis
uralensis	uralensis	k1gFnSc2	uralensis
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Populace	populace	k1gFnSc1	populace
tohoto	tento	k3xDgInSc2	tento
poddruhu	poddruh	k1gInSc2	poddruh
čítá	čítat	k5eAaImIp3nS	čítat
na	na	k7c4	na
100	[number]	k4	100
až	až	k9	až
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
obývají	obývat	k5eAaImIp3nP	obývat
především	především	k6eAd1	především
západní	západní	k2eAgFnSc4d1	západní
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
areál	areál	k1gInSc1	areál
však	však	k9	však
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
na	na	k7c4	na
východ	východ	k1gInSc4	východ
až	až	k9	až
k	k	k7c3	k
Ochotskému	ochotský	k2eAgNnSc3d1	Ochotské
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Zástupce	zástupce	k1gMnSc1	zástupce
puštíka	puštík	k1gMnSc2	puštík
bělavého	bělavý	k2eAgNnSc2d1	bělavé
západosibiřského	západosibiřský	k2eAgNnSc2d1	západosibiřský
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
reprezentanti	reprezentant	k1gMnPc1	reprezentant
geneticky	geneticky	k6eAd1	geneticky
čistého	čistý	k2eAgInSc2d1	čistý
poddruhu	poddruh	k1gInSc2	poddruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Dungel	Dungel	k1gMnSc1	Dungel
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Hudes	Hudes	k1gMnSc1	Hudes
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
ptáků	pták	k1gMnPc2	pták
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
927	[number]	k4	927
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
150	[number]	k4	150
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HANZÁK	HANZÁK	kA	HANZÁK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
BOUCHNER	BOUCHNER	kA	BOUCHNER
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
;	;	kIx,	;
HUDEC	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Světem	svět	k1gInSc7	svět
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
69	[number]	k4	69
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SVENSSON	SVENSSON	kA	SVENSSON
<g/>
,	,	kIx,	,
Lars	Lars	k1gInSc1	Lars
<g/>
:	:	kIx,	:
Ptáci	pták	k1gMnPc1	pták
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Ševčík	Ševčík	k1gMnSc1	Ševčík
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
opravené	opravený	k2eAgNnSc1d1	opravené
a	a	k8xC	a
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7291-224-7	[number]	k4	978-80-7291-224-7
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Reintrodukce	Reintrodukce	k1gFnSc1	Reintrodukce
puštíka	puštík	k1gMnSc2	puštík
bělavého	bělavý	k2eAgInSc2d1	bělavý
středoevropského	středoevropský	k2eAgInSc2d1	středoevropský
</s>
</p>
<p>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
početnosti	početnost	k1gFnSc2	početnost
puštíka	puštík	k1gMnSc2	puštík
bělavého	bělavý	k2eAgMnSc2d1	bělavý
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Strix	Strix	k1gInSc1	Strix
uralensis	uralensis	k1gFnSc1	uralensis
(	(	kIx(	(
<g/>
puštík	puštík	k1gMnSc1	puštík
bělavý	bělavý	k2eAgMnSc1d1	bělavý
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlas	hlas	k1gInSc1	hlas
puštíka	puštík	k1gMnSc2	puštík
bělavého	bělavý	k2eAgMnSc2d1	bělavý
</s>
</p>
