<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
<g/>
,	,	kIx,	,
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
a	a	k8xC	a
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
skupin	skupina	k1gFnPc2	skupina
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prodala	prodat	k5eAaPmAgFnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
alb	alba	k1gFnPc2	alba
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přibližně	přibližně	k6eAd1	přibližně
74,5	[number]	k4	74,5
milionů	milion	k4xCgInPc2	milion
jen	jen	k6eAd1	jen
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
