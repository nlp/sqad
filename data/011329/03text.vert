<p>
<s>
Lord	lord	k1gMnSc1	lord
George	Georg	k1gInSc2	Georg
Francis	Francis	k1gInSc1	Francis
Hamilton	Hamilton	k1gInSc1	Hamilton
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1845	[number]	k4	1845
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
státník	státník	k1gMnSc1	státník
ze	z	k7c2	z
starobylého	starobylý	k2eAgInSc2d1	starobylý
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Hamiltonů	Hamilton	k1gInPc2	Hamilton
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
let	léto	k1gNnPc2	léto
poslancem	poslanec	k1gMnSc7	poslanec
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
ministr	ministr	k1gMnSc1	ministr
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
Indii	Indie	k1gFnSc4	Indie
patřil	patřit	k5eAaImAgMnS	patřit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
k	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
osobnostem	osobnost	k1gFnPc3	osobnost
britské	britský	k2eAgFnSc2d1	britská
koloniální	koloniální	k2eAgFnPc4d1	koloniální
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
třetím	třetí	k4xOgMnSc7	třetí
synem	syn	k1gMnSc7	syn
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Abercornu	Abercorn	k1gInSc2	Abercorn
<g/>
,	,	kIx,	,
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
Louise	Louis	k1gMnSc2	Louis
Jane	Jan	k1gMnSc5	Jan
Russell	Russell	k1gMnSc1	Russell
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
spřízněn	spřízněn	k2eAgInSc1d1	spřízněn
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Bedfordu	Bedford	k1gInSc2	Bedford
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nejstarší	starý	k2eAgMnSc1d3	nejstarší
bratr	bratr	k1gMnSc1	bratr
byl	být	k5eAaImAgMnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
vévodou	vévoda	k1gMnSc7	vévoda
z	z	k7c2	z
Abercornu	Abercorn	k1gInSc2	Abercorn
a	a	k8xC	a
zastával	zastávat	k5eAaImAgMnS	zastávat
čestné	čestný	k2eAgInPc4d1	čestný
posty	post	k1gInPc4	post
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
bratr	bratr	k1gMnSc1	bratr
Claud	Claud	k1gMnSc1	Claud
John	John	k1gMnSc1	John
Hamilton	Hamilton	k1gInSc1	Hamilton
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
vévody	vévoda	k1gMnSc2	vévoda
užíval	užívat	k5eAaImAgInS	užívat
titul	titul	k1gInSc1	titul
lorda	lord	k1gMnSc2	lord
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
hodnosti	hodnost	k1gFnSc3	hodnost
poručíka	poručík	k1gMnSc2	poručík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
Konzervativní	konzervativní	k2eAgFnSc4d1	konzervativní
stranu	strana	k1gFnSc4	strana
poprvé	poprvé	k6eAd1	poprvé
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
za	za	k7c4	za
hrabství	hrabství	k1gNnSc4	hrabství
Middlesex	Middlesex	k1gInSc1	Middlesex
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
uspěl	uspět	k5eAaPmAgMnS	uspět
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgFnSc7d1	volební
reformou	reforma	k1gFnSc7	reforma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
byl	být	k5eAaImAgInS	být
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
Middlesex	Middlesex	k1gInSc1	Middlesex
rozdělen	rozdělen	k2eAgInSc1d1	rozdělen
na	na	k7c4	na
šest	šest	k4xCc4	šest
okrsků	okrsek	k1gInPc2	okrsek
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
Ealing	Ealing	k1gInSc4	Ealing
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
západní	západní	k2eAgInSc1d1	západní
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
lord	lord	k1gMnSc1	lord
Hamilton	Hamilton	k1gInSc4	Hamilton
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
zahájil	zahájit	k5eAaPmAgMnS	zahájit
jako	jako	k9	jako
státní	státní	k2eAgMnSc1d1	státní
podsekretář	podsekretář	k1gMnSc1	podsekretář
pro	pro	k7c4	pro
Indii	Indie	k1gFnSc4	Indie
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
byl	být	k5eAaImAgInS	být
podřízeným	podřízený	k2eAgMnSc7d1	podřízený
a	a	k8xC	a
blízkým	blízký	k2eAgMnSc7d1	blízký
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
markýze	markýz	k1gMnSc2	markýz
ze	z	k7c2	z
Salisbury	Salisbura	k1gFnSc2	Salisbura
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zasedal	zasedat	k5eAaImAgInS	zasedat
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
lordů	lord	k1gMnPc2	lord
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Hamilton	Hamilton	k1gInSc4	Hamilton
byl	být	k5eAaImAgMnS	být
mluvčím	mluvčí	k1gMnSc7	mluvčí
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
pro	pro	k7c4	pro
Indii	Indie	k1gFnSc4	Indie
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Tajné	tajný	k2eAgFnSc2d1	tajná
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1880	[number]	k4	1880
byl	být	k5eAaImAgMnS	být
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
výboru	výbor	k1gInSc2	výbor
Tajné	tajný	k2eAgFnSc2d1	tajná
rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
školství	školství	k1gNnSc4	školství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konzervativních	konzervativní	k2eAgFnPc6d1	konzervativní
vládách	vláda	k1gFnPc6	vláda
markýze	markýz	k1gMnSc2	markýz
ze	z	k7c2	z
Salisbury	Salisbura	k1gFnSc2	Salisbura
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1886	[number]	k4	1886
a	a	k8xC	a
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
ministrem	ministr	k1gMnSc7	ministr
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1903	[number]	k4	1903
státním	státní	k2eAgInSc7d1	státní
sekretářem	sekretář	k1gInSc7	sekretář
pro	pro	k7c4	pro
Indii	Indie	k1gFnSc4	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
odešli	odejít	k5eAaPmAgMnP	odejít
z	z	k7c2	z
kabinetu	kabinet	k1gInSc2	kabinet
také	také	k9	také
ministr	ministr	k1gMnSc1	ministr
kolonií	kolonie	k1gFnPc2	kolonie
Joseph	Joseph	k1gMnSc1	Joseph
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
T.	T.	kA	T.
Ritchie	Ritchie	k1gFnSc2	Ritchie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
pro	pro	k7c4	pro
Indii	Indie	k1gFnSc4	Indie
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Hamiltona	Hamilton	k1gMnSc2	Hamilton
9	[number]	k4	9
<g/>
.	.	kIx.	.
vikomt	vikomt	k1gMnSc1	vikomt
Midleton	Midleton	k1gInSc4	Midleton
<g/>
,	,	kIx,	,
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
ministr	ministr	k1gMnSc1	ministr
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Získal	získat	k5eAaPmAgInS	získat
čestné	čestný	k2eAgInPc4d1	čestný
doktoráty	doktorát	k1gInPc4	doktorát
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
a	a	k8xC	a
Glasgow	Glasgow	k1gNnSc1	Glasgow
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
byl	být	k5eAaImAgInS	být
smírčím	smírčí	k2eAgMnSc7d1	smírčí
soudcem	soudce	k1gMnSc7	soudce
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
a	a	k8xC	a
Middlesexu	Middlesex	k1gInSc6	Middlesex
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1895	[number]	k4	1895
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
školské	školský	k2eAgFnSc2d1	školská
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Maud	Maud	k1gInSc4	Maud
Lascelles	Lascellesa	k1gFnPc2	Lascellesa
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
3	[number]	k4	3
<g/>
.	.	kIx.	.
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Harewoodu	Harewood	k1gInSc2	Harewood
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
Ronald	Ronald	k1gMnSc1	Ronald
James	James	k1gMnSc1	James
Hamilton	Hamilton	k1gInSc1	Hamilton
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
diplomacii	diplomacie	k1gFnSc6	diplomacie
<g/>
,	,	kIx,	,
druhorozený	druhorozený	k2eAgInSc1d1	druhorozený
Anthony	Anthon	k1gMnPc7	Anthon
George	George	k1gNnPc2	George
Hamilton	Hamilton	k1gInSc1	Hamilton
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
Robert	Robert	k1gMnSc1	Robert
Cecil	Cecil	k1gMnSc1	Cecil
Hamilton	Hamilton	k1gInSc1	Hamilton
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
kmotrem	kmotr	k1gMnSc7	kmotr
byl	být	k5eAaImAgInS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
markýz	markýza	k1gFnPc2	markýza
ze	z	k7c2	z
Salisbury	Salisbura	k1gFnSc2	Salisbura
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
hodnosti	hodnost	k1gFnPc4	hodnost
viceadmirála	viceadmirál	k1gMnSc2	viceadmirál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Rodokmen	rodokmen	k1gInSc1	rodokmen
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
AbercornuGeorge	AbercornuGeorge	k1gNnSc2	AbercornuGeorge
Francis	Francis	k1gFnSc2	Francis
Hamilton	Hamilton	k1gInSc1	Hamilton
</s>
</p>
