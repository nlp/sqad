<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
známým	známý	k2eAgInSc7d1	známý
jazykem	jazyk	k1gInSc7	jazyk
řecké	řecký	k2eAgFnSc2d1	řecká
jazykové	jazykový	k2eAgFnSc2d1	jazyková
větve	větev	k1gFnSc2	větev
je	být	k5eAaImIp3nS	být
mykénština	mykénština	k1gFnSc1	mykénština
(	(	kIx(	(
<g/>
Kréta	Kréta	k1gFnSc1	Kréta
a	a	k8xC	a
pevninské	pevninský	k2eAgNnSc1d1	pevninské
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
zapisována	zapisovat	k5eAaImNgFnS	zapisovat
pomocí	pomocí	k7c2	pomocí
dosud	dosud	k6eAd1	dosud
nerozluštěného	rozluštěný	k2eNgNnSc2d1	nerozluštěné
lineárního	lineární	k2eAgNnSc2d1	lineární
písma	písmo	k1gNnSc2	písmo
A	A	kA	A
<g/>
.	.	kIx.	.
</s>
