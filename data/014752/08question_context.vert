<s>
Švýcarský	švýcarský	k2eAgInSc1d1
frank	frank	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Schweizer	Schweizer	k1gInSc1
Franken	Frankna	k1gFnPc2
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
franc	franc	k6eAd1
suisse	suisse	k6eAd1
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
franco	franco	k6eAd1
svizzero	svizzero	k1gNnSc1
<g/>
,	,	kIx,
rétorománsky	rétorománsky	k6eAd1
franc	franc	k1gInSc1
svizzer	svizzer	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
100	#num#	k4
rappů	rapp	k1gInPc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Rappen	Rappen	k2eAgInSc1d1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
centimes	centimes	k1gInSc1
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
centisimi	centisi	k1gFnPc7
<g/>
,	,	kIx,
rétorománsky	rétorománsky	k6eAd1
raps	raps	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>