<s>
Švýcarský	švýcarský	k2eAgInSc1d1
frank	frank	k1gInSc1
</s>
<s>
Švýcarský	švýcarský	k2eAgInSc1d1
frank	frank	k1gInSc1
švýcarské	švýcarský	k2eAgFnSc2d1
minceZemě	minceZemě	k6eAd1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
ŠvýcarskoLichtenštejnsko	ŠvýcarskoLichtenštejnsko	k1gNnSc1
LichtenštejnskoItálie	LichtenštejnskoItálie	k1gFnSc2
Itálie	Itálie	k1gFnSc2
(	(	kIx(
<g/>
Campione	Campion	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Italia	Italia	k1gFnSc1
<g/>
)	)	kIx)
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
CHF	CHF	kA
Inflace	inflace	k1gFnSc1
</s>
<s>
−	−	k?
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2015	#num#	k4
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
rapp	rapp	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
rappů	rapp	k1gInPc2
<g/>
;	;	kIx,
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
franků	frank	k1gInPc2
Bankovky	bankovka	k1gFnSc2
</s>
<s>
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
a	a	k8xC
1000	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
Švýcarský	švýcarský	k2eAgInSc1d1
frank	frank	k1gInSc1
je	být	k5eAaImIp3nS
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
ve	v	k7c6
dvou	dva	k4xCgInPc6
alpských	alpský	k2eAgInPc6d1
státech	stát	k1gInPc6
–	–	k?
Švýcarsku	Švýcarsko	k1gNnSc6
a	a	k8xC
Lichtenštejnsku	Lichtenštejnsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgNnSc7d1
oficiálním	oficiální	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
italské	italský	k2eAgFnSc2d1
enklávy	enkláva	k1gFnSc2
na	na	k7c6
švýcarském	švýcarský	k2eAgNnSc6d1
území	území	k1gNnSc6
–	–	k?
Campione	Campion	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Italia	Italius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
Itálie	Itálie	k1gFnSc1
vyjma	vyjma	k7c2
Campione	Campion	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Italia	Italia	k1gFnSc1
používá	používat	k5eAaImIp3nS
euro	euro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiná	jiný	k2eAgFnSc1d1
enkláva	enkláva	k1gFnSc1
na	na	k7c6
švýcarském	švýcarský	k2eAgNnSc6d1
území	území	k1gNnSc6
–	–	k?
německý	německý	k2eAgInSc1d1
Büsingen	Büsingen	k1gInSc1
am	am	k?
Hochrhein	Hochrhein	k1gInSc1
–	–	k?
má	mít	k5eAaImIp3nS
formálně	formálně	k6eAd1
jako	jako	k9
jedinou	jediný	k2eAgFnSc4d1
měnu	měna	k1gFnSc4
euro	euro	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
švýcarský	švýcarský	k2eAgInSc1d1
frank	frank	k1gInSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
oblíbený	oblíbený	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
každodenním	každodenní	k2eAgInSc6d1
platebním	platební	k2eAgInSc6d1
styku	styk	k1gInSc6
je	být	k5eAaImIp3nS
častěji	často	k6eAd2
používaný	používaný	k2eAgMnSc1d1
než	než	k8xS
euro	euro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
se	s	k7c7
Švýcarskem	Švýcarsko	k1gNnSc7
v	v	k7c6
celní	celní	k2eAgFnSc6d1
a	a	k8xC
měnové	měnový	k2eAgFnSc3d1
unii	unie	k1gFnSc3
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
jako	jako	k8xS,k8xC
svou	svůj	k3xOyFgFnSc4
měnu	měna	k1gFnSc4
od	od	k7c2
roku	rok	k1gInSc2
1921	#num#	k4
a	a	k8xC
má	mít	k5eAaImIp3nS
podle	podle	k7c2
smlouvy	smlouva	k1gFnSc2
se	s	k7c7
Švýcarskem	Švýcarsko	k1gNnSc7
z	z	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
právo	právo	k1gNnSc4
razit	razit	k5eAaImF
mince	mince	k1gFnSc1
švýcarského	švýcarský	k2eAgInSc2d1
franku	frank	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nemůže	moct	k5eNaImIp3nS
vydávat	vydávat	k5eAaPmF,k5eAaImF
bankovky	bankovka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
zavedlo	zavést	k5eAaPmAgNnS
frank	frank	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
místo	místo	k1gNnSc4
rakouské	rakouský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
používalo	používat	k5eAaImAgNnS
od	od	k7c2
roku	rok	k1gInSc2
1918	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
nestabilní	stabilní	k2eNgFnSc7d1
měnou	měna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
ní	on	k3xPp3gFnSc7
používalo	používat	k5eAaImAgNnS
jako	jako	k9
svou	svůj	k3xOyFgFnSc4
měnu	měna	k1gFnSc4
rakousko-uherskou	rakousko-uherský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
(	(	kIx(
<g/>
od	od	k7c2
1898	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Švýcarský	švýcarský	k2eAgInSc1d1
frank	frank	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Schweizer	Schweizer	k1gInSc1
Franken	Frankna	k1gFnPc2
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
franc	franc	k6eAd1
suisse	suisse	k6eAd1
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
franco	franco	k6eAd1
svizzero	svizzero	k1gNnSc1
<g/>
,	,	kIx,
rétorománsky	rétorománsky	k6eAd1
franc	franc	k1gInSc1
svizzer	svizzer	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
100	#num#	k4
rappů	rapp	k1gInPc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Rappen	Rappen	k2eAgInSc1d1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
centimes	centimes	k1gInSc1
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
centisimi	centisi	k1gFnPc7
<g/>
,	,	kIx,
rétorománsky	rétorománsky	k6eAd1
raps	raps	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc1
je	být	k5eAaImIp3nS
CHF	CHF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
„	„	k?
<g/>
frank	frank	k1gInSc1
<g/>
“	“	k?
má	mít	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
měna	měna	k1gFnSc1
shodný	shodný	k2eAgInSc4d1
s	s	k7c7
několika	několik	k4yIc7
měnami	měna	k1gFnPc7
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
Tichém	tichý	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
</s>
<s>
Mince	mince	k1gFnSc1
švýcarského	švýcarský	k2eAgInSc2d1
franku	frank	k1gInSc2
mají	mít	k5eAaImIp3nP
hodnoty	hodnota	k1gFnSc2
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
a	a	k8xC
20	#num#	k4
rappů	rapp	k1gInPc2
<g/>
;	;	kIx,
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
a	a	k8xC
5	#num#	k4
franků	frank	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Bankovky	bankovka	k1gFnPc1
v	v	k7c6
oběhu	oběh	k1gInSc6
mají	mít	k5eAaImIp3nP
hodnoty	hodnota	k1gFnSc2
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
a	a	k8xC
1000	#num#	k4
franků	frank	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
VyobrazeníHodnotaTechnické	VyobrazeníHodnotaTechnický	k2eAgInPc1d1
parametry	parametr	k1gInPc1
</s>
<s>
MateriálHmotnost	MateriálHmotnost	k1gFnSc1
[	[	kIx(
<g/>
g	g	kA
<g/>
]	]	kIx)
<g/>
Průměr	průměr	k1gInSc1
[	[	kIx(
<g/>
mm	mm	kA
<g/>
]	]	kIx)
<g/>
Tloušťka	tloušťka	k1gFnSc1
[	[	kIx(
<g/>
mm	mm	kA
<g/>
]	]	kIx)
</s>
<s>
5	#num#	k4
rappů	rapp	k1gInPc2
<g/>
92	#num#	k4
%	%	kIx~
měď	měď	k1gFnSc1
<g/>
,	,	kIx,
6	#num#	k4
%	%	kIx~
hliník	hliník	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
%	%	kIx~
nikl	nikl	k1gInSc1
<g/>
1,817,151,25	1,817,151,25	k4
</s>
<s>
10	#num#	k4
rappů	rapp	k1gInPc2
<g/>
75	#num#	k4
%	%	kIx~
měď	měď	k1gFnSc1
<g/>
,25	,25	k4
%	%	kIx~
nikl	nikl	k1gInSc1
<g/>
3,019,151,45	3,019,151,45	k4
</s>
<s>
20	#num#	k4
rappů	rapp	k1gInPc2
<g/>
4,021,051,65	4,021,051,65	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
franku	frank	k1gInSc2
<g/>
2,218,201,25	2,218,201,25	k4
</s>
<s>
1	#num#	k4
frank	frank	k1gInSc1
<g/>
4,423,201,55	4,423,201,55	k4
</s>
<s>
2	#num#	k4
franky	frank	k1gInPc4
<g/>
8,827,402,15	8,827,402,15	k4
</s>
<s>
5	#num#	k4
franků	frank	k1gInPc2
<g/>
13,231,452,35	13,231,452,35	k4
</s>
<s>
Osmá	osmý	k4xOgFnSc1
série	série	k1gFnSc1
bankovek	bankovka	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Všechny	všechen	k3xTgFnPc1
bankovky	bankovka	k1gFnPc1
jsou	být	k5eAaImIp3nP
stále	stále	k6eAd1
platné	platný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
bankovky	bankovka	k1gFnPc1
jsou	být	k5eAaImIp3nP
postupně	postupně	k6eAd1
stahovány	stahovat	k5eAaImNgInP
z	z	k7c2
oběhu	oběh	k1gInSc2
a	a	k8xC
nahrazovány	nahrazovat	k5eAaImNgFnP
novými	nový	k2eAgFnPc7d1
z	z	k7c2
deváté	devátý	k4xOgFnSc2
série	série	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
VyobrazeníHodnotaRozměryBarvaVyobrazená	VyobrazeníHodnotaRozměryBarvaVyobrazený	k2eAgFnSc1d1
osobnostV	osobnostV	k?
oběhu	oběh	k1gInSc2
od	od	k7c2
</s>
<s>
LícRub	LícRub	k1gMnSc1
</s>
<s>
10	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
126	#num#	k4
×	×	k?
74	#num#	k4
mm	mm	kA
</s>
<s>
Žlutá	žlutat	k5eAaImIp3nS
</s>
<s>
Le	Le	k?
Corbusier	Corbusier	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1997	#num#	k4
</s>
<s>
20	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
137	#num#	k4
×	×	k?
74	#num#	k4
mm	mm	kA
</s>
<s>
Červená	červený	k2eAgFnSc1d1
</s>
<s>
Arthur	Arthur	k1gMnSc1
Honegger	Honegger	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1996	#num#	k4
</s>
<s>
50	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
148	#num#	k4
×	×	k?
74	#num#	k4
mm	mm	kA
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Sophie	Sophie	k1gFnSc1
Taeuber-Arp	Taeuber-Arp	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1995	#num#	k4
</s>
<s>
100	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
159	#num#	k4
×	×	k?
74	#num#	k4
mm	mm	kA
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Alberto	Alberta	k1gFnSc5
Giacometti	Giacometť	k1gFnSc5
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1998	#num#	k4
</s>
<s>
200	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
170	#num#	k4
×	×	k?
74	#num#	k4
mm	mm	kA
</s>
<s>
Hnědá	hnědat	k5eAaImIp3nS
</s>
<s>
Charles	Charles	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Ramuz	Ramuz	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1997	#num#	k4
</s>
<s>
1000	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
181	#num#	k4
×	×	k?
74	#num#	k4
mm	mm	kA
</s>
<s>
Fialová	Fialová	k1gFnSc1
</s>
<s>
Jacob	Jacoba	k1gFnPc2
Burckhardt	Burckhardta	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1998	#num#	k4
</s>
<s>
Devátá	devátý	k4xOgFnSc1
série	série	k1gFnSc1
bankovek	bankovka	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
VyobrazeníHodnotaRozměryMotivV	VyobrazeníHodnotaRozměryMotivV	k?
oběhu	oběh	k1gInSc2
od	od	k7c2
</s>
<s>
LícRub	LícRub	k1gMnSc1
</s>
<s>
10	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
123	#num#	k4
×	×	k?
70	#num#	k4
mm	mm	kA
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
20	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
130	#num#	k4
×	×	k?
70	#num#	k4
mm	mm	kA
</s>
<s>
Světlo	světlo	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
50	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
137	#num#	k4
×	×	k?
70	#num#	k4
mm	mm	kA
</s>
<s>
Vítr	vítr	k1gInSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
100	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
144	#num#	k4
×	×	k?
70	#num#	k4
mm	mm	kA
</s>
<s>
Voda	voda	k1gFnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
200	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
151	#num#	k4
×	×	k?
70	#num#	k4
mm	mm	kA
</s>
<s>
Hmota	hmota	k1gFnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1000	#num#	k4
franků	frank	k1gInPc2
</s>
<s>
158	#num#	k4
×	×	k?
70	#num#	k4
mm	mm	kA
</s>
<s>
Řeč	řeč	k1gFnSc1
<g/>
/	/	kIx~
<g/>
komunikace	komunikace	k1gFnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1
kurz	kurz	k1gInSc1
měny	měna	k1gFnSc2
Švýcarský	švýcarský	k2eAgInSc1d1
frank	frank	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Aktuální	aktuální	k2eAgInSc1d1
kurz	kurz	k1gInSc1
měny	měna	k1gFnSc2
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
Podle	podle	k7c2
ČNB	ČNB	kA
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
Podle	podle	k7c2
Google	Google	k1gFnSc2
Finance	finance	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
(	(	kIx(
<g/>
Graf	graf	k1gInSc1
Banky	banka	k1gFnSc2
<g/>
)	)	kIx)
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Yahoo	Yahoo	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Finance	finance	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1553	#num#	k4
<g/>
-	-	kIx~
<g/>
8133	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SNB	SNB	kA
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Gestaltung	Gestaltung	k1gMnSc1
der	drát	k5eAaImRp2nS
neuen	neuna	k1gFnPc2
Banknoten	Banknoten	k2eAgInSc1d1
auf	auf	k?
einen	einen	k2eAgInSc1d1
Blick	Blick	k1gInSc1
-	-	kIx~
Die	Die	k1gFnSc1
10	#num#	k4
<g/>
-Franken-Note	-Franken-Not	k1gInSc5
<g/>
↑	↑	k?
SNB	SNB	kA
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Gestaltung	Gestaltung	k1gMnSc1
der	drát	k5eAaImRp2nS
neuen	neuna	k1gFnPc2
Banknoten	Banknoten	k2eAgInSc1d1
auf	auf	k?
einen	einen	k2eAgInSc1d1
Blick	Blick	k1gInSc1
-	-	kIx~
Die	Die	k1gFnSc1
20	#num#	k4
<g/>
-Franken-Note	-Franken-Not	k1gInSc5
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
SNB	SNB	kA
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Gestaltung	Gestaltung	k1gMnSc1
der	drát	k5eAaImRp2nS
neuen	neuna	k1gFnPc2
Banknoten	Banknoten	k2eAgInSc1d1
auf	auf	k?
einen	einen	k2eAgInSc1d1
Blick	Blick	k1gInSc1
-	-	kIx~
Die	Die	k1gFnSc1
50	#num#	k4
<g/>
-Franken-Note	-Franken-Not	k1gInSc5
<g/>
↑	↑	k?
SNB	SNB	kA
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Gestaltung	Gestaltung	k1gMnSc1
der	drát	k5eAaImRp2nS
neuen	neuna	k1gFnPc2
Banknoten	Banknoten	k2eAgInSc1d1
auf	auf	k?
einen	einen	k2eAgInSc1d1
Blick	Blick	k1gInSc1
-	-	kIx~
Die	Die	k1gFnSc1
100	#num#	k4
<g/>
-Franken-Note	-Franken-Not	k1gInSc5
<g/>
↑	↑	k?
SNB	SNB	kA
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Gestaltung	Gestaltung	k1gMnSc1
der	drát	k5eAaImRp2nS
neuen	neuna	k1gFnPc2
Banknoten	Banknoten	k2eAgInSc1d1
auf	auf	k?
einen	einen	k2eAgInSc1d1
Blick	Blick	k1gInSc1
-	-	kIx~
Die	Die	k1gFnSc1
200	#num#	k4
<g/>
-Franken-Note	-Franken-Not	k1gInSc5
<g/>
↑	↑	k?
SNB	SNB	kA
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Gestaltung	Gestaltung	k1gMnSc1
der	drát	k5eAaImRp2nS
neuen	neuna	k1gFnPc2
Banknoten	Banknoten	k2eAgInSc1d1
auf	auf	k?
einen	einen	k2eAgInSc1d1
Blick	Blick	k1gInSc1
-	-	kIx~
Die	Die	k1gFnSc1
1000	#num#	k4
<g/>
-Franken-Note	-Franken-Not	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
Vyobrazení	vyobrazení	k1gNnSc4
současných	současný	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
místní	místní	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
Vyobrazení	vyobrazení	k1gNnSc4
současných	současný	k2eAgFnPc2d1
bankovek	bankovka	k1gFnPc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
místní	místní	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
Švýcarsko	Švýcarsko	k1gNnSc4
(	(	kIx(
<g/>
katalog	katalog	k1gInSc1
a	a	k8xC
galerie	galerie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Historické	historický	k2eAgFnPc4d1
a	a	k8xC
současné	současný	k2eAgFnPc4d1
bankovky	bankovka	k1gFnPc4
Švýcarska	Švýcarsko	k1gNnSc2
</s>
<s>
Měny	měna	k1gFnPc1
Evropy	Evropa	k1gFnSc2
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1
lev	lev	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Chorvatská	chorvatský	k2eAgFnSc1d1
kuna	kuna	k1gFnSc1
•	•	k?
Euro	euro	k1gNnSc1
•	•	k?
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
•	•	k?
Rumunský	rumunský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Moldavský	moldavský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Podněsterský	podněsterský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
<g/>
)	)	kIx)
Jižní	jižní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánský	albánský	k2eAgInSc1d1
lek	lek	k1gInSc1
•	•	k?
Bosenskohercegovská	bosenskohercegovský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Makedonský	makedonský	k2eAgInSc1d1
denár	denár	k1gInSc1
•	•	k?
Srbský	srbský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
•	•	k?
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Islandská	islandský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
</s>
<s>
Globální	globální	k2eAgFnSc2d1
rezervní	rezervní	k2eAgFnSc2d1
měny	měna	k1gFnSc2
</s>
<s>
Americký	americký	k2eAgInSc1d1
dolar	dolar	k1gInSc1
(	(	kIx(
<g/>
USD	USD	kA
<g/>
)	)	kIx)
•	•	k?
Euro	euro	k1gNnSc1
(	(	kIx(
<g/>
EUR	euro	k1gNnPc2
<g/>
)	)	kIx)
•	•	k?
Čínský	čínský	k2eAgInSc4d1
jüan	jüan	k1gInSc4
(	(	kIx(
<g/>
CNY	CNY	kA
<g/>
)	)	kIx)
•	•	k?
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
(	(	kIx(
<g/>
GBP	GBP	kA
<g/>
)	)	kIx)
•	•	k?
Japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
(	(	kIx(
<g/>
JPY	JPY	kA
<g/>
)	)	kIx)
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
(	(	kIx(
<g/>
CHF	CHF	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4180480-6	4180480-6	k4
</s>
