<s>
Budováním	budování	k1gNnSc7	budování
korpusů	korpus	k1gInPc2	korpus
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
zabývá	zabývat	k5eAaImIp3nS	zabývat
Ústav	ústav	k1gInSc1	ústav
Českého	český	k2eAgInSc2d1	český
národního	národní	k2eAgInSc2d1	národní
korpusu	korpus	k1gInSc2	korpus
(	(	kIx(	(
<g/>
ÚČNK	ÚČNK	kA	ÚČNK
<g/>
)	)	kIx)	)
při	při	k7c6	při
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
UK	UK	kA	UK
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
založil	založit	k5eAaPmAgMnS	založit
lingvista	lingvista	k1gMnSc1	lingvista
František	František	k1gMnSc1	František
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
</s>
