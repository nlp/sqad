<s>
Vědci	vědec	k1gMnPc1	vědec
však	však	k9	však
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
kaloňů	kaloň	k1gMnPc2	kaloň
a	a	k8xC	a
netopýrů	netopýr	k1gMnPc2	netopýr
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
ale	ale	k9	ale
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
žádné	žádný	k3yNgFnPc4	žádný
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
komplikace	komplikace	k1gFnPc4	komplikace
<g/>
.	.	kIx.	.
</s>
