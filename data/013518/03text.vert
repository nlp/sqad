<s>
Helena	Helena	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
</s>
<s>
Helena	Helena	k1gFnSc1
BavorskáPrincezna	BavorskáPrincezna	k1gFnSc1
z	z	k7c2
Thurnu	Thurn	k1gInSc2
a	a	k8xC
Taxisu	taxis	k1gInSc2
Helena	Helena	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
Manžel	manžel	k1gMnSc1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
Antonín	Antonín	k1gMnSc1
z	z	k7c2
Thurnu	Thurn	k1gInSc2
a	a	k8xC
Taxisu	taxis	k1gInSc2
Úplné	úplný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Helene	Helen	k1gMnSc5
Caroline	Carolin	k1gInSc5
Therese	Therese	k1gFnPc5
Narození	narození	k1gNnPc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc4
1834	#num#	k4
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
Bavorské	bavorský	k2eAgNnSc1d1
království	království	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1890	#num#	k4
<g/>
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
56	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Řezno	Řezno	k1gNnSc1
<g/>
,	,	kIx,
Bavorské	bavorský	k2eAgNnSc1d1
království	království	k1gNnSc1
Pohřbena	pohřben	k2eAgFnSc1d1
</s>
<s>
Gruftkapelle	Gruftkapelle	k1gNnSc1
<g/>
,	,	kIx,
Řezno	Řezno	k1gNnSc1
Potomci	potomek	k1gMnPc5
</s>
<s>
Albert	Albert	k1gMnSc1
z	z	k7c2
Thurn-Taxisu	Thurn-Taxis	k1gInSc2
<g/>
,	,	kIx,
Luisa	Luisa	k1gFnSc1
z	z	k7c2
Thurnu	Thurn	k1gInSc2
a	a	k8xC
Taxisu	taxis	k1gInSc2
<g/>
,	,	kIx,
Alžběta	Alžběta	k1gFnSc1
z	z	k7c2
Thurnu	Thurn	k1gInSc2
a	a	k8xC
Taxisu	taxis	k1gInSc2
a	a	k8xC
Maxmilián	Maxmilián	k1gMnSc1
Maria	Maria	k1gFnSc1
z	z	k7c2
Thurnu	Thurn	k1gInSc2
a	a	k8xC
Taxisu	taxis	k1gInSc2
Dynastie	dynastie	k1gFnSc2
</s>
<s>
Wittelsbachové	Wittelsbachové	k2eAgMnSc1d1
Otec	otec	k1gMnSc1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
Matka	matka	k1gFnSc1
</s>
<s>
Ludovika	Ludovika	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Helena	Helena	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Tereza	Tereza	k1gFnSc1
<g/>
,	,	kIx,
zvaná	zvaný	k2eAgNnPc1d1
Néné	Néné	k1gFnPc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1834	#num#	k4
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1890	#num#	k4
<g/>
,	,	kIx,
Řezno	Řezno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bavorská	bavorský	k2eAgFnSc1d1
vévodkyně	vévodkyně	k1gFnSc1
a	a	k8xC
princezna	princezna	k1gFnSc1
<g/>
,	,	kIx,
sňatkem	sňatek	k1gInSc7
se	se	k3xPyFc4
dočasně	dočasně	k6eAd1
stala	stát	k5eAaPmAgFnS
hlavou	hlava	k1gFnSc7
domu	dům	k1gInSc2
Thurn-Taxis	Thurn-Taxis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mladá	mladý	k2eAgNnPc1d1
léta	léto	k1gNnPc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
jako	jako	k9
třetí	třetí	k4xOgMnSc1
potomek	potomek	k1gMnSc1
bavorského	bavorský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
Maxe	Max	k1gMnSc2
Josefa	Josef	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
princezny	princezna	k1gFnSc2
Ludoviky	Ludovika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
manželství	manželství	k1gNnSc1
ale	ale	k9
nebylo	být	k5eNaImAgNnS
šťastné	šťastný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vévoda	vévoda	k1gMnSc1
Max	Max	k1gMnSc1
byl	být	k5eAaImAgMnS
veselý	veselý	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
mnoho	mnoho	k6eAd1
cestoval	cestovat	k5eAaImAgMnS
<g/>
,	,	kIx,
dával	dávat	k5eAaImAgMnS
přednost	přednost	k1gFnSc4
společnosti	společnost	k1gFnSc2
obyčejných	obyčejný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
a	a	k8xC
nenechal	nechat	k5eNaPmAgMnS
na	na	k7c6
pokoji	pokoj	k1gInSc6
žádnou	žádný	k3yNgFnSc4
sukni	sukně	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
manželka	manželka	k1gFnSc1
Ludovika	Ludovika	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
jako	jako	k9
královská	královský	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
a	a	k8xC
domluvený	domluvený	k2eAgInSc1d1
sňatek	sňatek	k1gInSc1
s	s	k7c7
požitkářským	požitkářský	k2eAgMnSc7d1
Maxem	Max	k1gMnSc7
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
rozhodně	rozhodně	k6eAd1
nezamlouval	zamlouvat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
manželé	manžel	k1gMnPc1
byli	být	k5eAaImAgMnP
velmi	velmi	k6eAd1
rozdílní	rozdílný	k2eAgMnPc1d1
<g/>
,	,	kIx,
Ludovika	Ludovika	k1gFnSc1
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
svým	svůj	k3xOyFgFnPc3
dětem	dítě	k1gFnPc3
vštípit	vštípit	k5eAaPmF
nějaké	nějaký	k3yIgFnPc4
vědomosti	vědomost	k1gFnPc4
(	(	kIx(
<g/>
podle	podle	k7c2
otcova	otcův	k2eAgInSc2d1
vzoru	vzor	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
vévoda	vévoda	k1gMnSc1
Max	Max	k1gMnSc1
dělal	dělat	k5eAaImAgMnS
vše	všechen	k3xTgNnSc4
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
to	ten	k3xDgNnSc1
nepodařilo	podařit	k5eNaPmAgNnS
(	(	kIx(
<g/>
brával	brávat	k5eAaImAgMnS
děti	dítě	k1gFnPc4
do	do	k7c2
cirkusu	cirkus	k1gInSc2
<g/>
,	,	kIx,
cvičil	cvičit	k5eAaImAgMnS
je	být	k5eAaImIp3nS
v	v	k7c6
jízdě	jízda	k1gFnSc6
na	na	k7c6
koni	kůň	k1gMnSc6
a	a	k8xC
hře	hra	k1gFnSc6
na	na	k7c4
citeru	citera	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Většinu	většina	k1gFnSc4
času	čas	k1gInSc2
trávila	trávit	k5eAaImAgFnS
Néné	Néné	k1gNnSc4
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
sourozenci	sourozenec	k1gMnPc7
Ludvíkem	Ludvík	k1gMnSc7
<g/>
,	,	kIx,
Alžbětou	Alžběta	k1gFnSc7
<g/>
,	,	kIx,
Karlem	Karel	k1gMnSc7
Teodorem	Teodor	k1gMnSc7
<g/>
,	,	kIx,
Marií	Maria	k1gFnSc7
<g/>
,	,	kIx,
Matyldou	Matylda	k1gFnSc7
<g/>
,	,	kIx,
Sofií	Sofia	k1gFnSc7
a	a	k8xC
Maxem	Max	k1gMnSc7
Emanuelem	Emanuel	k1gMnSc7
v	v	k7c6
mnichovském	mnichovský	k2eAgInSc6d1
paláci	palác	k1gInSc6
nebo	nebo	k8xC
na	na	k7c6
venkovském	venkovský	k2eAgInSc6d1
zámečku	zámeček	k1gInSc6
Possenhofen	Possenhofna	k1gFnPc2
u	u	k7c2
Starnberského	Starnberský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejbližší	blízký	k2eAgInSc4d3
vztah	vztah	k1gInSc4
měla	mít	k5eAaImAgFnS
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
mladší	mladý	k2eAgFnSc7d2
sestrou	sestra	k1gFnSc7
Alžbětou	Alžběta	k1gFnSc7
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc1
pouto	pouto	k1gNnSc1
přetrvalo	přetrvat	k5eAaPmAgNnS
po	po	k7c4
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
až	až	k9
do	do	k7c2
Heleniny	Helenin	k2eAgFnSc2d1
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
učení	učení	k1gNnSc6
se	se	k3xPyFc4
příliš	příliš	k6eAd1
důrazu	důraz	k1gInSc2
nekladlo	klást	k5eNaImAgNnS
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
se	se	k3xPyFc4
sice	sice	k8xC
snažila	snažit	k5eAaImAgFnS
zajistit	zajistit	k5eAaPmF
vhodné	vhodný	k2eAgMnPc4d1
učitele	učitel	k1gMnPc4
pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
ratolesti	ratolest	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
tím	ten	k3xDgNnSc7
její	její	k3xOp3gFnSc4
iniciativa	iniciativa	k1gFnSc1
končila	končit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Sestra	sestra	k1gFnSc1
císařovny	císařovna	k1gFnSc2
a	a	k8xC
kněžna	kněžna	k1gFnSc1
z	z	k7c2
Thurnu	Thurn	k1gInSc2
a	a	k8xC
Taxisu	taxis	k1gInSc2
</s>
<s>
Kněžna	kněžna	k1gFnSc1
Helena	Helena	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gFnSc1
sestra	sestra	k1gFnSc1
císařovna	císařovna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
</s>
<s>
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
změnila	změnit	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
Ludovičina	Ludovičina	k1gFnSc1
sestra	sestra	k1gFnSc1
arcivévodkyně	arcivévodkyně	k1gFnSc1
Žofie	Žofie	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
její	její	k3xOp3gFnSc1
neteř	neteř	k1gFnSc1
Helena	Helena	k1gFnSc1
stane	stanout	k5eAaPmIp3nS
manželkou	manželka	k1gFnSc7
jejího	její	k3xOp3gMnSc2
syna	syn	k1gMnSc2
rakouského	rakouský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc2
I.	I.	kA
Snažili	snažit	k5eAaImAgMnP
se	se	k3xPyFc4
rychle	rychle	k6eAd1
dohnat	dohnat	k5eAaPmF
<g/>
,	,	kIx,
co	co	k9
by	by	kYmCp3nS
Helena	Helena	k1gFnSc1
mohla	moct	k5eAaImAgFnS
potřebovat	potřebovat	k5eAaImF
jako	jako	k9
budoucí	budoucí	k2eAgFnSc1d1
císařovna	císařovna	k1gFnSc1
–	–	k?
správně	správně	k6eAd1
tančit	tančit	k5eAaImF
<g/>
,	,	kIx,
pohybovat	pohybovat	k5eAaImF
se	se	k3xPyFc4
<g/>
,	,	kIx,
oblékat	oblékat	k5eAaImF
atd.	atd.	kA
Největší	veliký	k2eAgFnPc4d3
mezery	mezera	k1gFnPc4
měla	mít	k5eAaImAgFnS
ale	ale	k9
princezna	princezna	k1gFnSc1
ve	v	k7c6
znalosti	znalost	k1gFnSc6
cizích	cizí	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
doučit	doučit	k5eAaPmF
zejména	zejména	k9
důležité	důležitý	k2eAgFnSc3d1
francouzštině	francouzština	k1gFnSc3
a	a	k8xC
poznat	poznat	k5eAaPmF
další	další	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
podunajské	podunajský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1853	#num#	k4
dorazila	dorazit	k5eAaPmAgFnS
Ludovika	Ludovika	k1gFnSc1
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
dvěma	dva	k4xCgFnPc7
dcerami	dcera	k1gFnPc7
Helenou	Helena	k1gFnSc7
a	a	k8xC
Alžbětou	Alžběta	k1gFnSc7
do	do	k7c2
Bad	Bad	k1gFnSc2
Ischlu	Ischl	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
setkání	setkání	k1gNnSc3
budoucích	budoucí	k2eAgMnPc2d1
snoubenců	snoubenec	k1gMnPc2
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
císařových	císařův	k2eAgInPc2d1
23	#num#	k4
<g/>
.	.	kIx.
narozenin	narozeniny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
tři	tři	k4xCgFnPc1
dámy	dáma	k1gFnPc1
držely	držet	k5eAaImAgFnP
smutek	smutek	k1gInSc4
za	za	k7c4
nedávno	nedávno	k6eAd1
zesnulého	zesnulý	k2eAgMnSc4d1
vzdáleného	vzdálený	k2eAgMnSc4d1
příbuzného	příbuzný	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Helena	Helena	k1gFnSc1
působila	působit	k5eAaImAgFnS
v	v	k7c6
černé	černá	k1gFnSc6
upjatě	upjatě	k6eAd1
a	a	k8xC
přísně	přísně	k6eAd1
<g/>
,	,	kIx,
naproti	naproti	k7c3
tomu	ten	k3xDgMnSc3
mladistvá	mladistvý	k2eAgFnSc1d1
Alžběta	Alžběta	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
nevšední	všednět	k5eNaImIp3nS
krásu	krása	k1gFnSc4
černá	černá	k1gFnSc1
jen	jen	k6eAd1
podtrhovala	podtrhovat	k5eAaImAgFnS
<g/>
,	,	kIx,
císaře	císař	k1gMnPc4
záhy	záhy	k6eAd1
upoutala	upoutat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k8xS,k8xC
se	se	k3xPyFc4
roku	rok	k1gInSc2
1854	#num#	k4
konala	konat	k5eAaImAgFnS
svatba	svatba	k1gFnSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
nikoli	nikoli	k9
Helenina	Helenin	k2eAgFnSc1d1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
její	její	k3xOp3gFnPc1
sestry	sestra	k1gFnPc1
Alžběty	Alžběta	k1gFnSc2
s	s	k7c7
císařem	císař	k1gMnSc7
Františkem	František	k1gMnSc7
Josefem	Josef	k1gMnSc7
I.	I.	kA
</s>
<s>
Roku	rok	k1gInSc2
1856	#num#	k4
přivedla	přivést	k5eAaPmAgFnS
matka	matka	k1gFnSc1
Heleně	Helena	k1gFnSc3
do	do	k7c2
cesty	cesta	k1gFnSc2
Maxmiliána	Maxmilián	k1gMnSc2
Antonína	Antonín	k1gMnSc2
<g/>
,	,	kIx,
syna	syn	k1gMnSc2
knížete	kníže	k1gMnSc2
Maxmiliána	Maxmilián	k1gMnSc2
Karla	Karel	k1gMnSc2
z	z	k7c2
rodu	rod	k1gInSc2
Thurn-Taxisů	Thurn-Taxis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
rod	rod	k1gInSc1
sídlící	sídlící	k2eAgInSc1d1
v	v	k7c6
Řezně	Řezno	k1gNnSc6
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejbohatších	bohatý	k2eAgMnPc2d3
v	v	k7c4
monarchii	monarchie	k1gFnSc4
(	(	kIx(
<g/>
byli	být	k5eAaImAgMnP
zakladateli	zakladatel	k1gMnPc7
poštovní	poštovní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
niž	jenž	k3xRgFnSc4
měli	mít	k5eAaImAgMnP
monopol	monopol	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thurn-Taxisové	Thurn-Taxisový	k2eAgMnPc4d1
často	často	k6eAd1
pořádali	pořádat	k5eAaImAgMnP
hony	hon	k1gInPc4
pro	pro	k7c4
velkou	velký	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
a	a	k8xC
jako	jako	k9
hostitelé	hostitel	k1gMnPc1
byli	být	k5eAaImAgMnP
vyhlášení	vyhlášení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jejich	jejich	k3xOp3gFnSc6
jídelní	jídelní	k2eAgFnSc6d1
tabuli	tabule	k1gFnSc6
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
mohl	moct	k5eAaImAgMnS
setkat	setkat	k5eAaPmF
jak	jak	k6eAd1
s	s	k7c7
nejvyhlášenějšími	vyhlášený	k2eAgMnPc7d3
<g/>
,	,	kIx,
tak	tak	k9
s	s	k7c7
nejexotičtějšími	exotický	k2eAgFnPc7d3
lahůdkami	lahůdka	k1gFnPc7
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
jejich	jejich	k3xOp3gInPc1
paláce	palác	k1gInPc1
byly	být	k5eAaImAgInP
krásně	krásně	k6eAd1
a	a	k8xC
moderně	moderně	k6eAd1
zařízeny	zařízen	k2eAgFnPc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
jejich	jejich	k3xOp3gMnPc1
návštěvníci	návštěvník	k1gMnPc1
měli	mít	k5eAaImAgMnP
co	co	k9
nejlepší	dobrý	k2eAgNnSc4d3
pohodlí	pohodlí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
Antonín	Antonín	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
v	v	k7c6
sobě	se	k3xPyFc3
našli	najít	k5eAaPmAgMnP
zalíbení	zalíbení	k1gNnSc4
<g/>
,	,	kIx,
Maxmilián	Maxmilián	k1gMnSc1
však	však	k9
nebyl	být	k5eNaImAgMnS
Heleně	Helena	k1gFnSc3
roven	roven	k2eAgInSc1d1
rodem	rod	k1gInSc7
(	(	kIx(
<g/>
byl	být	k5eAaImAgMnS
pouze	pouze	k6eAd1
kníže	kníže	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
sestra	sestra	k1gFnSc1
císařovny	císařovna	k1gFnSc2
rakouské	rakouský	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
celou	celý	k2eAgFnSc4d1
záležitost	záležitost	k1gFnSc4
vyvolal	vyvolat	k5eAaPmAgMnS
Helenin	Helenin	k2eAgMnSc1d1
příbuzný	příbuzný	k1gMnSc1
<g/>
,	,	kIx,
bavorský	bavorský	k2eAgMnSc1d1
král	král	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský	bavorský	k2eAgInSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ji	on	k3xPp3gFnSc4
mohl	moct	k5eAaImAgMnS
opět	opět	k6eAd1
vyřešit	vyřešit	k5eAaPmF
–	–	k?
Helena	Helena	k1gFnSc1
byla	být	k5eAaImAgFnS
oprávněna	oprávnit	k5eAaPmNgFnS
<g/>
,	,	kIx,
kromě	kromě	k7c2
titulu	titul	k1gInSc2
dědičná	dědičný	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
<g/>
/	/	kIx~
<g/>
kněžna	kněžna	k1gFnSc1
z	z	k7c2
Thurnu	Thurn	k1gInSc2
a	a	k8xC
Taxisu	taxis	k1gInSc2
<g/>
,	,	kIx,
používat	používat	k5eAaImF
i	i	k8xC
titul	titul	k1gInSc1
vévodkyně	vévodkyně	k1gFnSc2
bavorská	bavorský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Její	její	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1858	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Possenhofenu	Possenhofen	k1gInSc6
konala	konat	k5eAaImAgFnS
velkolepá	velkolepý	k2eAgFnSc1d1
svatba	svatba	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c4
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
Helena	Helena	k1gFnSc1
přesídlila	přesídlit	k5eAaPmAgFnS
do	do	k7c2
Řezna	Řezno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
byla	být	k5eAaImAgFnS
známa	znám	k2eAgFnSc1d1
svou	svůj	k3xOyFgFnSc7
péčí	péče	k1gFnSc7
o	o	k7c4
chudé	chudý	k2eAgMnPc4d1
a	a	k8xC
potřebné	potřebný	k2eAgMnPc4d1
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
v	v	k7c6
celém	celý	k2eAgNnSc6d1
Bavorsku	Bavorsko	k1gNnSc6
<g/>
,	,	kIx,
iniciovala	iniciovat	k5eAaBmAgFnS
zřizování	zřizování	k1gNnSc4
různých	různý	k2eAgInPc2d1
spolků	spolek	k1gInPc2
a	a	k8xC
památníků	památník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princezna	princezna	k1gFnSc1
byla	být	k5eAaImAgFnS
jak	jak	k6eAd1
mezi	mezi	k7c7
obyčejnými	obyčejný	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
mezi	mezi	k7c7
svými	svůj	k3xOyFgFnPc7
novými	nový	k2eAgFnPc7d1
příbuznými	příbuzná	k1gFnPc7
velmi	velmi	k6eAd1
oblíbená	oblíbený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Helenin	Helenin	k2eAgInSc1d1
nesnadný	snadný	k2eNgInSc1d1
život	život	k1gInSc1
</s>
<s>
Její	její	k3xOp3gMnSc1
manžel	manžel	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1867	#num#	k4
v	v	k7c6
35	#num#	k4
letech	léto	k1gNnPc6
v	v	k7c6
Řezně	Řezno	k1gNnSc6
na	na	k7c4
ledvinové	ledvinový	k2eAgNnSc4d1
onemocnění	onemocnění	k1gNnSc4
<g/>
,	,	kIx,
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
zemřel	zemřít	k5eAaPmAgInS
i	i	k8xC
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
se	s	k7c7
hlavou	hlava	k1gFnSc7
rodu	rod	k1gInSc2
Thurn-Taxisů	Thurn-Taxis	k1gInPc2
až	až	k9
do	do	k7c2
plnoletosti	plnoletost	k1gFnSc2
syna	syn	k1gMnSc2
Maxmiliána	Maxmilián	k1gMnSc2
Marii	Maria	k1gFnSc4
stala	stát	k5eAaPmAgFnS
Helena	Helena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1877	#num#	k4
se	se	k3xPyFc4
její	její	k3xOp3gFnSc1
dcera	dcera	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
vdala	vdát	k5eAaPmAgFnS
za	za	k7c2
prince	princ	k1gMnSc2
Michala	Michal	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
zemřela	zemřít	k5eAaPmAgFnS
při	při	k7c6
porodu	porod	k1gInSc6
třetího	třetí	k4xOgNnSc2
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1885	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
její	její	k3xOp3gMnSc1
starší	starý	k2eAgMnSc1d2
syn	syn	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
Maria	Maria	k1gFnSc1
<g/>
,	,	kIx,
nepomohla	pomoct	k5eNaPmAgFnS
ani	ani	k9
její	její	k3xOp3gFnSc1
snaha	snaha	k1gFnSc1
kontaktovat	kontaktovat	k5eAaImF
svého	svůj	k3xOyFgMnSc4
bratra	bratr	k1gMnSc4
Karla	Karel	k1gMnSc4
Teodora	Teodor	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
lékařem	lékař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1890	#num#	k4
těžce	těžce	k6eAd1
onemocněla	onemocnět	k5eAaPmAgFnS
a	a	k8xC
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
v	v	k7c6
Řezně	Řezno	k1gNnSc6
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
své	svůj	k3xOyFgFnSc2
sestry	sestra	k1gFnSc2
císařovny	císařovna	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
zemřela	zemřít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Luisa	Luisa	k1gFnSc1
z	z	k7c2
Thurnu	Thurn	k1gInSc2
a	a	k8xC
Taxisu	taxis	k1gInSc2
(	(	kIx(
<g/>
1859	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
~	~	kIx~
1879	#num#	k4
Fridrich	Fridrich	k1gMnSc1
Hohenzollern-Sigmaringen	Hohenzollern-Sigmaringen	k1gInSc4
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
z	z	k7c2
Thurnu	Thurn	k1gInSc2
a	a	k8xC
Taxisu	taxis	k1gInSc2
(	(	kIx(
<g/>
1860	#num#	k4
<g/>
–	–	k?
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
~	~	kIx~
1877	#num#	k4
Michal	Michala	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Braganza	Braganza	k1gFnSc1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
Maria	Mario	k1gMnSc2
z	z	k7c2
Thurnu	Thurn	k1gInSc2
a	a	k8xC
Taxisu	taxis	k1gInSc2
(	(	kIx(
<g/>
1862	#num#	k4
<g/>
–	–	k?
<g/>
1885	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Albert	Albert	k1gMnSc1
I.	I.	kA
z	z	k7c2
Thurnu	Thurn	k1gInSc2
a	a	k8xC
Taxisu	taxis	k1gInSc2
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
~	~	kIx~
1890	#num#	k4
Markéta	Markéta	k1gFnSc1
Klementina	Klementina	k1gFnSc1
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Johann	Johann	k1gNnSc1
von	von	k1gInSc1
Pfalz-Birkenfeld	Pfalz-Birkenfeld	k1gMnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Sophie	Sophius	k1gMnSc5
Charlotte	Charlott	k1gMnSc5
von	von	k1gInSc4
Salm-Dhaun	Salm-Dhaun	k1gNnSc4
</s>
<s>
Pius	Pius	k1gMnSc1
Augustus	Augustus	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Maria	Mario	k1gMnSc2
z	z	k7c2
Ligne	Lign	k1gMnSc5
</s>
<s>
Maria	Maria	k1gFnSc1
Anna	Anna	k1gFnSc1
Falcko-Zweibrückenská	Falcko-Zweibrückenský	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
z	z	k7c2
Mailly-Nestle	Mailly-Nestle	k1gFnSc2
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Maria	Mario	k1gMnSc2
Raimund	Raimunda	k1gFnPc2
z	z	k7c2
Arenbergu	Arenberg	k1gInSc2
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Engelbert	Engelbert	k1gMnSc1
z	z	k7c2
Arenbergu	Arenberg	k1gInSc2
</s>
<s>
Luisa	Luisa	k1gFnSc1
Marie	Marie	k1gFnSc1
ze	z	k7c2
Schleidenu	Schleiden	k1gInSc2
</s>
<s>
Amálie	Amálie	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Arenberková	Arenberková	k1gFnSc1
</s>
<s>
Louis	Louis	k1gMnSc1
Joseph	Joseph	k1gMnSc1
de	de	k?
Mailly	Mailla	k1gFnSc2
</s>
<s>
Marie	Marie	k1gFnSc1
Adélaï	Adélaï	k1gInSc5
Julie	Julie	k1gFnSc1
de	de	k?
Mailly	Mailla	k1gFnSc2
</s>
<s>
Adélaï	Adélaï	k6eAd1
Julie	Julie	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Hautefort	Hautefort	k1gInSc1
</s>
<s>
Helena	Helena	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
</s>
<s>
Kristián	Kristián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgInSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Michal	Michal	k1gMnSc1
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgMnSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Nasavsko-Saarbrückenská	Nasavsko-Saarbrückenský	k2eAgFnSc1d1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
Sulzbašský	Sulzbašský	k2eAgMnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Františka	Františka	k1gFnSc1
Falcko-Sulzbašská	Falcko-Sulzbašský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Augusta	August	k1gMnSc2
Falcko-Neuburská	Falcko-Neuburský	k2eAgFnSc1d1
</s>
<s>
Ludovika	Ludovika	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Bádenský	bádenský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Bádenský	bádenský	k2eAgMnSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Hesensko-Darmstadtská	Hesensko-Darmstadtský	k2eAgFnSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Frederika	Frederika	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Bádenská	bádenský	k2eAgFnSc1d1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hesensko-Darmstadtský	Hesensko-Darmstadtský	k2eAgInSc1d1
</s>
<s>
Amálie	Amálie	k1gFnSc1
Hesensko-Darmstadtská	Hesensko-Darmstadtský	k2eAgFnSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Falcko-Zweibrückenská	Falcko-Zweibrückenský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
GRÖSSING	GRÖSSING	kA
Sigrid-Maria	Sigrid-Marium	k1gNnPc4
<g/>
,	,	kIx,
Dvě	dva	k4xCgFnPc1
nevěsty	nevěsta	k1gFnPc1
pro	pro	k7c4
císaře	císař	k1gMnSc4
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgInS
Ikar	Ikar	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2002	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-249-0109-9	80-249-0109-9	k4
</s>
<s>
BESTENREINER	BESTENREINER	kA
Erika	Erika	k1gFnSc1
<g/>
,	,	kIx,
Sisi	Sis	k1gMnPc1
a	a	k8xC
její	její	k3xOp3gMnPc1
sourozenci	sourozenec	k1gMnPc1
<g/>
,	,	kIx,
vydavatelství	vydavatelství	k1gNnPc1
Brána	brána	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7243-232-X	80-7243-232-X	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Wittelsbachové	Wittelsbachové	k2eAgFnSc1d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Helena	Helena	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://cisarovna-sisi.wz.cz	http://cisarovna-sisi.wz.cz	k1gMnSc1
–	–	k?
životopisy	životopis	k1gInPc1
a	a	k8xC
portréty	portrét	k1gInPc1
císařovny	císařovna	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gMnPc2
sourozenců	sourozenec	k1gMnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20030310001	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
12130924X	12130924X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5141	#num#	k4
4917	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
17847	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
40231723	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
17847	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
|	|	kIx~
Monarchie	monarchie	k1gFnSc1
</s>
