<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
(	(	kIx(
<g/>
zkracováno	zkracován	k2eAgNnSc1d1
ZČU	ZČU	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
univerzitní	univerzitní	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1991	#num#	k4
sloučením	sloučení	k1gNnSc7
Vysoké	vysoká	k1gFnSc2
školy	škola	k1gFnSc2
strojní	strojní	k2eAgFnSc2d1
a	a	k8xC
elektrotechnické	elektrotechnický	k2eAgFnSc2d1
a	a	k8xC
Pedagogické	pedagogický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
.	.	kIx.
</s>