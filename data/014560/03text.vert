<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plznilatinsky	Plznilatinsky	k1gFnSc6
Universitas	Universitasa	k1gFnPc2
Bohemiae	Bohemiae	k1gFnSc1
occidentalis	occidentalis	k1gFnSc2
Logo	logo	k1gNnSc4
Univerzitní	univerzitní	k2eAgInSc1d1
kampus	kampus	k1gInSc1
na	na	k7c6
Borech	bor	k1gInPc6
Zkratka	zkratka	k1gFnSc1
</s>
<s>
ZČU	ZČU	kA
Rok	rok	k1gInSc1
založení	založení	k1gNnSc2
</s>
<s>
1991	#num#	k4
Typ	typ	k1gInSc1
školy	škola	k1gFnSc2
</s>
<s>
veřejná	veřejný	k2eAgNnPc1d1
Vedení	vedení	k1gNnPc1
Rektor	rektor	k1gMnSc1
</s>
<s>
doc.	doc.	kA
Dr	dr	kA
<g/>
.	.	kIx.
RNDr.	RNDr.	kA
Miroslav	Miroslav	k1gMnSc1
Holeček	Holeček	k1gMnSc1
Kvestor	kvestor	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Petr	Petr	k1gMnSc1
Hofman	Hofman	k1gMnSc1
Kancléř	kancléř	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Lucie	Lucie	k1gFnSc1
Václavová	Václavová	k1gFnSc1
Čulíková	Čulíková	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
a	a	k8xC
vnější	vnější	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
</s>
<s>
doc.	doc.	kA
Ing.	ing.	kA
Vladimír	Vladimír	k1gMnSc1
Duchek	Duchek	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
studijní	studijní	k2eAgFnSc4d1
a	a	k8xC
pedagogickou	pedagogický	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
</s>
<s>
RNDr.	RNDr.	kA
Blanka	Blanka	k1gFnSc1
Šedivá	Šedivá	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
a	a	k8xC
vývoj	vývoj	k1gInSc4
</s>
<s>
doc.	doc.	kA
Ing.	ing.	kA
Luděk	Luděk	k1gMnSc1
Hynčík	Hynčík	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
internacionalizaci	internacionalizace	k1gFnSc4
</s>
<s>
Ing.	ing.	kA
Dita	Dita	k1gFnSc1
Hommerová	Hommerová	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
MBA	MBA	kA
Počty	počet	k1gInPc1
akademiků	akademik	k1gMnPc2
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
bakalářských	bakalářský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
6647	#num#	k4
Počet	počet	k1gInSc4
magisterských	magisterský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
3480	#num#	k4
Počet	počet	k1gInSc4
doktorandů	doktorand	k1gMnPc2
</s>
<s>
532	#num#	k4
Počet	počet	k1gInSc4
studentů	student	k1gMnPc2
</s>
<s>
10	#num#	k4
661	#num#	k4
Počet	počet	k1gInSc4
akademických	akademický	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
</s>
<s>
960	#num#	k4
Počet	počet	k1gInSc4
vědeckých	vědecký	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
</s>
<s>
493	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Počet	počet	k1gInSc1
fakult	fakulta	k1gFnPc2
</s>
<s>
9	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Univerzitní	univerzitní	k2eAgNnSc4d1
2732	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
301	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
25,32	25,32	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
5,4	5,4	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Členství	členství	k1gNnSc1
</s>
<s>
Asociace	asociace	k1gFnSc1
evropských	evropský	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
</s>
<s>
http://www.zcu.cz	http://www.zcu.cz	k1gMnSc1
</s>
<s>
Profil	profil	k1gInSc1
na	na	k7c4
Facebooku	Facebook	k1gInSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
(	(	kIx(
<g/>
zkracováno	zkracován	k2eAgNnSc1d1
ZČU	ZČU	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
univerzitní	univerzitní	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1991	#num#	k4
sloučením	sloučení	k1gNnSc7
Vysoké	vysoká	k1gFnSc2
školy	škola	k1gFnSc2
strojní	strojní	k2eAgFnSc2d1
a	a	k8xC
elektrotechnické	elektrotechnický	k2eAgFnSc2d1
a	a	k8xC
Pedagogické	pedagogický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Obecné	obecný	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vzniku	vznik	k1gInSc2
univerzity	univerzita	k1gFnPc1
předcházely	předcházet	k5eAaImAgFnP
dvě	dva	k4xCgFnPc4
starší	starý	k2eAgFnPc4d2
vysokoškolské	vysokoškolský	k2eAgFnPc4d1
instituce	instituce	k1gFnPc4
–	–	k?
Vysoká	vysoká	k1gFnSc1
škola	škola	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
a	a	k8xC
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
a	a	k8xC
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
a	a	k8xC
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
jako	jako	k8xC,k8xS
pobočka	pobočka	k1gFnSc1
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1950	#num#	k4
byla	být	k5eAaImAgFnS
z	z	k7c2
plzeňské	plzeňský	k2eAgFnSc2d1
VŠSE	VŠSE	kA
vytvořena	vytvořen	k2eAgFnSc1d1
oddělená	oddělený	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
děkanem	děkan	k1gMnSc7
a	a	k8xC
fakultní	fakultní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
z	z	k7c2
ní	on	k3xPp3gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1953	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
samostatná	samostatný	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
řízená	řízený	k2eAgFnSc1d1
rektorem	rektor	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1960	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozdělení	rozdělení	k1gNnSc3
na	na	k7c4
fakulty	fakulta	k1gFnPc4
strojní	strojní	k2eAgNnSc4d1
a	a	k8xC
elektrotechnickou	elektrotechnický	k2eAgFnSc7d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
pak	pak	k6eAd1
vznikly	vzniknout	k5eAaPmAgInP
fakulty	fakulta	k1gFnPc4
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
jako	jako	k8xC,k8xS
pobočka	pobočka	k1gFnSc1
Pedagogické	pedagogický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1953	#num#	k4
byla	být	k5eAaImAgFnS
jen	jen	k6eAd1
Vyšší	vysoký	k2eAgFnSc1d2
pedagogickou	pedagogický	k2eAgFnSc7d1
školou	škola	k1gFnSc7
<g/>
,	,	kIx,
pak	pak	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
Pedagogickým	pedagogický	k2eAgInSc7d1
institutem	institut	k1gInSc7
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1964	#num#	k4
samostatnou	samostatný	k2eAgFnSc7d1
Pedagogickou	pedagogický	k2eAgFnSc7d1
fakultou	fakulta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1991	#num#	k4
byly	být	k5eAaImAgFnP
obě	dva	k4xCgFnPc1
vysoké	vysoká	k1gFnPc1
školy	škola	k1gFnSc2
zákonem	zákon	k1gInSc7
č.	č.	k?
314	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
sloučeny	sloučen	k2eAgInPc1d1
do	do	k7c2
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Tu	tu	k6eAd1
oficiálně	oficiálně	k6eAd1
tvořilo	tvořit	k5eAaImAgNnS
celkem	celkem	k6eAd1
5	#num#	k4
fakult	fakulta	k1gFnPc2
–	–	k?
Fakulta	fakulta	k1gFnSc1
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
Fakulta	fakulta	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Fakulta	fakulta	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
a	a	k8xC
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1992	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přesunu	přesun	k1gInSc3
Fakulty	fakulta	k1gFnSc2
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
Fakulty	fakulta	k1gFnPc1
strojní	strojní	k2eAgFnPc1d1
do	do	k7c2
nového	nový	k2eAgInSc2d1
univerzitního	univerzitní	k2eAgInSc2d1
areálu	areál	k1gInSc2
na	na	k7c6
tzv.	tzv.	kA
Zelený	zelený	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
Bory	bor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plán	plán	k1gInSc1
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
výstavbu	výstavba	k1gFnSc4
byl	být	k5eAaImAgInS
přitom	přitom	k6eAd1
schválen	schválit	k5eAaPmNgInS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
Fakulta	fakulta	k1gFnSc1
právnická	právnický	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
Fakulta	fakulta	k1gFnSc1
humanitních	humanitní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
2005	#num#	k4
na	na	k7c4
návrh	návrh	k1gInSc4
prvního	první	k4xOgMnSc2
děkana	děkan	k1gMnSc2
Ivo	Ivo	k1gMnSc1
T.	T.	kA
Budila	Budil	k1gMnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
Fakultu	fakulta	k1gFnSc4
filozofickou	filozofický	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
Ústav	ústav	k1gInSc1
umění	umění	k1gNnSc2
a	a	k8xC
designu	design	k1gInSc2
jako	jako	k8xC,k8xS
samostatný	samostatný	k2eAgInSc4d1
vysokoškolský	vysokoškolský	k2eAgInSc4d1
ústav	ústav	k1gInSc4
<g/>
;	;	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
povýšil	povýšit	k5eAaPmAgInS
na	na	k7c4
Fakultu	fakulta	k1gFnSc4
umění	umění	k1gNnSc2
a	a	k8xC
designu	design	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
přejmenovala	přejmenovat	k5eAaPmAgFnS
na	na	k7c4
Fakultu	fakulta	k1gFnSc4
designu	design	k1gInSc2
a	a	k8xC
umění	umění	k1gNnSc2
Ladislava	Ladislav	k1gMnSc2
Sutnara	Sutnar	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
se	se	k3xPyFc4
s	s	k7c7
univerzitou	univerzita	k1gFnSc7
spojila	spojit	k5eAaPmAgFnS
soukromá	soukromý	k2eAgFnSc1d1
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
tak	tak	k6eAd1
Fakulta	fakulta	k1gFnSc1
zdravotnických	zdravotnický	k2eAgNnPc2d1
studií	studio	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
první	první	k4xOgFnSc2
dekády	dekáda	k1gFnSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
proběhla	proběhnout	k5eAaPmAgFnS
na	na	k7c6
Fakultě	fakulta	k1gFnSc6
právnické	právnický	k2eAgFnSc6d1
plagiátorská	plagiátorský	k2eAgFnSc1d1
aféra	aféra	k1gFnSc1
a	a	k8xC
skandál	skandál	k1gInSc1
s	s	k7c7
údajnými	údajný	k2eAgInPc7d1
podvody	podvod	k1gInPc7
v	v	k7c6
procesu	proces	k1gInSc6
udělování	udělování	k1gNnSc2
akademických	akademický	k2eAgMnPc2d1
titulů	titul	k1gInPc2
a	a	k8xC
řádného	řádný	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
sklonku	sklonek	k1gInSc6
roku	rok	k1gInSc2
2019	#num#	k4
získala	získat	k5eAaPmAgFnS
ZČU	ZČU	kA
institucionální	institucionální	k2eAgFnSc4d1
akreditaci	akreditace	k1gFnSc4
pro	pro	k7c4
jedenáct	jedenáct	k4xCc4
oblastí	oblast	k1gFnPc2
vzdělávání	vzdělávání	k1gNnSc2
v	v	k7c6
bakalářském	bakalářský	k2eAgInSc6d1
stupni	stupeň	k1gInSc6
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
osmi	osm	k4xCc6
magisterských	magisterský	k2eAgFnPc6d1
a	a	k8xC
ve	v	k7c6
čtyřech	čtyři	k4xCgFnPc6
doktorských	doktorský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
tak	tak	k6eAd1
může	moct	k5eAaImIp3nS
v	v	k7c6
těchto	tento	k3xDgFnPc6
oblastech	oblast	k1gFnPc6
tvořit	tvořit	k5eAaImF
a	a	k8xC
samostatně	samostatně	k6eAd1
schvalovat	schvalovat	k5eAaImF
studijní	studijní	k2eAgInPc4d1
programy	program	k1gInPc4
a	a	k8xC
na	na	k7c6
jejich	jejich	k3xOp3gInSc6
základě	základ	k1gInSc6
poskytovat	poskytovat	k5eAaImF
vysokoškolské	vysokoškolský	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
2018	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
získalo	získat	k5eAaPmAgNnS
sedm	sedm	k4xCc4
fakult	fakulta	k1gFnPc2
a	a	k8xC
součástí	součást	k1gFnSc7
ZČU	ZČU	kA
ocenění	ocenění	k1gNnPc4
Evropské	evropský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
HR	hr	k2eAgFnSc2d1
Excellence	Excellence	k1gFnSc2
in	in	k?
Research	Research	k1gInSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
HR	hr	k2eAgMnSc1d1
Award	Award	k1gMnSc1
<g/>
,	,	kIx,
deklarující	deklarující	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
daná	daný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
vědeckým	vědecký	k2eAgMnPc3d1
pracovníkům	pracovník	k1gMnPc3
ve	v	k7c6
všech	všecek	k3xTgInPc6
ohledech	ohled	k1gInPc6
příznivé	příznivý	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
a	a	k8xC
dodržuje	dodržovat	k5eAaImIp3nS
evropské	evropský	k2eAgInPc4d1
standardy	standard	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
žebříčku	žebříček	k1gInSc6
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
World	World	k1gMnSc1
University	universita	k1gFnSc2
Rankings	Rankings	k1gInSc1
2019	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
se	se	k3xPyFc4
ZČU	ZČU	kA
umístila	umístit	k5eAaPmAgFnS
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
z	z	k7c2
univerzit	univerzita	k1gFnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
rektorů	rektor	k1gMnPc2
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
a	a	k8xC
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
</s>
<s>
Prof.	prof.	kA
RNDr.	RNDr.	kA
Vojtěch	Vojtěch	k1gMnSc1
Voleník	Voleník	k1gMnSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prof.	prof.	kA
Ing.	ing.	kA
dr	dr	kA
<g/>
.	.	kIx.
Přemysl	Přemysl	k1gMnSc1
Breník	Breník	k1gMnSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
–	–	k?
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prof.	prof.	kA
Ing.	ing.	kA
Emil	Emil	k1gMnSc1
Langer	Langer	k1gMnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prof.	prof.	kA
Ing.	ing.	kA
Stanislav	Stanislav	k1gMnSc1
Kubík	Kubík	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prof.	prof.	kA
Ing.	ing.	kA
František	František	k1gMnSc1
Plánička	plánička	k1gFnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
doc.	doc.	kA
RNDr.	RNDr.	kA
Jiří	Jiří	k1gMnSc2
Holenda	Holend	k1gMnSc2
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
</s>
<s>
doc.	doc.	kA
RNDr.	RNDr.	kA
Jiří	Jiří	k1gMnSc2
Holenda	Holend	k1gMnSc2
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Zdeněk	Zdeněk	k1gMnSc1
Vostracký	Vostracký	k1gMnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
doc.	doc.	kA
Ing.	ing.	kA
Josef	Josef	k1gMnSc1
Průša	Průša	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
doc.	doc.	kA
PaedDr	PaedDr	kA
<g/>
.	.	kIx.
Ilona	Ilona	k1gFnSc1
Mauritzová	Mauritzová	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
doc.	doc.	kA
Dr	dr	kA
<g/>
.	.	kIx.
RNDr.	RNDr.	kA
Miroslav	Miroslav	k1gMnSc1
Holeček	Holeček	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
</s>
<s>
ZČU	ZČU	kA
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
univerzitami	univerzita	k1gFnPc7
ze	z	k7c2
zemí	zem	k1gFnPc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
a	a	k8xC
řadou	řada	k1gFnSc7
dalších	další	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
např.	např.	kA
z	z	k7c2
USA	USA	kA
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Koreje	Korea	k1gFnSc2
<g/>
,	,	kIx,
Tchaj-wanu	Tchaj-wan	k1gInSc2
či	či	k8xC
Mexika	Mexiko	k1gNnSc2
na	na	k7c6
poli	pole	k1gNnSc6
vzdělávání	vzdělávání	k1gNnSc2
i	i	k8xC
vědy	věda	k1gFnSc2
a	a	k8xC
výzkumu	výzkum	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ročně	ročně	k6eAd1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
možnosti	možnost	k1gFnPc4
studijních	studijní	k2eAgFnPc2d1
či	či	k8xC
praktických	praktický	k2eAgFnPc2d1
stáží	stáž	k1gFnPc2
více	hodně	k6eAd2
než	než	k8xS
500	#num#	k4
studentů	student	k1gMnPc2
bakalářského	bakalářský	k2eAgInSc2d1
<g/>
,	,	kIx,
magisterského	magisterský	k2eAgMnSc2d1
či	či	k8xC
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
čerstvých	čerstvý	k2eAgMnPc2d1
absolventů	absolvent	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZČU	ZČU	kA
zároveň	zároveň	k6eAd1
hostí	hostit	k5eAaImIp3nS
ročně	ročně	k6eAd1
na	na	k7c6
stážích	stáž	k1gFnPc6
přes	přes	k7c4
500	#num#	k4
studentů	student	k1gMnPc2
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
Erasmus	Erasmus	k1gInSc4
<g/>
+	+	kIx~
<g/>
,	,	kIx,
Inter	Inter	k1gInSc1
<g/>
/	/	kIx~
<g/>
Freemover	Freemover	k1gInSc1
a	a	k8xC
další	další	k2eAgInPc1d1
programy	program	k1gInPc1
nebo	nebo	k8xC
o	o	k7c4
meziuniverzitní	meziuniverzitní	k2eAgFnSc4d1
<g/>
,	,	kIx,
fakultní	fakultní	k2eAgFnSc4d1
a	a	k8xC
jinou	jiný	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
partnerských	partnerský	k2eAgFnPc6d1
institucích	instituce	k1gFnPc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
získat	získat	k5eAaPmF
tzv.	tzv.	kA
double	double	k2eAgFnPc7d1
degree	degree	k1gFnPc7
<g/>
,	,	kIx,
tedy	tedy	k8xC
diplom	diplom	k1gInSc1
ve	v	k7c6
druhém	druhý	k4xOgInSc6
jazyce	jazyk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Věda	věda	k1gFnSc1
a	a	k8xC
výzkum	výzkum	k1gInSc1
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
má	mít	k5eAaImIp3nS
čtyři	čtyři	k4xCgNnPc4
výzkumná	výzkumný	k2eAgNnPc4d1
centra	centrum	k1gNnPc4
řešící	řešící	k2eAgFnSc2d1
tuzemské	tuzemský	k2eAgFnSc2d1
i	i	k8xC
zahraniční	zahraniční	k2eAgInPc1d1
projekty	projekt	k1gInPc1
základního	základní	k2eAgMnSc2d1
a	a	k8xC
aplikovaného	aplikovaný	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
v	v	k7c6
oblastech	oblast	k1gFnPc6
perspektivních	perspektivní	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
a	a	k8xC
technologií	technologie	k1gFnPc2
a	a	k8xC
inteligentní	inteligentní	k2eAgNnPc1d1
řešení	řešení	k1gNnPc1
pro	pro	k7c4
průmysl	průmysl	k1gInSc4
<g/>
,	,	kIx,
lékařství	lékařství	k1gNnSc4
a	a	k8xC
veřejný	veřejný	k2eAgInSc4d1
sektor	sektor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkumné	výzkumný	k2eAgInPc1d1
týmy	tým	k1gInPc1
transferují	transferovat	k5eAaBmIp3nP
výsledky	výsledek	k1gInPc4
díky	díky	k7c3
vazbě	vazba	k1gFnSc3
na	na	k7c4
průmyslové	průmyslový	k2eAgInPc4d1
podniky	podnik	k1gInPc4
v	v	k7c6
regionu	region	k1gInSc6
i	i	k8xC
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkum	výzkum	k1gInSc1
na	na	k7c4
ZČU	ZČU	kA
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
humanitních	humanitní	k2eAgFnPc6d1
<g/>
,	,	kIx,
přírodních	přírodní	k2eAgFnPc6d1
a	a	k8xC
společenskovědních	společenskovědní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Fakulty	fakulta	k1gFnPc1
a	a	k8xC
součásti	součást	k1gFnPc1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
</s>
<s>
Fakulta	fakulta	k1gFnSc1
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
ZČU	ZČU	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
je	být	k5eAaImIp3nS
fakultou	fakulta	k1gFnSc7
inženýrsko-přírodovědného	inženýrsko-přírodovědný	k2eAgInSc2d1
profilu	profil	k1gInSc2
a	a	k8xC
klade	klást	k5eAaImIp3nS
si	se	k3xPyFc3
za	za	k7c4
cíl	cíl	k1gInSc4
připravovat	připravovat	k5eAaImF
budoucí	budoucí	k2eAgMnPc4d1
inženýry	inženýr	k1gMnPc4
<g/>
,	,	kIx,
magistry	magister	k1gMnPc4
<g/>
,	,	kIx,
bakaláře	bakalář	k1gMnPc4
a	a	k8xC
doktory	doktor	k1gMnPc4
(	(	kIx(
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
v	v	k7c6
oborech	obor	k1gInPc6
se	s	k7c7
znalostmi	znalost	k1gFnPc7
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
geomatiky	geomatika	k1gFnSc2
<g/>
,	,	kIx,
fyziky	fyzika	k1gFnSc2
<g/>
,	,	kIx,
mechaniky	mechanika	k1gFnSc2
<g/>
,	,	kIx,
stavitelství	stavitelství	k1gNnSc2
<g/>
,	,	kIx,
informatiky	informatika	k1gFnSc2
a	a	k8xC
kybernetiky	kybernetika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
designu	design	k1gInSc2
a	a	k8xC
umění	umění	k1gNnSc2
Ladislava	Ladislav	k1gMnSc2
Sutnara	Sutnar	k1gMnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
designu	design	k1gInSc2
a	a	k8xC
umění	umění	k1gNnSc2
Ladislava	Ladislav	k1gMnSc2
Sutnara	Sutnar	k1gMnSc2
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Umělecká	umělecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
nesoucí	nesoucí	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Ladislava	Ladislav	k1gMnSc2
Sutnara	Sutnar	k1gMnSc2
<g/>
,	,	kIx,
plzeňského	plzeňský	k2eAgMnSc4d1
rodáka	rodák	k1gMnSc4
a	a	k8xC
česko-amerického	česko-americký	k2eAgMnSc4d1
designéra	designér	k1gMnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
provozuje	provozovat	k5eAaImIp3nS
Galerii	galerie	k1gFnSc4
Ladislava	Ladislav	k1gMnSc2
Sutnara	Sutnar	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
Západočeské	západočeský	k2eAgFnPc4d1
univerzity	univerzita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
fakultu	fakulta	k1gFnSc4
ekonomického	ekonomický	k2eAgNnSc2d1
zaměření	zaměření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
a	a	k8xC
v	v	k7c6
Chebu	Cheb	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výuka	výuka	k1gFnSc1
se	se	k3xPyFc4
realizuje	realizovat	k5eAaBmIp3nS
v	v	k7c6
bakalářských	bakalářský	k2eAgInPc6d1
<g/>
,	,	kIx,
navazujících	navazující	k2eAgInPc6d1
magisterských	magisterský	k2eAgInPc6d1
i	i	k8xC
doktorských	doktorský	k2eAgInPc6d1
programech	program	k1gInPc6
v	v	k7c6
oblastech	oblast	k1gFnPc6
managementu	management	k1gInSc2
<g/>
,	,	kIx,
marketingu	marketing	k1gInSc2
<g/>
,	,	kIx,
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
projektového	projektový	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
informačního	informační	k2eAgInSc2d1
managementu	management	k1gInSc2
a	a	k8xC
geografie	geografie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
ZČU	ZČU	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
Západočeské	západočeský	k2eAgFnPc4d1
univerzity	univerzita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Technická	technický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
realizuje	realizovat	k5eAaBmIp3nS
studium	studium	k1gNnSc4
v	v	k7c6
bakalářských	bakalářský	k2eAgInPc6d1
<g/>
,	,	kIx,
magisterských	magisterský	k2eAgInPc6d1
a	a	k8xC
doktorských	doktorský	k2eAgInPc6d1
studijních	studijní	k2eAgInPc6d1
programech	program	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
fakult	fakulta	k1gFnPc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
filozofická	filozofický	k2eAgFnSc1d1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
filozofická	filozofický	k2eAgFnSc1d1
Západočeské	západočeský	k2eAgFnPc4d1
univerzity	univerzita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
humanitního	humanitní	k2eAgNnSc2d1
zaměření	zaměření	k1gNnSc2
zahrnující	zahrnující	k2eAgFnSc4d1
paletu	paleta	k1gFnSc4
oborů	obor	k1gInPc2
z	z	k7c2
oblasti	oblast	k1gFnSc2
společenských	společenský	k2eAgFnPc2d1
a	a	k8xC
humanitních	humanitní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studovat	studovat	k5eAaImF
lze	lze	k6eAd1
v	v	k7c6
bakalářských	bakalářský	k2eAgInPc6d1
<g/>
,	,	kIx,
navazujících	navazující	k2eAgInPc6d1
magisterských	magisterský	k2eAgInPc6d1
i	i	k8xC
doktorských	doktorský	k2eAgInPc6d1
programech	program	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
na	na	k7c6
společenských	společenský	k2eAgFnPc6d1
a	a	k8xC
kulturních	kulturní	k2eAgFnPc6d1
aktivitách	aktivita	k1gFnPc6
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
ZČU	ZČU	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
Západočeské	západočeský	k2eAgFnPc4d1
univerzity	univerzita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
připravuje	připravovat	k5eAaImIp3nS
studenty	student	k1gMnPc4
na	na	k7c4
učitelské	učitelský	k2eAgFnPc4d1
i	i	k8xC
neučitelské	učitelský	k2eNgFnPc4d1
profese	profes	k1gFnPc4
v	v	k7c6
bakalářských	bakalářský	k2eAgFnPc6d1
(	(	kIx(
<g/>
Bc.	Bc.	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
magisterských	magisterský	k2eAgFnPc2d1
(	(	kIx(
<g/>
Mgr.	Mgr.	kA
<g/>
)	)	kIx)
a	a	k8xC
doktorských	doktorský	k2eAgInPc2d1
(	(	kIx(
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
studijních	studijní	k2eAgInPc6d1
programech	program	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
a	a	k8xC
zakládající	zakládající	k2eAgFnSc7d1
fakultou	fakulta	k1gFnSc7
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
právnická	právnický	k2eAgFnSc1d1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
právnická	právnický	k2eAgFnSc1d1
ZČU	ZČU	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
právnická	právnický	k2eAgFnSc1d1
Západočeské	západočeský	k2eAgFnPc4d1
univerzity	univerzita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
FPR	FPR	kA
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
právnických	právnický	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
uskutečňuje	uskutečňovat	k5eAaImIp3nS
v	v	k7c6
bakalářském	bakalářský	k2eAgNnSc6d1
<g/>
,	,	kIx,
magisterském	magisterský	k2eAgNnSc6d1
<g/>
,	,	kIx,
navazujícím	navazující	k2eAgNnSc6d1
i	i	k8xC
doktorském	doktorský	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obory	obora	k1gFnPc1
jsou	být	k5eAaImIp3nP
zaměřené	zaměřený	k2eAgFnPc1d1
na	na	k7c4
studium	studium	k1gNnSc4
různých	různý	k2eAgFnPc2d1
forem	forma	k1gFnPc2
Práva	právo	k1gNnSc2
<g/>
,	,	kIx,
Veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
i	i	k8xC
Právních	právní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
disponuje	disponovat	k5eAaBmIp3nS
i	i	k9
právem	právem	k6eAd1
konat	konat	k5eAaImF
rigorózní	rigorózní	k2eAgFnSc4d1
zkoušku	zkouška	k1gFnSc4
(	(	kIx(
<g/>
JUDr.	JUDr.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc2d1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc2d1
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
fakultě	fakulta	k1gFnSc6
lze	lze	k6eAd1
studovat	studovat	k5eAaImF
bakalářské	bakalářský	k2eAgInPc4d1
<g/>
,	,	kIx,
navazující	navazující	k2eAgInPc4d1
magisterské	magisterský	k2eAgInPc4d1
i	i	k8xC
doktorské	doktorský	k2eAgInPc4d1
studijní	studijní	k2eAgInPc4d1
programy	program	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
fakult	fakulta	k1gFnPc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
zdravotnických	zdravotnický	k2eAgNnPc2d1
studií	studio	k1gNnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
zdravotnických	zdravotnický	k2eAgFnPc2d1
studií	studie	k1gFnPc2
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
zdravotnických	zdravotnický	k2eAgNnPc2d1
studií	studio	k1gNnPc2
si	se	k3xPyFc3
klade	klást	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
připravovat	připravovat	k5eAaImF
jak	jak	k8xC,k8xS
v	v	k7c6
rovině	rovina	k1gFnSc6
teoretické	teoretický	k2eAgMnPc4d1
tak	tak	k8xC,k8xS
praktické	praktický	k2eAgMnPc4d1
odborníky	odborník	k1gMnPc4
způsobilé	způsobilý	k2eAgFnSc2d1
k	k	k7c3
výkonu	výkon	k1gInSc3
nelékařského	lékařský	k2eNgNnSc2d1
zdravotnického	zdravotnický	k2eAgNnSc2d1
povolání	povolání	k1gNnSc2
v	v	k7c6
ČR	ČR	kA
s	s	k7c7
možností	možnost	k1gFnSc7
uplatnění	uplatnění	k1gNnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
členských	členský	k2eAgFnPc2d1
zemích	zem	k1gFnPc6
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s>
Ústav	ústav	k1gInSc1
jazykové	jazykový	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
</s>
<s>
UJP	UJP	kA
poskytuje	poskytovat	k5eAaImIp3nS
výuku	výuka	k1gFnSc4
devíti	devět	k4xCc2
jazyků	jazyk	k1gInPc2
pro	pro	k7c4
studenty	student	k1gMnPc4
ZČU	ZČU	kA
i	i	k9
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
pořádá	pořádat	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc4d1
letní	letní	k2eAgFnSc4d1
jazykovou	jazykový	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
–	–	k?
výzkumné	výzkumný	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
–	–	k?
výzkumné	výzkumný	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
ZČU	ZČU	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Nové	Nové	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
–	–	k?
výzkumné	výzkumný	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
–	–	k?
výzkumné	výzkumný	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
NTC	NTC	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nezávislý	závislý	k2eNgInSc1d1
vysokoškolský	vysokoškolský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
výzkumu	výzkum	k1gInSc2
<g/>
,	,	kIx,
vývoji	vývoj	k1gInSc3
a	a	k8xC
inovacím	inovace	k1gFnPc3
pro	pro	k7c4
průmyslové	průmyslový	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Jednotky	jednotka	k1gFnPc1
fakult	fakulta	k1gFnPc2
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
pro	pro	k7c4
informační	informační	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Nové	Nové	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
pro	pro	k7c4
informační	informační	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
Fakultě	fakulta	k1gFnSc6
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
vybudováno	vybudovat	k5eAaPmNgNnS
evropské	evropský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
excelence	excelence	k1gFnSc2
<g/>
,	,	kIx,
Nové	Nové	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
pro	pro	k7c4
informační	informační	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
(	(	kIx(
<g/>
NTIS	NTIS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkumné	výzkumný	k2eAgInPc1d1
programy	program	k1gInPc1
centra	centrum	k1gNnSc2
jsou	být	k5eAaImIp3nP
zaměřené	zaměřený	k2eAgInPc1d1
na	na	k7c4
výzkum	výzkum	k1gInSc4
<g/>
,	,	kIx,
vývoj	vývoj	k1gInSc1
a	a	k8xC
inovace	inovace	k1gFnSc1
v	v	k7c6
oboru	obor	k1gInSc6
informační	informační	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Regionální	regionální	k2eAgNnSc1d1
inovační	inovační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
elektrotechniky	elektrotechnika	k1gFnSc2
ZČU	ZČU	kA
</s>
<s>
Research	Research	k1gInSc1
and	and	k?
Innovation	Innovation	k1gInSc1
Centre	centr	k1gInSc5
for	forum	k1gNnPc2
Electrical	Electrical	k1gMnSc5
Engineering	Engineering	k1gInSc4
</s>
<s>
Research	Research	k1gInSc1
and	and	k?
Innovation	Innovation	k1gInSc1
Centre	centr	k1gInSc5
for	forum	k1gNnPc2
Electrical	Electrical	k1gMnSc1
Engineering	Engineering	k1gInSc1
(	(	kIx(
<g/>
RICE	RICE	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
výzkumným	výzkumný	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
Fakulty	fakulta	k1gFnSc2
elektrotechnické	elektrotechnický	k2eAgFnSc2d1
ZČU	ZČU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
nové	nový	k2eAgFnPc4d1
koncepce	koncepce	k1gFnPc4
pohonů	pohon	k1gInPc2
a	a	k8xC
technologií	technologie	k1gFnPc2
pro	pro	k7c4
dopravní	dopravní	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
řídicí	řídicí	k2eAgInPc4d1
systémy	systém	k1gInPc4
pro	pro	k7c4
dopravní	dopravní	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
a	a	k8xC
energetiku	energetika	k1gFnSc4
<g/>
,	,	kIx,
materiálový	materiálový	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
<g/>
,	,	kIx,
rozvoj	rozvoj	k1gInSc4
pokročilých	pokročilý	k2eAgFnPc2d1
jaderných	jaderný	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
diagnostiku	diagnostika	k1gFnSc4
<g/>
,	,	kIx,
matematické	matematický	k2eAgNnSc4d1
modelování	modelování	k1gNnSc4
a	a	k8xC
další	další	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Regionální	regionální	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institut	institut	k1gInSc1
</s>
<s>
Regionální	regionální	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institut	institut	k1gInSc1
(	(	kIx(
<g/>
RTI	RTI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
strojírenským	strojírenský	k2eAgNnSc7d1
a	a	k8xC
technologickým	technologický	k2eAgNnSc7d1
výzkumným	výzkumný	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
Fakulty	fakulta	k1gFnSc2
strojní	strojní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Specializuje	specializovat	k5eAaBmIp3nS
se	se	k3xPyFc4
na	na	k7c6
moderní	moderní	k2eAgFnSc6d1
konstrukci	konstrukce	k1gFnSc6
vozidel	vozidlo	k1gNnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc2
pohonných	pohonný	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
výrobní	výrobní	k2eAgInPc4d1
stroje	stroj	k1gInPc4
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
modernizaci	modernizace	k1gFnSc4
<g/>
,	,	kIx,
tvářecí	tvářecí	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
a	a	k8xC
technologie	technologie	k1gFnPc4
obrábění	obrábění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Katedra	katedra	k1gFnSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
</s>
<s>
Katedra	katedra	k1gFnSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Fakulty	fakulta	k1gFnSc2
strojní	strojní	k2eAgNnSc4d1
a	a	k8xC
zajišťuje	zajišťovat	k5eAaImIp3nS
tzv.	tzv.	kA
neoborovou	oborový	k2eNgFnSc4d1
tělesnou	tělesný	k2eAgFnSc4d1
výchovu	výchova	k1gFnSc4
pro	pro	k7c4
studenty	student	k1gMnPc4
všech	všecek	k3xTgFnPc2
fakult	fakulta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
toho	ten	k3xDgInSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
sportovní	sportovní	k2eAgFnSc3d1
reprezentaci	reprezentace	k1gFnSc3
ZČU	ZČU	kA
<g/>
,	,	kIx,
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
ČAUS	ČAUS	kA
pak	pak	k6eAd1
organizuje	organizovat	k5eAaBmIp3nS
akademické	akademický	k2eAgFnPc4d1
mistrovské	mistrovský	k2eAgFnPc4d1
i	i	k8xC
nemistrovské	mistrovský	k2eNgFnPc4d1
sportovní	sportovní	k2eAgFnPc4d1
soutěže	soutěž	k1gFnPc4
<g/>
,	,	kIx,
soutěže	soutěž	k1gFnPc4
pro	pro	k7c4
studenty	student	k1gMnPc4
i	i	k9
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Western	Western	kA
Stars	Stars	k1gInSc1
</s>
<s>
Univerzitní	univerzitní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
tým	tým	k1gInSc1
Western	Western	kA
Stars	Stars	k1gInSc1
při	při	k7c6
Západočeské	západočeský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
sdružuje	sdružovat	k5eAaImIp3nS
sportovně	sportovně	k6eAd1
nadané	nadaný	k2eAgMnPc4d1
studenty	student	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
reprezentují	reprezentovat	k5eAaImIp3nP
svou	svůj	k3xOyFgFnSc4
alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
i	i	k9
svou	svůj	k3xOyFgFnSc4
vlast	vlast	k1gFnSc4
na	na	k7c6
světových	světový	k2eAgFnPc6d1
meziuniverzitních	meziuniverzitní	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Českých	český	k2eAgFnPc6d1
akademických	akademický	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c6
vrcholných	vrcholný	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
<g/>
,	,	kIx,
např.	např.	kA
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
a	a	k8xC
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
Světovém	světový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
,	,	kIx,
Univerziádě	univerziáda	k1gFnSc6
apod.	apod.	kA
</s>
<s>
Centrum	centrum	k1gNnSc1
afrických	africký	k2eAgFnPc2d1
studií	studie	k1gFnPc2
v	v	k7c6
Etiopii	Etiopie	k1gFnSc6
</s>
<s>
ZČU	ZČU	kA
založila	založit	k5eAaPmAgFnS
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2014	#num#	k4
pobočku	pobočka	k1gFnSc4
v	v	k7c6
Etiopii	Etiopie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
zřízena	zřídit	k5eAaPmNgFnS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Jimmě	Jimma	k1gFnSc6
jako	jako	k8xC,k8xS
společné	společný	k2eAgFnSc3d1
vzdělávací	vzdělávací	k2eAgFnPc4d1
a	a	k8xC
výzkumné	výzkumný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
pod	pod	k7c7
názvem	název	k1gInSc7
Centrum	centrum	k1gNnSc1
afrických	africký	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
tak	tak	k6eAd1
učinila	učinit	k5eAaImAgFnS,k5eAaPmAgFnS
se	s	k7c7
záměrem	záměr	k1gInSc7
stát	stát	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
ohniskem	ohnisko	k1gNnSc7
výzkumu	výzkum	k1gInSc2
a	a	k8xC
vzdělávání	vzdělávání	k1gNnSc2
zaměřeného	zaměřený	k2eAgNnSc2d1
na	na	k7c4
subsaharskou	subsaharský	k2eAgFnSc4d1
Afriku	Afrika	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
součásti	součást	k1gFnPc1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Knihovna	knihovna	k1gFnSc1
ZČU	ZČU	kA
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Univerzitní	univerzitní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
byla	být	k5eAaImAgFnS
zřízena	zřídit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
5	#num#	k4
dílčích	dílčí	k2eAgFnPc2d1
knihoven	knihovna	k1gFnPc2
<g/>
:	:	kIx,
Knihovna	knihovna	k1gFnSc1
Bory	bor	k1gInPc4
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
a	a	k8xC
právnická	právnický	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
,	,	kIx,
Knihovna	knihovna	k1gFnSc1
zdravotnických	zdravotnický	k2eAgNnPc2d1
studií	studio	k1gNnPc2
<g/>
,	,	kIx,
Pedagogická	pedagogický	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
a	a	k8xC
Ekonomická	ekonomický	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
v	v	k7c6
Chebu	Cheb	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
univerzitní	univerzitní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
jsou	být	k5eAaImIp3nP
i	i	k9
Oddělení	oddělení	k1gNnSc4
vydavatelství	vydavatelství	k1gNnSc2
a	a	k8xC
tiskových	tiskový	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Univerzitní	univerzitní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
ročně	ročně	k6eAd1
kolem	kolem	k7c2
100	#num#	k4
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Centrum	centrum	k1gNnSc1
informatizace	informatizace	k1gFnSc2
a	a	k8xC
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
</s>
<s>
Centrum	centrum	k1gNnSc1
informatizace	informatizace	k1gFnSc2
a	a	k8xC
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
provozuje	provozovat	k5eAaImIp3nS
a	a	k8xC
rozvíjí	rozvíjet	k5eAaImIp3nS
informační	informační	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
a	a	k8xC
informační	informační	k2eAgInPc4d1
systémy	systém	k1gInPc4
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
ZČU	ZČU	kA
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
také	také	k9
vyvíjen	vyvíjen	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
pro	pro	k7c4
administraci	administrace	k1gFnSc4
studijní	studijní	k2eAgFnSc2d1
agendy	agenda	k1gFnSc2
IS	IS	kA
<g/>
/	/	kIx~
<g/>
STAG	STAG	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
užívá	užívat	k5eAaImIp3nS
14	#num#	k4
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
a	a	k8xC
univerzit	univerzita	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Celoživotní	celoživotní	k2eAgNnSc1d1
vzdělávání	vzdělávání	k1gNnSc1
</s>
<s>
Oddělení	oddělení	k1gNnSc1
koncepce	koncepce	k1gFnSc2
celoživotního	celoživotní	k2eAgMnSc2d1
a	a	k8xC
distančního	distanční	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
ZČU	ZČU	kA
připravuje	připravovat	k5eAaImIp3nS
prezenční	prezenční	k2eAgFnSc1d1
i	i	k9
on-line	on-lin	k1gInSc5
kurzy	kurz	k1gInPc7
k	k	k7c3
pracovnímu	pracovní	k2eAgInSc3d1
a	a	k8xC
osobnímu	osobní	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
pro	pro	k7c4
zaměstnance	zaměstnanec	k1gMnPc4
i	i	k8xC
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozuje	provozovat	k5eAaImIp3nS
popularizační	popularizační	k2eAgInSc1d1
portál	portál	k1gInSc1
Bav	bavit	k5eAaImRp2nS
se	se	k3xPyFc4
vědou	věda	k1gFnSc7
a	a	k8xC
připravuje	připravovat	k5eAaImIp3nS
příležitosti	příležitost	k1gFnPc4
pro	pro	k7c4
popularizaci	popularizace	k1gFnSc4
vědy	věda	k1gFnSc2
na	na	k7c6
základních	základní	k2eAgFnPc6d1
a	a	k8xC
středních	střední	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
pořádá	pořádat	k5eAaImIp3nS
konferenci	konference	k1gFnSc4
Učitel-IN	Učitel-IN	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
využití	využití	k1gNnSc3
ICT	ICT	kA
(	(	kIx(
<g/>
informačních	informační	k2eAgFnPc2d1
a	a	k8xC
komunikačních	komunikační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
)	)	kIx)
ve	v	k7c6
vzdělávání	vzdělávání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Univerzita	univerzita	k1gFnSc1
třetího	třetí	k4xOgInSc2
věku	věk	k1gInSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
třetího	třetí	k4xOgInSc2
věku	věk	k1gInSc2
(	(	kIx(
<g/>
U	u	k7c2
<g/>
3	#num#	k4
<g/>
V	V	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
program	program	k1gInSc1
neprofesního	profesní	k2eNgNnSc2d1
celoživotního	celoživotní	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
pro	pro	k7c4
zájemce	zájemce	k1gMnPc4
ve	v	k7c6
starobním	starobní	k2eAgInSc6d1
či	či	k8xC
plném	plný	k2eAgInSc6d1
invalidním	invalidní	k2eAgInSc6d1
důchodu	důchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
přednášky	přednáška	k1gFnPc4
<g/>
,	,	kIx,
semináře	seminář	k1gInPc4
<g/>
,	,	kIx,
exkurze	exkurze	k1gFnPc4
i	i	k8xC
letní	letní	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
široké	široký	k2eAgFnSc6d1
škále	škála	k1gFnSc6
témat	téma	k1gNnPc2
od	od	k7c2
historie	historie	k1gFnSc2
přes	přes	k7c4
astronomii	astronomie	k1gFnSc4
<g/>
,	,	kIx,
strojírenství	strojírenství	k1gNnSc4
a	a	k8xC
techniku	technika	k1gFnSc4
<g/>
,	,	kIx,
literaturu	literatura	k1gFnSc4
až	až	k9
po	po	k7c4
moderní	moderní	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výuka	výuka	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
nejen	nejen	k6eAd1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
dalších	další	k2eAgNnPc6d1
městech	město	k1gNnPc6
Plzeňského	plzeňský	k2eAgInSc2d1
a	a	k8xC
Karlovarského	karlovarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Informační	informační	k2eAgNnSc1d1
a	a	k8xC
poradenské	poradenský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
</s>
<s>
Informační	informační	k2eAgNnSc1d1
a	a	k8xC
poradenské	poradenský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
ZČU	ZČU	kA
nabízí	nabízet	k5eAaImIp3nS
studijní	studijní	k2eAgInSc1d1
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnSc2d1
<g/>
,	,	kIx,
psychologické	psychologický	k2eAgFnSc2d1
a	a	k8xC
právní	právní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
klienty	klient	k1gMnPc7
jsou	být	k5eAaImIp3nP
uchazeči	uchazeč	k1gMnPc1
o	o	k7c4
studium	studium	k1gNnSc4
a	a	k8xC
stávající	stávající	k2eAgMnPc1d1
studenti	student	k1gMnPc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
absolventi	absolvent	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
podpory	podpora	k1gFnSc2
je	být	k5eAaImIp3nS
doplněn	doplnit	k5eAaPmNgInS
o	o	k7c4
organizaci	organizace	k1gFnSc4
pomoci	pomoc	k1gFnSc2
pro	pro	k7c4
osoby	osoba	k1gFnPc4
se	s	k7c7
specifickými	specifický	k2eAgFnPc7d1
potřebami	potřeba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrum	centrum	k1gNnSc1
také	také	k6eAd1
koordinuje	koordinovat	k5eAaBmIp3nS
aktivity	aktivita	k1gFnPc4
Akademického	akademický	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
studentská	studentský	k2eAgFnSc1d1
klubovna	klubovna	k1gFnSc1
a	a	k8xC
coworkingové	coworkingový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Kulturka	Kulturka	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
zabývá	zabývat	k5eAaImIp3nS
se	s	k7c7
projektovou	projektový	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Kulturka	Kulturka	k1gFnSc1
–	–	k?
klubovna	klubovna	k1gFnSc1
a	a	k8xC
coworking	coworking	k1gInSc1
</s>
<s>
Kulturka	Kulturka	k1gFnSc1
</s>
<s>
Univerzitní	univerzitní	k2eAgInSc1d1
prostor	prostor	k1gInSc1
Kulturka	Kulturka	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
centru	centrum	k1gNnSc6
Plzně	Plzeň	k1gFnSc2
a	a	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
studentskou	studentský	k2eAgFnSc4d1
klubovnu	klubovna	k1gFnSc4
a	a	k8xC
univerzitní	univerzitní	k2eAgInSc4d1
coworking	coworking	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgNnPc1
zázemí	zázemí	k1gNnPc1
jsou	být	k5eAaImIp3nP
využívána	využíván	k2eAgNnPc1d1
k	k	k7c3
pořádání	pořádání	k1gNnSc3
formálních	formální	k2eAgFnPc2d1
i	i	k8xC
neformálních	formální	k2eNgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1
kavárna	kavárna	k1gFnSc1
Družba	družba	k1gFnSc1
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1
kavárna	kavárna	k1gFnSc1
Družba	družba	k1gFnSc1
je	být	k5eAaImIp3nS
jedinou	jediný	k2eAgFnSc7d1
družstevní	družstevní	k2eAgFnSc7d1
kavárnou	kavárna	k1gFnSc7
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
provozovanou	provozovaný	k2eAgFnSc7d1
absolventy	absolvent	k1gMnPc7
<g/>
,	,	kIx,
pracovníky	pracovník	k1gMnPc7
a	a	k8xC
studenty	student	k1gMnPc7
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Družstvo	družstvo	k1gNnSc1
provozuje	provozovat	k5eAaImIp3nS
kavárnu	kavárna	k1gFnSc4
jako	jako	k8xC,k8xS
otevřený	otevřený	k2eAgInSc4d1
kulturní	kulturní	k2eAgInSc4d1
a	a	k8xC
společenský	společenský	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
,	,	kIx,
snažící	snažící	k2eAgFnSc4d1
se	se	k3xPyFc4
o	o	k7c4
bezodpadový	bezodpadový	k2eAgInSc4d1
provoz	provoz	k1gInSc4
a	a	k8xC
podporu	podpora	k1gFnSc4
etické	etický	k2eAgFnSc2d1
<g/>
,	,	kIx,
ekologické	ekologický	k2eAgFnSc2d1
a	a	k8xC
pokud	pokud	k8xS
možno	možno	k6eAd1
lokální	lokální	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kavárně	kavárna	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
semestru	semestr	k1gInSc2
i	i	k9
mimo	mimo	k7c4
něj	on	k3xPp3gMnSc4
konají	konat	k5eAaImIp3nP
pravidelně	pravidelně	k6eAd1
akce	akce	k1gFnPc4
organizovány	organizován	k2eAgFnPc4d1
studenty	student	k1gMnPc7
<g/>
,	,	kIx,
katedrami	katedra	k1gFnPc7
nebo	nebo	k8xC
neziskovými	ziskový	k2eNgFnPc7d1
organizacemi	organizace	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
pokrývají	pokrývat	k5eAaImIp3nP
široké	široký	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
témat	téma	k1gNnPc2
a	a	k8xC
žánrů	žánr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zámek	zámek	k1gInSc1
Nečtiny	Nečtina	k1gFnSc2
</s>
<s>
Zámek	zámek	k1gInSc1
Nečtiny	Nečtina	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Nečtiny	Nečtina	k1gFnSc2
(	(	kIx(
<g/>
zámek	zámek	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
ZČU	ZČU	kA
je	být	k5eAaImIp3nS
vlastníkem	vlastník	k1gMnSc7
zámku	zámek	k1gInSc2
Nečtiny	Nečtina	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
její	její	k3xOp3gNnSc1
školící	školící	k2eAgNnSc1d1
a	a	k8xC
rekreační	rekreační	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
kapacity	kapacita	k1gFnPc1
pro	pro	k7c4
konference	konference	k1gFnPc4
a	a	k8xC
školení	školení	k1gNnPc4
<g/>
,	,	kIx,
sportovní	sportovní	k2eAgFnPc4d1
a	a	k8xC
turistické	turistický	k2eAgFnPc4d1
akce	akce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zámecký	zámecký	k2eAgInSc1d1
park	park	k1gInSc1
je	být	k5eAaImIp3nS
volně	volně	k6eAd1
přístupný	přístupný	k2eAgInSc1d1
veřejnosti	veřejnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Budovy	budova	k1gFnPc1
ZČU	ZČU	kA
</s>
<s>
Výstavba	výstavba	k1gFnSc1
univerzitního	univerzitní	k2eAgInSc2d1
kampusu	kampus	k1gInSc2
</s>
<s>
Po	po	k7c6
svém	svůj	k3xOyFgInSc6
vzniku	vznik	k1gInSc6
sídlila	sídlit	k5eAaImAgFnS
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
(	(	kIx(
<g/>
VŠSE	VŠSE	kA
<g/>
)	)	kIx)
v	v	k7c6
mnoha	mnoho	k4c6
pronajatých	pronajatý	k2eAgFnPc6d1
i	i	k8xC
vlastních	vlastní	k2eAgFnPc6d1
prostorách	prostora	k1gFnPc6
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Plzni	Plzeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
uvažovat	uvažovat	k5eAaImF
o	o	k7c6
výstavbě	výstavba	k1gFnSc6
nového	nový	k2eAgInSc2d1
areálu	areál	k1gInSc2
na	na	k7c6
Borských	Borských	k2eAgNnPc6d1
polích	pole	k1gNnPc6
s	s	k7c7
univerzitním	univerzitní	k2eAgNnSc7d1
zázemím	zázemí	k1gNnSc7
<g/>
,	,	kIx,
příslušnou	příslušný	k2eAgFnSc7d1
infrastrukturou	infrastruktura	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
hlavně	hlavně	k9
se	s	k7c7
zajištěním	zajištění	k1gNnSc7
výuky	výuka	k1gFnSc2
koncentrovaně	koncentrovaně	k6eAd1
na	na	k7c6
jednom	jeden	k4xCgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
byl	být	k5eAaImAgInS
položen	položit	k5eAaPmNgInS
základní	základní	k2eAgInSc1d1
kámen	kámen	k1gInSc1
budovy	budova	k1gFnSc2
Fakulty	fakulta	k1gFnSc2
strojní	strojní	k2eAgMnSc1d1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
vlaštovka	vlaštovka	k1gFnSc1
univerzitního	univerzitní	k2eAgInSc2d1
kampusu	kampus	k1gInSc2
přezdívaného	přezdívaný	k2eAgInSc2d1
„	„	k?
<g/>
Zelený	zelený	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
zde	zde	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
rektorát	rektorát	k1gInSc4
<g/>
,	,	kIx,
pět	pět	k4xCc1
fakult	fakulta	k1gFnPc2
<g/>
,	,	kIx,
Ústav	ústav	k1gInSc1
jazykové	jazykový	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
<g/>
,	,	kIx,
tři	tři	k4xCgInPc4
fakultní	fakultní	k2eAgInPc4d1
výzkumné	výzkumný	k2eAgInPc4d1
ústavy	ústav	k1gInPc4
<g/>
,	,	kIx,
Centrum	centrum	k1gNnSc1
informatizace	informatizace	k1gFnSc2
a	a	k8xC
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
(	(	kIx(
<g/>
CIV	civět	k5eAaImRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tělocvičny	tělocvična	k1gFnSc2
<g/>
,	,	kIx,
menza	menza	k1gFnSc1
<g/>
,	,	kIx,
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
i	i	k9
několik	několik	k4yIc4
kaváren	kavárna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Kampus	kampus	k1gInSc1
ZČU	ZČU	kA
</s>
<s>
Historie	historie	k1gFnSc1
univerzitního	univerzitní	k2eAgInSc2d1
kampusu	kampus	k1gInSc2
ZČU	ZČU	kA
v	v	k7c6
bodech	bod	k1gInPc6
</s>
<s>
1964	#num#	k4
–	–	k?
vypracován	vypracován	k2eAgInSc1d1
investiční	investiční	k2eAgInSc1d1
záměr	záměr	k1gInSc1
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
nového	nový	k2eAgInSc2d1
areálu	areál	k1gInSc2
VŠSE	VŠSE	kA
na	na	k7c6
Borských	Borských	k2eAgNnPc6d1
polích	pole	k1gNnPc6
</s>
<s>
1980	#num#	k4
–	–	k?
vznik	vznik	k1gInSc1
studie	studie	k1gFnSc2
areálu	areál	k1gInSc2
„	„	k?
<g/>
Zelený	zelený	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
<g/>
“	“	k?
od	od	k7c2
architekta	architekt	k1gMnSc2
Pavla	Pavel	k1gMnSc2
Němečka	Němeček	k1gMnSc2
</s>
<s>
1985	#num#	k4
–	–	k?
položení	položení	k1gNnSc1
základního	základní	k2eAgInSc2d1
kamene	kámen	k1gInSc2
Fakulty	fakulta	k1gFnSc2
strojní	strojní	k2eAgFnSc2d1
na	na	k7c6
Borských	Borských	k2eAgNnPc6d1
polích	pole	k1gNnPc6
</s>
<s>
1992	#num#	k4
–	–	k?
dokončení	dokončení	k1gNnSc4
budovy	budova	k1gFnSc2
Fakulty	fakulta	k1gFnSc2
strojní	strojní	k2eAgMnSc1d1
<g/>
;	;	kIx,
do	do	k7c2
jejích	její	k3xOp3gFnPc2
prostor	prostora	k1gFnPc2
se	se	k3xPyFc4
nastěhovaly	nastěhovat	k5eAaPmAgFnP
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgMnPc1d1
a	a	k8xC
Fakulta	fakulta	k1gFnSc1
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
</s>
<s>
1996	#num#	k4
–	–	k?
dokončení	dokončení	k1gNnSc2
budov	budova	k1gFnPc2
rektorátu	rektorát	k1gInSc2
<g/>
,	,	kIx,
menzy	menza	k1gFnSc2
i	i	k8xC
tělocvičny	tělocvična	k1gFnSc2
Katedry	katedra	k1gFnSc2
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
</s>
<s>
1998	#num#	k4
–	–	k?
dokončení	dokončení	k1gNnSc4
budovy	budova	k1gFnSc2
Centra	centrum	k1gNnSc2
informatizace	informatizace	k1gFnSc2
a	a	k8xC
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
(	(	kIx(
<g/>
CIV	civět	k5eAaImRp2nS
<g/>
)	)	kIx)
</s>
<s>
2001	#num#	k4
–	–	k?
dokončení	dokončení	k1gNnSc4
budovy	budova	k1gFnSc2
Univerzitní	univerzitní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
</s>
<s>
2004	#num#	k4
–	–	k?
dokončení	dokončení	k1gNnSc4
budovy	budova	k1gFnSc2
Fakulty	fakulta	k1gFnSc2
elektrotechnické	elektrotechnický	k2eAgFnSc2d1
</s>
<s>
2012	#num#	k4
–	–	k?
dokončení	dokončení	k1gNnSc2
budov	budova	k1gFnPc2
Fakulty	fakulta	k1gFnSc2
designu	design	k1gInSc2
a	a	k8xC
umění	umění	k1gNnSc2
Ladislava	Ladislav	k1gMnSc2
Sutnara	Sutnar	k1gMnSc2
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Regionálního	regionální	k2eAgInSc2d1
technologického	technologický	k2eAgInSc2d1
institutu	institut	k1gInSc2
(	(	kIx(
<g/>
RTI	RTI	kA
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
dokončení	dokončení	k1gNnSc3
budovy	budova	k1gFnSc2
Fakulty	fakulta	k1gFnSc2
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
výzkumného	výzkumný	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
Nové	Nové	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
pro	pro	k7c4
informační	informační	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
(	(	kIx(
<g/>
NTIS	NTIS	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
dokončení	dokončení	k1gNnSc1
přístavby	přístavba	k1gFnSc2
knihovny	knihovna	k1gFnSc2
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2015	#num#	k4
–	–	k?
dokončení	dokončení	k1gNnSc4
budovy	budova	k1gFnSc2
Regionálního	regionální	k2eAgNnSc2d1
inovačního	inovační	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
elektrotechniky	elektrotechnika	k1gFnSc2
(	(	kIx(
<g/>
RICE	RICE	kA
<g/>
)	)	kIx)
</s>
<s>
2016	#num#	k4
–	–	k?
přestěhování	přestěhování	k1gNnSc2
Fakulty	fakulta	k1gFnSc2
ekonomické	ekonomický	k2eAgFnSc2d1
do	do	k7c2
budovy	budova	k1gFnSc2
Fakulty	fakulta	k1gFnSc2
strojní	strojní	k2eAgFnSc4d1
</s>
<s>
2018	#num#	k4
–	–	k?
dokončení	dokončení	k1gNnSc4
přístavby	přístavba	k1gFnSc2
menzy	menza	k1gFnSc2
</s>
<s>
2019	#num#	k4
–	–	k?
rozšíření	rozšíření	k1gNnSc4
tramvajové	tramvajový	k2eAgFnSc2d1
linky	linka	k1gFnSc2
k	k	k7c3
univerzitnímu	univerzitní	k2eAgInSc3d1
kampusu	kampus	k1gInSc3
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Budovy	budova	k1gFnPc1
ZČU	ZČU	kA
v	v	k7c6
kampusu	kampus	k1gInSc6
a	a	k8xC
ve	v	k7c6
městě	město	k1gNnSc6
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
se	se	k3xPyFc4
rozšiřoval	rozšiřovat	k5eAaImAgMnS
nejen	nejen	k6eAd1
univerzitní	univerzitní	k2eAgInSc4d1
kampus	kampus	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
šíře	šíře	k1gFnSc1
poskytovaného	poskytovaný	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
„	„	k?
<g/>
Zelený	zelený	k2eAgInSc4d1
trojúhelník	trojúhelník	k1gInSc4
<g/>
“	“	k?
se	se	k3xPyFc4
tak	tak	k6eAd1
mohla	moct	k5eAaImAgFnS
přestěhovat	přestěhovat	k5eAaPmF
jen	jen	k9
část	část	k1gFnSc4
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
některé	některý	k3yIgFnPc1
fakulty	fakulta	k1gFnPc1
a	a	k8xC
součásti	součást	k1gFnPc1
univerzity	univerzita	k1gFnSc2
zůstávají	zůstávat	k5eAaImIp3nP
v	v	k7c6
centru	centrum	k1gNnSc6
Plzně	Plzeň	k1gFnSc2
a	a	k8xC
výzkumný	výzkumný	k2eAgInSc1d1
ústav	ústav	k1gInSc1
NTC	NTC	kA
je	být	k5eAaImIp3nS
situován	situovat	k5eAaBmNgInS
na	na	k7c6
vzdálenějším	vzdálený	k2eAgInSc6d2
konci	konec	k1gInSc6
Borských	Borských	k2eAgFnPc2d1
polí	pole	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
univerzitních	univerzitní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
</s>
<s>
Univerzitní	univerzitní	k2eAgInSc1d1
kampus	kampus	k1gInSc1
</s>
<s>
Rektorát	rektorát	k1gInSc1
–	–	k?
Univerzitní	univerzitní	k2eAgInSc1d1
2732	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
a	a	k8xC
Regionální	regionální	k2eAgNnSc1d1
inovační	inovační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
elektrotechniky	elektrotechnika	k1gFnSc2
(	(	kIx(
<g/>
RICE	RICE	kA
<g/>
)	)	kIx)
–	–	k?
Univerzitní	univerzitní	k2eAgInSc4d1
2795	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
a	a	k8xC
Regionální	regionální	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
Fakulta	fakulta	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Ústav	ústav	k1gInSc1
jazykové	jazykový	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
–	–	k?
Univerzitní	univerzitní	k2eAgNnSc1d1
2762	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
</s>
<s>
Centrum	centrum	k1gNnSc1
informatizace	informatizace	k1gFnSc2
a	a	k8xC
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
–	–	k?
Univerzitní	univerzitní	k2eAgNnSc1d1
2746	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
International	International	k1gFnSc7
Office	Office	kA
<g/>
,	,	kIx,
Informačním	informační	k2eAgNnSc7d1
a	a	k8xC
poradenským	poradenský	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
na	na	k7c6
Borech	bor	k1gInPc6
–	–	k?
Univerzitní	univerzitní	k2eAgNnSc1d1
2763	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
designu	design	k1gInSc2
a	a	k8xC
umění	umění	k1gNnSc2
Ladislava	Ladislav	k1gMnSc2
Sutnara	Sutnar	k1gMnSc2
–	–	k?
Univerzitní	univerzitní	k2eAgInSc4d1
2954	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
</s>
<s>
Menza	menza	k1gFnSc1
na	na	k7c6
Borech	bor	k1gInPc6
–	–	k?
Univerzitní	univerzitní	k2eAgNnSc1d1
12	#num#	k4
</s>
<s>
Katedra	katedra	k1gFnSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
–	–	k?
Univerzitní	univerzitní	k2eAgInSc1d1
2765	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
výzkumné	výzkumný	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
NTIS	NTIS	kA
–	–	k?
Technická	technický	k2eAgFnSc1d1
2967	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
</s>
<s>
Centrum	centrum	k1gNnSc1
Plzně	Plzeň	k1gFnSc2
<g/>
,	,	kIx,
další	další	k2eAgNnPc1d1
místa	místo	k1gNnPc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
</s>
<s>
Fakulta	fakulta	k1gFnSc1
právnická	právnický	k2eAgFnSc1d1
–	–	k?
sady	sad	k1gInPc4
Pětatřicátníků	pětatřicátník	k1gMnPc2
320	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
filozofická	filozofický	k2eAgFnSc1d1
–	–	k?
komplex	komplex	k1gInSc1
budov	budova	k1gFnPc2
Sedláčkova	Sedláčkův	k2eAgInSc2d1
36-40	36-40	k4
a	a	k8xC
Veleslavínova	Veleslavínův	k2eAgFnSc1d1
27-29	27-29	k4
(	(	kIx(
<g/>
děkanát	děkanát	k1gInSc1
<g/>
,	,	kIx,
studijní	studijní	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
<g/>
,	,	kIx,
KAR	kar	k1gInSc1
<g/>
,	,	kIx,
KHV	KHV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sedláčkova	Sedláčkův	k2eAgFnSc1d1
214	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
(	(	kIx(
<g/>
KAJ	kát	k5eAaImRp2nS
<g/>
,	,	kIx,
KBS	KBS	kA
<g/>
,	,	kIx,
KAR	kar	k1gInSc1
–	–	k?
laboratoře	laboratoř	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
budova	budova	k1gFnSc1
na	na	k7c6
rohu	roh	k1gInSc6
ulic	ulice	k1gFnPc2
Sedláčkova	Sedláčkův	k2eAgNnSc2d1
216	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
a	a	k8xC
Riegrova	Riegrov	k1gInSc2
217	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
(	(	kIx(
<g/>
KAG	KAG	kA
<g/>
,	,	kIx,
KRO	kra	k1gFnSc5
<g/>
,	,	kIx,
KSJ	KSJ	kA
<g/>
,	,	kIx,
KFI	KFI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jungmannova	Jungmannův	k2eAgFnSc1d1
153	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
(	(	kIx(
<g/>
KAP	kap	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Fakulta	fakulta	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
–	–	k?
Veleslavínova	Veleslavínův	k2eAgFnSc1d1
342	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
(	(	kIx(
<g/>
KČJ	KČJ	kA
<g/>
,	,	kIx,
KHI	KHI	kA
<g/>
,	,	kIx,
KCH	kch	k0
<g/>
,	,	kIx,
KRF	KRF	kA
<g/>
,	,	kIx,
děkanát	děkanát	k1gInSc4
<g/>
,	,	kIx,
studijní	studijní	k2eAgNnSc4d1
oddělení	oddělení	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dominikánská	dominikánský	k2eAgFnSc1d1
284	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
Chodské	chodský	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
1015	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
(	(	kIx(
<g/>
KBI	KBI	kA
<g/>
,	,	kIx,
KPS	KPS	kA
<g/>
,	,	kIx,
KPG	KPG	kA
<g/>
,	,	kIx,
KAN	KAN	kA
<g/>
,	,	kIx,
KNJ	KNJ	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Klatovská	klatovský	k2eAgFnSc1d1
třída	třída	k1gFnSc1
1736	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
(	(	kIx(
<g/>
KTV	KTV	kA
<g/>
,	,	kIx,
KVD	KVD	kA
<g/>
,	,	kIx,
KVK	KVK	kA
<g/>
,	,	kIx,
KMT	KMT	kA
<g/>
,	,	kIx,
knihovna	knihovna	k1gFnSc1
FPE	FPE	kA
<g/>
)	)	kIx)
</s>
<s>
Fakulta	fakulta	k1gFnSc1
zdravotnických	zdravotnický	k2eAgNnPc2d1
studií	studio	k1gNnPc2
–	–	k?
Husova	Husův	k2eAgNnSc2d1
664	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
Dominikánská	dominikánský	k2eAgFnSc1d1
284	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
Sedláčkova	Sedláčkův	k2eAgFnSc1d1
252	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
(	(	kIx(
<g/>
knihovna	knihovna	k1gFnSc1
FZS	FZS	kA
<g/>
)	)	kIx)
</s>
<s>
Univerzita	univerzita	k1gFnSc1
třetího	třetí	k4xOgInSc2
věku	věk	k1gInSc2
a	a	k8xC
Ústav	ústav	k1gInSc1
jazykové	jazykový	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
(	(	kIx(
<g/>
učebny	učebna	k1gFnSc2
v	v	k7c6
centru	centrum	k1gNnSc6
<g/>
)	)	kIx)
–	–	k?
Jungmannova	Jungmannov	k1gInSc2
153	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
</s>
<s>
Menza	menza	k1gFnSc1
Kollárova	Kollárův	k2eAgFnSc1d1
–	–	k?
Kollárova	Kollárův	k2eAgFnSc1d1
1239	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Galerie	galerie	k1gFnSc1
Ladislava	Ladislav	k1gMnSc2
Sutnara	Sutnar	k1gMnSc2
–	–	k?
Riegrova	Riegrov	k1gInSc2
217	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
Fakulty	fakulta	k1gFnSc2
právnické	právnický	k2eAgFnSc2d1
a	a	k8xC
Fakulty	fakulta	k1gFnSc2
filozofické	filozofický	k2eAgFnSc2d1
–	–	k?
sady	sada	k1gFnSc2
Pětatřicátníků	pětatřicátník	k1gMnPc2
321	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Kavárna	kavárna	k1gFnSc1
Družba	družba	k1gFnSc1
–	–	k?
Sedláčkova	Sedláčkův	k2eAgFnSc1d1
216	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Kulturka	Kulturka	k1gFnSc1
–	–	k?
Sedláčkova	Sedláčkův	k2eAgFnSc1d1
216	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Výzkumné	výzkumný	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
NTC	NTC	kA
–	–	k?
Teslova	Teslův	k2eAgFnSc1d1
9	#num#	k4
<g/>
,	,	kIx,
Teslova	Teslův	k2eAgFnSc1d1
9	#num#	k4
<g/>
a	a	k8xC
<g/>
,	,	kIx,
Teslova	Teslův	k2eAgFnSc1d1
11	#num#	k4
<g/>
,	,	kIx,
Teslova	Teslův	k2eAgFnSc1d1
5	#num#	k4
<g/>
b	b	k?
(	(	kIx(
<g/>
Plzeňský	plzeňský	k2eAgInSc1d1
vědecko-technologický	vědecko-technologický	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
,	,	kIx,
budovy	budova	k1gFnPc1
F	F	kA
<g/>
,	,	kIx,
G	G	kA
<g/>
,	,	kIx,
H	H	kA
<g/>
,	,	kIx,
C	C	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cheb	Cheb	k1gInSc1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Chebu	Cheb	k1gInSc6
–	–	k?
Hradební	hradební	k2eAgInSc1d1
22	#num#	k4
<g/>
/	/	kIx~
<g/>
2047	#num#	k4
</s>
<s>
Letní	letní	k2eAgFnPc1d1
školy	škola	k1gFnPc1
</s>
<s>
Prázdninové	prázdninový	k2eAgInPc1d1
měsíce	měsíc	k1gInPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
Západočeské	západočeský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
každoročně	každoročně	k6eAd1
ve	v	k7c6
znamení	znamení	k1gNnSc6
letních	letní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
určených	určený	k2eAgFnPc2d1
dětem	dítě	k1gFnPc3
<g/>
,	,	kIx,
středoškolákům	středoškolák	k1gMnPc3
<g/>
,	,	kIx,
vysokoškolákům	vysokoškolák	k1gMnPc3
i	i	k8xC
široké	široký	k2eAgFnSc3d1
veřejnosti	veřejnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
dvou	dva	k4xCgFnPc2
největších	veliký	k2eAgFnPc2d3
tradičních	tradiční	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
,	,	kIx,
ArtCamp	ArtCamp	k1gInSc1
a	a	k8xC
MLJŠ	MLJŠ	kA
<g/>
,	,	kIx,
pořádá	pořádat	k5eAaImIp3nS
ZČU	ZČU	kA
ještě	ještě	k6eAd1
mnoho	mnoho	k6eAd1
dalších	další	k2eAgFnPc2d1
letních	letní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
zaměřených	zaměřený	k2eAgFnPc2d1
na	na	k7c4
techniku	technika	k1gFnSc4
<g/>
,	,	kIx,
ekonomiku	ekonomika	k1gFnSc4
<g/>
,	,	kIx,
sport	sport	k1gInSc4
i	i	k8xC
kreativitu	kreativita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
letní	letní	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
pořádá	pořádat	k5eAaImIp3nS
ZČU	ZČU	kA
v	v	k7c6
červenci	červenec	k1gInSc6
letní	letní	k2eAgInPc4d1
jazykové	jazykový	k2eAgInPc4d1
kurzy	kurz	k1gInPc4
pro	pro	k7c4
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc4
<g/>
,	,	kIx,
děti	dítě	k1gFnPc4
i	i	k8xC
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyučuje	vyučovat	k5eAaImIp3nS
se	s	k7c7
zde	zde	k6eAd1
8	#num#	k4
jazyků	jazyk	k1gInPc2
v	v	k7c6
kurzech	kurz	k1gInPc6
jak	jak	k8xC,k8xS
obecně	obecně	k6eAd1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
odborně	odborně	k6eAd1
zaměřených	zaměřený	k2eAgNnPc6d1
<g/>
.	.	kIx.
</s>
<s>
ArtCamp	ArtCamp	k1gMnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
pořádá	pořádat	k5eAaImIp3nS
Fakulta	fakulta	k1gFnSc1
designu	design	k1gInSc2
a	a	k8xC
umění	umění	k1gNnSc2
Ladislava	Ladislav	k1gMnSc2
Sutnara	Sutnar	k1gMnSc2
mezinárodní	mezinárodní	k2eAgFnSc4d1
letní	letní	k2eAgFnSc4d1
školu	škola	k1gFnSc4
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
neboli	neboli	k8xC
ArtCamp	ArtCamp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koná	konat	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
třech	tři	k4xCgInPc6
červencových	červencový	k2eAgInPc6d1
týdnech	týden	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Klub	klub	k1gInSc1
absolventů	absolvent	k1gMnPc2
</s>
<s>
Klub	klub	k1gInSc1
absolventů	absolvent	k1gMnPc2
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
funguje	fungovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
chce	chtít	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc7
prostřednictvím	prostřednictví	k1gNnSc7
zůstat	zůstat	k5eAaPmF
v	v	k7c6
kontaktu	kontakt	k1gInSc6
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
bývalými	bývalý	k2eAgMnPc7d1
studenty	student	k1gMnPc7
a	a	k8xC
nadále	nadále	k6eAd1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
spolupracovat	spolupracovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
měl	mít	k5eAaImAgInS
roku	rok	k1gInSc2
2020	#num#	k4
přes	přes	k7c4
tři	tři	k4xCgInPc4
tisíce	tisíc	k4xCgInPc4
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
za	za	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
pořádá	pořádat	k5eAaImIp3nS
ZČU	ZČU	kA
pro	pro	k7c4
své	svůj	k3xOyFgMnPc4
absolventy	absolvent	k1gMnPc4
Festival	festival	k1gInSc1
absolventů	absolvent	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Popularizace	popularizace	k1gFnSc1
vědy	věda	k1gFnSc2
</s>
<s>
Bav	bavit	k5eAaImRp2nS
se	se	k3xPyFc4
vědou	věda	k1gFnSc7
</s>
<s>
ZČU	ZČU	kA
provozuje	provozovat	k5eAaImIp3nS
internetový	internetový	k2eAgInSc1d1
magazín	magazín	k1gInSc1
Bav	bavit	k5eAaImRp2nS
se	se	k3xPyFc4
vědou	věda	k1gFnSc7
o	o	k7c6
vědě	věda	k1gFnSc6
a	a	k8xC
technice	technika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dny	den	k1gInPc1
vědy	věda	k1gFnSc2
a	a	k8xC
techniky	technika	k1gFnSc2
</s>
<s>
Interaktivní	interaktivní	k2eAgFnSc1d1
výstava	výstava	k1gFnSc1
Dny	dna	k1gFnSc2
vědy	věda	k1gFnSc2
a	a	k8xC
techniky	technika	k1gFnSc2
proměňuje	proměňovat	k5eAaImIp3nS
každoročně	každoročně	k6eAd1
na	na	k7c4
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
plzeňské	plzeňský	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
ve	v	k7c4
vědeckou	vědecký	k2eAgFnSc4d1
laboratoř	laboratoř	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
třicet	třicet	k4xCc4
expozic	expozice	k1gFnPc2
představuje	představovat	k5eAaImIp3nS
vědecké	vědecký	k2eAgFnPc4d1
obory	obora	k1gFnPc4
z	z	k7c2
nevšedního	všední	k2eNgInSc2d1
úhlu	úhel	k1gInSc2
pohledu	pohled	k1gInSc2
díky	díky	k7c3
experimentům	experiment	k1gInPc3
vycházejícím	vycházející	k2eAgInPc3d1
jak	jak	k6eAd1
z	z	k7c2
běžného	běžný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
z	z	k7c2
netradičních	tradiční	k2eNgNnPc2d1
vědeckých	vědecký	k2eAgNnPc2d1
odvětví	odvětví	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Noc	noc	k1gFnSc1
vědců	vědec	k1gMnPc2
</s>
<s>
Noc	noc	k1gFnSc1
vědců	vědec	k1gMnPc2
je	být	k5eAaImIp3nS
celoevropská	celoevropský	k2eAgFnSc1d1
populárně	populárně	k6eAd1
naučná	naučný	k2eAgFnSc1d1
akce	akce	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
se	se	k3xPyFc4
každým	každý	k3xTgInSc7
rokem	rok	k1gInSc7
na	na	k7c4
jeden	jeden	k4xCgInSc4
den	den	k1gInSc4
otevírají	otevírat	k5eAaImIp3nP
brány	brána	k1gFnPc1
vědeckých	vědecký	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
univerzit	univerzita	k1gFnPc2
<g/>
,	,	kIx,
science	science	k1gFnPc1
center	centrum	k1gNnPc2
<g/>
,	,	kIx,
muzeí	muzeum	k1gNnPc2
či	či	k8xC
knihoven	knihovna	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	k9
zábavnou	zábavný	k2eAgFnSc7d1
formou	forma	k1gFnSc7
přestavily	přestavit	k5eAaPmAgInP
práci	práce	k1gFnSc4
vědců	vědec	k1gMnPc2
a	a	k8xC
její	její	k3xOp3gInSc4
význam	význam	k1gInSc4
pro	pro	k7c4
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
více	hodně	k6eAd2
než	než	k8xS
30	#num#	k4
zapojenými	zapojený	k2eAgNnPc7d1
městy	město	k1gNnPc7
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
i	i	k9
Plzeň	Plzeň	k1gFnSc1
a	a	k8xC
ZČU	ZČU	kA
se	se	k3xPyFc4
zde	zde	k6eAd1
pravidelně	pravidelně	k6eAd1
aktivně	aktivně	k6eAd1
podílí	podílet	k5eAaImIp3nS
na	na	k7c6
programu	program	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Areál	areál	k1gInSc1
Techmania	Techmanium	k1gNnSc2
Science	Scienec	k1gInSc2
Center	centrum	k1gNnPc2
</s>
<s>
Techmania	Techmanium	k1gNnPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zřizovatelů	zřizovatel	k1gMnPc2
science	science	k1gFnSc2
centra	centrum	k1gNnSc2
Techmania	Techmanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
podnítit	podnítit	k5eAaPmF
nebo	nebo	k8xC
posílit	posílit	k5eAaPmF
zájem	zájem	k1gInSc4
veřejnosti	veřejnost	k1gFnSc2
o	o	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
techniku	technika	k1gFnSc4
a	a	k8xC
inspirovat	inspirovat	k5eAaBmF
děti	dítě	k1gFnPc4
a	a	k8xC
mládež	mládež	k1gFnSc4
ke	k	k7c3
spojení	spojení	k1gNnSc3
jejich	jejich	k3xOp3gFnSc2
profesní	profesní	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
s	s	k7c7
výzkumem	výzkum	k1gInSc7
a	a	k8xC
technickými	technický	k2eAgInPc7d1
obory	obor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Akademici	akademik	k1gMnPc1
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Akademici	akademik	k1gMnPc1
Plzeň	Plzeň	k1gFnSc4
jsou	být	k5eAaImIp3nP
hokejovým	hokejový	k2eAgInSc7d1
týmem	tým	k1gInSc7
plzeňských	plzeňský	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
–	–	k?
ZČU	ZČU	kA
a	a	k8xC
Lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
UK	UK	kA
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
Univerzitní	univerzitní	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Tým	tým	k1gInSc1
pořádá	pořádat	k5eAaImIp3nS
každoroční	každoroční	k2eAgFnSc4d1
hokejovou	hokejový	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
o	o	k7c4
Plzeň	Plzeň	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
střetnou	střetnout	k5eAaPmIp3nP
hráči	hráč	k1gMnPc1
ZČU	ZČU	kA
s	s	k7c7
týmem	tým	k1gInSc7
lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
UK	UK	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
za	za	k7c4
rok	rok	k1gInSc4
2019	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
2020-06-22	2020-06-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
146	#num#	k4
<g/>
-	-	kIx~
<g/>
150	#num#	k4
<g/>
,	,	kIx,
171	#num#	k4
<g/>
-	-	kIx~
<g/>
175	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
10	#num#	k4
let	léto	k1gNnPc2
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
40	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7082	#num#	k4
<g/>
-	-	kIx~
<g/>
886	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
§	§	k?
1	#num#	k4
písm	písmo	k1gNnPc2
<g/>
.	.	kIx.
c	c	k0
<g/>
)	)	kIx)
a	a	k8xC
§	§	k?
4	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
314	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
zřízení	zřízení	k1gNnSc6
Slezské	slezský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
Jihočeské	jihočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
Univerzity	univerzita	k1gFnSc2
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc2
a	a	k8xC
Ostravské	ostravský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Profil	profil	k1gInSc1
fakulty	fakulta	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
filozofická	filozofický	k2eAgFnSc1d1
Západočeské	západočeský	k2eAgFnPc4d1
univerzity	univerzita	k1gFnPc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Historie	historie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MORAVEC	Moravec	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Průša	Průša	k1gMnSc1
<g/>
,	,	kIx,
rektor	rektor	k1gMnSc1
Západočeské	západočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rádio	rádio	k1gNnSc1
Impuls	impuls	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-11-03	2009-11-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
obdržela	obdržet	k5eAaPmAgFnS
institucionální	institucionální	k2eAgFnSc4d1
akreditaci	akreditace	k1gFnSc4
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Fakulta	fakulta	k1gFnSc1
filozofická	filozofický	k2eAgFnSc1d1
získala	získat	k5eAaPmAgFnS
HR	hr	k6eAd1
Award	Award	k1gInSc4
<g/>
.	.	kIx.
info	info	k6eAd1
<g/>
.	.	kIx.
<g/>
zcu	zcu	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HRS	HRS	kA
<g/>
4	#num#	k4
<g/>
R.	R.	kA
EURAXESS	EURAXESS	kA
Czech	Czecha	k1gFnPc2
Republic	Republice	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-07-17	2015-07-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
World	World	k1gMnSc1
University	universita	k1gFnSc2
Rankings	Rankings	k1gInSc1
2019	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
↑	↑	k?
Zahraniční	zahraniční	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
-	-	kIx~
<g/>
.	.	kIx.
international	internationat	k5eAaImAgInS,k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
zcu	zcu	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čeští	český	k2eAgMnPc1d1
studenti	student	k1gMnPc1
vytvořili	vytvořit	k5eAaPmAgMnP
výzkumné	výzkumný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Etiopii	Etiopie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-02-20	2014-02-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
OUDOVÁ	OUDOVÁ	kA
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
založila	založit	k5eAaPmAgFnS
v	v	k7c6
Etiopii	Etiopie	k1gFnSc6
Centrum	centrum	k1gNnSc1
afrických	africký	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-02-9	2014-02-9	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kulturka	Kulturka	k1gFnSc1
ZČU	ZČU	kA
<g/>
.	.	kIx.
www.facebook.com	www.facebook.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Družba	družba	k1gFnSc1
<g/>
.	.	kIx.
www.facebook.com	www.facebook.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Plzeň	Plzeň	k1gFnSc1
otevře	otevřít	k5eAaPmIp3nS
novou	nový	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
Ústavu	ústav	k1gInSc2
umění	umění	k1gNnSc2
a	a	k8xC
designu	design	k1gInSc2
–	–	k?
DesignMag	DesignMag	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.designmag.cz	www.designmag.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
má	mít	k5eAaImIp3nS
nový	nový	k2eAgInSc1d1
výzkumný	výzkumný	k2eAgInSc1d1
komplex	komplex	k1gInSc1
<g/>
,	,	kIx,
míří	mířit	k5eAaImIp3nS
na	na	k7c4
technologie	technologie	k1gFnPc4
|	|	kIx~
Věda	věda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-09-21	2014-09-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PLZEŇ	Plzeň	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Správa	správa	k1gFnSc1
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
města	město	k1gNnSc2
Plzně	Plzeň	k1gFnSc2
|	|	kIx~
Dominikánská	dominikánský	k2eAgFnSc1d1
4	#num#	k4
|	|	kIx~
306	#num#	k4
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
univerzitní	univerzitní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
byla	být	k5eAaImAgFnS
otevřena	otevřít	k5eAaPmNgFnS
v	v	k7c6
areálu	areál	k1gInSc6
na	na	k7c6
Zeleném	zelený	k2eAgInSc6d1
trojúhelníku	trojúhelník	k1gInSc6
<g/>
.	.	kIx.
www.plzen.eu	www.plzen.eu	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VIDEO	video	k1gNnSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
tramvajová	tramvajový	k2eAgFnSc1d1
trať	trať	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
vozí	vozit	k5eAaImIp3nS
cestující	cestující	k1gMnPc4
až	až	k9
k	k	k7c3
univerzitě	univerzita	k1gFnSc3
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-12-16	2019-12-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Na	na	k7c6
ZČU	ZČU	kA
ostartovaly	ostartovat	k5eAaPmAgFnP,k5eAaBmAgFnP,k5eAaImAgFnP
dvě	dva	k4xCgFnPc1
největší	veliký	k2eAgFnPc1d3
a	a	k8xC
nejstarší	starý	k2eAgFnPc1d3
letní	letní	k2eAgFnPc1d1
školy	škola	k1gFnPc1
-	-	kIx~
FOTKY	fotka	k1gFnPc1
<g/>
.	.	kIx.
www.qap.cz	www.qap.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TOPINKA	Topinka	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolventi	absolvent	k1gMnPc1
ZČU	ZČU	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bav	bavit	k5eAaImRp2nS
se	se	k3xPyFc4
vědou	věda	k1gFnSc7
-	-	kIx~
Věda	věda	k1gFnSc1
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bav	bavit	k5eAaImRp2nS
se	se	k3xPyFc4
vědou	věda	k1gFnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dny	dna	k1gFnSc2
vědy	věda	k1gFnSc2
a	a	k8xC
techniky	technika	k1gFnSc2
2020	#num#	k4
<g/>
.	.	kIx.
dnyvedy	dnyveda	k1gMnSc2
<g/>
.	.	kIx.
<g/>
zcu	zcu	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2019	#num#	k4
|	|	kIx~
Šetrně	šetrně	k6eAd1
k	k	k7c3
planetě	planeta	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noc	noc	k1gFnSc1
vědců	vědec	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Univerzitní	univerzitní	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzitní	univerzitní	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
REJTHAR	REJTHAR	kA
<g/>
,	,	kIx,
Dominik	Dominik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypukne	vypuknout	k5eAaPmIp3nS
hokejová	hokejový	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
o	o	k7c4
Plzeň	Plzeň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeňský	plzeňský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
na	na	k7c6
Facebooku	Facebook	k1gInSc6
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
na	na	k7c6
Instagramu	Instagram	k1gInSc6
</s>
<s>
ZČU	ZČU	kA
&	&	k?
architektura	architektura	k1gFnSc1
</s>
<s>
ZČU	ZČU	kA
&	&	k?
25	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
Fakulty	fakulta	k1gFnSc2
</s>
<s>
Fakulta	fakulta	k1gFnSc1
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
•	•	k?
Fakulta	fakulta	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
filozofická	filozofický	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
právnická	právnický	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
designu	design	k1gInSc2
a	a	k8xC
umění	umění	k1gNnSc2
Ladislava	Ladislava	k1gFnSc1
Sutnara	Sutnara	k1gFnSc1
•	•	k?
Fakulta	fakulta	k1gFnSc1
zdravotnických	zdravotnický	k2eAgNnPc2d1
studií	studio	k1gNnPc2
Ústavy	ústava	k1gFnSc2
</s>
<s>
Ústav	ústav	k1gInSc1
jazykové	jazykový	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
•	•	k?
Nové	Nové	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
–	–	k?
výzkumné	výzkumný	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Knihovny	knihovna	k1gFnSc2
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
Další	další	k2eAgFnSc1d1
pracoviště	pracoviště	k1gNnSc4
</s>
<s>
Centrum	centrum	k1gNnSc1
informatizace	informatizace	k1gFnSc2
a	a	k8xC
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
•	•	k?
Katedra	katedra	k1gFnSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
•	•	k?
Projektové	projektový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
•	•	k?
Ústav	ústava	k1gFnPc2
celoživotního	celoživotní	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
•	•	k?
Zahraniční	zahraniční	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
Účelová	účelový	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Provoz	provoz	k1gInSc1
a	a	k8xC
služby	služba	k1gFnPc1
•	•	k?
Správa	správa	k1gFnSc1
kolejí	kolej	k1gFnPc2
a	a	k8xC
menz	menza	k1gFnPc2
Ostatní	ostatní	k2eAgFnSc2d1
</s>
<s>
Akademici	akademik	k1gMnPc1
Plzeň	Plzeň	k1gFnSc1
•	•	k?
ArtCamp	ArtCamp	k1gInSc1
•	•	k?
IS	IS	kA
<g/>
/	/	kIx~
<g/>
STAG	STAG	kA
•	•	k?
Mezinárodní	mezinárodní	k2eAgFnSc1d1
letní	letní	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Nečtiny	Nečtina	k1gFnSc2
•	•	k?
Techmania	Techmanium	k1gNnSc2
</s>
<s>
Veřejné	veřejný	k2eAgFnPc1d1
vysoké	vysoký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
Praha	Praha	k1gFnSc1
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Akademie	akademie	k1gFnSc2
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Česká	český	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
České	český	k2eAgFnSc6d1
vysoké	vysoká	k1gFnSc6
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
chemicko-technologická	chemicko-technologický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Jihočeská	jihočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
technická	technický	k2eAgFnSc1d1
a	a	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Liberec	Liberec	k1gInSc1
</s>
<s>
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Pardubice	Pardubice	k1gInPc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
polytechnická	polytechnický	k2eAgFnSc1d1
Jihlava	Jihlava	k1gFnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
•	•	k?
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Mendelova	Mendelův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
•	•	k?
Veterinární	veterinární	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Brno	Brno	k1gNnSc1
•	•	k?
Vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Brně	Brno	k1gNnSc6
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Ostravská	ostravský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
báňská	báňský	k2eAgFnSc1d1
–	–	k?
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
Opava	Opava	k1gFnSc1
</s>
<s>
Slezská	slezský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Opavě	Opava	k1gFnSc6
Zlín	Zlín	k1gInSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010709221	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0176	#num#	k4
7631	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2002023224	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
156258303	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2002023224	#num#	k4
</s>
