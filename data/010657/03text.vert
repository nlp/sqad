<p>
<s>
Tořič	tořič	k1gInSc1	tořič
včelonosný	včelonosný	k2eAgInSc1d1	včelonosný
(	(	kIx(	(
<g/>
Ophrys	Ophrys	k1gInSc1	Ophrys
apifera	apifero	k1gNnSc2	apifero
<g/>
;	;	kIx,	;
synonyma	synonymum	k1gNnSc2	synonymum
<g/>
:	:	kIx,	:
Ophrys	Ophrysa	k1gFnPc2	Ophrysa
insectifera	insectifero	k1gNnSc2	insectifero
var.	var.	k?	var.
andrachnites	andrachnitesa	k1gFnPc2	andrachnitesa
L.	L.	kA	L.
<g/>
,	,	kIx,	,
Ophrys	Ophrys	k1gInSc1	Ophrys
chlorantha	chlorantha	k1gMnSc1	chlorantha
Hegetschw	Hegetschw	k1gMnSc1	Hegetschw
<g/>
.	.	kIx.	.
et	et	k?	et
Heer	Heer	k1gMnSc1	Heer
<g/>
,	,	kIx,	,
Arachnites	Arachnites	k1gMnSc1	Arachnites
apifera	apifer	k1gMnSc2	apifer
Tod	Tod	k1gMnSc1	Tod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
bylina	bylina	k1gFnSc1	bylina
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
tořič	tořič	k1gInSc1	tořič
(	(	kIx(	(
<g/>
Ophrys	Ophrys	k1gInSc1	Ophrys
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
chráněným	chráněný	k2eAgInPc3d1	chráněný
druhům	druh	k1gInPc3	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgNnSc7d1	jiné
českým	český	k2eAgNnSc7d1	české
pojmenováním	pojmenování	k1gNnSc7	pojmenování
je	být	k5eAaImIp3nS	být
bezostrožka	bezostrožka	k1gFnSc1	bezostrožka
včelí	včelí	k2eAgFnSc1d1	včelí
(	(	kIx(	(
<g/>
Sloboda	sloboda	k1gFnSc1	sloboda
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tořič	tořič	k1gInSc1	tořič
včelonosný	včelonosný	k2eAgInSc1d1	včelonosný
je	být	k5eAaImIp3nS	být
vzpřímená	vzpřímený	k2eAgFnSc1d1	vzpřímená
rostlina	rostlina	k1gFnSc1	rostlina
dosahující	dosahující	k2eAgFnSc1d1	dosahující
20-45	[number]	k4	20-45
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hlízy	hlíza	k1gFnPc1	hlíza
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
mají	mít	k5eAaImIp3nP	mít
kopinatý	kopinatý	k2eAgInSc4d1	kopinatý
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
nahoru	nahoru	k6eAd1	nahoru
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zmenšují	zmenšovat	k5eAaImIp3nP	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Maximálně	maximálně	k6eAd1	maximálně
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnSc2	délka
6-13	[number]	k4	6-13
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Květenství	květenství	k1gNnSc1	květenství
je	být	k5eAaImIp3nS	být
vzpřímené	vzpřímený	k2eAgNnSc1d1	vzpřímené
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
jen	jen	k9	jen
3-8	[number]	k4	3-8
květů	květ	k1gInPc2	květ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
nápadné	nápadný	k2eAgInPc4d1	nápadný
svým	svůj	k3xOyFgNnPc3	svůj
zbarvením	zbarvení	k1gNnPc3	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInPc1d1	vnější
okvětní	okvětní	k2eAgInPc1d1	okvětní
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
podlouhlé	podlouhlý	k2eAgInPc1d1	podlouhlý
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
růžovou	růžový	k2eAgFnSc4d1	růžová
až	až	k9	až
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
okvětní	okvětní	k2eAgInPc1d1	okvětní
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgInPc1d2	kratší
<g/>
,	,	kIx,	,
pysk	pysk	k1gInSc1	pysk
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1,2	[number]	k4	1,2
cm	cm	kA	cm
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
okrouhle	okrouhle	k6eAd1	okrouhle
vejčitý	vejčitý	k2eAgInSc4d1	vejčitý
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tmavohnědou	tmavohnědý	k2eAgFnSc4d1	tmavohnědá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
jsou	být	k5eAaImIp3nP	být
válcovitě	válcovitě	k6eAd1	válcovitě
zelené	zelený	k2eAgFnPc1d1	zelená
tobolky	tobolka	k1gFnPc1	tobolka
<g/>
.	.	kIx.	.
</s>
<s>
Tořič	tořič	k1gInSc1	tořič
včelonosný	včelonosný	k2eAgInSc1d1	včelonosný
kvete	kvést	k5eAaImIp3nS	kvést
v	v	k7c4	v
období	období	k1gNnSc4	období
mezi	mezi	k7c7	mezi
květnem	květno	k1gNnSc7	květno
a	a	k8xC	a
červencem	červenec	k1gInSc7	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
rostlina	rostlina	k1gFnSc1	rostlina
samosprašná	samosprašný	k2eAgFnSc1d1	samosprašná
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
květy	květ	k1gInPc4	květ
uzpůsobené	uzpůsobený	k2eAgInPc4d1	uzpůsobený
k	k	k7c3	k
opylování	opylování	k1gNnSc4	opylování
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
samoopylení	samoopylení	k1gNnSc1	samoopylení
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
jako	jako	k9	jako
sekundární	sekundární	k2eAgInSc4d1	sekundární
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stanoviště	stanoviště	k1gNnSc1	stanoviště
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Tořič	tořič	k1gInSc1	tořič
včelonosný	včelonosný	k2eAgMnSc1d1	včelonosný
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
stanovištích	stanoviště	k1gNnPc6	stanoviště
od	od	k7c2	od
pahorkatin	pahorkatina	k1gFnPc2	pahorkatina
do	do	k7c2	do
podhůří	podhůří	k1gNnSc2	podhůří
a	a	k8xC	a
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
slunné	slunný	k2eAgFnPc4d1	slunná
travnaté	travnatý	k2eAgFnPc4d1	travnatá
nebo	nebo	k8xC	nebo
křovinaté	křovinatý	k2eAgFnPc4d1	křovinatá
louky	louka	k1gFnPc4	louka
nebo	nebo	k8xC	nebo
světlé	světlý	k2eAgInPc4d1	světlý
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
bazických	bazický	k2eAgFnPc6d1	bazická
a	a	k8xC	a
na	na	k7c4	na
živiny	živina	k1gFnPc4	živina
bohatých	bohatý	k2eAgMnPc2d1	bohatý
půdách	půda	k1gFnPc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
tořiče	tořič	k1gInSc2	tořič
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
nejseverněji	severně	k6eAd3	severně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Beneluxu	Benelux	k1gInSc2	Benelux
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
hranice	hranice	k1gFnSc1	hranice
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
Kavkazem	Kavkaz	k1gInSc7	Kavkaz
a	a	k8xC	a
Krymem	Krym	k1gInSc7	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
uváděn	uvádět	k5eAaImNgInS	uvádět
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
byl	být	k5eAaImAgInS	být
doložen	doložit	k5eAaPmNgInS	doložit
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
roste	růst	k5eAaImIp3nS	růst
jen	jen	k9	jen
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
či	či	k8xC	či
střední	střední	k2eAgFnSc6d1	střední
Moravě	Morava	k1gFnSc6	Morava
-	-	kIx~	-
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Štramberka	Štramberk	k1gInSc2	Štramberk
a	a	k8xC	a
Kurdějova	Kurdějův	k2eAgFnSc1d1	Kurdějova
a	a	k8xC	a
v	v	k7c6	v
Bílých	bílý	k2eAgInPc6d1	bílý
Karpatech	Karpaty	k1gInPc6	Karpaty
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
a	a	k8xC	a
nejnověji	nově	k6eAd3	nově
i	i	k9	i
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
v	v	k7c6	v
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tořič	tořič	k1gInSc1	tořič
včelonosný	včelonosný	k2eAgMnSc1d1	včelonosný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Ophrys	Ophrysa	k1gFnPc2	Ophrysa
apifera	apifero	k1gNnSc2	apifero
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Tořič	tořič	k1gInSc1	tořič
včelonosný	včelonosný	k2eAgInSc1d1	včelonosný
(	(	kIx(	(
<g/>
Ophrys	Ophrys	k1gInSc1	Ophrys
apifera	apifero	k1gNnSc2	apifero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samoopylovač	samoopylovač	k1gMnSc1	samoopylovač
</s>
</p>
