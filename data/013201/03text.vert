<p>
<s>
Kormorán	kormorán	k1gMnSc1	kormorán
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Phalacrocorax	Phalacrocorax	k1gInSc1	Phalacrocorax
carbo	carba	k1gMnSc5	carba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
kormoránovitých	kormoránovitý	k2eAgMnPc2d1	kormoránovitý
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnSc2	délka
90	[number]	k4	90
<g/>
cm	cm	kA	cm
s	s	k7c7	s
rozpětím	rozpětí	k1gNnSc7	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
140	[number]	k4	140
<g/>
cm	cm	kA	cm
-	-	kIx~	-
150	[number]	k4	150
<g/>
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
1500	[number]	k4	1500
<g/>
g	g	kA	g
-	-	kIx~	-
3100	[number]	k4	3100
<g/>
g.	g.	k?	g.
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
snůšce	snůška	k1gFnSc6	snůška
mívá	mívat	k5eAaImIp3nS	mívat
4	[number]	k4	4
-	-	kIx~	-
5	[number]	k4	5
kusů	kus	k1gInPc2	kus
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Plave	plavat	k5eAaImIp3nS	plavat
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
hákovitý	hákovitý	k2eAgInSc1d1	hákovitý
zobák	zobák	k1gInSc1	zobák
směřuje	směřovat	k5eAaImIp3nS	směřovat
šikmo	šikmo	k6eAd1	šikmo
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Taky	taky	k6eAd1	taky
se	se	k3xPyFc4	se
potápí	potápět	k5eAaImIp3nP	potápět
a	a	k8xC	a
loví	lovit	k5eAaImIp3nP	lovit
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Plovací	plovací	k2eAgFnSc1d1	plovací
blána	blána	k1gFnSc1	blána
spojuje	spojovat	k5eAaImIp3nS	spojovat
všechny	všechen	k3xTgInPc4	všechen
čtyři	čtyři	k4xCgInPc4	čtyři
prsty	prst	k1gInPc4	prst
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
si	se	k3xPyFc3	se
staví	stavit	k5eAaPmIp3nP	stavit
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
krmí	krmit	k5eAaImIp3nP	krmit
mláďata	mládě	k1gNnPc4	mládě
menšími	malý	k2eAgFnPc7d2	menší
rybami	ryba	k1gFnPc7	ryba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
kormorán	kormorán	k1gMnSc1	kormorán
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
zvláště	zvláště	k6eAd1	zvláště
chráněným	chráněný	k2eAgMnSc7d1	chráněný
druhem	druh	k1gMnSc7	druh
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
obecné	obecný	k2eAgFnSc2d1	obecná
ochrany	ochrana	k1gFnSc2	ochrana
vztahující	vztahující	k2eAgFnSc1d1	vztahující
se	se	k3xPyFc4	se
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
kormorána	kormorán	k1gMnSc2	kormorán
velkého	velký	k2eAgInSc2d1	velký
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
všechny	všechen	k3xTgInPc4	všechen
kontinenty	kontinent	k1gInPc4	kontinent
mimo	mimo	k7c4	mimo
Jižní	jižní	k2eAgFnSc4d1	jižní
Ameriku	Amerika	k1gFnSc4	Amerika
a	a	k8xC	a
Antarktidu	Antarktida	k1gFnSc4	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
však	však	k9	však
zimují	zimovat	k5eAaImIp3nP	zimovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
až	až	k9	až
po	po	k7c4	po
jižní	jižní	k2eAgFnSc4d1	jižní
Floridu	Florida	k1gFnSc4	Florida
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
také	také	k9	také
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
obývá	obývat	k5eAaImIp3nS	obývat
většinu	většina	k1gFnSc4	většina
atlantského	atlantský	k2eAgNnSc2d1	Atlantské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
Středomoří	středomoří	k1gNnSc2	středomoří
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
oblastí	oblast	k1gFnPc2	oblast
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
zimuje	zimovat	k5eAaImIp3nS	zimovat
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
podél	podél	k7c2	podél
Nilu	Nil	k1gInSc2	Nil
<g/>
,	,	kIx,	,
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
a	a	k8xC	a
celoročně	celoročně	k6eAd1	celoročně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
od	od	k7c2	od
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
na	na	k7c4	na
východ	východ	k1gInSc4	východ
po	po	k7c4	po
východní	východní	k2eAgFnSc4d1	východní
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
zimující	zimující	k2eAgMnPc1d1	zimující
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
pozorováni	pozorovat	k5eAaImNgMnP	pozorovat
i	i	k9	i
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
také	také	k9	také
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Austrálie	Austrálie	k1gFnSc2	Austrálie
mimo	mimo	k7c4	mimo
centrální	centrální	k2eAgFnPc4d1	centrální
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
také	také	k9	také
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
kormoráni	kormorán	k1gMnPc1	kormorán
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
neúspěšně	úspěšně	k6eNd1	úspěšně
zahnízdit	zahnízdit	k5eAaPmF	zahnízdit
již	již	k9	již
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
stálá	stálý	k2eAgFnSc1d1	stálá
kolonie	kolonie	k1gFnSc1	kolonie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
počty	počet	k1gInPc4	počet
hnízdících	hnízdící	k2eAgMnPc2d1	hnízdící
ptáků	pták	k1gMnPc2	pták
nejdříve	dříve	k6eAd3	dříve
prudce	prudko	k6eAd1	prudko
narůstaly	narůstat	k5eAaImAgFnP	narůstat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
zpět	zpět	k6eAd1	zpět
prudce	prudko	k6eAd1	prudko
poklesly	poklesnout	k5eAaPmAgFnP	poklesnout
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Kormoráni	kormorán	k1gMnPc1	kormorán
osídlili	osídlit	k5eAaPmAgMnP	osídlit
také	také	k9	také
další	další	k2eAgFnPc4d1	další
oblasti	oblast	k1gFnPc4	oblast
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnPc4d1	jižní
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnPc4d1	severozápadní
Čechy	Čechy	k1gFnPc4	Čechy
<g/>
,	,	kIx,	,
Poodří	Poodří	k1gNnSc1	Poodří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
hnízdilo	hnízdit	k5eAaImAgNnS	hnízdit
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
232	[number]	k4	232
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
výrazné	výrazný	k2eAgNnSc4d1	výrazné
snížení	snížení	k1gNnSc4	snížení
proti	proti	k7c3	proti
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
660	[number]	k4	660
párům	pár	k1gInPc3	pár
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
;	;	kIx,	;
pokles	pokles	k1gInSc1	pokles
byl	být	k5eAaImAgInS	být
jistě	jistě	k6eAd1	jistě
způsoben	způsobit	k5eAaPmNgInS	způsobit
regulací	regulace	k1gFnSc7	regulace
početnosti	početnost	k1gFnPc1	početnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
u	u	k7c2	u
nás	my	k3xPp1nPc4	my
v	v	k7c6	v
šesti	šest	k4xCc6	šest
koloniích	kolonie	k1gFnPc6	kolonie
hnízdilo	hnízdit	k5eAaImAgNnS	hnízdit
přibližně	přibližně	k6eAd1	přibližně
350	[number]	k4	350
párů	pár	k1gInPc2	pár
kormoránů	kormorán	k1gMnPc2	kormorán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimního	zimní	k2eAgNnSc2d1	zimní
období	období	k1gNnSc2	období
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
pak	pak	k6eAd1	pak
zavítalo	zavítat	k5eAaPmAgNnS	zavítat
10000	[number]	k4	10000
<g/>
–	–	k?	–
<g/>
15000	[number]	k4	15000
ptáků	pták	k1gMnPc2	pták
ze	z	k7c2	z
severských	severský	k2eAgFnPc2d1	severská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
mimo	mimo	k7c4	mimo
produkční	produkční	k2eAgInPc4d1	produkční
rybníky	rybník	k1gInPc4	rybník
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
na	na	k7c6	na
volných	volný	k2eAgFnPc6d1	volná
vodách	voda	k1gFnPc6	voda
<g/>
)	)	kIx)	)
loví	lovit	k5eAaImIp3nP	lovit
kormoráni	kormorán	k1gMnPc1	kormorán
především	především	k6eAd1	především
plotice	plotice	k1gFnPc4	plotice
<g/>
,	,	kIx,	,
okouny	okoun	k1gMnPc4	okoun
<g/>
,	,	kIx,	,
oukleje	ouklej	k1gFnPc4	ouklej
a	a	k8xC	a
jelce	jelec	k1gMnPc4	jelec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
přes	přes	k7c4	přes
75	[number]	k4	75
<g/>
%	%	kIx~	%
jejich	jejich	k3xOp3gFnSc2	jejich
kořisti	kořist	k1gFnSc2	kořist
(	(	kIx(	(
<g/>
početně	početně	k6eAd1	početně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rybářských	rybářský	k2eAgInPc6d1	rybářský
revírech	revír	k1gInPc6	revír
jen	jen	k9	jen
za	za	k7c4	za
zimní	zimní	k2eAgNnSc4d1	zimní
období	období	k1gNnSc4	období
odloví	odlovit	k5eAaPmIp3nS	odlovit
až	až	k9	až
80	[number]	k4	80
kg	kg	kA	kg
ryb	ryba	k1gFnPc2	ryba
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
hektaru	hektar	k1gInSc2	hektar
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
loví	lovit	k5eAaImIp3nP	lovit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
až	až	k9	až
5	[number]	k4	5
<g/>
krát	krát	k6eAd1	krát
těžší	těžký	k2eAgFnSc2d2	těžší
ryby	ryba	k1gFnSc2	ryba
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
157	[number]	k4	157
gramů	gram	k1gInPc2	gram
<g/>
)	)	kIx)	)
než	než	k8xS	než
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
30	[number]	k4	30
gramů	gram	k1gInPc2	gram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
kořist	kořist	k1gFnSc4	kořist
představují	představovat	k5eAaImIp3nP	představovat
ryby	ryba	k1gFnPc1	ryba
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
18,5	[number]	k4	18,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
95	[number]	k4	95
%	%	kIx~	%
ulovených	ulovený	k2eAgFnPc2d1	ulovená
ryb	ryba	k1gFnPc2	ryba
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
uloví	ulovit	k5eAaPmIp3nS	ulovit
i	i	k9	i
rybu	ryba	k1gFnSc4	ryba
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
50	[number]	k4	50
cm	cm	kA	cm
(	(	kIx(	(
<g/>
štika	štika	k1gFnSc1	štika
<g/>
,	,	kIx,	,
parma	parma	k1gFnSc1	parma
<g/>
,	,	kIx,	,
úhoř	úhoř	k1gMnSc1	úhoř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgFnSc1d1	denní
spotřeba	spotřeba	k1gFnSc1	spotřeba
potravy	potrava	k1gFnSc2	potrava
průměrného	průměrný	k2eAgMnSc4d1	průměrný
ptáka	pták	k1gMnSc4	pták
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
400	[number]	k4	400
gramů	gram	k1gInPc2	gram
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgInPc1d3	nejnovější
výzkumy	výzkum	k1gInPc1	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolonie	kolonie	k1gFnSc1	kolonie
zimujících	zimující	k2eAgMnPc2d1	zimující
kormoránů	kormorán	k1gMnPc2	kormorán
nemají	mít	k5eNaImIp3nP	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
výsledky	výsledek	k1gInPc4	výsledek
a	a	k8xC	a
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
reintrodukčního	reintrodukční	k2eAgInSc2d1	reintrodukční
programu	program	k1gInSc2	program
Losos	losos	k1gMnSc1	losos
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řek	řeka	k1gFnPc2	řeka
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
potrava	potrava	k1gFnSc1	potrava
kormoránů	kormorán	k1gMnPc2	kormorán
sledována	sledován	k2eAgFnSc1d1	sledována
na	na	k7c6	na
nádržích	nádrž	k1gFnPc6	nádrž
Slapy	slap	k1gInPc4	slap
a	a	k8xC	a
Želivka	Želivka	k1gFnSc1	Želivka
<g/>
,	,	kIx,	,
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
,	,	kIx,	,
Berounce	Berounka	k1gFnSc6	Berounka
a	a	k8xC	a
na	na	k7c6	na
Labi	Labe	k1gNnSc6	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Expanze	expanze	k1gFnSc1	expanze
kormorána	kormorán	k1gMnSc2	kormorán
velkého	velký	k2eAgMnSc2d1	velký
===	===	k?	===
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
poddruhu	poddruh	k1gInSc2	poddruh
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
c.	c.	k?	c.
sinensis	sinensis	k1gFnSc2	sinensis
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
téměř	téměř	k6eAd1	téměř
vyhynula	vyhynout	k5eAaPmAgNnP	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
ochranářských	ochranářský	k2eAgNnPc2d1	ochranářské
opatření	opatření	k1gNnPc2	opatření
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1965	[number]	k4	1965
až	až	k9	až
1981	[number]	k4	1981
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
expanzívnímu	expanzívní	k2eAgInSc3d1	expanzívní
růstu	růst	k1gInSc3	růst
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
až	až	k9	až
o	o	k7c4	o
18	[number]	k4	18
<g/>
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdní	hnízdní	k2eAgFnPc1d1	hnízdní
populace	populace	k1gFnPc1	populace
ve	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
a	a	k8xC	a
Švédsku	Švédsko	k1gNnSc6	Švédsko
tak	tak	k9	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
62	[number]	k4	62
250	[number]	k4	250
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
čítá	čítat	k5eAaImIp3nS	čítat
evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
kormorána	kormorán	k1gMnSc2	kormorán
velkého	velký	k2eAgInSc2d1	velký
více	hodně	k6eAd2	hodně
než	než	k8xS	než
310	[number]	k4	310
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
jich	on	k3xPp3gMnPc2	on
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
(	(	kIx(	(
<g/>
36	[number]	k4	36
až	až	k9	až
41	[number]	k4	41
tisíc	tisíc	k4xCgInPc2	tisíc
párů	pár	k1gInPc2	pár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
kormorána	kormorán	k1gMnSc2	kormorán
velkého	velký	k2eAgInSc2d1	velký
k	k	k7c3	k
severu	sever	k1gInSc3	sever
přispělo	přispět	k5eAaPmAgNnS	přispět
jistě	jistě	k6eAd1	jistě
i	i	k8xC	i
oteplování	oteplování	k1gNnSc4	oteplování
moří	moře	k1gNnPc2	moře
<g/>
;	;	kIx,	;
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
nyní	nyní	k6eAd1	nyní
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
i	i	k9	i
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
místní	místní	k2eAgFnSc2d1	místní
populace	populace	k1gFnSc2	populace
přímo	přímo	k6eAd1	přímo
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
kolísáním	kolísání	k1gNnSc7	kolísání
povrchové	povrchový	k2eAgFnSc2d1	povrchová
teploty	teplota	k1gFnSc2	teplota
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konflikt	konflikt	k1gInSc1	konflikt
ochranářů	ochranář	k1gMnPc2	ochranář
s	s	k7c7	s
rybáři	rybář	k1gMnPc7	rybář
==	==	k?	==
</s>
</p>
<p>
<s>
Škody	škoda	k1gFnPc1	škoda
způsobené	způsobený	k2eAgFnPc1d1	způsobená
predačním	predační	k2eAgInSc7d1	predační
tlakem	tlak	k1gInSc7	tlak
kormorána	kormorán	k1gMnSc2	kormorán
velkého	velký	k2eAgMnSc2d1	velký
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
-	-	kIx~	-
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
produkčním	produkční	k2eAgMnPc3d1	produkční
rybářům	rybář	k1gMnPc3	rybář
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
115	[number]	k4	115
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c4	o
poskytování	poskytování	k1gNnSc4	poskytování
náhrad	náhrada	k1gFnPc2	náhrada
škod	škoda	k1gFnPc2	škoda
způsobených	způsobený	k2eAgFnPc2d1	způsobená
vybranými	vybraná	k1gFnPc7	vybraná
zvláště	zvláště	k6eAd1	zvláště
chráněnými	chráněný	k2eAgMnPc7d1	chráněný
živočichy	živočich	k1gMnPc7	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
dostali	dostat	k5eAaPmAgMnP	dostat
produkční	produkční	k2eAgMnPc1d1	produkční
rybáři	rybář	k1gMnPc1	rybář
od	od	k7c2	od
státu	stát	k1gInSc2	stát
náhrady	náhrada	k1gFnSc2	náhrada
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
téměř	téměř	k6eAd1	téměř
350	[number]	k4	350
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
přes	přes	k7c4	přes
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
data	datum	k1gNnSc2	datum
MŽP	MŽP	kA	MŽP
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgMnPc1d1	sportovní
rybáři	rybář	k1gMnPc1	rybář
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
mnohé	mnohý	k2eAgInPc1d1	mnohý
revíry	revír	k1gInPc1	revír
byly	být	k5eAaImAgInP	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
predačním	predační	k2eAgInSc7d1	predační
tlakem	tlak	k1gInSc7	tlak
kormorána	kormorán	k1gMnSc2	kormorán
rovněž	rovněž	k9	rovněž
postiženy	postižen	k2eAgInPc1d1	postižen
<g/>
,	,	kIx,	,
nedostali	dostat	k5eNaPmAgMnP	dostat
od	od	k7c2	od
státu	stát	k1gInSc2	stát
nikdy	nikdy	k6eAd1	nikdy
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
datu	datum	k1gNnSc3	datum
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
kormorán	kormorán	k1gMnSc1	kormorán
velký	velký	k2eAgMnSc1d1	velký
z	z	k7c2	z
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
upravuje	upravovat	k5eAaImIp3nS	upravovat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
vyňat	vynít	k5eAaPmNgInS	vynít
z	z	k7c2	z
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kormorána	kormorán	k1gMnSc4	kormorán
velkého	velký	k2eAgNnSc2d1	velké
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
přestal	přestat	k5eAaPmAgInS	přestat
vztahovat	vztahovat	k5eAaImF	vztahovat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
115	[number]	k4	115
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
a	a	k8xC	a
škody	škoda	k1gFnPc1	škoda
způsobené	způsobený	k2eAgFnPc1d1	způsobená
rybářům	rybář	k1gMnPc3	rybář
tímto	tento	k3xDgInSc7	tento
rybožravcem	rybožravec	k1gMnSc7	rybožravec
přestaly	přestat	k5eAaPmAgInP	přestat
být	být	k5eAaImF	být
státem	stát	k1gInSc7	stát
vypláceny	vyplácet	k5eAaImNgFnP	vyplácet
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zcela	zcela	k6eAd1	zcela
neudržitelný	udržitelný	k2eNgInSc1d1	neudržitelný
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
opět	opět	k6eAd1	opět
pro	pro	k7c4	pro
produkční	produkční	k2eAgMnPc4d1	produkční
rybáře	rybář	k1gMnPc4	rybář
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
úpravou	úprava	k1gFnSc7	úprava
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
škody	škoda	k1gFnPc1	škoda
na	na	k7c6	na
rybách	ryba	k1gFnPc6	ryba
rozhodně	rozhodně	k6eAd1	rozhodně
nepřestaly	přestat	k5eNaPmAgInP	přestat
vznikat	vznikat	k5eAaImF	vznikat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
přijat	přijat	k2eAgInSc1d1	přijat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
197	[number]	k4	197
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2017	[number]	k4	2017
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
115	[number]	k4	115
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
škody	škoda	k1gFnPc1	škoda
způsobené	způsobený	k2eAgFnSc2d1	způsobená
kormoránem	kormorán	k1gMnSc7	kormorán
velkým	velký	k2eAgNnPc3d1	velké
budou	být	k5eAaImBp3nP	být
produkčním	produkční	k2eAgMnPc3d1	produkční
rybářům	rybář	k1gMnPc3	rybář
opět	opět	k6eAd1	opět
vyplácet	vyplácet	k5eAaImF	vyplácet
počínaje	počínaje	k7c7	počínaje
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
2018	[number]	k4	2018
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgInPc2	tři
let	léto	k1gNnPc2	léto
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
a	a	k8xC	a
2019	[number]	k4	2019
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
výši	výše	k1gFnSc6	výše
uznaných	uznaný	k2eAgFnPc2d1	uznaná
škod	škoda	k1gFnPc2	škoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
jen	jen	k9	jen
80	[number]	k4	80
<g/>
%	%	kIx~	%
výše	výše	k1gFnSc1	výše
uznaných	uznaný	k2eAgFnPc2d1	uznaná
škod	škoda	k1gFnPc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
jmenovitě	jmenovitě	k6eAd1	jmenovitě
zmíněn	zmínit	k5eAaPmNgInS	zmínit
ve	v	k7c6	v
vyhlášce	vyhláška	k1gFnSc6	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kormorán	kormorán	k1gMnSc1	kormorán
velký	velký	k2eAgInSc4d1	velký
v	v	k7c4	v
ČR	ČR	kA	ČR
stále	stále	k6eAd1	stále
pod	pod	k7c7	pod
obecnou	obecný	k2eAgFnSc7d1	obecná
ochranou	ochrana	k1gFnSc7	ochrana
podle	podle	k7c2	podle
§	§	k?	§
5	[number]	k4	5
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
evropské	evropský	k2eAgFnSc2d1	Evropská
legislativy	legislativa	k1gFnSc2	legislativa
–	–	k?	–
směrnice	směrnice	k1gFnSc2	směrnice
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
147	[number]	k4	147
<g/>
/	/	kIx~	/
<g/>
ES	ES	kA	ES
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
stále	stále	k6eAd1	stále
není	být	k5eNaImIp3nS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
mezi	mezi	k7c4	mezi
zvěř	zvěř	k1gFnSc4	zvěř
obhospodařovanou	obhospodařovaný	k2eAgFnSc4d1	obhospodařovaná
lovem	lov	k1gInSc7	lov
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
č.	č.	k?	č.
449	[number]	k4	449
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
myslivosti	myslivost	k1gFnSc6	myslivost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kormorán	kormorán	k1gMnSc1	kormorán
velký	velký	k2eAgMnSc1d1	velký
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
výrazný	výrazný	k2eAgInSc4d1	výrazný
růst	růst	k1gInSc4	růst
počtu	počet	k1gInSc2	počet
jak	jak	k8xS	jak
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zimujících	zimující	k2eAgMnPc2d1	zimující
ptáků	pták	k1gMnPc2	pták
stále	stále	k6eAd1	stále
zvláště	zvláště	k6eAd1	zvláště
chráněným	chráněný	k2eAgInSc7d1	chráněný
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
jeho	jeho	k3xOp3gFnSc4	jeho
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
a	a	k8xC	a
plošnou	plošný	k2eAgFnSc4d1	plošná
regulaci	regulace	k1gFnSc4	regulace
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rybářům	rybář	k1gMnPc3	rybář
čerpat	čerpat	k5eAaImF	čerpat
od	od	k7c2	od
státu	stát	k1gInSc2	stát
finanční	finanční	k2eAgFnSc4d1	finanční
náhradu	náhrada	k1gFnSc4	náhrada
prokázaných	prokázaný	k2eAgFnPc2d1	prokázaná
škod	škoda	k1gFnPc2	škoda
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
chovných	chovný	k2eAgInPc6d1	chovný
rybnících	rybník	k1gInPc6	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
škody	škoda	k1gFnPc1	škoda
způsobované	způsobovaný	k2eAgFnPc1d1	způsobovaná
na	na	k7c6	na
populaci	populace	k1gFnSc6	populace
ryb	ryba	k1gFnPc2	ryba
v	v	k7c6	v
řekách	řeka	k1gFnPc6	řeka
<g/>
,	,	kIx,	,
jezerech	jezero	k1gNnPc6	jezero
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc6d1	ostatní
nádržích	nádrž	k1gFnPc6	nádrž
nejsou	být	k5eNaImIp3nP	být
propláceny	proplácet	k5eAaImNgInP	proplácet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
kritiky	kritika	k1gFnSc2	kritika
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Českého	český	k2eAgInSc2d1	český
rybářského	rybářský	k2eAgInSc2d1	rybářský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Napjatá	napjatý	k2eAgFnSc1d1	napjatá
situace	situace	k1gFnSc1	situace
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
měřítku	měřítko	k1gNnSc6	měřítko
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
Usnesení	usnesení	k1gNnSc2	usnesení
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
ze	z	k7c2	z
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
o	o	k7c6	o
vypracování	vypracování	k1gNnSc6	vypracování
evropského	evropský	k2eAgInSc2d1	evropský
plánu	plán	k1gInSc2	plán
na	na	k7c4	na
regulaci	regulace	k1gFnSc4	regulace
populace	populace	k1gFnSc2	populace
kormoránů	kormorán	k1gMnPc2	kormorán
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
snížit	snížit	k5eAaPmF	snížit
rostoucí	rostoucí	k2eAgFnPc4d1	rostoucí
škody	škoda	k1gFnPc4	škoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
kormoráni	kormorán	k1gMnPc1	kormorán
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
rybím	rybí	k2eAgFnPc3d1	rybí
populacím	populace	k1gFnPc3	populace
<g/>
,	,	kIx,	,
rybolovu	rybolov	k1gInSc6	rybolov
a	a	k8xC	a
akvakultuře	akvakultura	k1gFnSc6	akvakultura
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2177	[number]	k4	2177
<g/>
(	(	kIx(	(
<g/>
INI	INI	kA	INI
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
usnesení	usnesení	k1gNnSc1	usnesení
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
Evropskou	evropský	k2eAgFnSc4d1	Evropská
komisi	komise	k1gFnSc4	komise
a	a	k8xC	a
členské	členský	k2eAgInPc4d1	členský
státy	stát	k1gInPc4	stát
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
populace	populace	k1gFnSc2	populace
kormorána	kormorán	k1gMnSc2	kormorán
velkého	velký	k2eAgMnSc2d1	velký
(	(	kIx(	(
<g/>
které	který	k3yRgFnPc4	který
bude	být	k5eAaImBp3nS	být
podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
každoroční	každoroční	k2eAgNnSc4d1	každoroční
zachycení	zachycení	k1gNnSc4	zachycení
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
této	tento	k3xDgFnSc2	tento
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vypracování	vypracování	k1gNnSc1	vypracování
vědeckého	vědecký	k2eAgInSc2d1	vědecký
projektu	projekt	k1gInSc2	projekt
hodnotícího	hodnotící	k2eAgInSc2d1	hodnotící
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
strukturu	struktura	k1gFnSc4	struktura
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc1	vytvoření
příznivých	příznivý	k2eAgFnPc2d1	příznivá
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
na	na	k7c6	na
základě	základ	k1gInSc6	základ
spolehlivých	spolehlivý	k2eAgFnPc2d1	spolehlivá
<g />
.	.	kIx.	.
</s>
<s>
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
ustavení	ustavení	k1gNnSc6	ustavení
pracovní	pracovní	k2eAgFnSc2d1	pracovní
skupiny	skupina	k1gFnSc2	skupina
k	k	k7c3	k
utřídění	utřídění	k1gNnSc3	utřídění
postojů	postoj	k1gInPc2	postoj
a	a	k8xC	a
argumentace	argumentace	k1gFnSc2	argumentace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
stran	strana	k1gFnPc2	strana
sporu	spor	k1gInSc2	spor
<g/>
,	,	kIx,	,
k	k	k7c3	k
vypracování	vypracování	k1gNnSc3	vypracování
plánu	plán	k1gInSc2	plán
regulace	regulace	k1gFnSc2	regulace
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožní	umožnit	k5eAaPmIp3nS	umožnit
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
kormorány	kormorán	k1gMnPc4	kormorán
integrovat	integrovat	k5eAaBmF	integrovat
do	do	k7c2	do
kulturní	kulturní	k2eAgFnSc2d1	kulturní
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
ke	k	k7c3	k
stanovení	stanovení	k1gNnSc3	stanovení
definice	definice	k1gFnSc2	definice
"	"	kIx"	"
<g/>
závažné	závažný	k2eAgFnPc1d1	závažná
škody	škoda	k1gFnPc1	škoda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
k	k	k7c3	k
vypracování	vypracování	k1gNnSc3	vypracování
pokynů	pokyn	k1gInPc2	pokyn
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
výjimek	výjimka	k1gFnPc2	výjimka
ze	z	k7c2	z
směrnice	směrnice	k1gFnSc2	směrnice
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
ptactva	ptactvo	k1gNnSc2	ptactvo
<g/>
,	,	kIx,	,
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
udržitelné	udržitelný	k2eAgFnSc2d1	udržitelná
regulace	regulace	k1gFnSc2	regulace
populace	populace	k1gFnSc2	populace
kormorána	kormorán	k1gMnSc2	kormorán
velkého	velký	k2eAgMnSc2d1	velký
a	a	k8xC	a
hledání	hledání	k1gNnSc3	hledání
řešení	řešení	k1gNnSc2	řešení
těchto	tento	k3xDgInPc2	tento
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povolený	povolený	k2eAgInSc1d1	povolený
odstřel	odstřel	k1gInSc1	odstřel
===	===	k?	===
</s>
</p>
<p>
<s>
Rybářská	rybářský	k2eAgFnSc1d1	rybářská
organizace	organizace	k1gFnSc1	organizace
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
kvůli	kvůli	k7c3	kvůli
likvidaci	likvidace	k1gFnSc3	likvidace
ryb	ryba	k1gFnPc2	ryba
hejny	hejno	k1gNnPc7	hejno
kormoránů	kormorán	k1gMnPc2	kormorán
na	na	k7c6	na
rybnících	rybník	k1gInPc6	rybník
a	a	k8xC	a
v	v	k7c6	v
řekách	řeka	k1gFnPc6	řeka
požádala	požádat	k5eAaPmAgFnS	požádat
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
u	u	k7c2	u
Krajského	krajský	k2eAgInSc2d1	krajský
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2010	[number]	k4	2010
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
časově	časově	k6eAd1	časově
i	i	k9	i
místně	místně	k6eAd1	místně
omezeného	omezený	k2eAgInSc2d1	omezený
odstřelu	odstřel	k1gInSc2	odstřel
a	a	k8xC	a
plašení	plašení	k1gNnSc2	plašení
velkých	velký	k2eAgMnPc2d1	velký
kormoránů	kormorán	k1gMnPc2	kormorán
<g/>
.	.	kIx.	.
</s>
<s>
Odstřely	odstřel	k1gInPc1	odstřel
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
revíru	revír	k1gInSc2	revír
Lužická	lužický	k2eAgFnSc1d1	Lužická
Nisa	Nisa	k1gFnSc1	Nisa
<g/>
,	,	kIx,	,
řeky	řeka	k1gFnPc1	řeka
Jizery	Jizera	k1gFnPc1	Jizera
(	(	kIx(	(
<g/>
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Holanských	Holanský	k2eAgInPc2d1	Holanský
a	a	k8xC	a
Mimoňských	mimoňský	k2eAgInPc2d1	mimoňský
rybníků	rybník	k1gInPc2	rybník
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
rybářů	rybář	k1gMnPc2	rybář
kormoráni	kormorán	k1gMnPc1	kormorán
způsobili	způsobit	k5eAaPmAgMnP	způsobit
škodu	škoda	k1gFnSc4	škoda
přes	přes	k7c4	přes
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
požadavkem	požadavek	k1gInSc7	požadavek
nesouhlasí	souhlasit	k5eNaImIp3nP	souhlasit
ochránci	ochránce	k1gMnPc1	ochránce
přírody	příroda	k1gFnSc2	příroda
z	z	k7c2	z
Jizersko-ještědského	jizerskoeštědský	k2eAgInSc2d1	jizersko-ještědský
spolku	spolek	k1gInSc2	spolek
a	a	k8xC	a
spor	spor	k1gInSc1	spor
není	být	k5eNaImIp3nS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
<g/>
Povolený	povolený	k2eAgInSc1d1	povolený
odstřel	odstřel	k1gInSc1	odstřel
kormoránů	kormorán	k1gMnPc2	kormorán
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
umožnit	umožnit	k5eAaPmF	umožnit
i	i	k9	i
vyhláška	vyhláška	k1gFnSc1	vyhláška
MŽP	MŽP	kA	MŽP
ČR	ČR	kA	ČR
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Benešova	Benešov	k1gInSc2	Benešov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rybáři	Rybář	k1gMnSc3	Rybář
placený	placený	k2eAgInSc4d1	placený
odstřel	odstřel	k1gInSc4	odstřel
kormoránů	kormorán	k1gMnPc2	kormorán
===	===	k?	===
</s>
</p>
<p>
<s>
Rybářské	rybářský	k2eAgInPc1d1	rybářský
svazy	svaz	k1gInPc1	svaz
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
odměňují	odměňovat	k5eAaImIp3nP	odměňovat
odstřel	odstřel	k1gInSc4	odstřel
kormoránů	kormorán	k1gMnPc2	kormorán
myslivci	myslivec	k1gMnSc3	myslivec
částkou	částka	k1gFnSc7	částka
až	až	k9	až
150	[number]	k4	150
Kč	Kč	kA	Kč
za	za	k7c4	za
zabitý	zabitý	k2eAgInSc4d1	zabitý
kus	kus	k1gInSc4	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kormorán	kormorán	k1gMnSc1	kormorán
velký	velký	k2eAgMnSc1d1	velký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
kormorán	kormorán	k1gMnSc1	kormorán
velký	velký	k2eAgMnSc1d1	velký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Phalacrocorax	Phalacrocorax	k1gInSc1	Phalacrocorax
carbo	carba	k1gFnSc5	carba
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
