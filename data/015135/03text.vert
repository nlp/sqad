<s>
Zdislávky	Zdislávka	k1gFnPc1
</s>
<s>
ZdislávkyDílo	ZdislávkyDílo	k1gNnSc1
blažené	blažený	k2eAgFnSc2d1
Zdislavy	Zdislava	k1gFnSc2
Znak	znak	k1gInSc1
Zdislávek	Zdislávek	k1gMnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1946	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
ženský	ženský	k2eAgInSc4d1
institut	institut	k1gInSc4
zasvěceného	zasvěcený	k2eAgInSc2d1
života	život	k1gInSc2
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
Sekulární	sekulární	k2eAgInSc1d1
institut	institut	k1gInSc1
Účel	účel	k1gInSc1
</s>
<s>
katechetická	katechetický	k2eAgFnSc1d1
a	a	k8xC
charitní	charitní	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
Představená	představená	k1gFnSc1
</s>
<s>
Lenka	Lenka	k1gFnSc1
Nezbedová	Nezbedový	k2eAgFnSc1d1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.zdislavky.wbs.cz	www.zdislavky.wbs.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zdislávky	Zdislávka	k1gFnPc1
nazývaný	nazývaný	k2eAgMnSc1d1
také	také	k9
Dílo	dílo	k1gNnSc1
blažené	blažený	k2eAgFnSc2d1
Zdislavy	Zdislava	k1gFnSc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
<g/>
:	:	kIx,
DBZ	DBZ	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
římskokatolický	římskokatolický	k2eAgInSc1d1
sekulární	sekulární	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charitní	charitní	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Zdislava	Zdislava	k1gFnSc1
(	(	kIx(
<g/>
podle	podle	k7c2
Zdislavy	Zdislava	k1gFnSc2
z	z	k7c2
Lemberka	Lemberka	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
Janem	Jan	k1gMnSc7
Bernardinem	bernardin	k1gMnSc7
Skácelem	Skácel	k1gMnSc7
<g/>
,	,	kIx,
MUDr.	MUDr.	kA
Marií	Maria	k1gFnSc7
Veselou	Veselá	k1gFnSc7
a	a	k8xC
sestrami	sestra	k1gFnPc7
III	III	kA
<g/>
.	.	kIx.
řádu	řád	k1gInSc2
sv.	sv.	kA
Dominika	Dominik	k1gMnSc2
v	v	k7c6
Třebíči	Třebíč	k1gFnSc6
po	po	k7c6
skončení	skončení	k1gNnSc6
II	II	kA
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
sekulární	sekulární	k2eAgInSc1d1
institut	institut	k1gInSc1
byl	být	k5eAaImAgInS
potvrzen	potvrdit	k5eAaPmNgInS
ústně	ústně	k6eAd1
podle	podle	k7c2
diecézního	diecézní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
litoměřickým	litoměřický	k2eAgInSc7d1
biskupem	biskup	k1gInSc7
Štěpánem	Štěpán	k1gMnSc7
Trochtou	Trochta	k1gMnSc7
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
pozdější	pozdní	k2eAgMnSc1d2
nástupce	nástupce	k1gMnSc1
Josef	Josef	k1gMnSc1
Koukl	kouknout	k5eAaPmAgMnS
potvrdil	potvrdit	k5eAaPmAgMnS
zřízení	zřízení	k1gNnSc4
podle	podle	k7c2
apoštolské	apoštolský	k2eAgFnSc2d1
konstituce	konstituce	k1gFnSc2
Provida	Provid	k1gMnSc2
Mater	Matra	k1gFnPc2
papeže	papež	k1gMnSc2
Pia	Pius	k1gMnSc2
XII	XII	kA
<g/>
.	.	kIx.
dne	den	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1947	#num#	k4
a	a	k8xC
koncilním	koncilní	k2eAgInSc7d1
dekretem	dekret	k1gInSc7
Perfectae	Perfecta	k1gFnSc2
caritatis	caritatis	k1gFnSc2
a	a	k8xC
kánonu	kánon	k1gInSc2
710	#num#	k4
kodexu	kodex	k1gInSc2
kanonického	kanonický	k2eAgNnSc2d1
práva	právo	k1gNnSc2
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Litoměřicích	Litoměřice	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Komunita	komunita	k1gFnSc1
sester	sestra	k1gFnPc2
Zdislávek	Zdislávka	k1gFnPc2
kolem	kolem	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedicí	Sedice	k1gFnPc2
uprostřed	uprostřed	k6eAd1
je	být	k5eAaImIp3nS
MUDr.	MUDr.	kA
Marie	Marie	k1gFnSc1
Veselá	Veselá	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnSc1
působiště	působiště	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1946	#num#	k4
v	v	k7c6
západních	západní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
v	v	k7c6
Hradci	Hradec	k1gInSc6
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Stodu	Stod	k1gInSc2
a	a	k8xC
v	v	k7c6
Holýšově	Holýšův	k2eAgInSc6d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
za	za	k7c4
II	II	kA
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
nacistický	nacistický	k2eAgInSc1d1
zbrojní	zbrojní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
s	s	k7c7
koncentračním	koncentrační	k2eAgInSc7d1
táborem	tábor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
sekulární	sekulární	k2eAgInSc1d1
institut	institut	k1gInSc1
byl	být	k5eAaImAgInS
umístěn	umístit	k5eAaPmNgInS
ve	v	k7c6
farní	farní	k2eAgFnSc6d1
budově	budova	k1gFnSc6
v	v	k7c6
Hradci	Hradec	k1gInSc6
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
byl	být	k5eAaImAgMnS
i	i	k8xC
kostel	kostel	k1gInSc4
sv.	sv.	kA
Jiří	Jiří	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Již	již	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
1946	#num#	k4
pořádalo	pořádat	k5eAaImAgNnS
Dílo	dílo	k1gNnSc1
blažené	blažený	k2eAgFnSc2d1
Zdislavy	Zdislava	k1gFnSc2
první	první	k4xOgMnSc1
tříměsíční	tříměsíční	k2eAgInSc1d1
kurz	kurz	k1gInSc1
pro	pro	k7c4
katechety	katecheta	k1gMnPc4
a	a	k8xC
farní	farní	k2eAgFnSc1d1
hospodyně	hospodyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
získání	získání	k1gNnSc6
fary	fara	k1gFnSc2
v	v	k7c6
Holýšově	Holýšův	k2eAgFnSc6d1
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1947	#num#	k4
byla	být	k5eAaImAgFnS
činnost	činnost	k1gFnSc1
zdislávek	zdislávka	k1gFnPc2
rozšířena	rozšířen	k2eAgFnSc1d1
a	a	k8xC
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1947	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
dvě	dva	k4xCgNnPc4
oddělení	oddělení	k1gNnPc4
v	v	k7c6
Hradci	Hradec	k1gInSc6
a	a	k8xC
Holýšově	Holýšův	k2eAgInSc6d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
zřízený	zřízený	k2eAgInSc1d1
katechetický	katechetický	k2eAgInSc1d1
kurz	kurz	k1gInSc1
navštěvovalo	navštěvovat	k5eAaImAgNnS
téměř	téměř	k6eAd1
80	#num#	k4
posluchačů	posluchač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
činnost	činnost	k1gFnSc1
zdislávek	zdislávka	k1gFnPc2
byla	být	k5eAaImAgFnS
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
,	,	kIx,
nabídlo	nabídnout	k5eAaPmAgNnS
litoměřické	litoměřický	k2eAgNnSc1d1
biskupství	biskupství	k1gNnSc1
prostory	prostora	k1gFnSc2
v	v	k7c6
Litoměřicích	Litoměřice	k1gInPc6
a	a	k8xC
v	v	k7c6
Jablonném	Jablonné	k1gNnSc6
v	v	k7c6
Podještědí	Podještědí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
přestěhování	přestěhování	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
o	o	k7c6
prázdninách	prázdniny	k1gFnPc6
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Litoměřicích	Litoměřice	k1gInPc6
při	při	k7c6
bohosloveckém	bohoslovecký	k2eAgNnSc6d1
učilišti	učiliště	k1gNnSc6
vznikl	vzniknout	k5eAaPmAgInS
katecheticko-pedagogický	katecheticko-pedagogický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1948	#num#	k4
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1950	#num#	k4
pracovaly	pracovat	k5eAaImAgInP
již	již	k6eAd1
dva	dva	k4xCgInPc1
ročníky	ročník	k1gInPc1
v	v	k7c6
Litoměřicích	Litoměřice	k1gInPc6
(	(	kIx(
<g/>
90	#num#	k4
posluchačů	posluchač	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
dva	dva	k4xCgMnPc1
v	v	k7c6
Jablonném	Jablonné	k1gNnSc6
(	(	kIx(
<g/>
rovněž	rovněž	k9
90	#num#	k4
posluchačů	posluchač	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Litoměřic	Litoměřice	k1gInPc2
přišlo	přijít	k5eAaPmAgNnS
asi	asi	k9
deset	deset	k4xCc1
chlapců	chlapec	k1gMnPc2
od	od	k7c2
salesiánů	salesián	k1gMnPc2
z	z	k7c2
Mníšku	Mníšek	k1gInSc2
pod	pod	k7c7
Brdy	Brdy	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Litoměřicích	Litoměřice	k1gInPc6
byla	být	k5eAaImAgFnS
připravována	připravován	k2eAgFnSc1d1
i	i	k8xC
samostatná	samostatný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
pro	pro	k7c4
varhaníky	varhaník	k1gMnPc4
Stefanikum	Stefanikum	k1gNnSc4
v	v	k7c6
Jezuitské	jezuitský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
politických	politický	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
začaly	začít	k5eAaPmAgFnP
od	od	k7c2
února	únor	k1gInSc2
1948	#num#	k4
zdislávkám	zdislávka	k1gFnPc3
potíže	potíž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Litoměřicích	Litoměřice	k1gInPc6
byla	být	k5eAaImAgFnS
vystěhována	vystěhován	k2eAgFnSc1d1
budova	budova	k1gFnSc1
ve	v	k7c6
Vavřinecké	vavřinecký	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
přišly	přijít	k5eAaPmAgFnP
sestry	sestra	k1gFnPc4
Navštívení	navštívení	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
Chotěšova	Chotěšův	k2eAgNnSc2d1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
pečovaly	pečovat	k5eAaImAgFnP
o	o	k7c4
staré	starý	k2eAgFnPc4d1
ženy	žena	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdislávky	Zdislávka	k1gFnSc2
odešly	odejít	k5eAaPmAgFnP
<g/>
,	,	kIx,
ale	ale	k8xC
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
umístěny	umístit	k5eAaPmNgInP
v	v	k7c6
prázdné	prázdný	k2eAgFnSc6d1
budově	budova	k1gFnSc6
Stefanika	Stefanik	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pronásledování	pronásledování	k1gNnSc1
bylo	být	k5eAaImAgNnS
stále	stále	k6eAd1
tvrdší	tvrdý	k2eAgMnSc1d2
a	a	k8xC
biskup	biskup	k1gMnSc1
Trochta	Trochta	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1950	#num#	k4
přemístit	přemístit	k5eAaPmF
zdislávky	zdislávka	k1gFnPc4
do	do	k7c2
charitního	charitní	k2eAgInSc2d1
domova	domov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunistický	komunistický	k2eAgInSc1d1
útisk	útisk	k1gInSc1
vyvrcholil	vyvrcholit	k5eAaPmAgInS
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1953	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
zakázaná	zakázaný	k2eAgFnSc1d1
veškerá	veškerý	k3xTgFnSc1
charitní	charitní	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdislávky	Zdislávka	k1gFnSc2
pak	pak	k6eAd1
pracovaly	pracovat	k5eAaImAgFnP
v	v	k7c6
civilích	civilí	k2eAgNnPc6d1
zaměstnáních	zaměstnání	k1gNnPc6
především	především	k6eAd1
jako	jako	k8xS,k8xC
zdravotní	zdravotní	k2eAgFnPc1d1
sestry	sestra	k1gFnPc1
a	a	k8xC
současně	současně	k6eAd1
vypomáhaly	vypomáhat	k5eAaImAgFnP
kněžím	kněz	k1gMnPc3
ve	v	k7c6
farnostech	farnost	k1gFnPc6
v	v	k7c6
charitní	charitní	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Základem	základ	k1gInSc7
institutu	institut	k1gInSc2
byly	být	k5eAaImAgFnP
sestry	sestra	k1gFnPc1
se	s	k7c7
zásvětnými	zásvětný	k2eAgInPc7d1
sliby	slib	k1gInPc7
působící	působící	k2eAgFnSc2d1
předevšm	předevšm	k6eAd1
v	v	k7c6
církvi	církev	k1gFnSc6
jako	jako	k8xC,k8xS
pomocnice	pomocnice	k1gFnSc1
na	na	k7c6
farách	fara	k1gFnPc6
<g/>
,	,	kIx,
katechetky	katechetka	k1gFnPc1
apod.	apod.	kA
a	a	k8xC
ve	v	k7c6
zdravotnictví	zdravotnictví	k1gNnSc6
jako	jako	k9
kvalifikované	kvalifikovaný	k2eAgFnPc4d1
zdravotní	zdravotní	k2eAgFnPc4d1
sestry	sestra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jimi	on	k3xPp3gMnPc7
spravovaných	spravovaný	k2eAgInPc6d1
domech	dům	k1gInPc6
nacházeli	nacházet	k5eAaImAgMnP
útočiště	útočiště	k1gNnSc4
staří	starý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
dominikánského	dominikánský	k2eAgInSc2d1
řádu	řád	k1gInSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
Litoměřice	Litoměřice	k1gInPc4
a	a	k8xC
Jablonné	Jablonná	k1gFnPc4
v	v	k7c6
Podještědí	Podještědí	k1gNnSc6
sloužily	sloužit	k5eAaImAgFnP
zdislávky	zdislávka	k1gFnPc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Mladé	mladý	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
brněnské	brněnský	k2eAgFnSc6d1
diecézi	diecéze	k1gFnSc6
a	a	k8xC
v	v	k7c6
olomoucké	olomoucký	k2eAgFnSc6d1
arcidiecézi	arcidiecéze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
přijetí	přijetí	k1gNnSc6
se	se	k3xPyFc4
zájemkyně	zájemkyně	k1gFnPc1
setkaly	setkat	k5eAaPmAgFnP
dvakrát	dvakrát	k6eAd1
či	či	k8xC
třikrát	třikrát	k6eAd1
se	s	k7c7
sestrou	sestra	k1gFnSc7
představenou	představená	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
kladném	kladný	k2eAgNnSc6d1
vyřízení	vyřízení	k1gNnSc6
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
čekatelkami	čekatelka	k1gFnPc7
a	a	k8xC
po	po	k7c6
půl	půl	k1xP
až	až	k9
jedném	jedný	k2eAgInSc6d1
roce	rok	k1gInSc6
následovala	následovat	k5eAaImAgFnS
kandidatura	kandidatura	k1gFnSc1
a	a	k8xC
po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
skládaly	skládat	k5eAaImAgInP
sliby	slib	k1gInPc1
(	(	kIx(
<g/>
třikrát	třikrát	k6eAd1
na	na	k7c4
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
<g/>
,	,	kIx,
jedenkrát	jedenkrát	k6eAd1
na	na	k7c4
3	#num#	k4
roky	rok	k1gInPc4
<g/>
)	)	kIx)
a	a	k8xC
nakonec	nakonec	k6eAd1
následují	následovat	k5eAaImIp3nP
doživotní	doživotní	k2eAgInPc1d1
sliby	slib	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
se	se	k3xPyFc4
komunita	komunita	k1gFnSc1
sester	sestra	k1gFnPc2
zdislávek	zdislávka	k1gFnPc2
přestěhovala	přestěhovat	k5eAaPmAgFnS
z	z	k7c2
Litoměřic	Litoměřice	k1gInPc2
do	do	k7c2
brněnské	brněnský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
na	na	k7c6
Moravec	Moravec	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
žijí	žít	k5eAaImIp3nP
starší	starý	k2eAgFnPc4d2
členky	členka	k1gFnPc4
<g/>
,	,	kIx,
mladší	mladý	k2eAgFnPc4d2
působí	působit	k5eAaImIp3nP
v	v	k7c6
různých	různý	k2eAgNnPc6d1
civilních	civilní	k2eAgNnPc6d1
povoláních	povolání	k1gNnPc6
souvisejících	související	k2eAgInPc2d1
s	s	k7c7
charismatem	charisma	k1gNnSc7
institutu	institut	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Představené	představená	k1gFnPc1
Zdislávek	Zdislávka	k1gFnPc2
</s>
<s>
MUDr.	MUDr.	kA
Marie	Marie	k1gFnSc1
Veselá	Veselá	k1gFnSc1
</s>
<s>
MUDr.	MUDr.	kA
Jitka	Jitka	k1gFnSc1
Dominika	Dominik	k1gMnSc2
Krausová	Krausová	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
Legnerová	Legnerová	k1gFnSc1
(	(	kIx(
<g/>
†	†	k?
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Lenka	Lenka	k1gFnSc1
Bernadeta	Bernadet	k2eAgFnSc1d1
Nezbedová	Nezbedový	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
MACEK	Macek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
950	#num#	k4
let	léto	k1gNnPc2
litoměřické	litoměřický	k2eAgFnSc2d1
kapituly	kapitula	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostelní	kostelní	k2eAgInSc1d1
Vydří	vydří	k2eAgInSc1d1
<g/>
:	:	kIx,
Karmelitánské	karmelitánský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
320	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7195	#num#	k4
<g/>
-	-	kIx~
<g/>
121	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
229	#num#	k4
<g/>
-	-	kIx~
<g/>
230	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MICHÁLKOVÁ	Michálková	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřela	zemřít	k5eAaPmAgFnS
sestra	sestra	k1gFnSc1
zdislávka	zdislávka	k1gFnSc1
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc1
Legnerová	Legnerová	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biskupství	biskupství	k1gNnSc1
litoměřické	litoměřický	k2eAgInPc1d1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2014-06-19	2014-06-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
NEZBEDOVÁ	NEZBEDOVÁ	kA
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sekulární	sekulární	k2eAgInSc1d1
institut	institut	k1gInSc1
Dílo	dílo	k1gNnSc1
blažené	blažený	k2eAgFnSc2d1
Zdislavy	Zdislava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cyrilometodějská	cyrilometodějský	k2eAgFnSc1d1
teologická	teologický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
UP	UP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Martin	Martin	k1gMnSc1
Filip	Filip	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
HRUDNÍKOVÁ	hrudníkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Miriam	Miriam	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeholní	řeholní	k2eAgInSc4d1
život	život	k1gInSc4
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
:	:	kIx,
řeholní	řeholní	k2eAgInPc4d1
řády	řád	k1gInPc4
a	a	k8xC
kongregace	kongregace	k1gFnPc4
<g/>
,	,	kIx,
sekulární	sekulární	k2eAgInPc4d1
instituty	institut	k1gInPc4
a	a	k8xC
společnosti	společnost	k1gFnPc4
apoštolského	apoštolský	k2eAgInSc2d1
života	život	k1gInSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostelní	kostelní	k2eAgInSc1d1
Vydří	vydří	k2eAgInSc1d1
<g/>
:	:	kIx,
Karmelitánské	karmelitánský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7192	#num#	k4
<g/>
-	-	kIx~
<g/>
222	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zdislávky	Zdislávka	k1gFnPc1
<g/>
,	,	kIx,
web	web	k1gInSc1
sester	sestra	k1gFnPc2
</s>
<s>
Dvě	dva	k4xCgFnPc1
nové	nový	k2eAgFnPc1d1
sestry	sestra	k1gFnPc1
zdislávky	zdislávka	k1gFnSc2
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
dominikánská	dominikánský	k2eAgFnSc1d1
provincie	provincie	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
