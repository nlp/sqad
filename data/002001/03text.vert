<s>
Internet	Internet	k1gInSc1	Internet
Protocol	Protocol	k1gInSc1	Protocol
version	version	k1gInSc1	version
4	[number]	k4	4
(	(	kIx(	(
<g/>
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
revize	revize	k1gFnSc1	revize
IP	IP	kA	IP
(	(	kIx(	(
<g/>
Internet	Internet	k1gInSc1	Internet
Protocol	Protocol	k1gInSc1	Protocol
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gFnPc4	jeho
první	první	k4xOgFnPc4	první
verze	verze	k1gFnPc4	verze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
masivně	masivně	k6eAd1	masivně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
IPv	IPv	k1gFnSc7	IPv
<g/>
6	[number]	k4	6
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sítě	síť	k1gFnSc2	síť
Internet	Internet	k1gInSc1	Internet
<g/>
.	.	kIx.	.
</s>
<s>
IPv	IPv	k?	IPv
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
IETF	IETF	kA	IETF
v	v	k7c4	v
RFC	RFC	kA	RFC
791	[number]	k4	791
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
RFC	RFC	kA	RFC
760	[number]	k4	760
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
standardizována	standardizován	k2eAgFnSc1d1	standardizována
jako	jako	k8xC	jako
MIL-STD-1777	MIL-STD-1777	k1gMnPc4	MIL-STD-1777
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
obrany	obrana	k1gFnSc2	obrana
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
IPv	IPv	k?	IPv
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
datově	datově	k6eAd1	datově
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
protokol	protokol	k1gInSc1	protokol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
sítích	síť	k1gFnPc6	síť
s	s	k7c7	s
přepojováním	přepojování	k1gNnSc7	přepojování
paketů	paket	k1gInPc2	paket
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ethernet	Ethernet	k1gInSc1	Ethernet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
protokol	protokol	k1gInSc4	protokol
přepravující	přepravující	k2eAgNnPc4d1	přepravující
data	datum	k1gNnPc4	datum
bez	bez	k7c2	bez
záruky	záruka	k1gFnSc2	záruka
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
negarantuje	garantovat	k5eNaBmIp3nS	garantovat
ani	ani	k8xC	ani
doručení	doručení	k1gNnSc4	doručení
ani	ani	k8xC	ani
zachování	zachování	k1gNnSc4	zachování
pořadí	pořadí	k1gNnSc2	pořadí
ani	ani	k8xC	ani
vyloučení	vyloučení	k1gNnSc2	vyloučení
duplicit	duplicita	k1gFnPc2	duplicita
<g/>
.	.	kIx.	.
</s>
<s>
Zajištění	zajištění	k1gNnSc1	zajištění
těchto	tento	k3xDgFnPc2	tento
záruk	záruka	k1gFnPc2	záruka
je	být	k5eAaImIp3nS	být
ponecháno	ponechat	k5eAaPmNgNnS	ponechat
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
představuje	představovat	k5eAaImIp3nS	představovat
protokol	protokol	k1gInSc1	protokol
TCP	TCP	kA	TCP
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
vrstvě	vrstva	k1gFnSc6	vrstva
ponechána	ponechat	k5eAaPmNgFnS	ponechat
kontrola	kontrola	k1gFnSc1	kontrola
integrity	integrita	k1gFnSc2	integrita
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
IPv	IPv	k1gMnSc1	IPv
<g/>
4	[number]	k4	4
datagram	datagram	k1gInSc1	datagram
nese	nést	k5eAaImIp3nS	nést
pouze	pouze	k6eAd1	pouze
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
kontrolním	kontrolní	k2eAgInSc6d1	kontrolní
součtu	součet	k1gInSc6	součet
hlavičky	hlavička	k1gFnSc2	hlavička
datagramu	datagram	k1gInSc2	datagram
se	s	k7c7	s
služebními	služební	k2eAgInPc7d1	služební
údaji	údaj	k1gInPc7	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
protokol	protokol	k1gInSc1	protokol
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
omezený	omezený	k2eAgInSc1d1	omezený
adresní	adresní	k2eAgInSc1d1	adresní
prostor	prostor	k1gInSc1	prostor
-	-	kIx~	-
teoreticky	teoreticky	k6eAd1	teoreticky
232	[number]	k4	232
adres	adresa	k1gFnPc2	adresa
(	(	kIx(	(
<g/>
cca	cca	kA	cca
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
109	[number]	k4	109
=	=	kIx~	=
4	[number]	k4	4
miliardy	miliarda	k4xCgFnSc2	miliarda
adres	adresa	k1gFnPc2	adresa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
adresy	adresa	k1gFnPc1	adresa
jsou	být	k5eAaImIp3nP	být
sdružovány	sdružovat	k5eAaImNgFnP	sdružovat
kvůli	kvůli	k7c3	kvůli
snadnějšímu	snadný	k2eAgNnSc3d2	snazší
směrování	směrování	k1gNnSc3	směrování
do	do	k7c2	do
podsítí	podsíť	k1gFnPc2	podsíť
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
maska	maska	k1gFnSc1	maska
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
též	též	k9	též
nevyhovuje	vyhovovat	k5eNaImIp3nS	vyhovovat
současnému	současný	k2eAgInSc3d1	současný
nárůstu	nárůst	k1gInSc3	nárůst
přenosových	přenosový	k2eAgFnPc2d1	přenosová
rychlostí	rychlost	k1gFnPc2	rychlost
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
přenosu	přenos	k1gInSc2	přenos
multimediálních	multimediální	k2eAgNnPc2d1	multimediální
dat	datum	k1gNnPc2	datum
-	-	kIx~	-
videokonference	videokonference	k1gFnSc1	videokonference
<g/>
,	,	kIx,	,
internetová	internetový	k2eAgFnSc1d1	internetová
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
telefonování	telefonování	k1gNnSc1	telefonování
po	po	k7c6	po
internetu	internet	k1gInSc6	internet
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
byly	být	k5eAaImAgFnP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
poslední	poslední	k2eAgInPc1d1	poslední
bloky	blok	k1gInPc1	blok
adres	adresa	k1gFnPc2	adresa
protokolu	protokol	k1gInSc2	protokol
IPv	IPv	k1gFnSc2	IPv
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
postupně	postupně	k6eAd1	postupně
probíhá	probíhat	k5eAaImIp3nS	probíhat
přechod	přechod	k1gInSc4	přechod
na	na	k7c4	na
protokol	protokol	k1gInSc4	protokol
IPv	IPv	k1gMnPc2	IPv
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Uložen	uložen	k2eAgMnSc1d1	uložen
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
endianu	endian	k1gInSc6	endian
<g/>
.	.	kIx.	.
</s>
<s>
Datagram	Datagram	k1gInSc1	Datagram
IPv	IPv	k1gFnSc2	IPv
<g/>
4	[number]	k4	4
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hlavičku	hlavička	k1gFnSc4	hlavička
se	s	k7c7	s
služebními	služební	k2eAgInPc7d1	služební
údaji	údaj	k1gInPc7	údaj
nutnými	nutný	k2eAgInPc7d1	nutný
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
a	a	k8xC	a
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
následují	následovat	k5eAaImIp3nP	následovat
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
hlavičky	hlavička	k1gFnSc2	hlavička
je	být	k5eAaImIp3nS	být
zarovnán	zarovnat	k5eAaPmNgInS	zarovnat
na	na	k7c4	na
násobek	násobek	k1gInSc4	násobek
čtveřice	čtveřice	k1gFnSc1	čtveřice
bajtů	bajt	k1gInPc2	bajt
pomocí	pomocí	k7c2	pomocí
výplně	výplň	k1gFnSc2	výplň
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
padding	padding	k1gInSc1	padding
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strukturu	struktura	k1gFnSc4	struktura
IP	IP	kA	IP
datagramu	datagram	k1gInSc2	datagram
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
tabulka	tabulka	k1gFnSc1	tabulka
uvedená	uvedený	k2eAgFnSc1d1	uvedená
nahoře	nahoře	k6eAd1	nahoře
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
popis	popis	k1gInSc4	popis
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
polí	pole	k1gFnPc2	pole
<g/>
:	:	kIx,	:
Verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
verze	verze	k1gFnSc1	verze
protokolu	protokol	k1gInSc2	protokol
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
x	x	k?	x
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
IHL	IHL	kA	IHL
<g/>
:	:	kIx,	:
délka	délka	k1gFnSc1	délka
hlavičky	hlavička	k1gFnSc2	hlavička
v	v	k7c6	v
půl	půl	k1xP	půl
bajtu	bajt	k1gInSc6	bajt
<g/>
;	;	kIx,	;
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vynásobená	vynásobený	k2eAgFnSc1d1	vynásobená
4	[number]	k4	4
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
typická	typický	k2eAgFnSc1d1	typická
a	a	k8xC	a
minimální	minimální	k2eAgFnSc1d1	minimální
délka	délka	k1gFnSc1	délka
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
x	x	k?	x
<g/>
5	[number]	k4	5
<<	<<	k?	<<
2	[number]	k4	2
<g/>
)	)	kIx)	)
=	=	kIx~	=
20	[number]	k4	20
bajtů	bajt	k1gInPc2	bajt
<g/>
,	,	kIx,	,
a	a	k8xC	a
maximální	maximální	k2eAgFnSc1d1	maximální
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
xF	xF	k?	xF
<<	<<	k?	<<
2	[number]	k4	2
<g/>
)	)	kIx)	)
=	=	kIx~	=
60	[number]	k4	60
bajtů	bajt	k1gInPc2	bajt
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
služby	služba	k1gFnSc2	služba
(	(	kIx(	(
<g/>
TOS	TOS	kA	TOS
<g/>
,	,	kIx,	,
Type	typ	k1gInSc5	typ
of	of	k?	of
Service	Service	k1gFnSc1	Service
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
původních	původní	k2eAgFnPc2d1	původní
představ	představa	k1gFnPc2	představa
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
položka	položka	k1gFnSc1	položka
umožnit	umožnit	k5eAaPmF	umožnit
odesilateli	odesilatel	k1gMnSc3	odesilatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zvolil	zvolit	k5eAaPmAgInS	zvolit
charakter	charakter	k1gInSc4	charakter
přepravní	přepravní	k2eAgFnSc2d1	přepravní
služby	služba	k1gFnSc2	služba
ideální	ideální	k2eAgFnSc2d1	ideální
pro	pro	k7c4	pro
dotyčný	dotyčný	k2eAgInSc4d1	dotyčný
datagram	datagram	k1gInSc4	datagram
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
bity	bit	k1gInPc4	bit
znamenaly	znamenat	k5eAaImAgFnP	znamenat
např.	např.	kA	např.
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
nejmenší	malý	k2eAgNnSc4d3	nejmenší
zpoždění	zpoždění	k1gNnSc4	zpoždění
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
šířku	šířka	k1gFnSc4	šířka
pásma	pásmo	k1gNnSc2	pásmo
či	či	k8xC	či
nejlevnější	levný	k2eAgFnSc4d3	nejlevnější
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Směrování	směrování	k1gNnSc1	směrování
pak	pak	k6eAd1	pak
mělo	mít	k5eAaImAgNnS	mít
brát	brát	k5eAaImF	brát
ohled	ohled	k1gInSc4	ohled
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
TOS	TOS	kA	TOS
a	a	k8xC	a
volit	volit	k5eAaImF	volit
z	z	k7c2	z
alternativních	alternativní	k2eAgFnPc2d1	alternativní
tras	trasa	k1gFnPc2	trasa
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nejlépe	dobře	k6eAd3	dobře
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
požadavkům	požadavek	k1gInPc3	požadavek
datagramu	datagram	k1gInSc2	datagram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
však	však	k9	však
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
položka	položka	k1gFnSc1	položka
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
podobným	podobný	k2eAgInPc3d1	podobný
účelům	účel	k1gInPc3	účel
-	-	kIx~	-
nese	nést	k5eAaImIp3nS	nést
značku	značka	k1gFnSc4	značka
pro	pro	k7c4	pro
mechanismy	mechanismus	k1gInPc4	mechanismus
zajišťující	zajišťující	k2eAgFnSc2d1	zajišťující
služby	služba	k1gFnSc2	služba
s	s	k7c7	s
definovanou	definovaný	k2eAgFnSc7d1	definovaná
kvalitou	kvalita	k1gFnSc7	kvalita
(	(	kIx(	(
<g/>
QoS	QoS	k1gFnSc7	QoS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
délka	délka	k1gFnSc1	délka
datagramu	datagram	k1gInSc2	datagram
v	v	k7c6	v
bajtech	bajt	k1gInPc6	bajt
<g/>
.	.	kIx.	.
</s>
<s>
Identifikace	identifikace	k1gFnSc1	identifikace
<g/>
:	:	kIx,	:
odesilatel	odesilatel	k1gMnSc1	odesilatel
přidělí	přidělit	k5eAaPmIp3nS	přidělit
každému	každý	k3xTgInSc3	každý
odeslanému	odeslaný	k2eAgInSc3d1	odeslaný
paketu	paket	k1gInSc3	paket
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
identifikátor	identifikátor	k1gInSc1	identifikátor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgInS	být
datagram	datagram	k1gInSc4	datagram
při	při	k7c6	při
přepravě	přeprava	k1gFnSc6	přeprava
fragmentován	fragmentován	k2eAgInSc1d1	fragmentován
<g/>
,	,	kIx,	,
pozná	poznat	k5eAaPmIp3nS	poznat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
položky	položka	k1gFnSc2	položka
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
fragmenty	fragment	k1gInPc1	fragment
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
sobě	se	k3xPyFc3	se
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
identifikátor	identifikátor	k1gInSc4	identifikátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
<g/>
:	:	kIx,	:
slouží	sloužit	k5eAaImIp3nP	sloužit
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
fragmentace	fragmentace	k1gFnSc2	fragmentace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
je	být	k5eAaImIp3nS	být
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
fragment	fragment	k1gInSc4	fragment
zakazující	zakazující	k2eAgInSc4d1	zakazující
tento	tento	k3xDgInSc4	tento
datagram	datagram	k1gInSc4	datagram
fragmentovat	fragmentovat	k5eAaPmF	fragmentovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
More	mor	k1gInSc5	mor
fragments	fragments	k6eAd1	fragments
nastavuje	nastavovat	k5eAaImIp3nS	nastavovat
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
není	být	k5eNaImIp3nS	být
fragmentem	fragment	k1gInSc7	fragment
posledním	poslední	k2eAgInSc7d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Offset	offset	k1gInSc1	offset
fragmentu	fragment	k1gInSc2	fragment
<g/>
:	:	kIx,	:
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
na	na	k7c6	na
jaké	jaký	k3yQgFnSc6	jaký
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
datagramu	datagram	k1gInSc6	datagram
začíná	začínat	k5eAaImIp3nS	začínat
tento	tento	k3xDgInSc4	tento
fragment	fragment	k1gInSc4	fragment
<g/>
.	.	kIx.	.
</s>
<s>
Jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
osm	osm	k4xCc4	osm
bajtů	bajt	k1gInPc2	bajt
<g/>
.	.	kIx.	.
</s>
<s>
TTL	TTL	kA	TTL
(	(	kIx(	(
<g/>
Time	Time	k1gFnSc4	Time
To	ten	k3xDgNnSc1	ten
Live	Live	k1gNnSc1	Live
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
představuje	představovat	k5eAaImIp3nS	představovat
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
zacyklení	zacyklení	k1gNnSc3	zacyklení
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
směrovač	směrovač	k1gInSc1	směrovač
zmenší	zmenšit	k5eAaPmIp3nS	zmenšit
tuto	tento	k3xDgFnSc4	tento
hodnotu	hodnota	k1gFnSc4	hodnota
o	o	k7c4	o
jedničku	jednička	k1gFnSc4	jednička
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
o	o	k7c4	o
počet	počet	k1gInSc4	počet
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
datagram	datagram	k1gInSc1	datagram
ve	v	k7c6	v
směrovači	směrovač	k1gInSc6	směrovač
strávil	strávit	k5eAaPmAgMnS	strávit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zde	zde	k6eAd1	zde
čeká	čekat	k5eAaImIp3nS	čekat
déle	dlouho	k6eAd2	dlouho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tím	ten	k3xDgNnSc7	ten
TTL	TTL	kA	TTL
nabude	nabýt	k5eAaPmIp3nS	nabýt
hodnotu	hodnota	k1gFnSc4	hodnota
nula	nula	k1gFnSc1	nula
<g/>
,	,	kIx,	,
datagram	datagram	k1gInSc1	datagram
zahodí	zahodit	k5eAaPmIp3nS	zahodit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vypršela	vypršet	k5eAaPmAgFnS	vypršet
jeho	jeho	k3xOp3gFnSc4	jeho
životnost	životnost	k1gFnSc4	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
<g/>
:	:	kIx,	:
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
protokolu	protokol	k1gInSc3	protokol
vyšší	vysoký	k2eAgFnSc2d2	vyšší
vrstvy	vrstva	k1gFnSc2	vrstva
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
data	datum	k1gNnSc2	datum
předat	předat	k5eAaPmF	předat
při	při	k7c6	při
doručení	doručení	k1gNnSc6	doručení
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
protokolů	protokol	k1gInPc2	protokol
definována	definován	k2eAgFnSc1d1	definována
v	v	k7c4	v
RFC	RFC	kA	RFC
1700	[number]	k4	1700
(	(	kIx(	(
<g/>
TCP	TCP	kA	TCP
<g/>
:	:	kIx,	:
6	[number]	k4	6
<g/>
,	,	kIx,	,
UDP	UDP	kA	UDP
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
,	,	kIx,	,
ICMP	ICMP	kA	ICMP
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
,	,	kIx,	,
EGP	EGP	kA	EGP
<g/>
:	:	kIx,	:
8	[number]	k4	8
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
RFC	RFC	kA	RFC
1700	[number]	k4	1700
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
překonáno	překonat	k5eAaPmNgNnS	překonat
novým	nový	k2eAgInSc7d1	nový
standardem	standard	k1gInSc7	standard
RFC	RFC	kA	RFC
3232	[number]	k4	3232
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
databáze	databáze	k1gFnPc4	databáze
organizace	organizace	k1gFnSc2	organizace
IANA	Ianus	k1gMnSc2	Ianus
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
stránky	stránka	k1gFnSc2	stránka
<g/>
:	:	kIx,	:
http://www.iana.org	[url]	k1gMnSc1	http://www.iana.org
.	.	kIx.	.
</s>
<s>
Kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
součet	součet	k1gInSc1	součet
hlavičky	hlavička	k1gFnSc2	hlavička
<g/>
:	:	kIx,	:
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
ověření	ověření	k1gNnSc3	ověření
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Počítá	počítat	k5eAaImIp3nS	počítat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
hlavičky	hlavička	k1gFnSc2	hlavička
a	a	k8xC	a
pokud	pokud	k8xS	pokud
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
datagram	datagram	k1gInSc1	datagram
bude	být	k5eAaImBp3nS	být
zahozen	zahozen	k2eAgInSc1d1	zahozen
<g/>
.	.	kIx.	.
</s>
<s>
Adresa	adresa	k1gFnSc1	adresa
odesílatele	odesílatel	k1gMnSc2	odesílatel
<g/>
:	:	kIx,	:
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
adresa	adresa	k1gFnSc1	adresa
síťového	síťový	k2eAgNnSc2d1	síťové
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
datagram	datagram	k1gInSc4	datagram
vyslalo	vyslat	k5eAaPmAgNnS	vyslat
<g/>
.	.	kIx.	.
</s>
<s>
Adresa	adresa	k1gFnSc1	adresa
cíle	cíl	k1gInSc2	cíl
<g/>
:	:	kIx,	:
IP	IP	kA	IP
adresa	adresa	k1gFnSc1	adresa
síťového	síťový	k2eAgNnSc2d1	síťové
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
je	být	k5eAaImIp3nS	být
datagram	datagram	k1gInSc1	datagram
určen	určit	k5eAaPmNgInS	určit
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
<g/>
:	:	kIx,	:
různé	různý	k2eAgFnPc1d1	různá
rozšiřující	rozšiřující	k2eAgFnPc1d1	rozšiřující
informace	informace	k1gFnPc1	informace
či	či	k8xC	či
požadavky	požadavek	k1gInPc1	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
lze	lze	k6eAd1	lze
předepsat	předepsat	k5eAaPmF	předepsat
sérii	série	k1gFnSc4	série
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
má	mít	k5eAaImIp3nS	mít
datagram	datagram	k1gInSc1	datagram
projít	projít	k5eAaPmF	projít
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
obvykle	obvykle	k6eAd1	obvykle
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
datagramu	datagram	k1gInSc6	datagram
použity	použit	k2eAgFnPc1d1	použita
(	(	kIx(	(
<g/>
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
barevně	barevně	k6eAd1	barevně
odlišeny	odlišen	k2eAgInPc1d1	odlišen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
<g/>
:	:	kIx,	:
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
další	další	k2eAgInPc4d1	další
zapouzdřené	zapouzdřený	k2eAgInPc4d1	zapouzdřený
protokoly	protokol	k1gInPc4	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
byly	být	k5eAaImAgFnP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
poslední	poslední	k2eAgInPc4d1	poslední
bloky	blok	k1gInPc4	blok
adres	adresa	k1gFnPc2	adresa
protokolu	protokol	k1gInSc2	protokol
IPv	IPv	k1gFnSc2	IPv
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
událostí	událost	k1gFnPc2	událost
historie	historie	k1gFnSc2	historie
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
<s>
Spotřeba	spotřeba	k1gFnSc1	spotřeba
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
stále	stále	k6eAd1	stále
stoupá	stoupat	k5eAaImIp3nS	stoupat
a	a	k8xC	a
největším	veliký	k2eAgMnSc7d3	veliký
spotřebitelem	spotřebitel	k1gMnSc7	spotřebitel
je	být	k5eAaImIp3nS	být
asijsko-pacifický	asijskoacifický	k2eAgInSc1d1	asijsko-pacifický
region	region	k1gInSc1	region
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
poslední	poslední	k2eAgInSc4d1	poslední
přidělený	přidělený	k2eAgInSc4d1	přidělený
blok	blok	k1gInSc4	blok
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
stávající	stávající	k2eAgFnSc7d1	stávající
zásobou	zásoba	k1gFnSc7	zásoba
vystačí	vystačit	k5eAaBmIp3nS	vystačit
maximálně	maximálně	k6eAd1	maximálně
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
vydrží	vydržet	k5eAaPmIp3nS	vydržet
zásoby	zásoba	k1gFnPc4	zásoba
IPv	IPv	k1gFnSc2	IPv
<g/>
4	[number]	k4	4
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
mohou	moct	k5eAaImIp3nP	moct
počítat	počítat	k5eAaImF	počítat
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
