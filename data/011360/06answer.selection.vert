<s>
McLaren	McLarna	k1gFnPc2	McLarna
tento	tento	k3xDgInSc4	tento
výsledek	výsledek	k1gInSc4	výsledek
rozhodně	rozhodně	k6eAd1	rozhodně
nemohl	moct	k5eNaImAgMnS	moct
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
sezónou	sezóna	k1gFnSc7	sezóna
favoritem	favorit	k1gInSc7	favorit
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
auto	auto	k1gNnSc1	auto
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
především	především	k9	především
v	v	k7c6	v
úvodních	úvodní	k2eAgInPc6d1	úvodní
dvou	dva	k4xCgInPc6	dva
závodech	závod	k1gInPc6	závod
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
jeho	jeho	k3xOp3gFnSc1	jeho
nižší	nízký	k2eAgFnSc1d2	nižší
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
problémem	problém	k1gInSc7	problém
McLarenu	McLaren	k1gInSc2	McLaren
<g/>
.	.	kIx.	.
</s>
