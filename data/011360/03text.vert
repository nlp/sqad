<p>
<s>
McLaren	McLarna	k1gFnPc2	McLarna
MP	MP	kA	MP
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
(	(	kIx(	(
<g/>
McLaren	McLarno	k1gNnPc2	McLarno
Project	Projecta	k1gFnPc2	Projecta
4	[number]	k4	4
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
design	design	k1gInSc1	design
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
monopost	monopost	k1gInSc1	monopost
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
McLaren-Mercedes	McLaren-Mercedes	k1gMnSc1	McLaren-Mercedes
účastnil	účastnit	k5eAaImAgMnS	účastnit
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Vůz	vůz	k1gInSc4	vůz
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
Adrian	Adrian	k1gMnSc1	Adrian
Newey	Newea	k1gFnSc2	Newea
a	a	k8xC	a
Neil	Neil	k1gMnSc1	Neil
Oatley	Oatlea	k1gFnSc2	Oatlea
a	a	k8xC	a
navázali	navázat	k5eAaPmAgMnP	navázat
na	na	k7c4	na
modely	model	k1gInPc4	model
z	z	k7c2	z
předchozích	předchozí	k2eAgNnPc2d1	předchozí
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
McLaren	McLarna	k1gFnPc2	McLarna
MP	MP	kA	MP
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
a	a	k8xC	a
McLaren	McLarna	k1gFnPc2	McLarna
MP	MP	kA	MP
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vůz	vůz	k1gInSc1	vůz
byl	být	k5eAaImAgInS	být
osazen	osadit	k5eAaPmNgInS	osadit
deseti	deset	k4xCc7	deset
válcovým	válcový	k2eAgInSc7d1	válcový
motorem	motor	k1gInSc7	motor
Ilmor	Ilmor	k1gMnSc1	Ilmor
FO110J	FO110J	k1gMnSc1	FO110J
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Mercedes	mercedes	k1gInSc1	mercedes
<g/>
.	.	kIx.	.
</s>
<s>
Motor	motor	k1gInSc1	motor
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
výkonný	výkonný	k2eAgMnSc1d1	výkonný
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
dobře	dobře	k6eAd1	dobře
sladit	sladit	k5eAaImF	sladit
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
úspěšných	úspěšný	k2eAgFnPc6d1	úspěšná
sezónách	sezóna	k1gFnPc6	sezóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Mika	Mik	k1gMnSc4	Mik
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
porážka	porážka	k1gFnSc1	porážka
od	od	k7c2	od
Ferrari	ferrari	k1gNnSc2	ferrari
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
model	model	k1gInSc1	model
dokázal	dokázat	k5eAaPmAgInS	dokázat
zvítězit	zvítězit	k5eAaPmF	zvítězit
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
závodech	závod	k1gInPc6	závod
a	a	k8xC	a
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
skončil	skončit	k5eAaPmAgInS	skončit
celkově	celkově	k6eAd1	celkově
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
spolujezdec	spolujezdec	k1gMnSc1	spolujezdec
David	David	k1gMnSc1	David
Coulthard	Coulthard	k1gMnSc1	Coulthard
skončil	skončit	k5eAaPmAgMnS	skončit
třetí	třetí	k4xOgMnSc1	třetí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
týmu	tým	k1gInSc6	tým
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
druhé	druhý	k4xOgNnSc1	druhý
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
poháru	pohár	k1gInSc6	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
<g/>
.	.	kIx.	.
</s>
<s>
McLaren	McLarna	k1gFnPc2	McLarna
tento	tento	k3xDgInSc4	tento
výsledek	výsledek	k1gInSc4	výsledek
rozhodně	rozhodně	k6eAd1	rozhodně
nemohl	moct	k5eNaImAgMnS	moct
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
sezónou	sezóna	k1gFnSc7	sezóna
favoritem	favorit	k1gInSc7	favorit
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
auto	auto	k1gNnSc1	auto
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
především	především	k9	především
v	v	k7c6	v
úvodních	úvodní	k2eAgInPc6d1	úvodní
dvou	dva	k4xCgInPc6	dva
závodech	závod	k1gInPc6	závod
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
jeho	jeho	k3xOp3gFnSc1	jeho
nižší	nízký	k2eAgFnSc1d2	nižší
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
problémem	problém	k1gInSc7	problém
McLarenu	McLaren	k1gInSc2	McLaren
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úvodních	úvodní	k2eAgInPc6d1	úvodní
závodech	závod	k1gInPc6	závod
se	se	k3xPyFc4	se
problémy	problém	k1gInPc1	problém
vyřešily	vyřešit	k5eAaPmAgInP	vyřešit
a	a	k8xC	a
vůz	vůz	k1gInSc1	vůz
byl	být	k5eAaImAgInS	být
výrazně	výrazně	k6eAd1	výrazně
spolehlivější	spolehlivý	k2eAgMnSc1d2	spolehlivější
než	než	k8xS	než
v	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
úvodních	úvodní	k2eAgInPc6d1	úvodní
závodech	závod	k1gInPc6	závod
tým	tým	k1gInSc4	tým
nezískal	získat	k5eNaPmAgInS	získat
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jak	jak	k6eAd1	jak
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
tak	tak	k8xS	tak
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
startoval	startovat	k5eAaBmAgMnS	startovat
Mika	Mik	k1gMnSc2	Mik
Häkkinen	Häkkinna	k1gFnPc2	Häkkinna
z	z	k7c2	z
první	první	k4xOgFnSc2	první
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
závodech	závod	k1gInPc6	závod
již	již	k6eAd1	již
jezdci	jezdec	k1gMnPc1	jezdec
využívali	využívat	k5eAaImAgMnP	využívat
rychlosti	rychlost	k1gFnPc4	rychlost
vozu	vůz	k1gInSc2	vůz
a	a	k8xC	a
dokázali	dokázat	k5eAaPmAgMnP	dokázat
bodovat	bodovat	k5eAaImF	bodovat
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
McLaren	McLarno	k1gNnPc2	McLarno
nestanul	stanout	k5eNaPmAgInS	stanout
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technická	technický	k2eAgNnPc1d1	technické
data	datum	k1gNnPc1	datum
==	==	k?	==
</s>
</p>
<p>
<s>
Motor	motor	k1gInSc1	motor
<g/>
:	:	kIx,	:
Mercedes	mercedes	k1gInSc1	mercedes
FO110J	FO110J	k1gFnSc2	FO110J
</s>
</p>
<p>
<s>
V10	V10	k4	V10
72	[number]	k4	72
<g/>
°	°	k?	°
</s>
</p>
<p>
<s>
Objem	objem	k1gInSc1	objem
<g/>
:	:	kIx,	:
2990	[number]	k4	2990
cc	cc	k?	cc
</s>
</p>
<p>
<s>
Vstřikování	vstřikování	k1gNnSc1	vstřikování
<g/>
:	:	kIx,	:
TAG	tag	k1gInSc1	tag
</s>
</p>
<p>
<s>
Výkon	výkon	k1gInSc1	výkon
<g/>
:	:	kIx,	:
830	[number]	k4	830
<g/>
cv	cv	k?	cv
<g/>
/	/	kIx~	/
<g/>
18300	[number]	k4	18300
otáček	otáčka	k1gFnPc2	otáčka
</s>
</p>
<p>
<s>
Palivo	palivo	k1gNnSc1	palivo
<g/>
:	:	kIx,	:
Mobil	mobil	k1gInSc1	mobil
1	[number]	k4	1
</s>
</p>
<p>
<s>
Palivový	palivový	k2eAgInSc1d1	palivový
systém	systém	k1gInSc1	systém
<g/>
:	:	kIx,	:
TAG	tag	k1gInSc1	tag
f.	f.	k?	f.
<g/>
i.	i.	k?	i.
</s>
</p>
<p>
<s>
Převodovka	převodovka	k1gFnSc1	převodovka
<g/>
:	:	kIx,	:
McLaren	McLarna	k1gFnPc2	McLarna
L	L	kA	L
7	[number]	k4	7
stupňová	stupňový	k2eAgFnSc1d1	stupňová
poloautomatická	poloautomatický	k2eAgFnSc1d1	poloautomatická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pneumatiky	pneumatika	k1gFnPc1	pneumatika
<g/>
:	:	kIx,	:
Bridgestone	Bridgeston	k1gInSc5	Bridgeston
</s>
</p>
<p>
<s>
Brzdy	brzda	k1gFnPc1	brzda
<g/>
:	:	kIx,	:
AP	ap	kA	ap
Racing	Racing	k1gInSc1	Racing
</s>
</p>
<p>
<s>
==	==	k?	==
Statistika	statistika	k1gFnSc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
17	[number]	k4	17
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
</s>
</p>
<p>
<s>
7	[number]	k4	7
vítězství	vítězství	k1gNnSc1	vítězství
(	(	kIx(	(
<g/>
Mika	Mik	k1gMnSc2	Mik
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
7	[number]	k4	7
pole	pole	k1gNnSc1	pole
positions	positionsa	k1gFnPc2	positionsa
(	(	kIx(	(
<g/>
Mika	Mik	k1gMnSc2	Mik
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
162	[number]	k4	162
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
Mika	Mik	k1gMnSc2	Mik
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
89	[number]	k4	89
+	+	kIx~	+
David	David	k1gMnSc1	David
Coulthard	Coulthard	k1gMnSc1	Coulthard
73	[number]	k4	73
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
22	[number]	k4	22
x	x	k?	x
podium	podium	k1gNnSc1	podium
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
po	po	k7c6	po
11	[number]	k4	11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejrychlejší	rychlý	k2eAgNnSc1d3	nejrychlejší
kolo	kolo	k1gNnSc1	kolo
11	[number]	k4	11
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2000	[number]	k4	2000
==	==	k?	==
</s>
</p>
<p>
<s>
tučně	tučně	k6eAd1	tučně
vyznačené	vyznačený	k2eAgInPc1d1	vyznačený
závody	závod	k1gInPc1	závod
znamenají	znamenat	k5eAaImIp3nP	znamenat
zisk	zisk	k1gInSc4	zisk
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
<g/>
,	,	kIx,	,
závody	závod	k1gInPc1	závod
vyznačené	vyznačený	k2eAgFnSc2d1	vyznačená
kurzívou	kurzíva	k1gFnSc7	kurzíva
zajetí	zajetí	k1gNnSc2	zajetí
nejrychlejšího	rychlý	k2eAgNnSc2d3	nejrychlejší
kola	kolo	k1gNnSc2	kolo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
<g/>
)	)	kIx)	)
</s>
</p>
