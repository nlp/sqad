<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2010	[number]	k4	2010
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
(	(	kIx(	(
<g/>
FAO	FAO	kA	FAO
<g/>
)	)	kIx)	)
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mor	mor	k1gInSc1	mor
skotu	skot	k1gInSc2	skot
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
zcela	zcela	k6eAd1	zcela
vymýcen	vymýcen	k2eAgInSc1d1	vymýcen
<g/>
;	;	kIx,	;
Světová	světový	k2eAgFnSc1d1	světová
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
zvířat	zvíře	k1gNnPc2	zvíře
vydala	vydat	k5eAaPmAgFnS	vydat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
potvrzení	potvrzení	k1gNnSc4	potvrzení
o	o	k7c6	o
úplném	úplný	k2eAgNnSc6d1	úplné
vymýcení	vymýcení	k1gNnSc6	vymýcení
nemoci	nemoc	k1gFnSc2	nemoc
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
