<s>
Analfabetismus	analfabetismus	k1gInSc1	analfabetismus
neboli	neboli	k8xC	neboli
negramotnost	negramotnost	k1gFnSc1	negramotnost
je	být	k5eAaImIp3nS	být
neschopnost	neschopnost	k1gFnSc4	neschopnost
číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
opakem	opak	k1gInSc7	opak
je	být	k5eAaImIp3nS	být
gramotnost	gramotnost	k1gFnSc1	gramotnost
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neumí	umět	k5eNaImIp3nS	umět
číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
negramotný	gramotný	k2eNgMnSc1d1	negramotný
neboli	neboli	k8xC	neboli
analfabet	analfabet	k1gMnSc1	analfabet
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
poměr	poměr	k1gInSc1	poměr
gramotné	gramotný	k2eAgFnSc2d1	gramotná
a	a	k8xC	a
negramotné	gramotný	k2eNgFnSc2d1	negramotná
části	část	k1gFnSc2	část
populace	populace	k1gFnSc2	populace
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
ukazatelů	ukazatel	k1gMnPc2	ukazatel
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
určité	určitý	k2eAgFnSc2d1	určitá
skupiny	skupina	k1gFnSc2	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
procento	procento	k1gNnSc1	procento
negramotných	gramotný	k2eNgInPc2d1	negramotný
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgFnSc3d1	velká
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
problém	problém	k1gInSc4	problém
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
pracovní	pracovní	k2eAgInSc1d1	pracovní
trh	trh	k1gInSc1	trh
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc4d1	plný
nekvalifikované	kvalifikovaný	k2eNgInPc4d1	nekvalifikovaný
pracovní	pracovní	k2eAgInPc4d1	pracovní
síly	síl	k1gInPc4	síl
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnPc1	vláda
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
bojovat	bojovat	k5eAaImF	bojovat
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
povinnou	povinný	k2eAgFnSc7d1	povinná
školní	školní	k2eAgFnSc7d1	školní
docházkou	docházka	k1gFnSc7	docházka
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
vzdělávacími	vzdělávací	k2eAgInPc7d1	vzdělávací
projekty	projekt	k1gInPc7	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Kampaním	kampaň	k1gFnPc3	kampaň
proti	proti	k7c3	proti
negramotnosti	negramotnost	k1gFnSc3	negramotnost
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
alfabetizace	alfabetizace	k1gFnSc1	alfabetizace
<g/>
.	.	kIx.	.
</s>
<s>
Negramotnost	negramotnost	k1gFnSc1	negramotnost
byla	být	k5eAaImAgFnS	být
i	i	k9	i
v	v	k7c6	v
dnes	dnes	k6eAd1	dnes
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
až	až	k9	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lidé	člověk	k1gMnPc1	člověk
číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
nepotřebovali	potřebovat	k5eNaImAgMnP	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
moderní	moderní	k2eAgFnSc6d1	moderní
společnosti	společnost	k1gFnSc6	společnost
ji	on	k3xPp3gFnSc4	on
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
skutečně	skutečně	k6eAd1	skutečně
podstatně	podstatně	k6eAd1	podstatně
snížila	snížit	k5eAaPmAgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
ovšem	ovšem	k9	ovšem
opět	opět	k6eAd1	opět
vzniká	vznikat	k5eAaImIp3nS	vznikat
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
sekundární	sekundární	k2eAgFnSc1d1	sekundární
negramotnost	negramotnost	k1gFnSc1	negramotnost
<g/>
,	,	kIx,	,
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
obrazová	obrazový	k2eAgFnSc1d1	obrazová
a	a	k8xC	a
hlasová	hlasový	k2eAgFnSc1d1	hlasová
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
především	především	k9	především
zásluhou	zásluha	k1gFnSc7	zásluha
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
pojem	pojem	k1gInSc1	pojem
funkční	funkční	k2eAgFnSc4d1	funkční
negramotnost	negramotnost	k1gFnSc4	negramotnost
<g/>
.	.	kIx.	.
</s>
<s>
Funkčně	funkčně	k6eAd1	funkčně
negramotná	gramotný	k2eNgFnSc1d1	negramotná
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
umět	umět	k5eAaImF	umět
číst	číst	k5eAaImF	číst
(	(	kIx(	(
<g/>
např.	např.	kA	např.
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
<g/>
)	)	kIx)	)
a	a	k8xC	a
psát	psát	k5eAaImF	psát
(	(	kIx(	(
<g/>
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
věty	věta	k1gFnPc4	věta
a	a	k8xC	a
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
slovní	slovní	k2eAgFnSc7d1	slovní
zásobou	zásoba	k1gFnSc7	zásoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
číst	číst	k5eAaImF	číst
nebo	nebo	k8xC	nebo
psát	psát	k5eAaImF	psát
v	v	k7c6	v
dostatečné	dostatečný	k2eAgFnSc6d1	dostatečná
míře	míra	k1gFnSc6	míra
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyrovnala	vyrovnat	k5eAaBmAgFnS	vyrovnat
s	s	k7c7	s
každodenními	každodenní	k2eAgInPc7d1	každodenní
požadavky	požadavek	k1gInPc7	požadavek
života	život	k1gInSc2	život
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
např.	např.	kA	např.
neschopnost	neschopnost	k1gFnSc1	neschopnost
využívat	využívat	k5eAaImF	využívat
informací	informace	k1gFnSc7	informace
z	z	k7c2	z
běžných	běžný	k2eAgInPc2d1	běžný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jízdních	jízdní	k2eAgInPc2d1	jízdní
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
statistik	statistika	k1gFnPc2	statistika
<g/>
,	,	kIx,	,
právních	právní	k2eAgInPc2d1	právní
dokumentů	dokument	k1gInPc2	dokument
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgFnSc1d1	funkční
negramotnost	negramotnost	k1gFnSc1	negramotnost
tedy	tedy	k9	tedy
souvisí	souviset	k5eAaImIp3nS	souviset
nejenom	nejenom	k6eAd1	nejenom
s	s	k7c7	s
faktickou	faktický	k2eAgFnSc7d1	faktická
schopností	schopnost	k1gFnSc7	schopnost
číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
porozumět	porozumět	k5eAaPmF	porozumět
textu	text	k1gInSc3	text
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
orientovat	orientovat	k5eAaBmF	orientovat
se	se	k3xPyFc4	se
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
konvencích	konvence	k1gFnPc6	konvence
psaného	psaný	k2eAgInSc2d1	psaný
projevu	projev	k1gInSc2	projev
či	či	k8xC	či
specifické	specifický	k2eAgFnSc2d1	specifická
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
funkční	funkční	k2eAgFnSc1d1	funkční
negramotnost	negramotnost	k1gFnSc1	negramotnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
překážkou	překážka	k1gFnSc7	překážka
pro	pro	k7c4	pro
pracovní	pracovní	k2eAgNnSc4d1	pracovní
a	a	k8xC	a
společenské	společenský	k2eAgNnSc4d1	společenské
uplatnění	uplatnění	k1gNnSc4	uplatnění
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
a	a	k8xC	a
sleduje	sledovat	k5eAaImIp3nS	sledovat
<g/>
.	.	kIx.	.
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Analfabeta	analfabet	k1gMnSc2	analfabet
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
2	[number]	k4	2
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
235	[number]	k4	235
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Negramotnost	negramotnost	k1gFnSc1	negramotnost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
<g/>
Čo	Čo	k1gMnSc1	Čo
dnes	dnes	k6eAd1	dnes
znamenajú	znamenajú	k?	znamenajú
pojmy	pojem	k1gInPc1	pojem
gramotnosť	gramotnostit	k5eAaPmRp2nS	gramotnostit
a	a	k8xC	a
negramotnosť	gramotnostit	k5eNaPmRp2nS	gramotnostit
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Gramotnost	gramotnost	k1gFnSc1	gramotnost
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
podle	podle	k7c2	podle
CIA	CIA	kA	CIA
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Statistiky	statistik	k1gMnPc7	statistik
negramotnosti	negramotnost	k1gFnSc2	negramotnost
UNESCO	Unesco	k1gNnSc1	Unesco
</s>
