<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
část	část	k1gFnSc1	část
přímky	přímka	k1gFnSc2	přímka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
rozdělením	rozdělení	k1gNnSc7	rozdělení
přímky	přímka	k1gFnSc2	přímka
jedním	jeden	k4xCgInSc7	jeden
bodem	bod	k1gInSc7	bod
<g/>
?	?	kIx.	?
</s>
