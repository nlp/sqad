<s>
Polopřímka	polopřímka	k1gFnSc1	polopřímka
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
přímky	přímka	k1gFnSc2	přímka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
rozdělením	rozdělení	k1gNnSc7	rozdělení
přímky	přímka	k1gFnSc2	přímka
jedním	jeden	k4xCgInSc7	jeden
jejím	její	k3xOp3gInSc7	její
bodem	bod	k1gInSc7	bod
<g/>
.	.	kIx.	.
</s>
