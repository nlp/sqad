<s>
Polopřímka	polopřímka	k1gFnSc1	polopřímka
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
přímky	přímka	k1gFnSc2	přímka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
rozdělením	rozdělení	k1gNnSc7	rozdělení
přímky	přímka	k1gFnSc2	přímka
jedním	jeden	k4xCgInSc7	jeden
jejím	její	k3xOp3gInSc7	její
bodem	bod	k1gInSc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
přímku	přímka	k1gFnSc4	přímka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
počáteční	počáteční	k2eAgInSc1d1	počáteční
bod	bod	k1gInSc1	bod
polopřímky	polopřímka	k1gFnSc2	polopřímka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
polopřímky	polopřímka	k1gFnSc2	polopřímka
se	se	k3xPyFc4	se
na	na	k7c6	na
polopřímce	polopřímka	k1gFnSc6	polopřímka
volí	volit	k5eAaImIp3nP	volit
další	další	k2eAgInSc4d1	další
bod	bod	k1gInSc4	bod
různý	různý	k2eAgInSc4d1	různý
od	od	k7c2	od
počátečního	počáteční	k2eAgInSc2d1	počáteční
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
bod	bod	k1gInSc1	bod
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pomocný	pomocný	k2eAgInSc1d1	pomocný
bod	bod	k1gInSc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Polopřímka	polopřímka	k1gFnSc1	polopřímka
se	se	k3xPyFc4	se
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
rovnou	rovnou	k6eAd1	rovnou
čarou	čára	k1gFnSc7	čára
jdoucí	jdoucí	k2eAgFnSc7d1	jdoucí
od	od	k7c2	od
počátečního	počáteční	k2eAgInSc2d1	počáteční
bodu	bod	k1gInSc2	bod
přes	přes	k7c4	přes
pomocný	pomocný	k2eAgInSc4d1	pomocný
bod	bod	k1gInSc4	bod
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
počátečního	počáteční	k2eAgInSc2d1	počáteční
a	a	k8xC	a
pomocného	pomocný	k2eAgInSc2d1	pomocný
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overrightarrow	overrightarrow	k?	overrightarrow
{	{	kIx(	{
<g/>
AB	AB	kA	AB
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Opačná	opačný	k2eAgFnSc1d1	opačná
polopřímka	polopřímka	k1gFnSc1	polopřímka
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
polopřímce	polopřímka	k1gFnSc3	polopřímka
je	být	k5eAaImIp3nS	být
polopřímka	polopřímka	k1gFnSc1	polopřímka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
přímce	přímka	k1gFnSc6	přímka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
danou	daný	k2eAgFnSc7d1	daná
polopřímkou	polopřímka	k1gFnSc7	polopřímka
stejný	stejný	k2eAgInSc1d1	stejný
počáteční	počáteční	k2eAgInSc1d1	počáteční
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opačný	opačný	k2eAgInSc1d1	opačný
směr	směr	k1gInSc1	směr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
směru	směr	k1gInSc2	směr
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
šipka	šipka	k1gFnSc1	šipka
směřující	směřující	k2eAgInSc4d1	směřující
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
C	C	kA	C
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overrightarrow	overrightarrow	k?	overrightarrow
{	{	kIx(	{
<g/>
AC	AC	kA	AC
<g/>
}}}	}}}	k?	}}}
je	být	k5eAaImIp3nS	být
opačná	opačný	k2eAgFnSc1d1	opačná
polopřímka	polopřímka	k1gFnSc1	polopřímka
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overrightarrow	overrightarrow	k?	overrightarrow
{	{	kIx(	{
<g/>
AB	AB	kA	AB
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Sjednocením	sjednocení	k1gNnSc7	sjednocení
polopřímky	polopřímka	k1gFnSc2	polopřímka
a	a	k8xC	a
k	k	k7c3	k
ní	on	k3xPp3gFnSc2	on
opačné	opačný	k2eAgFnSc2d1	opačná
polopřímky	polopřímka	k1gFnSc2	polopřímka
je	být	k5eAaImIp3nS	být
přímka	přímka	k1gFnSc1	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
na	na	k7c6	na
přímce	přímka	k1gFnSc6	přímka
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
,	,	kIx,	,
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
průnikem	průnik	k1gInSc7	průnik
polopřímek	polopřímka	k1gFnPc2	polopřímka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
overrightarrow	overrightarrow	k?	overrightarrow
{	{	kIx(	{
<g/>
AB	AB	kA	AB
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
A	A	kA	A
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overrightarrow	overrightarrow	k?	overrightarrow
{	{	kIx(	{
<g/>
BA	ba	k9	ba
<g/>
}}}	}}}	k?	}}}
je	být	k5eAaImIp3nS	být
úsečka	úsečka	k1gFnSc1	úsečka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
AB	AB	kA	AB
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Přímka	přímka	k1gFnSc1	přímka
Lineární	lineární	k2eAgMnSc1d1	lineární
geometrické	geometrický	k2eAgInPc4d1	geometrický
útvary	útvar	k1gInPc4	útvar
</s>
