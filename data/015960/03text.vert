<s>
Husova	Husův	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
</s>
<s>
Husova	Husův	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
Husova	Husův	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
v	v	k7c6
létě	léto	k1gNnSc6
s	s	k7c7
vrcholem	vrchol	k1gInSc7
Sněžky	Sněžka	k1gFnSc2
v	v	k7c6
pozadí	pozadí	k1gNnSc6
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1830	#num#	k4
/	/	kIx~
1930	#num#	k4
Další	další	k2eAgMnPc1d1
majitelé	majitel	k1gMnPc1
</s>
<s>
ČSR	ČSR	kA
<g/>
,	,	kIx,
ROH	roh	k1gInSc1
<g/>
,	,	kIx,
MSDU	MSDU	kA
OS	OS	kA
Současný	současný	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
</s>
<s>
HOKOZ	HOKOZ	kA
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
<g/>
o.	o.	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pojmenováno	pojmenovat	k5eAaPmNgNnS
po	po	k7c6
</s>
<s>
Jan	Jan	k1gMnSc1
Hus	Hus	k1gMnSc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Pec	Pec	k1gFnSc1
pod	pod	k7c7
Sněžkou	Sněžka	k1gFnSc7
234	#num#	k4
<g/>
,	,	kIx,
Pec	Pec	k1gFnSc1
pod	pod	k7c7
Sněžkou	Sněžka	k1gFnSc7
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Pohoří	pohoří	k1gNnSc1
</s>
<s>
Krkonoše	Krkonoše	k1gFnPc4
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
1065	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
25,68	25,68	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
36,48	36,48	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc4
</s>
<s>
http://www.husova-bouda.cz/husova-bouda/	http://www.husova-bouda.cz/husova-bouda/	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Husova	Husův	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
též	též	k9
Husovka	husovka	k1gFnSc1
(	(	kIx(
<g/>
pův	pův	k?
<g/>
.	.	kIx.
německy	německy	k6eAd1
Koppenblickbaude	Koppenblickbaud	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
horská	horský	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
ubytovací	ubytovací	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stojí	stát	k5eAaImIp3nS
v	v	k7c6
Krkonoších	Krkonoše	k1gFnPc6
nad	nad	k7c7
Pecí	Pec	k1gFnSc7
pod	pod	k7c7
Sněžkou	Sněžka	k1gFnSc7
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
1065	#num#	k4
m.	m.	k?
Je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c4
rozhraní	rozhraní	k1gNnSc4
Lučin	lučina	k1gFnPc2
a	a	k8xC
Vysokého	vysoký	k2eAgInSc2d1
svahu	svah	k1gInSc2
několik	několik	k4yIc4
metrů	metr	k1gInPc2
od	od	k7c2
jednoho	jeden	k4xCgInSc2
z	z	k7c2
nejstarších	starý	k2eAgInPc2d3
krkonošských	krkonošský	k2eAgInPc2d1
lyžařských	lyžařský	k2eAgInPc2d1
vleků	vlek	k1gInPc2
Na	na	k7c6
Muldě	mulda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Původní	původní	k2eAgFnSc1d1
horská	horský	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
Josefem	Josef	k1gMnSc7
Bönschem	Bönsch	k1gMnSc7
okolo	okolo	k7c2
roku	rok	k1gInSc2
1830	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
se	se	k3xPyFc4
její	její	k3xOp3gMnSc1
tehdejší	tehdejší	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
Johann	Johann	k1gMnSc1
Ettrich	Ettrich	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
dům	dům	k1gInSc4
přestavět	přestavět	k5eAaPmF
na	na	k7c4
ubytovnu	ubytovna	k1gFnSc4
pro	pro	k7c4
hosty	host	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
tak	tak	k6eAd1
na	na	k7c6
místě	místo	k1gNnSc6
původního	původní	k2eAgInSc2d1
domu	dům	k1gInSc2
vyrostl	vyrůst	k5eAaPmAgInS
třípatrový	třípatrový	k2eAgInSc1d1
penzion	penzion	k1gInSc1
vyprojektovaný	vyprojektovaný	k2eAgInSc1d1
firmou	firma	k1gFnSc7
Fischer	Fischer	k1gMnSc1
&	&	k?
Hollmann	Hollmann	k1gMnSc1
ze	z	k7c2
Svobody	svoboda	k1gFnSc2
nad	nad	k7c7
Úpou	Úpa	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
pojmenoval	pojmenovat	k5eAaPmAgMnS
Koppenblickbaude	Koppenblickbaud	k1gInSc5
–	–	k?
bouda	bouda	k1gFnSc1
s	s	k7c7
vyhlídkou	vyhlídka	k1gFnSc7
na	na	k7c4
Sněžku	Sněžka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
byla	být	k5eAaImAgFnS
budova	budova	k1gFnSc1
zabavena	zabavit	k5eAaPmNgFnS
československým	československý	k2eAgInSc7d1
státem	stát	k1gInSc7
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
po	po	k7c6
Janu	Jan	k1gMnSc6
Husovi	Hus	k1gMnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
byla	být	k5eAaImAgFnS
svěřena	svěřit	k5eAaPmNgFnS
odborům	odbor	k1gInPc3
k	k	k7c3
rekreaci	rekreace	k1gFnSc3
pracujících	pracující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
několik	několik	k4yIc4
dílčích	dílčí	k2eAgFnPc2d1
přestaveb	přestavba	k1gFnPc2
se	se	k3xPyFc4
zachovala	zachovat	k5eAaPmAgFnS
v	v	k7c6
podstatě	podstata	k1gFnSc6
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
,	,	kIx,
jakou	jaký	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
měla	mít	k5eAaImAgFnS
v	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dostupnost	dostupnost	k1gFnSc1
</s>
<s>
Motorovým	motorový	k2eAgNnSc7d1
vozidlem	vozidlo	k1gNnSc7
je	být	k5eAaImIp3nS
dostupný	dostupný	k2eAgInSc1d1
po	po	k7c6
asfaltové	asfaltový	k2eAgFnSc6d1
silnici	silnice	k1gFnSc6
z	z	k7c2
Pece	Pec	k1gFnSc2
pod	pod	k7c7
Sněžkou	Sněžka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
pokračuje	pokračovat	k5eAaImIp3nS
do	do	k7c2
osady	osada	k1gFnSc2
Lučiny	lučina	k1gFnSc2
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
jako	jako	k9
cyklotrasa	cyklotrasa	k1gFnSc1
K	K	kA
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pěší	pěší	k2eAgInSc1d1
přístup	přístup	k1gInSc1
je	být	k5eAaImIp3nS
možný	možný	k2eAgInSc1d1
po	po	k7c6
turistických	turistický	k2eAgFnPc6d1
trasách	trasa	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
po	po	k7c6
modré	modrý	k2eAgFnSc6d1
turistické	turistický	k2eAgFnSc6d1
značce	značka	k1gFnSc6
z	z	k7c2
Pece	Pec	k1gFnSc2
pod	pod	k7c7
Sněžkou	Sněžka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
částečně	částečně	k6eAd1
kopíruje	kopírovat	k5eAaImIp3nS
výše	vysoce	k6eAd2
zmíněnou	zmíněný	k2eAgFnSc4d1
silnici	silnice	k1gFnSc4
a	a	k8xC
cyklotrasu	cyklotrasa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
po	po	k7c6
modré	modrý	k2eAgFnSc6d1
turistické	turistický	k2eAgFnSc6d1
značce	značka	k1gFnSc6
od	od	k7c2
Hrnčířských	hrnčířský	k2eAgFnPc2d1
Bud	bouda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
po	po	k7c6
žluté	žlutý	k2eAgFnSc6d1
turistické	turistický	k2eAgFnSc6d1
značce	značka	k1gFnSc6
od	od	k7c2
Kolínské	kolínský	k2eAgFnSc2d1
boudy	bouda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Husova	Husův	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
v	v	k7c6
zimě	zima	k1gFnSc6
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
Sněžku	Sněžka	k1gFnSc4
ze	z	k7c2
svahu	svah	k1gInSc2
Na	na	k7c6
Muldě	mulda	k1gFnSc6
</s>
<s>
↑	↑	k?
Zápis	zápis	k1gInSc1
v	v	k7c6
katastru	katastr	k1gInSc6
nemovitostí	nemovitost	k1gFnPc2
<g/>
↑	↑	k?
http://www.veselyvylet.cz/cz/pdf/veselyvylet_22_cz.pdf	http://www.veselyvylet.cz/cz/pdf/veselyvylet_22_cz.pdf	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Husova	Husův	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Web	web	k1gInSc1
Husovy	Husův	k2eAgFnSc2d1
boudy	bouda	k1gFnSc2
</s>
<s>
Webkamera	Webkamera	k1gFnSc1
na	na	k7c6
Husově	Husův	k2eAgFnSc6d1
boudě	bouda	k1gFnSc6
(	(	kIx(
<g/>
chata	chata	k1gFnSc1
Na	na	k7c6
Muldě	mulda	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
