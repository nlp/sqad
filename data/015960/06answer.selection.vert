<s desamb="1">
Stojí	stát	k5eAaImIp3nS
v	v	k7c6
Krkonoších	Krkonoše	k1gFnPc6
nad	nad	k7c7
Pecí	Pec	k1gFnSc7
pod	pod	k7c7
Sněžkou	Sněžka	k1gFnSc7
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
1065	#num#	k4
m.	m.	k?
Je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c4
rozhraní	rozhraní	k1gNnSc4
Lučin	lučina	k1gFnPc2
a	a	k8xC
Vysokého	vysoký	k2eAgInSc2d1
svahu	svah	k1gInSc2
několik	několik	k4yIc4
metrů	metr	k1gInPc2
od	od	k7c2
jednoho	jeden	k4xCgInSc2
z	z	k7c2
nejstarších	starý	k2eAgInPc2d3
krkonošských	krkonošský	k2eAgInPc2d1
lyžařských	lyžařský	k2eAgInPc2d1
vleků	vlek	k1gInPc2
Na	na	k7c6
Muldě	mulda	k1gFnSc6
<g/>
.	.	kIx.
</s>