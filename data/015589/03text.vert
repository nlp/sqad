<s>
Jiří	Jiří	k1gMnSc1
Weigl	Weigl	k1gMnSc1
</s>
<s>
PhDr.	PhDr.	kA
Ing.	ing.	kA
Jiří	Jiří	k1gMnSc1
Weigl	Weigl	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
Jiří	Jiří	k1gMnSc1
Weigl	Weigl	k1gMnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
vedoucí	vedoucí	k2eAgFnSc2d1
Kanceláře	kancelář	k1gFnSc2
prezidenta	prezident	k1gMnSc2
republiky	republika	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2003	#num#	k4
–	–	k?
březen	březen	k1gInSc4
2013	#num#	k4
Prezident	prezident	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Ivo	Ivo	k1gMnSc1
Mathé	Mathý	k2eAgFnSc2d1
Nástupce	nástupce	k1gMnSc2
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
Mynář	Mynář	k1gMnSc1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
KSČ	KSČ	kA
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
)	)	kIx)
nestraník	nestraník	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1958	#num#	k4
(	(	kIx(
<g/>
63	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Strakonice	Strakonice	k1gFnPc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
česká	český	k2eAgFnSc1d1
Děti	dítě	k1gFnPc1
</s>
<s>
Magdaléna	Magdaléna	k1gFnSc1
Weiglová	Weiglová	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
UK	UK	kA
Profese	profese	k1gFnSc1
</s>
<s>
ekonom	ekonom	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
Commons	Commonsa	k1gFnPc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
Weigl	Weigl	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
Weigl	Weigl	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1958	#num#	k4
Strakonice	Strakonice	k1gFnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
2003	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
vedoucí	vedoucí	k1gFnSc1
Kanceláře	kancelář	k1gFnSc2
prezidenta	prezident	k1gMnSc2
republiky	republika	k1gFnSc2
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
<g/>
,	,	kIx,
od	od	k7c2
ledna	leden	k1gInSc2
2009	#num#	k4
člen	člen	k1gMnSc1
Národní	národní	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
rady	rada	k1gFnSc2
vlády	vláda	k1gFnSc2
(	(	kIx(
<g/>
NERV	nerv	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Studium	studium	k1gNnSc1
a	a	k8xC
profesní	profesní	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
vystudoval	vystudovat	k5eAaPmAgMnS
ekonomiku	ekonomika	k1gFnSc4
zahraničního	zahraniční	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
ekonomické	ekonomický	k2eAgFnSc6d1
a	a	k8xC
poté	poté	k6eAd1
studoval	studovat	k5eAaImAgMnS
arabistiku	arabistika	k1gFnSc4
a	a	k8xC
orientalistiku	orientalistika	k1gFnSc4
na	na	k7c6
Filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
UK	UK	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1984	#num#	k4
až	až	k9
1989	#num#	k4
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1986	#num#	k4
až	až	k9
1990	#num#	k4
byl	být	k5eAaImAgInS
zaměstnán	zaměstnat	k5eAaPmNgInS
v	v	k7c6
Ekonomickém	ekonomický	k2eAgInSc6d1
ústavu	ústav	k1gInSc6
ČSAV	ČSAV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1990	#num#	k4
až	až	k9
1992	#num#	k4
byl	být	k5eAaImAgInS
poradcem	poradce	k1gMnSc7
ministra	ministr	k1gMnSc2
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
na	na	k7c6
ministerstvu	ministerstvo	k1gNnSc6
financí	finance	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1992	#num#	k4
si	se	k3xPyFc3
jej	on	k3xPp3gMnSc4
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
přivedl	přivést	k5eAaPmAgMnS
na	na	k7c4
Úřad	úřad	k1gInSc4
vlády	vláda	k1gFnSc2
ČR	ČR	kA
jako	jako	k8xS,k8xC
vedoucího	vedoucí	k2eAgInSc2d1
sboru	sbor	k1gInSc2
poradců	poradce	k1gMnPc2
předsedy	předseda	k1gMnSc2
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
působil	působit	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1999	#num#	k4
až	až	k9
2000	#num#	k4
byl	být	k5eAaImAgInS
vrchním	vrchní	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
Investiční	investiční	k2eAgFnSc2d1
a	a	k8xC
poštovní	poštovní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2000	#num#	k4
až	až	k9
2003	#num#	k4
výkonným	výkonný	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
Centra	centrum	k1gNnSc2
pro	pro	k7c4
ekonomiku	ekonomika	k1gFnSc4
a	a	k8xC
politiku	politika	k1gFnSc4
(	(	kIx(
<g/>
CEP	cep	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
2003	#num#	k4
až	až	k9
2013	#num#	k4
působil	působit	k5eAaImAgInS
jako	jako	k9
vedoucí	vedoucí	k1gMnSc1
Kanceláře	kancelář	k1gFnSc2
prezidenta	prezident	k1gMnSc2
republiky	republika	k1gFnSc2
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezidentský	prezidentský	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
Miloš	Miloš	k1gMnSc1
Zeman	Zeman	k1gMnSc1
dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Weigla	Weigla	k1gMnSc1
měl	mít	k5eAaImAgMnS
ve	v	k7c6
funkci	funkce	k1gFnSc6
nahradit	nahradit	k5eAaPmF
dosavadní	dosavadní	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
SPOZ	SPOZ	kA
Vratislav	Vratislav	k1gMnSc1
Mynář	Mynář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeman	Zeman	k1gMnSc1
nabídl	nabídnout	k5eAaPmAgMnS
Jiřímu	Jiří	k1gMnSc3
Weiglovi	Weigl	k1gMnSc3
setrvání	setrvání	k1gNnSc4
ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
však	však	k9
odmítl	odmítnout	k5eAaPmAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
odejde	odejít	k5eAaPmIp3nS
pracovat	pracovat	k5eAaImF
do	do	k7c2
Institutu	institut	k1gInSc2
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
Schůzka	schůzka	k1gFnSc1
s	s	k7c7
Miroslavem	Miroslav	k1gMnSc7
Šloufem	Šlouf	k1gMnSc7
</s>
<s>
Pražský	pražský	k2eAgInSc1d1
Hotel	hotel	k1gInSc1
Savoy	Savoa	k1gFnSc2
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2008	#num#	k4
se	se	k3xPyFc4
Jiří	Jiří	k1gMnSc1
Weigl	Weigl	k1gMnSc1
sešel	sejít	k5eAaPmAgMnS
v	v	k7c6
pražském	pražský	k2eAgInSc6d1
hotelu	hotel	k1gInSc6
Savoy	Savoa	k1gFnSc2
s	s	k7c7
lobbistou	lobbista	k1gMnSc7
Miroslavem	Miroslav	k1gMnSc7
Šloufem	Šlouf	k1gMnSc7
(	(	kIx(
<g/>
ČSSD	ČSSD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Server	server	k1gInSc1
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
pak	pak	k6eAd1
část	část	k1gFnSc4
záznamu	záznam	k1gInSc2
schůzky	schůzka	k1gFnSc2
zveřejnil	zveřejnit	k5eAaPmAgInS
čtyři	čtyři	k4xCgInPc4
dny	den	k1gInPc4
před	před	k7c7
volbou	volba	k1gFnSc7
prezidenta	prezident	k1gMnSc2
a	a	k8xC
únik	únik	k1gInSc1
tohoto	tento	k3xDgInSc2
videozáznamu	videozáznam	k1gInSc2
následně	následně	k6eAd1
vyšetřovala	vyšetřovat	k5eAaImAgFnS
česká	český	k2eAgFnSc1d1
rozvědka	rozvědka	k1gFnSc1
a	a	k8xC
inspekce	inspekce	k1gFnSc2
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zveřejněné	zveřejněný	k2eAgInPc1d1
záběry	záběr	k1gInPc1
totiž	totiž	k9
ovlivnily	ovlivnit	k5eAaPmAgInP
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
volbu	volba	k1gFnSc4
prezidenta	prezident	k1gMnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
změnila	změnit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
názor	názor	k1gInSc4
a	a	k8xC
trvala	trvat	k5eAaImAgFnS
na	na	k7c6
veřejné	veřejný	k2eAgFnSc6d1
volbě	volba	k1gFnSc6
hlavy	hlava	k1gFnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlouf	Šlouf	k1gInSc1
je	být	k5eAaImIp3nS
totiž	totiž	k9
vnímán	vnímat	k5eAaImNgInS
jako	jako	k8xS,k8xC
velice	velice	k6eAd1
kontroverzní	kontroverzní	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
přiznal	přiznat	k5eAaPmAgMnS
ke	k	k7c3
kontaktům	kontakt	k1gInPc3
se	se	k3xPyFc4
zavražděným	zavražděný	k2eAgMnSc7d1
podnikatelem	podnikatel	k1gMnSc7
Františkem	František	k1gMnSc7
Mrázkem	Mrázek	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
vůdčí	vůdčí	k2eAgFnSc4d1
osobnost	osobnost	k1gFnSc4
organizovaného	organizovaný	k2eAgInSc2d1
zločinu	zločin	k1gInSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Doporučující	doporučující	k2eAgInSc1d1
dopis	dopis	k1gInSc1
pro	pro	k7c4
Viktora	Viktor	k1gMnSc4
Koženého	Kožený	k1gMnSc2
</s>
<s>
Týdeník	týdeník	k1gInSc1
Respekt	respekt	k1gInSc1
v	v	k7c6
únoru	únor	k1gInSc6
2009	#num#	k4
zveřejnil	zveřejnit	k5eAaPmAgInS
Weiglův	Weiglův	k2eAgInSc1d1
dopis	dopis	k1gInSc1
z	z	k7c2
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1997	#num#	k4
ázerbájdžánskému	ázerbájdžánský	k2eAgMnSc3d1
prezidentu	prezident	k1gMnSc3
Hejdaru	Hejdar	k1gMnSc3
Alijevovi	Alijeva	k1gMnSc3
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgMnSc6,k3yIgMnSc6,k3yRgMnSc6
doporučil	doporučit	k5eAaPmAgMnS
Viktora	Viktora	k1gMnSc1
Koženého	Kožený	k1gMnSc2
jako	jako	k8xS,k8xC
průkopníka	průkopník	k1gMnSc2
privatizačních	privatizační	k2eAgInPc2d1
procesů	proces	k1gInPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
zkušenost	zkušenost	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
užitečná	užitečný	k2eAgFnSc1d1
i	i	k9
pro	pro	k7c4
privatizační	privatizační	k2eAgInPc4d1
projekty	projekt	k1gInPc4
v	v	k7c6
Ázerbájdžánu	Ázerbájdžán	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Weigl	Weigl	k1gMnSc1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
doporučující	doporučující	k2eAgInSc4d1
dopis	dopis	k1gInSc4
na	na	k7c4
přání	přání	k1gNnSc4
tehdejšího	tehdejší	k2eAgMnSc2d1
premiéra	premiér	k1gMnSc2
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
<g/>
,	,	kIx,
když	když	k8xS
působil	působit	k5eAaImAgMnS
jako	jako	k9
vedoucí	vedoucí	k1gMnSc1
týmu	tým	k1gInSc2
jeho	jeho	k3xOp3gMnPc2
poradců	poradce	k1gMnPc2
na	na	k7c6
Úřadu	úřad	k1gInSc6
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kožený	Kožený	k1gMnSc1
přitom	přitom	k6eAd1
už	už	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
odeslání	odeslání	k1gNnSc2
dopisu	dopis	k1gInSc2
žil	žít	k5eAaImAgMnS
s	s	k7c7
irským	irský	k2eAgInSc7d1
pasem	pas	k1gInSc7
na	na	k7c6
Bahamách	Bahamy	k1gFnPc6
a	a	k8xC
byl	být	k5eAaImAgMnS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
kontroverzní	kontroverzní	k2eAgFnSc4d1
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
podezřelá	podezřelý	k2eAgFnSc1d1
z	z	k7c2
vytunelování	vytunelování	k1gNnSc2
privatizačních	privatizační	k2eAgInPc2d1
fondů	fond	k1gInPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rodinný	rodinný	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
otcem	otec	k1gMnSc7
Magdalény	Magdaléna	k1gFnSc2
Weiglové	Weiglová	k1gFnSc2
a	a	k8xC
Josefa	Josef	k1gMnSc2
Weigla	Weigl	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Premiér	premiér	k1gMnSc1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
členy	člen	k1gMnPc4
Národní	národní	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
rady	rada	k1gFnSc2
vlády	vláda	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2009-01-08	2009-01-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
;	;	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ekonomické	ekonomický	k2eAgFnSc6d1
radě	rada	k1gFnSc6
bude	být	k5eAaImBp3nS
i	i	k9
člověk	člověk	k1gMnSc1
z	z	k7c2
Hradu	hrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-01-05	2009-01-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PERKNEROVÁ	PERKNEROVÁ	kA
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
Miloš	Miloš	k1gMnSc1
Zeman	Zeman	k1gMnSc1
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
kancléřem	kancléř	k1gMnSc7
Vratislava	Vratislav	k1gMnSc2
Mynáře	Mynář	k1gMnSc2
<g/>
,	,	kIx,
šéfa	šéf	k1gMnSc2
SPOZ	SPOZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-03-10	2013-03-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Videozáznam	videozáznam	k1gInSc1
schůzky	schůzka	k1gFnSc2
Weigl	Weigla	k1gFnPc2
-	-	kIx~
Šlouf	Šlouf	k1gInSc1
vyšetřuje	vyšetřovat	k5eAaImIp3nS
rozvědka	rozvědka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-02-12	2008-02-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Dopis	dopis	k1gInSc1
Jiřího	Jiří	k1gMnSc2
Weigla	Weiglo	k1gNnSc2
Heydaru	Heydar	k1gMnSc3
Alijevovi	Alijeva	k1gMnSc3
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Kožený	Kožený	k1gMnSc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
dostal	dostat	k5eAaPmAgMnS
pochvalné	pochvalný	k2eAgNnSc4d1
doporučení	doporučení	k1gNnSc4
od	od	k7c2
poradce	poradce	k1gMnSc2
prezidenta	prezident	k1gMnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jiří	Jiří	k1gMnSc1
Weigl	Weigla	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jiří	Jiří	k1gMnSc1
Weigl	Weigl	k1gMnSc1
</s>
<s>
Dokonalý	dokonalý	k2eAgMnSc1d1
kancléř	kancléř	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
je	být	k5eAaImIp3nS
Jiří	Jiří	k1gMnSc1
Weigl	Weigl	k1gMnSc1
<g/>
,	,	kIx,
nejbližší	blízký	k2eAgMnSc1d3
muž	muž	k1gMnSc1
prezidenta	prezident	k1gMnSc2
Klause	Klaus	k1gMnSc2
<g/>
,	,	kIx,
Mf	Mf	k1gFnSc3
DNES	dnes	k6eAd1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
</s>
<s>
Prezident	prezident	k1gMnSc1
Klaus	Klaus	k1gMnSc1
<g/>
:	:	kIx,
Můj	můj	k3xOp1gMnSc1
kancléř	kancléř	k1gMnSc1
Weigl	Weigl	k1gMnSc1
<g/>
,	,	kIx,
Mf	Mf	k1gMnSc1
DNES	dnes	k6eAd1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
</s>
<s>
Weigl	Weigl	k1gInSc1
šéfoval	šéfovat	k5eAaImAgInS
firmě	firma	k1gFnSc3
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
dcera	dcera	k1gFnSc1
půjčila	půjčit	k5eAaPmAgFnS
miliony	milion	k4xCgInPc4
Grossovi	Gross	k1gMnSc6
</s>
<s>
Jiří	Jiří	k1gMnSc1
Weigl	Weigl	k1gMnSc1
-	-	kIx~
Interview	interview	k1gInSc1
BBC	BBC	kA
<g/>
,	,	kIx,
BBC	BBC	kA
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2003	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Vedoucí	vedoucí	k1gMnSc1
Kanceláře	kancelář	k1gFnSc2
prezidenta	prezident	k1gMnSc2
republiky	republika	k1gFnSc2
</s>
<s>
Přemysl	Přemysl	k1gMnSc1
Šámal	Šámal	k1gMnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jiří	Jiří	k1gMnSc1
Havelka	Havelka	k1gMnSc1
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
•	•	k?
August	August	k1gMnSc1
Adolf	Adolf	k1gMnSc1
Popelka	Popelka	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jaromír	Jaromír	k1gMnSc1
Smutný	Smutný	k1gMnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bohumil	Bohumil	k1gMnSc1
Červíček	červíček	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Novák	Novák	k1gMnSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ján	Ján	k1gMnSc1
Pudlák	Pudlák	k1gMnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Josef	Josef	k1gMnSc1
Haman	Haman	k1gMnSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
Šalda	Šalda	k1gMnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1978	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Josef	Josef	k1gMnSc1
Lžičař	Lžičař	k1gMnSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
Schwarzenberg	Schwarzenberg	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Luboš	Luboš	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ivan	Ivan	k1gMnSc1
Medek	Medek	k1gMnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ivo	Ivo	k1gMnSc1
Mathé	Mathý	k2eAgInPc4d1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jiří	Jiří	k1gMnSc1
Weigl	Weigl	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vratislav	Vratislav	k1gMnSc1
Mynář	Mynář	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
osa	osa	k1gFnSc1
<g/>
2016935307	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
170938905	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5544	#num#	k4
0274	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
91046881	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
36092145	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
91046881	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Ekonomie	ekonomie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
