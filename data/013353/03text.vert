<p>
<s>
José	José	k1gNnSc1	José
Luis	Luisa	k1gFnPc2	Luisa
Cuciuffo	Cuciuffo	k6eAd1	Cuciuffo
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
Córdoba	Córdoba	k1gFnSc1	Córdoba
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Bahía	Bahí	k2eAgFnSc1d1	Bahía
San	San	k1gFnSc1	San
Blas	Blasa	k1gFnPc2	Blasa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
argentinský	argentinský	k2eAgMnSc1d1	argentinský
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
postu	post	k1gInSc6	post
obránce	obránce	k1gMnSc2	obránce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
argentinskou	argentinský	k2eAgFnSc7d1	Argentinská
reprezentací	reprezentace	k1gFnSc7	reprezentace
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
..	..	k?	..
</s>
<s>
V	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
mužstvu	mužstvo	k1gNnSc6	mužstvo
odehrál	odehrát	k5eAaPmAgMnS	odehrát
21	[number]	k4	21
utkání	utkání	k1gNnSc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
argentinských	argentinský	k2eAgInPc2d1	argentinský
klubů	klub	k1gInPc2	klub
(	(	kIx(	(
<g/>
Chaco	Chaco	k1gNnSc4	Chaco
For	forum	k1gNnPc2	forum
Ever	Evera	k1gFnPc2	Evera
<g/>
,	,	kIx,	,
Talleres	Talleres	k1gInSc1	Talleres
de	de	k?	de
Córdoba	Córdoba	k1gFnSc1	Córdoba
<g/>
,	,	kIx,	,
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
<g/>
,	,	kIx,	,
Boca	Boca	k1gMnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
<g/>
,	,	kIx,	,
Belgrano	Belgrana	k1gFnSc5	Belgrana
de	de	k?	de
Cordoba	Cordoba	k1gFnSc1	Cordoba
<g/>
)	)	kIx)	)
hrál	hrát	k5eAaImAgInS	hrát
též	též	k9	též
tři	tři	k4xCgFnPc4	tři
sezóny	sezóna	k1gFnPc4	sezóna
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
první	první	k4xOgFnSc4	první
ligu	liga	k1gFnSc4	liga
v	v	k7c4	v
Nîmes	Nîmes	k1gInSc4	Nîmes
Olympique	Olympiqu	k1gFnSc2	Olympiqu
<g/>
.	.	kIx.	.
<g/>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
