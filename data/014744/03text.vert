<s>
Novozélandský	novozélandský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
</s>
<s>
Novozélandský	novozélandský	k2eAgMnSc1d1
dolarZemě	dolarZemě	k6eAd1
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
Nový	nový	k2eAgInSc1d1
ZélandNiue	ZélandNiue	k1gFnSc4
Niue	Niu	k1gInSc2
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
<g/>
Tokelau	Tokelaus	k1gInSc2
TokelauCookovy	TokelauCookův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
Cookovy	Cookův	k2eAgInPc4d1
ostrovyPitcairnovy	ostrovyPitcairnův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
Pitcairnovy	Pitcairnův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
NZD	NZD	kA
Inflace	inflace	k1gFnSc1
</s>
<s>
1,9	1,9	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
;	;	kIx,
pouze	pouze	k6eAd1
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
$	$	kIx~
nebo	nebo	k8xC
NZ	NZ	kA
<g/>
$	$	kIx~
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
cent	cent	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
centů	cent	k1gInPc2
<g/>
;	;	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
dolarů	dolar	k1gInPc2
Bankovky	bankovka	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
Novozélandský	novozélandský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
je	být	k5eAaImIp3nS
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
ostrovního	ostrovní	k2eAgInSc2d1
státu	stát	k1gInSc2
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc1
je	být	k5eAaImIp3nS
NZD	NZD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
setina	setina	k1gFnSc1
dolaru	dolar	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
cent	cent	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
psaném	psaný	k2eAgInSc6d1
projevu	projev	k1gInSc6
se	se	k3xPyFc4
zapisuje	zapisovat	k5eAaImIp3nS
znakem	znak	k1gInSc7
$	$	kIx~
<g/>
,	,	kIx,
nebo	nebo	k8xC
NZ	NZ	kA
<g/>
$	$	kIx~
pro	pro	k7c4
odlišení	odlišení	k1gNnSc4
od	od	k7c2
jiných	jiný	k2eAgFnPc2d1
národních	národní	k2eAgFnPc2d1
měn	měna	k1gFnPc2
se	s	k7c7
stejným	stejný	k2eAgInSc7d1
názvem	název	k1gInSc7
dolar	dolar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novozélandský	novozélandský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
byl	být	k5eAaImAgInS
uvolněn	uvolnit	k5eAaPmNgInS
do	do	k7c2
oběhu	oběh	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nahradil	nahradit	k5eAaPmAgMnS
novozélandskou	novozélandský	k2eAgFnSc4d1
libru	libra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směnný	směnný	k2eAgInSc1d1
kurz	kurz	k1gInSc1
byl	být	k5eAaImAgInS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
tzn.	tzn.	kA
1	#num#	k4
libra	libra	k1gFnSc1
se	se	k3xPyFc4
změnila	změnit	k5eAaPmAgFnS
na	na	k7c4
2	#num#	k4
dolary	dolar	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
samotného	samotný	k2eAgInSc2d1
Nového	Nového	k2eAgInSc2d1
Zélandu	Zéland	k1gInSc2
je	být	k5eAaImIp3nS
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
i	i	k9
v	v	k7c6
dalších	další	k2eAgNnPc6d1
územích	území	k1gNnPc6
spolutvořících	spolutvořící	k2eAgInPc2d1
Novozélandské	novozélandský	k2eAgNnSc4d1
království	království	k1gNnSc4
(	(	kIx(
<g/>
Niue	Niu	k1gFnSc2
<g/>
,	,	kIx,
Tokelau	Tokelaus	k1gInSc2
<g/>
,	,	kIx,
Cookovy	Cookův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
Pitcairnových	Pitcairnův	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
(	(	kIx(
<g/>
zámořské	zámořský	k2eAgNnSc4d1
území	území	k1gNnSc4
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cookovy	Cookův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
souběžně	souběžně	k6eAd1
s	s	k7c7
novozélandským	novozélandský	k2eAgInSc7d1
dolarem	dolar	k1gInSc7
používají	používat	k5eAaImIp3nP
i	i	k9
dolar	dolar	k1gInSc4
Cookových	Cookův	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
lokální	lokální	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
novozélandského	novozélandský	k2eAgInSc2d1
dolaru	dolar	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
platná	platný	k2eAgFnSc1d1
pouze	pouze	k6eAd1
na	na	k7c6
Cookových	Cookův	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
a	a	k8xC
je	být	k5eAaImIp3nS
pevně	pevně	k6eAd1
navázána	navázán	k2eAgFnSc1d1
na	na	k7c4
novozélandský	novozélandský	k2eAgInSc4d1
dolar	dolar	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
</s>
<s>
Mince	mince	k1gFnSc1
novozélandského	novozélandský	k2eAgInSc2d1
dolaru	dolar	k1gInSc2
mají	mít	k5eAaImIp3nP
hodnoty	hodnota	k1gFnSc2
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
a	a	k8xC
50	#num#	k4
centů	cent	k1gInPc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
1	#num#	k4
a	a	k8xC
2	#num#	k4
dolary	dolar	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
aversní	aversní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
mincí	mince	k1gFnPc2
všech	všecek	k3xTgFnPc2
nominálních	nominální	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
je	být	k5eAaImIp3nS
podobizna	podobizna	k1gFnSc1
Alžběty	Alžběta	k1gFnSc2
II	II	kA
<g/>
..	..	k?
Na	na	k7c4
reversní	reversní	k2eAgInPc4d1
motivy	motiv	k1gInPc4
novozélandské	novozélandský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
kultury	kultura	k1gFnSc2
a	a	k8xC
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bankovky	bankovka	k1gFnPc1
jsou	být	k5eAaImIp3nP
vydávány	vydávat	k5eAaImNgFnP,k5eAaPmNgFnP
o	o	k7c6
nominálních	nominální	k2eAgFnPc6d1
hodnotách	hodnota	k1gFnPc6
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
a	a	k8xC
100	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Dřívější	dřívější	k2eAgFnSc2d1
papírové	papírový	k2eAgFnSc2d1
bankovky	bankovka	k1gFnSc2
byly	být	k5eAaImAgInP
nahrazeny	nahradit	k5eAaPmNgInP
polymerovými	polymerový	k2eAgFnPc7d1
bankovkami	bankovka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Hodnota	hodnota	k1gFnSc1
</s>
<s>
Rozměry	rozměra	k1gFnPc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
barva	barva	k1gFnSc1
</s>
<s>
Motiv	motiv	k1gInSc1
</s>
<s>
Avers	avers	k1gInSc1
</s>
<s>
Revers	revers	k1gInSc1
</s>
<s>
$	$	kIx~
<g/>
5	#num#	k4
</s>
<s>
135	#num#	k4
mm	mm	kA
×	×	k?
66	#num#	k4
mm	mm	kA
</s>
<s>
oranžová	oranžový	k2eAgFnSc1d1
</s>
<s>
Sir	sir	k1gMnSc1
Edmund	Edmund	k1gMnSc1
Hillary	Hillara	k1gFnSc2
Aoraki	Aorak	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Mount	Mount	k1gMnSc1
Cook	Cook	k1gMnSc1
Massey	Massea	k1gFnSc2
Ferguson	Ferguson	k1gMnSc1
tractor	tractor	k1gMnSc1
</s>
<s>
tučňák	tučňák	k1gMnSc1
žlutooký	žlutooký	k2eAgMnSc1d1
scenérie	scenérie	k1gFnPc4
Campbellova	Campbellův	k2eAgNnPc1d1
ostrovu	ostrov	k1gInSc3
</s>
<s>
$	$	kIx~
<g/>
10	#num#	k4
</s>
<s>
140	#num#	k4
mm	mm	kA
×	×	k?
68	#num#	k4
mm	mm	kA
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Kate	kat	k1gMnSc5
Sheppard	Sheppard	k1gMnSc1
bílé	bílý	k2eAgFnPc4d1
květiny	květina	k1gFnPc4
Camellie	Camellie	k1gFnSc2
</s>
<s>
kachna	kachna	k1gFnSc1
měkkozobá	měkkozobat	k5eAaPmIp3nS,k5eAaImIp3nS
scenérie	scenérie	k1gFnSc1
řeky	řeka	k1gFnSc2
</s>
<s>
$	$	kIx~
<g/>
20	#num#	k4
</s>
<s>
145	#num#	k4
mm	mm	kA
×	×	k?
70	#num#	k4
mm	mm	kA
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
budova	budova	k1gFnSc1
Novozélandského	novozélandský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
</s>
<s>
ostříž	ostříž	k1gMnSc1
novozélandský	novozélandský	k2eAgMnSc1d1
vysokohorská	vysokohorský	k2eAgFnSc1d1
scenérie	scenérie	k1gFnSc1
</s>
<s>
$	$	kIx~
<g/>
50	#num#	k4
</s>
<s>
150	#num#	k4
mm	mm	kA
×	×	k?
72	#num#	k4
mm	mm	kA
</s>
<s>
purpurová	purpurový	k2eAgFnSc1d1
</s>
<s>
Sir	sir	k1gMnSc1
Apirana	Apiran	k1gMnSc2
Ngata	Ngata	k1gMnSc1
Porourangi	Porourang	k1gFnPc4
Meeting	meeting	k1gInSc4
House	house	k1gNnSc1
</s>
<s>
laločník	laločník	k1gMnSc1
šedý	šedý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Kō	Kō	k1gNnSc1
<g/>
)	)	kIx)
scenérie	scenérie	k1gFnSc1
jehličnatého	jehličnatý	k2eAgInSc2d1
lesa	les	k1gInSc2
</s>
<s>
$	$	kIx~
<g/>
100	#num#	k4
</s>
<s>
155	#num#	k4
mm	mm	kA
×	×	k?
74	#num#	k4
mm	mm	kA
</s>
<s>
červená	červený	k2eAgFnSc1d1
</s>
<s>
Ernest	Ernest	k1gMnSc1
Rutherford	Rutherford	k1gMnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
baron	baron	k1gMnSc1
Rutherford	Rutherford	k1gMnSc1
z	z	k7c2
Nelsonu	Nelson	k1gMnSc6
Medaile	medaile	k1gFnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
</s>
<s>
pištec	pištec	k1gMnSc1
žlutý	žlutý	k2eAgMnSc1d1
můra	můra	k1gFnSc1
Declana	Declana	k1gFnSc1
egregia	egregia	k1gFnSc1
pocházející	pocházející	k2eAgFnSc1d1
z	z	k7c2
Jižního	jižní	k2eAgNnSc2d1
ostrovu	ostrov	k1gInSc3
scenérie	scenérie	k1gFnPc1
pobřežního	pobřežní	k2eAgInSc2d1
lesa	les	k1gInSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Niue	Niue	k1gFnPc2
a	a	k8xC
Pitcairnovy	Pitcairnův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
vydávají	vydávat	k5eAaPmIp3nP,k5eAaImIp3nP
malé	malý	k2eAgNnSc4d1
množství	množství	k1gNnSc4
vlastních	vlastní	k2eAgFnPc2d1
dolarových	dolarový	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
-	-	kIx~
především	především	k9
pro	pro	k7c4
sběratelské	sběratelský	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dolar	dolar	k1gInSc1
Cookových	Cookův	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
nemá	mít	k5eNaImIp3nS
vlastní	vlastní	k2eAgInSc1d1
ISO	ISO	kA
kód	kód	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztah	vztah	k1gInSc1
mezi	mezi	k7c7
novozélandským	novozélandský	k2eAgInSc7d1
dolarem	dolar	k1gInSc7
a	a	k8xC
dolarem	dolar	k1gInSc7
Cookových	Cookův	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
je	být	k5eAaImIp3nS
obdobný	obdobný	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
mezi	mezi	k7c7
dánskou	dánský	k2eAgFnSc7d1
a	a	k8xC
faerskou	faerský	k2eAgFnSc7d1
korunou	koruna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Coins	Coins	k1gInSc1
in	in	k?
circulation	circulation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reserve	Reserev	k1gFnSc2
Bank	banka	k1gFnPc2
of	of	k?
New	New	k1gFnSc2
Zealand	Zealanda	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Banknotes	Banknotes	k1gInSc1
in	in	k?
circulation	circulation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reserve	Reserev	k1gFnSc2
Bank	banka	k1gFnPc2
of	of	k?
New	New	k1gFnSc2
Zealand	Zealanda	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Novozélandský	novozélandský	k2eAgInSc4d1
dolar	dolar	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
