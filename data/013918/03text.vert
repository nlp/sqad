<s>
Dobrovolný	dobrovolný	k2eAgInSc1d1
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
Mikroregion	mikroregion	k1gInSc1
Stráně	stráň	k1gFnPc1
</s>
<s>
Dobrovolný	dobrovolný	k2eAgInSc1d1
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
Mikroregion	mikroregion	k1gInSc4
StráněForma	StráněForma	k1gNnSc1
</s>
<s>
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
(	(	kIx(
<g/>
dle	dle	k7c2
§	§	k?
49	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
<g/>
128	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
obcích	obec	k1gFnPc6
<g/>
)	)	kIx)
Předseda	předseda	k1gMnSc1
</s>
<s>
Milan	Milan	k1gMnSc1
Andreovský	Andreovský	k1gMnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Hořičky	Hořička	k1gFnPc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2000	#num#	k4
Poloha	poloha	k1gFnSc1
Kraj	kraj	k7c2
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Náchod	Náchod	k1gInSc1
Kontakt	kontakt	k1gInSc1
E-mail	e-mail	k1gInSc1
</s>
<s>
obec.horicky@quick.cz	obec.horicky@quick.cz	k1gInSc1
Web	web	k1gInSc1
</s>
<s>
www.horicky.cz/strane.html	www.horicky.cz/strane.html	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Dobrovolný	dobrovolný	k2eAgInSc1d1
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
Mikroregion	mikroregion	k1gInSc1
Stráně	stráň	k1gFnPc4
je	být	k5eAaImIp3nS
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
(	(	kIx(
<g/>
dle	dle	k7c2
§	§	k?
49	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
<g/>
128	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
obcích	obec	k1gFnPc6
<g/>
)	)	kIx)
v	v	k7c6
okresu	okres	k1gInSc6
Náchod	Náchod	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
sídlem	sídlo	k1gNnSc7
jsou	být	k5eAaImIp3nP
Hořičky	Hořička	k1gFnPc4
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
koordinování	koordinování	k1gNnSc1
celkového	celkový	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
území	území	k1gNnSc2
mikroregionu	mikroregion	k1gInSc2
na	na	k7c6
základě	základ	k1gInSc6
společné	společný	k2eAgFnSc2d1
strategie	strategie	k1gFnSc2
<g/>
,	,	kIx,
přímé	přímý	k2eAgNnSc4d1
provádění	provádění	k1gNnSc4
společných	společný	k2eAgFnPc2d1
investičních	investiční	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
,	,	kIx,
společná	společný	k2eAgFnSc1d1
propagace	propagace	k1gFnSc1
mikroregionu	mikroregion	k1gInSc2
v	v	k7c6
cestovním	cestovní	k2eAgInSc6d1
ruchu	ruch	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdružuje	sdružovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
7	#num#	k4
obcí	obec	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
sdružené	sdružený	k2eAgFnPc1d1
v	v	k7c6
mikroregionu	mikroregion	k1gInSc6
</s>
<s>
Brzice	Brzice	k1gFnSc1
</s>
<s>
Hořičky	Hořička	k1gFnPc1
</s>
<s>
Chvalkovice	Chvalkovice	k1gFnSc1
</s>
<s>
Lhota	Lhota	k1gFnSc1
pod	pod	k7c7
Hořičkami	Hořička	k1gFnPc7
</s>
<s>
Litoboř	Litoboř	k1gFnSc1
</s>
<s>
Velký	velký	k2eAgInSc1d1
Třebešov	Třebešov	k1gInSc1
</s>
<s>
Vestec	Vestec	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Mikroregion	mikroregion	k1gInSc1
Dobrovolný	dobrovolný	k2eAgInSc1d1
svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
Mikroregion	mikroregion	k1gInSc4
Stráně	stráň	k1gFnPc1
na	na	k7c6
Regionálním	regionální	k2eAgInSc6d1
informačním	informační	k2eAgInSc6d1
servisu	servis	k1gInSc6
</s>
<s>
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
