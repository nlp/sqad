<s>
Control-Alt-Delete	Control-Alt-Delést	k5eAaPmIp3nS
</s>
<s>
QWERTY	QWERTY	kA
klávesnice	klávesnice	k1gFnSc1
se	s	k7c7
zvýrazněnými	zvýrazněný	k2eAgFnPc7d1
pozicemi	pozice	k1gFnPc7
kláves	klávesa	k1gFnPc2
Ctrl	Ctrl	kA
<g/>
,	,	kIx,
Alt	Alt	kA
a	a	k8xC
Del	Del	k1gFnSc1
</s>
<s>
Control-Alt-Delete	Control-Alt-Delat	k5eAaPmIp2nP
(	(	kIx(
<g/>
často	často	k6eAd1
zkracováno	zkracovat	k5eAaImNgNnS
na	na	k7c4
Ctrl	Ctrl	kA
<g/>
+	+	kIx~
<g/>
Alt	Alt	kA
<g/>
+	+	kIx~
<g/>
Del	Del	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
na	na	k7c6
počítačích	počítač	k1gInPc6
kompatibilních	kompatibilní	k2eAgFnPc2d1
s	s	k7c7
původním	původní	k2eAgNnSc7d1
IBM	IBM	kA
PC	PC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
stisknutím	stisknutí	k1gNnSc7
klávesy	klávesa	k1gFnSc2
Delete	Dele	k1gNnSc2
při	při	k7c6
zároveň	zároveň	k6eAd1
držených	držený	k2eAgFnPc6d1
klávesách	klávesa	k1gFnPc6
Alt	Alt	kA
a	a	k8xC
Control	Control	k1gInSc1
<g/>
:	:	kIx,
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
kbd	kbd	k?
<g/>
.	.	kIx.
<g/>
Sablona__Klavesa	Sablona__Klavesa	k1gFnSc1
<g/>
{	{	kIx(
<g/>
background-color	background-color	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
;	;	kIx,
<g/>
background-image	background-imag	k1gInSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
<g/>
linear-gradient	linear-gradient	k1gMnSc1
<g/>
(	(	kIx(
<g/>
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
255,255,255	255,255,255	k4
<g/>
,	,	kIx,
<g/>
.4	.4	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
0,0	0,0	k4
<g/>
,0	,0	k4
<g/>
,	,	kIx,
<g/>
.1	.1	k4
<g/>
))	))	k?
<g/>
;	;	kIx,
<g/>
border	border	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-color	border-color	k1gInSc1
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
#	#	kIx~
<g/>
DDD	DDD	kA
#	#	kIx~
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
b	b	k?
<g/>
1	#num#	k4
#	#	kIx~
<g/>
888	#num#	k4
#	#	kIx~
<g/>
CCC	CCC	kA
<g/>
;	;	kIx,
<g/>
border-radius	border-radius	k1gMnSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
.4	.4	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
text-shadow	text-shadow	k?
<g/>
:	:	kIx,
<g/>
0	#num#	k4
1	#num#	k4
<g/>
px	px	k?
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
255,255,255	255,255,255	k4
<g/>
,	,	kIx,
<g/>
.5	.5	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gMnSc1
<g/>
}	}	kIx)
<g/>
Ctrl	Ctrl	kA
<g/>
+	+	kIx~
<g/>
Alt	Alt	kA
<g/>
+	+	kIx~
<g/>
Delete	Dele	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkce	funkce	k1gFnSc1
této	tento	k3xDgFnSc2
klávesové	klávesový	k2eAgFnSc2d1
zkratky	zkratka	k1gFnSc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
kontextu	kontext	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
všeobecně	všeobecně	k6eAd1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
přerušuje	přerušovat	k5eAaImIp3nS
probíhající	probíhající	k2eAgFnSc3d1
operaci	operace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
během	během	k7c2
okamžiku	okamžik	k1gInSc2
před	před	k7c7
bootem	boot	k1gInSc7
počítače	počítač	k1gInSc2
(	(	kIx(
<g/>
před	před	k7c7
startem	start	k1gInSc7
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
v	v	k7c6
DOSu	DOSus	k1gInSc6
<g/>
,	,	kIx,
Windows	Windows	kA
3.0	3.0	k4
a	a	k8xC
brzké	brzký	k2eAgFnPc1d1
verze	verze	k1gFnPc1
Windowsu	Windows	k1gInSc2
nebo	nebo	k8xC
OS	OS	kA
<g/>
/	/	kIx~
<g/>
2	#num#	k4
tato	tento	k3xDgFnSc1
klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
rebootuje	rebootovat	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
počítač	počítač	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Windows	Windows	kA
95	#num#	k4
tento	tento	k3xDgInSc1
příkaz	příkaz	k1gInSc4
spouští	spouštět	k5eAaImIp3nS
správce	správce	k1gMnSc1
úloh	úloha	k1gFnPc2
anebo	anebo	k8xC
bezpečnostní	bezpečnostní	k2eAgFnSc4d1
komponentu	komponenta	k1gFnSc4
počítače	počítač	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
spolupracuje	spolupracovat	k5eAaImIp3nS
na	na	k7c6
ukončení	ukončení	k1gNnSc6
sezení	sezení	k1gNnPc2
Windows	Windows	kA
<g/>
.	.	kIx.
</s>
<s>
Kvůli	kvůli	k7c3
obtížnému	obtížný	k2eAgInSc3d1
stisknutí	stisknutí	k1gNnSc2
jednou	jeden	k4xCgFnSc7
rukou	ruka	k1gFnSc7
je	být	k5eAaImIp3nS
kombinace	kombinace	k1gFnSc1
přezdívána	přezdíván	k2eAgFnSc1d1
„	„	k?
<g/>
opičí	opičit	k5eAaImIp3nS
trojhmat	trojhmat	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Na	na	k7c4
originální	originální	k2eAgFnSc4d1
klávesnici	klávesnice	k1gFnSc4
k	k	k7c3
IBM	IBM	kA
PC	PC	kA
5150	#num#	k4
není	být	k5eNaImIp3nS
možné	možný	k2eAgFnPc4d1
klávesovou	klávesový	k2eAgFnSc4d1
zkratku	zkratka	k1gFnSc4
Ctrl	Ctrl	kA
<g/>
+	+	kIx~
<g/>
Alt	Alt	kA
<g/>
+	+	kIx~
<g/>
Delete	Dele	k1gNnSc2
stisknout	stisknout	k5eAaPmF
jednou	jeden	k4xCgFnSc7
rukou	ruka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Funkce	funkce	k1gFnSc1
rebootu	reboot	k1gInSc2
pomocí	pomocí	k7c2
klávesnice	klávesnice	k1gFnSc2
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
vymyšlena	vymyšlen	k2eAgFnSc1d1
Davidem	David	k1gMnSc7
Bradleyem	Bradley	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bradley	Bradle	k2eAgInPc1d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
hlavní	hlavní	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
projektu	projekt	k1gInSc2
IBM	IBM	kA
PC	PC	kA
a	a	k8xC
vývojář	vývojář	k1gMnSc1
BIOSu	BIOSa	k1gFnSc4
původně	původně	k6eAd1
používal	používat	k5eAaImAgMnS
klávesovou	klávesový	k2eAgFnSc4d1
zkratku	zkratka	k1gFnSc4
Ctrl	Ctrl	kA
<g/>
+	+	kIx~
<g/>
Alt	Alt	kA
<g/>
+	+	kIx~
<g/>
Esc	Esc	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
jednoduché	jednoduchý	k2eAgNnSc1d1
tuto	tento	k3xDgFnSc4
kombinaci	kombinace	k1gFnSc4
stisknout	stisknout	k5eAaPmF
omylem	omylem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
namísto	namísto	k7c2
této	tento	k3xDgFnSc2
kombinace	kombinace	k1gFnSc2
využil	využít	k5eAaPmAgInS
kombinaci	kombinace	k1gFnSc4
Ctrl	Ctrl	kA
<g/>
+	+	kIx~
<g/>
Alt	Alt	kA
<g/>
+	+	kIx~
<g/>
Delete	Dele	k1gNnSc2
jako	jako	k8xC,k8xS
bezpečnostní	bezpečnostní	k2eAgNnPc4d1
opatření	opatření	k1gNnPc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
je	být	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
klávesovou	klávesový	k2eAgFnSc4d1
zkratku	zkratka	k1gFnSc4
na	na	k7c6
původních	původní	k2eAgInPc6d1
klávesnících	klávesník	k1gInPc6
k	k	k7c3
IBM	IBM	kA
PC	PC	kA
5150	#num#	k4
nemožné	nemožná	k1gFnSc2
stisknout	stisknout	k5eAaPmF
jednou	jeden	k4xCgFnSc7
rukou	ruka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
funkce	funkce	k1gFnSc1
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
zamýšlena	zamýšlet	k5eAaImNgFnS
jen	jen	k9
pro	pro	k7c4
interní	interní	k2eAgNnSc4d1
použití	použití	k1gNnSc4
a	a	k8xC
nebyla	být	k5eNaImAgFnS
zamýšlena	zamýšlet	k5eAaImNgFnS
pro	pro	k7c4
koncové	koncový	k2eAgMnPc4d1
uživatele	uživatel	k1gMnPc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
rebootovala	rebootovat	k5eAaImAgFnS,k5eAaBmAgFnS,k5eAaPmAgFnS
počítač	počítač	k1gInSc4
bez	bez	k7c2
jakéhokoliv	jakýkoliv	k3yIgNnSc2
varování	varování	k1gNnSc2
či	či	k8xC
potvrzení	potvrzení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
používána	používat	k5eAaImNgFnS
lidmi	člověk	k1gMnPc7
píšící	píšící	k2eAgMnPc4d1
programy	program	k1gInPc4
nebo	nebo	k8xC
dokumentaci	dokumentace	k1gFnSc4
k	k	k7c3
nim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
snadno	snadno	k6eAd1
restartovat	restartovat	k5eAaBmF
svůj	svůj	k3xOyFgInSc4
počítač	počítač	k1gInSc4
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
ho	on	k3xPp3gInSc4
odpojit	odpojit	k5eAaPmF
od	od	k7c2
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
funkce	funkce	k1gFnPc4
byla	být	k5eAaImAgFnS
ale	ale	k9
zmíněna	zmínit	k5eAaPmNgFnS
v	v	k7c6
technické	technický	k2eAgFnSc6d1
dokumentaci	dokumentace	k1gFnSc6
k	k	k7c3
původním	původní	k2eAgMnPc3d1
počítačům	počítač	k1gInPc3
IBM	IBM	kA
a	a	k8xC
tedy	tedy	k8xC
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
informace	informace	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
na	na	k7c4
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Bradley	Bradley	k1gInPc1
se	se	k3xPyFc4
na	na	k7c4
tuto	tento	k3xDgFnSc4
práci	práce	k1gFnSc4
dívá	dívat	k5eAaImIp3nS
jen	jen	k9
jako	jako	k9
na	na	k7c4
jeden	jeden	k4xCgInSc4
úkol	úkol	k1gInSc4
z	z	k7c2
mnoha	mnoho	k4c2
<g/>
:	:	kIx,
„	„	k?
<g/>
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
pět	pět	k4xCc1
deset	deset	k4xCc1
minut	minuta	k1gFnPc2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
jsem	být	k5eAaImIp1nS
se	se	k3xPyFc4
přesunul	přesunout	k5eAaPmAgMnS
na	na	k7c4
další	další	k2eAgFnSc4d1
ze	z	k7c2
stovek	stovka	k1gFnPc2
úkolů	úkol	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
dokončeny	dokončit	k5eAaPmNgInP
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
BIOS	BIOS	kA
</s>
<s>
Ve	v	k7c6
výchozím	výchozí	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
běží	běžet	k5eAaImIp3nS
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
režimu	režim	k1gInSc6
(	(	kIx(
<g/>
nebo	nebo	k8xC
v	v	k7c6
režimu	režim	k1gInSc6
před	před	k7c7
dokončením	dokončení	k1gNnSc7
bootu	boot	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
není	být	k5eNaImIp3nS
spuštěn	spustit	k5eAaPmNgInS
žádný	žádný	k3yNgInSc1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
zachycena	zachytit	k5eAaPmNgFnS
BIOSem	BIOS	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
na	na	k7c4
ni	on	k3xPp3gFnSc4
reaguje	reagovat	k5eAaBmIp3nS
vynucením	vynucení	k1gNnSc7
rebootu	reboot	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Control-Alt-Delete	Control-Alt-Dele	k1gNnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
https://krize15.cz/zpravy/clanek/bill-gates-a-opici-trojhmat	https://krize15.cz/zpravy/clanek/bill-gates-a-opici-trojhmat	k1gInSc1
</s>
