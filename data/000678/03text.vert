<s>
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
hard	hard	k6eAd1	hard
rocková	rockový	k2eAgFnSc1d1	rocková
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
ze	z	k7c2	z
Sayreville	Sayrevill	k1gMnSc2	Sayrevill
<g/>
,	,	kIx,	,
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
kapela	kapela	k1gFnSc1	kapela
pětičlenná	pětičlenný	k2eAgFnSc1d1	pětičlenná
<g/>
,	,	kIx,	,
jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
Jon	Jon	k1gFnSc1	Jon
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
Richie	Richie	k1gFnSc1	Richie
Sambora	Sambora	k1gFnSc1	Sambora
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Phil	Phil	k1gMnSc1	Phil
X	X	kA	X
(	(	kIx(	(
<g/>
Náhradní	náhradní	k2eAgMnSc1d1	náhradní
kytarista	kytarista	k1gMnSc1	kytarista
za	za	k7c2	za
Richieho	Richie	k1gMnSc2	Richie
Samboru	Sambor	k1gInSc2	Sambor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tico	Tico	k1gMnSc1	Tico
Torres	Torres	k1gMnSc1	Torres
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc1	perkuse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Bryan	Bryan	k1gInSc1	Bryan
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hugh	Hugh	k1gMnSc1	Hugh
McDonald	McDonald	k1gMnSc1	McDonald
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
turné	turné	k1gNnSc6	turné
kapelu	kapela	k1gFnSc4	kapela
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
Bobby	Bobba	k1gFnPc4	Bobba
Bandiera	Bandiero	k1gNnSc2	Bandiero
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
založení	založení	k1gNnSc2	založení
se	se	k3xPyFc4	se
sestava	sestava	k1gFnSc1	sestava
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
neměnila	měnit	k5eNaImAgFnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
Richie	Richie	k1gFnSc2	Richie
Sambora	Sambora	k1gFnSc1	Sambora
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Davea	Davea	k1gMnSc1	Davea
Saboa	Saboa	k1gMnSc1	Saboa
na	na	k7c6	na
postu	post	k1gInSc6	post
kytaristy	kytarista	k1gMnSc2	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
kapelu	kapela	k1gFnSc4	kapela
opustil	opustit	k5eAaPmAgMnS	opustit
baskytarista	baskytarista	k1gMnSc1	baskytarista
Alec	Alec	k1gFnSc4	Alec
John	John	k1gMnSc1	John
Such	sucho	k1gNnPc2	sucho
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
oficiálně	oficiálně	k6eAd1	oficiálně
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
však	však	k9	však
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
zastoupil	zastoupit	k5eAaPmAgMnS	zastoupit
Hugh	Hugh	k1gMnSc1	Hugh
McDonnald	McDonnald	k1gMnSc1	McDonnald
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
kapely	kapela	k1gFnSc2	kapela
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
zkomoleného	zkomolený	k2eAgNnSc2d1	zkomolené
příjmení	příjmení	k1gNnSc2	příjmení
zakládajícího	zakládající	k2eAgMnSc2d1	zakládající
člena	člen	k1gMnSc2	člen
Johna	John	k1gMnSc2	John
Francise	Francise	k1gFnSc2	Francise
Bongioviho	Bongiovi	k1gMnSc2	Bongiovi
Jr	Jr	k1gMnSc2	Jr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Jon	Jon	k1gFnPc1	Jon
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kapelu	kapela	k1gFnSc4	kapela
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
založil	založit	k5eAaPmAgMnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
působí	působit	k5eAaImIp3nS	působit
nepřetržitě	přetržitě	k6eNd1	přetržitě
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
až	až	k6eAd1	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
několik	několik	k4yIc1	několik
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
hrála	hrát	k5eAaImAgFnS	hrát
roli	role	k1gFnSc4	role
při	při	k7c6	při
popularizování	popularizování	k1gNnSc6	popularizování
hard	hard	k1gInSc4	hard
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
kapel	kapela	k1gFnPc2	kapela
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
první	první	k4xOgInSc4	první
hit	hit	k1gInSc4	hit
z	z	k7c2	z
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
Bon	bon	k1gInSc1	bon
Jovi	Jove	k1gFnSc4	Jove
"	"	kIx"	"
<g/>
Runaway	Runaway	k1gInPc4	Runaway
<g/>
"	"	kIx"	"
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
na	na	k7c4	na
39	[number]	k4	39
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
získala	získat	k5eAaPmAgFnS	získat
první	první	k4xOgFnPc4	první
zlaté	zlatý	k2eAgFnPc4d1	zlatá
desky	deska	k1gFnPc4	deska
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
alba	alba	k1gFnSc1	alba
7800	[number]	k4	7800
<g/>
°	°	k?	°
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přineslo	přinést	k5eAaPmAgNnS	přinést
tři	tři	k4xCgInPc1	tři
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
singly	singl	k1gInPc1	singl
<g/>
,	,	kIx,	,
umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
na	na	k7c4	na
37	[number]	k4	37
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
zlaté	zlatý	k2eAgFnSc2d1	zlatá
desky	deska	k1gFnSc2	deska
<g/>
;	;	kIx,	;
ještě	ještě	k6eAd1	ještě
úspěšnější	úspěšný	k2eAgFnSc2d2	úspěšnější
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
Slippery	Slippera	k1gFnSc2	Slippera
When	Whena	k1gFnPc2	Whena
Wet	Wet	k1gFnSc2	Wet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
přineslo	přinést	k5eAaPmAgNnS	přinést
dva	dva	k4xCgInPc1	dva
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
Give	Giv	k1gFnSc2	Giv
Love	lov	k1gInSc5	lov
a	a	k8xC	a
Bad	Bad	k1gMnSc4	Bad
Name	Nam	k1gMnSc4	Nam
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Livin	Livin	k1gInSc1	Livin
<g/>
'	'	kIx"	'
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Prayer	Prayero	k1gNnPc2	Prayero
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
oba	dva	k4xCgMnPc4	dva
dobyly	dobýt	k5eAaPmAgFnP	dobýt
vrchol	vrchol	k1gInSc4	vrchol
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
a	a	k8xC	a
album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
trojnásobné	trojnásobný	k2eAgNnSc1d1	trojnásobné
ocenění	ocenění	k1gNnSc1	ocenění
platinové	platinový	k2eAgFnSc2d1	platinová
desky	deska	k1gFnSc2	deska
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
hard	hard	k6eAd1	hard
rocková	rockový	k2eAgFnSc1d1	rocková
osmdesátá	osmdesátý	k4xOgFnSc1	osmdesátý
léta	léto	k1gNnSc2	léto
završilo	završit	k5eAaPmAgNnS	završit
album	album	k1gNnSc4	album
New	New	k1gFnSc3	New
Jersey	Jersea	k1gFnSc2	Jersea
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
první	první	k4xOgNnSc1	první
místo	místo	k1gNnSc1	místo
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
a	a	k8xC	a
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
žebříček	žebříček	k1gInSc1	žebříček
UK	UK	kA	UK
Albums	Albums	k1gInSc4	Albums
Chart	charta	k1gFnPc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
přineslo	přinést	k5eAaPmAgNnS	přinést
pět	pět	k4xCc4	pět
singlů	singl	k1gInPc2	singl
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgInPc1d3	nejúspěšnější
byly	být	k5eAaImAgInP	být
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Bad	Bad	k1gMnSc5	Bad
Medicine	Medicin	k1gMnSc5	Medicin
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Be	Be	k1gMnSc5	Be
There	Ther	k1gMnSc5	Ther
for	forum	k1gNnPc2	forum
You	You	k1gFnSc1	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
sedminásobné	sedminásobný	k2eAgNnSc1d1	sedminásobné
ocenění	ocenění	k1gNnSc1	ocenění
platinové	platinový	k2eAgFnSc2d1	platinová
desky	deska	k1gFnSc2	deska
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
aktivita	aktivita	k1gFnSc1	aktivita
skupiny	skupina	k1gFnPc4	skupina
opadla	opadnout	k5eAaPmAgFnS	opadnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990	[number]	k4	1990
a	a	k8xC	a
1991	[number]	k4	1991
kapela	kapela	k1gFnSc1	kapela
vůbec	vůbec	k9	vůbec
nevystupovala	vystupovat	k5eNaImAgFnS	vystupovat
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
vydávali	vydávat	k5eAaPmAgMnP	vydávat
svá	svůj	k3xOyFgNnPc4	svůj
sólová	sólový	k2eAgNnPc4d1	sólové
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
totéž	týž	k3xTgNnSc1	týž
pak	pak	k6eAd1	pak
kapelu	kapela	k1gFnSc4	kapela
čekalo	čekat	k5eAaImAgNnS	čekat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
až	až	k9	až
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
kapela	kapela	k1gFnSc1	kapela
stihla	stihnout	k5eAaPmAgFnS	stihnout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
vydat	vydat	k5eAaPmF	vydat
tři	tři	k4xCgFnPc4	tři
velmi	velmi	k6eAd1	velmi
úspěšná	úspěšný	k2eAgNnPc1d1	úspěšné
alba	album	k1gNnPc1	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
album	album	k1gNnSc4	album
Keep	Keep	k1gMnSc1	Keep
the	the	k?	the
Faith	Faith	k1gInSc1	Faith
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
získalo	získat	k5eAaPmAgNnS	získat
dvojnásobné	dvojnásobný	k2eAgNnSc1d1	dvojnásobné
platinové	platinový	k2eAgNnSc1d1	platinové
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc4	zvuk
kapely	kapela	k1gFnSc2	kapela
začala	začít	k5eAaPmAgFnS	začít
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
narůstající	narůstající	k2eAgFnSc1d1	narůstající
popularita	popularita	k1gFnSc1	popularita
grunge	grungat	k5eAaPmIp3nS	grungat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
si	se	k3xPyFc3	se
udržovala	udržovat	k5eAaImAgFnS	udržovat
rockový	rockový	k2eAgInSc4d1	rockový
standard	standard	k1gInSc4	standard
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kompilační	kompilační	k2eAgNnSc1d1	kompilační
album	album	k1gNnSc1	album
Cross	Crossa	k1gFnPc2	Crossa
Road	Roada	k1gFnPc2	Roada
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
přineslo	přinést	k5eAaPmAgNnS	přinést
dva	dva	k4xCgInPc4	dva
nové	nový	k2eAgInPc4d1	nový
hity	hit	k1gInPc4	hit
"	"	kIx"	"
<g/>
Someday	Someday	k1gInPc4	Someday
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Be	Be	k1gMnSc1	Be
Saturday	Saturdaa	k1gFnSc2	Saturdaa
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Always	Always	k1gInSc1	Always
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
obrovským	obrovský	k2eAgInSc7d1	obrovský
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
po	po	k7c4	po
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
Top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
platinové	platinový	k2eAgNnSc4d1	platinové
ocenění	ocenění	k1gNnSc4	ocenění
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
nejlépe	dobře	k6eAd3	dobře
prodávaný	prodávaný	k2eAgInSc1d1	prodávaný
singl	singl	k1gInSc1	singl
skupiny	skupina	k1gFnSc2	skupina
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
These	these	k1gFnSc1	these
Days	Daysa	k1gFnPc2	Daysa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
rovněž	rovněž	k9	rovněž
získalo	získat	k5eAaPmAgNnS	získat
platinové	platinový	k2eAgNnSc4d1	platinové
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
nahrané	nahraný	k2eAgNnSc1d1	nahrané
bez	bez	k7c2	bez
baskytaristy	baskytarista	k1gMnSc2	baskytarista
Aleca	Alecus	k1gMnSc2	Alecus
Suche	Such	k1gMnSc2	Such
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
kapelu	kapela	k1gFnSc4	kapela
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pauze	pauza	k1gFnSc6	pauza
na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
Crush	Crush	k1gInSc1	Crush
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
hlavní	hlavní	k2eAgInSc1d1	hlavní
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Life	Lifus	k1gMnSc5	Lifus
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgInPc2d3	nejúspěšnější
singlů	singl	k1gInPc2	singl
kapely	kapela	k1gFnSc2	kapela
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
významně	významně	k6eAd1	významně
změnila	změnit	k5eAaPmAgFnS	změnit
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
k	k	k7c3	k
pop	pop	k1gInSc1	pop
rockovému	rockový	k2eAgInSc3d1	rockový
zvuku	zvuk	k1gInSc3	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
oslovit	oslovit	k5eAaPmF	oslovit
novou	nový	k2eAgFnSc4d1	nová
generaci	generace	k1gFnSc4	generace
mladých	mladý	k2eAgMnPc2d1	mladý
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
kapela	kapela	k1gFnSc1	kapela
opět	opět	k6eAd1	opět
aktivnější	aktivní	k2eAgFnSc1d2	aktivnější
<g/>
,	,	kIx,	,
když	když	k8xS	když
vydala	vydat	k5eAaPmAgFnS	vydat
dalších	další	k2eAgNnPc2d1	další
pět	pět	k4xCc4	pět
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgNnSc7d1	poslední
vydaným	vydaný	k2eAgNnSc7d1	vydané
albem	album	k1gNnSc7	album
je	být	k5eAaImIp3nS	být
The	The	k1gFnSc1	The
Circle	Circle	k1gFnSc1	Circle
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
ještě	ještě	k6eAd1	ještě
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
další	další	k2eAgFnSc4d1	další
kompilaci	kompilace	k1gFnSc4	kompilace
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
kapela	kapela	k1gFnSc1	kapela
prodala	prodat	k5eAaPmAgFnS	prodat
celosvětově	celosvětově	k6eAd1	celosvětově
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
130	[number]	k4	130
000	[number]	k4	000
000	[number]	k4	000
alb	alba	k1gFnPc2	alba
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
připuštěni	připustit	k5eAaPmNgMnP	připustit
do	do	k7c2	do
UK	UK	kA	UK
Music	Musice	k1gFnPc2	Musice
Hall	Halla	k1gFnPc2	Halla
of	of	k?	of
Fame	Fame	k1gNnSc6	Fame
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byli	být	k5eAaImAgMnP	být
nominováni	nominován	k2eAgMnPc1d1	nominován
na	na	k7c6	na
přijetí	přijetí	k1gNnSc6	přijetí
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
.	.	kIx.	.
</s>
<s>
Zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Francis	Francis	k1gFnSc2	Francis
Bongiovi	Bongiův	k2eAgMnPc1d1	Bongiův
(	(	kIx(	(
<g/>
Jon	Jon	k1gMnPc1	Jon
Bon	bona	k1gFnPc2	bona
Jovi	Jovi	k1gNnSc2	Jovi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nápad	nápad	k1gInSc1	nápad
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
kapely	kapela	k1gFnSc2	kapela
dostal	dostat	k5eAaPmAgMnS	dostat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
mu	on	k3xPp3gMnSc3	on
jeho	jeho	k3xOp3gNnSc1	jeho
bratranec	bratranec	k1gMnSc1	bratranec
Tony	Tony	k1gMnSc1	Tony
Bongiovi	Bongius	k1gMnSc3	Bongius
umožnil	umožnit	k5eAaPmAgMnS	umožnit
natočit	natočit	k5eAaBmF	natočit
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nahrávacím	nahrávací	k2eAgNnSc6d1	nahrávací
studiu	studio	k1gNnSc6	studio
Power	Powra	k1gFnPc2	Powra
Station	station	k1gInSc4	station
první	první	k4xOgFnSc4	první
demo	demo	k2eAgFnSc4d1	demo
nahrávku	nahrávka	k1gFnSc4	nahrávka
pro	pro	k7c4	pro
velké	velký	k2eAgFnPc4d1	velká
nahrávací	nahrávací	k2eAgFnPc4d1	nahrávací
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
demo	demo	k2eAgFnSc7d1	demo
písní	píseň	k1gFnSc7	píseň
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Runaway	Runawaa	k1gFnSc2	Runawaa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
Jona	Jon	k1gInSc2	Jon
podílel	podílet	k5eAaImAgMnS	podílet
baskytarista	baskytarista	k1gMnSc1	baskytarista
Hugh	Hugh	k1gMnSc1	Hugh
McDonald	McDonald	k1gMnSc1	McDonald
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
člen	člen	k1gMnSc1	člen
kapely	kapela	k1gFnSc2	kapela
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
Frankie	Frankie	k1gFnSc2	Frankie
LaRocka	LaRocka	k1gFnSc1	LaRocka
a	a	k8xC	a
klávesista	klávesista	k1gMnSc1	klávesista
Roy	Roy	k1gMnSc1	Roy
Bittan	Bittan	k1gInSc4	Bittan
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
neočekávaný	očekávaný	k2eNgInSc4d1	neočekávaný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
39	[number]	k4	39
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
singlového	singlový	k2eAgInSc2d1	singlový
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
Jona	Jonus	k1gMnSc4	Jonus
k	k	k7c3	k
sestavení	sestavení	k1gNnSc3	sestavení
nové	nový	k2eAgFnSc2d1	nová
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
příjmení	příjmení	k1gNnSc6	příjmení
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
členem	člen	k1gInSc7	člen
kapely	kapela	k1gFnSc2	kapela
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
David	David	k1gMnSc1	David
Bryan	Bryan	k1gMnSc1	Bryan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
Jonem	Jonum	k1gNnSc7	Jonum
znal	znát	k5eAaImAgInS	znát
už	už	k6eAd1	už
z	z	k7c2	z
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Atlantic	Atlantice	k1gFnPc2	Atlantice
City	City	k1gFnSc2	City
Expressway	Expresswaa	k1gFnSc2	Expresswaa
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
členem	člen	k1gMnSc7	člen
kapely	kapela	k1gFnSc2	kapela
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Alec	Alec	k1gInSc1	Alec
John	John	k1gMnSc1	John
Such	sucho	k1gNnPc2	sucho
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
dosazen	dosadit	k5eAaPmNgInS	dosadit
na	na	k7c4	na
post	post	k1gInSc4	post
baskytaristy	baskytarista	k1gMnSc2	baskytarista
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
hrál	hrát	k5eAaImAgMnS	hrát
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
The	The	k1gMnSc2	The
Message	Messag	k1gMnSc2	Messag
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
dvěma	dva	k4xCgInPc7	dva
členy	člen	k1gInPc7	člen
budoucími	budoucí	k2eAgMnPc7d1	budoucí
kapely	kapela	k1gFnSc2	kapela
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
<g/>
,	,	kIx,	,
Tico	Tico	k6eAd1	Tico
Torresem	Torres	k1gInSc7	Torres
a	a	k8xC	a
Richiem	Richium	k1gNnSc7	Richium
Samborou	Sambora	k1gFnSc7	Sambora
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
kytaristy	kytarista	k1gMnSc2	kytarista
hrál	hrát	k5eAaImAgMnS	hrát
pozdější	pozdní	k2eAgMnSc1d2	pozdější
člen	člen	k1gMnSc1	člen
Skid	Skid	k1gMnSc1	Skid
Row	Row	k1gMnSc1	Row
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Sabo	Sabo	k1gMnSc1	Sabo
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
vzápětí	vzápětí	k6eAd1	vzápětí
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Richie	Richie	k1gFnSc2	Richie
Sambora	Sambor	k1gMnSc2	Sambor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
přidal	přidat	k5eAaPmAgInS	přidat
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Jon	Jon	k?	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
jeho	jeho	k3xOp3gNnSc2	jeho
přijetí	přijetí	k1gNnSc2	přijetí
popisuje	popisovat	k5eAaImIp3nS	popisovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jednoho	jeden	k4xCgInSc2	jeden
večera	večer	k1gInSc2	večer
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
koncertě	koncert	k1gInSc6	koncert
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
pořádala	pořádat	k5eAaImAgFnS	pořádat
jedna	jeden	k4xCgFnSc1	jeden
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
šel	jít	k5eAaImAgMnS	jít
z	z	k7c2	z
jeviště	jeviště	k1gNnSc2	jeviště
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
týpek	týpek	k1gInSc1	týpek
mezi	mezi	k7c7	mezi
diváky	divák	k1gMnPc7	divák
mi	já	k3xPp1nSc3	já
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
́	́	k?	́
<g/>
Budu	být	k5eAaImBp1nS	být
tvůj	tvůj	k3xOp2gMnSc1	tvůj
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
́	́	k?	́
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
smál	smát	k5eAaImAgMnS	smát
a	a	k8xC	a
šel	jít	k5eAaImAgMnS	jít
jsem	být	k5eAaImIp1nS	být
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
slyšel	slyšet	k5eAaImAgMnS	slyšet
hrát	hrát	k5eAaImF	hrát
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vše	všechen	k3xTgNnSc1	všechen
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
několika	několik	k4yIc6	několik
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
jako	jako	k9	jako
předkapela	předkapela	k1gFnSc1	předkapela
jiných	jiný	k2eAgFnPc2d1	jiná
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
jich	on	k3xPp3gFnPc2	on
všiml	všimnout	k5eAaPmAgMnS	všimnout
Derek	Derek	k1gMnSc1	Derek
Shulman	Shulman	k1gMnSc1	Shulman
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jim	on	k3xPp3gMnPc3	on
domohl	domoct	k5eAaPmAgInS	domoct
ke	k	k7c3	k
smlouvě	smlouva	k1gFnSc3	smlouva
s	s	k7c7	s
Mercury	Mercur	k1gMnPc7	Mercur
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
většího	veliký	k2eAgInSc2d2	veliký
koncernu	koncern	k1gInSc2	koncern
Polygram	Polygram	k1gInSc1	Polygram
<g/>
.	.	kIx.	.
</s>
<s>
Jon	Jon	k?	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
chtěl	chtít	k5eAaImAgInS	chtít
nějaké	nějaký	k3yIgNnSc4	nějaký
jméno	jméno	k1gNnSc4	jméno
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
Pamela	Pamela	k1gFnSc1	Pamela
Maherová	Maherová	k1gFnSc1	Maherová
<g/>
,	,	kIx,	,
podřízená	podřízená	k1gFnSc1	podřízená
jejich	jejich	k3xOp3gMnSc2	jejich
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
manažera	manažer	k1gMnSc2	manažer
<g/>
,	,	kIx,	,
jim	on	k3xPp3gMnPc3	on
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
jméno	jméno	k1gNnSc1	jméno
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
slavné	slavný	k2eAgFnSc2d1	slavná
skupiny	skupina	k1gFnSc2	skupina
Van	vana	k1gFnPc2	vana
Halen	halena	k1gFnPc2	halena
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
zamýšleli	zamýšlet	k5eAaImAgMnP	zamýšlet
jméno	jméno	k1gNnSc4	jméno
Johnny	Johnna	k1gFnSc2	Johnna
Electric	Electrice	k1gFnPc2	Electrice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
nechali	nechat	k5eAaPmAgMnP	nechat
poradit	poradit	k5eAaPmF	poradit
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nebyli	být	k5eNaImAgMnP	být
příliš	příliš	k6eAd1	příliš
nadšení	nadšený	k2eAgMnPc1d1	nadšený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
pod	pod	k7c7	pod
tím	ten	k3xDgNnSc7	ten
jménem	jméno	k1gNnSc7	jméno
dobyli	dobýt	k5eAaPmAgMnP	dobýt
přední	přední	k2eAgFnPc4d1	přední
pozice	pozice	k1gFnPc4	pozice
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
manažera	manažer	k1gMnSc2	manažer
Doca	Docus	k1gMnSc2	Docus
McGheeho	McGhee	k1gMnSc2	McGhee
vydali	vydat	k5eAaPmAgMnP	vydat
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Bon	bona	k1gFnPc2	bona
Jovi	Jovi	k1gNnSc2	Jovi
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
znovu	znovu	k6eAd1	znovu
vyšel	vyjít	k5eAaPmAgInS	vyjít
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
kapely	kapela	k1gFnSc2	kapela
"	"	kIx"	"
<g/>
Runaway	Runawaa	k1gFnSc2	Runawaa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vešel	vejít	k5eAaPmAgInS	vejít
do	do	k7c2	do
Top	topit	k5eAaImRp2nS	topit
40	[number]	k4	40
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
She	She	k1gMnSc1	She
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Know	Know	k1gMnSc1	Know
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
museli	muset	k5eAaImAgMnP	muset
na	na	k7c4	na
album	album	k1gNnSc4	album
přidat	přidat	k5eAaPmF	přidat
kvůli	kvůli	k7c3	kvůli
přání	přání	k1gNnSc3	přání
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
kdy	kdy	k6eAd1	kdy
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
albu	album	k1gNnSc6	album
Bon	bona	k1gFnPc2	bona
Jovi	Jovi	k1gNnPc2	Jovi
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
Jon	Jon	k1gFnSc3	Jon
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
nenapsal	napsat	k5eNaPmAgMnS	napsat
text	text	k1gInSc4	text
(	(	kIx(	(
<g/>
napsal	napsat	k5eAaPmAgInS	napsat
ji	on	k3xPp3gFnSc4	on
Marc	Marc	k1gFnSc4	Marc
Avsec	Avsec	k1gInSc1	Avsec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
kapela	kapela	k1gFnSc1	kapela
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
otevírala	otevírat	k5eAaImAgFnS	otevírat
programy	program	k1gInPc4	program
například	například	k6eAd1	například
pro	pro	k7c4	pro
kapelu	kapela	k1gFnSc4	kapela
Scorpions	Scorpionsa	k1gFnPc2	Scorpionsa
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
kapelu	kapela	k1gFnSc4	kapela
Kiss	Kissa	k1gFnPc2	Kissa
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
turné	turné	k1gNnSc6	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c4	na
Super	super	k2eAgInSc4d1	super
Rock	rock	k1gInSc4	rock
festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
jako	jako	k8xS	jako
speciální	speciální	k2eAgMnSc1d1	speciální
host	host	k1gMnSc1	host
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Runaway	Runaway	k1gInPc1	Runaway
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
stala	stát	k5eAaPmAgFnS	stát
hitem	hit	k1gInSc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
ocenění	ocenění	k1gNnSc2	ocenění
zlaté	zlatý	k2eAgFnPc1d1	zlatá
desky	deska	k1gFnPc1	deska
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
za	za	k7c4	za
rok	rok	k1gInSc4	rok
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
toto	tento	k3xDgNnSc4	tento
ocenění	ocenění	k1gNnSc1	ocenění
získalo	získat	k5eAaPmAgNnS	získat
i	i	k9	i
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
43	[number]	k4	43
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
druhé	druhý	k4xOgFnSc2	druhý
studiové	studiový	k2eAgFnSc2d1	studiová
album	album	k1gNnSc4	album
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
7800	[number]	k4	7800
<g/>
°	°	k?	°
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
přineslo	přinést	k5eAaPmAgNnS	přinést
deset	deset	k4xCc1	deset
nových	nový	k2eAgFnPc2d1	nová
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
pět	pět	k4xCc1	pět
singlů	singl	k1gInPc2	singl
-	-	kIx~	-
"	"	kIx"	"
<g/>
Only	Onl	k2eAgFnPc1d1	Onl
Lonely	Lonela	k1gFnPc1	Lonela
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
In	In	k1gMnSc1	In
And	Anda	k1gFnPc2	Anda
Out	Out	k1gMnSc1	Out
Of	Of	k1gMnSc1	Of
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
balada	balada	k1gFnSc1	balada
"	"	kIx"	"
<g/>
Silent	Silent	k1gMnSc1	Silent
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Hardest	Hardest	k1gMnSc1	Hardest
Part	parta	k1gFnPc2	parta
Is	Is	k1gMnSc1	Is
the	the	k?	the
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Price	Price	k1gFnSc2	Price
of	of	k?	of
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
37	[number]	k4	37
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
ocenění	ocenění	k1gNnSc4	ocenění
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
albu	album	k1gNnSc6	album
nedařilo	dařit	k5eNaImAgNnS	dařit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
kapela	kapela	k1gFnSc1	kapela
doufala	doufat	k5eAaImAgFnS	doufat
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
si	se	k3xPyFc3	se
dovolit	dovolit	k5eAaPmF	dovolit
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
výtěžků	výtěžek	k1gInPc2	výtěžek
vyjet	vyjet	k5eAaPmF	vyjet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
jako	jako	k8xS	jako
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahrála	zahrát	k5eAaPmAgFnS	zahrát
na	na	k7c6	na
osmi	osm	k4xCc6	osm
úplně	úplně	k6eAd1	úplně
vyprodaných	vyprodaný	k2eAgNnPc6d1	vyprodané
představeních	představení	k1gNnPc6	představení
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
Top	topit	k5eAaImRp2nS	topit
5	[number]	k4	5
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
zde	zde	k6eAd1	zde
taktéž	taktéž	k?	taktéž
ocenění	ocenění	k1gNnSc4	ocenění
zlaté	zlatý	k2eAgFnSc2d1	zlatá
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1985	[number]	k4	1985
vedla	vést	k5eAaImAgFnS	vést
kapela	kapela	k1gFnSc1	kapela
turné	turné	k1gNnSc2	turné
po	po	k7c6	po
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
starém	starý	k2eAgInSc6d1	starý
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
7800	[number]	k4	7800
<g/>
°	°	k?	°
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
UK	UK	kA	UK
Albums	Albumsa	k1gFnPc2	Albumsa
Chart	charta	k1gFnPc2	charta
a	a	k8xC	a
na	na	k7c4	na
40	[number]	k4	40
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
evropském	evropský	k2eAgNnSc6d1	Evropské
turné	turné	k1gNnSc6	turné
následovalo	následovat	k5eAaImAgNnS	následovat
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
koncertování	koncertování	k1gNnSc4	koncertování
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Ratt	Ratta	k1gFnPc2	Ratta
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
tohoto	tento	k3xDgNnSc2	tento
turné	turné	k1gNnSc2	turné
také	také	k9	také
znovu	znovu	k6eAd1	znovu
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Anglii	Anglie	k1gFnSc4	Anglie
při	při	k7c6	při
festivalu	festival	k1gInSc6	festival
Monsters	Monsters	k1gInSc1	Monsters
of	of	k?	of
Rock	rock	k1gInSc1	rock
a	a	k8xC	a
Jon	Jon	k1gFnSc1	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
Farm	Farma	k1gFnPc2	Farma
Aid	Aida	k1gFnPc2	Aida
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
Bon	bon	k1gInSc1	bon
Jovi	Jovi	k1gNnSc2	Jovi
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
do	do	k7c2	do
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zde	zde	k6eAd1	zde
natočili	natočit	k5eAaBmAgMnP	natočit
svoje	svůj	k3xOyFgNnSc4	svůj
třetí	třetí	k4xOgNnSc4	třetí
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
šestiměsíční	šestiměsíční	k2eAgFnSc2d1	šestiměsíční
práce	práce	k1gFnSc2	práce
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Slippery	Slippera	k1gFnSc2	Slippera
When	Whena	k1gFnPc2	Whena
Wet	Wet	k1gMnSc1	Wet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
albu	alba	k1gFnSc4	alba
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
producenty	producent	k1gMnPc7	producent
Brucem	Bruce	k1gMnSc7	Bruce
Fairbairnem	Fairbairno	k1gNnSc7	Fairbairno
a	a	k8xC	a
Bobem	Bob	k1gMnSc7	Bob
Rockem	rock	k1gInSc7	rock
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1986	[number]	k4	1986
a	a	k8xC	a
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pro	pro	k7c4	pro
kapelu	kapela	k1gFnSc4	kapela
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
další	další	k2eAgInSc4d1	další
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
Give	Giv	k1gFnSc2	Giv
Love	lov	k1gInSc5	lov
a	a	k8xC	a
Bad	Bad	k1gMnSc4	Bad
Name	Nam	k1gMnSc4	Nam
<g/>
"	"	kIx"	"
obsadil	obsadit	k5eAaPmAgMnS	obsadit
poprvé	poprvé	k6eAd1	poprvé
první	první	k4xOgFnSc4	první
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
i	i	k9	i
druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
alba	album	k1gNnSc2	album
"	"	kIx"	"
<g/>
Livin	Livin	k1gMnSc1	Livin
<g/>
'	'	kIx"	'
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Prayer	Prayero	k1gNnPc2	Prayero
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
singly	singl	k1gInPc1	singl
byly	být	k5eAaImAgInP	být
napsány	napsat	k5eAaBmNgInP	napsat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Desmondem	Desmond	k1gMnSc7	Desmond
Childem	Child	k1gMnSc7	Child
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
seznámila	seznámit	k5eAaPmAgFnS	seznámit
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
Paula	Paul	k1gMnSc2	Paul
Stanleye	Stanley	k1gMnSc2	Stanley
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
mezi	mezi	k7c7	mezi
Jonem	Jon	k1gInSc7	Jon
Bon	bon	k1gInSc1	bon
Jovim	Jovima	k1gFnPc2	Jovima
<g/>
,	,	kIx,	,
Richiem	Richium	k1gNnSc7	Richium
Samborou	Sambora	k1gFnSc7	Sambora
a	a	k8xC	a
Childem	Child	k1gInSc7	Child
vydržela	vydržet	k5eAaPmAgFnS	vydržet
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
singl	singl	k1gInSc1	singl
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Wanted	Wanted	k1gInSc1	Wanted
Dead	Dead	k1gInSc1	Dead
or	or	k?	or
Alive	Aliev	k1gFnSc2	Aliev
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazývat	k5eAaImNgMnS	nazývat
"	"	kIx"	"
<g/>
národní	národní	k2eAgFnSc7d1	národní
hymnou	hymna	k1gFnSc7	hymna
kapely	kapela	k1gFnSc2	kapela
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
MTV	MTV	kA	MTV
srdečně	srdečně	k6eAd1	srdečně
přijalo	přijmout	k5eAaPmAgNnS	přijmout
kapelu	kapela	k1gFnSc4	kapela
Bon	bona	k1gFnPc2	bona
Jovi	Jovi	k1gNnPc2	Jovi
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
dobře	dobře	k6eAd1	dobře
natočená	natočený	k2eAgNnPc1d1	natočené
videa	video	k1gNnPc1	video
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
koncertní	koncertní	k2eAgNnSc4d1	koncertní
videa	video	k1gNnSc2	video
pomohla	pomoct	k5eAaPmAgFnS	pomoct
skupině	skupina	k1gFnSc3	skupina
k	k	k7c3	k
hvězdnému	hvězdný	k2eAgInSc3d1	hvězdný
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ohromujícím	ohromující	k2eAgInSc6d1	ohromující
úspěchu	úspěch	k1gInSc6	úspěch
alba	album	k1gNnSc2	album
Slippery	Slippera	k1gFnSc2	Slippera
When	When	k1gMnSc1	When
Wet	Wet	k1gMnSc1	Wet
se	se	k3xPyFc4	se
z	z	k7c2	z
Bon	bona	k1gFnPc2	bona
Jovi	Jovi	k1gNnPc2	Jovi
staly	stát	k5eAaPmAgFnP	stát
celosvětové	celosvětový	k2eAgFnPc1d1	celosvětová
superhvězdy	superhvězda	k1gFnPc1	superhvězda
<g/>
,	,	kIx,	,
po	po	k7c6	po
čemž	což	k3yQnSc6	což
tak	tak	k6eAd1	tak
toužili	toužit	k5eAaImAgMnP	toužit
<g/>
.	.	kIx.	.
</s>
<s>
Slippery	Slipper	k1gInPc1	Slipper
When	Whena	k1gFnPc2	Whena
Wet	Wet	k1gFnPc2	Wet
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c6	na
první	první	k4xOgFnSc6	první
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
rekord	rekord	k1gInSc4	rekord
jako	jako	k8xC	jako
nejdéle	dlouho	k6eAd3	dlouho
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
hard	hard	k1gMnSc1	hard
rockové	rockový	k2eAgNnSc4d1	rockové
album	album	k1gNnSc4	album
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
pozici	pozice	k1gFnSc6	pozice
udrželo	udržet	k5eAaPmAgNnS	udržet
po	po	k7c4	po
8	[number]	k4	8
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
alb	album	k1gNnPc2	album
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
také	také	k9	také
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
UK	UK	kA	UK
Albums	Albums	k1gInSc4	Albums
Chart	charta	k1gFnPc2	charta
figurovalo	figurovat	k5eAaImAgNnS	figurovat
po	po	k7c4	po
123	[number]	k4	123
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Billboard	billboard	k1gInSc1	billboard
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
a	a	k8xC	a
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Livin	Livin	k1gMnSc1	Livin
<g/>
'	'	kIx"	'
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Prayer	Prayero	k1gNnPc2	Prayero
<g/>
"	"	kIx"	"
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
cenu	cena	k1gFnSc4	cena
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Music	k1gMnSc1	Music
Award	Award	k1gMnSc1	Award
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výkon	výkon	k1gInSc4	výkon
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
kapela	kapela	k1gFnSc1	kapela
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
American	American	k1gInSc4	American
Music	Musice	k1gInPc2	Musice
Award	Awarda	k1gFnPc2	Awarda
jako	jako	k8xC	jako
nejoblíbenější	oblíbený	k2eAgInSc4d3	nejoblíbenější
pop	pop	k1gInSc4	pop
<g/>
/	/	kIx~	/
<g/>
rockovou	rockový	k2eAgFnSc4d1	rocková
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	alba	k1gFnSc1	alba
kapela	kapela	k1gFnSc1	kapela
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
nejprve	nejprve	k6eAd1	nejprve
vyjela	vyjet	k5eAaPmAgFnS	vyjet
pomoci	pomoct	k5eAaPmF	pomoct
při	při	k7c6	při
turné	turné	k1gNnSc6	turné
kapele	kapela	k1gFnSc3	kapela
38	[number]	k4	38
Special	Special	k1gInSc1	Special
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
další	další	k2eAgInPc1d1	další
půl	půl	k6eAd1	půl
rok	rok	k1gInSc4	rok
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
koncertech	koncert	k1gInPc6	koncert
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
byli	být	k5eAaImAgMnP	být
leadery	leader	k1gMnPc7	leader
festivalu	festival	k1gInSc2	festival
Monsters	Monsters	k1gInSc1	Monsters
of	of	k?	of
Rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
zazpívali	zazpívat	k5eAaPmAgMnP	zazpívat
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
We	We	k1gMnSc1	We
All	All	k1gMnSc1	All
Sleep	Sleep	k1gMnSc1	Sleep
Alone	Alon	k1gMnSc5	Alon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
přidali	přidat	k5eAaPmAgMnP	přidat
Dee	Dee	k1gMnPc1	Dee
Snider	Snidero	k1gNnPc2	Snidero
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Twisted	Twisted	k1gMnSc1	Twisted
Sister	Sister	k1gMnSc1	Sister
<g/>
,	,	kIx,	,
Bruce	Bruce	k1gMnSc1	Bruce
Dickinson	Dickinson	k1gMnSc1	Dickinson
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Iron	iron	k1gInSc1	iron
Maiden	Maidno	k1gNnPc2	Maidno
a	a	k8xC	a
Paul	Paula	k1gFnPc2	Paula
Stanley	Stanlea	k1gFnSc2	Stanlea
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
za	za	k7c4	za
sebou	se	k3xPyFc7	se
měla	mít	k5eAaImAgFnS	mít
130	[number]	k4	130
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
při	pře	k1gFnSc4	pře
"	"	kIx"	"
<g/>
Tour	Tour	k1gMnSc1	Tour
Without	Without	k1gMnSc1	Without
End	End	k1gMnSc1	End
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
Turné	turné	k1gNnSc1	turné
bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
<g/>
)	)	kIx)	)
a	a	k8xC	a
vydělala	vydělat	k5eAaPmAgFnS	vydělat
přes	přes	k7c4	přes
28	[number]	k4	28
milionů	milion	k4xCgInPc2	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
tak	tak	k6eAd1	tak
obrovský	obrovský	k2eAgInSc1d1	obrovský
úspěch	úspěch	k1gInSc1	úspěch
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
Jon	Jon	k1gMnSc1	Jon
Bon	bon	k1gInSc4	bon
Jovi	Jovi	k1gNnSc2	Jovi
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všechno	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgNnSc1d2	veliký
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
dvakrát	dvakrát	k6eAd1	dvakrát
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
vás	vy	k3xPp2nPc4	vy
poznávají	poznávat	k5eAaImIp3nP	poznávat
dvakrát	dvakrát	k6eAd1	dvakrát
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc4d2	veliký
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
je	být	k5eAaImIp3nS	být
najednou	najednou	k6eAd1	najednou
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Musíte	muset	k5eAaImIp2nP	muset
prodávat	prodávat	k5eAaImF	prodávat
další	další	k2eAgFnPc1d1	další
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
nahrávky	nahrávka	k1gFnPc1	nahrávka
<g/>
,	,	kIx,	,
být	být	k5eAaImF	být
aktivní	aktivní	k2eAgInSc4d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Jste	být	k5eAaImIp2nP	být
šikovnější	šikovný	k2eAgFnPc1d2	šikovnější
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
orientujete	orientovat	k5eAaBmIp2nP	orientovat
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
jste	být	k5eAaImIp2nP	být
zodpovědnější	zodpovědný	k2eAgFnPc1d2	zodpovědnější
<g/>
.	.	kIx.	.
</s>
<s>
Najednou	najednou	k6eAd1	najednou
tomu	ten	k3xDgNnSc3	ten
teď	teď	k6eAd1	teď
lépe	dobře	k6eAd2	dobře
rozumíte	rozumět	k5eAaImIp2nP	rozumět
a	a	k8xC	a
snažíte	snažit	k5eAaImIp2nP	snažit
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechno	všechen	k3xTgNnSc1	všechen
klape	klapat	k5eAaImIp3nS	klapat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
kapely	kapela	k1gFnSc2	kapela
byli	být	k5eAaImAgMnP	být
Jon	Jon	k1gMnSc1	Jon
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
a	a	k8xC	a
Richie	Richie	k1gFnSc1	Richie
Sambora	Sambora	k1gFnSc1	Sambora
požádáni	požádat	k5eAaPmNgMnP	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomohli	pomoct	k5eAaPmAgMnP	pomoct
jako	jako	k8xC	jako
producenti	producent	k1gMnPc1	producent
zpěvačce	zpěvačka	k1gFnSc3	zpěvačka
Cher	Cher	k1gInSc1	Cher
<g/>
,	,	kIx,	,
při	při	k7c6	při
vydávání	vydávání	k1gNnSc6	vydávání
jejího	její	k3xOp3gNnSc2	její
eponymního	eponymní	k2eAgNnSc2d1	eponymní
alba	album	k1gNnSc2	album
Cher	Chera	k1gFnPc2	Chera
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Pomohli	pomoct	k5eAaPmAgMnP	pomoct
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
We	We	k1gMnSc1	We
All	All	k1gMnSc1	All
Sleep	Sleep	k1gMnSc1	Sleep
Alone	Alon	k1gMnSc5	Alon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgNnSc3	který
poté	poté	k6eAd1	poté
nazpívali	nazpívat	k5eAaBmAgMnP	nazpívat
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
vokály	vokál	k1gInPc4	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
produkovali	produkovat	k5eAaImAgMnP	produkovat
její	její	k3xOp3gNnSc4	její
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
Heart	Hearta	k1gFnPc2	Hearta
of	of	k?	of
Stone	ston	k1gInSc5	ston
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
získalo	získat	k5eAaPmAgNnS	získat
multi-platinové	multilatinový	k2eAgNnSc1d1	multi-platinové
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnuti	rozhodnut	k2eAgMnPc1d1	rozhodnut
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
úspěch	úspěch	k1gInSc1	úspěch
třetího	třetí	k4xOgNnSc2	třetí
alba	album	k1gNnSc2	album
nebyla	být	k5eNaImAgFnS	být
jen	jen	k9	jen
šťastná	šťastný	k2eAgFnSc1d1	šťastná
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
stále	stále	k6eAd1	stále
hard	hard	k6eAd1	hard
rockoví	rockový	k2eAgMnPc1d1	rockový
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc6	Jov
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
album	album	k1gNnSc4	album
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
září	září	k1gNnSc6	září
1988	[number]	k4	1988
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
komerčním	komerční	k2eAgInSc7d1	komerční
hitem	hit	k1gInSc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
celé	celý	k2eAgInPc4d1	celý
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
strávilo	strávit	k5eAaPmAgNnS	strávit
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
britské	britský	k2eAgFnSc2d1	britská
hitparády	hitparáda	k1gFnSc2	hitparáda
UK	UK	kA	UK
Albums	Albums	k1gInSc4	Albums
Chart	charta	k1gFnPc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
přineslo	přinést	k5eAaPmAgNnS	přinést
pět	pět	k4xCc4	pět
singlů	singl	k1gInPc2	singl
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
vešly	vejít	k5eAaPmAgInP	vejít
do	do	k7c2	do
Top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
rekord	rekord	k1gInSc4	rekord
mezi	mezi	k7c4	mezi
hard	harda	k1gFnPc2	harda
rockovými	rockový	k2eAgNnPc7d1	rockové
alby	album	k1gNnPc7	album
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
byly	být	k5eAaImAgInP	být
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Bad	Bad	k1gMnSc5	Bad
Medicine	Medicin	k1gMnSc5	Medicin
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Be	Be	k1gMnSc5	Be
There	Ther	k1gMnSc5	Ther
for	forum	k1gNnPc2	forum
You	You	k1gFnSc1	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
postupně	postupně	k6eAd1	postupně
vystřídaly	vystřídat	k5eAaPmAgInP	vystřídat
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
Born	Born	k1gMnSc1	Born
to	ten	k3xDgNnSc4	ten
Be	Be	k1gMnSc1	Be
My	my	k3xPp1nPc1	my
Baby	baby	k1gNnPc3	baby
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lay	Lay	k1gMnSc1	Lay
Your	Your	k1gMnSc1	Your
Hands	Hands	k1gInSc4	Hands
on	on	k3xPp3gMnSc1	on
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Living	Living	k1gInSc1	Living
in	in	k?	in
Sin	sin	kA	sin
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
velkými	velký	k2eAgInPc7d1	velký
hity	hit	k1gInPc7	hit
na	na	k7c6	na
MTV	MTV	kA	MTV
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
k	k	k7c3	k
singlu	singl	k1gInSc3	singl
"	"	kIx"	"
<g/>
Living	Living	k1gInSc1	Living
in	in	k?	in
Sin	sin	kA	sin
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
získalo	získat	k5eAaPmAgNnS	získat
další	další	k2eAgNnSc4d1	další
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
kapely	kapela	k1gFnSc2	kapela
jako	jako	k9	jako
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
MTV	MTV	kA	MTV
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
pro	pro	k7c4	pro
příliš	příliš	k6eAd1	příliš
řízné	řízný	k2eAgFnPc4d1	řízná
romantické	romantický	k2eAgFnPc4d1	romantická
a	a	k8xC	a
sexuální	sexuální	k2eAgFnPc4d1	sexuální
scény	scéna	k1gFnPc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
znovu	znovu	k6eAd1	znovu
a	a	k8xC	a
MTV	MTV	kA	MTV
jej	on	k3xPp3gInSc4	on
často	často	k6eAd1	často
vysílala	vysílat	k5eAaImAgFnS	vysílat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
veleúspěšné	veleúspěšný	k2eAgNnSc1d1	veleúspěšné
i	i	k9	i
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Čelo	čelo	k1gNnSc1	čelo
místních	místní	k2eAgFnPc2d1	místní
hitparád	hitparáda	k1gFnPc2	hitparáda
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
na	na	k7c6	na
starém	starý	k2eAgInSc6d1	starý
kontinentě	kontinent	k1gInSc6	kontinent
pak	pak	k6eAd1	pak
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
již	již	k6eAd1	již
zmíněném	zmíněný	k2eAgNnSc6d1	zmíněné
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
se	se	k3xPyFc4	se
vešlo	vejít	k5eAaPmAgNnS	vejít
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Jersey	Jersea	k1gFnPc4	Jersea
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
dvou	dva	k4xCgMnPc6	dva
deskové	deskový	k2eAgNnSc1d1	deskové
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vydavatel	vydavatel	k1gMnSc1	vydavatel
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
neúspěchu	neúspěch	k1gInSc2	neúspěch
při	při	k7c6	při
příliš	příliš	k6eAd1	příliš
vysoké	vysoký	k2eAgFnSc3d1	vysoká
ceně	cena	k1gFnSc3	cena
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
album	album	k1gNnSc4	album
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
<g/>
.	.	kIx.	.
</s>
<s>
Kapele	kapela	k1gFnSc3	kapela
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
17	[number]	k4	17
nevydaných	vydaný	k2eNgFnPc2d1	nevydaná
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
na	na	k7c6	na
pozdějších	pozdní	k2eAgInPc6d2	pozdější
albech	album	k1gNnPc6	album
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
nebyly	být	k5eNaImAgFnP	být
dosud	dosud	k6eAd1	dosud
vydány	vydat	k5eAaPmNgFnP	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
mnoho	mnoho	k4c4	mnoho
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
sedminásobné	sedminásobný	k2eAgNnSc4d1	sedminásobné
platinové	platinový	k2eAgNnSc4d1	platinové
ocenění	ocenění	k1gNnSc4	ocenění
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
7	[number]	k4	7
000	[number]	k4	000
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
a	a	k8xC	a
pětinásobné	pětinásobný	k2eAgFnPc4d1	pětinásobná
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
milionem	milion	k4xCgInSc7	milion
prodaných	prodaný	k2eAgNnPc2d1	prodané
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
platinových	platinový	k2eAgFnPc6d1	platinová
deskách	deska	k1gFnPc6	deska
získalo	získat	k5eAaPmAgNnS	získat
album	album	k1gNnSc1	album
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvních	první	k4xOgInPc6	první
úspěších	úspěch	k1gInPc6	úspěch
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1989	[number]	k4	1989
a	a	k8xC	a
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
navštívili	navštívit	k5eAaPmAgMnP	navštívit
22	[number]	k4	22
států	stát	k1gInPc2	stát
a	a	k8xC	a
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c6	na
232	[number]	k4	232
koncertech	koncert	k1gInPc6	koncert
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejzajímavější	zajímavý	k2eAgMnSc1d3	nejzajímavější
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
jistě	jistě	k9	jistě
koncert	koncert	k1gInSc4	koncert
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1989	[number]	k4	1989
před	před	k7c7	před
úplně	úplně	k6eAd1	úplně
vyprodaným	vyprodaný	k2eAgInSc7d1	vyprodaný
Giants	Giants	k1gInSc1	Giants
Stadionem	stadion	k1gInSc7	stadion
doma	doma	k6eAd1	doma
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
kapela	kapela	k1gFnSc1	kapela
navštívila	navštívit	k5eAaPmAgFnS	navštívit
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vedla	vést	k5eAaImAgFnS	vést
festival	festival	k1gInSc4	festival
Moscow	Moscow	k1gMnPc2	Moscow
Music	Music	k1gMnSc1	Music
Peace	Peace	k1gFnSc2	Peace
Festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
staly	stát	k5eAaPmAgFnP	stát
první	první	k4xOgFnPc1	první
kapelou	kapela	k1gFnSc7	kapela
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Ozzy	Ozz	k1gMnPc4	Ozz
Osbournem	Osbourn	k1gInSc7	Osbourn
<g/>
,	,	kIx,	,
Skid	Skid	k1gMnSc1	Skid
Row	Row	k1gMnSc1	Row
<g/>
,	,	kIx,	,
Mötley	Mötle	k2eAgFnPc1d1	Mötle
Crüe	Crü	k1gFnPc1	Crü
<g/>
,	,	kIx,	,
Scorpions	Scorpions	k1gInSc1	Scorpions
<g/>
,	,	kIx,	,
Cinderella	Cinderella	k1gFnSc1	Cinderella
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
vystoupení	vystoupení	k1gNnSc1	vystoupení
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
povoleno	povolit	k5eAaPmNgNnS	povolit
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
album	album	k1gNnSc4	album
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
bylo	být	k5eAaImAgNnS	být
legálně	legálně	k6eAd1	legálně
vydáno	vydat	k5eAaPmNgNnS	vydat
ruským	ruský	k2eAgNnSc7d1	ruské
státním	státní	k2eAgNnSc7d1	státní
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Melodiya	Melodiy	k1gInSc2	Melodiy
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
nepovedlo	povést	k5eNaPmAgNnS	povést
žádnému	žádný	k3yNgMnSc3	žádný
západnímu	západní	k2eAgMnSc3d1	západní
umělci	umělec	k1gMnSc3	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
Monsters	Monsters	k1gInSc1	Monsters
of	of	k?	of
Rock	rock	k1gInSc1	rock
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
rok	rok	k1gInSc4	rok
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
při	pře	k1gFnSc4	pře
vystoupení	vystoupení	k1gNnSc2	vystoupení
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
dva	dva	k4xCgMnPc1	dva
fanoušci	fanoušek	k1gMnPc1	fanoušek
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c6	na
jiném	jiný	k2eAgInSc6d1	jiný
rockovém	rockový	k2eAgInSc6d1	rockový
festivalu	festival	k1gInSc6	festival
v	v	k7c4	v
Milton	Milton	k1gInSc4	Milton
Keynes	Keynesa	k1gFnPc2	Keynesa
společně	společně	k6eAd1	společně
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
Europe	Europ	k1gInSc5	Europ
<g/>
,	,	kIx,	,
Skid	Skid	k1gInSc4	Skid
Row	Row	k1gFnPc2	Row
a	a	k8xC	a
Vixen	Vixna	k1gFnPc2	Vixna
<g/>
.	.	kIx.	.
</s>
<s>
Nepřetržité	přetržitý	k2eNgNnSc1d1	nepřetržité
koncertování	koncertování	k1gNnSc1	koncertování
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
si	se	k3xPyFc3	se
na	na	k7c6	na
kapele	kapela	k1gFnSc6	kapela
vybralo	vybrat	k5eAaPmAgNnS	vybrat
svou	svůj	k3xOyFgFnSc4	svůj
daň	daň	k1gFnSc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
turné	turné	k1gNnSc2	turné
za	za	k7c7	za
sebou	se	k3xPyFc7	se
měli	mít	k5eAaImAgMnP	mít
16	[number]	k4	16
měsíců	měsíc	k1gInPc2	měsíc
nepřetržitého	přetržitý	k2eNgNnSc2d1	nepřetržité
vystupování	vystupování	k1gNnSc2	vystupování
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
byli	být	k5eAaImAgMnP	být
úplně	úplně	k6eAd1	úplně
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
sérii	série	k1gFnSc6	série
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
pouze	pouze	k6eAd1	pouze
odjeli	odjet	k5eAaPmAgMnP	odjet
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgInPc2	jakýkoliv
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dvouletému	dvouletý	k2eAgNnSc3d1	dvouleté
období	období	k1gNnSc3	období
nečinnosti	nečinnost	k1gFnSc2	nečinnost
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990	[number]	k4	1990
až	až	k8xS	až
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
kapela	kapela	k1gFnSc1	kapela
v	v	k7c6	v
nečinnosti	nečinnost	k1gFnSc6	nečinnost
<g/>
.	.	kIx.	.
</s>
<s>
Vyčerpání	vyčerpání	k1gNnSc4	vyčerpání
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
alb	alba	k1gFnPc2	alba
Slippery	Slippera	k1gFnSc2	Slippera
When	When	k1gMnSc1	When
Wet	Wet	k1gMnSc1	Wet
a	a	k8xC	a
New	New	k1gMnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
vydaných	vydaný	k2eAgMnPc2d1	vydaný
hned	hned	k6eAd1	hned
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yIgMnPc6	který
následovala	následovat	k5eAaImAgFnS	následovat
dvě	dva	k4xCgNnPc4	dva
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
turné	turné	k1gNnPc4	turné
si	se	k3xPyFc3	se
vybralo	vybrat	k5eAaPmAgNnS	vybrat
svou	svůj	k3xOyFgFnSc4	svůj
daň	daň	k1gFnSc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
pouze	pouze	k6eAd1	pouze
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
rozloučili	rozloučit	k5eAaPmAgMnP	rozloučit
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
svou	svůj	k3xOyFgFnSc7	svůj
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
přestávky	přestávka	k1gFnSc2	přestávka
začali	začít	k5eAaPmAgMnP	začít
členové	člen	k1gMnPc1	člen
vydávat	vydávat	k5eAaPmF	vydávat
sólová	sólový	k2eAgNnPc1d1	sólové
alba	album	k1gNnPc1	album
<g/>
.	.	kIx.	.
</s>
<s>
Jon	Jon	k?	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
vydal	vydat	k5eAaPmAgInS	vydat
své	svůj	k3xOyFgNnSc4	svůj
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Blaze	blaze	k6eAd1	blaze
of	of	k?	of
Glory	Glora	k1gFnSc2	Glora
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
také	také	k6eAd1	také
soundtrackem	soundtrack	k1gInSc7	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Mladé	mladý	k2eAgFnSc2d1	mladá
pušky	puška	k1gFnSc2	puška
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
ho	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Emilio	Emilio	k1gMnSc1	Emilio
Estevez	Estevez	k1gMnSc1	Estevez
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
propůjčení	propůjčení	k1gNnSc4	propůjčení
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Wanted	Wanted	k1gMnSc1	Wanted
Dead	Dead	k1gMnSc1	Dead
or	or	k?	or
Alive	Aliev	k1gFnSc2	Aliev
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
uvážení	uvážení	k1gNnSc6	uvážení
Jon	Jon	k1gFnSc2	Jon
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
filmu	film	k1gInSc2	film
nehodí	hodit	k5eNaPmIp3nS	hodit
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
pro	pro	k7c4	pro
film	film	k1gInSc4	film
nahrál	nahrát	k5eAaPmAgInS	nahrát
nový	nový	k2eAgInSc1d1	nový
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nahrávání	nahrávání	k1gNnSc6	nahrávání
byli	být	k5eAaImAgMnP	být
pozváni	pozván	k2eAgMnPc1d1	pozván
speciální	speciální	k2eAgMnPc1d1	speciální
hosté	host	k1gMnPc1	host
jako	jako	k8xC	jako
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
Little	Little	k1gFnSc1	Little
Richard	Richard	k1gMnSc1	Richard
a	a	k8xC	a
Jeff	Jeff	k1gMnSc1	Jeff
Beck	Beck	k1gMnSc1	Beck
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgInSc1d1	titulní
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Blaze	blaze	k6eAd1	blaze
of	of	k?	of
Glory	Glora	k1gFnSc2	Glora
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
album	album	k1gNnSc4	album
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
pozici	pozice	k1gFnSc6	pozice
hitparády	hitparáda	k1gFnSc2	hitparáda
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
a	a	k8xC	a
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
UK	UK	kA	UK
Albums	Albumsa	k1gFnPc2	Albumsa
Chart	charta	k1gFnPc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
skladba	skladba	k1gFnSc1	skladba
získala	získat	k5eAaPmAgFnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
American	Americana	k1gFnPc2	Americana
Music	Music	k1gMnSc1	Music
Award	Award	k1gMnSc1	Award
za	za	k7c4	za
nejoblíbenější	oblíbený	k2eAgInSc4d3	nejoblíbenější
pop	pop	k1gInSc4	pop
<g/>
/	/	kIx~	/
<g/>
rockový	rockový	k2eAgInSc4d1	rockový
singl	singl	k1gInSc4	singl
a	a	k8xC	a
přinesla	přinést	k5eAaPmAgFnS	přinést
Jon	Jon	k1gFnSc1	Jon
Bon	bon	k1gInSc1	bon
Jovimu	Jovima	k1gFnSc4	Jovima
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc1	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
skladbu	skladba	k1gFnSc4	skladba
získal	získat	k5eAaPmAgMnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
a	a	k8xC	a
Cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Richie	Richie	k1gFnSc1	Richie
Sambora	Sambora	k1gFnSc1	Sambora
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
přátel	přítel	k1gMnPc2	přítel
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Tico	Tico	k1gMnSc1	Tico
Torresem	Torres	k1gInSc7	Torres
a	a	k8xC	a
Davidem	David	k1gMnSc7	David
Bryanem	Bryan	k1gMnSc7	Bryan
vydal	vydat	k5eAaPmAgMnS	vydat
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Stranger	Stranger	k1gMnSc1	Stranger
In	In	k1gMnSc1	In
This	Thisa	k1gFnPc2	Thisa
Town	Town	k1gMnSc1	Town
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
představilo	představit	k5eAaPmAgNnS	představit
také	také	k9	také
Erica	Eric	k2eAgMnSc4d1	Eric
Clamtona	Clamton	k1gMnSc4	Clamton
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnPc6	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bluesman	bluesman	k1gMnSc1	bluesman
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
a	a	k8xC	a
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
UK	UK	kA	UK
Albums	Albumsa	k1gFnPc2	Albumsa
Chart	charta	k1gFnPc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Rosie	Rosi	k1gMnPc4	Rosi
<g/>
"	"	kIx"	"
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
vydána	vydán	k2eAgFnSc1d1	vydána
na	na	k7c6	na
albu	album	k1gNnSc6	album
Bon	bona	k1gFnPc2	bona
Jovi	Jove	k1gFnSc4	Jove
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
vydavatele	vydavatel	k1gMnSc2	vydavatel
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
albu	album	k1gNnSc6	album
celého	celý	k2eAgNnSc2d1	celé
dvojalba	dvojalbum	k1gNnSc2	dvojalbum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Bryan	Bryan	k1gMnSc1	Bryan
natočil	natočit	k5eAaBmAgMnS	natočit
soundtrack	soundtrack	k1gInSc4	soundtrack
k	k	k7c3	k
hororovému	hororový	k2eAgInSc3d1	hororový
snímku	snímek	k1gInSc3	snímek
The	The	k1gFnSc2	The
Netherworld	Netherworld	k1gInSc1	Netherworld
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
roku	rok	k1gInSc2	rok
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
hospitalizován	hospitalizován	k2eAgInSc1d1	hospitalizován
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nakazil	nakazit	k5eAaPmAgMnS	nakazit
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
jihoamerickou	jihoamerický	k2eAgFnSc7d1	jihoamerická
chorobou	choroba	k1gFnSc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Alec	Alec	k6eAd1	Alec
John	John	k1gMnSc1	John
Such	sucho	k1gNnPc2	sucho
spadnul	spadnout	k5eAaPmAgInS	spadnout
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
motorky	motorka	k1gFnSc2	motorka
a	a	k8xC	a
poranil	poranit	k5eAaPmAgMnS	poranit
si	se	k3xPyFc3	se
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
musel	muset	k5eAaImAgInS	muset
poté	poté	k6eAd1	poté
vymyslet	vymyslet	k5eAaPmF	vymyslet
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
držení	držení	k1gNnSc2	držení
své	svůj	k3xOyFgFnSc2	svůj
basové	basový	k2eAgFnSc2d1	basová
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
mohl	moct	k5eAaImAgMnS	moct
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
veškerý	veškerý	k3xTgInSc4	veškerý
svůj	svůj	k3xOyFgInSc4	svůj
úspěch	úspěch	k1gInSc4	úspěch
byl	být	k5eAaImAgMnS	být
Jon	Jon	k1gMnSc1	Jon
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
nespokojený	spokojený	k2eNgInSc4d1	nespokojený
se	se	k3xPyFc4	se
současným	současný	k2eAgInSc7d1	současný
stavem	stav	k1gInSc7	stav
svého	svůj	k3xOyFgNnSc2	svůj
hudebního	hudební	k2eAgNnSc2d1	hudební
business	business	k1gInSc4	business
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
propustil	propustit	k5eAaPmAgMnS	propustit
veškerý	veškerý	k3xTgInSc4	veškerý
svůj	svůj	k3xOyFgInSc4	svůj
managment	managment	k1gInSc4	managment
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dlouholetého	dlouholetý	k2eAgMnSc2d1	dlouholetý
manažera	manažer	k1gMnSc2	manažer
Doca	Docus	k1gMnSc2	Docus
McGhee	McGhe	k1gMnSc2	McGhe
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
přebral	přebrat	k5eAaPmAgMnS	přebrat
celou	celý	k2eAgFnSc4d1	celá
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
když	když	k8xS	když
založil	založit	k5eAaPmAgMnS	založit
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
Management	management	k1gInSc1	management
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
kapela	kapela	k1gFnSc1	kapela
odebrala	odebrat	k5eAaPmAgFnS	odebrat
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
spolu	spolu	k6eAd1	spolu
prodiskutovali	prodiskutovat	k5eAaPmAgMnP	prodiskutovat
plány	plán	k1gInPc4	plán
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
problémy	problém	k1gInPc4	problém
vyřešili	vyřešit	k5eAaPmAgMnP	vyřešit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechali	nechat	k5eAaPmAgMnP	nechat
každého	každý	k3xTgMnSc4	každý
člena	člen	k1gMnSc4	člen
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
pocitech	pocit	k1gInPc6	pocit
a	a	k8xC	a
problémech	problém	k1gInPc6	problém
bez	bez	k7c2	bez
narušování	narušování	k1gNnSc2	narušování
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vyřešily	vyřešit	k5eAaPmAgInP	vyřešit
všechny	všechen	k3xTgInPc1	všechen
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
odebrali	odebrat	k5eAaPmAgMnP	odebrat
se	se	k3xPyFc4	se
do	do	k7c2	do
kanadského	kanadský	k2eAgInSc2d1	kanadský
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Bobem	Bob	k1gMnSc7	Bob
Rockem	rock	k1gInSc7	rock
natočili	natočit	k5eAaBmAgMnP	natočit
své	svůj	k3xOyFgFnPc4	svůj
páté	pátá	k1gFnPc4	pátá
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
Páté	pátá	k1gFnSc2	pátá
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
s	s	k7c7	s
názvem	název	k1gInSc7	název
Keep	Keep	k1gInSc4	Keep
the	the	k?	the
Faith	Faitha	k1gFnPc2	Faitha
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
skupina	skupina	k1gFnSc1	skupina
představila	představit	k5eAaPmAgFnS	představit
jako	jako	k9	jako
začátek	začátek	k1gInSc4	začátek
nové	nový	k2eAgFnSc2d1	nová
kapitoly	kapitola	k1gFnSc2	kapitola
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
<g/>
,	,	kIx,	,
od	od	k7c2	od
posledního	poslední	k2eAgNnSc2d1	poslední
alba	album	k1gNnSc2	album
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
změnila	změnit	k5eAaPmAgFnS	změnit
styl	styl	k1gInSc4	styl
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
změnili	změnit	k5eAaPmAgMnP	změnit
svůj	svůj	k3xOyFgInSc4	svůj
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
nápadně	nápadně	k6eAd1	nápadně
změnili	změnit	k5eAaPmAgMnP	změnit
svůj	svůj	k3xOyFgInSc4	svůj
účes	účes	k1gInSc4	účes
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
ostříhali	ostříhat	k5eAaPmAgMnP	ostříhat
na	na	k7c4	na
krátko	krátko	k6eAd1	krátko
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
páté	pátý	k4xOgFnSc6	pátý
pozici	pozice	k1gFnSc6	pozice
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
bylo	být	k5eAaImAgNnS	být
oceněno	oceněn	k2eAgNnSc1d1	oceněno
dvěma	dva	k4xCgFnPc7	dva
platinovými	platinový	k2eAgFnPc7d1	platinová
deskami	deska	k1gFnPc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInSc4d2	veliký
úspěch	úspěch	k1gInSc4	úspěch
si	se	k3xPyFc3	se
album	album	k1gNnSc1	album
vydobylo	vydobýt	k5eAaPmAgNnS	vydobýt
na	na	k7c6	na
starém	starý	k2eAgInSc6d1	starý
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
UK	UK	kA	UK
Albums	Albumsa	k1gFnPc2	Albumsa
Chart	charta	k1gFnPc2	charta
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
první	první	k4xOgFnSc6	první
pozici	pozice	k1gFnSc6	pozice
a	a	k8xC	a
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
slavilo	slavit	k5eAaImAgNnS	slavit
také	také	k9	také
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Pětinásobné	pětinásobný	k2eAgNnSc1d1	pětinásobné
platinové	platinový	k2eAgNnSc1d1	platinové
ocenění	ocenění	k1gNnSc1	ocenění
získalo	získat	k5eAaPmAgNnS	získat
i	i	k9	i
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
platinových	platinový	k2eAgFnPc6d1	platinová
deskách	deska	k1gFnPc6	deska
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vedlo	vést	k5eAaImAgNnS	vést
na	na	k7c6	na
asijských	asijský	k2eAgInPc6d1	asijský
i	i	k8xC	i
jihoamerických	jihoamerický	k2eAgInPc6d1	jihoamerický
trzích	trh	k1gInPc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
přineslo	přinést	k5eAaPmAgNnS	přinést
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc1	šest
singlů	singl	k1gInPc2	singl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Keep	Keep	k1gInSc1	Keep
the	the	k?	the
Faith	Faith	k1gInSc1	Faith
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Bed	Beda	k1gFnPc2	Beda
of	of	k?	of
Roses	Roses	k1gMnSc1	Roses
<g/>
"	"	kIx"	"
-	-	kIx~	-
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
desáté	desátý	k4xOgNnSc4	desátý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
In	In	k1gFnSc1	In
These	these	k1gFnSc2	these
Arms	Armsa	k1gFnPc2	Armsa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Sleep	Sleep	k1gInSc1	Sleep
When	When	k1gMnSc1	When
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Dead	Dead	k1gInSc1	Dead
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	i	k9	i
Believe	Belieev	k1gFnSc2	Belieev
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Dry	Dry	k1gFnSc1	Dry
County	Counta	k1gFnSc2	Counta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
alb	album	k1gNnPc2	album
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gInSc1	Unplugged
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
epizoda	epizoda	k1gFnSc1	epizoda
byla	být	k5eAaImAgFnS	být
jiná	jiný	k2eAgFnSc1d1	jiná
než	než	k8xS	než
obvykle	obvykle	k6eAd1	obvykle
<g/>
.	.	kIx.	.
</s>
<s>
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
poprvé	poprvé	k6eAd1	poprvé
naživo	naživo	k1gNnSc1	naživo
zahráli	zahrát	k5eAaPmAgMnP	zahrát
některé	některý	k3yIgInPc4	některý
klasické	klasický	k2eAgInPc4d1	klasický
hity	hit	k1gInPc4	hit
(	(	kIx(	(
<g/>
některé	některý	k3yIgNnSc1	některý
svoje	své	k1gNnSc1	své
<g/>
,	,	kIx,	,
některé	některý	k3yIgNnSc1	některý
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
autorů	autor	k1gMnPc2	autor
<g/>
)	)	kIx)	)
akusticky	akusticky	k6eAd1	akusticky
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
elektronické	elektronický	k2eAgFnSc6d1	elektronická
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ještě	ještě	k9	ještě
předvedli	předvést	k5eAaPmAgMnP	předvést
část	část	k1gFnSc4	část
skladeb	skladba	k1gFnPc2	skladba
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
Keep	Keep	k1gMnSc1	Keep
the	the	k?	the
Faith	Faith	k1gMnSc1	Faith
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
vyšel	vyjít	k5eAaPmAgInS	vyjít
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Keep	Keep	k1gMnSc1	Keep
the	the	k?	the
Faith	Faith	k1gMnSc1	Faith
<g/>
:	:	kIx,	:
An	An	k1gFnSc1	An
Evening	Evening	k1gInSc1	Evening
with	with	k1gInSc1	with
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potvrzení	potvrzení	k1gNnSc4	potvrzení
nové	nový	k2eAgFnSc2d1	nová
éry	éra	k1gFnSc2	éra
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
kapela	kapela	k1gFnSc1	kapela
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nemělo	mít	k5eNaImAgNnS	mít
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
historii	historie	k1gFnSc6	historie
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
navštívila	navštívit	k5eAaPmAgFnS	navštívit
38	[number]	k4	38
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
177	[number]	k4	177
koncertech	koncert	k1gInPc6	koncert
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
s	s	k7c7	s
názvem	název	k1gInSc7	název
Keep	Keep	k1gMnSc1	Keep
the	the	k?	the
Faith	Faith	k1gMnSc1	Faith
Tour	Tour	k1gMnSc1	Tour
a	a	k8xC	a
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Sleep	Sleep	k1gInSc1	Sleep
When	When	k1gMnSc1	When
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Dead	Dead	k1gMnSc1	Dead
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1993	[number]	k4	1993
navštívili	navštívit	k5eAaPmAgMnP	navštívit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
druhého	druhý	k4xOgNnSc2	druhý
jmenovaného	jmenovaný	k2eAgNnSc2d1	jmenované
turné	turné	k1gNnSc2	turné
také	také	k9	také
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
strahovském	strahovský	k2eAgInSc6d1	strahovský
stadionu	stadion	k1gInSc6	stadion
společně	společně	k6eAd1	společně
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Billy	Bill	k1gMnPc4	Bill
Idolem	idol	k1gInSc7	idol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
kompilační	kompilační	k2eAgNnSc1d1	kompilační
album	album	k1gNnSc1	album
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
s	s	k7c7	s
názvem	název	k1gInSc7	název
Cross	Crossa	k1gFnPc2	Crossa
Road	Roada	k1gFnPc2	Roada
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejúspěšnější	úspěšný	k2eAgInPc4d3	nejúspěšnější
singly	singl	k1gInPc4	singl
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
11	[number]	k4	11
let	léto	k1gNnPc2	léto
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
nové	nový	k2eAgInPc1d1	nový
singly	singl	k1gInPc1	singl
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Someday	Somedaa	k1gFnPc4	Somedaa
I	i	k8xC	i
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Be	Be	k1gMnSc1	Be
Saturday	Saturdaa	k1gFnSc2	Saturdaa
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Always	Always	k1gInSc1	Always
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prvnímu	první	k4xOgInSc3	první
singlu	singl	k1gInSc3	singl
se	se	k3xPyFc4	se
vedlo	vést	k5eAaImAgNnS	vést
nejlépe	dobře	k6eAd3	dobře
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obsadil	obsadit	k5eAaPmAgMnS	obsadit
sedmou	sedmý	k4xOgFnSc4	sedmý
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
UK	UK	kA	UK
Singles	Singlesa	k1gFnPc2	Singlesa
Chart	charta	k1gFnPc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Always	Always	k1gInSc1	Always
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
mnohem	mnohem	k6eAd1	mnohem
úspěšnější	úspěšný	k2eAgMnSc1d2	úspěšnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
obrovským	obrovský	k2eAgInSc7d1	obrovský
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
udržel	udržet	k5eAaPmAgMnS	udržet
šest	šest	k4xCc1	šest
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
vydržel	vydržet	k5eAaPmAgMnS	vydržet
i	i	k9	i
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
získal	získat	k5eAaPmAgInS	získat
samotný	samotný	k2eAgInSc4d1	samotný
singl	singl	k1gInSc4	singl
dvě	dva	k4xCgNnPc4	dva
ocenění	ocenění	k1gNnPc4	ocenění
platinové	platinový	k2eAgFnSc2d1	platinová
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
singlem	singl	k1gInSc7	singl
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
skupiny	skupina	k1gFnSc2	skupina
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
obdržela	obdržet	k5eAaPmAgFnS	obdržet
ocenění	ocenění	k1gNnSc4	ocenění
World	Worlda	k1gFnPc2	Worlda
Music	Music	k1gMnSc1	Music
Award	Award	k1gMnSc1	Award
jako	jako	k8xC	jako
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
rocková	rockový	k2eAgFnSc1d1	rocková
kapela	kapela	k1gFnSc1	kapela
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
kapelu	kapela	k1gFnSc4	kapela
opustil	opustit	k5eAaPmAgMnS	opustit
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
baskytarista	baskytarista	k1gMnSc1	baskytarista
Alec	Alec	k1gFnSc4	Alec
John	John	k1gMnSc1	John
Such	sucho	k1gNnPc2	sucho
a	a	k8xC	a
sestava	sestava	k1gFnSc1	sestava
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
se	se	k3xPyFc4	se
tak	tak	k9	tak
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
změnila	změnit	k5eAaPmAgFnS	změnit
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
výměny	výměna	k1gFnSc2	výměna
kytaristů	kytarista	k1gMnPc2	kytarista
hned	hned	k6eAd1	hned
vzápětí	vzápětí	k6eAd1	vzápětí
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
stanoviska	stanovisko	k1gNnSc2	stanovisko
kapely	kapela	k1gFnSc2	kapela
nebyl	být	k5eNaImAgInS	být
Such	sucho	k1gNnPc2	sucho
nikdy	nikdy	k6eAd1	nikdy
nahrazen	nahradit	k5eAaPmNgInS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Neoficiálně	neoficiálně	k6eAd1	neoficiálně
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
zastoupil	zastoupit	k5eAaPmAgMnS	zastoupit
Hugh	Hugh	k1gMnSc1	Hugh
McDonald	McDonald	k1gMnSc1	McDonald
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
Jon	Jon	k1gFnSc7	Jon
Bon	bona	k1gFnPc2	bona
Jovim	Jovima	k1gFnPc2	Jovima
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
singlu	singl	k1gInSc6	singl
"	"	kIx"	"
<g/>
Runaway	Runawaa	k1gFnSc2	Runawaa
<g/>
"	"	kIx"	"
a	a	k8xC	a
kolovaly	kolovat	k5eAaImAgFnP	kolovat
pověsti	pověst	k1gFnPc1	pověst
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
i	i	k9	i
na	na	k7c6	na
předchozích	předchozí	k2eAgNnPc6d1	předchozí
albech	album	k1gNnPc6	album
(	(	kIx(	(
<g/>
v	v	k7c6	v
diskografii	diskografie	k1gFnSc6	diskografie
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
uvádí	uvádět	k5eAaImIp3nS	uvádět
všechna	všechen	k3xTgNnPc4	všechen
předchozí	předchozí	k2eAgNnPc4d1	předchozí
alba	album	k1gNnPc4	album
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
7800	[number]	k4	7800
<g/>
°	°	k?	°
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
baskytaristy	baskytarista	k1gMnSc2	baskytarista
Aleca	Alecus	k1gMnSc2	Alecus
Johna	John	k1gMnSc2	John
Sucha	sucho	k1gNnSc2	sucho
se	se	k3xPyFc4	se
Jon	Jon	k1gFnSc1	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jove	k1gFnSc4	Jove
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
raní	ranit	k5eAaPmIp3nS	ranit
<g/>
.	.	kIx.	.
</s>
<s>
Naučil	naučit	k5eAaPmAgMnS	naučit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
žít	žít	k5eAaImF	žít
a	a	k8xC	a
respektuji	respektovat	k5eAaImIp1nS	respektovat
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
workoholik	workoholik	k1gMnSc1	workoholik
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
na	na	k7c4	na
jeviště	jeviště	k1gNnSc4	jeviště
a	a	k8xC	a
zase	zase	k9	zase
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
chci	chtít	k5eAaImIp1nS	chtít
se	se	k3xPyFc4	se
hudbě	hudba	k1gFnSc3	hudba
věnovat	věnovat	k5eAaPmF	věnovat
ve	v	k7c4	v
dne	den	k1gInSc2	den
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
chápu	chápat	k5eAaImIp1nS	chápat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
každý	každý	k3xTgMnSc1	každý
je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
toto	tento	k3xDgNnSc4	tento
tempo	tempo	k1gNnSc4	tempo
udržet	udržet	k5eAaPmF	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Alec	Alec	k1gInSc1	Alec
chtěl	chtít	k5eAaImAgInS	chtít
kapelu	kapela	k1gFnSc4	kapela
opustit	opustit	k5eAaPmF	opustit
už	už	k6eAd1	už
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
není	být	k5eNaImIp3nS	být
úplné	úplný	k2eAgNnSc4d1	úplné
překvapení	překvapení	k1gNnSc4	překvapení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
šesté	šestý	k4xOgNnSc1	šestý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
These	these	k1gFnSc2	these
Days	Days	k1gInSc1	Days
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vydali	vydat	k5eAaPmAgMnP	vydat
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
bez	bez	k7c2	bez
Aleca	Alec	k1gInSc2	Alec
Sucha	sucho	k1gNnSc2	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
dobře	dobře	k6eAd1	dobře
přijato	přijmout	k5eAaPmNgNnS	přijmout
kritiky	kritik	k1gMnPc7	kritik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
adresu	adresa	k1gFnSc4	adresa
poznamenali	poznamenat	k5eAaPmAgMnP	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
zase	zase	k9	zase
posunula	posunout	k5eAaPmAgFnS	posunout
v	v	k7c6	v
textech	text	k1gInPc6	text
a	a	k8xC	a
zkouší	zkoušet	k5eAaImIp3nP	zkoušet
nové	nový	k2eAgInPc4d1	nový
hudební	hudební	k2eAgInPc4d1	hudební
styly	styl	k1gInPc4	styl
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
svůj	svůj	k3xOyFgInSc4	svůj
výraz	výraz	k1gInSc4	výraz
si	se	k3xPyFc3	se
bezpochyby	bezpochyby	k6eAd1	bezpochyby
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
skončilo	skončit	k5eAaPmAgNnS	skončit
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
hitparády	hitparáda	k1gFnSc2	hitparáda
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
platinové	platinový	k2eAgNnSc1d1	platinové
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
úspěšnější	úspěšný	k2eAgNnSc4d2	úspěšnější
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
čela	čelo	k1gNnSc2	čelo
hitparád	hitparáda	k1gFnPc2	hitparáda
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
i	i	k9	i
první	první	k4xOgNnSc1	první
místo	místo	k1gNnSc1	místo
evropské	evropský	k2eAgFnSc2d1	Evropská
obdoby	obdoba	k1gFnSc2	obdoba
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
European	European	k1gInSc4	European
Albums	Albums	k1gInSc4	Albums
Chart	charta	k1gFnPc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Čelo	čelo	k1gNnSc1	čelo
hitparády	hitparáda	k1gFnSc2	hitparáda
dobylo	dobýt	k5eAaPmAgNnS	dobýt
album	album	k1gNnSc1	album
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
přineslo	přinést	k5eAaPmAgNnS	přinést
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc1	pět
nových	nový	k2eAgInPc2d1	nový
singlů	singl	k1gInPc2	singl
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
This	This	k1gInSc1	This
Ain	Ain	k1gFnSc2	Ain
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
a	a	k8xC	a
Love	lov	k1gInSc5	lov
Song	song	k1gInSc1	song
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Something	Something	k1gInSc1	Something
for	forum	k1gNnPc2	forum
the	the	k?	the
Pain	Pain	k1gMnSc1	Pain
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lie	Lie	k1gMnSc4	Lie
to	ten	k3xDgNnSc1	ten
Me	Me	k1gFnPc1	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
These	these	k1gFnSc1	these
Days	Daysa	k1gFnPc2	Daysa
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Hey	Hey	k1gFnSc1	Hey
God	God	k1gMnSc1	God
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc1	první
čtyři	čtyři	k4xCgMnPc4	čtyři
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
UK	UK	kA	UK
Singles	Singlesa	k1gFnPc2	Singlesa
Chart	charta	k1gFnPc2	charta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvíc	nejvíc	k6eAd1	nejvíc
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
BRIT	Brit	k1gMnSc1	Brit
Award	Award	k1gMnSc1	Award
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kapela	kapela	k1gFnSc1	kapela
a	a	k8xC	a
také	také	k9	také
MTV	MTV	kA	MTV
Europe	Europ	k1gInSc5	Europ
Music	Musice	k1gInPc2	Musice
Award	Award	k1gInSc4	Award
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
rockovou	rockový	k2eAgFnSc4d1	rocková
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
po	po	k7c6	po
albu	album	k1gNnSc6	album
These	these	k1gFnSc2	these
Days	Daysa	k1gFnPc2	Daysa
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
skupina	skupina	k1gFnSc1	skupina
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
přes	přes	k7c4	přes
Asii	Asie	k1gFnSc4	Asie
<g/>
,	,	kIx,	,
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc4	Austrálie
až	až	k9	až
k	k	k7c3	k
prvnímu	první	k4xOgNnSc3	první
vystoupení	vystoupení	k1gNnPc1	vystoupení
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Afirce	Afirka	k1gFnSc6	Afirka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
turné	turné	k1gNnSc6	turné
zažila	zažít	k5eAaPmAgFnS	zažít
kapela	kapela	k1gFnSc1	kapela
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
když	když	k8xS	když
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
třikrát	třikrát	k6eAd1	třikrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
před	před	k7c7	před
naprosto	naprosto	k6eAd1	naprosto
vyprodaným	vyprodaný	k2eAgInSc7d1	vyprodaný
stadionem	stadion	k1gInSc7	stadion
ve	v	k7c4	v
Wembley	Wemble	k1gMnPc4	Wemble
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
koncertů	koncert	k1gInPc2	koncert
byly	být	k5eAaImAgFnP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
záznamy	záznam	k1gInPc1	záznam
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
později	pozdě	k6eAd2	pozdě
vyšly	vyjít	k5eAaPmAgInP	vyjít
jako	jako	k8xS	jako
Bon	bon	k1gInSc1	bon
Jovi	Jovi	k1gNnSc2	Jovi
<g/>
:	:	kIx,	:
Live	Live	k1gFnSc1	Live
from	from	k1gMnSc1	from
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
získalo	získat	k5eAaPmAgNnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
překročení	překročení	k1gNnSc4	překročení
svého	svůj	k3xOyFgInSc2	svůj
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
rekordu	rekord	k1gInSc2	rekord
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
<g/>
.	.	kIx.	.
</s>
<s>
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gMnPc1	Jov
navštívili	navštívit	k5eAaPmAgMnP	navštívit
při	při	k7c6	při
turné	turné	k1gNnSc6	turné
35	[number]	k4	35
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
celkem	celkem	k6eAd1	celkem
126	[number]	k4	126
<g/>
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
turné	turné	k1gNnSc6	turné
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
These	these	k1gFnSc2	these
Days	Daysa	k1gFnPc2	Daysa
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
opět	opět	k6eAd1	opět
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
tentokrát	tentokrát	k6eAd1	tentokrát
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
všech	všecek	k3xTgInPc2	všecek
členů	člen	k1gInPc2	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
dvouleté	dvouletý	k2eAgFnSc6d1	dvouletá
pauze	pauza	k1gFnSc6	pauza
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
každý	každý	k3xTgMnSc1	každý
věnovat	věnovat	k5eAaImF	věnovat
vlastní	vlastní	k2eAgFnSc3d1	vlastní
sólové	sólový	k2eAgFnSc3d1	sólová
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
Jon	Jon	k1gFnSc1	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
objevil	objevit	k5eAaPmAgInS	objevit
hned	hned	k6eAd1	hned
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přestávkách	přestávka	k1gFnPc6	přestávka
mezi	mezi	k7c7	mezi
filmováním	filmování	k1gNnSc7	filmování
napsal	napsat	k5eAaBmAgMnS	napsat
a	a	k8xC	a
nahrál	nahrát	k5eAaPmAgMnS	nahrát
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
druhém	druhý	k4xOgNnSc6	druhý
sólovém	sólový	k2eAgNnSc6d1	sólové
albu	album	k1gNnSc6	album
Destination	Destination	k1gInSc1	Destination
Anywhere	Anywher	k1gInSc5	Anywher
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Krátký	krátký	k2eAgInSc1d1	krátký
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
ku	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgMnSc1d1	postavený
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
skladbách	skladba	k1gFnPc6	skladba
toho	ten	k3xDgNnSc2	ten
alba	album	k1gNnSc2	album
a	a	k8xC	a
s	s	k7c7	s
obsazením	obsazení	k1gNnSc7	obsazení
Jon	Jon	k1gFnSc2	Jon
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
<g/>
,	,	kIx,	,
Demi	Dem	k1gMnSc5	Dem
Moore	Moor	k1gMnSc5	Moor
<g/>
,	,	kIx,	,
Kevin	Kevin	k2eAgInSc4d1	Kevin
Bacon	Bacon	k1gInSc4	Bacon
a	a	k8xC	a
Whoopi	Whoope	k1gFnSc4	Whoope
Goldberg	Goldberg	k1gMnSc1	Goldberg
<g/>
.	.	kIx.	.
</s>
<s>
Tico	Tico	k1gMnSc1	Tico
Torres	Torres	k1gMnSc1	Torres
využil	využít	k5eAaPmAgMnS	využít
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
především	především	k6eAd1	především
své	svůj	k3xOyFgFnSc3	svůj
další	další	k2eAgFnSc3d1	další
zálibě	záliba	k1gFnSc3	záliba
-	-	kIx~	-
malování	malování	k1gNnSc1	malování
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Bryan	Bryan	k1gMnSc1	Bryan
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
a	a	k8xC	a
skládat	skládat	k5eAaImF	skládat
muzikály	muzikál	k1gInPc4	muzikál
a	a	k8xC	a
Richie	Richie	k1gFnSc2	Richie
Sambora	Sambora	k1gFnSc1	Sambora
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Undiscovered	Undiscovered	k1gMnSc1	Undiscovered
Soul	Soul	k1gInSc1	Soul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
spojila	spojit	k5eAaPmAgFnS	spojit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
natočila	natočit	k5eAaBmAgFnS	natočit
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Real	Real	k1gInSc1	Real
Life	Lif	k1gInSc2	Lif
<g/>
"	"	kIx"	"
k	k	k7c3	k
filmu	film	k1gInSc3	film
EDtv	EDtva	k1gFnPc2	EDtva
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
Bryana	Bryan	k1gMnSc2	Bryan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zotavoval	zotavovat	k5eAaImAgMnS	zotavovat
ze	z	k7c2	z
zranění	zranění	k1gNnSc2	zranění
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
si	se	k3xPyFc3	se
málem	málem	k6eAd1	málem
uřízl	uříznout	k5eAaPmAgMnS	uříznout
prst	prst	k1gInSc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
kapela	kapela	k1gFnSc1	kapela
připravena	připraven	k2eAgFnSc1d1	připravena
vydat	vydat	k5eAaPmF	vydat
další	další	k2eAgNnSc4d1	další
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
pod	pod	k7c7	pod
pracovním	pracovní	k2eAgInSc7d1	pracovní
názvem	název	k1gInSc7	název
Sex	sex	k1gInSc1	sex
Sells	Sellsa	k1gFnPc2	Sellsa
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gNnSc1	jeho
nahrávání	nahrávání	k1gNnSc1	nahrávání
bylo	být	k5eAaImAgNnS	být
pozastaveno	pozastavit	k5eAaPmNgNnS	pozastavit
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
z	z	k7c2	z
nahraných	nahraný	k2eAgNnPc2d1	nahrané
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
třiceti	třicet	k4xCc2	třicet
skladeb	skladba	k1gFnPc2	skladba
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
albu	album	k1gNnSc6	album
Crush	Crusha	k1gFnPc2	Crusha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tříletém	tříletý	k2eAgNnSc6d1	tříleté
období	období	k1gNnSc6	období
nečinnosti	nečinnost	k1gFnSc2	nečinnost
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
opět	opět	k6eAd1	opět
znovu	znovu	k6eAd1	znovu
seskupila	seskupit	k5eAaPmAgFnS	seskupit
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
sedmém	sedmý	k4xOgNnSc6	sedmý
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Crush	Crusha	k1gFnPc2	Crusha
<g/>
.	.	kIx.	.
</s>
<s>
Pilotním	pilotní	k2eAgInSc7d1	pilotní
singlem	singl	k1gInSc7	singl
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Life	Lifus	k1gMnSc5	Lifus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgInPc2d3	nejúspěšnější
singlů	singl	k1gInPc2	singl
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
singl	singl	k1gInSc1	singl
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
také	také	k9	také
symbolem	symbol	k1gInSc7	symbol
dlouhověkosti	dlouhověkost	k1gFnSc2	dlouhověkost
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přes	přes	k7c4	přes
změnu	změna	k1gFnSc4	změna
doby	doba	k1gFnSc2	doba
stále	stále	k6eAd1	stále
oslovovala	oslovovat	k5eAaImAgFnS	oslovovat
mnoho	mnoho	k4c4	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgNnSc7d1	nové
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
laděno	laděn	k2eAgNnSc1d1	laděno
spíše	spíše	k9	spíše
v	v	k7c4	v
pop	pop	k1gInSc4	pop
rockovém	rockový	k2eAgInSc6d1	rockový
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
oslovila	oslovit	k5eAaPmAgFnS	oslovit
novou	nový	k2eAgFnSc4d1	nová
generaci	generace	k1gFnSc4	generace
mladých	mladý	k2eAgMnPc2d1	mladý
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
získala	získat	k5eAaPmAgFnS	získat
kapela	kapela	k1gFnSc1	kapela
dvě	dva	k4xCgFnPc1	dva
nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
Cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
rockové	rockový	k2eAgNnSc1d1	rockové
album	album	k1gNnSc1	album
za	za	k7c4	za
Crush	Crush	k1gInSc4	Crush
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
rockové	rockový	k2eAgNnSc1d1	rockové
vystoupení	vystoupení	k1gNnSc1	vystoupení
duetu	duet	k1gInSc2	duet
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnSc2	skupina
za	za	k7c4	za
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
It	It	k1gFnSc4	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Life	Life	k1gFnSc3	Life
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Albu	alba	k1gFnSc4	alba
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vedlo	vést	k5eAaImAgNnS	vést
na	na	k7c6	na
starém	starý	k2eAgInSc6d1	starý
kontinentě	kontinent	k1gInSc6	kontinent
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
nežli	nežli	k8xS	nežli
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
<g/>
"	"	kIx"	"
deváté	devátý	k4xOgNnSc4	devátý
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
většiny	většina	k1gFnSc2	většina
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
dominovalo	dominovat	k5eAaImAgNnS	dominovat
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
evropské	evropský	k2eAgFnSc2d1	Evropská
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
příčky	příčka	k1gFnPc4	příčka
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
také	také	k9	také
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
albu	album	k1gNnSc3	album
vedlo	vést	k5eAaImAgNnS	vést
(	(	kIx(	(
<g/>
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
<g/>
)	)	kIx)	)
také	také	k9	také
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
získalo	získat	k5eAaPmAgNnS	získat
album	album	k1gNnSc4	album
dvě	dva	k4xCgNnPc4	dva
platinová	platinový	k2eAgNnPc4d1	platinové
ocenění	ocenění	k1gNnPc4	ocenění
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgInPc2d1	další
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
těchto	tento	k3xDgNnPc2	tento
ocenění	ocenění	k1gNnPc2	ocenění
dočkal	dočkat	k5eAaPmAgMnS	dočkat
i	i	k9	i
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
It	It	k1gFnSc4	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Life	Life	k1gFnSc3	Life
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
dvěma	dva	k4xCgFnPc7	dva
singly	singl	k1gInPc1	singl
alba	album	k1gNnSc2	album
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
Say	Say	k1gMnPc1	Say
It	It	k1gMnSc1	It
Isn	Isn	k1gMnSc1	Isn
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
So	So	kA	So
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Thank	Thank	k1gInSc1	Thank
You	You	k1gFnPc2	You
for	forum	k1gNnPc2	forum
Loving	Loving	k1gInSc1	Loving
Me	Me	k1gFnSc3	Me
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
turné	turné	k1gNnSc6	turné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiná	k1gFnPc4	jiná
představili	představit	k5eAaPmAgMnP	představit
na	na	k7c6	na
starém	staré	k1gNnSc6	staré
Wembley	Wemblea	k1gFnSc2	Wemblea
stadionu	stadion	k1gInSc2	stadion
jako	jako	k8xC	jako
poslední	poslední	k2eAgInSc4d1	poslední
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
uzavřením	uzavření	k1gNnSc7	uzavření
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
demolicí	demolice	k1gFnSc7	demolice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
přes	přes	k7c4	přes
Japonsko	Japonsko	k1gNnSc4	Japonsko
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
a	a	k8xC	a
poté	poté	k6eAd1	poté
znovu	znovu	k6eAd1	znovu
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2001	[number]	k4	2001
zahrála	zahrát	k5eAaPmAgFnS	zahrát
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
vyprodaných	vyprodaný	k2eAgInPc6d1	vyprodaný
stadionech	stadion	k1gInPc6	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
opět	opět	k6eAd1	opět
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
kolo	kolo	k1gNnSc4	kolo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
zakončili	zakončit	k5eAaPmAgMnP	zakončit
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
dvěma	dva	k4xCgInPc7	dva
koncerty	koncert	k1gInPc7	koncert
před	před	k7c7	před
vyprodaným	vyprodaný	k2eAgInSc7d1	vyprodaný
Giants	Giants	k1gInSc1	Giants
Stadionem	stadion	k1gInSc7	stadion
doma	doma	k6eAd1	doma
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
turné	turné	k1gNnSc6	turné
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
koncertní	koncertní	k2eAgNnSc4d1	koncertní
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
One	One	k1gMnSc1	One
Wild	Wild	k1gMnSc1	Wild
Night	Night	k1gMnSc1	Night
Live	Live	k1gInSc4	Live
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kolekci	kolekce	k1gFnSc4	kolekce
živých	živý	k1gMnPc2	živý
vystoupení	vystoupení	k1gNnSc2	vystoupení
kapely	kapela	k1gFnSc2	kapela
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
My	my	k3xPp1nPc1	my
VH1	VH1	k1gFnSc2	VH1
Music	Music	k1gMnSc1	Music
Award	Award	k1gMnSc1	Award
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2001	[number]	k4	2001
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Nejžhavější	žhavý	k2eAgNnSc1d3	nejžhavější
živé	živý	k2eAgNnSc1d1	živé
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
udělování	udělování	k1gNnSc6	udělování
cen	cena	k1gFnPc2	cena
Jon	Jon	k1gFnSc1	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
a	a	k8xC	a
Richie	Richie	k1gFnSc2	Richie
Sambora	Sambora	k1gFnSc1	Sambora
překvapili	překvapit	k5eAaPmAgMnP	překvapit
všechny	všechen	k3xTgMnPc4	všechen
návštěvníky	návštěvník	k1gMnPc4	návštěvník
včetně	včetně	k7c2	včetně
televizních	televizní	k2eAgMnPc2d1	televizní
komentátorů	komentátor	k1gMnPc2	komentátor
krásným	krásný	k2eAgNnSc7d1	krásné
ztvárněním	ztvárnění	k1gNnSc7	ztvárnění
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Here	Here	k1gInSc1	Here
Comes	Comes	k1gInSc1	Comes
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Georgi	Georg	k1gMnSc3	Georg
Harrisonovi	Harrison	k1gMnSc3	Harrison
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
skončila	skončit	k5eAaPmAgFnS	skončit
turné	turné	k1gNnSc4	turné
po	po	k7c4	po
vydání	vydání	k1gNnSc4	vydání
alba	album	k1gNnSc2	album
Crush	Crusha	k1gFnPc2	Crusha
respektive	respektive	k9	respektive
One	One	k1gMnSc1	One
Wild	Wild	k1gMnSc1	Wild
Night	Night	k1gMnSc1	Night
<g/>
,	,	kIx,	,
očekávali	očekávat	k5eAaImAgMnP	očekávat
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
krátkou	krátký	k2eAgFnSc4d1	krátká
dovolenou	dovolená	k1gFnSc4	dovolená
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
osmém	osmý	k4xOgInSc6	osmý
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
přišlo	přijít	k5eAaPmAgNnS	přijít
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
navždy	navždy	k6eAd1	navždy
změnilo	změnit	k5eAaPmAgNnS	změnit
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jon	Jon	k?	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
a	a	k8xC	a
Richie	Richie	k1gFnSc2	Richie
Sambora	Sambora	k1gFnSc1	Sambora
natočili	natočit	k5eAaBmAgMnP	natočit
veřejná	veřejný	k2eAgNnPc4d1	veřejné
oznámení	oznámení	k1gNnPc4	oznámení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
červeného	červený	k2eAgInSc2d1	červený
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
natočili	natočit	k5eAaBmAgMnP	natočit
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
America	Americ	k2eAgFnSc1d1	America
The	The	k1gFnSc1	The
Beautiful	Beautifula	k1gFnPc2	Beautifula
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
NFL	NFL	kA	NFL
a	a	k8xC	a
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
jako	jako	k8xC	jako
jedni	jeden	k4xCgMnPc1	jeden
z	z	k7c2	z
účinkujících	účinkující	k1gMnPc2	účinkující
na	na	k7c6	na
benefičním	benefiční	k2eAgInSc6d1	benefiční
koncertu	koncert	k1gInSc6	koncert
America	Americ	k1gInSc2	Americ
<g/>
:	:	kIx,	:
A	a	k9	a
Tribe	Trib	k1gInSc5	Trib
to	ten	k3xDgNnSc4	ten
Heroes	Heroes	k1gInSc1	Heroes
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
kapela	kapela	k1gFnSc1	kapela
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
dvou	dva	k4xCgInPc6	dva
benefičních	benefiční	k2eAgInPc6d1	benefiční
koncertech	koncert	k1gInPc6	koncert
v	v	k7c6	v
Red	Red	k1gFnSc6	Red
Bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
výdělky	výdělek	k1gInPc1	výdělek
byly	být	k5eAaImAgInP	být
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
rodiny	rodina	k1gFnPc4	rodina
postižené	postižený	k2eAgFnPc4d1	postižená
katastrofou	katastrofa	k1gFnSc7	katastrofa
ve	v	k7c6	v
Světovém	světový	k2eAgNnSc6d1	světové
obchodním	obchodní	k2eAgNnSc6d1	obchodní
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
kapela	kapela	k1gFnSc1	kapela
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
na	na	k7c4	na
The	The	k1gFnSc4	The
Concert	Concert	k1gInSc1	Concert
for	forum	k1gNnPc2	forum
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
vzdávali	vzdávat	k5eAaImAgMnP	vzdávat
hold	hold	k1gInSc4	hold
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
záchranných	záchranný	k2eAgFnPc6d1	záchranná
akcích	akce	k1gFnPc6	akce
po	po	k7c6	po
útocích	útok	k1gInPc6	útok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
natočila	natočit	k5eAaBmAgFnS	natočit
své	svůj	k3xOyFgNnSc4	svůj
osmé	osmý	k4xOgNnSc4	osmý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Bounce	Bounec	k1gInPc1	Bounec
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
silně	silně	k6eAd1	silně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
útoky	útok	k1gInPc4	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Bounce	Bounec	k1gInSc2	Bounec
<g/>
"	"	kIx"	"
odráží	odrážet	k5eAaImIp3nS	odrážet
schopnost	schopnost	k1gFnSc1	schopnost
Newyorčanů	Newyorčan	k1gMnPc2	Newyorčan
a	a	k8xC	a
celých	celý	k2eAgInPc2d1	celý
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
vzpamatovat	vzpamatovat	k5eAaPmF	vzpamatovat
se	se	k3xPyFc4	se
z	z	k7c2	z
nedávných	dávný	k2eNgInPc2d1	nedávný
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
Bounce	Bounec	k1gInSc2	Bounec
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
kořenům	kořen	k1gInPc3	kořen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Sambora	Sambora	k1gFnSc1	Sambora
hraje	hrát	k5eAaImIp3nS	hrát
velmi	velmi	k6eAd1	velmi
"	"	kIx"	"
<g/>
přebuzeně	přebuzeně	k6eAd1	přebuzeně
<g/>
"	"	kIx"	"
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
Jon	Jon	k1gFnSc4	Jon
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
zpívá	zpívat	k5eAaImIp3nS	zpívat
opět	opět	k6eAd1	opět
chraplavým	chraplavý	k2eAgInSc7d1	chraplavý
hlasem	hlas	k1gInSc7	hlas
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
klávesové	klávesový	k2eAgInPc4d1	klávesový
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
Bryanova	Bryanův	k2eAgFnSc1d1	Bryanova
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
piano	piano	k1gNnSc4	piano
a	a	k8xC	a
Torresovy	Torresův	k2eAgInPc4d1	Torresův
silné	silný	k2eAgInPc4d1	silný
bicí	bicí	k2eAgInPc4d1	bicí
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2002	[number]	k4	2002
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gMnPc1	Jov
odstartovali	odstartovat	k5eAaPmAgMnP	odstartovat
novou	nový	k2eAgFnSc4d1	nová
sezónu	sezóna	k1gFnSc4	sezóna
NFL	NFL	kA	NFL
hodinovým	hodinový	k2eAgInSc7d1	hodinový
koncertem	koncert	k1gInSc7	koncert
na	na	k7c4	na
Times	Times	k1gInSc4	Times
Square	square	k1gInSc4	square
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zahráli	zahrát	k5eAaPmAgMnP	zahrát
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
nadcházejícího	nadcházející	k2eAgNnSc2d1	nadcházející
alba	album	k1gNnSc2	album
"	"	kIx"	"
<g/>
Everyday	Everydaa	k1gFnSc2	Everydaa
<g/>
"	"	kIx"	"
a	a	k8xC	a
poslechlo	poslechnout	k5eAaPmAgNnS	poslechnout
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
živě	živě	k6eAd1	živě
přes	přes	k7c4	přes
750	[number]	k4	750
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Bounce	Bounec	k1gInPc4	Bounec
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
pozici	pozice	k1gFnSc6	pozice
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
UK	UK	kA	UK
Albums	Albums	k1gInSc4	Albums
Chart	charta	k1gFnPc2	charta
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
začalo	začít	k5eAaPmAgNnS	začít
hned	hned	k6eAd1	hned
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Everyday	Everyday	k1gInPc1	Everyday
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
vešel	vejít	k5eAaPmAgMnS	vejít
se	se	k3xPyFc4	se
do	do	k7c2	do
prvních	první	k4xOgMnPc2	první
pěti	pět	k4xCc2	pět
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
zlaté	zlatý	k2eAgFnSc2d1	zlatá
desky	deska	k1gFnSc2	deska
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
platinové	platinový	k2eAgNnSc4d1	platinové
ocenění	ocenění	k1gNnSc4	ocenění
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
celkově	celkově	k6eAd1	celkově
(	(	kIx(	(
<g/>
samostatně	samostatně	k6eAd1	samostatně
také	také	k9	také
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
přineslo	přinést	k5eAaPmAgNnS	přinést
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
Misunderstood	Misunderstood	k1gInSc4	Misunderstood
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
About	About	k1gMnSc1	About
Lovin	Lovin	k1gMnSc1	Lovin
<g/>
'	'	kIx"	'
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Bounce	Bounec	k1gInPc4	Bounec
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Distance	distance	k1gFnSc2	distance
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
jako	jako	k8xC	jako
poslední	poslední	k2eAgFnSc1d1	poslední
kapela	kapela	k1gFnSc1	kapela
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
zahrála	zahrát	k5eAaPmAgFnS	zahrát
na	na	k7c4	na
Veterans	Veterans	k1gInSc4	Veterans
Stadium	stadium	k1gNnSc1	stadium
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
stadion	stadion	k1gInSc1	stadion
zbořen	zbořit	k5eAaPmNgInS	zbořit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Bounce	Bounec	k1gInSc2	Bounec
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
kompilační	kompilační	k2eAgNnSc1d1	kompilační
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
This	This	k1gInSc1	This
Left	Lefta	k1gFnPc2	Lefta
Feels	Feelsa	k1gFnPc2	Feelsa
Right	Right	k1gInSc1	Right
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnPc1	Jov
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
nově	nově	k6eAd1	nově
nahrány	nahrát	k5eAaPmNgFnP	nahrát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
dvě	dva	k4xCgFnPc1	dva
nové	nový	k2eAgFnPc1d1	nová
skladby	skladba	k1gFnPc1	skladba
"	"	kIx"	"
<g/>
Last	Last	k1gInSc1	Last
Man	Man	k1gMnSc1	Man
Standing	Standing	k1gInSc1	Standing
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Thief	Thief	k1gInSc1	Thief
of	of	k?	of
Hearts	Hearts	k1gInSc1	Hearts
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
box	box	k1gInSc1	box
set	set	k1gInSc1	set
s	s	k7c7	s
názvem	název	k1gInSc7	název
100,000,000	[number]	k4	100,000,000
Bon	bona	k1gFnPc2	bona
Jovi	Jovi	k1gNnSc2	Jovi
Fans	Fansa	k1gFnPc2	Fansa
Can	Can	k1gFnSc7	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Be	Be	k1gMnSc1	Be
Wrong	Wrong	k1gMnSc1	Wrong
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
parafráze	parafráze	k1gFnSc1	parafráze
na	na	k7c4	na
desku	deska	k1gFnSc4	deska
Elvise	Elvis	k1gMnSc2	Elvis
Presleyho	Presley	k1gMnSc2	Presley
50,000,000	[number]	k4	50,000,000
Elvis	Elvis	k1gMnSc1	Elvis
Fans	Fansa	k1gFnPc2	Fansa
Can	Can	k1gMnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Be	Be	k1gMnSc1	Be
Wrong	Wrong	k1gMnSc1	Wrong
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
odráží	odrážet	k5eAaImIp3nS	odrážet
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapela	kapela	k1gFnSc1	kapela
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
oslavovala	oslavovat	k5eAaImAgFnS	oslavovat
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
alb	alba	k1gFnPc2	alba
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
slavila	slavit	k5eAaImAgFnS	slavit
dvacáté	dvacátý	k4xOgInPc1	dvacátý
výročí	výročí	k1gNnPc2	výročí
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Set	set	k1gInSc1	set
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čtyři	čtyři	k4xCgInPc4	čtyři
CD	CD	kA	CD
s	s	k7c7	s
38	[number]	k4	38
dosud	dosud	k6eAd1	dosud
nevydanými	vydaný	k2eNgInPc7d1	nevydaný
a	a	k8xC	a
12	[number]	k4	12
unikátními	unikátní	k2eAgFnPc7d1	unikátní
písněmi	píseň	k1gFnPc7	píseň
<g/>
,	,	kIx,	,
plus	plus	k6eAd1	plus
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Deváté	devátý	k4xOgNnSc1	devátý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Have	Hav	k1gFnSc2	Hav
a	a	k8xC	a
Nice	Nice	k1gFnSc2	Nice
Day	Day	k1gFnSc2	Day
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
a	a	k8xC	a
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
opět	opět	k6eAd1	opět
na	na	k7c6	na
druhých	druhý	k4xOgFnPc6	druhý
pozicích	pozice	k1gFnPc6	pozice
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
Billboard	billboard	k1gInSc1	billboard
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
(	(	kIx(	(
<g/>
UK	UK	kA	UK
Albums	Albumsa	k1gFnPc2	Albumsa
Chart	charta	k1gFnPc2	charta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
čelo	čelo	k1gNnSc1	čelo
patnácti	patnáct	k4xCc2	patnáct
dalších	další	k2eAgFnPc2d1	další
světových	světový	k2eAgFnPc2d1	světová
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Have	Have	k1gFnSc1	Have
a	a	k8xC	a
Nice	Nice	k1gFnSc1	Nice
Day	Day	k1gFnSc2	Day
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
singlem	singl	k1gInSc7	singl
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Who	Who	k1gMnSc1	Who
Says	Saysa	k1gFnPc2	Saysa
You	You	k1gMnSc1	You
Can	Can	k1gMnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Go	Go	k1gFnSc1	Go
Home	Home	k1gFnSc1	Home
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
jedné	jeden	k4xCgFnSc6	jeden
verzi	verze	k1gFnSc6	verze
zpívá	zpívat	k5eAaImIp3nS	zpívat
kapela	kapela	k1gFnSc1	kapela
společně	společně	k6eAd1	společně
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Jennifer	Jennifer	k1gMnSc1	Jennifer
Nettles	Nettles	k1gMnSc1	Nettles
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Sugarland	Sugarlanda	k1gFnPc2	Sugarlanda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Country	country	k2eAgFnSc7d1	country
Chart	charta	k1gFnPc2	charta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
kapele	kapela	k1gFnSc3	kapela
Bon	bona	k1gFnPc2	bona
Jovi	Jovi	k1gNnSc2	Jovi
povedlo	povést	k5eAaPmAgNnS	povést
jako	jako	k8xS	jako
první	první	k4xOgFnSc3	první
rockové	rockový	k2eAgFnSc3d1	rocková
kapele	kapela	k1gFnSc3	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
pak	pak	k6eAd1	pak
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
písní	píseň	k1gFnSc7	píseň
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
a	a	k8xC	a
Jennifer	Jennifer	k1gMnSc1	Jennifer
Nettles	Nettles	k1gMnSc1	Nettles
Cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
pěvecké	pěvecký	k2eAgNnSc4d1	pěvecké
country	country	k2eAgNnSc4d1	country
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
samou	samý	k3xTgFnSc7	samý
skladbou	skladba	k1gFnSc7	skladba
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
i	i	k9	i
cenu	cena	k1gFnSc4	cena
People	People	k1gFnSc2	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Music	Music	k1gMnSc1	Music
Award	Award	k1gMnSc1	Award
za	za	k7c4	za
nejoblíbenější	oblíbený	k2eAgFnSc4d3	nejoblíbenější
rockovou	rockový	k2eAgFnSc4d1	rocková
píseň	píseň	k1gFnSc4	píseň
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
velké	velký	k2eAgNnSc4d1	velké
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
zahrála	zahrát	k5eAaPmAgFnS	zahrát
dvěma	dva	k4xCgFnPc7	dva
milionům	milion	k4xCgInPc3	milion
fanoušků	fanoušek	k1gMnPc2	fanoušek
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
přes	přes	k7c4	přes
191	[number]	k4	191
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
turné	turné	k1gNnSc1	turné
se	s	k7c7	s
131	[number]	k4	131
miliony	milion	k4xCgInPc7	milion
stalo	stát	k5eAaPmAgNnS	stát
třetím	třetí	k4xOgNnSc6	třetí
nejvýdělečnějším	výdělečný	k2eAgNnSc6d3	nejvýdělečnější
turné	turné	k1gNnSc6	turné
roku	rok	k1gInSc2	rok
po	po	k7c6	po
A	A	kA	A
Big	Big	k1gMnSc1	Big
Bang	Bang	k1gMnSc1	Bang
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
kapely	kapela	k1gFnSc2	kapela
The	The	k1gMnPc2	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
a	a	k8xC	a
Confessions	Confessionsa	k1gFnPc2	Confessionsa
Tour	Toura	k1gFnPc2	Toura
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Madonny	Madonna	k1gFnSc2	Madonna
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
kapela	kapela	k1gFnSc1	kapela
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
přijata	přijmout	k5eAaPmNgFnS	přijmout
do	do	k7c2	do
UK	UK	kA	UK
Music	Musice	k1gFnPc2	Musice
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Fame	Fame	k1gFnSc1	Fame
společně	společně	k6eAd1	společně
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Jamesem	James	k1gMnSc7	James
Brownem	Brown	k1gMnSc7	Brown
a	a	k8xC	a
skupinou	skupina	k1gFnSc7	skupina
Led	led	k1gInSc4	led
Zeppelin	Zeppelin	k2eAgInSc4d1	Zeppelin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
své	svůj	k3xOyFgNnSc4	svůj
desáté	desátý	k4xOgNnSc4	desátý
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Lost	Lost	k1gMnSc1	Lost
Highway	Highwaa	k1gMnSc2	Highwaa
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
prvky	prvek	k1gInPc4	prvek
klasického	klasický	k2eAgInSc2d1	klasický
rocku	rock	k1gInSc2	rock
s	s	k7c7	s
country	country	k2eAgFnSc7d1	country
muzikou	muzika	k1gFnSc7	muzika
<g/>
,	,	kIx,	,
při	při	k7c6	při
čemž	což	k3yRnSc6	což
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
úspěchu	úspěch	k1gInSc2	úspěch
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Who	Who	k1gMnSc1	Who
Says	Saysa	k1gFnPc2	Saysa
You	You	k1gMnSc1	You
Can	Can	k1gMnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Go	Go	k1gFnSc1	Go
Home	Home	k1gFnSc1	Home
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
a	a	k8xC	a
čela	čelo	k1gNnSc2	čelo
hitparád	hitparáda	k1gFnPc2	hitparáda
v	v	k7c6	v
několika	několik	k4yIc6	několik
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
deváté	devátý	k4xOgFnSc6	devátý
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
i	i	k9	i
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
Have	Have	k1gFnSc1	Have
a	a	k8xC	a
Nice	Nice	k1gFnSc1	Nice
Day	Day	k1gFnSc1	Day
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Lost	Lost	k1gInSc1	Lost
Highway	Highwaa	k1gFnSc2	Highwaa
získalo	získat	k5eAaPmAgNnS	získat
platinové	platinový	k2eAgNnSc1d1	platinové
ocenění	ocenění	k1gNnSc1	ocenění
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Lost	Lost	k5eAaPmF	Lost
Highway	Highwaa	k1gFnPc1	Highwaa
přineslo	přinést	k5eAaPmAgNnS	přinést
čtyři	čtyři	k4xCgInPc4	čtyři
nové	nový	k2eAgInPc4d1	nový
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
You	You	k1gMnSc1	You
Want	Want	k1gMnSc1	Want
To	ten	k3xDgNnSc4	ten
<g/>
)	)	kIx)	)
Make	Make	k1gNnSc4	Make
a	a	k8xC	a
Memory	Memor	k1gInPc4	Memor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lost	Lost	k1gInSc1	Lost
Highway	Highwaa	k1gFnSc2	Highwaa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Till	Till	k1gMnSc1	Till
We	We	k1gMnSc1	We
Ain	Ain	k1gMnSc1	Ain
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Strangers	Strangers	k1gInSc1	Strangers
Anymore	Anymor	k1gInSc5	Anymor
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Whole	Whole	k1gFnSc1	Whole
Lot	lot	k1gInSc1	lot
Of	Of	k1gMnSc1	Of
Leavin	Leavina	k1gFnPc2	Leavina
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
kapela	kapela	k1gFnSc1	kapela
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
velké	velký	k2eAgNnSc4d1	velké
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
příznačným	příznačný	k2eAgInSc7d1	příznačný
názvem	název	k1gInSc7	název
Lost	Lost	k1gInSc1	Lost
Highway	Highwaa	k1gFnSc2	Highwaa
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
deseti	deset	k4xCc7	deset
koncerty	koncert	k1gInPc7	koncert
v	v	k7c6	v
Newarku	Newark	k1gInSc6	Newark
ku	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
otevření	otevření	k1gNnSc2	otevření
stadionu	stadion	k1gInSc2	stadion
Prudential	Prudential	k1gMnSc1	Prudential
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nynější	nynější	k2eAgInSc1d1	nynější
domácí	domácí	k2eAgInSc1d1	domácí
led	led	k1gInSc1	led
hokejového	hokejový	k2eAgInSc2d1	hokejový
klubu	klub	k1gInSc2	klub
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
Devils	Devilsa	k1gFnPc2	Devilsa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
kapela	kapela	k1gFnSc1	kapela
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
a	a	k8xC	a
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
pak	pak	k6eAd1	pak
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
komerčně	komerčně	k6eAd1	komerčně
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
turné	turné	k1gNnSc4	turné
a	a	k8xC	a
dle	dle	k7c2	dle
časopisu	časopis	k1gInSc2	časopis
Billboard	billboard	k1gInSc1	billboard
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
nejvýdělečnější	výdělečný	k2eAgNnSc1d3	nejvýdělečnější
turné	turné	k1gNnSc1	turné
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
zisky	zisk	k1gInPc1	zisk
přes	přes	k7c4	přes
210	[number]	k4	210
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvěma	dva	k4xCgInPc7	dva
miliony	milion	k4xCgInPc7	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
vyšel	vyjít	k5eAaPmAgInS	vyjít
při	pře	k1gFnSc4	pře
Tribeca	Tribec	k1gInSc2	Tribec
Film	film	k1gInSc1	film
Festivalu	festival	k1gInSc2	festival
dokument	dokument	k1gInSc4	dokument
o	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
s	s	k7c7	s
názvem	název	k1gInSc7	název
When	When	k1gNnSc1	When
We	We	k1gMnPc1	We
Were	Wer	k1gFnSc2	Wer
Beautiful	Beautiful	k1gInSc1	Beautiful
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mapuje	mapovat	k5eAaImIp3nS	mapovat
25	[number]	k4	25
let	léto	k1gNnPc2	léto
působení	působení	k1gNnSc2	působení
kapely	kapela	k1gFnPc1	kapela
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
režiséra	režisér	k1gMnSc2	režisér
Phila	Phil	k1gMnSc2	Phil
Griffina	Griffin	k1gMnSc2	Griffin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byli	být	k5eAaImAgMnP	být
Jon	Jon	k1gMnPc1	Jon
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
a	a	k8xC	a
Richie	Richie	k1gFnSc2	Richie
Sambora	Sambor	k1gMnSc2	Sambor
přijati	přijmout	k5eAaPmNgMnP	přijmout
do	do	k7c2	do
Songwriters	Songwritersa	k1gFnPc2	Songwritersa
Hall	Hallum	k1gNnPc2	Hallum
of	of	k?	of
Fame	Fam	k1gFnSc2	Fam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
rovněž	rovněž	k9	rovněž
nahráli	nahrát	k5eAaBmAgMnP	nahrát
coververzi	coververze	k1gFnSc4	coververze
písně	píseň	k1gFnSc2	píseň
Bena	Bena	k?	Bena
E.	E.	kA	E.
Kinga	Kinga	k1gFnSc1	Kinga
"	"	kIx"	"
<g/>
Stand	Standa	k1gFnPc2	Standa
By	by	k9	by
Me	Me	k1gMnPc2	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
íránským	íránský	k2eAgMnSc7d1	íránský
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Andym	Andym	k1gInSc4	Andym
Madadianem	Madadian	k1gInSc7	Madadian
<g/>
,	,	kIx,	,
části	část	k1gFnPc1	část
písně	píseň	k1gFnPc1	píseň
jsou	být	k5eAaImIp3nP	být
zpívány	zpívat	k5eAaImNgFnP	zpívat
persky	persky	k6eAd1	persky
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jedenácté	jedenáctý	k4xOgNnSc1	jedenáctý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc2	The
Circle	Circl	k1gMnSc2	Circl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
rock	rock	k1gInSc4	rock
n	n	k0	n
<g/>
'	'	kIx"	'
rollu	rollo	k1gNnSc6	rollo
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
předchozí	předchozí	k2eAgInSc4d1	předchozí
Lost	Lost	k1gInSc4	Lost
Highway	Highwaa	k1gFnSc2	Highwaa
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
kombinace	kombinace	k1gFnSc1	kombinace
s	s	k7c7	s
country	country	k2eAgFnSc7d1	country
muzikou	muzika	k1gFnSc7	muzika
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
čelo	čelo	k1gNnSc4	čelo
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
dominovalo	dominovat	k5eAaImAgNnS	dominovat
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
předchozí	předchozí	k2eAgNnPc4d1	předchozí
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
druhou	druhý	k4xOgFnSc4	druhý
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
jedenácté	jedenáctý	k4xOgFnSc6	jedenáctý
pozici	pozice	k1gFnSc6	pozice
hitparády	hitparáda	k1gFnSc2	hitparáda
IFPI	IFPI	kA	IFPI
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
přineslo	přinést	k5eAaPmAgNnS	přinést
tři	tři	k4xCgInPc4	tři
nové	nový	k2eAgInPc4d1	nový
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
We	We	k1gFnSc1	We
Weren	Werna	k1gFnPc2	Werna
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Born	Born	k1gInSc1	Born
to	ten	k3xDgNnSc1	ten
Follow	Follow	k1gFnPc1	Follow
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Superman	superman	k1gMnSc1	superman
Tonight	Tonight	k1gMnSc1	Tonight
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
When	When	k1gNnSc1	When
We	We	k1gFnSc2	We
Were	Wer	k1gMnSc2	Wer
Beautiful	Beautifula	k1gFnPc2	Beautifula
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
zlaté	zlatý	k2eAgFnSc2d1	zlatá
desky	deska	k1gFnSc2	deska
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
opět	opět	k6eAd1	opět
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
koncertní	koncertní	k2eAgFnSc4d1	koncertní
sérii	série	k1gFnSc4	série
The	The	k1gMnSc4	The
Circle	Circle	k1gNnSc7	Circle
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
zahrála	zahrát	k5eAaPmAgFnS	zahrát
na	na	k7c6	na
zbrusu	zbrusu	k6eAd1	zbrusu
novém	nový	k2eAgInSc6d1	nový
New	New	k1gFnPc4	New
Meadowlands	Meadowlands	k1gInSc4	Meadowlands
Stadium	stadium	k1gNnSc1	stadium
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
MetLife	MetLif	k1gInSc5	MetLif
Stadium	stadium	k1gNnSc1	stadium
<g/>
)	)	kIx)	)
v	v	k7c4	v
East	East	k2eAgInSc4d1	East
Ruthenford	Ruthenford	k1gInSc4	Ruthenford
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
domácí	domácí	k2eAgInSc4d1	domácí
stadion	stadion	k1gInSc4	stadion
pro	pro	k7c4	pro
týmy	tým	k1gInPc4	tým
NFL	NFL	kA	NFL
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Giants	Giants	k1gInSc1	Giants
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Jets	Jets	k1gInSc1	Jets
<g/>
.	.	kIx.	.
</s>
<s>
Komerčně	komerčně	k6eAd1	komerčně
byla	být	k5eAaImAgFnS	být
The	The	k1gFnSc1	The
Circle	Circle	k1gFnSc1	Circle
Tour	Tour	k1gInSc4	Tour
opět	opět	k6eAd1	opět
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Billboard	billboard	k1gInSc1	billboard
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nejvýdělečnější	výdělečný	k2eAgNnSc4d3	nejvýdělečnější
turné	turné	k1gNnSc4	turné
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
vydělala	vydělat	k5eAaPmAgFnS	vydělat
přes	přes	k7c4	přes
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
další	další	k2eAgFnSc1d1	další
kompilační	kompilační	k2eAgNnSc4d1	kompilační
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Greatest	Greatest	k1gInSc1	Greatest
Hits	Hitsa	k1gFnPc2	Hitsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc4	čtyři
nové	nový	k2eAgFnPc4d1	nová
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
What	What	k1gMnSc1	What
Do	do	k7c2	do
You	You	k1gFnSc2	You
Got	Got	k1gFnSc2	Got
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
No	no	k9	no
Apologies	Apologies	k1gInSc1	Apologies
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
This	This	k1gInSc1	This
Is	Is	k1gFnSc2	Is
Love	lov	k1gInSc5	lov
This	This	k1gInSc4	This
Is	Is	k1gFnSc3	Is
Life	Life	k1gInSc1	Life
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
More	mor	k1gInSc5	mor
Things	Things	k1gInSc4	Things
Change	change	k1gFnPc4	change
<g/>
"	"	kIx"	"
a	a	k8xC	a
ještě	ještě	k9	ještě
jedna	jeden	k4xCgFnSc1	jeden
bonusová	bonusový	k2eAgFnSc1d1	bonusová
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
This	This	k1gInSc1	This
Is	Is	k1gFnSc2	Is
Our	Our	k1gFnSc2	Our
House	house	k1gNnSc1	house
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
jmenované	jmenovaná	k1gFnPc4	jmenovaná
a	a	k8xC	a
bonusová	bonusový	k2eAgFnSc1d1	bonusová
píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
třemi	tři	k4xCgNnPc7	tři
singly	singl	k1gInPc4	singl
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
pátou	pátý	k4xOgFnSc4	pátý
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
ocenění	ocenění	k1gNnSc1	ocenění
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
tři	tři	k4xCgNnPc4	tři
platinová	platinový	k2eAgNnPc4d1	platinové
ocenění	ocenění	k1gNnPc4	ocenění
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
platinových	platinový	k2eAgNnPc6d1	platinové
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
jedno	jeden	k4xCgNnSc4	jeden
platinové	platinový	k2eAgNnSc4d1	platinové
ocenění	ocenění	k1gNnSc4	ocenění
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
MTV	MTV	kA	MTV
Europe	Europ	k1gInSc5	Europ
Music	Musice	k1gInPc2	Musice
Awards	Awards	k1gInSc4	Awards
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
získala	získat	k5eAaPmAgFnS	získat
kapela	kapela	k1gFnSc1	kapela
ocenění	ocenění	k1gNnSc2	ocenění
Global	globat	k5eAaImAgMnS	globat
Icon	Icon	k1gMnSc1	Icon
Award	Award	k1gMnSc1	Award
<g/>
.	.	kIx.	.
</s>
<s>
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gMnPc1	Jov
vydali	vydat	k5eAaPmAgMnP	vydat
film	film	k1gInSc4	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc2	The
Circle	Circl	k1gMnSc2	Circl
Tour	Tour	k1gMnSc1	Tour
Live	Liv	k1gMnSc2	Liv
From	From	k1gMnSc1	From
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
kapela	kapela	k1gFnSc1	kapela
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k6eAd1	právě
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
písních	píseň	k1gFnPc6	píseň
pro	pro	k7c4	pro
nové	nový	k2eAgNnSc4d1	nové
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jon	Jon	k1gFnSc2	Jon
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1962	[number]	k4	1962
italským	italský	k2eAgMnPc3d1	italský
emigrantům	emigrant	k1gMnPc3	emigrant
v	v	k7c4	v
Perth	Perth	k1gInSc4	Perth
Amboy	Amboa	k1gFnSc2	Amboa
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
první	první	k4xOgInPc1	první
hudební	hudební	k2eAgInPc1d1	hudební
krůčky	krůček	k1gInPc1	krůček
vedly	vést	k5eAaImAgInP	vést
přes	přes	k7c4	přes
školní	školní	k2eAgFnSc4d1	školní
kapelu	kapela	k1gFnSc4	kapela
STARZ	STARZ	kA	STARZ
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
přes	přes	k7c4	přes
kapelu	kapela	k1gFnSc4	kapela
Atlantic	Atlantice	k1gFnPc2	Atlantice
City	City	k1gFnSc2	City
Expessway	Expesswaa	k1gFnSc2	Expesswaa
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
další	další	k2eAgFnPc4d1	další
zastávky	zastávka	k1gFnPc4	zastávka
v	v	k7c6	v
kapelách	kapela	k1gFnPc6	kapela
jako	jako	k8xS	jako
The	The	k1gFnPc6	The
Rest	rest	k6eAd1	rest
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Lechers	Lechersa	k1gFnPc2	Lechersa
a	a	k8xC	a
Jon	Jon	k1gMnSc1	Jon
Bongiovi	Bongius	k1gMnSc3	Bongius
and	and	k?	and
The	The	k1gMnSc1	The
Wild	Wild	k1gMnSc1	Wild
Ones	Ones	k1gInSc4	Ones
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
stálým	stálý	k2eAgMnSc7d1	stálý
frontmanem	frontman	k1gMnSc7	frontman
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
David	David	k1gMnSc1	David
Bryan	Bryan	k1gMnSc1	Bryan
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Bryan	Bryan	k1gMnSc1	Bryan
(	(	kIx(	(
<g/>
narozený	narozený	k2eAgMnSc1d1	narozený
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1962	[number]	k4	1962
v	v	k7c4	v
Edison	Edison	k1gNnSc4	Edison
<g/>
,	,	kIx,	,
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Svoje	své	k1gNnSc1	své
hudební	hudební	k2eAgFnSc2d1	hudební
vlohy	vloha	k1gFnSc2	vloha
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
a	a	k8xC	a
začínal	začínat	k5eAaImAgMnS	začínat
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
trumpetu	trumpeta	k1gFnSc4	trumpeta
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
Transition	Transition	k1gInSc1	Transition
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
zejména	zejména	k9	zejména
na	na	k7c4	na
předělávky	předělávka	k1gFnPc4	předělávka
od	od	k7c2	od
kapel	kapela	k1gFnPc2	kapela
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
a	a	k8xC	a
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tico	Tico	k1gMnSc1	Tico
Torres	Torres	k1gMnSc1	Torres
<g/>
.	.	kIx.	.
</s>
<s>
Tico	Tico	k1gMnSc1	Tico
Torres	Torres	k1gMnSc1	Torres
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1953	[number]	k4	1953
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
kubánských	kubánský	k2eAgMnPc2d1	kubánský
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
hudební	hudební	k2eAgFnSc4d1	hudební
kariéru	kariéra	k1gFnSc4	kariéra
začínal	začínat	k5eAaImAgMnS	začínat
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
Milesa	Milesa	k1gFnSc1	Milesa
Davise	Davise	k1gFnSc1	Davise
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
hrál	hrát	k5eAaImAgMnS	hrát
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Marvelettes	Marvelettesa	k1gFnPc2	Marvelettesa
a	a	k8xC	a
s	s	k7c7	s
Chuckem	Chuck	k1gInSc7	Chuck
Berrym	Berrym	k1gInSc1	Berrym
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Franke	Frank	k1gMnSc5	Frank
and	and	k?	and
the	the	k?	the
Knockouts	Knockouts	k1gInSc1	Knockouts
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Richie	Richie	k1gFnSc2	Richie
Sambora	Sambora	k1gFnSc1	Sambora
<g/>
.	.	kIx.	.
</s>
<s>
Hugh	Hugh	k1gMnSc1	Hugh
McDonald	McDonald	k1gMnSc1	McDonald
(	(	kIx(	(
<g/>
narozený	narozený	k2eAgMnSc1d1	narozený
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1950	[number]	k4	1950
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
<g/>
,	,	kIx,	,
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
baskytarista	baskytarista	k1gMnSc1	baskytarista
hrající	hrající	k2eAgMnSc1d1	hrající
s	s	k7c7	s
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc3	Jov
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
nahrál	nahrát	k5eAaPmAgMnS	nahrát
s	s	k7c7	s
Jonem	Jonum	k1gNnSc7	Jonum
Bon	bon	k1gInSc1	bon
Jovim	Jovim	k1gInSc1	Jovim
singl	singl	k1gInSc4	singl
Runaway	Runawaa	k1gFnSc2	Runawaa
<g/>
.	.	kIx.	.
</s>
<s>
Alec	Alec	k6eAd1	Alec
John	John	k1gMnSc1	John
Such	sucho	k1gNnPc2	sucho
(	(	kIx(	(
<g/>
narozený	narozený	k2eAgInSc1d1	narozený
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1956	[number]	k4	1956
v	v	k7c4	v
Perth	Perth	k1gInSc4	Perth
Amboy	Amboa	k1gMnSc2	Amboa
<g/>
,	,	kIx,	,
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
baskytarista	baskytarista	k1gMnSc1	baskytarista
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Hugh	Hugh	k1gMnSc1	Hugh
McDonald	McDonald	k1gMnSc1	McDonald
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
do	do	k7c2	do
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
měl	mít	k5eAaImAgMnS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
již	již	k6eAd1	již
vystupování	vystupování	k1gNnSc4	vystupování
v	v	k7c6	v
kapelách	kapela	k1gFnPc6	kapela
Phantom	Phantom	k1gInSc1	Phantom
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Opera	opera	k1gFnSc1	opera
a	a	k8xC	a
The	The	k1gFnSc1	The
Message	Message	k1gFnSc1	Message
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
7	[number]	k4	7
<g/>
th	th	k?	th
Heaven	Heavna	k1gFnPc2	Heavna
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Youngem	Young	k1gInSc7	Young
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Styx	Styx	k1gInSc1	Styx
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
<g/>
.	.	kIx.	.
</s>
<s>
Studiová	studiový	k2eAgFnSc1d1	studiová
alba	alba	k1gFnSc1	alba
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
7800	[number]	k4	7800
<g/>
°	°	k?	°
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Slippery	Slippera	k1gFnSc2	Slippera
When	When	k1gMnSc1	When
Wet	Wet	k1gMnSc1	Wet
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Keep	Keep	k1gInSc1	Keep
the	the	k?	the
Faith	Faith	k1gInSc1	Faith
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
These	these	k1gFnSc1	these
Days	Daysa	k1gFnPc2	Daysa
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Crush	Crusha	k1gFnPc2	Crusha
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Bounce	Bounec	k1gInSc2	Bounec
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Have	Hav	k1gInSc2	Hav
a	a	k8xC	a
Nice	Nice	k1gFnSc2	Nice
Day	Day	k1gFnSc2	Day
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Lost	Lost	k1gInSc1	Lost
Highway	Highwaa	k1gFnSc2	Highwaa
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Circle	Circle	k1gFnSc1	Circle
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
What	What	k1gMnSc1	What
About	About	k1gMnSc1	About
Now	Now	k1gMnSc1	Now
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Burning	Burning	k1gInSc1	Burning
Bridges	Bridges	k1gInSc1	Bridges
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
This	This	k1gInSc1	This
House	house	k1gNnSc1	house
Is	Is	k1gFnPc2	Is
Not	nota	k1gFnPc2	nota
for	forum	k1gNnPc2	forum
Sale	Sale	k1gInSc1	Sale
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
