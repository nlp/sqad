<p>
<s>
Oblak	oblak	k1gInSc1	oblak
<g/>
,	,	kIx,	,
též	též	k9	též
mrak	mrak	k1gInSc1	mrak
nebo	nebo	k8xC	nebo
mračno	mračno	k1gNnSc1	mračno
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
p.	p.	k?	p.
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
p.	p.	k?	p.
oblaky	oblak	k1gInPc4	oblak
i	i	k8xC	i
oblaka	oblaka	k1gNnPc4	oblaka
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
p.	p.	k?	p.
oblaků	oblak	k1gInPc2	oblak
i	i	k8xC	i
oblak	oblaka	k1gNnPc2	oblaka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
soustava	soustava	k1gFnSc1	soustava
malých	malý	k2eAgFnPc2d1	malá
částic	částice	k1gFnPc2	částice
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
ledu	led	k1gInSc2	led
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
jiných	jiný	k2eAgFnPc2d1	jiná
látek	látka	k1gFnPc2	látka
<g/>
)	)	kIx)	)
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Země	zem	k1gFnSc2	zem
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Oblaky	oblak	k1gInPc1	oblak
vznikají	vznikat	k5eAaImIp3nP	vznikat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
zkondenzuje	zkondenzovat	k5eAaPmIp3nS	zkondenzovat
na	na	k7c4	na
kapky	kapka	k1gFnPc4	kapka
nebo	nebo	k8xC	nebo
ledové	ledový	k2eAgFnPc4d1	ledová
krystalky	krystalka	k1gFnPc4	krystalka
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
různá	různý	k2eAgFnSc1d1	různá
a	a	k8xC	a
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
plynném	plynný	k2eAgNnSc6d1	plynné
skupenství	skupenství	k1gNnSc6	skupenství
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
kapalinu	kapalina	k1gFnSc4	kapalina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rosný	rosný	k2eAgInSc1d1	rosný
bod	bod	k1gInSc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
stabilitě	stabilita	k1gFnSc6	stabilita
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
přítomné	přítomný	k2eAgFnSc2d1	přítomná
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
oblaková	oblakový	k2eAgFnSc1d1	oblakový
kapka	kapka	k1gFnSc1	kapka
nebo	nebo	k8xC	nebo
ledový	ledový	k2eAgInSc1d1	ledový
krystalek	krystalek	k1gInSc1	krystalek
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
přibližně	přibližně	k6eAd1	přibližně
0,01	[number]	k4	0,01
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgNnPc4d1	studené
oblaka	oblaka	k1gNnPc4	oblaka
tvořící	tvořící	k2eAgFnPc1d1	tvořící
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
výškách	výška	k1gFnPc6	výška
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
ledové	ledový	k2eAgFnPc1d1	ledová
krystalky	krystalka	k1gFnPc1	krystalka
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgNnPc1d2	nižší
<g/>
,	,	kIx,	,
teplejší	teplý	k2eAgNnPc1d2	teplejší
oblaka	oblaka	k1gNnPc1	oblaka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
vodní	vodní	k2eAgFnPc1d1	vodní
kapky	kapka	k1gFnPc1	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
kapalného	kapalný	k2eAgNnSc2d1	kapalné
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
mylný	mylný	k2eAgInSc1d1	mylný
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblaky	oblak	k1gInPc1	oblak
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
samotná	samotný	k2eAgFnSc1d1	samotná
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
je	být	k5eAaImIp3nS	být
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
hustotu	hustota	k1gFnSc4	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Oblaky	oblak	k1gInPc4	oblak
tvoří	tvořit	k5eAaImIp3nS	tvořit
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
nebo	nebo	k8xC	nebo
pevném	pevný	k2eAgNnSc6d1	pevné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblaky	oblak	k1gInPc1	oblak
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
výškou	výška	k1gFnSc7	výška
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
i	i	k8xC	i
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
systému	systém	k1gInSc2	systém
jejich	jejich	k3xOp3gFnSc2	jejich
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
klasifikace	klasifikace	k1gFnSc1	klasifikace
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
Luke	Luke	k1gNnPc2	Luke
Howard	Howarda	k1gFnPc2	Howarda
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
mraků	mrak	k1gInPc2	mrak
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
kombinací	kombinace	k1gFnSc7	kombinace
čtyř	čtyři	k4xCgNnPc2	čtyři
latinských	latinský	k2eAgNnPc2d1	latinské
slov	slovo	k1gNnPc2	slovo
<g/>
:	:	kIx,	:
cirrus	cirrus	k1gInSc1	cirrus
(	(	kIx(	(
<g/>
řasa	řasa	k1gFnSc1	řasa
nebo	nebo	k8xC	nebo
kučera	kučera	k1gFnSc1	kučera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stratus	stratus	k1gInSc1	stratus
(	(	kIx(	(
<g/>
vrstva	vrstva	k1gFnSc1	vrstva
nebo	nebo	k8xC	nebo
sloha	sloha	k1gFnSc1	sloha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nimbus	nimbus	k1gInSc1	nimbus
(	(	kIx(	(
<g/>
déšť	déšť	k1gInSc1	déšť
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
cumulus	cumulus	k1gInSc1	cumulus
(	(	kIx(	(
<g/>
kupa	kupa	k1gFnSc1	kupa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
klasifikace	klasifikace	k1gFnSc1	klasifikace
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
10	[number]	k4	10
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
oblaků	oblak	k1gInPc2	oblak
padají	padat	k5eAaImIp3nP	padat
pevné	pevný	k2eAgFnPc4d1	pevná
či	či	k8xC	či
kapalné	kapalný	k2eAgFnPc4d1	kapalná
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
oblaka	oblaka	k1gNnPc4	oblaka
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
považují	považovat	k5eAaImIp3nP	považovat
i	i	k9	i
přírodní	přírodní	k2eAgInPc4d1	přírodní
nebo	nebo	k8xC	nebo
umělé	umělý	k2eAgInPc4d1	umělý
viditelné	viditelný	k2eAgInPc4d1	viditelný
útvary	útvar	k1gInPc4	útvar
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
mrak	mrak	k1gInSc1	mrak
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
sopky	sopka	k1gFnSc2	sopka
<g/>
,	,	kIx,	,
z	z	k7c2	z
požáru	požár	k1gInSc2	požár
<g/>
,	,	kIx,	,
kondenzační	kondenzační	k2eAgFnPc4d1	kondenzační
stopy	stopa	k1gFnPc4	stopa
po	po	k7c6	po
přeletu	přelet	k1gInSc6	přelet
letadla	letadlo	k1gNnSc2	letadlo
či	či	k8xC	či
atomový	atomový	k2eAgInSc4d1	atomový
hřib	hřib	k1gInSc4	hřib
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
význam	význam	k1gInSc1	význam
oblaků	oblak	k1gInPc2	oblak
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
padá	padat	k5eAaImIp3nS	padat
déšť	déšť	k1gInSc1	déšť
nebo	nebo	k8xC	nebo
sníh	sníh	k1gInSc1	sníh
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc7	jejich
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
kapky	kapka	k1gFnPc1	kapka
vody	voda	k1gFnPc1	voda
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
určitou	určitý	k2eAgFnSc4d1	určitá
minimální	minimální	k2eAgFnSc4d1	minimální
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
nevypařily	vypařit	k5eNaPmAgInP	vypařit
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
nevysublimovaly	vysublimovat	k5eNaPmAgInP	vysublimovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
též	též	k9	též
délka	délka	k1gFnSc1	délka
jejich	jejich	k3xOp3gInSc2	jejich
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
výška	výška	k1gFnSc1	výška
oblaku	oblak	k1gInSc2	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vysokých	vysoký	k2eAgFnPc2d1	vysoká
a	a	k8xC	a
středně	středně	k6eAd1	středně
vysokých	vysoký	k2eAgInPc2d1	vysoký
oblaků	oblak	k1gInPc2	oblak
srážky	srážka	k1gFnSc2	srážka
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
nedopadají	dopadat	k5eNaImIp3nP	dopadat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
vypaří	vypařit	k5eAaPmIp3nP	vypařit
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
dopadem	dopad	k1gInSc7	dopad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
bílé	bílý	k2eAgFnSc3d1	bílá
barvě	barva	k1gFnSc3	barva
oblaky	oblak	k1gInPc7	oblak
výrazně	výrazně	k6eAd1	výrazně
odráží	odrážet	k5eAaImIp3nS	odrážet
dopadající	dopadající	k2eAgNnSc1d1	dopadající
sluneční	sluneční	k2eAgNnSc1d1	sluneční
světlo	světlo	k1gNnSc1	světlo
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
kosmu	kosmos	k1gInSc2	kosmos
a	a	k8xC	a
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
tak	tak	k9	tak
odrazivost	odrazivost	k1gFnSc4	odrazivost
(	(	kIx(	(
<g/>
albedo	albedo	k1gNnSc4	albedo
<g/>
)	)	kIx)	)
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
ukrytý	ukrytý	k2eAgInSc1d1	ukrytý
pod	pod	k7c7	pod
mraky	mrak	k1gInPc7	mrak
méně	málo	k6eAd2	málo
zahříván	zahříván	k2eAgMnSc1d1	zahříván
<g/>
.	.	kIx.	.
</s>
<s>
Odrazivost	odrazivost	k1gFnSc1	odrazivost
jasného	jasný	k2eAgInSc2d1	jasný
oblaku	oblak	k1gInSc2	oblak
je	být	k5eAaImIp3nS	být
až	až	k9	až
0,7	[number]	k4	0,7
<g/>
–	–	k?	–
<g/>
0,9	[number]	k4	0,9
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblak	oblak	k1gInSc1	oblak
odrazí	odrazit	k5eAaPmIp3nS	odrazit
70	[number]	k4	70
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odrazivost	odrazivost	k1gFnSc1	odrazivost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
už	už	k6eAd1	už
jen	jen	k9	jen
čerstvě	čerstvě	k6eAd1	čerstvě
napadaný	napadaný	k2eAgInSc1d1	napadaný
sníh	sníh	k1gInSc1	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
oblaka	oblaka	k1gNnPc1	oblaka
odrazí	odrazit	k5eAaPmIp3nP	odrazit
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
kosmu	kosmos	k1gInSc2	kosmos
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
20	[number]	k4	20
%	%	kIx~	%
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
19	[number]	k4	19
%	%	kIx~	%
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
pozorování	pozorování	k1gNnPc2	pozorování
oblaků	oblak	k1gInPc2	oblak
je	být	k5eAaImIp3nS	být
též	též	k9	též
možné	možný	k2eAgNnSc1d1	možné
odhadnout	odhadnout	k5eAaPmF	odhadnout
vývoj	vývoj	k1gInSc4	vývoj
počasí	počasí	k1gNnSc2	počasí
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
hodinách	hodina	k1gFnPc6	hodina
až	až	k8xS	až
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Oblaky	oblak	k1gInPc1	oblak
vypovídají	vypovídat	k5eAaImIp3nP	vypovídat
mnoho	mnoho	k6eAd1	mnoho
o	o	k7c6	o
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Přesnější	přesný	k2eAgInSc1d2	přesnější
odhad	odhad	k1gInSc1	odhad
počasí	počasí	k1gNnSc2	počasí
je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
udělat	udělat	k5eAaPmF	udělat
jen	jen	k9	jen
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sledováním	sledování	k1gNnSc7	sledování
dalších	další	k2eAgInPc2d1	další
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vlhkosti	vlhkost	k1gFnSc2	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
tlaku	tlak	k1gInSc2	tlak
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Oblaka	oblaka	k1gNnPc1	oblaka
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
skládat	skládat	k5eAaImF	skládat
z	z	k7c2	z
částic	částice	k1gFnPc2	částice
různé	různý	k2eAgFnSc2d1	různá
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
různého	různý	k2eAgInSc2d1	různý
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
přírodních	přírodní	k2eAgInPc2d1	přírodní
oblaků	oblak	k1gInPc2	oblak
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
termodynamické	termodynamický	k2eAgFnPc4d1	termodynamická
podmínky	podmínka	k1gFnPc4	podmínka
v	v	k7c6	v
okolní	okolní	k2eAgFnSc6d1	okolní
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
uvnitř	uvnitř	k7c2	uvnitř
oblaku	oblak	k1gInSc2	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
podmínky	podmínka	k1gFnPc1	podmínka
s	s	k7c7	s
časem	čas	k1gInSc7	čas
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
oblak	oblak	k1gInSc1	oblak
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
oblaku	oblak	k1gInSc2	oblak
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
koloběhu	koloběh	k1gInSc2	koloběh
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
koncentrace	koncentrace	k1gFnSc1	koncentrace
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
100	[number]	k4	100
<g/>
procentní	procentní	k2eAgFnSc6d1	procentní
vlhkosti	vlhkost	k1gFnSc6	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
začne	začít	k5eAaPmIp3nS	začít
stoupat	stoupat	k5eAaImF	stoupat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
z	z	k7c2	z
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
příčin	příčina	k1gFnPc2	příčina
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
kvůli	kvůli	k7c3	kvůli
vyšší	vysoký	k2eAgFnSc3d2	vyšší
teplotě	teplota	k1gFnSc3	teplota
a	a	k8xC	a
proto	proto	k8xC	proto
menší	malý	k2eAgFnSc3d2	menší
hustotě	hustota	k1gFnSc3	hustota
v	v	k7c6	v
porovnaní	porovnaný	k2eAgMnPc1d1	porovnaný
s	s	k7c7	s
chladnějším	chladný	k2eAgInSc7d2	chladnější
<g/>
,	,	kIx,	,
hustějším	hustý	k2eAgInSc7d2	hustší
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
teplý	teplý	k2eAgInSc1d1	teplý
vzduch	vzduch	k1gInSc1	vzduch
vytlačuje	vytlačovat	k5eAaImIp3nS	vytlačovat
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
i	i	k9	i
podél	podél	k7c2	podél
frontální	frontální	k2eAgFnSc2d1	frontální
plochy	plocha	k1gFnSc2	plocha
či	či	k8xC	či
podél	podél	k7c2	podél
terénních	terénní	k2eAgFnPc2d1	terénní
překážek	překážka	k1gFnPc2	překážka
(	(	kIx(	(
<g/>
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
se	s	k7c7	s
stoupající	stoupající	k2eAgFnSc7d1	stoupající
výškou	výška	k1gFnSc7	výška
tlak	tlak	k1gInSc1	tlak
vzduchu	vzduch	k1gInSc2	vzduch
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
zahřátý	zahřátý	k2eAgInSc1d1	zahřátý
vzduch	vzduch	k1gInSc1	vzduch
se	se	k3xPyFc4	se
rozpíná	rozpínat	k5eAaImIp3nS	rozpínat
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poklesu	pokles	k1gInSc6	pokles
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
začne	začít	k5eAaPmIp3nS	začít
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
opět	opět	k6eAd1	opět
přecházet	přecházet	k5eAaImF	přecházet
do	do	k7c2	do
kapalného	kapalný	k2eAgNnSc2d1	kapalné
skupenství	skupenství	k1gNnSc2	skupenství
čili	čili	k8xC	čili
kondenzovat	kondenzovat	k5eAaImF	kondenzovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
(	(	kIx(	(
<g/>
desublimuje	desublimovat	k5eAaImIp3nS	desublimovat
<g/>
)	)	kIx)	)
na	na	k7c4	na
drobné	drobný	k2eAgFnPc4d1	drobná
ledové	ledový	k2eAgFnPc4d1	ledová
krystalky	krystalka	k1gFnPc4	krystalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
pod	pod	k7c4	pod
kondenzační	kondenzační	k2eAgFnSc4d1	kondenzační
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kondenzační	kondenzační	k2eAgFnSc1d1	kondenzační
hladina	hladina	k1gFnSc1	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
kondenzační	kondenzační	k2eAgFnSc2d1	kondenzační
hladiny	hladina	k1gFnSc2	hladina
není	být	k5eNaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
podmínkách	podmínka	k1gFnPc6	podmínka
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Představu	představa	k1gFnSc4	představa
o	o	k7c6	o
aktuální	aktuální	k2eAgFnSc6d1	aktuální
výšce	výška	k1gFnSc6	výška
kondenzační	kondenzační	k2eAgFnSc2d1	kondenzační
hladiny	hladina	k1gFnSc2	hladina
si	se	k3xPyFc3	se
můžeme	moct	k5eAaImIp1nP	moct
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
obloha	obloha	k1gFnSc1	obloha
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
většími	veliký	k2eAgInPc7d2	veliký
oblaky	oblak	k1gInPc7	oblak
typu	typ	k1gInSc2	typ
cumulus	cumulus	k1gInSc1	cumulus
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
tmavá	tmavý	k2eAgFnSc1d1	tmavá
<g/>
,	,	kIx,	,
ostře	ostro	k6eAd1	ostro
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
základna	základna	k1gFnSc1	základna
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
kondenzační	kondenzační	k2eAgFnSc6d1	kondenzační
hladině	hladina	k1gFnSc6	hladina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vertikální	vertikální	k2eAgInPc1d1	vertikální
pohyby	pohyb	k1gInPc1	pohyb
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vynášejí	vynášet	k5eAaImIp3nP	vynášet
vzduch	vzduch	k1gInSc4	vzduch
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
do	do	k7c2	do
větších	veliký	k2eAgFnPc2d2	veliký
výšek	výška	k1gFnPc2	výška
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
konvekce	konvekce	k1gFnPc1	konvekce
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
výstupních	výstupní	k2eAgInPc2d1	výstupní
proudů	proud	k1gInPc2	proud
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
i	i	k9	i
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
oblaku	oblak	k1gInSc2	oblak
je	být	k5eAaImIp3nS	být
však	však	k9	však
kromě	kromě	k7c2	kromě
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
nutná	nutný	k2eAgFnSc1d1	nutná
též	též	k9	též
přítomnost	přítomnost	k1gFnSc1	přítomnost
kondenzačních	kondenzační	k2eAgNnPc2d1	kondenzační
jader	jádro	k1gNnPc2	jádro
<g/>
,	,	kIx,	,
maličkých	maličký	k2eAgFnPc2d1	maličká
částic	částice	k1gFnPc2	částice
aerosolů	aerosol	k1gInPc2	aerosol
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
okolo	okolo	k7c2	okolo
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
7	[number]	k4	7
až	až	k9	až
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
9	[number]	k4	9
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
Přirozenými	přirozený	k2eAgFnPc7d1	přirozená
kondenzačními	kondenzační	k2eAgFnPc7d1	kondenzační
jádry	jádro	k1gNnPc7	jádro
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
částice	částice	k1gFnPc1	částice
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
krystalky	krystalka	k1gFnSc2	krystalka
mořské	mořský	k2eAgFnSc2d1	mořská
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
částečky	částečka	k1gFnSc2	částečka
půdy	půda	k1gFnSc2	půda
či	či	k8xC	či
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kondenzačním	kondenzační	k2eAgNnSc6d1	kondenzační
jádru	jádro	k1gNnSc6	jádro
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
opět	opět	k6eAd1	opět
kondenzuje	kondenzovat	k5eAaImIp3nS	kondenzovat
do	do	k7c2	do
kapalného	kapalný	k2eAgNnSc2d1	kapalné
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c7	pod
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
rovnou	rovnou	k6eAd1	rovnou
desublimuje	desublimovat	k5eAaBmIp3nS	desublimovat
do	do	k7c2	do
pevného	pevný	k2eAgNnSc2d1	pevné
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Kapičky	kapička	k1gFnPc1	kapička
rostou	růst	k5eAaImIp3nP	růst
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
navzájem	navzájem	k6eAd1	navzájem
splývají	splývat	k5eAaImIp3nP	splývat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vodní	vodní	k2eAgFnSc2d1	vodní
či	či	k8xC	či
ledové	ledový	k2eAgFnSc2d1	ledová
kapičky	kapička	k1gFnSc2	kapička
vypaří	vypařit	k5eAaPmIp3nP	vypařit
nebo	nebo	k8xC	nebo
sublimují	sublimovat	k5eAaBmIp3nP	sublimovat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
velikosti	velikost	k1gFnPc4	velikost
srážkových	srážkový	k2eAgInPc2d1	srážkový
elementů	element	k1gInPc2	element
<g/>
,	,	kIx,	,
oblak	oblak	k1gInSc1	oblak
se	se	k3xPyFc4	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapičky	kapička	k1gFnPc1	kapička
tvořící	tvořící	k2eAgFnPc1d1	tvořící
oblaka	oblaka	k1gNnPc4	oblaka
mohou	moct	k5eAaImIp3nP	moct
narůst	narůst	k5eAaPmF	narůst
do	do	k7c2	do
rozměrů	rozměr	k1gInPc2	rozměr
potřebných	potřebný	k2eAgInPc2d1	potřebný
pro	pro	k7c4	pro
utvoření	utvoření	k1gNnSc4	utvoření
oblaků	oblak	k1gInPc2	oblak
i	i	k8xC	i
bez	bez	k7c2	bez
kondenzačních	kondenzační	k2eAgNnPc2d1	kondenzační
jader	jádro	k1gNnPc2	jádro
<g/>
,	,	kIx,	,
agregací	agregace	k1gFnPc2	agregace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
molekul	molekula	k1gFnPc2	molekula
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
však	však	k9	však
pomalý	pomalý	k2eAgInSc1d1	pomalý
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
oblaků	oblak	k1gInPc2	oblak
podílí	podílet	k5eAaImIp3nS	podílet
jen	jen	k9	jen
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
Kondenzační	kondenzační	k2eAgNnPc1d1	kondenzační
jádra	jádro	k1gNnPc1	jádro
ulehčují	ulehčovat	k5eAaImIp3nP	ulehčovat
přechod	přechod	k1gInSc4	přechod
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
skupenství	skupenství	k1gNnPc2	skupenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
kapiček	kapička	k1gFnPc2	kapička
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
oblaka	oblaka	k1gNnPc1	oblaka
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
různá	různý	k2eAgFnSc1d1	různá
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
typech	typ	k1gInPc6	typ
mraků	mrak	k1gInPc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
kapičky	kapička	k1gFnPc1	kapička
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dešťových	dešťový	k2eAgInPc6d1	dešťový
mracích	mrak	k1gInPc6	mrak
(	(	kIx(	(
<g/>
nimbostratech	nimbostratus	k1gInPc6	nimbostratus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
oblaku	oblak	k1gInSc2	oblak
rozměry	rozměra	k1gFnSc2	rozměra
až	až	k9	až
100	[number]	k4	100
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
,	,	kIx,	,
Nejmenší	malý	k2eAgFnPc1d3	nejmenší
kapičky	kapička	k1gFnPc1	kapička
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
v	v	k7c6	v
oblacích	oblace	k1gFnPc6	oblace
kumulus	kumulus	k1gInSc1	kumulus
a	a	k8xC	a
stratus	stratus	k1gInSc1	stratus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
rozměru	rozměra	k1gFnSc4	rozměra
okolo	okolo	k7c2	okolo
9	[number]	k4	9
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
<g/>
Zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
oblak	oblak	k1gInSc1	oblak
tvořen	tvořit	k5eAaImNgInS	tvořit
drobnými	drobný	k2eAgFnPc7d1	drobná
kapičkami	kapička	k1gFnPc7	kapička
nebo	nebo	k8xC	nebo
ledovými	ledový	k2eAgInPc7d1	ledový
krystalky	krystalek	k1gInPc7	krystalek
<g/>
,	,	kIx,	,
nezávisí	záviset	k5eNaImIp3nS	záviset
jen	jen	k9	jen
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
vodních	vodní	k2eAgFnPc2d1	vodní
kapek	kapka	k1gFnPc2	kapka
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
s	s	k7c7	s
teplotami	teplota	k1gFnPc7	teplota
do	do	k7c2	do
-42	-42	k4	-42
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
teplotní	teplotní	k2eAgFnSc7d1	teplotní
hranicí	hranice	k1gFnSc7	hranice
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
oblaky	oblak	k1gInPc1	oblak
tvořené	tvořený	k2eAgInPc1d1	tvořený
výlučně	výlučně	k6eAd1	výlučně
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
kapky	kapka	k1gFnPc1	kapka
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
pod	pod	k7c4	pod
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
přechlazené	přechlazený	k2eAgFnPc1d1	přechlazená
<g/>
.	.	kIx.	.
</s>
<s>
Přechlazené	přechlazený	k2eAgFnPc1d1	přechlazená
kapky	kapka	k1gFnPc1	kapka
hrají	hrát	k5eAaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
<g/>
Kapičky	kapička	k1gFnPc1	kapička
vody	voda	k1gFnPc1	voda
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
mohou	moct	k5eAaImIp3nP	moct
navázat	navázat	k5eAaPmF	navázat
i	i	k9	i
prachové	prachový	k2eAgFnPc1d1	prachová
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
v	v	k7c6	v
přemísťování	přemísťování	k1gNnSc6	přemísťování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vybuchla	vybuchnout	k5eAaPmAgFnS	vybuchnout
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
sopka	sopka	k1gFnSc1	sopka
Eyjafjallajökull	Eyjafjallajökull	k1gInSc1	Eyjafjallajökull
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
oblaka	oblaka	k1gNnPc4	oblaka
většinový	většinový	k2eAgInSc4d1	většinový
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
přesunu	přesun	k1gInSc6	přesun
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
popela	popel	k1gInSc2	popel
nad	nad	k7c4	nad
kontinentální	kontinentální	k2eAgFnSc4d1	kontinentální
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hmotnost	hmotnost	k1gFnSc4	hmotnost
===	===	k?	===
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
oblaku	oblak	k1gInSc2	oblak
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
rozměrech	rozměr	k1gInPc6	rozměr
<g/>
,	,	kIx,	,
typu	typ	k1gInSc6	typ
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc6	velikost
a	a	k8xC	a
hustotě	hustota	k1gFnSc6	hustota
vodních	vodní	k2eAgFnPc2d1	vodní
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ho	on	k3xPp3gMnSc4	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
oddělené	oddělený	k2eAgInPc4d1	oddělený
oblaky	oblak	k1gInPc4	oblak
typu	typ	k1gInSc2	typ
cumulus	cumulus	k1gInSc1	cumulus
mediocris	mediocris	k1gFnSc2	mediocris
například	například	k6eAd1	například
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
asi	asi	k9	asi
1	[number]	k4	1
gram	gram	k1gInSc4	gram
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Oblak	oblak	k1gInSc1	oblak
cumulus	cumulus	k1gInSc1	cumulus
mediocris	mediocris	k1gInSc1	mediocris
se	s	k7c7	s
základnou	základna	k1gFnSc7	základna
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
785	[number]	k4	785
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
a	a	k8xC	a
maximální	maximální	k2eAgFnSc7d1	maximální
výškou	výška	k1gFnSc7	výška
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
by	by	kYmCp3nS	by
vážil	vážit	k5eAaImAgInS	vážit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
250	[number]	k4	250
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Oblak	oblak	k1gInSc1	oblak
cumulonimbus	cumulonimbus	k1gInSc1	cumulonimbus
se	s	k7c7	s
stejně	stejně	k6eAd1	stejně
velkou	velký	k2eAgFnSc7d1	velká
základnou	základna	k1gFnSc7	základna
jako	jako	k8xS	jako
v	v	k7c6	v
předcházejícím	předcházející	k2eAgInSc6d1	předcházející
případě	případ	k1gInSc6	případ
a	a	k8xC	a
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
10	[number]	k4	10
km	km	kA	km
váží	vážit	k5eAaImIp3nS	vážit
při	při	k7c6	při
hustotě	hustota	k1gFnSc6	hustota
4	[number]	k4	4
g	g	kA	g
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
m	m	kA	m
<g/>
3	[number]	k4	3
až	až	k9	až
31	[number]	k4	31
400	[number]	k4	400
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc1	klasifikace
oblaků	oblak	k1gInPc2	oblak
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
rozlišování	rozlišování	k1gNnSc1	rozlišování
oblaků	oblak	k1gInPc2	oblak
podle	podle	k7c2	podle
tvarů	tvar	k1gInPc2	tvar
a	a	k8xC	a
barev	barva	k1gFnPc2	barva
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
už	už	k6eAd1	už
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
kvůli	kvůli	k7c3	kvůli
určování	určování	k1gNnSc3	určování
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
jako	jako	k8xC	jako
první	první	k4xOgFnSc7	první
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
nutnost	nutnost	k1gFnSc4	nutnost
třídění	třídění	k1gNnSc2	třídění
oblaků	oblak	k1gInPc2	oblak
francouzský	francouzský	k2eAgMnSc1d1	francouzský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Jean	Jean	k1gMnSc1	Jean
Baptiste	Baptist	k1gMnSc5	Baptist
Lamarck	Lamarck	k1gInSc4	Lamarck
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meteorologické	meteorologický	k2eAgFnSc6d1	meteorologická
ročence	ročenka	k1gFnSc6	ročenka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
publikoval	publikovat	k5eAaBmAgMnS	publikovat
svoji	svůj	k3xOyFgFnSc4	svůj
klasifikaci	klasifikace	k1gFnSc4	klasifikace
oblaků	oblak	k1gInPc2	oblak
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgNnSc4	který
používal	používat	k5eAaImAgMnS	používat
francouzské	francouzský	k2eAgNnSc4d1	francouzské
názvosloví	názvosloví	k1gNnSc4	názvosloví
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
neujala	ujmout	k5eNaPmAgFnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
Lamarckovi	Lamarcek	k1gMnSc6	Lamarcek
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
své	svůj	k3xOyFgNnSc4	svůj
schéma	schéma	k1gNnSc4	schéma
oblaků	oblak	k1gInPc2	oblak
amatérský	amatérský	k2eAgMnSc1d1	amatérský
meteorolog	meteorolog	k1gMnSc1	meteorolog
Luke	Luke	k1gFnPc2	Luke
Howard	Howard	k1gMnSc1	Howard
<g/>
.	.	kIx.	.
</s>
<s>
Howard	Howard	k1gMnSc1	Howard
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
latinské	latinský	k2eAgNnSc4d1	latinské
názvosloví	názvosloví	k1gNnSc4	názvosloví
a	a	k8xC	a
tři	tři	k4xCgInPc1	tři
hlavní	hlavní	k2eAgInPc1d1	hlavní
druhy	druh	k1gInPc1	druh
oblaků	oblak	k1gInPc2	oblak
(	(	kIx(	(
<g/>
Cirrus	Cirrus	k1gInSc1	Cirrus
<g/>
,	,	kIx,	,
Cumulus	Cumulus	k1gInSc1	Cumulus
<g/>
,	,	kIx,	,
Stratus	stratus	k1gInSc1	stratus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezitvary	mezitvar	k1gInPc4	mezitvar
však	však	k9	však
prosadil	prosadit	k5eAaPmAgInS	prosadit
až	až	k9	až
Francouz	Francouz	k1gMnSc1	Francouz
E.	E.	kA	E.
Renou	Rena	k1gFnSc7	Rena
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Howard	Howard	k1gInSc1	Howard
zavedl	zavést	k5eAaPmAgInS	zavést
i	i	k8xC	i
označení	označení	k1gNnSc1	označení
Nimbus	nimbus	k1gInSc1	nimbus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
souvislosti	souvislost	k1gFnSc6	souvislost
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c4	v
jaké	jaký	k3yQgNnSc4	jaký
ho	on	k3xPp3gNnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Howardova	Howardův	k2eAgFnSc1d1	Howardova
klasifikace	klasifikace	k1gFnSc1	klasifikace
se	se	k3xPyFc4	se
orientovala	orientovat	k5eAaBmAgFnS	orientovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
viditelné	viditelný	k2eAgInPc4d1	viditelný
znaky	znak	k1gInPc4	znak
oblaků	oblak	k1gInPc2	oblak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
výška	výška	k1gFnSc1	výška
<g/>
,	,	kIx,	,
rozsah	rozsah	k1gInSc1	rozsah
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
znaky	znak	k1gInPc1	znak
však	však	k9	však
nevypovídaly	vypovídat	k5eNaPmAgInP	vypovídat
nic	nic	k6eAd1	nic
o	o	k7c6	o
příčině	příčina	k1gFnSc6	příčina
vzniku	vznik	k1gInSc2	vznik
oblaků	oblak	k1gInPc2	oblak
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vyskytlo	vyskytnout	k5eAaPmAgNnS	vyskytnout
několik	několik	k4yIc1	několik
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
takové	takový	k3xDgFnSc2	takový
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc4	tento
nedostatek	nedostatek	k1gInSc4	nedostatek
neměla	mít	k5eNaImAgFnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
soustava	soustava	k1gFnSc1	soustava
oblaků	oblak	k1gInPc2	oblak
vyšla	vyjít	k5eAaPmAgFnS	vyjít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
vzniku	vznik	k1gInSc6	vznik
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
Švéd	Švéd	k1gMnSc1	Švéd
Hugo	Hugo	k1gMnSc1	Hugo
Hildebrand	Hildebranda	k1gFnPc2	Hildebranda
Hildebrandsson	Hildebrandsson	k1gMnSc1	Hildebrandsson
a	a	k8xC	a
Angličan	Angličan	k1gMnSc1	Angličan
R.	R.	kA	R.
Abercromby	Abercromb	k1gInPc1	Abercromb
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
zásluhou	zásluha	k1gFnSc7	zásluha
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
dohodě	dohoda	k1gFnSc3	dohoda
a	a	k8xC	a
vydání	vydání	k1gNnSc3	vydání
prvního	první	k4xOgInSc2	první
atlasu	atlas	k1gInSc2	atlas
věrných	věrný	k2eAgFnPc2d1	věrná
zobrazení	zobrazení	k1gNnSc4	zobrazení
tvarů	tvar	k1gInPc2	tvar
oblaků	oblak	k1gInPc2	oblak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současné	současný	k2eAgNnSc1d1	současné
třídění	třídění	k1gNnSc1	třídění
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
oblaků	oblak	k1gInPc2	oblak
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
10	[number]	k4	10
druhů	druh	k1gInPc2	druh
</s>
</p>
<p>
<s>
14	[number]	k4	14
tvarů	tvar	k1gInPc2	tvar
</s>
</p>
<p>
<s>
9	[number]	k4	9
odrůd	odrůda	k1gFnPc2	odrůda
</s>
</p>
<p>
<s>
9	[number]	k4	9
zvláštnostíV	zvláštnostíV	k?	zvláštnostíV
jedné	jeden	k4xCgFnSc6	jeden
chvíli	chvíle	k1gFnSc6	chvíle
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
jeden	jeden	k4xCgInSc1	jeden
oblak	oblak	k1gInSc1	oblak
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
odrůdu	odrůda	k1gFnSc4	odrůda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
výšky	výška	k1gFnSc2	výška
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
oblaky	oblak	k1gInPc4	oblak
zařadit	zařadit	k5eAaPmF	zařadit
mezi	mezi	k7c4	mezi
vysoké	vysoký	k2eAgInPc4d1	vysoký
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
nízké	nízký	k2eAgNnSc1d1	nízké
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bouřkový	bouřkový	k2eAgInSc1d1	bouřkový
oblak	oblak	k1gInSc1	oblak
cumulonimbus	cumulonimbus	k1gInSc1	cumulonimbus
není	být	k5eNaImIp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
žádné	žádný	k3yNgFnSc2	žádný
výškové	výškový	k2eAgFnSc2d1	výšková
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
současně	současně	k6eAd1	současně
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
výškách	výška	k1gFnPc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
výška	výška	k1gFnSc1	výška
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
oblaky	oblak	k1gInPc1	oblak
běžně	běžně	k6eAd1	běžně
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
km	km	kA	km
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rovníkových	rovníkový	k2eAgFnPc6d1	Rovníková
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
výška	výška	k1gFnSc1	výška
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
vymezená	vymezený	k2eAgFnSc1d1	vymezená
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oblak	oblak	k1gInSc1	oblak
může	moct	k5eAaImIp3nS	moct
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
"	"	kIx"	"
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
mlha	mlha	k1gFnSc1	mlha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Méně	málo	k6eAd2	málo
používaná	používaný	k2eAgFnSc1d1	používaná
je	být	k5eAaImIp3nS	být
klasifikace	klasifikace	k1gFnSc1	klasifikace
podle	podle	k7c2	podle
mikrostruktury	mikrostruktura	k1gFnSc2	mikrostruktura
oblaku	oblak	k1gInSc2	oblak
a	a	k8xC	a
genetická	genetický	k2eAgFnSc1d1	genetická
klasifikace	klasifikace	k1gFnSc1	klasifikace
oblaků	oblak	k1gInPc2	oblak
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Oblaky	oblak	k1gInPc1	oblak
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
označovat	označovat	k5eAaImF	označovat
i	i	k9	i
pomocí	pomocí	k7c2	pomocí
jejich	jejich	k3xOp3gInSc2	jejich
mateřského	mateřský	k2eAgInSc2d1	mateřský
oblaku	oblak	k1gInSc2	oblak
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
oblaku	oblak	k1gInSc2	oblak
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
názvem	název	k1gInSc7	název
patřičného	patřičný	k2eAgInSc2d1	patřičný
druhu	druh	k1gInSc2	druh
s	s	k7c7	s
přívlastkem	přívlastek	k1gInSc7	přívlastek
z	z	k7c2	z
názvu	název	k1gInSc2	název
druhu	druh	k1gInSc2	druh
mateřského	mateřský	k2eAgInSc2d1	mateřský
oblaku	oblak	k1gInSc2	oblak
+	+	kIx~	+
přípona	přípona	k1gFnSc1	přípona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastější	častý	k2eAgNnSc1d3	nejčastější
využívané	využívaný	k2eAgNnSc1d1	využívané
je	být	k5eAaImIp3nS	být
třídění	třídění	k1gNnSc1	třídění
oblaků	oblak	k1gInPc2	oblak
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
tvaru	tvar	k1gInSc2	tvar
(	(	kIx(	(
<g/>
morfologie	morfologie	k1gFnSc1	morfologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
klasifikace	klasifikace	k1gFnSc1	klasifikace
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
atlase	atlas	k1gInSc6	atlas
oblaků	oblak	k1gInPc2	oblak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
aktualizuje	aktualizovat	k5eAaBmIp3nS	aktualizovat
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
vydává	vydávat	k5eAaImIp3nS	vydávat
Světová	světový	k2eAgFnSc1d1	světová
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
World	World	k1gMnSc1	World
Meteorological	Meteorological	k1gFnSc2	Meteorological
Organisation	Organisation	k1gInSc1	Organisation
WMO	WMO	kA	WMO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
vydání	vydání	k1gNnSc1	vydání
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
10	[number]	k4	10
morfologických	morfologický	k2eAgInPc2d1	morfologický
druhů	druh	k1gInPc2	druh
oblaků	oblak	k1gInPc2	oblak
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Cirrus	Cirrus	k1gMnSc1	Cirrus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cirrocumulus	Cirrocumulus	k1gMnSc1	Cirrocumulus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cirrostratus	Cirrostratus	k1gMnSc1	Cirrostratus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Altocumulus	Altocumulus	k1gMnSc1	Altocumulus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Altostratus	altostratus	k1gInSc1	altostratus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stratocumulus	Stratocumulus	k1gMnSc1	Stratocumulus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cumulus	Cumulus	k1gMnSc1	Cumulus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stratus	stratus	k1gInSc1	stratus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Nimbostratus	nimbostratus	k1gInSc1	nimbostratus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cumulonimbus	Cumulonimbus	k1gInSc1	Cumulonimbus
<g/>
.	.	kIx.	.
<g/>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
morfologickou	morfologický	k2eAgFnSc4d1	morfologická
a	a	k8xC	a
výškovou	výškový	k2eAgFnSc4d1	výšková
klasifikaci	klasifikace	k1gFnSc4	klasifikace
oblaků	oblak	k1gInPc2	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
vyznačena	vyznačit	k5eAaPmNgNnP	vyznačit
písmena	písmeno	k1gNnPc1	písmeno
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
je	být	k5eAaImIp3nS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
zkratka	zkratka	k1gFnSc1	zkratka
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
tvaru	tvar	k1gInSc2	tvar
nebo	nebo	k8xC	nebo
odrůdy	odrůda	k1gFnSc2	odrůda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oblačnost	oblačnost	k1gFnSc4	oblačnost
==	==	k?	==
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
oblohy	obloha	k1gFnSc2	obloha
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
oblaky	oblak	k1gInPc4	oblak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
oblačnost	oblačnost	k1gFnSc1	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Oblačnost	oblačnost	k1gFnSc1	oblačnost
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
udávat	udávat	k5eAaImF	udávat
v	v	k7c6	v
osminách	osmina	k1gFnPc6	osmina
nebo	nebo	k8xC	nebo
desetinách	desetina	k1gFnPc6	desetina
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
zlomek	zlomek	k1gInSc4	zlomek
oblohy	obloha	k1gFnSc2	obloha
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
oblaky	oblak	k1gInPc4	oblak
přikrytý	přikrytý	k2eAgInSc1d1	přikrytý
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
jasná	jasný	k2eAgFnSc1d1	jasná
obloha	obloha	k1gFnSc1	obloha
má	mít	k5eAaImIp3nS	mít
oblačnost	oblačnost	k1gFnSc1	oblačnost
nula	nula	k1gFnSc1	nula
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nebe	nebe	k1gNnSc4	nebe
úplně	úplně	k6eAd1	úplně
zakryté	zakrytý	k2eAgInPc1d1	zakrytý
mraky	mrak	k1gInPc1	mrak
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oblačnost	oblačnost	k1gFnSc1	oblačnost
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
použité	použitý	k2eAgFnSc2d1	použitá
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
)	)	kIx)	)
osm	osm	k4xCc1	osm
nebo	nebo	k8xC	nebo
deset	deset	k4xCc1	deset
<g/>
.	.	kIx.	.
</s>
<s>
Meteorologové	meteorolog	k1gMnPc1	meteorolog
jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
mezistupně	mezistupeň	k1gInPc4	mezistupeň
pojmenovávají	pojmenovávat	k5eAaImIp3nP	pojmenovávat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
–	–	k?	–
jasno	jasno	k1gNnSc4	jasno
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
–	–	k?	–
skoro	skoro	k6eAd1	skoro
jasno	jasno	k6eAd1	jasno
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
–	–	k?	–
malá	malý	k2eAgFnSc1d1	malá
oblačnost	oblačnost	k1gFnSc1	oblačnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
–	–	k?	–
polojasno	polojasno	k1gNnSc1	polojasno
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
–	–	k?	–
oblačno	oblačno	k1gNnSc1	oblačno
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
–	–	k?	–
velká	velký	k2eAgFnSc1d1	velká
oblačnost	oblačnost	k1gFnSc1	oblačnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
–	–	k?	–
skoro	skoro	k6eAd1	skoro
zataženo	zatáhnout	k5eAaPmNgNnS	zatáhnout
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
–	–	k?	–
zataženo	zatažen	k2eAgNnSc1d1	zataženo
<g/>
.	.	kIx.	.
<g/>
Oblačnost	oblačnost	k1gFnSc1	oblačnost
se	se	k3xPyFc4	se
neurčuje	určovat	k5eNaImIp3nS	určovat
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
tam	tam	k6eAd1	tam
člověku	člověk	k1gMnSc6	člověk
většinou	většinou	k6eAd1	většinou
brání	bránit	k5eAaImIp3nS	bránit
v	v	k7c6	v
rozhledu	rozhled	k1gInSc6	rozhled
terénní	terénní	k2eAgFnPc1d1	terénní
překážky	překážka	k1gFnPc1	překážka
(	(	kIx(	(
<g/>
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
lesy	les	k1gInPc1	les
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
ze	z	k7c2	z
satelitních	satelitní	k2eAgInPc2d1	satelitní
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
a	a	k8xC	a
neobvyklá	obvyklý	k2eNgNnPc4d1	neobvyklé
oblaka	oblaka	k1gNnPc4	oblaka
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInPc1d1	základní
druhy	druh	k1gInPc1	druh
oblaků	oblak	k1gInPc2	oblak
mohou	moct	k5eAaImIp3nP	moct
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
nabýt	nabýt	k5eAaPmF	nabýt
nezvyklé	zvyklý	k2eNgFnPc4d1	nezvyklá
formy	forma	k1gFnPc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
formám	forma	k1gFnPc3	forma
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
zvláštnosti	zvláštnost	k1gFnSc3	zvláštnost
<g/>
.	.	kIx.	.
</s>
<s>
Bouřkový	bouřkový	k2eAgInSc1d1	bouřkový
mrak	mrak	k1gInSc1	mrak
například	například	k6eAd1	například
může	moct	k5eAaImIp3nS	moct
rozšířit	rozšířit	k5eAaPmF	rozšířit
svůj	svůj	k3xOyFgInSc4	svůj
vrchol	vrchol	k1gInSc4	vrchol
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
kovadliny	kovadlina	k1gFnSc2	kovadlina
<g/>
.	.	kIx.	.
</s>
<s>
Takovému	takový	k3xDgInSc3	takový
oblaku	oblak	k1gInSc3	oblak
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
incus	incus	k1gInSc1	incus
<g/>
.	.	kIx.	.
</s>
<s>
Základna	základna	k1gFnSc1	základna
oblaků	oblak	k1gInPc2	oblak
více	hodně	k6eAd2	hodně
typů	typ	k1gInPc2	typ
zase	zase	k9	zase
může	moct	k5eAaImIp3nS	moct
nabýt	nabýt	k5eAaPmF	nabýt
formu	forma	k1gFnSc4	forma
mamma	mammum	k1gNnSc2	mammum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
oblaku	oblak	k1gInSc2	oblak
visí	viset	k5eAaImIp3nP	viset
zaoblené	zaoblený	k2eAgInPc1d1	zaoblený
výběžky	výběžek	k1gInPc1	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pod	pod	k7c4	pod
základnu	základna	k1gFnSc4	základna
oblaku	oblak	k1gInSc2	oblak
směřují	směřovat	k5eAaImIp3nP	směřovat
srážkové	srážkový	k2eAgInPc1d1	srážkový
pruhy	pruh	k1gInPc1	pruh
nedosahující	dosahující	k2eNgInPc1d1	nedosahující
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
oblak	oblak	k1gInSc1	oblak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
virga	virga	k1gFnSc1	virga
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
srážkové	srážkový	k2eAgInPc1d1	srážkový
pruhy	pruh	k1gInPc1	pruh
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
–	–	k?	–
praecipitatio	praecipitatio	k6eAd1	praecipitatio
(	(	kIx(	(
<g/>
srážka	srážka	k1gFnSc1	srážka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
možné	možný	k2eAgFnPc1d1	možná
zvláštnosti	zvláštnost	k1gFnPc1	zvláštnost
jsou	být	k5eAaImIp3nP	být
arcus	arcus	k1gInSc4	arcus
(	(	kIx(	(
<g/>
hustý	hustý	k2eAgInSc4d1	hustý
horizontální	horizontální	k2eAgInSc4d1	horizontální
oblak	oblak	k1gInSc4	oblak
válcovitého	válcovitý	k2eAgInSc2d1	válcovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tuba	tuba	k1gFnSc1	tuba
(	(	kIx(	(
<g/>
oblačný	oblačný	k2eAgInSc1d1	oblačný
sloup	sloup	k1gInSc1	sloup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pileus	pileus	k1gInSc1	pileus
(	(	kIx(	(
<g/>
ledový	ledový	k2eAgInSc1d1	ledový
oblak	oblak	k1gInSc1	oblak
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
"	"	kIx"	"
<g/>
čepky	čepka	k1gFnSc2	čepka
<g/>
"	"	kIx"	"
nad	nad	k7c7	nad
cumuly	cumul	k1gInPc7	cumul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velum	velum	k1gInSc1	velum
(	(	kIx(	(
<g/>
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
oblak	oblak	k1gInSc1	oblak
podobný	podobný	k2eAgInSc1d1	podobný
plachtě	plachta	k1gFnSc3	plachta
nad	nad	k7c7	nad
kupovitými	kupovitý	k2eAgInPc7d1	kupovitý
oblaky	oblak	k1gInPc7	oblak
<g/>
)	)	kIx)	)
a	a	k8xC	a
pannus	pannus	k1gMnSc1	pannus
(	(	kIx(	(
<g/>
roztrhané	roztrhaný	k2eAgFnPc1d1	roztrhaná
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
fragmenty	fragment	k1gInPc1	fragment
oblaku	oblak	k1gInSc2	oblak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
oblaků	oblak	k1gInPc2	oblak
má	mít	k5eAaImIp3nS	mít
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
druhově	druhově	k6eAd1	druhově
specifické	specifický	k2eAgFnPc4d1	specifická
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
zvláštnosti	zvláštnost	k1gFnPc1	zvláštnost
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	s	k7c7	s
specifickými	specifický	k2eAgInPc7d1	specifický
doprovodnými	doprovodný	k2eAgInPc7d1	doprovodný
oblaky	oblak	k1gInPc7	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zvláštnost	zvláštnost	k1gFnSc1	zvláštnost
praecipitatio	praecipitatio	k6eAd1	praecipitatio
je	být	k5eAaImIp3nS	být
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
spodní	spodní	k2eAgFnSc4d1	spodní
stranu	strana	k1gFnSc4	strana
oblaku	oblak	k1gInSc2	oblak
nimbostratus	nimbostratus	k1gInSc1	nimbostratus
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
základních	základní	k2eAgInPc2d1	základní
druhů	druh	k1gInPc2	druh
oblaků	oblak	k1gInPc2	oblak
existují	existovat	k5eAaImIp3nP	existovat
další	další	k2eAgInPc1d1	další
útvary	útvar	k1gInPc1	útvar
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
částic	částice	k1gFnPc2	částice
rozptýlených	rozptýlený	k2eAgFnPc2d1	rozptýlená
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
dají	dát	k5eAaPmIp3nP	dát
nazývat	nazývat	k5eAaImF	nazývat
oblaka	oblaka	k1gNnPc4	oblaka
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
silného	silný	k2eAgInSc2d1	silný
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
prachové	prachový	k2eAgInPc1d1	prachový
nebo	nebo	k8xC	nebo
písečné	písečný	k2eAgInPc1d1	písečný
stěny	stěn	k1gInPc1	stěn
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
též	též	k9	též
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
oblakům	oblak	k1gInPc3	oblak
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
studených	studený	k2eAgNnPc2d1	studené
moří	moře	k1gNnPc2	moře
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
tvoří	tvořit	k5eAaImIp3nP	tvořit
tzv.	tzv.	kA	tzv.
rotorové	rotorový	k2eAgInPc4d1	rotorový
oblaky	oblak	k1gInPc4	oblak
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
oblaky	oblak	k1gInPc4	oblak
válcovitého	válcovitý	k2eAgInSc2d1	válcovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
klesnout	klesnout	k5eAaPmF	klesnout
až	až	k9	až
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
stanou	stanout	k5eAaPmIp3nP	stanout
rotorové	rotorový	k2eAgInPc4d1	rotorový
stěnové	stěnový	k2eAgInPc4d1	stěnový
oblaky	oblak	k1gInPc4	oblak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
překonání	překonání	k1gNnSc6	překonání
rychlosti	rychlost	k1gFnSc2	rychlost
zvuku	zvuk	k1gInSc2	zvuk
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
letadla	letadlo	k1gNnSc2	letadlo
utvoří	utvořit	k5eAaPmIp3nS	utvořit
Prandt-Glauertův	Prandt-Glauertův	k2eAgInSc1d1	Prandt-Glauertův
oblak	oblak	k1gInSc1	oblak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
změnou	změna	k1gFnSc7	změna
objemu	objem	k1gInSc2	objem
vzduchu	vzduch	k1gInSc2	vzduch
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
<s>
Umělá	umělý	k2eAgNnPc1d1	umělé
oblaka	oblaka	k1gNnPc1	oblaka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
bílých	bílý	k2eAgInPc2d1	bílý
pruhů	pruh	k1gInPc2	pruh
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
za	za	k7c7	za
dráhou	dráha	k1gFnSc7	dráha
letadel	letadlo	k1gNnPc2	letadlo
letících	letící	k2eAgNnPc2d1	letící
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
7	[number]	k4	7
až	až	k9	až
12	[number]	k4	12
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
zmrznutím	zmrznutí	k1gNnSc7	zmrznutí
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
ve	v	k7c6	v
spalinách	spaliny	k1gFnPc6	spaliny
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
už	už	k6eAd1	už
přítomné	přítomný	k2eAgInPc1d1	přítomný
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
v	v	k7c6	v
okolním	okolní	k2eAgInSc6d1	okolní
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
k	k	k7c3	k
desublimaci	desublimace	k1gFnSc3	desublimace
"	"	kIx"	"
<g/>
nastartují	nastartovat	k5eAaPmIp3nP	nastartovat
<g/>
"	"	kIx"	"
páry	pár	k1gInPc4	pár
z	z	k7c2	z
výfuku	výfuk	k1gInSc2	výfuk
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
vzniku	vznik	k1gInSc3	vznik
pruhů	pruh	k1gInPc2	pruh
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
-60	-60	k4	-60
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejprve	nejprve	k6eAd1	nejprve
mají	mít	k5eAaImIp3nP	mít
vzhled	vzhled	k1gInSc4	vzhled
zářivě	zářivě	k6eAd1	zářivě
bílých	bílý	k2eAgFnPc2d1	bílá
čar	čára	k1gFnPc2	čára
<g/>
,	,	kIx,	,
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
však	však	k9	však
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
krystalky	krystalka	k1gFnPc1	krystalka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
je	on	k3xPp3gMnPc4	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdalují	vzdalovat	k5eAaImIp3nP	vzdalovat
a	a	k8xC	a
sublimují	sublimovat	k5eAaBmIp3nP	sublimovat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
vydržet	vydržet	k5eAaPmF	vydržet
i	i	k9	i
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
stanou	stanout	k5eAaPmIp3nP	stanout
nerozeznatelnými	rozeznatelný	k2eNgFnPc7d1	nerozeznatelná
od	od	k7c2	od
přírodních	přírodní	k2eAgInPc2d1	přírodní
oblaků	oblak	k1gInPc2	oblak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
zvláštně	zvláštně	k6eAd1	zvláštně
tvarované	tvarovaný	k2eAgInPc1d1	tvarovaný
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
Kelvinovy-Helmholtzovy	Kelvinovy-Helmholtzův	k2eAgInPc1d1	Kelvinovy-Helmholtzův
oblaky	oblak	k1gInPc1	oblak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
vzhled	vzhled	k1gInSc4	vzhled
ostře	ostro	k6eAd1	ostro
vykrojených	vykrojený	k2eAgFnPc2d1	vykrojená
jemných	jemný	k2eAgFnPc2d1	jemná
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
už	už	k9	už
na	na	k7c6	na
existujícím	existující	k2eAgInSc6d1	existující
oblaku	oblak	k1gInSc6	oblak
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
pohybujícího	pohybující	k2eAgInSc2d1	pohybující
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vane	vanout	k5eAaImIp3nS	vanout
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Kelvinova	Kelvinův	k2eAgFnSc1d1	Kelvinova
<g/>
–	–	k?	–
<g/>
Helmholtzova	Helmholtzův	k2eAgFnSc1d1	Helmholtzova
nestabilita	nestabilita	k1gFnSc1	nestabilita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
oblaků	oblak	k1gInPc2	oblak
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
nejspodnější	spodní	k2eAgFnSc6d3	nejspodnější
vrstvě	vrstva	k1gFnSc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
v	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
stratosférická	stratosférický	k2eAgNnPc4d1	stratosférické
a	a	k8xC	a
mezosférická	mezosférický	k2eAgNnPc4d1	mezosférický
oblaka	oblaka	k1gNnPc4	oblaka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patří	patřit	k5eAaImIp3nS	patřit
perleťová	perleťový	k2eAgNnPc4d1	perleťové
oblaka	oblaka	k1gNnPc4	oblaka
a	a	k8xC	a
noční	noční	k2eAgNnPc4d1	noční
svítící	svítící	k2eAgNnPc4d1	svítící
mračna	mračno	k1gNnPc4	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Perleťová	perleťový	k2eAgNnPc1d1	perleťové
oblaka	oblaka	k1gNnPc1	oblaka
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
podobají	podobat	k5eAaImIp3nP	podobat
cirrům	cirr	k1gInPc3	cirr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
výraznou	výrazný	k2eAgFnSc7d1	výrazná
irizací	irizace	k1gFnPc2	irizace
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnSc1d1	připomínající
perleť	perleť	k1gFnSc1	perleť
<g/>
.	.	kIx.	.
</s>
<s>
Cirrům	Cirr	k1gMnPc3	Cirr
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
i	i	k9	i
noční	noční	k2eAgNnPc4d1	noční
svítící	svítící	k2eAgNnPc4d1	svítící
mračna	mračno	k1gNnPc4	mračno
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pravých	pravý	k2eAgInPc2d1	pravý
cirrů	cirr	k1gInPc2	cirr
se	se	k3xPyFc4	se
však	však	k9	však
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mnohem	mnohem	k6eAd1	mnohem
výš	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
v	v	k7c6	v
mezosféře	mezosféra	k1gFnSc6	mezosféra
(	(	kIx(	(
<g/>
75	[number]	k4	75
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
km	km	kA	km
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
původ	původ	k1gInSc1	původ
dosud	dosud	k6eAd1	dosud
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Svítí	svítit	k5eAaImIp3nS	svítit
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odrážejí	odrážet	k5eAaImIp3nP	odrážet
sluneční	sluneční	k2eAgNnSc4d1	sluneční
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
slunce	slunce	k1gNnSc1	slunce
příliš	příliš	k6eAd1	příliš
nízko	nízko	k6eAd1	nízko
pod	pod	k7c7	pod
obzorem	obzor	k1gInSc7	obzor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
osvětlovalo	osvětlovat	k5eAaImAgNnS	osvětlovat
níže	nízce	k6eAd2	nízce
položená	položený	k2eAgNnPc4d1	položené
troposférická	troposférický	k2eAgNnPc4d1	troposférické
oblaka	oblaka	k1gNnPc4	oblaka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc1	století
se	se	k3xPyFc4	se
vyskytla	vyskytnout	k5eAaPmAgNnP	vyskytnout
opakovaná	opakovaný	k2eAgNnPc1d1	opakované
pozorování	pozorování	k1gNnPc1	pozorování
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
oblaku	oblak	k1gInSc2	oblak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
lze	lze	k6eAd1	lze
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
zařadit	zařadit	k5eAaPmF	zařadit
mezi	mezi	k7c4	mezi
známé	známý	k2eAgInPc4d1	známý
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Světová	světový	k2eAgFnSc1d1	světová
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
organizace	organizace	k1gFnSc1	organizace
doplní	doplnit	k5eAaPmIp3nS	doplnit
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
atlas	atlas	k1gInSc4	atlas
oblaků	oblak	k1gInPc2	oblak
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
asperatus	asperatus	k1gInSc1	asperatus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
znamená	znamenat	k5eAaImIp3nS	znamenat
hrubý	hrubý	k2eAgInSc1d1	hrubý
<g/>
,	,	kIx,	,
bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
<g/>
,	,	kIx,	,
zvlněný	zvlněný	k2eAgInSc1d1	zvlněný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeho	jeho	k3xOp3gInSc3	jeho
nezvyklému	zvyklý	k2eNgInSc3d1	nezvyklý
vzhledu	vzhled	k1gInSc3	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
jej	on	k3xPp3gMnSc4	on
často	často	k6eAd1	často
přirovnávají	přirovnávat	k5eAaImIp3nP	přirovnávat
k	k	k7c3	k
pohledu	pohled	k1gInSc3	pohled
na	na	k7c4	na
zvlněnou	zvlněný	k2eAgFnSc4d1	zvlněná
vodní	vodní	k2eAgFnSc4d1	vodní
hladinu	hladina	k1gFnSc4	hladina
zespodu	zespodu	k6eAd1	zespodu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Orografická	orografický	k2eAgNnPc1d1	orografické
oblaka	oblaka	k1gNnPc1	oblaka
==	==	k?	==
</s>
</p>
<p>
<s>
Oblaka	oblaka	k1gNnPc4	oblaka
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
vznik	vznik	k1gInSc4	vznik
měly	mít	k5eAaImAgFnP	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
terénní	terénní	k2eAgFnSc2d1	terénní
překážky	překážka	k1gFnSc2	překážka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
orografická	orografický	k2eAgNnPc1d1	orografické
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
některého	některý	k3yIgMnSc2	některý
z	z	k7c2	z
10	[number]	k4	10
druhů	druh	k1gInPc2	druh
oblaků	oblak	k1gInPc2	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
orografický	orografický	k2eAgInSc1d1	orografický
oblak	oblak	k1gInSc1	oblak
je	být	k5eAaImIp3nS	být
altocumulus	altocumulus	k1gInSc4	altocumulus
lenticularis	lenticularis	k1gFnSc2	lenticularis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
za	za	k7c4	za
hřebeny	hřeben	k1gInPc4	hřeben
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bílý	bílý	k2eAgInSc1d1	bílý
oblak	oblak	k1gInSc1	oblak
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
někdy	někdy	k6eAd1	někdy
připomíná	připomínat	k5eAaImIp3nS	připomínat
létající	létající	k2eAgInSc4d1	létající
talíř	talíř	k1gInSc4	talíř
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc4d1	typický
způsob	způsob	k1gInSc4	způsob
jeho	jeho	k3xOp3gInSc2	jeho
vzniku	vznik	k1gInSc2	vznik
je	být	k5eAaImIp3nS	být
vlnové	vlnový	k2eAgNnSc1d1	vlnové
proudění	proudění	k1gNnSc1	proudění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
předním	přední	k2eAgInSc6d1	přední
okraji	okraj	k1gInSc6	okraj
jsou	být	k5eAaImIp3nP	být
oblaka	oblaka	k1gNnPc1	oblaka
většinou	většina	k1gFnSc7	většina
ostře	ostro	k6eAd1	ostro
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
<g/>
,	,	kIx,	,
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
roztřepená	roztřepený	k2eAgNnPc1d1	roztřepené
a	a	k8xC	a
tenká	tenký	k2eAgNnPc1d1	tenké
<g/>
.	.	kIx.	.
</s>
<s>
Oblak	oblak	k1gInSc1	oblak
působí	působit	k5eAaImIp3nS	působit
stacionárně	stacionárně	k6eAd1	stacionárně
<g/>
,	,	kIx,	,
nemění	měnit	k5eNaImIp3nS	měnit
tvar	tvar	k1gInSc4	tvar
ani	ani	k8xC	ani
polohu	poloha	k1gFnSc4	poloha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
však	však	k9	však
probíhají	probíhat	k5eAaImIp3nP	probíhat
dynamické	dynamický	k2eAgInPc1d1	dynamický
procesy	proces	k1gInPc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
návětrné	návětrný	k2eAgFnSc6d1	návětrná
straně	strana	k1gFnSc6	strana
do	do	k7c2	do
oblaku	oblak	k1gInSc2	oblak
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
kondenzující	kondenzující	k2eAgFnSc1d1	kondenzující
pára	pára	k1gFnSc1	pára
a	a	k8xC	a
na	na	k7c6	na
závětrné	závětrný	k2eAgFnSc6d1	závětrná
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
<g/>
.	.	kIx.	.
</s>
<s>
Oblak	oblak	k1gInSc4	oblak
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
silné	silný	k2eAgNnSc1d1	silné
proudění	proudění	k1gNnSc1	proudění
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
osamělém	osamělý	k2eAgInSc6d1	osamělý
horském	horský	k2eAgInSc6d1	horský
vrcholu	vrchol	k1gInSc6	vrchol
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
při	při	k7c6	při
silném	silný	k2eAgInSc6d1	silný
větru	vítr	k1gInSc6	vítr
tzv.	tzv.	kA	tzv.
vlajkový	vlajkový	k2eAgInSc4d1	vlajkový
oblak	oblak	k1gInSc4	oblak
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
čapka	čapka	k1gFnSc1	čapka
<g/>
"	"	kIx"	"
zahalující	zahalující	k2eAgNnSc1d1	zahalující
jeho	jeho	k3xOp3gInSc4	jeho
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInPc1d1	lokální
větrné	větrný	k2eAgInPc1d1	větrný
systémy	systém	k1gInPc1	systém
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
vznik	vznik	k1gInSc1	vznik
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
oblaků	oblak	k1gInPc2	oblak
i	i	k9	i
na	na	k7c6	na
pobřežích	pobřeží	k1gNnPc6	pobřeží
moří	mořit	k5eAaImIp3nS	mořit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bríze	bríza	k1gFnSc6	bríza
<g/>
,	,	kIx,	,
mořském	mořský	k2eAgInSc6d1	mořský
větru	vítr	k1gInSc6	vítr
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nad	nad	k7c7	nad
pevninou	pevnina	k1gFnSc7	pevnina
vznikat	vznikat	k5eAaImF	vznikat
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
hradba	hradba	k1gFnSc1	hradba
oblaků	oblak	k1gInPc2	oblak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Souvislost	souvislost	k1gFnSc1	souvislost
s	s	k7c7	s
počasím	počasí	k1gNnSc7	počasí
==	==	k?	==
</s>
</p>
<p>
<s>
Oblaky	oblak	k1gInPc1	oblak
mnoho	mnoho	k6eAd1	mnoho
vypovídají	vypovídat	k5eAaImIp3nP	vypovídat
o	o	k7c6	o
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
právě	právě	k6eAd1	právě
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
vlhkosti	vlhkost	k1gFnSc6	vlhkost
a	a	k8xC	a
proudění	proudění	k1gNnSc6	proudění
vzduchu	vzduch	k1gInSc2	vzduch
vznikají	vznikat	k5eAaImIp3nP	vznikat
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
druhy	druh	k1gInPc1	druh
oblaků	oblak	k1gInPc2	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Jasná	jasný	k2eAgFnSc1d1	jasná
obloha	obloha	k1gFnSc1	obloha
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
se	se	k3xPyFc4	se
netvoří	tvořit	k5eNaImIp3nP	tvořit
kupovité	kupovitý	k2eAgInPc1d1	kupovitý
oblaky	oblak	k1gInPc1	oblak
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
mizící	mizící	k2eAgInPc4d1	mizící
kondenzační	kondenzační	k2eAgInPc4d1	kondenzační
pruhy	pruh	k1gInPc4	pruh
za	za	k7c7	za
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
,	,	kIx,	,
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
ustálené	ustálený	k2eAgNnSc4d1	ustálené
jasné	jasný	k2eAgNnSc4d1	jasné
počasí	počasí	k1gNnSc4	počasí
<g/>
.	.	kIx.	.
<g/>
Bouřky	bouřka	k1gFnPc1	bouřka
jsou	být	k5eAaImIp3nP	být
vázány	vázán	k2eAgFnPc1d1	vázána
výlučně	výlučně	k6eAd1	výlučně
na	na	k7c4	na
oblak	oblak	k1gInSc4	oblak
kumulonimbus	kumulonimbus	k1gInSc4	kumulonimbus
<g/>
,	,	kIx,	,
nejhrubší	hrubý	k2eAgInSc4d3	nejhrubší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
oblaků	oblak	k1gInPc2	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Kumulonimby	kumulonimbus	k1gInPc1	kumulonimbus
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
kumulů	kumulus	k1gInPc2	kumulus
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jejich	jejich	k3xOp3gFnSc1	jejich
přítomnost	přítomnost	k1gFnSc1	přítomnost
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
cumulus	cumulus	k1gInSc4	cumulus
humilis	humilis	k1gFnSc4	humilis
<g/>
)	)	kIx)	)
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
značí	značit	k5eAaImIp3nS	značit
zhoršení	zhoršení	k1gNnSc1	zhoršení
počasí	počasí	k1gNnSc2	počasí
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
bouřkového	bouřkový	k2eAgInSc2d1	bouřkový
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
<g/>
Cirry	Cirra	k1gFnPc1	Cirra
jsou	být	k5eAaImIp3nP	být
oblaky	oblak	k1gInPc4	oblak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
od	od	k7c2	od
typu	typ	k1gInSc2	typ
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
pěkné	pěkný	k2eAgFnPc1d1	pěkná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
špatné	špatný	k2eAgNnSc1d1	špatné
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Cirrokumuly	Cirrokumula	k1gFnPc1	Cirrokumula
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
malých	malý	k2eAgMnPc2d1	malý
"	"	kIx"	"
<g/>
beránků	beránek	k1gMnPc2	beránek
<g/>
"	"	kIx"	"
bývají	bývat	k5eAaImIp3nP	bývat
předzvěstí	předzvěst	k1gFnSc7	předzvěst
bouřek	bouřka	k1gFnPc2	bouřka
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
oblačnost	oblačnost	k1gFnSc1	oblačnost
(	(	kIx(	(
<g/>
stratocumulus	stratocumulus	k1gInSc1	stratocumulus
<g/>
,	,	kIx,	,
stratus	stratus	k1gInSc1	stratus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
předzvěstí	předzvěst	k1gFnPc2	předzvěst
vytrvalého	vytrvalý	k2eAgInSc2d1	vytrvalý
deště	dešť	k1gInSc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jemné	jemný	k2eAgInPc1d1	jemný
<g/>
,	,	kIx,	,
řídké	řídký	k2eAgInPc1d1	řídký
<g/>
,	,	kIx,	,
nezahušťující	zahušťující	k2eNgInPc1d1	zahušťující
se	se	k3xPyFc4	se
oblaky	oblak	k1gInPc7	oblak
vysoké	vysoký	k2eAgFnSc2d1	vysoká
hladiny	hladina	k1gFnSc2	hladina
předznamenávají	předznamenávat	k5eAaImIp3nP	předznamenávat
přetrvávající	přetrvávající	k2eAgNnSc4d1	přetrvávající
pěkné	pěkný	k2eAgNnSc4d1	pěkné
počasí	počasí	k1gNnSc4	počasí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zvětšující	zvětšující	k2eAgInPc1d1	zvětšující
a	a	k8xC	a
houstnoucí	houstnoucí	k2eAgInPc1d1	houstnoucí
vysoké	vysoký	k2eAgInPc1d1	vysoký
oblaky	oblak	k1gInPc1	oblak
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
jeho	jeho	k3xOp3gNnSc4	jeho
zhoršení	zhoršení	k1gNnSc4	zhoršení
<g/>
.	.	kIx.	.
<g/>
Vysoko	vysoko	k6eAd1	vysoko
položené	položený	k2eAgNnSc1d1	položené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řídnoucí	řídnoucí	k2eAgInPc1d1	řídnoucí
oblaky	oblak	k1gInPc1	oblak
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
optické	optický	k2eAgInPc4d1	optický
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
souhrnně	souhrnně	k6eAd1	souhrnně
nazývají	nazývat	k5eAaImIp3nP	nazývat
halové	halový	k2eAgInPc1d1	halový
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krystalcích	krystalek	k1gInPc6	krystalek
oblaků	oblak	k1gInPc2	oblak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
lomu	lom	k1gInSc2	lom
a	a	k8xC	a
odrazu	odraz	k1gInSc2	odraz
slunečního	sluneční	k2eAgNnSc2d1	sluneční
nebo	nebo	k8xC	nebo
měsíčního	měsíční	k2eAgNnSc2d1	měsíční
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
těchto	tento	k3xDgInPc2	tento
světelných	světelný	k2eAgInPc2d1	světelný
zdrojů	zdroj	k1gInPc2	zdroj
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
různě	různě	k6eAd1	různě
zbarvené	zbarvený	k2eAgInPc1d1	zbarvený
kruhy	kruh	k1gInPc1	kruh
<g/>
,	,	kIx,	,
oblouky	oblouk	k1gInPc1	oblouk
a	a	k8xC	a
skvrny	skvrna	k1gFnPc1	skvrna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oblačnost	oblačnost	k1gFnSc1	oblačnost
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
tělesech	těleso	k1gNnPc6	těleso
==	==	k?	==
</s>
</p>
<p>
<s>
Oblaka	oblaka	k1gNnPc4	oblaka
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
každé	každý	k3xTgNnSc4	každý
těleso	těleso	k1gNnSc4	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
dostatečně	dostatečně	k6eAd1	dostatečně
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kondenzaci	kondenzace	k1gFnSc3	kondenzace
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
to	ten	k3xDgNnSc4	ten
nejsou	být	k5eNaImIp3nP	být
vždy	vždy	k6eAd1	vždy
částice	částice	k1gFnPc1	částice
vody	voda	k1gFnSc2	voda
–	–	k?	–
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Marsu	Mars	k1gInSc2	Mars
tvoří	tvořit	k5eAaImIp3nS	tvořit
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
jen	jen	k6eAd1	jen
malou	malý	k2eAgFnSc4d1	malá
příměs	příměs	k1gFnSc4	příměs
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
jiných	jiný	k2eAgFnPc2d1	jiná
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Extraterestrické	Extraterestrický	k2eAgInPc1d1	Extraterestrický
oblaky	oblak	k1gInPc1	oblak
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
především	především	k9	především
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
krystalického	krystalický	k2eAgInSc2d1	krystalický
amoniaku	amoniak	k1gInSc2	amoniak
a	a	k8xC	a
hydrosulfidu	hydrosulfid	k1gInSc2	hydrosulfid
amonného	amonný	k2eAgInSc2d1	amonný
(	(	kIx(	(
<g/>
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
metanu	metan	k1gInSc2	metan
(	(	kIx(	(
<g/>
Uran	Uran	k1gInSc1	Uran
<g/>
,	,	kIx,	,
Neptun	Neptun	k1gInSc1	Neptun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
(	(	kIx(	(
<g/>
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Planety	planeta	k1gFnPc1	planeta
===	===	k?	===
</s>
</p>
<p>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
kosmickými	kosmický	k2eAgFnPc7d1	kosmická
sondami	sonda	k1gFnPc7	sonda
odhalil	odhalit	k5eAaPmAgMnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
Merkur	Merkur	k1gInSc1	Merkur
má	mít	k5eAaImIp3nS	mít
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
příliš	příliš	k6eAd1	příliš
řídká	řídký	k2eAgNnPc1d1	řídké
a	a	k8xC	a
oblaka	oblaka	k1gNnPc1	oblaka
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
netvoří	tvořit	k5eNaImIp3nP	tvořit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
má	mít	k5eAaImIp3nS	mít
atmosféru	atmosféra	k1gFnSc4	atmosféra
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
hustší	hustý	k2eAgNnSc4d2	hustší
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
celou	celá	k1gFnSc4	celá
ji	on	k3xPp3gFnSc4	on
zahaluje	zahalovat	k5eAaImIp3nS	zahalovat
vrstva	vrstva	k1gFnSc1	vrstva
oblaků	oblak	k1gInPc2	oblak
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nimž	jenž	k3xRgFnPc3	jenž
není	být	k5eNaImIp3nS	být
povrch	povrch	k7c2wR	povrch
planety	planeta	k1gFnSc2	planeta
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
měří	měřit	k5eAaImIp3nS	měřit
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
870	[number]	k4	870
km	km	kA	km
a	a	k8xC	a
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Oblaka	oblaka	k1gNnPc1	oblaka
Venuše	Venuše	k1gFnSc2	Venuše
jsou	být	k5eAaImIp3nP	být
složena	složit	k5eAaPmNgNnP	složit
z	z	k7c2	z
kapiček	kapička	k1gFnPc2	kapička
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
velikost	velikost	k1gFnSc1	velikost
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
vrstvě	vrstva	k1gFnSc6	vrstva
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
mm	mm	kA	mm
<g/>
.	.	kIx.	.
<g/>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
také	také	k9	také
poměrně	poměrně	k6eAd1	poměrně
řídká	řídký	k2eAgNnPc1d1	řídké
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zpozorovat	zpozorovat	k5eAaPmF	zpozorovat
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
oblaků	oblak	k1gInPc2	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
oblaka	oblaka	k1gNnPc4	oblaka
z	z	k7c2	z
ledových	ledový	k2eAgFnPc2d1	ledová
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
pozemské	pozemský	k2eAgFnPc1d1	pozemská
Cirry	Cirra	k1gFnPc1	Cirra
nebo	nebo	k8xC	nebo
Cumuly	Cumul	k1gInPc1	Cumul
<g/>
,	,	kIx,	,
oblaka	oblaka	k1gNnPc1	oblaka
tvořená	tvořený	k2eAgNnPc1d1	tvořené
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
krystalickým	krystalický	k2eAgFnPc3d1	krystalická
CO2	CO2	k1gFnPc3	CO2
a	a	k8xC	a
na	na	k7c4	na
oblaka	oblaka	k1gNnPc4	oblaka
z	z	k7c2	z
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
marsovského	marsovský	k2eAgNnSc2d1	marsovské
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
léta	léto	k1gNnSc2	léto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vypařování	vypařování	k1gNnSc3	vypařování
nad	nad	k7c7	nad
oblastmi	oblast	k1gFnPc7	oblast
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
<g/>
,	,	kIx,	,
vzniku	vznik	k1gInSc3	vznik
oblačnosti	oblačnost	k1gFnSc2	oblačnost
a	a	k8xC	a
jejího	její	k3xOp3gInSc2	její
přesunu	přesun	k1gInSc2	přesun
do	do	k7c2	do
rovníkových	rovníkový	k2eAgFnPc2d1	Rovníková
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
oblaka	oblaka	k1gNnPc1	oblaka
zamrznou	zamrznout	k5eAaPmIp3nP	zamrznout
a	a	k8xC	a
dopadnou	dopadnout	k5eAaPmIp3nP	dopadnout
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ledových	ledový	k2eAgInPc2d1	ledový
krystalků	krystalek	k1gInPc2	krystalek
<g/>
.	.	kIx.	.
<g/>
Plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
mají	mít	k5eAaImIp3nP	mít
mohutné	mohutný	k2eAgFnPc4d1	mohutná
atmosféry	atmosféra	k1gFnPc4	atmosféra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nedovolují	dovolovat	k5eNaImIp3nP	dovolovat
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
do	do	k7c2	do
hlubších	hluboký	k2eAgFnPc2d2	hlubší
vrstev	vrstva	k1gFnPc2	vrstva
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
svou	svůj	k3xOyFgFnSc7	svůj
bouřlivou	bouřlivý	k2eAgFnSc7d1	bouřlivá
a	a	k8xC	a
pestře	pestro	k6eAd1	pestro
zbarvenou	zbarvený	k2eAgFnSc7d1	zbarvená
oblačností	oblačnost	k1gFnSc7	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholky	vrcholek	k1gInPc1	vrcholek
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
mraků	mrak	k1gInPc2	mrak
jsou	být	k5eAaImIp3nP	být
červené	červená	k1gFnPc1	červená
kvůli	kvůli	k7c3	kvůli
anorganickým	anorganický	k2eAgFnPc3d1	anorganická
polymerním	polymerní	k2eAgFnPc3d1	polymerní
sloučeninám	sloučenina	k1gFnPc3	sloučenina
fosforu	fosfor	k1gInSc2	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
nimi	on	k3xPp3gFnPc7	on
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnPc1d2	nižší
jsou	být	k5eAaImIp3nP	být
hnědé	hnědý	k2eAgFnPc1d1	hnědá
a	a	k8xC	a
nejnižší	nízký	k2eAgFnPc1d3	nejnižší
pozorovatelné	pozorovatelný	k2eAgFnPc1d1	pozorovatelná
oblasti	oblast	k1gFnPc1	oblast
mají	mít	k5eAaImIp3nP	mít
modré	modrý	k2eAgNnSc4d1	modré
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
zbarvení	zbarvení	k1gNnSc1	zbarvení
mraků	mrak	k1gInPc2	mrak
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
kromě	kromě	k7c2	kromě
různých	různý	k2eAgFnPc2d1	různá
barevných	barevný	k2eAgFnPc2d1	barevná
příměsí	příměs	k1gFnPc2	příměs
i	i	k8xC	i
měnící	měnící	k2eAgFnSc1d1	měnící
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
oblaků	oblak	k1gInPc2	oblak
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
výpočtů	výpočet	k1gInPc2	výpočet
asi	asi	k9	asi
1	[number]	k4	1
000	[number]	k4	000
km	km	kA	km
tlustá	tlustý	k2eAgFnSc1d1	tlustá
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
do	do	k7c2	do
tmavých	tmavý	k2eAgInPc2d1	tmavý
pruhů	pruh	k1gInPc2	pruh
a	a	k8xC	a
světlejších	světlý	k2eAgNnPc2d2	světlejší
pásem	pásmo	k1gNnPc2	pásmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Viditelné	viditelný	k2eAgInPc1d1	viditelný
mraky	mrak	k1gInPc1	mrak
Saturnu	Saturn	k1gInSc2	Saturn
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
hlavně	hlavně	k9	hlavně
krystalickým	krystalický	k2eAgInSc7d1	krystalický
amoniakem	amoniak	k1gInSc7	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Výraznými	výrazný	k2eAgInPc7d1	výrazný
atmosférickými	atmosférický	k2eAgInPc7d1	atmosférický
útvary	útvar	k1gInPc7	útvar
jsou	být	k5eAaImIp3nP	být
světlé	světlý	k2eAgFnPc4d1	světlá
skvrny	skvrna	k1gFnPc4	skvrna
podobné	podobný	k2eAgFnPc4d1	podobná
tlakovým	tlakový	k2eAgFnPc3d1	tlaková
nížím	níže	k1gFnPc3	níže
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
je	on	k3xPp3gInPc4	on
konvektivní	konvektivní	k2eAgInPc4d1	konvektivní
proudy	proud	k1gInPc4	proud
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
mění	měnit	k5eAaImIp3nS	měnit
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
po	po	k7c6	po
čase	čas	k1gInSc6	čas
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
.	.	kIx.	.
<g/>
Uran	Uran	k1gInSc1	Uran
má	mít	k5eAaImIp3nS	mít
mraky	mrak	k1gInPc4	mrak
tvořené	tvořený	k2eAgInPc4d1	tvořený
hlavně	hlavně	k9	hlavně
metanem	metan	k1gInSc7	metan
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
oblaka	oblaka	k1gNnPc1	oblaka
tvoří	tvořit	k5eAaImIp3nP	tvořit
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
méně	málo	k6eAd2	málo
výrazné	výrazný	k2eAgInPc4d1	výrazný
obrazce	obrazec	k1gInPc4	obrazec
než	než	k8xS	než
u	u	k7c2	u
Jupitera	Jupiter	k1gMnSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gMnSc1	Neptun
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
svou	svůj	k3xOyFgFnSc7	svůj
bouřlivou	bouřlivý	k2eAgFnSc7d1	bouřlivá
aktivitou	aktivita	k1gFnSc7	aktivita
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Unikátním	unikátní	k2eAgInSc7d1	unikátní
úkazem	úkaz	k1gInSc7	úkaz
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Neptunu	Neptun	k1gInSc2	Neptun
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
vysokých	vysoký	k2eAgInPc2d1	vysoký
oblaků	oblak	k1gInPc2	oblak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vrhají	vrhat	k5eAaImIp3nP	vrhat
stíny	stín	k1gInPc1	stín
na	na	k7c4	na
neprůhledné	průhledný	k2eNgFnPc4d1	neprůhledná
vrstvy	vrstva	k1gFnPc4	vrstva
pod	pod	k7c7	pod
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Měsíce	měsíc	k1gInSc2	měsíc
===	===	k?	===
</s>
</p>
<p>
<s>
Dostatečně	dostatečně	k6eAd1	dostatečně
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
oblaků	oblak	k1gInPc2	oblak
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
Saturnův	Saturnův	k2eAgInSc4d1	Saturnův
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
atmosféra	atmosféra	k1gFnSc1	atmosféra
obsahující	obsahující	k2eAgFnSc1d1	obsahující
především	především	k9	především
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
metan	metan	k1gInSc4	metan
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
je	být	k5eAaImIp3nS	být
neprůhledná	průhledný	k2eNgFnSc1d1	neprůhledná
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
objevila	objevit	k5eAaPmAgFnS	objevit
gigantický	gigantický	k2eAgInSc4d1	gigantický
oblak	oblak	k1gInSc4	oblak
nad	nad	k7c7	nad
severním	severní	k2eAgInSc7d1	severní
pólem	pól	k1gInSc7	pól
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
až	až	k9	až
2400	[number]	k4	2400
km	km	kA	km
a	a	k8xC	a
sahá	sahat	k5eAaImIp3nS	sahat
po	po	k7c4	po
30	[number]	k4	30
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
oblaku	oblak	k1gInSc2	oblak
prší	pršet	k5eAaImIp3nP	pršet
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
hromadící	hromadící	k2eAgInPc1d1	hromadící
se	se	k3xPyFc4	se
v	v	k7c6	v
povrchových	povrchový	k2eAgNnPc6d1	povrchové
jezerech	jezero	k1gNnPc6	jezero
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
také	také	k9	také
objevila	objevit	k5eAaPmAgFnS	objevit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblaka	oblaka	k1gNnPc1	oblaka
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
nejchladnějšího	chladný	k2eAgNnSc2d3	nejchladnější
prozkoumaného	prozkoumaný	k2eAgNnSc2d1	prozkoumané
tělesa	těleso	k1gNnSc2	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
Neptunova	Neptunův	k2eAgInSc2d1	Neptunův
měsíce	měsíc	k1gInSc2	měsíc
Tritonu	triton	k1gInSc2	triton
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
krystalického	krystalický	k2eAgInSc2d1	krystalický
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Sondě	sonda	k1gFnSc3	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
jeden	jeden	k4xCgInSc4	jeden
oblak	oblak	k1gInSc4	oblak
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
disku	disk	k1gInSc2	disk
měsíce	měsíc	k1gInPc4	měsíc
vyfotografovat	vyfotografovat	k5eAaPmF	vyfotografovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
okolo	okolo	k7c2	okolo
něj	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
přelétala	přelétat	k5eAaPmAgFnS	přelétat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Oblak	oblak	k1gInSc1	oblak
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
oblak	oblaka	k1gNnPc2	oblaka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
oblak	oblaka	k1gNnPc2	oblaka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
oblak	oblak	k1gInSc1	oblak
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
oblaků	oblak	k1gInPc2	oblak
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnPc1	galerie
druhů	druh	k1gInPc2	druh
oblaků	oblak	k1gInPc2	oblak
</s>
</p>
