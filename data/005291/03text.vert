<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
С	С	k?	С
С	С	k?	С
<g/>
,	,	kIx,	,
Sovětskij	Sovětskij	k1gMnSc1	Sovětskij
Sojuz	Sojuz	k1gInSc1	Sojuz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Svaz	svaz	k1gInSc1	svaz
sovětských	sovětský	k2eAgFnPc2d1	sovětská
socialistických	socialistický	k2eAgFnPc2d1	socialistická
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
С	С	k?	С
<g/>
́	́	k?	́
<g/>
з	з	k?	з
С	С	k?	С
<g/>
́	́	k?	́
<g/>
т	т	k?	т
С	С	k?	С
<g/>
́	́	k?	́
<g/>
ч	ч	k?	ч
Р	Р	k?	Р
<g/>
́	́	k?	́
<g/>
б	б	k?	б
<g/>
,	,	kIx,	,
С	С	k?	С
<g/>
,	,	kIx,	,
Sojuz	Sojuz	k1gInSc1	Sojuz
Sovětskich	Sovětskich	k1gMnSc1	Sovětskich
Socialističeskich	Socialističeskich	k1gMnSc1	Socialističeskich
Respublik	Respublik	k1gMnSc1	Respublik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
eurasijský	eurasijský	k2eAgInSc1d1	eurasijský
stát	stát	k1gInSc1	stát
se	s	k7c7	s
socialistickým	socialistický	k2eAgNnSc7d1	socialistické
zřízením	zřízení	k1gNnSc7	zřízení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
existoval	existovat	k5eAaImAgInS	existovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1922	[number]	k4	1922
až	až	k6eAd1	až
1991	[number]	k4	1991
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
měl	mít	k5eAaImAgInS	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
ústavě	ústava	k1gFnSc6	ústava
zakotvenou	zakotvený	k2eAgFnSc4d1	zakotvená
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
roli	role	k1gFnSc4	role
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
dominantní	dominantní	k2eAgFnSc7d1	dominantní
politickou	politický	k2eAgFnSc7d1	politická
silou	síla	k1gFnSc7	síla
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nominálně	nominálně	k6eAd1	nominálně
unií	unie	k1gFnSc7	unie
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
silně	silně	k6eAd1	silně
centralizovaný	centralizovaný	k2eAgInSc4d1	centralizovaný
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Říjnová	říjnový	k2eAgFnSc1d1	říjnová
revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1917	[number]	k4	1917
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
odstranění	odstranění	k1gNnSc2	odstranění
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
bolševici	bolševik	k1gMnPc1	bolševik
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1922	[number]	k4	1922
založen	založen	k2eAgInSc1d1	založen
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
sdružující	sdružující	k2eAgFnSc4d1	sdružující
Ruskou	ruský	k2eAgFnSc4d1	ruská
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
federativní	federativní	k2eAgFnSc4d1	federativní
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
Ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
<g/>
,	,	kIx,	,
Běloruskou	běloruský	k2eAgFnSc4d1	Běloruská
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
Zakavkazskou	zakavkazský	k2eAgFnSc4d1	zakavkazská
federativní	federativní	k2eAgFnSc4d1	federativní
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
další	další	k2eAgFnPc1d1	další
sovětské	sovětský	k2eAgFnPc1d1	sovětská
republiky	republika	k1gFnPc1	republika
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
prvního	první	k4xOgMnSc2	první
sovětského	sovětský	k2eAgMnSc2d1	sovětský
vůdce	vůdce	k1gMnSc2	vůdce
Vladimira	Vladimir	k1gMnSc2	Vladimir
Iljiče	Iljič	k1gMnSc2	Iljič
Lenina	Lenin	k1gMnSc2	Lenin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
dalším	další	k2eAgMnSc7d1	další
vůdcem	vůdce	k1gMnSc7	vůdce
země	zem	k1gFnSc2	zem
stal	stát	k5eAaPmAgMnS	stát
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zahájil	zahájit	k5eAaPmAgMnS	zahájit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
industrializaci	industrializace	k1gFnSc4	industrializace
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
plánovanou	plánovaný	k2eAgFnSc7d1	plánovaná
ekonomikou	ekonomika	k1gFnSc7	ekonomika
a	a	k8xC	a
politickým	politický	k2eAgInSc7d1	politický
útlakem	útlak	k1gInSc7	útlak
<g/>
;	;	kIx,	;
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
gulazích	gulag	k1gInPc6	gulag
tehdy	tehdy	k6eAd1	tehdy
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
několik	několik	k4yIc1	několik
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
spojenci	spojenec	k1gMnPc1	spojenec
napadli	napadnout	k5eAaPmAgMnP	napadnout
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
dříve	dříve	k6eAd2	dříve
podepsali	podepsat	k5eAaPmAgMnP	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
následujících	následující	k2eAgNnPc2d1	následující
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
těžkých	těžký	k2eAgInPc2d1	těžký
bojů	boj	k1gInPc2	boj
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
vyšel	vyjít	k5eAaPmAgInS	vyjít
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
světových	světový	k2eAgFnPc2d1	světová
supervelmocí	supervelmoc	k1gFnPc2	supervelmoc
<g/>
;	;	kIx,	;
tou	ten	k3xDgFnSc7	ten
druhou	druhý	k4xOgFnSc4	druhý
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
získal	získat	k5eAaPmAgMnS	získat
(	(	kIx(	(
<g/>
staro	staro	k6eAd1	staro
<g/>
)	)	kIx)	)
<g/>
nová	nový	k2eAgNnPc4d1	nové
území	území	k1gNnPc4	území
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
<g/>
,	,	kIx,	,
a	a	k8xC	a
počet	počet	k1gInSc1	počet
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
na	na	k7c4	na
16	[number]	k4	16
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgInS	ustálit
na	na	k7c4	na
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
satelitními	satelitní	k2eAgFnPc7d1	satelitní
zeměmi	zem	k1gFnPc7	zem
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
ideologickém	ideologický	k2eAgInSc6d1	ideologický
a	a	k8xC	a
politickém	politický	k2eAgInSc6d1	politický
boji	boj	k1gInSc6	boj
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nakonec	nakonec	k6eAd1	nakonec
neuspěl	uspět	k5eNaPmAgMnS	uspět
kvůli	kvůli	k7c3	kvůli
ekonomickým	ekonomický	k2eAgInPc3d1	ekonomický
problémům	problém	k1gInPc3	problém
a	a	k8xC	a
také	také	k9	také
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
a	a	k8xC	a
domácím	domácí	k2eAgInPc3d1	domácí
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
poslední	poslední	k2eAgMnSc1d1	poslední
sovětský	sovětský	k2eAgMnSc1d1	sovětský
vůdce	vůdce	k1gMnSc1	vůdce
Michail	Michail	k1gMnSc1	Michail
Gorbačov	Gorbačov	k1gInSc4	Gorbačov
pokusil	pokusit	k5eAaPmAgMnS	pokusit
reformovat	reformovat	k5eAaBmF	reformovat
stát	stát	k5eAaPmF	stát
politikou	politika	k1gFnSc7	politika
perestrojky	perestrojka	k1gFnSc2	perestrojka
a	a	k8xC	a
glasnosti	glasnost	k1gFnSc2	glasnost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
rozpuštěn	rozpustit	k5eAaPmNgInS	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Práva	právo	k1gNnPc4	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
převzala	převzít	k5eAaPmAgFnS	převzít
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnPc4	geografie
<g/>
,	,	kIx,	,
klima	klima	k1gNnSc4	klima
a	a	k8xC	a
prostředí	prostředí	k1gNnSc4	prostředí
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
22 402 200	[number]	k4	22 402 200
km2	km2	k4	km2
byl	být	k5eAaImAgMnS	být
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
status	status	k1gInSc1	status
zůstal	zůstat	k5eAaPmAgInS	zůstat
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Pokrýval	pokrývat	k5eAaImAgInS	pokrývat
šestinu	šestina	k1gFnSc4	šestina
suchozemského	suchozemský	k2eAgInSc2d1	suchozemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc4	velikost
byla	být	k5eAaImAgFnS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
část	část	k1gFnSc1	část
představovala	představovat	k5eAaImAgFnS	představovat
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
kulturním	kulturní	k2eAgNnSc7d1	kulturní
a	a	k8xC	a
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
k	k	k7c3	k
Tichému	tichý	k2eAgInSc3d1	tichý
oceánu	oceán	k1gInSc3	oceán
a	a	k8xC	a
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
některých	některý	k3yIgFnPc2	některý
oblastí	oblast	k1gFnPc2	oblast
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
byla	být	k5eAaImAgFnS	být
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
obydlená	obydlený	k2eAgFnSc1d1	obydlená
<g/>
.	.	kIx.	.
</s>
<s>
Táhl	táhnout	k5eAaImAgInS	táhnout
se	se	k3xPyFc4	se
od	od	k7c2	od
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
přes	přes	k7c4	přes
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
10 000	[number]	k4	10 000
kilometrů	kilometr	k1gInPc2	kilometr
11	[number]	k4	11
časovými	časový	k2eAgNnPc7d1	časové
pásmy	pásmo	k1gNnPc7	pásmo
a	a	k8xC	a
přes	přes	k7c4	přes
7 200	[number]	k4	7 200
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
do	do	k7c2	do
pěti	pět	k4xCc2	pět
klimatických	klimatický	k2eAgFnPc2d1	klimatická
zón	zóna	k1gFnPc2	zóna
<g/>
:	:	kIx,	:
tundra	tundra	k1gFnSc1	tundra
<g/>
,	,	kIx,	,
tajga	tajga	k1gFnSc1	tajga
<g/>
,	,	kIx,	,
step	step	k1gFnSc1	step
<g/>
,	,	kIx,	,
poušť	poušť	k1gFnSc1	poušť
a	a	k8xC	a
hory	hora	k1gFnPc1	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
měl	mít	k5eAaImAgInS	mít
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
hranici	hranice	k1gFnSc4	hranice
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Měřila	měřit	k5eAaImAgFnS	měřit
přes	přes	k7c4	přes
60 000	[number]	k4	60 000
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
obvodu	obvod	k1gInSc2	obvod
Země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
zaujímalo	zaujímat	k5eAaImAgNnS	zaujímat
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Beringovým	Beringův	k2eAgInSc7d1	Beringův
průlivem	průliv	k1gInSc7	průliv
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgInP	nacházet
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
až	až	k9	až
1991	[number]	k4	1991
hraničil	hraničit	k5eAaImAgInS	hraničit
se	s	k7c7	s
zeměmi	zem	k1gFnPc7	zem
jako	jako	k9	jako
Afghánistán	Afghánistán	k1gInSc4	Afghánistán
<g/>
,	,	kIx,	,
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
horou	hora	k1gFnSc7	hora
SSSR	SSSR	kA	SSSR
byla	být	k5eAaImAgFnS	být
Pik	pik	k1gInSc4	pik
Kommunizma	Kommunizmum	k1gNnSc2	Kommunizmum
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Qullai	Qulla	k1gFnSc3	Qulla
Ismoili	Ismoili	k1gFnPc2	Ismoili
Somoni	Somon	k1gMnPc1	Somon
<g/>
)	)	kIx)	)
v	v	k7c6	v
Tádžikistánu	Tádžikistán	k1gInSc6	Tádžikistán
s	s	k7c7	s
7 495	[number]	k4	7 495
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
také	také	k9	také
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
většinu	většina	k1gFnSc4	většina
největších	veliký	k2eAgNnPc2d3	veliký
světových	světový	k2eAgNnPc2d1	světové
jezer	jezero	k1gNnPc2	jezero
<g/>
;	;	kIx,	;
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Íránem	Írán	k1gInSc7	Írán
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc1d3	veliký
a	a	k8xC	a
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
sladkovodní	sladkovodní	k2eAgNnSc1d1	sladkovodní
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
vodním	vodní	k2eAgInSc7d1	vodní
zdrojem	zdroj	k1gInSc7	zdroj
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
SSSR	SSSR	kA	SSSR
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
Ruské	ruský	k2eAgNnSc1d1	ruské
impérium	impérium	k1gNnSc1	impérium
v	v	k7c6	v
chaosu	chaos	k1gInSc6	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
existovala	existovat	k5eAaImAgFnS	existovat
již	již	k6eAd1	již
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
uspořádat	uspořádat	k5eAaPmF	uspořádat
demokratické	demokratický	k2eAgFnPc4d1	demokratická
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
bolševici	bolševik	k1gMnPc1	bolševik
ustanovili	ustanovit	k5eAaPmAgMnP	ustanovit
okamžitě	okamžitě	k6eAd1	okamžitě
první	první	k4xOgInSc1	první
sovět	sovět	k1gInSc1	sovět
a	a	k8xC	a
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
Lenina	Lenin	k1gMnSc2	Lenin
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
začali	začít	k5eAaPmAgMnP	začít
postupně	postupně	k6eAd1	postupně
a	a	k8xC	a
získávat	získávat	k5eAaImF	získávat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
rudá	rudý	k2eAgFnSc1d1	rudá
vláda	vláda	k1gFnSc1	vláda
anulovala	anulovat	k5eAaBmAgFnS	anulovat
dluhy	dluh	k1gInPc4	dluh
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jako	jako	k8xC	jako
obrannou	obranný	k2eAgFnSc4d1	obranná
složku	složka	k1gFnSc4	složka
zřídili	zřídit	k5eAaPmAgMnP	zřídit
komunisté	komunista	k1gMnPc1	komunista
Rudou	rudý	k2eAgFnSc4d1	rudá
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
dalšími	další	k2eAgInPc7d1	další
dekrety	dekret	k1gInPc7	dekret
byla	být	k5eAaImAgFnS	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
odluka	odluka	k1gFnSc1	odluka
církve	církev	k1gFnSc2	církev
od	od	k7c2	od
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Ruská	ruský	k2eAgFnSc1d1	ruská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
federativní	federativní	k2eAgFnSc1d1	federativní
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
RSFSR	RSFSR	kA	RSFSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3.	[number]	k4	3.
března	březen	k1gInSc2	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
podepsali	podepsat	k5eAaPmAgMnP	podepsat
v	v	k7c6	v
Brest-Litevsku	Brest-Litevsko	k1gNnSc6	Brest-Litevsko
zástupci	zástupce	k1gMnPc1	zástupce
sovětského	sovětský	k2eAgNnSc2d1	sovětské
Ruska	Rusko	k1gNnSc2	Rusko
separátní	separátní	k2eAgFnSc4d1	separátní
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
(	(	kIx(	(
<g/>
Brestlitevský	brestlitevský	k2eAgInSc1d1	brestlitevský
mír	mír	k1gInSc1	mír
<g/>
)	)	kIx)	)
ukončující	ukončující	k2eAgFnSc1d1	ukončující
účast	účast	k1gFnSc1	účast
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
podmínek	podmínka	k1gFnPc2	podmínka
separátního	separátní	k2eAgInSc2d1	separátní
míru	mír	k1gInSc2	mír
přišlo	přijít	k5eAaPmAgNnS	přijít
Rusko	Rusko	k1gNnSc1	Rusko
o	o	k7c4	o
Finsko	Finsko	k1gNnSc4	Finsko
<g/>
,	,	kIx,	,
Pobaltí	Pobaltí	k1gNnSc4	Pobaltí
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
a	a	k8xC	a
Besarábii	Besarábie	k1gFnSc4	Besarábie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
komunistům	komunista	k1gMnPc3	komunista
povstalo	povstat	k5eAaPmAgNnS	povstat
mnoho	mnoho	k4c1	mnoho
jejich	jejich	k3xOp3gMnPc2	jejich
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
však	však	k9	však
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
nejednotní	jednotný	k2eNgMnPc1d1	nejednotný
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nemohli	moct	k5eNaImAgMnP	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
bolševiky	bolševik	k1gMnPc4	bolševik
silným	silný	k2eAgMnSc7d1	silný
protivníkem	protivník	k1gMnSc7	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
i	i	k9	i
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
intervencí	intervence	k1gFnPc2	intervence
vojsk	vojsko	k1gNnPc2	vojsko
Dohody	dohoda	k1gFnSc2	dohoda
dokázala	dokázat	k5eAaPmAgFnS	dokázat
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
odpor	odpor	k1gInSc4	odpor
potlačit	potlačit	k5eAaPmF	potlačit
a	a	k8xC	a
ustanovit	ustanovit	k5eAaPmF	ustanovit
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
moc	moc	k1gFnSc4	moc
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
Zakavkazska	Zakavkazsko	k1gNnSc2	Zakavkazsko
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
však	však	k9	však
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Rusko-polské	ruskoolský	k2eAgFnSc2d1	rusko-polská
války	válka	k1gFnSc2	válka
o	o	k7c6	o
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
<g/>
;	;	kIx,	;
po	po	k7c6	po
izolacionismu	izolacionismus	k1gInSc6	izolacionismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
spory	spora	k1gFnPc4	spora
ohledně	ohledně	k7c2	ohledně
neuznání	neuznání	k1gNnSc2	neuznání
starých	starý	k2eAgInPc2d1	starý
ruských	ruský	k2eAgInPc2d1	ruský
dluhů	dluh	k1gInPc2	dluh
<g/>
,	,	kIx,	,
navázala	navázat	k5eAaPmAgFnS	navázat
RSFSR	RSFSR	kA	RSFSR
postupně	postupně	k6eAd1	postupně
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
30.	[number]	k4	30.
prosince	prosinec	k1gInSc2	prosinec
1922	[number]	k4	1922
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
RSFSR	RSFSR	kA	RSFSR
<g/>
,	,	kIx,	,
Ukrajinské	ukrajinský	k2eAgInPc1d1	ukrajinský
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
Běloruské	běloruský	k2eAgFnPc4d1	Běloruská
SSR	SSR	kA	SSR
a	a	k8xC	a
Zakavkazské	zakavkazský	k2eAgFnSc2d1	zakavkazská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
stalinismu	stalinismus	k1gInSc2	stalinismus
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Vladimir	Vladimir	k1gMnSc1	Vladimir
Iljič	Iljič	k1gMnSc1	Iljič
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
Josif	Josif	k1gMnSc1	Josif
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
realizaci	realizace	k1gFnSc4	realizace
mnohých	mnohý	k2eAgInPc2d1	mnohý
velkolepých	velkolepý	k2eAgInPc2d1	velkolepý
plánů	plán	k1gInPc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
i	i	k8xC	i
muži	muž	k1gMnPc1	muž
byli	být	k5eAaImAgMnP	být
(	(	kIx(	(
<g/>
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
<g/>
)	)	kIx)	)
zrovnoprávněni	zrovnoprávněn	k2eAgMnPc1d1	zrovnoprávněn
<g/>
,	,	kIx,	,
zahájena	zahájit	k5eAaPmNgFnS	zahájit
byla	být	k5eAaImAgFnS	být
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
a	a	k8xC	a
industrializace	industrializace	k1gFnSc1	industrializace
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
vzdělání	vzdělání	k1gNnSc3	vzdělání
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ovlivněnému	ovlivněný	k2eAgInSc3d1	ovlivněný
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
ideologií	ideologie	k1gFnSc7	ideologie
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
přístup	přístup	k1gInSc4	přístup
všichni	všechen	k3xTgMnPc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
se	se	k3xPyFc4	se
i	i	k9	i
lékařská	lékařský	k2eAgFnSc1d1	lékařská
péče	péče	k1gFnSc1	péče
a	a	k8xC	a
likvidovány	likvidován	k2eAgFnPc1d1	likvidována
tak	tak	k6eAd1	tak
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
mnohé	mnohý	k2eAgFnPc4d1	mnohá
choroby	choroba	k1gFnPc4	choroba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
cholera	cholera	k1gFnSc1	cholera
<g/>
,	,	kIx,	,
malárie	malárie	k1gFnSc1	malárie
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
plánů	plán	k1gInPc2	plán
budování	budování	k1gNnSc2	budování
socialistického	socialistický	k2eAgInSc2d1	socialistický
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
násilná	násilný	k2eAgFnSc1d1	násilná
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zemi	zem	k1gFnSc4	zem
po	po	k7c6	po
následných	následný	k2eAgFnPc6d1	následná
chybách	chyba	k1gFnPc6	chyba
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
zemědělském	zemědělský	k2eAgInSc6d1	zemědělský
sektoru	sektor	k1gInSc6	sektor
postihl	postihnout	k5eAaPmAgInS	postihnout
hladomor	hladomor	k1gInSc1	hladomor
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vyvolaný	vyvolaný	k2eAgInSc4d1	vyvolaný
jako	jako	k8xC	jako
donucovací	donucovací	k2eAgInSc4d1	donucovací
nástroj	nástroj	k1gInSc4	nástroj
proti	proti	k7c3	proti
odpůrcům	odpůrce	k1gMnPc3	odpůrce
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
gulazích	gulag	k1gInPc6	gulag
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
zahynuly	zahynout	k5eAaPmAgInP	zahynout
miliony	milion	k4xCgInPc1	milion
odpůrců	odpůrce	k1gMnPc2	odpůrce
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Stalinem	Stalin	k1gMnSc7	Stalin
vyvolaný	vyvolaný	k2eAgMnSc1d1	vyvolaný
hladomor	hladomor	k1gMnSc1	hladomor
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1932-33	[number]	k4	1932-33
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
postihl	postihnout	k5eAaPmAgInS	postihnout
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgNnSc4d1	jižní
Rusko	Rusko	k1gNnSc4	Rusko
a	a	k8xC	a
severní	severní	k2eAgInSc4d1	severní
Kazachstán	Kazachstán	k1gInSc4	Kazachstán
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
až	až	k9	až
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkem	celkem	k6eAd1	celkem
měl	mít	k5eAaImAgMnS	mít
Stalinův	Stalinův	k2eAgInSc4d1	Stalinův
režim	režim	k1gInSc4	režim
ve	v	k7c6	v
30.	[number]	k4	30.
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
až	až	k9	až
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
akce	akce	k1gFnPc1	akce
snížily	snížit	k5eAaPmAgFnP	snížit
i	i	k9	i
jinak	jinak	k6eAd1	jinak
velký	velký	k2eAgInSc4d1	velký
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
snížily	snížit	k5eAaPmAgFnP	snížit
bojeschopnost	bojeschopnost	k1gFnSc4	bojeschopnost
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poškodily	poškodit	k5eAaPmAgInP	poškodit
i	i	k9	i
další	další	k2eAgFnPc1d1	další
důležité	důležitý	k2eAgFnPc1d1	důležitá
složky	složka	k1gFnPc1	složka
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zkolektivizováno	zkolektivizovat	k5eAaPmNgNnS	zkolektivizovat
90	[number]	k4	90
%	%	kIx~	%
veškerých	veškerý	k3xTgFnPc2	veškerý
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
hospodářství	hospodářství	k1gNnPc2	hospodářství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
a	a	k8xC	a
velkolepá	velkolepý	k2eAgFnSc1d1	velkolepá
industrializace	industrializace	k1gFnSc1	industrializace
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
zaostalých	zaostalý	k2eAgFnPc2d1	zaostalá
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
zemi	zem	k1gFnSc4	zem
pomoci	pomoc	k1gFnSc2	pomoc
vymanit	vymanit	k5eAaPmF	vymanit
se	se	k3xPyFc4	se
mnohde	mnohde	k6eAd1	mnohde
ze	z	k7c2	z
středověkých	středověký	k2eAgFnPc2d1	středověká
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Zaváděla	zavádět	k5eAaImAgFnS	zavádět
se	se	k3xPyFc4	se
elektřina	elektřina	k1gFnSc1	elektřina
<g/>
,	,	kIx,	,
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
se	se	k3xPyFc4	se
vzdělanost	vzdělanost	k1gFnSc1	vzdělanost
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
závody	závod	k1gInPc1	závod
schopné	schopný	k2eAgInPc1d1	schopný
vyrábět	vyrábět	k5eAaImF	vyrábět
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
techniku	technika	k1gFnSc4	technika
byly	být	k5eAaImAgFnP	být
odpovědí	odpověď	k1gFnSc7	odpověď
na	na	k7c4	na
vzrůstající	vzrůstající	k2eAgNnSc4d1	vzrůstající
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
podepsal	podepsat	k5eAaPmAgInS	podepsat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
<g/>
.	.	kIx.	.
<g/>
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
si	se	k3xPyFc3	se
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
získal	získat	k5eAaPmAgMnS	získat
mnoho	mnoho	k4c4	mnoho
sympatizantů	sympatizant	k1gMnPc2	sympatizant
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
evropské	evropský	k2eAgFnSc2d1	Evropská
levice	levice	k1gFnSc2	levice
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
časech	čas	k1gInPc6	čas
světové	světový	k2eAgFnSc2d1	světová
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
SSSR	SSSR	kA	SSSR
díky	díky	k7c3	díky
izolovanosti	izolovanost	k1gFnSc3	izolovanost
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
nepostihla	postihnout	k5eNaPmAgNnP	postihnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
jednal	jednat	k5eAaImAgInS	jednat
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nejprve	nejprve	k6eAd1	nejprve
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
zejména	zejména	k9	zejména
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
není	být	k5eNaImIp3nS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
podporovat	podporovat	k5eAaImF	podporovat
jeho	jeho	k3xOp3gFnSc3	jeho
expanzi	expanze	k1gFnSc3	expanze
vůči	vůči	k7c3	vůči
státům	stát	k1gInPc3	stát
spadajícím	spadající	k2eAgInPc3d1	spadající
dříve	dříve	k6eAd2	dříve
pod	pod	k7c4	pod
vládu	vláda	k1gFnSc4	vláda
carského	carský	k2eAgNnSc2d1	carské
impéria	impérium	k1gNnSc2	impérium
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
nacistické	nacistický	k2eAgNnSc4d1	nacistické
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
si	se	k3xPyFc3	se
nakonec	nakonec	k6eAd1	nakonec
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
rozdělení	rozdělení	k1gNnSc4	rozdělení
sfér	sféra	k1gFnPc2	sféra
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
paktu	pakt	k1gInSc2	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bez	bez	k7c2	bez
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
války	válka	k1gFnSc2	válka
přepadl	přepadnout	k5eAaPmAgMnS	přepadnout
17.	[number]	k4	17.
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabral	zabrat	k5eAaPmAgMnS	zabrat
východní	východní	k2eAgFnSc4d1	východní
polovinu	polovina	k1gFnSc4	polovina
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
též	též	k9	též
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
když	když	k8xS	když
nejprve	nejprve	k6eAd1	nejprve
přinutil	přinutit	k5eAaPmAgMnS	přinutit
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
a	a	k8xC	a
poté	poté	k6eAd1	poté
násilím	násilí	k1gNnSc7	násilí
zabral	zabrat	k5eAaPmAgMnS	zabrat
Estonsko	Estonsko	k1gNnSc4	Estonsko
<g/>
,	,	kIx,	,
Litvu	Litva	k1gFnSc4	Litva
a	a	k8xC	a
Lotyšsko	Lotyšsko	k1gNnSc4	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
dobytých	dobytý	k2eAgFnPc2d1	dobytá
a	a	k8xC	a
obsazených	obsazený	k2eAgFnPc2d1	obsazená
zemí	zem	k1gFnPc2	zem
rozsáhle	rozsáhle	k6eAd1	rozsáhle
dopouštěl	dopouštět	k5eAaImAgInS	dopouštět
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
a	a	k8xC	a
zločinů	zločin	k1gInPc2	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
válčil	válčit	k5eAaImAgInS	válčit
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Finskem	Finsko	k1gNnSc7	Finsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hned	hned	k6eAd1	hned
dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
a	a	k8xC	a
1940	[number]	k4	1940
–	–	k?	–
tzv.	tzv.	kA	tzv.
zimní	zimní	k2eAgFnSc1d1	zimní
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941	[number]	k4	1941
až	až	k9	až
1945	[number]	k4	1945
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
pokračovací	pokračovací	k2eAgFnSc1d1	pokračovací
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
přepadení	přepadení	k1gNnSc4	přepadení
Finska	Finsko	k1gNnSc2	Finsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
byl	být	k5eAaImAgMnS	být
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
ze	z	k7c2	z
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
byl	být	k5eAaImAgInS	být
nečekaně	nečekaně	k6eAd1	nečekaně
napaden	napadnout	k5eAaPmNgInS	napadnout
Německem	Německo	k1gNnSc7	Německo
22.	[number]	k4	22.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
nepřipravenosti	nepřipravenost	k1gFnSc3	nepřipravenost
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
postupovali	postupovat	k5eAaImAgMnP	postupovat
Němci	Němec	k1gMnPc1	Němec
úspěšně	úspěšně	k6eAd1	úspěšně
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Nacistickým	nacistický	k2eAgFnPc3d1	nacistická
silám	síla	k1gFnPc3	síla
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
obsadit	obsadit	k5eAaPmF	obsadit
západní	západní	k2eAgFnSc2d1	západní
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
též	též	k9	též
i	i	k9	i
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
RSFSR	RSFSR	kA	RSFSR
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
až	až	k9	až
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
i	i	k9	i
díky	díky	k7c3	díky
pomoci	pomoc	k1gFnSc3	pomoc
západních	západní	k2eAgMnPc2d1	západní
Spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
přesunu	přesun	k1gInSc2	přesun
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
závodů	závod	k1gInPc2	závod
do	do	k7c2	do
bezpečných	bezpečný	k2eAgFnPc2d1	bezpečná
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Uralu	Ural	k1gInSc2	Ural
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
obrátila	obrátit	k5eAaPmAgFnS	obrátit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Němce	Němec	k1gMnPc4	Němec
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
přes	přes	k7c4	přes
východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
až	až	k9	až
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
utrpěl	utrpět	k5eAaPmAgInS	utrpět
80	[number]	k4	80
%	%	kIx~	%
veškerých	veškerý	k3xTgFnPc2	veškerý
vojenských	vojenský	k2eAgFnPc2d1	vojenská
ztrát	ztráta	k1gFnPc2	ztráta
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
dohod	dohoda	k1gFnPc2	dohoda
jaltské	jaltský	k2eAgFnSc2d1	Jaltská
konference	konference	k1gFnSc2	konference
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
dne	den	k1gInSc2	den
8.	[number]	k4	8.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
válku	válka	k1gFnSc4	válka
Japonsku	Japonsko	k1gNnSc3	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
týdne	týden	k1gInSc2	týden
rozdrtil	rozdrtit	k5eAaPmAgInS	rozdrtit
ve	v	k7c6	v
skvěle	skvěle	k6eAd1	skvěle
naplánované	naplánovaný	k2eAgFnSc6d1	naplánovaná
a	a	k8xC	a
provedené	provedený	k2eAgFnSc6d1	provedená
operaci	operace	k1gFnSc6	operace
Kuantungskou	Kuantungský	k2eAgFnSc4d1	Kuantungský
armádu	armáda	k1gFnSc4	armáda
v	v	k7c6	v
Mandžusku	Mandžusko	k1gNnSc6	Mandžusko
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
obklíčené	obklíčený	k2eAgInPc1d1	obklíčený
zbytky	zbytek	k1gInPc1	zbytek
se	se	k3xPyFc4	se
dne	den	k1gInSc2	den
20.	[number]	k4	20.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
vzdaly	vzdát	k5eAaPmAgFnP	vzdát
(	(	kIx(	(
<g/>
na	na	k7c6	na
Sachalinu	Sachalin	k1gInSc6	Sachalin
tak	tak	k6eAd1	tak
učinily	učinit	k5eAaImAgInP	učinit
25.	[number]	k4	25.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
na	na	k7c6	na
posledním	poslední	k2eAgMnSc6d1	poslední
z	z	k7c2	z
Kurilských	kurilský	k2eAgInPc2d1	kurilský
ostrovů	ostrov	k1gInPc2	ostrov
1.	[number]	k4	1.
září	září	k1gNnSc2	září
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
2.	[number]	k4	2.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
v	v	k7c6	v
boji	boj	k1gInSc6	boj
8,7	[number]	k4	8,7
milionů	milion	k4xCgInPc2	milion
vojáků	voják	k1gMnPc2	voják
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
5,8	[number]	k4	5,8
milionů	milion	k4xCgInPc2	milion
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
1,4	[number]	k4	1,4
milionů	milion	k4xCgInPc2	milion
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
a	a	k8xC	a
1,5	[number]	k4	1,5
milionů	milion	k4xCgInPc2	milion
příslušníků	příslušník	k1gMnPc2	příslušník
ostatních	ostatní	k2eAgInPc2d1	ostatní
sovětských	sovětský	k2eAgInPc2d1	sovětský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941-42	[number]	k4	1941-42
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
zajetí	zajetí	k1gNnSc6	zajetí
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
okolo	okolo	k7c2	okolo
3	[number]	k4	3
milionů	milion	k4xCgInPc2	milion
sovětských	sovětský	k2eAgMnPc2d1	sovětský
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
německého	německý	k2eAgInSc2d1	německý
plánu	plán	k1gInSc2	plán
genocidy	genocida	k1gFnSc2	genocida
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
tzv.	tzv.	kA	tzv.
Generalplan	Generalplan	k1gInSc1	Generalplan
Ost	Ost	k1gMnPc2	Ost
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
spojenci	spojenec	k1gMnPc1	spojenec
se	se	k3xPyFc4	se
na	na	k7c6	na
sovětském	sovětský	k2eAgNnSc6d1	sovětské
území	území	k1gNnSc6	území
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
dopouštěli	dopouštět	k5eAaImAgMnP	dopouštět
válečných	válečný	k2eAgMnPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
a	a	k8xC	a
zločinů	zločin	k1gInPc2	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Oděse	Oděsa	k1gFnSc6	Oděsa
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
rumunské	rumunský	k2eAgFnSc2d1	rumunská
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
jednotky	jednotka	k1gFnSc2	jednotka
zastřelily	zastřelit	k5eAaPmAgFnP	zastřelit
nebo	nebo	k8xC	nebo
upálily	upálit	k5eAaPmAgFnP	upálit
až	až	k9	až
34 000	[number]	k4	34 000
oděských	oděský	k2eAgMnPc2d1	oděský
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obležení	obležení	k1gNnSc1	obležení
Leningradu	Leningrad	k1gInSc2	Leningrad
německými	německý	k2eAgNnPc7d1	německé
a	a	k8xC	a
finskými	finský	k2eAgNnPc7d1	finské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
životy	život	k1gInPc4	život
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
byly	být	k5eAaImAgFnP	být
vyvražděny	vyvražděn	k2eAgFnPc1d1	vyvražděna
stovky	stovka	k1gFnPc1	stovka
vesnic	vesnice	k1gFnPc2	vesnice
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
zahynula	zahynout	k5eAaPmAgFnS	zahynout
asi	asi	k9	asi
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
běloruské	běloruský	k2eAgFnSc2d1	Běloruská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Poválečné	poválečný	k2eAgInPc4d1	poválečný
úspěchy	úspěch	k1gInPc4	úspěch
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
střední	střední	k2eAgFnSc7d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
,	,	kIx,	,
za	za	k7c2	za
které	který	k3yIgFnSc2	který
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
26	[number]	k4	26
miliony	milion	k4xCgInPc1	milion
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
zničenou	zničený	k2eAgFnSc7d1	zničená
velkou	velký	k2eAgFnSc7d1	velká
částí	část	k1gFnSc7	část
země	zem	k1gFnSc2	zem
a	a	k8xC	a
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
SSSR	SSSR	kA	SSSR
ohromnou	ohromný	k2eAgFnSc4d1	ohromná
prestiž	prestiž	k1gFnSc4	prestiž
a	a	k8xC	a
komunisté	komunista	k1gMnPc1	komunista
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
vnímáni	vnímán	k2eAgMnPc1d1	vnímán
jako	jako	k8xS	jako
zachránci	zachránce	k1gMnPc1	zachránce
lidstva	lidstvo	k1gNnSc2	lidstvo
před	před	k7c7	před
nacismem	nacismus	k1gInSc7	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Nadšení	nadšení	k1gNnSc4	nadšení
však	však	k9	však
rychle	rychle	k6eAd1	rychle
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
zklamání	zklamání	k1gNnSc1	zklamání
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
si	se	k3xPyFc3	se
osvobozené	osvobozený	k2eAgFnPc4d1	osvobozená
země	zem	k1gFnPc4	zem
podrobil	podrobit	k5eAaPmAgMnS	podrobit
a	a	k8xC	a
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pouze	pouze	k6eAd1	pouze
formálně	formálně	k6eAd1	formálně
nezávislé	závislý	k2eNgInPc1d1	nezávislý
satelity	satelit	k1gInPc1	satelit
s	s	k7c7	s
komunistickým	komunistický	k2eAgNnSc7d1	komunistické
zřízením	zřízení	k1gNnSc7	zřízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
rychle	rychle	k6eAd1	rychle
tempo	tempo	k1gNnSc1	tempo
industrializace	industrializace	k1gFnSc2	industrializace
započaté	započatý	k2eAgFnSc2d1	započatá
již	již	k6eAd1	již
v	v	k7c6	v
30.	[number]	k4	30.
letech	léto	k1gNnPc6	léto
Stalinem	Stalin	k1gMnSc7	Stalin
<g/>
,	,	kIx,	,
přicházely	přicházet	k5eAaImAgFnP	přicházet
velké	velký	k2eAgInPc4d1	velký
úspěchy	úspěch	k1gInPc4	úspěch
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
sestrojení	sestrojení	k1gNnSc1	sestrojení
jaderné	jaderný	k2eAgFnSc2d1	jaderná
bomby	bomba	k1gFnSc2	bomba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stavěla	stavět	k5eAaImAgNnP	stavět
se	se	k3xPyFc4	se
nová	nový	k2eAgNnPc1d1	nové
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
elektrárny	elektrárna	k1gFnPc1	elektrárna
i	i	k8xC	i
továrny	továrna	k1gFnPc1	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
byl	být	k5eAaImAgInS	být
však	však	k9	však
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
tuhý	tuhý	k2eAgInSc1d1	tuhý
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
určité	určitý	k2eAgNnSc4d1	určité
nadšení	nadšení	k1gNnSc4	nadšení
z	z	k7c2	z
porážky	porážka	k1gFnSc2	porážka
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
plánovitě	plánovitě	k6eAd1	plánovitě
šířit	šířit	k5eAaImF	šířit
nenávist	nenávist	k1gFnSc4	nenávist
k	k	k7c3	k
západním	západní	k2eAgFnPc3d1	západní
zemím	zem	k1gFnPc3	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
válce	válka	k1gFnSc3	válka
se	se	k3xPyFc4	se
SSSR	SSSR	kA	SSSR
podařilo	podařit	k5eAaPmAgNnS	podařit
rozšířit	rozšířit	k5eAaPmF	rozšířit
socialismus	socialismus	k1gInSc4	socialismus
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
či	či	k8xC	či
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
(	(	kIx(	(
<g/>
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
komunistické	komunistický	k2eAgFnPc1d1	komunistická
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
spolupracující	spolupracující	k2eAgInPc1d1	spolupracující
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
usilující	usilující	k2eAgFnPc1d1	usilující
o	o	k7c4	o
nastolení	nastolení	k1gNnSc4	nastolení
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
soupeřil	soupeřit	k5eAaImAgInS	soupeřit
s	s	k7c7	s
USA	USA	kA	USA
v	v	k7c6	v
kosmickém	kosmický	k2eAgInSc6d1	kosmický
výzkumu	výzkum	k1gInSc6	výzkum
(	(	kIx(	(
<g/>
vyslání	vyslání	k1gNnSc6	vyslání
prvního	první	k4xOgMnSc2	první
člověka	člověk	k1gMnSc2	člověk
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ideologicky	ideologicky	k6eAd1	ideologicky
(	(	kIx(	(
<g/>
souboj	souboj	k1gInSc1	souboj
–	–	k?	–
podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
mnohých	mnohý	k2eAgMnPc2d1	mnohý
sovětských	sovětský	k2eAgMnPc2d1	sovětský
politiků	politik	k1gMnPc2	politik
–	–	k?	–
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
,	,	kIx,	,
či	či	k8xC	či
–	–	k?	–
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
bývalo	bývat	k5eAaImAgNnS	bývat
označováno	označovat	k5eAaImNgNnS	označovat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
–	–	k?	–
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
totality	totalita	k1gFnSc2	totalita
<g/>
)	)	kIx)	)
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
vojensky	vojensky	k6eAd1	vojensky
<g/>
,	,	kIx,	,
podporou	podpora	k1gFnSc7	podpora
svých	svůj	k3xOyFgMnPc2	svůj
spojenců	spojenec	k1gMnPc2	spojenec
(	(	kIx(	(
<g/>
korejská	korejský	k2eAgFnSc1d1	Korejská
či	či	k8xC	či
vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
arabsko-izraelský	arabskozraelský	k2eAgInSc1d1	arabsko-izraelský
konflikt	konflikt	k1gInSc1	konflikt
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k1gNnPc2	další
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgMnS	stát
bipolárním	bipolární	k2eAgNnSc7d1	bipolární
<g/>
;	;	kIx,	;
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dvě	dva	k4xCgFnPc1	dva
sféry	sféra	k1gFnPc1	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
sovětská	sovětský	k2eAgFnSc1d1	sovětská
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zánik	zánik	k1gInSc1	zánik
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
boji	boj	k1gInSc6	boj
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
SSSR	SSSR	kA	SSSR
ekonomicky	ekonomicky	k6eAd1	ekonomicky
postupně	postupně	k6eAd1	postupně
vyčerpáván	vyčerpáván	k2eAgInSc1d1	vyčerpáván
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
takových	takový	k3xDgNnPc2	takový
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
pak	pak	k6eAd1	pak
nastal	nastat	k5eAaPmAgInS	nastat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
80.	[number]	k4	80.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
politikou	politika	k1gFnSc7	politika
cíleného	cílený	k2eAgNnSc2d1	cílené
zbrojení	zbrojení	k1gNnSc2	zbrojení
dokázal	dokázat	k5eAaPmAgMnS	dokázat
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
donutit	donutit	k5eAaPmF	donutit
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
vynakládat	vynakládat	k5eAaImF	vynakládat
na	na	k7c4	na
zbrojení	zbrojení	k1gNnSc4	zbrojení
až	až	k8xS	až
desítky	desítka	k1gFnPc4	desítka
procent	procento	k1gNnPc2	procento
HDP	HDP	kA	HDP
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
generálové	generál	k1gMnPc1	generál
postupně	postupně	k6eAd1	postupně
prohrávali	prohrávat	k5eAaImAgMnP	prohrávat
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
následně	následně	k6eAd1	následně
projevilo	projevit	k5eAaPmAgNnS	projevit
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
poklesem	pokles	k1gInSc7	pokles
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nedostatky	nedostatek	k1gInPc7	nedostatek
socialistické	socialistický	k2eAgFnSc2d1	socialistická
centrálně	centrálně	k6eAd1	centrálně
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
ekonomiky	ekonomika	k1gFnSc2	ekonomika
úpadkem	úpadek	k1gInSc7	úpadek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1986	[number]	k4	1986
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Černobylská	černobylský	k2eAgFnSc1d1	Černobylská
havárie	havárie	k1gFnSc1	havárie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prokázala	prokázat	k5eAaPmAgFnS	prokázat
neschopnost	neschopnost	k1gFnSc4	neschopnost
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
nepřipravenost	nepřipravenost	k1gFnSc4	nepřipravenost
klíčových	klíčový	k2eAgNnPc2d1	klíčové
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
čelit	čelit	k5eAaImF	čelit
události	událost	k1gFnPc4	událost
takového	takový	k3xDgInSc2	takový
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
země	země	k1gFnSc1	země
jako	jako	k8xS	jako
samotná	samotný	k2eAgFnSc1d1	samotná
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
oficiálně	oficiálně	k6eAd1	oficiálně
připravená	připravený	k2eAgFnSc1d1	připravená
stále	stále	k6eAd1	stále
i	i	k9	i
na	na	k7c4	na
jadernou	jaderný	k2eAgFnSc4d1	jaderná
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
území	území	k1gNnSc6	území
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
ropnému	ropný	k2eAgInSc3d1	ropný
zlomu	zlom	k1gInSc3	zlom
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
produkce	produkce	k1gFnSc1	produkce
ropy	ropa	k1gFnSc2	ropa
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svého	svůj	k3xOyFgNnSc2	svůj
historického	historický	k2eAgNnSc2d1	historické
maxima	maximum	k1gNnSc2	maximum
a	a	k8xC	a
od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
okamžiku	okamžik	k1gInSc2	okamžik
začala	začít	k5eAaPmAgFnS	začít
klesat	klesat	k5eAaImF	klesat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
ropě	ropa	k1gFnSc6	ropa
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
)	)	kIx)	)
nepřestala	přestat	k5eNaPmAgFnS	přestat
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
významný	významný	k2eAgInSc1d1	významný
překmit	překmit	k1gInSc1	překmit
prakticky	prakticky	k6eAd1	prakticky
nebyl	být	k5eNaImAgInS	být
nijak	nijak	k6eAd1	nijak
korigován	korigovat	k5eAaBmNgInS	korigovat
a	a	k8xC	a
během	během	k7c2	během
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
až	až	k6eAd1	až
dvou	dva	k4xCgInPc2	dva
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
příčinou	příčina	k1gFnSc7	příčina
vážných	vážný	k2eAgInPc2d1	vážný
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
problémů	problém	k1gInPc2	problém
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
alokací	alokace	k1gFnSc7	alokace
potřebné	potřebný	k2eAgFnSc2d1	potřebná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
nabyly	nabýt	k5eAaPmAgFnP	nabýt
všechny	všechen	k3xTgInPc4	všechen
tyto	tento	k3xDgInPc4	tento
trendy	trend	k1gInPc4	trend
a	a	k8xC	a
vlivy	vliv	k1gInPc4	vliv
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
konkrétnějších	konkrétní	k2eAgFnPc2d2	konkrétnější
podob	podoba	k1gFnPc2	podoba
–	–	k?	–
synergii	synergie	k1gFnSc3	synergie
neudržitelnosti	neudržitelnost	k1gFnSc2	neudržitelnost
stávajícího	stávající	k2eAgInSc2d1	stávající
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
"	"	kIx"	"
<g/>
vítr	vítr	k1gInSc1	vítr
změny	změna	k1gFnSc2	změna
<g/>
"	"	kIx"	"
začaly	začít	k5eAaPmAgInP	začít
cítit	cítit	k5eAaImF	cítit
státy	stát	k1gInPc4	stát
pod	pod	k7c7	pod
sovětským	sovětský	k2eAgInSc7d1	sovětský
vlivem	vliv	k1gInSc7	vliv
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
hnutí	hnutí	k1gNnSc2	hnutí
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
státech	stát	k1gInPc6	stát
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
KSSS	KSSS	kA	KSSS
Michail	Michail	k1gMnSc1	Michail
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
jako	jako	k8xS	jako
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
narůstající	narůstající	k2eAgNnSc4d1	narůstající
množství	množství	k1gNnSc4	množství
problémů	problém	k1gInPc2	problém
zahájil	zahájit	k5eAaPmAgMnS	zahájit
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
program	program	k1gInSc1	program
tzv.	tzv.	kA	tzv.
perestrojky	perestrojka	k1gFnSc2	perestrojka
a	a	k8xC	a
demokratizace	demokratizace	k1gFnSc2	demokratizace
<g/>
.	.	kIx.	.
</s>
<s>
Stažena	stáhnout	k5eAaPmNgFnS	stáhnout
byla	být	k5eAaImAgFnS	být
i	i	k8xC	i
vojska	vojsko	k1gNnPc1	vojsko
z	z	k7c2	z
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
glasnosti	glasnost	k1gFnSc2	glasnost
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
změně	změna	k1gFnSc3	změna
témat	téma	k1gNnPc2	téma
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
probírala	probírat	k5eAaImAgFnS	probírat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
oficiálně	oficiálně	k6eAd1	oficiálně
podporovaného	podporovaný	k2eAgNnSc2d1	podporované
budování	budování	k1gNnSc2	budování
komunismu	komunismus	k1gInSc2	komunismus
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
omylech	omyl	k1gInPc6	omyl
a	a	k8xC	a
přehmatech	přehmat	k1gInPc6	přehmat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xS	jak
dřívějších	dřívější	k2eAgInPc2d1	dřívější
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
současných	současný	k2eAgInPc2d1	současný
<g/>
.	.	kIx.	.
</s>
<s>
Kredit	kredit	k1gInSc1	kredit
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vlády	vláda	k1gFnSc2	vláda
tak	tak	k9	tak
ještě	ještě	k6eAd1	ještě
klesl	klesnout	k5eAaPmAgInS	klesnout
a	a	k8xC	a
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
se	se	k3xPyFc4	se
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
klesající	klesající	k2eAgFnSc7d1	klesající
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
výkonnosti	výkonnost	k1gFnSc6	výkonnost
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
náboženské	náboženský	k2eAgInPc1d1	náboženský
<g/>
,	,	kIx,	,
národnostní	národnostní	k2eAgInPc1d1	národnostní
i	i	k8xC	i
sociální	sociální	k2eAgInPc1d1	sociální
nepokoje	nepokoj	k1gInPc1	nepokoj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
později	pozdě	k6eAd2	pozdě
přerostly	přerůst	k5eAaPmAgInP	přerůst
do	do	k7c2	do
neřešitelných	řešitelný	k2eNgFnPc2d1	neřešitelná
mezí	mez	k1gFnPc2	mez
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
Západem	západ	k1gInSc7	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
podpepsal	podpepsat	k5eAaPmAgInS	podpepsat
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
Reaganem	Reagan	k1gMnSc7	Reagan
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
likvidaci	likvidace	k1gFnSc6	likvidace
raket	raketa	k1gFnPc2	raketa
středního	střední	k2eAgInSc2d1	střední
a	a	k8xC	a
kratšího	krátký	k2eAgInSc2d2	kratší
doletu	dolet	k1gInSc2	dolet
(	(	kIx(	(
<g/>
INF	INF	kA	INF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
významně	významně	k6eAd1	významně
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
světového	světový	k2eAgInSc2d1	světový
jaderného	jaderný	k2eAgInSc2d1	jaderný
arzenálu	arzenál	k1gInSc2	arzenál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
veřejně	veřejně	k6eAd1	veřejně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Brežněvovu	Brežněvův	k2eAgFnSc4d1	Brežněvova
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dal	dát	k5eAaPmAgMnS	dát
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nebude	být	k5eNaImBp3nS	být
do	do	k7c2	do
dění	dění	k1gNnSc2	dění
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
nijak	nijak	k6eAd1	nijak
zasahovat	zasahovat	k5eAaImF	zasahovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90.	[number]	k4	90.
let	léto	k1gNnPc2	léto
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
pobaltské	pobaltský	k2eAgFnSc2d1	pobaltská
republiky	republika	k1gFnSc2	republika
<g/>
;	;	kIx,	;
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
ve	v	k7c6	v
zbylých	zbylý	k2eAgFnPc6d1	zbylá
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
12	[number]	k4	12
republikami	republika	k1gFnPc7	republika
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
horšily	horšit	k5eAaImAgFnP	horšit
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
přestal	přestat	k5eAaPmAgInS	přestat
existovat	existovat	k5eAaImF	existovat
26.	[number]	k4	26.
prosince	prosinec	k1gInSc2	prosinec
1991	[number]	k4	1991
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
nástupnických	nástupnický	k2eAgInPc2d1	nástupnický
států	stát	k1gInPc2	stát
současně	současně	k6eAd1	současně
utvořila	utvořit	k5eAaPmAgFnS	utvořit
Společenství	společenství	k1gNnSc3	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nástupnické	nástupnický	k2eAgInPc4d1	nástupnický
státy	stát	k1gInPc4	stát
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
považuje	považovat	k5eAaImIp3nS	považovat
jen	jen	k9	jen
těchto	tento	k3xDgFnPc2	tento
12	[number]	k4	12
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Pobaltské	pobaltský	k2eAgInPc1d1	pobaltský
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
a	a	k8xC	a
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
považují	považovat	k5eAaImIp3nP	považovat
své	svůj	k3xOyFgNnSc4	svůj
nucené	nucený	k2eAgNnSc4d1	nucené
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
za	za	k7c4	za
okupaci	okupace	k1gFnSc4	okupace
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
tudíž	tudíž	k8xC	tudíž
nástupnickými	nástupnický	k2eAgInPc7d1	nástupnický
státy	stát	k1gInPc7	stát
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
vnímána	vnímat	k5eAaImNgFnS	vnímat
i	i	k9	i
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
<g/>
Rozpad	rozpad	k1gInSc1	rozpad
SSSR	SSSR	kA	SSSR
měl	mít	k5eAaImAgInS	mít
globální	globální	k2eAgInSc1d1	globální
geopolitický	geopolitický	k2eAgInSc1d1	geopolitický
význam	význam	k1gInSc1	význam
–	–	k?	–
doslova	doslova	k6eAd1	doslova
zatřásl	zatřást	k5eAaPmAgMnS	zatřást
s	s	k7c7	s
rovnováhou	rovnováha	k1gFnSc7	rovnováha
sil	síla	k1gFnPc2	síla
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
–	–	k?	–
z	z	k7c2	z
dvoupolárního	dvoupolární	k2eAgInSc2d1	dvoupolární
světa	svět	k1gInSc2	svět
rozděleného	rozdělený	k2eAgMnSc4d1	rozdělený
na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
východní	východní	k2eAgInSc4d1	východní
a	a	k8xC	a
západní	západní	k2eAgInSc4d1	západní
<g/>
,	,	kIx,	,
zbyly	zbýt	k5eAaPmAgFnP	zbýt
jako	jako	k8xC	jako
poslední	poslední	k2eAgFnSc4d1	poslední
supervelmoc	supervelmoc	k1gFnSc4	supervelmoc
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
Konec	konec	k1gInSc4	konec
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
zastihl	zastihnout	k5eAaPmAgMnS	zastihnout
obě	dva	k4xCgFnPc4	dva
supervelmoci	supervelmoc	k1gFnPc4	supervelmoc
s	s	k7c7	s
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
infrastrukturou	infrastruktura	k1gFnSc7	infrastruktura
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
arzenálem	arzenál	k1gInSc7	arzenál
zastarávající	zastarávající	k2eAgFnSc2d1	zastarávající
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
nevyužitelné	využitelný	k2eNgFnPc1d1	nevyužitelná
vojenské	vojenský	k2eAgFnPc1d1	vojenská
techniky	technika	k1gFnPc1	technika
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
průmyslu	průmysl	k1gInSc2	průmysl
přímo	přímo	k6eAd1	přímo
napojeného	napojený	k2eAgInSc2d1	napojený
na	na	k7c6	na
zbrojení	zbrojení	k1gNnSc6	zbrojení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
"	"	kIx"	"
<g/>
desovětizaci	desovětizace	k1gFnSc3	desovětizace
<g/>
"	"	kIx"	"
satelitních	satelitní	k2eAgInPc2d1	satelitní
států	stát	k1gInPc2	stát
a	a	k8xC	a
států	stát	k1gInPc2	stát
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
–	–	k?	–
konec	konec	k1gInSc1	konec
principu	princip	k1gInSc2	princip
politického	politický	k2eAgInSc2d1	politický
monopolu	monopol	k1gInSc2	monopol
jedné	jeden	k4xCgFnSc2	jeden
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
zavedení	zavedení	k1gNnSc1	zavedení
demokratických	demokratický	k2eAgInPc2d1	demokratický
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
včetně	včetně	k7c2	včetně
přepsání	přepsání	k1gNnSc2	přepsání
samotné	samotný	k2eAgFnSc2d1	samotná
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
decentralizace	decentralizace	k1gFnSc1	decentralizace
plánování	plánování	k1gNnSc2	plánování
<g/>
,	,	kIx,	,
liberalizace	liberalizace	k1gFnSc2	liberalizace
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
ostatních	ostatní	k2eAgFnPc2d1	ostatní
agend	agenda	k1gFnPc2	agenda
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
fungováním	fungování	k1gNnSc7	fungování
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
privatizace	privatizace	k1gFnSc2	privatizace
státního	státní	k2eAgInSc2d1	státní
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
zavedení	zavedení	k1gNnSc4	zavedení
soukromého	soukromý	k2eAgNnSc2d1	soukromé
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
,	,	kIx,	,
příliv	příliv	k1gInSc1	příliv
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
kapitálu	kapitál	k1gInSc2	kapitál
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
a	a	k8xC	a
politickými	politický	k2eAgFnPc7d1	politická
změnami	změna	k1gFnPc7	změna
i	i	k9	i
k	k	k7c3	k
(	(	kIx(	(
<g/>
většímu	veliký	k2eAgMnSc3d2	veliký
či	či	k8xC	či
menšímu	malý	k2eAgMnSc3d2	menší
<g/>
)	)	kIx)	)
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
se	se	k3xPyFc4	se
se	s	k7c7	s
zločiny	zločin	k1gInPc7	zločin
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
bývalého	bývalý	k2eAgInSc2d1	bývalý
sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
států	stát	k1gInPc2	stát
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
90.	[number]	k4	90.
let	léto	k1gNnPc2	léto
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
kvůli	kvůli	k7c3	kvůli
přehnané	přehnaný	k2eAgFnSc3d1	přehnaná
privatizaci	privatizace	k1gFnSc3	privatizace
propukla	propuknout	k5eAaPmAgFnS	propuknout
hluboká	hluboký	k2eAgFnSc1d1	hluboká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
většina	většina	k1gFnSc1	většina
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
rapidně	rapidně	k6eAd1	rapidně
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
na	na	k7c6	na
pomníku	pomník	k1gInSc6	pomník
Karla	Karel	k1gMnSc4	Karel
Marxe	Marx	k1gMnSc4	Marx
objevil	objevit	k5eAaPmAgInS	objevit
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Proletáři	proletář	k1gMnPc1	proletář
celého	celý	k2eAgInSc2d1	celý
světa-promiňte	světarominit	k5eAaPmRp2nP	světa-prominit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Konečná	konečný	k2eAgNnPc1d1	konečné
politická	politický	k2eAgNnPc1d1	politické
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1925-1953	[number]	k4	1925-1953
učinil	učinit	k5eAaImAgMnS	učinit
vždy	vždy	k6eAd1	vždy
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinak	jinak	k6eAd1	jinak
byla	být	k5eAaImAgFnS	být
sovětská	sovětský	k2eAgFnSc1d1	sovětská
politika	politika	k1gFnSc1	politika
vyhlašována	vyhlašovat	k5eAaImNgFnS	vyhlašovat
komisí	komise	k1gFnSc7	komise
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
KSSS	KSSS	kA	KSSS
nebo	nebo	k8xC	nebo
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
strany	strana	k1gFnSc2	strana
-	-	kIx~	-
politbyrem	politbyro	k1gNnSc7	politbyro
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
oddělené	oddělený	k2eAgNnSc4d1	oddělené
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
známé	známá	k1gFnSc2	známá
jako	jako	k8xS	jako
Lidový	lidový	k2eAgInSc1d1	lidový
komisariát	komisariát	k1gInSc1	komisariát
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
záležitosti	záležitost	k1gFnPc4	záležitost
či	či	k8xC	či
Narkomindel	Narkomindlo	k1gNnPc2	Narkomindlo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejvlivnější	vlivný	k2eAgFnSc7d3	nejvlivnější
mluvčí	mluvčí	k1gFnSc7	mluvčí
patřili	patřit	k5eAaImAgMnP	patřit
Georgij	Georgij	k1gMnSc1	Georgij
Čičerin	Čičerin	k1gInSc1	Čičerin
(	(	kIx(	(
<g/>
1872-1936	[number]	k4	1872-1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maxim	Maxim	k1gMnSc1	Maxim
Litvinov	Litvinov	k1gInSc1	Litvinov
(	(	kIx(	(
<g/>
1876-1951	[number]	k4	1876-1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Molotov	Molotov	k1gInSc1	Molotov
(	(	kIx(	(
<g/>
1890-1986	[number]	k4	1890-1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Vyšinskij	Vyšinskij	k1gMnSc1	Vyšinskij
(	(	kIx(	(
<g/>
1883-1954	[number]	k4	1883-1954
<g/>
)	)	kIx)	)
a	a	k8xC	a
Andrej	Andrej	k1gMnSc1	Andrej
Gromyko	Gromyko	k1gNnSc4	Gromyko
(	(	kIx(	(
<g/>
1909-89	[number]	k4	1909-89
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Intelektuálové	intelektuál	k1gMnPc1	intelektuál
sídlili	sídlit	k5eAaImAgMnP	sídlit
v	v	k7c6	v
Moskevském	moskevský	k2eAgInSc6d1	moskevský
státním	státní	k2eAgInSc6d1	státní
institutu	institut	k1gInSc6	institut
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Organizace	organizace	k1gFnSc2	organizace
===	===	k?	===
</s>
</p>
<p>
<s>
Kominterna	Kominterna	k1gFnSc1	Kominterna
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
internacionála	internacionála	k1gFnSc1	internacionála
byla	být	k5eAaImAgFnS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
komunistická	komunistický	k2eAgFnSc1d1	komunistická
organizace	organizace	k1gFnSc1	organizace
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Kremlu	Kreml	k1gInSc6	Kreml
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
světový	světový	k2eAgInSc4d1	světový
komunismus	komunismus	k1gInSc4	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Komiterna	Komiterna	k1gFnSc1	Komiterna
chtěla	chtít	k5eAaImAgFnS	chtít
"	"	kIx"	"
<g/>
bojovat	bojovat	k5eAaImF	bojovat
všemi	všecek	k3xTgInPc7	všecek
dostupnými	dostupný	k2eAgInPc7d1	dostupný
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
za	za	k7c4	za
svržení	svržení	k1gNnSc4	svržení
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
buržoazie	buržoazie	k1gFnSc2	buržoazie
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
sovětské	sovětský	k2eAgFnSc2d1	sovětská
republiky	republika	k1gFnSc2	republika
jako	jako	k8xS	jako
přechodné	přechodný	k2eAgFnSc2d1	přechodná
fáze	fáze	k1gFnSc2	fáze
než	než	k8xS	než
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
zrušení	zrušení	k1gNnSc3	zrušení
státu	stát	k1gInSc2	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
jako	jako	k8xS	jako
smírčí	smírčí	k2eAgNnSc1d1	smírčí
opatření	opatření	k1gNnSc1	opatření
vůči	vůči	k7c3	vůči
Británii	Británie	k1gFnSc3	Británie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
<g/>
RVHP	RVHP	kA	RVHP
(	(	kIx(	(
<g/>
С	С	k?	С
Э	Э	k?	Э
В	В	k?	В
<g/>
,	,	kIx,	,
Sovět	Sovět	k1gMnSc1	Sovět
Ekonomičeskoj	Ekonomičeskoj	k1gInSc1	Ekonomičeskoj
Vzaimopomošči	Vzaimopomošč	k1gFnSc3	Vzaimopomošč
<g/>
,	,	kIx,	,
С	С	k?	С
<g/>
,	,	kIx,	,
SEV	SEV	kA	SEV
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949-1991	[number]	k4	1949-1991
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
organizací	organizace	k1gFnSc7	organizace
pod	pod	k7c7	pod
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
státy	stát	k1gInPc4	stát
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
komunistických	komunistický	k2eAgInPc2d1	komunistický
států	stát	k1gInPc2	stát
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
byla	být	k5eAaImAgFnS	být
znepokojena	znepokojit	k5eAaPmNgFnS	znepokojit
Marshallovým	Marshallův	k2eAgInSc7d1	Marshallův
plánem	plán	k1gInSc7	plán
a	a	k8xC	a
cílem	cíl	k1gInSc7	cíl
RVHP	RVHP	kA	RVHP
bylo	být	k5eAaImAgNnS	být
zabránit	zabránit	k5eAaPmF	zabránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
země	zem	k1gFnPc1	zem
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
sféře	sféra	k1gFnSc6	sféra
vlivu	vliv	k1gInSc2	vliv
směřovaly	směřovat	k5eAaImAgFnP	směřovat
k	k	k7c3	k
Američanům	Američan	k1gMnPc3	Američan
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc3d1	jihovýchodní
Asii	Asie	k1gFnSc3	Asie
<g/>
.	.	kIx.	.
</s>
<s>
RVHP	RVHP	kA	RVHP
byla	být	k5eAaImAgFnS	být
také	také	k9	také
odpovědí	odpověď	k1gFnSc7	odpověď
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
OEEC	OEEC	kA	OEEC
<g/>
)	)	kIx)	)
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
<g/>
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
smlouva	smlouva	k1gFnSc1	smlouva
byl	být	k5eAaImAgInS	být
vojenský	vojenský	k2eAgInSc1d1	vojenský
pakt	pakt	k1gInSc1	pakt
zformovaný	zformovaný	k2eAgInSc1d1	zformovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
mezi	mezi	k7c7	mezi
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
sedmi	sedm	k4xCc7	sedm
sovětskými	sovětský	k2eAgInPc7d1	sovětský
satelitními	satelitní	k2eAgInPc7d1	satelitní
státy	stát	k1gInPc7	stát
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
během	během	k7c2	během
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
vojenským	vojenský	k2eAgInSc7d1	vojenský
doplňkem	doplněk	k1gInSc7	doplněk
RVHP	RVHP	kA	RVHP
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgFnSc2d1	regionální
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
socialistické	socialistický	k2eAgInPc4d1	socialistický
státy	stát	k1gInPc4	stát
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
do	do	k7c2	do
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
<g/>
Kominforma	kominforma	k1gFnSc1	kominforma
(	(	kIx(	(
<g/>
1947-1956	[number]	k4	1947-1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Informační	informační	k2eAgNnSc1d1	informační
byro	byro	k1gNnSc1	byro
komunistických	komunistický	k2eAgFnPc2d1	komunistická
a	a	k8xC	a
dělnických	dělnický	k2eAgFnPc2d1	Dělnická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
oficiální	oficiální	k2eAgFnSc7d1	oficiální
agenturou	agentura	k1gFnSc7	agentura
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
komunistického	komunistický	k2eAgNnSc2d1	komunistické
hnutí	hnutí	k1gNnSc2	hnutí
od	od	k7c2	od
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
Kominterny	Kominterna	k1gFnSc2	Kominterna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
koordinovat	koordinovat	k5eAaBmF	koordinovat
akce	akce	k1gFnPc4	akce
mezi	mezi	k7c7	mezi
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
stranami	strana	k1gFnPc7	strana
pod	pod	k7c7	pod
sovětským	sovětský	k2eAgNnSc7d1	sovětské
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
ji	on	k3xPp3gFnSc4	on
využil	využít	k5eAaPmAgMnS	využít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nařídil	nařídit	k5eAaPmAgMnS	nařídit
západoevropským	západoevropský	k2eAgFnPc3d1	západoevropská
komunistickým	komunistický	k2eAgFnPc3d1	komunistická
stranám	strana	k1gFnPc3	strana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
opustily	opustit	k5eAaPmAgFnP	opustit
svou	svůj	k3xOyFgFnSc4	svůj
výlučně	výlučně	k6eAd1	výlučně
parlamentní	parlamentní	k2eAgFnSc4d1	parlamentní
linii	linie	k1gFnSc4	linie
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
soustředily	soustředit	k5eAaPmAgInP	soustředit
na	na	k7c4	na
politické	politický	k2eAgFnPc4d1	politická
překážky	překážka	k1gFnPc4	překážka
operacím	operace	k1gFnPc3	operace
ohledně	ohledně	k7c2	ohledně
Marshallova	Marshallův	k2eAgInSc2d1	Marshallův
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
koordinovala	koordinovat	k5eAaBmAgFnS	koordinovat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pomoc	pomoc	k1gFnSc4	pomoc
komunistickým	komunistický	k2eAgMnPc3d1	komunistický
povstalcům	povstalec	k1gMnPc3	povstalec
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1947-49	[number]	k4	1947-49
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
vyloučena	vyloučen	k2eAgFnSc1d1	vyloučena
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Tito	tento	k3xDgMnPc1	tento
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
nezávislém	závislý	k2eNgInSc6d1	nezávislý
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
Kominformy	kominforma	k1gFnSc2	kominforma
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
trvalý	trvalý	k2eAgInSc4d1	trvalý
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
lidovou	lidový	k2eAgFnSc4d1	lidová
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
podporovaly	podporovat	k5eAaImAgFnP	podporovat
Stalinovy	Stalinův	k2eAgInPc4d1	Stalinův
postoje	postoj	k1gInPc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
Kominformy	kominforma	k1gFnSc2	kominforma
na	na	k7c4	na
Evropu	Evropa	k1gFnSc4	Evropa
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
zahraniční	zahraniční	k2eAgFnSc3d1	zahraniční
politice	politika	k1gFnSc3	politika
znamenala	znamenat	k5eAaImAgFnS	znamenat
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyjádřovala	vyjádřovat	k5eAaImAgFnS	vyjádřovat
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
ideologii	ideologie	k1gFnSc4	ideologie
<g/>
,	,	kIx,	,
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
účastníkům	účastník	k1gMnPc3	účastník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
soustřeďovali	soustřeďovat	k5eAaImAgMnP	soustřeďovat
spíše	spíše	k9	spíše
na	na	k7c4	na
osobnosti	osobnost	k1gFnPc4	osobnost
než	než	k8xS	než
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
byly	být	k5eAaImAgInP	být
tři	tři	k4xCgInPc1	tři
zdroje	zdroj	k1gInPc1	zdroj
moci	moc	k1gFnSc2	moc
<g/>
:	:	kIx,	:
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
sovět	sovět	k1gInSc1	sovět
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
Rada	rada	k1gFnSc1	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
legální	legální	k2eAgFnSc7d1	legální
politickou	politický	k2eAgFnSc7d1	politická
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
jediným	jediný	k2eAgMnSc7d1	jediný
tvůrcem	tvůrce	k1gMnSc7	tvůrce
politiky	politika	k1gFnSc2	politika
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
způsobu	způsob	k1gInSc3	způsob
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
(	(	kIx(	(
<g/>
Stalinova	Stalinův	k2eAgFnSc1d1	Stalinova
smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
o	o	k7c4	o
totalitní	totalitní	k2eAgInSc4d1	totalitní
politický	politický	k2eAgInSc4d1	politický
systém	systém	k1gInSc4	systém
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
kvazitotalitní	kvazitotalitní	k2eAgInSc1d1	kvazitotalitní
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Perestrojka	perestrojka	k1gFnSc1	perestrojka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
reformy	reforma	k1gFnSc2	reforma
zkostnatělého	zkostnatělý	k2eAgInSc2d1	zkostnatělý
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
označovaly	označovat	k5eAaImAgInP	označovat
hesly	heslo	k1gNnPc7	heslo
perestrojka	perestrojka	k1gFnSc1	perestrojka
(	(	kIx(	(
<g/>
přestavba	přestavba	k1gFnSc1	přestavba
<g/>
)	)	kIx)	)
a	a	k8xC	a
glasnosť	glasnosť	k1gFnSc4	glasnosť
(	(	kIx(	(
<g/>
otevřenost	otevřenost	k1gFnSc4	otevřenost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
byl	být	k5eAaImAgInS	být
federací	federace	k1gFnSc7	federace
sovětských	sovětský	k2eAgFnPc2d1	sovětská
socialistických	socialistický	k2eAgFnPc2d1	socialistická
republik	republika	k1gFnPc2	republika
(	(	kIx(	(
<g/>
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
byly	být	k5eAaImAgFnP	být
autonomní	autonomní	k2eAgFnPc1d1	autonomní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ovládány	ovládán	k2eAgInPc1d1	ovládán
centrální	centrální	k2eAgFnSc7d1	centrální
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
(	(	kIx(	(
<g/>
KSSS	KSSS	kA	KSSS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Republiky	republika	k1gFnPc1	republika
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělily	dělit	k5eAaImAgFnP	dělit
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Litvy	Litva	k1gFnSc2	Litva
<g/>
,	,	kIx,	,
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
,	,	kIx,	,
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
,	,	kIx,	,
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
a	a	k8xC	a
Arménie	Arménie	k1gFnSc2	Arménie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RSFSR	RSFSR	kA	RSFSR
tvořily	tvořit	k5eAaImAgInP	tvořit
i	i	k9	i
kraje	kraj	k1gInPc1	kraj
<g/>
,	,	kIx,	,
národnostní	národnostní	k2eAgInPc1d1	národnostní
okruhy	okruh	k1gInPc1	okruh
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
autonomní	autonomní	k2eAgInPc4d1	autonomní
okruhy	okruh	k1gInPc4	okruh
<g/>
)	)	kIx)	)
a	a	k8xC	a
autonomní	autonomní	k2eAgFnSc2d1	autonomní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgFnPc2	některý
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
,	,	kIx,	,
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
,	,	kIx,	,
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
a	a	k8xC	a
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
autonomní	autonomní	k2eAgFnPc1d1	autonomní
sovětské	sovětský	k2eAgFnPc1d1	sovětská
socialistické	socialistický	k2eAgFnPc1d1	socialistická
republiky	republika	k1gFnPc1	republika
(	(	kIx(	(
<g/>
ASSR	ASSR	kA	ASSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc7d1	základní
správní	správní	k2eAgFnSc7d1	správní
jednotkou	jednotka	k1gFnSc7	jednotka
byl	být	k5eAaImAgInS	být
rajón	rajón	k1gInSc1	rajón
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1961	[number]	k4	1961
-	-	kIx~	-
1963	[number]	k4	1963
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
18	[number]	k4	18
velkých	velký	k2eAgInPc2d1	velký
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
rajónů	rajón	k1gInPc2	rajón
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pokrývaly	pokrývat	k5eAaImAgFnP	pokrývat
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Podle	podle	k7c2	podle
vojenského	vojenský	k2eAgInSc2d1	vojenský
zákona	zákon	k1gInSc2	zákon
ze	z	k7c2	z
září	září	k1gNnSc2	září
1925	[number]	k4	1925
se	se	k3xPyFc4	se
sovětské	sovětský	k2eAgFnPc1d1	sovětská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
skládaly	skládat	k5eAaImAgFnP	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
složek	složka	k1gFnPc2	složka
<g/>
:	:	kIx,	:
pozemních	pozemní	k2eAgFnPc2d1	pozemní
síl	síl	k1gInSc1	síl
<g/>
,	,	kIx,	,
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
státní	státní	k2eAgFnSc2d1	státní
politické	politický	k2eAgFnSc2d1	politická
správy	správa	k1gFnSc2	správa
(	(	kIx(	(
<g/>
OGPU	OGPU	kA	OGPU
<g/>
)	)	kIx)	)
a	a	k8xC	a
vnitřních	vnitřní	k2eAgNnPc2d1	vnitřní
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
OGPU	OGPU	kA	OGPU
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
součila	součit	k5eAaPmAgFnS	součit
s	s	k7c7	s
NKVD	NKVD	kA	NKVD
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gNnPc4	její
vnitřní	vnitřní	k2eAgNnPc4d1	vnitřní
vojska	vojsko	k1gNnPc4	vojsko
pod	pod	k7c7	pod
společným	společný	k2eAgNnSc7d1	společné
vedením	vedení	k1gNnSc7	vedení
obranných	obranný	k2eAgInPc2d1	obranný
a	a	k8xC	a
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
komisariátů	komisariát	k1gInPc2	komisariát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
strategická	strategický	k2eAgNnPc1d1	strategické
raketová	raketový	k2eAgNnPc1d1	raketové
vojska	vojsko	k1gNnPc1	vojsko
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojska	vojsko	k1gNnSc2	vojsko
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
všenárodní	všenárodní	k2eAgFnSc2d1	všenárodní
civilní	civilní	k2eAgFnSc2d1	civilní
obrany	obrana	k1gFnSc2	obrana
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
stály	stát	k5eAaImAgInP	stát
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
<g/>
,	,	kIx,	,
třetím	třetí	k4xOgMnSc6	třetí
a	a	k8xC	a
šestém	šestý	k4xOgInSc6	šestý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
sovětském	sovětský	k2eAgInSc6d1	sovětský
systému	systém	k1gInSc6	systém
důležitosti	důležitost	k1gFnSc2	důležitost
(	(	kIx(	(
<g/>
pozemní	pozemní	k2eAgFnPc1d1	pozemní
síly	síla	k1gFnPc1	síla
byly	být	k5eAaImAgFnP	být
druhé	druhý	k4xOgNnSc4	druhý
<g/>
,	,	kIx,	,
letectvo	letectvo	k1gNnSc1	letectvo
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
a	a	k8xC	a
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
páté	pátá	k1gFnSc2	pátá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Armáda	armáda	k1gFnSc1	armáda
měla	mít	k5eAaImAgFnS	mít
největší	veliký	k2eAgInSc4d3	veliký
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
čítala	čítat	k5eAaImAgFnS	čítat
dva	dva	k4xCgInPc4	dva
milióny	milión	k4xCgInPc4	milión
vojáků	voják	k1gMnPc2	voják
rozdělených	rozdělený	k2eAgMnPc2d1	rozdělený
mezi	mezi	k7c4	mezi
150	[number]	k4	150
motorizovaných	motorizovaný	k2eAgMnPc2d1	motorizovaný
a	a	k8xC	a
52	[number]	k4	52
obrněných	obrněný	k2eAgFnPc2d1	obrněná
divizí	divize	k1gFnPc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgNnSc1d1	sovětské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
bylo	být	k5eAaImAgNnS	být
až	až	k6eAd1	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
60.	[number]	k4	60.
let	léto	k1gNnPc2	léto
spíše	spíše	k9	spíše
méně	málo	k6eAd2	málo
početnou	početný	k2eAgFnSc7d1	početná
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
větví	větev	k1gFnSc7	větev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
karibské	karibský	k2eAgFnSc6d1	karibská
krizi	krize	k1gFnSc6	krize
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Sergeje	Sergej	k1gMnSc2	Sergej
Gorškova	Gorškův	k2eAgInSc2d1	Gorškův
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
výrazného	výrazný	k2eAgNnSc2d1	výrazné
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známá	k1gFnSc3	známá
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
díky	díky	k7c3	díky
raketovým	raketový	k2eAgInPc3d1	raketový
křižníkům	křižník	k1gInPc3	křižník
a	a	k8xC	a
ponorkám	ponorka	k1gFnPc3	ponorka
a	a	k8xC	a
sloužilo	sloužit	k5eAaImAgNnS	sloužit
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
500 000	[number]	k4	500 000
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgNnSc1d1	sovětské
letectvo	letectvo	k1gNnSc1	letectvo
se	se	k3xPyFc4	se
soustřeďovalo	soustřeďovat	k5eAaImAgNnS	soustřeďovat
na	na	k7c4	na
flotilu	flotila	k1gFnSc4	flotila
strategických	strategický	k2eAgInPc2d1	strategický
bombardérů	bombardér	k1gInPc2	bombardér
a	a	k8xC	a
ve	v	k7c6	v
válečné	válečný	k2eAgFnSc6d1	válečná
situaci	situace	k1gFnSc6	situace
mělo	mít	k5eAaImAgNnS	mít
vymýtit	vymýtit	k5eAaPmF	vymýtit
nepřátelskou	přátelský	k2eNgFnSc4d1	nepřátelská
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
a	a	k8xC	a
jadernou	jaderný	k2eAgFnSc4d1	jaderná
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušná	vzdušný	k2eAgFnSc1d1	vzdušná
síla	síla	k1gFnSc1	síla
měla	mít	k5eAaImAgFnS	mít
také	také	k9	také
množství	množství	k1gNnSc4	množství
stíhaček	stíhačka	k1gFnPc2	stíhačka
a	a	k8xC	a
taktických	taktický	k2eAgInPc2d1	taktický
bombardérů	bombardér	k1gInPc2	bombardér
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
armádu	armáda	k1gFnSc4	armáda
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
podporovaly	podporovat	k5eAaImAgFnP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Strategická	strategický	k2eAgNnPc1d1	strategické
raketová	raketový	k2eAgNnPc1d1	raketové
vojska	vojsko	k1gNnPc1	vojsko
disponovala	disponovat	k5eAaBmAgNnP	disponovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1400	[number]	k4	1400
mezikontinentálními	mezikontinentální	k2eAgFnPc7d1	mezikontinentální
balistickými	balistický	k2eAgFnPc7d1	balistická
raketami	raketa	k1gFnPc7	raketa
(	(	kIx(	(
<g/>
ICBM	ICBM	kA	ICBM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozmístěných	rozmístěný	k2eAgMnPc2d1	rozmístěný
mezi	mezi	k7c4	mezi
28	[number]	k4	28
základen	základna	k1gFnPc2	základna
a	a	k8xC	a
300	[number]	k4	300
velitelských	velitelský	k2eAgNnPc2d1	velitelské
středisek	středisko	k1gNnPc2	středisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poválečné	poválečný	k2eAgFnSc6d1	poválečná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
přímo	přímo	k6eAd1	přímo
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
několika	několik	k4yIc6	několik
vojenských	vojenský	k2eAgFnPc6d1	vojenská
operacích	operace	k1gFnPc6	operace
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
patřilo	patřit	k5eAaImAgNnS	patřit
potlačení	potlačení	k1gNnSc1	potlačení
povstání	povstání	k1gNnSc2	povstání
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
a	a	k8xC	a
vpád	vpád	k1gInSc4	vpád
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
se	se	k3xPyFc4	se
také	také	k9	také
účastnil	účastnit	k5eAaImAgInS	účastnit
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1979	[number]	k4	1979
až	až	k9	až
1989.	[number]	k4	1989.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
platila	platit	k5eAaImAgFnS	platit
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
představitelé	představitel	k1gMnPc1	představitel
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Nejvyššími	vysoký	k2eAgMnPc7d3	nejvyšší
představiteli	představitel	k1gMnPc7	představitel
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
prezídia	prezídium	k1gNnSc2	prezídium
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
kolektivní	kolektivní	k2eAgFnSc7d1	kolektivní
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
byli	být	k5eAaImAgMnP	být
vždy	vždy	k6eAd1	vždy
zároveň	zároveň	k6eAd1	zároveň
vůdcové	vůdce	k1gMnPc1	vůdce
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
strany	strana	k1gFnSc2	strana
–	–	k?	–
zpravidla	zpravidla	k6eAd1	zpravidla
první	první	k4xOgMnSc1	první
nebo	nebo	k8xC	nebo
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
–	–	k?	–
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
*	*	kIx~	*
<g/>
)	)	kIx)	)
Lenin	Lenin	k1gMnSc1	Lenin
byl	být	k5eAaImAgMnS	být
jen	jen	k9	jen
neformálně	formálně	k6eNd1	formálně
vedoucím	vedoucí	k1gMnSc7	vedoucí
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922/24	[number]	k4	1922/24
až	až	k9	až
1953	[number]	k4	1953
a	a	k8xC	a
1966	[number]	k4	1966
až	až	k9	až
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
-	-	kIx~	-
1966	[number]	k4	1966
"	"	kIx"	"
<g/>
první	první	k4xOgMnSc1	první
tajemník	tajemník	k1gMnSc1	tajemník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Sovětský	sovětský	k2eAgInSc1d1	sovětský
kosmický	kosmický	k2eAgInSc1d1	kosmický
program	program	k1gInSc1	program
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
50.	[number]	k4	50.
let	let	k1gInSc4	let
SSSR	SSSR	kA	SSSR
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
inženýrů	inženýr	k1gMnPc2	inženýr
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
dovezených	dovezený	k2eAgFnPc2d1	dovezená
z	z	k7c2	z
poraženého	poražený	k2eAgNnSc2d1	poražené
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
zkonstruovali	zkonstruovat	k5eAaPmAgMnP	zkonstruovat
Sověti	Sovět	k1gMnPc1	Sovět
první	první	k4xOgFnSc4	první
umělou	umělý	k2eAgFnSc4d1	umělá
družici	družice	k1gFnSc4	družice
–	–	k?	–
Sputnik	sputnik	k1gInSc1	sputnik
1	[number]	k4	1
a	a	k8xC	a
předběhli	předběhnout	k5eAaPmAgMnP	předběhnout
tak	tak	k6eAd1	tak
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovaly	následovat	k5eAaImAgFnP	následovat
další	další	k2eAgFnPc4d1	další
úspěšné	úspěšný	k2eAgFnPc4d1	úspěšná
družice	družice	k1gFnPc4	družice
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
vysláni	vyslat	k5eAaPmNgMnP	vyslat
i	i	k8xC	i
pokusní	pokusný	k2eAgMnPc1d1	pokusný
psi	pes	k1gMnPc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12.	[number]	k4	12.
dubna	duben	k1gInSc2	duben
1961	[number]	k4	1961
byl	být	k5eAaImAgMnS	být
vyslán	vyslán	k2eAgMnSc1d1	vyslán
první	první	k4xOgMnSc1	první
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
Jurij	Jurij	k1gFnSc2	Jurij
Gagarin	Gagarin	k1gInSc1	Gagarin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
jednou	jednou	k6eAd1	jednou
obletěl	obletět	k5eAaPmAgMnS	obletět
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
přistál	přistát	k5eAaPmAgInS	přistát
v	v	k7c6	v
kazašské	kazašský	k2eAgFnSc6d1	kazašská
stepi	step	k1gFnSc6	step
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
sovětských	sovětský	k2eAgFnPc6d1	sovětská
projekčních	projekční	k2eAgFnPc6d1	projekční
kancelářích	kancelář	k1gFnPc6	kancelář
rýsovaly	rýsovat	k5eAaImAgInP	rýsovat
první	první	k4xOgInPc1	první
plány	plán	k1gInPc1	plán
raketoplánů	raketoplán	k1gInPc2	raketoplán
a	a	k8xC	a
orbitálních	orbitální	k2eAgFnPc2d1	orbitální
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
zamezily	zamezit	k5eAaPmAgInP	zamezit
osobní	osobní	k2eAgInPc1d1	osobní
spory	spor	k1gInPc1	spor
mezi	mezi	k7c4	mezi
projektanty	projektant	k1gMnPc4	projektant
a	a	k8xC	a
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
velkým	velký	k2eAgInSc7d1	velký
fiaskem	fiasko	k1gNnSc7	fiasko
pro	pro	k7c4	pro
SSSR	SSSR	kA	SSSR
bylo	být	k5eAaImAgNnS	být
přistání	přistání	k1gNnSc1	přistání
Američanů	Američan	k1gMnPc2	Američan
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Rusové	Rus	k1gMnPc1	Rus
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
v	v	k7c6	v
dostatečné	dostatečný	k2eAgFnSc6d1	dostatečná
době	doba	k1gFnSc6	doba
odpovědět	odpovědět	k5eAaPmF	odpovědět
Američanům	Američan	k1gMnPc3	Američan
stejným	stejný	k2eAgInSc7d1	stejný
projektem	projekt	k1gInSc7	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70.	[number]	k4	70.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
rýsovat	rýsovat	k5eAaImF	rýsovat
konkrétnější	konkrétní	k2eAgInPc4d2	konkrétnější
návrhy	návrh	k1gInPc4	návrh
konstrukce	konstrukce	k1gFnSc2	konstrukce
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
elektronickém	elektronický	k2eAgInSc6d1	elektronický
průmyslu	průmysl	k1gInSc6	průmysl
(	(	kIx(	(
<g/>
rychlé	rychlý	k2eAgNnSc1d1	rychlé
přehřívání	přehřívání	k1gNnSc1	přehřívání
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odsunuly	odsunout	k5eAaPmAgFnP	odsunout
program	program	k1gInSc4	program
až	až	k6eAd1	až
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
raketoplán	raketoplán	k1gInSc1	raketoplán
<g/>
,	,	kIx,	,
Buran	buran	k1gMnSc1	buran
<g/>
,	,	kIx,	,
letěl	letět	k5eAaImAgMnS	letět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
lidské	lidský	k2eAgFnSc2d1	lidská
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
raketoplán	raketoplán	k1gInSc1	raketoplán
<g/>
,	,	kIx,	,
Ptička	Ptička	k1gFnSc1	Ptička
<g/>
,	,	kIx,	,
skončil	skončit	k5eAaPmAgMnS	skončit
nakonec	nakonec	k6eAd1	nakonec
rozestavěný	rozestavěný	k2eAgMnSc1d1	rozestavěný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
raketoplánů	raketoplán	k1gInPc2	raketoplán
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
vypuštění	vypuštění	k1gNnSc4	vypuštění
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
existuje	existovat	k5eAaImIp3nS	existovat
dnes	dnes	k6eAd1	dnes
nevyužívaná	využívaný	k2eNgFnSc1d1	nevyužívaná
supersilná	supersilný	k2eAgFnSc1d1	supersilná
raketa	raketa	k1gFnSc1	raketa
Eněrgija	Eněrgija	k1gFnSc1	Eněrgija
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
podařilo	podařit	k5eAaPmAgNnS	podařit
vybudovat	vybudovat	k5eAaPmF	vybudovat
orbitální	orbitální	k2eAgFnSc4d1	orbitální
stanici	stanice	k1gFnSc4	stanice
Mir	Mira	k1gFnPc2	Mira
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
na	na	k7c6	na
konstrukci	konstrukce	k1gFnSc6	konstrukce
stanic	stanice	k1gFnPc2	stanice
Saljut	Saljut	k1gInSc1	Saljut
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
úkoly	úkol	k1gInPc1	úkol
byly	být	k5eAaImAgInP	být
čistě	čistě	k6eAd1	čistě
civilní	civilní	k2eAgInPc4d1	civilní
a	a	k8xC	a
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90.	[number]	k4	90.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
amerického	americký	k2eAgInSc2d1	americký
Skylabu	Skylab	k1gInSc2	Skylab
ukončen	ukončit	k5eAaPmNgInS	ukončit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
orbitální	orbitální	k2eAgFnSc7d1	orbitální
stanicí	stanice	k1gFnSc7	stanice
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
byly	být	k5eAaImAgInP	být
přidávány	přidáván	k2eAgInPc1d1	přidáván
další	další	k2eAgInPc1d1	další
moduly	modul	k1gInPc1	modul
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
americké	americký	k2eAgNnSc1d1	americké
<g/>
.	.	kIx.	.
</s>
<s>
Technický	technický	k2eAgInSc1d1	technický
stav	stav	k1gInSc1	stav
stanice	stanice	k1gFnSc2	stanice
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
zhoršoval	zhoršovat	k5eAaImAgMnS	zhoršovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
navedení	navedení	k1gNnSc6	navedení
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
shořel	shořet	k5eAaPmAgInS	shořet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
první	první	k4xOgFnSc7	první
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přijala	přijmout	k5eAaPmAgFnS	přijmout
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
distribuci	distribuce	k1gFnSc4	distribuce
zboží	zboží	k1gNnSc2	zboží
centralizovala	centralizovat	k5eAaBmAgFnS	centralizovat
a	a	k8xC	a
řídila	řídit	k5eAaImAgFnS	řídit
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
bolševická	bolševický	k2eAgFnSc1d1	bolševická
zkušenost	zkušenost	k1gFnSc1	zkušenost
s	s	k7c7	s
velitelskou	velitelský	k2eAgFnSc7d1	velitelská
ekonomikou	ekonomika	k1gFnSc7	ekonomika
byla	být	k5eAaImAgFnS	být
politika	politika	k1gFnSc1	politika
válečného	válečný	k2eAgInSc2d1	válečný
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
znárodnění	znárodnění	k1gNnSc3	znárodnění
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
centralizovanou	centralizovaný	k2eAgFnSc4d1	centralizovaná
distribuci	distribuce	k1gFnSc4	distribuce
outputu	output	k1gInSc2	output
(	(	kIx(	(
<g/>
výsledek	výsledek	k1gInSc1	výsledek
výrobního	výrobní	k2eAgInSc2d1	výrobní
procesu	proces	k1gInSc2	proces
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
donucovací	donucovací	k2eAgInPc4d1	donucovací
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
pokusy	pokus	k1gInPc4	pokus
odstranit	odstranit	k5eAaPmF	odstranit
peněžní	peněžní	k2eAgInSc4d1	peněžní
oběh	oběh	k1gInSc4	oběh
<g/>
,	,	kIx,	,
soukromé	soukromý	k2eAgInPc4d1	soukromý
podniky	podnik	k1gInPc4	podnik
a	a	k8xC	a
volný	volný	k2eAgInSc4d1	volný
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vážném	vážný	k2eAgInSc6d1	vážný
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
kolapsu	kolaps	k1gInSc6	kolaps
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Lenin	Lenin	k1gMnSc1	Lenin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
válečný	válečný	k2eAgInSc1d1	válečný
komunismus	komunismus	k1gInSc1	komunismus
novou	nový	k2eAgFnSc7d1	nová
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
politikou	politika	k1gFnSc7	politika
(	(	kIx(	(
<g/>
NEP	Nep	k1gInSc1	Nep
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
legalizoval	legalizovat	k5eAaBmAgInS	legalizovat
volný	volný	k2eAgInSc4d1	volný
trh	trh	k1gInSc4	trh
a	a	k8xC	a
soukromé	soukromý	k2eAgNnSc4d1	soukromé
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
malých	malý	k2eAgInPc2d1	malý
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zotavila	zotavit	k5eAaPmAgFnS	zotavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
debatě	debata	k1gFnSc6	debata
mezi	mezi	k7c7	mezi
členy	člen	k1gInPc7	člen
politbyra	politbyro	k1gNnSc2	politbyro
o	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928-1929	[number]	k4	1928-1929
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
Josef	Josef	k1gMnSc1	Josef
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
NEP	Nep	k1gInSc4	Nep
a	a	k8xC	a
prosadil	prosadit	k5eAaPmAgMnS	prosadit
plné	plný	k2eAgNnSc4d1	plné
centrální	centrální	k2eAgNnSc4d1	centrální
plánování	plánování	k1gNnSc4	plánování
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
násilnou	násilný	k2eAgFnSc4d1	násilná
kolektivizaci	kolektivizace	k1gFnSc4	kolektivizace
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgInS	přijmout
drakonickou	drakonický	k2eAgFnSc4d1	drakonická
pracovní	pracovní	k2eAgFnSc4d1	pracovní
legislativu	legislativa	k1gFnSc4	legislativa
<g/>
.	.	kIx.	.
</s>
<s>
Zdroje	zdroj	k1gInPc1	zdroj
byly	být	k5eAaImAgInP	být
zmobilizovány	zmobilizovat	k5eAaPmNgInP	zmobilizovat
pro	pro	k7c4	pro
rychlou	rychlý	k2eAgFnSc4d1	rychlá
industrializaci	industrializace	k1gFnSc4	industrializace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
během	během	k7c2	během
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
značně	značně	k6eAd1	značně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
kapacitu	kapacita	k1gFnSc4	kapacita
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
investičního	investiční	k2eAgInSc2d1	investiční
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
motivací	motivace	k1gFnSc7	motivace
pro	pro	k7c4	pro
industrializaci	industrializace	k1gFnSc4	industrializace
byla	být	k5eAaImAgFnS	být
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
kvůli	kvůli	k7c3	kvůli
nedůvěře	nedůvěra	k1gFnSc3	nedůvěra
k	k	k7c3	k
"	"	kIx"	"
<g/>
vnejšímu	vnejší	k2eAgInSc3d1	vnejší
<g/>
"	"	kIx"	"
kapitalistickému	kapitalistický	k2eAgInSc3d1	kapitalistický
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
SSSR	SSSR	kA	SSSR
přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
z	z	k7c2	z
převážně	převážně	k6eAd1	převážně
agrární	agrární	k2eAgFnSc2d1	agrární
ekonomiky	ekonomika	k1gFnSc2	ekonomika
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
razil	razit	k5eAaImAgMnS	razit
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
jako	jako	k8xC	jako
velmoc	velmoc	k1gFnSc1	velmoc
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Válka	Válka	k1gMnSc1	Válka
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
velmi	velmi	k6eAd1	velmi
zdevastovala	zdevastovat	k5eAaPmAgFnS	zdevastovat
a	a	k8xC	a
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
sovětská	sovětský	k2eAgFnSc1d1	sovětská
ekonomika	ekonomika	k1gFnSc1	ekonomika
stala	stát	k5eAaPmAgFnS	stát
relativně	relativně	k6eAd1	relativně
soběstačnou	soběstačný	k2eAgFnSc7d1	soběstačná
<g/>
;	;	kIx,	;
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
období	období	k1gNnSc2	období
až	až	k9	až
do	do	k7c2	do
vzniku	vznik	k1gInSc2	vznik
RVHP	RVHP	kA	RVHP
se	se	k3xPyFc4	se
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
trhu	trh	k1gInSc6	trh
obchodovalo	obchodovat	k5eAaImAgNnS	obchodovat
s	s	k7c7	s
pouze	pouze	k6eAd1	pouze
velmi	velmi	k6eAd1	velmi
malým	malý	k2eAgInSc7d1	malý
podílem	podíl	k1gInSc7	podíl
sovětských	sovětský	k2eAgInPc2d1	sovětský
domácích	domácí	k2eAgInPc2d1	domácí
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
obchod	obchod	k1gInSc1	obchod
rychle	rychle	k6eAd1	rychle
rostl	růst	k5eAaImAgInS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
vliv	vliv	k1gInSc4	vliv
světové	světový	k2eAgFnSc2d1	světová
ekonomiky	ekonomika	k1gFnSc2	ekonomika
na	na	k7c6	na
SSSR	SSSR	kA	SSSR
omezen	omezit	k5eAaPmNgInS	omezit
pevnými	pevný	k2eAgFnPc7d1	pevná
domácími	domácí	k2eAgFnPc7d1	domácí
cenami	cena	k1gFnPc7	cena
a	a	k8xC	a
státním	státní	k2eAgInSc7d1	státní
monopolem	monopol	k1gInSc7	monopol
na	na	k7c4	na
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
hlavními	hlavní	k2eAgInPc7d1	hlavní
objekty	objekt	k1gInPc7	objekt
dovozu	dovoz	k1gInSc2	dovoz
obilí	obilí	k1gNnSc2	obilí
a	a	k8xC	a
sofistikované	sofistikovaný	k2eAgInPc4d1	sofistikovaný
spotřební	spotřební	k2eAgInPc4d1	spotřební
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
závodů	závod	k1gInPc2	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc6	zbrojení
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
sovětská	sovětský	k2eAgFnSc1d1	sovětská
ekonomika	ekonomika	k1gFnSc1	ekonomika
zatěžována	zatěžovat	k5eAaImNgFnS	zatěžovat
vojenskými	vojenský	k2eAgInPc7d1	vojenský
výdaji	výdaj	k1gInPc7	výdaj
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yRgMnPc4	který
byly	být	k5eAaImAgFnP	být
silně	silně	k6eAd1	silně
lobovány	lobovat	k5eAaBmNgFnP	lobovat
mocnou	mocný	k2eAgFnSc7d1	mocná
byrokracií	byrokracie	k1gFnSc7	byrokracie
závislou	závislý	k2eAgFnSc7d1	závislá
na	na	k7c6	na
zbrojním	zbrojní	k2eAgInSc6d1	zbrojní
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
stal	stát	k5eAaPmAgMnS	stát
největším	veliký	k2eAgMnSc7d3	veliký
vývozcem	vývozce	k1gMnSc7	vývozce
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
až	až	k9	až
do	do	k7c2	do
rozpadu	rozpad	k1gInSc2	rozpad
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
fungování	fungování	k1gNnSc1	fungování
ekonomiky	ekonomika	k1gFnSc2	ekonomika
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stejné	stejný	k2eAgFnSc6d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
byla	být	k5eAaImAgFnS	být
formálně	formálně	k6eAd1	formálně
řízena	řídit	k5eAaImNgFnS	řídit
centrálním	centrální	k2eAgNnSc7d1	centrální
plánováním	plánování	k1gNnSc7	plánování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
prováděl	provádět	k5eAaImAgInS	provádět
Gosplan	Gosplan	k1gInSc1	Gosplan
a	a	k8xC	a
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
v	v	k7c6	v
pětiletých	pětiletý	k2eAgInPc6d1	pětiletý
plánech	plán	k1gInPc6	plán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
však	však	k9	však
byly	být	k5eAaImAgInP	být
plány	plán	k1gInPc1	plán
velmi	velmi	k6eAd1	velmi
"	"	kIx"	"
<g/>
agregované	agregovaný	k2eAgFnSc3d1	agregovaná
<g/>
"	"	kIx"	"
a	a	k8xC	a
provizorní	provizorní	k2eAgMnSc1d1	provizorní
<g/>
,	,	kIx,	,
s	s	k7c7	s
výhradou	výhrada	k1gFnSc7	výhrada
intervencí	intervence	k1gFnPc2	intervence
ad	ad	k7c4	ad
hoc	hoc	k?	hoc
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nadřízených	nadřízený	k1gMnPc2	nadřízený
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
klíčová	klíčový	k2eAgNnPc1d1	klíčové
hospodářská	hospodářský	k2eAgNnPc1d1	hospodářské
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
byla	být	k5eAaImAgNnP	být
přijata	přijmout	k5eAaPmNgNnP	přijmout
politickým	politický	k2eAgNnSc7d1	politické
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Přidělené	přidělený	k2eAgInPc1d1	přidělený
prostředky	prostředek	k1gInPc1	prostředek
a	a	k8xC	a
cíle	cíl	k1gInPc1	cíl
plánu	plán	k1gInSc2	plán
byly	být	k5eAaImAgInP	být
obvykle	obvykle	k6eAd1	obvykle
denominovány	denominovat	k5eAaPmNgInP	denominovat
spíše	spíše	k9	spíše
v	v	k7c6	v
rublech	rubl	k1gInPc6	rubl
než	než	k8xS	než
ve	v	k7c6	v
fyzickém	fyzický	k2eAgNnSc6d1	fyzické
zboží	zboží	k1gNnSc6	zboží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
součástí	součást	k1gFnSc7	součást
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
centralizace	centralizace	k1gFnSc1	centralizace
konce	konec	k1gInSc2	konec
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
masivnímu	masivní	k2eAgInSc3d1	masivní
rozvoji	rozvoj	k1gInSc3	rozvoj
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
aerolinek	aerolinka	k1gFnPc2	aerolinka
Aeroflot	aeroflot	k1gInSc1	aeroflot
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
disponovala	disponovat	k5eAaBmAgFnS	disponovat
širokým	široký	k2eAgNnSc7d1	široké
spektrem	spektrum	k1gNnSc7	spektrum
dopravy	doprava	k1gFnSc2	doprava
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
kvůli	kvůli	k7c3	kvůli
špatné	špatný	k2eAgFnSc3d1	špatná
údržbě	údržba	k1gFnSc3	údržba
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
sovětské	sovětský	k2eAgFnSc2d1	sovětská
silniční	silniční	k2eAgFnSc2d1	silniční
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc2d1	vodní
a	a	k8xC	a
civilní	civilní	k2eAgFnSc2d1	civilní
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
západním	západní	k2eAgInSc7d1	západní
světem	svět	k1gInSc7	svět
zastaralá	zastaralý	k2eAgFnSc1d1	zastaralá
a	a	k8xC	a
technologicky	technologicky	k6eAd1	technologicky
zaostalá	zaostalý	k2eAgFnSc1d1	zaostalá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
nejintenzivněji	intenzivně	k6eAd3	intenzivně
používaná	používaný	k2eAgNnPc1d1	používané
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
sovětští	sovětský	k2eAgMnPc1d1	sovětský
ekonomové	ekonom	k1gMnPc1	ekonom
volali	volat	k5eAaImAgMnP	volat
po	po	k7c6	po
výstavbě	výstavba	k1gFnSc6	výstavba
více	hodně	k6eAd2	hodně
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zmírnili	zmírnit	k5eAaPmAgMnP	zmírnit
zatížení	zatížení	k1gNnSc4	zatížení
železnic	železnice	k1gFnPc2	železnice
a	a	k8xC	a
zlepšili	zlepšit	k5eAaPmAgMnP	zlepšit
sovětský	sovětský	k2eAgInSc4d1	sovětský
vládní	vládní	k2eAgInSc4d1	vládní
rozpočet	rozpočet	k1gInSc4	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnSc4d1	silniční
síť	síť	k1gFnSc4	síť
a	a	k8xC	a
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
nerozvinuté	rozvinutý	k2eNgFnPc1d1	nerozvinutá
a	a	k8xC	a
mimo	mimo	k6eAd1	mimo
velká	velký	k2eAgNnPc1d1	velké
města	město	k1gNnPc1	město
byly	být	k5eAaImAgFnP	být
běžné	běžný	k2eAgFnPc4d1	běžná
polní	polní	k2eAgFnPc4d1	polní
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
údržba	údržba	k1gFnSc1	údržba
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
neschopná	schopný	k2eNgFnSc1d1	neschopná
postarat	postarat	k5eAaPmF	postarat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c4	o
ty	ten	k3xDgFnPc4	ten
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
země	země	k1gFnSc1	země
již	již	k6eAd1	již
měla	mít	k5eAaImAgFnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
sovětské	sovětský	k2eAgInPc1d1	sovětský
úřady	úřad	k1gInPc1	úřad
snažily	snažit	k5eAaImAgInP	snažit
vyřešit	vyřešit	k5eAaPmF	vyřešit
problém	problém	k1gInSc4	problém
silnic	silnice	k1gFnPc2	silnice
objednáním	objednání	k1gNnSc7	objednání
výstavby	výstavba	k1gFnSc2	výstavba
nových	nový	k2eAgInPc2d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Automobilový	automobilový	k2eAgInSc1d1	automobilový
průmysl	průmysl	k1gInSc1	průmysl
mezitím	mezitím	k6eAd1	mezitím
rostl	růst	k5eAaImAgInS	růst
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
výstavba	výstavba	k1gFnSc1	výstavba
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Nerozvinutá	rozvinutý	k2eNgFnSc1d1	nerozvinutá
silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
poptávce	poptávka	k1gFnSc3	poptávka
po	po	k7c6	po
veřejné	veřejný	k2eAgFnSc6d1	veřejná
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
<g/>
Navzdory	navzdory	k7c3	navzdory
zlepšení	zlepšení	k1gNnSc3	zlepšení
byly	být	k5eAaImAgInP	být
některé	některý	k3yIgInPc1	některý
aspekty	aspekt	k1gInPc1	aspekt
odvětví	odvětví	k1gNnSc4	odvětví
dopravy	doprava	k1gFnSc2	doprava
zmítány	zmítán	k2eAgInPc1d1	zmítán
problémy	problém	k1gInPc1	problém
kvůli	kvůli	k7c3	kvůli
zastaralé	zastaralý	k2eAgFnSc3d1	zastaralá
infrastruktuře	infrastruktura	k1gFnSc3	infrastruktura
<g/>
,	,	kIx,	,
nedostatku	nedostatek	k1gInSc3	nedostatek
investic	investice	k1gFnPc2	investice
<g/>
,	,	kIx,	,
korupci	korupce	k1gFnSc3	korupce
a	a	k8xC	a
špatnému	špatný	k2eAgNnSc3d1	špatné
rozhodování	rozhodování	k1gNnSc3	rozhodování
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgInPc1d1	sovětský
orgány	orgán	k1gInPc1	orgán
nebyly	být	k5eNaImAgInP	být
schopné	schopný	k2eAgInPc1d1	schopný
uspokojit	uspokojit	k5eAaPmF	uspokojit
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
dopravní	dopravní	k2eAgFnSc6d1	dopravní
infrastruktuře	infrastruktura	k1gFnSc6	infrastruktura
a	a	k8xC	a
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sovětské	sovětský	k2eAgNnSc1d1	sovětské
obchodní	obchodní	k2eAgNnSc1d1	obchodní
loďstvo	loďstvo	k1gNnSc1	loďstvo
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Nadměrná	nadměrný	k2eAgNnPc1d1	nadměrné
úmrtí	úmrtí	k1gNnPc1	úmrtí
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
ruské	ruský	k2eAgFnSc2d1	ruská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
poválečného	poválečný	k2eAgInSc2d1	poválečný
hladomoru	hladomor	k1gInSc2	hladomor
<g/>
)	)	kIx)	)
činila	činit	k5eAaImAgFnS	činit
celkem	celkem	k6eAd1	celkem
18	[number]	k4	18
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
26	[number]	k4	26
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941-45	[number]	k4	1941-45
<g/>
.	.	kIx.	.
</s>
<s>
Poválečná	poválečný	k2eAgFnSc1d1	poválečná
sovětská	sovětský	k2eAgFnSc1d1	sovětská
populace	populace	k1gFnSc1	populace
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
45	[number]	k4	45
až	až	k9	až
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
menší	malý	k2eAgNnPc1d2	menší
než	než	k8xS	než
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
předválečný	předválečný	k2eAgInSc1d1	předválečný
demografický	demografický	k2eAgInSc1d1	demografický
vývoj	vývoj	k1gInSc1	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Catherine	Catherin	k1gInSc5	Catherin
Merridale	Merridala	k1gFnSc6	Merridala
by	by	k9	by
"	"	kIx"	"
<g/>
rozumný	rozumný	k2eAgInSc4d1	rozumný
odhad	odhad	k1gInSc4	odhad
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
nadměrných	nadměrný	k2eAgNnPc2d1	nadměrné
úmrtí	úmrtí	k1gNnPc2	úmrtí
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
období	období	k1gNnSc4	období
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
asi	asi	k9	asi
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Porodnost	porodnost	k1gFnSc1	porodnost
se	se	k3xPyFc4	se
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
snížila	snížit	k5eAaPmAgFnS	snížit
z	z	k7c2	z
44,0	[number]	k4	44,0
na	na	k7c4	na
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
na	na	k7c4	na
18,0	[number]	k4	18,0
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
urbanizaci	urbanizace	k1gFnSc3	urbanizace
a	a	k8xC	a
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
průměrnému	průměrný	k2eAgInSc3d1	průměrný
věku	věk	k1gInSc3	věk
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
také	také	k9	také
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
postupný	postupný	k2eAgInSc4d1	postupný
pokles	pokles	k1gInSc4	pokles
-	-	kIx~	-
z	z	k7c2	z
23,7	[number]	k4	23,7
na	na	k7c4	na
tisíc	tisíc	k4xCgInSc4	tisíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
na	na	k7c4	na
8,7	[number]	k4	8,7
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
byla	být	k5eAaImAgFnS	být
míra	míra	k1gFnSc1	míra
porodnosti	porodnost	k1gFnSc2	porodnost
jižních	jižní	k2eAgFnPc2d1	jižní
republik	republika	k1gFnPc2	republika
v	v	k7c6	v
Zakavkazsku	Zakavkazsko	k1gNnSc6	Zakavkazsko
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
míra	míra	k1gFnSc1	míra
porodnosti	porodnost	k1gFnSc2	porodnost
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
částech	část	k1gFnPc6	část
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
v	v	k7c4	v
období	období	k1gNnSc4	období
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
dokonce	dokonce	k9	dokonce
ještě	ještě	k6eAd1	ještě
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
-	-	kIx~	-
fenomén	fenomén	k1gInSc1	fenomén
částečně	částečně	k6eAd1	částečně
připsaný	připsaný	k2eAgInSc1d1	připsaný
pomalejší	pomalý	k2eAgFnSc3d2	pomalejší
míře	míra	k1gFnSc3	míra
urbanizace	urbanizace	k1gFnSc2	urbanizace
a	a	k8xC	a
tradičně	tradičně	k6eAd1	tradičně
brzkým	brzký	k2eAgInPc3d1	brzký
sňatkům	sňatek	k1gInPc3	sňatek
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
republikách	republika	k1gFnPc6	republika
<g/>
.	.	kIx.	.
<g/>
Konec	konec	k1gInSc4	konec
60.	[number]	k4	60.
a	a	k8xC	a
70.	[number]	k4	70.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
svědky	svědek	k1gMnPc7	svědek
obrácení	obrácení	k1gNnSc1	obrácení
klesající	klesající	k2eAgFnPc1d1	klesající
trajektorie	trajektorie	k1gFnPc1	trajektorie
míry	míra	k1gFnSc2	míra
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
obzvláště	obzvláště	k6eAd1	obzvláště
výrazná	výrazný	k2eAgFnSc1d1	výrazná
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
produktivním	produktivní	k2eAgInSc6d1	produktivní
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
byla	být	k5eAaImAgFnS	být
převládající	převládající	k2eAgFnSc1d1	převládající
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
převážně	převážně	k6eAd1	převážně
slovanských	slovanský	k2eAgFnPc6d1	Slovanská
oblastech	oblast	k1gFnPc6	oblast
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
oficiálních	oficiální	k2eAgInPc2d1	oficiální
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
konce	konec	k1gInSc2	konec
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
zhoršení	zhoršení	k1gNnSc6	zhoršení
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
dospělých	dospělí	k1gMnPc2	dospělí
začala	začít	k5eAaPmAgFnS	začít
znovu	znovu	k6eAd1	znovu
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
kojenecké	kojenecký	k2eAgFnSc2d1	kojenecká
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
z	z	k7c2	z
24,7	[number]	k4	24,7
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
na	na	k7c4	na
27,9	[number]	k4	27,9
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
výzkumníci	výzkumník	k1gMnPc1	výzkumník
považovali	považovat	k5eAaImAgMnP	považovat
nárůst	nárůst	k1gInSc4	nárůst
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
reálný	reálný	k2eAgInSc4d1	reálný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
zhoršujících	zhoršující	k2eAgFnPc2d1	zhoršující
se	se	k3xPyFc4	se
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
jak	jak	k8xS	jak
dospělé	dospělý	k2eAgFnPc1d1	dospělá
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
kojenecké	kojenecký	k2eAgFnSc2d1	kojenecká
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
nebyly	být	k5eNaImAgFnP	být
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
úředníky	úředník	k1gMnPc4	úředník
vysvětlovány	vysvětlován	k2eAgMnPc4d1	vysvětlován
nebo	nebo	k8xC	nebo
obhajovány	obhajován	k2eAgMnPc4d1	obhajován
a	a	k8xC	a
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
prostě	prostě	k6eAd1	prostě
přestala	přestat	k5eAaPmAgFnS	přestat
všechny	všechen	k3xTgFnPc4	všechen
statistiky	statistika	k1gFnPc4	statistika
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
demografové	demograf	k1gMnPc1	demograf
a	a	k8xC	a
zdravotní	zdravotní	k2eAgMnPc1d1	zdravotní
specialisté	specialista	k1gMnPc1	specialista
mlčeli	mlčet	k5eAaImAgMnP	mlčet
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
publikace	publikace	k1gFnSc1	publikace
údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
úmrtnosti	úmrtnost	k1gFnSc6	úmrtnost
a	a	k8xC	a
vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
ponořit	ponořit	k5eAaPmF	ponořit
do	do	k7c2	do
skutečných	skutečný	k2eAgFnPc2d1	skutečná
příčin	příčina	k1gFnPc2	příčina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Etnické	etnický	k2eAgFnSc2d1	etnická
skupiny	skupina	k1gFnSc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
etnicky	etnicky	k6eAd1	etnicky
rozmanitým	rozmanitý	k2eAgInSc7d1	rozmanitý
státem	stát	k1gInSc7	stát
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
etnickými	etnický	k2eAgFnPc7d1	etnická
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
odhadován	odhadovat	k5eAaImNgMnS	odhadovat
na	na	k7c4	na
293	[number]	k4	293
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
Rusové	Rus	k1gMnPc1	Rus
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
<g/>
78	[number]	k4	78
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgFnSc1d1	následovaná
Ukrajinci	Ukrajinec	k1gMnPc7	Ukrajinec
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
<g/>
45	[number]	k4	45
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Uzbeky	Uzbek	k1gMnPc7	Uzbek
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
<g/>
84	[number]	k4	84
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
občané	občan	k1gMnPc1	občan
SSSR	SSSR	kA	SSSR
měli	mít	k5eAaImAgMnP	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
etnickou	etnický	k2eAgFnSc4d1	etnická
příslušnost	příslušnost	k1gFnSc4	příslušnost
<g/>
.	.	kIx.	.
</s>
<s>
Etnická	etnický	k2eAgFnSc1d1	etnická
příslušnost	příslušnost	k1gFnSc1	příslušnost
osoby	osoba	k1gFnSc2	osoba
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
rodiči	rodič	k1gMnPc7	rodič
dítěte	dítě	k1gNnSc2	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
šestnácti	šestnáct	k4xCc2	šestnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
rodiče	rodič	k1gMnPc1	rodič
neshodli	shodnout	k5eNaPmAgMnP	shodnout
<g/>
,	,	kIx,	,
dítě	dítě	k1gNnSc1	dítě
bylo	být	k5eAaImAgNnS	být
automaticky	automaticky	k6eAd1	automaticky
přiřazeno	přiřazen	k2eAgNnSc1d1	přiřazeno
k	k	k7c3	k
etnickému	etnický	k2eAgInSc3d1	etnický
původu	původ	k1gInSc3	původ
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
kvůli	kvůli	k7c3	kvůli
sovětské	sovětský	k2eAgFnSc3d1	sovětská
politice	politika	k1gFnSc3	politika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
některé	některý	k3yIgFnPc4	některý
menší	malý	k2eAgFnPc4d2	menší
minoritní	minoritní	k2eAgFnPc4d1	minoritní
etnické	etnický	k2eAgFnPc4d1	etnická
skupiny	skupina	k1gFnPc4	skupina
považovány	považován	k2eAgFnPc4d1	považována
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
větších	veliký	k2eAgFnPc2d2	veliký
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
gruzínští	gruzínský	k2eAgMnPc1d1	gruzínský
Mingrelové	Mingrel	k1gMnPc1	Mingrel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
zařazováni	zařazován	k2eAgMnPc1d1	zařazován
s	s	k7c7	s
jazykově	jazykově	k6eAd1	jazykově
příbuznými	příbuzný	k2eAgMnPc7d1	příbuzný
Gruzínci	Gruzínec	k1gMnPc7	Gruzínec
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
asimilovaly	asimilovat	k5eAaBmAgFnP	asimilovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgNnSc4d1	jiné
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
byly	být	k5eAaImAgFnP	být
přivedeny	přiveden	k2eAgFnPc1d1	přivedena
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Bělorusové	Bělorus	k1gMnPc1	Bělorus
a	a	k8xC	a
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
sdíleli	sdílet	k5eAaImAgMnP	sdílet
těsné	těsný	k2eAgFnPc4d1	těsná
kulturní	kulturní	k2eAgFnPc4d1	kulturní
vazby	vazba	k1gFnPc4	vazba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgFnSc2d1	jiná
skupiny	skupina	k1gFnSc2	skupina
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vícero	vícero	k1gNnSc1	vícero
národnostmi	národnost	k1gFnPc7	národnost
žijícími	žijící	k2eAgInPc7d1	žijící
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
rozvíjely	rozvíjet	k5eAaImAgInP	rozvíjet
etnické	etnický	k2eAgInPc1d1	etnický
antagonizmy	antagonizmus	k1gInPc1	antagonizmus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
jako	jako	k8xC	jako
mnohonárodnostní	mnohonárodnostní	k2eAgInSc1d1	mnohonárodnostní
stát	stát	k1gInSc1	stát
byl	být	k5eAaImAgInS	být
jazykově	jazykově	k6eAd1	jazykově
velmi	velmi	k6eAd1	velmi
bohatý	bohatý	k2eAgInSc1d1	bohatý
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dominující	dominující	k2eAgFnSc2d1	dominující
ruštiny	ruština	k1gFnSc2	ruština
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
území	území	k1gNnSc1	území
hovořilo	hovořit	k5eAaImAgNnS	hovořit
dalšími	další	k2eAgFnPc7d1	další
desítkami	desítka	k1gFnPc7	desítka
jazyků	jazyk	k1gInPc2	jazyk
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
jazykových	jazykový	k2eAgFnPc2d1	jazyková
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
jako	jako	k8xS	jako
taková	takový	k3xDgFnSc1	takový
neměla	mít	k5eNaImAgFnS	mít
oficiální	oficiální	k2eAgInSc4d1	oficiální
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
ruština	ruština	k1gFnSc1	ruština
jako	jako	k8xS	jako
řeč	řeč	k1gFnSc1	řeč
majoritní	majoritní	k2eAgFnSc1d1	majoritní
byla	být	k5eAaImAgFnS	být
mnohdy	mnohdy	k6eAd1	mnohdy
preferována	preferován	k2eAgFnSc1d1	preferována
a	a	k8xC	a
zdůrazňoval	zdůrazňovat	k5eAaImAgInS	zdůrazňovat
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
význam	význam	k1gInSc1	význam
jako	jako	k8xS	jako
hlavního	hlavní	k2eAgInSc2d1	hlavní
komunikačního	komunikační	k2eAgInSc2d1	komunikační
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
я	я	k?	я
м	м	k?	м
о	о	k?	о
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
bilingvismu	bilingvismus	k1gInSc3	bilingvismus
a	a	k8xC	a
multilingvismu	multilingvismus	k1gInSc3	multilingvismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
mnohými	mnohý	k2eAgMnPc7d1	mnohý
národy	národ	k1gInPc4	národ
nenáviděn	nenáviděn	k2eAgMnSc1d1	nenáviděn
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
pobaltských	pobaltský	k2eAgFnPc6d1	pobaltská
republikách	republika	k1gFnPc6	republika
<g/>
,	,	kIx,	,
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
jazyky	jazyk	k1gInPc1	jazyk
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
bolševici	bolševik	k1gMnPc1	bolševik
ustanovili	ustanovit	k5eAaPmAgMnP	ustanovit
základní	základní	k2eAgInPc4d1	základní
body	bod	k1gInPc4	bod
politiky	politika	k1gFnSc2	politika
rozvoje	rozvoj	k1gInSc2	rozvoj
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
marxistů	marxista	k1gMnPc2	marxista
se	se	k3xPyFc4	se
ale	ale	k9	ale
proti	proti	k7c3	proti
myšlence	myšlenka	k1gFnSc3	myšlenka
jednoho	jeden	k4xCgInSc2	jeden
národního	národní	k2eAgInSc2d1	národní
jazyka	jazyk	k1gInSc2	jazyk
postavila	postavit	k5eAaPmAgFnS	postavit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
pak	pak	k6eAd1	pak
tuto	tento	k3xDgFnSc4	tento
myšlenku	myšlenka	k1gFnSc4	myšlenka
bolševický	bolševický	k2eAgInSc4d1	bolševický
sjezd	sjezd	k1gInSc4	sjezd
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
tak	tak	k9	tak
k	k	k7c3	k
dočasné	dočasný	k2eAgFnSc3d1	dočasná
derusifikaci	derusifikace	k1gFnSc3	derusifikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
ostře	ostro	k6eAd1	ostro
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
s	s	k7c7	s
obdobím	období	k1gNnSc7	období
stalinismu	stalinismus	k1gInSc2	stalinismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
celé	celý	k2eAgInPc1d1	celý
národy	národ	k1gInPc1	národ
stěhovány	stěhován	k2eAgInPc1d1	stěhován
a	a	k8xC	a
rozprášeny	rozprášen	k2eAgInPc1d1	rozprášen
po	po	k7c4	po
území	území	k1gNnSc4	území
Svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
jazyků	jazyk	k1gInPc2	jazyk
jako	jako	k8xS	jako
politicky	politicky	k6eAd1	politicky
citlivá	citlivý	k2eAgFnSc1d1	citlivá
ještě	ještě	k9	ještě
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
carské	carský	k2eAgFnSc2d1	carská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
někdy	někdy	k6eAd1	někdy
násilné	násilný	k2eAgFnSc3d1	násilná
rusifikaci	rusifikace	k1gFnSc3	rusifikace
také	také	k9	také
docházelo	docházet	k5eAaImAgNnS	docházet
a	a	k8xC	a
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
30.	[number]	k4	30.
letech	let	k1gInPc6	let
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
některých	některý	k3yIgMnPc2	některý
jazyků	jazyk	k1gMnPc2	jazyk
národů	národ	k1gInPc2	národ
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
arabské	arabský	k2eAgNnSc4d1	arabské
písmo	písmo	k1gNnSc4	písmo
nahrazovala	nahrazovat	k5eAaImAgFnS	nahrazovat
cyrilice	cyrilice	k1gFnSc1	cyrilice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
u	u	k7c2	u
turkických	turkický	k2eAgInPc2d1	turkický
jazyků	jazyk	k1gInPc2	jazyk
nebo	nebo	k8xC	nebo
jidiš	jidiš	k6eAd1	jidiš
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
cizí	cizí	k2eAgInSc1d1	cizí
prvek	prvek	k1gInSc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
náhrada	náhrada	k1gFnSc1	náhrada
cyrilicí	cyrilice	k1gFnSc7	cyrilice
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
většinou	většinou	k6eAd1	většinou
zachovala	zachovat	k5eAaPmAgFnS	zachovat
i	i	k9	i
po	po	k7c4	po
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
středoasijských	středoasijský	k2eAgFnPc2d1	středoasijská
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
náboženské	náboženský	k2eAgFnSc6d1	náboženská
příslušnosti	příslušnost	k1gFnSc6	příslušnost
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
nebyly	být	k5eNaImAgFnP	být
zveřejněny	zveřejnit	k5eAaPmNgFnP	zveřejnit
a	a	k8xC	a
země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
ateistická	ateistický	k2eAgFnSc1d1	ateistická
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInPc1d1	západní
zdroje	zdroj	k1gInPc1	zdroj
však	však	k9	však
odhadovaly	odhadovat	k5eAaImAgInP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přibližně	přibližně	k6eAd1	přibližně
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
občanů	občan	k1gMnPc2	občan
byla	být	k5eAaImAgFnS	být
nábožensky	nábožensky	k6eAd1	nábožensky
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgNnPc7d3	nejdůležitější
náboženstvími	náboženství	k1gNnPc7	náboženství
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byly	být	k5eAaImAgFnP	být
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
islám	islám	k1gInSc4	islám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
křesťanství	křesťanství	k1gNnSc2	křesťanství
měla	mít	k5eAaImAgFnS	mít
největší	veliký	k2eAgFnSc4d3	veliký
podporu	podpora	k1gFnSc4	podpora
Ruská	ruský	k2eAgFnSc1d1	ruská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
zde	zde	k6eAd1	zde
i	i	k9	i
jiné	jiný	k2eAgFnSc2d1	jiná
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
řeckokatolické	řeckokatolický	k2eAgFnSc2d1	řeckokatolická
a	a	k8xC	a
různé	různý	k2eAgFnSc2d1	různá
protestantské	protestantský	k2eAgFnSc2d1	protestantská
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
muslimů	muslim	k1gMnPc2	muslim
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
sunnitům	sunnita	k1gMnPc3	sunnita
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
náležela	náležet	k5eAaImAgFnS	náležet
k	k	k7c3	k
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itům	itům	k1gMnSc1	itům
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
súfismus	súfismus	k1gInSc1	súfismus
byl	být	k5eAaImAgInS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
a	a	k8xC	a
Čečensku	Čečensko	k1gNnSc6	Čečensko
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zde	zde	k6eAd1	zde
žila	žít	k5eAaImAgFnS	žít
významná	významný	k2eAgFnSc1d1	významná
židovská	židovský	k2eAgFnSc1d1	židovská
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ateismus	ateismus	k1gInSc1	ateismus
byl	být	k5eAaImAgInS	být
zdůrazňován	zdůrazňovat	k5eAaImNgInS	zdůrazňovat
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
stát	stát	k5eAaImF	stát
náboženství	náboženství	k1gNnSc3	náboženství
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
míře	míra	k1gFnSc6	míra
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
potlačoval	potlačovat	k5eAaImAgInS	potlačovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
dekret	dekret	k1gInSc4	dekret
Rady	rada	k1gFnSc2	rada
lidových	lidový	k2eAgMnPc2d1	lidový
komisařů	komisař	k1gMnPc2	komisař
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Ruská	ruský	k2eAgFnSc1d1	ruská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
federativní	federativní	k2eAgFnSc1d1	federativní
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
RSFSR	RSFSR	kA	RSFSR
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
sekulární	sekulární	k2eAgInSc1d1	sekulární
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
také	také	k9	také
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
učení	učení	k1gNnSc3	učení
o	o	k7c4	o
náboženství	náboženství	k1gNnSc4	náboženství
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
vyučovány	vyučován	k2eAgInPc4d1	vyučován
předměty	předmět	k1gInPc4	předmět
obecné	obecný	k2eAgFnSc2d1	obecná
výuky	výuka	k1gFnSc2	výuka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vzdělávat	vzdělávat	k5eAaImF	vzdělávat
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyučováni	vyučovat	k5eAaImNgMnP	vyučovat
náboženství	náboženství	k1gNnSc3	náboženství
soukromě	soukromě	k6eAd1	soukromě
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
omezení	omezení	k1gNnSc4	omezení
<g/>
,	,	kIx,	,
přijatá	přijatý	k2eAgFnSc1d1	přijatá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
během	během	k7c2	během
prvních	první	k4xOgMnPc2	první
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
Stalinovy	Stalinův	k2eAgFnSc2d1	Stalinova
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
patřily	patřit	k5eAaImAgInP	patřit
výslovné	výslovný	k2eAgInPc1d1	výslovný
zákazy	zákaz	k1gInPc1	zákaz
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
církevních	církevní	k2eAgFnPc2d1	církevní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
setkání	setkání	k1gNnSc2	setkání
organizovaného	organizovaný	k2eAgNnSc2d1	organizované
studia	studio	k1gNnSc2	studio
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Různá	různý	k2eAgFnSc1d1	různá
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
i	i	k8xC	i
nekřesťanská	křesťanský	k2eNgNnPc1d1	nekřesťanské
zařízení	zařízení	k1gNnPc1	zařízení
byla	být	k5eAaImAgNnP	být
po	po	k7c6	po
tisících	tisící	k4xOgFnPc2	tisící
během	během	k7c2	během
20.	[number]	k4	20.
a	a	k8xC	a
30.	[number]	k4	30.
let	léto	k1gNnPc2	léto
zavírána	zavírán	k2eAgFnSc1d1	zavírána
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
až	až	k9	až
90	[number]	k4	90
procent	procento	k1gNnPc2	procento
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
synagog	synagoga	k1gFnPc2	synagoga
a	a	k8xC	a
mešit	mešita	k1gFnPc2	mešita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
fungovaly	fungovat	k5eAaImAgInP	fungovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Společnost	společnost	k1gFnSc1	společnost
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
systém	systém	k1gInSc1	systém
===	===	k?	===
</s>
</p>
<p>
<s>
Právo	právo	k1gNnSc1	právo
na	na	k7c4	na
bezplatné	bezplatný	k2eAgNnSc4d1	bezplatné
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
,	,	kIx,	,
od	od	k7c2	od
základního	základní	k2eAgNnSc2d1	základní
vzdělání	vzdělání	k1gNnSc2	vzdělání
až	až	k9	až
po	po	k7c4	po
vyšší	vysoký	k2eAgNnSc4d2	vyšší
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
všichni	všechen	k3xTgMnPc1	všechen
občané	občan	k1gMnPc1	občan
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zakotveno	zakotvit	k5eAaPmNgNnS	zakotvit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
45	[number]	k4	45
Ústavy	ústava	k1gFnSc2	ústava
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Media	medium	k1gNnPc4	medium
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Masová	masový	k2eAgNnPc1d1	masové
média	médium	k1gNnPc1	médium
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
ruka	ruka	k1gFnSc1	ruka
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
ovládat	ovládat	k5eAaImF	ovládat
a	a	k8xC	a
mobilizovat	mobilizovat	k5eAaBmF	mobilizovat
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
revoluce	revoluce	k1gFnSc2	revoluce
byli	být	k5eAaImAgMnP	být
Lenin	Lenin	k1gMnSc1	Lenin
a	a	k8xC	a
bolševici	bolševik	k1gMnPc1	bolševik
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
mediální	mediální	k2eAgFnSc6d1	mediální
podpoře	podpora	k1gFnSc6	podpora
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získali	získat	k5eAaPmAgMnP	získat
podporu	podpora	k1gFnSc4	podpora
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
publikací	publikace	k1gFnPc2	publikace
úřady	úřad	k1gInPc1	úřad
naléhaly	naléhat	k5eAaBmAgInP	naléhat
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
"	"	kIx"	"
<g/>
budovali	budovat	k5eAaImAgMnP	budovat
socialismus	socialismus	k1gInSc4	socialismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
propagandy	propaganda	k1gFnSc2	propaganda
sovětských	sovětský	k2eAgInPc2d1	sovětský
úřadů	úřad	k1gInPc2	úřad
staly	stát	k5eAaPmAgInP	stát
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
televize	televize	k1gFnSc1	televize
a	a	k8xC	a
počítače	počítač	k1gInPc1	počítač
a	a	k8xC	a
všechna	všechen	k3xTgNnPc1	všechen
média	médium	k1gNnPc1	médium
byla	být	k5eAaImAgNnP	být
používána	používán	k2eAgNnPc1d1	používáno
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
marxisticko-leninských	marxistickoeninský	k2eAgFnPc2d1	marxisticko-leninská
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Technologický	technologický	k2eAgInSc1d1	technologický
vývoj	vývoj	k1gInSc1	vývoj
však	však	k9	však
ztížil	ztížit	k5eAaPmAgInS	ztížit
udržení	udržení	k1gNnSc4	udržení
přilnavosti	přilnavost	k1gFnSc2	přilnavost
k	k	k7c3	k
masové	masový	k2eAgFnSc3d1	masová
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
domácí	domácí	k2eAgInPc4d1	domácí
videosystémy	videosystém	k1gInPc4	videosystém
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
znesnadňovaly	znesnadňovat	k5eAaImAgInP	znesnadňovat
kontrolu	kontrola	k1gFnSc4	kontrola
cirkulace	cirkulace	k1gFnSc2	cirkulace
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
videokazet	videokazeta	k1gFnPc2	videokazeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
vycházelo	vycházet	k5eAaImAgNnS	vycházet
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
více	hodně	k6eAd2	hodně
než	než	k8xS	než
8 000	[number]	k4	8 000
denních	denní	k2eAgFnPc2d1	denní
tiskovin	tiskovina	k1gFnPc2	tiskovina
v	v	k7c6	v
60	[number]	k4	60
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
měly	mít	k5eAaImAgFnP	mít
celkový	celkový	k2eAgInSc4d1	celkový
oběh	oběh	k1gInSc4	oběh
asi	asi	k9	asi
170	[number]	k4	170
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Vnitrostátní	vnitrostátní	k2eAgFnPc1d1	vnitrostátní
noviny	novina	k1gFnPc1	novina
byly	být	k5eAaImAgFnP	být
vždycky	vždycky	k6eAd1	vždycky
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jen	jen	k9	jen
polovina	polovina	k1gFnSc1	polovina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byla	být	k5eAaImAgFnS	být
etnickými	etnický	k2eAgMnPc7d1	etnický
Rusy	Rus	k1gMnPc7	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
jazyky	jazyk	k1gInPc1	jazyk
představovaly	představovat	k5eAaImAgInP	představovat
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
<g/>
%	%	kIx~	%
celkového	celkový	k2eAgNnSc2d1	celkové
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
novináři	novinář	k1gMnPc1	novinář
a	a	k8xC	a
redaktoři	redaktor	k1gMnPc1	redaktor
novinářů	novinář	k1gMnPc2	novinář
byli	být	k5eAaImAgMnP	být
členy	člen	k1gInPc4	člen
stranicky	stranicky	k6eAd1	stranicky
řízené	řízený	k2eAgFnSc2d1	řízená
odborové	odborový	k2eAgFnSc2d1	odborová
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
novináře	novinář	k1gMnPc4	novinář
a	a	k8xC	a
80	[number]	k4	80
<g/>
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
členy	člen	k1gMnPc4	člen
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
redaktoři	redaktor	k1gMnPc1	redaktor
byli	být	k5eAaImAgMnP	být
také	také	k9	také
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
stranou	stranou	k6eAd1	stranou
<g/>
.	.	kIx.	.
</s>
<s>
Žurnalistické	žurnalistický	k2eAgNnSc1d1	žurnalistické
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
silně	silně	k6eAd1	silně
politicky	politicky	k6eAd1	politicky
řízeno	řídit	k5eAaImNgNnS	řídit
a	a	k8xC	a
běžná	běžný	k2eAgFnSc1d1	běžná
cesta	cesta	k1gFnSc1	cesta
v	v	k7c6	v
novinovém	novinový	k2eAgInSc6d1	novinový
průmyslu	průmysl	k1gInSc6	průmysl
vedla	vést	k5eAaImAgFnS	vést
přes	přes	k7c4	přes
katedru	katedra	k1gFnSc4	katedra
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
na	na	k7c6	na
Moskevské	moskevský	k2eAgFnSc6d1	Moskevská
státní	státní	k2eAgFnSc6d1	státní
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studenti	student	k1gMnPc1	student
dostali	dostat	k5eAaPmAgMnP	dostat
dobré	dobrý	k2eAgNnSc4d1	dobré
vzdělání	vzdělání	k1gNnSc4	vzdělání
ve	v	k7c6	v
stranické	stranický	k2eAgFnSc6d1	stranická
linii	linie	k1gFnSc6	linie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
deníků	deník	k1gInPc2	deník
byla	být	k5eAaImAgFnS	být
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
Pravda	pravda	k1gFnSc1	pravda
s	s	k7c7	s
20	[number]	k4	20
miliony	milion	k4xCgInPc7	milion
čtenáři	čtenář	k1gMnPc7	čtenář
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Soustředila	soustředit	k5eAaPmAgFnS	soustředit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
události	událost	k1gFnPc4	událost
v	v	k7c6	v
komunistické	komunistický	k2eAgFnSc6d1	komunistická
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
domácí	domácí	k1gMnPc4	domácí
i	i	k9	i
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
novinami	novina	k1gFnPc7	novina
byla	být	k5eAaImAgFnS	být
Izvestija	Izvestija	k1gFnSc1	Izvestija
(	(	kIx(	(
<g/>
vydávaná	vydávaný	k2eAgNnPc4d1	vydávané
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
sovětem	sovět	k1gInSc7	sovět
s	s	k7c7	s
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
miliony	milion	k4xCgInPc7	milion
čtenáři	čtenář	k1gMnPc7	čtenář
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trud	trud	k1gInSc1	trud
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Práce	práce	k1gFnSc1	práce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydávaná	vydávaný	k2eAgFnSc1d1	vydávaná
odborovými	odborový	k2eAgInPc7d1	odborový
svazy	svaz	k1gInPc1	svaz
s	s	k7c7	s
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
miliony	milion	k4xCgInPc7	milion
čtenáři	čtenář	k1gMnPc7	čtenář
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
Komsomolskaja	Komsomolskaj	k2eAgFnSc1d1	Komsomolskaja
pravda	pravda	k1gFnSc1	pravda
(	(	kIx(	(
<g/>
vydávaná	vydávaný	k2eAgFnSc1d1	vydávaná
Komsomolem	Komsomol	k1gInSc7	Komsomol
s	s	k7c7	s
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
miliony	milion	k4xCgInPc7	milion
čtenáři	čtenář	k1gMnPc7	čtenář
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
masovým	masový	k2eAgNnSc7d1	masové
médiem	médium	k1gNnSc7	médium
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
ji	on	k3xPp3gFnSc4	on
mělo	mít	k5eAaImAgNnS	mít
přibližně	přibližně	k6eAd1	přibližně
75	[number]	k4	75
milionů	milion	k4xCgInPc2	milion
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
93	[number]	k4	93
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
televizi	televize	k1gFnSc4	televize
sledovalo	sledovat	k5eAaImAgNnS	sledovat
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
měl	mít	k5eAaImAgMnS	mít
čtyři	čtyři	k4xCgMnPc4	čtyři
televizní	televizní	k2eAgInPc4d1	televizní
kanály	kanál	k1gInPc7	kanál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vysílaly	vysílat	k5eAaImAgInP	vysílat
denně	denně	k6eAd1	denně
pětatřicet	pětatřicet	k4xCc4	pětatřicet
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
kanály	kanál	k1gInPc1	kanál
vysílaly	vysílat	k5eAaImAgInP	vysílat
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
20	[number]	k4	20
<g/>
%	%	kIx~	%
času	čas	k1gInSc2	čas
byly	být	k5eAaImAgFnP	být
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
program	program	k1gInSc1	program
Vremja	Vremjus	k1gMnSc4	Vremjus
byl	být	k5eAaImAgInS	být
vlajkovou	vlajkový	k2eAgFnSc7d1	vlajková
lodí	loď	k1gFnSc7	loď
<g/>
.	.	kIx.	.
</s>
<s>
Vremju	Vremju	k5eAaPmIp1nS	Vremju
sledovalo	sledovat	k5eAaImAgNnS	sledovat
80	[number]	k4	80
až	až	k8xS	až
90	[number]	k4	90
<g/>
%	%	kIx~	%
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vysílána	vysílat	k5eAaImNgFnS	vysílat
v	v	k7c6	v
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
00	[number]	k4	00
hodin	hodina	k1gFnPc2	hodina
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
zaměřením	zaměření	k1gNnSc7	zaměření
zpráv	zpráva	k1gFnPc2	zpráva
byly	být	k5eAaImAgFnP	být
zprávy	zpráva	k1gFnPc1	zpráva
z	z	k7c2	z
domova	domov	k1gInSc2	domov
a	a	k8xC	a
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
sportu	sport	k1gInSc6	sport
bylo	být	k5eAaImAgNnS	být
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
programem	program	k1gInSc7	program
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgNnSc4d1	zahraniční
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
bylo	být	k5eAaImAgNnS	být
Vokrug	Vokrug	k1gInSc4	Vokrug
sveta	svet	k1gInSc2	svet
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
dvěma	dva	k4xCgMnPc7	dva
vysílání	vysílání	k1gNnSc4	vysílání
každé	každý	k3xTgNnSc4	každý
odpoledne	odpoledne	k1gNnSc4	odpoledne
a	a	k8xC	a
večer	večer	k6eAd1	večer
denně	denně	k6eAd1	denně
přilákal	přilákat	k5eAaPmAgInS	přilákat
mezi	mezi	k7c7	mezi
60	[number]	k4	60
a	a	k8xC	a
90	[number]	k4	90
miliony	milion	k4xCgInPc1	milion
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
televizní	televizní	k2eAgInPc1d1	televizní
pořady	pořad	k1gInPc1	pořad
byly	být	k5eAaImAgInP	být
ideologického	ideologický	k2eAgInSc2d1	ideologický
rázu	ráz	k1gInSc2	ráz
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
diváky	divák	k1gMnPc4	divák
podněcovali	podněcovat	k5eAaImAgMnP	podněcovat
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgInPc1d1	televizní
filmy	film	k1gInPc1	film
často	často	k6eAd1	často
hrály	hrát	k5eAaImAgInP	hrát
na	na	k7c4	na
druhoválečně	druhoválečně	k6eAd1	druhoválečně
heroismy	heroismus	k1gInPc7	heroismus
nebo	nebo	k8xC	nebo
na	na	k7c4	na
policejní	policejní	k2eAgFnPc4d1	policejní
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnPc4d1	bezpečnostní
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
"	"	kIx"	"
<g/>
imperialistickým	imperialistický	k2eAgFnPc3d1	imperialistická
hrozbám	hrozba	k1gFnPc3	hrozba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dětské	dětský	k2eAgInPc1d1	dětský
programy	program	k1gInPc1	program
byly	být	k5eAaImAgInP	být
vždy	vždy	k6eAd1	vždy
nenásilné	násilný	k2eNgFnSc2d1	nenásilná
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zábavní	zábavní	k2eAgInPc1d1	zábavní
programy	program	k1gInPc1	program
zdůrazňovaly	zdůrazňovat	k5eAaImAgInP	zdůrazňovat
správné	správný	k2eAgNnSc4d1	správné
společenské	společenský	k2eAgFnPc4d1	společenská
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
televize	televize	k1gFnSc2	televize
a	a	k8xC	a
novin	novina	k1gFnPc2	novina
také	také	k6eAd1	také
rozhlasové	rozhlasový	k2eAgNnSc1d1	rozhlasové
vysílání	vysílání	k1gNnSc1	vysílání
mělo	mít	k5eAaImAgNnS	mít
ideologickou	ideologický	k2eAgFnSc4d1	ideologická
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Úřady	úřad	k1gInPc1	úřad
vysílaly	vysílat	k5eAaImAgInP	vysílat
celkem	celkem	k6eAd1	celkem
1 400	[number]	k4	1 400
hodin	hodina	k1gFnPc2	hodina
rozhlasu	rozhlas	k1gInSc2	rozhlas
denně	denně	k6eAd1	denně
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
vysílače	vysílač	k1gInPc1	vysílač
vysílaly	vysílat	k5eAaImAgInP	vysílat
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
osm	osm	k4xCc1	osm
stanic	stanice	k1gFnPc2	stanice
vysílalo	vysílat	k5eAaImAgNnS	vysílat
180	[number]	k4	180
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
bylo	být	k5eAaImAgNnS	být
Rádio	rádio	k1gNnSc1	rádio
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
sovětským	sovětský	k2eAgInSc7d1	sovětský
státním	státní	k2eAgNnPc3d1	státní
oficiálními	oficiální	k2eAgFnPc7d1	oficiální
mluvčím	mluvčí	k1gMnSc7	mluvčí
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
70.	[number]	k4	70.
let	léto	k1gNnPc2	léto
vysílalo	vysílat	k5eAaImAgNnS	vysílat
254	[number]	k4	254
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
77	[number]	k4	77
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
vláda	vláda	k1gFnSc1	vláda
nad	nad	k7c7	nad
éterem	éter	k1gInSc7	éter
nebyla	být	k5eNaImAgFnS	být
taková	takový	k3xDgFnSc1	takový
jako	jako	k9	jako
u	u	k7c2	u
televize	televize	k1gFnSc2	televize
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
část	část	k1gFnSc1	část
země	země	k1gFnSc1	země
chytala	chytat	k5eAaImAgFnS	chytat
finskou	finský	k2eAgFnSc4d1	finská
televizi	televize	k1gFnSc4	televize
<g/>
)	)	kIx)	)
a	a	k8xC	a
státní	státní	k2eAgFnPc1d1	státní
stanice	stanice	k1gFnPc1	stanice
měly	mít	k5eAaImAgFnP	mít
konkurovat	konkurovat	k5eAaImF	konkurovat
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
stanicím	stanice	k1gFnPc3	stanice
navzdory	navzdory	k7c3	navzdory
pokusu	pokus	k1gInSc3	pokus
sovětských	sovětský	k2eAgInPc2d1	sovětský
úřadů	úřad	k1gInPc2	úřad
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
zablokování	zablokování	k1gNnSc4	zablokování
<g/>
.	.	kIx.	.
</s>
<s>
Důležitými	důležitý	k2eAgFnPc7d1	důležitá
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
stanicemi	stanice	k1gFnPc7	stanice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Hlas	hlas	k1gInSc1	hlas
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
BBC	BBC	kA	BBC
a	a	k8xC	a
Deutsche	Deutsche	k1gFnSc1	Deutsche
Welle	Welle	k1gFnSc1	Welle
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgInPc1	dva
až	až	k8xS	až
tři	tři	k4xCgInPc1	tři
miliony	milion	k4xCgInPc1	milion
sovětských	sovětský	k2eAgMnPc2d1	sovětský
občanů	občan	k1gMnPc2	občan
pravidelně	pravidelně	k6eAd1	pravidelně
poslouchali	poslouchat	k5eAaImAgMnP	poslouchat
zahraniční	zahraniční	k2eAgNnSc4d1	zahraniční
rádio	rádio	k1gNnSc4	rádio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
život	život	k1gInSc1	život
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
prošel	projít	k5eAaPmAgInS	projít
několika	několik	k4yIc7	několik
fázemi	fáze	k1gFnPc7	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918-1929	[number]	k4	1918-1929
existovaly	existovat	k5eAaImAgFnP	existovat
poměrně	poměrně	k6eAd1	poměrně
svobodné	svobodný	k2eAgFnPc1d1	svobodná
podmínky	podmínka	k1gFnPc1	podmínka
a	a	k8xC	a
umělci	umělec	k1gMnPc1	umělec
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
hledání	hledání	k1gNnSc2	hledání
výrazného	výrazný	k2eAgInSc2d1	výrazný
sovětského	sovětský	k2eAgInSc2d1	sovětský
uměleckého	umělecký	k2eAgInSc2d1	umělecký
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umění	umění	k1gNnSc1	umění
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
masám	masa	k1gFnPc3	masa
<g/>
.	.	kIx.	.
</s>
<s>
Orgány	orgány	k1gFnPc1	orgány
tolerovaly	tolerovat	k5eAaImAgFnP	tolerovat
více	hodně	k6eAd2	hodně
trendů	trend	k1gInPc2	trend
pokud	pokud	k8xS	pokud
nebyly	být	k5eNaImAgFnP	být
zjevně	zjevně	k6eAd1	zjevně
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
vzkvétalo	vzkvétat	k5eAaImAgNnS	vzkvétat
několik	několik	k4yIc1	několik
různých	různý	k2eAgFnPc2d1	různá
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
tradiční	tradiční	k2eAgFnPc4d1	tradiční
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc4d1	jiná
radikálně	radikálně	k6eAd1	radikálně
experimentální	experimentální	k2eAgFnPc1d1	experimentální
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
působili	působit	k5eAaImAgMnP	působit
komunisté	komunista	k1gMnPc1	komunista
jako	jako	k8xC	jako
Maxim	Maxim	k1gMnSc1	Maxim
Gorkij	Gorkij	k1gMnSc1	Gorkij
a	a	k8xC	a
Vladimir	Vladimir	k1gMnSc1	Vladimir
Majakovskij	Majakovskij	k1gMnSc1	Majakovskij
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
produkce	produkce	k1gFnSc1	produkce
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
užitečný	užitečný	k2eAgInSc4d1	užitečný
propagační	propagační	k2eAgInSc4d1	propagační
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
širokých	široký	k2eAgFnPc2d1	široká
vrstev	vrstva	k1gFnPc2	vrstva
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
povětšinou	povětšina	k1gFnSc7	povětšina
negramotní	gramotný	k2eNgMnPc1d1	negramotný
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
montážní	montážní	k2eAgFnSc1d1	montážní
škola	škola	k1gFnSc1	škola
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
období	období	k1gNnSc6	období
1920	[number]	k4	1920
až	až	k9	až
1932	[number]	k4	1932
své	své	k1gNnSc4	své
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
filmů	film	k1gInPc2	film
režiséra	režisér	k1gMnSc2	režisér
Sergeje	Sergej	k1gMnSc2	Sergej
Ejzenštejn	Ejzenštejn	k1gMnSc1	Ejzenštejn
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
filmu	film	k1gInSc2	film
Křižník	křižník	k1gInSc4	křižník
Potěmkin	Potěmkin	k1gMnSc1	Potěmkin
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
se	se	k3xPyFc4	se
také	také	k6eAd1	také
stalo	stát	k5eAaPmAgNnS	stát
jednotným	jednotný	k2eAgMnSc7d1	jednotný
a	a	k8xC	a
zaměřovalo	zaměřovat	k5eAaImAgNnS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c6	na
ukázání	ukázání	k1gNnSc6	ukázání
nadvlády	nadvláda	k1gFnSc2	nadvláda
komunismu	komunismus	k1gInSc2	komunismus
nad	nad	k7c7	nad
všemi	všecek	k3xTgFnPc7	všecek
ostatními	ostatní	k2eAgFnPc7d1	ostatní
formami	forma	k1gFnPc7	forma
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
během	během	k7c2	během
Stalinovy	Stalinův	k2eAgFnSc2d1	Stalinova
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
sovětská	sovětský	k2eAgFnSc1d1	sovětská
kultura	kultura	k1gFnSc1	kultura
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
vzestupem	vzestup	k1gInSc7	vzestup
a	a	k8xC	a
provládním	provládní	k2eAgInSc7d1	provládní
stylem	styl	k1gInSc7	styl
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
trendy	trend	k1gInPc1	trend
byly	být	k5eAaImAgInP	být
silně	silně	k6eAd1	silně
potlačovány	potlačován	k2eAgInPc1d1	potlačován
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
výjimky	výjimka	k1gFnPc1	výjimka
jako	jako	k9	jako
například	například	k6eAd1	například
Michail	Michail	k1gInSc1	Michail
Bulgakov	Bulgakov	k1gInSc1	Bulgakov
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
spisovatelů	spisovatel	k1gMnPc2	spisovatel
bylo	být	k5eAaImAgNnS	být
uvězněno	uvěznit	k5eAaPmNgNnS	uvěznit
a	a	k8xC	a
zabito	zabít	k5eAaPmNgNnS	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Chruščovovském	Chruščovovský	k2eAgNnSc6d1	Chruščovovský
tání	tání	k1gNnSc6	tání
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
padesátých	padesátý	k4xOgNnPc2	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
cenzury	cenzura	k1gFnSc2	cenzura
ubylo	ubýt	k5eAaPmAgNnS	ubýt
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
období	období	k1gNnSc4	období
sovětské	sovětský	k2eAgFnSc2d1	sovětská
kultury	kultura	k1gFnSc2	kultura
charakterizované	charakterizovaný	k2eAgNnSc1d1	charakterizované
konformním	konformní	k2eAgInSc7d1	konformní
veřejným	veřejný	k2eAgInSc7d1	veřejný
životem	život	k1gInSc7	život
a	a	k8xC	a
intenzivním	intenzivní	k2eAgNnSc7d1	intenzivní
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
experimenty	experiment	k1gInPc1	experiment
s	s	k7c7	s
uměleckými	umělecký	k2eAgFnPc7d1	umělecká
formami	forma	k1gFnPc7	forma
byly	být	k5eAaImAgFnP	být
znovu	znovu	k6eAd1	znovu
povoleny	povolen	k2eAgFnPc1d1	povolena
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
sofistikovanější	sofistikovaný	k2eAgFnPc1d2	sofistikovanější
a	a	k8xC	a
jemně	jemně	k6eAd1	jemně
kritické	kritický	k2eAgFnPc4d1	kritická
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
socialistický	socialistický	k2eAgInSc4d1	socialistický
realismus	realismus	k1gInSc4	realismus
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
mnoho	mnoho	k4c1	mnoho
postav	postava	k1gFnPc2	postava
románů	román	k1gInPc2	román
autora	autor	k1gMnSc4	autor
Jurije	Jurije	k1gFnSc2	Jurije
Trifonova	Trifonův	k2eAgInSc2d1	Trifonův
se	se	k3xPyFc4	se
zabývalo	zabývat	k5eAaImAgNnS	zabývat
spíše	spíše	k9	spíše
problémy	problém	k1gInPc4	problém
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
než	než	k8xS	než
budováním	budování	k1gNnSc7	budování
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozdní	pozdní	k2eAgFnSc6d1	pozdní
době	doba	k1gFnSc6	doba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
podzemní	podzemní	k2eAgFnSc1d1	podzemní
disidentská	disidentský	k2eAgFnSc1d1	disidentská
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k9	jako
samizdat	samizdat	k1gInSc4	samizdat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
se	se	k3xPyFc4	se
Chruščovova	Chruščovův	k2eAgFnSc1d1	Chruščovova
éra	éra	k1gFnSc1	éra
soustředila	soustředit	k5eAaPmAgFnS	soustředit
především	především	k9	především
na	na	k7c4	na
funkční	funkční	k2eAgInSc4d1	funkční
design	design	k1gInSc4	design
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vysoce	vysoce	k6eAd1	vysoce
zdobeného	zdobený	k2eAgInSc2d1	zdobený
stylu	styl	k1gInSc2	styl
Stalinovy	Stalinův	k2eAgFnSc2d1	Stalinova
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
Gorbačovova	Gorbačovův	k2eAgFnSc1d1	Gorbačovova
politika	politika	k1gFnSc1	politika
perestrojky	perestrojka	k1gFnSc2	perestrojka
a	a	k8xC	a
glasnosti	glasnost	k1gFnSc2	glasnost
výrazně	výrazně	k6eAd1	výrazně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
svobodu	svoboda	k1gFnSc4	svoboda
projevu	projev	k1gInSc2	projev
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
i	i	k9	i
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
vždy	vždy	k6eAd1	vždy
existovala	existovat	k5eAaImAgFnS	existovat
jasná	jasný	k2eAgFnSc1d1	jasná
souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
sportem	sport	k1gInSc7	sport
a	a	k8xC	a
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
hlavní	hlavní	k2eAgFnSc1d1	hlavní
rezoluce	rezoluce	k1gFnSc1	rezoluce
strany	strana	k1gFnSc2	strana
o	o	k7c6	o
sportu	sport	k1gInSc6	sport
to	ten	k3xDgNnSc4	ten
jasně	jasně	k6eAd1	jasně
ukázala	ukázat	k5eAaPmAgFnS	ukázat
"	"	kIx"	"
<g/>
na	na	k7c4	na
sportovní	sportovní	k2eAgFnPc4d1	sportovní
aktivity	aktivita	k1gFnPc4	aktivita
by	by	kYmCp3nS	by
nemělo	mít	k5eNaImAgNnS	mít
nahlížet	nahlížet	k5eAaImF	nahlížet
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
aspektu	aspekt	k1gInSc2	aspekt
kulturního	kulturní	k2eAgInSc2d1	kulturní
<g/>
,	,	kIx,	,
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
a	a	k8xC	a
vojenského	vojenský	k2eAgInSc2d1	vojenský
výcviku	výcvik	k1gInSc2	výcvik
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
prostředku	prostředek	k1gInSc2	prostředek
socializace	socializace	k1gFnSc2	socializace
masy	masa	k1gFnSc2	masa
<g/>
...	...	k?	...
<g/>
ale	ale	k8xC	ale
jako	jako	k9	jako
prostředek	prostředek	k1gInSc4	prostředek
náboru	nábor	k1gInSc2	nábor
rolníků	rolník	k1gMnPc2	rolník
a	a	k8xC	a
pracovníků	pracovník	k1gMnPc2	pracovník
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
stranických	stranický	k2eAgFnPc2d1	stranická
<g/>
,	,	kIx,	,
pracovních	pracovní	k2eAgFnPc2d1	pracovní
a	a	k8xC	a
odborových	odborový	k2eAgFnPc2d1	odborová
organizací	organizace	k1gFnPc2	organizace
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
přivedeni	přivést	k5eAaPmNgMnP	přivést
k	k	k7c3	k
společenským	společenský	k2eAgFnPc3d1	společenská
a	a	k8xC	a
politickým	politický	k2eAgFnPc3d1	politická
činnostem	činnost	k1gFnPc3	činnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Sporty	sport	k1gInPc1	sport
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
organizovány	organizovat	k5eAaBmNgInP	organizovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spadal	spadat	k5eAaPmAgInS	spadat
pod	pod	k7c4	pod
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výbor	výbor	k1gInSc1	výbor
měl	mít	k5eAaImAgInS	mít
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
36	[number]	k4	36
sportovních	sportovní	k2eAgInPc2d1	sportovní
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
kromě	kromě	k7c2	kromě
dvou	dva	k4xCgMnPc6	dva
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
odborovými	odborový	k2eAgInPc7d1	odborový
svazy	svaz	k1gInPc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
příklady	příklad	k1gInPc4	příklad
známých	známý	k2eAgInPc2d1	známý
sportovních	sportovní	k2eAgInPc2d1	sportovní
klubů	klub	k1gInPc2	klub
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
odborovými	odborový	k2eAgFnPc7d1	odborová
organizacemi	organizace	k1gFnPc7	organizace
patřily	patřit	k5eAaImAgFnP	patřit
Locomotiv	Locomotiv	k1gFnSc7	Locomotiv
(	(	kIx(	(
<g/>
železniční	železniční	k2eAgMnPc1d1	železniční
pracovníci	pracovník	k1gMnPc1	pracovník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Burevestnik	Burevestnik	k1gInSc1	Burevestnik
(	(	kIx(	(
<g/>
studenti	student	k1gMnPc1	student
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spartak	Spartak	k1gInSc4	Spartak
(	(	kIx(	(
<g/>
osoby	osoba	k1gFnPc4	osoba
zaměstnané	zaměstnaný	k2eAgFnPc4d1	zaměstnaná
např.	např.	kA	např.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
kulturních	kulturní	k2eAgNnPc2d1	kulturní
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgInP	existovat
také	také	k6eAd1	také
dva	dva	k4xCgInPc4	dva
sportovní	sportovní	k2eAgInPc4d1	sportovní
kluby	klub	k1gInPc4	klub
mimo	mimo	k7c4	mimo
odbory	odbor	k1gInPc4	odbor
<g/>
;	;	kIx,	;
Dynamo	dynamo	k1gNnSc1	dynamo
(	(	kIx(	(
<g/>
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
KGB	KGB	kA	KGB
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
pracovní	pracovní	k2eAgFnSc1d1	pracovní
rezerva	rezerva	k1gFnSc1	rezerva
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
studenti	student	k1gMnPc1	student
technických	technický	k2eAgFnPc2d1	technická
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
ještě	ještě	k9	ještě
existoval	existovat	k5eAaImAgInS	existovat
armádní	armádní	k2eAgInSc1d1	armádní
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
CSKA	CSKA	kA	CSKA
<g/>
.	.	kIx.	.
</s>
<s>
Kluby	klub	k1gInPc1	klub
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
CSKA	CSKA	kA	CSKA
a	a	k8xC	a
Dynama	dynamo	k1gNnSc2	dynamo
slavily	slavit	k5eAaImAgInP	slavit
nejvíce	hodně	k6eAd3	hodně
sportovních	sportovní	k2eAgInPc2d1	sportovní
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
stál	stát	k5eAaImAgInS	stát
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
mimo	mimo	k7c4	mimo
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
sportovní	sportovní	k2eAgNnPc4d1	sportovní
hnutí	hnutí	k1gNnPc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
byl	být	k5eAaImAgInS	být
však	však	k9	však
založen	založit	k5eAaPmNgInS	založit
sovětský	sovětský	k2eAgInSc1d1	sovětský
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
a	a	k8xC	a
zimních	zimní	k2eAgMnPc2d1	zimní
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
v	v	k7c6	v
Cortina	Cortina	k1gFnSc1	Cortina
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ampezzo	Ampezza	k1gFnSc5	Ampezza
se	se	k3xPyFc4	se
již	již	k9	již
SSSR	SSSR	kA	SSSR
poprvé	poprvé	k6eAd1	poprvé
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
získal	získat	k5eAaPmAgInS	získat
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
úspěchů	úspěch	k1gInPc2	úspěch
a	a	k8xC	a
ve	v	k7c6	v
statistikách	statistika	k1gFnPc6	statistika
medailí	medaile	k1gFnPc2	medaile
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
druhou	druhý	k4xOgFnSc7	druhý
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
a	a	k8xC	a
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
v	v	k7c6	v
Cortině	Cortina	k1gFnSc6	Cortina
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ampezzo	Ampezza	k1gFnSc5	Ampezza
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
letní	letní	k2eAgFnSc2d1	letní
olympiády	olympiáda	k1gFnSc2	olympiáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
a	a	k8xC	a
zimní	zimní	k2eAgFnPc1d1	zimní
olympiády	olympiáda	k1gFnPc1	olympiáda
v	v	k7c6	v
Calgary	Calgary	k1gNnSc6	Calgary
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
získal	získat	k5eAaPmAgInS	získat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
dohromady	dohromady	k6eAd1	dohromady
395	[number]	k4	395
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
78	[number]	k4	78
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
hostila	hostit	k5eAaImAgFnS	hostit
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
NSR	NSR	kA	NSR
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
sovětské	sovětský	k2eAgFnSc3d1	sovětská
invazi	invaze	k1gFnSc3	invaze
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
bojkotovány	bojkotován	k2eAgFnPc1d1	bojkotována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
zemí	zem	k1gFnPc2	zem
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
bojkotovala	bojkotovat	k5eAaImAgFnS	bojkotovat
letní	letní	k2eAgFnSc1d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
SSSR	SSSR	kA	SSSR
byl	být	k5eAaImAgInS	být
také	také	k9	také
široce	široko	k6eAd1	široko
využíván	využívat	k5eAaImNgInS	využívat
doping	doping	k1gInSc1	doping
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
sovětská	sovětský	k2eAgFnSc1d1	sovětská
politika	politika	k1gFnSc1	politika
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
vždy	vždy	k6eAd1	vždy
přikládala	přikládat	k5eAaImAgFnS	přikládat
velkou	velký	k2eAgFnSc4d1	velká
důležitost	důležitost	k1gFnSc4	důležitost
akcím	akce	k1gFnPc3	akce
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
lidské	lidský	k2eAgFnPc1d1	lidská
bytosti	bytost	k1gFnPc1	bytost
aktivně	aktivně	k6eAd1	aktivně
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
přírodu	příroda	k1gFnSc4	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Leninův	Leninův	k2eAgInSc1d1	Leninův
citát	citát	k1gInSc1	citát
"	"	kIx"	"
<g/>
Komunismus	komunismus	k1gInSc1	komunismus
je	být	k5eAaImIp3nS	být
sovětská	sovětský	k2eAgFnSc1d1	sovětská
moc	moc	k1gFnSc1	moc
a	a	k8xC	a
elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
země	zem	k1gFnSc2	zem
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
zaměření	zaměření	k1gNnSc1	zaměření
na	na	k7c4	na
modernizaci	modernizace	k1gFnSc4	modernizace
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgInSc2	první
pětiletého	pětiletý	k2eAgInSc2d1	pětiletý
plánu	plán	k1gInSc2	plán
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
Stalin	Stalin	k1gMnSc1	Stalin
přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
k	k	k7c3	k
industrializaci	industrializace	k1gFnSc3	industrializace
země	zem	k1gFnSc2	zem
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
jako	jako	k8xS	jako
ochrana	ochrana	k1gFnSc1	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
přírody	příroda	k1gFnSc2	příroda
byly	být	k5eAaImAgFnP	být
zcela	zcela	k6eAd1	zcela
ignorovány	ignorovat	k5eAaImNgInP	ignorovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
za	za	k7c4	za
vytvoření	vytvoření	k1gNnSc4	vytvoření
moderní	moderní	k2eAgFnSc2d1	moderní
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Stalinově	Stalinův	k2eAgFnSc6d1	Stalinova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
zaměřilo	zaměřit	k5eAaPmAgNnS	zaměřit
na	na	k7c4	na
problematiku	problematika	k1gFnSc4	problematika
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
základní	základní	k2eAgNnSc1d1	základní
vnímání	vnímání	k1gNnSc1	vnímání
hodnoty	hodnota	k1gFnSc2	hodnota
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sovětské	sovětský	k2eAgInPc1d1	sovětský
sdělovací	sdělovací	k2eAgInPc1d1	sdělovací
prostředky	prostředek	k1gInPc1	prostředek
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
věnovaly	věnovat	k5eAaPmAgFnP	věnovat
obrovské	obrovský	k2eAgFnSc3d1	obrovská
rozloze	rozloha	k1gFnSc3	rozloha
země	zem	k1gFnSc2	zem
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
nezničitelným	zničitelný	k2eNgInPc3d1	nezničitelný
přírodním	přírodní	k2eAgInPc3d1	přírodní
zdrojům	zdroj	k1gInPc3	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kontaminace	kontaminace	k1gFnSc2	kontaminace
a	a	k8xC	a
drancování	drancování	k1gNnSc2	drancování
přírody	příroda	k1gFnSc2	příroda
nebyly	být	k5eNaImAgFnP	být
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
stát	stát	k1gInSc1	stát
také	také	k9	také
pevně	pevně	k6eAd1	pevně
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vědecký	vědecký	k2eAgInSc1d1	vědecký
a	a	k8xC	a
technologický	technologický	k2eAgInSc1d1	technologický
pokrok	pokrok	k1gInSc1	pokrok
by	by	kYmCp3nS	by
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
všechny	všechen	k3xTgInPc4	všechen
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
ideologie	ideologie	k1gFnSc1	ideologie
říkala	říkat	k5eAaImAgFnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
problémy	problém	k1gInPc1	problém
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
mohly	moct	k5eAaImAgFnP	moct
snadno	snadno	k6eAd1	snadno
překonat	překonat	k5eAaPmF	překonat
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kapitalistických	kapitalistický	k2eAgFnPc2d1	kapitalistická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nemohly	moct	k5eNaImAgFnP	moct
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgInPc1d1	sovětský
úřady	úřad	k1gInPc1	úřad
měly	mít	k5eAaImAgFnP	mít
téměř	téměř	k6eAd1	téměř
neochvějné	neochvějný	k2eAgNnSc4d1	neochvějné
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
přírodu	příroda	k1gFnSc4	příroda
překonat	překonat	k5eAaPmF	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
orgány	orgán	k1gInPc4	orgán
v	v	k7c6	v
80.	[number]	k4	80.
letech	léto	k1gNnPc6	léto
musely	muset	k5eAaImAgFnP	muset
připustit	připustit	k5eAaPmF	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
existují	existovat	k5eAaImIp3nP	existovat
ekologické	ekologický	k2eAgInPc4d1	ekologický
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
vysvětlily	vysvětlit	k5eAaPmAgInP	vysvětlit
problémy	problém	k1gInPc1	problém
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
socialismus	socialismus	k1gInSc1	socialismus
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
plně	plně	k6eAd1	plně
rozvinut	rozvinut	k2eAgInSc1d1	rozvinut
<g/>
;	;	kIx,	;
znečištění	znečištění	k1gNnSc1	znečištění
v	v	k7c6	v
socialistické	socialistický	k2eAgFnSc6d1	socialistická
společnosti	společnost	k1gFnSc6	společnost
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
dočasnou	dočasný	k2eAgFnSc7d1	dočasná
anomálií	anomálie	k1gFnSc7	anomálie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
socialismus	socialismus	k1gInSc1	socialismus
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černobylská	černobylský	k2eAgFnSc1d1	Černobylská
havárie	havárie	k1gFnSc1	havárie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
svět	svět	k1gInSc4	svět
šokovala	šokovat	k5eAaBmAgFnS	šokovat
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
velkou	velký	k2eAgFnSc4d1	velká
nehodu	nehoda	k1gFnSc4	nehoda
v	v	k7c6	v
civilní	civilní	k2eAgFnSc6d1	civilní
jaderné	jaderný	k2eAgFnSc6d1	jaderná
elektrárně	elektrárna	k1gFnSc6	elektrárna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
světe	svět	k1gInSc5	svět
neměla	mít	k5eNaImAgFnS	mít
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
uvolněno	uvolněn	k2eAgNnSc4d1	uvolněno
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
izotopů	izotop	k1gInPc2	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnPc1d1	radioaktivní
dávky	dávka	k1gFnPc1	dávka
se	se	k3xPyFc4	se
rozptýlily	rozptýlit	k5eAaPmAgFnP	rozptýlit
relativně	relativně	k6eAd1	relativně
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdravotním	zdravotní	k2eAgInSc7d1	zdravotní
problémem	problém	k1gInSc7	problém
po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
bylo	být	k5eAaImAgNnS	být
4000	[number]	k4	4000
nových	nový	k2eAgInPc2d1	nový
případů	případ	k1gInPc2	případ
rakoviny	rakovina	k1gFnSc2	rakovina
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
relativně	relativně	k6eAd1	relativně
nízkému	nízký	k2eAgInSc3d1	nízký
počtu	počet	k1gInSc3	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
(	(	kIx(	(
<g/>
údaje	údaj	k1gInSc2	údaj
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
dlouhodobé	dlouhodobý	k2eAgInPc1d1	dlouhodobý
účinky	účinek	k1gInPc1	účinek
nehody	nehoda	k1gFnSc2	nehoda
jsou	být	k5eAaImIp3nP	být
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnSc4d1	další
velkou	velký	k2eAgFnSc4d1	velká
havárii	havárie	k1gFnSc4	havárie
patří	patřit	k5eAaImIp3nS	patřit
kyštymská	kyštymský	k2eAgFnSc1d1	kyštymský
katastrofa	katastrofa	k1gFnSc1	katastrofa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
životním	životní	k2eAgNnSc7d1	životní
prostředím	prostředí	k1gNnSc7	prostředí
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
jaké	jaký	k3yIgInPc1	jaký
sovětské	sovětský	k2eAgInPc1d1	sovětský
úřady	úřad	k1gInPc1	úřad
připouštěly	připouštět	k5eAaImAgInP	připouštět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
místa	místo	k1gNnPc4	místo
s	s	k7c7	s
jasnými	jasný	k2eAgInPc7d1	jasný
problémy	problém	k1gInPc7	problém
patřil	patřit	k5eAaImAgInS	patřit
poloostrov	poloostrov	k1gInSc1	poloostrov
Kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
měst	město	k1gNnPc2	město
Mončegorsk	Mončegorsk	k1gInSc1	Mončegorsk
a	a	k8xC	a
Norilsk	Norilsk	k1gInSc1	Norilsk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
např.	např.	kA	např.
nikl	nikl	k1gInSc4	nikl
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
lesy	les	k1gInPc1	les
zabity	zabít	k5eAaPmNgInP	zabít
kontaminací	kontaminace	k1gFnSc7	kontaminace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
severské	severský	k2eAgFnPc1d1	severská
i	i	k8xC	i
jiné	jiný	k2eAgFnPc1d1	jiná
části	část	k1gFnPc1	část
Ruska	Rusko	k1gNnSc2	Rusko
byly	být	k5eAaImAgFnP	být
ovlivněny	ovlivněn	k2eAgFnPc1d1	ovlivněna
emisemi	emise	k1gFnPc7	emise
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
také	také	k9	také
zajímali	zajímat	k5eAaImAgMnP	zajímat
o	o	k7c4	o
radioaktivní	radioaktivní	k2eAgNnPc4d1	radioaktivní
nebezpečí	nebezpečí	k1gNnPc4	nebezpečí
z	z	k7c2	z
jaderných	jaderný	k2eAgNnPc2d1	jaderné
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
vyřazených	vyřazený	k2eAgFnPc2d1	vyřazená
jaderných	jaderný	k2eAgFnPc2d1	jaderná
ponorek	ponorka	k1gFnPc2	ponorka
a	a	k8xC	a
zpracování	zpracování	k1gNnSc4	zpracování
jaderného	jaderný	k2eAgInSc2d1	jaderný
odpadu	odpad	k1gInSc2	odpad
či	či	k8xC	či
vyhořelého	vyhořelý	k2eAgNnSc2d1	vyhořelé
jaderného	jaderný	k2eAgNnSc2d1	jaderné
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
svážel	svážet	k5eAaImAgInS	svážet
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
materiál	materiál	k1gInSc1	materiál
do	do	k7c2	do
Barentsova	Barentsův	k2eAgNnSc2d1	Barentsovo
a	a	k8xC	a
Karského	karský	k2eAgNnSc2d1	Karské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
později	pozdě	k6eAd2	pozdě
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
ruský	ruský	k2eAgInSc4d1	ruský
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Havárie	havárie	k1gFnSc1	havárie
ponorky	ponorka	k1gFnSc2	ponorka
K-141	K-141	k1gFnSc1	K-141
Kursk	Kursk	k1gInSc1	Kursk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
na	na	k7c6	na
západě	západ	k1gInSc6	západ
dále	daleko	k6eAd2	daleko
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
obavám	obava	k1gFnPc3	obava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nehodám	nehoda	k1gFnPc3	nehoda
ponorek	ponorka	k1gFnPc2	ponorka
K-19	K-19	k1gFnPc2	K-19
<g/>
,	,	kIx,	,
K-8	K-8	k1gFnPc2	K-8
nebo	nebo	k8xC	nebo
K-129	K-129	k1gFnPc2	K-129
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAHENSKÝ	BAHENSKÝ	kA	BAHENSKÝ
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Národnostní	národnostní	k2eAgFnSc1d1	národnostní
politika	politika	k1gFnSc1	politika
na	na	k7c6	na
teritoriu	teritorium	k1gNnSc6	teritorium
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Etnologický	etnologický	k2eAgInSc1d1	etnologický
ústav	ústav	k1gInSc1	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2010.	[number]	k4	2010.
219	[number]	k4	219
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-87112-39-7	[number]	k4	978-80-87112-39-7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CROZIER	CROZIER	kA	CROZIER
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
<g/>
.	.	kIx.	.
</s>
<s>
Vzestup	vzestup	k1gInSc1	vzestup
a	a	k8xC	a
pád	pád	k1gInSc1	pád
Sovětské	sovětský	k2eAgFnSc2d1	sovětská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2004.	[number]	k4	2004.
679	[number]	k4	679
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7341-349-3	[number]	k4	80-7341-349-3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FLORES	FLORES	kA	FLORES
<g/>
,	,	kIx,	,
Marcello	Marcello	k1gNnSc1	Marcello
<g/>
.	.	kIx.	.
</s>
<s>
Komunismus	komunismus	k1gInSc1	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
2008.	[number]	k4	2008.
185	[number]	k4	185
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7309-388-4	[number]	k4	978-80-7309-388-4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KOTYK	KOTYK	kA	KOTYK
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
a	a	k8xC	a
rozpad	rozpad	k1gInSc1	rozpad
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Oeconomica	Oeconomica	k1gMnSc1	Oeconomica
<g/>
,	,	kIx,	,
2009.	[number]	k4	2009.
277	[number]	k4	277
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-245-1512-0	[number]	k4	978-80-245-1512-0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NETOPIL	topit	k5eNaImAgMnS	topit
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
<g/>
;	;	kIx,	;
SKOKAN	Skokan	k1gMnSc1	Skokan
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Geografie	geografie	k1gFnSc1	geografie
SSSR	SSSR	kA	SSSR
:	:	kIx,	:
celostátní	celostátní	k2eAgFnSc1d1	celostátní
vysokoškolská	vysokoškolský	k2eAgFnSc1d1	vysokoškolská
učebnice	učebnice	k1gFnSc1	učebnice
pro	pro	k7c4	pro
stud	stud	k1gInSc4	stud
<g/>
.	.	kIx.	.
pedag	pedag	k1gInSc4	pedag
<g/>
.	.	kIx.	.
a	a	k8xC	a
přírodověd	přírodověda	k1gFnPc2	přírodověda
<g/>
.	.	kIx.	.
fak	fak	k?	fak
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
stud	stud	k1gInSc1	stud
<g/>
.	.	kIx.	.
oboru	obor	k1gInSc2	obor
76-12-8	[number]	k4	76-12-8
Učitelství	učitelství	k1gNnSc2	učitelství
všeobecně	všeobecně	k6eAd1	všeobecně
vzdělávacích	vzdělávací	k2eAgInPc2d1	vzdělávací
předmětů	předmět	k1gInPc2	předmět
-	-	kIx~	-
geografie	geografie	k1gFnSc2	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1989.	[number]	k4	1989.
400	[number]	k4	400
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-04-22269-2	[number]	k4	80-04-22269-2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
REIMAN	REIMAN	kA	REIMAN
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
velmoci	velmoc	k1gFnSc2	velmoc
:	:	kIx,	:
dějiny	dějiny	k1gFnPc4	dějiny
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
1917-1945	[number]	k4	1917-1945
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2013.	[number]	k4	2013.
583	[number]	k4	583
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-246-2266-8	[number]	k4	978-80-246-2266-8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SERVICE	SERVICE	kA	SERVICE
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
history	histor	k1gInPc1	histor
of	of	k?	of
twentieth-century	twentiethentura	k1gFnSc2	twentieth-centura
Russia	Russium	k1gNnSc2	Russium
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
(	(	kIx(	(
<g/>
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1998.	[number]	k4	1998.
653	[number]	k4	653
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
0-674-40348-7	[number]	k4	0-674-40348-7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ŠVANKMAJER	ŠVANKMAJER	kA	ŠVANKMAJER
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1999.	[number]	k4	1999.
558	[number]	k4	558
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7106-183-2	[number]	k4	80-7106-183-2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VEBER	VEBER	kA	VEBER
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Stalinovo	Stalinův	k2eAgNnSc1d1	Stalinovo
impérium	impérium	k1gNnSc1	impérium
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
1924-1953	[number]	k4	1924-1953
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2003.	[number]	k4	2003.
167	[number]	k4	167
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7254-391-1	[number]	k4	80-7254-391-1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VYDRA	Vydra	k1gMnSc1	Vydra
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2017.	[number]	k4	2017.
499	[number]	k4	499
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7422-324-2	[number]	k4	978-80-7422-324-2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Co	co	k3yQnSc1	co
hájí	hájit	k5eAaImIp3nS	hájit
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Tiskové	tiskový	k2eAgInPc1d1	tiskový
podniky	podnik	k1gInPc1	podnik
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
1945.	[number]	k4	1945.
30	[number]	k4	30
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
(	(	kIx(	(
<g/>
základní	základní	k2eAgInSc1d1	základní
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
Svazu	svaz	k1gInSc2	svaz
sovětských	sovětský	k2eAgFnPc2d1	sovětská
socialistických	socialistický	k2eAgFnPc2d1	socialistická
republik	republika	k1gFnPc2	republika
<g/>
:	:	kIx,	:
se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
a	a	k8xC	a
doplňky	doplněk	k1gInPc7	doplněk
schválenými	schválený	k2eAgInPc7d1	schválený
I.	I.	kA	I.
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
VII	VII	kA	VII
<g/>
.	.	kIx.	.
a	a	k8xC	a
X.	X.	kA	X.
zasedáním	zasedání	k1gNnSc7	zasedání
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
1945.	[number]	k4	1945.
30	[number]	k4	30
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZUBOV	ZUBOV	kA	ZUBOV
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Borisovič	Borisovič	k1gMnSc1	Borisovič
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Ruska	Ruska	k1gFnSc1	Ruska
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2014-2015	[number]	k4	2014-2015
<g/>
.	.	kIx.	.
2	[number]	k4	2
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-257-0921-4	[number]	k4	978-80-257-0921-4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc7	téma
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
