<s>
Cirka	Cirka	k6eAd1
</s>
<s>
Cirka	Cirka	k6eAd1
latinsky	latinsky	k6eAd1
znamená	znamenat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
i	i	k9
latinský	latinský	k2eAgInSc4d1
tvar	tvar	k1gInSc4
circa	circa	k6eAd1
nebo	nebo	k8xC
zkratky	zkratka	k1gFnSc2
cca	cca	kA
<g/>
,	,	kIx,
ca	ca	kA
<g/>
,	,	kIx,
c	c	kA
nebo	nebo	k8xC
značka	značka	k1gFnSc1
~	~	kIx~
(	(	kIx(
<g/>
vlnovka	vlnovka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
případech	případ	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
hodnota	hodnota	k1gFnSc1
známá	známý	k2eAgFnSc1d1
pouze	pouze	k6eAd1
zhruba	zhruba	k6eAd1
nebo	nebo	k8xC
přibližně	přibližně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
si	se	k3xPyFc3
nejsme	být	k5eNaImIp1nP
jisti	jist	k2eAgMnPc1d1
o	o	k7c6
přesném	přesný	k2eAgInSc6d1
čase	čas	k1gInSc6
nebo	nebo	k8xC
počtu	počet	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
jsme	být	k5eAaImIp1nP
si	se	k3xPyFc3
jisti	jist	k2eAgMnPc1d1
blízkou	blízký	k2eAgFnSc7d1
přibližností	přibližnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
použití	použití	k1gNnSc2
</s>
<s>
V	v	k7c6
článku	článek	k1gInSc6
Vilém	Vilém	k1gMnSc1
I.	I.	kA
Dobyvatel	dobyvatel	k1gMnSc1
je	být	k5eAaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
„	„	k?
<g/>
cca	cca	kA
1028	#num#	k4
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1087	#num#	k4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
rok	rok	k1gInSc1
1028	#num#	k4
je	být	k5eAaImIp3nS
uveden	uvést	k5eAaPmNgInS
pouze	pouze	k6eAd1
přibližně	přibližně	k6eAd1
a	a	k8xC
naopak	naopak	k6eAd1
že	že	k8xS
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1087	#num#	k4
je	být	k5eAaImIp3nS
přesné	přesný	k2eAgNnSc4d1
datum	datum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
