<s>
Ikarus	Ikarus	k1gMnSc1
417	#num#	k4
</s>
<s>
Ikarus	Ikarus	k1gMnSc1
417	#num#	k4
</s>
<s>
Ikarus	Ikarus	k1gMnSc1
417	#num#	k4
v	v	k7c6
Táboře	Tábor	k1gInSc6
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
417	#num#	k4
</s>
<s>
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Ikarus	Ikarus	k1gMnSc1
</s>
<s>
Vyráběn	vyrábět	k5eAaImNgMnS
v	v	k7c6
letech	léto	k1gNnPc6
</s>
<s>
1993	#num#	k4
-	-	kIx~
1999	#num#	k4
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
17530	#num#	k4
mm	mm	kA
</s>
<s>
Šířka	šířka	k1gFnSc1
</s>
<s>
2500	#num#	k4
mm	mm	kA
</s>
<s>
Výška	výška	k1gFnSc1
</s>
<s>
2805	#num#	k4
mm	mm	kA
</s>
<s>
Pohotov	Pohotov	k1gInSc1
<g/>
.	.	kIx.
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
14860	#num#	k4
kg	kg	kA
</s>
<s>
Obsaditelnost	obsaditelnost	k1gFnSc1
(	(	kIx(
<g/>
sezení	sezení	k1gNnSc1
<g/>
:	:	kIx,
<g/>
stání	stání	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
46	#num#	k4
<g/>
:	:	kIx,
<g/>
120	#num#	k4
</s>
<s>
Pohonné	pohonný	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
Motory	motor	k1gInPc1
</s>
<s>
Motor	motor	k1gInSc1
</s>
<s>
MAN	Man	k1gMnSc1
D	D	kA
0826	#num#	k4
LUH	luh	k1gInSc1
10	#num#	k4
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
198	#num#	k4
kW	kW	kA
</s>
<s>
Převodovky	převodovka	k1gFnPc1
</s>
<s>
Převodovka	převodovka	k1gFnSc1
</s>
<s>
ZF	ZF	kA
4	#num#	k4
HP	HP	kA
500	#num#	k4
</s>
<s>
Druh	druh	k1gInSc1
</s>
<s>
automatická	automatický	k2eAgFnSc1d1
</s>
<s>
Počet	počet	k1gInSc1
přev	převa	k1gFnPc2
<g/>
.	.	kIx.
stupňů	stupeň	k1gInPc2
</s>
<s>
4	#num#	k4
</s>
<s>
Ikarus	Ikarus	k1gMnSc1
417	#num#	k4
je	být	k5eAaImIp3nS
model	model	k1gInSc4
maďarského	maďarský	k2eAgInSc2d1
městského	městský	k2eAgInSc2d1
kloubového	kloubový	k2eAgInSc2d1
plně	plně	k6eAd1
nízkopodlažního	nízkopodlažní	k2eAgInSc2d1
autobusu	autobus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
vyráběn	vyrábět	k5eAaImNgInS
společností	společnost	k1gFnSc7
Ikarus	Ikarus	k1gMnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1993	#num#	k4
až	až	k9
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Ikarus	Ikarus	k1gMnSc1
417	#num#	k4
je	být	k5eAaImIp3nS
kloubový	kloubový	k2eAgInSc1d1
nízkopodlažní	nízkopodlažní	k2eAgInSc1d1
autobus	autobus	k1gInSc1
<g/>
,	,	kIx,
vycházející	vycházející	k2eAgFnSc1d1
z	z	k7c2
řady	řada	k1gFnSc2
400	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Model	model	k1gInSc1
417	#num#	k4
je	být	k5eAaImIp3nS
třínápravový	třínápravový	k2eAgInSc4d1
autobus	autobus	k1gInSc4
se	s	k7c7
samonosnou	samonosný	k2eAgFnSc7d1
karoserií	karoserie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
jsou	být	k5eAaImIp3nP
navzájem	navzájem	k6eAd1
spojeny	spojit	k5eAaPmNgInP
kloubem	kloub	k1gInSc7
a	a	k8xC
krycím	krycí	k2eAgInSc7d1
měchem	měch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadní	zadní	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
je	být	k5eAaImIp3nS
hnací	hnací	k2eAgFnSc1d1
<g/>
,	,	kIx,
motor	motor	k1gInSc1
a	a	k8xC
převodovka	převodovka	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
motorové	motorový	k2eAgFnSc6d1
věži	věž	k1gFnSc6
v	v	k7c6
zadní	zadní	k2eAgFnSc6d1
části	část	k1gFnSc6
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kabina	kabina	k1gFnSc1
řidiče	řidič	k1gMnPc4
je	být	k5eAaImIp3nS
uzavřená	uzavřený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
a	a	k8xC
provoz	provoz	k1gInSc1
</s>
<s>
První	první	k4xOgInSc1
prototyp	prototyp	k1gInSc1
Ikarusu	Ikarus	k1gInSc2
417	#num#	k4
byl	být	k5eAaImAgInS
vyroben	vyrobit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
a	a	k8xC
následně	následně	k6eAd1
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
sériová	sériový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
trvala	trvat	k5eAaImAgFnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
prošla	projít	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
400	#num#	k4
modernizací	modernizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
33	#num#	k4
autobusů	autobus	k1gInPc2
Ikarus	Ikarus	k1gMnSc1
417	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ikarusy	Ikarus	k1gInPc1
417	#num#	k4
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
málo	málo	k6eAd1
rozšířené	rozšířený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
jich	on	k3xPp3gFnPc2
najdeme	najít	k5eAaPmIp1nP
v	v	k7c6
německém	německý	k2eAgMnSc6d1
Wuppertalu	Wuppertal	k1gMnSc6
(	(	kIx(
<g/>
17	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poté	poté	k6eAd1
několik	několik	k4yIc4
kousků	kousek	k1gInPc2
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
Maďarsku	Maďarsko	k1gNnSc6
a	a	k8xC
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
provozu	provoz	k1gInSc6
pouze	pouze	k6eAd1
jeden	jeden	k4xCgMnSc1
Ikarus	Ikarus	k1gMnSc1
417	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
Táboře	Tábor	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
sloužil	sloužit	k5eAaImAgInS
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
vyřazen	vyřadit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Táborský	Táborský	k1gMnSc1
Ikarus	Ikarus	k1gMnSc1
417	#num#	k4
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
nízkopodlažním	nízkopodlažní	k2eAgMnSc7d1
kloubovým	kloubový	k2eAgInSc7d1
autobusem	autobus	k1gInSc7
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgInPc1d1
vozy	vůz	k1gInPc1
</s>
<s>
soukromá	soukromý	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
(	(	kIx(
<g/>
táborský	táborský	k2eAgInSc1d1
vůz	vůz	k1gInSc1
ev.	ev.	k?
č.	č.	k?
99	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
soukromá	soukromý	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
(	(	kIx(
<g/>
vratislavský	vratislavský	k2eAgInSc1d1
vůz	vůz	k1gInSc1
vůz	vůz	k1gInSc1
ev.	ev.	k?
č.	č.	k?
5000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Stránka	stránka	k1gFnSc1
o	o	k7c6
autobusech	autobus	k1gInPc6
Ikarus	Ikarus	k1gMnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
Stránka	stránka	k1gFnSc1
o	o	k7c6
autobusech	autobus	k1gInPc6
Ikarus	Ikarus	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
