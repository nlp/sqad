<s>
Gilmorova	Gilmorův	k2eAgNnPc4d1	Gilmorův
děvčata	děvče	k1gNnPc4	děvče
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Gilmore	Gilmor	k1gInSc5	Gilmor
Girls	girl	k1gFnPc3	girl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
komediálně-dramatický	komediálněramatický	k2eAgInSc1d1	komediálně-dramatický
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
autorkou	autorka	k1gFnSc7	autorka
je	být	k5eAaImIp3nS	být
Amy	Amy	k1gFnSc2	Amy
Sherman-Palladino	Sherman-Palladin	k2eAgNnSc1d1	Sherman-Palladin
<g/>
.	.	kIx.	.
</s>
<s>
Premiérově	premiérově	k6eAd1	premiérově
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
v	v	k7c6	v
letech	let	k1gInPc6	let
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
The	The	k1gFnSc2	The
WB	WB	kA	WB
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
na	na	k7c4	na
The	The	k1gFnSc4	The
CW	CW	kA	CW
(	(	kIx(	(
<g/>
od	od	k7c2	od
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
153	[number]	k4	153
dílů	díl	k1gInPc2	díl
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
řadách	řada	k1gFnPc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
seriál	seriál	k1gInSc4	seriál
navazuje	navazovat	k5eAaImIp3nS	navazovat
minisérie	minisérie	k1gFnSc1	minisérie
Gilmore	Gilmor	k1gInSc5	Gilmor
Girls	girl	k1gFnPc4	girl
<g/>
:	:	kIx,	:
A	a	k9	a
Year	Year	k1gInSc1	Year
in	in	k?	in
the	the	k?	the
Life	Lif	k1gInSc2	Lif
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgNnSc6d1	fiktivní
městečku	městečko	k1gNnSc6	městečko
Stars	Starsa	k1gFnPc2	Starsa
Hollow	Hollow	k1gFnSc2	Hollow
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
svobodné	svobodný	k2eAgFnSc6d1	svobodná
matce	matka	k1gFnSc6	matka
Lorelai	Lorela	k1gFnSc2	Lorela
Gilmorové	Gilmorový	k2eAgFnSc2d1	Gilmorový
(	(	kIx(	(
<g/>
Lauren	Laurna	k1gFnPc2	Laurna
Graham	graham	k1gInSc1	graham
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
dospívající	dospívající	k2eAgFnSc3d1	dospívající
dceři	dcera	k1gFnSc3	dcera
Rory	Rora	k1gFnSc2	Rora
(	(	kIx(	(
<g/>
Alexis	Alexis	k1gInSc1	Alexis
Bledel	Bledlo	k1gNnPc2	Bledlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
především	především	k9	především
svými	svůj	k3xOyFgInPc7	svůj
rychlými	rychlý	k2eAgInPc7d1	rychlý
a	a	k8xC	a
vtipnými	vtipný	k2eAgInPc7d1	vtipný
dialogy	dialog	k1gInPc7	dialog
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
prošpikovány	prošpikovat	k5eAaPmNgInP	prošpikovat
narážkami	narážka	k1gFnPc7	narážka
na	na	k7c4	na
pop	pop	k1gInSc4	pop
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
seriálu	seriál	k1gInSc2	seriál
je	být	k5eAaImIp3nS	být
svobodná	svobodný	k2eAgFnSc1d1	svobodná
matka	matka	k1gFnSc1	matka
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
rodiny	rodina	k1gFnSc2	rodina
Lorelai	Lorela	k1gFnSc2	Lorela
Gilmorová	Gilmorová	k1gFnSc1	Gilmorová
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
dospívající	dospívající	k2eAgFnSc1d1	dospívající
dcera	dcera	k1gFnSc1	dcera
Lorelai	Lorela	k1gFnSc2	Lorela
Leigh	Leigh	k1gInSc4	Leigh
Gilmorová	Gilmorový	k2eAgFnSc1d1	Gilmorový
alias	alias	k9	alias
Rory	Ror	k1gMnPc4	Ror
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
diváka	divák	k1gMnSc4	divák
s	s	k7c7	s
celým	celý	k2eAgNnSc7d1	celé
pozadím	pozadí	k1gNnSc7	pozadí
příběhu	příběh	k1gInSc2	příběh
<g/>
:	:	kIx,	:
Lorelai	Lorela	k1gFnSc2	Lorela
v	v	k7c6	v
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
<g/>
,	,	kIx,	,
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
si	se	k3xPyFc3	se
navzdory	navzdory	k6eAd1	navzdory
přání	přání	k1gNnSc4	přání
rodičů	rodič	k1gMnPc2	rodič
vzít	vzít	k5eAaPmF	vzít
otce	otka	k1gFnSc3	otka
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
se	se	k3xPyFc4	se
od	od	k7c2	od
rodičů	rodič	k1gMnPc2	rodič
do	do	k7c2	do
městečka	městečko	k1gNnSc2	městečko
Stars	Stars	k1gInSc1	Stars
Hollow	Hollow	k1gFnSc4	Hollow
v	v	k7c6	v
Connecticutu	Connecticut	k1gInSc6	Connecticut
<g/>
,	,	kIx,	,
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
udržovala	udržovat	k5eAaImAgFnS	udržovat
jen	jen	k9	jen
sporadický	sporadický	k2eAgInSc4d1	sporadický
kontakt	kontakt	k1gInSc4	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Rory	Rory	k1gInPc1	Rory
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
seriálu	seriál	k1gInSc2	seriál
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
na	na	k7c4	na
prestižní	prestižní	k2eAgFnSc4d1	prestižní
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
Chilton	Chilton	k1gInSc1	Chilton
<g/>
.	.	kIx.	.
</s>
<s>
Školné	školný	k2eAgNnSc1d1	školné
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
příliš	příliš	k6eAd1	příliš
vysoké	vysoký	k2eAgNnSc1d1	vysoké
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
Lorelai	Lorela	k1gFnSc3	Lorela
nucena	nucen	k2eAgFnSc1d1	nucena
uchýlit	uchýlit	k5eAaPmF	uchýlit
se	se	k3xPyFc4	se
k	k	k7c3	k
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
nejzazší	zadní	k2eAgFnPc1d3	nejzazší
možnosti	možnost	k1gFnPc1	možnost
–	–	k?	–
požádat	požádat	k5eAaPmF	požádat
rodiče	rodič	k1gMnSc4	rodič
o	o	k7c4	o
půjčku	půjčka	k1gFnSc4	půjčka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Emily	Emil	k1gMnPc4	Emil
Gilmorová	Gilmorová	k1gFnSc1	Gilmorová
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
ale	ale	k9	ale
vymíní	vymínit	k5eAaPmIp3nS	vymínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
obě	dva	k4xCgFnPc1	dva
každý	každý	k3xTgInSc4	každý
pátek	pátek	k1gInSc4	pátek
docházet	docházet	k5eAaImF	docházet
na	na	k7c4	na
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
rodinné	rodinný	k2eAgFnPc4d1	rodinná
večeře	večeře	k1gFnPc4	večeře
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
Emily	Emil	k1gMnPc7	Emil
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
stávají	stávat	k5eAaImIp3nP	stávat
znovu	znovu	k6eAd1	znovu
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
života	život	k1gInSc2	život
obou	dva	k4xCgNnPc2	dva
děvčat	děvče	k1gNnPc2	děvče
<g/>
.	.	kIx.	.
</s>
<s>
Lorelain	Lorelain	k2eAgInSc4d1	Lorelain
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
rodičům	rodič	k1gMnPc3	rodič
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
napjatý	napjatý	k2eAgInSc1d1	napjatý
a	a	k8xC	a
ostře	ostro	k6eAd1	ostro
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
s	s	k7c7	s
kamarádským	kamarádský	k2eAgInSc7d1	kamarádský
vztahem	vztah	k1gInSc7	vztah
mezi	mezi	k7c7	mezi
Lorelai	Lorela	k1gMnPc7	Lorela
a	a	k8xC	a
Rory	Ror	k1gMnPc7	Ror
<g/>
.	.	kIx.	.
</s>
<s>
Ústředními	ústřední	k2eAgInPc7d1	ústřední
motivy	motiv	k1gInPc7	motiv
seriálu	seriál	k1gInSc2	seriál
jsou	být	k5eAaImIp3nP	být
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
přátelství	přátelství	k1gNnSc1	přátelství
a	a	k8xC	a
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
generační	generační	k2eAgInPc4d1	generační
a	a	k8xC	a
třídní	třídní	k2eAgInPc4d1	třídní
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
se	se	k3xPyFc4	se
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
obě	dva	k4xCgNnPc1	dva
Gilmorova	Gilmorův	k2eAgNnPc1d1	Gilmorův
děvčata	děvče	k1gNnPc1	děvče
nakonec	nakonec	k6eAd1	nakonec
dostanou	dostat	k5eAaPmIp3nP	dostat
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Lorelai	Lorelai	k6eAd1	Lorelai
se	se	k3xPyFc4	se
z	z	k7c2	z
pokojské	pokojská	k1gFnSc2	pokojská
s	s	k7c7	s
nálepkou	nálepka	k1gFnSc7	nálepka
svobodné	svobodný	k2eAgFnSc2d1	svobodná
matky	matka	k1gFnSc2	matka
vypracuje	vypracovat	k5eAaPmIp3nS	vypracovat
na	na	k7c4	na
majitelku	majitelka	k1gFnSc4	majitelka
vlastního	vlastní	k2eAgInSc2d1	vlastní
hotelu	hotel	k1gInSc2	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Rory	Rora	k1gFnPc4	Rora
exceluje	excelovat	k5eAaImIp3nS	excelovat
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
prestižní	prestižní	k2eAgFnSc6d1	prestižní
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
Chilton	Chilton	k1gInSc4	Chilton
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
pak	pak	k6eAd1	pak
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Yale	Yale	k1gFnSc6	Yale
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dále	daleko	k6eAd2	daleko
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
snu	sen	k1gInSc6	sen
stát	stát	k5eAaImF	stát
se	s	k7c7	s
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
dopisovatelkou	dopisovatelka	k1gFnSc7	dopisovatelka
některých	některý	k3yIgFnPc2	některý
proslulých	proslulý	k2eAgFnPc2d1	proslulá
amerických	americký	k2eAgFnPc2d1	americká
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
společenskými	společenský	k2eAgFnPc7d1	společenská
vrstvami	vrstva	k1gFnPc7	vrstva
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
nastíněn	nastínit	k5eAaPmNgInS	nastínit
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
složitého	složitý	k2eAgInSc2d1	složitý
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c4	mezi
Lorelai	Lorelae	k1gFnSc4	Lorelae
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
privilegovaný	privilegovaný	k2eAgInSc4d1	privilegovaný
život	život	k1gInSc4	život
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejími	její	k3xOp3gMnPc7	její
rodiči	rodič	k1gMnPc7	rodič
z	z	k7c2	z
vyšší	vysoký	k2eAgFnSc2d2	vyšší
společenské	společenský	k2eAgFnSc2d1	společenská
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mezi	mezi	k7c7	mezi
dětmi	dítě	k1gFnPc7	dítě
na	na	k7c6	na
Chiltonu	Chilton	k1gInSc6	Chilton
a	a	k8xC	a
Yaleu	Yaleus	k1gInSc6	Yaleus
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
důležitými	důležitý	k2eAgFnPc7d1	důležitá
postavami	postava	k1gFnPc7	postava
jsou	být	k5eAaImIp3nP	být
majitel	majitel	k1gMnSc1	majitel
bistra	bistro	k1gNnSc2	bistro
ve	v	k7c4	v
Stars	Stars	k1gInSc4	Stars
Hollow	Hollow	k1gMnPc2	Hollow
Luke	Luke	k1gInSc1	Luke
Danes	Danesa	k1gFnPc2	Danesa
<g/>
,	,	kIx,	,
Roryina	Roryina	k1gFnSc1	Roryina
kamarádka	kamarádka	k1gFnSc1	kamarádka
Lane	Lane	k1gFnSc1	Lane
Kimová	Kimová	k1gFnSc1	Kimová
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamarádka	kamarádka	k1gFnSc1	kamarádka
Lorelai	Lorela	k1gFnSc2	Lorela
Sookie	Sookie	k1gFnSc2	Sookie
St.	st.	kA	st.
James	James	k1gMnSc1	James
a	a	k8xC	a
rodiče	rodič	k1gMnPc1	rodič
Lorelai	Lorelai	k1gNnSc2	Lorelai
<g/>
,	,	kIx,	,
Emily	Emil	k1gMnPc4	Emil
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Gilmorovi	Gilmor	k1gMnSc3	Gilmor
<g/>
.	.	kIx.	.
</s>
<s>
Lauren	Laurna	k1gFnPc2	Laurna
Graham	Graham	k1gMnSc1	Graham
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Simona	Simona	k1gFnSc1	Simona
Postlerová	Postlerová	k1gFnSc1	Postlerová
<g/>
)	)	kIx)	)
jako	jako	k9	jako
Lorelai	Lorela	k1gFnSc2	Lorela
Gilmorová	Gilmorový	k2eAgFnSc1d1	Gilmorový
Alexis	Alexis	k1gFnSc1	Alexis
Bledel	Bledlo	k1gNnPc2	Bledlo
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Klára	Klára	k1gFnSc1	Klára
Jandová	Jandová	k1gFnSc1	Jandová
<g/>
)	)	kIx)	)
jako	jako	k9	jako
Rory	Rora	k1gFnSc2	Rora
Gilmorová	Gilmorový	k2eAgFnSc1d1	Gilmorový
Melissa	Melissa	k1gFnSc1	Melissa
McCarthy	McCartha	k1gFnSc2	McCartha
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Olga	Olga	k1gFnSc1	Olga
Želenská	Želenský	k2eAgFnSc1d1	Želenská
a	a	k8xC	a
Vlasta	Vlasta	k1gFnSc1	Vlasta
Žehrová	Žehrová	k1gFnSc1	Žehrová
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Sookie	Sookie	k1gFnSc2	Sookie
St.	st.	kA	st.
James	James	k1gMnSc1	James
Keiko	Keiko	k1gNnSc1	Keiko
<g />
.	.	kIx.	.
</s>
<s>
Agena	Agena	k1gFnSc1	Agena
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Kateřina	Kateřina	k1gFnSc1	Kateřina
Lojdová	Lojdová	k1gFnSc1	Lojdová
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
Hobzová	Hobzová	k1gFnSc1	Hobzová
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Lane	Lane	k1gNnSc1	Lane
Kim	Kim	k1gMnPc2	Kim
Yanic	Yanice	k1gInPc2	Yanice
Truesdale	Truesdala	k1gFnSc3	Truesdala
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Pantůček	Pantůček	k1gMnSc1	Pantůček
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
Terš	Terš	k1gMnSc1	Terš
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Michel	Michel	k1gMnSc1	Michel
Gerard	Gerard	k1gMnSc1	Gerard
Scott	Scott	k1gMnSc1	Scott
Patterson	Patterson	k1gMnSc1	Patterson
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g />
.	.	kIx.	.
</s>
<s>
Vaculík	Vaculík	k1gMnSc1	Vaculík
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Luke	Luke	k1gInSc1	Luke
Danes	Danesa	k1gFnPc2	Danesa
Kelly	Kella	k1gFnSc2	Kella
Bishop	Bishop	k1gInSc1	Bishop
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Růžena	Růžena	k1gFnSc1	Růžena
Merunková	Merunková	k1gFnSc1	Merunková
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Emily	Emil	k1gMnPc4	Emil
Gilmorová	Gilmorová	k1gFnSc1	Gilmorová
Edward	Edward	k1gMnSc1	Edward
Herrmann	Herrmann	k1gInSc1	Herrmann
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Miloš	Miloš	k1gMnSc1	Miloš
Hlavica	Hlavica	k1gMnSc1	Hlavica
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Richard	Richard	k1gMnSc1	Richard
Gilmore	Gilmor	k1gInSc5	Gilmor
Liza	Liz	k2eAgFnSc1d1	Liza
Weil	Weil	k1gInSc1	Weil
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Kateřina	Kateřina	k1gFnSc1	Kateřina
Březinová	Březinová	k1gFnSc1	Březinová
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Paris	Paris	k1gMnSc1	Paris
<g />
.	.	kIx.	.
</s>
<s>
Geller	Geller	k1gInSc1	Geller
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
host	host	k1gMnSc1	host
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
Jared	Jared	k1gInSc1	Jared
Padalecki	Padalecki	k1gNnSc1	Padalecki
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Libor	Libor	k1gMnSc1	Libor
Bouček	Bouček	k1gMnSc1	Bouček
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Dean	Dean	k1gMnSc1	Dean
Forester	Forester	k1gMnSc1	Forester
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
host	host	k1gMnSc1	host
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
Milo	milo	k6eAd1	milo
Ventimiglia	Ventimiglius	k1gMnSc2	Ventimiglius
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Maxián	Maxián	k1gMnSc1	Maxián
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Jess	Jess	k1gInSc1	Jess
Mariano	Mariana	k1gFnSc5	Mariana
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k8xS	jako
host	host	k1gMnSc1	host
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
Sean	Sean	k1gInSc1	Sean
Gunn	Gunn	k1gNnSc1	Gunn
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Ivo	Ivo	k1gMnSc1	Ivo
Novák	Novák	k1gMnSc1	Novák
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Kirk	Kirk	k1gMnSc1	Kirk
Gleason	Gleason	k1gMnSc1	Gleason
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
host	host	k1gMnSc1	host
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
řadě	řada	k1gFnSc3	řada
<g/>
)	)	kIx)	)
Chris	Chris	k1gFnSc1	Chris
Eigeman	Eigeman	k1gMnSc1	Eigeman
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Kolár	Kolár	k1gMnSc1	Kolár
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Jason	Jason	k1gMnSc1	Jason
Stiles	Stiles	k1gMnSc1	Stiles
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
Matt	Matt	k1gInSc1	Matt
Czuchry	Czuchra	k1gFnSc2	Czuchra
(	(	kIx(	(
<g/>
český	český	k2eAgInSc4d1	český
dabing	dabing	k1gInSc4	dabing
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kalous	Kalous	k1gMnSc1	Kalous
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Logan	Logan	k1gMnSc1	Logan
Huntzberger	Huntzberger	k1gMnSc1	Huntzberger
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
host	host	k1gMnSc1	host
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
Většina	většina	k1gFnSc1	většina
dialogů	dialog	k1gInPc2	dialog
v	v	k7c6	v
Gilmorových	Gilmorův	k2eAgNnPc6d1	Gilmorův
děvčatech	děvče	k1gNnPc6	děvče
je	být	k5eAaImIp3nS	být
obohacena	obohatit	k5eAaPmNgFnS	obohatit
rozmanitými	rozmanitý	k2eAgFnPc7d1	rozmanitá
narážkami	narážka	k1gFnPc7	narážka
na	na	k7c4	na
popkulturní	popkulturní	k2eAgInSc4d1	popkulturní
fenomény	fenomén	k1gInPc4	fenomén
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInPc4d1	televizní
pořady	pořad	k1gInPc4	pořad
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
či	či	k8xC	či
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zapeklitosti	zapeklitost	k1gFnSc3	zapeklitost
některých	některý	k3yIgFnPc2	některý
aluzí	aluze	k1gFnPc2	aluze
se	se	k3xPyFc4	se
studio	studio	k1gNnSc1	studio
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
sepsat	sepsat	k5eAaPmF	sepsat
seznam	seznam	k1gInSc4	seznam
"	"	kIx"	"
<g/>
gilmorismů	gilmorismus	k1gInPc2	gilmorismus
<g/>
"	"	kIx"	"
a	a	k8xC	a
přidat	přidat	k5eAaPmF	přidat
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
bookletu	booklet	k1gInSc2	booklet
k	k	k7c3	k
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
významných	významný	k2eAgFnPc2d1	významná
rolí	role	k1gFnPc2	role
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
Roryině	Roryin	k2eAgFnSc3d1	Roryin
kamarádce	kamarádka	k1gFnSc3	kamarádka
Lane	Lane	k1gFnSc3	Lane
Kim	Kim	k1gFnSc3	Kim
<g/>
,	,	kIx,	,
dceři	dcera	k1gFnSc3	dcera
přísných	přísný	k2eAgMnPc2d1	přísný
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
v	v	k7c6	v
bubnování	bubnování	k1gNnSc6	bubnování
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
své	svůj	k3xOyFgNnSc4	svůj
životní	životní	k2eAgNnSc4d1	životní
poslání	poslání	k1gNnSc4	poslání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
díky	díky	k7c3	díky
dvěma	dva	k4xCgFnPc3	dva
titulním	titulní	k2eAgFnPc3d1	titulní
postavám	postava	k1gFnPc3	postava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
je	být	k5eAaImIp3nS	být
hudba	hudba	k1gFnSc1	hudba
také	také	k6eAd1	také
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
života	život	k1gInSc2	život
a	a	k8xC	a
častým	častý	k2eAgInSc7d1	častý
námětem	námět	k1gInSc7	námět
hovorů	hovor	k1gInPc2	hovor
<g/>
.	.	kIx.	.
</s>
<s>
Nemálo	málo	k6eNd1	málo
hudebníků	hudebník	k1gMnPc2	hudebník
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
objeví	objevit	k5eAaPmIp3nS	objevit
osobně	osobně	k6eAd1	osobně
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Bangles	Bangles	k1gMnSc1	Bangles
<g/>
,	,	kIx,	,
Sonic	Sonic	k1gMnSc1	Sonic
Youth	Youth	k1gMnSc1	Youth
nebo	nebo	k8xC	nebo
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
Gil	Gil	k1gMnSc1	Gil
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
Laneiny	Lanein	k2eAgFnSc2d1	Lanein
skupiny	skupina	k1gFnSc2	skupina
Hep	Hep	k1gMnSc1	Hep
Alien	Alien	k1gInSc1	Alien
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
osobností	osobnost	k1gFnSc7	osobnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
Paul	Paul	k1gMnSc1	Paul
Anka	Anka	k1gFnSc1	Anka
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yIgInSc6	který
Lorelai	Lorelai	k1gNnSc6	Lorelai
pojmenuje	pojmenovat	k5eAaPmIp3nS	pojmenovat
svého	svůj	k3xOyFgMnSc4	svůj
psa	pes	k1gMnSc4	pes
a	a	k8xC	a
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
šesté	šestý	k4xOgFnSc6	šestý
řadě	řada	k1gFnSc6	řada
seriálu	seriál	k1gInSc2	seriál
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
Lorelaině	Lorelain	k2eAgInSc6d1	Lorelain
snu	sen	k1gInSc6	sen
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
úvodní	úvodní	k2eAgFnSc4d1	úvodní
píseň	píseň	k1gFnSc4	píseň
Carole	Carole	k1gFnSc1	Carole
King	King	k1gMnSc1	King
a	a	k8xC	a
Louise	Louis	k1gMnSc2	Louis
Goffin	Goffina	k1gFnPc2	Goffina
"	"	kIx"	"
<g/>
Where	Wher	k1gMnSc5	Wher
You	You	k1gMnSc5	You
Lead	Leado	k1gNnPc2	Leado
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Gilmorova	Gilmorův	k2eAgNnPc1d1	Gilmorův
děvčata	děvče	k1gNnPc1	děvče
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
i	i	k8xC	i
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
představitelé	představitel	k1gMnPc1	představitel
získali	získat	k5eAaPmAgMnP	získat
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
např.	např.	kA	např.
Family	Famila	k1gFnSc2	Famila
TV	TV	kA	TV
Award	Award	k1gMnSc1	Award
<g/>
,	,	kIx,	,
Teen	Teen	k1gMnSc1	Teen
Choice	Choice	k1gFnSc2	Choice
Award	Awardo	k1gNnPc2	Awardo
pro	pro	k7c4	pro
Lauren	Laurno	k1gNnPc2	Laurno
Graham	Graham	k1gMnSc1	Graham
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
televizní	televizní	k2eAgFnSc4d1	televizní
mámu	máma	k1gFnSc4	máma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
Amerického	americký	k2eAgInSc2d1	americký
filmového	filmový	k2eAgInSc2d1	filmový
institutu	institut	k1gInSc2	institut
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
časopis	časopis	k1gInSc1	časopis
Entertainment	Entertainment	k1gInSc1	Entertainment
Weekly	Weekl	k1gInPc7	Weekl
zařadil	zařadit	k5eAaPmAgInS	zařadit
seriál	seriál	k1gInSc1	seriál
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
"	"	kIx"	"
<g/>
best	best	k1gInSc1	best
of	of	k?	of
<g/>
"	"	kIx"	"
končícího	končící	k2eAgNnSc2d1	končící
desetiletí	desetiletí	k1gNnSc2	desetiletí
s	s	k7c7	s
poznámkou	poznámka	k1gFnSc7	poznámka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Děkujeme	děkovat	k5eAaImIp1nP	děkovat
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
Lorelai	Lorelai	k1gNnSc4	Lorelai
a	a	k8xC	a
Rory	Rory	k1gInPc4	Rory
<g/>
,	,	kIx,	,
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
jste	být	k5eAaImIp2nP	být
nám	my	k3xPp1nPc3	my
<g/>
,	,	kIx,	,
že	že	k8xS	že
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
dospívající	dospívající	k2eAgFnSc7d1	dospívající
dcerou	dcera	k1gFnSc7	dcera
není	být	k5eNaImIp3nS	být
sci-fi	scii	k1gFnSc1	sci-fi
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
jen	jen	k9	jen
pořádná	pořádný	k2eAgFnSc1d1	pořádná
dávka	dávka	k1gFnSc1	dávka
trpělivosti	trpělivost	k1gFnSc2	trpělivost
<g/>
,	,	kIx,	,
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Časopis	časopis	k1gInSc1	časopis
Time	Time	k1gInSc1	Time
zařadil	zařadit	k5eAaPmAgInS	zařadit
Gilmorova	Gilmorův	k2eAgNnPc4d1	Gilmorův
děvčata	děvče	k1gNnPc4	děvče
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
sta	sto	k4xCgNnSc2	sto
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
televizních	televizní	k2eAgInPc2d1	televizní
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
také	také	k9	také
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
32	[number]	k4	32
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
"	"	kIx"	"
<g/>
Nové	Nové	k2eAgFnPc1d1	Nové
televizní	televizní	k2eAgFnPc1d1	televizní
klasiky	klasika	k1gFnPc1	klasika
<g/>
"	"	kIx"	"
Entertainment	Entertainment	k1gInSc1	Entertainment
Weekly	Weekl	k1gInPc1	Weekl
<g/>
.	.	kIx.	.
</s>
