<s>
Čipová	čipový	k2eAgFnSc1d1	čipová
sada	sada	k1gFnSc1	sada
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
chipset	chipset	k5eAaImF	chipset
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
čipů	čip	k1gInPc2	čip
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
navrženy	navrhnout	k5eAaPmNgFnP	navrhnout
ke	k	k7c3	k
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
spolupráci	spolupráce	k1gFnSc3	spolupráce
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
prodávány	prodávat	k5eAaImNgInP	prodávat
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
produkt	produkt	k1gInSc1	produkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
počítačů	počítač	k1gInPc2	počítač
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
obvykle	obvykle	k6eAd1	obvykle
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
čipů	čip	k1gInPc2	čip
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
desce	deska	k1gFnSc6	deska
nebo	nebo	k8xC	nebo
na	na	k7c6	na
rozšiřujících	rozšiřující	k2eAgFnPc6d1	rozšiřující
kartách	karta	k1gFnPc6	karta
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
počítačů	počítač	k1gMnPc2	počítač
třídy	třída	k1gFnSc2	třída
PC	PC	kA	PC
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
dva	dva	k4xCgInPc4	dva
čipy	čip	k1gInPc4	čip
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
desce	deska	k1gFnSc6	deska
–	–	k?	–
tzv.	tzv.	kA	tzv.
northbridge	northbridg	k1gFnSc2	northbridg
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
severní	severní	k2eAgInSc4d1	severní
můstek	můstek	k1gInSc4	můstek
<g/>
)	)	kIx)	)
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
southbridge	southbridge	k6eAd1	southbridge
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
northbridge	northbridg	k1gMnSc2	northbridg
a	a	k8xC	a
southbridge	southbridg	k1gMnSc2	southbridg
výrobce	výrobce	k1gMnSc2	výrobce
někdy	někdy	k6eAd1	někdy
implementuje	implementovat	k5eAaImIp3nS	implementovat
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
čipu	čip	k1gInSc2	čip
–	–	k?	–
funkci	funkce	k1gFnSc4	funkce
obou	dva	k4xCgFnPc6	dva
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
jeden	jeden	k4xCgInSc1	jeden
celistvý	celistvý	k2eAgInSc1d1	celistvý
čip	čip	k1gInSc1	čip
<g/>
;	;	kIx,	;
výrobci	výrobce	k1gMnPc1	výrobce
takovýchto	takovýto	k3xDgFnPc2	takovýto
čipových	čipový	k2eAgFnPc2d1	čipová
sad	sada	k1gFnPc2	sada
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
na	na	k7c6	na
výrobcích	výrobek	k1gInPc6	výrobek
základních	základní	k2eAgFnPc2d1	základní
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Čipová	čipový	k2eAgFnSc1d1	čipová
sada	sada	k1gFnSc1	sada
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
procesorem	procesor	k1gInSc7	procesor
<g/>
,	,	kIx,	,
sběrnicemi	sběrnice	k1gFnPc7	sběrnice
<g/>
,	,	kIx,	,
sloty	slot	k1gInPc7	slot
<g/>
,	,	kIx,	,
řadiči	řadič	k1gInPc7	řadič
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
součástmi	součást	k1gFnPc7	součást
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
producenty	producent	k1gMnPc4	producent
čipů	čip	k1gInPc2	čip
patří	patřit	k5eAaImIp3nP	patřit
společnostem	společnost	k1gFnPc3	společnost
jako	jako	k9	jako
NVIDIA	NVIDIA	kA	NVIDIA
<g/>
,	,	kIx,	,
AMD	AMD	kA	AMD
<g/>
,	,	kIx,	,
VIA	via	k7c4	via
Technologies	Technologies	k1gInSc4	Technologies
<g/>
,	,	kIx,	,
SiS	SiS	k1gFnSc4	SiS
a	a	k8xC	a
Intel	Intel	kA	Intel
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
vyvíjené	vyvíjený	k2eAgFnSc6d1	vyvíjená
ATI	ATI	kA	ATI
<g/>
.	.	kIx.	.
</s>
<s>
AMD	AMD	kA	AMD
vždy	vždy	k6eAd1	vždy
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
čipsetové	čipsetový	k2eAgFnPc4d1	čipsetová
sady	sada	k1gFnPc4	sada
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
platformy	platforma	k1gFnPc4	platforma
<g/>
.	.	kIx.	.
</s>
<s>
ATI	ATI	kA	ATI
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
pro	pro	k7c4	pro
Intel	Intel	kA	Intel
i	i	k8xC	i
AMD	AMD	kA	AMD
platformy	platforma	k1gFnPc4	platforma
<g/>
.	.	kIx.	.
</s>
<s>
AMD	AMD	kA	AMD
900	[number]	k4	900
série	série	k1gFnSc2	série
AMD	AMD	kA	AMD
970	[number]	k4	970
AMD	AMD	kA	AMD
990X	[number]	k4	990X
AMD	AMD	kA	AMD
990FX	[number]	k4	990FX
AMD	AMD	kA	AMD
800	[number]	k4	800
série	série	k1gFnSc2	série
AMD	AMD	kA	AMD
870	[number]	k4	870
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
SB	sb	kA	sb
<g/>
750	[number]	k4	750
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
880G	[number]	k4	880G
(	(	kIx(	(
<g/>
VGA	VGA	kA	VGA
HD	HD	kA	HD
4250	[number]	k4	4250
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
SB	sb	kA	sb
<g/>
750	[number]	k4	750
<g/>
/	/	kIx~	/
<g/>
SB	sb	kA	sb
<g/>
850	[number]	k4	850
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
890GX	[number]	k4	890GX
(	(	kIx(	(
<g/>
VGA	VGA	kA	VGA
<g />
.	.	kIx.	.
</s>
<s>
HD	HD	kA	HD
4290	[number]	k4	4290
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
SB	sb	kA	sb
<g/>
850	[number]	k4	850
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
890FX	[number]	k4	890FX
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
SB	sb	kA	sb
<g/>
850	[number]	k4	850
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
700	[number]	k4	700
série	série	k1gFnSc2	série
AMD	AMD	kA	AMD
790GX	[number]	k4	790GX
(	(	kIx(	(
<g/>
VGA	VGA	kA	VGA
HD	HD	kA	HD
3300	[number]	k4	3300
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
SB	sb	kA	sb
<g/>
750	[number]	k4	750
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
790FX	[number]	k4	790FX
(	(	kIx(	(
<g/>
SB	sb	kA	sb
<g/>
750	[number]	k4	750
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
AMD	AMD	kA	AMD
790X	[number]	k4	790X
(	(	kIx(	(
<g/>
SB	sb	kA	sb
<g/>
750	[number]	k4	750
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
785G	[number]	k4	785G
(	(	kIx(	(
<g/>
HD	HD	kA	HD
4200	[number]	k4	4200
<g/>
,	,	kIx,	,
SB	sb	kA	sb
<g/>
710	[number]	k4	710
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
780G	[number]	k4	780G
(	(	kIx(	(
<g/>
HD	HD	kA	HD
3200	[number]	k4	3200
<g/>
,	,	kIx,	,
SB	sb	kA	sb
<g/>
700	[number]	k4	700
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
780M	[number]	k4	780M
(	(	kIx(	(
<g/>
HD	HD	kA	HD
3200	[number]	k4	3200
<g/>
,	,	kIx,	,
SB	sb	kA	sb
<g/>
700	[number]	k4	700
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
780V	[number]	k4	780V
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
HD	HD	kA	HD
3100	[number]	k4	3100
<g/>
,	,	kIx,	,
SB	sb	kA	sb
<g/>
700	[number]	k4	700
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
770	[number]	k4	770
(	(	kIx(	(
<g/>
SB	sb	kA	sb
<g/>
700	[number]	k4	700
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
760G	[number]	k4	760G
(	(	kIx(	(
<g/>
HD	HD	kA	HD
3200	[number]	k4	3200
<g/>
,	,	kIx,	,
SB	sb	kA	sb
<g/>
710	[number]	k4	710
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
740G	[number]	k4	740G
AMD	AMD	kA	AMD
600	[number]	k4	600
série	série	k1gFnSc1	série
(	(	kIx(	(
<g/>
vyvíjená	vyvíjený	k2eAgFnSc1d1	vyvíjená
ATI	ATI	kA	ATI
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
M690V	M690V	k1gFnSc2	M690V
AMD	AMD	kA	AMD
M690T	M690T	k1gMnSc2	M690T
–	–	k?	–
mobilní	mobilní	k2eAgInSc4d1	mobilní
<g />
.	.	kIx.	.
</s>
<s>
verze	verze	k1gFnSc1	verze
AMD	AMD	kA	AMD
690G	[number]	k4	690G
(	(	kIx(	(
<g/>
Radeon	Radeon	k1gInSc1	Radeon
X	X	kA	X
<g/>
1250	[number]	k4	1250
<g/>
,	,	kIx,	,
SB	sb	kA	sb
<g/>
600	[number]	k4	600
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
690V	[number]	k4	690V
(	(	kIx(	(
<g/>
Radeon	Radeon	k1gInSc1	Radeon
X	X	kA	X
<g/>
1200	[number]	k4	1200
<g/>
,	,	kIx,	,
SB	sb	kA	sb
<g/>
600	[number]	k4	600
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
580X	[number]	k4	580X
CrossFire	CrossFir	k1gInSc5	CrossFir
(	(	kIx(	(
<g/>
Radeon	Radeon	k1gInSc4	Radeon
X	X	kA	X
<g/>
200	[number]	k4	200
<g/>
,	,	kIx,	,
ALi	ALi	k1gFnSc1	ALi
M	M	kA	M
<g/>
1575	[number]	k4	1575
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
570X	[number]	k4	570X
<g />
.	.	kIx.	.
</s>
<s>
CrossFire	CrossFir	k1gMnSc5	CrossFir
(	(	kIx(	(
<g/>
Radeon	Radeon	k1gInSc4	Radeon
X	X	kA	X
<g/>
200	[number]	k4	200
<g/>
,	,	kIx,	,
ALi	ALi	k1gFnSc1	ALi
M	M	kA	M
<g/>
1575	[number]	k4	1575
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
480X	[number]	k4	480X
CrossFire	CrossFir	k1gInSc5	CrossFir
(	(	kIx(	(
<g/>
Radeon	Radeon	k1gInSc4	Radeon
X	X	kA	X
<g/>
200	[number]	k4	200
<g/>
,	,	kIx,	,
IXP	IXP	kA	IXP
<g/>
400	[number]	k4	400
<g/>
)	)	kIx)	)
AMD	AMD	kA	AMD
Express	express	k1gInSc4	express
1000	[number]	k4	1000
série	série	k1gFnSc1	série
(	(	kIx(	(
<g/>
vyvíjená	vyvíjený	k2eAgFnSc1d1	vyvíjená
ATI	ATI	kA	ATI
<g/>
)	)	kIx)	)
ATI	ATI	kA	ATI
Radeon	Radeon	k1gNnSc4	Radeon
Xpress	Xpress	k1gInSc1	Xpress
1150	[number]	k4	1150
(	(	kIx(	(
<g/>
Radeon	Radeon	k1gInSc1	Radeon
X	X	kA	X
<g/>
<g />
.	.	kIx.	.
</s>
<s>
300	[number]	k4	300
<g/>
,	,	kIx,	,
IXP	IXP	kA	IXP
<g/>
400	[number]	k4	400
<g/>
)	)	kIx)	)
ATI	ATI	kA	ATI
Radeon	Radeon	k1gNnSc4	Radeon
Xpress	Xpress	k1gInSc1	Xpress
1100	[number]	k4	1100
(	(	kIx(	(
<g/>
Radeon	Radeon	k1gInSc1	Radeon
X	X	kA	X
<g/>
300	[number]	k4	300
<g/>
,	,	kIx,	,
IXP	IXP	kA	IXP
<g/>
400	[number]	k4	400
<g/>
)	)	kIx)	)
AMD-700	AMD-700	k1gFnSc1	AMD-700
série	série	k1gFnSc1	série
AMD-762	AMD-762	k1gFnSc1	AMD-762
(	(	kIx(	(
<g/>
760	[number]	k4	760
<g/>
MPX	MPX	kA	MPX
+	+	kIx~	+
768	[number]	k4	768
<g/>
)	)	kIx)	)
AMD-761	AMD-761	k1gFnSc2	AMD-761
(	(	kIx(	(
<g/>
760	[number]	k4	760
+	+	kIx~	+
766	[number]	k4	766
<g/>
)	)	kIx)	)
AMD-751	AMD-751	k1gFnSc2	AMD-751
(	(	kIx(	(
<g/>
750	[number]	k4	750
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
756	[number]	k4	756
<g/>
)	)	kIx)	)
AMD-600	AMD-600	k1gFnSc1	AMD-600
série	série	k1gFnSc1	série
AMD-640	AMD-640	k1gFnSc1	AMD-640
(	(	kIx(	(
<g/>
640	[number]	k4	640
+	+	kIx~	+
645	[number]	k4	645
<g/>
)	)	kIx)	)
ATI	ATI	kA	ATI
CrossFire	CrossFir	k1gInSc5	CrossFir
Xpress	Xpress	k1gInSc4	Xpress
3200	[number]	k4	3200
ATI	ATI	kA	ATI
Radeon	Radeon	k1gInSc1	Radeon
Xpress	Xpress	k1gInSc1	Xpress
1250	[number]	k4	1250
ATI	ATI	kA	ATI
Radeon	Radeon	k1gInSc1	Radeon
Xpress	Xpress	k1gInSc1	Xpress
1100	[number]	k4	1100
ATI	ATI	kA	ATI
Radeon	Radeon	k1gInSc1	Radeon
Xpress	Xpress	k1gInSc1	Xpress
200	[number]	k4	200
<g/>
,	,	kIx,	,
200M	[number]	k4	200M
SB750	SB750	k1gFnSc1	SB750
SB710	SB710	k1gFnSc1	SB710
SB700S	SB700S	k1gFnSc1	SB700S
–	–	k?	–
zaměření	zaměření	k1gNnSc2	zaměření
na	na	k7c4	na
servery	server	k1gInPc4	server
SB700	SB700	k1gMnSc1	SB700
SB600	SB600	k1gFnSc2	SB600
AMD-768	AMD-768	k1gMnSc1	AMD-768
AMD-766	AMD-766	k1gMnSc1	AMD-766
AMD-756	AMD-756	k1gMnSc1	AMD-756
AMD-645	AMD-645	k1gMnSc1	AMD-645
Intel	Intel	kA	Intel
vždy	vždy	k6eAd1	vždy
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
čipsetové	čipsetový	k2eAgFnPc4d1	čipsetová
sady	sada	k1gFnPc4	sada
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
platformy	platforma	k1gFnPc4	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
x	x	k?	x
<g/>
50	[number]	k4	50
série	série	k1gFnSc2	série
Intel	Intel	kA	Intel
X58	X58	k1gMnSc1	X58
Podporuje	podporovat	k5eAaImIp3nS	podporovat
procesory	procesor	k1gInPc4	procesor
Core	Cor	k1gFnSc2	Cor
i	i	k9	i
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
P55	P55	k1gFnSc1	P55
Podporuje	podporovat	k5eAaImIp3nS	podporovat
procesory	procesor	k1gInPc4	procesor
Core	Cor	k1gFnSc2	Cor
i	i	k9	i
<g/>
5	[number]	k4	5
a	a	k8xC	a
Core	Core	k1gInSc1	Core
i	i	k9	i
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
H55	H55	k1gFnSc1	H55
/	/	kIx~	/
H57	H57	k1gFnSc1	H57
Podporuje	podporovat	k5eAaImIp3nS	podporovat
procesory	procesor	k1gInPc4	procesor
Core	Cor	k1gFnSc2	Cor
i	i	k9	i
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
5	[number]	k4	5
a	a	k8xC	a
i	i	k9	i
<g/>
7	[number]	k4	7
s	s	k7c7	s
grafikou	grafika	k1gFnSc7	grafika
Intel	Intel	kA	Intel
x	x	k?	x
<g/>
40	[number]	k4	40
série	série	k1gFnSc1	série
Intel	Intel	kA	Intel
G41	G41	k1gFnSc1	G41
čip	čip	k1gInSc1	čip
829	[number]	k4	829
<g/>
G	G	kA	G
<g/>
41	[number]	k4	41
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
ICH	ICH	kA	ICH
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
grafika	grafik	k1gMnSc2	grafik
Intel	Intel	kA	Intel
GMA	GMA	kA	GMA
X4500	X4500	k1gFnPc2	X4500
Intel	Intel	kA	Intel
G43	G43	k1gFnSc1	G43
čip	čip	k1gInSc1	čip
<g />
.	.	kIx.	.
</s>
<s>
829	[number]	k4	829
<g/>
G	G	kA	G
<g/>
43	[number]	k4	43
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
ICH	ICH	kA	ICH
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
grafika	grafik	k1gMnSc2	grafik
Intel	Intel	kA	Intel
GMA	GMA	kA	GMA
X4500	X4500	k1gFnPc2	X4500
Intel	Intel	kA	Intel
G45	G45	k1gFnSc1	G45
čip	čip	k1gInSc1	čip
829	[number]	k4	829
<g/>
G	G	kA	G
<g/>
45	[number]	k4	45
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
ICH	ICH	kA	ICH
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
grafika	grafik	k1gMnSc2	grafik
Intel	Intel	kA	Intel
GMA	GMA	kA	GMA
X4500HD	X4500HD	k1gMnSc1	X4500HD
Intel	Intel	kA	Intel
P43	P43	k1gMnSc1	P43
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
ICH10	ICH10	k1gFnSc2	ICH10
Intel	Intel	kA	Intel
P45	P45	k1gMnSc1	P45
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
ICH10	ICH10	k1gFnSc2	ICH10
Intel	Intel	kA	Intel
Q45	Q45	k1gMnSc1	Q45
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
ICH10	ICH10	k1gFnSc2	ICH10
Intel	Intel	kA	Intel
X48	X48	k1gMnSc1	X48
jižní	jižní	k2eAgInSc1d1	jižní
můstek	můstek	k1gInSc1	můstek
ICH9	ICH9	k1gMnSc2	ICH9
Intel	Intel	kA	Intel
x	x	k?	x
<g/>
30	[number]	k4	30
série	série	k1gFnSc1	série
Intel	Intel	kA	Intel
G31	G31	k1gFnSc1	G31
Express	express	k1gInSc1	express
Podporuje	podporovat	k5eAaImIp3nS	podporovat
paměti	paměť	k1gFnSc2	paměť
typu	typ	k1gInSc2	typ
DDR	DDR	kA	DDR
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
podporuje	podporovat	k5eAaImIp3nS	podporovat
do	do	k7c2	do
1066	[number]	k4	1066
MHz	Mhz	kA	Mhz
System	Syst	k1gInSc7	Syst
Bus	bus	k1gInSc1	bus
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
G33	G33	k1gFnSc1	G33
Express	express	k1gInSc1	express
Podporuje	podporovat	k5eAaImIp3nS	podporovat
paměti	paměť	k1gFnSc2	paměť
typu	typ	k1gInSc2	typ
DDR2	DDR2	k1gFnSc2	DDR2
a	a	k8xC	a
DDR	DDR	kA	DDR
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
G35	G35	k1gFnSc1	G35
Express	express	k1gInSc1	express
Podporuje	podporovat	k5eAaImIp3nS	podporovat
paměti	paměť	k1gFnSc2	paměť
typu	typ	k1gInSc2	typ
DDR2	DDR2	k1gMnSc2	DDR2
P31	P31	k1gMnSc2	P31
P35	P35	k1gMnSc2	P35
Express	express	k1gInSc1	express
Podporuje	podporovat	k5eAaImIp3nS	podporovat
procesory	procesor	k1gInPc4	procesor
Intel	Intel	kA	Intel
Core	Cor	k1gInSc2	Cor
2	[number]	k4	2
Duo	duo	k1gNnSc1	duo
<g/>
/	/	kIx~	/
<g/>
Quad	Quad	k1gInSc1	Quad
<g/>
/	/	kIx~	/
<g/>
Extreme	Extrem	k1gInSc5	Extrem
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
X38	X38	k1gFnSc1	X38
900	[number]	k4	900
série	série	k1gFnSc1	série
963	[number]	k4	963
<g/>
Q	Q	kA	Q
<g/>
,	,	kIx,	,
965	[number]	k4	965
<g/>
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
G	G	kA	G
<g/>
/	/	kIx~	/
<g/>
Q	Q	kA	Q
<g/>
,	,	kIx,	,
975X	[number]	k4	975X
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Intel	Intel	kA	Intel
Core	Core	k1gInSc4	Core
2	[number]	k4	2
Duo	duo	k1gNnSc1	duo
<g/>
)	)	kIx)	)
910	[number]	k4	910
<g/>
GL	GL	kA	GL
<g/>
,	,	kIx,	,
915	[number]	k4	915
<g/>
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
PL	PL	kA	PL
<g/>
/	/	kIx~	/
<g/>
G	G	kA	G
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
GL	GL	kA	GL
<g/>
/	/	kIx~	/
<g/>
GV	GV	kA	GV
<g/>
,	,	kIx,	,
925	[number]	k4	925
<g/>
X	X	kA	X
<g/>
/	/	kIx~	/
<g/>
XE	XE	kA	XE
<g/>
,	,	kIx,	,
945	[number]	k4	945
<g/>
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
PL	PL	kA	PL
<g/>
/	/	kIx~	/
<g/>
G	G	kA	G
<g/>
/	/	kIx~	/
<g/>
GZ	GZ	kA	GZ
<g/>
/	/	kIx~	/
<g/>
GSE	GSE	kA	GSE
<g/>
,	,	kIx,	,
955X	[number]	k4	955X
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Pentium	Pentium	kA	Pentium
D	D	kA	D
<g/>
/	/	kIx~	/
<g/>
XE	XE	kA	XE
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
910	[number]	k4	910
<g/>
GML	GML	kA	GML
<g/>
/	/	kIx~	/
<g/>
GMZ	GMZ	kA	GMZ
<g/>
,	,	kIx,	,
915	[number]	k4	915
<g/>
GM	GM	kA	GM
<g/>
/	/	kIx~	/
<g/>
PM	PM	kA	PM
<g/>
,	,	kIx,	,
945	[number]	k4	945
<g/>
PM	PM	kA	PM
<g/>
/	/	kIx~	/
<g/>
GM	GM	kA	GM
945	[number]	k4	945
<g/>
PM	PM	kA	PM
<g/>
/	/	kIx~	/
<g/>
GM	GM	kA	GM
+	+	kIx~	+
Intel	Intel	kA	Intel
PRO	pro	k7c4	pro
<g/>
/	/	kIx~	/
<g/>
Wireless	Wireless	k1gInSc4	Wireless
3945ABG	[number]	k4	3945ABG
+	+	kIx~	+
Intel	Intel	kA	Intel
Core	Core	k1gNnSc1	Core
Solo	Solo	k1gNnSc1	Solo
=	=	kIx~	=
Centrino	Centrino	k1gNnSc1	Centrino
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgFnSc1	třetí
generace	generace	k1gFnSc1	generace
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
945	[number]	k4	945
<g/>
PM	PM	kA	PM
<g/>
/	/	kIx~	/
<g/>
GM	GM	kA	GM
+	+	kIx~	+
Intel	Intel	kA	Intel
PRO	pro	k7c4	pro
<g/>
/	/	kIx~	/
<g/>
Wireless	Wireless	k1gInSc4	Wireless
3945ABG	[number]	k4	3945ABG
+	+	kIx~	+
Intel	Intel	kA	Intel
Core	Core	k1gNnSc1	Core
Duo	duo	k1gNnSc1	duo
=	=	kIx~	=
Centrino	Centrin	k2eAgNnSc1d1	Centrino
Duo	duo	k1gNnSc1	duo
Řada	řada	k1gFnSc1	řada
Intel	Intel	kA	Intel
E700	E700	k1gFnSc1	E700
E7205	E7205	k1gFnSc1	E7205
(	(	kIx(	(
<g/>
for	forum	k1gNnPc2	forum
Pentium	Pentium	kA	Pentium
4	[number]	k4	4
servers	serversa	k1gFnPc2	serversa
<g/>
)	)	kIx)	)
E	E	kA	E
<g/>
7500	[number]	k4	7500
<g/>
/	/	kIx~	/
<g/>
E	E	kA	E
<g/>
7501	[number]	k4	7501
<g/>
/	/	kIx~	/
<g/>
E	E	kA	E
<g/>
<g />
.	.	kIx.	.
</s>
<s>
7505	[number]	k4	7505
(	(	kIx(	(
<g/>
for	forum	k1gNnPc2	forum
Xeon	Xeona	k1gFnPc2	Xeona
servers	serversa	k1gFnPc2	serversa
<g/>
))	))	k?	))
E	E	kA	E
<g/>
7520	[number]	k4	7520
<g/>
/	/	kIx~	/
<g/>
E	E	kA	E
<g/>
7525	[number]	k4	7525
<g/>
/	/	kIx~	/
<g/>
E	E	kA	E
<g/>
7530	[number]	k4	7530
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
dual	dual	k1gInSc4	dual
Xeon	Xeona	k1gFnPc2	Xeona
servers	serversa	k1gFnPc2	serversa
<g/>
)	)	kIx)	)
i	i	k9	i
<g/>
800	[number]	k4	800
série	série	k1gFnSc2	série
i	i	k8xC	i
<g/>
875	[number]	k4	875
<g/>
p	p	k?	p
=	=	kIx~	=
optimized	optimized	k1gMnSc1	optimized
i	i	k9	i
<g/>
865	[number]	k4	865
i	i	k9	i
<g/>
865	[number]	k4	865
<g/>
G	G	kA	G
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
P	P	kA	P
4	[number]	k4	4
GB	GB	kA	GB
dual-channel	dualhannela	k1gFnPc2	dual-channela
DDR	DDR	kA	DDR
533	[number]	k4	533
MHz	Mhz	kA	Mhz
FSB	FSB	kA	FSB
AGP	AGP	kA	AGP
<g/>
8	[number]	k4	8
<g/>
x	x	k?	x
SATA	SATA	kA	SATA
i	i	k9	i
<g/>
850	[number]	k4	850
<g/>
e	e	k0	e
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
855	[number]	k4	855
<g/>
G	G	kA	G
(	(	kIx(	(
<g/>
855	[number]	k4	855
<g/>
GME	GME	kA	GME
pro	pro	k7c4	pro
Pentium	Pentium	kA	Pentium
M	M	kA	M
<g/>
)	)	kIx)	)
i	i	k9	i
<g/>
845	[number]	k4	845
<g/>
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
GV	GV	kA	GV
Bus	bus	k1gInSc4	bus
533	[number]	k4	533
<g />
.	.	kIx.	.
</s>
<s>
MHz	Mhz	kA	Mhz
AGP	AGP	kA	AGP
<g/>
4	[number]	k4	4
<g/>
x	x	k?	x
2GB	[number]	k4	2GB
DDR	DDR	kA	DDR
PC2700	PC2700	k1gFnPc2	PC2700
max	max	kA	max
NVIDIA	NVIDIA	kA	NVIDIA
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
pro	pro	k7c4	pro
Intel	Intel	kA	Intel
i	i	k8xC	i
AMD	AMD	kA	AMD
platformy	platforma	k1gFnSc2	platforma
<g/>
.	.	kIx.	.
nForce	nForec	k1gInSc2	nForec
900	[number]	k4	900
980	[number]	k4	980
nForce	nForec	k1gInSc2	nForec
700	[number]	k4	700
720	[number]	k4	720
750	[number]	k4	750
780	[number]	k4	780
<g/>
i	i	k8xC	i
SLI	SLI	kA	SLI
790	[number]	k4	790
<g/>
i	i	k8xC	i
SLI	SLI	kA	SLI
nForce	nForec	k1gInSc2	nForec
600	[number]	k4	600
630	[number]	k4	630
650	[number]	k4	650
<g/>
i	i	k9	i
650	[number]	k4	650
<g/>
i	i	k8xC	i
SLI	SLI	kA	SLI
650	[number]	k4	650
<g/>
i	i	k8xC	i
Ultra	ultra	k2eAgInSc1d1	ultra
680	[number]	k4	680
<g/>
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
SLI	SLI	kA	SLI
680	[number]	k4	680
SLI	SLI	kA	SLI
680	[number]	k4	680
<g/>
i	i	k8xC	i
LT	LT	kA	LT
SLI	SLI	kA	SLI
nForce	nForce	k1gMnSc1	nForce
500	[number]	k4	500
520	[number]	k4	520
550	[number]	k4	550
SLI	SLI	kA	SLI
AMD	AMD	kA	AMD
570	[number]	k4	570
SLI	SLI	kA	SLI
AMD	AMD	kA	AMD
570	[number]	k4	570
SLI	SLI	kA	SLI
Intel	Intel	kA	Intel
590	[number]	k4	590
SLI	SLI	kA	SLI
AMD	AMD	kA	AMD
590	[number]	k4	590
SLI	SLI	kA	SLI
Intel	Intel	kA	Intel
nForce	nForce	k1gMnSc1	nForce
4	[number]	k4	4
430	[number]	k4	430
SLI	SLI	kA	SLI
Intel	Intel	kA	Intel
Series	Seriesa	k1gFnPc2	Seriesa
AMD	AMD	kA	AMD
SLI	SLI	kA	SLI
<g/>
/	/	kIx~	/
<g/>
XE	XE	kA	XE
Ultra	ultra	k2eAgFnPc1d1	ultra
Intel	Intel	kA	Intel
Intel	Intel	kA	Intel
x	x	k?	x
<g/>
16	[number]	k4	16
nForce	nForec	k1gInSc2	nForec
3	[number]	k4	3
Go	Go	k1gFnSc2	Go
Professional	Professional	k1gFnSc2	Professional
nForce	nForec	k1gInSc2	nForec
2	[number]	k4	2
Ultra	ultra	k2eAgInPc2d1	ultra
400	[number]	k4	400
<g/>
Gb	Gb	k1gFnSc1	Gb
Ultra	ultra	k2eAgInPc2d1	ultra
400R	[number]	k4	400R
Ultra	ultra	k2eAgInPc2d1	ultra
400	[number]	k4	400
nForce	nForec	k1gInSc2	nForec
1	[number]	k4	1
VIA	via	k7c4	via
CLE266	CLE266	k1gFnSc4	CLE266
VIA	via	k7c4	via
CN700	CN700	k1gFnSc4	CN700
VIA	via	k7c4	via
CN896	CN896	k1gFnSc4	CN896
VIA	via	k7c4	via
CX700	CX700	k1gFnSc4	CX700
VIA	via	k7c4	via
CX700M2	CX700M2	k1gFnSc4	CX700M2
VIA	via	k7c4	via
P4M900	P4M900	k1gFnSc4	P4M900
VIA	via	k7c4	via
VT8237R	VT8237R	k1gFnSc4	VT8237R
VIA	via	k7c4	via
VX700	VX700	k1gFnSc4	VX700
Asus	Asus	k1gInSc1	Asus
Biostar	Biostar	k1gMnSc1	Biostar
DFI	DFI	kA	DFI
EVGA	EVGA	kA	EVGA
Gigabyte	Gigabyt	k1gInSc5	Gigabyt
Intel	Intel	kA	Intel
MSI	MSI	kA	MSI
ASRock	ASRock	k1gMnSc1	ASRock
</s>
