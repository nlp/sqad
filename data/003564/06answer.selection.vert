<s>
Čipová	čipový	k2eAgFnSc1d1	čipová
sada	sada	k1gFnSc1	sada
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
chipset	chipset	k5eAaPmF	chipset
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
čipů	čip	k1gInPc2	čip
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
navrženy	navrhnout	k5eAaPmNgFnP	navrhnout
ke	k	k7c3	k
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
spolupráci	spolupráce	k1gFnSc3	spolupráce
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
prodávány	prodávat	k5eAaImNgInP	prodávat
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
produkt	produkt	k1gInSc1	produkt
<g/>
.	.	kIx.	.
</s>
