<s>
Mramorový	mramorový	k2eAgInSc1d1	mramorový
oblouk	oblouk	k1gInSc1	oblouk
(	(	kIx(	(
<g/>
Marble	Marble	k1gFnSc1	Marble
Arch	arch	k1gInSc1	arch
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oblouk	oblouk	k1gInSc4	oblouk
z	z	k7c2	z
kararskéko	kararskéko	k6eAd1	kararskéko
mramoru	mramor	k1gInSc3	mramor
poblíž	poblíž	k7c2	poblíž
Speakers	Speakersa	k1gFnPc2	Speakersa
<g/>
'	'	kIx"	'
Corner	Corner	k1gMnSc1	Corner
v	v	k7c6	v
Londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Hyde	Hyde	k1gNnSc6	Hyde
Parku	park	k1gInSc2	park
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
obvodu	obvod	k1gInSc6	obvod
Westminster	Westminstra	k1gFnPc2	Westminstra
u	u	k7c2	u
západního	západní	k2eAgInSc2d1	západní
konce	konec	k1gInSc2	konec
Oxford	Oxford	k1gInSc1	Oxford
Street	Street	k1gInSc1	Street
<g/>
.	.	kIx.	.
</s>
