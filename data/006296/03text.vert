<s>
Mramorový	mramorový	k2eAgInSc1d1	mramorový
oblouk	oblouk	k1gInSc1	oblouk
(	(	kIx(	(
<g/>
Marble	Marble	k1gFnSc1	Marble
Arch	arch	k1gInSc1	arch
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oblouk	oblouk	k1gInSc4	oblouk
z	z	k7c2	z
kararskéko	kararskéko	k6eAd1	kararskéko
mramoru	mramor	k1gInSc3	mramor
poblíž	poblíž	k7c2	poblíž
Speakers	Speakersa	k1gFnPc2	Speakersa
<g/>
'	'	kIx"	'
Corner	Corner	k1gMnSc1	Corner
v	v	k7c6	v
Londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Hyde	Hyde	k1gNnSc6	Hyde
Parku	park	k1gInSc2	park
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
obvodu	obvod	k1gInSc6	obvod
Westminster	Westminstra	k1gFnPc2	Westminstra
u	u	k7c2	u
západního	západní	k2eAgInSc2d1	západní
konce	konec	k1gInSc2	konec
Oxford	Oxford	k1gInSc1	Oxford
Street	Street	k1gInSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
návrhu	návrh	k1gInSc2	návrh
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
John	John	k1gMnSc1	John
Nash	Nash	k1gMnSc1	Nash
<g/>
.	.	kIx.	.
</s>
<s>
Inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
se	s	k7c7	s
triumfálním	triumfální	k2eAgInSc7d1	triumfální
Konstantinovým	Konstantinův	k2eAgInSc7d1	Konstantinův
obloukem	oblouk	k1gInSc7	oblouk
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
vztyčen	vztyčit	k5eAaPmNgInS	vztyčit
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Mall	Malla	k1gFnPc2	Malla
jako	jako	k8xS	jako
brána	brána	k1gFnSc1	brána
do	do	k7c2	do
Buckinghamského	buckinghamský	k2eAgInSc2d1	buckinghamský
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
Nash	Nash	k1gInSc1	Nash
původní	původní	k2eAgInSc1d1	původní
Buckingham	Buckingham	k1gInSc1	Buckingham
House	house	k1gNnSc1	house
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
pro	pro	k7c4	pro
Jiřího	Jiří	k1gMnSc4	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byl	být	k5eAaImAgMnS	být
shledán	shledat	k5eAaPmNgMnS	shledat
příliš	příliš	k6eAd1	příliš
úzkým	úzký	k2eAgMnSc7d1	úzký
pro	pro	k7c4	pro
královský	královský	k2eAgInSc4d1	královský
kočár	kočár	k1gInSc4	kočár
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
přemístěn	přemístit	k5eAaPmNgMnS	přemístit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
oblouku	oblouk	k1gInSc2	oblouk
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgFnPc1	tři
malé	malý	k2eAgFnPc1d1	malá
místnosti	místnost	k1gFnPc1	místnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
používány	používán	k2eAgInPc1d1	používán
jako	jako	k8xC	jako
policejní	policejní	k2eAgFnSc1d1	policejní
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
sochy	socha	k1gFnPc1	socha
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
výzdobu	výzdoba	k1gFnSc4	výzdoba
<g/>
,	,	kIx,	,
především	především	k9	především
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
zdobit	zdobit	k5eAaImF	zdobit
původní	původní	k2eAgInSc4d1	původní
hlavní	hlavní	k2eAgInSc4d1	hlavní
vstup	vstup	k1gInSc4	vstup
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c4	na
průčelí	průčelí	k1gNnSc4	průčelí
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Oblouk	oblouk	k1gInSc1	oblouk
stál	stát	k5eAaImAgInS	stát
poblíž	poblíž	k6eAd1	poblíž
tyburnského	tyburnského	k2eAgNnSc4d1	tyburnského
popraviště	popraviště	k1gNnSc4	popraviště
<g/>
,	,	kIx,	,
místa	místo	k1gNnPc1	místo
veřejných	veřejný	k2eAgFnPc2d1	veřejná
poprav	poprava	k1gFnPc2	poprava
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1388	[number]	k4	1388
až	až	k6eAd1	až
1793	[number]	k4	1793
<g/>
.	.	kIx.	.
</s>
<s>
Průchod	průchod	k1gInSc4	průchod
nebo	nebo	k8xC	nebo
průjezd	průjezd	k1gInSc4	průjezd
obloukem	oblouk	k1gInSc7	oblouk
je	být	k5eAaImIp3nS	být
povolen	povolit	k5eAaPmNgInS	povolit
pouze	pouze	k6eAd1	pouze
členům	člen	k1gMnPc3	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
příslušníkům	příslušník	k1gMnPc3	příslušník
královského	královský	k2eAgNnSc2d1	královské
jízdního	jízdní	k2eAgNnSc2d1	jízdní
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
–	–	k?	–
metro	metro	k1gNnSc1	metro
–	–	k?	–
Marble	Marble	k1gFnSc2	Marble
Arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mramorový	mramorový	k2eAgInSc4d1	mramorový
oblouk	oblouk	k1gInSc4	oblouk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
