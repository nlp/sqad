<s>
Afázie	afázie	k1gFnSc1	afázie
(	(	kIx(	(
<g/>
afasie	afasie	k1gFnSc1	afasie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
a-	a-	k?	a-
<g/>
,	,	kIx,	,
záporka	záporka	k1gFnSc1	záporka
<g/>
,	,	kIx,	,
a	a	k8xC	a
fémi	fémi	k6eAd1	fémi
<g/>
,	,	kIx,	,
mluvím	mluvit	k5eAaImIp1nS	mluvit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
nebo	nebo	k8xC	nebo
porucha	porucha	k1gFnSc1	porucha
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
porušením	porušení	k1gNnSc7	porušení
řečových	řečový	k2eAgFnPc2d1	řečová
oblastí	oblast	k1gFnPc2	oblast
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
