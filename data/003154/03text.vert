<s>
Afázie	afázie	k1gFnSc1	afázie
(	(	kIx(	(
<g/>
afasie	afasie	k1gFnSc1	afasie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
a-	a-	k?	a-
<g/>
,	,	kIx,	,
záporka	záporka	k1gFnSc1	záporka
<g/>
,	,	kIx,	,
a	a	k8xC	a
fémi	fémi	k6eAd1	fémi
<g/>
,	,	kIx,	,
mluvím	mluvit	k5eAaImIp1nS	mluvit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
nebo	nebo	k8xC	nebo
porucha	porucha	k1gFnSc1	porucha
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
porušením	porušení	k1gNnSc7	porušení
řečových	řečový	k2eAgFnPc2d1	řečová
oblastí	oblast	k1gFnPc2	oblast
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
následek	následek	k1gInSc1	následek
úrazů	úraz	k1gInPc2	úraz
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
krvácení	krvácení	k1gNnSc2	krvácení
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
nádorů	nádor	k1gInPc2	nádor
<g/>
,	,	kIx,	,
zánětlivých	zánětlivý	k2eAgNnPc2d1	zánětlivé
onemocnění	onemocnění	k1gNnPc2	onemocnění
nebo	nebo	k8xC	nebo
intoxikace	intoxikace	k1gFnSc2	intoxikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
klasifikaci	klasifikace	k1gFnSc6	klasifikace
nemocí	nemoc	k1gFnPc2	nemoc
ICD-10	ICD-10	k1gMnSc1	ICD-10
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
R	R	kA	R
<g/>
47.0	[number]	k4	47.0
<g/>
.	.	kIx.	.
</s>
<s>
Afázie	afázie	k1gFnPc1	afázie
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
různých	různý	k2eAgFnPc2d1	různá
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
neurologie	neurologie	k1gFnSc2	neurologie
<g/>
,	,	kIx,	,
foniatrie	foniatrie	k1gFnSc2	foniatrie
<g/>
,	,	kIx,	,
neuropsychologie	neuropsychologie	k1gFnSc2	neuropsychologie
a	a	k8xC	a
neuropsychiatrie	neuropsychiatrie	k1gFnSc2	neuropsychiatrie
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
I.	I.	kA	I.
kortikální	kortikální	k2eAgInPc1d1	kortikální
(	(	kIx(	(
<g/>
korové	korový	k2eAgFnPc1d1	korová
<g/>
)	)	kIx)	)
afázie	afázie	k1gFnPc1	afázie
subkortikální	subkortikální	k2eAgFnPc1d1	subkortikální
(	(	kIx(	(
<g/>
podkorové	podkorový	k2eAgFnPc1d1	podkorová
<g/>
)	)	kIx)	)
afázie	afázie	k1gFnPc1	afázie
Dělení	dělení	k1gNnSc2	dělení
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Brocova	Brocův	k2eAgFnSc1d1	Brocova
(	(	kIx(	(
<g/>
expresivní	expresivní	k2eAgFnSc1d1	expresivní
<g/>
,	,	kIx,	,
motorická	motorický	k2eAgFnSc1d1	motorická
<g/>
)	)	kIx)	)
afázie	afázie	k1gFnSc1	afázie
-	-	kIx~	-
chápání	chápání	k1gNnSc1	chápání
zachované	zachovaný	k2eAgNnSc1d1	zachované
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
není	být	k5eNaImIp3nS	být
plynulá	plynulý	k2eAgFnSc1d1	plynulá
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
opakování	opakování	k1gNnSc2	opakování
Wernickeho	Wernicke	k1gMnSc2	Wernicke
(	(	kIx(	(
<g/>
percepční	percepční	k2eAgFnSc1d1	percepční
<g/>
,	,	kIx,	,
senzorická	senzorický	k2eAgFnSc1d1	senzorická
<g/>
)	)	kIx)	)
afázie	afázie	k1gFnSc1	afázie
-	-	kIx~	-
špatné	špatný	k2eAgNnSc1d1	špatné
chápání	chápání	k1gNnSc1	chápání
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
plynulá	plynulý	k2eAgFnSc1d1	plynulá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postrádá	postrádat	k5eAaImIp3nS	postrádat
smysluplnost	smysluplnost	k1gFnSc1	smysluplnost
(	(	kIx(	(
<g/>
jelikož	jelikož	k8xS	jelikož
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
vnitřně	vnitřně	k6eAd1	vnitřně
<g />
.	.	kIx.	.
</s>
<s>
kontrolována	kontrolován	k2eAgFnSc1d1	kontrolována
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
opakování	opakování	k1gNnSc2	opakování
kondukční	kondukční	k2eAgFnSc2d1	kondukční
afázie	afázie	k1gFnSc2	afázie
-	-	kIx~	-
ztráta	ztráta	k1gFnSc1	ztráta
opakování	opakování	k1gNnSc1	opakování
<g/>
,	,	kIx,	,
zachované	zachovaný	k2eAgNnSc1d1	zachované
chápání	chápání	k1gNnSc1	chápání
a	a	k8xC	a
tvorba	tvorba	k1gFnSc1	tvorba
řeči	řeč	k1gFnSc2	řeč
transkortikální	transkortikální	k2eAgFnSc1d1	transkortikální
senzorická	senzorický	k2eAgFnSc1d1	senzorická
afázie	afázie	k1gFnSc1	afázie
-	-	kIx~	-
špatné	špatný	k2eAgNnSc1d1	špatné
chápání	chápání	k1gNnSc1	chápání
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
plynulá	plynulý	k2eAgFnSc1d1	plynulá
<g/>
,	,	kIx,	,
opakování	opakování	k1gNnSc1	opakování
zachované	zachovaný	k2eAgInPc1d1	zachovaný
transkortikální	transkortikální	k2eAgFnSc1d1	transkortikální
motorická	motorický	k2eAgFnSc1d1	motorická
afázie	afázie	k1gFnSc1	afázie
-	-	kIx~	-
chápání	chápání	k1gNnSc1	chápání
zachované	zachovaný	k2eAgNnSc1d1	zachované
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
není	být	k5eNaImIp3nS	být
plynulá	plynulý	k2eAgFnSc1d1	plynulá
<g/>
,	,	kIx,	,
opakování	opakování	k1gNnSc1	opakování
zachované	zachovaný	k2eAgNnSc1d1	zachované
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
dělení	dělení	k1gNnSc1	dělení
afázie	afázie	k1gFnSc2	afázie
na	na	k7c6	na
expresivní	expresivní	k2eAgFnSc6d1	expresivní
a	a	k8xC	a
senzorické	senzorický	k2eAgFnSc6d1	senzorická
(	(	kIx(	(
<g/>
receptivní	receptivní	k2eAgFnSc6d1	receptivní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
dělení	dělení	k1gNnSc4	dělení
na	na	k7c4	na
afasie	afasie	k1gFnPc4	afasie
neplynulé	plynulý	k2eNgFnPc4d1	neplynulá
(	(	kIx(	(
<g/>
přední	přední	k2eAgFnPc4d1	přední
<g/>
,	,	kIx,	,
nonfluent	nonfluent	k1gInSc1	nonfluent
<g/>
)	)	kIx)	)
a	a	k8xC	a
plynulé	plynulý	k2eAgFnSc2d1	plynulá
(	(	kIx(	(
<g/>
zadní	zadní	k2eAgFnSc2d1	zadní
<g/>
,	,	kIx,	,
fluent	fluent	k1gMnSc1	fluent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
je	být	k5eAaImIp3nS	být
pacientův	pacientův	k2eAgInSc1d1	pacientův
rodný	rodný	k2eAgInSc1d1	rodný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
ujistit	ujistit	k5eAaPmF	ujistit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
netrpí	trpět	k5eNaImIp3nP	trpět
nějakou	nějaký	k3yIgFnSc7	nějaký
poruchou	porucha	k1gFnSc7	porucha
sluchu	sluch	k1gInSc2	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
otestovat	otestovat	k5eAaPmF	otestovat
pacientovo	pacientův	k2eAgNnSc4d1	pacientovo
chápání	chápání	k1gNnSc4	chápání
a	a	k8xC	a
orientaci	orientace	k1gFnSc4	orientace
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
místem	místo	k1gNnSc7	místo
a	a	k8xC	a
časem	časem	k6eAd1	časem
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
otázkami	otázka	k1gFnPc7	otázka
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
jmenujete	jmenovat	k5eAaBmIp2nP	jmenovat
<g/>
?	?	kIx.	?
</s>
<s>
Kolik	kolik	k4yIc1	kolik
je	být	k5eAaImIp3nS	být
vám	vy	k3xPp2nPc3	vy
let	léto	k1gNnPc2	léto
<g/>
?	?	kIx.	?
</s>
<s>
Víte	vědět	k5eAaImIp2nP	vědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
teď	teď	k6eAd1	teď
jste	být	k5eAaImIp2nP	být
<g/>
?	?	kIx.	?
</s>
<s>
Jaké	jaký	k3yQgNnSc1	jaký
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
datum	datum	k1gNnSc1	datum
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
rok	rok	k1gInSc1	rok
<g/>
)	)	kIx)	)
<g/>
?	?	kIx.	?
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
nechat	nechat	k5eAaPmF	nechat
pacienta	pacient	k1gMnSc4	pacient
vyprávět	vyprávět	k5eAaImF	vyprávět
o	o	k7c6	o
jeho	jeho	k3xOp3gInPc6	jeho
problémech	problém	k1gInPc6	problém
a	a	k8xC	a
pozorně	pozorně	k6eAd1	pozorně
si	se	k3xPyFc3	se
všímat	všímat	k5eAaImF	všímat
jeho	jeho	k3xOp3gNnSc4	jeho
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
<g/>
,	,	kIx,	,
plynulosti	plynulost	k1gFnPc4	plynulost
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
užívání	užívání	k1gNnSc1	užívání
slov	slovo	k1gNnPc2	slovo
atd.	atd.	kA	atd.
Existují	existovat	k5eAaImIp3nP	existovat
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
testy	test	k1gInPc1	test
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
posoudit	posoudit	k5eAaPmF	posoudit
schopnost	schopnost	k1gFnSc4	schopnost
nacházet	nacházet	k5eAaImF	nacházet
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
test	test	k1gInSc1	test
slovní	slovní	k2eAgFnSc2d1	slovní
fluence	fluence	k1gFnSc2	fluence
(	(	kIx(	(
<g/>
pacient	pacient	k1gMnSc1	pacient
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vyjmenovat	vyjmenovat	k5eAaPmF	vyjmenovat
všechna	všechen	k3xTgNnPc4	všechen
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
na	na	k7c6	na
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
vzpomene	vzpomenout	k5eAaPmIp3nS	vzpomenout
-	-	kIx~	-
norma	norma	k1gFnSc1	norma
<g/>
:	:	kIx,	:
18-22	[number]	k4	18-22
za	za	k7c4	za
1	[number]	k4	1
minutu	minuta	k1gFnSc4	minuta
<g/>
;	;	kIx,	;
případně	případně	k6eAd1	případně
vyjmenovat	vyjmenovat	k5eAaPmF	vyjmenovat
všechna	všechen	k3xTgNnPc4	všechen
slova	slovo	k1gNnPc4	slovo
začínající	začínající	k2eAgNnPc4d1	začínající
na	na	k7c6	na
určité	určitý	k2eAgNnSc4d1	určité
písmeno	písmeno	k1gNnSc4	písmeno
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
schopnost	schopnost	k1gFnSc4	schopnost
opakovat	opakovat	k5eAaImF	opakovat
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
několikaslovnou	několikaslovný	k2eAgFnSc4d1	několikaslovná
větu	věta	k1gFnSc4	věta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
afasie	afasie	k1gFnPc4	afasie
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
pacient	pacient	k1gMnSc1	pacient
pravo-	pravo-	k?	pravo-
nebo	nebo	k8xC	nebo
levoruký	levoruký	k2eAgMnSc1d1	levoruký
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
centrum	centrum	k1gNnSc1	centrum
řeči	řeč	k1gFnSc2	řeč
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
dominantní	dominantní	k2eAgFnSc6d1	dominantní
hemisféře	hemisféra	k1gFnSc6	hemisféra
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
praváků	pravák	k1gMnPc2	pravák
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgFnSc1d1	dominantní
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
levá	levý	k2eAgFnSc1d1	levá
hemisféra	hemisféra	k1gFnSc1	hemisféra
<g/>
,	,	kIx,	,
u	u	k7c2	u
leváků	levák	k1gMnPc2	levák
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
70	[number]	k4	70
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Brocova	Brocův	k2eAgFnSc1d1	Brocova
(	(	kIx(	(
<g/>
motorická	motorický	k2eAgFnSc1d1	motorická
<g/>
)	)	kIx)	)
afázie	afázie	k1gFnSc1	afázie
je	být	k5eAaImIp3nS	být
neplynulá	plynulý	k2eNgFnSc1d1	neplynulá
<g/>
,	,	kIx,	,
pacienti	pacient	k1gMnPc1	pacient
říkají	říkat	k5eAaImIp3nP	říkat
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
slov	slovo	k1gNnPc2	slovo
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
přestávky	přestávka	k1gFnPc4	přestávka
v	v	k7c6	v
řeči	řeč	k1gFnSc6	řeč
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgInPc1d2	delší
než	než	k8xS	než
doba	doba	k1gFnSc1	doba
po	po	k7c4	po
kterou	který	k3yRgFnSc4	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
<g/>
.	.	kIx.	.
</s>
<s>
Řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
namáhavá	namáhavý	k2eAgFnSc1d1	namáhavá
s	s	k7c7	s
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
přízvukem	přízvuk	k1gInSc7	přízvuk
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
příznakem	příznak	k1gInSc7	příznak
je	být	k5eAaImIp3nS	být
agramatismus	agramatismus	k1gInSc1	agramatismus
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
neschopnost	neschopnost	k1gFnSc1	neschopnost
tvorby	tvorba	k1gFnSc2	tvorba
vět	věta	k1gFnPc2	věta
podle	podle	k7c2	podle
gramatických	gramatický	k2eAgNnPc2d1	gramatické
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
chybné	chybný	k2eAgNnSc1d1	chybné
používání	používání	k1gNnSc1	používání
morfémů	morfém	k1gInPc2	morfém
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
spojky	spojka	k1gFnPc1	spojka
<g/>
,	,	kIx,	,
přípony	přípona	k1gFnPc1	přípona
<g/>
,	,	kIx,	,
pomocná	pomocný	k2eAgNnPc1d1	pomocné
slovesa	sloveso	k1gNnPc1	sloveso
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pacienti	pacient	k1gMnPc1	pacient
obtížně	obtížně	k6eAd1	obtížně
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
znělé	znělý	k2eAgInPc1d1	znělý
a	a	k8xC	a
neznělé	znělý	k2eNgInPc1d1	neznělý
hlásky	hlásek	k1gInPc1	hlásek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
B	B	kA	B
a	a	k8xC	a
P	P	kA	P
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
T.	T.	kA	T.
Mluvené	mluvený	k2eAgFnSc2d1	mluvená
i	i	k8xC	i
čtené	čtený	k2eAgFnSc2d1	čtená
řeči	řeč	k1gFnSc2	řeč
rozumějí	rozumět	k5eAaImIp3nP	rozumět
<g/>
,	,	kIx,	,
úroveň	úroveň	k1gFnSc1	úroveň
psaní	psaní	k1gNnSc2	psaní
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
jejich	jejich	k3xOp3gFnSc3	jejich
úrovni	úroveň	k1gFnSc3	úroveň
mluvené	mluvený	k2eAgFnSc2d1	mluvená
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Motorickou	motorický	k2eAgFnSc4d1	motorická
afázii	afázie	k1gFnSc4	afázie
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
neurolog	neurolog	k1gMnSc1	neurolog
Paul	Paul	k1gMnSc1	Paul
Broca	Broca	k1gMnSc1	Broca
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
pitval	pitvat	k5eAaImAgMnS	pitvat
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
rozuměl	rozumět	k5eAaImAgMnS	rozumět
mluvenému	mluvený	k2eAgNnSc3d1	mluvené
slovu	slovo	k1gNnSc3	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odpovídat	odpovídat	k5eAaImF	odpovídat
dokázal	dokázat	k5eAaPmAgInS	dokázat
pouze	pouze	k6eAd1	pouze
gesty	gest	k1gInPc7	gest
a	a	k8xC	a
nesmyslnými	smyslný	k2eNgInPc7d1	nesmyslný
zvuky	zvuk	k1gInPc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
se	se	k3xPyFc4	se
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
poškození	poškození	k1gNnSc1	poškození
dolní	dolní	k2eAgFnSc2d1	dolní
levé	levý	k2eAgFnSc2d1	levá
oblasti	oblast	k1gFnSc2	oblast
čelního	čelní	k2eAgInSc2d1	čelní
laloku	lalok	k1gInSc2	lalok
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
tedy	tedy	k9	tedy
Brocova	Brocův	k2eAgFnSc1d1	Brocova
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Topograficko-anatomická	topografickonatomický	k2eAgFnSc1d1	topograficko-anatomický
poškozená	poškozený	k2eAgFnSc1d1	poškozená
oblast	oblast	k1gFnSc1	oblast
<g/>
:	:	kIx,	:
Brocova	Brocův	k2eAgFnSc1d1	Brocova
oblast	oblast	k1gFnSc1	oblast
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
anatomicky	anatomicky	k6eAd1	anatomicky
Brodmanově	Brodmanův	k2eAgFnSc6d1	Brodmanův
aree	aree	k1gFnSc6	aree
44	[number]	k4	44
a	a	k8xC	a
45	[number]	k4	45
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c4	v
gyrus	gyrus	k1gInSc4	gyrus
inferior	inferior	k1gInSc4	inferior
frontálního	frontální	k2eAgInSc2d1	frontální
laloku	lalok	k1gInSc2	lalok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ohraničeném	ohraničený	k2eAgNnSc6d1	ohraničené
poškození	poškození	k1gNnSc6	poškození
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
mírná	mírný	k2eAgFnSc1d1	mírná
či	či	k8xC	či
přechodná	přechodný	k2eAgFnSc1d1	přechodná
afázie	afázie	k1gFnSc1	afázie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Brocovu	Brocův	k2eAgFnSc4d1	Brocova
afázii	afázie	k1gFnSc4	afázie
<g/>
!	!	kIx.	!
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
Brocova	Brocův	k2eAgFnSc1d1	Brocova
afázie	afázie	k1gFnSc1	afázie
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
při	při	k7c6	při
poškození	poškození	k1gNnSc6	poškození
dvou	dva	k4xCgFnPc2	dva
struktur	struktura	k1gFnPc2	struktura
-	-	kIx~	-
mediálních	mediální	k2eAgFnPc2d1	mediální
a	a	k8xC	a
rostrálních	rostrální	k2eAgFnPc2d1	rostrální
částí	část	k1gFnPc2	část
fasciculus	fasciculus	k1gMnSc1	fasciculus
subcallosus	subcallosus	k1gMnSc1	subcallosus
a	a	k8xC	a
periventrikulární	periventrikulární	k2eAgMnPc1d1	periventrikulární
(	(	kIx(	(
<g/>
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
mozkových	mozkový	k2eAgFnPc2d1	mozková
komor	komora	k1gFnPc2	komora
<g/>
)	)	kIx)	)
bílé	bílý	k2eAgFnSc2d1	bílá
hmoty	hmota	k1gFnSc2	hmota
uložené	uložený	k2eAgInPc1d1	uložený
pod	pod	k7c7	pod
korovými	korový	k2eAgFnPc7d1	korová
motorickými	motorický	k2eAgFnPc7d1	motorická
a	a	k8xC	a
senzorickými	senzorický	k2eAgFnPc7d1	senzorická
oblastmi	oblast	k1gFnPc7	oblast
inervujícími	inervující	k2eAgFnPc7d1	inervující
ústa	ústa	k1gNnPc1	ústa
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
pod	pod	k7c7	pod
"	"	kIx"	"
<g/>
patkou	patka	k1gFnSc7	patka
<g/>
"	"	kIx"	"
pre-	pre-	k?	pre-
a	a	k8xC	a
post-	post-	k?	post-
centrálního	centrální	k2eAgInSc2d1	centrální
závitu	závit	k1gInSc2	závit
vlevo	vlevo	k6eAd1	vlevo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
vláken	vlákna	k1gFnPc2	vlákna
(	(	kIx(	(
<g/>
bílé	bílý	k2eAgFnSc2d1	bílá
hmoty	hmota	k1gFnSc2	hmota
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
a	a	k8xC	a
započetí	započetí	k1gNnSc6	započetí
řečových	řečový	k2eAgInPc2d1	řečový
pohybů	pohyb	k1gInPc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc4	poškození
jen	jen	k6eAd1	jen
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
motorického	motorický	k2eAgMnSc2d1	motorický
nebo	nebo	k8xC	nebo
senzorického	senzorický	k2eAgMnSc2d1	senzorický
<g/>
)	)	kIx)	)
k	k	k7c3	k
trvalému	trvalý	k2eAgNnSc3d1	trvalé
poškození	poškození	k1gNnSc3	poškození
nevede	vést	k5eNaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Wernickeova	Wernickeův	k2eAgFnSc1d1	Wernickeova
(	(	kIx(	(
<g/>
senzorická	senzorický	k2eAgFnSc1d1	senzorická
<g/>
)	)	kIx)	)
afázie	afázie	k1gFnSc1	afázie
je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
plynulou	plynulý	k2eAgFnSc7d1	plynulá
řečí	řeč	k1gFnSc7	řeč
<g/>
,	,	kIx,	,
špatným	špatný	k2eAgNnSc7d1	špatné
pojmenováváním	pojmenovávání	k1gNnSc7	pojmenovávání
<g/>
,	,	kIx,	,
chápáním	chápání	k1gNnSc7	chápání
i	i	k8xC	i
opakováním	opakování	k1gNnSc7	opakování
<g/>
.	.	kIx.	.
</s>
<s>
Řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
vyslovována	vyslovovat	k5eAaImNgFnS	vyslovovat
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
věty	věta	k1gFnPc1	věta
jsou	být	k5eAaImIp3nP	být
přiměřeně	přiměřeně	k6eAd1	přiměřeně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
rytmické	rytmický	k2eAgFnPc1d1	rytmická
a	a	k8xC	a
melodické	melodický	k2eAgFnPc1d1	melodická
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
však	však	k9	však
zcela	zcela	k6eAd1	zcela
nesmyslné	smyslný	k2eNgFnPc4d1	nesmyslná
slabiky	slabika	k1gFnPc4	slabika
či	či	k8xC	či
slova	slovo	k1gNnPc4	slovo
(	(	kIx(	(
<g/>
parafázie	parafázie	k1gFnSc1	parafázie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
řeč	řeč	k1gFnSc1	řeč
někdy	někdy	k6eAd1	někdy
nesrozumitelná	srozumitelný	k2eNgFnSc1d1	nesrozumitelná
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
psaní	psaní	k1gNnSc2	psaní
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
úrovni	úroveň	k1gFnSc3	úroveň
mluvené	mluvený	k2eAgFnSc3d1	mluvená
řeči	řeč	k1gFnSc3	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
objevením	objevení	k1gNnSc7	objevení
oblasti	oblast	k1gFnSc2	oblast
obstarávající	obstarávající	k2eAgNnSc4d1	obstarávající
porozumění	porozumění	k1gNnSc4	porozumění
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
profesor	profesor	k1gMnSc1	profesor
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
Carl	Carl	k1gMnSc1	Carl
Wernicke	Wernicke	k1gFnSc1	Wernicke
<g/>
.	.	kIx.	.
</s>
<s>
Topograficko-anatomická	topografickonatomický	k2eAgFnSc1d1	topograficko-anatomický
poškozená	poškozený	k2eAgFnSc1d1	poškozená
oblast	oblast	k1gFnSc1	oblast
<g/>
:	:	kIx,	:
Od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
Wernickeovy	Wernickeův	k2eAgFnSc2d1	Wernickeova
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
Wernickeova	Wernickeův	k2eAgFnSc1d1	Wernickeova
oblast	oblast	k1gFnSc1	oblast
zakreslována	zakreslován	k2eAgFnSc1d1	zakreslován
v	v	k7c6	v
učebnicích	učebnice	k1gFnPc6	učebnice
neurčitě	určitě	k6eNd1	určitě
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vyléčili	vyléčit	k5eAaPmAgMnP	vyléčit
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
afasie	afasie	k1gFnSc2	afasie
a	a	k8xC	a
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
prováděna	provádět	k5eAaImNgNnP	provádět
vyšetření	vyšetření	k1gNnSc1	vyšetření
CT	CT	kA	CT
mozku	mozek	k1gInSc2	mozek
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wernickeova	Wernickeův	k2eAgFnSc1d1	Wernickeova
oblast	oblast	k1gFnSc1	oblast
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zadním	zadní	k2eAgInSc7d1	zadní
dvěma	dva	k4xCgFnPc7	dva
třetinám	třetina	k1gFnPc3	třetina
gyrus	gyrus	k1gMnSc1	gyrus
temporalis	temporalis	k1gFnPc2	temporalis
superior	superior	k1gMnSc1	superior
inferior	inferior	k1gMnSc1	inferior
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgFnPc1d1	spodní
části	část	k1gFnPc1	část
horního	horní	k2eAgInSc2d1	horní
spánkového	spánkový	k2eAgInSc2d1	spánkový
závitu	závit	k1gInSc2	závit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
zničena	zničit	k5eAaPmNgFnS	zničit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
k	k	k7c3	k
vyléčení	vyléčení	k1gNnSc3	vyléčení
nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Konduktivní	Konduktivní	k2eAgFnSc1d1	Konduktivní
(	(	kIx(	(
<g/>
kondukční	kondukční	k2eAgFnSc1d1	kondukční
<g/>
)	)	kIx)	)
afázie	afázie	k1gFnSc1	afázie
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
dobře	dobře	k6eAd1	dobře
zachovanou	zachovaný	k2eAgFnSc4d1	zachovaná
mluvenou	mluvený	k2eAgFnSc4d1	mluvená
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
pacient	pacient	k1gMnSc1	pacient
rozumí	rozumět	k5eAaImIp3nS	rozumět
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
rozumí	rozumět	k5eAaImIp3nS	rozumět
řeči	řeč	k1gFnSc3	řeč
psané	psaný	k2eAgFnSc3d1	psaná
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
schopen	schopen	k2eAgMnSc1d1	schopen
dobře	dobře	k6eAd1	dobře
opakovat	opakovat	k5eAaImF	opakovat
řeč	řeč	k1gFnSc4	řeč
slyšenou	slyšený	k2eAgFnSc4d1	slyšená
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
víceslabičná	víceslabičný	k2eAgNnPc4d1	víceslabičné
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Psaní	psaní	k1gNnSc1	psaní
je	být	k5eAaImIp3nS	být
úměrné	úměrný	k2eAgFnSc3d1	úměrná
mluvě	mluva	k1gFnSc3	mluva
<g/>
.	.	kIx.	.
</s>
<s>
Topograficko-anatomická	topografickonatomický	k2eAgFnSc1d1	topograficko-anatomický
poškozená	poškozený	k2eAgFnSc1d1	poškozená
oblast	oblast	k1gFnSc1	oblast
<g/>
:	:	kIx,	:
Léze	léze	k1gFnSc1	léze
fasciculus	fasciculus	k1gMnSc1	fasciculus
arcuatus	arcuatus	k1gMnSc1	arcuatus
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc1	poškození
levého	levý	k2eAgInSc2d1	levý
supramarginálního	supramarginální	k2eAgInSc2d1	supramarginální
závitu	závit	k1gInSc2	závit
(	(	kIx(	(
<g/>
Brodmanova	Brodmanův	k2eAgFnSc1d1	Brodmanův
area	area	k1gFnSc1	area
40	[number]	k4	40
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doprovázeno	doprovázen	k2eAgNnSc1d1	doprovázeno
poškozením	poškození	k1gNnSc7	poškození
insuly	insula	k1gFnSc2	insula
a	a	k8xC	a
primární	primární	k2eAgFnSc2d1	primární
sluchové	sluchový	k2eAgFnSc2d1	sluchová
kůry	kůra	k1gFnSc2	kůra
(	(	kIx(	(
<g/>
Brodmanova	Brodmanův	k2eAgFnSc1d1	Brodmanův
area	area	k1gFnSc1	area
41,42	[number]	k4	41,42
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Transkortikální	Transkortikální	k2eAgFnSc1d1	Transkortikální
motorická	motorický	k2eAgFnSc1d1	motorická
afázie	afázie	k1gFnSc1	afázie
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
Brockově	Brockův	k2eAgFnSc3d1	Brockova
afázii	afázie	k1gFnSc3	afázie
malým	malý	k2eAgInPc3d1	malý
počtem	počet	k1gInSc7	počet
slov	slovo	k1gNnPc2	slovo
za	za	k7c4	za
delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
a	a	k8xC	a
také	také	k9	také
omezením	omezení	k1gNnSc7	omezení
složitosti	složitost	k1gFnSc2	složitost
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Pacienti	pacient	k1gMnPc1	pacient
však	však	k9	však
dobře	dobře	k6eAd1	dobře
opakují	opakovat	k5eAaImIp3nP	opakovat
<g/>
,	,	kIx,	,
čtou	číst	k5eAaImIp3nP	číst
nahlas	nahlas	k6eAd1	nahlas
a	a	k8xC	a
pojmenovávají	pojmenovávat	k5eAaImIp3nP	pojmenovávat
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Slušně	slušně	k6eAd1	slušně
rozumějí	rozumět	k5eAaImIp3nP	rozumět
mluvené	mluvený	k2eAgFnPc4d1	mluvená
a	a	k8xC	a
čtené	čtený	k2eAgFnPc4d1	čtená
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Topograficko-anatomická	topografickonatomický	k2eAgFnSc1d1	topograficko-anatomický
poškozená	poškozený	k2eAgFnSc1d1	poškozená
oblast	oblast	k1gFnSc1	oblast
<g/>
:	:	kIx,	:
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
inkompletní	inkompletní	k2eAgFnSc4d1	inkompletní
lézi	léze	k1gFnSc4	léze
v	v	k7c6	v
Brocově	Brocův	k2eAgFnSc6d1	Brocova
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
afázii	afázie	k1gFnSc3	afázie
tedy	tedy	k9	tedy
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
poškození	poškození	k1gNnSc4	poškození
kůry	kůra	k1gFnSc2	kůra
či	či	k8xC	či
bílé	bílý	k2eAgFnSc2d1	bílá
hmoty	hmota	k1gFnSc2	hmota
nad	nad	k7c7	nad
Brocovou	Brocův	k2eAgFnSc7d1	Brocova
oblastí	oblast	k1gFnSc7	oblast
a	a	k8xC	a
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
poškozena	poškozen	k2eAgFnSc1d1	poškozena
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Transkortikální	Transkortikální	k2eAgFnSc1d1	Transkortikální
senzorická	senzorický	k2eAgFnSc1d1	senzorická
afázie	afázie	k1gFnSc1	afázie
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
plynulou	plynulý	k2eAgFnSc7d1	plynulá
řečí	řeč	k1gFnSc7	řeč
<g/>
,	,	kIx,	,
dobrým	dobrý	k2eAgNnSc7d1	dobré
opakováním	opakování	k1gNnSc7	opakování
slyšené	slyšený	k2eAgFnSc2d1	slyšená
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spontánní	spontánní	k2eAgFnSc6d1	spontánní
řeči	řeč	k1gFnSc6	řeč
se	se	k3xPyFc4	se
však	však	k9	však
objevují	objevovat	k5eAaImIp3nP	objevovat
parafázie	parafázie	k1gFnPc1	parafázie
a	a	k8xC	a
potíže	potíž	k1gFnPc1	potíž
s	s	k7c7	s
pojmenováním	pojmenování	k1gNnSc7	pojmenování
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Slyšená	slyšený	k2eAgFnSc1d1	slyšená
řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
chápána	chápat	k5eAaImNgFnS	chápat
s	s	k7c7	s
poruchami	porucha	k1gFnPc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Pacienti	pacient	k1gMnPc1	pacient
můžou	můžou	k?	můžou
číst	číst	k5eAaImF	číst
nahlas	nahlas	k6eAd1	nahlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
špatně	špatně	k6eAd1	špatně
chápou	chápat	k5eAaImIp3nP	chápat
co	co	k3yInSc4	co
čtou	číst	k5eAaImIp3nP	číst
<g/>
.	.	kIx.	.
</s>
<s>
Psaní	psaní	k1gNnSc1	psaní
je	být	k5eAaImIp3nS	být
horší	zlý	k2eAgFnSc1d2	horší
než	než	k8xS	než
spontánní	spontánní	k2eAgFnSc1d1	spontánní
řeč	řeč	k1gFnSc1	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Topograficko-anatomická	topografickonatomický	k2eAgFnSc1d1	topograficko-anatomický
poškozená	poškozený	k2eAgFnSc1d1	poškozená
oblast	oblast	k1gFnSc1	oblast
<g/>
:	:	kIx,	:
Temenní	temenní	k2eAgFnSc1d1	temenní
a	a	k8xC	a
spánková	spánkový	k2eAgFnSc1d1	spánková
kůra	kůra	k1gFnSc1	kůra
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Wernickeovy	Wernickeův	k2eAgFnSc2d1	Wernickeova
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Anatomická	anatomický	k2eAgFnSc1d1	anatomická
(	(	kIx(	(
<g/>
anomická	anomický	k2eAgFnSc1d1	anomická
<g/>
)	)	kIx)	)
afázie	afázie	k1gFnSc1	afázie
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
všech	všecek	k3xTgFnPc2	všecek
afázií	afázie	k1gFnPc2	afázie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
rekonvalescenci	rekonvalescence	k1gFnSc6	rekonvalescence
afázie	afázie	k1gFnSc2	afázie
Wernickeovy	Wernickeův	k2eAgFnSc2d1	Wernickeova
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
neschopností	neschopnost	k1gFnSc7	neschopnost
pojmenovávat	pojmenovávat	k5eAaImF	pojmenovávat
předvedený	předvedený	k2eAgInSc4d1	předvedený
předmět	předmět	k1gInSc4	předmět
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
neschopností	neschopnost	k1gFnSc7	neschopnost
pojmenovávat	pojmenovávat	k5eAaImF	pojmenovávat
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
děje	dít	k5eAaImIp3nS	dít
během	během	k7c2	během
spontánní	spontánní	k2eAgFnSc2d1	spontánní
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Porozumění	porozumění	k1gNnSc1	porozumění
slyšené	slyšený	k2eAgFnSc2d1	slyšená
i	i	k8xC	i
čtené	čtený	k2eAgFnSc2d1	čtená
řeči	řeč	k1gFnSc2	řeč
je	být	k5eAaImIp3nS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
čtení	čtení	k1gNnSc4	čtení
nahlas	nahlas	k6eAd1	nahlas
a	a	k8xC	a
opakování	opakování	k1gNnSc4	opakování
<g/>
.	.	kIx.	.
</s>
<s>
Plynulou	plynulý	k2eAgFnSc4d1	plynulá
mluvenou	mluvený	k2eAgFnSc4d1	mluvená
řeč	řeč	k1gFnSc4	řeč
přerušuje	přerušovat	k5eAaImIp3nS	přerušovat
hledání	hledání	k1gNnSc4	hledání
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Topograficko-anatomická	topografickonatomický	k2eAgFnSc1d1	topograficko-anatomický
poškozená	poškozený	k2eAgFnSc1d1	poškozená
oblast	oblast	k1gFnSc1	oblast
<g/>
:	:	kIx,	:
Poškození	poškození	k1gNnSc1	poškození
řečových	řečový	k2eAgFnPc2d1	řečová
oblastí	oblast	k1gFnPc2	oblast
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
je	být	k5eAaImIp3nS	být
poškozen	poškodit	k5eAaPmNgInS	poškodit
levostranný	levostranný	k2eAgInSc1d1	levostranný
střední	střední	k2eAgInSc1d1	střední
spánkový	spánkový	k2eAgInSc1d1	spánkový
závit	závit	k1gInSc1	závit
a	a	k8xC	a
gyrus	gyrus	k1gInSc1	gyrus
angularis	angularis	k1gFnSc2	angularis
<g/>
.	.	kIx.	.
</s>
<s>
Globální	globální	k2eAgFnSc1d1	globální
afasie	afasie	k1gFnSc1	afasie
se	se	k3xPyFc4	se
projektuje	projektovat	k5eAaBmIp3nS	projektovat
téměř	téměř	k6eAd1	téměř
úplnou	úplný	k2eAgFnSc7d1	úplná
ztrátou	ztráta	k1gFnSc7	ztráta
tvorby	tvorba	k1gFnSc2	tvorba
řeči	řeč	k1gFnSc2	řeč
nebo	nebo	k8xC	nebo
chápání	chápání	k1gNnSc2	chápání
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
něco	něco	k3yInSc4	něco
řečí	řečit	k5eAaImIp3nS	řečit
sdělit	sdělit	k5eAaPmF	sdělit
je	on	k3xPp3gFnPc4	on
minimální	minimální	k2eAgFnPc4d1	minimální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
automatismy	automatismus	k1gInPc4	automatismus
jako	jako	k8xS	jako
například	například	k6eAd1	například
kletby	kletba	k1gFnPc1	kletba
<g/>
,	,	kIx,	,
fungují	fungovat	k5eAaImIp3nP	fungovat
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
doprovázená	doprovázený	k2eAgFnSc1d1	doprovázená
pravostranným	pravostranný	k2eAgNnSc7d1	pravostranné
ochrnutím	ochrnutí	k1gNnSc7	ochrnutí
tváře	tvář	k1gFnSc2	tvář
a	a	k8xC	a
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Topograficko-anatomická	topografickonatomický	k2eAgFnSc1d1	topograficko-anatomický
poškozená	poškozený	k2eAgFnSc1d1	poškozená
oblast	oblast	k1gFnSc1	oblast
<g/>
:	:	kIx,	:
Léze	léze	k1gFnSc1	léze
v	v	k7c6	v
dominantní	dominantní	k2eAgFnSc6d1	dominantní
hemisféře	hemisféra	k1gFnSc6	hemisféra
<g/>
,	,	kIx,	,
zasahující	zasahující	k2eAgNnSc4d1	zasahující
Brocovo	Brocův	k2eAgNnSc4d1	Brocovo
i	i	k8xC	i
Wernickeho	Wernicke	k1gMnSc2	Wernicke
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
prosté	prostý	k2eAgNnSc1d1	prosté
členění	členění	k1gNnSc1	členění
na	na	k7c4	na
afázie	afázie	k1gFnPc4	afázie
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
poškozením	poškození	k1gNnSc7	poškození
thalamu	thalamus	k1gInSc2	thalamus
<g/>
,	,	kIx,	,
neostriata	neostriata	k1gFnSc1	neostriata
či	či	k8xC	či
capsula	capsula	k1gFnSc1	capsula
interna	interna	k1gFnSc1	interna
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
<g/>
.	.	kIx.	.
</s>
<s>
Thalamickou	thalamický	k2eAgFnSc4d1	thalamický
afasii	afasie	k1gFnSc4	afasie
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
jak	jak	k8xC	jak
jeho	jeho	k3xOp3gFnSc1	jeho
destrukce	destrukce	k1gFnSc1	destrukce
nebo	nebo	k8xC	nebo
diskonekce	diskonekce	k1gFnSc1	diskonekce
(	(	kIx(	(
<g/>
špatné	špatný	k2eAgNnSc1d1	špatné
zapojení	zapojení	k1gNnSc1	zapojení
<g/>
)	)	kIx)	)
jeho	jeho	k3xOp3gNnPc2	jeho
jader	jádro	k1gNnPc2	jádro
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
součástí	součást	k1gFnSc7	součást
většího	veliký	k2eAgInSc2d2	veliký
syndromu	syndrom	k1gInSc2	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Zkřížené	zkřížený	k2eAgFnPc1d1	zkřížená
afázie	afázie	k1gFnPc1	afázie
-	-	kIx~	-
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
při	pře	k1gFnSc3	pře
ložiskových	ložiskový	k2eAgNnPc2d1	ložiskové
poškození	poškození	k1gNnPc2	poškození
pravé	pravá	k1gFnSc2	pravá
hemisféry	hemisféra	k1gFnSc2	hemisféra
u	u	k7c2	u
praváků	pravák	k1gMnPc2	pravák
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
třetinách	třetina	k1gFnPc6	třetina
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
klinický	klinický	k2eAgInSc1d1	klinický
obraz	obraz	k1gInSc1	obraz
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k9	jako
u	u	k7c2	u
afázií	afázie	k1gFnPc2	afázie
klasických	klasický	k2eAgFnPc2d1	klasická
<g/>
.	.	kIx.	.
</s>
<s>
Aprosodie	Aprosodie	k1gFnSc1	Aprosodie
-	-	kIx~	-
Jsou	být	k5eAaImIp3nP	být
poškozením	poškození	k1gNnSc7	poškození
prosodie	prosodie	k1gFnSc2	prosodie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
využitím	využití	k1gNnSc7	využití
řečové	řečový	k2eAgFnSc2d1	řečová
melodie	melodie	k1gFnSc2	melodie
<g/>
,	,	kIx,	,
časových	časový	k2eAgFnPc2d1	časová
následností	následnost	k1gFnPc2	následnost
<g/>
,	,	kIx,	,
pomlk	pomlk	k1gInSc1	pomlk
<g/>
,	,	kIx,	,
různého	různý	k2eAgInSc2d1	různý
stupně	stupeň	k1gInSc2	stupeň
hlasitosti	hlasitost	k1gFnSc2	hlasitost
a	a	k8xC	a
zabarvení	zabarvení	k1gNnSc2	zabarvení
hlasu	hlas	k1gInSc2	hlas
ke	k	k7c3	k
sdělení	sdělení	k1gNnSc3	sdělení
citového	citový	k2eAgInSc2d1	citový
obsahu	obsah	k1gInSc2	obsah
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
postojů	postoj	k1gInPc2	postoj
mluvícího	mluvící	k2eAgMnSc2d1	mluvící
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Získaná	získaný	k2eAgFnSc1d1	získaná
afázie	afázie	k1gFnSc1	afázie
s	s	k7c7	s
epilepsií	epilepsie	k1gFnSc7	epilepsie
(	(	kIx(	(
<g/>
Landau	Landaus	k1gInSc2	Landaus
<g/>
-	-	kIx~	-
<g/>
Kleffner	Kleffner	k1gInSc1	Kleffner
<g/>
)	)	kIx)	)
-	-	kIx~	-
Některé	některý	k3yIgInPc1	některý
symptomy	symptom	k1gInPc1	symptom
afazií	afazie	k1gFnPc2	afazie
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
specifičtější	specifický	k2eAgFnPc1d2	specifičtější
<g/>
.	.	kIx.	.
</s>
<s>
Landau-Kleffnerův	Landau-Kleffnerův	k2eAgInSc1d1	Landau-Kleffnerův
symptom	symptom	k1gInSc1	symptom
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
3-7	[number]	k4	3-7
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
již	již	k6eAd1	již
učinily	učinit	k5eAaImAgFnP	učinit
běžný	běžný	k2eAgInSc4d1	běžný
pokrok	pokrok	k1gInSc4	pokrok
v	v	k7c6	v
řeči	řeč	k1gFnSc6	řeč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pojednou	pojednou	k6eAd1	pojednou
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
expresivní	expresivní	k2eAgFnPc1d1	expresivní
i	i	k8xC	i
receptivní	receptivní	k2eAgFnPc1d1	receptivní
funkce	funkce	k1gFnPc1	funkce
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
inteligence	inteligence	k1gFnSc1	inteligence
je	být	k5eAaImIp3nS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
pojí	pojíst	k5eAaPmIp3nS	pojíst
se	se	k3xPyFc4	se
abnormalitami	abnormalita	k1gFnPc7	abnormalita
na	na	k7c6	na
EEG	EEG	kA	EEG
a	a	k8xC	a
epileptickými	epileptický	k2eAgFnPc7d1	epileptická
obtížemi	obtíž	k1gFnPc7	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
řeči	řeč	k1gFnSc2	řeč
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
období	období	k1gNnSc6	období
dní	den	k1gInPc2	den
až	až	k8xS	až
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
zánětlivé	zánětlivý	k2eAgNnSc1d1	zánětlivé
onemocnění	onemocnění	k1gNnSc1	onemocnění
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
u	u	k7c2	u
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
dětí	dítě	k1gFnPc2	dítě
není	být	k5eNaImIp3nS	být
onemocnění	onemocnění	k1gNnSc1	onemocnění
zcela	zcela	k6eAd1	zcela
reverzibilní	reverzibilní	k2eAgMnSc1d1	reverzibilní
<g/>
.	.	kIx.	.
</s>
<s>
Porucha	porucha	k1gFnSc1	porucha
tvorby	tvorba	k1gFnSc2	tvorba
hlasu	hlas	k1gInSc2	hlas
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
dysfonie	dysfonie	k1gFnSc1	dysfonie
a	a	k8xC	a
porucha	porucha	k1gFnSc1	porucha
vyslovování	vyslovování	k1gNnSc2	vyslovování
dysartrie	dysartrie	k1gFnSc2	dysartrie
<g/>
.	.	kIx.	.
</s>
