<s>
Claudia	Claudia	k1gFnSc1
Winkleman	Winkleman	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Claudia	Claudia	k1gFnSc1
Winkleman	Winkleman	k1gMnSc1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Claudia	Claudia	k1gFnSc1
Anne	Ann	k1gFnSc2
I.	I.	kA
Winkleman	Winkleman	k1gMnSc1
Narození	narozený	k2eAgMnPc1d1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1972	#num#	k4
(	(	kIx(
<g/>
49	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Londýn	Londýn	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
novinářka	novinářka	k1gFnSc1
<g/>
,	,	kIx,
spisovatelka	spisovatelka	k1gFnSc1
a	a	k8xC
televizní	televizní	k2eAgFnSc1d1
hlasatelka	hlasatelka	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Murray	Murraa	k1gFnPc1
Edwards	Edwardsa	k1gFnPc2
CollegeCity	CollegeCita	k1gFnSc2
of	of	k?
London	London	k1gMnSc1
School	School	k1gInSc1
for	forum	k1gNnPc2
Girls	girl	k1gFnPc2
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Kris	kris	k1gInSc1
Thykier	Thykira	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
2000	#num#	k4
<g/>
)	)	kIx)
Rodiče	rodič	k1gMnPc1
</s>
<s>
Barry	Barr	k1gInPc1
L.	L.	kA
D.	D.	kA
Winkleman	Winkleman	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Eve	Eve	k1gMnSc1
Pollard	Pollard	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Sophie	Sophie	k1gFnSc1
Winkleman	Winkleman	k1gMnSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Claudia	Claudia	k1gFnSc1
Anne	Ann	k1gFnSc2
I.	I.	kA
Winkleman	Winkleman	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1972	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
anglická	anglický	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
moderátorka	moderátorka	k1gFnSc1
<g/>
,	,	kIx,
filmová	filmový	k2eAgFnSc1d1
kritička	kritička	k1gFnSc1
<g/>
,	,	kIx,
osobnost	osobnost	k1gFnSc1
v	v	k7c6
rádiu	rádio	k1gNnSc6
a	a	k8xC
novinářka	novinářka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Winklemanová	Winklemanová	k1gFnSc1
je	být	k5eAaImIp3nS
dcerou	dcera	k1gFnSc7
Eve	Eve	k1gFnSc2
Pollard	Pollard	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
pracuje	pracovat	k5eAaImIp3nS
v	v	k7c6
Sunday	Sunday	k1gInPc1
Express	express	k1gInSc1
a	a	k8xC
Barryho	Barryha	k1gFnSc5
Winkleman	Winkleman	k1gMnSc1
<g/>
,	,	kIx,
publicisty	publicista	k1gMnSc2
z	z	k7c2
The	The	k1gFnSc2
Times	Times	k1gMnSc1
Atlast	Atlast	k1gMnSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnPc1
rodiče	rodič	k1gMnPc1
se	se	k3xPyFc4
rozvedli	rozvést	k5eAaPmAgMnP
<g/>
,	,	kIx,
když	když	k8xS
jí	on	k3xPp3gFnSc3
byli	být	k5eAaImAgMnP
3	#num#	k4
roky	rok	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
matka	matka	k1gFnSc1
se	se	k3xPyFc4
znovuvdala	znovuvdat	k5eAaImAgFnS,k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
nevlastním	vlastní	k2eNgMnSc7d1
otcem	otec	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
sir	sir	k1gMnSc1
Nicholas	Nicholas	k1gMnSc1
Lloyd	Lloyd	k1gMnSc1
<g/>
,	,	kIx,
editor	editor	k1gInSc1
z	z	k7c2
Daily	Daila	k1gFnSc2
Express	express	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
nevlastní	vlastní	k2eNgFnSc4d1
sestru	sestra	k1gFnSc4
<g/>
,	,	kIx,
z	z	k7c2
otcova	otcův	k2eAgInSc2d1
druhého	druhý	k4xOgInSc2
manželství	manželství	k1gNnSc1
s	s	k7c7
autorkou	autorka	k1gFnSc7
Cindy	Cinda	k1gFnSc2
Black	Black	k1gInSc1
<g/>
,	,	kIx,
herečku	herečka	k1gFnSc4
Sophii	Sophie	k1gFnSc4
Winkleman	Winkleman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
také	také	k9
nevlastního	vlastní	k2eNgMnSc4d1
bratra	bratr	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
ze	z	k7c2
sourozenců	sourozenec	k1gMnPc2
nejmladší	mladý	k2eAgMnSc1d3
<g/>
,	,	kIx,
Olivera	Olivera	k1gFnSc1
Lloyda	Lloyda	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
matčina	matčin	k2eAgInSc2d1
druhého	druhý	k4xOgInSc2
manželství	manželství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Claudia	Claudia	k1gFnSc1
studovala	studovat	k5eAaImAgFnS
školu	škola	k1gFnSc4
v	v	k7c6
Hampsteadu	Hampstead	k1gInSc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
Dívčí	dívčí	k2eAgFnSc4d1
školu	škola	k1gFnSc4
města	město	k1gNnSc2
Londýn	Londýn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
alma	alma	k1gFnSc1
mater	mater	k1gFnSc7
je	být	k5eAaImIp3nS
univerzita	univerzita	k1gFnSc1
v	v	k7c4
Cambridge	Cambridge	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
studovala	studovat	k5eAaImAgFnS
na	na	k7c4
Murray	Murraa	k1gFnPc4
Edwards	Edwards	k1gInSc4
College	Colleg	k1gInSc2
<g/>
,	,	kIx,
jejím	její	k3xOp3gInSc7
oborem	obor	k1gInSc7
byla	být	k5eAaImAgFnS
historie	historie	k1gFnSc1
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Její	její	k3xOp3gFnSc1
teta	teta	k1gFnSc1
byla	být	k5eAaImAgFnS
portrétní	portrétní	k2eAgFnSc1d1
a	a	k8xC
novinářská	novinářský	k2eAgFnSc1d1
fotografka	fotografka	k1gFnSc1
Sally	Salla	k1gFnSc2
Soames	Soamesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Darryl	Darryl	k1gInSc1
Roger	Roger	k1gInSc4
Lundy	Lunda	k1gMnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc2
Peerage	Peerag	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Claudia	Claudia	k1gFnSc1
Winkleman	Winkleman	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Claudia	Claudia	k1gFnSc1
Winkleman	Winkleman	k1gMnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Claudia	Claudia	k1gFnSc1
Winkleman	Winkleman	k1gMnSc1
na	na	k7c6
Twitteru	Twitter	k1gInSc6
</s>
<s>
Claudia	Claudia	k1gFnSc1
Winkleman	Winkleman	k1gMnSc1
on	on	k3xPp3gMnSc1
Unreality	Unrealita	k1gFnPc4
TV	TV	kA
</s>
<s>
Claudia	Claudia	k1gFnSc1
Winkleman	Winkleman	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Independent	independent	k1gMnSc1
column	column	k1gMnSc1
</s>
<s>
Radio	radio	k1gNnSc1
Times	Timesa	k1gFnPc2
photo	photo	k1gNnSc1
gallery	galler	k1gInPc5
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
