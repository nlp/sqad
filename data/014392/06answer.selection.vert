<s>
Globální	globální	k2eAgInSc1d1
družicový	družicový	k2eAgInSc1d1
polohový	polohový	k2eAgInSc1d1
systém	systém	k1gInSc1
(	(	kIx(
<g/>
GNSS	GNSS	kA
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Global	globat	k5eAaImAgMnS
Navigation	Navigation	k1gInSc1
Satellite	Satellit	k1gInSc5
System	Syst	k1gInSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
služba	služba	k1gFnSc1
umožňující	umožňující	k2eAgFnSc1d1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
družic	družice	k1gFnPc2
autonomní	autonomní	k2eAgNnSc4d1
prostorové	prostorový	k2eAgNnSc4d1
určování	určování	k1gNnSc4
polohy	poloha	k1gFnSc2
s	s	k7c7
celosvětovým	celosvětový	k2eAgNnSc7d1
pokrytím	pokrytí	k1gNnSc7
<g/>
.	.	kIx.
</s>