<s>
Globální	globální	k2eAgInSc1d1
družicový	družicový	k2eAgInSc1d1
polohový	polohový	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Globální	globální	k2eAgInSc1d1
družicový	družicový	k2eAgInSc1d1
polohový	polohový	k2eAgInSc1d1
systém	systém	k1gInSc1
(	(	kIx(
<g/>
GNSS	GNSS	kA
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Global	globat	k5eAaImAgMnS
Navigation	Navigation	k1gInSc1
Satellite	Satellit	k1gInSc5
System	Syst	k1gInSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
služba	služba	k1gFnSc1
umožňující	umožňující	k2eAgFnSc1d1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
družic	družice	k1gFnPc2
autonomní	autonomní	k2eAgNnSc4d1
prostorové	prostorový	k2eAgNnSc4d1
určování	určování	k1gNnSc4
polohy	poloha	k1gFnSc2
s	s	k7c7
celosvětovým	celosvětový	k2eAgNnSc7d1
pokrytím	pokrytí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelé	uživatel	k1gMnPc1
této	tento	k3xDgFnSc2
služby	služba	k1gFnSc2
používají	používat	k5eAaImIp3nP
malé	malý	k2eAgInPc4d1
elektronické	elektronický	k2eAgInPc4d1
rádiové	rádiový	k2eAgInPc4d1
přijímače	přijímač	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
na	na	k7c6
základě	základ	k1gInSc6
odeslaných	odeslaný	k2eAgInPc2d1
signálů	signál	k1gInPc2
z	z	k7c2
družic	družice	k1gFnPc2
umožňují	umožňovat	k5eAaImIp3nP
vypočítat	vypočítat	k5eAaPmF
jejich	jejich	k3xOp3gFnSc4
polohu	poloha	k1gFnSc4
s	s	k7c7
přesností	přesnost	k1gFnSc7
na	na	k7c4
desítky	desítka	k1gFnPc4
až	až	k8xS
jednotky	jednotka	k1gFnPc4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesnost	přesnost	k1gFnSc1
ve	v	k7c6
speciálních	speciální	k2eAgFnPc6d1
nebo	nebo	k8xC
vědeckých	vědecký	k2eAgFnPc6d1
aplikacích	aplikace	k1gFnPc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
až	až	k9
několik	několik	k4yIc4
centimetrů	centimetr	k1gInPc2
až	až	k8xS
milimetrů	milimetr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
je	být	k5eAaImIp3nS
plně	plně	k6eAd1
funkční	funkční	k2eAgInSc1d1
globální	globální	k2eAgInSc1d1
systém	systém	k1gInSc1
pouze	pouze	k6eAd1
americký	americký	k2eAgMnSc1d1
Global	globat	k5eAaImAgInS
Positioning	Positioning	k1gInSc1
System	Syst	k1gInSc7
(	(	kIx(
<g/>
GPS	GPS	kA
<g/>
)	)	kIx)
a	a	k8xC
ruský	ruský	k2eAgMnSc1d1
GLONASS	GLONASS	kA
(	(	kIx(
<g/>
Г	Г	k?
<g/>
́	́	k?
<g/>
л	л	k?
н	н	k?
<g/>
́	́	k?
<g/>
н	н	k?
с	с	k?
<g/>
́	́	k?
<g/>
т	т	k?
с	с	k?
<g/>
́	́	k?
<g/>
м	м	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
je	být	k5eAaImIp3nS
funkční	funkční	k2eAgInSc1d1
evropský	evropský	k2eAgInSc1d1
Galileo	Galilea	k1gFnSc5
a	a	k8xC
čínský	čínský	k2eAgMnSc1d1
BeiDou-	BeiDou-	k1gMnSc1
<g/>
3	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
jejich	jejich	k3xOp3gNnSc7
uvedením	uvedení	k1gNnSc7
do	do	k7c2
plné	plný	k2eAgFnSc2d1
operační	operační	k2eAgFnSc2d1
schopnosti	schopnost	k1gFnSc2
se	se	k3xPyFc4
počítá	počítat	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
globální	globální	k2eAgInPc4d1
družicové	družicový	k2eAgInPc4d1
polohové	polohový	k2eAgInPc4d1
systémy	systém	k1gInPc4
existují	existovat	k5eAaImIp3nP
i	i	k9
regionální	regionální	k2eAgInPc4d1
autonomní	autonomní	k2eAgInPc4d1
družicové	družicový	k2eAgInPc4d1
polohové	polohový	k2eAgInPc4d1
systémy	systém	k1gInPc4
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
existující	existující	k2eAgInSc4d1
čínský	čínský	k2eAgInSc4d1
Beidou	Beida	k1gFnSc7
1	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
,	,	kIx,
japonský	japonský	k2eAgInSc1d1
Quasi-Zenith	Quasi-Zenith	k1gInSc1
Satellite	Satellit	k1gInSc5
System	Syst	k1gInSc7
a	a	k8xC
vyvíjený	vyvíjený	k2eAgInSc1d1
indický	indický	k2eAgInSc1d1
IRNSS	IRNSS	kA
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc4
globálních	globální	k2eAgInPc2d1
družicových	družicový	k2eAgInPc2d1
polohových	polohový	k2eAgInPc2d1
systémů	systém	k1gInPc2
</s>
<s>
název	název	k1gInSc1
</s>
<s>
výchozí	výchozí	k2eAgInSc1d1
princip	princip	k1gInSc1
měření	měření	k1gNnSc2
</s>
<s>
stát	stát	k1gInSc1
</s>
<s>
vypouštění	vypouštění	k1gNnSc1
družic	družice	k1gFnPc2
</s>
<s>
globální	globální	k2eAgNnSc4d1
zprovoznění	zprovoznění	k1gNnSc4
</s>
<s>
počet	počet	k1gInSc1
družic	družice	k1gFnPc2
na	na	k7c6
polární	polární	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
<g/>
+	+	kIx~
<g/>
záloha	záloha	k1gFnSc1
</s>
<s>
inklinace	inklinace	k1gFnSc1
</s>
<s>
počet	počet	k1gInSc1
polárních	polární	k2eAgFnPc2d1
drah	draha	k1gFnPc2
</s>
<s>
výška	výška	k1gFnSc1
orbitu	orbita	k1gFnSc4
[	[	kIx(
<g/>
km	km	kA
<g/>
]	]	kIx)
</s>
<s>
doba	doba	k1gFnSc1
oběhu	oběh	k1gInSc2
[	[	kIx(
<g/>
hh	hh	k?
<g/>
:	:	kIx,
<g/>
mm	mm	kA
<g/>
]	]	kIx)
</s>
<s>
spuštěníplná	spuštěníplný	k2eAgFnSc1d1
funkčnostplánovanýve	funkčnostplánovanývat	k5eAaPmIp3nS
službě	služba	k1gFnSc3
</s>
<s>
Transitdoppler	Transitdoppler	k1gMnSc1
<g/>
.	.	kIx.
<g/>
USA	USA	kA
<g/>
1959	#num#	k4
<g/>
–	–	k?
<g/>
19883	#num#	k4
<g/>
+	+	kIx~
<g/>
3	#num#	k4
</s>
<s>
0	#num#	k4
ref	ref	k?
</s>
<s>
67	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
100	#num#	k4
LEO	Leo	k1gMnSc1
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
</s>
<s>
Parus	Parus	k1gInSc1
(	(	kIx(
<g/>
Cyklon	cyklon	k1gInSc1
<g/>
,	,	kIx,
Zaliv	zalít	k5eAaPmDgInS
<g/>
,	,	kIx,
Cikada-M	Cikada-M	k1gMnSc1
<g/>
)	)	kIx)
<g/>
doppler	doppler	k1gMnSc1
<g/>
.	.	kIx.
<g/>
SSSR	SSSR	kA
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
20106	#num#	k4
</s>
<s>
6	#num#	k4
ref	ref	k?
<g/>
83	#num#	k4
<g/>
°	°	k?
<g/>
6730	#num#	k4
<g/>
-	-	kIx~
<g/>
960	#num#	k4
LEO	Leo	k1gMnSc1
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
</s>
<s>
Cikadadoppler	Cikadadoppler	k1gMnSc1
<g/>
.	.	kIx.
<g/>
SSSR	SSSR	kA
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
?	?	kIx.
</s>
<s>
0	#num#	k4
ref	ref	k?
<g/>
83	#num#	k4
<g/>
°	°	k?
<g/>
4965	#num#	k4
LEO	Leo	k1gMnSc1
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
</s>
<s>
Navstar	Navstar	k1gInSc1
GPSkódovéUSA	GPSkódovéUSA	k1gFnSc2
<g/>
19781993199424	#num#	k4
<g/>
+	+	kIx~
<g/>
3	#num#	k4
</s>
<s>
31	#num#	k4
ref	ref	k?
<g/>
55	#num#	k4
<g/>
°	°	k?
<g/>
620	#num#	k4
200	#num#	k4
MEO	MEO	kA
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
</s>
<s>
GlonasskódovéRusko	GlonasskódovéRusko	k6eAd1
<g/>
19821995201124	#num#	k4
</s>
<s>
23	#num#	k4
ref	ref	k?
<g/>
65	#num#	k4
<g/>
°	°	k?
<g/>
319	#num#	k4
100	#num#	k4
MEO	MEO	kA
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
</s>
<s>
Dorisdoppler	Dorisdoppler	k1gInSc1
<g/>
.	.	kIx.
<g/>
Francie	Francie	k1gFnSc1
<g/>
1990	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
5	#num#	k4
ref	ref	k?
<g/>
35	#num#	k4
<g/>
-	-	kIx~
<g/>
66	#num#	k4
<g/>
-	-	kIx~
<g/>
99	#num#	k4
<g/>
°	°	k?
<g/>
-	-	kIx~
<g/>
560	#num#	k4
<g/>
-	-	kIx~
<g/>
825	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
340	#num#	k4
LEO	Leo	k1gMnSc1
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
41	#num#	k4
</s>
<s>
GalileokódovéEU	GalileokódovéEU	k?
<g/>
20062016202027	#num#	k4
<g/>
+	+	kIx~
<g/>
3	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
ref	ref	k?
<g/>
56	#num#	k4
<g/>
°	°	k?
<g/>
323	#num#	k4
200	#num#	k4
MEO	MEO	kA
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
BeiDou-	BeiDou-	k?
<g/>
3	#num#	k4
(	(	kIx(
<g/>
Compass	Compass	k1gInSc4
<g/>
,	,	kIx,
Pej-tou	Pej-tá	k1gFnSc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
kódovéČína	kódovéČína	k1gFnSc1
<g/>
2007202035	#num#	k4
</s>
<s>
3355	#num#	k4
<g/>
°	°	k?
<g/>
331	#num#	k4
500	#num#	k4
MEO	MEO	kA
38	#num#	k4
300	#num#	k4
IGSO	IGSO	kA
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
Poslední	poslední	k2eAgFnSc1d1
změna	změna	k1gFnSc1
<g/>
:	:	kIx,
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
</s>
<s>
Princip	princip	k1gInSc1
funkce	funkce	k1gFnSc2
</s>
<s>
Geometrické	geometrický	k2eAgNnSc1d1
znázornění	znázornění	k1gNnSc1
hledání	hledání	k1gNnSc1
polohy	poloha	k1gFnSc2
pomocí	pomocí	k7c2
těžiště	těžiště	k1gNnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
v	v	k7c6
průniku	průnik	k1gInSc6
tří	tři	k4xCgFnPc2
kružnic	kružnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1
lze	lze	k6eAd1
družicové	družicový	k2eAgInPc4d1
polohové	polohový	k2eAgInPc4d1
systémy	systém	k1gInPc4
popsat	popsat	k5eAaPmF
jako	jako	k8xC,k8xS
družicový	družicový	k2eAgInSc1d1
rádiový	rádiový	k2eAgInSc1d1
dálkoměrný	dálkoměrný	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Dálkoměrný	dálkoměrný	k2eAgInSc1d1
systém	systém	k1gInSc1
dovolí	dovolit	k5eAaPmIp3nS
určit	určit	k5eAaPmF
naši	náš	k3xOp1gFnSc4
vlastní	vlastní	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
je	být	k5eAaImIp3nS
naše	náš	k3xOp1gFnSc1
vzdálenost	vzdálenost	k1gFnSc1
od	od	k7c2
dvou	dva	k4xCgInPc2
známých	známý	k2eAgInPc2d1
bodů	bod	k1gInPc2
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
současně	současně	k6eAd1
i	i	k9
viděných	viděná	k1gFnPc2
v	v	k7c6
krajině	krajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nutný	nutný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
dalekohled	dalekohled	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
změří	změřit	k5eAaPmIp3nS
naše	náš	k3xOp1gFnPc4
vzdálenosti	vzdálenost	k1gFnPc4
od	od	k7c2
oněch	onen	k3xDgInPc2
bodů	bod	k1gInPc2
v	v	k7c6
krajině	krajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
mapy	mapa	k1gFnSc2
nakreslí	nakreslit	k5eAaPmIp3nS
kružnice	kružnice	k1gFnSc1
se	s	k7c7
středem	střed	k1gInSc7
v	v	k7c6
jednom	jeden	k4xCgMnSc6
a	a	k8xC
druhém	druhý	k4xOgInSc6
bodě	bod	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
poloměry	poloměr	k1gInPc1
jsou	být	k5eAaImIp3nP
dané	daný	k2eAgInPc1d1
změřenými	změřený	k2eAgFnPc7d1
vzdálenostmi	vzdálenost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
průsečíku	průsečík	k1gInSc6
vzniklých	vzniklý	k2eAgFnPc2d1
kružnic	kružnice	k1gFnPc2
(	(	kIx(
<g/>
průsečíky	průsečík	k1gInPc1
jsou	být	k5eAaImIp3nP
však	však	k9
dva	dva	k4xCgInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rádiový	rádiový	k2eAgInSc1d1
systém	systém	k1gInSc1
pro	pro	k7c4
měření	měření	k1gNnSc4
určitého	určitý	k2eAgInSc2d1
parametru	parametr	k1gInSc2
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
rádiových	rádiový	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
<g/>
.	.	kIx.
„	„	k?
<g/>
Rádiový	rádiový	k2eAgInSc4d1
dálkoměrný	dálkoměrný	k2eAgInSc4d1
<g/>
“	“	k?
systém	systém	k1gInSc4
k	k	k7c3
měření	měření	k1gNnSc3
vzdálenosti	vzdálenost	k1gFnSc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
rádiových	rádiový	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
takto	takto	k6eAd1
<g/>
:	:	kIx,
Do	do	k7c2
bodu	bod	k1gInSc2
se	s	k7c7
známou	známý	k2eAgFnSc7d1
polohou	poloha	k1gFnSc7
je	být	k5eAaImIp3nS
umístěn	umístěn	k2eAgInSc1d1
vysílač	vysílač	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vysílá	vysílat	k5eAaImIp3nS
rádiové	rádiový	k2eAgFnPc4d1
vlny	vlna	k1gFnPc4
s	s	k7c7
časovými	časový	k2eAgFnPc7d1
značkami	značka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bodě	bod	k1gInSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
poloha	poloha	k1gFnSc1
se	se	k3xPyFc4
měří	měřit	k5eAaImIp3nS
<g/>
,	,	kIx,
umístíme	umístit	k5eAaPmIp1nP
přijímač	přijímač	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
porovnává	porovnávat	k5eAaImIp3nS
časové	časový	k2eAgFnPc4d1
značky	značka	k1gFnPc4
se	s	k7c7
svými	svůj	k3xOyFgInPc7
„	„	k?
<g/>
hodinami	hodina	k1gFnPc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
změřit	změřit	k5eAaPmF
zpoždění	zpoždění	k1gNnSc4
<g/>
,	,	kIx,
tj.	tj.	kA
jak	jak	k8xC,k8xS
dlouho	dlouho	k6eAd1
trvalo	trvat	k5eAaImAgNnS
rádiové	rádiový	k2eAgFnSc3d1
vlně	vlna	k1gFnSc3
<g/>
,	,	kIx,
než	než	k8xS
k	k	k7c3
přijímači	přijímač	k1gInSc3
dorazila	dorazit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
se	se	k3xPyFc4
radiové	radiový	k2eAgFnPc1d1
vlny	vlna	k1gFnPc1
pohybují	pohybovat	k5eAaImIp3nP
známou	známý	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
<g/>
,	,	kIx,
stačí	stačit	k5eAaBmIp3nS
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
požadované	požadovaný	k2eAgFnSc2d1
vzdálenosti	vzdálenost	k1gFnSc2
vynásobit	vynásobit	k5eAaPmF
změřené	změřený	k2eAgNnSc4d1
zpoždění	zpoždění	k1gNnSc4
touto	tento	k3xDgFnSc7
rychlostí	rychlost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
určení	určení	k1gNnSc4
polohy	poloha	k1gFnSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
vzdálenost	vzdálenost	k1gFnSc1
změřena	změřen	k2eAgFnSc1d1
z	z	k7c2
více	hodně	k6eAd2
bodů	bod	k1gInPc2
stejně	stejně	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
v	v	k7c6
prvním	první	k4xOgInSc6
případě	případ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledná	výsledný	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
přijímače	přijímač	k1gInSc2
pak	pak	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vypočítána	vypočítat	k5eAaPmNgFnS
například	například	k6eAd1
pomocí	pomocí	k7c2
trilaterace	trilaterace	k1gFnSc2
nebo	nebo	k8xC
multilaterace	multilaterace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Družicový	družicový	k2eAgInSc1d1
je	být	k5eAaImIp3nS
systém	systém	k1gInSc1
označován	označovat	k5eAaImNgInS
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
body	bod	k1gInPc1
se	s	k7c7
známou	známý	k2eAgFnSc7d1
polohou	poloha	k1gFnSc7
jsou	být	k5eAaImIp3nP
družice	družice	k1gFnPc4
obíhající	obíhající	k2eAgFnSc4d1
Zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
v	v	k7c6
jejich	jejich	k3xOp3gNnSc6
vysílání	vysílání	k1gNnSc6
nejen	nejen	k6eAd1
časové	časový	k2eAgFnSc2d1
značky	značka	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
parametry	parametr	k1gInPc4
dráhy	dráha	k1gFnSc2
dané	daný	k2eAgFnSc2d1
družice	družice	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
lze	lze	k6eAd1
polohu	poloha	k1gFnSc4
při	při	k7c6
odeslání	odeslání	k1gNnSc6
zprávy	zpráva	k1gFnSc2
vypočítat	vypočítat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Družicovým	družicový	k2eAgInPc3d1
systémům	systém	k1gInPc3
předcházely	předcházet	k5eAaImAgFnP
pozemní	pozemní	k2eAgFnPc1d1
(	(	kIx(
<g/>
například	například	k6eAd1
LORAN	LORAN	kA
<g/>
,	,	kIx,
Gee	Gee	k1gFnSc1
<g/>
,	,	kIx,
Čajka	čajka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgFnSc1
literatura	literatura	k1gFnSc1
se	se	k3xPyFc4
zmiňuje	zmiňovat	k5eAaImIp3nS
o	o	k7c6
dvou	dva	k4xCgFnPc6
generacích	generace	k1gFnPc6
GNSS	GNSS	kA
<g/>
:	:	kIx,
</s>
<s>
GNSS-1	GNSS-1	k4
Do	do	k7c2
první	první	k4xOgFnSc2
generace	generace	k1gFnSc2
jsou	být	k5eAaImIp3nP
zařazovány	zařazován	k2eAgInPc1d1
GPS	GPS	kA
a	a	k8xC
GLONASS	GLONASS	kA
s	s	k7c7
podpůrnými	podpůrný	k2eAgInPc7d1
systémy	systém	k1gInPc7
SBAS	SBAS	kA
<g/>
,	,	kIx,
GBAS	GBAS	kA
a	a	k8xC
LAAS	LAAS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
systémy	systém	k1gInPc4
byly	být	k5eAaImAgInP
prioritně	prioritně	k6eAd1
vyvinuty	vyvinout	k5eAaPmNgInP
pro	pro	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
sféru	sféra	k1gFnSc4
a	a	k8xC
sekundárně	sekundárně	k6eAd1
zajišťují	zajišťovat	k5eAaImIp3nP
stálé	stálý	k2eAgNnSc4d1
globální	globální	k2eAgNnSc4d1
pokrytí	pokrytí	k1gNnSc4
službou	služba	k1gFnSc7
pro	pro	k7c4
civilní	civilní	k2eAgInSc4d1
sektor	sektor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
GNSS-2	GNSS-2	k4
Do	do	k7c2
druhé	druhý	k4xOgFnSc2
generace	generace	k1gFnSc2
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nP
vyvíjené	vyvíjený	k2eAgInPc1d1
GNSS	GNSS	kA
jako	jako	k8xC,k8xS
GPS-III	GPS-III	k1gFnSc1
<g/>
,	,	kIx,
Galileo	Galilea	k1gFnSc5
<g/>
,	,	kIx,
Compass	Compassa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajišťují	zajišťovat	k5eAaImIp3nP
vysokou	vysoký	k2eAgFnSc4d1
přesnost	přesnost	k1gFnSc4
a	a	k8xC
spolehlivost	spolehlivost	k1gFnSc4
pro	pro	k7c4
aplikace	aplikace	k1gFnPc4
Safety	Safeta	k1gFnSc2
of	of	k?
Life	Lif	k1gFnSc2
plnohodnotné	plnohodnotný	k2eAgFnPc1d1
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
uživatele	uživatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Rádiové	rádiový	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
</s>
<s>
Schéma	schéma	k1gNnSc1
užívaných	užívaný	k2eAgNnPc2d1
pásem	pásmo	k1gNnPc2
a	a	k8xC
nominálních	nominální	k2eAgFnPc2d1
frekvencí	frekvence	k1gFnPc2
GNSS	GNSS	kA
<g/>
.	.	kIx.
</s>
<s>
Každá	každý	k3xTgFnSc1
družice	družice	k1gFnSc1
v	v	k7c6
různých	různý	k2eAgInPc6d1
systémech	systém	k1gInPc6
GNSS	GNSS	kA
vysílá	vysílat	k5eAaImIp3nS
rádiové	rádiový	k2eAgFnPc4d1
vlny	vlna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
jednotlivé	jednotlivý	k2eAgInPc1d1
systémy	systém	k1gInPc1
GNSS	GNSS	kA
vzájemně	vzájemně	k6eAd1
nerušily	rušit	k5eNaImAgInP
má	mít	k5eAaImIp3nS
každý	každý	k3xTgMnSc1
vyhrazenou	vyhrazený	k2eAgFnSc4d1
frekvenci	frekvence	k1gFnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
smluvený	smluvený	k2eAgInSc1d1
způsob	způsob	k1gInSc1
vysílání	vysílání	k1gNnSc2
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
frekvenci	frekvence	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
jednoho	jeden	k4xCgInSc2
systému	systém	k1gInSc2
GNSS	GNSS	kA
je	být	k5eAaImIp3nS
družic	družice	k1gFnPc2
několik	několik	k4yIc4
desítek	desítka	k1gFnPc2
a	a	k8xC
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
odlišit	odlišit	k5eAaPmF
jednu	jeden	k4xCgFnSc4
od	od	k7c2
druhé	druhý	k4xOgFnSc2
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
několik	několik	k4yIc1
metod	metoda	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
kódové	kódový	k2eAgNnSc1d1
-	-	kIx~
CDMA	CDMA	kA
(	(	kIx(
<g/>
Code	Cod	k1gMnSc2
Division	Division	k1gInSc1
Multiple	multipl	k1gInSc5
Access	Access	k1gInSc4
<g/>
)	)	kIx)
-	-	kIx~
každá	každý	k3xTgFnSc1
družice	družice	k1gFnSc1
vysílá	vysílat	k5eAaImIp3nS
různé	různý	k2eAgInPc4d1
kódy	kód	k1gInPc4
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
frekvenci	frekvence	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
charakteristikou	charakteristika	k1gFnSc7
blíží	blížit	k5eAaImIp3nS
náhodnému	náhodný	k2eAgInSc3d1
kódu	kód	k1gInSc3
a	a	k8xC
proto	proto	k6eAd1
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
za	za	k7c2
PRN	PRN	kA
(	(	kIx(
<g/>
Pseudo	Pseudo	k1gNnSc1
Random	Random	k1gInSc4
Noise	Noise	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijímač	přijímač	k1gInSc1
pak	pak	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
znalosti	znalost	k1gFnSc2
tohoto	tento	k3xDgInSc2
kódu	kód	k1gInSc2
najde	najít	k5eAaPmIp3nS
signál	signál	k1gInSc1
konkrétní	konkrétní	k2eAgFnSc2d1
družice	družice	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
pomocí	pomocí	k7c2
korelace	korelace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Signály	signál	k1gInPc7
ostatních	ostatní	k2eAgFnPc2d1
družic	družice	k1gFnPc2
s	s	k7c7
nekorelovaným	korelovaný	k2eNgInSc7d1
signálem	signál	k1gInSc7
se	se	k3xPyFc4
pak	pak	k6eAd1
projevují	projevovat	k5eAaImIp3nP
jako	jako	k8xS,k8xC
šumové	šumový	k2eAgNnSc4d1
pozadí	pozadí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
přístup	přístup	k1gInSc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
například	například	k6eAd1
systémy	systém	k1gInPc1
GPS	GPS	kA
a	a	k8xC
Galileo	Galilea	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
frekvenční	frekvenční	k2eAgFnSc1d1
-	-	kIx~
FDMA	FDMA	kA
(	(	kIx(
<g/>
Frequency	Frequenca	k1gMnSc2
Division	Division	k1gInSc1
Multiple	multipl	k1gInSc5
Access	Access	k1gInSc4
<g/>
)	)	kIx)
-	-	kIx~
každá	každý	k3xTgFnSc1
družice	družice	k1gFnSc1
vysílá	vysílat	k5eAaImIp3nS
stejné	stejný	k2eAgInPc4d1
kódy	kód	k1gInPc4
na	na	k7c6
jiné	jiný	k2eAgFnSc6d1
frekvenci	frekvence	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
počet	počet	k1gInSc1
volných	volný	k2eAgFnPc2d1
frekvencí	frekvence	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevýhodou	nevýhoda	k1gFnSc7
jsou	být	k5eAaImIp3nP
vznikající	vznikající	k2eAgInPc1d1
interference	interference	k1gFnPc4
vlnění	vlnění	k1gNnPc2
při	při	k7c6
šíření	šíření	k1gNnSc6
radiových	radiový	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
a	a	k8xC
obtížná	obtížný	k2eAgFnSc1d1
interoperabilita	interoperabilita	k1gFnSc1
mezi	mezi	k7c7
různými	různý	k2eAgInPc7d1
systémy	systém	k1gInPc7
GNSS	GNSS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
ho	on	k3xPp3gNnSc4
GLONASS	GLONASS	kA
<g/>
.	.	kIx.
</s>
<s>
časové	časový	k2eAgNnSc1d1
-	-	kIx~
TDMA	TDMA	kA
(	(	kIx(
<g/>
Time	Tim	k1gMnSc2
Division	Division	k1gInSc1
Multiple	multipl	k1gInSc5
Access	Access	k1gInSc4
<g/>
)	)	kIx)
-	-	kIx~
každá	každý	k3xTgFnSc1
družice	družice	k1gFnSc1
vysílá	vysílat	k5eAaImIp3nS
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
frekvenci	frekvence	k1gFnSc6
stejné	stejný	k2eAgInPc4d1
kódy	kód	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
je	být	k5eAaImIp3nS
komplikovaný	komplikovaný	k2eAgInSc1d1
na	na	k7c4
realizaci	realizace	k1gFnSc4
přijímače	přijímač	k1gInSc2
(	(	kIx(
<g/>
nastavení	nastavení	k1gNnSc4
přesných	přesný	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
nepoužívá	používat	k5eNaImIp3nS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s>
Určování	určování	k1gNnSc1
polohy	poloha	k1gFnSc2
a	a	k8xC
času	čas	k1gInSc2
</s>
<s>
GNSS	GNSS	kA
systémy	systém	k1gInPc1
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
navrženy	navrhnout	k5eAaPmNgInP
k	k	k7c3
jednomu	jeden	k4xCgNnSc3
principiálně	principiálně	k6eAd1
jednoduchému	jednoduchý	k2eAgInSc3d1
způsobu	způsob	k1gInSc3
výpočtu	výpočet	k1gInSc2
polohy	poloha	k1gFnSc2
<g/>
,	,	kIx,
přesto	přesto	k8xC
je	být	k5eAaImIp3nS
však	však	k9
možno	možno	k6eAd1
ve	v	k7c6
speciálních	speciální	k2eAgFnPc6d1
aplikacích	aplikace	k1gFnPc6
uplatnit	uplatnit	k5eAaPmF
jiné	jiný	k2eAgInPc4d1
přístupy	přístup	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Kódová	kódový	k2eAgFnSc1d1
</s>
<s>
Fázová	fázový	k2eAgFnSc1d1
</s>
<s>
Dopplerovská	Dopplerovský	k2eAgFnSc1d1
</s>
<s>
Úhloměrná	Úhloměrný	k2eAgFnSc1d1
</s>
<s>
Kódová	kódový	k2eAgFnSc1d1
</s>
<s>
Měření	měření	k1gNnPc1
jsou	být	k5eAaImIp3nP
jednoduchá	jednoduchý	k2eAgNnPc1d1
<g/>
,	,	kIx,
spolehlivá	spolehlivý	k2eAgNnPc1d1
a	a	k8xC
nejčastěji	často	k6eAd3
používaná	používaný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
časových	časový	k2eAgFnPc2d1
značek	značka	k1gFnPc2
a	a	k8xC
známé	známý	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
vysílačů	vysílač	k1gInPc2
(	(	kIx(
<g/>
družic	družice	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
spočítat	spočítat	k5eAaPmF
polohu	poloha	k1gFnSc4
a	a	k8xC
čas	čas	k1gInSc4
v	v	k7c6
místě	místo	k1gNnSc6
přijímače	přijímač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
přijetí	přijetí	k1gNnSc6
rádiového	rádiový	k2eAgInSc2d1
signálu	signál	k1gInSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
přijímači	přijímač	k1gInSc6
dekódovány	dekódován	k2eAgFnPc1d1
<g/>
:	:	kIx,
</s>
<s>
časové	časový	k2eAgFnPc1d1
značky	značka	k1gFnPc1
při	při	k7c6
odeslání	odeslání	k1gNnSc6
signálu	signál	k1gInSc2
každé	každý	k3xTgFnSc2
družice	družice	k1gFnSc2
</s>
<s>
(	(	kIx(
</s>
<s>
t	t	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
polohy	poloh	k1gInPc1
každé	každý	k3xTgFnSc2
družice	družice	k1gFnSc2
v	v	k7c6
prostoru	prostor	k1gInSc6
<g/>
,	,	kIx,
tzv.	tzv.	kA
efemeridy	efemerida	k1gFnPc1
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
,	,	kIx,
</s>
<s>
z	z	k7c2
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
,	,	kIx,
<g/>
z	z	k7c2
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Hledáme	hledat	k5eAaImIp1nP
<g/>
-li	-li	k?
pozici	pozice	k1gFnSc4
uživatele	uživatel	k1gMnSc2
v	v	k7c6
prostoru	prostor	k1gInSc6
<g/>
,	,	kIx,
musíme	muset	k5eAaImIp1nP
ji	on	k3xPp3gFnSc4
popsat	popsat	k5eAaPmF
třemi	tři	k4xCgNnPc7
souřadnicemi	souřadnice	k1gFnPc7
např.	např.	kA
v	v	k7c6
kartézském	kartézský	k2eAgInSc6d1
systému	systém	k1gInSc6
</s>
<s>
E	E	kA
</s>
<s>
C	C	kA
</s>
<s>
E	E	kA
</s>
<s>
F	F	kA
</s>
<s>
(	(	kIx(
</s>
<s>
X	X	kA
</s>
<s>
,	,	kIx,
</s>
<s>
Y	Y	kA
</s>
<s>
,	,	kIx,
</s>
<s>
Z	z	k7c2
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
ECEF	ECEF	kA
<g/>
(	(	kIx(
<g/>
X	X	kA
<g/>
,	,	kIx,
<g/>
Y	Y	kA
<g/>
,	,	kIx,
<g/>
Z	z	k7c2
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
Protože	protože	k8xS
čas	čas	k1gInSc1
v	v	k7c6
přijímači	přijímač	k1gInSc6
není	být	k5eNaImIp3nS
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
výpočtu	výpočet	k1gInSc2
přesný	přesný	k2eAgMnSc1d1
a	a	k8xC
synchronní	synchronní	k2eAgInSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
čas	čas	k1gInSc1
uživatele	uživatel	k1gMnSc4
také	také	k6eAd1
proměnná	proměnný	k2eAgFnSc1d1
</s>
<s>
(	(	kIx(
</s>
<s>
T	T	kA
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
T	T	kA
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
Neznámé	neznámá	k1gFnPc1
jsou	být	k5eAaImIp3nP
tedy	tedy	k9
</s>
<s>
(	(	kIx(
</s>
<s>
X	X	kA
</s>
<s>
,	,	kIx,
</s>
<s>
Y	Y	kA
</s>
<s>
,	,	kIx,
</s>
<s>
Z	z	k7c2
</s>
<s>
,	,	kIx,
</s>
<s>
T	T	kA
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
X	X	kA
<g/>
,	,	kIx,
<g/>
Y	Y	kA
<g/>
,	,	kIx,
<g/>
Z	Z	kA
<g/>
,	,	kIx,
<g/>
T	T	kA
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
proto	proto	k8xC
můžeme	moct	k5eAaImIp1nP
sestavit	sestavit	k5eAaPmF
4	#num#	k4
rovnice	rovnice	k1gFnPc4
koule	koule	k1gFnSc2
</s>
<s>
(	(	kIx(
</s>
<s>
X	X	kA
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
Y	Y	kA
</s>
<s>
−	−	k?
</s>
<s>
y	y	k?
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
Z	z	k7c2
</s>
<s>
−	−	k?
</s>
<s>
z	z	k7c2
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
[	[	kIx(
</s>
<s>
(	(	kIx(
</s>
<s>
T	T	kA
</s>
<s>
−	−	k?
</s>
<s>
t	t	k?
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
c	c	k0
</s>
<s>
]	]	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
X-x_	X-x_	k1gMnPc5
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
Y-y_	Y-y_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
(	(	kIx(
<g/>
Z-z_	Z-z_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
[	[	kIx(
<g/>
(	(	kIx(
<g/>
T-t_	T-t_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
c	c	k0
<g/>
]	]	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
o	o	k7c4
4	#num#	k4
neznámých	známý	k2eNgMnPc2d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
c	c	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
c	c	k0
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc4
světla	světlo	k1gNnSc2
a	a	k8xC
za	za	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
známe	znát	k5eAaImIp1nP
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
,	,	kIx,
</s>
<s>
z	z	k7c2
</s>
<s>
,	,	kIx,
</s>
<s>
t	t	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
,	,	kIx,
<g/>
z	z	k0
<g/>
,	,	kIx,
<g/>
t	t	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
pro	pro	k7c4
4	#num#	k4
družice	družice	k1gFnPc4
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
3	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
1,2	1,2	k4
<g/>
,3	,3	k4
<g/>
,4	,4	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
je	být	k5eAaImIp3nS
řešením	řešení	k1gNnSc7
rovnice	rovnice	k1gFnSc2
poloha	poloha	k1gFnSc1
a	a	k8xC
čas	čas	k1gInSc1
uživatele	uživatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
převod	převod	k1gInSc4
do	do	k7c2
zeměpisných	zeměpisný	k2eAgFnPc2d1
souřadnic	souřadnice	k1gFnPc2
a	a	k8xC
občanského	občanský	k2eAgInSc2d1
času	čas	k1gInSc2
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
definovaných	definovaný	k2eAgInPc2d1
matematických	matematický	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
např.	např.	kA
platí	platit	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
E	E	kA
</s>
<s>
C	C	kA
</s>
<s>
E	E	kA
</s>
<s>
F	F	kA
</s>
<s>
(	(	kIx(
</s>
<s>
X	X	kA
</s>
<s>
,	,	kIx,
</s>
<s>
Y	Y	kA
</s>
<s>
,	,	kIx,
</s>
<s>
Z	z	k7c2
</s>
<s>
,	,	kIx,
</s>
<s>
T	T	kA
</s>
<s>
)	)	kIx)
</s>
<s>
→	→	k?
</s>
<s>
W	W	kA
</s>
<s>
G	G	kA
</s>
<s>
S	s	k7c7
</s>
<s>
84	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
l	l	kA
</s>
<s>
a	a	k8xC
</s>
<s>
t	t	k?
</s>
<s>
,	,	kIx,
</s>
<s>
l	l	kA
</s>
<s>
o	o	k7c6
</s>
<s>
n	n	k0
</s>
<s>
,	,	kIx,
</s>
<s>
H	H	kA
</s>
<s>
A	a	k9
</s>
<s>
E	E	kA
</s>
<s>
,	,	kIx,
</s>
<s>
U	u	k7c2
</s>
<s>
T	T	kA
</s>
<s>
C	C	kA
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
ECEF	ECEF	kA
<g/>
(	(	kIx(
<g/>
X	X	kA
<g/>
,	,	kIx,
<g/>
Y	Y	kA
<g/>
,	,	kIx,
<g/>
Z	Z	kA
<g/>
,	,	kIx,
<g/>
T	T	kA
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
WGS	WGS	kA
<g/>
84	#num#	k4
<g/>
(	(	kIx(
<g/>
lat	lat	k1gInSc1
<g/>
,	,	kIx,
<g/>
lon	lon	k?
<g/>
,	,	kIx,
<g/>
HAE	HAE	kA
<g/>
,	,	kIx,
<g/>
UTC	UTC	kA
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
lat	lat	k1gInSc1
~	~	kIx~
zeměpisná	zeměpisný	k2eAgFnSc1d1
šířka	šířka	k1gFnSc1
</s>
<s>
lon	lon	k?
~	~	kIx~
zeměpisná	zeměpisný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
HAE	HAE	kA
~	~	kIx~
výška	výška	k1gFnSc1
nad	nad	k7c7
elipsoidem	elipsoid	k1gInSc7
(	(	kIx(
<g/>
Height	Height	k1gMnSc1
Above	Aboev	k1gFnSc2
Ellipsoid	Ellipsoid	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UTC	UTC	kA
~	~	kIx~
čas	čas	k1gInSc1
(	(	kIx(
<g/>
Coordinated	Coordinated	k1gInSc1
Universal	Universal	k1gMnSc2
Time	Tim	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Pro	pro	k7c4
získání	získání	k1gNnSc4
výšky	výška	k1gFnSc2
vztažené	vztažený	k2eAgFnSc2d1
k	k	k7c3
hladině	hladina	k1gFnSc3
moře	moře	k1gNnSc2
(	(	kIx(
<g/>
MSL	MSL	kA
<g/>
,	,	kIx,
Mean	Mean	k1gMnSc1
Sea	Sea	k1gFnSc2
Level	level	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
opravit	opravit	k5eAaPmF
výšku	výška	k1gFnSc4
HAE	HAE	kA
o	o	k7c4
hodnotu	hodnota	k1gFnSc4
převýšení	převýšení	k1gNnSc2
geoidu	geoid	k1gInSc2
nad	nad	k7c7
elipsoidem	elipsoid	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
u	u	k7c2
WGS	WGS	kA
84	#num#	k4
řádově	řádově	k6eAd1
o	o	k7c4
hodnoty	hodnota	k1gFnPc4
-40	-40	k4
až	až	k9
-50	-50	k4
<g/>
m.	m.	k?
Výpočetní	výpočetní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
GPS	GPS	kA
přijímače	přijímač	k1gInSc2
již	již	k9
přibližný	přibližný	k2eAgInSc1d1
model	model	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
a	a	k8xC
opravu	oprava	k1gFnSc4
provádí	provádět	k5eAaImIp3nS
automaticky	automaticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatel	uživatel	k1gMnSc1
má	mít	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
obě	dva	k4xCgFnPc4
hodnoty	hodnota	k1gFnPc4
výšek	výška	k1gFnPc2
HAE	HAE	kA
i	i	k8xC
MSL	MSL	kA
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
výpočet	výpočet	k1gInSc4
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
pouze	pouze	k6eAd1
družice	družice	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
nad	nad	k7c7
obzorem	obzor	k1gInSc7
výše	výše	k1gFnSc1
než	než	k8xS
limitní	limitní	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
5	#num#	k4
<g/>
°	°	k?
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
opatření	opatření	k1gNnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
elevační	elevační	k2eAgFnSc1d1
maska	maska	k1gFnSc1
a	a	k8xC
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
rádiový	rádiový	k2eAgInSc1d1
signál	signál	k1gInSc1
nízko	nízko	k6eAd1
nad	nad	k7c7
obzorem	obzor	k1gInSc7
delší	dlouhý	k2eAgFnSc7d2
dráhou	dráha	k1gFnSc7
více	hodně	k6eAd2
ovlivňuje	ovlivňovat	k5eAaImIp3nS
atmosféra	atmosféra	k1gFnSc1
než	než	k8xS
družice	družice	k1gFnSc1
ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
pozicích	pozice	k1gFnPc6
a	a	k8xC
má	mít	k5eAaImIp3nS
náchylnost	náchylnost	k1gFnSc1
k	k	k7c3
vícecestnému	vícecestný	k2eAgNnSc3d1
šíření	šíření	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
případě	případ	k1gInSc6
příjmu	příjem	k1gInSc2
signálu	signál	k1gInSc2
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
4	#num#	k4
družic	družice	k1gFnPc2
je	být	k5eAaImIp3nS
poloha	poloha	k1gFnSc1
váženým	vážený	k2eAgInSc7d1
průměrem	průměr	k1gInSc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
aby	aby	kYmCp3nS
výhodná	výhodný	k2eAgFnSc1d1
geometrická	geometrický	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
družice	družice	k1gFnSc1
a	a	k8xC
kvalitní	kvalitní	k2eAgInSc1d1
radiový	radiový	k2eAgInSc1d1
signál	signál	k1gInSc1
hrály	hrát	k5eAaImAgInP
významnější	významný	k2eAgFnSc4d2
roli	role	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
výsledek	výsledek	k1gInSc1
výrazně	výrazně	k6eAd1
stabilnější	stabilní	k2eAgMnSc1d2
a	a	k8xC
přesnější	přesný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
ve	v	k7c6
výpočtu	výpočet	k1gInSc6
jen	jen	k9
3	#num#	k4
družice	družice	k1gFnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
určena	určen	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
pouze	pouze	k6eAd1
na	na	k7c6
povrchu	povrch	k1gInSc6
elipsoidu	elipsoid	k1gInSc2
</s>
<s>
(	(	kIx(
</s>
<s>
l	l	kA
</s>
<s>
a	a	k8xC
</s>
<s>
t	t	k?
</s>
<s>
,	,	kIx,
</s>
<s>
l	l	kA
</s>
<s>
o	o	k7c6
</s>
<s>
n	n	k0
</s>
<s>
,	,	kIx,
</s>
<s>
H	H	kA
</s>
<s>
A	a	k9
</s>
<s>
E	E	kA
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
U	u	k7c2
</s>
<s>
T	T	kA
</s>
<s>
C	C	kA
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
lat	lat	k1gInSc1
<g/>
,	,	kIx,
<g/>
lon	lon	k?
<g/>
,	,	kIx,
<g/>
HAE	HAE	kA
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
,	,	kIx,
<g/>
UTC	UTC	kA
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
často	často	k6eAd1
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
neplnohodnotná	plnohodnotný	k2eNgFnSc1d1
navigace	navigace	k1gFnSc1
2	#num#	k4
<g/>
D.	D.	kA
</s>
<s>
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
ve	v	k7c6
výpočtu	výpočet	k1gInSc6
jen	jen	k9
2	#num#	k4
družice	družice	k1gFnSc2
lze	lze	k6eAd1
teoreticky	teoreticky	k6eAd1
určit	určit	k5eAaPmF
výšku	výška	k1gFnSc4
nad	nad	k7c7
elipsoidem	elipsoid	k1gInSc7
</s>
<s>
(	(	kIx(
</s>
<s>
l	l	kA
</s>
<s>
a	a	k8xC
</s>
<s>
t	t	k?
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
l	l	kA
</s>
<s>
o	o	k7c6
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
H	H	kA
</s>
<s>
A	a	k9
</s>
<s>
E	E	kA
</s>
<s>
,	,	kIx,
</s>
<s>
U	u	k7c2
</s>
<s>
T	T	kA
</s>
<s>
C	C	kA
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
lat	lata	k1gFnPc2
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
,	,	kIx,
<g/>
lon	lon	k?
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
,	,	kIx,
<g/>
HAE	HAE	kA
<g/>
,	,	kIx,
<g/>
UTC	UTC	kA
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
toto	tento	k3xDgNnSc4
řešení	řešení	k1gNnSc4
se	se	k3xPyFc4
však	však	k9
nepoužívá	používat	k5eNaImIp3nS
<g/>
,	,	kIx,
neboť	neboť	k8xC
skutečná	skutečný	k2eAgFnSc1d1
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
je	být	k5eAaImIp3nS
polohově	polohově	k6eAd1
závislá	závislý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Výsledek	výsledek	k1gInSc1
výpočtu	výpočet	k1gInSc2
předává	předávat	k5eAaImIp3nS
přijímač	přijímač	k1gInSc1
dále	daleko	k6eAd2
ke	k	k7c3
zpracování	zpracování	k1gNnSc3
pomocí	pomocí	k7c2
standardizovaných	standardizovaný	k2eAgInPc2d1
formátů	formát	k1gInPc2
zpráv	zpráva	k1gFnPc2
(	(	kIx(
<g/>
NMEA	NMEA	kA
<g/>
,	,	kIx,
RTCM	RTCM	kA
<g/>
,	,	kIx,
RINEX	RINEX	kA
<g/>
,	,	kIx,
SiRF	SiRF	k1gFnSc1
<g/>
)	)	kIx)
skrze	skrze	k?
komunikační	komunikační	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
(	(	kIx(
<g/>
Bluetooth	Bluetooth	k1gInSc1
<g/>
,	,	kIx,
sériový	sériový	k2eAgInSc1d1
port	port	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Fázová	fázový	k2eAgFnSc1d1
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
možnosti	možnost	k1gFnSc2
měřit	měřit	k5eAaImF
jednotlivé	jednotlivý	k2eAgFnPc4d1
fáze	fáze	k1gFnPc4
harmonických	harmonický	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
vysílaných	vysílaný	k2eAgInPc2d1
družicí	družice	k1gFnSc7
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
změny	změna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měření	měření	k1gNnPc1
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
vysokou	vysoký	k2eAgFnSc7d1
přesností	přesnost	k1gFnSc7
a	a	k8xC
nejednoznačností	nejednoznačnost	k1gFnSc7
(	(	kIx(
<g/>
ambiguity	ambiguita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejednoznačnost	Nejednoznačnost	k1gFnSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
neznámém	známý	k2eNgInSc6d1
počátečním	počáteční	k2eAgInSc6d1
celkovém	celkový	k2eAgInSc6d1
počtu	počet	k1gInSc6
vln	vlna	k1gFnPc2
mezi	mezi	k7c7
družicí	družice	k1gFnSc7
a	a	k8xC
přijímačem	přijímač	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
několik	několik	k4yIc1
matematických	matematický	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
najít	najít	k5eAaPmF
možné	možný	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
s	s	k7c7
dostatečnou	dostatečný	k2eAgFnSc7d1
pravděpodobností	pravděpodobnost	k1gFnSc7
nebo	nebo	k8xC
se	se	k3xPyFc4
využije	využít	k5eAaPmIp3nS
přesná	přesný	k2eAgFnSc1d1
informace	informace	k1gFnSc1
o	o	k7c6
poloze	poloha	k1gFnSc6
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
zdroje	zdroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
výpočet	výpočet	k1gInSc4
přijímače	přijímač	k1gInSc2
(	(	kIx(
<g/>
rover	rover	k1gMnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
dostupná	dostupný	k2eAgNnPc4d1
kódová	kódový	k2eAgNnPc4d1
měření	měření	k1gNnPc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
almanachu	almanach	k1gInSc2
a	a	k8xC
efemerid	efemerida	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
nezbytné	zbytný	k2eNgNnSc4d1,k2eAgNnSc4d1
průběžné	průběžný	k2eAgNnSc4d1
nebo	nebo	k8xC
zpětně	zpětně	k6eAd1
aplikované	aplikovaný	k2eAgFnSc2d1
diference	diference	k1gFnSc2
z	z	k7c2
jiného	jiné	k1gNnSc2
přijímače-základny	přijímače-základna	k1gFnSc2
(	(	kIx(
<g/>
base	basa	k1gFnSc6
<g/>
)	)	kIx)
o	o	k7c6
známých	známý	k2eAgFnPc6d1
souřadnicích	souřadnice	k1gFnPc6
a	a	k8xC
sledující	sledující	k2eAgFnPc4d1
stejné	stejný	k2eAgFnPc4d1
družice	družice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výhodou	výhoda	k1gFnSc7
rychlejšího	rychlý	k2eAgInSc2d2
výpočtu	výpočet	k1gInSc2
(	(	kIx(
<g/>
délka	délka	k1gFnSc1
vlny	vlna	k1gFnSc2
na	na	k7c6
frekvenci	frekvence	k1gFnSc6
L	L	kA
<g/>
1	#num#	k4
<g/>
=	=	kIx~
<g/>
19	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
L	L	kA
<g/>
2	#num#	k4
<g/>
=	=	kIx~
<g/>
24,2	24,2	k4
cm	cm	kA
<g/>
)	)	kIx)
a	a	k8xC
eliminace	eliminace	k1gFnPc1
atmosférických	atmosférický	k2eAgInPc2d1
vlivů	vliv	k1gInPc2
pracují	pracovat	k5eAaImIp3nP
přijímače	přijímač	k1gInPc1
na	na	k7c6
více	hodně	k6eAd2
frekvencích	frekvence	k1gFnPc6
a	a	k8xC
s	s	k7c7
více	hodně	k6eAd2
družicovými	družicový	k2eAgInPc7d1
systémy	systém	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
urbanizovaných	urbanizovaný	k2eAgFnPc6d1
krajinách	krajina	k1gFnPc6
Evropy	Evropa	k1gFnSc2
se	se	k3xPyFc4
s	s	k7c7
výhodou	výhoda	k1gFnSc7
budují	budovat	k5eAaImIp3nP
veřejné	veřejný	k2eAgInPc1d1
přijímače	přijímač	k1gInPc1
fungující	fungující	k2eAgInPc1d1
jako	jako	k8xS,k8xC
základny	základna	k1gFnPc4
(	(	kIx(
<g/>
base	basa	k1gFnSc3
<g/>
)	)	kIx)
zapojují	zapojovat	k5eAaImIp3nP
se	se	k3xPyFc4
do	do	k7c2
sítí	síť	k1gFnPc2
se	s	k7c7
vzájemnými	vzájemný	k2eAgFnPc7d1
vzdálenostmi	vzdálenost	k1gFnPc7
cca	cca	kA
65	#num#	k4
<g/>
km	km	kA
(	(	kIx(
<g/>
v	v	k7c6
Česku	Česko	k1gNnSc6
např.	např.	kA
Czepos	Czepos	k1gMnSc1
<g/>
,	,	kIx,
Topnet	Topnet	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statisticky	statisticky	k6eAd1
spolehlivé	spolehlivý	k2eAgNnSc1d1
řešení	řešení	k1gNnSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
fixed	fixed	k1gInSc4
<g/>
/	/	kIx~
<g/>
static	statice	k1gFnPc2
pro	pro	k7c4
něž	jenž	k3xRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
nejméně	málo	k6eAd3
5	#num#	k4
družic	družice	k1gFnPc2
jednoho	jeden	k4xCgMnSc4
GNSS	GNSS	kA
systému	systém	k1gInSc2
nebo	nebo	k8xC
6	#num#	k4
ze	z	k7c2
dvou	dva	k4xCgInPc2
systémů	systém	k1gInPc2
plně	plně	k6eAd1
viditelných	viditelný	k2eAgMnPc2d1
z	z	k7c2
přijímače	přijímač	k1gInSc2
base	basa	k1gFnSc3
i	i	k8xC
rover	rover	k1gMnSc1
<g/>
,	,	kIx,
méně	málo	k6eAd2
spolehlivé	spolehlivý	k2eAgNnSc1d1
float	float	k5eAaBmF,k5eAaImF,k5eAaPmF
<g/>
/	/	kIx~
<g/>
kinematic	kinematice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Měření	měření	k1gNnSc3
jsou	být	k5eAaImIp3nP
náchylná	náchylný	k2eAgFnSc1d1
na	na	k7c6
přerušení	přerušení	k1gNnSc6
kontinuity	kontinuita	k1gFnSc2
signálu	signál	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
odstranit	odstranit	k5eAaPmF
všechny	všechen	k3xTgFnPc4
systematické	systematický	k2eAgFnPc4d1
chyby	chyba	k1gFnPc4
jako	jako	k8xS,k8xC
<g/>
:	:	kIx,
ionosférickou	ionosférický	k2eAgFnSc4d1
a	a	k8xC
troposférickou	troposférický	k2eAgFnSc4d1
refrakci	refrakce	k1gFnSc4
<g/>
,	,	kIx,
chybu	chyba	k1gFnSc4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
nepřesné	přesný	k2eNgFnPc1d1
efemeridy	efemerida	k1gFnPc1
<g/>
,	,	kIx,
poloha	poloha	k1gFnSc1
a	a	k8xC
orientace	orientace	k1gFnSc1
fázového	fázový	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
antény	anténa	k1gFnSc2
<g/>
,	,	kIx,
vícecestné	vícecestný	k2eAgNnSc1d1
šíření	šíření	k1gNnSc1
<g/>
,	,	kIx,
zastínění	zastínění	k1gNnSc1
výhledu	výhled	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Používané	používaný	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
Statická	statický	k2eAgFnSc1d1
(	(	kIx(
<g/>
Static	statice	k1gFnPc2
<g/>
)	)	kIx)
-	-	kIx~
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
dlouhodobá	dlouhodobý	k2eAgNnPc4d1
měření	měření	k1gNnPc4
(	(	kIx(
<g/>
hodiny	hodina	k1gFnPc4
až	až	k8xS
dny	den	k1gInPc4
<g/>
)	)	kIx)
více	hodně	k6eAd2
referenčních	referenční	k2eAgMnPc2d1
přijímačů	přijímač	k1gMnPc2
a	a	k8xC
postprocesní	postprocesní	k2eAgFnSc2d1
korekce	korekce	k1gFnSc2
</s>
<s>
Rychlá	rychlý	k2eAgFnSc1d1
statická	statický	k2eAgFnSc1d1
(	(	kIx(
<g/>
Fast	Fastum	k1gNnPc2
static	statice	k1gFnPc2
<g/>
)	)	kIx)
-	-	kIx~
pro	pro	k7c4
měření	měření	k1gNnSc4
(	(	kIx(
<g/>
minuty	minuta	k1gFnPc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
dvojice	dvojice	k1gFnSc1
referenčních	referenční	k2eAgMnPc2d1
dvoufrekvenčních	dvoufrekvenční	k2eAgMnPc2d1
přijímačů	přijímač	k1gMnPc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
o	o	k7c6
známých	známý	k2eAgFnPc6d1
souřadnicích	souřadnice	k1gFnPc6
a	a	k8xC
vyhodnocení	vyhodnocení	k1gNnSc6
postprocesních	postprocesní	k2eAgFnPc2d1
korekcí	korekce	k1gFnPc2
</s>
<s>
Stop	stop	k1gInSc1
and	and	k?
go	go	k?
(	(	kIx(
<g/>
polokinematická	polokinematický	k2eAgFnSc1d1
<g/>
)	)	kIx)
-	-	kIx~
měření	měření	k1gNnSc1
(	(	kIx(
<g/>
sekundy	sekunda	k1gFnPc1
<g/>
)	)	kIx)
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c6
principu	princip	k1gInSc6
jednoho	jeden	k4xCgInSc2
referenčního	referenční	k2eAgInSc2d1
a	a	k8xC
jednoho	jeden	k4xCgInSc2
terénního	terénní	k2eAgInSc2d1
přijímače	přijímač	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS
fáze	fáze	k1gFnPc4
i	i	k9
během	během	k7c2
přesunu	přesun	k1gInSc2
mezi	mezi	k7c7
měřenými	měřený	k2eAgNnPc7d1
stanovišti	stanoviště	k1gNnPc7
</s>
<s>
Kinematická	kinematický	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kinematic	Kinematice	k1gFnPc2
<g/>
)	)	kIx)
-	-	kIx~
vyžaduje	vyžadovat	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
terénní	terénní	k2eAgMnSc1d1
přijímač	přijímač	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
na	na	k7c6
počátku	počátek	k1gInSc6
vyřeší	vyřešit	k5eAaPmIp3nP
nejednoznačnosti	nejednoznačnost	k1gFnPc1
(	(	kIx(
<g/>
inicializace	inicializace	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
ambiguity	ambiguita	k1gFnPc4
úspěšně	úspěšně	k6eAd1
řešit	řešit	k5eAaImF
i	i	k9
během	během	k7c2
přesunu	přesun	k1gInSc2
mezi	mezi	k7c7
měřenými	měřený	k2eAgNnPc7d1
stanovišti	stanoviště	k1gNnPc7
(	(	kIx(
<g/>
bez	bez	k7c2
inicializace	inicializace	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
RTK	RTK	kA
(	(	kIx(
<g/>
Real	Real	k1gInSc1
Time	Time	k1gNnSc1
Kinematic	Kinematice	k1gFnPc2
<g/>
)	)	kIx)
-	-	kIx~
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
metodu	metoda	k1gFnSc4
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
jedním	jeden	k4xCgInSc7
přijímačem	přijímač	k1gInSc7
v	v	k7c6
terénu	terén	k1gInSc6
zpracovávány	zpracováván	k2eAgInPc4d1
RTCM	RTCM	kA
diferenciální	diferenciální	k2eAgFnSc2d1
korekce	korekce	k1gFnSc2
permanentních	permanentní	k2eAgFnPc2d1
referenčních	referenční	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
získané	získaný	k2eAgFnSc2d1
z	z	k7c2
geostacionární	geostacionární	k2eAgFnSc2d1
družice	družice	k1gFnSc2
<g/>
,	,	kIx,
rádia	rádio	k1gNnSc2
nebo	nebo	k8xC
internetu	internet	k1gInSc2
a	a	k8xC
to	ten	k3xDgNnSc1
jako	jako	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
plošné	plošný	k2eAgFnPc1d1
korekce	korekce	k1gFnPc1
</s>
<s>
korekce	korekce	k1gFnSc1
blízké	blízký	k2eAgFnSc2d1
virtuální	virtuální	k2eAgFnSc2d1
referenční	referenční	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
vypočtené	vypočtený	k2eAgFnSc6d1
ze	z	k7c2
síťového	síťový	k2eAgNnSc2d1
řešení	řešení	k1gNnSc2
</s>
<s>
měřená	měřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
referenční	referenční	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
(	(	kIx(
<g/>
výpočet	výpočet	k1gInSc1
korekcí	korekce	k1gFnSc7
proveden	provést	k5eAaPmNgInS
u	u	k7c2
příjemce	příjemce	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Dopplerovská	Dopplerovský	k2eAgFnSc1d1
</s>
<s>
Měření	měření	k1gNnSc1
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c6
principu	princip	k1gInSc6
zjišťování	zjišťování	k1gNnSc2
změny	změna	k1gFnSc2
frekvence	frekvence	k1gFnSc2
pro	pro	k7c4
pohybující	pohybující	k2eAgInSc4d1
se	se	k3xPyFc4
zdroj	zdroj	k1gInSc4
(	(	kIx(
<g/>
nebo	nebo	k8xC
i	i	k9
příjemce	příjemce	k1gMnSc4
<g/>
)	)	kIx)
signálu	signál	k1gInSc2
(	(	kIx(
<g/>
dopplerův	dopplerův	k2eAgInSc1d1
efekt	efekt	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
údajů	údaj	k1gInPc2
z	z	k7c2
jedné	jeden	k4xCgFnSc2
družice	družice	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
aktuálního	aktuální	k2eAgNnSc2d1
orbitu	orbita	k1gFnSc4
družice	družice	k1gFnPc4
</s>
<s>
změna	změna	k1gFnSc1
frekvence	frekvence	k1gFnSc2
jejího	její	k3xOp3gNnSc2
vysílání	vysílání	k1gNnSc2
proti	proti	k7c3
výchozí	výchozí	k2eAgFnSc3d1
</s>
<s>
sledováním	sledování	k1gNnSc7
dvou	dva	k4xCgFnPc2
frekvencí	frekvence	k1gFnPc2
</s>
<s>
časových	časový	k2eAgFnPc2d1
značek	značka	k1gFnPc2
</s>
<s>
několika	několik	k4yIc2
měření	měření	k1gNnPc2
během	během	k7c2
jednoho	jeden	k4xCgInSc2
přeletu	přelet	k1gInSc2
</s>
<s>
lze	lze	k6eAd1
vypočítat	vypočítat	k5eAaPmF
relativní	relativní	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
vůči	vůči	k7c3
družici	družice	k1gFnSc3
ve	v	k7c6
dvojrozměrném	dvojrozměrný	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
dopočítat	dopočítat	k5eAaPmF
následně	následně	k6eAd1
polohu	poloha	k1gFnSc4
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
rychlost	rychlost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
trojrozměrnou	trojrozměrný	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
měření	měření	k1gNnPc4
z	z	k7c2
více	hodně	k6eAd2
družic	družice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílání	vysílání	k1gNnSc6
družice	družice	k1gFnSc2
lze	lze	k6eAd1
využít	využít	k5eAaPmF
i	i	k9
pro	pro	k7c4
časovou	časový	k2eAgFnSc4d1
synchronizaci	synchronizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Úhloměrná	Úhloměrný	k2eAgFnSc1d1
</s>
<s>
Měření	měření	k1gNnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
možnosti	možnost	k1gFnSc2
zaměřovat	zaměřovat	k5eAaImF
zdroj	zdroj	k1gInSc1
signálu	signál	k1gInSc2
(	(	kIx(
<g/>
družici	družice	k1gFnSc4
<g/>
)	)	kIx)
pomocí	pomocí	k7c2
směrových	směrový	k2eAgFnPc2d1
antén	anténa	k1gFnPc2
a	a	k8xC
určit	určit	k5eAaPmF
úhly	úhel	k1gInPc4
vzhledem	vzhledem	k7c3
vodorovné	vodorovný	k2eAgFnSc3d1
rovině	rovina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provádí	provádět	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
více	hodně	k6eAd2
družicím	družice	k1gFnPc3
zároveň	zároveň	k6eAd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
k	k	k7c3
jedné	jeden	k4xCgFnSc3
družici	družice	k1gFnSc3
v	v	k7c6
různém	různý	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
z	z	k7c2
důvodu	důvod	k1gInSc2
komplikovaného	komplikovaný	k2eAgNnSc2d1
řešení	řešení	k1gNnSc2
a	a	k8xC
malé	malý	k2eAgFnSc2d1
přesnosti	přesnost	k1gFnSc2
nepoužívá	používat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Rozšiřující	rozšiřující	k2eAgInPc1d1
systémy	systém	k1gInPc1
GNSS	GNSS	kA
</s>
<s>
SBAS	SBAS	kA
</s>
<s>
SBAS	SBAS	kA
(	(	kIx(
<g/>
Satellite	Satellit	k1gInSc5
Based	Based	k1gInSc4
Augmentation	Augmentation	k1gInSc1
Systems	Systems	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
obecný	obecný	k2eAgInSc4d1
název	název	k1gInSc4
pro	pro	k7c4
systém	systém	k1gInSc4
pozemních	pozemní	k2eAgFnPc2d1
monitorovacích	monitorovací	k2eAgFnPc2d1
rozsáhlejší	rozsáhlý	k2eAgFnSc3d2
původnímu	původní	k2eAgInSc3d1
monitorovacímu	monitorovací	k2eAgInSc3d1
segmentu	segment	k1gInSc3
GNSS	GNSS	kA
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
vyhodnocují	vyhodnocovat	k5eAaImIp3nP
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
kosmického	kosmický	k2eAgInSc2d1
segmentu	segment	k1gInSc2
GNSS	GNSS	kA
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
GPS	GPS	kA
<g/>
+	+	kIx~
<g/>
GLONASS	GLONASS	kA
<g/>
)	)	kIx)
a	a	k8xC
stav	stav	k1gInSc4
ionosféry	ionosféra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypočítávají	vypočítávat	k5eAaImIp3nP
korekce	korekce	k1gFnPc1
těchto	tento	k3xDgInPc2
vlivů	vliv	k1gInPc2
a	a	k8xC
tato	tento	k3xDgNnPc4
data	datum	k1gNnPc4
s	s	k7c7
malým	malý	k2eAgNnSc7d1
časovým	časový	k2eAgNnSc7d1
zpožděním	zpoždění	k1gNnSc7
vysílají	vysílat	k5eAaImIp3nP
k	k	k7c3
uživatelům	uživatel	k1gMnPc3
skrze	skrze	k?
družice	družice	k1gFnPc1
na	na	k7c6
geostacionární	geostacionární	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geostacionární	geostacionární	k2eAgFnPc4d1
družice	družice	k1gFnPc4
mají	mít	k5eAaImIp3nP
čísla	číslo	k1gNnPc4
#	#	kIx~
<g/>
ID	Ida	k1gFnPc2
nad	nad	k7c4
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevýhodou	nevýhoda	k1gFnSc7
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
umístění	umístění	k1gNnSc4
družic	družice	k1gFnPc2
nad	nad	k7c7
rovníkem	rovník	k1gInSc7
(	(	kIx(
<g/>
v	v	k7c6
Česku	Česko	k1gNnSc6
nízko	nízko	k6eAd1
nad	nad	k7c7
jižním	jižní	k2eAgInSc7d1
horizontem	horizont	k1gInSc7
<g/>
)	)	kIx)
se	s	k7c7
slabým	slabý	k2eAgInSc7d1
vysílacím	vysílací	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
určuje	určovat	k5eAaImIp3nS
použití	použití	k1gNnSc4
jen	jen	k9
pro	pro	k7c4
leteckou	letecký	k2eAgFnSc4d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
námořní	námořní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Globální	globální	k2eAgFnSc1d1
SBAS	SBAS	kA
jsou	být	k5eAaImIp3nP
komerční	komerční	k2eAgInPc1d1
produkty	produkt	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Omnistar	Omnistar	k1gMnSc1
</s>
<s>
StarFire	StarFir	k1gMnSc5
</s>
<s>
Starfix	Starfix	k1gInSc1
</s>
<s>
Regionální	regionální	k2eAgInPc1d1
SBAS	SBAS	kA
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
zřizovány	zřizovat	k5eAaImNgInP
vládními	vládní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
a	a	k8xC
jsou	být	k5eAaImIp3nP
volně	volně	k6eAd1
dostupné	dostupný	k2eAgFnPc1d1
(	(	kIx(
<g/>
název	název	k1gInSc1
<g/>
:	:	kIx,
<g/>
plánovaný	plánovaný	k2eAgInSc1d1
počet	počet	k1gInSc1
družic	družice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
WAAS	WAAS	kA
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
US	US	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Wide	Wide	k1gFnSc1
Area	area	k1gFnSc1
Augmentation	Augmentation	k1gInSc4
System	Systo	k1gNnSc7
</s>
<s>
EGNOS	EGNOS	kA
<g/>
:	:	kIx,
<g/>
3	#num#	k4
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
European	European	k1gMnSc1
Geostationary	Geostationara	k1gFnSc2
Navigation	Navigation	k1gInSc1
Overlay	Overlaa	k1gFnSc2
Service	Service	k1gFnSc2
</s>
<s>
MSAS	MSAS	kA
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
Japonsko	Japonsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
MTSAT	MTSAT	kA
Satellite-Based	Satellite-Based	k1gInSc1
Augmentation	Augmentation	k1gInSc1
System	Systo	k1gNnSc7
</s>
<s>
GAGAN	GAGAN	kA
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
Indie	Indie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
GPS	GPS	kA
Aided	Aided	k1gMnSc1
Geo	Geo	k1gMnSc1
Augmented	Augmented	k1gMnSc1
Navigation	Navigation	k1gInSc4
</s>
<s>
CWAAS	CWAAS	kA
<g/>
:	:	kIx,
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
Kanada	Kanada	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Canadian	Canadian	k1gMnSc1
WAAS	WAAS	kA
</s>
<s>
SNAS	SNAS	kA
<g/>
:	:	kIx,
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
Čína	Čína	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Satellite	Satellit	k1gInSc5
Navigation	Navigation	k1gInSc1
Augmentation	Augmentation	k1gInSc4
System	Syst	k1gInSc7
</s>
<s>
GBAS	GBAS	kA
</s>
<s>
GBAS	GBAS	kA
(	(	kIx(
<g/>
Ground	Ground	k1gInSc1
Based	Based	k1gInSc1
Augmentation	Augmentation	k1gInSc1
Systems	Systems	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
označované	označovaný	k2eAgFnPc1d1
někdy	někdy	k6eAd1
jako	jako	k9
GRAS	GRAS	kA
(	(	kIx(
<g/>
Ground-based	Ground-based	k1gMnSc1
Regional	Regional	k1gFnSc2
Augmentation	Augmentation	k1gInSc1
Systems	Systems	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
obecný	obecný	k2eAgInSc4d1
název	název	k1gInSc4
pro	pro	k7c4
systém	systém	k1gInSc4
pozemních	pozemní	k2eAgFnPc2d1
referenčních	referenční	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
vyhodnocují	vyhodnocovat	k5eAaImIp3nP
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
kosmického	kosmický	k2eAgInSc2d1
segmentu	segment	k1gInSc2
GNSS	GNSS	kA
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
GPS	GPS	kA
<g/>
+	+	kIx~
<g/>
GLONASS	GLONASS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypočítávají	vypočítávat	k5eAaImIp3nP
korekce	korekce	k1gFnPc4
vzhledem	vzhledem	k7c3
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
absolutní	absolutní	k2eAgFnSc3d1
poloze	poloha	k1gFnSc3
a	a	k8xC
poskytují	poskytovat	k5eAaImIp3nP
je	být	k5eAaImIp3nS
uživatelům	uživatel	k1gMnPc3
pomocí	pomocí	k7c2
mobilních	mobilní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
<g/>
,	,	kIx,
radiových	radiový	k2eAgNnPc2d1
vysílání	vysílání	k1gNnPc2
nebo	nebo	k8xC
až	až	k6eAd1
zpětně	zpětně	k6eAd1
pro	pro	k7c4
korekce	korekce	k1gFnPc4
prováděné	prováděný	k2eAgFnPc4d1
po	po	k7c6
skončení	skončení	k1gNnSc6
měření	měření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Regionální	regionální	k2eAgInPc1d1
GBAS	GBAS	kA
jsou	být	k5eAaImIp3nP
např.	např.	kA
<g/>
:	:	kIx,
</s>
<s>
GRAS	GRAS	kA
(	(	kIx(
<g/>
Austrálie	Austrálie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
DGPS	DGPS	kA
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
WAGE	WAGE	kA
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
EUREF	EUREF	kA
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
</s>
<s>
CZEPOS	CZEPOS	kA
(	(	kIx(
<g/>
Česko	Česko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
-ceska	-ceska	k1gFnSc1
republika-cestina	republika-cestina	k1gFnSc1
</s>
<s>
Místní	místní	k2eAgInPc1d1
GBAS	GBAS	kA
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
značené	značený	k2eAgFnSc2d1
jako	jako	k8xS,k8xC
LAAS	LAAS	kA
(	(	kIx(
<g/>
Local	Local	k1gMnSc1
Area	Ares	k1gMnSc2
Augmentation	Augmentation	k1gInSc1
System	Syst	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
typicky	typicky	k6eAd1
jako	jako	k8xC,k8xS
osamělé	osamělý	k2eAgFnPc1d1
referenční	referenční	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
<g/>
,	,	kIx,
např.	např.	kA
u	u	k7c2
letišť	letiště	k1gNnPc2
<g/>
,	,	kIx,
dolů	dol	k1gInPc2
<g/>
,	,	kIx,
rozsáhlých	rozsáhlý	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
</s>
<s>
IGS	IGS	kA
</s>
<s>
IGS	IGS	kA
(	(	kIx(
<g/>
International	International	k1gMnSc1
GNSS	GNSS	kA
Service	Service	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
sleduje	sledovat	k5eAaImIp3nS
a	a	k8xC
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS
kosmické	kosmický	k2eAgInPc4d1
segmenty	segment	k1gInPc4
GNSS	GNSS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčovými	klíčový	k2eAgInPc7d1
produkty	produkt	k1gInPc7
tří	tři	k4xCgFnPc2
stovek	stovka	k1gFnPc2
stanic	stanice	k1gFnPc2
jsou	být	k5eAaImIp3nP
zpětně	zpětně	k6eAd1
dopočtené	dopočtený	k2eAgFnPc1d1
a	a	k8xC
velmi	velmi	k6eAd1
přesné	přesný	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
efemeridy	efemerida	k1gFnSc2
družic	družice	k1gFnPc2
GPS	GPS	kA
<g/>
/	/	kIx~
<g/>
GLONASS	GLONASS	kA
~	~	kIx~
<g/>
5	#num#	k4
cm	cm	kA
<g/>
/	/	kIx~
<g/>
15	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
predikce	predikce	k1gFnSc1
v	v	k7c6
navigační	navigační	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
GPS	GPS	kA
<g/>
:	:	kIx,
~	~	kIx~
<g/>
160	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
</s>
<s>
přesné	přesný	k2eAgFnPc1d1
korekce	korekce	k1gFnPc1
pro	pro	k7c4
palubní	palubní	k2eAgFnPc4d1
hodiny	hodina	k1gFnPc4
GPS	GPS	kA
~	~	kIx~
<g/>
0,1	0,1	k4
<g/>
ns	ns	k?
(	(	kIx(
<g/>
predikce	predikce	k1gFnSc1
v	v	k7c6
navigační	navigační	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
GPS	GPS	kA
<g/>
:	:	kIx,
~	~	kIx~
<g/>
7	#num#	k4
<g/>
ns	ns	k?
<g/>
)	)	kIx)
</s>
<s>
ionosférické	ionosférický	k2eAgNnSc1d1
a	a	k8xC
troposférické	troposférický	k2eAgNnSc1d1
zpoždění	zpoždění	k1gNnSc1
</s>
<s>
3D	3D	k4
souřadnice	souřadnice	k1gFnSc1
monitorovacích	monitorovací	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
~	~	kIx~
<g/>
3	#num#	k4
<g/>
mm	mm	kA
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
pohyb	pohyb	k1gInSc1
~	~	kIx~
<g/>
2	#num#	k4
<g/>
mm	mm	kA
<g/>
/	/	kIx~
<g/>
rok	rok	k1gInSc1
</s>
<s>
parametry	parametr	k1gInPc1
rotace	rotace	k1gFnSc2
Země	zem	k1gFnSc2
(	(	kIx(
<g/>
Earth	Earth	k1gInSc1
Rotation	Rotation	k1gInSc1
Parameters	Parameters	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ILRS	ILRS	kA
</s>
<s>
ILRS	ILRS	kA
(	(	kIx(
<g/>
International	International	k1gFnSc1
Laser	laser	k1gInSc4
Ranging	Ranging	k1gInSc1
Service	Service	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vyvíjená	vyvíjený	k2eAgFnSc1d1
služba	služba	k1gFnSc1
umožňující	umožňující	k2eAgFnSc1d1
nezávislé	závislý	k2eNgNnSc4d1
zjišťování	zjišťování	k1gNnSc4
polohy	poloha	k1gFnSc2
družic	družice	k1gFnPc2
na	na	k7c6
oběžné	oběžný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
za	za	k7c2
pomoci	pomoc	k1gFnSc2
laserových	laserový	k2eAgNnPc2d1
měřidel	měřidlo	k1gNnPc2
(	(	kIx(
<g/>
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
vysílaný	vysílaný	k2eAgInSc4d1
rádiový	rádiový	k2eAgInSc4d1
signál	signál	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
měření	měření	k1gNnSc4
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
vybavit	vybavit	k5eAaPmF
družici	družice	k1gFnSc4
odražečem	odražeč	k1gInSc7
a	a	k8xC
jejich	jejich	k3xOp3gNnSc1
nasazení	nasazení	k1gNnSc1
je	být	k5eAaImIp3nS
plánované	plánovaný	k2eAgNnSc1d1
pro	pro	k7c4
GNSS	GNSS	kA
II	II	kA
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Realizovaná	realizovaný	k2eAgNnPc4d1
řešení	řešení	k1gNnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
GNSSpočet	GNSSpočet	k1gInSc1
vybavených	vybavený	k2eAgInPc2d1
družicpopis	družicpopis	k1gInSc1
</s>
<s>
GLONASS51	GLONASS51	k4
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
č.	č.	k?
40	#num#	k4
<g/>
-	-	kIx~
<g/>
108	#num#	k4
<g/>
)	)	kIx)
<g/>
délka	délka	k1gFnSc1
úhlopříčky	úhlopříčka	k1gFnSc2
zrcadla	zrcadlo	k1gNnSc2
60	#num#	k4
cm	cm	kA
</s>
<s>
GPS2	GPS2	k4
(	(	kIx(
<g/>
PRN	PRN	kA
05	#num#	k4
a	a	k8xC
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
10	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
32	#num#	k4
buněk	buňka	k1gFnPc2
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
úhlopříčky	úhlopříčka	k1gFnSc2
zrcadla	zrcadlo	k1gNnSc2
20	#num#	k4
cm	cm	kA
</s>
<s>
Galileo	Galilea	k1gFnSc5
<g/>
3	#num#	k4
(	(	kIx(
<g/>
GIOVE-A	GIOVE-A	k1gFnSc2
<g/>
,	,	kIx,
B	B	kA
<g/>
,	,	kIx,
A	A	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
76	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
buněk	buňka	k1gFnPc2
<g/>
;	;	kIx,
30	#num#	k4
<g/>
×	×	k?
<g/>
40	#num#	k4
cm	cm	kA
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
×	×	k?
<g/>
30	#num#	k4
cm	cm	kA
</s>
<s>
Compass	Compass	k1gInSc1
<g/>
1	#num#	k4
(	(	kIx(
<g/>
M	M	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
2,5	2,5	k4
kg	kg	kA
<g/>
;	;	kIx,
42	#num#	k4
buněk	buňka	k1gFnPc2
<g/>
;	;	kIx,
efektivní	efektivní	k2eAgFnSc1d1
odrazivá	odrazivý	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
360	#num#	k4
cm²	cm²	k?
<g/>
,	,	kIx,
32	#num#	k4
<g/>
×	×	k?
<g/>
28	#num#	k4
cm	cm	kA
</s>
<s>
Regionání	Regionání	k1gNnSc1
NSS	NSS	kA
</s>
<s>
Regionální	regionální	k2eAgInPc1d1
navigační	navigační	k2eAgInPc1d1
družicové	družicový	k2eAgInPc1d1
systémy	systém	k1gInPc1
často	často	k6eAd1
doplňují	doplňovat	k5eAaImIp3nP
GNSS	GNSS	kA
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
vývojový	vývojový	k2eAgInSc4d1
předstupeň	předstupeň	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
QZSS	QZSS	kA
(	(	kIx(
<g/>
Quasi-Zenith	Quasi-Zenith	k1gMnSc1
Satellite	Satellit	k1gInSc5
System	Syst	k1gInSc7
in	in	k?
Japan	japan	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
4	#num#	k4
družice	družice	k1gFnSc1
<g/>
,	,	kIx,
zprovoznění	zprovoznění	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
</s>
<s>
IRNSS	IRNSS	kA
(	(	kIx(
<g/>
Indian	Indiana	k1gFnPc2
Regional	Regional	k1gFnPc2
Navigation	Navigation	k1gInSc1
Satellite	Satellit	k1gInSc5
System	Systo	k1gNnSc7
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
družic	družice	k1gFnPc2
<g/>
,	,	kIx,
zprovoznění	zprovoznění	k1gNnSc4
červen	červen	k1gInSc1
2016	#num#	k4
</s>
<s>
Beidou	Beiít	k5eAaPmIp3nP
<g/>
:	:	kIx,
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
realizován	realizován	k2eAgInSc1d1
(	(	kIx(
<g/>
globální	globální	k2eAgFnSc1d1
služba	služba	k1gFnSc1
je	být	k5eAaImIp3nS
plánována	plánovat	k5eAaImNgFnS
na	na	k7c4
rok	rok	k1gInSc4
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://mobil.idnes.cz/cina-spousti-navigacni-system-pej-tou-dxz-/navigace.aspx?c=A121228_163812_navigace_jm	http://mobil.idnes.cz/cina-spousti-navigacni-system-pej-tou-dxz-/navigace.aspx?c=A121228_163812_navigace_jm	k1gInSc1
–	–	k?
Čína	Čína	k1gFnSc1
dává	dávat	k5eAaImIp3nS
sbohem	sbohem	k0
GPS	GPS	kA
<g/>
,	,	kIx,
navigovat	navigovat	k5eAaImF
ji	on	k3xPp3gFnSc4
bude	být	k5eAaImBp3nS
„	„	k?
<g/>
souhvězdí	souhvězdí	k1gNnPc2
Velký	velký	k2eAgInSc1d1
vůz	vůz	k1gInSc1
<g/>
“	“	k?
<g/>
↑	↑	k?
LUŇÁK	Luňák	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Testování	testování	k1gNnSc4
přesnosti	přesnost	k1gFnSc2
měření	měření	k1gNnSc2
GPS	GPS	kA
metodou	metoda	k1gFnSc7
RTK	RTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
45	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
aplikovaných	aplikovaný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
ZČU	ZČU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Karel	Karel	k1gMnSc1
Jedlička	Jedlička	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
2	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
http://gis.vsb.cz/vojtek/index.php?page=gnps/cviceni05	http://gis.vsb.cz/vojtek/index.php?page=gnps/cviceni05	k4
<g/>
↑	↑	k?
Kostelecký	Kostelecký	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
přednesy	přednes	k1gInPc1
Vyšší	vysoký	k2eAgFnSc2d2
geodézie	geodézie	k1gFnSc2
1	#num#	k4
Princip	princip	k1gInSc1
zpracování	zpracování	k1gNnSc2
měření	měření	k1gNnSc2
GPS	GPS	kA
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FSV	FSV	kA
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2007	#num#	k4
<g/>
↑	↑	k?
Koukl	kouknout	k5eAaPmAgMnS
J.	J.	kA
<g/>
:	:	kIx,
Metody	metoda	k1gFnPc1
kterými	který	k3yRgMnPc7,k3yQgMnPc7,k3yIgMnPc7
lze	lze	k6eAd1
z	z	k7c2
GPS	GPS	kA
dostat	dostat	k5eAaPmF
milimetry	milimetr	k1gInPc4
Archivováno	archivován	k2eAgNnSc1d1
13	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
výtah	výtah	k1gInSc4
z	z	k7c2
DP	DP	kA
<g/>
:	:	kIx,
„	„	k?
<g/>
Společné	společný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
měření	měření	k1gNnSc2
totální	totální	k2eAgFnSc7d1
stanicí	stanice	k1gFnSc7
a	a	k8xC
GPS	GPS	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČVUT	ČVUT	kA
1999	#num#	k4
<g/>
↑	↑	k?
Kostelecký	Kostelecký	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
přednesy	přednes	k1gInPc1
Vyšší	vysoký	k2eAgFnSc2d2
geodézie	geodézie	k1gFnSc2
1	#num#	k4
Diferenciální	diferenciální	k2eAgNnSc1d1
GPS	GPS	kA
(	(	kIx(
<g/>
DGPS	DGPS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FSV	FSV	kA
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2007	#num#	k4
<g/>
↑	↑	k?
Ray	Ray	k1gMnSc5
Clore	Clor	k1gMnSc5
<g/>
:	:	kIx,
Worldwide	Worldwid	k1gInSc5
GNSS	GNSS	kA
Interoperability	interoperabilita	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
28	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
U.	U.	kA
<g/>
S.	S.	kA
State	status	k1gInSc5
Department	department	k1gInSc1
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
<g/>
↑	↑	k?
GPS	GPS	kA
World	World	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Almanac	Almanac	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
11	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
1	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2008	#num#	k4
<g/>
↑	↑	k?
IGS	IGS	kA
<g/>
:	:	kIx,
IGS	IGS	kA
Products	Products	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
20	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2008	#num#	k4
<g/>
↑	↑	k?
Beutler	Beutler	k1gMnSc1
G.	G.	kA
<g/>
:	:	kIx,
GPS	GPS	kA
and	and	k?
GNSS	GNSS	kA
from	from	k1gMnSc1
the	the	k?
International	International	k1gMnSc1
Geosciences	Geosciences	k1gMnSc1
Perspective	Perspectiv	k1gInSc5
Archivováno	archivován	k2eAgNnSc4d1
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
;	;	kIx,
ILRS	ILRS	kA
<g/>
;	;	kIx,
str	str	kA
<g/>
.	.	kIx.
17	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
↑	↑	k?
ILRS	ILRS	kA
Home	Hom	k1gFnSc2
<g/>
:	:	kIx,
List	list	k1gInSc1
of	of	k?
Satellites	Satellites	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Rapant	Rapant	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
:	:	kIx,
Družicové	družicový	k2eAgInPc1d1
polohové	polohový	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VŠB-TU	VŠB-TU	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
200	#num#	k4
str	str	kA
<g/>
.	.	kIx.
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
248	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
124	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
dostupné	dostupný	k2eAgInPc1d1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Geocaching	Geocaching	k1gInSc1
</s>
<s>
Degree	Degree	k1gFnSc1
Confluence	Confluence	k1gFnSc2
Project	Projecta	k1gFnPc2
</s>
<s>
OpenStreetMap	OpenStreetMap	k1gMnSc1
</s>
<s>
http://airnav.eu/index.php?stranka=gnss	http://airnav.eu/index.php?stranka=gnss	k6eAd1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Globální	globální	k2eAgInPc4d1
družicové	družicový	k2eAgInPc4d1
polohové	polohový	k2eAgInPc4d1
systémy	systém	k1gInPc4
(	(	kIx(
<g/>
GNSS	GNSS	kA
<g/>
)	)	kIx)
Zastaralé	zastaralý	k2eAgFnSc2d1
</s>
<s>
Transit	transit	k1gInSc1
•	•	k?
PARUS	PARUS	kA
•	•	k?
CIKADA	CIKADA	kA
Provozované	provozovaný	k2eAgNnSc1d1
</s>
<s>
GPS	GPS	kA
•	•	k?
GLONASS	GLONASS	kA
•	•	k?
DORIS	DORIS	kA
Ve	v	k7c6
vývoji	vývoj	k1gInSc6
</s>
<s>
Galileo	Galilea	k1gFnSc5
•	•	k?
BeiDou	BeiDa	k1gMnSc7
•	•	k?
QZSS	QZSS	kA
•	•	k?
IRNSS	IRNSS	kA
Rozšiřující	rozšiřující	k2eAgFnSc1d1
</s>
<s>
EGNOS	EGNOS	kA
•	•	k?
WAAS	WAAS	kA
•	•	k?
MSAS	MSAS	kA
•	•	k?
GAGAN	GAGAN	kA
•	•	k?
CWAAS	CWAAS	kA
Technologie	technologie	k1gFnSc2
</s>
<s>
SBAS	SBAS	kA
<g/>
,	,	kIx,
LAAS	LAAS	kA
<g/>
,	,	kIx,
DGPS	DGPS	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4202846-2	4202846-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
9837	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85008239	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85008239	#num#	k4
</s>
