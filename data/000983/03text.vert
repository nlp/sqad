<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1936	[number]	k4	1936
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
písňových	písňový	k2eAgMnPc2d1	písňový
textů	text	k1gInPc2	text
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
a	a	k8xC	a
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dokončil	dokončit	k5eAaPmAgMnS	dokončit
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
manželkou	manželka	k1gFnSc7	manželka
učit	učit	k5eAaImF	učit
do	do	k7c2	do
severozápadních	severozápadní	k2eAgFnPc2d1	severozápadní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Měcholup	Měcholup	k1gMnSc1	Měcholup
a	a	k8xC	a
poté	poté	k6eAd1	poté
do	do	k7c2	do
Žatce	Žatec	k1gInSc2	Žatec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
ale	ale	k8xC	ale
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
dráhu	dráha	k1gFnSc4	dráha
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
Československém	československý	k2eAgInSc6d1	československý
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
autorů	autor	k1gMnPc2	autor
rozhlasového	rozhlasový	k2eAgInSc2d1	rozhlasový
pořadu	pořad	k1gInSc2	pořad
Nealkoholická	alkoholický	k2eNgFnSc1d1	nealkoholická
vinárna	vinárna	k1gFnSc1	vinárna
U	u	k7c2	u
Pavouka	pavouk	k1gMnSc2	pavouk
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
postava	postava	k1gFnSc1	postava
nedoceněného	doceněný	k2eNgMnSc2d1	nedoceněný
českého	český	k2eAgMnSc2d1	český
velikána	velikán	k1gMnSc2	velikán
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
také	také	k9	také
poznal	poznat	k5eAaPmAgMnS	poznat
hudebníka	hudebník	k1gMnSc4	hudebník
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Uhlíře	Uhlíř	k1gMnSc4	Uhlíř
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
zkoumající	zkoumající	k2eAgInSc4d1	zkoumající
odkaz	odkaz	k1gInSc4	odkaz
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
divadlo	divadlo	k1gNnSc4	divadlo
připravil	připravit	k5eAaPmAgMnS	připravit
Svěrák	svěrák	k1gInSc4	svěrák
první	první	k4xOgFnSc4	první
hru	hra	k1gFnSc4	hra
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
Akt	akt	k1gInSc4	akt
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
třinácti	třináct	k4xCc3	třináct
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
spoluautorsky	spoluautorsky	k6eAd1	spoluautorsky
podílel	podílet	k5eAaImAgMnS	podílet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
hrách	hra	k1gFnPc6	hra
divadla	divadlo	k1gNnSc2	divadlo
také	také	k9	také
Svěrák	Svěrák	k1gMnSc1	Svěrák
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Smoljakem	Smoljak	k1gMnSc7	Smoljak
začali	začít	k5eAaPmAgMnP	začít
posléze	posléze	k6eAd1	posléze
psát	psát	k5eAaImF	psát
i	i	k9	i
filmové	filmový	k2eAgInPc4d1	filmový
scénáře	scénář	k1gInPc4	scénář
a	a	k8xC	a
v	v	k7c6	v
natočených	natočený	k2eAgInPc6d1	natočený
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
obvykle	obvykle	k6eAd1	obvykle
jako	jako	k8xS	jako
herci	herec	k1gMnPc1	herec
také	také	k9	také
objevovali	objevovat	k5eAaImAgMnP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
scénáře	scénář	k1gInPc1	scénář
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
filmy	film	k1gInPc4	film
Vrchní	vrchní	k1gFnSc2	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
</s>
<s>
nebo	nebo	k8xC	nebo
Tři	tři	k4xCgMnPc1	tři
veteráni	veterán	k1gMnPc1	veterán
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
psal	psát	k5eAaImAgMnS	psát
Svěrák	Svěrák	k1gMnSc1	Svěrák
i	i	k8xC	i
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
Svěrák	Svěrák	k1gMnSc1	Svěrák
také	také	k9	také
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Vítem	Vít	k1gMnSc7	Vít
Olmerem	Olmer	k1gMnSc7	Olmer
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInPc6	jehož
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
rovněž	rovněž	k9	rovněž
hrál	hrát	k5eAaImAgInS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
téhož	týž	k3xTgNnSc2	týž
století	století	k1gNnSc2	století
začal	začít	k5eAaPmAgMnS	začít
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
psát	psát	k5eAaImF	psát
scénáře	scénář	k1gInPc4	scénář
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
filmy	film	k1gInPc4	film
natáčel	natáčet	k5eAaImAgMnS	natáčet
jeho	jeho	k3xOp3gNnSc4	jeho
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
tak	tak	k9	tak
snímky	snímka	k1gFnSc2	snímka
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Kolja	Kolja	k1gFnSc1	Kolja
<g/>
,	,	kIx,	,
Tmavomodrý	tmavomodrý	k2eAgInSc1d1	tmavomodrý
svět	svět	k1gInSc1	svět
či	či	k8xC	či
Vratné	vratný	k2eAgFnPc1d1	vratná
lahve	lahev	k1gFnPc1	lahev
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Kolja	Koljum	k1gNnSc2	Koljum
získal	získat	k5eAaPmAgInS	získat
krom	krom	k7c2	krom
jiného	jiné	k1gNnSc2	jiné
také	také	k9	také
Oscara	Oscara	k1gFnSc1	Oscara
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
neanglicky	anglicky	k6eNd1	anglicky
mluvený	mluvený	k2eAgInSc4d1	mluvený
film	film	k1gInSc4	film
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
Centrem	centr	k1gInSc7	centr
Paraple	Paraple	k?	Paraple
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
pacienty	pacient	k1gMnPc4	pacient
s	s	k7c7	s
poraněním	poranění	k1gNnSc7	poranění
míchy	mícha	k1gFnSc2	mícha
odkázané	odkázaný	k2eAgFnSc2d1	odkázaná
na	na	k7c4	na
invalidní	invalidní	k2eAgInSc4d1	invalidní
vozík	vozík	k1gInSc4	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
dramat	drama	k1gNnPc2	drama
a	a	k8xC	a
filmových	filmový	k2eAgInPc2d1	filmový
scénářů	scénář	k1gInPc2	scénář
píše	psát	k5eAaImIp3nS	psát
též	též	k9	též
písňové	písňový	k2eAgInPc4d1	písňový
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
také	také	k9	také
zpívá	zpívat	k5eAaImIp3nS	zpívat
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
pisatelem	pisatel	k1gMnSc7	pisatel
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
autorem	autor	k1gMnSc7	autor
tří	tři	k4xCgFnPc2	tři
povídkových	povídkový	k2eAgFnPc2d1	povídková
knih	kniha	k1gFnPc2	kniha
nazvaných	nazvaný	k2eAgMnPc2d1	nazvaný
Povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnSc2d1	Nové
povídky	povídka	k1gFnSc2	povídka
a	a	k8xC	a
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k1gMnSc1	bos
<g/>
.	.	kIx.	.
</s>
<s>
Připravil	připravit	k5eAaPmAgInS	připravit
též	též	k9	též
televizní	televizní	k2eAgInSc1d1	televizní
večerníček	večerníček	k1gInSc1	večerníček
Radovanovy	Radovanův	k2eAgFnSc2d1	Radovanova
radovánky	radovánka	k1gFnSc2	radovánka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
získal	získat	k5eAaPmAgMnS	získat
několik	několik	k4yIc1	několik
ocenění	ocenění	k1gNnPc2	ocenění
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Svěrák	Svěrák	k1gMnSc1	Svěrák
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
Františka	František	k1gMnSc2	František
Svěráka	Svěrák	k1gMnSc2	Svěrák
<g/>
,	,	kIx,	,
pracovníka	pracovník	k1gMnSc2	pracovník
v	v	k7c6	v
rozvodných	rozvodný	k2eAgInPc6d1	rozvodný
energetických	energetický	k2eAgInPc6d1	energetický
závodech	závod	k1gInPc6	závod
na	na	k7c6	na
pražském	pražský	k2eAgMnSc6d1	pražský
Bohdalci	Bohdalec	k1gMnSc6	Bohdalec
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
manželky	manželka	k1gFnPc4	manželka
Růženy	Růžena	k1gFnSc2	Růžena
rozené	rozený	k2eAgFnSc2d1	rozená
Synkové	Synková	k1gFnSc2	Synková
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
zámečníkem	zámečník	k1gMnSc7	zámečník
a	a	k8xC	a
elektrikářem	elektrikář	k1gMnSc7	elektrikář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c4	na
vedoucího	vedoucí	k1gMnSc4	vedoucí
rozvodny	rozvodna	k1gFnSc2	rozvodna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgMnS	být
inženýrem	inženýr	k1gMnSc7	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgInS	dokázat
si	se	k3xPyFc3	se
například	například	k6eAd1	například
sám	sám	k3xTgInSc4	sám
sestrojit	sestrojit	k5eAaPmF	sestrojit
televizor	televizor	k1gInSc4	televizor
<g/>
.	.	kIx.	.
</s>
<s>
Růžena	Růžena	k1gFnSc1	Růžena
Svěráková	Svěráková	k1gFnSc1	Svěráková
pracovala	pracovat	k5eAaImAgFnS	pracovat
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
si	se	k3xPyFc3	se
zakládal	zakládat	k5eAaImAgMnS	zakládat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vydělat	vydělat	k5eAaPmF	vydělat
tolik	tolik	k4yIc4	tolik
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
mohla	moct	k5eAaImAgFnS	moct
zůstávat	zůstávat	k5eAaImF	zůstávat
doma	doma	k6eAd1	doma
a	a	k8xC	a
pečovat	pečovat	k5eAaImF	pečovat
o	o	k7c4	o
výchovu	výchova	k1gFnSc4	výchova
jejich	jejich	k3xOp3gFnPc2	jejich
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
Svěrákovi	Svěrákův	k2eAgMnPc1d1	Svěrákův
se	se	k3xPyFc4	se
brali	brát	k5eAaImAgMnP	brát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
a	a	k8xC	a
nastávající	nastávající	k1gFnSc1	nastávající
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
těhotná	těhotný	k2eAgFnSc1d1	těhotná
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
ještě	ještě	k9	ještě
v	v	k7c6	v
témž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
jméno	jméno	k1gNnSc4	jméno
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
stoupl	stoupnout	k5eAaPmAgMnS	stoupnout
na	na	k7c4	na
rezavý	rezavý	k2eAgInSc4d1	rezavý
hřebík	hřebík	k1gInSc4	hřebík
a	a	k8xC	a
následně	následně	k6eAd1	následně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
sepsi	sepse	k1gFnSc6	sepse
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
ztrátu	ztráta	k1gFnSc4	ztráta
těžce	těžce	k6eAd1	těžce
nesli	nést	k5eAaImAgMnP	nést
a	a	k8xC	a
maminka	maminka	k1gFnSc1	maminka
se	se	k3xPyFc4	se
ze	z	k7c2	z
žalu	žal	k1gInSc2	žal
pokusila	pokusit	k5eAaPmAgNnP	pokusit
i	i	k9	i
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vypumpovali	vypumpovat	k5eAaPmAgMnP	vypumpovat
jí	on	k3xPp3gFnSc7	on
žaludek	žaludek	k1gInSc1	žaludek
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ji	on	k3xPp3gFnSc4	on
zachránili	zachránit	k5eAaPmAgMnP	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
Svěrákovým	Svěráková	k1gFnPc3	Svěráková
narodilo	narodit	k5eAaPmAgNnS	narodit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1936	[number]	k4	1936
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
také	také	k9	také
jméno	jméno	k1gNnSc1	jméno
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
o	o	k7c4	o
svého	svůj	k3xOyFgMnSc4	svůj
druhého	druhý	k4xOgMnSc4	druhý
syna	syn	k1gMnSc4	syn
po	po	k7c4	po
prožité	prožitý	k2eAgFnPc4d1	prožitá
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
prvním	první	k4xOgNnSc7	první
dítětem	dítě	k1gNnSc7	dítě
velmi	velmi	k6eAd1	velmi
báli	bát	k5eAaImAgMnP	bát
a	a	k8xC	a
úzkostlivě	úzkostlivě	k6eAd1	úzkostlivě
dbali	dbát	k5eAaImAgMnP	dbát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
něco	něco	k6eAd1	něco
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
ještě	ještě	k6eAd1	ještě
narodilo	narodit	k5eAaPmAgNnS	narodit
třetí	třetí	k4xOgNnSc1	třetí
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Božena	Božena	k1gFnSc1	Božena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
živila	živit	k5eAaImAgFnS	živit
jako	jako	k9	jako
prodavačka	prodavačka	k1gFnSc1	prodavačka
v	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
za	za	k7c4	za
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
počátku	počátek	k1gInSc6	počátek
bydlela	bydlet	k5eAaImAgFnS	bydlet
rodina	rodina	k1gFnSc1	rodina
Svěrákových	Svěráková	k1gFnPc2	Svěráková
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Bohdalci	Bohdalec	k1gMnPc5	Bohdalec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
služebním	služební	k2eAgInSc6d1	služební
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
patřil	patřit	k5eAaImAgInS	patřit
podniku	podnik	k1gInSc2	podnik
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
pracoval	pracovat	k5eAaImAgMnS	pracovat
Zdeňkův	Zdeňkův	k2eAgMnSc1d1	Zdeňkův
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
Němců	Němec	k1gMnPc2	Němec
se	se	k3xPyFc4	se
však	však	k9	však
byt	byt	k1gInSc1	byt
Svěrákových	Svěráková	k1gFnPc2	Svěráková
líbil	líbit	k5eAaImAgInS	líbit
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
o	o	k7c4	o
něj	on	k3xPp3gInSc4	on
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Označil	označit	k5eAaPmAgMnS	označit
proto	proto	k8xC	proto
Zdeňkova	Zdeňkův	k2eAgMnSc4d1	Zdeňkův
otce	otec	k1gMnSc4	otec
za	za	k7c4	za
nespolehlivého	spolehlivý	k2eNgMnSc4d1	nespolehlivý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
ho	on	k3xPp3gMnSc4	on
tak	tak	k6eAd1	tak
propustili	propustit	k5eAaPmAgMnP	propustit
ze	z	k7c2	z
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
musel	muset	k5eAaImAgInS	muset
vrátit	vrátit	k5eAaPmF	vrátit
užívaný	užívaný	k2eAgInSc1d1	užívaný
služební	služební	k2eAgInSc1d1	služební
byt	byt	k1gInSc1	byt
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
do	do	k7c2	do
otcova	otcův	k2eAgNnSc2d1	otcovo
rodiště	rodiště	k1gNnSc2	rodiště
<g/>
,	,	kIx,	,
do	do	k7c2	do
Kopidlna	Kopidlno	k1gNnSc2	Kopidlno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c6	na
Bohdalec	Bohdalec	k1gMnSc1	Bohdalec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
se	se	k3xPyFc4	se
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
chtěl	chtít	k5eAaImAgMnS	chtít
stát	stát	k5eAaPmF	stát
řidičem	řidič	k1gMnSc7	řidič
pekařského	pekařský	k2eAgNnSc2d1	pekařské
auta	auto	k1gNnSc2	auto
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
prezidentem	prezident	k1gMnSc7	prezident
nebo	nebo	k8xC	nebo
průvodčím	průvodčí	k1gMnSc7	průvodčí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c6	na
Bohdalci	Bohdalec	k1gMnSc6	Bohdalec
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
seděl	sedět	k5eAaImAgMnS	sedět
hned	hned	k6eAd1	hned
od	od	k7c2	od
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
v	v	k7c6	v
lavici	lavice	k1gFnSc6	lavice
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
kamarádem	kamarád	k1gMnSc7	kamarád
a	a	k8xC	a
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
kolegou	kolega	k1gMnSc7	kolega
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
Bořivojem	Bořivoj	k1gMnSc7	Bořivoj
Pencem	Pence	k1gMnSc7	Pence
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jeho	jeho	k3xOp3gMnSc7	jeho
nejstarším	starý	k2eAgMnSc7d3	nejstarší
kamarádem	kamarád	k1gMnSc7	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
se	se	k3xPyFc4	se
též	též	k9	též
učil	učít	k5eAaPmAgInS	učít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c4	na
tahací	tahací	k2eAgFnSc4d1	tahací
pianovou	pianový	k2eAgFnSc4d1	pianová
harmoniku	harmonika	k1gFnSc4	harmonika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
příliš	příliš	k6eAd1	příliš
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nešlo	jít	k5eNaImAgNnS	jít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
také	také	k9	také
rád	rád	k6eAd1	rád
četl	číst	k5eAaImAgMnS	číst
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
knížku	knížka	k1gFnSc4	knížka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
vypůjčil	vypůjčit	k5eAaPmAgMnS	vypůjčit
a	a	k8xC	a
přečetl	přečíst	k5eAaPmAgMnS	přečíst
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Zvířátka	zvířátko	k1gNnPc1	zvířátko
a	a	k8xC	a
Petrovští	petrovský	k2eAgMnPc1d1	petrovský
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
měli	mít	k5eAaImAgMnP	mít
svázané	svázaný	k2eAgNnSc4d1	svázané
celé	celý	k2eAgNnSc4d1	celé
dílo	dílo	k1gNnSc4	dílo
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgInPc1d1	doplněný
barevnými	barevný	k2eAgFnPc7d1	barevná
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Adolfa	Adolf	k1gMnSc2	Adolf
Kašpara	Kašpar	k1gMnSc2	Kašpar
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
rodičům	rodič	k1gMnPc3	rodič
nahlas	nahlas	k6eAd1	nahlas
předčítal	předčítat	k5eAaImAgMnS	předčítat
a	a	k8xC	a
asi	asi	k9	asi
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
zrodila	zrodit	k5eAaPmAgFnS	zrodit
jeho	jeho	k3xOp3gFnSc1	jeho
touha	touha	k1gFnSc1	touha
něco	něco	k3yInSc4	něco
přednášet	přednášet	k5eAaImF	přednášet
před	před	k7c7	před
publikem	publikum	k1gNnSc7	publikum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
dalším	další	k2eAgMnPc3d1	další
oblíbeným	oblíbený	k2eAgMnPc3d1	oblíbený
autorům	autor	k1gMnPc3	autor
patřil	patřit	k5eAaImAgInS	patřit
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
či	či	k8xC	či
Fráňa	Fráňa	k1gMnSc1	Fráňa
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
verš	verš	k1gInSc1	verš
"	"	kIx"	"
<g/>
Rozlomíš	rozlomit	k5eAaPmIp2nS	rozlomit
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
podáš	podat	k5eAaPmIp2nS	podat
ženě	žena	k1gFnSc6	žena
půl	půl	k1xP	půl
<g/>
"	"	kIx"	"
z	z	k7c2	z
básně	báseň	k1gFnSc2	báseň
Splav	splav	k1gInSc1	splav
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgInS	dát
i	i	k9	i
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
svatební	svatební	k2eAgNnSc4d1	svatební
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
Stalinovo	Stalinův	k2eAgNnSc1d1	Stalinovo
(	(	kIx(	(
<g/>
nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
gymnázium	gymnázium	k1gNnSc4	gymnázium
Voděradská	Voděradský	k2eAgFnSc1d1	Voděradská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Penc	Penc	k1gInSc1	Penc
šel	jít	k5eAaImAgInS	jít
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
o	o	k7c4	o
rok	rok	k1gInSc4	rok
níž	nízce	k6eAd2	nízce
chodil	chodit	k5eAaImAgMnS	chodit
také	také	k9	také
budoucí	budoucí	k2eAgMnSc1d1	budoucí
režisér	režisér	k1gMnSc1	režisér
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
.	.	kIx.	.
</s>
<s>
Zdejším	zdejší	k2eAgMnSc7d1	zdejší
Svěrákovým	Svěrákův	k2eAgMnSc7d1	Svěrákův
spolužákem	spolužák	k1gMnSc7	spolužák
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
i	i	k9	i
jistý	jistý	k2eAgInSc4d1	jistý
Oliverius	Oliverius	k1gInSc4	Oliverius
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
ale	ale	k8xC	ale
nenastoupil	nastoupit	k5eNaPmAgInS	nastoupit
a	a	k8xC	a
spolužáci	spolužák	k1gMnPc1	spolužák
ho	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
nikdy	nikdy	k6eAd1	nikdy
nepoznali	poznat	k5eNaPmAgMnP	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
postava	postava	k1gFnSc1	postava
neznámého	známý	k2eNgMnSc2d1	neznámý
studenta	student	k1gMnSc2	student
byla	být	k5eAaImAgFnS	být
posléze	posléze	k6eAd1	posléze
předobrazem	předobraz	k1gInSc7	předobraz
žáka	žák	k1gMnSc2	žák
Hliníka	Hliník	k1gMnSc2	Hliník
z	z	k7c2	z
filmu	film	k1gInSc2	film
Marečku	Mareček	k1gMnSc5	Mareček
<g/>
,	,	kIx,	,
podejte	podat	k5eAaPmRp2nP	podat
mi	já	k3xPp1nSc3	já
pero	pero	k1gNnSc1	pero
<g/>
!	!	kIx.	!
</s>
<s>
Na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
začal	začít	k5eAaPmAgMnS	začít
Svěrák	Svěrák	k1gMnSc1	Svěrák
psát	psát	k5eAaImF	psát
první	první	k4xOgFnPc1	první
humorné	humorný	k2eAgFnPc1d1	humorná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
bavil	bavit	k5eAaImAgMnS	bavit
své	svůj	k3xOyFgMnPc4	svůj
spolužáky	spolužák	k1gMnPc4	spolužák
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
dělá	dělat	k5eAaImIp3nS	dělat
radost	radost	k1gFnSc4	radost
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
smějí	smát	k5eAaImIp3nP	smát
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
ale	ale	k9	ale
sám	sám	k3xTgMnSc1	sám
nebyl	být	k5eNaImAgInS	být
spolužákům	spolužák	k1gMnPc3	spolužák
a	a	k8xC	a
kamarádům	kamarád	k1gMnPc3	kamarád
pro	pro	k7c4	pro
smích	smích	k1gInSc4	smích
<g/>
,	,	kIx,	,
uváděl	uvádět	k5eAaImAgMnS	uvádět
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
"	"	kIx"	"
<g/>
Tornillo	Tornillo	k1gNnSc1	Tornillo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
španělsky	španělsky	k6eAd1	španělsky
"	"	kIx"	"
<g/>
šroub	šroub	k1gInSc1	šroub
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgInS	pokoušet
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
o	o	k7c4	o
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
básní	báseň	k1gFnPc2	báseň
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Pětačtyřicet	pětačtyřicet	k4xCc4	pětačtyřicet
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
husí	husí	k2eAgFnSc6d1	husí
kůži	kůže	k1gFnSc6	kůže
a	a	k8xC	a
pojednávala	pojednávat	k5eAaImAgFnS	pojednávat
o	o	k7c6	o
Svěrákově	Svěrákův	k2eAgMnSc6d1	Svěrákův
učiteli	učitel	k1gMnSc6	učitel
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
deskriptivní	deskriptivní	k2eAgFnSc2d1	deskriptivní
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
vyučovací	vyučovací	k2eAgFnPc1d1	vyučovací
hodiny	hodina	k1gFnPc1	hodina
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
permanentním	permanentní	k2eAgInSc6d1	permanentní
strachu	strach	k1gInSc6	strach
<g/>
.	.	kIx.	.
</s>
<s>
Svěrákův	Svěrákův	k2eAgMnSc1d1	Svěrákův
otec	otec	k1gMnSc1	otec
chtěl	chtít	k5eAaImAgMnS	chtít
mít	mít	k5eAaImF	mít
ze	z	k7c2	z
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
elektroinženýra	elektroinženýr	k1gMnSc2	elektroinženýr
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Zhlédl	zhlédnout	k5eAaPmAgInS	zhlédnout
se	se	k3xPyFc4	se
v	v	k7c6	v
Aloisi	Alois	k1gMnSc6	Alois
Jiráskovi	Jirásek	k1gMnSc6	Jirásek
a	a	k8xC	a
ve	v	k7c6	v
svém	svůj	k3xOyFgMnSc6	svůj
učiteli	učitel	k1gMnSc6	učitel
z	z	k7c2	z
obecné	obecná	k1gFnSc2	obecná
školy	škola	k1gFnSc2	škola
Václavu	Václav	k1gMnSc3	Václav
Mejstříkovi	Mejstřík	k1gMnSc3	Mejstřík
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
oba	dva	k4xCgMnPc1	dva
během	během	k7c2	během
dopoledne	dopoledne	k1gNnSc2	dopoledne
učili	učit	k5eAaImAgMnP	učit
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
odpoledne	odpoledne	k6eAd1	odpoledne
psali	psát	k5eAaImAgMnP	psát
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
tedy	tedy	k8xC	tedy
stát	stát	k5eAaPmF	stát
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
žádná	žádný	k3yNgFnSc1	žádný
škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
obor	obor	k1gInSc4	obor
nebyla	být	k5eNaImAgFnS	být
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
obor	obor	k1gInSc4	obor
český	český	k2eAgInSc1d1	český
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
-	-	kIx~	-
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
-	-	kIx~	-
jednak	jednak	k8xC	jednak
naučil	naučit	k5eAaPmAgMnS	naučit
pravopis	pravopis	k1gInSc4	pravopis
a	a	k8xC	a
jednak	jednak	k8xC	jednak
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
již	již	k6eAd1	již
napsali	napsat	k5eAaBmAgMnP	napsat
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vysokoškolských	vysokoškolský	k2eAgFnPc2d1	vysokoškolská
studií	studie	k1gFnPc2	studie
vydával	vydávat	k5eAaPmAgInS	vydávat
se	s	k7c7	s
spolustudentem	spolustudent	k1gMnSc7	spolustudent
Miloněm	Miloň	k1gMnSc7	Miloň
Čepelkou	čepelka	k1gFnSc7	čepelka
školní	školní	k2eAgInSc1d1	školní
časopis	časopis	k1gInSc1	časopis
"	"	kIx"	"
<g/>
Hříbě	hříbě	k1gNnSc1	hříbě
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgMnSc2	který
jim	on	k3xPp3gFnPc3	on
básněmi	báseň	k1gFnPc7	báseň
nazvanými	nazvaný	k2eAgFnPc7d1	nazvaná
Vandrácké	vandrácký	k2eAgInPc4d1	vandrácký
zpěvy	zpěv	k1gInPc4	zpěv
přispíval	přispívat	k5eAaImAgMnS	přispívat
i	i	k9	i
jejich	jejich	k3xOp3gMnSc1	jejich
starší	starý	k2eAgMnSc1d2	starší
spolužák	spolužák	k1gMnSc1	spolužák
-	-	kIx~	-
student	student	k1gMnSc1	student
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
-	-	kIx~	-
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
také	také	k9	také
režíroval	režírovat	k5eAaImAgInS	režírovat
studentský	studentský	k2eAgInSc1d1	studentský
ochotnický	ochotnický	k2eAgInSc1d1	ochotnický
soubor	soubor	k1gInSc1	soubor
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
"	"	kIx"	"
<g/>
Dramatický	dramatický	k2eAgInSc1d1	dramatický
soubor	soubor	k1gInSc1	soubor
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
Svěrák	svěrák	k1gInSc4	svěrák
s	s	k7c7	s
Čepelkou	čepelka	k1gFnSc7	čepelka
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
přijímací	přijímací	k2eAgFnSc2d1	přijímací
zkoušky	zkouška	k1gFnSc2	zkouška
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
kroužku	kroužek	k1gInSc2	kroužek
recitoval	recitovat	k5eAaImAgMnS	recitovat
Svěrák	Svěrák	k1gMnSc1	Svěrák
Nerudovu	Nerudův	k2eAgFnSc4d1	Nerudova
báseň	báseň	k1gFnSc4	báseň
Balada	balada	k1gFnSc1	balada
rajská	rajský	k2eAgFnSc1d1	rajská
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
rolí	role	k1gFnSc7	role
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Svěrák	Svěrák	k1gMnSc1	Svěrák
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
druhý	druhý	k4xOgMnSc1	druhý
mladík	mladík	k1gMnSc1	mladík
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
Federica	Federicus	k1gMnSc2	Federicus
García	Garcíus	k1gMnSc2	Garcíus
Lorcy	Lorca	k1gMnSc2	Lorca
Fantastická	fantastický	k2eAgFnSc1d1	fantastická
ševcová	ševcová	k1gFnSc1	ševcová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
také	také	k9	také
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
poznal	poznat	k5eAaPmAgMnS	poznat
svoji	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
manželku	manželka	k1gFnSc4	manželka
Boženu	Božena	k1gFnSc4	Božena
Němečkovou	Němečková	k1gFnSc4	Němečková
(	(	kIx(	(
<g/>
narozenou	narozený	k2eAgFnSc4d1	narozená
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	nízce	k6eAd2	nízce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
studií	studie	k1gFnPc2	studie
odešel	odejít	k5eAaPmAgInS	odejít
učit	učit	k5eAaImF	učit
do	do	k7c2	do
západních	západní	k2eAgFnPc2d1	západní
Čech	Čechy	k1gFnPc2	Čechy
na	na	k7c4	na
Žatecko	Žatecko	k1gNnSc4	Žatecko
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
spolu	spolu	k6eAd1	spolu
měli	mít	k5eAaImAgMnP	mít
svatbu	svatba	k1gFnSc4	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
Svěrák	Svěrák	k1gMnSc1	Svěrák
rok	rok	k1gInSc4	rok
učil	učit	k5eAaImAgMnS	učit
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Měcholupech	Měcholup	k1gInPc6	Měcholup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
založil	založit	k5eAaPmAgInS	založit
ochotnický	ochotnický	k2eAgInSc1d1	ochotnický
spolek	spolek	k1gInSc1	spolek
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
jej	on	k3xPp3gInSc4	on
režíroval	režírovat	k5eAaImAgMnS	režírovat
<g/>
.	.	kIx.	.
</s>
<s>
Uvedli	uvést	k5eAaPmAgMnP	uvést
například	například	k6eAd1	například
Černou	černý	k2eAgFnSc4d1	černá
vlajku	vlajka	k1gFnSc4	vlajka
od	od	k7c2	od
Františka	František	k1gMnSc2	František
Pavlíčka	Pavlíček	k1gMnSc2	Pavlíček
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
Žatce	Žatec	k1gInSc2	Žatec
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Komenského	Komenského	k2eAgFnSc6d1	Komenského
aleji	alej	k1gFnSc6	alej
a	a	k8xC	a
následně	následně	k6eAd1	následně
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
tomtéž	týž	k3xTgNnSc6	týž
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
jím	jíst	k5eAaImIp1nS	jíst
započaté	započatý	k2eAgFnSc3d1	započatá
práci	práce	k1gFnSc3	práce
v	v	k7c6	v
Měcholupech	Měcholup	k1gInPc6	Měcholup
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
jeho	jeho	k3xOp3gMnPc3	jeho
manželka	manželka	k1gFnSc1	manželka
Božena	Božena	k1gFnSc1	Božena
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
února	únor	k1gInSc2	únor
1961	[number]	k4	1961
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
Svěrákovým	Svěrákův	k2eAgMnSc7d1	Svěrákův
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
narodilo	narodit	k5eAaPmAgNnS	narodit
první	první	k4xOgNnSc1	první
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Hanka	Hanka	k1gFnSc1	Hanka
<g/>
.	.	kIx.	.
</s>
<s>
Svěrák	Svěrák	k1gMnSc1	Svěrák
sice	sice	k8xC	sice
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
antikomunistické	antikomunistický	k2eAgFnSc2d1	antikomunistická
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
vyučujícími	vyučující	k2eAgInPc7d1	vyučující
z	z	k7c2	z
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgMnPc2	jenž
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
univerzitními	univerzitní	k2eAgMnPc7d1	univerzitní
profesory	profesor	k1gMnPc7	profesor
<g/>
,	,	kIx,	,
musejí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
přehled	přehled	k1gInSc4	přehled
a	a	k8xC	a
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
do	do	k7c2	do
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
ho	on	k3xPp3gMnSc4	on
přesvědčovali	přesvědčovat	k5eAaImAgMnP	přesvědčovat
také	také	k9	také
kolegové	kolega	k1gMnPc1	kolega
ve	v	k7c6	v
sborovně	sborovna	k1gFnSc6	sborovna
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
si	se	k3xPyFc3	se
vážil	vážit	k5eAaImAgInS	vážit
<g/>
,	,	kIx,	,
a	a	k8xC	a
kteří	který	k3yRgMnPc1	který
již	již	k6eAd1	již
členy	člen	k1gMnPc7	člen
KSČ	KSČ	kA	KSČ
byli	být	k5eAaImAgMnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Svěrák	Svěrák	k1gMnSc1	Svěrák
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	čí	k3xOyQgNnSc7	čí
více	hodně	k6eAd2	hodně
bude	být	k5eAaImBp3nS	být
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
dobrých	dobrý	k2eAgMnPc2d1	dobrý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
snáze	snadno	k6eAd2	snadno
ji	on	k3xPp3gFnSc4	on
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
posunout	posunout	k5eAaPmF	posunout
k	k	k7c3	k
lepšímu	dobrý	k2eAgMnSc3d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
psal	psát	k5eAaImAgInS	psát
též	též	k9	též
texty	text	k1gInPc4	text
k	k	k7c3	k
písním	píseň	k1gFnPc3	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
textoval	textovat	k5eAaBmAgMnS	textovat
nápěvy	nápěv	k1gInPc4	nápěv
svého	svůj	k3xOyFgMnSc4	svůj
vzdáleného	vzdálený	k2eAgMnSc4d1	vzdálený
strýce	strýc	k1gMnSc4	strýc
Antonína	Antonín	k1gMnSc4	Antonín
Ulricha	Ulrich	k1gMnSc4	Ulrich
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
hrál	hrát	k5eAaImAgMnS	hrát
jednak	jednak	k8xC	jednak
v	v	k7c6	v
Symfonickém	symfonický	k2eAgInSc6d1	symfonický
orchestru	orchestr	k1gInSc6	orchestr
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
FOK	FOK	kA	FOK
a	a	k8xC	a
jednak	jednak	k8xC	jednak
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vlastním	vlastní	k2eAgInSc6d1	vlastní
Smyčcovém	smyčcový	k2eAgInSc6d1	smyčcový
orchestru	orchestr	k1gInSc6	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
napsal	napsat	k5eAaPmAgInS	napsat
text	text	k1gInSc1	text
například	například	k6eAd1	například
k	k	k7c3	k
písním	píseň	k1gFnPc3	píseň
"	"	kIx"	"
<g/>
Mám	mít	k5eAaImIp1nS	mít
ráda	rád	k2eAgFnSc1d1	ráda
hajnýho	hajnýze	k6eAd1	hajnýze
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Kapesníčky	kapesníček	k1gInPc1	kapesníček
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
však	však	k9	však
Svěrákovi	Svěrákův	k2eAgMnPc1d1	Svěrákův
tyto	tento	k3xDgInPc4	tento
texty	text	k1gInPc7	text
zdály	zdát	k5eAaImAgFnP	zdát
nehodné	hodný	k2eNgInPc4d1	nehodný
jeho	jeho	k3xOp3gNnSc1	jeho
mládí	mládí	k1gNnSc1	mládí
<g/>
,	,	kIx,	,
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
je	být	k5eAaImIp3nS	být
raději	rád	k6eAd2	rád
pseudonymem	pseudonym	k1gInSc7	pseudonym
Emil	Emil	k1gMnSc1	Emil
Synek	Synek	k1gMnSc1	Synek
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
složením	složení	k1gNnSc7	složení
křestního	křestní	k2eAgNnSc2d1	křestní
jména	jméno	k1gNnSc2	jméno
Svěrákova	Svěrákův	k2eAgMnSc4d1	Svěrákův
tchána	tchán	k1gMnSc4	tchán
(	(	kIx(	(
<g/>
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
se	se	k3xPyFc4	se
Emil	Emil	k1gMnSc1	Emil
Němeček	Němeček	k1gMnSc1	Němeček
<g/>
)	)	kIx)	)
a	a	k8xC	a
dívčího	dívčí	k2eAgNnSc2d1	dívčí
příjmení	příjmení	k1gNnSc2	příjmení
Svěrákovy	Svěrákův	k2eAgFnSc2d1	Svěrákova
matky	matka	k1gFnSc2	matka
(	(	kIx(	(
<g/>
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
se	se	k3xPyFc4	se
Růžena	Růžena	k1gFnSc1	Růžena
Synková	Synková	k1gFnSc1	Synková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevěděl	vědět	k5eNaImAgMnS	vědět
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
osobnost	osobnost	k1gFnSc1	osobnost
s	s	k7c7	s
takovým	takový	k3xDgNnSc7	takový
jménem	jméno	k1gNnSc7	jméno
již	již	k6eAd1	již
existovala	existovat	k5eAaImAgFnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jím	jíst	k5eAaImIp1nS	jíst
český	český	k2eAgMnSc1d1	český
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovšem	ovšem	k9	ovšem
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
pseudonym	pseudonym	k1gInSc4	pseudonym
již	již	k6eAd1	již
Svěrák	Svěrák	k1gMnSc1	Svěrák
nechal	nechat	k5eAaPmAgMnS	nechat
<g/>
.	.	kIx.	.
</s>
<s>
Kantorská	kantorský	k2eAgFnSc1d1	kantorská
práce	práce	k1gFnSc1	práce
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
příliš	příliš	k6eAd1	příliš
neuspokojovala	uspokojovat	k5eNaImAgFnS	uspokojovat
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
totiž	totiž	k9	totiž
učit	učit	k5eAaImF	učit
a	a	k8xC	a
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
pracovním	pracovní	k2eAgInSc6d1	pracovní
stole	stol	k1gInSc6	stol
kupily	kupit	k5eAaImAgInP	kupit
diktáty	diktát	k1gInPc1	diktát
a	a	k8xC	a
slohové	slohový	k2eAgFnPc1d1	slohová
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
musel	muset	k5eAaImAgMnS	muset
opravovat	opravovat	k5eAaImF	opravovat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ač	ač	k8xS	ač
napsal	napsat	k5eAaPmAgMnS	napsat
několik	několik	k4yIc4	několik
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
nezbývalo	zbývat	k5eNaImAgNnS	zbývat
mu	on	k3xPp3gNnSc3	on
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
tolik	tolik	k6eAd1	tolik
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
představoval	představovat	k5eAaImAgMnS	představovat
<g/>
.	.	kIx.	.
</s>
<s>
Zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
proto	proto	k8xC	proto
na	na	k7c4	na
nabídku	nabídka	k1gFnSc4	nabídka
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
konkurzu	konkurz	k1gInSc2	konkurz
do	do	k7c2	do
armádní	armádní	k2eAgFnSc2d1	armádní
redakce	redakce	k1gFnSc2	redakce
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Uspěl	uspět	k5eAaPmAgMnS	uspět
a	a	k8xC	a
do	do	k7c2	do
rozhlasu	rozhlas	k1gInSc2	rozhlas
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
se	s	k7c7	s
členem	člen	k1gInSc7	člen
téže	týž	k3xTgFnSc2	týž
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
redakce	redakce	k1gFnSc2	redakce
stal	stát	k5eAaPmAgMnS	stát
Svěrákův	Svěrákův	k2eAgMnSc1d1	Svěrákův
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
spolužák	spolužák	k1gMnSc1	spolužák
Miloň	Miloň	k1gFnSc2	Miloň
Čepelka	čepelka	k1gFnSc1	čepelka
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
pořadem	pořad	k1gInSc7	pořad
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
Svěrák	svěrák	k1gInSc1	svěrák
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
připravoval	připravovat	k5eAaImAgMnS	připravovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ranní	ranní	k2eAgFnSc1d1	ranní
pětiminutovka	pětiminutovka	k1gFnSc1	pětiminutovka
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Reportoval	reportovat	k5eAaImAgMnS	reportovat
ale	ale	k9	ale
také	také	k9	také
o	o	k7c6	o
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
Československé	československý	k2eAgFnSc2d1	Československá
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
ČSLA	ČSLA	kA	ČSLA
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
o	o	k7c4	o
vystoupení	vystoupení	k1gNnSc4	vystoupení
armádních	armádní	k2eAgMnPc2d1	armádní
sportovců	sportovec	k1gMnPc2	sportovec
na	na	k7c6	na
spartakiádě	spartakiáda	k1gFnSc6	spartakiáda
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jenž	k3xRgNnSc2	jenž
vhodného	vhodný	k2eAgNnSc2d1	vhodné
přiblížení	přiblížení	k1gNnSc2	přiblížení
posluchačům	posluchač	k1gMnPc3	posluchač
ho	on	k3xPp3gNnSc4	on
zasvěcoval	zasvěcovat	k5eAaImAgMnS	zasvěcovat
sportovní	sportovní	k2eAgMnSc1d1	sportovní
komentátor	komentátor	k1gMnSc1	komentátor
Stanislav	Stanislav	k1gMnSc1	Stanislav
Sigmund	Sigmund	k1gMnSc1	Sigmund
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c6	o
jugoslávských	jugoslávský	k2eAgMnPc6d1	jugoslávský
partyzánech	partyzán	k1gMnPc6	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
také	také	k9	také
pocházejí	pocházet	k5eAaImIp3nP	pocházet
tři	tři	k4xCgFnPc4	tři
Svěrákovy	Svěrákův	k2eAgFnPc4d1	Svěrákova
nedělní	nedělní	k2eAgFnPc4d1	nedělní
rozhlasové	rozhlasový	k2eAgFnPc4d1	rozhlasová
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Tři	tři	k4xCgNnPc4	tři
auta	auto	k1gNnPc4	auto
<g/>
,	,	kIx,	,
a	a	k8xC	a
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
natočil	natočit	k5eAaBmAgMnS	natočit
například	například	k6eAd1	například
s	s	k7c7	s
Eduardem	Eduard	k1gMnSc7	Eduard
Kohoutem	Kohout	k1gMnSc7	Kohout
<g/>
,	,	kIx,	,
Ljubou	Ljuba	k1gFnSc7	Ljuba
Hermanovou	Hermanová	k1gFnSc7	Hermanová
<g/>
,	,	kIx,	,
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Trojanem	Trojan	k1gMnSc7	Trojan
či	či	k8xC	či
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Deylem	Deyl	k1gMnSc7	Deyl
mladším	mladý	k2eAgMnSc7d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
pohádka	pohádka	k1gFnSc1	pohádka
Tiché	Tichá	k1gFnSc2	Tichá
šlapací	šlapací	k2eAgNnPc4d1	šlapací
království	království	k1gNnPc4	království
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgFnSc6d1	nazvaná
Krápník	krápník	k1gInSc4	krápník
a	a	k8xC	a
Františka	František	k1gMnSc4	František
<g/>
,	,	kIx,	,
nahrané	nahraný	k2eAgInPc1d1	nahraný
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
jako	jako	k8xC	jako
první	první	k4xOgFnSc7	první
stereofonně	stereofonně	k6eAd1	stereofonně
realizované	realizovaný	k2eAgFnSc6d1	realizovaná
hře	hra	k1gFnSc6	hra
na	na	k7c6	na
území	území	k1gNnSc6	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
třeba	třeba	k6eAd1	třeba
Josef	Josef	k1gMnSc1	Josef
Kemr	Kemr	k1gMnSc1	Kemr
<g/>
,	,	kIx,	,
Ilja	Ilja	k1gMnSc1	Ilja
Racek	racek	k1gMnSc1	racek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Satoranský	Satoranský	k2eAgMnSc1d1	Satoranský
či	či	k8xC	či
Jana	Jana	k1gFnSc1	Jana
Drbohlavová	Drbohlavový	k2eAgFnSc1d1	Drbohlavová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
Svěrák	Svěrák	k1gMnSc1	Svěrák
již	již	k6eAd1	již
podruhé	podruhé	k6eAd1	podruhé
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc3	jeho
manželce	manželka	k1gFnSc3	manželka
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
v	v	k7c6	v
Žatci	Žatec	k1gInSc6	Žatec
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
začali	začít	k5eAaPmAgMnP	začít
postupně	postupně	k6eAd1	postupně
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kolegou	kolega	k1gMnSc7	kolega
Jiřím	Jiří	k1gMnSc7	Jiří
Šebánkem	Šebánek	k1gMnSc7	Šebánek
a	a	k8xC	a
jazzmanem	jazzman	k1gMnSc7	jazzman
Karlem	Karel	k1gMnSc7	Karel
Velebným	velebný	k2eAgNnSc7d1	velebné
připravovat	připravovat	k5eAaImF	připravovat
přímé	přímý	k2eAgInPc4d1	přímý
přenosy	přenos	k1gInPc4	přenos
z	z	k7c2	z
imaginární	imaginární	k2eAgFnSc2d1	imaginární
nealkoholické	alkoholický	k2eNgFnSc2d1	nealkoholická
vinárny	vinárna	k1gFnSc2	vinárna
U	u	k7c2	u
Pavouka	pavouk	k1gMnSc2	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vysílání	vysílání	k1gNnSc1	vysílání
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1965	[number]	k4	1965
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnPc2	součást
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
cyklu	cyklus	k1gInSc2	cyklus
Večer	večer	k6eAd1	večer
po	po	k7c6	po
neděli	neděle	k1gFnSc6	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
pořad	pořad	k1gInSc1	pořad
osamostatnil	osamostatnit	k5eAaPmAgInS	osamostatnit
a	a	k8xC	a
vysílal	vysílat	k5eAaImAgInS	vysílat
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
posluchači	posluchač	k1gMnPc1	posluchač
smyšlenost	smyšlenost	k1gFnSc4	smyšlenost
celého	celý	k2eAgInSc2d1	celý
pořadu	pořad	k1gInSc2	pořad
ani	ani	k8xC	ani
nepoznali	poznat	k5eNaPmAgMnP	poznat
a	a	k8xC	a
sháněli	shánět	k5eAaImAgMnP	shánět
se	se	k3xPyFc4	se
-	-	kIx~	-
jako	jako	k9	jako
například	například	k6eAd1	například
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
-	-	kIx~	-
po	po	k7c6	po
adrese	adresa	k1gFnSc6	adresa
vinárny	vinárna	k1gFnSc2	vinárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
se	se	k3xPyFc4	se
také	také	k9	také
prvně	prvně	k?	prvně
objevila	objevit	k5eAaPmAgFnS	objevit
postava	postava	k1gFnSc1	postava
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1966	[number]	k4	1966
a	a	k8xC	a
představen	představit	k5eAaPmNgMnS	představit
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
řidič	řidič	k1gMnSc1	řidič
parního	parní	k2eAgInSc2d1	parní
válce	válec	k1gInSc2	válec
u	u	k7c2	u
národního	národní	k2eAgInSc2d1	národní
podniku	podnik	k1gInSc2	podnik
Stavby	stavba	k1gFnSc2	stavba
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
železnic	železnice	k1gFnPc2	železnice
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
coby	coby	k?	coby
amatérský	amatérský	k2eAgMnSc1d1	amatérský
sochař	sochař	k1gMnSc1	sochař
v	v	k7c6	v
nafukovací	nafukovací	k2eAgFnSc6d1	nafukovací
hale	hala	k1gFnSc6	hala
vinárny	vinárna	k1gFnSc2	vinárna
U	u	k7c2	u
pavouka	pavouk	k1gMnSc2	pavouk
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
naivní	naivní	k2eAgInPc4d1	naivní
exponáty	exponát	k1gInPc4	exponát
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
předměty	předmět	k1gInPc1	předmět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
přejel	přejet	k5eAaPmAgInS	přejet
svým	svůj	k3xOyFgInSc7	svůj
parním	parní	k2eAgInSc7d1	parní
válcem	válec	k1gInSc7	válec
<g/>
.	.	kIx.	.
</s>
<s>
Výstava	výstava	k1gFnSc1	výstava
však	však	k9	však
tehdy	tehdy	k6eAd1	tehdy
skončila	skončit	k5eAaPmAgFnS	skončit
fiaskem	fiasko	k1gNnSc7	fiasko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
z	z	k7c2	z
haly	hala	k1gFnSc2	hala
ucházel	ucházet	k5eAaImAgInS	ucházet
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
půl	půl	k1xP	půl
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Hviezdoslavově	Hviezdoslavův	k2eAgFnSc6d1	Hviezdoslavova
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Dykově	Dykův	k2eAgFnSc6d1	Dykova
<g/>
)	)	kIx)	)
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
pražských	pražský	k2eAgInPc6d1	pražský
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
v	v	k7c6	v
domě	dům	k1gInSc6	dům
číslo	číslo	k1gNnSc1	číslo
14	[number]	k4	14
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
patřila	patřit	k5eAaImAgFnS	patřit
rozhlasu	rozhlas	k1gInSc3	rozhlas
<g/>
,	,	kIx,	,
konala	konat	k5eAaImAgFnS	konat
první	první	k4xOgFnSc1	první
schůzka	schůzka	k1gFnSc1	schůzka
"	"	kIx"	"
<g/>
Společnosti	společnost	k1gFnPc1	společnost
pro	pro	k7c4	pro
rehabilitaci	rehabilitace	k1gFnSc4	rehabilitace
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
díla	dílo	k1gNnSc2	dílo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Účastnili	účastnit	k5eAaImAgMnP	účastnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
vedle	vedle	k7c2	vedle
Svěráka	Svěrák	k1gMnSc2	Svěrák
také	také	k9	také
Jiří	Jiří	k1gMnSc1	Jiří
Šebánek	Šebánek	k1gMnSc1	Šebánek
<g/>
,	,	kIx,	,
Miloň	Miloň	k1gFnSc1	Miloň
Čepelka	čepelka	k1gFnSc1	čepelka
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
Štědrého	štědrý	k2eAgInSc2d1	štědrý
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
zazněla	zaznít	k5eAaPmAgFnS	zaznít
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
z	z	k7c2	z
vinárny	vinárna	k1gFnSc2	vinárna
U	u	k7c2	u
pavouka	pavouk	k1gMnSc2	pavouk
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Evžen	Evžen	k1gMnSc1	Evžen
Hedvábný	hedvábný	k2eAgMnSc1d1	hedvábný
nalezl	nalézt	k5eAaBmAgInS	nalézt
při	při	k7c6	při
budování	budování	k1gNnSc6	budování
krbu	krb	k1gInSc2	krb
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
chatě	chata	k1gFnSc6	chata
v	v	k7c6	v
pojizerské	pojizerský	k2eAgFnSc6d1	Pojizerská
obci	obec	k1gFnSc6	obec
Liptákov	Liptákov	k1gInSc1	Liptákov
kovanou	kovaný	k2eAgFnSc4d1	kovaná
truhlu	truhla	k1gFnSc4	truhla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
otevření	otevření	k1gNnSc4	otevření
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
provedl	provést	k5eAaPmAgMnS	provést
pyrotechnik	pyrotechnik	k1gMnSc1	pyrotechnik
Šťáhlavský	Šťáhlavský	k2eAgMnSc1d1	Šťáhlavský
<g/>
,	,	kIx,	,
zničena	zničit	k5eAaPmNgFnS	zničit
výbuchem	výbuch	k1gInSc7	výbuch
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
obsah	obsah	k1gInSc1	obsah
rozmetán	rozmetán	k2eAgInSc1d1	rozmetán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
tak	tak	k6eAd1	tak
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
objevování	objevování	k1gNnSc3	objevování
a	a	k8xC	a
obnovování	obnovování	k1gNnSc3	obnovování
detonací	detonace	k1gFnSc7	detonace
zničeného	zničený	k2eAgInSc2d1	zničený
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
neznámý	známý	k2eNgMnSc1d1	neznámý
český	český	k2eAgMnSc1d1	český
velikán	velikán	k1gMnSc1	velikán
a	a	k8xC	a
všeuměl	všeuměl	k1gMnSc1	všeuměl
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozjezd	rozjezd	k1gInSc4	rozjezd
divadla	divadlo	k1gNnSc2	divadlo
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
jeho	jeho	k3xOp3gMnPc1	jeho
zakládající	zakládající	k2eAgMnPc1d1	zakládající
členové	člen	k1gMnPc1	člen
sepsat	sepsat	k5eAaPmF	sepsat
každý	každý	k3xTgMnSc1	každý
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
jednoaktovou	jednoaktový	k2eAgFnSc4d1	jednoaktová
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Svěrák	Svěrák	k1gMnSc1	Svěrák
sepsal	sepsat	k5eAaPmAgMnS	sepsat
drama	drama	k1gNnSc4	drama
Akt	akt	k1gInSc1	akt
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgNnSc2	jeden
představení	představení	k1gNnSc2	představení
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Šebánkovou	Šebánkův	k2eAgFnSc7d1	Šebánkův
Domácí	domácí	k2eAgFnSc7d1	domácí
zabijačkou	zabijačka	k1gFnSc7	zabijačka
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
Akt	akt	k1gInSc1	akt
již	již	k6eAd1	již
měl	mít	k5eAaImAgInS	mít
Svěrák	svěrák	k1gInSc1	svěrák
hotový	hotový	k2eAgInSc1d1	hotový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Šebánkovo	Šebánkův	k2eAgNnSc1d1	Šebánkův
dílo	dílo	k1gNnSc1	dílo
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
nebylo	být	k5eNaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
a	a	k8xC	a
termín	termín	k1gInSc1	termín
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
na	na	k7c6	na
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
domluveno	domluven	k2eAgNnSc1d1	domluveno
předváděcí	předváděcí	k2eAgNnSc1d1	předváděcí
představení	představení	k1gNnSc1	představení
pro	pro	k7c4	pro
úředníky	úředník	k1gMnPc4	úředník
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
měli	mít	k5eAaImAgMnP	mít
divadlo	divadlo	k1gNnSc4	divadlo
administrativně	administrativně	k6eAd1	administrativně
zaštítit	zaštítit	k5eAaPmF	zaštítit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
blížilo	blížit	k5eAaImAgNnS	blížit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
divadelníci	divadelník	k1gMnPc1	divadelník
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
nečekat	čekat	k5eNaImF	čekat
na	na	k7c6	na
dokončení	dokončení	k1gNnSc6	dokončení
Domácí	domácí	k2eAgFnSc2d1	domácí
zabijačky	zabijačka	k1gFnSc2	zabijačka
a	a	k8xC	a
představení	představení	k1gNnSc2	představení
doplnili	doplnit	k5eAaPmAgMnP	doplnit
seminářem	seminář	k1gInSc7	seminář
s	s	k7c7	s
přednáškami	přednáška	k1gFnPc7	přednáška
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
díle	díl	k1gInSc6	díl
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
první	první	k4xOgFnSc2	první
hry	hra	k1gFnSc2	hra
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1967	[number]	k4	1967
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Malostranské	malostranský	k2eAgFnSc6d1	Malostranská
besedě	beseda	k1gFnSc6	beseda
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gFnSc1	drama
kritika	kritika	k1gFnSc1	kritika
přijala	přijmout	k5eAaPmAgFnS	přijmout
vlídně	vlídně	k6eAd1	vlídně
a	a	k8xC	a
pochvalně	pochvalně	k6eAd1	pochvalně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
prostorách	prostora	k1gFnPc6	prostora
měla	mít	k5eAaImAgFnS	mít
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1967	[number]	k4	1967
premiéru	premiéra	k1gFnSc4	premiéra
hra	hra	k1gFnSc1	hra
Vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
ztráty	ztráta	k1gFnSc2	ztráta
třídní	třídní	k2eAgFnSc2d1	třídní
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1968	[number]	k4	1968
také	také	k6eAd1	také
Domácí	domácí	k2eAgFnSc1d1	domácí
zabijačka	zabijačka	k1gFnSc1	zabijačka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Armádní	armádní	k2eAgFnSc6d1	armádní
redakci	redakce	k1gFnSc6	redakce
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
potkal	potkat	k5eAaPmAgInS	potkat
Svěrák	svěrák	k1gInSc1	svěrák
také	také	k9	také
hudebníka	hudebník	k1gMnSc4	hudebník
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Uhlíře	Uhlíř	k1gMnSc4	Uhlíř
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tam	tam	k6eAd1	tam
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
coby	coby	k?	coby
vítěz	vítěz	k1gMnSc1	vítěz
Armádní	armádní	k2eAgFnSc2d1	armádní
soutěže	soutěž	k1gFnSc2	soutěž
umělecké	umělecký	k2eAgFnSc2d1	umělecká
tvořivosti	tvořivost	k1gFnSc2	tvořivost
(	(	kIx(	(
<g/>
ASUT	ASUT	kA	ASUT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
nejprve	nejprve	k6eAd1	nejprve
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Miloněm	Miloň	k1gMnSc7	Miloň
Čepelkou	čepelka	k1gFnSc7	čepelka
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
psal	psát	k5eAaImAgInS	psát
texty	text	k1gInPc4	text
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
tvořil	tvořit	k5eAaImAgInS	tvořit
nápěv	nápěv	k1gInSc1	nápěv
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
tvorbu	tvorba	k1gFnSc4	tvorba
Svěrák	svěrák	k1gInSc1	svěrák
znal	znát	k5eAaImAgInS	znát
z	z	k7c2	z
rozhlasového	rozhlasový	k2eAgInSc2d1	rozhlasový
pořadu	pořad	k1gInSc2	pořad
"	"	kIx"	"
<g/>
Polní	polní	k2eAgFnSc1d1	polní
pošta	pošta	k1gFnSc1	pošta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
ale	ale	k8xC	ale
oslovil	oslovit	k5eAaPmAgMnS	oslovit
Uhlíře	Uhlíř	k1gMnSc4	Uhlíř
i	i	k9	i
Svěrák	Svěrák	k1gMnSc1	Svěrák
s	s	k7c7	s
dotazem	dotaz	k1gInSc7	dotaz
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
nechtěl	chtít	k5eNaImAgMnS	chtít
napsat	napsat	k5eAaPmF	napsat
písně	píseň	k1gFnPc4	píseň
do	do	k7c2	do
dětského	dětský	k2eAgInSc2d1	dětský
pořadu	pořad	k1gInSc2	pořad
"	"	kIx"	"
<g/>
Pionýrská	pionýrský	k2eAgFnSc1d1	Pionýrská
jitřenka	jitřenka	k1gFnSc1	jitřenka
<g/>
"	"	kIx"	"
a	a	k8xC	a
zhudebnit	zhudebnit	k5eAaPmF	zhudebnit
také	také	k9	také
jeho	jeho	k3xOp3gInSc4	jeho
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
nic	nic	k3yNnSc4	nic
nenamítal	namítat	k5eNaImAgMnS	namítat
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
Svěrákově	Svěrákův	k2eAgInSc6d1	Svěrákův
textu	text	k1gInSc6	text
"	"	kIx"	"
<g/>
Strašidýlko	strašidýlko	k1gNnSc1	strašidýlko
Emílek	Emílka	k1gFnPc2	Emílka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
se	se	k3xPyFc4	se
Svěrákovi	Svěrák	k1gMnSc3	Svěrák
zamlouval	zamlouvat	k5eAaImAgInS	zamlouvat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jejich	jejich	k3xOp3gFnSc1	jejich
spolupráce	spolupráce	k1gFnSc1	spolupráce
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
Svěrák	Svěrák	k1gMnSc1	Svěrák
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
role	role	k1gFnSc1	role
prokurátora	prokurátor	k1gMnSc2	prokurátor
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Zločin	zločin	k1gInSc1	zločin
v	v	k7c6	v
šantánu	šantán	k1gInSc6	šantán
režiséra	režisér	k1gMnSc2	režisér
Jiřího	Jiří	k1gMnSc2	Jiří
Menzela	Menzela	k1gMnSc2	Menzela
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
divadle	divadlo	k1gNnSc6	divadlo
ovšem	ovšem	k9	ovšem
začalo	začít	k5eAaPmAgNnS	začít
mezi	mezi	k7c7	mezi
Šebánkem	Šebánek	k1gMnSc7	Šebánek
a	a	k8xC	a
Smoljakem	Smoljak	k1gMnSc7	Smoljak
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
uměleckým	umělecký	k2eAgInPc3d1	umělecký
i	i	k8xC	i
osobním	osobní	k2eAgInPc3d1	osobní
sporům	spor	k1gInPc3	spor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyústily	vyústit	k5eAaPmAgInP	vyústit
v	v	k7c4	v
Šebánkův	Šebánkův	k2eAgInSc4d1	Šebánkův
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
divadla	divadlo	k1gNnSc2	divadlo
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
svoji	svůj	k3xOyFgFnSc4	svůj
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dočkala	dočkat	k5eAaPmAgFnS	dočkat
pouze	pouze	k6eAd1	pouze
34	[number]	k4	34
repríz	repríza	k1gFnPc2	repríza
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
noci	noc	k1gFnSc2	noc
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
napadly	napadnout	k5eAaPmAgInP	napadnout
Československo	Československo	k1gNnSc1	Československo
armády	armáda	k1gFnPc1	armáda
pěti	pět	k4xCc2	pět
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
a	a	k8xC	a
Východního	východní	k2eAgNnSc2d1	východní
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ukončily	ukončit	k5eAaPmAgFnP	ukončit
společenské	společenský	k2eAgNnSc4d1	společenské
uvolnění	uvolnění	k1gNnSc4	uvolnění
konce	konec	k1gInSc2	konec
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
Pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
příchodu	příchod	k1gInSc2	příchod
armád	armáda	k1gFnPc2	armáda
Svěrák	Svěrák	k1gMnSc1	Svěrák
vysílal	vysílat	k5eAaImAgMnS	vysílat
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
a	a	k8xC	a
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
vytížení	vytížení	k1gNnSc4	vytížení
neměl	mít	k5eNaImAgMnS	mít
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
holit	holit	k5eAaImF	holit
<g/>
.	.	kIx.	.
</s>
<s>
Vousy	vous	k1gInPc1	vous
mu	on	k3xPp3gMnSc3	on
postupně	postupně	k6eAd1	postupně
narostly	narůst	k5eAaPmAgInP	narůst
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
asi	asi	k9	asi
deseti	deset	k4xCc6	deset
dnech	den	k1gInPc6	den
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
podíval	podívat	k5eAaImAgMnS	podívat
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
plnovous	plnovous	k1gInSc1	plnovous
sluší	slušet	k5eAaImIp3nS	slušet
<g/>
,	,	kIx,	,
a	a	k8xC	a
zařekl	zařeknout	k5eAaPmAgMnS	zařeknout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neoholí	oholit	k5eNaPmIp3nS	oholit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
okupační	okupační	k2eAgNnPc1d1	okupační
vojska	vojsko	k1gNnPc1	vojsko
odejdou	odejít	k5eAaPmIp3nP	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
ve	v	k7c6	v
filmech	film	k1gInPc6	film
hrával	hrávat	k5eAaImAgInS	hrávat
oholen	oholit	k5eAaPmNgInS	oholit
<g/>
,	,	kIx,	,
fanoušci	fanoušek	k1gMnPc1	fanoušek
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
obvykle	obvykle	k6eAd1	obvykle
nepoznávali	poznávat	k5eNaImAgMnP	poznávat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozladění	rozladění	k1gNnSc1	rozladění
ze	z	k7c2	z
srpnové	srpnový	k2eAgFnSc2d1	srpnová
invaze	invaze	k1gFnSc2	invaze
cizích	cizí	k2eAgFnPc2d1	cizí
armád	armáda	k1gFnPc2	armáda
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Svěrák	Svěrák	k1gMnSc1	Svěrák
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
z	z	k7c2	z
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
opustil	opustit	k5eAaPmAgInS	opustit
také	také	k9	také
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Československém	československý	k2eAgInSc6d1	československý
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
z	z	k7c2	z
rozhlasu	rozhlas	k1gInSc2	rozhlas
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
s	s	k7c7	s
Miloněm	Miloň	k1gMnSc7	Miloň
Čepelkou	čepelka	k1gFnSc7	čepelka
do	do	k7c2	do
pořadu	pořad	k1gInSc2	pořad
Meteor	meteor	k1gInSc1	meteor
několik	několik	k4yIc1	několik
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Šest	šest	k4xCc1	šest
na	na	k7c6	na
bidýlku	bidýlko	k1gNnSc6	bidýlko
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
parodoval	parodovat	k5eAaImAgInS	parodovat
hudební	hudební	k2eAgFnSc4d1	hudební
hitparády	hitparáda	k1gFnPc4	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
objevili	objevit	k5eAaPmAgMnP	objevit
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Svěrák	Svěrák	k1gMnSc1	Svěrák
věnoval	věnovat	k5eAaImAgMnS	věnovat
svobodnému	svobodný	k2eAgNnSc3d1	svobodné
povolání	povolání	k1gNnSc3	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
první	první	k4xOgFnSc1	první
hra	hra	k1gFnSc1	hra
s	s	k7c7	s
Cimrmanovskou	cimrmanovský	k2eAgFnSc7d1	cimrmanovská
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Svěrák	Svěrák	k1gMnSc1	Svěrák
sepsal	sepsat	k5eAaPmAgMnS	sepsat
společně	společně	k6eAd1	společně
se	s	k7c7	s
Smoljakem	Smoljak	k1gMnSc7	Smoljak
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
se	se	k3xPyFc4	se
Hospoda	Hospoda	k?	Hospoda
Na	na	k7c6	na
mýtince	mýtinka	k1gFnSc6	mýtinka
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
nejhranějším	hraný	k2eAgInSc7d3	nejhranější
titulem	titul	k1gInSc7	titul
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Svěrák	Svěrák	k1gMnSc1	Svěrák
také	také	k9	také
opětovně	opětovně	k6eAd1	opětovně
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
v	v	k7c6	v
epizodní	epizodní	k2eAgFnSc6d1	epizodní
roli	role	k1gFnSc6	role
funkcionáře	funkcionář	k1gMnSc2	funkcionář
Ústřední	ústřední	k2eAgFnSc2d1	ústřední
rady	rada	k1gFnSc2	rada
odborů	odbor	k1gInPc2	odbor
v	v	k7c6	v
Menzelově	Menzelův	k2eAgInSc6d1	Menzelův
snímku	snímek	k1gInSc6	snímek
Skřivánci	Skřivánek	k1gMnPc1	Skřivánek
na	na	k7c6	na
niti	nit	k1gFnSc6	nit
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgFnSc7d1	literární
předlohou	předloha	k1gFnSc7	předloha
pro	pro	k7c4	pro
film	film	k1gInSc4	film
byla	být	k5eAaImAgFnS	být
povídková	povídkový	k2eAgFnSc1d1	povídková
kniha	kniha	k1gFnSc1	kniha
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Inzerát	inzerát	k1gInSc4	inzerát
na	na	k7c4	na
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
už	už	k6eAd1	už
nechci	chtít	k5eNaImIp1nS	chtít
bydlet	bydlet	k5eAaImF	bydlet
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
premiéru	premiéra	k1gFnSc4	premiéra
hra	hra	k1gFnSc1	hra
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
salonním	salonní	k2eAgMnSc6d1	salonní
coupé	coupý	k2eAgFnPc4d1	coupý
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
se	s	k7c7	s
Svěrákem	svěrák	k1gInSc7	svěrák
sepsal	sepsat	k5eAaPmAgInS	sepsat
i	i	k9	i
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
.	.	kIx.	.
</s>
<s>
Malostranskou	malostranský	k2eAgFnSc4d1	Malostranská
besedu	beseda	k1gFnSc4	beseda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tehdy	tehdy	k6eAd1	tehdy
divadlo	divadlo	k1gNnSc1	divadlo
hrálo	hrát	k5eAaImAgNnS	hrát
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
dramaturg	dramaturg	k1gMnSc1	dramaturg
barrandovských	barrandovský	k2eAgInPc2d1	barrandovský
filmových	filmový	k2eAgInPc2d1	filmový
ateliérů	ateliér	k1gInPc2	ateliér
Sergej	Sergej	k1gMnSc1	Sergej
Machonin	Machonin	k2eAgMnSc1d1	Machonin
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Svěrákovi	Svěrák	k1gMnSc3	Svěrák
se	s	k7c7	s
Smoljakem	Smoljak	k1gMnSc7	Smoljak
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nP	by
nechtěli	chtít	k5eNaImAgMnP	chtít
něco	něco	k6eAd1	něco
s	s	k7c7	s
cimrmanovskou	cimrmanovský	k2eAgFnSc7d1	cimrmanovská
tematikou	tematika	k1gFnSc7	tematika
napsat	napsat	k5eAaBmF	napsat
pro	pro	k7c4	pro
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
tak	tak	k6eAd1	tak
sepsali	sepsat	k5eAaPmAgMnP	sepsat
Sedm	sedm	k4xCc4	sedm
zásad	zásada	k1gFnPc2	zásada
inspektora	inspektor	k1gMnSc2	inspektor
Trachty	Trachty	k?	Trachty
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
dílo	dílo	k1gNnSc1	dílo
barrandovské	barrandovský	k2eAgMnPc4d1	barrandovský
pracovníky	pracovník	k1gMnPc4	pracovník
příliš	příliš	k6eAd1	příliš
nezaujalo	zaujmout	k5eNaPmAgNnS	zaujmout
<g/>
,	,	kIx,	,
a	a	k8xC	a
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nenatočil	natočit	k5eNaBmAgInS	natočit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gFnSc1	jeho
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
scénáře	scénář	k1gInSc2	scénář
filmu	film	k1gInSc2	film
Rozpuštěný	rozpuštěný	k2eAgMnSc1d1	rozpuštěný
a	a	k8xC	a
vypuštěný	vypuštěný	k2eAgMnSc1d1	vypuštěný
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
Smoljak	Smoljak	k1gMnSc1	Smoljak
se	se	k3xPyFc4	se
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
objevili	objevit	k5eAaPmAgMnP	objevit
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Pane	Pan	k1gMnSc5	Pan
<g/>
,	,	kIx,	,
vy	vy	k3xPp2nPc1	vy
jste	být	k5eAaImIp2nP	být
vdova	vdova	k1gFnSc1	vdova
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Václav	Václav	k1gMnSc1	Václav
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráli	hrát	k5eAaImAgMnP	hrát
role	role	k1gFnSc2	role
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
strážníci	strážník	k1gMnPc1	strážník
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Lucie	Lucie	k1gFnSc1	Lucie
a	a	k8xC	a
zázraky	zázrak	k1gInPc1	zázrak
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Ota	Ota	k1gMnSc1	Ota
Koval	kovat	k5eAaImAgMnS	kovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
premiéru	premiér	k1gMnSc3	premiér
další	další	k2eAgNnSc1d1	další
drama	drama	k1gNnSc1	drama
dvojice	dvojice	k1gFnSc1	dvojice
Svěrák	Svěrák	k1gMnSc1	Svěrák
-	-	kIx~	-
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
Němý	němý	k2eAgMnSc1d1	němý
Bobeš	Bobeš	k1gMnSc1	Bobeš
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
poslední	poslední	k2eAgFnSc7d1	poslední
hrou	hra	k1gFnSc7	hra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
Malostranské	malostranský	k2eAgFnSc6d1	Malostranská
besedě	beseda	k1gFnSc6	beseda
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
divadlo	divadlo	k1gNnSc1	divadlo
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
do	do	k7c2	do
pražské	pražský	k2eAgFnSc2d1	Pražská
Reduty	reduta	k1gFnSc2	reduta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1973	[number]	k4	1973
premiéru	premiéra	k1gFnSc4	premiéra
hra	hra	k1gFnSc1	hra
Cimrman	Cimrman	k1gMnSc1	Cimrman
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
hudby	hudba	k1gFnSc2	hudba
od	od	k7c2	od
téže	tenže	k3xDgFnSc2	tenže
autorské	autorský	k2eAgFnSc2d1	autorská
dvojice	dvojice	k1gFnSc2	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
ještě	ještě	k9	ještě
Svěrák	Svěrák	k1gMnSc1	Svěrák
hrál	hrát	k5eAaImAgMnS	hrát
učitele	učitel	k1gMnPc4	učitel
ve	v	k7c6	v
filmu	film	k1gInSc6	film
30	[number]	k4	30
panen	panna	k1gFnPc2	panna
a	a	k8xC	a
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
režírovaném	režírovaný	k2eAgInSc6d1	režírovaný
Pavlem	Pavel	k1gMnSc7	Pavel
Hoblem	Hobl	k1gMnSc7	Hobl
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
na	na	k7c6	na
konci	konec	k1gInSc6	konec
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
opustil	opustit	k5eAaPmAgMnS	opustit
Svěrák	Svěrák	k1gMnSc1	Svěrák
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
a	a	k8xC	a
vysílání	vysílání	k1gNnSc1	vysílání
jím	jíst	k5eAaImIp1nS	jíst
podepsaných	podepsaný	k2eAgFnPc2d1	podepsaná
písní	píseň	k1gFnPc2	píseň
nebylo	být	k5eNaImAgNnS	být
žádoucí	žádoucí	k2eAgNnSc1d1	žádoucí
<g/>
,	,	kIx,	,
vydával	vydávat	k5eAaImAgMnS	vydávat
svou	svůj	k3xOyFgFnSc4	svůj
písňovou	písňový	k2eAgFnSc4d1	písňová
tvorbu	tvorba	k1gFnSc4	tvorba
pod	pod	k7c7	pod
svým	svůj	k3xOyFgInSc7	svůj
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
užívaným	užívaný	k2eAgInSc7d1	užívaný
pseudonymem	pseudonym	k1gInSc7	pseudonym
Emil	Emil	k1gMnSc1	Emil
Synek	Synek	k1gMnSc1	Synek
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
pro	pro	k7c4	pro
Jiřího	Jiří	k1gMnSc4	Jiří
Schelingera	Schelinger	k1gMnSc4	Schelinger
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Holubí	holubí	k2eAgInSc1d1	holubí
dům	dům	k1gInSc1	dům
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Hit	hit	k1gInSc1	hit
století	století	k1gNnSc2	století
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
Ivana	Ivan	k1gMnSc4	Ivan
Mládka	Mládek	k1gMnSc2	Mládek
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Láďa	Láďa	k1gFnSc1	Láďa
jede	jet	k5eAaImIp3nS	jet
lodí	loď	k1gFnSc7	loď
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Mravenci	mravenec	k1gMnSc6	mravenec
v	v	k7c6	v
kredenci	kredenc	k1gInSc6	kredenc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Švadlenka	švadlenka	k1gFnSc1	švadlenka
Madlenka	Madlenka	k1gFnSc1	Madlenka
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Dívka	dívka	k1gFnSc1	dívka
s	s	k7c7	s
modrou	modrý	k2eAgFnSc7d1	modrá
matrací	matrace	k1gFnSc7	matrace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Svěrák	svěrák	k1gInSc1	svěrák
se	s	k7c7	s
Smoljakem	Smoljak	k1gMnSc7	Smoljak
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
napsat	napsat	k5eAaPmF	napsat
film	film	k1gInSc4	film
ze	z	k7c2	z
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
odkazu	odkaz	k1gInSc2	odkaz
na	na	k7c4	na
Járu	Jára	k1gFnSc4	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgInSc1	první
společný	společný	k2eAgInSc1d1	společný
filmový	filmový	k2eAgInSc1d1	filmový
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
Jáchyme	Jáchym	k1gMnSc5	Jáchym
<g/>
,	,	kIx,	,
hoď	hodit	k5eAaImRp2nS	hodit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
stroje	stroj	k1gInSc2	stroj
<g/>
!	!	kIx.	!
</s>
<s>
Režisérem	režisér	k1gMnSc7	režisér
snímku	snímek	k1gInSc2	snímek
byl	být	k5eAaImAgMnS	být
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lipský	lipský	k2eAgMnSc1d1	lipský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
scenáristé	scenárista	k1gMnPc1	scenárista
si	se	k3xPyFc3	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
také	také	k6eAd1	také
zahráli	zahrát	k5eAaPmAgMnP	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
první	první	k4xOgNnPc1	první
představení	představení	k1gNnPc1	představení
další	další	k2eAgFnSc2d1	další
hry	hra	k1gFnSc2	hra
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Svěrák	Svěrák	k1gMnSc1	Svěrák
se	s	k7c7	s
Smoljakem	Smoljak	k1gInSc7	Smoljak
sepsali	sepsat	k5eAaPmAgMnP	sepsat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Redutě	reduta	k1gFnSc6	reduta
měla	mít	k5eAaImAgFnS	mít
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
premiéru	premiéra	k1gFnSc4	premiéra
pohádka	pohádka	k1gFnSc1	pohádka
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Široký	Široký	k1gMnSc1	Široký
a	a	k8xC	a
Krátkozraký	krátkozraký	k2eAgMnSc1d1	krátkozraký
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
napsali	napsat	k5eAaPmAgMnP	napsat
Svěrák	svěrák	k1gInSc4	svěrák
společně	společně	k6eAd1	společně
se	s	k7c7	s
Smoljakem	Smoljak	k1gMnSc7	Smoljak
scénáře	scénář	k1gInPc1	scénář
ke	k	k7c3	k
dvěma	dva	k4xCgInPc3	dva
svým	svůj	k3xOyFgInPc3	svůj
filmům	film	k1gInPc3	film
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
měly	mít	k5eAaImAgInP	mít
oba	dva	k4xCgMnPc1	dva
premiéru	premiér	k1gMnSc3	premiér
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
filmech	film	k1gInPc6	film
si	se	k3xPyFc3	se
opět	opět	k6eAd1	opět
i	i	k9	i
zahráli	zahrát	k5eAaPmAgMnP	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
Marečku	Mareček	k1gMnSc5	Mareček
<g/>
,	,	kIx,	,
podejte	podat	k5eAaPmRp2nP	podat
mi	já	k3xPp1nSc3	já
pero	pero	k1gNnSc5	pero
<g/>
!	!	kIx.	!
</s>
<s>
režíroval	režírovat	k5eAaImAgMnS	režírovat
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lipský	lipský	k2eAgMnSc1d1	lipský
<g/>
.	.	kIx.	.
</s>
<s>
Námět	námět	k1gInSc1	námět
snímku	snímek	k1gInSc2	snímek
čerpá	čerpat	k5eAaImIp3nS	čerpat
z	z	k7c2	z
vlastních	vlastní	k2eAgFnPc2d1	vlastní
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
zkušeností	zkušenost	k1gFnPc2	zkušenost
scenáristů	scenárista	k1gMnPc2	scenárista
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
film	film	k1gInSc1	film
Na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
u	u	k7c2	u
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
snahu	snaha	k1gFnSc4	snaha
manželů	manžel	k1gMnPc2	manžel
Lavičkových	Lavičkových	k2eAgMnSc1d1	Lavičkových
získat	získat	k5eAaPmF	získat
rekreační	rekreační	k2eAgFnSc4d1	rekreační
chalupu	chalupa	k1gFnSc4	chalupa
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc4	takový
stavení	stavení	k1gNnSc4	stavení
naleznou	nalézt	k5eAaBmIp3nP	nalézt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
zatím	zatím	k6eAd1	zatím
děda	děda	k1gMnSc1	děda
Komárek	Komárek	k1gMnSc1	Komárek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
slibuje	slibovat	k5eAaImIp3nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prodá	prodat	k5eAaPmIp3nS	prodat
krávu	kráva	k1gFnSc4	kráva
a	a	k8xC	a
že	že	k8xS	že
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
již	již	k6eAd1	již
nezaseje	zasít	k5eNaPmIp3nS	zasít
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
nesplní	splnit	k5eNaPmIp3nS	splnit
a	a	k8xC	a
napřesrok	napřesrok	k6eAd1	napřesrok
opět	opět	k6eAd1	opět
zaseje	zasít	k5eAaPmIp3nS	zasít
<g/>
.	.	kIx.	.
</s>
<s>
Obdobnou	obdobný	k2eAgFnSc4d1	obdobná
zkušenost	zkušenost	k1gFnSc4	zkušenost
měli	mít	k5eAaImAgMnP	mít
i	i	k9	i
autoři	autor	k1gMnPc1	autor
scénáře	scénář	k1gInSc2	scénář
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
reálném	reálný	k2eAgInSc6d1	reálný
životě	život	k1gInSc6	život
také	také	k6eAd1	také
sháněli	shánět	k5eAaImAgMnP	shánět
rekreační	rekreační	k2eAgInSc4d1	rekreační
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Svěrákovi	Svěrák	k1gMnSc3	Svěrák
nakonec	nakonec	k6eAd1	nakonec
chalupu	chalupa	k1gFnSc4	chalupa
nalezli	naleznout	k5eAaPmAgMnP	naleznout
u	u	k7c2	u
Melechova	Melechův	k2eAgInSc2d1	Melechův
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
majitel	majitel	k1gMnSc1	majitel
Josef	Josef	k1gMnSc1	Josef
Lukšíček	Lukšíček	k1gMnSc1	Lukšíček
skutečně	skutečně	k6eAd1	skutečně
Svěrákovým	Svěrákův	k2eAgMnPc3d1	Svěrákův
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prodá	prodat	k5eAaPmIp3nS	prodat
krávu	kráva	k1gFnSc4	kráva
a	a	k8xC	a
že	že	k8xS	že
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
již	již	k6eAd1	již
nezaseje	zasít	k5eNaPmIp3nS	zasít
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přesně	přesně	k6eAd1	přesně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
ale	ale	k9	ale
Svěrákovi	Svěrákův	k2eAgMnPc1d1	Svěrákův
a	a	k8xC	a
Smoljakovi	Smoljakův	k2eAgMnPc1d1	Smoljakův
pořídili	pořídit	k5eAaPmAgMnP	pořídit
společnou	společný	k2eAgFnSc4d1	společná
chalupu	chalupa	k1gFnSc4	chalupa
v	v	k7c6	v
Růžkových	Růžkových	k2eAgFnPc6d1	Růžkových
Lhoticích	Lhotice	k1gFnPc6	Lhotice
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
ale	ale	k9	ale
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
rodinami	rodina	k1gFnPc7	rodina
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
každá	každý	k3xTgFnSc1	každý
si	se	k3xPyFc3	se
chtěla	chtít	k5eAaImAgFnS	chtít
na	na	k7c6	na
chatě	chata	k1gFnSc6	chata
užívat	užívat	k5eAaImF	užívat
svého	svůj	k3xOyFgNnSc2	svůj
soukromí	soukromí	k1gNnSc2	soukromí
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
urovnaly	urovnat	k5eAaPmAgInP	urovnat
až	až	k9	až
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Smoljak	Smoljak	k1gMnSc1	Smoljak
stavěl	stavět	k5eAaImAgMnS	stavět
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
chalupu	chalupa	k1gFnSc4	chalupa
Svěrákovi	Svěrák	k1gMnSc3	Svěrák
prodal	prodat	k5eAaPmAgMnS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1977	[number]	k4	1977
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
kolem	kolem	k7c2	kolem
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
Petra	Petr	k1gMnSc2	Petr
Uhla	Uhlus	k1gMnSc2	Uhlus
či	či	k8xC	či
Pavla	Pavel	k1gMnSc2	Pavel
Kohouta	Kohout	k1gMnSc2	Kohout
dokument	dokument	k1gInSc1	dokument
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Charta	charta	k1gFnSc1	charta
77	[number]	k4	77
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
upozorňoval	upozorňovat	k5eAaImAgInS	upozorňovat
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
země	země	k1gFnSc1	země
mezinárodními	mezinárodní	k2eAgInPc7d1	mezinárodní
smlouvami	smlouva	k1gFnPc7	smlouva
zavázala	zavázat	k5eAaPmAgFnS	zavázat
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
dodržována	dodržován	k2eAgNnPc4d1	dodržováno
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
jejích	její	k3xOp3gMnPc2	její
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
akt	akt	k1gInSc4	akt
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
sepsání	sepsání	k1gNnSc1	sepsání
"	"	kIx"	"
<g/>
Provolání	provolání	k1gNnSc1	provolání
československých	československý	k2eAgInPc2d1	československý
výborů	výbor	k1gInPc2	výbor
uměleckých	umělecký	k2eAgInPc2d1	umělecký
svazů	svaz	k1gInPc2	svaz
<g/>
:	:	kIx,	:
Za	za	k7c4	za
nové	nový	k2eAgInPc4d1	nový
tvůrčí	tvůrčí	k2eAgInPc4d1	tvůrčí
činy	čin	k1gInPc4	čin
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
míru	mír	k1gInSc2	mír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neoficiálně	neoficiálně	k6eAd1	neoficiálně
označované	označovaný	k2eAgFnPc1d1	označovaná
také	také	k9	také
jako	jako	k9	jako
Anticharta	anticharta	k1gFnSc1	anticharta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
svým	svůj	k3xOyFgInSc7	svůj
podpisem	podpis	k1gInSc7	podpis
podpořila	podpořit	k5eAaPmAgFnS	podpořit
většina	většina	k1gFnSc1	většina
herců	herec	k1gMnPc2	herec
<g/>
,	,	kIx,	,
hudebníků	hudebník	k1gMnPc2	hudebník
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
umělců	umělec	k1gMnPc2	umělec
vystupujících	vystupující	k2eAgMnPc2d1	vystupující
v	v	k7c6	v
zábavných	zábavný	k2eAgInPc6d1	zábavný
pořadech	pořad	k1gInPc6	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
původně	původně	k6eAd1	původně
tento	tento	k3xDgInSc4	tento
dokument	dokument	k1gInSc4	dokument
podepsat	podepsat	k5eAaPmF	podepsat
neplánovali	plánovat	k5eNaImAgMnP	plánovat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
Redutě	reduta	k1gFnSc6	reduta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
,	,	kIx,	,
oslovil	oslovit	k5eAaPmAgMnS	oslovit
Ladislava	Ladislav	k1gMnSc4	Ladislav
Smoljaka	Smoljak	k1gMnSc4	Smoljak
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dokument	dokument	k1gInSc4	dokument
nepodepsal	podepsat	k5eNaPmAgInS	podepsat
a	a	k8xC	a
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
strana	strana	k1gFnSc1	strana
mu	on	k3xPp3gMnSc3	on
proto	proto	k6eAd1	proto
zakázala	zakázat	k5eAaPmAgFnS	zakázat
kulturní	kulturní	k2eAgFnSc4d1	kulturní
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Anticharta	anticharta	k1gFnSc1	anticharta
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
kulturní	kulturní	k2eAgFnSc4d1	kulturní
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
totiž	totiž	k9	totiž
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
dokument	dokument	k1gInSc1	dokument
nepodepíše	podepsat	k5eNaPmIp3nS	podepsat
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
již	již	k6eAd1	již
moci	moct	k5eAaImF	moct
dál	daleko	k6eAd2	daleko
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
divadelního	divadelní	k2eAgInSc2d1	divadelní
souboru	soubor	k1gInSc2	soubor
se	se	k3xPyFc4	se
o	o	k7c6	o
celé	celý	k2eAgFnSc6d1	celá
věci	věc	k1gFnSc6	věc
poradili	poradit	k5eAaPmAgMnP	poradit
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
dokument	dokument	k1gInSc4	dokument
také	také	k9	také
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Seznamy	seznam	k1gInPc1	seznam
podepsaných	podepsaný	k1gMnPc2	podepsaný
otiskovalo	otiskovat	k5eAaImAgNnS	otiskovat
Rudé	rudý	k2eAgNnSc4d1	Rudé
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c4	o
Svěrákově	svěrákově	k6eAd1	svěrákově
a	a	k8xC	a
Čepelkově	čepelkově	k6eAd1	čepelkově
podpisu	podpis	k1gInSc3	podpis
Anticharty	anticharta	k1gFnSc2	anticharta
se	se	k3xPyFc4	se
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
ještě	ještě	k6eAd1	ještě
objevila	objevit	k5eAaPmAgFnS	objevit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Smoljak	Smoljak	k1gMnSc1	Smoljak
podepsal	podepsat	k5eAaPmAgMnS	podepsat
dokument	dokument	k1gInSc4	dokument
až	až	k9	až
po	po	k7c6	po
uzávěrce	uzávěrka	k1gFnSc6	uzávěrka
periodika	periodikum	k1gNnSc2	periodikum
a	a	k8xC	a
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
podepsání	podepsání	k1gNnSc6	podepsání
se	se	k3xPyFc4	se
tak	tak	k9	tak
již	již	k6eAd1	již
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
podpis	podpis	k1gInSc4	podpis
pod	pod	k7c7	pod
Antichartou	anticharta	k1gFnSc7	anticharta
se	se	k3xPyFc4	se
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
Svěrák	Svěrák	k1gMnSc1	Svěrák
veřejně	veřejně	k6eAd1	veřejně
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
pracoval	pracovat	k5eAaImAgMnS	pracovat
Svěrák	Svěrák	k1gMnSc1	Svěrák
coby	coby	k?	coby
scenárista	scenárista	k1gMnSc1	scenárista
ve	v	k7c6	v
Filmových	filmový	k2eAgFnPc6d1	filmová
studiích	studie	k1gFnPc6	studie
Barrandov	Barrandov	k1gInSc4	Barrandov
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
napsal	napsat	k5eAaPmAgInS	napsat
scénář	scénář	k1gInSc1	scénář
k	k	k7c3	k
filmové	filmový	k2eAgFnSc3d1	filmová
pohádce	pohádka	k1gFnSc3	pohádka
Ať	ať	k9	ať
žijí	žít	k5eAaImIp3nP	žít
duchové	duch	k1gMnPc1	duch
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
poté	poté	k6eAd1	poté
zrežíroval	zrežírovat	k5eAaPmAgMnS	zrežírovat
opět	opět	k6eAd1	opět
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lipský	lipský	k2eAgMnSc1d1	lipský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
další	další	k2eAgFnSc4d1	další
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
šestá	šestý	k4xOgFnSc1	šestý
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
napsaná	napsaný	k2eAgFnSc1d1	napsaná
společně	společně	k6eAd1	společně
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
a	a	k8xC	a
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Posel	posít	k5eAaPmAgInS	posít
z	z	k7c2	z
Liptákova	Liptákův	k2eAgMnSc2d1	Liptákův
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
první	první	k4xOgNnSc1	první
představení	představení	k1gNnSc1	představení
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Braníku	Braník	k1gInSc6	Braník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
připravil	připravit	k5eAaPmAgMnS	připravit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
představení	představení	k1gNnSc4	představení
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Všechno	všechen	k3xTgNnSc1	všechen
máme	mít	k5eAaImIp1nP	mít
schovaný	schovaný	k2eAgMnSc1d1	schovaný
<g/>
.	.	kIx.	.
</s>
<s>
Hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
v	v	k7c6	v
divadélku	divadélko	k1gNnSc6	divadélko
Albatros	albatros	k1gMnSc1	albatros
a	a	k8xC	a
vedle	vedle	k7c2	vedle
obou	dva	k4xCgMnPc2	dva
autorů	autor	k1gMnPc2	autor
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
také	také	k9	také
subreta	subreta	k1gFnSc1	subreta
karlínského	karlínský	k2eAgNnSc2d1	Karlínské
Hudebního	hudební	k2eAgNnSc2d1	hudební
divadla	divadlo	k1gNnSc2	divadlo
Libuše	Libuše	k1gFnSc2	Libuše
Cincibusová	Cincibusový	k2eAgFnSc1d1	Cincibusová
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
posléze	posléze	k6eAd1	posléze
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovská	k1gFnSc1	Filipovská
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Divadlo	divadlo	k1gNnSc1	divadlo
ABC	ABC	kA	ABC
požádalo	požádat	k5eAaPmAgNnS	požádat
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
Svěráka	Svěrák	k1gMnSc4	Svěrák
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
připravil	připravit	k5eAaPmAgMnS	připravit
představení	představení	k1gNnSc4	představení
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
proto	proto	k6eAd1	proto
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
představení	představení	k1gNnSc4	představení
Hrachová	hrachový	k2eAgFnSc1d1	hrachová
polévka	polévka	k1gFnSc1	polévka
z	z	k7c2	z
pytlíku	pytlík	k1gInSc2	pytlík
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
obsah	obsah	k1gInSc1	obsah
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
osudem	osud	k1gInSc7	osud
Dalibora	Dalibor	k1gMnSc2	Dalibor
z	z	k7c2	z
Jiráskových	Jiráskových	k2eAgFnPc2d1	Jiráskových
Starých	Starých	k2eAgFnPc2d1	Starých
pověstí	pověst	k1gFnPc2	pověst
českých	český	k2eAgFnPc2d1	Česká
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
operním	operní	k2eAgNnSc7d1	operní
zpracováním	zpracování	k1gNnSc7	zpracování
tématu	téma	k1gNnSc2	téma
od	od	k7c2	od
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vytvořeném	vytvořený	k2eAgNnSc6d1	vytvořené
představení	představení	k1gNnSc6	představení
účinkovali	účinkovat	k5eAaImAgMnP	účinkovat
vedle	vedle	k7c2	vedle
Svěráka	Svěrák	k1gMnSc2	Svěrák
se	s	k7c7	s
Smoljakem	Smoljak	k1gInSc7	Smoljak
ještě	ještě	k9	ještě
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
a	a	k8xC	a
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovská	k1gFnSc1	Filipovská
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
této	tento	k3xDgFnSc2	tento
pohádkové	pohádkový	k2eAgFnSc2d1	pohádková
komedie	komedie	k1gFnSc2	komedie
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
hraním	hraní	k1gNnSc7	hraní
se	se	k3xPyFc4	se
skončilo	skončit	k5eAaPmAgNnS	skončit
již	již	k6eAd1	již
po	po	k7c6	po
odehrání	odehrání	k1gNnSc6	odehrání
třiceti	třicet	k4xCc2	třicet
repríz	repríza	k1gFnPc2	repríza
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
režíroval	režírovat	k5eAaImAgMnS	režírovat
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
film	film	k1gInSc4	film
Kulový	kulový	k2eAgInSc4d1	kulový
blesk	blesk	k1gInSc4	blesk
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
si	se	k3xPyFc3	se
předtím	předtím	k6eAd1	předtím
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
napsali	napsat	k5eAaPmAgMnP	napsat
scénář	scénář	k1gInSc4	scénář
a	a	k8xC	a
oba	dva	k4xCgMnPc4	dva
si	se	k3xPyFc3	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
také	také	k9	také
zahráli	zahrát	k5eAaPmAgMnP	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
hned	hned	k6eAd1	hned
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
roce	rok	k1gInSc6	rok
dostal	dostat	k5eAaPmAgInS	dostat
Svěrák	svěrák	k1gInSc1	svěrák
roli	role	k1gFnSc4	role
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Věra	Věra	k1gFnSc1	Věra
Plívová-Šimková	Plívová-Šimková	k1gFnSc1	Plívová-Šimková
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
Brontosaurovi	brontosaurus	k1gMnSc6	brontosaurus
svěřila	svěřit	k5eAaPmAgFnS	svěřit
roli	role	k1gFnSc4	role
Bobra	bobr	k1gMnSc2	bobr
a	a	k8xC	a
Věra	Věra	k1gFnSc1	Věra
Chytilová	Chytilová	k1gFnSc1	Chytilová
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Kalamita	kalamita	k1gFnSc1	kalamita
postavu	postav	k1gInSc2	postav
učitele	učitel	k1gMnSc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
režíroval	režírovat	k5eAaImAgMnS	režírovat
film	film	k1gInSc4	film
Vrchní	vrchní	k1gMnSc1	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgMnSc3	který
napsal	napsat	k5eAaBmAgInS	napsat
scénář	scénář	k1gInSc1	scénář
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
umělci	umělec	k1gMnPc1	umělec
si	se	k3xPyFc3	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
také	také	k6eAd1	také
zahráli	zahrát	k5eAaPmAgMnP	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
scénáře	scénář	k1gInSc2	scénář
se	se	k3xPyFc4	se
Svěrákovi	Svěrák	k1gMnSc3	Svěrák
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc4	jeho
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc2	on
-	-	kIx~	-
oděného	oděný	k2eAgMnSc2d1	oděný
do	do	k7c2	do
tmavého	tmavý	k2eAgInSc2d1	tmavý
obleku	oblek	k1gInSc2	oblek
s	s	k7c7	s
motýlkem	motýlek	k1gInSc7	motýlek
-	-	kIx~	-
několik	několik	k4yIc1	několik
hostů	host	k1gMnPc2	host
restaurace	restaurace	k1gFnSc2	restaurace
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
pražské	pražský	k2eAgFnSc2d1	Pražská
Malostranské	malostranský	k2eAgFnSc2d1	Malostranská
besedy	beseda	k1gFnSc2	beseda
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
tamní	tamní	k2eAgFnSc6d1	tamní
návštěvě	návštěva	k1gFnSc6	návštěva
spletlo	splést	k5eAaPmAgNnS	splést
se	s	k7c7	s
skutečným	skutečný	k2eAgMnSc7d1	skutečný
vrchním	vrchní	k1gMnSc7	vrchní
a	a	k8xC	a
chtěli	chtít	k5eAaImAgMnP	chtít
mu	on	k3xPp3gMnSc3	on
platit	platit	k5eAaImF	platit
svou	svůj	k3xOyFgFnSc4	svůj
útratu	útrata	k1gFnSc4	útrata
<g/>
.	.	kIx.	.
</s>
<s>
Svěrák	Svěrák	k1gMnSc1	Svěrák
jim	on	k3xPp3gMnPc3	on
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gFnSc4	jejich
mýlku	mýlka	k1gFnSc4	mýlka
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
a	a	k8xC	a
žádné	žádný	k3yNgInPc4	žádný
peníze	peníz	k1gInPc4	peníz
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Smoljakem	Smoljak	k1gMnSc7	Smoljak
pak	pak	k6eAd1	pak
Svěrák	svěrák	k1gInSc4	svěrák
sepsal	sepsat	k5eAaPmAgMnS	sepsat
pro	pro	k7c4	pro
režiséra	režisér	k1gMnSc4	režisér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Podskalského	podskalský	k2eAgMnSc2d1	podskalský
hudební	hudební	k2eAgNnSc4d1	hudební
komedii	komedie	k1gFnSc4	komedie
Trhák	trhák	k1gInSc4	trhák
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
oba	dva	k4xCgMnPc1	dva
dostali	dostat	k5eAaPmAgMnP	dostat
své	svůj	k3xOyFgFnPc4	svůj
role	role	k1gFnPc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
hrál	hrát	k5eAaImAgMnS	hrát
Svěrák	Svěrák	k1gMnSc1	Svěrák
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jako	jako	k9	jako
zajíci	zajíc	k1gMnPc1	zajíc
roli	role	k1gFnSc4	role
tatínka	tatínek	k1gMnSc2	tatínek
a	a	k8xC	a
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jsme	být	k5eAaImIp1nP	být
normální	normální	k2eAgInPc1d1	normální
hrál	hrát	k5eAaImAgInS	hrát
vedoucího	vedoucí	k2eAgNnSc2d1	vedoucí
školení	školení	k1gNnSc2	školení
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
mihl	mihnout	k5eAaPmAgInS	mihnout
v	v	k7c6	v
roli	role	k1gFnSc6	role
vědce	vědec	k1gMnSc2	vědec
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Srdečný	srdečný	k2eAgInSc1d1	srdečný
pozdrav	pozdrav	k1gInSc1	pozdrav
ze	z	k7c2	z
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
letních	letní	k2eAgFnPc2d1	letní
prázdnin	prázdniny	k1gFnPc2	prázdniny
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
divadlo	divadlo	k1gNnSc4	divadlo
Járy	Jára	k1gFnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
do	do	k7c2	do
Divadla	divadlo	k1gNnSc2	divadlo
Solidarita	solidarita	k1gFnSc1	solidarita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
přesunu	přesun	k1gInSc6	přesun
z	z	k7c2	z
Braníka	Braník	k1gInSc2	Braník
a	a	k8xC	a
prvních	první	k4xOgInPc6	první
měsících	měsíc	k1gInPc6	měsíc
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
působišti	působiště	k1gNnSc6	působiště
vypovídá	vypovídat	k5eAaImIp3nS	vypovídat
filmový	filmový	k2eAgInSc1d1	filmový
snímek	snímek	k1gInSc1	snímek
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
natočený	natočený	k2eAgInSc1d1	natočený
ale	ale	k8xC	ale
až	až	k9	až
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
Svěrák	Svěrák	k1gMnSc1	Svěrák
společně	společně	k6eAd1	společně
se	s	k7c7	s
Smoljakem	Smoljak	k1gInSc7	Smoljak
napsali	napsat	k5eAaPmAgMnP	napsat
scénář	scénář	k1gInSc4	scénář
a	a	k8xC	a
oba	dva	k4xCgInPc4	dva
dva	dva	k4xCgInPc4	dva
si	se	k3xPyFc3	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
také	také	k9	také
zahráli	zahrát	k5eAaPmAgMnP	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Svěrák	Svěrák	k1gMnSc1	Svěrák
navíc	navíc	k6eAd1	navíc
ještě	ještě	k6eAd1	ještě
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Slavnosti	slavnost	k1gFnSc2	slavnost
sněženek	sněženka	k1gFnPc2	sněženka
v	v	k7c6	v
roli	role	k1gFnSc6	role
řidiče	řidič	k1gMnSc2	řidič
trabantu	trabant	k1gInSc2	trabant
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
natočením	natočení	k1gNnSc7	natočení
filmu	film	k1gInSc2	film
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
napsal	napsat	k5eAaPmAgMnS	napsat
Svěrák	Svěrák	k1gMnSc1	Svěrák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
scénář	scénář	k1gInSc1	scénář
k	k	k7c3	k
filmové	filmový	k2eAgFnSc3d1	filmová
pohádce	pohádka	k1gFnSc3	pohádka
Tři	tři	k4xCgMnPc1	tři
veteráni	veterán	k1gMnPc1	veterán
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Smoljakem	Smoljak	k1gInSc7	Smoljak
též	též	k9	též
k	k	k7c3	k
filmu	film	k1gInSc3	film
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
ležící	ležící	k2eAgMnSc1d1	ležící
<g/>
,	,	kIx,	,
spící	spící	k2eAgMnSc1d1	spící
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
filmech	film	k1gInPc6	film
si	se	k3xPyFc3	se
Svěrák	svěrák	k1gInSc1	svěrák
také	také	k9	také
zahrál	zahrát	k5eAaPmAgInS	zahrát
<g/>
;	;	kIx,	;
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
roli	role	k1gFnSc4	role
ministra	ministr	k1gMnSc2	ministr
a	a	k8xC	a
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
ležící	ležící	k2eAgFnSc4d1	ležící
<g/>
,	,	kIx,	,
spící	spící	k2eAgFnSc4d1	spící
postavu	postava	k1gFnSc4	postava
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
socialistických	socialistický	k2eAgInPc2d1	socialistický
úřadů	úřad	k1gInPc2	úřad
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
hra	hra	k1gFnSc1	hra
Lijavec	lijavec	k1gInSc1	lijavec
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
poslední	poslední	k2eAgNnSc1d1	poslední
představení	představení	k1gNnSc1	představení
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1983	[number]	k4	1983
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
Redutě	reduta	k1gFnSc6	reduta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
hrál	hrát	k5eAaImAgMnS	hrát
Svěrák	Svěrák	k1gMnSc1	Svěrák
roli	role	k1gFnSc4	role
docenta	docent	k1gMnSc2	docent
Zajíce	Zajíc	k1gMnSc2	Zajíc
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jak	jak	k6eAd1	jak
básníci	básník	k1gMnPc1	básník
přicházejí	přicházet	k5eAaImIp3nP	přicházet
o	o	k7c4	o
iluze	iluze	k1gFnPc4	iluze
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Dušan	Dušan	k1gMnSc1	Dušan
Klein	Klein	k1gMnSc1	Klein
<g/>
)	)	kIx)	)
a	a	k8xC	a
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
sepsali	sepsat	k5eAaPmAgMnP	sepsat
scénář	scénář	k1gInSc4	scénář
ke	k	k7c3	k
snímku	snímek	k1gInSc3	snímek
Rozpuštěný	rozpuštěný	k2eAgMnSc1d1	rozpuštěný
a	a	k8xC	a
vypuštěný	vypuštěný	k2eAgMnSc1d1	vypuštěný
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
natáčením	natáčení	k1gNnSc7	natáčení
ovšem	ovšem	k9	ovšem
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
scenáristy	scenárista	k1gMnSc2	scenárista
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
<g/>
.	.	kIx.	.
</s>
<s>
Námět	námět	k1gInSc1	námět
filmu	film	k1gInSc2	film
totiž	totiž	k9	totiž
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
salónním	salónní	k2eAgMnSc6d1	salónní
coupé	coupý	k2eAgInPc4d1	coupý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
inspektora	inspektor	k1gMnSc4	inspektor
Trachtu	Trachtu	k?	Trachtu
<g/>
,	,	kIx,	,
alternuje	alternovat	k5eAaImIp3nS	alternovat
v	v	k7c6	v
divadelním	divadelní	k2eAgNnSc6d1	divadelní
představení	představení	k1gNnSc6	představení
Svěrák	svěrák	k1gInSc4	svěrák
se	s	k7c7	s
Smoljakem	Smoljak	k1gMnSc7	Smoljak
a	a	k8xC	a
v	v	k7c6	v
postavě	postava	k1gFnSc6	postava
Hlaváčka	Hlaváček	k1gMnSc2	Hlaváček
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
Miloň	Miloň	k1gFnSc1	Miloň
Čepelka	čepelka	k1gFnSc1	čepelka
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Bruknerem	Brukner	k1gMnSc7	Brukner
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
očekávali	očekávat	k5eAaImAgMnP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
rolích	role	k1gFnPc6	role
objeví	objevit	k5eAaPmIp3nS	objevit
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
film	film	k1gInSc4	film
režíroval	režírovat	k5eAaImAgMnS	režírovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
obsadit	obsadit	k5eAaPmF	obsadit
tyto	tento	k3xDgFnPc4	tento
postavy	postava	k1gFnPc4	postava
profesionálními	profesionální	k2eAgInPc7d1	profesionální
herci	herc	k1gInPc7	herc
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tedy	tedy	k9	tedy
Svěrák	Svěrák	k1gMnSc1	Svěrák
nehrál	hrát	k5eNaImAgMnS	hrát
Trachtu	Trachtu	k?	Trachtu
(	(	kIx(	(
<g/>
toho	ten	k3xDgNnSc2	ten
ve	v	k7c6	v
filmu	film	k1gInSc6	film
ztvárňuje	ztvárňovat	k5eAaImIp3nS	ztvárňovat
Jiří	Jiří	k1gMnSc1	Jiří
Zahajský	Zahajský	k2eAgMnSc1d1	Zahajský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
mu	on	k3xPp3gMnSc3	on
Smoljak	Smoljak	k1gInSc4	Smoljak
roli	role	k1gFnSc4	role
hajného	hajný	k1gMnSc2	hajný
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
i	i	k9	i
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
nakonec	nakonec	k6eAd1	nakonec
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Rudolfa	Rudolf	k1gMnSc4	Rudolf
Hrušínského	Hrušínský	k2eAgMnSc4d1	Hrušínský
a	a	k8xC	a
Svěráka	Svěrák	k1gMnSc4	Svěrák
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
role	role	k1gFnSc2	role
průmyslového	průmyslový	k2eAgMnSc2d1	průmyslový
básníka	básník	k1gMnSc2	básník
Jelínka	Jelínek	k1gMnSc2	Jelínek
<g/>
.	.	kIx.	.
</s>
<s>
Svěrák	svěrák	k1gInSc1	svěrák
sice	sice	k8xC	sice
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
nakonec	nakonec	k6eAd1	nakonec
vzal	vzít	k5eAaPmAgMnS	vzít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
naučí	naučit	k5eAaPmIp3nS	naučit
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
i	i	k9	i
Smoljak	Smoljak	k1gMnSc1	Smoljak
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
profesionála	profesionál	k1gMnSc4	profesionál
<g/>
.	.	kIx.	.
</s>
<s>
Souhlasil	souhlasit	k5eAaImAgInS	souhlasit
proto	proto	k8xC	proto
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
režiséra	režisér	k1gMnSc2	režisér
Víta	Vít	k1gMnSc2	Vít
Olmera	Olmer	k1gMnSc2	Olmer
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
doktore	doktor	k1gMnSc5	doktor
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Smoljakově	Smoljakův	k2eAgNnSc6d1	Smoljakovo
uznání	uznání	k1gNnSc6	uznání
svých	svůj	k3xOyFgFnPc2	svůj
hereckých	herecký	k2eAgFnPc2d1	herecká
kvalit	kvalita	k1gFnPc2	kvalita
se	se	k3xPyFc4	se
Svěrák	Svěrák	k1gMnSc1	Svěrák
zprostředkovaně	zprostředkovaně	k6eAd1	zprostředkovaně
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
až	až	k9	až
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
Olmerově	Olmerův	k2eAgInSc6d1	Olmerův
snímku	snímek	k1gInSc6	snímek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
Svěrák	Svěrák	k1gMnSc1	Svěrák
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
film	film	k1gInSc1	film
Jako	jako	k8xC	jako
jed	jed	k1gInSc1	jed
natočený	natočený	k2eAgInSc1d1	natočený
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Dobytí	dobytí	k1gNnSc2	dobytí
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
Čechem	Čech	k1gMnSc7	Čech
Karlem	Karel	k1gMnSc7	Karel
Němcem	Němec	k1gMnSc7	Němec
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
spolu	spolu	k6eAd1	spolu
sepsali	sepsat	k5eAaPmAgMnP	sepsat
Svěrák	Svěrák	k1gMnSc1	Svěrák
se	s	k7c7	s
Smoljakem	Smoljak	k1gMnSc7	Smoljak
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
první	první	k4xOgNnSc1	první
uvedení	uvedení	k1gNnSc1	uvedení
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1985	[number]	k4	1985
a	a	k8xC	a
až	až	k9	až
o	o	k7c4	o
osm	osm	k4xCc4	osm
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
-	-	kIx~	-
zhlédli	zhlédnout	k5eAaPmAgMnP	zhlédnout
hru	hra	k1gFnSc4	hra
také	také	k9	také
diváci	divák	k1gMnPc1	divák
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
napsal	napsat	k5eAaPmAgMnS	napsat
Svěrák	Svěrák	k1gMnSc1	Svěrák
scénář	scénář	k1gInSc4	scénář
k	k	k7c3	k
filmu	film	k1gInSc3	film
Vesničko	vesnička	k1gFnSc5	vesnička
má	mít	k5eAaImIp3nS	mít
středisková	střediskový	k2eAgFnSc1d1	středisková
<g/>
.	.	kIx.	.
</s>
<s>
Impulzem	impulz	k1gInSc7	impulz
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
sepsání	sepsání	k1gNnSc3	sepsání
byla	být	k5eAaImAgFnS	být
účetní	účetní	k2eAgFnSc1d1	účetní
chyba	chyba	k1gFnSc1	chyba
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
Filmových	filmový	k2eAgInPc6d1	filmový
ateliérech	ateliér	k1gInPc6	ateliér
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
na	na	k7c4	na
blížící	blížící	k2eAgFnPc4d1	blížící
se	se	k3xPyFc4	se
revizi	revize	k1gFnSc3	revize
v	v	k7c6	v
oddělení	oddělení	k1gNnSc6	oddělení
scénářů	scénář	k1gInPc2	scénář
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zdeňku	Zdeněk	k1gMnSc6	Zdeněk
Svěrákovi	Svěrák	k1gMnSc6	Svěrák
a	a	k8xC	a
Ladislavu	Ladislav	k1gMnSc6	Ladislav
Smoljakovi	Smoljak	k1gMnSc6	Smoljak
před	před	k7c7	před
časem	čas	k1gInSc7	čas
vyplatili	vyplatit	k5eAaPmAgMnP	vyplatit
finanční	finanční	k2eAgFnSc4d1	finanční
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
dodaný	dodaný	k2eAgInSc4d1	dodaný
námět	námět	k1gInSc4	námět
k	k	k7c3	k
filmu	film	k1gInSc3	film
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vrátit	vrátit	k5eAaPmF	vrátit
a	a	k8xC	a
barrandovský	barrandovský	k2eAgMnSc1d1	barrandovský
dramaturg	dramaturg	k1gMnSc1	dramaturg
Karel	Karel	k1gMnSc1	Karel
Cop	cop	k1gInSc1	cop
proto	proto	k8xC	proto
zvolil	zvolit	k5eAaPmAgInS	zvolit
jiné	jiný	k2eAgNnSc4d1	jiné
řešení	řešení	k1gNnSc4	řešení
spočívající	spočívající	k2eAgNnSc4d1	spočívající
v	v	k7c6	v
sepsání	sepsání	k1gNnSc6	sepsání
nového	nový	k2eAgInSc2d1	nový
námětu	námět	k1gInSc2	námět
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nenatočí	natočit	k5eNaBmIp3nP	natočit
a	a	k8xC	a
založí	založit	k5eAaPmIp3nP	založit
se	se	k3xPyFc4	se
do	do	k7c2	do
archivu	archiv	k1gInSc2	archiv
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
bude	být	k5eAaImBp3nS	být
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Požadovaný	požadovaný	k2eAgInSc1d1	požadovaný
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
však	však	k9	však
musel	muset	k5eAaImAgInS	muset
odehrávat	odehrávat	k5eAaImF	odehrávat
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
ovšem	ovšem	k9	ovšem
námět	námět	k1gInSc1	námět
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
Karel	Karel	k1gMnSc1	Karel
Cop	cop	k1gInSc4	cop
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ze	z	k7c2	z
sepsaného	sepsaný	k2eAgInSc2d1	sepsaný
textu	text	k1gInSc2	text
mohla	moct	k5eAaImAgFnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
povídka	povídka	k1gFnSc1	povídka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
když	když	k8xS	když
pak	pak	k9	pak
Svěrák	Svěrák	k1gMnSc1	Svěrák
cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
zájezdové	zájezdový	k2eAgNnSc4d1	zájezdové
představení	představení	k1gNnSc4	představení
s	s	k7c7	s
Divadlem	divadlo	k1gNnSc7	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
četl	číst	k5eAaImAgMnS	číst
svůj	svůj	k3xOyFgInSc4	svůj
výtvor	výtvor	k1gInSc4	výtvor
filmovému	filmový	k2eAgMnSc3d1	filmový
režisérovi	režisér	k1gMnSc3	režisér
Jiřímu	Jiří	k1gMnSc3	Jiří
Menzelovi	Menzel	k1gMnSc3	Menzel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
také	také	k9	také
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
,	,	kIx,	,
zalíbil	zalíbit	k5eAaPmAgMnS	zalíbit
se	se	k3xPyFc4	se
Menzelovi	Menzel	k1gMnSc3	Menzel
Svěrákův	Svěrákův	k2eAgInSc4d1	Svěrákův
text	text	k1gInSc4	text
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
filmového	filmový	k2eAgInSc2d1	filmový
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
si	se	k3xPyFc3	se
roli	role	k1gFnSc4	role
malíře	malíř	k1gMnSc2	malíř
Ryby	Ryba	k1gMnSc2	Ryba
zahrál	zahrát	k5eAaPmAgInS	zahrát
také	také	k9	také
Svěrák	svěrák	k1gInSc1	svěrák
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
divákům	divák	k1gMnPc3	divák
zalíbil	zalíbit	k5eAaPmAgMnS	zalíbit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
neanglicky	anglicky	k6eNd1	anglicky
mluvený	mluvený	k2eAgInSc4d1	mluvený
film	film	k1gInSc4	film
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Čtenáři	čtenář	k1gMnPc1	čtenář
internetového	internetový	k2eAgInSc2d1	internetový
portálu	portál	k1gInSc2	portál
Novinky	novinka	k1gFnSc2	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
jej	on	k3xPp3gNnSc4	on
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
českou	český	k2eAgFnSc4d1	Česká
filmovou	filmový	k2eAgFnSc4d1	filmová
komedii	komedie	k1gFnSc4	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
si	se	k3xPyFc3	se
dali	dát	k5eAaPmAgMnP	dát
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
minimálně	minimálně	k6eAd1	minimálně
osm	osm	k4xCc4	osm
písniček	písnička	k1gFnPc2	písnička
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
budou	být	k5eAaImBp3nP	být
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
pořadu	pořad	k1gInSc6	pořad
Hodina	hodina	k1gFnSc1	hodina
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
pravidelně	pravidelně	k6eAd1	pravidelně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
a	a	k8xC	a
písničky	písnička	k1gFnPc4	písnička
zpívá	zpívat	k5eAaImIp3nS	zpívat
dětský	dětský	k2eAgInSc1d1	dětský
sbor	sbor	k1gInSc1	sbor
Sedmihlásek	sedmihlásek	k1gMnSc1	sedmihlásek
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Režisérem	režisér	k1gMnSc7	režisér
pořadu	pořad	k1gInSc2	pořad
je	být	k5eAaImIp3nS	být
Viktor	Viktor	k1gMnSc1	Viktor
Polesný	polesný	k1gMnSc1	polesný
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tak	tak	k6eAd1	tak
režijně	režijně	k6eAd1	režijně
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
i	i	k9	i
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
hudební	hudební	k2eAgInPc4d1	hudební
klipy	klip	k1gInPc4	klip
ztvárňující	ztvárňující	k2eAgFnSc2d1	ztvárňující
písně	píseň	k1gFnSc2	píseň
této	tento	k3xDgFnSc2	tento
umělecké	umělecký	k2eAgFnSc2d1	umělecká
dvojice	dvojice	k1gFnSc2	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
jimi	on	k3xPp3gMnPc7	on
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
písní	píseň	k1gFnPc2	píseň
zlidověla	zlidovět	k5eAaPmAgFnS	zlidovět
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
rádiích	rádio	k1gNnPc6	rádio
se	se	k3xPyFc4	se
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
hrály	hrát	k5eAaImAgFnP	hrát
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc4	tři
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
"	"	kIx"	"
<g/>
Severní	severní	k2eAgFnSc1d1	severní
vítr	vítr	k1gInSc1	vítr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Holubí	holubí	k2eAgInSc1d1	holubí
dům	dům	k1gInSc1	dům
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
nutno	nutno	k6eAd1	nutno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
písní	píseň	k1gFnPc2	píseň
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
Svěrák	svěrák	k1gInSc1	svěrák
nejraději	rád	k6eAd3	rád
"	"	kIx"	"
<g/>
Semišku	semiška	k1gFnSc4	semiška
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
napsal	napsat	k5eAaPmAgMnS	napsat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
pro	pro	k7c4	pro
televizní	televizní	k2eAgInSc4d1	televizní
Večerníček	večerníček	k1gInSc4	večerníček
scénář	scénář	k1gInSc1	scénář
třináctidílného	třináctidílný	k2eAgInSc2d1	třináctidílný
seriálu	seriál	k1gInSc2	seriál
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
Radovanovy	Radovanův	k2eAgFnPc4d1	Radovanova
radovánky	radovánka	k1gFnPc4	radovánka
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
dubna	duben	k1gInSc2	duben
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
herec	herec	k1gMnSc1	herec
divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vozáb	Vozáb	k1gMnSc1	Vozáb
a	a	k8xC	a
před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
pohřeb	pohřeb	k1gInSc4	pohřeb
do	do	k7c2	do
Velvar	Velvar	k1gInSc1	Velvar
se	se	k3xPyFc4	se
zbylí	zbylý	k2eAgMnPc1d1	zbylý
členové	člen	k1gMnPc1	člen
divadla	divadlo	k1gNnSc2	divadlo
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
českobudějovické	českobudějovický	k2eAgFnSc6d1	českobudějovická
nemocnici	nemocnice	k1gFnSc6	nemocnice
leží	ležet	k5eAaImIp3nS	ležet
jejich	jejich	k3xOp3gNnSc1	jejich
kolega	kolega	k1gMnSc1	kolega
Jan	Jan	k1gMnSc1	Jan
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
pádem	pád	k1gInSc7	pád
ze	z	k7c2	z
stromu	strom	k1gInSc2	strom
přivodil	přivodit	k5eAaBmAgInS	přivodit
poranění	poranění	k1gNnSc4	poranění
páteře	páteř	k1gFnSc2	páteř
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
k	k	k7c3	k
ochrnutí	ochrnutí	k1gNnSc3	ochrnutí
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
Kašparovo	Kašparův	k2eAgNnSc4d1	Kašparovo
zranění	zranění	k1gNnSc4	zranění
se	se	k3xPyFc4	se
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
uvádělo	uvádět	k5eAaImAgNnS	uvádět
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
během	během	k7c2	během
představení	představení	k1gNnSc2	představení
sedí	sedit	k5eAaImIp3nP	sedit
<g/>
,	,	kIx,	,
a	a	k8xC	a
divák	divák	k1gMnSc1	divák
tak	tak	k9	tak
nepozná	poznat	k5eNaPmIp3nS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnSc1	jejich
představitel	představitel	k1gMnSc1	představitel
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
chůze	chůze	k1gFnPc4	chůze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
petice	petice	k1gFnSc1	petice
Několik	několik	k4yIc4	několik
vět	věta	k1gFnPc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
Svěrák	Svěrák	k1gMnSc1	Svěrák
se	s	k7c7	s
Smoljakem	Smoljak	k1gMnSc7	Smoljak
podepsali	podepsat	k5eAaPmAgMnP	podepsat
a	a	k8xC	a
svými	svůj	k3xOyFgInPc7	svůj
podpisy	podpis	k1gInPc7	podpis
také	také	k6eAd1	také
podpořili	podpořit	k5eAaPmAgMnP	podpořit
výzvu	výzva	k1gFnSc4	výzva
za	za	k7c4	za
propuštění	propuštění	k1gNnSc4	propuštění
vězněného	vězněný	k2eAgMnSc2d1	vězněný
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
Svěrák	Svěrák	k1gMnSc1	Svěrák
aktivně	aktivně	k6eAd1	aktivně
účastnil	účastnit	k5eAaImAgMnS	účastnit
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
například	například	k6eAd1	například
navštívil	navštívit	k5eAaPmAgMnS	navštívit
studenty	student	k1gMnPc4	student
liberecké	liberecký	k2eAgFnSc2d1	liberecká
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
strojní	strojní	k2eAgFnSc2d1	strojní
a	a	k8xC	a
textilní	textilní	k2eAgFnSc2d1	textilní
(	(	kIx(	(
<g/>
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
o	o	k7c6	o
probíhajících	probíhající	k2eAgFnPc6d1	probíhající
společenských	společenský	k2eAgFnPc6d1	společenská
změnách	změna	k1gFnPc6	změna
a	a	k8xC	a
představil	představit	k5eAaPmAgMnS	představit
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
názor	názor	k1gInSc1	názor
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
na	na	k7c6	na
kontrarevoluci	kontrarevoluce	k1gFnSc6	kontrarevoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
května	květen	k1gInSc2	květen
1990	[number]	k4	1990
měla	mít	k5eAaImAgFnS	mít
první	první	k4xOgFnSc1	první
představení	představení	k1gNnPc1	představení
jedenáctá	jedenáctý	k4xOgFnSc1	jedenáctý
hra	hra	k1gFnSc1	hra
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
dvojice	dvojice	k1gFnSc2	dvojice
Svěrák	Svěrák	k1gMnSc1	Svěrák
-	-	kIx~	-
Smoljak	Smoljak	k1gInSc1	Smoljak
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Blaník	Blaník	k1gInSc1	Blaník
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
film	film	k1gInSc1	film
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
první	první	k4xOgInSc4	první
společný	společný	k2eAgInSc4d1	společný
snímek	snímek	k1gInSc4	snímek
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
scénáře	scénář	k1gInSc2	scénář
filmu	film	k1gInSc2	film
bylo	být	k5eAaImAgNnS	být
vyprávění	vyprávění	k1gNnSc1	vyprávění
školních	školní	k2eAgFnPc2d1	školní
příhod	příhoda	k1gFnPc2	příhoda
účastníky	účastník	k1gMnPc7	účastník
dotočné	dotočný	k2eAgFnSc6d1	dotočná
k	k	k7c3	k
filmu	film	k1gInSc3	film
Víta	Vít	k1gMnSc2	Vít
Olmera	Olmer	k1gMnSc2	Olmer
Jako	jako	k8xS	jako
jed	jed	k1gInSc1	jed
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
košickém	košický	k2eAgInSc6d1	košický
hotelu	hotel	k1gInSc6	hotel
Slovan	Slovan	k1gInSc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Olmer	Olmer	k1gInSc4	Olmer
Svěráka	Svěrák	k1gMnSc2	Svěrák
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
z	z	k7c2	z
příběhů	příběh	k1gInPc2	příběh
sepíše	sepsat	k5eAaPmIp3nS	sepsat
film	film	k1gInSc1	film
o	o	k7c6	o
dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
a	a	k8xC	a
Svěrák	svěrák	k1gInSc1	svěrák
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
psaní	psaní	k1gNnSc6	psaní
použil	použít	k5eAaPmAgInS	použít
nejen	nejen	k6eAd1	nejen
zážitky	zážitek	k1gInPc4	zážitek
své	svůj	k3xOyFgNnSc1	svůj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
Oty	Ota	k1gMnSc2	Ota
Kopřivy	Kopřiva	k1gMnSc2	Kopřiva
<g/>
,	,	kIx,	,
kameramana	kameraman	k1gMnSc2	kameraman
snímku	snímek	k1gInSc2	snímek
Jako	jako	k8xC	jako
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Vítem	Vít	k1gMnSc7	Vít
Olmerem	Olmer	k1gMnSc7	Olmer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
film	film	k1gInSc4	film
režírovat	režírovat	k5eAaImF	režírovat
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
Svěrák	Svěrák	k1gMnSc1	Svěrák
vybíral	vybírat	k5eAaImAgMnS	vybírat
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
postavy	postava	k1gFnPc4	postava
vhodné	vhodný	k2eAgFnSc3d1	vhodná
herce	herka	k1gFnSc3	herka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
najednou	najednou	k6eAd1	najednou
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Olmer	Olmer	k1gInSc1	Olmer
dal	dát	k5eAaPmAgInS	dát
přednost	přednost	k1gFnSc4	přednost
natáčení	natáčení	k1gNnSc2	natáčení
filmu	film	k1gInSc2	film
Tankový	tankový	k2eAgInSc4d1	tankový
prapor	prapor	k1gInSc4	prapor
a	a	k8xC	a
o	o	k7c4	o
Svěrákův	Svěrákův	k2eAgInSc4d1	Svěrákův
scénář	scénář	k1gInSc4	scénář
již	již	k6eAd1	již
nemá	mít	k5eNaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barrandovských	barrandovský	k2eAgInPc6d1	barrandovský
ateliérech	ateliér	k1gInPc6	ateliér
tak	tak	k9	tak
Svěrákovi	Svěrákův	k2eAgMnPc1d1	Svěrákův
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
film	film	k1gInSc4	film
natočí	natočit	k5eAaBmIp3nS	natočit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
ale	ale	k9	ale
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
filmový	filmový	k2eAgInSc1d1	filmový
dokument	dokument	k1gInSc1	dokument
a	a	k8xC	a
hraný	hraný	k2eAgInSc1d1	hraný
snímek	snímek	k1gInSc1	snímek
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
netočil	točit	k5eNaImAgMnS	točit
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
však	však	k9	však
začal	začít	k5eAaPmAgMnS	začít
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
film	film	k1gInSc4	film
točit	točit	k5eAaImF	točit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
s	s	k7c7	s
kvalitou	kvalita	k1gFnSc7	kvalita
natáčeného	natáčený	k2eAgNnSc2d1	natáčené
díla	dílo	k1gNnSc2	dílo
spokojen	spokojen	k2eAgMnSc1d1	spokojen
a	a	k8xC	a
ztratil	ztratit	k5eAaPmAgMnS	ztratit
obavy	obava	k1gFnPc4	obava
o	o	k7c4	o
jeho	jeho	k3xOp3gInSc4	jeho
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
posléze	posléze	k6eAd1	posléze
získal	získat	k5eAaPmAgInS	získat
též	též	k9	též
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
neanglicky	anglicky	k6eNd1	anglicky
mluvený	mluvený	k2eAgInSc4d1	mluvený
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
ale	ale	k9	ale
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hodnotitelé	hodnotitel	k1gMnPc1	hodnotitel
upřednostnili	upřednostnit	k5eAaPmAgMnP	upřednostnit
italský	italský	k2eAgInSc4d1	italský
film	film	k1gInSc4	film
Mediterraneo	Mediterraneo	k6eAd1	Mediterraneo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
navíc	navíc	k6eAd1	navíc
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
opustil	opustit	k5eAaPmAgMnS	opustit
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
scenáristy	scenárista	k1gMnSc2	scenárista
v	v	k7c6	v
Barrandovských	barrandovský	k2eAgInPc6d1	barrandovský
ateliérech	ateliér	k1gInPc6	ateliér
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
svobodném	svobodný	k2eAgNnSc6d1	svobodné
povolání	povolání	k1gNnSc6	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
během	běh	k1gInSc7	běh
léta	léto	k1gNnSc2	léto
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
-	-	kIx~	-
tentokrát	tentokrát	k6eAd1	tentokrát
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
blíže	blíž	k1gFnSc2	blíž
divákům	divák	k1gMnPc3	divák
-	-	kIx~	-
ze	z	k7c2	z
Strašnic	Strašnice	k1gFnPc2	Strašnice
na	na	k7c4	na
Žižkov	Žižkov	k1gInSc4	Žižkov
do	do	k7c2	do
Štítného	štítný	k2eAgInSc2d1	štítný
ulice	ulice	k1gFnPc1	ulice
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
výzkumy	výzkum	k1gInPc1	výzkum
"	"	kIx"	"
<g/>
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
Cimrman	Cimrman	k1gMnSc1	Cimrman
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
ulici	ulice	k1gFnSc6	ulice
kdysi	kdysi	k6eAd1	kdysi
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
premiérou	premiéra	k1gFnSc7	premiéra
souboru	soubor	k1gInSc2	soubor
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
budově	budova	k1gFnSc6	budova
se	s	k7c7	s
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1994	[number]	k4	1994
stalo	stát	k5eAaPmAgNnS	stát
představení	představení	k1gNnSc1	představení
Záskok	záskok	k1gInSc1	záskok
o	o	k7c6	o
nešťastné	šťastný	k2eNgFnSc6d1	nešťastná
premiéře	premiéra	k1gFnSc6	premiéra
hry	hra	k1gFnSc2	hra
Vlasta	Vlasta	k1gFnSc1	Vlasta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sepsání	sepsání	k1gNnSc6	sepsání
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
spolupodíleli	spolupodílet	k5eAaImAgMnP	spolupodílet
Svěrák	Svěrák	k1gMnSc1	Svěrák
se	s	k7c7	s
Smoljakem	Smoljak	k1gMnSc7	Smoljak
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
spoluzaložil	spoluzaložit	k5eAaPmAgMnS	spoluzaložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
se	s	k7c7	s
Svazem	svaz	k1gInSc7	svaz
paraplegiků	paraplegik	k1gMnPc2	paraplegik
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Olgou	Olga	k1gFnSc7	Olga
Havlovou	Havlová	k1gFnSc7	Havlová
Centrum	centrum	k1gNnSc1	centrum
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
pomoci	pomoc	k1gFnSc2	pomoc
Paraple	Paraple	k?	Paraple
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ochrnuli	ochrnout	k5eAaPmAgMnP	ochrnout
následkem	následkem	k7c2	následkem
poškození	poškození	k1gNnSc2	poškození
míchy	mícha	k1gFnSc2	mícha
vlivem	vlivem	k7c2	vlivem
úrazu	úraz	k1gInSc2	úraz
a	a	k8xC	a
nebo	nebo	k8xC	nebo
vinou	vinout	k5eAaImIp3nP	vinout
onemocnění	onemocnění	k1gNnSc4	onemocnění
během	během	k7c2	během
jejich	jejich	k3xOp3gInSc2	jejich
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
i	i	k8xC	i
rodinám	rodina	k1gFnPc3	rodina
postižených	postižený	k2eAgFnPc2d1	postižená
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c6	o
překonání	překonání	k1gNnSc6	překonání
této	tento	k3xDgFnSc2	tento
životní	životní	k2eAgFnSc2d1	životní
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
shromažďování	shromažďování	k1gNnSc2	shromažďování
potřebných	potřebný	k2eAgInPc2d1	potřebný
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
takto	takto	k6eAd1	takto
postiženým	postižený	k2eAgMnSc7d1	postižený
pořádá	pořádat	k5eAaImIp3nS	pořádat
již	již	k9	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
Dobročinnou	dobročinný	k2eAgFnSc4d1	dobročinná
akademii	akademie	k1gFnSc4	akademie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
přímým	přímý	k2eAgInSc7d1	přímý
přenosem	přenos	k1gInSc7	přenos
vysílá	vysílat	k5eAaImIp3nS	vysílat
jak	jak	k6eAd1	jak
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ní	on	k3xPp3gFnSc3	on
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
umělci	umělec	k1gMnPc1	umělec
a	a	k8xC	a
diváci	divák	k1gMnPc1	divák
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
přislíbit	přislíbit	k5eAaPmF	přislíbit
zaslání	zaslání	k1gNnSc4	zaslání
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
účet	účet	k1gInSc4	účet
Centra	centrum	k1gNnSc2	centrum
Paraple	Paraple	k?	Paraple
(	(	kIx(	(
<g/>
a	a	k8xC	a
pak	pak	k6eAd1	pak
částku	částka	k1gFnSc4	částka
skutečně	skutečně	k6eAd1	skutečně
odeslat	odeslat	k5eAaPmF	odeslat
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
centrum	centrum	k1gNnSc1	centrum
podpořit	podpořit	k5eAaPmF	podpořit
zasláním	zaslání	k1gNnSc7	zaslání
dárcovské	dárcovský	k2eAgFnSc2d1	dárcovská
SMS	SMS	kA	SMS
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
akademie	akademie	k1gFnSc2	akademie
pořádá	pořádat	k5eAaImIp3nS	pořádat
Svěrák	Svěrák	k1gMnSc1	Svěrák
pro	pro	k7c4	pro
Centrum	centrum	k1gNnSc4	centrum
Paraple	Paraple	k?	Paraple
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
benefiční	benefiční	k2eAgFnSc4d1	benefiční
akci	akce	k1gFnSc4	akce
"	"	kIx"	"
<g/>
Běh	běh	k1gInSc1	běh
pro	pro	k7c4	pro
Paraple	Paraple	k?	Paraple
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
či	či	k8xC	či
v	v	k7c6	v
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c6	na
koloběžkách	koloběžka	k1gFnPc6	koloběžka
na	na	k7c6	na
stometrové	stometrový	k2eAgFnSc6d1	stometrová
trati	trať	k1gFnSc6	trať
spolu	spolu	k6eAd1	spolu
zápolí	zápolit	k5eAaImIp3nP	zápolit
rodiče	rodič	k1gMnPc1	rodič
nebo	nebo	k8xC	nebo
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
nebo	nebo	k8xC	nebo
samy	sám	k3xTgFnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Klání	klání	k1gNnSc1	klání
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
i	i	k9	i
veřejně	veřejně	k6eAd1	veřejně
známé	známý	k2eAgFnPc4d1	známá
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
Akumulátor	akumulátor	k1gInSc1	akumulátor
1	[number]	k4	1
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
druhý	druhý	k4xOgInSc4	druhý
snímek	snímek	k1gInSc4	snímek
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
napsal	napsat	k5eAaBmAgMnS	napsat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
a	a	k8xC	a
pod	pod	k7c7	pod
režijním	režijní	k2eAgNnSc7d1	režijní
vedením	vedení	k1gNnSc7	vedení
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Jana	Jan	k1gMnSc2	Jan
se	se	k3xPyFc4	se
film	film	k1gInSc4	film
realizoval	realizovat	k5eAaBmAgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
si	se	k3xPyFc3	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
i	i	k9	i
zahrál	zahrát	k5eAaPmAgMnS	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
ve	v	k7c6	v
strašnickém	strašnický	k2eAgNnSc6d1	Strašnické
krematoriu	krematorium	k1gNnSc6	krematorium
natáčet	natáčet	k5eAaImF	natáčet
film	film	k1gInSc1	film
Kolja	Kolj	k1gInSc2	Kolj
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
napsal	napsat	k5eAaPmAgInS	napsat
scénář	scénář	k1gInSc1	scénář
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
režíroval	režírovat	k5eAaImAgMnS	režírovat
opět	opět	k6eAd1	opět
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
také	také	k9	také
zahrál	zahrát	k5eAaPmAgMnS	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1996	[number]	k4	1996
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
kině	kino	k1gNnSc6	kino
Blaník	Blaník	k1gInSc4	Blaník
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
získal	získat	k5eAaPmAgMnS	získat
šest	šest	k4xCc4	šest
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
dále	daleko	k6eAd2	daleko
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
1997	[number]	k4	1997
mělo	mít	k5eAaImAgNnS	mít
premiéru	premiér	k1gMnSc3	premiér
další	další	k2eAgNnSc1d1	další
drama	drama	k1gNnSc1	drama
sepsané	sepsaný	k2eAgNnSc1d1	sepsané
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
a	a	k8xC	a
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
Švestka	Švestka	k1gMnSc1	Švestka
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
sehrálo	sehrát	k5eAaPmAgNnS	sehrát
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
dvě	dva	k4xCgNnPc1	dva
představení	představení	k1gNnPc1	představení
hry	hra	k1gFnSc2	hra
Záskok	záskok	k1gInSc1	záskok
na	na	k7c6	na
prknech	prkno	k1gNnPc6	prkno
pražského	pražský	k2eAgNnSc2d1	Pražské
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výtěžek	výtěžek	k1gInSc1	výtěžek
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
na	na	k7c4	na
konto	konto	k1gNnSc4	konto
Centra	centrum	k1gNnSc2	centrum
Paraple	Paraple	k?	Paraple
a	a	k8xC	a
nadaci	nadace	k1gFnSc4	nadace
Respin	Respina	k1gFnPc2	Respina
pomáhající	pomáhající	k2eAgFnSc2d1	pomáhající
dětem	dítě	k1gFnPc3	dítě
při	při	k7c6	při
hipoterapii	hipoterapie	k1gFnSc6	hipoterapie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hudebního	hudební	k2eAgInSc2d1	hudební
festivalu	festival	k1gInSc2	festival
Pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1998	[number]	k4	1998
Svěrák	svěrák	k1gInSc1	svěrák
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
herci	herc	k1gInPc7	herc
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
s	s	k7c7	s
představením	představení	k1gNnSc7	představení
Cimrman	Cimrman	k1gMnSc1	Cimrman
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
hudby	hudba	k1gFnSc2	hudba
ve	v	k7c6	v
Dvořákově	Dvořákův	k2eAgFnSc6d1	Dvořákova
síni	síň	k1gFnSc6	síň
pražského	pražský	k2eAgNnSc2d1	Pražské
Rudolfina	Rudolfinum	k1gNnSc2	Rudolfinum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
vysílat	vysílat	k5eAaImF	vysílat
také	také	k9	také
diktát	diktát	k1gInSc4	diktát
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
diktuje	diktovat	k5eAaImIp3nS	diktovat
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
televizní	televizní	k2eAgMnPc1d1	televizní
diváci	divák	k1gMnPc1	divák
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
a	a	k8xC	a
následně	následně	k6eAd1	následně
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
gramatickou	gramatický	k2eAgFnSc4d1	gramatická
správnost	správnost	k1gFnSc4	správnost
zapsaného	zapsaný	k2eAgInSc2d1	zapsaný
textu	text	k1gInSc2	text
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
pořad	pořad	k1gInSc4	pořad
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
podobným	podobný	k2eAgInSc7d1	podobný
francouzským	francouzský	k2eAgInSc7d1	francouzský
projektem	projekt	k1gInSc7	projekt
<g/>
,	,	kIx,	,
upozornil	upozornit	k5eAaPmAgMnS	upozornit
české	český	k2eAgMnPc4d1	český
autory	autor	k1gMnPc4	autor
herec	herec	k1gMnSc1	herec
Jan	Jan	k1gMnSc1	Jan
Vala	Vala	k1gMnSc1	Vala
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
měl	mít	k5eAaImAgMnS	mít
premiéru	premiér	k1gMnSc3	premiér
film	film	k1gInSc4	film
Tmavomodrý	tmavomodrý	k2eAgInSc4d1	tmavomodrý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
psaním	psaní	k1gNnSc7	psaní
jeho	on	k3xPp3gInSc2	on
scénáře	scénář	k1gInSc2	scénář
začal	začít	k5eAaPmAgMnS	začít
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
již	již	k6eAd1	již
krátce	krátce	k6eAd1	krátce
po	po	k7c4	po
natočení	natočení	k1gNnSc4	natočení
filmu	film	k1gInSc2	film
Kolja	Kolj	k1gInSc2	Kolj
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1996	[number]	k4	1996
a	a	k8xC	a
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
natočen	natočit	k5eAaBmNgInS	natočit
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
snadněji	snadno	k6eAd2	snadno
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
zahraničních	zahraniční	k2eAgNnPc2d1	zahraniční
kin	kino	k1gNnPc2	kino
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nakonec	nakonec	k6eAd1	nakonec
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
představy	představa	k1gFnSc2	představa
jeho	jeho	k3xOp3gMnSc1	jeho
režisér	režisér	k1gMnSc1	režisér
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
upustil	upustit	k5eAaPmAgMnS	upustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
dají	dát	k5eAaPmIp3nP	dát
obtížně	obtížně	k6eAd1	obtížně
přeložit	přeložit	k5eAaPmF	přeložit
český	český	k2eAgInSc4d1	český
humor	humor	k1gInSc4	humor
a	a	k8xC	a
poetika	poetika	k1gFnSc1	poetika
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Světozoru	světozor	k1gInSc6	světozor
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spojena	spojen	k2eAgFnSc1d1	spojena
velká	velká	k1gFnSc1	velká
očekávání	očekávání	k1gNnSc2	očekávání
na	na	k7c4	na
zisk	zisk	k1gInSc4	zisk
Oscara	Oscar	k1gMnSc2	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
ovšem	ovšem	k9	ovšem
nezískal	získat	k5eNaPmAgMnS	získat
a	a	k8xC	a
nepotvrdil	potvrdit	k5eNaPmAgMnS	potvrdit
ani	ani	k8xC	ani
roli	role	k1gFnSc4	role
největšího	veliký	k2eAgMnSc2d3	veliký
favorita	favorit	k1gMnSc2	favorit
udílení	udílení	k1gNnSc2	udílení
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
na	na	k7c6	na
prknech	prkno	k1gNnPc6	prkno
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
konala	konat	k5eAaImAgFnS	konat
premiéra	premiéra	k1gFnSc1	premiéra
hry	hra	k1gFnSc2	hra
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
sepsal	sepsat	k5eAaPmAgMnS	sepsat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
o	o	k7c6	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
navázala	navázat	k5eAaPmAgFnS	navázat
poslední	poslední	k2eAgFnSc1d1	poslední
hra	hra	k1gFnSc1	hra
tohoto	tento	k3xDgInSc2	tento
souboru	soubor	k1gInSc2	soubor
České	český	k2eAgNnSc1d1	české
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
první	první	k4xOgNnSc4	první
představení	představení	k1gNnSc4	představení
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
již	již	k6eAd1	již
měl	mít	k5eAaImAgMnS	mít
Svěrák	Svěrák	k1gMnSc1	Svěrák
za	za	k7c7	za
sebou	se	k3xPyFc7	se
přípravu	příprava	k1gFnSc4	příprava
scénáře	scénář	k1gInPc1	scénář
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Vratné	vratný	k2eAgFnSc2d1	vratná
lahve	lahev	k1gFnSc2	lahev
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
musel	muset	k5eAaImAgMnS	muset
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
několikrát	několikrát	k6eAd1	několikrát
přepracovat	přepracovat	k5eAaPmF	přepracovat
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
scénáři	scénář	k1gInSc6	scénář
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
v	v	k7c6	v
dokumentárním	dokumentární	k2eAgInSc6d1	dokumentární
filmu	film	k1gInSc6	film
Tatínek	tatínek	k1gMnSc1	tatínek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
otci	otec	k1gMnSc3	otec
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
dokumentaristou	dokumentarista	k1gMnSc7	dokumentarista
Martinem	Martin	k1gMnSc7	Martin
Dostálem	Dostál	k1gMnSc7	Dostál
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
natočil	natočit	k5eAaBmAgMnS	natočit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
je	být	k5eAaImIp3nS	být
zdokumentován	zdokumentován	k2eAgInSc1d1	zdokumentován
život	život	k1gInSc1	život
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
školní	školní	k2eAgNnPc4d1	školní
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
i	i	k8xC	i
jejich	jejich	k3xOp3gInPc1	jejich
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
spory	spor	k1gInPc1	spor
až	až	k9	až
po	po	k7c6	po
psaní	psaní	k1gNnSc6	psaní
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Vratné	vratný	k2eAgFnSc2d1	vratná
lahve	lahev	k1gFnSc2	lahev
se	se	k3xPyFc4	se
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
točit	točit	k5eAaImF	točit
začal	začít	k5eAaPmAgMnS	začít
a	a	k8xC	a
premiéru	premiéra	k1gFnSc4	premiéra
měl	mít	k5eAaImAgMnS	mít
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Slovanském	slovanský	k2eAgInSc6d1	slovanský
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
pak	pak	k6eAd1	pak
premiéru	premiéra	k1gFnSc4	premiéra
další	další	k2eAgInSc1d1	další
film	film	k1gInSc1	film
Jana	Jan	k1gMnSc2	Jan
Svěráka	Svěrák	k1gMnSc2	Svěrák
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Kuky	kuk	k1gInPc4	kuk
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
o	o	k7c4	o
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
plyšového	plyšový	k2eAgMnSc2d1	plyšový
medvídka	medvídek	k1gMnSc2	medvídek
Kukyho	Kuky	k1gMnSc2	Kuky
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
navrátit	navrátit	k5eAaPmF	navrátit
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
původnímu	původní	k2eAgMnSc3d1	původní
majiteli	majitel	k1gMnSc3	majitel
<g/>
,	,	kIx,	,
chlapci	chlapec	k1gMnSc3	chlapec
Ondrovi	Ondra	k1gMnSc3	Ondra
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
ve	v	k7c6	v
filmu	film	k1gInSc6	film
namluvil	namluvit	k5eAaPmAgMnS	namluvit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
loutkových	loutkový	k2eAgFnPc2d1	loutková
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
rakovině	rakovina	k1gFnSc3	rakovina
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
Svěrákův	Svěrákův	k2eAgMnSc1d1	Svěrákův
přítel	přítel	k1gMnSc1	přítel
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
<g/>
,	,	kIx,	,
hru	hra	k1gFnSc4	hra
České	český	k2eAgNnSc1d1	české
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgInS	hrát
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
před	před	k7c7	před
úmrtím	úmrtí	k1gNnSc7	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
však	však	k9	však
svá	svůj	k3xOyFgNnPc4	svůj
představení	představení	k1gNnPc4	představení
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
během	během	k7c2	během
měsíce	měsíc	k1gInSc2	měsíc
snížil	snížit	k5eAaPmAgInS	snížit
na	na	k7c4	na
dvacet	dvacet	k4xCc4	dvacet
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
salonním	salonní	k2eAgInSc6d1	salonní
coupé	coupý	k2eAgInPc1d1	coupý
hrají	hrát	k5eAaImIp3nP	hrát
postavu	postava	k1gFnSc4	postava
policejního	policejní	k2eAgMnSc2d1	policejní
praktikanta	praktikant	k1gMnSc2	praktikant
Hlaváčka	Hlaváček	k1gMnSc2	Hlaváček
dva	dva	k4xCgMnPc1	dva
noví	nový	k2eAgMnPc1d1	nový
herci	herec	k1gMnPc1	herec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
otcové	otec	k1gMnPc1	otec
již	již	k6eAd1	již
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
syn	syn	k1gMnSc1	syn
Miloně	Miloň	k1gFnSc2	Miloň
Čepelky	čepelka	k1gFnSc2	čepelka
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jinak	jinak	k6eAd1	jinak
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
královéhradeckém	královéhradecký	k2eAgNnSc6d1	královéhradecké
Klicperově	Klicperův	k2eAgNnSc6d1	Klicperovo
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
Václava	Václav	k1gMnSc4	Václav
Kotka	Kotek	k1gMnSc4	Kotek
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Široký	Široký	k1gMnSc1	Široký
a	a	k8xC	a
Krátkozraký	krátkozraký	k2eAgMnSc1d1	krátkozraký
v	v	k7c6	v
roli	role	k1gFnSc6	role
Krátkozrakého	krátkozraký	k2eAgInSc2d1	krátkozraký
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Miroslav	Miroslav	k1gMnSc1	Miroslav
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
Svěrák	svěrák	k1gInSc1	svěrák
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
ambasadorů	ambasador	k1gMnPc2	ambasador
projektu	projekt	k1gInSc2	projekt
Čtení	čtení	k1gNnSc3	čtení
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
dětské	dětský	k2eAgFnSc2d1	dětská
četby	četba	k1gFnSc2	četba
a	a	k8xC	a
současně	současně	k6eAd1	současně
též	též	k9	též
charitativně	charitativně	k6eAd1	charitativně
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
potřebným	potřebný	k2eAgInSc7d1	potřebný
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
předsedá	předsedat	k5eAaImIp3nS	předsedat
odborné	odborný	k2eAgNnSc1d1	odborné
komisi	komise	k1gFnSc4	komise
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vyšly	vyjít	k5eAaPmAgFnP	vyjít
na	na	k7c4	na
čtyřcédéčku	čtyřcédéčka	k1gFnSc4	čtyřcédéčka
Svěrákovy	Svěrákův	k2eAgFnSc2d1	Svěrákova
pohádky	pohádka	k1gFnSc2	pohádka
Tři	tři	k4xCgNnPc1	tři
auta	auto	k1gNnPc1	auto
<g/>
,	,	kIx,	,
Krápník	krápník	k1gInSc1	krápník
a	a	k8xC	a
Františka	Františka	k1gFnSc1	Františka
a	a	k8xC	a
Kolo	kolo	k1gNnSc1	kolo
se	s	k7c7	s
zlatými	zlatý	k2eAgInPc7d1	zlatý
ráfky	ráfek	k1gInPc7	ráfek
společně	společně	k6eAd1	společně
s	s	k7c7	s
Radovanovými	Radovanův	k2eAgFnPc7d1	Radovanova
radovánkami	radovánka	k1gFnPc7	radovánka
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
slavilo	slavit	k5eAaImAgNnS	slavit
divadlo	divadlo	k1gNnSc1	divadlo
45	[number]	k4	45
let	léto	k1gNnPc2	léto
od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
své	svůj	k3xOyFgFnSc2	svůj
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
-	-	kIx~	-
přesně	přesně	k6eAd1	přesně
v	v	k7c4	v
den	den	k1gInSc4	den
výročí	výročí	k1gNnSc2	výročí
-	-	kIx~	-
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
představení	představení	k1gNnSc1	představení
hry	hra	k1gFnSc2	hra
České	český	k2eAgNnSc1d1	české
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
přímým	přímý	k2eAgInSc7d1	přímý
přenosem	přenos	k1gInSc7	přenos
přenášeno	přenášet	k5eAaImNgNnS	přenášet
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sedmdesáti	sedmdesát	k4xCc2	sedmdesát
kin	kino	k1gNnPc2	kino
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hudebního	hudební	k2eAgInSc2d1	hudební
festivalu	festival	k1gInSc2	festival
Prague	Pragu	k1gFnSc2	Pragu
Proms	Promsa	k1gFnPc2	Promsa
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
členů	člen	k1gMnPc2	člen
Českého	český	k2eAgInSc2d1	český
národního	národní	k2eAgInSc2d1	národní
symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
konala	konat	k5eAaImAgFnS	konat
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgFnSc6d1	Smetanova
síni	síň	k1gFnSc6	síň
pražského	pražský	k2eAgInSc2d1	pražský
Obecního	obecní	k2eAgInSc2d1	obecní
domu	dům	k1gInSc2	dům
premiéra	premiér	k1gMnSc2	premiér
a	a	k8xC	a
současně	současně	k6eAd1	současně
též	též	k9	též
derniéra	derniéra	k1gFnSc1	derniéra
představení	představení	k1gNnSc2	představení
Proso	proso	k1gNnSc4	proso
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
divákům	divák	k1gMnPc3	divák
představilo	představit	k5eAaPmAgNnS	představit
fragmenty	fragment	k1gInPc4	fragment
ze	z	k7c2	z
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
Cimrmanovy	Cimrmanův	k2eAgFnSc2d1	Cimrmanova
operety	opereta	k1gFnSc2	opereta
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
-	-	kIx~	-
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
semináři	seminář	k1gInSc6	seminář
představení	představení	k1gNnSc2	představení
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
Hospoda	Hospoda	k?	Hospoda
Na	na	k7c6	na
mýtince	mýtinka	k1gFnSc6	mýtinka
-	-	kIx~	-
některé	některý	k3yIgInPc4	některý
hudební	hudební	k2eAgInPc4d1	hudební
motivy	motiv	k1gInPc4	motiv
rozebrali	rozebrat	k5eAaPmAgMnP	rozebrat
skladatelé	skladatel	k1gMnPc1	skladatel
Johann	Johanno	k1gNnPc2	Johanno
Strauss	Straussa	k1gFnPc2	Straussa
či	či	k8xC	či
Franz	Franz	k1gMnSc1	Franz
Lehár	Lehár	k1gMnSc1	Lehár
<g/>
,	,	kIx,	,
a	a	k8xC	a
využili	využít	k5eAaPmAgMnP	využít
je	on	k3xPp3gNnSc4	on
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
skladbách	skladba	k1gFnPc6	skladba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	on	k3xPp3gNnSc4	on
proslavily	proslavit	k5eAaPmAgFnP	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnPc1	představení
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgFnSc6d1	Smetanova
síni	síň	k1gFnSc6	síň
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Libor	Libor	k1gMnSc1	Libor
Pešek	Pešek	k1gMnSc1	Pešek
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gNnSc7	jehož
vedením	vedení	k1gNnSc7	vedení
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
zahráli	zahrát	k5eAaPmAgMnP	zahrát
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
souboru	soubor	k1gInSc2	soubor
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
například	například	k6eAd1	například
na	na	k7c4	na
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
pod	pod	k7c7	pod
režijním	režijní	k2eAgNnSc7d1	režijní
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Svěráka	Svěrák	k1gMnSc2	Svěrák
vznikal	vznikat	k5eAaImAgInS	vznikat
záznam	záznam	k1gInSc1	záznam
představení	představení	k1gNnPc2	představení
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
mohli	moct	k5eAaImAgMnP	moct
sledovat	sledovat	k5eAaImF	sledovat
diváci	divák	k1gMnPc1	divák
v	v	k7c6	v
54	[number]	k4	54
kinech	kino	k1gNnPc6	kino
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Výtěžek	výtěžek	k1gInSc1	výtěžek
představení	představení	k1gNnSc3	představení
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
Centru	centr	k1gMnSc3	centr
Paraple	Paraple	k?	Paraple
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
konaly	konat	k5eAaImAgFnP	konat
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgFnPc2	jenž
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
svého	svůj	k3xOyFgMnSc4	svůj
prezidenta	prezident	k1gMnSc4	prezident
vybírali	vybírat	k5eAaImAgMnP	vybírat
přímo	přímo	k6eAd1	přímo
občané	občan	k1gMnPc1	občan
a	a	k8xC	a
nevolili	volit	k5eNaImAgMnP	volit
ho	on	k3xPp3gInSc4	on
členové	člen	k1gMnPc1	člen
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
společné	společný	k2eAgFnSc6d1	společná
schůzi	schůze	k1gFnSc6	schůze
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
volbou	volba	k1gFnSc7	volba
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
podporu	podpora	k1gFnSc4	podpora
Karlu	Karel	k1gMnSc3	Karel
Schwarzenbergovi	Schwarzenberg	k1gMnSc3	Schwarzenberg
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
Janem	Jan	k1gMnSc7	Jan
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
kampaň	kampaň	k1gFnSc4	kampaň
natočil	natočit	k5eAaBmAgMnS	natočit
i	i	k9	i
tři	tři	k4xCgNnPc4	tři
videa	video	k1gNnPc4	video
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
Schwarzenbergem	Schwarzenberg	k1gInSc7	Schwarzenberg
vede	vést	k5eAaImIp3nS	vést
rozhovor	rozhovor	k1gInSc1	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
společně	společně	k6eAd1	společně
s	s	k7c7	s
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tam	tam	k6eAd1	tam
svému	svůj	k3xOyFgMnSc3	svůj
rivalovi	rival	k1gMnSc3	rival
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Svěrák	Svěrák	k1gMnSc1	Svěrák
o	o	k7c6	o
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
nikdy	nikdy	k6eAd1	nikdy
neuvažoval	uvažovat	k5eNaImAgMnS	uvažovat
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
se	se	k3xPyFc4	se
ve	v	k7c6	v
veřejných	veřejný	k2eAgFnPc6d1	veřejná
anketách	anketa	k1gFnPc6	anketa
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
také	také	k9	také
objevovalo	objevovat	k5eAaImAgNnS	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
píše	psát	k5eAaImIp3nS	psát
Svěrák	Svěrák	k1gMnSc1	Svěrák
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
jich	on	k3xPp3gMnPc2	on
vybral	vybrat	k5eAaPmAgMnS	vybrat
deset	deset	k4xCc4	deset
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vydal	vydat	k5eAaPmAgInS	vydat
knižně	knižně	k6eAd1	knižně
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Povídky	povídka	k1gFnSc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
šest	šest	k4xCc1	šest
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
povídek	povídka	k1gFnPc2	povídka
namluvil	namluvit	k5eAaBmAgInS	namluvit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
herečkami	herečka	k1gFnPc7	herečka
Danielou	Daniela	k1gFnSc7	Daniela
Kolářovou	Kolářová	k1gFnSc7	Kolářová
a	a	k8xC	a
Libuší	Libuše	k1gFnSc7	Libuše
Šafránkovou	Šafránková	k1gFnSc7	Šafránková
na	na	k7c6	na
CD	CD	kA	CD
nazvané	nazvaný	k2eAgFnPc4d1	nazvaná
Povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc4d1	zbylý
čtyři	čtyři	k4xCgInPc4	čtyři
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c6	na
CD	CD	kA	CD
pojmenovaném	pojmenovaný	k2eAgNnSc6d1	pojmenované
Horká	Horká	k1gFnSc1	Horká
neděle	neděle	k1gFnSc2	neděle
aneb	aneb	k?	aneb
Vynechané	vynechaný	k2eAgFnSc2d1	vynechaná
povídky	povídka	k1gFnSc2	povídka
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
vyšla	vyjít	k5eAaPmAgFnS	vyjít
další	další	k2eAgFnSc1d1	další
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Nové	Nová	k1gFnSc2	Nová
povídky	povídka	k1gFnSc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Svěrák	Svěrák	k1gMnSc1	Svěrák
navázal	navázat	k5eAaPmAgMnS	navázat
sbírkou	sbírka	k1gFnSc7	sbírka
povídek	povídka	k1gFnPc2	povídka
inspirovaných	inspirovaný	k2eAgFnPc2d1	inspirovaná
svými	svůj	k3xOyFgInPc7	svůj
zážitky	zážitek	k1gInPc7	zážitek
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k1gMnSc1	bos
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
nich	on	k3xPp3gInPc6	on
původně	původně	k6eAd1	původně
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
filmový	filmový	k2eAgInSc4d1	filmový
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vyšla	vyjít	k5eAaPmAgFnS	vyjít
Svěrákovi	Svěrák	k1gMnSc3	Svěrák
další	další	k2eAgFnSc1d1	další
kniha	kniha	k1gFnSc1	kniha
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Filmové	filmový	k2eAgInPc1d1	filmový
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc1	čtyři
povídky	povídka	k1gFnPc1	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
scénáře	scénář	k1gInPc4	scénář
k	k	k7c3	k
již	již	k6eAd1	již
natočeným	natočený	k2eAgInPc3d1	natočený
filmům	film	k1gInPc3	film
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Kolja	Kolja	k1gFnSc1	Kolja
a	a	k8xC	a
Vratné	vratný	k2eAgFnPc1d1	vratná
lahve	lahev	k1gFnPc1	lahev
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
poslední	poslední	k2eAgMnSc1d1	poslední
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
snímek	snímek	k1gInSc4	snímek
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k1gMnSc1	bos
teprve	teprve	k6eAd1	teprve
natočit	natočit	k5eAaBmF	natočit
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
písní	píseň	k1gFnPc2	píseň
sestavil	sestavit	k5eAaPmAgInS	sestavit
několik	několik	k4yIc4	několik
knížek	knížka	k1gFnPc2	knížka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gNnPc7	on
například	například	k6eAd1	například
Jaké	jaký	k3yIgNnSc1	jaký
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
v	v	k7c6	v
Čudu	čud	k1gInSc6	čud
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Když	když	k8xS	když
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
kůň	kůň	k1gMnSc1	kůň
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mám	mít	k5eAaImIp1nS	mít
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
myš	myš	k1gFnSc4	myš
Lenku	Lenka	k1gFnSc4	Lenka
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
pěkné	pěkný	k2eAgNnSc1d1	pěkné
počasí	počasí	k1gNnSc1	počasí
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
píše	psát	k5eAaImIp3nS	psát
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
minioperky	minioperk	k1gInPc4	minioperk
vycházející	vycházející	k2eAgInPc4d1	vycházející
z	z	k7c2	z
pohádkových	pohádkový	k2eAgInPc2d1	pohádkový
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
tak	tak	k9	tak
minioperky	minioperk	k1gInPc1	minioperk
O	o	k7c6	o
Budulínkovi	Budulínek	k1gMnSc6	Budulínek
<g/>
,	,	kIx,	,
Červená	červený	k2eAgFnSc1d1	červená
karkulka	karkulka	k1gFnSc1	karkulka
<g/>
,	,	kIx,	,
O	o	k7c6	o
12	[number]	k4	12
měsíčkách	měsíček	k1gInPc6	měsíček
či	či	k8xC	či
Šípková	šípkový	k2eAgFnSc1d1	šípková
Růženka	Růženka	k1gFnSc1	Růženka
<g/>
.	.	kIx.	.
</s>
<s>
Spojením	spojení	k1gNnSc7	spojení
tří	tři	k4xCgFnPc2	tři
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
(	(	kIx(	(
<g/>
Šípkové	Šípková	k1gFnSc2	Šípková
Růženky	Růženka	k1gFnSc2	Růženka
<g/>
,	,	kIx,	,
Červené	Červené	k2eAgFnPc1d1	Červené
karkulky	karkulka	k1gFnPc1	karkulka
a	a	k8xC	a
O	o	k7c6	o
12	[number]	k4	12
měsíčkách	měsíček	k1gInPc6	měsíček
<g/>
)	)	kIx)	)
připravil	připravit	k5eAaPmAgMnS	připravit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
filmový	filmový	k2eAgInSc1d1	filmový
scénář	scénář	k1gInSc1	scénář
pro	pro	k7c4	pro
pohádku	pohádka	k1gFnSc4	pohádka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
jeho	jeho	k3xOp3gMnSc7	jeho
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
premiéru	premiéra	k1gFnSc4	premiéra
měla	mít	k5eAaImAgFnS	mít
o	o	k7c6	o
letních	letní	k2eAgFnPc6d1	letní
prázdninách	prázdniny	k1gFnPc6	prázdniny
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Klus	klus	k1gInSc1	klus
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Piškula	Piškula	k1gFnSc1	Piškula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
netanečník	netanečník	k1gMnSc1	netanečník
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
pátým	pátý	k4xOgMnSc7	pátý
porotcem	porotce	k1gMnSc7	porotce
v	v	k7c6	v
sedmém	sedmý	k4xOgInSc6	sedmý
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgFnSc4d1	speciální
dílu	dílo	k1gNnSc3	dílo
taneční	taneční	k2eAgFnSc2d1	taneční
soutěže	soutěž	k1gFnSc2	soutěž
Staance	Staance	k1gFnSc2	Staance
...	...	k?	...
<g/>
když	když	k8xS	když
hvězdy	hvězda	k1gFnPc1	hvězda
tančí	tančit	k5eAaImIp3nP	tančit
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
sedmou	sedmý	k4xOgFnSc4	sedmý
řadu	řada	k1gFnSc4	řada
během	během	k7c2	během
podzimu	podzim	k1gInSc2	podzim
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
vysílala	vysílat	k5eAaImAgFnS	vysílat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vysílání	vysílání	k1gNnSc2	vysílání
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
dílu	díl	k1gInSc2	díl
mohli	moct	k5eAaImAgMnP	moct
televizní	televizní	k2eAgMnPc1d1	televizní
diváci	divák	k1gMnPc1	divák
zasílat	zasílat	k5eAaImF	zasílat
své	svůj	k3xOyFgInPc4	svůj
finanční	finanční	k2eAgInPc4d1	finanční
příspěvky	příspěvek	k1gInPc4	příspěvek
na	na	k7c4	na
Konto	konto	k1gNnSc4	konto
Paraple	Paraple	k?	Paraple
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
DMS	DMS	kA	DMS
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c4	v
den	den	k1gInSc4	den
Svěrákových	Svěráková	k1gFnPc2	Svěráková
80	[number]	k4	80
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
uvedla	uvést	k5eAaPmAgFnS	uvést
vybraná	vybraná	k1gFnSc1	vybraná
kina	kino	k1gNnSc2	kino
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
vyčištěnou	vyčištěný	k2eAgFnSc4d1	vyčištěná
(	(	kIx(	(
<g/>
remasterovanou	remasterovaný	k2eAgFnSc4d1	remasterovaná
<g/>
)	)	kIx)	)
verzi	verze	k1gFnSc4	verze
snímku	snímek	k1gInSc2	snímek
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
začne	začít	k5eAaPmIp3nS	začít
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
během	během	k7c2	během
srpna	srpen	k1gInSc2	srpen
natáčet	natáčet	k5eAaImF	natáčet
i	i	k9	i
další	další	k2eAgInSc4d1	další
film	film	k1gInSc4	film
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k1gMnSc1	bos
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
premiéra	premiéra	k1gFnSc1	premiéra
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
konat	konat	k5eAaImF	konat
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Boženou	Božena	k1gFnSc7	Božena
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
dcera	dcera	k1gFnSc1	dcera
Hanka	Hanka	k1gFnSc1	Hanka
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Jelínková	Jelínková	k1gFnSc1	Jelínková
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
dva	dva	k4xCgMnPc1	dva
dospělé	dospělí	k1gMnPc4	dospělí
syny	syn	k1gMnPc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
bratr	bratr	k1gMnSc1	bratr
Jan	Jan	k1gMnSc1	Jan
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Evou	Eva	k1gFnSc7	Eva
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Dceru	dcera	k1gFnSc4	dcera
Kateřinu	Kateřina	k1gFnSc4	Kateřina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
partnera	partner	k1gMnSc4	partner
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
bratry	bratr	k1gMnPc7	bratr
jsou	být	k5eAaImIp3nP	být
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
studuje	studovat	k5eAaImIp3nS	studovat
na	na	k7c6	na
filmové	filmový	k2eAgFnSc6d1	filmová
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
fotografii	fotografia	k1gFnSc4	fotografia
a	a	k8xC	a
střih	střih	k1gInSc4	střih
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
vnoučat	vnouče	k1gNnPc2	vnouče
a	a	k8xC	a
jedno	jeden	k4xCgNnSc4	jeden
pravnouče	pravnouče	k1gNnSc4	pravnouče
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgInSc1d1	obchodní
řetězec	řetězec	k1gInSc1	řetězec
Bauhaus	Bauhaus	k1gInSc1	Bauhaus
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
reklamě	reklama	k1gFnSc3	reklama
uveřejněné	uveřejněný	k2eAgFnSc3d1	uveřejněná
během	během	k7c2	během
září	září	k1gNnSc2	září
2004	[number]	k4	2004
na	na	k7c4	na
nejméně	málo	k6eAd3	málo
211	[number]	k4	211
billboardech	billboard	k1gInPc6	billboard
uvedl	uvést	k5eAaPmAgMnS	uvést
slovní	slovní	k2eAgNnSc4d1	slovní
spojení	spojení	k1gNnSc4	spojení
"	"	kIx"	"
<g/>
upeč	upéct	k5eAaPmRp2nS	upéct
<g/>
...	...	k?	...
třeba	třeba	k6eAd1	třeba
zeď	zeď	k1gFnSc1	zeď
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Svěráka	Svěrák	k1gMnSc2	Svěrák
je	být	k5eAaImIp3nS	být
text	text	k1gInSc4	text
inspirován	inspirován	k2eAgMnSc1d1	inspirován
jeho	jeho	k3xOp3gFnSc7	jeho
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Dělání	dělání	k1gNnSc4	dělání
<g/>
"	"	kIx"	"
uveřejněné	uveřejněný	k2eAgFnSc2d1	uveřejněná
prvně	prvně	k?	prvně
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Princové	princ	k1gMnPc1	princ
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
draka	drak	k1gMnSc4	drak
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
verš	verš	k1gInSc1	verš
"	"	kIx"	"
<g/>
upeč	upéct	k5eAaPmRp2nS	upéct
třeba	třeba	k6eAd1	třeba
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
postav	postava	k1gFnPc2	postava
třeba	třeba	k8wRxS	třeba
zeď	zeď	k1gFnSc4	zeď
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podal	podat	k5eAaPmAgMnS	podat
proto	proto	k8xC	proto
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
svých	svůj	k3xOyFgNnPc2	svůj
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
brněnský	brněnský	k2eAgInSc4d1	brněnský
krajský	krajský	k2eAgInSc4d1	krajský
soud	soud	k1gInSc4	soud
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2005	[number]	k4	2005
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgInSc1d1	obchodní
řetězec	řetězec	k1gInSc1	řetězec
se	se	k3xPyFc4	se
však	však	k9	však
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
odvolal	odvolat	k5eAaPmAgInS	odvolat
s	s	k7c7	s
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
danou	daný	k2eAgFnSc4d1	daná
píseň	píseň	k1gFnSc4	píseň
vůbec	vůbec	k9	vůbec
nezná	znát	k5eNaImIp3nS	znát
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
sporu	spor	k1gInSc6	spor
v	v	k7c6	v
září	září	k1gNnSc6	září
2006	[number]	k4	2006
proto	proto	k8xC	proto
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
opět	opět	k6eAd1	opět
dal	dát	k5eAaPmAgMnS	dát
Svěrákovi	Svěrák	k1gMnSc3	Svěrák
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
má	mít	k5eAaImIp3nS	mít
Bauhaus	Bauhaus	k1gInSc1	Bauhaus
vyplatit	vyplatit	k5eAaPmF	vyplatit
odškodné	odškodné	k1gNnSc4	odškodné
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
českých	český	k2eAgInPc2d1	český
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
uveřejnit	uveřejnit	k5eAaPmF	uveřejnit
omluvu	omluva	k1gFnSc4	omluva
v	v	k7c6	v
celostátně	celostátně	k6eAd1	celostátně
vydávaném	vydávaný	k2eAgInSc6d1	vydávaný
deníku	deník	k1gInSc6	deník
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
se	se	k3xPyFc4	se
Bauhaus	Bauhaus	k1gInSc1	Bauhaus
dovolal	dovolat	k5eAaPmAgInS	dovolat
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
oba	dva	k4xCgInPc4	dva
předchozí	předchozí	k2eAgInPc4d1	předchozí
rozsudky	rozsudek	k1gInPc4	rozsudek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2010	[number]	k4	2010
tak	tak	k6eAd1	tak
o	o	k7c6	o
sporu	spor	k1gInSc6	spor
znovu	znovu	k6eAd1	znovu
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
Krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
opět	opět	k6eAd1	opět
dal	dát	k5eAaPmAgMnS	dát
Svěrákovi	Svěrák	k1gMnSc3	Svěrák
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
nařídil	nařídit	k5eAaPmAgMnS	nařídit
obchodnímu	obchodní	k2eAgInSc3d1	obchodní
řetězci	řetězec	k1gInSc3	řetězec
uhradit	uhradit	k5eAaPmF	uhradit
odškodnění	odškodnění	k1gNnSc4	odškodnění
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
celostátních	celostátní	k2eAgInPc2d1	celostátní
deníků	deník	k1gInPc2	deník
uvést	uvést	k5eAaPmF	uvést
omluvu	omluva	k1gFnSc4	omluva
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
se	se	k3xPyFc4	se
Bauhaus	Bauhaus	k1gInSc1	Bauhaus
odvolal	odvolat	k5eAaPmAgInS	odvolat
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	září	k1gNnSc2	září
2010	[number]	k4	2010
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
spor	spor	k1gInSc4	spor
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
na	na	k7c4	na
Svěrákovu	Svěrákův	k2eAgFnSc4d1	Svěrákova
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
zaplacení	zaplacení	k1gNnSc4	zaplacení
požadovaného	požadovaný	k2eAgNnSc2d1	požadované
dvousettisícového	dvousettisícový	k2eAgNnSc2d1	dvousettisícové
odškodného	odškodné	k1gNnSc2	odškodné
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
uveřejněním	uveřejnění	k1gNnSc7	uveřejnění
omluvy	omluva	k1gFnSc2	omluva
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
jedné	jeden	k4xCgFnSc2	jeden
osminy	osmina	k1gFnSc2	osmina
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
celostátních	celostátní	k2eAgInPc2d1	celostátní
deníků	deník	k1gInPc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
soudu	soud	k1gInSc2	soud
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
obchodní	obchodní	k2eAgInSc1d1	obchodní
řetězec	řetězec	k1gInSc1	řetězec
opět	opět	k6eAd1	opět
dovolával	dovolávat	k5eAaImAgInS	dovolávat
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
leč	leč	k8xS	leč
ten	ten	k3xDgMnSc1	ten
toto	tento	k3xDgNnSc4	tento
dovolání	dovolání	k1gNnSc4	dovolání
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
ponechal	ponechat	k5eAaPmAgMnS	ponechat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
soudu	soud	k1gInSc2	soud
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Vysouzený	vysouzený	k2eAgInSc4d1	vysouzený
finanční	finanční	k2eAgInSc4d1	finanční
obnos	obnos	k1gInSc4	obnos
plánoval	plánovat	k5eAaImAgMnS	plánovat
Svěrák	Svěrák	k1gMnSc1	Svěrák
věnovat	věnovat	k5eAaImF	věnovat
na	na	k7c4	na
konto	konto	k1gNnSc4	konto
paraplegiků	paraplegik	k1gMnPc2	paraplegik
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Metelesku	Metelesk	k1gInSc2	Metelesk
blesku	blesk	k1gInSc2	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
mimořádnými	mimořádný	k2eAgFnPc7d1	mimořádná
volbami	volba	k1gFnPc7	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
konanými	konaný	k2eAgFnPc7d1	konaná
roku	rok	k1gInSc3	rok
2013	[number]	k4	2013
začalo	začít	k5eAaPmAgNnS	začít
uskupení	uskupení	k1gNnSc1	uskupení
Hlavu	hlava	k1gFnSc4	hlava
vzhůru	vzhůru	k6eAd1	vzhůru
vedené	vedený	k2eAgFnSc2d1	vedená
Janou	Jana	k1gFnSc7	Jana
Bobošíkovou	Bobošíkový	k2eAgFnSc4d1	Bobošíková
používat	používat	k5eAaImF	používat
jako	jako	k8xS	jako
svoji	svůj	k3xOyFgFnSc4	svůj
hymnu	hymna	k1gFnSc4	hymna
skladbu	skladba	k1gFnSc4	skladba
složenou	složený	k2eAgFnSc4d1	složená
Tomášem	Tomáš	k1gMnSc7	Tomáš
Úlehlou	Úlehlý	k2eAgFnSc7d1	Úlehlý
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
rým	rým	k1gInSc4	rým
"	"	kIx"	"
<g/>
Metelesku	Meteleska	k1gFnSc4	Meteleska
blesku	blesk	k1gInSc2	blesk
<g/>
,	,	kIx,	,
pojďme	jít	k5eAaImRp1nP	jít
pomoct	pomoct	k5eAaPmF	pomoct
Česku	Česko	k1gNnSc6	Česko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
Metelesku	Metelesk	k1gInSc2	Metelesk
blesku	blesk	k1gInSc2	blesk
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Marečku	Mareček	k1gMnSc5	Mareček
<g/>
,	,	kIx,	,
podejte	podat	k5eAaPmRp2nP	podat
mi	já	k3xPp1nSc3	já
pero	pero	k1gNnSc1	pero
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
Svěrák	Svěrák	k1gMnSc1	Svěrák
se	s	k7c7	s
Smoljakem	Smoljak	k1gInSc7	Smoljak
napsali	napsat	k5eAaPmAgMnP	napsat
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Svěrák	Svěrák	k1gMnSc1	Svěrák
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgNnSc3	tento
použití	použití	k1gNnSc3	použití
sloganu	slogan	k1gInSc2	slogan
obrátí	obrátit	k5eAaPmIp3nS	obrátit
na	na	k7c4	na
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
písně	píseň	k1gFnSc2	píseň
Tomáš	Tomáš	k1gMnSc1	Tomáš
Úlehla	Úlehla	k1gMnSc1	Úlehla
však	však	k9	však
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
Metelesku	Metelesk	k1gInSc2	Metelesk
blesku	blesk	k1gInSc2	blesk
<g/>
"	"	kIx"	"
používá	používat	k5eAaImIp3nS	používat
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
jako	jako	k8xC	jako
zaklínadlo	zaklínadlo	k1gNnSc4	zaklínadlo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
však	však	k9	však
Svěrákovi	Svěrák	k1gMnSc3	Svěrák
připadalo	připadat	k5eAaPmAgNnS	připadat
směšné	směšný	k2eAgNnSc1d1	směšné
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
začala	začít	k5eAaPmAgFnS	začít
Bobošíková	Bobošíková	k1gFnSc1	Bobošíková
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
výraz	výraz	k1gInSc1	výraz
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
estonštiny	estonština	k1gFnSc2	estonština
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
ověřoval	ověřovat	k5eAaImAgMnS	ověřovat
její	její	k3xOp3gNnPc4	její
tvrzení	tvrzení	k1gNnPc4	tvrzení
v	v	k7c6	v
estonsko-českém	estonsko-český	k2eAgInSc6d1	estonsko-český
slovníku	slovník	k1gInSc6	slovník
<g/>
,	,	kIx,	,
leč	leč	k8xS	leč
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
,	,	kIx,	,
a	a	k8xC	a
spojení	spojení	k1gNnSc1	spojení
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rodné	rodný	k2eAgFnSc6d1	rodná
řeči	řeč	k1gFnSc6	řeč
neznal	znát	k5eNaImAgMnS	znát
ani	ani	k8xC	ani
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
estonský	estonský	k2eAgMnSc1d1	estonský
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Lembit	Lembit	k1gMnSc1	Lembit
Uibo	Uibo	k1gMnSc1	Uibo
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgNnSc1d1	politické
uskupení	uskupení	k1gNnSc1	uskupení
nakonec	nakonec	k6eAd1	nakonec
přestalo	přestat	k5eAaPmAgNnS	přestat
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výraz	výraz	k1gInSc1	výraz
z	z	k7c2	z
moravštiny	moravština	k1gFnSc2	moravština
nebo	nebo	k8xC	nebo
z	z	k7c2	z
estonštiny	estonština	k1gFnSc2	estonština
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
omluvu	omluva	k1gFnSc4	omluva
poslalo	poslat	k5eAaPmAgNnS	poslat
ještě	ještě	k9	ještě
před	před	k7c7	před
podáním	podání	k1gNnSc7	podání
Svěrákovy	Svěrákův	k2eAgFnSc2d1	Svěrákova
žaloby	žaloba	k1gFnSc2	žaloba
na	na	k7c4	na
účet	účet	k1gInSc4	účet
Centra	centr	k1gMnSc2	centr
Paraple	Paraple	k?	Paraple
částku	částka	k1gFnSc4	částka
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
českých	český	k2eAgInPc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Svěrák	Svěrák	k1gMnSc1	Svěrák
následně	následně	k6eAd1	následně
žalobu	žaloba	k1gFnSc4	žaloba
nepodal	podat	k5eNaPmAgMnS	podat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jak	jak	k6eAd1	jak
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
jeho	jeho	k3xOp3gMnSc1	jeho
právní	právní	k2eAgMnSc1d1	právní
zástupce	zástupce	k1gMnSc1	zástupce
František	František	k1gMnSc1	František
Vyskočil	Vyskočil	k1gMnSc1	Vyskočil
<g/>
,	,	kIx,	,
hrozil	hrozit	k5eAaImAgInS	hrozit
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
rozpad	rozpad	k1gInSc1	rozpad
uskupení	uskupení	k1gNnSc6	uskupení
Hlavu	hlava	k1gFnSc4	hlava
vzhůru	vzhůru	k6eAd1	vzhůru
a	a	k8xC	a
výsledek	výsledek	k1gInSc1	výsledek
soudního	soudní	k2eAgNnSc2d1	soudní
řízení	řízení	k1gNnSc2	řízení
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
Vyskočila	Vyskočil	k1gMnSc4	Vyskočil
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
na	na	k7c6	na
sociální	sociální	k2eAgFnSc6d1	sociální
síti	síť	k1gFnSc6	síť
Facebook	Facebook	k1gInSc4	Facebook
objevila	objevit	k5eAaPmAgFnS	objevit
skupina	skupina	k1gFnSc1	skupina
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
"	"	kIx"	"
<g/>
Znásilnil	znásilnit	k5eAaPmAgInS	znásilnit
mě	já	k3xPp1nSc4	já
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
členové	člen	k1gMnPc1	člen
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Svěrák	svěrák	k1gInSc4	svěrák
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
chodili	chodit	k5eAaImAgMnP	chodit
do	do	k7c2	do
dětského	dětský	k2eAgInSc2d1	dětský
sboru	sbor	k1gInSc2	sbor
Sedmihlásek	sedmihlásek	k1gMnSc1	sedmihlásek
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
sexuálně	sexuálně	k6eAd1	sexuálně
zneužíval	zneužívat	k5eAaImAgMnS	zneužívat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
facebookové	facebookový	k2eAgFnSc6d1	facebooková
skupině	skupina	k1gFnSc6	skupina
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
také	také	k9	také
vyjádření	vyjádření	k1gNnSc1	vyjádření
sexuologa	sexuolog	k1gMnSc2	sexuolog
Radima	Radim	k1gMnSc2	Radim
Uzla	Uzlus	k1gMnSc2	Uzlus
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
zjištění	zjištění	k1gNnSc2	zjištění
časopisu	časopis	k1gInSc2	časopis
Týden	týden	k1gInSc1	týden
podvrhem	podvrh	k1gInSc7	podvrh
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nařčení	nařčení	k1gNnSc3	nařčení
podal	podat	k5eAaPmAgMnS	podat
Svěrák	Svěrák	k1gMnSc1	Svěrák
trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
na	na	k7c4	na
neznámého	známý	k2eNgMnSc4d1	neznámý
pachatele	pachatel	k1gMnSc4	pachatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
se	se	k3xPyFc4	se
však	však	k9	však
objevil	objevit	k5eAaPmAgInS	objevit
také	také	k9	také
příspěvek	příspěvek	k1gInSc1	příspěvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
založení	založení	k1gNnSc6	založení
skupiny	skupina	k1gFnSc2	skupina
přisuzoval	přisuzovat	k5eAaImAgInS	přisuzovat
novináři	novinář	k1gMnSc3	novinář
Jiřímu	Jiří	k1gMnSc3	Jiří
X.	X.	kA	X.
Doležalovi	Doležal	k1gMnSc3	Doležal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
tohoto	tento	k3xDgInSc2	tento
příspěvku	příspěvek	k1gInSc2	příspěvek
stálo	stát	k5eAaImAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Svěrák	Svěrák	k1gMnSc1	Svěrák
nikoho	nikdo	k3yNnSc4	nikdo
neznásilnil	znásilnit	k5eNaPmAgMnS	znásilnit
a	a	k8xC	a
že	že	k8xS	že
facebooková	facebookový	k2eAgFnSc1d1	facebooková
skupina	skupina	k1gFnSc1	skupina
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
sociálního	sociální	k2eAgInSc2d1	sociální
experimentu	experiment	k1gInSc2	experiment
sledujícího	sledující	k2eAgNnSc2d1	sledující
šíření	šíření	k1gNnSc2	šíření
bulvárních	bulvární	k2eAgFnPc2d1	bulvární
informací	informace	k1gFnPc2	informace
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
této	tento	k3xDgFnSc2	tento
sociální	sociální	k2eAgFnSc2d1	sociální
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Doležal	Doležal	k1gMnSc1	Doležal
však	však	k9	však
autorem	autor	k1gMnSc7	autor
tohoto	tento	k3xDgInSc2	tento
výzkumu	výzkum	k1gInSc2	výzkum
nebyl	být	k5eNaImAgMnS	být
a	a	k8xC	a
pachatel	pachatel	k1gMnSc1	pachatel
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
literární	literární	k2eAgFnSc4d1	literární
tvorbu	tvorba	k1gFnSc4	tvorba
získal	získat	k5eAaPmAgMnS	získat
Svěrák	Svěrák	k1gMnSc1	Svěrák
několik	několik	k4yIc4	několik
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
trojnásobným	trojnásobný	k2eAgMnSc7d1	trojnásobný
nositelem	nositel	k1gMnSc7	nositel
ceny	cena	k1gFnSc2	cena
čtenářů	čtenář	k1gMnPc2	čtenář
Magnesia	magnesium	k1gNnSc2	magnesium
Litera	litera	k1gFnSc1	litera
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
získal	získat	k5eAaPmAgMnS	získat
hned	hned	k6eAd1	hned
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
premiérovém	premiérový	k2eAgNnSc6d1	premiérové
udílení	udílení	k1gNnSc6	udílení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
za	za	k7c4	za
publikaci	publikace	k1gFnSc4	publikace
Jaké	jaký	k3yRgNnSc1	jaký
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
v	v	k7c6	v
Čudu	čud	k1gInSc6	čud
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
za	za	k7c4	za
knihu	kniha	k1gFnSc4	kniha
Nové	Nové	k2eAgFnSc2d1	Nové
povídky	povídka	k1gFnSc2	povídka
a	a	k8xC	a
poslední	poslední	k2eAgFnSc2d1	poslední
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
za	za	k7c4	za
vzpomínkovou	vzpomínkový	k2eAgFnSc4d1	vzpomínková
knihu	kniha	k1gFnSc4	kniha
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k1gMnSc1	bos
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
byl	být	k5eAaImAgInS	být
také	také	k9	také
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
bestseller	bestseller	k1gInSc1	bestseller
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
spojena	spojen	k2eAgFnSc1d1	spojena
soška	soška	k1gFnSc1	soška
Olbrama	Olbram	k1gMnSc2	Olbram
Zoubka	Zoubek	k1gMnSc2	Zoubek
ztvárňující	ztvárňující	k2eAgFnSc4d1	ztvárňující
ženu	žena	k1gFnSc4	žena
vystupující	vystupující	k2eAgMnPc1d1	vystupující
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
prozaickou	prozaický	k2eAgFnSc4d1	prozaická
prvotinu	prvotina	k1gFnSc4	prvotina
Povídky	povídka	k1gFnSc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Dalšího	další	k2eAgInSc2d1	další
úspěchu	úspěch	k1gInSc2	úspěch
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uspěl	uspět	k5eAaPmAgMnS	uspět
s	s	k7c7	s
knihou	kniha	k1gFnSc7	kniha
Nové	Nové	k2eAgFnSc2d1	Nové
povídky	povídka	k1gFnSc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
úspěch	úspěch	k1gInSc1	úspěch
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
anketě	anketa	k1gFnSc6	anketa
sklidil	sklidit	k5eAaPmAgMnS	sklidit
Svěrák	Svěrák	k1gMnSc1	Svěrák
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dostal	dostat	k5eAaPmAgMnS	dostat
sošku	soška	k1gFnSc4	soška
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
knihou	kniha	k1gFnSc7	kniha
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k1gMnSc1	bos
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
držitelem	držitel	k1gMnSc7	držitel
"	"	kIx"	"
<g/>
Ceny	cena	k1gFnPc1	cena
pro	pro	k7c4	pro
nejdéle	dlouho	k6eAd3	dlouho
přemlouvaného	přemlouvaný	k2eAgMnSc4d1	přemlouvaný
autora	autor	k1gMnSc4	autor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
udělilo	udělit	k5eAaPmAgNnS	udělit
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
dopsání	dopsání	k1gNnSc1	dopsání
Svěrákovy	Svěrákův	k2eAgFnSc2d1	Svěrákova
pohádkové	pohádkový	k2eAgFnSc2d1	pohádková
knihy	kniha	k1gFnSc2	kniha
Pan	Pan	k1gMnSc1	Pan
Buřtík	buřtík	k1gInSc1	buřtík
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
Špejlička	špejlička	k1gFnSc1	špejlička
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
ji	on	k3xPp3gFnSc4	on
knižně	knižně	k6eAd1	knižně
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
získal	získat	k5eAaPmAgMnS	získat
Svěrák	Svěrák	k1gMnSc1	Svěrák
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Svět	svět	k1gInSc1	svět
knihy	kniha	k1gFnSc2	kniha
také	také	k9	také
Cenu	cena	k1gFnSc4	cena
Miloslava	Miloslav	k1gMnSc2	Miloslav
Švandrlíka	Švandrlík	k1gMnSc2	Švandrlík
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
jsou	být	k5eAaImIp3nP	být
dekorováni	dekorován	k2eAgMnPc1d1	dekorován
autoři	autor	k1gMnPc1	autor
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
humoristických	humoristický	k2eAgFnPc2d1	humoristická
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Porota	porota	k1gFnSc1	porota
takto	takto	k6eAd1	takto
ocenila	ocenit	k5eAaPmAgFnS	ocenit
publikaci	publikace	k1gFnSc4	publikace
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k1gMnSc1	bos
<g/>
.	.	kIx.	.
</s>
<s>
Svěrák	Svěrák	k1gMnSc1	Svěrák
je	být	k5eAaImIp3nS	být
též	též	k6eAd1	též
nositelem	nositel	k1gMnSc7	nositel
několika	několik	k4yIc2	několik
filmových	filmový	k2eAgNnPc2d1	filmové
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
dva	dva	k4xCgInPc4	dva
České	český	k2eAgInPc4d1	český
lvy	lev	k1gInPc4	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
za	za	k7c4	za
film	film	k1gInSc4	film
Kolja	Kolj	k1gInSc2	Kolj
a	a	k8xC	a
2007	[number]	k4	2007
za	za	k7c4	za
snímek	snímek	k1gInSc4	snímek
Vratné	vratný	k2eAgFnSc2d1	vratná
lahve	lahev	k1gFnSc2	lahev
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
navíc	navíc	k6eAd1	navíc
obdržel	obdržet	k5eAaPmAgInS	obdržet
ještě	ještě	k9	ještě
Českého	český	k2eAgInSc2d1	český
lva	lev	k1gInSc2	lev
za	za	k7c4	za
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
přínos	přínos	k1gInSc4	přínos
české	český	k2eAgFnSc3d1	Česká
kinematografii	kinematografie	k1gFnSc3	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
Vesničko	vesnička	k1gFnSc5	vesnička
má	mít	k5eAaImIp3nS	mít
středisková	střediskový	k2eAgFnSc1d1	středisková
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgMnSc3	který
napsal	napsat	k5eAaBmAgMnS	napsat
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
napsal	napsat	k5eAaBmAgMnS	napsat
nejenom	nejenom	k6eAd1	nejenom
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
si	se	k3xPyFc3	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
i	i	k9	i
zahrál	zahrát	k5eAaPmAgMnS	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
ocenění	ocenění	k1gNnSc4	ocenění
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Kolja	Koljus	k1gMnSc2	Koljus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
scenáristou	scenárista	k1gMnSc7	scenárista
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
hlavní	hlavní	k2eAgFnSc7d1	hlavní
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Českého	český	k2eAgInSc2d1	český
lva	lev	k1gInSc2	lev
1996	[number]	k4	1996
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
soutěži	soutěž	k1gFnSc6	soutěž
cenu	cena	k1gFnSc4	cena
filmových	filmový	k2eAgMnPc2d1	filmový
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
časopisu	časopis	k1gInSc2	časopis
Cinema	Cinemum	k1gNnSc2	Cinemum
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
uspěl	uspět	k5eAaPmAgInS	uspět
také	také	k9	také
na	na	k7c6	na
tokijském	tokijský	k2eAgInSc6d1	tokijský
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
dostal	dostat	k5eAaPmAgMnS	dostat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
a	a	k8xC	a
Svěrák	Svěrák	k1gMnSc1	Svěrák
ocenění	ocenění	k1gNnSc4	ocenění
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Dalšího	další	k2eAgNnSc2d1	další
ocenění	ocenění	k1gNnSc2	ocenění
se	se	k3xPyFc4	se
Svěrákovi	Svěrák	k1gMnSc3	Svěrák
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
dostalo	dostat	k5eAaPmAgNnS	dostat
ve	v	k7c6	v
španělském	španělský	k2eAgInSc6d1	španělský
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Madridimagen	Madridimagen	k1gInSc4	Madridimagen
1996	[number]	k4	1996
obdržel	obdržet	k5eAaPmAgMnS	obdržet
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejfotogeničtější	fotogenický	k2eAgInSc4d3	nejfotogeničtější
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Hollywoodská	hollywoodský	k2eAgFnSc1d1	hollywoodská
Asociace	asociace	k1gFnSc1	asociace
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
novinářů	novinář	k1gMnPc2	novinář
udělila	udělit	k5eAaPmAgFnS	udělit
filmu	film	k1gInSc3	film
Kolja	Kolj	k1gInSc2	Kolj
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
Akademie	akademie	k1gFnSc1	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
udělila	udělit	k5eAaPmAgFnS	udělit
snímku	snímka	k1gFnSc4	snímka
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1996	[number]	k4	1996
Oscara	Oscara	k1gFnSc1	Oscara
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novoměstském	novoměstský	k2eAgInSc6d1	novoměstský
hrnci	hrnec	k1gInSc6	hrnec
smíchu	smích	k1gInSc2	smích
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
cenu	cena	k1gFnSc4	cena
diváků	divák	k1gMnPc2	divák
za	za	k7c4	za
film	film	k1gInSc4	film
Tři	tři	k4xCgMnPc1	tři
veteráni	veterán	k1gMnPc1	veterán
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
cenu	cena	k1gFnSc4	cena
FITES	FITES	kA	FITES
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Kolja	Kolj	k1gInSc2	Kolj
<g/>
.	.	kIx.	.
</s>
<s>
Dalšího	další	k2eAgNnSc2d1	další
ocenění	ocenění	k1gNnSc2	ocenění
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
festivalu	festival	k1gInSc6	festival
dostalo	dostat	k5eAaPmAgNnS	dostat
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
Tři	tři	k4xCgMnPc4	tři
veteráni	veterán	k1gMnPc1	veterán
přinesla	přinést	k5eAaPmAgNnP	přinést
Svěrákovi	Svěrák	k1gMnSc3	Svěrák
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
i	i	k9	i
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
sci-fi	scii	k1gNnSc7	sci-fi
filmů	film	k1gInPc2	film
IMAGFIC	IMAGFIC	kA	IMAGFIC
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jubilejním	jubilejní	k2eAgInSc6d1	jubilejní
padesátém	padesátý	k4xOgInSc6	padesátý
ročníku	ročník	k1gInSc6	ročník
zlínského	zlínský	k2eAgInSc2d1	zlínský
Film	film	k1gInSc4	film
festivalu	festival	k1gInSc2	festival
obdržel	obdržet	k5eAaPmAgMnS	obdržet
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Uznání	uznání	k1gNnSc1	uznání
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc3d1	filmová
tvorbě	tvorba	k1gFnSc3	tvorba
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Ocenění	ocenění	k1gNnSc4	ocenění
získal	získat	k5eAaPmAgMnS	získat
také	také	k9	také
na	na	k7c6	na
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
(	(	kIx(	(
<g/>
MFF	MFF	kA	MFF
KV	KV	kA	KV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
udělili	udělit	k5eAaPmAgMnP	udělit
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
uznání	uznání	k1gNnSc4	uznání
za	za	k7c4	za
scénář	scénář	k1gInSc4	scénář
k	k	k7c3	k
filmu	film	k1gInSc3	film
Vratné	vratný	k2eAgFnSc2d1	vratná
lahve	lahev	k1gFnSc2	lahev
a	a	k8xC	a
snímek	snímek	k1gInSc4	snímek
zde	zde	k6eAd1	zde
tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
získal	získat	k5eAaPmAgInS	získat
Cenu	cena	k1gFnSc4	cena
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
tomtéž	týž	k3xTgNnSc6	týž
místě	místo	k1gNnSc6	místo
Cenu	cena	k1gFnSc4	cena
prezidenta	prezident	k1gMnSc2	prezident
festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
Berouně	Beroun	k1gInSc6	Beroun
"	"	kIx"	"
<g/>
Cenu	cena	k1gFnSc4	cena
Vladislava	Vladislav	k1gMnSc2	Vladislav
Vančury	Vančura	k1gMnSc2	Vančura
<g/>
"	"	kIx"	"
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
české	český	k2eAgFnSc3d1	Česká
kinematografii	kinematografie	k1gFnSc3	kinematografie
na	na	k7c6	na
tamních	tamní	k2eAgFnPc6d1	tamní
cenách	cena	k1gFnPc6	cena
určených	určený	k2eAgInPc2d1	určený
tvůrcům	tvůrce	k1gMnPc3	tvůrce
audiovizuální	audiovizuální	k2eAgFnSc2d1	audiovizuální
tvorby	tvorba	k1gFnSc2	tvorba
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Trilobit	trilobit	k1gMnSc1	trilobit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgNnP	být
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Art	Art	k1gFnSc1	Art
Film	film	k1gInSc1	film
Fest	fest	k6eAd1	fest
ve	v	k7c6	v
slovenských	slovenský	k2eAgFnPc6d1	slovenská
Trenčianských	trenčianský	k2eAgFnPc6d1	Trenčianská
Teplicích	Teplice	k1gFnPc6	Teplice
předána	předán	k2eAgFnSc1d1	předána
cena	cena	k1gFnSc1	cena
"	"	kIx"	"
<g/>
Hercova	hercův	k2eAgFnSc1d1	hercova
mise	mise	k1gFnSc1	mise
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jsou	být	k5eAaImIp3nP	být
dekorováni	dekorován	k2eAgMnPc1d1	dekorován
herci	herec	k1gMnPc1	herec
za	za	k7c4	za
jejich	jejich	k3xOp3gInSc4	jejich
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
filmovému	filmový	k2eAgNnSc3d1	filmové
hereckému	herecký	k2eAgNnSc3d1	herecké
umění	umění	k1gNnSc3	umění
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
ocenění	ocenění	k1gNnSc2	ocenění
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
tradice	tradice	k1gFnSc1	tradice
připevnění	připevnění	k1gNnSc2	připevnění
cedulky	cedulka	k1gFnSc2	cedulka
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
oceněného	oceněný	k2eAgNnSc2d1	oceněné
na	na	k7c4	na
most	most	k1gInSc4	most
Slávy	Sláva	k1gFnSc2	Sláva
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
městském	městský	k2eAgInSc6d1	městský
parku	park	k1gInSc6	park
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Janem	Jan	k1gMnSc7	Jan
získal	získat	k5eAaPmAgMnS	získat
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
na	na	k7c6	na
bratislavském	bratislavský	k2eAgInSc6d1	bratislavský
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
výjimečnost	výjimečnost	k1gFnSc4	výjimečnost
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
kinematografii	kinematografie	k1gFnSc6	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
hry	hra	k1gFnPc4	hra
vydávalo	vydávat	k5eAaImAgNnS	vydávat
na	na	k7c6	na
zvukových	zvukový	k2eAgInPc6d1	zvukový
nosičích	nosič	k1gInPc6	nosič
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
převzalo	převzít	k5eAaPmAgNnS	převzít
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
speciální	speciální	k2eAgFnSc4d1	speciální
Miliónovou	miliónový	k2eAgFnSc4d1	miliónová
desku	deska	k1gFnSc4	deska
Supraphonu	supraphon	k1gInSc2	supraphon
udělenou	udělený	k2eAgFnSc4d1	udělená
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
milión	milión	k4xCgInSc1	milión
prodaných	prodaný	k2eAgInPc2d1	prodaný
zvukových	zvukový	k2eAgInPc2d1	zvukový
nosičů	nosič	k1gInPc2	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
navíc	navíc	k6eAd1	navíc
převzal	převzít	k5eAaPmAgMnS	převzít
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
dvojnásobnou	dvojnásobný	k2eAgFnSc4d1	dvojnásobná
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
Supraphonu	supraphon	k1gInSc2	supraphon
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
tisíc	tisíc	k4xCgInSc4	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
s	s	k7c7	s
nahrávkami	nahrávka	k1gFnPc7	nahrávka
Svěrákových	Svěrákových	k2eAgFnPc2d1	Svěrákových
knih	kniha	k1gFnPc2	kniha
Povídky	povídka	k1gFnSc2	povídka
a	a	k8xC	a
Nové	Nové	k2eAgFnSc2d1	Nové
povídky	povídka	k1gFnSc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
nadace	nadace	k1gFnSc2	nadace
Pangea	Pangea	k1gFnSc1	Pangea
ocenění	ocenění	k1gNnSc4	ocenění
"	"	kIx"	"
<g/>
Za	za	k7c4	za
úsilí	úsilí	k1gNnSc4	úsilí
o	o	k7c4	o
nápravu	náprava	k1gFnSc4	náprava
věcí	věc	k1gFnPc2	věc
lidských	lidský	k2eAgFnPc2d1	lidská
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1999	[number]	k4	1999
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Medaili	medaile	k1gFnSc4	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
udělenou	udělený	k2eAgFnSc4d1	udělená
za	za	k7c4	za
vynikající	vynikající	k2eAgInPc4d1	vynikající
umělecké	umělecký	k2eAgInPc4d1	umělecký
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
soutěž	soutěž	k1gFnSc1	soutěž
Největší	veliký	k2eAgFnSc7d3	veliký
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Svěrák	Svěrák	k1gMnSc1	Svěrák
obsadil	obsadit	k5eAaPmAgMnS	obsadit
25	[number]	k4	25
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
v	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
anketě	anketa	k1gFnSc6	anketa
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
také	také	k9	také
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
nakonec	nakonec	k6eAd1	nakonec
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
ankety	anketa	k1gFnPc1	anketa
nemohly	moct	k5eNaImAgFnP	moct
účastnit	účastnit	k5eAaImF	účastnit
fiktivní	fiktivní	k2eAgFnPc4d1	fiktivní
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
Svěrák	Svěrák	k1gMnSc1	Svěrák
stal	stát	k5eAaPmAgMnS	stát
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
Kopidlna	Kopidlno	k1gNnSc2	Kopidlno
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
jako	jako	k9	jako
chlapec	chlapec	k1gMnSc1	chlapec
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
mu	on	k3xPp3gMnSc3	on
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
pivovar	pivovar	k1gInSc1	pivovar
předal	předat	k5eAaPmAgInS	předat
ocenění	ocenění	k1gNnSc4	ocenění
Honorary	Honorara	k1gFnSc2	Honorara
Connoisseur	Connoisseura	k1gFnPc2	Connoisseura
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
šíří	šířit	k5eAaImIp3nS	šířit
věhlas	věhlas	k1gInSc4	věhlas
plzeňského	plzeňský	k2eAgNnSc2d1	plzeňské
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
královéhradecké	královéhradecký	k2eAgFnSc6d1	Královéhradecká
univerzitě	univerzita	k1gFnSc6	univerzita
získal	získat	k5eAaPmAgInS	získat
čestný	čestný	k2eAgInSc1d1	čestný
doktorát	doktorát	k1gInSc1	doktorát
udělený	udělený	k2eAgInSc1d1	udělený
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
jazykové	jazykový	k2eAgFnSc2d1	jazyková
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Svěrákovo	Svěrákův	k2eAgNnSc1d1	Svěrákovo
herectví	herectví	k1gNnSc1	herectví
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
na	na	k7c6	na
divadelním	divadelní	k2eAgNnSc6d1	divadelní
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
před	před	k7c7	před
filmovou	filmový	k2eAgFnSc7d1	filmová
či	či	k8xC	či
televizní	televizní	k2eAgFnSc7d1	televizní
kamerou	kamera	k1gFnSc7	kamera
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
opravdovosti	opravdovost	k1gFnSc2	opravdovost
a	a	k8xC	a
vážnosti	vážnost	k1gFnSc2	vážnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yRgFnSc7	jaký
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
ať	ať	k9	ať
už	už	k6eAd1	už
slovní	slovní	k2eAgNnSc1d1	slovní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
situační	situační	k2eAgFnPc1d1	situační
absurdity	absurdita	k1gFnPc1	absurdita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gInPc6	on
přehnaně	přehnaně	k6eAd1	přehnaně
využívá	využívat	k5eAaImIp3nS	využívat
učitelský	učitelský	k2eAgInSc4d1	učitelský
výklad	výklad	k1gInSc4	výklad
a	a	k8xC	a
gestikulaci	gestikulace	k1gFnSc4	gestikulace
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
běžných	běžný	k2eAgFnPc6d1	běžná
věcech	věc	k1gFnPc6	věc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
herectví	herectví	k1gNnSc1	herectví
postupně	postupně	k6eAd1	postupně
nabytými	nabytý	k2eAgFnPc7d1	nabytá
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
širokému	široký	k2eAgInSc3d1	široký
výrazovému	výrazový	k2eAgInSc3d1	výrazový
rejstříku	rejstřík	k1gInSc3	rejstřík
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
stále	stále	k6eAd1	stále
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
neherce	neherec	k1gMnPc4	neherec
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
herectví	herectví	k1gNnSc2	herectví
za	za	k7c4	za
samouka	samouk	k1gMnSc4	samouk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
scenáristické	scenáristický	k2eAgNnSc1d1	scenáristické
i	i	k8xC	i
herecké	herecký	k2eAgNnSc1d1	herecké
umění	umění	k1gNnSc1	umění
naplno	naplno	k6eAd1	naplno
rozvinulo	rozvinout	k5eAaPmAgNnS	rozvinout
až	až	k6eAd1	až
během	během	k7c2	během
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
role	role	k1gFnPc4	role
podlézavých	podlézavý	k2eAgInPc2d1	podlézavý
<g/>
,	,	kIx,	,
úslužných	úslužný	k2eAgInPc2d1	úslužný
až	až	k8xS	až
škodolibých	škodolibý	k2eAgInPc2d1	škodolibý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
i	i	k9	i
šprýmařských	šprýmařský	k2eAgFnPc2d1	šprýmařský
epizodních	epizodní	k2eAgFnPc2d1	epizodní
postav	postava	k1gFnPc2	postava
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
autorských	autorský	k2eAgFnPc6d1	autorská
komediích	komedie	k1gFnPc6	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
ale	ale	k9	ale
Smoljak	Smoljak	k1gMnSc1	Smoljak
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
od	od	k7c2	od
herectví	herectví	k1gNnSc2	herectví
odkláněl	odklánět	k5eAaImAgInS	odklánět
k	k	k7c3	k
režii	režie	k1gFnSc3	režie
a	a	k8xC	a
Svěrák	Svěrák	k1gMnSc1	Svěrák
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
psaní	psaní	k1gNnSc4	psaní
scénářů	scénář	k1gInPc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
využívají	využívat	k5eAaPmIp3nP	využívat
nejen	nejen	k6eAd1	nejen
typické	typický	k2eAgFnPc1d1	typická
nápadité	nápaditý	k2eAgFnPc1d1	nápaditá
fabulace	fabulace	k1gFnPc1	fabulace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poetiky	poetika	k1gFnSc2	poetika
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
inteligentním	inteligentní	k2eAgInSc7d1	inteligentní
humorem	humor	k1gInSc7	humor
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
tak	tak	k9	tak
kvalita	kvalita	k1gFnSc1	kvalita
Svěrákovy	Svěrákův	k2eAgFnSc2d1	Svěrákova
autorské	autorský	k2eAgFnSc2d1	autorská
činnosti	činnost	k1gFnSc2	činnost
upozadila	upozadit	k5eAaPmAgFnS	upozadit
jeho	on	k3xPp3gNnSc2	on
herectví	herectví	k1gNnSc2	herectví
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgMnSc1d1	filmový
publicista	publicista	k1gMnSc1	publicista
Kamil	Kamil	k1gMnSc1	Kamil
Fila	Fila	k1gMnSc1	Fila
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
u	u	k7c2	u
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
Vrchní	vrchní	k1gMnSc1	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Kulový	kulový	k2eAgInSc1d1	kulový
blesk	blesk	k1gInSc1	blesk
či	či	k8xC	či
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
téměř	téměř	k6eAd1	téměř
nevystupují	vystupovat	k5eNaImIp3nP	vystupovat
hrdé	hrdý	k2eAgFnPc4d1	hrdá
postavy	postava	k1gFnPc4	postava
(	(	kIx(	(
<g/>
lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
trapní	trapný	k2eAgMnPc1d1	trapný
<g/>
,	,	kIx,	,
ohnutí	ohnutý	k2eAgMnPc1d1	ohnutý
či	či	k8xC	či
ponížení	ponížený	k2eAgMnPc1d1	ponížený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
filmů	film	k1gInPc2	film
tak	tak	k8xS	tak
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
nešvary	nešvar	k1gInPc1	nešvar
normalizační	normalizační	k2eAgFnSc2d1	normalizační
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
Fila	Fila	k1gMnSc1	Fila
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
jejich	jejich	k3xOp3gFnSc4	jejich
schopnost	schopnost	k1gFnSc4	schopnost
kriticky	kriticky	k6eAd1	kriticky
vypovídat	vypovídat	k5eAaPmF	vypovídat
i	i	k9	i
v	v	k7c6	v
komediálních	komediální	k2eAgInPc6d1	komediální
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Akt	akt	k1gInSc1	akt
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Hospoda	Hospoda	k?	Hospoda
Na	na	k7c6	na
mýtince	mýtinka	k1gFnSc6	mýtinka
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
salonním	salonní	k2eAgMnSc6d1	salonní
coupé	coupý	k2eAgInPc4d1	coupý
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Němý	němý	k2eAgMnSc1d1	němý
Bobeš	Bobeš	k1gMnSc1	Bobeš
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Cimrman	Cimrman	k1gMnSc1	Cimrman
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Široký	Široký	k1gMnSc1	Široký
a	a	k8xC	a
Krátkozraký	krátkozraký	k2eAgMnSc1d1	krátkozraký
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Posel	posel	k1gMnSc1	posel
z	z	k7c2	z
Liptákova	Liptákův	k2eAgInSc2d1	Liptákův
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Lijavec	lijavec	k1gInSc4	lijavec
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Dobytí	dobytí	k1gNnSc2	dobytí
<g />
.	.	kIx.	.
</s>
<s>
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
Čechem	Čech	k1gMnSc7	Čech
Karlem	Karel	k1gMnSc7	Karel
Němcem	Němec	k1gMnSc7	Němec
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1909	[number]	k4	1909
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Blaník	Blaník	k1gInSc4	Blaník
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Záskok	záskok	k1gInSc4	záskok
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Švestka	Švestka	k1gMnSc1	Švestka
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Afrika	Afrika	k1gFnSc1	Afrika
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gInSc7	Smoljak
České	český	k2eAgNnSc1d1	české
nebe	nebe	k1gNnSc1	nebe
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
hry	hra	k1gFnSc2	hra
postupně	postupně	k6eAd1	postupně
vycházely	vycházet	k5eAaImAgFnP	vycházet
také	také	k9	také
na	na	k7c6	na
kompaktních	kompaktní	k2eAgInPc6d1	kompaktní
discích	disk	k1gInPc6	disk
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
zpočátku	zpočátku	k6eAd1	zpočátku
též	též	k9	též
na	na	k7c6	na
gramofonových	gramofonový	k2eAgFnPc6d1	gramofonová
deskách	deska	k1gFnPc6	deska
(	(	kIx(	(
<g/>
LP	LP	kA	LP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
nahranou	nahraný	k2eAgFnSc7d1	nahraná
hrou	hra	k1gFnSc7	hra
bylo	být	k5eAaImAgNnS	být
představení	představení	k1gNnSc1	představení
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Široký	Široký	k1gMnSc1	Široký
a	a	k8xC	a
Krátkozraký	krátkozraký	k2eAgMnSc1d1	krátkozraký
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
gramofonových	gramofonový	k2eAgFnPc6d1	gramofonová
deskách	deska	k1gFnPc6	deska
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
na	na	k7c6	na
kompaktních	kompaktní	k2eAgInPc6d1	kompaktní
discích	disk	k1gInPc6	disk
vycházely	vycházet	k5eAaImAgFnP	vycházet
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc1d1	další
záznamy	záznam	k1gInPc1	záznam
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
<g/>
:	:	kIx,	:
Cimrman	Cimrman	k1gMnSc1	Cimrman
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
vědecké	vědecký	k2eAgInPc4d1	vědecký
<g/>
"	"	kIx"	"
semináře	seminář	k1gInPc4	seminář
z	z	k7c2	z
představení	představení	k1gNnSc2	představení
Cimrman	Cimrman	k1gMnSc1	Cimrman
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
hudby	hudba	k1gFnSc2	hudba
Zapomenuté	zapomenutý	k2eAgFnSc2d1	zapomenutá
věci	věc	k1gFnSc2	věc
zapomenutého	zapomenutý	k2eAgMnSc2d1	zapomenutý
génia	génius	k1gMnSc2	génius
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Posel	posel	k1gMnSc1	posel
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Cimrmanovské	cimrmanovský	k2eAgInPc4d1	cimrmanovský
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
nedostalo	dostat	k5eNaPmAgNnS	dostat
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
záznam	záznam	k1gInSc4	záznam
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
představení	představení	k1gNnSc2	představení
ke	k	k7c3	k
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
založení	založení	k1gNnSc2	založení
divadla	divadlo	k1gNnSc2	divadlo
Všech	všecek	k3xTgNnPc2	všecek
15	[number]	k4	15
her	hra	k1gFnPc2	hra
a	a	k8xC	a
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
nedostalo	dostat	k5eNaPmAgNnS	dostat
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Lochneska	lochneska	k1gFnSc1	lochneska
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
Hodina	hodina	k1gFnSc1	hodina
zpěvu	zpěv	k1gInSc2	zpěv
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Není	být	k5eNaImIp3nS	být
nutno	nutno	k6eAd1	nutno
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
...	...	k?	...
<g/>
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
přímo	přímo	k6eAd1	přímo
veselo	veselo	k6eAd1	veselo
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Hlavně	hlavně	k9	hlavně
nesmí	smět	k5eNaImIp3nS	smět
býti	být	k5eAaImF	být
smutno	smutno	k6eAd1	smutno
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
...	...	k?	...
<g/>
natož	natož	k6eAd1	natož
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
brečelo	brečet	k5eAaImAgNnS	brečet
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Zpěvník	zpěvník	k1gInSc4	zpěvník
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Nemít	mít	k5eNaImF	mít
prachy	prach	k1gInPc4	prach
-	-	kIx~	-
nevadí	vadit	k5eNaImIp3nS	vadit
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
<g />
.	.	kIx.	.
</s>
<s>
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Vánoční	vánoční	k2eAgInPc1d1	vánoční
a	a	k8xC	a
noční	noční	k2eAgInPc1d1	noční
sny	sen	k1gInPc1	sen
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Nemít	mít	k5eNaImF	mít
srdce	srdce	k1gNnSc4	srdce
-	-	kIx~	-
vadí	vadit	k5eAaImIp3nS	vadit
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Zpěvník	zpěvník	k1gInSc4	zpěvník
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
...	...	k?	...
<g/>
zažít	zažít	k5eAaPmF	zažít
krachy	krach	k1gInPc4	krach
-	-	kIx~	-
nevadí	vadit	k5eNaImIp3nS	vadit
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
...	...	k?	...
<g/>
zažít	zažít	k5eAaPmF	zažít
nudu	nuda	k1gFnSc4	nuda
-	-	kIx~	-
vadí	vadit	k5eAaImIp3nS	vadit
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
20	[number]	k4	20
let	léto	k1gNnPc2	léto
písniček	písnička	k1gFnPc2	písnička
z	z	k7c2	z
pořadu	pořad	k1gInSc2	pořad
Hodina	hodina	k1gFnSc1	hodina
zpěvu	zpěv	k1gInSc2	zpěv
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Hity	hit	k1gInPc1	hit
a	a	k8xC	a
skorohity	skorohit	k1gInPc1	skorohit
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Takovej	Takovej	k?	Takovej
ten	ten	k3xDgInSc1	ten
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
tou	ten	k3xDgFnSc7	ten
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Písničky	písnička	k1gFnSc2	písnička
o	o	k7c6	o
zvířatech	zvíře	k1gNnPc6	zvíře
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Alchymisti	Alchymisti	k?	Alchymisti
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Jupí	Jupí	k1gMnSc1	Jupí
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Operky	operka	k1gFnPc1	operka
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
<g />
.	.	kIx.	.
</s>
<s>
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Cirkusový	cirkusový	k2eAgInSc1d1	cirkusový
stan	stan	k1gInSc1	stan
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Ty	ty	k3xPp2nSc1	ty
nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
písničky	písnička	k1gFnPc1	písnička
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
kabátě	kabát	k1gInSc6	kabát
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Uhlířem	Uhlíř	k1gMnSc7	Uhlíř
Na	na	k7c6	na
kompaktních	kompaktní	k2eAgInPc6d1	kompaktní
discích	disk	k1gInPc6	disk
vyšla	vyjít	k5eAaPmAgFnS	vyjít
dále	daleko	k6eAd2	daleko
tato	tento	k3xDgFnSc1	tento
Svěrákova	Svěrákův	k2eAgFnSc1d1	Svěrákova
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
:	:	kIx,	:
Tatínku	tatínek	k1gMnSc5	tatínek
<g/>
,	,	kIx,	,
ta	ten	k3xDgNnPc1	ten
se	se	k3xPyFc4	se
ti	ty	k3xPp2nSc3	ty
povedla	povést	k5eAaPmAgFnS	povést
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Podzemnice	podzemnice	k1gFnSc1	podzemnice
olejná	olejný	k2eAgFnSc1d1	olejná
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Radovanovy	Radovanův	k2eAgFnSc2d1	Radovanova
radovánky	radovánka	k1gFnSc2	radovánka
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
-	-	kIx~	-
Svěrák	Svěrák	k1gMnSc1	Svěrák
načetl	načíst	k5eAaPmAgMnS	načíst
přepis	přepis	k1gInSc4	přepis
televizního	televizní	k2eAgInSc2d1	televizní
večerníčku	večerníček	k1gInSc2	večerníček
(	(	kIx(	(
<g/>
večerníček	večerníček	k1gInSc1	večerníček
namluvil	namluvit	k5eAaBmAgMnS	namluvit
Luděk	Luděk	k1gMnSc1	Luděk
Sobota	Sobota	k1gMnSc1	Sobota
<g/>
)	)	kIx)	)
Staré	Staré	k2eAgFnPc1d1	Staré
pověsti	pověst	k1gFnPc1	pověst
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
-	-	kIx~	-
Svěrák	Svěrák	k1gMnSc1	Svěrák
načetl	načíst	k5eAaBmAgMnS	načíst
Jiráskovy	Jiráskův	k2eAgFnPc4d1	Jiráskova
pověsti	pověst	k1gFnPc4	pověst
Odskok	odskok	k1gInSc4	odskok
od	od	k7c2	od
Cimrmana	Cimrman	k1gMnSc2	Cimrman
do	do	k7c2	do
lázní	lázeň	k1gFnPc2	lázeň
Kožich	kožich	k1gInSc1	kožich
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Bruknerem	Brukner	k1gMnSc7	Brukner
Posel	posít	k5eAaPmAgMnS	posít
Hydrometeorologického	hydrometeorologický	k2eAgInSc2d1	hydrometeorologický
ústavu	ústav	k1gInSc2	ústav
a	a	k8xC	a
To	to	k9	to
jeli	jet	k5eAaImAgMnP	jet
dva	dva	k4xCgInPc4	dva
ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Pan	Pan	k1gMnSc1	Pan
Buřtík	buřtík	k1gInSc1	buřtík
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
Špejlička	špejlička	k1gFnSc1	špejlička
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Nové	Nové	k2eAgFnSc2d1	Nové
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
O	o	k7c6	o
lidech	člověk	k1gMnPc6	člověk
a	a	k8xC	a
lidech	člověk	k1gMnPc6	člověk
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
-	-	kIx~	-
fejetony	fejeton	k1gInPc1	fejeton
Milana	Milan	k1gMnSc2	Milan
Lasicy	Lasica	k1gMnSc2	Lasica
čte	číst	k5eAaImIp3nS	číst
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc1	jejich
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
předmluva	předmluva	k1gFnSc1	předmluva
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k1gMnSc1	bos
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Rybí	rybí	k2eAgFnSc2d1	rybí
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
Svěrák	Svěrák	k1gMnSc1	Svěrák
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
(	(	kIx(	(
<g/>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc1	eben
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Lábus	Lábus	k1gMnSc1	Lábus
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
načetli	načíst	k5eAaBmAgMnP	načíst
povídky	povídka	k1gFnPc4	povídka
Oty	Ota	k1gMnSc2	Ota
Pavla	Pavel	k1gMnSc2	Pavel
jako	jako	k8xC	jako
charitativní	charitativní	k2eAgNnSc4d1	charitativní
album	album	k1gNnSc4	album
pro	pro	k7c4	pro
Centrum	centrum	k1gNnSc4	centrum
Paraple	Paraple	k?	Paraple
Horká	Horká	k1gFnSc1	Horká
neděle	neděle	k1gFnSc2	neděle
aneb	aneb	k?	aneb
Vynechané	vynechaný	k2eAgFnSc2d1	vynechaná
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Dětské	dětský	k2eAgFnSc2d1	dětská
etudy	etuda	k1gFnSc2	etuda
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
načetl	načíst	k5eAaPmAgMnS	načíst
knihu	kniha	k1gFnSc4	kniha
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Aškenazyho	Aškenazy	k1gMnSc2	Aškenazy
Tři	tři	k4xCgNnPc1	tři
auta	auto	k1gNnPc1	auto
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Tiché	Tiché	k2eAgNnSc1d1	Tiché
šlapací	šlapací	k2eAgNnSc1d1	šlapací
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
nahrána	nahrát	k5eAaBmNgFnS	nahrát
jako	jako	k8xS	jako
Kolo	kolo	k1gNnSc1	kolo
se	s	k7c7	s
zlatými	zlatý	k2eAgInPc7d1	zlatý
ráfky	ráfek	k1gInPc7	ráfek
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Krápník	krápník	k1gInSc1	krápník
a	a	k8xC	a
Františka	Františka	k1gFnSc1	Františka
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
To	to	k9	to
jeli	jet	k5eAaImAgMnP	jet
dva	dva	k4xCgInPc4	dva
ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Posel	posel	k1gMnSc1	posel
Hydrometeorologického	hydrometeorologický	k2eAgInSc2d1	hydrometeorologický
ústavu	ústav	k1gInSc2	ústav
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Příliš	příliš	k6eAd1	příliš
krásná	krásný	k2eAgFnSc1d1	krásná
dívka	dívka	k1gFnSc1	dívka
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Miloněm	Miloň	k1gMnSc7	Miloň
Čepelkou	čepelka	k1gFnSc7	čepelka
Jáchyme	Jáchym	k1gMnSc5	Jáchym
<g/>
,	,	kIx,	,
hoď	hodit	k5eAaImRp2nS	hodit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
stroje	stroj	k1gInSc2	stroj
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Kdo	kdo	k3yInSc1	kdo
hledá	hledat	k5eAaImIp3nS	hledat
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
dno	dno	k1gNnSc4	dno
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Vojtěchem	Vojtěch	k1gMnSc7	Vojtěch
Měšťanem	Měšťan	k1gMnSc7	Měšťan
a	a	k8xC	a
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Rážem	Ráž	k1gMnSc7	Ráž
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
spustil	spustit	k5eAaPmAgInS	spustit
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Miloněm	Miloň	k1gMnSc7	Miloň
Čepelkou	čepelka	k1gFnSc7	čepelka
(	(	kIx(	(
<g/>
hudební	hudební	k2eAgFnSc1d1	hudební
veselohra	veselohra	k1gFnSc1	veselohra
neblaze	blaze	k6eNd1	blaze
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Nuselákem	Nuselák	k1gInSc7	Nuselák
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Příliš	příliš	k6eAd1	příliš
krásná	krásný	k2eAgFnSc1d1	krásná
dívka	dívka	k1gFnSc1	dívka
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Miloněm	Miloň	k1gMnSc7	Miloň
Čepelkou	čepelka	k1gFnSc7	čepelka
Dveře	dveře	k1gFnPc1	dveře
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Miloněm	Miloň	k1gMnSc7	Miloň
Čepelkou	čepelka	k1gFnSc7	čepelka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
malá	malý	k2eAgFnSc1d1	malá
televizní	televizní	k2eAgFnSc1d1	televizní
komedie	komedie	k1gFnSc1	komedie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
u	u	k7c2	u
lesa	les	k1gInSc2	les
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Marečku	Mareček	k1gMnSc5	Mareček
<g/>
,	,	kIx,	,
podejte	podat	k5eAaPmRp2nP	podat
mi	já	k3xPp1nSc3	já
pero	pero	k1gNnSc5	pero
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Ať	ať	k8xC	ať
žijí	žít	k5eAaImIp3nP	žít
duchové	duch	k1gMnPc1	duch
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Lipským	lipský	k2eAgMnSc7d1	lipský
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Melíškem	Melíšek	k1gMnSc7	Melíšek
Kulový	kulový	k2eAgInSc4d1	kulový
blesk	blesk	k1gInSc4	blesk
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Trhák	trhák	k1gInSc4	trhák
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Vrchní	vrchní	k1gFnSc2	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Jára	Jára	k1gFnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
ležící	ležící	k2eAgMnSc1d1	ležící
<g/>
,	,	kIx,	,
spící	spící	k2eAgMnSc1d1	spící
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Tři	tři	k4xCgMnPc4	tři
veteráni	veterán	k1gMnPc1	veterán
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Inzerát	inzerát	k1gInSc1	inzerát
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Rozpuštěný	rozpuštěný	k2eAgMnSc1d1	rozpuštěný
a	a	k8xC	a
vypuštěný	vypuštěný	k2eAgMnSc1d1	vypuštěný
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Co	co	k8xS	co
je	být	k5eAaImIp3nS	být
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
doktore	doktor	k1gMnSc5	doktor
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Antonínem	Antonín	k1gMnSc7	Antonín
Mášou	Máša	k1gFnSc7	Máša
a	a	k8xC	a
Vítem	Vít	k1gMnSc7	Vít
Olmerem	Olmer	k1gMnSc7	Olmer
Vesničko	vesnička	k1gFnSc5	vesnička
má	mít	k5eAaImIp3nS	mít
středisková	střediskový	k2eAgFnSc1d1	středisková
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Případ	případ	k1gInSc1	případ
Platfus	platfus	k2eAgMnPc2d1	platfus
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
Narostl	narůst	k5eAaPmAgMnS	narůst
mu	on	k3xPp3gMnSc3	on
hřebínek	hřebínek	k1gInSc1	hřebínek
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
Radovanovy	Radovanův	k2eAgFnSc2d1	Radovanova
radovánky	radovánka	k1gFnSc2	radovánka
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc4d1	televizní
večerníček	večerníček	k1gInSc4	večerníček
Utopím	utopit	k5eAaPmIp1nS	utopit
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
sám	sám	k3xTgInSc4	sám
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
Obecná	obecná	k1gFnSc5	obecná
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Osvětová	osvětový	k2eAgNnPc4d1	osvětové
přednáška	přednáška	k1gFnSc1	přednáška
v	v	k7c6	v
Suché	Suché	k2eAgFnSc6d1	Suché
Vrbici	vrbice	k1gFnSc6	vrbice
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc4d1	televizní
<g />
.	.	kIx.	.
</s>
<s>
film	film	k1gInSc1	film
Život	život	k1gInSc1	život
a	a	k8xC	a
neobyčejná	obyčejný	k2eNgNnPc1d1	neobyčejné
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
vojáka	voják	k1gMnSc2	voják
Ivana	Ivan	k1gMnSc2	Ivan
Čonkina	Čonkin	k1gMnSc2	Čonkin
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Akumulátor	akumulátor	k1gInSc1	akumulátor
1	[number]	k4	1
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Kolja	Kolj	k1gInSc2	Kolj
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Chlípník	chlípník	k1gMnSc1	chlípník
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
policejní	policejní	k2eAgFnSc2d1	policejní
brašny	brašna	k1gFnSc2	brašna
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Lotrando	Lotrando	k1gMnSc1	Lotrando
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Zubejda	Zubejda	k1gMnSc1	Zubejda
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Tmavomodrý	tmavomodrý	k2eAgInSc1d1	tmavomodrý
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Vratné	vratný	k2eAgFnSc2d1	vratná
lahve	lahev	k1gFnSc2	lahev
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Zločin	zločin	k1gInSc1	zločin
v	v	k7c6	v
šantánu	šantán	k1gInSc6	šantán
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
-	-	kIx~	-
prokurátor	prokurátor	k1gMnSc1	prokurátor
Skřivánci	Skřivánek	k1gMnPc1	Skřivánek
na	na	k7c6	na
niti	nit	k1gFnSc6	nit
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
-	-	kIx~	-
delegát	delegát	k1gMnSc1	delegát
Ústřední	ústřední	k2eAgFnSc2d1	ústřední
rady	rada	k1gFnSc2	rada
odborů	odbor	k1gInPc2	odbor
Pane	Pan	k1gMnSc5	Pan
<g/>
,	,	kIx,	,
vy	vy	k3xPp2nPc1	vy
jste	být	k5eAaImIp2nP	být
vdova	vdova	k1gFnSc1	vdova
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
-	-	kIx~	-
kritik	kritik	k1gMnSc1	kritik
Lucie	Lucie	k1gFnSc1	Lucie
a	a	k8xC	a
zázraky	zázrak	k1gInPc1	zázrak
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
-	-	kIx~	-
strážník	strážník	k1gMnSc1	strážník
30	[number]	k4	30
panen	panna	k1gFnPc2	panna
a	a	k8xC	a
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
-	-	kIx~	-
učitel	učitel	k1gMnSc1	učitel
Jáchyme	Jáchym	k1gMnSc5	Jáchym
<g/>
,	,	kIx,	,
hoď	hodit	k5eAaPmRp2nS	hodit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
stroje	stroj	k1gInSc2	stroj
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
-	-	kIx~	-
psycholog	psycholog	k1gMnSc1	psycholog
Klásek	klásek	k1gInSc4	klásek
Na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
u	u	k7c2	u
lesa	les	k1gInSc2	les
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
-	-	kIx~	-
Lavička	Lavička	k1gMnSc1	Lavička
Marečku	Mareček	k1gMnSc3	Mareček
<g/>
,	,	kIx,	,
podejte	podat	k5eAaPmRp2nP	podat
mi	já	k3xPp1nSc3	já
pero	pero	k1gNnSc1	pero
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
-	-	kIx~	-
Šlajs	Šlajs	k1gInSc1	Šlajs
Kulový	kulový	k2eAgInSc1d1	kulový
blesk	blesk	k1gInSc1	blesk
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
-	-	kIx~	-
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Ječný	ječný	k2eAgMnSc1d1	ječný
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bobr	bobr	k1gMnSc1	bobr
Kam	kam	k6eAd1	kam
nikdo	nikdo	k3yNnSc1	nikdo
nesmí	smět	k5eNaImIp3nS	smět
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Vrchní	vrchní	k2eAgFnSc2d1	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
-	-	kIx~	-
soused	soused	k1gMnSc1	soused
Pařízek	Pařízek	k1gMnSc1	Pařízek
Trhák	trhák	k1gInSc1	trhák
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
-	-	kIx~	-
scenárista	scenárista	k1gMnSc1	scenárista
Jíša	Jíša	k1gMnSc1	Jíša
Kalamita	kalamita	k1gFnSc1	kalamita
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
-	-	kIx~	-
učitel	učitel	k1gMnSc1	učitel
Jako	jako	k8xS	jako
zajíci	zajíc	k1gMnPc1	zajíc
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
Dušek	Dušek	k1gMnSc1	Dušek
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jsme	být	k5eAaImIp1nP	být
normální	normální	k2eAgMnSc1d1	normální
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
-	-	kIx~	-
vedoucí	vedoucí	k1gMnSc1	vedoucí
školení	školení	k1gNnSc2	školení
Morávek	Morávek	k1gMnSc1	Morávek
Srdečný	srdečný	k2eAgInSc1d1	srdečný
pozdrav	pozdrav	k1gInSc1	pozdrav
<g />
.	.	kIx.	.
</s>
<s>
ze	z	k7c2	z
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
-	-	kIx~	-
vědec	vědec	k1gMnSc1	vědec
Karty	karta	k1gFnSc2	karta
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
-	-	kIx~	-
vypravěč	vypravěč	k1gMnSc1	vypravěč
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
ležící	ležící	k2eAgMnSc1d1	ležící
<g/>
,	,	kIx,	,
spící	spící	k2eAgMnSc1d1	spící
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jára	Jára	k1gFnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
Inzerát	inzerát	k1gInSc1	inzerát
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
veteráni	veterán	k1gMnPc1	veterán
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
ministr	ministr	k1gMnSc1	ministr
a	a	k8xC	a
rádce	rádce	k1gMnSc1	rádce
Slavnosti	slavnost	k1gFnSc2	slavnost
sněženek	sněženka	k1gFnPc2	sněženka
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
řidič	řidič	k1gMnSc1	řidič
trabantu	trabant	k1gInSc2	trabant
Svatební	svatební	k2eAgFnSc1d1	svatební
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Jiljí	Jiljí	k1gMnSc2	Jiljí
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
-	-	kIx~	-
průvodčí	průvodčí	k1gMnSc1	průvodčí
Rozpuštěný	rozpuštěný	k2eAgMnSc1d1	rozpuštěný
a	a	k8xC	a
vypuštěný	vypuštěný	k2eAgMnSc1d1	vypuštěný
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
-	-	kIx~	-
průmyslový	průmyslový	k2eAgMnSc1d1	průmyslový
básník	básník	k1gMnSc1	básník
Jelínek	Jelínek	k1gMnSc1	Jelínek
Co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
doktore	doktor	k1gMnSc5	doktor
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
-	-	kIx~	-
zubař	zubař	k1gMnSc1	zubař
Bohouš	Bohouš	k1gMnSc1	Bohouš
Burda	Burda	k1gMnSc1	Burda
Jak	jak	k8xS	jak
básníci	básník	k1gMnPc1	básník
přicházejí	přicházet	k5eAaImIp3nP	přicházet
o	o	k7c4	o
iluze	iluze	k1gFnPc4	iluze
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
-	-	kIx~	-
doc.	doc.	kA	doc.
Zajíc	Zajíc	k1gMnSc1	Zajíc
Vesničko	vesnička	k1gFnSc5	vesnička
má	mít	k5eAaImIp3nS	mít
středisková	střediskový	k2eAgFnSc1d1	středisková
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
-	-	kIx~	-
malíř	malíř	k1gMnSc1	malíř
Evžen	Evžen	k1gMnSc1	Evžen
Ryba	Ryba	k1gMnSc1	Ryba
Jako	jako	k8xS	jako
jed	jed	k1gInSc1	jed
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
-	-	kIx~	-
Ing.	ing.	kA	ing.
Pavel	Pavel	k1gMnSc1	Pavel
Hnyk	Hnyk	k1gMnSc1	Hnyk
Rohy	roh	k1gInPc1	roh
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
-	-	kIx~	-
vypravěč	vypravěč	k1gMnSc1	vypravěč
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Rybník	rybník	k1gInSc4	rybník
Narostl	narůst	k5eAaPmAgMnS	narůst
mu	on	k3xPp3gMnSc3	on
hřebínek	hřebínek	k1gInSc1	hřebínek
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
-	-	kIx~	-
vypravěč	vypravěč	k1gMnSc1	vypravěč
Tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
chleba	chléb	k1gInSc2	chléb
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
Souček	Souček	k1gMnSc1	Souček
Akumulátor	akumulátor	k1gInSc4	akumulátor
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
Fišárek	Fišárek	k1gMnSc1	Fišárek
Kolja	Kolja	k1gMnSc1	Kolja
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
František	františek	k1gInSc1	františek
Louka	louka	k1gFnSc1	louka
Lotrando	Lotrando	k1gMnSc1	Lotrando
a	a	k8xC	a
Zubejda	Zubejda	k1gMnSc1	Zubejda
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
-	-	kIx~	-
vypravěč	vypravěč	k1gMnSc1	vypravěč
Talár	talár	k1gInSc1	talár
a	a	k8xC	a
ptačí	ptačí	k2eAgInSc1d1	ptačí
zob	zob	k1gInSc1	zob
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
-	-	kIx~	-
Morgenhall	Morgenhall	k1gMnSc1	Morgenhall
Tatínek	tatínek	k1gMnSc1	tatínek
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g />
.	.	kIx.	.
</s>
<s>
Vratné	vratný	k2eAgFnPc1d1	vratná
lahve	lahev	k1gFnPc1	lahev
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Tkaloun	tkaloun	k1gInSc4	tkaloun
Kuky	kuka	k1gFnSc2	kuka
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kapitán	kapitán	k1gMnSc1	kapitán
von	von	k1gInSc4	von
Hergot	hergot	k0	hergot
Tři	tři	k4xCgMnPc4	tři
bratři	bratr	k1gMnPc1	bratr
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
vypravěč	vypravěč	k1gMnSc1	vypravěč
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k1gMnSc1	bos
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
-	-	kIx~	-
učitel	učitel	k1gMnSc1	učitel
Domov	domov	k1gInSc1	domov
aneb	aneb	k?	aneb
Kam	kam	k6eAd1	kam
směřuje	směřovat	k5eAaImIp3nS	směřovat
naše	náš	k3xOp1gFnSc1	náš
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
originále	originál	k1gInSc6	originál
Home	Hom	k1gFnSc2	Hom
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
vypravěč	vypravěč	k1gMnSc1	vypravěč
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
MALÁK	MALÁK	kA	MALÁK
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
malíři	malíř	k1gMnSc3	malíř
nestačí	stačit	k5eNaBmIp3nS	stačit
plnovous	plnovous	k1gInSc4	plnovous
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
dětské	dětský	k2eAgFnSc2d1	dětská
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
23	[number]	k4	23
s.	s.	k?	s.
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Lochneska	lochneska	k1gFnSc1	lochneska
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Dilia	Dilia	k1gFnSc1	Dilia
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
68	[number]	k4	68
s.	s.	k?	s.
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
veteráni	veterán	k1gMnPc1	veterán
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
140	[number]	k4	140
s.	s.	k?	s.
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Tatínku	tatínek	k1gMnSc5	tatínek
<g/>
,	,	kIx,	,
ta	ten	k3xDgNnPc1	ten
se	se	k3xPyFc4	se
ti	ty	k3xPp2nSc3	ty
povedla	povést	k5eAaPmAgFnS	povést
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
58	[number]	k4	58
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
306	[number]	k4	306
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
princezně	princezna	k1gFnSc6	princezna
Jasněnce	Jasněnka	k1gFnSc6	Jasněnka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Luftbaba	Luftbaba	k1gFnSc1	Luftbaba
zaklela	zaklít	k5eAaPmAgFnS	zaklít
v	v	k7c4	v
želvu	želva	k1gFnSc4	želva
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
Olda	Olda	k1gMnSc1	Olda
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
Aničkám	Anička	k1gFnPc3	Anička
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Akumulátor	akumulátor	k1gInSc1	akumulátor
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mht	Mht	k1gMnSc1	Mht
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
10	[number]	k4	10
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85861	[number]	k4	85861
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Radovanovy	Radovanův	k2eAgFnPc1d1	Radovanova
radovánky	radovánka	k1gFnPc1	radovánka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
125	[number]	k4	125
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7176	[number]	k4	7176
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Dělání	dělání	k1gNnSc6	dělání
všechny	všechen	k3xTgInPc4	všechen
smutky	smutek	k1gInPc4	smutek
zahání	zahánět	k5eAaImIp3nP	zahánět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
63	[number]	k4	63
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
476	[number]	k4	476
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
UHLÍŘ	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
nutno	nutno	k6eAd1	nutno
<g/>
...	...	k?	...
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Primus	primus	k1gMnSc1	primus
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
47	[number]	k4	47
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85625	[number]	k4	85625
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Jaké	jaký	k3yIgNnSc1	jaký
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
v	v	k7c6	v
Čudu	čud	k1gInSc6	čud
<g/>
:	:	kIx,	:
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
písničky	písnička	k1gFnSc2	písnička
a	a	k8xC	a
povídky	povídka	k1gFnSc2	povídka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
od	od	k7c2	od
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
92	[number]	k4	92
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7200	[number]	k4	7200
<g/>
-	-	kIx~	-
<g/>
776	[number]	k4	776
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
UHLÍŘ	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
93	[number]	k4	93
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7200	[number]	k4	7200
<g/>
-	-	kIx~	-
<g/>
910	[number]	k4	910
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Psí	psí	k2eAgNnSc1d1	psí
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Jiří	Jiří	k1gMnSc1	Jiří
Žáček	Žáček	k1gMnSc1	Žáček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tramtárii	Tramtárii	k?	Tramtárii
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
hej	hej	k6eAd1	hej
-	-	kIx~	-
básničky	básnička	k1gFnPc1	básnička
pro	pro	k7c4	pro
zasmání	zasmání	k1gNnSc4	zasmání
a	a	k8xC	a
dobrou	dobrý	k2eAgFnSc4d1	dobrá
náladu	nálada	k1gFnSc4	nálada
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1382	[number]	k4	1382
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
UHLÍŘ	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
myš	myš	k1gFnSc4	myš
Lenku	Lenka	k1gFnSc4	Lenka
-	-	kIx~	-
zpěv	zpěv	k1gInSc4	zpěv
a	a	k8xC	a
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
93	[number]	k4	93
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
320	[number]	k4	320
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Radovanovy	Radovanův	k2eAgFnPc4d1	Radovanova
radovánky	radovánka	k1gFnPc4	radovánka
-	-	kIx~	-
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
Radovan	Radovan	k1gMnSc1	Radovan
naučil	naučit	k5eAaPmAgMnS	naučit
hvízdat	hvízdat	k5eAaImF	hvízdat
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
příhody	příhoda	k1gFnPc4	příhoda
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
63	[number]	k4	63
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
542	[number]	k4	542
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Radovanovy	Radovanův	k2eAgFnPc4d1	Radovanova
radovánky	radovánka	k1gFnPc4	radovánka
-	-	kIx~	-
jak	jak	k8xC	jak
vyzrát	vyzrát	k5eAaPmF	vyzrát
na	na	k7c4	na
motýly	motýl	k1gMnPc4	motýl
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
příhody	příhoda	k1gFnPc4	příhoda
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
58	[number]	k4	58
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
543	[number]	k4	543
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
UHLÍŘ	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvník	zpěvník	k1gInSc1	zpěvník
-	-	kIx~	-
největší	veliký	k2eAgInPc1d3	veliký
hity	hit	k1gInPc1	hit
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
122	[number]	k4	122
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
666	[number]	k4	666
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
UHLÍŘ	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Zpívání	zpívání	k1gNnSc1	zpívání
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
1	[number]	k4	1
leporelo	leporelo	k1gNnSc1	leporelo
s	s	k7c7	s
piánkem	piánek	k1gInSc7	piánek
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
660	[number]	k4	660
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Buřtík	buřtík	k1gInSc1	buřtík
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
Špejlička	špejlička	k1gFnSc1	špejlička
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
60	[number]	k4	60
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2681	[number]	k4	2681
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Krávy	kráva	k1gFnPc1	kráva
<g/>
,	,	kIx,	,
krávy	kráva	k1gFnPc1	kráva
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
1027	[number]	k4	1027
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Mravenčí	mravenčí	k2eAgFnSc1d1	mravenčí
ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
1030	[number]	k4	1030
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Šupy	šupa	k1gFnPc1	šupa
dupy	dup	k1gInPc1	dup
dup	dup	k1gInSc1	dup
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
písničky	písnička	k1gFnPc1	písnička
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
8	[number]	k4	8
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
1276	[number]	k4	1276
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
UHLÍŘ	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
pěkné	pěkný	k2eAgNnSc1d1	pěkné
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
68	[number]	k4	68
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
1252	[number]	k4	1252
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
UHLÍŘ	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
349	[number]	k4	349
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Buřtík	buřtík	k1gInSc1	buřtík
a	a	k8xC	a
Špejlička	špejlička	k1gFnSc1	špejlička
-	-	kIx~	-
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Žatce	Žatec	k1gInSc2	Žatec
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
44	[number]	k4	44
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4454	[number]	k4	4454
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
ŠEBÁNEK	Šebánek	k1gMnSc1	Šebánek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Jára	Jára	k1gMnSc1	Jára
da	da	k?	da
Cimrman	Cimrman	k1gMnSc1	Cimrman
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
179	[number]	k4	179
s.	s.	k?	s.
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
191	[number]	k4	191
s.	s.	k?	s.
TAUSSIG	TAUSSIG	kA	TAUSSIG
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
smích	smích	k1gInSc1	smích
Ladislava	Ladislav	k1gMnSc2	Ladislav
Smoljaka	Smoljak	k1gMnSc2	Smoljak
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Československý	československý	k2eAgInSc1d1	československý
filmový	filmový	k2eAgInSc1d1	filmový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
JUST	just	k6eAd1	just
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
KUBÍKOVÁ	Kubíková	k1gFnSc1	Kubíková
<g/>
,	,	kIx,	,
Pavlína	Pavlína	k1gFnSc1	Pavlína
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
-	-	kIx~	-
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Československý	československý	k2eAgInSc1d1	československý
filmový	filmový	k2eAgInSc1d1	filmový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1	filmová
komedie	komedie	k1gFnPc1	komedie
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
a	a	k8xC	a
Ladislava	Ladislav	k1gMnSc2	Ladislav
Smoljaka	Smoljak	k1gMnSc2	Smoljak
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
:	:	kIx,	:
Kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
191	[number]	k4	191
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7031	[number]	k4	7031
<g/>
-	-	kIx~	-
<g/>
504	[number]	k4	504
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Akt	akt	k1gInSc1	akt
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
80	[number]	k4	80
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
ztráty	ztráta	k1gFnSc2	ztráta
třídní	třídní	k2eAgFnSc2d1	třídní
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
56	[number]	k4	56
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Hospoda	Hospoda	k?	Hospoda
na	na	k7c6	na
mýtince	mýtinka	k1gFnSc6	mýtinka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
64	[number]	k4	64
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
salónním	salónní	k2eAgMnSc6d1	salónní
coupé	coupý	k2eAgInPc4d1	coupý
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
64	[number]	k4	64
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Němý	němý	k2eAgMnSc1d1	němý
Bobeš	Bobeš	k1gMnSc1	Bobeš
aneb	aneb	k?	aneb
Český	český	k2eAgMnSc1d1	český
Tarzan	Tarzan	k1gMnSc1	Tarzan
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
64	[number]	k4	64
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
46	[number]	k4	46
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Cimrman	Cimrman	k1gMnSc1	Cimrman
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
56	[number]	k4	56
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Široký	Široký	k1gMnSc1	Široký
a	a	k8xC	a
Krátkozraký	krátkozraký	k2eAgMnSc1d1	krátkozraký
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
64	[number]	k4	64
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Posel	posel	k1gMnSc1	posel
z	z	k7c2	z
Liptákova	Liptákův	k2eAgInSc2d1	Liptákův
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
72	[number]	k4	72
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
41	[number]	k4	41
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Lijavec	lijavec	k1gInSc1	lijavec
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
64	[number]	k4	64
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Dobytí	dobytí	k1gNnSc1	dobytí
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
Čechem	Čech	k1gMnSc7	Čech
Karlem	Karel	k1gMnSc7	Karel
Němcem	Němec	k1gMnSc7	Němec
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
74	[number]	k4	74
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
52	[number]	k4	52
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Blaník	Blaník	k1gInSc1	Blaník
-	-	kIx~	-
jevištní	jevištní	k2eAgFnSc1d1	jevištní
podoba	podoba	k1gFnSc1	podoba
historického	historický	k2eAgInSc2d1	historický
mýtu	mýtus	k1gInSc2	mýtus
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
64	[number]	k4	64
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
TROUSIL	trousit	k5eAaImAgMnS	trousit
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
-	-	kIx~	-
filmová	filmový	k2eAgFnSc1d1	filmová
povídka	povídka	k1gFnSc1	povídka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
138	[number]	k4	138
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901091	[number]	k4	901091
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
ze	z	k7c2	z
Smoljaka	Smoljak	k1gMnSc2	Smoljak
<g/>
,	,	kIx,	,
Svěráka	Svěrák	k1gMnSc2	Svěrák
a	a	k8xC	a
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Exact	Exact	k1gInSc1	Exact
Service	Service	k1gFnSc2	Service
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
251	[number]	k4	251
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901380	[number]	k4	901380
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
RUT	rout	k5eAaImNgMnS	rout
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
140	[number]	k4	140
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
SLOVÁK	Slovák	k1gMnSc1	Slovák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Akumulátor	akumulátor	k1gInSc1	akumulátor
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mht	Mht	k1gMnSc1	Mht
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
122	[number]	k4	122
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85861	[number]	k4	85861
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Záskok	záskok	k1gInSc1	záskok
-	-	kIx~	-
Cimrmanova	Cimrmanův	k2eAgFnSc1d1	Cimrmanova
hra	hra	k1gFnSc1	hra
o	o	k7c6	o
nešťastné	šťastný	k2eNgFnSc6d1	nešťastná
premiéře	premiéra	k1gFnSc6	premiéra
hry	hra	k1gFnSc2	hra
Vlasta	Vlasta	k1gFnSc1	Vlasta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
80	[number]	k4	80
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Vesničko	vesnička	k1gFnSc5	vesnička
má	mít	k5eAaImIp3nS	mít
středisková	střediskový	k2eAgFnSc1d1	středisková
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Primus	primus	k1gMnSc1	primus
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
88	[number]	k4	88
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85625	[number]	k4	85625
<g/>
-	-	kIx~	-
<g/>
49	[number]	k4	49
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
TAUSSIG	TAUSSIG	kA	TAUSSIG
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Kolja	Kolja	k6eAd1	Kolja
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Primus	primus	k1gMnSc1	primus
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
135	[number]	k4	135
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85625	[number]	k4	85625
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Švestka	švestka	k1gFnSc1	švestka
-	-	kIx~	-
jevištní	jevištní	k2eAgInSc1d1	jevištní
sklerotikon	sklerotikon	k1gInSc1	sklerotikon
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
74	[number]	k4	74
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
181	[number]	k4	181
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
JUST	just	k6eAd1	just
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
ze	z	k7c2	z
Smoljaka	Smoljak	k1gMnSc2	Smoljak
<g/>
,	,	kIx,	,
Svěráka	Svěrák	k1gMnSc2	Svěrák
a	a	k8xC	a
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc4	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
První	první	k4xOgFnSc1	první
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Knihcentrum	Knihcentrum	k1gNnSc1	Knihcentrum
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
292	[number]	k4	292
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86054	[number]	k4	86054
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
ŠEBÁNEK	Šebánek	k1gMnSc1	Šebánek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VONDRÁČEK	Vondráček	k1gMnSc1	Vondráček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Vinárna	vinárna	k1gFnSc1	vinárna
U	u	k7c2	u
Pavouka	pavouk	k1gMnSc2	pavouk
-	-	kIx~	-
výbor	výbor	k1gInSc1	výbor
scénářů	scénář	k1gInPc2	scénář
a	a	k8xC	a
textů	text	k1gInPc2	text
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
rozhlasového	rozhlasový	k2eAgInSc2d1	rozhlasový
pořadu	pořad	k1gInSc2	pořad
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
212	[number]	k4	212
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86212	[number]	k4	86212
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Tmavomodrý	tmavomodrý	k2eAgInSc1d1	tmavomodrý
svět	svět	k1gInSc1	svět
-	-	kIx~	-
filmová	filmový	k2eAgFnSc1d1	filmová
povídka	povídka	k1gFnSc1	povídka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Primus	primus	k1gMnSc1	primus
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
146	[number]	k4	146
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86207	[number]	k4	86207
<g/>
-	-	kIx~	-
<g/>
32	[number]	k4	32
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Usměj	usmát	k5eAaPmRp2nS	usmát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
Lízo	Líza	k1gFnSc5	Líza
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Jihlava	Jihlava	k1gFnSc1	Jihlava
:	:	kIx,	:
Listen	listen	k1gInSc1	listen
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
155	[number]	k4	155
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
povídka	povídka	k1gFnSc1	povídka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86526	[number]	k4	86526
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Povídka	povídka	k1gFnSc1	povídka
Natáčecí	natáčecí	k2eAgFnSc2d1	natáčecí
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Vratné	vratný	k2eAgFnPc1d1	vratná
lahve	lahev	k1gFnPc1	lahev
-	-	kIx~	-
literární	literární	k2eAgInSc1d1	literární
filmový	filmový	k2eAgInSc1d1	filmový
scénář	scénář	k1gInSc1	scénář
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
96	[number]	k4	96
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
403	[number]	k4	403
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
106	[number]	k4	106
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
761	[number]	k4	761
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
SMOLJAK	SMOLJAK	kA	SMOLJAK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
RUT	rout	k5eAaImNgMnS	rout
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
a	a	k8xC	a
semináře	seminář	k1gInPc1	seminář
-	-	kIx~	-
úplné	úplný	k2eAgNnSc4d1	úplné
vydání	vydání	k1gNnSc4	vydání
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
565	[number]	k4	565
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
973	[number]	k4	973
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
WEIGEL	WEIGEL	kA	WEIGEL
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
WEIGEL	WEIGEL	kA	WEIGEL
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
hrající	hrající	k2eAgMnSc1d1	hrající
<g/>
,	,	kIx,	,
bdící	bdící	k2eAgMnSc1d1	bdící
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
60	[number]	k4	60
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Literární	literární	k2eAgFnPc1d1	literární
biografie	biografie	k1gFnPc1	biografie
slavných	slavný	k2eAgFnPc2d1	slavná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
1157	[number]	k4	1157
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
povídky	povídka	k1gFnPc1	povídka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
118	[number]	k4	118
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
1233	[number]	k4	1233
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k1gMnSc1	bos
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
92	[number]	k4	92
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
1820	[number]	k4	1820
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
SVĚRÁK	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Filmové	filmový	k2eAgInPc1d1	filmový
příběhy	příběh	k1gInPc1	příběh
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
344	[number]	k4	344
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
2465	[number]	k4	2465
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
