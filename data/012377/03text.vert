<p>
<s>
Modrásek	modrásek	k1gMnSc1	modrásek
černoskvrnný	černoskvrnný	k2eAgMnSc1d1	černoskvrnný
(	(	kIx(	(
<g/>
Phengaris	Phengaris	k1gInSc1	Phengaris
arion	arion	k1gInSc1	arion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
vyskytujícího	vyskytující	k2eAgMnSc4d1	vyskytující
motýla	motýl	k1gMnSc4	motýl
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Phengaris	Phengaris	k1gInSc1	Phengaris
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
vývoj	vývoj	k1gInSc1	vývoj
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
obligátní	obligátní	k2eAgFnSc1d1	obligátní
parazitická	parazitický	k2eAgFnSc1d1	parazitická
myrmekofilie	myrmekofilie	k1gFnSc1	myrmekofilie
<g/>
.	.	kIx.	.
</s>
<s>
Specializuje	specializovat	k5eAaBmIp3nS	specializovat
se	se	k3xPyFc4	se
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
zdroje	zdroj	k1gInPc4	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
na	na	k7c4	na
živnou	živný	k2eAgFnSc4d1	živná
rostlinu	rostlina	k1gFnSc4	rostlina
a	a	k8xC	a
symbiotické	symbiotický	k2eAgInPc4d1	symbiotický
druhy	druh	k1gInPc4	druh
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Palearktický	palearktický	k2eAgInSc1d1	palearktický
druh	druh	k1gInSc1	druh
vyskytující	vyskytující	k2eAgFnSc2d1	vyskytující
se	se	k3xPyFc4	se
od	od	k7c2	od
severu	sever	k1gInSc2	sever
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
přes	přes	k7c4	přes
jih	jih	k1gInSc4	jih
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
,	,	kIx,	,
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
<g/>
,	,	kIx,	,
Západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
Střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Balkán	Balkán	k1gInSc1	Balkán
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
evropského	evropský	k2eAgNnSc2d1	Evropské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Malou	malý	k2eAgFnSc4d1	malá
a	a	k8xC	a
Střední	střední	k2eAgFnSc4d1	střední
Asii	Asie	k1gFnSc4	Asie
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
Sibiř	Sibiř	k1gFnSc4	Sibiř
až	až	k9	až
po	po	k7c6	po
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
byl	být	k5eAaImAgInS	být
úspěšně	úspěšně	k6eAd1	úspěšně
nově	nově	k6eAd1	nově
vysazeny	vysazen	k2eAgFnPc1d1	vysazena
populace	populace	k1gFnPc1	populace
ze	z	k7c2	z
švédského	švédský	k2eAgInSc2d1	švédský
ostrova	ostrov	k1gInSc2	ostrov
Ölandu	Öland	k1gInSc2	Öland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
býval	bývat	k5eAaImAgInS	bývat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
přežívá	přežívat	k5eAaImIp3nS	přežívat
jen	jen	k9	jen
na	na	k7c6	na
hrstce	hrstka	k1gFnSc6	hrstka
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
mikropopulace	mikropopulace	k1gFnPc1	mikropopulace
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hojnější	hojný	k2eAgInPc4d2	hojnější
lze	lze	k6eAd1	lze
jeho	on	k3xPp3gInSc4	on
výskyt	výskyt	k1gInSc4	výskyt
považovat	považovat	k5eAaImF	považovat
jen	jen	k9	jen
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vsetínských	vsetínský	k2eAgInPc6d1	vsetínský
vrších	vrch	k1gInPc6	vrch
a	a	k8xC	a
v	v	k7c6	v
Bílých	bílý	k2eAgInPc6d1	bílý
Karpatech	Karpaty	k1gInPc6	Karpaty
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
modrásek	modrásek	k1gMnSc1	modrásek
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc4	jeho
přední	přední	k2eAgNnPc1d1	přední
křídla	křídlo	k1gNnPc1	křídlo
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rozpětí	rozpětí	k1gNnSc4	rozpětí
35	[number]	k4	35
až	až	k9	až
40	[number]	k4	40
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svrchní	svrchní	k2eAgFnSc6d1	svrchní
straně	strana	k1gFnSc6	strana
křídel	křídlo	k1gNnPc2	křídlo
mají	mít	k5eAaImIp3nP	mít
motýli	motýl	k1gMnPc1	motýl
modravý	modravý	k2eAgInSc4d1	modravý
nádech	nádech	k1gInSc4	nádech
(	(	kIx(	(
<g/>
samci	samec	k1gMnSc3	samec
mnohem	mnohem	k6eAd1	mnohem
silnější	silný	k2eAgFnPc1d2	silnější
<g/>
)	)	kIx)	)
a	a	k8xC	a
spodní	spodní	k2eAgFnSc4d1	spodní
stranu	strana	k1gFnSc4	strana
mají	mít	k5eAaImIp3nP	mít
světle	světle	k6eAd1	světle
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
příbuzných	příbuzný	k2eAgMnPc2d1	příbuzný
modrásků	modrásek	k1gMnPc2	modrásek
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
především	především	k9	především
dvěma	dva	k4xCgNnPc7	dva
řadami	řada	k1gFnPc7	řada
černých	černý	k2eAgFnPc2d1	černá
skvrn	skvrna	k1gFnPc2	skvrna
u	u	k7c2	u
okraje	okraj	k1gInSc2	okraj
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
zadních	zadní	k2eAgNnPc2d1	zadní
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
výraznými	výrazný	k2eAgFnPc7d1	výrazná
skvrnami	skvrna	k1gFnPc7	skvrna
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
straně	strana	k1gFnSc6	strana
předních	přední	k2eAgNnPc2d1	přední
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
svázán	svázat	k5eAaPmNgInS	svázat
s	s	k7c7	s
nehnojenými	hnojený	k2eNgInPc7d1	hnojený
<g/>
,	,	kIx,	,
extenzivně	extenzivně	k6eAd1	extenzivně
spásanými	spásaný	k2eAgFnPc7d1	spásaná
pastvinami	pastvina	k1gFnPc7	pastvina
<g/>
,	,	kIx,	,
vřesovišti	vřesoviště	k1gNnPc7	vřesoviště
a	a	k8xC	a
nízkostébelnými	nízkostébelný	k2eAgFnPc7d1	nízkostébelný
stepmi	step	k1gFnPc7	step
se	s	k7c7	s
slabým	slabý	k2eAgNnSc7d1	slabé
zapojením	zapojení	k1gNnSc7	zapojení
porostů	porost	k1gInPc2	porost
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
s	s	k7c7	s
travnatými	travnatý	k2eAgFnPc7d1	travnatá
plochami	plocha	k1gFnPc7	plocha
s	s	k7c7	s
nízkými	nízký	k2eAgFnPc7d1	nízká
rostlinami	rostlina	k1gFnPc7	rostlina
které	který	k3yQgNnSc1	který
bývají	bývat	k5eAaImIp3nP	bývat
ob	ob	k7c4	ob
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
sesečeny	sesečen	k2eAgInPc4d1	sesečen
nebo	nebo	k8xC	nebo
spaseny	spasen	k2eAgInPc4d1	spasen
<g/>
.	.	kIx.	.
</s>
<s>
Vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
se	se	k3xPyFc4	se
loukám	louka	k1gFnPc3	louka
s	s	k7c7	s
homogenním	homogenní	k2eAgInSc7d1	homogenní
porostem	porost	k1gInSc7	porost
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
sečou	síct	k5eAaImIp3nP	síct
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
vícekrát	vícekrát	k6eAd1	vícekrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
daří	dařit	k5eAaImIp3nS	dařit
na	na	k7c6	na
lokalitách	lokalita	k1gFnPc6	lokalita
s	s	k7c7	s
nedávno	nedávno	k6eAd1	nedávno
ukončenou	ukončený	k2eAgFnSc7d1	ukončená
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
na	na	k7c6	na
svažitých	svažitý	k2eAgNnPc6d1	svažité
členitých	členitý	k2eAgNnPc6d1	členité
místech	místo	k1gNnPc6	místo
situovaných	situovaný	k2eAgFnPc2d1	situovaná
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
nebo	nebo	k8xC	nebo
jihozápadu	jihozápad	k1gInSc2	jihozápad
a	a	k8xC	a
s	s	k7c7	s
jen	jen	k6eAd1	jen
nesouvisle	souvisle	k6eNd1	souvisle
zapojeným	zapojený	k2eAgNnSc7d1	zapojené
bylinným	bylinný	k2eAgNnSc7d1	bylinné
patrem	patro	k1gNnSc7	patro
a	a	k8xC	a
vyvinutým	vyvinutý	k2eAgInSc7d1	vyvinutý
mechovým	mechový	k2eAgInSc7d1	mechový
pokryvem	pokryv	k1gInSc7	pokryv
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
prostředí	prostředí	k1gNnSc1	prostředí
je	být	k5eAaImIp3nS	být
nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
jeho	jeho	k3xOp3gFnPc2	jeho
živných	živný	k2eAgFnPc2d1	živná
rostlin	rostlina	k1gFnPc2	rostlina
mateřídoušky	mateřídouška	k1gFnSc2	mateřídouška
i	i	k8xC	i
dobromysli	dobromysl	k1gFnSc2	dobromysl
obecné	obecný	k2eAgFnSc2d1	obecná
a	a	k8xC	a
pro	pro	k7c4	pro
život	život	k1gInSc4	život
hostitelských	hostitelský	k2eAgFnPc2d1	hostitelská
mravenčích	mravenčí	k2eAgFnPc2d1	mravenčí
kolonií	kolonie	k1gFnPc2	kolonie
rodu	rod	k1gInSc2	rod
Myrmica	Myrmic	k1gInSc2	Myrmic
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
se	se	k3xPyFc4	se
životní	životní	k2eAgFnSc2d1	životní
podmínky	podmínka	k1gFnSc2	podmínka
na	na	k7c6	na
stanovišti	stanoviště	k1gNnSc6	stanoviště
rapidně	rapidně	k6eAd1	rapidně
zhorší	zhoršit	k5eAaPmIp3nS	zhoršit
<g/>
,	,	kIx,	,
modrásek	modrásek	k1gMnSc1	modrásek
černoskvrnný	černoskvrnný	k2eAgMnSc1d1	černoskvrnný
obvykle	obvykle	k6eAd1	obvykle
vyhyne	vyhynout	k5eAaPmIp3nS	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
přesídlit	přesídlit	k5eAaPmF	přesídlit
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
létá	létat	k5eAaImIp3nS	létat
nejdále	daleko	k6eAd3	daleko
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
vylíhnutí	vylíhnutí	k1gNnSc2	vylíhnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
generaci	generace	k1gFnSc4	generace
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
v	v	k7c6	v
podhorských	podhorský	k2eAgFnPc6d1	podhorská
podmínkách	podmínka	k1gFnPc6	podmínka
Západních	západní	k2eAgInPc2d1	západní
Karpat	Karpaty	k1gInPc2	Karpaty
žijí	žít	k5eAaImIp3nP	žít
jeho	jeho	k3xOp3gMnPc1	jeho
dospělci	dospělec	k1gMnPc1	dospělec
od	od	k7c2	od
konce	konec	k1gInSc2	konec
června	červen	k1gInSc2	červen
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
samice	samice	k1gFnPc1	samice
musí	muset	k5eAaImIp3nP	muset
spářit	spářit	k5eAaPmF	spářit
a	a	k8xC	a
naklást	naklást	k5eAaPmF	naklást
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
je	on	k3xPp3gInPc4	on
dává	dávat	k5eAaImIp3nS	dávat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
do	do	k7c2	do
nerozvinutých	rozvinutý	k2eNgFnPc2d1	nerozvinutá
vrcholových	vrcholový	k2eAgFnPc2d1	vrcholová
částí	část	k1gFnPc2	část
květenství	květenství	k1gNnSc2	květenství
živných	živný	k2eAgFnPc2d1	živná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
květenství	květenství	k1gNnSc6	květenství
více	hodně	k6eAd2	hodně
vajíček	vajíčko	k1gNnPc2	vajíčko
dojde	dojít	k5eAaPmIp3nS	dojít
mezi	mezi	k7c7	mezi
housenkami	housenka	k1gFnPc7	housenka
ke	k	k7c3	k
kanibalismu	kanibalismus	k1gInSc3	kanibalismus
a	a	k8xC	a
přežívá	přežívat	k5eAaImIp3nS	přežívat
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Housenka	housenka	k1gFnSc1	housenka
vyžírá	vyžírat	k5eAaImIp3nS	vyžírat
semeníky	semeník	k1gInPc4	semeník
květů	květ	k1gInPc2	květ
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
rychle	rychle	k6eAd1	rychle
prodělá	prodělat	k5eAaPmIp3nS	prodělat
instary	instar	k1gInPc4	instar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třetím	třetí	k4xOgInSc6	třetí
sleze	slézt	k5eAaPmIp3nS	slézt
z	z	k7c2	z
mateřské	mateřský	k2eAgFnSc2d1	mateřská
rostliny	rostlina	k1gFnSc2	rostlina
a	a	k8xC	a
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
začne	začít	k5eAaPmIp3nS	začít
ze	z	k7c2	z
speciální	speciální	k2eAgFnSc2d1	speciální
žlázy	žláza	k1gFnSc2	žláza
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
vylučovat	vylučovat	k5eAaImF	vylučovat
směs	směs	k1gFnSc4	směs
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
láká	lákat	k5eAaImIp3nS	lákat
tak	tak	k6eAd1	tak
mravence	mravenec	k1gMnSc2	mravenec
aby	aby	kYmCp3nP	aby
ji	on	k3xPp3gFnSc4	on
odnesli	odnést	k5eAaPmAgMnP	odnést
do	do	k7c2	do
mraveniště	mraveniště	k1gNnSc2	mraveniště
<g/>
;	;	kIx,	;
jinak	jinak	k6eAd1	jinak
zahyne	zahynout	k5eAaPmIp3nS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
ji	on	k3xPp3gFnSc4	on
adoptuje	adoptovat	k5eAaPmIp3nS	adoptovat
dělnice	dělnice	k1gFnSc1	dělnice
druhu	druh	k1gInSc2	druh
Myrmica	Myrmicum	k1gNnSc2	Myrmicum
sabuleti	sabulet	k5eAaPmF	sabulet
nebo	nebo	k8xC	nebo
Myrmica	Myrmic	k2eAgFnSc1d1	Myrmica
scabrinodis	scabrinodis	k1gFnSc1	scabrinodis
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc7d1	zásadní
podmínkou	podmínka	k1gFnSc7	podmínka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
housenka	housenka	k1gFnSc1	housenka
nebyla	být	k5eNaImAgFnS	být
od	od	k7c2	od
mraveniště	mraveniště	k1gNnSc2	mraveniště
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
m	m	kA	m
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
akční	akční	k2eAgInSc4d1	akční
rádius	rádius	k1gInSc4	rádius
mravenců	mravenec	k1gMnPc2	mravenec
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mraveništi	mraveniště	k1gNnSc6	mraveniště
je	být	k5eAaImIp3nS	být
housenka	housenka	k1gFnSc1	housenka
uložena	uložit	k5eAaPmNgFnS	uložit
do	do	k7c2	do
oddělení	oddělení	k1gNnSc2	oddělení
mezi	mezi	k7c4	mezi
larvy	larva	k1gFnPc4	larva
<g/>
,	,	kIx,	,
mravenci	mravenec	k1gMnPc1	mravenec
ji	on	k3xPp3gFnSc4	on
nekrmí	krmit	k5eNaImIp3nP	krmit
ale	ale	k8xC	ale
chrání	chránit	k5eAaImIp3nP	chránit
před	před	k7c7	před
případnými	případný	k2eAgMnPc7d1	případný
parazity	parazit	k1gMnPc7	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Housenka	housenka	k1gFnSc1	housenka
se	se	k3xPyFc4	se
sama	sám	k3xTgMnSc4	sám
živí	živit	k5eAaImIp3nS	živit
larvami	larva	k1gFnPc7	larva
a	a	k8xC	a
kuklami	kukla	k1gFnPc7	kukla
svých	svůj	k3xOyFgMnPc2	svůj
hostitelů	hostitel	k1gMnPc2	hostitel
pro	pro	k7c4	pro
které	který	k3yIgNnSc4	který
produkuje	produkovat	k5eAaImIp3nS	produkovat
medovici	medovice	k1gFnSc4	medovice
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
mšice	mšice	k1gFnSc1	mšice
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
sežere	sežrat	k5eAaPmIp3nS	sežrat
asi	asi	k9	asi
230	[number]	k4	230
potenciálních	potenciální	k2eAgMnPc2d1	potenciální
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
housenek	housenka	k1gFnPc2	housenka
mraveniště	mraveniště	k1gNnSc2	mraveniště
úplně	úplně	k6eAd1	úplně
zlikviduje	zlikvidovat	k5eAaPmIp3nS	zlikvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
housenka	housenka	k1gFnSc1	housenka
společně	společně	k6eAd1	společně
s	s	k7c7	s
mravenci	mravenec	k1gMnPc7	mravenec
hibernuje	hibernovat	k5eAaBmIp3nS	hibernovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
léta	léto	k1gNnSc2	léto
se	se	k3xPyFc4	se
zakuklí	zakuklit	k5eAaPmIp3nS	zakuklit
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
z	z	k7c2	z
kukly	kukla	k1gFnSc2	kukla
vylíhne	vylíhnout	k5eAaPmIp3nS	vylíhnout
imago	imago	k6eAd1	imago
které	který	k3yRgNnSc1	který
musí	muset	k5eAaImIp3nS	muset
před	před	k7c7	před
mravenci	mravenec	k1gMnPc7	mravenec
utéct	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
opouštění	opouštění	k1gNnSc2	opouštění
mraveniště	mraveniště	k1gNnSc2	mraveniště
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
šupiny	šupina	k1gFnSc2	šupina
které	který	k3yIgNnSc1	který
ho	on	k3xPp3gMnSc4	on
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
útočícími	útočící	k2eAgMnPc7d1	útočící
mravenci	mravenec	k1gMnPc7	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
o	o	k7c4	o
málo	málo	k1gNnSc4	málo
dříve	dříve	k6eAd2	dříve
před	před	k7c7	před
samicemi	samice	k1gFnPc7	samice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Housenku	housenka	k1gFnSc4	housenka
modráska	modrásek	k1gMnSc2	modrásek
černoskvrnného	černoskvrnný	k2eAgNnSc2d1	černoskvrnný
přijmou	přijmout	k5eAaPmIp3nP	přijmout
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
hnízda	hnízdo	k1gNnSc2	hnízdo
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
mravenců	mravenec	k1gMnPc2	mravenec
rodu	rod	k1gInSc2	rod
Myrmica	Myrmic	k1gInSc2	Myrmic
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
značné	značný	k2eAgInPc4d1	značný
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
počtech	počet	k1gInPc6	počet
přežití	přežití	k1gNnSc2	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohé	k1gNnPc1	mnohé
mraveniště	mraveniště	k1gNnSc2	mraveniště
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
málo	málo	k6eAd1	málo
početná	početný	k2eAgFnSc1d1	početná
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
uživí	uživit	k5eAaPmIp3nS	uživit
jednu	jeden	k4xCgFnSc4	jeden
housenku	housenka	k1gFnSc4	housenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Modrásek	modrásek	k1gMnSc1	modrásek
černoskvrnný	černoskvrnný	k2eAgMnSc1d1	černoskvrnný
je	být	k5eAaImIp3nS	být
celoevropsky	celoevropsky	k6eAd1	celoevropsky
ustupující	ustupující	k2eAgInSc1d1	ustupující
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Tuzemský	tuzemský	k2eAgInSc4d1	tuzemský
výskyt	výskyt	k1gInSc4	výskyt
tohoto	tento	k3xDgMnSc2	tento
motýla	motýl	k1gMnSc2	motýl
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zcela	zcela	k6eAd1	zcela
hojného	hojný	k2eAgNnSc2d1	hojné
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
do	do	k7c2	do
několika	několik	k4yIc2	několik
drobných	drobný	k2eAgFnPc2d1	drobná
populací	populace	k1gFnPc2	populace
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
počtu	počet	k1gInSc6	počet
lokalit	lokalita	k1gFnPc2	lokalita
v	v	k7c6	v
podhorských	podhorský	k2eAgFnPc6d1	podhorská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Světlou	světlý	k2eAgFnSc7d1	světlá
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
některá	některý	k3yIgNnPc4	některý
horská	horský	k2eAgNnPc4d1	horské
údolí	údolí	k1gNnPc4	údolí
v	v	k7c6	v
Karpatech	Karpaty	k1gInPc6	Karpaty
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Moravy	Morava	k1gFnSc2	Morava
kde	kde	k6eAd1	kde
v	v	k7c6	v
CHKO	CHKO	kA	CHKO
Beskydy	Beskyd	k1gInPc4	Beskyd
a	a	k8xC	a
CHKO	CHKO	kA	CHKO
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
ještě	ještě	k9	ještě
přežívají	přežívat	k5eAaImIp3nP	přežívat
životaschopné	životaschopný	k2eAgFnPc1d1	životaschopná
populace	populace	k1gFnPc1	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
tohoto	tento	k3xDgInSc2	tento
burcujícího	burcující	k2eAgInSc2d1	burcující
jevu	jev	k1gInSc2	jev
jsou	být	k5eAaImIp3nP	být
hluboké	hluboký	k2eAgFnPc1d1	hluboká
změny	změna	k1gFnPc1	změna
ve	v	k7c6	v
využívání	využívání	k1gNnSc6	využívání
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Intenzifikací	intenzifikace	k1gFnSc7	intenzifikace
zemědělství	zemědělství	k1gNnSc2	zemědělství
s	s	k7c7	s
nadužíváním	nadužívání	k1gNnSc7	nadužívání
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
,	,	kIx,	,
melioracemi	meliorace	k1gFnPc7	meliorace
a	a	k8xC	a
získáváním	získávání	k1gNnSc7	získávání
orné	orný	k2eAgFnSc2d1	orná
půdy	půda	k1gFnSc2	půda
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
luk	louka	k1gFnPc2	louka
<g/>
,	,	kIx,	,
pastvin	pastvina	k1gFnPc2	pastvina
a	a	k8xC	a
ladem	lad	k1gInSc7	lad
ležících	ležící	k2eAgInPc2d1	ležící
pozemků	pozemek	k1gInPc2	pozemek
i	i	k9	i
nepromyšleným	promyšlený	k2eNgNnSc7d1	nepromyšlené
zalesňováním	zalesňování	k1gNnSc7	zalesňování
byli	být	k5eAaImAgMnP	být
dříve	dříve	k6eAd2	dříve
hojní	hojný	k2eAgMnPc1d1	hojný
živočichové	živočich	k1gMnPc1	živočich
zbavení	zbavení	k1gNnSc2	zbavení
svých	svůj	k3xOyFgInPc2	svůj
přirozených	přirozený	k2eAgInPc2d1	přirozený
biotopů	biotop	k1gInPc2	biotop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
modráska	modrásek	k1gMnSc2	modrásek
je	být	k5eAaImIp3nS	být
chráněný	chráněný	k2eAgInSc4d1	chráněný
legislativou	legislativa	k1gFnSc7	legislativa
EU	EU	kA	EU
i	i	k8xC	i
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
začleněn	začlenit	k5eAaPmNgMnS	začlenit
do	do	k7c2	do
Bernské	bernský	k2eAgFnSc2d1	Bernská
úmluvy	úmluva	k1gFnSc2	úmluva
(	(	kIx(	(
<g/>
přílohy	příloha	k1gFnSc2	příloha
č.	č.	k?	č.
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
IUCN	IUCN	kA	IUCN
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
(	(	kIx(	(
<g/>
NT	NT	kA	NT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
"	"	kIx"	"
<g/>
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
ČR	ČR	kA	ČR
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
shodně	shodně	k6eAd1	shodně
jako	jako	k9	jako
ve	v	k7c6	v
"	"	kIx"	"
<g/>
vyhlášce	vyhláška	k1gFnSc6	vyhláška
MŽP	MŽP	kA	MŽP
ČR	ČR	kA	ČR
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
vyhl	vyhla	k1gFnPc2	vyhla
<g/>
.	.	kIx.	.
č.	č.	k?	č.
175	[number]	k4	175
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
(	(	kIx(	(
<g/>
CR	cr	k0	cr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
modrásek	modrásek	k1gMnSc1	modrásek
černoskvrnný	černoskvrnný	k2eAgMnSc1d1	černoskvrnný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
