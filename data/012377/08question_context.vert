<s>
Modrásek	modrásek	k1gMnSc1
černoskvrnný	černoskvrnný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Phengaris	Phengaris	k1gInSc1
arion	arion	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
vzácně	vzácně	k6eAd1
se	se	k3xPyFc4
vyskytujícího	vyskytující	k2eAgMnSc4d1
motýla	motýl	k1gMnSc4
z	z	k7c2
rodu	rod	k1gInSc2
Phengaris	Phengaris	k1gInSc1
který	který	k3yQgInSc1
má	mít	k5eAaImIp3nS
komplikovaný	komplikovaný	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
označovaný	označovaný	k2eAgInSc1d1
jako	jako	k8xC
obligátní	obligátní	k2eAgFnSc1d1
parazitická	parazitický	k2eAgFnSc1d1
myrmekofilie	myrmekofilie	k1gFnSc1
<g/>
.	.	kIx.
</s>