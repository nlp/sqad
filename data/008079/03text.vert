<s>
Nealkoholické	alkoholický	k2eNgNnSc1d1	nealkoholické
pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
kvašeného	kvašený	k2eAgInSc2d1	kvašený
nápoje	nápoj	k1gInSc2	nápoj
z	z	k7c2	z
obilného	obilný	k2eAgInSc2d1	obilný
sladu	slad	k1gInSc2	slad
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
nebo	nebo	k8xC	nebo
žádným	žádný	k3yNgInSc7	žádný
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
přitom	přitom	k6eAd1	přitom
nepanuje	panovat	k5eNaImIp3nS	panovat
jednota	jednota	k1gFnSc1	jednota
v	v	k7c4	v
pojmenování	pojmenování	k1gNnSc4	pojmenování
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
piva	pivo	k1gNnSc2	pivo
ani	ani	k8xC	ani
ve	v	k7c6	v
stanovení	stanovení	k1gNnSc6	stanovení
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
pivem	pivo	k1gNnSc7	pivo
alkoholickým	alkoholický	k2eAgNnSc7d1	alkoholické
a	a	k8xC	a
nealkoholickým	alkoholický	k2eNgNnSc7d1	nealkoholické
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
non-alcoholic	nonlcoholice	k1gFnPc2	non-alcoholice
beer	beer	k1gInSc1	beer
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
EU	EU	kA	EU
"	"	kIx"	"
<g/>
alcohol-free	alcoholreat	k5eAaPmIp3nS	alcohol-freat
beer	beer	k1gMnSc1	beer
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
však	však	k9	však
nápoj	nápoj	k1gInSc4	nápoj
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
až	až	k9	až
0,5	[number]	k4	0,5
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
nazýván	nazývat	k5eAaImNgInS	nazývat
nealkoholickým	alkoholický	k2eNgInSc7d1	nealkoholický
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
no	no	k9	no
alcohol	alcohol	k1gInSc1	alcohol
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
alcohol-free	alcoholree	k1gFnSc1	alcohol-free
<g/>
"	"	kIx"	"
označují	označovat	k5eAaImIp3nP	označovat
pouze	pouze	k6eAd1	pouze
piva	pivo	k1gNnSc2	pivo
s	s	k7c7	s
objemem	objem	k1gInSc7	objem
alkoholu	alkohol	k1gInSc2	alkohol
menším	menšit	k5eAaImIp1nS	menšit
než	než	k8xS	než
0,05	[number]	k4	0,05
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Piva	pivo	k1gNnPc1	pivo
s	s	k7c7	s
objemovým	objemový	k2eAgInSc7d1	objemový
procentem	procent	k1gInSc7	procent
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0,05	[number]	k4	0,05
%	%	kIx~	%
–	–	k?	–
0,5	[number]	k4	0,5
%	%	kIx~	%
nesou	nést	k5eAaImIp3nP	nést
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
dealcoholised	dealcoholised	k1gInSc1	dealcoholised
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
islámských	islámský	k2eAgFnPc6d1	islámská
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
důsledně	důsledně	k6eAd1	důsledně
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nealkoholická	alkoholický	k2eNgNnPc1d1	nealkoholické
piva	pivo	k1gNnPc1	pivo
neobsahovala	obsahovat	k5eNaImAgNnP	obsahovat
absolutně	absolutně	k6eAd1	absolutně
žádný	žádný	k3yNgInSc4	žádný
alkohol	alkohol	k1gInSc4	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
platí	platit	k5eAaImIp3nS	platit
evropský	evropský	k2eAgInSc1d1	evropský
úzus	úzus	k1gInSc1	úzus
a	a	k8xC	a
jako	jako	k8xS	jako
nealkoholické	alkoholický	k2eNgNnSc1d1	nealkoholické
pivo	pivo	k1gNnSc1	pivo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
označen	označit	k5eAaPmNgInS	označit
sladový	sladový	k2eAgInSc1d1	sladový
nápoj	nápoj	k1gInSc1	nápoj
s	s	k7c7	s
objemem	objem	k1gInSc7	objem
alkoholu	alkohol	k1gInSc2	alkohol
menším	menšit	k5eAaImIp1nS	menšit
než	než	k8xS	než
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
neutrálního	neutrální	k2eAgInSc2d1	neutrální
"	"	kIx"	"
<g/>
nealkoholického	alkoholický	k2eNgInSc2d1	nealkoholický
pivo	pivo	k1gNnSc1	pivo
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
také	také	k9	také
se	s	k7c7	s
zkráceným	zkrácený	k2eAgNnSc7d1	zkrácené
pojmenováním	pojmenování	k1gNnSc7	pojmenování
"	"	kIx"	"
<g/>
nealko	nealko	k1gNnSc1	nealko
pivo	pivo	k1gNnSc1	pivo
<g/>
"	"	kIx"	"
či	či	k8xC	či
s	s	k7c7	s
názvy	název	k1gInPc7	název
vzniklými	vzniklý	k2eAgInPc7d1	vzniklý
synekdochickým	synekdochický	k2eAgNnSc7d1	synekdochický
přenesením	přenesení	k1gNnSc7	přenesení
názvů	název	k1gInPc2	název
značek	značka	k1gFnPc2	značka
(	(	kIx(	(
<g/>
Pito	pit	k2eAgNnSc1d1	pito
<g/>
,	,	kIx,	,
Birell	Birell	k1gInSc1	Birell
<g/>
)	)	kIx)	)
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
nápoje	nápoj	k1gInPc4	nápoj
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
pito	pit	k2eAgNnSc1d1	pito
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
birell	birelnout	k5eAaPmAgMnS	birelnout
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvním	první	k4xOgInPc3	první
pokusům	pokus	k1gInPc3	pokus
o	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
nealkoholického	alkoholický	k2eNgNnSc2d1	nealkoholické
piva	pivo	k1gNnSc2	pivo
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
nápoj	nápoj	k1gInSc4	nápoj
i	i	k9	i
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvod	důvod	k1gInSc4	důvod
nemohou	moct	k5eNaImIp3nP	moct
či	či	k8xC	či
nechtějí	chtít	k5eNaImIp3nP	chtít
pít	pít	k5eAaImF	pít
alkohol	alkohol	k1gInSc4	alkohol
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
technologové	technolog	k1gMnPc1	technolog
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vesměs	vesměs	k6eAd1	vesměs
stejnou	stejný	k2eAgFnSc4d1	stejná
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
:	:	kIx,	:
odstraněním	odstranění	k1gNnSc7	odstranění
alkoholu	alkohol	k1gInSc2	alkohol
z	z	k7c2	z
tradičního	tradiční	k2eAgNnSc2d1	tradiční
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
alkoholického	alkoholický	k2eAgNnSc2d1	alkoholické
<g/>
)	)	kIx)	)
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
tzv.	tzv.	kA	tzv.
vakuová	vakuový	k2eAgFnSc1d1	vakuová
destilace	destilace	k1gFnSc1	destilace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
inventář	inventář	k1gInSc1	inventář
výrobních	výrobní	k2eAgInPc2d1	výrobní
postupů	postup	k1gInPc2	postup
užívaných	užívaný	k2eAgInPc2d1	užívaný
při	při	k7c6	při
produkci	produkce	k1gFnSc6	produkce
nealkoholického	alkoholický	k2eNgNnSc2d1	nealkoholické
piva	pivo	k1gNnSc2	pivo
značně	značně	k6eAd1	značně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gMnPc2	jeho
konzumentů	konzument	k1gMnPc2	konzument
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
současných	současný	k2eAgFnPc2d1	současná
pivovarnických	pivovarnický	k2eAgFnPc2d1	pivovarnická
společností	společnost	k1gFnPc2	společnost
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
produktovém	produktový	k2eAgNnSc6d1	produktové
portfoliu	portfolio	k1gNnSc6	portfolio
nějaký	nějaký	k3yIgInSc1	nějaký
nápoj	nápoj	k1gInSc1	nápoj
na	na	k7c6	na
pivní	pivní	k2eAgFnSc6d1	pivní
bázi	báze	k1gFnSc6	báze
s	s	k7c7	s
minimálním	minimální	k2eAgInSc7d1	minimální
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
často	často	k6eAd1	často
také	také	k9	také
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
(	(	kIx(	(
<g/>
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
či	či	k8xC	či
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
utajovanou	utajovaný	k2eAgFnSc4d1	utajovaná
<g/>
)	)	kIx)	)
technologii	technologie	k1gFnSc4	technologie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
výrobě	výroba	k1gFnSc6	výroba
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
lze	lze	k6eAd1	lze
vyrobit	vyrobit	k5eAaPmF	vyrobit
nealkoholické	alkoholický	k2eNgNnSc4d1	nealkoholické
pivo	pivo	k1gNnSc4	pivo
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
vzato	vzat	k2eAgNnSc1d1	vzato
se	se	k3xPyFc4	se
však	však	k9	však
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
typy	typ	k1gInPc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
se	se	k3xPyFc4	se
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
alkohol	alkohol	k1gInSc1	alkohol
z	z	k7c2	z
piva	pivo	k1gNnSc2	pivo
vyrobeného	vyrobený	k2eAgNnSc2d1	vyrobené
tradiční	tradiční	k2eAgNnSc1d1	tradiční
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
už	už	k6eAd1	už
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
počátku	počátek	k1gInSc6	počátek
zvolí	zvolit	k5eAaPmIp3nS	zvolit
taková	takový	k3xDgFnSc1	takový
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vzniku	vznik	k1gInSc3	vznik
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
alkoholu	alkohol	k1gInSc2	alkohol
zabrání	zabránit	k5eAaPmIp3nS	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
metodám	metoda	k1gFnPc3	metoda
prvního	první	k4xOgInSc2	první
typu	typ	k1gInSc2	typ
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
různé	různý	k2eAgInPc4d1	různý
způsoby	způsob	k1gInPc4	způsob
destilace	destilace	k1gFnSc2	destilace
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
vakuové	vakuový	k2eAgFnPc1d1	vakuová
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
alkohol	alkohol	k1gInSc1	alkohol
je	být	k5eAaImIp3nS	být
odpařován	odpařovat	k5eAaImNgInS	odpařovat
za	za	k7c2	za
sníženého	snížený	k2eAgInSc2d1	snížený
tlaku	tlak	k1gInSc2	tlak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
reverzní	reverzní	k2eAgFnSc1d1	reverzní
osmóza	osmóza	k1gFnSc1	osmóza
(	(	kIx(	(
<g/>
pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
vháněno	vhánět	k5eAaImNgNnS	vhánět
na	na	k7c4	na
speciální	speciální	k2eAgFnSc4d1	speciální
membránu	membrána	k1gFnSc4	membrána
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
niž	jenž	k3xRgFnSc4	jenž
projdou	projít	k5eAaPmIp3nP	projít
pouze	pouze	k6eAd1	pouze
molekuly	molekula	k1gFnPc4	molekula
ethanolu	ethanol	k1gInSc2	ethanol
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
posléze	posléze	k6eAd1	posléze
třeba	třeba	k6eAd1	třeba
do	do	k7c2	do
piva	pivo	k1gNnSc2	pivo
doplnit	doplnit	k5eAaPmF	doplnit
<g/>
)	)	kIx)	)
či	či	k8xC	či
dialýza	dialýza	k1gFnSc1	dialýza
(	(	kIx(	(
<g/>
alkohol	alkohol	k1gInSc1	alkohol
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
piva	pivo	k1gNnSc2	pivo
odváděn	odváděn	k2eAgMnSc1d1	odváděn
na	na	k7c6	na
principu	princip	k1gInSc6	princip
rozdílné	rozdílný	k2eAgFnSc2d1	rozdílná
koncentrace	koncentrace	k1gFnSc2	koncentrace
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
výrobní	výrobní	k2eAgInPc4d1	výrobní
postupy	postup	k1gInPc4	postup
druhého	druhý	k4xOgInSc2	druhý
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
zkvašování	zkvašování	k1gNnSc1	zkvašování
sladiny	sladina	k1gFnSc2	sladina
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
obsahem	obsah	k1gInSc7	obsah
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
smíchávání	smíchávání	k1gNnSc2	smíchávání
piva	pivo	k1gNnSc2	pivo
s	s	k7c7	s
nezkvašenou	zkvašený	k2eNgFnSc7d1	zkvašený
sladinou	sladina	k1gFnSc7	sladina
či	či	k8xC	či
mladinou	mladina	k1gFnSc7	mladina
(	(	kIx(	(
<g/>
de	de	k?	de
facto	facto	k1gNnSc1	facto
"	"	kIx"	"
<g/>
ředění	ředění	k1gNnSc1	ředění
<g/>
"	"	kIx"	"
nezkvašeným	zkvašený	k2eNgNnSc7d1	zkvašený
pivem	pivo	k1gNnSc7	pivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
imobilizace	imobilizace	k1gFnSc1	imobilizace
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
znehybnění	znehybněný	k2eAgMnPc1d1	znehybněný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pak	pak	k6eAd1	pak
nemohou	moct	k5eNaImIp3nP	moct
produkovat	produkovat	k5eAaImF	produkovat
ethanol	ethanol	k1gInSc4	ethanol
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
míře	míra	k1gFnSc6	míra
jako	jako	k8xS	jako
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
normálních	normální	k2eAgNnPc2d1	normální
piv	pivo	k1gNnPc2	pivo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
konečně	konečně	k6eAd1	konečně
využití	využití	k1gNnSc1	využití
kvasinek	kvasinka	k1gFnPc2	kvasinka
speciálních	speciální	k2eAgMnPc2d1	speciální
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
už	už	k9	už
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pouze	pouze	k6eAd1	pouze
nepatrné	patrný	k2eNgNnSc4d1	nepatrné
množství	množství	k1gNnSc4	množství
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
i	i	k9	i
pivovarníci	pivovarník	k1gMnPc1	pivovarník
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
začali	začít	k5eAaPmAgMnP	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
nápoj	nápoj	k1gInSc4	nápoj
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
piva	pivo	k1gNnSc2	pivo
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
řidiče	řidič	k1gMnPc4	řidič
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
legendární	legendární	k2eAgNnSc1d1	legendární
Pito	pit	k2eAgNnSc1d1	pito
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
utvořen	utvořit	k5eAaPmNgInS	utvořit
spojením	spojení	k1gNnSc7	spojení
první	první	k4xOgFnSc2	první
a	a	k8xC	a
poslední	poslední	k2eAgFnSc2d1	poslední
slabiky	slabika	k1gFnSc2	slabika
slov	slovo	k1gNnPc2	slovo
PIvo	pivo	k1gNnSc4	pivo
a	a	k8xC	a
auTO	auto	k1gNnSc4	auto
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
směšnému	směšný	k2eAgInSc3d1	směšný
lidovému	lidový	k2eAgInSc3d1	lidový
názvu	název	k1gInSc3	název
tohoto	tento	k3xDgInSc2	tento
moku	mok	k1gInSc2	mok
<g/>
,	,	kIx,	,
vyráběného	vyráběný	k2eAgNnSc2d1	vyráběné
v	v	k7c6	v
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zrušeném	zrušený	k2eAgInSc6d1	zrušený
Mosteckém	mostecký	k2eAgInSc6d1	mostecký
pivovaru	pivovar	k1gInSc6	pivovar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pito	pit	k2eAgNnSc1d1	pito
se	se	k3xPyFc4	se
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
metodou	metoda	k1gFnSc7	metoda
řízeného	řízený	k2eAgNnSc2d1	řízené
kvašení	kvašení	k1gNnSc2	kvašení
spočívající	spočívající	k2eAgFnSc1d1	spočívající
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvasný	kvasný	k2eAgInSc1d1	kvasný
proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
už	už	k6eAd1	už
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jen	jen	k6eAd1	jen
zanedbatelné	zanedbatelný	k2eAgNnSc1d1	zanedbatelné
množství	množství	k1gNnSc1	množství
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
většina	většina	k1gFnSc1	většina
nealkoholických	alkoholický	k2eNgNnPc2d1	nealkoholické
piv	pivo	k1gNnPc2	pivo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
spotřeba	spotřeba	k1gFnSc1	spotřeba
nealkoholického	alkoholický	k2eNgNnSc2d1	nealkoholické
piva	pivo	k1gNnSc2	pivo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
výrazně	výrazně	k6eAd1	výrazně
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
proměně	proměna	k1gFnSc3	proměna
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
počet	počet	k1gInSc4	počet
nabízených	nabízený	k2eAgFnPc2d1	nabízená
značek	značka	k1gFnPc2	značka
–	–	k?	–
jak	jak	k8xC	jak
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
<g/>
,	,	kIx,	,
tak	tak	k9	tak
tuzemských	tuzemský	k2eAgFnPc2d1	tuzemská
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
centrálně	centrálně	k6eAd1	centrálně
plánované	plánovaný	k2eAgNnSc4d1	plánované
československé	československý	k2eAgNnSc4d1	Československé
hospodářství	hospodářství	k1gNnSc4	hospodářství
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
pouze	pouze	k6eAd1	pouze
Pitem	Pitem	k1gInSc4	Pitem
<g/>
,	,	kIx,	,
dvacet	dvacet	k4xCc1	dvacet
let	léto	k1gNnPc2	léto
po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
nealkoholické	alkoholický	k2eNgNnSc4d1	nealkoholické
pivo	pivo	k1gNnSc4	pivo
vaří	vařit	k5eAaImIp3nS	vařit
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
českých	český	k2eAgInPc2d1	český
pivovarů	pivovar	k1gInPc2	pivovar
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
i	i	k9	i
několik	několik	k4yIc4	několik
nealkoholických	alkoholický	k2eNgInPc2d1	nealkoholický
nápojů	nápoj	k1gInPc2	nápoj
pivního	pivní	k2eAgInSc2d1	pivní
typu	typ	k1gInSc2	typ
naráz	naráz	k6eAd1	naráz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
celorepublikový	celorepublikový	k2eAgInSc1d1	celorepublikový
výstav	výstav	k1gInSc1	výstav
nealkoholického	alkoholický	k2eNgNnSc2d1	nealkoholické
piva	pivo	k1gNnSc2	pivo
600	[number]	k4	600
000	[number]	k4	000
hektolitrů	hektolitr	k1gInPc2	hektolitr
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
2000	[number]	k4	2000
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nárůst	nárůst	k1gInSc4	nárůst
produkce	produkce	k1gFnSc2	produkce
až	až	k6eAd1	až
pětinásobný	pětinásobný	k2eAgInSc1d1	pětinásobný
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
domácím	domácí	k2eAgInSc6d1	domácí
trhu	trh	k1gInSc6	trh
má	mít	k5eAaImIp3nS	mít
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
Radegast	Radegast	k1gMnSc1	Radegast
Birell	Birell	k1gMnSc1	Birell
společnosti	společnost	k1gFnSc2	společnost
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
Prazdroj	prazdroj	k1gInSc1	prazdroj
a.s.	a.s.	k?	a.s.
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
60	[number]	k4	60
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
relativně	relativně	k6eAd1	relativně
novým	nový	k2eAgInPc3d1	nový
trendům	trend	k1gInPc3	trend
patří	patřit	k5eAaImIp3nS	patřit
rozšíření	rozšíření	k1gNnSc1	rozšíření
točených	točený	k2eAgNnPc2d1	točené
nealkoholických	alkoholický	k2eNgNnPc2d1	nealkoholické
piv	pivo	k1gNnPc2	pivo
do	do	k7c2	do
restaurací	restaurace	k1gFnPc2	restaurace
nebo	nebo	k8xC	nebo
vytváření	vytváření	k1gNnSc2	vytváření
různých	různý	k2eAgFnPc2d1	různá
chuťových	chuťový	k2eAgFnPc2d1	chuťová
variant	varianta	k1gFnPc2	varianta
známých	známý	k2eAgFnPc2d1	známá
značek	značka	k1gFnPc2	značka
(	(	kIx(	(
<g/>
Bernard	Bernard	k1gMnSc1	Bernard
Jantarový	jantarový	k2eAgMnSc1d1	jantarový
<g/>
,	,	kIx,	,
Bernard	Bernard	k1gMnSc1	Bernard
Švestka	Švestka	k1gMnSc1	Švestka
<g/>
,	,	kIx,	,
Birell	Birell	k1gMnSc1	Birell
Polotmavý	polotmavý	k2eAgMnSc1d1	polotmavý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
