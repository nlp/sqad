<s>
Např.	např.	kA	např.
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
non-alcoholic	nonlcoholice	k1gFnPc2	non-alcoholice
beer	beer	k1gInSc1	beer
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
EU	EU	kA	EU
"	"	kIx"	"
<g/>
alcohol-free	alcoholreat	k5eAaPmIp3nS	alcohol-freat
beer	beer	k1gMnSc1	beer
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
však	však	k9	však
nápoj	nápoj	k1gInSc4	nápoj
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
až	až	k9	až
0,5	[number]	k4	0,5
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
nazýván	nazývat	k5eAaImNgInS	nazývat
nealkoholickým	alkoholický	k2eNgInSc7d1	nealkoholický
<g/>
.	.	kIx.	.
</s>
