<s>
Peer-to-peer	Peeroeer	k1gInSc1	Peer-to-peer
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
rovný	rovný	k2eAgInSc4d1	rovný
s	s	k7c7	s
rovným	rovný	k2eAgInSc7d1	rovný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
P2P	P2P	k1gMnSc1	P2P
nebo	nebo	k8xC	nebo
klient-klient	klientlient	k1gMnSc1	klient-klient
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
typu	typ	k1gInSc2	typ
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgMnPc4	který
spolu	spolu	k6eAd1	spolu
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
přímo	přímo	k6eAd1	přímo
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
klienti	klient	k1gMnPc1	klient
(	(	kIx(	(
<g/>
uživatelé	uživatel	k1gMnPc1	uživatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
je	být	k5eAaImIp3nS	být
klient-server	klienterver	k1gInSc1	klient-server
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgMnPc4	který
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
klienti	klient	k1gMnPc1	klient
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
vždy	vždy	k6eAd1	vždy
s	s	k7c7	s
centrálním	centrální	k2eAgInSc7d1	centrální
serverem	server	k1gInSc7	server
či	či	k8xC	či
servery	server	k1gInPc1	server
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kterého	který	k3yRgNnSc2	který
i	i	k9	i
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
klienty	klient	k1gMnPc7	klient
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Čistá	čistý	k2eAgFnSc1d1	čistá
P2P	P2P	k1gFnSc1	P2P
architektura	architektura	k1gFnSc1	architektura
vůbec	vůbec	k9	vůbec
pojem	pojem	k1gInSc1	pojem
server	server	k1gInSc1	server
nezná	neznat	k5eAaImIp3nS	neznat
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
uzly	uzel	k1gInPc4	uzel
sítě	síť	k1gFnSc2	síť
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
rovnocenné	rovnocenný	k2eAgInPc4d1	rovnocenný
(	(	kIx(	(
<g/>
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
současně	současně	k6eAd1	současně
jako	jako	k8xC	jako
klienti	klient	k1gMnPc1	klient
i	i	k9	i
servery	server	k1gInPc4	server
pro	pro	k7c4	pro
jiné	jiný	k2eAgMnPc4d1	jiný
klienty	klient	k1gMnPc4	klient
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
však	však	k9	však
často	často	k6eAd1	často
pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
návrhu	návrh	k1gInSc2	návrh
v	v	k7c6	v
protokolu	protokol	k1gInSc6	protokol
objevují	objevovat	k5eAaImIp3nP	objevovat
specializované	specializovaný	k2eAgInPc1d1	specializovaný
servery	server	k1gInPc1	server
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ovšem	ovšem	k9	ovšem
slouží	sloužit	k5eAaImIp3nP	sloužit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
počáteční	počáteční	k2eAgNnSc4d1	počáteční
navázání	navázání	k1gNnSc4	navázání
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
seznámení	seznámení	k1gNnSc4	seznámení
<g/>
"	"	kIx"	"
klientů	klient	k1gMnPc2	klient
navzájem	navzájem	k6eAd1	navzájem
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
jako	jako	k8xC	jako
proxy	prox	k1gInPc4	prox
server	server	k1gInSc1	server
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
nemohou	moct	k5eNaImIp3nP	moct
koncové	koncový	k2eAgInPc1d1	koncový
uzly	uzel	k1gInPc1	uzel
komunikovat	komunikovat	k5eAaImF	komunikovat
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
označení	označení	k1gNnSc1	označení
P2P	P2P	k1gFnSc2	P2P
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
výměnné	výměnný	k2eAgFnPc4d1	výměnná
sítě	síť	k1gFnPc4	síť
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kterých	který	k3yQgFnPc2	který
si	se	k3xPyFc3	se
mnoho	mnoho	k4c1	mnoho
uživatelů	uživatel	k1gMnPc2	uživatel
může	moct	k5eAaImIp3nS	moct
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takových	takový	k3xDgFnPc2	takový
sítí	síť	k1gFnPc2	síť
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Gnutella	Gnutello	k1gNnSc2	Gnutello
či	či	k8xC	či
původní	původní	k2eAgFnSc2d1	původní
verze	verze	k1gFnSc2	verze
Napsteru	Napster	k1gInSc2	Napster
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
výhod	výhoda	k1gFnPc2	výhoda
P2P	P2P	k1gFnSc2	P2P
sítí	síť	k1gFnPc2	síť
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
množstvím	množství	k1gNnSc7	množství
uživatelů	uživatel	k1gMnPc2	uživatel
celková	celkový	k2eAgFnSc1d1	celková
dostupná	dostupný	k2eAgFnSc1d1	dostupná
přenosová	přenosový	k2eAgFnSc1d1	přenosová
kapacita	kapacita	k1gFnSc1	kapacita
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
modelu	model	k1gInSc2	model
klient-server	klientervra	k1gFnPc2	klient-servra
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
uživatelé	uživatel	k1gMnPc1	uživatel
dělit	dělit	k5eAaImF	dělit
o	o	k7c4	o
konstantní	konstantní	k2eAgFnSc4d1	konstantní
kapacitu	kapacita	k1gFnSc4	kapacita
serveru	server	k1gInSc2	server
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
při	při	k7c6	při
nárůstu	nárůst	k1gInSc6	nárůst
uživatelů	uživatel	k1gMnPc2	uživatel
klesá	klesat	k5eAaImIp3nS	klesat
průměrná	průměrný	k2eAgFnSc1d1	průměrná
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
obsahem	obsah	k1gInSc7	obsah
šířeným	šířený	k2eAgInSc7d1	šířený
po	po	k7c6	po
výměnných	výměnný	k2eAgFnPc6d1	výměnná
sítích	síť	k1gFnPc6	síť
jsou	být	k5eAaImIp3nP	být
hudební	hudební	k2eAgFnPc1d1	hudební
nahrávky	nahrávka	k1gFnPc1	nahrávka
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
filmy	film	k1gInPc1	film
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
MPEG	MPEG	kA	MPEG
a	a	k8xC	a
software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
výrazná	výrazný	k2eAgFnSc1d1	výrazná
většina	většina	k1gFnSc1	většina
<g/>
)	)	kIx)	)
tohoto	tento	k3xDgInSc2	tento
obsahu	obsah	k1gInSc2	obsah
je	být	k5eAaImIp3nS	být
šířena	šířit	k5eAaImNgFnS	šířit
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
držitele	držitel	k1gMnSc2	držitel
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
žaloby	žaloba	k1gFnPc4	žaloba
na	na	k7c4	na
provozovatele	provozovatel	k1gMnPc4	provozovatel
takových	takový	k3xDgFnPc2	takový
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podáváné	podáváný	k2eAgFnSc2d1	podáváná
zástupci	zástupce	k1gMnPc1	zástupce
autorů	autor	k1gMnPc2	autor
a	a	k8xC	a
organizacemi	organizace	k1gFnPc7	organizace
jako	jako	k8xS	jako
RIAA	RIAA	kA	RIAA
či	či	k8xC	či
MPAA	MPAA	kA	MPAA
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
Napster	Napstrum	k1gNnPc2	Napstrum
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
úspěšné	úspěšný	k2eAgFnSc3d1	úspěšná
žalobě	žaloba	k1gFnSc3	žaloba
organizace	organizace	k1gFnSc2	organizace
RIAA	RIAA	kA	RIAA
zrušena	zrušen	k2eAgFnSc1d1	zrušena
(	(	kIx(	(
<g/>
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
Napster	Napster	k1gInSc1	Napster
odkoupen	odkoupit	k5eAaPmNgInS	odkoupit
a	a	k8xC	a
pod	pod	k7c7	pod
stejnou	stejný	k2eAgFnSc7d1	stejná
značkou	značka	k1gFnSc7	značka
nyní	nyní	k6eAd1	nyní
nabízí	nabízet	k5eAaImIp3nS	nabízet
legální	legální	k2eAgInSc4d1	legální
prodej	prodej	k1gInSc4	prodej
hudby	hudba	k1gFnSc2	hudba
přes	přes	k7c4	přes
Internet	Internet	k1gInSc4	Internet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
sítě	síť	k1gFnPc1	síť
mnoha	mnoho	k4c3	mnoho
podobným	podobný	k2eAgFnPc3d1	podobná
žalobám	žaloba	k1gFnPc3	žaloba
čelí	čelit	k5eAaImIp3nS	čelit
<g/>
;	;	kIx,	;
zástupci	zástupce	k1gMnPc1	zástupce
autorských	autorský	k2eAgInPc2d1	autorský
svazů	svaz	k1gInPc2	svaz
často	často	k6eAd1	často
lobbují	lobbovat	k5eAaImIp3nP	lobbovat
za	za	k7c4	za
přijetí	přijetí	k1gNnSc4	přijetí
tvrdších	tvrdý	k2eAgInPc2d2	tvrdší
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
výměnné	výměnný	k2eAgFnPc1d1	výměnná
sítě	síť	k1gFnPc1	síť
postihovaly	postihovat	k5eAaImAgFnP	postihovat
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnPc1d1	dnešní
anonymní	anonymní	k2eAgFnPc1d1	anonymní
výměnné	výměnný	k2eAgFnPc1d1	výměnná
sítě	síť	k1gFnPc1	síť
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
(	(	kIx(	(
<g/>
legální	legální	k2eAgFnSc7d1	legální
i	i	k8xC	i
nelegální	legální	k2eNgFnSc4d1	nelegální
<g/>
)	)	kIx)	)
výměnu	výměna	k1gFnSc4	výměna
souborů	soubor	k1gInPc2	soubor
s	s	k7c7	s
prakticky	prakticky	k6eAd1	prakticky
nulovou	nulový	k2eAgFnSc7d1	nulová
mírou	míra	k1gFnSc7	míra
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kritici	kritik	k1gMnPc1	kritik
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
výměnných	výměnný	k2eAgFnPc2d1	výměnná
sítí	síť	k1gFnPc2	síť
může	moct	k5eAaImIp3nS	moct
distribuovat	distribuovat	k5eAaBmF	distribuovat
dětská	dětský	k2eAgFnSc1d1	dětská
pornografie	pornografie	k1gFnSc1	pornografie
či	či	k8xC	či
podporovat	podporovat	k5eAaImF	podporovat
terorismus	terorismus	k1gInSc4	terorismus
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
žádají	žádat	k5eAaImIp3nP	žádat
o	o	k7c6	o
regulaci	regulace	k1gFnSc6	regulace
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
zákaz	zákaz	k1gInSc1	zákaz
takových	takový	k3xDgFnPc2	takový
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
naopak	naopak	k6eAd1	naopak
argumentují	argumentovat	k5eAaImIp3nP	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
možnost	možnost	k1gFnSc1	možnost
zneužití	zneužití	k1gNnSc2	zneužití
technologie	technologie	k1gFnSc2	technologie
k	k	k7c3	k
nezákonným	zákonný	k2eNgInPc3d1	nezákonný
účelům	účel	k1gInPc3	účel
nesmí	smět	k5eNaImIp3nS	smět
bránit	bránit	k5eAaImF	bránit
jejímu	její	k3xOp3gNnSc3	její
legálnímu	legální	k2eAgNnSc3d1	legální
využívání	využívání	k1gNnSc3	využívání
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dodržovat	dodržovat	k5eAaImF	dodržovat
princip	princip	k1gInSc4	princip
presumpce	presumpce	k1gFnSc2	presumpce
neviny	nevina	k1gFnSc2	nevina
<g/>
.	.	kIx.	.
</s>
<s>
P2P	P2P	k4	P2P
sítě	síť	k1gFnPc1	síť
mají	mít	k5eAaImIp3nP	mít
už	už	k6eAd1	už
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
některá	některý	k3yIgNnPc1	některý
slabá	slabý	k2eAgNnPc1d1	slabé
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
zvýrazněna	zvýrazněn	k2eAgNnPc1d1	zvýrazněno
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
právních	právní	k2eAgInPc2d1	právní
kroků	krok	k1gInPc2	krok
někdy	někdy	k6eAd1	někdy
zástupci	zástupce	k1gMnPc1	zástupce
umělců	umělec	k1gMnPc2	umělec
podnikají	podnikat	k5eAaImIp3nP	podnikat
také	také	k9	také
přímé	přímý	k2eAgInPc4d1	přímý
útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
P2P	P2P	k1gFnSc3	P2P
sítím	síť	k1gFnPc3	síť
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
P2P	P2P	k1gMnPc1	P2P
sítě	síť	k1gFnSc2	síť
předmětem	předmět	k1gInSc7	předmět
"	"	kIx"	"
<g/>
tradičních	tradiční	k2eAgInPc2d1	tradiční
<g/>
"	"	kIx"	"
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
slabiny	slabina	k1gFnPc4	slabina
P2P	P2P	k1gMnPc2	P2P
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
metody	metoda	k1gFnSc2	metoda
útoků	útok	k1gInPc2	útok
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
otrava	otrava	k1gFnSc1	otrava
sítě	síť	k1gFnSc2	síť
<g/>
"	"	kIx"	"
-	-	kIx~	-
poskytování	poskytování	k1gNnSc1	poskytování
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
něco	něco	k3yInSc4	něco
jiného	jiný	k2eAgNnSc2d1	jiné
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zcela	zcela	k6eAd1	zcela
neužitečná	užitečný	k2eNgNnPc1d1	neužitečné
data	datum	k1gNnPc1	datum
<g/>
)	)	kIx)	)
než	než	k8xS	než
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
názvu	název	k1gInSc2	název
souboru	soubor	k1gInSc2	soubor
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInPc1	jeho
komentáře	komentář	k1gInPc1	komentář
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
fake	fake	k1gInSc1	fake
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
DoS	DoS	k1gMnSc1	DoS
<g />
.	.	kIx.	.
</s>
<s>
útoky	útok	k1gInPc1	útok
-	-	kIx~	-
přetěžování	přetěžování	k1gNnSc1	přetěžování
sítě	síť	k1gFnSc2	síť
či	či	k8xC	či
jiné	jiný	k2eAgInPc4d1	jiný
útoky	útok	k1gInPc4	útok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
síť	síť	k1gFnSc1	síť
zpomalují	zpomalovat	k5eAaImIp3nP	zpomalovat
popř.	popř.	kA	popř.
úplně	úplně	k6eAd1	úplně
znefunkční	znefunkční	k2eAgMnPc1d1	znefunkční
<g/>
,	,	kIx,	,
neužiteční	užitečný	k2eNgMnPc1d1	neužitečný
uživatelé	uživatel	k1gMnPc1	uživatel
-	-	kIx~	-
pouze	pouze	k6eAd1	pouze
získávají	získávat	k5eAaImIp3nP	získávat
obsah	obsah	k1gInSc4	obsah
od	od	k7c2	od
jiných	jiný	k2eAgNnPc2d1	jiné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sami	sám	k3xTgMnPc1	sám
žádná	žádný	k3yNgNnPc1	žádný
užitečná	užitečný	k2eAgNnPc1d1	užitečné
data	datum	k1gNnPc1	datum
nenabízejí	nabízet	k5eNaImIp3nP	nabízet
<g/>
,	,	kIx,	,
počítačové	počítačový	k2eAgInPc1d1	počítačový
viry	vir	k1gInPc1	vir
nebo	nebo	k8xC	nebo
trojské	trojský	k2eAgMnPc4d1	trojský
koně	kůň	k1gMnPc4	kůň
v	v	k7c6	v
nabízených	nabízený	k2eAgInPc6d1	nabízený
souborech	soubor	k1gInPc6	soubor
<g/>
,	,	kIx,	,
filtrování	filtrování	k1gNnSc1	filtrování
protokolů	protokol	k1gInPc2	protokol
-	-	kIx~	-
poskytovatelé	poskytovatel	k1gMnPc1	poskytovatel
mohou	moct	k5eAaImIp3nP	moct
blokovat	blokovat	k5eAaImF	blokovat
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
pomocí	pomocí	k7c2	pomocí
P2P	P2P	k1gFnSc2	P2P
protokolů	protokol	k1gInPc2	protokol
<g/>
,	,	kIx,	,
útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
soukromí	soukromí	k1gNnSc3	soukromí
-	-	kIx~	-
zjišťování	zjišťování	k1gNnSc3	zjišťování
osobních	osobní	k2eAgInPc2d1	osobní
údajů	údaj	k1gInPc2	údaj
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
jejich	jejich	k3xOp3gNnSc4	jejich
obtěžování	obtěžování	k1gNnSc4	obtěžování
<g/>
,	,	kIx,	,
spam	spam	k1gInSc1	spam
-	-	kIx~	-
rozesílání	rozesílání	k1gNnSc1	rozesílání
nevyžádaných	vyžádaný	k2eNgFnPc2d1	nevyžádaná
informací	informace	k1gFnPc2	informace
pomocí	pomocí	k7c2	pomocí
P2P	P2P	k1gFnSc2	P2P
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Akamai	Akama	k1gMnPc1	Akama
Applejuice	Applejuice	k1gFnSc2	Applejuice
Bitcoin	Bitcoin	k1gMnSc1	Bitcoin
BitTorrent	BitTorrent	k1gMnSc1	BitTorrent
(	(	kIx(	(
<g/>
μ	μ	k?	μ
<g/>
,	,	kIx,	,
Vuze	Vuze	k1gFnSc1	Vuze
<g/>
/	/	kIx~	/
<g/>
Azureus	Azureus	k1gMnSc1	Azureus
<g/>
,	,	kIx,	,
BitTorrent	BitTorrent	k1gMnSc1	BitTorrent
<g/>
,	,	kIx,	,
Bittornado	Bittornada	k1gFnSc5	Bittornada
<g/>
,	,	kIx,	,
KTorrent	KTorrent	k1gMnSc1	KTorrent
<g/>
,	,	kIx,	,
MLDonkey	MLDonkey	k1gInPc1	MLDonkey
<g/>
,	,	kIx,	,
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
Shareaza	Shareaza	k1gFnSc1	Shareaza
<g/>
)	)	kIx)	)
Napster	Napster	k1gMnSc1	Napster
CAKE	CAKE	kA	CAKE
Direct	Direct	k1gMnSc1	Direct
Connect	Connect	k1gMnSc1	Connect
(	(	kIx(	(
<g/>
DC	DC	kA	DC
<g/>
,	,	kIx,	,
DC	DC	kA	DC
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
MLDonkey	MLDonkea	k1gFnPc4	MLDonkea
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
ed	ed	k?	ed
<g/>
2	[number]	k4	2
<g/>
k	k	k7c3	k
(	(	kIx(	(
<g/>
eDonkey	eDonkea	k1gMnSc2	eDonkea
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
eMule	eMule	k1gFnSc1	eMule
<g/>
,	,	kIx,	,
aMule	aMule	k1gFnSc1	aMule
<g/>
)	)	kIx)	)
FastTrack	FastTrack	k1gInSc1	FastTrack
(	(	kIx(	(
<g/>
Grokster	Grokster	k1gInSc1	Grokster
<g/>
,	,	kIx,	,
Kazaa	Kazaum	k1gNnPc1	Kazaum
<g/>
,	,	kIx,	,
MLDonkey	MLDonkey	k1gInPc1	MLDonkey
<g/>
)	)	kIx)	)
FileTopia	FileTopia	k1gFnSc1	FileTopia
Freenet	Freenet	k1gMnSc1	Freenet
Gnutella	Gnutella	k1gMnSc1	Gnutella
<g/>
,	,	kIx,	,
Gnutella	Gnutella	k1gFnSc1	Gnutella
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
Shareaza	Shareaza	k1gFnSc1	Shareaza
<g/>
,	,	kIx,	,
MLDonkey	MLDonkey	k1gInPc1	MLDonkey
<g/>
)	)	kIx)	)
HyperCast	HyperCast	k1gFnSc1	HyperCast
Joltid	Joltid	k1gInSc4	Joltid
PeerEnabler	PeerEnabler	k1gInSc1	PeerEnabler
Kademlia	Kademlia	k1gFnSc1	Kademlia
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
eMule	eMule	k1gNnPc1	eMule
<g/>
,	,	kIx,	,
aMule	aMule	k1gNnPc1	aMule
<g/>
,	,	kIx,	,
eDonkey	eDonkey	k1gInPc1	eDonkey
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
MLDonkey	MLDonkea	k1gFnPc1	MLDonkea
<g/>
)	)	kIx)	)
Kazaa	Kaza	k2eAgFnSc1d1	Kaza
MANOLITO	MANOLITO	kA	MANOLITO
(	(	kIx(	(
<g/>
MP	MP	kA	MP
<g/>
2	[number]	k4	2
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
PeerCast	PeerCast	k1gMnSc1	PeerCast
<g/>
,	,	kIx,	,
IceShare	IceShar	k1gMnSc5	IceShar
<g/>
,	,	kIx,	,
Freecast	Freecast	k1gMnSc1	Freecast
Skype	Skyp	k1gInSc5	Skyp
Soulseek	Soulseky	k1gFnPc2	Soulseky
Warez	Warez	k1gMnSc1	Warez
P2P	P2P	k1gMnSc1	P2P
(	(	kIx(	(
<g/>
Ares	Ares	k1gMnSc1	Ares
<g/>
)	)	kIx)	)
WinMX	WinMX	k1gMnSc1	WinMX
Warez	Warez	k1gMnSc1	Warez
Klient-server	Klienterver	k1gMnSc1	Klient-server
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
<g />
.	.	kIx.	.
</s>
<s>
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Peer-to-peer	Peeroera	k1gFnPc2	Peer-to-pera
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
PCWorld	PCWorld	k1gInSc1	PCWorld
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Sdílení	sdílení	k1gNnSc1	sdílení
souborů	soubor	k1gInPc2	soubor
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
a	a	k8xC	a
sítě	síť	k1gFnSc2	síť
P2P	P2P	k1gFnSc2	P2P
-	-	kIx~	-
základní	základní	k2eAgInSc4d1	základní
technologický	technologický	k2eAgInSc4d1	technologický
přehled	přehled	k1gInSc4	přehled
sdileni	sdilen	k2eAgMnPc1d1	sdilen
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Server	server	k1gInSc1	server
o	o	k7c4	o
sdílení	sdílení	k1gNnSc4	sdílení
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
Seriál	seriál	k1gInSc1	seriál
Techniky	technika	k1gFnSc2	technika
skryté	skrytý	k2eAgFnSc2d1	skrytá
v	v	k7c4	v
p	p	k?	p
<g/>
2	[number]	k4	2
<g/>
p	p	k?	p
sieťach	sieťach	k1gMnSc1	sieťach
-	-	kIx~	-
Root	Root	k1gMnSc1	Root
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
