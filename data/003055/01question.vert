<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
typ	typ	k1gInSc1	typ
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
spolu	spolu	k6eAd1	spolu
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
přímo	přímo	k6eAd1	přímo
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
uživatelé	uživatel	k1gMnPc1	uživatel
<g/>
?	?	kIx.	?
</s>
