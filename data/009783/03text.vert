<p>
<s>
Kolín	Kolín	k1gInSc1	Kolín
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Labi	Labe	k1gNnSc6	Labe
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
32	[number]	k4	32
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
35	[number]	k4	35
km2	km2	k4	km2
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
220	[number]	k4	220
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgNnSc1d1	vlastní
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
Kolín	Kolín	k1gInSc1	Kolín
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
23,47	[number]	k4	23,47
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kolín	Kolín	k1gInSc1	Kolín
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
průmysl	průmysl	k1gInSc4	průmysl
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgInSc4d1	automobilový
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
,	,	kIx,	,
polygrafický	polygrafický	k2eAgInSc4d1	polygrafický
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
hutnický	hutnický	k2eAgInSc1d1	hutnický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Kolín	Kolín	k1gInSc1	Kolín
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
jazykovědce	jazykovědec	k1gMnSc2	jazykovědec
Milana	Milan	k1gMnSc2	Milan
Harvalíka	Harvalík	k1gMnSc2	Harvalík
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
sousední	sousední	k2eAgFnSc2d1	sousední
obce	obec	k1gFnSc2	obec
Starý	starý	k2eAgInSc1d1	starý
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
"	"	kIx"	"
<g/>
Kolín	Kolín	k1gInSc4	Kolín
<g/>
"	"	kIx"	"
a	a	k8xC	a
přívlastek	přívlastek	k1gInSc4	přívlastek
"	"	kIx"	"
<g/>
Starý	starý	k2eAgMnSc1d1	starý
<g/>
"	"	kIx"	"
jí	on	k3xPp3gFnSc7	on
byl	být	k5eAaImAgMnS	být
dán	dát	k5eAaPmNgMnS	dát
až	až	k6eAd1	až
posléze	posléze	k6eAd1	posléze
<g/>
,	,	kIx,	,
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Kolína	Kolín	k1gInSc2	Kolín
<g/>
;	;	kIx,	;
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
Nový	nový	k2eAgInSc1d1	nový
Kolín	Kolín	k1gInSc1	Kolín
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
prostředí	prostředí	k1gNnSc6	prostředí
přívlastek	přívlastek	k1gInSc4	přívlastek
"	"	kIx"	"
<g/>
Neu	Neu	k1gMnSc1	Neu
<g/>
"	"	kIx"	"
přetrval	přetrvat	k5eAaPmAgMnS	přetrvat
déle	dlouho	k6eAd2	dlouho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
Kolín	Kolín	k1gInSc1	Kolín
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Harvalíka	Harvalík	k1gMnSc2	Harvalík
osobní	osobní	k2eAgNnSc4d1	osobní
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
Kola	Kola	k1gFnSc1	Kola
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
domácké	domácký	k2eAgNnSc1d1	domácké
oslovení	oslovení	k1gNnSc1	oslovení
nositele	nositel	k1gMnSc2	nositel
středověkého	středověký	k2eAgNnSc2d1	středověké
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
Kolimír	Kolimír	k1gMnSc1	Kolimír
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Chola	Chola	k1gFnSc1	Chola
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
staročeského	staročeský	k2eAgInSc2d1	staročeský
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
choliti	cholit	k5eAaImF	cholit
<g/>
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
chlácholit	chlácholit	k5eAaImF	chlácholit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
byla	být	k5eAaImAgFnS	být
přidána	přidán	k2eAgFnSc1d1	přidána
přivlastňovací	přivlastňovací	k2eAgFnSc1d1	přivlastňovací
přípona	přípona	k1gFnSc1	přípona
"	"	kIx"	"
<g/>
-in	n	k?	-in
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spojení	spojení	k1gNnSc4	spojení
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
Cholův	Cholův	k2eAgInSc4d1	Cholův
dvůr	dvůr	k1gInSc4	dvůr
<g/>
,	,	kIx,	,
Cholův	Cholův	k2eAgInSc4d1	Cholův
hrad	hrad	k1gInSc4	hrad
či	či	k8xC	či
Cholův	Cholův	k2eAgInSc4d1	Cholův
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
(	(	kIx(	(
<g/>
prezentovaných	prezentovaný	k2eAgFnPc2d1	prezentovaná
například	například	k6eAd1	například
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s>
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc1	název
ze	z	k7c2	z
staročeského	staročeský	k2eAgNnSc2d1	staročeské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
koliti	kolit	k5eAaImF	kolit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
zatloukat	zatloukat	k5eAaImF	zatloukat
kolíky	kolík	k1gInPc4	kolík
<g/>
,	,	kIx,	,
kůly	kůl	k1gInPc4	kůl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
polohou	poloha	k1gFnSc7	poloha
Starého	Starého	k2eAgInSc2d1	Starého
Kolína	Kolín	k1gInSc2	Kolín
v	v	k7c6	v
často	často	k6eAd1	často
zaplavované	zaplavovaný	k2eAgFnSc6d1	zaplavovaná
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Klejnárky	Klejnárka	k1gFnSc2	Klejnárka
a	a	k8xC	a
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
kůlů	kůl	k1gInPc2	kůl
zpevňovala	zpevňovat	k5eAaImAgFnS	zpevňovat
půda	půda	k1gFnSc1	půda
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
rozvíjející	rozvíjející	k2eAgFnSc4d1	rozvíjející
se	se	k3xPyFc4	se
trhovou	trhový	k2eAgFnSc4d1	trhová
osadu	osada	k1gFnSc4	osada
vybrána	vybrán	k2eAgFnSc1d1	vybrána
výhodnější	výhodný	k2eAgFnSc1d2	výhodnější
poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
vyvýšeném	vyvýšený	k2eAgNnSc6d1	vyvýšené
místě	místo	k1gNnSc6	místo
7	[number]	k4	7
km	km	kA	km
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
osada	osada	k1gFnSc1	osada
(	(	kIx(	(
<g/>
a	a	k8xC	a
pozdější	pozdní	k2eAgNnSc1d2	pozdější
královské	královský	k2eAgNnSc1d1	královské
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
ponechala	ponechat	k5eAaPmAgFnS	ponechat
jméno	jméno	k1gNnSc4	jméno
původní	původní	k2eAgFnSc2d1	původní
osady	osada	k1gFnSc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
způsob	způsob	k1gInSc1	způsob
vzniku	vznik	k1gInSc2	vznik
názvu	název	k1gInSc2	název
však	však	k9	však
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Harvalík	Harvalík	k1gMnSc1	Harvalík
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
výklady	výklad	k1gInPc1	výklad
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
také	také	k9	také
být	být	k5eAaImF	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
polohy	poloha	k1gFnSc2	poloha
na	na	k7c6	na
vyvýšeném	vyvýšený	k2eAgNnSc6d1	vyvýšené
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
pahorku	pahorek	k1gInSc2	pahorek
–	–	k?	–
latinsky	latinsky	k6eAd1	latinsky
"	"	kIx"	"
<g/>
colinus	colinus	k1gInSc1	colinus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
Starého	Starého	k2eAgInSc2d1	Starého
Kolína	Kolín	k1gInSc2	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
teorie	teorie	k1gFnSc2	teorie
latinská	latinský	k2eAgFnSc1d1	Latinská
podoba	podoba	k1gFnSc1	podoba
Colonia	Colonium	k1gNnSc2	Colonium
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
německých	německý	k2eAgMnPc2d1	německý
kolonistů	kolonista	k1gMnPc2	kolonista
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Kolín	Kolín	k1gInSc1	Kolín
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
relokací	relokace	k1gFnSc7	relokace
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1261	[number]	k4	1261
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
<g/>
)	)	kIx)	)
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
osídlena	osídlen	k2eAgFnSc1d1	osídlena
je	být	k5eAaImIp3nS	být
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
nepřetržitě	přetržitě	k6eNd1	přetržitě
už	už	k6eAd1	už
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
Slovanů	Slovan	k1gInPc2	Slovan
lze	lze	k6eAd1	lze
doložit	doložit	k5eAaPmF	doložit
už	už	k6eAd1	už
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
příkazu	příkaz	k1gInSc2	příkaz
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
městské	městský	k2eAgNnSc1d1	Městské
opevnění	opevnění	k1gNnSc1	opevnění
z	z	k7c2	z
dvojitého	dvojitý	k2eAgInSc2d1	dvojitý
pásu	pás	k1gInSc2	pás
kamenných	kamenný	k2eAgFnPc2d1	kamenná
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Středověké	středověký	k2eAgNnSc1d1	středověké
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
chráněno	chránit	k5eAaImNgNnS	chránit
i	i	k9	i
mohutným	mohutný	k2eAgInSc7d1	mohutný
tokem	tok	k1gInSc7	tok
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
protější	protější	k2eAgInSc1d1	protější
břeh	břeh	k1gInSc1	břeh
byl	být	k5eAaImAgInS	být
opevněn	opevnit	k5eAaPmNgInS	opevnit
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
hradební	hradební	k2eAgFnSc2d1	hradební
soustavy	soustava	k1gFnSc2	soustava
je	být	k5eAaImIp3nS	být
zálabská	zálabský	k2eAgFnSc1d1	Zálabská
bašta	bašta	k1gFnSc1	bašta
–	–	k?	–
Práchovna	Práchovna	k1gFnSc1	Práchovna
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usídlili	usídlit	k5eAaPmAgMnP	usídlit
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
doloženi	doložen	k2eAgMnPc1d1	doložen
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
knihách	kniha	k1gFnPc6	kniha
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1377	[number]	k4	1377
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
postupně	postupně	k6eAd1	postupně
nabyla	nabýt	k5eAaPmAgFnS	nabýt
značného	značný	k2eAgInSc2d1	značný
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
a	a	k8xC	a
nejvlivnější	vlivný	k2eAgFnPc4d3	nejvlivnější
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
žil	žít	k5eAaImAgInS	žít
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
vlivný	vlivný	k2eAgMnSc1d1	vlivný
městský	městský	k2eAgMnSc1d1	městský
úředník	úředník	k1gMnSc1	úředník
Tumlíř	tumlíř	k1gMnSc1	tumlíř
<g/>
.	.	kIx.	.
</s>
<s>
Nařízení	nařízení	k1gNnPc1	nařízení
císařovny	císařovna	k1gFnSc2	císařovna
o	o	k7c6	o
vystěhování	vystěhování	k1gNnSc6	vystěhování
Židů	Žid	k1gMnPc2	Žid
z	z	k7c2	z
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
uposlechnout	uposlechnout	k5eAaPmF	uposlechnout
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
zůstali	zůstat	k5eAaPmAgMnP	zůstat
a	a	k8xC	a
o	o	k7c6	o
každém	každý	k3xTgInSc6	každý
velkém	velký	k2eAgInSc6d1	velký
svátku	svátek	k1gInSc6	svátek
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
za	za	k7c2	za
svého	svůj	k3xOyFgMnSc2	svůj
dobrodince	dobrodinec	k1gMnSc2	dobrodinec
modlili	modlit	k5eAaImAgMnP	modlit
v	v	k7c6	v
synagoze	synagoga	k1gFnSc6	synagoga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dějiny	dějiny	k1gFnPc1	dějiny
stručně	stručně	k6eAd1	stručně
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
===	===	k?	===
</s>
</p>
<p>
<s>
1413	[number]	k4	1413
město	město	k1gNnSc1	město
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1421	[number]	k4	1421
jej	on	k3xPp3gMnSc4	on
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Pražané	Pražan	k1gMnPc1	Pražan
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Orebskými	orebský	k2eAgMnPc7d1	orebský
husity	husita	k1gMnPc7	husita
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
spálili	spálit	k5eAaPmAgMnP	spálit
klášter	klášter	k1gInSc4	klášter
dominikánů	dominikán	k1gMnPc2	dominikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1427	[number]	k4	1427
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
sirotčího	sirotčí	k2eAgMnSc2d1	sirotčí
hejtmana	hejtman	k1gMnSc2	hejtman
Jana	Jan	k1gMnSc2	Jan
Čapka	Čapek	k1gMnSc2	Čapek
ze	z	k7c2	z
Sán	sán	k2eAgMnSc1d1	sán
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gMnSc4	on
držel	držet	k5eAaImAgMnS	držet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1434	[number]	k4	1434
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1435	[number]	k4	1435
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
město	město	k1gNnSc4	město
s	s	k7c7	s
okolními	okolní	k2eAgInPc7d1	okolní
církevními	církevní	k2eAgInPc7d1	církevní
statky	statek	k1gInPc7	statek
táborský	táborský	k2eAgMnSc1d1	táborský
kněz	kněz	k1gMnSc1	kněz
Bedřich	Bedřich	k1gMnSc1	Bedřich
ze	z	k7c2	z
Strážnice	Strážnice	k1gFnSc2	Strážnice
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c6	na
troskách	troska	k1gFnPc6	troska
kláštera	klášter	k1gInSc2	klášter
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
roku	rok	k1gInSc2	rok
1437	[number]	k4	1437
pevný	pevný	k2eAgInSc1d1	pevný
hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
na	na	k7c6	na
protější	protější	k2eAgFnSc6d1	protější
skále	skála	k1gFnSc6	skála
za	za	k7c7	za
Labem	Labe	k1gNnSc7	Labe
vysokou	vysoká	k1gFnSc4	vysoká
obrannou	obranný	k2eAgFnSc4d1	obranná
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Lapis	lapis	k1gInSc1	lapis
Refugii	refugium	k1gNnPc7	refugium
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
rodem	rod	k1gInSc7	rod
Žerotínů	Žerotín	k1gInPc2	Žerotín
přestavěn	přestavěn	k2eAgInSc4d1	přestavěn
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sloužil	sloužit	k5eAaImAgInS	sloužit
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
budovy	budova	k1gFnPc1	budova
a	a	k8xC	a
pivovar	pivovar	k1gInSc1	pivovar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1454	[number]	k4	1454
Bedřich	Bedřich	k1gMnSc1	Bedřich
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
katolickými	katolický	k2eAgMnPc7d1	katolický
odpůrci	odpůrce	k1gMnPc7	odpůrce
zemského	zemský	k2eAgInSc2d1	zemský
správce	správce	k1gMnSc2	správce
Jiřího	Jiří	k1gMnSc4	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
pány	pan	k1gMnPc7	pan
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
a	a	k8xC	a
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
.	.	kIx.	.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
ze	z	k7c2	z
Strážnice	Strážnice	k1gFnSc2	Strážnice
vládl	vládnout	k5eAaImAgInS	vládnout
na	na	k7c6	na
kolínském	kolínský	k2eAgNnSc6d1	kolínské
panství	panství	k1gNnSc6	panství
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1458	[number]	k4	1458
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
zvolen	zvolit	k5eAaPmNgInS	zvolit
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
a	a	k8xC	a
vykázal	vykázat	k5eAaPmAgInS	vykázat
Bedřicha	Bedřich	k1gMnSc4	Bedřich
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
hrad	hrad	k1gInSc4	hrad
Potštejn	Potštejn	k1gInSc1	Potštejn
<g/>
.	.	kIx.	.
</s>
<s>
Městu	město	k1gNnSc3	město
Jiří	Jiří	k1gMnPc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
daroval	darovat	k5eAaPmAgInS	darovat
ungelt	ungelt	k1gInSc1	ungelt
v	v	k7c6	v
branách	brána	k1gFnPc6	brána
a	a	k8xC	a
na	na	k7c6	na
mostě	most	k1gInSc6	most
a	a	k8xC	a
také	také	k9	také
dvůr	dvůr	k1gInSc1	dvůr
v	v	k7c6	v
Křečhoři	Křečhoř	k1gFnSc6	Křečhoř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1471	[number]	k4	1471
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c4	v
držení	držení	k1gNnSc4	držení
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Viktorina	Viktorin	k1gMnSc2	Viktorin
knížete	kníže	k1gMnSc2	kníže
z	z	k7c2	z
Minstrberka	Minstrberka	k1gFnSc1	Minstrberka
<g/>
,	,	kIx,	,
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
nově	nově	k6eAd1	nově
zvolenému	zvolený	k2eAgMnSc3d1	zvolený
králi	král	k1gMnSc3	král
Vladislavu	Vladislav	k1gMnSc3	Vladislav
Jagellonskému	jagellonský	k2eAgMnSc3d1	jagellonský
a	a	k8xC	a
pozval	pozvat	k5eAaPmAgMnS	pozvat
do	do	k7c2	do
Kolína	Kolín	k1gInSc2	Kolín
svého	svůj	k3xOyFgMnSc2	svůj
švagra	švagr	k1gMnSc2	švagr
<g/>
,	,	kIx,	,
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
<g/>
.	.	kIx.	.
</s>
<s>
Uherská	uherský	k2eAgNnPc1d1	Uherské
vojska	vojsko	k1gNnPc1	vojsko
držela	držet	k5eAaImAgNnP	držet
Kolín	Kolín	k1gInSc4	Kolín
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1473	[number]	k4	1473
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přešlo	přejít	k5eAaPmAgNnS	přejít
město	město	k1gNnSc1	město
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Viktorínova	Viktorínův	k2eAgMnSc2d1	Viktorínův
bratra	bratr	k1gMnSc2	bratr
Hynka	Hynek	k1gMnSc2	Hynek
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
po	po	k7c6	po
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Benešově	Benešov	k1gInSc6	Benešov
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Hynek	Hynek	k1gMnSc1	Hynek
stal	stát	k5eAaPmAgMnS	stát
správcem	správce	k1gMnSc7	správce
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
stal	stát	k5eAaPmAgInS	stát
rovněž	rovněž	k9	rovněž
Vladislavovým	Vladislavův	k2eAgMnSc7d1	Vladislavův
odpůrcem	odpůrce	k1gMnSc7	odpůrce
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
Vladislav	Vladislav	k1gMnSc1	Vladislav
nesplnil	splnit	k5eNaPmAgMnS	splnit
slib	slib	k1gInSc4	slib
vzít	vzít	k5eAaPmF	vzít
si	se	k3xPyFc3	se
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
Hynkovu	Hynkův	k2eAgFnSc4d1	Hynkova
sestru	sestra	k1gFnSc4	sestra
Ludmilu	Ludmila	k1gFnSc4	Ludmila
<g/>
,	,	kIx,	,
a	a	k8xC	a
postoupil	postoupit	k5eAaPmAgMnS	postoupit
město	město	k1gNnSc4	město
roku	rok	k1gInSc2	rok
1476	[number]	k4	1476
Matyášovi	Matyášův	k2eAgMnPc1d1	Matyášův
za	za	k7c4	za
20	[number]	k4	20
000	[number]	k4	000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
červnu	červen	k1gInSc6	červen
1477	[number]	k4	1477
dvakrát	dvakrát	k6eAd1	dvakrát
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
obsazení	obsazení	k1gNnSc4	obsazení
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
(	(	kIx(	(
<g/>
1486	[number]	k4	1486
<g/>
)	)	kIx)	)
nakonec	nakonec	k6eAd1	nakonec
Matyáš	Matyáš	k1gMnSc1	Matyáš
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Kolín	Kolín	k1gInSc4	Kolín
králi	král	k1gMnSc3	král
Vladislavovi	Vladislav	k1gMnSc3	Vladislav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
kolínského	kolínský	k2eAgInSc2d1	kolínský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
hejtmani	hejtman	k1gMnPc1	hejtman
nebo	nebo	k8xC	nebo
zapsaní	zapsaný	k2eAgMnPc1d1	zapsaný
držitelé	držitel	k1gMnPc1	držitel
sami	sám	k3xTgMnPc1	sám
obnovovali	obnovovat	k5eAaImAgMnP	obnovovat
městskou	městský	k2eAgFnSc4d1	městská
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
osobovali	osobovat	k5eAaImAgMnP	osobovat
si	se	k3xPyFc3	se
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
správu	správa	k1gFnSc4	správa
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
změnil	změnit	k5eAaPmAgMnS	změnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1519	[number]	k4	1519
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
výnosem	výnos	k1gInSc7	výnos
zařadil	zařadit	k5eAaPmAgInS	zařadit
Kolín	Kolín	k1gInSc1	Kolín
opět	opět	k6eAd1	opět
mezi	mezi	k7c4	mezi
královská	královský	k2eAgNnPc4d1	královské
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1547	[number]	k4	1547
město	město	k1gNnSc1	město
těžce	těžce	k6eAd1	těžce
pokutováno	pokutován	k2eAgNnSc1d1	pokutováno
králem	král	k1gMnSc7	král
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
ve	v	k7c6	v
stavovské	stavovský	k2eAgFnSc6d1	stavovská
vzpouře	vzpoura	k1gFnSc6	vzpoura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vývoji	vývoj	k1gInSc6	vývoj
města	město	k1gNnSc2	město
škodily	škodit	k5eAaImAgFnP	škodit
požáry	požár	k1gInPc4	požár
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1579	[number]	k4	1579
<g/>
,	,	kIx,	,
1587	[number]	k4	1587
<g/>
,	,	kIx,	,
1589	[number]	k4	1589
<g/>
,	,	kIx,	,
1597	[number]	k4	1597
a	a	k8xC	a
1617	[number]	k4	1617
a	a	k8xC	a
mor	mor	k1gInSc1	mor
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1568	[number]	k4	1568
<g/>
,	,	kIx,	,
1582	[number]	k4	1582
<g/>
,	,	kIx,	,
1598	[number]	k4	1598
a	a	k8xC	a
1613	[number]	k4	1613
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1618	[number]	k4	1618
se	se	k3xPyFc4	se
kolínští	kolínský	k2eAgMnPc1d1	kolínský
účastnili	účastnit	k5eAaImAgMnP	účastnit
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
byli	být	k5eAaImAgMnP	být
postiženi	postihnout	k5eAaPmNgMnP	postihnout
konfiskací	konfiskace	k1gFnSc7	konfiskace
městských	městský	k2eAgInPc2d1	městský
statků	statek	k1gInPc2	statek
a	a	k8xC	a
odejmutím	odejmutí	k1gNnSc7	odejmutí
mnoha	mnoho	k4c2	mnoho
privilegií	privilegium	k1gNnPc2	privilegium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1628	[number]	k4	1628
město	město	k1gNnSc1	město
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
za	za	k7c2	za
katolické	katolický	k2eAgFnSc2d1	katolická
a	a	k8xC	a
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
mu	on	k3xPp3gMnSc3	on
vrátil	vrátit	k5eAaPmAgMnS	vrátit
většinu	většina	k1gFnSc4	většina
privilegií	privilegium	k1gNnPc2	privilegium
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgInS	vrátit
i	i	k9	i
zabavené	zabavený	k2eAgInPc4d1	zabavený
statky	statek	k1gInPc4	statek
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
velké	velký	k2eAgFnPc4d1	velká
škody	škoda	k1gFnPc4	škoda
v	v	k7c6	v
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vydrancováno	vydrancovat	k5eAaPmNgNnS	vydrancovat
Švédy	švéda	k1gFnSc2	švéda
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1634	[number]	k4	1634
<g/>
,	,	kIx,	,
1639	[number]	k4	1639
<g/>
–	–	k?	–
<g/>
1640	[number]	k4	1640
<g/>
,	,	kIx,	,
1643	[number]	k4	1643
a	a	k8xC	a
1648	[number]	k4	1648
<g/>
.	.	kIx.	.
</s>
<s>
Napadené	napadený	k2eAgFnPc1d1	napadená
epidemií	epidemie	k1gFnSc7	epidemie
moru	mor	k1gInSc2	mor
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1625	[number]	k4	1625
<g/>
,	,	kIx,	,
1633	[number]	k4	1633
<g/>
,	,	kIx,	,
1640	[number]	k4	1640
a	a	k8xC	a
1649	[number]	k4	1649
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1748	[number]	k4	1748
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
rakouské	rakouský	k2eAgNnSc4d1	rakouské
dědictví	dědictví	k1gNnSc4	dědictví
opět	opět	k6eAd1	opět
poškodila	poškodit	k5eAaPmAgFnS	poškodit
silně	silně	k6eAd1	silně
město	město	k1gNnSc4	město
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
další	další	k2eAgFnSc1d1	další
sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
letech	let	k1gInPc6	let
1756	[number]	k4	1756
<g/>
–	–	k?	–
<g/>
1763	[number]	k4	1763
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
svedena	sveden	k2eAgFnSc1d1	svedena
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kolína	Kolín	k1gInSc2	Kolín
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1757	[number]	k4	1757
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
pruský	pruský	k2eAgMnSc1d1	pruský
král	král	k1gMnSc1	král
Bedřich	Bedřich	k1gMnSc1	Bedřich
II	II	kA	II
<g/>
.	.	kIx.	.
poražen	porazit	k5eAaPmNgInS	porazit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uklidnění	uklidnění	k1gNnSc1	uklidnění
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
přinesly	přinést	k5eAaPmAgFnP	přinést
městu	město	k1gNnSc3	město
až	až	k8xS	až
reformy	reforma	k1gFnSc2	reforma
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1796	[number]	k4	1796
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc1d1	celé
zničeno	zničen	k2eAgNnSc1d1	zničeno
velkým	velký	k2eAgInSc7d1	velký
požárem	požár	k1gInSc7	požár
<g/>
.	.	kIx.	.
1815	[number]	k4	1815
po	po	k7c6	po
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
město	město	k1gNnSc1	město
růst	růst	k5eAaImF	růst
a	a	k8xC	a
blahobyt	blahobyt	k1gInSc4	blahobyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
byl	být	k5eAaImAgInS	být
Kolín	Kolín	k1gInSc1	Kolín
napojen	napojit	k5eAaPmNgInS	napojit
na	na	k7c4	na
důležitou	důležitý	k2eAgFnSc4d1	důležitá
železnici	železnice	k1gFnSc4	železnice
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Olomoucí	Olomouc	k1gFnSc7	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
rozkvět	rozkvět	k1gInSc4	rozkvět
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
prošlo	projít	k5eAaPmAgNnS	projít
město	město	k1gNnSc1	město
rychlou	rychlý	k2eAgFnSc7d1	rychlá
industrializací	industrializace	k1gFnSc7	industrializace
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
potravinářské	potravinářský	k2eAgInPc1d1	potravinářský
závody	závod	k1gInPc1	závod
(	(	kIx(	(
<g/>
olejny	olejna	k1gFnPc1	olejna
<g/>
,	,	kIx,	,
lihovary	lihovar	k1gInPc1	lihovar
<g/>
,	,	kIx,	,
cukrovary	cukrovar	k1gInPc1	cukrovar
<g/>
,	,	kIx,	,
pivovary	pivovar	k1gInPc1	pivovar
<g/>
,	,	kIx,	,
parní	parní	k2eAgInPc1d1	parní
mlýny	mlýn	k1gInPc1	mlýn
<g/>
)	)	kIx)	)
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
úrodným	úrodný	k2eAgNnSc7d1	úrodné
zemědělským	zemědělský	k2eAgNnSc7d1	zemědělské
zázemím	zázemí	k1gNnSc7	zázemí
města	město	k1gNnSc2	město
a	a	k8xC	a
lučební	lučební	k2eAgFnSc1d1	lučební
továrna	továrna	k1gFnSc1	továrna
vyrábějící	vyrábějící	k2eAgFnSc1d1	vyrábějící
pro	pro	k7c4	pro
rolníky	rolník	k1gMnPc4	rolník
hnojiva	hnojivo	k1gNnSc2	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
předchozí	předchozí	k2eAgInPc4d1	předchozí
závody	závod	k1gInPc4	závod
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc4d1	nová
továrny	továrna	k1gFnPc4	továrna
s	s	k7c7	s
progresivními	progresivní	k2eAgInPc7d1	progresivní
obory	obor	k1gInPc7	obor
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
především	především	k9	především
s	s	k7c7	s
chemickým	chemický	k2eAgNnSc7d1	chemické
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
byl	být	k5eAaImAgInS	být
Kolín	Kolín	k1gInSc1	Kolín
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
měst	město	k1gNnPc2	město
s	s	k7c7	s
vlasteneckým	vlastenecký	k2eAgInSc7d1	vlastenecký
ruchem	ruch	k1gInSc7	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1890	[number]	k4	1890
přednášel	přednášet	k5eAaImAgMnS	přednášet
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
Prof.	prof.	kA	prof.
T.G.	T.G.	k1gMnSc1	T.G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
posluchači	posluchač	k1gMnPc7	posluchač
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
početné	početný	k2eAgNnSc1d1	početné
zastoupení	zastoupení	k1gNnSc1	zastoupení
židovské	židovský	k2eAgFnSc2d1	židovská
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
přikláněla	přiklánět	k5eAaImAgFnS	přiklánět
spíše	spíše	k9	spíše
ke	k	k7c3	k
staročechům	staročech	k1gMnPc3	staročech
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
navštívená	navštívený	k2eAgFnSc1d1	navštívená
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
přednáška	přednáška	k1gFnSc1	přednáška
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
Občanském	občanský	k2eAgInSc6d1	občanský
klubu	klub	k1gInSc6	klub
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1895	[number]	k4	1895
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
480	[number]	k4	480
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
Husova	Husův	k2eAgNnSc2d1	Husovo
upálení	upálení	k1gNnSc2	upálení
<g/>
.	.	kIx.	.
</s>
<s>
Přednášku	přednáška	k1gFnSc4	přednáška
připravil	připravit	k5eAaPmAgMnS	připravit
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
naše	náš	k3xOp1gNnSc4	náš
obrození	obrození	k1gNnSc4	obrození
a	a	k8xC	a
naše	náš	k3xOp1gFnPc4	náš
reformace	reformace	k1gFnPc4	reformace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
elektrárna	elektrárna	k1gFnSc1	elektrárna
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
komínem	komín	k1gInSc7	komín
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárnu	elektrárna	k1gFnSc4	elektrárna
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
architekt	architekt	k1gMnSc1	architekt
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Fragner	Fragner	k1gMnSc1	Fragner
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgInPc2d1	další
objektů	objekt	k1gInPc2	objekt
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
zvonu	zvon	k1gInSc6	zvon
===	===	k?	===
</s>
</p>
<p>
<s>
Rytíř	Rytíř	k1gMnSc1	Rytíř
Vužan	Vužan	k1gMnSc1	Vužan
se	se	k3xPyFc4	se
chystal	chystat	k5eAaImAgMnS	chystat
vykonat	vykonat	k5eAaPmF	vykonat
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
mezitím	mezitím	k6eAd1	mezitím
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
nezapomněla	zapomenout	k5eNaPmAgFnS	zapomenout
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
jí	jíst	k5eAaImIp3nS	jíst
ulít	ulít	k5eAaPmF	ulít
zvon	zvon	k1gInSc4	zvon
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
prý	prý	k9	prý
do	do	k7c2	do
zvonoviny	zvonovina	k1gFnSc2	zvonovina
vhodila	vhodit	k5eAaPmAgFnS	vhodit
několik	několik	k4yIc4	několik
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
mincí	mince	k1gFnPc2	mince
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hlasu	hlas	k1gInSc3	hlas
zvonu	zvon	k1gInSc2	zvon
přidala	přidat	k5eAaPmAgFnS	přidat
na	na	k7c6	na
mohutnosti	mohutnost	k1gFnSc6	mohutnost
<g/>
.	.	kIx.	.
</s>
<s>
Rytíř	Rytíř	k1gMnSc1	Rytíř
pravil	pravit	k5eAaImAgMnS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
rytíř	rytíř	k1gMnSc1	rytíř
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
ani	ani	k8xC	ani
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
žena	žena	k1gFnSc1	žena
začala	začít	k5eAaPmAgFnS	začít
pomýšlet	pomýšlet	k5eAaImF	pomýšlet
na	na	k7c4	na
nové	nový	k2eAgFnPc4d1	nová
vdavky	vdavka	k1gFnPc4	vdavka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
kráčela	kráčet	k5eAaImAgFnS	kráčet
s	s	k7c7	s
ženichem	ženich	k1gMnSc7	ženich
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
myslela	myslet	k5eAaImAgFnS	myslet
více	hodně	k6eAd2	hodně
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
ztraceného	ztracený	k2eAgMnSc4d1	ztracený
muže	muž	k1gMnSc4	muž
než	než	k8xS	než
na	na	k7c4	na
nastávající	nastávající	k2eAgFnSc4d1	nastávající
svatbu	svatba	k1gFnSc4	svatba
<g/>
,	,	kIx,	,
když	když	k8xS	když
náhle	náhle	k6eAd1	náhle
začal	začít	k5eAaPmAgInS	začít
její	její	k3xOp3gInSc4	její
zvon	zvon	k1gInSc4	zvon
vyzvánět	vyzvánět	k5eAaImF	vyzvánět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hola	hola	k1gFnSc1	hola
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
z	z	k7c2	z
vojny	vojna	k1gFnSc2	vojna
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
můj	můj	k3xOp1gMnSc1	můj
pán	pán	k1gMnSc1	pán
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Rytíř	Rytíř	k1gMnSc1	Rytíř
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
šťastně	šťastně	k6eAd1	šťastně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dlouho	dlouho	k6eAd1	dlouho
mohl	moct	k5eAaImAgInS	moct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvon	zvon	k1gInSc1	zvon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
spadl	spadnout	k5eAaPmAgInS	spadnout
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
roztloukl	roztlouct	k5eAaPmAgMnS	roztlouct
se	se	k3xPyFc4	se
na	na	k7c4	na
kusy	kus	k1gInPc4	kus
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přelit	přelít	k5eAaPmNgInS	přelít
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
Vužanovi	Vužan	k1gMnSc6	Vužan
zachytil	zachytit	k5eAaPmAgMnS	zachytit
Josef	Josef	k1gMnSc1	Josef
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Machar	Machar	k1gMnSc1	Machar
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Zvon	zvon	k1gInSc4	zvon
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
sbírce	sbírka	k1gFnSc6	sbírka
Golgatha	Golgatha	k1gMnSc1	Golgatha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rok	rok	k1gInSc1	rok
1932	[number]	k4	1932
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kolín	Kolín	k1gInSc1	Kolín
(	(	kIx(	(
<g/>
18509	[number]	k4	18509
obyvatel	obyvatel	k1gMnSc1	obyvatel
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Instituce	instituce	k1gFnSc1	instituce
<g/>
:	:	kIx,	:
okresní	okresní	k2eAgInSc1d1	okresní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
berní	berní	k2eAgFnSc1d1	berní
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
berní	berní	k2eAgInSc1d1	berní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
celní	celní	k2eAgInSc1d1	celní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
cejchovní	cejchovní	k2eAgInSc1d1	cejchovní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgFnSc1d1	finanční
technická	technický	k2eAgFnSc1d1	technická
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
,	,	kIx,	,
důchodkový	důchodkový	k2eAgInSc1d1	důchodkový
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgInSc4d1	katastrální
zeměměřičský	zeměměřičský	k2eAgInSc4d1	zeměměřičský
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgNnSc4d1	okresní
četnické	četnický	k2eAgNnSc4d1	četnické
velitelství	velitelství	k1gNnSc4	velitelství
<g/>
,	,	kIx,	,
poštovní	poštovní	k2eAgInSc4d1	poštovní
úřad	úřad	k1gInSc4	úřad
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
telegrafní	telegrafní	k2eAgInSc1d1	telegrafní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
telefonní	telefonní	k2eAgInSc1d1	telefonní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
3	[number]	k4	3
katolické	katolický	k2eAgInPc1d1	katolický
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
československé	československý	k2eAgFnSc2d1	Československá
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
státní	státní	k2eAgNnSc1d1	státní
reálné	reálný	k2eAgNnSc1d1	reálné
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
zemská	zemský	k2eAgFnSc1d1	zemská
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgInSc1d1	okresní
chudobinec	chudobinec	k1gInSc1	chudobinec
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgFnSc1d1	okresní
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
<g/>
:	:	kIx,	:
obchodní	obchodní	k2eAgNnSc1d1	obchodní
grémium	grémium	k1gNnSc1	grémium
<g/>
,	,	kIx,	,
společenstvo	společenstvo	k1gNnSc1	společenstvo
cukrářů	cukrář	k1gMnPc2	cukrář
<g/>
,	,	kIx,	,
holičů	holič	k1gMnPc2	holič
<g/>
,	,	kIx,	,
hostinských	hostinský	k1gMnPc2	hostinský
<g/>
,	,	kIx,	,
kolářů	kolář	k1gMnPc2	kolář
<g/>
,	,	kIx,	,
kovářů	kovář	k1gMnPc2	kovář
<g/>
,	,	kIx,	,
lakýrníků	lakýrník	k1gMnPc2	lakýrník
<g/>
,	,	kIx,	,
malířů	malíř	k1gMnPc2	malíř
pokojů	pokoj	k1gInPc2	pokoj
<g/>
,	,	kIx,	,
obuvníků	obuvník	k1gMnPc2	obuvník
<g/>
,	,	kIx,	,
pekařů	pekař	k1gMnPc2	pekař
<g/>
,	,	kIx,	,
řezníků	řezník	k1gMnPc2	řezník
a	a	k8xC	a
uzenářů	uzenář	k1gMnPc2	uzenář
<g/>
,	,	kIx,	,
sedlářů	sedlář	k1gMnPc2	sedlář
a	a	k8xC	a
čalouníků	čalouník	k1gMnPc2	čalouník
<g/>
,	,	kIx,	,
stavebních	stavební	k2eAgFnPc2d1	stavební
živností	živnost	k1gFnPc2	živnost
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
živností	živnost	k1gFnSc7	živnost
oděvních	oděvní	k2eAgInPc2d1	oděvní
a	a	k8xC	a
živností	živnost	k1gFnSc7	živnost
kovy	kov	k1gInPc7	kov
zpracujících	zpracující	k2eAgInPc2d1	zpracující
a	a	k8xC	a
společenstvo	společenstvo	k1gNnSc1	společenstvo
truhlářů	truhlář	k1gMnPc2	truhlář
<g/>
,	,	kIx,	,
řezbářů	řezbář	k1gMnPc2	řezbář
<g/>
,	,	kIx,	,
soustružníků	soustružník	k1gMnPc2	soustružník
a	a	k8xC	a
dřevozvorkářů	dřevozvorkář	k1gMnPc2	dřevozvorkář
<g/>
,	,	kIx,	,
7	[number]	k4	7
cihelen	cihelna	k1gFnPc2	cihelna
<g/>
,	,	kIx,	,
cukrovar	cukrovar	k1gInSc1	cukrovar
<g/>
,	,	kIx,	,
3	[number]	k4	3
továrny	továrna	k1gFnPc4	továrna
na	na	k7c4	na
cukrovinky	cukrovinka	k1gFnPc4	cukrovinka
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
továrna	továrna	k1gFnSc1	továrna
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
káble	kábl	k1gInSc5	kábl
<g/>
,	,	kIx,	,
na	na	k7c4	na
kávoviny	kávovina	k1gFnPc4	kávovina
<g/>
,	,	kIx,	,
2	[number]	k4	2
lihovary	lihovar	k1gInPc1	lihovar
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
nábytek	nábytek	k1gInSc4	nábytek
<g/>
,	,	kIx,	,
na	na	k7c4	na
minerální	minerální	k2eAgInPc4d1	minerální
oleje	olej	k1gInPc4	olej
Vacuum	Vacuum	k1gNnSc4	Vacuum
Oil	Oil	k1gFnSc2	Oil
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
na	na	k7c4	na
váhy	váha	k1gFnPc4	váha
<g/>
,	,	kIx,	,
vozy	vůz	k1gInPc4	vůz
a	a	k8xC	a
železářské	železářský	k2eAgNnSc4d1	železářské
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
mýdla	mýdlo	k1gNnSc2	mýdlo
Hellada	Hellada	k1gFnSc1	Hellada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Služby	služba	k1gFnPc1	služba
<g/>
:	:	kIx,	:
19	[number]	k4	19
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
4	[number]	k4	4
zubní	zubní	k2eAgMnPc1d1	zubní
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
3	[number]	k4	3
zvěrolékaři	zvěrolékař	k1gMnPc7	zvěrolékař
<g/>
,	,	kIx,	,
13	[number]	k4	13
advokátů	advokát	k1gMnPc2	advokát
<g/>
,	,	kIx,	,
2	[number]	k4	2
notáři	notář	k1gMnPc7	notář
<g/>
,	,	kIx,	,
Anglo-československá	Anglo-československý	k2eAgFnSc1d1	Anglo-československá
a	a	k8xC	a
Pražská	pražský	k2eAgFnSc1d1	Pražská
úvěrní	úvěrní	k2eAgFnSc1d1	úvěrní
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
Banka	banka	k1gFnSc1	banka
československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
<g/>
,	,	kIx,	,
Moravská	moravský	k2eAgFnSc1d1	Moravská
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
Živnostenská	živnostenský	k2eAgFnSc1d1	Živnostenská
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
filmů	film	k1gInPc2	film
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2	[number]	k4	2
geometři	geometr	k1gMnPc1	geometr
<g/>
,	,	kIx,	,
8	[number]	k4	8
hotelů	hotel	k1gInPc2	hotel
(	(	kIx(	(
<g/>
Central	Central	k1gMnSc1	Central
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
<g/>
,	,	kIx,	,
Lidový	lidový	k2eAgInSc1d1	lidový
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
Pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
U	u	k7c2	u
černého	černé	k1gNnSc2	černé
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
U	u	k7c2	u
Přemysla	Přemysl	k1gMnSc2	Přemysl
<g/>
,	,	kIx,	,
Veselý	veselý	k2eAgMnSc1d1	veselý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
kavárny	kavárna	k1gFnPc1	kavárna
(	(	kIx(	(
<g/>
Arco	Arco	k1gNnSc1	Arco
<g/>
,	,	kIx,	,
Lidový	lidový	k2eAgInSc1d1	lidový
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
U	u	k7c2	u
Amerikána	amerikán	k1gInSc2	amerikán
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
lékárny	lékárna	k1gFnSc2	lékárna
U	u	k7c2	u
zlaté	zlatý	k2eAgFnSc2d1	zlatá
Koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
U	u	k7c2	u
zlatého	zlatý	k2eAgInSc2d1	zlatý
lva	lev	k1gInSc2	lev
<g/>
,	,	kIx,	,
U	u	k7c2	u
černého	černé	k1gNnSc2	černé
orla	orel	k1gMnSc2	orel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
Lidová	lidový	k2eAgFnSc1d1	lidová
záložna	záložna	k1gFnSc1	záložna
<g/>
,	,	kIx,	,
Okresní	okresní	k2eAgFnSc1d1	okresní
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
záložna	záložna	k1gFnSc1	záložna
<g/>
,	,	kIx,	,
Reifeissenova	Reifeissenův	k2eAgFnSc1d1	Reifeissenův
záložna	záložna	k1gFnSc1	záložna
<g/>
,	,	kIx,	,
Spořitelna	spořitelna	k1gFnSc1	spořitelna
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Středočeská	středočeský	k2eAgFnSc1d1	Středočeská
záložna	záložna	k1gFnSc1	záložna
<g/>
,	,	kIx,	,
Záložna	záložna	k1gFnSc1	záložna
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
<g/>
,	,	kIx,	,
Živnostenská	živnostenský	k2eAgFnSc1d1	Živnostenská
záložna	záložna	k1gFnSc1	záložna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Sendražice	Sendražice	k1gFnSc2	Sendražice
(	(	kIx(	(
<g/>
přísl	přísl	k1gInSc1	přísl
<g/>
.	.	kIx.	.
</s>
<s>
Františkov	Františkov	k1gInSc1	Františkov
<g/>
,	,	kIx,	,
1551	[number]	k4	1551
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Kolína	Kolín	k1gInSc2	Kolín
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
2	[number]	k4	2
holiči	holič	k1gMnPc7	holič
<g/>
,	,	kIx,	,
4	[number]	k4	4
hostince	hostinec	k1gInPc1	hostinec
<g/>
,	,	kIx,	,
jednatelství	jednatelství	k1gNnPc1	jednatelství
<g/>
,	,	kIx,	,
kolář	kolář	k1gMnSc1	kolář
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
<g/>
,	,	kIx,	,
pekař	pekař	k1gMnSc1	pekař
<g/>
,	,	kIx,	,
2	[number]	k4	2
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
3	[number]	k4	3
řezníci	řezník	k1gMnPc1	řezník
<g/>
,	,	kIx,	,
sedlář	sedlář	k1gMnSc1	sedlář
<g/>
,	,	kIx,	,
8	[number]	k4	8
obchodů	obchod	k1gInPc2	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
2	[number]	k4	2
sběrny	sběrna	k1gFnPc4	sběrna
starého	starý	k2eAgInSc2d1	starý
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
2	[number]	k4	2
trafiky	trafika	k1gFnSc2	trafika
<g/>
,	,	kIx,	,
truhlář	truhlář	k1gMnSc1	truhlář
<g/>
,	,	kIx,	,
velkostatek	velkostatek	k1gInSc1	velkostatek
<g/>
,	,	kIx,	,
zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
<g/>
,	,	kIx,	,
zámečník	zámečník	k1gMnSc1	zámečník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Štítary	Štítara	k1gFnSc2	Štítara
(	(	kIx(	(
<g/>
380	[number]	k4	380
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
ves	ves	k1gFnSc1	ves
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Kolína	Kolín	k1gInSc2	Kolín
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
cukrář	cukrář	k1gMnSc1	cukrář
<g/>
,	,	kIx,	,
obecní	obecní	k2eAgFnSc1d1	obecní
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
,	,	kIx,	,
2	[number]	k4	2
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
11	[number]	k4	11
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
2	[number]	k4	2
sadaři	sadař	k1gMnPc7	sadař
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
spořitelní	spořitelní	k2eAgFnPc1d1	spořitelní
a	a	k8xC	a
záložní	záložní	k2eAgInSc1d1	záložní
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
Štítary	Štítara	k1gFnPc4	Štítara
<g/>
,	,	kIx,	,
trafika	trafika	k1gFnSc1	trafika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Zibohlavy	Zibohlava	k1gFnSc2	Zibohlava
(	(	kIx(	(
<g/>
277	[number]	k4	277
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
ves	ves	k1gFnSc1	ves
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Kolína	Kolín	k1gInSc2	Kolín
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
3	[number]	k4	3
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kolář	kolář	k1gMnSc1	kolář
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
<g/>
,	,	kIx,	,
4	[number]	k4	4
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
trafika	trafika	k1gFnSc1	trafika
<g/>
,	,	kIx,	,
vápenka	vápenka	k1gFnSc1	vápenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Kolín	Kolín	k1gInSc1	Kolín
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
asi	asi	k9	asi
60	[number]	k4	60
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
Labe	Labe	k1gNnSc2	Labe
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řeka	řeka	k1gFnSc1	řeka
velkým	velký	k2eAgInSc7d1	velký
obloukem	oblouk	k1gInSc7	oblouk
mění	měnit	k5eAaImIp3nS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
směr	směr	k1gInSc4	směr
ze	z	k7c2	z
západního	západní	k2eAgInSc2d1	západní
na	na	k7c4	na
severní	severní	k2eAgInSc4d1	severní
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Polabské	polabský	k2eAgFnSc2d1	Polabská
nížiny	nížina	k1gFnSc2	nížina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
severně	severně	k6eAd1	severně
a	a	k8xC	a
východně	východně	k6eAd1	východně
od	od	k7c2	od
města	město	k1gNnSc2	město
a	a	k8xC	a
náleží	náležet	k5eAaImIp3nS	náležet
ke	k	k7c3	k
Středolabské	středolabský	k2eAgFnSc3d1	Středolabská
tabuli	tabule	k1gFnSc3	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jihu	jih	k1gInSc2	jih
a	a	k8xC	a
západu	západ	k1gInSc2	západ
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
svažují	svažovat	k5eAaImIp3nP	svažovat
výběžky	výběžek	k1gInPc1	výběžek
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Pozvolně	pozvolně	k6eAd1	pozvolně
klesající	klesající	k2eAgFnSc1d1	klesající
rovina	rovina	k1gFnSc1	rovina
je	být	k5eAaImIp3nS	být
narušena	narušit	k5eAaPmNgFnS	narušit
údolími	údolí	k1gNnPc7	údolí
Polepského	Polepský	k2eAgInSc2d1	Polepský
a	a	k8xC	a
Pekelského	pekelský	k2eAgInSc2d1	pekelský
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodně	severovýchodně	k6eAd1	severovýchodně
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
k	k	k7c3	k
městu	město	k1gNnSc3	město
vrchem	vrchem	k6eAd1	vrchem
Vinice	vinice	k1gFnSc1	vinice
(	(	kIx(	(
<g/>
237	[number]	k4	237
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
Východolabská	Východolabský	k2eAgFnSc1d1	Východolabská
tabule	tabule	k1gFnSc1	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
skále	skála	k1gFnSc6	skála
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
lužní	lužní	k2eAgInPc1d1	lužní
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
státem	stát	k1gInSc7	stát
chráněny	chráněn	k2eAgInPc4d1	chráněn
(	(	kIx(	(
<g/>
Veltrubský	Veltrubský	k2eAgInSc1d1	Veltrubský
luh	luh	k1gInSc1	luh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Kolínské	kolínský	k2eAgFnSc2d1	Kolínská
tůně	tůně	k1gFnSc2	tůně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
10	[number]	k4	10
částí	část	k1gFnPc2	část
na	na	k7c6	na
čtyřech	čtyři	k4xCgNnPc6	čtyři
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
Kolín	Kolín	k1gInSc1	Kolín
I	i	k8xC	i
–	–	k?	–
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Památky	památka	k1gFnPc1	památka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
částmi	část	k1gFnPc7	část
Kolín	Kolín	k1gInSc1	Kolín
II	II	kA	II
<g/>
–	–	k?	–
<g/>
IV	IV	kA	IV
a	a	k8xC	a
Kolín	Kolín	k1gInSc1	Kolín
VI	VI	kA	VI
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolín	Kolín	k1gInSc1	Kolín
II	II	kA	II
–	–	k?	–
Pražské	pražský	k2eAgNnSc1d1	Pražské
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
západně	západně	k6eAd1	západně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejlidnatější	lidnatý	k2eAgFnSc4d3	nejlidnatější
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
největší	veliký	k2eAgNnSc4d3	veliký
kolínské	kolínský	k2eAgNnSc4d1	kolínské
sídliště	sídliště	k1gNnSc4	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
vodárna	vodárna	k1gFnSc1	vodárna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
již	již	k6eAd1	již
neplní	plnit	k5eNaImIp3nP	plnit
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
účel	účel	k1gInSc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolín	Kolín	k1gInSc1	Kolín
III	III	kA	III
–	–	k?	–
Kouřimské	kouřimský	k2eAgNnSc4d1	kouřimské
Předměstí	předměstí	k1gNnSc4	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Dominanta	dominanta	k1gFnSc1	dominanta
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolín	Kolín	k1gInSc1	Kolín
IV	IV	kA	IV
–	–	k?	–
Kutnohorské	kutnohorský	k2eAgNnSc4d1	kutnohorské
Předměstí	předměstí	k1gNnSc4	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
východně	východně	k6eAd1	východně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vlakové	vlakový	k2eAgNnSc4d1	vlakové
a	a	k8xC	a
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolín	Kolín	k1gInSc1	Kolín
V	v	k7c6	v
–	–	k?	–
Zálabí	zálabí	k1gNnSc6	zálabí
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
kolínské	kolínský	k2eAgNnSc1d1	kolínské
předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
vějířovitým	vějířovitý	k2eAgNnSc7d1	vějířovité
rozvržením	rozvržení	k1gNnSc7	rozvržení
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
narušeným	narušený	k2eAgNnSc7d1	narušené
výstavbou	výstavba	k1gFnSc7	výstavba
Nového	Nového	k2eAgInSc2d1	Nového
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hlavní	hlavní	k2eAgInSc1d1	hlavní
městský	městský	k2eAgInSc1d1	městský
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Zálabí	zálabí	k1gNnSc1	zálabí
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
ke	k	k7c3	k
Kolínu	Kolín	k1gInSc3	Kolín
v	v	k7c4	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
sedleckého	sedlecký	k2eAgInSc2d1	sedlecký
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
zaniklých	zaniklý	k2eAgFnPc2d1	zaniklá
obcí	obec	k1gFnPc2	obec
Brankovice	Brankovice	k1gFnSc2	Brankovice
a	a	k8xC	a
Mnichovice	Mnichovice	k1gFnPc4	Mnichovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolín	Kolín	k1gInSc1	Kolín
VI	VI	kA	VI
–	–	k?	–
Štítarské	Štítarský	k2eAgNnSc4d1	Štítarský
předměstí	předměstí	k1gNnSc4	předměstí
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
místními	místní	k2eAgFnPc7d1	místní
lidmi	člověk	k1gMnPc7	člověk
Vejfuk	Vejfuk	k1gMnSc1	Vejfuk
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
Kolínem	Kolín	k1gInSc7	Kolín
II	II	kA	II
a	a	k8xC	a
Štítary	Štítara	k1gFnSc2	Štítara
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k8xS	jako
vilová	vilový	k2eAgFnSc1d1	vilová
čtvrť	čtvrť	k1gFnSc1	čtvrť
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šťáralka	Šťáralka	k1gFnSc1	Šťáralka
–	–	k?	–
původně	původně	k6eAd1	původně
samostatná	samostatný	k2eAgFnSc1d1	samostatná
osada	osada	k1gFnSc1	osada
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
při	při	k7c6	při
hlavní	hlavní	k2eAgFnSc6d1	hlavní
silnici	silnice	k1gFnSc6	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
Čáslav	Čáslav	k1gFnSc4	Čáslav
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Kutnohorské	kutnohorský	k2eAgNnSc4d1	kutnohorské
předměstí	předměstí	k1gNnSc4	předměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Sendražice	Sendražice	k1gFnSc2	Sendražice
u	u	k7c2	u
Kolína	Kolín	k1gInSc2	Kolín
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Sendražice	Sendražice	k1gFnSc1	Sendražice
–	–	k?	–
původně	původně	k6eAd1	původně
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Ovčáry	Ovčáry	k1gInPc4	Ovčáry
<g/>
,	,	kIx,	,
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
Zálabí	zálabí	k1gNnSc6	zálabí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Štítary	Štítara	k1gFnSc2	Štítara
u	u	k7c2	u
Kolína	Kolín	k1gInSc2	Kolín
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Štítary	Štítara	k1gFnSc2	Štítara
–	–	k?	–
původně	původně	k6eAd1	původně
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Zibohlavy	Zibohlava	k1gFnSc2	Zibohlava
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Zibohlavy	Zibohlava	k1gFnSc2	Zibohlava
–	–	k?	–
původně	původně	k6eAd1	původně
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
konci	konec	k1gInSc6	konec
Štítarského	Štítarský	k2eAgNnSc2d1	Štítarský
údolí	údolí	k1gNnSc2	údolí
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
<g/>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
Radovesnice	Radovesnice	k1gFnSc2	Radovesnice
I.	I.	kA	I.
</s>
</p>
<p>
<s>
===	===	k?	===
Územněsprávní	územněsprávní	k2eAgNnSc4d1	územněsprávní
začlenění	začlenění	k1gNnSc4	začlenění
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
územněsprávního	územněsprávní	k2eAgNnSc2d1	územněsprávní
začleňování	začleňování	k1gNnSc2	začleňování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
období	období	k1gNnSc4	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chronologickém	chronologický	k2eAgInSc6d1	chronologický
přehledu	přehled	k1gInSc6	přehled
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
územně	územně	k6eAd1	územně
administrativní	administrativní	k2eAgFnSc4d1	administrativní
příslušnost	příslušnost	k1gFnSc4	příslušnost
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1850	[number]	k4	1850
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
1855	[number]	k4	1855
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
1868	[number]	k4	1868
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
1942	[number]	k4	1942
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
správní	správní	k2eAgInSc1d1	správní
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
Pražský	pražský	k2eAgInSc1d1	pražský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Zachovalo	zachovat	k5eAaPmAgNnS	zachovat
si	se	k3xPyFc3	se
původní	původní	k2eAgInSc4d1	původní
středověký	středověký	k2eAgInSc4d1	středověký
půdorys	půdorys	k1gInSc4	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
barokní	barokní	k2eAgInPc1d1	barokní
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
a	a	k8xC	a
kašna	kašna	k1gFnSc1	kašna
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
bývalé	bývalý	k2eAgNnSc4d1	bývalé
židovské	židovský	k2eAgNnSc4d1	Židovské
ghetto	ghetto	k1gNnSc4	ghetto
se	s	k7c7	s
synagogou	synagoga	k1gFnSc7	synagoga
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
obehnáno	obehnat	k5eAaPmNgNnS	obehnat
dvojitým	dvojitý	k2eAgInSc7d1	dvojitý
pásem	pás	k1gInSc7	pás
hradeb	hradba	k1gFnPc2	hradba
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
zachovaným	zachovaný	k2eAgMnPc3d1	zachovaný
<g/>
)	)	kIx)	)
s	s	k7c7	s
parkánem	parkán	k1gInSc7	parkán
<g/>
.	.	kIx.	.
</s>
<s>
Práchovna	Práchovna	k1gFnSc1	Práchovna
je	být	k5eAaImIp3nS	být
předsunutá	předsunutý	k2eAgFnSc1d1	předsunutá
součást	součást	k1gFnSc1	součást
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
sklad	sklad	k1gInSc4	sklad
střelného	střelný	k2eAgInSc2d1	střelný
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
raně	raně	k6eAd1	raně
gotický	gotický	k2eAgMnSc1d1	gotický
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
přestavbě	přestavba	k1gFnSc6	přestavba
v	v	k7c4	v
gotickou	gotický	k2eAgFnSc4d1	gotická
katedrálu	katedrála	k1gFnSc4	katedrála
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
podílel	podílet	k5eAaImAgMnS	podílet
Petr	Petr	k1gMnSc1	Petr
Parléř	Parléř	k1gMnSc1	Parléř
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
kostely	kostel	k1gInPc1	kostel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
barokní	barokní	k2eAgInSc4d1	barokní
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
na	na	k7c6	na
Zálabí	zálabí	k1gNnSc6	zálabí
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
gotický	gotický	k2eAgMnSc1d1	gotický
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
trojice	trojice	k1gFnSc2	trojice
s	s	k7c7	s
kapucínským	kapucínský	k2eAgInSc7d1	kapucínský
klášterem	klášter	k1gInSc7	klášter
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
nyní	nyní	k6eAd1	nyní
užívaný	užívaný	k2eAgInSc1d1	užívaný
pravoslavnou	pravoslavný	k2eAgFnSc7d1	pravoslavná
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
a	a	k8xC	a
ruiny	ruina	k1gFnPc1	ruina
kostela	kostel	k1gInSc2	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
u	u	k7c2	u
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
druhý	druhý	k4xOgInSc1	druhý
nejstarší	starý	k2eAgInSc1d3	nejstarší
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1418	[number]	k4	1418
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2600	[number]	k4	2600
náhrobky	náhrobek	k1gInPc7	náhrobek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
náhrobek	náhrobek	k1gInSc1	náhrobek
syna	syn	k1gMnSc2	syn
známého	známý	k1gMnSc2	známý
pražského	pražský	k2eAgMnSc2d1	pražský
rabína	rabín	k1gMnSc2	rabín
Löwa	Löwus	k1gMnSc2	Löwus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1599	[number]	k4	1599
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zálabí	zálabí	k1gNnSc6	zálabí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nový	nový	k2eAgInSc4d1	nový
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
poškozen	poškodit	k5eAaPmNgInS	poškodit
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
mostu	most	k1gInSc2	most
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
stojí	stát	k5eAaImIp3nS	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
obnovený	obnovený	k2eAgInSc1d1	obnovený
pomník	pomník	k1gInSc1	pomník
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
pod	pod	k7c7	pod
uhlím	uhlí	k1gNnSc7	uhlí
a	a	k8xC	a
starými	starý	k2eAgFnPc7d1	stará
pneumatikami	pneumatika	k1gFnPc7	pneumatika
ve	v	k7c6	v
sklepení	sklepení	k1gNnSc6	sklepení
Nelahozeveského	Nelahozeveský	k2eAgInSc2d1	Nelahozeveský
zámku	zámek	k1gInSc2	zámek
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pomníkem	pomník	k1gInSc7	pomník
TGM	TGM	kA	TGM
od	od	k7c2	od
Břetislava	Břetislav	k1gMnSc4	Břetislav
Bendy	Benda	k1gMnSc2	Benda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
pro	pro	k7c4	pro
Brandýs	Brandýs	k1gInSc4	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
obnovená	obnovený	k2eAgFnSc1d1	obnovená
Kolínská	kolínský	k2eAgFnSc1d1	Kolínská
řepařská	řepařský	k2eAgFnSc1d1	řepařská
drážka	drážka	k1gFnSc1	drážka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
dráze	dráha	k1gFnSc6	dráha
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
zaplatit	zaplatit	k5eAaPmF	zaplatit
jízdu	jízda	k1gFnSc4	jízda
kolem	kolem	k7c2	kolem
Kolína	Kolín	k1gInSc2	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
kolem	kolem	k7c2	kolem
čtyř	čtyři	k4xCgInPc2	čtyři
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Známou	známý	k2eAgFnSc7d1	známá
kolínskou	kolínský	k2eAgFnSc7d1	Kolínská
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
také	také	k9	také
již	již	k6eAd1	již
nefunkční	funkční	k2eNgFnSc1d1	nefunkční
kolínská	kolínský	k2eAgFnSc1d1	Kolínská
vodárna	vodárna	k1gFnSc1	vodárna
a	a	k8xC	a
také	také	k9	také
mezi	mezi	k7c7	mezi
místními	místní	k2eAgFnPc7d1	místní
známý	známý	k2eAgMnSc1d1	známý
"	"	kIx"	"
<g/>
šestnáctipatrák	šestnáctipatrák	k1gMnSc1	šestnáctipatrák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
kolínská	kolínský	k2eAgFnSc1d1	Kolínská
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
přízemí	přízemí	k1gNnSc6	přízemí
je	být	k5eAaImIp3nS	být
lékárna	lékárna	k1gFnSc1	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
architektonicky	architektonicky	k6eAd1	architektonicky
cenné	cenný	k2eAgFnPc4d1	cenná
meziválečné	meziválečný	k2eAgFnPc4d1	meziválečná
budovy	budova	k1gFnPc4	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kolínského	kolínský	k2eAgNnSc2d1	kolínské
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
dynamicky	dynamicky	k6eAd1	dynamicky
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
jako	jako	k9	jako
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
centrum	centrum	k1gNnSc1	centrum
středního	střední	k2eAgNnSc2d1	střední
Polabí	Polabí	k1gNnSc2	Polabí
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
východních	východní	k2eAgFnPc2d1	východní
středních	střední	k2eAgFnPc2d1	střední
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
zachovala	zachovat	k5eAaPmAgFnS	zachovat
řada	řada	k1gFnSc1	řada
industriálních	industriální	k2eAgFnPc2d1	industriální
památek	památka	k1gFnPc2	památka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zámecký	zámecký	k2eAgInSc1d1	zámecký
pivovar	pivovar	k1gInSc1	pivovar
se	s	k7c7	s
sladovnou	sladovna	k1gFnSc7	sladovna
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
kolínského	kolínský	k2eAgInSc2d1	kolínský
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1531	[number]	k4	1531
<g/>
,	,	kIx,	,
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
významným	významný	k2eAgMnSc7d1	významný
hospodářským	hospodářský	k2eAgMnSc7d1	hospodářský
odborníkem	odborník	k1gMnSc7	odborník
Františkem	František	k1gMnSc7	František
Horským	Horský	k1gMnSc7	Horský
z	z	k7c2	z
Horskyfeldu	Horskyfeld	k1gInSc2	Horskyfeld
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1865	[number]	k4	1865
až	až	k9	až
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc1	některý
budovy	budova	k1gFnPc1	budova
opravené	opravený	k2eAgFnPc1d1	opravená
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc1d1	jiné
město	město	k1gNnSc1	město
nechalo	nechat	k5eAaPmAgNnS	nechat
nesmyslně	smyslně	k6eNd1	smyslně
zbořit	zbořit	k5eAaPmF	zbořit
<g/>
.	.	kIx.	.
<g/>
Radimského	Radimský	k2eAgMnSc2d1	Radimský
či	či	k8xC	či
tzv.	tzv.	kA	tzv.
podskalský	podskalský	k2eAgInSc1d1	podskalský
mlýn	mlýn	k1gInSc1	mlýn
na	na	k7c6	na
podskalském	podskalský	k2eAgNnSc6d1	Podskalské
nábřeží	nábřeží	k1gNnSc6	nábřeží
na	na	k7c6	na
Zálabí	zálabí	k1gNnSc6	zálabí
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1285	[number]	k4	1285
<g/>
,	,	kIx,	,
přestavby	přestavba	k1gFnPc1	přestavba
1870	[number]	k4	1870
a	a	k8xC	a
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Mlýn	mlýn	k1gInSc1	mlýn
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
provozován	provozovat	k5eAaImNgInS	provozovat
proslulou	proslulý	k2eAgFnSc7d1	proslulá
kolínskou	kolínský	k2eAgFnSc7d1	Kolínská
rodinou	rodina	k1gFnSc7	rodina
Radimských	Radimský	k2eAgInPc2d1	Radimský
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
i	i	k9	i
významní	významný	k2eAgMnPc1d1	významný
politici	politik	k1gMnPc1	politik
či	či	k8xC	či
umělci	umělec	k1gMnPc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
představitelem	představitel	k1gMnSc7	představitel
je	být	k5eAaImIp3nS	být
malíř	malíř	k1gMnSc1	malíř
Václav	Václav	k1gMnSc1	Václav
Radimský	Radimský	k2eAgMnSc1d1	Radimský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
mlýn	mlýn	k1gInSc1	mlýn
udržovaný	udržovaný	k2eAgInSc1d1	udržovaný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
chátrá	chátrat	k5eAaImIp3nS	chátrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formánkův	Formánkův	k2eAgInSc1d1	Formánkův
mlýn	mlýn	k1gInSc1	mlýn
se	s	k7c7	s
strojírnou	strojírna	k1gFnSc7	strojírna
a	a	k8xC	a
slévárnou	slévárna	k1gFnSc7	slévárna
v	v	k7c6	v
Rybářské	rybářský	k2eAgFnSc6d1	rybářská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1285	[number]	k4	1285
<g/>
,	,	kIx,	,
přestavba	přestavba	k1gFnSc1	přestavba
1879	[number]	k4	1879
<g/>
.	.	kIx.	.
</s>
<s>
Majitelem	majitel	k1gMnSc7	majitel
byl	být	k5eAaImAgMnS	být
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Josef	Josef	k1gMnSc1	Josef
Formánek	Formánek	k1gMnSc1	Formánek
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1868	[number]	k4	1868
až	až	k9	až
1890	[number]	k4	1890
kolínský	kolínský	k2eAgMnSc1d1	kolínský
starosta	starosta	k1gMnSc1	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
využíváno	využívat	k5eAaPmNgNnS	využívat
jako	jako	k9	jako
sklady	sklad	k1gInPc1	sklad
a	a	k8xC	a
dílny	dílna	k1gFnPc1	dílna
pro	pro	k7c4	pro
menší	malý	k2eAgInPc4d2	menší
podniky	podnik	k1gInPc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
chátrá	chátrat	k5eAaImIp3nS	chátrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolínská	kolínský	k2eAgFnSc1d1	Kolínská
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
kávové	kávový	k2eAgFnPc4d1	kávová
náhražky	náhražka	k1gFnPc4	náhražka
<g/>
,	,	kIx,	,
proslulá	proslulý	k2eAgFnSc1d1	proslulá
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
kolínská	kolínská	k1gFnSc1	kolínská
cikorka	cikorka	k1gFnSc1	cikorka
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
známým	známý	k1gMnSc7	známý
politikem	politik	k1gMnSc7	politik
a	a	k8xC	a
architektem	architekt	k1gMnSc7	architekt
Čeňkem	Čeněk	k1gMnSc7	Čeněk
Křičkou	Křička	k1gMnSc7	Křička
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
výrou	výrá	k1gFnSc4	výrá
cukrovinek	cukrovinka	k1gFnPc2	cukrovinka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
přistavěno	přistavět	k5eAaPmNgNnS	přistavět
silo	sít	k5eAaImAgNnS	sít
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
funkční	funkční	k2eAgInSc1d1	funkční
s	s	k7c7	s
mimořádně	mimořádně	k6eAd1	mimořádně
zachovalými	zachovalý	k2eAgInPc7d1	zachovalý
secesními	secesní	k2eAgInPc7d1	secesní
prvky	prvek	k1gInPc7	prvek
a	a	k8xC	a
35	[number]	k4	35
metrů	metr	k1gInPc2	metr
vysokým	vysoký	k2eAgInSc7d1	vysoký
cihlovým	cihlový	k2eAgInSc7d1	cihlový
komínem	komín	k1gInSc7	komín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vavruškova	Vavruškův	k2eAgFnSc1d1	Vavruškův
sodovkárna	sodovkárna	k1gFnSc1	sodovkárna
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
původně	původně	k6eAd1	původně
založen	založit	k5eAaPmNgInS	založit
jako	jako	k8xS	jako
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
lisování	lisování	k1gNnSc4	lisování
oleje	olej	k1gInSc2	olej
Ignáce	Ignác	k1gMnSc2	Ignác
Selikovského	Selikovský	k2eAgMnSc2d1	Selikovský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
převzato	převzít	k5eAaPmNgNnS	převzít
bratry	bratr	k1gMnPc7	bratr
Fischery	Fischer	k1gMnPc4	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
objekt	objekt	k1gInSc4	objekt
koupil	koupit	k5eAaPmAgMnS	koupit
Otakar	Otakar	k1gMnSc1	Otakar
Vavruška	Vavruška	k1gMnSc1	Vavruška
a	a	k8xC	a
rozjel	rozjet	k5eAaPmAgMnS	rozjet
zde	zde	k6eAd1	zde
výrobu	výroba	k1gFnSc4	výroba
limonád	limonáda	k1gFnPc2	limonáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Koli	Kol	k1gFnSc2	Kol
probíhá	probíhat	k5eAaImIp3nS	probíhat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
přístavby	přístavba	k1gFnPc1	přístavba
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
budov	budova	k1gFnPc2	budova
se	se	k3xPyFc4	se
však	však	k9	však
dochovala	dochovat	k5eAaPmAgFnS	dochovat
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společná	společný	k2eAgFnSc1d1	společná
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
pardubickým	pardubický	k2eAgMnSc7d1	pardubický
stavitelem	stavitel	k1gMnSc7	stavitel
Karlem	Karel	k1gMnSc7	Karel
Krátkým	Krátký	k1gMnSc7	Krátký
východně	východně	k6eAd1	východně
od	od	k7c2	od
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
budov	budova	k1gFnPc2	budova
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
demolici	demolice	k1gFnSc4	demolice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
zachována	zachován	k2eAgFnSc1d1	zachována
pouze	pouze	k6eAd1	pouze
malá	malý	k2eAgFnSc1d1	malá
výrobní	výrobní	k2eAgFnSc1d1	výrobní
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
pro	pro	k7c4	pro
úředníky	úředník	k1gMnPc4	úředník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
cukrovinky	cukrovinka	k1gFnPc4	cukrovinka
a	a	k8xC	a
zboží	zboží	k1gNnSc4	zboží
čokoládové	čokoládový	k2eAgNnSc4d1	čokoládové
Kolinea	Kolinea	k1gFnSc1	Kolinea
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
východně	východně	k6eAd1	východně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
Karlem	Karel	k1gMnSc7	Karel
Cyvínem	Cyvín	k1gMnSc7	Cyvín
<g/>
,	,	kIx,	,
přístavba	přístavba	k1gFnSc1	přístavba
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1940	[number]	k4	1940
až	až	k9	až
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akciová	akciový	k2eAgFnSc1d1	akciová
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
umělých	umělý	k2eAgNnPc2d1	umělé
hnojiv	hnojivo	k1gNnPc2	hnojivo
a	a	k8xC	a
lučebnin	lučebnina	k1gFnPc2	lučebnina
na	na	k7c6	na
Pražské	pražský	k2eAgFnSc6d1	Pražská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
lučebka	lučebka	k1gFnSc1	lučebka
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Průběžné	průběžný	k2eAgFnPc1d1	průběžná
přístavby	přístavba	k1gFnPc1	přístavba
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
funkční	funkční	k2eAgNnSc1d1	funkční
<g/>
,	,	kIx,	,
produkuje	produkovat	k5eAaImIp3nS	produkovat
výrobky	výrobek	k1gInPc4	výrobek
stavební	stavební	k2eAgFnSc2d1	stavební
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
rafinování	rafinování	k1gNnSc4	rafinování
petroleje	petrolej	k1gInSc2	petrolej
v	v	k7c6	v
Ovčárecké	Ovčárecký	k2eAgFnSc6d1	Ovčárecká
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
Zálabí	zálabí	k1gNnSc6	zálabí
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Petrolka	petrolka	k1gFnSc1	petrolka
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
součást	součást	k1gFnSc1	součást
Vacuum	Vacuum	k1gNnSc4	Vacuum
Oil	Oil	k1gMnSc2	Oil
Compan	Compan	k1gInSc4	Compan
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
později	pozdě	k6eAd2	pozdě
KORAMO	KORAMO	kA	KORAMO
<g/>
.	.	kIx.	.
</s>
<s>
Postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1902	[number]	k4	1902
až	až	k9	až
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
modernizována	modernizován	k2eAgFnSc1d1	modernizována
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
až	až	k9	až
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
po	po	k7c6	po
náletech	nálet	k1gInPc6	nálet
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
obnovena	obnoven	k2eAgFnSc1d1	obnovena
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
funkční	funkční	k2eAgFnSc1d1	funkční
a	a	k8xC	a
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
koncernu	koncern	k1gInSc2	koncern
Unipetrol	Unipetrola	k1gFnPc2	Unipetrola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
draselných	draselný	k2eAgInPc2d1	draselný
louhů	louh	k1gInPc2	louh
v	v	k7c6	v
Havlíčkově	Havlíčkův	k2eAgFnSc6d1	Havlíčkova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
draslovka	draslovka	k1gFnSc1	draslovka
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1907	[number]	k4	1907
až	až	k9	až
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
přestavba	přestavba	k1gFnSc1	přestavba
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
obnovena	obnoven	k2eAgFnSc1d1	obnovena
po	po	k7c6	po
náletech	nálet	k1gInPc6	nálet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
funkční	funkční	k2eAgFnSc1d1	funkční
a	a	k8xC	a
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
a	a	k8xC	a
zpracuje	zpracovat	k5eAaPmIp3nS	zpracovat
především	především	k9	především
kyanovodík	kyanovodík	k1gInSc1	kyanovodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
stojí	stát	k5eAaImIp3nS	stát
též	též	k9	též
dva	dva	k4xCgInPc4	dva
hodnotné	hodnotný	k2eAgInPc4d1	hodnotný
cihlové	cihlový	k2eAgInPc4d1	cihlový
tovární	tovární	k2eAgInPc4d1	tovární
komíny	komín	k1gInPc4	komín
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgInPc4d1	vysoký
64	[number]	k4	64
a	a	k8xC	a
35	[number]	k4	35
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
areálem	areál	k1gInSc7	areál
draslovky	draslovka	k1gFnSc2	draslovka
a	a	k8xC	a
železnicí	železnice	k1gFnSc7	železnice
se	se	k3xPyFc4	se
též	též	k9	též
dobře	dobře	k6eAd1	dobře
dochovala	dochovat	k5eAaPmAgFnS	dochovat
původní	původní	k2eAgFnSc1d1	původní
budova	budova	k1gFnSc1	budova
tzv.	tzv.	kA	tzv.
nového	nový	k2eAgInSc2d1	nový
lihovaru	lihovar	k1gInSc2	lihovar
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wiesnerova	Wiesnerův	k2eAgFnSc1d1	Wiesnerova
strojírna	strojírna	k1gFnSc1	strojírna
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1870	[number]	k4	1870
až	až	k9	až
1875	[number]	k4	1875
v	v	k7c6	v
Rybářské	rybářský	k2eAgFnSc6d1	rybářská
ulici	ulice	k1gFnSc6	ulice
čp.	čp.	k?	čp.
56	[number]	k4	56
vedle	vedle	k7c2	vedle
kolínské	kolínský	k2eAgFnSc2d1	Kolínská
zastávky	zastávka	k1gFnSc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
především	především	k9	především
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
cukrovary	cukrovar	k1gInPc4	cukrovar
<g/>
.	.	kIx.	.
</s>
<s>
Zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc2	začátek
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
sem	sem	k6eAd1	sem
byla	být	k5eAaImAgFnS	být
přemístěna	přemístěn	k2eAgFnSc1d1	přemístěna
výroba	výroba	k1gFnSc1	výroba
pecí	pec	k1gFnPc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Pece	Pec	k1gFnPc1	Pec
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Elektroteplo	Elektroteplo	k1gFnSc7	Elektroteplo
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
je	být	k5eAaImIp3nS	být
především	především	k9	především
vstupní	vstupní	k2eAgFnSc1d1	vstupní
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
ozdobnými	ozdobný	k2eAgInPc7d1	ozdobný
prvky	prvek	k1gInPc7	prvek
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
<g/>
,	,	kIx,	,
situovaná	situovaný	k2eAgFnSc1d1	situovaná
do	do	k7c2	do
malebného	malebný	k2eAgNnSc2d1	malebné
zákoutí	zákoutí	k1gNnSc2	zákoutí
vedle	vedle	k7c2	vedle
ramene	rameno	k1gNnSc2	rameno
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Masarykův	Masarykův	k2eAgInSc1d1	Masarykův
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
a	a	k8xC	a
hydroelektrárna	hydroelektrárna	k1gFnSc1	hydroelektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1924	[number]	k4	1924
až	až	k9	až
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
elektrárna	elektrárna	k1gFnSc1	elektrárna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
komplex	komplex	k1gInSc1	komplex
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
funkční	funkční	k2eAgNnSc1d1	funkční
<g/>
,	,	kIx,	,
hydroelektrárna	hydroelektrárna	k1gFnSc1	hydroelektrárna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2011	[number]	k4	2011
až	až	k6eAd1	až
2012	[number]	k4	2012
zrekonstruována	zrekonstruován	k2eAgFnSc1d1	zrekonstruována
a	a	k8xC	a
osazena	osazen	k2eAgFnSc1d1	osazena
novou	nový	k2eAgFnSc7d1	nová
technologií	technologie	k1gFnSc7	technologie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
turbín	turbína	k1gFnPc2	turbína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lávka	lávka	k1gFnSc1	lávka
na	na	k7c4	na
Kmochův	kmochův	k2eAgInSc4d1	kmochův
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Kamenné	kamenný	k2eAgInPc4d1	kamenný
pilíře	pilíř	k1gInPc4	pilíř
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
kolínský	kolínský	k2eAgMnSc1d1	kolínský
stavitel	stavitel	k1gMnSc1	stavitel
Josef	Josef	k1gMnSc1	Josef
Sklenář	Sklenář	k1gMnSc1	Sklenář
<g/>
,	,	kIx,	,
železnou	železný	k2eAgFnSc4d1	železná
konstrukci	konstrukce	k1gFnSc4	konstrukce
dodala	dodat	k5eAaPmAgFnS	dodat
Vojtěšská	vojtěšský	k2eAgFnSc1d1	Vojtěšská
huť	huť	k1gFnSc1	huť
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgFnPc1d1	funkční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
ESSO	ESSO	kA	ESSO
v	v	k7c6	v
Tovární	tovární	k2eAgFnSc6d1	tovární
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
Zálabí	zálabí	k1gNnSc6	zálabí
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
architektem	architekt	k1gMnSc7	architekt
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Fragnerem	Fragner	k1gMnSc7	Fragner
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
sekce	sekce	k1gFnSc2	sekce
Ekonomika	ekonomika	k1gFnSc1	ekonomika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
cenná	cenný	k2eAgFnSc1d1	cenná
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
teplárenský	teplárenský	k2eAgInSc4d1	teplárenský
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
funkční	funkční	k2eAgNnSc1d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Továrna	továrna	k1gFnSc1	továrna
nahradila	nahradit	k5eAaPmAgFnS	nahradit
původní	původní	k2eAgFnSc4d1	původní
Křižíkovu	Křižíkův	k2eAgFnSc4d1	Křižíkova
parní	parní	k2eAgFnSc4d1	parní
elektrárnu	elektrárna	k1gFnSc4	elektrárna
u	u	k7c2	u
nádraží	nádraží	k1gNnSc2	nádraží
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
zachovala	zachovat	k5eAaPmAgFnS	zachovat
pouze	pouze	k6eAd1	pouze
nevelká	velký	k2eNgFnSc1d1	nevelká
budova	budova	k1gFnSc1	budova
čp.	čp.	k?	čp.
271	[number]	k4	271
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Pod	pod	k7c7	pod
Hroby	hrob	k1gInPc7	hrob
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
zachovalých	zachovalý	k2eAgInPc2d1	zachovalý
areálů	areál	k1gInPc2	areál
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Chemická	chemický	k2eAgFnSc1d1	chemická
továrna	továrna	k1gFnSc1	továrna
Stanislava	Stanislav	k1gMnSc2	Stanislav
Orla	Orel	k1gMnSc2	Orel
v	v	k7c6	v
Brankovické	Brankovický	k2eAgFnSc6d1	Brankovická
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
Zálabí	zálabí	k1gNnSc6	zálabí
<g/>
,	,	kIx,	,
Továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
vozy	vůz	k1gInPc4	vůz
Fram	Frama	k1gFnPc2	Frama
v	v	k7c6	v
Ovčárecké	Ovčárecký	k2eAgFnSc6d1	Ovčárecká
ulici	ulice	k1gFnSc6	ulice
na	na	k7c4	na
Zálabí	zálabí	k1gNnSc4	zálabí
<g/>
,	,	kIx,	,
areál	areál	k1gInSc1	areál
Prchal-Ericcson	Prchal-Ericcson	k1gInSc1	Prchal-Ericcson
/	/	kIx~	/
Tesla	Tesla	k1gFnSc1	Tesla
či	či	k8xC	či
železářské	železářský	k2eAgInPc1d1	železářský
závody	závod	k1gInPc1	závod
Josefa	Josef	k1gMnSc2	Josef
a	a	k8xC	a
Prokopa	Prokop	k1gMnSc2	Prokop
Červinkových	Červinková	k1gFnPc2	Červinková
v	v	k7c6	v
Havlíčkově	Havlíčkův	k2eAgFnSc6d1	Havlíčkova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
Veletovského	Veletovský	k2eAgNnSc2d1	Veletovský
cihelna	cihelna	k1gFnSc1	cihelna
v	v	k7c6	v
Plynárenské	plynárenský	k2eAgFnSc6d1	plynárenská
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
strojírna	strojírna	k1gFnSc1	strojírna
bratří	bratr	k1gMnPc2	bratr
Kašparidesů	Kašparides	k1gInPc2	Kašparides
v	v	k7c6	v
Polepské	Polepský	k2eAgFnSc6d1	Polepská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Potenciálně	potenciálně	k6eAd1	potenciálně
velmi	velmi	k6eAd1	velmi
atraktivní	atraktivní	k2eAgNnSc4d1	atraktivní
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
dědictví	dědictví	k1gNnSc4	dědictví
však	však	k9	však
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
systematicky	systematicky	k6eAd1	systematicky
rozvíjeno	rozvíjen	k2eAgNnSc1d1	rozvíjeno
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
jeho	jeho	k3xOp3gNnSc3	jeho
hojnému	hojný	k2eAgNnSc3d1	hojné
zastoupení	zastoupení	k1gNnSc3	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
již	již	k6eAd1	již
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
demolice	demolice	k1gFnPc1	demolice
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
části	část	k1gFnSc2	část
objektů	objekt	k1gInPc2	objekt
zámeckého	zámecký	k2eAgInSc2d1	zámecký
pivovaru	pivovar	k1gInSc2	pivovar
(	(	kIx(	(
<g/>
spilka	spilka	k1gFnSc1	spilka
<g/>
,	,	kIx,	,
ječné	ječný	k2eAgFnPc1d1	Ječná
půdy	půda	k1gFnPc1	půda
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
zbořen	zbořit	k5eAaPmNgInS	zbořit
i	i	k8xC	i
cenný	cenný	k2eAgInSc1d1	cenný
areál	areál	k1gInSc1	areál
tiskárny	tiskárna	k1gFnSc2	tiskárna
J.	J.	kA	J.
L.	L.	kA	L.
Bayera	Bayer	k1gMnSc2	Bayer
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
součástí	součást	k1gFnSc7	součást
byly	být	k5eAaImAgFnP	být
budovy	budova	k1gFnPc4	budova
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
století	století	k1gNnSc2	století
následujícího	následující	k2eAgNnSc2d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
Futurum	futurum	k1gNnSc4	futurum
<g/>
,	,	kIx,	,
projektovaný	projektovaný	k2eAgInSc4d1	projektovaný
však	však	k9	však
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
návaznosti	návaznost	k1gFnSc2	návaznost
na	na	k7c4	na
historii	historie	k1gFnSc4	historie
či	či	k8xC	či
architekturu	architektura	k1gFnSc4	architektura
proslulého	proslulý	k2eAgInSc2d1	proslulý
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
dvou	dva	k4xCgInPc2	dva
židovských	židovský	k2eAgInPc2d1	židovský
hřbitovů	hřbitov	k1gInPc2	hřbitov
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Zálabí	zálabí	k1gNnSc2	zálabí
nachází	nacházet	k5eAaImIp3nS	nacházet
Městský	městský	k2eAgInSc1d1	městský
hřbitov	hřbitov	k1gInSc1	hřbitov
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
a	a	k8xC	a
evangelický	evangelický	k2eAgInSc4d1	evangelický
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
veřejným	veřejný	k2eAgNnSc7d1	veřejné
pohřebištěm	pohřebiště	k1gNnSc7	pohřebiště
je	být	k5eAaImIp3nS	být
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Sendražicích	Sendražik	k1gInPc6	Sendražik
a	a	k8xC	a
kolumbárium	kolumbárium	k1gNnSc1	kolumbárium
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
církve	církev	k1gFnSc2	církev
Československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
1	[number]	k4	1
508	[number]	k4	508
domech	dům	k1gInPc6	dům
16	[number]	k4	16
204	[number]	k4	204
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
8	[number]	k4	8
601	[number]	k4	601
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
16	[number]	k4	16
015	[number]	k4	015
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
39	[number]	k4	39
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
32	[number]	k4	32
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
8	[number]	k4	8
268	[number]	k4	268
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
893	[number]	k4	893
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
4	[number]	k4	4
311	[number]	k4	311
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
482	[number]	k4	482
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
2	[number]	k4	2
324	[number]	k4	324
domech	dům	k1gInPc6	dům
18	[number]	k4	18
488	[number]	k4	488
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
18	[number]	k4	18
200	[number]	k4	200
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
108	[number]	k4	108
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
9	[number]	k4	9
173	[number]	k4	173
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
1	[number]	k4	1
369	[number]	k4	369
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
5	[number]	k4	5
234	[number]	k4	234
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
430	[number]	k4	430
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
místního	místní	k2eAgMnSc2d1	místní
kapelníka	kapelník	k1gMnSc2	kapelník
a	a	k8xC	a
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Františka	František	k1gMnSc2	František
Kmocha	Kmocha	k?	Kmocha
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
června	červen	k1gInSc2	červen
koná	konat	k5eAaImIp3nS	konat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
dechové	dechový	k2eAgFnSc2d1	dechová
hudby	hudba	k1gFnSc2	hudba
Kmochův	kmochův	k2eAgInSc1d1	kmochův
Kolín	Kolín	k1gInSc1	Kolín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvorbu	tvorba	k1gFnSc4	tvorba
slavného	slavný	k2eAgMnSc2d1	slavný
českého	český	k2eAgMnSc2d1	český
fotografa	fotograf	k1gMnSc2	fotograf
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Funkeho	Funke	k1gMnSc2	Funke
připomíná	připomínat	k5eAaImIp3nS	připomínat
fotografických	fotografický	k2eAgFnPc2d1	fotografická
festival	festival	k1gInSc4	festival
výstav	výstava	k1gFnPc2	výstava
a	a	k8xC	a
přednášek	přednáška	k1gFnPc2	přednáška
Funkeho	Funke	k1gMnSc2	Funke
Kolín	Kolín	k1gInSc4	Kolín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Školství	školství	k1gNnSc2	školství
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
základní	základní	k2eAgNnSc4d1	základní
vzdělání	vzdělání	k1gNnSc4	vzdělání
sedm	sedm	k4xCc4	sedm
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
označených	označený	k2eAgFnPc2d1	označená
číslicí	číslice	k1gFnSc7	číslice
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
k	k	k7c3	k
pořadovému	pořadový	k2eAgNnSc3d1	pořadové
číslu	číslo	k1gNnSc3	číslo
školy	škola	k1gFnSc2	škola
přidává	přidávat	k5eAaImIp3nS	přidávat
i	i	k9	i
název	název	k1gInSc1	název
ulice	ulice	k1gFnSc2	ulice
–	–	k?	–
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Prokopa	Prokop	k1gMnSc2	Prokop
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
školy	škola	k1gFnPc1	škola
připojených	připojený	k2eAgFnPc2d1	připojená
obcí	obec	k1gFnPc2	obec
–	–	k?	–
například	například	k6eAd1	například
Sendražice	Sendražice	k1gFnSc2	Sendražice
–	–	k?	–
se	se	k3xPyFc4	se
do	do	k7c2	do
číslování	číslování	k1gNnSc2	číslování
kolínských	kolínský	k2eAgFnPc2d1	Kolínská
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
nepočítají	počítat	k5eNaImIp3nP	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Výuku	výuka	k1gFnSc4	výuka
uměleckých	umělecký	k2eAgInPc2d1	umělecký
oborů	obor	k1gInPc2	obor
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
Františka	František	k1gMnSc2	František
Kmocha	Kmocha	k?	Kmocha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
také	také	k9	také
působí	působit	k5eAaImIp3nS	působit
škola	škola	k1gFnSc1	škola
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střední	střední	k1gMnPc1	střední
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
stavební	stavební	k2eAgNnSc1d1	stavební
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
informatiky	informatika	k1gFnSc2	informatika
a	a	k8xC	a
spojů	spoj	k1gInPc2	spoj
a	a	k8xC	a
Střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
strojírenská	strojírenský	k2eAgFnSc1d1	strojírenská
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
obchodní	obchodní	k2eAgFnSc1d1	obchodní
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Dívčí	dívčí	k2eAgFnSc1d1	dívčí
katolická	katolický	k2eAgFnSc1d1	katolická
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Soukromými	soukromý	k2eAgFnPc7d1	soukromá
školami	škola	k1gFnPc7	škola
jsou	být	k5eAaImIp3nP	být
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
podnikatelská	podnikatelský	k2eAgFnSc1d1	podnikatelská
Kolín	Kolín	k1gInSc4	Kolín
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
managementu	management	k1gInSc2	management
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vyššího	vysoký	k2eAgNnSc2d2	vyšší
školství	školství	k1gNnSc2	školství
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
působí	působit	k5eAaImIp3nS	působit
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
misijní	misijní	k2eAgFnSc1d1	misijní
a	a	k8xC	a
teologická	teologický	k2eAgFnSc1d1	teologická
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
Rerum	Rerum	k1gInSc1	Rerum
Civilium	Civilium	k1gNnSc1	Civilium
-	-	kIx~	-
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
politických	politický	k2eAgFnPc2d1	politická
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Církve	církev	k1gFnSc2	církev
==	==	k?	==
</s>
</p>
<p>
<s>
Apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
církev	církev	k1gFnSc1	církev
</s>
</p>
<p>
<s>
Církev	církev	k1gFnSc1	církev
bratrská	bratrský	k2eAgFnSc1d1	bratrská
</s>
</p>
<p>
<s>
Českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
</s>
</p>
<p>
<s>
Církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
</s>
</p>
<p>
<s>
Církev	církev	k1gFnSc1	církev
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
</s>
</p>
<p>
<s>
Církev	církev	k1gFnSc1	církev
adventistů	adventista	k1gMnPc2	adventista
sedmého	sedmý	k4xOgNnSc2	sedmý
dne	den	k1gInSc2	den
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
života	život	k1gInSc2	život
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
otevřena	otevřen	k2eAgFnSc1d1	otevřena
automobilka	automobilka	k1gFnSc1	automobilka
konsorcia	konsorcium	k1gNnSc2	konsorcium
TPCA	TPCA	kA	TPCA
(	(	kIx(	(
<g/>
Toyota-Peugeot-Citroën	Toyota-Peugeot-Citroën	k1gMnSc1	Toyota-Peugeot-Citroën
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
3	[number]	k4	3
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
s	s	k7c7	s
výrobní	výrobní	k2eAgFnSc7d1	výrobní
kapacitou	kapacita	k1gFnSc7	kapacita
300	[number]	k4	300
000	[number]	k4	000
vozů	vůz	k1gInPc2	vůz
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
modelem	model	k1gInSc7	model
je	být	k5eAaImIp3nS	být
trojice	trojice	k1gFnSc1	trojice
Toyota	toyota	k1gFnSc1	toyota
Aygo	Aygo	k6eAd1	Aygo
<g/>
,	,	kIx,	,
Peugeot	peugeot	k1gInSc1	peugeot
107	[number]	k4	107
a	a	k8xC	a
Citroën	Citroën	k1gInSc1	Citroën
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
a	a	k8xC	a
z	z	k7c2	z
92	[number]	k4	92
%	%	kIx~	%
identická	identický	k2eAgFnSc1d1	identická
<g/>
,	,	kIx,	,
uvedená	uvedený	k2eAgFnSc1d1	uvedená
na	na	k7c4	na
trh	trh	k1gInSc4	trh
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kolínská	kolínský	k2eAgFnSc1d1	Kolínská
Draslovka	draslovka	k1gFnSc1	draslovka
<g/>
,	,	kIx,	,
Lučební	lučební	k2eAgInPc1d1	lučební
závody	závod	k1gInPc1	závod
a	a	k8xC	a
rafinerie	rafinerie	k1gFnPc1	rafinerie
PARAMO	PARAMO	kA	PARAMO
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
KORAMO	KORAMO	kA	KORAMO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
Zálabí	zálabí	k1gNnSc6	zálabí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
elektrárna	elektrárna	k1gFnSc1	elektrárna
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
ESSO	ESSO	kA	ESSO
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
architekta	architekt	k1gMnSc2	architekt
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Fragnera	Fragner	k1gMnSc2	Fragner
realizoval	realizovat	k5eAaBmAgMnS	realizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
Elektrárenský	elektrárenský	k2eAgInSc1d1	elektrárenský
svaz	svaz	k1gInSc1	svaz
středolabských	středolabský	k2eAgInPc2d1	středolabský
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Masarykova	Masarykův	k2eAgInSc2d1	Masarykův
mostu	most	k1gInSc2	most
na	na	k7c6	na
Horním	horní	k2eAgInSc6d1	horní
ostrově	ostrov	k1gInSc6	ostrov
hydroelektrárna	hydroelektrárna	k1gFnSc1	hydroelektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
elektrárny	elektrárna	k1gFnPc4	elektrárna
provozuje	provozovat	k5eAaImIp3nS	provozovat
Dalkia	Dalkia	k1gFnSc1	Dalkia
a.s.	a.s.	k?	a.s.
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
závod	závod	k1gInSc4	závod
francouzské	francouzský	k2eAgFnSc2d1	francouzská
firmy	firma	k1gFnSc2	firma
Dalkia	Dalkium	k1gNnSc2	Dalkium
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
české	český	k2eAgFnSc2d1	Česká
pobočky	pobočka	k1gFnSc2	pobočka
Dalkia	Dalkium	k1gNnSc2	Dalkium
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zabývá	zabývat	k5eAaImIp3nS	zabývat
také	také	k9	také
výrobou	výroba	k1gFnSc7	výroba
a	a	k8xC	a
rozvodem	rozvod	k1gInSc7	rozvod
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Kolínem	Kolín	k1gInSc7	Kolín
procházejí	procházet	k5eAaImIp3nP	procházet
následující	následující	k2eAgFnPc4d1	následující
komunikace	komunikace	k1gFnPc4	komunikace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
Chrudim	Chrudim	k1gFnSc1	Chrudim
-	-	kIx~	-
Čáslav	Čáslav	k1gFnSc1	Čáslav
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Nymburk	Nymburk	k1gInSc1	Nymburk
-	-	kIx~	-
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
</s>
</p>
<p>
<s>
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
125	[number]	k4	125
Libice	Libice	k1gFnPc1	Libice
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Uhlířské	uhlířský	k2eAgFnPc1d1	uhlířská
Janovice	Janovice	k1gFnPc1	Janovice
-	-	kIx~	-
Vlašim	Vlašim	k1gFnSc1	Vlašim
</s>
</p>
<p>
<s>
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
322	[number]	k4	322
Přelouč	Přelouč	k1gFnSc1	Přelouč
-	-	kIx~	-
Týnec	Týnec	k1gInSc1	Týnec
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
328	[number]	k4	328
Jičíněves	Jičíněves	k1gInSc1	Jičíněves
-	-	kIx~	-
Městec	Městec	k1gInSc1	Městec
Králové	Králová	k1gFnSc2	Králová
-	-	kIx~	-
KolínV	KolínV	k1gFnSc1	KolínV
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
zprovozněn	zprovozněn	k2eAgInSc1d1	zprovozněn
obchvat	obchvat	k1gInSc1	obchvat
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojení	spojení	k1gNnSc1	spojení
obou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dva	dva	k4xCgInPc1	dva
silniční	silniční	k2eAgInPc1d1	silniční
mosty	most	k1gInPc1	most
(	(	kIx(	(
<g/>
Masarykův	Masarykův	k2eAgInSc1d1	Masarykův
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
slouží	sloužit	k5eAaImIp3nP	sloužit
též	též	k9	též
lávky	lávka	k1gFnPc1	lávka
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
přes	přes	k7c4	přes
Kmochův	kmochův	k2eAgInSc4d1	kmochův
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
provozovala	provozovat	k5eAaImAgFnS	provozovat
vedly	vést	k5eAaImAgFnP	vést
do	do	k7c2	do
těchto	tento	k3xDgNnPc2	tento
míst	místo	k1gNnPc2	místo
<g/>
:	:	kIx,	:
Bystřice	Bystřice	k1gFnSc1	Bystřice
nad	nad	k7c7	nad
Pernštejnem	Pernštejn	k1gInSc7	Pernštejn
<g/>
,	,	kIx,	,
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Chlumec	Chlumec	k1gInSc1	Chlumec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
<g/>
,	,	kIx,	,
Kouřim	Kouřim	k1gFnSc1	Kouřim
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Městec	Městec	k1gInSc1	Městec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
<g/>
,	,	kIx,	,
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
Týnec	Týnec	k1gInSc1	Týnec
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Uhlířské	uhlířský	k2eAgFnPc1d1	uhlířská
Janovice	Janovice	k1gFnPc1	Janovice
<g/>
,	,	kIx,	,
Vlašim	Vlašim	k1gFnSc1	Vlašim
<g/>
,	,	kIx,	,
Zásmuky	Zásmuky	k1gInPc1	Zásmuky
<g/>
,	,	kIx,	,
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
Žiželice	Žiželice	k1gFnSc1	Žiželice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Kolín	Kolín	k1gInSc1	Kolín
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
nejen	nejen	k6eAd1	nejen
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
jím	jíst	k5eAaImIp1nS	jíst
první	první	k4xOgInSc4	první
i	i	k8xC	i
třetí	třetí	k4xOgInSc4	třetí
železniční	železniční	k2eAgInSc4d1	železniční
koridor	koridor	k1gInSc4	koridor
<g/>
.	.	kIx.	.
</s>
<s>
Kolín	Kolín	k1gInSc1	Kolín
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
tratích	trať	k1gFnPc6	trať
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
,	,	kIx,	,
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
dráha	dráha	k1gFnSc1	dráha
zařazená	zařazený	k2eAgFnSc1d1	zařazená
do	do	k7c2	do
evropského	evropský	k2eAgInSc2d1	evropský
železničního	železniční	k2eAgInSc2d1	železniční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
koridoru	koridor	k1gInSc2	koridor
<g/>
,	,	kIx,	,
trať	trať	k1gFnSc1	trať
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
hustým	hustý	k2eAgInSc7d1	hustý
příměstským	příměstský	k2eAgInSc7d1	příměstský
<g/>
,	,	kIx,	,
vnitrostátním	vnitrostátní	k2eAgInSc7d1	vnitrostátní
i	i	k8xC	i
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
provozem	provoz	k1gInSc7	provoz
<g/>
,	,	kIx,	,
zprovozněná	zprovozněný	k2eAgFnSc1d1	zprovozněná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Uhlířské	uhlířský	k2eAgFnPc1d1	uhlířská
Janovice	Janovice	k1gFnPc1	Janovice
-	-	kIx~	-
Ledečko	Ledečko	k1gNnSc1	Ledečko
<g/>
,	,	kIx,	,
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
neelektrifikovaná	elektrifikovaný	k2eNgFnSc1d1	neelektrifikovaná
regionální	regionální	k2eAgFnSc1d1	regionální
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
-	-	kIx~	-
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
dráha	dráha	k1gFnSc1	dráha
zařazená	zařazený	k2eAgFnSc1d1	zařazená
do	do	k7c2	do
evropského	evropský	k2eAgInSc2d1	evropský
železničního	železniční	k2eAgInSc2d1	železniční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
trať	trať	k1gFnSc1	trať
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
vnitrostátním	vnitrostátní	k2eAgInSc7d1	vnitrostátní
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
zprovozněná	zprovozněný	k2eAgFnSc1d1	zprovozněná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Čelákovice	Čelákovice	k1gFnPc1	Čelákovice
-	-	kIx~	-
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
-	-	kIx~	-
Nymburk	Nymburk	k1gInSc1	Nymburk
-	-	kIx~	-
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
dráha	dráha	k1gFnSc1	dráha
zařazená	zařazený	k2eAgFnSc1d1	zařazená
do	do	k7c2	do
evropského	evropský	k2eAgInSc2d1	evropský
železničního	železniční	k2eAgInSc2d1	železniční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
trať	trať	k1gFnSc1	trať
s	s	k7c7	s
hustým	hustý	k2eAgInSc7d1	hustý
příměstským	příměstský	k2eAgInSc7d1	příměstský
a	a	k8xC	a
vnitrostátním	vnitrostátní	k2eAgInSc7d1	vnitrostátní
provozem	provoz	k1gInSc7	provoz
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1870V	[number]	k4	1870V
Kolíně	Kolín	k1gInSc6	Kolín
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedno	jeden	k4xCgNnSc1	jeden
nádraží	nádraží	k1gNnSc1	nádraží
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc1	čtyři
zastávky	zastávka	k1gFnPc1	zastávka
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
odbočná	odbočný	k2eAgFnSc1d1	odbočná
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
staniční	staniční	k2eAgFnSc1d1	staniční
budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
obnovena	obnovit	k5eAaPmNgFnS	obnovit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
Živá	živá	k1gFnSc1	živá
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trati	trať	k1gFnSc6	trať
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
leží	ležet	k5eAaImIp3nS	ležet
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Kolín	Kolín	k1gInSc1	Kolín
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
,	,	kIx,	,
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Kolín	Kolín	k1gInSc1	Kolín
dílny	dílna	k1gFnSc2	dílna
<g/>
,	,	kIx,	,
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
do	do	k7c2	do
Nymburka	Nymburk	k1gInSc2	Nymburk
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Kolín-Zálabí	Kolín-Zálabí	k1gNnSc2	Kolín-Zálabí
a	a	k8xC	a
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
do	do	k7c2	do
Ledečka	Ledeček	k1gMnSc2	Ledeček
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Kolín	Kolín	k1gInSc4	Kolín
místní	místní	k2eAgNnSc4d1	místní
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
nebo	nebo	k8xC	nebo
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
mnohé	mnohý	k2eAgFnPc4d1	mnohá
dálkové	dálkový	k2eAgFnPc4d1	dálková
železniční	železniční	k2eAgFnPc4d1	železniční
linky	linka	k1gFnPc4	linka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
Ex	ex	k6eAd1	ex
<g/>
1	[number]	k4	1
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Olomouc	Olomouc	k1gFnSc1	Olomouc
-	-	kIx~	-
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
-	-	kIx~	-
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
/	/	kIx~	/
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
Ex	ex	k6eAd1	ex
<g/>
2	[number]	k4	2
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Olomouc	Olomouc	k1gFnSc1	Olomouc
-	-	kIx~	-
Vsetín	Vsetín	k1gInSc1	Vsetín
(	(	kIx(	(
<g/>
-Slovensko	-Slovensko	k1gNnSc1	-Slovensko
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
společný	společný	k2eAgInSc4d1	společný
interval	interval	k1gInSc4	interval
linek	linka	k1gFnPc2	linka
Ex	ex	k6eAd1	ex
<g/>
1	[number]	k4	1
a	a	k8xC	a
Ex	ex	k6eAd1	ex
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
Ex	ex	k6eAd1	ex
<g/>
3	[number]	k4	3
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Pardubice	Pardubice	k1gInPc1	Pardubice
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Břeclav	Břeclav	k1gFnSc1	Břeclav
(	(	kIx(	(
<g/>
-	-	kIx~	-
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
interval	interval	k1gInSc4	interval
120	[number]	k4	120
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
R19	R19	k1gFnSc1	R19
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Pardubice	Pardubice	k1gInPc1	Pardubice
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
-	-	kIx~	-
Letovice	Letovice	k1gFnSc1	Letovice
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
interval	interval	k1gInSc1	interval
120	[number]	k4	120
minut	minuta	k1gFnPc2	minuta
-	-	kIx~	-
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Č.	Č.	kA	Č.
<g/>
Třebová	Třebová	k1gFnSc1	Třebová
proklad	proklad	k1gInSc1	proklad
s	s	k7c7	s
linkou	linka	k1gFnSc7	linka
R	R	kA	R
18	[number]	k4	18
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
R18	R18	k1gFnSc1	R18
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Olomouc	Olomouc	k1gFnSc1	Olomouc
-	-	kIx~	-
Luhačovice	Luhačovice	k1gFnPc1	Luhačovice
/	/	kIx~	/
Zlín	Zlín	k1gInSc1	Zlín
/	/	kIx~	/
Vsetín	Vsetín	k1gInSc1	Vsetín
(	(	kIx(	(
<g/>
interval	interval	k1gInSc1	interval
120	[number]	k4	120
minut	minuta	k1gFnPc2	minuta
-	-	kIx~	-
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Č.	Č.	kA	Č.
<g/>
Třebová	Třebová	k1gFnSc1	Třebová
proklad	proklad	k1gInSc1	proklad
s	s	k7c7	s
linkou	linka	k1gFnSc7	linka
R	R	kA	R
19	[number]	k4	19
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
R9	R9	k1gFnSc1	R9
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
<g/>
/	/	kIx~	/
<g/>
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
interval	interval	k1gInSc1	interval
60-120	[number]	k4	60-120
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
R23	R23	k1gMnSc1	R23
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Nymburk	Nymburk	k1gInSc1	Nymburk
-	-	kIx~	-
Mělník	Mělník	k1gInSc1	Mělník
-	-	kIx~	-
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
interval	interval	k1gInSc1	interval
120	[number]	k4	120
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
R22	R22	k1gMnSc1	R22
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Nymburk	Nymburk	k1gInSc1	Nymburk
-	-	kIx~	-
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
Lípa	lípa	k1gFnSc1	lípa
-	-	kIx~	-
Rumburk	Rumburk	k1gInSc1	Rumburk
(	(	kIx(	(
<g/>
interval	interval	k1gInSc1	interval
120	[number]	k4	120
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
<g/>
Kolín	Kolín	k1gInSc1	Kolín
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
systému	systém	k1gInSc6	systém
pražské	pražský	k2eAgFnSc2d1	Pražská
příměstské	příměstský	k2eAgFnSc2d1	příměstská
dopravy	doprava	k1gFnSc2	doprava
Esko	eska	k1gFnSc5	eska
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
sem	sem	k6eAd1	sem
následující	následující	k2eAgFnPc1d1	následující
linky	linka	k1gFnPc1	linka
(	(	kIx(	(
<g/>
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
<g/>
uvedena	uveden	k2eAgFnSc1d1	uvedena
je	být	k5eAaImIp3nS	být
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
končí	končit	k5eAaImIp3nS	končit
systém	systém	k1gInSc1	systém
Esko	eska	k1gFnSc5	eska
<g/>
,	,	kIx,	,
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
je	být	k5eAaImIp3nS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
cílová	cílový	k2eAgFnSc1d1	cílová
stanice	stanice	k1gFnSc1	stanice
vlaků	vlak	k1gInPc2	vlak
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
S1	S1	k1gFnSc1	S1
Praha	Praha	k1gFnSc1	Praha
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
nádraží	nádraží	k1gNnSc1	nádraží
-	-	kIx~	-
Český	český	k2eAgInSc1d1	český
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
(	(	kIx(	(
<g/>
interval	interval	k1gInSc1	interval
30-60	[number]	k4	30-60
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
S2	S2	k1gFnSc1	S2
Praha	Praha	k1gFnSc1	Praha
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
nádraží	nádraží	k1gNnSc4	nádraží
-	-	kIx~	-
Nymburk	Nymburk	k1gInSc1	Nymburk
-	-	kIx~	-
Kolín	Kolín	k1gInSc1	Kolín
(	(	kIx(	(
<g/>
interval	interval	k1gInSc1	interval
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
S10	S10	k1gMnSc1	S10
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Týnec	Týnec	k1gInSc1	Týnec
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
-	-	kIx~	-
Pardubice	Pardubice	k1gInPc1	Pardubice
-	-	kIx~	-
Č.	Č.	kA	Č.
<g/>
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
interval	interval	k1gInSc4	interval
60-120	[number]	k4	60-120
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
S15	S15	k1gMnSc1	S15
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Chlumec	Chlumec	k1gInSc1	Chlumec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
(	(	kIx(	(
<g/>
-	-	kIx~	-
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
interval	interval	k1gInSc4	interval
120	[number]	k4	120
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
S18	S18	k1gMnSc1	S18
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Ledečko	Ledečko	k1gNnSc1	Ledečko
(	(	kIx(	(
<g/>
interval	interval	k1gInSc1	interval
nejednoznačný	jednoznačný	k2eNgInSc1d1	nejednoznačný
-	-	kIx~	-
přibližně	přibližně	k6eAd1	přibližně
60-120	[number]	k4	60-120
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
linka	linka	k1gFnSc1	linka
S20	S20	k1gMnSc1	S20
Kolín	Kolín	k1gInSc1	Kolín
-	-	kIx~	-
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
(	(	kIx(	(
<g/>
-Havlíčkův	-Havlíčkův	k2eAgInSc1d1	-Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
interval	interval	k1gInSc4	interval
nejednoznačný	jednoznačný	k2eNgInSc4d1	nejednoznačný
-	-	kIx~	-
přibližně	přibližně	k6eAd1	přibližně
90	[number]	k4	90
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
existuje	existovat	k5eAaImIp3nS	existovat
už	už	k6eAd1	už
od	od	k7c2	od
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
11	[number]	k4	11
linkami	linka	k1gFnPc7	linka
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ji	on	k3xPp3gFnSc4	on
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
společnost	společnost	k1gFnSc1	společnost
ARRIVA	ARRIVA	kA	ARRIVA
VÝCHODNÍ	východní	k2eAgFnPc4d1	východní
ČECHY	Čechy	k1gFnPc4	Čechy
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
říční	říční	k2eAgInSc1d1	říční
přístav	přístav	k1gInSc1	přístav
a	a	k8xC	a
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
rodáci	rodák	k1gMnPc1	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Vlk	Vlk	k1gMnSc1	Vlk
(	(	kIx(	(
<g/>
†	†	k?	†
1439	[number]	k4	1439
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
radikální	radikální	k2eAgMnSc1d1	radikální
husitský	husitský	k2eAgMnSc1d1	husitský
kněz	kněz	k1gMnSc1	kněz
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Krčín	Krčín	k1gMnSc1	Krčín
z	z	k7c2	z
Jelčan	Jelčan	k1gMnSc1	Jelčan
a	a	k8xC	a
Sedlčan	Sedlčany	k1gInPc2	Sedlčany
(	(	kIx(	(
<g/>
1535	[number]	k4	1535
<g/>
–	–	k?	–
<g/>
1604	[number]	k4	1604
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rybníkář	rybníkář	k1gMnSc1	rybníkář
</s>
</p>
<p>
<s>
Jean	Jean	k1gMnSc1	Jean
Gaspard	Gaspard	k1gMnSc1	Gaspard
Deburau	Deburaa	k1gFnSc4	Deburaa
(	(	kIx(	(
<g/>
1796	[number]	k4	1796
<g/>
–	–	k?	–
<g/>
1846	[number]	k4	1846
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
mim	mim	k1gMnSc1	mim
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
</s>
</p>
<p>
<s>
Vincenc	Vincenc	k1gMnSc1	Vincenc
Morstadt	Morstadt	k1gMnSc1	Morstadt
(	(	kIx(	(
<g/>
1802	[number]	k4	1802
<g/>
–	–	k?	–
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Čeněk	Čeněk	k1gMnSc1	Čeněk
Hevera	Hever	k1gMnSc2	Hever
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
odborný	odborný	k2eAgMnSc1d1	odborný
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kmoch	Kmoch	k?	Kmoch
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
kapelník	kapelník	k1gMnSc1	kapelník
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Popper-Lynkeus	Popper-Lynkeus	k1gMnSc1	Popper-Lynkeus
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
židovský	židovský	k2eAgMnSc1d1	židovský
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
</s>
</p>
<p>
<s>
Ignaz	Ignaz	k1gInSc1	Ignaz
Petschek	Petschek	k1gInSc1	Petschek
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
židovský	židovský	k2eAgMnSc1d1	židovský
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
průmyslník	průmyslník	k1gMnSc1	průmyslník
<g/>
,	,	kIx,	,
uhlobaron	uhlobaron	k1gMnSc1	uhlobaron
a	a	k8xC	a
mecenáš	mecenáš	k1gMnSc1	mecenáš
</s>
</p>
<p>
<s>
Julius	Julius	k1gMnSc1	Julius
Petschek	Petschek	k1gMnSc1	Petschek
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
židovský	židovský	k2eAgMnSc1d1	židovský
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
bankéř	bankéř	k1gMnSc1	bankéř
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
stavitel	stavitel	k1gMnSc1	stavitel
Petschkova	Petschkov	k1gInSc2	Petschkov
paláce	palác	k1gInSc2	palác
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Leger	Leger	k1gMnSc1	Leger
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Machar	Machar	k1gMnSc1	Machar
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Radimský	Radimský	k2eAgMnSc1d1	Radimský
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Terezie	Terezie	k1gFnSc1	Terezie
Brzková	Brzková	k1gFnSc1	Brzková
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Jelínková	Jelínková	k1gFnSc1	Jelínková
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Vincenc	Vincenc	k1gMnSc1	Vincenc
Červinka	Červinka	k1gMnSc1	Červinka
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
</s>
</p>
<p>
<s>
Otokar	Otokar	k1gMnSc1	Otokar
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
</s>
</p>
<p>
<s>
Lev	Lev	k1gMnSc1	Lev
Borský	Borský	k1gMnSc1	Borský
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
diplomat	diplomat	k1gMnSc1	diplomat
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kremlička	Kremlička	k1gFnSc1	Kremlička
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
von	von	k1gInSc1	von
Czibulka	Czibulka	k1gFnSc1	Czibulka
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česko-rakouský	českoakouský	k2eAgMnSc1d1	česko-rakouský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Sudek	sudka	k1gFnPc2	sudka
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
vl	vl	k?	vl
<g/>
.	.	kIx.	.
jm	jm	k?	jm
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Radimský	Radimský	k2eAgMnSc1d1	Radimský
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Janík	Janík	k1gMnSc1	Janík
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
knihovník	knihovník	k1gMnSc1	knihovník
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgMnSc1d1	kulturní
pracovník	pracovník	k1gMnSc1	pracovník
</s>
</p>
<p>
<s>
Jarmila	Jarmila	k1gFnSc1	Jarmila
Svatá	svatý	k2eAgFnSc1d1	svatá
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Morávek	Morávek	k1gMnSc1	Morávek
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
hrdina	hrdina	k1gMnSc1	hrdina
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
odbojové	odbojový	k2eAgFnSc2d1	odbojová
skupiny	skupina	k1gFnSc2	skupina
Tři	tři	k4xCgFnPc4	tři
králové	králová	k1gFnPc1	králová
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Březina	Březina	k1gMnSc1	Březina
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgMnSc1d1	sovětský
polární	polární	k2eAgMnSc1d1	polární
letec	letec	k1gMnSc1	letec
<g/>
,	,	kIx,	,
oběť	oběť	k1gFnSc1	oběť
Velké	velký	k2eAgFnSc2d1	velká
čistky	čistka	k1gFnSc2	čistka
</s>
</p>
<p>
<s>
Luboš	Luboš	k1gMnSc1	Luboš
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
diplomat	diplomat	k1gMnSc1	diplomat
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Randová	Randová	k1gFnSc1	Randová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnPc1d1	operní
pěvkyně	pěvkyně	k1gFnPc1	pěvkyně
</s>
</p>
<p>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
Geprtová	Geprtová	k1gFnSc1	Geprtová
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Skalický	Skalický	k1gMnSc1	Skalický
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Mejstřík	Mejstřík	k1gMnSc1	Mejstřík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
studentských	studentský	k2eAgMnPc2d1	studentský
vůdců	vůdce	k1gMnPc2	vůdce
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Bohdan	Bohdan	k1gMnSc1	Bohdan
Ulihrach	Ulihrach	k1gMnSc1	Ulihrach
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
</s>
</p>
<p>
<s>
Vít	Vít	k1gMnSc1	Vít
Rakušan	Rakušan	k1gMnSc1	Rakušan
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Čáslava	Čáslava	k1gFnSc1	Čáslava
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Fialová	Fialová	k1gFnSc1	Fialová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Barbora	Barbora	k1gFnSc1	Barbora
Poláková	Poláková	k1gFnSc1	Poláková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgNnPc1d1	správní
území	území	k1gNnPc1	území
==	==	k?	==
</s>
</p>
<p>
<s>
Kolín	Kolín	k1gInSc1	Kolín
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Kolín	Kolín	k1gInSc1	Kolín
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
89	[number]	k4	89
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Kolín	Kolín	k1gInSc1	Kolín
z	z	k7c2	z
69	[number]	k4	69
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Kamenz	Kamenz	k1gInSc1	Kamenz
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Gransee	Gransee	k1gNnSc1	Gransee
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Lubań	Lubań	k?	Lubań
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
Dietikon	Dietikon	k1gNnSc1	Dietikon
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
</s>
</p>
<p>
<s>
Rimavská	rimavský	k2eAgFnSc1d1	Rimavská
Sobota	sobota	k1gFnSc1	sobota
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FIEDLER	Fiedler	k1gMnSc1	Fiedler
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Židovské	židovský	k2eAgFnPc1d1	židovská
památky	památka	k1gFnPc1	památka
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Sefer	Sefer	k1gMnSc1	Sefer
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
200	[number]	k4	200
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
900895	[number]	k4	900895
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PROCHÁZKA	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Kolín	Kolín	k1gInSc1	Kolín
ve	v	k7c6	v
fotografii	fotografia	k1gFnSc6	fotografia
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Středočeské	středočeský	k2eAgNnSc1d1	Středočeské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
a	a	k8xC	a
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠORM	Šorm	k1gMnSc1	Šorm
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Pověsti	pověst	k1gFnSc2	pověst
o	o	k7c6	o
českých	český	k2eAgInPc6d1	český
zvonech	zvon	k1gInPc6	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
V.	V.	kA	V.
Kotrba	kotrba	k1gFnSc1	kotrba
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OTTA	Otta	k1gMnSc1	Otta
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
-	-	kIx~	-
14	[number]	k4	14
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
str	str	kA	str
<g/>
.	.	kIx.	.
555	[number]	k4	555
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Otta	Otta	k1gMnSc1	Otta
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kolín	Kolín	k1gInSc1	Kolín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Kolín	Kolín	k1gInSc4	Kolín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Kolín	Kolín	k1gInSc1	Kolín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
Kolína	Kolín	k1gInSc2	Kolín
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Kolín	Kolín	k1gInSc1	Kolín
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
od	od	k7c2	od
severu	sever	k1gInSc2	sever
před	před	k7c7	před
obnovou	obnova	k1gFnSc7	obnova
<g/>
,	,	kIx,	,
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Mocker	Mocker	k1gMnSc1	Mocker
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
</s>
</p>
<p>
<s>
Polooficiální	polooficiální	k2eAgFnPc1d1	polooficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
článek	článek	k1gInSc1	článek
o	o	k7c6	o
městě	město	k1gNnSc6	město
</s>
</p>
<p>
<s>
Zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
stránky	stránka	k1gFnPc1	stránka
s	s	k7c7	s
fotomapou	fotomapat	k5eAaPmIp3nP	fotomapat
a	a	k8xC	a
galerií	galerie	k1gFnPc2	galerie
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
Kolína	Kolín	k1gInSc2	Kolín
</s>
</p>
<p>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
televize	televize	k1gFnSc1	televize
|	|	kIx~	|
Zprávy	zpráva	k1gFnPc1	zpráva
a	a	k8xC	a
reportáže	reportáž	k1gFnPc1	reportáž
z	z	k7c2	z
regionu	region	k1gInSc2	region
</s>
</p>
<p>
<s>
Neoficiální	neoficiální	k2eAgFnPc1d1	neoficiální
turistické	turistický	k2eAgFnPc1d1	turistická
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
a	a	k8xC	a
regionu	region	k1gInSc2	region
</s>
</p>
<p>
<s>
Statistické	statistický	k2eAgInPc4d1	statistický
údaje	údaj	k1gInPc4	údaj
-	-	kIx~	-
Městská	městský	k2eAgFnSc1d1	městská
a	a	k8xC	a
Obecní	obecní	k2eAgFnSc1d1	obecní
Statistika	statistika	k1gFnSc1	statistika
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
ESSO	ESSO	kA	ESSO
-	-	kIx~	-
Areál	areál	k1gInSc1	areál
s	s	k7c7	s
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
elektrárnou	elektrárna	k1gFnSc7	elektrárna
ESSO	ESSO	kA	ESSO
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Zibohlavy	Zibohlava	k1gFnSc2	Zibohlava
</s>
</p>
<p>
<s>
Památky	památka	k1gFnPc1	památka
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
</s>
</p>
