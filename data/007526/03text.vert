<s>
Starobrněnský	Starobrněnský	k2eAgInSc4d1	Starobrněnský
klášter	klášter	k1gInSc4	klášter
založila	založit	k5eAaPmAgFnS	založit
jako	jako	k9	jako
klášter	klášter	k1gInSc4	klášter
cisterciaček	cisterciačka	k1gFnPc2	cisterciačka
roku	rok	k1gInSc2	rok
1323	[number]	k4	1323
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
polská	polský	k2eAgFnSc1d1	polská
královna	královna	k1gFnSc1	královna
vdova	vdova	k1gFnSc1	vdova
Eliška	Eliška	k1gFnSc1	Eliška
Rejčka	Rejčka	k1gFnSc1	Rejčka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pochována	pochován	k2eAgFnSc1d1	pochována
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
moravským	moravský	k2eAgMnSc7d1	moravský
zemským	zemský	k2eAgMnSc7d1	zemský
hejtmanem	hejtman	k1gMnSc7	hejtman
Jindřichem	Jindřich	k1gMnSc7	Jindřich
z	z	k7c2	z
Lipé	Lipé	k1gNnSc2	Lipé
<g/>
.	.	kIx.	.
</s>
<s>
Prostředky	prostředek	k1gInPc1	prostředek
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
kláštera	klášter	k1gInSc2	klášter
Eliška	Eliška	k1gFnSc1	Eliška
získala	získat	k5eAaPmAgFnS	získat
od	od	k7c2	od
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jí	on	k3xPp3gFnSc3	on
věnoval	věnovat	k5eAaImAgInS	věnovat
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1333	[number]	k4	1333
byl	být	k5eAaImAgMnS	být
ke	k	k7c3	k
klášteru	klášter	k1gInSc3	klášter
přistavěn	přistavěn	k2eAgInSc1d1	přistavěn
špitál	špitál	k1gInSc1	špitál
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
se	se	k3xPyFc4	se
budoval	budovat	k5eAaImAgInS	budovat
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Eliška	Eliška	k1gFnSc1	Eliška
se	se	k3xPyFc4	se
jeho	on	k3xPp3gNnSc2	on
dokončení	dokončení	k1gNnSc2	dokončení
nedočkala	dočkat	k5eNaPmAgFnS	dočkat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
byl	být	k5eAaImAgInS	být
císařským	císařský	k2eAgInSc7d1	císařský
výnosem	výnos	k1gInSc7	výnos
klášter	klášter	k1gInSc1	klášter
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
majetek	majetek	k1gInSc1	majetek
připadl	připadnout	k5eAaPmAgInS	připadnout
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
předán	předat	k5eAaPmNgInS	předat
augustiniánům	augustinián	k1gMnPc3	augustinián
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
objekt	objekt	k1gInSc4	objekt
původního	původní	k2eAgInSc2d1	původní
augustiniánského	augustiniánský	k2eAgInSc2d1	augustiniánský
konventu	konvent	k1gInSc2	konvent
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Tomáše	Tomáš	k1gMnSc2	Tomáš
na	na	k7c6	na
nynějším	nynější	k2eAgNnSc6d1	nynější
Moravském	moravský	k2eAgNnSc6d1	Moravské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
funguje	fungovat	k5eAaImIp3nS	fungovat
ve	v	k7c6	v
starobrněnském	starobrněnský	k2eAgInSc6d1	starobrněnský
klášteře	klášter	k1gInSc6	klášter
augustiniánské	augustiniánský	k2eAgNnSc4d1	augustiniánské
Opatství	opatství	k1gNnSc4	opatství
svatého	svatý	k2eAgMnSc2d1	svatý
Tomáše	Tomáš	k1gMnSc2	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bazilika	bazilika	k1gFnSc1	bazilika
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kláštera	klášter	k1gInSc2	klášter
je	být	k5eAaImIp3nS	být
gotický	gotický	k2eAgInSc4d1	gotický
kostel	kostel	k1gInSc4	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
z	z	k7c2	z
neomítnutých	omítnutý	k2eNgFnPc2d1	neomítnutá
cihel	cihla	k1gFnPc2	cihla
(	(	kIx(	(
<g/>
slezský	slezský	k2eAgInSc1d1	slezský
styl	styl	k1gInSc1	styl
<g/>
)	)	kIx)	)
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stál	stát	k5eAaImAgInS	stát
románský	románský	k2eAgInSc1d1	románský
kostelík	kostelík	k1gInSc1	kostelík
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
dlažbou	dlažba	k1gFnSc7	dlažba
chrámové	chrámový	k2eAgFnSc2d1	chrámová
lodi	loď	k1gFnSc2	loď
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
označeném	označený	k2eAgNnSc6d1	označené
korunkou	korunka	k1gFnSc7	korunka
a	a	k8xC	a
písmenem	písmeno	k1gNnSc7	písmeno
E	E	kA	E
je	být	k5eAaImIp3nS	být
hrobka	hrobka	k1gFnSc1	hrobka
Elišky	Eliška	k1gFnSc2	Eliška
Rejčky	Rejčka	k1gFnSc2	Rejčka
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
opatství	opatství	k1gNnSc2	opatství
byly	být	k5eAaImAgInP	být
barokizovány	barokizován	k2eAgInPc1d1	barokizován
Mořicem	Mořic	k1gMnSc7	Mořic
Grimmem	Grimm	k1gMnSc7	Grimm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
Řád	řád	k1gInSc1	řád
augustiniánů	augustinián	k1gMnPc2	augustinián
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1344	[number]	k4	1344
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
moravského	moravský	k2eAgMnSc2d1	moravský
markraběte	markrabě	k1gMnSc2	markrabě
Jana	Jan	k1gMnSc2	Jan
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
založil	založit	k5eAaPmAgMnS	založit
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1350	[number]	k4	1350
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
jejich	jejich	k3xOp3gInSc1	jejich
klášter	klášter	k1gInSc1	klášter
s	s	k7c7	s
chrámem	chrám	k1gInSc7	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Tomáše	Tomáš	k1gMnSc2	Tomáš
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
dokončení	dokončení	k1gNnSc2	dokončení
jeho	jeho	k3xOp3gNnSc2	jeho
kněžiště	kněžiště	k1gNnSc2	kněžiště
1356	[number]	k4	1356
i	i	k8xC	i
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
koncipován	koncipovat	k5eAaBmNgInS	koncipovat
jako	jako	k8xC	jako
místo	místo	k7c2	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
moravských	moravský	k2eAgNnPc2d1	Moravské
markrabat	markrabě	k1gNnPc2	markrabě
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
pohřben	pohřben	k2eAgMnSc1d1	pohřben
syn	syn	k1gMnSc1	syn
zakladatele	zakladatel	k1gMnSc2	zakladatel
markrabě	markrabě	k1gMnSc1	markrabě
a	a	k8xC	a
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Jošt	Jošt	k1gMnSc1	Jošt
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
věnoval	věnovat	k5eAaPmAgMnS	věnovat
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
Janu	Jan	k1gMnSc3	Jan
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
pro	pro	k7c4	pro
klášter	klášter	k1gInSc4	klášter
obraz	obraz	k1gInSc4	obraz
byzantsko-italského	byzantskotalský	k2eAgInSc2d1	byzantsko-italský
původu	původ	k1gInSc2	původ
-	-	kIx~	-
Černou	černý	k2eAgFnSc4d1	černá
Madonu	Madona	k1gFnSc4	Madona
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
uchováván	uchovávat	k5eAaImNgInS	uchovávat
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
středověkých	středověký	k2eAgFnPc2d1	středověká
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozšířením	rozšíření	k1gNnSc7	rozšíření
opevnění	opevnění	k1gNnSc2	opevnění
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
kláštera	klášter	k1gInSc2	klášter
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
i	i	k8xC	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
řádu	řád	k1gInSc2	řád
dosvědčuje	dosvědčovat	k5eAaImIp3nS	dosvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
získal	získat	k5eAaPmAgMnS	získat
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
papežské	papežský	k2eAgNnSc4d1	papežské
právo	právo	k1gNnSc4	právo
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
představeného	představený	k1gMnSc4	představený
hodnost	hodnost	k1gFnSc1	hodnost
infulovaného	infulovaný	k2eAgMnSc2d1	infulovaný
opata	opat	k1gMnSc2	opat
<g/>
,	,	kIx,	,
kteréžto	kteréžto	k?	kteréžto
označení	označení	k1gNnSc4	označení
jinak	jinak	k6eAd1	jinak
augustiniáni	augustinián	k1gMnPc1	augustinián
jako	jako	k8xS	jako
žebravý	žebravý	k2eAgInSc4d1	žebravý
řád	řád	k1gInSc4	řád
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Brněnské	brněnský	k2eAgNnSc1d1	brněnské
opatství	opatství	k1gNnSc1	opatství
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
augustiniánským	augustiniánský	k2eAgNnSc7d1	augustiniánské
opatstvím	opatství	k1gNnSc7	opatství
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Reformy	reforma	k1gFnSc2	reforma
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
augustiniánského	augustiniánský	k2eAgInSc2d1	augustiniánský
kláštera	klášter	k1gInSc2	klášter
značně	značně	k6eAd1	značně
dotkly	dotknout	k5eAaPmAgFnP	dotknout
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
význam	význam	k1gInSc1	význam
kláštera	klášter	k1gInSc2	klášter
jej	on	k3xPp3gMnSc4	on
ochránil	ochránit	k5eAaPmAgMnS	ochránit
před	před	k7c7	před
zrušením	zrušení	k1gNnSc7	zrušení
<g/>
.	.	kIx.	.
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
totiž	totiž	k9	totiž
nařídil	nařídit	k5eAaPmAgMnS	nařídit
výnosem	výnos	k1gInSc7	výnos
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1783	[number]	k4	1783
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
augustiniáni	augustinián	k1gMnPc1	augustinián
opustili	opustit	k5eAaPmAgMnP	opustit
stávající	stávající	k2eAgInSc4d1	stávající
klášter	klášter	k1gInSc4	klášter
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pak	pak	k6eAd1	pak
využil	využít	k5eAaPmAgInS	využít
pro	pro	k7c4	pro
umístění	umístění	k1gNnSc4	umístění
moravských	moravský	k2eAgInPc2d1	moravský
politických	politický	k2eAgInPc2d1	politický
úřadů	úřad	k1gInPc2	úřad
(	(	kIx(	(
<g/>
místodržitelství	místodržitelství	k1gNnSc1	místodržitelství
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Moravská	moravský	k2eAgFnSc1d1	Moravská
galerie	galerie	k1gFnSc1	galerie
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
náměstí	náměstí	k1gNnSc6	náměstí
čp.	čp.	k?	čp.
1	[number]	k4	1
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Augustiniáni	augustinián	k1gMnPc1	augustinián
náhradou	náhrada	k1gFnSc7	náhrada
dostali	dostat	k5eAaPmAgMnP	dostat
budovy	budova	k1gFnSc2	budova
zrušeného	zrušený	k2eAgInSc2d1	zrušený
kláštera	klášter	k1gInSc2	klášter
cisterciaček	cisterciačka	k1gFnPc2	cisterciačka
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
Mendlově	Mendlův	k2eAgNnSc6d1	Mendlovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
klášter	klášter	k1gInSc4	klášter
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
kostelu	kostel	k1gInSc3	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
udělil	udělit	k5eAaPmAgInS	udělit
titul	titul	k1gInSc1	titul
bazilika	bazilika	k1gFnSc1	bazilika
minor	minor	k2eAgNnSc7d1	minor
apoštolským	apoštolský	k2eAgNnSc7d1	apoštolské
brevem	breve	k1gNnSc7	breve
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
umístěno	umístěn	k2eAgNnSc1d1	umístěno
Mendelovo	Mendelův	k2eAgNnSc1d1	Mendelovo
muzeum	muzeum	k1gNnSc1	muzeum
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
s	s	k7c7	s
expozicí	expozice	k1gFnSc7	expozice
zakladatele	zakladatel	k1gMnSc2	zakladatel
genetiky	genetika	k1gFnSc2	genetika
Gregora	Gregor	k1gMnSc2	Gregor
Johana	Johan	k1gMnSc2	Johan
Mendla	Mendl	k1gMnSc2	Mendl
(	(	kIx(	(
<g/>
1822	[number]	k4	1822
<g/>
–	–	k?	–
<g/>
84	[number]	k4	84
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
opatem	opat	k1gMnSc7	opat
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
letech	let	k1gInPc6	let
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
84	[number]	k4	84
a	a	k8xC	a
po	po	k7c6	po
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
klášter	klášter	k1gInSc1	klášter
nalézá	nalézat	k5eAaImIp3nS	nalézat
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
augustiniánské	augustiniánský	k2eAgFnSc2d1	augustiniánská
thurnovské	thurnovský	k2eAgFnSc2d1	thurnovský
hudební	hudební	k2eAgFnSc2d1	hudební
fundace	fundace	k1gFnSc2	fundace
vychováván	vychováván	k2eAgMnSc1d1	vychováván
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
ředitel	ředitel	k1gMnSc1	ředitel
kůru	kůra	k1gFnSc4	kůra
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
z	z	k7c2	z
klášterního	klášterní	k2eAgInSc2d1	klášterní
pivovaru	pivovar	k1gInSc2	pivovar
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dnešní	dnešní	k2eAgInSc1d1	dnešní
pivovar	pivovar	k1gInSc1	pivovar
Starobrno	Starobrno	k1gNnSc1	Starobrno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
jedenáctým	jedenáctý	k4xOgMnSc7	jedenáctý
opatem	opat	k1gMnSc7	opat
kláštera	klášter	k1gInSc2	klášter
Lukáš	Lukáš	k1gMnSc1	Lukáš
Evžen	Evžen	k1gMnSc1	Evžen
Martinec	Martinec	k1gMnSc1	Martinec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dle	dle	k7c2	dle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
požádal	požádat	k5eAaPmAgInS	požádat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
náročné	náročný	k2eAgFnSc2d1	náročná
práce	práce	k1gFnSc2	práce
o	o	k7c4	o
dočasné	dočasný	k2eAgNnSc4d1	dočasné
uvolnění	uvolnění	k1gNnSc4	uvolnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
mu	on	k3xPp3gMnSc3	on
generál	generál	k1gMnSc1	generál
řádu	řád	k1gInSc3	řád
Alejandro	Alejandra	k1gFnSc5	Alejandra
Moralo	Morala	k1gFnSc5	Morala
schválil	schválit	k5eAaPmAgInS	schválit
roční	roční	k2eAgFnSc4d1	roční
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Martinec	Martinec	k1gMnSc1	Martinec
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
vyvázán	vyvázat	k5eAaPmNgInS	vyvázat
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
diecézi	diecéze	k1gFnSc6	diecéze
a	a	k8xC	a
ukončil	ukončit	k5eAaPmAgMnS	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
opata	opat	k1gMnSc4	opat
ve	v	k7c6	v
starobrněnském	starobrněnský	k2eAgInSc6d1	starobrněnský
klášteře	klášter	k1gInSc6	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
data	datum	k1gNnSc2	datum
tedy	tedy	k8xC	tedy
správu	správa	k1gFnSc4	správa
opatství	opatství	k1gNnSc2	opatství
převzal	převzít	k5eAaPmAgMnS	převzít
delegát	delegát	k1gMnSc1	delegát
<g/>
,	,	kIx,	,
pověřený	pověřený	k2eAgMnSc1d1	pověřený
generálem	generál	k1gMnSc7	generál
OSA	osa	k1gFnSc1	osa
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Juan	Juan	k1gMnSc1	Juan
Ignacio	Ignacio	k1gMnSc1	Ignacio
Provecho	Provecha	k1gFnSc5	Provecha
Lopez	Lopez	k1gMnSc1	Lopez
OSA	osa	k1gFnSc1	osa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opatství	opatství	k1gNnSc6	opatství
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
komunita	komunita	k1gFnSc1	komunita
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Tomáš	Tomáš	k1gMnSc1	Tomáš
Bratránek	bratránek	k1gMnSc1	bratránek
František	František	k1gMnSc1	František
Matouš	Matouš	k1gMnSc1	Matouš
Klácel	Klácel	k1gMnSc1	Klácel
Pavel	Pavel	k1gMnSc1	Pavel
Křížkovský	Křížkovský	k2eAgMnSc1d1	Křížkovský
Augustin	Augustin	k1gMnSc1	Augustin
Emil	Emil	k1gMnSc1	Emil
Dorničák	Dorničák	k1gMnSc1	Dorničák
Tomáš	Tomáš	k1gMnSc1	Tomáš
Eduard	Eduard	k1gMnSc1	Eduard
Šilinger	Šilinger	k1gMnSc1	Šilinger
Cyril	Cyril	k1gMnSc1	Cyril
František	František	k1gMnSc1	František
Napp	Napp	k1gMnSc1	Napp
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
František	František	k1gMnSc1	František
Bařina	Bařina	k1gMnSc1	Bařina
Tomáš	Tomáš	k1gMnSc1	Tomáš
Josef	Josef	k1gMnSc1	Josef
Martinec	Martinec	k1gMnSc1	Martinec
Lukáš	Lukáš	k1gMnSc1	Lukáš
Evžen	Evžen	k1gMnSc1	Evžen
Martinec	Martinec	k1gMnSc1	Martinec
</s>
