<s>
Štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
originále	originál	k1gInSc6	originál
Der	drát	k5eAaImRp2nS	drát
Schild	Schild	k1gInSc1	Schild
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
romantická	romantický	k2eAgFnSc1d1	romantická
opera	opera	k1gFnSc1	opera
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
dějstvích	dějství	k1gNnPc6	dějství
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Leopolda	Leopold	k1gMnSc2	Leopold
Eugena	Eugen	k1gMnSc2	Eugen
Měchury	Měchury	k?	Měchury
na	na	k7c4	na
německé	německý	k2eAgNnSc4d1	německé
libreto	libreto	k1gNnSc4	libreto
dramatika	dramatik	k1gMnSc2	dramatik
Karla	Karel	k1gMnSc2	Karel
(	(	kIx(	(
<g/>
Carla	Carl	k1gMnSc2	Carl
<g/>
)	)	kIx)	)
Egona	Egon	k1gMnSc2	Egon
Eberta	Ebert	k1gMnSc2	Ebert
<g/>
.	.	kIx.	.
</s>
