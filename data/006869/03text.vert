<s>
Štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
originále	originál	k1gInSc6	originál
Der	drát	k5eAaImRp2nS	drát
Schild	Schild	k1gInSc1	Schild
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
romantická	romantický	k2eAgFnSc1d1	romantická
opera	opera	k1gFnSc1	opera
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
dějstvích	dějství	k1gNnPc6	dějství
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Leopolda	Leopold	k1gMnSc2	Leopold
Eugena	Eugen	k1gMnSc2	Eugen
Měchury	Měchury	k?	Měchury
na	na	k7c4	na
německé	německý	k2eAgNnSc4d1	německé
libreto	libreto	k1gNnSc4	libreto
dramatika	dramatik	k1gMnSc2	dramatik
Karla	Karel	k1gMnSc2	Karel
(	(	kIx(	(
<g/>
Carla	Carl	k1gMnSc2	Carl
<g/>
)	)	kIx)	)
Egona	Egon	k1gMnSc2	Egon
Eberta	Ebert	k1gMnSc2	Ebert
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1843-44	[number]	k4	1843-44
a	a	k8xC	a
provedena	provést	k5eAaPmNgFnS	provést
pouze	pouze	k6eAd1	pouze
soukromě	soukromě	k6eAd1	soukromě
v	v	k7c6	v
Klatovech	Klatovy	k1gInPc6	Klatovy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc4	libreto
k	k	k7c3	k
romantické	romantický	k2eAgFnSc3d1	romantická
opeře	opera	k1gFnSc3	opera
Štít	štít	k1gInSc4	štít
na	na	k7c4	na
námět	námět	k1gInSc4	námět
vycházející	vycházející	k2eAgInSc4d1	vycházející
z	z	k7c2	z
dobově	dobově	k6eAd1	dobově
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
lidových	lidový	k2eAgFnPc2d1	lidová
kouzelných	kouzelný	k2eAgFnPc2d1	kouzelná
her	hra	k1gFnPc2	hra
napsal	napsat	k5eAaPmAgMnS	napsat
pražský	pražský	k2eAgMnSc1d1	pražský
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgMnSc1d1	píšící
spisovatel	spisovatel	k1gMnSc1	spisovatel
Karl	Karl	k1gMnSc1	Karl
Egon	Egon	k1gMnSc1	Egon
Ebert	Eberta	k1gFnPc2	Eberta
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
-	-	kIx~	-
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
skladatele	skladatel	k1gMnSc4	skladatel
Jana	Jan	k1gMnSc4	Jan
Václava	Václav	k1gMnSc4	Václav
Kalivodu	Kalivoda	k1gMnSc4	Kalivoda
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
-	-	kIx~	-
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
jej	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nezhudebnil	zhudebnit	k5eNaPmAgMnS	zhudebnit
<g/>
,	,	kIx,	,
libreta	libreto	k1gNnPc1	libreto
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ujal	ujmout	k5eAaPmAgInS	ujmout
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
skladatel	skladatel	k1gMnSc1	skladatel
Leopold	Leopold	k1gMnSc1	Leopold
Měchura	Měchura	k?	Měchura
(	(	kIx(	(
<g/>
švagr	švagr	k1gMnSc1	švagr
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
mezi	mezi	k7c4	mezi
18	[number]	k4	18
<g/>
.	.	kIx.	.
červnem	červen	k1gInSc7	červen
1843	[number]	k4	1843
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
březnem	březen	k1gInSc7	březen
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Klatovech	Klatovy	k1gInPc6	Klatovy
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
činně	činně	k6eAd1	činně
tamějšího	tamější	k2eAgInSc2d1	tamější
hudebního	hudební	k2eAgInSc2d1	hudební
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
nese	nést	k5eAaImIp3nS	nést
opusové	opusový	k2eAgNnSc1d1	opusové
číslo	číslo	k1gNnSc1	číslo
59	[number]	k4	59
<g/>
.	.	kIx.	.
</s>
<s>
Štít	štít	k1gInSc1	štít
byl	být	k5eAaImAgInS	být
proveden	proveden	k2eAgInSc1d1	proveden
roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
klatovskými	klatovský	k2eAgMnPc7d1	klatovský
ochotníky	ochotník	k1gMnPc7	ochotník
<g/>
,	,	kIx,	,
posílenými	posílená	k1gFnPc7	posílená
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
hudbou	hudba	k1gFnSc7	hudba
místní	místní	k2eAgFnSc2d1	místní
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
soukromě	soukromě	k6eAd1	soukromě
v	v	k7c6	v
klatovském	klatovský	k2eAgNnSc6d1	Klatovské
divadle	divadlo	k1gNnSc6	divadlo
a	a	k8xC	a
poté	poté	k6eAd1	poté
na	na	k7c6	na
klatovském	klatovský	k2eAgNnSc6d1	Klatovské
děkanství	děkanství	k1gNnSc6	děkanství
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
Eberta	Ebert	k1gMnSc2	Ebert
provedl	provést	k5eAaPmAgMnS	provést
Měchura	Měchura	k?	Měchura
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1850	[number]	k4	1850
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
úpravy	úprava	k1gFnSc2	úprava
(	(	kIx(	(
<g/>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
63	[number]	k4	63
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
rovněž	rovněž	k9	rovněž
klavírní	klavírní	k2eAgInSc4d1	klavírní
výtah	výtah	k1gInSc4	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Ebert	Ebert	k1gInSc1	Ebert
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c6	o
provedení	provedení	k1gNnSc6	provedení
opery	opera	k1gFnSc2	opera
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
na	na	k7c4	na
mnichovské	mnichovský	k2eAgMnPc4d1	mnichovský
<g/>
;	;	kIx,	;
neúspěch	neúspěch	k1gInSc1	neúspěch
lze	lze	k6eAd1	lze
zřejmě	zřejmě	k6eAd1	zřejmě
přičíst	přičíst	k5eAaPmF	přičíst
pasivitě	pasivita	k1gFnSc3	pasivita
samotného	samotný	k2eAgMnSc2d1	samotný
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
Král	Král	k1gMnSc1	Král
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
shromážděným	shromážděný	k2eAgMnPc3d1	shromážděný
dvořanům	dvořan	k1gMnPc3	dvořan
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc2	on
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
spánek	spánek	k1gInSc1	spánek
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
uloupen	uloupen	k2eAgInSc4d1	uloupen
kouzelný	kouzelný	k2eAgInSc4d1	kouzelný
štít	štít	k1gInSc4	štít
poskytující	poskytující	k2eAgFnSc4d1	poskytující
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Slibuje	slibovat	k5eAaImIp3nS	slibovat
ruku	ruka	k1gFnSc4	ruka
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
Griseldy	Griselda	k1gFnSc2	Griselda
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
království	království	k1gNnSc4	království
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
ho	on	k3xPp3gMnSc4	on
získá	získat	k5eAaPmIp3nS	získat
od	od	k7c2	od
nepřátel	nepřítel	k1gMnPc2	nepřítel
zpět	zpět	k6eAd1	zpět
v	v	k7c6	v
čestném	čestný	k2eAgInSc6d1	čestný
souboji	souboj	k1gInSc6	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
splnění	splnění	k1gNnSc3	splnění
tohoto	tento	k3xDgInSc2	tento
úkolu	úkol	k1gInSc2	úkol
se	se	k3xPyFc4	se
přihlásí	přihlásit	k5eAaPmIp3nP	přihlásit
královi	králův	k2eAgMnPc1d1	králův
synovci	synovec	k1gMnPc1	synovec
Valdemar	Valdemara	k1gFnPc2	Valdemara
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
je	být	k5eAaImIp3nS	být
Griselda	Griselda	k1gFnSc1	Griselda
zamilována	zamilován	k2eAgFnSc1d1	zamilována
<g/>
,	,	kIx,	,
a	a	k8xC	a
věrolomný	věrolomný	k2eAgMnSc1d1	věrolomný
Ulf	Ulf	k1gMnSc1	Ulf
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
Ulf	Ulf	k1gMnSc1	Ulf
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
síle	síla	k1gFnSc6	síla
<g/>
,	,	kIx,	,
odvaze	odvaha	k1gFnSc6	odvaha
i	i	k8xC	i
důmyslu	důmysl	k1gInSc6	důmysl
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
mu	on	k3xPp3gMnSc3	on
jistě	jistě	k9	jistě
pomohou	pomoct	k5eAaPmIp3nP	pomoct
splnit	splnit	k5eAaPmF	splnit
úkol	úkol	k1gInSc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vidí	vidět	k5eAaImIp3nS	vidět
přicházet	přicházet	k5eAaImF	přicházet
Griseldu	Griselda	k1gMnSc4	Griselda
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc4	svůj
soka	sok	k1gMnSc4	sok
Valdemara	Valdemara	k1gFnSc1	Valdemara
<g/>
,	,	kIx,	,
ukryje	ukrýt	k5eAaPmIp3nS	ukrýt
se	se	k3xPyFc4	se
a	a	k8xC	a
s	s	k7c7	s
hněvem	hněv	k1gInSc7	hněv
naslouchá	naslouchat	k5eAaImIp3nS	naslouchat
jejich	jejich	k3xOp3gNnSc3	jejich
láskyplnému	láskyplný	k2eAgNnSc3d1	láskyplné
loučení	loučení	k1gNnSc3	loučení
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pusté	pustý	k2eAgNnSc1d1	pusté
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
lese	les	k1gInSc6	les
u	u	k7c2	u
věže	věž	k1gFnSc2	věž
<g/>
)	)	kIx)	)
Valdemar	Valdemara	k1gFnPc2	Valdemara
přemohl	přemoct	k5eAaPmAgMnS	přemoct
nepřátele	nepřítel	k1gMnPc4	nepřítel
a	a	k8xC	a
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
se	se	k3xPyFc4	se
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Ulf	Ulf	k?	Ulf
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
naoko	naoko	k6eAd1	naoko
blahopřeje	blahopřát	k5eAaImIp3nS	blahopřát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
hodlá	hodlat	k5eAaImIp3nS	hodlat
přelstít	přelstít	k5eAaPmF	přelstít
<g/>
.	.	kIx.	.
</s>
<s>
Podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
Valdemara	Valdemara	k1gFnSc1	Valdemara
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
naklonil	naklonit	k5eAaPmAgInS	naklonit
nad	nad	k7c4	nad
prohlubeň	prohlubeň	k1gFnSc4	prohlubeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
svrhne	svrhnout	k5eAaPmIp3nS	svrhnout
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Lesním	lesní	k2eAgInSc7d1	lesní
rohem	roh	k1gInSc7	roh
přivolá	přivolat	k5eAaPmIp3nS	přivolat
královskou	královský	k2eAgFnSc4d1	královská
družinu	družina	k1gFnSc4	družina
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
štít	štít	k1gInSc4	štít
a	a	k8xC	a
že	že	k9	že
Valdemar	Valdemara	k1gFnPc2	Valdemara
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
nepřítelem	nepřítel	k1gMnSc7	nepřítel
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jásotu	jásot	k1gInSc2	jásot
družiny	družina	k1gFnSc2	družina
se	se	k3xPyFc4	se
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
<g/>
)	)	kIx)	)
Valdemar	Valdemara	k1gFnPc2	Valdemara
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
shromáždí	shromáždět	k5eAaImIp3nS	shromáždět
král	král	k1gMnSc1	král
skřítků	skřítek	k1gMnPc2	skřítek
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
poddanými	poddaný	k1gMnPc7	poddaný
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
princ	princ	k1gMnSc1	princ
vzbudí	vzbudit	k5eAaPmIp3nS	vzbudit
<g/>
,	,	kIx,	,
skřeti	skřet	k1gMnPc1	skřet
se	se	k3xPyFc4	se
rozprchnou	rozprchnout	k5eAaPmIp3nP	rozprchnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gMnSc1	jejich
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
a	a	k8xC	a
slibuje	slibovat	k5eAaImIp3nS	slibovat
Valdemarovi	Valdemar	k1gMnSc3	Valdemar
záchranu	záchrana	k1gFnSc4	záchrana
i	i	k8xC	i
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
Ulfovi	Ulfa	k1gMnSc3	Ulfa
<g/>
.	.	kIx.	.
</s>
<s>
Skřítci	skřítek	k1gMnPc1	skřítek
rytíře	rytíř	k1gMnSc2	rytíř
odvádějí	odvádět	k5eAaImIp3nP	odvádět
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
Griselda	Griselda	k1gFnSc1	Griselda
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
komorná	komorný	k2eAgFnSc1d1	komorná
Berta	Berta	k1gFnSc1	Berta
netrpělivě	trpělivě	k6eNd1	trpělivě
očekávají	očekávat	k5eAaImIp3nP	očekávat
výsledek	výsledek	k1gInSc4	výsledek
souboje	souboj	k1gInSc2	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
Ulf	Ulf	k1gFnSc7	Ulf
<g/>
,	,	kIx,	,
přináší	přinášet	k5eAaImIp3nS	přinášet
štít	štít	k1gInSc1	štít
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
za	za	k7c4	za
vítěze	vítěz	k1gMnSc4	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
kouzelný	kouzelný	k2eAgInSc4d1	kouzelný
diamant	diamant	k1gInSc4	diamant
na	na	k7c6	na
štítu	štít	k1gInSc6	štít
ztratil	ztratit	k5eAaPmAgMnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
lesk	lesk	k1gInSc4	lesk
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
ho	on	k3xPp3gMnSc4	on
o	o	k7c4	o
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Griseldina	Griseldin	k2eAgFnSc1d1	Griseldin
komnata	komnata	k1gFnSc1	komnata
<g/>
)	)	kIx)	)
Nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
Griselda	Griselda	k1gFnSc1	Griselda
si	se	k3xPyFc3	se
chce	chtít	k5eAaImIp3nS	chtít
raději	rád	k6eAd2	rád
vzít	vzít	k5eAaPmF	vzít
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
Ulfa	Ulf	k1gInSc2	Ulf
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jí	jíst	k5eAaImIp3nS	jíst
Berta	Berta	k1gFnSc1	Berta
sebevraždu	sebevražda	k1gFnSc4	sebevražda
překazí	překazit	k5eAaPmIp3nS	překazit
<g/>
,	,	kIx,	,
usne	usnout	k5eAaPmIp3nS	usnout
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
sen	sen	k1gInSc1	sen
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Valdemara	Valdemara	k1gFnSc1	Valdemara
potkalo	potkat	k5eAaPmAgNnS	potkat
a	a	k8xC	a
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
Ulf	Ulf	k1gMnSc1	Ulf
provinil	provinit	k5eAaPmAgMnS	provinit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vzbudí	vzbudit	k5eAaPmIp3nS	vzbudit
<g/>
,	,	kIx,	,
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
dvořanů	dvořan	k1gMnPc2	dvořan
včetně	včetně	k7c2	včetně
Ulfa	Ulfum	k1gNnSc2	Ulfum
vypravil	vypravit	k5eAaPmAgInS	vypravit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jim	on	k3xPp3gMnPc3	on
princezna	princezna	k1gFnSc1	princezna
určí	určit	k5eAaPmIp3nP	určit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
lese	les	k1gInSc6	les
u	u	k7c2	u
věže	věž	k1gFnSc2	věž
<g/>
)	)	kIx)	)
Griselda	Griselda	k1gFnSc1	Griselda
přivedla	přivést	k5eAaPmAgFnS	přivést
všechny	všechen	k3xTgInPc1	všechen
ke	k	k7c3	k
věži	věž	k1gFnSc3	věž
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
Ulf	Ulf	k1gFnSc2	Ulf
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
Vladimíra	Vladimír	k1gMnSc4	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
intrikánův	intrikánův	k2eAgInSc4d1	intrikánův
zločin	zločin	k1gInSc4	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Ulf	Ulf	k?	Ulf
sice	sice	k8xC	sice
zapírá	zapírat	k5eAaImIp3nS	zapírat
<g/>
,	,	kIx,	,
Griseldin	Griseldin	k2eAgInSc1d1	Griseldin
příběh	příběh	k1gInSc1	příběh
však	však	k9	však
dosvědčí	dosvědčit	k5eAaPmIp3nS	dosvědčit
sám	sám	k3xTgMnSc1	sám
Valdemar	Valdemara	k1gFnPc2	Valdemara
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Ulf	Ulf	k?	Ulf
je	být	k5eAaImIp3nS	být
zadržen	zadržen	k2eAgInSc1d1	zadržen
a	a	k8xC	a
uvězněn	uvězněn	k2eAgInSc1d1	uvězněn
a	a	k8xC	a
Valdemar	Valdemara	k1gFnPc2	Valdemara
přebírá	přebírat	k5eAaImIp3nS	přebírat
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
diamant	diamant	k1gInSc4	diamant
opět	opět	k6eAd1	opět
nabyl	nabýt	k5eAaPmAgMnS	nabýt
lesku	lesk	k1gInSc2	lesk
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
ochranné	ochranný	k2eAgFnSc2d1	ochranná
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
PETRÁNĚK	PETRÁNĚK	kA	PETRÁNĚK
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Měchura	Měchura	k?	Měchura
<g/>
:	:	kIx,	:
Marie	Marie	k1gFnSc1	Marie
Potocká	Potocký	k2eAgFnSc1d1	Potocká
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
k	k	k7c3	k
premiéře	premiéra	k1gFnSc3	premiéra
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7528	[number]	k4	7528
<g/>
-	-	kIx~	-
<g/>
121	[number]	k4	121
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
REITTEREROVÁ	REITTEREROVÁ	kA	REITTEREROVÁ
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
<g/>
.	.	kIx.	.
</s>
<s>
Měchura	Měchura	k?	Měchura
Leopold	Leopold	k1gMnSc1	Leopold
Eugen	Eugen	k2eAgMnSc1d1	Eugen
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
LUDVOVÁ	Ludvová	k1gFnSc1	Ludvová
<g/>
,	,	kIx,	,
Jitka	Jitka	k1gFnSc1	Jitka
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1	hudební
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
:	:	kIx,	:
Osobnosti	osobnost	k1gFnSc2	osobnost
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Divadelní	divadelní	k2eAgInSc1d1	divadelní
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7008	[number]	k4	7008
<g/>
-	-	kIx~	-
<g/>
188	[number]	k4	188
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1436	[number]	k4	1436
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
332	[number]	k4	332
<g/>
-	-	kIx~	-
<g/>
333	[number]	k4	333
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
