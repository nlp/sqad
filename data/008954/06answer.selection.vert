<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Austrálie	Austrálie	k1gFnSc2	Austrálie
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
modrého	modrý	k2eAgInSc2d1	modrý
obdélníku	obdélník	k1gInSc2	obdélník
<g/>
,	,	kIx,	,
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
Union	union	k1gInSc1	union
Jack	Jack	k1gInSc1	Jack
–	–	k?	–
symbol	symbol	k1gInSc4	symbol
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
–	–	k?	–
připomínající	připomínající	k2eAgFnSc2d1	připomínající
vazby	vazba	k1gFnSc2	vazba
Austrálie	Austrálie	k1gFnSc2	Austrálie
na	na	k7c4	na
britskou	britský	k2eAgFnSc4d1	britská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
