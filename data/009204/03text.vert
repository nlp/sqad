<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Sebastian	Sebastian	k1gMnSc1	Sebastian
Štrauch	Štrauch	k1gMnSc1	Štrauch
alias	alias	k9	alias
GoGoManTV	GoGoManTV	k1gMnSc1	GoGoManTV
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1996	[number]	k4	1996
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
GoGo	GoGo	k6eAd1	GoGo
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgInSc1d1	slovenský
nejodebíranější	odebíraný	k2eAgInSc1d3	odebíraný
youtuber	youtuber	k1gInSc1	youtuber
<g/>
,	,	kIx,	,
let	let	k1gInSc1	let
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
player	player	k1gInSc1	player
a	a	k8xC	a
vlogger	vlogger	k1gInSc1	vlogger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Štrauch	Štrauch	k1gMnSc1	Štrauch
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1996	[number]	k4	1996
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
následně	následně	k6eAd1	následně
i	i	k9	i
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
si	se	k3xPyFc3	se
ruce	ruka	k1gFnPc4	ruka
spálil	spálit	k5eAaPmAgMnS	spálit
o	o	k7c4	o
kamna	kamna	k1gNnPc4	kamna
<g/>
,	,	kIx,	,
po	po	k7c6	po
čemž	což	k3yQnSc6	což
mu	on	k3xPp3gInSc3	on
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
zůstaly	zůstat	k5eAaPmAgInP	zůstat
jizvy	jizva	k1gFnPc4	jizva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
rodinného	rodinný	k2eAgInSc2d1	rodinný
domu	dům	k1gInSc2	dům
v	v	k7c6	v
Rusovcích	Rusovec	k1gInPc6	Rusovec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
církevní	církevní	k2eAgFnSc4d1	církevní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
ale	ale	k9	ale
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
letech	let	k1gInPc6	let
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
úspěšně	úspěšně	k6eAd1	úspěšně
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
se	se	k3xPyFc4	se
po	po	k7c6	po
několikaletém	několikaletý	k2eAgInSc6d1	několikaletý
vztahu	vztah	k1gInSc6	vztah
rozešel	rozejít	k5eAaPmAgMnS	rozejít
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
a	a	k8xC	a
youtuberkou	youtuberka	k1gFnSc7	youtuberka
LucyPug	LucyPuga	k1gFnPc2	LucyPuga
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zase	zase	k9	zase
začali	začít	k5eAaPmAgMnP	začít
stýkat	stýkat	k5eAaImF	stýkat
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Natáčení	natáčení	k1gNnSc2	natáčení
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
video	video	k1gNnSc1	video
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
YouTube	YouTub	k1gInSc5	YouTub
kanále	kanál	k1gInSc6	kanál
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
se	se	k3xPyFc4	se
tvorbou	tvorba	k1gFnSc7	tvorba
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
youtuberů	youtuber	k1gMnPc2	youtuber
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
české	český	k2eAgInPc4d1	český
a	a	k8xC	a
slovenské	slovenský	k2eAgMnPc4d1	slovenský
youtubery	youtuber	k1gMnPc4	youtuber
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
let	let	k1gInSc4	let
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
playe	playe	k1gFnSc7	playe
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
speciální	speciální	k2eAgInSc4d1	speciální
kanál	kanál	k1gInSc4	kanál
na	na	k7c4	na
denní	denní	k2eAgFnPc4d1	denní
vlogy	vloga	k1gFnPc4	vloga
<g/>
.	.	kIx.	.
</s>
<s>
GoGo	GoGo	k6eAd1	GoGo
se	se	k3xPyFc4	se
natáčením	natáčení	k1gNnSc7	natáčení
videí	video	k1gNnPc2	video
živí	živý	k1gMnPc1	živý
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
příjmy	příjem	k1gInPc1	příjem
získává	získávat	k5eAaImIp3nS	získávat
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
reklamních	reklamní	k2eAgInPc2d1	reklamní
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
product	product	k1gInSc4	product
placementem	placement	k1gInSc7	placement
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hlavní	hlavní	k2eAgInSc1d1	hlavní
kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
GoGoManTV	GoGoManTV	k1gFnSc1	GoGoManTV
<g/>
,	,	kIx,	,
sledovalo	sledovat	k5eAaImAgNnS	sledovat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2016	[number]	k4	2016
1,40	[number]	k4	1,40
milionu	milion	k4xCgInSc2	milion
odběratelů	odběratel	k1gMnPc2	odběratel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vlogový	vlogový	k2eAgInSc1d1	vlogový
kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
GoGosVlog	GoGosVlog	k1gInSc1	GoGosVlog
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
tehdy	tehdy	k6eAd1	tehdy
490	[number]	k4	490
tisíc	tisíc	k4xCgInPc2	tisíc
odběratelů	odběratel	k1gMnPc2	odběratel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
akce	akce	k1gFnSc2	akce
zvané	zvaný	k2eAgInPc1d1	zvaný
Utubering	Utubering	k1gInSc1	Utubering
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
youtuber	youtuber	k1gMnSc1	youtuber
na	na	k7c6	na
československé	československý	k2eAgFnSc6d1	Československá
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Tatran	Tatran	k1gInSc4	Tatran
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
GOGO	GOGO	kA	GOGO
–	–	k?	–
Chalan	Chalan	k1gInSc1	Chalan
z	z	k7c2	z
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
vydal	vydat	k5eAaPmAgInS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
hudební	hudební	k2eAgInSc4d1	hudební
videoklip	videoklip	k1gInSc4	videoklip
jako	jako	k8xS	jako
poděkování	poděkování	k1gNnSc4	poděkování
za	za	k7c4	za
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
odběratelů	odběratel	k1gMnPc2	odběratel
jménem	jméno	k1gNnSc7	jméno
MŸ	MŸ	k1gFnSc2	MŸ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
vydal	vydat	k5eAaPmAgInS	vydat
překlad	překlad	k1gInSc4	překlad
své	svůj	k3xOyFgFnSc2	svůj
knihy	kniha	k1gFnSc2	kniha
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
GOGO	GOGO	kA	GOGO
–	–	k?	–
Kluk	kluk	k1gMnSc1	kluk
z	z	k7c2	z
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
GoGoManTV	GoGoManTV	k?	GoGoManTV
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
<p>
<s>
GoGosVlog	GoGosVlog	k1gInSc1	GoGosVlog
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
