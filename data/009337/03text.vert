<p>
<s>
Ledovec	ledovec	k1gInSc4	ledovec
je	být	k5eAaImIp3nS	být
nehomogenní	homogenní	k2eNgNnSc1d1	nehomogenní
přírodní	přírodní	k2eAgNnSc1d1	přírodní
těleso	těleso	k1gNnSc1	těleso
tvořené	tvořený	k2eAgNnSc1d1	tvořené
ledem	led	k1gInSc7	led
<g/>
,	,	kIx,	,
omezené	omezený	k2eAgFnPc1d1	omezená
jen	jen	k9	jen
jinou	jiný	k2eAgFnSc7d1	jiná
horninou	hornina	k1gFnSc7	hornina
<g/>
;	;	kIx,	;
led	led	k1gInSc1	led
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
vlivem	vliv	k1gInSc7	vliv
zemské	zemský	k2eAgFnSc2d1	zemská
tíže	tíž	k1gFnSc2	tíž
obdobně	obdobně	k6eAd1	obdobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
částí	část	k1gFnSc7	část
kryosféry	kryosféra	k1gFnSc2	kryosféra
a	a	k8xC	a
hydrosféry	hydrosféra	k1gFnSc2	hydrosféra
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
jazykovitý	jazykovitý	k2eAgInSc4d1	jazykovitý
nebo	nebo	k8xC	nebo
bochníkovitý	bochníkovitý	k2eAgInSc4d1	bochníkovitý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
studiem	studio	k1gNnSc7	studio
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
glaciologie	glaciologie	k1gFnSc1	glaciologie
a	a	k8xC	a
jistou	jistý	k2eAgFnSc7d1	jistá
částí	část	k1gFnSc7	část
také	také	k9	také
geokryologie	geokryologie	k1gFnSc1	geokryologie
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
hromaděním	hromadění	k1gNnSc7	hromadění
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
okolí	okolí	k1gNnSc2	okolí
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
firn	firn	k1gInSc4	firn
<g/>
,	,	kIx,	,
z	z	k7c2	z
firnu	firn	k1gInSc2	firn
na	na	k7c4	na
firnový	firnový	k2eAgInSc4d1	firnový
led	led	k1gInSc4	led
a	a	k8xC	a
z	z	k7c2	z
firnového	firnový	k2eAgInSc2d1	firnový
ledu	led	k1gInSc2	led
na	na	k7c4	na
led	led	k1gInSc4	led
ledovcový	ledovcový	k2eAgInSc4d1	ledovcový
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
přeměny	přeměna	k1gFnSc2	přeměna
sněhu	sníh	k1gInSc2	sníh
ve	v	k7c4	v
firn	firn	k1gInSc4	firn
je	být	k5eAaImIp3nS	být
nazýván	nazýván	k2eAgInSc1d1	nazýván
firnovatění	firnovatění	k1gNnSc3	firnovatění
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
především	především	k9	především
táním	tání	k1gNnSc7	tání
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
zamrzáním	zamrzání	k1gNnSc7	zamrzání
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
tlaku	tlak	k1gInSc2	tlak
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
i	i	k9	i
se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
zdánlivě	zdánlivě	k6eAd1	zdánlivě
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
proces	proces	k1gInSc4	proces
tání	tání	k1gNnSc1	tání
a	a	k8xC	a
mrznutí	mrznutí	k1gNnSc1	mrznutí
se	se	k3xPyFc4	se
odborně	odborně	k6eAd1	odborně
nazývá	nazývat	k5eAaImIp3nS	nazývat
regelace	regelace	k1gFnSc1	regelace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
činí	činit	k5eAaImIp3nS	činit
z	z	k7c2	z
ledovce	ledovec	k1gInSc2	ledovec
plastické	plastický	k2eAgNnSc1d1	plastické
těleso	těleso	k1gNnSc1	těleso
schopné	schopný	k2eAgNnSc1d1	schopné
vyplňovat	vyplňovat	k5eAaImF	vyplňovat
a	a	k8xC	a
modelovat	modelovat	k5eAaImF	modelovat
reliéf	reliéf	k1gInSc4	reliéf
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
kontinentech	kontinent	k1gInPc6	kontinent
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zonální	zonální	k2eAgInPc1d1	zonální
ledovce	ledovec	k1gInPc1	ledovec
(	(	kIx(	(
<g/>
kontinentální	kontinentální	k2eAgMnSc1d1	kontinentální
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
extrazonální	extrazonální	k2eAgFnSc4d1	extrazonální
variantu	varianta	k1gFnSc4	varianta
v	v	k7c6	v
horských	horský	k2eAgInPc6d1	horský
ledovcích	ledovec	k1gInPc6	ledovec
(	(	kIx(	(
<g/>
údolních	údolní	k2eAgMnPc2d1	údolní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Česká	český	k2eAgFnSc1d1	Česká
terminologie	terminologie	k1gFnSc1	terminologie
označuje	označovat	k5eAaImIp3nS	označovat
slovem	slovo	k1gNnSc7	slovo
ledovec	ledovec	k1gInSc1	ledovec
různá	různý	k2eAgFnSc1d1	různá
rozsahem	rozsah	k1gInSc7	rozsah
a	a	k8xC	a
pohybem	pohyb	k1gInSc7	pohyb
odlišná	odlišný	k2eAgNnPc4d1	odlišné
ledová	ledový	k2eAgNnPc4d1	ledové
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
gigantické	gigantický	k2eAgInPc1d1	gigantický
ledové	ledový	k2eAgInPc1d1	ledový
příkrovy	příkrov	k1gInPc1	příkrov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
rozlehlost	rozlehlost	k1gFnSc4	rozlehlost
označovány	označovat	k5eAaImNgInP	označovat
též	též	k9	též
jako	jako	k9	jako
pevninské	pevninský	k2eAgInPc1d1	pevninský
ledovce	ledovec	k1gInPc1	ledovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
led	led	k1gInSc1	led
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
oceánu	oceán	k1gInSc2	oceán
mnoha	mnoho	k4c7	mnoho
ledovci	ledovec	k1gInPc7	ledovec
a	a	k8xC	a
ledovcovými	ledovcový	k2eAgInPc7d1	ledovcový
proudy	proud	k1gInPc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
menší	malý	k2eAgFnPc4d2	menší
ledovcové	ledovcový	k2eAgFnPc4d1	ledovcová
čepice	čepice	k1gFnPc4	čepice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
icebergů	iceberg	k1gInPc2	iceberg
takovým	takový	k3xDgMnPc3	takový
slovem	slovem	k6eAd1	slovem
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
špička	špička	k1gFnSc1	špička
ledovce	ledovec	k1gInSc2	ledovec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ledovec	ledovec	k1gInSc1	ledovec
končící	končící	k2eAgInSc1d1	končící
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
plovoucí	plovoucí	k2eAgInSc4d1	plovoucí
konec	konec	k1gInSc4	konec
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
šelfový	šelfový	k2eAgInSc1d1	šelfový
ledovec	ledovec	k1gInSc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozsáhlejší	rozsáhlý	k2eAgInPc1d3	nejrozsáhlejší
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
Rossův	Rossův	k2eAgMnSc1d1	Rossův
a	a	k8xC	a
Filchnera-Ronneové	Filchnera-Ronneové	k2eAgMnSc1d1	Filchnera-Ronneové
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc4	dva
větší	veliký	k2eAgInPc4d2	veliký
než	než	k8xS	než
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
ledovců	ledovec	k1gInPc2	ledovec
==	==	k?	==
</s>
</p>
<p>
<s>
Ledovce	ledovec	k1gInPc4	ledovec
dělí	dělit	k5eAaImIp3nS	dělit
česká	český	k2eAgFnSc1d1	Česká
geologie	geologie	k1gFnSc1	geologie
<g/>
,	,	kIx,	,
geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
a	a	k8xC	a
geografie	geografie	k1gFnSc1	geografie
do	do	k7c2	do
typů	typ	k1gInPc2	typ
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
karový	karový	k2eAgInSc4d1	karový
<g/>
,	,	kIx,	,
svahový	svahový	k2eAgInSc4d1	svahový
<g/>
,	,	kIx,	,
údolní	údolní	k2eAgInSc4d1	údolní
<g/>
,	,	kIx,	,
dendritický	dendritický	k2eAgInSc4d1	dendritický
<g/>
,	,	kIx,	,
malaspinský	malaspinský	k2eAgInSc4d1	malaspinský
(	(	kIx(	(
<g/>
úpatní	úpatní	k2eAgInSc4d1	úpatní
<g/>
,	,	kIx,	,
piedmontní	piedmontnět	k5eAaImIp3nS	piedmontnět
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
karový	karový	k2eAgInSc1d1	karový
ledovec	ledovec	k1gInSc1	ledovec
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
vysoko	vysoko	k6eAd1	vysoko
položené	položený	k2eAgFnPc4d1	položená
deprese	deprese	k1gFnPc4	deprese
–	–	k?	–
kary	kara	k1gFnPc4	kara
<g/>
,	,	kIx,	,
karové	karový	k2eAgFnPc4d1	Karová
terasy	terasa	k1gFnPc4	terasa
(	(	kIx(	(
<g/>
vyživovací	vyživovací	k2eAgFnPc4d1	vyživovací
oblasti	oblast	k1gFnPc4	oblast
<g/>
)	)	kIx)	)
–	–	k?	–
na	na	k7c6	na
údolních	údolní	k2eAgInPc6d1	údolní
svazích	svah	k1gInPc6	svah
a	a	k8xC	a
schází	scházet	k5eAaImIp3nS	scházet
mu	on	k3xPp3gNnSc3	on
typický	typický	k2eAgInSc4d1	typický
ledovcový	ledovcový	k2eAgInSc4d1	ledovcový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
odvislý	odvislý	k2eAgInSc1d1	odvislý
především	především	k6eAd1	především
od	od	k7c2	od
tvaru	tvar	k1gInSc2	tvar
karu	kar	k1gInSc2	kar
a	a	k8xC	a
šířka	šířka	k1gFnSc1	šířka
obvykle	obvykle	k6eAd1	obvykle
převládá	převládat	k5eAaImIp3nS	převládat
nad	nad	k7c7	nad
délkou	délka	k1gFnSc7	délka
<g/>
.	.	kIx.	.
</s>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
v	v	k7c6	v
Pyrenejích	Pyreneje	k1gFnPc6	Pyreneje
a	a	k8xC	a
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Karový	karový	k2eAgInSc1d1	karový
ledovec	ledovec	k1gInSc1	ledovec
je	být	k5eAaImIp3nS	být
přechodovým	přechodový	k2eAgInSc7d1	přechodový
typem	typ	k1gInSc7	typ
ke	k	k7c3	k
sněžníku	sněžník	k1gInSc3	sněžník
<g/>
.	.	kIx.	.
<g/>
svahový	svahový	k2eAgInSc1d1	svahový
ledovec	ledovec	k1gInSc1	ledovec
(	(	kIx(	(
<g/>
visutý	visutý	k2eAgMnSc1d1	visutý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
karový	karový	k2eAgInSc1d1	karový
ledovec	ledovec	k1gInSc1	ledovec
<g/>
,	,	kIx,	,
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
deprese	deprese	k1gFnSc1	deprese
na	na	k7c6	na
údolních	údolní	k2eAgInPc6d1	údolní
svazích	svah	k1gInPc6	svah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
alespoň	alespoň	k9	alespoň
krátký	krátký	k2eAgInSc4d1	krátký
splaz	splaz	k1gInSc4	splaz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zavěšen	zavěsit	k5eAaPmNgInS	zavěsit
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
<g/>
.	.	kIx.	.
</s>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
především	především	k9	především
v	v	k7c6	v
Pyrenejích	Pyreneje	k1gFnPc6	Pyreneje
a	a	k8xC	a
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
geomorfologové	geomorfolog	k1gMnPc1	geomorfolog
chápou	chápat	k5eAaImIp3nP	chápat
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
svahový	svahový	k2eAgInSc1d1	svahový
ledovec	ledovec	k1gInSc1	ledovec
ledovce	ledovec	k1gInSc2	ledovec
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
depresích	deprese	k1gFnPc6	deprese
nebo	nebo	k8xC	nebo
na	na	k7c6	na
strukturních	strukturní	k2eAgInPc6d1	strukturní
stupních	stupeň	k1gInPc6	stupeň
na	na	k7c6	na
příkrých	příkrý	k2eAgInPc6d1	příkrý
svazích	svah	k1gInPc6	svah
a	a	k8xC	a
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
ze	z	k7c2	z
sněžníku	sněžník	k1gInSc2	sněžník
v	v	k7c6	v
nivačních	nivační	k2eAgFnPc6d1	nivační
depresích	deprese	k1gFnPc6	deprese
anebo	anebo	k8xC	anebo
nivačních	nivační	k2eAgFnPc6d1	nivační
lištách	lišta	k1gFnPc6	lišta
<g/>
.	.	kIx.	.
</s>
<s>
údolní	údolní	k2eAgInSc1d1	údolní
ledovec	ledovec	k1gInSc1	ledovec
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
vyšší	vysoký	k2eAgFnPc4d2	vyšší
části	část	k1gFnPc4	část
horských	horský	k2eAgNnPc2d1	horské
údolí	údolí	k1gNnPc2	údolí
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dobře	dobře	k6eAd1	dobře
vyvinutou	vyvinutý	k2eAgFnSc4d1	vyvinutá
vyživovací	vyživovací	k2eAgFnSc4d1	vyživovací
oblast	oblast	k1gFnSc4	oblast
i	i	k8xC	i
ledovcový	ledovcový	k2eAgInSc4d1	ledovcový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
ledovce	ledovec	k1gInSc2	ledovec
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
také	také	k9	také
jako	jako	k8xC	jako
alpský	alpský	k2eAgInSc1d1	alpský
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
hojného	hojný	k2eAgNnSc2d1	hojné
zastoupení	zastoupení	k1gNnSc2	zastoupení
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
vyděleny	vydělen	k2eAgInPc1d1	vydělen
údolní	údolní	k2eAgInPc1d1	údolní
ledovce	ledovec	k1gInPc1	ledovec
plazového	plazový	k2eAgInSc2d1	plazový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
údolním	údolní	k2eAgInPc3d1	údolní
ledovcům	ledovec	k1gInPc3	ledovec
alpského	alpský	k2eAgInSc2d1	alpský
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
však	však	k9	však
živeny	živit	k5eAaImNgInP	živit
ledovci	ledovec	k1gInPc7	ledovec
vznikajícími	vznikající	k2eAgInPc7d1	vznikající
v	v	k7c6	v
karech	kar	k1gInPc6	kar
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
ledovcovými	ledovcový	k2eAgFnPc7d1	ledovcová
čapkami	čapka	k1gFnPc7	čapka
na	na	k7c6	na
rozvodích	rozvodí	k1gNnPc6	rozvodí
<g/>
.	.	kIx.	.
<g/>
dendritický	dendritický	k2eAgInSc1d1	dendritický
ledovec	ledovec	k1gInSc1	ledovec
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
vyživovacích	vyživovací	k2eAgFnPc2d1	vyživovací
oblastí	oblast	k1gFnPc2	oblast
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
údolních	údolní	k2eAgInPc2d1	údolní
svahů	svah	k1gInPc2	svah
splývají	splývat	k5eAaImIp3nP	splývat
ledovcové	ledovcový	k2eAgInPc1d1	ledovcový
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
vysokých	vysoký	k2eAgNnPc6d1	vysoké
asijských	asijský	k2eAgNnPc6d1	asijské
pohořích	pohoří	k1gNnPc6	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
dendritického	dendritický	k2eAgInSc2d1	dendritický
ledovce	ledovec	k1gInSc2	ledovec
je	být	k5eAaImIp3nS	být
Fedčenkův	Fedčenkův	k2eAgInSc1d1	Fedčenkův
ledovec	ledovec	k1gInSc1	ledovec
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
77	[number]	k4	77
km	km	kA	km
a	a	k8xC	a
šířce	šířka	k1gFnSc6	šířka
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
km	km	kA	km
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgInSc4	jenž
se	se	k3xPyFc4	se
z	z	k7c2	z
údolních	údolní	k2eAgInPc2d1	údolní
ledovců	ledovec	k1gInPc2	ledovec
napojuje	napojovat	k5eAaImIp3nS	napojovat
34	[number]	k4	34
krátkých	krátký	k2eAgInPc2d1	krátký
ledovcových	ledovcový	k2eAgInPc2d1	ledovcový
splazů	splaz	k1gInPc2	splaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
typu	typ	k1gInSc3	typ
blíží	blížit	k5eAaImIp3nS	blížit
Aletschský	Aletschský	k2eAgInSc4d1	Aletschský
ledovec	ledovec	k1gInSc4	ledovec
v	v	k7c6	v
Bernských	bernský	k2eAgFnPc6d1	Bernská
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
<g/>
malaspinský	malaspinský	k2eAgInSc4d1	malaspinský
typ	typ	k1gInSc4	typ
(	(	kIx(	(
<g/>
podhorský	podhorský	k2eAgInSc4d1	podhorský
<g/>
,	,	kIx,	,
úpatní	úpatní	k2eAgInSc4d1	úpatní
<g/>
,	,	kIx,	,
piedmontní	piedmontnět	k5eAaImIp3nS	piedmontnět
<g/>
)	)	kIx)	)
ledovce	ledovec	k1gInPc1	ledovec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
silného	silný	k2eAgInSc2d1	silný
vývoje	vývoj	k1gInSc2	vývoj
dendritických	dendritický	k2eAgInPc2d1	dendritický
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
často	často	k6eAd1	často
přestupují	přestupovat	k5eAaImIp3nP	přestupovat
přes	přes	k7c4	přes
sedla	sedlo	k1gNnPc4	sedlo
a	a	k8xC	a
rozvodní	rozvodní	k2eAgInPc4d1	rozvodní
hřbety	hřbet	k1gInPc4	hřbet
do	do	k7c2	do
sousedních	sousední	k2eAgNnPc2d1	sousední
údolí	údolí	k1gNnPc2	údolí
(	(	kIx(	(
<g/>
ledovcová	ledovcový	k2eAgFnSc1d1	ledovcová
transfluace	transfluace	k1gFnSc1	transfluace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ledovce	ledovec	k1gInPc4	ledovec
ze	z	k7c2	z
sousedních	sousední	k2eAgNnPc2d1	sousední
údolí	údolí	k1gNnSc2	údolí
se	se	k3xPyFc4	se
při	při	k7c6	při
výchozu	výchoz	k1gInSc6	výchoz
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
spojují	spojovat	k5eAaImIp3nP	spojovat
v	v	k7c4	v
mohutný	mohutný	k2eAgInSc4d1	mohutný
jednotný	jednotný	k2eAgInSc4d1	jednotný
ledovcový	ledovcový	k2eAgInSc4d1	ledovcový
krunýř	krunýř	k1gInSc4	krunýř
<g/>
.	.	kIx.	.
</s>
<s>
Malaspinský	Malaspinský	k2eAgInSc1d1	Malaspinský
typ	typ	k1gInSc1	typ
ledovce	ledovec	k1gInSc2	ledovec
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
typové	typový	k2eAgFnSc2d1	typová
lokality	lokalita	k1gFnSc2	lokalita
Malaspinský	Malaspinský	k2eAgInSc1d1	Malaspinský
ledovec	ledovec	k1gInSc1	ledovec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
5000	[number]	k4	5000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnSc1d1	další
označení	označení	k1gNnSc1	označení
===	===	k?	===
</s>
</p>
<p>
<s>
norský	norský	k2eAgInSc1d1	norský
typ	typ	k1gInSc1	typ
(	(	kIx(	(
<g/>
plošný	plošný	k2eAgInSc4d1	plošný
<g/>
,	,	kIx,	,
fjeldový	fjeldový	k2eAgInSc4d1	fjeldový
<g/>
,	,	kIx,	,
patagonský	patagonský	k2eAgInSc4d1	patagonský
<g/>
,	,	kIx,	,
skandinávský	skandinávský	k2eAgInSc4d1	skandinávský
<g/>
)	)	kIx)	)
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
doopravdy	doopravdy	k6eAd1	doopravdy
o	o	k7c4	o
ledovou	ledový	k2eAgFnSc4d1	ledová
čepici	čepice	k1gFnSc4	čepice
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgFnSc4d1	vznikající
na	na	k7c6	na
plochých	plochý	k2eAgNnPc6d1	ploché
temenech	temeno	k1gNnPc6	temeno
a	a	k8xC	a
vrcholech	vrchol	k1gInPc6	vrchol
a	a	k8xC	a
náhorních	náhorní	k2eAgFnPc6d1	náhorní
plošinách	plošina	k1gFnPc6	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Ledové	ledový	k2eAgFnPc1d1	ledová
čepice	čepice	k1gFnPc1	čepice
mají	mít	k5eAaImIp3nP	mít
vypouklý	vypouklý	k2eAgInSc4d1	vypouklý
profil	profil	k1gInSc4	profil
<g/>
,	,	kIx,	,
a	a	k8xC	a
stékají	stékat	k5eAaImIp3nP	stékat
přes	přes	k7c4	přes
okraj	okraj	k1gInSc4	okraj
splazy	splaz	k1gInPc7	splaz
po	po	k7c6	po
úbočích	úboč	k1gFnPc6	úboč
–	–	k?	–
Folgefonn	Folgefonn	k1gInSc1	Folgefonn
<g/>
,	,	kIx,	,
Jostedalsbreen	Jostedalsbreen	k1gInSc1	Jostedalsbreen
<g/>
,	,	kIx,	,
Svart	Svart	k1gInSc1	Svart
Isen	Isen	k1gInSc1	Isen
<g/>
.	.	kIx.	.
<g/>
radiální	radiální	k2eAgInSc1d1	radiální
ledovec	ledovec	k1gInSc1	ledovec
–	–	k?	–
doopravdy	doopravdy	k6eAd1	doopravdy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ledové	ledový	k2eAgNnSc4d1	ledové
pole	pole	k1gNnSc4	pole
čili	čili	k8xC	čili
soustavu	soustava	k1gFnSc4	soustava
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vlivem	vliv	k1gInSc7	vliv
<g />
.	.	kIx.	.
</s>
<s>
fyzickogeografických	fyzickogeografický	k2eAgInPc2d1	fyzickogeografický
poměrů	poměr	k1gInPc2	poměr
nejsou	být	k5eNaImIp3nP	být
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
ledové	ledový	k2eAgFnSc2d1	ledová
čepice	čepice	k1gFnSc2	čepice
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
z	z	k7c2	z
ledu	led	k1gInSc2	led
vyčnívají	vyčnívat	k5eAaImIp3nP	vyčnívat
jiné	jiný	k2eAgFnPc1d1	jiná
horniny	hornina	k1gFnPc1	hornina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
od	od	k7c2	od
centrálního	centrální	k2eAgNnSc2d1	centrální
místa	místo	k1gNnSc2	místo
radiálně	radiálně	k6eAd1	radiálně
rozbíhají	rozbíhat	k5eAaImIp3nP	rozbíhat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
splazy	splaz	k1gInPc4	splaz
<g/>
.	.	kIx.	.
<g/>
špicberský	špicberský	k2eAgInSc4d1	špicberský
typ	typ	k1gInSc4	typ
(	(	kIx(	(
<g/>
svalbardský	svalbardský	k2eAgInSc4d1	svalbardský
<g/>
)	)	kIx)	)
ledového	ledový	k2eAgNnSc2d1	ledové
pole	pole	k1gNnSc2	pole
má	mít	k5eAaImIp3nS	mít
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
mohutná	mohutný	k2eAgNnPc4d1	mohutné
firnoviště	firnoviště	k1gNnPc4	firnoviště
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
přestupují	přestupovat	k5eAaImIp3nP	přestupovat
široká	široký	k2eAgNnPc1d1	široké
horská	horský	k2eAgNnPc1d1	horské
sedla	sedlo	k1gNnPc1	sedlo
a	a	k8xC	a
průsmyky	průsmyk	k1gInPc1	průsmyk
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
holé	holý	k2eAgFnPc4d1	holá
horské	horský	k2eAgFnPc4d1	horská
hory	hora	k1gFnPc4	hora
(	(	kIx(	(
<g/>
nunataky	nunatak	k1gInPc4	nunatak
<g/>
)	)	kIx)	)
a	a	k8xC	a
hřebeny	hřeben	k1gInPc1	hřeben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
mají	mít	k5eAaImIp3nP	mít
různý	různý	k2eAgInSc4d1	různý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgInPc1	všechen
ledovce	ledovec	k1gInPc1	ledovec
mají	mít	k5eAaImIp3nP	mít
akumulační	akumulační	k2eAgFnSc4d1	akumulační
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgInPc4	který
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
hromadění	hromadění	k1gNnSc3	hromadění
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
a	a	k8xC	a
ablační	ablační	k2eAgFnSc1d1	ablační
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
odtávání	odtávání	k1gNnSc3	odtávání
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odděleny	oddělen	k2eAgInPc1d1	oddělen
myšlenou	myšlený	k2eAgFnSc7d1	myšlená
čarou	čára	k1gFnSc7	čára
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
níž	jenž	k3xRgFnSc7	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
akumulaci	akumulace	k1gFnSc3	akumulace
a	a	k8xC	a
pod	pod	k7c4	pod
niž	jenž	k3xRgFnSc4	jenž
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
ablaci	ablace	k1gFnSc3	ablace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
nejsou	být	k5eNaImIp3nP	být
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
odtávání	odtávání	k1gNnSc4	odtávání
(	(	kIx(	(
<g/>
vyšší	vysoký	k2eAgFnPc1d2	vyšší
zeměpisné	zeměpisný	k2eAgFnPc1d1	zeměpisná
šířky	šířka	k1gFnPc1	šířka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ledovce	ledovec	k1gInPc1	ledovec
nezanikají	zanikat	k5eNaImIp3nP	zanikat
táním	tání	k1gNnSc7	tání
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jako	jako	k9	jako
plovoucí	plovoucí	k2eAgInPc4d1	plovoucí
ledovcové	ledovcový	k2eAgInPc4d1	ledovcový
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
rozlámány	rozlámán	k2eAgInPc4d1	rozlámán
do	do	k7c2	do
icebergů	iceberg	k1gInPc2	iceberg
a	a	k8xC	a
odnášeny	odnášet	k5eAaImNgFnP	odnášet
na	na	k7c4	na
širé	širý	k2eAgNnSc4d1	širé
moře	moře	k1gNnSc4	moře
do	do	k7c2	do
nižších	nízký	k2eAgFnPc2d2	nižší
šířek	šířka	k1gFnPc2	šířka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
postupně	postupně	k6eAd1	postupně
tají	tajit	k5eAaImIp3nP	tajit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ledovec	ledovec	k1gInSc1	ledovec
vzniká	vznikat	k5eAaImIp3nS	vznikat
se	se	k3xPyFc4	se
odborně	odborně	k6eAd1	odborně
nazývá	nazývat	k5eAaImIp3nS	nazývat
kar	kar	k1gInSc4	kar
neboli	neboli	k8xC	neboli
česky	česky	k6eAd1	česky
ledovcový	ledovcový	k2eAgInSc4d1	ledovcový
kotel	kotel	k1gInSc4	kotel
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
sestupuje	sestupovat	k5eAaImIp3nS	sestupovat
ledovcovým	ledovcový	k2eAgNnSc7d1	ledovcové
údolím	údolí	k1gNnSc7	údolí
tzv.	tzv.	kA	tzv.
trogem	trog	k1gInSc7	trog
až	až	k8xS	až
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začíná	začínat	k5eAaImIp3nS	začínat
odtávat	odtávat	k5eAaImF	odtávat
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
odlamovat	odlamovat	k5eAaImF	odlamovat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
telení	telení	k1gNnSc1	telení
ledovců	ledovec	k1gInPc2	ledovec
<g/>
)	)	kIx)	)
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pohyb	pohyb	k1gInSc1	pohyb
ledovců	ledovec	k1gInPc2	ledovec
==	==	k?	==
</s>
</p>
<p>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnPc1	jejich
rychlosti	rychlost	k1gFnPc1	rychlost
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
km	km	kA	km
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vhodné	vhodný	k2eAgFnPc1d1	vhodná
podmínky	podmínka	k1gFnPc1	podmínka
jako	jako	k8xS	jako
příkrý	příkrý	k2eAgInSc1d1	příkrý
svah	svah	k1gInSc1	svah
či	či	k8xC	či
vysoká	vysoký	k2eAgFnSc1d1	vysoká
rychlost	rychlost	k1gFnSc1	rychlost
tvoření	tvoření	k1gNnSc2	tvoření
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
a	a	k8xC	a
Grónsku	Grónsko	k1gNnSc6	Grónsko
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
ledovce	ledovec	k1gInPc1	ledovec
v	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
7	[number]	k4	7
až	až	k9	až
12	[number]	k4	12
km	km	kA	km
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
ledovce	ledovec	k1gInPc1	ledovec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nepohybují	pohybovat	k5eNaImIp3nP	pohybovat
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
přimrznuty	přimrznut	k2eAgFnPc1d1	přimrznut
v	v	k7c4	v
podloží	podloží	k1gNnSc4	podloží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
případě	případ	k1gInSc6	případ
pak	pak	k6eAd1	pak
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
tání	tání	k1gNnSc3	tání
ledovců	ledovec	k1gInPc2	ledovec
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
a	a	k8xC	a
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
studených	studený	k2eAgInPc6d1	studený
ledovcích	ledovec	k1gInPc6	ledovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
barvu	barva	k1gFnSc4	barva
vděčí	vděčit	k5eAaImIp3nS	vděčit
led	led	k1gInSc1	led
krystalové	krystalový	k2eAgFnSc6d1	krystalová
struktuře	struktura	k1gFnSc6	struktura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
všechny	všechen	k3xTgFnPc4	všechen
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
světla	světlo	k1gNnSc2	světlo
kromě	kromě	k7c2	kromě
té	ten	k3xDgFnSc2	ten
nejkratší	krátký	k2eAgFnSc2d3	nejkratší
(	(	kIx(	(
<g/>
nejmodřejší	modrý	k2eAgFnSc2d3	nejmodřejší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modrý	modrý	k2eAgInSc1d1	modrý
led	led	k1gInSc1	led
je	být	k5eAaImIp3nS	být
pevnější	pevný	k2eAgInSc1d2	pevnější
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
méně	málo	k6eAd2	málo
vzduchu	vzduch	k1gInSc2	vzduch
než	než	k8xS	než
led	led	k1gInSc1	led
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
"	"	kIx"	"
<g/>
Tvořivá	tvořivý	k2eAgFnSc1d1	tvořivá
<g/>
"	"	kIx"	"
síla	síla	k1gFnSc1	síla
ledovců	ledovec	k1gInPc2	ledovec
==	==	k?	==
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
ledovců	ledovec	k1gInPc2	ledovec
je	být	k5eAaImIp3nS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
mnoha	mnoho	k4c7	mnoho
geomorfologickými	geomorfologický	k2eAgInPc7d1	geomorfologický
tvary	tvar	k1gInPc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tvořivou	tvořivý	k2eAgFnSc4d1	tvořivá
činnost	činnost	k1gFnSc4	činnost
ledovce	ledovec	k1gInSc2	ledovec
má	mít	k5eAaImIp3nS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
teplota	teplota	k1gFnSc1	teplota
samotného	samotný	k2eAgNnSc2d1	samotné
ledovcového	ledovcový	k2eAgNnSc2d1	ledovcové
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc2	on
podkladu	podklad	k1gInSc2	podklad
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
jeho	jeho	k3xOp3gInSc2	jeho
povrchu	povrch	k1gInSc2	povrch
(	(	kIx(	(
<g/>
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
ploch	plocha	k1gFnPc2	plocha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
"	"	kIx"	"
<g/>
viskozitu	viskozita	k1gFnSc4	viskozita
<g/>
"	"	kIx"	"
báze	báze	k1gFnSc1	báze
ledovce	ledovec	k1gInSc2	ledovec
<g/>
,	,	kIx,	,
kapalná	kapalný	k2eAgFnSc1d1	kapalná
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
prvořadým	prvořadý	k2eAgInSc7d1	prvořadý
modelačním	modelační	k2eAgInSc7d1	modelační
faktorem	faktor	k1gInSc7	faktor
a	a	k8xC	a
tekutost	tekutost	k1gFnSc1	tekutost
báze	báze	k1gFnSc2	báze
ledovce	ledovec	k1gInSc2	ledovec
má	mít	k5eAaImIp3nS	mít
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
glaciálních	glaciální	k2eAgInPc2d1	glaciální
tvarů	tvar	k1gInPc2	tvar
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
destruktivní	destruktivní	k2eAgFnSc1d1	destruktivní
i	i	k8xC	i
akumulační	akumulační	k2eAgFnSc1d1	akumulační
činnost	činnost	k1gFnSc1	činnost
ledovcového	ledovcový	k2eAgNnSc2d1	ledovcové
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Destruktivní	destruktivní	k2eAgFnSc7d1	destruktivní
činností	činnost	k1gFnSc7	činnost
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
zpětná	zpětný	k2eAgFnSc1d1	zpětná
<g/>
,	,	kIx,	,
boční	boční	k2eAgFnSc1d1	boční
a	a	k8xC	a
hloubková	hloubkový	k2eAgFnSc1d1	hloubková
eroze	eroze	k1gFnSc1	eroze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
narušují	narušovat	k5eAaImIp3nP	narušovat
horniny	hornina	k1gFnPc4	hornina
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nejbližším	blízký	k2eAgNnSc6d3	nejbližší
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
účinnosti	účinnost	k1gFnSc3	účinnost
ledovcové	ledovcový	k2eAgFnSc2d1	ledovcová
eroze	eroze	k1gFnSc2	eroze
přispívají	přispívat	k5eAaImIp3nP	přispívat
především	především	k9	především
spodní	spodní	k2eAgFnPc1d1	spodní
morény	moréna	k1gFnPc1	moréna
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
svou	svůj	k3xOyFgFnSc4	svůj
účinnost	účinnost	k1gFnSc4	účinnost
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
vznikem	vznik	k1gInSc7	vznik
hlubokých	hluboký	k2eAgInPc2d1	hluboký
i	i	k8xC	i
mělkých	mělký	k2eAgFnPc2d1	mělká
ledovcových	ledovcový	k2eAgFnPc2d1	ledovcová
rýh	rýha	k1gFnPc2	rýha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Erozní	erozní	k2eAgFnSc4d1	erozní
činnost	činnost	k1gFnSc4	činnost
ledovců	ledovec	k1gInPc2	ledovec
==	==	k?	==
</s>
</p>
<p>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
jsou	být	k5eAaImIp3nP	být
výrazným	výrazný	k2eAgInSc7d1	výrazný
erozním	erozní	k2eAgInSc7d1	erozní
činitelem	činitel	k1gInSc7	činitel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
tvary	tvar	k1gInPc4	tvar
reliéfu	reliéf	k1gInSc2	reliéf
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
působil	působit	k5eAaImAgMnS	působit
a	a	k8xC	a
který	který	k3yRgMnSc1	který
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
identifikovat	identifikovat	k5eAaBmF	identifikovat
jeho	jeho	k3xOp3gNnSc4	jeho
působení	působení	k1gNnSc4	působení
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
tíha	tíha	k1gFnSc1	tíha
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
sestupující	sestupující	k2eAgFnSc2d1	sestupující
ledovcové	ledovcový	k2eAgFnSc2d1	ledovcová
masy	masa	k1gFnSc2	masa
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
typické	typický	k2eAgNnSc1d1	typické
ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
údolí	údolí	k1gNnSc1	údolí
tvaru	tvar	k1gInSc2	tvar
U	U	kA	U
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
může	moct	k5eAaImIp3nS	moct
ledovec	ledovec	k1gInSc1	ledovec
vyhloubit	vyhloubit	k5eAaPmF	vyhloubit
až	až	k9	až
do	do	k7c2	do
několikasetmetrové	několikasetmetrový	k2eAgFnSc2d1	několikasetmetrová
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
několik	několik	k4yIc4	několik
základních	základní	k2eAgInPc2d1	základní
druhů	druh	k1gInPc2	druh
ledovcové	ledovcový	k2eAgFnSc2d1	ledovcová
eroze	eroze	k1gFnSc2	eroze
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
brázdění	brázdění	k1gNnSc1	brázdění
(	(	kIx(	(
<g/>
exarace	exarace	k1gFnSc1	exarace
<g/>
)	)	kIx)	)
–	–	k?	–
postupující	postupující	k2eAgInSc4d1	postupující
ledovec	ledovec	k1gInSc4	ledovec
před	před	k7c7	před
sebou	se	k3xPyFc7	se
tlačí	tlačit	k5eAaImIp3nP	tlačit
úlomky	úlomek	k1gInPc1	úlomek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pod	pod	k7c7	pod
velikým	veliký	k2eAgInSc7d1	veliký
tlakem	tlak	k1gInSc7	tlak
ryjí	rýt	k5eAaImIp3nP	rýt
podložní	podložní	k2eAgFnSc4d1	podložní
horninu	hornina	k1gFnSc4	hornina
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zahlubují	zahlubovat	k5eAaImIp3nP	zahlubovat
ledovcové	ledovcový	k2eAgNnSc4d1	ledovcové
údolí	údolí	k1gNnSc4	údolí
stále	stále	k6eAd1	stále
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
reliéfu	reliéf	k1gInSc2	reliéf
a	a	k8xC	a
případně	případně	k6eAd1	případně
ho	on	k3xPp3gMnSc4	on
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
typický	typický	k2eAgInSc1d1	typický
tvar	tvar	k1gInSc1	tvar
písmene	písmeno	k1gNnSc2	písmeno
U.	U.	kA	U.
Díky	díky	k7c3	díky
rozdílné	rozdílný	k2eAgFnSc3d1	rozdílná
tvrdosti	tvrdost	k1gFnSc3	tvrdost
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
bočních	boční	k2eAgInPc2d1	boční
ledovců	ledovec	k1gInPc2	ledovec
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
druh	druh	k1gInSc4	druh
visutého	visutý	k2eAgNnSc2d1	visuté
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
odlamování	odlamování	k1gNnSc1	odlamování
(	(	kIx(	(
<g/>
detrakce	detrakce	k1gFnSc1	detrakce
<g/>
)	)	kIx)	)
–	–	k?	–
změny	změna	k1gFnPc1	změna
teplot	teplota	k1gFnPc2	teplota
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
roztávání	roztávání	k1gNnSc2	roztávání
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
zamrzání	zamrzání	k1gNnSc4	zamrzání
vody	voda	k1gFnSc2	voda
pod	pod	k7c7	pod
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
vsakuje	vsakovat	k5eAaImIp3nS	vsakovat
do	do	k7c2	do
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
při	při	k7c6	při
zmrznutí	zmrznutí	k1gNnSc6	zmrznutí
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
svůj	svůj	k3xOyFgInSc4	svůj
objem	objem	k1gInSc4	objem
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
roztrhávání	roztrhávání	k1gNnSc2	roztrhávání
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Hornina	hornina	k1gFnSc1	hornina
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
unášena	unášen	k2eAgFnSc1d1	unášena
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ledovcem	ledovec	k1gInSc7	ledovec
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
ohlazováním	ohlazování	k1gNnSc7	ohlazování
vznikají	vznikat	k5eAaImIp3nP	vznikat
souvky	souvek	k1gInPc1	souvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ohlazování	ohlazování	k1gNnSc1	ohlazování
(	(	kIx(	(
<g/>
abraze	abraze	k1gFnSc1	abraze
<g/>
)	)	kIx)	)
a	a	k8xC	a
obrušování	obrušování	k1gNnSc1	obrušování
(	(	kIx(	(
<g/>
deterze	deterze	k1gFnSc1	deterze
<g/>
)	)	kIx)	)
–	–	k?	–
části	část	k1gFnSc6	část
unášené	unášený	k2eAgFnSc6d1	unášená
ledovcem	ledovec	k1gInSc7	ledovec
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
třou	třít	k5eAaImIp3nP	třít
o	o	k7c6	o
jiné	jiný	k2eAgFnSc6d1	jiná
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
na	na	k7c6	na
tělesu	těleso	k1gNnSc6	těleso
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
typické	typický	k2eAgNnSc1d1	typické
rýhování	rýhování	k1gNnSc1	rýhování
a	a	k8xC	a
zahlazování	zahlazování	k1gNnSc1	zahlazování
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
ohlazení	ohlazení	k1gNnSc2	ohlazení
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c4	na
tvrdosti	tvrdost	k1gFnPc4	tvrdost
obou	dva	k4xCgFnPc2	dva
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
boční	boční	k2eAgFnSc6d1	boční
erozi	eroze	k1gFnSc6	eroze
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
boční	boční	k2eAgFnPc1d1	boční
morény	moréna	k1gFnPc1	moréna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ovšem	ovšem	k9	ovšem
podobné	podobný	k2eAgInPc4d1	podobný
projevy	projev	k1gInPc4	projev
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
problém	problém	k1gInSc4	problém
při	při	k7c6	při
stanovení	stanovení	k1gNnSc6	stanovení
přítomnosti	přítomnost	k1gFnSc2	přítomnost
ledovce	ledovec	k1gInSc2	ledovec
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgInPc1d1	významný
světové	světový	k2eAgInPc1d1	světový
ledovce	ledovec	k1gInPc1	ledovec
==	==	k?	==
</s>
</p>
<p>
<s>
Antarktický	antarktický	k2eAgInSc1d1	antarktický
ledovec	ledovec	k1gInSc1	ledovec
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
objem	objem	k1gInSc1	objem
je	být	k5eAaImIp3nS	být
26,5	[number]	k4	26,5
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Grónský	grónský	k2eAgInSc1d1	grónský
ledovec	ledovec	k1gInSc1	ledovec
má	mít	k5eAaImIp3nS	mít
objem	objem	k1gInSc4	objem
2,85	[number]	k4	2,85
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
ledovce	ledovec	k1gInPc1	ledovec
mají	mít	k5eAaImIp3nP	mít
objem	objem	k1gInSc1	objem
0,158	[number]	k4	0,158
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HORNÍK	Horník	k1gMnSc1	Horník
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
geografie	geografie	k1gFnSc1	geografie
II	II	kA	II
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
320	[number]	k4	320
s.	s.	k?	s.
</s>
</p>
<p>
<s>
DEMEK	DEMEK	kA	DEMEK
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
476	[number]	k4	476
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mořský	mořský	k2eAgInSc1d1	mořský
led	led	k1gInSc1	led
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ledovec	ledovec	k1gInSc1	ledovec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ledovec	ledovec	k1gInSc1	ledovec
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
fotografií	fotografia	k1gFnPc2	fotografia
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
let	léto	k1gNnPc2	léto
s	s	k7c7	s
posuvným	posuvný	k2eAgNnSc7d1	posuvné
rozhraním	rozhraní	k1gNnSc7	rozhraní
dokumentující	dokumentující	k2eAgInSc4d1	dokumentující
úbytek	úbytek	k1gInSc4	úbytek
ledovců	ledovec	k1gInPc2	ledovec
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
aktualizovaně	aktualizovaně	k6eAd1	aktualizovaně
na	na	k7c4	na
gletschervergleiche	gletschervergleiche	k1gInSc4	gletschervergleiche
<g/>
.	.	kIx.	.
<g/>
ch	ch	k0	ch
(	(	kIx(	(
<g/>
či	či	k8xC	či
s	s	k7c7	s
českými	český	k2eAgInPc7d1	český
popisky	popisek	k1gInPc7	popisek
na	na	k7c4	na
aktualne	aktualnout	k5eAaPmIp3nS	aktualnout
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
ze	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
