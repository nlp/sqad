<s>
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
pošta	pošta	k1gFnSc1	pošta
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
jako	jako	k8xS	jako
způsob	způsob	k1gInSc1	způsob
komunikace	komunikace	k1gFnSc2	komunikace
více	hodně	k6eAd2	hodně
uživatelů	uživatel	k1gMnPc2	uživatel
mainframového	mainframový	k2eAgInSc2d1	mainframový
počítače	počítač	k1gInSc2	počítač
se	s	k7c7	s
sdílením	sdílení	k1gNnSc7	sdílení
času	čas	k1gInSc2	čas
<g/>
;	;	kIx,	;
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
přesná	přesný	k2eAgFnSc1d1	přesná
historie	historie	k1gFnSc1	historie
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
systémy	systém	k1gInPc7	systém
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
schopností	schopnost	k1gFnSc7	schopnost
byly	být	k5eAaImAgFnP	být
Q32	Q32	k1gFnPc4	Q32
od	od	k7c2	od
SDC	SDC	kA	SDC
a	a	k8xC	a
CTSS	CTSS	kA	CTSS
z	z	k7c2	z
MIT	MIT	kA	MIT
<g/>
.	.	kIx.	.
</s>
