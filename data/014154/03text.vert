<s>
Evangelický	evangelický	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
v	v	k7c6
Lískovci	Lískovec	k1gInSc6
</s>
<s>
Evangelický	evangelický	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
v	v	k7c6
Lískovci	Lískovec	k1gInSc6
Lokalita	lokalita	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Lískovec	Lískovec	k1gInSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
7,26	7,26	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
57,99	57,99	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Evangelický	evangelický	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
v	v	k7c6
Lískovci	Lískovec	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
Frýdku-Místku	Frýdku-Místek	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
místní	místní	k2eAgFnSc6d1
části	část	k1gFnSc6
Lískovec	Lískovec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
okraji	okraj	k1gInSc6
Lískovce	Lískovec	k1gInSc2
<g/>
,	,	kIx,
u	u	k7c2
ul	ul	kA
<g/>
.	.	kIx.
Valcířské	valcířský	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
(	(	kIx(
<g/>
s	s	k7c7
márnicí	márnice	k1gFnSc7
<g/>
)	)	kIx)
v	v	k7c6
letech	let	k1gInPc6
1883	#num#	k4
<g/>
–	–	k?
<g/>
1884	#num#	k4
a	a	k8xC
posvěcen	posvěcen	k2eAgInSc1d1
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1884	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
hutě	huť	k1gFnSc2
na	na	k7c6
darovaném	darovaný	k2eAgInSc6d1
pozemku	pozemek	k1gInSc6
arcivévody	arcivévoda	k1gMnSc2
Alberta	Albert	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
museli	muset	k5eAaImAgMnP
být	být	k5eAaImF
evangelíci	evangelík	k1gMnPc1
pohřbíváni	pohřbívat	k5eAaImNgMnP
v	v	k7c6
ústraní	ústraní	k1gNnSc6
na	na	k7c6
katolických	katolický	k2eAgInPc6d1
hřbitovech	hřbitov	k1gInPc6
mezi	mezi	k7c7
sebevrahy	sebevrah	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
spoluzakladatele	spoluzakladatel	k1gMnSc4
hřbitova	hřbitov	k1gInSc2
náležel	náležet	k5eAaImAgMnS
Jan	Jan	k1gMnSc1
Tacina	Tacina	k1gMnSc1
(	(	kIx(
<g/>
1836	#num#	k4
<g/>
–	–	k?
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vlastníkem	vlastník	k1gMnSc7
hřbitova	hřbitov	k1gInSc2
je	být	k5eAaImIp3nS
Farní	farní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Českobratrské	českobratrský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
evangelické	evangelický	k2eAgFnSc2d1
ve	v	k7c6
Frýdku-Místku	Frýdku-Místek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
protestantských	protestantský	k2eAgInPc2d1
hřbitovů	hřbitov	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Farní	farní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Českobratrské	českobratrský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
evangelické	evangelický	k2eAgFnSc2d1
ve	v	k7c6
Frýdku-Místku	Frýdku-Místek	k1gInSc6
</s>
<s>
Farní	farní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Slezské	slezský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
evangelické	evangelický	k2eAgFnSc2d1
augsburského	augsburský	k2eAgNnSc2d1
vyznání	vyznání	k1gNnSc2
v	v	k7c6
Frýdku-Místku	Frýdku-Místek	k1gInSc6
</s>
<s>
Židovský	židovský	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
ve	v	k7c6
Frýdku	Frýdek	k1gInSc6
</s>
<s>
Centrální	centrální	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
ve	v	k7c6
Frýdku	Frýdek	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
