<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1851	[number]	k4	1851
Hronov	Hronov	k1gInSc1	Hronov
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1930	[number]	k4	1930
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
řady	řada	k1gFnSc2	řada
historických	historický	k2eAgInPc2d1	historický
románů	román	k1gInPc2	román
a	a	k8xC	a
představitel	představitel	k1gMnSc1	představitel
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
dějepisu	dějepis	k1gInSc2	dějepis
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Přátelil	přátelit	k5eAaImAgMnS	přátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
mnoha	mnoho	k4c2	mnoho
vynikajícími	vynikající	k2eAgFnPc7d1	vynikající
osobnostmi	osobnost	k1gFnPc7	osobnost
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
–	–	k?	–
například	například	k6eAd1	například
s	s	k7c7	s
M.	M.	kA	M.
Alšem	Aleš	k1gMnSc7	Aleš
<g/>
,	,	kIx,	,
J.	J.	kA	J.
V.	V.	kA	V.
Sládkem	Sládek	k1gMnSc7	Sládek
<g/>
,	,	kIx,	,
K.	K.	kA	K.
V.	V.	kA	V.
Raisem	Rais	k1gMnSc7	Rais
<g/>
,	,	kIx,	,
J.	J.	kA	J.
S.	S.	kA	S.
Macharem	Machar	k1gMnSc7	Machar
či	či	k8xC	či
Z.	Z.	kA	Z.
Nejedlým	Nejedlý	k1gMnPc3	Nejedlý
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
i	i	k9	i
umělecký	umělecký	k2eAgInSc4d1	umělecký
kroužek	kroužek	k1gInSc4	kroužek
v	v	k7c6	v
Kavárně	kavárna	k1gFnSc6	kavárna
Union	union	k1gInSc1	union
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
čp.	čp.	k?	čp.
524	[number]	k4	524
<g/>
/	/	kIx~	/
<g/>
I	i	k9	i
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Myslbek	Myslbek	k1gMnSc1	Myslbek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Ženíšek	Ženíšek	k1gMnSc1	Ženíšek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Wiehl	Wiehl	k1gMnSc1	Wiehl
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
časopisu	časopis	k1gInSc2	časopis
Zvon	zvon	k1gInSc1	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
samostatnosti	samostatnost	k1gFnSc2	samostatnost
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
národa	národ	k1gInSc2	národ
a	a	k8xC	a
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Manifest	manifest	k1gInSc4	manifest
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
východočeském	východočeský	k2eAgInSc6d1	východočeský
Hronově	Hronov	k1gInSc6	Hronov
u	u	k7c2	u
Náchoda	Náchod	k1gInSc2	Náchod
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
selského	selský	k2eAgInSc2d1	selský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Jirásek	Jirásek	k1gMnSc1	Jirásek
(	(	kIx(	(
<g/>
1822	[number]	k4	1822
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
tkadlec	tkadlec	k1gMnSc1	tkadlec
a	a	k8xC	a
poté	poté	k6eAd1	poté
pekař	pekař	k1gMnSc1	pekař
<g/>
,	,	kIx,	,
matkou	matka	k1gFnSc7	matka
Vincencie	Vincencie	k1gFnSc2	Vincencie
Jirásková	Jirásková	k1gFnSc1	Jirásková
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Prouzová	Prouzová	k1gFnSc1	Prouzová
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
–	–	k?	–
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Aloisem	Alois	k1gMnSc7	Alois
Jiráskem	Jirásek	k1gMnSc7	Jirásek
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc3	jeho
rodičům	rodič	k1gMnPc3	rodič
narodily	narodit	k5eAaPmAgFnP	narodit
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
Emílie	Emílie	k1gFnSc1	Emílie
<g/>
;	;	kIx,	;
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
Žofie	Žofie	k1gFnSc1	Žofie
<g/>
,	,	kIx,	,
Božena	Božena	k1gFnSc1	Božena
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
německé	německý	k2eAgNnSc4d1	německé
benediktinské	benediktinský	k2eAgNnSc4d1	benediktinské
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Broumově	Broumov	k1gInSc6	Broumov
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
67	[number]	k4	67
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
české	český	k2eAgNnSc1d1	české
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
71	[number]	k4	71
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
historii	historie	k1gFnSc4	historie
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
–	–	k?	–
<g/>
74	[number]	k4	74
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnáct	čtrnáct	k4xCc4	čtrnáct
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
tam	tam	k6eAd1	tam
jako	jako	k8xC	jako
gymnaziální	gymnaziální	k2eAgMnSc1d1	gymnaziální
profesor	profesor	k1gMnSc1	profesor
dějepisu	dějepis	k1gInSc2	dějepis
a	a	k8xC	a
zeměpisu	zeměpis	k1gInSc2	zeměpis
<g/>
.	.	kIx.	.
</s>
<s>
Publikovat	publikovat	k5eAaBmF	publikovat
začal	začít	k5eAaPmAgInS	začít
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svých	svůj	k3xOyFgNnPc2	svůj
pražských	pražský	k2eAgNnPc2d1	Pražské
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
vznikají	vznikat	k5eAaImIp3nP	vznikat
první	první	k4xOgFnPc1	první
významné	významný	k2eAgFnPc1d1	významná
práce	práce	k1gFnPc1	práce
(	(	kIx(	(
<g/>
Filozofská	filozofský	k2eAgFnSc1d1	Filozofská
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
Psohlavci	psohlavec	k1gMnPc1	psohlavec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1879	[number]	k4	1879
se	se	k3xPyFc4	se
Jirásek	Jirásek	k1gMnSc1	Jirásek
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Podhajskou	Podhajský	k2eAgFnSc7d1	Podhajská
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
zámožného	zámožný	k2eAgMnSc2d1	zámožný
soukromníka	soukromník	k1gMnSc2	soukromník
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
8	[number]	k4	8
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc4	sedm
dcer	dcera	k1gFnPc2	dcera
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
první	první	k4xOgInPc4	první
byty	byt	k1gInPc4	byt
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
příliš	příliš	k6eAd1	příliš
nevyhovovaly	vyhovovat	k5eNaImAgFnP	vyhovovat
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
za	za	k7c4	za
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
získal	získat	k5eAaPmAgInS	získat
prostorný	prostorný	k2eAgInSc1d1	prostorný
byt	byt	k1gInSc1	byt
v	v	k7c6	v
Resslově	Resslově	k1gFnSc6	Resslově
ulici	ulice	k1gFnSc6	ulice
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
na	na	k7c6	na
nároží	nároží	k1gNnSc6	nároží
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Jiráskova	Jiráskův	k2eAgNnSc2d1	Jiráskovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
osazena	osadit	k5eAaPmNgFnS	osadit
jeho	jeho	k3xOp3gFnSc1	jeho
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bytě	byt	k1gInSc6	byt
pak	pak	k6eAd1	pak
žil	žít	k5eAaImAgMnS	žít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
práci	práce	k1gFnSc6	práce
(	(	kIx(	(
<g/>
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Žitné	žitný	k2eAgFnSc6d1	Žitná
ulici	ulice	k1gFnSc6	ulice
<g/>
)	)	kIx)	)
i	i	k9	i
v	v	k7c6	v
literární	literární	k2eAgFnSc6d1	literární
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
možnost	možnost	k1gFnSc4	možnost
styku	styk	k1gInSc2	styk
s	s	k7c7	s
uměleckým	umělecký	k2eAgInSc7d1	umělecký
i	i	k8xC	i
vědeckým	vědecký	k2eAgInSc7d1	vědecký
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Obnovil	obnovit	k5eAaPmAgMnS	obnovit
osobní	osobní	k2eAgInPc4d1	osobní
styky	styk	k1gInPc4	styk
s	s	k7c7	s
Mikolášem	Mikoláš	k1gMnSc7	Mikoláš
Alšem	Aleš	k1gMnSc7	Aleš
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
sdílel	sdílet	k5eAaImAgMnS	sdílet
obdobné	obdobný	k2eAgFnPc4d1	obdobná
umělecké	umělecký	k2eAgFnPc4d1	umělecká
představy	představa	k1gFnPc4	představa
a	a	k8xC	a
plány	plán	k1gInPc4	plán
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	se	k3xPyFc4	se
spisovateli	spisovatel	k1gMnSc3	spisovatel
lumírovského	lumírovský	k2eAgInSc2d1	lumírovský
kruhu	kruh	k1gInSc2	kruh
(	(	kIx(	(
<g/>
J.	J.	kA	J.
V.	V.	kA	V.
Sládkem	Sládek	k1gMnSc7	Sládek
<g/>
,	,	kIx,	,
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Vrchlickým	Vrchlický	k1gMnSc7	Vrchlický
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
Thomayerem	Thomayer	k1gMnSc7	Thomayer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navázal	navázat	k5eAaPmAgInS	navázat
přátelství	přátelství	k1gNnSc4	přátelství
se	s	k7c7	s
Zikmundem	Zikmund	k1gMnSc7	Zikmund
Wintrem	Winter	k1gMnSc7	Winter
a	a	k8xC	a
K.	K.	kA	K.
V.	V.	kA	V.
Raisem	Rais	k1gMnSc7	Rais
<g/>
,	,	kIx,	,
trvalé	trvalý	k2eAgInPc4d1	trvalý
kontakty	kontakt	k1gInPc4	kontakt
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
s	s	k7c7	s
mladší	mladý	k2eAgFnSc7d2	mladší
generací	generace	k1gFnSc7	generace
(	(	kIx(	(
<g/>
J.	J.	kA	J.
S.	S.	kA	S.
Macharem	Machar	k1gMnSc7	Machar
<g/>
,	,	kIx,	,
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Kvapilem	Kvapil	k1gMnSc7	Kvapil
a	a	k8xC	a
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Nejedlým	Nejedlý	k1gMnSc7	Nejedlý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vznikají	vznikat	k5eAaImIp3nP	vznikat
všechna	všechen	k3xTgNnPc4	všechen
jeho	jeho	k3xOp3gNnPc4	jeho
dramata	drama	k1gNnPc4	drama
i	i	k8xC	i
nejrozsáhlejší	rozsáhlý	k2eAgNnPc4d3	nejrozsáhlejší
románová	románový	k2eAgNnPc4d1	románové
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1890	[number]	k4	1890
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
řádným	řádný	k2eAgMnSc7d1	řádný
členem	člen	k1gMnSc7	člen
České	český	k2eAgFnSc2d1	Česká
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
mimořádným	mimořádný	k2eAgMnSc7d1	mimořádný
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
nauk	nauka	k1gFnPc2	nauka
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
penzi	penze	k1gFnSc4	penze
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
často	často	k6eAd1	často
zajížděl	zajíždět	k5eAaImAgMnS	zajíždět
do	do	k7c2	do
rodného	rodný	k2eAgInSc2d1	rodný
Hronova	Hronov	k1gInSc2	Hronov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
podnikal	podnikat	k5eAaImAgMnS	podnikat
studijní	studijní	k2eAgFnPc4d1	studijní
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
umisťoval	umisťovat	k5eAaImAgInS	umisťovat
děj	děj	k1gInSc1	děj
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
poprvé	poprvé	k6eAd1	poprvé
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Chodsko	Chodsko	k1gNnSc4	Chodsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
pak	pak	k6eAd1	pak
navštívil	navštívit	k5eAaPmAgMnS	navštívit
ještě	ještě	k6eAd1	ještě
několikrát	několikrát	k6eAd1	několikrát
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
podíval	podívat	k5eAaPmAgMnS	podívat
do	do	k7c2	do
Drážďan	Drážďany	k1gInPc2	Drážďany
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kostnice	Kostnice	k1gFnSc2	Kostnice
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
Bledu	Bled	k1gInSc2	Bled
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
charakterem	charakter	k1gInSc7	charakter
svého	svůj	k3xOyFgNnSc2	svůj
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
díla	dílo	k1gNnSc2	dílo
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
podepsal	podepsat	k5eAaPmAgMnS	podepsat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1917	[number]	k4	1917
Manifest	manifest	k1gInSc4	manifest
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
významné	významný	k2eAgNnSc4d1	významné
prohlášení	prohlášení	k1gNnSc4	prohlášení
podporující	podporující	k2eAgNnSc4d1	podporující
politické	politický	k2eAgNnSc4d1	politické
úsilí	úsilí	k1gNnSc4	úsilí
o	o	k7c4	o
státní	státní	k2eAgFnSc4d1	státní
samostatnost	samostatnost	k1gFnSc4	samostatnost
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Izidorem	Izidor	k1gMnSc7	Izidor
Zahradníkem	Zahradník	k1gMnSc7	Zahradník
(	(	kIx(	(
<g/>
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
ministrem	ministr	k1gMnSc7	ministr
železnic	železnice	k1gFnPc2	železnice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
čtení	čtení	k1gNnSc4	čtení
deklarace	deklarace	k1gFnSc2	deklarace
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
československé	československý	k2eAgFnSc2d1	Československá
samostatnosti	samostatnost	k1gFnSc2	samostatnost
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
v	v	k7c6	v
11	[number]	k4	11
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
pod	pod	k7c7	pod
sochou	socha	k1gFnSc7	socha
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
vítal	vítat	k5eAaImAgMnS	vítat
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
hodině	hodina	k1gFnSc6	hodina
odpolední	odpolední	k1gNnSc2	odpolední
projevem	projev	k1gInSc7	projev
prezidenta	prezident	k1gMnSc2	prezident
Masaryka	Masaryk	k1gMnSc2	Masaryk
na	na	k7c6	na
Wilsonově	Wilsonův	k2eAgNnSc6d1	Wilsonovo
nádraží	nádraží	k1gNnSc6	nádraží
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
triumfálním	triumfální	k2eAgInSc6d1	triumfální
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Masarykem	Masaryk	k1gMnSc7	Masaryk
často	často	k6eAd1	často
setkával	setkávat	k5eAaImAgMnS	setkávat
<g/>
.	.	kIx.	.
<g/>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
udělila	udělit	k5eAaPmAgFnS	udělit
Aloisu	Aloisu	k?	Aloisu
Jiráskovi	Jirásek	k1gMnSc3	Jirásek
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1919	[number]	k4	1919
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Resslově	Resslově	k1gFnSc6	Resslově
ulici	ulice	k1gFnSc6	ulice
neslo	nést	k5eAaImAgNnS	nést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
počest	počest	k1gFnSc6	počest
název	název	k1gInSc4	název
Jiráskovo	Jiráskův	k2eAgNnSc4d1	Jiráskovo
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
Československé	československý	k2eAgFnSc3d1	Československá
republice	republika	k1gFnSc3	republika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
poslancem	poslanec	k1gMnSc7	poslanec
Revolučního	revoluční	k2eAgNnSc2d1	revoluční
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
Republiky	republika	k1gFnSc2	republika
československé	československý	k2eAgFnSc2d1	Československá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
získal	získat	k5eAaPmAgMnS	získat
senátorské	senátorský	k2eAgNnSc4d1	senátorské
křeslo	křeslo	k1gNnSc4	křeslo
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Senátorem	senátor	k1gMnSc7	senátor
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
zasedal	zasedat	k5eAaImAgInS	zasedat
za	za	k7c4	za
výrazně	výrazně	k6eAd1	výrazně
pravicovou	pravicový	k2eAgFnSc4d1	pravicová
Československou	československý	k2eAgFnSc4d1	Československá
národní	národní	k2eAgFnSc4d1	národní
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
Československé	československý	k2eAgFnSc2d1	Československá
národní	národní	k2eAgFnSc2d1	národní
demokracie	demokracie	k1gFnSc2	demokracie
byl	být	k5eAaImAgInS	být
logický	logický	k2eAgInSc1d1	logický
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tato	tento	k3xDgFnSc1	tento
strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
pokračovatelkou	pokračovatelka	k1gFnSc7	pokračovatelka
strany	strana	k1gFnSc2	strana
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
přívržencem	přívrženec	k1gMnSc7	přívrženec
byl	být	k5eAaImAgMnS	být
Jirásek	Jirásek	k1gMnSc1	Jirásek
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
politice	politika	k1gFnSc6	politika
setrval	setrvat	k5eAaPmAgMnS	setrvat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1921	[number]	k4	1921
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Jirásek	Jirásek	k1gMnSc1	Jirásek
z	z	k7c2	z
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
;	;	kIx,	;
zůstal	zůstat	k5eAaPmAgMnS	zůstat
sice	sice	k8xC	sice
nadále	nadále	k6eAd1	nadále
věřícím	věřící	k2eAgMnSc7d1	věřící
křesťanem	křesťan	k1gMnSc7	křesťan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
žádné	žádný	k3yNgFnSc2	žádný
církve	církev	k1gFnSc2	církev
již	již	k6eAd1	již
nevstoupil	vstoupit	k5eNaPmAgMnS	vstoupit
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
a	a	k8xC	a
1930	[number]	k4	1930
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jirásek	Jirásek	k1gMnSc1	Jirásek
byl	být	k5eAaImAgMnS	být
nositelem	nositel	k1gMnSc7	nositel
Československé	československý	k2eAgFnSc2d1	Československá
revoluční	revoluční	k2eAgFnSc2d1	revoluční
medaile	medaile	k1gFnSc2	medaile
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1921	[number]	k4	1921
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
bulharský	bulharský	k2eAgInSc1d1	bulharský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Alexandra	Alexandr	k1gMnSc2	Alexandr
(	(	kIx(	(
<g/>
О	О	k?	О
С	С	k?	С
А	А	k?	А
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
jugoslávský	jugoslávský	k2eAgInSc1d1	jugoslávský
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Sávy	Sáva	k1gFnSc2	Sáva
<g/>
.	.	kIx.	.
<g/>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
12	[number]	k4	12
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Hronově	Hronov	k1gInSc6	Hronov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
15	[number]	k4	15
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
před	před	k7c7	před
Národním	národní	k2eAgNnSc7d1	národní
muzeem	muzeum	k1gNnSc7	muzeum
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
u	u	k7c2	u
sochy	socha	k1gFnSc2	socha
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
konala	konat	k5eAaImAgFnS	konat
panychida	panychida	k1gFnSc1	panychida
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
promluvili	promluvit	k5eAaPmAgMnP	promluvit
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
a	a	k8xC	a
František	František	k1gMnSc1	František
Soukup	Soukup	k1gMnSc1	Soukup
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
pohřeb	pohřeb	k1gInSc1	pohřeb
začal	začít	k5eAaPmAgInS	začít
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
v	v	k7c6	v
panteonu	panteon	k1gInSc6	panteon
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
</s>
</p>
<p>
<s>
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zakázala	zakázat	k5eAaPmAgFnS	zakázat
k	k	k7c3	k
uctění	uctění	k1gNnSc3	uctění
jeho	jeho	k3xOp3gFnSc2	jeho
památky	památka	k1gFnSc2	památka
zvonit	zvonit	k5eAaImF	zvonit
zvony	zvon	k1gInPc4	zvon
pražských	pražský	k2eAgInPc2d1	pražský
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
konat	konat	k5eAaImF	konat
pohřební	pohřební	k2eAgInPc4d1	pohřební
obřady	obřad	k1gInPc4	obřad
bez	bez	k7c2	bez
účasti	účast	k1gFnSc2	účast
kněze	kněz	k1gMnSc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbu	pohřeb	k1gInSc2	pohřeb
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
prezident	prezident	k1gMnSc1	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
(	(	kIx(	(
<g/>
několik	několik	k4yIc4	několik
katolických	katolický	k2eAgMnPc2d1	katolický
ministrů	ministr	k1gMnPc2	ministr
se	se	k3xPyFc4	se
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generalita	generalita	k1gFnSc1	generalita
<g/>
,	,	kIx,	,
rektoři	rektor	k1gMnPc1	rektor
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
diplomaté	diplomat	k1gMnPc1	diplomat
spřátelených	spřátelený	k2eAgInPc2d1	spřátelený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Čestnou	čestný	k2eAgFnSc4d1	čestná
stráž	stráž	k1gFnSc4	stráž
stáli	stát	k5eAaImAgMnP	stát
vojáci	voják	k1gMnPc1	voják
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Jiráskova	Jiráskův	k2eAgInSc2d1	Jiráskův
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
a	a	k8xC	a
sokolové	sokol	k1gMnPc1	sokol
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
rakví	rakev	k1gFnSc7	rakev
promluvili	promluvit	k5eAaPmAgMnP	promluvit
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
Ivan	Ivan	k1gMnSc1	Ivan
Dérer	Dérer	k1gMnSc1	Dérer
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvapil	Kvapil	k1gMnSc1	Kvapil
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Medek	Medek	k1gMnSc1	Medek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kremaci	kremace	k1gFnSc6	kremace
kolona	kolona	k1gFnSc1	kolona
aut	auto	k1gNnPc2	auto
vezla	vézt	k5eAaImAgFnS	vézt
urnu	urna	k1gFnSc4	urna
do	do	k7c2	do
Hronova	Hronov	k1gInSc2	Hronov
přes	přes	k7c4	přes
řadu	řada	k1gFnSc4	řada
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
též	též	k9	též
konala	konat	k5eAaImAgNnP	konat
smuteční	smuteční	k2eAgNnPc1d1	smuteční
shromáždění	shromáždění	k1gNnPc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
fáze	fáze	k1gFnSc1	fáze
pohřbu	pohřeb	k1gInSc2	pohřeb
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
19	[number]	k4	19
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1930	[number]	k4	1930
v	v	k7c6	v
Hronově	Hronov	k1gInSc6	Hronov
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
četných	četný	k2eAgInPc2d1	četný
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
korporací	korporace	k1gFnPc2	korporace
i	i	k8xC	i
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
zvonů	zvon	k1gInPc2	zvon
zněly	znět	k5eAaImAgFnP	znět
nad	nad	k7c7	nad
Hronovem	Hronov	k1gInSc7	Hronov
sborové	sborový	k2eAgFnSc2d1	sborová
skladby	skladba	k1gFnSc2	skladba
J.	J.	kA	J.
B.	B.	kA	B.
Foerstera	Foerster	k1gMnSc2	Foerster
na	na	k7c6	na
slova	slovo	k1gNnSc2	slovo
J.	J.	kA	J.
V.	V.	kA	V.
Sládka	Sládek	k1gMnSc2	Sládek
<g/>
:	:	kIx,	:
Z	z	k7c2	z
osudu	osud	k1gInSc2	osud
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
Polní	polní	k2eAgFnSc7d1	polní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
<g/>
Jirásek	Jirásek	k1gMnSc1	Jirásek
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
našim	náš	k3xOp1gMnPc3	náš
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
spisovatelům	spisovatel	k1gMnPc3	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Dovedl	dovést	k5eAaPmAgMnS	dovést
mistrovsky	mistrovsky	k6eAd1	mistrovsky
využít	využít	k5eAaPmF	využít
příznačných	příznačný	k2eAgInPc2d1	příznačný
historických	historický	k2eAgInPc2d1	historický
detailů	detail	k1gInPc2	detail
k	k	k7c3	k
podrobnému	podrobný	k2eAgNnSc3d1	podrobné
prokreslení	prokreslení	k1gNnSc3	prokreslení
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
postavy	postava	k1gFnPc1	postava
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
nositeli	nositel	k1gMnPc7	nositel
i	i	k8xC	i
tvůrci	tvůrce	k1gMnPc7	tvůrce
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Výrazným	výrazný	k2eAgInSc7d1	výrazný
znakem	znak	k1gInSc7	znak
Jiráskova	Jiráskův	k2eAgNnSc2d1	Jiráskovo
mistrovství	mistrovství	k1gNnSc2	mistrovství
je	být	k5eAaImIp3nS	být
soulad	soulad	k1gInSc4	soulad
mezi	mezi	k7c7	mezi
individuálním	individuální	k2eAgInSc7d1	individuální
a	a	k8xC	a
obecným	obecný	k2eAgInSc7d1	obecný
významem	význam	k1gInSc7	význam
jeho	jeho	k3xOp3gFnPc2	jeho
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Jiráskovi	Jiráskův	k2eAgMnPc1d1	Jiráskův
vždy	vždy	k6eAd1	vždy
šlo	jít	k5eAaImAgNnS	jít
především	především	k9	především
o	o	k7c4	o
obraz	obraz	k1gInSc4	obraz
společenského	společenský	k2eAgNnSc2d1	společenské
dění	dění	k1gNnSc2	dění
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jedinec	jedinec	k1gMnSc1	jedinec
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
volen	volno	k1gNnPc2	volno
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zároveň	zároveň	k6eAd1	zároveň
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
tendence	tendence	k1gFnSc1	tendence
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Literární	literární	k2eAgFnSc4d1	literární
dráhu	dráha	k1gFnSc4	dráha
Jirásek	Jirásek	k1gMnSc1	Jirásek
zahájil	zahájit	k5eAaPmAgMnS	zahájit
veršovanou	veršovaný	k2eAgFnSc7d1	veršovaná
tvorbou	tvorba	k1gFnSc7	tvorba
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
a	a	k8xC	a
vlasteneckém	vlastenecký	k2eAgMnSc6d1	vlastenecký
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
Svoje	svůj	k3xOyFgFnPc4	svůj
prózy	próza	k1gFnPc4	próza
pak	pak	k6eAd1	pak
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
na	na	k7c4	na
realitu	realita	k1gFnSc4	realita
soudobého	soudobý	k2eAgInSc2d1	soudobý
venkova	venkov	k1gInSc2	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
za	za	k7c2	za
časů	čas	k1gInPc2	čas
ruchovců	ruchovec	k1gMnPc2	ruchovec
<g/>
,	,	kIx,	,
literárně	literárně	k6eAd1	literárně
činný	činný	k2eAgMnSc1d1	činný
byl	být	k5eAaImAgInS	být
však	však	k9	však
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
prvním	první	k4xOgInSc7	první
větším	veliký	k2eAgInSc7d2	veliký
literárním	literární	k2eAgInSc7d1	literární
pokusem	pokus	k1gInSc7	pokus
byla	být	k5eAaImAgFnS	být
rozsáhlejší	rozsáhlý	k2eAgFnSc1d2	rozsáhlejší
historická	historický	k2eAgFnSc1d1	historická
povídka	povídka	k1gFnSc1	povídka
Skaláci	Skalák	k1gMnPc1	Skalák
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
dílem	díl	k1gInSc7	díl
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
román	román	k1gInSc1	román
Husitský	husitský	k2eAgMnSc1d1	husitský
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
let	léto	k1gNnPc2	léto
psal	psát	k5eAaImAgMnS	psát
povídky	povídka	k1gFnPc4	povídka
(	(	kIx(	(
<g/>
Povídka	povídka	k1gFnSc1	povídka
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
Z	z	k7c2	z
bouřlivých	bouřlivý	k2eAgFnPc2d1	bouřlivá
dob	doba	k1gFnPc2	doba
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
Rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
próza	próza	k1gFnSc1	próza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
prvotinou	prvotina	k1gFnSc7	prvotina
byla	být	k5eAaImAgFnS	být
povídka	povídka	k1gFnSc1	povídka
Poklad	poklad	k1gInSc1	poklad
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
Jiráskova	Jiráskův	k2eAgInSc2d1	Jiráskův
historického	historický	k2eAgInSc2d1	historický
záběru	záběr	k1gInSc2	záběr
je	být	k5eAaImIp3nS	být
obdivuhodně	obdivuhodně	k6eAd1	obdivuhodně
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
období	období	k1gNnSc4	období
bájné	bájný	k2eAgNnSc4d1	bájné
<g/>
,	,	kIx,	,
předdějinné	předdějinný	k2eAgNnSc4d1	předdějinný
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgFnSc6d1	Staré
pověsti	pověst	k1gFnSc6	pověst
české	český	k2eAgFnSc6d1	Česká
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
veršovanost	veršovanost	k1gFnSc4	veršovanost
úspěšně	úspěšně	k6eAd1	úspěšně
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
do	do	k7c2	do
četby	četba	k1gFnSc2	četba
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
šťastný	šťastný	k2eAgInSc4d1	šťastný
osud	osud	k1gInSc4	osud
měly	mít	k5eAaImAgFnP	mít
méně	málo	k6eAd2	málo
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
balady	balada	k1gFnPc4	balada
<g/>
,	,	kIx,	,
idyly	idyla	k1gFnPc4	idyla
a	a	k8xC	a
romance	romance	k1gFnPc4	romance
<g/>
,	,	kIx,	,
vyhledávané	vyhledávaný	k2eAgFnPc4d1	vyhledávaná
nakladateli	nakladatel	k1gMnPc7	nakladatel
<g/>
,	,	kIx,	,
ilustrátory	ilustrátor	k1gMnPc7	ilustrátor
a	a	k8xC	a
potom	potom	k6eAd1	potom
i	i	k9	i
filmovými	filmový	k2eAgMnPc7d1	filmový
tvůrci	tvůrce	k1gMnPc7	tvůrce
(	(	kIx(	(
<g/>
Filosofská	Filosofská	k?	Filosofská
historie	historie	k1gFnSc2	historie
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Maryla	Maryla	k1gFnSc1	Maryla
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
Zahořanský	Zahořanský	k2eAgInSc1d1	Zahořanský
hon	hon	k1gInSc1	hon
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Balada	balada	k1gFnSc1	balada
z	z	k7c2	z
rokoka	rokoko	k1gNnSc2	rokoko
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
cyklus	cyklus	k1gInSc1	cyklus
doby	doba	k1gFnSc2	doba
pobělohorské	pobělohorský	k2eAgFnSc2d1	pobělohorská
v	v	k7c6	v
jednosvazkových	jednosvazkový	k2eAgFnPc6d1	jednosvazková
příběhů	příběh	k1gInPc2	příběh
komponovaných	komponovaný	k2eAgInPc2d1	komponovaný
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
novel	novela	k1gFnPc2	novela
(	(	kIx(	(
<g/>
Skaláci	Skalák	k1gMnPc1	Skalák
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
Skály	skála	k1gFnSc2	skála
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
Poklad	poklad	k1gInSc1	poklad
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
Na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
vévodském	vévodský	k2eAgInSc6d1	vévodský
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
román	román	k1gInSc1	román
V	v	k7c6	v
cizích	cizí	k2eAgFnPc6d1	cizí
službách	služba	k1gFnPc6	služba
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
a	a	k8xC	a
povahopisný	povahopisný	k2eAgInSc4d1	povahopisný
obraz	obraz	k1gInSc4	obraz
Psohlavci	psohlavec	k1gMnPc1	psohlavec
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
časové	časový	k2eAgFnPc1d1	časová
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
,	,	kIx,	,
postavy	postava	k1gFnPc1	postava
epizodické	epizodický	k2eAgFnPc1d1	epizodická
utvářejí	utvářet	k5eAaImIp3nP	utvářet
místní	místní	k2eAgNnSc4d1	místní
prostředí	prostředí	k1gNnSc4	prostředí
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
všednodennosti	všednodennost	k1gFnSc6	všednodennost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
cyklus	cyklus	k1gInSc1	cyklus
obrazů	obraz	k1gInPc2	obraz
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
má	mít	k5eAaImIp3nS	mít
ráz	ráz	k1gInSc4	ráz
románových	románový	k2eAgFnPc2d1	románová
epopejí	epopej	k1gFnPc2	epopej
a	a	k8xC	a
kronik	kronika	k1gFnPc2	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Obsáhly	obsáhnout	k5eAaPmAgInP	obsáhnout
proměny	proměna	k1gFnPc4	proměna
Čech	Čechy	k1gFnPc2	Čechy
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
husitství	husitství	k1gNnSc2	husitství
po	po	k7c4	po
jeho	jeho	k3xOp3gInSc4	jeho
konec	konec	k1gInSc4	konec
(	(	kIx(	(
<g/>
v	v	k7c6	v
rozsáhlých	rozsáhlý	k2eAgInPc6d1	rozsáhlý
románech	román	k1gInPc6	román
Mezi	mezi	k7c7	mezi
proudy	proud	k1gInPc7	proud
I	i	k8xC	i
<g/>
–	–	k?	–
<g/>
III	III	kA	III
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Proti	proti	k7c3	proti
všem	všecek	k3xTgFnPc3	všecek
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
I	i	k8xC	i
<g/>
–	–	k?	–
<g/>
III	III	kA	III
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
rekatolizace	rekatolizace	k1gFnSc2	rekatolizace
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Temno	temno	k1gNnSc4	temno
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzestup	vzestup	k1gInSc4	vzestup
Čech	Čechy	k1gFnPc2	Čechy
od	od	k7c2	od
sklonku	sklonek	k1gInSc2	sklonek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c4	po
1848	[number]	k4	1848
(	(	kIx(	(
<g/>
F.	F.	kA	F.
L.	L.	kA	L.
Věk	věk	k1gInSc1	věk
I	i	k8xC	i
<g/>
–	–	k?	–
<g/>
V	v	k7c6	v
1888	[number]	k4	1888
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
I	i	k9	i
<g/>
–	–	k?	–
<g/>
IV	IV	kA	IV
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Husitský	husitský	k2eAgMnSc1d1	husitský
král	král	k1gMnSc1	král
I	I	kA	I
<g/>
–	–	k?	–
<g/>
II	II	kA	II
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
o	o	k7c4	o
panování	panování	k1gNnSc4	panování
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
zůstal	zůstat	k5eAaPmAgInS	zůstat
nedokončen	dokončit	k5eNaPmNgInS	dokončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
divadelní	divadelní	k2eAgFnPc1d1	divadelní
hry	hra	k1gFnPc1	hra
ze	z	k7c2	z
současné	současný	k2eAgFnSc2d1	současná
vesnice	vesnice	k1gFnSc2	vesnice
přispívaly	přispívat	k5eAaImAgFnP	přispívat
k	k	k7c3	k
prosazování	prosazování	k1gNnSc3	prosazování
realismu	realismus	k1gInSc2	realismus
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
(	(	kIx(	(
<g/>
Vojnarka	Vojnarka	k1gFnSc1	Vojnarka
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Otec	otec	k1gMnSc1	otec
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
historické	historický	k2eAgFnPc1d1	historická
(	(	kIx(	(
<g/>
Emigrant	emigrant	k1gMnSc1	emigrant
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
trilogie	trilogie	k1gFnSc1	trilogie
tří	tři	k4xCgMnPc2	tři
Janů	Jan	k1gMnPc2	Jan
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Roháč	roháč	k1gMnSc1	roháč
1914	[number]	k4	1914
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trvalou	trvalý	k2eAgFnSc7d1	trvalá
součástí	součást	k1gFnSc7	součást
repertoáru	repertoár	k1gInSc2	repertoár
domácích	domácí	k1gMnPc2	domácí
divadel	divadlo	k1gNnPc2	divadlo
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pohádkově	pohádkově	k6eAd1	pohádkově
laděná	laděný	k2eAgFnSc1d1	laděná
hra	hra	k1gFnSc1	hra
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jirásek	Jirásek	k1gMnSc1	Jirásek
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
redigování	redigování	k1gNnSc2	redigování
časopisu	časopis	k1gInSc2	časopis
Zvon	zvon	k1gInSc1	zvon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Próza	próza	k1gFnSc1	próza
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Nejstarší	starý	k2eAgNnPc1d3	nejstarší
období	období	k1gNnPc1	období
====	====	k?	====
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
pověsti	pověst	k1gFnPc1	pověst
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
–	–	k?	–
soubor	soubor	k1gInSc1	soubor
pověstí	pověst	k1gFnPc2	pověst
od	od	k7c2	od
praotce	praotec	k1gMnSc2	praotec
Čecha	Čech	k1gMnSc2	Čech
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
dobu	doba	k1gFnSc4	doba
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
husitství	husitství	k1gNnSc4	husitství
až	až	k9	až
do	do	k7c2	do
období	období	k1gNnSc2	období
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Husitství	husitství	k1gNnSc2	husitství
====	====	k?	====
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
proudy	proud	k1gInPc7	proud
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
–	–	k?	–
líčí	líčit	k5eAaImIp3nP	líčit
počátky	počátek	k1gInPc1	počátek
husitského	husitský	k2eAgNnSc2d1	husitské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
spory	spor	k1gInPc1	spor
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	s	k7c7	s
šlechtou	šlechta	k1gFnSc7	šlechta
a	a	k8xC	a
duchovenstvem	duchovenstvo	k1gNnSc7	duchovenstvo
<g/>
,	,	kIx,	,
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
zde	zde	k6eAd1	zde
historické	historický	k2eAgFnPc1d1	historická
postavy	postava	k1gFnPc1	postava
–	–	k?	–
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
všem	všecek	k3xTgInPc3	všecek
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
–	–	k?	–
líčí	líčit	k5eAaImIp3nS	líčit
vrchol	vrchol	k1gInSc1	vrchol
husitského	husitský	k2eAgNnSc2d1	husitské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
Tábora	Tábor	k1gInSc2	Tábor
<g/>
,	,	kIx,	,
Zikmundovo	Zikmundův	k2eAgNnSc1d1	Zikmundovo
tažení	tažení	k1gNnSc1	tažení
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
bitvu	bitva	k1gFnSc4	bitva
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
–	–	k?	–
třídílný	třídílný	k2eAgInSc4d1	třídílný
román	román	k1gInSc4	román
o	o	k7c6	o
úpadku	úpadek	k1gInSc6	úpadek
husitství	husitství	k1gNnSc2	husitství
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
a	a	k8xC	a
odchod	odchod	k1gInSc1	odchod
husitských	husitský	k2eAgNnPc2d1	husitské
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Jiskry	jiskra	k1gFnSc2	jiskra
z	z	k7c2	z
Brandýsa	Brandýs	k1gInSc2	Brandýs
</s>
</p>
<p>
<s>
Husitský	husitský	k2eAgMnSc1d1	husitský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
ho	on	k3xPp3gNnSc4	on
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jako	jako	k8xS	jako
silného	silný	k2eAgMnSc4d1	silný
panovníka	panovník	k1gMnSc4	panovník
<g/>
;	;	kIx,	;
Jirásek	Jirásek	k1gMnSc1	Jirásek
román	román	k1gInSc4	román
pro	pro	k7c4	pro
nemoc	nemoc	k1gFnSc4	nemoc
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
,	,	kIx,	,
vyšly	vyjít	k5eAaPmAgInP	vyjít
jen	jen	k6eAd1	jen
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
díly	díl	k1gInPc4	díl
<g/>
,	,	kIx,	,
děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
doveden	dovést	k5eAaPmNgInS	dovést
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1464	[number]	k4	1464
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
až	až	k9	až
na	na	k7c4	na
konec	konec	k1gInSc4	konec
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
–	–	k?	–
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
mise	mise	k1gFnSc1	mise
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
podle	podle	k7c2	podle
cestopisu	cestopis	k1gInSc2	cestopis
Václava	Václav	k1gMnSc2	Václav
Šaška	Šašek	k1gMnSc2	Šašek
z	z	k7c2	z
Bířkova	Bířkov	k1gInSc2	Bířkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slavný	slavný	k2eAgInSc1d1	slavný
den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
letech	let	k1gInPc6	let
1419	[number]	k4	1419
<g/>
–	–	k?	–
<g/>
1420	[number]	k4	1420
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
období	období	k1gNnSc1	období
od	od	k7c2	od
bouří	bouř	k1gFnPc2	bouř
pražské	pražský	k2eAgFnSc2d1	Pražská
chudiny	chudina	k1gFnSc2	chudina
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc4	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
až	až	k9	až
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
hyne	hynout	k5eAaImIp3nS	hynout
hlavní	hlavní	k2eAgFnSc1d1	hlavní
hrdinka	hrdinka	k1gFnSc1	hrdinka
tohoto	tento	k3xDgInSc2	tento
romantického	romantický	k2eAgInSc2d1	romantický
příběhu	příběh	k1gInSc2	příběh
</s>
</p>
<p>
<s>
Konec	konec	k1gInSc1	konec
a	a	k8xC	a
počátek	počátek	k1gInSc1	počátek
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
letech	let	k1gInPc6	let
1452	[number]	k4	1452
<g/>
–	–	k?	–
<g/>
1453	[number]	k4	1453
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Litice	Litice	k1gFnSc2	Litice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vězněn	vězněn	k2eAgMnSc1d1	vězněn
Václav	Václav	k1gMnSc1	Václav
Koranda	Korando	k1gNnSc2	Korando
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
a	a	k8xC	a
líčí	líčit	k5eAaImIp3nS	líčit
zánik	zánik	k1gInSc1	zánik
táborství	táborství	k1gNnSc2	táborství
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
Jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
</s>
</p>
<p>
<s>
Zemanka	zemanka	k1gFnSc1	zemanka
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
–	–	k?	–
elegická	elegický	k2eAgFnSc1d1	elegická
povídka	povídka	k1gFnSc1	povídka
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
s	s	k7c7	s
osudovou	osudový	k2eAgFnSc7d1	osudová
láskou	láska	k1gFnSc7	láska
paní	paní	k1gFnSc2	paní
Elišky	Eliška	k1gFnSc2	Eliška
k	k	k7c3	k
Laurinovi	Laurin	k1gMnSc3	Laurin
z	z	k7c2	z
adamitské	adamitský	k2eAgFnSc2d1	adamitský
sekty	sekta	k1gFnSc2	sekta
</s>
</p>
<p>
<s>
V	v	k7c6	v
cizích	cizí	k2eAgFnPc6d1	cizí
službách	služba	k1gFnPc6	služba
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Vladislava	Vladislav	k1gMnSc2	Vladislav
Jagellonského	jagellonský	k2eAgMnSc2d1	jagellonský
a	a	k8xC	a
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
tragický	tragický	k2eAgInSc4d1	tragický
osud	osud	k1gInSc4	osud
potomků	potomek	k1gMnPc2	potomek
táboritů	táborita	k1gMnPc2	táborita
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jako	jako	k9	jako
žoldnéři	žoldnér	k1gMnPc1	žoldnér
prolévají	prolévat	k5eAaImIp3nP	prolévat
krev	krev	k1gFnSc4	krev
za	za	k7c4	za
cizí	cizí	k2eAgInPc4d1	cizí
zájmy	zájem	k1gInPc4	zájem
</s>
</p>
<p>
<s>
Maryla	Maryla	k1gFnSc1	Maryla
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
–	–	k?	–
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
milostný	milostný	k2eAgInSc1d1	milostný
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
poděbradské	poděbradský	k2eAgFnSc2d1	Poděbradská
</s>
</p>
<p>
<s>
====	====	k?	====
Doba	doba	k1gFnSc1	doba
pobělohorská	pobělohorský	k2eAgFnSc1d1	pobělohorská
====	====	k?	====
</s>
</p>
<p>
<s>
Temno	temno	k1gNnSc1	temno
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
–	–	k?	–
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
doba	doba	k1gFnSc1	doba
největšího	veliký	k2eAgInSc2d3	veliký
útlaku	útlak	k1gInSc2	útlak
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Duchovní	duchovní	k2eAgInSc4d1	duchovní
život	život	k1gInSc4	život
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
–	–	k?	–
jezuitský	jezuitský	k2eAgInSc1d1	jezuitský
řád	řád	k1gInSc1	řád
<g/>
;	;	kIx,	;
nekatolíci	nekatolík	k1gMnPc1	nekatolík
se	se	k3xPyFc4	se
tajně	tajně	k6eAd1	tajně
scházejí	scházet	k5eAaImIp3nP	scházet
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
bible	bible	k1gFnSc2	bible
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
jsou	být	k5eAaImIp3nP	být
pronásledováni	pronásledován	k2eAgMnPc1d1	pronásledován
jezuity	jezuita	k1gMnSc2	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
dějová	dějový	k2eAgFnSc1d1	dějová
linie	linie	k1gFnSc1	linie
sleduje	sledovat	k5eAaImIp3nS	sledovat
osudy	osud	k1gInPc4	osud
příslušníků	příslušník	k1gMnPc2	příslušník
rodiny	rodina	k1gFnSc2	rodina
myslivce	myslivec	k1gMnSc2	myslivec
Tomáše	Tomáš	k1gMnSc4	Tomáš
Machovce	Machovec	k1gMnSc4	Machovec
<g/>
,	,	kIx,	,
postižené	postižený	k2eAgMnPc4d1	postižený
a	a	k8xC	a
rozbité	rozbitý	k2eAgMnPc4d1	rozbitý
vizitací	vizitace	k1gFnPc2	vizitace
jezuitských	jezuitský	k2eAgMnPc2d1	jezuitský
páterů	páter	k1gMnPc2	páter
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Temno	temno	k1gNnSc1	temno
stalo	stát	k5eAaPmAgNnS	stát
nejoblíbenější	oblíbený	k2eAgFnSc7d3	nejoblíbenější
českou	český	k2eAgFnSc7d1	Česká
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
připomínalo	připomínat	k5eAaImAgNnS	připomínat
pomíjivost	pomíjivost	k1gFnSc4	pomíjivost
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c4	na
násilí	násilí	k1gNnSc4	násilí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Psohlavci	psohlavec	k1gMnPc1	psohlavec
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
Chodové	Chod	k1gMnPc1	Chod
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Koziny	kozina	k1gFnSc2	kozina
usilují	usilovat	k5eAaImIp3nP	usilovat
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
chodských	chodský	k2eAgFnPc2d1	Chodská
výsad	výsada	k1gFnPc2	výsada
<g/>
,	,	kIx,	,
odboj	odboj	k1gInSc4	odboj
je	být	k5eAaImIp3nS	být
potlačen	potlačen	k2eAgMnSc1d1	potlačen
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Sladký	Sladký	k1gMnSc1	Sladký
Kozina	kozina	k1gFnSc1	kozina
popraven	popravit	k5eAaPmNgInS	popravit
<g/>
,	,	kIx,	,
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
"	"	kIx"	"
<g/>
Lomikara	Lomikar	k1gMnSc4	Lomikar
<g/>
"	"	kIx"	"
na	na	k7c4	na
Boží	boží	k2eAgInSc4d1	boží
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skaláci	Skalák	k1gMnPc1	Skalák
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
–	–	k?	–
o	o	k7c6	o
selské	selský	k2eAgFnSc6d1	selská
vzpouře	vzpoura	k1gFnSc6	vzpoura
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
na	na	k7c6	na
Náchodsku	Náchodsek	k1gInSc6	Náchodsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skály	skála	k1gFnPc1	skála
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
–	–	k?	–
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
desetiletí	desetiletí	k1gNnSc2	desetiletí
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Líčí	líčit	k5eAaImIp3nS	líčit
osudy	osud	k1gInPc4	osud
evangelické	evangelický	k2eAgFnSc2d1	evangelická
rodiny	rodina	k1gFnSc2	rodina
Křineckých	Křinecký	k2eAgInPc2d1	Křinecký
z	z	k7c2	z
Ronova	Ronův	k2eAgMnSc2d1	Ronův
<g/>
,	,	kIx,	,
vystěhované	vystěhovaný	k2eAgNnSc1d1	vystěhované
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Adršpachu	Adršpach	k1gInSc2	Adršpach
<g/>
,	,	kIx,	,
prolíná	prolínat	k5eAaImIp3nS	prolínat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
i	i	k9	i
příběh	příběh	k1gInSc1	příběh
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Voborského	Voborský	k2eAgMnSc2d1	Voborský
<g/>
,	,	kIx,	,
potomka	potomek	k1gMnSc2	potomek
roku	rok	k1gInSc2	rok
1621	[number]	k4	1621
popraveného	popravený	k2eAgInSc2d1	popravený
V.	V.	kA	V.
Kochana	kochan	k1gMnSc4	kochan
z	z	k7c2	z
Prachové	prachový	k2eAgFnSc2d1	prachová
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
útisk	útisk	k1gInSc1	útisk
poddaného	poddaný	k2eAgInSc2d1	poddaný
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyvrcholí	vyvrcholit	k5eAaPmIp3nS	vyvrcholit
neúspěšným	úspěšný	k2eNgNnSc7d1	neúspěšné
povstáním	povstání	k1gNnSc7	povstání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
vedeno	vést	k5eAaImNgNnS	vést
čáslavským	čáslavský	k2eAgMnSc7d1	čáslavský
knězem	kněz	k1gMnSc7	kněz
Matyášem	Matyáš	k1gMnSc7	Matyáš
(	(	kIx(	(
<g/>
Matoušem	Matouš	k1gMnSc7	Matouš
<g/>
)	)	kIx)	)
Ulickým	Ulický	k2eAgFnPc3d1	Ulická
<g/>
,	,	kIx,	,
exkomunikovaným	exkomunikovaný	k2eAgFnPc3d1	exkomunikovaná
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
v	v	k7c6	v
Čáslavi	Čáslav	k1gFnSc6	Čáslav
popraveným	popravený	k2eAgMnSc7d1	popravený
na	na	k7c6	na
šibenici	šibenice	k1gFnSc6	šibenice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
vévodském	vévodský	k2eAgInSc6d1	vévodský
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
líčí	líčit	k5eAaImIp3nS	líčit
osvícenskou	osvícenský	k2eAgFnSc4d1	osvícenská
atmosféru	atmosféra	k1gFnSc4	atmosféra
na	na	k7c6	na
náchodském	náchodský	k2eAgInSc6d1	náchodský
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
snahy	snaha	k1gFnSc2	snaha
vlastenců	vlastenec	k1gMnPc2	vlastenec
o	o	k7c6	o
zrušení	zrušení	k1gNnSc6	zrušení
roboty	robota	k1gFnSc2	robota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ráj	ráj	k1gInSc1	ráj
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
časově	časově	k6eAd1	časově
i	i	k9	i
některými	některý	k3yIgFnPc7	některý
postavami	postava	k1gFnPc7	postava
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
román	román	k1gInSc4	román
Na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
vévodském	vévodský	k2eAgInSc6d1	vévodský
<g/>
;	;	kIx,	;
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
podrobnou	podrobný	k2eAgFnSc4d1	podrobná
malbu	malba	k1gFnSc4	malba
rozkošnického	rozkošnický	k2eAgInSc2d1	rozkošnický
života	život	k1gInSc2	život
na	na	k7c6	na
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongresu	kongres	k1gInSc6	kongres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Obrození	obrození	k1gNnSc2	obrození
====	====	k?	====
</s>
</p>
<p>
<s>
F.	F.	kA	F.
L.	L.	kA	L.
Věk	věk	k1gInSc1	věk
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
–	–	k?	–
pětidílný	pětidílný	k2eAgInSc4d1	pětidílný
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
František	František	k1gMnSc1	František
Ladislav	Ladislav	k1gMnSc1	Ladislav
Věk	věk	k1gInSc1	věk
–	–	k?	–
kupec	kupec	k1gMnSc1	kupec
z	z	k7c2	z
Dobrušky	Dobruška	k1gFnSc2	Dobruška
<g/>
,	,	kIx,	,
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
;	;	kIx,	;
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
probíhal	probíhat	k5eAaImAgInS	probíhat
proces	proces	k1gInSc4	proces
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
proud	proud	k1gInSc1	proud
intelektuální	intelektuální	k2eAgInSc1d1	intelektuální
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Dobrušce	Dobruška	k1gFnSc6	Dobruška
(	(	kIx(	(
<g/>
proud	proud	k1gInSc1	proud
lidový	lidový	k2eAgInSc1d1	lidový
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
historické	historický	k2eAgFnPc4d1	historická
postavy	postava	k1gFnPc4	postava
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Thám	Thám	k1gMnSc1	Thám
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Matěj	Matěj	k1gMnSc1	Matěj
Kramerius	Kramerius	k1gMnSc1	Kramerius
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
;	;	kIx,	;
F.	F.	kA	F.
L.	L.	kA	L.
Věk	věk	k1gInSc1	věk
je	být	k5eAaImIp3nS	být
líčen	líčit	k5eAaImNgInS	líčit
podle	podle	k7c2	podle
skutečné	skutečný	k2eAgFnSc2d1	skutečná
postavy	postava	k1gFnSc2	postava
kupce	kupec	k1gMnSc2	kupec
Františka	František	k1gMnSc2	František
Vladislava	Vladislav	k1gMnSc2	Vladislav
Heka	Hekus	k1gMnSc2	Hekus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
–	–	k?	–
čtyřdílná	čtyřdílný	k2eAgFnSc1d1	čtyřdílná
kronika	kronika	k1gFnSc1	kronika
(	(	kIx(	(
<g/>
Úhor	úhor	k1gInSc1	úhor
<g/>
,	,	kIx,	,
Novina	novina	k1gFnSc1	novina
<g/>
,	,	kIx,	,
Osetek	osetek	k1gInSc1	osetek
<g/>
,	,	kIx,	,
Zeměžluč	zeměžluč	k1gFnSc1	zeměžluč
<g/>
)	)	kIx)	)
odehrávající	odehrávající	k2eAgMnSc1d1	odehrávající
se	se	k3xPyFc4	se
na	na	k7c6	na
Náchodsku	Náchodsko	k1gNnSc6	Náchodsko
a	a	k8xC	a
Hronovsku	Hronovsko	k1gNnSc6	Hronovsko
(	(	kIx(	(
<g/>
Padolí	Padolí	k1gNnSc1	Padolí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
kněz	kněz	k1gMnSc1	kněz
–	–	k?	–
buditel	buditel	k1gMnSc1	buditel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filosofská	Filosofská	k?	Filosofská
historie	historie	k1gFnSc1	historie
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
–	–	k?	–
o	o	k7c6	o
životě	život	k1gInSc6	život
studentů	student	k1gMnPc2	student
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
a	a	k8xC	a
o	o	k7c6	o
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Historická	historický	k2eAgNnPc1d1	historické
dramata	drama	k1gNnPc1	drama
====	====	k?	====
</s>
</p>
<p>
<s>
Tzv.	tzv.	kA	tzv.
trilogie	trilogie	k1gFnSc1	trilogie
tří	tři	k4xCgMnPc2	tři
Janů	Jan	k1gMnPc2	Jan
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Roháč	roháč	k1gMnSc1	roháč
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
Další	další	k2eAgFnPc1d1	další
historické	historický	k2eAgFnPc1d1	historická
hry	hra	k1gFnPc1	hra
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kolébka	kolébka	k1gFnSc1	kolébka
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
–	–	k?	–
veselohra	veselohra	k1gFnSc1	veselohra
<g/>
,	,	kIx,	,
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Emigrant	emigrant	k1gMnSc1	emigrant
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
–	–	k?	–
drama	drama	k1gNnSc1	drama
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1741	[number]	k4	1741
v	v	k7c6	v
Polici	police	k1gFnSc6	police
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
,	,	kIx,	,
když	když	k8xS	když
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
vpadla	vpadnout	k5eAaPmAgFnS	vpadnout
vojska	vojsko	k1gNnSc2	vojsko
pruského	pruský	k2eAgMnSc2d1	pruský
krále	král	k1gMnSc2	král
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
M.	M.	kA	M.
D.	D.	kA	D.
Rettigová	Rettigový	k2eAgFnSc1d1	Rettigová
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
–	–	k?	–
veselohra	veselohra	k1gFnSc1	veselohra
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
památce	památka	k1gFnSc3	památka
vlastenecké	vlastenecký	k2eAgFnSc2d1	vlastenecká
autorky	autorka	k1gFnSc2	autorka
a	a	k8xC	a
kuchařky	kuchařka	k1gFnSc2	kuchařka
</s>
</p>
<p>
<s>
Gero	Gero	k1gMnSc1	Gero
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
–	–	k?	–
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
,	,	kIx,	,
hledá	hledat	k5eAaImIp3nS	hledat
příčiny	příčina	k1gFnPc4	příčina
záhuby	záhuba	k1gFnSc2	záhuba
polabských	polabský	k2eAgInPc2d1	polabský
Slovanů	Slovan	k1gInPc2	Slovan
</s>
</p>
<p>
<s>
====	====	k?	====
Ostatní	ostatní	k2eAgFnSc2d1	ostatní
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
====	====	k?	====
</s>
</p>
<p>
<s>
Lucerna	lucerna	k1gFnSc1	lucerna
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
–	–	k?	–
pohádkové	pohádkový	k2eAgNnSc4d1	pohádkové
drama	drama	k1gNnSc4	drama
spojující	spojující	k2eAgInSc1d1	spojující
svět	svět	k1gInSc1	svět
reálných	reálný	k2eAgFnPc2d1	reálná
a	a	k8xC	a
pohádkových	pohádkový	k2eAgFnPc2d1	pohádková
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vojnarka	Vojnarka	k1gFnSc1	Vojnarka
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
–	–	k?	–
tragédie	tragédie	k1gFnSc2	tragédie
staré	starý	k2eAgFnSc2d1	stará
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
nenaplněna	naplnit	k5eNaPmNgFnS	naplnit
</s>
</p>
<p>
<s>
Otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
–	–	k?	–
venkovské	venkovský	k2eAgNnSc4d1	venkovské
drama	drama	k1gNnSc4	drama
o	o	k7c4	o
lidské	lidský	k2eAgFnPc4d1	lidská
závisti	závist	k1gFnPc4	závist
a	a	k8xC	a
chamtivosti	chamtivost	k1gFnPc4	chamtivost
</s>
</p>
<p>
<s>
Samota	samota	k1gFnSc1	samota
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
–	–	k?	–
hra	hra	k1gFnSc1	hra
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
jednáních	jednání	k1gNnPc6	jednání
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
nudící	nudící	k2eAgFnSc2d1	nudící
se	se	k3xPyFc4	se
paničky	panička	k1gFnSc2	panička
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tráví	trávit	k5eAaImIp3nS	trávit
letní	letní	k2eAgInSc4d1	letní
čas	čas	k1gInSc4	čas
na	na	k7c6	na
horské	horský	k2eAgFnSc6d1	horská
samotě	samota	k1gFnSc6	samota
a	a	k8xC	a
prožívá	prožívat	k5eAaImIp3nS	prožívat
milostné	milostný	k2eAgNnSc1d1	milostné
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
<g/>
;	;	kIx,	;
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zkouška	zkouška	k1gFnSc1	zkouška
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
–	–	k?	–
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
z	z	k7c2	z
maloměstského	maloměstský	k2eAgNnSc2d1	maloměstské
prostředí	prostředí	k1gNnSc2	prostředí
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
Johanes	Johanes	k1gMnSc1	Johanes
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
–	–	k?	–
pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
jednáních	jednání	k1gNnPc6	jednání
<g/>
;	;	kIx,	;
proti	proti	k7c3	proti
cizí	cizí	k2eAgFnSc3d1	cizí
moci	moc	k1gFnSc3	moc
pana	pan	k1gMnSc2	pan
Johanesa	Johanes	k1gMnSc2	Johanes
(	(	kIx(	(
<g/>
Rýbrcoula	Rýbrcoul	k1gMnSc2	Rýbrcoul
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
brání	bránit	k5eAaImIp3nS	bránit
česká	český	k2eAgFnSc1d1	Česká
princezna	princezna	k1gFnSc1	princezna
Kačenka	Kačenka	k1gFnSc1	Kačenka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kolem	kolem	k7c2	kolem
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
chápáno	chápán	k2eAgNnSc4d1	chápáno
jako	jako	k8xS	jako
alegorie	alegorie	k1gFnSc2	alegorie
boje	boj	k1gInSc2	boj
za	za	k7c4	za
státní	státní	k2eAgFnSc4d1	státní
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
;	;	kIx,	;
dle	dle	k7c2	dle
hry	hra	k1gFnSc2	hra
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
Ivo	Ivo	k1gMnSc1	Ivo
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
libreto	libreto	k1gNnSc4	libreto
napsal	napsat	k5eAaPmAgMnS	napsat
Jiří	Jiří	k1gMnSc1	Jiří
Šrámek	Šrámek	k1gMnSc1	Šrámek
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vydávání	vydávání	k1gNnSc1	vydávání
Jiráskových	Jiráskových	k2eAgNnPc2d1	Jiráskových
děl	dělo	k1gNnPc2	dělo
===	===	k?	===
</s>
</p>
<p>
<s>
Jiráskova	Jiráskův	k2eAgFnSc1d1	Jiráskova
tvorba	tvorba	k1gFnSc1	tvorba
se	se	k3xPyFc4	se
těšila	těšit	k5eAaImAgFnS	těšit
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
značné	značný	k2eAgFnSc2d1	značná
oblibě	obliba	k1gFnSc3	obliba
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
prací	práce	k1gFnPc2	práce
byla	být	k5eAaImAgFnS	být
vydávána	vydávat	k5eAaImNgFnS	vydávat
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
soubornému	souborný	k2eAgNnSc3d1	souborné
vydání	vydání	k1gNnSc3	vydání
Sebraných	sebraný	k2eAgInPc2d1	sebraný
spisů	spis	k1gInPc2	spis
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
přikročilo	přikročit	k5eAaPmAgNnS	přikročit
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
v	v	k7c6	v
letech	let	k1gInPc6	let
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
(	(	kIx(	(
<g/>
45	[number]	k4	45
svazků	svazek	k1gInPc2	svazek
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	let	k1gInPc6	let
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
témže	týž	k3xTgNnSc6	týž
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
(	(	kIx(	(
<g/>
47	[number]	k4	47
svazků	svazek	k1gInPc2	svazek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k9	ani
toto	tento	k3xDgNnSc1	tento
druhé	druhý	k4xOgNnSc1	druhý
vydání	vydání	k1gNnSc1	vydání
však	však	k9	však
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
celou	celý	k2eAgFnSc4d1	celá
Jiráskovu	Jiráskův	k2eAgFnSc4d1	Jiráskova
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
např.	např.	kA	např.
raný	raný	k2eAgInSc1d1	raný
Jiráskův	Jiráskův	k2eAgInSc1d1	Jiráskův
román	román	k1gInSc1	román
Slavný	slavný	k2eAgInSc1d1	slavný
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
vydání	vydání	k1gNnSc1	vydání
Jiráskových	Jiráskových	k2eAgInPc2d1	Jiráskových
spisů	spis	k1gInPc2	spis
vycházelo	vycházet	k5eAaImAgNnS	vycházet
v	v	k7c6	v
letech	let	k1gInPc6	let
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
označen	označit	k5eAaPmNgMnS	označit
"	"	kIx"	"
<g/>
za	za	k7c2	za
nejnebezpečnějšího	bezpečný	k2eNgMnSc2d3	nejnebezpečnější
šiřitele	šiřitel	k1gMnSc2	šiřitel
lživých	lživý	k2eAgFnPc2d1	lživá
interpretací	interpretace	k1gFnPc2	interpretace
našich	náš	k3xOp1gFnPc2	náš
dějin	dějiny	k1gFnPc2	dějiny
<g/>
"	"	kIx"	"
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
dala	dát	k5eAaPmAgFnS	dát
diskrétní	diskrétní	k2eAgInSc4d1	diskrétní
pokyn	pokyn	k1gInSc4	pokyn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
zmizela	zmizet	k5eAaPmAgNnP	zmizet
z	z	k7c2	z
knihkupeckých	knihkupecký	k2eAgInPc2d1	knihkupecký
výkladů	výklad	k1gInPc2	výklad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oficiálnímu	oficiální	k2eAgInSc3d1	oficiální
zákazu	zákaz	k1gInSc3	zákaz
jeho	jeho	k3xOp3gInPc2	jeho
spisů	spis	k1gInPc2	spis
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
některá	některý	k3yIgNnPc1	některý
díla	dílo	k1gNnPc1	dílo
byla	být	k5eAaImAgNnP	být
vydána	vydat	k5eAaPmNgNnP	vydat
i	i	k9	i
za	za	k7c4	za
okupace	okupace	k1gFnPc4	okupace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
obsáhlý	obsáhlý	k2eAgInSc1d1	obsáhlý
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
Jiráskových	Jiráskových	k2eAgInPc2d1	Jiráskových
spisů	spis	k1gInPc2	spis
(	(	kIx(	(
<g/>
edice	edice	k1gFnSc2	edice
"	"	kIx"	"
<g/>
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
odkaz	odkaz	k1gInSc1	odkaz
národu	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
32	[number]	k4	32
svazků	svazek	k1gInPc2	svazek
s	s	k7c7	s
doslovy	doslov	k1gInPc7	doslov
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Nejedlého	Nejedlý	k1gMnSc2	Nejedlý
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Jiráskovy	Jiráskův	k2eAgFnPc1d1	Jiráskova
práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
propagovány	propagován	k2eAgFnPc1d1	propagována
a	a	k8xC	a
často	často	k6eAd1	často
vydávány	vydáván	k2eAgFnPc1d1	vydávána
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Václav	Václav	k1gMnSc1	Václav
Černý	Černý	k1gMnSc1	Černý
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
kladný	kladný	k2eAgInSc4d1	kladný
počin	počin	k1gInSc4	počin
<g/>
,	,	kIx,	,
vzešlý	vzešlý	k2eAgInSc4d1	vzešlý
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
[	[	kIx(	[
<g/>
Nejedlého	Nejedlého	k2eAgFnSc2d1	Nejedlého
<g/>
]	]	kIx)	]
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
pokládat	pokládat	k5eAaImF	pokládat
pouze	pouze	k6eAd1	pouze
lidovou	lidový	k2eAgFnSc4d1	lidová
edici	edice	k1gFnSc4	edice
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
,	,	kIx,	,
vyhlášenou	vyhlášený	k2eAgFnSc7d1	vyhlášená
Gottwaldem	Gottwald	k1gMnSc7	Gottwald
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1948	[number]	k4	1948
jako	jako	k8xC	jako
propagaci	propagace	k1gFnSc4	propagace
"	"	kIx"	"
<g/>
klasického	klasický	k2eAgNnSc2d1	klasické
dědictví	dědictví	k1gNnSc2	dědictví
české	český	k2eAgFnSc2d1	Česká
kultury	kultura	k1gFnSc2	kultura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ne	ne	k9	ne
že	že	k8xS	že
by	by	kYmCp3nP	by
ti	ten	k3xDgMnPc1	ten
dva	dva	k4xCgMnPc1	dva
[	[	kIx(	[
<g/>
Gottwald	Gottwald	k1gMnSc1	Gottwald
a	a	k8xC	a
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
<g/>
]	]	kIx)	]
smyslu	smysl	k1gInSc6	smysl
Jiráskova	Jiráskův	k2eAgNnSc2d1	Jiráskovo
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
rostlého	rostlý	k2eAgMnSc2d1	rostlý
z	z	k7c2	z
přástkové	přástkový	k2eAgFnSc2d1	přástkový
podstaty	podstata	k1gFnSc2	podstata
lidového	lidový	k2eAgNnSc2d1	lidové
báječství	báječství	k1gNnSc2	báječství
<g/>
,	,	kIx,	,
vskutku	vskutku	k9	vskutku
rozuměli	rozumět	k5eAaImAgMnP	rozumět
<g/>
:	:	kIx,	:
viděli	vidět	k5eAaImAgMnP	vidět
nikoliv	nikoliv	k9	nikoliv
umění	umění	k1gNnSc4	umění
největšího	veliký	k2eAgInSc2d3	veliký
epického	epický	k2eAgInSc2d1	epický
vypravěče	vypravěč	k1gMnPc4	vypravěč
naší	náš	k3xOp1gFnSc2	náš
prózy	próza	k1gFnSc2	próza
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
jeho	jeho	k3xOp3gFnSc4	jeho
politickou	politický	k2eAgFnSc4d1	politická
tendenci	tendence	k1gFnSc4	tendence
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
ostatně	ostatně	k6eAd1	ostatně
mýlili	mýlit	k5eAaImAgMnP	mýlit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
alespoň	alespoň	k9	alespoň
byl	být	k5eAaImAgMnS	být
nejširším	široký	k2eAgFnPc3d3	nejširší
čtenářským	čtenářský	k2eAgFnPc3d1	čtenářská
vrstvám	vrstva	k1gFnPc3	vrstva
našeho	náš	k3xOp1gInSc2	náš
národa	národ	k1gInSc2	národ
podán	podán	k2eAgInSc1d1	podán
kus	kus	k1gInSc1	kus
chleba	chléb	k1gInSc2	chléb
umělecky	umělecky	k6eAd1	umělecky
i	i	k9	i
lidsky	lidsky	k6eAd1	lidsky
opravdu	opravdu	k6eAd1	opravdu
živného	živný	k2eAgMnSc4d1	živný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Jiráskova	Jiráskův	k2eAgNnPc1d1	Jiráskovo
díla	dílo	k1gNnPc1	dílo
vycházela	vycházet	k5eAaImAgNnP	vycházet
i	i	k9	i
v	v	k7c6	v
nakladatelstvích	nakladatelství	k1gNnPc6	nakladatelství
speciálně	speciálně	k6eAd1	speciálně
zaměřených	zaměřený	k2eAgNnPc6d1	zaměřené
na	na	k7c6	na
mládež	mládež	k1gFnSc1	mládež
(	(	kIx(	(
<g/>
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
dětské	dětský	k2eAgFnSc2d1	dětská
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Albatros	albatros	k1gMnSc1	albatros
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
Státním	státní	k2eAgNnSc6d1	státní
pedagogickém	pedagogický	k2eAgNnSc6d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
vydání	vydání	k1gNnSc4	vydání
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
opatřena	opatřit	k5eAaPmNgFnS	opatřit
vysvětlivkami	vysvětlivka	k1gFnPc7	vysvětlivka
a	a	k8xC	a
komentářem	komentář	k1gInSc7	komentář
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
mimočítankové	mimočítankový	k2eAgFnSc2d1	mimočítanková
četby	četba	k1gFnSc2	četba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
Jiráskových	Jiráskových	k2eAgFnPc2d1	Jiráskových
prací	práce	k1gFnPc2	práce
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
slovenštiny	slovenština	k1gFnSc2	slovenština
<g/>
,	,	kIx,	,
polštiny	polština	k1gFnSc2	polština
<g/>
,	,	kIx,	,
maďarštiny	maďarština	k1gFnSc2	maďarština
<g/>
,	,	kIx,	,
ruštiny	ruština	k1gFnSc2	ruština
i	i	k8xC	i
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
Jiráskovy	Jiráskův	k2eAgInPc4d1	Jiráskův
spisy	spis	k1gInPc4	spis
vydávány	vydáván	k2eAgInPc4d1	vydáván
také	také	k9	také
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
e-knih	enih	k1gMnSc1	e-knih
<g/>
;	;	kIx,	;
např.	např.	kA	např.
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
e-knihy	enih	k1gInPc1	e-knih
<g/>
:	:	kIx,	:
F.	F.	kA	F.
L.	L.	kA	L.
Věk	věk	k1gInSc4	věk
I.	I.	kA	I.
–	–	k?	–
V.	V.	kA	V.
<g/>
,	,	kIx,	,
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
I.	I.	kA	I.
–	–	k?	–
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
až	až	k9	až
na	na	k7c4	na
konec	konec	k1gInSc4	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Filosofská	Filosofská	k?	Filosofská
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
,	,	kIx,	,
Psohlavci	psohlavec	k1gMnPc1	psohlavec
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgFnPc1d1	Staré
pověsti	pověst	k1gFnPc1	pověst
české	český	k2eAgFnPc1d1	Česká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
prózy	próza	k1gFnSc2	próza
a	a	k8xC	a
dramat	drama	k1gNnPc2	drama
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
i	i	k9	i
Jiráskovy	Jiráskův	k2eAgFnPc1d1	Jiráskova
básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
a	a	k8xC	a
část	část	k1gFnSc1	část
korespondence	korespondence	k1gFnSc2	korespondence
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc2	vydání
druhé	druhý	k4xOgFnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
:	:	kIx,	:
Nartex	Nartex	k1gInSc1	Nartex
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
2006	[number]	k4	2006
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
©	©	k?	©
<g/>
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
93	[number]	k4	93
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
8713	[number]	k4	8713
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mých	můj	k3xOp1gFnPc2	můj
pamětí	paměť	k1gFnPc2	paměť
<g/>
:	:	kIx,	:
Poslední	poslední	k2eAgFnPc1d1	poslední
kapitoly	kapitola	k1gFnPc1	kapitola
k	k	k7c3	k
Nové	Nové	k2eAgFnSc3d1	Nové
kronice	kronika	k1gFnSc3	kronika
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
v	v	k7c6	v
MF	MF	kA	MF
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
478	[number]	k4	478
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
s.	s.	k?	s.
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Dopisy	dopis	k1gInPc1	dopis
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
Z	z	k7c2	z
let	léto	k1gNnPc2	léto
<g/>
]	]	kIx)	]
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
209	[number]	k4	209
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ALEŠ	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
<g/>
,	,	kIx,	,
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
a	a	k8xC	a
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
listy	lista	k1gFnSc2	lista
dvou	dva	k4xCgMnPc2	dva
přátel	přítel	k1gMnPc2	přítel
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s	s	k7c7	s
reprodukcemi	reprodukce	k1gFnPc7	reprodukce
kreseb	kresba	k1gFnPc2	kresba
Mikoláše	Mikoláš	k1gMnSc4	Mikoláš
Alše	Aleš	k1gMnSc4	Aleš
a	a	k8xC	a
Aloise	Alois	k1gMnSc4	Alois
Jiráska	Jirásek	k1gMnSc4	Jirásek
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
242	[number]	k4	242
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
l.	l.	k?	l.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Celková	celkový	k2eAgFnSc1d1	celková
charakteristika	charakteristika	k1gFnSc1	charakteristika
díla	dílo	k1gNnSc2	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
žánrově	žánrově	k6eAd1	žánrově
rozmanitých	rozmanitý	k2eAgInPc6d1	rozmanitý
dílech	díl	k1gInPc6	díl
podává	podávat	k5eAaImIp3nS	podávat
podstatný	podstatný	k2eAgInSc4d1	podstatný
průřez	průřez	k1gInSc4	průřez
téměř	téměř	k6eAd1	téměř
celými	celý	k2eAgFnPc7d1	celá
českými	český	k2eAgFnPc7d1	Česká
dějinami	dějiny	k1gFnPc7	dějiny
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
úseky	úsek	k1gInPc4	úsek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Jirásek	Jirásek	k1gMnSc1	Jirásek
chápal	chápat	k5eAaImAgMnS	chápat
jako	jako	k9	jako
podstatné	podstatný	k2eAgNnSc4d1	podstatné
pro	pro	k7c4	pro
poznání	poznání	k1gNnSc4	poznání
minulosti	minulost	k1gFnSc2	minulost
důležité	důležitý	k2eAgFnSc2d1	důležitá
k	k	k7c3	k
porozumění	porozumění	k1gNnSc3	porozumění
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
široké	široký	k2eAgInPc1d1	široký
obrazy	obraz	k1gInPc1	obraz
se	se	k3xPyFc4	se
zajímají	zajímat	k5eAaImIp3nP	zajímat
o	o	k7c4	o
rozrůzněnost	rozrůzněnost	k1gFnSc4	rozrůzněnost
hromadných	hromadný	k2eAgNnPc2d1	hromadné
hnutí	hnutí	k1gNnPc2	hnutí
i	i	k9	i
o	o	k7c4	o
morální	morální	k2eAgFnPc4d1	morální
příčiny	příčina	k1gFnPc4	příčina
jejich	jejich	k3xOp3gInSc2	jejich
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Jirásek	Jirásek	k1gMnSc1	Jirásek
psal	psát	k5eAaImAgMnS	psát
historickou	historický	k2eAgFnSc4d1	historická
prózu	próza	k1gFnSc4	próza
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ovšem	ovšem	k9	ovšem
nelze	lze	k6eNd1	lze
jeho	jeho	k3xOp3gNnSc4	jeho
podání	podání	k1gNnSc4	podání
historie	historie	k1gFnSc2	historie
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xS	jako
historicky	historicky	k6eAd1	historicky
zcela	zcela	k6eAd1	zcela
věrné	věrný	k2eAgFnPc1d1	věrná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dobových	dobový	k2eAgFnPc2d1	dobová
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
chápání	chápání	k1gNnSc1	chápání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Jiráskově	Jiráskův	k2eAgInSc6d1	Jiráskův
díle	díl	k1gInSc6	díl
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
historie	historie	k1gFnSc2	historie
aktualizována	aktualizovat	k5eAaBmNgFnS	aktualizovat
<g/>
,	,	kIx,	,
idealizována	idealizovat	k5eAaBmNgFnS	idealizovat
a	a	k8xC	a
líčena	líčit	k5eAaImNgFnS	líčit
v	v	k7c6	v
opravené	opravený	k2eAgFnSc6d1	opravená
či	či	k8xC	či
změněné	změněný	k2eAgFnSc6d1	změněná
podobě	podoba	k1gFnSc6	podoba
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhovovala	vyhovovat	k5eAaImAgFnS	vyhovovat
pokračující	pokračující	k2eAgFnSc4d1	pokračující
radikalizaci	radikalizace	k1gFnSc4	radikalizace
boje	boj	k1gInSc2	boj
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jirásek	Jirásek	k1gMnSc1	Jirásek
líčí	líčit	k5eAaImIp3nS	líčit
historii	historie	k1gFnSc4	historie
polarizovaně	polarizovaně	k6eAd1	polarizovaně
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
dobově	dobově	k6eAd1	dobově
příznačném	příznačný	k2eAgNnSc6d1	příznačné
napětí	napětí	k1gNnSc6	napětí
češství	češství	k1gNnSc2	češství
a	a	k8xC	a
němectví	němectví	k1gNnSc2	němectví
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
často	často	k6eAd1	často
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
svár	svár	k1gInSc1	svár
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
lži	lež	k1gFnSc2	lež
<g/>
,	,	kIx,	,
poctivosti	poctivost	k1gFnSc2	poctivost
a	a	k8xC	a
podvodu	podvod	k1gInSc2	podvod
<g/>
,	,	kIx,	,
dobrého	dobrý	k2eAgInSc2d1	dobrý
a	a	k8xC	a
špatného	špatný	k2eAgInSc2d1	špatný
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
pýchy	pýcha	k1gFnSc2	pýcha
a	a	k8xC	a
pokory	pokora	k1gFnSc2	pokora
<g/>
,	,	kIx,	,
nezájmu	nezájem	k1gInSc3	nezájem
a	a	k8xC	a
obětavosti	obětavost	k1gFnSc3	obětavost
<g/>
.	.	kIx.	.
</s>
<s>
Jirásek	Jirásek	k1gMnSc1	Jirásek
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
snažil	snažit	k5eAaImAgInS	snažit
podpořit	podpořit	k5eAaPmF	podpořit
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
spravedlnosti	spravedlnost	k1gFnSc6	spravedlnost
a	a	k8xC	a
po	po	k7c6	po
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
,	,	kIx,	,
chápané	chápaný	k2eAgFnSc6d1	chápaná
jako	jako	k8xS	jako
oddělení	oddělení	k1gNnSc6	oddělení
české	český	k2eAgFnSc2d1	Česká
mono-lingvistické	monoingvistický	k2eAgFnSc2d1	mono-lingvistický
skupiny	skupina	k1gFnSc2	skupina
od	od	k7c2	od
liberální	liberální	k2eAgFnSc2d1	liberální
multi-kulturality	multiulturalita	k1gFnSc2	multi-kulturalita
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
Jiráska	Jirásek	k1gMnSc2	Jirásek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uměl	umět	k5eAaImAgMnS	umět
psát	psát	k5eAaImF	psát
čtivě	čtivě	k6eAd1	čtivě
<g/>
,	,	kIx,	,
a	a	k8xC	a
psal	psát	k5eAaImAgInS	psát
jen	jen	k9	jen
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
si	se	k3xPyFc3	se
národ	národ	k1gInSc1	národ
přál	přát	k5eAaImAgInS	přát
o	o	k7c4	o
sobě	se	k3xPyFc3	se
slyšet	slyšet	k5eAaImF	slyšet
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ačkoliv	ačkoliv	k8xS	ačkoliv
počátkem	počátkem	k7c2	počátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začal	začít	k5eAaPmAgInS	začít
realismus	realismus	k1gInSc1	realismus
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
Jirásek	Jirásek	k1gMnSc1	Jirásek
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
proudu	proud	k1gInSc3	proud
nikdy	nikdy	k6eAd1	nikdy
nepřidal	přidat	k5eNaPmAgInS	přidat
a	a	k8xC	a
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
se	se	k3xPyFc4	se
kriticky	kriticky	k6eAd1	kriticky
proti	proti	k7c3	proti
národu	národ	k1gInSc3	národ
nevyjádřil	vyjádřit	k5eNaPmAgMnS	vyjádřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
líčení	líčení	k1gNnSc2	líčení
převratných	převratný	k2eAgFnPc2d1	převratná
historických	historický	k2eAgFnPc2d1	historická
scén	scéna	k1gFnPc2	scéna
věnuje	věnovat	k5eAaPmIp3nS	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
také	také	k9	také
častému	častý	k2eAgNnSc3d1	časté
líčení	líčení	k1gNnSc3	líčení
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
osobních	osobní	k2eAgInPc2d1	osobní
pocitů	pocit	k1gInPc2	pocit
a	a	k8xC	a
prožitků	prožitek	k1gInPc2	prožitek
hrdiny	hrdina	k1gMnSc2	hrdina
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
o	o	k7c6	o
zachycení	zachycení	k1gNnSc6	zachycení
domácí	domácí	k2eAgNnPc1d1	domácí
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
venkovské	venkovský	k2eAgFnSc2d1	venkovská
idyly	idyla	k1gFnSc2	idyla
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
též	též	k9	též
jeho	jeho	k3xOp3gInSc1	jeho
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
etnografii	etnografie	k1gFnSc4	etnografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jirásek	Jirásek	k1gMnSc1	Jirásek
byl	být	k5eAaImAgMnS	být
i	i	k9	i
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgMnS	napsat
12	[number]	k4	12
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
premiéry	premiéra	k1gFnPc1	premiéra
byly	být	k5eAaImAgFnP	být
většinou	většinou	k6eAd1	většinou
uváděny	uvádět	k5eAaImNgFnP	uvádět
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Jiráskových	Jiráskových	k2eAgNnPc2d1	Jiráskových
dramat	drama	k1gNnPc2	drama
rád	rád	k6eAd1	rád
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
Eduard	Eduard	k1gMnSc1	Eduard
Vojan	Vojan	k1gMnSc1	Vojan
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
her	hra	k1gFnPc2	hra
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
repertoáru	repertoár	k1gInSc6	repertoár
českých	český	k2eAgNnPc2d1	české
divadel	divadlo	k1gNnPc2	divadlo
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiráskovy	Jiráskův	k2eAgInPc1d1	Jiráskův
historické	historický	k2eAgInPc1d1	historický
romány	román	k1gInPc1	román
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
byly	být	k5eAaImAgFnP	být
vesměs	vesměs	k6eAd1	vesměs
vnímány	vnímat	k5eAaImNgInP	vnímat
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
soudobého	soudobý	k2eAgInSc2d1	soudobý
boje	boj	k1gInSc2	boj
o	o	k7c4	o
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc4d1	národní
i	i	k8xC	i
státní	státní	k2eAgFnSc4d1	státní
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
,	,	kIx,	,
Jirásek	Jirásek	k1gMnSc1	Jirásek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejpopulárnějším	populární	k2eAgMnSc7d3	nejpopulárnější
českým	český	k2eAgMnSc7d1	český
autorem	autor	k1gMnSc7	autor
historické	historický	k2eAgFnSc2d1	historická
beletrie	beletrie	k1gFnSc2	beletrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Málokterý	málokterý	k3yIgMnSc1	málokterý
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
tak	tak	k6eAd1	tak
nekritickým	kritický	k2eNgInSc7d1	nekritický
obdivem	obdiv	k1gInSc7	obdiv
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
s	s	k7c7	s
tak	tak	k6eAd1	tak
pohrdlivým	pohrdlivý	k2eAgNnSc7d1	pohrdlivé
odsouzením	odsouzení	k1gNnSc7	odsouzení
jako	jako	k9	jako
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
mu	on	k3xPp3gMnSc3	on
bývá	bývat	k5eAaImIp3nS	bývat
vyčítána	vyčítán	k2eAgFnSc1d1	vyčítána
rozvláčnost	rozvláčnost	k1gFnSc1	rozvláčnost
a	a	k8xC	a
idealizování	idealizování	k1gNnSc1	idealizování
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
podloženo	podložit	k5eAaPmNgNnS	podložit
studiem	studio	k1gNnSc7	studio
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
si	se	k3xPyFc3	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
autor	autor	k1gMnSc1	autor
vybíral	vybírat	k5eAaImAgMnS	vybírat
především	především	k6eAd1	především
ta	ten	k3xDgNnPc1	ten
fakta	faktum	k1gNnPc1	faktum
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zapadala	zapadat	k5eAaImAgFnS	zapadat
do	do	k7c2	do
jeho	jeho	k3xOp3gInPc2	jeho
vlastenecky	vlastenecky	k6eAd1	vlastenecky
výchovných	výchovný	k2eAgInPc2d1	výchovný
záměrů	záměr	k1gInPc2	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
těžkých	těžký	k2eAgFnPc6d1	těžká
dobách	doba	k1gFnPc6	doba
obou	dva	k4xCgFnPc6	dva
světových	světový	k2eAgFnPc2d1	světová
válek	válka	k1gFnPc2	válka
měly	mít	k5eAaImAgInP	mít
Jiráskovy	Jiráskův	k2eAgInPc1d1	Jiráskův
romány	román	k1gInPc1	román
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
povzbuzení	povzbuzení	k1gNnSc4	povzbuzení
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmová	filmový	k2eAgNnPc4d1	filmové
zpracování	zpracování	k1gNnPc4	zpracování
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Jiráskova	Jiráskův	k2eAgNnPc1d1	Jiráskovo
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
1925	[number]	k4	1925
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
Němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
78	[number]	k4	78
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Lamač	lamač	k1gMnSc1	lamač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1931	[number]	k4	1931
Psohlavci	psohlavec	k1gMnPc7	psohlavec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
105	[number]	k4	105
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Innemann	Innemann	k1gMnSc1	Innemann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
Vojnarka	Vojnarka	k1gFnSc1	Vojnarka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
102	[number]	k4	102
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vladimír	Vladimír	k1gMnSc1	Vladimír
Borský	Borský	k1gMnSc1	Borský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1937	[number]	k4	1937
Filosofská	Filosofská	k?	Filosofská
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
novely	novela	k1gFnSc2	novela
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
88	[number]	k4	88
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Otakar	Otakar	k1gMnSc1	Otakar
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
84	[number]	k4	84
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Lamač	lamač	k1gMnSc1	lamač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
Jan	Jan	k1gMnSc1	Jan
Roháč	roháč	k1gMnSc1	roháč
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
84	[number]	k4	84
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vladimír	Vladimír	k1gMnSc1	Vladimír
Borský	Borský	k1gMnSc1	Borský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
Temno	temno	k1gNnSc1	temno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
115	[number]	k4	115
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Steklý	Steklý	k1gMnSc1	Steklý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1952	[number]	k4	1952
Staré	Staré	k2eAgFnPc1d1	Staré
pověsti	pověst	k1gFnPc1	pověst
české	český	k2eAgFnPc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
Animovaný	animovaný	k2eAgInSc1d1	animovaný
loutkový	loutkový	k2eAgInSc1d1	loutkový
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
91	[number]	k4	91
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jiří	Jiří	k1gMnSc1	Jiří
Trnka	Trnka	k1gMnSc1	Trnka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
115	[number]	k4	115
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Otakar	Otakar	k1gMnSc1	Otakar
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1955	[number]	k4	1955
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
102	[number]	k4	102
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Otakar	Otakar	k1gMnSc1	Otakar
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1955	[number]	k4	1955
Psohlavci	psohlavec	k1gMnPc7	psohlavec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
91	[number]	k4	91
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Martin	Martin	k1gMnSc1	Martin
Frič	Frič	k1gMnSc1	Frič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
Ztracenci	ztracenec	k1gMnPc7	ztracenec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
povídky	povídka	k1gFnSc2	povídka
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
83	[number]	k4	83
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Miloš	Miloš	k1gMnSc1	Miloš
Makovec	Makovec	k1gMnSc1	Makovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
Proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
109	[number]	k4	109
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Otakar	Otakar	k1gMnSc1	Otakar
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
Magdalena	Magdalena	k1gFnSc1	Magdalena
Dobromila	Dobromila	k1gFnSc1	Dobromila
Rettigová	Rettigový	k2eAgFnSc1d1	Rettigová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
101	[number]	k4	101
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
František	František	k1gMnSc1	František
Filip	Filip	k1gMnSc1	Filip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1965	[number]	k4	1965
Samota	samota	k1gFnSc1	samota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
76	[number]	k4	76
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
112	[number]	k4	112
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
František	František	k1gMnSc1	František
Filip	Filip	k1gMnSc1	Filip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
Vojnarka	Vojnarka	k1gFnSc1	Vojnarka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
72	[number]	k4	72
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
Záhořanský	Záhořanský	k2eAgInSc4d1	Záhořanský
hon	hon	k1gInSc4	hon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
povídky	povídka	k1gFnSc2	povídka
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
87	[number]	k4	87
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Bohumil	Bohumil	k1gMnSc1	Bohumil
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
F.	F.	kA	F.
L.	L.	kA	L.
Věk	věk	k1gInSc1	věk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
.	.	kIx.	.
13	[number]	k4	13
x	x	k?	x
53	[number]	k4	53
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
František	František	k1gMnSc1	František
Filip	Filip	k1gMnSc1	Filip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
Na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
vévodském	vévodský	k2eAgInSc6d1	vévodský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
74	[number]	k4	74
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Evžen	Evžen	k1gMnSc1	Evžen
Němec	Němec	k1gMnSc1	Němec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Poklad	poklad	k1gInSc1	poklad
hraběte	hrabě	k1gMnSc2	hrabě
Chamaré	Chamarý	k2eAgNnSc4d1	Chamaré
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Jiráskovy	Jiráskův	k2eAgFnSc2d1	Jiráskova
novely	novela	k1gFnSc2	novela
Poklad	poklad	k1gInSc1	poklad
<g/>
.	.	kIx.	.
78	[number]	k4	78
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Troška	troška	k6eAd1	troška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
Psohlavci	psohlavec	k1gMnPc7	psohlavec
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgFnSc1d1	televizní
inscenace	inscenace	k1gFnSc1	inscenace
opery	opera	k1gFnSc2	opera
Karla	Karel	k1gMnSc2	Karel
Kovařovice	Kovařovice	k1gFnSc2	Kovařovice
<g/>
;	;	kIx,	;
libreto	libreto	k1gNnSc1	libreto
napsal	napsat	k5eAaPmAgInS	napsat
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
Jiráskova	Jiráskův	k2eAgInSc2d1	Jiráskův
románu	román	k1gInSc2	román
Karel	Karel	k1gMnSc1	Karel
Šípek	Šípek	k1gMnSc1	Šípek
<g/>
;	;	kIx,	;
93	[number]	k4	93
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Milan	Milan	k1gMnSc1	Milan
Macků	Macků	k1gMnSc1	Macků
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
Preceptor	preceptor	k1gMnSc1	preceptor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
povídky	povídka	k1gFnSc2	povídka
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
Na	na	k7c6	na
Ostrově	ostrov	k1gInSc6	ostrov
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
73	[number]	k4	73
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jiří	Jiří	k1gMnSc1	Jiří
Bělka	bělka	k1gFnSc1	bělka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Staré	Staré	k2eAgFnPc1d1	Staré
pověsti	pověst	k1gFnPc1	pověst
české	český	k2eAgFnPc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
Animovaný	animovaný	k2eAgInSc1d1	animovaný
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
.	.	kIx.	.
26	[number]	k4	26
x	x	k?	x
10	[number]	k4	10
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mráz	Mráz	k1gMnSc1	Mráz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Život	život	k1gInSc1	život
Jiráska	Jirásek	k1gMnSc2	Jirásek
===	===	k?	===
</s>
</p>
<p>
<s>
1952	[number]	k4	1952
Mladá	mladá	k1gFnSc1	mladá
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Neffa	Neff	k1gMnSc2	Neff
<g/>
.	.	kIx.	.
110	[number]	k4	110
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Václav	Václav	k1gMnSc1	Václav
Krška	Krška	k1gMnSc1	Krška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Citát	citát	k1gInSc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BORECKÝ	borecký	k2eAgMnSc1d1	borecký
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Nákladem	náklad	k1gInSc7	náklad
České	český	k2eAgFnSc2d1	Česká
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
666	[number]	k4	666
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
371	[number]	k4	371
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
2497	[number]	k4	2497
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
116	[number]	k4	116
<g/>
–	–	k?	–
<g/>
122	[number]	k4	122
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Literatura	literatura	k1gFnSc1	literatura
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
/	/	kIx~	/
Redaktor	redaktor	k1gMnSc1	redaktor
svazku	svazek	k1gInSc2	svazek
Miloš	Miloš	k1gMnSc1	Miloš
Pohorský	pohorský	k2eAgMnSc1d1	pohorský
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československá	československý	k2eAgFnSc1d1	Československá
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
631	[number]	k4	631
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Kapitolu	Kapitol	k1gInSc2	Kapitol
"	"	kIx"	"
<g/>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
"	"	kIx"	"
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
431	[number]	k4	431
<g/>
–	–	k?	–
<g/>
464	[number]	k4	464
napsal	napsat	k5eAaPmAgMnS	napsat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pešat	Pešat	k2eAgMnSc1d1	Pešat
→	→	k?	→
dostupné	dostupný	k2eAgNnSc1d1	dostupné
též	též	k9	též
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
/	/	kIx~	/
redakce	redakce	k1gFnSc2	redakce
Otakar	Otakar	k1gMnSc1	Otakar
Chaloupka	Chaloupka	k1gMnSc1	Chaloupka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
154	[number]	k4	154
<g/>
–	–	k?	–
<g/>
159	[number]	k4	159
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FORST	FORST	kA	FORST
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
H	H	kA	H
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
589	[number]	k4	589
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
468	[number]	k4	468
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
543	[number]	k4	543
<g/>
–	–	k?	–
<g/>
549	[number]	k4	549
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JANÁČKOVÁ	Janáčková	k1gFnSc1	Janáčková
<g/>
,	,	kIx,	,
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
monografie	monografie	k1gFnPc1	monografie
s	s	k7c7	s
ukázkami	ukázka	k1gFnPc7	ukázka
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
581	[number]	k4	581
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
40	[number]	k4	40
<g/>
]	]	kIx)	]
s.	s.	k?	s.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUNC	Kunc	k1gMnSc1	Kunc
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Soupis	soupis	k1gInSc1	soupis
díla	dílo	k1gNnSc2	dílo
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
o	o	k7c6	o
něm.	něm.	k?	něm.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
80	[number]	k4	80
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KVĚT	květ	k1gInSc1	květ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
ilustraci	ilustrace	k1gFnSc6	ilustrace
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
93	[number]	k4	93
s.	s.	k?	s.
<g/>
,	,	kIx,	,
160	[number]	k4	160
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
–	–	k?	–
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
291	[number]	k4	291
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVÁK	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Arne	Arne	k1gMnSc1	Arne
<g/>
.	.	kIx.	.
</s>
<s>
Zvony	zvon	k1gInPc1	zvon
domova	domov	k1gInSc2	domov
a	a	k8xC	a
Myšlenky	myšlenka	k1gFnSc2	myšlenka
a	a	k8xC	a
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Novina	novina	k1gFnSc1	novina
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Jiráskovo	Jiráskův	k2eAgNnSc1d1	Jiráskovo
"	"	kIx"	"
<g/>
Temno	temno	k1gNnSc1	temno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s.	s.	k?	s.
109	[number]	k4	109
<g/>
–	–	k?	–
<g/>
132	[number]	k4	132
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
616	[number]	k4	616
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	let	k1gInPc6	let
1815	[number]	k4	1815
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
</s>
</p>
<p>
<s>
Jiráskův	Jiráskův	k2eAgInSc1d1	Jiráskův
kraj	kraj	k1gInSc1	kraj
</s>
</p>
<p>
<s>
Jiráskův	Jiráskův	k2eAgInSc1d1	Jiráskův
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Jiráskovo	Jiráskův	k2eAgNnSc1d1	Jiráskovo
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
Lípa	lípa	k1gFnSc1	lípa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiráskovo	Jiráskův	k2eAgNnSc1d1	Jiráskovo
gymnázium	gymnázium	k1gNnSc1	gymnázium
Náchod	Náchod	k1gInSc1	Náchod
</s>
</p>
<p>
<s>
Jiráskův	Jiráskův	k2eAgInSc1d1	Jiráskův
Hronov	Hronov	k1gInSc1	Hronov
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc1	pomník
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Díla	dílo	k1gNnSc2	dílo
online	onlinout	k5eAaPmIp3nS	onlinout
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
Jiráskových	Jiráskových	k2eAgNnPc2d1	Jiráskových
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
online	onlinout	k5eAaPmIp3nS	onlinout
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
Kramerius	Kramerius	k1gMnSc1	Kramerius
(	(	kIx(	(
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
Kramerius	Kramerius	k1gMnSc1	Kramerius
4	[number]	k4	4
(	(	kIx(	(
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc1d1	konkrétní
odkazy	odkaz	k1gInPc1	odkaz
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
o	o	k7c6	o
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
dílech	díl	k1gInPc6	díl
(	(	kIx(	(
<g/>
F.	F.	kA	F.
L.	L.	kA	L.
Věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
Mezi	mezi	k7c7	mezi
proudy	proud	k1gInPc7	proud
<g/>
,	,	kIx,	,
Proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
Bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
<g/>
,	,	kIx,	,
Temno	temno	k1gNnSc1	temno
<g/>
,	,	kIx,	,
Maryla	Maryla	k1gFnSc1	Maryla
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
Jiráskových	Jiráskových	k2eAgNnPc2d1	Jiráskových
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
e-knihy	enih	k1gInPc1	e-knih
volně	volně	k6eAd1	volně
dostupná	dostupný	k2eAgFnSc1d1	dostupná
i	i	k9	i
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
:	:	kIx,	:
historická	historický	k2eAgFnSc1d1	historická
hra	hra	k1gFnSc1	hra
o	o	k7c6	o
pěti	pět	k4xCc6	pět
jednáních	jednání	k1gNnPc6	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
Jiráskovy	Jiráskův	k2eAgFnSc2d1	Jiráskova
historické	historický	k2eAgFnSc2d1	historická
hry	hra	k1gFnSc2	hra
o	o	k7c6	o
5	[number]	k4	5
jednáních	jednání	k1gNnPc6	jednání
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Jirchářích	jirchář	k1gMnPc6	jirchář
a	a	k8xC	a
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
:	:	kIx,	:
drama	drama	k1gFnSc1	drama
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
jednáních	jednání	k1gNnPc6	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
o	o	k7c6	o
3	[number]	k4	3
dějstvích	dějství	k1gNnPc6	dějství
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
Čechách	Čechy	k1gFnPc6	Čechy
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
ND	ND	kA	ND
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Vojnarka	Vojnarka	k1gFnSc1	Vojnarka
:	:	kIx,	:
drama	drama	k1gNnSc1	drama
o	o	k7c6	o
čtyřech	čtyři	k4xCgInPc6	čtyři
jednáních	jednání	k1gNnPc6	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
o	o	k7c6	o
4	[number]	k4	4
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
Čechách	Čechy	k1gFnPc6	Čechy
poblíž	poblíž	k7c2	poblíž
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Moravou	Morava	k1gFnSc7	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Realistické	realistický	k2eAgNnSc1d1	realistické
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
selka	selka	k1gFnSc1	selka
Madlena	Madlena	k1gFnSc1	Madlena
Vojnarová	Vojnarová	k1gFnSc1	Vojnarová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vzepře	vzepřít	k5eAaPmIp3nS	vzepřít
tvrdým	tvrdý	k2eAgInPc3d1	tvrdý
venkovským	venkovský	k2eAgInPc3d1	venkovský
zvykům	zvyk	k1gInPc3	zvyk
a	a	k8xC	a
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
naplnění	naplnění	k1gNnSc4	naplnění
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Psohlavci	psohlavec	k1gMnPc1	psohlavec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
Chodové	Chod	k1gMnPc1	Chod
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Koziny	kozina	k1gFnSc2	kozina
usilují	usilovat	k5eAaImIp3nP	usilovat
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
chodských	chodský	k2eAgFnPc2d1	Chodská
výsad	výsada	k1gFnPc2	výsada
<g/>
,	,	kIx,	,
odboj	odboj	k1gInSc4	odboj
je	být	k5eAaImIp3nS	být
potlačen	potlačen	k2eAgMnSc1d1	potlačen
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Sladký	Sladký	k1gMnSc1	Sladký
Kozina	kozina	k1gFnSc1	kozina
popraven	popravit	k5eAaPmNgInS	popravit
<g/>
,	,	kIx,	,
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
"	"	kIx"	"
<g/>
Lomikara	Lomikar	k1gMnSc4	Lomikar
<g/>
"	"	kIx"	"
na	na	k7c4	na
Boží	boží	k2eAgInSc4d1	boží
Soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
pověsti	pověst	k1gFnPc1	pověst
české	český	k2eAgFnPc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
Černý	Černý	k1gMnSc1	Černý
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jos	Jos	k1gFnSc1	Jos
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
375	[number]	k4	375
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
prosa	prosa	k1gFnSc1	prosa
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
252	[number]	k4	252
s.	s.	k?	s.
Sebr	Sebra	k1gFnPc2	Sebra
<g/>
.	.	kIx.	.
spisy	spis	k1gInPc7	spis
<g/>
;	;	kIx,	;
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Díla	dílo	k1gNnPc1	dílo
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
z	z	k7c2	z
katalogu	katalog	k1gInSc2	katalog
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
dostupná	dostupný	k2eAgNnPc1d1	dostupné
díla	dílo	k1gNnPc1	dílo
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Veřejně	veřejně	k6eAd1	veřejně
dostupná	dostupný	k2eAgNnPc1d1	dostupné
díla	dílo	k1gNnPc1	dílo
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
knihovny	knihovna	k1gFnSc2	knihovna
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
v	v	k7c6	v
souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
–	–	k?	–
stránky	stránka	k1gFnSc2	stránka
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
pouze	pouze	k6eAd1	pouze
Aloisu	Aloisu	k?	Aloisu
Jiráskovi	Jirásek	k1gMnSc3	Jirásek
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc3	jeho
životu	život	k1gInSc3	život
a	a	k8xC	a
dílu	dílo	k1gNnSc3	dílo
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
</s>
</p>
<p>
<s>
Rozhlasové	rozhlasový	k2eAgFnPc1d1	rozhlasová
adaptace	adaptace	k1gFnPc1	adaptace
vybraných	vybraný	k2eAgNnPc2d1	vybrané
děl	dělo	k1gNnPc2	dělo
k	k	k7c3	k
bezplatnému	bezplatný	k2eAgNnSc3d1	bezplatné
stáhnutí	stáhnutí	k1gNnSc3	stáhnutí
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
mp	mp	k?	mp
<g/>
3	[number]	k4	3
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
3	[number]	k4	3
<g/>
-Vltava	-Vltava	k1gFnSc1	-Vltava
</s>
</p>
<p>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Hledání	hledání	k1gNnSc2	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemožnost	nemožnost	k1gFnSc1	nemožnost
on-line	onin	k1gInSc5	on-lin
přehrání	přehrání	k1gNnSc3	přehrání
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
–	–	k?	–
video	video	k1gNnSc4	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
