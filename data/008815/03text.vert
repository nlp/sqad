<p>
<s>
Generátor	generátor	k1gInSc1	generátor
překladačů	překladač	k1gInPc2	překladač
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
compiler-compiler	compilerompiler	k1gMnSc1	compiler-compiler
nebo	nebo	k8xC	nebo
compiler	compiler	k1gMnSc1	compiler
generator	generator	k1gMnSc1	generator
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
matematické	matematický	k2eAgFnSc6d1	matematická
informatice	informatika	k1gFnSc6	informatika
nástroj	nástroj	k1gInSc1	nástroj
pro	pro	k7c4	pro
programování	programování	k1gNnSc4	programování
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
syntaktický	syntaktický	k2eAgMnSc1d1	syntaktický
analyzátor	analyzátor	k1gMnSc1	analyzátor
<g/>
,	,	kIx,	,
interpret	interpret	k1gMnSc1	interpret
nebo	nebo	k8xC	nebo
překladač	překladač	k1gMnSc1	překladač
z	z	k7c2	z
formálního	formální	k2eAgInSc2d1	formální
popisu	popis	k1gInSc2	popis
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
cílového	cílový	k2eAgInSc2d1	cílový
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgMnPc3d3	nejstarší
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
typem	typ	k1gInSc7	typ
generátorů	generátor	k1gInPc2	generátor
překladačů	překladač	k1gMnPc2	překladač
je	být	k5eAaImIp3nS	být
generátor	generátor	k1gInSc1	generátor
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
parser	parser	k1gMnSc1	parser
generator	generator	k1gMnSc1	generator
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
vstupem	vstup	k1gInSc7	vstup
je	být	k5eAaImIp3nS	být
gramatika	gramatika	k1gFnSc1	gramatika
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
BNF	BNF	kA	BNF
<g/>
)	)	kIx)	)
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
výstupem	výstup	k1gInSc7	výstup
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
text	text	k1gInSc1	text
syntaktického	syntaktický	k2eAgInSc2d1	syntaktický
analyzátoru	analyzátor	k1gInSc2	analyzátor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
překladače	překladač	k1gInSc2	překladač
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
generátory	generátor	k1gInPc1	generátor
generátorů	generátor	k1gInPc2	generátor
kódu	kód	k1gInSc2	kód
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
code	code	k6eAd1	code
generator-generators	generatorenerators	k6eAd1	generator-generators
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
JBurg	JBurg	k1gInSc1	JBurg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
nedosáhly	dosáhnout	k5eNaPmAgFnP	dosáhnout
potřebné	potřebný	k2eAgFnPc4d1	potřebná
zralosti	zralost	k1gFnPc4	zralost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ideální	ideální	k2eAgInSc1d1	ideální
generátor	generátor	k1gInSc1	generátor
překladačů	překladač	k1gInPc2	překladač
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
z	z	k7c2	z
popisu	popis	k1gInSc2	popis
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
cílové	cílový	k2eAgFnSc2d1	cílová
instrukční	instrukční	k2eAgFnSc2d1	instrukční
sady	sada	k1gFnSc2	sada
automaticky	automaticky	k6eAd1	automaticky
vygenerovat	vygenerovat	k5eAaPmF	vygenerovat
použitelný	použitelný	k2eAgInSc1d1	použitelný
překladač	překladač	k1gInSc1	překladač
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tohoto	tento	k3xDgInSc2	tento
stupně	stupeň	k1gInSc2	stupeň
dokonalosti	dokonalost	k1gFnSc2	dokonalost
zatím	zatím	k6eAd1	zatím
nedosáhly	dosáhnout	k5eNaPmAgFnP	dosáhnout
ani	ani	k9	ani
nejmodernější	moderní	k2eAgInPc4d3	nejmodernější
generátory	generátor	k1gInPc4	generátor
<g/>
,	,	kIx,	,
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
generátorů	generátor	k1gInPc2	generátor
překladačů	překladač	k1gInPc2	překladač
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
sémantice	sémantika	k1gFnSc6	sémantika
nebo	nebo	k8xC	nebo
cílové	cílový	k2eAgFnSc6d1	cílová
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Varianty	variant	k1gInPc4	variant
==	==	k?	==
</s>
</p>
<p>
<s>
Typický	typický	k2eAgInSc1d1	typický
generátor	generátor	k1gInSc1	generátor
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
každému	každý	k3xTgNnSc3	každý
pravidlu	pravidlo	k1gNnSc3	pravidlo
gramatiky	gramatika	k1gFnSc2	gramatika
programový	programový	k2eAgInSc4d1	programový
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
vykonán	vykonán	k2eAgMnSc1d1	vykonán
<g/>
,	,	kIx,	,
když	když	k8xS	když
syntaktický	syntaktický	k2eAgInSc1d1	syntaktický
analyzátor	analyzátor	k1gInSc1	analyzátor
použije	použít	k5eAaPmIp3nS	použít
příslušné	příslušný	k2eAgNnSc4d1	příslušné
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
fragmenty	fragment	k1gInPc1	fragment
kódu	kód	k1gInSc2	kód
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
sémantické	sémantický	k2eAgFnPc1d1	sémantická
akce	akce	k1gFnPc1	akce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
definují	definovat	k5eAaBmIp3nP	definovat
sémantiku	sémantika	k1gFnSc4	sémantika
syntaktické	syntaktický	k2eAgFnSc2d1	syntaktická
struktury	struktura	k1gFnSc2	struktura
analyzované	analyzovaný	k2eAgNnSc1d1	analyzované
syntaktickým	syntaktický	k2eAgInSc7d1	syntaktický
analyzátorem	analyzátor	k1gInSc7	analyzátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
požadovaném	požadovaný	k2eAgInSc6d1	požadovaný
typu	typ	k1gInSc6	typ
syntaktického	syntaktický	k2eAgInSc2d1	syntaktický
analyzátoru	analyzátor	k1gInSc2	analyzátor
mohou	moct	k5eAaImIp3nP	moct
tyto	tento	k3xDgFnPc1	tento
akce	akce	k1gFnPc1	akce
vytvářet	vytvářet	k5eAaImF	vytvářet
derivační	derivační	k2eAgInSc4d1	derivační
strom	strom	k1gInSc4	strom
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
parse	parse	k6eAd1	parse
tree	treat	k5eAaPmIp3nS	treat
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
strom	strom	k1gInSc4	strom
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
abstract	abstract	k5eAaPmF	abstract
syntax	syntax	k1gFnSc4	syntax
tree	tre	k1gFnSc2	tre
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
generovat	generovat	k5eAaImF	generovat
proveditelný	proveditelný	k2eAgInSc4d1	proveditelný
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
překvapivě	překvapivě	k6eAd1	překvapivě
výkonných	výkonný	k2eAgInPc2d1	výkonný
<g/>
,	,	kIx,	,
generátorů	generátor	k1gInPc2	generátor
překladačů	překladač	k1gMnPc2	překladač
je	být	k5eAaImIp3nS	být
META	meta	k1gFnSc1	meta
II	II	kA	II
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přijímal	přijímat	k5eAaImAgInS	přijímat
gramatiky	gramatika	k1gFnPc4	gramatika
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
kódu	kód	k1gInSc2	kód
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
přeložit	přeložit	k5eAaPmF	přeložit
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
pokusné	pokusný	k2eAgInPc1d1	pokusný
generátory	generátor	k1gInPc1	generátor
překladačů	překladač	k1gMnPc2	překladač
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
vstup	vstup	k1gInSc4	vstup
formální	formální	k2eAgInSc4d1	formální
popis	popis	k1gInSc4	popis
sémantiky	sémantika	k1gFnSc2	sémantika
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
'	'	kIx"	'
<g/>
používání	používání	k1gNnSc1	používání
sémantického	sémantický	k2eAgInSc2d1	sémantický
překladu	překlad	k1gInSc2	překlad
<g/>
'	'	kIx"	'
a	a	k8xC	a
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
jej	on	k3xPp3gMnSc4	on
Peter	Peter	k1gMnSc1	Peter
Mosses	Mosses	k1gMnSc1	Mosses
jako	jako	k8xS	jako
Systém	systém	k1gInSc1	systém
implementace	implementace	k1gFnSc2	implementace
sémantiky	sémantika	k1gFnSc2	sémantika
(	(	kIx(	(
<g/>
SIS	SIS	kA	SIS
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jak	jak	k6eAd1	jak
generované	generovaný	k2eAgInPc1d1	generovaný
překladače	překladač	k1gInPc1	překladač
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
časově	časově	k6eAd1	časově
i	i	k9	i
prostorově	prostorově	k6eAd1	prostorově
neefektivní	efektivní	k2eNgMnSc1d1	neefektivní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc1	žádný
překladače	překladač	k1gInPc1	překladač
přepisovacích	přepisovací	k2eAgNnPc2d1	přepisovací
pravidel	pravidlo	k1gNnPc2	pravidlo
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výzkum	výzkum	k1gInSc1	výzkum
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Projekt	projekt	k1gInSc1	projekt
Production	Production	k1gInSc4	Production
Quality	Qualita	k1gFnSc2	Qualita
Compiler-Compiler	Compiler-Compiler	k1gInSc4	Compiler-Compiler
v	v	k7c4	v
Carnegie-Mellon	Carnegie-Mellon	k1gInSc4	Carnegie-Mellon
University	universita	k1gFnSc2	universita
neformalizuje	formalizovat	k5eNaBmIp3nS	formalizovat
sémantiku	sémantika	k1gFnSc4	sémantika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používá	používat	k5eAaImIp3nS	používat
poloformální	poloformální	k2eAgInSc4d1	poloformální
rámec	rámec	k1gInSc4	rámec
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
variant	varianta	k1gFnPc2	varianta
generátorů	generátor	k1gInPc2	generátor
překladačů	překladač	k1gInPc2	překladač
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
generátorů	generátor	k1gInPc2	generátor
přepisovacích	přepisovací	k2eAgInPc2d1	přepisovací
strojů	stroj	k1gInPc2	stroj
zdola	zdola	k6eAd1	zdola
nahoru	nahoru	k6eAd1	nahoru
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
JBurg	JBurg	k1gInSc1	JBurg
<g/>
)	)	kIx)	)
používaných	používaný	k2eAgInPc2d1	používaný
pro	pro	k7c4	pro
pokrytí	pokrytí	k1gNnSc4	pokrytí
derivačních	derivační	k2eAgInPc2d1	derivační
stromů	strom	k1gInPc2	strom
podle	podle	k7c2	podle
přepisovací	přepisovací	k2eAgFnSc2d1	přepisovací
gramatiky	gramatika	k1gFnSc2	gramatika
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
kódu	kód	k1gInSc2	kód
a	a	k8xC	a
generátory	generátor	k1gInPc4	generátor
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
používající	používající	k2eAgFnSc2d1	používající
atributové	atributový	k2eAgFnSc2d1	atributová
gramatiky	gramatika	k1gFnSc2	gramatika
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
například	například	k6eAd1	například
ANTLR	ANTLR	kA	ANTLR
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
současnou	současný	k2eAgFnSc4d1	současná
typovou	typový	k2eAgFnSc4d1	typová
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
šíření	šíření	k1gNnSc4	šíření
konstant	konstanta	k1gFnPc2	konstanta
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
úkoly	úkol	k1gInPc4	úkol
během	během	k7c2	během
fáze	fáze	k1gFnSc2	fáze
analýzy	analýza	k1gFnSc2	analýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
generátor	generátor	k1gInSc1	generátor
překladačů	překladač	k1gInPc2	překladač
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
používal	používat	k5eAaImAgMnS	používat
jméno	jméno	k1gNnSc4	jméno
Compiler-Compiler	Compiler-Compiler	k1gMnSc1	Compiler-Compiler
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Tony	Tony	k1gMnSc1	Tony
Brooker	Brooker	k1gMnSc1	Brooker
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgMnS	sloužit
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
překladače	překladač	k1gInSc2	překladač
pro	pro	k7c4	pro
počítač	počítač	k1gInSc4	počítač
Atlas	Atlas	k1gInSc1	Atlas
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
překladače	překladač	k1gInSc2	překladač
Atlas	Atlas	k1gInSc1	Atlas
Autocode	Autocod	k1gInSc5	Autocod
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nástroje	nástroj	k1gInPc1	nástroj
se	se	k3xPyFc4	se
však	však	k9	však
značně	značně	k6eAd1	značně
lišily	lišit	k5eAaImAgFnP	lišit
od	od	k7c2	od
moderních	moderní	k2eAgInPc2d1	moderní
generátorů	generátor	k1gInPc2	generátor
překladačů	překladač	k1gInPc2	překladač
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
by	by	kYmCp3nS	by
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
byl	být	k5eAaImAgInS	být
zařazován	zařazovat	k5eAaImNgInS	zařazovat
někam	někam	k6eAd1	někam
mezi	mezi	k7c4	mezi
vysoce	vysoce	k6eAd1	vysoce
přizpůsobitelné	přizpůsobitelný	k2eAgInPc4d1	přizpůsobitelný
obecné	obecný	k2eAgInPc4d1	obecný
překladače	překladač	k1gInPc4	překladač
a	a	k8xC	a
jazyky	jazyk	k1gInPc4	jazyk
s	s	k7c7	s
rozšiřitelnou	rozšiřitelný	k2eAgFnSc7d1	rozšiřitelná
syntaxí	syntax	k1gFnSc7	syntax
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
název	název	k1gInSc1	název
Compiler-Compiler	Compiler-Compiler	k1gInSc1	Compiler-Compiler
–	–	k?	–
generátor	generátor	k1gInSc1	generátor
překladačů	překladač	k1gInPc2	překladač
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
daleko	daleko	k6eAd1	daleko
přilehavější	přilehavý	k2eAgInSc1d2	přilehavý
pro	pro	k7c4	pro
Brookerův	Brookerův	k2eAgInSc4d1	Brookerův
systém	systém	k1gInSc4	systém
než	než	k8xS	než
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
moderních	moderní	k2eAgInPc2d1	moderní
generátorů	generátor	k1gInPc2	generátor
překladačů	překladač	k1gInPc2	překladač
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
pouhými	pouhý	k2eAgInPc7d1	pouhý
generátory	generátor	k1gInPc7	generátor
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Compiler	Compiler	k1gInSc1	Compiler
Compiler	Compiler	k1gInSc1	Compiler
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
díky	díky	k7c3	díky
programu	program	k1gInSc3	program
Yacc	Yacc	k1gInSc1	Yacc
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
Brookerova	Brookerův	k2eAgInSc2d1	Brookerův
výtvoru	výtvor	k1gInSc2	výtvor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgInPc7d1	další
příklady	příklad	k1gInPc7	příklad
generátorů	generátor	k1gInPc2	generátor
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
programu	program	k1gInSc2	program
Yacc	Yacc	k1gInSc4	Yacc
jsou	být	k5eAaImIp3nP	být
ANTLR	ANTLR	kA	ANTLR
<g/>
,	,	kIx,	,
Coco	Coco	k1gMnSc1	Coco
<g/>
/	/	kIx~	/
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
CUP	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
GNU	gnu	k1gMnSc1	gnu
bison	bison	k1gMnSc1	bison
<g/>
,	,	kIx,	,
Eli	Eli	k1gMnSc1	Eli
<g/>
,	,	kIx,	,
FSL	FSL	kA	FSL
<g/>
,	,	kIx,	,
SableCC	SableCC	k1gFnSc2	SableCC
a	a	k8xC	a
JavaCC	JavaCC	k1gFnSc2	JavaCC
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jsou	být	k5eAaImIp3nP	být
užitečné	užitečný	k2eAgInPc1d1	užitečný
<g/>
,	,	kIx,	,
prosté	prostý	k2eAgInPc1d1	prostý
generátory	generátor	k1gInPc1	generátor
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
řeší	řešit	k5eAaImIp3nP	řešit
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
překladače	překladač	k1gInSc2	překladač
pouze	pouze	k6eAd1	pouze
analýzu	analýza	k1gFnSc4	analýza
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nástroje	nástroj	k1gInPc1	nástroj
se	s	k7c7	s
širším	široký	k2eAgInSc7d2	širší
záběrem	záběr	k1gInSc7	záběr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
PQCC	PQCC	kA	PQCC
<g/>
,	,	kIx,	,
Coco	Coco	k1gMnSc1	Coco
<g/>
/	/	kIx~	/
<g/>
R	R	kA	R
a	a	k8xC	a
DMS	DMS	kA	DMS
Software	software	k1gInSc1	software
Reengineering	Reengineering	k1gInSc1	Reengineering
Toolkit	Toolkit	k1gInSc4	Toolkit
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
významnou	významný	k2eAgFnSc4d1	významná
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
obtížnější	obtížný	k2eAgFnPc4d2	obtížnější
činnosti	činnost	k1gFnPc4	činnost
následující	následující	k2eAgFnPc4d1	následující
po	po	k7c6	po
syntaktické	syntaktický	k2eAgFnSc6d1	syntaktická
analýze	analýza	k1gFnSc6	analýza
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
sémantickou	sémantický	k2eAgFnSc4d1	sémantická
analýzu	analýza	k1gFnSc4	analýza
<g/>
,	,	kIx,	,
generování	generování	k1gNnSc4	generování
a	a	k8xC	a
optimalizaci	optimalizace	k1gFnSc4	optimalizace
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Některé	některý	k3yIgInPc1	některý
generátory	generátor	k1gInPc1	generátor
překladačů	překladač	k1gMnPc2	překladač
==	==	k?	==
</s>
</p>
<p>
<s>
ANTLR	ANTLR	kA	ANTLR
</s>
</p>
<p>
<s>
Bison	Bison	k1gMnSc1	Bison
</s>
</p>
<p>
<s>
Coco	Coco	k1gMnSc1	Coco
<g/>
/	/	kIx~	/
<g/>
R	R	kA	R
</s>
</p>
<p>
<s>
DMS	DMS	kA	DMS
Software	software	k1gInSc1	software
Reengineering	Reengineering	k1gInSc1	Reengineering
Toolkit	Toolkit	k2eAgInSc1d1	Toolkit
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
transformaci	transformace	k1gFnSc4	transformace
programů	program	k1gInPc2	program
s	s	k7c7	s
generátory	generátor	k1gInPc7	generátor
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ELI	ELI	kA	ELI
<g/>
,	,	kIx,	,
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
sada	sada	k1gFnSc1	sada
nástrojů	nástroj	k1gInPc2	nástroj
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
překladačů	překladač	k1gMnPc2	překladač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Grako	Grako	k6eAd1	Grako
<g/>
,	,	kIx,	,
Python	Python	k1gMnSc1	Python
EBNF-to-PEG	EBNFo-PEG	k1gFnSc2	EBNF-to-PEG
generátor	generátor	k1gInSc1	generátor
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lemon	lemon	k1gInSc1	lemon
</s>
</p>
<p>
<s>
META	meta	k1gFnSc1	meta
II	II	kA	II
</s>
</p>
<p>
<s>
parboiled	parboiled	k1gInSc1	parboiled
<g/>
,	,	kIx,	,
Knihovna	knihovna	k1gFnSc1	knihovna
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
Java	Jav	k1gInSc2	Jav
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Packrat	Packrat	k1gMnSc1	Packrat
parser	parser	k1gMnSc1	parser
</s>
</p>
<p>
<s>
PackCC	PackCC	k?	PackCC
<g/>
,	,	kIx,	,
packrat	packrat	k1gMnSc1	packrat
parser	parser	k1gMnSc1	parser
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
levé	levý	k2eAgFnSc2d1	levá
rekurze	rekurze	k1gFnSc2	rekurze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PQCC	PQCC	kA	PQCC
<g/>
,	,	kIx,	,
generátor	generátor	k1gInSc4	generátor
překladačů	překladač	k1gInPc2	překladač
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
není	být	k5eNaImIp3nS	být
jenom	jenom	k9	jenom
generátorem	generátor	k1gInSc7	generátor
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SYNTAX	syntax	k1gFnSc1	syntax
<g/>
,	,	kIx,	,
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
sada	sada	k1gFnSc1	sada
nástrojů	nástroj	k1gInPc2	nástroj
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
překladačů	překladač	k1gMnPc2	překladač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TREEMETA	TREEMETA	kA	TREEMETA
</s>
</p>
<p>
<s>
Yacc	Yacc	k6eAd1	Yacc
</s>
</p>
<p>
<s>
XPL	XPL	kA	XPL
</s>
</p>
<p>
<s>
JavaCC	JavaCC	k?	JavaCC
https://web.archive.org/web/20130608172614/https://javacc.java.net/	[url]	k4	https://web.archive.org/web/20130608172614/https://javacc.java.net/
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Compiler-compiler	Compilerompiler	k1gInSc1	Compiler-compiler
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BROOKER	BROOKER	kA	BROOKER
<g/>
,	,	kIx,	,
R	R	kA	R
.	.	kIx.	.
<g/>
A.	A.	kA	A.
<g/>
;	;	kIx,	;
MACCALLUM	MACCALLUM	kA	MACCALLUM
<g/>
,	,	kIx,	,
I.	I.	kA	I.
R.	R.	kA	R.
<g/>
;	;	kIx,	;
MORRIS	MORRIS	kA	MORRIS
<g/>
,	,	kIx,	,
D.	D.	kA	D.
The	The	k1gMnSc1	The
compiler-compiler	compilerompiler	k1gMnSc1	compiler-compiler
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
3	[number]	k4	3
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brooker	Brooker	k1gMnSc1	Brooker
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Morris	Morris	k1gInSc1	Morris
<g/>
,	,	kIx,	,
D.	D.	kA	D.
a	a	k8xC	a
Rohl	rohnout	k5eAaPmAgInS	rohnout
<g/>
,	,	kIx,	,
J.	J.	kA	J.
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Experience	Experience	k1gFnSc1	Experience
with	with	k1gMnSc1	with
the	the	k?	the
Compiler	Compiler	k1gMnSc1	Compiler
Compiler	Compiler	k1gMnSc1	Compiler
<g/>
,	,	kIx,	,
Computer	computer	k1gInSc1	computer
Journal	Journal	k1gFnSc2	Journal
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
,	,	kIx,	,
p.	p.	k?	p.
350	[number]	k4	350
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
February	Februara	k1gFnSc2	Februara
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
Stephen	Stephen	k2eAgMnSc1d1	Stephen
C.	C.	kA	C.
<g/>
,	,	kIx,	,
Yacc	Yacc	k1gFnSc1	Yacc
<g/>
–	–	k?	–
<g/>
yet	yet	k?	yet
another	anothra	k1gFnPc2	anothra
compiler-compiler	compilerompiler	k1gMnSc1	compiler-compiler
<g/>
,	,	kIx,	,
Computer	computer	k1gInSc1	computer
Science	Science	k1gFnSc2	Science
Technical	Technical	k1gFnSc2	Technical
Report	report	k1gInSc1	report
32	[number]	k4	32
<g/>
,	,	kIx,	,
Bell	bell	k1gInSc1	bell
Laboratories	Laboratoriesa	k1gFnPc2	Laboratoriesa
<g/>
,	,	kIx,	,
Murray	Murraa	k1gFnPc1	Murraa
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
NJ	NJ	kA	NJ
<g/>
,	,	kIx,	,
July	Jula	k1gFnSc2	Jula
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
MCKEEMAN	MCKEEMAN	kA	MCKEEMAN
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
M.	M.	kA	M.
<g/>
;	;	kIx,	;
HORNING	HORNING	kA	HORNING
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
J.	J.	kA	J.
<g/>
;	;	kIx,	;
WORTMAN	WORTMAN	kA	WORTMAN
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
B.	B.	kA	B.
A	A	kA	A
Compiler	Compiler	k1gMnSc1	Compiler
Generator	Generator	k1gMnSc1	Generator
<g/>
.	.	kIx.	.
</s>
<s>
Englewood	Englewood	k1gInSc1	Englewood
Cliffs	Cliffs	k1gInSc1	Cliffs
<g/>
,	,	kIx,	,
N.	N.	kA	N.
<g/>
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Prentice-Hall	Prentice-Hall	k1gMnSc1	Prentice-Hall
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
155077	[number]	k4	155077
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Parsing	Parsing	k1gInSc1	Parsing
výraz	výraz	k1gInSc1	výraz
gramatika	gramatika	k1gFnSc1	gramatika
</s>
</p>
<p>
<s>
LL	LL	kA	LL
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
analyzátor	analyzátor	k1gInSc4	analyzátor
</s>
</p>
<p>
<s>
LR	LR	kA	LR
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
analyzátor	analyzátor	k1gInSc4	analyzátor
</s>
</p>
<p>
<s>
Jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
LR	LR	kA	LR
syntaktický	syntaktický	k2eAgInSc1d1	syntaktický
analyzátor	analyzátor	k1gInSc1	analyzátor
</s>
</p>
<p>
<s>
LALR	LALR	kA	LALR
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
analyzátor	analyzátor	k1gInSc4	analyzátor
</s>
</p>
<p>
<s>
GLR	GLR	kA	GLR
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
analyzátor	analyzátor	k1gInSc4	analyzátor
</s>
</p>
<p>
<s>
Metacompiler	Metacompiler	k1gMnSc1	Metacompiler
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Computer	computer	k1gInSc1	computer
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
Brooker	Brooker	k1gMnSc1	Brooker
Autocodes	Autocodes	k1gMnSc1	Autocodes
</s>
</p>
<p>
<s>
Catalog	Catalog	k1gInSc1	Catalog
<g/>
.	.	kIx.	.
<g/>
compilertools	compilertools	k1gInSc1	compilertools
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
,	,	kIx,	,
Katalog	katalog	k1gInSc1	katalog
nástrojů	nástroj	k1gInPc2	nástroj
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
překladačů	překladač	k1gInPc2	překladač
</s>
</p>
<p>
<s>
Labraj	Labraj	k1gFnSc1	Labraj
<g/>
.	.	kIx.	.
<g/>
uni-mb	unib	k1gInSc1	uni-mb
<g/>
.	.	kIx.	.
<g/>
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
Lisa	Lisa	k1gFnSc1	Lisa
</s>
</p>
<p>
<s>
Skenz	Skenz	k1gInSc1	Skenz
<g/>
.	.	kIx.	.
<g/>
it	it	k?	it
<g/>
,	,	kIx,	,
Nástroje	nástroj	k1gInPc1	nástroj
Jflex	Jflex	k1gInSc1	Jflex
a	a	k8xC	a
Cup	cup	k1gInSc1	cup
</s>
</p>
<p>
<s>
Gentle	Gentle	k1gFnSc1	Gentle
<g/>
.	.	kIx.	.
<g/>
compilertools	compilertools	k1gInSc1	compilertools
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
,	,	kIx,	,
Systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
překladačů	překladač	k1gMnPc2	překladač
Gentle	Gentle	k1gFnSc2	Gentle
</s>
</p>
<p>
<s>
Accent	Accent	k1gInSc1	Accent
<g/>
.	.	kIx.	.
<g/>
compilertools	compilertools	k1gInSc1	compilertools
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
,	,	kIx,	,
Accent	Accent	k1gInSc1	Accent
<g/>
:	:	kIx,	:
Překladač	překladač	k1gInSc1	překladač
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
třídu	třída	k1gFnSc4	třída
bezkontextových	bezkontextový	k2eAgInPc2d1	bezkontextový
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
Grammatica	Grammatica	k1gFnSc1	Grammatica
<g/>
.	.	kIx.	.
<g/>
percederberg	percederberg	k1gInSc1	percederberg
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
Generátor	generátor	k1gInSc1	generátor
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
analyzátorů	analyzátor	k1gInPc2	analyzátor
pro	pro	k7c4	pro
.	.	kIx.	.
<g/>
NET	NET	kA	NET
a	a	k8xC	a
Java	Java	k1gFnSc1	Java
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
zdrojovým	zdrojový	k2eAgInSc7d1	zdrojový
kódem	kód	k1gInSc7	kód
</s>
</p>
