<s>
První	první	k4xOgInSc1	první
generátor	generátor	k1gInSc1	generátor
překladačů	překladač	k1gInPc2	překladač
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
používal	používat	k5eAaImAgMnS	používat
jméno	jméno	k1gNnSc4	jméno
Compiler-Compiler	Compiler-Compiler	k1gMnSc1	Compiler-Compiler
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Tony	Tony	k1gMnSc1	Tony
Brooker	Brooker	k1gMnSc1	Brooker
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgMnS	sloužit
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
překladače	překladač	k1gInSc2	překladač
pro	pro	k7c4	pro
počítač	počítač	k1gInSc4	počítač
Atlas	Atlas	k1gInSc1	Atlas
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
překladače	překladač	k1gInSc2	překladač
Atlas	Atlas	k1gInSc1	Atlas
Autocode	Autocod	k1gInSc5	Autocod
<g/>
.	.	kIx.	.
</s>
