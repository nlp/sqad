<s>
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
nukleozom	nukleozom	k1gInSc1	nukleozom
<g/>
,	,	kIx,	,
struktura	struktura	k1gFnSc1	struktura
tvořená	tvořený	k2eAgFnSc1d1	tvořená
histonovými	histonový	k2eAgFnPc7d1	histonový
molekulami	molekula	k1gFnPc7	molekula
omotanými	omotaný	k2eAgFnPc7d1	omotaná
vláknem	vlákno	k1gNnSc7	vlákno
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
asi	asi	k9	asi
80	[number]	k4	80
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
