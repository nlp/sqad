<s>
Tento	tento	k3xDgInSc1	tento
komplex	komplex	k1gInSc1	komplex
DNA	dno	k1gNnSc2	dno
a	a	k8xC	a
bílkovin	bílkovina	k1gFnPc2	bílkovina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
chromatin	chromatin	k1gInSc1	chromatin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
chromozomu	chromozom	k1gInSc2	chromozom
se	s	k7c7	s
strukturní	strukturní	k2eAgFnSc7d1	strukturní
funkcí	funkce	k1gFnSc7	funkce
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
chromatinu	chromatin	k1gInSc2	chromatin
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
nukleozom	nukleozom	k1gInSc1	nukleozom
<g/>
,	,	kIx,	,
struktura	struktura	k1gFnSc1	struktura
tvořená	tvořený	k2eAgFnSc1d1	tvořená
histonovými	histonový	k2eAgFnPc7d1	histonový
molekulami	molekula	k1gFnPc7	molekula
omotanými	omotaný	k2eAgFnPc7d1	omotaná
vláknem	vlákno	k1gNnSc7	vlákno
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
asi	asi	k9	asi
80	[number]	k4	80
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>

