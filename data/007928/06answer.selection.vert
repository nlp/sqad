<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
infekcí	infekce	k1gFnPc2	infekce
bakteriemi	bakterie	k1gFnPc7	bakterie
nebo	nebo	k8xC	nebo
viry	vir	k1gInPc7	vir
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
pak	pak	k6eAd1	pak
dalšími	další	k2eAgInPc7d1	další
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
<g/>
,	,	kIx,	,
některými	některý	k3yIgInPc7	některý
léky	lék	k1gInPc7	lék
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgFnPc7d1	jiná
chorobami	choroba	k1gFnPc7	choroba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
autoimunitní	autoimunitní	k2eAgNnSc1d1	autoimunitní
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
