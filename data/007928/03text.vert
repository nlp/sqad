<s>
Zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
neboli	neboli	k8xC	neboli
pneumonie	pneumonie	k1gFnSc2	pneumonie
je	být	k5eAaImIp3nS	být
zánětlivé	zánětlivý	k2eAgNnSc1d1	zánětlivé
onemocnění	onemocnění	k1gNnSc1	onemocnění
plic	plíce	k1gFnPc2	plíce
postihující	postihující	k2eAgNnSc4d1	postihující
zejména	zejména	k9	zejména
mikroskopické	mikroskopický	k2eAgFnPc1d1	mikroskopická
dutinky	dutinka	k1gFnPc1	dutinka
naplněné	naplněný	k2eAgFnPc1d1	naplněná
vzduchem	vzduch	k1gInSc7	vzduch
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
plicní	plicní	k2eAgFnPc1d1	plicní
sklípky	sklípek	k1gInPc4	sklípek
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
infekcí	infekce	k1gFnPc2	infekce
bakteriemi	bakterie	k1gFnPc7	bakterie
nebo	nebo	k8xC	nebo
viry	vir	k1gInPc7	vir
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
pak	pak	k6eAd1	pak
dalšími	další	k2eAgInPc7d1	další
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
<g/>
,	,	kIx,	,
některými	některý	k3yIgInPc7	některý
léky	lék	k1gInPc7	lék
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgFnPc7d1	jiná
chorobami	choroba	k1gFnPc7	choroba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
autoimunitní	autoimunitní	k2eAgNnSc1d1	autoimunitní
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
typickým	typický	k2eAgInPc3d1	typický
symptomům	symptom	k1gInPc3	symptom
patří	patřit	k5eAaImIp3nS	patřit
kašel	kašel	k1gInSc1	kašel
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc1	bolest
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
<g/>
,	,	kIx,	,
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
obtíže	obtíž	k1gFnPc1	obtíž
při	při	k7c6	při
dýchání	dýchání	k1gNnSc6	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
bývá	bývat	k5eAaImIp3nS	bývat
stanovena	stanovit	k5eAaPmNgFnS	stanovit
zejména	zejména	k9	zejména
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
RTG	RTG	kA	RTG
snímkování	snímkování	k1gNnSc2	snímkování
a	a	k8xC	a
kultivačního	kultivační	k2eAgNnSc2d1	kultivační
vyšetření	vyšetření	k1gNnSc2	vyšetření
sputa	sputum	k1gNnSc2	sputum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vakcíny	vakcína	k1gFnPc4	vakcína
zabraňující	zabraňující	k2eAgFnPc4d1	zabraňující
vzniku	vznik	k1gInSc3	vznik
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
původní	původní	k2eAgFnSc6d1	původní
příčině	příčina	k1gFnSc6	příčina
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládaný	předpokládaný	k2eAgInSc1d1	předpokládaný
bakteriální	bakteriální	k2eAgInSc1d1	bakteriální
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
je	být	k5eAaImIp3nS	být
léčen	léčit	k5eAaImNgMnS	léčit
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
choroba	choroba	k1gFnSc1	choroba
závažný	závažný	k2eAgInSc4d1	závažný
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
nemocný	nemocný	k2eAgMnSc1d1	nemocný
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
je	být	k5eAaImIp3nS	být
pneumonií	pneumonie	k1gFnSc7	pneumonie
postiženo	postihnout	k5eAaPmNgNnS	postihnout
kolem	kolem	k6eAd1	kolem
450	[number]	k4	450
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
sedm	sedm	k4xCc4	sedm
procent	procento	k1gNnPc2	procento
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
;	;	kIx,	;
na	na	k7c4	na
následky	následek	k1gInPc4	následek
nemoci	nemoc	k1gFnSc2	nemoc
umírají	umírat	k5eAaImIp3nP	umírat
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
milióny	milión	k4xCgInPc4	milión
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
William	William	k1gInSc1	William
Osler	Oslra	k1gFnPc2	Oslra
označil	označit	k5eAaPmAgInS	označit
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
za	za	k7c7	za
"	"	kIx"	"
<g/>
kapitána	kapitán	k1gMnSc2	kapitán
pěšáků	pěšák	k1gMnPc2	pěšák
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
díky	dík	k1gInPc4	dík
nástupu	nástup	k1gInSc2	nástup
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
a	a	k8xC	a
vakcín	vakcína	k1gFnPc2	vakcína
ve	v	k7c6	v
století	století	k1gNnSc6	století
20	[number]	k4	20
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
přežití	přežití	k1gNnSc2	přežití
podstatně	podstatně	k6eAd1	podstatně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
staršími	starý	k2eAgInPc7d2	starší
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
mladými	mladý	k2eAgInPc7d1	mladý
a	a	k8xC	a
chronicky	chronicky	k6eAd1	chronicky
nemocnými	mocný	k2eNgMnPc7d1	nemocný
pacienty	pacient	k1gMnPc7	pacient
však	však	k9	však
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
příčin	příčina	k1gFnPc2	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
postižení	postižení	k1gNnSc2	postižení
infekčním	infekční	k2eAgInSc7d1	infekční
zápalem	zápal	k1gInSc7	zápal
plic	plíce	k1gFnPc2	plíce
často	často	k6eAd1	často
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
produktivní	produktivní	k2eAgInSc4d1	produktivní
kašel	kašel	k1gInSc4	kašel
<g/>
,	,	kIx,	,
horečku	horečka	k1gFnSc4	horečka
doprovázenou	doprovázený	k2eAgFnSc7d1	doprovázená
zimnicí	zimnice	k1gFnSc7	zimnice
<g/>
,	,	kIx,	,
dušnost	dušnost	k1gFnSc4	dušnost
<g/>
,	,	kIx,	,
ostrou	ostrý	k2eAgFnSc4d1	ostrá
nebo	nebo	k8xC	nebo
bodavou	bodavý	k2eAgFnSc4d1	bodavá
bolest	bolest	k1gFnSc4	bolest
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
při	při	k7c6	při
hlubokém	hluboký	k2eAgInSc6d1	hluboký
nádechu	nádech	k1gInSc6	nádech
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
dechovou	dechový	k2eAgFnSc4d1	dechová
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
pacientů	pacient	k1gMnPc2	pacient
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nejzřetelnějším	zřetelný	k2eAgInSc7d3	nejzřetelnější
příznakem	příznak	k1gInSc7	příznak
zmatenost	zmatenost	k1gFnSc1	zmatenost
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgInPc1d1	typický
příznaky	příznak	k1gInPc1	příznak
a	a	k8xC	a
symptomy	symptom	k1gInPc1	symptom
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
mladších	mladý	k2eAgNnPc2d2	mladší
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
pak	pak	k6eAd1	pak
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
horečku	horečka	k1gFnSc4	horečka
<g/>
,	,	kIx,	,
kašel	kašel	k1gInSc4	kašel
a	a	k8xC	a
zrychlené	zrychlený	k2eAgNnSc4d1	zrychlené
nebo	nebo	k8xC	nebo
obtížné	obtížný	k2eAgNnSc4d1	obtížné
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
chorobu	choroba	k1gFnSc4	choroba
příliš	příliš	k6eAd1	příliš
specifická	specifický	k2eAgFnSc1d1	specifická
(	(	kIx(	(
<g/>
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgFnPc2d1	další
běžných	běžný	k2eAgFnPc2d1	běžná
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
trpí	trpět	k5eAaImIp3nP	trpět
závažnou	závažný	k2eAgFnSc7d1	závažná
formou	forma	k1gFnSc7	forma
nemoci	nemoc	k1gFnSc2	nemoc
nebo	nebo	k8xC	nebo
podvýživou	podvýživa	k1gFnSc7	podvýživa
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
zcela	zcela	k6eAd1	zcela
chybět	chybět	k5eAaImF	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
mladší	mladý	k2eAgFnSc2d2	mladší
2	[number]	k4	2
měsíců	měsíc	k1gInPc2	měsíc
často	často	k6eAd1	často
navíc	navíc	k6eAd1	navíc
nevykazují	vykazovat	k5eNaImIp3nP	vykazovat
ani	ani	k8xC	ani
kašel	kašel	k1gInSc4	kašel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
závažnějším	závažný	k2eAgInPc3d2	závažnější
příznakům	příznak	k1gInPc3	příznak
a	a	k8xC	a
symptomům	symptom	k1gInPc3	symptom
lze	lze	k6eAd1	lze
počítat	počítat	k5eAaImF	počítat
promodralou	promodralý	k2eAgFnSc4d1	promodralá
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
snížený	snížený	k2eAgInSc4d1	snížený
pocit	pocit	k1gInSc4	pocit
žízně	žízeň	k1gFnSc2	žízeň
<g/>
,	,	kIx,	,
křeče	křeč	k1gFnPc4	křeč
<g/>
,	,	kIx,	,
přetrvávající	přetrvávající	k2eAgNnSc4d1	přetrvávající
zvracení	zvracení	k1gNnSc4	zvracení
<g/>
,	,	kIx,	,
extrémně	extrémně	k6eAd1	extrémně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
teploty	teplota	k1gFnPc1	teplota
nebo	nebo	k8xC	nebo
poruchy	poruch	k1gInPc1	poruch
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bakteriální	bakteriální	k2eAgNnSc1d1	bakteriální
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
virová	virový	k2eAgFnSc1d1	virová
pneumonie	pneumonie	k1gFnSc1	pneumonie
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
provázeny	provázen	k2eAgInPc4d1	provázen
podobnými	podobný	k2eAgInPc7d1	podobný
příznaky	příznak	k1gInPc7	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
klasickými	klasický	k2eAgInPc7d1	klasický
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nespecifickými	specifický	k2eNgFnPc7d1	nespecifická
klinickými	klinický	k2eAgFnPc7d1	klinická
charakteristikami	charakteristika	k1gFnPc7	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
Pneumonii	pneumonie	k1gFnSc4	pneumonie
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
bakterií	bakterie	k1gFnSc7	bakterie
Legionella	Legionello	k1gNnSc2	Legionello
může	moct	k5eAaImIp3nS	moct
doprovázet	doprovázet	k5eAaImF	doprovázet
abdominální	abdominální	k2eAgFnSc4d1	abdominální
bolest	bolest	k1gFnSc4	bolest
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc4	průjem
či	či	k8xC	či
zmatenost	zmatenost	k1gFnSc4	zmatenost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
vyvolaný	vyvolaný	k2eAgMnSc1d1	vyvolaný
streptokokem	streptokok	k1gMnSc7	streptokok
Streptococcus	Streptococcus	k1gInSc4	Streptococcus
pneumoniae	pneumonia	k1gFnSc2	pneumonia
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
vykašláváním	vykašlávání	k1gNnSc7	vykašlávání
rezavě	rezavě	k6eAd1	rezavě
zbarveného	zbarvený	k2eAgNnSc2d1	zbarvené
sputa	sputum	k1gNnSc2	sputum
<g/>
;	;	kIx,	;
pneumonie	pneumonie	k1gFnSc1	pneumonie
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
bakterií	bakterie	k1gFnSc7	bakterie
Klebsiella	Klebsiello	k1gNnSc2	Klebsiello
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
vykašláváním	vykašlávání	k1gNnSc7	vykašlávání
sputa	sputum	k1gNnSc2	sputum
krvavého	krvavý	k2eAgNnSc2d1	krvavé
(	(	kIx(	(
<g/>
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
anglosaských	anglosaský	k2eAgFnPc6d1	anglosaská
zemích	zem	k1gFnPc6	zem
často	často	k6eAd1	často
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
rybízový	rybízový	k2eAgInSc1d1	rybízový
džem	džem	k1gInSc1	džem
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krvavé	krvavý	k2eAgNnSc1d1	krvavé
sputum	sputum	k1gNnSc1	sputum
(	(	kIx(	(
<g/>
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
hemoptýza	hemoptýza	k1gFnSc1	hemoptýza
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
u	u	k7c2	u
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
pneumonie	pneumonie	k1gFnSc2	pneumonie
vyvolané	vyvolaný	k2eAgInPc1d1	vyvolaný
gramnegativními	gramnegativní	k2eAgFnPc7d1	gramnegativní
bakteriemi	bakterie	k1gFnPc7	bakterie
a	a	k8xC	a
plicních	plicní	k2eAgInPc2d1	plicní
abscesů	absces	k1gInPc2	absces
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
ale	ale	k8xC	ale
u	u	k7c2	u
akutní	akutní	k2eAgFnSc2d1	akutní
bronchitidy	bronchitida	k1gFnSc2	bronchitida
<g/>
.	.	kIx.	.
</s>
<s>
Mykoplazmatický	Mykoplazmatický	k2eAgInSc1d1	Mykoplazmatický
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
otokem	otok	k1gInSc7	otok
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
na	na	k7c6	na
krku	krk	k1gInSc6	krk
<g/>
,	,	kIx,	,
bolestí	bolestit	k5eAaImIp3nP	bolestit
kloubů	kloub	k1gInPc2	kloub
nebo	nebo	k8xC	nebo
zánětem	zánět	k1gInSc7	zánět
středního	střední	k2eAgNnSc2d1	střední
ucha	ucho	k1gNnSc2	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Virový	virový	k2eAgInSc1d1	virový
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
je	být	k5eAaImIp3nS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
sípáním	sípání	k1gNnSc7	sípání
častěji	často	k6eAd2	často
než	než	k8xS	než
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
pneumonie	pneumonie	k1gFnSc2	pneumonie
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zápalu	zápal	k1gInSc3	zápal
plic	plíce	k1gFnPc2	plíce
dochází	docházet	k5eAaImIp3nS	docházet
zejména	zejména	k9	zejména
na	na	k7c6	na
základě	základ	k1gInSc6	základ
infekce	infekce	k1gFnPc4	infekce
způsobené	způsobený	k2eAgFnPc1d1	způsobená
bakteriemi	bakterie	k1gFnPc7	bakterie
či	či	k8xC	či
viry	vir	k1gInPc7	vir
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
plísněmi	plíseň	k1gFnPc7	plíseň
a	a	k8xC	a
parazity	parazit	k1gMnPc7	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
bylo	být	k5eAaImAgNnS	být
identifikováno	identifikovat	k5eAaBmNgNnS	identifikovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
kmenů	kmen	k1gInPc2	kmen
infekčních	infekční	k2eAgInPc2d1	infekční
činitelů	činitel	k1gInPc2	činitel
<g/>
,	,	kIx,	,
za	za	k7c7	za
většinou	většina	k1gFnSc7	většina
případů	případ	k1gInPc2	případ
nemoci	nemoc	k1gFnSc3	nemoc
stojí	stát	k5eAaImIp3nS	stát
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Smíšené	smíšený	k2eAgFnPc1d1	smíšená
infekce	infekce	k1gFnPc1	infekce
způsobené	způsobený	k2eAgFnSc2d1	způsobená
viry	vira	k1gFnSc2	vira
i	i	k8xC	i
bakteriemi	bakterie	k1gFnPc7	bakterie
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
až	až	k9	až
ve	v	k7c6	v
45	[number]	k4	45
%	%	kIx~	%
onemocnění	onemocnění	k1gNnSc1	onemocnění
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
15	[number]	k4	15
%	%	kIx~	%
onemocnění	onemocnění	k1gNnPc4	onemocnění
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
choroby	choroba	k1gFnSc2	choroba
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
polovině	polovina	k1gFnSc6	polovina
případů	případ	k1gInPc2	případ
navzdory	navzdory	k7c3	navzdory
pečlivému	pečlivý	k2eAgNnSc3d1	pečlivé
testování	testování	k1gNnSc3	testování
vůbec	vůbec	k9	vůbec
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
<g/>
.	.	kIx.	.
</s>
<s>
Pojmem	pojem	k1gInSc7	pojem
pneumonie	pneumonie	k1gFnSc2	pneumonie
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
měřítku	měřítko	k1gNnSc6	měřítko
označováno	označovat	k5eAaImNgNnS	označovat
jakékoli	jakýkoli	k3yIgNnSc1	jakýkoli
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
následkem	následek	k1gInSc7	následek
je	být	k5eAaImIp3nS	být
zánět	zánět	k1gInSc1	zánět
plic	plíce	k1gFnPc2	plíce
(	(	kIx(	(
<g/>
způsobený	způsobený	k2eAgMnSc1d1	způsobený
například	například	k6eAd1	například
autoimunitním	autoimunitní	k2eAgNnPc3d1	autoimunitní
onemocněním	onemocnění	k1gNnPc3	onemocnění
<g/>
,	,	kIx,	,
chemickým	chemický	k2eAgNnSc7d1	chemické
popálením	popálení	k1gNnSc7	popálení
nebo	nebo	k8xC	nebo
působením	působení	k1gNnSc7	působení
léků	lék	k1gInPc2	lék
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
zánětu	zánět	k1gInSc3	zánět
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
přesnější	přesný	k2eAgNnSc1d2	přesnější
označení	označení	k1gNnSc1	označení
pneumonitida	pneumonitida	k1gFnSc1	pneumonitida
<g/>
.	.	kIx.	.
</s>
<s>
Původci	původce	k1gMnPc1	původce
infekce	infekce	k1gFnSc2	infekce
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
na	na	k7c6	na
základě	základ	k1gInSc6	základ
předpokládaných	předpokládaný	k2eAgInPc2d1	předpokládaný
projevů	projev	k1gInPc2	projev
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
na	na	k7c4	na
typické	typický	k2eAgInPc4d1	typický
a	a	k8xC	a
netypické	typický	k2eNgMnPc4d1	netypický
<g/>
,	,	kIx,	,
důkazy	důkaz	k1gInPc4	důkaz
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc4	tento
rozlišení	rozlišení	k1gNnSc4	rozlišení
nepotvrdily	potvrdit	k5eNaPmAgInP	potvrdit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
upustilo	upustit	k5eAaPmAgNnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Choroby	choroba	k1gFnPc1	choroba
a	a	k8xC	a
rizikové	rizikový	k2eAgInPc1d1	rizikový
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
náchylnost	náchylnost	k1gFnSc4	náchylnost
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
zápalem	zápal	k1gInSc7	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
kouření	kouření	k1gNnSc4	kouření
<g/>
,	,	kIx,	,
sníženou	snížený	k2eAgFnSc4d1	snížená
imunitu	imunita	k1gFnSc4	imunita
<g/>
,	,	kIx,	,
alkoholismus	alkoholismus	k1gInSc4	alkoholismus
<g/>
,	,	kIx,	,
chronickou	chronický	k2eAgFnSc4d1	chronická
obstrukční	obstrukční	k2eAgFnSc4d1	obstrukční
plicní	plicní	k2eAgFnSc4d1	plicní
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
chronické	chronický	k2eAgNnSc1d1	chronické
onemocnění	onemocnění	k1gNnSc1	onemocnění
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
choroby	choroba	k1gFnSc2	choroba
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Užívání	užívání	k1gNnSc1	užívání
léků	lék	k1gInPc2	lék
potlačujících	potlačující	k2eAgInPc2d1	potlačující
tvorbu	tvorba	k1gFnSc4	tvorba
žaludeční	žaludeční	k2eAgFnSc2d1	žaludeční
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
inhibitorů	inhibitor	k1gInPc2	inhibitor
protonové	protonový	k2eAgFnSc2d1	protonová
pumpy	pumpa	k1gFnSc2	pumpa
nebo	nebo	k8xC	nebo
blokátorů	blokátor	k1gInPc2	blokátor
H2	H2	k1gFnPc2	H2
receptorů	receptor	k1gInPc2	receptor
<g/>
,	,	kIx,	,
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
rizikem	riziko	k1gNnSc7	riziko
vzniku	vznik	k1gInSc2	vznik
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
také	také	k9	také
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
vyšší	vysoký	k2eAgInSc4d2	vyšší
věk	věk	k1gInSc4	věk
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
příčinou	příčina	k1gFnSc7	příčina
komunitně	komunitně	k6eAd1	komunitně
získané	získaný	k2eAgFnSc2d1	získaná
pneumonie	pneumonie	k1gFnSc2	pneumonie
(	(	kIx(	(
<g/>
CAP	cap	k1gMnSc1	cap
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
v	v	k7c4	v
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
%	%	kIx~	%
případů	případ	k1gInPc2	případ
bakterií	bakterie	k1gFnPc2	bakterie
Streptococcus	Streptococcus	k1gInSc1	Streptococcus
pneumoniae	pneumonia	k1gInSc2	pneumonia
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
často	často	k6eAd1	často
izolovaným	izolovaný	k2eAgNnPc3d1	izolované
bakteriím	bakterium	k1gNnPc3	bakterium
patří	patřit	k5eAaImIp3nS	patřit
Haemophilus	Haemophilus	k1gInSc1	Haemophilus
influenzae	influenza	k1gFnSc2	influenza
ve	v	k7c6	v
20	[number]	k4	20
%	%	kIx~	%
<g/>
,	,	kIx,	,
Chlamydophila	Chlamydophila	k1gFnSc1	Chlamydophila
pneumoniae	pneumoniaat	k5eAaPmIp3nS	pneumoniaat
ve	v	k7c6	v
13	[number]	k4	13
%	%	kIx~	%
a	a	k8xC	a
Mycoplasma	Mycoplasma	k1gFnSc1	Mycoplasma
pneumoniae	pneumoniaat	k5eAaPmIp3nS	pneumoniaat
ve	v	k7c6	v
3	[number]	k4	3
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
;	;	kIx,	;
jinými	jiný	k2eAgMnPc7d1	jiný
původci	původce	k1gMnPc7	původce
jsou	být	k5eAaImIp3nP	být
bakterie	bakterie	k1gFnSc1	bakterie
Staphylococcus	Staphylococcus	k1gInSc4	Staphylococcus
aureus	aureus	k1gInSc1	aureus
<g/>
;	;	kIx,	;
Moraxella	Moraxella	k1gFnSc1	Moraxella
catarrhalis	catarrhalis	k1gFnSc1	catarrhalis
<g/>
;	;	kIx,	;
Legionella	Legionella	k1gFnSc1	Legionella
pneumophila	pneumophila	k1gFnSc1	pneumophila
a	a	k8xC	a
gramnegativní	gramnegativní	k2eAgInPc1d1	gramnegativní
bacily	bacil	k1gInPc1	bacil
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
ale	ale	k9	ale
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
léčivům	léčivo	k1gNnPc3	léčivo
odolné	odolný	k2eAgFnPc1d1	odolná
varianty	varianta	k1gFnSc2	varianta
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
bakterií	bakterie	k1gFnPc2	bakterie
včetně	včetně	k7c2	včetně
lékům	lék	k1gInPc3	lék
odolných	odolný	k2eAgFnPc2d1	odolná
bakterií	bakterie	k1gFnPc2	bakterie
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pneumoniae	pneumonia	k1gFnSc2	pneumonia
(	(	kIx(	(
<g/>
DRSP	DRSP	kA	DRSP
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
meticilin	meticilin	k1gInSc4	meticilin
rezistentního	rezistentní	k2eAgMnSc2d1	rezistentní
stafylokoka	stafylokok	k1gMnSc2	stafylokok
Staphylococcus	Staphylococcus	k1gMnSc1	Staphylococcus
aureus	aureus	k1gMnSc1	aureus
(	(	kIx(	(
<g/>
MRSA	MRSA	kA	MRSA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šíření	šíření	k1gNnSc4	šíření
těchto	tento	k3xDgInPc2	tento
organismů	organismus	k1gInPc2	organismus
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
rizikové	rizikový	k2eAgInPc1d1	rizikový
faktory	faktor	k1gInPc1	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Alkoholismus	alkoholismus	k1gInSc1	alkoholismus
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
působení	působení	k1gNnSc4	působení
bakterieStreptococcus	bakterieStreptococcus	k1gMnSc1	bakterieStreptococcus
pneumoniae	pneumonia	k1gInSc2	pneumonia
<g/>
,	,	kIx,	,
anaerobních	anaerobní	k2eAgInPc2d1	anaerobní
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
tuberculosis	tuberculosis	k1gFnSc2	tuberculosis
<g/>
;	;	kIx,	;
kouření	kouření	k1gNnSc1	kouření
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
účinkyStreptococcus	účinkyStreptococcus	k1gInSc4	účinkyStreptococcus
pneumoniae	pneumonia	k1gFnSc2	pneumonia
<g/>
,	,	kIx,	,
Haemophilus	Haemophilus	k1gMnSc1	Haemophilus
influenzae	influenzae	k1gFnSc1	influenzae
<g/>
,	,	kIx,	,
Moraxella	Moraxella	k1gFnSc1	Moraxella
catarrhalis	catarrhalis	k1gFnSc1	catarrhalis
a	a	k8xC	a
Legionella	Legionella	k1gFnSc1	Legionella
pneumophila	pneumophila	k1gFnSc1	pneumophila
<g/>
.	.	kIx.	.
</s>
<s>
Blízký	blízký	k2eAgInSc1d1	blízký
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
ptáky	pták	k1gMnPc7	pták
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
přenosem	přenos	k1gInSc7	přenos
Chlamydia	Chlamydium	k1gNnSc2	Chlamydium
psittaci	psittace	k1gFnSc4	psittace
<g/>
,	,	kIx,	,
styk	styk	k1gInSc4	styk
s	s	k7c7	s
hospodářskými	hospodářský	k2eAgNnPc7d1	hospodářské
zvířaty	zvíře	k1gNnPc7	zvíře
pak	pak	k8xC	pak
s	s	k7c7	s
nákazou	nákaza	k1gFnSc7	nákaza
Coxiella	Coxiell	k1gMnSc2	Coxiell
burnetti	burnetti	k1gNnSc2	burnetti
<g/>
;	;	kIx,	;
vdechnutí	vdechnutí	k1gNnSc2	vdechnutí
obsahu	obsah	k1gInSc2	obsah
žaludku	žaludek	k1gInSc2	žaludek
je	být	k5eAaImIp3nS	být
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
infekcí	infekce	k1gFnSc7	infekce
anaerobními	anaerobní	k2eAgInPc7d1	anaerobní
organismy	organismus	k1gInPc7	organismus
a	a	k8xC	a
cystická	cystický	k2eAgFnSc1d1	cystická
fibróza	fibróza	k1gFnSc1	fibróza
s	s	k7c7	s
šířením	šíření	k1gNnSc7	šíření
Pseudomonas	Pseudomonasa	k1gFnPc2	Pseudomonasa
aeruginosa	aeruginosa	k1gFnSc1	aeruginosa
a	a	k8xC	a
Staphylococcus	Staphylococcus	k1gMnSc1	Staphylococcus
aureus	aureus	k1gMnSc1	aureus
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
stafylokokem	stafylokok	k1gMnSc7	stafylokok
Streptococcus	Streptococcus	k1gInSc4	Streptococcus
pneumoniae	pneumonia	k1gFnSc2	pneumonia
je	být	k5eAaImIp3nS	být
běžnější	běžný	k2eAgNnSc1d2	běžnější
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
zjišťována	zjišťovat	k5eAaImNgFnS	zjišťovat
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vdechly	vdechnout	k5eAaPmAgFnP	vdechnout
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
anaerobních	anaerobní	k2eAgInPc2d1	anaerobní
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Viry	vir	k1gInPc1	vir
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
přibližně	přibližně	k6eAd1	přibližně
třetinu	třetina	k1gFnSc4	třetina
a	a	k8xC	a
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
asi	asi	k9	asi
15	[number]	k4	15
%	%	kIx~	%
případů	případ	k1gInPc2	případ
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
běžným	běžný	k2eAgMnPc3d1	běžný
původcům	původce	k1gMnPc3	původce
patří	patřit	k5eAaImIp3nS	patřit
rhinoviry	rhinovir	k1gInPc1	rhinovir
<g/>
,	,	kIx,	,
koronaviry	koronavira	k1gFnPc1	koronavira
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
influenzy	influenza	k1gFnSc2	influenza
<g/>
,	,	kIx,	,
respirační	respirační	k2eAgInSc1d1	respirační
syncytiální	syncytiální	k2eAgInSc1d1	syncytiální
virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
RSV	RSV	kA	RSV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
adenoviry	adenovira	k1gFnPc4	adenovira
a	a	k8xC	a
virus	virus	k1gInSc4	virus
parainfluenzy	parainfluenza	k1gFnSc2	parainfluenza
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
Herpes	herpes	k1gInSc1	herpes
simplex	simplex	k1gInSc4	simplex
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
,	,	kIx,	,
osob	osoba	k1gFnPc2	osoba
trpících	trpící	k2eAgFnPc2d1	trpící
rakovinou	rakovina	k1gFnSc7	rakovina
<g/>
,	,	kIx,	,
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
podstoupili	podstoupit	k5eAaPmAgMnP	podstoupit
transplantaci	transplantace	k1gFnSc4	transplantace
orgánu	orgán	k1gInSc2	orgán
<g/>
,	,	kIx,	,
a	a	k8xC	a
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
těžkými	těžký	k2eAgFnPc7d1	těžká
popáleninami	popálenina	k1gFnPc7	popálenina
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
po	po	k7c6	po
transplantaci	transplantace	k1gFnSc6	transplantace
orgánu	orgán	k1gInSc2	orgán
nebo	nebo	k8xC	nebo
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
trpí	trpět	k5eAaImIp3nP	trpět
sníženou	snížený	k2eAgFnSc7d1	snížená
imunitou	imunita	k1gFnSc7	imunita
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vyšší	vysoký	k2eAgFnSc6d2	vyšší
míře	míra	k1gFnSc6	míra
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
riziku	riziko	k1gNnSc3	riziko
pneumonie	pneumonie	k1gFnSc2	pneumonie
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
cytomegalovirem	cytomegalovir	k1gInSc7	cytomegalovir
<g/>
.	.	kIx.	.
</s>
<s>
Pacienti	pacient	k1gMnPc1	pacient
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
onemocněli	onemocnět	k5eAaPmAgMnP	onemocnět
virovou	virový	k2eAgFnSc7d1	virová
infekcí	infekce	k1gFnSc7	infekce
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
sekundárně	sekundárně	k6eAd1	sekundárně
infikováni	infikován	k2eAgMnPc1d1	infikován
bakteriemi	bakterie	k1gFnPc7	bakterie
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pneumoniae	pneumonia	k1gFnSc2	pneumonia
<g/>
,	,	kIx,	,
Staphylococcus	Staphylococcus	k1gMnSc1	Staphylococcus
aureus	aureus	k1gMnSc1	aureus
či	či	k8xC	či
Haemophilus	Haemophilus	k1gMnSc1	Haemophilus
influenzae	influenzaat	k5eAaPmIp3nS	influenzaat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
další	další	k2eAgInPc4d1	další
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
roční	roční	k2eAgFnSc6d1	roční
době	doba	k1gFnSc6	doba
převažují	převažovat	k5eAaImIp3nP	převažovat
jiné	jiný	k2eAgInPc1d1	jiný
typy	typ	k1gInPc1	typ
virů	vir	k1gInPc2	vir
-	-	kIx~	-
například	například	k6eAd1	například
během	během	k7c2	během
chřipkové	chřipkový	k2eAgFnSc2d1	chřipková
sezóny	sezóna	k1gFnSc2	sezóna
může	moct	k5eAaImIp3nS	moct
chřipka	chřipka	k1gFnSc1	chřipka
způsobit	způsobit	k5eAaPmF	způsobit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
případů	případ	k1gInPc2	případ
virové	virový	k2eAgFnSc2d1	virová
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
způsoben	způsobit	k5eAaPmNgInS	způsobit
jinými	jiný	k2eAgInPc7d1	jiný
viry	vir	k1gInPc7	vir
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
hantaviry	hantavira	k1gFnPc4	hantavira
a	a	k8xC	a
koronaviry	koronavira	k1gFnPc4	koronavira
<g/>
.	.	kIx.	.
</s>
<s>
Plísňová	plísňový	k2eAgFnSc1d1	plísňová
pneumonie	pneumonie	k1gFnSc1	pneumonie
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
,	,	kIx,	,
častější	častý	k2eAgNnSc1d2	častější
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
jedinců	jedinec	k1gMnPc2	jedinec
s	s	k7c7	s
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
oslabeným	oslabený	k2eAgInSc7d1	oslabený
vlivem	vliv	k1gInSc7	vliv
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
imunosupresivních	imunosupresivní	k2eAgInPc2d1	imunosupresivní
léků	lék	k1gInPc2	lék
či	či	k8xC	či
jiných	jiný	k2eAgInPc2d1	jiný
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
ji	on	k3xPp3gFnSc4	on
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
organismy	organismus	k1gInPc1	organismus
Histoplasma	Histoplasm	k1gMnSc2	Histoplasm
capsulatum	capsulatum	k1gNnSc4	capsulatum
<g/>
,	,	kIx,	,
patogenní	patogenní	k2eAgFnPc4d1	patogenní
houby	houba	k1gFnPc4	houba
rodu	rod	k1gInSc2	rod
blastomyces	blastomyces	k1gInSc1	blastomyces
<g/>
,	,	kIx,	,
Cryptococcus	Cryptococcus	k1gInSc1	Cryptococcus
neoformans	neoformans	k1gInSc1	neoformans
<g/>
,	,	kIx,	,
Pneumocystis	Pneumocystis	k1gInSc1	Pneumocystis
jiroveci	jirovece	k1gFnSc4	jirovece
a	a	k8xC	a
Coccidioides	Coccidioides	k1gInSc4	Coccidioides
immitis	immitis	k1gFnSc1	immitis
<g/>
.	.	kIx.	.
</s>
<s>
Histoplazmóza	Histoplazmóza	k1gFnSc1	Histoplazmóza
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
a	a	k8xC	a
kokcidiomykóza	kokcidiomykóza	k1gFnSc1	kokcidiomykóza
je	být	k5eAaImIp3nS	být
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
počet	počet	k1gInSc1	počet
případů	případ	k1gInPc2	případ
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozvoje	rozvoj	k1gInSc2	rozvoj
cestování	cestování	k1gNnSc2	cestování
a	a	k8xC	a
rychlejšího	rychlý	k2eAgNnSc2d2	rychlejší
snižování	snižování	k1gNnSc2	snižování
obranyschopnosti	obranyschopnost	k1gFnSc2	obranyschopnost
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
plic	plíce	k1gFnPc2	plíce
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
řada	řada	k1gFnSc1	řada
parazitů	parazit	k1gMnPc2	parazit
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
například	například	k6eAd1	například
prvok	prvok	k1gMnSc1	prvok
Toxoplasma	Toxoplasmum	k1gNnSc2	Toxoplasmum
gondii	gondie	k1gFnSc4	gondie
<g/>
,	,	kIx,	,
háďátko	háďátko	k1gNnSc1	háďátko
střevní	střevní	k2eAgNnSc1d1	střevní
<g/>
,	,	kIx,	,
škrkavka	škrkavka	k1gFnSc1	škrkavka
dětská	dětský	k2eAgFnSc1d1	dětská
či	či	k8xC	či
zimnička	zimnička	k1gFnSc1	zimnička
čtvrtodenní	čtvrtodenní	k2eAgFnSc1d1	čtvrtodenní
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
organismy	organismus	k1gInPc1	organismus
obvykle	obvykle	k6eAd1	obvykle
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
přímým	přímý	k2eAgInSc7d1	přímý
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
pokožkou	pokožka	k1gFnSc7	pokožka
<g/>
,	,	kIx,	,
požitím	požití	k1gNnSc7	požití
nebo	nebo	k8xC	nebo
přenesením	přenesení	k1gNnSc7	přenesení
hmyzím	hmyzí	k2eAgMnSc7d1	hmyzí
bacilonosičem	bacilonosič	k1gMnSc7	bacilonosič
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
motolice	motolice	k1gFnSc1	motolice
plicní	plicní	k2eAgFnSc1d1	plicní
většina	většina	k1gFnSc1	většina
parazitů	parazit	k1gMnPc2	parazit
sice	sice	k8xC	sice
plíce	plíce	k1gFnSc1	plíce
specificky	specificky	k6eAd1	specificky
nezasahuje	zasahovat	k5eNaImIp3nS	zasahovat
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
orgán	orgán	k1gInSc1	orgán
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
postižen	postihnout	k5eAaPmNgMnS	postihnout
sekundárně	sekundárně	k6eAd1	sekundárně
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
paraziti	parazit	k1gMnPc1	parazit
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ti	ten	k3xDgMnPc1	ten
náležící	náležící	k2eAgMnPc1d1	náležící
k	k	k7c3	k
rodům	rod	k1gInPc3	rod
Ascaris	Ascaris	k1gFnPc2	Ascaris
a	a	k8xC	a
Strongyloides	Strongyloidesa	k1gFnPc2	Strongyloidesa
<g/>
,	,	kIx,	,
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
silnou	silný	k2eAgFnSc4d1	silná
eozinofilní	eozinofilní	k2eAgFnSc4d1	eozinofilní
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
může	moct	k5eAaImIp3nS	moct
vyústit	vyústit	k5eAaPmF	vyústit
až	až	k9	až
v	v	k7c6	v
eozinofilní	eozinofilní	k2eAgFnSc6d1	eozinofilní
pneumonii	pneumonie	k1gFnSc6	pneumonie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
infekcí	infekce	k1gFnPc2	infekce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
malárie	malárie	k1gFnSc2	malárie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
plíce	plíce	k1gFnPc1	plíce
postiženy	postižen	k2eAgFnPc1d1	postižena
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
systémovému	systémový	k2eAgInSc3d1	systémový
zánětu	zánět	k1gInSc3	zánět
navozenému	navozený	k2eAgInSc3d1	navozený
cytokiny	cytokina	k1gFnPc5	cytokina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvinutém	rozvinutý	k2eAgInSc6d1	rozvinutý
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
infekce	infekce	k1gFnPc1	infekce
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
nejčastěji	často	k6eAd3	často
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgFnP	vrátit
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
globálním	globální	k2eAgNnSc6d1	globální
měřítku	měřítko	k1gNnSc6	měřítko
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
nejběžnější	běžný	k2eAgMnPc1d3	Nejběžnější
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
trpí	trpět	k5eAaImIp3nP	trpět
sníženou	snížený	k2eAgFnSc7d1	snížená
imunitou	imunita	k1gFnSc7	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Idiopatická	idiopatický	k2eAgFnSc1d1	idiopatická
intersticiální	intersticiální	k2eAgFnSc1d1	intersticiální
pneumonie	pneumonie	k1gFnSc1	pneumonie
nebo	nebo	k8xC	nebo
neinfekční	infekční	k2eNgInSc1d1	neinfekční
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
chorob	choroba	k1gFnPc2	choroba
zvané	zvaný	k2eAgFnSc2d1	zvaná
difuzní	difuzní	k2eAgFnSc2d1	difuzní
nemoci	nemoc	k1gFnSc2	nemoc
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gMnPc2	on
sem	sem	k6eAd1	sem
řadíme	řadit	k5eAaImIp1nP	řadit
také	také	k9	také
difuzní	difuzní	k2eAgNnSc4d1	difuzní
alveolární	alveolární	k2eAgNnSc4d1	alveolární
poškození	poškození	k1gNnSc4	poškození
<g/>
,	,	kIx,	,
organizující	organizující	k2eAgFnSc4d1	organizující
se	se	k3xPyFc4	se
pneumonii	pneumonie	k1gFnSc4	pneumonie
<g/>
,	,	kIx,	,
nespecifickou	specifický	k2eNgFnSc4d1	nespecifická
intersticiální	intersticiální	k2eAgFnSc4d1	intersticiální
pneumonii	pneumonie	k1gFnSc4	pneumonie
<g/>
,	,	kIx,	,
lymfocytickou	lymfocytický	k2eAgFnSc4d1	lymfocytický
intersticiální	intersticiální	k2eAgFnSc4d1	intersticiální
pneumonii	pneumonie	k1gFnSc4	pneumonie
<g/>
,	,	kIx,	,
deskvamační	deskvamační	k2eAgFnSc4d1	deskvamační
intersticiální	intersticiální	k2eAgFnSc4d1	intersticiální
pneumonii	pneumonie	k1gFnSc4	pneumonie
<g/>
,	,	kIx,	,
respirační	respirační	k2eAgFnSc4d1	respirační
bronchiolitidu	bronchiolitida	k1gFnSc4	bronchiolitida
s	s	k7c7	s
postižením	postižení	k1gNnSc7	postižení
plicního	plicní	k2eAgNnSc2d1	plicní
intersticia	intersticium	k1gNnSc2	intersticium
a	a	k8xC	a
běžnou	běžný	k2eAgFnSc4d1	běžná
intersticiální	intersticiální	k2eAgFnSc4d1	intersticiální
pneumonii	pneumonie	k1gFnSc4	pneumonie
<g/>
.	.	kIx.	.
</s>
<s>
Zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
často	často	k6eAd1	často
začíná	začínat	k5eAaImIp3nS	začínat
jako	jako	k9	jako
zánět	zánět	k1gInSc1	zánět
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
i	i	k9	i
do	do	k7c2	do
dolních	dolní	k2eAgFnPc2d1	dolní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
.	.	kIx.	.
</s>
<s>
Viry	vir	k1gInPc1	vir
se	se	k3xPyFc4	se
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
řadou	řada	k1gFnSc7	řada
různých	různý	k2eAgFnPc2d1	různá
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
respiračního	respirační	k2eAgInSc2d1	respirační
syncytiálního	syncytiální	k2eAgInSc2d1	syncytiální
viru	vir	k1gInSc2	vir
typicky	typicky	k6eAd1	typicky
dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
dotknou	dotknout	k5eAaPmIp3nP	dotknout
kontaminovaných	kontaminovaný	k2eAgInPc2d1	kontaminovaný
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
poté	poté	k6eAd1	poté
očí	oko	k1gNnPc2	oko
nebo	nebo	k8xC	nebo
nosu	nos	k1gInSc2	nos
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
nákazy	nákaza	k1gFnSc2	nákaza
virem	vir	k1gInSc7	vir
je	být	k5eAaImIp3nS	být
vdechnutí	vdechnutí	k1gNnSc1	vdechnutí
kontaminovaných	kontaminovaný	k2eAgFnPc2d1	kontaminovaná
<g/>
,	,	kIx,	,
vzduchem	vzduch	k1gInSc7	vzduch
přenášených	přenášený	k2eAgFnPc2d1	přenášená
kapének	kapénka	k1gFnPc2	kapénka
ústy	ústa	k1gNnPc7	ústa
nebo	nebo	k8xC	nebo
nosem	nos	k1gInSc7	nos
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
jsou	být	k5eAaImIp3nP	být
viry	vir	k1gInPc1	vir
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
cestách	cesta	k1gFnPc6	cesta
dýchacích	dýchací	k2eAgMnPc2d1	dýchací
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napadnou	napadnout	k5eAaPmIp3nP	napadnout
buňky	buňka	k1gFnPc1	buňka
vystýlající	vystýlající	k2eAgFnSc2d1	vystýlající
dýchací	dýchací	k2eAgFnPc4d1	dýchací
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
plicní	plicní	k2eAgInPc4d1	plicní
sklípky	sklípek	k1gInPc4	sklípek
nebo	nebo	k8xC	nebo
plicní	plicní	k2eAgInSc4d1	plicní
parenchym	parenchym	k1gInSc4	parenchym
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
spalničkový	spalničkový	k2eAgInSc1d1	spalničkový
virus	virus	k1gInSc1	virus
nebo	nebo	k8xC	nebo
virus	virus	k1gInSc1	virus
Herpes	herpes	k1gInSc1	herpes
simplex	simplex	k1gInSc4	simplex
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
dostat	dostat	k5eAaPmF	dostat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Zasažení	zasažení	k1gNnSc1	zasažení
plic	plíce	k1gFnPc2	plíce
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
stupňům	stupeň	k1gInPc3	stupeň
buněčné	buněčný	k2eAgFnSc2d1	buněčná
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
imunitní	imunitní	k2eAgInSc1d1	imunitní
systém	systém	k1gInSc1	systém
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
infekci	infekce	k1gFnSc4	infekce
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
pak	pak	k6eAd1	pak
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
ještě	ještě	k6eAd1	ještě
většímu	veliký	k2eAgNnSc3d2	veliký
poškození	poškození	k1gNnSc3	poškození
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
agranulocyty	agranulocyt	k1gInPc4	agranulocyt
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
primárním	primární	k2eAgMnSc7d1	primární
původcem	původce	k1gMnSc7	původce
zánětu	zánět	k1gInSc2	zánět
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
virů	vir	k1gInPc2	vir
postihuje	postihovat	k5eAaImIp3nS	postihovat
nejen	nejen	k6eAd1	nejen
plíce	plíce	k1gFnPc4	plíce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
narušuje	narušovat	k5eAaImIp3nS	narušovat
další	další	k2eAgFnPc4d1	další
funkce	funkce	k1gFnPc4	funkce
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Viry	vir	k1gInPc1	vir
mohou	moct	k5eAaImIp3nP	moct
rovněž	rovněž	k9	rovněž
zvýšit	zvýšit	k5eAaPmF	zvýšit
náchylnost	náchylnost	k1gFnSc4	náchylnost
k	k	k7c3	k
bakteriálním	bakteriální	k2eAgFnPc3d1	bakteriální
infekcím	infekce	k1gFnPc3	infekce
<g/>
;	;	kIx,	;
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
bakteriální	bakteriální	k2eAgInSc4d1	bakteriální
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
jako	jako	k8xC	jako
přidružené	přidružený	k2eAgNnSc4d1	přidružené
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
dostane	dostat	k5eAaPmIp3nS	dostat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mikroaspirace	mikroaspirace	k1gFnSc2	mikroaspirace
organismů	organismus	k1gInPc2	organismus
žijících	žijící	k2eAgInPc2d1	žijící
v	v	k7c6	v
krku	krk	k1gInSc6	krk
či	či	k8xC	či
nose	nos	k1gInSc6	nos
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
poloviny	polovina	k1gFnSc2	polovina
lidí	člověk	k1gMnPc2	člověk
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
mikroaspirace	mikroaspirace	k1gFnSc2	mikroaspirace
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
v	v	k7c6	v
krku	krk	k1gInSc6	krk
jsou	být	k5eAaImIp3nP	být
jisté	jistý	k2eAgFnPc1d1	jistá
bakterie	bakterie	k1gFnPc1	bakterie
přítomny	přítomen	k2eAgFnPc1d1	přítomna
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
potenciálně	potenciálně	k6eAd1	potenciálně
nakažlivé	nakažlivý	k2eAgFnPc4d1	nakažlivá
bakterie	bakterie	k1gFnPc4	bakterie
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
usidlují	usidlovat	k5eAaImIp3nP	usidlovat
jen	jen	k9	jen
někdy	někdy	k6eAd1	někdy
a	a	k8xC	a
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
typů	typ	k1gInPc2	typ
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
tuberculosis	tuberculosis	k1gFnSc2	tuberculosis
a	a	k8xC	a
Legionella	Legionell	k1gMnSc2	Legionell
pneumophila	pneumophil	k1gMnSc2	pneumophil
<g/>
,	,	kIx,	,
vniká	vnikat	k5eAaImIp3nS	vnikat
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kontaminovaných	kontaminovaný	k2eAgInPc2d1	kontaminovaný
<g/>
,	,	kIx,	,
vzduchem	vzduch	k1gInSc7	vzduch
přenášených	přenášený	k2eAgFnPc2d1	přenášená
kapének	kapénka	k1gFnPc2	kapénka
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
mohou	moct	k5eAaImIp3nP	moct
šířit	šířit	k5eAaImF	šířit
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
napadnout	napadnout	k5eAaPmF	napadnout
prostor	prostor	k1gInSc4	prostor
mezi	mezi	k7c7	mezi
buňkami	buňka	k1gFnPc7	buňka
a	a	k8xC	a
plicními	plicní	k2eAgInPc7d1	plicní
sklípky	sklípek	k1gInPc7	sklípek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
makrofágy	makrofág	k1gInPc7	makrofág
a	a	k8xC	a
neutrofily	neutrofil	k1gMnPc7	neutrofil
(	(	kIx(	(
<g/>
obranné	obranný	k2eAgFnSc2d1	obranná
bílé	bílý	k2eAgFnSc2d1	bílá
krvinky	krvinka	k1gFnSc2	krvinka
<g/>
)	)	kIx)	)
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
tyto	tento	k3xDgFnPc4	tento
bakterie	bakterie	k1gFnPc4	bakterie
inaktivovat	inaktivovat	k5eAaBmF	inaktivovat
<g/>
.	.	kIx.	.
</s>
<s>
Neutrofily	Neutrofil	k1gMnPc4	Neutrofil
rovněž	rovněž	k9	rovněž
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
cytokiny	cytokina	k1gFnPc1	cytokina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
aktivaci	aktivace	k1gFnSc4	aktivace
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
horečce	horečka	k1gFnSc3	horečka
<g/>
,	,	kIx,	,
zimnici	zimnice	k1gFnSc3	zimnice
a	a	k8xC	a
únavě	únava	k1gFnSc3	únava
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
běžným	běžný	k2eAgInPc3d1	běžný
projevům	projev	k1gInPc3	projev
bakteriálního	bakteriální	k2eAgInSc2d1	bakteriální
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Neutrofily	Neutrofil	k1gMnPc4	Neutrofil
<g/>
,	,	kIx,	,
bakterie	bakterie	k1gFnSc1	bakterie
a	a	k8xC	a
tekutina	tekutina	k1gFnSc1	tekutina
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
cév	céva	k1gFnPc2	céva
pak	pak	k6eAd1	pak
naplní	naplnit	k5eAaPmIp3nS	naplnit
plicní	plicní	k2eAgInPc4d1	plicní
sklípky	sklípek	k1gInPc4	sklípek
<g/>
;	;	kIx,	;
tuto	tento	k3xDgFnSc4	tento
konsolidaci	konsolidace	k1gFnSc4	konsolidace
(	(	kIx(	(
<g/>
zhuštění	zhuštění	k1gNnSc2	zhuštění
<g/>
)	)	kIx)	)
plicní	plicní	k2eAgFnSc2d1	plicní
tkáně	tkáň	k1gFnSc2	tkáň
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
na	na	k7c6	na
rentgenovém	rentgenový	k2eAgInSc6d1	rentgenový
snímku	snímek	k1gInSc6	snímek
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
.	.	kIx.	.
</s>
<s>
Pneumonie	pneumonie	k1gFnSc1	pneumonie
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kombinace	kombinace	k1gFnSc1	kombinace
fyzických	fyzický	k2eAgMnPc2d1	fyzický
příznaků	příznak	k1gInPc2	příznak
a	a	k8xC	a
rentgenu	rentgen	k1gInSc2	rentgen
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzení	potvrzení	k1gNnSc1	potvrzení
základní	základní	k2eAgFnSc2d1	základní
příčiny	příčina	k1gFnSc2	příčina
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
test	test	k1gInSc4	test
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
definitivně	definitivně	k6eAd1	definitivně
rozlišil	rozlišit	k5eAaPmAgInS	rozlišit
bakteriální	bakteriální	k2eAgInSc4d1	bakteriální
a	a	k8xC	a
nebakteriální	bakteriální	k2eNgInSc4d1	nebakteriální
původ	původ	k1gInSc4	původ
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
definuje	definovat	k5eAaBmIp3nS	definovat
dětský	dětský	k2eAgInSc4d1	dětský
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
na	na	k7c6	na
základě	základ	k1gInSc6	základ
klinických	klinický	k2eAgInPc2d1	klinický
příznaků	příznak	k1gInPc2	příznak
-	-	kIx~	-
buď	buď	k8xC	buď
kašle	kašel	k1gInSc2	kašel
nebo	nebo	k8xC	nebo
dechových	dechový	k2eAgFnPc2d1	dechová
potíží	potíž	k1gFnPc2	potíž
a	a	k8xC	a
zrychleného	zrychlený	k2eAgNnSc2d1	zrychlené
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
,	,	kIx,	,
vtažení	vtažení	k1gNnSc2	vtažení
hrudníku	hrudník	k1gInSc2	hrudník
nebo	nebo	k8xC	nebo
poruchy	porucha	k1gFnSc2	porucha
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
dechová	dechový	k2eAgFnSc1d1	dechová
frekvence	frekvence	k1gFnSc1	frekvence
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
dechů	dech	k1gInPc2	dech
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
2	[number]	k4	2
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
50	[number]	k4	50
dechů	dech	k1gInPc2	dech
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
od	od	k7c2	od
2	[number]	k4	2
měsíců	měsíc	k1gInPc2	měsíc
do	do	k7c2	do
1	[number]	k4	1
roku	rok	k1gInSc2	rok
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
dechů	dech	k1gInPc2	dech
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
5	[number]	k4	5
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
dechová	dechový	k2eAgFnSc1d1	dechová
frekvence	frekvence	k1gFnSc1	frekvence
a	a	k8xC	a
vtažení	vtažení	k1gNnSc1	vtažení
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
hrudníku	hrudník	k1gInSc2	hrudník
vyšší	vysoký	k2eAgFnSc4d2	vyšší
senzitivitu	senzitivita	k1gFnSc4	senzitivita
než	než	k8xS	než
poslech	poslech	k1gInSc4	poslech
třecích	třecí	k2eAgInPc2d1	třecí
šelestů	šelest	k1gInPc2	šelest
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
pomocí	pomocí	k7c2	pomocí
stetoskopu	stetoskop	k1gInSc2	stetoskop
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
mírných	mírný	k2eAgInPc6d1	mírný
případech	případ	k1gInPc6	případ
nemoci	moct	k5eNaImF	moct
obvykle	obvykle	k6eAd1	obvykle
nutné	nutný	k2eAgNnSc1d1	nutné
žádné	žádný	k3yNgNnSc4	žádný
vyšetření	vyšetření	k1gNnSc4	vyšetření
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc4	všechen
vitální	vitální	k2eAgInPc4d1	vitální
znaky	znak	k1gInPc4	znak
a	a	k8xC	a
poslech	poslech	k1gInSc4	poslech
hrudníku	hrudník	k1gInSc2	hrudník
v	v	k7c6	v
normálu	normál	k1gInSc6	normál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
riziko	riziko	k1gNnSc1	riziko
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
hospitalizace	hospitalizace	k1gFnSc1	hospitalizace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
provedení	provedení	k1gNnSc1	provedení
pulzní	pulzní	k2eAgFnSc2d1	pulzní
oxymetrie	oxymetrie	k1gFnSc2	oxymetrie
<g/>
,	,	kIx,	,
radiografie	radiografie	k1gFnSc2	radiografie
hrudníku	hrudník	k1gInSc2	hrudník
a	a	k8xC	a
krevních	krevní	k2eAgInPc2d1	krevní
testů	test	k1gInPc2	test
<g/>
–	–	k?	–
<g/>
včetně	včetně	k7c2	včetně
krevního	krevní	k2eAgInSc2d1	krevní
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
elektrolytů	elektrolyt	k1gInPc2	elektrolyt
v	v	k7c6	v
séru	sérum	k1gNnSc6	sérum
<g/>
,	,	kIx,	,
hladiny	hladina	k1gFnPc4	hladina
C-reaktivního	Ceaktivní	k2eAgInSc2d1	C-reaktivní
proteinu	protein	k1gInSc2	protein
a	a	k8xC	a
případně	případně	k6eAd1	případně
testů	test	k1gInPc2	test
funkce	funkce	k1gFnSc2	funkce
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
chřipce	chřipka	k1gFnSc3	chřipka
podobného	podobný	k2eAgNnSc2d1	podobné
onemocnění	onemocnění	k1gNnSc2	onemocnění
(	(	kIx(	(
<g/>
ILI	ILI	kA	ILI
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
příznacích	příznak	k1gInPc6	příznak
a	a	k8xC	a
symptomech	symptom	k1gInPc6	symptom
<g/>
;	;	kIx,	;
potvrzení	potvrzení	k1gNnSc4	potvrzení
nákazy	nákaza	k1gFnSc2	nákaza
takovým	takový	k3xDgNnSc7	takový
onemocněním	onemocnění	k1gNnSc7	onemocnění
však	však	k9	však
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
testování	testování	k1gNnSc1	testování
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
často	často	k6eAd1	často
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
výskytu	výskyt	k1gInSc6	výskyt
chřipky	chřipka	k1gFnSc2	chřipka
v	v	k7c6	v
komunitě	komunita	k1gFnSc6	komunita
nebo	nebo	k8xC	nebo
na	na	k7c6	na
výsledcích	výsledek	k1gInPc6	výsledek
rychlého	rychlý	k2eAgInSc2d1	rychlý
testu	test	k1gInSc2	test
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
chřipky	chřipka	k1gFnSc2	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgNnSc1d1	fyzikální
vyšetření	vyšetření	k1gNnSc1	vyšetření
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
odhalit	odhalit	k5eAaPmF	odhalit
nízký	nízký	k2eAgInSc4d1	nízký
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc4d1	vysoká
tepovou	tepový	k2eAgFnSc4d1	tepová
frekvenci	frekvence	k1gFnSc4	frekvence
nebo	nebo	k8xC	nebo
nízké	nízký	k2eAgNnSc1d1	nízké
procento	procento	k1gNnSc1	procento
nasycení	nasycení	k1gNnSc2	nasycení
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
dýchání	dýchání	k1gNnSc2	dýchání
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
obvykle	obvykle	k6eAd1	obvykle
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc1d1	ostatní
příznaky	příznak	k1gInPc1	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetření	vyšetření	k1gNnSc1	vyšetření
hrudníku	hrudník	k1gInSc2	hrudník
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
normálu	normál	k1gInSc6	normál
<g/>
,	,	kIx,	,
na	na	k7c6	na
postižené	postižený	k2eAgFnSc6d1	postižená
straně	strana	k1gFnSc6	strana
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
viditelné	viditelný	k2eAgNnSc1d1	viditelné
snížené	snížený	k2eAgNnSc1d1	snížené
rozpínání	rozpínání	k1gNnSc1	rozpínání
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
.	.	kIx.	.
</s>
<s>
Drsné	drsný	k2eAgInPc1d1	drsný
zvuky	zvuk	k1gInPc1	zvuk
vycházející	vycházející	k2eAgInPc1d1	vycházející
z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
rezonující	rezonující	k2eAgFnSc7d1	rezonující
zanícenou	zanícený	k2eAgFnSc7d1	zanícená
plící	plíce	k1gFnSc7	plíce
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
hrudní	hrudní	k2eAgNnPc1d1	hrudní
dýchání	dýchání	k1gNnPc1	dýchání
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
při	při	k7c6	při
poslechu	poslech	k1gInSc6	poslech
stetoskopem	stetoskop	k1gInSc7	stetoskop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
postižené	postižený	k2eAgFnSc6d1	postižená
oblasti	oblast	k1gFnSc6	oblast
lze	lze	k6eAd1	lze
během	během	k7c2	během
vdechování	vdechování	k1gNnSc2	vdechování
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
i	i	k9	i
třecí	třecí	k2eAgInPc4d1	třecí
šelesty	šelest	k1gInPc4	šelest
(	(	kIx(	(
<g/>
krepitace	krepitace	k1gFnPc4	krepitace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Perkuse	perkuse	k1gFnSc1	perkuse
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
u	u	k7c2	u
postižené	postižený	k2eAgFnSc2d1	postižená
plíce	plíce	k1gFnSc2	plíce
utlumená	utlumený	k2eAgFnSc1d1	utlumená
<g/>
,	,	kIx,	,
a	a	k8xC	a
spíše	spíše	k9	spíše
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
nižší	nízký	k2eAgFnSc1d2	nižší
vokální	vokální	k2eAgFnSc1d1	vokální
rezonance	rezonance	k1gFnSc1	rezonance
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
od	od	k7c2	od
pleurální	pleurální	k2eAgFnSc2d1	pleurální
efuze	efuze	k1gFnSc2	efuze
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
diagnózy	diagnóza	k1gFnSc2	diagnóza
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
rentgenový	rentgenový	k2eAgInSc4d1	rentgenový
snímek	snímek	k1gInSc4	snímek
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
mírnými	mírný	k2eAgInPc7d1	mírný
projevy	projev	k1gInPc7	projev
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
snímkování	snímkování	k1gNnSc1	snímkování
potřebné	potřebný	k2eAgNnSc1d1	potřebné
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
potenciálními	potenciální	k2eAgFnPc7d1	potenciální
komplikacemi	komplikace	k1gFnPc7	komplikace
<g/>
,	,	kIx,	,
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
nelepší	lepšit	k5eNaImIp3nS	lepšit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
příčina	příčina	k1gFnSc1	příčina
onemocnění	onemocnění	k1gNnSc2	onemocnění
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
nemocného	nemocný	k1gMnSc2	nemocný
natolik	natolik	k6eAd1	natolik
vážný	vážný	k2eAgInSc1d1	vážný
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
hospitalizace	hospitalizace	k1gFnSc1	hospitalizace
<g/>
,	,	kIx,	,
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
provést	provést	k5eAaPmF	provést
rentgen	rentgen	k1gInSc4	rentgen
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
však	však	k9	však
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
závažnosti	závažnost	k1gFnPc4	závažnost
onemocnění	onemocnění	k1gNnSc2	onemocnění
a	a	k8xC	a
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
spolehlivě	spolehlivě	k6eAd1	spolehlivě
rozliší	rozlišit	k5eAaPmIp3nS	rozlišit
mezi	mezi	k7c7	mezi
bakteriální	bakteriální	k2eAgFnSc7d1	bakteriální
a	a	k8xC	a
virovou	virový	k2eAgFnSc7d1	virová
infekcí	infekce	k1gFnSc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Rentgenové	rentgenový	k2eAgFnPc4d1	rentgenová
snímky	snímka	k1gFnPc4	snímka
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
lze	lze	k6eAd1	lze
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
jako	jako	k9	jako
lobární	lobární	k2eAgFnSc4d1	lobární
pneumonii	pneumonie	k1gFnSc4	pneumonie
<g/>
,	,	kIx,	,
bronchopneumonii	bronchopneumonie	k1gFnSc4	bronchopneumonie
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
známou	známá	k1gFnSc4	známá
jako	jako	k8xS	jako
lalůčkový	lalůčkový	k2eAgInSc4d1	lalůčkový
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
)	)	kIx)	)
a	a	k8xC	a
intersticiální	intersticiální	k2eAgFnSc4d1	intersticiální
pneumonii	pneumonie	k1gFnSc4	pneumonie
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
<g/>
,	,	kIx,	,
komunitně	komunitně	k6eAd1	komunitně
získaná	získaný	k2eAgFnSc1d1	získaná
pneumonie	pneumonie	k1gFnSc1	pneumonie
klasicky	klasicky	k6eAd1	klasicky
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
známky	známka	k1gFnPc4	známka
plicní	plicní	k2eAgFnPc4d1	plicní
konsolidace	konsolidace	k1gFnPc4	konsolidace
jednoho	jeden	k4xCgInSc2	jeden
segmentálního	segmentální	k2eAgInSc2d1	segmentální
plicního	plicní	k2eAgInSc2d1	plicní
laloku	lalok	k1gInSc2	lalok
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
znaky	znak	k1gInPc1	znak
lobární	lobární	k2eAgFnSc2d1	lobární
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
se	se	k3xPyFc4	se
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
typů	typ	k1gInPc2	typ
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgFnPc1d1	běžná
jiné	jiný	k2eAgFnPc1d1	jiná
charakteristiky	charakteristika	k1gFnPc1	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
Aspirační	aspirační	k2eAgFnSc1d1	aspirační
pneumonie	pneumonie	k1gFnSc1	pneumonie
může	moct	k5eAaImIp3nS	moct
vykazovat	vykazovat	k5eAaImF	vykazovat
oboustranné	oboustranný	k2eAgNnSc1d1	oboustranné
zastínění	zastínění	k1gNnSc1	zastínění
zejména	zejména	k9	zejména
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Rentgenové	rentgenový	k2eAgInPc4d1	rentgenový
snímky	snímek	k1gInPc4	snímek
virového	virový	k2eAgInSc2d1	virový
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
jevit	jevit	k5eAaImF	jevit
normální	normální	k2eAgMnPc4d1	normální
<g/>
,	,	kIx,	,
vykazovat	vykazovat	k5eAaImF	vykazovat
hyperinflaci	hyperinflace	k1gFnSc4	hyperinflace
<g/>
,	,	kIx,	,
ostrůvkovité	ostrůvkovitý	k2eAgFnPc4d1	ostrůvkovitá
oblasti	oblast	k1gFnPc4	oblast
konsolidace	konsolidace	k1gFnSc2	konsolidace
plicní	plicní	k2eAgFnSc2d1	plicní
tkáně	tkáň	k1gFnSc2	tkáň
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
plicích	plíce	k1gFnPc6	plíce
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
podobat	podobat	k5eAaImF	podobat
bakteriálnímu	bakteriální	k2eAgNnSc3d1	bakteriální
zápalu	zápal	k1gInSc6	zápal
plic	plíce	k1gFnPc2	plíce
s	s	k7c7	s
lobární	lobární	k2eAgFnSc7d1	lobární
konsolidací	konsolidace	k1gFnSc7	konsolidace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
fázích	fáze	k1gFnPc6	fáze
onemocnění	onemocnění	k1gNnSc2	onemocnění
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
radiologické	radiologický	k2eAgInPc1d1	radiologický
nálezy	nález	k1gInPc1	nález
patrné	patrný	k2eAgInPc1d1	patrný
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
při	při	k7c6	při
dehydrataci	dehydratace	k1gFnSc6	dehydratace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
je	být	k5eAaImIp3nS	být
interpretovat	interpretovat	k5eAaBmF	interpretovat
u	u	k7c2	u
obézních	obézní	k2eAgFnPc2d1	obézní
osob	osoba	k1gFnPc2	osoba
nebo	nebo	k8xC	nebo
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
již	již	k6eAd1	již
onemocnění	onemocnění	k1gNnSc4	onemocnění
plic	plíce	k1gFnPc2	plíce
prodělali	prodělat	k5eAaPmAgMnP	prodělat
<g/>
.	.	kIx.	.
</s>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
tomografie	tomografie	k1gFnSc1	tomografie
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
nejasných	jasný	k2eNgFnPc2d1	nejasná
příčin	příčina	k1gFnPc2	příčina
poskytnout	poskytnout	k5eAaPmF	poskytnout
další	další	k2eAgFnPc4d1	další
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
léčených	léčený	k2eAgMnPc2d1	léčený
mimo	mimo	k7c4	mimo
nemocnici	nemocnice	k1gFnSc4	nemocnice
není	být	k5eNaImIp3nS	být
určení	určení	k1gNnSc4	určení
původce	původce	k1gMnSc2	původce
nemoci	nemoc	k1gFnSc2	nemoc
finančně	finančně	k6eAd1	finančně
výhodné	výhodný	k2eAgNnSc1d1	výhodné
a	a	k8xC	a
zvolený	zvolený	k2eAgInSc1d1	zvolený
způsob	způsob	k1gInSc1	způsob
léčby	léčba	k1gFnSc2	léčba
obvykle	obvykle	k6eAd1	obvykle
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
léčba	léčba	k1gFnSc1	léčba
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zvážit	zvážit	k5eAaPmF	zvážit
rozbor	rozbor	k1gInSc4	rozbor
kultur	kultura	k1gFnPc2	kultura
sputa	sputum	k1gNnSc2	sputum
a	a	k8xC	a
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
chronickým	chronický	k2eAgInSc7d1	chronický
produktivním	produktivní	k2eAgInSc7d1	produktivní
kašlem	kašel	k1gInSc7	kašel
pak	pak	k6eAd1	pak
test	testa	k1gFnPc2	testa
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
bakterie	bakterie	k1gFnSc2	bakterie
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
tuberculosis	tuberculosis	k1gInSc1	tuberculosis
<g/>
.	.	kIx.	.
</s>
<s>
Testy	test	k1gInPc1	test
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
dalších	další	k2eAgInPc2d1	další
specifických	specifický	k2eAgInPc2d1	specifický
organismů	organismus	k1gInPc2	organismus
lze	lze	k6eAd1	lze
doporučit	doporučit	k5eAaPmF	doporučit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
epidemií	epidemie	k1gFnPc2	epidemie
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
hospitalizovaných	hospitalizovaný	k2eAgFnPc2d1	hospitalizovaná
se	se	k3xPyFc4	se
závažným	závažný	k2eAgNnSc7d1	závažné
onemocněním	onemocnění	k1gNnSc7	onemocnění
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
provedení	provedení	k1gNnSc1	provedení
rozboru	rozbor	k1gInSc2	rozbor
kultur	kultura	k1gFnPc2	kultura
sputa	sputum	k1gNnSc2	sputum
i	i	k8xC	i
krevních	krevní	k2eAgFnPc2d1	krevní
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
test	test	k1gInSc4	test
moči	moč	k1gFnSc2	moč
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
antigenů	antigen	k1gInPc2	antigen
bakterií	bakterium	k1gNnPc2	bakterium
Legionella	Legionell	k1gMnSc2	Legionell
and	and	k?	and
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
<g/>
.	.	kIx.	.
</s>
<s>
Virové	virový	k2eAgFnPc1d1	virová
infekce	infekce	k1gFnPc1	infekce
lze	lze	k6eAd1	lze
potvrdit	potvrdit	k5eAaPmF	potvrdit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
detekce	detekce	k1gFnSc2	detekce
buď	buď	k8xC	buď
viru	vir	k1gInSc2	vir
samotného	samotný	k2eAgMnSc2d1	samotný
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInPc2	jeho
antigenů	antigen	k1gInPc2	antigen
pomocí	pomocí	k7c2	pomocí
rozboru	rozbor	k1gInSc2	rozbor
virových	virový	k2eAgFnPc2d1	virová
kultur	kultura	k1gFnPc2	kultura
nebo	nebo	k8xC	nebo
polymerázové	polymerázový	k2eAgFnSc2d1	polymerázová
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
(	(	kIx(	(
<g/>
PCR	PCR	kA	PCR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
rutinních	rutinní	k2eAgInPc2d1	rutinní
mikrobiologických	mikrobiologický	k2eAgInPc2d1	mikrobiologický
testů	test	k1gInPc2	test
se	se	k3xPyFc4	se
však	však	k9	však
původce	původce	k1gMnSc1	původce
podaří	podařit	k5eAaPmIp3nS	podařit
určit	určit	k5eAaPmF	určit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
15	[number]	k4	15
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Pneumonitida	Pneumonitida	k1gFnSc1	Pneumonitida
je	být	k5eAaImIp3nS	být
zánět	zánět	k1gInSc4	zánět
plic	plíce	k1gFnPc2	plíce
<g/>
;	;	kIx,	;
pneumonie	pneumonie	k1gFnSc1	pneumonie
(	(	kIx(	(
<g/>
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
pneumonitidě	pneumonitida	k1gFnSc3	pneumonitida
<g/>
,	,	kIx,	,
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ale	ale	k9	ale
i	i	k9	i
neinfekční	infekční	k2eNgNnSc1d1	neinfekční
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
plicní	plicní	k2eAgFnSc1d1	plicní
konsolidace	konsolidace	k1gFnSc1	konsolidace
<g/>
.	.	kIx.	.
</s>
<s>
Zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
se	se	k3xPyFc4	se
nejběžněji	běžně	k6eAd3	běžně
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
a	a	k8xC	a
jak	jak	k6eAd1	jak
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
došlo	dojít	k5eAaPmAgNnS	dojít
-	-	kIx~	-
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
komunitně	komunitně	k6eAd1	komunitně
získaný	získaný	k2eAgInSc4d1	získaný
<g/>
,	,	kIx,	,
aspirační	aspirační	k2eAgInSc4d1	aspirační
<g/>
,	,	kIx,	,
související	související	k2eAgInPc4d1	související
se	se	k3xPyFc4	se
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
péčí	péče	k1gFnSc7	péče
<g/>
,	,	kIx,	,
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
získaný	získaný	k2eAgMnSc1d1	získaný
v	v	k7c6	v
nemocničním	nemocniční	k2eAgNnSc6d1	nemocniční
prostředí	prostředí	k1gNnSc6	prostředí
či	či	k8xC	či
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
ventilátorový	ventilátorový	k2eAgMnSc1d1	ventilátorový
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
rovněž	rovněž	k9	rovněž
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
podle	podle	k7c2	podle
postižené	postižený	k2eAgFnSc2d1	postižená
oblasti	oblast	k1gFnSc2	oblast
plic	plíce	k1gFnPc2	plíce
(	(	kIx(	(
<g/>
na	na	k7c4	na
lobární	lobární	k2eAgFnSc4d1	lobární
pneumonii	pneumonie	k1gFnSc4	pneumonie
<g/>
,	,	kIx,	,
bronchiální	bronchiální	k2eAgFnSc4d1	bronchiální
pneumonii	pneumonie	k1gFnSc4	pneumonie
a	a	k8xC	a
akutní	akutní	k2eAgFnSc4d1	akutní
intersticiální	intersticiální	k2eAgFnSc4d1	intersticiální
pneumonii	pneumonie	k1gFnSc4	pneumonie
)	)	kIx)	)
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
organického	organický	k2eAgMnSc2d1	organický
původce	původce	k1gMnSc2	původce
<g/>
.	.	kIx.	.
</s>
<s>
Zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
příznaků	příznak	k1gInPc2	příznak
a	a	k8xC	a
symptomů	symptom	k1gInPc2	symptom
dále	daleko	k6eAd2	daleko
označen	označit	k5eAaPmNgInS	označit
jako	jako	k8xC	jako
nezávažný	závažný	k2eNgInSc1d1	nezávažný
<g/>
,	,	kIx,	,
závažný	závažný	k2eAgInSc1d1	závažný
nebo	nebo	k8xC	nebo
velmi	velmi	k6eAd1	velmi
závažný	závažný	k2eAgInSc1d1	závažný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
onemocnění	onemocnění	k1gNnPc2	onemocnění
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
projevovat	projevovat	k5eAaImF	projevovat
příznaky	příznak	k1gInPc1	příznak
a	a	k8xC	a
symptomy	symptom	k1gInPc1	symptom
podobné	podobný	k2eAgInPc1d1	podobný
těm	ten	k3xDgFnPc3	ten
u	u	k7c2	u
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c4	o
chronickou	chronický	k2eAgFnSc4d1	chronická
obstrukční	obstrukční	k2eAgFnSc4d1	obstrukční
plicní	plicní	k2eAgFnSc4d1	plicní
nemoc	nemoc	k1gFnSc4	nemoc
(	(	kIx(	(
<g/>
COPD	COPD	kA	COPD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astma	astma	k1gNnSc4	astma
<g/>
,	,	kIx,	,
plicní	plicní	k2eAgInSc4d1	plicní
edém	edém	k1gInSc4	edém
<g/>
,	,	kIx,	,
bronchiektázii	bronchiektázie	k1gFnSc4	bronchiektázie
<g/>
,	,	kIx,	,
rakovinu	rakovina	k1gFnSc4	rakovina
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
plicní	plicní	k2eAgFnSc4d1	plicní
embolii	embolie	k1gFnSc4	embolie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
jsou	být	k5eAaImIp3nP	být
astma	astma	k1gFnSc1	astma
a	a	k8xC	a
COPD	COPD	kA	COPD
typicky	typicky	k6eAd1	typicky
doprovázeny	doprovázet	k5eAaImNgInP	doprovázet
sípáním	sípání	k1gNnSc7	sípání
<g/>
,	,	kIx,	,
u	u	k7c2	u
plicního	plicní	k2eAgInSc2d1	plicní
edému	edém	k1gInSc2	edém
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
abnormální	abnormální	k2eAgInSc1d1	abnormální
elektrokardiogram	elektrokardiogram	k1gInSc1	elektrokardiogram
<g/>
,	,	kIx,	,
rakovinu	rakovina	k1gFnSc4	rakovina
a	a	k8xC	a
bronchiektázii	bronchiektázie	k1gFnSc4	bronchiektázie
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
kašel	kašel	k1gInSc1	kašel
a	a	k8xC	a
pro	pro	k7c4	pro
plicní	plicní	k2eAgFnSc4d1	plicní
embolii	embolie	k1gFnSc4	embolie
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
akutní	akutní	k2eAgInSc1d1	akutní
nástup	nástup	k1gInSc1	nástup
ostré	ostrý	k2eAgFnSc2d1	ostrá
bolesti	bolest	k1gFnSc2	bolest
hrudníku	hrudník	k1gInSc2	hrudník
a	a	k8xC	a
dýchavičnost	dýchavičnost	k1gFnSc4	dýchavičnost
<g/>
.	.	kIx.	.
</s>
<s>
Prevence	prevence	k1gFnSc1	prevence
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vakcinaci	vakcinace	k1gFnSc4	vakcinace
<g/>
,	,	kIx,	,
environmentální	environmentální	k2eAgNnSc4d1	environmentální
opatření	opatření	k1gNnSc4	opatření
a	a	k8xC	a
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
léčbu	léčba	k1gFnSc4	léčba
dalších	další	k2eAgInPc2d1	další
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nP	kdyby
byla	být	k5eAaImAgNnP	být
celosvětově	celosvětově	k6eAd1	celosvětově
zavedena	zaveden	k2eAgNnPc1d1	zavedeno
vhodná	vhodný	k2eAgNnPc1d1	vhodné
preventivní	preventivní	k2eAgNnPc1d1	preventivní
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
dětí	dítě	k1gFnPc2	dítě
by	by	kYmCp3nS	by
klesla	klesnout	k5eAaPmAgFnS	klesnout
o	o	k7c4	o
400	[number]	k4	400
000	[number]	k4	000
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
a	a	k8xC	a
kdyby	kdyby	kYmCp3nS	kdyby
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
dostupná	dostupný	k2eAgFnSc1d1	dostupná
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
léčba	léčba	k1gFnSc1	léčba
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
dětí	dítě	k1gFnPc2	dítě
by	by	kYmCp3nS	by
klesl	klesnout	k5eAaPmAgInS	klesnout
o	o	k7c6	o
dalších	další	k2eAgInPc6d1	další
600	[number]	k4	600
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Vakcinace	vakcinace	k1gFnSc1	vakcinace
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
šíření	šíření	k1gNnSc4	šíření
určitých	určitý	k2eAgInPc2d1	určitý
typů	typ	k1gInPc2	typ
bakteriálních	bakteriální	k2eAgInPc2d1	bakteriální
a	a	k8xC	a
virových	virový	k2eAgInPc2d1	virový
zánětů	zánět	k1gInPc2	zánět
plic	plíce	k1gFnPc2	plíce
jak	jak	k8xS	jak
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
tak	tak	k9	tak
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Vakcíny	vakcína	k1gFnPc4	vakcína
proti	proti	k7c3	proti
chřipce	chřipka	k1gFnSc3	chřipka
jsou	být	k5eAaImIp3nP	být
mírně	mírně	k6eAd1	mírně
účinné	účinný	k2eAgInPc1d1	účinný
v	v	k7c6	v
případě	případ	k1gInSc6	případ
virů	vir	k1gInPc2	vir
influenzy	influenza	k1gFnSc2	influenza
A	A	kA	A
a	a	k8xC	a
B.	B.	kA	B.
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
a	a	k8xC	a
prevenci	prevence	k1gFnSc4	prevence
nemocí	nemoc	k1gFnPc2	nemoc
(	(	kIx(	(
<g/>
CDC	CDC	kA	CDC
<g/>
)	)	kIx)	)
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
každoroční	každoroční	k2eAgFnSc4d1	každoroční
vakcinaci	vakcinace	k1gFnSc4	vakcinace
všem	všecek	k3xTgNnPc3	všecek
osobám	osoba	k1gFnPc3	osoba
starším	starý	k2eAgFnPc3d2	starší
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Imunizace	imunizace	k1gFnSc1	imunizace
pracovníků	pracovník	k1gMnPc2	pracovník
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc1	riziko
šíření	šíření	k1gNnSc4	šíření
virového	virový	k2eAgInSc2d1	virový
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gMnPc4	jejich
pacienty	pacient	k1gMnPc4	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
propuknutí	propuknutí	k1gNnSc6	propuknutí
epidemie	epidemie	k1gFnSc2	epidemie
chřipky	chřipka	k1gFnSc2	chřipka
mohou	moct	k5eAaImIp3nP	moct
šíření	šíření	k1gNnSc3	šíření
nákazy	nákaza	k1gFnSc2	nákaza
zabránit	zabránit	k5eAaPmF	zabránit
léky	lék	k1gInPc4	lék
jako	jako	k8xS	jako
například	například	k6eAd1	například
amantadin	amantadin	k2eAgMnSc1d1	amantadin
nebo	nebo	k8xC	nebo
rimantadin	rimantadin	k2eAgMnSc1d1	rimantadin
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
zanamiviru	zanamivir	k1gInSc2	zanamivir
nebo	nebo	k8xC	nebo
oseltamiviru	oseltamivir	k1gInSc2	oseltamivir
není	být	k5eNaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
vyrábějící	vyrábějící	k2eAgFnSc1d1	vyrábějící
oseltamivir	oseltamivir	k1gInSc4	oseltamivir
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
údaje	údaj	k1gInPc4	údaj
získané	získaný	k2eAgInPc4d1	získaný
v	v	k7c6	v
klinických	klinický	k2eAgNnPc6d1	klinické
hodnoceních	hodnocení	k1gNnPc6	hodnocení
k	k	k7c3	k
nezávislé	závislý	k2eNgFnSc3d1	nezávislá
analýze	analýza	k1gFnSc3	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Provádění	provádění	k1gNnSc1	provádění
vakcinace	vakcinace	k1gFnSc2	vakcinace
proti	proti	k7c3	proti
bakteriím	bakterie	k1gFnPc3	bakterie
Haemophilus	Haemophilus	k1gInSc4	Haemophilus
influenzae	influenzae	k1gNnSc1	influenzae
a	a	k8xC	a
Streptococcus	Streptococcus	k1gInSc1	Streptococcus
pneumoniae	pneumoniae	k6eAd1	pneumoniae
podporuje	podporovat	k5eAaImIp3nS	podporovat
řada	řada	k1gFnSc1	řada
důkazů	důkaz	k1gInPc2	důkaz
potvrzujících	potvrzující	k2eAgInPc2d1	potvrzující
její	její	k3xOp3gFnSc4	její
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Očkování	očkování	k1gNnSc1	očkování
dětí	dítě	k1gFnPc2	dítě
proti	proti	k7c3	proti
Streptococcus	Streptococcus	k1gInSc1	Streptococcus
pneumoniae	pneumoniae	k6eAd1	pneumoniae
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
sníženému	snížený	k2eAgInSc3d1	snížený
výskytu	výskyt	k1gInSc3	výskyt
infekcí	infekce	k1gFnPc2	infekce
způsobených	způsobený	k2eAgFnPc2d1	způsobená
touto	tento	k3xDgFnSc7	tento
bakterií	bakterie	k1gFnSc7	bakterie
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnozí	mnohý	k2eAgMnPc1d1	mnohý
dospělí	dospělí	k1gMnPc1	dospělí
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nakazí	nakazit	k5eAaPmIp3nS	nakazit
právě	právě	k6eAd1	právě
od	od	k7c2	od
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
také	také	k9	také
vakcína	vakcína	k1gFnSc1	vakcína
proti	proti	k7c3	proti
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pneumoniae	pneumoniae	k1gFnSc1	pneumoniae
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc4	riziko
invazivního	invazivní	k2eAgNnSc2d1	invazivní
pneumokokového	pneumokokový	k2eAgNnSc2d1	pneumokokové
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
vakcíny	vakcína	k1gFnPc4	vakcína
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
prokázaný	prokázaný	k2eAgInSc4d1	prokázaný
ochranný	ochranný	k2eAgInSc4d1	ochranný
účinek	účinek	k1gInSc4	účinek
před	před	k7c7	před
zápalem	zápal	k1gInSc7	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
vakcína	vakcína	k1gFnSc1	vakcína
proti	proti	k7c3	proti
černému	černý	k2eAgInSc3d1	černý
kašli	kašel	k1gInSc3	kašel
<g/>
,	,	kIx,	,
planým	planý	k2eAgFnPc3d1	planá
neštovicím	neštovice	k1gFnPc3	neštovice
a	a	k8xC	a
spalničkám	spalničky	k1gFnPc3	spalničky
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
doporučované	doporučovaný	k2eAgInPc4d1	doporučovaný
prevenční	prevenční	k2eAgInPc4d1	prevenční
kroky	krok	k1gInPc4	krok
patří	patřit	k5eAaImIp3nS	patřit
přestat	přestat	k5eAaPmF	přestat
kouřit	kouřit	k5eAaImF	kouřit
a	a	k8xC	a
snížit	snížit	k5eAaPmF	snížit
znečištění	znečištění	k1gNnSc4	znečištění
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
za	za	k7c4	za
spalování	spalování	k1gNnSc4	spalování
dřeva	dřevo	k1gNnSc2	dřevo
nebo	nebo	k8xC	nebo
trusu	trus	k1gInSc2	trus
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kouření	kouření	k1gNnSc1	kouření
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
rizikovým	rizikový	k2eAgInSc7d1	rizikový
faktorem	faktor	k1gInSc7	faktor
pneumokokové	pneumokokový	k2eAgFnSc2d1	pneumokoková
pneumonie	pneumonie	k1gFnSc2	pneumonie
u	u	k7c2	u
jinak	jinak	k6eAd1	jinak
zdravých	zdravý	k2eAgFnPc2d1	zdravá
dospělých	dospělý	k2eAgFnPc2d1	dospělá
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Účinnými	účinný	k2eAgFnPc7d1	účinná
preventivními	preventivní	k2eAgFnPc7d1	preventivní
opatřeními	opatření	k1gNnPc7	opatření
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
mytí	mytí	k1gNnSc1	mytí
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
zakrytí	zakrytí	k1gNnPc2	zakrytí
úst	ústa	k1gNnPc2	ústa
při	při	k7c6	při
kašli	kašel	k1gInSc6	kašel
rukávem	rukáv	k1gInSc7	rukáv
oděvu	oděv	k1gInSc2	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Nošení	nošení	k1gNnSc1	nošení
chirurgické	chirurgický	k2eAgFnSc2d1	chirurgická
roušky	rouška	k1gFnSc2	rouška
nemocnými	mocný	k2eNgFnPc7d1	mocný
osobami	osoba	k1gFnPc7	osoba
může	moct	k5eAaImIp3nS	moct
zabránit	zabránit	k5eAaPmF	zabránit
šíření	šíření	k1gNnSc4	šíření
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Vhodná	vhodný	k2eAgFnSc1d1	vhodná
léčba	léčba	k1gFnSc1	léčba
dlouhodobých	dlouhodobý	k2eAgFnPc2d1	dlouhodobá
nemocí	nemoc	k1gFnPc2	nemoc
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
cukrovky	cukrovka	k1gFnSc2	cukrovka
a	a	k8xC	a
podvýživy	podvýživa	k1gFnSc2	podvýživa
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
riziko	riziko	k1gNnSc1	riziko
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
také	také	k6eAd1	také
snížit	snížit	k5eAaPmF	snížit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
mladších	mladý	k2eAgFnPc2d2	mladší
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
snižuje	snižovat	k5eAaImIp3nS	snižovat
kojení	kojení	k1gNnSc4	kojení
jak	jak	k8xS	jak
riziko	riziko	k1gNnSc4	riziko
přenosu	přenos	k1gInSc2	přenos
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
závažnost	závažnost	k1gFnSc4	závažnost
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
CD4	CD4	k1gFnPc2	CD4
lymfocytů	lymfocyt	k1gInPc2	lymfocyt
nižším	nízký	k2eAgInSc6d2	nižší
než	než	k8xS	než
200	[number]	k4	200
buněk	buňka	k1gFnPc2	buňka
<g/>
/	/	kIx~	/
<g/>
uL	ul	kA	ul
snižuje	snižovat	k5eAaImIp3nS	snižovat
antibiotikum	antibiotikum	k1gNnSc4	antibiotikum
trimethoprim	trimethoprim	k1gInSc1	trimethoprim
<g/>
/	/	kIx~	/
<g/>
sulfamethoxazol	sulfamethoxazol	k1gInSc1	sulfamethoxazol
riziko	riziko	k1gNnSc1	riziko
pneumocystové	pneumocystová	k1gFnSc2	pneumocystová
pneumonie	pneumonie	k1gFnSc2	pneumonie
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
posloužit	posloužit	k5eAaPmF	posloužit
jako	jako	k9	jako
prevence	prevence	k1gFnSc1	prevence
imunokompromitovaných	imunokompromitovaný	k2eAgFnPc2d1	imunokompromitovaný
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
nakaženy	nakazit	k5eAaPmNgInP	nakazit
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
<s>
Testy	test	k1gInPc1	test
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
streptokoka	streptokok	k1gMnSc2	streptokok
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
a	a	k8xC	a
bakterie	bakterie	k1gFnSc2	bakterie
Chlamydia	Chlamydium	k1gNnSc2	Chlamydium
trachomatis	trachomatis	k1gFnPc2	trachomatis
u	u	k7c2	u
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
případná	případný	k2eAgFnSc1d1	případná
antibiotická	antibiotický	k2eAgFnSc1d1	antibiotická
léčba	léčba	k1gFnSc1	léčba
snižují	snižovat	k5eAaImIp3nP	snižovat
počet	počet	k1gInSc4	počet
případů	případ	k1gInPc2	případ
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
u	u	k7c2	u
kojenců	kojenec	k1gMnPc2	kojenec
<g/>
;	;	kIx,	;
preventivní	preventivní	k2eAgNnSc4d1	preventivní
opatření	opatření	k1gNnSc4	opatření
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
přenosu	přenos	k1gInSc2	přenos
HIV	HIV	kA	HIV
z	z	k7c2	z
matky	matka	k1gFnSc2	matka
na	na	k7c4	na
dítě	dítě	k1gNnSc4	dítě
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rovněž	rovněž	k9	rovněž
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vysávání	vysávání	k1gNnSc2	vysávání
smolkou	smolka	k1gFnSc7	smolka
znečištěné	znečištěný	k2eAgFnSc2d1	znečištěná
plodové	plodový	k2eAgFnSc2d1	plodová
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
a	a	k8xC	a
krku	krk	k1gInSc2	krk
kojenců	kojenec	k1gMnPc2	kojenec
nebylo	být	k5eNaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
snížení	snížení	k1gNnSc1	snížení
počtu	počet	k1gInSc2	počet
případů	případ	k1gInPc2	případ
aspirační	aspirační	k2eAgFnSc2d1	aspirační
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
;	;	kIx,	;
navíc	navíc	k6eAd1	navíc
může	moct	k5eAaImIp3nS	moct
tento	tento	k3xDgInSc4	tento
zásah	zásah	k1gInSc4	zásah
způsobit	způsobit	k5eAaPmF	způsobit
potenciální	potenciální	k2eAgFnSc4d1	potenciální
újmu	újma	k1gFnSc4	újma
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
doporučována	doporučován	k2eAgFnSc1d1	doporučována
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
oslabených	oslabený	k2eAgFnPc2d1	oslabená
starších	starý	k2eAgFnPc2d2	starší
osob	osoba	k1gFnPc2	osoba
může	moct	k5eAaImIp3nS	moct
riziko	riziko	k1gNnSc1	riziko
aspirační	aspirační	k2eAgFnSc2d1	aspirační
pneumonie	pneumonie	k1gFnSc2	pneumonie
snížit	snížit	k5eAaPmF	snížit
správná	správný	k2eAgFnSc1d1	správná
ústní	ústní	k2eAgFnSc1d1	ústní
hygiena	hygiena	k1gFnSc1	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úplné	úplný	k2eAgNnSc4d1	úplné
odeznění	odeznění	k1gNnSc4	odeznění
příznaků	příznak	k1gInPc2	příznak
obvykle	obvykle	k6eAd1	obvykle
postačují	postačovat	k5eAaImIp3nP	postačovat
orálně	orálně	k6eAd1	orálně
podávaná	podávaný	k2eAgNnPc1d1	podávané
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
klid	klid	k1gInSc1	klid
<g/>
,	,	kIx,	,
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
analgetika	analgetikum	k1gNnPc1	analgetikum
a	a	k8xC	a
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
přísun	přísun	k1gInSc1	přísun
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc1	osoba
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
zdravotními	zdravotní	k2eAgFnPc7d1	zdravotní
komplikacemi	komplikace	k1gFnPc7	komplikace
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnPc1d2	starší
lidé	člověk	k1gMnPc1	člověk
nebo	nebo	k8xC	nebo
nemocní	nemocný	k1gMnPc1	nemocný
se	s	k7c7	s
závažnějšími	závažný	k2eAgFnPc7d2	závažnější
dýchacími	dýchací	k2eAgFnPc7d1	dýchací
potížemi	potíž	k1gFnPc7	potíž
mohou	moct	k5eAaImIp3nP	moct
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
symptomy	symptom	k1gInPc1	symptom
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
a	a	k8xC	a
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
se	se	k3xPyFc4	se
domácí	domácí	k2eAgFnSc7d1	domácí
léčbou	léčba	k1gFnSc7	léčba
nezlepšuje	zlepšovat	k5eNaImIp3nS	zlepšovat
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
nastanou	nastat	k5eAaPmIp3nP	nastat
komplikace	komplikace	k1gFnPc1	komplikace
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
hospitalizace	hospitalizace	k1gFnSc1	hospitalizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
končí	končit	k5eAaImIp3nS	končit
hospitalizací	hospitalizace	k1gFnSc7	hospitalizace
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
%	%	kIx~	%
případů	případ	k1gInPc2	případ
onemocnění	onemocnění	k1gNnSc2	onemocnění
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
dospělých	dospělí	k1gMnPc2	dospělí
s	s	k7c7	s
komunitní	komunitní	k2eAgFnSc7d1	komunitní
pneumonií	pneumonie	k1gFnSc7	pneumonie
hospitalizováno	hospitalizovat	k5eAaBmNgNnS	hospitalizovat
přibližně	přibližně	k6eAd1	přibližně
22	[number]	k4	22
až	až	k9	až
42	[number]	k4	42
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Skóre	skóre	k1gNnSc1	skóre
CURB-65	CURB-65	k1gFnSc2	CURB-65
je	být	k5eAaImIp3nS	být
užitečné	užitečný	k2eAgNnSc1d1	užitečné
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
nutnosti	nutnost	k1gFnSc2	nutnost
hospitalizace	hospitalizace	k1gFnSc2	hospitalizace
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
skóre	skóre	k1gNnSc4	skóre
0	[number]	k4	0
nebo	nebo	k8xC	nebo
1	[number]	k4	1
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
pacienti	pacient	k1gMnPc1	pacient
obvykle	obvykle	k6eAd1	obvykle
léčit	léčit	k5eAaImF	léčit
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
při	při	k7c6	při
dosažení	dosažení	k1gNnSc6	dosažení
2	[number]	k4	2
bodů	bod	k1gInPc2	bod
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
krátký	krátký	k2eAgInSc4d1	krátký
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
nebo	nebo	k8xC	nebo
pečlivé	pečlivý	k2eAgNnSc4d1	pečlivé
sledování	sledování	k1gNnSc4	sledování
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
skóre	skóre	k1gNnSc1	skóre
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
doporučována	doporučován	k2eAgFnSc1d1	doporučována
hospitalizace	hospitalizace	k1gFnSc1	hospitalizace
<g/>
.	.	kIx.	.
</s>
<s>
Hospitalizovány	hospitalizovat	k5eAaBmNgFnP	hospitalizovat
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
i	i	k9	i
děti	dítě	k1gFnPc4	dítě
s	s	k7c7	s
dušností	dušnost	k1gFnSc7	dušnost
nebo	nebo	k8xC	nebo
hladinou	hladina	k1gFnSc7	hladina
nasycení	nasycení	k1gNnSc1	nasycení
kyslíkem	kyslík	k1gInSc7	kyslík
nižší	nízký	k2eAgFnSc2d2	nižší
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Prospěšnost	prospěšnost	k1gFnSc1	prospěšnost
fyzioterapie	fyzioterapie	k1gFnSc2	fyzioterapie
hrudníku	hrudník	k1gInSc2	hrudník
v	v	k7c6	v
případě	případ	k1gInSc6	případ
onemocnění	onemocnění	k1gNnSc2	onemocnění
pneumonií	pneumonie	k1gFnPc2	pneumonie
zatím	zatím	k6eAd1	zatím
nebyla	být	k5eNaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Neinvazivní	Neinvazivní	k2eAgFnSc1d1	Neinvazivní
ventilace	ventilace	k1gFnSc1	ventilace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vhodná	vhodný	k2eAgNnPc1d1	vhodné
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
hospitalizované	hospitalizovaný	k2eAgFnPc4d1	hospitalizovaná
na	na	k7c6	na
jednotce	jednotka	k1gFnSc6	jednotka
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Volně	volně	k6eAd1	volně
prodejné	prodejný	k2eAgInPc1d1	prodejný
léky	lék	k1gInPc1	lék
proti	proti	k7c3	proti
kašli	kašel	k1gInSc3	kašel
ani	ani	k8xC	ani
užívání	užívání	k1gNnSc3	užívání
zinku	zinek	k1gInSc2	zinek
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
nevykazují	vykazovat	k5eNaImIp3nP	vykazovat
žádné	žádný	k3yNgInPc4	žádný
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
důkazní	důkazní	k2eAgFnSc1d1	důkazní
opora	opora	k1gFnSc1	opora
neexistuje	existovat	k5eNaImIp3nS	existovat
ani	ani	k8xC	ani
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
mukolytik	mukolytika	k1gFnPc2	mukolytika
<g/>
.	.	kIx.	.
</s>
<s>
Antibiotika	antibiotikum	k1gNnPc1	antibiotikum
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
výsledky	výsledek	k1gInPc1	výsledek
léčby	léčba	k1gFnSc2	léčba
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
bakteriálním	bakteriální	k2eAgInSc7d1	bakteriální
zápalem	zápal	k1gInSc7	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
volba	volba	k1gFnSc1	volba
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
charakteristikách	charakteristika	k1gFnPc6	charakteristika
dané	daný	k2eAgFnSc2d1	daná
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
celkový	celkový	k2eAgInSc4d1	celkový
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
jako	jako	k9	jako
první	první	k4xOgInSc1	první
způsob	způsob	k1gInSc1	způsob
léčby	léčba	k1gFnSc2	léčba
komunitně	komunitně	k6eAd1	komunitně
získané	získaný	k2eAgFnSc2d1	získaná
pneumonie	pneumonie	k1gFnSc2	pneumonie
empirická	empirický	k2eAgFnSc1d1	empirická
léčba	léčba	k1gFnSc1	léčba
amoxicilinem	amoxicilin	k1gInSc7	amoxicilin
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jako	jako	k8xS	jako
alternativa	alternativa	k1gFnSc1	alternativa
mohou	moct	k5eAaImIp3nP	moct
posloužit	posloužit	k5eAaPmF	posloužit
doxycyklin	doxycyklin	k1gInSc4	doxycyklin
nebo	nebo	k8xC	nebo
clarithromycin	clarithromycin	k1gInSc4	clarithromycin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
atypické	atypický	k2eAgFnPc4d1	atypická
<g/>
"	"	kIx"	"
formy	forma	k1gFnPc4	forma
komunitní	komunitní	k2eAgFnSc2d1	komunitní
pneumonie	pneumonie	k1gFnSc2	pneumonie
běžnější	běžný	k2eAgFnPc1d2	běžnější
<g/>
,	,	kIx,	,
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
amoxicilin	amoxicilin	k1gInSc4	amoxicilin
při	při	k7c6	při
prvotní	prvotní	k2eAgFnSc6d1	prvotní
léčbě	léčba	k1gFnSc6	léčba
dospělých	dospělý	k2eAgFnPc2d1	dospělá
osob	osoba	k1gFnPc2	osoba
mimo	mimo	k7c4	mimo
nemocnici	nemocnice	k1gFnSc4	nemocnice
makrolidy	makrolida	k1gFnSc2	makrolida
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
azithromycin	azithromycin	k2eAgInSc1d1	azithromycin
nebo	nebo	k8xC	nebo
erythromycin	erythromycin	k2eAgInSc1d1	erythromycin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
mírnými	mírný	k2eAgFnPc7d1	mírná
nebo	nebo	k8xC	nebo
středně	středně	k6eAd1	středně
těžkými	těžký	k2eAgInPc7d1	těžký
symptomy	symptom	k1gInPc7	symptom
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
amoxicilin	amoxicilin	k1gInSc1	amoxicilin
stále	stále	k6eAd1	stále
první	první	k4xOgFnSc7	první
volbou	volba	k1gFnSc7	volba
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
fluorochinolonů	fluorochinolon	k1gInPc2	fluorochinolon
v	v	k7c6	v
nekomplikovaných	komplikovaný	k2eNgInPc6d1	nekomplikovaný
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
obav	obava	k1gFnPc2	obava
o	o	k7c4	o
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
rezistence	rezistence	k1gFnSc2	rezistence
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
klinický	klinický	k2eAgInSc4d1	klinický
přínos	přínos	k1gInSc4	přínos
nevyvažuje	vyvažovat	k5eNaImIp3nS	vyvažovat
možné	možný	k2eAgNnSc1d1	možné
riziko	riziko	k1gNnSc1	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
léčení	léčení	k1gNnSc2	léčení
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
sedmi	sedm	k4xCc2	sedm
až	až	k9	až
deseti	deset	k4xCc2	deset
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
ale	ale	k9	ale
objevují	objevovat	k5eAaImIp3nP	objevovat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
kratší	krátký	k2eAgFnSc1d2	kratší
léčba	léčba	k1gFnSc1	léčba
(	(	kIx(	(
<g/>
tři	tři	k4xCgNnPc4	tři
až	až	k9	až
pět	pět	k4xCc4	pět
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obdobně	obdobně	k6eAd1	obdobně
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
.	.	kIx.	.
</s>
<s>
Doporučené	doporučený	k2eAgInPc1d1	doporučený
léky	lék	k1gInPc1	lék
pro	pro	k7c4	pro
pneumonii	pneumonie	k1gFnSc4	pneumonie
získanou	získaný	k2eAgFnSc4d1	získaná
v	v	k7c6	v
nemocničním	nemocniční	k2eAgNnSc6d1	nemocniční
prostředí	prostředí	k1gNnSc6	prostředí
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
cefalosporiny	cefalosporin	k1gInPc1	cefalosporin
třetí	třetí	k4xOgNnSc1	třetí
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgFnPc1	čtvrtý
generace	generace	k1gFnPc1	generace
<g/>
,	,	kIx,	,
karbapenemy	karbapenem	k1gInPc1	karbapenem
<g/>
,	,	kIx,	,
fluorochinolony	fluorochinolon	k1gInPc1	fluorochinolon
<g/>
,	,	kIx,	,
aminoglykosidy	aminoglykosid	k1gInPc1	aminoglykosid
a	a	k8xC	a
vankomycin	vankomycin	k1gInSc1	vankomycin
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
podávána	podávat	k5eAaImNgFnS	podávat
nitrožilně	nitrožilně	k6eAd1	nitrožilně
a	a	k8xC	a
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
osob	osoba	k1gFnPc2	osoba
léčených	léčený	k2eAgFnPc2d1	léčená
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
se	se	k3xPyFc4	se
po	po	k7c6	po
počátečním	počáteční	k2eAgNnSc6d1	počáteční
podání	podání	k1gNnSc6	podání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
v	v	k7c6	v
90	[number]	k4	90
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
virové	virový	k2eAgFnSc2d1	virová
pneumonie	pneumonie	k1gFnSc2	pneumonie
způsobené	způsobený	k2eAgFnSc2d1	způsobená
viry	vira	k1gFnSc2	vira
(	(	kIx(	(
<g/>
influenzy	influenza	k1gFnSc2	influenza
A	A	kA	A
a	a	k8xC	a
influenzy	influenza	k1gFnSc2	influenza
B	B	kA	B
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použit	k2eAgInPc4d1	použit
inhibitory	inhibitor	k1gInPc4	inhibitor
neuraminidázy	neuraminidáza	k1gFnSc2	neuraminidáza
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
ostatních	ostatní	k2eAgInPc2d1	ostatní
typů	typ	k1gInPc2	typ
komunitní	komunitní	k2eAgFnSc2d1	komunitní
virové	virový	k2eAgFnSc2d1	virová
pneumonie	pneumonie	k1gFnSc2	pneumonie
včetně	včetně	k7c2	včetně
onemocnění	onemocnění	k1gNnSc2	onemocnění
způsobeného	způsobený	k2eAgNnSc2d1	způsobené
koronavirem	koronavirem	k6eAd1	koronavirem
SARS	SARS	kA	SARS
<g/>
,	,	kIx,	,
adenovirem	adenovir	k1gInSc7	adenovir
<g/>
,	,	kIx,	,
hantavirem	hantavir	k1gInSc7	hantavir
a	a	k8xC	a
virem	vir	k1gInSc7	vir
parainfluenzy	parainfluenza	k1gFnSc2	parainfluenza
nejsou	být	k5eNaImIp3nP	být
doporučována	doporučovat	k5eAaImNgFnS	doporučovat
žádná	žádný	k3yNgFnSc1	žádný
specifická	specifický	k2eAgFnSc1d1	specifická
antivirotika	antivirotika	k1gFnSc1	antivirotika
<g/>
.	.	kIx.	.
</s>
<s>
Influenza	influenza	k1gFnSc1	influenza
A	a	k9	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
léčena	léčen	k2eAgFnSc1d1	léčena
rimantadinem	rimantadino	k1gNnSc7	rimantadino
nebo	nebo	k8xC	nebo
amantadinem	amantadino	k1gNnSc7	amantadino
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
influenzu	influenza	k1gFnSc4	influenza
typu	typ	k1gInSc2	typ
A	A	kA	A
nebo	nebo	k8xC	nebo
B	B	kA	B
lze	lze	k6eAd1	lze
léčit	léčit	k5eAaImF	léčit
oseltamivirem	oseltamivirem	k6eAd1	oseltamivirem
<g/>
,	,	kIx,	,
zanamivirem	zanamivirem	k6eAd1	zanamivirem
nebo	nebo	k8xC	nebo
peramivirem	peramivirem	k6eAd1	peramivirem
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
účinku	účinek	k1gInSc2	účinek
tyto	tento	k3xDgInPc4	tento
léky	lék	k1gInPc4	lék
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
podány	podat	k5eAaPmNgInP	podat
do	do	k7c2	do
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
symptomů	symptom	k1gInPc2	symptom
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
kmenů	kmen	k1gInPc2	kmen
influenzy	influenza	k1gFnSc2	influenza
A	A	kA	A
<g/>
,	,	kIx,	,
podtypu	podtyp	k1gInSc2	podtyp
H5N1	H5N1	k1gFnPc2	H5N1
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xS	jako
aviární	aviární	k2eAgFnSc1d1	aviární
influenza	influenza	k1gFnSc1	influenza
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
ptačí	ptačí	k2eAgFnSc1d1	ptačí
chřipka	chřipka	k1gFnSc1	chřipka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
rezistenci	rezistence	k1gFnSc4	rezistence
na	na	k7c4	na
rimantadin	rimantadin	k2eAgInSc4d1	rimantadin
a	a	k8xC	a
amantadin	amantadin	k2eAgInSc4d1	amantadin
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
nasazení	nasazení	k1gNnSc4	nasazení
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
i	i	k8xC	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
virové	virový	k2eAgFnSc2d1	virová
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vyloučit	vyloučit	k5eAaPmF	vyloučit
komplikující	komplikující	k2eAgFnSc4d1	komplikující
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
infekci	infekce	k1gFnSc4	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
společnost	společnost	k1gFnSc1	společnost
hrudního	hrudní	k2eAgNnSc2d1	hrudní
lékařství	lékařství	k1gNnSc2	lékařství
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
zdržet	zdržet	k5eAaPmF	zdržet
se	se	k3xPyFc4	se
podávání	podávání	k1gNnSc6	podávání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
u	u	k7c2	u
mírných	mírný	k2eAgInPc2d1	mírný
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
kortikosteroidů	kortikosteroid	k1gInPc2	kortikosteroid
je	být	k5eAaImIp3nS	být
kontroverzní	kontroverzní	k2eAgInSc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
aspirační	aspirační	k2eAgFnSc1d1	aspirační
pneumonitida	pneumonitida	k1gFnSc1	pneumonitida
léčí	léčit	k5eAaImIp3nS	léčit
konzervativně	konzervativně	k6eAd1	konzervativně
za	za	k7c4	za
nasazení	nasazení	k1gNnSc4	nasazení
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
určených	určený	k2eAgFnPc2d1	určená
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
aspirační	aspirační	k2eAgFnSc2d1	aspirační
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
několika	několik	k4yIc6	několik
faktorech	faktor	k1gInPc6	faktor
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
na	na	k7c6	na
domnělém	domnělý	k2eAgMnSc6d1	domnělý
organickém	organický	k2eAgMnSc6d1	organický
původci	původce	k1gMnSc6	původce
a	a	k8xC	a
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
k	k	k7c3	k
pneumonii	pneumonie	k1gFnSc3	pneumonie
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
komunitě	komunita	k1gFnSc6	komunita
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
v	v	k7c6	v
nemocničním	nemocniční	k2eAgNnSc6d1	nemocniční
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
volbou	volba	k1gFnSc7	volba
jsou	být	k5eAaImIp3nP	být
klindamycin	klindamycin	k1gMnSc1	klindamycin
<g/>
,	,	kIx,	,
kombinace	kombinace	k1gFnSc1	kombinace
beta-laktamového	betaaktamový	k2eAgNnSc2d1	beta-laktamový
antibiotika	antibiotikum	k1gNnSc2	antibiotikum
a	a	k8xC	a
metronidazolu	metronidazol	k1gInSc2	metronidazol
nebo	nebo	k8xC	nebo
aminoglykosid	aminoglykosida	k1gFnPc2	aminoglykosida
<g/>
.	.	kIx.	.
</s>
<s>
Kortikosteroidy	kortikosteroid	k1gInPc1	kortikosteroid
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
aspirační	aspirační	k2eAgFnSc2d1	aspirační
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
omezené	omezený	k2eAgInPc1d1	omezený
důkazy	důkaz	k1gInPc1	důkaz
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
jejich	jejich	k3xOp3gFnSc2	jejich
účinnosti	účinnost	k1gFnSc2	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
léčbě	léčba	k1gFnSc3	léčba
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
typů	typ	k1gInPc2	typ
bakteriálního	bakteriální	k2eAgInSc2d1	bakteriální
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
do	do	k7c2	do
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Odeznění	odeznění	k1gNnSc1	odeznění
většiny	většina	k1gFnSc2	většina
symptomů	symptom	k1gInPc2	symptom
většinou	většinou	k6eAd1	většinou
trvá	trvat	k5eAaImIp3nS	trvat
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Rentgenový	rentgenový	k2eAgInSc1d1	rentgenový
nález	nález	k1gInSc1	nález
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
čistý	čistý	k2eAgInSc1d1	čistý
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
osob	osoba	k1gFnPc2	osoba
nebo	nebo	k8xC	nebo
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
plicními	plicní	k2eAgInPc7d1	plicní
problémy	problém	k1gInPc7	problém
může	moct	k5eAaImIp3nS	moct
vyléčení	vyléčení	k1gNnSc1	vyléčení
trvat	trvat	k5eAaImF	trvat
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
12	[number]	k4	12
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
hospitalizace	hospitalizace	k1gFnSc1	hospitalizace
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
a	a	k8xC	a
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
péče	péče	k1gFnSc1	péče
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
je	být	k5eAaImIp3nS	být
nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
infekcí	infekce	k1gFnSc7	infekce
získanou	získaný	k2eAgFnSc7d1	získaná
v	v	k7c6	v
nemocničním	nemocniční	k2eAgNnSc6d1	nemocniční
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
končí	končit	k5eAaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
byla	být	k5eAaImAgFnS	být
typická	typický	k2eAgFnSc1d1	typická
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
hospitalizovaných	hospitalizovaný	k2eAgFnPc2d1	hospitalizovaná
osob	osoba	k1gFnPc2	osoba
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
pacientů	pacient	k1gMnPc2	pacient
a	a	k8xC	a
osob	osoba	k1gFnPc2	osoba
se	s	k7c7	s
zdravotními	zdravotní	k2eAgInPc7d1	zdravotní
problémy	problém	k1gInPc7	problém
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
komplikace	komplikace	k1gFnPc1	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těm	ten	k3xDgInPc3	ten
mimo	mimo	k6eAd1	mimo
jiné	jiné	k1gNnSc1	jiné
patří	patřit	k5eAaImIp3nS	patřit
empyém	empyém	k1gInSc4	empyém
<g/>
,	,	kIx,	,
absces	absces	k1gInSc4	absces
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
obliterující	obliterující	k2eAgFnSc1d1	obliterující
bronchiolitida	bronchiolitida	k1gFnSc1	bronchiolitida
<g/>
,	,	kIx,	,
akutní	akutní	k2eAgInSc1d1	akutní
syndrom	syndrom	k1gInSc1	syndrom
respirační	respirační	k2eAgFnSc2d1	respirační
tísně	tíseň	k1gFnSc2	tíseň
<g/>
,	,	kIx,	,
sepse	sepse	k1gFnSc2	sepse
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
zhoršený	zhoršený	k2eAgInSc4d1	zhoršený
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
klinické	klinický	k2eAgFnSc2d1	klinická
predikce	predikce	k1gFnSc2	predikce
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
pro	pro	k7c4	pro
objektivnější	objektivní	k2eAgFnSc4d2	objektivnější
prognózu	prognóza	k1gFnSc4	prognóza
závěrů	závěr	k1gInPc2	závěr
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
zápalu	zápal	k1gInSc3	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
používána	používat	k5eAaImNgNnP	používat
při	při	k7c6	při
rozhodování	rozhodování	k1gNnSc6	rozhodování
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
osobu	osoba	k1gFnSc4	osoba
hospitalizovat	hospitalizovat	k5eAaBmF	hospitalizovat
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Index	index	k1gInSc1	index
závažnosti	závažnost	k1gFnSc2	závažnost
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
skóre	skóre	k1gNnSc3	skóre
PSI	pes	k1gMnPc1	pes
<g/>
)	)	kIx)	)
Skóre	skóre	k1gNnSc1	skóre
CURB-	CURB-	k1gFnSc2	CURB-
<g/>
65	[number]	k4	65
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bere	brát	k5eAaImIp3nS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
závažnost	závažnost	k1gFnSc4	závažnost
symptomů	symptom	k1gInPc2	symptom
<g/>
,	,	kIx,	,
základní	základní	k2eAgNnSc4d1	základní
onemocnění	onemocnění	k1gNnSc4	onemocnění
a	a	k8xC	a
věk	věk	k1gInSc4	věk
Při	při	k7c6	při
zápalu	zápal	k1gInSc6	zápal
plic	plíce	k1gFnPc2	plíce
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nahromadění	nahromadění	k1gNnSc3	nahromadění
tekutiny	tekutina	k1gFnSc2	tekutina
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
obklopujícím	obklopující	k2eAgInSc6d1	obklopující
plíci	plíce	k1gFnSc4	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tekutina	tekutina	k1gFnSc1	tekutina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
jistých	jistý	k2eAgInPc6d1	jistý
případech	případ	k1gInPc6	případ
infikována	infikován	k2eAgFnSc1d1	infikována
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
a	a	k8xC	a
způsobit	způsobit	k5eAaPmF	způsobit
tak	tak	k6eAd1	tak
empyém	empyém	k1gInSc4	empyém
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
empyému	empyém	k1gInSc2	empyém
od	od	k7c2	od
běžnější	běžný	k2eAgFnSc2d2	běžnější
prosté	prostý	k2eAgFnSc2d1	prostá
parapneumonické	parapneumonický	k2eAgFnSc2d1	parapneumonický
efuze	efuze	k1gFnSc2	efuze
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tekutina	tekutina	k1gFnSc1	tekutina
odebrána	odebrán	k2eAgFnSc1d1	odebrána
pomocí	pomocí	k7c2	pomocí
jehly	jehla	k1gFnSc2	jehla
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
(	(	kIx(	(
<g/>
hrudní	hrudní	k2eAgFnSc1d1	hrudní
punkce	punkce	k1gFnSc1	punkce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
prokáže	prokázat	k5eAaPmIp3nS	prokázat
empyém	empyém	k1gInSc4	empyém
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
úplná	úplný	k2eAgFnSc1d1	úplná
drenáž	drenáž	k1gFnSc1	drenáž
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
často	často	k6eAd1	často
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
aplikaci	aplikace	k1gFnSc4	aplikace
drenážního	drenážní	k2eAgInSc2d1	drenážní
katetru	katetr	k1gInSc2	katetr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závažných	závažný	k2eAgInPc6d1	závažný
případech	případ	k1gInPc6	případ
empyému	empyém	k1gInSc2	empyém
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nutné	nutný	k2eAgNnSc1d1	nutné
provést	provést	k5eAaPmF	provést
chirurgický	chirurgický	k2eAgInSc4d1	chirurgický
zákrok	zákrok	k1gInSc4	zákrok
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
infikovaná	infikovaný	k2eAgFnSc1d1	infikovaná
tekutina	tekutina	k1gFnSc1	tekutina
odvedena	odveden	k2eAgFnSc1d1	odvedena
<g/>
,	,	kIx,	,
infekce	infekce	k1gFnSc1	infekce
může	moct	k5eAaImIp3nS	moct
přetrvávat	přetrvávat	k5eAaImF	přetrvávat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
nemají	mít	k5eNaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
dostatečně	dostatečně	k6eAd1	dostatečně
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
pleurální	pleurální	k2eAgFnSc2d1	pleurální
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tekutina	tekutina	k1gFnSc1	tekutina
sterilní	sterilní	k2eAgFnSc1d1	sterilní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ji	on	k3xPp3gFnSc4	on
odvést	odvést	k5eAaPmF	odvést
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
symptomy	symptom	k1gInPc1	symptom
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nevstřebává	vstřebávat	k5eNaImIp3nS	vstřebávat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzácných	vzácný	k2eAgInPc6d1	vzácný
případech	případ	k1gInPc6	případ
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
bakterie	bakterie	k1gFnSc1	bakterie
v	v	k7c6	v
plíci	plíce	k1gFnSc6	plíce
dutinu	dutina	k1gFnSc4	dutina
naplněnou	naplněný	k2eAgFnSc7d1	naplněná
infikovanou	infikovaný	k2eAgFnSc7d1	infikovaná
tekutinou	tekutina	k1gFnSc7	tekutina
zvanou	zvaný	k2eAgFnSc7d1	zvaná
plicní	plicní	k2eAgMnPc1d1	plicní
absces	absces	k1gInSc4	absces
<g/>
.	.	kIx.	.
</s>
<s>
Plicní	plicní	k2eAgInPc1d1	plicní
abscesy	absces	k1gInPc1	absces
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
viditelné	viditelný	k2eAgFnPc1d1	viditelná
na	na	k7c6	na
rentgenovém	rentgenový	k2eAgInSc6d1	rentgenový
snímku	snímek	k1gInSc6	snímek
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
potvrzení	potvrzení	k1gNnSc4	potvrzení
diagnózy	diagnóza	k1gFnSc2	diagnóza
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
často	často	k6eAd1	často
zapotřebí	zapotřebí	k6eAd1	zapotřebí
CT	CT	kA	CT
snímek	snímek	k1gInSc4	snímek
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
.	.	kIx.	.
</s>
<s>
Abscesy	absces	k1gInPc1	absces
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
objevují	objevovat	k5eAaImIp3nP	objevovat
při	při	k7c6	při
aspirační	aspirační	k2eAgFnSc6d1	aspirační
pneumonii	pneumonie	k1gFnSc6	pneumonie
a	a	k8xC	a
často	často	k6eAd1	často
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Plicní	plicní	k2eAgInSc1d1	plicní
absces	absces	k1gInSc1	absces
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
léčí	léčit	k5eAaImIp3nS	léčit
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
podávanými	podávaný	k2eAgNnPc7d1	podávané
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ale	ale	k9	ale
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
absces	absces	k1gInSc1	absces
vyprázdněn	vyprázdněn	k2eAgInSc1d1	vyprázdněn
chirurgem	chirurg	k1gMnSc7	chirurg
nebo	nebo	k8xC	nebo
radiologem	radiolog	k1gMnSc7	radiolog
<g/>
.	.	kIx.	.
</s>
<s>
Zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
respirační	respirační	k2eAgNnSc4d1	respirační
selhání	selhání	k1gNnSc4	selhání
spuštěním	spuštění	k1gNnSc7	spuštění
syndromu	syndrom	k1gInSc2	syndrom
akutní	akutní	k2eAgFnSc2d1	akutní
respirační	respirační	k2eAgFnSc2d1	respirační
tísně	tíseň	k1gFnSc2	tíseň
(	(	kIx(	(
<g/>
ARDS	ARDS	kA	ARDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kombinace	kombinace	k1gFnSc2	kombinace
infekce	infekce	k1gFnSc2	infekce
a	a	k8xC	a
zánětlivé	zánětlivý	k2eAgFnSc2d1	zánětlivá
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Plíce	plíce	k1gFnSc1	plíce
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zaplní	zaplnit	k5eAaPmIp3nP	zaplnit
tekutinou	tekutina	k1gFnSc7	tekutina
a	a	k8xC	a
ztuhnou	ztuhnout	k5eAaPmIp3nP	ztuhnout
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
vážnými	vážný	k2eAgFnPc7d1	vážná
potížemi	potíž	k1gFnPc7	potíž
s	s	k7c7	s
absorpcí	absorpce	k1gFnSc7	absorpce
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přítomnosti	přítomnost	k1gFnSc2	přítomnost
alveolární	alveolární	k2eAgFnSc2d1	alveolární
tekutiny	tekutina	k1gFnSc2	tekutina
může	moct	k5eAaImIp3nS	moct
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
období	období	k1gNnSc4	období
mechanické	mechanický	k2eAgFnSc2d1	mechanická
ventilace	ventilace	k1gFnSc2	ventilace
nutné	nutný	k2eAgNnSc4d1	nutné
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pacient	pacient	k1gMnSc1	pacient
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Sepse	sepse	k1gFnSc1	sepse
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
potenciální	potenciální	k2eAgFnSc7d1	potenciální
komplikací	komplikace	k1gFnSc7	komplikace
u	u	k7c2	u
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
ale	ale	k8xC	ale
dochází	docházet	k5eAaImIp3nS	docházet
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
imunitou	imunita	k1gFnSc7	imunita
nebo	nebo	k8xC	nebo
se	s	k7c7	s
sníženou	snížený	k2eAgFnSc7d1	snížená
funkcí	funkce	k1gFnSc7	funkce
sleziny	slezina	k1gFnSc2	slezina
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnějšími	běžný	k2eAgMnPc7d3	Nejběžnější
organickými	organický	k2eAgMnPc7d1	organický
původci	původce	k1gMnPc7	původce
jsou	být	k5eAaImIp3nP	být
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pneumoniae	pneumonia	k1gFnSc2	pneumonia
<g/>
,	,	kIx,	,
Haemophilus	Haemophilus	k1gMnSc1	Haemophilus
influenzae	influenza	k1gFnSc2	influenza
a	a	k8xC	a
Klebsiella	Klebsiello	k1gNnSc2	Klebsiello
pneumoniae	pneumonia	k1gInSc2	pneumonia
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
příčiny	příčina	k1gFnPc4	příčina
symptomů	symptom	k1gInPc2	symptom
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zvážit	zvážit	k5eAaPmF	zvážit
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
nebo	nebo	k8xC	nebo
plicní	plicní	k2eAgFnSc2d1	plicní
embolie	embolie	k1gFnSc2	embolie
<g/>
.	.	kIx.	.
</s>
<s>
Zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
je	být	k5eAaImIp3nS	být
běžným	běžný	k2eAgNnSc7d1	běžné
onemocněním	onemocnění	k1gNnSc7	onemocnění
vyskytujícím	vyskytující	k2eAgInPc3d1	vyskytující
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
a	a	k8xC	a
postihujícím	postihující	k2eAgInSc7d1	postihující
přibližně	přibližně	k6eAd1	přibližně
450	[number]	k4	450
miliónů	milión	k4xCgInPc2	milión
osob	osoba	k1gFnPc2	osoba
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
napříč	napříč	k7c7	napříč
všemi	všecek	k3xTgFnPc7	všecek
věkovými	věkový	k2eAgFnPc7d1	věková
skupinami	skupina	k1gFnPc7	skupina
-	-	kIx~	-
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
4	[number]	k4	4
milióny	milión	k4xCgInPc7	milión
úmrtí	úmrtí	k1gNnPc2	úmrtí
(	(	kIx(	(
<g/>
7	[number]	k4	7
%	%	kIx~	%
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
počtu	počet	k1gInSc2	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
)	)	kIx)	)
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
mladších	mladý	k2eAgNnPc2d2	mladší
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
a	a	k8xC	a
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
starších	starší	k1gMnPc2	starší
75	[number]	k4	75
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pneumonie	pneumonie	k1gFnSc1	pneumonie
se	se	k3xPyFc4	se
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
přibližně	přibližně	k6eAd1	přibližně
pětkrát	pětkrát	k6eAd1	pětkrát
častěji	často	k6eAd2	často
než	než	k8xS	než
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
<g/>
.	.	kIx.	.
</s>
<s>
Virový	virový	k2eAgInSc1d1	virový
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
milióny	milión	k4xCgInPc4	milión
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
8	[number]	k4	8
<g/>
.	.	kIx.	.
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
onemocnělo	onemocnět	k5eAaPmAgNnS	onemocnět
pneumonií	pneumonie	k1gFnSc7	pneumonie
přibližně	přibližně	k6eAd1	přibližně
156	[number]	k4	156
miliónů	milión	k4xCgInPc2	milión
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
151	[number]	k4	151
miliónů	milión	k4xCgInPc2	milión
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Choroba	choroba	k1gFnSc1	choroba
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
1,6	[number]	k4	1,6
miliónů	milión	k4xCgInPc2	milión
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
úmrtí	úmrť	k1gFnPc2	úmrť
dětí	dítě	k1gFnPc2	dítě
mladších	mladý	k2eAgInPc2d2	mladší
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
k	k	k7c3	k
95	[number]	k4	95
%	%	kIx~	%
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
s	s	k7c7	s
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
výskytem	výskyt	k1gInSc7	výskyt
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
patří	patřit	k5eAaImIp3nS	patřit
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
43	[number]	k4	43
miliónů	milión	k4xCgInPc2	milión
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
21	[number]	k4	21
miliónů	milión	k4xCgInPc2	milión
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pákistán	Pákistán	k1gInSc1	Pákistán
(	(	kIx(	(
<g/>
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
nízkopříjmových	nízkopříjmův	k2eAgFnPc6d1	nízkopříjmův
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
těmto	tento	k3xDgNnPc3	tento
úmrtím	úmrtí	k1gNnPc3	úmrtí
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
novorozeneckém	novorozenecký	k2eAgInSc6d1	novorozenecký
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
každé	každý	k3xTgNnSc4	každý
třetí	třetí	k4xOgNnSc4	třetí
úmrtí	úmrtí	k1gNnSc4	úmrtí
novorozeněte	novorozeně	k1gNnSc2	novorozeně
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
polovině	polovina	k1gFnSc3	polovina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
by	by	kYmCp3nS	by
teoreticky	teoreticky	k6eAd1	teoreticky
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
způsobena	způsoben	k2eAgFnSc1d1	způsobena
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
účinná	účinný	k2eAgFnSc1d1	účinná
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
běžným	běžný	k2eAgNnSc7d1	běžné
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc4	jeho
symptomy	symptom	k1gInPc4	symptom
popsal	popsat	k5eAaPmAgMnS	popsat
už	už	k9	už
Hippokratés	Hippokratés	k1gInSc4	Hippokratés
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
460	[number]	k4	460
př	př	kA	př
<g/>
.	.	kIx.	.
Kr.	Kr.	k1gFnSc1	Kr.
–	–	k?	–
370	[number]	k4	370
př	př	kA	př
<g/>
.	.	kIx.	.
Kr	Kr	k1gMnSc1	Kr
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Peripneumonie	Peripneumonie	k1gFnSc1	Peripneumonie
a	a	k8xC	a
pleurální	pleurální	k2eAgNnSc1d1	pleurální
onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
příznaků	příznak	k1gInPc2	příznak
<g/>
:	:	kIx,	:
Horečka	horečka	k1gFnSc1	horečka
je	být	k5eAaImIp3nS	být
akutní	akutní	k2eAgFnSc1d1	akutní
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc1	bolest
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
nebo	nebo	k8xC	nebo
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
hrudi	hruď	k1gFnSc2	hruď
<g/>
,	,	kIx,	,
nemocný	mocný	k2eNgMnSc1d1	nemocný
při	při	k7c6	při
výdechu	výdech	k1gInSc6	výdech
kašle	kašlat	k5eAaImIp3nS	kašlat
a	a	k8xC	a
vykašlávané	vykašlávaný	k2eAgNnSc1d1	vykašlávaný
sputum	sputum	k1gNnSc1	sputum
je	být	k5eAaImIp3nS	být
světlé	světlý	k2eAgNnSc1d1	světlé
či	či	k8xC	či
popelavě	popelavě	k6eAd1	popelavě
šedé	šedý	k2eAgFnPc1d1	šedá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
řídké	řídký	k2eAgNnSc1d1	řídké
<g/>
,	,	kIx,	,
zpěněné	zpěněný	k2eAgNnSc1d1	zpěněné
či	či	k8xC	či
načervenalé	načervenalý	k2eAgNnSc1d1	načervenalé
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
jiný	jiný	k2eAgInSc4d1	jiný
vzhled	vzhled	k1gInSc4	vzhled
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
...	...	k?	...
Když	když	k8xS	když
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
pročištění	pročištění	k1gNnSc2	pročištění
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
nemocného	nemocný	k1gMnSc2	nemocný
<g />
.	.	kIx.	.
</s>
<s>
beznadějný	beznadějný	k2eAgMnSc1d1	beznadějný
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
není	být	k5eNaImIp3nS	být
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nemocný	nemocný	k1gMnSc1	nemocný
trpí	trpět	k5eAaImIp3nS	trpět
dušností	dušnost	k1gFnSc7	dušnost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
moč	moč	k1gFnSc1	moč
je	být	k5eAaImIp3nS	být
řídká	řídký	k2eAgFnSc1d1	řídká
a	a	k8xC	a
čpavá	čpavý	k2eAgFnSc1d1	čpavá
a	a	k8xC	a
krk	krk	k1gInSc1	krk
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
se	se	k3xPyFc4	se
potí	potit	k5eAaImIp3nS	potit
<g/>
,	,	kIx,	,
takový	takový	k3xDgInSc1	takový
pot	pot	k1gInSc1	pot
není	být	k5eNaImIp3nS	být
dobrým	dobrý	k2eAgNnSc7d1	dobré
znamením	znamení	k1gNnSc7	znamení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dušení	dušení	k1gNnSc2	dušení
<g/>
,	,	kIx,	,
chrastění	chrastěný	k2eAgMnPc1d1	chrastěný
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
a	a	k8xC	a
prudkosti	prudkost	k1gFnSc6	prudkost
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nad	nad	k7c7	nad
nemocným	nemocný	k1gMnPc3	nemocný
získává	získávat	k5eAaImIp3nS	získávat
převahu	převaha	k1gFnSc4	převaha
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hippokratés	Hippokratés	k1gInSc1	Hippokratés
však	však	k9	však
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
nemoc	nemoc	k1gFnSc4	nemoc
"	"	kIx"	"
<g/>
zmiňovanou	zmiňovaný	k2eAgFnSc4d1	zmiňovaná
už	už	k6eAd1	už
předky	předek	k1gMnPc7	předek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
rovněž	rovněž	k9	rovněž
výsledky	výsledek	k1gInPc4	výsledek
chirurgické	chirurgický	k2eAgFnSc2d1	chirurgická
drenáže	drenáž	k1gFnSc2	drenáž
empyému	empyém	k1gInSc2	empyém
<g/>
.	.	kIx.	.
</s>
<s>
Maimonides	Maimonides	k1gInSc1	Maimonides
(	(	kIx(	(
<g/>
1135	[number]	k4	1135
<g/>
–	–	k?	–
<g/>
1204	[number]	k4	1204
<g/>
)	)	kIx)	)
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Základními	základní	k2eAgInPc7d1	základní
symptomy	symptom	k1gInPc7	symptom
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
u	u	k7c2	u
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
vždy	vždy	k6eAd1	vždy
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
akutní	akutní	k2eAgFnSc1d1	akutní
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
bodavá	bodavý	k2eAgFnSc1d1	bodavá
pleuritická	pleuritický	k2eAgFnSc1d1	pleuritický
bolest	bolest	k1gFnSc1	bolest
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
<g/>
,	,	kIx,	,
krátké	krátký	k2eAgNnSc4d1	krátké
zrychlené	zrychlený	k2eAgNnSc4d1	zrychlené
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
,	,	kIx,	,
zrychlený	zrychlený	k2eAgInSc4d1	zrychlený
puls	puls	k1gInSc4	puls
a	a	k8xC	a
kašel	kašel	k1gInSc4	kašel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tento	tento	k3xDgInSc1	tento
klinický	klinický	k2eAgInSc1d1	klinický
popis	popis	k1gInSc1	popis
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
podobný	podobný	k2eAgInSc1d1	podobný
charakteristikám	charakteristika	k1gFnPc3	charakteristika
objevujícím	objevující	k2eAgInPc3d1	objevující
se	se	k3xPyFc4	se
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
učebnicích	učebnice	k1gFnPc6	učebnice
a	a	k8xC	a
odráží	odrážet	k5eAaImIp3nS	odrážet
rozsah	rozsah	k1gInSc4	rozsah
lékařských	lékařský	k2eAgFnPc2d1	lékařská
znalostí	znalost	k1gFnPc2	znalost
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Edwin	Edwin	k2eAgInSc1d1	Edwin
Klebs	Klebs	k1gInSc1	Klebs
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zjistil	zjistit	k5eAaPmAgMnS	zjistit
přítomnost	přítomnost	k1gFnSc4	přítomnost
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
dýchacích	dýchací	k2eAgFnPc6d1	dýchací
cestách	cesta	k1gFnPc6	cesta
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zemřely	zemřít	k5eAaPmAgFnP	zemřít
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Průkopnickou	průkopnický	k2eAgFnSc4d1	průkopnická
práci	práce	k1gFnSc4	práce
identifikující	identifikující	k2eAgFnPc1d1	identifikující
dvě	dva	k4xCgFnPc1	dva
běžné	běžný	k2eAgFnPc1d1	běžná
bakteriální	bakteriální	k2eAgFnPc1d1	bakteriální
příčiny	příčina	k1gFnPc1	příčina
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
,	,	kIx,	,
Streptococcus	Streptococcus	k1gInSc4	Streptococcus
pneumoniae	pneumonia	k1gMnSc2	pneumonia
a	a	k8xC	a
Klebsiella	Klebsiell	k1gMnSc2	Klebsiell
pneumoniae	pneumonia	k1gMnSc2	pneumonia
<g/>
,	,	kIx,	,
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
Carl	Carl	k1gMnSc1	Carl
Friedländer	Friedländer	k1gMnSc1	Friedländer
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
Fränkel	Fränkel	k1gMnSc1	Fränkel
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1882	[number]	k4	1882
a	a	k8xC	a
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Friedländerova	Friedländerův	k2eAgFnSc1d1	Friedländerův
původní	původní	k2eAgFnSc1d1	původní
práce	práce	k1gFnSc1	práce
poprvé	poprvé	k6eAd1	poprvé
představila	představit	k5eAaPmAgFnS	představit
Gramovo	Gramův	k2eAgNnSc4d1	Gramovo
barvení	barvení	k1gNnSc4	barvení
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
základní	základní	k2eAgInSc4d1	základní
laboratorní	laboratorní	k2eAgInSc4d1	laboratorní
test	test	k1gInSc4	test
používaný	používaný	k2eAgInSc4d1	používaný
k	k	k7c3	k
identifikaci	identifikace	k1gFnSc3	identifikace
a	a	k8xC	a
kategorizování	kategorizování	k1gNnSc3	kategorizování
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
Christiana	Christian	k1gMnSc2	Christian
Grama	Gram	k1gMnSc2	Gram
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
popisující	popisující	k2eAgInSc4d1	popisující
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
pomohla	pomoct	k5eAaPmAgFnS	pomoct
rozlišit	rozlišit	k5eAaPmF	rozlišit
dvě	dva	k4xCgFnPc4	dva
výše	výše	k1gFnPc4	výše
uvedené	uvedený	k2eAgFnSc2d1	uvedená
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
prokázala	prokázat	k5eAaPmAgFnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
mikroorganismus	mikroorganismus	k1gInSc4	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc4	William
Osler	Osler	k1gMnSc1	Osler
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
rovněž	rovněž	k9	rovněž
jako	jako	k9	jako
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
moderní	moderní	k2eAgFnSc2d1	moderní
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
byl	být	k5eAaImAgInS	být
plně	plně	k6eAd1	plně
vědom	vědom	k2eAgMnSc1d1	vědom
počtu	počet	k1gInSc3	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
a	a	k8xC	a
postižení	postižení	k1gNnSc2	postižení
způsobených	způsobený	k2eAgFnPc2d1	způsobená
zápalem	zápal	k1gInSc7	zápal
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
jej	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
kapitána	kapitán	k1gMnSc4	kapitán
pěšáků	pěšák	k1gMnPc2	pěšák
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
tato	tento	k3xDgFnSc1	tento
choroba	choroba	k1gFnSc1	choroba
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
jakožto	jakožto	k8xS	jakožto
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
příčin	příčina	k1gFnPc2	příčina
úmrtí	úmrtí	k1gNnSc4	úmrtí
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgInS	použít
John	John	k1gMnSc1	John
Bunyan	Bunyan	k1gMnSc1	Bunyan
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
souchotinami	souchotiny	k1gFnPc7	souchotiny
(	(	kIx(	(
<g/>
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osler	Osler	k1gMnSc1	Osler
rovněž	rovněž	k9	rovněž
popsal	popsat	k5eAaPmAgMnS	popsat
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
přítele	přítel	k1gMnSc4	přítel
starců	stařec	k1gMnPc2	stařec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
smrt	smrt	k1gFnSc1	smrt
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
pomalejšími	pomalý	k2eAgInPc7d2	pomalejší
a	a	k8xC	a
bolestivějšími	bolestivý	k2eAgInPc7d2	bolestivější
způsoby	způsob	k1gInPc7	způsob
smrti	smrt	k1gFnSc2	smrt
často	často	k6eAd1	často
rychlá	rychlý	k2eAgFnSc1d1	rychlá
a	a	k8xC	a
bezbolestná	bezbolestný	k2eAgFnSc1d1	bezbolestná
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
léčby	léčba	k1gFnSc2	léčba
pacientů	pacient	k1gMnPc2	pacient
se	s	k7c7	s
zápalem	zápal	k1gInSc7	zápal
plic	plíce	k1gFnPc2	plíce
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zlepšilo	zlepšit	k5eAaPmAgNnS	zlepšit
několik	několik	k4yIc4	několik
objevů	objev	k1gInPc2	objev
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
penicilinu	penicilin	k1gInSc2	penicilin
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
,	,	kIx,	,
moderních	moderní	k2eAgFnPc2d1	moderní
chirurgických	chirurgický	k2eAgFnPc2d1	chirurgická
technik	technika	k1gFnPc2	technika
a	a	k8xC	a
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
péče	péče	k1gFnSc2	péče
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
předtím	předtím	k6eAd1	předtím
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
až	až	k9	až
30	[number]	k4	30
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
výrazně	výrazně	k6eAd1	výrazně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
započala	započnout	k5eAaPmAgFnS	započnout
vakcinace	vakcinace	k1gFnSc1	vakcinace
kojenců	kojenec	k1gMnPc2	kojenec
proti	proti	k7c3	proti
Haemophilus	Haemophilus	k1gInSc1	Haemophilus
influenzae	influenza	k1gMnSc4	influenza
typu	typ	k1gInSc2	typ
B	B	kA	B
a	a	k8xC	a
zakrátko	zakrátko	k6eAd1	zakrátko
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
dramatickému	dramatický	k2eAgInSc3d1	dramatický
poklesu	pokles	k1gInSc3	pokles
počtu	počet	k1gInSc2	počet
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Vakcinace	vakcinace	k1gFnSc1	vakcinace
proti	proti	k7c3	proti
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pneumoniae	pneumoniae	k1gInSc1	pneumoniae
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
a	a	k8xC	a
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
podobný	podobný	k2eAgInSc1d1	podobný
pokles	pokles	k1gInSc1	pokles
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
nemocnosti	nemocnost	k1gFnSc3	nemocnost
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
nízké	nízký	k2eAgFnSc3d1	nízká
informovanosti	informovanost	k1gFnSc3	informovanost
o	o	k7c4	o
nemoci	nemoc	k1gFnPc4	nemoc
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
komunita	komunita	k1gFnSc1	komunita
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc4	listopad
Světovým	světový	k2eAgInSc7d1	světový
dnem	den	k1gInSc7	den
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
,	,	kIx,	,
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohou	moct	k5eAaImIp3nP	moct
zainteresovaní	zainteresovaný	k2eAgMnPc1d1	zainteresovaný
občané	občan	k1gMnPc1	občan
a	a	k8xC	a
činitelé	činitel	k1gMnPc1	činitel
s	s	k7c7	s
rozhodovacími	rozhodovací	k2eAgFnPc7d1	rozhodovací
pravomocemi	pravomoc	k1gFnPc7	pravomoc
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
kroky	krok	k1gInPc4	krok
proti	proti	k7c3	proti
šíření	šíření	k1gNnSc3	šíření
této	tento	k3xDgFnSc2	tento
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
komunitní	komunitní	k2eAgFnSc2d1	komunitní
pneumonie	pneumonie	k1gFnSc2	pneumonie
jsou	být	k5eAaImIp3nP	být
celosvětově	celosvětově	k6eAd1	celosvětově
odhadovány	odhadovat	k5eAaImNgFnP	odhadovat
na	na	k7c4	na
17	[number]	k4	17
miliard	miliarda	k4xCgFnPc2	miliarda
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
