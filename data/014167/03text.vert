<s>
Dominique	Dominique	k1gFnSc1
de	de	k?
Villepin	Villepin	k1gInSc1
</s>
<s>
Dominique	Dominique	k1gFnSc1
de	de	k?
Villepin	Villepin	k1gInSc1
</s>
<s>
167	#num#	k4
<g/>
.	.	kIx.
předseda	předseda	k1gMnSc1
francouzské	francouzský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2005	#num#	k4
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2007	#num#	k4
Prezident	prezident	k1gMnSc1
</s>
<s>
Nicolas	Nicolas	k1gInSc1
Sarkozy	Sarkoz	k1gInPc1
Předchůdce	předchůdce	k1gMnSc2
</s>
<s>
Jean-Pierre	Jean-Pierr	k1gMnSc5
Raffarin	Raffarin	k1gInSc1
Nástupce	nástupce	k1gMnSc2
</s>
<s>
François	François	k1gInSc1
Fillon	Fillon	k1gInSc1
</s>
<s>
Ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2004	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2005	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Nicolas	Nicolas	k1gInSc1
Sarkozy	Sarkoz	k1gInPc1
Nástupce	nástupce	k1gMnSc2
</s>
<s>
Nicolas	Nicolas	k1gInSc1
Sarkozy	Sarkoz	k1gInPc1
</s>
<s>
Ministr	ministr	k1gMnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
7	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2002	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc4
2004	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Hubert	Hubert	k1gMnSc1
Védrine	Védrin	k1gInSc5
Nástupce	nástupce	k1gMnSc2
</s>
<s>
Michel	Michel	k1gMnSc1
Barnier	Barnier	k1gMnSc1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Unie	unie	k1gFnSc1
pro	pro	k7c4
lidové	lidový	k2eAgInPc4d1
hnutíRassemblement	hnutíRassemblement	k1gInSc4
pour	pour	k1gInSc1
la	la	k1gNnSc1
République	République	k1gInSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1953	#num#	k4
(	(	kIx(
<g/>
67	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Rabat	rabat	k1gInSc1
<g/>
,	,	kIx,
Maroko	Maroko	k1gNnSc1
Maroko	Maroko	k1gNnSc1
Choť	choť	k1gFnSc4
</s>
<s>
Marie-Laure	Marie-Laur	k1gMnSc5
de	de	k?
Villepin	Villepin	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Xavier	Xavier	k1gMnSc1
de	de	k?
Villepin	Villepin	k1gMnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Marie	Marie	k1gFnSc1
de	de	k?
Villepin	Villepin	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Patrick	Patrick	k1gMnSc1
de	de	k?
Villepin	Villepin	k1gMnSc1
a	a	k8xC
Véronique	Véronique	k1gFnSc1
Albanel	Albanela	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnSc3
<g/>
)	)	kIx)
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
École	École	k1gFnSc1
Nationale	Nationale	k1gFnSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
Administration	Administration	k1gInSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
Univerzita	univerzita	k1gFnSc1
Paříž	Paříž	k1gFnSc1
XInstitut	XInstitut	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
études	études	k1gMnSc1
politiques	politiques	k1gMnSc1
de	de	k?
ParisPařížská	ParisPařížský	k2eAgFnSc1d1
univerzitaUniverzita	univerzitaUniverzita	k1gFnSc1
Paříž	Paříž	k1gFnSc1
II	II	kA
Profese	profese	k1gFnSc1
</s>
<s>
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
diplomat	diplomat	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
advokát	advokát	k1gMnSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
prix	prix	k1gInSc1
Premier-Empire	Premier-Empir	k1gInSc5
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
velkokříž	velkokříž	k1gInSc1
Řádu	řád	k1gInSc2
za	za	k7c2
občanské	občanský	k2eAgFnSc2d1
zásluhy	zásluha	k1gFnSc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
Pierre	Pierr	k1gMnSc5
Lafue	Lafuus	k1gMnSc5
Prize	Priza	k1gFnSc6
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
velkokříž	velkokříž	k1gInSc1
Řádu	řád	k1gInSc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
Polské	polský	k2eAgNnSc1d1
republikyCena	republikyCeno	k1gNnPc1
akademie	akademie	k1gFnSc2
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dominique	Dominique	k1gFnSc1
de	de	k?
Villepin	Villepin	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dominique	Dominique	k1gFnSc1
Marie	Maria	k1gFnSc2
François	François	k1gFnSc1
René	René	k1gMnSc1
Galouzeau	Galouzeaus	k1gInSc2
de	de	k?
Villepin	Villepina	k1gFnPc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
Dominique	Dominique	k1gInSc1
de	de	k?
Villepin	Villepin	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1953	#num#	k4
Rabat	rabato	k1gNnPc2
<g/>
,	,	kIx,
Maroko	Maroko	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
francouzský	francouzský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
diplomat	diplomat	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2005	#num#	k4
do	do	k7c2
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2007	#num#	k4
byl	být	k5eAaImAgMnS
předsedou	předseda	k1gMnSc7
francouzské	francouzský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
politologii	politologie	k1gFnSc4
na	na	k7c6
Pařížském	pařížský	k2eAgInSc6d1
institutu	institut	k1gInSc6
politických	politický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
pokračoval	pokračovat	k5eAaImAgInS
na	na	k7c6
prestižní	prestižní	k2eAgFnSc6d1
francouzské	francouzský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
<g/>
)	)	kIx)
École	École	k1gFnSc1
nationale	nationale	k6eAd1
d	d	k7
<g/>
'	'	kIx"
<g/>
administration	administration	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
diplomatem	diplomat	k1gInSc7
a	a	k8xC
působil	působit	k5eAaImAgMnS
jako	jako	k9
vyslanec	vyslanec	k1gMnSc1
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
D.C.	D.C.	k1gFnSc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Dillí	Dillí	k1gNnSc6
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
-	-	kIx~
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoupal	stoupat	k5eAaImAgMnS
v	v	k7c6
hodnostech	hodnost	k1gFnPc6
francouzského	francouzský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
jako	jako	k9
jeden	jeden	k4xCgMnSc1
z	z	k7c2
chráněnců	chráněnec	k1gMnPc2
tehdejšího	tehdejší	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Jacquese	Jacques	k1gMnSc2
Chiraca	Chiracus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
všeobecného	všeobecný	k2eAgNnSc2d1
světového	světový	k2eAgNnSc2d1
povědomí	povědomí	k1gNnSc2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
jako	jako	k9
ministr	ministr	k1gMnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
když	když	k8xS
vyjádřil	vyjádřit	k5eAaPmAgMnS
v	v	k7c6
r.	r.	kA
2003	#num#	k4
svůj	svůj	k3xOyFgInSc4
nesouhlas	nesouhlas	k1gInSc4
s	s	k7c7
invazí	invaze	k1gFnSc7
do	do	k7c2
Iráku	Irák	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
jeho	jeho	k3xOp3gInSc7
proslovem	proslov	k1gInSc7
před	před	k7c7
OSN	OSN	kA
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vystoupil	vystoupit	k5eAaPmAgMnS
proti	proti	k7c3
druhé	druhý	k4xOgFnSc3
rezoluci	rezoluce	k1gFnSc3
OSN	OSN	kA
povolující	povolující	k2eAgNnSc1d1
použití	použití	k1gNnSc1
síly	síla	k1gFnSc2
proti	proti	k7c3
režimu	režim	k1gInSc3
diktátora	diktátor	k1gMnSc2
Saddáma	Saddám	k1gMnSc2
Husajna	Husajn	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
velkodůstojník	velkodůstojník	k1gInSc1
Řádu	řád	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Karla	Karel	k1gMnSc2
–	–	k?
Monako	Monako	k1gNnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
velkodůstojník	velkodůstojník	k1gInSc1
Řádu	řád	k1gInSc2
litevského	litevský	k2eAgMnSc2d1
velkoknížete	velkokníže	k1gMnSc2
Gediminase	Gediminasa	k1gFnSc3
–	–	k?
Litva	Litva	k1gFnSc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1998	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
velkokříž	velkokříž	k1gInSc1
Řádu	řád	k1gInSc2
prince	princ	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
–	–	k?
Portugalsko	Portugalsko	k1gNnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1999	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
velkodůstojník	velkodůstojník	k1gMnSc1
Řádu	řád	k1gInSc2
zásluh	zásluha	k1gFnPc2
o	o	k7c4
Italskou	italský	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
–	–	k?
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1999	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
velkokříž	velkokříž	k1gInSc1
Řádu	řád	k1gInSc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
Polské	polský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
velkodůstojník	velkodůstojník	k1gMnSc1
Řádu	řád	k1gInSc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
–	–	k?
Litva	Litva	k1gFnSc1
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2004	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
velkokříž	velkokříž	k1gInSc1
Národního	národní	k2eAgInSc2d1
řádu	řád	k1gInSc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
–	–	k?
Francie	Francie	k1gFnSc2
</s>
<s>
velkokříž	velkokříž	k1gInSc1
s	s	k7c7
řetězem	řetěz	k1gInSc7
Řádu	řád	k1gInSc2
tří	tři	k4xCgFnPc2
hvězd	hvězda	k1gFnPc2
–	–	k?
Lotyšsko	Lotyšsko	k1gNnSc4
</s>
<s>
velkokříž	velkokříž	k1gInSc1
Norského	norský	k2eAgInSc2d1
královského	královský	k2eAgInSc2d1
řádu	řád	k1gInSc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
–	–	k?
Norsko	Norsko	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ordonnance	Ordonnance	k1gFnSc1
souveraine	souverainout	k5eAaPmIp3nS
no	no	k9
13149	#num#	k4
du	du	k?
25	#num#	k4
juillet	juillet	k1gInSc1
19971	#num#	k4
2	#num#	k4
Lietuvos	Lietuvos	k1gMnSc1
Respublikos	Respublikos	k1gMnSc1
Prezidentė	Prezidentė	k1gMnSc1
<g/>
.	.	kIx.
grybauskaite	grybauskait	k1gInSc5
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
lrp	lrp	k?
<g/>
.	.	kIx.
<g/>
lt	lt	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ENTIDADES	ENTIDADES	kA
ESTRANGEIRAS	ESTRANGEIRAS	kA
AGRACIADAS	AGRACIADAS	kA
COM	COM	kA
ORDENS	ORDENS	kA
PORTUGUESAS	PORTUGUESAS	kA
-	-	kIx~
Página	Página	k1gMnSc1
Oficial	Oficial	k1gMnSc1
das	das	k?
Ordens	Ordens	k1gInSc1
Honoríficas	Honoríficas	k1gMnSc1
Portuguesas	Portuguesas	k1gMnSc1
<g/>
.	.	kIx.
www.ordens.presidencia.pt	www.ordens.presidencia.pt	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Le	Le	k1gFnSc1
onorificenze	onorificenze	k6eAd1
della	delnout	k5eAaImAgFnS,k5eAaBmAgFnS,k5eAaPmAgFnS
Repubblica	Repubblic	k2eAgFnSc1d1
Italiana	Italiana	k1gFnSc1
<g/>
.	.	kIx.
www.quirinale.it	www.quirinale.it	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Postanowienie	Postanowienie	k1gFnPc1
Prezydenta	Prezydent	k1gMnSc2
Rzeczypospolitej	Rzeczypospolitej	k1gFnSc1
Polskiej	Polskiej	k1gInSc1
z	z	k7c2
dnia	dni	k1gInSc2
12	#num#	k4
maja	maja	k6eAd1
2000	#num#	k4
r.	r.	kA
o	o	k7c6
nadaniu	nadanium	k1gNnSc6
orderów	orderów	k?
<g/>
..	..	k?
prawo	prawo	k1gMnSc1
<g/>
.	.	kIx.
<g/>
sejm	sejm	k1gInSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
pl	pl	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Dominique	Dominiqu	k1gInSc2
de	de	k?
Villepin	Villepin	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Premiéři	premiér	k1gMnPc1
Francie	Francie	k1gFnSc2
</s>
<s>
Michel	Michel	k1gMnSc1
Debré	Debrý	k2eAgFnSc2d1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Georges	Georgesa	k1gFnPc2
Pompidou	Pompida	k1gFnSc7
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Maurice	Maurika	k1gFnSc6
Couve	couvat	k5eAaImIp3nS
de	de	k?
Murville	Murville	k1gFnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jacques	Jacques	k1gMnSc1
Chaban-Delmas	Chaban-Delmas	k1gMnSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pierre	Pierr	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
Messmer	Messmer	k1gInSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jacques	Jacques	k1gMnSc1
Chirac	Chirac	k1gMnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Raymond	Raymond	k1gMnSc1
Barre	Barr	k1gInSc5
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pierre	Pierr	k1gInSc5
Mauroy	Mauroa	k1gMnSc2
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Laurent	Laurent	k1gMnSc1
Fabius	Fabius	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jacques	Jacques	k1gMnSc1
Chirac	Chirac	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michel	Michel	k1gMnSc1
Rocard	Rocard	k1gMnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Édith	Édith	k1gInSc1
Cressonová	Cressonová	k1gFnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pierre	Pierr	k1gInSc5
Bérégovoy	Bérégovoa	k1gMnSc2
(	(	kIx(
<g/>
1992	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Édouard	Édouard	k1gMnSc1
Balladur	Balladur	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alain	Alain	k1gMnSc1
Juppé	Juppý	k2eAgInPc4d1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lionel	Lionel	k1gMnSc1
Jospin	Jospin	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
•	•	k?
Jean-Pierre	Jean-Pierr	k1gInSc5
Raffarin	Raffarin	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Dominique	Dominique	k1gInSc1
de	de	k?
Villepin	Villepin	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
François	François	k1gInSc1
Fillon	Fillon	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jean-Marc	Jean-Marc	k1gInSc1
Ayrault	Ayrault	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Manuel	Manuel	k1gMnSc1
Valls	Valls	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bernard	Bernard	k1gMnSc1
Cazeneuve	Cazeneuev	k1gFnSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Édouard	Édouard	k1gInSc1
Philippe	Philipp	k1gInSc5
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jean	Jean	k1gMnSc1
Castex	Castex	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
245639	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
129234397	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2139	#num#	k4
6495	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
2001027547	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
74026065	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
2001027547	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Francie	Francie	k1gFnSc1
</s>
