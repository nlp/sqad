<s>
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
</s>
<s>
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
lososově	lososově	k6eAd1
ružový	ružový	k2eAgInSc1d1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
zranitelný	zranitelný	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
Aves	Aves	k1gInSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
papoušci	papoušek	k1gMnPc1
(	(	kIx(
<g/>
Psittaciformes	Psittaciformes	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
kakaduovití	kakaduovití	k1gMnPc1
(	(	kIx(
<g/>
Cacatuidae	Cacatuidae	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Cacatua	Cacatua	k1gFnSc1
moluccensisGmelin	moluccensisGmelina	k1gFnPc2
<g/>
,	,	kIx,
1788	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cacatua	Cacatu	k2eAgFnSc1d1
moluccensis	moluccensis	k1gFnSc1
Gmelin	Gmelin	k1gInSc1
1788	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velký	velký	k2eAgMnSc1d1
endemický	endemický	k2eAgMnSc1d1
papoušek	papoušek	k1gMnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
kakaduovitých	kakaduovití	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
ničení	ničení	k1gNnSc3
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
nelegálnímu	legální	k2eNgInSc3d1
odchytu	odchyt	k1gInSc3
je	být	k5eAaImIp3nS
ve	v	k7c6
volné	volný	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
ohrožen	ohrožen	k2eAgInSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
zařazen	zařadit	k5eAaPmNgInS
na	na	k7c4
seznam	seznam	k1gInSc4
CITES	CITES	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
ubývajících	ubývající	k2eAgInPc6d1
domovských	domovský	k2eAgInPc6d1
pralesích	prales	k1gInPc6
přežívá	přežívat	k5eAaImIp3nS
jen	jen	k9
něco	něco	k3yInSc4
kolem	kolem	k7c2
2000	#num#	k4
jedinců	jedinec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Délka	délka	k1gFnSc1
těla	tělo	k1gNnSc2
dosahuje	dosahovat	k5eAaImIp3nS
až	až	k9
50	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
hmotnost	hmotnost	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
780	#num#	k4
<g/>
–	–	k?
<g/>
1100	#num#	k4
g.	g.	k?
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
má	mít	k5eAaImIp3nS
celé	celý	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
a	a	k8xC
hlavu	hlava	k1gFnSc4
zbarvenou	zbarvený	k2eAgFnSc4d1
světle	světle	k6eAd1
lososově	lososově	k6eAd1
růžově	růžově	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztyčitelná	vztyčitelný	k2eAgFnSc1d1
chocholka	chocholka	k1gFnSc1
je	být	k5eAaImIp3nS
tmavěji	tmavě	k6eAd2
lososově	lososově	k6eAd1
růžově	růžově	k6eAd1
zbarvená	zbarvený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oči	oko	k1gNnPc1
mají	mít	k5eAaImIp3nP
hnědou	hnědý	k2eAgFnSc4d1
duhovku	duhovka	k1gFnSc4
<g/>
,	,	kIx,
zobák	zobák	k1gInSc4
a	a	k8xC
nohy	noha	k1gFnPc4
jsou	být	k5eAaImIp3nP
tmavě	tmavě	k6eAd1
šedé	šedý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
samec	samec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
endemicky	endemicky	k6eAd1
žije	žít	k5eAaImIp3nS
na	na	k7c6
Jižních	jižní	k2eAgInPc6d1
Moluckých	molucký	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Indonésii	Indonésie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
kolem	kolem	k7c2
1000	#num#	k4
m	m	kA
<g/>
,	,	kIx,
v	v	k7c6
pobřežních	pobřežní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
horách	hora	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
kokosových	kokosový	k2eAgFnPc6d1
plantážích	plantáž	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
hnízdění	hnízdění	k1gNnSc2
žijí	žít	k5eAaImIp3nP
v	v	k7c6
párech	pár	k1gInPc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
sdružují	sdružovat	k5eAaImIp3nP
do	do	k7c2
velkých	velká	k1gFnPc2
hejn	hejno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Potrava	potrava	k1gFnSc1
</s>
<s>
Živí	živit	k5eAaImIp3nP
se	se	k3xPyFc4
převážně	převážně	k6eAd1
rostlinnou	rostlinný	k2eAgFnSc7d1
potravou	potrava	k1gFnSc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
semena	semeno	k1gNnPc1
<g/>
,	,	kIx,
ovoce	ovoce	k1gNnPc1
<g/>
,	,	kIx,
méně	málo	k6eAd2
hmyz	hmyz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zajetí	zajetí	k1gNnSc6
se	se	k3xPyFc4
podává	podávat	k5eAaImIp3nS
slunečnicové	slunečnicový	k2eAgNnSc1d1
semínko	semínko	k1gNnSc1
<g/>
,	,	kIx,
různé	různý	k2eAgInPc1d1
druhy	druh	k1gInPc1
ořechů	ořech	k1gInPc2
<g/>
,	,	kIx,
pšenici	pšenice	k1gFnSc4
<g/>
,	,	kIx,
oves	oves	k1gInSc4
<g/>
,	,	kIx,
proso	proso	k1gNnSc4
<g/>
,	,	kIx,
lesknici	lesknice	k1gFnSc4
<g/>
,	,	kIx,
máčenou	máčený	k2eAgFnSc4d1
kukuřici	kukuřice	k1gFnSc4
<g/>
,	,	kIx,
granule	granule	k1gFnPc1
<g/>
,	,	kIx,
piškoty	piškot	k1gInPc1
<g/>
,	,	kIx,
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
mrkev	mrkev	k1gFnSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc1d1
krmení	krmení	k1gNnSc1
a	a	k8xC
čerstvé	čerstvý	k2eAgFnPc1d1
větve	větev	k1gFnPc1
k	k	k7c3
okusu	okus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS
v	v	k7c6
dutinách	dutina	k1gFnPc6
stromů	strom	k1gInPc2
<g/>
,	,	kIx,
samice	samice	k1gFnSc1
snáší	snášet	k5eAaImIp3nS
2	#num#	k4
vejce	vejce	k1gNnSc2
<g/>
,	,	kIx,
mláďata	mládě	k1gNnPc1
se	se	k3xPyFc4
líhnou	líhnout	k5eAaImIp3nP
za	za	k7c4
29	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
dní	den	k1gInPc2
a	a	k8xC
opouštějí	opouštět	k5eAaImIp3nP
hnízdo	hnízdo	k1gNnSc4
po	po	k7c4
8	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
týdnech	týden	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Chov	chov	k1gInSc1
</s>
<s>
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
v	v	k7c6
domácnosti	domácnost	k1gFnSc6
</s>
<s>
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
inteligentní	inteligentní	k2eAgInSc1d1
<g/>
,	,	kIx,
citlivý	citlivý	k2eAgInSc1d1
a	a	k8xC
učenlivý	učenlivý	k2eAgMnSc1d1
pták	pták	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Vyžaduje	vyžadovat	k5eAaImIp3nS
pravidelný	pravidelný	k2eAgInSc4d1
kontakt	kontakt	k1gInSc4
s	s	k7c7
členy	člen	k1gInPc7
svého	svůj	k3xOyFgNnSc2
lidského	lidský	k2eAgNnSc2d1
hejna	hejno	k1gNnSc2
a	a	k8xC
potřebuje	potřebovat	k5eAaImIp3nS
neustálou	neustálý	k2eAgFnSc4d1
mentální	mentální	k2eAgFnSc4d1
stimulaci	stimulace	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
mu	on	k3xPp3gMnSc3
můžeme	moct	k5eAaImIp1nP
poskytnout	poskytnout	k5eAaPmF
ve	v	k7c6
formě	forma	k1gFnSc6
průběžně	průběžně	k6eAd1
obměňovaných	obměňovaný	k2eAgFnPc2d1
hraček	hračka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechává	nechávat	k5eAaImIp3nS
se	se	k3xPyFc4
poškrábat	poškrábat	k5eAaPmF
a	a	k8xC
někdy	někdy	k6eAd1
se	se	k3xPyFc4
toho	ten	k3xDgNnSc2
i	i	k9
nepříjemně	příjemně	k6eNd1
dožaduje	dožadovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klade	klást	k5eAaImIp3nS
tak	tak	k9
na	na	k7c4
chovatele	chovatel	k1gMnSc4
poměrně	poměrně	k6eAd1
velké	velký	k2eAgInPc1d1
časové	časový	k2eAgInPc1d1
nároky	nárok	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zanedbání	zanedbání	k1gNnSc6
péče	péče	k1gFnSc2
se	se	k3xPyFc4
u	u	k7c2
něj	on	k3xPp3gMnSc2
může	moct	k5eAaImIp3nS
objevit	objevit	k5eAaPmF
zlozvyk	zlozvyk	k1gInSc1
vytrhávání	vytrhávání	k1gNnSc2
peří	peřit	k5eAaImIp3nS
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
držen	držet	k5eAaImNgInS
v	v	k7c6
kleci	klec	k1gFnSc6
nevyhovujících	vyhovující	k2eNgInPc2d1
rozměrů	rozměr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlépe	dobře	k6eAd3
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
totiž	totiž	k9
daří	dařit	k5eAaImIp3nS
v	v	k7c6
prostorné	prostorný	k2eAgFnSc6d1
voliéře	voliéra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
hlučný	hlučný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Není	být	k5eNaImIp3nS
náhodou	náhoda	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
nejhojnějšími	hojný	k2eAgMnPc7d3
obyvateli	obyvatel	k1gMnPc7
různých	různý	k2eAgNnPc2d1
záchytných	záchytný	k2eAgNnPc2d1
center	centrum	k1gNnPc2
pro	pro	k7c4
problémové	problémový	k2eAgMnPc4d1
papoušky	papoušek	k1gMnPc4
jsou	být	k5eAaImIp3nP
právě	právě	k9
tito	tento	k3xDgMnPc1
kakaduové	kakadu	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naštěstí	naštěstí	k6eAd1
však	však	k9
existují	existovat	k5eAaImIp3nP
výcvikové	výcvikový	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
,	,	kIx,
díky	díky	k7c3
nimž	jenž	k3xRgFnPc3
lze	lze	k6eAd1
křik	křik	k1gInSc4
mírnit	mírnit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
nikdy	nikdy	k6eAd1
nebude	být	k5eNaImBp3nS
ptákem	pták	k1gMnSc7
vhodným	vhodný	k2eAgInSc7d1
do	do	k7c2
bytu	byt	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
lze	lze	k6eAd1
jej	on	k3xPp3gMnSc4
naučit	naučit	k5eAaPmF
křičet	křičet	k5eAaImF
v	v	k7c6
omezených	omezený	k2eAgInPc6d1
časových	časový	k2eAgInPc6d1
úsecích	úsek	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nejméně	málo	k6eAd3
rušivé	rušivý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
výcvik	výcvik	k1gInSc4
je	být	k5eAaImIp3nS
vhodný	vhodný	k2eAgMnSc1d1
zejména	zejména	k9
clicker	clicker	k1gMnSc1
training	training	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
vyžaduje	vyžadovat	k5eAaImIp3nS
správné	správný	k2eAgNnSc1d1
zacházení	zacházení	k1gNnSc1
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
nebezpečným	bezpečný	k2eNgFnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
právě	právě	k6eAd1
ideálním	ideální	k2eAgMnSc7d1
mazlíčkem	mazlíček	k1gMnSc7
do	do	k7c2
rodin	rodina	k1gFnPc2
s	s	k7c7
malými	malý	k2eAgFnPc7d1
dětmi	dítě	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	svůj	k3xOyFgInSc7
zobákem	zobák	k1gInSc7
bez	bez	k7c2
problémů	problém	k1gInPc2
rozlouskne	rozlousknout	k5eAaPmIp3nS
paraořech	paraořech	k1gInSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k6eAd1
dokáže	dokázat	k5eAaPmIp3nS
i	i	k9
člověku	člověk	k1gMnSc3
zlomit	zlomit	k5eAaPmF
prst	prst	k1gInSc4
nebo	nebo	k8xC
způsobit	způsobit	k5eAaPmF
jiná	jiný	k2eAgNnPc4d1
vážná	vážný	k2eAgNnPc4d1
zranění	zranění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
naučit	naučit	k5eAaPmF
se	se	k3xPyFc4
před	před	k7c7
pořízením	pořízení	k1gNnSc7
tohoto	tento	k3xDgMnSc2
kakadua	kakadu	k1gMnSc2
základním	základní	k2eAgFnPc3d1
výcvikovým	výcvikový	k2eAgFnPc3d1
metodám	metoda	k1gFnPc3
<g/>
,	,	kIx,
především	především	k9
již	již	k6eAd1
zmíněnému	zmíněný	k2eAgInSc3d1
clicker	clicker	k1gInSc1
trainingu	training	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
má	mít	k5eAaImIp3nS
rovněž	rovněž	k9
silné	silný	k2eAgInPc4d1
ničitelské	ničitelský	k2eAgInPc4d1
sklony	sklon	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
poskytnout	poskytnout	k5eAaPmF
mu	on	k3xPp3gMnSc3
dostatek	dostatek	k1gInSc4
příležitostí	příležitost	k1gFnPc2
k	k	k7c3
okusování	okusování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgInSc3
účelu	účel	k1gInSc3
poslouží	posloužit	k5eAaPmIp3nP
čerstvě	čerstvě	k6eAd1
natrhané	natrhaný	k2eAgFnPc1d1
větve	větev	k1gFnPc1
<g/>
,	,	kIx,
dřevěné	dřevěný	k2eAgFnPc1d1
hračky	hračka	k1gFnPc1
atd.	atd.	kA
Tento	tento	k3xDgMnSc1
pták	pták	k1gMnSc1
je	být	k5eAaImIp3nS
značně	značně	k6eAd1
mechanicky	mechanicky	k6eAd1
nadaný	nadaný	k2eAgMnSc1d1
a	a	k8xC
brzy	brzy	k6eAd1
se	se	k3xPyFc4
naučí	naučit	k5eAaPmIp3nS
otevírat	otevírat	k5eAaImF
dvířka	dvířka	k1gNnPc4
klece	klec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
tudíž	tudíž	k8xC
třeba	třeba	k6eAd1
dodatečně	dodatečně	k6eAd1
zajistit	zajistit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
kakaduů	kakadu	k1gMnPc2
ráda	rád	k2eAgFnSc1d1
poskakuje	poskakovat	k5eAaImIp3nS
po	po	k7c6
podlaze	podlaha	k1gFnSc6
<g/>
;	;	kIx,
avšak	avšak	k8xC
pokud	pokud	k8xS
jim	on	k3xPp3gMnPc3
to	ten	k3xDgNnSc4
majitel	majitel	k1gMnSc1
umožní	umožnit	k5eAaPmIp3nS
příliš	příliš	k6eAd1
často	často	k6eAd1
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
stát	stát	k5eAaPmF,k5eAaImF
agresivními	agresivní	k2eAgFnPc7d1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
mít	mít	k5eAaImF
na	na	k7c6
paměti	paměť	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
se	se	k3xPyFc4
dožívá	dožívat	k5eAaImIp3nS
70	#num#	k4
i	i	k8xC
více	hodně	k6eAd2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
je	být	k5eAaImIp3nS
před	před	k7c7
jeho	jeho	k3xOp3gNnSc7
pořízením	pořízení	k1gNnSc7
třeba	třeba	k6eAd1
uvážit	uvážit	k5eAaPmF
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
o	o	k7c4
něj	on	k3xPp3gMnSc4
postaral	postarat	k5eAaPmAgMnS
v	v	k7c6
případě	případ	k1gInSc6
majitelovy	majitelův	k2eAgFnSc2d1
nehody	nehoda	k1gFnSc2
či	či	k8xC
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
pečlivě	pečlivě	k6eAd1
promyšlená	promyšlený	k2eAgFnSc1d1
koupě	koupě	k1gFnSc1
a	a	k8xC
zvážení	zvážení	k1gNnSc4
všech	všecek	k3xTgInPc2
životních	životní	k2eAgInPc2d1
nároků	nárok	k1gInPc2
i	i	k8xC
rizik	riziko	k1gNnPc2
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
k	k	k7c3
žádoucímu	žádoucí	k2eAgInSc3d1
výsledku	výsledek	k1gInSc3
<g/>
,	,	kIx,
tj.	tj.	kA
získání	získání	k1gNnSc4
společníka	společník	k1gMnSc2
na	na	k7c4
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ČR	ČR	kA
chová	chovat	k5eAaImIp3nS
kakadu	kakadu	k1gMnSc1
molucké	molucký	k2eAgFnSc2d1
zoo	zoo	k1gFnSc2
Tábor	Tábor	k1gInSc1
a	a	k8xC
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2020.3	2020.3	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
https://web.archive.org/web/20070120185328/http://papousci.webz.cz/popis/kakadu_molucky.html	https://web.archive.org/web/20070120185328/http://papousci.webz.cz/popis/kakadu_molucky.html	k1gMnSc1
</s>
<s>
http://www.parrotclub.sk	http://www.parrotclub.sk	k1gInSc1
</s>
<s>
http://ifauna.cz	http://ifauna.cz	k1gMnSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kakaduovití	Kakaduovití	k1gMnPc1
(	(	kIx(
<g/>
Cacatuidae	Cacatuidae	k1gInSc1
<g/>
)	)	kIx)
rod	rod	k1gInSc1
Nymphicus	Nymphicus	k1gInSc1
</s>
<s>
korela	korela	k1gFnSc1
chocholatá	chocholatý	k2eAgFnSc1d1
rod	rod	k1gInSc4
Probosciger	Probosciger	k1gInSc4
</s>
<s>
kakadu	kakadu	k1gMnSc1
arový	arový	k2eAgInSc1d1
rod	rod	k1gInSc1
Calyptorhynchus	Calyptorhynchus	k1gInSc1
</s>
<s>
kakadu	kakadu	k1gMnSc1
havraní	havraní	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
hnědohlavý	hnědohlavý	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
černý	černý	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
krátkozobý	krátkozobý	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
dlouhozobý	dlouhozobý	k2eAgInSc1d1
rod	rod	k1gInSc1
Callocephalon	Callocephalon	k1gInSc1
</s>
<s>
kakadu	kakadu	k1gMnSc1
přilbový	přilbový	k2eAgInSc1d1
rod	rod	k1gInSc1
Eolophus	Eolophus	k1gInSc1
</s>
<s>
kakadu	kakadu	k1gMnSc1
růžový	růžový	k2eAgInSc1d1
rod	rod	k1gInSc1
Cacatua	Cacatu	k1gInSc2
</s>
<s>
kakadu	kakadu	k1gMnSc1
inka	inka	k1gMnSc1
•	•	k?
kakadu	kakadu	k1gMnSc1
žlutolící	žlutolící	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
žlutočečelatý	žlutočečelatý	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
brýlový	brýlový	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
bílý	bílý	k1gMnSc1
•	•	k?
kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
tenkozobý	tenkozobý	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
hrabavý	hrabavý	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
naholící	naholící	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
Goffinův	Goffinův	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
šalamounský	šalamounský	k2eAgMnSc1d1
•	•	k?
kakadu	kakadu	k1gMnSc1
filipínský	filipínský	k2eAgMnSc1d1
Papoušci	Papoušek	k1gMnPc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ptáci	pták	k1gMnPc1
</s>
