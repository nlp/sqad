<s>
Kakadu	kakadu	k1gMnSc1
molucký	molucký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cacatua	Cacatu	k2eAgFnSc1d1
moluccensis	moluccensis	k1gFnSc1
Gmelin	Gmelin	k1gInSc1
1788	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velký	velký	k2eAgMnSc1d1
endemický	endemický	k2eAgMnSc1d1
papoušek	papoušek	k1gMnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
kakaduovitých	kakaduovití	k1gMnPc2
<g/>
.	.	kIx.
</s>