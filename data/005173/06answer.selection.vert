<s>
Desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
je	být	k5eAaImIp3nS	být
komplexní	komplexní	k2eAgFnSc1d1	komplexní
vědecká	vědecký	k2eAgFnSc1d1	vědecká
teorie	teorie	k1gFnSc1	teorie
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
dynamickým	dynamický	k2eAgInSc7d1	dynamický
vývojem	vývoj	k1gInSc7	vývoj
systému	systém	k1gInSc2	systém
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
procesy	proces	k1gInPc4	proces
a	a	k8xC	a
strukturu	struktura	k1gFnSc4	struktura
zemského	zemský	k2eAgInSc2d1	zemský
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
