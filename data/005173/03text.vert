<s>
Desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
je	být	k5eAaImIp3nS	být
komplexní	komplexní	k2eAgFnSc1d1	komplexní
vědecká	vědecký	k2eAgFnSc1d1	vědecká
teorie	teorie	k1gFnSc1	teorie
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
dynamickým	dynamický	k2eAgInSc7d1	dynamický
vývojem	vývoj	k1gInSc7	vývoj
systému	systém	k1gInSc2	systém
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
procesy	proces	k1gInPc4	proces
a	a	k8xC	a
strukturu	struktura	k1gFnSc4	struktura
zemského	zemský	k2eAgInSc2d1	zemský
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Nejsvrchnější	svrchní	k2eAgFnSc1d3	nejsvrchnější
vrstva	vrstva	k1gFnSc1	vrstva
Země	zem	k1gFnSc2	zem
tzv.	tzv.	kA	tzv.
litosféra	litosféra	k1gFnSc1	litosféra
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
rozlámána	rozlámán	k2eAgFnSc1d1	rozlámána
na	na	k7c4	na
několik	několik	k4yIc4	několik
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
díky	díky	k7c3	díky
plastické	plastický	k2eAgFnSc3d1	plastická
astenosféře	astenosféra	k1gFnSc3	astenosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tzv.	tzv.	kA	tzv.
středooceánských	středooceánský	k2eAgInPc6d1	středooceánský
hřbetech	hřbet	k1gInPc6	hřbet
vzniká	vznikat	k5eAaImIp3nS	vznikat
nová	nový	k2eAgFnSc1d1	nová
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
<g/>
,	,	kIx,	,
stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
zanořuje	zanořovat	k5eAaImIp3nS	zanořovat
do	do	k7c2	do
zemského	zemský	k2eAgNnSc2d1	zemské
nitra	nitro	k1gNnSc2	nitro
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
tzv.	tzv.	kA	tzv.
subdukcí	subdukce	k1gFnPc2	subdukce
<g/>
.	.	kIx.	.
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
víceméně	víceméně	k9	víceméně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
kolizím	kolize	k1gFnPc3	kolize
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mezi	mezi	k7c7	mezi
Indií	Indie	k1gFnSc7	Indie
a	a	k8xC	a
Asií	Asie	k1gFnPc2	Asie
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Himálaj	Himálaj	k1gFnSc4	Himálaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0	[number]	k4	0
až	až	k9	až
100	[number]	k4	100
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
desky	deska	k1gFnPc1	deska
se	se	k3xPyFc4	se
ale	ale	k9	ale
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
různými	různý	k2eAgInPc7d1	různý
směry	směr	k1gInPc7	směr
i	i	k8xC	i
rychlostmi	rychlost	k1gFnPc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
je	být	k5eAaImIp3nS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
účinný	účinný	k2eAgInSc1d1	účinný
proces	proces	k1gInSc1	proces
pro	pro	k7c4	pro
chladnutí	chladnutí	k1gNnSc4	chladnutí
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
generování	generování	k1gNnSc1	generování
silného	silný	k2eAgNnSc2d1	silné
geomagnetického	geomagnetický	k2eAgNnSc2d1	geomagnetické
pole	pole	k1gNnSc2	pole
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
–	–	k?	–
žádný	žádný	k3yNgInSc4	žádný
jiný	jiný	k2eAgInSc4d1	jiný
objekt	objekt	k1gInSc4	objekt
(	(	kIx(	(
<g/>
s	s	k7c7	s
možnou	možný	k2eAgFnSc7d1	možná
výjimkou	výjimka	k1gFnSc7	výjimka
ledových	ledový	k2eAgInPc2d1	ledový
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
známky	známka	k1gFnSc2	známka
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
dnes	dnes	k6eAd1	dnes
nevykazuje	vykazovat	k5eNaImIp3nS	vykazovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Země	zem	k1gFnSc2	zem
patrně	patrně	k6eAd1	patrně
započal	započnout	k5eAaPmAgInS	započnout
před	před	k7c7	před
3	[number]	k4	3
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
rozšířením	rozšíření	k1gNnSc7	rozšíření
staršího	starý	k2eAgInSc2d2	starší
konceptu	koncept	k1gInSc2	koncept
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
driftu	drift	k1gInSc2	drift
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
německým	německý	k2eAgMnSc7d1	německý
geologem	geolog	k1gMnSc7	geolog
Alfredem	Alfred	k1gMnSc7	Alfred
Wegenerem	Wegener	k1gMnSc7	Wegener
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
navrženým	navržený	k2eAgInSc7d1	navržený
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
nachází	nacházet	k5eAaImIp3nS	nacházet
7	[number]	k4	7
či	či	k8xC	či
9	[number]	k4	9
velkých	velký	k2eAgFnPc2d1	velká
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jsou	být	k5eAaImIp3nP	být
definovány	definován	k2eAgFnPc1d1	definována
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
menších	malý	k2eAgFnPc2d2	menší
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
pohybu	pohyb	k1gInSc6	pohyb
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
rozhraní	rozhraní	k1gNnSc1	rozhraní
konvergentní	konvergentní	k2eAgFnSc2d1	konvergentní
(	(	kIx(	(
<g/>
kolizní	kolizní	k2eAgFnSc2d1	kolizní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divergentní	divergentní	k2eAgNnSc4d1	divergentní
(	(	kIx(	(
<g/>
rozestupující	rozestupující	k2eAgNnSc4d1	rozestupující
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
a	a	k8xC	a
transformní	transformní	k2eAgInSc1d1	transformní
(	(	kIx(	(
<g/>
posunující	posunující	k2eAgInSc1d1	posunující
se	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doprovodnými	doprovodný	k2eAgInPc7d1	doprovodný
jevy	jev	k1gInPc7	jev
pohybů	pohyb	k1gInPc2	pohyb
desek	deska	k1gFnPc2	deska
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
,	,	kIx,	,
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc4	vznik
horstev	horstvo	k1gNnPc2	horstvo
a	a	k8xC	a
hlubokomořských	hlubokomořský	k2eAgInPc2d1	hlubokomořský
příkopů	příkop	k1gInPc2	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Tektonické	tektonický	k2eAgFnPc1d1	tektonická
desky	deska	k1gFnPc1	deska
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemská	zemský	k2eAgFnSc1d1	zemská
litosféra	litosféra	k1gFnSc1	litosféra
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
nižší	nízký	k2eAgFnSc4d2	nižší
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
spodnější	spodní	k2eAgFnSc1d2	spodnější
astenosféra	astenosféra	k1gFnSc1	astenosféra
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
způsobován	způsobovat	k5eAaImNgInS	způsobovat
výstupem	výstup	k1gInSc7	výstup
teplejšího	teplý	k2eAgInSc2d2	teplejší
materiálu	materiál	k1gInSc2	materiál
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
hustotě	hustota	k1gFnSc6	hustota
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
plášťové	plášťový	k2eAgFnSc3d1	plášťová
konvekci	konvekce	k1gFnSc3	konvekce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
jako	jako	k8xC	jako
kombinace	kombinace	k1gFnSc2	kombinace
tahu	tah	k1gInSc2	tah
<g/>
,	,	kIx,	,
sestupného	sestupný	k2eAgNnSc2d1	sestupné
nasávání	nasávání	k1gNnSc2	nasávání
v	v	k7c6	v
subdukčních	subdukční	k2eAgFnPc6d1	subdukční
zónách	zóna	k1gFnPc6	zóna
a	a	k8xC	a
variace	variace	k1gFnSc2	variace
topografie	topografie	k1gFnSc2	topografie
a	a	k8xC	a
hustoty	hustota	k1gFnSc2	hustota
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
rozdílům	rozdíl	k1gInPc3	rozdíl
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
působící	působící	k2eAgFnSc2d1	působící
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sil	síla	k1gFnPc2	síla
působících	působící	k2eAgFnPc2d1	působící
na	na	k7c4	na
desku	deska	k1gFnSc4	deska
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
neznámý	známý	k2eNgMnSc1d1	neznámý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
debat	debata	k1gFnPc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
geologové	geolog	k1gMnPc1	geolog
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgInPc1d1	hlavní
útvary	útvar	k1gInPc1	útvar
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
pozičně	pozičně	k6eAd1	pozičně
statické	statický	k2eAgFnPc1d1	statická
a	a	k8xC	a
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
geologických	geologický	k2eAgNnPc2d1	geologické
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
horská	horský	k2eAgNnPc4d1	horské
pásma	pásmo	k1gNnPc4	pásmo
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vysvětlena	vysvětlit	k5eAaPmNgFnS	vysvětlit
vertikálním	vertikální	k2eAgInSc7d1	vertikální
pohybem	pohyb	k1gInSc7	pohyb
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
geosynklinální	geosynklinální	k2eAgFnSc2d1	geosynklinální
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1596	[number]	k4	1596
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
ležící	ležící	k2eAgNnSc4d1	ležící
pobřeží	pobřeží	k1gNnSc4	pobřeží
kontinentů	kontinent	k1gInPc2	kontinent
v	v	k7c6	v
Atlantickém	atlantický	k2eAgInSc6d1	atlantický
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
viditelné	viditelný	k2eAgInPc1d1	viditelný
okraje	okraj	k1gInPc1	okraj
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
podobný	podobný	k2eAgInSc4d1	podobný
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednou	jednou	k6eAd1	jednou
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
spolu	spolu	k6eAd1	spolu
spojeny	spojit	k5eAaPmNgInP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
mnoho	mnoho	k4c1	mnoho
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
toto	tento	k3xDgNnSc4	tento
spojení	spojení	k1gNnSc4	spojení
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpoklad	předpoklad	k1gInSc1	předpoklad
pevné	pevný	k2eAgFnSc2d1	pevná
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
nevyvijí	vyvít	k5eNaPmIp3nP	vyvít
<g/>
,	,	kIx,	,
činil	činit	k5eAaImAgInS	činit
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
obtížné	obtížný	k2eAgNnSc4d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Objevení	objevení	k1gNnSc1	objevení
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgNnSc1d1	spojené
uvolňování	uvolňování	k1gNnSc1	uvolňování
tepla	teplo	k1gNnSc2	teplo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
pokusit	pokusit	k5eAaPmF	pokusit
se	se	k3xPyFc4	se
přesněji	přesně	k6eAd2	přesně
datovat	datovat	k5eAaImF	datovat
stáří	stáří	k1gNnSc4	stáří
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
před	před	k7c7	před
tím	ten	k3xDgInSc7	ten
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
stáří	stáří	k1gNnSc2	stáří
Země	zem	k1gFnSc2	zem
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
rychlosti	rychlost	k1gFnSc2	rychlost
chladnutí	chladnutí	k1gNnSc2	chladnutí
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
černé	černý	k2eAgNnSc1d1	černé
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výpočty	výpočet	k1gInPc1	výpočet
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
Země	země	k1gFnSc1	země
začala	začít	k5eAaPmAgFnS	začít
chladnout	chladnout	k5eAaImF	chladnout
z	z	k7c2	z
doruda	dorudo	k1gNnSc2	dorudo
rozžhaveného	rozžhavený	k2eAgNnSc2d1	rozžhavené
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
by	by	kYmCp3nS	by
vychladla	vychladnout	k5eAaPmAgFnS	vychladnout
na	na	k7c4	na
současné	současný	k2eAgFnPc4d1	současná
teploty	teplota	k1gFnPc4	teplota
během	během	k7c2	během
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nově	nově	k6eAd1	nově
objevený	objevený	k2eAgInSc1d1	objevený
zdroj	zdroj	k1gInSc1	zdroj
tepla	teplo	k1gNnSc2	teplo
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
zahřívat	zahřívat	k5eAaImF	zahřívat
Zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
starší	starý	k2eAgMnSc1d2	starší
a	a	k8xC	a
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
má	mít	k5eAaImIp3nS	mít
dostatek	dostatek	k1gInSc4	dostatek
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
tekuté	tekutý	k2eAgNnSc1d1	tekuté
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
rozpracováním	rozpracování	k1gNnSc7	rozpracování
hypotézy	hypotéza	k1gFnSc2	hypotéza
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
driftu	drift	k1gInSc2	drift
navržené	navržený	k2eAgFnPc4d1	navržená
německým	německý	k2eAgMnSc7d1	německý
geologem	geolog	k1gMnSc7	geolog
Alfredem	Alfred	k1gMnSc7	Alfred
Wegenerem	Wegener	k1gMnSc7	Wegener
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
a	a	k8xC	a
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Původ	původ	k1gInSc1	původ
kontinentů	kontinent	k1gInPc2	kontinent
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Entstehung	Entstehung	k1gMnSc1	Entstehung
der	drát	k5eAaImRp2nS	drát
Kontinente	kontinent	k1gInSc5	kontinent
und	und	k?	und
Ozeane	Ozean	k1gMnSc5	Ozean
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wegener	Wegener	k1gMnSc1	Wegener
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnešní	dnešní	k2eAgInPc1d1	dnešní
kontinenty	kontinent	k1gInPc1	kontinent
dříve	dříve	k6eAd2	dříve
tvořily	tvořit	k5eAaImAgInP	tvořit
jednotné	jednotný	k2eAgInPc1d1	jednotný
kompaktní	kompaktní	k2eAgInPc1d1	kompaktní
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
kontinentů	kontinent	k1gInPc2	kontinent
popisoval	popisovat	k5eAaImAgMnS	popisovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ledové	ledový	k2eAgFnPc4d1	ledová
kry	kra	k1gFnPc4	kra
<g/>
"	"	kIx"	"
méně	málo	k6eAd2	málo
hustého	hustý	k2eAgInSc2d1	hustý
granitu	granit	k1gInSc2	granit
(	(	kIx(	(
<g/>
žuly	žula	k1gFnPc1	žula
<g/>
)	)	kIx)	)
plovoucí	plovoucí	k2eAgFnSc1d1	plovoucí
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
hustšího	hustý	k2eAgInSc2d2	hustší
bazaltu	bazalt	k1gInSc2	bazalt
(	(	kIx(	(
<g/>
čediče	čedič	k1gInSc2	čedič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
bez	bez	k7c2	bez
přesných	přesný	k2eAgNnPc2d1	přesné
pozorování	pozorování	k1gNnPc2	pozorování
a	a	k8xC	a
sil	síla	k1gFnPc2	síla
dostatečně	dostatečně	k6eAd1	dostatečně
silných	silný	k2eAgFnPc2d1	silná
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
tento	tento	k3xDgInSc4	tento
pohyb	pohyb	k1gInSc4	pohyb
kontinentů	kontinent	k1gInPc2	kontinent
řídit	řídit	k5eAaImF	řídit
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
teorie	teorie	k1gFnSc1	teorie
všeobecně	všeobecně	k6eAd1	všeobecně
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
představách	představa	k1gFnPc6	představa
Země	země	k1gFnSc1	země
mohla	moct	k5eAaImAgFnS	moct
mít	mít	k5eAaImF	mít
pevnou	pevný	k2eAgFnSc4d1	pevná
kůru	kůra	k1gFnSc4	kůra
a	a	k8xC	a
tekuté	tekutý	k2eAgNnSc4d1	tekuté
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
pevná	pevný	k2eAgFnSc1d1	pevná
kůra	kůra	k1gFnSc1	kůra
pohybovat	pohybovat	k5eAaImF	pohybovat
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
výzkumy	výzkum	k1gInPc1	výzkum
podporovaly	podporovat	k5eAaImAgInP	podporovat
teorii	teorie	k1gFnSc4	teorie
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
driftu	drift	k1gInSc2	drift
obhajovanou	obhajovaný	k2eAgFnSc4d1	obhajovaná
anglickým	anglický	k2eAgMnSc7d1	anglický
geologem	geolog	k1gMnSc7	geolog
Arthurem	Arthur	k1gMnSc7	Arthur
Holmesem	Holmes	k1gMnSc7	Holmes
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhraní	rozhraní	k1gNnSc1	rozhraní
desek	deska	k1gFnPc2	deska
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
Holmes	Holmes	k1gMnSc1	Holmes
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
konvektivní	konvektivní	k2eAgInPc1d1	konvektivní
proudy	proud	k1gInPc1	proud
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
řídící	řídící	k2eAgFnSc1d1	řídící
síla	síla	k1gFnSc1	síla
těchto	tento	k3xDgInPc2	tento
procesů	proces	k1gInPc2	proces
způsobující	způsobující	k2eAgNnSc4d1	způsobující
oddalování	oddalování	k1gNnSc4	oddalování
kontinentů	kontinent	k1gInPc2	kontinent
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
důkaz	důkaz	k1gInSc1	důkaz
pohybu	pohyb	k1gInSc2	pohyb
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
objevením	objevení	k1gNnSc7	objevení
různé	různý	k2eAgFnSc2d1	různá
orientace	orientace	k1gFnSc2	orientace
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
různého	různý	k2eAgNnSc2d1	různé
stáří	stáří	k1gNnSc2	stáří
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
na	na	k7c6	na
sympoziu	sympozion	k1gNnSc6	sympozion
v	v	k7c6	v
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
globální	globální	k2eAgFnSc2d1	globální
expanze	expanze	k1gFnSc2	expanze
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
spoluprací	spolupráce	k1gFnPc2	spolupráce
zapracováno	zapracovat	k5eAaPmNgNnS	zapracovat
do	do	k7c2	do
teorie	teorie	k1gFnSc2	teorie
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
výstupu	výstup	k1gInSc2	výstup
materiálu	materiál	k1gInSc2	materiál
tvořící	tvořící	k2eAgFnSc2d1	tvořící
nové	nový	k2eAgFnSc2d1	nová
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyžadující	vyžadující	k2eAgFnSc4d1	vyžadující
existenci	existence	k1gFnSc4	existence
subdukčních	subdukční	k2eAgFnPc2d1	subdukční
zón	zóna	k1gFnPc2	zóna
a	a	k8xC	a
transformních	transformní	k2eAgInPc2d1	transformní
zlomů	zlom	k1gInPc2	zlom
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
poznatky	poznatek	k1gInPc7	poznatek
o	o	k7c6	o
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
páskování	páskování	k1gNnSc6	páskování
hornin	hornina	k1gFnPc2	hornina
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wegenerova	Wegenerův	k2eAgFnSc1d1	Wegenerova
teorie	teorie	k1gFnSc1	teorie
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
driftu	drift	k1gInSc2	drift
byla	být	k5eAaImAgFnS	být
správná	správný	k2eAgFnSc1d1	správná
a	a	k8xC	a
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
teorii	teorie	k1gFnSc3	teorie
dostalo	dostat	k5eAaPmAgNnS	dostat
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
uznání	uznání	k1gNnSc2	uznání
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
komunitou	komunita	k1gFnSc7	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
práce	práce	k1gFnSc1	práce
o	o	k7c4	o
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
Harryho	Harry	k1gMnSc2	Harry
Hesse	Hess	k1gMnSc2	Hess
a	a	k8xC	a
změně	změna	k1gFnSc3	změna
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Rona	Ron	k1gMnSc2	Ron
G.	G.	kA	G.
Masona	mason	k1gMnSc2	mason
<g/>
,	,	kIx,	,
pomohly	pomoct	k5eAaPmAgFnP	pomoct
přesněji	přesně	k6eAd2	přesně
popsat	popsat	k5eAaPmF	popsat
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
výstupu	výstup	k1gInSc2	výstup
magmatu	magma	k1gNnSc2	magma
na	na	k7c6	na
středooceánských	středooceánský	k2eAgInPc6d1	středooceánský
hřbetech	hřbet	k1gInPc6	hřbet
a	a	k8xC	a
vzniku	vznik	k1gInSc6	vznik
nové	nový	k2eAgFnSc2d1	nová
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
po	po	k7c6	po
rozeznání	rozeznání	k1gNnSc6	rozeznání
magnetických	magnetický	k2eAgFnPc2d1	magnetická
anomálií	anomálie	k1gFnPc2	anomálie
projevující	projevující	k2eAgFnSc1d1	projevující
se	s	k7c7	s
symetrickými	symetrický	k2eAgFnPc7d1	symetrická
<g/>
,	,	kIx,	,
paralelními	paralelní	k2eAgInPc7d1	paralelní
pruhy	pruh	k1gInPc7	pruh
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
orientaci	orientace	k1gFnSc6	orientace
tvořící	tvořící	k2eAgFnSc4d1	tvořící
mořské	mořský	k2eAgNnSc4d1	mořské
dno	dno	k1gNnSc4	dno
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
od	od	k7c2	od
středooceánského	středooceánský	k2eAgInSc2d1	středooceánský
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
teorie	teorie	k1gFnSc1	teorie
široce	široko	k6eAd1	široko
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
pokrok	pokrok	k1gInSc4	pokrok
v	v	k7c6	v
seismice	seismika	k1gFnSc6	seismika
umožňující	umožňující	k2eAgFnSc1d1	umožňující
vidět	vidět	k5eAaImF	vidět
události	událost	k1gFnPc4	událost
v	v	k7c4	v
a	a	k8xC	a
okolo	okolo	k6eAd1	okolo
Wadaaati-Benioffovo	Wadaaati-Benioffův	k2eAgNnSc1d1	Wadaaati-Benioffův
zóny	zóna	k1gFnSc2	zóna
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
geologickými	geologický	k2eAgNnPc7d1	geologické
pozorováními	pozorování	k1gNnPc7	pozorování
brzy	brzy	k6eAd1	brzy
podpořily	podpořit	k5eAaPmAgFnP	podpořit
správnost	správnost	k1gFnSc4	správnost
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
a	a	k8xC	a
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
dají	dát	k5eAaPmIp3nP	dát
pochody	pochod	k1gInPc1	pochod
i	i	k9	i
předvídat	předvídat	k5eAaImF	předvídat
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
hlubokého	hluboký	k2eAgNnSc2d1	hluboké
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zrychlilo	zrychlit	k5eAaPmAgNnS	zrychlit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mořské	mořský	k2eAgFnSc2d1	mořská
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
událostí	událost	k1gFnSc7	událost
pro	pro	k7c4	pro
zformování	zformování	k1gNnSc4	zformování
teorie	teorie	k1gFnSc2	teorie
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
teorie	teorie	k1gFnPc1	teorie
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stala	stát	k5eAaPmAgFnS	stát
všeobecně	všeobecně	k6eAd1	všeobecně
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
teorií	teorie	k1gFnSc7	teorie
mezi	mezi	k7c7	mezi
většinou	většina	k1gFnSc7	většina
vědců	vědec	k1gMnPc2	vědec
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
polí	pole	k1gFnPc2	pole
geověd	geověd	k6eAd1	geověd
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
revoluční	revoluční	k2eAgFnSc7d1	revoluční
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pozměnila	pozměnit	k5eAaPmAgFnS	pozměnit
vědy	věda	k1gFnPc4	věda
o	o	k7c6	o
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
vysvětlila	vysvětlit	k5eAaPmAgFnS	vysvětlit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
různorodých	různorodý	k2eAgInPc2d1	různorodý
geologických	geologický	k2eAgInPc2d1	geologický
fenoménů	fenomén	k1gInPc2	fenomén
a	a	k8xC	a
pomohla	pomoct	k5eAaPmAgFnS	pomoct
lépe	dobře	k6eAd2	dobře
pochopit	pochopit	k5eAaPmF	pochopit
další	další	k2eAgInPc4d1	další
jevy	jev	k1gInPc4	jev
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
paleografie	paleografie	k1gFnPc4	paleografie
a	a	k8xC	a
paleobiologie	paleobiologie	k1gFnPc4	paleobiologie
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
hlavní	hlavní	k2eAgFnSc1d1	hlavní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
drift	drift	k1gInSc1	drift
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
publikoval	publikovat	k5eAaBmAgMnS	publikovat
německý	německý	k2eAgMnSc1d1	německý
meteorolog	meteorolog	k1gMnSc1	meteorolog
a	a	k8xC	a
geofyzik	geofyzik	k1gMnSc1	geofyzik
Alfred	Alfred	k1gMnSc1	Alfred
Wegener	Wegener	k1gMnSc1	Wegener
svou	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
driftu	drift	k1gInSc2	drift
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vysvětlovala	vysvětlovat	k5eAaImAgFnS	vysvětlovat
vznik	vznik	k1gInSc4	vznik
kontinentů	kontinent	k1gInPc2	kontinent
rozpadem	rozpad	k1gInSc7	rozpad
původního	původní	k2eAgInSc2d1	původní
superkontinentu	superkontinent	k1gInSc2	superkontinent
Pangea	Pangea	k1gFnSc1	Pangea
<g/>
;	;	kIx,	;
nově	nově	k6eAd1	nově
oddělené	oddělený	k2eAgInPc4d1	oddělený
světadíly	světadíl	k1gInPc4	světadíl
poté	poté	k6eAd1	poté
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
navzájem	navzájem	k6eAd1	navzájem
oddriftovaly	oddriftovat	k5eAaBmAgInP	oddriftovat
do	do	k7c2	do
dnešních	dnešní	k2eAgFnPc2d1	dnešní
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
se	se	k3xPyFc4	se
zakládala	zakládat	k5eAaImAgFnS	zakládat
především	především	k9	především
na	na	k7c6	na
kvantitativních	kvantitativní	k2eAgNnPc6d1	kvantitativní
pozorováních	pozorování	k1gNnPc6	pozorování
(	(	kIx(	(
<g/>
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
učiněna	učiněn	k2eAgFnSc1d1	učiněna
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
např.	např.	kA	např.
Francisem	Francis	k1gInSc7	Francis
Baconem	Bacon	k1gInSc7	Bacon
či	či	k8xC	či
Benjaminem	Benjamin	k1gMnSc7	Benjamin
Franklinem	Franklin	k1gInSc7	Franklin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wegener	Wegener	k1gMnSc1	Wegener
založil	založit	k5eAaPmAgMnS	založit
svoji	svůj	k3xOyFgFnSc4	svůj
hypotézu	hypotéza	k1gFnSc4	hypotéza
na	na	k7c6	na
pozorování	pozorování	k1gNnSc6	pozorování
výskytu	výskyt	k1gInSc2	výskyt
stejných	stejný	k2eAgFnPc2d1	stejná
fosílií	fosílie	k1gFnPc2	fosílie
na	na	k7c6	na
území	území	k1gNnSc6	území
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
paleo-topografických	paleoopografický	k2eAgInPc6d1	paleo-topografický
a	a	k8xC	a
klimatologických	klimatologický	k2eAgInPc6d1	klimatologický
poznatcích	poznatek	k1gInPc6	poznatek
<g/>
,	,	kIx,	,
naznačující	naznačující	k2eAgNnSc4d1	naznačující
spojení	spojení	k1gNnSc4	spojení
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
kontinentů	kontinent	k1gInPc2	kontinent
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
hypotéze	hypotéza	k1gFnSc3	hypotéza
nenabízela	nabízet	k5eNaImAgFnS	nabízet
vhodné	vhodný	k2eAgNnSc4d1	vhodné
fyzikální	fyzikální	k2eAgNnSc4d1	fyzikální
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Uznání	uznání	k1gNnSc1	uznání
Wegenerovy	Wegenerův	k2eAgFnSc2d1	Wegenerova
práce	práce	k1gFnSc2	práce
přišlo	přijít	k5eAaPmAgNnS	přijít
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgInS	zemřít
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
tým	tým	k1gInSc1	tým
vědců	vědec	k1gMnPc2	vědec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Maurice	Maurika	k1gFnSc3	Maurika
Ewinga	Ewing	k1gMnSc4	Ewing
upravili	upravit	k5eAaPmAgMnP	upravit
námořní	námořní	k2eAgFnSc4d1	námořní
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
loď	loď	k1gFnSc4	loď
Atlantis	Atlantis	k1gFnSc1	Atlantis
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
zaštítěného	zaštítěný	k2eAgInSc2d1	zaštítěný
Woods	Woods	k1gInSc1	Woods
Hole	hole	k1gFnSc1	hole
Oceanographic	Oceanographic	k1gMnSc1	Oceanographic
Institution	Institution	k1gInSc1	Institution
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
její	její	k3xOp3gFnSc7	její
pomocí	pomoc	k1gFnSc7	pomoc
následně	následně	k6eAd1	následně
objevili	objevit	k5eAaPmAgMnP	objevit
existenci	existence	k1gFnSc4	existence
středooceánského	středooceánský	k2eAgInSc2d1	středooceánský
hřbetu	hřbet	k1gInSc2	hřbet
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
oceánské	oceánský	k2eAgNnSc1d1	oceánské
dno	dno	k1gNnSc1	dno
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
různě	různě	k6eAd1	různě
mocné	mocný	k2eAgFnSc2d1	mocná
vrstvy	vrstva	k1gFnSc2	vrstva
sedimentů	sediment	k1gInPc2	sediment
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
čedičová	čedičový	k2eAgFnSc1d1	čedičová
vrstva	vrstva	k1gFnSc1	vrstva
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
granitová	granitový	k2eAgFnSc1d1	granitová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
část	část	k1gFnSc4	část
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
objevily	objevit	k5eAaPmAgInP	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
kůra	kůra	k1gFnSc1	kůra
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
nové	nový	k2eAgInPc1d1	nový
poznatky	poznatek	k1gInPc1	poznatek
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
řadu	řada	k1gFnSc4	řada
důležitých	důležitý	k2eAgInPc2d1	důležitý
a	a	k8xC	a
podstatných	podstatný	k2eAgFnPc2d1	podstatná
otázek	otázka	k1gFnPc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
vědci	vědec	k1gMnPc1	vědec
včetně	včetně	k7c2	včetně
Harryho	Harry	k1gMnSc2	Harry
Hesse	Hess	k1gMnSc2	Hess
a	a	k8xC	a
Victora	Victor	k1gMnSc2	Victor
Vacquiera	Vacquier	k1gMnSc2	Vacquier
začali	začít	k5eAaPmAgMnP	začít
používat	používat	k5eAaImF	používat
magnetometr	magnetometr	k1gInSc4	magnetometr
upravený	upravený	k2eAgInSc4d1	upravený
z	z	k7c2	z
leteckého	letecký	k2eAgNnSc2d1	letecké
zařízení	zařízení	k1gNnSc2	zařízení
vyvinutého	vyvinutý	k2eAgNnSc2d1	vyvinuté
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pro	pro	k7c4	pro
detekci	detekce	k1gFnSc4	detekce
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
měření	měření	k1gNnSc4	měření
začali	začít	k5eAaPmAgMnP	začít
rozpoznávat	rozpoznávat	k5eAaImF	rozpoznávat
různé	různý	k2eAgFnPc4d1	různá
magnetické	magnetický	k2eAgFnPc4d1	magnetická
variace	variace	k1gFnPc4	variace
hornin	hornina	k1gFnPc2	hornina
na	na	k7c6	na
mořském	mořský	k2eAgNnSc6d1	mořské
dně	dno	k1gNnSc6	dno
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pozorování	pozorování	k1gNnSc1	pozorování
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
nečekané	čekaný	k2eNgFnPc1d1	nečekaná
<g/>
,	,	kIx,	,
překvapilo	překvapit	k5eAaPmAgNnS	překvapit
vědce	vědec	k1gMnSc4	vědec
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bazalty	bazalt	k1gInPc1	bazalt
tvořící	tvořící	k2eAgFnSc4d1	tvořící
oceánské	oceánský	k2eAgNnSc4d1	oceánské
dno	dno	k1gNnSc4	dno
jsou	být	k5eAaImIp3nP	být
železem	železo	k1gNnSc7	železo
bohaté	bohatý	k2eAgFnSc2d1	bohatá
horniny	hornina	k1gFnSc2	hornina
obsahující	obsahující	k2eAgMnSc1d1	obsahující
silně	silně	k6eAd1	silně
magnetické	magnetický	k2eAgInPc4d1	magnetický
minerály	minerál	k1gInPc4	minerál
jako	jako	k8xS	jako
např.	např.	kA	např.
magnetit	magnetit	k1gInSc1	magnetit
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
způsobovaly	způsobovat	k5eAaImAgFnP	způsobovat
lokální	lokální	k2eAgFnPc4d1	lokální
odchylky	odchylka	k1gFnPc4	odchylka
v	v	k7c4	v
měření	měření	k1gNnPc4	měření
kompasem	kompas	k1gInSc7	kompas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
již	již	k9	již
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
islandskými	islandský	k2eAgMnPc7d1	islandský
námořníky	námořník	k1gMnPc7	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Důležitějším	důležitý	k2eAgFnPc3d2	důležitější
než	než	k8xS	než
odklánění	odklánění	k1gNnSc3	odklánění
kompasu	kompas	k1gInSc2	kompas
se	se	k3xPyFc4	se
ale	ale	k9	ale
stala	stát	k5eAaPmAgFnS	stát
schopnost	schopnost	k1gFnSc4	schopnost
bazaltů	bazalt	k1gInPc2	bazalt
zachovávat	zachovávat	k5eAaImF	zachovávat
si	se	k3xPyFc3	se
zmagnetizování	zmagnetizování	k1gNnSc4	zmagnetizování
po	po	k7c6	po
utuhnutí	utuhnutí	k1gNnSc6	utuhnutí
z	z	k7c2	z
lávy	láva	k1gFnSc2	láva
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
nový	nový	k2eAgInSc4d1	nový
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
utuhnutí	utuhnutí	k1gNnSc2	utuhnutí
lávy	láva	k1gFnSc2	láva
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
bazaltů	bazalt	k1gInPc2	bazalt
zaznamená	zaznamenat	k5eAaPmIp3nS	zaznamenat
orientace	orientace	k1gFnSc1	orientace
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
v	v	k7c6	v
čase	čas	k1gInSc6	čas
vzniku	vznik	k1gInSc2	vznik
horniny	hornina	k1gFnSc2	hornina
vlivem	vlivem	k7c2	vlivem
natočení	natočení	k1gNnSc2	natočení
drobných	drobný	k2eAgFnPc2d1	drobná
částic	částice	k1gFnPc2	částice
magnetitu	magnetit	k1gInSc2	magnetit
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
mapováno	mapovat	k5eAaImNgNnS	mapovat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc6d2	veliký
oblasti	oblast	k1gFnSc6	oblast
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
magnetické	magnetický	k2eAgFnPc1d1	magnetická
variace	variace	k1gFnPc1	variace
netvoří	tvořit	k5eNaImIp3nP	tvořit
nahodilé	nahodilý	k2eAgInPc1d1	nahodilý
shluky	shluk	k1gInPc1	shluk
nebo	nebo	k8xC	nebo
izolované	izolovaný	k2eAgInPc1d1	izolovaný
pruhy	pruh	k1gInPc1	pruh
zmagnetizovaných	zmagnetizovaný	k2eAgFnPc2d1	zmagnetizovaná
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpoznatelné	rozpoznatelný	k2eAgNnSc4d1	rozpoznatelné
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
tyto	tyt	k2eAgNnSc1d1	tyto
magnetické	magnetický	k2eAgNnSc1d1	magnetické
chování	chování	k1gNnSc1	chování
zmapováno	zmapovat	k5eAaPmNgNnS	zmapovat
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
páskování	páskování	k1gNnSc1	páskování
připomíná	připomínat	k5eAaImIp3nS	připomínat
vzhledově	vzhledově	k6eAd1	vzhledově
pruhy	pruh	k1gInPc4	pruh
zebry	zebra	k1gFnSc2	zebra
<g/>
.	.	kIx.	.
</s>
<s>
Střídající	střídající	k2eAgFnSc1d1	střídající
se	s	k7c7	s
pruhy	pruh	k1gInPc7	pruh
zmagnetizovaných	zmagnetizovaný	k2eAgFnPc2d1	zmagnetizovaná
hornin	hornina	k1gFnPc2	hornina
ležících	ležící	k2eAgFnPc2d1	ležící
v	v	k7c6	v
pruzích	pruh	k1gInPc6	pruh
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
středooceánského	středooceánský	k2eAgInSc2d1	středooceánský
hřbetu	hřbet	k1gInSc2	hřbet
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
polarity	polarita	k1gFnSc2	polarita
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
takovýchto	takovýto	k3xDgInPc2	takovýto
sledů	sled	k1gInPc2	sled
hornin	hornina	k1gFnPc2	hornina
se	se	k3xPyFc4	se
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc1	označení
magnetické	magnetický	k2eAgNnSc1d1	magnetické
páskování	páskování	k1gNnSc1	páskování
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vrstvy	vrstva	k1gFnPc1	vrstva
hornin	hornina	k1gFnPc2	hornina
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
kontinentech	kontinent	k1gInPc6	kontinent
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
,	,	kIx,	,
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgFnSc2	takový
horniny	hornina	k1gFnSc2	hornina
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
některé	některý	k3yIgFnPc4	některý
oblasti	oblast	k1gFnPc4	oblast
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
horniny	hornina	k1gFnPc1	hornina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgFnPc1d1	podobná
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Newfoundlandu	Newfoundland	k1gInSc2	Newfoundland
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Nového	Nového	k2eAgInSc2d1	Nového
Brunšviku	Brunšvik	k1gInSc2	Brunšvik
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Kaledonindy	Kaledoninda	k1gFnPc1	Kaledoninda
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgInPc1d1	podobný
Apalačskému	Apalačský	k2eAgNnSc3d1	Apalačské
pohoří	pohoří	k1gNnSc3	pohoří
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
složení	složení	k1gNnSc2	složení
tak	tak	k8xC	tak
i	i	k9	i
strukturní	strukturní	k2eAgInPc1d1	strukturní
<g/>
,	,	kIx,	,
když	když	k8xS	když
zde	zde	k6eAd1	zde
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
proces	proces	k1gInSc1	proces
tzv.	tzv.	kA	tzv.
Grenvillského	Grenvillský	k2eAgNnSc2d1	Grenvillský
vrásnění	vrásnění	k1gNnSc2	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
rozvoji	rozvoj	k1gInSc3	rozvoj
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
také	také	k9	také
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
použitím	použití	k1gNnSc7	použití
těchto	tento	k3xDgFnPc2	tento
technologií	technologie	k1gFnPc2	technologie
tak	tak	k9	tak
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
definitivně	definitivně	k6eAd1	definitivně
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
existence	existence	k1gFnSc1	existence
středooceánského	středooceánský	k2eAgInSc2d1	středooceánský
hřbetu	hřbet	k1gInSc2	hřbet
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
také	také	k9	také
zjištěny	zjistit	k5eAaPmNgFnP	zjistit
střídavé	střídavý	k2eAgFnPc1d1	střídavá
(	(	kIx(	(
<g/>
kladné	kladný	k2eAgFnPc1d1	kladná
a	a	k8xC	a
záporné	záporný	k2eAgFnPc1d1	záporná
<g/>
)	)	kIx)	)
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
magnetické	magnetický	k2eAgFnPc1d1	magnetická
anomálie	anomálie	k1gFnPc1	anomálie
na	na	k7c6	na
okolním	okolní	k2eAgNnSc6d1	okolní
mořském	mořský	k2eAgNnSc6d1	mořské
dně	dno	k1gNnSc6	dno
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
s	s	k7c7	s
teorií	teorie	k1gFnSc7	teorie
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
–	–	k?	–
uprostřed	uprostřed	k7c2	uprostřed
oceánů	oceán	k1gInPc2	oceán
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
nové	nový	k2eAgFnSc2d1	nová
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
současnou	současný	k2eAgFnSc4d1	současná
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
orientaci	orientace	k1gFnSc4	orientace
geomagnetického	geomagnetický	k2eAgNnSc2d1	geomagnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
pozice	pozice	k1gFnSc1	pozice
magnetického	magnetický	k2eAgInSc2d1	magnetický
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
vzorec	vzorec	k1gInSc1	vzorec
lineárních	lineární	k2eAgFnPc2d1	lineární
magnetických	magnetický	k2eAgFnPc2d1	magnetická
anomálií	anomálie	k1gFnPc2	anomálie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nebyla	být	k5eNaImAgFnS	být
jen	jen	k9	jen
podpořena	podpořit	k5eAaPmNgFnS	podpořit
teorie	teorie	k1gFnSc1	teorie
rozpínání	rozpínání	k1gNnSc2	rozpínání
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
pozorování	pozorování	k1gNnSc3	pozorování
vedla	vést	k5eAaImAgFnS	vést
i	i	k9	i
k	k	k7c3	k
hypotéze	hypotéza	k1gFnSc3	hypotéza
přepólování	přepólování	k1gNnSc2	přepólování
geomagnetického	geomagnetický	k2eAgNnSc2d1	geomagnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Převládající	převládající	k2eAgInSc1d1	převládající
koncept	koncept	k1gInSc1	koncept
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
kontinenty	kontinent	k1gInPc4	kontinent
nacházela	nacházet	k5eAaImAgFnS	nacházet
statická	statický	k2eAgFnSc1d1	statická
vrstva	vrstva	k1gFnSc1	vrstva
tvořící	tvořící	k2eAgFnSc1d1	tvořící
jakousi	jakýsi	k3yIgFnSc4	jakýsi
skořápku	skořápka	k1gFnSc4	skořápka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
granitem	granit	k1gInSc7	granit
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
hustším	hustý	k2eAgInSc7d2	hustší
bazaltem	bazalt	k1gInSc7	bazalt
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
bazaltová	bazaltový	k2eAgFnSc1d1	bazaltová
vrstva	vrstva	k1gFnSc1	vrstva
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
ležet	ležet	k5eAaImF	ležet
i	i	k9	i
pod	pod	k7c7	pod
kontinenty	kontinent	k1gInPc7	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
měření	měření	k1gNnPc1	měření
olovnicí	olovnice	k1gFnSc7	olovnice
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
abnormality	abnormalita	k1gFnSc2	abnormalita
v	v	k7c6	v
odklonu	odklon	k1gInSc6	odklon
směřování	směřování	k1gNnSc2	směřování
olovnice	olovnice	k1gFnSc2	olovnice
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
orogenních	orogenní	k2eAgInPc2d1	orogenní
kořenů	kořen	k1gInPc2	kořen
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
Andách	Anda	k1gFnPc6	Anda
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Pierre	Pierr	k1gInSc5	Pierr
Bouguer	Bougura	k1gFnPc2	Bougura
začal	začít	k5eAaPmAgMnS	začít
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
méně	málo	k6eAd2	málo
husté	hustý	k2eAgNnSc1d1	husté
horstva	horstvo	k1gNnPc1	horstvo
musí	muset	k5eAaImIp3nP	muset
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
hustější	hustý	k2eAgFnSc2d2	hustší
vrstvy	vrstva	k1gFnSc2	vrstva
pod	pod	k7c7	pod
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
<g/>
,	,	kIx,	,
že	že	k8xS	že
hory	hora	k1gFnPc1	hora
mají	mít	k5eAaImIp3nP	mít
"	"	kIx"	"
<g/>
kořeny	kořen	k1gInPc4	kořen
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
potvrzen	potvrdit	k5eAaPmNgMnS	potvrdit
geologem	geolog	k1gMnSc7	geolog
Georgem	Georg	k1gMnSc7	Georg
Airym	Airym	k1gInSc4	Airym
o	o	k7c4	o
století	století	k1gNnSc4	století
později	pozdě	k6eAd2	pozdě
během	během	k7c2	během
jeho	on	k3xPp3gInSc2	on
výzkumu	výzkum	k1gInSc2	výzkum
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
Himálaje	Himálaj	k1gFnSc2	Himálaj
a	a	k8xC	a
seismických	seismický	k2eAgFnPc2d1	seismická
studií	studie	k1gFnPc2	studie
zaznamenávajících	zaznamenávající	k2eAgFnPc2d1	zaznamenávající
rozdílné	rozdílný	k2eAgMnPc4d1	rozdílný
hustoty	hustota	k1gFnSc2	hustota
vrstev	vrstva	k1gFnPc2	vrstva
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
stále	stále	k6eAd1	stále
otevřená	otevřený	k2eAgFnSc1d1	otevřená
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
orogenní	orogenní	k2eAgInPc1d1	orogenní
kořeny	kořen	k1gInPc1	kořen
jsou	být	k5eAaImIp3nP	být
zanořeny	zanořen	k2eAgInPc1d1	zanořen
do	do	k7c2	do
okolní	okolní	k2eAgFnSc2d1	okolní
bazaltové	bazaltový	k2eAgFnSc2d1	bazaltová
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
či	či	k8xC	či
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
vznášejí	vznášet	k5eAaImIp3nP	vznášet
jako	jako	k9	jako
ledové	ledový	k2eAgFnPc1d1	ledová
kry	kra	k1gFnPc1	kra
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
tak	tak	k6eAd1	tak
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
modely	model	k1gInPc1	model
<g/>
,	,	kIx,	,
Airyho	Airy	k1gMnSc2	Airy
model	model	k1gInSc1	model
a	a	k8xC	a
Prattův	Prattův	k2eAgInSc1d1	Prattův
model	model	k1gInSc1	model
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
australský	australský	k2eAgMnSc1d1	australský
geolog	geolog	k1gMnSc1	geolog
Samuel	Samuel	k1gMnSc1	Samuel
Warren	Warrna	k1gFnPc2	Warrna
Carey	Carea	k1gFnSc2	Carea
publikoval	publikovat	k5eAaBmAgMnS	publikovat
esej	esej	k1gFnSc4	esej
The	The	k1gMnSc1	The
tectonic	tectonice	k1gFnPc2	tectonice
approach	approach	k1gInSc1	approach
to	ten	k3xDgNnSc1	ten
continental	continentat	k5eAaPmAgInS	continentat
drift	drift	k1gInSc1	drift
podporující	podporující	k2eAgInSc4d1	podporující
model	model	k1gInSc4	model
expandující	expandující	k2eAgFnSc2d1	expandující
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
pokrok	pokrok	k1gInSc1	pokrok
byl	být	k5eAaImAgInS	být
učiněn	učinit	k5eAaPmNgInS	učinit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vlivem	vlivem	k7c2	vlivem
řady	řada	k1gFnSc2	řada
objevů	objev	k1gInPc2	objev
jako	jako	k9	jako
například	například	k6eAd1	například
objevením	objevení	k1gNnSc7	objevení
středoatlantského	středoatlantský	k2eAgInSc2d1	středoatlantský
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
publikací	publikace	k1gFnSc7	publikace
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
práce	práce	k1gFnSc1	práce
amerického	americký	k2eAgMnSc2d1	americký
geologa	geolog	k1gMnSc2	geolog
Harryho	Harry	k1gMnSc2	Harry
Hesse	Hess	k1gMnSc2	Hess
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
S.	S.	kA	S.
Dietz	Dietz	k1gMnSc1	Dietz
publikoval	publikovat	k5eAaBmAgMnS	publikovat
stejnou	stejný	k2eAgFnSc4d1	stejná
myšlenku	myšlenka	k1gFnSc4	myšlenka
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Nature	Natur	k1gInSc5	Natur
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
prvenství	prvenství	k1gNnSc1	prvenství
patří	patřit	k5eAaImIp3nS	patřit
Hessovi	Hess	k1gMnSc3	Hess
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
svoji	svůj	k3xOyFgFnSc4	svůj
nepublikovanou	publikovaný	k2eNgFnSc4d1	nepublikovaná
práci	práce	k1gFnSc4	práce
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hess	Hess	k6eAd1	Hess
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kontinenty	kontinent	k1gInPc7	kontinent
nepohybují	pohybovat	k5eNaImIp3nP	pohybovat
po	po	k7c6	po
oceánské	oceánský	k2eAgFnSc6d1	oceánská
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
předpokládáno	předpokládat	k5eAaImNgNnS	předpokládat
u	u	k7c2	u
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
driftu	drift	k1gInSc2	drift
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
se	se	k3xPyFc4	se
oceánské	oceánský	k2eAgFnPc1d1	oceánská
pánve	pánev	k1gFnPc1	pánev
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
kontinenty	kontinent	k1gInPc7	kontinent
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
Robert	Robert	k1gMnSc1	Robert
R.	R.	kA	R.
Coats	Coats	k1gInSc1	Coats
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
geologické	geologický	k2eAgFnSc2d1	geologická
služby	služba	k1gFnSc2	služba
popsal	popsat	k5eAaPmAgMnS	popsat
hlavní	hlavní	k2eAgInPc4d1	hlavní
útvary	útvar	k1gInPc4	útvar
subdukce	subdukce	k1gFnSc2	subdukce
ostrovního	ostrovní	k2eAgInSc2d1	ostrovní
oblouku	oblouk	k1gInSc2	oblouk
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Aleutských	aleutský	k2eAgInPc2d1	aleutský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
publikace	publikace	k1gFnSc1	publikace
zesměšňována	zesměšňován	k2eAgFnSc1d1	zesměšňována
a	a	k8xC	a
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
jen	jen	k9	jen
malý	malý	k2eAgInSc4d1	malý
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
značného	značný	k2eAgNnSc2d1	značné
uznání	uznání	k1gNnSc2	uznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
W.	W.	kA	W.
Jason	Jason	k1gMnSc1	Jason
Morgan	morgan	k1gMnSc1	morgan
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
povrch	povrch	k1gInSc1	povrch
Země	zem	k1gFnSc2	zem
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
12	[number]	k4	12
pevných	pevný	k2eAgFnPc2d1	pevná
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
Xavier	Xavier	k1gMnSc1	Xavier
Le	Le	k1gMnSc1	Le
Pichon	Pichon	k1gMnSc1	Pichon
publikoval	publikovat	k5eAaBmAgMnS	publikovat
kompletní	kompletní	k2eAgInSc4d1	kompletní
model	model	k1gInSc4	model
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
obsahující	obsahující	k2eAgInPc1d1	obsahující
6	[number]	k4	6
hlavních	hlavní	k2eAgFnPc2d1	hlavní
desek	deska	k1gFnPc2	deska
s	s	k7c7	s
jejich	jejich	k3xOp3gInPc7	jejich
relativními	relativní	k2eAgInPc7d1	relativní
pohyby	pohyb	k1gInPc7	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Objevení	objevení	k1gNnSc1	objevení
magnetického	magnetický	k2eAgNnSc2d1	magnetické
páskování	páskování	k1gNnSc2	páskování
rozprostřeného	rozprostřený	k2eAgNnSc2d1	rozprostřené
symetricky	symetricky	k6eAd1	symetricky
kolem	kolem	k7c2	kolem
hřebenů	hřeben	k1gInPc2	hřeben
středooceánských	středooceánský	k2eAgInPc2d1	středooceánský
hřbetů	hřbet	k1gInPc2	hřbet
naznačovalo	naznačovat	k5eAaImAgNnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
fenomény	fenomén	k1gInPc1	fenomén
spolu	spolu	k6eAd1	spolu
nějak	nějak	k6eAd1	nějak
spojeny	spojit	k5eAaPmNgInP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
vědci	vědec	k1gMnPc1	vědec
začali	začít	k5eAaPmAgMnP	začít
teoretizovat	teoretizovat	k5eAaImF	teoretizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
středooceánské	středooceánský	k2eAgInPc1d1	středooceánský
hřbety	hřbet	k1gInPc1	hřbet
označují	označovat	k5eAaImIp3nP	označovat
strukturně	strukturně	k6eAd1	strukturně
slabé	slabý	k2eAgFnSc2d1	slabá
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
oceánské	oceánský	k2eAgNnSc1d1	oceánské
dno	dno	k1gNnSc1	dno
je	být	k5eAaImIp3nS	být
rozlomeno	rozlomen	k2eAgNnSc1d1	rozlomeno
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
části	část	k1gFnPc4	část
podél	podél	k7c2	podél
středového	středový	k2eAgInSc2d1	středový
hřebenu	hřeben	k1gInSc2	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
magma	magma	k1gNnSc1	magma
vystupující	vystupující	k2eAgNnSc1d1	vystupující
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
Země	zem	k1gFnSc2	zem
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
snadno	snadno	k6eAd1	snadno
skrze	skrze	k?	skrze
tuto	tento	k3xDgFnSc4	tento
oslabenou	oslabený	k2eAgFnSc4d1	oslabená
zónu	zóna	k1gFnSc4	zóna
a	a	k8xC	a
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
vylévá	vylévat	k5eAaImIp3nS	vylévat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
lávy	láva	k1gFnSc2	láva
na	na	k7c4	na
mořské	mořský	k2eAgNnSc4d1	mořské
dno	dno	k1gNnSc4	dno
podél	podél	k7c2	podél
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
novou	nový	k2eAgFnSc4d1	nová
oceánskou	oceánský	k2eAgFnSc4d1	oceánská
kůru	kůra	k1gFnSc4	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazýván	nazýván	k2eAgInSc1d1	nazýván
rozpínání	rozpínání	k1gNnSc4	rozpínání
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
nepřetržitě	přetržitě	k6eNd1	přetržitě
na	na	k7c6	na
oceánském	oceánský	k2eAgNnSc6d1	oceánské
dně	dno	k1gNnSc6	dno
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
okolo	okolo	k7c2	okolo
50	[number]	k4	50
000	[number]	k4	000
km	km	kA	km
okolo	okolo	k7c2	okolo
středooceánských	středooceánský	k2eAgInPc2d1	středooceánský
hřbetů	hřbet	k1gInPc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
byla	být	k5eAaImAgFnS	být
podpořena	podpořit	k5eAaPmNgFnS	podpořit
několika	několik	k4yIc7	několik
důkazy	důkaz	k1gInPc7	důkaz
<g/>
:	:	kIx,	:
hřbet	hřbet	k1gInSc1	hřbet
či	či	k8xC	či
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
velice	velice	k6eAd1	velice
mladé	mladý	k2eAgFnSc2d1	mladá
horniny	hornina	k1gFnSc2	hornina
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
stáří	stáří	k1gNnSc2	stáří
narůstá	narůstat	k5eAaImIp3nS	narůstat
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
hřbetu	hřbet	k1gInSc2	hřbet
nejmladší	mladý	k2eAgFnSc2d3	nejmladší
horniny	hornina	k1gFnSc2	hornina
hřbetu	hřbet	k1gInSc2	hřbet
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vždycky	vždycky	k6eAd1	vždycky
dnešní	dnešní	k2eAgFnSc4d1	dnešní
(	(	kIx(	(
<g/>
normální	normální	k2eAgFnSc4d1	normální
<g/>
)	)	kIx)	)
polaritu	polarita	k1gFnSc4	polarita
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
pásy	pás	k1gInPc1	pás
hornin	hornina	k1gFnPc2	hornina
rovnoběžné	rovnoběžný	k2eAgFnPc4d1	rovnoběžná
s	s	k7c7	s
hřbetem	hřbet	k1gInSc7	hřbet
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
v	v	k7c6	v
magnetické	magnetický	k2eAgFnSc6d1	magnetická
polaritě	polarita	k1gFnSc6	polarita
(	(	kIx(	(
<g/>
normální-reversní-normální	normálníeversníormální	k2eAgMnSc1d1	normální-reversní-normální
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
naznačující	naznačující	k2eAgFnSc2d1	naznačující
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Země	zem	k1gFnSc2	zem
přepólovávalo	přepólovávat	k5eAaPmAgNnS	přepólovávat
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
mnohokrát	mnohokrát	k6eAd1	mnohokrát
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
zebrovitého	zebrovitý	k2eAgNnSc2d1	zebrovitý
páskování	páskování	k1gNnSc2	páskování
polarity	polarita	k1gFnSc2	polarita
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
procesy	proces	k1gInPc4	proces
odehrávajícími	odehrávající	k2eAgMnPc7d1	odehrávající
se	se	k3xPyFc4	se
na	na	k7c6	na
středooceánských	středooceánský	k2eAgInPc6d1	středooceánský
hřbetech	hřbet	k1gInPc6	hřbet
se	se	k3xPyFc4	se
hypotéza	hypotéza	k1gFnSc1	hypotéza
rozpínajícího	rozpínající	k2eAgMnSc2d1	rozpínající
se	se	k3xPyFc4	se
oceánského	oceánský	k2eAgInSc2d1	oceánský
dna	dno	k1gNnSc2	dno
rychle	rychle	k6eAd1	rychle
dočkala	dočkat	k5eAaPmAgFnS	dočkat
řady	řada	k1gFnSc2	řada
zastánců	zastánce	k1gMnPc2	zastánce
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
záznamové	záznamový	k2eAgNnSc4d1	záznamové
médium	médium	k1gNnSc4	médium
schopné	schopný	k2eAgNnSc1d1	schopné
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
historii	historie	k1gFnSc4	historie
přepólovávání	přepólovávání	k1gNnSc2	přepólovávání
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Subdukce	Subdukce	k1gFnSc2	Subdukce
<g/>
.	.	kIx.	.
</s>
<s>
Podstatným	podstatný	k2eAgInSc7d1	podstatný
důsledkem	důsledek	k1gInSc7	důsledek
rozpínání	rozpínání	k1gNnSc4	rozpínání
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
docházelo	docházet	k5eAaImAgNnS	docházet
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
neustále	neustále	k6eAd1	neustále
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nové	nový	k2eAgFnSc2d1	nová
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
podél	podél	k7c2	podél
středooceánských	středooceánský	k2eAgInPc2d1	středooceánský
hřbetů	hřbet	k1gInPc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
našel	najít	k5eAaPmAgMnS	najít
velikého	veliký	k2eAgInSc2d1	veliký
ohlasu	ohlas	k1gInSc2	ohlas
mezi	mezi	k7c7	mezi
vědci	vědec	k1gMnPc7	vědec
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
S.	S.	kA	S.
Warrenem	Warren	k1gInSc7	Warren
Carey	Carea	k1gFnSc2	Carea
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
rozpínání	rozpínání	k1gNnSc1	rozpínání
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
a	a	k8xC	a
pohyb	pohyb	k1gInSc1	pohyb
kontinentů	kontinent	k1gInPc2	kontinent
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nejsnáze	snadno	k6eAd3	snadno
vysvětleno	vysvětlit	k5eAaPmNgNnS	vysvětlit
velkým	velký	k2eAgInSc7d1	velký
nárůstem	nárůst	k1gInSc7	nárůst
velikosti	velikost	k1gFnSc2	velikost
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
expandující	expandující	k2eAgFnSc2d1	expandující
Země	zem	k1gFnSc2	zem
byla	být	k5eAaImAgFnS	být
neuspokojující	uspokojující	k2eNgFnSc1d1	neuspokojující
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nenabízela	nabízet	k5eNaImAgNnP	nabízet
žádné	žádný	k3yNgNnSc4	žádný
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
pomocí	pomocí	k7c2	pomocí
známého	známý	k2eAgInSc2d1	známý
mechanismu	mechanismus	k1gInSc2	mechanismus
pro	pro	k7c4	pro
takto	takto	k6eAd1	takto
dramatické	dramatický	k2eAgNnSc4d1	dramatické
zvětšování	zvětšování	k1gNnSc4	zvětšování
objemu	objem	k1gInSc2	objem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
pozorování	pozorování	k1gNnSc6	pozorování
Měsíce	měsíc	k1gInSc2	měsíc
nepřinesla	přinést	k5eNaPmAgFnS	přinést
žádné	žádný	k3yNgInPc4	žádný
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
on	on	k3xPp3gMnSc1	on
za	za	k7c2	za
poslední	poslední	k2eAgFnSc2d1	poslední
tři	tři	k4xCgFnPc4	tři
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
zvětšil	zvětšit	k5eAaPmAgInS	zvětšit
<g/>
.	.	kIx.	.
</s>
<s>
Zůstala	zůstat	k5eAaPmAgFnS	zůstat
tak	tak	k9	tak
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tvořila	tvořit	k5eAaImAgFnS	tvořit
nová	nový	k2eAgFnSc1d1	nová
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
zvětšovala	zvětšovat	k5eAaImAgFnS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
otázka	otázka	k1gFnSc1	otázka
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
Harryho	Harry	k1gMnSc4	Harry
Hesse	Hess	k1gMnSc4	Hess
<g/>
,	,	kIx,	,
geologa	geolog	k1gMnSc4	geolog
z	z	k7c2	z
Princeton	Princeton	k1gInSc4	Princeton
University	universita	k1gFnSc2	universita
a	a	k8xC	a
námořního	námořní	k2eAgMnSc2d1	námořní
admirála	admirál	k1gMnSc2	admirál
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Dietzem	Dietz	k1gInSc7	Dietz
z	z	k7c2	z
USGS	USGS	kA	USGS
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
použil	použít	k5eAaPmAgMnS	použít
termín	termín	k1gInSc4	termín
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Dietz	Dietza	k1gFnPc2	Dietza
a	a	k8xC	a
Hess	Hessa	k1gFnPc2	Hessa
byli	být	k5eAaImAgMnP	být
mezi	mezi	k7c7	mezi
malou	malý	k2eAgFnSc7d1	malá
hrstkou	hrstka	k1gFnSc7	hrstka
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
skutečně	skutečně	k6eAd1	skutečně
rozuměli	rozumět	k5eAaImAgMnP	rozumět
širokým	široký	k2eAgNnSc7d1	široké
důsledkům	důsledek	k1gInPc3	důsledek
vyplývajících	vyplývající	k2eAgInPc2d1	vyplývající
z	z	k7c2	z
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Hess	Hess	k6eAd1	Hess
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
podél	podél	k7c2	podél
hřbetů	hřbet	k1gInPc2	hřbet
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
někde	někde	k6eAd1	někde
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
ponořování	ponořování	k1gNnSc3	ponořování
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
se	se	k3xPyFc4	se
od	od	k7c2	od
hřbetů	hřbet	k1gInPc2	hřbet
bude	být	k5eAaImBp3nS	být
neustále	neustále	k6eAd1	neustále
šířit	šířit	k5eAaImF	šířit
dál	daleko	k6eAd2	daleko
postupným	postupný	k2eAgInSc7d1	postupný
pohybem	pohyb	k1gInSc7	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
miliónech	milión	k4xCgInPc6	milión
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
hlubokomořských	hlubokomořský	k2eAgInPc2d1	hlubokomořský
příkopů	příkop	k1gInPc2	příkop
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
velmi	velmi	k6eAd1	velmi
hluboké	hluboký	k2eAgInPc1d1	hluboký
kaňony	kaňon	k1gInPc1	kaňon
obepínající	obepínající	k2eAgInSc1d1	obepínající
Tichý	tichý	k2eAgInSc4d1	tichý
oceán	oceán	k1gInSc4	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Hess	Hess	k6eAd1	Hess
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Atlantický	atlantický	k2eAgInSc1d1	atlantický
oceán	oceán	k1gInSc1	oceán
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
ponořovat	ponořovat	k5eAaImF	ponořovat
a	a	k8xC	a
tedy	tedy	k9	tedy
zmenšovat	zmenšovat	k5eAaImF	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
stará	starý	k2eAgFnSc1d1	stará
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
tavena	taven	k2eAgFnSc1d1	tavena
v	v	k7c6	v
příkopech	příkop	k1gInPc6	příkop
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc4d1	nové
magma	magma	k1gNnSc4	magma
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dostávalo	dostávat	k5eAaImAgNnS	dostávat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
způsobovalo	způsobovat	k5eAaImAgNnS	způsobovat
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
a	a	k8xC	a
formování	formování	k1gNnSc2	formování
nové	nový	k2eAgFnSc2d1	nová
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
Hess	Hess	k1gInSc1	Hess
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oceánské	oceánský	k2eAgFnSc2d1	oceánská
pánve	pánev	k1gFnSc2	pánev
jsou	být	k5eAaImIp3nP	být
neustále	neustále	k6eAd1	neustále
recyklovány	recyklován	k2eAgFnPc1d1	recyklována
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
navržený	navržený	k2eAgInSc1d1	navržený
Hessem	Hess	k1gMnSc7	Hess
tak	tak	k8xS	tak
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
nezvětšuje	zvětšovat	k5eNaImIp3nS	zvětšovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
neustále	neustále	k6eAd1	neustále
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
mocnost	mocnost	k1gFnSc1	mocnost
sedimentů	sediment	k1gInPc2	sediment
na	na	k7c6	na
oceánském	oceánský	k2eAgNnSc6d1	oceánské
dně	dno	k1gNnSc6	dno
a	a	k8xC	a
proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
oceánské	oceánský	k2eAgFnSc2d1	oceánská
horniny	hornina	k1gFnSc2	hornina
značně	značně	k6eAd1	značně
mladší	mladý	k2eAgMnSc1d2	mladší
než	než	k8xS	než
horniny	hornina	k1gFnPc1	hornina
z	z	k7c2	z
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zlepšení	zlepšení	k1gNnPc2	zlepšení
a	a	k8xC	a
větší	veliký	k2eAgNnPc4d2	veliký
používání	používání	k1gNnPc4	používání
seismických	seismický	k2eAgInPc2d1	seismický
přístrojů	přístroj	k1gInPc2	přístroj
jako	jako	k9	jako
například	například	k6eAd1	například
seismografu	seismograf	k1gInSc2	seismograf
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vědcům	vědec	k1gMnPc3	vědec
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
nahodile	nahodile	k6eAd1	nahodile
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
soustředěn	soustředěn	k2eAgInSc1d1	soustředěn
do	do	k7c2	do
specifických	specifický	k2eAgFnPc2d1	specifická
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
okolo	okolo	k7c2	okolo
hlubokomořských	hlubokomořský	k2eAgInPc2d1	hlubokomořský
příkopů	příkop	k1gInPc2	příkop
a	a	k8xC	a
středooceánských	středooceánský	k2eAgInPc2d1	středooceánský
hřbetů	hřbet	k1gInPc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
seismologové	seismolog	k1gMnPc1	seismolog
začali	začít	k5eAaPmAgMnP	začít
rozpoznávat	rozpoznávat	k5eAaImF	rozpoznávat
několik	několik	k4yIc4	několik
hlavních	hlavní	k2eAgFnPc2d1	hlavní
zemětřesných	zemětřesný	k2eAgFnPc2d1	zemětřesná
zón	zóna	k1gFnPc2	zóna
rovnoběžných	rovnoběžný	k2eAgFnPc2d1	rovnoběžná
s	s	k7c7	s
příkopy	příkop	k1gInPc7	příkop
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
většinou	většinou	k6eAd1	většinou
ukloněny	ukloněn	k2eAgInPc4d1	ukloněn
40	[number]	k4	40
až	až	k8xS	až
60	[number]	k4	60
<g/>
°	°	k?	°
od	od	k7c2	od
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
roviny	rovina	k1gFnSc2	rovina
a	a	k8xC	a
táhnoucí	táhnoucí	k2eAgMnSc1d1	táhnoucí
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
do	do	k7c2	do
zemského	zemský	k2eAgNnSc2d1	zemské
nitra	nitro	k1gNnSc2	nitro
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
zóny	zóna	k1gFnPc4	zóna
se	se	k3xPyFc4	se
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc3	označení
Wadati-Benioffova	Wadati-Benioffův	k2eAgFnSc1d1	Wadati-Benioffův
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
dvou	dva	k4xCgMnPc2	dva
seismologů	seismolog	k1gMnPc2	seismolog
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
je	být	k5eAaImIp3nS	být
jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
rozpoznali	rozpoznat	k5eAaPmAgMnP	rozpoznat
<g/>
:	:	kIx,	:
Kiyoo	Kiyoo	k6eAd1	Kiyoo
Wadati	Wadat	k1gMnPc1	Wadat
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
Hugo	Hugo	k1gMnSc1	Hugo
Benioff	Benioff	k1gMnSc1	Benioff
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Studování	studování	k1gNnSc1	studování
globálního	globální	k2eAgInSc2d1	globální
výskytu	výskyt	k1gInSc2	výskyt
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
založením	založení	k1gNnSc7	založení
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
seismologické	seismologický	k2eAgFnSc2d1	seismologická
sítě	síť	k1gFnSc2	síť
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xC	jako
Worldwide	Worldwid	k1gInSc5	Worldwid
Standardized	Standardized	k1gInSc1	Standardized
Seismograph	Seismograph	k1gMnSc1	Seismograph
Network	network	k1gInSc1	network
(	(	kIx(	(
<g/>
WWSSN	WWSSN	kA	WWSSN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
zákaz	zákaz	k1gInSc4	zákaz
experimentálních	experimentální	k2eAgFnPc2d1	experimentální
podzemních	podzemní	k2eAgFnPc2d1	podzemní
jaderných	jaderný	k2eAgFnPc2d1	jaderná
explozí	exploze	k1gFnPc2	exploze
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
tak	tak	k9	tak
umožnila	umožnit	k5eAaPmAgFnS	umožnit
mapovat	mapovat	k5eAaImF	mapovat
a	a	k8xC	a
sledovat	sledovat	k5eAaImF	sledovat
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
celosvětově	celosvětově	k6eAd1	celosvětově
a	a	k8xC	a
objevit	objevit	k5eAaPmF	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
je	být	k5eAaImIp3nS	být
lokalizována	lokalizován	k2eAgFnSc1d1	lokalizována
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
driftu	drift	k1gInSc2	drift
pomohla	pomoct	k5eAaPmAgFnS	pomoct
biogeografům	biogeograf	k1gMnPc3	biogeograf
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
dnešní	dnešní	k2eAgNnSc4d1	dnešní
biogeografické	biogeografický	k2eAgNnSc4d1	Biogeografické
rozšíření	rozšíření	k1gNnSc4	rozšíření
různých	různý	k2eAgFnPc2d1	různá
forem	forma	k1gFnPc2	forma
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
pozorovat	pozorovat	k5eAaImF	pozorovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mající	mající	k2eAgInPc1d1	mající
jasné	jasný	k2eAgInPc1d1	jasný
společné	společný	k2eAgInPc1d1	společný
předky	předek	k1gInPc1	předek
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnPc1d1	vnější
vrstvy	vrstva	k1gFnPc1	vrstva
Země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
litosféru	litosféra	k1gFnSc4	litosféra
a	a	k8xC	a
astenosféru	astenosféra	k1gFnSc4	astenosféra
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
rozdílech	rozdíl	k1gInPc6	rozdíl
v	v	k7c6	v
mechanických	mechanický	k2eAgFnPc6d1	mechanická
vlastnostech	vlastnost	k1gFnPc6	vlastnost
a	a	k8xC	a
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
dominantního	dominantní	k2eAgInSc2d1	dominantní
způsobu	způsob	k1gInSc2	způsob
přenosu	přenos	k1gInSc2	přenos
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Litosféra	litosféra	k1gFnSc1	litosféra
je	být	k5eAaImIp3nS	být
chladnější	chladný	k2eAgFnSc1d2	chladnější
a	a	k8xC	a
pevnější	pevný	k2eAgFnSc1d2	pevnější
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
astenosféra	astenosféra	k1gFnSc1	astenosféra
je	být	k5eAaImIp3nS	být
teplejší	teplý	k2eAgFnSc1d2	teplejší
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
snáze	snadno	k6eAd2	snadno
teče	téct	k5eAaImIp3nS	téct
(	(	kIx(	(
<g/>
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
duktilně	duktilně	k6eAd1	duktilně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
přenosu	přenos	k1gInSc2	přenos
tepla	teplo	k1gNnSc2	teplo
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgInSc4d1	dominantní
proces	proces	k1gInSc4	proces
u	u	k7c2	u
litosféry	litosféra	k1gFnSc2	litosféra
kondukce	kondukce	k1gFnSc2	kondukce
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
u	u	k7c2	u
astenosféry	astenosféra	k1gFnSc2	astenosféra
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
taktéž	taktéž	k?	taktéž
přenos	přenos	k1gInSc1	přenos
tepla	teplo	k1gNnSc2	teplo
konvekcí	konvekce	k1gFnPc2	konvekce
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
adiabatický	adiabatický	k2eAgInSc1d1	adiabatický
teplotní	teplotní	k2eAgInSc1d1	teplotní
gradient	gradient	k1gInSc1	gradient
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
členění	členění	k1gNnSc1	členění
ale	ale	k9	ale
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
chemickému	chemický	k2eAgNnSc3d1	chemické
členění	členění	k1gNnSc3	členění
vrstev	vrstva	k1gFnPc2	vrstva
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
(	(	kIx(	(
<g/>
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
jak	jak	k8xS	jak
astenosféru	astenosféra	k1gFnSc4	astenosféra
a	a	k8xC	a
část	část	k1gFnSc4	část
litosféry	litosféra	k1gFnSc2	litosféra
<g/>
)	)	kIx)	)
a	a	k8xC	a
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
pláště	plášť	k1gInSc2	plášť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
částí	část	k1gFnPc2	část
litosféry	litosféra	k1gFnSc2	litosféra
či	či	k8xC	či
astenosféry	astenosféra	k1gFnSc2	astenosféra
v	v	k7c6	v
rozdílných	rozdílný	k2eAgInPc6d1	rozdílný
časech	čas	k1gInPc6	čas
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc6	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Klíčový	klíčový	k2eAgInSc1d1	klíčový
princip	princip	k1gInSc1	princip
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
litosféra	litosféra	k1gFnSc1	litosféra
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
oddělených	oddělený	k2eAgInPc2d1	oddělený
a	a	k8xC	a
jasně	jasně	k6eAd1	jasně
rozlišitelných	rozlišitelný	k2eAgFnPc2d1	rozlišitelná
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
plavou	plavat	k5eAaImIp3nP	plavat
na	na	k7c6	na
viskoelastickém	viskoelastický	k2eAgInSc6d1	viskoelastický
podkladu	podklad	k1gInSc6	podklad
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
astenosféry	astenosféra	k1gFnSc2	astenosféra
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
pohybu	pohyb	k1gInSc2	pohyb
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
10	[number]	k4	10
až	až	k9	až
40	[number]	k4	40
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
na	na	k7c6	na
Středoatlantickém	Středoatlantický	k2eAgInSc6d1	Středoatlantický
hřbetě	hřbet	k1gInSc6	hřbet
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
rychlost	rychlost	k1gFnSc1	rychlost
růstu	růst	k1gInSc2	růst
lidských	lidský	k2eAgInPc2d1	lidský
nehtů	nehet	k1gInPc2	nehet
<g/>
)	)	kIx)	)
až	až	k9	až
k	k	k7c3	k
rychlosti	rychlost	k1gFnSc3	rychlost
160	[number]	k4	160
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
v	v	k7c6	v
případě	případ	k1gInSc6	případ
desky	deska	k1gFnSc2	deska
Nazca	Nazc	k1gInSc2	Nazc
(	(	kIx(	(
<g/>
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
přibližně	přibližně	k6eAd1	přibližně
rychlosti	rychlost	k1gFnPc1	rychlost
růstů	růst	k1gInPc2	růst
vlasů	vlas	k1gInPc2	vlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tektonické	tektonický	k2eAgFnPc1d1	tektonická
desky	deska	k1gFnPc1	deska
tvořící	tvořící	k2eAgFnSc4d1	tvořící
litosféru	litosféra	k1gFnSc4	litosféra
jsou	být	k5eAaImIp3nP	být
dvojího	dvojí	k4xRgInSc2	dvojí
základního	základní	k2eAgInSc2d1	základní
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
tvořená	tvořený	k2eAgFnSc1d1	tvořená
hlavně	hlavně	k9	hlavně
křemíkem	křemík	k1gInSc7	křemík
a	a	k8xC	a
hořčíkem	hořčík	k1gInSc7	hořčík
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
pramení	pramenit	k5eAaImIp3nS	pramenit
starší	starý	k2eAgFnSc1d2	starší
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
sima	sima	k1gFnSc1	sima
<g/>
"	"	kIx"	"
a	a	k8xC	a
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
kůra	kůra	k1gFnSc1	kůra
tvořená	tvořený	k2eAgFnSc1d1	tvořená
hlavně	hlavně	k9	hlavně
křemíkem	křemík	k1gInSc7	křemík
a	a	k8xC	a
hliníkem	hliník	k1gInSc7	hliník
<g/>
,	,	kIx,	,
starší	starší	k1gMnSc1	starší
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
sial	sial	k1gInSc1	sial
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
tloušťka	tloušťka	k1gFnSc1	tloušťka
litosféry	litosféra	k1gFnSc2	litosféra
pod	pod	k7c7	pod
oceánem	oceán	k1gInSc7	oceán
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
100	[number]	k4	100
km	km	kA	km
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
tloušťka	tloušťka	k1gFnSc1	tloušťka
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	s	k7c7	s
stářím	stáří	k1gNnSc7	stáří
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyQgNnSc7	čí
je	být	k5eAaImIp3nS	být
kůra	kůra	k1gFnSc1	kůra
starší	starý	k2eAgFnSc1d2	starší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
ztratila	ztratit	k5eAaPmAgNnP	ztratit
tepla	teplo	k1gNnPc1	teplo
kondukcí	kondukce	k1gFnPc2	kondukce
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
ztluštění	ztluštění	k1gNnSc3	ztluštění
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
středooceánských	středooceánský	k2eAgInPc6d1	středooceánský
hřbetech	hřbet	k1gInPc6	hřbet
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tloušťka	tloušťka	k1gFnSc1	tloušťka
závislá	závislý	k2eAgFnSc1d1	závislá
i	i	k9	i
na	na	k7c4	na
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
vzniku	vznik	k1gInSc2	vznik
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
je	být	k5eAaImIp3nS	být
litosféra	litosféra	k1gFnSc1	litosféra
pouze	pouze	k6eAd1	pouze
okolo	okolo	k7c2	okolo
6	[number]	k4	6
km	km	kA	km
mocná	mocný	k2eAgFnSc1d1	mocná
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
subdukce	subdukce	k1gFnSc2	subdukce
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
mocnosti	mocnost	k1gFnSc3	mocnost
až	až	k9	až
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Litosféra	litosféra	k1gFnSc1	litosféra
pod	pod	k7c7	pod
kontinenty	kontinent	k1gInPc7	kontinent
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
okolo	okolo	k7c2	okolo
200	[number]	k4	200
km	km	kA	km
mocná	mocný	k2eAgFnSc1d1	mocná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
různorodá	různorodý	k2eAgFnSc1d1	různorodá
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
pánvích	pánev	k1gFnPc6	pánev
<g/>
,	,	kIx,	,
pohořích	pohoří	k1gNnPc6	pohoří
<g/>
,	,	kIx,	,
stabilních	stabilní	k2eAgInPc6d1	stabilní
kratónech	kratón	k1gInPc6	kratón
atd.	atd.	kA	atd.
Mimo	mimo	k7c4	mimo
litosféry	litosféra	k1gFnPc4	litosféra
jsou	být	k5eAaImIp3nP	být
rozdíly	rozdíl	k1gInPc1	rozdíl
i	i	k9	i
mezi	mezi	k7c7	mezi
mocnostmi	mocnost	k1gFnPc7	mocnost
oceánské	oceánský	k2eAgFnSc2d1	oceánská
a	a	k8xC	a
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
pouze	pouze	k6eAd1	pouze
6	[number]	k4	6
km	km	kA	km
mocná	mocný	k2eAgFnSc1d1	mocná
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
okolo	okolo	k7c2	okolo
35	[number]	k4	35
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
desky	deska	k1gFnPc1	deska
setkávají	setkávat	k5eAaImIp3nP	setkávat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
deskové	deskový	k2eAgNnSc1d1	deskové
rozhraní	rozhraní	k1gNnSc1	rozhraní
<g/>
"	"	kIx"	"
a	a	k8xC	a
tyto	tyt	k2eAgNnSc1d1	tyto
místa	místo	k1gNnPc1	místo
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
geologických	geologický	k2eAgFnPc2d1	geologická
událostí	událost	k1gFnPc2	událost
jako	jako	k8xC	jako
například	například	k6eAd1	například
zemětřeseními	zemětřesení	k1gNnPc7	zemětřesení
a	a	k8xC	a
vznikem	vznik	k1gInSc7	vznik
topografických	topografický	k2eAgInPc2d1	topografický
útvarů	útvar	k1gInPc2	útvar
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
,	,	kIx,	,
sopky	sopka	k1gFnPc4	sopka
<g/>
,	,	kIx,	,
středooceánské	středooceánský	k2eAgInPc4d1	středooceánský
hřbety	hřbet	k1gInPc4	hřbet
a	a	k8xC	a
hlubokomořských	hlubokomořský	k2eAgInPc6d1	hlubokomořský
příkopy	příkop	k1gInPc1	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
aktivních	aktivní	k2eAgFnPc2d1	aktivní
sopek	sopka	k1gFnPc2	sopka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
deskových	deskový	k2eAgNnPc2d1	deskové
rozhraní	rozhraní	k1gNnPc2	rozhraní
okolo	okolo	k7c2	okolo
Pacifické	pacifický	k2eAgFnSc2d1	Pacifická
desky	deska	k1gFnSc2	deska
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
Ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
kruh	kruh	k1gInSc1	kruh
Tektonické	tektonický	k2eAgFnSc2d1	tektonická
desky	deska	k1gFnSc2	deska
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
skládat	skládat	k5eAaImF	skládat
z	z	k7c2	z
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
či	či	k8xC	či
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
desek	deska	k1gFnPc2	deska
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Africká	africký	k2eAgFnSc1d1	africká
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
kontinentální	kontinentální	k2eAgFnSc7d1	kontinentální
kůrou	kůra	k1gFnSc7	kůra
a	a	k8xC	a
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
části	část	k1gFnSc2	část
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
tvořící	tvořící	k2eAgInSc1d1	tvořící
Atlantický	atlantický	k2eAgInSc1d1	atlantický
a	a	k8xC	a
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
oceánskou	oceánský	k2eAgFnSc7d1	oceánská
a	a	k8xC	a
kontinentální	kontinentální	k2eAgFnSc7d1	kontinentální
kůrou	kůra	k1gFnSc7	kůra
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
kůra	kůra	k1gFnSc1	kůra
vzniká	vznikat	k5eAaImIp3nS	vznikat
vulkanismem	vulkanismus	k1gInSc7	vulkanismus
oblouků	oblouk	k1gInPc2	oblouk
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
akrecí	akrece	k1gFnSc7	akrece
teránů	terán	k1gMnPc2	terán
pomocí	pomocí	k7c2	pomocí
tektonických	tektonický	k2eAgInPc2d1	tektonický
pochodů	pochod	k1gInPc2	pochod
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
některé	některý	k3yIgInPc1	některý
tyto	tento	k3xDgInPc1	tento
terány	terán	k1gInPc1	terán
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
sekvence	sekvence	k1gFnPc4	sekvence
ofiolitů	ofiolit	k1gInPc2	ofiolit
<g/>
.	.	kIx.	.
</s>
<s>
Ofiolity	Ofiolit	k1gInPc1	Ofiolit
tvoří	tvořit	k5eAaImIp3nP	tvořit
oceánskou	oceánský	k2eAgFnSc4d1	oceánská
kůru	kůra	k1gFnSc4	kůra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
příležitostně	příležitostně	k6eAd1	příležitostně
mohou	moct	k5eAaImIp3nP	moct
opustit	opustit	k5eAaPmF	opustit
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
cyklus	cyklus	k1gInSc4	cyklus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zanikají	zanikat	k5eAaImIp3nP	zanikat
v	v	k7c6	v
subdukčních	subdukční	k2eAgFnPc6d1	subdukční
zónách	zóna	k1gFnPc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
hustší	hustý	k2eAgFnSc1d2	hustší
než	než	k8xS	než
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
kůra	kůra	k1gFnSc1	kůra
vlivem	vlivem	k7c2	vlivem
rozdílného	rozdílný	k2eAgNnSc2d1	rozdílné
složení	složení	k1gNnSc2	složení
<g/>
.	.	kIx.	.
</s>
<s>
Oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
hustší	hustý	k2eAgFnSc1d2	hustší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
menším	malý	k2eAgNnSc7d2	menší
zastoupením	zastoupení	k1gNnSc7	zastoupení
křemíku	křemík	k1gInSc2	křemík
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc7d2	vyšší
koncentrací	koncentrace	k1gFnSc7	koncentrace
mafických	mafický	k2eAgInPc2d1	mafický
prvků	prvek	k1gInPc2	prvek
vůči	vůči	k7c3	vůči
kontinentální	kontinentální	k2eAgFnSc3d1	kontinentální
kůře	kůra	k1gFnSc3	kůra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
větší	veliký	k2eAgNnSc4d2	veliký
zastoupení	zastoupení	k1gNnSc4	zastoupení
felsických	felsický	k2eAgInPc2d1	felsický
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
této	tento	k3xDgFnSc2	tento
hustotní	hustotní	k2eAgFnSc2d1	hustotní
rozdílnosti	rozdílnost	k1gFnSc2	rozdílnost
je	být	k5eAaImIp3nS	být
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
většinou	většinou	k6eAd1	většinou
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
kůra	kůra	k1gFnSc1	kůra
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
rozhraní	rozhraní	k1gNnSc2	rozhraní
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
charakterizovány	charakterizovat	k5eAaBmNgFnP	charakterizovat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
desky	deska	k1gFnPc1	deska
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
typem	typ	k1gInSc7	typ
povrchového	povrchový	k2eAgInSc2d1	povrchový
fenoménu	fenomén	k1gInSc2	fenomén
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
typy	typ	k1gInPc1	typ
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Transformní	Transformní	k2eAgNnSc1d1	Transformní
rozhraní	rozhraní	k1gNnSc1	rozhraní
nastává	nastávat	k5eAaImIp3nS	nastávat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
desky	deska	k1gFnPc1	deska
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
či	či	k8xC	či
prokluzují	prokluzovat	k5eAaImIp3nP	prokluzovat
podél	podél	k7c2	podél
transformního	transformní	k2eAgInSc2d1	transformní
zlomu	zlom	k1gInSc2	zlom
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
podél	podél	k7c2	podél
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgInSc1d1	relativní
pohyb	pohyb	k1gInSc1	pohyb
dvou	dva	k4xCgFnPc2	dva
pohybujících	pohybující	k2eAgFnPc2d1	pohybující
desek	deska	k1gFnPc2	deska
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
dextrální	dextrální	k2eAgInSc1d1	dextrální
(	(	kIx(	(
<g/>
pravostranný	pravostranný	k2eAgInSc1d1	pravostranný
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
)	)	kIx)	)
či	či	k8xC	či
sinistrální	sinistrální	k2eAgInSc1d1	sinistrální
(	(	kIx(	(
<g/>
levostranný	levostranný	k2eAgInSc1d1	levostranný
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
transformního	transformní	k2eAgInSc2d1	transformní
zlomu	zlom	k1gInSc2	zlom
je	být	k5eAaImIp3nS	být
San	San	k1gMnSc1	San
Andreas	Andreas	k1gMnSc1	Andreas
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dextrální	dextrální	k2eAgInSc1d1	dextrální
<g/>
.	.	kIx.	.
</s>
<s>
Divergentní	divergentní	k2eAgNnSc1d1	divergentní
rozhraní	rozhraní	k1gNnSc1	rozhraní
nastává	nastávat	k5eAaImIp3nS	nastávat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
desky	deska	k1gFnPc1	deska
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takovéhoto	takovýto	k3xDgNnSc2	takovýto
rozhraní	rozhraní	k1gNnSc2	rozhraní
jsou	být	k5eAaImIp3nP	být
středooceánské	středooceánský	k2eAgInPc1d1	středooceánský
hřbety	hřbet	k1gInPc1	hřbet
(	(	kIx(	(
<g/>
např.	např.	kA	např.
středoatlantický	středoatlantický	k2eAgInSc4d1	středoatlantický
hřbet	hřbet	k1gInSc4	hřbet
<g/>
)	)	kIx)	)
či	či	k8xC	či
aktivní	aktivní	k2eAgFnSc2d1	aktivní
zóny	zóna	k1gFnSc2	zóna
riftingu	rifting	k1gInSc2	rifting
jako	jako	k8xC	jako
například	například	k6eAd1	například
Velké	velký	k2eAgFnSc6d1	velká
riftové	riftová	k1gFnSc6	riftová
údolí	údolí	k1gNnSc2	údolí
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Konvergentní	konvergentní	k2eAgNnSc1d1	konvergentní
rozhraní	rozhraní	k1gNnSc1	rozhraní
(	(	kIx(	(
<g/>
či	či	k8xC	či
aktivní	aktivní	k2eAgInPc1d1	aktivní
okraje	okraj	k1gInPc1	okraj
<g/>
)	)	kIx)	)
nastává	nastávat	k5eAaImIp3nS	nastávat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
desky	deska	k1gFnPc1	deska
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
buď	buď	k8xC	buď
subdukční	subdukční	k2eAgFnSc1d1	subdukční
zóna	zóna	k1gFnSc1	zóna
(	(	kIx(	(
<g/>
když	když	k8xS	když
se	se	k3xPyFc4	se
jedna	jeden	k4xCgFnSc1	jeden
deska	deska	k1gFnSc1	deska
zasunuje	zasunovat	k5eAaImIp3nS	zasunovat
pod	pod	k7c4	pod
druhou	druhý	k4xOgFnSc4	druhý
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
kolize	kolize	k1gFnSc2	kolize
(	(	kIx(	(
<g/>
když	když	k8xS	když
se	se	k3xPyFc4	se
srazí	srazit	k5eAaPmIp3nP	srazit
dvě	dva	k4xCgFnPc1	dva
kontinentální	kontinentální	k2eAgFnPc1d1	kontinentální
desky	deska	k1gFnPc1	deska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hluboké	hluboký	k2eAgInPc1d1	hluboký
hlubokomořské	hlubokomořský	k2eAgInPc1d1	hlubokomořský
příkopy	příkop	k1gInPc1	příkop
jsou	být	k5eAaImIp3nP	být
typickým	typický	k2eAgInSc7d1	typický
doprovodným	doprovodný	k2eAgInSc7d1	doprovodný
jevem	jev	k1gInSc7	jev
subdukčních	subdukční	k2eAgFnPc2d1	subdukční
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Subdukující	Subdukující	k2eAgFnSc1d1	Subdukující
se	se	k3xPyFc4	se
deska	deska	k1gFnSc1	deska
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vodou	voda	k1gFnSc7	voda
bohatých	bohatý	k2eAgInPc2d1	bohatý
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
zahřátí	zahřátí	k1gNnSc6	zahřátí
desky	deska	k1gFnSc2	deska
začne	začít	k5eAaPmIp3nS	začít
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobí	způsobit	k5eAaPmIp3nS	způsobit
pokles	pokles	k1gInSc4	pokles
teploty	teplota	k1gFnSc2	teplota
solidu	solid	k1gInSc2	solid
a	a	k8xC	a
následnému	následný	k2eAgNnSc3d1	následné
tavení	tavení	k1gNnSc3	tavení
subdukující	subdukující	k2eAgFnSc2d1	subdukující
desky	deska	k1gFnSc2	deska
ústící	ústící	k2eAgFnSc2d1	ústící
ve	v	k7c4	v
vulkanickou	vulkanický	k2eAgFnSc4d1	vulkanická
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
subdukční	subdukční	k2eAgFnSc2d1	subdukční
zóny	zóna	k1gFnSc2	zóna
je	být	k5eAaImIp3nS	být
pásemné	pásemný	k2eAgNnSc4d1	pásemné
pohoří	pohoří	k1gNnSc4	pohoří
Andy	Anda	k1gFnSc2	Anda
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Plášťová	plášťový	k2eAgFnSc1d1	plášťová
konvekce	konvekce	k1gFnSc1	konvekce
<g/>
.	.	kIx.	.
</s>
<s>
Tektonické	tektonický	k2eAgFnPc1d1	tektonická
desky	deska	k1gFnPc1	deska
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
vlivem	vliv	k1gInSc7	vliv
relativní	relativní	k2eAgFnSc2d1	relativní
hustoty	hustota	k1gFnSc2	hustota
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
relativní	relativní	k2eAgFnSc2d1	relativní
slabosti	slabost	k1gFnSc2	slabost
astenosféry	astenosféra	k1gFnSc2	astenosféra
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
řídící	řídící	k2eAgFnSc4d1	řídící
deskovou	deskový	k2eAgFnSc4d1	desková
tektoniku	tektonika	k1gFnSc4	tektonika
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
transport	transport	k1gInSc1	transport
tepla	teplo	k1gNnSc2	teplo
z	z	k7c2	z
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
pohled	pohled	k1gInSc1	pohled
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
předmětem	předmět	k1gInSc7	předmět
stále	stále	k6eAd1	stále
probíhajících	probíhající	k2eAgFnPc2d1	probíhající
debat	debata	k1gFnPc2	debata
<g/>
,	,	kIx,	,
že	že	k8xS	že
zanořování	zanořování	k1gNnSc1	zanořování
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
v	v	k7c6	v
subdukčních	subdukční	k2eAgFnPc6d1	subdukční
zónách	zóna	k1gFnPc6	zóna
vlivem	vlivem	k7c2	vlivem
narůstající	narůstající	k2eAgFnSc2d1	narůstající
hustoty	hustota	k1gFnSc2	hustota
subdukující	subdukující	k2eAgFnSc2d1	subdukující
desky	deska	k1gFnSc2	deska
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
pohybu	pohyb	k1gInSc2	pohyb
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
středooceánském	středooceánský	k2eAgInSc6d1	středooceánský
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
méně	málo	k6eAd2	málo
hustá	hustý	k2eAgFnSc1d1	hustá
než	než	k8xS	než
podložní	podložní	k2eAgFnSc1d1	podložní
vrstva	vrstva	k1gFnSc1	vrstva
astenosféry	astenosféra	k1gFnSc2	astenosféra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
narůstajícím	narůstající	k2eAgInSc7d1	narůstající
věkem	věk	k1gInSc7	věk
kůry	kůra	k1gFnSc2	kůra
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
hustoty	hustota	k1gFnSc2	hustota
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dochází	docházet	k5eAaImIp3nS	docházet
postupně	postupně	k6eAd1	postupně
kondukcí	kondukce	k1gFnSc7	kondukce
ke	k	k7c3	k
chladnutí	chladnutí	k1gNnSc3	chladnutí
desky	deska	k1gFnSc2	deska
a	a	k8xC	a
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
ztlušťování	ztlušťování	k1gNnSc3	ztlušťování
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
hustota	hustota	k1gFnSc1	hustota
staré	starý	k2eAgFnSc2d1	stará
kůry	kůra	k1gFnSc2	kůra
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
astenosféra	astenosféra	k1gFnSc1	astenosféra
časem	časem	k6eAd1	časem
umožní	umožnit	k5eAaPmIp3nS	umožnit
ponoření	ponoření	k1gNnSc4	ponoření
staré	starý	k2eAgFnSc2d1	stará
kůry	kůra	k1gFnSc2	kůra
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
pláště	plášť	k1gInSc2	plášť
v	v	k7c6	v
subdukčních	subdukční	k2eAgFnPc6d1	subdukční
zónách	zóna	k1gFnPc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Slabost	slabost	k1gFnSc1	slabost
astenosféry	astenosféra	k1gFnSc2	astenosféra
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tektonické	tektonický	k2eAgFnSc3d1	tektonická
desce	deska	k1gFnSc3	deska
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
pohybovat	pohybovat	k5eAaImF	pohybovat
k	k	k7c3	k
subdukční	subdukční	k2eAgFnSc3d1	subdukční
zóně	zóna	k1gFnSc3	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc7d1	hlavní
silou	síla	k1gFnSc7	síla
řídící	řídící	k2eAgInSc1d1	řídící
pohyb	pohyb	k1gInSc1	pohyb
desek	deska	k1gFnPc2	deska
je	být	k5eAaImIp3nS	být
subdukce	subdukce	k1gFnSc1	subdukce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
brát	brát	k5eAaImF	brát
na	na	k7c4	na
zřetel	zřetel	k1gInSc4	zřetel
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pohyb	pohyb	k1gInSc4	pohyb
desek	deska	k1gFnPc2	deska
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
a	a	k8xC	a
například	například	k6eAd1	například
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
deska	deska	k1gFnSc1	deska
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
někde	někde	k6eAd1	někde
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
subdukci	subdukce	k1gFnSc3	subdukce
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
Euroasijská	euroasijský	k2eAgFnSc1d1	euroasijská
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Pochopení	pochopení	k1gNnSc1	pochopení
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gFnSc1	vedoucí
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
stále	stále	k6eAd1	stále
předmětem	předmět	k1gInSc7	předmět
intenzivních	intenzivní	k2eAgInPc2d1	intenzivní
výzkumů	výzkum	k1gInPc2	výzkum
mezi	mezi	k7c7	mezi
geology	geolog	k1gMnPc7	geolog
<g/>
.	.	kIx.	.
</s>
<s>
Dvou	dva	k4xCgFnPc2	dva
a	a	k8xC	a
tří	tři	k4xCgFnPc2	tři
dimensní	dimensnit	k5eAaPmIp3nP	dimensnit
snímkování	snímkování	k1gNnSc2	snímkování
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
stavby	stavba	k1gFnSc2	stavba
Země	zem	k1gFnSc2	zem
pomocí	pomocí	k7c2	pomocí
seismické	seismický	k2eAgFnSc2d1	seismická
tomografie	tomografie	k1gFnSc2	tomografie
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
existují	existovat	k5eAaImIp3nP	existovat
sestupně	sestupně	k6eAd1	sestupně
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
hustota	hustota	k1gFnSc1	hustota
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rozdílnosti	rozdílnost	k1gFnPc1	rozdílnost
v	v	k7c6	v
hustotě	hustota	k1gFnSc6	hustota
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
chemismem	chemismus	k1gInSc7	chemismus
<g/>
,	,	kIx,	,
mineralogickým	mineralogický	k2eAgNnSc7d1	mineralogické
složením	složení	k1gNnSc7	složení
nebo	nebo	k8xC	nebo
teplotně	teplotně	k6eAd1	teplotně
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
teplotní	teplotní	k2eAgFnSc2d1	teplotní
roztažnosti	roztažnost	k1gFnSc2	roztažnost
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Projevem	projev	k1gInSc7	projev
této	tento	k3xDgFnSc2	tento
členitosti	členitost	k1gFnSc2	členitost
v	v	k7c6	v
hustotách	hustota	k1gFnPc6	hustota
je	být	k5eAaImIp3nS	být
plášťová	plášťový	k2eAgFnSc1d1	plášťová
konvekce	konvekce	k1gFnSc1	konvekce
způsobená	způsobený	k2eAgFnSc1d1	způsobená
rozdílností	rozdílnost	k1gFnSc7	rozdílnost
v	v	k7c6	v
hustotách	hustota	k1gFnPc6	hustota
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
vztlakem	vztlak	k1gInSc7	vztlak
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
plášťová	plášťový	k2eAgFnSc1d1	plášťová
konvekce	konvekce	k1gFnSc1	konvekce
přímo	přímo	k6eAd1	přímo
i	i	k8xC	i
nepřímo	přímo	k6eNd1	přímo
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
pohybu	pohyb	k1gInSc3	pohyb
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
debat	debata	k1gFnPc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
síly	síla	k1gFnPc1	síla
působící	působící	k2eAgFnPc1d1	působící
na	na	k7c4	na
pohybující	pohybující	k2eAgInPc4d1	pohybující
se	se	k3xPyFc4	se
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gFnPc7	on
třeníí	třeníí	k1gFnSc1	třeníí
a	a	k8xC	a
gravitace	gravitace	k1gFnSc1	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Plášťový	plášťový	k2eAgInSc1d1	plášťový
chochol	chochol	k1gInSc1	chochol
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
v	v	k7c6	v
deskové	deskový	k2eAgFnSc6d1	desková
tektonice	tektonika	k1gFnSc6	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Bazální	bazální	k2eAgNnPc1d1	bazální
tření	tření	k1gNnPc1	tření
obrovské	obrovský	k2eAgInPc4d1	obrovský
konvektivní	konvektivní	k2eAgInSc4d1	konvektivní
proudy	proud	k1gInPc4	proud
ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
plášti	plášť	k1gInSc6	plášť
jsou	být	k5eAaImIp3nP	být
přenášeny	přenášet	k5eAaImNgInP	přenášet
přes	přes	k7c4	přes
astenosféru	astenosféra	k1gFnSc4	astenosféra
<g/>
;	;	kIx,	;
pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
řízen	řídit	k5eAaImNgInS	řídit
třením	tření	k1gNnSc7	tření
mezi	mezi	k7c7	mezi
astenosférou	astenosféra	k1gFnSc7	astenosféra
a	a	k8xC	a
litosférou	litosféra	k1gFnSc7	litosféra
Nasávání	nasávání	k1gNnSc2	nasávání
desky	deska	k1gFnSc2	deska
místní	místní	k2eAgNnSc1d1	místní
konvektivní	konvektivní	k2eAgInPc1d1	konvektivní
proudy	proud	k1gInPc1	proud
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
ponořování	ponořování	k1gNnSc4	ponořování
desky	deska	k1gFnSc2	deska
do	do	k7c2	do
větší	veliký	k2eAgFnSc2d2	veliký
hloubky	hloubka	k1gFnSc2	hloubka
vlivem	vlivem	k7c2	vlivem
sestupných	sestupný	k2eAgInPc2d1	sestupný
proudů	proud	k1gInPc2	proud
v	v	k7c6	v
konvektivní	konvektivní	k2eAgFnSc6d1	konvektivní
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
geodynamických	geodynamický	k2eAgInPc6d1	geodynamický
prostředích	prostředí	k1gNnPc6	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
basální	basální	k2eAgFnSc1d1	basální
trakce	trakce	k1gFnSc1	trakce
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
působení	působení	k1gNnSc6	působení
na	na	k7c4	na
desku	deska	k1gFnSc4	deska
ponořující	ponořující	k2eAgFnSc2d1	ponořující
se	se	k3xPyFc4	se
do	do	k7c2	do
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgInSc1d1	gravitační
skluz	skluz	k1gInSc1	skluz
<g/>
/	/	kIx~	/
<g/>
sliding	sliding	k1gInSc1	sliding
<g/>
:	:	kIx,	:
pohyb	pohyb	k1gInSc1	pohyb
oceánské	oceánský	k2eAgFnSc2d1	oceánská
litosféry	litosféra	k1gFnSc2	litosféra
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
vyšší	vysoký	k2eAgFnSc7d2	vyšší
topografickou	topografický	k2eAgFnSc7d1	topografická
výško	výška	k1gFnSc5	výška
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
středooceánských	středooceánský	k2eAgInPc2d1	středooceánský
hřbetů	hřbet	k1gInPc2	hřbet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
litosféry	litosféra	k1gFnSc2	litosféra
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
oceánská	oceánský	k2eAgFnSc1d1	oceánská
litosféra	litosféra	k1gFnSc1	litosféra
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
pozvolna	pozvolna	k6eAd1	pozvolna
chladnout	chladnout	k5eAaImF	chladnout
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
více	hodně	k6eAd2	hodně
vychladne	vychladnout	k5eAaPmIp3nS	vychladnout
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
hustší	hustý	k2eAgInSc1d2	hustší
a	a	k8xC	a
mocnější	mocný	k2eAgInSc1d2	mocnější
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
oceánská	oceánský	k2eAgFnSc1d1	oceánská
litosféra	litosféra	k1gFnSc1	litosféra
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
hustší	hustý	k2eAgInSc1d2	hustší
než	než	k8xS	než
teplý	teplý	k2eAgInSc1d1	teplý
plášťový	plášťový	k2eAgInSc1d1	plášťový
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
mocnosti	mocnost	k1gFnSc2	mocnost
a	a	k8xC	a
hustoty	hustota	k1gFnSc2	hustota
se	se	k3xPyFc4	se
tak	tak	k9	tak
pomalu	pomalu	k6eAd1	pomalu
začne	začít	k5eAaPmIp3nS	začít
zanořovat	zanořovat	k5eAaImF	zanořovat
do	do	k7c2	do
pláště	plášť	k1gInSc2	plášť
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
vyšší	vysoký	k2eAgNnSc4d2	vyšší
zatížení	zatížení	k1gNnSc4	zatížení
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
mírný	mírný	k2eAgInSc4d1	mírný
boční	boční	k2eAgInSc4d1	boční
skluz	skluz	k1gInSc4	skluz
oceánské	oceánský	k2eAgFnSc2d1	oceánská
litosféry	litosféra	k1gFnSc2	litosféra
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
středooceánského	středooceánský	k2eAgInSc2d1	středooceánský
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
geofyzikální	geofyzikální	k2eAgFnSc6d1	geofyzikální
komunitě	komunita	k1gFnSc6	komunita
a	a	k8xC	a
v	v	k7c6	v
geologické	geologický	k2eAgFnSc6d1	geologická
literatuře	literatura	k1gFnSc6	literatura
nazývá	nazývat	k5eAaImIp3nS	nazývat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
ridge-push	ridgeush	k1gMnSc1	ridge-push
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
tlačena	tlačit	k5eAaImNgFnS	tlačit
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
vzniku	vznik	k1gInSc2	vznik
dalšími	další	k2eAgInPc7d1	další
výlevy	výlev	k1gInPc7	výlev
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
přesné	přesný	k2eAgNnSc1d1	přesné
je	být	k5eAaImIp3nS	být
však	však	k9	však
označovat	označovat	k5eAaImF	označovat
tuto	tento	k3xDgFnSc4	tento
sílu	síla	k1gFnSc4	síla
jako	jako	k8xC	jako
gravitační	gravitační	k2eAgInSc4d1	gravitační
skluz	skluz	k1gInSc4	skluz
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
topografie	topografie	k1gFnSc1	topografie
desky	deska	k1gFnSc2	deska
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různá	různý	k2eAgFnSc1d1	různá
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
desky	deska	k1gFnSc2	deska
a	a	k8xC	a
ne	ne	k9	ne
všude	všude	k6eAd1	všude
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
deska	deska	k1gFnSc1	deska
odtlačována	odtlačován	k2eAgFnSc1d1	odtlačován
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc1	příklad
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odtlačování	odtlačování	k1gNnSc1	odtlačování
může	moct	k5eAaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Flexurální	Flexurální	k2eAgNnSc1d1	Flexurální
vyklenutí	vyklenutí	k1gNnSc1	vyklenutí
litosféry	litosféra	k1gFnSc2	litosféra
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
deska	deska	k1gFnSc1	deska
zanoří	zanořit	k5eAaPmIp3nS	zanořit
jako	jako	k9	jako
důsledek	důsledek	k1gInSc1	důsledek
ohnutí	ohnutí	k1gNnSc2	ohnutí
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
procesem	proces	k1gInSc7	proces
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
jasné	jasný	k2eAgFnPc4d1	jasná
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
topografické	topografický	k2eAgFnPc4d1	topografická
anomálie	anomálie	k1gFnPc4	anomálie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
může	moct	k5eAaImIp3nS	moct
různě	různě	k6eAd1	různě
výrazně	výrazně	k6eAd1	výrazně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
celkovou	celkový	k2eAgFnSc4d1	celková
topografii	topografie	k1gFnSc4	topografie
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Plášťové	plášťový	k2eAgInPc1d1	plášťový
chocholy	chochol	k1gInPc1	chochol
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
taktéž	taktéž	k?	taktéž
výrazně	výrazně	k6eAd1	výrazně
změnit	změnit	k5eAaPmF	změnit
topografii	topografie	k1gFnSc4	topografie
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Táhnutí	táhnutí	k1gNnSc1	táhnutí
desky	deska	k1gFnSc2	deska
(	(	kIx(	(
<g/>
či	či	k8xC	či
často	často	k6eAd1	často
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Slab	slabit	k5eAaImRp2nS	slabit
pull	pull	k1gInSc1	pull
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pohyb	pohyb	k1gInSc4	pohyb
desky	deska	k1gFnSc2	deska
způsobovaný	způsobovaný	k2eAgInSc4d1	způsobovaný
váhou	váha	k1gFnSc7	váha
ponořující	ponořující	k2eAgFnSc2d1	ponořující
se	se	k3xPyFc4	se
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stahuje	stahovat	k5eAaImIp3nS	stahovat
sebou	se	k3xPyFc7	se
celou	celý	k2eAgFnSc4d1	celá
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
subdukující	subdukující	k2eAgFnSc1d1	subdukující
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgInSc4d2	těžší
než	než	k8xS	než
okolní	okolní	k2eAgInSc4d1	okolní
plášťový	plášťový	k2eAgInSc4d1	plášťový
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
působení	působení	k1gNnSc4	působení
vztlakové	vztlakový	k2eAgFnSc2d1	vztlaková
síly	síla	k1gFnSc2	síla
působícího	působící	k2eAgInSc2d1	působící
okolního	okolní	k2eAgInSc2d1	okolní
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
probíhá	probíhat	k5eAaImIp3nS	probíhat
konvektivní	konvektivní	k2eAgNnSc1d1	konvektivní
proudění	proudění	k1gNnSc1	proudění
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
materiálu	materiál	k1gInSc2	materiál
na	na	k7c6	na
středooceánských	středooceánský	k2eAgInPc6d1	středooceánský
hřbetech	hřbet	k1gInPc6	hřbet
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
jasně	jasně	k6eAd1	jasně
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
právě	právě	k9	právě
tímto	tento	k3xDgInSc7	tento
pohybem	pohyb	k1gInSc7	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
dřívější	dřívější	k2eAgInPc1d1	dřívější
modely	model	k1gInPc1	model
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
předpokládaly	předpokládat	k5eAaImAgFnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
litosféra	litosféra	k1gFnSc1	litosféra
klouže	klouzat	k5eAaImIp3nS	klouzat
na	na	k7c6	na
vrcholcích	vrcholek	k1gInPc6	vrcholek
těchto	tento	k3xDgFnPc2	tento
konvektivních	konvektivní	k2eAgFnPc2d1	konvektivní
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
dnes	dnes	k6eAd1	dnes
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
astenosféra	astenosféra	k1gFnSc1	astenosféra
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
silná	silný	k2eAgFnSc1d1	silná
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přímo	přímo	k6eAd1	přímo
způsobila	způsobit	k5eAaPmAgFnS	způsobit
pohyb	pohyb	k1gInSc4	pohyb
kvůli	kvůli	k7c3	kvůli
tření	tření	k1gNnSc3	tření
na	na	k7c6	na
bazálních	bazální	k2eAgFnPc6d1	bazální
plochách	plocha	k1gFnPc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
táhnutí	táhnutí	k1gNnSc1	táhnutí
desky	deska	k1gFnSc2	deska
(	(	kIx(	(
<g/>
slab	slabit	k5eAaImRp2nS	slabit
pull	pull	k1gInSc1	pull
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
deska	deska	k1gFnSc1	deska
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInPc1d1	dnešní
modely	model	k1gInPc1	model
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgFnSc4d1	další
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
hraje	hrát	k5eAaImIp3nS	hrát
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
trench	trench	k1gInSc1	trench
suction	suction	k1gInSc1	suction
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
nasávání	nasávání	k1gNnSc1	nasávání
desky	deska	k1gFnSc2	deska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
nad	nad	k7c7	nad
subdukující	subdukující	k2eAgFnSc7d1	subdukující
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
deska	deska	k1gFnSc1	deska
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
i	i	k9	i
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
někde	někde	k6eAd1	někde
byla	být	k5eAaImAgFnS	být
subdukována	subdukován	k2eAgFnSc1d1	subdukován
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
Africká	africký	k2eAgFnSc1d1	africká
<g/>
,	,	kIx,	,
Euroasijská	euroasijský	k2eAgFnSc1d1	euroasijská
a	a	k8xC	a
Antarktická	antarktický	k2eAgFnSc1d1	antarktická
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
při	při	k7c6	při
proudění	proudění	k1gNnSc6	proudění
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
atmosféry	atmosféra	k1gFnSc2	atmosféra
hraje	hrát	k5eAaImIp3nS	hrát
určitou	určitý	k2eAgFnSc4d1	určitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
vliv	vliv	k1gInSc4	vliv
ostatních	ostatní	k2eAgNnPc2d1	ostatní
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k6eAd1	hlavně
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
brzdit	brzdit	k5eAaImF	brzdit
pohyby	pohyb	k1gInPc4	pohyb
Země	zem	k1gFnSc2	zem
z	z	k7c2	z
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
můžou	můžou	k?	můžou
podporovat	podporovat	k5eAaImF	podporovat
složku	složka	k1gFnSc4	složka
pohybu	pohyb	k1gInSc2	pohyb
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
směřuje	směřovat	k5eAaImIp3nS	směřovat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
publikované	publikovaný	k2eAgFnSc2d1	publikovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
skupina	skupina	k1gFnSc1	skupina
italských	italský	k2eAgMnPc2d1	italský
a	a	k8xC	a
amerických	americký	k2eAgMnPc2d1	americký
vědců	vědec	k1gMnPc2	vědec
argumentovala	argumentovat	k5eAaImAgFnS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
západní	západní	k2eAgFnSc1d1	západní
komponenta	komponenta	k1gFnSc1	komponenta
pohybu	pohyb	k1gInSc2	pohyb
každé	každý	k3xTgFnSc2	každý
desky	deska	k1gFnSc2	deska
je	být	k5eAaImIp3nS	být
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
zemskou	zemský	k2eAgFnSc7d1	zemská
rotací	rotace	k1gFnSc7	rotace
a	a	k8xC	a
působením	působení	k1gNnSc7	působení
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Podstata	podstata	k1gFnSc1	podstata
práce	práce	k1gFnSc2	práce
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
točí	točit	k5eAaImIp3nS	točit
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
Měsíc	měsíc	k1gInSc1	měsíc
proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
pohybu	pohyb	k1gInSc3	pohyb
a	a	k8xC	a
svojí	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
táhne	táhnout	k5eAaImIp3nS	táhnout
částečně	částečně	k6eAd1	částečně
desky	deska	k1gFnPc1	deska
k	k	k7c3	k
západu	západ	k1gInSc3	západ
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
svým	svůj	k3xOyFgInSc7	svůj
závěrem	závěr	k1gInSc7	závěr
i	i	k9	i
neexistenci	neexistence	k1gFnSc3	neexistence
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
a	a	k8xC	a
Venuši	Venuše	k1gFnSc6	Venuše
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obecně	obecně	k6eAd1	obecně
tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
nebyla	být	k5eNaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Coriolisova	Coriolisův	k2eAgFnSc1d1	Coriolisova
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
všechny	všechen	k3xTgFnPc4	všechen
horizontální	horizontální	k2eAgNnSc4d1	horizontální
proudění	proudění	k1gNnSc4	proudění
stáčet	stáčet	k5eAaImF	stáčet
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
úbytek	úbytek	k1gInSc4	úbytek
hmoty	hmota	k1gFnSc2	hmota
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1	aktuální
vektor	vektor	k1gInSc1	vektor
deskových	deskový	k2eAgInPc2d1	deskový
pohybů	pohyb	k1gInPc2	pohyb
musí	muset	k5eAaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
být	být	k5eAaImF	být
funkcí	funkce	k1gFnSc7	funkce
všech	všecek	k3xTgFnPc2	všecek
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
na	na	k7c4	na
desku	deska	k1gFnSc4	deska
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
problém	problém	k1gInSc4	problém
rozeznat	rozeznat	k5eAaPmF	rozeznat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
přispěl	přispět	k5eAaPmAgInS	přispět
každý	každý	k3xTgInSc1	každý
člen	člen	k1gInSc1	člen
pohybů	pohyb	k1gInPc2	pohyb
k	k	k7c3	k
výslednému	výsledný	k2eAgInSc3d1	výsledný
pohybu	pohyb	k1gInSc3	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílnost	rozdílnost	k1gFnSc1	rozdílnost
mezi	mezi	k7c7	mezi
deskami	deska	k1gFnPc7	deska
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
složení	složení	k1gNnSc2	složení
<g/>
,	,	kIx,	,
rozhraní	rozhraní	k1gNnSc2	rozhraní
či	či	k8xC	či
topografie	topografie	k1gFnSc2	topografie
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
projevovat	projevovat	k5eAaImF	projevovat
i	i	k9	i
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
a	a	k8xC	a
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
subdukující	subdukující	k2eAgFnSc1d1	subdukující
se	se	k3xPyFc4	se
deska	deska	k1gFnSc1	deska
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
značně	značně	k6eAd1	značně
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nesubdukuje	subdukovat	k5eNaPmIp3nS	subdukovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
Pacifickou	pacifický	k2eAgFnSc4d1	Pacifická
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
subdukčními	subdukční	k2eAgFnPc7d1	subdukční
zónami	zóna	k1gFnPc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
subdukční	subdukční	k2eAgFnPc1d1	subdukční
zóny	zóna	k1gFnPc1	zóna
nenachází	nacházet	k5eNaImIp3nP	nacházet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
značně	značně	k6eAd1	značně
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
se	s	k7c7	s
silou	síla	k1gFnSc7	síla
ponořující	ponořující	k2eAgFnSc2d1	ponořující
se	se	k3xPyFc4	se
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
stahovat	stahovat	k5eAaImF	stahovat
desku	deska	k1gFnSc4	deska
rychleji	rychle	k6eAd2	rychle
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
a	a	k8xC	a
tak	tak	k6eAd1	tak
přispívat	přispívat	k5eAaImF	přispívat
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
celkovému	celkový	k2eAgInSc3d1	celkový
pohybu	pohyb	k1gInSc3	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
řídící	řídící	k2eAgFnPc4d1	řídící
síly	síla	k1gFnPc4	síla
deskových	deskový	k2eAgInPc2d1	deskový
pohybů	pohyb	k1gInPc2	pohyb
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
předmětem	předmět	k1gInSc7	předmět
debat	debata	k1gFnPc2	debata
a	a	k8xC	a
výzkumů	výzkum	k1gInPc2	výzkum
geofyzikální	geofyzikální	k2eAgFnSc2d1	geofyzikální
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
důsledky	důsledek	k1gInPc4	důsledek
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
velmi	velmi	k6eAd1	velmi
jednoduše	jednoduše	k6eAd1	jednoduše
–	–	k?	–
stačí	stačit	k5eAaBmIp3nS	stačit
přesné	přesný	k2eAgNnSc4d1	přesné
zařízení	zařízení	k1gNnSc4	zařízení
GPS	GPS	kA	GPS
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
pozičního	poziční	k2eAgInSc2d1	poziční
systému	systém	k1gInSc2	systém
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přímo	přímo	k6eAd1	přímo
měřeny	měřen	k2eAgFnPc4d1	měřena
roční	roční	k2eAgFnPc4d1	roční
rychlosti	rychlost	k1gFnPc4	rychlost
vzdalování	vzdalování	k1gNnSc2	vzdalování
se	se	k3xPyFc4	se
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
příležitost	příležitost	k1gFnSc1	příležitost
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
představují	představovat	k5eAaImIp3nP	představovat
tzv.	tzv.	kA	tzv.
rifty	rift	k1gInPc1	rift
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
zřejmě	zřejmě	k6eAd1	zřejmě
nový	nový	k2eAgInSc1d1	nový
oceán	oceán	k1gInSc1	oceán
a	a	k8xC	a
Somálský	somálský	k2eAgInSc1d1	somálský
poloostrov	poloostrov	k1gInSc1	poloostrov
s	s	k7c7	s
územím	území	k1gNnSc7	území
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
oddělí	oddělit	k5eAaPmIp3nP	oddělit
od	od	k7c2	od
celého	celý	k2eAgInSc2d1	celý
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
je	být	k5eAaImIp3nS	být
také	také	k9	také
hlavním	hlavní	k2eAgMnSc7d1	hlavní
původcem	původce	k1gMnSc7	původce
velkoškálových	velkoškálový	k2eAgInPc2d1	velkoškálový
tektonických	tektonický	k2eAgInPc2d1	tektonický
jevů	jev	k1gInPc2	jev
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
<g/>
,	,	kIx,	,
sopečná	sopečný	k2eAgFnSc1d1	sopečná
činnost	činnost	k1gFnSc1	činnost
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
sopečných	sopečný	k2eAgInPc2d1	sopečný
nebo	nebo	k8xC	nebo
ostrovních	ostrovní	k2eAgInPc2d1	ostrovní
oblouků	oblouk	k1gInPc2	oblouk
nad	nad	k7c7	nad
subdukovanou	subdukovaný	k2eAgFnSc7d1	subdukovaný
deskou	deska	k1gFnSc7	deska
<g/>
,	,	kIx,	,
pásemných	pásemný	k2eAgNnPc2d1	pásemné
pohoří	pohoří	k1gNnPc2	pohoří
nebo	nebo	k8xC	nebo
řetězů	řetěz	k1gInPc2	řetěz
vyhaslých	vyhaslý	k2eAgFnPc2d1	vyhaslá
sopek	sopka	k1gFnPc2	sopka
odsunutých	odsunutý	k2eAgFnPc2d1	odsunutá
od	od	k7c2	od
horkých	horký	k2eAgFnPc2d1	horká
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
svými	svůj	k3xOyFgInPc7	svůj
důsledky	důsledek	k1gInPc7	důsledek
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožnil	umožnit	k5eAaPmAgInS	umožnit
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
vznik	vznik	k1gInSc4	vznik
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
faktorů	faktor	k1gMnPc2	faktor
habitability	habitabilita	k1gFnSc2	habitabilita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jsou	být	k5eAaImIp3nP	být
definovány	definován	k2eAgInPc4d1	definován
se	se	k3xPyFc4	se
vyčleňuje	vyčleňovat	k5eAaImIp3nS	vyčleňovat
nejčastěji	často	k6eAd3	často
sedm	sedm	k4xCc4	sedm
až	až	k9	až
osm	osm	k4xCc4	osm
hlavních	hlavní	k2eAgFnPc2d1	hlavní
desek	deska	k1gFnPc2	deska
<g/>
:	:	kIx,	:
Pacifická	pacifický	k2eAgFnSc1d1	Pacifická
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
Jihoamerická	jihoamerický	k2eAgFnSc1d1	jihoamerická
Euroasijská	euroasijský	k2eAgFnSc1d1	euroasijská
Africká	africký	k2eAgFnSc1d1	africká
Indo-australská	Indoustralský	k2eAgFnSc1d1	Indo-australský
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
rozdělená	rozdělená	k1gFnSc1	rozdělená
na	na	k7c4	na
<g/>
:	:	kIx,	:
Indická	indický	k2eAgFnSc1d1	indická
Australská	australský	k2eAgFnSc1d1	australská
Antarktická	antarktický	k2eAgFnSc1d1	antarktická
Existují	existovat	k5eAaImIp3nP	existovat
desítky	desítka	k1gFnPc1	desítka
malých	malý	k2eAgFnPc2d1	malá
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc1	sedm
největších	veliký	k2eAgMnPc2d3	veliký
je	být	k5eAaImIp3nS	být
vypsaných	vypsaný	k2eAgFnPc2d1	vypsaná
níže	nízce	k6eAd2	nízce
<g/>
:	:	kIx,	:
Nazca	Nazc	k2eAgFnSc1d1	Nazca
Kokosová	kokosový	k2eAgFnSc1d1	kokosová
Filipínská	filipínský	k2eAgFnSc1d1	filipínská
Arabská	arabský	k2eAgFnSc1d1	arabská
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
Fuca	Fuca	k1gFnSc1	Fuca
Karibská	karibský	k2eAgFnSc1d1	karibská
Scotia	Scotia	k1gFnSc1	Scotia
Pohyby	pohyb	k1gInPc1	pohyb
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
způsobovaly	způsobovat	k5eAaImAgInP	způsobovat
vzniky	vznik	k1gInPc1	vznik
a	a	k8xC	a
rozpady	rozpad	k1gInPc1	rozpad
kontinentů	kontinent	k1gInPc2	kontinent
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
občasného	občasný	k2eAgNnSc2d1	občasné
formování	formování	k1gNnSc2	formování
superkontinetu	superkontinet	k1gInSc2	superkontinet
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgMnS	skládat
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
tehdy	tehdy	k6eAd1	tehdy
existující	existující	k2eAgFnSc2d1	existující
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
superkontinent	superkontinent	k1gInSc4	superkontinent
Rodinie	Rodinie	k1gFnSc2	Rodinie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
1	[number]	k4	1
miliardou	miliarda	k4xCgFnSc7	miliarda
let	léto	k1gNnPc2	léto
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
většinu	většina	k1gFnSc4	většina
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
Rodinie	Rodinie	k1gFnSc2	Rodinie
došlo	dojít	k5eAaPmAgNnS	dojít
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
600	[number]	k4	600
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tak	tak	k9	tak
osm	osm	k4xCc1	osm
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
spojily	spojit	k5eAaPmAgFnP	spojit
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
superkontinentu	superkontinent	k1gInSc2	superkontinent
nazvaného	nazvaný	k2eAgInSc2d1	nazvaný
Pangea	Pangea	k1gFnSc1	Pangea
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
kontinenty	kontinent	k1gInPc4	kontinent
Laurasii	Laurasie	k1gFnSc6	Laurasie
(	(	kIx(	(
<g/>
skládající	skládající	k2eAgNnSc1d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Gondwanu	Gondwan	k1gInSc2	Gondwan
(	(	kIx(	(
<g/>
skládající	skládající	k2eAgNnSc1d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
zbylých	zbylý	k2eAgInPc2d1	zbylý
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Deskové	deskový	k2eAgFnSc2d1	desková
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
pozice	pozice	k1gFnSc2	pozice
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
pozice	pozice	k1gFnSc2	pozice
desek	deska	k1gFnPc2	deska
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
odhad	odhad	k1gInSc4	odhad
budoucích	budoucí	k2eAgInPc2d1	budoucí
pohybů	pohyb	k1gInPc2	pohyb
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Určování	určování	k1gNnSc1	určování
zpětné	zpětný	k2eAgFnSc2d1	zpětná
pozice	pozice	k1gFnSc2	pozice
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
případné	případný	k2eAgFnPc4d1	případná
rekonstrukce	rekonstrukce	k1gFnPc4	rekonstrukce
dříve	dříve	k6eAd2	dříve
existujících	existující	k2eAgMnPc2d1	existující
superkontinentů	superkontinent	k1gMnPc2	superkontinent
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
paleogeografii	paleogeografie	k1gFnSc4	paleogeografie
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
deskové	deskový	k2eAgFnPc1d1	desková
hranice	hranice	k1gFnPc1	hranice
jsou	být	k5eAaImIp3nP	být
snadno	snadno	k6eAd1	snadno
rozpoznatelné	rozpoznatelný	k2eAgFnPc1d1	rozpoznatelná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
seismických	seismický	k2eAgNnPc2d1	seismické
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgFnPc1d1	historická
hranice	hranice	k1gFnPc1	hranice
desek	deska	k1gFnPc2	deska
se	se	k3xPyFc4	se
určují	určovat	k5eAaImIp3nP	určovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
různých	různý	k2eAgFnPc2d1	různá
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ofiolitů	ofiolit	k1gInPc2	ofiolit
<g/>
.	.	kIx.	.
</s>
<s>
Různorodá	různorodý	k2eAgFnSc1d1	různorodá
řada	řada	k1gFnSc1	řada
kvantitativních	kvantitativní	k2eAgFnPc2d1	kvantitativní
a	a	k8xC	a
semi-kvantitativních	semivantitativní	k2eAgFnPc2d1	semi-kvantitativní
informací	informace	k1gFnPc2	informace
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
minulé	minulý	k2eAgInPc4d1	minulý
pohyby	pohyb	k1gInPc4	pohyb
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
podobnost	podobnost	k1gFnSc1	podobnost
mezi	mezi	k7c7	mezi
pobřežími	pobřeží	k1gNnPc7	pobřeží
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kontinentů	kontinent	k1gInPc2	kontinent
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
důležité	důležitý	k2eAgInPc1d1	důležitý
náznaky	náznak	k1gInPc1	náznak
dřívější	dřívější	k2eAgFnSc2d1	dřívější
spojitosti	spojitost	k1gFnSc2	spojitost
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgInPc1d1	magnetický
páskován	páskován	k2eAgMnSc1d1	páskován
hornin	hornina	k1gFnPc2	hornina
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
spolehlivého	spolehlivý	k2eAgMnSc4d1	spolehlivý
průvodce	průvodce	k1gMnSc4	průvodce
pohybu	pohyb	k1gInSc2	pohyb
desek	deska	k1gFnPc2	deska
zpětně	zpětně	k6eAd1	zpětně
až	až	k9	až
do	do	k7c2	do
období	období	k1gNnSc2	období
jury	jura	k1gFnSc2	jura
<g/>
.	.	kIx.	.
</s>
<s>
Horké	Horké	k2eAgFnPc1d1	Horké
skvrny	skvrna	k1gFnPc1	skvrna
vytvářející	vytvářející	k2eAgFnPc1d1	vytvářející
řetězce	řetězec	k1gInSc2	řetězec
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Havajské	havajský	k2eAgInPc1d1	havajský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
absolutní	absolutní	k2eAgFnPc4d1	absolutní
rekonstrukce	rekonstrukce	k1gFnPc4	rekonstrukce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
zpětně	zpětně	k6eAd1	zpětně
k	k	k7c3	k
období	období	k1gNnSc3	období
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnPc1d2	starší
rekonstrukce	rekonstrukce	k1gFnPc1	rekonstrukce
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
datech	datum	k1gNnPc6	datum
paleomagnetického	paleomagnetický	k2eAgNnSc2d1	paleomagnetický
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pouze	pouze	k6eAd1	pouze
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
určit	určit	k5eAaPmF	určit
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
šířku	šířka	k1gFnSc4	šířka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Dodatečné	dodatečný	k2eAgInPc4d1	dodatečný
důkazy	důkaz	k1gInPc4	důkaz
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
distribuce	distribuce	k1gFnSc2	distribuce
určitých	určitý	k2eAgFnPc2d1	určitá
sedimentárních	sedimentární	k2eAgFnPc2d1	sedimentární
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc4	rozšíření
stejných	stejný	k2eAgFnPc2d1	stejná
fosílií	fosílie	k1gFnPc2	fosílie
a	a	k8xC	a
pozicí	pozice	k1gFnPc2	pozice
orogeních	orogení	k1gNnPc6	orogení
pásů	pás	k1gInPc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
byla	být	k5eAaImAgFnS	být
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
přijata	přijmout	k5eAaPmNgFnS	přijmout
většinou	většina	k1gFnSc7	většina
odborné	odborný	k2eAgFnSc2d1	odborná
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
<g/>
,	,	kIx,	,
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
regionální	regionální	k2eAgFnPc4d1	regionální
odchylky	odchylka	k1gFnPc4	odchylka
od	od	k7c2	od
globálního	globální	k2eAgInSc2d1	globální
konceptu	koncept	k1gInSc2	koncept
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
rozpory	rozpor	k1gInPc4	rozpor
se	se	k3xPyFc4	se
ale	ale	k9	ale
teorie	teorie	k1gFnSc1	teorie
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
ve	v	k7c6	v
vědeckém	vědecký	k2eAgInSc6d1	vědecký
světě	svět	k1gInSc6	svět
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
většinou	většinou	k6eAd1	většinou
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
správnou	správný	k2eAgFnSc4d1	správná
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
odpůrců	odpůrce	k1gMnPc2	odpůrce
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
stále	stále	k6eAd1	stále
existují	existovat	k5eAaImIp3nP	existovat
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
několik	několik	k4yIc4	několik
alternativních	alternativní	k2eAgFnPc2d1	alternativní
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Konkurenční	konkurenční	k2eAgFnPc1d1	konkurenční
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
teorie	teorie	k1gFnSc1	teorie
expandující	expandující	k2eAgFnSc2d1	expandující
Země	zem	k1gFnSc2	zem
nebo	nebo	k8xC	nebo
teorie	teorie	k1gFnSc1	teorie
jejího	její	k3xOp3gNnSc2	její
smršťování	smršťování	k1gNnSc2	smršťování
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
chladnutí	chladnutí	k1gNnSc2	chladnutí
nebyly	být	k5eNaImAgFnP	být
ale	ale	k9	ale
širší	široký	k2eAgFnSc7d2	širší
odbornou	odborný	k2eAgFnSc7d1	odborná
veřejností	veřejnost	k1gFnSc7	veřejnost
akceptovány	akceptován	k2eAgFnPc1d1	akceptována
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
na	na	k7c6	na
terestrických	terestrický	k2eAgFnPc6d1	terestrická
planetách	planeta	k1gFnPc6	planeta
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
planetách	planeta	k1gFnPc6	planeta
hmotnější	hmotný	k2eAgFnSc2d2	hmotnější
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
budou	být	k5eAaImBp3nP	být
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
probíhat	probíhat	k5eAaImF	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hraničním	hraniční	k2eAgInSc7d1	hraniční
případem	případ	k1gInSc7	případ
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
přítomnosti	přítomnost	k1gFnSc2	přítomnost
tektonických	tektonický	k2eAgInPc2d1	tektonický
pochodů	pochod	k1gInPc2	pochod
způsobených	způsobený	k2eAgFnPc2d1	způsobená
dostatkem	dostatek	k1gInSc7	dostatek
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kontinenty	kontinent	k1gInPc1	kontinent
jsou	být	k5eAaImIp3nP	být
výtvory	výtvor	k1gInPc4	výtvor
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geologie	geologie	k1gFnSc2	geologie
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
Venuše	Venuše	k1gFnSc1	Venuše
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
Zemi	zem	k1gFnSc3	zem
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
,	,	kIx,	,
povrchová	povrchový	k2eAgFnSc1d1	povrchová
geologie	geologie	k1gFnSc1	geologie
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
–	–	k?	–
povrch	povrch	k1gInSc1	povrch
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
spíše	spíše	k9	spíše
bazaltickému	bazaltický	k2eAgNnSc3d1	bazaltický
mořskému	mořský	k2eAgNnSc3d1	mořské
dnu	dno	k1gNnSc3	dno
a	a	k8xC	a
postrádá	postrádat	k5eAaImIp3nS	postrádat
kontinenty	kontinent	k1gInPc4	kontinent
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
vyvýšené	vyvýšený	k2eAgInPc4d1	vyvýšený
útvary	útvar	k1gInPc4	útvar
Ishtar	Ishtar	k1gMnSc1	Ishtar
Terra	Terra	k1gMnSc1	Terra
a	a	k8xC	a
Aphrodite	Aphrodit	k1gInSc5	Aphrodit
Terra	Terra	k1gMnSc1	Terra
nejsou	být	k5eNaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
chemicky	chemicky	k6eAd1	chemicky
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
povrchu	povrch	k1gInSc2	povrch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
zvažují	zvažovat	k5eAaImIp3nP	zvažovat
alespoň	alespoň	k9	alespoň
částečnou	částečný	k2eAgFnSc4d1	částečná
subdukci	subdukce	k1gFnSc4	subdukce
kůry	kůra	k1gFnSc2	kůra
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tzv.	tzv.	kA	tzv.
korón	koróna	k1gFnPc2	koróna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
obří	obří	k2eAgFnSc1d1	obří
Artemis	Artemis	k1gFnSc1	Artemis
Chasma	Chasmum	k1gNnSc2	Chasmum
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
~	~	kIx~	~
<g/>
800	[number]	k4	800
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Venuše	Venuše	k1gFnSc2	Venuše
neukazuje	ukazovat	k5eNaImIp3nS	ukazovat
žádné	žádný	k3yNgFnPc4	žádný
známky	známka	k1gFnPc4	známka
aktivní	aktivní	k2eAgFnSc2d1	aktivní
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vedou	vést	k5eAaImIp3nP	vést
se	se	k3xPyFc4	se
diskuse	diskuse	k1gFnPc1	diskuse
o	o	k7c6	o
poznatcích	poznatek	k1gInPc6	poznatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
historii	historie	k1gFnSc6	historie
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
aktivní	aktivní	k2eAgInPc1d1	aktivní
tektonické	tektonický	k2eAgInPc1d1	tektonický
pochody	pochod	k1gInPc1	pochod
mohly	moct	k5eAaImAgInP	moct
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
nacházet	nacházet	k5eAaImF	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
pochody	pochod	k1gInPc1	pochod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgInP	odehrát
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
planety	planeta	k1gFnSc2	planeta
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
například	například	k6eAd1	například
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
a	a	k8xC	a
všeobecně	všeobecně	k6eAd1	všeobecně
přijímaná	přijímaný	k2eAgFnSc1d1	přijímaná
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
litosféra	litosféra	k1gFnSc1	litosféra
Venuše	Venuše	k1gFnSc2	Venuše
rychle	rychle	k6eAd1	rychle
ztloustla	ztloustnout	k5eAaPmAgFnS	ztloustnout
během	během	k7c2	během
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znemožnily	znemožnit	k5eAaPmAgInP	znemožnit
jasnou	jasný	k2eAgFnSc4d1	jasná
interpretaci	interpretace	k1gFnSc4	interpretace
geologických	geologický	k2eAgInPc2d1	geologický
záznamů	záznam	k1gInPc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
dobře	dobře	k6eAd1	dobře
zachovaných	zachovaný	k2eAgInPc2d1	zachovaný
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Venuše	Venuše	k1gFnSc2	Venuše
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
využít	využít	k5eAaPmF	využít
metodu	metoda	k1gFnSc4	metoda
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xS	jako
počítání	počítání	k1gNnSc1	počítání
množství	množství	k1gNnSc1	množství
kráterů	kráter	k1gInPc2	kráter
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
absolutního	absolutní	k2eAgNnSc2d1	absolutní
stáří	stáří	k1gNnSc2	stáří
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
počtů	počet	k1gInPc2	počet
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
povrchu	povrch	k1gInSc2	povrch
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
stará	stará	k1gFnSc1	stará
500	[number]	k4	500
až	až	k9	až
750	[number]	k4	750
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
jiné	jiný	k2eAgInPc1d1	jiný
výpočty	výpočet	k1gInPc1	výpočet
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
věk	věk	k1gInSc4	věk
okolo	okolo	k7c2	okolo
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věk	věk	k1gInSc1	věk
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
mladý	mladý	k2eAgMnSc1d1	mladý
a	a	k8xC	a
že	že	k8xS	že
během	během	k7c2	během
historie	historie	k1gFnSc2	historie
planety	planeta	k1gFnSc2	planeta
došlo	dojít	k5eAaPmAgNnS	dojít
minimálně	minimálně	k6eAd1	minimálně
jednou	jednou	k6eAd1	jednou
k	k	k7c3	k
celkovému	celkový	k2eAgNnSc3d1	celkové
vulkanickému	vulkanický	k2eAgNnSc3d1	vulkanické
přetvoření	přetvoření	k1gNnSc3	přetvoření
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
umožňující	umožňující	k2eAgInSc4d1	umožňující
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
únik	únik	k1gInSc4	únik
takového	takový	k3xDgNnSc2	takový
množství	množství	k1gNnSc2	množství
tepla	teplo	k1gNnSc2	teplo
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
předmětem	předmět	k1gInSc7	předmět
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
debat	debata	k1gFnPc2	debata
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
zastávají	zastávat	k5eAaImIp3nP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
určitý	určitý	k2eAgInSc4d1	určitý
styl	styl	k1gInSc4	styl
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
absence	absence	k1gFnSc2	absence
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
teploty	teplota	k1gFnPc1	teplota
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
vysoké	vysoký	k2eAgInPc1d1	vysoký
pro	pro	k7c4	pro
možnost	možnost	k1gFnSc4	možnost
existence	existence	k1gFnSc2	existence
významnějšího	významný	k2eAgNnSc2d2	významnější
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
prosycena	prosytit	k5eAaPmNgFnS	prosytit
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pak	pak	k6eAd1	pak
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
střižných	střižný	k2eAgFnPc2d1	střižná
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
slabé	slabý	k2eAgFnPc4d1	slabá
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yIgInPc6	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
korové	korový	k2eAgInPc1d1	korový
bloky	blok	k1gInPc1	blok
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
slabé	slabý	k2eAgFnPc1d1	slabá
zóny	zóna	k1gFnPc1	zóna
se	se	k3xPyFc4	se
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
nikdy	nikdy	k6eAd1	nikdy
nevyvinuly	vyvinout	k5eNaPmAgFnP	vyvinout
kvůli	kvůli	k7c3	kvůli
nepřítomnosti	nepřítomnost	k1gFnSc3	nepřítomnost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
existence	existence	k1gFnSc2	existence
či	či	k8xC	či
neexistence	neexistence	k1gFnSc2	neexistence
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
předmětem	předmět	k1gInSc7	předmět
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
debat	debata	k1gFnPc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geologie	geologie	k1gFnSc2	geologie
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
Země	země	k1gFnSc1	země
či	či	k8xC	či
Venuše	Venuše	k1gFnSc1	Venuše
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
důkazy	důkaz	k1gInPc7	důkaz
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
ledu	led	k1gInSc2	led
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
i	i	k9	i
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
automatických	automatický	k2eAgFnPc2d1	automatická
sond	sonda	k1gFnPc2	sonda
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mars	Mars	k1gInSc1	Mars
kdysi	kdysi	k6eAd1	kdysi
měl	mít	k5eAaImAgInS	mít
deskovou	deskový	k2eAgFnSc4d1	desková
tektoniku	tektonika	k1gFnSc4	tektonika
a	a	k8xC	a
rozhraní	rozhraní	k1gNnSc4	rozhraní
marsovské	marsovský	k2eAgFnSc2d1	marsovská
dichotomie	dichotomie	k1gFnSc2	dichotomie
tvořilo	tvořit	k5eAaImAgNnS	tvořit
rozhraní	rozhraní	k1gNnSc1	rozhraní
mezi	mezi	k7c7	mezi
oceánskou	oceánský	k2eAgFnSc7d1	oceánská
a	a	k8xC	a
pevninskou	pevninský	k2eAgFnSc7d1	pevninská
kůrou	kůra	k1gFnSc7	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
detailní	detailní	k2eAgFnSc7d1	detailní
analýzou	analýza	k1gFnSc7	analýza
navrhovaného	navrhovaný	k2eAgNnSc2d1	navrhované
pobřeží	pobřeží	k1gNnSc2	pobřeží
na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
sondy	sonda	k1gFnSc2	sonda
Mars	Mars	k1gInSc1	Mars
Global	globat	k5eAaImAgMnS	globat
Surveyor	Surveyor	k1gMnSc1	Surveyor
–	–	k?	–
ta	ten	k3xDgFnSc1	ten
ale	ale	k8xC	ale
také	také	k9	také
přinesla	přinést	k5eAaPmAgNnP	přinést
měření	měření	k1gNnPc1	měření
magnetizace	magnetizace	k1gFnSc2	magnetizace
kůry	kůra	k1gFnSc2	kůra
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižních	jižní	k2eAgFnPc2d1	jižní
vysočin	vysočina	k1gFnPc2	vysočina
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nápadně	nápadně	k6eAd1	nápadně
podobají	podobat	k5eAaImIp3nP	podobat
alternující	alternující	k2eAgFnSc4d1	alternující
magnetizaci	magnetizace	k1gFnSc4	magnetizace
rozpínajícího	rozpínající	k2eAgMnSc2d1	rozpínající
se	se	k3xPyFc4	se
mořského	mořský	k2eAgInSc2d1	mořský
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
planety	planeta	k1gFnSc2	planeta
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
rozpínání	rozpínání	k1gNnSc3	rozpínání
a	a	k8xC	a
tvorbě	tvorba	k1gFnSc3	tvorba
nové	nový	k2eAgFnSc2d1	nová
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgNnPc1	tento
měření	měření	k1gNnPc1	měření
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mars	Mars	k1gInSc1	Mars
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
minulosti	minulost	k1gFnSc6	minulost
globální	globální	k2eAgFnSc2d1	globální
magnetické	magnetický	k2eAgFnSc2d1	magnetická
pole	pole	k1gFnSc2	pole
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
deskovou	deskový	k2eAgFnSc4d1	desková
tektoniku	tektonika	k1gFnSc4	tektonika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
stopy	stopa	k1gFnPc4	stopa
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
překryty	překrýt	k5eAaPmNgFnP	překrýt
<g/>
.	.	kIx.	.
</s>
<s>
Modelování	modelování	k1gNnSc1	modelování
termální	termální	k2eAgFnSc2d1	termální
evoluce	evoluce	k1gFnSc2	evoluce
planety	planeta	k1gFnSc2	planeta
ovšem	ovšem	k9	ovšem
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
gravitační	gravitační	k2eAgFnSc1d1	gravitační
<g/>
,	,	kIx,	,
topografická	topografický	k2eAgFnSc1d1	topografická
i	i	k8xC	i
magnetická	magnetický	k2eAgNnPc1d1	magnetické
data	datum	k1gNnPc1	datum
lze	lze	k6eAd1	lze
jednodušeji	jednoduše	k6eAd2	jednoduše
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
jednodušším	jednoduchý	k2eAgInSc7d2	jednodušší
jednodeskovým	jednodeskový	k2eAgInSc7d1	jednodeskový
modelem	model	k1gInSc7	model
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
vědci	vědec	k1gMnPc1	vědec
nesouhlasí	souhlasit	k5eNaImIp3nP	souhlasit
a	a	k8xC	a
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
magnetické	magnetický	k2eAgFnPc1d1	magnetická
anomálie	anomálie	k1gFnPc1	anomálie
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
spíše	spíše	k9	spíše
upwellingem	upwelling	k1gInSc7	upwelling
(	(	kIx(	(
<g/>
výstupem	výstup	k1gInSc7	výstup
<g/>
)	)	kIx)	)
materiálu	materiál	k1gInSc2	materiál
v	v	k7c6	v
marsovském	marsovský	k2eAgInSc6d1	marsovský
plášti	plášť	k1gInSc6	plášť
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vedl	vést	k5eAaImAgInS	vést
ke	k	k7c3	k
ztluštění	ztluštění	k1gNnSc3	ztluštění
kůry	kůra	k1gFnSc2	kůra
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Southern	Southerna	k1gFnPc2	Southerna
Highlands	Highlandsa	k1gFnPc2	Highlandsa
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
oblasti	oblast	k1gFnSc2	oblast
Tharsis	Tharsis	k1gFnSc2	Tharsis
nebo	nebo	k8xC	nebo
obrovským	obrovský	k2eAgInSc7d1	obrovský
impaktem	impakt	k1gInSc7	impakt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Severní	severní	k2eAgFnSc4d1	severní
nížiny	nížina	k1gFnPc4	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Marsu	Mars	k1gInSc2	Mars
sondy	sonda	k1gFnSc2	sonda
Mars	Mars	k1gInSc1	Mars
Global	globat	k5eAaImAgMnS	globat
Surveyor	Surveyor	k1gMnSc1	Surveyor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
ukázala	ukázat	k5eAaPmAgFnS	ukázat
magnetické	magnetický	k2eAgNnSc4d1	magnetické
páskování	páskování	k1gNnSc4	páskování
hornin	hornina	k1gFnPc2	hornina
i	i	k8xC	i
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
pro	pro	k7c4	pro
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
vzniku	vznik	k1gInSc2	vznik
páskování	páskování	k1gNnSc2	páskování
vyžadovali	vyžadovat	k5eAaImAgMnP	vyžadovat
existenci	existence	k1gFnSc4	existence
procesy	proces	k1gInPc1	proces
deskové	deskový	k2eAgInPc1d1	deskový
tektoniky	tektonika	k1gFnPc4	tektonika
jako	jako	k8xS	jako
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
data	datum	k1gNnPc1	datum
neobstály	obstát	k5eNaPmAgFnP	obstát
během	během	k7c2	během
testu	test	k1gInSc2	test
nazvaného	nazvaný	k2eAgInSc2d1	nazvaný
"	"	kIx"	"
<g/>
magnetic	magnetice	k1gFnPc2	magnetice
reversal	reversat	k5eAaImAgInS	reversat
test	test	k1gInSc1	test
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zjištění	zjištění	k1gNnSc4	zjištění
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
horniny	hornina	k1gFnPc1	hornina
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
obrácené	obrácený	k2eAgFnSc2d1	obrácená
magnetické	magnetický	k2eAgFnSc2d1	magnetická
polarity	polarita	k1gFnSc2	polarita
planetárního	planetární	k2eAgNnSc2d1	planetární
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
ledových	ledový	k2eAgInPc6d1	ledový
satelitech	satelit	k1gInPc6	satelit
obíhajících	obíhající	k2eAgInPc2d1	obíhající
Jupiter	Jupiter	k1gInSc4	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gInSc4	Saturn
je	být	k5eAaImIp3nS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
endogenní	endogenní	k2eAgFnSc1d1	endogenní
aktivita	aktivita	k1gFnSc1	aktivita
včetně	včetně	k7c2	včetně
procesů	proces	k1gInPc2	proces
připomínajících	připomínající	k2eAgInPc2d1	připomínající
tektonické	tektonický	k2eAgInPc4d1	tektonický
jevy	jev	k1gInPc4	jev
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
–	–	k?	–
žádná	žádný	k3yNgNnPc4	žádný
pozorování	pozorování	k1gNnPc4	pozorování
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
nenasvědčují	nasvědčovat	k5eNaImIp3nP	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
fungovala	fungovat	k5eAaImAgFnS	fungovat
přímá	přímý	k2eAgFnSc1d1	přímá
obdoba	obdoba	k1gFnSc1	obdoba
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
hlášeno	hlásit	k5eAaImNgNnS	hlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Titan	titan	k1gInSc4	titan
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
známky	známka	k1gFnPc4	známka
tektonických	tektonický	k2eAgInPc2d1	tektonický
procesů	proces	k1gInPc2	proces
na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
přistávacího	přistávací	k2eAgInSc2d1	přistávací
modulu	modul	k1gInSc2	modul
Huygens	Huygensa	k1gFnPc2	Huygensa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přistála	přistát	k5eAaPmAgFnS	přistát
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
má	mít	k5eAaImIp3nS	mít
deskovou	deskový	k2eAgFnSc4d1	desková
tektoniku	tektonika	k1gFnSc4	tektonika
formující	formující	k2eAgInSc4d1	formující
jejich	jejich	k3xOp3gInSc4	jejich
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
exoplanetách	exoplaneta	k1gFnPc6	exoplaneta
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
povrchu	povrch	k1gInSc6	povrch
přítomen	přítomen	k2eAgInSc1d1	přítomen
oceán	oceán	k1gInSc1	oceán
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
místo	místo	k1gNnSc4	místo
toho	ten	k3xDgNnSc2	ten
na	na	k7c6	na
větších	veliký	k2eAgFnPc6d2	veliký
planetách	planeta	k1gFnPc6	planeta
tzv.	tzv.	kA	tzv.
super-Zemích	super-Zemí	k1gNnPc6	super-Zemí
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
nacházet	nacházet	k5eAaImF	nacházet
i	i	k9	i
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
