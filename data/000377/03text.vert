<s>
Zámek	zámek	k1gInSc1	zámek
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Das	Das	k1gFnSc2	Das
Schloß	Schloß	k1gFnSc2	Schloß
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
Franze	Franze	k1gFnSc1	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
napsaný	napsaný	k2eAgInSc4d1	napsaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
možných	možný	k2eAgFnPc2d1	možná
interpretací	interpretace	k1gFnPc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
tragické	tragický	k2eAgFnSc6d1	tragická
srážce	srážka	k1gFnSc6	srážka
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
s	s	k7c7	s
nepochopitelnou	pochopitelný	k2eNgFnSc7d1	nepochopitelná
byrokratickou	byrokratický	k2eAgFnSc7d1	byrokratická
mašinerií	mašinerie	k1gFnSc7	mašinerie
<g/>
.	.	kIx.	.
</s>
<s>
Kafka	Kafka	k1gMnSc1	Kafka
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
židovskou	židovský	k2eAgFnSc7d1	židovská
tradicí	tradice	k1gFnSc7	tradice
a	a	k8xC	a
mystikou	mystika	k1gFnSc7	mystika
-	-	kIx~	-
podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
interpretace	interpretace	k1gFnSc2	interpretace
román	román	k1gInSc4	román
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
příchod	příchod	k1gInSc1	příchod
nerozpoznaného	rozpoznaný	k2eNgMnSc2d1	nerozpoznaný
mesiáše	mesiáš	k1gMnSc2	mesiáš
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
Kafkovo	Kafkův	k2eAgNnSc4d1	Kafkovo
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nedokončené	dokončený	k2eNgNnSc1d1	nedokončené
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
K.	K.	kA	K.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
<g/>
)	)	kIx)	)
vydává	vydávat	k5eAaPmIp3nS	vydávat
za	za	k7c4	za
zeměměřiče	zeměměřič	k1gMnPc4	zeměměřič
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
zeměměřičem	zeměměřič	k1gMnSc7	zeměměřič
skutečně	skutečně	k6eAd1	skutečně
je	být	k5eAaImIp3nS	být
-	-	kIx~	-
zde	zde	k6eAd1	zde
román	román	k1gInSc1	román
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgInPc6d1	další
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
značný	značný	k2eAgInSc1d1	značný
prostor	prostor	k1gInSc1	prostor
individuálnímu	individuální	k2eAgInSc3d1	individuální
výkladu	výklad	k1gInSc3	výklad
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
pod	pod	k7c7	pod
zámkem	zámek	k1gInSc7	zámek
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yIgFnPc4	který
původně	původně	k6eAd1	původně
vůbec	vůbec	k9	vůbec
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
aklimatizuje	aklimatizovat	k5eAaBmIp3nS	aklimatizovat
a	a	k8xC	a
marně	marně	k6eAd1	marně
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
kontaktovat	kontaktovat	k5eAaImF	kontaktovat
úředníky	úředník	k1gMnPc4	úředník
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgInS	dozvědět
nějaké	nějaký	k3yIgFnPc4	nějaký
podrobnosti	podrobnost	k1gFnPc4	podrobnost
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
byl	být	k5eAaImAgInS	být
pozván	pozván	k2eAgInSc1d1	pozván
<g/>
.	.	kIx.	.
</s>
<s>
Nejdřív	dříve	k6eAd3	dříve
zámek	zámek	k1gInSc1	zámek
nevidí	vidět	k5eNaImIp3nS	vidět
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
zahalen	zahalit	k5eAaPmNgInS	zahalit
mlhou	mlha	k1gFnSc7	mlha
<g/>
)	)	kIx)	)
a	a	k8xC	a
ani	ani	k8xC	ani
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
nedaří	dařit	k5eNaImIp3nS	dařit
dostat	dostat	k5eAaPmF	dostat
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
tráví	trávit	k5eAaImIp3nS	trávit
několik	několik	k4yIc1	několik
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
prožívá	prožívat	k5eAaImIp3nS	prožívat
milostný	milostný	k2eAgInSc1d1	milostný
románek	románek	k1gInSc1	románek
s	s	k7c7	s
Frídou	Frída	k1gFnSc7	Frída
(	(	kIx(	(
<g/>
číšnice	číšnice	k1gFnSc1	číšnice
panského	panský	k2eAgInSc2d1	panský
hostince	hostinec	k1gInSc2	hostinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
však	však	k9	však
brzy	brzy	k6eAd1	brzy
rozchází	rozcházet	k5eAaImIp3nS	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Frída	Frída	k1gFnSc1	Frída
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
i	i	k9	i
milenkou	milenka	k1gFnSc7	milenka
Klamma	Klammum	k1gNnSc2	Klammum
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
K.	K.	kA	K.
stává	stávat	k5eAaImIp3nS	stávat
školníkem	školník	k1gMnSc7	školník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
pro	pro	k7c4	pro
zeměměřiče	zeměměřič	k1gMnPc4	zeměměřič
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
práce	práce	k1gFnSc1	práce
a	a	k8xC	a
taky	taky	k9	taky
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
z	z	k7c2	z
hostince	hostinec	k1gInSc2	hostinec
U	u	k7c2	u
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
o	o	k7c4	o
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
Klammem	Klamm	k1gInSc7	Klamm
nebo	nebo	k8xC	nebo
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
úředníkem	úředník	k1gMnSc7	úředník
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Frída	Frída	k1gFnSc1	Frída
ho	on	k3xPp3gMnSc4	on
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
,	,	kIx,	,
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
K.	K.	kA	K.
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
vydává	vydávat	k5eAaImIp3nS	vydávat
hledat	hledat	k5eAaImF	hledat
<g/>
,	,	kIx,	,
nenalezne	naleznout	k5eNaPmIp3nS	naleznout
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zabloudí	zabloudit	k5eAaPmIp3nS	zabloudit
až	až	k9	až
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
na	na	k7c4	na
která	který	k3yIgNnPc4	který
nemá	mít	k5eNaImIp3nS	mít
přístup	přístup	k1gInSc1	přístup
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vyveden	vyvést	k5eAaPmNgInS	vyvést
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Úředníci	úředník	k1gMnPc1	úředník
<g/>
,	,	kIx,	,
odtrženi	odtržen	k2eAgMnPc1d1	odtržen
od	od	k7c2	od
reality	realita	k1gFnSc2	realita
obyčejných	obyčejný	k2eAgMnPc2d1	obyčejný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
nebyl	být	k5eNaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dle	dle	k7c2	dle
zprávy	zpráva	k1gFnSc2	zpráva
Maxe	Max	k1gMnSc4	Max
Broda	Broda	k1gMnSc1	Broda
zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
nakonec	nakonec	k6eAd1	nakonec
umírá	umírat	k5eAaImIp3nS	umírat
vysílením	vysílení	k1gNnSc7	vysílení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
posteli	postel	k1gFnSc6	postel
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
přijdou	přijít	k5eAaPmIp3nP	přijít
úředníci	úředník	k1gMnPc1	úředník
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
konečně	konečně	k6eAd1	konečně
nastoupit	nastoupit	k5eAaPmF	nastoupit
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
i	i	k9	i
ostatní	ostatní	k2eAgNnPc1d1	ostatní
Kafkova	Kafkův	k2eAgNnPc1d1	Kafkovo
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
i	i	k8xC	i
tento	tento	k3xDgInSc4	tento
příběh	příběh	k1gInSc4	příběh
lze	lze	k6eAd1	lze
vykládat	vykládat	k5eAaImF	vykládat
jako	jako	k8xS	jako
metaforu	metafora	k1gFnSc4	metafora
marného	marný	k2eAgInSc2d1	marný
boje	boj	k1gInSc2	boj
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
proti	proti	k7c3	proti
mašinérii	mašinérie	k1gFnSc3	mašinérie
byrokratického	byrokratický	k2eAgNnSc2d1	byrokratické
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
nedává	dávat	k5eNaImIp3nS	dávat
žádnou	žádný	k3yNgFnSc4	žádný
naději	naděje	k1gFnSc4	naděje
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
K.	K.	kA	K.
by	by	kYmCp3nS	by
jednou	jeden	k4xCgFnSc7	jeden
mohl	moct	k5eAaImAgInS	moct
dojít	dojít	k5eAaPmF	dojít
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
rozřešení	rozřešení	k1gNnSc2	rozřešení
své	svůj	k3xOyFgFnSc2	svůj
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
v	v	k7c4	v
jeho	jeho	k3xOp3gInSc4	jeho
prospěch	prospěch	k1gInSc4	prospěch
čí	čí	k3xOyRgInSc4	čí
neprospěch	neprospěch	k1gInSc4	neprospěch
<g/>
.	.	kIx.	.
</s>
<s>
Jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
K-ova	Kva	k1gFnSc1	K-ova
akce	akce	k1gFnSc1	akce
či	či	k8xC	či
intrika	intrika	k1gFnSc1	intrika
je	být	k5eAaImIp3nS	být
marná	marný	k2eAgFnSc1d1	marná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
spřádají	spřádat	k5eAaImIp3nP	spřádat
své	svůj	k3xOyFgInPc4	svůj
plány	plán	k1gInPc4	plán
a	a	k8xC	a
dostávají	dostávat	k5eAaImIp3nP	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
s	s	k7c7	s
byrokracií	byrokracie	k1gFnSc7	byrokracie
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
šíří	šířit	k5eAaImIp3nP	šířit
a	a	k8xC	a
byrokratické	byrokratický	k2eAgFnPc4d1	byrokratická
manýry	manýra	k1gFnPc4	manýra
prorůstají	prorůstat	k5eAaImIp3nP	prorůstat
celou	celý	k2eAgFnSc7d1	celá
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
komiksu	komiks	k1gInSc2	komiks
román	román	k1gInSc1	román
adaptoval	adaptovat	k5eAaBmAgMnS	adaptovat
David	David	k1gMnSc1	David
Z.	Z.	kA	Z.
Mairowitz	Mairowitz	k1gMnSc1	Mairowitz
a	a	k8xC	a
Jaromír	Jaromír	k1gMnSc1	Jaromír
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
SelfMadeHero	SelfMadeHero	k1gNnSc1	SelfMadeHero
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyjde	vyjít	k5eAaPmIp3nS	vyjít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
v	v	k7c6	v
Labyrintu	labyrint	k1gInSc6	labyrint
<g/>
.	.	kIx.	.
</s>
