<p>
<s>
Harare	Harar	k1gMnSc5	Harar
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
rozlohu	rozloha	k1gFnSc4	rozloha
559	[number]	k4	559
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
600	[number]	k4	600
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
2	[number]	k4	2
800	[number]	k4	800
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Harare	Harar	k1gMnSc5	Harar
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Republiky	republika	k1gFnSc2	republika
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
nacházející	nacházející	k2eAgFnSc6d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Mashonaland	Mashonalanda	k1gFnPc2	Mashonalanda
Central	Central	k1gFnSc2	Central
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
1483	[number]	k4	1483
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1430	[number]	k4	1430
m	m	kA	m
(	(	kIx(	(
<g/>
hladina	hladina	k1gFnSc1	hladina
jezera	jezero	k1gNnSc2	jezero
Chivero	Chivero	k1gNnSc4	Chivero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1617	[number]	k4	1617
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
21	[number]	k4	21
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
okolo	okolo	k7c2	okolo
14	[number]	k4	14
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
průměrně	průměrně	k6eAd1	průměrně
spadne	spadnout	k5eAaPmIp3nS	spadnout
140	[number]	k4	140
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téměř	téměř	k6eAd1	téměř
vůbec	vůbec	k9	vůbec
neprší	pršet	k5eNaImIp3nS	pršet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
jako	jako	k8xS	jako
Salisbury	Salisbura	k1gFnPc4	Salisbura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
Britskou	britský	k2eAgFnSc7d1	britská
jihoafrickou	jihoafrický	k2eAgFnSc7d1	Jihoafrická
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
městské	městský	k2eAgFnSc2d1	městská
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
započal	započnout	k5eAaPmAgInS	započnout
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
otevřela	otevřít	k5eAaPmAgFnS	otevřít
železnice	železnice	k1gFnSc1	železnice
spojující	spojující	k2eAgNnSc1d1	spojující
město	město	k1gNnSc1	město
s	s	k7c7	s
Beirou	Beira	k1gFnSc7	Beira
v	v	k7c6	v
Mosambiku	Mosambik	k1gInSc6	Mosambik
<g/>
.	.	kIx.	.
</s>
<s>
Práva	práv	k2eAgNnPc1d1	právo
města	město	k1gNnPc1	město
Salisbury	Salisbura	k1gFnSc2	Salisbura
získalo	získat	k5eAaPmAgNnS	získat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
začala	začít	k5eAaPmAgFnS	začít
industrializace	industrializace	k1gFnSc1	industrializace
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgMnPc2d1	nový
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
status	status	k1gInSc4	status
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
kolonie	kolonie	k1gFnSc2	kolonie
Rhodésie	Rhodésie	k1gFnSc2	Rhodésie
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953-1963	[number]	k4	1953-1963
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Federace	federace	k1gFnSc2	federace
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
a	a	k8xC	a
Ňaska	Ňasko	k1gNnSc2	Ňasko
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1965	[number]	k4	1965
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Republiky	republika	k1gFnSc2	republika
Rhodesie	Rhodesie	k1gFnSc2	Rhodesie
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Zimbabwe	Zimbabwe	k1gNnSc4	Zimbabwe
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
se	se	k3xPyFc4	se
přejmenovalo	přejmenovat	k5eAaPmAgNnS	přejmenovat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Harare	Harar	k1gMnSc5	Harar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
město	město	k1gNnSc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Harare	Harar	k1gMnSc5	Harar
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgNnSc1d1	moderní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
na	na	k7c4	na
africké	africký	k2eAgInPc4d1	africký
poměry	poměr	k1gInPc4	poměr
velmi	velmi	k6eAd1	velmi
čisté	čistý	k2eAgInPc1d1	čistý
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
třídou	třída	k1gFnSc7	třída
je	být	k5eAaImIp3nS	být
Samora	Samora	k1gFnSc1	Samora
Machel	Machel	k1gInSc4	Machel
avenue	avenue	k1gFnSc2	avenue
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
stojí	stát	k5eAaImIp3nP	stát
katedrály	katedrála	k1gFnPc1	katedrála
římsko-katolické	římskoatolický	k2eAgFnPc1d1	římsko-katolická
a	a	k8xC	a
anglikánské	anglikánský	k2eAgFnPc1d1	anglikánská
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Knihovna	knihovna	k1gFnSc1	knihovna
královny	královna	k1gFnSc2	královna
Viktorie	Viktorie	k1gFnSc1	Viktorie
<g/>
,	,	kIx,	,
Zimbabwská	zimbabwský	k2eAgFnSc1d1	Zimbabwská
národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
Národní	národní	k2eAgInSc1d1	národní
archiv	archiv	k1gInSc1	archiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metropoli	metropol	k1gFnSc6	metropol
nechybí	chybit	k5eNaPmIp3nS	chybit
ani	ani	k8xC	ani
výškové	výškový	k2eAgFnPc4d1	výšková
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
ulice	ulice	k1gFnPc4	ulice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
lemují	lemovat	k5eAaImIp3nP	lemovat
aleje	alej	k1gFnPc4	alej
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
jeho	jeho	k3xOp3gNnSc1	jeho
zakladatelé	zakladatel	k1gMnPc1	zakladatel
původně	původně	k6eAd1	původně
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
Salisbury	Salisbur	k1gInPc4	Salisbur
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poctili	poctít	k5eAaPmAgMnP	poctít
anglického	anglický	k2eAgMnSc4d1	anglický
markýze	markýz	k1gMnSc4	markýz
Roberta	Robert	k1gMnSc4	Robert
Gascoyne-Cecil	Gascoyne-Cecil	k1gInSc4	Gascoyne-Cecil
<g/>
,	,	kIx,	,
markýze	markýz	k1gMnSc4	markýz
ze	z	k7c2	z
Salisbury	Salisbura	k1gFnSc2	Salisbura
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
-	-	kIx~	-
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
,	,	kIx,	,
ministerského	ministerský	k2eAgMnSc4d1	ministerský
předsedu	předseda	k1gMnSc4	předseda
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
na	na	k7c6	na
Harare	Harar	k1gMnSc5	Harar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
původní	původní	k2eAgInSc4d1	původní
název	název	k1gInSc4	název
satelitního	satelitní	k2eAgNnSc2d1	satelitní
města	město	k1gNnSc2	město
Chitungwiza	Chitungwiza	k1gFnSc1	Chitungwiza
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
Harare	Harar	k1gMnSc5	Harar
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
náčelníka	náčelník	k1gMnSc2	náčelník
Neharaweho	Neharawe	k1gMnSc2	Neharawe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Luke	Luke	k1gFnSc1	Luke
Steyn	Steyna	k1gFnPc2	Steyna
(	(	kIx(	(
<g/>
*	*	kIx~	*
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
alpský	alpský	k2eAgMnSc1d1	alpský
lyžař	lyžař	k1gMnSc1	lyžař
<g/>
,	,	kIx,	,
účastník	účastník	k1gMnSc1	účastník
ZOH	ZOH	kA	ZOH
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
</s>
</p>
<p>
<s>
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Harare	Harar	k1gMnSc5	Harar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
