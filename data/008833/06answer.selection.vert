<s>
Opočenský	opočenský	k2eAgInSc4d1	opočenský
zámek	zámek	k1gInSc4	zámek
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
Státní	státní	k2eAgInSc1d1	státní
zámek	zámek	k1gInSc1	zámek
Opočno	Opočno	k1gNnSc1	Opočno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
renesanční	renesanční	k2eAgFnSc1d1	renesanční
stavba	stavba	k1gFnSc1	stavba
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
Opočno	Opočno	k1gNnSc1	Opočno
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Orlických	orlický	k2eAgFnPc2d1	Orlická
hor	hora	k1gFnPc2	hora
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
asi	asi	k9	asi
30	[number]	k4	30
km	km	kA	km
od	od	k7c2	od
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
