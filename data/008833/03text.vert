<p>
<s>
Opočenský	opočenský	k2eAgInSc4d1	opočenský
zámek	zámek	k1gInSc4	zámek
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
Státní	státní	k2eAgInSc1d1	státní
zámek	zámek	k1gInSc1	zámek
Opočno	Opočno	k1gNnSc1	Opočno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
renesanční	renesanční	k2eAgFnSc1d1	renesanční
stavba	stavba	k1gFnSc1	stavba
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
Opočno	Opočno	k1gNnSc1	Opočno
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Orlických	orlický	k2eAgFnPc2d1	Orlická
hor	hora	k1gFnPc2	hora
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
asi	asi	k9	asi
30	[number]	k4	30
km	km	kA	km
od	od	k7c2	od
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
správu	správa	k1gFnSc4	správa
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
přestavbou	přestavba	k1gFnSc7	přestavba
původně	původně	k6eAd1	původně
gotického	gotický	k2eAgInSc2d1	gotický
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
majitele	majitel	k1gMnPc4	majitel
patřily	patřit	k5eAaImAgInP	patřit
rody	rod	k1gInPc1	rod
Trčků	Trčka	k1gMnPc2	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
a	a	k8xC	a
Colloredo-Mansfeldové	Colloredo-Mansfeldová	k1gFnSc2	Colloredo-Mansfeldová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nacistické	nacistický	k2eAgFnSc6d1	nacistická
okupaci	okupace	k1gFnSc6	okupace
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
konfiskován	konfiskovat	k5eAaBmNgInS	konfiskovat
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc4	zámek
předmětem	předmět	k1gInSc7	předmět
restitučních	restituční	k2eAgInPc2d1	restituční
nároků	nárok	k1gInPc2	nárok
potomků	potomek	k1gMnPc2	potomek
rodu	rod	k1gInSc2	rod
Colloredo-Mansfeld	Colloredo-Mansfeld	k1gMnSc1	Colloredo-Mansfeld
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
zámek	zámek	k1gInSc1	zámek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
unikátních	unikátní	k2eAgFnPc2d1	unikátní
sbírek	sbírka	k1gFnPc2	sbírka
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	k9	by
jeho	jeho	k3xOp3gMnSc7	jeho
autorem	autor	k1gMnSc7	autor
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Bosch	Bosch	kA	Bosch
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc4	zámek
Opočno	Opočno	k1gNnSc4	Opočno
zařazen	zařazen	k2eAgMnSc1d1	zařazen
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
národních	národní	k2eAgFnPc2d1	národní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
historie	historie	k1gFnSc1	historie
hradu	hrad	k1gInSc2	hrad
===	===	k?	===
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
archeologickým	archeologický	k2eAgInPc3d1	archeologický
výzkumům	výzkum	k1gInPc3	výzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgNnSc1	první
osídlení	osídlení	k1gNnSc1	osídlení
prostoru	prostor	k1gInSc2	prostor
datováno	datovat	k5eAaImNgNnS	datovat
do	do	k7c2	do
sklonku	sklonek	k1gInSc2	sklonek
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
a	a	k8xC	a
období	období	k1gNnSc2	období
tzv.	tzv.	kA	tzv.
popelnicových	popelnicový	k2eAgFnPc2d1	popelnicová
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Stáří	stáří	k1gNnSc1	stáří
nálezů	nález	k1gInPc2	nález
bylo	být	k5eAaImAgNnS	být
je	on	k3xPp3gNnSc4	on
odhadováno	odhadován	k2eAgNnSc4d1	odhadováno
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Opočně	Opočno	k1gNnSc6	Opočno
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1068	[number]	k4	1068
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
líčením	líčení	k1gNnSc7	líčení
událostí	událost	k1gFnPc2	událost
na	na	k7c6	na
Dobeníně	Dobenína	k1gFnSc6	Dobenína
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
záznamem	záznam	k1gInSc7	záznam
potvrzujícím	potvrzující	k2eAgInSc7d1	potvrzující
existenci	existence	k1gFnSc4	existence
Opočna	Opočno	k1gNnSc2	Opočno
je	být	k5eAaImIp3nS	být
Vyšehradská	vyšehradský	k2eAgFnSc1d1	Vyšehradská
listina	listina	k1gFnSc1	listina
vydaná	vydaný	k2eAgFnSc1d1	vydaná
knížetem	kníže	k1gMnSc7	kníže
Soběslavem	Soběslav	k1gMnSc7	Soběslav
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1130	[number]	k4	1130
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
kníže	kníže	k1gMnSc1	kníže
Soběslav	Soběslav	k1gMnSc1	Soběslav
daroval	darovat	k5eAaPmAgMnS	darovat
plat	plat	k1gInSc4	plat
z	z	k7c2	z
Opočna	Opočno	k1gNnSc2	Opočno
Vyšehradské	vyšehradský	k2eAgFnSc6d1	Vyšehradská
kapitule	kapitula	k1gFnSc6	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
Nedoloženými	doložený	k2eNgFnPc7d1	nedoložená
prvními	první	k4xOgFnPc7	první
majiteli	majitel	k1gMnSc3	majitel
hradiště	hradiště	k1gNnPc1	hradiště
Opočen	Opočno	k1gNnPc2	Opočno
byli	být	k5eAaImAgMnP	být
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
995	[number]	k4	995
Slavníkovci	Slavníkovec	k1gInPc7	Slavníkovec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
až	až	k9	až
do	do	k7c2	do
polovina	polovina	k1gFnSc1	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
knížecí	knížecí	k2eAgInSc4d1	knížecí
rod	rod	k1gInSc4	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
známým	známý	k2eAgMnSc7d1	známý
majitelem	majitel	k1gMnSc7	majitel
Opočna	Opočno	k1gNnSc2	Opočno
byl	být	k5eAaImAgMnS	být
Čeněk	Čeněk	k1gMnSc1	Čeněk
z	z	k7c2	z
Potštejna	Potštejn	k1gInSc2	Potštejn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1341	[number]	k4	1341
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
doloženým	doložený	k2eAgMnSc7d1	doložený
majitelem	majitel	k1gMnSc7	majitel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1359	[number]	k4	1359
Mutina	Mutina	k1gFnSc1	Mutina
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Dobrušky	Dobruška	k1gFnSc2	Dobruška
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
Opočna	Opočno	k1gNnSc2	Opočno
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
rodových	rodový	k2eAgFnPc2d1	rodová
větví	větev	k1gFnPc2	větev
Drslaviců	Drslavice	k1gMnPc2	Drslavice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
majitelem	majitel	k1gMnSc7	majitel
hradu	hrad	k1gInSc2	hrad
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Sezema	Sezemum	k1gNnSc2	Sezemum
z	z	k7c2	z
Dobrušky	Dobruška	k1gFnSc2	Dobruška
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Sezemy	Sezema	k1gFnSc2	Sezema
se	se	k3xPyFc4	se
majiteli	majitel	k1gMnPc7	majitel
Opočna	Opočno	k1gNnSc2	Opočno
stali	stát	k5eAaPmAgMnP	stát
jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc4	dva
synové	syn	k1gMnPc1	syn
Štěpán	Štěpán	k1gMnSc1	Štěpán
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Dobrušky	Dobruška	k1gFnSc2	Dobruška
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
panství	panství	k1gNnPc2	panství
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1376	[number]	k4	1376
a	a	k8xC	a
Opočno	Opočno	k1gNnSc4	Opočno
získal	získat	k5eAaPmAgMnS	získat
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1390	[number]	k4	1390
až	až	k9	až
1397	[number]	k4	1397
byl	být	k5eAaImAgInS	být
majitelem	majitel	k1gMnSc7	majitel
Půta	Půta	k1gMnSc1	Půta
starší	starší	k1gMnSc1	starší
z	z	k7c2	z
Častolovic	Častolovice	k1gFnPc2	Častolovice
a	a	k8xC	a
poté	poté	k6eAd1	poté
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Půta	Půta	k1gMnSc1	Půta
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Koupí	koupě	k1gFnSc7	koupě
získal	získat	k5eAaPmAgMnS	získat
Opočno	Opočno	k1gNnSc4	Opočno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1406	[number]	k4	1406
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
purkrabí	purkrabí	k1gMnPc1	purkrabí
Jan	Jan	k1gMnSc1	Jan
Krušina	krušina	k1gFnSc1	krušina
z	z	k7c2	z
Lichtenburka	Lichtenburek	k1gMnSc2	Lichtenburek
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
Opočna	Opočno	k1gNnSc2	Opočno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1407	[number]	k4	1407
(	(	kIx(	(
<g/>
či	či	k8xC	či
1408	[number]	k4	1408
<g/>
)	)	kIx)	)
až	až	k9	až
1413	[number]	k4	1413
Opočno	Opočno	k1gNnSc1	Opočno
drželi	držet	k5eAaImAgMnP	držet
jeho	jeho	k3xOp3gMnPc1	jeho
tři	tři	k4xCgMnPc4	tři
synové	syn	k1gMnPc1	syn
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
husitských	husitský	k2eAgFnPc2d1	husitská
bouří	bouř	k1gFnPc2	bouř
se	se	k3xPyFc4	se
jako	jako	k9	jako
držitel	držitel	k1gMnSc1	držitel
Opočna	Opočno	k1gNnSc2	Opočno
nejčastěji	často	k6eAd3	často
uvádí	uvádět	k5eAaImIp3nS	uvádět
potomek	potomek	k1gMnSc1	potomek
Štěpána	Štěpána	k1gFnSc1	Štěpána
z	z	k7c2	z
Dobrušky	Dobruška	k1gFnSc2	Dobruška
a	a	k8xC	a
Opočna	Opočno	k1gNnSc2	Opočno
Jan	Jan	k1gMnSc1	Jan
Městecký	městecký	k2eAgMnSc1d1	městecký
z	z	k7c2	z
Opočna	Opočno	k1gNnSc2	Opočno
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
ještě	ještě	k9	ještě
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1415	[number]	k4	1415
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
nejprve	nejprve	k6eAd1	nejprve
husitou	husita	k1gMnSc7	husita
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ale	ale	k9	ale
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
opačnou	opačný	k2eAgFnSc4d1	opačná
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1421	[number]	k4	1421
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
o	o	k7c4	o
dobytí	dobytí	k1gNnSc4	dobytí
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1422	[number]	k4	1422
bylo	být	k5eAaImAgNnS	být
Opočno	Opočno	k1gNnSc1	Opočno
neúspěšně	úspěšně	k6eNd1	úspěšně
opět	opět	k6eAd1	opět
obleženo	oblehnout	k5eAaPmNgNnS	oblehnout
husity	husita	k1gMnPc7	husita
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Korybutoviče	Korybutovič	k1gMnSc2	Korybutovič
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1425	[number]	k4	1425
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
Opočno	Opočno	k1gNnSc1	Opočno
dobyt	dobyt	k2eAgInSc1d1	dobyt
a	a	k8xC	a
pobořen	pobořen	k2eAgInSc1d1	pobořen
sirotky	sirotek	k1gMnPc7	sirotek
jako	jako	k8xC	jako
odveta	odveta	k1gFnSc1	odveta
za	za	k7c4	za
vyslání	vyslání	k1gNnSc4	vyslání
nájemného	nájemné	k1gNnSc2	nájemné
vraha	vrah	k1gMnSc2	vrah
proti	proti	k7c3	proti
Janu	Jan	k1gMnSc3	Jan
Žižkovi	Žižka	k1gMnSc3	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1431	[number]	k4	1431
Jan	Jan	k1gMnSc1	Jan
Městecký	městecký	k2eAgInSc1d1	městecký
zemřel	zemřít	k5eAaPmAgInS	zemřít
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
vymřel	vymřít	k5eAaPmAgInS	vymřít
i	i	k9	i
rod	rod	k1gInSc1	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Dobrušky	Dobruška	k1gFnSc2	Dobruška
a	a	k8xC	a
Opočna	Opočno	k1gNnSc2	Opočno
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
Opočna	Opočno	k1gNnSc2	Opočno
je	být	k5eAaImIp3nS	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1455	[number]	k4	1455
nejasné	jasný	k2eNgFnSc2d1	nejasná
<g/>
:	:	kIx,	:
držitelem	držitel	k1gMnSc7	držitel
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
Jan	Jan	k1gMnSc1	Jan
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
a	a	k8xC	a
na	na	k7c6	na
Vízmburku	Vízmburk	k1gInSc6	Vízmburk
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Petr	Petr	k1gMnSc1	Petr
Svojše	Svojš	k1gInSc2	Svojš
ze	z	k7c2	z
Zahrádky	zahrádka	k1gFnSc2	zahrádka
a	a	k8xC	a
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Suchan	Suchan	k1gMnSc1	Suchan
z	z	k7c2	z
Libouně	Libouna	k1gFnSc6	Libouna
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1451	[number]	k4	1451
přešlo	přejít	k5eAaPmAgNnS	přejít
Opočno	Opočno	k1gNnSc1	Opočno
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
Ladislava	Ladislav	k1gMnSc2	Ladislav
Pohrobka	pohrobek	k1gMnSc2	pohrobek
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1455	[number]	k4	1455
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
a	a	k8xC	a
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
další	další	k2eAgFnSc2d1	další
verze	verze	k1gFnSc2	verze
získal	získat	k5eAaPmAgMnS	získat
Opočno	Opočno	k1gNnSc4	Opočno
Jiří	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
po	po	k7c6	po
trestné	trestný	k2eAgFnSc6d1	trestná
výpravě	výprava	k1gFnSc6	výprava
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
opočenského	opočenský	k2eAgNnSc2d1	opočenské
panství	panství	k1gNnSc2	panství
vypuzen	vypudit	k5eAaPmNgMnS	vypudit
loupeživý	loupeživý	k2eAgMnSc1d1	loupeživý
rytíř	rytíř	k1gMnSc1	rytíř
Petr	Petr	k1gMnSc1	Petr
Svojše	Svojše	k1gFnSc1	Svojše
ze	z	k7c2	z
Zahrádky	zahrádka	k1gFnSc2	zahrádka
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1455	[number]	k4	1455
se	se	k3xPyFc4	se
pánem	pán	k1gMnSc7	pán
na	na	k7c6	na
Opočně	Opočno	k1gNnSc6	Opočno
stal	stát	k5eAaPmAgMnS	stát
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Valečova	Valečův	k2eAgInSc2d1	Valečův
a	a	k8xC	a
Kněžmostu	Kněžmost	k1gInSc2	Kněžmost
a	a	k8xC	a
poté	poté	k6eAd1	poté
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Viktorín	Viktorín	k1gMnSc1	Viktorín
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1481	[number]	k4	1481
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bez	bez	k7c2	bez
mužských	mužský	k2eAgMnPc2d1	mužský
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Dědicem	dědic	k1gMnSc7	dědic
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Samuel	Samuel	k1gMnSc1	Samuel
z	z	k7c2	z
Hrádku	Hrádok	k1gInSc2	Hrádok
a	a	k8xC	a
Valečova	Valečův	k2eAgMnSc2d1	Valečův
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1487	[number]	k4	1487
koupí	koupit	k5eAaPmIp3nS	koupit
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Janovic	Janovice	k1gFnPc2	Janovice
a	a	k8xC	a
Petršpurka	Petršpurka	k1gFnSc1	Petršpurka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1493	[number]	k4	1493
až	až	k9	až
1495	[number]	k4	1495
získal	získat	k5eAaPmAgInS	získat
hrad	hrad	k1gInSc1	hrad
Opočno	Opočno	k1gNnSc1	Opočno
a	a	k8xC	a
následně	následně	k6eAd1	následně
za	za	k7c4	za
20	[number]	k4	20
000	[number]	k4	000
uherských	uherský	k2eAgFnPc2d1	uherská
zlatých	zlatá	k1gFnPc2	zlatá
celé	celý	k2eAgNnSc4d1	celé
opočenské	opočenský	k2eAgNnSc4d1	opočenské
panství	panství	k1gNnSc4	panství
včetně	včetně	k7c2	včetně
Dobrušky	Dobruška	k1gFnSc2	Dobruška
rod	rod	k1gInSc1	rod
Trčků	Trčka	k1gMnPc2	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Trčkové	Trčková	k1gFnSc2	Trčková
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
===	===	k?	===
</s>
</p>
<p>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
mladší	mladý	k2eAgMnSc1d2	mladší
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
sídlil	sídlit	k5eAaImAgInS	sídlit
na	na	k7c6	na
Opočně	Opočno	k1gNnSc6	Opočno
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bez	bez	k7c2	bez
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc4	panství
významně	významně	k6eAd1	významně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
dalším	další	k2eAgInSc7d1	další
dokupem	dokup	k1gInSc7	dokup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
panství	panství	k1gNnSc2	panství
zdědila	zdědit	k5eAaPmAgFnS	zdědit
jeho	jeho	k3xOp3gFnSc1	jeho
teta	teta	k1gFnSc1	teta
Johanka	Johanka	k1gFnSc1	Johanka
Kroměšínová	Kroměšínová	k1gFnSc1	Kroměšínová
z	z	k7c2	z
Březovic	Březovice	k1gFnPc2	Březovice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
majetek	majetek	k1gInSc4	majetek
postoupila	postoupit	k5eAaPmAgFnS	postoupit
svým	svůj	k3xOyFgMnPc3	svůj
synům	syn	k1gMnPc3	syn
Janovi	Jan	k1gMnSc6	Jan
<g/>
,	,	kIx,	,
Vilémovi	Vilém	k1gMnSc6	Vilém
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zdeňkovi	Zdeněk	k1gMnSc3	Zdeněk
<g/>
,	,	kIx,	,
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
a	a	k8xC	a
Mikulášovi	Mikuláš	k1gMnSc3	Mikuláš
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
však	však	k9	však
zemřela	zemřít	k5eAaPmAgFnS	zemřít
ještě	ještě	k6eAd1	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Správy	správa	k1gFnSc2	správa
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
nejstarší	starý	k2eAgMnSc1d3	nejstarší
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
panství	panství	k1gNnSc4	panství
dále	daleko	k6eAd2	daleko
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
první	první	k4xOgFnPc4	první
přestavby	přestavba	k1gFnPc4	přestavba
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1533	[number]	k4	1533
získal	získat	k5eAaPmAgMnS	získat
opočenskou	opočenský	k2eAgFnSc4d1	Opočenská
část	část	k1gFnSc4	část
trčkovských	trčkovský	k2eAgInPc2d1	trčkovský
majetků	majetek	k1gInPc2	majetek
jeho	jeho	k3xOp3gFnSc4	jeho
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
mladší	mladý	k2eAgMnSc1d2	mladší
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
neměl	mít	k5eNaImAgMnS	mít
mužské	mužský	k2eAgMnPc4d1	mužský
potomky	potomek	k1gMnPc4	potomek
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1449	[number]	k4	1449
(	(	kIx(	(
<g/>
či	či	k8xC	či
1450	[number]	k4	1450
<g/>
)	)	kIx)	)
panství	panství	k1gNnSc6	panství
předal	předat	k5eAaPmAgMnS	předat
Vilémovi	Vilém	k1gMnSc3	Vilém
Trčkovi	Trčka	k1gMnSc3	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1560	[number]	k4	1560
<g/>
–	–	k?	–
<g/>
1567	[number]	k4	1567
v	v	k7c6	v
nákladné	nákladný	k2eAgFnSc6d1	nákladná
přestavbě	přestavba	k1gFnSc6	přestavba
hradu	hrad	k1gInSc2	hrad
na	na	k7c4	na
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1569	[number]	k4	1569
došlo	dojít	k5eAaPmAgNnS	dojít
kvůli	kvůli	k7c3	kvůli
Opočnu	Opočno	k1gNnSc3	Opočno
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
mezi	mezi	k7c4	mezi
rody	rod	k1gInPc4	rod
Trčků	Trčka	k1gMnPc2	Trčka
a	a	k8xC	a
Žerotínů	Žerotín	k1gMnPc2	Žerotín
(	(	kIx(	(
<g/>
Vilém	Vilém	k1gMnSc1	Vilém
odkázal	odkázat	k5eAaPmAgMnS	odkázat
Opočno	Opočno	k1gNnSc1	Opočno
sestře	sestra	k1gFnSc3	sestra
Veronice	Veronika	k1gFnSc3	Veronika
<g/>
,	,	kIx,	,
vdově	vdova	k1gFnSc6	vdova
po	po	k7c6	po
Karlovi	Karel	k1gMnSc6	Karel
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
urovnat	urovnat	k5eAaPmF	urovnat
až	až	k8xS	až
sňatkem	sňatek	k1gInSc7	sňatek
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
rody	rod	k1gInPc7	rod
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1581	[number]	k4	1581
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
sňatkem	sňatek	k1gInSc7	sňatek
se	se	k3xPyFc4	se
pánem	pán	k1gMnSc7	pán
na	na	k7c6	na
Opočně	Opočno	k1gNnSc6	Opočno
stal	stát	k5eAaPmAgMnS	stát
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Trčka	Trčka	k1gMnSc1	Trčka
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc4	panství
hodlal	hodlat	k5eAaImAgMnS	hodlat
předat	předat	k5eAaPmF	předat
svým	svůj	k3xOyFgMnSc7	svůj
čtyřem	čtyři	k4xCgMnPc3	čtyři
synům	syn	k1gMnPc3	syn
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
a	a	k8xC	a
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
Jindřicha	Jindřich	k1gMnSc4	Jindřich
(	(	kIx(	(
<g/>
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
<g/>
)	)	kIx)	)
z	z	k7c2	z
dědictví	dědictví	k1gNnSc2	dědictví
vyloučil	vyloučit	k5eAaPmAgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc1	panství
proto	proto	k8xC	proto
zdědil	zdědit	k5eAaPmAgMnS	zdědit
bratranec	bratranec	k1gMnSc1	bratranec
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
sice	sice	k8xC	sice
podporoval	podporovat	k5eAaImAgMnS	podporovat
vzbouřence	vzbouřenec	k1gMnSc4	vzbouřenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
porážce	porážka	k1gFnSc6	porážka
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
pomocí	pomocí	k7c2	pomocí
prosebných	prosebný	k2eAgMnPc2d1	prosebný
dopisů	dopis	k1gInPc2	dopis
a	a	k8xC	a
úplatků	úplatek	k1gInPc2	úplatek
předávaných	předávaný	k2eAgFnPc2d1	předávaná
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
Marií	Maria	k1gFnSc7	Maria
Magdalenou	Magdalena	k1gFnSc7	Magdalena
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
zachránit	zachránit	k5eAaPmF	zachránit
si	se	k3xPyFc3	se
život	život	k1gInSc4	život
a	a	k8xC	a
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
odpykal	odpykat	k5eAaPmAgInS	odpykat
domácím	domácí	k2eAgNnSc7d1	domácí
vězením	vězení	k1gNnSc7	vězení
a	a	k8xC	a
díky	díky	k7c3	díky
půjčkám	půjčka	k1gFnPc3	půjčka
císaři	císař	k1gMnSc3	císař
se	se	k3xPyFc4	se
směl	smět	k5eAaImAgMnS	smět
dokonce	dokonce	k9	dokonce
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
pobělohorských	pobělohorský	k2eAgFnPc6d1	pobělohorská
konfiskacích	konfiskace	k1gFnPc6	konfiskace
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Marie	Marie	k1gFnSc1	Marie
Magdalena	Magdalena	k1gFnSc1	Magdalena
udržovala	udržovat	k5eAaImAgFnS	udržovat
s	s	k7c7	s
protihabsburskými	protihabsburský	k2eAgMnPc7d1	protihabsburský
vzbouřenci	vzbouřenec	k1gMnPc7	vzbouřenec
kontakt	kontakt	k1gInSc4	kontakt
a	a	k8xC	a
v	v	k7c4	v
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
mocenský	mocenský	k2eAgInSc4d1	mocenský
vzestup	vzestup	k1gInSc4	vzestup
spojila	spojit	k5eAaPmAgFnS	spojit
osud	osud	k1gInSc4	osud
syna	syn	k1gMnSc2	syn
Adama	Adam	k1gMnSc2	Adam
Erdmana	Erdman	k1gMnSc2	Erdman
s	s	k7c7	s
Albrechtem	Albrecht	k1gMnSc7	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
.	.	kIx.	.
</s>
<s>
Albrecht	Albrecht	k1gMnSc1	Albrecht
byl	být	k5eAaImAgMnS	být
ovšem	ovšem	k9	ovšem
při	při	k7c6	při
spiknutí	spiknutí	k1gNnSc6	spiknutí
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
tam	tam	k6eAd1	tam
zahynul	zahynout	k5eAaPmAgMnS	zahynout
i	i	k9	i
Adam	Adam	k1gMnSc1	Adam
Erdman	Erdman	k1gMnSc1	Erdman
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
snažil	snažit	k5eAaImAgMnS	snažit
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
včetně	včetně	k7c2	včetně
Opočna	Opočno	k1gNnSc2	Opočno
přepsat	přepsat	k5eAaPmF	přepsat
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
dcery	dcera	k1gFnPc4	dcera
a	a	k8xC	a
vnučku	vnučka	k1gFnSc4	vnučka
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
statky	statek	k1gInPc1	statek
konfiskovány	konfiskován	k2eAgInPc1d1	konfiskován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Colloredo-Mansfeldové	Colloredo-Mansfeldové	k2eAgNnPc6d1	Colloredo-Mansfeldové
===	===	k?	===
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Colloredů	Collored	k1gMnPc2	Collored
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
a	a	k8xC	a
Vilém	Vilém	k1gMnSc1	Vilém
Colloredo-Walsee	Colloredo-Walsee	k1gInSc1	Colloredo-Walsee
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgInS	získat
Opočno	Opočno	k1gNnSc4	Opočno
do	do	k7c2	do
dědičného	dědičný	k2eAgNnSc2d1	dědičné
držení	držení	k1gNnSc2	držení
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1636	[number]	k4	1636
za	za	k7c4	za
51	[number]	k4	51
456	[number]	k4	456
rýnských	rýnský	k2eAgFnPc2d1	Rýnská
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Colloredo	Colloredo	k1gNnSc1	Colloredo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1638	[number]	k4	1638
stal	stát	k5eAaPmAgInS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
majitelem	majitel	k1gMnSc7	majitel
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
svobodný	svobodný	k2eAgMnSc1d1	svobodný
a	a	k8xC	a
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
<g/>
.	.	kIx.	.
</s>
<s>
Opočenský	opočenský	k2eAgInSc1d1	opočenský
zámek	zámek	k1gInSc1	zámek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
nedělitelného	dělitelný	k2eNgInSc2d1	nedělitelný
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Rudolfovi	Rudolf	k1gMnSc6	Rudolf
Opočno	Opočno	k1gNnSc1	Opočno
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
zanechal	zanechat	k5eAaPmAgInS	zanechat
však	však	k9	však
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
pouze	pouze	k6eAd1	pouze
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zděděný	zděděný	k2eAgInSc4d1	zděděný
majetek	majetek	k1gInSc4	majetek
převedla	převést	k5eAaPmAgFnS	převést
na	na	k7c4	na
Kamila	Kamil	k1gMnSc4	Kamil
Colloreda	Collored	k1gMnSc4	Collored
z	z	k7c2	z
další	další	k2eAgFnSc2d1	další
větve	větev	k1gFnSc2	větev
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
majetek	majetek	k1gInSc4	majetek
postoupil	postoupit	k5eAaPmAgMnS	postoupit
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
Karlu	Karel	k1gMnSc3	Karel
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
předal	předat	k5eAaPmAgMnS	předat
Jeronýmovi	Jeroným	k1gMnSc3	Jeroným
z	z	k7c2	z
další	další	k2eAgFnSc2d1	další
colloredovské	colloredovský	k2eAgFnSc2d1	colloredovský
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Jeroným	Jeroným	k1gMnSc1	Jeroným
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
provést	provést	k5eAaPmF	provést
řadu	řada	k1gFnSc4	řada
barokních	barokní	k2eAgFnPc2d1	barokní
úprav	úprava	k1gFnPc2	úprava
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
z	z	k7c2	z
konce	konec	k1gInSc2	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1726	[number]	k4	1726
zdědil	zdědit	k5eAaPmAgInS	zdědit
Opočno	Opočno	k1gNnSc4	Opočno
jeho	jeho	k3xOp3gMnSc1	jeho
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Rudolf	Rudolf	k1gMnSc1	Rudolf
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	on	k3xPp3gNnSc2	on
panování	panování	k1gNnSc2	panování
na	na	k7c6	na
Opočně	Opočno	k1gNnSc6	Opočno
zámek	zámek	k1gInSc1	zámek
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
Lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
Rudolf	Rudolf	k1gMnSc1	Rudolf
Josef	Josef	k1gMnSc1	Josef
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
zámku	zámek	k1gInSc2	zámek
i	i	k8xC	i
panství	panství	k1gNnSc2	panství
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
jeho	jeho	k3xOp3gMnSc1	jeho
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
František	František	k1gMnSc1	František
de	de	k?	de
Paula	Paul	k1gMnSc4	Paul
Gundakar	Gundakar	k1gInSc1	Gundakar
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
z	z	k7c2	z
Marií	Maria	k1gFnPc2	Maria
Isabellou	Isabellá	k1gFnSc4	Isabellá
z	z	k7c2	z
Mansfeldu	Mansfeld	k1gInSc2	Mansfeld
a	a	k8xC	a
oba	dva	k4xCgInPc1	dva
rody	rod	k1gInPc1	rod
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
spojily	spojit	k5eAaPmAgInP	spojit
a	a	k8xC	a
používaly	používat	k5eAaImAgInP	používat
název	název	k1gInSc4	název
Colloredo-Mansfeld	Colloredo-Mansfelda	k1gFnPc2	Colloredo-Mansfelda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Františkově	Františkův	k2eAgFnSc6d1	Františkova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
se	se	k3xPyFc4	se
Opočna	Opočno	k1gNnSc2	Opočno
ujal	ujmout	k5eAaPmAgMnS	ujmout
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Rudolf	Rudolf	k1gMnSc1	Rudolf
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Colloredo	Colloredo	k1gNnSc1	Colloredo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
úpravách	úprava	k1gFnPc6	úprava
zámku	zámek	k1gInSc2	zámek
a	a	k8xC	a
zejména	zejména	k9	zejména
zámeckého	zámecký	k2eAgInSc2d1	zámecký
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
opočenského	opočenský	k2eAgInSc2d1	opočenský
zámku	zámek	k1gInSc2	zámek
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
začátkem	začátkem	k7c2	začátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1813	[number]	k4	1813
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
jednání	jednání	k1gNnSc1	jednání
protinapoleonské	protinapoleonský	k2eAgFnSc2d1	protinapoleonská
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
pobýval	pobývat	k5eAaImAgMnS	pobývat
zde	zde	k6eAd1	zde
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
,	,	kIx,	,
pruský	pruský	k2eAgMnSc1d1	pruský
král	král	k1gMnSc1	král
Bedřich	Bedřich	k1gMnSc1	Bedřich
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
rakouský	rakouský	k2eAgMnSc1d1	rakouský
kancléř	kancléř	k1gMnSc1	kancléř
Metternich	Metternich	k1gMnSc1	Metternich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
a	a	k8xC	a
zámku	zámek	k1gInSc2	zámek
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
František	František	k1gMnSc1	František
II	II	kA	II
<g/>
.	.	kIx.	.
de	de	k?	de
Paula	Paul	k1gMnSc4	Paul
Gundakar	Gundakar	k1gInSc1	Gundakar
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
zanechal	zanechat	k5eAaPmAgMnS	zanechat
pouze	pouze	k6eAd1	pouze
dceru	dcera	k1gFnSc4	dcera
Vilemínu	Vilemína	k1gFnSc4	Vilemína
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc1	panství
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
ujal	ujmout	k5eAaPmAgMnS	ujmout
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Františkův	Františkův	k2eAgMnSc1d1	Františkův
bratranec	bratranec	k1gMnSc1	bratranec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
nejen	nejen	k6eAd1	nejen
vypořádat	vypořádat	k5eAaPmF	vypořádat
s	s	k7c7	s
obrovskými	obrovský	k2eAgInPc7d1	obrovský
dluhy	dluh	k1gInPc7	dluh
<g/>
,	,	kIx,	,
kterými	který	k3yQgMnPc7	který
bylo	být	k5eAaImAgNnS	být
panství	panství	k1gNnSc1	panství
zatíženo	zatížit	k5eAaPmNgNnS	zatížit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vyřešit	vyřešit	k5eAaPmF	vyřešit
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
potíže	potíž	k1gFnPc4	potíž
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
zánikem	zánik	k1gInSc7	zánik
roboty	robota	k1gFnSc2	robota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
se	se	k3xPyFc4	se
opočenským	opočenský	k2eAgMnSc7d1	opočenský
pánem	pán	k1gMnSc7	pán
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
..	..	k?	..
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1903	[number]	k4	1903
až	až	k9	až
1912	[number]	k4	1912
provedl	provést	k5eAaPmAgMnS	provést
další	další	k2eAgFnPc4d1	další
stavební	stavební	k2eAgFnPc4d1	stavební
úpravy	úprava	k1gFnPc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
omezenému	omezený	k2eAgNnSc3d1	omezené
zpřístupnění	zpřístupnění	k1gNnSc3	zpřístupnění
zámku	zámek	k1gInSc2	zámek
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
neměl	mít	k5eNaImAgMnS	mít
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
své	svůj	k3xOyFgMnPc4	svůj
čtyři	čtyři	k4xCgMnPc4	čtyři
synovce	synovec	k1gMnPc4	synovec
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
přenechal	přenechat	k5eAaPmAgInS	přenechat
rodinný	rodinný	k2eAgInSc4d1	rodinný
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Leopold	Leopold	k1gMnSc1	Leopold
Colloredo	Colloredo	k1gNnSc4	Colloredo
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
Opočno	Opočno	k1gNnSc4	Opočno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
Leopold	Leopold	k1gMnSc1	Leopold
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
příslušníky	příslušník	k1gMnPc7	příslušník
rodu	rod	k1gInSc2	rod
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
opočenský	opočenský	k2eAgInSc4d1	opočenský
zámek	zámek	k1gInSc4	zámek
uvalena	uvalen	k2eAgFnSc1d1	uvalena
nucená	nucený	k2eAgFnSc1d1	nucená
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byl	být	k5eAaImAgInS	být
zkonfiskován	zkonfiskovat	k5eAaPmNgInS	zkonfiskovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
okupace	okupace	k1gFnSc2	okupace
byl	být	k5eAaImAgInS	být
majetek	majetek	k1gInSc1	majetek
Colloredo-Mansfeldových	Colloredo-Mansfeldová	k1gFnPc2	Colloredo-Mansfeldová
zkonfiskován	zkonfiskován	k2eAgInSc1d1	zkonfiskován
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
tentokráte	tentokráte	k?	tentokráte
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Benešových	Benešových	k2eAgInPc2d1	Benešových
dekretů	dekret	k1gInPc2	dekret
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
předložení	předložení	k1gNnSc2	předložení
potvrzení	potvrzení	k1gNnSc4	potvrzení
o	o	k7c6	o
národní	národní	k2eAgFnSc6d1	národní
spolehlivosti	spolehlivost	k1gFnSc6	spolehlivost
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
učiněny	učiněn	k2eAgFnPc1d1	učiněna
výjimky	výjimka	k1gFnPc1	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
výjimky	výjimka	k1gFnSc2	výjimka
získal	získat	k5eAaPmAgInS	získat
rod	rod	k1gInSc1	rod
Colloredo-Mansfeldových	Colloredo-Mansfeldová	k1gFnPc2	Colloredo-Mansfeldová
zpět	zpět	k6eAd1	zpět
Zbiroh	Zbiroh	k1gInSc4	Zbiroh
<g/>
.	.	kIx.	.
</s>
<s>
Josefu	Josef	k1gMnSc3	Josef
Leopoldovi	Leopold	k1gMnSc3	Leopold
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
potvrzení	potvrzení	k1gNnSc1	potvrzení
o	o	k7c6	o
bezúhonnosti	bezúhonnost	k1gFnSc6	bezúhonnost
vydáno	vydat	k5eAaPmNgNnS	vydat
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
Opočno	Opočno	k1gNnSc4	Opočno
zůstal	zůstat	k5eAaPmAgInS	zůstat
rukách	ruka	k1gFnPc6	ruka
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spor	spor	k1gInSc1	spor
o	o	k7c6	o
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
Opočno	Opočno	k1gNnSc4	Opočno
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
státních	státní	k2eAgInPc2d1	státní
hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
zámků	zámek	k1gInPc2	zámek
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
plně	plně	k6eAd1	plně
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
byl	být	k5eAaImAgInS	být
převeden	převést	k5eAaPmNgInS	převést
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
Státního	státní	k2eAgInSc2d1	státní
památkového	památkový	k2eAgInSc2d1	památkový
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
požádali	požádat	k5eAaPmAgMnP	požádat
Kristina	Kristina	k1gFnSc1	Kristina
Colloredo-Mansfeldová	Colloredo-Mansfeldová	k1gFnSc1	Colloredo-Mansfeldová
a	a	k8xC	a
Jeroným	Jeroným	k1gMnSc1	Jeroným
Colloredo-Mansfeld	Colloredo-Mansfeld	k1gMnSc1	Colloredo-Mansfeld
o	o	k7c4	o
navrácení	navrácení	k1gNnSc4	navrácení
opočenského	opočenský	k2eAgInSc2d1	opočenský
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
restitučních	restituční	k2eAgInPc2d1	restituční
nároků	nárok	k1gInPc2	nárok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
jejich	jejich	k3xOp3gInPc4	jejich
nároky	nárok	k1gInPc4	nárok
neuznal	uznat	k5eNaPmAgInS	uznat
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
žalobu	žaloba	k1gFnSc4	žaloba
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
i	i	k9	i
Krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
i	i	k9	i
žalobu	žaloba	k1gFnSc4	žaloba
Colloredo-Mansfeldových	Colloredo-Mansfeldová	k1gFnPc2	Colloredo-Mansfeldová
proti	proti	k7c3	proti
Památkovému	památkový	k2eAgInSc3d1	památkový
úřadu	úřad	k1gInSc2	úřad
Pardubice	Pardubice	k1gInPc1	Pardubice
o	o	k7c6	o
vydání	vydání	k1gNnSc6	vydání
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
i	i	k9	i
Krajský	krajský	k2eAgInSc4d1	krajský
soud	soud	k1gInSc4	soud
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
však	však	k9	však
zamítavé	zamítavý	k2eAgInPc4d1	zamítavý
rozsudky	rozsudek	k1gInPc4	rozsudek
obou	dva	k4xCgInPc2	dva
soudů	soud	k1gInPc2	soud
zrušil	zrušit	k5eAaPmAgInS	zrušit
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
ČR	ČR	kA	ČR
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgInS	vrátit
případ	případ	k1gInSc1	případ
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
okresnímu	okresní	k2eAgInSc3d1	okresní
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2002	[number]	k4	2002
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
vydání	vydání	k1gNnSc6	vydání
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
pozemků	pozemek	k1gInPc2	pozemek
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgNnSc2	tento
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
rodině	rodina	k1gFnSc6	rodina
Colloredo-Mansfeldových	Colloredo-Mansfeldových	k2eAgInSc1d1	Colloredo-Mansfeldových
opočenský	opočenský	k2eAgInSc1d1	opočenský
zámek	zámek	k1gInSc1	zámek
vrácen	vrátit	k5eAaPmNgInS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
ale	ale	k8xC	ale
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
ČR	ČR	kA	ČR
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zrušil	zrušit	k5eAaPmAgInS	zrušit
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
o	o	k7c6	o
vrácení	vrácení	k1gNnSc6	vrácení
zámku	zámek	k1gInSc2	zámek
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
státu	stát	k1gInSc2	stát
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
i	i	k9	i
Krajský	krajský	k2eAgInSc4d1	krajský
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
předán	předat	k5eAaPmNgInS	předat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Správou	správa	k1gFnSc7	správa
zámku	zámek	k1gInSc2	zámek
Opočno	Opočno	k1gNnSc1	Opočno
je	být	k5eAaImIp3nS	být
pověřen	pověřen	k2eAgInSc1d1	pověřen
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
pracoviště	pracoviště	k1gNnSc1	pracoviště
Josefov	Josefov	k1gInSc1	Josefov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
si	se	k3xPyFc3	se
Dipl	Dipl	k1gMnSc1	Dipl
<g/>
.	.	kIx.	.
</s>
<s>
Ing.	ing.	kA	ing.
Jerome	Jerom	k1gInSc5	Jerom
Colloredo-Mansfeld	Colloredo-Mansfelda	k1gFnPc2	Colloredo-Mansfelda
odvezl	odvézt	k5eAaPmAgInS	odvézt
po	po	k7c6	po
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
dohodě	dohoda	k1gFnSc6	dohoda
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
20	[number]	k4	20
obrazů	obraz	k1gInPc2	obraz
vydaných	vydaný	k2eAgInPc2d1	vydaný
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
do	do	k7c2	do
jeho	jeho	k3xOp3gNnPc2	jeho
vlastnictví	vlastnictví	k1gNnPc2	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
si	se	k3xPyFc3	se
Kristina	Kristin	k2eAgFnSc1d1	Kristina
Colloredo-Mansfeldová	Colloredo-Mansfeldová	k1gFnSc1	Colloredo-Mansfeldová
odvezla	odvézt	k5eAaPmAgFnS	odvézt
dalších	další	k2eAgInPc2d1	další
34	[number]	k4	34
obrazů	obraz	k1gInPc2	obraz
vydaných	vydaný	k2eAgInPc2d1	vydaný
jí	jíst	k5eAaImIp3nS	jíst
do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
žádosti	žádost	k1gFnSc3	žádost
Kristiny	Kristin	k2eAgFnSc2d1	Kristina
Colloredo-Mansfeldové	Colloredo-Mansfeldová	k1gFnSc2	Colloredo-Mansfeldová
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
soudního	soudní	k2eAgNnSc2d1	soudní
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
předloženy	předložen	k2eAgInPc4d1	předložen
nové	nový	k2eAgInPc4d1	nový
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
povolil	povolit	k5eAaPmAgInS	povolit
obnovu	obnova	k1gFnSc4	obnova
soudního	soudní	k2eAgNnSc2d1	soudní
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
Krajský	krajský	k2eAgInSc4d1	krajský
soud	soud	k1gInSc4	soud
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
odvolání	odvolání	k1gNnSc4	odvolání
Národního	národní	k2eAgInSc2d1	národní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
proti	proti	k7c3	proti
udělenému	udělený	k2eAgNnSc3d1	udělené
povolení	povolení	k1gNnSc3	povolení
obnovy	obnova	k1gFnSc2	obnova
řízení	řízení	k1gNnSc2	řízení
restituční	restituční	k2eAgFnSc2d1	restituční
kauzy	kauza	k1gFnSc2	kauza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
zahájil	zahájit	k5eAaPmAgInS	zahájit
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
projednávání	projednávání	k1gNnSc2	projednávání
restitučního	restituční	k2eAgInSc2d1	restituční
sporu	spor	k1gInSc2	spor
o	o	k7c4	o
nemovitý	movitý	k2eNgInSc4d1	nemovitý
majetek	majetek	k1gInSc4	majetek
spravovaný	spravovaný	k2eAgInSc4d1	spravovaný
Národním	národní	k2eAgInSc7d1	národní
památkovým	památkový	k2eAgInSc7d1	památkový
ústavem	ústav	k1gInSc7	ústav
na	na	k7c6	na
Státním	státní	k2eAgInSc6d1	státní
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Opočně	Opočno	k1gNnSc6	Opočno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
tento	tento	k3xDgInSc4	tento
soud	soud	k1gInSc4	soud
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zámek	zámek	k1gInSc1	zámek
patří	patřit	k5eAaImIp3nS	patřit
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
nový	nový	k2eAgMnSc1d1	nový
advokát	advokát	k1gMnSc1	advokát
Kristiny	Kristin	k2eAgFnSc2d1	Kristina
Colloredo-Mansfeldové	Colloredo-Mansfeldová	k1gFnSc2	Colloredo-Mansfeldová
<g/>
,	,	kIx,	,
JUDr.	JUDr.	kA	JUDr.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Nahodil	Nahodil	k1gMnSc1	Nahodil
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
projednávání	projednávání	k1gNnSc2	projednávání
tohoto	tento	k3xDgInSc2	tento
restitučního	restituční	k2eAgInSc2d1	restituční
případu	případ	k1gInSc2	případ
jako	jako	k8xS	jako
důkaz	důkaz	k1gInSc1	důkaz
předložil	předložit	k5eAaPmAgInS	předložit
znalecký	znalecký	k2eAgInSc4d1	znalecký
posudek	posudek	k1gInSc4	posudek
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
nacismus	nacismus	k1gInSc1	nacismus
<g/>
,	,	kIx,	,
neonacismus	neonacismus	k1gInSc1	neonacismus
<g/>
,	,	kIx,	,
fašismus	fašismus	k1gInSc1	fašismus
<g/>
,	,	kIx,	,
neofašismus	neofašismus	k1gInSc1	neofašismus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jehož	jehož	k3xOyRp3gInPc2	jehož
závěrů	závěr	k1gInPc2	závěr
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
dokumenty	dokument	k1gInPc1	dokument
německých	německý	k2eAgMnPc2d1	německý
konfiskačních	konfiskační	k2eAgMnPc2d1	konfiskační
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
zpráva	zpráva	k1gFnSc1	zpráva
královéhradecké	královéhradecký	k2eAgFnPc1d1	Královéhradecká
rezidentury	rezidentura	k1gFnPc1	rezidentura
Tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
-	-	kIx~	-
Gestapa	gestapo	k1gNnSc2	gestapo
<g/>
)	)	kIx)	)
zřetelné	zřetelný	k2eAgInPc4d1	zřetelný
motivy	motiv	k1gInPc4	motiv
rasové	rasový	k2eAgFnSc2d1	rasová
perzekuce	perzekuce	k1gFnSc2	perzekuce
vůči	vůči	k7c3	vůči
rodině	rodina	k1gFnSc3	rodina
Colloredo-Mansfeldových	Colloredo-Mansfeldová	k1gFnPc2	Colloredo-Mansfeldová
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
po	po	k7c6	po
výslechu	výslech	k1gInSc6	výslech
autora	autor	k1gMnSc2	autor
tohoto	tento	k3xDgInSc2	tento
znaleckého	znalecký	k2eAgInSc2d1	znalecký
posudku	posudek	k1gInSc2	posudek
<g/>
,	,	kIx,	,
znalce	znalec	k1gMnSc2	znalec
PhDr.	PhDr.	kA	PhDr.
Jana	Jan	k1gMnSc2	Jan
Borise	Boris	k1gMnSc2	Boris
Uhlíře	Uhlíř	k1gMnSc2	Uhlíř
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
tento	tento	k3xDgInSc4	tento
rozsudek	rozsudek	k1gInSc4	rozsudek
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
žalobkyni	žalobkyně	k1gFnSc4	žalobkyně
se	se	k3xPyFc4	se
rasový	rasový	k2eAgInSc4d1	rasový
motiv	motiv	k1gInSc4	motiv
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
začal	začít	k5eAaPmAgInS	začít
Krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
projednávat	projednávat	k5eAaImF	projednávat
odvolání	odvolání	k1gNnSc4	odvolání
Kristiny	Kristina	k1gFnSc2	Kristina
Colloredo-Mansfeld	Colloredo-Mansfeld	k1gMnSc1	Colloredo-Mansfeld
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
skončilo	skončit	k5eAaPmAgNnS	skončit
zamítnutím	zamítnutí	k1gNnSc7	zamítnutí
odvolání	odvolání	k1gNnSc1	odvolání
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2016	[number]	k4	2016
uspěl	uspět	k5eAaPmAgMnS	uspět
Jerome	Jerom	k1gInSc5	Jerom
Colloredo-Mansfeld	Colloredo-Mansfelda	k1gFnPc2	Colloredo-Mansfelda
u	u	k7c2	u
Evropského	evropský	k2eAgInSc2d1	evropský
soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
ČR	ČR	kA	ČR
o	o	k7c6	o
vydání	vydání	k1gNnSc6	vydání
mobiliáře	mobiliář	k1gInSc2	mobiliář
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
když	když	k8xS	když
soud	soud	k1gInSc1	soud
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
...	...	k?	...
že	že	k8xS	že
české	český	k2eAgInPc1d1	český
soudy	soud	k1gInPc1	soud
porušily	porušit	k5eAaPmAgInP	porušit
jeho	on	k3xPp3gInSc2	on
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
spravedlivé	spravedlivý	k2eAgNnSc4d1	spravedlivé
soudní	soudní	k2eAgNnSc4d1	soudní
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
zamítnuto	zamítnut	k2eAgNnSc1d1	zamítnuto
dovolání	dovolání	k1gNnSc1	dovolání
Kristiny	Kristin	k2eAgFnSc2d1	Kristina
Colloredo-Mansfeldové	Colloredo-Mansfeldová	k1gFnSc2	Colloredo-Mansfeldová
proti	proti	k7c3	proti
verdiktu	verdikt	k1gInSc3	verdikt
Krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Okresního	okresní	k2eAgInSc2d1	okresní
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
Kristina	Kristin	k2eAgFnSc1d1	Kristina
Colloredo-Mansfeldová	Colloredo-Mansfeldová	k1gFnSc1	Colloredo-Mansfeldová
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2017	[number]	k4	2017
podala	podat	k5eAaPmAgFnS	podat
stížnost	stížnost	k1gFnSc1	stížnost
u	u	k7c2	u
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
ČR	ČR	kA	ČR
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
restituce	restituce	k1gFnSc1	restituce
nemovitého	movitý	k2eNgInSc2d1	nemovitý
majetku	majetek	k1gInSc2	majetek
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Opočně	Opočno	k1gNnSc6	Opočno
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
Evropského	evropský	k2eAgInSc2d1	evropský
soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
se	se	k3xPyFc4	se
Kristina	Kristin	k2eAgFnSc1d1	Kristina
Colloredo-Mansfeldová	Colloredo-Mansfeldová	k1gFnSc1	Colloredo-Mansfeldová
domáhala	domáhat	k5eAaImAgFnS	domáhat
obnovy	obnova	k1gFnSc2	obnova
soudního	soudní	k2eAgNnSc2d1	soudní
řízení	řízení	k1gNnSc2	řízení
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
sporu	spor	k1gInSc6	spor
<g/>
.	.	kIx.	.
</s>
<s>
Poukázala	poukázat	k5eAaPmAgFnS	poukázat
přitom	přitom	k6eAd1	přitom
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
ČR	ČR	kA	ČR
nedostalo	dostat	k5eNaPmAgNnS	dostat
spravedlivého	spravedlivý	k2eAgInSc2d1	spravedlivý
soudu	soud	k1gInSc2	soud
podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
6	[number]	k4	6
Evropské	evropský	k2eAgFnSc2d1	Evropská
úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
lidských	lidský	k2eAgNnPc6d1	lidské
právech	právo	k1gNnPc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
soud	soud	k1gInSc1	soud
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
stížnost	stížnost	k1gFnSc1	stížnost
není	být	k5eNaImIp3nS	být
zjevně	zjevně	k6eAd1	zjevně
nepodložená	podložený	k2eNgFnSc1d1	nepodložená
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
ČR	ČR	kA	ČR
verdikt	verdikt	k1gInSc1	verdikt
Evropské	evropský	k2eAgFnSc2d1	Evropská
soudu	soud	k1gInSc3	soud
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
vyjádření	vyjádření	k1gNnSc2	vyjádření
obnovit	obnovit	k5eAaPmF	obnovit
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
soud	soud	k1gInSc1	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
dále	daleko	k6eAd2	daleko
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
českých	český	k2eAgInPc2d1	český
soudů	soud	k1gInPc2	soud
se	se	k3xPyFc4	se
opírala	opírat	k5eAaImAgFnS	opírat
o	o	k7c4	o
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
československého	československý	k2eAgNnSc2d1	Československé
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
Opočno	Opočno	k1gNnSc1	Opočno
včetně	včetně	k7c2	včetně
mobiliáře	mobiliář	k1gInSc2	mobiliář
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
za	za	k7c4	za
státní	státní	k2eAgInSc4d1	státní
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dokument	dokument	k1gInSc1	dokument
ale	ale	k9	ale
nebyl	být	k5eNaImAgInS	být
zahrnut	zahrnout	k5eAaPmNgMnS	zahrnout
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
důkazů	důkaz	k1gInPc2	důkaz
a	a	k8xC	a
dědička	dědička	k1gFnSc1	dědička
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
nemohla	moct	k5eNaImAgFnS	moct
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
verdiktu	verdikt	k1gInSc6	verdikt
Evropský	evropský	k2eAgInSc1d1	evropský
soud	soud	k1gInSc1	soud
hodnotil	hodnotit	k5eAaImAgInS	hodnotit
procesní	procesní	k2eAgInSc1d1	procesní
průběh	průběh	k1gInSc1	průběh
řízení	řízení	k1gNnSc2	řízení
českých	český	k2eAgInPc2d1	český
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
nerozhodoval	rozhodovat	k5eNaImAgInS	rozhodovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
má	mít	k5eAaImIp3nS	mít
ČR	ČR	kA	ČR
majetek	majetek	k1gInSc4	majetek
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dispozice	dispozice	k1gFnSc1	dispozice
zámku	zámek	k1gInSc2	zámek
a	a	k8xC	a
přestavby	přestavba	k1gFnSc2	přestavba
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Opočnu	Opočno	k1gNnSc3	Opočno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1068	[number]	k4	1068
<g/>
,	,	kIx,	,
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
lokalitě	lokalita	k1gFnSc6	lokalita
nacházelo	nacházet	k5eAaImAgNnS	nacházet
hradiště	hradiště	k1gNnSc4	hradiště
plnící	plnící	k2eAgFnSc4d1	plnící
obrannou	obranný	k2eAgFnSc4d1	obranná
funkci	funkce	k1gFnSc4	funkce
proti	proti	k7c3	proti
polským	polský	k2eAgMnPc3d1	polský
knížatům	kníže	k1gMnPc3wR	kníže
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Kladské	kladský	k2eAgFnSc6d1	Kladská
cestě	cesta	k1gFnSc6	cesta
(	(	kIx(	(
<g/>
též	též	k9	též
Polské	polský	k2eAgFnSc6d1	polská
cestě	cesta	k1gFnSc6	cesta
<g/>
)	)	kIx)	)
spojující	spojující	k2eAgFnPc1d1	spojující
Čechy	Čechy	k1gFnPc1	Čechy
s	s	k7c7	s
Kladskem	Kladsko	k1gNnSc7	Kladsko
<g/>
.	.	kIx.	.
</s>
<s>
Opočno	Opočno	k1gNnSc1	Opočno
bylo	být	k5eAaImAgNnS	být
poslední	poslední	k2eAgNnSc1d1	poslední
opevnění	opevnění	k1gNnSc1	opevnění
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
hradiště	hradiště	k1gNnSc2	hradiště
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
archeologický	archeologický	k2eAgInSc4d1	archeologický
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
před	před	k7c7	před
dnešní	dnešní	k2eAgFnSc7d1	dnešní
Zámeckou	zámecký	k2eAgFnSc7d1	zámecká
jízdárnou	jízdárna	k1gFnSc7	jízdárna
nalezena	nalezen	k2eAgFnSc1d1	nalezena
kamenná	kamenný	k2eAgFnSc1d1	kamenná
plenta	plenta	k1gFnSc1	plenta
původního	původní	k2eAgNnSc2d1	původní
hradiště	hradiště	k1gNnSc2	hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Nalezeny	nalezen	k2eAgInPc1d1	nalezen
byly	být	k5eAaImAgInP	být
zbytky	zbytek	k1gInPc1	zbytek
srubové	srubový	k2eAgFnSc2d1	srubová
či	či	k8xC	či
prkenné	prkenný	k2eAgFnSc2d1	prkenná
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
píckami	pícka	k1gFnPc7	pícka
<g/>
,	,	kIx,	,
zlomky	zlomek	k1gInPc4	zlomek
hliněných	hliněný	k2eAgFnPc2d1	hliněná
nádob	nádoba	k1gFnPc2	nádoba
a	a	k8xC	a
železných	železný	k2eAgInPc2d1	železný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
hradiště	hradiště	k1gNnSc1	hradiště
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
požárem	požár	k1gInSc7	požár
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
val	val	k1gInSc1	val
a	a	k8xC	a
hradiště	hradiště	k1gNnSc1	hradiště
tak	tak	k6eAd1	tak
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
obrannou	obranný	k2eAgFnSc4d1	obranná
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
gotický	gotický	k2eAgInSc1d1	gotický
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
hradiště	hradiště	k1gNnSc2	hradiště
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1330	[number]	k4	1330
rodem	rod	k1gInSc7	rod
Drslaviců	Drslaviec	k1gInPc2	Drslaviec
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
ostrohu	ostroh	k1gInSc2	ostroh
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
a	a	k8xC	a
dispozice	dispozice	k1gFnSc1	dispozice
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
hradu	hrad	k1gInSc2	hrad
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
představou	představa	k1gFnSc7	představa
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
ukončen	ukončit	k5eAaPmNgInS	ukončit
obranným	obranný	k2eAgInSc7d1	obranný
příkopem	příkop	k1gInSc7	příkop
(	(	kIx(	(
<g/>
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Hradební	hradební	k2eAgFnSc6d1	hradební
ulici	ulice	k1gFnSc6	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
vstupovalo	vstupovat	k5eAaImAgNnS	vstupovat
hradní	hradní	k2eAgFnSc7d1	hradní
bránou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
vedla	vést	k5eAaImAgFnS	vést
podél	podél	k7c2	podél
obytných	obytný	k2eAgFnPc2d1	obytná
a	a	k8xC	a
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
končila	končit	k5eAaImAgFnS	končit
na	na	k7c6	na
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
nádvoří	nádvoří	k1gNnSc6	nádvoří
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
hradu	hrad	k1gInSc2	hrad
byla	být	k5eAaImAgFnS	být
i	i	k9	i
kaple	kaple	k1gFnSc1	kaple
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
sv.	sv.	kA	sv.
Ondřejovi	Ondřejův	k2eAgMnPc1d1	Ondřejův
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
zásahem	zásah	k1gInSc7	zásah
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
rozsah	rozsah	k1gInSc1	rozsah
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
vypálení	vypálení	k1gNnSc4	vypálení
a	a	k8xC	a
poboření	poboření	k1gNnSc4	poboření
Sirotky	sirotka	k1gFnSc2	sirotka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1425	[number]	k4	1425
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
částečná	částečný	k2eAgFnSc1d1	částečná
obnova	obnova	k1gFnSc1	obnova
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1480	[number]	k4	1480
až	až	k9	až
1488	[number]	k4	1488
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
středověkého	středověký	k2eAgInSc2d1	středověký
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
východního	východní	k2eAgInSc2d1	východní
svahu	svah	k1gInSc2	svah
vložená	vložený	k2eAgFnSc1d1	vložená
okrouhlá	okrouhlý	k2eAgFnSc1d1	okrouhlá
studniční	studniční	k2eAgFnSc1d1	studniční
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
arkýřem	arkýř	k1gInSc7	arkýř
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
doznala	doznat	k5eAaPmAgFnS	doznat
silných	silný	k2eAgFnPc2d1	silná
přestaveb	přestavba	k1gFnPc2	přestavba
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
původní	původní	k2eAgFnSc1d1	původní
funkce	funkce	k1gFnSc1	funkce
není	být	k5eNaImIp3nS	být
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
,	,	kIx,	,
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
studniční	studniční	k2eAgFnSc4d1	studniční
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc4	hrad
zásobován	zásobován	k2eAgInSc4d1	zásobován
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
možností	možnost	k1gFnSc7	možnost
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
čistě	čistě	k6eAd1	čistě
obranná	obranný	k2eAgFnSc1d1	obranná
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
tento	tento	k3xDgInSc1	tento
fortifikační	fortifikační	k2eAgInSc1d1	fortifikační
typ	typ	k1gInSc1	typ
nemá	mít	k5eNaImIp3nS	mít
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
obdoby	obdoba	k1gFnSc2	obdoba
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
měřítku	měřítko	k1gNnSc6	měřítko
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
datování	datování	k1gNnSc1	datování
není	být	k5eNaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významných	významný	k2eAgFnPc2d1	významná
úprav	úprava	k1gFnPc2	úprava
doznal	doznat	k5eAaPmAgMnS	doznat
hrad	hrad	k1gInSc4	hrad
za	za	k7c4	za
panství	panství	k1gNnSc4	panství
Trčků	Trčka	k1gMnPc2	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
první	první	k4xOgMnSc1	první
majitel	majitel	k1gMnSc1	majitel
<g/>
,	,	kIx,	,
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
mladší	mladý	k2eAgMnSc1d2	mladší
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
prováděl	provádět	k5eAaImAgMnS	provádět
stavební	stavební	k2eAgFnPc4d1	stavební
úpravy	úprava	k1gFnPc4	úprava
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
snahy	snaha	k1gFnSc2	snaha
vybudovat	vybudovat	k5eAaPmF	vybudovat
reprezentativní	reprezentativní	k2eAgNnSc1d1	reprezentativní
a	a	k8xC	a
správní	správní	k2eAgNnSc1d1	správní
centrum	centrum	k1gNnSc1	centrum
zvětšujícího	zvětšující	k2eAgInSc2d1	zvětšující
se	se	k3xPyFc4	se
panství	panství	k1gNnSc1	panství
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
jiným	jiný	k2eAgInSc7d1	jiný
například	například	k6eAd1	například
zbourání	zbourání	k1gNnPc4	zbourání
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
výstavba	výstavba	k1gFnSc1	výstavba
zadního	zadní	k2eAgInSc2d1	zadní
dílu	díl	k1gInSc2	díl
hradu	hrad	k1gInSc2	hrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přestavbách	přestavba	k1gFnPc6	přestavba
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1517	[number]	k4	1517
až	až	k9	až
1573	[number]	k4	1573
provedl	provést	k5eAaPmAgInS	provést
řadu	řada	k1gFnSc4	řada
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
goticko-renesančním	gotickoenesanční	k2eAgInSc6d1	goticko-renesanční
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
přestavby	přestavba	k1gFnSc2	přestavba
provedl	provést	k5eAaPmAgMnS	provést
Vilém	Vilém	k1gMnSc1	Vilém
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1560	[number]	k4	1560
až	až	k9	až
1567	[number]	k4	1567
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrad	hrad	k1gInSc4	hrad
přebudoval	přebudovat	k5eAaPmAgMnS	přebudovat
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
dostavěl	dostavět	k5eAaPmAgInS	dostavět
příjezdové	příjezdový	k2eAgNnSc4d1	příjezdové
křídlo	křídlo	k1gNnSc4	křídlo
a	a	k8xC	a
část	část	k1gFnSc4	část
západního	západní	k2eAgNnSc2d1	západní
křídla	křídlo	k1gNnSc2	křídlo
s	s	k7c7	s
arkádami	arkáda	k1gFnPc7	arkáda
a	a	k8xC	a
fasády	fasáda	k1gFnPc1	fasáda
byly	být	k5eAaImAgFnP	být
opatřeny	opatřit	k5eAaPmNgFnP	opatřit
sgrafitovou	sgrafitový	k2eAgFnSc7d1	sgrafitová
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přestavbě	přestavba	k1gFnSc3	přestavba
byl	být	k5eAaImAgMnS	být
Vilém	Vilém	k1gMnSc1	Vilém
Trčka	Trčka	k1gMnSc1	Trčka
inspirován	inspirován	k2eAgMnSc1d1	inspirován
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
Janově	Janov	k1gInSc6	Janov
s	s	k7c7	s
renesancí	renesance	k1gFnSc7	renesance
jako	jako	k8xC	jako
stavebním	stavební	k2eAgInSc7d1	stavební
stylem	styl	k1gInSc7	styl
seznámil	seznámit	k5eAaPmAgMnS	seznámit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
původní	původní	k2eAgFnSc4d1	původní
hradní	hradní	k2eAgFnSc4d1	hradní
zástavbu	zástavba	k1gFnSc4	zástavba
napojil	napojit	k5eAaPmAgMnS	napojit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
západě	západ	k1gInSc6	západ
nově	nově	k6eAd1	nově
vybudované	vybudovaný	k2eAgFnPc1d1	vybudovaná
zámecké	zámecký	k2eAgFnPc1d1	zámecká
prostory	prostora	k1gFnPc1	prostora
jejichž	jejichž	k3xOyRp3gFnPc2	jejichž
součástí	součást	k1gFnPc2	součást
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
monumentální	monumentální	k2eAgFnPc1d1	monumentální
arkády	arkáda	k1gFnPc1	arkáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
další	další	k2eAgInPc4d1	další
objekty	objekt	k1gInPc4	objekt
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
sakrálního	sakrální	k2eAgInSc2d1	sakrální
a	a	k8xC	a
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
rázu	ráz	k1gInSc2	ráz
jako	jako	k8xS	jako
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1567	[number]	k4	1567
výstavba	výstavba	k1gFnSc1	výstavba
kaple	kaple	k1gFnSc2	kaple
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
a	a	k8xC	a
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
trčkovská	trčkovský	k2eAgFnSc1d1	trčkovský
hrobka	hrobka	k1gFnSc1	hrobka
v	v	k7c6	v
renesančním	renesanční	k2eAgInSc6d1	renesanční
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1602	[number]	k4	1602
byla	být	k5eAaImAgFnS	být
Janem	Jan	k1gMnSc7	Jan
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Trčkou	Trčka	k1gMnSc7	Trčka
založena	založit	k5eAaPmNgFnS	založit
obdélná	obdélný	k2eAgFnSc1d1	obdélná
renesanční	renesanční	k2eAgFnSc1d1	renesanční
zahrada	zahrada	k1gFnSc1	zahrada
a	a	k8xC	a
započata	započat	k2eAgFnSc1d1	započata
stavba	stavba	k1gFnSc1	stavba
letohrádku	letohrádek	k1gInSc2	letohrádek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přístavbě	přístavba	k1gFnSc3	přístavba
míčovny	míčovna	k1gFnSc2	míčovna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
překlenula	překlenout	k5eAaPmAgFnS	překlenout
městský	městský	k2eAgInSc4d1	městský
příkop	příkop	k1gInSc4	příkop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
po	po	k7c6	po
zavraždění	zavraždění	k1gNnSc6	zavraždění
Jana	Jan	k1gMnSc2	Jan
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Adama	Adam	k1gMnSc2	Adam
Erdmana	Erdman	k1gMnSc2	Erdman
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
bylo	být	k5eAaImAgNnS	být
konfiskací	konfiskace	k1gFnSc7	konfiskace
ukončeno	ukončen	k2eAgNnSc4d1	ukončeno
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
Opočna	Opočno	k1gNnSc2	Opočno
rodem	rod	k1gInSc7	rod
Trčků	Trčka	k1gMnPc2	Trčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panství	panství	k1gNnSc1	panství
přešlo	přejít	k5eAaPmAgNnS	přejít
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
rodu	rod	k1gInSc2	rod
Colloredo	Colloredo	k1gNnSc4	Colloredo
z	z	k7c2	z
Wallsee	Wallse	k1gFnSc2	Wallse
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1690	[number]	k4	1690
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
včetně	včetně	k7c2	včetně
předzámčí	předzámčí	k1gNnSc2	předzámčí
zachvácen	zachvátit	k5eAaPmNgInS	zachvátit
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
požárem	požár	k1gInSc7	požár
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
zničil	zničit	k5eAaPmAgInS	zničit
horní	horní	k2eAgNnPc4d1	horní
patra	patro	k1gNnPc4	patro
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
střechy	střecha	k1gFnSc2	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
ale	ale	k9	ale
také	také	k9	také
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
přestavbu	přestavba	k1gFnSc4	přestavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
přestavba	přestavba	k1gFnSc1	přestavba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
celá	celý	k2eAgFnSc1d1	celá
nesla	nést	k5eAaImAgFnS	nést
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
fázích	fáze	k1gFnPc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
bylo	být	k5eAaImAgNnS	být
vystavěno	vystavěn	k2eAgNnSc1d1	vystavěno
jižní	jižní	k2eAgNnSc1d1	jižní
křídlo	křídlo	k1gNnSc1	křídlo
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
střecha	střecha	k1gFnSc1	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
fasád	fasáda	k1gFnPc2	fasáda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tak	tak	k6eAd1	tak
získaly	získat	k5eAaPmAgFnP	získat
současnou	současný	k2eAgFnSc4d1	současná
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
trojkřídlou	trojkřídlý	k2eAgFnSc7d1	trojkřídlá
<g/>
,	,	kIx,	,
dvoupatrovou	dvoupatrový	k2eAgFnSc7d1	dvoupatrová
budovou	budova	k1gFnSc7	budova
arkádového	arkádový	k2eAgInSc2d1	arkádový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
úpravách	úprava	k1gFnPc6	úprava
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podílel	podílet	k5eAaImAgMnS	podílet
stavitel	stavitel	k1gMnSc1	stavitel
Nicola	Nicola	k1gFnSc1	Nicola
Rossi	Ross	k1gMnSc6	Ross
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
přestavby	přestavba	k1gFnSc2	přestavba
dokončila	dokončit	k5eAaPmAgFnS	dokončit
stavbu	stavba	k1gFnSc4	stavba
hlavní	hlavní	k2eAgFnSc2d1	hlavní
budovy	budova	k1gFnSc2	budova
ve	v	k7c6	v
vrcholně	vrcholně	k6eAd1	vrcholně
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
Giovanni	Giovanň	k1gMnSc3	Giovanň
Battista	Battista	k1gMnSc1	Battista
Alliprandi	Alliprand	k1gMnPc1	Alliprand
<g/>
,	,	kIx,	,
k	k	k7c3	k
jehož	jehož	k3xOyRp3gInPc3	jehož
dílům	díl	k1gInPc3	díl
patří	patřit	k5eAaImIp3nS	patřit
také	také	k6eAd1	také
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
v	v	k7c6	v
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
křídle	křídlo	k1gNnSc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1716-1720	[number]	k4	1716-1720
byla	být	k5eAaImAgFnS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
tzv.	tzv.	kA	tzv.
úřednická	úřednický	k2eAgFnSc1d1	úřednická
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
nároží	nároží	k1gNnSc6	nároží
zámeckého	zámecký	k2eAgInSc2d1	zámecký
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
správní	správní	k2eAgFnSc2d1	správní
budovy	budova	k1gFnSc2	budova
panství	panství	k1gNnSc1	panství
a	a	k8xC	a
k	k	k7c3	k
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
arkád	arkáda	k1gFnPc2	arkáda
<g/>
.	.	kIx.	.
</s>
<s>
Úpravu	úprava	k1gFnSc4	úprava
doznala	doznat	k5eAaPmAgFnS	doznat
i	i	k9	i
studniční	studniční	k2eAgFnSc1d1	studniční
bašta	bašta	k1gFnSc1	bašta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
přestavbám	přestavba	k1gFnPc3	přestavba
docházelo	docházet	k5eAaImAgNnS	docházet
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnSc2d1	významná
klasicistní	klasicistní	k2eAgFnSc2d1	klasicistní
úpravy	úprava	k1gFnSc2	úprava
doznal	doznat	k5eAaPmAgMnS	doznat
zámecký	zámecký	k2eAgInSc4d1	zámecký
park	park	k1gInSc4	park
v	v	k7c6	v
dolních	dolní	k2eAgFnPc6d1	dolní
partiích	partie	k1gFnPc6	partie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgFnSc6d1	renesanční
části	část	k1gFnSc6	část
parku	park	k1gInSc2	park
přibyly	přibýt	k5eAaPmAgInP	přibýt
skleníky	skleník	k1gInPc1	skleník
a	a	k8xC	a
oranžérie	oranžérie	k1gFnPc1	oranžérie
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kostelem	kostel	k1gInSc7	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
hrobka	hrobka	k1gFnSc1	hrobka
rodu	rod	k1gInSc2	rod
Colloredů	Collored	k1gMnPc2	Collored
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
zámku	zámek	k1gInSc6	zámek
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
stavebním	stavební	k2eAgFnPc3d1	stavební
úpravám	úprava	k1gFnPc3	úprava
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1878	[number]	k4	1878
-	-	kIx~	-
1879	[number]	k4	1879
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
náročná	náročný	k2eAgFnSc1d1	náročná
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
arkád	arkáda	k1gFnPc2	arkáda
<g/>
,	,	kIx,	,
výměna	výměna	k1gFnSc1	výměna
vikýřů	vikýř	k1gInPc2	vikýř
a	a	k8xC	a
oprava	oprava	k1gFnSc1	oprava
chrličů	chrlič	k1gMnPc2	chrlič
<g/>
.	.	kIx.	.
</s>
<s>
Pokoje	pokoj	k1gInPc1	pokoj
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
patře	patro	k1gNnSc6	patro
západního	západní	k2eAgNnSc2d1	západní
křídla	křídlo	k1gNnSc2	křídlo
získaly	získat	k5eAaPmAgFnP	získat
štukovou	štukový	k2eAgFnSc4d1	štuková
výzdobu	výzdoba	k1gFnSc4	výzdoba
stropů	strop	k1gInPc2	strop
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
pětimetrová	pětimetrový	k2eAgFnSc1d1	pětimetrová
zeď	zeď	k1gFnSc1	zeď
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
vyhlídkové	vyhlídkový	k2eAgFnPc1d1	vyhlídková
terasa	terasa	k1gFnSc1	terasa
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
schodištěm	schodiště	k1gNnSc7	schodiště
propojena	propojit	k5eAaPmNgFnS	propojit
se	s	k7c7	s
zámeckým	zámecký	k2eAgInSc7d1	zámecký
parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
rekonstrukcím	rekonstrukce	k1gFnPc3	rekonstrukce
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1960	[number]	k4	1960
až	až	k6eAd1	až
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
obnovovány	obnovován	k2eAgFnPc1d1	obnovována
fasády	fasáda	k1gFnPc1	fasáda
a	a	k8xC	a
restaurovány	restaurován	k2eAgFnPc1d1	restaurována
a	a	k8xC	a
reinstalovány	reinstalován	k2eAgFnPc1d1	reinstalován
zámecké	zámecký	k2eAgFnPc1d1	zámecká
sbírky	sbírka	k1gFnPc1	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
tzv.	tzv.	kA	tzv.
Orientální	orientální	k2eAgFnPc4d1	orientální
zbrojnice	zbrojnice	k1gFnPc4	zbrojnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
až	až	k9	až
2000	[number]	k4	2000
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
restaurování	restaurování	k1gNnSc3	restaurování
arkád	arkáda	k1gFnPc2	arkáda
a	a	k8xC	a
k	k	k7c3	k
opravám	oprava	k1gFnPc3	oprava
střech	střecha	k1gFnPc2	střecha
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
samotná	samotný	k2eAgFnSc1d1	samotná
budova	budova	k1gFnSc1	budova
zámku	zámek	k1gInSc2	zámek
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
nádvoří	nádvoří	k1gNnSc1	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
křídlo	křídlo	k1gNnSc1	křídlo
je	být	k5eAaImIp3nS	být
nahrazeno	nahrazen	k2eAgNnSc1d1	nahrazeno
terasou	terasa	k1gFnSc7	terasa
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
park	park	k1gInSc4	park
a	a	k8xC	a
Orlické	orlický	k2eAgFnPc4d1	Orlická
hory	hora	k1gFnPc4	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sbírky	sbírka	k1gFnSc2	sbírka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1905	[number]	k4	1905
až	až	k9	až
1925	[number]	k4	1925
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úpravám	úprava	k1gFnPc3	úprava
interiérů	interiér	k1gInPc2	interiér
<g/>
;	;	kIx,	;
v	v	k7c6	v
rokokovém	rokokový	k2eAgInSc6d1	rokokový
a	a	k8xC	a
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
byl	být	k5eAaImAgInS	být
štukovou	štukový	k2eAgFnSc7d1	štuková
výzdobou	výzdoba	k1gFnSc7	výzdoba
vybaven	vybaven	k2eAgInSc1d1	vybaven
reprezentační	reprezentační	k2eAgInSc1d1	reprezentační
salón	salón	k1gInSc1	salón
a	a	k8xC	a
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
i	i	k9	i
rodové	rodový	k2eAgFnPc1d1	rodová
sbírky	sbírka	k1gFnPc1	sbírka
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
sem	sem	k6eAd1	sem
byly	být	k5eAaImAgFnP	být
převezeny	převézt	k5eAaPmNgFnP	převézt
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
s	s	k7c7	s
díly	díl	k1gInPc7	díl
českých	český	k2eAgMnPc2d1	český
<g/>
,	,	kIx,	,
italských	italský	k2eAgMnPc2d1	italský
a	a	k8xC	a
holandských	holandský	k2eAgMnPc2d1	holandský
mistrů	mistr	k1gMnPc2	mistr
(	(	kIx(	(
<g/>
nejstarším	starý	k2eAgInPc3d3	nejstarší
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
sbírky	sbírka	k1gFnSc2	sbírka
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Žerotínský	žerotínský	k2eAgInSc1d1	žerotínský
epitaf	epitaf	k1gInSc1	epitaf
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1575	[number]	k4	1575
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
pozornost	pozornost	k1gFnSc1	pozornost
budí	budit	k5eAaImIp3nS	budit
díla	dílo	k1gNnSc2	dílo
malířů	malíř	k1gMnPc2	malíř
tzv.	tzv.	kA	tzv.
neapolské	neapolský	k2eAgFnPc4d1	neapolská
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Umístěna	umístěn	k2eAgFnSc1d1	umístěna
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
také	také	k9	také
etnografická	etnografický	k2eAgFnSc1d1	etnografická
sbírka	sbírka	k1gFnSc1	sbírka
předmětů	předmět	k1gInPc2	předmět
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
sály	sál	k1gInPc1	sál
byly	být	k5eAaImAgInP	být
upraveny	upravit	k5eAaPmNgInP	upravit
pro	pro	k7c4	pro
sbírku	sbírka	k1gFnSc4	sbírka
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejrozsáhlejších	rozsáhlý	k2eAgFnPc2d3	nejrozsáhlejší
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
opočenský	opočenský	k2eAgInSc4d1	opočenský
zámek	zámek	k1gInSc4	zámek
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
převezena	převézt	k5eAaPmNgFnS	převézt
i	i	k9	i
tzv.	tzv.	kA	tzv.
francouzská	francouzský	k2eAgFnSc1d1	francouzská
knihovna	knihovna	k1gFnSc1	knihovna
čítající	čítající	k2eAgFnSc1d1	čítající
kolem	kolem	k7c2	kolem
12	[number]	k4	12
000	[number]	k4	000
svazků	svazek	k1gInPc2	svazek
a	a	k8xC	a
obsahující	obsahující	k2eAgNnPc1d1	obsahující
cenná	cenný	k2eAgNnPc1d1	cenné
bohemikální	bohemikální	k2eAgNnPc1d1	bohemikální
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kolowratsko-Colloredovská	Kolowratsko-Colloredovský	k2eAgFnSc1d1	Kolowratsko-Colloredovský
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
domnělý	domnělý	k2eAgInSc1d1	domnělý
obraz	obraz	k1gInSc1	obraz
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Bosche	Bosch	k1gMnSc2	Bosch
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
významná	významný	k2eAgFnSc1d1	významná
obrazová	obrazový	k2eAgFnSc1d1	obrazová
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
založená	založený	k2eAgFnSc1d1	založená
rodem	rod	k1gInSc7	rod
Kolowratů	Kolowrat	k1gMnPc2	Kolowrat
(	(	kIx(	(
<g/>
nejcennější	cenný	k2eAgNnSc1d3	nejcennější
je	být	k5eAaImIp3nS	být
pozdně	pozdně	k6eAd1	pozdně
gotický	gotický	k2eAgInSc1d1	gotický
portrét	portrét	k1gInSc1	portrét
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
<g/>
)	)	kIx)	)
a	a	k8xC	a
desítky	desítka	k1gFnPc1	desítka
dalších	další	k2eAgInPc2d1	další
obrazů	obraz	k1gInPc2	obraz
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
renesance	renesance	k1gFnSc2	renesance
až	až	k8xS	až
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
byly	být	k5eAaImAgInP	být
katalogizovány	katalogizovat	k5eAaImNgInP	katalogizovat
a	a	k8xC	a
vystaveny	vystavit	k5eAaPmNgInP	vystavit
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
katalog	katalog	k1gInSc4	katalog
napsala	napsat	k5eAaPmAgFnS	napsat
Olga	Olga	k1gFnSc1	Olga
Kotková	Kotková	k1gFnSc1	Kotková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
restaurátoři	restaurátor	k1gMnPc1	restaurátor
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Bergerovi	Bergerův	k2eAgMnPc1d1	Bergerův
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
opočenském	opočenský	k2eAgInSc6d1	opočenský
zámku	zámek	k1gInSc6	zámek
je	být	k5eAaImIp3nS	být
ztracený	ztracený	k2eAgInSc4d1	ztracený
obraz	obraz	k1gInSc4	obraz
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Bosche	Bosch	k1gFnSc2	Bosch
Dvanáctiletý	dvanáctiletý	k2eAgMnSc1d1	dvanáctiletý
Ježíš	Ježíš	k1gMnSc1	Ježíš
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
tvrzení	tvrzení	k1gNnSc4	tvrzení
opírali	opírat	k5eAaImAgMnP	opírat
o	o	k7c4	o
výsledky	výsledek	k1gInPc4	výsledek
reflektografického	reflektografický	k2eAgNnSc2d1	reflektografický
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
nalezli	nalézt	k5eAaBmAgMnP	nalézt
zamalovanou	zamalovaný	k2eAgFnSc4d1	zamalovaná
postavu	postava	k1gFnSc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
analýza	analýza	k1gFnSc1	analýza
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
umělecko-historický	uměleckoistorický	k2eAgInSc4d1	umělecko-historický
rozbor	rozbor	k1gInSc4	rozbor
a	a	k8xC	a
především	především	k6eAd1	především
dendrochronologická	dendrochronologický	k2eAgFnSc1d1	dendrochronologická
analýza	analýza	k1gFnSc1	analýza
Boschovo	Boschův	k2eAgNnSc1d1	Boschovo
autorství	autorství	k1gNnSc4	autorství
vyvrátily	vyvrátit	k5eAaPmAgFnP	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
originál	originál	k1gInSc4	originál
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
kopie	kopie	k1gFnPc1	kopie
ztraceného	ztracený	k2eAgInSc2d1	ztracený
originálu	originál	k1gInSc2	originál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
DURDÍK	DURDÍK	kA	DURDÍK
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrovaná	ilustrovaný	k2eAgFnSc1d1	ilustrovaná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
českých	český	k2eAgInPc2d1	český
hradů	hrad	k1gInPc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Opočno	Opočno	k1gNnSc4	Opočno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Podrobná	podrobný	k2eAgFnSc1d1	podrobná
historie	historie	k1gFnSc1	historie
na	na	k7c4	na
hejkalkovo	hejkalkův	k2eAgNnSc4d1	hejkalkův
<g/>
.	.	kIx.	.
<g/>
opocno	opocno	k1gNnSc4	opocno
<g/>
.	.	kIx.	.
<g/>
sweb	sweb	k1gInSc1	sweb
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
castles	castles	k1gInSc4	castles
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
npu	npu	k?	npu
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
rodu	rod	k1gInSc2	rod
Colloredo-Mansfeld	Colloredo-Mansfeld	k1gInSc1	Colloredo-Mansfeld
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
restitučního	restituční	k2eAgInSc2d1	restituční
sporu	spor	k1gInSc2	spor
o	o	k7c4	o
zámek	zámek	k1gInSc4	zámek
Opočno	Opočno	k1gNnSc4	Opočno
</s>
</p>
