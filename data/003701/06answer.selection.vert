<s>
Nemá	mít	k5eNaImIp3nS	mít
jiné	jiný	k2eAgNnSc4d1	jiné
formální	formální	k2eAgNnSc4d1	formální
jméno	jméno	k1gNnSc4	jméno
než	než	k8xS	než
"	"	kIx"	"
<g/>
měsíc	měsíc	k1gInSc1	měsíc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
odborně	odborně	k6eAd1	odborně
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
básnicky	básnicky	k6eAd1	básnicky
nazýván	nazývat	k5eAaImNgInS	nazývat
Luna	luna	k1gFnSc1	luna
(	(	kIx(	(
<g/>
slovanský	slovanský	k2eAgMnSc1d1	slovanský
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
latinský	latinský	k2eAgInSc1d1	latinský
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
