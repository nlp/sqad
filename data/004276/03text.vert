<s>
Adéla	Adéla	k1gFnSc1	Adéla
ještě	ještě	k6eAd1	ještě
nevečeřela	večeřet	k5eNaImAgFnS	večeřet
je	on	k3xPp3gNnSc4	on
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
natočená	natočený	k2eAgFnSc1d1	natočená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
ve	v	k7c6	v
Filmovém	filmový	k2eAgNnSc6d1	filmové
studiu	studio	k1gNnSc6	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Lipským	lipský	k2eAgFnPc3d1	Lipská
jako	jako	k9	jako
parodie	parodie	k1gFnSc2	parodie
na	na	k7c4	na
filmy	film	k1gInPc4	film
o	o	k7c6	o
neohrožených	ohrožený	k2eNgMnPc6d1	neohrožený
detektivech	detektiv	k1gMnPc6	detektiv
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
všehoschopných	všehoschopný	k2eAgInPc2d1	všehoschopný
protivnících	protivník	k1gMnPc6	protivník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
obdržela	obdržet	k5eAaPmAgFnS	obdržet
cenu	cena	k1gFnSc4	cena
Saturn	Saturn	k1gInSc1	Saturn
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rozmanitých	rozmanitý	k2eAgFnPc2d1	rozmanitá
technických	technický	k2eAgFnPc2d1	technická
novinek	novinka	k1gFnPc2	novinka
a	a	k8xC	a
rozvoje	rozvoj	k1gInSc2	rozvoj
nových	nový	k2eAgFnPc2d1	nová
detektivních	detektivní	k2eAgFnPc2d1	detektivní
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
–	–	k?	–
americký	americký	k2eAgMnSc1d1	americký
detektiv	detektiv	k1gMnSc1	detektiv
Nick	Nick	k1gMnSc1	Nick
Carter	Carter	k1gMnSc1	Carter
(	(	kIx(	(
<g/>
Michal	Michal	k1gMnSc1	Michal
Dočolomanský	Dočolomanský	k2eAgMnSc1d1	Dočolomanský
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
českého	český	k2eAgMnSc2d1	český
komisaře	komisař	k1gMnSc2	komisař
Ledviny	Ledvina	k1gMnSc2	Ledvina
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
<g/>
)	)	kIx)	)
snaží	snažit	k5eAaImIp3nS	snažit
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
záhadné	záhadný	k2eAgNnSc1d1	záhadné
zmizení	zmizení	k1gNnSc1	zmizení
psa	pes	k1gMnSc2	pes
z	z	k7c2	z
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
pokoje	pokoj	k1gInSc2	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zmizení	zmizení	k1gNnSc2	zmizení
pejska	pejsek	k1gMnSc2	pejsek
je	být	k5eAaImIp3nS	být
podezřelá	podezřelý	k2eAgFnSc1d1	podezřelá
masožravá	masožravý	k2eAgFnSc1d1	masožravá
rostlina	rostlina	k1gFnSc1	rostlina
–	–	k?	–
Adéla	Adéla	k1gFnSc1	Adéla
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mohl	moct	k5eAaImAgInS	moct
vypěstovat	vypěstovat	k5eAaPmF	vypěstovat
jedině	jedině	k6eAd1	jedině
zločinec	zločinec	k1gMnSc1	zločinec
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Zahradník	Zahradník	k1gMnSc1	Zahradník
(	(	kIx(	(
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
již	již	k9	již
nebožtíkem	nebožtík	k1gMnSc7	nebožtík
<g/>
.	.	kIx.	.
</s>
<s>
Podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
dopadnout	dopadnout	k5eAaPmF	dopadnout
<g/>
?	?	kIx.	?
</s>
<s>
Na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
Ondřeje	Ondřej	k1gMnSc4	Ondřej
Brouska	Brousek	k1gMnSc4	Brousek
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
filmu	film	k1gInSc2	film
muzikál	muzikál	k1gInSc4	muzikál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
uváděn	uváděn	k2eAgInSc1d1	uváděn
pražským	pražský	k2eAgNnSc7d1	Pražské
Divadlem	divadlo	k1gNnSc7	divadlo
Broadway	Broadwaa	k1gFnSc2	Broadwaa
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Recesistické	recesistický	k2eAgNnSc1d1	recesistické
divadelní	divadelní	k2eAgNnSc1d1	divadelní
zpracování	zpracování	k1gNnSc1	zpracování
filmu	film	k1gInSc2	film
bylo	být	k5eAaImAgNnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
v	v	k7c6	v
zámeckém	zámecký	k2eAgNnSc6d1	zámecké
barokním	barokní	k2eAgNnSc6d1	barokní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
v	v	k7c6	v
nastudování	nastudování	k1gNnSc6	nastudování
místního	místní	k2eAgInSc2d1	místní
spolku	spolek	k1gInSc2	spolek
Proradost	Proradost	k1gFnSc1	Proradost
<g/>
.	.	kIx.	.
</s>
