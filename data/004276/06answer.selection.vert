<s>
Adéla	Adéla	k1gFnSc1	Adéla
ještě	ještě	k6eAd1	ještě
nevečeřela	večeřet	k5eNaImAgFnS	večeřet
je	on	k3xPp3gNnSc4	on
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
natočená	natočený	k2eAgFnSc1d1	natočená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
ve	v	k7c6	v
Filmovém	filmový	k2eAgNnSc6d1	filmové
studiu	studio	k1gNnSc6	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Lipským	lipský	k2eAgFnPc3d1	Lipská
jako	jako	k9	jako
parodie	parodie	k1gFnSc2	parodie
na	na	k7c4	na
filmy	film	k1gInPc4	film
o	o	k7c6	o
neohrožených	ohrožený	k2eNgMnPc6d1	neohrožený
detektivech	detektiv	k1gMnPc6	detektiv
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
všehoschopných	všehoschopný	k2eAgInPc2d1	všehoschopný
protivnících	protivník	k1gMnPc6	protivník
<g/>
.	.	kIx.	.
</s>
