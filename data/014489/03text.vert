<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
atletice	atletika	k1gFnSc6
1997	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
atletice	atletika	k1gFnSc6
–	–	k?
sportovní	sportovní	k2eAgInSc4d1
závod	závod	k1gInSc4
organizovaný	organizovaný	k2eAgInSc4d1
EAA	EAA	kA
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
ve	v	k7c6
Slovinsku	Slovinsko	k1gNnSc6
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
Lublani	Lublaň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závod	závod	k1gInSc1
s	s	k7c7
odehrál	odehrát	k5eAaPmAgInS
od	od	k7c2
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
Muži	muž	k1gMnPc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
Stříbro	stříbro	k1gNnSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
Bronz	bronz	k1gInSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
100	#num#	k4
m	m	kA
</s>
<s>
Dwain	Dwain	k2eAgInSc1d1
Chambers	Chambers	k1gInSc1
<g/>
10,06	10,06	k4
</s>
<s>
Christian	Christian	k1gMnSc1
Malcolm	Malcolm	k1gMnSc1
<g/>
10,24	10,24	k4
</s>
<s>
Frédéric	Frédéric	k1gMnSc1
Krantz	Krantz	k1gMnSc1
<g/>
10,35	10,35	k4
</s>
<s>
200	#num#	k4
m	m	kA
</s>
<s>
Christian	Christian	k1gMnSc1
Malcolm	Malcolm	k1gMnSc1
<g/>
20,51	20,51	k4
</s>
<s>
Piotr	Piotr	k1gMnSc1
Berestiuk	Berestiuk	k1gMnSc1
<g/>
20,91	20,91	k4
</s>
<s>
Mark	Mark	k1gMnSc1
Findlay	Findlaa	k1gFnSc2
<g/>
20,99	20,99	k4
</s>
<s>
400	#num#	k4
m	m	kA
</s>
<s>
David	David	k1gMnSc1
Canal	Canal	k1gMnSc1
<g/>
46,04	46,04	k4
</s>
<s>
Periklis	Periklis	k1gFnSc1
Jakowakis	Jakowakis	k1gFnSc2
<g/>
46,68	46,68	k4
</s>
<s>
David	David	k1gMnSc1
Naismith	Naismith	k1gMnSc1
<g/>
47,11	47,11	k4
</s>
<s>
800	#num#	k4
m	m	kA
</s>
<s>
Nils	Nils	k1gInSc1
Schumann	Schumann	k1gNnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
51,00	51,00	k4
</s>
<s>
Robert	Robert	k1gMnSc1
Neryng	Neryng	k1gMnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
51,45	51,45	k4
</s>
<s>
Roman	Roman	k1gMnSc1
Oravec	Oravec	k1gMnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
51,51	51,51	k4
</s>
<s>
1500	#num#	k4
m	m	kA
</s>
<s>
Gert-Jan	Gert-Jan	k1gInSc1
Liefers	Liefers	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
46,91	46,91	k4
</s>
<s>
Benjamin	Benjamin	k1gMnSc1
Hetzler	Hetzler	k1gMnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
48,15	48,15	k4
</s>
<s>
Gareth	Gareth	k1gInSc1
Turnbull	Turnbull	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
48,16	48,16	k4
</s>
<s>
5000	#num#	k4
m	m	kA
</s>
<s>
Bouabdellah	Bouabdellah	k1gInSc1
Tahri	Tahri	k1gNnSc1
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
25,71	25,71	k4
</s>
<s>
Ferenc	Ferenc	k1gMnSc1
Békési	Békés	k1gMnSc3
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
27,87	27,87	k4
</s>
<s>
Juan	Juan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Higuero	Higuero	k1gNnSc4
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
31,79	31,79	k4
</s>
<s>
10	#num#	k4
000	#num#	k4
m	m	kA
</s>
<s>
Ovidiu	Ovidius	k1gMnSc3
Tat	Tat	k1gMnSc3
<g/>
29	#num#	k4
<g/>
:	:	kIx,
<g/>
56,35	56,35	k4
</s>
<s>
Mustafa	Mustaf	k1gMnSc2
Mohamed	Mohamed	k1gMnSc1
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4,33	4,33	k4
</s>
<s>
Jussi	Jusse	k1gFnSc3
Utriainen	Utriainen	k1gInSc1
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
23,02	23,02	k4
</s>
<s>
110	#num#	k4
m	m	kA
překážek	překážka	k1gFnPc2
</s>
<s>
Tomasz	Tomasz	k1gInSc1
Ścigaczewski	Ścigaczewsk	k1gFnSc2
<g/>
13,55	13,55	k4
</s>
<s>
Staņ	Staņ	k1gInSc1
Olijars	Olijars	k1gInSc1
<g/>
13,74	13,74	k4
</s>
<s>
Jan	Jan	k1gMnSc1
Schindzielorz	Schindzielorz	k1gMnSc1
<g/>
14,10	14,10	k4
</s>
<s>
400	#num#	k4
m	m	kA
překážek	překážka	k1gFnPc2
</s>
<s>
Boris	Boris	k1gMnSc1
Gorbań	Gorbań	k1gMnSc1
<g/>
50,95	50,95	k4
</s>
<s>
Aljoscha	Aljoscha	k1gMnSc1
Nemitz	Nemitz	k1gMnSc1
<g/>
51,08	51,08	k4
</s>
<s>
Boris	Boris	k1gMnSc1
Vazovan	Vazovan	k1gMnSc1
<g/>
51,26	51,26	k4
</s>
<s>
3000	#num#	k4
m	m	kA
překážek	překážka	k1gFnPc2
</s>
<s>
Günther	Günthra	k1gFnPc2
Weidlinger	Weidlingero	k1gNnPc2
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
41,54	41,54	k4
</s>
<s>
Roman	Roman	k1gMnSc1
Usow	Usow	k1gMnSc1
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
48,47	48,47	k4
</s>
<s>
Antonio	Antonio	k1gMnSc1
Martinez	Martinez	k1gMnSc1
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
56,60	56,60	k4
</s>
<s>
Štafeta	štafeta	k1gFnSc1
4	#num#	k4
×	×	k?
100	#num#	k4
m	m	kA
</s>
<s>
Spojené	spojený	k2eAgFnSc2d1
královstvíUvie	královstvíUvie	k1gFnSc2
UgonoMark	UgonoMark	k1gInSc1
FindlayChristian	FindlayChristian	k1gMnSc1
MalcolmDwain	MalcolmDwain	k1gMnSc1
Chambers	Chambers	k1gInSc4
<g/>
39,62	39,62	k4
</s>
<s>
FrancieVincent	FrancieVincent	k1gMnSc1
CaureFrédéric	CaureFrédéric	k1gMnSc1
KrantzDidier	KrantzDidier	k1gMnSc1
HéryDimitri	HéryDimitre	k1gFnSc4
Demoniè	Demoniè	k1gInSc5
<g/>
39,89	39,89	k4
</s>
<s>
NěmeckoThomas	NěmeckoThomas	k1gMnSc1
HüttingerTim	HüttingerTim	k1gMnSc1
StudzinskiThomas	StudzinskiThomas	k1gMnSc1
ZeimentzJirka	ZeimentzJirka	k1gMnSc1
Zapletal	Zapletal	k1gMnSc1
<g/>
40,26	40,26	k4
</s>
<s>
Štafeta	štafeta	k1gFnSc1
4	#num#	k4
×	×	k?
400	#num#	k4
m	m	kA
</s>
<s>
ŠpanělskoAdrián	ŠpanělskoAdrián	k1gMnSc1
FernándezLuis	FernándezLuis	k1gFnSc2
FloresAlberto	FloresAlberta	k1gFnSc5
MartínezDavid	MartínezDavid	k1gInSc4
Canal	Canal	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8,18	8,18	k4
</s>
<s>
ŘeckoEvangelios	ŘeckoEvangelios	k1gMnSc1
MoustakidisGeórgios	MoustakidisGeórgios	k1gMnSc1
IkonomidisIoannis	IkonomidisIoannis	k1gFnSc1
LessisPeriklis	LessisPeriklis	k1gFnSc1
Jakowakis	Jakowakis	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8,29	8,29	k4
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
královstvíLee	královstvíLeat	k5eAaPmIp3nS
BlackMichael	BlackMichael	k1gInSc4
ParperMark	ParperMark	k1gInSc1
RowlandsDavid	RowlandsDavid	k1gInSc1
Naismith	Naismith	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8,48	8,48	k4
</s>
<s>
Chůze	chůze	k1gFnSc1
na	na	k7c4
10	#num#	k4
000	#num#	k4
m	m	kA
</s>
<s>
Andrea	Andrea	k1gFnSc1
Manfredini	Manfredin	k2eAgMnPc1d1
<g/>
42	#num#	k4
<g/>
:	:	kIx,
<g/>
43,75	43,75	k4
</s>
<s>
Andre	Andr	k1gMnSc5
Höhne	Höhn	k1gMnSc5
<g/>
43	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0,71	0,71	k4
</s>
<s>
Martin	Martin	k1gMnSc1
Pupiš	Pupiš	k1gMnSc1
<g/>
43	#num#	k4
<g/>
:	:	kIx,
<g/>
11,53	11,53	k4
</s>
<s>
Skok	skok	k1gInSc1
do	do	k7c2
výšky	výška	k1gFnSc2
</s>
<s>
Hienadź	Hienadź	k?
Maroz	Maroz	k1gMnSc1
<g/>
2,20	2,20	k4
m	m	kA
</s>
<s>
Ben	Ben	k1gInSc1
Challenger	Challenger	k1gInSc1
<g/>
2,20	2,20	k4
m	m	kA
</s>
<s>
Aleksiej	Aleksiej	k1gInSc1
Lesniczy	Lesnicza	k1gFnSc2
<g/>
2,17	2,17	k4
m	m	kA
</s>
<s>
Skok	skok	k1gInSc1
o	o	k7c6
tyči	tyč	k1gFnSc6
</s>
<s>
Lars	Lars	k1gInSc1
Börgeling	Börgeling	k1gInSc1
<g/>
5,40	5,40	k4
m	m	kA
</s>
<s>
Pavel	Pavel	k1gMnSc1
Gerasimov	Gerasimov	k1gInSc4
<g/>
5,30	5,30	k4
m	m	kA
</s>
<s>
Christian	Christian	k1gMnSc1
Linskey	Linskea	k1gFnSc2
<g/>
5,00	5,00	k4
m	m	kA
</s>
<s>
Skok	skok	k1gInSc1
daleký	daleký	k2eAgInSc1d1
</s>
<s>
Nathan	Nathan	k1gMnSc1
Morgan	morgan	k1gMnSc1
<g/>
7,90	7,90	k4
m	m	kA
</s>
<s>
Raúl	Raúl	k1gMnSc1
Fernández	Fernández	k1gMnSc1
<g/>
7,90	7,90	k4
m	m	kA
</s>
<s>
Yann	Yann	k1gMnSc1
Domenech	Domen	k1gInPc6
<g/>
7,87	7,87	k4
m	m	kA
</s>
<s>
Trojskok	trojskok	k1gInSc1
</s>
<s>
Wiktor	Wiktor	k1gInSc1
Guszczinski	Guszczinsk	k1gFnSc2
<g/>
16,78	16,78	k4
m	m	kA
</s>
<s>
Ionuţ	Ionuţ	k?
Pungă	Pungă	k1gFnSc1
<g/>
16,43	16,43	k4
m	m	kA
</s>
<s>
Eduardo	Eduardo	k1gNnSc4
Perez	Pereza	k1gFnPc2
<g/>
16,40	16,40	k4
m	m	kA
</s>
<s>
Vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
Ralf	Ralf	k1gMnSc1
Bartels	Bartels	k1gInSc4
<g/>
18,30	18,30	k4
m	m	kA
</s>
<s>
Mikuláš	mikuláš	k1gInSc1
Konopka	konopka	k1gFnSc1
<g/>
17,63	17,63	k4
m	m	kA
</s>
<s>
Peter	Peter	k1gMnSc1
Sack	Sack	k1gMnSc1
<g/>
17,23	17,23	k4
m	m	kA
</s>
<s>
Hod	hod	k1gInSc4
diskem	disk	k1gInSc7
</s>
<s>
Emeka	Emeka	k1gFnSc1
Udechuku	Udechuk	k1gInSc2
<g/>
53,90	53,90	k4
m	m	kA
</s>
<s>
Patrick	Patrick	k1gMnSc1
Stang	Stang	k1gMnSc1
<g/>
53,02	53,02	k4
m	m	kA
</s>
<s>
Zoltán	Zoltán	k2eAgMnSc1d1
Kővágó	Kővágó	k1gMnSc1
<g/>
52,90	52,90	k4
m	m	kA
</s>
<s>
Hod	hod	k1gInSc4
kladivem	kladivo	k1gNnSc7
</s>
<s>
Maciej	Maciej	k1gInSc1
Pałyszko	Pałyszka	k1gFnSc5
<g/>
74,12	74,12	k4
m	m	kA
</s>
<s>
Siergiej	Siergiej	k1gInSc1
Martiemianow	Martiemianow	k1gFnSc1
<g/>
70,62	70,62	k4
m	m	kA
</s>
<s>
Olli-Pekka	Olli-Pekka	k1gFnSc1
Karjalainen	Karjalainno	k1gNnPc2
<g/>
69,84	69,84	k4
m	m	kA
</s>
<s>
Hod	hod	k1gInSc4
oštěpem	oštěp	k1gInSc7
</s>
<s>
Adrian	Adrian	k1gMnSc1
Markowski	Markowsk	k1gFnSc2
<g/>
78,42	78,42	k4
m	m	kA
</s>
<s>
Juha	Juha	k6eAd1
Aarnio	Aarnio	k6eAd1
<g/>
77,60	77,60	k4
m	m	kA
</s>
<s>
Christian	Christian	k1gMnSc1
Fusenig	Fusenig	k1gMnSc1
<g/>
75,86	75,86	k4
m	m	kA
</s>
<s>
Desetiboj	desetiboj	k1gInSc1
</s>
<s>
Chiel	Chiel	k1gInSc1
Warners	Warners	k1gInSc1
<g/>
7	#num#	k4
664	#num#	k4
</s>
<s>
Steffen	Steffen	k2eAgInSc1d1
Munz	Munz	k1gInSc1
<g/>
7	#num#	k4
258	#num#	k4
</s>
<s>
Sebastian	Sebastian	k1gMnSc1
Knabe	Knab	k1gInSc5
<g/>
7	#num#	k4
218	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
Stříbro	stříbro	k1gNnSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
Bronz	bronz	k1gInSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
100	#num#	k4
m	m	kA
</s>
<s>
Johanna	Johanna	k1gFnSc1
Manninen	Manninen	k1gInSc1
<g/>
11,39	11,39	k4
</s>
<s>
Agnė	Agnė	k?
Visockaitė	Visockaitė	k1gFnSc1
<g/>
11,42	11,42	k4
</s>
<s>
Erica	Erica	k1gFnSc1
Marchetti	Marchetť	k1gFnSc2
<g/>
11,47	11,47	k4
</s>
<s>
200	#num#	k4
m	m	kA
</s>
<s>
Sabrina	Sabrina	k1gFnSc1
Mulrain	Mulrain	k1gInSc1
<g/>
23,35	23,35	k4
</s>
<s>
Muriel	Muriel	k1gInSc1
Hurtisová	Hurtisový	k2eAgFnSc1d1
<g/>
23,36	23,36	k4
</s>
<s>
Johanna	Johanna	k1gFnSc1
Manninen	Manninen	k1gInSc1
<g/>
23,43	23,43	k4
</s>
<s>
400	#num#	k4
m	m	kA
</s>
<s>
Kristina	Kristin	k2eAgFnSc1d1
Perica	Perica	k1gFnSc1
<g/>
53,07	53,07	k4
</s>
<s>
Alina	Alina	k1gFnSc1
Ripanu	Ripan	k1gInSc2
<g/>
53,10	53,10	k4
</s>
<s>
Cindy	Cinda	k1gFnPc1
Ega	ego	k1gNnSc2
<g/>
53,17	53,17	k4
</s>
<s>
800	#num#	k4
m	m	kA
</s>
<s>
Anca	Anc	k2eAgFnSc1d1
Safta	Safta	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3,47	3,47	k4
</s>
<s>
Aleksandra	Aleksandra	k1gFnSc1
Dereń	Dereń	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3,70	3,70	k4
</s>
<s>
Miriam	Miriam	k1gFnSc1
Mašeková	Mašeková	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6,17	6,17	k4
</s>
<s>
1500	#num#	k4
m	m	kA
</s>
<s>
Natalija	Natalij	k2eAgFnSc1d1
Jevdokimova	Jevdokimův	k2eAgFnSc1d1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
23,34	23,34	k4
</s>
<s>
Małgorzata	Małgorzata	k1gFnSc1
Bury	Bury	k?
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
24,37	24,37	k4
</s>
<s>
Ljiljana	Ljiljana	k1gFnSc1
Ćulibrk	Ćulibrk	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
24,75	24,75	k4
</s>
<s>
3000	#num#	k4
m	m	kA
</s>
<s>
Laura	Laura	k1gFnSc1
Suffa	Suff	k1gMnSc2
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
27,81	27,81	k4
</s>
<s>
Sonja	Sonj	k2eAgFnSc1d1
Stolić	Stolić	k1gFnSc1
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
29,65	29,65	k4
</s>
<s>
Sandra	Sandra	k1gFnSc1
Levenez	Levenez	k1gInSc1
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
31,66	31,66	k4
</s>
<s>
5000	#num#	k4
m	m	kA
</s>
<s>
Katalin	Katalin	k1gInSc1
Szentgyörgyi	Szentgyörgyi	k1gNnSc1
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
38,73	38,73	k4
</s>
<s>
Tatiana	Tatiana	k1gFnSc1
Gierasimowa	Gierasimowa	k1gFnSc1
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
39,31	39,31	k4
</s>
<s>
Ionela	Ionela	k1gFnSc1
Bungardean	Bungardean	k1gInSc1
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
39,97	39,97	k4
</s>
<s>
100	#num#	k4
m	m	kA
překážek	překážka	k1gFnPc2
</s>
<s>
Tatiana	Tatiana	k1gFnSc1
Mišakova	Mišakův	k2eAgFnSc1d1
<g/>
13,49	13,49	k4
</s>
<s>
Eva	Eva	k1gFnSc1
Miklos	Miklos	k1gInSc1
<g/>
13,81	13,81	k4
</s>
<s>
Hanna	Hanna	k1gFnSc1
Korell	Korell	k1gInSc1
<g/>
13,92	13,92	k4
</s>
<s>
400	#num#	k4
m	m	kA
překážek	překážka	k1gFnPc2
</s>
<s>
Florence	Florenc	k1gFnPc1
Delaune	Delaun	k1gMnSc5
<g/>
57,91	57,91	k4
</s>
<s>
Thelma	Thelma	k1gFnSc1
Joziasse	Joziasse	k1gFnSc2
<g/>
58,65	58,65	k4
</s>
<s>
Medina	Medina	k1gFnSc1
Tudor	tudor	k1gInSc1
<g/>
58,85	58,85	k4
</s>
<s>
Štafeta	štafeta	k1gFnSc1
4	#num#	k4
×	×	k?
100	#num#	k4
m	m	kA
</s>
<s>
NěmeckoAnne	NěmeckoAnnout	k5eAaImIp3nS,k5eAaPmIp3nS
ReucherMarion	ReucherMarion	k1gInSc1
WagnerováAlice	WagnerováAlice	k1gFnSc2
ReussSabrina	ReussSabrina	k1gMnSc1
Mulrain	Mulrain	k1gMnSc1
<g/>
44,24	44,24	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
královstvíRebecca	královstvíRebecca	k6eAd1
DrummondSarah	DrummondSarah	k1gInSc4
WilhelmyMelanie	WilhelmyMelanie	k1gFnSc2
PurkissTatum	PurkissTatum	k1gNnSc4
Nelson	Nelson	k1gMnSc1
<g/>
44,55	44,55	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
Ewa	Ewa	k1gMnSc2
Klarecka	Klarecka	k1gFnSc1
Agnieszka	Agnieszka	k1gFnSc1
Rysiukiewicz	Rysiukiewicz	k1gInSc1
Monika	Monika	k1gFnSc1
Giemzo	Giemza	k1gFnSc5
Anna	Anna	k1gFnSc1
Pacholak	Pacholak	k1gInSc1
<g/>
44,59	44,59	k4
</s>
<s>
Štafeta	štafeta	k1gFnSc1
4	#num#	k4
×	×	k?
400	#num#	k4
m	m	kA
</s>
<s>
FrancieKatiana	FrancieKatiana	k1gFnSc1
RenéFlorence	RenéFlorence	k1gFnSc2
DelauneSylvanie	DelauneSylvanie	k1gFnSc2
MorandaisCindy	MorandaisCinda	k1gFnSc2
Ega	ego	k1gNnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
33,73	33,73	k4
</s>
<s>
MaďarskoEnikö	MaďarskoEnikö	k?
SzabóRenáta	SzabóRenáta	k1gFnSc1
BalazsicKrisztina	BalazsicKrisztin	k2eAgFnSc1d1
DomaBarbara	DomaBarbara	k1gFnSc1
Petráhn	Petráhn	k1gNnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
34,90	34,90	k4
</s>
<s>
NěmeckoKerstin	NěmeckoKerstin	k2eAgInSc1d1
SeitzJennifer	SeitzJennifer	k1gInSc1
VollrathDoreen	VollrathDorena	k1gFnPc2
HarstickClaudia	HarstickClaudium	k1gNnSc2
Marx	Marx	k1gMnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
34,94	34,94	k4
</s>
<s>
Chůze	chůze	k1gFnSc1
na	na	k7c4
5000	#num#	k4
m	m	kA
</s>
<s>
Claudia	Claudia	k1gFnSc1
Iovan	Iovan	k1gInSc1
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
15,99	15,99	k4
</s>
<s>
Ludmiła	Ludmił	k2eAgFnSc1d1
Diediekina	Diediekina	k1gFnSc1
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
42,21	42,21	k4
</s>
<s>
Ludmiła	Ludmił	k2eAgFnSc1d1
Jefimkina	Jefimkina	k1gFnSc1
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4,11	4,11	k4
</s>
<s>
Skok	skok	k1gInSc1
do	do	k7c2
výšky	výška	k1gFnSc2
</s>
<s>
Linda	Linda	k1gFnSc1
Horvath	Horvath	k1gInSc1
<g/>
1,92	1,92	k4
m	m	kA
</s>
<s>
Marina	Marina	k1gFnSc1
Kupcovová	Kupcovová	k1gFnSc1
<g/>
1,90	1,90	k4
m	m	kA
</s>
<s>
Světlana	Světlana	k1gFnSc1
Lapinová	Lapinová	k1gFnSc1
<g/>
1,90	1,90	k4
m	m	kA
</s>
<s>
Skok	skok	k1gInSc1
o	o	k7c6
tyči	tyč	k1gFnSc6
</s>
<s>
Annika	Annika	k1gFnSc1
Beckerová	Beckerová	k1gFnSc1
<g/>
4,00	4,00	k4
m	m	kA
</s>
<s>
Vala	Vala	k1gMnSc1
Flosadóttir	Flosadóttir	k1gMnSc1
<g/>
4,00	4,00	k4
m	m	kA
</s>
<s>
Monika	Monika	k1gFnSc1
Erlach	Erlach	k1gInSc1
<g/>
3,95	3,95	k4
m	m	kA
</s>
<s>
Skok	skok	k1gInSc1
daleký	daleký	k2eAgInSc1d1
</s>
<s>
Aurélie	Aurélie	k1gFnSc1
Félix	Félix	k1gInSc1
<g/>
6,52	6,52	k4
m	m	kA
</s>
<s>
Sandra	Sandra	k1gFnSc1
Stube	Stub	k1gInSc5
<g/>
6,50	6,50	k4
m	m	kA
</s>
<s>
Olivia	Olivia	k1gFnSc1
Wockinger	Wockingero	k1gNnPc2
<g/>
6,47	6,47	k4
m	m	kA
</s>
<s>
Trojskok	trojskok	k1gInSc1
</s>
<s>
Adelina	Adelin	k2eAgFnSc1d1
Gavrila	Gavrila	k1gFnSc1
<g/>
13,58	13,58	k4
m	m	kA
</s>
<s>
Marija	Marij	k2eAgFnSc1d1
Šestaková	Šestakový	k2eAgFnSc1d1
<g/>
13,54	13,54	k4
m	m	kA
</s>
<s>
Diana	Diana	k1gFnSc1
Nikitina	Nikitina	k1gFnSc1
<g/>
13,45	13,45	k4
m	m	kA
</s>
<s>
Vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
Jelena	Jelena	k1gFnSc1
Ivaněnko	Ivaněnka	k1gFnSc5
<g/>
17,05	17,05	k4
m	m	kA
</s>
<s>
Nadine	Nadinout	k5eAaPmIp3nS
Banse	Banse	k1gFnSc1
<g/>
16,60	16,60	k4
m	m	kA
</s>
<s>
Assunta	Assunta	k1gFnSc1
Legnanteová	Legnanteová	k1gFnSc1
<g/>
16,18	16,18	k4
m	m	kA
</s>
<s>
Hod	hod	k1gInSc4
diskem	disk	k1gInSc7
</s>
<s>
Lacramioara	Lacramioara	k1gFnSc1
Ionescu	Ionesco	k1gMnSc3
<g/>
52,54	52,54	k4
m	m	kA
</s>
<s>
Satu	Sat	k2eAgFnSc4d1
Järvenpää	Järvenpää	k1gFnSc4
<g/>
51,48	51,48	k4
m	m	kA
</s>
<s>
Philippa	Philippa	k1gFnSc1
Roles	Roles	k1gInSc1
<g/>
50,62	50,62	k4
m	m	kA
</s>
<s>
Hod	hod	k1gInSc4
kladivem	kladivo	k1gNnSc7
</s>
<s>
Kamila	Kamila	k1gFnSc1
Skolimowska	Skolimowska	k1gFnSc1
<g/>
59,72	59,72	k4
m	m	kA
</s>
<s>
Sini	Sini	k1gNnSc1
Pöyry	Pöyra	k1gFnSc2
<g/>
59,42	59,42	k4
m	m	kA
</s>
<s>
Susanne	Susannout	k5eAaPmIp3nS,k5eAaImIp3nS
Keil	Keil	k1gInSc4
<g/>
59,22	59,22	k4
m	m	kA
</s>
<s>
Hod	hod	k1gInSc4
oštěpem	oštěp	k1gInSc7
</s>
<s>
Nikolett	Nikolett	k2eAgMnSc1d1
Szabó	Szabó	k1gMnSc1
<g/>
61,76	61,76	k4
m	m	kA
</s>
<s>
Sarah	Sarah	k1gFnSc1
Walter	Walter	k1gMnSc1
<g/>
57,34	57,34	k4
m	m	kA
</s>
<s>
Bina	Bina	k1gMnSc1
Ramesh	Ramesh	k1gMnSc1
<g/>
55,78	55,78	k4
m	m	kA
</s>
<s>
Sedmiboj	sedmiboj	k1gInSc1
</s>
<s>
Saskia	Saskia	k1gFnSc1
Meijer	Meijer	k1gInSc1
<g/>
5	#num#	k4
882	#num#	k4
</s>
<s>
Jelena	Jelena	k1gFnSc1
Czerniawska	Czerniawska	k1gFnSc1
<g/>
5	#num#	k4
827	#num#	k4
</s>
<s>
Sonja	Sonja	k1gMnSc1
Kesselschläger	Kesselschläger	k1gMnSc1
<g/>
5	#num#	k4
753	#num#	k4
</s>
<s>
Medailové	medailový	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc1
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
Stříbro	stříbro	k1gNnSc1
</s>
<s>
Bronz	bronz	k1gInSc1
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
434343129	#num#	k4
</s>
<s>
Čeští	český	k2eAgMnPc1d1
atleti	atlet	k1gMnPc1
do	do	k7c2
8	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnSc2
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
800	#num#	k4
m	m	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Roman	Roman	k1gMnSc1
Oravec	Oravec	k1gMnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
51,51	51,51	k4
</s>
<s>
Skok	skok	k1gInSc1
o	o	k7c6
tyči	tyč	k1gFnSc6
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pavla	Pavla	k1gFnSc1
Hamáčková	Hamáčková	k1gFnSc1
<g/>
3,95	3,95	k4
m	m	kA
</s>
<s>
Hod	hod	k1gInSc4
oštěpem	oštěp	k1gInSc7
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Michaela	Michaela	k1gFnSc1
Jačková	Jačková	k1gFnSc1
<g/>
55,24	55,24	k4
m	m	kA
</s>
<s>
Sedmiboj	sedmiboj	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Michaela	Michaela	k1gFnSc1
Hejnová	Hejnová	k1gFnSc1
<g/>
5	#num#	k4
453	#num#	k4
</s>
<s>
Hod	hod	k1gInSc4
diskem	disk	k1gInSc7
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Adam	Adam	k1gMnSc1
Doležel	doležet	k5eAaPmAgMnS
<g/>
51,14	51,14	k4
m	m	kA
</s>
<s>
Štafeta	štafeta	k1gFnSc1
4	#num#	k4
×	×	k?
400	#num#	k4
m	m	kA
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Michal	Michal	k1gMnSc1
Škvára	škvára	k1gFnSc1
Jan	Jan	k1gMnSc1
Hanzl	Hanzl	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
Tesařík	Tesařík	k1gMnSc1
Roman	Roman	k1gMnSc1
Oravec	Oravec	k1gMnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
16,12	16,12	k4
</s>
<s>
Trojskok	trojskok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Eva	Eva	k1gFnSc1
Doležalová	Doležalová	k1gFnSc1
<g/>
13,14	13,14	k4
m	m	kA
</s>
<s>
Hod	hod	k1gInSc4
diskem	disk	k1gInSc7
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Věra	Věra	k1gFnSc1
Pospíšilová	Pospíšilová	k1gFnSc1
<g/>
46,08	46,08	k4
m	m	kA
</s>
<s>
Sedmiboj	sedmiboj	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hana	Hana	k1gFnSc1
Doleželová	Doleželová	k1gFnSc1
<g/>
5	#num#	k4
415	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
ME	ME	kA
juniorů	junior	k1gMnPc2
v	v	k7c6
atletice	atletika	k1gFnSc6
1997	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
medailistů	medailista	k1gMnPc2
1964	#num#	k4
–	–	k?
2005	#num#	k4
(	(	kIx(
<g/>
muži	muž	k1gMnPc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
medailistů	medailista	k1gMnPc2
1964	#num#	k4
–	–	k?
2005	#num#	k4
(	(	kIx(
<g/>
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
atletice	atletika	k1gFnSc6
Evropské	evropský	k2eAgFnSc2d1
juniorské	juniorský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
</s>
<s>
Varšava	Varšava	k1gFnSc1
1964	#num#	k4
•	•	k?
Oděsa	Oděsa	k1gFnSc1
1966	#num#	k4
•	•	k?
Lipsko	Lipsko	k1gNnSc1
1968	#num#	k4
Mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
</s>
<s>
Paříž	Paříž	k1gFnSc1
1970	#num#	k4
•	•	k?
Duisburg	Duisburg	k1gInSc1
1973	#num#	k4
•	•	k?
Athény	Athéna	k1gFnSc2
1975	#num#	k4
•	•	k?
Doněck	Doněck	k1gInSc1
1977	#num#	k4
•	•	k?
Bydhošť	Bydhošť	k1gFnPc2
1979	#num#	k4
•	•	k?
Utrecht	Utrecht	k1gInSc1
1981	#num#	k4
•	•	k?
Schwechat	Schwechat	k1gFnPc2
1983	#num#	k4
•	•	k?
Chotěbuz	Chotěbuz	k1gFnSc1
1985	#num#	k4
•	•	k?
Birmingham	Birmingham	k1gInSc1
1987	#num#	k4
•	•	k?
Varaždín	Varaždín	k1gInSc1
1989	#num#	k4
•	•	k?
Soluň	Soluň	k1gFnSc1
1991	#num#	k4
•	•	k?
San	San	k1gMnSc1
Sebastián	Sebastián	k1gMnSc1
1993	#num#	k4
•	•	k?
Nyíregyháza	Nyíregyháza	k1gFnSc1
1995	#num#	k4
•	•	k?
Lublaň	Lublaň	k1gFnSc1
1997	#num#	k4
•	•	k?
Riga	Riga	k1gFnSc1
1999	#num#	k4
•	•	k?
Grosseto	Grosseto	k1gNnSc1
2001	#num#	k4
•	•	k?
Tampere	Tamper	k1gInSc5
2003	#num#	k4
•	•	k?
Kaunas	Kaunas	k1gInSc1
2005	#num#	k4
•	•	k?
Hengelo	Hengela	k1gFnSc5
2007	#num#	k4
•	•	k?
Novi	Novi	k1gNnSc7
Sad	sada	k1gFnPc2
2009	#num#	k4
•	•	k?
Tallinn	Tallinn	k1gNnSc1
2011	#num#	k4
•	•	k?
Rieti	Rieti	k1gNnSc1
2013	#num#	k4
•	•	k?
Eskilstuna	Eskilstuna	k1gFnSc1
2015	#num#	k4
•	•	k?
Grosseto	Grosseto	k1gNnSc1
2017	#num#	k4
•	•	k?
Borå	Borå	k1gInSc1
2019	#num#	k4
•	•	k?
Tallinn	Tallinn	k1gNnSc1
2021	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Slovinsko	Slovinsko	k1gNnSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
