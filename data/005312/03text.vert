<s>
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
NAFTA	nafta	k1gFnSc1	nafta
z	z	k7c2	z
angl.	angl.	k?	angl.
North	Northa	k1gFnPc2	Northa
American	Americany	k1gInPc2	Americany
Free	Fre	k1gFnSc2	Fre
Trade	Trad	k1gInSc5	Trad
Agreement	Agreement	k1gInSc1	Agreement
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgFnSc1d1	obchodní
dohoda	dohoda	k1gFnSc1	dohoda
spojující	spojující	k2eAgFnSc4d1	spojující
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc4	Mexiko
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
omezit	omezit	k5eAaPmF	omezit
obchodní	obchodní	k2eAgFnPc4d1	obchodní
a	a	k8xC	a
celní	celní	k2eAgFnPc4d1	celní
bariéry	bariéra	k1gFnPc4	bariéra
a	a	k8xC	a
liberalizovat	liberalizovat	k5eAaImF	liberalizovat
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgNnPc1d1	hlavní
města	město	k1gNnPc1	město
všech	všecek	k3xTgInPc2	všecek
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
Ottawa	Ottawa	k1gFnSc1	Ottawa
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
D.C.	D.C.	k1gFnSc2	D.C.
a	a	k8xC	a
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k1gNnSc1	México
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výší	výše	k1gFnSc7	výše
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
NAFTA	nafta	k1gFnSc1	nafta
největším	veliký	k2eAgNnSc7d3	veliký
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
sdružením	sdružení	k1gNnSc7	sdružení
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc1	její
ekonomiky	ekonomika	k1gFnPc1	ekonomika
představují	představovat	k5eAaImIp3nP	představovat
třetinu	třetina	k1gFnSc4	třetina
světového	světový	k2eAgInSc2d1	světový
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
NAFTA	nafta	k1gFnSc1	nafta
nabyla	nabýt	k5eAaPmAgFnS	nabýt
účinnosti	účinnost	k1gFnPc4	účinnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
