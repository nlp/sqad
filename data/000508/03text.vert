<s>
Jutsko	Jutsko	k1gNnSc1	Jutsko
či	či	k8xC	či
Jutský	jutský	k2eAgInSc1d1	jutský
poloostrov	poloostrov	k1gInSc1	poloostrov
(	(	kIx(	(
<g/>
dánsky	dánsky	k6eAd1	dánsky
Jylland	Jylland	k1gInSc1	Jylland
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poloostrov	poloostrov	k1gInSc4	poloostrov
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
pevninskou	pevninský	k2eAgFnSc4d1	pevninská
část	část	k1gFnSc4	část
Dánska	Dánsko	k1gNnSc2	Dánsko
(	(	kIx(	(
<g/>
70	[number]	k4	70
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejsevernější	severní	k2eAgFnSc1d3	nejsevernější
část	část	k1gFnSc1	část
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Kimberský	Kimberský	k2eAgInSc4d1	Kimberský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
Severní	severní	k2eAgNnSc1d1	severní
a	a	k8xC	a
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nížinaté	nížinatý	k2eAgNnSc1d1	nížinaté
<g/>
,	,	kIx,	,
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
vyvýšeninami	vyvýšenina	k1gFnPc7	vyvýšenina
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
Jutska	Jutsko	k1gNnSc2	Jutsko
činí	činit	k5eAaImIp3nS	činit
29	[number]	k4	29
775	[number]	k4	775
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
2	[number]	k4	2
491	[number]	k4	491
852	[number]	k4	852
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2,188	[number]	k4	2,188
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
40	[number]	k4	40
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Jutsko	Jutsko	k1gNnSc4	Jutsko
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
germánského	germánský	k2eAgInSc2d1	germánský
kmene	kmen	k1gInSc2	kmen
Jutů	Jut	k1gMnPc2	Jut
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odtud	odtud	k6eAd1	odtud
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Angly	Angl	k1gMnPc7	Angl
a	a	k8xC	a
Sasy	Sas	k1gMnPc7	Sas
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
též	též	k9	též
byli	být	k5eAaImAgMnP	být
asimilováni	asimilovat	k5eAaBmNgMnP	asimilovat
Dány	Dán	k1gMnPc7	Dán
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
následně	následně	k6eAd1	následně
obsadili	obsadit	k5eAaPmAgMnP	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
jutské	jutský	k2eAgNnSc1d1	jutský
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Å	Å	k?	Å
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrch	vrch	k1gInSc4	vrch
je	být	k5eAaImIp3nS	být
Ejer	Ejer	k1gMnSc1	Ejer
Baunehø	Baunehø	k1gMnSc1	Baunehø
(	(	kIx(	(
<g/>
171	[number]	k4	171
m	m	kA	m
<g/>
)	)	kIx)	)
či	či	k8xC	či
poblíž	poblíž	k6eAd1	poblíž
ležící	ležící	k2eAgInSc4d1	ležící
vrch	vrch	k1gInSc4	vrch
Yding	Yding	k1gMnSc1	Yding
Skovhø	Skovhø	k1gMnSc1	Skovhø
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
Gudenå	Gudenå	k1gMnPc2	Gudenå
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
158	[number]	k4	158
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
větší	veliký	k2eAgNnPc1d2	veliký
města	město	k1gNnPc1	město
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
jsou	být	k5eAaImIp3nP	být
Aalborg	Aalborg	k1gInSc4	Aalborg
<g/>
,	,	kIx,	,
Esbjerg	Esbjerg	k1gInSc1	Esbjerg
<g/>
,	,	kIx,	,
Frederikshavn	Frederikshavn	k1gInSc1	Frederikshavn
<g/>
,	,	kIx,	,
Randers	Randers	k1gInSc1	Randers
<g/>
,	,	kIx,	,
Kolding	Kolding	k1gInSc1	Kolding
<g/>
,	,	kIx,	,
Silkeborg	Silkeborg	k1gInSc1	Silkeborg
<g/>
,	,	kIx,	,
Ribe	Ribe	k1gInSc1	Ribe
<g/>
,	,	kIx,	,
Vejle	vejl	k1gInSc5	vejl
<g/>
,	,	kIx,	,
Viborg	Viborg	k1gInSc1	Viborg
<g/>
,	,	kIx,	,
Horsens	Horsens	k1gInSc1	Horsens
a	a	k8xC	a
Billund	Billund	k1gInSc1	Billund
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
města	město	k1gNnPc1	město
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
straně	strana	k1gFnSc6	strana
pak	pak	k6eAd1	pak
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
Kiel	Kiel	k1gInSc4	Kiel
<g/>
,	,	kIx,	,
Flensburg	Flensburg	k1gInSc4	Flensburg
a	a	k8xC	a
Neumünster	Neumünster	k1gInSc4	Neumünster
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Jutsko	Jutsko	k1gNnSc4	Jutsko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jutsko	Jutsko	k1gNnSc4	Jutsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
