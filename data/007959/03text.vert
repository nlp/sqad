<s>
Amoniak	amoniak	k1gInSc1	amoniak
neboli	neboli	k8xC	neboli
azan	azan	k1gInSc1	azan
(	(	kIx(	(
<g/>
triviální	triviální	k2eAgInSc1d1	triviální
název	název	k1gInSc1	název
čpavek	čpavek	k1gInSc1	čpavek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgMnSc1d1	bezbarvý
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
štiplavý	štiplavý	k2eAgInSc4d1	štiplavý
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
je	být	k5eAaImIp3nS	být
toxická	toxický	k2eAgFnSc1d1	toxická
<g/>
,	,	kIx,	,
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
látka	látka	k1gFnSc1	látka
zásadité	zásaditý	k2eAgFnSc2d1	zásaditá
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vdechování	vdechování	k1gNnSc6	vdechování
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
sliznici	sliznice	k1gFnSc4	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
světová	světový	k2eAgFnSc1d1	světová
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
amoniaku	amoniak	k1gInSc2	amoniak
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
146,5	[number]	k4	146,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
používanou	používaný	k2eAgFnSc4d1	používaná
metodu	metoda	k1gFnSc4	metoda
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
Haber-Boschův	Haber-Boschův	k2eAgInSc1d1	Haber-Boschův
proces	proces	k1gInSc1	proces
<g/>
)	)	kIx)	)
objevil	objevit	k5eAaPmAgMnS	objevit
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
Fritz	Fritz	k1gMnSc1	Fritz
Haber	Haber	k1gMnSc1	Haber
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
dostal	dostat	k5eAaPmAgInS	dostat
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
amoniak	amoniak	k1gInSc1	amoniak
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
boha	bůh	k1gMnSc2	bůh
Amona	Amon	k1gMnSc2	Amon
(	(	kIx(	(
<g/>
salmiak	salmiak	k1gInSc1	salmiak
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
sůl	sůl	k1gFnSc1	sůl
Amonova	Amonov	k1gInSc2	Amonov
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
čpavek	čpavek	k1gInSc1	čpavek
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
čpět	čpět	k5eAaImF	čpět
(	(	kIx(	(
<g/>
zapáchat	zapáchat	k5eAaImF	zapáchat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
amonných	amonný	k2eAgFnPc2d1	amonná
solí	sůl	k1gFnPc2	sůl
se	s	k7c7	s
silnými	silný	k2eAgInPc7d1	silný
hydroxidy	hydroxid	k1gInPc7	hydroxid
<g/>
,	,	kIx,	,
např.	např.	kA	např.
působením	působení	k1gNnSc7	působení
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
na	na	k7c4	na
chlorid	chlorid	k1gInSc4	chlorid
amonný	amonný	k2eAgInSc4d1	amonný
<g/>
:	:	kIx,	:
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
Cl	Cl	k1gFnPc1	Cl
+	+	kIx~	+
NaOH	NaOH	k1gFnPc1	NaOH
→	→	k?	→
NH3	NH3	k1gMnSc1	NH3
+	+	kIx~	+
NaCl	NaCl	k1gMnSc1	NaCl
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
případně	případně	k6eAd1	případně
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
uhličitanu	uhličitan	k1gInSc2	uhličitan
amonného	amonný	k2eAgInSc2d1	amonný
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g />
.	.	kIx.	.
</s>
<s>
<g/>
3	[number]	k4	3
→	→	k?	→
2	[number]	k4	2
NH3	NH3	k1gFnSc1	NH3
+	+	kIx~	+
CO2	CO2	k1gFnSc1	CO2
+	+	kIx~	+
H2O	H2O	k1gFnSc7	H2O
nebo	nebo	k8xC	nebo
působením	působení	k1gNnSc7	působení
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
na	na	k7c4	na
vodný	vodný	k2eAgInSc4d1	vodný
roztok	roztok	k1gInSc4	roztok
močoviny	močovina	k1gFnSc2	močovina
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
CO	co	k6eAd1	co
+	+	kIx~	+
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
→	→	k?	→
2	[number]	k4	2
NH3	NH3	k1gFnSc1	NH3
+	+	kIx~	+
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
<g />
.	.	kIx.	.
</s>
<s>
katalytickým	katalytický	k2eAgNnSc7d1	katalytické
slučováním	slučování	k1gNnSc7	slučování
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
houbové	houbový	k2eAgNnSc1d1	houbové
železo	železo	k1gNnSc1	železo
<g/>
)	)	kIx)	)
za	za	k7c2	za
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
20	[number]	k4	20
až	až	k9	až
100	[number]	k4	100
MPa	MPa	k1gFnPc2	MPa
<g/>
)	)	kIx)	)
a	a	k8xC	a
vysoké	vysoký	k2eAgFnPc1d1	vysoká
teploty	teplota	k1gFnPc1	teplota
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
500	[number]	k4	500
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Haberova	Haberův	k2eAgFnSc1d1	Haberova
<g/>
–	–	k?	–
<g/>
Boschova	Boschův	k2eAgFnSc1d1	Boschova
syntéza	syntéza	k1gFnSc1	syntéza
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
>	>	kIx)	>
:	:	kIx,	:
500	[number]	k4	500
:	:	kIx,	:
∘	∘	k?	∘
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
N_	N_	k1gMnSc1	N_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
xrightarrow	xrightarrow	k?	xrightarrow
<g/>
[	[	kIx(	[
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
>	>	kIx)	>
<g/>
500	[number]	k4	500
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k1gInSc1	circ
}	}	kIx)	}
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
{	{	kIx(	{
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
NH_	NH_	k1gMnSc1	NH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
exotermní	exotermní	k2eAgFnSc1d1	exotermní
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
při	při	k7c6	při
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
1	[number]	k4	1
148	[number]	k4	148
cm3	cm3	k4	cm3
ve	v	k7c4	v
100	[number]	k4	100
ml	ml	kA	ml
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
zásaditého	zásaditý	k2eAgInSc2d1	zásaditý
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
čpavek	čpavek	k1gInSc1	čpavek
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
roztok	roztok	k1gInSc1	roztok
je	být	k5eAaImIp3nS	být
zásaditý	zásaditý	k2eAgInSc1d1	zásaditý
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
také	také	k9	také
"	"	kIx"	"
<g/>
hydroxid	hydroxid	k1gInSc1	hydroxid
amonný	amonný	k2eAgInSc1d1	amonný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tohle	tenhle	k3xDgNnSc1	tenhle
označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
však	však	k9	však
poněkud	poněkud	k6eAd1	poněkud
nesprávné	správný	k2eNgNnSc1d1	nesprávné
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
molekula	molekula	k1gFnSc1	molekula
"	"	kIx"	"
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
OH	OH	kA	OH
<g/>
"	"	kIx"	"
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Neexistence	neexistence	k1gFnSc1	neexistence
molekuly	molekula	k1gFnSc2	molekula
"	"	kIx"	"
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
OH	OH	kA	OH
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
amoniak	amoniak	k1gInSc1	amoniak
je	být	k5eAaImIp3nS	být
Brø	Brø	k1gFnSc4	Brø
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
Arrheniova	Arrheniův	k2eAgFnSc1d1	Arrheniova
zásada	zásada	k1gFnSc1	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Zásaditý	zásaditý	k2eAgInSc1d1	zásaditý
charakter	charakter	k1gInSc1	charakter
amoniaku	amoniak	k1gInSc2	amoniak
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
podmíněn	podmíněn	k2eAgMnSc1d1	podmíněn
jeho	jeho	k3xOp3gFnSc7	jeho
schopností	schopnost	k1gFnSc7	schopnost
vázat	vázat	k5eAaImF	vázat
proton	proton	k1gInSc4	proton
vodíku	vodík	k1gInSc2	vodík
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
tvořením	tvoření	k1gNnSc7	tvoření
hydroxidových	hydroxidový	k2eAgInPc2d1	hydroxidový
iontů	ion	k1gInPc2	ion
OH	OH	kA	OH
<g/>
−	−	k?	−
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
reakce	reakce	k1gFnSc2	reakce
NH3	NH3	k1gFnSc2	NH3
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
→	→	k?	→
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
+	+	kIx~	+
OH	OH	kA	OH
<g/>
−	−	k?	−
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
této	tento	k3xDgFnSc2	tento
reakce	reakce	k1gFnSc2	reakce
reagují	reagovat	k5eAaBmIp3nP	reagovat
pouze	pouze	k6eAd1	pouze
4	[number]	k4	4
z	z	k7c2	z
1	[number]	k4	1
000	[number]	k4	000
molekul	molekula	k1gFnPc2	molekula
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Správné	správný	k2eAgNnSc1d1	správné
označení	označení	k1gNnSc1	označení
vodního	vodní	k2eAgInSc2d1	vodní
roztoku	roztok	k1gInSc2	roztok
amoniaku	amoniak	k1gInSc2	amoniak
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
NH3	NH3	k1gFnSc1	NH3
•	•	k?	•
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
V	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
chemii	chemie	k1gFnSc6	chemie
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
amoniak	amoniak	k1gInSc4	amoniak
užívá	užívat	k5eAaImIp3nS	užívat
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
azan	azana	k1gFnPc2	azana
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
derivát	derivát	k1gInSc4	derivát
hydrazin	hydrazin	k1gInSc1	hydrazin
NH	NH	kA	NH
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
NH	NH	kA	NH
<g/>
2	[number]	k4	2
název	název	k1gInSc4	název
diazan	diazany	k1gInPc2	diazany
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kyselinami	kyselina	k1gFnPc7	kyselina
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
amonných	amonný	k2eAgFnPc2d1	amonná
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
síran	síran	k1gInSc1	síran
amonný	amonný	k2eAgInSc1d1	amonný
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
(	(	kIx(	(
N	N	kA	N
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
H	H	kA	H
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
NH_	NH_	k1gMnSc1	NH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gFnSc1	H_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
(	(	kIx(	(
<g/>
NH_	NH_	k1gMnSc1	NH_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Amonné	amonný	k2eAgFnPc1d1	amonná
soli	sůl	k1gFnPc1	sůl
silných	silný	k2eAgFnPc2d1	silná
kyselin	kyselina	k1gFnPc2	kyselina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
)	)	kIx)	)
reagují	reagovat	k5eAaBmIp3nP	reagovat
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
slabě	slabě	k6eAd1	slabě
kysele	kysele	k6eAd1	kysele
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
amoniak	amoniak	k1gInSc1	amoniak
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
slabou	slabý	k2eAgFnSc7d1	slabá
zásadou	zásada	k1gFnSc7	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
vzniká	vznikat	k5eAaImIp3nS	vznikat
mikrobiálním	mikrobiální	k2eAgInSc7d1	mikrobiální
rozkladem	rozklad	k1gInSc7	rozklad
organických	organický	k2eAgInPc2d1	organický
zbytků	zbytek	k1gInPc2	zbytek
<g/>
,	,	kIx,	,
exkrementů	exkrement	k1gInPc2	exkrement
a	a	k8xC	a
moči	moč	k1gFnSc2	moč
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
váže	vázat	k5eAaImIp3nS	vázat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
amonných	amonný	k2eAgFnPc2d1	amonná
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
většinu	většina	k1gFnSc4	většina
odpadního	odpadní	k2eAgInSc2d1	odpadní
dusíku	dusík	k1gInSc2	dusík
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
ve	v	k7c6	v
stopovém	stopový	k2eAgNnSc6d1	stopové
množství	množství	k1gNnSc6	množství
obsažen	obsažen	k2eAgMnSc1d1	obsažen
i	i	k8xC	i
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
chloridu	chlorid	k1gInSc2	chlorid
amonného	amonný	k2eAgInSc2d1	amonný
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
minerál	minerál	k1gInSc1	minerál
salmiak	salmiak	k1gInSc4	salmiak
zejména	zejména	k9	zejména
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
solfatar	solfatara	k1gFnPc2	solfatara
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
vulkanických	vulkanický	k2eAgInPc2d1	vulkanický
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
je	být	k5eAaImIp3nS	být
obsažen	obsažen	k2eAgInSc1d1	obsažen
v	v	k7c6	v
atmosférách	atmosféra	k1gFnPc6	atmosféra
velkých	velký	k2eAgFnPc2d1	velká
planet	planeta	k1gFnPc2	planeta
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
Uranu	Uran	k1gInSc2	Uran
a	a	k8xC	a
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Saturnova	Saturnův	k2eAgInSc2d1	Saturnův
měsíce	měsíc	k1gInSc2	měsíc
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
<s>
Nalezen	nalézt	k5eAaBmNgInS	nalézt
byl	být	k5eAaImAgInS	být
i	i	k9	i
v	v	k7c6	v
kometách	kometa	k1gFnPc6	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
mezihvězdném	mezihvězdný	k2eAgInSc6d1	mezihvězdný
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
83	[number]	k4	83
%	%	kIx~	%
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
amoniaku	amoniak	k1gInSc2	amoniak
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
hnojiva	hnojivo	k1gNnPc4	hnojivo
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
solí	sůl	k1gFnPc2	sůl
nebo	nebo	k8xC	nebo
roztoků	roztok	k1gInPc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
amoniaku	amoniak	k1gInSc2	amoniak
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
%	%	kIx~	%
energie	energie	k1gFnPc1	energie
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
lidstvem	lidstvo	k1gNnSc7	lidstvo
–	–	k?	–
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc7d1	významná
složkou	složka	k1gFnSc7	složka
světové	světový	k2eAgFnSc2d1	světová
spotřeby	spotřeba	k1gFnSc2	spotřeba
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
roztoku	roztok	k1gInSc2	roztok
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
složka	složka	k1gFnSc1	složka
čisticích	čisticí	k2eAgInPc2d1	čisticí
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
lesku	lesk	k1gInSc2	lesk
beze	beze	k7c2	beze
šmouh	šmouha	k1gFnPc2	šmouha
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
čištění	čištění	k1gNnSc4	čištění
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
porcelánu	porcelán	k1gInSc2	porcelán
a	a	k8xC	a
nerezavějící	rezavějící	k2eNgFnSc2d1	nerezavějící
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
také	také	k9	také
v	v	k7c6	v
čističích	čistič	k1gInPc6	čistič
pro	pro	k7c4	pro
trouby	trouba	k1gFnPc4	trouba
<g/>
,	,	kIx,	,
grily	gril	k1gInPc1	gril
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
napečené	napečený	k2eAgFnPc4d1	napečená
nečistoty	nečistota	k1gFnPc4	nečistota
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
amoniaku	amoniak	k1gInSc2	amoniak
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
roztocích	roztok	k1gInPc6	roztok
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
je	být	k5eAaImIp3nS	být
přímým	přímý	k2eAgInSc7d1	přímý
nebo	nebo	k8xC	nebo
nepřímým	přímý	k2eNgInSc7d1	nepřímý
prekurzorem	prekurzor	k1gInSc7	prekurzor
většiny	většina	k1gFnSc2	většina
dusíkatých	dusíkatý	k2eAgFnPc2d1	dusíkatá
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc1	všechen
syntetické	syntetický	k2eAgFnPc1d1	syntetická
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
anorganické	anorganický	k2eAgFnPc1d1	anorganická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
dusíku	dusík	k1gInSc2	dusík
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
z	z	k7c2	z
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
produktem	produkt	k1gInSc7	produkt
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
Ostwaldovým	Ostwaldův	k2eAgInSc7d1	Ostwaldův
procesem	proces	k1gInSc7	proces
–	–	k?	–
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
oxidací	oxidace	k1gFnSc7	oxidace
amoniaku	amoniak	k1gInSc2	amoniak
na	na	k7c6	na
katalyzátoru	katalyzátor	k1gInSc6	katalyzátor
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
850	[number]	k4	850
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
900	[number]	k4	900
kPa	kPa	k?	kPa
<g/>
.	.	kIx.	.
</s>
<s>
Meziproduktem	meziprodukt	k1gInSc7	meziprodukt
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc1	oxid
dusnatý	dusnatý	k2eAgInSc1d1	dusnatý
<g/>
:	:	kIx,	:
NH3	NH3	k1gFnSc1	NH3
+	+	kIx~	+
2	[number]	k4	2
O2	O2	k1gMnSc1	O2
→	→	k?	→
HNO3	HNO3	k1gMnSc1	HNO3
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
Kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
,	,	kIx,	,
výbušnin	výbušnina	k1gFnPc2	výbušnina
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1	termodynamická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
amoniaku	amoniak	k1gInSc2	amoniak
ho	on	k3xPp3gNnSc4	on
učinily	učinit	k5eAaPmAgInP	učinit
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
chladiv	chladivo	k1gNnPc2	chladivo
běžně	běžně	k6eAd1	běžně
používaných	používaný	k2eAgMnPc2d1	používaný
před	před	k7c7	před
objevem	objev	k1gInSc7	objev
dichlordifluormethanů	dichlordifluormethan	k1gInPc2	dichlordifluormethan
známých	známý	k2eAgInPc2d1	známý
jako	jako	k8xC	jako
freony	freon	k1gInPc7	freon
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
však	však	k9	však
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
toxicita	toxicita	k1gFnSc1	toxicita
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Bezvodý	bezvodý	k2eAgInSc1d1	bezvodý
amoniak	amoniak	k1gInSc1	amoniak
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
široce	široko	k6eAd1	široko
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
chladicích	chladicí	k2eAgInPc6d1	chladicí
systémech	systém	k1gInPc6	systém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
zimních	zimní	k2eAgInPc6d1	zimní
stadionech	stadion	k1gInPc6	stadion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
účinnosti	účinnost	k1gFnSc3	účinnost
a	a	k8xC	a
nízké	nízký	k2eAgFnSc3d1	nízká
ceně	cena	k1gFnSc3	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kompresorových	kompresorový	k2eAgInPc6d1	kompresorový
spotřebitelských	spotřebitelský	k2eAgInPc6d1	spotřebitelský
výrobcích	výrobek	k1gInPc6	výrobek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
chladničkách	chladnička	k1gFnPc6	chladnička
<g/>
)	)	kIx)	)
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
zmíněné	zmíněný	k2eAgFnSc3d1	zmíněná
toxicitě	toxicita	k1gFnSc3	toxicita
používá	používat	k5eAaImIp3nS	používat
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
uplatnění	uplatnění	k1gNnSc1	uplatnění
však	však	k9	však
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
absorpčních	absorpční	k2eAgFnPc6d1	absorpční
chladničkách	chladnička	k1gFnPc6	chladnička
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc4	amoniak
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
odstraňování	odstraňování	k1gNnSc3	odstraňování
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
spalováním	spalování	k1gNnSc7	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
produkt	produkt	k1gInSc1	produkt
se	se	k3xPyFc4	se
převádí	převádět	k5eAaImIp3nS	převádět
na	na	k7c4	na
síran	síran	k1gInSc4	síran
amonný	amonný	k2eAgMnSc1d1	amonný
používaný	používaný	k2eAgMnSc1d1	používaný
jako	jako	k8xS	jako
hnojivo	hnojivo	k1gNnSc1	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
také	také	k9	také
neutralizuje	neutralizovat	k5eAaBmIp3nS	neutralizovat
oxidy	oxid	k1gInPc4	oxid
dusíku	dusík	k1gInSc2	dusík
(	(	kIx(	(
<g/>
NOx	noxa	k1gFnPc2	noxa
<g/>
)	)	kIx)	)
produkované	produkovaný	k2eAgInPc1d1	produkovaný
vznětovými	vznětový	k2eAgInPc7d1	vznětový
motory	motor	k1gInPc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
SCR	SCR	kA	SCR
(	(	kIx(	(
<g/>
selective	selectiv	k1gInSc5	selectiv
catalytic	catalytice	k1gFnPc2	catalytice
reduction	reduction	k1gInSc1	reduction
<g/>
,	,	kIx,	,
selektivní	selektivní	k2eAgFnSc1d1	selektivní
katalytická	katalytický	k2eAgFnSc1d1	katalytická
redukce	redukce	k1gFnSc1	redukce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
vanadového	vanadový	k2eAgInSc2d1	vanadový
katalyzátoru	katalyzátor	k1gInSc2	katalyzátor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
amoniak	amoniak	k1gInSc1	amoniak
používal	používat	k5eAaImAgInS	používat
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
jako	jako	k8xC	jako
palivo	palivo	k1gNnSc4	palivo
pro	pro	k7c4	pro
autobusy	autobus	k1gInPc4	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
využíval	využívat	k5eAaPmAgInS	využívat
do	do	k7c2	do
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
pro	pro	k7c4	pro
zařízení	zařízení	k1gNnPc4	zařízení
založená	založený	k2eAgNnPc4d1	založené
na	na	k7c6	na
sluneční	sluneční	k2eAgFnSc6d1	sluneční
energii	energie	k1gFnSc6	energie
<g/>
.	.	kIx.	.
</s>
<s>
Kapalný	kapalný	k2eAgInSc1d1	kapalný
amoniak	amoniak	k1gInSc1	amoniak
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
raketové	raketový	k2eAgNnSc4d1	raketové
palivo	palivo	k1gNnSc4	palivo
(	(	kIx(	(
<g/>
letoun	letoun	k1gInSc1	letoun
X-	X-	k1gFnSc2	X-
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nemá	mít	k5eNaImIp3nS	mít
tak	tak	k6eAd1	tak
vysokou	vysoký	k2eAgFnSc4d1	vysoká
výhřevnost	výhřevnost	k1gFnSc4	výhřevnost
jako	jako	k8xS	jako
jiná	jiný	k2eAgNnPc4d1	jiné
paliva	palivo	k1gNnPc4	palivo
<g/>
,	,	kIx,	,
nezanechává	zanechávat	k5eNaImIp3nS	zanechávat
v	v	k7c6	v
motorech	motor	k1gInPc6	motor
saze	saze	k1gFnSc1	saze
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
hustota	hustota	k1gFnSc1	hustota
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xC	jako
u	u	k7c2	u
kyslíku	kyslík	k1gInSc2	kyslík
použitého	použitý	k2eAgMnSc2d1	použitý
jako	jako	k8xC	jako
oxidant	oxidant	k1gInSc1	oxidant
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
konstrukci	konstrukce	k1gFnSc4	konstrukce
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Rovnice	rovnice	k1gFnSc1	rovnice
hoření	hoření	k1gNnSc2	hoření
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgFnSc1d1	následující
<g/>
:	:	kIx,	:
4	[number]	k4	4
NH3	NH3	k1gFnPc2	NH3
+	+	kIx~	+
7	[number]	k4	7
O2	O2	k1gFnSc2	O2
→	→	k?	→
4	[number]	k4	4
NO2	NO2	k1gFnPc2	NO2
+	+	kIx~	+
6	[number]	k4	6
H2O	H2O	k1gFnPc2	H2O
Nově	nově	k6eAd1	nově
byl	být	k5eAaImAgInS	být
amoniak	amoniak	k1gInSc1	amoniak
navržen	navrhnout	k5eAaPmNgInS	navrhnout
jako	jako	k8xC	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
fosilním	fosilní	k2eAgNnPc3d1	fosilní
palivům	palivo	k1gNnPc3	palivo
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
ve	v	k7c6	v
spalovacích	spalovací	k2eAgInPc6d1	spalovací
motorech	motor	k1gInPc6	motor
<g/>
.	.	kIx.	.
</s>
<s>
Spalné	spalný	k2eAgNnSc1d1	spalné
teplo	teplo	k1gNnSc1	teplo
amoniaku	amoniak	k1gInSc2	amoniak
je	být	k5eAaImIp3nS	být
22,5	[number]	k4	22,5
MJ	mj	kA	mj
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
oproti	oproti	k7c3	oproti
naftě	nafta	k1gFnSc3	nafta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
motorech	motor	k1gInPc6	motor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
nekondenzuje	kondenzovat	k5eNaImIp3nS	kondenzovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
výhřevností	výhřevnost	k1gFnSc7	výhřevnost
amoniaku	amoniak	k1gInSc2	amoniak
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
o	o	k7c4	o
21	[number]	k4	21
%	%	kIx~	%
menší	malý	k2eAgFnSc2d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
činí	činit	k5eAaImIp3nS	činit
spalné	spalný	k2eAgNnSc4d1	spalné
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc4	amoniak
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
v	v	k7c6	v
existujících	existující	k2eAgInPc6d1	existující
motorech	motor	k1gInPc6	motor
jen	jen	k9	jen
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
úpravami	úprava	k1gFnPc7	úprava
karburátorů	karburátor	k1gInPc2	karburátor
nebo	nebo	k8xC	nebo
vstřikování	vstřikování	k1gNnSc2	vstřikování
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
naplnění	naplnění	k1gNnSc4	naplnění
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c6	po
zvýšeném	zvýšený	k2eAgNnSc6d1	zvýšené
množství	množství	k1gNnSc6	množství
amoniaku	amoniak	k1gInSc2	amoniak
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
zvýšit	zvýšit	k5eAaPmF	zvýšit
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jde	jít	k5eAaImIp3nS	jít
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vyrobeného	vyrobený	k2eAgNnSc2d1	vyrobené
množství	množství	k1gNnSc2	množství
<g/>
)	)	kIx)	)
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
chemikálii	chemikálie	k1gFnSc4	chemikálie
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
objem	objem	k1gInSc4	objem
výroby	výroba	k1gFnSc2	výroba
tvoří	tvořit	k5eAaImIp3nS	tvořit
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
zlomek	zlomek	k1gInSc1	zlomek
využívaného	využívaný	k2eAgNnSc2d1	využívané
množství	množství	k1gNnSc2	množství
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
vyrábět	vyrábět	k5eAaImF	vyrábět
pomocí	pomoc	k1gFnSc7	pomoc
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pomocí	pomocí	k7c2	pomocí
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
uhlí	uhlí	k1gNnSc2	uhlí
nebo	nebo	k8xC	nebo
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
akumulátorových	akumulátorový	k2eAgFnPc2d1	akumulátorová
baterií	baterie	k1gFnPc2	baterie
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
v	v	k7c6	v
Rjukanu	Rjukan	k1gInSc6	Rjukan
(	(	kIx(	(
<g/>
Telemark	telemark	k1gInSc1	telemark
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
60	[number]	k4	60
MW	MW	kA	MW
dodávala	dodávat	k5eAaImAgFnS	dodávat
proud	proud	k1gInSc4	proud
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
amoniaku	amoniak	k1gInSc2	amoniak
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
vody	voda	k1gFnSc2	voda
–	–	k?	–
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
sloužila	sloužit	k5eAaImAgFnS	sloužit
výrobě	výroba	k1gFnSc3	výroba
hnojiv	hnojivo	k1gNnPc2	hnojivo
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
uhlí	uhlí	k1gNnSc2	uhlí
lze	lze	k6eAd1	lze
CO2	CO2	k1gFnSc2	CO2
snadno	snadno	k6eAd1	snadno
ukládat	ukládat	k5eAaImF	ukládat
(	(	kIx(	(
<g/>
produkty	produkt	k1gInPc7	produkt
vlastního	vlastní	k2eAgNnSc2d1	vlastní
spalování	spalování	k1gNnSc2	spalování
jsou	být	k5eAaImIp3nP	být
dusík	dusík	k1gInSc1	dusík
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
jedna	jeden	k4xCgFnSc1	jeden
kanadská	kanadský	k2eAgFnSc1d1	kanadská
firma	firma	k1gFnSc1	firma
upravila	upravit	k5eAaPmAgFnS	upravit
automobil	automobil	k1gInSc4	automobil
Chevrolet	chevrolet	k1gInSc4	chevrolet
Impala	Impala	k1gFnSc1	Impala
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
na	na	k7c4	na
amoniak	amoniak	k1gInSc4	amoniak
jako	jako	k8xC	jako
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
byly	být	k5eAaImAgInP	být
zkonstruovány	zkonstruován	k2eAgInPc1d1	zkonstruován
speciální	speciální	k2eAgInPc1d1	speciální
motory	motor	k1gInPc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
i	i	k9	i
tzv.	tzv.	kA	tzv.
čpavkový	čpavkový	k2eAgInSc1d1	čpavkový
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
uvedený	uvedený	k2eAgInSc1d1	uvedený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
uvolňování	uvolňování	k1gNnSc4	uvolňování
amoniaku	amoniak	k1gInSc2	amoniak
z	z	k7c2	z
vodného	vodný	k2eAgInSc2d1	vodný
roztoku	roztok	k1gInSc2	roztok
při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
–	–	k?	–
plynný	plynný	k2eAgInSc1d1	plynný
amoniak	amoniak	k1gInSc1	amoniak
poháněl	pohánět	k5eAaImAgInS	pohánět
pístový	pístový	k2eAgInSc1d1	pístový
stroj	stroj	k1gInSc1	stroj
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
pohlcován	pohlcovat	k5eAaImNgInS	pohlcovat
zpět	zpět	k6eAd1	zpět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
uvolňovalo	uvolňovat	k5eAaImAgNnS	uvolňovat
teplo	teplo	k1gNnSc1	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
to	ten	k3xDgNnSc1	ten
fungovalo	fungovat	k5eAaImAgNnS	fungovat
do	do	k7c2	do
vyrovnání	vyrovnání	k1gNnSc2	vyrovnání
tlaků	tlak	k1gInPc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Praktické	praktický	k2eAgNnSc1d1	praktické
nasazení	nasazení	k1gNnSc1	nasazení
ztroskotalo	ztroskotat	k5eAaPmAgNnS	ztroskotat
kvůli	kvůli	k7c3	kvůli
silnému	silný	k2eAgInSc3d1	silný
zápachu	zápach	k1gInSc3	zápach
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
amoniak	amoniak	k1gInSc1	amoniak
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
silné	silný	k2eAgNnSc1d1	silné
antiseptikum	antiseptikum	k1gNnSc1	antiseptikum
...	...	k?	...
ke	k	k7c3	k
konzervaci	konzervace	k1gFnSc3	konzervace
masového	masový	k2eAgInSc2d1	masový
extraktu	extrakt	k1gInSc2	extrakt
postačuje	postačovat	k5eAaImIp3nS	postačovat
1,4	[number]	k4	1,4
g	g	kA	g
na	na	k7c4	na
litr	litr	k1gInSc4	litr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bezvodý	bezvodý	k2eAgInSc1d1	bezvodý
amoniak	amoniak	k1gInSc1	amoniak
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
účinné	účinný	k2eAgNnSc4d1	účinné
antimikrobiální	antimikrobiální	k2eAgNnSc4d1	antimikrobiální
činidlo	činidlo	k1gNnSc4	činidlo
pro	pro	k7c4	pro
krmiva	krmivo	k1gNnPc4	krmivo
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
komerčně	komerčně	k6eAd1	komerčně
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
nebo	nebo	k8xC	nebo
eliminaci	eliminace	k1gFnSc3	eliminace
mikrobiální	mikrobiální	k2eAgFnSc2d1	mikrobiální
kontaminace	kontaminace	k1gFnSc2	kontaminace
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
oznámil	oznámit	k5eAaPmAgMnS	oznámit
The	The	k1gMnSc1	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
,	,	kIx,	,
že	že	k8xS	že
americká	americký	k2eAgFnSc1d1	americká
firma	firma	k1gFnSc1	firma
Beef	Beef	k1gInSc4	Beef
Products	Products	k1gInSc1	Products
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
hovězí	hovězí	k2eAgInSc4d1	hovězí
masový	masový	k2eAgInSc4d1	masový
separát	separát	k1gInSc4	separát
(	(	kIx(	(
<g/>
obsahující	obsahující	k2eAgFnSc4d1	obsahující
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
%	%	kIx~	%
tuku	tuk	k1gInSc2	tuk
<g/>
)	)	kIx)	)
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
libového	libový	k2eAgMnSc2d1	libový
jemně	jemně	k6eAd1	jemně
tvarovaného	tvarovaný	k2eAgNnSc2d1	tvarované
masa	maso	k1gNnSc2	maso
týdně	týdně	k6eAd1	týdně
odstraňováním	odstraňování	k1gNnSc7	odstraňování
tuku	tuk	k1gInSc2	tuk
(	(	kIx(	(
<g/>
zahříváním	zahřívání	k1gNnSc7	zahřívání
a	a	k8xC	a
centrifugací	centrifugace	k1gFnSc7	centrifugace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
dezinfikuje	dezinfikovat	k5eAaBmIp3nS	dezinfikovat
výsledný	výsledný	k2eAgInSc1d1	výsledný
produkt	produkt	k1gInSc1	produkt
amoniakem	amoniak	k1gInSc7	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
americkým	americký	k2eAgNnSc7d1	americké
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
kvalifikován	kvalifikovat	k5eAaBmNgMnS	kvalifikovat
jako	jako	k9	jako
účinný	účinný	k2eAgMnSc1d1	účinný
a	a	k8xC	a
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
studie	studie	k1gFnSc2	studie
(	(	kIx(	(
<g/>
financované	financovaný	k2eAgFnPc1d1	financovaná
Beef	Beef	k1gInSc4	Beef
Products	Productsa	k1gFnPc2	Productsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prokázala	prokázat	k5eAaPmAgFnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgNnSc4	takový
ošetření	ošetření	k1gNnSc4	ošetření
snižuje	snižovat	k5eAaImIp3nS	snižovat
výskyt	výskyt	k1gInSc1	výskyt
E.	E.	kA	E.
coli	col	k1gFnSc2	col
na	na	k7c4	na
nedetekovatelné	detekovatelný	k2eNgFnPc4d1	nedetekovatelná
úrovně	úroveň	k1gFnPc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
pátrání	pátrání	k1gNnSc1	pátrání
novinářů	novinář	k1gMnPc2	novinář
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2009	[number]	k4	2009
odhalilo	odhalit	k5eAaPmAgNnS	odhalit
obavy	obava	k1gFnPc4	obava
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
stížnosti	stížnost	k1gFnSc3	stížnost
spotřebitelů	spotřebitel	k1gMnPc2	spotřebitel
na	na	k7c4	na
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
pach	pach	k1gInSc4	pach
masa	maso	k1gNnSc2	maso
ošetřeného	ošetřený	k2eAgNnSc2d1	ošetřené
optimálními	optimální	k2eAgFnPc7d1	optimální
úrovněmi	úroveň	k1gFnPc7	úroveň
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
účinkem	účinek	k1gInSc7	účinek
amoniaku	amoniak	k1gInSc2	amoniak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jím	jíst	k5eAaImIp1nS	jíst
zpracované	zpracovaný	k2eAgNnSc4d1	zpracované
maso	maso	k1gNnSc4	maso
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
růžovou	růžový	k2eAgFnSc4d1	růžová
<g/>
,	,	kIx,	,
zdánlivě	zdánlivě	k6eAd1	zdánlivě
zdravě	zdravě	k6eAd1	zdravě
vypadající	vypadající	k2eAgFnSc4d1	vypadající
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zkažené	zkažený	k2eAgFnSc6d1	zkažená
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
nachází	nacházet	k5eAaImIp3nS	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
silových	silový	k2eAgFnPc6d1	silová
disciplinách	disciplina	k1gFnPc6	disciplina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vzpírání	vzpírání	k1gNnSc4	vzpírání
nebo	nebo	k8xC	nebo
silový	silový	k2eAgInSc4d1	silový
trojboj	trojboj	k1gInSc4	trojboj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
respirační	respirační	k2eAgNnSc1d1	respirační
stimulans	stimulans	k1gNnSc1	stimulans
<g/>
.	.	kIx.	.
</s>
<s>
Kapalný	kapalný	k2eAgInSc1d1	kapalný
amoniak	amoniak	k1gInSc1	amoniak
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
ošetření	ošetření	k1gNnSc3	ošetření
materiálů	materiál	k1gInPc2	materiál
z	z	k7c2	z
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
získat	získat	k5eAaPmF	získat
stejné	stejný	k2eAgFnPc4d1	stejná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xS	jako
při	při	k7c6	při
merceraci	mercerace	k1gFnSc6	mercerace
<g/>
.	.	kIx.	.
</s>
<s>
Specifickou	specifický	k2eAgFnSc7d1	specifická
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
také	také	k9	také
předsrážení	předsrážení	k1gNnSc1	předsrážení
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
běžné	běžný	k2eAgFnSc6d1	běžná
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
je	být	k5eAaImIp3nS	být
amoniak	amoniak	k1gInSc1	amoniak
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
nadnáší	nadnášet	k5eAaImIp3nS	nadnášet
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
60	[number]	k4	60
%	%	kIx~	%
vodíku	vodík	k1gInSc2	vodík
nebo	nebo	k8xC	nebo
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
balonů	balon	k1gInPc2	balon
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
relativně	relativně	k6eAd1	relativně
vysoké	vysoký	k2eAgFnSc3d1	vysoká
teplotě	teplota	k1gFnSc3	teplota
varu	var	k1gInSc2	var
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
heliem	helium	k1gNnSc7	helium
a	a	k8xC	a
vodíkem	vodík	k1gInSc7	vodík
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
amoniak	amoniak	k1gInSc1	amoniak
případně	případně	k6eAd1	případně
ochladit	ochladit	k5eAaPmF	ochladit
a	a	k8xC	a
zkapalnit	zkapalnit	k5eAaPmF	zkapalnit
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
vzdušném	vzdušný	k2eAgInSc6d1	vzdušný
prostředku	prostředek	k1gInSc6	prostředek
a	a	k8xC	a
snížit	snížit	k5eAaPmF	snížit
tak	tak	k6eAd1	tak
vztlak	vztlak	k1gInSc4	vztlak
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
naopak	naopak	k6eAd1	naopak
přeměnit	přeměnit	k5eAaPmF	přeměnit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
plyn	plyn	k1gInSc4	plyn
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
vztlaku	vztlak	k1gInSc2	vztlak
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
používal	používat	k5eAaImAgMnS	používat
ke	k	k7c3	k
ztmavování	ztmavování	k1gNnSc3	ztmavování
dřeva	dřevo	k1gNnSc2	dřevo
z	z	k7c2	z
bílého	bílý	k2eAgInSc2d1	bílý
dubu	dub	k1gInSc2	dub
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
nábytku	nábytek	k1gInSc2	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Plynný	plynný	k2eAgInSc1d1	plynný
amoniak	amoniak	k1gInSc1	amoniak
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
přírodními	přírodní	k2eAgInPc7d1	přírodní
taniny	tanin	k1gInPc7	tanin
ve	v	k7c6	v
dřevě	dřevo	k1gNnSc6	dřevo
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
změnu	změna	k1gFnSc4	změna
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Toxicita	toxicita	k1gFnSc1	toxicita
roztoků	roztok	k1gInPc2	roztok
amoniaku	amoniak	k1gInSc2	amoniak
obvykle	obvykle	k6eAd1	obvykle
nepůsobí	působit	k5eNaImIp3nS	působit
problémy	problém	k1gInPc4	problém
člověku	člověk	k1gMnSc3	člověk
a	a	k8xC	a
jiným	jiný	k1gMnPc3	jiný
savcům	savec	k1gMnPc3	savec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
specifický	specifický	k2eAgInSc4d1	specifický
mechanismus	mechanismus	k1gInSc4	mechanismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
amoniak	amoniak	k1gInSc1	amoniak
eliminovat	eliminovat	k5eAaBmF	eliminovat
<g/>
.	.	kIx.	.
</s>
<s>
Eliminace	eliminace	k1gFnSc1	eliminace
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
konverzi	konverze	k1gFnSc6	konverze
na	na	k7c4	na
karbamoylfosfát	karbamoylfosfát	k1gInSc4	karbamoylfosfát
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
enzymu	enzym	k1gInSc2	enzym
karbamoylfosfátsyntázy	karbamoylfosfátsyntáza	k1gFnSc2	karbamoylfosfátsyntáza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
následně	následně	k6eAd1	následně
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
močovinového	močovinový	k2eAgInSc2d1	močovinový
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přeměněn	přeměnit	k5eAaPmNgInS	přeměnit
na	na	k7c4	na
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
nebo	nebo	k8xC	nebo
vyloučen	vyloučen	k2eAgMnSc1d1	vyloučen
močí	močit	k5eAaImIp3nS	močit
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
a	a	k8xC	a
obojživelníci	obojživelník	k1gMnPc1	obojživelník
však	však	k9	však
tento	tento	k3xDgInSc4	tento
mechanismus	mechanismus	k1gInSc4	mechanismus
postrádají	postrádat	k5eAaImIp3nP	postrádat
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
obvykle	obvykle	k6eAd1	obvykle
amoniak	amoniak	k1gInSc4	amoniak
pouze	pouze	k6eAd1	pouze
přímo	přímo	k6eAd1	přímo
vylučovat	vylučovat	k5eAaImF	vylučovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
I	i	k9	i
v	v	k7c6	v
nízkých	nízký	k2eAgFnPc6d1	nízká
koncentracích	koncentrace	k1gFnPc6	koncentrace
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
amoniak	amoniak	k1gInSc4	amoniak
velmi	velmi	k6eAd1	velmi
toxický	toxický	k2eAgInSc4d1	toxický
pro	pro	k7c4	pro
vodní	vodní	k2eAgMnPc4d1	vodní
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Směrnicí	směrnice	k1gFnSc7	směrnice
Rady	rada	k1gFnSc2	rada
67	[number]	k4	67
<g/>
/	/	kIx~	/
<g/>
548	[number]	k4	548
<g/>
/	/	kIx~	/
<g/>
EHS	EHS	kA	EHS
klasifikován	klasifikován	k2eAgMnSc1d1	klasifikován
jako	jako	k8xS	jako
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
úřad	úřad	k1gInSc1	úřad
OSHA	OSHA	kA	OSHA
stanovil	stanovit	k5eAaPmAgInS	stanovit
patnáctiminutový	patnáctiminutový	k2eAgInSc1d1	patnáctiminutový
expoziční	expoziční	k2eAgInSc1d1	expoziční
limit	limit	k1gInSc1	limit
pro	pro	k7c4	pro
plynný	plynný	k2eAgInSc4d1	plynný
amoniak	amoniak	k1gInSc4	amoniak
na	na	k7c4	na
35	[number]	k4	35
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
objemově	objemově	k6eAd1	objemově
<g/>
)	)	kIx)	)
a	a	k8xC	a
osmihodinový	osmihodinový	k2eAgInSc1d1	osmihodinový
limit	limit	k1gInSc1	limit
na	na	k7c4	na
25	[number]	k4	25
ppm	ppm	k?	ppm
<g/>
.	.	kIx.	.
</s>
<s>
Agentura	agentura	k1gFnSc1	agentura
National	National	k1gFnSc2	National
Institute	institut	k1gInSc5	institut
for	forum	k1gNnPc2	forum
Occupational	Occupational	k1gFnPc7	Occupational
Safety	Safeta	k1gFnSc2	Safeta
and	and	k?	and
Health	Health	k1gInSc1	Health
snížila	snížit	k5eAaPmAgFnS	snížit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nedávné	dávný	k2eNgFnSc2d1	nedávná
konzervativnější	konzervativní	k2eAgFnSc2d2	konzervativnější
interpretace	interpretace	k1gFnSc2	interpretace
původního	původní	k2eAgInSc2d1	původní
výzkumu	výzkum	k1gInSc2	výzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
koncentraci	koncentrace	k1gFnSc3	koncentrace
IDLH	IDLH	kA	IDLH
(	(	kIx(	(
<g/>
bezprostředně	bezprostředně	k6eAd1	bezprostředně
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
pro	pro	k7c4	pro
život	život	k1gInSc4	život
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
)	)	kIx)	)
z	z	k7c2	z
500	[number]	k4	500
na	na	k7c4	na
300	[number]	k4	300
ppm	ppm	k?	ppm
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
koncentraci	koncentrace	k1gFnSc4	koncentrace
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zdravý	zdravý	k2eAgMnSc1d1	zdravý
pracovník	pracovník	k1gMnSc1	pracovník
vystaven	vystavit	k5eAaPmNgMnS	vystavit
po	po	k7c4	po
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
nevratné	vratný	k2eNgFnPc4d1	nevratná
škody	škoda	k1gFnPc4	škoda
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
organizace	organizace	k1gFnPc1	organizace
či	či	k8xC	či
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc1d1	různý
limity	limit	k1gInPc1	limit
expozic	expozice	k1gFnPc2	expozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
platí	platit	k5eAaImIp3nP	platit
limity	limit	k1gInPc1	limit
PEL	pel	k1gInSc1	pel
14	[number]	k4	14
mg	mg	kA	mg
<g/>
.	.	kIx.	.
<g/>
m	m	kA	m
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
a	a	k8xC	a
NPK	NPK	kA	NPK
<g/>
–	–	k?	–
<g/>
P	P	kA	P
36	[number]	k4	36
mg	mg	kA	mg
<g/>
.	.	kIx.	.
<g/>
m	m	kA	m
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
při	při	k7c6	při
emisích	emise	k1gFnPc6	emise
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
nad	nad	k7c4	nad
10000	[number]	k4	10000
kg	kg	kA	kg
ročně	ročně	k6eAd1	ročně
platí	platit	k5eAaImIp3nS	platit
povinnost	povinnost	k1gFnSc4	povinnost
hlášení	hlášení	k1gNnSc2	hlášení
do	do	k7c2	do
Integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
registru	registr	k1gInSc2	registr
znečišťování	znečišťování	k1gNnSc2	znečišťování
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
znečištění	znečištění	k1gNnSc4	znečištění
amoniakem	amoniak	k1gInSc7	amoniak
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
má	mít	k5eAaImIp3nS	mít
ostrý	ostrý	k2eAgInSc1d1	ostrý
<g/>
,	,	kIx,	,
dráždivý	dráždivý	k2eAgInSc1d1	dráždivý
<g/>
,	,	kIx,	,
štiplavý	štiplavý	k2eAgInSc1d1	štiplavý
zápach	zápach	k1gInSc1	zápach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
varuje	varovat	k5eAaImIp3nS	varovat
před	před	k7c7	před
potenciálně	potenciálně	k6eAd1	potenciálně
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
expozicí	expozice	k1gFnSc7	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Průměrných	průměrný	k2eAgInPc2d1	průměrný
práh	práh	k1gInSc1	práh
vnímání	vnímání	k1gNnSc2	vnímání
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
ppm	ppm	k?	ppm
<g/>
,	,	kIx,	,
dostatečně	dostatečně	k6eAd1	dostatečně
nižší	nízký	k2eAgFnPc1d2	nižší
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
nebo	nebo	k8xC	nebo
škodlivé	škodlivý	k2eAgFnPc1d1	škodlivá
koncentrace	koncentrace	k1gFnPc1	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Expozice	expozice	k1gFnSc1	expozice
velmi	velmi	k6eAd1	velmi
vysokým	vysoký	k2eAgFnPc3d1	vysoká
koncentracím	koncentrace	k1gFnPc3	koncentrace
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Amonné	amonný	k2eAgFnPc1d1	amonná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgFnP	mít
přijít	přijít	k5eAaPmF	přijít
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
se	s	k7c7	s
zásadami	zásada	k1gFnPc7	zásada
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
uvolnit	uvolnit	k5eAaPmF	uvolnit
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
dávky	dávka	k1gFnPc4	dávka
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
