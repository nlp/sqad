<s>
Bejrút	Bejrút	k1gInSc1	Bejrút
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ب	ب	k?	ب
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Beyrouth	Beyrouth	k1gInSc1	Beyrouth
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
největším	veliký	k2eAgMnSc7d3	veliký
i	i	k8xC	i
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
2,1	[number]	k4	2,1
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
zněl	znět	k5eAaImAgInS	znět
Bê	Bê	k1gFnSc4	Bê
-	-	kIx~	-
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Studně	studně	k6eAd1	studně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
mu	on	k3xPp3gMnSc3	on
dali	dát	k5eAaPmAgMnP	dát
Féničané	Féničan	k1gMnPc1	Féničan
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
byl	být	k5eAaImAgMnS	být
také	také	k9	také
znám	znám	k2eAgMnSc1d1	znám
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Berytus	Berytus	k1gMnSc1	Berytus
<g/>
,	,	kIx,	,
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
přišli	přijít	k5eAaPmAgMnP	přijít
archeologové	archeolog	k1gMnPc1	archeolog
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
140	[number]	k4	140
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
město	město	k1gNnSc4	město
obsadil	obsadit	k5eAaPmAgMnS	obsadit
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zničil	zničit	k5eAaPmAgInS	zničit
Diodotus	Diodotus	k1gInSc1	Diodotus
Tryfon	Tryfon	k1gInSc1	Tryfon
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
Seleukovské	Seleukovský	k2eAgFnSc2d1	Seleukovská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
přestavěno	přestavět	k5eAaPmNgNnS	přestavět
v	v	k7c6	v
helénském	helénský	k2eAgInSc6d1	helénský
stylu	styl	k1gInSc6	styl
a	a	k8xC	a
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Laodicea	Laodiceus	k1gMnSc4	Laodiceus
<g/>
,	,	kIx,	,
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
seleukovské	seleukovský	k2eAgFnSc2d1	seleukovská
královny	královna	k1gFnSc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
právě	právě	k9	právě
tohoto	tento	k3xDgNnSc2	tento
starého	staré	k1gNnSc2	staré
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Bejrútu	Bejrút	k1gInSc2	Bejrút
nacházejí	nacházet	k5eAaImIp3nP	nacházet
historická	historický	k2eAgNnPc1d1	historické
naleziště	naleziště	k1gNnPc1	naleziště
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgNnPc6	který
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
pracovat	pracovat	k5eAaImF	pracovat
až	až	k9	až
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
průzkumu	průzkum	k1gInSc3	průzkum
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
současné	současný	k2eAgFnPc1d1	současná
ulice	ulice	k1gFnPc1	ulice
kopírují	kopírovat	k5eAaImIp3nP	kopírovat
tvar	tvar	k1gInSc4	tvar
těch	ten	k3xDgFnPc2	ten
starověkých	starověký	k2eAgFnPc2d1	starověká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
existovala	existovat	k5eAaImAgFnS	existovat
velmi	velmi	k6eAd1	velmi
významná	významný	k2eAgFnSc1d1	významná
právnická	právnický	k2eAgFnSc1d1	právnická
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
císaře	císař	k1gMnSc2	císař
Justiniána	Justinián	k1gMnSc2	Justinián
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Byzantské	byzantský	k2eAgFnSc6d1	byzantská
říši	říš	k1gFnSc6	říš
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
právnických	právnický	k2eAgFnPc2d1	právnická
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
553	[number]	k4	553
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
o	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
20	[number]	k4	20
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
přestěhována	přestěhovat	k5eAaPmNgFnS	přestěhovat
do	do	k7c2	do
Sidonu	Sidon	k1gInSc2	Sidon
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Beirút	Beirút	k1gInSc4	Beirút
postihlo	postihnout	k5eAaPmAgNnS	postihnout
silné	silný	k2eAgNnSc1d1	silné
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
635	[number]	k4	635
padlo	padnout	k5eAaPmAgNnS	padnout
město	město	k1gNnSc1	město
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Arabům	Arab	k1gMnPc3	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
jako	jako	k8xS	jako
centrum	centrum	k1gNnSc4	centrum
východního	východní	k2eAgNnSc2d1	východní
středomoří	středomoří	k1gNnSc2	středomoří
nezískalo	získat	k5eNaPmAgNnS	získat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
totiž	totiž	k9	totiž
zastíněno	zastíněn	k2eAgNnSc1d1	zastíněno
městem	město	k1gNnSc7	město
Akko	Akko	k1gMnSc1	Akko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
rozvoj	rozvoj	k1gInSc4	rozvoj
byl	být	k5eAaImAgInS	být
větší	veliký	k2eAgInSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1110	[number]	k4	1110
a	a	k8xC	a
1291	[number]	k4	1291
Bejrút	Bejrút	k1gInSc1	Bejrút
obsadila	obsadit	k5eAaPmAgFnS	obsadit
vojska	vojsko	k1gNnPc1	vojsko
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
drúzští	drúzský	k2eAgMnPc1d1	drúzský
emírové	emír	k1gMnPc1	emír
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
byl	být	k5eAaImAgInS	být
Fakr	Fakr	k1gInSc4	Fakr
ed-Din	ed-Din	k2eAgMnSc1d1	ed-Din
Maan	Maan	k1gMnSc1	Maan
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
postavil	postavit	k5eAaPmAgMnS	postavit
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
silné	silný	k2eAgNnSc1d1	silné
opevnění	opevnění	k1gNnSc1	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
se	se	k3xPyFc4	se
městu	město	k1gNnSc3	město
podařilo	podařit	k5eAaPmAgNnS	podařit
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
centrem	centr	k1gInSc7	centr
Východního	východní	k2eAgNnSc2d1	východní
středomoří	středomoří	k1gNnSc2	středomoří
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
Akku	Akkus	k1gInSc6	Akkus
propukly	propuknout	k5eAaPmAgInP	propuknout
nepokoje	nepokoj	k1gInPc1	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Bejrút	Bejrút	k1gInSc1	Bejrút
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
různých	různý	k2eAgInPc2d1	různý
pašů	paš	k1gInPc2	paš
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
prudce	prudko	k6eAd1	prudko
klesl	klesnout	k5eAaPmAgInS	klesnout
<g/>
,	,	kIx,	,
z	z	k7c2	z
kvetoucího	kvetoucí	k2eAgNnSc2d1	kvetoucí
obchodního	obchodní	k2eAgNnSc2d1	obchodní
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
městečko	městečko	k1gNnSc1	městečko
s	s	k7c7	s
pouhými	pouhý	k2eAgInPc7d1	pouhý
10	[number]	k4	10
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
rok	rok	k1gInSc4	rok
1888	[number]	k4	1888
přinesl	přinést	k5eAaPmAgInS	přinést
zvrat	zvrat	k1gInSc4	zvrat
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
centrem	centrum	k1gNnSc7	centrum
Vilájetu	Vilájet	k1gInSc2	Vilájet
Sýrie	Sýrie	k1gFnSc2	Sýrie
(	(	kIx(	(
<g/>
turecké	turecký	k2eAgFnSc2d1	turecká
Syrské	syrský	k2eAgFnSc2d1	Syrská
provincie	provincie	k1gFnSc2	provincie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
i	i	k8xC	i
obchodní	obchodní	k2eAgInSc4d1	obchodní
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Spojenými	spojený	k2eAgInPc7d1	spojený
Státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
masakru	masakr	k1gInSc6	masakr
křesťanů	křesťan	k1gMnPc2	křesťan
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
tu	tu	k6eAd1	tu
byla	být	k5eAaImAgFnS	být
zintenzivněna	zintenzivnit	k5eAaPmNgFnS	zintenzivnit
činnost	činnost	k1gFnSc1	činnost
západních	západní	k2eAgMnPc2d1	západní
misionářů	misionář	k1gMnPc2	misionář
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
obrátit	obrátit	k5eAaPmF	obrátit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
další	další	k2eAgFnSc2d1	další
místní	místní	k2eAgFnSc2d1	místní
obyvatele	obyvatel	k1gMnSc2	obyvatel
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediné	k1gNnSc1	jediné
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
práce	práce	k1gFnSc2	práce
amerických	americký	k2eAgMnPc2d1	americký
misionářů	misionář	k1gMnPc2	misionář
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
ujalo	ujmout	k5eAaPmAgNnS	ujmout
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Syrská	syrský	k2eAgFnSc1d1	Syrská
protestantská	protestantský	k2eAgFnSc1d1	protestantská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
v	v	k7c4	v
Americkou	americký	k2eAgFnSc4d1	americká
Bejrútskou	bejrútský	k2eAgFnSc4d1	bejrútská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
školám	škola	k1gFnPc3	škola
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
arabská	arabský	k2eAgFnSc1d1	arabská
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Angličtí	anglický	k2eAgMnPc1d1	anglický
a	a	k8xC	a
francouzští	francouzský	k2eAgMnPc1d1	francouzský
inženýři	inženýr	k1gMnPc1	inženýr
zde	zde	k6eAd1	zde
stavěli	stavět	k5eAaImAgMnP	stavět
moderní	moderní	k2eAgFnPc4d1	moderní
stavby	stavba	k1gFnPc4	stavba
(	(	kIx(	(
<g/>
přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
železnice	železnice	k1gFnSc1	železnice
Bejrút	Bejrút	k1gInSc1	Bejrút
-	-	kIx~	-
Damašek	Damašek	k1gInSc1	Damašek
-	-	kIx~	-
Halab	Halab	k1gInSc1	Halab
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zboží	zboží	k1gNnSc1	zboží
se	se	k3xPyFc4	se
vyváželo	vyvážet	k5eAaImAgNnS	vyvážet
většinou	většinou	k6eAd1	většinou
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
postupně	postupně	k6eAd1	postupně
rostl	růst	k5eAaImAgInS	růst
francouzský	francouzský	k2eAgInSc4d1	francouzský
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
města	město	k1gNnSc2	město
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
velmi	velmi	k6eAd1	velmi
podporovali	podporovat	k5eAaImAgMnP	podporovat
komunitu	komunita	k1gFnSc4	komunita
maronitských	maronitský	k2eAgMnPc2d1	maronitský
křesťanů	křesťan	k1gMnPc2	křesťan
a	a	k8xC	a
zajistili	zajistit	k5eAaPmAgMnP	zajistit
mírný	mírný	k2eAgInSc4d1	mírný
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Libanon	Libanon	k1gInSc4	Libanon
nezávislost	nezávislost	k1gFnSc4	nezávislost
s	s	k7c7	s
Bejrútem	Bejrút	k1gInSc7	Bejrút
jako	jako	k8xS	jako
svým	svůj	k3xOyFgNnSc7	svůj
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
městu	město	k1gNnSc3	město
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
centrem	centr	k1gInSc7	centr
arabské	arabský	k2eAgFnSc2d1	arabská
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
turismu	turismus	k1gInSc2	turismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
s	s	k7c7	s
muslimy	muslim	k1gMnPc7	muslim
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Bejrútu	Bejrút	k1gInSc2	Bejrút
oddělili	oddělit	k5eAaPmAgMnP	oddělit
Zelenou	zelený	k2eAgFnSc4d1	zelená
linii	linie	k1gFnSc4	linie
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
přes	přes	k7c4	přes
náměstí	náměstí	k1gNnSc4	náměstí
Mučedníků	mučedník	k1gMnPc2	mučedník
(	(	kIx(	(
<g/>
Place	plac	k1gInSc6	plac
des	des	k1gNnSc4	des
Martyrs	Martyrsa	k1gFnPc2	Martyrsa
<g/>
)	)	kIx)	)
<g/>
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
pomyslně	pomyslně	k6eAd1	pomyslně
funguje	fungovat	k5eAaImIp3nS	fungovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
prakticky	prakticky	k6eAd1	prakticky
zničeno	zničit	k5eAaPmNgNnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
byly	být	k5eAaImAgFnP	být
sutiny	sutina	k1gFnPc1	sutina
obchodních	obchodní	k2eAgMnPc2d1	obchodní
domů	dům	k1gInPc2	dům
a	a	k8xC	a
administrativních	administrativní	k2eAgFnPc2d1	administrativní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
20	[number]	k4	20
let	léto	k1gNnPc2	léto
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
jsou	být	k5eAaImIp3nP	být
rány	rána	k1gFnPc4	rána
stále	stále	k6eAd1	stále
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
opravené	opravený	k2eAgInPc1d1	opravený
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
libanonské	libanonský	k2eAgFnSc6d1	libanonská
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgFnP	být
ale	ale	k8xC	ale
Izraelem	Izrael	k1gInSc7	Izrael
bombardovány	bombardován	k2eAgFnPc1d1	bombardována
jižní	jižní	k2eAgFnPc1d1	jižní
čtvrtě	čtvrt	k1gFnPc1	čtvrt
ovládané	ovládaný	k2eAgFnSc2d1	ovládaná
Hizballáhem	Hizballáh	k1gInSc7	Hizballáh
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
taky	taky	k9	taky
atentáty	atentát	k1gInPc1	atentát
na	na	k7c4	na
významné	významný	k2eAgFnPc4d1	významná
politické	politický	k2eAgFnPc4d1	politická
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
nejznámější	známý	k2eAgMnSc1d3	nejznámější
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
-	-	kIx~	-
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
bývalého	bývalý	k2eAgMnSc4d1	bývalý
premiéra	premiér	k1gMnSc4	premiér
Rafíka	Rafík	k1gMnSc4	Rafík
Harírího	Harírí	k1gMnSc4	Harírí
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Bejrútu	Bejrút	k1gInSc6	Bejrút
u	u	k7c2	u
hotelu	hotel	k1gInSc2	hotel
St.	st.	kA	st.
George	Georg	k1gMnSc2	Georg
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
akt	akt	k1gInSc4	akt
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
Cedrové	cedrový	k2eAgFnSc6d1	cedrová
revoluci	revoluce	k1gFnSc6	revoluce
a	a	k8xC	a
stažení	stažení	k1gNnSc6	stažení
Syrských	syrský	k2eAgNnPc2d1	syrské
vojsk	vojsko	k1gNnPc2	vojsko
z	z	k7c2	z
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Univerzity	univerzita	k1gFnSc2	univerzita
Americká	americký	k2eAgFnSc1d1	americká
Bejrútská	bejrútský	k2eAgFnSc1d1	bejrútská
univerzita	univerzita	k1gFnSc1	univerzita
Americká	americký	k2eAgFnSc1d1	americká
Libanonská	libanonský	k2eAgFnSc1d1	libanonská
univerzita	univerzita	k1gFnSc1	univerzita
Univerzita	univerzita	k1gFnSc1	univerzita
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc4	Josef
Kasina	kasino	k1gNnSc2	kasino
Casino	Casin	k2eAgNnSc4d1	Casino
du	du	k?	du
Liban	Liban	k1gInSc4	Liban
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
Brest	Brest	k1gInSc1	Brest
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Dubaj	Dubaj	k1gFnSc1	Dubaj
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
arabské	arabský	k2eAgInPc1d1	arabský
emiráty	emirát	k1gInPc1	emirát
Štrasburk	Štrasburk	k1gInSc4	Štrasburk
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Jerevan	Jerevan	k1gMnSc1	Jerevan
<g/>
,	,	kIx,	,
Arménie	Arménie	k1gFnSc1	Arménie
Karáčí	Karáčí	k1gNnSc2	Karáčí
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc4	Pákistán
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
Sã	Sã	k1gFnSc2	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
Damašek	Damašek	k1gInSc1	Damašek
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Québec	Québec	k1gInSc1	Québec
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Bagdád	Bagdád	k1gInSc1	Bagdád
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
Ammán	Ammán	k1gInSc1	Ammán
<g/>
,	,	kIx,	,
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
<g/>
,	,	kIx,	,
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
Nikósie	Nikósie	k1gFnSc1	Nikósie
<g/>
,	,	kIx,	,
Kypr	Kypr	k1gInSc1	Kypr
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
Split	Split	k1gInSc1	Split
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
Bogotá	Bogotá	k1gFnPc2	Bogotá
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
Isfahán	Isfahán	k1gInSc1	Isfahán
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
Bejrútské	bejrútský	k2eAgFnSc2d1	bejrútská
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
Rafíka	Rafík	k1gMnSc2	Rafík
Harírího	Harírí	k1gMnSc2	Harírí
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bejrút	Bejrút	k1gInSc1	Bejrút
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bejrút	Bejrút	k1gInSc1	Bejrút
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Galerie	galerie	k1gFnSc2	galerie
Bejrút	Bejrút	k1gInSc1	Bejrút
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
