<s>
Vlas	vlas	k1gInSc1	vlas
a	a	k8xC	a
chlup	chlup	k1gInSc1	chlup
jsou	být	k5eAaImIp3nP	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
anatomicky	anatomicky	k6eAd1	anatomicky
téměř	téměř	k6eAd1	téměř
totožné	totožný	k2eAgInPc1d1	totožný
útvary	útvar	k1gInPc1	útvar
tvořené	tvořený	k2eAgInPc1d1	tvořený
keratinem	keratin	k1gInSc7	keratin
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
výjimek	výjimka	k1gFnPc2	výjimka
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
srsti	srst	k1gFnSc2	srst
<g/>
)	)	kIx)	)
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
termoregulace	termoregulace	k1gFnSc1	termoregulace
(	(	kIx(	(
<g/>
ať	ať	k8xS	ať
už	už	k6eAd1	už
v	v	k7c6	v
teplém	teplý	k2eAgNnSc6d1	teplé
nebo	nebo	k8xC	nebo
chladném	chladný	k2eAgNnSc6d1	chladné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
vlas	vlas	k1gInSc1	vlas
<g/>
/	/	kIx~	/
<g/>
chlup	chlup	k1gInSc1	chlup
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
vlasový	vlasový	k2eAgInSc4d1	vlasový
stvol	stvol	k1gInSc4	stvol
(	(	kIx(	(
<g/>
scapus	scapus	k1gInSc4	scapus
pili	pít	k5eAaImAgMnP	pít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vyčnívá	vyčnívat	k5eAaImIp3nS	vyčnívat
nad	nad	k7c4	nad
pokožku	pokožka	k1gFnSc4	pokožka
<g/>
,	,	kIx,	,
a	a	k8xC	a
kořen	kořen	k1gInSc1	kořen
vlasu	vlas	k1gInSc2	vlas
(	(	kIx(	(
<g/>
radix	radix	k1gInSc4	radix
pili	pít	k5eAaImAgMnP	pít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
její	její	k3xOp3gFnSc7	její
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Nejhlubší	hluboký	k2eAgFnSc7d3	nejhlubší
částí	část	k1gFnSc7	část
kořene	kořen	k1gInSc2	kořen
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
vlasová	vlasový	k2eAgFnSc1d1	vlasová
cibulka	cibulka	k1gFnSc1	cibulka
(	(	kIx(	(
<g/>
bulbus	bulbus	k1gInSc1	bulbus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
následuje	následovat	k5eAaImIp3nS	následovat
zbytek	zbytek	k1gInSc4	zbytek
kořene	kořen	k1gInSc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Cibulka	cibulka	k1gFnSc1	cibulka
je	být	k5eAaImIp3nS	být
obalena	obalit	k5eAaPmNgFnS	obalit
vlasovými	vlasový	k2eAgFnPc7d1	vlasová
pochvami	pochva	k1gFnPc7	pochva
(	(	kIx(	(
<g/>
dvěma	dva	k4xCgInPc7	dva
epitely	epitel	k1gInPc7	epitel
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
vazivovou	vazivový	k2eAgFnSc4d1	vazivová
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dohromady	dohromady	k6eAd1	dohromady
se	se	k3xPyFc4	se
kořenu	kořen	k2eAgFnSc4d1	Kořena
a	a	k8xC	a
kolem	kolem	k6eAd1	kolem
obalené	obalený	k2eAgFnSc6d1	obalená
pochvě	pochva	k1gFnSc6	pochva
říká	říkat	k5eAaImIp3nS	říkat
vlasový	vlasový	k2eAgInSc4d1	vlasový
folikul	folikul	k1gInSc4	folikul
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
mazové	mazový	k2eAgFnPc1d1	mazová
a	a	k8xC	a
potní	potní	k2eAgFnPc1d1	potní
žlázy	žláza	k1gFnPc1	žláza
a	a	k8xC	a
cévní	cévní	k2eAgNnSc1d1	cévní
zásobení	zásobení	k1gNnSc1	zásobení
<g/>
.	.	kIx.	.
</s>
<s>
Vlasový	vlasový	k2eAgInSc1d1	vlasový
stvol	stvol	k1gInSc1	stvol
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
:	:	kIx,	:
kutikuly	kutikula	k1gFnSc2	kutikula
<g/>
,	,	kIx,	,
kortexu	kortex	k1gInSc2	kortex
a	a	k8xC	a
meduly	medula	k1gFnSc2	medula
<g/>
.	.	kIx.	.
</s>
<s>
Kutikula	kutikula	k1gFnSc1	kutikula
(	(	kIx(	(
<g/>
šupinatá	šupinatý	k2eAgFnSc1d1	šupinatá
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
vnějším	vnější	k2eAgNnSc7d1	vnější
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
správné	správný	k2eAgFnSc6d1	správná
funkci	funkce	k1gFnSc6	funkce
mazové	mazový	k2eAgFnSc2d1	mazová
žlázy	žláza	k1gFnSc2	žláza
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
konzervována	konzervovat	k5eAaBmNgFnS	konzervovat
vlasovým	vlasový	k2eAgInSc7d1	vlasový
mazem	maz	k1gInSc7	maz
(	(	kIx(	(
<g/>
tukem	tuk	k1gInSc7	tuk
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlas	vlas	k1gInSc1	vlas
méně	málo	k6eAd2	málo
propouští	propouštět	k5eAaImIp3nS	propouštět
škodliviny	škodlivina	k1gFnPc4	škodlivina
a	a	k8xC	a
vodné	vodný	k2eAgInPc4d1	vodný
roztoky	roztok	k1gInPc4	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
souběžně	souběžně	k6eAd1	souběžně
a	a	k8xC	a
stříškovitě	stříškovitě	k6eAd1	stříškovitě
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
odumřelých	odumřelý	k2eAgFnPc2d1	odumřelá
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
šupin	šupina	k1gFnPc2	šupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
k	k	k7c3	k
sobě	se	k3xPyFc3	se
přiléhají	přiléhat	k5eAaImIp3nP	přiléhat
<g/>
.	.	kIx.	.
</s>
<s>
Šupiny	šupina	k1gFnPc1	šupina
se	se	k3xPyFc4	se
překrývají	překrývat	k5eAaImIp3nP	překrývat
asi	asi	k9	asi
do	do	k7c2	do
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
vlasu	vlas	k1gInSc2	vlas
tvoří	tvořit	k5eAaImIp3nP	tvořit
úhel	úhel	k1gInSc4	úhel
asi	asi	k9	asi
18	[number]	k4	18
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vrstvy	vrstva	k1gFnPc1	vrstva
jsou	být	k5eAaImIp3nP	být
pokryty	pokrýt	k5eAaPmNgFnP	pokrýt
jemnou	jemný	k2eAgFnSc7d1	jemná
membránou	membrána	k1gFnSc7	membrána
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
proteinovým	proteinový	k2eAgInSc7d1	proteinový
a	a	k8xC	a
lipidovým	lipidový	k2eAgInSc7d1	lipidový
tmelem	tmel	k1gInSc7	tmel
<g/>
.	.	kIx.	.
</s>
<s>
Kortex	Kortex	k1gInSc1	Kortex
(	(	kIx(	(
<g/>
kůra	kůra	k1gFnSc1	kůra
<g/>
/	/	kIx~	/
<g/>
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
)	)	kIx)	)
zabírá	zabírat	k5eAaImIp3nS	zabírat
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
%	%	kIx~	%
vlastní	vlastní	k2eAgFnSc2d1	vlastní
vlasové	vlasový	k2eAgFnSc2d1	vlasová
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
protáhlých	protáhlý	k2eAgFnPc2d1	protáhlá
neživých	živý	k2eNgFnPc2d1	neživá
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
vláken	vlákna	k1gFnPc2	vlákna
keratinu	keratin	k1gInSc2	keratin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
vlákna	vlákno	k1gNnPc1	vlákno
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
stavební	stavební	k2eAgFnSc4d1	stavební
jednotku	jednotka	k1gFnSc4	jednotka
mikrofibrilu	mikrofibril	k1gInSc2	mikrofibril
-	-	kIx~	-
ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
kruhovitě	kruhovitě	k6eAd1	kruhovitě
seskupují	seskupovat	k5eAaImIp3nP	seskupovat
<g/>
,	,	kIx,	,
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
snopečky	snopeček	k1gInPc7	snopeček
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
tvoří	tvořit	k5eAaImIp3nS	tvořit
další	další	k2eAgFnSc4d1	další
stavební	stavební	k2eAgFnSc4d1	stavební
jednotku	jednotka	k1gFnSc4	jednotka
makrofibrilu	makrofibril	k1gInSc2	makrofibril
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ortokortex	ortokortex	k1gInSc1	ortokortex
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
uspořádání	uspořádání	k1gNnSc4	uspořádání
vlasového	vlasový	k2eAgInSc2d1	vlasový
kortexu	kortex	k1gInSc2	kortex
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nepravidelném	pravidelný	k2eNgNnSc6d1	nepravidelné
uspořádání	uspořádání	k1gNnSc6	uspořádání
kortexu	kortex	k1gInSc2	kortex
(	(	kIx(	(
<g/>
především	především	k9	především
vlasy	vlas	k1gInPc4	vlas
upravované	upravovaný	k2eAgInPc4d1	upravovaný
preparací	preparace	k1gFnSc7	preparace
<g/>
,	,	kIx,	,
barvením	barvení	k1gNnSc7	barvení
a	a	k8xC	a
odbarvováním	odbarvování	k1gNnSc7	odbarvování
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
parakortex	parakortex	k1gInSc4	parakortex
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
nejen	nejen	k6eAd1	nejen
fyzikálním	fyzikální	k2eAgInSc6d1	fyzikální
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
rozdílným	rozdílný	k2eAgNnSc7d1	rozdílné
složením	složení	k1gNnSc7	složení
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
různou	různý	k2eAgFnSc7d1	různá
chemickou	chemický	k2eAgFnSc7d1	chemická
reaktivitou	reaktivita	k1gFnSc7	reaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vláknité	vláknitý	k2eAgFnPc1d1	vláknitá
makrofibrily	makrofibrila	k1gFnPc1	makrofibrila
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
spojeny	spojen	k2eAgInPc1d1	spojen
buněčnými	buněčný	k2eAgFnPc7d1	buněčná
membránami	membrána	k1gFnPc7	membrána
a	a	k8xC	a
proteinovým	proteinový	k2eAgInSc7d1	proteinový
tmelem	tmel	k1gInSc7	tmel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kortexu	kortex	k1gInSc6	kortex
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
shluky	shluk	k1gInPc1	shluk
pigmentových	pigmentový	k2eAgNnPc2d1	pigmentové
zrn	zrno	k1gNnPc2	zrno
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nP	jevit
jako	jako	k9	jako
tmavé	tmavý	k2eAgFnPc1d1	tmavá
skvrny	skvrna	k1gFnPc1	skvrna
(	(	kIx(	(
<g/>
ostrůvky	ostrůvek	k1gInPc1	ostrůvek
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
fibrilami	fibrila	k1gFnPc7	fibrila
<g/>
.	.	kIx.	.
</s>
<s>
Medulla	Medulla	k1gFnSc1	Medulla
(	(	kIx(	(
<g/>
dřeň	dřeň	k1gFnSc1	dřeň
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
centrální	centrální	k2eAgFnSc4d1	centrální
část	část	k1gFnSc4	část
vlasu	vlas	k1gInSc2	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
vlasů	vlas	k1gInPc2	vlas
je	být	k5eAaImIp3nS	být
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
u	u	k7c2	u
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
u	u	k7c2	u
kořínků	kořínek	k1gInPc2	kořínek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
u	u	k7c2	u
vousů	vous	k1gInPc2	vous
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
hvězdicovitý	hvězdicovitý	k2eAgInSc1d1	hvězdicovitý
tvar	tvar	k1gInSc1	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Keratin	keratin	k1gInSc1	keratin
meduly	medula	k1gFnSc2	medula
má	mít	k5eAaImIp3nS	mít
houbovitou	houbovitý	k2eAgFnSc4d1	houbovitá
strukturu	struktura	k1gFnSc4	struktura
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dutinkami	dutinka	k1gFnPc7	dutinka
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
3,5	[number]	k4	3,5
%	%	kIx~	%
lipidů	lipid	k1gInPc2	lipid
<g/>
.	.	kIx.	.
</s>
<s>
Keratin	keratin	k1gInSc1	keratin
meduly	medula	k1gFnSc2	medula
má	mít	k5eAaImIp3nS	mít
jiné	jiný	k2eAgNnSc1d1	jiné
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
než	než	k8xS	než
kortex	kortex	k1gInSc1	kortex
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
chemického	chemický	k2eAgNnSc2d1	chemické
hlediska	hledisko	k1gNnSc2	hledisko
vlasy	vlas	k1gInPc1	vlas
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
uhlík	uhlík	k1gInSc4	uhlík
<g/>
,	,	kIx,	,
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
jód	jód	k1gInSc1	jód
<g/>
,	,	kIx,	,
20	[number]	k4	20
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
proteiny	protein	k1gInPc1	protein
<g/>
,	,	kIx,	,
lipidy	lipid	k1gInPc1	lipid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
cholesterin	cholesterin	k1gInSc1	cholesterin
<g/>
)	)	kIx)	)
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
asi	asi	k9	asi
12	[number]	k4	12
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vlasu	vlas	k1gInSc2	vlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlas	vlas	k1gInSc1	vlas
po	po	k7c6	po
chemické	chemický	k2eAgFnSc6d1	chemická
stránce	stránka	k1gFnSc6	stránka
<g/>
:	:	kIx,	:
Původně	původně	k6eAd1	původně
měkká	měkký	k2eAgFnSc1d1	měkká
buničitá	buničitý	k2eAgFnSc1d1	buničitá
bílkovinná	bílkovinný	k2eAgFnSc1d1	bílkovinná
hmota	hmota	k1gFnSc1	hmota
prekeratin	prekeratina	k1gFnPc2	prekeratina
se	se	k3xPyFc4	se
keratinizací	keratinizace	k1gFnSc7	keratinizace
neboli	neboli	k8xC	neboli
rohovatěním	rohovatění	k1gNnSc7	rohovatění
mění	měnit	k5eAaImIp3nP	měnit
pevnou	pevný	k2eAgFnSc4d1	pevná
hmotu	hmota	k1gFnSc4	hmota
keratin	keratin	k1gInSc1	keratin
<g/>
.	.	kIx.	.
</s>
<s>
Vlas	vlas	k1gInSc1	vlas
má	mít	k5eAaImIp3nS	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
pevnost	pevnost	k1gFnSc4	pevnost
jako	jako	k8xS	jako
kevlar	kevlar	k1gInSc4	kevlar
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jediné	jediný	k2eAgNnSc1d1	jediné
vlasové	vlasový	k2eAgNnSc1d1	vlasové
vlákno	vlákno	k1gNnSc1	vlákno
dokáže	dokázat	k5eAaPmIp3nS	dokázat
udržet	udržet	k5eAaPmF	udržet
100	[number]	k4	100
gramové	gramový	k2eAgFnPc4d1	gramová
závaží	závaží	k1gNnPc4	závaží
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
vlasu	vlas	k1gInSc2	vlas
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
18	[number]	k4	18
μ	μ	k?	μ
<g/>
-	-	kIx~	-
<g/>
180	[number]	k4	180
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
nejvíce	hodně	k6eAd3	hodně
vlasů	vlas	k1gInPc2	vlas
světlovlasí	světlovlasý	k2eAgMnPc1d1	světlovlasý
lidé	člověk	k1gMnPc1	člověk
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
140	[number]	k4	140
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
zrzaví	zrzavět	k5eAaImIp3nS	zrzavět
(	(	kIx(	(
<g/>
asi	asi	k9	asi
80	[number]	k4	80
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
vlasový	vlasový	k2eAgInSc1d1	vlasový
kořínek	kořínek	k1gInSc1	kořínek
má	mít	k5eAaImIp3nS	mít
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
existence	existence	k1gFnSc2	existence
(	(	kIx(	(
<g/>
postupně	postupně	k6eAd1	postupně
<g/>
)	)	kIx)	)
až	až	k9	až
12	[number]	k4	12
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
tloušťka	tloušťka	k1gFnSc1	tloušťka
vlasu	vlas	k1gInSc2	vlas
je	být	k5eAaImIp3nS	být
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
95	[number]	k4	95
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
mají	mít	k5eAaImIp3nP	mít
vlasy	vlas	k1gInPc4	vlas
nejsilnější	silný	k2eAgInPc4d3	nejsilnější
v	v	k7c6	v
týlu	týl	k1gInSc6	týl
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ženské	ženský	k2eAgInPc4d1	ženský
vlasy	vlas	k1gInPc4	vlas
jsou	být	k5eAaImIp3nP	být
silnější	silný	k2eAgMnSc1d2	silnější
než	než	k8xS	než
mužské	mužský	k2eAgNnSc1d1	mužské
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
pohlaví	pohlaví	k1gNnSc2	pohlaví
má	mít	k5eAaImIp3nS	mít
pod	pod	k7c7	pod
pokožkou	pokožka	k1gFnSc7	pokožka
2	[number]	k4	2
až	až	k9	až
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
vlasových	vlasový	k2eAgInPc2d1	vlasový
váčků	váček	k1gInPc2	váček
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
a	a	k8xC	a
druh	druh	k1gInSc1	druh
váčků	váček	k1gInPc2	váček
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
už	už	k6eAd1	už
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
Vlas	vlas	k1gInSc1	vlas
roste	růst	k5eAaImIp3nS	růst
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
let	léto	k1gNnPc2	léto
a	a	k8xC	a
potom	potom	k6eAd1	potom
vypadne	vypadnout	k5eAaPmIp3nS	vypadnout
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
vypadne	vypadnout	k5eAaPmIp3nS	vypadnout
člověku	člověk	k1gMnSc3	člověk
asi	asi	k9	asi
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
vlasů	vlas	k1gInPc2	vlas
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
vlasy	vlas	k1gInPc7	vlas
žádné	žádný	k3yNgInPc4	žádný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
vlasy	vlas	k1gInPc1	vlas
povyrostou	povyrůst	k5eAaPmIp3nP	povyrůst
o	o	k7c4	o
0,2	[number]	k4	0,2
<g/>
-	-	kIx~	-
<g/>
0,45	[number]	k4	0,45
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Padání	padání	k1gNnSc1	padání
vlasů	vlas	k1gInPc2	vlas
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
vratné	vratný	k2eAgNnSc4d1	vratné
(	(	kIx(	(
<g/>
dočasné	dočasný	k2eAgNnSc4d1	dočasné
<g/>
)	)	kIx)	)
a	a	k8xC	a
nevratné	vratný	k2eNgFnPc1d1	nevratná
<g/>
.	.	kIx.	.
</s>
<s>
Příčinami	příčina	k1gFnPc7	příčina
vratného	vratný	k2eAgNnSc2d1	vratné
vypadávání	vypadávání	k1gNnSc2	vypadávání
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
stres	stres	k1gInSc1	stres
<g/>
,	,	kIx,	,
těhotenství	těhotenství	k1gNnSc1	těhotenství
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Častou	častý	k2eAgFnSc7d1	častá
příčinou	příčina	k1gFnSc7	příčina
padání	padání	k1gNnSc2	padání
vlasů	vlas	k1gInPc2	vlas
je	být	k5eAaImIp3nS	být
užívání	užívání	k1gNnSc1	užívání
některých	některý	k3yIgInPc2	některý
léků	lék	k1gInPc2	lék
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
léky	lék	k1gInPc4	lék
na	na	k7c6	na
akné	akné	k1gFnSc6	akné
<g/>
,	,	kIx,	,
hormonální	hormonální	k2eAgFnSc1d1	hormonální
terapie	terapie	k1gFnSc1	terapie
<g/>
,	,	kIx,	,
epileptika	epileptik	k1gMnSc2	epileptik
<g/>
,	,	kIx,	,
léky	lék	k1gInPc1	lék
potlačující	potlačující	k2eAgInSc1d1	potlačující
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
léky	lék	k1gInPc4	lék
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
cholesterolu	cholesterol	k1gInSc2	cholesterol
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevratné	vratný	k2eNgNnSc1d1	nevratné
vypadávání	vypadávání	k1gNnSc1	vypadávání
vlasů	vlas	k1gInPc2	vlas
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
činnosti	činnost	k1gFnSc2	činnost
mužských	mužský	k2eAgInPc2d1	mužský
hormonů	hormon	k1gInPc2	hormon
testosteronu	testosteron	k1gInSc2	testosteron
a	a	k8xC	a
dihydrotestosteronu	dihydrotestosteron	k1gInSc2	dihydrotestosteron
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
i	i	k9	i
důsledkem	důsledek	k1gInSc7	důsledek
léčby	léčba	k1gFnSc2	léčba
nádorového	nádorový	k2eAgNnSc2d1	nádorové
onemocnění	onemocnění	k1gNnSc2	onemocnění
(	(	kIx(	(
<g/>
radioterapie	radioterapie	k1gFnSc1	radioterapie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotných	samotný	k2eAgFnPc2d1	samotná
příčin	příčina	k1gFnPc2	příčina
padání	padání	k1gNnSc2	padání
vlasů	vlas	k1gInPc2	vlas
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
a	a	k8xC	a
při	při	k7c6	při
diagnostice	diagnostika	k1gFnSc6	diagnostika
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
brát	brát	k5eAaImF	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
anamnézu	anamnéza	k1gFnSc4	anamnéza
<g/>
,	,	kIx,	,
aktuální	aktuální	k2eAgInSc4d1	aktuální
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
překonané	překonaný	k2eAgNnSc4d1	překonané
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
stresovou	stresový	k2eAgFnSc4d1	stresová
zátěž	zátěž	k1gFnSc4	zátěž
<g/>
,	,	kIx,	,
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
omezení	omezení	k1gNnSc4	omezení
vypadávání	vypadávání	k1gNnSc2	vypadávání
vlasů	vlas	k1gInPc2	vlas
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
řada	řada	k1gFnSc1	řada
faktorů	faktor	k1gInPc2	faktor
-	-	kIx~	-
zejména	zejména	k9	zejména
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
prokrvení	prokrvení	k1gNnSc1	prokrvení
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vypadávání	vypadávání	k1gNnSc4	vypadávání
vlasů	vlas	k1gInPc2	vlas
zmírnit	zmírnit	k5eAaPmF	zmírnit
masáží	masáž	k1gFnSc7	masáž
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
udělat	udělat	k5eAaPmF	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
padání	padání	k1gNnSc2	padání
vlasů	vlas	k1gInPc2	vlas
je	být	k5eAaImIp3nS	být
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
a	a	k8xC	a
často	často	k6eAd1	často
s	s	k7c7	s
nejistými	jistý	k2eNgInPc7d1	nejistý
výsledky	výsledek	k1gInPc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
látek	látka	k1gFnPc2	látka
testovaných	testovaný	k2eAgFnPc2d1	testovaná
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
klinických	klinický	k2eAgFnPc2d1	klinická
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
příznivý	příznivý	k2eAgInSc4d1	příznivý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
hustotu	hustota	k1gFnSc4	hustota
a	a	k8xC	a
růst	růst	k1gInSc4	růst
vlasů	vlas	k1gInPc2	vlas
<g/>
:	:	kIx,	:
minoxidil	minoxidit	k5eAaImAgInS	minoxidit
5	[number]	k4	5
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
fluridil	fluridit	k5eAaPmAgMnS	fluridit
<g/>
,	,	kIx,	,
aminexil	aminexil	k1gInSc1	aminexil
<g/>
,	,	kIx,	,
melatonin	melatonin	k1gInSc1	melatonin
(	(	kIx(	(
<g/>
spánkový	spánkový	k2eAgInSc1d1	spánkový
hormon	hormon	k1gInSc1	hormon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
kyselina	kyselina	k1gFnSc1	kyselina
hyaluronová	hyaluronová	k1gFnSc1	hyaluronová
(	(	kIx(	(
<g/>
klinické	klinický	k2eAgFnPc1d1	klinická
studie	studie	k1gFnPc1	studie
jsou	být	k5eAaImIp3nP	být
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
v	v	k7c6	v
odkazu	odkaz	k1gInSc6	odkaz
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
užití	užití	k1gNnSc4	užití
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
kombinace	kombinace	k1gFnSc1	kombinace
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
respektuje	respektovat	k5eAaImIp3nS	respektovat
potřeby	potřeba	k1gFnPc4	potřeba
každého	každý	k3xTgMnSc2	každý
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
častější	častý	k2eAgInSc1d2	častější
je	být	k5eAaImIp3nS	být
také	také	k9	také
ložiskové	ložiskový	k2eAgNnSc1d1	ložiskové
vypadávání	vypadávání	k1gNnSc1	vypadávání
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
Alopecia	Alopecia	k1gFnSc1	Alopecia
Areata	Areata	k1gFnSc1	Areata
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
autoimunitní	autoimunitní	k2eAgNnSc4d1	autoimunitní
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
začne	začít	k5eAaPmIp3nS	začít
napadat	napadat	k5eAaImF	napadat
vlasové	vlasový	k2eAgInPc4d1	vlasový
folikuly	folikul	k1gInPc4	folikul
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
malé	malý	k2eAgFnPc4d1	malá
lysinky	lysinka	k1gFnPc4	lysinka
velikosti	velikost	k1gFnSc2	velikost
mince	mince	k1gFnSc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Spouštěčem	spouštěč	k1gInSc7	spouštěč
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
stres	stres	k1gInSc1	stres
nebo	nebo	k8xC	nebo
traumatický	traumatický	k2eAgInSc1d1	traumatický
zážitek	zážitek	k1gInSc1	zážitek
(	(	kIx(	(
<g/>
postihuje	postihovat	k5eAaImIp3nS	postihovat
i	i	k9	i
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
například	například	k6eAd1	například
při	při	k7c6	při
šikaně	šikana	k1gFnSc6	šikana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
šetrné	šetrný	k2eAgInPc4d1	šetrný
kosmetické	kosmetický	k2eAgInPc4d1	kosmetický
přípravky	přípravek	k1gInPc4	přípravek
<g/>
,	,	kIx,	,
hydratovat	hydratovat	k5eAaBmF	hydratovat
pokožku	pokožka	k1gFnSc4	pokožka
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
dodržovat	dodržovat	k5eAaImF	dodržovat
duševní	duševní	k2eAgFnSc4d1	duševní
hygienu	hygiena	k1gFnSc4	hygiena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
klíčem	klíč	k1gInSc7	klíč
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
často	často	k6eAd1	často
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
recidiva	recidiva	k1gFnSc1	recidiva
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
návrat	návrat	k1gInSc1	návrat
onemocnění	onemocnění	k1gNnSc2	onemocnění
po	po	k7c4	po
"	"	kIx"	"
<g/>
vyléčení	vyléčení	k1gNnSc4	vyléčení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
individuální	individuální	k2eAgNnSc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
například	například	k6eAd1	například
stres	stres	k1gInSc1	stres
a	a	k8xC	a
organizmus	organizmus	k1gInSc1	organizmus
se	se	k3xPyFc4	se
zaměří	zaměřit	k5eAaPmIp3nS	zaměřit
na	na	k7c4	na
vlastní	vlastní	k2eAgFnPc4d1	vlastní
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
podívat	podívat	k5eAaPmF	podívat
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
jádro	jádro	k1gNnSc4	jádro
problému	problém	k1gInSc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Kosmetické	kosmetický	k2eAgInPc1d1	kosmetický
prostředky	prostředek	k1gInPc1	prostředek
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
problém	problém	k1gInSc1	problém
neřeší	řešit	k5eNaImIp3nS	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
porucha	porucha	k1gFnSc1	porucha
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
příčinu	příčina	k1gFnSc4	příčina
a	a	k8xC	a
tu	tu	k6eAd1	tu
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
najít	najít	k5eAaPmF	najít
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zkušeností	zkušenost	k1gFnPc2	zkušenost
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Alopecia	Alopecia	k1gFnSc1	Alopecia
areata	areata	k1gFnSc1	areata
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
jiným	jiný	k2eAgNnSc7d1	jiné
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
silnou	silný	k2eAgFnSc7d1	silná
alergií	alergie	k1gFnSc7	alergie
<g/>
,	,	kIx,	,
psychickým	psychický	k2eAgNnSc7d1	psychické
onemocněním	onemocnění	k1gNnSc7	onemocnění
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
deprese	deprese	k1gFnPc4	deprese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
onemocněním	onemocnění	k1gNnSc7	onemocnění
zažívacího	zažívací	k2eAgInSc2d1	zažívací
traktu	trakt	k1gInSc2	trakt
atd.	atd.	kA	atd.
Různé	různý	k2eAgNnSc1d1	různé
zbarvení	zbarvení	k1gNnSc1	zbarvení
vlasů	vlas	k1gInPc2	vlas
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
hlavně	hlavně	k6eAd1	hlavně
geneticky	geneticky	k6eAd1	geneticky
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
skladba	skladba	k1gFnSc1	skladba
melaninových	melaninový	k2eAgNnPc2d1	melaninový
zrn	zrno	k1gNnPc2	zrno
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vlasy	vlas	k1gInPc1	vlas
mají	mít	k5eAaImIp3nP	mít
barvu	barva	k1gFnSc4	barva
tmavou	tmavý	k2eAgFnSc4d1	tmavá
nebo	nebo	k8xC	nebo
světlou	světlý	k2eAgFnSc4d1	světlá
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
pigmentových	pigmentový	k2eAgNnPc2d1	pigmentové
zrn	zrno	k1gNnPc2	zrno
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
pouze	pouze	k6eAd1	pouze
intenzitu	intenzita	k1gFnSc4	intenzita
zbarvení	zbarvení	k1gNnPc2	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
samotný	samotný	k2eAgMnSc1d1	samotný
většinou	většinou	k6eAd1	většinou
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
společenskou	společenský	k2eAgFnSc4d1	společenská
funkci	funkce	k1gFnSc4	funkce
vlasů	vlas	k1gInPc2	vlas
-	-	kIx~	-
upravuje	upravovat	k5eAaImIp3nS	upravovat
si	se	k3xPyFc3	se
účes	účes	k1gInSc4	účes
<g/>
,	,	kIx,	,
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
délku	délka	k1gFnSc4	délka
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
85	[number]	k4	85
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
jsou	být	k5eAaImIp3nP	být
vlasy	vlas	k1gInPc1	vlas
téměř	téměř	k6eAd1	téměř
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
naprosto	naprosto	k6eAd1	naprosto
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
-	-	kIx~	-
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
melaninu	melanin	k1gInSc2	melanin
-	-	kIx~	-
látky	látka	k1gFnPc1	látka
tvořené	tvořený	k2eAgInPc1d1	tvořený
pigmentovými	pigmentový	k2eAgFnPc7d1	pigmentová
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tyto	tento	k3xDgFnPc1	tento
buňky	buňka	k1gFnPc1	buňka
přestanou	přestat	k5eAaPmIp3nP	přestat
fungovat	fungovat	k5eAaImF	fungovat
<g/>
,	,	kIx,	,
vlas	vlas	k1gInSc1	vlas
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Účes	účes	k1gInSc1	účes
vždy	vždy	k6eAd1	vždy
hrál	hrát	k5eAaImAgInS	hrát
vyznanou	vyznaný	k2eAgFnSc4d1	vyznaná
kulturní	kulturní	k2eAgFnSc4d1	kulturní
a	a	k8xC	a
společenskou	společenský	k2eAgFnSc4d1	společenská
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
nosili	nosit	k5eAaImAgMnP	nosit
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
vlasy	vlas	k1gInPc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgInPc1d2	delší
vlasy	vlas	k1gInPc1	vlas
byly	být	k5eAaImAgInP	být
zejména	zejména	k9	zejména
ve	v	k7c6	v
středověku	středověk	k1gInSc2	středověk
jasným	jasný	k2eAgInSc7d1	jasný
symbolem	symbol	k1gInSc7	symbol
vyššího	vysoký	k2eAgNnSc2d2	vyšší
společenského	společenský	k2eAgNnSc2d1	společenské
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kratší	krátký	k2eAgInPc1d2	kratší
vlasy	vlas	k1gInPc1	vlas
byly	být	k5eAaImAgFnP	být
především	především	k6eAd1	především
symbolem	symbol	k1gInSc7	symbol
podřízenosti	podřízenost	k1gFnSc2	podřízenost
a	a	k8xC	a
zejména	zejména	k9	zejména
poddanství	poddanství	k1gNnSc1	poddanství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
začínají	začínat	k5eAaImIp3nP	začínat
muži	muž	k1gMnPc1	muž
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
kulturách	kultura	k1gFnPc6	kultura
nosit	nosit	k5eAaImF	nosit
vlasy	vlas	k1gInPc1	vlas
kratší	krátký	k2eAgInPc1d2	kratší
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ženy	žena	k1gFnPc1	žena
je	on	k3xPp3gMnPc4	on
mají	mít	k5eAaImIp3nP	mít
typicky	typicky	k6eAd1	typicky
delší	dlouhý	k2eAgFnSc4d2	delší
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
určitá	určitý	k2eAgFnSc1d1	určitá
část	část	k1gFnSc1	část
žen	žena	k1gFnPc2	žena
nosit	nosit	k5eAaImF	nosit
též	též	k9	též
i	i	k9	i
vlasy	vlas	k1gInPc1	vlas
kratší	krátký	k2eAgInPc1d2	kratší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
přímý	přímý	k2eAgInSc1d1	přímý
důsledek	důsledek	k1gInSc1	důsledek
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
muži	muž	k1gMnPc1	muž
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
vypořádat	vypořádat	k5eAaPmF	vypořádat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
nemocí	nemoc	k1gFnPc2	nemoc
a	a	k8xC	a
parazitů	parazit	k1gMnPc2	parazit
a	a	k8xC	a
krátký	krátký	k2eAgInSc4d1	krátký
sestřih	sestřih	k1gInSc4	sestřih
byl	být	k5eAaImAgInS	být
výhodnější	výhodný	k2eAgInSc1d2	výhodnější
z	z	k7c2	z
hygienického	hygienický	k2eAgNnSc2d1	hygienické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Nošení	nošení	k1gNnSc1	nošení
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
vlasů	vlas	k1gInPc2	vlas
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
vracet	vracet	k5eAaImF	vracet
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
symbolem	symbol	k1gInSc7	symbol
hippies	hippiesa	k1gFnPc2	hippiesa
a	a	k8xC	a
politického	politický	k2eAgInSc2d1	politický
protestu	protest	k1gInSc2	protest
proti	proti	k7c3	proti
establishmentu	establishment	k1gInSc3	establishment
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
delší	dlouhý	k2eAgInPc1d2	delší
vlasy	vlas	k1gInPc1	vlas
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
symbolem	symbol	k1gInSc7	symbol
určité	určitý	k2eAgFnPc4d1	určitá
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
nonkonformitu	nonkonformita	k1gFnSc4	nonkonformita
<g/>
.	.	kIx.	.
</s>
