<s>
Až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
výjimek	výjimka	k1gFnPc2	výjimka
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
srsti	srst	k1gFnSc2	srst
<g/>
)	)	kIx)	)
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
termoregulace	termoregulace	k1gFnSc1	termoregulace
(	(	kIx(	(
<g/>
ať	ať	k8xS	ať
už	už	k6eAd1	už
v	v	k7c6	v
teplém	teplý	k2eAgNnSc6d1	teplé
nebo	nebo	k8xC	nebo
chladném	chladný	k2eAgNnSc6d1	chladné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
