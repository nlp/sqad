<p>
<s>
Ruthenium	ruthenium	k1gNnSc1	ruthenium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ru	Ru	k1gFnSc2	Ru
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Ruthenium	ruthenium	k1gNnSc1	ruthenium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drahý	drahý	k2eAgInSc1d1	drahý
kov	kov	k1gInSc1	kov
šedivě	šedivě	k6eAd1	šedivě
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
výskyt	výskyt	k1gInSc4	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Ruthenium	ruthenium	k1gNnSc1	ruthenium
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
v	v	k7c6	v
sibiřské	sibiřský	k2eAgFnSc6d1	sibiřská
platinové	platinový	k2eAgFnSc6d1	platinová
rudě	ruda	k1gFnSc6	ruda
chemikem	chemik	k1gMnSc7	chemik
Karlem	Karel	k1gMnSc7	Karel
Ernstem	Ernst	k1gMnSc7	Ernst
Clausem	Claus	k1gMnSc7	Claus
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
názvu	název	k1gInSc2	název
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ušlechtilý	ušlechtilý	k2eAgMnSc1d1	ušlechtilý
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
,	,	kIx,	,
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
elektricky	elektricky	k6eAd1	elektricky
i	i	k9	i
tepelně	tepelně	k6eAd1	tepelně
středně	středně	k6eAd1	středně
dobře	dobře	k6eAd1	dobře
vodivý	vodivý	k2eAgInSc1d1	vodivý
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
rhodiem	rhodium	k1gNnSc7	rhodium
a	a	k8xC	a
palladiem	palladion	k1gNnSc7	palladion
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
triády	triáda	k1gFnSc2	triáda
lehkých	lehký	k2eAgInPc2d1	lehký
platinových	platinový	k2eAgInPc2d1	platinový
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
ostatní	ostatní	k2eAgInPc4d1	ostatní
platinové	platinový	k2eAgInPc4d1	platinový
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
naleziště	naleziště	k1gNnSc4	naleziště
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Urale	Ural	k1gInSc6	Ural
a	a	k8xC	a
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruthenium	ruthenium	k1gNnSc1	ruthenium
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
(	(	kIx(	(
<g/>
za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
)	)	kIx)	)
vytvořit	vytvořit	k5eAaPmF	vytvořit
oxid	oxid	k1gInSc4	oxid
s	s	k7c7	s
prvkem	prvek	k1gInSc7	prvek
v	v	k7c6	v
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
oxidačním	oxidační	k2eAgNnSc6d1	oxidační
čísle	číslo	k1gNnSc6	číslo
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
ještě	ještě	k6eAd1	ještě
osmium	osmium	k1gNnSc1	osmium
a	a	k8xC	a
xenon	xenon	k1gInSc1	xenon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
ruthenia	ruthenium	k1gNnSc2	ruthenium
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
legováno	legován	k2eAgNnSc1d1	legován
do	do	k7c2	do
slitin	slitina	k1gFnPc2	slitina
s	s	k7c7	s
platinou	platina	k1gFnSc7	platina
a	a	k8xC	a
palladiem	palladion	k1gNnSc7	palladion
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
jejich	jejich	k3xOp3gFnSc2	jejich
tvrdosti	tvrdost	k1gFnSc2	tvrdost
a	a	k8xC	a
mechanické	mechanický	k2eAgFnSc2d1	mechanická
odolnosti	odolnost	k1gFnSc2	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Přídavek	přídavek	k1gInSc1	přídavek
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
ruthenia	ruthenium	k1gNnSc2	ruthenium
do	do	k7c2	do
titanových	titanový	k2eAgFnPc2d1	titanová
slitin	slitina	k1gFnPc2	slitina
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
podstatným	podstatný	k2eAgInSc7d1	podstatný
způsobem	způsob	k1gInSc7	způsob
jejich	jejich	k3xOp3gFnSc4	jejich
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Slitina	slitina	k1gFnSc1	slitina
platiny	platina	k1gFnSc2	platina
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
5	[number]	k4	5
<g/>
%	%	kIx~	%
ruthenia	ruthenium	k1gNnSc2	ruthenium
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
luxusních	luxusní	k2eAgFnPc2d1	luxusní
náramkových	náramkový	k2eAgFnPc2d1	náramková
hodinek	hodinka	k1gFnPc2	hodinka
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
odolností	odolnost	k1gFnSc7	odolnost
vůči	vůči	k7c3	vůči
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
nebo	nebo	k8xC	nebo
chemickému	chemický	k2eAgNnSc3d1	chemické
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Katalyzátory	katalyzátor	k1gInPc1	katalyzátor
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
oxidu	oxid	k1gInSc2	oxid
ruthenia	ruthenium	k1gNnSc2	ruthenium
jsou	být	k5eAaImIp3nP	být
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
v	v	k7c6	v
odstraňování	odstraňování	k1gNnSc6	odstraňování
sulfanu	sulfan	k1gInSc2	sulfan
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
ropných	ropný	k2eAgInPc2d1	ropný
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
farmaceutickým	farmaceutický	k2eAgInSc7d1	farmaceutický
průmyslem	průmysl	k1gInSc7	průmysl
intenzivně	intenzivně	k6eAd1	intenzivně
zkoumány	zkoumat	k5eAaImNgFnP	zkoumat
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
ruthenia	ruthenium	k1gNnSc2	ruthenium
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaPmF	stát
základem	základ	k1gInSc7	základ
účinných	účinný	k2eAgNnPc2d1	účinné
cytostatik	cytostatikum	k1gNnPc2	cytostatikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
dostal	dostat	k5eAaPmAgMnS	dostat
Robert	Robert	k1gMnSc1	Robert
Grubbs	Grubbsa	k1gFnPc2	Grubbsa
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
za	za	k7c4	za
objev	objev	k1gInSc4	objev
nové	nový	k2eAgFnSc2d1	nová
katalytické	katalytický	k2eAgFnSc2d1	katalytická
reakce	reakce	k1gFnSc2	reakce
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
rutheniu	ruthenium	k1gNnSc6	ruthenium
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
metateze	metateze	k1gFnSc1	metateze
olefinů	olefin	k1gInPc2	olefin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rutheniové	Rutheniový	k2eAgInPc1d1	Rutheniový
komplexy	komplex	k1gInPc1	komplex
jsou	být	k5eAaImIp3nP	být
absorpcí	absorpce	k1gFnSc7	absorpce
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
377	[number]	k4	377
až	až	k9	až
430	[number]	k4	430
nm	nm	k?	nm
<g/>
)	)	kIx)	)
schopny	schopen	k2eAgInPc1d1	schopen
katalyzovat	katalyzovat	k5eAaImF	katalyzovat
rozklad	rozklad	k1gInSc4	rozklad
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
směs	směs	k1gFnSc4	směs
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
objeveno	objevit	k5eAaPmNgNnS	objevit
Davidem	David	k1gMnSc7	David
G.	G.	kA	G.
Whittenem	Whitten	k1gMnSc7	Whitten
<g/>
,	,	kIx,	,
1973-80	[number]	k4	1973-80
Profesorem	profesor	k1gMnSc7	profesor
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
North	North	k1gInSc1	North
Carolina	Carolina	k1gFnSc1	Carolina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rutheniový	Rutheniový	k2eAgInSc1d1	Rutheniový
komplex	komplex	k1gInSc1	komplex
je	být	k5eAaImIp3nS	být
donorem	donor	k1gInSc7	donor
i	i	k8xC	i
akceptorem	akceptor	k1gInSc7	akceptor
elektronu	elektron	k1gInSc2	elektron
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
dvojstrannosti	dvojstrannost	k1gFnSc6	dvojstrannost
spočívá	spočívat	k5eAaImIp3nS	spočívat
jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
rozklad	rozklad	k1gInSc4	rozklad
vody	voda	k1gFnSc2	voda
světlem	světlo	k1gNnSc7	světlo
(	(	kIx(	(
<g/>
Doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
Josip	Josip	k1gInSc1	Josip
Kleczek	Kleczek	k1gInSc1	Kleczek
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
energie	energie	k1gFnSc1	energie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Ruthenium	ruthenium	k1gNnSc1	ruthenium
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
velice	velice	k6eAd1	velice
nereaktivní	reaktivní	k2eNgInSc1d1	nereaktivní
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
v	v	k7c6	v
Beketovově	Beketovův	k2eAgFnSc6d1	Beketovův
řadě	řada	k1gFnSc6	řada
kovů	kov	k1gInPc2	kov
nachází	nacházet	k5eAaImIp3nS	nacházet
vpravo	vpravo	k6eAd1	vpravo
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
ušlechtilý	ušlechtilý	k2eAgInSc4d1	ušlechtilý
kov	kov	k1gInSc4	kov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Nereaguje	reagovat	k5eNaBmIp3nS	reagovat
však	však	k9	však
ani	ani	k9	ani
s	s	k7c7	s
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
reagují	reagovat	k5eAaBmIp3nP	reagovat
i	i	k9	i
s	s	k7c7	s
ušlechtilými	ušlechtilý	k2eAgInPc7d1	ušlechtilý
kovy	kov	k1gInPc7	kov
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
<g/>
:	:	kIx,	:
kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
či	či	k8xC	či
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lučavce	lučavka	k1gFnSc6	lučavka
královské	královský	k2eAgFnPc1d1	královská
se	se	k3xPyFc4	se
nerozpouští	rozpouštět	k5eNaImIp3nP	rozpouštět
ani	ani	k8xC	ani
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
tak	tak	k6eAd1	tak
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
ani	ani	k8xC	ani
se	s	k7c7	s
zásaditými	zásaditý	k2eAgFnPc7d1	zásaditá
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
nebo	nebo	k8xC	nebo
amoniak	amoniak	k1gInSc1	amoniak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
tohoto	tento	k3xDgInSc2	tento
kovu	kov	k1gInSc2	kov
lze	lze	k6eAd1	lze
získávat	získávat	k5eAaImF	získávat
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
s	s	k7c7	s
kovovým	kovový	k2eAgNnSc7d1	kovové
rutheniem	ruthenium	k1gNnSc7	ruthenium
reaguje	reagovat	k5eAaBmIp3nS	reagovat
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc1	směs
chlornanu	chlornan	k1gInSc2	chlornan
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
řada	řada	k1gFnSc1	řada
autokatalyzovaných	autokatalyzovaný	k2eAgMnPc2d1	autokatalyzovaný
(	(	kIx(	(
<g/>
katalyzátorem	katalyzátor	k1gInSc7	katalyzátor
je	být	k5eAaImIp3nS	být
samotné	samotný	k2eAgNnSc1d1	samotné
ruthenium	ruthenium	k1gNnSc1	ruthenium
<g/>
)	)	kIx)	)
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc1	oxid
rutheničelý	rutheničelý	k2eAgInSc1d1	rutheničelý
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
rutheničelý	rutheničelý	k2eAgInSc1d1	rutheničelý
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
využívá	využívat	k5eAaImIp3nS	využívat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
chloridu	chlorid	k1gInSc2	chlorid
ruthenitého	ruthenitý	k2eAgInSc2d1	ruthenitý
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
organokovové	organokovový	k2eAgFnPc4d1	organokovová
syntézy	syntéza	k1gFnPc4	syntéza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
dražší	drahý	k2eAgFnSc1d2	dražší
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
reakce	reakce	k1gFnSc1	reakce
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
a	a	k8xC	a
peroxidem	peroxid	k1gInSc7	peroxid
sodným	sodný	k2eAgInSc7d1	sodný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
však	však	k9	však
ruthenium	ruthenium	k1gNnSc1	ruthenium
stává	stávat	k5eAaImIp3nS	stávat
reaktivní	reaktivní	k2eAgNnSc1d1	reaktivní
<g/>
,	,	kIx,	,
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
fluorem	fluor	k1gInSc7	fluor
<g/>
,	,	kIx,	,
chlórem	chlór	k1gInSc7	chlór
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
ruthenia	ruthenium	k1gNnSc2	ruthenium
===	===	k?	===
</s>
</p>
<p>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
ruthenitý	ruthenitý	k2eAgInSc1d1	ruthenitý
–	–	k?	–
RuCl	RuCl	k1gInSc1	RuCl
<g/>
3	[number]	k4	3
–	–	k?	–
hnědočerná	hnědočerný	k2eAgFnSc1d1	hnědočerná
<g/>
,	,	kIx,	,
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
organoruthenitých	organoruthenitý	k2eAgFnPc2d1	organoruthenitý
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
rutheničitý	rutheničitý	k2eAgInSc1d1	rutheničitý
–	–	k?	–
RuO	RuO	k1gFnSc7	RuO
<g/>
2	[number]	k4	2
–	–	k?	–
tmavě	tmavě	k6eAd1	tmavě
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
a	a	k8xC	a
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
oxidu	oxid	k1gInSc2	oxid
rutheničitého	rutheničitý	k2eAgInSc2d1	rutheničitý
se	se	k3xPyFc4	se
potahují	potahovat	k5eAaImIp3nP	potahovat
elektrody	elektroda	k1gFnPc1	elektroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
rutheničelý	rutheničelý	k2eAgInSc1d1	rutheničelý
–	–	k?	–
RuO	RuO	k1gFnSc7	RuO
<g/>
4	[number]	k4	4
–	–	k?	–
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
,	,	kIx,	,
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
velice	velice	k6eAd1	velice
snadno	snadno	k6eAd1	snadno
taje	tát	k5eAaImIp3nS	tát
(	(	kIx(	(
<g/>
tt	tt	k?	tt
<g/>
=	=	kIx~	=
<g/>
25,6	[number]	k4	25,6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fluorid	fluorid	k1gInSc1	fluorid
rutheniový	rutheniový	k2eAgInSc1d1	rutheniový
–	–	k?	–
RuF	RuF	k1gFnSc7	RuF
<g/>
6	[number]	k4	6
–	–	k?	–
tmavě	tmavě	k6eAd1	tmavě
hnědá	hnědý	k2eAgFnSc1d1	hnědá
<g/>
,	,	kIx,	,
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
organorutheniových	organorutheniový	k2eAgFnPc2d1	organorutheniový
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
rutheniová	rutheniový	k2eAgFnSc1d1	rutheniový
–	–	k?	–
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
RuO	RuO	k1gFnSc1	RuO
<g/>
4	[number]	k4	4
–	–	k?	–
látka	látka	k1gFnSc1	látka
bez	bez	k7c2	bez
praktického	praktický	k2eAgNnSc2d1	praktické
využití	využití	k1gNnSc2	využití
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
však	však	k9	však
soli	sůl	k1gFnSc2	sůl
–	–	k?	–
ruthenany	ruthenana	k1gFnSc2	ruthenana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
ruthenistá	ruthenistý	k2eAgFnSc1d1	ruthenistý
–	–	k?	–
HRuO	HRuO	k1gFnSc1	HRuO
<g/>
4	[number]	k4	4
–	–	k?	–
látka	látka	k1gFnSc1	látka
bez	bez	k7c2	bez
praktického	praktický	k2eAgNnSc2d1	praktické
využití	využití	k1gNnSc2	využití
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
však	však	k9	však
soli	sůl	k1gFnSc2	sůl
–	–	k?	–
ruthenistany	ruthenistan	k1gInPc4	ruthenistan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ruthenium	ruthenium	k1gNnSc4	ruthenium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ruthenium	ruthenium	k1gNnSc4	ruthenium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
