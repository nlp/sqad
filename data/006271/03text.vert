<s>
Joe	Joe	k?	Joe
Bonamassa	Bonamassa	k1gFnSc1	Bonamassa
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
blues-rockový	bluesockový	k2eAgMnSc1d1	blues-rockový
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
svým	svůj	k3xOyFgInSc7	svůj
hrubým	hrubý	k2eAgInSc7d1	hrubý
hlasem	hlas	k1gInSc7	hlas
a	a	k8xC	a
technicky	technicky	k6eAd1	technicky
velmi	velmi	k6eAd1	velmi
propracovanou	propracovaný	k2eAgFnSc7d1	propracovaná
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Kytaře	kytara	k1gFnSc3	kytara
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vydal	vydat	k5eAaPmAgInS	vydat
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
album	album	k1gNnSc4	album
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Bloodline	Bloodlin	k1gInSc5	Bloodlin
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gInPc7	jejíž
dalšími	další	k2eAgInPc7d1	další
členy	člen	k1gInPc7	člen
byli	být	k5eAaImAgMnP	být
synové	syn	k1gMnPc1	syn
Milese	Milese	k1gFnSc1	Milese
Davise	Davise	k1gFnSc1	Davise
Erin	Erin	k1gInSc4	Erin
<g/>
,	,	kIx,	,
Robbieho	Robbie	k1gMnSc4	Robbie
Kriegra	Kriegr	k1gInSc2	Kriegr
Waylon	Waylon	k1gInSc1	Waylon
a	a	k8xC	a
Berry	Berr	k1gInPc1	Berr
Oakley	Oaklea	k1gFnSc2	Oaklea
jr	jr	k?	jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
baskytaristy	baskytarista	k1gMnSc2	baskytarista
Berry	Berra	k1gMnSc2	Berra
Oakleyho	Oakley	k1gMnSc2	Oakley
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Allman	Allman	k1gMnSc1	Allman
Brothers	Brothersa	k1gFnPc2	Brothersa
Band	banda	k1gFnPc2	banda
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
kytarový	kytarový	k2eAgInSc1d1	kytarový
časopis	časopis	k1gInSc1	časopis
Guitar	Guitar	k1gMnSc1	Guitar
One	One	k1gMnSc1	One
Magazine	Magazin	k1gInSc5	Magazin
ho	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
kytaristu	kytarista	k1gMnSc4	kytarista
jeho	jeho	k3xOp3gFnSc2	jeho
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc7	jeho
inspiračními	inspirační	k2eAgInPc7d1	inspirační
zdroji	zdroj	k1gInPc7	zdroj
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dalších	další	k2eAgMnPc2d1	další
výrazných	výrazný	k2eAgMnPc2d1	výrazný
hráčů	hráč	k1gMnPc2	hráč
jeho	jeho	k3xOp3gFnSc2	jeho
generace	generace	k1gFnSc2	generace
(	(	kIx(	(
<g/>
Kenny	Kenna	k1gFnSc2	Kenna
Wayne	Wayn	k1gInSc5	Wayn
Shepherd	Shepherda	k1gFnPc2	Shepherda
<g/>
,	,	kIx,	,
Jonny	Jonna	k1gFnSc2	Jonna
Lang	Lang	k1gMnSc1	Lang
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kytaristé	kytarista	k1gMnPc1	kytarista
britské	britský	k2eAgFnSc2d1	britská
bluesové	bluesový	k2eAgFnSc2d1	bluesová
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Eric	Eric	k1gFnSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Green	Green	k1gInSc1	Green
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Kossoff	Kossoff	k1gMnSc1	Kossoff
<g/>
,	,	kIx,	,
Jeff	Jeff	k1gMnSc1	Jeff
Beck	Beck	k1gMnSc1	Beck
<g/>
,	,	kIx,	,
Jimmy	Jimma	k1gFnPc1	Jimma
Page	Pag	k1gFnPc1	Pag
<g/>
,	,	kIx,	,
Rory	Rory	k1gInPc1	Rory
Gallagher	Gallaghra	k1gFnPc2	Gallaghra
atd.	atd.	kA	atd.
Z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
tradičnějšího	tradiční	k2eAgNnSc2d2	tradičnější
blues	blues	k1gNnSc2	blues
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
svou	svůj	k3xOyFgFnSc4	svůj
největší	veliký	k2eAgFnSc4d3	veliký
inspiraci	inspirace	k1gFnSc4	inspirace
B.	B.	kA	B.
B.	B.	kA	B.
Kinga	King	k1gMnSc2	King
<g/>
.	.	kIx.	.
</s>
<s>
Oceněn	oceněn	k2eAgMnSc1d1	oceněn
Guitar	Guitar	k1gMnSc1	Guitar
Players	Playersa	k1gFnPc2	Playersa
Reader	Reader	k1gMnSc1	Reader
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Choice	Choice	k1gFnSc1	Choice
Award	Award	k1gMnSc1	Award
-	-	kIx~	-
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
bluesový	bluesový	k2eAgMnSc1d1	bluesový
kytarista	kytarista	k1gMnSc1	kytarista
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Bonamassa	Bonamassa	k1gFnSc1	Bonamassa
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
superskupinu	superskupina	k1gFnSc4	superskupina
Black	Black	k1gInSc1	Black
Country	country	k2eAgInSc4d1	country
Communion	Communion	k1gInSc4	Communion
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgInPc4	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
hráli	hrát	k5eAaImAgMnP	hrát
ještě	ještě	k9	ještě
Glenn	Glenn	k1gMnSc1	Glenn
Hughes	Hughes	k1gMnSc1	Hughes
<g/>
,	,	kIx,	,
Jason	Jason	k1gMnSc1	Jason
Bonham	Bonham	k1gInSc1	Bonham
a	a	k8xC	a
Derek	Derek	k1gMnSc1	Derek
Sherinian	Sherinian	k1gMnSc1	Sherinian
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2010	[number]	k4	2010
<g/>
−	−	k?	−
<g/>
2012	[number]	k4	2012
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
tři	tři	k4xCgNnPc4	tři
alba	album	k1gNnPc4	album
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
Bonamassa	Bonamass	k1gMnSc2	Bonamass
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgInS	opustit
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
–	–	k?	–
Bloodline	Bloodlin	k1gInSc5	Bloodlin
2002	[number]	k4	2002
–	–	k?	–
So	So	kA	So
<g/>
,	,	kIx,	,
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Like	Like	k1gNnSc7	Like
That	That	k1gInSc1	That
2002	[number]	k4	2002
–	–	k?	–
Live	Live	k1gInSc1	Live
at	at	k?	at
the	the	k?	the
Cabooze	Cabooz	k1gInSc5	Cabooz
2003	[number]	k4	2003
–	–	k?	–
Blues	blues	k1gInSc1	blues
Deluxe	Deluxe	k1gFnSc1	Deluxe
2004	[number]	k4	2004
–	–	k?	–
Had	had	k1gMnSc1	had
To	to	k9	to
Cry	Cry	k1gMnSc1	Cry
<g />
.	.	kIx.	.
</s>
<s>
Today	Today	k1gInPc1	Today
2005	[number]	k4	2005
–	–	k?	–
A	a	k8xC	a
New	New	k1gMnSc2	New
Day	Day	k1gMnSc2	Day
Yesterday	Yesterdaa	k1gMnSc2	Yesterdaa
<g/>
:	:	kIx,	:
Live	Live	k1gInSc1	Live
2005	[number]	k4	2005
–	–	k?	–
Live	Live	k1gInSc1	Live
at	at	k?	at
Rockpalast	Rockpalast	k1gInSc1	Rockpalast
2006	[number]	k4	2006
–	–	k?	–
You	You	k1gMnSc1	You
&	&	k?	&
Me	Me	k1gMnSc1	Me
2007	[number]	k4	2007
–	–	k?	–
Sloe	Slo	k1gInPc1	Slo
Gin	gin	k1gInSc1	gin
2008	[number]	k4	2008
–	–	k?	–
LIVE	LIVE	kA	LIVE
from	from	k6eAd1	from
nowhere	nowhrat	k5eAaPmIp3nS	nowhrat
in	in	k?	in
particular	particular	k1gInSc1	particular
2009	[number]	k4	2009
–	–	k?	–
The	The	k1gFnSc1	The
Ballad	Ballad	k1gInSc1	Ballad
of	of	k?	of
John	John	k1gMnSc1	John
Henry	Henry	k1gMnSc1	Henry
2010	[number]	k4	2010
–	–	k?	–
Black	Black	k1gInSc1	Black
Rock	rock	k1gInSc1	rock
2011	[number]	k4	2011
–	–	k?	–
Dust	Dust	k2eAgInSc4d1	Dust
Bowl	Bowl	k1gInSc4	Bowl
2011	[number]	k4	2011
–	–	k?	–
Don	Don	k1gMnSc1	Don
<g/>
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
<g/>
t	t	k?	t
Explain	Explain	k1gInSc1	Explain
(	(	kIx(	(
<g/>
s	s	k7c7	s
Beth	Beth	k1gMnSc1	Beth
Hart	Hart	k1gMnSc1	Hart
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
−	−	k?	−
Driving	Driving	k1gInSc1	Driving
Towards	Towards	k1gInSc1	Towards
the	the	k?	the
Daylight	Daylight	k2eAgInSc1d1	Daylight
2014	[number]	k4	2014
–	–	k?	–
Different	Different	k1gMnSc1	Different
Shades	Shades	k1gMnSc1	Shades
of	of	k?	of
Blue	Blue	k1gInSc1	Blue
2016	[number]	k4	2016
–	–	k?	–
Blues	blues	k1gInSc1	blues
of	of	k?	of
Desperation	Desperation	k1gInSc1	Desperation
2010	[number]	k4	2010
–	–	k?	–
Black	Black	k1gMnSc1	Black
Country	country	k2eAgMnSc1d1	country
2011	[number]	k4	2011
–	–	k?	–
2	[number]	k4	2
2012	[number]	k4	2012
−	−	k?	−
Afterglow	Afterglow	k1gFnPc2	Afterglow
2000	[number]	k4	2000
–	–	k?	–
Holy	hola	k1gFnSc2	hola
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
Joe	Joe	k1gMnSc1	Joe
Lynn	Lynn	k1gMnSc1	Lynn
Turner	turner	k1gMnSc1	turner
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
JLT	JLT	kA	JLT
(	(	kIx(	(
<g/>
Joe	Joe	k1gMnSc1	Joe
Lynn	Lynn	k1gMnSc1	Lynn
Turner	turner	k1gMnSc1	turner
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
–	–	k?	–
Full	Full	k1gInSc1	Full
Circle	Circle	k1gFnSc1	Circle
(	(	kIx(	(
<g/>
Walter	Walter	k1gMnSc1	Walter
Trout	trout	k5eAaImF	trout
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
–	–	k?	–
Trading	Trading	k1gInSc1	Trading
8	[number]	k4	8
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
Carl	Carl	k1gMnSc1	Carl
Verheyen	Verheyna	k1gFnPc2	Verheyna
Band	banda	k1gFnPc2	banda
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
–	–	k?	–
What	What	k1gMnSc1	What
You	You	k1gMnSc1	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Getting	Getting	k1gInSc1	Getting
Into	Into	k1gMnSc1	Into
(	(	kIx(	(
<g/>
Shannon	Shannon	k1gMnSc1	Shannon
Curfman	Curfman	k1gMnSc1	Curfman
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
2010	[number]	k4	2010
–	–	k?	–
Merchants	Merchants	k1gInSc1	Merchants
and	and	k?	and
Thieves	Thieves	k1gInSc1	Thieves
(	(	kIx(	(
<g/>
Sandi	Sand	k1gMnPc1	Sand
Thom	Thom	k1gMnSc1	Thom
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
–	–	k?	–
Blue	Blu	k1gMnSc4	Blu
Jay	Jay	k1gMnSc1	Jay
(	(	kIx(	(
<g/>
Healing	Healing	k1gInSc1	Healing
Sixes	Sixes	k1gInSc1	Sixes
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
–	–	k?	–
Six	Six	k1gFnSc1	Six
String	String	k1gInSc1	String
Theory	Theora	k1gFnSc2	Theora
(	(	kIx(	(
<g/>
Lee	Lea	k1gFnSc6	Lea
Ritenour	Ritenoura	k1gFnPc2	Ritenoura
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
–	–	k?	–
Still	Still	k1gMnSc1	Still
Frame	Fram	k1gInSc5	Fram
Replay	Replaa	k1gMnSc2	Replaa
(	(	kIx(	(
<g/>
Henrik	Henrik	k1gMnSc1	Henrik
Freischlader	Freischlader	k1gMnSc1	Freischlader
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
–	–	k?	–
All	All	k1gMnSc1	All
Out	Out	k1gMnSc1	Out
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
Airey	Airea	k1gMnSc2	Airea
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
–	–	k?	–
Unusual	Unusual	k1gInSc1	Unusual
Suspects	Suspects	k1gInSc1	Suspects
(	(	kIx(	(
<g/>
Leslie	Leslie	k1gFnSc1	Leslie
West	West	k1gMnSc1	West
<g/>
)	)	kIx)	)
Official	Official	k1gInSc1	Official
Joe	Joe	k1gMnSc2	Joe
Bonamassa	Bonamass	k1gMnSc2	Bonamass
Website	Websit	k1gInSc5	Websit
European	Europeany	k1gInPc2	Europeany
Joe	Joe	k1gMnSc1	Joe
Bonamassa	Bonamass	k1gMnSc2	Bonamass
Blog	Blog	k1gMnSc1	Blog
</s>
