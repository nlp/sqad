<p>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
ナ	ナ	k?	ナ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonská	japonský	k2eAgFnSc1d1	japonská
šónen	šónno	k1gNnPc2	šónno
manga	mango	k1gNnSc2	mango
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
a	a	k8xC	a
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
Masaši	Masaše	k1gFnSc4	Masaše
Kišimoto	Kišimota	k1gFnSc5	Kišimota
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
sleduje	sledovat	k5eAaImIp3nS	sledovat
chlapce	chlapec	k1gMnSc4	chlapec
Naruta	Narut	k1gMnSc4	Narut
Uzumakiho	Uzumaki	k1gMnSc4	Uzumaki
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Listové	listový	k2eAgFnSc6d1	listová
vesnici	vesnice	k1gFnSc6	vesnice
učí	učit	k5eAaImIp3nP	učit
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
pro	pro	k7c4	pro
nindža	nindža	k1gMnSc1	nindža
bojovníky	bojovník	k1gMnPc4	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
je	být	k5eAaImIp3nS	být
energický	energický	k2eAgMnSc1d1	energický
<g/>
,	,	kIx,	,
zbrklý	zbrklý	k2eAgMnSc1d1	zbrklý
a	a	k8xC	a
cílevědomý	cílevědomý	k2eAgMnSc1d1	cílevědomý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
snem	sen	k1gInSc7	sen
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
a	a	k8xC	a
nejmocnějším	mocný	k2eAgMnSc7d3	nejmocnější
nindžou	nindža	k1gMnSc7	nindža
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
–	–	k?	–
hokagem	hokag	k1gInSc7	hokag
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
nindžovském	nindžovský	k2eAgInSc6d1	nindžovský
světě	svět	k1gInSc6	svět
znamená	znamenat	k5eAaImIp3nS	znamenat
něco	něco	k3yInSc1	něco
jako	jako	k8xS	jako
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Manga	mango	k1gNnPc1	mango
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgNnP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Šúkan	Šúkan	k1gInSc4	Šúkan
šónen	šónen	k2eAgInSc1d1	šónen
Jump	Jump	k1gInSc1	Jump
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začaly	začít	k5eAaPmAgFnP	začít
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
kapitoly	kapitola	k1gFnPc1	kapitola
vycházet	vycházet	k5eAaImF	vycházet
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
tankóbon	tankóbon	k1gInSc1	tankóbon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
anime	anim	k1gInSc5	anim
adaptace	adaptace	k1gFnSc1	adaptace
mangy	mango	k1gNnPc7	mango
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
stojí	stát	k5eAaImIp3nS	stát
studio	studio	k1gNnSc1	studio
Pierrot	Pierrota	k1gFnPc2	Pierrota
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Hajato	Hajat	k2eAgNnSc4d1	Hajat
Date	Date	k1gNnSc4	Date
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
anime	animat	k5eAaPmIp3nS	animat
série	série	k1gFnPc4	série
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
220	[number]	k4	220
dílů	díl	k1gInPc2	díl
a	a	k8xC	a
poslední	poslední	k2eAgInSc1d1	poslední
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
odvysílán	odvysílat	k5eAaPmNgInS	odvysílat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
začala	začít	k5eAaPmAgFnS	začít
vycházet	vycházet	k5eAaImF	vycházet
nová	nový	k2eAgFnSc1d1	nová
série	série	k1gFnSc1	série
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
:	:	kIx,	:
Šippúden	Šippúdna	k1gFnPc2	Šippúdna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
sérii	série	k1gFnSc6	série
první	první	k4xOgFnSc6	první
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
po	po	k7c6	po
ději	děj	k1gInSc6	děj
popsaném	popsaný	k2eAgInSc6d1	popsaný
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tato	tento	k3xDgFnSc1	tento
série	série	k1gFnSc1	série
adaptuje	adaptovat	k5eAaBmIp3nS	adaptovat
manga	mango	k1gNnPc4	mango
předlohu	předloha	k1gFnSc4	předloha
a	a	k8xC	a
vycházela	vycházet	k5eAaImAgFnS	vycházet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
i	i	k9	i
několik	několik	k4yIc1	několik
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
a	a	k8xC	a
OVA	OVA	kA	OVA
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
však	však	k9	však
příběhem	příběh	k1gInSc7	příběh
nenavazují	navazovat	k5eNaImIp3nP	navazovat
na	na	k7c4	na
děj	děj	k1gInSc4	děj
v	v	k7c6	v
manze	manza	k1gFnSc6	manza
a	a	k8xC	a
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nebyly	být	k5eNaImAgFnP	být
dosud	dosud	k6eAd1	dosud
distribuovány	distribuován	k2eAgFnPc1d1	distribuována
<g/>
,	,	kIx,	,
a	a	k8xC	a
několik	několik	k4yIc4	několik
konzolových	konzolový	k2eAgFnPc2d1	konzolová
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
light	lighta	k1gFnPc2	lighta
novel	novela	k1gFnPc2	novela
a	a	k8xC	a
sběratelských	sběratelský	k2eAgFnPc2d1	sběratelská
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Licenci	licence	k1gFnSc4	licence
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
mangy	mango	k1gNnPc7	mango
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zakoupilo	zakoupit	k5eAaPmAgNnS	zakoupit
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
CREW	CREW	kA	CREW
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeden	jeden	k4xCgInSc4	jeden
svazek	svazek	k1gInSc4	svazek
vychází	vycházet	k5eAaImIp3nS	vycházet
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
CREW	CREW	kA	CREW
doposud	doposud	k6eAd1	doposud
vydalo	vydat	k5eAaPmAgNnS	vydat
třicet	třicet	k4xCc1	třicet
osm	osm	k4xCc1	osm
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Anime	Animat	k5eAaPmIp3nS	Animat
seriál	seriál	k1gInSc4	seriál
Naruto	Narut	k2eAgNnSc1d1	Naruto
v	v	k7c6	v
dabované	dabovaný	k2eAgFnSc6d1	dabovaná
verzi	verze	k1gFnSc6	verze
vysílala	vysílat	k5eAaImAgFnS	vysílat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
Jetix	Jetix	k1gInSc1	Jetix
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Disney	Disney	k1gInPc4	Disney
Channel	Channlo	k1gNnPc2	Channlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
95	[number]	k4	95
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
Jetixu	Jetix	k1gInSc2	Jetix
další	další	k2eAgInPc1d1	další
díly	díl	k1gInPc1	díl
zdály	zdát	k5eAaImAgInP	zdát
být	být	k5eAaImF	být
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
převzala	převzít	k5eAaPmAgFnS	převzít
práva	práv	k2eAgFnSc1d1	práva
k	k	k7c3	k
vysílání	vysílání	k1gNnSc3	vysílání
seriálu	seriál	k1gInSc2	seriál
stanice	stanice	k1gFnSc2	stanice
Animax	Animax	k1gInSc1	Animax
a	a	k8xC	a
AXN	AXN	kA	AXN
Sci-Fi	scii	k1gFnSc1	sci-fi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Naruta	Narut	k1gMnSc2	Narut
Uzumakiho	Uzumaki	k1gMnSc2	Uzumaki
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
zapečetěn	zapečetěn	k2eAgMnSc1d1	zapečetěn
mocný	mocný	k2eAgMnSc1d1	mocný
devítiocasý	devítiocasý	k2eAgMnSc1d1	devítiocasý
démon	démon	k1gMnSc1	démon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
devítiocasé	devítiocasý	k2eAgFnSc2d1	devítiocasá
lišky	liška	k1gFnSc2	liška
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
totiž	totiž	k9	totiž
tento	tento	k3xDgMnSc1	tento
démon	démon	k1gMnSc1	démon
napadl	napadnout	k5eAaPmAgMnS	napadnout
Listovou	listový	k2eAgFnSc4d1	listová
vesnici	vesnice	k1gFnSc4	vesnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
nebyla	být	k5eNaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
,	,	kIx,	,
zapečetil	zapečetit	k5eAaPmAgMnS	zapečetit
vůdce	vůdce	k1gMnSc1	vůdce
vesnice	vesnice	k1gFnSc2	vesnice
–	–	k?	–
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
hokage	hokage	k1gInSc1	hokage
–	–	k?	–
démona	démon	k1gMnSc2	démon
do	do	k7c2	do
právě	právě	k9	právě
narozeného	narozený	k2eAgNnSc2d1	narozené
nemluvněte	nemluvně	k1gNnSc2	nemluvně
Naruta	Narut	k1gMnSc2	Narut
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
činu	čin	k1gInSc6	čin
však	však	k9	však
sám	sám	k3xTgMnSc1	sám
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
znova	znova	k6eAd1	znova
starý	starý	k2eAgInSc4d1	starý
třetí	třetí	k4xOgInSc4	třetí
hokage	hokage	k1gInSc4	hokage
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
až	až	k9	až
o	o	k7c4	o
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
Naruta	Narut	k1gMnSc2	Narut
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
bez	bez	k7c2	bez
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
také	také	k9	také
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
zapečetěn	zapečetěn	k2eAgMnSc1d1	zapečetěn
devítiocasý	devítiocasý	k2eAgMnSc1d1	devítiocasý
démon	démon	k1gMnSc1	démon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
velmi	velmi	k6eAd1	velmi
hlučný	hlučný	k2eAgInSc1d1	hlučný
<g/>
,	,	kIx,	,
hyperaktivní	hyperaktivní	k2eAgInSc1d1	hyperaktivní
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
problematický	problematický	k2eAgMnSc1d1	problematický
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
přání	přání	k1gNnSc3	přání
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
hokage	hokag	k1gInSc2	hokag
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vesničané	vesničan	k1gMnPc1	vesničan
v	v	k7c6	v
Narutovi	Narut	k1gMnSc6	Narut
viděli	vidět	k5eAaImAgMnP	vidět
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jejich	jejich	k3xOp3gFnSc4	jejich
vesnici	vesnice	k1gFnSc4	vesnice
spasil	spasit	k5eAaPmAgMnS	spasit
před	před	k7c7	před
devítiocasým	devítiocasý	k2eAgMnSc7d1	devítiocasý
démonem	démon	k1gMnSc7	démon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc4	on
vesničané	vesničan	k1gMnPc1	vesničan
bojí	bát	k5eAaImIp3nP	bát
<g/>
,	,	kIx,	,
nenávidí	návidět	k5eNaImIp3nP	návidět
jej	on	k3xPp3gMnSc4	on
a	a	k8xC	a
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
místo	místo	k7c2	místo
symbolu	symbol	k1gInSc2	symbol
spásy	spása	k1gFnSc2	spása
vidí	vidět	k5eAaImIp3nS	vidět
hrozbu	hrozba	k1gFnSc4	hrozba
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
devítiocasého	devítiocasý	k2eAgMnSc2d1	devítiocasý
démona	démon	k1gMnSc2	démon
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
aby	aby	kYmCp3nS	aby
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
existenci	existence	k1gFnSc4	existence
<g/>
,	,	kIx,	,
Naruto	Narut	k2eAgNnSc4d1	Naruto
provádí	provádět	k5eAaImIp3nS	provádět
různé	různý	k2eAgFnPc4d1	různá
rošťárny	rošťárna	k1gFnPc4	rošťárna
a	a	k8xC	a
zoufale	zoufale	k6eAd1	zoufale
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
o	o	k7c4	o
uznání	uznání	k1gNnSc4	uznání
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
však	však	k9	však
Naruto	Narut	k2eAgNnSc1d1	Naruto
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
zapečetěn	zapečetěn	k2eAgMnSc1d1	zapečetěn
devítiocasý	devítiocasý	k2eAgMnSc1d1	devítiocasý
démon	démon	k1gMnSc1	démon
<g/>
,	,	kIx,	,
a	a	k8xC	a
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
celá	celý	k2eAgFnSc1d1	celá
ta	ten	k3xDgNnPc1	ten
léta	léto	k1gNnPc4	léto
vesničané	vesničan	k1gMnPc1	vesničan
vyhýbali	vyhýbat	k5eAaImAgMnP	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
zkoušek	zkouška	k1gFnPc2	zkouška
na	na	k7c6	na
nindžovské	nindžovský	k2eAgFnSc6d1	nindžovský
akademii	akademie	k1gFnSc6	akademie
získává	získávat	k5eAaImIp3nS	získávat
titul	titul	k1gInSc4	titul
genin	genina	k1gFnPc2	genina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přidělen	přidělit	k5eAaPmNgInS	přidělit
na	na	k7c4	na
plnění	plnění	k1gNnSc4	plnění
misí	mise	k1gFnPc2	mise
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
7	[number]	k4	7
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Kakašiho	Kakaši	k1gMnSc2	Kakaši
Hatake	Hatak	k1gMnSc2	Hatak
<g/>
,	,	kIx,	,
uznávaného	uznávaný	k2eAgMnSc2d1	uznávaný
nindži	nindža	k1gMnSc2	nindža
permanentně	permanentně	k6eAd1	permanentně
nosícího	nosící	k2eAgInSc2d1	nosící
přes	přes	k7c4	přes
obličej	obličej	k1gInSc4	obličej
masku	maska	k1gFnSc4	maska
<g/>
.	.	kIx.	.
</s>
<s>
Kakaši	Kakaš	k1gMnPc1	Kakaš
patří	patřit	k5eAaImIp3nP	patřit
v	v	k7c6	v
Listové	listový	k2eAgFnSc6d1	listová
vesnici	vesnice	k1gFnSc6	vesnice
k	k	k7c3	k
jednotkám	jednotka	k1gFnPc3	jednotka
ANBU	ANBU	kA	ANBU
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
elitní	elitní	k2eAgMnPc1d1	elitní
nindžové	nindža	k1gMnPc1	nindža
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
týmovými	týmový	k2eAgMnPc7d1	týmový
kolegy	kolega	k1gMnPc7	kolega
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
Narutova	Narutův	k2eAgFnSc1d1	Narutova
dětská	dětský	k2eAgFnSc1d1	dětská
láska	láska	k1gFnSc1	láska
-	-	kIx~	-
energická	energický	k2eAgFnSc1d1	energická
a	a	k8xC	a
protivná	protivný	k2eAgFnSc1d1	protivná
Sakura	sakura	k1gFnSc1	sakura
Haruno	Haruna	k1gFnSc5	Haruna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Naruta	Narut	k2eAgFnSc1d1	Naruta
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
hyperaktivní	hyperaktivní	k2eAgFnSc3d1	hyperaktivní
povaze	povaha	k1gFnSc3	povaha
nesnáší	snášet	k5eNaImIp3nS	snášet
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
největší	veliký	k2eAgMnSc1d3	veliký
rival	rival	k1gMnSc1	rival
Sasuke	Sasuke	k1gNnSc2	Sasuke
Učiha	Učiha	k1gMnSc1	Učiha
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgMnSc1d1	jediný
přeživší	přeživší	k2eAgMnSc1d1	přeživší
člen	člen	k1gMnSc1	člen
masakru	masakr	k1gInSc2	masakr
věhlasného	věhlasný	k2eAgInSc2d1	věhlasný
klanu	klan	k1gInSc2	klan
Učiha	Učih	k1gMnSc2	Učih
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
vyvražděn	vyvražděn	k2eAgMnSc1d1	vyvražděn
Sasukeho	Sasuke	k1gMnSc4	Sasuke
bratrem	bratr	k1gMnSc7	bratr
Itačim	Itačima	k1gFnPc2	Itačima
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
vesnici	vesnice	k1gFnSc4	vesnice
napadne	napadnout	k5eAaPmIp3nS	napadnout
Oročimaru	Oročimara	k1gFnSc4	Oročimara
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
učeň	učeň	k1gMnSc1	učeň
třetího	třetí	k4xOgMnSc2	třetí
hokageho	hokage	k1gMnSc2	hokage
<g/>
,	,	kIx,	,
Třetí	třetí	k4xOgFnSc1	třetí
při	při	k7c6	při
souboji	souboj	k1gInSc6	souboj
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
Sasuke	Sasuke	k1gFnSc1	Sasuke
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
označkován	označkován	k2eAgInSc4d1	označkován
<g/>
"	"	kIx"	"
Oročimarovou	Oročimarová	k1gFnSc4	Oročimarová
prokletou	prokletý	k2eAgFnSc4d1	prokletá
pečetí	pečeť	k1gFnSc7	pečeť
na	na	k7c4	na
krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Sasuke	Sasuke	k1gFnSc4	Sasuke
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pečeť	pečeť	k1gFnSc1	pečeť
mu	on	k3xPp3gMnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
nelidskou	lidský	k2eNgFnSc4d1	nelidská
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Sasuke	Sasuke	k1gFnSc1	Sasuke
opustí	opustit	k5eAaPmIp3nS	opustit
vesnici	vesnice	k1gFnSc4	vesnice
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
Oročimara	Oročimar	k1gMnSc4	Oročimar
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
žáka	žák	k1gMnSc4	žák
a	a	k8xC	a
naučil	naučit	k5eAaPmAgMnS	naučit
ho	on	k3xPp3gNnSc4	on
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
umí	umět	k5eAaImIp3nS	umět
<g/>
.	.	kIx.	.
</s>
<s>
Oročimaru	Oročimara	k1gFnSc4	Oročimara
jej	on	k3xPp3gNnSc4	on
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
přijme	přijmout	k5eAaPmIp3nS	přijmout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
díky	díky	k7c3	díky
složité	složitý	k2eAgFnSc3d1	složitá
technice	technika	k1gFnSc3	technika
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
musí	muset	k5eAaImIp3nS	muset
každé	každý	k3xTgInPc4	každý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
vyměnit	vyměnit	k5eAaPmF	vyměnit
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
touží	toužit	k5eAaImIp3nP	toužit
právě	právě	k9	právě
po	po	k7c6	po
tělesné	tělesný	k2eAgFnSc6d1	tělesná
schránce	schránka	k1gFnSc6	schránka
mladého	mladý	k2eAgMnSc2d1	mladý
Sasukeho	Sasuke	k1gMnSc2	Sasuke
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
síly	síla	k1gFnSc2	síla
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
klanu	klan	k1gInSc2	klan
Učiha	Učiha	k1gFnSc1	Učiha
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
tým	tým	k1gInSc1	tým
7	[number]	k4	7
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Sakuru	sakura	k1gFnSc4	sakura
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
jako	jako	k9	jako
žákyni	žákyně	k1gFnSc4	žákyně
sama	sám	k3xTgFnSc1	sám
nově	nově	k6eAd1	nově
zvolená	zvolený	k2eAgFnSc1d1	zvolená
pátá	pátý	k4xOgFnSc1	pátý
hokage	hokage	k1gFnSc1	hokage
–	–	k?	–
Cunade	Cunad	k1gInSc5	Cunad
<g/>
,	,	kIx,	,
také	také	k9	také
bývalá	bývalý	k2eAgFnSc1d1	bývalá
žákyně	žákyně	k1gFnSc1	žákyně
Třetího	třetí	k4xOgInSc2	třetí
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
s	s	k7c7	s
Oročimarem	Oročimar	k1gInSc7	Oročimar
<g/>
,	,	kIx,	,
a	a	k8xC	a
Naruta	Narut	k2eAgFnSc1d1	Naruta
přijme	přijmout	k5eAaPmIp3nS	přijmout
jako	jako	k8xC	jako
učedníka	učedník	k1gMnSc2	učedník
třetí	třetí	k4xOgFnSc3	třetí
žák	žák	k1gMnSc1	žák
Třetího	třetí	k4xOgInSc2	třetí
<g/>
,	,	kIx,	,
poutník	poutník	k1gMnSc1	poutník
Džiraija	Džiraija	k1gMnSc1	Džiraija
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
kromě	kromě	k7c2	kromě
své	svůj	k3xOyFgFnSc2	svůj
nepřemožitelnosti	nepřemožitelnost	k1gFnSc2	nepřemožitelnost
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
slavným	slavný	k2eAgMnSc7d1	slavný
pisatelem	pisatel	k1gMnSc7	pisatel
erotických	erotický	k2eAgFnPc2d1	erotická
knížek	knížka	k1gFnPc2	knížka
<g/>
.	.	kIx.	.
</s>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
ho	on	k3xPp3gMnSc4	on
proto	proto	k8xC	proto
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
zvrhlým	zvrhlý	k2eAgMnSc7d1	zvrhlý
poustevníkem	poustevník	k1gMnSc7	poustevník
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
hierarchii	hierarchie	k1gFnSc6	hierarchie
nezvyklé	zvyklý	k2eNgNnSc1d1	nezvyklé
<g/>
,	,	kIx,	,
tyká	tykat	k5eAaImIp3nS	tykat
mu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
neuctivě	uctivě	k6eNd1	uctivě
<g/>
.	.	kIx.	.
</s>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
ovšem	ovšem	k9	ovšem
netuší	tušit	k5eNaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Džiraija	Džiraija	k1gFnSc1	Džiraija
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jeho	jeho	k3xOp3gMnPc7	jeho
kmotrem	kmotr	k1gMnSc7	kmotr
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
ho	on	k3xPp3gMnSc4	on
moc	moc	k6eAd1	moc
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
nechá	nechat	k5eAaPmIp3nS	nechat
mnoho	mnoho	k6eAd1	mnoho
líbit	líbit	k5eAaImF	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Sasukeho	Sasukeze	k6eAd1	Sasukeze
odchod	odchod	k1gInSc1	odchod
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
a	a	k8xC	a
Akacuki	Akacuk	k1gFnSc2	Akacuk
<g/>
,	,	kIx,	,
tajemná	tajemný	k2eAgFnSc1d1	tajemná
organizace	organizace	k1gFnSc1	organizace
vysoce	vysoce	k6eAd1	vysoce
nebezpečných	bezpečný	k2eNgMnPc2d1	nebezpečný
nindžů	nindža	k1gMnPc2	nindža
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
touží	toužit	k5eAaImIp3nP	toužit
získat	získat	k5eAaPmF	získat
devítiocasého	devítiocasý	k2eAgMnSc4d1	devítiocasý
démona	démon	k1gMnSc4	démon
uvnitř	uvnitř	k6eAd1	uvnitř
Naruta	Narut	k2eAgFnSc1d1	Naruta
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vedou	vést	k5eAaImIp3nP	vést
Naruta	Narut	k1gMnSc4	Narut
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
učitelem	učitel	k1gMnSc7	učitel
Džiraijou	Džiraijou	k1gMnSc7	Džiraijou
na	na	k7c4	na
dalekou	daleký	k2eAgFnSc4d1	daleká
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
naučit	naučit	k5eAaPmF	naučit
nové	nový	k2eAgFnPc4d1	nová
techniky	technika	k1gFnPc4	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Naruto	Narut	k2eAgNnSc1d1	Naruto
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Listové	listový	k2eAgFnSc2d1	listová
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzo	brzo	k6eAd1	brzo
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
již	již	k6eAd1	již
vyšší	vysoký	k2eAgFnSc1d2	vyšší
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
příběhu	příběh	k1gInSc2	příběh
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
organizace	organizace	k1gFnSc1	organizace
Akacuki	Akacuk	k1gFnSc2	Akacuk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
získat	získat	k5eAaPmF	získat
všechny	všechen	k3xTgMnPc4	všechen
ocasaté	ocasatý	k2eAgMnPc4d1	ocasatý
démony	démon	k1gMnPc4	démon
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
účely	účel	k1gInPc4	účel
–	–	k?	–
a	a	k8xC	a
právě	právě	k9	právě
Narutův	Narutův	k2eAgMnSc1d1	Narutův
bývalý	bývalý	k2eAgMnSc1d1	bývalý
nepřítel	nepřítel	k1gMnSc1	nepřítel
Gaara	Gaara	k1gMnSc1	Gaara
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
zapečetěného	zapečetěný	k2eAgMnSc2d1	zapečetěný
jednoocasého	jednoocasý	k2eAgMnSc2d1	jednoocasý
démona	démon	k1gMnSc2	démon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
jejich	jejich	k3xOp3gFnSc7	jejich
obětí	oběť	k1gFnSc7	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
a	a	k8xC	a
Sakura	sakura	k1gFnSc1	sakura
získávají	získávat	k5eAaImIp3nP	získávat
také	také	k9	také
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Sasukeho	Sasuke	k1gMnSc2	Sasuke
z	z	k7c2	z
Listové	listový	k2eAgFnSc2d1	listová
vesnice	vesnice	k1gFnSc2	vesnice
nového	nový	k2eAgMnSc4d1	nový
týmového	týmový	k2eAgMnSc4d1	týmový
kolegu	kolega	k1gMnSc4	kolega
<g/>
,	,	kIx,	,
tajemného	tajemný	k2eAgMnSc4d1	tajemný
mladého	mladý	k1gMnSc4	mladý
nindžu	nindža	k1gMnSc4	nindža
jménem	jméno	k1gNnSc7	jméno
Sai	Sai	k1gMnSc1	Sai
<g/>
,	,	kIx,	,
a	a	k8xC	a
během	během	k7c2	během
krátkého	krátký	k2eAgInSc2d1	krátký
pobytu	pobyt	k1gInSc2	pobyt
jejich	jejich	k3xOp3gMnSc2	jejich
velitele	velitel	k1gMnSc2	velitel
Kakašiho	Kakaši	k1gMnSc2	Kakaši
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
i	i	k9	i
nového	nový	k2eAgMnSc4d1	nový
kapitána	kapitán	k1gMnSc4	kapitán
Jamata	Jamat	k1gMnSc4	Jamat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Narutova	Narutův	k2eAgFnSc1d1	Narutova
přírodní	přírodní	k2eAgFnSc1d1	přírodní
čakra	čakra	k1gFnSc1	čakra
je	být	k5eAaImIp3nS	být
větrného	větrný	k2eAgInSc2d1	větrný
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
Naruto	Narut	k2eAgNnSc1d1	Naruto
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
zdokonalení	zdokonalení	k1gNnSc4	zdokonalení
a	a	k8xC	a
vylepšení	vylepšení	k1gNnSc4	vylepšení
svých	svůj	k3xOyFgFnPc2	svůj
větrných	větrný	k2eAgFnPc2d1	větrná
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
o	o	k7c4	o
zdokonalení	zdokonalení	k1gNnSc4	zdokonalení
svého	svůj	k3xOyFgInSc2	svůj
rasenganu	rasengan	k1gInSc2	rasengan
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
mu	on	k3xPp3gInSc3	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
Kakaši	Kakaš	k1gMnPc1	Kakaš
a	a	k8xC	a
Jamato	Jamat	k2eAgNnSc1d1	Jamato
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
také	také	k9	také
snaží	snažit	k5eAaImIp3nS	snažit
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
ze	z	k7c2	z
spárů	spár	k1gInPc2	spár
Oročimara	Oročimar	k1gMnSc4	Oročimar
svého	svůj	k1gMnSc4	svůj
bývalého	bývalý	k2eAgMnSc4d1	bývalý
týmového	týmový	k2eAgMnSc4d1	týmový
kolegu	kolega	k1gMnSc4	kolega
Sasukeho	Sasuke	k1gMnSc4	Sasuke
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
však	však	k9	však
později	pozdě	k6eAd2	pozdě
samotného	samotný	k2eAgMnSc2d1	samotný
Oročimara	Oročimar	k1gMnSc2	Oročimar
překoná	překonat	k5eAaPmIp3nS	překonat
díky	díky	k7c3	díky
faktu	fakt	k1gInSc3	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
Oročimaru	Oročimara	k1gFnSc4	Oročimara
již	již	k6eAd1	již
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
mají	mít	k5eAaImIp3nP	mít
obnovit	obnovit	k5eAaPmF	obnovit
po	po	k7c6	po
technice	technika	k1gFnSc6	technika
převtělení	převtělení	k1gNnPc2	převtělení
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Džiraija	Džiraija	k1gFnSc1	Džiraija
vydává	vydávat	k5eAaImIp3nS	vydávat
na	na	k7c4	na
výzvědnou	výzvědný	k2eAgFnSc4d1	výzvědná
misi	mise	k1gFnSc4	mise
do	do	k7c2	do
Deštné	deštný	k2eAgFnSc2d1	Deštná
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
žije	žít	k5eAaImIp3nS	žít
samotný	samotný	k2eAgInSc1d1	samotný
vůdce	vůdce	k1gMnSc4	vůdce
Akacuki	Akacuki	k1gNnSc2	Akacuki
zvaný	zvaný	k2eAgInSc4d1	zvaný
Pain	Pain	k1gInSc4	Pain
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
bývalými	bývalý	k2eAgMnPc7d1	bývalý
žáky	žák	k1gMnPc7	žák
Konan	Konany	k1gInPc2	Konany
a	a	k8xC	a
Jahikem	Jahik	k1gInSc7	Jahik
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
z	z	k7c2	z
neznámého	známý	k2eNgInSc2d1	neznámý
důvodu	důvod	k1gInSc2	důvod
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
téměř	téměř	k6eAd1	téměř
legendární	legendární	k2eAgFnSc4d1	legendární
oční	oční	k2eAgFnSc4d1	oční
techniku	technika	k1gFnSc4	technika
zvanou	zvaný	k2eAgFnSc4d1	zvaná
rinnegan	rinnegana	k1gFnPc2	rinnegana
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
boha	bůh	k1gMnSc2	bůh
Paina	Pain	k1gMnSc2	Pain
<g/>
.	.	kIx.	.
</s>
<s>
Džiraija	Džiraija	k6eAd1	Džiraija
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
tajemství	tajemství	k1gNnSc4	tajemství
Painových	Painův	k2eAgNnPc2d1	Painův
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
taky	taky	k6eAd1	taky
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
s	s	k7c7	s
Nagatem	Nagat	k1gInSc7	Nagat
–	–	k?	–
třetím	třetí	k4xOgMnSc7	třetí
žákem	žák	k1gMnSc7	žák
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
učil	učit	k5eAaImAgInS	učit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Konan	Konana	k1gFnPc2	Konana
a	a	k8xC	a
Jahikem	Jahikum	k1gNnSc7	Jahikum
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
však	však	k9	však
k	k	k7c3	k
souboji	souboj	k1gInSc3	souboj
s	s	k7c7	s
Konan	Konan	k1gInSc4	Konan
a	a	k8xC	a
Jahikem	Jahik	k1gMnSc7	Jahik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Džiraija	Džiraija	k1gMnSc1	Džiraija
prohrává	prohrávat	k5eAaImIp3nS	prohrávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
než	než	k8xS	než
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
stihne	stihnout	k5eAaPmIp3nS	stihnout
poslat	poslat	k5eAaPmF	poslat
zakódovanou	zakódovaný	k2eAgFnSc4d1	zakódovaná
zprávu	zpráva	k1gFnSc4	zpráva
do	do	k7c2	do
Listové	listový	k2eAgFnSc2d1	listová
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Oročimara	Oročimar	k1gMnSc2	Oročimar
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
Sasuke	Sasuke	k1gFnSc1	Sasuke
zabít	zabít	k5eAaPmF	zabít
bratra	bratr	k1gMnSc4	bratr
Itačiho	Itači	k1gMnSc4	Itači
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
nakonec	nakonec	k6eAd1	nakonec
skutečně	skutečně	k6eAd1	skutečně
porazí	porazit	k5eAaPmIp3nS	porazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
Tobiho	Tobi	k1gMnSc2	Tobi
<g/>
,	,	kIx,	,
dalšího	další	k2eAgMnSc4d1	další
člena	člen	k1gMnSc4	člen
Akacuki	Akacuk	k1gFnSc2	Akacuk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Itači	Itač	k1gFnSc6	Itač
svůj	svůj	k3xOyFgInSc4	svůj
klan	klan	k1gInSc4	klan
vyvraždil	vyvraždit	k5eAaPmAgMnS	vyvraždit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
vedení	vedení	k1gNnSc2	vedení
Listové	listový	k2eAgFnSc2d1	listová
vesnice	vesnice	k1gFnSc2	vesnice
a	a	k8xC	a
Sasukeho	Sasuke	k1gMnSc4	Sasuke
ušetřil	ušetřit	k5eAaPmAgMnS	ušetřit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
zkrátka	zkrátka	k6eAd1	zkrátka
sám	sám	k3xTgInSc4	sám
zabít	zabít	k5eAaPmF	zabít
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Sasuke	Sasuke	k1gFnSc1	Sasuke
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
pomstít	pomstít	k5eAaPmF	pomstít
Listové	listový	k2eAgFnSc3d1	listová
vesnici	vesnice	k1gFnSc3	vesnice
a	a	k8xC	a
vyhladit	vyhladit	k5eAaPmF	vyhladit
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Naruto	Narut	k2eAgNnSc4d1	Naruto
zatím	zatím	k6eAd1	zatím
netuší	tušit	k5eNaImIp3nS	tušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Itačiho	Itači	k1gMnSc2	Itači
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaPmIp3nS	vydávat
Pain	Pain	k1gInSc1	Pain
do	do	k7c2	do
Listové	listový	k2eAgFnSc2d1	listová
vesnice	vesnice	k1gFnSc2	vesnice
chytit	chytit	k5eAaPmF	chytit
Naruta	Narut	k1gMnSc4	Narut
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
vyextrahovat	vyextrahovat	k5eAaPmF	vyextrahovat
devítiocasého	devítiocasý	k2eAgMnSc4d1	devítiocasý
démona	démon	k1gMnSc4	démon
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
obrovské	obrovský	k2eAgFnPc4d1	obrovská
síly	síla	k1gFnPc4	síla
těžící	těžící	k2eAgFnPc4d1	těžící
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
legendární	legendární	k2eAgFnSc2d1	legendární
oční	oční	k2eAgFnSc2d1	oční
techniky	technika	k1gFnSc2	technika
rinneganu	rinnegan	k1gInSc2	rinnegan
využije	využít	k5eAaPmIp3nS	využít
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
šesti	šest	k4xCc2	šest
sfér	sféra	k1gFnPc2	sféra
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc2d1	celá
Listové	listový	k2eAgFnSc2d1	listová
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Naruta	Narut	k1gMnSc4	Narut
našel	najít	k5eAaPmAgInS	najít
a	a	k8xC	a
porazil	porazit	k5eAaPmAgInS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
však	však	k9	však
pomocí	pomocí	k7c2	pomocí
nové	nový	k2eAgFnSc2d1	nová
techniky	technika	k1gFnSc2	technika
sendžucu	sendžucu	k5eAaPmIp1nS	sendžucu
všechny	všechen	k3xTgFnPc4	všechen
sféry	sféra	k1gFnPc4	sféra
postupně	postupně	k6eAd1	postupně
porazí	porazit	k5eAaPmIp3nS	porazit
a	a	k8xC	a
vydá	vydat	k5eAaPmIp3nS	vydat
se	se	k3xPyFc4	se
za	za	k7c7	za
Painem	Pain	k1gInSc7	Pain
a	a	k8xC	a
Konan	Konan	k1gInSc4	Konan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
promluvil	promluvit	k5eAaPmAgMnS	promluvit
o	o	k7c6	o
Džiraijovi	Džiraij	k1gMnSc6	Džiraij
a	a	k8xC	a
otázce	otázka	k1gFnSc6	otázka
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Painova	Painův	k2eAgInSc2d1	Painův
příběhu	příběh	k1gInSc2	příběh
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Painem	Pain	k1gInSc7	Pain
samotným	samotný	k2eAgInSc7d1	samotný
není	být	k5eNaImIp3nS	být
Jahiko	Jahika	k1gFnSc5	Jahika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Nagato	Nagat	k2eAgNnSc1d1	Nagato
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Jahikovo	Jahikův	k2eAgNnSc1d1	Jahikův
tělo	tělo	k1gNnSc4	tělo
používal	používat	k5eAaImAgInS	používat
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svým	svůj	k3xOyFgInSc7	svůj
šesti	šest	k4xCc3	šest
sfér	sféra	k1gFnPc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Narutovi	Narut	k1gMnSc3	Narut
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
podaří	podařit	k5eAaPmIp3nS	podařit
Nagata	Nagata	k1gFnSc1	Nagata
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
absolutního	absolutní	k2eAgInSc2d1	absolutní
míru	mír	k1gInSc2	mír
nedosáhne	dosáhnout	k5eNaPmIp3nS	dosáhnout
ničením	ničení	k1gNnSc7	ničení
a	a	k8xC	a
zabíjením	zabíjení	k1gNnSc7	zabíjení
<g/>
.	.	kIx.	.
</s>
<s>
Nagato	Nagato	k6eAd1	Nagato
následně	následně	k6eAd1	následně
obětuje	obětovat	k5eAaBmIp3nS	obětovat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
oživil	oživit	k5eAaPmAgMnS	oživit
všechny	všechen	k3xTgMnPc4	všechen
padlé	padlý	k1gMnPc4	padlý
během	během	k7c2	během
ničení	ničení	k1gNnSc2	ničení
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
činem	čin	k1gInSc7	čin
se	se	k3xPyFc4	se
z	z	k7c2	z
Naruta	Narut	k1gMnSc2	Narut
stává	stávat	k5eAaImIp3nS	stávat
hrdina	hrdina	k1gMnSc1	hrdina
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tobi	Tob	k1gMnSc3	Tob
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Nagata	Nagata	k1gFnSc1	Nagata
začne	začít	k5eAaPmIp3nS	začít
veřejně	veřejně	k6eAd1	veřejně
představovat	představovat	k5eAaImF	představovat
jako	jako	k9	jako
legendární	legendární	k2eAgMnSc1d1	legendární
nindža	nindža	k1gMnSc1	nindža
Madara	Madar	k1gMnSc2	Madar
Učiha	Učih	k1gMnSc2	Učih
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Sasukem	Sasuk	k1gInSc7	Sasuk
na	na	k7c4	na
sněm	sněm	k1gInSc4	sněm
všech	všecek	k3xTgInPc2	všecek
pěti	pět	k4xCc2	pět
kage	kage	k1gNnPc2	kage
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
nindžů	nindža	k1gMnPc2	nindža
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
získat	získat	k5eAaPmF	získat
všechny	všechen	k3xTgInPc1	všechen
bidžú	bidžú	k?	bidžú
<g/>
,	,	kIx,	,
oživit	oživit	k5eAaPmF	oživit
původního	původní	k2eAgMnSc4d1	původní
desetiocasého	desetiocasý	k2eAgMnSc4d1	desetiocasý
démona	démon	k1gMnSc4	démon
a	a	k8xC	a
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
vytvořit	vytvořit	k5eAaPmF	vytvořit
idealizovaný	idealizovaný	k2eAgInSc1d1	idealizovaný
svět	svět	k1gInSc1	svět
bez	bez	k7c2	bez
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
proto	proto	k8xC	proto
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
na	na	k7c4	na
želví	želví	k2eAgInSc4d1	želví
ostrov	ostrov	k1gInSc4	ostrov
trénovat	trénovat	k5eAaImF	trénovat
ovládání	ovládání	k1gNnSc4	ovládání
devítiocasého	devítiocasý	k2eAgMnSc2d1	devítiocasý
démona	démon	k1gMnSc2	démon
společně	společně	k6eAd1	společně
s	s	k7c7	s
hostitelem	hostitel	k1gMnSc7	hostitel
osmiocasého	osmiocasý	k2eAgMnSc4d1	osmiocasý
démona	démon	k1gMnSc4	démon
<g/>
,	,	kIx,	,
Killer	Killer	k1gMnSc1	Killer
Beem	Beem	k1gMnSc1	Beem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
boje	boj	k1gInSc2	boj
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
zemřelou	zemřelá	k1gFnSc7	zemřelá
matkou	matka	k1gFnSc7	matka
Kušinou	Kušina	k1gFnSc7	Kušina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
před	před	k7c7	před
16	[number]	k4	16
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
tajemný	tajemný	k2eAgInSc1d1	tajemný
Tobi	Tob	k1gFnSc3	Tob
Konan	Konan	k1gInSc1	Konan
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ona	onen	k3xDgFnSc1	onen
jediná	jediný	k2eAgFnSc1d1	jediná
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Nagatův	Nagatův	k2eAgInSc1d1	Nagatův
rinnegan	rinnegan	k1gInSc1	rinnegan
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Konan	Konan	k1gMnSc1	Konan
po	po	k7c6	po
Nagatově	Nagatův	k2eAgFnSc6d1	Nagatova
smrti	smrt	k1gFnSc6	smrt
Akacuki	Akacuki	k1gNnSc2	Akacuki
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
,	,	kIx,	,
odmítá	odmítat	k5eAaImIp3nS	odmítat
s	s	k7c7	s
Tobim	Tobi	k1gNnSc7	Tobi
jakkoliv	jakkoliv	k6eAd1	jakkoliv
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
a	a	k8xC	a
pokusí	pokusit	k5eAaPmIp3nS	pokusit
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
zničit	zničit	k5eAaPmF	zničit
pomocí	pomoc	k1gFnSc7	pomoc
miliard	miliarda	k4xCgFnPc2	miliarda
výbušných	výbušný	k2eAgInPc2d1	výbušný
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Tobi	Tobe	k1gFnSc4	Tobe
díky	díky	k7c3	díky
zakázané	zakázaný	k2eAgFnSc3d1	zakázaná
technice	technika	k1gFnSc3	technika
Učihů	Učih	k1gMnPc2	Učih
přežije	přežít	k5eAaPmIp3nS	přežít
<g/>
,	,	kIx,	,
z	z	k7c2	z
Konan	Konana	k1gFnPc2	Konana
získá	získat	k5eAaPmIp3nS	získat
potřebné	potřebný	k2eAgFnPc4d1	potřebná
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
zabije	zabít	k5eAaPmIp3nS	zabít
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgFnPc2	všecek
pět	pět	k4xCc1	pět
velkých	velká	k1gFnPc2	velká
národů	národ	k1gInPc2	národ
spojuje	spojovat	k5eAaImIp3nS	spojovat
své	svůj	k3xOyFgFnSc2	svůj
síly	síla	k1gFnSc2	síla
proti	proti	k7c3	proti
Akacuki	Akacuk	k1gFnSc3	Akacuk
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
Aliance	aliance	k1gFnSc1	aliance
pěti	pět	k4xCc2	pět
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
ale	ale	k9	ale
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
válka	válka	k1gFnSc1	válka
slouží	sloužit	k5eAaImIp3nS	sloužit
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
doví	dovědět	k5eAaPmIp3nS	dovědět
<g/>
,	,	kIx,	,
rozhodně	rozhodně	k6eAd1	rozhodně
se	se	k3xPyFc4	se
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
mu	on	k3xPp3gMnSc3	on
chce	chtít	k5eAaImIp3nS	chtít
zabránit	zabránit	k5eAaPmF	zabránit
Cunade	Cunad	k1gInSc5	Cunad
a	a	k8xC	a
současný	současný	k2eAgInSc4d1	současný
raikage	raikage	k1gInSc4	raikage
<g/>
.	.	kIx.	.
</s>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
důležitosti	důležitost	k1gFnSc6	důležitost
na	na	k7c6	na
bitevním	bitevní	k2eAgNnSc6d1	bitevní
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
kagů	kag	k1gMnPc2	kag
mezitím	mezitím	k6eAd1	mezitím
čelí	čelit	k5eAaImIp3nS	čelit
oživenému	oživený	k2eAgMnSc3d1	oživený
Madarovi	Madar	k1gMnSc3	Madar
Učihovi	Učih	k1gMnSc3	Učih
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
všechny	všechen	k3xTgFnPc4	všechen
téměř	téměř	k6eAd1	téměř
zabije	zabít	k5eAaPmIp3nS	zabít
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
pomoci	pomoct	k5eAaPmF	pomoct
svému	svůj	k3xOyFgMnSc3	svůj
parťákovi	parťák	k1gMnSc3	parťák
tajemnému	tajemný	k2eAgMnSc3d1	tajemný
Tobimu	Tobim	k1gMnSc3	Tobim
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
souboji	souboj	k1gInSc6	souboj
Tobiho	Tobi	k1gMnSc2	Tobi
s	s	k7c7	s
Narutem	Narut	k1gMnSc7	Narut
se	se	k3xPyFc4	se
však	však	k9	však
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
za	za	k7c4	za
Tobiho	Tobi	k1gMnSc4	Tobi
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
vydával	vydávat	k5eAaImAgInS	vydávat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Obito	obit	k2eAgNnSc1d1	Obito
Učiha	Učih	k1gMnSc4	Učih
<g/>
,	,	kIx,	,
Kakašiho	Kakaši	k1gMnSc4	Kakaši
bývalý	bývalý	k2eAgMnSc1d1	bývalý
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
údajně	údajně	k6eAd1	údajně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
třetí	třetí	k4xOgFnSc6	třetí
velké	velký	k2eAgFnSc6d1	velká
válce	válka	k1gFnSc6	válka
nindžů	nindža	k1gMnPc2	nindža
<g/>
.	.	kIx.	.
</s>
<s>
Obitovi	Obitův	k2eAgMnPc1d1	Obitův
a	a	k8xC	a
Madarovi	Madarův	k2eAgMnPc1d1	Madarův
se	se	k3xPyFc4	se
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
podaří	podařit	k5eAaPmIp3nS	podařit
vyvolat	vyvolat	k5eAaPmF	vyvolat
desetiocasého	desetiocasý	k2eAgMnSc4d1	desetiocasý
démona	démon	k1gMnSc4	démon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
prostředkem	prostředek	k1gInSc7	prostředek
k	k	k7c3	k
ovládnutí	ovládnutí	k1gNnSc3	ovládnutí
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Desetiocasého	desetiocasý	k2eAgMnSc2d1	desetiocasý
ale	ale	k8xC	ale
nejde	jít	k5eNaImIp3nS	jít
přemoci	přemoct	k5eAaPmF	přemoct
tak	tak	k9	tak
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
zničení	zničení	k1gNnSc4	zničení
zemřou	zemřít	k5eAaPmIp3nP	zemřít
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Nedži	Nedž	k1gFnSc3	Nedž
<g/>
,	,	kIx,	,
Inoiči	Inoič	k1gInSc3	Inoič
nebo	nebo	k8xC	nebo
Šikaku	Šikak	k1gInSc3	Šikak
<g/>
.	.	kIx.	.
</s>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
propadá	propadat	k5eAaPmIp3nS	propadat
zoufalství	zoufalství	k1gNnSc1	zoufalství
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
ho	on	k3xPp3gInSc4	on
nakonec	nakonec	k6eAd1	nakonec
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
Hinata	Hinata	k1gFnSc1	Hinata
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
chystají	chystat	k5eAaImIp3nP	chystat
i	i	k9	i
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
armády	armáda	k1gFnSc2	armáda
zničit	zničit	k5eAaPmF	zničit
nadobro	nadobro	k6eAd1	nadobro
desetiocasého	desetiocasý	k2eAgMnSc4d1	desetiocasý
démona	démon	k1gMnSc4	démon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Manga	mango	k1gNnSc2	mango
==	==	k?	==
</s>
</p>
<p>
<s>
Masaši	Masasat	k5eAaPmIp1nSwK	Masasat
Kišimoto	Kišimota	k1gFnSc5	Kišimota
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
pilotní	pilotní	k2eAgFnSc4d1	pilotní
kapitolu	kapitola	k1gFnSc4	kapitola
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Akamaru	Akamar	k1gInSc2	Akamar
Jump	Jump	k1gMnSc1	Jump
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sklidila	sklidit	k5eAaPmAgFnS	sklidit
velmi	velmi	k6eAd1	velmi
příznivé	příznivý	k2eAgInPc4d1	příznivý
ohlasy	ohlas	k1gInPc4	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
manga	mango	k1gNnSc2	mango
vydávat	vydávat	k5eAaPmF	vydávat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Šúkan	Šúkan	k1gInSc4	Šúkan
šónen	šónen	k2eAgInSc1d1	šónen
Jump	Jump	k1gInSc1	Jump
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
238	[number]	k4	238
kapitol	kapitola	k1gFnPc2	kapitola
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
příběh	příběh	k1gInSc4	příběh
Naruta	Narut	k1gMnSc4	Narut
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
dvanácti	dvanáct	k4xCc2	dvanáct
až	až	k8xS	až
třinácti	třináct	k4xCc2	třináct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
děj	děj	k1gInSc1	děj
od	od	k7c2	od
kapitoly	kapitola	k1gFnSc2	kapitola
245	[number]	k4	245
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
je	být	k5eAaImIp3nS	být
vložen	vložen	k2eAgInSc1d1	vložen
Kakašiho	Kakaši	k1gMnSc2	Kakaši
gaiden	gaidna	k1gFnPc2	gaidna
(	(	kIx(	(
<g/>
kapitoly	kapitola	k1gFnSc2	kapitola
239	[number]	k4	239
<g/>
–	–	k?	–
<g/>
244	[number]	k4	244
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doposud	doposud	k6eAd1	doposud
bylo	být	k5eAaImAgNnS	být
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Šúeiša	Šúeiš	k1gInSc2	Šúeiš
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
vydáno	vydat	k5eAaPmNgNnS	vydat
72	[number]	k4	72
svazků	svazek	k1gInPc2	svazek
mangy	mango	k1gNnPc7	mango
formátu	formát	k1gInSc3	formát
tankóbon	tankóbon	k1gInSc1	tankóbon
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
úvodní	úvodní	k2eAgInSc4d1	úvodní
svazek	svazek	k1gInSc4	svazek
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Narutova	Narutův	k2eAgInSc2d1	Narutův
příběhu	příběh	k1gInSc2	příběh
v	v	k7c6	v
období	období	k1gNnSc6	období
dvanácti	dvanáct	k4xCc2	dvanáct
až	až	k9	až
třinácti	třináct	k4xCc2	třináct
let	léto	k1gNnPc2	léto
včetně	včetně	k7c2	včetně
zmíněného	zmíněný	k2eAgMnSc2d1	zmíněný
Kakašiho	Kakaši	k1gMnSc2	Kakaši
gaidenu	gaiden	k1gInSc2	gaiden
popisuje	popisovat	k5eAaImIp3nS	popisovat
prvních	první	k4xOgInPc2	první
27	[number]	k4	27
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
–	–	k?	–
sedmdesátý	sedmdesátý	k4xOgInSc1	sedmdesátý
první	první	k4xOgInSc1	první
–	–	k?	–
svazek	svazek	k1gInSc1	svazek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
vydán	vydat	k5eAaPmNgInS	vydat
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Šúeiša	Šúeiša	k6eAd1	Šúeiša
vydává	vydávat	k5eAaImIp3nS	vydávat
mangu	mango	k1gNnSc3	mango
i	i	k8xC	i
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Manga	mango	k1gNnSc2	mango
Capsule	Capsule	k1gFnSc2	Capsule
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
mangu	mango	k1gNnSc6	mango
vydává	vydávat	k5eAaPmIp3nS	vydávat
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
CREW	CREW	kA	CREW
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
doposud	doposud	k6eAd1	doposud
vydalo	vydat	k5eAaPmAgNnS	vydat
třicet	třicet	k4xCc1	třicet
osm	osm	k4xCc1	osm
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Anime	Anim	k1gInSc5	Anim
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Naruto	Narut	k2eAgNnSc4d1	Naruto
===	===	k?	===
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
seriál	seriál	k1gInSc1	seriál
Studia	studio	k1gNnSc2	studio
Pierrot	Pierrota	k1gFnPc2	Pierrota
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Hajata	Hajat	k1gMnSc2	Hajat
Dateho	Date	k1gMnSc2	Date
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
vysílat	vysílat	k5eAaImF	vysílat
každý	každý	k3xTgInSc4	každý
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2002	[number]	k4	2002
na	na	k7c6	na
TV	TV	kA	TV
Tokyo	Tokyo	k6eAd1	Tokyo
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
adaptuje	adaptovat	k5eAaBmIp3nS	adaptovat
děj	děj	k1gInSc4	děj
prvních	první	k4xOgInPc2	první
dvaceti	dvacet	k4xCc2	dvacet
sedmi	sedm	k4xCc2	sedm
svazků	svazek	k1gInPc2	svazek
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
mangy	mango	k1gNnPc7	mango
od	od	k7c2	od
Masaši	Masaš	k1gMnSc3	Masaš
Kišimota	Kišimota	k1gFnSc1	Kišimota
do	do	k7c2	do
135	[number]	k4	135
<g/>
.	.	kIx.	.
dílu	díl	k1gInSc2	díl
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
děj	děj	k1gInSc1	děj
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
epizod	epizoda	k1gFnPc2	epizoda
není	být	k5eNaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
manze	manza	k1gFnSc6	manza
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
vymyšlen	vymyslet	k5eAaPmNgInS	vymyslet
scenáristy	scenárista	k1gMnPc4	scenárista
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
220	[number]	k4	220
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
odvysílán	odvysílat	k5eAaPmNgInS	odvysílat
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
začala	začít	k5eAaPmAgFnS	začít
TV	TV	kA	TV
Tokyo	Tokyo	k6eAd1	Tokyo
vysílat	vysílat	k5eAaImF	vysílat
seriál	seriál	k1gInSc4	seriál
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
každou	každý	k3xTgFnSc4	každý
středu	středa	k1gFnSc4	středa
a	a	k8xC	a
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
(	(	kIx(	(
<g/>
od	od	k7c2	od
konce	konec	k1gInSc2	konec
září	zářit	k5eAaImIp3nS	zářit
2009	[number]	k4	2009
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
nese	nést	k5eAaImIp3nS	nést
podtitul	podtitul	k1gInSc4	podtitul
Šónen-hen	Šónenen	k2eAgInSc4d1	Šónen-hen
(	(	kIx(	(
<g/>
少	少	k?	少
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
překladu	překlad	k1gInSc6	překlad
Chlapecká	chlapecký	k2eAgFnSc1d1	chlapecká
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nové	nový	k2eAgFnPc4d1	nová
úvodní	úvodní	k2eAgFnPc4d1	úvodní
a	a	k8xC	a
závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
znělky	znělka	k1gFnPc4	znělka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
seriál	seriál	k1gInSc4	seriál
vysílal	vysílat	k5eAaImAgInS	vysílat
televizní	televizní	k2eAgInSc1d1	televizní
kanál	kanál	k1gInSc1	kanál
Jetix	Jetix	k1gInSc1	Jetix
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Disney	Disney	k1gInPc4	Disney
Channel	Channlo	k1gNnPc2	Channlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
cenzurované	cenzurovaný	k2eAgFnSc6d1	cenzurovaná
a	a	k8xC	a
česky	česky	k6eAd1	česky
dabované	dabovaný	k2eAgFnSc3d1	dabovaná
verzi	verze	k1gFnSc3	verze
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgFnP	být
vystřiženy	vystřižen	k2eAgFnPc1d1	vystřižena
či	či	k8xC	či
znatelně	znatelně	k6eAd1	znatelně
poupraveny	poupraven	k2eAgFnPc4d1	poupraven
scény	scéna	k1gFnPc4	scéna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
neslušná	slušný	k2eNgFnSc1d1	neslušná
mluva	mluva	k1gFnSc1	mluva
<g/>
,	,	kIx,	,
kouření	kouření	k1gNnSc1	kouření
atd.	atd.	kA	atd.
Odvysíláno	odvysílán	k2eAgNnSc1d1	odvysíláno
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
prvních	první	k4xOgInPc2	první
95	[number]	k4	95
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
Jetixu	Jetix	k1gInSc2	Jetix
další	další	k2eAgInPc1d1	další
díly	díl	k1gInPc1	díl
zdály	zdát	k5eAaImAgInP	zdát
být	být	k5eAaImF	být
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
převzala	převzít	k5eAaPmAgFnS	převzít
práva	práv	k2eAgFnSc1d1	práva
k	k	k7c3	k
vysílání	vysílání	k1gNnSc3	vysílání
seriálu	seriál	k1gInSc2	seriál
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
stanice	stanice	k1gFnSc2	stanice
Animax	Animax	k1gInSc1	Animax
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vysílat	vysílat	k5eAaImF	vysílat
seriál	seriál	k1gInSc4	seriál
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
dabingem	dabing	k1gInSc7	dabing
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
i	i	k9	i
sesterský	sesterský	k2eAgInSc4d1	sesterský
kanál	kanál	k1gInSc4	kanál
Animaxu	Animax	k1gInSc2	Animax
s	s	k7c7	s
názvem	název	k1gInSc7	název
AXN	AXN	kA	AXN
Sci-Fi	scii	k1gNnSc2	sci-fi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Naruto	Narut	k2eAgNnSc4d1	Naruto
<g/>
:	:	kIx,	:
Šippúden	Šippúdno	k1gNnPc2	Šippúdno
===	===	k?	===
</s>
</p>
<p>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
:	:	kIx,	:
Šippúden	Šippúdno	k1gNnPc2	Šippúdno
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
NARUTO	NARUTO	kA	NARUTO
-ナ	-ナ	k?	-ナ
疾	疾	k?	疾
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
:	:	kIx,	:
Kronika	kronika	k1gFnSc1	kronika
hurikánu	hurikán	k1gInSc2	hurikán
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přímým	přímý	k2eAgNnSc7d1	přímé
pokračováním	pokračování	k1gNnSc7	pokračování
původního	původní	k2eAgInSc2d1	původní
seriálu	seriál	k1gInSc2	seriál
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
od	od	k7c2	od
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
osmého	osmý	k4xOgInSc2	osmý
svazku	svazek	k1gInSc2	svazek
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
mangy	mango	k1gNnPc7	mango
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
režisérem	režisér	k1gMnSc7	režisér
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
původního	původní	k2eAgInSc2d1	původní
seriálu	seriál	k1gInSc2	seriál
Hajato	Hajat	k2eAgNnSc4d1	Hajat
Date	Date	k1gNnSc4	Date
a	a	k8xC	a
animačním	animační	k2eAgNnSc7d1	animační
studiem	studio	k1gNnSc7	studio
rovněž	rovněž	k9	rovněž
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
Studio	studio	k1gNnSc1	studio
Pierrot	Pierrot	k1gInSc1	Pierrot
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
odvysílán	odvysílat	k5eAaPmNgInS	odvysílat
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
na	na	k7c6	na
TV	TV	kA	TV
Tokyo	Tokyo	k6eAd1	Tokyo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
začala	začít	k5eAaPmAgFnS	začít
TV	TV	kA	TV
Tokyo	Tokyo	k6eAd1	Tokyo
vysílat	vysílat	k5eAaImF	vysílat
nové	nový	k2eAgInPc4d1	nový
díly	díl	k1gInPc4	díl
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
je	být	k5eAaImIp3nS	být
však	však	k9	však
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
měsíční	měsíční	k2eAgMnPc4d1	měsíční
předplatitele	předplatitel	k1gMnPc4	předplatitel
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
vysílaná	vysílaný	k2eAgFnSc1d1	vysílaná
epizoda	epizoda	k1gFnSc1	epizoda
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
online	onlinout	k5eAaPmIp3nS	onlinout
do	do	k7c2	do
hodiny	hodina	k1gFnSc2	hodina
od	od	k7c2	od
odvysílání	odvysílání	k1gNnSc2	odvysílání
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
anglické	anglický	k2eAgInPc4d1	anglický
titulky	titulek	k1gInPc4	titulek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
pokračování	pokračování	k1gNnSc1	pokračování
dosud	dosud	k6eAd1	dosud
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
žádnou	žádný	k3yNgFnSc7	žádný
televizní	televizní	k2eAgFnSc7d1	televizní
stanicí	stanice	k1gFnSc7	stanice
odvysíláno	odvysílat	k5eAaPmNgNnS	odvysílat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
seriálu	seriál	k1gInSc3	seriál
Naruto	Narut	k2eAgNnSc1d1	Naruto
je	být	k5eAaImIp3nS	být
Tošio	Tošio	k1gNnSc1	Tošio
Masuda	Masudo	k1gNnSc2	Masudo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
CD	CD	kA	CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Naruto	Narut	k2eAgNnSc1d1	Naruto
Original	Original	k1gFnSc4	Original
Soundtrack	soundtrack	k1gInSc4	soundtrack
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2003	[number]	k4	2003
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celkem	celkem	k6eAd1	celkem
22	[number]	k4	22
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
dvě	dva	k4xCgFnPc1	dva
skladby	skladba	k1gFnPc1	skladba
jsou	být	k5eAaImIp3nP	být
úvodními	úvodní	k2eAgFnPc7d1	úvodní
a	a	k8xC	a
závěrečnými	závěrečný	k2eAgFnPc7d1	závěrečná
znělkami	znělka	k1gFnPc7	znělka
první	první	k4xOgFnSc2	první
řady	řada	k1gFnSc2	řada
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
CD	CD	kA	CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Naruto	Narut	k2eAgNnSc1d1	Naruto
Original	Original	k1gFnPc7	Original
Soundtrack	soundtrack	k1gInSc1	soundtrack
II	II	kA	II
a	a	k8xC	a
celkem	celek	k1gInSc7	celek
19	[number]	k4	19
skladbami	skladba	k1gFnPc7	skladba
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
CD	CD	kA	CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Naruto	Narut	k2eAgNnSc1d1	Naruto
Original	Original	k1gFnPc7	Original
Soundtrack	soundtrack	k1gInSc1	soundtrack
III	III	kA	III
a	a	k8xC	a
23	[number]	k4	23
skladbami	skladba	k1gFnPc7	skladba
pak	pak	k6eAd1	pak
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
seriálu	seriál	k1gInSc2	seriál
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
:	:	kIx,	:
Šippúden	Šippúdna	k1gFnPc2	Šippúdna
je	být	k5eAaImIp3nS	být
Jasuharu	Jasuhar	k1gMnSc3	Jasuhar
Takanaši	Takanaš	k1gMnSc3	Takanaš
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
CD	CD	kA	CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
:	:	kIx,	:
Šippúden	Šippúdno	k1gNnPc2	Šippúdno
Original	Original	k1gFnSc4	Original
Soundtrack	soundtrack	k1gInSc4	soundtrack
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celkem	celkem	k6eAd1	celkem
28	[number]	k4	28
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
CD	CD	kA	CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
:	:	kIx,	:
Šippúden	Šippúdna	k1gFnPc2	Šippúdna
Original	Original	k1gFnSc2	Original
Soundtrack	soundtrack	k1gInSc1	soundtrack
II	II	kA	II
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
28	[number]	k4	28
skladbami	skladba	k1gFnPc7	skladba
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hry	hra	k1gFnPc1	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Obrovská	obrovský	k2eAgFnSc1d1	obrovská
popularita	popularita	k1gFnSc1	popularita
mangy	mango	k1gNnPc7	mango
i	i	k8xC	i
anime	animat	k5eAaPmIp3nS	animat
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
další	další	k2eAgFnSc2d1	další
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
velké	velký	k2eAgFnPc1d1	velká
řady	řada	k1gFnPc1	řada
herních	herní	k2eAgFnPc2d1	herní
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nedostupná	dostupný	k2eNgFnSc1d1	nedostupná
a	a	k8xC	a
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
titulů	titul	k1gInPc2	titul
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pro	pro	k7c4	pro
herní	herní	k2eAgFnSc4d1	herní
konzole	konzola	k1gFnSc3	konzola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
osobních	osobní	k2eAgInPc6d1	osobní
počítačích	počítač	k1gInPc6	počítač
vznikají	vznikat	k5eAaImIp3nP	vznikat
převážně	převážně	k6eAd1	převážně
browserové	browserový	k2eAgFnPc1d1	browserová
hry	hra	k1gFnPc1	hra
a	a	k8xC	a
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
bojové	bojový	k2eAgFnPc1d1	bojová
hry	hra	k1gFnPc1	hra
na	na	k7c6	na
enginu	engino	k1gNnSc6	engino
MUGEN	MUGEN	kA	MUGEN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Naruto	Narut	k2eAgNnSc4d1	Naruto
<g/>
:	:	kIx,	:
Ultimate	Ultimat	k1gInSc5	Ultimat
Ninja	Ninj	k2eAgFnSc1d1	Ninja
Storm	Storm	k1gInSc1	Storm
===	===	k?	===
</s>
</p>
<p>
<s>
Série	série	k1gFnSc1	série
vydávaná	vydávaný	k2eAgFnSc1d1	vydávaná
na	na	k7c4	na
PlayStation	PlayStation	k1gInSc4	PlayStation
3	[number]	k4	3
<g/>
,	,	kIx,	,
Xbox	Xbox	k1gInSc1	Xbox
360	[number]	k4	360
a	a	k8xC	a
od	od	k7c2	od
dílu	díl	k1gInSc2	díl
Ninja	Ninjum	k1gNnSc2	Ninjum
Storm	Storm	k1gInSc4	Storm
3	[number]	k4	3
i	i	k8xC	i
na	na	k7c4	na
PC	PC	kA	PC
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
volně	volně	k6eAd1	volně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
herní	herní	k2eAgFnSc6d1	herní
sérii	série	k1gFnSc6	série
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
:	:	kIx,	:
Ultimate	Ultimat	k1gInSc5	Ultimat
Ninja	Ninja	k1gMnSc1	Ninja
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
šest	šest	k4xCc4	šest
dílů	díl	k1gInPc2	díl
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
:	:	kIx,	:
Ultimate	Ultimat	k1gInSc5	Ultimat
Ninja	Ninj	k1gInSc2	Ninj
Storm	Storm	k1gInSc1	Storm
</s>
</p>
<p>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
Shippuden	Shippudno	k1gNnPc2	Shippudno
<g/>
:	:	kIx,	:
Ultimate	Ultimat	k1gInSc5	Ultimat
Ninja	Ninjus	k1gMnSc4	Ninjus
Storm	Storm	k1gInSc4	Storm
2	[number]	k4	2
</s>
</p>
<p>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
Shippuden	Shippudno	k1gNnPc2	Shippudno
<g/>
:	:	kIx,	:
Ultimate	Ultimat	k1gInSc5	Ultimat
Ninja	Ninj	k1gInSc2	Ninj
Storm	Storm	k1gInSc4	Storm
Generations	Generations	k1gInSc1	Generations
</s>
</p>
<p>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
Shippuden	Shippudno	k1gNnPc2	Shippudno
<g/>
:	:	kIx,	:
Ultimate	Ultimat	k1gInSc5	Ultimat
Ninja	Ninjus	k1gMnSc4	Ninjus
Storm	Storm	k1gInSc4	Storm
3	[number]	k4	3
</s>
</p>
<p>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
Shippuden	Shippudno	k1gNnPc2	Shippudno
<g/>
:	:	kIx,	:
Ultimate	Ultimat	k1gInSc5	Ultimat
Ninja	Ninj	k1gInSc2	Ninj
Storm	Storm	k1gInSc4	Storm
Revolution	Revolution	k1gInSc1	Revolution
</s>
</p>
<p>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
Shippuden	Shippudno	k1gNnPc2	Shippudno
<g/>
:	:	kIx,	:
Ultimate	Ultimat	k1gInSc5	Ultimat
Ninja	Ninjus	k1gMnSc4	Ninjus
Storm	Storm	k1gInSc4	Storm
4	[number]	k4	4
<g/>
Základní	základní	k2eAgFnSc1d1	základní
hratelnost	hratelnost	k1gFnSc1	hratelnost
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
titulů	titul	k1gInPc2	titul
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
bojovým	bojový	k2eAgFnPc3d1	bojová
hrám	hra	k1gFnPc3	hra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
arénách	aréna	k1gFnPc6	aréna
utkávají	utkávat	k5eAaImIp3nP	utkávat
charaktery	charakter	k1gInPc1	charakter
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
protivníka	protivník	k1gMnSc2	protivník
porazit	porazit	k5eAaPmF	porazit
pomocí	pomocí	k7c2	pomocí
úderů	úder	k1gInPc2	úder
a	a	k8xC	a
komb	komba	k1gFnPc2	komba
<g/>
.	.	kIx.	.
</s>
<s>
Ultimate	Ultimat	k1gInSc5	Ultimat
Ninja	Ninjus	k1gMnSc4	Ninjus
Storm	Storm	k1gInSc4	Storm
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
sbírá	sbírat	k5eAaImIp3nS	sbírat
velmi	velmi	k6eAd1	velmi
kladná	kladný	k2eAgNnPc4d1	kladné
hodnocení	hodnocení	k1gNnPc4	hodnocení
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
grafickou	grafický	k2eAgFnSc4d1	grafická
vytříbenost	vytříbenost	k1gFnSc4	vytříbenost
a	a	k8xC	a
rozlehlé	rozlehlý	k2eAgFnPc4d1	rozlehlá
arény	aréna	k1gFnPc4	aréna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
plně	plně	k6eAd1	plně
3D	[number]	k4	3D
a	a	k8xC	a
značně	značně	k6eAd1	značně
interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
taktéž	taktéž	k?	taktéž
dějově	dějově	k6eAd1	dějově
sledují	sledovat	k5eAaImIp3nP	sledovat
určitý	určitý	k2eAgInSc4d1	určitý
úsek	úsek	k1gInSc4	úsek
mangy	mango	k1gNnPc7	mango
a	a	k8xC	a
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
unikátního	unikátní	k2eAgInSc2d1	unikátní
animovaného	animovaný	k2eAgInSc2d1	animovaný
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Masaši	Masasat	k5eAaPmIp1nSwK	Masasat
Kišimoto	Kišimota	k1gFnSc5	Kišimota
</s>
</p>
<p>
<s>
Svět	svět	k1gInSc1	svět
Naruta	Narut	k1gMnSc2	Narut
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
postav	postav	k1gInSc1	postav
v	v	k7c6	v
Narutovi	Narut	k1gMnSc6	Narut
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Naruto	Narut	k2eAgNnSc1d1	Naruto
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Manga	mango	k1gNnPc1	mango
Naruto	Narut	k2eAgNnSc4d1	Naruto
na	na	k7c4	na
AnimeNewsNetwork	AnimeNewsNetwork	k1gInSc4	AnimeNewsNetwork
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
českých	český	k2eAgMnPc2d1	český
fanoušků	fanoušek	k1gMnPc2	fanoušek
a	a	k8xC	a
překladatelů	překladatel	k1gMnPc2	překladatel
Naruta	Narut	k1gMnSc2	Narut
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
českých	český	k2eAgMnPc2d1	český
fanoušků	fanoušek	k1gMnPc2	fanoušek
a	a	k8xC	a
překladatelů	překladatel	k1gMnPc2	překladatel
Naruta	Narut	k1gMnSc2	Narut
2	[number]	k4	2
</s>
</p>
<p>
<s>
Naruto	Narut	k2eAgNnSc1d1	Naruto
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
