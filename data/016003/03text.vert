<s>
Bamberk	Bamberk	k1gInSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Bamberg	Bamberg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
městě	město	k1gNnSc6
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Bamberg	Bamberg	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Bamberk	Bamberk	k1gInSc1
Stará	starý	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
10	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
262	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc4
Spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1
Vládní	vládní	k2eAgNnSc1d1
obvod	obvod	k1gInSc4
</s>
<s>
Horní	horní	k2eAgInPc1d1
Franky	Franky	k1gInPc1
</s>
<s>
Bamberk	Bamberk	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
54,6	54,6	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
77	#num#	k4
592	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1	#num#	k4
420,6	420,6	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Andreas	Andreas	k1gInSc1
Starke	Stark	k1gFnSc2
(	(	kIx(
<g/>
SPD	SPD	kA
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.stadt.bamberg.de	www.stadt.bamberg.de	k6eAd1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
0951	#num#	k4
PSČ	PSČ	kA
</s>
<s>
96001	#num#	k4
a	a	k8xC
96052	#num#	k4
Označení	označení	k1gNnPc2
vozidel	vozidlo	k1gNnPc2
</s>
<s>
BA	ba	k9
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bamberk	Bamberk	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Bamberg	Bamberg	k1gInSc1
<g/>
,	,	kIx,
staroněmecky	staroněmecky	k6eAd1
Babenberg	Babenberg	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
starobylé	starobylý	k2eAgNnSc1d1
město	město	k1gNnSc1
ležící	ležící	k2eAgFnSc1d1
v	v	k7c6
úrodném	úrodný	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
rameny	rameno	k1gNnPc7
řeky	řeka	k1gFnSc2
Regnitz	Regnitza	k1gFnPc2
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
První	první	k4xOgFnPc1
zmínky	zmínka	k1gFnPc1
o	o	k7c6
Bamberku	Bamberk	k1gInSc6
jako	jako	k8xC,k8xS
o	o	k7c6
hradu	hrad	k1gInSc6
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
rodu	rod	k1gInSc2
Babenberků	Babenberka	k1gMnPc2
pocházejí	pocházet	k5eAaImIp3nP
již	již	k6eAd1
z	z	k7c2
roku	rok	k1gInSc2
902	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
starší	starý	k2eAgMnPc1d2
<g/>
,	,	kIx,
snad	snad	k9
již	již	k6eAd1
keltské	keltský	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
(	(	kIx(
<g/>
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Altenburg	Altenburg	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
stěhování	stěhování	k1gNnSc2
národů	národ	k1gInPc2
odsud	odsud	k6eAd1
Slovani	Slovan	k1gMnPc1
vytlačili	vytlačit	k5eAaPmAgMnP
Germány	Germán	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkou	památka	k1gFnSc7
na	na	k7c4
ně	on	k3xPp3gNnSc4
jsou	být	k5eAaImIp3nP
zbytky	zbytek	k1gInPc1
primitivních	primitivní	k2eAgFnPc2d1
soch	socha	k1gFnPc2
kamenných	kamenný	k2eAgNnPc2d1
božstev	božstvo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
nalezeny	naleznout	k5eAaPmNgInP,k5eAaBmNgInP
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
však	však	k9
byla	být	k5eAaImAgFnS
zdejší	zdejší	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
znovu	znovu	k6eAd1
osídlena	osídlit	k5eAaPmNgFnS
germánským	germánský	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
903	#num#	k4
Babenberkové	Babenberkové	k2eAgMnSc2d1
v	v	k7c6
boji	boj	k1gInSc6
s	s	k7c7
franskými	franský	k2eAgInPc7d1
Konrádovci	Konrádovec	k1gInPc7
o	o	k7c4
Bamberk	Bamberk	k1gInSc4
přišli	přijít	k5eAaPmAgMnP
a	a	k8xC
město	město	k1gNnSc4
později	pozdě	k6eAd2
připadlo	připadnout	k5eAaPmAgNnS
královské	královský	k2eAgFnSc3d1
koruně	koruna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
973	#num#	k4
udělil	udělit	k5eAaPmAgMnS
císař	císař	k1gMnSc1
Ota	Ota	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bamberk	Bamberk	k1gInSc4
svému	svůj	k3xOyFgMnSc3
bratranci	bratranec	k1gMnSc3
<g/>
,	,	kIx,
bavorskému	bavorský	k2eAgMnSc3d1
vévodovi	vévoda	k1gMnSc3
Jindřichu	Jindřich	k1gMnSc3
Svárlivému	svárlivý	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
římský	římský	k2eAgMnSc1d1
král	král	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
císař	císař	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
založil	založit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1007	#num#	k4
v	v	k7c6
Bamberku	Bamberk	k1gInSc6
biskupství	biskupství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
zde	zde	k6eAd1
začal	začít	k5eAaPmAgInS
stavět	stavět	k5eAaImF
císařskou	císařský	k2eAgFnSc4d1
falc	falc	k1gFnSc4
a	a	k8xC
honosný	honosný	k2eAgInSc4d1
Dóm	dóm	k1gInSc4
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
Bamberk	Bamberk	k1gInSc1
zvolil	zvolit	k5eAaPmAgInS
za	za	k7c4
své	svůj	k3xOyFgNnSc4
sídelní	sídelní	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biskupství	biskupství	k1gNnSc1
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
podřízeno	podřídit	k5eAaPmNgNnS
přímo	přímo	k6eAd1
papeži	papež	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
biskupové	biskup	k1gMnPc1
stali	stát	k5eAaPmAgMnP
pány	pan	k1gMnPc7
města	město	k1gNnSc2
a	a	k8xC
nic	nic	k3yNnSc1
na	na	k7c6
tom	ten	k3xDgNnSc6
nezměnila	změnit	k5eNaPmAgFnS
ani	ani	k8xC
měšťanská	měšťanský	k2eAgNnPc1d1
povstání	povstání	k1gNnPc1
(	(	kIx(
<g/>
největší	veliký	k2eAgFnSc1d3
proběhlo	proběhnout	k5eAaPmAgNnS
roku	rok	k1gInSc2
1435	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
město	město	k1gNnSc1
svobodu	svoboda	k1gFnSc4
nezískalo	získat	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
úspěšně	úspěšně	k6eAd1
rozvíjel	rozvíjet	k5eAaImAgInS
dálkový	dálkový	k2eAgInSc4d1
obchod	obchod	k1gInSc4
<g/>
,	,	kIx,
Bamberští	bamberský	k2eAgMnPc1d1
měli	mít	k5eAaImAgMnP
obchodní	obchodní	k2eAgInPc4d1
kontakty	kontakt	k1gInPc4
v	v	k7c6
Porýní	Porýní	k1gNnSc6
i	i	k8xC
v	v	k7c6
Čechách	Čechy	k1gFnPc6
(	(	kIx(
<g/>
z	z	k7c2
Norimberka	Norimberk	k1gInSc2
přes	přes	k7c4
Bamberk	Bamberk	k1gInSc4
do	do	k7c2
Tachova	Tachov	k1gInSc2
vedla	vést	k5eAaImAgFnS
Zlatá	zlatý	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
je	být	k5eAaImIp3nS
však	však	k9
o	o	k7c4
jejich	jejich	k3xOp3gNnSc4
postavení	postavení	k1gNnSc4
v	v	k7c6
tranzitu	tranzit	k1gInSc6
připravil	připravit	k5eAaPmAgInS
Norimberk	Norimberk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Biskupská	biskupský	k2eAgFnSc1d1
moc	moc	k1gFnSc1
byla	být	k5eAaImAgFnS
oslabena	oslabit	k5eAaPmNgFnS
v	v	k7c6
době	doba	k1gFnSc6
nástupu	nástup	k1gInSc2
reformace	reformace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
město	město	k1gNnSc1
zůstalo	zůstat	k5eAaPmAgNnS
na	na	k7c6
straně	strana	k1gFnSc6
Katolické	katolický	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
v	v	k7c6
letech	let	k1gInPc6
1631	#num#	k4
<g/>
–	–	k?
<g/>
1648	#num#	k4
obsazeno	obsadit	k5eAaPmNgNnS
švédskými	švédský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
biskupství	biskupství	k1gNnSc4
dočasně	dočasně	k6eAd1
zaniklo	zaniknout	k5eAaPmAgNnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těsně	těsně	k6eAd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
mezi	mezi	k7c7
léty	léto	k1gNnPc7
1625-1631	1625-1631	k4
ve	v	k7c6
městě	město	k1gNnSc6
a	a	k8xC
jeho	jeho	k3xOp3gNnSc6
okolí	okolí	k1gNnSc6
proběhla	proběhnout	k5eAaPmAgFnS
série	série	k1gFnSc1
brutálních	brutální	k2eAgInPc2d1
čarodějnických	čarodějnický	k2eAgInPc2d1
procesů	proces	k1gInPc2
<g/>
,	,	kIx,
za	za	k7c4
něž	jenž	k3xRgMnPc4
nesl	nést	k5eAaImAgMnS
zodpovědnost	zodpovědnost	k1gFnSc4
bamberský	bamberský	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
Jan	Jan	k1gMnSc1
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Dornheimu	Dornheim	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesný	přesný	k2eAgInSc1d1
počet	počet	k1gInSc1
obětí	oběť	k1gFnPc2
není	být	k5eNaImIp3nS
znám	znám	k2eAgMnSc1d1
<g/>
,	,	kIx,
podle	podle	k7c2
odhadů	odhad	k1gInPc2
a	a	k8xC
neúplných	úplný	k2eNgInPc2d1
dokumentů	dokument	k1gInPc2
mohlo	moct	k5eAaImAgNnS
jít	jít	k5eAaImF
přibližně	přibližně	k6eAd1
o	o	k7c4
tisícovku	tisícovka	k1gFnSc4
popravených	popravený	k2eAgFnPc2d1
či	či	k8xC
umučených	umučený	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
nastala	nastat	k5eAaPmAgFnS
pro	pro	k7c4
Bamberk	Bamberk	k1gInSc4
doba	doba	k1gFnSc1
vrcholného	vrcholný	k2eAgInSc2d1
kulturního	kulturní	k2eAgInSc2d1
rozkvětu	rozkvět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
vedením	vedení	k1gNnSc7
biskupů	biskup	k1gMnPc2
z	z	k7c2
rodu	rod	k1gInSc2
Schönbornů	Schönborn	k1gMnPc2
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
k	k	k7c3
rozsáhlé	rozsáhlý	k2eAgFnSc3d1
výstavbě	výstavba	k1gFnSc3
ve	v	k7c6
stylu	styl	k1gInSc6
baroka	baroko	k1gNnSc2
a	a	k8xC
barokizaci	barokizace	k1gFnSc4
interiérů	interiér	k1gInPc2
středověkých	středověký	k2eAgInPc2d1
kostelů	kostel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
stavitelé	stavitel	k1gMnPc1
působili	působit	k5eAaImAgMnP
tehdy	tehdy	k6eAd1
v	v	k7c6
biskupských	biskupský	k2eAgFnPc6d1
službách	služba	k1gFnPc6
čtyři	čtyři	k4xCgMnPc1
bratři	bratr	k1gMnPc1
Dientzenhoferové	Dientzenhoferové	k2eAgMnPc1d1
(	(	kIx(
<g/>
pátý	pátý	k4xOgMnSc1
Christoph	Christoph	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
trvale	trvale	k6eAd1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
bratři	bratr	k1gMnPc1
vypravili	vypravit	k5eAaPmAgMnP
na	na	k7c4
zkušenou	zkušená	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1801	#num#	k4
připadl	připadnout	k5eAaPmAgInS
Bamberk	Bamberk	k1gInSc1
po	po	k7c4
ujednání	ujednání	k1gNnSc4
francouzsko-rakouského	francouzsko-rakouský	k2eAgInSc2d1
míru	mír	k1gInSc2
v	v	k7c6
Lunéville	Lunévilla	k1gFnSc6
Bavorsku	Bavorsko	k1gNnSc6
jako	jako	k8xS,k8xC
kompenzace	kompenzace	k1gFnSc1
za	za	k7c4
ztrátu	ztráta	k1gFnSc4
Rýnské	rýnský	k2eAgFnSc2d1
Falce	Falc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1802	#num#	k4
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
obsazeno	obsadit	k5eAaPmNgNnS
vojáky	voják	k1gMnPc7
a	a	k8xC
biskup	biskup	k1gMnSc1
se	se	k3xPyFc4
podřídil	podřídit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
úřad	úřad	k1gInSc1
byl	být	k5eAaImAgInS
dočasně	dočasně	k6eAd1
zrušen	zrušit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začlenění	začlenění	k1gNnSc1
bamberského	bamberský	k2eAgNnSc2d1
území	území	k1gNnSc2
do	do	k7c2
Bavorského	bavorský	k2eAgNnSc2d1
království	království	k1gNnSc2
potvrdil	potvrdit	k5eAaPmAgInS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
jednání	jednání	k1gNnSc6
Vídeňský	vídeňský	k2eAgInSc4d1
kongres	kongres	k1gInSc4
v	v	k7c6
letech	let	k1gInPc6
1814	#num#	k4
<g/>
–	–	k?
<g/>
1815	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1817	#num#	k4
bylo	být	k5eAaImAgNnS
bamberské	bamberský	k2eAgNnSc1d1
biskupství	biskupství	k1gNnSc1
povýšeno	povýšen	k2eAgNnSc1d1
na	na	k7c6
arcibiskupství	arcibiskupství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
nové	nový	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
byla	být	k5eAaImAgNnP
začleněna	začleněn	k2eAgNnPc1d1
biskupství	biskupství	k1gNnPc1
ve	v	k7c6
Würzburgu	Würzburg	k1gInSc6
<g/>
,	,	kIx,
Špýru	Špýr	k1gInSc6
a	a	k8xC
Eichstättu	Eichstätt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Dóm	dóm	k1gInSc1
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Bamberk	Bamberk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
bývá	bývat	k5eAaImIp3nS
taky	taky	k6eAd1
nazýván	nazývat	k5eAaImNgInS
Francký	francký	k2eAgInSc1d1
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c6
sedmi	sedm	k4xCc6
pahorcích	pahorek	k1gInPc6
a	a	k8xC
v	v	k7c6
údolí	údolí	k1gNnSc6
mezi	mezi	k7c7
rameny	rameno	k1gNnPc7
řeky	řeka	k1gFnSc2
Regnitz	Regnitza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půvabným	půvabný	k2eAgInPc3d1
domkům	domek	k1gInPc3
na	na	k7c6
březích	břeh	k1gInPc6
řeky	řeka	k1gFnSc2
<g/>
,	,	kIx,
kdysi	kdysi	k6eAd1
obývaným	obývaný	k2eAgInSc7d1
rybáři	rybář	k1gMnPc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
„	„	k?
<g/>
Malé	Malé	k2eAgFnPc1d1
Benátky	Benátky	k1gFnPc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Klein	Klein	k1gMnSc1
Venedig	Venedig	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gFnSc6
historické	historický	k2eAgFnSc6d1
měšťanské	měšťanský	k2eAgFnSc6d1
části	část	k1gFnSc6
(	(	kIx(
<g/>
Bürgerstadt	Bürgerstadt	k2eAgMnSc1d1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
domy	dům	k1gInPc1
natěsnány	natěsnán	k2eAgInPc1d1
na	na	k7c4
sebe	sebe	k3xPyFc4
a	a	k8xC
město	město	k1gNnSc1
je	být	k5eAaImIp3nS
protkáno	protkán	k2eAgNnSc1d1
sítí	sítí	k1gNnSc1
křivolakých	křivolaký	k2eAgFnPc2d1
uliček	ulička	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bamberg	Bamberg	k1gInSc1
má	mít	k5eAaImIp3nS
i	i	k9
světovou	světový	k2eAgFnSc4d1
raritu	rarita	k1gFnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
původně	původně	k6eAd1
gotické	gotický	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
radnice	radnice	k1gFnSc2
<g/>
,	,	kIx,
stojící	stojící	k2eAgFnSc6d1
na	na	k7c6
mostě	most	k1gInSc6
uprostřed	uprostřed	k7c2
řeky	řeka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radnice	radnice	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
polovině	polovina	k1gFnSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přestavěna	přestavět	k5eAaPmNgFnS
v	v	k7c6
barokním	barokní	k2eAgInSc6d1
stylu	styl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	nedaleko	k7c2
Staré	Staré	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
stojí	stát	k5eAaImIp3nS
prstencová	prstencový	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
zámku	zámek	k1gInSc2
Geyerswörth	Geyerswörth	k1gInSc4
s	s	k7c7
věží	věž	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Dominantou	dominanta	k1gFnSc7
biskupské	biskupský	k2eAgFnSc2d1
části	část	k1gFnSc2
města	město	k1gNnSc2
(	(	kIx(
<g/>
Bischofsstadt	Bischofsstadt	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
Dóm	dóm	k1gInSc4
postavený	postavený	k2eAgInSc4d1
v	v	k7c6
románsko-gotickém	románsko-gotický	k2eAgInSc6d1
slohu	sloh	k1gInSc6
(	(	kIx(
<g/>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
třetí	třetí	k4xOgInSc4
chrám	chrám	k1gInSc4
z	z	k7c2
let	léto	k1gNnPc2
1211	#num#	k4
<g/>
–	–	k?
<g/>
1237	#num#	k4
<g/>
,	,	kIx,
předchozí	předchozí	k2eAgInPc4d1
dva	dva	k4xCgInPc4
shořely	shořet	k5eAaPmAgFnP
<g/>
)	)	kIx)
se	s	k7c7
čtyřmi	čtyři	k4xCgFnPc7
štíhlými	štíhlý	k2eAgFnPc7d1
věžemi	věž	k1gFnPc7
a	a	k8xC
interiérem	interiér	k1gInSc7
s	s	k7c7
překrásnou	překrásný	k2eAgFnSc7d1
gotickou	gotický	k2eAgFnSc7d1
výzdobou	výzdoba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světoznámá	světoznámý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
jezdecká	jezdecký	k2eAgFnSc1d1
socha	socha	k1gFnSc1
v	v	k7c6
životní	životní	k2eAgFnSc6d1
velikosti	velikost	k1gFnSc6
z	z	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
Bamberger	Bamberger	k1gMnSc1
Reiter	Reiter	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
představuje	představovat	k5eAaImIp3nS
neznámého	známý	k2eNgMnSc4d1
muže	muž	k1gMnSc4
<g/>
,	,	kIx,
ideál	ideál	k1gInSc1
středověkého	středověký	k2eAgMnSc2d1
rytíře	rytíř	k1gMnSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Soudí	soudit	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
jít	jít	k5eAaImF
o	o	k7c4
uherského	uherský	k2eAgMnSc4d1
krále	král	k1gMnSc4
sv.	sv.	kA
Štěpána	Štěpán	k1gMnSc4
I.	I.	kA
<g/>
,	,	kIx,
švagra	švagr	k1gMnSc2
císaře	císař	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domněnky	domněnka	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
jít	jít	k5eAaImF
o	o	k7c4
samotného	samotný	k2eAgMnSc4d1
Jindřicha	Jindřich	k1gMnSc4
II	II	kA
<g/>
.	.	kIx.
nebo	nebo	k8xC
tehdy	tehdy	k6eAd1
vládnoucího	vládnoucí	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štaufského	Štaufský	k2eAgInSc2d1
vyvrací	vyvracet	k5eAaImIp3nS
ta	ten	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
jezdec	jezdec	k1gMnSc1
sedí	sedit	k5eAaImIp3nS
na	na	k7c6
ryzáku	ryzák	k1gMnSc6
/	/	kIx~
<g/>
zbytky	zbytek	k1gInPc1
inkrustace	inkrustace	k1gFnSc2
<g/>
/	/	kIx~
a	a	k8xC
císař	císař	k1gMnSc1
jezdil	jezdit	k5eAaImAgMnS
vždy	vždy	k6eAd1
na	na	k7c6
bělouši	bělouš	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
i	i	k9
koruna	koruna	k1gFnSc1
není	být	k5eNaImIp3nS
císařská	císařský	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
královská	královský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
to	ten	k3xDgNnSc1
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
osoba	osoba	k1gFnSc1
svatořečená	svatořečený	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
kostele	kostel	k1gInSc6
smí	smět	k5eAaImIp3nS
být	být	k5eAaImF
zobrazen	zobrazit	k5eAaPmNgMnS
pouze	pouze	k6eAd1
nějaký	nějaký	k3yIgMnSc1
svatý	svatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
náhrobek	náhrobek	k1gInSc4
zemřelého	zemřelý	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc4
případ	případ	k1gInSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Interiér	interiér	k1gInSc1
katedrály	katedrála	k1gFnSc2
je	být	k5eAaImIp3nS
pozoruhodný	pozoruhodný	k2eAgInSc1d1
také	také	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
dva	dva	k4xCgInPc4
chóry	chór	k1gInPc4
s	s	k7c7
oltáři	oltář	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgInSc4d1
je	být	k5eAaImIp3nS
zasvěcený	zasvěcený	k2eAgInSc4d1
sv.	sv.	kA
Petrovi	Petr	k1gMnSc3
a	a	k8xC
jsou	být	k5eAaImIp3nP
v	v	k7c6
něm	on	k3xPp3gNnSc6
uloženy	uložit	k5eAaPmNgInP
ostatky	ostatek	k1gInPc7
papeže	papež	k1gMnSc2
Klementa	Klement	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jediný	jediný	k2eAgInSc4d1
papežský	papežský	k2eAgInSc4d1
hrob	hrob	k1gInSc4
severně	severně	k6eAd1
od	od	k7c2
Alp	Alpy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
východní	východní	k2eAgInSc1d1
chór	chór	k1gInSc1
s	s	k7c7
oltářem	oltář	k1gInSc7
sv.	sv.	kA
Jiří	Jiří	k1gMnSc1
a	a	k8xC
před	před	k7c7
chórem	chór	k1gInSc7
hrobka	hrobka	k1gFnSc1
s	s	k7c7
ostatky	ostatek	k1gInPc7
jediného	jediný	k2eAgInSc2d1
svatořečeného	svatořečený	k2eAgInSc2d1
panovnického	panovnický	k2eAgInSc2d1
páru	pár	k1gInSc2
<g/>
,	,	kIx,
císaře	císař	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
manželky	manželka	k1gFnPc1
Kunhuty	Kunhuta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
východní	východní	k2eAgFnSc6d1
kryptě	krypta	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
sarkofág	sarkofág	k1gInSc1
s	s	k7c7
rakví	rakev	k1gFnSc7
dalšího	další	k2eAgMnSc2d1
německého	německý	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
,	,	kIx,
Konráda	Konrád	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
v	v	k7c6
Bamberku	Bamberk	k1gInSc6
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
náměstí	náměstí	k1gNnSc6
před	před	k7c7
dómem	dóm	k1gInSc7
naproti	naproti	k7c3
Starému	starý	k2eAgInSc3d1
knížecímu	knížecí	k2eAgInSc3d1
dvoru	dvůr	k1gInSc3
(	(	kIx(
<g/>
Alte	alt	k1gInSc5
Hofhaltung	Hofhaltunga	k1gFnPc2
<g/>
)	)	kIx)
ze	z	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
nechal	nechat	k5eAaPmAgMnS
kníže-biskup	kníže-biskup	k1gMnSc1
Lothar	Lothar	k1gMnSc1
Franz	Franza	k1gFnPc2
von	von	k1gInSc1
Schönborn	Schönborn	k1gMnSc1
na	na	k7c6
přelomu	přelom	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
postavit	postavit	k5eAaPmF
skvostnou	skvostný	k2eAgFnSc4d1
barokní	barokní	k2eAgFnSc4d1
Novou	nový	k2eAgFnSc4d1
rezidenci	rezidence	k1gFnSc4
(	(	kIx(
<g/>
Neue	Neue	k1gFnSc1
Residenz	Residenz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reprezentativních	reprezentativní	k2eAgFnPc6d1
prostorách	prostora	k1gFnPc6
Nové	Nové	k2eAgFnSc2d1
rezidence	rezidence	k1gFnSc2
má	mít	k5eAaImIp3nS
dnes	dnes	k6eAd1
sídlo	sídlo	k1gNnSc4
bamberská	bamberský	k2eAgFnSc1d1
pobočka	pobočka	k1gFnSc1
státní	státní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
zde	zde	k6eAd1
návštěvníci	návštěvník	k1gMnPc1
mohou	moct	k5eAaImIp3nP
obdivovat	obdivovat	k5eAaImF
díla	dílo	k1gNnPc4
starých	starý	k2eAgMnPc2d1
mistrů	mistr	k1gMnPc2
i	i	k8xC
zámecké	zámecký	k2eAgInPc4d1
sály	sál	k1gInPc4
vyzdobené	vyzdobený	k2eAgInPc4d1
překrásnými	překrásný	k2eAgInPc7d1
štuky	štuk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1647	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Bamberku	Bamberk	k1gInSc6
založena	založen	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1803	#num#	k4
zrušena	zrušen	k2eAgFnSc1d1
a	a	k8xC
později	pozdě	k6eAd2
znovu	znovu	k6eAd1
obnovena	obnoven	k2eAgFnSc1d1
–	–	k?
dnes	dnes	k6eAd1
Otto-Friedrichova	Otto-Friedrichův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Bamberku	Bamberk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
Christopher	Christophra	k1gFnPc2
Clavius	Clavius	k1gMnSc1
(	(	kIx(
<g/>
1537	#num#	k4
<g/>
/	/	kIx~
<g/>
1538	#num#	k4
–	–	k?
1612	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
astronom	astronom	k1gMnSc1
a	a	k8xC
jesuita	jesuita	k1gMnSc1
</s>
<s>
Johannes	Johannes	k1gMnSc1
Junius	Junius	k1gMnSc1
(	(	kIx(
<g/>
1573	#num#	k4
-	-	kIx~
1628	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
purkmistr	purkmistr	k1gMnSc1
v	v	k7c6
Bambergu	Bamberg	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgInS
dopisem	dopis	k1gInSc7
z	z	k7c2
vězeňské	vězeňský	k2eAgFnSc2d1
cely	cela	k1gFnSc2
napsaným	napsaný	k2eAgInPc3d1
když	když	k8xS
očekával	očekávat	k5eAaImAgInS
popravu	poprava	k1gFnSc4
za	za	k7c4
čarodějnictví	čarodějnictví	k1gNnSc4
</s>
<s>
Georg	Georg	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Hegel	Hegel	k1gMnSc1
(	(	kIx(
<g/>
1770	#num#	k4
–	–	k?
1831	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
filosof	filosof	k1gMnSc1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1808	#num#	k4
–	–	k?
1888	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bavorský	bavorský	k2eAgMnSc1d1
vojvoda	vojvoda	k1gMnSc1
<g/>
,	,	kIx,
mecenáš	mecenáš	k1gMnSc1
bavorské	bavorský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
</s>
<s>
Ota	Ota	k1gMnSc1
I.	I.	kA
Řecký	řecký	k2eAgMnSc1d1
(	(	kIx(
<g/>
1815	#num#	k4
–	–	k?
1867	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bavorský	bavorský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
moderní	moderní	k2eAgMnSc1d1
král	král	k1gMnSc1
Řeckého	řecký	k2eAgNnSc2d1
království	království	k1gNnSc2
</s>
<s>
Wilhelm	Wilhelm	k1gMnSc1
Emil	Emil	k1gMnSc1
Messerschmitt	Messerschmitt	k1gMnSc1
(	(	kIx(
<g/>
1898	#num#	k4
–	–	k?
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
letecký	letecký	k2eAgMnSc1d1
konstruktér	konstruktér	k1gMnSc1
a	a	k8xC
průmyslník	průmyslník	k1gMnSc1
</s>
<s>
Annette	Annette	k5eAaPmIp2nP
von	von	k1gInSc4
Aretin	Aretin	k1gInSc1
(	(	kIx(
<g/>
1920	#num#	k4
–	–	k?
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
bavorská	bavorský	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
hlasatelka	hlasatelka	k1gFnSc1
</s>
<s>
Karlheinz	Karlheinz	k1gMnSc1
Deschner	Deschner	k1gMnSc1
(	(	kIx(
<g/>
1924	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
kritik	kritik	k1gMnSc1
náboženství	náboženství	k1gNnSc2
</s>
<s>
Hans	Hans	k1gMnSc1
Wollschläger	Wollschläger	k1gMnSc1
(	(	kIx(
<g/>
1935	#num#	k4
–	–	k?
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
překladatel	překladatel	k1gMnSc1
<g/>
,	,	kIx,
historik	historik	k1gMnSc1
a	a	k8xC
filolog	filolog	k1gMnSc1
</s>
<s>
Ulrich	Ulrich	k1gMnSc1
Beck	Beck	k1gMnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sociolog	sociolog	k1gMnSc1
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Bedford	Bedford	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Feldkirchen	Feldkirchen	k2eAgInSc1d1
in	in	k?
Kärnten	Kärntno	k1gNnPc2
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Nagaoka	Nagaoka	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Ostřihom	Ostřihom	k1gInSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Rodez	Rodez	k1gInSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Villach	Villach	k1gInSc1
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Dóm	dóm	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Michaela	Michaela	k1gFnSc1
a	a	k8xC
bývalé	bývalý	k2eAgNnSc1d1
benediktinské	benediktinský	k2eAgNnSc1d1
opatství	opatství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Martina	Martin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
rezidence	rezidence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1
Benátky	Benátky	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.zlatacesta.cz/historicka-trasa-zlata-cesta.html	http://www.zlatacesta.cz/historicka-trasa-zlata-cesta.htmnout	k5eAaPmAgMnS
Archivováno	archivován	k2eAgNnSc4d1
11	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
-	-	kIx~
Historická	historický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
Zlatá	zlatý	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Aféra	aféra	k1gFnSc1
Bamberg	Bamberg	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bamberk	Bamberk	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Bamberk	Bamberk	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Galerie	galerie	k1gFnSc1
Bamberk	Bamberk	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
města	město	k1gNnSc2
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Hotel	hotel	k1gInSc1
and	and	k?
Travel	Travel	k1gInSc1
Information	Information	k1gInSc1
</s>
<s>
Bamberk	Bamberk	k1gInSc1
ve	v	k7c6
Vlastenském	vlastenský	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
historickém	historický	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Městské	městský	k2eAgInPc1d1
a	a	k8xC
zemské	zemský	k2eAgInPc1d1
okresy	okres	k1gInPc1
Bavorska	Bavorsko	k1gNnSc2
Městské	městský	k2eAgInPc1d1
okresy	okres	k1gInPc1
</s>
<s>
Amberg	Amberg	k1gMnSc1
•	•	k?
Ansbach	Ansbach	k1gMnSc1
•	•	k?
Aschaffenburg	Aschaffenburg	k1gMnSc1
•	•	k?
Augsburg	Augsburg	k1gMnSc1
•	•	k?
Bamberk	Bamberk	k1gInSc1
•	•	k?
Bayreuth	Bayreuth	k1gInSc1
•	•	k?
Coburg	Coburg	k1gInSc1
•	•	k?
Erlangen	Erlangen	k1gInSc1
•	•	k?
Fürth	Fürth	k1gInSc1
•	•	k?
Hof	Hof	k1gFnSc2
•	•	k?
Ingolstadt	Ingolstadt	k1gInSc1
•	•	k?
Kaufbeuren	Kaufbeurna	k1gFnPc2
•	•	k?
Kempten	Kempten	k2eAgInSc1d1
•	•	k?
Landshut	Landshut	k1gInSc1
•	•	k?
Memmingen	Memmingen	k1gInSc1
•	•	k?
Mnichov	Mnichov	k1gInSc1
•	•	k?
Norimberk	Norimberk	k1gInSc1
•	•	k?
Pasov	Pasov	k1gInSc1
•	•	k?
Řezno	Řezno	k1gNnSc1
•	•	k?
Rosenheim	Rosenheim	k1gMnSc1
•	•	k?
Schwabach	Schwabach	k1gMnSc1
•	•	k?
Schweinfurt	Schweinfurt	k1gInSc1
•	•	k?
Straubing	Straubing	k1gInSc1
•	•	k?
Weiden	Weidna	k1gFnPc2
•	•	k?
Würzburg	Würzburg	k1gMnSc1
Zemské	zemský	k2eAgInPc4d1
okresy	okres	k1gInPc4
</s>
<s>
Aichach-Friedberg	Aichach-Friedberg	k1gInSc1
•	•	k?
Altötting	Altötting	k1gInSc1
•	•	k?
Amberg-Sulzbach	Amberg-Sulzbach	k1gMnSc1
•	•	k?
Ansbach	Ansbach	k1gMnSc1
•	•	k?
Aschaffenburg	Aschaffenburg	k1gMnSc1
•	•	k?
Augsburg	Augsburg	k1gMnSc1
•	•	k?
Bad	Bad	k1gFnSc1
Kissingen	Kissingen	k1gInSc1
•	•	k?
Bad	Bad	k1gMnSc1
Tölz-Wolfratshausen	Tölz-Wolfratshausen	k2eAgMnSc1d1
•	•	k?
Bamberg	Bamberg	k1gMnSc1
•	•	k?
Bayreuth	Bayreuth	k1gMnSc1
•	•	k?
Berchtesgadener	Berchtesgadener	k1gMnSc1
Land	Land	k1gMnSc1
•	•	k?
Cham	Cham	k1gMnSc1
•	•	k?
Coburg	Coburg	k1gInSc1
•	•	k?
Dachau	Dachaus	k1gInSc2
•	•	k?
Deggendorf	Deggendorf	k1gMnSc1
•	•	k?
Dillingen	Dillingen	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Donau	donau	k1gInSc1
•	•	k?
Dingolfing-Landau	Dingolfing-Landaus	k1gInSc2
•	•	k?
Donau-Ries	Donau-Ries	k1gMnSc1
•	•	k?
Ebersberg	Ebersberg	k1gMnSc1
•	•	k?
Eichstätt	Eichstätt	k1gMnSc1
•	•	k?
Erding	Erding	k1gInSc1
•	•	k?
Erlangen-Höchstadt	Erlangen-Höchstadt	k1gInSc1
•	•	k?
Forchheim	Forchheim	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Freising	Freising	k1gInSc1
•	•	k?
Freyung-Grafenau	Freyung-Grafenaus	k1gInSc2
•	•	k?
Fürstenfeldbruck	Fürstenfeldbruck	k1gMnSc1
•	•	k?
Fürth	Fürth	k1gMnSc1
•	•	k?
Garmisch-Partenkirchen	Garmisch-Partenkirchen	k1gInSc1
•	•	k?
Günzburg	Günzburg	k1gInSc1
•	•	k?
Haßberge	Haßberg	k1gFnSc2
•	•	k?
Hof	Hof	k1gMnSc1
•	•	k?
Kelheim	Kelheim	k1gMnSc1
•	•	k?
Kitzingen	Kitzingen	k1gInSc1
•	•	k?
Kronach	Kronach	k1gMnSc1
•	•	k?
Kulmbach	Kulmbach	k1gMnSc1
•	•	k?
Landsberg	Landsberg	k1gMnSc1
am	am	k?
Lech	Lech	k1gMnSc1
•	•	k?
Landshut	Landshut	k1gMnSc1
•	•	k?
Lichtenfels	Lichtenfels	k1gInSc1
•	•	k?
Lindau	Lindaus	k1gInSc2
(	(	kIx(
<g/>
Bodensee	Bodensee	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Main-Spessart	Main-Spessart	k1gInSc1
•	•	k?
Miesbach	Miesbach	k1gMnSc1
•	•	k?
Miltenberg	Miltenberg	k1gMnSc1
•	•	k?
Mühldorf	Mühldorf	k1gMnSc1
am	am	k?
Inn	Inn	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Mnichov	Mnichov	k1gInSc1
•	•	k?
Neu-Ulm	Neu-Ulm	k1gInSc1
•	•	k?
Neuburg-Schrobenhausen	Neuburg-Schrobenhausen	k1gInSc1
•	•	k?
Neumarkt	Neumarkt	k1gInSc1
in	in	k?
der	drát	k5eAaImRp2nS
Oberpfalz	Oberpfalz	k1gInSc1
•	•	k?
Neustadt	Neustadt	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Aisch-Bad	Aisch-Bad	k1gInSc1
Windsheim	Windsheim	k1gMnSc1
•	•	k?
Neustadt	Neustadt	k1gMnSc1
an	an	k?
der	drát	k5eAaImRp2nS
Waldnaab	Waldnaab	k1gInSc1
•	•	k?
Nürnberger	Nürnberger	k1gInSc1
Land	Land	k1gInSc1
•	•	k?
Oberallgäu	Oberallgäus	k1gInSc2
•	•	k?
Ostallgäu	Ostallgäus	k1gInSc2
•	•	k?
Pasov	Pasov	k1gInSc1
•	•	k?
Pfaffenhofen	Pfaffenhofen	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Ilm	Ilm	k1gFnSc2
•	•	k?
Regen	Regna	k1gFnPc2
•	•	k?
Řezno	Řezno	k1gNnSc4
•	•	k?
Rhön-Grabfeld	Rhön-Grabfeld	k1gMnSc1
•	•	k?
Rosenheim	Rosenheim	k1gMnSc1
•	•	k?
Roth	Roth	k1gMnSc1
•	•	k?
Rottal-Inn	Rottal-Inn	k1gMnSc1
•	•	k?
Schwandorf	Schwandorf	k1gMnSc1
•	•	k?
Schweinfurt	Schweinfurt	k1gInSc1
•	•	k?
Starnberg	Starnberg	k1gInSc1
•	•	k?
Straubing-Bogen	Straubing-Bogen	k1gInSc1
•	•	k?
Tirschenreuth	Tirschenreuth	k1gInSc1
•	•	k?
Traunstein	Traunstein	k1gInSc1
•	•	k?
Unterallgäu	Unterallgäus	k1gInSc2
•	•	k?
Weilheim-Schongau	Weilheim-Schongaus	k1gInSc2
•	•	k?
Weißenburg-Gunzenhausen	Weißenburg-Gunzenhausen	k2eAgInSc1d1
•	•	k?
Wunsiedel	Wunsiedlo	k1gNnPc2
im	im	k?
Fichtelgebirge	Fichtelgebirg	k1gFnSc2
•	•	k?
Würzburg	Würzburg	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
134047	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4004391-5	4004391-5	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2110	#num#	k4
0869	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80139571	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
129007724	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80139571	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Německo	Německo	k1gNnSc1
</s>
