<s>
Folkovou	folkový	k2eAgFnSc4d1	folková
píseň	píseň	k1gFnSc4	píseň
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
typu	typ	k1gInSc2	typ
tedy	tedy	k9	tedy
charakterizujeme	charakterizovat	k5eAaBmIp1nP	charakterizovat
jako	jako	k8xC	jako
typ	typ	k1gInSc1	typ
moderní	moderní	k2eAgFnSc2d1	moderní
zpěvné	zpěvný	k2eAgFnSc2d1	zpěvná
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spontánně	spontánně	k6eAd1	spontánně
vznikala	vznikat	k5eAaImAgFnS	vznikat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
amatérských	amatérský	k2eAgFnPc6d1	amatérská
podmínkách	podmínka	k1gFnPc6	podmínka
jako	jako	k8xS	jako
protiklad	protiklad	k1gInSc4	protiklad
konvenčního	konvenční	k2eAgInSc2d1	konvenční
a	a	k8xC	a
zkomercionalizovaného	zkomercionalizovaný	k2eAgInSc2d1	zkomercionalizovaný
popu	pop	k1gInSc2	pop
<g/>
.	.	kIx.	.
</s>
