<p>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
157	[number]	k4	157
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Českého	český	k2eAgInSc2d1	český
Krumlova	Krumlov	k1gInSc2	Krumlov
přes	přes	k7c4	přes
Borovany	Borovan	k1gMnPc4	Borovan
do	do	k7c2	do
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
56	[number]	k4	56
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
okruhem	okruh	k1gInSc7	okruh
z	z	k7c2	z
Českého	český	k2eAgInSc2d1	český
Krumlova	Krumlov	k1gInSc2	Krumlov
přes	přes	k7c4	přes
Kaplici-Nádraží	Kaplici-Nádraží	k1gNnSc4	Kaplici-Nádraží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vede	vést	k5eAaImIp3nS	vést
v	v	k7c6	v
peáži	peáž	k1gFnSc6	peáž
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
asi	asi	k9	asi
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
Besednici	besednice	k1gFnSc4	besednice
<g/>
,	,	kIx,	,
Trhové	trhový	k2eAgFnPc4d1	trhová
Sviny	Svina	k1gFnPc4	Svina
<g/>
,	,	kIx,	,
Borovany	Borovan	k1gMnPc4	Borovan
a	a	k8xC	a
Ledenice	Ledenice	k1gFnPc4	Ledenice
až	až	k9	až
na	na	k7c6	na
Nádražní	nádražní	k2eAgFnSc6d1	nádražní
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
regionální	regionální	k2eAgInSc4d1	regionální
význam	význam	k1gInSc4	význam
a	a	k8xC	a
největší	veliký	k2eAgInSc4d3	veliký
stupeň	stupeň	k1gInSc4	stupeň
intenzity	intenzita	k1gFnSc2	intenzita
dopravního	dopravní	k2eAgInSc2d1	dopravní
provozu	provoz	k1gInSc2	provoz
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
a	a	k8xC	a
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
včetně	včetně	k7c2	včetně
Kaplice-Nádraží	Kaplice-Nádraží	k1gNnSc2	Kaplice-Nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
převážně	převážně	k6eAd1	převážně
přes	přes	k7c4	přes
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
obcemi	obec	k1gFnPc7	obec
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc4d1	český
Krumlov	Krumlov	k1gInSc4	Krumlov
<g/>
,	,	kIx,	,
Kaplice-Nádraží	Kaplice-Nádraží	k1gNnPc4	Kaplice-Nádraží
<g/>
,	,	kIx,	,
Besednice	besednice	k1gFnPc4	besednice
<g/>
,	,	kIx,	,
Trhové	trhový	k2eAgMnPc4d1	trhový
Sviny	Svin	k1gMnPc4	Svin
<g/>
,	,	kIx,	,
Borovany	Borovan	k1gMnPc4	Borovan
<g/>
,	,	kIx,	,
Ledenice	Ledenice	k1gFnSc1	Ledenice
a	a	k8xC	a
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
</p>
