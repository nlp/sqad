<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
na	na	k7c4	na
Klondiku	Klondika	k1gFnSc4	Klondika
byla	být	k5eAaImAgFnS	být
migrace	migrace	k1gFnSc1	migrace
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
pusté	pustý	k2eAgFnSc2d1	pustá
oblasti	oblast	k1gFnSc2	oblast
Klondike	Klondik	k1gFnSc2	Klondik
u	u	k7c2	u
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Dawson	Dawson	k1gNnSc1	Dawson
City	City	k1gFnSc2	City
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
teritoriu	teritorium	k1gNnSc6	teritorium
Yukon	Yukona	k1gFnPc2	Yukona
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1896	[number]	k4	1896
a	a	k8xC	a
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1896	[number]	k4	1896
a	a	k8xC	a
poté	poté	k6eAd1	poté
co	co	k9	co
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
objevu	objev	k1gInSc6	objev
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
dorazily	dorazit	k5eAaPmAgFnP	dorazit
do	do	k7c2	do
měst	město	k1gNnPc2	město
Seattle	Seattle	k1gFnSc2	Seattle
a	a	k8xC	a
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
masový	masový	k2eAgInSc4d1	masový
přesun	přesun	k1gInSc4	přesun
lidí	člověk	k1gMnPc2	člověk
toužících	toužící	k2eAgMnPc2d1	toužící
zbohatnout	zbohatnout	k5eAaPmF	zbohatnout
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgNnSc7	některý
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
skutečně	skutečně	k6eAd1	skutečně
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
zůstala	zůstat	k5eAaPmAgFnS	zůstat
chudá	chudý	k2eAgFnSc1d1	chudá
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yInSc4	co
bylo	být	k5eAaImAgNnS	být
zlato	zlato	k1gNnSc1	zlato
objeveno	objevit	k5eAaPmNgNnS	objevit
u	u	k7c2	u
aljaškého	aljašký	k2eAgNnSc2d1	aljašký
města	město	k1gNnSc2	město
Nome	Nom	k1gFnSc2	Nom
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
zlata	zlato	k1gNnSc2	zlato
tam	tam	k6eAd1	tam
způsobil	způsobit	k5eAaPmAgInS	způsobit
exodus	exodus	k1gInSc1	exodus
prospektorů	prospektor	k1gMnPc2	prospektor
z	z	k7c2	z
Klondiku	Klondik	k1gInSc2	Klondik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
na	na	k7c4	na
Klondiku	Klondika	k1gFnSc4	Klondika
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
masových	masový	k2eAgFnPc2d1	masová
migrací	migrace	k1gFnPc2	migrace
za	za	k7c4	za
nalezišti	naleziště	k1gNnSc6	naleziště
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
a	a	k8xC	a
jako	jako	k9	jako
taková	takový	k3xDgFnSc1	takový
byla	být	k5eAaImAgFnS	být
zvěčněna	zvěčnit	k5eAaPmNgFnS	zvěčnit
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
<g/>
,	,	kIx,	,
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
i	i	k8xC	i
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objevení	objevení	k1gNnPc2	objevení
zlata	zlato	k1gNnSc2	zlato
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1896	[number]	k4	1896
Američan	Američan	k1gMnSc1	Američan
z	z	k7c2	z
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
George	George	k1gNnSc2	George
Carmack	Carmacka	k1gFnPc2	Carmacka
společně	společně	k6eAd1	společně
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Kate	kat	k1gInSc5	kat
a	a	k8xC	a
švagry	švagr	k1gMnPc4	švagr
Keishem	Keish	k1gInSc7	Keish
(	(	kIx(	(
<g/>
známým	známý	k1gMnSc7	známý
jako	jako	k8xC	jako
Skookum	Skookum	k1gInSc4	Skookum
Jim	on	k3xPp3gMnPc3	on
Mason	mason	k1gMnSc1	mason
<g/>
)	)	kIx)	)
a	a	k8xC	a
Káa	Káa	k1gFnSc4	Káa
Gooxem	Goox	k1gInSc7	Goox
(	(	kIx(	(
<g/>
Dawson	Dawson	k1gMnSc1	Dawson
Charlie	Charlie	k1gMnSc1	Charlie
nebo	nebo	k8xC	nebo
Tagish	Tagish	k1gMnSc1	Tagish
Charlie	Charlie	k1gMnSc1	Charlie
<g/>
)	)	kIx)	)
chytali	chytat	k5eAaImAgMnP	chytat
lososy	losos	k1gMnPc7	losos
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Klondike	Klondik	k1gInSc2	Klondik
<g/>
.	.	kIx.	.
</s>
<s>
Kate	kat	k1gMnSc5	kat
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
švagři	švagr	k1gMnPc1	švagr
byli	být	k5eAaImAgMnP	být
příslušníky	příslušník	k1gMnPc4	příslušník
indiánského	indiánský	k2eAgInSc2d1	indiánský
kmene	kmen	k1gInSc2	kmen
Tagishů	Tagish	k1gInPc2	Tagish
<g/>
.	.	kIx.	.
</s>
<s>
Čtveřice	čtveřice	k1gFnSc1	čtveřice
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Hendersonem	Henderson	k1gMnSc7	Henderson
<g/>
,	,	kIx,	,
zkušeným	zkušený	k2eAgMnSc7d1	zkušený
zlatokopem	zlatokop	k1gMnSc7	zlatokop
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hledal	hledat	k5eAaImAgInS	hledat
zlato	zlato	k1gNnSc4	zlato
už	už	k6eAd1	už
25	[number]	k4	25
let	léto	k1gNnPc2	léto
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedařilo	dařit	k5eNaImAgNnS	dařit
zbohatnout	zbohatnout	k5eAaPmF	zbohatnout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
vyprávění	vyprávění	k1gNnPc4	vyprávění
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řeky	řeka	k1gFnSc2	řeka
Klondike	Klondik	k1gFnSc2	Klondik
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgNnP	mít
nacházet	nacházet	k5eAaImF	nacházet
naleziště	naleziště	k1gNnPc1	naleziště
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
Carmacka	Carmacko	k1gNnPc4	Carmacko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
poslední	poslední	k2eAgNnPc1d1	poslední
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
vyjadřující	vyjadřující	k2eAgInSc1d1	vyjadřující
negativní	negativní	k2eAgInSc4d1	negativní
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
Indiánům	Indián	k1gMnPc3	Indián
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
velmi	velmi	k6eAd1	velmi
roztrpčila	roztrpčit	k5eAaPmAgFnS	roztrpčit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Carmack	Carmack	k6eAd1	Carmack
se	s	k7c7	s
společníky	společník	k1gMnPc7	společník
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
přesunul	přesunout	k5eAaPmAgMnS	přesunout
o	o	k7c4	o
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
Králičímu	králičí	k2eAgInSc3d1	králičí
potoku	potok	k1gInSc3	potok
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
zde	zde	k6eAd1	zde
kácet	kácet	k5eAaImF	kácet
stromy	strom	k1gInPc4	strom
a	a	k8xC	a
klády	kláda	k1gFnPc4	kláda
prodat	prodat	k5eAaPmF	prodat
na	na	k7c6	na
pile	pila	k1gFnSc6	pila
u	u	k7c2	u
Fortymile	Fortymila	k1gFnSc3	Fortymila
Creeku	Creek	k1gInSc2	Creek
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Hendersonovu	Hendersonův	k2eAgNnSc3d1	Hendersonovo
vyprávění	vyprávění	k1gNnSc3	vyprávění
zároveň	zároveň	k6eAd1	zároveň
hledali	hledat	k5eAaImAgMnP	hledat
v	v	k7c6	v
potoce	potok	k1gInSc6	potok
zlato	zlato	k1gNnSc1	zlato
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1896	[number]	k4	1896
zlato	zlato	k1gNnSc4	zlato
skutečně	skutečně	k6eAd1	skutečně
našli	najít	k5eAaPmAgMnP	najít
<g/>
.	.	kIx.	.
</s>
<s>
Naleziště	naleziště	k1gNnSc1	naleziště
bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
vydatnější	vydatný	k2eAgInSc4d2	vydatnější
než	než	k8xS	než
všechny	všechen	k3xTgInPc4	všechen
do	do	k7c2	do
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
objevené	objevený	k2eAgFnSc2d1	objevená
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Yukonu	Yukon	k1gInSc2	Yukon
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nálezu	nález	k1gInSc3	nález
byl	být	k5eAaImAgMnS	být
také	také	k9	také
Králičí	králičí	k2eAgInSc1d1	králičí
potok	potok	k1gInSc1	potok
přejmenován	přejmenován	k2eAgInSc1d1	přejmenován
na	na	k7c4	na
Bonanza	Bonanz	k1gMnSc4	Bonanz
Creek	Creek	k6eAd1	Creek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
první	první	k4xOgNnSc4	první
zlato	zlato	k1gNnSc4	zlato
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Většinový	většinový	k2eAgInSc1d1	většinový
názor	názor	k1gInSc1	názor
se	se	k3xPyFc4	se
přiklání	přiklánět	k5eAaImIp3nS	přiklánět
k	k	k7c3	k
Skookum	Skookum	k1gNnSc4	Skookum
Jimovi	Jima	k1gMnSc3	Jima
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byl	být	k5eAaImAgInS	být
objev	objev	k1gInSc1	objev
připsán	připsat	k5eAaPmNgInS	připsat
Carmackovi	Carmacek	k1gMnSc3	Carmacek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
on	on	k3xPp3gMnSc1	on
požádal	požádat	k5eAaPmAgMnS	požádat
úřady	úřada	k1gMnPc4	úřada
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
zlatonosné	zlatonosný	k2eAgFnSc2d1	zlatonosná
parcely	parcela	k1gFnSc2	parcela
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
společníky	společník	k1gMnPc4	společník
<g/>
,	,	kIx,	,
že	že	k8xS	že
požadavku	požadavek	k1gInSc2	požadavek
Indiánů	Indián	k1gMnPc2	Indián
by	by	kYmCp3nP	by
nemuselo	muset	k5eNaImAgNnS	muset
být	být	k5eAaImF	být
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
kanadských	kanadský	k2eAgInPc2d1	kanadský
zákonů	zákon	k1gInPc2	zákon
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
každý	každý	k3xTgInSc4	každý
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
parcelu	parcela	k1gFnSc4	parcela
-	-	kIx~	-
claim	claim	k1gInSc4	claim
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
152	[number]	k4	152
metrů	metr	k1gInPc2	metr
na	na	k7c4	na
305	[number]	k4	305
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgMnS	být
objevitel	objevitel	k1gMnSc1	objevitel
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mohl	moct	k5eAaImAgMnS	moct
mít	mít	k5eAaImF	mít
claimy	claim	k1gInPc4	claim
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
zaregistroval	zaregistrovat	k5eAaPmAgMnS	zaregistrovat
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
dva	dva	k4xCgMnPc4	dva
společníky	společník	k1gMnPc4	společník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
objevu	objev	k1gInSc6	objev
zlata	zlato	k1gNnSc2	zlato
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
ve	v	k7c6	v
zlatokopeckých	zlatokopecký	k2eAgInPc6d1	zlatokopecký
táborech	tábor	k1gInPc6	tábor
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Yukon	Yukona	k1gFnPc2	Yukona
a	a	k8xC	a
zanedlouho	zanedlouho	k6eAd1	zanedlouho
byly	být	k5eAaImAgInP	být
pozemky	pozemek	k1gInPc1	pozemek
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Bonanza	Bonanza	k1gFnSc1	Bonanza
Creek	Creek	k1gMnSc1	Creek
i	i	k8xC	i
sousedních	sousední	k2eAgMnPc2d1	sousední
Eldorado	Eldorada	k1gFnSc5	Eldorada
Creek	Creko	k1gNnPc2	Creko
a	a	k8xC	a
Hunker	Hunkra	k1gFnPc2	Hunkra
Creek	Creek	k6eAd1	Creek
zabrané	zabraný	k2eAgFnPc4d1	zabraná
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
předtím	předtím	k6eAd1	předtím
hledali	hledat	k5eAaImAgMnP	hledat
štěstí	štěstí	k1gNnSc4	štěstí
jinde	jinde	k6eAd1	jinde
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Yukon	Yukona	k1gFnPc2	Yukona
<g/>
.	.	kIx.	.
</s>
<s>
Henderson	Henderson	k1gInSc1	Henderson
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
hledal	hledat	k5eAaImAgMnS	hledat
zlato	zlato	k1gNnSc4	zlato
jen	jen	k9	jen
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
odtud	odtud	k6eAd1	odtud
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c6	o
nálezu	nález	k1gInSc6	nález
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
až	až	k6eAd1	až
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
všechny	všechen	k3xTgInPc1	všechen
bohaté	bohatý	k2eAgInPc1d1	bohatý
pozemky	pozemek	k1gInPc1	pozemek
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
obsazené	obsazený	k2eAgInPc1d1	obsazený
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
své	svůj	k3xOyFgNnSc4	svůj
štěstí	štěstí	k1gNnSc4	štěstí
na	na	k7c4	na
Hunker	Hunker	k1gInSc4	Hunker
Creeku	Creek	k1gInSc2	Creek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
podlomenému	podlomený	k2eAgNnSc3d1	podlomené
zdraví	zdraví	k1gNnSc3	zdraví
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
prodat	prodat	k5eAaPmF	prodat
svůj	svůj	k3xOyFgInSc4	svůj
pozemek	pozemek	k1gInSc4	pozemek
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
claim	claim	k1gMnSc1	claim
vynesl	vynést	k5eAaPmAgMnS	vynést
novým	nový	k2eAgMnSc7d1	nový
majitelům	majitel	k1gMnPc3	majitel
zlato	zlato	k1gNnSc4	zlato
za	za	k7c4	za
450	[number]	k4	450
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
než	než	k8xS	než
pozemek	pozemka	k1gFnPc2	pozemka
prodali	prodat	k5eAaPmAgMnP	prodat
dále	daleko	k6eAd2	daleko
za	za	k7c4	za
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
==	==	k?	==
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
extrémní	extrémní	k2eAgFnSc3d1	extrémní
izolaci	izolace	k1gFnSc3	izolace
Klondiku	Klondik	k1gInSc2	Klondik
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
bohatých	bohatý	k2eAgNnPc6d1	bohaté
nalezištích	naleziště	k1gNnPc6	naleziště
zlata	zlato	k1gNnSc2	zlato
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
USA	USA	kA	USA
až	až	k8xS	až
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1897	[number]	k4	1897
dorazili	dorazit	k5eAaPmAgMnP	dorazit
první	první	k4xOgMnPc1	první
zlatokopové	zlatokop	k1gMnPc1	zlatokop
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgInSc4d1	další
parník	parník	k1gInSc4	parník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
připlul	připlout	k5eAaPmAgInS	připlout
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
San	San	k1gFnSc2	San
Francisca	Franciscum	k1gNnSc2	Franciscum
už	už	k6eAd1	už
čekaly	čekat	k5eAaImAgFnP	čekat
tisíce	tisíc	k4xCgInPc1	tisíc
zvědavců	zvědavec	k1gMnPc2	zvědavec
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
tuně	tuna	k1gFnSc6	tuna
zlata	zlato	k1gNnSc2	zlato
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
palubě	paluba	k1gFnSc6	paluba
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
opravdová	opravdový	k2eAgFnSc1d1	opravdová
zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
odlehlé	odlehlý	k2eAgFnSc2d1	odlehlá
oblasti	oblast	k1gFnSc2	oblast
Klondike	Klondik	k1gInPc1	Klondik
se	se	k3xPyFc4	se
vydaly	vydat	k5eAaPmAgInP	vydat
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dychtivých	dychtivý	k2eAgMnPc2d1	dychtivý
zlatokopů	zlatokop	k1gMnPc2	zlatokop
připlula	připlout	k5eAaPmAgFnS	připlout
do	do	k7c2	do
přístavů	přístav	k1gInPc2	přístav
Skagway	Skagwaa	k1gFnSc2	Skagwaa
nebo	nebo	k8xC	nebo
Dyea	Dye	k1gInSc2	Dye
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Hřebeny	hřeben	k1gInPc1	hřeben
Pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
hor	hora	k1gFnPc2	hora
překonali	překonat	k5eAaPmAgMnP	překonat
přes	přes	k7c4	přes
průsmyk	průsmyk	k1gInSc4	průsmyk
Chilkoot	Chilkoot	k1gInSc1	Chilkoot
(	(	kIx(	(
<g/>
Chilkoot	Chilkoot	k1gInSc1	Chilkoot
Pass	Passa	k1gFnPc2	Passa
<g/>
,	,	kIx,	,
1067	[number]	k4	1067
m	m	kA	m
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Bílý	bílý	k2eAgInSc4d1	bílý
průsmyk	průsmyk	k1gInSc4	průsmyk
(	(	kIx(	(
<g/>
White	Whit	k1gMnSc5	Whit
Pass	Passa	k1gFnPc2	Passa
<g/>
,	,	kIx,	,
873	[number]	k4	873
m	m	kA	m
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cestu	cesta	k1gFnSc4	cesta
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
přes	přes	k7c4	přes
průsmyk	průsmyk	k1gInSc4	průsmyk
museli	muset	k5eAaImAgMnP	muset
překonat	překonat	k5eAaPmF	překonat
mnohokrát	mnohokrát	k6eAd1	mnohokrát
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
zlatokop	zlatokop	k1gMnSc1	zlatokop
s	s	k7c7	s
sebou	se	k3xPyFc7	se
musel	muset	k5eAaImAgMnS	muset
nést	nést	k5eAaImF	nést
mimo	mimo	k7c4	mimo
nářadí	nářadí	k1gNnSc4	nářadí
na	na	k7c6	na
rýžování	rýžování	k1gNnSc6	rýžování
až	až	k9	až
jednu	jeden	k4xCgFnSc4	jeden
tunu	tuna	k1gFnSc4	tuna
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
přežít	přežít	k5eAaPmF	přežít
zimu	zima	k1gFnSc4	zima
v	v	k7c6	v
kanadské	kanadský	k2eAgFnSc6d1	kanadská
divočině	divočina	k1gFnSc6	divočina
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
kanadské	kanadský	k2eAgFnSc2d1	kanadská
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
jízdní	jízdní	k2eAgFnSc2d1	jízdní
policie	policie	k1gFnSc2	policie
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Kanady	Kanada	k1gFnSc2	Kanada
důsledně	důsledně	k6eAd1	důsledně
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
zlatokop	zlatokop	k1gMnSc1	zlatokop
musel	muset	k5eAaImAgMnS	muset
mít	mít	k5eAaImF	mít
například	například	k6eAd1	například
150	[number]	k4	150
liber	libra	k1gFnPc2	libra
(	(	kIx(	(
<g/>
libra	libra	k1gFnSc1	libra
=	=	kIx~	=
0,45	[number]	k4	0,45
kg	kg	kA	kg
<g/>
)	)	kIx)	)
slaniny	slanina	k1gFnSc2	slanina
<g/>
,	,	kIx,	,
400	[number]	k4	400
liber	libra	k1gFnPc2	libra
pšeničné	pšeničný	k2eAgFnSc2d1	pšeničná
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
200	[number]	k4	200
liber	libra	k1gFnPc2	libra
kukuřičné	kukuřičný	k2eAgFnSc2d1	kukuřičná
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
125	[number]	k4	125
liber	libra	k1gFnPc2	libra
fazolí	fazole	k1gFnPc2	fazole
<g/>
,	,	kIx,	,
75	[number]	k4	75
liber	libra	k1gFnPc2	libra
sušeného	sušený	k2eAgNnSc2d1	sušené
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
25	[number]	k4	25
liber	libra	k1gFnPc2	libra
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
15	[number]	k4	15
liber	libra	k1gFnPc2	libra
soli	sůl	k1gFnSc2	sůl
a	a	k8xC	a
10	[number]	k4	10
liber	libra	k1gFnPc2	libra
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
cesty	cesta	k1gFnPc4	cesta
mohli	moct	k5eAaImAgMnP	moct
využívat	využívat	k5eAaPmF	využívat
tažná	tažný	k2eAgNnPc4d1	tažné
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
terén	terén	k1gInSc1	terén
stal	stát	k5eAaPmAgInS	stát
tak	tak	k6eAd1	tak
náročným	náročný	k2eAgInSc7d1	náročný
<g/>
,	,	kIx,	,
že	že	k8xS	že
zásoby	zásoba	k1gFnPc4	zásoba
museli	muset	k5eAaImAgMnP	muset
nést	nést	k5eAaImF	nést
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
průsmyku	průsmyk	k1gInSc3	průsmyk
Chilkoot	Chilkoota	k1gFnPc2	Chilkoota
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
strmý	strmý	k2eAgMnSc1d1	strmý
a	a	k8xC	a
riskantní	riskantní	k2eAgMnSc1d1	riskantní
<g/>
,	,	kIx,	,
na	na	k7c6	na
posledních	poslední	k2eAgInPc6d1	poslední
800	[number]	k4	800
metrech	metr	k1gInPc6	metr
bylo	být	k5eAaImAgNnS	být
převýšení	převýšení	k1gNnSc4	převýšení
až	až	k6eAd1	až
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
vedla	vést	k5eAaImAgFnS	vést
ledovým	ledový	k2eAgNnSc7d1	ledové
polem	pole	k1gNnSc7	pole
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
vytesali	vytesat	k5eAaPmAgMnP	vytesat
1500	[number]	k4	1500
schodů	schod	k1gInPc2	schod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
vešel	vejít	k5eAaPmAgInS	vejít
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
úzké	úzký	k2eAgFnSc6d1	úzká
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
tísnil	tísnit	k5eAaImAgInS	tísnit
nepřetržitý	přetržitý	k2eNgInSc4d1	nepřetržitý
zástup	zástup	k1gInSc4	zástup
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
dolů	dolů	k6eAd1	dolů
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Bílém	bílý	k2eAgInSc6d1	bílý
průsmyku	průsmyk	k1gInSc6	průsmyk
<g/>
,	,	kIx,	,
nacházejícím	nacházející	k2eAgMnSc7d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
nižší	nízký	k2eAgFnSc6d2	nižší
výšce	výška	k1gFnSc6	výška
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
ještě	ještě	k6eAd1	ještě
horší	zlý	k2eAgFnSc1d2	horší
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Chodník	chodník	k1gInSc1	chodník
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podél	podél	k7c2	podél
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
tažných	tažný	k2eAgNnPc2d1	tažné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
hřebenů	hřeben	k1gInPc2	hřeben
čekal	čekat	k5eAaImAgInS	čekat
zlatokopy	zlatokop	k1gMnPc4	zlatokop
sestup	sestup	k1gInSc1	sestup
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
Bennett	Bennetta	k1gFnPc2	Bennetta
na	na	k7c6	na
kanadské	kanadský	k2eAgFnSc6d1	kanadská
straně	strana	k1gFnSc6	strana
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nekonečný	konečný	k2eNgInSc1d1	nekonečný
proud	proud	k1gInSc1	proud
lidí	člověk	k1gMnPc2	člověk
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
městečko	městečko	k1gNnSc1	městečko
z	z	k7c2	z
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
stanů	stan	k1gInPc2	stan
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
káceli	kácet	k5eAaImAgMnP	kácet
stromy	strom	k1gInPc4	strom
a	a	k8xC	a
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
provizorní	provizorní	k2eAgNnPc4d1	provizorní
plavidla	plavidlo	k1gNnPc4	plavidlo
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
většinou	většinou	k6eAd1	většinou
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
nikdy	nikdy	k6eAd1	nikdy
nedělali	dělat	k5eNaImAgMnP	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
jim	on	k3xPp3gMnPc3	on
nezbývalo	zbývat	k5eNaImAgNnS	zbývat
než	než	k8xS	než
jen	jen	k6eAd1	jen
čekat	čekat	k5eAaImF	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
roztaje	roztát	k5eAaPmIp3nS	roztát
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
možno	možno	k6eAd1	možno
plavidla	plavidlo	k1gNnPc4	plavidlo
spustit	spustit	k5eAaPmF	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
flotila	flotila	k1gFnSc1	flotila
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgFnSc1d1	čítající
7	[number]	k4	7
124	[number]	k4	124
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
,	,	kIx,	,
naložená	naložený	k2eAgFnSc1d1	naložená
téměř	téměř	k6eAd1	téměř
15	[number]	k4	15
tisíci	tisíc	k4xCgInSc6	tisíc
tunami	tunami	k1gNnSc6	tunami
zásob	zásoba	k1gFnPc2	zásoba
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
800	[number]	k4	800
kilometrů	kilometr	k1gInPc2	kilometr
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
přes	přes	k7c4	přes
jezera	jezero	k1gNnPc4	jezero
a	a	k8xC	a
kaskády	kaskáda	k1gFnPc4	kaskáda
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
řeky	řeka	k1gFnSc2	řeka
Yukon	Yukona	k1gFnPc2	Yukona
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
vytoužený	vytoužený	k2eAgInSc4d1	vytoužený
Klondike	Klondike	k1gInSc4	Klondike
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
období	období	k1gNnSc6	období
zlaté	zlatý	k2eAgFnSc2d1	zlatá
horečky	horečka	k1gFnSc2	horečka
přišlo	přijít	k5eAaPmAgNnS	přijít
do	do	k7c2	do
liduprázdných	liduprázdný	k2eAgFnPc2d1	liduprázdná
oblastí	oblast	k1gFnPc2	oblast
okolo	okolo	k7c2	okolo
řeky	řeka	k1gFnSc2	řeka
Klondike	Klondike	k1gInSc1	Klondike
až	až	k6eAd1	až
sto	sto	k4xCgNnSc1	sto
tisíc	tisíc	k4xCgInSc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c4	v
Dawson	Dawson	k1gNnSc4	Dawson
City	city	k1gNnPc2	city
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
skoro	skoro	k6eAd1	skoro
30	[number]	k4	30
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
přišla	přijít	k5eAaPmAgFnS	přijít
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zde	zde	k6eAd1	zde
zbohatli	zbohatnout	k5eAaPmAgMnP	zbohatnout
<g/>
.	.	kIx.	.
</s>
<s>
Uplynuly	uplynout	k5eAaPmAgInP	uplynout
už	už	k6eAd1	už
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
od	od	k7c2	od
objevení	objevení	k1gNnSc2	objevení
naleziště	naleziště	k1gNnSc2	naleziště
na	na	k7c4	na
Bonanza	Bonanz	k1gMnSc4	Bonanz
Creek	Creek	k6eAd1	Creek
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
výnosné	výnosný	k2eAgFnPc1d1	výnosná
pozemky	pozemka	k1gFnPc1	pozemka
byly	být	k5eAaImAgFnP	být
už	už	k6eAd1	už
zabrané	zabraný	k2eAgFnPc1d1	zabraná
a	a	k8xC	a
mimoto	mimoto	k6eAd1	mimoto
zlata	zlato	k1gNnSc2	zlato
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
a	a	k8xC	a
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
přišla	přijít	k5eAaPmAgFnS	přijít
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
nových	nový	k2eAgNnPc6d1	nové
nalezištích	naleziště	k1gNnPc6	naleziště
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
houfně	houfně	k6eAd1	houfně
stěhovat	stěhovat	k5eAaImF	stěhovat
tam	tam	k6eAd1	tam
a	a	k8xC	a
zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
na	na	k7c4	na
Klondiku	Klondika	k1gFnSc4	Klondika
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Založení	založení	k1gNnSc1	založení
města	město	k1gNnSc2	město
Dawson	Dawsona	k1gFnPc2	Dawsona
City	City	k1gFnSc2	City
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
objevení	objevení	k1gNnSc6	objevení
zlata	zlato	k1gNnSc2	zlato
na	na	k7c6	na
Klondiku	Klondik	k1gInSc6	Klondik
zbohatli	zbohatnout	k5eAaPmAgMnP	zbohatnout
nejen	nejen	k6eAd1	nejen
zlatokopové	zlatokop	k1gMnPc1	zlatokop
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
podnikatelé	podnikatel	k1gMnPc1	podnikatel
a	a	k8xC	a
podnikavci	podnikavec	k1gMnPc1	podnikavec
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
byl	být	k5eAaImAgMnS	být
Joseph	Josepha	k1gFnPc2	Josepha
Francis	Francis	k1gFnSc2	Francis
Ladue	Ladue	k1gInSc1	Ladue
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
města	město	k1gNnSc2	město
Dawson	Dawson	k1gMnSc1	Dawson
<g/>
.	.	kIx.	.
</s>
<s>
Ladue	Ladue	k6eAd1	Ladue
na	na	k7c6	na
Yukonu	Yukon	k1gInSc6	Yukon
nebyl	být	k5eNaImAgMnS	být
žádný	žádný	k3yNgInSc4	žádný
nováček	nováček	k1gInSc4	nováček
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
zde	zde	k6eAd1	zde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
a	a	k8xC	a
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
své	svůj	k3xOyFgNnSc4	svůj
štěstí	štěstí	k1gNnSc4	štěstí
nejdříve	dříve	k6eAd3	dříve
jako	jako	k8xC	jako
zlatokop	zlatokop	k1gMnSc1	zlatokop
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
pilu	pila	k1gFnSc4	pila
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Sixtymile	Sixtymila	k1gFnSc3	Sixtymila
River	Rivra	k1gFnPc2	Rivra
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Klondiku	Klondik	k1gInSc2	Klondik
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řeky	řeka	k1gFnSc2	řeka
Yukon	Yukona	k1gFnPc2	Yukona
<g/>
.	.	kIx.	.
</s>
<s>
Pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obchodováním	obchodování	k1gNnSc7	obchodování
se	s	k7c7	s
zlatokopy	zlatokop	k1gMnPc7	zlatokop
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vydělat	vydělat	k5eAaPmF	vydělat
víc	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
než	než	k8xS	než
samotným	samotný	k2eAgNnSc7d1	samotné
hledáním	hledání	k1gNnSc7	hledání
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
soutok	soutok	k1gInSc4	soutok
řek	řeka	k1gFnPc2	řeka
Klondike	Klondike	k1gNnSc1	Klondike
a	a	k8xC	a
Yukon	Yukon	k1gNnSc1	Yukon
pouhých	pouhý	k2eAgInPc2d1	pouhý
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
po	po	k7c6	po
Carmackově	Carmackův	k2eAgInSc6d1	Carmackův
nálezu	nález	k1gInSc6	nález
<g/>
.	.	kIx.	.
</s>
<s>
Vytyčil	vytyčit	k5eAaPmAgMnS	vytyčit
zde	zde	k6eAd1	zde
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
64	[number]	k4	64
<g/>
hektarový	hektarový	k2eAgInSc4d1	hektarový
pozemek	pozemek	k1gInSc4	pozemek
a	a	k8xC	a
šel	jít	k5eAaImAgMnS	jít
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
okamžitě	okamžitě	k6eAd1	okamžitě
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
do	do	k7c2	do
Fort	Fort	k?	Fort
Constantine	Constantin	k1gInSc5	Constantin
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
Fortymile	Fortymila	k1gFnSc3	Fortymila
Creek	Creek	k6eAd1	Creek
do	do	k7c2	do
Yukonu	Yukon	k1gInSc2	Yukon
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
stavby	stavba	k1gFnSc2	stavba
skladu	sklad	k1gInSc2	sklad
a	a	k8xC	a
malé	malý	k2eAgFnPc1d1	malá
chatrče	chatrč	k1gFnPc1	chatrč
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
sloužila	sloužit	k5eAaImAgFnS	sloužit
i	i	k9	i
jako	jako	k9	jako
hostinec	hostinec	k1gInSc1	hostinec
<g/>
.	.	kIx.	.
</s>
<s>
Vznikající	vznikající	k2eAgNnSc4d1	vznikající
město	město	k1gNnSc4	město
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Dawson	Dawson	k1gInSc4	Dawson
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
kanadského	kanadský	k2eAgMnSc2d1	kanadský
geologa	geolog	k1gMnSc2	geolog
George	Georg	k1gMnSc2	Georg
Mercer	Mercer	k1gMnSc1	Mercer
Dawsona	Dawson	k1gMnSc2	Dawson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzestup	vzestup	k1gInSc4	vzestup
a	a	k8xC	a
pád	pád	k1gInSc4	pád
Dawson	Dawson	k1gInSc1	Dawson
City	city	k1gNnSc1	city
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1897	[number]	k4	1897
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Dawsonu	Dawson	k1gInSc6	Dawson
asi	asi	k9	asi
pět	pět	k4xCc4	pět
srubů	srub	k1gInPc2	srub
a	a	k8xC	a
malé	malý	k2eAgNnSc1d1	malé
stanové	stanový	k2eAgNnSc1d1	stanové
městečko	městečko	k1gNnSc1	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
už	už	k6eAd1	už
1400	[number]	k4	1400
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
točit	točit	k5eAaImF	točit
velké	velký	k2eAgInPc4d1	velký
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Noc	noc	k1gFnSc1	noc
v	v	k7c6	v
hostinci	hostinec	k1gInSc6	hostinec
J.	J.	kA	J.
Laduea	Laduea	k1gFnSc1	Laduea
stála	stát	k5eAaImAgFnS	stát
50	[number]	k4	50
$	$	kIx~	$
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
vejce	vejce	k1gNnSc1	vejce
1	[number]	k4	1
$	$	kIx~	$
<g/>
,	,	kIx,	,
pětiminutová	pětiminutový	k2eAgFnSc1d1	pětiminutová
koupel	koupel	k1gFnSc1	koupel
1,5	[number]	k4	1,5
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1897	[number]	k4	1897
otevřel	otevřít	k5eAaPmAgInS	otevřít
Harry	Harra	k1gFnSc2	Harra
Ash	Ash	k1gMnSc2	Ash
nový	nový	k2eAgInSc4d1	nový
hostinec	hostinec	k1gInSc4	hostinec
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
první	první	k4xOgFnSc4	první
noc	noc	k1gFnSc4	noc
měl	mít	k5eAaImAgMnS	mít
tržbu	tržba	k1gFnSc4	tržba
30	[number]	k4	30
000	[number]	k4	000
$	$	kIx~	$
a	a	k8xC	a
každou	každý	k3xTgFnSc4	každý
další	další	k2eAgFnSc4d1	další
3000	[number]	k4	3000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
Ash	Ash	k1gFnSc2	Ash
z	z	k7c2	z
Dawsonu	Dawson	k1gInSc2	Dawson
odešel	odejít	k5eAaPmAgMnS	odejít
se	s	k7c7	s
100	[number]	k4	100
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Dawsonu	Dawson	k1gInSc6	Dawson
už	už	k9	už
deset	deset	k4xCc4	deset
hostinců	hostinec	k1gInPc2	hostinec
<g/>
,	,	kIx,	,
tanečnice	tanečnice	k1gFnPc1	tanečnice
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
vydělávaly	vydělávat	k5eAaImAgFnP	vydělávat
až	až	k9	až
100	[number]	k4	100
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Ladue	Ladue	k1gInSc1	Ladue
začal	začít	k5eAaPmAgInS	začít
prodávat	prodávat	k5eAaImF	prodávat
městské	městský	k2eAgInPc4d1	městský
pozemky	pozemek	k1gInPc4	pozemek
za	za	k7c4	za
200	[number]	k4	200
$	$	kIx~	$
za	za	k7c4	za
čtvereční	čtvereční	k2eAgFnSc4d1	čtvereční
stopu	stopa	k1gFnSc4	stopa
(	(	kIx(	(
<g/>
0,09	[number]	k4	0,09
m2	m2	k4	m2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc4	město
už	už	k6eAd1	už
3500	[number]	k4	3500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
vlna	vlna	k1gFnSc1	vlna
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
však	však	k9	však
dorazila	dorazit	k5eAaPmAgFnS	dorazit
až	až	k9	až
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dorazili	dorazit	k5eAaPmAgMnP	dorazit
lidé	člověk	k1gMnPc1	člověk
putující	putující	k2eAgInSc4d1	putující
měsíce	měsíc	k1gInPc4	měsíc
ze	z	k7c2	z
Skagway	Skagwaa	k1gFnSc2	Skagwaa
a	a	k8xC	a
Dyea	Dyeum	k1gNnSc2	Dyeum
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sta	sto	k4xCgNnSc2	sto
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
jich	on	k3xPp3gMnPc2	on
sem	sem	k6eAd1	sem
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
čtyřicet	čtyřicet	k4xCc1	čtyřicet
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
městských	městský	k2eAgInPc2d1	městský
pozemků	pozemek	k1gInPc2	pozemek
vyletěly	vyletět	k5eAaPmAgInP	vyletět
do	do	k7c2	do
závratných	závratný	k2eAgFnPc2d1	závratná
výšek	výška	k1gFnPc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1898	[number]	k4	1898
se	se	k3xPyFc4	se
Dawson	Dawson	k1gNnSc1	Dawson
City	City	k1gFnSc2	City
stalo	stát	k5eAaPmAgNnS	stát
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
na	na	k7c6	na
severoamerickém	severoamerický	k2eAgInSc6d1	severoamerický
kontinentě	kontinent	k1gInSc6	kontinent
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Winnipegu	Winnipeg	k1gInSc2	Winnipeg
a	a	k8xC	a
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
ten	ten	k3xDgInSc1	ten
rok	rok	k1gInSc1	rok
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
sláva	sláva	k1gFnSc1	sláva
začala	začít	k5eAaPmAgFnS	začít
upadat	upadat	k5eAaImF	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
bohatá	bohatý	k2eAgNnPc1d1	bohaté
naleziště	naleziště	k1gNnPc1	naleziště
už	už	k6eAd1	už
byla	být	k5eAaImAgNnP	být
zabraná	zabraný	k2eAgFnSc1d1	zabraná
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
nově	nově	k6eAd1	nově
příchozích	příchozí	k1gFnPc2	příchozí
neměla	mít	k5eNaImAgFnS	mít
šanci	šance	k1gFnSc4	šance
zabrat	zabrat	k5eAaPmF	zabrat
si	se	k3xPyFc3	se
nové	nový	k2eAgFnPc4d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Mohli	moct	k5eAaImAgMnP	moct
si	se	k3xPyFc3	se
pozemek	pozemek	k1gInSc4	pozemek
jen	jen	k6eAd1	jen
odkoupit	odkoupit	k5eAaPmF	odkoupit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
příliš	příliš	k6eAd1	příliš
drahé	drahý	k2eAgNnSc1d1	drahé
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
přišly	přijít	k5eAaPmAgFnP	přijít
zvěsti	zvěst	k1gFnPc1	zvěst
o	o	k7c6	o
nových	nový	k2eAgNnPc6d1	nové
nalezištích	naleziště	k1gNnPc6	naleziště
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
tam	tam	k6eAd1	tam
a	a	k8xC	a
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
až	až	k9	až
na	na	k7c4	na
8000	[number]	k4	8000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
uznání	uznání	k1gNnSc2	uznání
Dawson	Dawson	k1gNnSc1	Dawson
City	City	k1gFnSc2	City
za	za	k7c4	za
město	město	k1gNnSc4	město
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
méně	málo	k6eAd2	málo
než	než	k8xS	než
pět	pět	k4xCc4	pět
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
i	i	k9	i
pod	pod	k7c4	pod
tisíc	tisíc	k4xCgInSc4	tisíc
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
tu	tu	k6eAd1	tu
žije	žít	k5eAaImIp3nS	žít
1327	[number]	k4	1327
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Těžba	těžba	k1gFnSc1	těžba
zlata	zlato	k1gNnSc2	zlato
na	na	k7c6	na
Klondiku	Klondik	k1gInSc6	Klondik
==	==	k?	==
</s>
</p>
<p>
<s>
Zlato	zlato	k1gNnSc1	zlato
se	se	k3xPyFc4	se
na	na	k7c6	na
Klondiku	Klondik	k1gInSc6	Klondik
těžilo	těžit	k5eAaImAgNnS	těžit
a	a	k8xC	a
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
těží	těžet	k5eAaImIp3nS	těžet
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
placerech	placero	k1gNnPc6	placero
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
placer	placra	k1gFnPc2	placra
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
španělštiny	španělština	k1gFnSc2	španělština
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
původním	původní	k2eAgInSc7d1	původní
významem	význam	k1gInSc7	význam
je	být	k5eAaImIp3nS	být
písčina	písčina	k1gFnSc1	písčina
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
místo	místo	k6eAd1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zlato	zlato	k1gNnSc4	zlato
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc4d1	jiný
vzácné	vzácný	k2eAgInPc4d1	vzácný
minerály	minerál	k1gInPc4	minerál
<g/>
)	)	kIx)	)
uložené	uložený	k2eAgFnPc4d1	uložená
v	v	k7c6	v
nánosech	nános	k1gInPc6	nános
písku	písek	k1gInSc2	písek
nebo	nebo	k8xC	nebo
štěrku	štěrk	k1gInSc2	štěrk
v	v	k7c6	v
korytech	koryto	k1gNnPc6	koryto
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
způsobem	způsob	k1gInSc7	způsob
jak	jak	k8xC	jak
zlato	zlato	k1gNnSc4	zlato
získat	získat	k5eAaPmF	získat
z	z	k7c2	z
placeru	placer	k1gInSc2	placer
je	být	k5eAaImIp3nS	být
rýžování	rýžování	k1gNnSc4	rýžování
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
devatenáctkrát	devatenáctkrát	k6eAd1	devatenáctkrát
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
těžší	těžký	k2eAgNnSc4d2	těžší
než	než	k8xS	než
štěrk	štěrk	k1gInSc4	štěrk
a	a	k8xC	a
písek	písek	k1gInSc4	písek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ručním	ruční	k2eAgNnSc6d1	ruční
rýžování	rýžování	k1gNnSc6	rýžování
se	se	k3xPyFc4	se
nabírá	nabírat	k5eAaImIp3nS	nabírat
směs	směs	k1gFnSc1	směs
štěrku	štěrk	k1gInSc2	štěrk
a	a	k8xC	a
písku	písek	k1gInSc2	písek
do	do	k7c2	do
kovové	kovový	k2eAgFnSc2d1	kovová
pánve	pánev	k1gFnSc2	pánev
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
proudu	proud	k1gInSc2	proud
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
za	za	k7c2	za
neustálého	neustálý	k2eAgNnSc2d1	neustálé
kroužení	kroužení	k1gNnSc2	kroužení
pánví	pánev	k1gFnPc2	pánev
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
postupně	postupně	k6eAd1	postupně
odplavují	odplavovat	k5eAaImIp3nP	odplavovat
lehčí	lehký	k2eAgFnPc1d2	lehčí
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
princip	princip	k1gInSc1	princip
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
při	při	k7c6	při
rýžování	rýžování	k1gNnSc6	rýžování
zlata	zlato	k1gNnSc2	zlato
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
otevřených	otevřený	k2eAgFnPc6d1	otevřená
bednách	bedna	k1gFnPc6	bedna
opatřených	opatřený	k2eAgInPc2d1	opatřený
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
příčkami	příčka	k1gFnPc7	příčka
na	na	k7c4	na
zpomalení	zpomalení	k1gNnSc4	zpomalení
pohybu	pohyb	k1gInSc2	pohyb
částeček	částečka	k1gFnPc2	částečka
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
rýžovaná	rýžovaný	k2eAgFnSc1d1	rýžovaný
směs	směs	k1gFnSc1	směs
lopatami	lopata	k1gFnPc7	lopata
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
počátečním	počáteční	k2eAgNnSc6d1	počáteční
období	období	k1gNnSc6	období
na	na	k7c4	na
Klondiku	Klondika	k1gFnSc4	Klondika
typický	typický	k2eAgInSc1d1	typický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těžba	těžba	k1gFnSc1	těžba
zlata	zlato	k1gNnSc2	zlato
na	na	k7c6	na
Klondiku	Klondikum	k1gNnSc6	Klondikum
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
svou	svůj	k3xOyFgFnSc4	svůj
zvláštnost	zvláštnost	k1gFnSc4	zvláštnost
<g/>
,	,	kIx,	,
vyplývající	vyplývající	k2eAgFnSc4d1	vyplývající
z	z	k7c2	z
klimatických	klimatický	k2eAgFnPc2d1	klimatická
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
několik	několik	k4yIc1	několik
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
permafrost	permafrost	k1gFnSc1	permafrost
-	-	kIx~	-
věčně	věčně	k6eAd1	věčně
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
nerozmrzající	rozmrzající	k2eNgFnSc1d1	rozmrzající
ani	ani	k8xC	ani
za	za	k7c2	za
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
letních	letní	k2eAgFnPc2d1	letní
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
počáteční	počáteční	k2eAgFnSc6d1	počáteční
těžbě	těžba	k1gFnSc6	těžba
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
začali	začít	k5eAaPmAgMnP	začít
zlatokopové	zlatokop	k1gMnPc1	zlatokop
těžit	těžit	k5eAaImF	těžit
hlubší	hluboký	k2eAgFnPc4d2	hlubší
vrstvy	vrstva	k1gFnPc4	vrstva
a	a	k8xC	a
narazili	narazit	k5eAaPmAgMnP	narazit
na	na	k7c4	na
zmrzlou	zmrzlý	k2eAgFnSc4d1	zmrzlá
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
napřed	napřed	k6eAd1	napřed
museli	muset	k5eAaImAgMnP	muset
rozmrazit	rozmrazit	k5eAaPmF	rozmrazit
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
prostě	prostě	k9	prostě
rozdělali	rozdělat	k5eAaPmAgMnP	rozdělat
na	na	k7c6	na
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
chtěli	chtít	k5eAaImAgMnP	chtít
odtěžit	odtěžit	k5eAaPmF	odtěžit
<g/>
,	,	kIx,	,
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
k	k	k7c3	k
rozmrazování	rozmrazování	k1gNnSc3	rozmrazování
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
činnost	činnost	k1gFnSc1	činnost
se	se	k3xPyFc4	se
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Rozmrazenou	Rozmrazený	k2eAgFnSc4d1	Rozmrazený
horninu	hornina	k1gFnSc4	hornina
těžili	těžit	k5eAaImAgMnP	těžit
ze	z	k7c2	z
šachet	šachta	k1gFnPc2	šachta
pomocí	pomocí	k7c2	pomocí
ručních	ruční	k2eAgInPc2d1	ruční
rumpálů	rumpál	k1gInPc2	rumpál
a	a	k8xC	a
ukládali	ukládat	k5eAaImAgMnP	ukládat
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
ji	on	k3xPp3gFnSc4	on
potom	potom	k8xC	potom
vodou	voda	k1gFnSc7	voda
promývali	promývat	k5eAaImAgMnP	promývat
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
se	se	k3xPyFc4	se
na	na	k7c6	na
Klondiku	Klondik	k1gInSc6	Klondik
těžilo	těžit	k5eAaImAgNnS	těžit
ještě	ještě	k6eAd1	ještě
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
zlaté	zlatý	k2eAgFnSc2d1	zlatá
horečky	horečka	k1gFnSc2	horečka
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
těžba	těžba	k1gFnSc1	těžba
obnovila	obnovit	k5eAaPmAgFnS	obnovit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
tolik	tolik	k6eAd1	tolik
rentabilní	rentabilní	k2eAgMnSc1d1	rentabilní
jako	jako	k8xS	jako
dřív	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
však	však	k9	však
o	o	k7c4	o
individuální	individuální	k2eAgNnSc4d1	individuální
rýžování	rýžování	k1gNnSc4	rýžování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
těžbu	těžba	k1gFnSc4	těžba
velkými	velký	k2eAgInPc7d1	velký
plovoucími	plovoucí	k2eAgInPc7d1	plovoucí
bagry	bagr	k1gInPc7	bagr
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
začal	začít	k5eAaPmAgMnS	začít
těžit	těžit	k5eAaImF	těžit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
za	za	k7c4	za
doznívání	doznívání	k1gNnSc4	doznívání
zlaté	zlatý	k2eAgFnSc2d1	zlatá
horečky	horečka	k1gFnSc2	horečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jack	Jack	k1gMnSc1	Jack
London	London	k1gMnSc1	London
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
desítkami	desítka	k1gFnPc7	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
za	za	k7c4	za
zlaté	zlatý	k2eAgFnPc4d1	zlatá
horečky	horečka	k1gFnPc4	horečka
hledat	hledat	k5eAaImF	hledat
na	na	k7c4	na
Klondike	Klondike	k1gInSc4	Klondike
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Jack	Jack	k1gMnSc1	Jack
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
své	svůj	k3xOyFgInPc4	svůj
zážitky	zážitek	k1gInPc4	zážitek
použil	použít	k5eAaPmAgMnS	použít
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
knih	kniha	k1gFnPc2	kniha
Bílý	bílý	k2eAgInSc1d1	bílý
tesák	tesák	k1gInSc1	tesák
a	a	k8xC	a
Volání	volání	k1gNnSc1	volání
divočiny	divočina	k1gFnSc2	divočina
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
Yukon	Yukon	k1gInSc4	Yukon
jako	jako	k8xS	jako
jednadvacetiletý	jednadvacetiletý	k2eAgInSc4d1	jednadvacetiletý
v	v	k7c6	v
září	září	k1gNnSc6	září
1897	[number]	k4	1897
a	a	k8xC	a
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
štěstí	štěstí	k1gNnSc4	štěstí
jako	jako	k8xC	jako
zlatokop	zlatokop	k1gMnSc1	zlatokop
na	na	k7c6	na
potoce	potok	k1gInSc6	potok
Henderson	Henderson	k1gMnSc1	Henderson
Creek	Creek	k1gMnSc1	Creek
<g/>
,	,	kIx,	,
120	[number]	k4	120
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Dawson	Dawsona	k1gFnPc2	Dawsona
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
chatrč	chatrč	k1gFnSc1	chatrč
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
zlaté	zlatý	k2eAgFnSc6d1	zlatá
horečce	horečka	k1gFnSc6	horečka
opuštěná	opuštěný	k2eAgNnPc1d1	opuštěné
a	a	k8xC	a
znovuobjevená	znovuobjevený	k2eAgFnSc1d1	znovuobjevená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byla	být	k5eAaImAgFnS	být
chatrč	chatrč	k1gFnSc1	chatrč
rozebrána	rozebrat	k5eAaPmNgFnS	rozebrat
a	a	k8xC	a
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
prken	prkno	k1gNnPc2	prkno
byly	být	k5eAaImAgFnP	být
zhotoveny	zhotovit	k5eAaPmNgFnP	zhotovit
dvě	dva	k4xCgFnPc1	dva
repliky	replika	k1gFnPc1	replika
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
Londonově	Londonův	k2eAgNnSc6d1	Londonův
rodišti	rodiště	k1gNnSc6	rodiště
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
v	v	k7c4	v
Dawson	Dawson	k1gInSc4	Dawson
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
zároveň	zároveň	k6eAd1	zároveň
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
i	i	k9	i
muzeum	muzeum	k1gNnSc1	muzeum
věnované	věnovaný	k2eAgNnSc1d1	věnované
Jacku	Jacka	k1gFnSc4	Jacka
Londonovi	London	k1gMnSc3	London
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
literátem	literát	k1gMnSc7	literát
spojeným	spojený	k2eAgMnSc7d1	spojený
se	se	k3xPyFc4	se
zlatou	zlatý	k2eAgFnSc7d1	zlatá
horečkou	horečka	k1gFnSc7	horečka
byl	být	k5eAaImAgMnS	být
básník	básník	k1gMnSc1	básník
Robert	Robert	k1gMnSc1	Robert
W.	W.	kA	W.
Service	Service	k1gFnSc1	Service
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
chatrč	chatrč	k1gFnSc1	chatrč
dodnes	dodnes	k6eAd1	dodnes
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c4	v
Dawson	Dawson	k1gNnSc4	Dawson
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
dnešek	dnešek	k1gInSc1	dnešek
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
na	na	k7c6	na
Klondiku	Klondik	k1gInSc6	Klondik
trvala	trvat	k5eAaImAgFnS	trvat
jen	jen	k6eAd1	jen
pouhé	pouhý	k2eAgMnPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc1	rok
<g/>
,	,	kIx,	,
Klondike	Klondike	k1gNnSc1	Klondike
a	a	k8xC	a
Dawson	Dawson	k1gNnSc1	Dawson
City	city	k1gNnPc2	city
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
staly	stát	k5eAaPmAgFnP	stát
známými	známá	k1gFnPc7	známá
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dawson	Dawson	k1gInSc1	Dawson
City	City	k1gFnSc2	City
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
na	na	k7c4	na
zlatou	zlatý	k2eAgFnSc4d1	zlatá
horečku	horečka	k1gFnSc4	horečka
stále	stále	k6eAd1	stále
udržovat	udržovat	k5eAaImF	udržovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgMnSc3	ten
přizpůsobena	přizpůsoben	k2eAgFnSc1d1	přizpůsobena
i	i	k8xC	i
architektura	architektura	k1gFnSc1	architektura
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
zde	zde	k6eAd1	zde
žádné	žádný	k3yNgFnPc4	žádný
výškové	výškový	k2eAgFnPc4d1	výšková
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
i	i	k9	i
nově	nově	k6eAd1	nově
postavené	postavený	k2eAgFnPc1d1	postavená
budovy	budova	k1gFnPc1	budova
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
stavět	stavět	k5eAaImF	stavět
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
z	z	k7c2	z
časů	čas	k1gInPc2	čas
zlaté	zlatý	k2eAgFnSc2d1	zlatá
horečky	horečka	k1gFnSc2	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Turistika	turistika	k1gFnSc1	turistika
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
odvětvím	odvětví	k1gNnSc7	odvětví
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
přes	přes	k7c4	přes
značnou	značný	k2eAgFnSc4d1	značná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
velkých	velký	k2eAgNnPc2d1	velké
center	centrum	k1gNnPc2	centrum
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
Dawson	Dawson	k1gInSc1	Dawson
City	city	k1gNnSc1	city
každoročně	každoročně	k6eAd1	každoročně
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
na	na	k7c6	na
Klondiku	Klondik	k1gInSc6	Klondik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
