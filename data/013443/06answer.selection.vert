<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
na	na	k7c4	na
Klondiku	Klondika	k1gFnSc4	Klondika
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
masových	masový	k2eAgFnPc2d1	masová
migrací	migrace	k1gFnPc2	migrace
za	za	k7c4	za
nalezišti	naleziště	k1gNnSc6	naleziště
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
a	a	k8xC	a
jako	jako	k9	jako
taková	takový	k3xDgFnSc1	takový
byla	být	k5eAaImAgFnS	být
zvěčněna	zvěčnit	k5eAaPmNgFnS	zvěčnit
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
<g/>
,	,	kIx,	,
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
i	i	k8xC	i
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
