<p>
<s>
Hongkong	Hongkong	k1gInSc1	Hongkong
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Siang-kang	Sianganga	k1gFnPc2	Siang-kanga
<g/>
,	,	kIx,	,
pchin-jinem	pchinino	k1gNnSc7	pchin-jino
Xiā	Xiā	k1gFnPc2	Xiā
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc7	znak
香	香	k?	香
<g/>
,	,	kIx,	,
kantonsky	kantonsky	k6eAd1	kantonsky
Hè	Hè	k1gFnSc1	Hè
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
Voňavý	voňavý	k2eAgInSc1d1	voňavý
přístav	přístav	k1gInSc1	přístav
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
region	region	k1gInSc4	region
ležící	ležící	k2eAgInSc4d1	ležící
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jihočínského	jihočínský	k2eAgNnSc2d1	Jihočínské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Hongkong	Hongkong	k1gInSc1	Hongkong
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc4d1	významné
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejhustěji	husto	k6eAd3	husto
osídlené	osídlený	k2eAgFnPc4d1	osídlená
oblasti	oblast	k1gFnPc4	oblast
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
regionu	region	k1gInSc2	region
je	být	k5eAaImIp3nS	být
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
administrativní	administrativní	k2eAgFnSc1d1	administrativní
oblast	oblast	k1gFnSc1	oblast
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
Hongkong	Hongkong	k1gInSc1	Hongkong
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Čung-chua	Čunghua	k1gFnSc1	Čung-chua
žen-min	ženin	k1gInSc1	žen-min
kung-che-kuo	kungheuo	k1gMnSc1	kung-che-kuo
Siang-kang	Siangang	k1gMnSc1	Siang-kang
tche-pie	tcheie	k1gFnSc2	tche-pie
sing-čeng-čchü	sing-čeng-čchü	k?	sing-čeng-čchü
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc1	znak
中	中	k?	中
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Hong	Honga	k1gFnPc2	Honga
Kong	Kongo	k1gNnPc2	Kongo
Special	Special	k1gMnSc1	Special
Administrative	Administrativ	k1gInSc5	Administrativ
Region	region	k1gInSc1	region
of	of	k?	of
the	the	k?	the
People	People	k1gFnSc2	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
China	China	k1gFnSc1	China
<g/>
)	)	kIx)	)
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
správní	správní	k2eAgFnPc4d1	správní
oblasti	oblast	k1gFnPc4	oblast
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
se	se	k3xPyFc4	se
Hongkong	Hongkong	k1gInSc1	Hongkong
těší	těšit	k5eAaImIp3nS	těšit
vysokému	vysoký	k2eAgInSc3d1	vysoký
stupni	stupeň	k1gInSc3	stupeň
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
zodpovídá	zodpovídat	k5eAaImIp3nS	zodpovídat
centrální	centrální	k2eAgFnSc1d1	centrální
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Hongkong	Hongkong	k1gInSc1	Hongkong
byl	být	k5eAaImAgInS	být
osídlen	osídlit	k5eAaPmNgInS	osídlit
již	již	k6eAd1	již
od	od	k7c2	od
neolitu	neolit	k1gInSc2	neolit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
portugalský	portugalský	k2eAgMnSc1d1	portugalský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
Jorge	Jorge	k1gFnPc2	Jorge
Álvares	Álvares	k1gMnSc1	Álvares
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1513	[number]	k4	1513
<g/>
.	.	kIx.	.
</s>
<s>
Portugalci	Portugalec	k1gMnPc1	Portugalec
poté	poté	k6eAd1	poté
zřídili	zřídit	k5eAaPmAgMnP	zřídit
obchodní	obchodní	k2eAgFnSc4d1	obchodní
stanici	stanice	k1gFnSc4	stanice
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Macau	Macao	k1gNnSc6	Macao
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Britská	britský	k2eAgFnSc1d1	britská
nadvláda	nadvláda	k1gFnSc1	nadvláda
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1699	[number]	k4	1699
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
obchodovat	obchodovat	k5eAaImF	obchodovat
britská	britský	k2eAgFnSc1d1	britská
Východoindická	východoindický	k2eAgFnSc1d1	Východoindická
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velká	velký	k2eAgFnSc1d1	velká
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
čínském	čínský	k2eAgNnSc6d1	čínské
zboží	zboží	k1gNnSc6	zboží
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
po	po	k7c6	po
evropském	evropský	k2eAgNnSc6d1	Evropské
jen	jen	k6eAd1	jen
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
Číňané	Číňan	k1gMnPc1	Číňan
vyžadovali	vyžadovat	k5eAaImAgMnP	vyžadovat
po	po	k7c6	po
Evropanech	Evropan	k1gMnPc6	Evropan
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
zboží	zboží	k1gNnSc4	zboží
(	(	kIx(	(
<g/>
hedvábí	hedvábí	k1gNnSc1	hedvábí
<g/>
,	,	kIx,	,
porcelán	porcelán	k1gInSc1	porcelán
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc1	čaj
<g/>
)	)	kIx)	)
platby	platba	k1gFnPc1	platba
ve	v	k7c6	v
stříbře	stříbro	k1gNnSc6	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
tak	tak	k6eAd1	tak
začali	začít	k5eAaPmAgMnP	začít
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
nejprve	nejprve	k6eAd1	nejprve
dovážet	dovážet	k5eAaImF	dovážet
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
pašovat	pašovat	k5eAaImF	pašovat
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
opium	opium	k1gNnSc1	opium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
produkovali	produkovat	k5eAaImAgMnP	produkovat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
koloniích	kolonie	k1gFnPc6	kolonie
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Drogově	drogově	k6eAd1	drogově
závislých	závislý	k2eAgInPc2d1	závislý
na	na	k7c6	na
opiu	opium	k1gNnSc6	opium
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
rapidně	rapidně	k6eAd1	rapidně
přibývalo	přibývat	k5eAaImAgNnS	přibývat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
již	již	k9	již
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
<g/>
,	,	kIx,	,
komisař	komisař	k1gMnSc1	komisař
Lin	Lina	k1gFnPc2	Lina
z	z	k7c2	z
pověření	pověření	k1gNnSc2	pověření
čínského	čínský	k2eAgMnSc2d1	čínský
císaře	císař	k1gMnSc2	císař
rázně	rázně	k6eAd1	rázně
zakročil	zakročit	k5eAaPmAgInS	zakročit
a	a	k8xC	a
nechal	nechat	k5eAaPmAgInS	nechat
zatknout	zatknout	k5eAaPmF	zatknout
1	[number]	k4	1
600	[number]	k4	600
čínských	čínský	k2eAgMnPc2d1	čínský
překupníků	překupník	k1gMnPc2	překupník
a	a	k8xC	a
zabavit	zabavit	k5eAaPmF	zabavit
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
přes	přes	k7c4	přes
1	[number]	k4	1
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
opia	opium	k1gNnSc2	opium
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
opatření	opatření	k1gNnPc1	opatření
dala	dát	k5eAaPmAgNnP	dát
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
první	první	k4xOgFnSc3	první
opiové	opiový	k2eAgFnSc3d1	opiová
válce	válka	k1gFnSc3	válka
–	–	k?	–
Britové	Brit	k1gMnPc1	Brit
vyslali	vyslat	k5eAaPmAgMnP	vyslat
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
válečné	válečný	k2eAgNnSc4d1	válečné
loďstvo	loďstvo	k1gNnSc4	loďstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
několik	několik	k4yIc4	několik
pobřežních	pobřežní	k2eAgNnPc2d1	pobřežní
měst	město	k1gNnPc2	město
a	a	k8xC	a
donutilo	donutit	k5eAaPmAgNnS	donutit
čínskou	čínský	k2eAgFnSc4d1	čínská
stranu	strana	k1gFnSc4	strana
podepsat	podepsat	k5eAaPmF	podepsat
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1842	[number]	k4	1842
Nankingskou	nankingský	k2eAgFnSc4d1	Nankingská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
museli	muset	k5eAaImAgMnP	muset
Číňané	Číňan	k1gMnPc1	Číňan
postoupit	postoupit	k5eAaPmF	postoupit
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
Hongkong	Hongkong	k1gInSc4	Hongkong
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
<g />
.	.	kIx.	.
</s>
<s>
omezení	omezení	k1gNnSc4	omezení
otevřít	otevřít	k5eAaPmF	otevřít
5	[number]	k4	5
svých	svůj	k3xOyFgInPc2	svůj
přístavů	přístav	k1gInPc2	přístav
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
obchodním	obchodní	k2eAgFnPc3d1	obchodní
společnostem	společnost	k1gFnPc3	společnost
<g/>
,	,	kIx,	,
povolit	povolit	k5eAaPmF	povolit
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
opiem	opium	k1gNnSc7	opium
<g/>
,	,	kIx,	,
zaplatit	zaplatit	k5eAaPmF	zaplatit
Britům	Brit	k1gMnPc3	Brit
zničené	zničený	k2eAgNnSc1d1	zničené
opium	opium	k1gNnSc1	opium
(	(	kIx(	(
<g/>
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
stříbrných	stříbrný	k2eAgMnPc2d1	stříbrný
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
a	a	k8xC	a
válečné	válečný	k2eAgInPc1d1	válečný
výdaje	výdaj	k1gInPc1	výdaj
(	(	kIx(	(
<g/>
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
stříbrných	stříbrný	k2eAgMnPc2d1	stříbrný
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
a	a	k8xC	a
odpustit	odpustit	k5eAaPmF	odpustit
britským	britský	k2eAgMnPc3d1	britský
obchodníkům	obchodník	k1gMnPc3	obchodník
dluhy	dluh	k1gInPc4	dluh
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
3	[number]	k4	3
milionů	milion	k4xCgInPc2	milion
stříbrných	stříbrný	k2eAgMnPc2d1	stříbrný
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poloostrov	poloostrov	k1gInSc1	poloostrov
Kowloon	Kowloona	k1gFnPc2	Kowloona
byl	být	k5eAaImAgInS	být
postoupen	postoupen	k2eAgInSc1d1	postoupen
Británii	Británie	k1gFnSc4	Británie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
podle	podle	k7c2	podle
Pekingské	pekingský	k2eAgFnSc2d1	Pekingská
smlouvy	smlouva	k1gFnSc2	smlouva
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
opiové	opiový	k2eAgFnSc6d1	opiová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Přiléhající	přiléhající	k2eAgNnSc4d1	přiléhající
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
New	New	k1gMnSc1	New
Territories	Territories	k1gMnSc1	Territories
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
New	New	k1gFnSc2	New
Kowloon	Kowloona	k1gFnPc2	Kowloona
a	a	k8xC	a
ostrova	ostrov	k1gInSc2	ostrov
Lantau	Lantaus	k1gInSc2	Lantaus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
pronajaty	pronajmout	k5eAaPmNgInP	pronajmout
Británii	Británie	k1gFnSc6	Británie
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
99	[number]	k4	99
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
počínaje	počínaje	k7c7	počínaje
1	[number]	k4	1
<g/>
.	.	kIx.	.
červencem	červenec	k1gInSc7	červenec
1898	[number]	k4	1898
a	a	k8xC	a
konče	konče	k7c7	konče
30	[number]	k4	30
<g/>
.	.	kIx.	.
červnem	červen	k1gInSc7	červen
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přechod	přechod	k1gInSc1	přechod
pod	pod	k7c4	pod
čínskou	čínský	k2eAgFnSc4d1	čínská
správu	správa	k1gFnSc4	správa
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
pronájmu	pronájem	k1gInSc6	pronájem
"	"	kIx"	"
<g/>
New	New	k1gMnSc1	New
Territories	Territories	k1gMnSc1	Territories
<g/>
"	"	kIx"	"
brzy	brzy	k6eAd1	brzy
vyprší	vypršet	k5eAaPmIp3nS	vypršet
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Británie	Británie	k1gFnSc1	Británie
vyjednala	vyjednat	k5eAaPmAgFnS	vyjednat
vrácení	vrácení	k1gNnSc3	vrácení
Hongkongu	Hongkong	k1gInSc2	Hongkong
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Čínsko-britské	čínskoritský	k2eAgNnSc1d1	čínsko-britský
společné	společný	k2eAgNnSc1d1	společné
prohlášení	prohlášení	k1gNnSc1	prohlášení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
Čínou	Čína	k1gFnSc7	Čína
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
Hongkongu	Hongkong	k1gInSc2	Hongkong
pod	pod	k7c7	pod
britskou	britský	k2eAgFnSc7d1	britská
koloniální	koloniální	k2eAgFnSc7d1	koloniální
vládou	vláda	k1gFnSc7	vláda
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
stalo	stát	k5eAaPmAgNnS	stát
"	"	kIx"	"
<g/>
Hongkongem	Hongkong	k1gInSc7	Hongkong
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
administrativní	administrativní	k2eAgFnSc7d1	administrativní
zónou	zóna	k1gFnSc7	zóna
ČLR	ČLR	kA	ČLR
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
společném	společný	k2eAgNnSc6d1	společné
prohlášení	prohlášení	k1gNnSc6	prohlášení
se	se	k3xPyFc4	se
ČLR	ČLR	kA	ČLR
zavázala	zavázat	k5eAaPmAgFnS	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
zásady	zásada	k1gFnSc2	zásada
"	"	kIx"	"
<g/>
jedna	jeden	k4xCgFnSc1	jeden
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
systémy	systém	k1gInPc1	systém
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
čínský	čínský	k2eAgMnSc1d1	čínský
politik	politik	k1gMnSc1	politik
Teng	Tenga	k1gFnPc2	Tenga
Siao-pching	Siaoching	k1gInSc1	Siao-pching
<g/>
,	,	kIx,	,
socialistický	socialistický	k2eAgInSc1d1	socialistický
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
systém	systém	k1gInSc1	systém
Číny	Čína	k1gFnSc2	Čína
nebude	být	k5eNaImBp3nS	být
v	v	k7c6	v
Hongkongu	Hongkong	k1gInSc6	Hongkong
praktikován	praktikovat	k5eAaImNgInS	praktikovat
a	a	k8xC	a
že	že	k8xS	že
současný	současný	k2eAgInSc1d1	současný
kapitalistický	kapitalistický	k2eAgInSc1d1	kapitalistický
systém	systém	k1gInSc1	systém
a	a	k8xC	a
životní	životní	k2eAgInSc1d1	životní
styl	styl	k1gInSc1	styl
zůstane	zůstat	k5eAaPmIp3nS	zůstat
nezměněn	změnit	k5eNaPmNgInS	změnit
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2047	[number]	k4	2047
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
vešla	vejít	k5eAaPmAgFnS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
když	když	k8xS	když
poslední	poslední	k2eAgMnSc1d1	poslední
britský	britský	k2eAgMnSc1d1	britský
guvernér	guvernér	k1gMnSc1	guvernér
Chris	Chris	k1gFnSc2	Chris
Patten	Patten	k2eAgMnSc1d1	Patten
odplul	odplout	k5eAaPmAgMnS	odplout
na	na	k7c6	na
Královské	královský	k2eAgFnSc6d1	královská
jachtě	jachta	k1gFnSc6	jachta
Britannia	Britannium	k1gNnSc2	Britannium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejistota	nejistota	k1gFnSc1	nejistota
během	během	k7c2	během
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
silný	silný	k2eAgInSc4d1	silný
nátlak	nátlak	k1gInSc4	nátlak
ČLR	ČLR	kA	ČLR
způsobily	způsobit	k5eAaPmAgFnP	způsobit
velký	velký	k2eAgInSc4d1	velký
odliv	odliv	k1gInSc4	odliv
investičního	investiční	k2eAgInSc2d1	investiční
kapitálu	kapitál	k1gInSc2	kapitál
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
jeden	jeden	k4xCgInSc1	jeden
milion	milion	k4xCgInSc1	milion
USD	USD	kA	USD
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pád	pád	k1gInSc1	pád
hongkongského	hongkongský	k2eAgInSc2d1	hongkongský
dolaru	dolar	k1gInSc2	dolar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
nastala	nastat	k5eAaPmAgFnS	nastat
velká	velký	k2eAgFnSc1d1	velká
vlna	vlna	k1gFnSc1	vlna
emigrace	emigrace	k1gFnSc2	emigrace
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
emigrace	emigrace	k1gFnSc1	emigrace
především	především	k6eAd1	především
majetných	majetný	k2eAgMnPc2d1	majetný
(	(	kIx(	(
<g/>
milionáři	milionář	k1gMnPc7	milionář
i	i	k8xC	i
miliardáři	miliardář	k1gMnPc7	miliardář
<g/>
)	)	kIx)	)
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
tchieanmenském	tchieanmenský	k2eAgInSc6d1	tchieanmenský
masakru	masakr	k1gInSc6	masakr
ještě	ještě	k6eAd1	ještě
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
statisících	statisíce	k1gInPc6	statisíce
(	(	kIx(	(
<g/>
45	[number]	k4	45
000-70	[number]	k4	000-70
000	[number]	k4	000
emigrantů	emigrant	k1gMnPc2	emigrant
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1984-1997	[number]	k4	1984-1997
opustilo	opustit	k5eAaPmAgNnS	opustit
Hongkong	Hongkong	k1gInSc4	Hongkong
údajně	údajně	k6eAd1	údajně
500	[number]	k4	500
000	[number]	k4	000
–	–	k?	–
700	[number]	k4	700
000	[number]	k4	000
občanů	občan	k1gMnPc2	občan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Pod	pod	k7c7	pod
čínskou	čínský	k2eAgFnSc7d1	čínská
správou	správa	k1gFnSc7	správa
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
První	první	k4xOgFnSc1	první
čínská	čínský	k2eAgFnSc1d1	čínská
dekáda	dekáda	k1gFnSc1	dekáda
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
====	====	k?	====
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
prognózy	prognóza	k1gFnPc1	prognóza
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	se	k3xPyFc4	se
budoucností	budoucnost	k1gFnSc7	budoucnost
Hongkongu	Hongkong	k1gInSc2	Hongkong
byly	být	k5eAaImAgFnP	být
vesměs	vesměs	k6eAd1	vesměs
pesimistické	pesimistický	k2eAgFnPc1d1	pesimistická
(	(	kIx(	(
<g/>
obávaly	obávat	k5eAaImAgFnP	obávat
se	se	k3xPyFc4	se
dopadu	dopad	k1gInSc6	dopad
přechodu	přechod	k1gInSc2	přechod
Hongkongu	Hongkong	k1gInSc2	Hongkong
pod	pod	k7c4	pod
čínskou	čínský	k2eAgFnSc4d1	čínská
samosprávu	samospráva	k1gFnSc4	samospráva
na	na	k7c4	na
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
<g/>
,	,	kIx,	,
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
a	a	k8xC	a
politickou	politický	k2eAgFnSc4d1	politická
situaci	situace	k1gFnSc4	situace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
katastrofické	katastrofický	k2eAgInPc1d1	katastrofický
scénáře	scénář	k1gInPc1	scénář
se	se	k3xPyFc4	se
nenaplnily	naplnit	k5eNaPmAgInP	naplnit
–	–	k?	–
Hongkongu	Hongkong	k1gInSc2	Hongkong
se	se	k3xPyFc4	se
i	i	k9	i
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
vlády	vláda	k1gFnSc2	vláda
stále	stále	k6eAd1	stále
dobře	dobře	k6eAd1	dobře
dařilo	dařit	k5eAaImAgNnS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Určité	určitý	k2eAgFnPc1d1	určitá
změny	změna	k1gFnPc1	změna
ale	ale	k9	ale
nastaly	nastat	k5eAaPmAgFnP	nastat
–	–	k?	–
například	například	k6eAd1	například
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
pevninské	pevninský	k2eAgFnSc2d1	pevninská
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
<g/>
Problémy	problém	k1gInPc4	problém
ovšem	ovšem	k9	ovšem
představuje	představovat	k5eAaImIp3nS	představovat
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
systém	systém	k1gInSc4	systém
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
demokratický	demokratický	k2eAgInSc1d1	demokratický
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
s	s	k7c7	s
dosavadním	dosavadní	k2eAgInSc7d1	dosavadní
politickým	politický	k2eAgInSc7d1	politický
systémem	systém	k1gInSc7	systém
vyústila	vyústit	k5eAaPmAgFnS	vyústit
například	například	k6eAd1	například
v	v	k7c4	v
protesty	protest	k1gInPc4	protest
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInPc1d3	veliký
masové	masový	k2eAgInPc1d1	masový
protesty	protest	k1gInPc1	protest
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Občanské	občanský	k2eAgInPc1d1	občanský
nepokoje	nepokoj	k1gInPc1	nepokoj
2014	[number]	k4	2014
a	a	k8xC	a
2019	[number]	k4	2019
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
v	v	k7c4	v
Hong	Hong	k1gInSc4	Hong
Kongu	Kongo	k1gNnSc6	Kongo
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
protesty	protest	k1gInPc4	protest
proti	proti	k7c3	proti
tzv.	tzv.	kA	tzv.
volební	volební	k2eAgFnSc6d1	volební
reformě	reforma	k1gFnSc6	reforma
schválené	schválený	k2eAgNnSc1d1	schválené
centrální	centrální	k2eAgFnSc7d1	centrální
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
také	také	k9	také
jako	jako	k9	jako
,,	,,	k?	,,
<g/>
Deštníková	deštníkový	k2eAgFnSc1d1	deštníková
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
reformy	reforma	k1gFnSc2	reforma
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
kandidáti	kandidát	k1gMnPc1	kandidát
do	do	k7c2	do
místních	místní	k2eAgFnPc2d1	místní
voleb	volba	k1gFnPc2	volba
schvalováni	schvalovat	k5eAaImNgMnP	schvalovat
komisí	komise	k1gFnSc7	komise
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
čínských	čínský	k2eAgInPc2d1	čínský
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
začali	začít	k5eAaPmAgMnP	začít
protestující	protestující	k2eAgMnPc1d1	protestující
požadovat	požadovat	k5eAaImF	požadovat
rezignaci	rezignace	k1gFnSc4	rezignace
místní	místní	k2eAgFnSc2d1	místní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vznikala	vznikat	k5eAaImAgFnS	vznikat
protestní	protestní	k2eAgNnSc4d1	protestní
hnutí	hnutí	k1gNnSc4	hnutí
jako	jako	k8xC	jako
,,	,,	k?	,,
<g/>
Occupy	Occupa	k1gFnSc2	Occupa
Central	Central	k1gMnSc1	Central
with	with	k1gMnSc1	with
Love	lov	k1gInSc5	lov
and	and	k?	and
Peace	Peaec	k1gInSc2	Peaec
<g/>
"	"	kIx"	"
či	či	k8xC	či
,,	,,	k?	,,
<g/>
Scholarism	Scholarism	k1gInSc1	Scholarism
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
protesty	protest	k1gInPc7	protest
pomalu	pomalu	k6eAd1	pomalu
utichaly	utichat	k5eAaImAgInP	utichat
<g/>
,	,	kIx,	,
demonstranti	demonstrant	k1gMnPc1	demonstrant
byli	být	k5eAaImAgMnP	být
definitivně	definitivně	k6eAd1	definitivně
rozehnáni	rozehnán	k2eAgMnPc1d1	rozehnán
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
CY	CY	kA	CY
Leung	Leung	k1gMnSc1	Leung
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
rezignaci	rezignace	k1gFnSc4	rezignace
demonstranti	demonstrant	k1gMnPc1	demonstrant
požadovali	požadovat	k5eAaImAgMnP	požadovat
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
udržel	udržet	k5eAaPmAgMnS	udržet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
protestů	protest	k1gInPc2	protest
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
také	také	k9	také
kritika	kritika	k1gFnSc1	kritika
hongkongské	hongkongský	k2eAgFnSc2d1	hongkongská
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
brutální	brutální	k2eAgFnSc1d1	brutální
(	(	kIx(	(
<g/>
např.	např.	kA	např.
používání	používání	k1gNnSc1	používání
slzného	slzný	k2eAgInSc2d1	slzný
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc4	násilí
při	při	k7c6	při
zatýkání	zatýkání	k1gNnSc6	zatýkání
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
to	ten	k3xDgNnSc4	ten
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
<g/>
Doposud	doposud	k6eAd1	doposud
nejmasovější	masový	k2eAgInPc4d3	nejmasovější
protesty	protest	k1gInPc4	protest
proti	proti	k7c3	proti
hongkongské	hongkongský	k2eAgFnSc3d1	hongkongská
vládě	vláda	k1gFnSc3	vláda
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
jeden	jeden	k4xCgInSc4	jeden
milion	milion	k4xCgInSc4	milion
demonstrantů	demonstrant	k1gMnPc2	demonstrant
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
zaplňoval	zaplňovat	k5eAaImAgMnS	zaplňovat
opakovaně	opakovaně	k6eAd1	opakovaně
poměrně	poměrně	k6eAd1	poměrně
úzkou	úzký	k2eAgFnSc4d1	úzká
ulici	ulice	k1gFnSc4	ulice
mezi	mezi	k7c7	mezi
mrakodrapy	mrakodrap	k1gInPc7	mrakodrap
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
připravovanému	připravovaný	k2eAgInSc3d1	připravovaný
zákonu	zákon	k1gInSc3	zákon
<g/>
,	,	kIx,	,
umožňujícímu	umožňující	k2eAgMnSc3d1	umožňující
vládě	vláda	k1gFnSc3	vláda
vydávat	vydávat	k5eAaImF	vydávat
určité	určitý	k2eAgFnPc4d1	určitá
obviněné	obviněný	k2eAgFnPc4d1	obviněná
osoby	osoba	k1gFnPc4	osoba
úřadům	úřad	k1gInPc3	úřad
pevninské	pevninský	k2eAgFnSc2d1	pevninská
Číny	Čína	k1gFnSc2	Čína
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
trestnímu	trestní	k2eAgNnSc3d1	trestní
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
šéfka	šéfka	k1gFnSc1	šéfka
hongkongské	hongkongský	k2eAgFnSc2d1	hongkongská
samosprávy	samospráva	k1gFnSc2	samospráva
tento	tento	k3xDgInSc4	tento
zákon	zákon	k1gInSc4	zákon
předběžně	předběžně	k6eAd1	předběžně
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
z	z	k7c2	z
projednávání	projednávání	k1gNnSc2	projednávání
<g/>
,	,	kIx,	,
masové	masový	k2eAgFnPc1d1	masová
demonstrace	demonstrace	k1gFnPc1	demonstrace
neustaly	ustat	k5eNaPmAgFnP	ustat
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
požadují	požadovat	k5eAaImIp3nP	požadovat
její	její	k3xOp3gNnSc4	její
odstoupení	odstoupení	k1gNnSc4	odstoupení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazyk	jazyk	k1gInSc1	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
Hongkongu	Hongkong	k1gInSc2	Hongkong
je	být	k5eAaImIp3nS	být
standardní	standardní	k2eAgFnSc1d1	standardní
čínština	čínština	k1gFnSc1	čínština
a	a	k8xC	a
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
mluveným	mluvený	k2eAgInSc7d1	mluvený
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
však	však	k9	však
kantonština	kantonština	k1gFnSc1	kantonština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kantonštině	kantonština	k1gFnSc6	kantonština
běžně	běžně	k6eAd1	běžně
vysílají	vysílat	k5eAaImIp3nP	vysílat
hongkongské	hongkongský	k2eAgInPc4d1	hongkongský
televizní	televizní	k2eAgInPc4d1	televizní
kanály	kanál	k1gInPc4	kanál
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
např.	např.	kA	např.
hlášení	hlášení	k1gNnSc4	hlášení
v	v	k7c6	v
metru	metro	k1gNnSc6	metro
jsou	být	k5eAaImIp3nP	být
trojjazyčná	trojjazyčný	k2eAgNnPc1d1	trojjazyčné
<g/>
:	:	kIx,	:
kantonsky	kantonsky	k6eAd1	kantonsky
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
<s>
Nápisy	nápis	k1gInPc1	nápis
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
dvojí	dvojí	k4xRgInPc1	dvojí
čínsko-anglické	čínskonglický	k2eAgInPc1d1	čínsko-anglický
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tytéž	týž	k3xTgInPc4	týž
tradiční	tradiční	k2eAgInPc4d1	tradiční
čínské	čínský	k2eAgInPc4d1	čínský
znaky	znak	k1gInPc4	znak
lze	lze	k6eAd1	lze
přečíst	přečíst	k5eAaPmF	přečíst
kantonsky	kantonsky	k6eAd1	kantonsky
i	i	k9	i
mandarínsky	mandarínsky	k6eAd1	mandarínsky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
čínského	čínský	k2eAgNnSc2d1	čínské
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
v	v	k7c6	v
Hongkongu	Hongkong	k1gInSc6	Hongkong
stále	stále	k6eAd1	stále
většinou	většinou	k6eAd1	většinou
používají	používat	k5eAaImIp3nP	používat
tradiční	tradiční	k2eAgInPc4d1	tradiční
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Hongkong	Hongkong	k1gInSc1	Hongkong
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejliberálnějších	liberální	k2eAgFnPc2d3	nejliberálnější
ekonomik	ekonomika	k1gFnPc2	ekonomika
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
světových	světový	k2eAgNnPc2d1	světové
středisek	středisko	k1gNnPc2	středisko
bankovnictví	bankovnictví	k1gNnSc2	bankovnictví
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc1d1	zdejší
přístav	přístav	k1gInSc1	přístav
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvytíženějším	vytížený	k2eAgInPc3d3	nejvytíženější
přístavům	přístav	k1gInPc3	přístav
kontejnerové	kontejnerový	k2eAgFnSc2d1	kontejnerová
námořní	námořní	k2eAgFnSc2d1	námořní
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Obchodní	obchodní	k2eAgFnSc7d1	obchodní
čtvrtí	čtvrt	k1gFnSc7	čtvrt
je	být	k5eAaImIp3nS	být
Kowloon	Kowloon	k1gInSc1	Kowloon
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
trhy	trh	k1gInPc4	trh
patří	patřit	k5eAaImIp3nS	patřit
nefritový	nefritový	k2eAgMnSc1d1	nefritový
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
smlouvá	smlouvat	k5eAaImIp3nS	smlouvat
i	i	k9	i
o	o	k7c6	o
ceně	cena	k1gFnSc6	cena
polévky	polévka	k1gFnSc2	polévka
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
turistickými	turistický	k2eAgFnPc7d1	turistická
atrakcemi	atrakce	k1gFnPc7	atrakce
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
chrám	chrám	k1gInSc4	chrám
Wong	Wonga	k1gFnPc2	Wonga
Tai	Tai	k1gFnPc2	Tai
Sin	sin	kA	sin
nebo	nebo	k8xC	nebo
Ptačí	ptačí	k2eAgInSc4d1	ptačí
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
výběžku	výběžek	k1gInSc6	výběžek
Kowloonu	Kowloon	k1gInSc2	Kowloon
ležící	ležící	k2eAgFnSc1d1	ležící
rybářská	rybářský	k2eAgFnSc1d1	rybářská
osada	osada	k1gFnSc1	osada
Lei	lei	k1gInSc2	lei
Yeu	Yeu	k1gFnSc2	Yeu
Mun	muna	k1gFnPc2	muna
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
nádech	nádech	k1gInSc4	nádech
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hongkongský	hongkongský	k2eAgInSc1d1	hongkongský
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
oázou	oáza	k1gFnSc7	oáza
klidu	klid	k1gInSc2	klid
uprostřed	uprostřed	k7c2	uprostřed
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
nejen	nejen	k6eAd1	nejen
zelené	zelený	k2eAgFnPc4d1	zelená
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
jezírka	jezírko	k1gNnPc1	jezírko
s	s	k7c7	s
vodními	vodní	k2eAgMnPc7d1	vodní
ptáky	pták	k1gMnPc7	pták
<g/>
,	,	kIx,	,
fontány	fontána	k1gFnPc4	fontána
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnPc4d1	přírodní
restaurace	restaurace	k1gFnPc4	restaurace
a	a	k8xC	a
muzeum	muzeum	k1gNnSc4	muzeum
čajů	čaj	k1gInPc2	čaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Victoria	Victorium	k1gNnPc1	Victorium
Peak	Peako	k1gNnPc2	Peako
je	být	k5eAaImIp3nS	být
554	[number]	k4	554
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
nádherný	nádherný	k2eAgInSc1d1	nádherný
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
zátoky	zátok	k1gInPc4	zátok
a	a	k8xC	a
ostrůvky	ostrůvek	k1gInPc4	ostrůvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hongkong	Hongkong	k1gInSc1	Hongkong
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
přilehlými	přilehlý	k2eAgInPc7d1	přilehlý
ostrovy	ostrov	k1gInPc7	ostrov
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
trajektů	trajekt	k1gInPc2	trajekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nádherné	nádherný	k2eAgFnPc1d1	nádherná
pláže	pláž	k1gFnPc1	pláž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Lamma	Lammum	k1gNnSc2	Lammum
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
nesmějí	smát	k5eNaImIp3nP	smát
ani	ani	k8xC	ani
auta	auto	k1gNnPc4	auto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
Tai	Tai	k1gMnSc1	Tai
Mo	Mo	k1gMnSc1	Mo
Shan	Shan	k1gMnSc1	Shan
(	(	kIx(	(
<g/>
957	[number]	k4	957
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
nepřístupný	přístupný	k2eNgInSc1d1	nepřístupný
vrchol	vrchol	k1gInSc1	vrchol
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
meteorologický	meteorologický	k2eAgInSc1d1	meteorologický
radar	radar	k1gInSc1	radar
a	a	k8xC	a
také	také	k9	také
základna	základna	k1gFnSc1	základna
čínské	čínský	k2eAgFnSc2d1	čínská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
přístupnou	přístupný	k2eAgFnSc7d1	přístupná
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Lantau	Lantaa	k1gFnSc4	Lantaa
Peak	Peaka	k1gFnPc2	Peaka
(	(	kIx(	(
<g/>
934	[number]	k4	934
m	m	kA	m
<g/>
)	)	kIx)	)
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Lantau	Lantaus	k1gInSc2	Lantaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hongkongská	hongkongský	k2eAgFnSc1d1	hongkongská
vlajka	vlajka	k1gFnSc1	vlajka
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
čínské	čínský	k2eAgNnSc1d1	čínské
jméno	jméno	k1gNnSc1	jméno
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Hongkong	Hongkong	k1gInSc1	Hongkong
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hongkong	Hongkong	k1gInSc4	Hongkong
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Hongkong	Hongkong	k1gInSc1	Hongkong
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Hongkong	Hongkong	k1gInSc1	Hongkong
-	-	kIx~	-
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
</s>
</p>
<p>
<s>
Videozáznam	videozáznam	k1gInSc1	videozáznam
z	z	k7c2	z
demonstrace	demonstrace	k1gFnSc2	demonstrace
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Hongkong	Hongkong	k1gInSc1	Hongkong
–	–	k?	–
kam	kam	k6eAd1	kam
v	v	k7c6	v
Hongkongu	Hongkong	k1gInSc6	Hongkong
zajít	zajít	k5eAaPmF	zajít
a	a	k8xC	a
co	co	k3yQnSc4	co
musíte	muset	k5eAaImIp2nP	muset
vidět	vidět	k5eAaImF	vidět
</s>
</p>
