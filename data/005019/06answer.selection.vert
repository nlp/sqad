<s>
Mezi	mezi	k7c7	mezi
korejštinou	korejština	k1gFnSc7	korejština
používanou	používaný	k2eAgFnSc7d1	používaná
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
existují	existovat	k5eAaImIp3nP	existovat
určité	určitý	k2eAgInPc4d1	určitý
rozdíly	rozdíl	k1gInPc4	rozdíl
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
<g/>
,	,	kIx,	,
hláskování	hláskování	k1gNnSc6	hláskování
<g/>
,	,	kIx,	,
gramatice	gramatika	k1gFnSc6	gramatika
a	a	k8xC	a
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
<g/>
.	.	kIx.	.
</s>
