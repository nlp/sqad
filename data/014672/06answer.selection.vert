<s>
Skupenství	skupenství	k1gNnSc1
neboli	neboli	k8xC
stav	stav	k1gInSc1
je	být	k5eAaImIp3nS
konkrétní	konkrétní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
látky	látka	k1gFnSc2
<g/>
,	,	kIx,
charakterizovaná	charakterizovaný	k2eAgFnSc1d1
především	především	k6eAd1
uspořádáním	uspořádání	k1gNnSc7
částic	částice	k1gFnPc2
v	v	k7c6
látce	látka	k1gFnSc6
a	a	k8xC
projevující	projevující	k2eAgMnSc1d1
se	se	k3xPyFc4
typickými	typický	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
<g/>
.	.	kIx.
</s>