<s>
Miki	Miki	k6eAd1	Miki
Ryvola	Ryvola	k1gFnSc1	Ryvola
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Mirko	Mirko	k1gMnSc1	Mirko
Ryvola	Ryvola	k1gFnSc1	Ryvola
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1942	[number]	k4	1942
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
trampský	trampský	k2eAgMnSc1d1	trampský
písničkář	písničkář	k1gMnSc1	písničkář
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
7	[number]	k4	7
let	léto	k1gNnPc2	léto
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
bratrovi	bratr	k1gMnSc3	bratr
Wabim	Wabim	k1gInSc4	Wabim
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
hrál	hrát	k5eAaImAgMnS	hrát
dlouho	dlouho	k6eAd1	dlouho
v	v	k7c6	v
trampské	trampský	k2eAgFnSc6d1	trampská
skupině	skupina	k1gFnSc6	skupina
Hoboes	Hoboesa	k1gFnPc2	Hoboesa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
společného	společný	k2eAgNnSc2d1	společné
působení	působení	k1gNnSc2	působení
složil	složit	k5eAaPmAgMnS	složit
a	a	k8xC	a
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
mnoho	mnoho	k4c4	mnoho
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
klasikou	klasika	k1gFnSc7	klasika
u	u	k7c2	u
táboráků	táborák	k1gInPc2	táborák
–	–	k?	–
například	například	k6eAd1	například
Bedna	bedna	k1gFnSc1	bedna
vod	voda	k1gFnPc2	voda
whisky	whisky	k1gFnSc2	whisky
<g/>
,	,	kIx,	,
Bál	bát	k5eAaImAgMnS	bát
v	v	k7c6	v
lapáku	lapák	k1gInSc6	lapák
<g/>
,	,	kIx,	,
Pane	Pan	k1gMnSc5	Pan
bože	bůh	k1gMnSc5	bůh
vod	voda	k1gFnPc2	voda
tý	tý	k0	tý
lásky	láska	k1gFnSc2	láska
zachraň	zachránit	k5eAaPmRp2nS	zachránit
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
Mrtvej	Mrtvej	k?	Mrtvej
vlak	vlak	k1gInSc1	vlak
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
buď	buď	k8xC	buď
sám	sám	k3xTgMnSc1	sám
nebo	nebo	k8xC	nebo
s	s	k7c7	s
členy	člen	k1gMnPc7	člen
skupiny	skupina	k1gFnSc2	skupina
Nezmaři	nezmar	k1gMnPc1	nezmar
<g/>
.	.	kIx.	.
</s>
<s>
Bedna	bedna	k1gFnSc1	bedna
vod	voda	k1gFnPc2	voda
whisky	whisky	k1gFnSc2	whisky
Mrtvej	Mrtvej	k?	Mrtvej
vlak	vlak	k1gInSc1	vlak
Trail	Trail	k1gInSc1	Trail
to	ten	k3xDgNnSc1	ten
Island	Island	k1gInSc1	Island
Jarní	jarní	k2eAgMnSc1d1	jarní
kurýr	kurýr	k1gMnSc1	kurýr
Dům	dům	k1gInSc1	dům
smutný	smutný	k2eAgInSc1d1	smutný
víly	víla	k1gFnSc2	víla
Pane	Pan	k1gMnSc5	Pan
bože	bůh	k1gMnSc5	bůh
vod	voda	k1gFnPc2	voda
tý	tý	k0	tý
lásky	láska	k1gFnSc2	láska
zachraň	zachránit	k5eAaPmRp2nS	zachránit
nás	my	k3xPp1nPc4	my
Bál	bát	k5eAaImAgInS	bát
v	v	k7c6	v
lapáku	lapák	k1gInSc6	lapák
Vrbový	vrbový	k2eAgMnSc1d1	vrbový
houštiny	houština	k1gFnPc1	houština
Poslední	poslední	k2eAgFnPc1d1	poslední
míle	míle	k1gFnSc2	míle
Země	zem	k1gFnSc2	zem
tří	tři	k4xCgInPc2	tři
sluncí	slunce	k1gNnPc2	slunce
Tunel	tunel	k1gInSc1	tunel
jménem	jméno	k1gNnSc7	jméno
Čas	čas	k1gInSc1	čas
Drátěný	drátěný	k2eAgInSc1d1	drátěný
ohrady	ohrada	k1gFnSc2	ohrada
Bodláky	bodlák	k1gInPc4	bodlák
ve	v	k7c6	v
vlasech	vlas	k1gInPc6	vlas
LP	LP	kA	LP
Miki	Mik	k1gFnSc2	Mik
Ryvola	Ryvola	k1gFnSc1	Ryvola
a	a	k8xC	a
Hoboes	Hoboes	k1gInSc1	Hoboes
<g/>
:	:	kIx,	:
Zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
znamení	znamení	k1gNnSc1	znamení
touha	touha	k1gFnSc1	touha
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
CD	CD	kA	CD
Bedna	bedna	k1gFnSc1	bedna
vod	voda	k1gFnPc2	voda
whisky	whisky	k1gFnSc2	whisky
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
1992	[number]	k4	1992
CD	CD	kA	CD
Miki	Mik	k1gFnSc2	Mik
Ryvola	Ryvola	k1gFnSc1	Ryvola
a	a	k8xC	a
Nezmaři	nezmar	k1gMnPc1	nezmar
<g/>
:	:	kIx,	:
Písně	píseň	k1gFnSc2	píseň
Wabiho	Wabi	k1gMnSc2	Wabi
a	a	k8xC	a
Mikiho	Miki	k1gMnSc2	Miki
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
2CD	[number]	k4	2CD
Hobousárny	Hobousárna	k1gFnSc2	Hobousárna
<g/>
,	,	kIx,	,
Písně	píseň	k1gFnSc2	píseň
bratří	bratr	k1gMnPc2	bratr
Ryvolů	Ryvol	k1gInPc2	Ryvol
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
55	[number]	k4	55
písní	píseň	k1gFnPc2	píseň
CD	CD	kA	CD
Miki	Mike	k1gFnSc4	Mike
Ryvola	Ryvola	k1gFnSc1	Ryvola
&	&	k?	&
Orchestr	orchestr	k1gInSc1	orchestr
–	–	k?	–
Tunel	tunel	k1gInSc1	tunel
jménem	jméno	k1gNnSc7	jméno
čas	čas	k1gInSc1	čas
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Good	Good	k1gMnSc1	Good
Day	Day	k1gMnSc1	Day
Records	Records	k1gInSc4	Records
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
7	[number]	k4	7
písní	píseň	k1gFnPc2	píseň
ve	v	k7c6	v
swingovém	swingový	k2eAgNnSc6d1	swingové
aranžmá	aranžmá	k1gNnSc6	aranžmá
1991	[number]	k4	1991
Tom	Tom	k1gMnSc1	Tom
Kečup	kečup	k1gInSc1	kečup
&	&	k?	&
pes	pes	k1gMnSc1	pes
Vorčestr	Vorčestr	k1gInSc1	Vorčestr
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
PASEKA	paseka	k1gFnSc1	paseka
ISBN	ISBN	kA	ISBN
80-85192-07-1	[number]	k4	80-85192-07-1
1995	[number]	k4	1995
Bedna	bedna	k1gFnSc1	bedna
vod	voda	k1gFnPc2	voda
whisky	whisky	k1gFnSc2	whisky
(	(	kIx(	(
<g/>
zpěvník	zpěvník	k1gInSc1	zpěvník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
KONVOJ	konvoj	k1gInSc1	konvoj
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
M-706501-05-5	M-706501-05-5	k1gFnSc1	M-706501-05-5
2008	[number]	k4	2008
Miki	Miki	k1gNnPc2	Miki
Ryvola	Ryvola	k1gFnSc1	Ryvola
<g/>
,	,	kIx,	,
LISTÍ	listí	k1gNnSc1	listí
aneb	aneb	k?	aneb
CO	co	k8xS	co
ZBEJVÁ	ZBEJVÁ	kA	ZBEJVÁ
vydalo	vydat	k5eAaPmAgNnS	vydat
o.s.	o.s.	k?	o.s.
AVALON	AVALON	kA	AVALON
jako	jako	k8xC	jako
zájmový	zájmový	k2eAgInSc4d1	zájmový
náklad	náklad	k1gInSc4	náklad
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Miki	Mik	k1gFnSc2	Mik
Ryvola	Ryvola	k1gFnSc1	Ryvola
Stránky	stránka	k1gFnSc2	stránka
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
Wabimu	Wabimo	k1gNnSc3	Wabimo
a	a	k8xC	a
Mikimu	Mikimo	k1gNnSc3	Mikimo
</s>
