<s>
Skylab	Skylab	k1gInSc1	Skylab
(	(	kIx(	(
<g/>
nebeská	nebeský	k2eAgFnSc1d1	nebeská
laboratoř	laboratoř	k1gFnSc1	laboratoř
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
orbitální	orbitální	k2eAgFnPc4d1	orbitální
kosmické	kosmický	k2eAgFnPc4d1	kosmická
stanice	stanice	k1gFnPc4	stanice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kroužila	kroužit	k5eAaImAgFnS	kroužit
6	[number]	k4	6
let	léto	k1gNnPc2	léto
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
