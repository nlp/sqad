<s>
Skylab	Skylab	k1gInSc1	Skylab
(	(	kIx(	(
<g/>
nebeská	nebeský	k2eAgFnSc1d1	nebeská
laboratoř	laboratoř	k1gFnSc1	laboratoř
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
orbitální	orbitální	k2eAgFnPc4d1	orbitální
kosmické	kosmický	k2eAgFnPc4d1	kosmická
stanice	stanice	k1gFnPc4	stanice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kroužila	kroužit	k5eAaImAgFnS	kroužit
6	[number]	k4	6
let	léto	k1gNnPc2	léto
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1973	[number]	k4	1973
-	-	kIx~	-
1974	[number]	k4	1974
na	na	k7c6	na
ní	on	k3xPp3gFnSc7	on
pracovaly	pracovat	k5eAaImAgFnP	pracovat
tři	tři	k4xCgFnPc1	tři
trojčlenné	trojčlenný	k2eAgFnPc1d1	trojčlenná
posádky	posádka	k1gFnPc1	posádka
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
občané	občan	k1gMnPc1	občan
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Délkou	délka	k1gFnSc7	délka
pobytu	pobyt	k1gInSc2	pobyt
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
59	[number]	k4	59
a	a	k8xC	a
84	[number]	k4	84
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
kosmonautické	kosmonautický	k2eAgInPc4d1	kosmonautický
světové	světový	k2eAgInPc4d1	světový
rekordy	rekord	k1gInPc4	rekord
<g/>
,	,	kIx,	,
překonané	překonaný	k2eAgInPc4d1	překonaný
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
posádkou	posádka	k1gFnSc7	posádka
sovětského	sovětský	k2eAgInSc2d1	sovětský
Sojuzu	Sojuz	k1gInSc2	Sojuz
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
program	program	k1gInSc4	program
Apollo	Apollo	k1gNnSc4	Apollo
navazovat	navazovat	k5eAaImF	navazovat
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
program	program	k1gInSc4	program
Apollo	Apollo	k1gMnSc1	Apollo
Applications	Applications	k1gInSc1	Applications
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
však	však	k9	však
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
kongresem	kongres	k1gInSc7	kongres
USA	USA	kA	USA
seškrtán	seškrtán	k2eAgInSc4d1	seškrtán
rozpočet	rozpočet	k1gInSc4	rozpočet
NASA	NASA	kA	NASA
a	a	k8xC	a
projekt	projekt	k1gInSc1	projekt
předčasně	předčasně	k6eAd1	předčasně
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
redukci	redukce	k1gFnSc6	redukce
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
postavit	postavit	k5eAaPmF	postavit
a	a	k8xC	a
provozovat	provozovat	k5eAaImF	provozovat
orbitální	orbitální	k2eAgFnSc4d1	orbitální
stanici	stanice	k1gFnSc4	stanice
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Skylab	Skylab	k1gInSc1	Skylab
bude	být	k5eAaImBp3nS	být
vynesen	vynést	k5eAaPmNgInS	vynést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
raketou	raketa	k1gFnSc7	raketa
Saturn	Saturn	k1gMnSc1	Saturn
IB	IB	kA	IB
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
omezené	omezený	k2eAgFnSc3d1	omezená
nosnosti	nosnost	k1gFnSc3	nosnost
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
plánovalo	plánovat	k5eAaImAgNnS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stanice	stanice	k1gFnSc1	stanice
bude	být	k5eAaImBp3nS	být
vynesena	vynést	k5eAaPmNgFnS	vynést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
v	v	k7c6	v
neobyvatelném	obyvatelný	k2eNgInSc6d1	neobyvatelný
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
teprve	teprve	k6eAd1	teprve
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zastavení	zastavení	k1gNnSc6	zastavení
programu	program	k1gInSc2	program
Apollo	Apollo	k1gMnSc1	Apollo
však	však	k9	však
NASA	NASA	kA	NASA
měla	mít	k5eAaImAgFnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
nosič	nosič	k1gMnSc1	nosič
Saturn	Saturn	k1gMnSc1	Saturn
V	V	kA	V
o	o	k7c4	o
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnPc4d2	vyšší
nosnosti	nosnost	k1gFnPc4	nosnost
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
pozměněn	pozměněn	k2eAgInSc1d1	pozměněn
a	a	k8xC	a
orbitální	orbitální	k2eAgFnSc1d1	orbitální
stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
vynesena	vynést	k5eAaPmNgFnS	vynést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
v	v	k7c6	v
obyvatelném	obyvatelný	k2eAgInSc6d1	obyvatelný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
základ	základ	k1gInSc1	základ
stanice	stanice	k1gFnSc2	stanice
byl	být	k5eAaImAgInS	být
využit	využít	k5eAaPmNgInS	využít
upravený	upravený	k2eAgInSc1d1	upravený
modul	modul	k1gInSc1	modul
S-IVB	S-IVB	k1gMnSc2	S-IVB
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
normálně	normálně	k6eAd1	normálně
montován	montovat	k5eAaImNgMnS	montovat
buď	buď	k8xC	buď
jako	jako	k9	jako
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
nosiče	nosič	k1gMnSc4	nosič
Saturn	Saturn	k1gInSc4	Saturn
1B	[number]	k4	1B
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
3	[number]	k4	3
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
nosiče	nosič	k1gMnSc4	nosič
Saturn	Saturn	k1gInSc4	Saturn
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
77	[number]	k4	77
088	[number]	k4	088
kg	kg	kA	kg
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
až	až	k6eAd1	až
6,7	[number]	k4	6,7
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
i	i	k9	i
s	s	k7c7	s
připojenou	připojený	k2eAgFnSc7d1	připojená
dopravní	dopravní	k2eAgFnSc7d1	dopravní
lodí	loď	k1gFnSc7	loď
36,1	[number]	k4	36,1
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
několikanásobně	několikanásobně	k6eAd1	několikanásobně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
předchozí	předchozí	k2eAgFnPc4d1	předchozí
sovětské	sovětský	k2eAgFnPc4d1	sovětská
stanice	stanice	k1gFnPc4	stanice
Saljut	Saljut	k1gInSc1	Saljut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
spodním	spodní	k2eAgNnSc6d1	spodní
patře	patro	k1gNnSc6	patro
byla	být	k5eAaImAgFnS	být
vědecká	vědecký	k2eAgFnSc1d1	vědecká
laboratoř	laboratoř	k1gFnSc1	laboratoř
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
obytné	obytný	k2eAgFnPc4d1	obytná
místnosti	místnost	k1gFnPc4	místnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
ložnice	ložnice	k1gFnSc1	ložnice
byla	být	k5eAaImAgFnS	být
členěna	členit	k5eAaImNgFnS	členit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
kóje	kóje	k1gFnPc4	kóje
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
toaleta	toaleta	k1gFnSc1	toaleta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horním	horní	k2eAgNnSc6d1	horní
patře	patro	k1gNnSc6	patro
byly	být	k5eAaImAgFnP	být
skladovací	skladovací	k2eAgFnPc1d1	skladovací
prostory	prostora	k1gFnPc1	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
těchto	tento	k3xDgFnPc2	tento
prostor	prostora	k1gFnPc2	prostora
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
další	další	k2eAgInPc4d1	další
prostory	prostor	k1gInPc4	prostor
s	s	k7c7	s
nádržemi	nádrž	k1gFnPc7	nádrž
<g/>
,	,	kIx,	,
spojovací	spojovací	k2eAgInSc1d1	spojovací
adaptér	adaptér	k1gInSc1	adaptér
pro	pro	k7c4	pro
obslužné	obslužný	k2eAgFnPc4d1	obslužná
lodě	loď	k1gFnPc4	loď
s	s	k7c7	s
kosmonauty	kosmonaut	k1gMnPc7	kosmonaut
<g/>
,	,	kIx,	,
na	na	k7c6	na
výklopné	výklopný	k2eAgFnSc6d1	výklopná
konstrukci	konstrukce	k1gFnSc6	konstrukce
sluneční	sluneční	k2eAgInSc1d1	sluneční
dalekohled	dalekohled	k1gInSc1	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Zásobování	zásobování	k1gNnSc1	zásobování
energií	energie	k1gFnPc2	energie
obstarávaly	obstarávat	k5eAaImAgInP	obstarávat
sluneční	sluneční	k2eAgInPc1d1	sluneční
panely	panel	k1gInPc1	panel
<g/>
.	.	kIx.	.
</s>
<s>
Vynesení	vynesení	k1gNnSc1	vynesení
stanice	stanice	k1gFnSc2	stanice
Skylab	Skylaba	k1gFnPc2	Skylaba
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
let	let	k1gInSc1	let
Skylab	Skylab	k1gInSc1	Skylab
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1973	[number]	k4	1973
z	z	k7c2	z
Mysu	mys	k1gInSc2	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
dvoustupňovou	dvoustupňový	k2eAgFnSc7d1	dvoustupňová
modifikací	modifikace	k1gFnSc7	modifikace
nosiče	nosič	k1gInPc4	nosič
Saturn	Saturn	k1gInSc1	Saturn
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
vlastně	vlastně	k9	vlastně
na	na	k7c6	na
startovní	startovní	k2eAgFnSc6d1	startovní
rampě	rampa	k1gFnSc6	rampa
stála	stát	k5eAaImAgFnS	stát
normální	normální	k2eAgFnSc1d1	normální
trojstupňová	trojstupňový	k2eAgFnSc1d1	trojstupňová
raketa	raketa	k1gFnSc1	raketa
známá	známý	k2eAgFnSc1d1	známá
z	z	k7c2	z
letů	let	k1gInPc2	let
programu	program	k1gInSc2	program
Apollo	Apollo	k1gNnSc1	Apollo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
tentokrát	tentokrát	k6eAd1	tentokrát
nenesla	nést	k5eNaImAgFnS	nést
servisní	servisní	k2eAgFnSc1d1	servisní
a	a	k8xC	a
velitelský	velitelský	k2eAgInSc1d1	velitelský
modul	modul	k1gInSc1	modul
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
třetím	třetí	k4xOgInSc6	třetí
stupni	stupeň	k1gInSc6	stupeň
byla	být	k5eAaImAgFnS	být
místo	místo	k7c2	místo
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
nádrží	nádrž	k1gFnPc2	nádrž
s	s	k7c7	s
palivem	palivo	k1gNnSc7	palivo
vestavěna	vestavět	k5eAaPmNgFnS	vestavět
orbitální	orbitální	k2eAgFnSc1d1	orbitální
kosmická	kosmický	k2eAgFnSc1d1	kosmická
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vynesení	vynesení	k1gNnSc6	vynesení
se	se	k3xPyFc4	se
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
projevila	projevit	k5eAaPmAgFnS	projevit
řada	řada	k1gFnSc1	řada
závad	závada	k1gFnPc2	závada
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byly	být	k5eAaImAgFnP	být
pochybnosti	pochybnost	k1gFnPc1	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bude	být	k5eAaImBp3nS	být
stanice	stanice	k1gFnSc1	stanice
obyvatelná	obyvatelný	k2eAgFnSc1d1	obyvatelná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
neotevřely	otevřít	k5eNaPmAgInP	otevřít
sluneční	sluneční	k2eAgInPc1d1	sluneční
panely	panel	k1gInPc1	panel
<g/>
,	,	kIx,	,
nadměrně	nadměrně	k6eAd1	nadměrně
stoupala	stoupat	k5eAaImAgFnS	stoupat
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
prvního	první	k4xOgInSc2	první
letu	let	k1gInSc2	let
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
proto	proto	k8xC	proto
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
opravy	oprava	k1gFnSc2	oprava
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
závad	závada	k1gFnPc2	závada
<g/>
,	,	kIx,	,
bránících	bránící	k2eAgFnPc2d1	bránící
provozu	provoz	k1gInSc3	provoz
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
orbitální	orbitální	k2eAgFnSc3d1	orbitální
stanici	stanice	k1gFnSc3	stanice
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
vyslány	vyslán	k2eAgInPc4d1	vyslán
tři	tři	k4xCgInPc4	tři
pilotované	pilotovaný	k2eAgInPc4d1	pilotovaný
lety	let	k1gInPc4	let
Skylab	Skylab	k1gInSc1	Skylab
2	[number]	k4	2
<g/>
:	:	kIx,	:
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
posádka	posádka	k1gFnSc1	posádka
Charles	Charles	k1gMnSc1	Charles
Conrad	Conrada	k1gFnPc2	Conrada
Jr	Jr	k1gMnSc1	Jr
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Weitz	Weitz	k1gMnSc1	Weitz
a	a	k8xC	a
Joseph	Joseph	k1gMnSc1	Joseph
P.	P.	kA	P.
Kerwin	Kerwin	k1gMnSc1	Kerwin
Skylab	Skylab	k1gMnSc1	Skylab
3	[number]	k4	3
<g/>
:	:	kIx,	:
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g />
.	.	kIx.	.
</s>
<s>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
posádka	posádka	k1gFnSc1	posádka
Alan	Alan	k1gMnSc1	Alan
L.	L.	kA	L.
Bean	Bean	k1gMnSc1	Bean
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
R.	R.	kA	R.
Lousma	Lousmum	k1gNnSc2	Lousmum
<g/>
,	,	kIx,	,
Owen	Owen	k1gMnSc1	Owen
K.	K.	kA	K.
Garriott	Garriott	k1gMnSc1	Garriott
Skylab	Skylab	k1gMnSc1	Skylab
4	[number]	k4	4
<g/>
:	:	kIx,	:
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1973	[number]	k4	1973
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
posádka	posádka	k1gFnSc1	posádka
Gerald	Gerald	k1gMnSc1	Gerald
P.	P.	kA	P.
Carr	Carr	k1gMnSc1	Carr
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
R.	R.	kA	R.
Pogue	Pogue	k1gInSc1	Pogue
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
G.	G.	kA	G.
Gibson	Gibson	k1gInSc1	Gibson
Pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
tři	tři	k4xCgFnPc4	tři
pilotované	pilotovaný	k2eAgFnPc4d1	pilotovaná
lety	let	k1gInPc7	let
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
nosič	nosič	k1gInSc1	nosič
Saturn	Saturn	k1gInSc1	Saturn
1	[number]	k4	1
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
vynesl	vynést	k5eAaPmAgInS	vynést
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
velitelským	velitelský	k2eAgInSc7d1	velitelský
a	a	k8xC	a
servisním	servisní	k2eAgInSc7d1	servisní
modulem	modul	k1gInSc7	modul
používaným	používaný	k2eAgInSc7d1	používaný
v	v	k7c6	v
programu	program	k1gInSc6	program
Apollo	Apollo	k1gNnSc1	Apollo
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
orbitální	orbitální	k2eAgFnSc7d1	orbitální
stanicí	stanice	k1gFnSc7	stanice
Skylab	Skylaba	k1gFnPc2	Skylaba
<g/>
.	.	kIx.	.
</s>
<s>
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
stanice	stanice	k1gFnSc1	stanice
Skylab	Skylaba	k1gFnPc2	Skylaba
částečně	částečně	k6eAd1	částečně
shořela	shořet	k5eAaPmAgFnS	shořet
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Navedena	naveden	k2eAgFnSc1d1	navedena
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
stanice	stanice	k1gFnSc2	stanice
ale	ale	k8xC	ale
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
nezanikly	zaniknout	k5eNaPmAgFnP	zaniknout
a	a	k8xC	a
dopadly	dopadnout	k5eAaPmAgFnP	dopadnout
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1979	[number]	k4	1979
v	v	k7c4	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
37	[number]	k4	37
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
UTC	UTC	kA	UTC
dopadly	dopadnout	k5eAaPmAgFnP	dopadnout
trosky	troska	k1gFnPc1	troska
poblíž	poblíž	k6eAd1	poblíž
australského	australský	k2eAgNnSc2d1	Australské
města	město	k1gNnSc2	město
Perthu	Perth	k1gInSc2	Perth
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
následně	následně	k6eAd1	následně
poslala	poslat	k5eAaPmAgFnS	poslat
americké	americký	k2eAgFnSc6d1	americká
agentuře	agentura	k1gFnSc6	agentura
NASA	NASA	kA	NASA
pokutu	pokuta	k1gFnSc4	pokuta
za	za	k7c4	za
znečišťování	znečišťování	k1gNnSc4	znečišťování
veřejného	veřejný	k2eAgNnSc2d1	veřejné
prostranství	prostranství	k1gNnSc2	prostranství
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
do	do	k7c2	do
města	město	k1gNnSc2	město
telefonoval	telefonovat	k5eAaImAgMnS	telefonovat
osobně	osobně	k6eAd1	osobně
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Jimmy	Jimma	k1gFnSc2	Jimma
Carter	Carter	k1gMnSc1	Carter
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
za	za	k7c4	za
dopad	dopad	k1gInSc4	dopad
tělesa	těleso	k1gNnSc2	těleso
jménem	jméno	k1gNnSc7	jméno
USA	USA	kA	USA
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
</s>
