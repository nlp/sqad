<p>
<s>
Kopretina	kopretina	k1gFnSc1	kopretina
bílá	bílý	k2eAgFnSc1d1	bílá
(	(	kIx(	(
<g/>
Leucanthemum	Leucanthemum	k1gInSc1	Leucanthemum
vulgare	vulgar	k1gMnSc5	vulgar
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
kvetoucí	kvetoucí	k2eAgInPc1d1	kvetoucí
<g/>
,	,	kIx,	,
20	[number]	k4	20
až	až	k9	až
80	[number]	k4	80
cm	cm	kA	cm
vysoká	vysoký	k2eAgFnSc1d1	vysoká
bylina	bylina	k1gFnSc1	bylina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hvězdnicovitých	hvězdnicovitý	k2eAgFnPc2d1	hvězdnicovitý
(	(	kIx(	(
<g/>
Asteraceae	Asteraceae	k1gFnPc2	Asteraceae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
leukos	leukos	k1gInSc1	leukos
(	(	kIx(	(
<g/>
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
)	)	kIx)	)
a	a	k8xC	a
anthemon	anthemon	k1gInSc4	anthemon
(	(	kIx(	(
<g/>
květ	květ	k1gInSc4	květ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
kopretina	kopretina	k1gFnSc1	kopretina
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
novotvary	novotvar	k1gInPc4	novotvar
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Janem	Jan	k1gMnSc7	Jan
Svatoplukem	Svatopluk	k1gMnSc7	Svatopluk
Preslem	Presl	k1gMnSc7	Presl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xC	jako
svatojánské	svatojánský	k2eAgNnSc1d1	svatojánské
kvítko	kvítko	k1gNnSc1	kvítko
<g/>
,	,	kIx,	,
janské	janský	k2eAgNnSc1d1	Janské
kvítí	kvítí	k1gNnSc1	kvítí
<g/>
,	,	kIx,	,
husička	husička	k1gFnSc1	husička
<g/>
,	,	kIx,	,
čáp	čáp	k1gMnSc1	čáp
nebo	nebo	k8xC	nebo
slunéčko	slunéčko	k1gNnSc1	slunéčko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
díky	díky	k7c3	díky
dětské	dětský	k2eAgFnSc3d1	dětská
hře	hra	k1gFnSc3	hra
"	"	kIx"	"
<g/>
má	mít	k5eAaImIp3nS	mít
mě	já	k3xPp1nSc4	já
rád	rád	k6eAd1	rád
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
mě	já	k3xPp1nSc4	já
rád	rád	k6eAd1	rád
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Synonyma	synonymum	k1gNnSc2	synonymum
==	==	k?	==
</s>
</p>
<p>
<s>
Chrysanthemum	Chrysanthemum	k1gInSc1	Chrysanthemum
leucanthemum	leucanthemum	k1gInSc1	leucanthemum
L.	L.	kA	L.
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
</s>
</p>
<p>
<s>
Leucanthemum	Leucanthemum	k1gInSc1	Leucanthemum
praecox	praecox	k1gInSc1	praecox
(	(	kIx(	(
<g/>
Horvatić	Horvatić	k1gMnSc1	Horvatić
<g/>
)	)	kIx)	)
Horvatić	Horvatić	k1gMnSc1	Horvatić
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
rostliny	rostlina	k1gFnSc2	rostlina
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vytrvalou	vytrvalý	k2eAgFnSc4d1	vytrvalá
bylinu	bylina	k1gFnSc4	bylina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
oddenek	oddenek	k1gInSc1	oddenek
je	být	k5eAaImIp3nS	být
válcovitý	válcovitý	k2eAgInSc1d1	válcovitý
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
krátký	krátký	k2eAgInSc4d1	krátký
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
a	a	k8xC	a
šikmo	šikmo	k6eAd1	šikmo
položený	položený	k2eAgInSc4d1	položený
<g/>
.	.	kIx.	.
</s>
<s>
Přímá	přímý	k2eAgFnSc1d1	přímá
<g/>
,	,	kIx,	,
rýhovaná	rýhovaný	k2eAgFnSc1d1	rýhovaná
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
chlupatá	chlupatý	k2eAgFnSc1d1	chlupatá
až	až	k8xS	až
lysá	lysý	k2eAgFnSc1d1	Lysá
lodyha	lodyha	k1gFnSc1	lodyha
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
nebo	nebo	k8xC	nebo
větvená	větvený	k2eAgFnSc1d1	větvená
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
vždy	vždy	k6eAd1	vždy
jediný	jediný	k2eAgInSc1d1	jediný
úbor	úbor	k1gInSc1	úbor
<g/>
.	.	kIx.	.
</s>
<s>
Přízemní	přízemní	k2eAgInPc1d1	přízemní
listy	list	k1gInPc1	list
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
obvejčité	obvejčitý	k2eAgInPc1d1	obvejčitý
nebo	nebo	k8xC	nebo
kopisťovité	kopisťovitý	k2eAgInPc1d1	kopisťovitý
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
však	však	k9	však
zúžené	zúžený	k2eAgNnSc1d1	zúžené
v	v	k7c4	v
řapík	řapík	k1gInSc4	řapík
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
na	na	k7c6	na
lodyze	lodyha	k1gFnSc6	lodyha
jsou	být	k5eAaImIp3nP	být
řídké	řídký	k2eAgFnPc1d1	řídká
<g/>
,	,	kIx,	,
střídavé	střídavý	k2eAgFnPc1d1	střídavá
a	a	k8xC	a
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
<g/>
,	,	kIx,	,
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
pilovité	pilovitý	k2eAgFnSc2d1	pilovitá
<g/>
.	.	kIx.	.
</s>
<s>
Úbor	úbor	k1gInSc1	úbor
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
s	s	k7c7	s
polokruhovitým	polokruhovitý	k2eAgInSc7d1	polokruhovitý
zeleným	zelený	k2eAgInSc7d1	zelený
zákrovem	zákrov	k1gInSc7	zákrov
složeným	složený	k2eAgInSc7d1	složený
z	z	k7c2	z
listenů	listen	k1gInPc2	listen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
černo-hnědě	černonědě	k6eAd1	černo-hnědě
lemované	lemovaný	k2eAgFnPc1d1	lemovaná
<g/>
.	.	kIx.	.
</s>
<s>
Okrajové	okrajový	k2eAgInPc1d1	okrajový
jazykovité	jazykovitý	k2eAgInPc1d1	jazykovitý
lístky	lístek	k1gInPc1	lístek
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
jsou	být	k5eAaImIp3nP	být
samčí	samčí	k2eAgFnPc1d1	samčí
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
cm	cm	kA	cm
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
zákrov	zákrov	k1gInSc1	zákrov
<g/>
.	.	kIx.	.
</s>
<s>
Kvítky	kvítek	k1gInPc1	kvítek
terčové	terčový	k2eAgInPc1d1	terčový
jsou	být	k5eAaImIp3nP	být
žluté	žlutý	k2eAgInPc1d1	žlutý
a	a	k8xC	a
oboupohlavní	oboupohlavní	k2eAgInPc1d1	oboupohlavní
<g/>
.	.	kIx.	.
</s>
<s>
Srůstem	srůst	k1gInSc7	srůst
dvou	dva	k4xCgInPc2	dva
plodolistů	plodolist	k1gInPc2	plodolist
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
semeník	semeník	k1gInSc1	semeník
obsahující	obsahující	k2eAgNnSc4d1	obsahující
jedno	jeden	k4xCgNnSc4	jeden
vajíčko	vajíčko	k1gNnSc4	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
nažky	nažka	k1gFnPc4	nažka
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
okrajové	okrajový	k2eAgFnPc4d1	okrajová
nažky	nažka	k1gFnPc4	nažka
i	i	k8xC	i
šikmý	šikmý	k2eAgInSc4d1	šikmý
lem	lem	k1gInSc4	lem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
opylení	opylení	k1gNnSc3	opylení
dochází	docházet	k5eAaImIp3nP	docházet
pomocí	pomoc	k1gFnSc7	pomoc
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
však	však	k9	však
i	i	k9	i
samoopylení	samoopylení	k1gNnSc1	samoopylení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Kopretina	kopretina	k1gFnSc1	kopretina
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
typický	typický	k2eAgInSc4d1	typický
luční	luční	k2eAgInSc4d1	luční
druh	druh	k1gInSc4	druh
byliny	bylina	k1gFnSc2	bylina
kvetoucí	kvetoucí	k2eAgFnSc6d1	kvetoucí
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
lukách	luka	k1gNnPc6	luka
<g/>
,	,	kIx,	,
svazích	svah	k1gInPc6	svah
<g/>
,	,	kIx,	,
pastvinách	pastvina	k1gFnPc6	pastvina
<g/>
,	,	kIx,	,
okrajích	okraj	k1gInPc6	okraj
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
světlých	světlý	k2eAgInPc6d1	světlý
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
až	až	k6eAd1	až
do	do	k7c2	do
2000	[number]	k4	2000
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
v	v	k7c6	v
nejsevernějších	severní	k2eAgFnPc6d3	nejsevernější
částech	část	k1gFnPc6	část
Skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Uralu	Ural	k1gInSc2	Ural
<g/>
.	.	kIx.	.
</s>
<s>
Zavlečena	zavlečen	k2eAgFnSc1d1	zavlečena
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Azorské	azorský	k2eAgInPc4d1	azorský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
i	i	k8xC	i
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
Brazílie	Brazílie	k1gFnSc2	Brazílie
až	až	k9	až
do	do	k7c2	do
Patagonie	Patagonie	k1gFnSc2	Patagonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
z	z	k7c2	z
hospodářsky	hospodářsky	k6eAd1	hospodářsky
využívaných	využívaný	k2eAgFnPc2d1	využívaná
ploch	plocha	k1gFnPc2	plocha
vytlačována	vytlačovat	k5eAaImNgFnS	vytlačovat
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vypěstováno	vypěstovat	k5eAaPmNgNnS	vypěstovat
i	i	k9	i
několik	několik	k4yIc1	několik
velkokvětých	velkokvětý	k2eAgInPc2d1	velkokvětý
druhů	druh	k1gInPc2	druh
určených	určený	k2eAgInPc2d1	určený
především	především	k6eAd1	především
do	do	k7c2	do
zahrad	zahrada	k1gFnPc2	zahrada
jako	jako	k8xS	jako
okrasné	okrasný	k2eAgFnPc1d1	okrasná
květiny	květina	k1gFnPc1	květina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
úbor	úbor	k1gInSc4	úbor
velký	velký	k2eAgInSc4d1	velký
až	až	k6eAd1	až
16	[number]	k4	16
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Magie	magie	k1gFnSc2	magie
a	a	k8xC	a
léčitelství	léčitelství	k1gNnSc2	léčitelství
==	==	k?	==
</s>
</p>
<p>
<s>
Kopretina	kopretina	k1gFnSc1	kopretina
bílá	bílý	k2eAgFnSc1d1	bílá
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
používána	používat	k5eAaImNgFnS	používat
k	k	k7c3	k
věštění	věštění	k1gNnSc3	věštění
a	a	k8xC	a
při	při	k7c6	při
rituálech	rituál	k1gInPc6	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Sloužila	sloužit	k5eAaImAgFnS	sloužit
také	také	k9	také
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
sirupů	sirup	k1gInPc2	sirup
<g/>
,	,	kIx,	,
esencí	esence	k1gFnPc2	esence
a	a	k8xC	a
pastilek	pastilka	k1gFnPc2	pastilka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RANDUŠKA	RANDUŠKA	kA	RANDUŠKA
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
,	,	kIx,	,
ŠOMŠÁK	ŠOMŠÁK	kA	ŠOMŠÁK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
,	,	kIx,	,
HÁBEROVÁ	HÁBEROVÁ	kA	HÁBEROVÁ
<g/>
,	,	kIx,	,
Izabela	Izabela	k1gFnSc1	Izabela
<g/>
.	.	kIx.	.
</s>
<s>
Barevný	barevný	k2eAgInSc4d1	barevný
atlas	atlas	k1gInSc4	atlas
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
(	(	kIx(	(
<g/>
Druhé	druhý	k4xOgFnPc1	druhý
české	český	k2eAgFnPc1d1	Česká
<g/>
,	,	kIx,	,
opravené	opravený	k2eAgNnSc1d1	opravené
vydání	vydání	k1gNnSc1	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Vydavatělstvo	Vydavatělstvo	k1gNnSc1	Vydavatělstvo
Obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
<g/>
,	,	kIx,	,
<g/>
Bratislava	Bratislava	k1gFnSc1	Bratislava
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
s	s	k7c7	s
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Profil	profil	k1gInSc1	profil
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
:	:	kIx,	:
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
s.	s.	k?	s.
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PILÁT	Pilát	k1gMnSc1	Pilát
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
UŠÁK	ušák	k1gMnSc1	ušák
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
<g/>
.	.	kIx.	.
</s>
<s>
Kapesní	kapesní	k2eAgInSc4d1	kapesní
atlas	atlas	k1gInSc4	atlas
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
,	,	kIx,	,
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
s	s	k7c7	s
222	[number]	k4	222
<g/>
-	-	kIx~	-
<g/>
223	[number]	k4	223
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘÍSKA	Tříska	k1gMnSc1	Tříska
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
flóra	flóra	k1gFnSc1	flóra
<g/>
.1	.1	k4	.1
<g/>
.	.	kIx.	.
české	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
<g/>
.	.	kIx.	.
<g/>
ARTIA	ARTIA	kA	ARTIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
s	s	k7c7	s
146	[number]	k4	146
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kopretina	kopretina	k1gFnSc1	kopretina
bílá	bílý	k2eAgFnSc1d1	bílá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Leucanthemum	Leucanthemum	k1gInSc1	Leucanthemum
vulgare	vulgar	k1gMnSc5	vulgar
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
na	na	k7c4	na
biolib	biolib	k1gInSc4	biolib
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
rostliny	rostlina	k1gFnSc2	rostlina
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
a	a	k8xC	a
fotografie	fotografie	k1gFnSc1	fotografie
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
na	na	k7c4	na
botany	botan	k1gInPc4	botan
</s>
</p>
