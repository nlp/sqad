<s>
Crazy	Craza	k1gFnPc1
Town	Towna	k1gFnPc2
</s>
<s>
Crazy	Craza	k1gFnPc1
Town	Towna	k1gFnPc2
Základní	základní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Původ	původ	k1gInSc1
</s>
<s>
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Žánry	žánr	k1gInPc7
</s>
<s>
alternativní	alternativní	k2eAgInSc4d1
rock	rock	k1gInSc4
a	a	k8xC
rap	rap	k1gMnSc1
rock	rock	k1gInSc4
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Columbia	Columbia	k1gFnSc1
Records	Recordsa	k1gFnPc2
Web	web	k1gInSc4
</s>
<s>
crazytownband	crazytownband	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
Současní	současný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Seth	Seth	k1gMnSc1
Brinzer	Brinzer	k1gMnSc1
<g/>
,	,	kIx,
Elias	Elias	k1gMnSc1
Tannous	Tannous	k1gMnSc1
<g/>
,	,	kIx,
Hasma	Hasma	k1gNnSc1
Angeleno	Angelen	k2eAgNnSc1d1
<g/>
,	,	kIx,
Luca	Luca	k1gMnSc1
Pretorius	Pretorius	k1gMnSc1
<g/>
,	,	kIx,
Stefano	Stefana	k1gFnSc5
Bassoli	Bassole	k1gFnSc3
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Crazy	Craz	k1gInPc1
Town	Towna	k1gFnPc2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
zkráceně	zkráceně	k6eAd1
jako	jako	k8xS,k8xC
CXT	CXT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
rap	rapa	k1gFnPc2
rocková	rockový	k2eAgFnSc1d1
kapela	kapela	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
založili	založit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
dva	dva	k4xCgMnPc1
zpěváci	zpěvák	k1gMnPc1
Bret	Bret	k2eAgInSc4d1
"	"	kIx"
<g/>
Epic	Epic	k1gInSc4
<g/>
"	"	kIx"
Mazur	Mazur	k1gMnSc1
a	a	k8xC
Seth	Seth	k1gMnSc1
"	"	kIx"
<g/>
Shifty	Shifta	k1gMnSc2
<g/>
"	"	kIx"
Binzer	Binzer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Crazy	Craza	k1gFnPc1
Town	Towna	k1gFnPc2
se	se	k3xPyFc4
nejvíce	hodně	k6eAd3,k6eAd1
proslavili	proslavit	k5eAaPmAgMnP
svým	svůj	k3xOyFgInSc7
hitem	hit	k1gInSc7
"	"	kIx"
<g/>
Butterfly	butterfly	k1gInSc7
<g/>
"	"	kIx"
z	z	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
na	na	k7c4
první	první	k4xOgFnSc4
příčku	příčka	k1gFnSc4
v	v	k7c6
hudebním	hudební	k2eAgInSc6d1
žebříčku	žebříček	k1gInSc6
Billboard	billboard	k1gInSc1
Hot	hot	k0
100	#num#	k4
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc2
debutové	debutový	k2eAgFnSc2d1
desky	deska	k1gFnSc2
The	The	k1gMnSc1
Gift	Gift	k1gMnSc1
of	of	k?
Game	game	k1gInSc1
se	se	k3xPyFc4
posléze	posléze	k6eAd1
prodalo	prodat	k5eAaPmAgNnS
přes	přes	k7c4
1	#num#	k4
a	a	k8xC
půl	půl	k1xP
milionu	milion	k4xCgInSc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Následující	následující	k2eAgNnSc1d1
album	album	k1gNnSc1
Darkhorse	Darkhorse	k1gFnSc2
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
již	již	k6eAd1
s	s	k7c7
podobným	podobný	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
nesetkalo	setkat	k5eNaPmAgNnS
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
také	také	k9
přispělo	přispět	k5eAaPmAgNnS
k	k	k7c3
následnému	následný	k2eAgNnSc3d1
rozpadnutí	rozpadnutí	k1gNnSc3
kapely	kapela	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpěváci	Zpěvák	k1gMnPc1
Shifty	Shifta	k1gFnPc4
a	a	k8xC
Epic	Epic	k1gFnSc4
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
kapelu	kapela	k1gFnSc4
obnovit	obnovit	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
pak	pak	k9
vyšla	vyjít	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc1
řadová	řadový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
The	The	k1gFnSc2
Brimstone	Brimston	k1gInSc5
Sluggers	Sluggers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Crazy	Craza	k1gFnSc2
Town	Towna	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9188	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
10324268-5	10324268-5	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0722	#num#	k4
8911	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2001037472	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
134489126	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2001037472	#num#	k4
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
