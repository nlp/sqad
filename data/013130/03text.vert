<p>
<s>
John	John	k1gMnSc1	John
Dowland	Dowlanda	k1gFnPc2	Dowlanda
(	(	kIx(	(
<g/>
1562	[number]	k4	1562
<g/>
/	/	kIx~	/
<g/>
1563	[number]	k4	1563
nejspíše	nejspíše	k9	nejspíše
Westminster	Westminster	k1gInSc1	Westminster
<g/>
,	,	kIx,	,
možná	možná	k9	možná
Dalkey	Dalkey	k1gInPc4	Dalkey
u	u	k7c2	u
Dublinu	Dublin	k1gInSc2	Dublin
–	–	k?	–
pohřben	pohřben	k2eAgInSc1d1	pohřben
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1626	[number]	k4	1626
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
loutnista	loutnista	k1gMnSc1	loutnista
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
představitelů	představitel	k1gMnPc2	představitel
anglické	anglický	k2eAgFnSc2d1	anglická
renesanční	renesanční	k2eAgFnSc2d1	renesanční
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1580	[number]	k4	1580
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
sira	sir	k1gMnSc2	sir
Henryho	Henry	k1gMnSc2	Henry
Cobhama	Cobham	k1gMnSc2	Cobham
<g/>
,	,	kIx,	,
vyslance	vyslanec	k1gMnSc2	vyslanec
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
nástupce	nástupce	k1gMnSc2	nástupce
Edwarda	Edward	k1gMnSc2	Edward
Stafforda	Stafford	k1gMnSc2	Stafford
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
.	.	kIx.	.
</s>
<s>
Poznal	poznat	k5eAaPmAgMnS	poznat
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
okouzlily	okouzlit	k5eAaPmAgInP	okouzlit
zvláště	zvláště	k9	zvláště
madrigaly	madrigal	k1gInPc1	madrigal
Luky	luk	k1gInPc4	luk
Marenzia	Marenzius	k1gMnSc2	Marenzius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1598	[number]	k4	1598
<g/>
–	–	k?	–
<g/>
1606	[number]	k4	1606
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
jako	jako	k8xS	jako
dvorní	dvorní	k2eAgMnSc1d1	dvorní
loutnista	loutnista	k1gMnSc1	loutnista
krále	král	k1gMnSc2	král
Kristiána	Kristián	k1gMnSc2	Kristián
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
loutnový	loutnový	k2eAgMnSc1d1	loutnový
virtuos	virtuos	k1gMnSc1	virtuos
proslul	proslout	k5eAaPmAgMnS	proslout
také	také	k9	také
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1612	[number]	k4	1612
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
Dowlandových	Dowlandový	k2eAgFnPc2d1	Dowlandový
skladeb	skladba	k1gFnPc2	skladba
je	být	k5eAaImIp3nS	být
loutně	loutna	k1gFnSc6	loutna
věnován	věnován	k2eAgInSc1d1	věnován
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
písně	píseň	k1gFnPc1	píseň
a	a	k8xC	a
zpěvy	zpěv	k1gInPc1	zpěv
s	s	k7c7	s
loutnovým	loutnový	k2eAgInSc7d1	loutnový
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dowlandovy	Dowlandův	k2eAgFnPc1d1	Dowlandův
skladby	skladba	k1gFnPc1	skladba
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
==	==	k?	==
</s>
</p>
<p>
<s>
Dowland	Dowland	k1gInSc1	Dowland
se	se	k3xPyFc4	se
za	za	k7c2	za
života	život	k1gInSc2	život
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
těšil	těšit	k5eAaImAgMnS	těšit
mimořádné	mimořádný	k2eAgFnSc3d1	mimořádná
slávě	sláva	k1gFnSc3	sláva
<g/>
,	,	kIx,	,
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
však	však	k9	však
pozapomenut	pozapomenut	k2eAgMnSc1d1	pozapomenut
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
oživil	oživit	k5eAaPmAgMnS	oživit
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Frederick	Frederick	k1gMnSc1	Frederick
Keel	Keel	k1gMnSc1	Keel
<g/>
,	,	kIx,	,
když	když	k8xS	když
vydal	vydat	k5eAaPmAgMnS	vydat
15	[number]	k4	15
Dowlandových	Dowlandový	k2eAgFnPc2d1	Dowlandový
skladeb	skladba	k1gFnPc2	skladba
ve	v	k7c6	v
svazcích	svazek	k1gInPc6	svazek
alžbětinských	alžbětinský	k2eAgFnPc2d1	Alžbětinská
milostných	milostný	k2eAgFnPc2d1	milostná
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
a	a	k8xC	a
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
ve	v	k7c4	v
volných	volný	k2eAgNnPc2d1	volné
aranžmá	aranžmá	k1gNnPc2	aranžmá
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
sólový	sólový	k2eAgInSc4d1	sólový
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
značné	značný	k2eAgFnPc4d1	značná
popularity	popularita	k1gFnPc4	popularita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sting	Sting	k1gMnSc1	Sting
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
vydal	vydat	k5eAaPmAgInS	vydat
album	album	k1gNnSc4	album
Dowlandových	Dowlandový	k2eAgFnPc2d1	Dowlandový
písní	píseň	k1gFnPc2	píseň
Songs	Songs	k1gInSc1	Songs
from	from	k1gMnSc1	from
the	the	k?	the
Labyrinth	Labyrinth	k1gMnSc1	Labyrinth
<g/>
;	;	kIx,	;
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
bosenský	bosenský	k2eAgMnSc1d1	bosenský
loutnista	loutnista	k1gMnSc1	loutnista
Edin	Edin	k1gMnSc1	Edin
Karamazov	Karamazovo	k1gNnPc2	Karamazovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
Now	Now	k1gFnSc1	Now
<g/>
,	,	kIx,	,
O	o	k7c6	o
now	now	k?	now
I	i	k9	i
needs	eds	k6eNd1	eds
must	must	k2eAgInSc4d1	must
part	part	k1gInSc4	part
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Teď	teď	k6eAd1	teď
<g/>
,	,	kIx,	,
ach	ach	k0	ach
teď	teď	k6eAd1	teď
se	se	k3xPyFc4	se
z	z	k7c2	z
nezbytí	nezbytí	k1gNnSc2	nezbytí
musím	muset	k5eAaImIp1nS	muset
rozloučit	rozloučit	k5eAaPmF	rozloučit
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
šestá	šestý	k4xOgFnSc1	šestý
z	z	k7c2	z
21	[number]	k4	21
v	v	k7c6	v
Dowlandově	Dowlandův	k2eAgFnSc6d1	Dowlandův
sbírce	sbírka	k1gFnSc6	sbírka
The	The	k1gMnSc1	The
first	first	k1gMnSc1	first
booke	booke	k6eAd1	booke
of	of	k?	of
Songes	Songes	k1gInSc1	Songes
<g/>
,	,	kIx,	,
or	or	k?	or
<g/>
,	,	kIx,	,
Ayres	Ayres	k1gInSc1	Ayres
of	of	k?	of
fowre	fowr	k1gInSc5	fowr
partes	partes	k?	partes
with	with	k1gInSc1	with
Tableture	Tabletur	k1gMnSc5	Tabletur
for	forum	k1gNnPc2	forum
<g />
.	.	kIx.	.
</s>
<s>
the	the	k?	the
Lute	Lute	k1gFnSc1	Lute
<g/>
:	:	kIx,	:
so	so	k?	so
made	made	k1gInSc1	made
that	that	k1gInSc1	that
all	all	k?	all
the	the	k?	the
partes	partes	k?	partes
together	togethra	k1gFnPc2	togethra
<g/>
,	,	kIx,	,
or	or	k?	or
either	eithra	k1gFnPc2	eithra
of	of	k?	of
them	them	k1gMnSc1	them
seuerally	seueralla	k1gFnSc2	seueralla
may	may	k?	may
be	be	k?	be
song	song	k1gInSc1	song
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Lute	Lute	k1gNnPc2	Lute
<g/>
,	,	kIx,	,
Orpherian	Orpheriana	k1gFnPc2	Orpheriana
or	or	k?	or
Viol	Viola	k1gFnPc2	Viola
de	de	k?	de
gambo	gamba	k1gFnSc5	gamba
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
1597	[number]	k4	1597
<g/>
)	)	kIx)	)
nahrál	nahrát	k5eAaPmAgInS	nahrát
Spirituál	spirituál	k1gInSc1	spirituál
Kvintet	kvintet	k1gInSc4	kvintet
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Dávno	dávno	k6eAd1	dávno
<g/>
,	,	kIx,	,
dávno	dávno	k6eAd1	dávno
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
album	album	k1gNnSc1	album
Na	na	k7c6	na
káře	kára	k1gFnSc6	kára
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
;	;	kIx,	;
reedice	reedice	k1gFnSc1	reedice
na	na	k7c6	na
posledním	poslední	k2eAgMnSc6d1	poslední
ze	z	k7c2	z
4	[number]	k4	4
CD	CD	kA	CD
kompilace	kompilace	k1gFnSc1	kompilace
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Sto	sto	k4xCgNnSc4	sto
nejkrásnějších	krásný	k2eAgFnPc2d3	nejkrásnější
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
"	"	kIx"	"
<g/>
Dávno	dávno	k6eAd1	dávno
<g/>
,	,	kIx,	,
dávno	dávno	k6eAd1	dávno
vím	vědět	k5eAaImIp1nS	vědět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
identifikován	identifikovat	k5eAaBmNgInS	identifikovat
pouze	pouze	k6eAd1	pouze
anglickým	anglický	k2eAgInSc7d1	anglický
názvem	název	k1gInSc7	název
a	a	k8xC	a
melodie	melodie	k1gFnSc1	melodie
popsána	popsat	k5eAaPmNgFnS	popsat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
tradicionál	tradicionál	k1gInSc1	tradicionál
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
český	český	k2eAgInSc1d1	český
text	text	k1gInSc1	text
Jiřího	Jiří	k1gMnSc2	Jiří
Tichoty	tichota	k1gFnSc2	tichota
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
trojím	trojí	k4xRgNnSc7	trojí
opakováním	opakování	k1gNnSc7	opakování
"	"	kIx"	"
<g/>
dávno	dávno	k6eAd1	dávno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
Dowlandovým	Dowlandový	k2eAgInSc7d1	Dowlandový
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pojat	pojmout	k5eAaPmNgInS	pojmout
jako	jako	k9	jako
duet	duet	k1gInSc1	duet
Zdenky	Zdenka	k1gFnSc2	Zdenka
Tichotové	Tichotový	k2eAgFnSc2d1	Tichotová
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Zicha	Zich	k1gMnSc2	Zich
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výběr	výběr	k1gInSc1	výběr
dvanácti	dvanáct	k4xCc2	dvanáct
Dowlandových	Dowlandový	k2eAgFnPc2d1	Dowlandový
(	(	kIx(	(
<g/>
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
jiných	jiný	k2eAgFnPc2d1	jiná
<g/>
)	)	kIx)	)
písní	píseň	k1gFnPc2	píseň
začínající	začínající	k2eAgFnSc4d1	začínající
výšeuvedenou	výšeuvedený	k2eAgFnSc4d1	výšeuvedená
nahrála	nahrát	k5eAaBmAgFnS	nahrát
Jana	Jana	k1gFnSc1	Jana
Lewitová	Lewitový	k2eAgFnSc1d1	Lewitová
s	s	k7c7	s
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Mertou	Merta	k1gMnSc7	Merta
na	na	k7c4	na
album	album	k1gNnSc4	album
Ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
mě	já	k3xPp1nSc4	já
zanechte	zanecht	k1gInSc5	zanecht
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
ARTA	ARTA	kA	ARTA
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
originále	originál	k1gInSc6	originál
a	a	k8xC	a
co	co	k9	co
možná	možná	k9	možná
původní	původní	k2eAgFnSc4d1	původní
instrumentaci	instrumentace	k1gFnSc4	instrumentace
<g/>
,	,	kIx,	,
doplněné	doplněná	k1gFnPc4	doplněná
Lewitové	Lewitový	k2eAgFnPc4d1	Lewitová
volnými	volný	k2eAgInPc7d1	volný
překlady	překlad	k1gInPc7	překlad
v	v	k7c6	v
bookletu	booklet	k1gInSc6	booklet
a	a	k8xC	a
recitaci	recitace	k1gFnSc6	recitace
sedmi	sedm	k4xCc2	sedm
na	na	k7c6	na
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
John	John	k1gMnSc1	John
Dowland	Dowlanda	k1gFnPc2	Dowlanda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
John	John	k1gMnSc1	John
Dowland	Dowlanda	k1gFnPc2	Dowlanda
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
JohnPowland	JohnPowland	k1gInSc1	JohnPowland
<g/>
.	.	kIx.	.
<g/>
de	de	k?	de
</s>
</p>
