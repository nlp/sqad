<s>
John	John	k1gMnSc1	John
Dowland	Dowlanda	k1gFnPc2	Dowlanda
(	(	kIx(	(
<g/>
1562	[number]	k4	1562
<g/>
/	/	kIx~	/
<g/>
1563	[number]	k4	1563
nejspíše	nejspíše	k9	nejspíše
Westminster	Westminster	k1gInSc1	Westminster
<g/>
,	,	kIx,	,
možná	možná	k9	možná
Dalkey	Dalkey	k1gInPc4	Dalkey
u	u	k7c2	u
Dublinu	Dublin	k1gInSc2	Dublin
–	–	k?	–
pohřben	pohřben	k2eAgInSc1d1	pohřben
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1626	[number]	k4	1626
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
loutnista	loutnista	k1gMnSc1	loutnista
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
představitelů	představitel	k1gMnPc2	představitel
anglické	anglický	k2eAgFnSc2d1	anglická
renesanční	renesanční	k2eAgFnSc2d1	renesanční
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
