<s desamb="1">
Na	na	k7c6
glóbu	glóbus	k1gInSc6
jsou	být	k5eAaImIp3nP
zachyceny	zachycen	k2eAgInPc1d1
tři	tři	k4xCgInPc1
tehdy	tehdy	k6eAd1
známé	známý	k2eAgInPc1d1
kontinenty	kontinent	k1gInPc1
(	(	kIx(
<g/>
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
Asie	Asie	k1gFnSc1
a	a	k8xC
Afrika	Afrika	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
množství	množství	k1gNnSc1
více	hodně	k6eAd2
či	či	k8xC
méně	málo	k6eAd2
hypotetických	hypotetický	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
.	.	kIx.
</s>