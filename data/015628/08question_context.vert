<s desamb="1">
Glóbus	glóbus	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
posledních	poslední	k2eAgNnPc2d1
kartografických	kartografický	k2eAgNnPc2d1
děl	dílo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
představují	představovat	k5eAaImIp3nP
svět	svět	k1gInSc4
před	před	k7c7
objevením	objevení	k1gNnSc7
Ameriky	Amerika	k1gFnSc2
Kryštofem	Kryštof	k1gMnSc7
Kolumbem	Kolumbus	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1492	#num#	k4
<g/>
.	.	kIx.
</s>