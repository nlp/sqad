<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Marek	Marek	k1gMnSc1	Marek
I.	I.	kA	I.
(	(	kIx(	(
<g/>
Marcus	Marcus	k1gMnSc1	Marcus
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
34	[number]	k4	34
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
trval	trvat	k5eAaImAgInS	trvat
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
20	[number]	k4	20
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
336	[number]	k4	336
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Datum	datum	k1gNnSc1	datum
narození	narození	k1gNnSc1	narození
známo	znám	k2eAgNnSc1d1	známo
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
první	první	k4xOgMnSc1	první
z	z	k7c2	z
papežů	papež	k1gMnPc2	papež
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
přesné	přesný	k2eAgNnSc1d1	přesné
datum	datum	k1gNnSc1	datum
zvolení	zvolení	k1gNnSc2	zvolení
a	a	k8xC	a
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Liber	libra	k1gFnPc2	libra
pontificalis	pontificalis	k1gFnPc2	pontificalis
byl	být	k5eAaImAgInS	být
narozen	narodit	k5eAaPmNgInS	narodit
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Priscus	Priscus	k1gMnSc1	Priscus
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
papežský	papežský	k2eAgInSc4d1	papežský
stolec	stolec	k1gInSc4	stolec
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
papeže	papež	k1gMnSc4	papež
Silvestra	Silvestr	k1gMnSc4	Silvestr
I.	I.	kA	I.
Patřil	patřit	k5eAaImAgMnS	patřit
patrně	patrně	k6eAd1	patrně
k	k	k7c3	k
nejbližším	blízký	k2eAgMnPc3d3	nejbližší
spolupracovníkům	spolupracovník	k1gMnPc3	spolupracovník
nejen	nejen	k6eAd1	nejen
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
papeže	papež	k1gMnSc4	papež
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jeho	on	k3xPp3gMnSc4	on
předchůdce	předchůdce	k1gMnSc4	předchůdce
Miltiada	Miltiada	k1gFnSc1	Miltiada
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zřejmě	zřejmě	k6eAd1	zřejmě
jemu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
adresován	adresován	k2eAgInSc1d1	adresován
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
313	[number]	k4	313
císař	císař	k1gMnSc1	císař
Konstantin	Konstantin	k1gMnSc1	Konstantin
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
svolání	svolání	k1gNnSc4	svolání
biskupského	biskupský	k2eAgInSc2d1	biskupský
sněmu	sněm	k1gInSc2	sněm
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
otázky	otázka	k1gFnSc2	otázka
donatistů	donatista	k1gMnPc2	donatista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
činech	čin	k1gInPc6	čin
tohoto	tento	k3xDgMnSc4	tento
papeže	papež	k1gMnSc4	papež
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k4c4	mnoho
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jistě	jistě	k6eAd1	jistě
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
krátkostí	krátkost	k1gFnSc7	krátkost
pontifikátu	pontifikát	k1gInSc2	pontifikát
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
hlavní	hlavní	k2eAgFnSc7d1	hlavní
událostí	událost	k1gFnSc7	událost
církve	církev	k1gFnSc2	církev
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgInP	být
důsledky	důsledek	k1gInPc1	důsledek
nicejského	nicejský	k2eAgInSc2d1	nicejský
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
odsouzení	odsouzení	k1gNnSc1	odsouzení
ariánství	ariánství	k1gNnSc2	ariánství
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
byl	být	k5eAaImAgMnS	být
alexandrijský	alexandrijský	k2eAgMnSc1d1	alexandrijský
patriarcha	patriarcha	k1gMnSc1	patriarcha
Atanásius	Atanásius	k1gMnSc1	Atanásius
(	(	kIx(	(
<g/>
296	[number]	k4	296
<g/>
–	–	k?	–
<g/>
373	[number]	k4	373
<g/>
)	)	kIx)	)
poslán	poslat	k5eAaPmNgMnS	poslat
z	z	k7c2	z
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
v	v	k7c6	v
Trevíru	Trevír	k1gInSc6	Trevír
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
ariánskými	ariánský	k2eAgMnPc7d1	ariánský
křesťany	křesťan	k1gMnPc7	křesťan
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
biskupského	biskupský	k2eAgInSc2d1	biskupský
stolce	stolec	k1gInSc2	stolec
sesazen	sesazen	k2eAgMnSc1d1	sesazen
Marcel	Marcel	k1gMnSc1	Marcel
z	z	k7c2	z
Ancyry	Ancyra	k1gMnSc2	Ancyra
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
představitelů	představitel	k1gMnPc2	představitel
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přijali	přijmout	k5eAaPmAgMnP	přijmout
nicejské	nicejský	k2eAgNnSc4d1	nicejské
vyznání	vyznání	k1gNnSc4	vyznání
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
sice	sice	k8xC	sice
korespondence	korespondence	k1gFnSc1	korespondence
mezi	mezi	k7c7	mezi
papežem	papež	k1gMnSc7	papež
Markem	Marek	k1gMnSc7	Marek
a	a	k8xC	a
biskupem	biskup	k1gMnSc7	biskup
Atanásiem	Atanásius	k1gMnSc7	Atanásius
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
podrobnějším	podrobný	k2eAgNnSc6d2	podrobnější
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
padělkem	padělek	k1gInSc7	padělek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
dějin	dějiny	k1gFnPc2	dějiny
církve	církev	k1gFnSc2	církev
udělil	udělit	k5eAaPmAgMnS	udělit
biskupům	biskup	k1gMnPc3	biskup
v	v	k7c6	v
Ostii	Ostie	k1gFnSc6	Ostie
pallium	pallium	k1gNnSc1	pallium
(	(	kIx(	(
<g/>
pás	pás	k1gInSc1	pás
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
ovčí	ovčí	k2eAgFnSc2d1	ovčí
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
vyzdobený	vyzdobený	k2eAgInSc1d1	vyzdobený
černými	černý	k2eAgInPc7d1	černý
křížky	křížek	k1gInPc7	křížek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
arcibiskupům-metropolitům	arcibiskupůmetropolit	k1gInPc3	arcibiskupům-metropolit
<g/>
)	)	kIx)	)
a	a	k8xC	a
výhradní	výhradní	k2eAgNnSc4d1	výhradní
právo	právo	k1gNnSc4	právo
světit	světit	k5eAaImF	světit
římského	římský	k2eAgInSc2d1	římský
biskupa	biskup	k1gInSc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
mši	mše	k1gFnSc6	mše
svaté	svatá	k1gFnSc2	svatá
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
nicejské	nicejský	k2eAgNnSc1d1	nicejské
vyznání	vyznání	k1gNnSc1	vyznání
víry	víra	k1gFnSc2	víra
(	(	kIx(	(
<g/>
Credo	Credo	k1gNnSc1	Credo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgInPc1d1	historický
prameny	pramen	k1gInPc1	pramen
také	také	k9	také
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
papeže	papež	k1gMnSc4	papež
Marka	Marek	k1gMnSc4	Marek
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
sestavovat	sestavovat	k5eAaImF	sestavovat
Depositio	Depositio	k6eAd1	Depositio
episcoporum	episcoporum	k1gNnSc4	episcoporum
a	a	k8xC	a
Depositio	Depositio	k1gNnSc4	Depositio
martyrum	martyrum	k?	martyrum
<g/>
,	,	kIx,	,
první	první	k4xOgInPc1	první
seznamy	seznam	k1gInPc1	seznam
římských	římský	k2eAgMnPc2d1	římský
biskupů	biskup	k1gMnPc2	biskup
a	a	k8xC	a
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Liber	libra	k1gFnPc2	libra
Pontificalis	Pontificalis	k1gFnPc2	Pontificalis
nechal	nechat	k5eAaPmAgInS	nechat
postavit	postavit	k5eAaPmF	postavit
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
dva	dva	k4xCgInPc4	dva
kostely	kostel	k1gInPc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Marka	Marek	k1gMnSc2	Marek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
součástí	součást	k1gFnSc7	součást
Palazzo	Palazza	k1gFnSc5	Palazza
di	di	k?	di
Venezia	Venezius	k1gMnSc4	Venezius
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
sv.	sv.	kA	sv.
Balbíny	Balbína	k1gFnPc4	Balbína
mezi	mezi	k7c7	mezi
Via	via	k7c4	via
Ardeatina	Ardeatina	k1gFnSc1	Ardeatina
a	a	k8xC	a
Via	via	k7c4	via
Appia	Appius	k1gMnSc4	Appius
<g/>
.	.	kIx.	.
</s>
<s>
Pozemek	pozemek	k1gInSc1	pozemek
i	i	k9	i
vybavení	vybavení	k1gNnSc1	vybavení
kostelů	kostel	k1gInPc2	kostel
dostal	dostat	k5eAaPmAgMnS	dostat
Marek	Marek	k1gMnSc1	Marek
darem	dar	k1gInSc7	dar
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
Konstantina	Konstantin	k1gMnSc2	Konstantin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
336	[number]	k4	336
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
sv.	sv.	kA	sv.
Balbíny	Balbína	k1gFnSc2	Balbína
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
sám	sám	k3xTgMnSc1	sám
postavil	postavit	k5eAaPmAgMnS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
hrobku	hrobka	k1gFnSc4	hrobka
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
italský	italský	k2eAgMnSc1d1	italský
archeolog	archeolog	k1gMnSc1	archeolog
De	De	k?	De
Rossi	Rosse	k1gFnSc4	Rosse
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Liber	libra	k1gFnPc2	libra
pontificalis	pontificalis	k1gFnSc2	pontificalis
však	však	k9	však
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
přeneseny	přenést	k5eAaPmNgInP	přenést
do	do	k7c2	do
druhého	druhý	k4xOgMnSc4	druhý
jím	jíst	k5eAaImIp1nS	jíst
založeného	založený	k2eAgInSc2d1	založený
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
do	do	k7c2	do
římské	římský	k2eAgFnSc2d1	římská
baziliky	bazilika	k1gFnSc2	bazilika
sv.	sv.	kA	sv.
Marka	Marek	k1gMnSc2	Marek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
starověkými	starověký	k2eAgInPc7d1	starověký
rukopisy	rukopis	k1gInPc7	rukopis
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
oslavná	oslavný	k2eAgFnSc1d1	oslavná
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
složil	složit	k5eAaPmAgMnS	složit
papež	papež	k1gMnSc1	papež
Damasus	Damasus	k1gMnSc1	Damasus
I.	I.	kA	I.
(	(	kIx(	(
<g/>
366	[number]	k4	366
<g/>
–	–	k?	–
<g/>
384	[number]	k4	384
<g/>
)	)	kIx)	)
údajně	údajně	k6eAd1	údajně
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
svatého	svatý	k2eAgMnSc2d1	svatý
Marka	Marek	k1gMnSc2	Marek
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
je	být	k5eAaImIp3nS	být
však	však	k9	však
psána	psát	k5eAaImNgFnS	psát
natolik	natolik	k6eAd1	natolik
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
že	že	k8xS	že
dedikaci	dedikace	k1gFnSc4	dedikace
nelze	lze	k6eNd1	lze
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Památku	památka	k1gFnSc4	památka
svatého	svatý	k2eAgMnSc2d1	svatý
Marka	Marek	k1gMnSc2	Marek
uctívá	uctívat	k5eAaImIp3nS	uctívat
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gNnSc2	jeho
úmrtí	úmrtí	k1gNnSc2	úmrtí
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VONDRUŠKA	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
<g/>
.	.	kIx.	.
</s>
<s>
Životopisy	životopis	k1gInPc1	životopis
svatých	svatá	k1gFnPc2	svatá
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
dějin	dějiny	k1gFnPc2	dějiny
církevních	církevní	k2eAgFnPc2d1	církevní
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuncíř	Kuncíř	k1gMnSc1	Kuncíř
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Marek	marka	k1gFnPc2	marka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
papežů	papež	k1gMnPc2	papež
</s>
</p>
<p>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
