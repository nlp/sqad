<s>
Igor	Igor	k1gMnSc1	Igor
Fjodorovič	Fjodorovič	k1gMnSc1	Fjodorovič
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
И	И	k?	И
Ф	Ф	k?	Ф
С	С	k?	С
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1882	[number]	k4	1882
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
podle	podle	k7c2	podle
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
]	]	kIx)	]
Oranienbaum	Oranienbaum	k1gNnSc1	Oranienbaum
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1971	[number]	k4	1971
New	New	k1gMnSc2	New
York	York	k1gInSc4	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
baletech	balet	k1gInPc6	balet
(	(	kIx(	(
<g/>
Svěcení	svěcení	k1gNnPc2	svěcení
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
Petruška	petruška	k1gMnSc1	petruška
<g/>
,	,	kIx,	,
Pták	pták	k1gMnSc1	pták
Ohnivák	Ohnivák	k1gMnSc1	Ohnivák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
skladatelské	skladatelský	k2eAgNnSc1d1	skladatelské
dílo	dílo	k1gNnSc1	dílo
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
základním	základní	k2eAgInPc3d1	základní
vývojovým	vývojový	k2eAgInPc3d1	vývojový
proudům	proud	k1gInPc3	proud
hudby	hudba	k1gFnSc2	hudba
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Fjodora	Fjodor	k1gMnSc2	Fjodor
Stravinského	Stravinský	k2eAgMnSc2d1	Stravinský
<g/>
,	,	kIx,	,
basisty	basista	k1gMnSc2	basista
Mariinské	Mariinský	k2eAgFnSc2d1	Mariinský
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
a	a	k8xC	a
přítele	přítel	k1gMnSc2	přítel
Petra	Petr	k1gMnSc2	Petr
Iljiče	Iljič	k1gMnSc2	Iljič
Čajkovského	Čajkovský	k2eAgMnSc2d1	Čajkovský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
neprojevoval	projevovat	k5eNaImAgInS	projevovat
výrazný	výrazný	k2eAgInSc1d1	výrazný
hudební	hudební	k2eAgInSc1d1	hudební
talent	talent	k1gInSc1	talent
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
napovídal	napovídat	k5eAaBmAgInS	napovídat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
významným	významný	k2eAgMnSc7d1	významný
skladatelem	skladatel	k1gMnSc7	skladatel
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
otcově	otcův	k2eAgFnSc3d1	otcova
povolání	povolání	k1gNnSc2	povolání
byl	být	k5eAaImAgInS	být
dům	dům	k1gInSc1	dům
Stravinských	Stravinský	k2eAgMnPc2d1	Stravinský
vždy	vždy	k6eAd1	vždy
plný	plný	k2eAgInSc4d1	plný
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
ruského	ruský	k2eAgInSc2d1	ruský
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
původně	původně	k6eAd1	původně
studoval	studovat	k5eAaImAgMnS	studovat
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
jen	jen	k9	jen
zkusmo	zkusmo	k6eAd1	zkusmo
komponoval	komponovat	k5eAaImAgMnS	komponovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
ukázal	ukázat	k5eAaPmAgInS	ukázat
některé	některý	k3yIgFnPc4	některý
své	svůj	k3xOyFgFnPc4	svůj
skladby	skladba	k1gFnPc4	skladba
N.	N.	kA	N.
A.	A.	kA	A.
Rimskému-Korsakovovi	Rimskému-Korsakovovi	k1gRnPc5	Rimskému-Korsakovovi
<g/>
.	.	kIx.	.
</s>
<s>
Skladatel	skladatel	k1gMnSc1	skladatel
zřejmě	zřejmě	k6eAd1	zřejmě
zpočátku	zpočátku	k6eAd1	zpočátku
nerozpoznal	rozpoznat	k5eNaPmAgMnS	rozpoznat
Stravinského	Stravinský	k2eAgMnSc4d1	Stravinský
talent	talent	k1gInSc4	talent
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
začal	začít	k5eAaPmAgInS	začít
dávat	dávat	k5eAaImF	dávat
Stravinskému	Stravinský	k2eAgInSc3d1	Stravinský
soukromé	soukromý	k2eAgNnSc4d1	soukromé
lekce	lekce	k1gFnPc4	lekce
instrumentace	instrumentace	k1gFnSc2	instrumentace
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
kompozice	kompozice	k1gFnSc1	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
lekce	lekce	k1gFnPc1	lekce
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
až	až	k9	až
do	do	k7c2	do
Korsakovovy	Korsakovův	k2eAgFnSc2d1	Korsakovova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Stravinským	Stravinský	k2eAgInSc7d1	Stravinský
smrt	smrt	k1gFnSc4	smrt
jeho	jeho	k3xOp3gMnSc2	jeho
učitele	učitel	k1gMnSc2	učitel
velmi	velmi	k6eAd1	velmi
otřásla	otřást	k5eAaPmAgFnS	otřást
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
napsal	napsat	k5eAaBmAgMnS	napsat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
velkou	velký	k2eAgFnSc4d1	velká
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
symfonii	symfonie	k1gFnSc4	symfonie
Es	es	k1gNnSc2	es
dur	dur	k1gNnSc2	dur
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
nenesla	nést	k5eNaImAgFnS	nést
stopy	stopa	k1gFnPc4	stopa
jeho	on	k3xPp3gInSc2	on
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
se	se	k3xPyFc4	se
tajně	tajně	k6eAd1	tajně
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestřenicí	sestřenice	k1gFnSc7	sestřenice
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Nosenkovou	Nosenkový	k2eAgFnSc7d1	Nosenkový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
Sergejem	Sergej	k1gMnSc7	Sergej
Prokofjevem	Prokofjev	k1gInSc7	Prokofjev
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
udržoval	udržovat	k5eAaImAgMnS	udržovat
přátelství	přátelství	k1gNnSc4	přátelství
i	i	k8xC	i
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
když	když	k8xS	když
Prokofjev	Prokofjev	k1gMnSc1	Prokofjev
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
"	"	kIx"	"
<g/>
Ruským	ruský	k2eAgInSc7d1	ruský
baletem	balet	k1gInSc7	balet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
skladba	skladba	k1gFnSc1	skladba
Ohňostroj	ohňostroj	k1gInSc1	ohňostroj
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
ruského	ruský	k2eAgMnSc4d1	ruský
impresária	impresário	k1gMnSc4	impresário
Sergeje	Sergej	k1gMnSc4	Sergej
Ďagileva	Ďagilev	k1gMnSc4	Ďagilev
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
organizoval	organizovat	k5eAaBmAgMnS	organizovat
vystoupení	vystoupení	k1gNnSc4	vystoupení
baletního	baletní	k2eAgNnSc2d1	baletní
tělesa	těleso	k1gNnSc2	těleso
"	"	kIx"	"
<g/>
Ruský	ruský	k2eAgInSc1d1	ruský
balet	balet	k1gInSc1	balet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Stravinskému	Stravinský	k2eAgInSc3d1	Stravinský
spolupráci	spolupráce	k1gFnSc6	spolupráce
na	na	k7c6	na
příštím	příští	k2eAgInSc6d1	příští
baletu	balet	k1gInSc6	balet
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
překvapen	překvapit	k5eAaPmNgMnS	překvapit
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
Stravinskij	Stravinskij	k1gMnSc3	Stravinskij
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
již	již	k6eAd1	již
začal	začít	k5eAaPmAgInS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
námět	námět	k1gInSc4	námět
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
ruská	ruský	k2eAgFnSc1d1	ruská
pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
ptáku	pták	k1gMnSc6	pták
Ohnivákovi	Ohnivák	k1gMnSc6	Ohnivák
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
typicky	typicky	k6eAd1	typicky
ruská	ruský	k2eAgFnSc1d1	ruská
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
Stravinského	Stravinský	k2eAgMnSc4d1	Stravinský
tento	tento	k3xDgInSc1	tento
námět	námět	k1gInSc4	námět
příliš	příliš	k6eAd1	příliš
nelákal	lákat	k5eNaImAgMnS	lákat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
hudbou	hudba	k1gFnSc7	hudba
příliš	příliš	k6eAd1	příliš
popisovat	popisovat	k5eAaImF	popisovat
dějovou	dějový	k2eAgFnSc4d1	dějová
složku	složka	k1gFnSc4	složka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
byl	být	k5eAaImAgInS	být
balet	balet	k1gInSc1	balet
Pták	pták	k1gMnSc1	pták
Ohnivák	Ohnivák	k1gMnSc1	Ohnivák
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Oiseau	Oiseaa	k1gMnSc4	Oiseaa
de	de	k?	de
feu	feu	k?	feu
<g/>
)	)	kIx)	)
uveden	uveden	k2eAgInSc1d1	uveden
v	v	k7c6	v
Théâtre	Théâtr	k1gInSc5	Théâtr
National	National	k1gMnSc3	National
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Opéra	Opéra	k1gMnSc1	Opéra
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Orchestr	orchestr	k1gInSc1	orchestr
řídil	řídit	k5eAaImAgInS	řídit
Henri	Henr	k1gInSc3	Henr
Pierné	Pierný	k2eAgFnSc2d1	Pierný
a	a	k8xC	a
choreografii	choreografie	k1gFnSc4	choreografie
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Michel	Michel	k1gMnSc1	Michel
Fokin	Fokin	k1gMnSc1	Fokin
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
Ohniváka	Ohnivák	k1gMnSc2	Ohnivák
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
Tamara	Tamara	k1gFnSc1	Tamara
Karsavinová	Karsavinová	k1gFnSc1	Karsavinová
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
(	(	kIx(	(
<g/>
kterou	který	k3yQgFnSc7	který
zhlédl	zhlédnout	k5eAaPmAgMnS	zhlédnout
i	i	k9	i
Claude	Claud	k1gInSc5	Claud
Debussy	Debuss	k1gMnPc7	Debuss
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
přítelem	přítel	k1gMnSc7	přítel
až	až	k8xS	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
nečekaný	čekaný	k2eNgInSc4d1	nečekaný
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
výjimečně	výjimečně	k6eAd1	výjimečně
trvale	trvale	k6eAd1	trvale
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
<g/>
.	.	kIx.	.
</s>
<s>
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
popularita	popularita	k1gFnSc1	popularita
Ohniváka	Ohnivák	k1gMnSc2	Ohnivák
dlouho	dlouho	k6eAd1	dlouho
provázela	provázet	k5eAaImAgFnS	provázet
a	a	k8xC	a
s	s	k7c7	s
nelibostí	nelibost	k1gFnSc7	nelibost
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
potýkal	potýkat	k5eAaImAgMnS	potýkat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
balet	balet	k1gInSc1	balet
zdál	zdát	k5eAaImAgInS	zdát
příliš	příliš	k6eAd1	příliš
zdlouhavý	zdlouhavý	k2eAgInSc1d1	zdlouhavý
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
několik	několik	k4yIc1	několik
verzí	verze	k1gFnPc2	verze
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
baletní	baletní	k2eAgFnSc2d1	baletní
suity	suita	k1gFnSc2	suita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
prováděna	provádět	k5eAaImNgFnS	provádět
častěji	často	k6eAd2	často
než	než	k8xS	než
samotný	samotný	k2eAgInSc1d1	samotný
balet	balet	k1gInSc1	balet
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
Ohnivákovi	Ohnivák	k1gMnSc6	Ohnivák
se	s	k7c7	s
Stravinskij	Stravinskij	k1gFnSc7	Stravinskij
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
ho	on	k3xPp3gInSc4	on
později	pozdě	k6eAd2	pozdě
následovala	následovat	k5eAaImAgFnS	následovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1910	[number]	k4	1910
byla	být	k5eAaImAgFnS	být
paní	paní	k1gFnSc1	paní
Stravinská	Stravinský	k2eAgFnSc1d1	Stravinský
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
těhotenství	těhotenství	k1gNnSc6	těhotenství
převezena	převézt	k5eAaPmNgFnS	převézt
na	na	k7c4	na
kliniku	klinika	k1gFnSc4	klinika
do	do	k7c2	do
Lausanne	Lausanne	k1gNnSc2	Lausanne
a	a	k8xC	a
i	i	k9	i
Igor	Igor	k1gMnSc1	Igor
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ubytoval	ubytovat	k5eAaPmAgMnS	ubytovat
<g/>
.	.	kIx.	.
</s>
<s>
Najal	najmout	k5eAaPmAgMnS	najmout
si	se	k3xPyFc3	se
podkroví	podkroví	k1gNnSc1	podkroví
domu	dům	k1gInSc2	dům
přes	přes	k7c4	přes
ulici	ulice	k1gFnSc4	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
Petruškovi	petruška	k1gMnSc6	petruška
<g/>
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Stravinským	Stravinský	k2eAgFnPc3d1	Stravinský
narodil	narodit	k5eAaPmAgMnS	narodit
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
Sulima	Sulimum	k1gNnSc2	Sulimum
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
také	také	k9	také
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
hotový	hotový	k2eAgMnSc1d1	hotový
již	již	k6eAd1	již
celý	celý	k2eAgInSc4d1	celý
druhý	druhý	k4xOgInSc4	druhý
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
v	v	k7c6	v
Beaulieu	Beaulieus	k1gInSc6	Beaulieus
(	(	kIx(	(
<g/>
u	u	k7c2	u
Nizzy	Nizza	k1gFnSc2	Nizza
<g/>
)	)	kIx)	)
partituru	partitura	k1gFnSc4	partitura
dokončil	dokončit	k5eAaPmAgMnS	dokončit
a	a	k8xC	a
odeslal	odeslat	k5eAaPmAgMnS	odeslat
ji	on	k3xPp3gFnSc4	on
Sergeji	Sergej	k1gMnSc3	Sergej
Kusevickému	Kusevický	k2eAgMnSc3d1	Kusevický
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kusevickij	Kusevickij	k1gMnSc1	Kusevickij
vydá	vydat	k5eAaPmIp3nS	vydat
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
mu	on	k3xPp3gMnSc3	on
Stravinskij	Stravinskij	k1gFnSc1	Stravinskij
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
domluvit	domluvit	k5eAaPmF	domluvit
scénář	scénář	k1gInSc4	scénář
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Benoisem	Benois	k1gInSc7	Benois
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
léčit	léčit	k5eAaImF	léčit
z	z	k7c2	z
akutní	akutní	k2eAgFnSc2d1	akutní
otravy	otrava	k1gFnSc2	otrava
nikotinem	nikotin	k1gInSc7	nikotin
<g/>
.	.	kIx.	.
</s>
<s>
Petruška	petruška	k1gMnSc1	petruška
měl	mít	k5eAaImAgMnS	mít
nakonec	nakonec	k6eAd1	nakonec
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
ani	ani	k8xC	ani
Ďagilev	Ďagilev	k1gMnPc1	Ďagilev
nečekali	čekat	k5eNaImAgMnP	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
představil	představit	k5eAaPmAgMnS	představit
Debussy	Debussa	k1gFnPc4	Debussa
Stravinskému	Stravinský	k2eAgInSc3d1	Stravinský
Erika	Erik	k1gMnSc4	Erik
Satieho	Satie	k1gMnSc4	Satie
a	a	k8xC	a
Stravinskij	Stravinskij	k1gMnSc4	Stravinskij
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
vyfotografoval	vyfotografovat	k5eAaPmAgMnS	vyfotografovat
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
postava	postava	k1gFnSc1	postava
tohoto	tento	k3xDgInSc2	tento
baletu	balet	k1gInSc2	balet
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
ruské	ruský	k2eAgFnSc2d1	ruská
lidové	lidový	k2eAgFnSc2d1	lidová
hry	hra	k1gFnSc2	hra
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
potulných	potulný	k2eAgMnPc2d1	potulný
komediantů	komediant	k1gMnPc2	komediant
a	a	k8xC	a
jarmarečních	jarmareční	k2eAgMnPc2d1	jarmareční
loutkářů	loutkář	k1gMnPc2	loutkář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
Théâtre	Théâtr	k1gInSc5	Théâtr
des	des	k1gNnSc3	des
Champs-Élysées	Champs-Élysées	k1gMnSc1	Champs-Élysées
premiéru	premiéra	k1gFnSc4	premiéra
Stravinského	Stravinský	k2eAgMnSc2d1	Stravinský
skandální	skandální	k2eAgInSc4d1	skandální
balet	balet	k1gInSc4	balet
Svěcení	svěcení	k1gNnSc2	svěcení
jara	jaro	k1gNnSc2	jaro
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc5	Le
Sacre	Sacr	k1gMnSc5	Sacr
du	du	k?	du
Printemps	Printemps	k1gInSc1	Printemps
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
pohanského	pohanský	k2eAgNnSc2d1	pohanské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
inspirované	inspirovaný	k2eAgNnSc1d1	inspirované
starým	starý	k2eAgInSc7d1	starý
rituálem	rituál	k1gInSc7	rituál
uctívání	uctívání	k1gNnSc2	uctívání
příchodu	příchod	k1gInSc2	příchod
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
malířem	malíř	k1gMnSc7	malíř
Nicholasem	Nicholas	k1gMnSc7	Nicholas
Roerichem	Roerich	k1gMnSc7	Roerich
<g/>
,	,	kIx,	,
způsobily	způsobit	k5eAaPmAgFnP	způsobit
při	při	k7c6	při
premiéře	premiéra	k1gFnSc6	premiéra
legendární	legendární	k2eAgInSc4d1	legendární
poprask	poprask	k1gInSc4	poprask
<g/>
,	,	kIx,	,
vyvolaný	vyvolaný	k2eAgInSc4d1	vyvolaný
jak	jak	k9	jak
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
revoluční	revoluční	k2eAgFnSc7d1	revoluční
choreografií	choreografie	k1gFnSc7	choreografie
Vjačeslava	Vjačeslav	k1gMnSc2	Vjačeslav
Nižinského	Nižinský	k2eAgMnSc2d1	Nižinský
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
i	i	k9	i
Nižinskij	Nižinskij	k1gMnPc1	Nižinskij
byli	být	k5eAaImAgMnP	být
neúspěchem	neúspěch	k1gInSc7	neúspěch
zklamáni	zklamat	k5eAaPmNgMnP	zklamat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
podle	podle	k7c2	podle
Ďagilevových	Ďagilevův	k2eAgNnPc2d1	Ďagilevův
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
stalo	stát	k5eAaPmAgNnS	stát
přesně	přesně	k6eAd1	přesně
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
koncertní	koncertní	k2eAgNnSc4d1	koncertní
provedení	provedení	k1gNnSc4	provedení
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nadšení	nadšení	k1gNnSc3	nadšení
obecenstva	obecenstvo	k1gNnSc2	obecenstvo
tak	tak	k6eAd1	tak
veliké	veliký	k2eAgFnPc1d1	veliká
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stravinskému	Stravinský	k2eAgInSc3d1	Stravinský
provolávalo	provolávat	k5eAaImAgNnS	provolávat
slávu	sláva	k1gFnSc4	sláva
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
síně	síň	k1gFnSc2	síň
vynesen	vynést	k5eAaPmNgInS	vynést
na	na	k7c6	na
ramenou	rameno	k1gNnPc6	rameno
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
novátorské	novátorský	k2eAgNnSc1d1	novátorské
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
ohledu	ohled	k1gInSc6	ohled
(	(	kIx(	(
<g/>
polytonalita	polytonalita	k1gFnSc1	polytonalita
<g/>
,	,	kIx,	,
polyrytmika	polyrytmika	k1gFnSc1	polyrytmika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
rytmus	rytmus	k1gInSc4	rytmus
a	a	k8xC	a
na	na	k7c4	na
posluchače	posluchač	k1gMnPc4	posluchač
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
řadách	řada	k1gFnPc6	řada
hudba	hudba	k1gFnSc1	hudba
působí	působit	k5eAaImIp3nS	působit
doslova	doslova	k6eAd1	doslova
fyzicky	fyzicky	k6eAd1	fyzicky
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
pracoval	pracovat	k5eAaImAgMnS	pracovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
na	na	k7c6	na
opeře	opera	k1gFnSc6	opera
Slavík	slavík	k1gInSc1	slavík
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Rossignol	Rossignola	k1gFnPc2	Rossignola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
přerušila	přerušit	k5eAaPmAgFnS	přerušit
Ďagilevova	Ďagilevův	k2eAgFnSc1d1	Ďagilevův
nabídka	nabídka	k1gFnSc1	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
se	se	k3xPyFc4	se
však	však	k9	však
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
opět	opět	k6eAd1	opět
(	(	kIx(	(
<g/>
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
Svěcení	svěcení	k1gNnSc2	svěcení
jara	jaro	k1gNnSc2	jaro
<g/>
)	)	kIx)	)
vrátil	vrátit	k5eAaPmAgInS	vrátit
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgInS	dokončit
jí	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
kontrast	kontrast	k1gInSc1	kontrast
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
.	.	kIx.	.
dějstvím	dějství	k1gNnSc7	dějství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
operní	operní	k2eAgFnSc3d1	operní
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
dějstvím	dějství	k1gNnSc7	dějství
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
baletní	baletní	k2eAgFnPc4d1	baletní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
byla	být	k5eAaImAgFnS	být
opera	opera	k1gFnSc1	opera
uvedena	uvést	k5eAaPmNgFnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Stravinský	Stravinský	k2eAgMnSc1d1	Stravinský
zde	zde	k6eAd1	zde
použil	použít	k5eAaPmAgMnS	použít
za	za	k7c4	za
námět	námět	k1gInSc4	námět
pohádku	pohádka	k1gFnSc4	pohádka
od	od	k7c2	od
H.	H.	kA	H.
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Andersena	Andersen	k1gMnSc4	Andersen
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
pohádky	pohádka	k1gFnPc4	pohádka
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
opera	opera	k1gFnSc1	opera
hýří	hýřit	k5eAaImIp3nS	hýřit
barvitou	barvitý	k2eAgFnSc7d1	barvitá
instrumentací	instrumentace	k1gFnSc7	instrumentace
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
ruského	ruský	k2eAgInSc2d1	ruský
impresionismu	impresionismus	k1gInSc2	impresionismus
a	a	k8xC	a
exotikou	exotika	k1gFnSc7	exotika
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k1gNnSc1	další
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
balet	balet	k1gInSc1	balet
Svatba	svatba	k1gFnSc1	svatba
(	(	kIx(	(
<g/>
Svaděbka	Svaděbka	k1gFnSc1	Svaděbka
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Noces	Noces	k1gInSc1	Noces
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
komp.	komp.	k?	komp.
1914	[number]	k4	1914
-	-	kIx~	-
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zhudebněná	zhudebněný	k2eAgNnPc1d1	zhudebněné
ruská	ruský	k2eAgNnPc1d1	ruské
pořekadla	pořekadlo	k1gNnPc1	pořekadlo
a	a	k8xC	a
říkanky	říkanka	k1gFnPc1	říkanka
ve	v	k7c6	v
scénickém	scénický	k2eAgNnSc6d1	scénické
provedení	provedení	k1gNnSc6	provedení
pro	pro	k7c4	pro
zpěváky	zpěvák	k1gMnPc4	zpěvák
<g/>
,	,	kIx,	,
herce	herec	k1gMnPc4	herec
a	a	k8xC	a
tanečníky	tanečník	k1gMnPc4	tanečník
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
dlouho	dlouho	k6eAd1	dlouho
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
instrumentaci	instrumentace	k1gFnSc4	instrumentace
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dílo	dílo	k1gNnSc1	dílo
znělo	znět	k5eAaImAgNnS	znět
"	"	kIx"	"
<g/>
mechanicky	mechanicky	k6eAd1	mechanicky
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
neobvyklé	obvyklý	k2eNgNnSc4d1	neobvyklé
řešení	řešení	k1gNnSc4	řešení
čtyř	čtyři	k4xCgInPc2	čtyři
klavírů	klavír	k1gInPc2	klavír
a	a	k8xC	a
souboru	soubor	k1gInSc2	soubor
bicích	bicí	k2eAgInPc2d1	bicí
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
také	také	k9	také
nejsou	být	k5eNaImIp3nP	být
zpěváci	zpěvák	k1gMnPc1	zpěvák
ztotožněni	ztotožněn	k2eAgMnPc1d1	ztotožněn
s	s	k7c7	s
herci	herec	k1gMnPc7	herec
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
až	až	k9	až
za	za	k7c4	za
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
v	v	k7c6	v
pařížském	pařížský	k2eAgNnSc6d1	pařížské
divadle	divadlo	k1gNnSc6	divadlo
Sáry	Sára	k1gFnSc2	Sára
Bernhardtové	Bernhardtová	k1gFnSc2	Bernhardtová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
dokončil	dokončit	k5eAaPmAgInS	dokončit
balet	balet	k1gInSc1	balet
Příběh	příběh	k1gInSc1	příběh
vojáka	voják	k1gMnSc2	voják
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Histoire	Histoir	k1gInSc5	Histoir
du	du	k?	du
soldat	soldat	k5eAaPmF	soldat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
se	s	k7c7	s
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Charlesem	Charles	k1gMnSc7	Charles
Ramuzem	Ramuz	k1gInSc7	Ramuz
<g/>
.	.	kIx.	.
</s>
<s>
Opustil	opustit	k5eAaPmAgMnS	opustit
zde	zde	k6eAd1	zde
zásady	zásada	k1gFnPc4	zásada
"	"	kIx"	"
<g/>
ruské	ruský	k2eAgFnSc2d1	ruská
orchestrální	orchestrální	k2eAgFnSc2d1	orchestrální
školy	škola	k1gFnSc2	škola
<g/>
"	"	kIx"	"
a	a	k8xC	a
použil	použít	k5eAaPmAgMnS	použít
jazzové	jazzový	k2eAgInPc4d1	jazzový
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
instrumentaci	instrumentace	k1gFnSc6	instrumentace
a	a	k8xC	a
rytmu	rytmus	k1gInSc6	rytmus
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jazz	jazz	k1gInSc1	jazz
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nikdy	nikdy	k6eAd1	nikdy
neslyšel	slyšet	k5eNaImAgMnS	slyšet
(	(	kIx(	(
<g/>
znal	znát	k5eAaImAgMnS	znát
ho	on	k3xPp3gMnSc4	on
jen	jen	k9	jen
z	z	k7c2	z
notového	notový	k2eAgInSc2d1	notový
zápisu	zápis	k1gInSc2	zápis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Námět	námět	k1gInSc1	námět
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Afanasjevovy	Afanasjevův	k2eAgFnSc2d1	Afanasjevův
bajky	bajka	k1gFnSc2	bajka
o	o	k7c6	o
vojákovi	voják	k1gMnSc6	voják
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
prohraje	prohrát	k5eAaPmIp3nS	prohrát
svoje	svůj	k3xOyFgFnPc4	svůj
housle	housle	k1gFnPc4	housle
(	(	kIx(	(
<g/>
duši	duše	k1gFnSc4	duše
<g/>
)	)	kIx)	)
s	s	k7c7	s
čertem	čert	k1gMnSc7	čert
v	v	k7c6	v
kartách	karta	k1gFnPc6	karta
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
ve	v	k7c6	v
skromných	skromný	k2eAgFnPc6d1	skromná
podmínkách	podmínka	k1gFnPc6	podmínka
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Příběh	příběh	k1gInSc1	příběh
vojáka	voják	k1gMnSc2	voják
znamená	znamenat	k5eAaImIp3nS	znamenat
můj	můj	k3xOp1gInSc4	můj
definitivní	definitivní	k2eAgInSc4d1	definitivní
rozchod	rozchod	k1gInSc4	rozchod
s	s	k7c7	s
ruskou	ruský	k2eAgFnSc7d1	ruská
orchestrální	orchestrální	k2eAgFnSc7d1	orchestrální
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
odchován	odchovat	k5eAaPmNgMnS	odchovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
S	s	k7c7	s
jazzem	jazz	k1gInSc7	jazz
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc1	ten
Ďagilev	Ďagilev	k1gMnSc1	Ďagilev
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
dal	dát	k5eAaPmAgMnS	dát
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
rozkošné	rozkošný	k2eAgFnSc2d1	rozkošná
skladby	skladba	k1gFnSc2	skladba
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
"	"	kIx"	"
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
zinstrumentovat	zinstrumentovat	k5eAaPmF	zinstrumentovat
hudbu	hudba	k1gFnSc4	hudba
"	"	kIx"	"
<g/>
jistého	jistý	k2eAgMnSc2d1	jistý
<g/>
"	"	kIx"	"
Giambattisty	Giambattista	k1gMnSc2	Giambattista
Pergolesiho	Pergolesi	k1gMnSc2	Pergolesi
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
zpočátku	zpočátku	k6eAd1	zpočátku
váhal	váhat	k5eAaImAgMnS	váhat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
do	do	k7c2	do
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
díla	dílo	k1gNnSc2	dílo
záměrně	záměrně	k6eAd1	záměrně
přidal	přidat	k5eAaPmAgMnS	přidat
mnoho	mnoho	k4c4	mnoho
pozdějších	pozdní	k2eAgInPc2d2	pozdější
hudebních	hudební	k2eAgInPc2d1	hudební
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
nezapadajících	zapadající	k2eNgInPc2d1	nezapadající
do	do	k7c2	do
hudby	hudba	k1gFnSc2	hudba
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
např.	např.	kA	např.
glissanda	glissando	k1gNnSc2	glissando
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc1	libreto
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Pergolesiho	Pergolesi	k1gMnSc2	Pergolesi
oper	opera	k1gFnPc2	opera
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
kantáty	kantáta	k1gFnSc2	kantáta
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ve	v	k7c6	v
Svatbě	svatba	k1gFnSc6	svatba
nejsou	být	k5eNaImIp3nP	být
zpěváci	zpěvák	k1gMnPc1	zpěvák
ztotožněni	ztotožněn	k2eAgMnPc1d1	ztotožněn
s	s	k7c7	s
postavami	postava	k1gFnPc7	postava
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1920	[number]	k4	1920
měl	mít	k5eAaImAgInS	mít
Pulcinella	pulcinello	k1gMnSc4	pulcinello
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
scény	scéna	k1gFnSc2	scéna
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
Pablo	Pablo	k1gNnSc4	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
hudební	hudební	k2eAgMnSc1d1	hudební
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
stránka	stránka	k1gFnSc1	stránka
baletu	balet	k1gInSc2	balet
pohoršily	pohoršit	k5eAaPmAgInP	pohoršit
nejen	nejen	k6eAd1	nejen
samotného	samotný	k2eAgMnSc4d1	samotný
Ďagileva	Ďagilev	k1gMnSc4	Ďagilev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nařkli	nařknout	k5eAaPmAgMnP	nařknout
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
z	z	k7c2	z
"	"	kIx"	"
<g/>
plagiátorství	plagiátorství	k1gNnSc2	plagiátorství
<g/>
"	"	kIx"	"
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
opouští	opouštět	k5eAaImIp3nS	opouštět
modernismus	modernismus	k1gInSc4	modernismus
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
kritikům	kritik	k1gMnPc3	kritik
(	(	kIx(	(
<g/>
i	i	k9	i
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
tímto	tento	k3xDgNnSc7	tento
dílem	dílo	k1gNnSc7	dílo
znepřátelil	znepřátelit	k5eAaPmAgInS	znepřátelit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
uměleckých	umělecký	k2eAgMnPc2d1	umělecký
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Ravela	Ravel	k1gMnSc2	Ravel
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
Stravinský	Stravinský	k2eAgMnSc1d1	Stravinský
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
baletu	balet	k1gInSc2	balet
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sledoval	sledovat	k5eAaImAgInS	sledovat
až	až	k6eAd1	až
do	do	k7c2	do
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nic	nic	k6eAd1	nic
nevěděli	vědět	k5eNaImAgMnP	vědět
o	o	k7c6	o
mých	můj	k3xOp1gFnPc6	můj
skutečných	skutečný	k2eAgFnPc6d1	skutečná
pohnutkách	pohnutka	k1gFnPc6	pohnutka
a	a	k8xC	a
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
nestarali	starat	k5eNaImAgMnP	starat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gMnPc4	on
poznali	poznat	k5eAaPmAgMnP	poznat
<g/>
,	,	kIx,	,
mě	já	k3xPp1nSc4	já
nařkli	nařknout	k5eAaPmAgMnP	nařknout
ze	z	k7c2	z
svatokrádeže	svatokrádež	k1gFnSc2	svatokrádež
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jen	jen	k9	jen
ať	ať	k9	ať
nechá	nechat	k5eAaPmIp3nS	nechat
klasiky	klasika	k1gFnPc4	klasika
na	na	k7c6	na
pokoji	pokoj	k1gInSc6	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
mu	on	k3xPp3gMnSc3	on
nedovolíme	dovolit	k5eNaPmIp1nP	dovolit
sáhnout	sáhnout	k5eAaPmF	sáhnout
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Pro	pro	k7c4	pro
tyhle	tenhle	k3xDgMnPc4	tenhle
lidi	člověk	k1gMnPc4	člověk
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgInS	mít
a	a	k8xC	a
mám	mít	k5eAaImIp1nS	mít
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
:	:	kIx,	:
Vy	vy	k3xPp2nPc1	vy
uctíváte	uctívat	k5eAaImIp2nP	uctívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
já	já	k3xPp1nSc1	já
miluji	milovat	k5eAaImIp1nS	milovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
Stravinských	Stravinský	k2eAgMnPc2d1	Stravinský
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
nakrátko	nakrátko	k6eAd1	nakrátko
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstali	zůstat	k5eAaPmAgMnP	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
dalším	další	k2eAgNnSc7d1	další
bydlištěm	bydliště	k1gNnSc7	bydliště
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
opět	opět	k6eAd1	opět
Paříž	Paříž	k1gFnSc4	Paříž
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Práci	práce	k1gFnSc4	práce
na	na	k7c6	na
Oktetu	okteto	k1gNnSc6	okteto
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
zapříčinil	zapříčinit	k5eAaPmAgInS	zapříčinit
sen	sen	k1gInSc1	sen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
Stravinskému	Stravinský	k2eAgInSc3d1	Stravinský
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
slyší	slyšet	k5eAaImIp3nS	slyšet
osm	osm	k4xCc1	osm
hudebníků	hudebník	k1gMnPc2	hudebník
hrající	hrající	k2eAgFnSc4d1	hrající
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
rozpomenout	rozpomenout	k5eAaPmF	rozpomenout
<g/>
,	,	kIx,	,
jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
si	se	k3xPyFc3	se
pamatoval	pamatovat	k5eAaImAgMnS	pamatovat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
nástroje	nástroj	k1gInPc1	nástroj
-	-	kIx~	-
flétna	flétna	k1gFnSc1	flétna
<g/>
,	,	kIx,	,
klarinet	klarinet	k1gInSc1	klarinet
<g/>
,	,	kIx,	,
fagoty	fagot	k1gInPc1	fagot
<g/>
,	,	kIx,	,
trubky	trubka	k1gFnPc1	trubka
a	a	k8xC	a
trombóny	trombón	k1gInPc1	trombón
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
ráno	ráno	k6eAd1	ráno
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
kompozicí	kompozice	k1gFnSc7	kompozice
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
práci	práce	k1gFnSc4	práce
dokončil	dokončit	k5eAaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
provedení	provedení	k1gNnSc4	provedení
koncertu	koncert	k1gInSc2	koncert
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
sám	sám	k3xTgMnSc1	sám
(	(	kIx(	(
<g/>
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
prý	prý	k9	prý
strašnou	strašný	k2eAgFnSc4d1	strašná
trému	tréma	k1gFnSc4	tréma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
neoklasicismu	neoklasicismus	k1gInSc2	neoklasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Hybrid	hybrid	k1gInSc1	hybrid
<g/>
,	,	kIx,	,
operu-oratorium	operuratorium	k1gNnSc1	operu-oratorium
Oedipus	Oedipus	k1gMnSc1	Oedipus
Rex	Rex	k1gMnSc1	Rex
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
komponovat	komponovat	k5eAaImF	komponovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
dlouho	dlouho	k6eAd1	dlouho
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
napíše	napsat	k5eAaBmIp3nS	napsat
"	"	kIx"	"
<g/>
široce	široko	k6eAd1	široko
založené	založený	k2eAgNnSc1d1	založené
<g/>
"	"	kIx"	"
dramatické	dramatický	k2eAgNnSc4d1	dramatické
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
vybíral	vybírat	k5eAaImAgMnS	vybírat
v	v	k7c6	v
jakém	jaký	k3yQgInSc6	jaký
jazyce	jazyk	k1gInSc6	jazyk
dílo	dílo	k1gNnSc4	dílo
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ruštinu	ruština	k1gFnSc4	ruština
předem	předem	k6eAd1	předem
zavrhl	zavrhnout	k5eAaPmAgMnS	zavrhnout
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
nebo	nebo	k8xC	nebo
italština	italština	k1gFnSc1	italština
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdály	zdát	k5eAaImAgFnP	zdát
svým	svůj	k3xOyFgInSc7	svůj
temperamentem	temperament	k1gInSc7	temperament
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
ciceronskou	ciceronský	k2eAgFnSc4d1	ciceronský
latinu	latina	k1gFnSc4	latina
<g/>
.	.	kIx.	.
</s>
<s>
Námět	námět	k1gInSc1	námět
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
všeobecně	všeobecně	k6eAd1	všeobecně
známý	známý	k2eAgMnSc1d1	známý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
nemusel	muset	k5eNaImAgInS	muset
zdlouhavě	zdlouhavě	k6eAd1	zdlouhavě
představovat	představovat	k5eAaImF	představovat
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
soustředit	soustředit	k5eAaPmF	soustředit
plně	plně	k6eAd1	plně
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
padla	padnout	k5eAaPmAgFnS	padnout
na	na	k7c4	na
Sofoklova	Sofoklův	k2eAgMnSc4d1	Sofoklův
Oidipa	Oidipus	k1gMnSc4	Oidipus
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
znal	znát	k5eAaImAgInS	znát
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
libreta	libreto	k1gNnSc2	libreto
spisovatele	spisovatel	k1gMnSc2	spisovatel
Jeana	Jean	k1gMnSc2	Jean
Cocteaua	Cocteauus	k1gMnSc2	Cocteauus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nabídku	nabídka	k1gFnSc4	nabídka
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
Cocteau	Coctea	k1gMnSc6	Coctea
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
přidat	přidat	k5eAaPmF	přidat
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
vypravěče	vypravěč	k1gMnSc2	vypravěč
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
později	pozdě	k6eAd2	pozdě
tento	tento	k3xDgInSc4	tento
nápad	nápad	k1gInSc4	nápad
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Skladatel	skladatel	k1gMnSc1	skladatel
sám	sám	k3xTgMnSc1	sám
řídil	řídit	k5eAaImAgMnS	řídit
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Sáry	Sára	k1gFnSc2	Sára
Bernhardtové	Bernhardtová	k1gFnSc2	Bernhardtová
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
chladně	chladně	k6eAd1	chladně
a	a	k8xC	a
rozporuplně	rozporuplně	k6eAd1	rozporuplně
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
vrcholem	vrchol	k1gInSc7	vrchol
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
neoklasicismu	neoklasicismus	k1gInSc2	neoklasicismus
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
sám	sám	k3xTgMnSc1	sám
označuje	označovat	k5eAaImIp3nS	označovat
dílo	dílo	k1gNnSc1	dílo
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
statické	statický	k2eAgNnSc1d1	statické
drama	drama	k1gNnSc1	drama
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
zpěváci	zpěvák	k1gMnPc1	zpěvák
přímo	přímo	k6eAd1	přímo
uloženo	uložen	k2eAgNnSc1d1	uloženo
se	se	k3xPyFc4	se
nehýbat	hýbat	k5eNaImF	hýbat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
premiéře	premiéra	k1gFnSc6	premiéra
Oedipa	Oedip	k1gMnSc2	Oedip
Rexe	Rex	k1gMnSc2	Rex
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
přišel	přijít	k5eAaPmAgInS	přijít
Stravinskému	Stravinský	k2eAgMnSc3d1	Stravinský
blahopřát	blahopřát	k5eAaImF	blahopřát
i	i	k9	i
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1934	[number]	k4	1934
získal	získat	k5eAaPmAgInS	získat
Stravinskij	Stravinskij	k1gFnSc7	Stravinskij
francouzské	francouzský	k2eAgNnSc4d1	francouzské
občanství	občanství	k1gNnSc4	občanství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
musel	muset	k5eAaImAgMnS	muset
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
uprchnout	uprchnout	k5eAaPmF	uprchnout
před	před	k7c7	před
německými	německý	k2eAgInPc7d1	německý
vojsky	vojsky	k6eAd1	vojsky
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Stravinského	Stravinský	k2eAgMnSc4d1	Stravinský
tragický	tragický	k2eAgInSc4d1	tragický
i	i	k9	i
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřely	zemřít	k5eAaPmAgFnP	zemřít
jeho	jeho	k3xOp3gNnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
i	i	k8xC	i
dcera	dcera	k1gFnSc1	dcera
na	na	k7c4	na
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
nemoc	nemoc	k1gFnSc4	nemoc
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
sám	sám	k3xTgInSc1	sám
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
bolestivě	bolestivě	k6eAd1	bolestivě
léčil	léčit	k5eAaImAgMnS	léčit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c2	za
Stravinského	Stravinský	k2eAgNnSc2d1	Stravinský
tvůrčí	tvůrčí	k2eAgFnPc4d1	tvůrčí
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
však	však	k9	však
brzo	brzo	k6eAd1	brzo
vyprostil	vyprostit	k5eAaPmAgMnS	vyprostit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Věrou	Věra	k1gFnSc7	Věra
Sudejkinovou	Sudejkinová	k1gFnSc7	Sudejkinová
(	(	kIx(	(
<g/>
de	de	k?	de
Bosset	Bosset	k1gInSc1	Bosset
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
stráveném	strávený	k2eAgInSc6d1	strávený
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
i	i	k9	i
Sergej	Sergej	k1gMnSc1	Sergej
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
získal	získat	k5eAaPmAgMnS	získat
americké	americký	k2eAgNnSc4d1	americké
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
skladby	skladba	k1gFnPc1	skladba
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
opera	opera	k1gFnSc1	opera
Persefona	Persefona	k1gFnSc1	Persefona
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
na	na	k7c4	na
které	který	k3yQgInPc4	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
se	s	k7c7	s
spisovatelem	spisovatel	k1gMnSc7	spisovatel
André	André	k1gMnSc7	André
Gidem	Gid	k1gMnSc7	Gid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Symfonie	symfonie	k1gFnSc1	symfonie
v	v	k7c6	v
C	C	kA	C
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Symfonie	symfonie	k1gFnSc1	symfonie
o	o	k7c6	o
třech	tři	k4xCgFnPc6	tři
větách	věta	k1gFnPc6	věta
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
balet	balet	k1gInSc1	balet
Orfeus	Orfeus	k1gMnSc1	Orfeus
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
skladba	skladba	k1gFnSc1	skladba
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
neoklasicismu	neoklasicismus	k1gInSc2	neoklasicismus
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
Život	život	k1gInSc1	život
prostopášníka	prostopášník	k1gMnSc2	prostopášník
(	(	kIx(	(
<g/>
The	The	k1gMnSc2	The
Rake	Rak	k1gMnSc2	Rak
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Progress	Progress	k1gInSc1	Progress
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
obrazů	obraz	k1gInPc2	obraz
klasicistního	klasicistní	k2eAgMnSc2d1	klasicistní
malíře	malíř	k1gMnSc2	malíř
Williama	William	k1gMnSc2	William
Hogartha	Hogarth	k1gMnSc2	Hogarth
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
W.	W.	kA	W.
H.	H.	kA	H.
Audena	Auden	k1gMnSc2	Auden
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
přátelil	přátelit	k5eAaImAgInS	přátelit
a	a	k8xC	a
úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
dirigentem	dirigent	k1gMnSc7	dirigent
Robertem	Robert	k1gMnSc7	Robert
Craftem	Craft	k1gMnSc7	Craft
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
premiéry	premiéra	k1gFnPc4	premiéra
některých	některý	k3yIgFnPc2	některý
jeho	jeho	k3xOp3gFnPc2	jeho
posledních	poslední	k2eAgFnPc2d1	poslední
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
zastával	zastávat	k5eAaImAgMnS	zastávat
opozici	opozice	k1gFnSc4	opozice
k	k	k7c3	k
teoriím	teorie	k1gFnPc3	teorie
skladatele	skladatel	k1gMnSc2	skladatel
Arnolda	Arnold	k1gMnSc2	Arnold
Schönberga	Schönberg	k1gMnSc2	Schönberg
<g/>
,	,	kIx,	,
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
nakonec	nakonec	k6eAd1	nakonec
Craft	Craft	k1gInSc4	Craft
<g/>
,	,	kIx,	,
Schönbergův	Schönbergův	k2eAgMnSc1d1	Schönbergův
obdivovatel	obdivovatel	k1gMnSc1	obdivovatel
<g/>
,	,	kIx,	,
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
k	k	k7c3	k
experimentům	experiment	k1gInPc3	experiment
se	s	k7c7	s
serialismem	serialismus	k1gInSc7	serialismus
nebo	nebo	k8xC	nebo
neoserialismem	neoserialismus	k1gInSc7	neoserialismus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Výsledkem	výsledek	k1gInSc7	výsledek
těchto	tento	k3xDgInPc2	tento
pokusů	pokus	k1gInPc2	pokus
jsou	být	k5eAaImIp3nP	být
díla	dílo	k1gNnPc1	dílo
jako	jako	k8xS	jako
Agon	agon	k1gInSc1	agon
a	a	k8xC	a
Canticum	Canticum	k1gInSc1	Canticum
Sacrum	Sacrum	k1gNnSc4	Sacrum
aj.	aj.	kA	aj.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
padesáti	padesát	k4xCc6	padesát
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
nadšeně	nadšeně	k6eAd1	nadšeně
přijat	přijmout	k5eAaPmNgMnS	přijmout
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgMnS	provést
tu	ten	k3xDgFnSc4	ten
sérii	série	k1gFnSc4	série
koncertů	koncert	k1gInPc2	koncert
svých	svůj	k3xOyFgInPc2	svůj
děl	dít	k5eAaImAgMnS	dít
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgMnS	navštívit
jeho	jeho	k3xOp3gInSc4	jeho
milovaný	milovaný	k2eAgInSc4d1	milovaný
Petrohrad	Petrohrad	k1gInSc4	Petrohrad
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
již	již	k9	již
Leningrad	Leningrad	k1gInSc1	Leningrad
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
i	i	k9	i
s	s	k7c7	s
Dmitrijem	Dmitrij	k1gMnSc7	Dmitrij
Šostakovičem	Šostakovič	k1gMnSc7	Šostakovič
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
setkání	setkání	k1gNnSc1	setkání
příliš	příliš	k6eAd1	příliš
nevydařilo	vydařit	k5eNaPmAgNnS	vydařit
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
skladatelé	skladatel	k1gMnPc1	skladatel
prohodili	prohodit	k5eAaPmAgMnP	prohodit
jen	jen	k6eAd1	jen
pár	pár	k4xCyI	pár
zdvořilostních	zdvořilostní	k2eAgFnPc2d1	zdvořilostní
frází	fráze	k1gFnPc2	fráze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
procestoval	procestovat	k5eAaPmAgMnS	procestovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Věrou	Věra	k1gFnSc7	Věra
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Craftem	Craft	k1gMnSc7	Craft
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc4d1	poslední
desetiletí	desetiletí	k1gNnSc4	desetiletí
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
zasvětil	zasvětit	k5eAaPmAgInS	zasvětit
také	také	k9	také
nahrávání	nahrávání	k1gNnSc2	nahrávání
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
za	za	k7c2	za
vlastního	vlastní	k2eAgNnSc2d1	vlastní
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
za	za	k7c7	za
sebou	se	k3xPyFc7	se
zanechal	zanechat	k5eAaPmAgMnS	zanechat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgInPc2d3	nejbohatší
souborů	soubor	k1gInPc2	soubor
nahrávek	nahrávka	k1gFnPc2	nahrávka
nežijícího	žijící	k2eNgMnSc2d1	nežijící
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
poslední	poslední	k2eAgFnSc1d1	poslední
skladbou	skladba	k1gFnSc7	skladba
je	být	k5eAaImIp3nS	být
Sova	sova	k1gFnSc1	sova
a	a	k8xC	a
kočka	kočka	k1gFnSc1	kočka
pro	pro	k7c4	pro
soprán	soprán	k1gInSc4	soprán
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
život	život	k1gInSc4	život
byl	být	k5eAaImAgMnS	být
celkem	celkem	k6eAd1	celkem
spořádaný	spořádaný	k2eAgMnSc1d1	spořádaný
a	a	k8xC	a
klidný	klidný	k2eAgMnSc1d1	klidný
<g/>
.	.	kIx.	.
</s>
<s>
Nepotrpěl	potrpět	k5eNaPmAgMnS	potrpět
si	se	k3xPyFc3	se
na	na	k7c4	na
žádné	žádný	k3yNgFnPc4	žádný
extravagance	extravagance	k1gFnPc4	extravagance
a	a	k8xC	a
takové	takový	k3xDgInPc4	takový
projevy	projev	k1gInPc4	projev
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
kolegů	kolega	k1gMnPc2	kolega
často	často	k6eAd1	často
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
..	..	k?	..
Byl	být	k5eAaImAgMnS	být
optimistou	optimista	k1gMnSc7	optimista
<g/>
,	,	kIx,	,
nepropadal	propadat	k5eNaPmAgMnS	propadat
hlubokým	hluboký	k2eAgFnPc3d1	hluboká
depresím	deprese	k1gFnPc3	deprese
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
mu	on	k3xPp3gMnSc3	on
bránily	bránit	k5eAaImAgFnP	bránit
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
např.	např.	kA	např.
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jeho	on	k3xPp3gNnSc2	on
okolí	okolí	k1gNnSc2	okolí
neunikla	uniknout	k5eNaPmAgFnS	uniknout
ani	ani	k9	ani
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
hospodárnost	hospodárnost	k1gFnSc4	hospodárnost
a	a	k8xC	a
nejednou	jednou	k6eNd1	jednou
byl	být	k5eAaImAgInS	být
nařčen	nařčen	k2eAgInSc1d1	nařčen
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
na	na	k7c4	na
prachy	prach	k1gInPc4	prach
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ďagilev	Ďagilev	k1gFnSc1	Ďagilev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
byl	být	k5eAaImAgInS	být
udiven	udiven	k2eAgMnSc1d1	udiven
<g/>
,	,	kIx,	,
když	když	k8xS	když
spatřil	spatřit	k5eAaPmAgMnS	spatřit
jeho	on	k3xPp3gInSc4	on
pracovní	pracovní	k2eAgInSc4d1	pracovní
stůl	stůl	k1gInSc4	stůl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všechno	všechen	k3xTgNnSc1	všechen
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
srovnané	srovnaný	k2eAgNnSc1d1	srovnané
jako	jako	k9	jako
podle	podle	k7c2	podle
pravítka	pravítko	k1gNnSc2	pravítko
<g/>
.	.	kIx.	.
</s>
<s>
Vypadal	vypadat	k5eAaImAgMnS	vypadat
spíš	spíš	k9	spíš
jako	jako	k9	jako
stůl	stůl	k1gInSc4	stůl
chirurga	chirurg	k1gMnSc2	chirurg
<g/>
,	,	kIx,	,
než	než	k8xS	než
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Stravinskij	Stravinskij	k1gFnSc1	Stravinskij
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
kritický	kritický	k2eAgInSc1d1	kritický
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
k	k	k7c3	k
druhým	druhý	k4xOgMnPc3	druhý
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
:	:	kIx,	:
o	o	k7c6	o
Richardu	Richard	k1gMnSc6	Richard
Straussovi	Strauss	k1gMnSc6	Strauss
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgMnS	chtít
bych	by	kYmCp1nS	by
uznat	uznat	k5eAaPmF	uznat
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
opery	opera	k1gFnPc4	opera
hodny	hoden	k2eAgFnPc4d1	hodna
odsouzení	odsouzení	k1gNnSc4	odsouzení
do	do	k7c2	do
kteréhokoli	kterýkoli	k3yIgInSc2	kterýkoli
očistce	očistec	k1gInSc2	očistec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
potrestána	potrestat	k5eAaPmNgFnS	potrestat
jejich	jejich	k3xOp3gFnSc1	jejich
triumfální	triumfální	k2eAgFnSc1d1	triumfální
banalita	banalita	k1gFnSc1	banalita
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
laciný	laciný	k2eAgInSc1d1	laciný
a	a	k8xC	a
chudý	chudý	k2eAgInSc1d1	chudý
hudební	hudební	k2eAgInSc1d1	hudební
obsah	obsah	k1gInSc1	obsah
nemůže	moct	k5eNaImIp3nS	moct
dnešního	dnešní	k2eAgMnSc4d1	dnešní
hudebníka	hudebník	k1gMnSc4	hudebník
zajímat	zajímat	k5eAaImF	zajímat
<g/>
;	;	kIx,	;
nesnáším	snášet	k5eNaImIp1nS	snášet
jeho	jeho	k3xOp3gInPc4	jeho
kvartsextakordy	kvartsextakord	k1gInPc4	kvartsextakord
<g/>
,	,	kIx,	,
když	když	k8xS	když
slyším	slyšet	k5eAaImIp1nS	slyšet
Ariadnu	Ariadna	k1gFnSc4	Ariadna
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
ječet	ječet	k5eAaImF	ječet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
o	o	k7c6	o
Antoniu	Antonio	k1gMnSc6	Antonio
Vivaldim	Vivaldim	k1gInSc4	Vivaldim
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Obávám	obávat	k5eAaImIp1nS	obávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vivaldi	Vivald	k1gMnPc1	Vivald
napsal	napsat	k5eAaBmAgMnS	napsat
pětsetkrát	pětsetkrát	k6eAd1	pětsetkrát
týž	týž	k3xTgInSc4	týž
koncert	koncert	k1gInSc4	koncert
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
hudební	hudební	k2eAgFnSc1d1	hudební
mluva	mluva	k1gFnSc1	mluva
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
předkompozičního	předkompoziční	k2eAgNnSc2d1	předkompoziční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
součástí	součást	k1gFnSc7	součást
přímé	přímý	k2eAgFnSc2d1	přímá
hudební	hudební	k2eAgFnSc2d1	hudební
akce	akce	k1gFnSc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
věc	věc	k1gFnSc1	věc
vyjadřuji	vyjadřovat	k5eAaImIp1nS	vyjadřovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
věc	věc	k1gFnSc1	věc
vyjádřená	vyjádřený	k2eAgFnSc1d1	vyjádřená
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
totéž	týž	k3xTgNnSc4	týž
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
žil	žít	k5eAaImAgMnS	žít
celkem	celkem	k6eAd1	celkem
spořádaným	spořádaný	k2eAgInSc7d1	spořádaný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
umělec	umělec	k1gMnSc1	umělec
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
přísnou	přísný	k2eAgFnSc4d1	přísná
disciplínu	disciplína	k1gFnSc4	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Komponoval	komponovat	k5eAaImAgMnS	komponovat
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
přesně	přesně	k6eAd1	přesně
podle	podle	k7c2	podle
rozvrhu	rozvrh	k1gInSc2	rozvrh
(	(	kIx(	(
<g/>
sám	sám	k3xTgInSc1	sám
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
každý	každý	k3xTgMnSc1	každý
dělník	dělník	k1gMnSc1	dělník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
myšlenky	myšlenka	k1gFnSc2	myšlenka
ho	on	k3xPp3gMnSc4	on
napadají	napadat	k5eAaBmIp3nP	napadat
pouze	pouze	k6eAd1	pouze
když	když	k8xS	když
pracuje	pracovat	k5eAaImIp3nS	pracovat
a	a	k8xC	a
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
nezabývá	zabývat	k5eNaImIp3nS	zabývat
-	-	kIx~	-
údajně	údajně	k6eAd1	údajně
ho	on	k3xPp3gMnSc4	on
jednou	jednou	k6eAd1	jednou
napadlo	napadnout	k5eAaPmAgNnS	napadnout
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
téma	téma	k1gNnSc1	téma
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
zpracovat	zpracovat	k5eAaPmF	zpracovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
než	než	k8xS	než
přišel	přijít	k5eAaPmAgInS	přijít
domů	domů	k6eAd1	domů
k	k	k7c3	k
pracovnímu	pracovní	k2eAgInSc3d1	pracovní
stolu	stol	k1gInSc3	stol
<g/>
,	,	kIx,	,
téma	téma	k1gNnSc2	téma
zapomněl	zapomenout	k5eAaPmAgMnS	zapomenout
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
svobodnější	svobodný	k2eAgFnSc1d2	svobodnější
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
kontrolovanější	kontrolovaný	k2eAgFnSc1d2	kontrolovanější
a	a	k8xC	a
propracovanější	propracovaný	k2eAgFnSc1d2	propracovanější
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
plná	plný	k2eAgFnSc1d1	plná
intelektuálního	intelektuální	k2eAgInSc2d1	intelektuální
vtipu	vtip	k1gInSc2	vtip
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
,	,	kIx,	,
stylově	stylově	k6eAd1	stylově
vyhraněná	vyhraněný	k2eAgFnSc1d1	vyhraněná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nepředvídatelná	předvídatelný	k2eNgFnSc1d1	nepředvídatelná
–	–	k?	–
záměrně	záměrně	k6eAd1	záměrně
uhýbající	uhýbající	k2eAgMnSc1d1	uhýbající
a	a	k8xC	a
často	často	k6eAd1	často
plná	plný	k2eAgFnSc1d1	plná
podivné	podivný	k2eAgFnPc4d1	podivná
ironie	ironie	k1gFnPc4	ironie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
expresionistických	expresionistický	k2eAgNnPc2d1	expresionistické
a	a	k8xC	a
pozdně	pozdně	k6eAd1	pozdně
romantických	romantický	k2eAgMnPc2d1	romantický
současníků	současník	k1gMnPc2	současník
není	být	k5eNaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
příliš	příliš	k6eAd1	příliš
citově	citově	k6eAd1	citově
silná	silný	k2eAgFnSc1d1	silná
–	–	k?	–
dává	dávat	k5eAaImIp3nS	dávat
důraz	důraz	k1gInSc4	důraz
především	především	k9	především
na	na	k7c4	na
řemeslnou	řemeslný	k2eAgFnSc4d1	řemeslná
stránku	stránka	k1gFnSc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
Schönbergovi	Schönberg	k1gMnSc3	Schönberg
se	se	k3xPyFc4	se
nebrání	bránit	k5eNaImIp3nS	bránit
využívání	využívání	k1gNnSc1	využívání
starších	starý	k2eAgInPc2d2	starší
skladebných	skladebný	k2eAgInPc2d1	skladebný
postupů	postup	k1gInPc2	postup
a	a	k8xC	a
hledá	hledat	k5eAaImIp3nS	hledat
inspiraci	inspirace	k1gFnSc4	inspirace
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Neopouští	opouštět	k5eNaImIp3nS	opouštět
tonalitu	tonalita	k1gFnSc4	tonalita
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
využívá	využívat	k5eAaPmIp3nS	využívat
neobvyklých	obvyklý	k2eNgInPc2d1	neobvyklý
harmonických	harmonický	k2eAgInPc2d1	harmonický
prvků	prvek	k1gInPc2	prvek
jako	jako	k8xC	jako
polytonalita	polytonalita	k1gFnSc1	polytonalita
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
je	on	k3xPp3gFnPc4	on
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
stylové	stylový	k2eAgFnSc3d1	stylová
různorodosti	různorodost	k1gFnSc3	různorodost
označován	označován	k2eAgMnSc1d1	označován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
hudebních	hudební	k2eAgMnPc2d1	hudební
syntetiků	syntetik	k1gMnPc2	syntetik
<g/>
.	.	kIx.	.
</s>
<s>
Melodika	melodika	k1gFnSc1	melodika
je	být	k5eAaImIp3nS	být
tematická	tematický	k2eAgFnSc1d1	tematická
s	s	k7c7	s
častým	častý	k2eAgNnSc7d1	časté
opakováním	opakování	k1gNnSc7	opakování
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Rytmus	rytmus	k1gInSc1	rytmus
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
stránkou	stránka	k1gFnSc7	stránka
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
hlavním	hlavní	k2eAgInSc7d1	hlavní
formotvorným	formotvorný	k2eAgInSc7d1	formotvorný
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ostrý	ostrý	k2eAgInSc1d1	ostrý
a	a	k8xC	a
metronomicky	metronomicky	k6eAd1	metronomicky
přísný	přísný	k2eAgInSc1d1	přísný
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgNnSc1d1	běžné
je	být	k5eAaImIp3nS	být
přenášení	přenášení	k1gNnSc1	přenášení
důrazu	důraz	k1gInSc2	důraz
–	–	k?	–
synkopace	synkopace	k1gFnSc2	synkopace
<g/>
.	.	kIx.	.
</s>
<s>
Harmonie	harmonie	k1gFnSc1	harmonie
je	být	k5eAaImIp3nS	být
složitá	složitý	k2eAgFnSc1d1	složitá
<g/>
,	,	kIx,	,
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
tonálním	tonální	k2eAgNnSc7d1	tonální
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
je	být	k5eAaImIp3nS	být
bitonalita	bitonalita	k1gFnSc1	bitonalita
a	a	k8xC	a
polytonalita	polytonalita	k1gFnSc1	polytonalita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
prvních	první	k4xOgFnPc6	první
skladbách	skladba	k1gFnPc6	skladba
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Rimského-Korsakova	Rimského-Korsakův	k2eAgInSc2d1	Rimského-Korsakův
a	a	k8xC	a
francouzských	francouzský	k2eAgMnPc2d1	francouzský
impresionistů	impresionista	k1gMnPc2	impresionista
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
avantgardní	avantgardní	k2eAgNnSc1d1	avantgardní
prostředí	prostředí	k1gNnSc1	prostředí
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
"	"	kIx"	"
<g/>
Ruského	ruský	k2eAgInSc2d1	ruský
baletu	balet	k1gInSc2	balet
<g/>
"	"	kIx"	"
formují	formovat	k5eAaImIp3nP	formovat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
práci	práce	k1gFnSc6	práce
nový	nový	k2eAgInSc4d1	nový
výraz	výraz	k1gInSc4	výraz
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
měl	mít	k5eAaImAgMnS	mít
blízko	blízko	k6eAd1	blízko
již	již	k6eAd1	již
za	za	k7c2	za
studií	studio	k1gNnPc2	studio
u	u	k7c2	u
Rimského-Korsakova	Rimského-Korsakův	k2eAgNnSc2d1	Rimského-Korsakův
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
harmonickou	harmonický	k2eAgFnSc7d1	harmonická
složitostí	složitost	k1gFnSc7	složitost
a	a	k8xC	a
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
instrumentací	instrumentace	k1gFnSc7	instrumentace
pro	pro	k7c4	pro
velká	velký	k2eAgNnPc4d1	velké
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
balet	balet	k1gInSc1	balet
Pták	pták	k1gMnSc1	pták
Ohnivák	Ohnivák	k1gMnSc1	Ohnivák
stále	stále	k6eAd1	stále
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Rimského-Korsakova	Rimského-Korsakův	k2eAgNnSc2d1	Rimského-Korsakův
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přejímá	přejímat	k5eAaImIp3nS	přejímat
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
styl	styl	k1gInSc4	styl
impresionistů	impresionista	k1gMnPc2	impresionista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Petruškovi	petruška	k1gMnSc6	petruška
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
předchozích	předchozí	k2eAgInPc2d1	předchozí
vlivů	vliv	k1gInPc2	vliv
značně	značně	k6eAd1	značně
osvobozuje	osvobozovat	k5eAaImIp3nS	osvobozovat
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
zde	zde	k6eAd1	zde
užívá	užívat	k5eAaImIp3nS	užívat
polytonalitu	polytonalita	k1gFnSc4	polytonalita
a	a	k8xC	a
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
rytmus	rytmus	k1gInSc1	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholným	vrcholný	k2eAgInSc7d1	vrcholný
dílem	díl	k1gInSc7	díl
toho	ten	k3xDgNnSc2	ten
období	období	k1gNnSc2	období
je	být	k5eAaImIp3nS	být
balet	balet	k1gInSc1	balet
Svěcení	svěcení	k1gNnSc2	svěcení
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrají	hrát	k5eAaImIp3nP	hrát
rytmus	rytmus	k1gInSc4	rytmus
a	a	k8xC	a
témbry	témbr	k1gInPc4	témbr
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgInPc1d1	bicí
a	a	k8xC	a
dechové	dechový	k2eAgInPc1d1	dechový
nástroje	nástroj	k1gInPc1	nástroj
<g/>
)	)	kIx)	)
takovou	takový	k3xDgFnSc4	takový
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
hlavními	hlavní	k2eAgInPc7d1	hlavní
výrazovými	výrazový	k2eAgInPc7d1	výrazový
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
jazzových	jazzový	k2eAgMnPc2d1	jazzový
experimentů	experiment	k1gInPc2	experiment
převzal	převzít	k5eAaPmAgInS	převzít
především	především	k9	především
rytmickou	rytmický	k2eAgFnSc4d1	rytmická
bohatost	bohatost	k1gFnSc4	bohatost
a	a	k8xC	a
novou	nový	k2eAgFnSc4d1	nová
výraznou	výrazný	k2eAgFnSc4d1	výrazná
instrumentaci	instrumentace	k1gFnSc4	instrumentace
–	–	k?	–
dechové	dechový	k2eAgInPc4d1	dechový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Neoklasicismus	neoklasicismus	k1gInSc1	neoklasicismus
představuje	představovat	k5eAaImIp3nS	představovat
těžiště	těžiště	k1gNnSc1	těžiště
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Odvrátil	odvrátit	k5eAaPmAgInS	odvrátit
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
od	od	k7c2	od
expresivnosti	expresivnost	k1gFnSc2	expresivnost
a	a	k8xC	a
přebujelé	přebujelý	k2eAgFnSc2d1	přebujelá
instrumentace	instrumentace	k1gFnSc2	instrumentace
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
a	a	k8xC	a
propracovanosti	propracovanost	k1gFnSc3	propracovanost
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nic	nic	k3yNnSc1	nic
než	než	k8xS	než
hudba	hudba	k1gFnSc1	hudba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eliminuje	eliminovat	k5eAaBmIp3nS	eliminovat
zde	zde	k6eAd1	zde
mimohudební	mimohudební	k2eAgFnPc4d1	mimohudební
struktury	struktura	k1gFnPc4	struktura
a	a	k8xC	a
souvislosti	souvislost	k1gFnPc4	souvislost
běžné	běžný	k2eAgFnPc4d1	běžná
z	z	k7c2	z
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
řemeslnou	řemeslný	k2eAgFnSc4d1	řemeslná
čistotu	čistota	k1gFnSc4	čistota
<g/>
.	.	kIx.	.
</s>
<s>
Inspiruje	inspirovat	k5eAaBmIp3nS	inspirovat
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
–	–	k?	–
renesanční	renesanční	k2eAgFnSc1d1	renesanční
polyfonie	polyfonie	k1gFnSc1	polyfonie
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgFnSc1d1	barokní
fuga	fuga	k1gFnSc1	fuga
<g/>
,	,	kIx,	,
raný	raný	k2eAgInSc1d1	raný
klasicismus	klasicismus	k1gInSc1	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Příběhu	příběh	k1gInSc6	příběh
vojáka	voják	k1gMnSc4	voják
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
vlivy	vliv	k1gInPc1	vliv
moderní	moderní	k2eAgFnSc2d1	moderní
taneční	taneční	k2eAgFnSc2d1	taneční
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
amerického	americký	k2eAgInSc2d1	americký
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Baletem	balet	k1gInSc7	balet
Pulcinella	pulcinello	k1gMnSc2	pulcinello
již	již	k6eAd1	již
objevuje	objevovat	k5eAaImIp3nS	objevovat
svůj	svůj	k3xOyFgInSc4	svůj
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ještě	ještě	k6eAd1	ještě
stále	stále	k6eAd1	stále
použit	použít	k5eAaPmNgInS	použít
jen	jen	k9	jen
jako	jako	k8xS	jako
výrazový	výrazový	k2eAgInSc4d1	výrazový
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
skladatele	skladatel	k1gMnSc2	skladatel
Arnolda	Arnold	k1gMnSc2	Arnold
Schönberga	Schönberg	k1gMnSc2	Schönberg
se	se	k3xPyFc4	se
překvapivě	překvapivě	k6eAd1	překvapivě
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
k	k	k7c3	k
dodekafonii	dodekafonie	k1gFnSc3	dodekafonie
a	a	k8xC	a
seriální	seriální	k2eAgFnSc3d1	seriální
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
odmítavý	odmítavý	k2eAgInSc4d1	odmítavý
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
především	především	k9	především
z	z	k7c2	z
prací	práce	k1gFnPc2	práce
Antona	Anton	k1gMnSc2	Anton
Weberna	Weberna	k1gFnSc1	Weberna
a	a	k8xC	a
Pierra	Pierra	k1gFnSc1	Pierra
Bouleze	Bouleze	k1gFnSc1	Bouleze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ostatních	ostatní	k2eAgMnPc2d1	ostatní
představitelů	představitel	k1gMnPc2	představitel
poválečné	poválečný	k2eAgFnSc2d1	poválečná
avantgardy	avantgarda	k1gFnSc2	avantgarda
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
tyto	tento	k3xDgInPc4	tento
podněty	podnět	k1gInPc4	podnět
přehodnocuje	přehodnocovat	k5eAaImIp3nS	přehodnocovat
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Komponuje	komponovat	k5eAaImIp3nS	komponovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
sám	sám	k3xTgMnSc1	sám
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
tonálním	tonální	k2eAgInSc6d1	tonální
systému	systém	k1gInSc6	systém
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pozdní	pozdní	k2eAgNnPc1d1	pozdní
díla	dílo	k1gNnPc1	dílo
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
tvrdost	tvrdost	k1gFnSc4	tvrdost
a	a	k8xC	a
drsnost	drsnost	k1gFnSc4	drsnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
snahou	snaha	k1gFnSc7	snaha
vyslovit	vyslovit	k5eAaPmF	vyslovit
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
a	a	k8xC	a
podstatné	podstatný	k2eAgNnSc1d1	podstatné
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
nikdy	nikdy	k6eAd1	nikdy
nevyhýbal	vyhýbat	k5eNaImAgInS	vyhýbat
"	"	kIx"	"
<g/>
vypůjčování	vypůjčování	k1gNnPc1	vypůjčování
si	se	k3xPyFc3	se
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přetvářel	přetvářet	k5eAaImAgMnS	přetvářet
však	však	k9	však
věci	věc	k1gFnSc2	věc
svým	svůj	k3xOyFgInSc7	svůj
osobitým	osobitý	k2eAgInSc7d1	osobitý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
"	"	kIx"	"
<g/>
krádeže	krádež	k1gFnPc1	krádež
<g/>
"	"	kIx"	"
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
libovolné	libovolný	k2eAgFnPc1d1	libovolná
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
celku	celek	k1gInSc6	celek
svoje	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
estetického	estetický	k2eAgNnSc2d1	estetické
nebo	nebo	k8xC	nebo
logického	logický	k2eAgNnSc2d1	logické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
atmosféry	atmosféra	k1gFnSc2	atmosféra
(	(	kIx(	(
<g/>
Polibek	polibek	k1gInSc1	polibek
víly	víla	k1gFnSc2	víla
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
základní	základní	k2eAgFnSc1d1	základní
myšlenka	myšlenka	k1gFnSc1	myšlenka
(	(	kIx(	(
<g/>
Pulcinella	pulcinello	k1gMnSc2	pulcinello
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
jednou	jeden	k4xCgFnSc7	jeden
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nikdy	nikdy	k6eAd1	nikdy
neučil	učit	k5eNaImAgMnS	učit
:	:	kIx,	:
"	"	kIx"	"
<g/>
Musel	muset	k5eAaImAgMnS	muset
bych	by	kYmCp1nS	by
práce	práce	k1gFnSc2	práce
svých	svůj	k3xOyFgMnPc2	svůj
žáků	žák	k1gMnPc2	žák
neustále	neustále	k6eAd1	neustále
přepracovávat	přepracovávat	k5eAaImF	přepracovávat
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Již	již	k6eAd1	již
v	v	k7c6	v
Ptáku	pták	k1gMnSc6	pták
Ohnivákovi	Ohnivák	k1gMnSc6	Ohnivák
použil	použít	k5eAaPmAgMnS	použít
pro	pro	k7c4	pro
finále	finála	k1gFnSc3	finála
jednu	jeden	k4xCgFnSc4	jeden
ranou	raný	k2eAgFnSc4d1	raná
klavírní	klavírní	k2eAgFnSc4d1	klavírní
skladbu	skladba	k1gFnSc4	skladba
Petra	Petr	k1gMnSc2	Petr
Iljiče	Iljič	k1gMnSc2	Iljič
Čajkovského	Čajkovský	k2eAgMnSc2d1	Čajkovský
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
baletu	balet	k1gInSc6	balet
Petruška	petruška	k1gMnSc1	petruška
<g/>
,	,	kIx,	,
slyšel	slyšet	k5eAaImAgMnS	slyšet
neustále	neustále	k6eAd1	neustále
hrát	hrát	k5eAaImF	hrát
za	za	k7c7	za
svým	svůj	k3xOyFgNnSc7	svůj
oknem	okno	k1gNnSc7	okno
populární	populární	k2eAgInSc4d1	populární
popěvek	popěvek	k1gInSc4	popěvek
"	"	kIx"	"
<g/>
Dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
noha	noha	k1gFnSc1	noha
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Jaimbe	Jaimb	k1gMnSc5	Jaimb
en	en	k?	en
bois	bois	k1gInSc1	bois
<g/>
)	)	kIx)	)
a	a	k8xC	a
protože	protože	k8xS	protože
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nějaká	nějaký	k3yIgFnSc1	nějaký
stará	starý	k2eAgFnSc1d1	stará
lidová	lidový	k2eAgFnSc1d1	lidová
melodie	melodie	k1gFnSc1	melodie
<g/>
,	,	kIx,	,
v	v	k7c6	v
baletu	balet	k1gInSc6	balet
ji	on	k3xPp3gFnSc4	on
použil	použít	k5eAaPmAgMnS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Netušil	tušit	k5eNaImAgMnS	tušit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
autor	autor	k1gMnSc1	autor
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
melodii	melodie	k1gFnSc6	melodie
autorská	autorský	k2eAgNnPc1d1	autorské
práva	právo	k1gNnPc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Stravinskij	Stravinskít	k5eAaPmRp2nS	Stravinskít
proto	proto	k8xC	proto
musel	muset	k5eAaImAgInS	muset
celou	celý	k2eAgFnSc4d1	celá
1	[number]	k4	1
třetinu	třetina	k1gFnSc4	třetina
honoráře	honorář	k1gInSc2	honorář
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
představení	představení	k1gNnPc2	představení
baletu	balet	k1gInSc2	balet
odeslat	odeslat	k5eAaPmF	odeslat
autorovi	autor	k1gMnSc3	autor
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgNnSc4d1	úvodní
téma	téma	k1gNnSc4	téma
Svěcení	svěcení	k1gNnSc2	svěcení
jara	jaro	k1gNnSc2	jaro
je	být	k5eAaImIp3nS	být
upravená	upravený	k2eAgFnSc1d1	upravená
litevská	litevský	k2eAgFnSc1d1	Litevská
lidová	lidový	k2eAgFnSc1d1	lidová
píseň	píseň	k1gFnSc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgInSc1d1	úvodní
motiv	motiv	k1gInSc1	motiv
ze	z	k7c2	z
Slavíka	Slavík	k1gMnSc2	Slavík
připomíná	připomínat	k5eAaImIp3nS	připomínat
motiv	motiv	k1gInSc4	motiv
z	z	k7c2	z
Oblak	oblaka	k1gNnPc2	oblaka
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
Debussyových	Debussyových	k2eAgInSc4d1	Debussyových
Nokturn	nokturn	k1gInSc4	nokturn
<g/>
.	.	kIx.	.
</s>
<s>
Balet	balet	k1gInSc4	balet
Pulcinella	pulcinello	k1gMnSc2	pulcinello
jsou	být	k5eAaImIp3nP	být
přepracované	přepracovaný	k2eAgFnPc4d1	přepracovaná
skladby	skladba	k1gFnPc4	skladba
Giovanniho	Giovanni	k1gMnSc2	Giovanni
Battisty	Battista	k1gMnSc2	Battista
Pergolesiho	Pergolesi	k1gMnSc2	Pergolesi
a	a	k8xC	a
Domenica	Domenicus	k1gMnSc2	Domenicus
Galla	Gall	k1gMnSc2	Gall
<g/>
.	.	kIx.	.
</s>
<s>
Balet	balet	k1gInSc1	balet
Polibek	polibek	k1gInSc4	polibek
víly	víla	k1gFnSc2	víla
jsou	být	k5eAaImIp3nP	být
přepracované	přepracovaný	k2eAgFnPc1d1	přepracovaná
rané	raný	k2eAgFnPc1d1	raná
klavírní	klavírní	k2eAgFnPc1d1	klavírní
skladby	skladba	k1gFnPc1	skladba
Petra	Petr	k1gMnSc2	Petr
Iljiče	Iljič	k1gMnSc2	Iljič
Čajkovského	Čajkovský	k2eAgMnSc2d1	Čajkovský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
skladby	skladba	k1gFnSc2	skladba
Cirkusová	cirkusový	k2eAgFnSc1d1	cirkusová
polka	polka	k1gFnSc1	polka
je	být	k5eAaImIp3nS	být
použit	použit	k2eAgInSc4d1	použit
motiv	motiv	k1gInSc4	motiv
z	z	k7c2	z
Schubertových	Schubertových	k2eAgFnPc2d1	Schubertových
Marches	Marchesa	k1gFnPc2	Marchesa
militaires	militaires	k1gMnSc1	militaires
<g/>
.	.	kIx.	.
</s>
<s>
Předehra	předehra	k1gFnSc1	předehra
Greeting	Greeting	k1gInSc1	Greeting
Prelude	Prelud	k1gInSc5	Prelud
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
80	[number]	k4	80
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
dirigentovi	dirigent	k1gMnSc6	dirigent
Pierru	Pierr	k1gMnSc6	Pierr
Monteuxovi	Monteuxa	k1gMnSc6	Monteuxa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
upravená	upravený	k2eAgFnSc1d1	upravená
melodie	melodie	k1gFnSc1	melodie
"	"	kIx"	"
<g/>
Happy	Happ	k1gInPc4	Happ
birthday	birthda	k2eAgInPc4d1	birthda
to	ten	k3xDgNnSc4	ten
you	you	k?	you
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
úpravu	úprava	k1gFnSc4	úprava
hymny	hymna	k1gFnSc2	hymna
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
zatčen	zatčen	k2eAgInSc1d1	zatčen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
<g/>
x	x	k?	x
překonal	překonat	k5eAaPmAgInS	překonat
Atlantický	atlantický	k2eAgInSc1d1	atlantický
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Byl	být	k5eAaImAgMnS	být
obrovský	obrovský	k2eAgMnSc1d1	obrovský
dříč	dříč	k1gMnSc1	dříč
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
až	až	k6eAd1	až
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
života	život	k1gInSc2	život
byl	být	k5eAaImAgInS	být
nemocen	nemocen	k2eAgInSc1d1	nemocen
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
hypochondr	hypochondr	k1gMnSc1	hypochondr
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
preventivně	preventivně	k6eAd1	preventivně
vyoperovat	vyoperovat	k5eAaPmF	vyoperovat
slepé	slepý	k2eAgNnSc4d1	slepé
střevo	střevo	k1gNnSc4	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
velký	velký	k2eAgInSc1d1	velký
kuřák	kuřák	k1gInSc1	kuřák
(	(	kIx(	(
<g/>
začal	začít	k5eAaPmAgInS	začít
ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
(	(	kIx(	(
<g/>
i	i	k8xC	i
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
<g/>
)	)	kIx)	)
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
:	:	kIx,	:
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
<g/>
,	,	kIx,	,
Benito	Benit	k2eAgNnSc1d1	Benito
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
,	,	kIx,	,
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
<g/>
,	,	kIx,	,
Ingmar	Ingmar	k1gMnSc1	Ingmar
Bergman	Bergman	k1gMnSc1	Bergman
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Schweitzer	Schweitzer	k1gMnSc1	Schweitzer
<g/>
,	,	kIx,	,
Nikita	Nikita	k1gMnSc1	Nikita
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Chruščov	Chruščov	k1gInSc4	Chruščov
<g/>
,	,	kIx,	,
Coco	Coco	k6eAd1	Coco
Chanel	Chanel	k1gInSc1	Chanel
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
i	i	k9	i
s	s	k7c7	s
prezidenty	prezident	k1gMnPc7	prezident
Masarykem	Masaryk	k1gMnSc7	Masaryk
a	a	k8xC	a
Benešem	Beneš	k1gMnSc7	Beneš
<g/>
,	,	kIx,	,
když	když	k8xS	když
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
tak	tak	k6eAd1	tak
ohromné	ohromný	k2eAgNnSc1d1	ohromné
přijetí	přijetí	k1gNnSc1	přijetí
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc4	jaký
pamatoval	pamatovat	k5eAaImAgMnS	pamatovat
už	už	k6eAd1	už
jen	jen	k9	jen
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgMnS	napsat
knihy	kniha	k1gFnPc4	kniha
<g/>
:	:	kIx,	:
Kronika	kronika	k1gFnSc1	kronika
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Hudební	hudební	k2eAgFnSc1d1	hudební
poetika	poetika	k1gFnSc1	poetika
a	a	k8xC	a
studie	studie	k1gFnSc1	studie
o	o	k7c6	o
tvorbě	tvorba	k1gFnSc6	tvorba
Antona	Anton	k1gMnSc2	Anton
Weberna	Weberna	k1gFnSc1	Weberna
a	a	k8xC	a
Carla	Carla	k1gFnSc1	Carla
Gesualda	Gesualda	k1gFnSc1	Gesualda
di	di	k?	di
Venosa	Venosa	k1gFnSc1	Venosa
<g/>
.	.	kIx.	.
</s>
<s>
Vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
přístroj	přístroj	k1gInSc1	přístroj
na	na	k7c4	na
linkování	linkování	k1gNnSc4	linkování
notového	notový	k2eAgInSc2d1	notový
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Stravigor	Stravigor	k1gInSc1	Stravigor
<g/>
"	"	kIx"	"
patentován	patentován	k2eAgMnSc1d1	patentován
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
skladatele	skladatel	k1gMnPc4	skladatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
ovlivněni	ovlivnit	k5eAaPmNgMnP	ovlivnit
jeho	jeho	k3xOp3gFnSc7	jeho
prací	práce	k1gFnSc7	práce
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k1gMnSc1	Martinů
<g/>
,	,	kIx,	,
Francis	Francis	k1gFnSc1	Francis
Poulenc	Poulenc	k1gFnSc1	Poulenc
<g/>
,	,	kIx,	,
Aaron	Aaron	k1gMnSc1	Aaron
Copland	Copland	k1gInSc1	Copland
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Britten	Britten	k2eAgMnSc1d1	Britten
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
Frank	Frank	k1gMnSc1	Frank
Zappa	Zappa	k1gFnSc1	Zappa
nebo	nebo	k8xC	nebo
John	John	k1gMnSc1	John
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Časopisem	časopis	k1gInSc7	časopis
Time	Tim	k1gFnSc2	Tim
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
mezi	mezi	k7c4	mezi
"	"	kIx"	"
<g/>
100	[number]	k4	100
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
osobností	osobnost	k1gFnPc2	osobnost
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
přání	přání	k1gNnSc2	přání
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
na	na	k7c6	na
hřbitovním	hřbitovní	k2eAgInSc6d1	hřbitovní
ostrově	ostrov	k1gInSc6	ostrov
San	San	k1gFnSc2	San
Michele	Michel	k1gInSc2	Michel
(	(	kIx(	(
<g/>
Sv.	sv.	kA	sv.
Michal	Michal	k1gMnSc1	Michal
<g/>
)	)	kIx)	)
nedaleko	nedaleko	k7c2	nedaleko
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
byly	být	k5eAaImAgInP	být
pojmenovány	pojmenován	k2eAgInPc1d1	pojmenován
extraterestrické	extraterestrický	k2eAgInPc1d1	extraterestrický
útvary	útvar	k1gInPc1	útvar
<g/>
:	:	kIx,	:
planetka	planetka	k1gFnSc1	planetka
Stravinsky	Stravinsky	k1gFnSc1	Stravinsky
a	a	k8xC	a
kráter	kráter	k1gInSc1	kráter
na	na	k7c6	na
Merkuru	Merkur	k1gInSc6	Merkur
Stravinskij	Stravinskij	k1gFnSc2	Stravinskij
<g/>
.	.	kIx.	.
</s>
<s>
Pták	pták	k1gMnSc1	pták
Ohnivák	Ohnivák	k1gMnSc1	Ohnivák
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
přepracováno	přepracovat	k5eAaPmNgNnS	přepracovat
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
–	–	k?	–
baletní	baletní	k2eAgFnSc1d1	baletní
suita	suita	k1gFnSc1	suita
Petruška	Petruška	k1gFnSc1	Petruška
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
přepracováno	přepracován	k2eAgNnSc4d1	přepracováno
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
Svěcení	svěcení	k1gNnSc6	svěcení
jara	jaro	k1gNnSc2	jaro
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
Příběh	příběh	k1gInSc1	příběh
vojáka	voják	k1gMnSc2	voják
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
Pulcinella	pulcinello	k1gMnSc4	pulcinello
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
Apollon	Apollon	k1gMnSc1	Apollon
Musagè	Musagè	k1gMnSc1	Musagè
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
přepracováno	přepracovat	k5eAaPmNgNnS	přepracovat
a	a	k8xC	a
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Apollo	Apollo	k1gNnSc4	Apollo
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
Polibek	polibek	k1gInSc1	polibek
víly	víla	k1gFnSc2	víla
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
Hra	hra	k1gFnSc1	hra
v	v	k7c4	v
karty	karta	k1gFnPc4	karta
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
Orfeus	Orfeus	k1gMnSc1	Orfeus
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
Agon	agon	k1gInSc1	agon
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Slavík	slavík	k1gInSc1	slavík
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Lišák	lišák	k1gMnSc1	lišák
(	(	kIx(	(
<g/>
Renard	Renard	k1gMnSc1	Renard
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
–	–	k?	–
burleska	burleska	k1gFnSc1	burleska
Mavra	Mavra	k1gFnSc1	Mavra
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Oedipus	Oedipus	k1gMnSc1	Oedipus
Rex	Rex	k1gMnSc1	Rex
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
–	–	k?	–
opera-oratorium	operaratorium	k1gNnSc1	opera-oratorium
Persefona	Persefona	k1gFnSc1	Persefona
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
Život	život	k1gInSc1	život
prostopášníka	prostopášník	k1gMnSc2	prostopášník
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
Potopa	potopa	k1gFnSc1	potopa
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
–	–	k?	–
hudební	hudební	k2eAgFnSc1d1	hudební
hra	hra	k1gFnSc1	hra
Capriccio	capriccio	k1gNnSc1	capriccio
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
přepracováno	přepracován	k2eAgNnSc4d1	přepracováno
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
Houslový	houslový	k2eAgInSc1d1	houslový
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
D	D	kA	D
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
sólové	sólový	k2eAgInPc4d1	sólový
klavíry	klavír	k1gInPc4	klavír
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
Koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
Es	es	k1gNnSc6	es
"	"	kIx"	"
<g/>
Dumbartonské	Dumbartonský	k2eAgInPc4d1	Dumbartonský
duby	dub	k1gInPc4	dub
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Danses	Danses	k1gMnSc1	Danses
concertantes	concertantes	k1gMnSc1	concertantes
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
Ebony	Ebona	k1gFnSc2	Ebona
Concerto	Concerta	k1gFnSc5	Concerta
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
D	D	kA	D
"	"	kIx"	"
<g/>
Basilejský	basilejský	k2eAgMnSc1d1	basilejský
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
Symfonie	symfonie	k1gFnSc1	symfonie
Es	es	k1gNnSc2	es
dur	dur	k1gNnSc2	dur
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
Žalmová	žalmový	k2eAgFnSc1d1	Žalmová
symfonie	symfonie	k1gFnSc1	symfonie
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přepracováno	přepracován	k2eAgNnSc4d1	přepracováno
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Symfonie	symfonie	k1gFnSc2	symfonie
v	v	k7c6	v
C	C	kA	C
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
Symfonie	symfonie	k1gFnSc1	symfonie
o	o	k7c6	o
třech	tři	k4xCgFnPc6	tři
větách	věta	k1gFnPc6	věta
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Ohňostroj	ohňostroj	k1gInSc1	ohňostroj
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
Ragtime	ragtime	k1gInSc1	ragtime
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
Tři	tři	k4xCgInPc1	tři
kusy	kus	k1gInPc1	kus
pro	pro	k7c4	pro
klarinet	klarinet	k1gInSc4	klarinet
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
Symfonie	symfonie	k1gFnSc2	symfonie
dechových	dechový	k2eAgInPc2d1	dechový
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Suita	suita	k1gFnSc1	suita
pro	pro	k7c4	pro
malý	malý	k2eAgInSc4d1	malý
orchestr	orchestr	k1gInSc4	orchestr
č.	č.	k?	č.
2	[number]	k4	2
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
a	a	k8xC	a
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
Concertino	concertino	k1gNnSc4	concertino
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
přepracováno	přepracován	k2eAgNnSc4d1	přepracováno
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
Oktet	oktet	k1gInSc1	oktet
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
Cirkusová	cirkusový	k2eAgFnSc1d1	cirkusová
polka	polka	k1gFnSc1	polka
(	(	kIx(	(
<g/>
orchestrace	orchestrace	k1gFnSc1	orchestrace
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
Scherzo	scherzo	k1gNnSc4	scherzo
à	à	k?	à
la	la	k1gNnSc2	la
Russe	Russe	k1gFnSc1	Russe
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
Mše	mše	k1gFnSc1	mše
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Canticum	Canticum	k1gInSc1	Canticum
Sacrum	Sacrum	k1gNnSc1	Sacrum
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
Nářky	nářek	k1gInPc4	nářek
proroka	prorok	k1gMnSc4	prorok
Jeremiáše	Jeremiáš	k1gMnSc2	Jeremiáš
(	(	kIx(	(
<g/>
Threni	Thren	k2eAgMnPc1d1	Thren
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Abrahám	Abrahám	k1gMnSc1	Abrahám
a	a	k8xC	a
Izák	Izák	k1gMnSc1	Izák
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Requiem	Requius	k1gMnSc7	Requius
Canticles	Canticles	k1gMnSc1	Canticles
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
Čtyři	čtyři	k4xCgFnPc1	čtyři
etudy	etuda	k1gFnPc1	etuda
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
7	[number]	k4	7
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
Piano	piano	k1gNnSc1	piano
Rag-Music	Rag-Musice	k1gFnPc2	Rag-Musice
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
Les	les	k1gInSc1	les
cinq	cinq	k?	cinq
doigts	doigts	k1gInSc1	doigts
(	(	kIx(	(
<g/>
Pět	pět	k4xCc4	pět
prstů	prst	k1gInPc2	prst
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
instrumentace	instrumentace	k1gFnSc1	instrumentace
a	a	k8xC	a
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c6	na
<g/>
"	"	kIx"	"
<g/>
Osm	osm	k4xCc4	osm
instrumentálních	instrumentální	k2eAgFnPc2d1	instrumentální
miniatur	miniatura	k1gFnPc2	miniatura
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Sonáta	sonáta	k1gFnSc1	sonáta
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
Serenáda	serenáda	k1gFnSc1	serenáda
in	in	k?	in
A	A	kA	A
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
Tango	tango	k1gNnSc1	tango
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
</s>
