<s>
Collè	Collè	k?	Collè
de	de	k?	de
France	Franc	k1gMnSc2	Franc
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
kolej	kolej	k1gFnSc1	kolej
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prestižní	prestižní	k2eAgFnSc1d1	prestižní
francouzská	francouzský	k2eAgFnSc1d1	francouzská
instituce	instituce	k1gFnSc1	instituce
<g/>
,	,	kIx,	,
věnovaná	věnovaný	k2eAgNnPc1d1	věnované
vědám	věda	k1gFnPc3	věda
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
čtvrti	čtvrt	k1gFnSc6	čtvrt
(	(	kIx(	(
<g/>
Quartier	Quartier	k1gMnSc1	Quartier
Latin	Latin	k1gMnSc1	Latin
<g/>
)	)	kIx)	)
na	na	k7c6	na
Rue	Rue	k1gFnSc6	Rue
des	des	k1gNnSc2	des
Écoles	Écolesa	k1gFnPc2	Écolesa
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
staré	starý	k2eAgFnSc2d1	stará
Sorbonny	Sorbonna	k1gFnSc2	Sorbonna
<g/>
.	.	kIx.	.
</s>
