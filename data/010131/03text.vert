<p>
<s>
Collè	Collè	k?	Collè
de	de	k?	de
France	Franc	k1gMnSc2	Franc
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
kolej	kolej	k1gFnSc1	kolej
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prestižní	prestižní	k2eAgFnSc1d1	prestižní
francouzská	francouzský	k2eAgFnSc1d1	francouzská
instituce	instituce	k1gFnSc1	instituce
<g/>
,	,	kIx,	,
věnovaná	věnovaný	k2eAgNnPc1d1	věnované
vědám	věda	k1gFnPc3	věda
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
čtvrti	čtvrt	k1gFnSc6	čtvrt
(	(	kIx(	(
<g/>
Quartier	Quartier	k1gMnSc1	Quartier
Latin	Latin	k1gMnSc1	Latin
<g/>
)	)	kIx)	)
na	na	k7c6	na
Rue	Rue	k1gFnSc6	Rue
des	des	k1gNnSc2	des
Écoles	Écolesa	k1gFnPc2	Écolesa
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
staré	starý	k2eAgFnSc2d1	stará
Sorbonny	Sorbonna	k1gFnSc2	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
historie	historie	k1gFnSc1	historie
začíná	začínat	k5eAaImIp3nS	začínat
rokem	rok	k1gInSc7	rok
1530	[number]	k4	1530
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
statut	statut	k1gInSc1	statut
tzv.	tzv.	kA	tzv.
grand	grand	k1gMnSc1	grand
établissement	établissement	k1gMnSc1	établissement
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiálním	oficiální	k2eAgNnSc7d1	oficiální
posláním	poslání	k1gNnSc7	poslání
Collè	Collè	k1gFnSc2	Collè
de	de	k?	de
France	Franc	k1gMnSc2	Franc
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
učit	učit	k5eAaImF	učit
vědění	vědění	k1gNnSc4	vědění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
právě	právě	k9	právě
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Enseigner	Enseigner	k1gMnSc1	Enseigner
le	le	k?	le
savoir	savoir	k1gMnSc1	savoir
en	en	k?	en
train	train	k1gMnSc1	train
de	de	k?	de
se	se	k3xPyFc4	se
faire	fair	k1gMnSc5	fair
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderně	moderně	k6eAd1	moderně
řečeno	říct	k5eAaPmNgNnS	říct
spojovat	spojovat	k5eAaImF	spojovat
bádání	bádání	k1gNnSc4	bádání
a	a	k8xC	a
výuku	výuka	k1gFnSc4	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
54	[number]	k4	54
profesorských	profesorský	k2eAgNnPc2d1	profesorské
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
rozdělených	rozdělený	k2eAgNnPc2d1	rozdělené
do	do	k7c2	do
pěti	pět	k4xCc2	pět
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
matematika	matematika	k1gFnSc1	matematika
</s>
</p>
<p>
<s>
fyzika	fyzika	k1gFnSc1	fyzika
</s>
</p>
<p>
<s>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
přírodní	přírodní	k2eAgFnPc1d1	přírodní
vědy	věda	k1gFnPc1	věda
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
lékařství	lékařství	k1gNnSc2	lékařství
(	(	kIx(	(
<g/>
medicíny	medicína	k1gFnSc2	medicína
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
společenské	společenský	k2eAgFnPc1d1	společenská
vědy	věda	k1gFnPc1	věda
</s>
</p>
<p>
<s>
historie	historie	k1gFnPc1	historie
<g/>
,	,	kIx,	,
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
linguistika	linguistika	k1gFnSc1	linguistika
<g/>
)	)	kIx)	)
a	a	k8xC	a
literaturaCollè	literaturaCollè	k?	literaturaCollè
de	de	k?	de
France	Franc	k1gMnSc2	Franc
pořádá	pořádat	k5eAaImIp3nS	pořádat
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
přednášky	přednáška	k1gFnPc4	přednáška
přístupné	přístupný	k2eAgFnSc2d1	přístupná
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
však	však	k9	však
žádné	žádný	k3yNgMnPc4	žádný
zapsané	zapsaný	k2eAgMnPc4d1	zapsaný
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgFnPc4	žádný
studijní	studijní	k2eAgFnPc4d1	studijní
obory	obora	k1gFnPc4	obora
a	a	k8xC	a
neuděluje	udělovat	k5eNaImIp3nS	udělovat
žádné	žádný	k3yNgInPc4	žádný
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
profesoři	profesor	k1gMnPc1	profesor
jsou	být	k5eAaImIp3nP	být
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
místa	místo	k1gNnPc1	místo
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
obsazují	obsazovat	k5eAaImIp3nP	obsazovat
hostujícími	hostující	k2eAgMnPc7d1	hostující
profesory	profesor	k1gMnPc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
nějaké	nějaký	k3yIgNnSc1	nějaký
místo	místo	k1gNnSc1	místo
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
<g/>
,	,	kIx,	,
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
sbor	sbor	k1gInSc1	sbor
profesorů	profesor	k1gMnPc2	profesor
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
povolán	povolat	k5eAaPmNgMnS	povolat
<g/>
.	.	kIx.	.
</s>
<s>
Profesura	profesura	k1gFnSc1	profesura
na	na	k7c6	na
Collè	Collè	k1gFnSc6	Collè
de	de	k?	de
France	Franc	k1gMnSc2	Franc
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
vrcholem	vrchol	k1gInSc7	vrchol
vědecké	vědecký	k2eAgFnSc2d1	vědecká
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1530	[number]	k4	1530
založil	založit	k5eAaPmAgMnS	založit
král	král	k1gMnSc1	král
František	František	k1gMnSc1	František
I.	I.	kA	I.
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
svého	svůj	k3xOyFgMnSc2	svůj
knihovníka	knihovník	k1gMnSc2	knihovník
<g/>
,	,	kIx,	,
humanisty	humanista	k1gMnSc2	humanista
G.	G.	kA	G.
Budého	Budého	k2eAgFnSc4d1	Budého
Královskou	královský	k2eAgFnSc4d1	královská
kolej	kolej	k1gFnSc4	kolej
nebo	nebo	k8xC	nebo
také	také	k9	také
Kolej	kolej	k1gFnSc1	kolej
tří	tři	k4xCgInPc2	tři
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zpočátku	zpočátku	k6eAd1	zpočátku
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
,	,	kIx,	,
hebrejština	hebrejština	k1gFnSc1	hebrejština
a	a	k8xC	a
latina	latina	k1gFnSc1	latina
<g/>
.	.	kIx.	.
</s>
<s>
Profesoři	profesor	k1gMnPc1	profesor
byli	být	k5eAaImAgMnP	být
hmotně	hmotně	k6eAd1	hmotně
zajištěni	zajištěn	k2eAgMnPc1d1	zajištěn
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
oborům	obor	k1gInPc3	obor
přibylo	přibýt	k5eAaPmAgNnS	přibýt
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
matematika	matematika	k1gFnSc1	matematika
a	a	k8xC	a
medicina	medicin	k2eAgFnSc1d1	medicina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
se	se	k3xPyFc4	se
kolej	kolej	k1gFnSc1	kolej
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Národní	národní	k2eAgFnSc4d1	národní
kolej	kolej	k1gFnSc4	kolej
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
na	na	k7c6	na
Collè	Collè	k1gFnSc6	Collè
de	de	k?	de
France	Franc	k1gMnSc4	Franc
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
heslem	heslo	k1gNnSc7	heslo
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
Docet	Doceta	k1gFnPc2	Doceta
omnia	omnium	k1gNnSc2	omnium
(	(	kIx(	(
<g/>
Učí	učit	k5eAaImIp3nS	učit
všemu	všecek	k3xTgNnSc3	všecek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavní	slavný	k2eAgMnPc1d1	slavný
profesoři	profesor	k1gMnPc1	profesor
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
profesory	profesor	k1gMnPc4	profesor
na	na	k7c6	na
Collè	Collè	k1gFnSc6	Collè
de	de	k?	de
France	Franc	k1gMnSc4	Franc
byly	být	k5eAaImAgInP	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgFnPc4d1	následující
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Collè	Collè	k1gMnSc2	Collè
de	de	k?	de
France	Franc	k1gMnSc2	Franc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Collè	Collè	k1gMnSc2	Collè
de	de	k?	de
France	Franc	k1gMnSc2	Franc
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Collè	Collè	k1gMnSc2	Collè
de	de	k?	de
France	Franc	k1gMnSc2	Franc
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Záznam	záznam	k1gInSc1	záznam
v	v	k7c6	v
evidenci	evidence	k1gFnSc6	evidence
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
</s>
</p>
