<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
síť	síť	k1gFnSc1	síť
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
computer	computer	k1gInSc1	computer
network	network	k1gInSc2	network
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc4d1	souhrnné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
technické	technický	k2eAgInPc4d1	technický
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
realizují	realizovat	k5eAaBmIp3nP	realizovat
spojení	spojení	k1gNnSc4	spojení
a	a	k8xC	a
výměnu	výměna	k1gFnSc4	výměna
informací	informace	k1gFnPc2	informace
mezi	mezi	k7c7	mezi
počítači	počítač	k1gInPc7	počítač
<g/>
.	.	kIx.	.
</s>
