<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
síť	síť	k1gFnSc1	síť
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
computer	computer	k1gInSc1	computer
network	network	k1gInSc2	network
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc4d1	souhrnné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
technické	technický	k2eAgInPc4d1	technický
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
realizují	realizovat	k5eAaBmIp3nP	realizovat
spojení	spojení	k1gNnSc4	spojení
a	a	k8xC	a
výměnu	výměna	k1gFnSc4	výměna
informací	informace	k1gFnPc2	informace
mezi	mezi	k7c7	mezi
počítači	počítač	k1gInPc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Umožňují	umožňovat	k5eAaImIp3nP	umožňovat
tedy	tedy	k9	tedy
uživatelům	uživatel	k1gMnPc3	uživatel
komunikaci	komunikace	k1gFnSc4	komunikace
podle	podle	k7c2	podle
určitých	určitý	k2eAgNnPc2d1	určité
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
sdílení	sdílení	k1gNnSc2	sdílení
využívání	využívání	k1gNnSc2	využívání
společných	společný	k2eAgInPc2d1	společný
zdrojů	zdroj	k1gInPc2	zdroj
nebo	nebo	k8xC	nebo
výměny	výměna	k1gFnSc2	výměna
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
sítí	síť	k1gFnPc2	síť
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začaly	začít	k5eAaPmAgFnP	začít
první	první	k4xOgInSc4	první
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
komunikací	komunikace	k1gFnSc7	komunikace
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vývoje	vývoj	k1gInSc2	vývoj
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
síťových	síťový	k2eAgFnPc2d1	síťová
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
sítě	síť	k1gFnPc1	síť
postupně	postupně	k6eAd1	postupně
spojovány	spojovat	k5eAaImNgFnP	spojovat
do	do	k7c2	do
globální	globální	k2eAgFnSc2d1	globální
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
sítě	síť	k1gFnSc2	síť
zvané	zvaný	k2eAgInPc1d1	zvaný
Internet	Internet	k1gInSc1	Internet
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
sadu	sada	k1gFnSc4	sada
protokolů	protokol	k1gInPc2	protokol
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
komunikací	komunikace	k1gFnSc7	komunikace
počítačů	počítač	k1gMnPc2	počítač
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Síťová	síťový	k2eAgFnSc1d1	síťová
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Síťová	síťový	k2eAgFnSc1d1	síťová
architektura	architektura	k1gFnSc1	architektura
představuje	představovat	k5eAaImIp3nS	představovat
strukturu	struktura	k1gFnSc4	struktura
řízení	řízení	k1gNnSc2	řízení
komunikace	komunikace	k1gFnSc2	komunikace
v	v	k7c6	v
systémech	systém	k1gInPc6	systém
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
souhrn	souhrn	k1gInSc1	souhrn
řídicích	řídicí	k2eAgFnPc2d1	řídicí
činností	činnost	k1gFnPc2	činnost
umožňujících	umožňující	k2eAgFnPc2d1	umožňující
výměnu	výměna	k1gFnSc4	výměna
dat	datum	k1gNnPc2	datum
mezi	mezi	k7c7	mezi
komunikujícími	komunikující	k2eAgInPc7d1	komunikující
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
řízení	řízení	k1gNnSc1	řízení
je	být	k5eAaImIp3nS	být
složitý	složitý	k2eAgInSc4d1	složitý
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
rozdělení	rozdělení	k1gNnSc4	rozdělení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Členění	členění	k1gNnSc1	členění
do	do	k7c2	do
vrstev	vrstva	k1gFnPc2	vrstva
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
hierarchii	hierarchie	k1gFnSc4	hierarchie
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
řízení	řízení	k1gNnSc6	řízení
komunikace	komunikace	k1gFnSc2	komunikace
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
vrstva	vrstva	k1gFnSc1	vrstva
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
službou	služba	k1gFnSc7	služba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
poskytována	poskytovat	k5eAaImNgFnS	poskytovat
vyšší	vysoký	k2eAgFnSc3d2	vyšší
sousední	sousední	k2eAgFnSc3d1	sousední
vrstvě	vrstva	k1gFnSc3	vrstva
<g/>
,	,	kIx,	,
a	a	k8xC	a
funkcemi	funkce	k1gFnPc7	funkce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
protokolu	protokol	k1gInSc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Řízení	řízení	k1gNnSc1	řízení
komunikace	komunikace	k1gFnSc2	komunikace
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
komunikujících	komunikující	k2eAgInPc2d1	komunikující
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
spolupráce	spolupráce	k1gFnSc1	spolupráce
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
koordinována	koordinovat	k5eAaBmNgFnS	koordinovat
pomocí	pomocí	k7c2	pomocí
řídicích	řídicí	k2eAgInPc2d1	řídicí
údajů	údaj	k1gInPc2	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Koordinaci	koordinace	k1gFnSc4	koordinace
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
protokoly	protokol	k1gInPc1	protokol
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
definují	definovat	k5eAaBmIp3nP	definovat
formální	formální	k2eAgFnSc4d1	formální
stránku	stránka	k1gFnSc4	stránka
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Protokoly	protokol	k1gInPc1	protokol
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k8xC	tedy
tvořeny	tvořit	k5eAaImNgFnP	tvořit
souhrnem	souhrn	k1gInSc7	souhrn
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
formátů	formát	k1gInPc2	formát
a	a	k8xC	a
procedur	procedura	k1gFnPc2	procedura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
určují	určovat	k5eAaImIp3nP	určovat
výměnu	výměna	k1gFnSc4	výměna
údajů	údaj	k1gInPc2	údaj
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
komunikujícími	komunikující	k2eAgInPc7d1	komunikující
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
síťová	síťový	k2eAgFnSc1d1	síťová
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
architektura	architektura	k1gFnSc1	architektura
otevřených	otevřený	k2eAgInPc2d1	otevřený
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
OSA	osa	k1gFnSc1	osa
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
Open	Open	k1gNnSc1	Open
Systems	Systems	k1gInSc1	Systems
Architecture	Architectur	k1gMnSc5	Architectur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
normalizována	normalizovat	k5eAaBmNgFnS	normalizovat
organizací	organizace	k1gFnSc7	organizace
ISO	ISO	kA	ISO
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
referenční	referenční	k2eAgInSc4d1	referenční
model	model	k1gInSc4	model
OSI	OSI	kA	OSI
<g/>
.	.	kIx.	.
</s>
<s>
Praktickou	praktický	k2eAgFnSc7d1	praktická
realizací	realizace	k1gFnSc7	realizace
vrstvové	vrstvový	k2eAgFnSc2d1	vrstvová
síťové	síťový	k2eAgFnSc2d1	síťová
architektury	architektura	k1gFnSc2	architektura
je	být	k5eAaImIp3nS	být
sada	sada	k1gFnSc1	sada
protokolů	protokol	k1gInPc2	protokol
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
přesně	přesně	k6eAd1	přesně
referenčnímu	referenční	k2eAgInSc3d1	referenční
modelu	model	k1gInSc3	model
ISO	ISO	kA	ISO
<g/>
.	.	kIx.	.
</s>
<s>
Sítě	síť	k1gFnPc1	síť
můžeme	moct	k5eAaImIp1nP	moct
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Dělíme	dělit	k5eAaImIp1nP	dělit
je	on	k3xPp3gNnSc4	on
podle	podle	k7c2	podle
přepojování	přepojování	k1gNnSc2	přepojování
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
přenášených	přenášený	k2eAgInPc2d1	přenášený
signálů	signál	k1gInPc2	signál
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
přenosu	přenos	k1gInSc2	přenos
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
nejužívanějším	užívaný	k2eAgMnSc7d3	nejužívanější
je	být	k5eAaImIp3nS	být
dělení	dělení	k1gNnSc1	dělení
sítí	síť	k1gFnPc2	síť
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
rozlehlosti	rozlehlost	k1gFnSc2	rozlehlost
a	a	k8xC	a
účelu	účel	k1gInSc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Komutační	komutační	k2eAgFnSc4d1	komutační
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Komutační	komutační	k2eAgFnSc1d1	komutační
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
síť	síť	k1gFnSc4	síť
s	s	k7c7	s
přepojováním	přepojování	k1gNnSc7	přepojování
okruhů	okruh	k1gInPc2	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
starší	starý	k2eAgFnSc3d2	starší
technologii	technologie	k1gFnSc3	technologie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
již	již	k6eAd1	již
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
telegrafie	telegrafie	k1gFnSc2	telegrafie
a	a	k8xC	a
telefonie	telefonie	k1gFnSc2	telefonie
a	a	k8xC	a
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
i	i	k9	i
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
ISDN	ISDN	kA	ISDN
(	(	kIx(	(
<g/>
původem	původ	k1gMnSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Integrated	Integrated	k1gInSc1	Integrated
Services	Services	k1gInSc4	Services
Digital	Digital	kA	Digital
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
telefonní	telefonní	k2eAgFnPc4d1	telefonní
sítě	síť	k1gFnPc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Paketová	paketový	k2eAgFnSc1d1	paketová
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Paketová	paketový	k2eAgFnSc1d1	paketová
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
síť	síť	k1gFnSc4	síť
s	s	k7c7	s
přepojováním	přepojování	k1gNnSc7	přepojování
paketů	paket	k1gInPc2	paket
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
paket	paket	k1gInSc1	paket
"	"	kIx"	"
<g/>
putuje	putovat	k5eAaImIp3nS	putovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
od	od	k7c2	od
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
po	po	k7c4	po
cílový	cílový	k2eAgInSc4d1	cílový
uzel	uzel	k1gInSc4	uzel
známá	známý	k2eAgFnSc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
paket	paket	k1gInSc1	paket
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
přenáší	přenášet	k5eAaImIp3nS	přenášet
jinou	jiný	k2eAgFnSc7d1	jiná
trasou	trasa	k1gFnSc7	trasa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
topologií	topologie	k1gFnSc7	topologie
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
propustností	propustnost	k1gFnSc7	propustnost
a	a	k8xC	a
výpadky	výpadek	k1gInPc1	výpadek
uzlů	uzel	k1gInPc2	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
cílový	cílový	k2eAgInSc1d1	cílový
uzel	uzel	k1gInSc1	uzel
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
ověřován	ověřován	k2eAgMnSc1d1	ověřován
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
paket	paket	k1gInSc1	paket
naplněn	naplnit	k5eAaPmNgInS	naplnit
informacemi	informace	k1gFnPc7	informace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
potřebné	potřebný	k2eAgFnPc1d1	potřebná
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
odeslání	odeslání	k1gNnSc4	odeslání
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
paket	paket	k1gInSc1	paket
dorazí	dorazit	k5eAaPmIp3nS	dorazit
do	do	k7c2	do
uzlu	uzel	k1gInSc2	uzel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přečtena	přečíst	k5eAaPmNgFnS	přečíst
jeho	jeho	k3xOp3gFnSc1	jeho
cílová	cílový	k2eAgFnSc1d1	cílová
adresa	adresa	k1gFnSc1	adresa
a	a	k8xC	a
popř.	popř.	kA	popř.
další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zařadí	zařadit	k5eAaPmIp3nS	zařadit
do	do	k7c2	do
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
odeslání	odeslání	k1gNnSc4	odeslání
k	k	k7c3	k
požadovanému	požadovaný	k2eAgMnSc3d1	požadovaný
adresátovi	adresát	k1gMnSc3	adresát
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Ethernet	Ethernet	k1gInSc1	Ethernet
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
technologie	technologie	k1gFnSc1	technologie
zde	zde	k6eAd1	zde
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
X	X	kA	X
<g/>
.25	.25	k4	.25
<g/>
,	,	kIx,	,
Frame	Fram	k1gInSc5	Fram
Relay	Relaum	k1gNnPc7	Relaum
nebo	nebo	k8xC	nebo
ATM	ATM	kA	ATM
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Peer-to-peer	Peeroeer	k1gInSc1	Peer-to-peer
<g/>
.	.	kIx.	.
</s>
<s>
Peer-to-peer	Peeroeer	k1gInSc1	Peer-to-peer
nebo	nebo	k8xC	nebo
též	též	k9	též
peer	peer	k1gMnSc1	peer
to	ten	k3xDgNnSc1	ten
peer	peer	k1gMnSc1	peer
<g/>
,	,	kIx,	,
rovný	rovný	k2eAgInSc4d1	rovný
s	s	k7c7	s
rovným	rovný	k2eAgInSc7d1	rovný
či	či	k8xC	či
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
všechny	všechen	k3xTgInPc4	všechen
počítače	počítač	k1gInPc4	počítač
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
uzly	uzel	k1gInPc1	uzel
<g/>
,	,	kIx,	,
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
rovny	roven	k2eAgFnPc1d1	rovna
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
čtyři	čtyři	k4xCgInPc1	čtyři
termíny	termín	k1gInPc1	termín
mají	mít	k5eAaImIp3nP	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
peer-to-peer	peeroeer	k1gInSc1	peer-to-peer
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
rovný	rovný	k2eAgInSc1d1	rovný
k	k	k7c3	k
rovnému	rovný	k2eAgMnSc3d1	rovný
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
rovný	rovný	k2eAgInSc1d1	rovný
s	s	k7c7	s
rovným	rovný	k2eAgMnSc7d1	rovný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
též	též	k9	též
tento	tento	k3xDgInSc1	tento
síťový	síťový	k2eAgInSc1d1	síťový
druh	druh	k1gInSc1	druh
nazývá	nazývat	k5eAaImIp3nS	nazývat
klient-klient	klientlient	k1gInSc4	klient-klient
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
může	moct	k5eAaImIp3nS	moct
vyčlenit	vyčlenit	k5eAaPmF	vyčlenit
některý	některý	k3yIgInSc4	některý
svůj	svůj	k3xOyFgInSc4	svůj
prostředek	prostředek	k1gInSc4	prostředek
(	(	kIx(	(
<g/>
tiskárnu	tiskárna	k1gFnSc4	tiskárna
<g/>
,	,	kIx,	,
úložné	úložný	k2eAgNnSc4d1	úložné
médium	médium	k1gNnSc4	médium
<g/>
,	,	kIx,	,
adresář	adresář	k1gInSc4	adresář
<g/>
)	)	kIx)	)
ke	k	k7c3	k
sdílení	sdílení	k1gNnSc3	sdílení
(	(	kIx(	(
<g/>
s	s	k7c7	s
heslem	heslo	k1gNnSc7	heslo
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
něj	on	k3xPp3gInSc2	on
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
stanice	stanice	k1gFnSc1	stanice
může	moct	k5eAaImIp3nS	moct
tyto	tento	k3xDgInPc4	tento
prostředky	prostředek	k1gInPc4	prostředek
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
sdílený	sdílený	k2eAgInSc4d1	sdílený
prostředek	prostředek	k1gInSc4	prostředek
připojí	připojit	k5eAaPmIp3nS	připojit
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
uživatel	uživatel	k1gMnSc1	uživatel
zná	znát	k5eAaImIp3nS	znát
případné	případný	k2eAgNnSc4d1	případné
heslo	heslo	k1gNnSc4	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Sdílení	sdílení	k1gNnPc1	sdílení
a	a	k8xC	a
hesla	heslo	k1gNnPc1	heslo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
kdykoliv	kdykoliv	k6eAd1	kdykoliv
změněna	změnit	k5eAaPmNgFnS	změnit
nebo	nebo	k8xC	nebo
zrušena	zrušit	k5eAaPmNgFnS	zrušit
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
u	u	k7c2	u
stanice	stanice	k1gFnSc2	stanice
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
sítě	síť	k1gFnSc2	síť
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nelze	lze	k6eNd1	lze
centrálně	centrálně	k6eAd1	centrálně
spravovat	spravovat	k5eAaImF	spravovat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
sdílení	sdílení	k1gNnSc4	sdílení
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
systémových	systémový	k2eAgInPc2d1	systémový
prostředků	prostředek	k1gInPc2	prostředek
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
a	a	k8xC	a
souborů	soubor	k1gInPc2	soubor
v	v	k7c6	v
internetových	internetový	k2eAgFnPc6d1	internetová
sítích	síť	k1gFnPc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Klient-server	Klienterver	k1gInSc1	Klient-server
<g/>
.	.	kIx.	.
</s>
<s>
Klient-server	Klienterver	k1gMnSc1	Klient-server
(	(	kIx(	(
<g/>
původem	původ	k1gMnSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
client-server	clienterver	k1gInSc1	client-server
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
počítač	počítač	k1gInSc1	počítač
(	(	kIx(	(
<g/>
server	server	k1gInSc1	server
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
počítačů	počítač	k1gMnPc2	počítač
(	(	kIx(	(
<g/>
několik	několik	k4yIc4	několik
serverů	server	k1gInPc2	server
<g/>
)	)	kIx)	)
nadřazen	nadřazen	k2eAgInSc1d1	nadřazen
jinému	jiný	k2eAgInSc3d1	jiný
počítači	počítač	k1gInSc3	počítač
(	(	kIx(	(
<g/>
klientovi	klientův	k2eAgMnPc1d1	klientův
<g/>
)	)	kIx)	)
či	či	k8xC	či
několika	několik	k4yIc3	několik
počítačům	počítač	k1gInPc3	počítač
(	(	kIx(	(
<g/>
několika	několik	k4yIc7	několik
klientům	klient	k1gMnPc3	klient
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
služby	služba	k1gFnPc4	služba
"	"	kIx"	"
<g/>
běžným	běžný	k2eAgFnPc3d1	běžná
<g/>
"	"	kIx"	"
stanicím	stanice	k1gFnPc3	stanice
-	-	kIx~	-
klientům	klient	k1gMnPc3	klient
(	(	kIx(	(
<g/>
zvaným	zvaný	k2eAgInSc7d1	zvaný
workstation	workstation	k1gInSc7	workstation
nebo	nebo	k8xC	nebo
pracovní	pracovní	k2eAgFnSc2d1	pracovní
stanice	stanice	k1gFnSc2	stanice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Serverů	server	k1gInPc2	server
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
typů	typ	k1gInPc2	typ
podle	podle	k7c2	podle
poskytovaných	poskytovaný	k2eAgFnPc2d1	poskytovaná
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
souborový	souborový	k2eAgInSc1d1	souborový
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
tiskový	tiskový	k2eAgInSc1d1	tiskový
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
poštovní	poštovní	k2eAgInSc1d1	poštovní
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
WWW	WWW	kA	WWW
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
FTP	FTP	kA	FTP
server	server	k1gInSc1	server
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nemusí	muset	k5eNaImIp3nP	muset
platit	platit	k5eAaImF	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
server	server	k1gInSc1	server
je	být	k5eAaImIp3nS	být
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
u	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
sítí	síť	k1gFnPc2	síť
plní	plnit	k5eAaImIp3nP	plnit
úlohu	úloha	k1gFnSc4	úloha
několika	několik	k4yIc2	několik
typů	typ	k1gInPc2	typ
serverů	server	k1gInPc2	server
jeden	jeden	k4xCgInSc4	jeden
"	"	kIx"	"
<g/>
fyzický	fyzický	k2eAgInSc4d1	fyzický
<g/>
"	"	kIx"	"
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
u	u	k7c2	u
velkých	velký	k2eAgFnPc2d1	velká
sítí	síť	k1gFnPc2	síť
může	moct	k5eAaImIp3nS	moct
např.	např.	kA	např.
jeden	jeden	k4xCgInSc4	jeden
"	"	kIx"	"
<g/>
fyzický	fyzický	k2eAgInSc4d1	fyzický
<g/>
"	"	kIx"	"
počítač	počítač	k1gInSc4	počítač
plnit	plnit	k5eAaImF	plnit
pouze	pouze	k6eAd1	pouze
úlohu	úloha	k1gFnSc4	úloha
tiskového	tiskový	k2eAgInSc2d1	tiskový
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
může	moct	k5eAaImIp3nS	moct
dokonce	dokonce	k9	dokonce
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
jako	jako	k9	jako
běžná	běžný	k2eAgFnSc1d1	běžná
pracovní	pracovní	k2eAgFnSc1d1	pracovní
stanice	stanice	k1gFnSc1	stanice
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
ojedinělé	ojedinělý	k2eAgFnPc1d1	ojedinělá
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
se	se	k3xPyFc4	se
výkonnost	výkonnost	k1gFnSc1	výkonnost
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Analogová	analogový	k2eAgFnSc1d1	analogová
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Analogová	analogový	k2eAgFnSc1d1	analogová
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
elektronický	elektronický	k2eAgInSc4d1	elektronický
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
analogovým	analogový	k2eAgInSc7d1	analogový
(	(	kIx(	(
<g/>
spojitým	spojitý	k2eAgInSc7d1	spojitý
<g/>
)	)	kIx)	)
proměnným	proměnný	k2eAgInSc7d1	proměnný
signálem	signál	k1gInSc7	signál
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
digitální	digitální	k2eAgFnSc2d1	digitální
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
různých	různý	k2eAgFnPc2d1	různá
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Analogové	analogový	k2eAgInPc1d1	analogový
signály	signál	k1gInPc1	signál
můžeme	moct	k5eAaImIp1nP	moct
rozdělovat	rozdělovat	k5eAaImF	rozdělovat
podle	podle	k7c2	podle
média	médium	k1gNnSc2	médium
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
jsou	být	k5eAaImIp3nP	být
přenášeny	přenášen	k2eAgFnPc1d1	přenášena
<g/>
.	.	kIx.	.
</s>
<s>
Mluvíme	mluvit	k5eAaImIp1nP	mluvit
tak	tak	k9	tak
například	například	k6eAd1	například
o	o	k7c6	o
akustických	akustický	k2eAgInPc6d1	akustický
signálech	signál	k1gInPc6	signál
<g/>
,	,	kIx,	,
elektrických	elektrický	k2eAgInPc6d1	elektrický
signálech	signál	k1gInPc6	signál
<g/>
,	,	kIx,	,
optických	optický	k2eAgInPc6d1	optický
signálech	signál	k1gInPc6	signál
apod.	apod.	kA	apod.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Digitální	digitální	k2eAgFnSc1d1	digitální
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgFnSc1d1	digitální
síť	síť	k1gFnSc1	síť
představuje	představovat	k5eAaImIp3nS	představovat
signály	signál	k1gInPc4	signál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
spíše	spíše	k9	spíše
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
šířky	šířka	k1gFnPc1	šířka
analogové	analogový	k2eAgFnSc2d1	analogová
úrovně	úroveň	k1gFnSc2	úroveň
než	než	k8xS	než
nepřetržitý	přetržitý	k2eNgInSc4d1	nepřetržitý
rozsah	rozsah	k1gInSc4	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
analogovými	analogový	k2eAgFnPc7d1	analogová
sítěmi	síť	k1gFnPc7	síť
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
ztráta	ztráta	k1gFnSc1	ztráta
kvality	kvalita	k1gFnSc2	kvalita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kvůli	kvůli	k7c3	kvůli
hluku	hluk	k1gInSc3	hluk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
snadnější	snadný	k2eAgNnSc4d2	snazší
ukládání	ukládání	k1gNnSc4	ukládání
informací	informace	k1gFnPc2	informace
bez	bez	k7c2	bez
degradace	degradace	k1gFnSc2	degradace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
občasná	občasný	k2eAgFnSc1d1	občasná
spotřeba	spotřeba	k1gFnSc1	spotřeba
větší	veliký	k2eAgFnSc2d2	veliký
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
u	u	k7c2	u
analogových	analogový	k2eAgInPc2d1	analogový
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
při	při	k7c6	při
stejné	stejný	k2eAgFnSc6d1	stejná
funkci	funkce	k1gFnSc6	funkce
<g/>
)	)	kIx)	)
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
produkuje	produkovat	k5eAaImIp3nS	produkovat
více	hodně	k6eAd2	hodně
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
složitost	složitost	k1gFnSc1	složitost
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
začlenění	začlenění	k1gNnSc1	začlenění
chladiče	chladič	k1gInSc2	chladič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přenosných	přenosný	k2eAgFnPc6d1	přenosná
a	a	k8xC	a
bateriově	bateriově	k6eAd1	bateriově
napájených	napájený	k2eAgInPc6d1	napájený
systémech	systém	k1gInPc6	systém
to	ten	k3xDgNnSc4	ten
může	moct	k5eAaImIp3nS	moct
omezit	omezit	k5eAaPmF	omezit
využívání	využívání	k1gNnSc4	využívání
digitálních	digitální	k2eAgInPc2d1	digitální
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgInPc1d1	digitální
obvody	obvod	k1gInPc1	obvod
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
někdy	někdy	k6eAd1	někdy
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
množstvích	množství	k1gNnPc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozlehlosti	rozlehlost	k1gFnSc2	rozlehlost
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
sítí	síť	k1gFnPc2	síť
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
sítě	síť	k1gFnPc4	síť
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
základní	základní	k2eAgFnPc4d1	základní
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
PAN	Pan	k1gMnSc1	Pan
-	-	kIx~	-
původem	původ	k1gInSc7	původ
anglickém	anglický	k2eAgInSc6d1	anglický
Personal	Personal	k1gFnPc7	Personal
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
<g/>
,	,	kIx,	,
též	též	k9	též
zván	zván	k2eAgInSc4d1	zván
osobní	osobní	k2eAgFnSc4d1	osobní
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velice	velice	k6eAd1	velice
malá	malý	k2eAgFnSc1d1	malá
počítačová	počítačový	k2eAgFnSc1d1	počítačová
síť	síť	k1gFnSc1	síť
používaná	používaný	k2eAgFnSc1d1	používaná
pro	pro	k7c4	pro
propojení	propojení	k1gNnSc2	propojení
jeho	jeho	k3xOp3gNnPc2	jeho
osobních	osobní	k2eAgNnPc2d1	osobní
elektronických	elektronický	k2eAgNnPc2d1	elektronické
zařízení	zařízení	k1gNnPc2	zařízení
typu	typ	k1gInSc2	typ
mobilní	mobilní	k2eAgFnSc1d1	mobilní
telefon	telefon	k1gInSc1	telefon
<g/>
,	,	kIx,	,
PDA	PDA	kA	PDA
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
notebook	notebook	k1gInSc4	notebook
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
rozlehlost	rozlehlost	k1gFnSc1	rozlehlost
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
sítí	síť	k1gFnPc2	síť
nejmenší	malý	k2eAgFnPc4d3	nejmenší
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Bluetooth	Bluetooth	k1gInSc1	Bluetooth
<g/>
,	,	kIx,	,
IA	ia	k0	ia
a	a	k8xC	a
ZigBee	ZigBee	k1gFnPc6	ZigBee
LAN	lano	k1gNnPc2	lano
-	-	kIx~	-
původem	původ	k1gMnSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Local	Local	k1gInSc1	Local
Area	Ares	k1gMnSc4	Ares
Network	network	k1gInSc2	network
<g/>
,	,	kIx,	,
též	též	k9	též
zván	zván	k2eAgInSc1d1	zván
lokální	lokální	k2eAgFnSc1d1	lokální
počítačová	počítačový	k2eAgFnSc1d1	počítačová
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
lokální	lokální	k2eAgFnSc4d1	lokální
síť	síť	k1gFnSc4	síť
nebo	nebo	k8xC	nebo
místní	místní	k2eAgFnSc4d1	místní
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
síť	síť	k1gFnSc4	síť
spojující	spojující	k2eAgInPc1d1	spojující
uzly	uzel	k1gInPc1	uzel
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
budovy	budova	k1gFnSc2	budova
nebo	nebo	k8xC	nebo
několika	několik	k4yIc2	několik
blízkých	blízký	k2eAgFnPc2d1	blízká
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
stovky	stovka	k1gFnSc2	stovka
metrů	metr	k1gInPc2	metr
až	až	k8xS	až
kilometry	kilometr	k1gInPc4	kilometr
(	(	kIx(	(
<g/>
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
optiky	optika	k1gFnSc2	optika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozlehlost	rozlehlost	k1gFnSc1	rozlehlost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
rozlehlost	rozlehlost	k1gFnSc4	rozlehlost
PAN	Pan	k1gMnSc1	Pan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
rozlehlost	rozlehlost	k1gFnSc1	rozlehlost
MAN	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
Ethernet	Ethernet	k1gInSc1	Ethernet
<g/>
;	;	kIx,	;
MAN	Man	k1gMnSc1	Man
-	-	kIx~	-
původem	původ	k1gMnSc7	původ
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
anglickém	anglický	k2eAgInSc6d1	anglický
Metropolitan	metropolitan	k1gInSc4	metropolitan
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
<g/>
,	,	kIx,	,
též	též	k9	též
zván	zván	k2eAgInSc4d1	zván
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
síť	síť	k1gFnSc1	síť
propojující	propojující	k2eAgFnSc1d1	propojující
lokální	lokální	k2eAgFnPc4d1	lokální
sítě	síť	k1gFnPc4	síť
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
zástavbě	zástavba	k1gFnSc6	zástavba
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
zvuku	zvuk	k1gInSc2	zvuk
a	a	k8xC	a
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
spojuje	spojovat	k5eAaImIp3nS	spojovat
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
řádově	řádově	k6eAd1	řádově
jednotek	jednotka	k1gFnPc2	jednotka
až	až	k8xS	až
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
rozlehlost	rozlehlost	k1gFnSc1	rozlehlost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
rozlehlost	rozlehlost	k1gFnSc1	rozlehlost
LAN	lano	k1gNnPc2	lano
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
rozlehlost	rozlehlost	k1gFnSc1	rozlehlost
WAN	WAN	kA	WAN
<g/>
;	;	kIx,	;
WAN	WAN	kA	WAN
-	-	kIx~	-
původem	původ	k1gMnSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
Wide	Wide	k1gNnSc6	Wide
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
<g/>
,	,	kIx,	,
též	též	k9	též
zván	zván	k2eAgInSc1d1	zván
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
síť	síť	k1gFnSc1	síť
spojující	spojující	k2eAgFnSc1d1	spojující
LAN	lano	k1gNnPc2	lano
a	a	k8xC	a
MAN	mana	k1gFnPc2	mana
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
největší	veliký	k2eAgFnSc4d3	veliký
působnost	působnost	k1gFnSc4	působnost
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
i	i	k9	i
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
kontinentu	kontinent	k1gInSc6	kontinent
nebo	nebo	k8xC	nebo
kamkoliv	kamkoliv	k6eAd1	kamkoliv
na	na	k7c6	na
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
nebo	nebo	k8xC	nebo
i	i	k9	i
do	do	k7c2	do
nejbližšího	blízký	k2eAgInSc2d3	Nejbližší
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Personal	Personal	k1gFnSc2	Personal
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgFnPc1d1	osobní
sítě	síť	k1gFnPc1	síť
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
též	též	k9	též
PAN	Pan	k1gMnSc1	Pan
(	(	kIx(	(
<g/>
původem	původ	k1gMnSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Personal	Personal	k1gFnPc7	Personal
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
sítě	síť	k1gFnPc1	síť
s	s	k7c7	s
nejmenší	malý	k2eAgFnSc7d3	nejmenší
rozlehlostí	rozlehlost	k1gFnSc7	rozlehlost
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
používané	používaný	k2eAgInPc1d1	používaný
pro	pro	k7c4	pro
propojení	propojení	k1gNnSc4	propojení
osobních	osobní	k2eAgNnPc2d1	osobní
elektronických	elektronický	k2eAgNnPc2d1	elektronické
zařízení	zařízení	k1gNnPc2	zařízení
typu	typ	k1gInSc2	typ
mobilní	mobilní	k2eAgFnSc1d1	mobilní
telefon	telefon	k1gInSc1	telefon
<g/>
,	,	kIx,	,
laptop	laptop	k1gInSc1	laptop
nebo	nebo	k8xC	nebo
PDA	PDA	kA	PDA
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgFnPc1d1	osobní
počítačové	počítačový	k2eAgFnPc1d1	počítačová
sítě	síť	k1gFnPc1	síť
si	se	k3xPyFc3	se
nekladou	klást	k5eNaImIp3nP	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
co	co	k8xS	co
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
přenosovou	přenosový	k2eAgFnSc4d1	přenosová
rychlost	rychlost	k1gFnSc4	rychlost
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
u	u	k7c2	u
PAN	Pan	k1gMnSc1	Pan
obvykle	obvykle	k6eAd1	obvykle
nepřekračuje	překračovat	k5eNaImIp3nS	překračovat
několik	několik	k4yIc4	několik
megabitů	megabit	k1gInPc2	megabit
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
rušení	rušení	k1gNnSc3	rušení
<g/>
,	,	kIx,	,
nízkou	nízký	k2eAgFnSc4d1	nízká
spotřebu	spotřeba	k1gFnSc4	spotřeba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
nebo	nebo	k8xC	nebo
také	také	k9	také
třeba	třeba	k6eAd1	třeba
snadnou	snadný	k2eAgFnSc4d1	snadná
konfigurovatelnost	konfigurovatelnost	k1gFnSc4	konfigurovatelnost
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc4d1	malý
dosah	dosah	k1gInSc4	dosah
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
realizovaná	realizovaný	k2eAgFnSc1d1	realizovaná
pomocí	pomocí	k7c2	pomocí
technologie	technologie	k1gFnSc2	technologie
Bluetooth	Bluetooth	k1gInSc1	Bluetooth
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
Wi-Fi	Wi-Fi	k1gNnSc1	Wi-Fi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ZigBee	ZigBee	k1gFnPc7	ZigBee
nebo	nebo	k8xC	nebo
IA	ia	k0	ia
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Local	Local	k1gInSc1	Local
Area	Ares	k1gMnSc4	Ares
Network	network	k1gInSc2	network
<g/>
.	.	kIx.	.
</s>
<s>
Lokální	lokální	k2eAgFnPc1d1	lokální
počítačové	počítačový	k2eAgFnPc1d1	počítačová
sítě	síť	k1gFnPc1	síť
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgNnSc1d1	zvané
též	též	k9	též
lokální	lokální	k2eAgFnPc4d1	lokální
sítě	síť	k1gFnPc4	síť
<g/>
,	,	kIx,	,
místní	místní	k2eAgMnPc4d1	místní
sítě	síť	k1gFnSc2	síť
nebo	nebo	k8xC	nebo
LAN	lano	k1gNnPc2	lano
(	(	kIx(	(
<g/>
původem	původ	k1gMnSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Local	Local	k1gInSc4	Local
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
sítě	síť	k1gFnPc1	síť
propojující	propojující	k2eAgInPc4d1	propojující
koncové	koncový	k2eAgInPc4d1	koncový
uzly	uzel	k1gInPc4	uzel
typu	typ	k1gInSc2	typ
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
tiskárna	tiskárna	k1gFnSc1	tiskárna
<g/>
,	,	kIx,	,
server	server	k1gInSc1	server
<g/>
.	.	kIx.	.
</s>
<s>
LAN	lano	k1gNnPc2	lano
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
soukromé	soukromý	k2eAgFnSc6d1	soukromá
správě	správa	k1gFnSc6	správa
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
malém	malý	k2eAgNnSc6d1	malé
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Připojená	připojený	k2eAgNnPc1d1	připojené
zařízení	zařízení	k1gNnPc1	zařízení
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
bez	bez	k7c2	bez
navazování	navazování	k1gNnSc2	navazování
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
sdílí	sdílet	k5eAaImIp3nS	sdílet
jeden	jeden	k4xCgInSc4	jeden
přenosový	přenosový	k2eAgInSc4d1	přenosový
prostředek	prostředek	k1gInSc4	prostředek
(	(	kIx(	(
<g/>
drát	drát	k1gInSc1	drát
<g/>
,	,	kIx,	,
radiové	radiový	k2eAgFnPc1d1	radiová
vlny	vlna	k1gFnPc1	vlna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgNnSc3	který
je	být	k5eAaImIp3nS	být
umožněn	umožněn	k2eAgInSc1d1	umožněn
mnohonásobný	mnohonásobný	k2eAgInSc1d1	mnohonásobný
přístup	přístup	k1gInSc1	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Přenosové	přenosový	k2eAgFnPc1d1	přenosová
rychlosti	rychlost	k1gFnPc1	rychlost
LAN	lano	k1gNnPc2	lano
začínají	začínat	k5eAaImIp3nP	začínat
na	na	k7c6	na
desítkách	desítka	k1gFnPc6	desítka
megabitů	megabit	k1gInPc2	megabit
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přenos	přenos	k1gInSc4	přenos
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
několik	několik	k4yIc4	několik
gigabitů	gigabit	k1gInPc2	gigabit
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
lokální	lokální	k2eAgFnPc4d1	lokální
sítě	síť	k1gFnPc4	síť
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
IEEE	IEEE	kA	IEEE
802.3	[number]	k4	802.3
(	(	kIx(	(
<g/>
Ethernet	Ethernet	k1gMnSc1	Ethernet
<g/>
,	,	kIx,	,
Fast	Fast	k1gMnSc1	Fast
Ethernet	Ethernet	k1gMnSc1	Ethernet
a	a	k8xC	a
Gigabit	Gigabit	k1gMnSc1	Gigabit
Ethernet	Ethernet	k1gMnSc1	Ethernet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ARCNET	ARCNET	kA	ARCNET
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
technologie	technologie	k1gFnSc1	technologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Token	Token	k2eAgInSc1d1	Token
Bus	bus	k1gInSc1	bus
(	(	kIx(	(
<g/>
IEEE	IEEE	kA	IEEE
802.4	[number]	k4	802.4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Token	Token	k2eAgInSc1d1	Token
ring	ring	k1gInSc1	ring
(	(	kIx(	(
<g/>
IEEE	IEEE	kA	IEEE
802.5	[number]	k4	802.5
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
IsoEthernet	IsoEthernet	k1gMnSc1	IsoEthernet
(	(	kIx(	(
<g/>
IEEE	IEEE	kA	IEEE
802.9	[number]	k4	802.9
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
a	a	k8xC	a
IEEE	IEEE	kA	IEEE
802.11	[number]	k4	802.11
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
BaseVG	BaseVG	k1gFnPc2	BaseVG
(	(	kIx(	(
<g/>
IEEE	IEEE	kA	IEEE
802.12	[number]	k4	802.12
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
FDDI	FDDI	kA	FDDI
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
9314	[number]	k4	9314
a	a	k8xC	a
ANSI	ANSI	kA	ANSI
X	X	kA	X
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
Fibre	Fibr	k1gInSc5	Fibr
Channel	Channel	k1gMnSc1	Channel
(	(	kIx(	(
<g/>
SCSI	SCSI	kA	SCSI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Metropolitan	metropolitan	k1gInSc1	metropolitan
Area	Ares	k1gMnSc4	Ares
Network	network	k1gInSc2	network
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnPc1d1	metropolitní
sítě	síť	k1gFnPc1	síť
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
též	též	k9	též
MAN	mana	k1gFnPc2	mana
(	(	kIx(	(
<g/>
původem	původ	k1gMnSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Metropolitan	metropolitan	k1gInSc4	metropolitan
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
sítě	síť	k1gFnPc1	síť
umožňující	umožňující	k2eAgFnPc1d1	umožňující
rozšíření	rozšíření	k1gNnSc4	rozšíření
působnosti	působnost	k1gFnSc2	působnost
lokálních	lokální	k2eAgFnPc2d1	lokální
sítí	síť	k1gFnPc2	síť
jejich	jejich	k3xOp3gNnSc7	jejich
prodloužením	prodloužení	k1gNnSc7	prodloužení
<g/>
,	,	kIx,	,
zvýšením	zvýšení	k1gNnSc7	zvýšení
počtu	počet	k1gInSc2	počet
připojených	připojený	k2eAgFnPc2d1	připojená
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
zvýšením	zvýšení	k1gNnSc7	zvýšení
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
MAN	mana	k1gFnPc2	mana
sítí	síť	k1gFnPc2	síť
bývá	bývat	k5eAaImIp3nS	bývat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
charakterem	charakter	k1gInSc7	charakter
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
zařadit	zařadit	k5eAaPmF	zařadit
k	k	k7c3	k
sítím	síť	k1gFnPc3	síť
LAN	lano	k1gNnPc2	lano
<g/>
.	.	kIx.	.
</s>
<s>
Sítě	síť	k1gFnPc1	síť
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jak	jak	k6eAd1	jak
privátní	privátní	k2eAgFnSc1d1	privátní
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
veřejné	veřejný	k2eAgInPc1d1	veřejný
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
provozovatel	provozovatel	k1gMnSc1	provozovatel
pronajímá	pronajímat	k5eAaImIp3nS	pronajímat
různým	různý	k2eAgMnPc3d1	různý
uživatelům	uživatel	k1gMnPc3	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Normalizovaná	normalizovaný	k2eAgFnSc1d1	normalizovaná
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
síť	síť	k1gFnSc1	síť
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
tou	ten	k3xDgFnSc7	ten
je	on	k3xPp3gFnPc4	on
protokol	protokol	k1gInSc1	protokol
DQDB	DQDB	kA	DQDB
(	(	kIx(	(
<g/>
původem	původ	k1gMnSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Distributed	Distributed	k1gInSc1	Distributed
Queue	Queue	k1gFnSc1	Queue
Dual	Dual	k1gMnSc1	Dual
Bus	bus	k1gInSc1	bus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DQDB	DQDB	kA	DQDB
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
koncepci	koncepce	k1gFnSc4	koncepce
ATM	ATM	kA	ATM
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
obdobně	obdobně	k6eAd1	obdobně
kapacita	kapacita	k1gFnSc1	kapacita
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
malé	malý	k2eAgInPc4d1	malý
kousky	kousek	k1gInPc4	kousek
(	(	kIx(	(
<g/>
buňky	buňka	k1gFnPc4	buňka
<g/>
)	)	kIx)	)
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
délkou	délka	k1gFnSc7	délka
(	(	kIx(	(
<g/>
53	[number]	k4	53
bytů	byt	k1gInPc2	byt
<g/>
;	;	kIx,	;
48	[number]	k4	48
bytů	byt	k1gInPc2	byt
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
5	[number]	k4	5
bytů	byt	k1gInPc2	byt
záhlaví	záhlaví	k1gNnSc2	záhlaví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezi	mezi	k7c7	mezi
účastníky	účastník	k1gMnPc7	účastník
komunikace	komunikace	k1gFnSc2	komunikace
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vytvořeno	vytvořen	k2eAgNnSc4d1	vytvořeno
virtuální	virtuální	k2eAgNnSc4d1	virtuální
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Wide	Wid	k1gFnSc2	Wid
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
<g/>
.	.	kIx.	.
</s>
<s>
Rozlehlé	rozlehlý	k2eAgFnPc1d1	rozlehlá
sítě	síť	k1gFnPc1	síť
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
též	též	k9	též
WAN	WAN	kA	WAN
(	(	kIx(	(
<g/>
původem	původ	k1gMnSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
Wide	Wide	k1gNnSc6	Wide
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
sítě	síť	k1gFnPc4	síť
umožňující	umožňující	k2eAgFnSc4d1	umožňující
komunikaci	komunikace	k1gFnSc4	komunikace
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
veřejné	veřejný	k2eAgFnPc1d1	veřejná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
privátní	privátní	k2eAgInSc4d1	privátní
WAN	WAN	kA	WAN
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
sítě	síť	k1gFnPc1	síť
typicky	typicky	k6eAd1	typicky
pracující	pracující	k2eAgInPc1d1	pracující
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
komunikace	komunikace	k1gFnSc2	komunikace
se	s	k7c7	s
spojením	spojení	k1gNnSc7	spojení
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nepoužívají	používat	k5eNaImIp3nP	používat
sdílený	sdílený	k2eAgInSc4d1	sdílený
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Přenosové	přenosový	k2eAgFnPc1d1	přenosová
rychlosti	rychlost	k1gFnPc1	rychlost
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Začínají	začínat	k5eAaImIp3nP	začínat
na	na	k7c6	na
desítkách	desítka	k1gFnPc6	desítka
kilobitů	kilobit	k1gInPc2	kilobit
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
i	i	k9	i
rychlostí	rychlost	k1gFnSc7	rychlost
řádu	řád	k1gInSc2	řád
několik	několik	k4yIc4	několik
gigabitů	gigabit	k1gInPc2	gigabit
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takové	takový	k3xDgFnSc2	takový
sítě	síť	k1gFnSc2	síť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
rozlehlé	rozlehlý	k2eAgFnPc4d1	rozlehlá
sítě	síť	k1gFnPc4	síť
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
ISDN	ISDN	kA	ISDN
X	X	kA	X
<g/>
.25	.25	k4	.25
Frame	Fram	k1gInSc5	Fram
Relay	Rela	k2eAgInPc4d1	Rela
SMDS	SMDS	kA	SMDS
ATM	ATM	kA	ATM
WiMAX	WiMAX	k1gMnPc3	WiMAX
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
datová	datový	k2eAgFnSc1d1	datová
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
datová	datový	k2eAgFnSc1d1	datová
síť	síť	k1gFnSc1	síť
(	(	kIx(	(
<g/>
PDN	PDN	kA	PDN
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Public	publicum	k1gNnPc2	publicum
data	datum	k1gNnSc2	datum
network	network	k1gInSc4	network
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
veřejné	veřejný	k2eAgFnSc2d1	veřejná
telekomunikační	telekomunikační	k2eAgFnSc2d1	telekomunikační
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Provozovateli	provozovatel	k1gMnPc7	provozovatel
jsou	být	k5eAaImIp3nP	být
spojové	spojový	k2eAgFnPc1d1	spojová
organizace	organizace	k1gFnPc1	organizace
(	(	kIx(	(
<g/>
správy	správa	k1gFnPc4	správa
spojů	spoj	k1gInPc2	spoj
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc4d1	jiný
subjekty	subjekt	k1gInPc4	subjekt
splňující	splňující	k2eAgInPc4d1	splňující
legislativní	legislativní	k2eAgInPc4d1	legislativní
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nabízejí	nabízet	k5eAaImIp3nP	nabízet
své	svůj	k3xOyFgFnPc4	svůj
služby	služba	k1gFnPc4	služba
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Sítě	síť	k1gFnSc2	síť
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
komunikace	komunikace	k1gFnSc2	komunikace
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
subjekty	subjekt	k1gInPc7	subjekt
(	(	kIx(	(
<g/>
také	také	k9	také
připojenými	připojený	k2eAgFnPc7d1	připojená
k	k	k7c3	k
veřejné	veřejný	k2eAgFnSc3d1	veřejná
datové	datový	k2eAgFnSc3d1	datová
síti	síť	k1gFnSc3	síť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
veřejné	veřejný	k2eAgFnSc2d1	veřejná
datové	datový	k2eAgFnSc2d1	datová
sítě	síť	k1gFnSc2	síť
propojovaly	propojovat	k5eAaImAgInP	propojovat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
své	svůj	k3xOyFgFnPc4	svůj
dílčí	dílčí	k2eAgFnPc4d1	dílčí
lokální	lokální	k2eAgFnPc4d1	lokální
sítě	síť	k1gFnPc4	síť
apod.	apod.	kA	apod.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Privátní	privátní	k2eAgFnSc4d1	privátní
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Privátní	privátní	k2eAgFnSc1d1	privátní
síť	síť	k1gFnSc1	síť
využívá	využívat	k5eAaImIp3nS	využívat
speciální	speciální	k2eAgInSc4d1	speciální
privátní	privátní	k2eAgInSc4d1	privátní
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
podle	podle	k7c2	podle
standardů	standard	k1gInPc2	standard
daných	daný	k2eAgInPc2d1	daný
RFC	RFC	kA	RFC
1918	[number]	k4	1918
a	a	k8xC	a
RFC	RFC	kA	RFC
4193	[number]	k4	4193
<g/>
.	.	kIx.	.
</s>
<s>
Privátní	privátní	k2eAgFnPc1d1	privátní
adresy	adresa	k1gFnPc1	adresa
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
používány	používat	k5eAaImNgFnP	používat
pro	pro	k7c4	pro
domácí	domácí	k2eAgFnPc4d1	domácí
<g/>
,	,	kIx,	,
kancelářské	kancelářský	k2eAgFnPc4d1	kancelářská
a	a	k8xC	a
podnikové	podnikový	k2eAgFnPc4d1	podniková
lokální	lokální	k2eAgFnPc4d1	lokální
sítě	síť	k1gFnPc4	síť
(	(	kIx(	(
<g/>
LAN	lano	k1gNnPc2	lano
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
veřejné	veřejný	k2eAgFnSc2d1	veřejná
adresy	adresa	k1gFnSc2	adresa
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
globálně	globálně	k6eAd1	globálně
směrovatelné	směrovatelný	k2eAgInPc4d1	směrovatelný
v	v	k7c6	v
internetu	internet	k1gInSc6	internet
<g/>
)	)	kIx)	)
nejsou	být	k5eNaImIp3nP	být
žádoucí	žádoucí	k2eAgNnPc1d1	žádoucí
nebo	nebo	k8xC	nebo
nejsou	být	k5eNaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Privátní	privátní	k2eAgInPc1d1	privátní
rozsahy	rozsah	k1gInPc1	rozsah
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
byly	být	k5eAaImAgInP	být
definovány	definován	k2eAgInPc1d1	definován
jako	jako	k8xC	jako
nástroj	nástroj	k1gInSc1	nástroj
pro	pro	k7c4	pro
zpomalení	zpomalení	k1gNnSc4	zpomalení
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
IPv	IPv	k1gFnSc2	IPv
<g/>
4	[number]	k4	4
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
součástí	součást	k1gFnSc7	součást
nastupující	nastupující	k2eAgFnSc2d1	nastupující
generace	generace	k1gFnSc2	generace
pro	pro	k7c4	pro
Internet	Internet	k1gInSc4	Internet
Protocol	Protocol	k1gInSc1	Protocol
verze	verze	k1gFnSc1	verze
6	[number]	k4	6
(	(	kIx(	(
<g/>
IPv	IPv	k1gFnSc1	IPv
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Virtuální	virtuální	k2eAgFnSc1d1	virtuální
privátní	privátní	k2eAgFnSc1d1	privátní
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Virtuální	virtuální	k2eAgFnSc1d1	virtuální
privátní	privátní	k2eAgFnSc1d1	privátní
síť	síť	k1gFnSc1	síť
(	(	kIx(	(
<g/>
VPN	VPN	kA	VPN
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Virtual	Virtual	k1gInSc1	Virtual
private	privat	k1gInSc5	privat
network	network	k1gInPc3	network
<g/>
)	)	kIx)	)
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
propojení	propojení	k1gNnSc4	propojení
několika	několik	k4yIc2	několik
počítačů	počítač	k1gInPc2	počítač
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
(	(	kIx(	(
<g/>
veřejné	veřejný	k2eAgFnSc2d1	veřejná
<g/>
)	)	kIx)	)
nedůvěryhodné	důvěryhodný	k2eNgFnSc2d1	nedůvěryhodná
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tak	tak	k9	tak
snadno	snadno	k6eAd1	snadno
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
stavu	stav	k1gInSc3	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spojené	spojený	k2eAgInPc1d1	spojený
počítače	počítač	k1gInPc1	počítač
budou	být	k5eAaImBp3nP	být
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
moci	moct	k5eAaImF	moct
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kdyby	kdyby	kYmCp3nP	kdyby
byly	být	k5eAaImAgFnP	být
propojeny	propojit	k5eAaPmNgFnP	propojit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jediné	jediný	k2eAgNnSc1d1	jediné
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
privátní	privátní	k2eAgNnSc1d1	privátní
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
důvěryhodné	důvěryhodný	k2eAgFnSc2d1	důvěryhodná
<g/>
)	)	kIx)	)
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
navazování	navazování	k1gNnSc6	navazování
spojení	spojení	k1gNnSc2	spojení
je	být	k5eAaImIp3nS	být
totožnost	totožnost	k1gFnSc4	totožnost
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
ověřována	ověřován	k2eAgFnSc1d1	ověřována
pomocí	pomocí	k7c2	pomocí
digitálních	digitální	k2eAgInPc2d1	digitální
certifikátů	certifikát	k1gInPc2	certifikát
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
autentizaci	autentizace	k1gFnSc3	autentizace
<g/>
,	,	kIx,	,
veškerá	veškerý	k3xTgFnSc1	veškerý
komunikace	komunikace	k1gFnSc1	komunikace
je	být	k5eAaImIp3nS	být
šifrována	šifrován	k2eAgFnSc1d1	šifrována
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
můžeme	moct	k5eAaImIp1nP	moct
takové	takový	k3xDgNnSc4	takový
propojení	propojení	k1gNnSc4	propojení
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
bezpečné	bezpečný	k2eAgFnPc4d1	bezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Síťové	síťový	k2eAgNnSc1d1	síťové
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Pojmem	pojem	k1gInSc7	pojem
síťové	síťový	k2eAgNnSc4d1	síťové
zařízení	zařízení	k1gNnSc4	zařízení
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
všechna	všechen	k3xTgNnPc1	všechen
zařízení	zařízení	k1gNnPc1	zařízení
(	(	kIx(	(
<g/>
prvky	prvek	k1gInPc1	prvek
<g/>
)	)	kIx)	)
připojené	připojený	k2eAgFnPc1d1	připojená
do	do	k7c2	do
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přijímají	přijímat	k5eAaImIp3nP	přijímat
a	a	k8xC	a
vysílají	vysílat	k5eAaImIp3nP	vysílat
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Aktivní	aktivní	k2eAgInSc4d1	aktivní
síťový	síťový	k2eAgInSc4d1	síťový
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInPc1d1	aktivní
síťové	síťový	k2eAgInPc1d1	síťový
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
všechna	všechen	k3xTgNnPc4	všechen
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
propojení	propojení	k1gNnSc3	propojení
v	v	k7c6	v
počítačových	počítačový	k2eAgFnPc6d1	počítačová
sítích	síť	k1gFnPc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInSc1d1	aktivní
síťový	síťový	k2eAgInSc1d1	síťový
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc1	všechen
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
aktivně	aktivně	k6eAd1	aktivně
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
přenášené	přenášený	k2eAgInPc4d1	přenášený
signály	signál	k1gInPc4	signál
-	-	kIx~	-
tedy	tedy	k8xC	tedy
je	on	k3xPp3gInPc4	on
zesiluje	zesilovat	k5eAaImIp3nS	zesilovat
a	a	k8xC	a
různě	různě	k6eAd1	různě
modifikuje	modifikovat	k5eAaBmIp3nS	modifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
aktivní	aktivní	k2eAgInPc4d1	aktivní
prvky	prvek	k1gInPc4	prvek
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
především	především	k9	především
opakovač	opakovač	k1gInSc1	opakovač
<g/>
,	,	kIx,	,
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
switch	switcha	k1gFnPc2	switcha
<g/>
,	,	kIx,	,
bridge	bridge	k1gFnPc2	bridge
nebo	nebo	k8xC	nebo
router	routra	k1gFnPc2	routra
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
zde	zde	k6eAd1	zde
však	však	k9	však
i	i	k9	i
další	další	k2eAgNnPc1d1	další
zařízení	zařízení	k1gNnPc1	zařízení
jako	jako	k8xC	jako
například	například	k6eAd1	například
síťová	síťový	k2eAgFnSc1d1	síťová
karta	karta	k1gFnSc1	karta
<g/>
,	,	kIx,	,
tiskový	tiskový	k2eAgInSc1d1	tiskový
server	server	k1gInSc1	server
nebo	nebo	k8xC	nebo
host	host	k1gMnSc1	host
adapter	adapter	k1gMnSc1	adapter
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Opakovač	opakovač	k1gInSc1	opakovač
<g/>
.	.	kIx.	.
</s>
<s>
Repeater	Repeater	k1gInSc1	Repeater
nebo	nebo	k8xC	nebo
též	též	k9	též
opakovač	opakovač	k1gInSc4	opakovač
či	či	k8xC	či
někdy	někdy	k6eAd1	někdy
špatně	špatně	k6eAd1	špatně
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
zesilovač	zesilovač	k1gInSc1	zesilovač
je	být	k5eAaImIp3nS	být
elektronický	elektronický	k2eAgInSc1d1	elektronický
aktivní	aktivní	k2eAgInSc4d1	aktivní
síťový	síťový	k2eAgInSc4d1	síťový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přijímá	přijímat	k5eAaImIp3nS	přijímat
zkreslený	zkreslený	k2eAgInSc4d1	zkreslený
<g/>
,	,	kIx,	,
zašuměný	zašuměný	k2eAgInSc4d1	zašuměný
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
poškozený	poškozený	k2eAgInSc4d1	poškozený
signál	signál	k1gInSc4	signál
a	a	k8xC	a
opravený	opravený	k2eAgInSc4d1	opravený
<g/>
,	,	kIx,	,
zesílený	zesílený	k2eAgInSc4d1	zesílený
a	a	k8xC	a
správně	správně	k6eAd1	správně
časovaný	časovaný	k2eAgMnSc1d1	časovaný
ho	on	k3xPp3gNnSc4	on
vysílá	vysílat	k5eAaImIp3nS	vysílat
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
snadno	snadno	k6eAd1	snadno
zvýšit	zvýšit	k5eAaPmF	zvýšit
dosah	dosah	k1gInSc4	dosah
média	médium	k1gNnSc2	médium
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
kvality	kvalita	k1gFnSc2	kvalita
a	a	k8xC	a
obsahu	obsah	k1gInSc2	obsah
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Opakovače	opakovač	k1gInPc1	opakovač
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
(	(	kIx(	(
<g/>
fyzické	fyzický	k2eAgFnPc4d1	fyzická
<g/>
)	)	kIx)	)
vrstvy	vrstva	k1gFnPc4	vrstva
referenčního	referenční	k2eAgInSc2d1	referenční
modelu	model	k1gInSc2	model
OSI	OSI	kA	OSI
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pracují	pracovat	k5eAaImIp3nP	pracovat
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
signálem	signál	k1gInSc7	signál
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Hub	houba	k1gFnPc2	houba
nebo	nebo	k8xC	nebo
též	též	k9	též
rozbočovač	rozbočovač	k1gInSc4	rozbočovač
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
její	její	k3xOp3gNnPc4	její
větvení	větvení	k1gNnPc4	větvení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
sítí	síť	k1gFnPc2	síť
s	s	k7c7	s
hvězdicovou	hvězdicový	k2eAgFnSc7d1	hvězdicová
topologií	topologie	k1gFnSc7	topologie
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
opakovač	opakovač	k1gMnSc1	opakovač
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškerá	veškerý	k3xTgNnPc4	veškerý
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
přijdou	přijít	k5eAaPmIp3nP	přijít
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
portů	port	k1gInPc2	port
(	(	kIx(	(
<g/>
zásuvek	zásuvka	k1gFnPc2	zásuvka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkopíruje	zkopírovat	k5eAaPmIp3nS	zkopírovat
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
porty	porta	k1gFnPc4	porta
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
portu	porta	k1gFnSc4	porta
(	(	kIx(	(
<g/>
počítači	počítač	k1gMnPc1	počítač
a	a	k8xC	a
IP	IP	kA	IP
adrese	adresa	k1gFnSc6	adresa
<g/>
)	)	kIx)	)
data	datum	k1gNnPc1	datum
náleží	náležet	k5eAaImIp3nP	náležet
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc4	všechen
počítače	počítač	k1gInPc4	počítač
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
"	"	kIx"	"
<g/>
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
"	"	kIx"	"
všechna	všechen	k3xTgNnPc1	všechen
síťová	síťový	k2eAgNnPc1d1	síťové
data	datum	k1gNnPc1	datum
a	a	k8xC	a
u	u	k7c2	u
větších	veliký	k2eAgFnPc2d2	veliký
sítí	síť	k1gFnPc2	síť
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
přetěžování	přetěžování	k1gNnSc1	přetěžování
těch	ten	k3xDgMnPc2	ten
segmentů	segment	k1gInPc2	segment
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnPc3	který
data	datum	k1gNnPc1	datum
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nejsou	být	k5eNaImIp3nP	být
určena	určen	k2eAgNnPc1d1	určeno
<g/>
.	.	kIx.	.
</s>
<s>
Hub	houba	k1gFnPc2	houba
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
aktivní	aktivní	k2eAgNnSc1d1	aktivní
síťové	síťový	k2eAgNnSc1d1	síťové
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Nijak	nijak	k6eAd1	nijak
neřídí	řídit	k5eNaImIp3nS	řídit
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
skrz	skrz	k7c4	skrz
něj	on	k3xPp3gMnSc4	on
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
a	a	k8xC	a
vyslán	vyslat	k5eAaPmNgInS	vyslat
všemi	všecek	k3xTgNnPc7	všecek
ostatními	ostatní	k2eAgNnPc7d1	ostatní
porty	porto	k1gNnPc7	porto
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Switch	Switch	k1gInSc1	Switch
<g/>
.	.	kIx.	.
</s>
<s>
Switch	Switch	k1gInSc1	Switch
nebo	nebo	k8xC	nebo
též	též	k9	též
přepínač	přepínač	k1gInSc1	přepínač
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc4	prvek
propojující	propojující	k2eAgInPc4d1	propojující
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
segmenty	segment	k1gInPc4	segment
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Switch	Switch	k1gInSc1	Switch
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
větší	veliký	k2eAgNnSc4d2	veliký
či	či	k8xC	či
menší	malý	k2eAgNnSc4d2	menší
množství	množství	k1gNnSc4	množství
portů	port	k1gInPc2	port
(	(	kIx(	(
<g/>
až	až	k9	až
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
síťová	síťový	k2eAgNnPc1d1	síťové
zařízení	zařízení	k1gNnPc1	zařízení
nebo	nebo	k8xC	nebo
části	část	k1gFnPc1	část
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
switch	switch	k1gInSc1	switch
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
různá	různý	k2eAgNnPc4d1	různé
zařízení	zařízení	k1gNnPc4	zařízení
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
síťových	síťový	k2eAgFnPc2d1	síťová
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
(	(	kIx(	(
<g/>
linkové	linkový	k2eAgFnSc6d1	Linková
<g/>
)	)	kIx)	)
vrstvě	vrstva	k1gFnSc6	vrstva
OSI	OSI	kA	OSI
modelu	model	k1gInSc6	model
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
vyššího	vysoký	k2eAgInSc2d2	vyšší
výkonu	výkon	k1gInSc2	výkon
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc2	stanice
připojené	připojený	k2eAgFnSc2d1	připojená
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
rozhraním	rozhraní	k1gNnSc7	rozhraní
switche	switche	k6eAd1	switche
navzájem	navzájem	k6eAd1	navzájem
nesoutěží	soutěžit	k5eNaImIp3nS	soutěžit
o	o	k7c4	o
datové	datový	k2eAgNnSc4d1	datové
médium	médium	k1gNnSc4	médium
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
přínos	přínos	k1gInSc4	přínos
i	i	k9	i
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
médium	médium	k1gNnSc1	médium
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
sdíleno	sdílet	k5eAaImNgNnS	sdílet
a	a	k8xC	a
data	datum	k1gNnPc1	datum
se	se	k3xPyFc4	se
vysílají	vysílat	k5eAaImIp3nP	vysílat
jen	jen	k9	jen
do	do	k7c2	do
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
je	být	k5eAaImIp3nS	být
připojen	připojen	k2eAgMnSc1d1	připojen
jejich	jejich	k3xOp3gMnSc1	jejich
adresát	adresát	k1gMnSc1	adresát
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
Bridge	Bridgat	k5eAaPmIp3nS	Bridgat
nebo	nebo	k8xC	nebo
též	též	k9	též
most	most	k1gInSc4	most
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
sítě	síť	k1gFnSc2	síť
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
(	(	kIx(	(
<g/>
linkové	linkový	k2eAgFnSc6d1	Linková
<g/>
)	)	kIx)	)
vrstvě	vrstva	k1gFnSc6	vrstva
referenčního	referenční	k2eAgInSc2d1	referenční
modelu	model	k1gInSc2	model
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
OSI	OSI	kA	OSI
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
protokoly	protokol	k1gInPc4	protokol
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
transparentní	transparentní	k2eAgFnPc1d1	transparentní
(	(	kIx(	(
<g/>
neviditelný	viditelný	k2eNgInSc1d1	neviditelný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
provoz	provoz	k1gInSc4	provoz
různých	různý	k2eAgInPc2d1	různý
segmentů	segment	k1gInPc2	segment
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
i	i	k9	i
zatížení	zatížení	k1gNnSc1	zatížení
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
provoz	provoz	k1gInSc4	provoz
dvou	dva	k4xCgInPc2	dva
segmentů	segment	k1gInPc2	segment
sítě	síť	k1gFnSc2	síť
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
paměti	paměť	k1gFnSc6	paměť
RAM	RAM	kA	RAM
sám	sám	k3xTgMnSc1	sám
sestaví	sestavit	k5eAaPmIp3nS	sestavit
tabulku	tabulka	k1gFnSc4	tabulka
MAC	Mac	kA	Mac
(	(	kIx(	(
<g/>
fyzických	fyzický	k2eAgFnPc2d1	fyzická
<g/>
)	)	kIx)	)
adres	adresa	k1gFnPc2	adresa
a	a	k8xC	a
portů	port	k1gInPc2	port
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
dané	daný	k2eAgFnPc1d1	daná
adresy	adresa	k1gFnPc1	adresa
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
<g/>
-li	i	k?	-li
příjemce	příjemce	k1gMnSc1	příjemce
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
segmentu	segment	k1gInSc6	segment
jako	jako	k8xC	jako
odesílatel	odesílatel	k1gMnSc1	odesílatel
<g/>
,	,	kIx,	,
most	most	k1gInSc1	most
rámce	rámec	k1gInSc2	rámec
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
sítě	síť	k1gFnSc2	síť
neodešle	odeslat	k5eNaPmIp3nS	odeslat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
odešle	odešle	k6eAd1	odešle
do	do	k7c2	do
příslušného	příslušný	k2eAgInSc2d1	příslušný
segmentu	segment	k1gInSc2	segment
v	v	k7c6	v
nezměněném	změněný	k2eNgInSc6d1	nezměněný
stavu	stav	k1gInSc6	stav
(	(	kIx(	(
<g/>
týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
tzv.	tzv.	kA	tzv.
Unicast	Unicast	k1gFnSc1	Unicast
rámců	rámec	k1gInPc2	rámec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
jedinému	jediné	k1gNnSc3	jediné
příjemci	příjemce	k1gMnPc1	příjemce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Router	Router	k1gInSc1	Router
<g/>
.	.	kIx.	.
</s>
<s>
Router	Router	k1gInSc1	Router
nebo	nebo	k8xC	nebo
též	též	k9	též
směrovač	směrovač	k1gInSc4	směrovač
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
procesem	proces	k1gInSc7	proces
zvaným	zvaný	k2eAgInSc7d1	zvaný
routování	routování	k1gNnSc4	routování
přeposílá	přeposílat	k5eAaImIp3nS	přeposílat
datagramy	datagram	k1gInPc1	datagram
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Routování	Routování	k1gNnSc1	Routování
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
(	(	kIx(	(
<g/>
síťové	síťový	k2eAgFnSc6d1	síťová
<g/>
)	)	kIx)	)
vrstvě	vrstva	k1gFnSc6	vrstva
referenčního	referenční	k2eAgInSc2d1	referenční
modelu	model	k1gInSc2	model
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
OSI	OSI	kA	OSI
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
jako	jako	k9	jako
router	router	k1gMnSc1	router
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
počítač	počítač	k1gInSc4	počítač
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
síťování	síťování	k1gNnSc2	síťování
a	a	k8xC	a
pro	pro	k7c4	pro
routování	routování	k1gNnSc4	routování
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
sítích	síť	k1gFnPc6	síť
se	se	k3xPyFc4	se
často	často	k6eAd1	často
dodnes	dodnes	k6eAd1	dodnes
používají	používat	k5eAaImIp3nP	používat
běžné	běžný	k2eAgInPc1d1	běžný
osobní	osobní	k2eAgInPc1d1	osobní
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
do	do	k7c2	do
vysokorychlostních	vysokorychlostní	k2eAgFnPc2d1	vysokorychlostní
sítí	síť	k1gFnPc2	síť
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
jako	jako	k9	jako
routery	router	k1gInPc1	router
používány	používán	k2eAgInPc1d1	používán
vysoce	vysoce	k6eAd1	vysoce
účelové	účelový	k2eAgInPc1d1	účelový
počítače	počítač	k1gInPc1	počítač
obvykle	obvykle	k6eAd1	obvykle
se	s	k7c7	s
speciálním	speciální	k2eAgInSc7d1	speciální
hardwarem	hardware	k1gInSc7	hardware
<g/>
,	,	kIx,	,
optimalizovaným	optimalizovaný	k2eAgNnSc7d1	optimalizované
jak	jak	k8xS	jak
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
přeposílání	přeposílání	k1gNnSc4	přeposílání
(	(	kIx(	(
<g/>
forwarding	forwarding	k1gInSc4	forwarding
<g/>
)	)	kIx)	)
datagramů	datagram	k1gInPc2	datagram
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
funkce	funkce	k1gFnPc4	funkce
jako	jako	k8xC	jako
šifrování	šifrování	k1gNnSc4	šifrování
u	u	k7c2	u
IPsec	IPsec	k1gInSc4	IPsec
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pasivní	pasivní	k2eAgInSc4d1	pasivní
síťový	síťový	k2eAgInSc4d1	síťový
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pasivní	pasivní	k2eAgInPc4d1	pasivní
prvky	prvek	k1gInPc4	prvek
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
především	především	k6eAd1	především
datové	datový	k2eAgInPc4d1	datový
rozvaděče	rozvaděč	k1gInPc4	rozvaděč
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
fyzicky	fyzicky	k6eAd1	fyzicky
přenášejí	přenášet	k5eAaImIp3nP	přenášet
data	datum	k1gNnPc1	datum
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Kroucená	kroucený	k2eAgFnSc1d1	kroucená
dvojlinka	dvojlinka	k1gFnSc1	dvojlinka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
kabelu	kabel	k1gInSc2	kabel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
páry	pár	k1gInPc7	pár
vodičů	vodič	k1gInPc2	vodič
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
délce	délka	k1gFnSc6	délka
pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
způsobem	způsob	k1gInSc7	způsob
zkrouceny	zkroucen	k2eAgFnPc4d1	zkroucena
a	a	k8xC	a
následně	následně	k6eAd1	následně
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
sebe	se	k3xPyFc2	se
zakrouceny	zakroutit	k5eAaPmNgInP	zakroutit
i	i	k9	i
samy	sám	k3xTgInPc1	sám
výsledné	výsledný	k2eAgInPc1d1	výsledný
páry	pár	k1gInPc1	pár
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
twisted	twisted	k1gInSc1	twisted
<g/>
,	,	kIx,	,
odsud	odsud	k6eAd1	odsud
také	také	k9	také
twisted	twisted	k1gMnSc1	twisted
pair	pair	k1gMnSc1	pair
<g/>
,	,	kIx,	,
či	či	k8xC	či
zkráceně	zkráceně	k6eAd1	zkráceně
"	"	kIx"	"
<g/>
twist	twist	k1gInSc1	twist
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
vodiče	vodič	k1gInPc1	vodič
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rovnocenné	rovnocenný	k2eAgFnSc6d1	rovnocenná
pozici	pozice	k1gFnSc6	pozice
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
není	být	k5eNaImIp3nS	být
spojován	spojován	k2eAgInSc1d1	spojován
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
či	či	k8xC	či
s	s	k7c7	s
kostrou	kostra	k1gFnSc7	kostra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
kroucená	kroucený	k2eAgFnSc1d1	kroucená
dvojlinka	dvojlinka	k1gFnSc1	dvojlinka
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
symetrická	symetrický	k2eAgNnPc1d1	symetrické
vedení	vedení	k1gNnPc1	vedení
(	(	kIx(	(
<g/>
dvojice	dvojice	k1gFnSc1	dvojice
spirálově	spirálově	k6eAd1	spirálově
stočených	stočený	k2eAgInPc2d1	stočený
vodičů	vodič	k1gInPc2	vodič
v	v	k7c6	v
kabelu	kabel	k1gInSc6	kabel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc4	signál
přenášený	přenášený	k2eAgInSc4d1	přenášený
po	po	k7c6	po
kroucené	kroucený	k2eAgFnSc6d1	kroucená
dvojlince	dvojlinka	k1gFnSc6	dvojlinka
je	být	k5eAaImIp3nS	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
rozdílem	rozdíl	k1gInSc7	rozdíl
potenciálů	potenciál	k1gInPc2	potenciál
obou	dva	k4xCgInPc2	dva
vodičů	vodič	k1gInPc2	vodič
<g/>
.	.	kIx.	.
</s>
<s>
Koaxiální	koaxiální	k2eAgInSc1d1	koaxiální
kabel	kabel	k1gInSc1	kabel
je	být	k5eAaImIp3nS	být
asymetrický	asymetrický	k2eAgInSc4d1	asymetrický
elektrický	elektrický	k2eAgInSc4d1	elektrický
kabel	kabel	k1gInSc4	kabel
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
válcovým	válcový	k2eAgInSc7d1	válcový
vnějším	vnější	k2eAgInSc7d1	vnější
vodičem	vodič	k1gInSc7	vodič
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
drátovým	drátový	k2eAgInSc7d1	drátový
nebo	nebo	k8xC	nebo
trubkovým	trubkový	k2eAgInSc7d1	trubkový
vodičem	vodič	k1gInSc7	vodič
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInSc4d1	vnější
vodič	vodič	k1gInSc4	vodič
nazýváme	nazývat	k5eAaImIp1nP	nazývat
často	často	k6eAd1	často
stíněním	stínění	k1gNnSc7	stínění
a	a	k8xC	a
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
vodič	vodič	k1gInSc4	vodič
jádrem	jádro	k1gNnSc7	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInSc4d1	vnější
a	a	k8xC	a
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
vodič	vodič	k1gInSc4	vodič
jsou	být	k5eAaImIp3nP	být
odděleny	oddělen	k2eAgInPc1d1	oddělen
nevodivou	vodivý	k2eNgFnSc7d1	nevodivá
vrstvou	vrstva	k1gFnSc7	vrstva
(	(	kIx(	(
<g/>
dielektrikum	dielektrikum	k1gNnSc1	dielektrikum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
a	a	k8xC	a
vnějšího	vnější	k2eAgInSc2d1	vnější
vodiče	vodič	k1gInSc2	vodič
lze	lze	k6eAd1	lze
přenášet	přenášet	k5eAaImF	přenášet
stejnosměrný	stejnosměrný	k2eAgInSc4d1	stejnosměrný
proud	proud	k1gInSc4	proud
(	(	kIx(	(
<g/>
napájení	napájení	k1gNnSc2	napájení
anténních	anténní	k2eAgInPc2d1	anténní
předzesilovačů	předzesilovač	k1gInPc2	předzesilovač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odrušit	odrušit	k5eAaPmF	odrušit
(	(	kIx(	(
<g/>
stínit	stínit	k5eAaImF	stínit
<g/>
)	)	kIx)	)
nízkofrekvenční	nízkofrekvenční	k2eAgInPc1d1	nízkofrekvenční
signály	signál	k1gInPc1	signál
(	(	kIx(	(
<g/>
kabely	kabel	k1gInPc1	kabel
k	k	k7c3	k
mikrofonům	mikrofon	k1gInPc3	mikrofon
a	a	k8xC	a
sluchátkům	sluchátko	k1gNnPc3	sluchátko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nejčastější	častý	k2eAgFnSc7d3	nejčastější
funkcí	funkce	k1gFnSc7	funkce
koaxiálního	koaxiální	k2eAgInSc2d1	koaxiální
kabelu	kabel	k1gInSc2	kabel
je	být	k5eAaImIp3nS	být
přenos	přenos	k1gInSc1	přenos
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
vlnění	vlnění	k1gNnSc2	vlnění
o	o	k7c6	o
vysokém	vysoký	k2eAgInSc6d1	vysoký
kmitočtu	kmitočet	k1gInSc6	kmitočet
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
do	do	k7c2	do
50	[number]	k4	50
gigahertzů	gigahertz	k1gInPc2	gigahertz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
koaxiálním	koaxiální	k2eAgInSc7d1	koaxiální
kabelem	kabel	k1gInSc7	kabel
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
<g/>
.	.	kIx.	.
</s>
<s>
ITU-T	ITU-T	k?	ITU-T
G.	G.	kA	G.
<g/>
hn	hn	k?	hn
technologie	technologie	k1gFnSc1	technologie
využívá	využívat	k5eAaImIp3nS	využívat
existující	existující	k2eAgFnSc2d1	existující
domácí	domácí	k2eAgFnSc2d1	domácí
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
síťová	síťový	k2eAgFnSc1d1	síťová
kabeláž	kabeláž	k1gFnSc1	kabeláž
<g/>
,	,	kIx,	,
koaxiální	koaxiální	k2eAgFnSc1d1	koaxiální
kabeláž	kabeláž	k1gFnSc1	kabeláž
<g/>
,	,	kIx,	,
ADSL	ADSL	kA	ADSL
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
provoz	provoz	k1gInSc4	provoz
sítě	síť	k1gFnSc2	síť
přes	přes	k7c4	přes
elektrické	elektrický	k2eAgFnPc4d1	elektrická
přípojky	přípojka	k1gFnPc4	přípojka
<g/>
,	,	kIx,	,
telefonní	telefonní	k2eAgFnPc4d1	telefonní
linky	linka	k1gFnPc4	linka
a	a	k8xC	a
koaxiální	koaxiální	k2eAgInPc4d1	koaxiální
kabely	kabel	k1gInPc4	kabel
s	s	k7c7	s
datovým	datový	k2eAgInSc7d1	datový
tokem	tok	k1gInSc7	tok
až	až	k6eAd1	až
do	do	k7c2	do
1	[number]	k4	1
gigabitu	gigabit	k1gInSc2	gigabit
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
v	v	k7c6	v
lokální	lokální	k2eAgFnSc6d1	lokální
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Optické	optický	k2eAgNnSc1d1	optické
vlákno	vlákno	k1gNnSc1	vlákno
je	být	k5eAaImIp3nS	být
skleněné	skleněný	k2eAgNnSc1d1	skleněné
nebo	nebo	k8xC	nebo
plastové	plastový	k2eAgNnSc1d1	plastové
vlákno	vlákno	k1gNnSc1	vlákno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
světla	světlo	k1gNnSc2	světlo
přenáší	přenášet	k5eAaImIp3nS	přenášet
signály	signál	k1gInPc4	signál
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
své	svůj	k3xOyFgFnSc2	svůj
podélné	podélný	k2eAgFnSc2d1	podélná
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Optická	optický	k2eAgNnPc1d1	optické
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
široce	široko	k6eAd1	široko
využívána	využívat	k5eAaPmNgFnS	využívat
v	v	k7c6	v
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přenos	přenos	k1gInSc4	přenos
na	na	k7c6	na
delší	dlouhý	k2eAgFnSc6d2	delší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
a	a	k8xC	a
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
přenosových	přenosový	k2eAgFnPc6d1	přenosová
rychlostech	rychlost	k1gFnPc6	rychlost
dat	datum	k1gNnPc2	datum
než	než	k8xS	než
jiné	jiný	k2eAgFnSc2d1	jiná
formy	forma	k1gFnSc2	forma
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
místo	místo	k7c2	místo
kovových	kovový	k2eAgInPc2d1	kovový
vodičů	vodič	k1gInPc2	vodič
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
signály	signál	k1gInPc1	signál
jsou	být	k5eAaImIp3nP	být
přenášeny	přenášet	k5eAaImNgInP	přenášet
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
ztrátou	ztráta	k1gFnSc7	ztráta
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
vlákna	vlákno	k1gNnPc1	vlákno
imunní	imunní	k2eAgNnPc1d1	imunní
vůči	vůči	k7c3	vůči
elektromagnetickému	elektromagnetický	k2eAgNnSc3d1	elektromagnetické
rušení	rušení	k1gNnSc3	rušení
<g/>
.	.	kIx.	.
síťový	síťový	k2eAgInSc4d1	síťový
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
Novell	Novell	kA	Novell
NetWare	NetWar	k1gMnSc5	NetWar
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
aplikace	aplikace	k1gFnPc1	aplikace
schopné	schopný	k2eAgFnPc1d1	schopná
využívat	využívat	k5eAaImF	využívat
prostředky	prostředek	k1gInPc4	prostředek
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
určené	určený	k2eAgFnSc2d1	určená
k	k	k7c3	k
síťové	síťový	k2eAgFnSc3d1	síťová
komunikaci	komunikace	k1gFnSc3	komunikace
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Topologie	topologie	k1gFnSc2	topologie
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Topologie	topologie	k1gFnSc1	topologie
sítí	síť	k1gFnPc2	síť
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
zapojením	zapojení	k1gNnSc7	zapojení
různých	různý	k2eAgInPc2d1	různý
prvků	prvek	k1gInPc2	prvek
do	do	k7c2	do
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
zachycením	zachycení	k1gNnSc7	zachycení
jejich	jejich	k3xOp3gFnSc2	jejich
skutečné	skutečný	k2eAgFnSc2d1	skutečná
(	(	kIx(	(
<g/>
reálné	reálný	k2eAgFnSc2d1	reálná
<g/>
)	)	kIx)	)
a	a	k8xC	a
logické	logický	k2eAgFnPc1d1	logická
(	(	kIx(	(
<g/>
virtuální	virtuální	k2eAgFnPc1d1	virtuální
<g/>
)	)	kIx)	)
podoby	podoba	k1gFnPc1	podoba
(	(	kIx(	(
<g/>
datové	datový	k2eAgFnPc1d1	datová
linky	linka	k1gFnPc1	linka
<g/>
,	,	kIx,	,
síťové	síťový	k2eAgInPc1d1	síťový
uzly	uzel	k1gInPc1	uzel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Topologii	topologie	k1gFnSc4	topologie
lze	lze	k6eAd1	lze
zvažovat	zvažovat	k5eAaImF	zvažovat
jako	jako	k9	jako
určitý	určitý	k2eAgInSc4d1	určitý
tvar	tvar	k1gInSc4	tvar
či	či	k8xC	či
strukturu	struktura	k1gFnSc4	struktura
dané	daný	k2eAgFnSc2d1	daná
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tvar	tvar	k1gInSc1	tvar
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
korespondovat	korespondovat	k5eAaImF	korespondovat
se	s	k7c7	s
skutečným	skutečný	k2eAgNnSc7d1	skutečné
fyzickým	fyzický	k2eAgNnSc7d1	fyzické
rozvržením	rozvržení	k1gNnSc7	rozvržení
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
zapojených	zapojený	k2eAgInPc2d1	zapojený
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
počítače	počítač	k1gInPc1	počítač
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
domácí	domácí	k2eAgFnSc6d1	domácí
síti	síť	k1gFnSc6	síť
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
v	v	k7c6	v
pomyslném	pomyslný	k2eAgInSc6d1	pomyslný
kruhovém	kruhový	k2eAgInSc6d1	kruhový
tvaru	tvar	k1gInSc6	tvar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nS	muset
to	ten	k3xDgNnSc4	ten
nutně	nutně	k6eAd1	nutně
znamenat	znamenat	k5eAaImF	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
logické	logický	k2eAgNnSc1d1	logické
zapojení	zapojení	k1gNnSc1	zapojení
představuje	představovat	k5eAaImIp3nS	představovat
příklad	příklad	k1gInSc4	příklad
kruhové	kruhový	k2eAgFnSc2d1	kruhová
topologie	topologie	k1gFnSc2	topologie
<g/>
.	.	kIx.	.
</s>
<s>
Sběrnicová	sběrnicový	k2eAgFnSc1d1	sběrnicová
topologie	topologie	k1gFnSc1	topologie
(	(	kIx(	(
<g/>
bus	bus	k1gInSc1	bus
<g/>
)	)	kIx)	)
-	-	kIx~	-
kabel	kabel	k1gInSc1	kabel
prochází	procházet	k5eAaImIp3nS	procházet
okolo	okolo	k7c2	okolo
všech	všecek	k3xTgMnPc2	všecek
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
nerozvětvuje	rozvětvovat	k5eNaImIp3nS	rozvětvovat
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
Ethernet	Ethernet	k1gMnSc1	Ethernet
s	s	k7c7	s
koaxiálním	koaxiální	k2eAgInSc7d1	koaxiální
kabelem	kabel	k1gInSc7	kabel
<g/>
)	)	kIx)	)
Hvězdicová	hvězdicový	k2eAgFnSc1d1	hvězdicová
topologie	topologie	k1gFnSc1	topologie
(	(	kIx(	(
<g/>
star	star	k1gFnSc1	star
<g/>
)	)	kIx)	)
-	-	kIx~	-
všechny	všechen	k3xTgInPc1	všechen
počítače	počítač	k1gInPc1	počítač
připojeny	připojit	k5eAaPmNgFnP	připojit
k	k	k7c3	k
aktivnímu	aktivní	k2eAgInSc3d1	aktivní
prvku	prvek	k1gInSc3	prvek
(	(	kIx(	(
<g/>
Ethernet	Ethernet	k1gMnSc1	Ethernet
s	s	k7c7	s
kroucenou	kroucený	k2eAgFnSc7d1	kroucená
dvojlinkou	dvojlinka	k1gFnSc7	dvojlinka
<g/>
)	)	kIx)	)
Kruhová	kruhový	k2eAgFnSc1d1	kruhová
topologie	topologie	k1gFnSc1	topologie
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
ring	ring	k1gInSc1	ring
<g/>
)	)	kIx)	)
-	-	kIx~	-
spojení	spojení	k1gNnSc1	spojení
je	být	k5eAaImIp3nS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
a	a	k8xC	a
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
propojením	propojení	k1gNnSc7	propojení
obou	dva	k4xCgInPc2	dva
konců	konec	k1gInPc2	konec
sběrnice	sběrnice	k1gFnSc2	sběrnice
(	(	kIx(	(
<g/>
FDDI	FDDI	kA	FDDI
<g/>
)	)	kIx)	)
Stromová	stromový	k2eAgFnSc1d1	stromová
topologie	topologie	k1gFnSc1	topologie
(	(	kIx(	(
<g/>
tree	tree	k1gFnSc1	tree
<g/>
)	)	kIx)	)
-	-	kIx~	-
propojení	propojení	k1gNnSc1	propojení
více	hodně	k6eAd2	hodně
hvězdicových	hvězdicový	k2eAgFnPc2d1	hvězdicová
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
v	v	k7c6	v
LAN	lano	k1gNnPc2	lano
<g/>
)	)	kIx)	)
Obecný	obecný	k2eAgInSc1d1	obecný
graf	graf	k1gInSc1	graf
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
redundantní	redundantní	k2eAgInPc4d1	redundantní
spoje	spoj	k1gInPc4	spoj
(	(	kIx(	(
<g/>
WAN	WAN	kA	WAN
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
Samostatný	samostatný	k2eAgInSc1d1	samostatný
počítač	počítač	k1gInSc1	počítač
(	(	kIx(	(
<g/>
virtuální	virtuální	k2eAgFnSc1d1	virtuální
síť	síť	k1gFnSc1	síť
<g/>
)	)	kIx)	)
</s>
