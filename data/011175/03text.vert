<p>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Olegovič	Olegovič	k1gMnSc1	Olegovič
Pelevin	Pelevina	k1gFnPc2	Pelevina
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
В	В	k?	В
О	О	k?	О
П	П	k?	П
<g/>
,	,	kIx,	,
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
současný	současný	k2eAgMnSc1d1	současný
ruský	ruský	k2eAgMnSc1d1	ruský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
nejvydávanější	vydávaný	k2eAgMnSc1d3	nejvydávanější
a	a	k8xC	a
nejpřekládanější	překládaný	k2eAgMnSc1d3	nejpřekládanější
ruský	ruský	k2eAgMnSc1d1	ruský
autor	autor	k1gMnSc1	autor
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Pelevin	Pelevina	k1gFnPc2	Pelevina
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
postmoderních	postmoderní	k2eAgInPc6d1	postmoderní
dílech	díl	k1gInPc6	díl
propojuje	propojovat	k5eAaImIp3nS	propojovat
narážky	narážka	k1gFnPc4	narážka
na	na	k7c4	na
život	život	k1gInSc4	život
v	v	k7c6	v
postsovětském	postsovětský	k2eAgNnSc6d1	postsovětské
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnSc4d1	populární
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
prvky	prvek	k1gInPc4	prvek
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
a	a	k8xC	a
odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
východní	východní	k2eAgInPc4d1	východní
náboženské	náboženský	k2eAgInPc4d1	náboženský
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
je	být	k5eAaImIp3nS	být
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybraná	vybraný	k2eAgNnPc1d1	vybrané
díla	dílo	k1gNnPc1	dílo
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Viktor	Viktor	k1gMnSc1	Viktor
Pelevin	Pelevina	k1gFnPc2	Pelevina
</s>
</p>
<p>
<s>
Čapajev	Čapajet	k5eAaPmDgInS	Čapajet
a	a	k8xC	a
prázdnota	prázdnota	k1gFnSc1	prázdnota
<g/>
,	,	kIx,	,
Humanitarian	Humanitarian	k1gInSc1	Humanitarian
Technologies	Technologies	k1gInSc1	Technologies
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86398-11-0	[number]	k4	80-86398-11-0
</s>
</p>
<p>
<s>
Generation	Generation	k1gInSc1	Generation
P	P	kA	P
<g/>
,	,	kIx,	,
Humanitarian	Humanitarian	k1gInSc1	Humanitarian
Technologies	Technologies	k1gInSc1	Technologies
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86398-18-8	[number]	k4	80-86398-18-8
</s>
</p>
<p>
<s>
Omon	Omon	k1gMnSc1	Omon
Ra	ra	k0	ra
<g/>
,	,	kIx,	,
Dokořán	dokořán	k6eAd1	dokořán
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
ISBN	ISBN	kA	ISBN
80-86569-28-4	[number]	k4	80-86569-28-4
</s>
</p>
<p>
<s>
Helma	helma	k1gFnSc1	helma
hrůzy	hrůza	k1gFnSc2	hrůza
<g/>
,	,	kIx,	,
Argo	Argo	k6eAd1	Argo
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7203-761-7	[number]	k4	80-7203-761-7
</s>
</p>
<p>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
kniha	kniha	k1gFnSc1	kniha
vlkodlaka	vlkodlak	k1gMnSc2	vlkodlak
<g/>
,	,	kIx,	,
Plus	plus	k1gInSc1	plus
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-259-0026-0	[number]	k4	978-80-259-0026-0
</s>
</p>
<p>
<s>
Snuff	Snuff	k1gInSc1	Snuff
<g/>
:	:	kIx,	:
Utopie	utopie	k1gFnSc1	utopie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Dokořán	dokořán	k6eAd1	dokořán
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
412	[number]	k4	412
S.	S.	kA	S.
Překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Libor	Libor	k1gMnSc1	Libor
Dvořák	Dvořák	k1gMnSc1	Dvořák
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
