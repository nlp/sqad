<s>
Pieter	Pieter	k1gMnSc1	Pieter
Zeeman	Zeeman	k1gMnSc1	Zeeman
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
Zonnemaire	Zonnemair	k1gMnSc5	Zonnemair
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
holandský	holandský	k2eAgMnSc1d1	holandský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
obdržel	obdržet	k5eAaPmAgInS	obdržet
společně	společně	k6eAd1	společně
s	s	k7c7	s
Hendrikem	Hendrik	k1gMnSc7	Hendrik
Lorentzem	Lorentz	k1gMnSc7	Lorentz
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
vlivu	vliv	k1gInSc2	vliv
magnetismu	magnetismus	k1gInSc2	magnetismus
na	na	k7c6	na
záření	záření	k1gNnSc6	záření
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
astronomii	astronomie	k1gFnSc4	astronomie
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc1d3	veliký
význam	význam	k1gInSc1	význam
jeho	jeho	k3xOp3gInSc4	jeho
objev	objev	k1gInSc4	objev
rozdvojení	rozdvojení	k1gNnSc4	rozdvojení
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Zeemanův	Zeemanův	k2eAgInSc1d1	Zeemanův
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
Pieter	Pieter	k1gMnSc1	Pieter
Zeeman	Zeeman	k1gMnSc1	Zeeman
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Zonnemaire	Zonnemair	k1gInSc5	Zonnemair
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
luteránského	luteránský	k2eAgMnSc2d1	luteránský
kněze	kněz	k1gMnSc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Zonnemaire	Zonnemair	k1gMnSc5	Zonnemair
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc4d1	malé
město	město	k1gNnSc4	město
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Schouwen-Duiveland	Schouwen-Duivelanda	k1gFnPc2	Schouwen-Duivelanda
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
rodiči	rodič	k1gMnPc7	rodič
byli	být	k5eAaImAgMnP	být
Catharinus	Catharinus	k1gMnSc1	Catharinus
Forandinus	Forandinus	k1gMnSc1	Forandinus
Zeeman	Zeeman	k1gMnSc1	Zeeman
a	a	k8xC	a
Willemina	Willemin	k2eAgFnSc1d1	Willemin
Worst	Worst	k1gFnSc1	Worst
<g/>
.	.	kIx.	.
</s>
<s>
Zeeman	Zeeman	k1gMnSc1	Zeeman
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
nakreslil	nakreslit	k5eAaPmAgInS	nakreslit
a	a	k8xC	a
popsal	popsat	k5eAaPmAgInS	popsat
polární	polární	k2eAgFnSc4d1	polární
záři	záře	k1gFnSc4	záře
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
nad	nad	k7c7	nad
Nizozemím	Nizozemí	k1gNnSc7	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
studoval	studovat	k5eAaImAgMnS	studovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Zierikzee	Zierikzee	k1gFnPc6	Zierikzee
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
popis	popis	k1gInSc4	popis
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
redakce	redakce	k1gFnSc2	redakce
Nature	Natur	k1gMnSc5	Natur
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Delftu	Delft	k1gInSc2	Delft
kvůli	kvůli	k7c3	kvůli
dalšímu	další	k2eAgNnSc3d1	další
vzdělání	vzdělání	k1gNnSc3	vzdělání
v	v	k7c6	v
klasických	klasický	k2eAgInPc6d1	klasický
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
podmínka	podmínka	k1gFnSc1	podmínka
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
J.W.	J.W.	k1gMnSc2	J.W.
Lely	Lela	k1gMnSc2	Lela
spoluředitele	spoluředitel	k1gMnSc2	spoluředitel
gymnázia	gymnázium	k1gNnSc2	gymnázium
a	a	k8xC	a
bratra	bratr	k1gMnSc4	bratr
Cornelia	Cornelius	k1gMnSc4	Cornelius
Lelyho	Lely	k1gMnSc4	Lely
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
koncepci	koncepce	k1gFnSc4	koncepce
a	a	k8xC	a
realizaci	realizace	k1gFnSc4	realizace
Zuiderzee	Zuiderze	k1gFnSc2	Zuiderze
Works	Works	kA	Works
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Heike	Heike	k1gFnSc7	Heike
Kamerlingh	Kamerlingh	k1gInSc4	Kamerlingh
Onnesem	Onnes	k1gInSc7	Onnes
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnPc3	jeho
vedoucím	vedoucí	k1gMnPc3	vedoucí
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
P.	P.	kA	P.
Zeeman	Zeeman	k1gMnSc1	Zeeman
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
učiteli	učitel	k1gMnPc7	učitel
byli	být	k5eAaImAgMnP	být
Lorentz	Lorentz	k1gMnSc1	Lorentz
a	a	k8xC	a
H.	H.	kA	H.
Kamerlingh	Kamerlingh	k1gInSc1	Kamerlingh
-	-	kIx~	-
Onnes	Onnes	k1gInSc1	Onnes
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
působil	působit	k5eAaImAgMnS	působit
Zeeman	Zeeman	k1gMnSc1	Zeeman
již	již	k6eAd1	již
jako	jako	k9	jako
Lorentzův	Lorentzův	k2eAgMnSc1d1	Lorentzův
asistent	asistent	k1gMnSc1	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Doktorát	doktorát	k1gInSc1	doktorát
získává	získávat	k5eAaImIp3nS	získávat
Zeeman	Zeeman	k1gMnSc1	Zeeman
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
Kerrovu	Kerrův	k2eAgInSc3d1	Kerrův
jevu	jev	k1gInSc3	jev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
polarizace	polarizace	k1gFnSc1	polarizace
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
zmagnetovaném	zmagnetovaný	k2eAgInSc6d1	zmagnetovaný
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ziskání	ziskání	k1gNnSc6	ziskání
doktorátu	doktorát	k1gInSc2	doktorát
pracoval	pracovat	k5eAaImAgMnS	pracovat
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
v	v	k7c6	v
F.	F.	kA	F.
Kohlrausch	Kohlrausch	k1gInSc4	Kohlrausch
institutu	institut	k1gInSc2	institut
ve	v	k7c6	v
Strasbourgu	Strasbourg	k1gInSc6	Strasbourg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
Strasbourgu	Strasbourg	k1gInSc2	Strasbourg
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Zeeman	Zeeman	k1gMnSc1	Zeeman
stal	stát	k5eAaPmAgMnS	stát
soukromým	soukromý	k2eAgMnSc7d1	soukromý
docentem	docent	k1gMnSc7	docent
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Johannou	Johanný	k2eAgFnSc7d1	Johanný
Elizabeth	Elizabeth	k1gFnSc7	Elizabeth
Lebretovou	Lebretový	k2eAgFnSc7d1	Lebretový
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
objevil	objevit	k5eAaPmAgInS	objevit
štěpení	štěpení	k1gNnSc4	štěpení
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
zdroje	zdroj	k1gInSc2	zdroj
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
nazván	nazván	k2eAgInSc1d1	nazván
Zeemanovým	Zeemanův	k2eAgInSc7d1	Zeemanův
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
objevu	objev	k1gInSc3	objev
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Zeemanovi	Zeemanův	k2eAgMnPc1d1	Zeemanův
nabídnuta	nabídnut	k2eAgFnSc1d1	nabídnuta
pozice	pozice	k1gFnSc1	pozice
docenta	docent	k1gMnSc2	docent
v	v	k7c6	v
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
následovalo	následovat	k5eAaImAgNnS	následovat
jeho	jeho	k3xOp3gNnSc1	jeho
jmenování	jmenování	k1gNnSc1	jmenování
profesorem	profesor	k1gMnSc7	profesor
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
v	v	k7c6	v
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
objev	objev	k1gInSc4	objev
Zeemanova	Zeemanův	k2eAgInSc2d1	Zeemanův
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
obdržel	obdržet	k5eAaPmAgMnS	obdržet
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bývalým	bývalý	k2eAgMnSc7d1	bývalý
učitelem	učitel	k1gMnSc7	učitel
Lorentzem	Lorentz	k1gMnSc7	Lorentz
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Waalsovým	Waalsův	k2eAgMnSc7d1	Waalsův
nástupcem	nástupce	k1gMnSc7	nástupce
jako	jako	k8xC	jako
řádný	řádný	k2eAgMnSc1d1	řádný
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
institutu	institut	k1gInSc2	institut
v	v	k7c6	v
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
zařízena	zařídit	k5eAaPmNgFnS	zařídit
nová	nový	k2eAgFnSc1d1	nová
laboratoř	laboratoř	k1gFnSc1	laboratoř
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Zeemanovu	Zeemanův	k2eAgFnSc4d1	Zeemanův
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nové	nový	k2eAgNnSc1d1	nové
zařízení	zařízení	k1gNnSc1	zařízení
dovolilo	dovolit	k5eAaPmAgNnS	dovolit
Zeemanu	Zeeman	k1gMnSc3	Zeeman
zabývat	zabývat	k5eAaImF	zabývat
se	se	k3xPyFc4	se
čistě	čistě	k6eAd1	čistě
výzkumem	výzkum	k1gInSc7	výzkum
Zeemanova	Zeemanův	k2eAgInSc2d1	Zeemanův
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
svého	svůj	k3xOyFgNnSc2	svůj
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
působení	působení	k1gNnSc2	působení
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
výzkumem	výzkum	k1gInSc7	výzkum
magneto-optických	magnetoptický	k2eAgInPc2d1	magneto-optický
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
byl	být	k5eAaImAgMnS	být
Zeeman	Zeeman	k1gMnSc1	Zeeman
zvolen	zvolit	k5eAaPmNgMnS	zvolit
členem	člen	k1gMnSc7	člen
Royal	Royal	k1gMnSc1	Royal
Netherlands	Netherlandsa	k1gFnPc2	Netherlandsa
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
and	and	k?	and
Sciences	Sciences	k1gInSc1	Sciences
v	v	k7c6	v
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
<g/>
,	,	kIx,	,
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
zde	zde	k6eAd1	zde
jako	jako	k8xS	jako
její	její	k3xOp3gMnSc1	její
tajemník	tajemník	k1gMnSc1	tajemník
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1912	[number]	k4	1912
až	až	k9	až
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
Henry	Henry	k1gMnSc1	Henry
Draper	Draper	k1gMnSc1	Draper
Medal	Medal	k1gMnSc1	Medal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
čestných	čestný	k2eAgInPc2d1	čestný
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
emeritním	emeritní	k2eAgMnSc7d1	emeritní
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Zeeman	Zeeman	k1gMnSc1	Zeeman
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
nemoci	nemoc	k1gFnSc6	nemoc
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1943	[number]	k4	1943
v	v	k7c6	v
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
Haarlemu	Haarlem	k1gInSc6	Haarlem
<g/>
.	.	kIx.	.
</s>
<s>
Zeeman	Zeeman	k1gMnSc1	Zeeman
získal	získat	k5eAaPmAgMnS	získat
čestné	čestný	k2eAgInPc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Göttingenu	Göttingen	k1gInSc6	Göttingen
<g/>
,	,	kIx,	,
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
,	,	kIx,	,
Liege	Liege	k1gFnSc6	Liege
<g/>
,	,	kIx,	,
Gwentu	Gwent	k1gInSc6	Gwent
<g/>
,	,	kIx,	,
Glasgow	Glasgow	k1gNnSc6	Glasgow
<g/>
,	,	kIx,	,
Bruselu	Brusel	k1gInSc6	Brusel
a	a	k8xC	a
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
členem	člen	k1gMnSc7	člen
nebo	nebo	k8xC	nebo
čestným	čestný	k2eAgMnSc7d1	čestný
členem	člen	k1gInSc7	člen
mnoha	mnoho	k4c2	mnoho
akademií	akademie	k1gFnPc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Získaná	získaný	k2eAgNnPc1d1	získané
ocenění	ocenění	k1gNnPc1	ocenění
<g/>
:	:	kIx,	:
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
Matteucci	Matteucce	k1gFnSc6	Matteucce
Medal	Medal	k1gInSc1	Medal
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
Henry	Henry	k1gMnSc1	Henry
Draper	Draper	k1gMnSc1	Draper
Medal	Medal	k1gMnSc1	Medal
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Rumford	Rumford	k1gMnSc1	Rumford
Medal	Medal	k1gMnSc1	Medal
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Po	po	k7c6	po
Zeemanovi	Zeeman	k1gMnSc6	Zeeman
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
kráter	kráter	k1gInSc1	kráter
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nedaleko	nedaleko	k7c2	nedaleko
měsíčního	měsíční	k2eAgInSc2d1	měsíční
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
a	a	k8xC	a
planetka	planetka	k1gFnSc1	planetka
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
29212	[number]	k4	29212
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
rozšíření	rozšíření	k1gNnSc1	rozšíření
své	svůj	k3xOyFgFnSc2	svůj
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
začal	začít	k5eAaPmAgMnS	začít
Zeeman	Zeeman	k1gMnSc1	Zeeman
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
účinek	účinek	k1gInSc4	účinek
magnetických	magnetický	k2eAgFnPc2d1	magnetická
polí	pole	k1gFnPc2	pole
na	na	k7c4	na
světelný	světelný	k2eAgInSc4d1	světelný
zdroj	zdroj	k1gInSc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
objevil	objevit	k5eAaPmAgInS	objevit
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
po	po	k7c6	po
zapnutí	zapnutí	k1gNnSc6	zapnutí
vnějšího	vnější	k2eAgNnSc2d1	vnější
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostré	ostrý	k2eAgFnPc1d1	ostrá
spektrální	spektrální	k2eAgFnPc1d1	spektrální
čáry	čára	k1gFnPc1	čára
se	se	k3xPyFc4	se
štěpily	štěpit	k5eAaImAgFnP	štěpit
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
spektrální	spektrální	k2eAgFnPc4d1	spektrální
čáry	čára	k1gFnPc4	čára
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nazývá	nazývat	k5eAaImIp3nS	nazývat
Zeemanův	Zeemanův	k2eAgInSc1d1	Zeemanův
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
Zeeman	Zeeman	k1gMnSc1	Zeeman
ho	on	k3xPp3gMnSc4	on
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
jako	jako	k9	jako
interakci	interakce	k1gFnSc4	interakce
mezi	mezi	k7c7	mezi
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
a	a	k8xC	a
magnetickým	magnetický	k2eAgInSc7d1	magnetický
dipólovým	dipólův	k2eAgInSc7d1	dipólův
momentem	moment	k1gInSc7	moment
<g/>
.	.	kIx.	.
</s>
<s>
Zeemanův	Zeemanův	k2eAgInSc1d1	Zeemanův
jev	jev	k1gInSc1	jev
byl	být	k5eAaImAgInS	být
zpracován	zpracovat	k5eAaPmNgInS	zpracovat
teoreticky	teoreticky	k6eAd1	teoreticky
jeho	jeho	k3xOp3gMnSc7	jeho
učitelem	učitel	k1gMnSc7	učitel
Lorentzem	Lorentz	k1gMnSc7	Lorentz
<g/>
.	.	kIx.	.
</s>
<s>
Zeemanův	Zeemanův	k2eAgInSc1d1	Zeemanův
objev	objev	k1gInSc1	objev
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
Lorentzovu	Lorentzův	k2eAgFnSc4d1	Lorentzova
předpověď	předpověď	k1gFnSc4	předpověď
o	o	k7c6	o
polarizaci	polarizace	k1gFnSc6	polarizace
světla	světlo	k1gNnSc2	světlo
vznikajícího	vznikající	k2eAgNnSc2d1	vznikající
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Vyplývalo	vyplývat	k5eAaImAgNnS	vyplývat
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
svým	svůj	k3xOyFgNnSc7	svůj
oscilováním	oscilování	k1gNnSc7	oscilování
podle	podle	k7c2	podle
Lorenze	Lorenz	k1gMnSc2	Lorenz
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tisíckrát	tisíckrát	k6eAd1	tisíckrát
lehčí	lehký	k2eAgInSc4d2	lehčí
než	než	k8xS	než
vodíkový	vodíkový	k2eAgInSc4d1	vodíkový
atom	atom	k1gInSc4	atom
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
záporný	záporný	k2eAgInSc4d1	záporný
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
závěru	závěr	k1gInSc3	závěr
došlo	dojít	k5eAaPmAgNnS	dojít
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
J.J.	J.J.	k1gMnSc1	J.J.
Thomson	Thomson	k1gMnSc1	Thomson
objevil	objevit	k5eAaPmAgMnS	objevit
elektron	elektron	k1gInSc4	elektron
(	(	kIx(	(
<g/>
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zeemanův	Zeemanův	k2eAgInSc1d1	Zeemanův
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
tak	tak	k8xS	tak
stal	stát	k5eAaPmAgInS	stát
důležitým	důležitý	k2eAgInSc7d1	důležitý
objevem	objev	k1gInSc7	objev
při	při	k7c6	při
objasňování	objasňování	k1gNnSc6	objasňování
struktury	struktura	k1gFnSc2	struktura
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
čtyřicetiletého	čtyřicetiletý	k2eAgNnSc2d1	čtyřicetileté
snažení	snažení	k1gNnSc2	snažení
mnoha	mnoho	k4c2	mnoho
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
korunováno	korunován	k2eAgNnSc1d1	korunováno
důkazem	důkaz	k1gInSc7	důkaz
existence	existence	k1gFnSc2	existence
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mimo	mimo	k7c4	mimo
jiné	jiný	k1gMnPc4	jiný
Zeeman	Zeeman	k1gMnSc1	Zeeman
experimentálně	experimentálně	k6eAd1	experimentálně
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
dvojitý	dvojitý	k2eAgInSc4d1	dvojitý
lom	lom	k1gInSc4	lom
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
elektrickém	elektrický	k2eAgNnSc6d1	elektrické
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
na	na	k7c6	na
torzním	torzní	k2eAgInSc6d1	torzní
kyvadlu	kyvadlo	k1gNnSc3	kyvadlo
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
vlastnost	vlastnost	k1gFnSc4	vlastnost
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
,	,	kIx,	,
že	že	k8xS	že
odchylky	odchylka	k1gFnPc4	odchylka
dráhy	dráha	k1gFnSc2	dráha
dané	daný	k2eAgFnSc2d1	daná
setrvačností	setrvačnost	k1gFnSc7	setrvačnost
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
všech	všecek	k3xTgNnPc2	všecek
těles	těleso	k1gNnPc2	těleso
stejné	stejná	k1gFnSc2	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
šířením	šíření	k1gNnSc7	šíření
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
pohybujícím	pohybující	k2eAgMnSc6d1	pohybující
se	se	k3xPyFc4	se
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
souviselo	souviset	k5eAaImAgNnS	souviset
se	s	k7c7	s
speciální	speciální	k2eAgFnSc7d1	speciální
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c6	o
hmotnostní	hmotnostní	k2eAgFnSc6d1	hmotnostní
spektrometrii	spektrometrie	k1gFnSc6	spektrometrie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Královské	královský	k2eAgFnSc2d1	královská
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
akademie	akademie	k1gFnSc2	akademie
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zeeman	Zeeman	k1gMnSc1	Zeeman
<g/>
,	,	kIx,	,
Pieter	Pieter	k1gInSc1	Pieter
(	(	kIx(	(
<g/>
February	Februara	k1gFnSc2	Februara
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Effect	Effect	k1gMnSc1	Effect
of	of	k?	of
Magnetisation	Magnetisation	k1gInSc1	Magnetisation
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Nature	Natur	k1gMnSc5	Natur
of	of	k?	of
Light	Light	k2eAgInSc4d1	Light
Emitted	Emitted	k1gInSc4	Emitted
by	by	kYmCp3nP	by
a	a	k8xC	a
Substance	substance	k1gFnSc1	substance
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nature	Natur	k1gMnSc5	Natur
55	[number]	k4	55
<g/>
:	:	kIx,	:
347	[number]	k4	347
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Lodge	Lodge	k1gFnSc1	Lodge
<g/>
,	,	kIx,	,
Oliver	Oliver	k1gInSc1	Oliver
(	(	kIx(	(
<g/>
February	Februara	k1gFnSc2	Februara
11	[number]	k4	11
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Influence	influence	k1gFnSc2	influence
of	of	k?	of
a	a	k8xC	a
Magnetic	Magnetice	k1gFnPc2	Magnetice
Field	Fielda	k1gFnPc2	Fielda
on	on	k3xPp3gMnSc1	on
Radiation	Radiation	k1gInSc4	Radiation
Frequency	Frequenc	k2eAgFnPc1d1	Frequenc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Proceedings	Proceedings	k1gInSc1	Proceedings
of	of	k?	of
the	the	k?	the
Royal	Royal	k1gMnSc1	Royal
Society	societa	k1gFnSc2	societa
of	of	k?	of
London	London	k1gMnSc1	London
60	[number]	k4	60
<g/>
:	:	kIx,	:
513	[number]	k4	513
<g/>
-	-	kIx~	-
<g/>
514	[number]	k4	514
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Larmor	Larmor	k1gInSc1	Larmor
<g/>
,	,	kIx,	,
J	J	kA	J
(	(	kIx(	(
<g/>
February	Februara	k1gFnSc2	Februara
11	[number]	k4	11
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Influence	influence	k1gFnSc2	influence
of	of	k?	of
a	a	k8xC	a
Magnetic	Magnetice	k1gFnPc2	Magnetice
Field	Fielda	k1gFnPc2	Fielda
on	on	k3xPp3gMnSc1	on
Radiation	Radiation	k1gInSc4	Radiation
Frequency	Frequenc	k2eAgFnPc1d1	Frequenc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Proceedings	Proceedings	k1gInSc1	Proceedings
of	of	k?	of
the	the	k?	the
Royal	Royal	k1gMnSc1	Royal
Society	societa	k1gFnSc2	societa
of	of	k?	of
London	London	k1gMnSc1	London
60	[number]	k4	60
<g/>
:	:	kIx,	:
514	[number]	k4	514
<g/>
-	-	kIx~	-
<g/>
515	[number]	k4	515
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Pieter	Pieter	k1gMnSc1	Pieter
Zeeman	Zeeman	k1gMnSc1	Zeeman
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Pieter	Pieter	k1gMnSc1	Pieter
Zeeman	Zeeman	k1gMnSc1	Zeeman
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pieter	Pietra	k1gFnPc2	Pietra
Zeeman	Zeeman	k1gMnSc1	Zeeman
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc2	galerie
Pieter	Pieter	k1gMnSc1	Pieter
Zeeman	Zeeman	k1gMnSc1	Zeeman
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
http://www.converter.cz/fyzici/zeeman.htm	[url]	k6eAd1	http://www.converter.cz/fyzici/zeeman.htm
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Biografie	biografie	k1gFnSc1	biografie
na	na	k7c6	na
nobelprize	nobelpriza	k1gFnSc6	nobelpriza
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
přednáška	přednáška	k1gFnSc1	přednáška
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
udělení	udělení	k1gNnSc2	udělení
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Albert	Albert	k1gMnSc1	Albert
van	vana	k1gFnPc2	vana
Helden	Heldna	k1gFnPc2	Heldna
Pieter	Pieter	k1gInSc1	Pieter
Zeeman	Zeeman	k1gMnSc1	Zeeman
1865	[number]	k4	1865
-	-	kIx~	-
1943	[number]	k4	1943
In	In	k1gFnPc2	In
<g/>
:	:	kIx,	:
K.	K.	kA	K.
van	van	k1gInSc1	van
Berkel	Berkel	k1gInSc1	Berkel
<g/>
,	,	kIx,	,
A.	A.	kA	A.
van	van	k1gInSc4	van
Helden	Heldna	k1gFnPc2	Heldna
and	and	k?	and
L.	L.	kA	L.
Palm	Palm	k1gInSc1	Palm
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
A	a	k9	a
History	Histor	k1gMnPc4	Histor
of	of	k?	of
Science	Science	k1gFnSc1	Science
in	in	k?	in
The	The	k1gFnSc2	The
Netherlands	Netherlandsa	k1gFnPc2	Netherlandsa
<g/>
.	.	kIx.	.
</s>
<s>
Survey	Survea	k1gFnPc1	Survea
<g/>
,	,	kIx,	,
Themes	Themes	k1gInSc1	Themes
and	and	k?	and
Reference	reference	k1gFnSc2	reference
(	(	kIx(	(
<g/>
Leiden	Leidna	k1gFnPc2	Leidna
<g/>
:	:	kIx,	:
Brill	Brill	k1gMnSc1	Brill
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
606	[number]	k4	606
-	-	kIx~	-
608	[number]	k4	608
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Museum	museum	k1gNnSc1	museum
Boerhaave	Boerhaav	k1gInSc5	Boerhaav
Negen	Negen	k1gInSc4	Negen
Nederlandse	Nederlandsa	k1gFnSc3	Nederlandsa
Nobelprijswinnaars	Nobelprijswinnaars	k1gInSc1	Nobelprijswinnaars
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
P.	P.	kA	P.
<g/>
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
Klinkenberg	Klinkenberg	k1gMnSc1	Klinkenberg
<g/>
,	,	kIx,	,
Zeeman	Zeeman	k1gMnSc1	Zeeman
<g/>
,	,	kIx,	,
Pieter	Pieter	k1gMnSc1	Pieter
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1865	[number]	k4	1865
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
in	in	k?	in
Biografisch	Biografisch	k1gInSc1	Biografisch
Woordenboek	Woordenboek	k1gMnSc1	Woordenboek
van	vana	k1gFnPc2	vana
Nederland	Nederlanda	k1gFnPc2	Nederlanda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Biography	Biographa	k1gFnPc1	Biographa
of	of	k?	of
Pieter	Pieter	k1gMnSc1	Pieter
Zeeman	Zeeman	k1gMnSc1	Zeeman
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
-	-	kIx~	-
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
at	at	k?	at
the	the	k?	the
National	National	k1gMnSc1	National
library	librara	k1gFnSc2	librara
of	of	k?	of
the	the	k?	the
Netherlands	Netherlands	k1gInSc1	Netherlands
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Anne	Anne	k1gInSc1	Anne
J.	J.	kA	J.
Kox	Kox	k1gMnSc2	Kox
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Wetenschappelijke	Wetenschappelijke	k1gInSc1	Wetenschappelijke
feiten	feiten	k2eAgInSc1d1	feiten
en	en	k?	en
postmoderne	postmodernout	k5eAaPmIp3nS	postmodernout
fictie	fictie	k1gFnSc1	fictie
in	in	k?	in
de	de	k?	de
wetenschapsgeschiedenis	wetenschapsgeschiedenis	k1gInSc1	wetenschapsgeschiedenis
<g/>
,	,	kIx,	,
Inaugural	Inaugural	k1gMnSc5	Inaugural
lecture	lectur	k1gMnSc5	lectur
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pim	Pim	k1gMnSc1	Pim
de	de	k?	de
Bie	Bie	k1gMnSc1	Bie
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
<g/>
dr	dr	kA	dr
<g/>
.	.	kIx.	.
P.	P.	kA	P.
Zeeman	Zeeman	k1gMnSc1	Zeeman
Zonnemaire	Zonnemair	k1gInSc5	Zonnemair
25	[number]	k4	25
mei	mei	k?	mei
1865	[number]	k4	1865
-	-	kIx~	-
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
9	[number]	k4	9
oktober	oktobra	k1gFnPc2	oktobra
1943	[number]	k4	1943
Gravesite	Gravesit	k1gInSc5	Gravesit
of	of	k?	of
Pieter	Pieter	k1gMnSc1	Pieter
Zeeman	Zeeman	k1gMnSc1	Zeeman
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pieter	Pieter	k1gMnSc1	Pieter
Zeeman	Zeeman	k1gMnSc1	Zeeman
<g/>
,	,	kIx,	,
Bijzondere	Bijzonder	k1gInSc5	Bijzonder
collecties	collectiesa	k1gFnPc2	collectiesa
Leiden	Leidna	k1gFnPc2	Leidna
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
photo	photo	k1gNnSc4	photo
&	&	k?	&
short	short	k1gInSc1	short
info	info	k6eAd1	info
</s>
