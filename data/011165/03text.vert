<p>
<s>
Dozimetrie	dozimetrie	k1gFnSc1	dozimetrie
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
fyziky	fyzika	k1gFnSc2	fyzika
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
veličinami	veličina	k1gFnPc7	veličina
charakterizujícími	charakterizující	k2eAgInPc7d1	charakterizující
procesy	proces	k1gInPc7	proces
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
interakce	interakce	k1gFnSc1	interakce
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
s	s	k7c7	s
látkou	látka	k1gFnSc7	látka
a	a	k8xC	a
metodami	metoda	k1gFnPc7	metoda
měření	měření	k1gNnSc2	měření
těchto	tento	k3xDgFnPc2	tento
veličin	veličina	k1gFnPc2	veličina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
biologických	biologický	k2eAgFnPc6d1	biologická
a	a	k8xC	a
lékařských	lékařský	k2eAgFnPc6d1	lékařská
vědách	věda	k1gFnPc6	věda
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
interakci	interakce	k1gFnSc4	interakce
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
s	s	k7c7	s
živou	živý	k2eAgFnSc7d1	živá
hmotou	hmota	k1gFnSc7	hmota
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
účinky	účinek	k1gInPc7	účinek
na	na	k7c4	na
organismy	organismus	k1gInPc4	organismus
a	a	k8xC	a
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
radiologie	radiologie	k1gFnSc1	radiologie
(	(	kIx(	(
<g/>
radiodiagnostika	radiodiagnostika	k1gFnSc1	radiodiagnostika
a	a	k8xC	a
radioterapie	radioterapie	k1gFnSc1	radioterapie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
technických	technický	k2eAgFnPc6d1	technická
vědách	věda	k1gFnPc6	věda
<g/>
,	,	kIx,	,
lékařství	lékařství	k1gNnSc6	lékařství
i	i	k8xC	i
vojenství	vojenství	k1gNnSc6	vojenství
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ochrany	ochrana	k1gFnSc2	ochrana
před	před	k7c7	před
zářením	záření	k1gNnSc7	záření
-	-	kIx~	-
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
monitorováním	monitorování	k1gNnSc7	monitorování
a	a	k8xC	a
usměrňováním	usměrňování	k1gNnSc7	usměrňování
rizik	riziko	k1gNnPc2	riziko
pro	pro	k7c4	pro
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
i	i	k8xC	i
pracovníky	pracovník	k1gMnPc4	pracovník
se	s	k7c7	s
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důležité	důležitý	k2eAgFnPc1d1	důležitá
veličiny	veličina	k1gFnPc1	veličina
a	a	k8xC	a
pojmy	pojem	k1gInPc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Expozice	expozice	k1gFnSc1	expozice
(	(	kIx(	(
<g/>
dávka	dávka	k1gFnSc1	dávka
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
získá	získat	k5eAaPmIp3nS	získat
1	[number]	k4	1
kg	kg	kA	kg
hmoty	hmota	k1gFnSc2	hmota
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Jednotkou	jednotka	k1gFnSc7	jednotka
expozice	expozice	k1gFnSc2	expozice
je	být	k5eAaImIp3nS	být
coulomb	coulomb	k1gInSc1	coulomb
na	na	k7c4	na
kilogram	kilogram	k1gInSc4	kilogram
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
·	·	k?	·
<g/>
kg-	kg-	k?	kg-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Expoziční	expoziční	k2eAgFnSc1d1	expoziční
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
expozicí	expozice	k1gFnSc7	expozice
vztaženou	vztažený	k2eAgFnSc7d1	vztažená
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
coulomb	coulomb	k1gInSc1	coulomb
na	na	k7c4	na
kilogram	kilogram	k1gInSc4	kilogram
a	a	k8xC	a
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
·	·	k?	·
<g/>
kg-	kg-	k?	kg-
<g/>
1	[number]	k4	1
<g/>
·	·	k?	·
<g/>
s-	s-	k?	s-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Absorbovaná	absorbovaný	k2eAgFnSc1d1	absorbovaná
dávka	dávka	k1gFnSc1	dávka
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc1	energie
dodaná	dodaný	k2eAgFnSc1d1	dodaná
jednomu	jeden	k4xCgNnSc3	jeden
kilogramu	kilogram	k1gInSc3	kilogram
hmoty	hmota	k1gFnSc2	hmota
průchodem	průchod	k1gInSc7	průchod
příslušného	příslušný	k2eAgNnSc2d1	příslušné
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Jednotkou	jednotka	k1gFnSc7	jednotka
absorbované	absorbovaný	k2eAgFnSc2d1	absorbovaná
dávky	dávka	k1gFnSc2	dávka
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
gray	graa	k1gFnPc4	graa
(	(	kIx(	(
<g/>
Gy	Gy	k1gMnSc1	Gy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozměrově	rozměrově	k6eAd1	rozměrově
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
joule	joule	k1gInSc4	joule
na	na	k7c4	na
kilogram	kilogram	k1gInSc4	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc7d2	starší
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
rad	rad	k1gInSc1	rad
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
100	[number]	k4	100
rad	rada	k1gFnPc2	rada
=	=	kIx~	=
1	[number]	k4	1
Gy	Gy	k1gFnPc2	Gy
<g/>
.	.	kIx.	.
<g/>
Biologický	biologický	k2eAgInSc1d1	biologický
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
(	(	kIx(	(
<g/>
Dávkový	dávkový	k2eAgInSc1d1	dávkový
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
biofyzikální	biofyzikální	k2eAgFnSc7d1	biofyzikální
veličinou	veličina	k1gFnSc7	veličina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vedle	vedle	k7c2	vedle
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
účinků	účinek	k1gInPc2	účinek
zohledňuje	zohledňovat	k5eAaImIp3nS	zohledňovat
i	i	k9	i
biologický	biologický	k2eAgInSc1d1	biologický
účinek	účinek	k1gInSc1	účinek
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Dávkový	dávkový	k2eAgInSc1d1	dávkový
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
se	se	k3xPyFc4	se
z	z	k7c2	z
dávky	dávka	k1gFnSc2	dávka
spočítá	spočítat	k5eAaPmIp3nS	spočítat
vynásobením	vynásobení	k1gNnSc7	vynásobení
údaje	údaj	k1gInSc2	údaj
faktorem	faktor	k1gInSc7	faktor
kvality	kvalita	k1gFnSc2	kvalita
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
se	se	k3xPyFc4	se
faktor	faktor	k1gInSc1	faktor
kvality	kvalita	k1gFnSc2	kvalita
u	u	k7c2	u
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
<g/>
,	,	kIx,	,
beta	beta	k1gNnSc1	beta
a	a	k8xC	a
gama	gama	k1gNnSc1	gama
záření	záření	k1gNnSc2	záření
rovná	rovnat	k5eAaImIp3nS	rovnat
jedné	jeden	k4xCgFnSc2	jeden
<g/>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
1	[number]	k4	1
Gy	Gy	k1gFnPc6	Gy
roven	roven	k2eAgInSc1d1	roven
1	[number]	k4	1
Sv.	sv.	kA	sv.
Starší	starý	k2eAgFnSc7d2	starší
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
rem	rem	k?	rem
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
100	[number]	k4	100
rem	rem	k?	rem
=	=	kIx~	=
1	[number]	k4	1
Sv.	sv.	kA	sv.
<g/>
Radiační	radiační	k2eAgFnSc1d1	radiační
ochrana	ochrana	k1gFnSc1	ochrana
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
minimalizovat	minimalizovat	k5eAaBmF	minimalizovat
rizika	riziko	k1gNnPc4	riziko
spojená	spojený	k2eAgNnPc4d1	spojené
s	s	k7c7	s
pobytem	pobyt	k1gInSc7	pobyt
člověka	člověk	k1gMnSc2	člověk
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
zasaženými	zasažený	k2eAgFnPc7d1	zasažená
ionizujícím	ionizující	k2eAgNnSc7d1	ionizující
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nalezení	nalezení	k1gNnSc1	nalezení
a	a	k8xC	a
popis	popis	k1gInSc1	popis
zdroje	zdroj	k1gInSc2	zdroj
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
určení	určení	k1gNnSc2	určení
případné	případný	k2eAgFnSc2d1	případná
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
újmy	újma	k1gFnSc2	újma
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
monitorovaném	monitorovaný	k2eAgInSc6d1	monitorovaný
prostoru	prostor	k1gInSc6	prostor
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
příslušným	příslušný	k2eAgInSc7d1	příslušný
zdrojem	zdroj	k1gInSc7	zdroj
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
<g/>
Úplný	úplný	k2eAgInSc1d1	úplný
přehled	přehled	k1gInSc1	přehled
mezinárodně	mezinárodně	k6eAd1	mezinárodně
doporučovaných	doporučovaný	k2eAgFnPc2d1	doporučovaná
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
definic	definice	k1gFnPc2	definice
a	a	k8xC	a
jednotek	jednotka	k1gFnPc2	jednotka
podává	podávat	k5eAaImIp3nS	podávat
ČSN	ČSN	kA	ČSN
ISO	ISO	kA	ISO
31	[number]	k4	31
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
její	její	k3xOp3gNnSc1	její
nové	nový	k2eAgNnSc1d1	nové
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
</s>
</p>
<p>
<s>
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
80000	[number]	k4	80000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Působení	působení	k1gNnSc6	působení
ionizujícím	ionizující	k2eAgNnSc6d1	ionizující
záření	záření	k1gNnSc4	záření
na	na	k7c4	na
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
==	==	k?	==
</s>
</p>
<p>
<s>
Ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jak	jak	k6eAd1	jak
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
slabého	slabý	k2eAgNnSc2d1	slabé
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
krátkodobého	krátkodobý	k2eAgNnSc2d1	krátkodobé
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
ozáření	ozáření	k1gNnSc2	ozáření
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
negativní	negativní	k2eAgInPc4d1	negativní
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc4d1	ostatní
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
<g/>
-li	i	k?	-li
na	na	k7c4	na
biologický	biologický	k2eAgInSc4d1	biologický
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
absorpci	absorpce	k1gFnSc3	absorpce
ionizujících	ionizující	k2eAgFnPc2d1	ionizující
částic	částice	k1gFnPc2	částice
nebo	nebo	k8xC	nebo
vlnění	vlnění	k1gNnSc1	vlnění
atomy	atom	k1gInPc1	atom
daného	daný	k2eAgInSc2d1	daný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vyrážení	vyrážení	k1gNnSc1	vyrážení
elektronů	elektron	k1gInPc2	elektron
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
orbit	orbita	k1gFnPc2	orbita
a	a	k8xC	a
tvorbu	tvorba	k1gFnSc4	tvorba
negativně	negativně	k6eAd1	negativně
nabitých	nabitý	k2eAgInPc2d1	nabitý
aniontů	anion	k1gInPc2	anion
<g/>
.	.	kIx.	.
</s>
<s>
Ionizované	ionizovaný	k2eAgFnPc1d1	ionizovaná
části	část	k1gFnPc1	část
molekul	molekula	k1gFnPc2	molekula
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
vysoce	vysoce	k6eAd1	vysoce
reaktivními	reaktivní	k2eAgInPc7d1	reaktivní
a	a	k8xC	a
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
buňku	buňka	k1gFnSc4	buňka
buď	buď	k8xC	buď
rovnou	rovnou	k6eAd1	rovnou
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
(	(	kIx(	(
<g/>
reakce	reakce	k1gFnPc1	reakce
radikálů	radikál	k1gMnPc2	radikál
s	s	k7c7	s
DNA	DNA	kA	DNA
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
porušení	porušení	k1gNnSc4	porušení
fosfodiesterových	fosfodiesterův	k2eAgFnPc2d1	fosfodiesterův
vazeb	vazba	k1gFnPc2	vazba
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zpřetrhání	zpřetrhání	k1gNnSc4	zpřetrhání
jejího	její	k3xOp3gInSc2	její
řetězce	řetězec	k1gInSc2	řetězec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dozimetr	dozimetr	k1gInSc1	dozimetr
</s>
</p>
<p>
<s>
Detektor	detektor	k1gInSc1	detektor
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
dozimetrie	dozimetrie	k1gFnSc1	dozimetrie
<g/>
?	?	kIx.	?
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
dozimetrie	dozimetrie	k1gFnSc2	dozimetrie
a	a	k8xC	a
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
jaderná	jaderný	k2eAgFnSc1d1	jaderná
a	a	k8xC	a
fyzikálně	fyzikálně	k6eAd1	fyzikálně
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
,	,	kIx,	,
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
Klinická	klinický	k2eAgFnSc1d1	klinická
dozimetrie	dozimetrie	k1gFnSc1	dozimetrie
Zdravotně	zdravotně	k6eAd1	zdravotně
sociální	sociální	k2eAgFnSc1d1	sociální
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
</s>
</p>
<p>
<s>
PASIVNÍ	pasivní	k2eAgFnSc1d1	pasivní
DOZIMETRIE	dozimetrie	k1gFnSc1	dozimetrie
chemicke-listy	chemickeista	k1gMnSc2	chemicke-lista
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
