<s>
Dozimetrie	dozimetrie	k1gFnSc1	dozimetrie
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
fyziky	fyzika	k1gFnSc2	fyzika
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
veličinami	veličina	k1gFnPc7	veličina
charakterizujícími	charakterizující	k2eAgInPc7d1	charakterizující
procesy	proces	k1gInPc7	proces
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
interakce	interakce	k1gFnSc1	interakce
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
s	s	k7c7	s
látkou	látka	k1gFnSc7	látka
a	a	k8xC	a
metodami	metoda	k1gFnPc7	metoda
měření	měření	k1gNnSc2	měření
těchto	tento	k3xDgFnPc2	tento
veličin	veličina	k1gFnPc2	veličina
<g/>
.	.	kIx.	.
</s>
