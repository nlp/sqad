<s>
Platónovi	Platónův	k2eAgMnPc1d1	Platónův
bratři	bratr	k1gMnPc1	bratr
Adeimantos	Adeimantos	k1gMnSc1	Adeimantos
a	a	k8xC	a
Glaukón	Glaukón	k1gMnSc1	Glaukón
také	také	k9	také
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
dialozích	dialog	k1gInPc6	dialog
a	a	k8xC	a
sestra	sestra	k1gFnSc1	sestra
Pótóné	Pótóná	k1gFnSc2	Pótóná
byla	být	k5eAaImAgFnS	být
matka	matka	k1gFnSc1	matka
filosofa	filosof	k1gMnSc2	filosof
Speusippa	Speusipp	k1gMnSc2	Speusipp
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
Platónově	Platónův	k2eAgFnSc6d1	Platónova
smrti	smrt	k1gFnSc6	smrt
vedl	vést	k5eAaImAgMnS	vést
Akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
