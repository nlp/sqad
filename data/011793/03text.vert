<p>
<s>
Grinderman	Grinderman	k1gMnSc1	Grinderman
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
Nick	Nick	k1gInSc4	Nick
Cave	Cav	k1gFnSc2	Cav
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Warren	Warrna	k1gFnPc2	Warrna
Ellis	Ellis	k1gFnPc2	Ellis
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc1	housle
<g/>
,	,	kIx,	,
viola	viola	k1gFnSc1	viola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Martyn	Martyn	k1gInSc1	Martyn
P.	P.	kA	P.
Casey	Casey	k1gInPc1	Casey
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
Sclavunos	Sclavunos	k1gInSc1	Sclavunos
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc1	perkuse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
zároveň	zároveň	k6eAd1	zároveň
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
Caveově	Caveův	k2eAgFnSc6d1	Caveova
domovské	domovský	k2eAgFnSc6d1	domovská
kapele	kapela	k1gFnSc6	kapela
The	The	k1gFnSc2	The
Bad	Bad	k1gFnSc2	Bad
Seeds	Seedsa	k1gFnPc2	Seedsa
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Grinderman	Grinderman	k1gMnSc1	Grinderman
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgInPc4	druhý
Grinderman	Grinderman	k1gMnSc1	Grinderman
2	[number]	k4	2
následovalo	následovat	k5eAaImAgNnS	následovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
původně	původně	k6eAd1	původně
<g/>
,	,	kIx,	,
když	když	k8xS	když
šla	jít	k5eAaImAgFnS	jít
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
nahrát	nahrát	k5eAaPmF	nahrát
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
neměla	mít	k5eNaImAgFnS	mít
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
zvažovali	zvažovat	k5eAaImAgMnP	zvažovat
různé	různý	k2eAgFnPc4d1	různá
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
zvolili	zvolit	k5eAaPmAgMnP	zvolit
název	název	k1gInSc4	název
Grinderman	Grinderman	k1gMnSc1	Grinderman
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Grinder	Grinder	k1gMnSc1	Grinder
Man	Man	k1gMnSc1	Man
<g/>
"	"	kIx"	"
od	od	k7c2	od
Johna	John	k1gMnSc2	John
Lee	Lea	k1gFnSc6	Lea
Hookera	Hookero	k1gNnSc2	Hookero
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zase	zase	k9	zase
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
dostala	dostat	k5eAaPmAgFnS	dostat
podle	podle	k7c2	podle
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Grinder	Grinder	k1gInSc1	Grinder
Man	mana	k1gFnPc2	mana
Blues	blues	k1gFnPc2	blues
<g/>
"	"	kIx"	"
od	od	k7c2	od
Memphis	Memphis	k1gFnSc2	Memphis
Slima	Slimum	k1gNnSc2	Slimum
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
skupina	skupina	k1gFnSc1	skupina
ukončila	ukončit	k5eAaPmAgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
Cave	Cave	k6eAd1	Cave
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
ohledně	ohledně	k7c2	ohledně
možného	možný	k2eAgInSc2d1	možný
reunionu	reunion	k1gInSc2	reunion
kapely	kapela	k1gFnSc2	kapela
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čím	co	k3yQnSc7	co
budou	být	k5eAaImBp3nP	být
členové	člen	k1gMnPc1	člen
starší	starý	k2eAgMnPc1d2	starší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
bude	být	k5eAaImBp3nS	být
Grinderman	Grinderman	k1gMnSc1	Grinderman
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
