<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgNnSc4	svůj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesátileté	padesátiletý	k2eAgFnSc2d1	padesátiletá
zpěvácké	zpěvácký	k2eAgFnSc2d1	zpěvácká
kariéry	kariéra	k1gFnSc2	kariéra
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
42krát	42krát	k1gInSc4	42krát
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
letech	let	k1gInPc6	let
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
pětkrát	pětkrát	k6eAd1	pětkrát
skončil	skončit	k5eAaPmAgInS	skončit
druhý	druhý	k4xOgMnSc1	druhý
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
a	a	k8xC	a
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
třikrát	třikrát	k6eAd1	třikrát
třetí	třetí	k4xOgFnSc2	třetí
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
a	a	k8xC	a
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
