<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
je	být	k5eAaImIp3nS	být
také	také	k9	také
přezdíván	přezdívat	k5eAaImNgInS	přezdívat
"	"	kIx"	"
<g/>
Bankfurt	Bankfurt	k1gInSc1	Bankfurt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
finančním	finanční	k2eAgNnSc7d1	finanční
srdcem	srdce	k1gNnSc7	srdce
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
centru	centrum	k1gNnSc6	centrum
sídlí	sídlet	k5eAaImIp3nS	sídlet
bankovní	bankovní	k2eAgFnSc1d1	bankovní
instituce	instituce	k1gFnSc1	instituce
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
