<p>
<s>
Max	Max	k1gMnSc1	Max
Kapferer	Kapferer	k1gMnSc1	Kapferer
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1866	[number]	k4	1866
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1950	[number]	k4	1950
Natters	Nattersa	k1gFnPc2	Nattersa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
z	z	k7c2	z
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Profesí	profes	k1gFnSc7	profes
byl	být	k5eAaImAgInS	být
advokátem	advokát	k1gMnSc7	advokát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
i	i	k9	i
poslancem	poslanec	k1gMnSc7	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
celostátního	celostátní	k2eAgInSc2d1	celostátní
parlamentu	parlament	k1gInSc2	parlament
Předlitavska	Předlitavsko	k1gNnSc2	Předlitavsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
usedl	usednout	k5eAaPmAgMnS	usednout
ve	v	k7c6	v
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
za	za	k7c4	za
kurii	kurie	k1gFnSc4	kurie
městskou	městský	k2eAgFnSc4d1	městská
a	a	k8xC	a
živnostenských	živnostenský	k2eAgFnPc2d1	Živnostenská
a	a	k8xC	a
obchodních	obchodní	k2eAgFnPc2d1	obchodní
komor	komora	k1gFnPc2	komora
v	v	k7c6	v
Tyrolsku	Tyrolsko	k1gNnSc6	Tyrolsko
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
<g/>
,	,	kIx,	,
Hall	Hall	k1gInSc1	Hall
atd.	atd.	kA	atd.
/	/	kIx~	/
Innsbruck	Innsbruck	k1gMnSc1	Innsbruck
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Max	Max	k1gMnSc1	Max
Kapferer	Kapferer	k1gMnSc1	Kapferer
<g/>
,	,	kIx,	,
advokátní	advokátní	k2eAgMnSc1d1	advokátní
koncipista	koncipista	k1gMnSc1	koncipista
<g/>
,	,	kIx,	,
bytem	byt	k1gInSc7	byt
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
jako	jako	k8xC	jako
katolický	katolický	k2eAgMnSc1d1	katolický
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Deklaroval	deklarovat	k5eAaBmAgMnS	deklarovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
hodlá	hodlat	k5eAaImIp3nS	hodlat
přistoupit	přistoupit	k5eAaPmF	přistoupit
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
ke	k	k7c3	k
Katolické	katolický	k2eAgFnSc3d1	katolická
lidové	lidový	k2eAgFnSc3d1	lidová
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
skutečně	skutečně	k6eAd1	skutečně
učinil	učinit	k5eAaImAgMnS	učinit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1898	[number]	k4	1898
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
Katolické	katolický	k2eAgFnSc2d1	katolická
lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgInS	být
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
neutralistickou	neutralistický	k2eAgFnSc7d1	neutralistická
politikou	politika	k1gFnSc7	politika
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
česko-německého	českoěmecký	k2eAgInSc2d1	česko-německý
konfliktu	konflikt	k1gInSc2	konflikt
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
zaváděna	zavádět	k5eAaImNgFnS	zavádět
kontroverzní	kontroverzní	k2eAgFnSc7d1	kontroverzní
a	a	k8xC	a
německou	německý	k2eAgFnSc7d1	německá
veřejnosti	veřejnost	k1gFnSc3	veřejnost
odmítaná	odmítaný	k2eAgFnSc1d1	odmítaná
Badeniho	Badeniha	k1gFnSc5	Badeniha
jazyková	jazykový	k2eAgNnPc4d1	jazykové
nařízení	nařízení	k1gNnPc4	nařízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Zasedal	zasedat	k5eAaImAgMnS	zasedat
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
Tyrolském	tyrolský	k2eAgInSc6d1	tyrolský
zemském	zemský	k2eAgInSc6d1	zemský
sněmu	sněm	k1gInSc6	sněm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
poslancem	poslanec	k1gMnSc7	poslanec
za	za	k7c4	za
kurii	kurie	k1gFnSc4	kurie
venkovských	venkovský	k2eAgFnPc2d1	venkovská
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
Hall	Halla	k1gFnPc2	Halla
v	v	k7c6	v
letech	let	k1gInPc6	let
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
Křesťansko-sociální	křesťanskoociální	k2eAgFnSc4d1	křesťansko-sociální
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
zemského	zemský	k2eAgInSc2d1	zemský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
