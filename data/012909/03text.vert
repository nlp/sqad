<p>
<s>
Vích	vích	k1gInSc1	vích
či	či	k8xC	či
věchet	věchet	k1gInSc1	věchet
je	být	k5eAaImIp3nS	být
svazek	svazek	k1gInSc4	svazek
slámy	sláma	k1gFnSc2	sláma
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
ze	z	k7c2	z
slámy	sláma	k1gFnSc2	sláma
při	při	k7c6	při
žních	žeň	k1gFnPc6	žeň
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svazků	svazek	k1gInPc2	svazek
klasů	klas	k1gInPc2	klas
se	se	k3xPyFc4	se
vázala	vázat	k5eAaImAgFnS	vázat
povřísla	povříslo	k1gNnSc2	povříslo
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
obilí	obilí	k1gNnSc1	obilí
svazovalo	svazovat	k5eAaImAgNnS	svazovat
do	do	k7c2	do
snopů	snop	k1gInPc2	snop
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
stavěli	stavět	k5eAaImAgMnP	stavět
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
slamění	slaměný	k2eAgMnPc1d1	slaměný
panáci	panák	k1gMnPc1	panák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
nepršelo	pršet	k5eNaImAgNnS	pršet
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
úroda	úroda	k1gFnSc1	úroda
před	před	k7c7	před
odvozem	odvoz	k1gInSc7	odvoz
do	do	k7c2	do
stodoly	stodola	k1gFnSc2	stodola
neznehodnotila	znehodnotit	k5eNaPmAgFnS	znehodnotit
<g/>
,	,	kIx,	,
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
se	se	k3xPyFc4	se
na	na	k7c4	na
zakrytí	zakrytí	k1gNnSc4	zakrytí
vrchů	vrch	k1gInPc2	vrch
ze	z	k7c2	z
slámy	sláma	k1gFnSc2	sláma
malá	malý	k2eAgFnSc1d1	malá
kulatá	kulatý	k2eAgFnSc1d1	kulatá
stříška	stříška	k1gFnSc1	stříška
–	–	k?	–
vích	vích	k1gInSc1	vích
<g/>
.	.	kIx.	.
</s>
</p>
