<p>
<s>
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Nürnberg	Nürnberg	k1gMnSc1	Nürnberg
U-Bahn	U-Bahn	k1gMnSc1	U-Bahn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
metro	metro	k1gNnSc1	metro
provozováno	provozován	k2eAgNnSc1d1	provozováno
společností	společnost	k1gFnSc7	společnost
VAG	VAG	kA	VAG
Nürnberg	Nürnberg	k1gMnSc1	Nürnberg
(	(	kIx(	(
<g/>
Verkehrsaktiengesellschaft	Verkehrsaktiengesellschaft	k1gMnSc1	Verkehrsaktiengesellschaft
Nürnberg	Nürnberg	k1gMnSc1	Nürnberg
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Norimberská	norimberský	k2eAgFnSc1d1	Norimberská
dopravní	dopravní	k2eAgFnSc1d1	dopravní
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
nejnovější	nový	k2eAgFnSc1d3	nejnovější
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
sítí	síť	k1gFnPc2	síť
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vozy	vůz	k1gInPc1	vůz
DT1	DT1	k1gFnSc2	DT1
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
designu	design	k1gInSc6	design
jako	jako	k9	jako
vozy	vůz	k1gInPc1	vůz
typu	typ	k1gInSc2	typ
A	a	k9	a
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
společnosti	společnost	k1gFnPc1	společnost
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
půjčily	půjčit	k5eAaPmAgInP	půjčit
vozy	vůz	k1gInPc1	vůz
jako	jako	k8xS	jako
rezervní	rezervní	k2eAgInPc1d1	rezervní
vozy	vůz	k1gInPc1	vůz
při	při	k7c6	při
významných	významný	k2eAgFnPc6d1	významná
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInPc3	jaký
byla	být	k5eAaImAgFnS	být
Olympiáda	olympiáda	k1gFnSc1	olympiáda
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
oba	dva	k4xCgInPc1	dva
systémy	systém	k1gInPc1	systém
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
začátcích	začátek	k1gInPc6	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
věc	věc	k1gFnSc1	věc
již	již	k6eAd1	již
nyní	nyní	k6eAd1	nyní
není	být	k5eNaImIp3nS	být
možná	možná	k9	možná
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
obě	dva	k4xCgFnPc1	dva
společnosti	společnost	k1gFnPc1	společnost
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
postupy	postup	k1gInPc4	postup
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
to	ten	k3xDgNnSc1	ten
znemožňují	znemožňovat	k5eAaImIp3nP	znemožňovat
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
musely	muset	k5eAaImAgFnP	muset
upravit	upravit	k5eAaPmF	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc1d2	novější
Norimberské	norimberský	k2eAgInPc1d1	norimberský
vozy	vůz	k1gInPc1	vůz
(	(	kIx(	(
<g/>
DT	DT	kA	DT
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nekompatibilní	kompatibilní	k2eNgMnPc1d1	nekompatibilní
s	s	k7c7	s
mnichovským	mnichovský	k2eAgInSc7d1	mnichovský
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
přepraví	přepravit	k5eAaPmIp3nS	přepravit
316	[number]	k4	316
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1967	[number]	k4	1967
německý	německý	k2eAgMnSc1d1	německý
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
Georg	Georg	k1gMnSc1	Georg
Leber	Leber	k1gMnSc1	Leber
a	a	k8xC	a
norimberský	norimberský	k2eAgMnSc1d1	norimberský
starosta	starosta	k1gMnSc1	starosta
Andreas	Andreas	k1gMnSc1	Andreas
Urschlechter	Urschlechter	k1gMnSc1	Urschlechter
měli	mít	k5eAaImAgMnP	mít
tu	tu	k6eAd1	tu
čest	čest	k1gFnSc4	čest
položit	položit	k5eAaPmF	položit
základní	základní	k2eAgInSc4d1	základní
kámen	kámen	k1gInSc4	kámen
pro	pro	k7c4	pro
nové	nový	k2eAgNnSc4d1	nové
metro	metro	k1gNnSc4	metro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
3,7	[number]	k4	3,7
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
linky	linka	k1gFnSc2	linka
U1	U1	k1gMnPc3	U1
z	z	k7c2	z
Langwasser	Langwassra	k1gFnPc2	Langwassra
Süd	Süd	k1gFnSc2	Süd
do	do	k7c2	do
Bauernfeindstraße	Bauernfeindstraß	k1gFnSc2	Bauernfeindstraß
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
příštích	příští	k2eAgNnPc2d1	příští
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgInP	být
otevřeny	otevřen	k2eAgInPc1d1	otevřen
další	další	k2eAgInPc1d1	další
úseky	úsek	k1gInPc1	úsek
linky	linka	k1gFnSc2	linka
U	u	k7c2	u
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1984	[number]	k4	1984
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
druhá	druhý	k4xOgFnSc1	druhý
linka	linka	k1gFnSc1	linka
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
od	od	k7c2	od
Plärrer	Plärrra	k1gFnPc2	Plärrra
do	do	k7c2	do
Schweinau	Schweinaus	k1gInSc2	Schweinaus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
linka	linka	k1gFnSc1	linka
eventuálně	eventuálně	k6eAd1	eventuálně
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
společností	společnost	k1gFnPc2	společnost
VGA	VGA	kA	VGA
zakoupeno	zakoupen	k2eAgNnSc1d1	zakoupeno
6	[number]	k4	6
vozů	vůz	k1gInPc2	vůz
typu	typ	k1gInSc2	typ
A	a	k8xC	a
k	k	k7c3	k
doplnění	doplnění	k1gNnSc3	doplnění
vozového	vozový	k2eAgInSc2d1	vozový
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
odlišnosti	odlišnost	k1gFnPc1	odlišnost
mezi	mezi	k7c4	mezi
mnichovský	mnichovský	k2eAgInSc4d1	mnichovský
a	a	k8xC	a
norimberským	norimberský	k2eAgInSc7d1	norimberský
systémem	systém	k1gInSc7	systém
zapříčinily	zapříčinit	k5eAaPmAgFnP	zapříčinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnichovské	mnichovský	k2eAgInPc1d1	mnichovský
vozy	vůz	k1gInPc1	vůz
nešly	jít	k5eNaImAgInP	jít
napojit	napojit	k5eAaPmF	napojit
s	s	k7c7	s
norimberskými	norimberský	k2eAgFnPc7d1	Norimberská
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
soupravy	souprava	k1gFnSc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
VGA	VGA	kA	VGA
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnichovské	mnichovský	k2eAgInPc1d1	mnichovský
vozy	vůz	k1gInPc1	vůz
nechá	nechat	k5eAaPmIp3nS	nechat
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
nátěru	nátěr	k1gInSc6	nátěr
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ani	ani	k8xC	ani
nestojí	stát	k5eNaImIp3nS	stát
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
nechat	nechat	k5eAaPmF	nechat
přelakovat	přelakovat	k5eAaPmF	přelakovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
blíží	blížit	k5eAaImIp3nS	blížit
konec	konec	k1gInSc1	konec
jejich	jejich	k3xOp3gFnSc2	jejich
životnosti	životnost	k1gFnSc2	životnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
nová	nový	k2eAgFnSc1d1	nová
<g/>
,	,	kIx,	,
1,3	[number]	k4	1,3
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
sekce	sekce	k1gFnSc1	sekce
linky	linka	k1gFnSc2	linka
U1	U1	k1gMnPc3	U1
do	do	k7c2	do
Fürth	Fürtha	k1gFnPc2	Fürtha
<g/>
,	,	kIx,	,
táhnoucí	táhnoucí	k2eAgInPc4d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
Stadthalle	Stadthalle	k1gFnSc2	Stadthalle
do	do	k7c2	do
Klinikum	Klinikum	k?	Klinikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
linka	linka	k1gFnSc1	linka
U1	U1	k1gFnSc1	U1
dále	daleko	k6eAd2	daleko
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
do	do	k7c2	do
Fürth	Fürtha	k1gFnPc2	Fürtha
Hardhöhe	Hardhöh	k1gFnSc2	Hardhöh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Síť	síť	k1gFnSc1	síť
metra	metro	k1gNnSc2	metro
==	==	k?	==
</s>
</p>
<p>
<s>
Síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
36	[number]	k4	36
km	km	kA	km
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
46	[number]	k4	46
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
stanice	stanice	k1gFnPc1	stanice
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
invalidům	invalid	k1gInPc3	invalid
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
ojedinělý	ojedinělý	k2eAgInSc1d1	ojedinělý
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
specialitou	specialita	k1gFnSc7	specialita
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolejnice	kolejnice	k1gFnPc1	kolejnice
nejsou	být	k5eNaImIp3nP	být
na	na	k7c6	na
pražcích	pražec	k1gInPc6	pražec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
betonové	betonový	k2eAgFnSc6d1	betonová
podložce	podložka	k1gFnSc6	podložka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
VAG	VAG	kA	VAG
Nürnberg	Nürnberg	k1gMnSc1	Nürnberg
</s>
</p>
