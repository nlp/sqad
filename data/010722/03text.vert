<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Horňák	Horňák	k1gMnSc1	Horňák
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1968	[number]	k4	1968
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
zpěvák	zpěvák	k1gMnSc1	zpěvák
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prodal	prodat	k5eAaPmAgInS	prodat
přes	přes	k7c4	přes
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
soudcem	soudce	k1gMnSc7	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Horňák	Horňák	k1gMnSc1	Horňák
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1968	[number]	k4	1968
v	v	k7c6	v
Sokolově	Sokolov	k1gInSc6	Sokolov
a	a	k8xC	a
neměl	mít	k5eNaImAgInS	mít
žádné	žádný	k3yNgMnPc4	žádný
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
se	se	k3xPyFc4	se
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zpěvu	zpěv	k1gInSc3	zpěv
byl	být	k5eAaImAgMnS	být
veden	vést	k5eAaImNgMnS	vést
již	již	k6eAd1	již
od	od	k7c2	od
útlého	útlý	k2eAgNnSc2d1	útlé
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Pavel	Pavel	k1gMnSc1	Pavel
Horňák	Horňák	k1gMnSc1	Horňák
proslavil	proslavit	k5eAaPmAgMnS	proslavit
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Janečkem	Janeček	k1gMnSc7	Janeček
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Kroky	krok	k1gInPc1	krok
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
např.	např.	kA	např.
s	s	k7c7	s
Michalem	Michal	k1gMnSc7	Michal
Davidem	David	k1gMnSc7	David
<g/>
,	,	kIx,	,
Markétou	Markéta	k1gFnSc7	Markéta
Muchovou	Muchová	k1gFnSc7	Muchová
<g/>
,	,	kIx,	,
Josefem	Josef	k1gMnSc7	Josef
Melenem	Melen	k1gMnSc7	Melen
<g/>
,	,	kIx,	,
Arnoštem	Arnošt	k1gMnSc7	Arnošt
Pátkem	Pátek	k1gMnSc7	Pátek
či	či	k8xC	či
Milanem	Milan	k1gMnSc7	Milan
Dykem	Dyk	k1gMnSc7	Dyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
zakončil	zakončit	k5eAaPmAgMnS	zakončit
studium	studium	k1gNnSc4	studium
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
magistra	magister	k1gMnSc2	magister
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
titul	titul	k1gInSc1	titul
doktora	doktor	k1gMnSc2	doktor
práv	práv	k2eAgInSc1d1	práv
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
odborný	odborný	k2eAgMnSc1d1	odborný
konzultant	konzultant	k1gMnSc1	konzultant
v	v	k7c6	v
Kanceláři	kancelář	k1gFnSc6	kancelář
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
soudcem	soudce	k1gMnSc7	soudce
<g/>
,	,	kIx,	,
zaměřeným	zaměřený	k2eAgNnSc7d1	zaměřené
na	na	k7c4	na
civilní	civilní	k2eAgFnSc4d1	civilní
agendu	agenda	k1gFnSc4	agenda
<g/>
.	.	kIx.	.
</s>
<s>
Soudil	soudil	k1gMnSc1	soudil
u	u	k7c2	u
Obvodního	obvodní	k2eAgInSc2d1	obvodní
soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
2	[number]	k4	2
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
k	k	k7c3	k
Městskému	městský	k2eAgInSc3d1	městský
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2019	[number]	k4	2019
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
soudcem	soudce	k1gMnSc7	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Horňák	Horňák	k1gMnSc1	Horňák
se	se	k3xPyFc4	se
v	v	k7c6	v
září	září	k1gNnSc6	září
1991	[number]	k4	1991
oženil	oženit	k5eAaPmAgInS	oženit
a	a	k8xC	a
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Renatou	Renata	k1gFnSc7	Renata
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Pavlínu	Pavlína	k1gFnSc4	Pavlína
(	(	kIx(	(
<g/>
*	*	kIx~	*
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
*	*	kIx~	*
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	album	k1gNnSc2	album
===	===	k?	===
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
prázdnin	prázdniny	k1gFnPc2	prázdniny
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
–	–	k?	–
Dívčí	dívčí	k2eAgNnSc4d1	dívčí
království	království	k1gNnSc4	království
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
S	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
i	i	k9	i
bez	bez	k7c2	bez
tebe	ty	k3xPp2nSc2	ty
</s>
</p>
<p>
<s>
===	===	k?	===
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
albech	album	k1gNnPc6	album
Kroků	krok	k1gInPc2	krok
Františka	František	k1gMnSc2	František
Janečka	Janeček	k1gMnSc2	Janeček
===	===	k?	===
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
To	to	k9	to
se	se	k3xPyFc4	se
oslaví	oslavit	k5eAaPmIp3nS	oslavit
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
senzace	senzace	k1gFnSc1	senzace
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Po	po	k7c6	po
cestách	cesta	k1gFnPc6	cesta
růžových	růžový	k2eAgFnPc6d1	růžová
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
Piknik	piknik	k1gInSc1	piknik
</s>
</p>
<p>
<s>
===	===	k?	===
Singly	singl	k1gInPc4	singl
===	===	k?	===
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
uvedena	uvést	k5eAaPmNgFnS	uvést
druhá	druhý	k4xOgFnSc1	druhý
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
interpretem	interpret	k1gMnSc7	interpret
Pavel	Pavel	k1gMnSc1	Pavel
Horňák	Horňák	k1gMnSc1	Horňák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
"	"	kIx"	"
<g/>
Škola	škola	k1gFnSc1	škola
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
"	"	kIx"	"
/	/	kIx~	/
"	"	kIx"	"
<g/>
Kdo	kdo	k3yInSc1	kdo
ví	vědět	k5eAaImIp3nS	vědět
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
oslaví	oslavit	k5eAaPmIp3nS	oslavit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Michalem	Michal	k1gMnSc7	Michal
Davidem	David	k1gMnSc7	David
a	a	k8xC	a
Markétou	Markéta	k1gFnSc7	Markéta
Muchovou	Muchová	k1gFnSc7	Muchová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
to	ten	k3xDgNnSc4	ten
zkrátka	zkrátka	k6eAd1	zkrátka
spískal	spískat	k5eAaPmAgMnS	spískat
<g/>
"	"	kIx"	"
/	/	kIx~	/
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
si	se	k3xPyFc3	se
piš	psát	k5eAaImRp2nS	psát
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
"	"	kIx"	"
<g/>
Sluníčko	sluníčko	k1gNnSc1	sluníčko
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
"	"	kIx"	"
<g/>
Tričko	tričko	k1gNnSc1	tričko
<g/>
"	"	kIx"	"
/	/	kIx~	/
"	"	kIx"	"
<g/>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
prázdnin	prázdniny	k1gFnPc2	prázdniny
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
"	"	kIx"	"
<g/>
Jsme	být	k5eAaImIp1nP	být
sehraní	sehraný	k2eAgMnPc1d1	sehraný
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
duet	duet	k1gInSc4	duet
s	s	k7c7	s
Michalem	Michal	k1gMnSc7	Michal
Davidem	David	k1gMnSc7	David
<g/>
)	)	kIx)	)
/	/	kIx~	/
Kotě	kotě	k1gNnSc4	kotě
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
"	"	kIx"	"
<g/>
Zítra	zítra	k6eAd1	zítra
zavolej	zavolat	k5eAaPmRp2nS	zavolat
<g/>
"	"	kIx"	"
/	/	kIx~	/
"	"	kIx"	"
<g/>
Maturitní	maturitní	k2eAgInSc1d1	maturitní
ples	ples	k1gInSc1	ples
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
"	"	kIx"	"
<g/>
S	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
i	i	k9	i
bez	bez	k7c2	bez
tebe	ty	k3xPp2nSc2	ty
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
"	"	kIx"	"
<g/>
Terč	terč	k1gFnSc1	terč
lásky	láska	k1gFnSc2	láska
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
"	"	kIx"	"
<g/>
Nebudem	Nebudem	k?	Nebudem
jinačí	jinačit	k5eAaImIp3nS	jinačit
<g/>
"	"	kIx"	"
/	/	kIx~	/
"	"	kIx"	"
<g/>
Tvůj	tvůj	k3xOp2gInSc1	tvůj
den	den	k1gInSc1	den
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
"	"	kIx"	"
<g/>
Stárneme	stárnout	k5eAaImIp1nP	stárnout
<g/>
"	"	kIx"	"
/	/	kIx~	/
"	"	kIx"	"
<g/>
Podnájem	podnájem	k1gInSc4	podnájem
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
projektech	projekt	k1gInPc6	projekt
===	===	k?	===
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Želvy	želva	k1gFnSc2	želva
Ninja	Ninjum	k1gNnSc2	Ninjum
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Písničky	písnička	k1gFnSc2	písnička
pro	pro	k7c4	pro
Barbie	Barbie	k1gFnPc4	Barbie
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Pavel	Pavel	k1gMnSc1	Pavel
Horňák	Horňák	k1gMnSc1	Horňák
</s>
</p>
