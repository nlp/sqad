<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Žďárské	Žďárská	k1gFnPc1
vrchy	vrch	k1gInPc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Žďárské	Žďárská	k1gFnSc2
vrchyIUCN	vrchyIUCN	k?
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Jeden	jeden	k4xCgInSc1
ze	z	k7c2
skalních	skalní	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
na	na	k7c6
hřebeni	hřeben	k1gInSc6
Devíti	devět	k4xCc2
skal	skála	k1gFnPc2
-	-	kIx~
Lisovská	Lisovská	k1gFnSc1
skálaZákladní	skálaZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1970	#num#	k4
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
490	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
709	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraje	kraj	k1gInPc4
</s>
<s>
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
a	a	k8xC
Vysočina	vysočina	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
8,6	8,6	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
2,75	2,75	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Žďárské	Žďárská	k1gFnPc1
vrchy	vrch	k1gInPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc1
</s>
<s>
www.zdarskevrchy.ochranaprirody.cz	www.zdarskevrchy.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Žďárské	Žďárská	k1gFnSc2
vrchy	vrch	k1gInPc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c6
pomezí	pomezí	k1gNnSc6
Pardubického	pardubický	k2eAgInSc2d1
kraje	kraj	k1gInSc2
a	a	k8xC
kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
mezi	mezi	k7c7
městy	město	k1gNnPc7
Hlinsko	Hlinsko	k1gNnSc1
<g/>
,	,	kIx,
Přibyslav	Přibyslav	k1gFnSc1
<g/>
,	,	kIx,
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
Polička	Polička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
rozloha	rozloha	k1gFnSc1
je	být	k5eAaImIp3nS
709	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severozápadě	severozápad	k1gInSc6
na	na	k7c4
ni	on	k3xPp3gFnSc4
bezprostředně	bezprostředně	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chráněné	chráněný	k2eAgNnSc1d1
území	území	k1gNnSc1
pokrývá	pokrývat	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc4d3
<g/>
,	,	kIx,
severovýchodní	severovýchodní	k2eAgFnSc4d1
část	část	k1gFnSc4
Českomoravské	českomoravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
a	a	k8xC
přilehlé	přilehlý	k2eAgFnPc4d1
vrchoviny	vrchovina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajina	Krajina	k1gFnSc1
na	na	k7c6
většině	většina	k1gFnSc6
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
vrchovinná	vrchovinný	k2eAgFnSc1d1
až	až	k6eAd1
pahorkatinná	pahorkatinný	k2eAgFnSc1d1
s	s	k7c7
nadmořskými	nadmořský	k2eAgFnPc7d1
výškami	výška	k1gFnPc7
490	#num#	k4
–	–	k?
836	#num#	k4
m.	m.	k?
Nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
vrch	vrch	k1gInSc4
Devět	devět	k4xCc1
skal	skála	k1gFnPc2
(	(	kIx(
<g/>
836	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholy	vrchol	k1gInPc7
jsou	být	k5eAaImIp3nP
převážně	převážně	k6eAd1
zaoblené	zaoblený	k2eAgInPc1d1
(	(	kIx(
<g/>
ve	v	k7c6
vrcholových	vrcholový	k2eAgFnPc6d1
partiích	partie	k1gFnPc6
však	však	k9
často	často	k6eAd1
najdeme	najít	k5eAaPmIp1nP
skalní	skalní	k2eAgInPc1d1
bloky	blok	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
svahy	svah	k1gInPc1
povlovné	povlovný	k2eAgInPc1d1
<g/>
,	,	kIx,
údolí	údolí	k1gNnPc1
mělká	mělký	k2eAgNnPc1d1
a	a	k8xC
široká	široký	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajina	Krajina	k1gFnSc1
si	se	k3xPyFc3
zachovala	zachovat	k5eAaPmAgFnS
poměrně	poměrně	k6eAd1
vyvážený	vyvážený	k2eAgInSc4d1
ráz	ráz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
oblast	oblast	k1gFnSc1
přirozené	přirozený	k2eAgFnSc2d1
akumulace	akumulace	k1gFnSc2
vod	voda	k1gFnPc2
<g/>
:	:	kIx,
pramení	pramenit	k5eAaImIp3nS
zde	zde	k6eAd1
řada	řada	k1gFnSc1
českých	český	k2eAgFnPc2d1
a	a	k8xC
moravských	moravský	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
(	(	kIx(
<g/>
Sázava	Sázava	k1gFnSc1
<g/>
,	,	kIx,
Chrudimka	Chrudimka	k1gFnSc1
<g/>
,	,	kIx,
Doubrava	Doubrava	k1gFnSc1
<g/>
,	,	kIx,
Svratka	Svratka	k1gFnSc1
a	a	k8xC
Oslava	oslava	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
prostírá	prostírat	k5eAaImIp3nS
se	se	k3xPyFc4
tu	tu	k6eAd1
několik	několik	k4yIc4
rybničních	rybniční	k2eAgFnPc2d1
soustav	soustava	k1gFnPc2
(	(	kIx(
<g/>
největší	veliký	k2eAgInPc1d3
rybníky	rybník	k1gInPc1
jsou	být	k5eAaImIp3nP
Velké	velký	k2eAgNnSc4d1
Dářko	Dářko	k1gNnSc4
<g/>
,	,	kIx,
Veselský	veselský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
<g/>
,	,	kIx,
Matějovský	Matějovský	k1gMnSc1
rybník	rybník	k1gInSc1
<g/>
,	,	kIx,
rybníky	rybník	k1gInPc1
Řeka	Řek	k1gMnSc2
a	a	k8xC
Medlov	Medlov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
oblastí	oblast	k1gFnSc7
prochází	procházet	k5eAaImIp3nS
evropské	evropský	k2eAgNnSc4d1
rozvodí	rozvodí	k1gNnSc4
Černého	Černý	k1gMnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Klima	klima	k1gNnSc1
oblasti	oblast	k1gFnSc2
je	být	k5eAaImIp3nS
chladnější	chladný	k2eAgInSc1d2
<g/>
,	,	kIx,
vlhčí	vlhký	k2eAgInSc1d2
a	a	k8xC
větrnější	větrný	k2eAgInSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
polovinu	polovina	k1gFnSc4
území	území	k1gNnSc2
pokrývá	pokrývat	k5eAaImIp3nS
les	les	k1gInSc1
<g/>
;	;	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
většinou	většinou	k6eAd1
o	o	k7c4
umělé	umělý	k2eAgFnPc4d1
smrkové	smrkový	k2eAgFnPc4d1
monokultury	monokultura	k1gFnPc4
na	na	k7c6
místě	místo	k1gNnSc6
přirozeného	přirozený	k2eAgInSc2d1
jedlobukového	jedlobukový	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
cenná	cenný	k2eAgNnPc1d1
společenstva	společenstvo	k1gNnPc1
rašelinišť	rašeliniště	k1gNnPc2
a	a	k8xC
vlhkých	vlhký	k2eAgFnPc2d1
rašelinných	rašelinný	k2eAgFnPc2d1
luk	louka	k1gFnPc2
s	s	k7c7
významným	významný	k2eAgInSc7d1
výskytem	výskyt	k1gInSc7
řady	řada	k1gFnSc2
chráněných	chráněný	k2eAgInPc2d1
a	a	k8xC
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
CHKO	CHKO	kA
jsou	být	k5eAaImIp3nP
vyhlášena	vyhlášen	k2eAgNnPc1d1
maloplošná	maloplošný	k2eAgNnPc1d1
zvláště	zvláště	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
čtyři	čtyři	k4xCgFnPc4
národní	národní	k2eAgFnPc4d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
<g/>
,	,	kIx,
devět	devět	k4xCc4
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
a	a	k8xC
36	#num#	k4
přírodních	přírodní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
sídlí	sídlet	k5eAaImIp3nP
ve	v	k7c6
Žďáře	Žďár	k1gInSc6
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
pod	pod	k7c4
ni	on	k3xPp3gFnSc4
též	též	k9
několik	několik	k4yIc4
dalších	další	k2eAgNnPc2d1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
:	:	kIx,
Mohelenská	Mohelenský	k2eAgFnSc1d1
hadcová	hadcový	k2eAgFnSc1d1
step	step	k1gFnSc1
<g/>
,	,	kIx,
národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
Velký	velký	k2eAgInSc1d1
Špičák	špičák	k1gInSc1
a	a	k8xC
rybník	rybník	k1gInSc1
Zhejral	Zhejral	k1gFnSc2
<g/>
;	;	kIx,
a	a	k8xC
národní	národní	k2eAgFnPc4d1
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
Hojkovské	Hojkovský	k2eAgNnSc4d1
rašeliniště	rašeliniště	k1gNnSc4
<g/>
,	,	kIx,
Jankovský	Jankovský	k2eAgInSc4d1
potok	potok	k1gInSc4
a	a	k8xC
Švařec	Švařec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Žďárské	Žďárská	k1gFnSc3
vrchy	vrch	k1gInPc4
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Ransko	Ransko	k6eAd1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Havranka	Havranka	k1gFnSc1
•	•	k?
Hroznětínská	Hroznětínský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
a	a	k8xC
olšina	olšina	k1gFnSc1
•	•	k?
Kamenná	kamenný	k2eAgFnSc1d1
trouba	trouba	k1gFnSc1
•	•	k?
Maršálka	maršálka	k1gFnSc1
•	•	k?
Mokřadlo	mokřadlo	k1gNnSc1
•	•	k?
Niva	niva	k1gFnSc1
Doubravy	Doubrava	k1gFnSc2
•	•	k?
Ranská	Ranská	k1gFnSc1
jezírka	jezírko	k1gNnSc2
•	•	k?
Řeka	řeka	k1gFnSc1
•	•	k?
Spálava	Spálava	k1gFnSc1
•	•	k?
Stvořidla	Stvořidlo	k1gNnSc2
•	•	k?
Svatomariánské	Svatomariánský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Štíří	štířit	k5eAaImIp3nS
důl	důl	k1gInSc4
•	•	k?
Údolí	údolí	k1gNnSc2
Doubravy	Doubrava	k1gFnSc2
•	•	k?
Velká	velký	k2eAgFnSc1d1
a	a	k8xC
Malá	malý	k2eAgFnSc1d1
olšina	olšina	k1gFnSc1
•	•	k?
Zlatá	zlatý	k2eAgFnSc1d1
louka	louka	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Borecká	borecký	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
•	•	k?
Čertův	čertův	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Chuchelská	chuchelský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Kamenický	kamenický	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Písník	písník	k1gInSc1
u	u	k7c2
Sokolovce	sokolovka	k1gFnSc3
•	•	k?
Pod	pod	k7c7
Kazbalem	Kazbal	k1gInSc7
•	•	k?
Rybníček	rybníček	k1gInSc1
u	u	k7c2
Dolního	dolní	k2eAgInSc2d1
Dvora	Dvůr	k1gInSc2
•	•	k?
Sochorov	Sochorov	k1gInSc1
•	•	k?
Šlapanka	Šlapanka	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Svitavy	Svitava	k1gFnSc2
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Bohdalov-Hartinkov	Bohdalov-Hartinkov	k1gInSc1
•	•	k?
Svratecká	svratecký	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
•	•	k?
Údolí	údolí	k1gNnSc1
Křetínky	Křetínka	k1gFnSc2
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1
palice	palice	k1gFnPc1
•	•	k?
Damašek	Damašek	k1gInSc1
•	•	k?
Dlouholoučské	Dlouholoučský	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
•	•	k?
Hřebečovský	Hřebečovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Králova	Králův	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
•	•	k?
Maštale	Maštale	k?
•	•	k?
Psí	psí	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
•	•	k?
Rohová	rohový	k2eAgFnSc1d1
Přírodní	přírodní	k2eAgFnSc1d1
památky	památka	k1gFnPc1
</s>
<s>
Hradisko	hradisko	k1gNnSc1
•	•	k?
Nedošínský	Nedošínský	k2eAgInSc4d1
háj	háj	k1gInSc4
•	•	k?
Pod	pod	k7c7
Skálou	skála	k1gFnSc7
•	•	k?
Rybenské	Rybenský	k2eAgFnSc2d1
Perničky	Perničky	k?
•	•	k?
Rychnovský	rychnovský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
•	•	k?
Sněženky	sněženka	k1gFnSc2
ve	v	k7c6
Vysokém	vysoký	k2eAgInSc6d1
lese	les	k1gInSc6
•	•	k?
U	u	k7c2
Banínského	Banínský	k2eAgInSc2d1
viaduktu	viadukt	k1gInSc2
•	•	k?
V	v	k7c6
bukách	bukách	k?
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Balinské	Balinský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Bohdalovsko	Bohdalovsko	k1gNnSc1
•	•	k?
Svratecká	svratecký	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Dářko	Dářko	k6eAd1
•	•	k?
Radostínské	Radostínský	k2eAgNnSc4d1
rašeliniště	rašeliniště	k1gNnSc4
•	•	k?
Žákova	Žákův	k2eAgFnSc1d1
hora	hora	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Švařec	Švařec	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Baba	baba	k1gFnSc1
–	–	k?
V	v	k7c6
bukách	bukách	k?
•	•	k?
Branty	Branty	k?
•	•	k?
Čtyři	čtyři	k4xCgNnPc4
palice	palice	k1gFnSc1
•	•	k?
Meandry	meandr	k1gInPc1
Svratky	Svratka	k1gFnSc2
u	u	k7c2
Milov	Milovo	k1gNnPc2
•	•	k?
Ochoza	Ochoza	k?
•	•	k?
Olšina	olšina	k1gFnSc1
u	u	k7c2
Skleného	sklený	k2eAgInSc2d1
•	•	k?
Pod	pod	k7c7
Kamenným	kamenný	k2eAgInSc7d1
vrchem	vrch	k1gInSc7
•	•	k?
Štíří	štíří	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Údolí	údolí	k1gNnSc1
Chlébského	Chlébský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Vírská	vírský	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Belfrídský	Belfrídský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Bílá	bílý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Brožova	Brožův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Černá	černý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Dědkovo	Dědkův	k2eAgNnSc1d1
•	•	k?
Devět	devět	k4xCc1
skal	skála	k1gFnPc2
•	•	k?
Díly	díl	k1gInPc1
u	u	k7c2
Lhotky	Lhotka	k1gFnSc2
•	•	k?
Dobrá	dobrý	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
•	•	k?
Drátenická	drátenický	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Heřmanov	Heřmanov	k1gInSc1
•	•	k?
Hodíškovský	Hodíškovský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Javorův	Javorův	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Kocoury	kocour	k1gInPc1
•	•	k?
Křižník	křižník	k1gInSc1
•	•	k?
Laguna	laguna	k1gFnSc1
u	u	k7c2
Bohdalova	Bohdalův	k2eAgInSc2d1
•	•	k?
Lisovská	Lisovská	k1gFnSc1
skála	skála	k1gFnSc1
•	•	k?
Louky	louka	k1gFnSc2
u	u	k7c2
Černého	Černého	k2eAgInSc2d1
lesa	les	k1gInSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Louky	louka	k1gFnSc2
u	u	k7c2
Polomu	polom	k1gInSc2
•	•	k?
Malinská	Malinský	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Milovské	milovský	k2eAgNnSc1d1
perničky	perničky	k?
•	•	k?
Mlýnský	mlýnský	k2eAgInSc4d1
potok	potok	k1gInSc4
a	a	k8xC
Uhlířky	uhlířka	k1gFnSc2
•	•	k?
Mrázkova	Mrázkův	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Na	na	k7c6
Ostrážné	Ostrážný	k2eAgFnSc6d1
•	•	k?
Na	na	k7c6
skále	skála	k1gFnSc6
•	•	k?
Nad	nad	k7c7
koupalištěm	koupaliště	k1gNnSc7
•	•	k?
Nyklovický	Nyklovický	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Obecník	Obecník	k1gInSc1
•	•	k?
Olšoveček	Olšoveček	k1gInSc1
•	•	k?
Ostražka	Ostražka	k1gFnSc1
•	•	k?
Ouperek	Ouperka	k1gFnPc2
•	•	k?
Pasecká	Pasecký	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Peperek	Peperka	k1gFnPc2
•	•	k?
Pernovka	Pernovka	k1gFnSc1
•	•	k?
Prosička	Prosička	k1gFnSc1
•	•	k?
Rasuveň	Rasuveň	k1gFnSc1
•	•	k?
Rozštípená	rozštípený	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Rybníky	rybník	k1gInPc1
u	u	k7c2
Rudolce	Rudolec	k1gInSc2
•	•	k?
Sklenské	Sklenský	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Staropavlovský	Staropavlovský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Suché	Suché	k2eAgInSc2d1
kopce	kopec	k1gInSc2
•	•	k?
Světnovské	Světnovský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Svratka	Svratka	k1gFnSc1
•	•	k?
Šafranice	šafranice	k1gFnSc1
•	•	k?
Šebeň	Šebeň	k1gFnSc1
•	•	k?
Štarkov	Štarkov	k1gInSc1
•	•	k?
Švařec	Švařec	k1gInSc1
II	II	kA
•	•	k?
Tisůvka	Tisůvka	k1gFnSc1
•	•	k?
U	u	k7c2
Bezděkova	Bezděkův	k2eAgNnSc2d1
•	•	k?
U	u	k7c2
Hamrů	Hamry	k1gInPc2
•	•	k?
Vlčí	vlčet	k5eAaImIp3nS
kámen	kámen	k1gInSc1
•	•	k?
Zkamenělý	zkamenělý	k2eAgInSc1d1
zámek	zámek	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
|	|	kIx~
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
