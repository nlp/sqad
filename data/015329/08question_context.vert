<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Žďárské	Žďárská	k1gFnSc2
vrchy	vrch	k1gInPc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c6
pomezí	pomezí	k1gNnSc6
Pardubického	pardubický	k2eAgInSc2d1
kraje	kraj	k1gInSc2
a	a	k8xC
kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
mezi	mezi	k7c7
městy	město	k1gNnPc7
Hlinsko	Hlinsko	k1gNnSc1
<g/>
,	,	kIx,
Přibyslav	Přibyslav	k1gFnSc1
<g/>
,	,	kIx,
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
Polička	Polička	k1gFnSc1
<g/>
.	.	kIx.
</s>