<s>
Boží	boží	k2eAgInSc1d1
sendvič	sendvič	k1gInSc1
</s>
<s>
Boží	boží	k2eAgInSc1d1
sendvič	sendvič	k1gInSc1
</s>
<s>
Díl	díl	k1gInSc1
seriálu	seriál	k1gInSc2
Glee	Gle	k1gMnSc2
Pův	Pův	k1gMnSc2
<g/>
.	.	kIx.
název	název	k1gInSc1
</s>
<s>
Grilled	Grilled	k1gMnSc1
Cheesus	Cheesus	k1gMnSc1
Číslo	číslo	k1gNnSc4
</s>
<s>
řada	řada	k1gFnSc1
2	#num#	k4
<g/>
díl	díl	k1gInSc1
3	#num#	k4
Premiéra	premiér	k1gMnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2010	#num#	k4
Tvorba	tvorba	k1gFnSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Brad	brada	k1gFnPc2
Falchuk	Falchuka	k1gFnPc2
Režie	režie	k1gFnSc2
</s>
<s>
Alfonso	Alfonso	k1gMnSc1
Gomez-Rejon	Gomez-Rejon	k1gMnSc1
Prod	Prod	k1gMnSc1
<g/>
.	.	kIx.
kód	kód	k1gInSc1
</s>
<s>
2ARC03	2ARC03	k4
Hosté	host	k1gMnPc1
</s>
<s>
Harry	Harra	k1gFnPc1
Shum	Shuma	k1gFnPc2
mladší	mladý	k2eAgFnSc2d2
jako	jako	k8xS,k8xC
Mike	Mik	k1gFnSc2
Chang	Chang	k1gMnSc1
</s>
<s>
Chord	chorda	k1gFnPc2
Overstreet	Overstreet	k1gMnSc1
jako	jako	k8xC,k8xS
Sam	Sam	k1gMnSc1
Evans	Evansa	k1gFnPc2
</s>
<s>
Dot-Marie	Dot-Marie	k1gFnSc1
Jones	Jonesa	k1gFnPc2
jako	jako	k8xS,k8xC
Shannon	Shannona	k1gFnPc2
Beiste	Beist	k1gInSc5
</s>
<s>
Iqbal	Iqbal	k1gMnSc1
Theba	Theba	k1gMnSc1
jako	jako	k8xC,k8xS
ředitel	ředitel	k1gMnSc1
Figgins	Figginsa	k1gFnPc2
</s>
<s>
Romy	Rom	k1gMnPc4
Rosemont	Rosemonta	k1gFnPc2
jako	jako	k8xC,k8xS
Carole	Carole	k1gFnSc2
Hudson	Hudson	k1gMnSc1
</s>
<s>
James	James	k1gMnSc1
Earl	earl	k1gMnSc1
jako	jako	k8xC,k8xS
Azimio	Azimio	k1gMnSc1
</s>
<s>
Robin	Robina	k1gFnPc2
Trocki	Trock	k1gFnSc2
jako	jako	k8xS,k8xC
Jean	Jean	k1gMnSc1
Sylvester	Sylvester	k1gMnSc1
</s>
<s>
Posloupnost	posloupnost	k1gFnSc1
dílů	díl	k1gInPc2
</s>
<s>
←	←	k?
Předchozí	předchozí	k2eAgFnSc2d1
Britney	Britnea	k1gFnSc2
<g/>
/	/	kIx~
<g/>
BrittanyNásledující	BrittanyNásledující	k2eAgFnSc1d1
→	→	k?
Soutěž	soutěž	k1gFnSc1
v	v	k7c6
duetech	duet	k1gInPc6
Seznam	seznam	k1gInSc4
dílů	díl	k1gInPc2
seriálu	seriál	k1gInSc2
GleeNěkterá	GleeNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Boží	boží	k2eAgInSc1d1
sendvič	sendvič	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
originále	originál	k1gInSc6
Grilled	Grilled	k1gMnSc1
Cheesus	Cheesus	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
třetí	třetí	k4xOgFnSc1
epizoda	epizoda	k1gFnSc1
druhé	druhý	k4xOgFnSc2
série	série	k1gFnSc2
amerického	americký	k2eAgInSc2d1
televizního	televizní	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
Glee	Glee	k1gInSc1
a	a	k8xC
v	v	k7c6
celkovém	celkový	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
dvacátá	dvacátý	k4xOgFnSc1
pátá	pátá	k1gFnSc1
epizoda	epizoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scénář	scénář	k1gInSc1
k	k	k7c3
ní	on	k3xPp3gFnSc3
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
Brad	brada	k1gFnPc2
Falchuk	Falchuk	k1gInSc1
<g/>
,	,	kIx,
režíroval	režírovat	k5eAaImAgMnS
ji	on	k3xPp3gFnSc4
Alfonso	Alfonso	k1gMnSc1
Gomez-Rejon	Gomez-Rejon	k1gMnSc1
a	a	k8xC
měla	mít	k5eAaImAgFnS
premiéru	premiéra	k1gFnSc4
ve	v	k7c6
vysílání	vysílání	k1gNnSc6
amerického	americký	k2eAgInSc2d1
televizního	televizní	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
Fox	fox	k1gInSc1
dne	den	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
vysíláním	vysílání	k1gNnSc7
epizody	epizoda	k1gFnSc2
prohlásil	prohlásit	k5eAaPmAgMnS
tvůrce	tvůrce	k1gMnSc1
seriálu	seriál	k1gInSc2
Ryan	Ryan	k1gMnSc1
Murphy	Murpha	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
díl	díl	k1gInSc1
bude	být	k5eAaImBp3nS
zatím	zatím	k6eAd1
nejkontroverznějším	kontroverzní	k2eAgNnSc7d3
dílem	dílo	k1gNnSc7
Glee	Gle	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
soustředí	soustředit	k5eAaPmIp3nS
na	na	k7c4
náboženství	náboženství	k1gNnSc4
a	a	k8xC
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
znamená	znamenat	k5eAaImIp3nS
Bůh	bůh	k1gMnSc1
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
členy	člen	k1gInPc4
sboru	sbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
má	mít	k5eAaImIp3nS
Burt	Burt	k2eAgInSc1d1
Hummel	Hummel	k1gInSc1
(	(	kIx(
<g/>
Mike	Mike	k1gFnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Malley	Malle	k2eAgFnPc4d1
<g/>
)	)	kIx)
srdeční	srdeční	k2eAgInSc4d1
záchvat	záchvat	k1gInSc4
<g/>
,	,	kIx,
sbor	sbor	k1gInSc1
se	se	k3xPyFc4
shromáždí	shromáždět	k5eAaImIp3nS,k5eAaPmIp3nS
okolo	okolo	k7c2
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
syna	syn	k1gMnSc2
Kurta	Kurt	k1gMnSc2
(	(	kIx(
<g/>
Chris	Chris	k1gFnSc1
Colfer	Colfer	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
pokouší	pokoušet	k5eAaImIp3nP
se	se	k3xPyFc4
Hummelovi	Hummelův	k2eAgMnPc1d1
podpořit	podpořit	k5eAaPmF
pomocí	pomocí	k7c2
svých	svůj	k3xOyFgNnPc2
různých	různý	k2eAgNnPc2d1
náboženství	náboženství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
hlavní	hlavní	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
sboru	sbor	k1gInSc2
Finn	Finn	k1gMnSc1
Hudson	Hudson	k1gMnSc1
(	(	kIx(
<g/>
Cory	Cory	k1gInPc1
Monteith	Monteitha	k1gFnPc2
<g/>
)	)	kIx)
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
našel	najít	k5eAaPmAgMnS
tvář	tvář	k1gFnSc4
Ježíše	Ježíš	k1gMnSc2
na	na	k7c6
grilovaném	grilovaný	k2eAgInSc6d1
sýrovém	sýrový	k2eAgInSc6d1
sendviči	sendvič	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Murphy	Murph	k1gMnPc4
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
epizoda	epizoda	k1gFnSc1
bude	být	k5eAaImBp3nS
obsahovat	obsahovat	k5eAaImF
vyvážené	vyvážený	k2eAgNnSc4d1
znázornění	znázornění	k1gNnSc4
náboženství	náboženství	k1gNnSc2
a	a	k8xC
on	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
Brad	brada	k1gFnPc2
Falchuk	Falchuka	k1gFnPc2
a	a	k8xC
Ian	Ian	k1gMnPc1
Brennan	Brennany	k1gInPc2
potvrdili	potvrdit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
tam	tam	k6eAd1
rovnováha	rovnováha	k1gFnSc1
mezi	mezi	k7c7
pro	pro	k7c4
a	a	k8xC
anti	anti	k6eAd1
náboženskými	náboženský	k2eAgInPc7d1
komentáři	komentář	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
epizoda	epizoda	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
coververze	coververze	k1gFnSc1
sedmi	sedm	k4xCc2
písní	píseň	k1gFnPc2
a	a	k8xC
všechny	všechen	k3xTgInPc1
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
v	v	k7c6
žebříčku	žebříček	k1gInSc6
Billboard	billboard	k1gInSc1
Hot	hot	k0
100	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vytvořilo	vytvořit	k5eAaPmAgNnS
jednotýdenní	jednotýdenní	k2eAgInSc4d1
debut	debut	k1gInSc4
seriálu	seriál	k1gInSc2
v	v	k7c6
žebříčku	žebříček	k1gInSc6
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritici	kritik	k1gMnPc1
si	se	k3xPyFc3
odporovali	odporovat	k5eAaImAgMnP
ohledně	ohledně	k7c2
vhodnosti	vhodnost	k1gFnSc2
hudebních	hudební	k2eAgInPc2d1
vystoupené	vystoupený	k2eAgMnPc4d1
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
si	se	k3xPyFc3
stěžovali	stěžovat	k5eAaImAgMnP
na	na	k7c4
dotýkající	dotýkající	k2eAgInSc4d1
se	se	k3xPyFc4
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
čísly	číslo	k1gNnPc7
a	a	k8xC
náboženstvím	náboženství	k1gNnSc7
a	a	k8xC
další	další	k2eAgMnPc4d1
ocenili	ocenit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Glee	Glee	k1gFnSc1
verze	verze	k1gFnSc1
do	do	k7c2
písní	píseň	k1gFnPc2
přinesly	přinést	k5eAaPmAgInP
písním	píseň	k1gFnPc3
nový	nový	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Epizodu	epizoda	k1gFnSc4
v	v	k7c4
den	den	k1gInSc4
vysílání	vysílání	k1gNnSc2
sledovalo	sledovat	k5eAaImAgNnS
11,20	11,20	k4
milionů	milion	k4xCgInPc2
amerických	americký	k2eAgMnPc2d1
diváků	divák	k1gMnPc2
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	s	k7c7
druhým	druhý	k4xOgInSc7
nejsledovanějším	sledovaný	k2eAgInSc7d3
napsaným	napsaný	k2eAgInSc7d1
pořadem	pořad	k1gInSc7
týdne	týden	k1gInSc2
ve	v	k7c6
věkové	věkový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
dospělí	dospělí	k1gMnPc1
od	od	k7c2
18	#num#	k4
do	do	k7c2
49	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získala	získat	k5eAaPmAgFnS
smíšené	smíšený	k2eAgFnPc4d1
recenze	recenze	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkony	výkon	k1gInPc1
Colfera	Colfero	k1gNnSc2
a	a	k8xC
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Malleyho	Malley	k1gMnSc4
si	se	k3xPyFc3
získaly	získat	k5eAaPmAgFnP
obdiv	obdiv	k1gInSc4
u	u	k7c2
kritiků	kritik	k1gMnPc2
a	a	k8xC
někteří	některý	k3yIgMnPc1
recenzenti	recenzent	k1gMnPc1
chválili	chválit	k5eAaImAgMnP
Glee	Glee	k1gFnSc4
za	za	k7c4
úspěšnou	úspěšný	k2eAgFnSc4d1
rovnováhu	rovnováha	k1gFnSc4
protichůdných	protichůdný	k2eAgInPc2d1
úhlů	úhel	k1gInPc2
pohledu	pohled	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
další	další	k2eAgMnPc1d1
recenzenti	recenzent	k1gMnPc1
kritizovali	kritizovat	k5eAaImAgMnP
epizodu	epizoda	k1gFnSc4
za	za	k7c4
nedostatek	nedostatek	k1gInSc4
jemnosti	jemnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
epizody	epizoda	k1gFnSc2
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
hlavní	hlavní	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
sboru	sbor	k1gInSc2
Finn	Finn	k1gMnSc1
Hudson	Hudson	k1gMnSc1
(	(	kIx(
<g/>
Cory	Cory	k1gInPc1
Monteith	Monteitha	k1gFnPc2
<g/>
)	)	kIx)
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
našel	najít	k5eAaPmAgMnS
tvář	tvář	k1gFnSc4
Ježíše	Ježíš	k1gMnSc2
na	na	k7c6
grilovaném	grilovaný	k2eAgInSc6d1
sýrovém	sýrový	k2eAgInSc6d1
sendviči	sendvič	k1gInSc6
<g/>
,	,	kIx,
žádá	žádat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
splnily	splnit	k5eAaPmAgFnP
tři	tři	k4xCgNnPc4
přání	přání	k1gNnPc4
<g/>
:	:	kIx,
aby	aby	kYmCp3nS
školní	školní	k2eAgNnSc1d1
fotbalové	fotbalový	k2eAgNnSc1d1
družstvo	družstvo	k1gNnSc1
vyhrálo	vyhrát	k5eAaPmAgNnS
hru	hra	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
jeho	jeho	k3xOp3gNnSc4
přítelkyně	přítelkyně	k1gFnPc1
Rachel	Rachela	k1gFnPc2
(	(	kIx(
<g/>
Lea	Lea	k1gFnSc1
Michele	Michel	k1gInSc2
<g/>
)	)	kIx)
dovolila	dovolit	k5eAaPmAgFnS
dotknout	dotknout	k5eAaPmF
se	se	k3xPyFc4
jejích	její	k3xOp3gNnPc2
ňader	ňadro	k1gNnPc2
a	a	k8xC
aby	aby	kYmCp3nS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
znovu	znovu	k6eAd1
quarterbackem	quarterbacko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
splní	splnit	k5eAaPmIp3nS
první	první	k4xOgNnSc4
přání	přání	k1gNnSc4
<g/>
,	,	kIx,
požádá	požádat	k5eAaPmIp3nS
sbor	sbor	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
k	k	k7c3
němu	on	k3xPp3gMnSc3
přidal	přidat	k5eAaPmAgMnS
ve	v	k7c6
ctění	ctění	k1gNnSc6
Ježíše	Ježíš	k1gMnSc2
prostřednictvím	prostřednictvím	k7c2
písně	píseň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Člen	člen	k1gMnSc1
sboru	sbor	k1gInSc2
Kurt	Kurt	k1gMnSc1
Hummel	Hummel	k1gMnSc1
(	(	kIx(
<g/>
Chris	Chris	k1gInSc1
Colfer	Colfer	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zničen	zničen	k2eAgMnSc1d1
<g/>
,	,	kIx,
když	když	k8xS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
Burt	Burt	k1gMnSc1
(	(	kIx(
<g/>
Mike	Mike	k1gFnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Malley	Mallea	k1gFnPc4
utrpí	utrpět	k5eAaPmIp3nS
srdeční	srdeční	k2eAgInSc1d1
záchvat	záchvat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
nejlepší	dobrý	k2eAgFnSc1d3
kamarádka	kamarádka	k1gFnSc1
Mercedes	mercedes	k1gInSc1
(	(	kIx(
<g/>
Amber	ambra	k1gFnPc2
Riley	Rilea	k1gFnSc2
<g/>
)	)	kIx)
u	u	k7c2
zpívá	zpívat	k5eAaImIp3nS
"	"	kIx"
<g/>
I	i	k8xC
Look	Look	k1gMnSc1
to	ten	k3xDgNnSc4
You	You	k1gFnSc1
<g/>
"	"	kIx"
od	od	k7c2
Whitney	Whitnea	k1gFnSc2
Houston	Houston	k1gInSc1
a	a	k8xC
doufá	doufat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
najde	najít	k5eAaPmIp3nS
sílu	síla	k1gFnSc4
ve	v	k7c6
víru	vír	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kurt	Kurt	k1gMnSc1
ji	on	k3xPp3gFnSc4
ale	ale	k8xC
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
ateista	ateista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenérka	trenérka	k1gFnSc1
roztleskávaček	roztleskávačka	k1gFnPc2
Sue	Sue	k1gMnSc1
Sylvester	Sylvester	k1gMnSc1
(	(	kIx(
<g/>
Jane	Jan	k1gMnSc5
Lynch	Lyn	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
také	také	k9
ateistka	ateistka	k1gFnSc1
<g/>
,	,	kIx,
chce	chtít	k5eAaImIp3nS
zakázat	zakázat	k5eAaPmF
sboru	sbor	k1gInSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zpíval	zpívat	k5eAaImAgMnS
náboženské	náboženský	k2eAgFnPc4d1
písně	píseň	k1gFnPc4
ve	v	k7c6
veřejném	veřejný	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
a	a	k8xC
proti	proti	k7c3
Kurtovi	Kurt	k1gMnSc3
vznese	vznést	k5eAaPmIp3nS
formální	formální	k2eAgFnSc4d1
stížnost	stížnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
ji	on	k3xPp3gFnSc4
konfrontuje	konfrontovat	k5eAaBmIp3nS
školní	školní	k2eAgFnSc1d1
výchovná	výchovný	k2eAgFnSc1d1
poradkyně	poradkyně	k1gFnSc1
Emma	Emma	k1gFnSc1
Pillsbury	Pillsbura	k1gFnSc2
(	(	kIx(
<g/>
Jayma	Jayma	k1gFnSc1
Mays	Maysa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sue	Sue	k1gMnSc1
přiznává	přiznávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jako	jako	k8xC,k8xS
malé	malý	k2eAgNnSc1d1
dítě	dítě	k1gNnSc1
modlila	modlil	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Bůh	bůh	k1gMnSc1
vyléčil	vyléčit	k5eAaPmAgMnS
její	její	k3xOp3gFnSc4
sestru	sestra	k1gFnSc4
Jean	Jean	k1gMnSc1
(	(	kIx(
<g/>
Robin	Robina	k1gFnPc2
Trocki	Trock	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
Downův	Downův	k2eAgInSc4d1
syndrom	syndrom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
modlitby	modlitba	k1gFnPc1
zůstaly	zůstat	k5eAaPmAgFnP
nevyslyšeny	vyslyšet	k5eNaPmNgFnP
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
došla	dojít	k5eAaPmAgFnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
Bůh	bůh	k1gMnSc1
prostě	prostě	k9
neexistuje	existovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Mercedes	mercedes	k1gInSc1
<g/>
,	,	kIx,
Rachel	Rachel	k1gMnSc1
a	a	k8xC
Quinn	Quinn	k1gMnSc1
(	(	kIx(
<g/>
Dianna	Dianna	k1gFnSc1
Agron	Agron	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
modlí	modlit	k5eAaImIp3nS
za	za	k7c4
Burta	Burt	k1gMnSc4
a	a	k8xC
Rachel	Rachel	k1gInSc1
mu	on	k3xPp3gMnSc3
u	u	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
lůžka	lůžko	k1gNnSc2
zpívá	zpívat	k5eAaImIp3nS
"	"	kIx"
<g/>
Papa	papa	k1gMnSc1
<g/>
,	,	kIx,
Can	Can	k1gMnSc1
You	You	k1gFnSc2
Hear	Hear	k1gMnSc1
Me	Me	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kurt	Kurt	k1gMnSc1
je	být	k5eAaImIp3nS
odolnější	odolný	k2eAgMnSc1d2
a	a	k8xC
později	pozdě	k6eAd2
spolu	spolu	k6eAd1
se	se	k3xPyFc4
sborem	sborem	k6eAd1
zkouší	zkoušet	k5eAaImIp3nP
píseň	píseň	k1gFnSc4
"	"	kIx"
<g/>
I	i	k8xC
Want	Want	k1gMnSc1
to	ten	k3xDgNnSc4
Hold	hold	k1gInSc1
Your	Your	k1gMnSc1
Hand	Hand	k1gMnSc1
<g/>
"	"	kIx"
od	od	k7c2
Beatles	Beatles	k1gFnPc2
a	a	k8xC
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
víra	víra	k1gFnSc1
je	být	k5eAaImIp3nS
forma	forma	k1gFnSc1
lásky	láska	k1gFnSc2
pro	pro	k7c4
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
otce	otec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijímá	přijímat	k5eAaImIp3nS
pozvání	pozvání	k1gNnSc4
Mercedes	mercedes	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
s	s	k7c7
ní	on	k3xPp3gFnSc7
šel	jít	k5eAaImAgMnS
do	do	k7c2
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
sbor	sbor	k1gInSc1
zpívá	zpívat	k5eAaImIp3nS
"	"	kIx"
<g/>
Bridge	Bridge	k1gFnSc1
over	over	k1gMnSc1
Troubled	Troubled	k1gMnSc1
Water	Water	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
bohuslužbě	bohuslužba	k1gFnSc6
žádá	žádat	k5eAaImIp3nS
Mercedes	mercedes	k1gInSc1
všechny	všechen	k3xTgMnPc4
shromážděné	shromážděný	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
za	za	k7c4
Hummelovy	Hummelův	k2eAgMnPc4d1
modlili	modlit	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
Finnovy	Finnův	k2eAgFnPc1d1
zbývající	zbývající	k2eAgFnPc1d1
modlitby	modlitba	k1gFnPc1
se	se	k3xPyFc4
také	také	k9
splní	splnit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoho	jeden	k4xCgInSc2
večera	večer	k1gInSc2
přijde	přijít	k5eAaPmIp3nS
Rachel	Rachel	k1gInSc4
k	k	k7c3
Finnovi	Finn	k1gMnSc3
domů	domů	k6eAd1
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
pokoji	pokoj	k1gInSc6
mu	on	k3xPp3gMnSc3
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
radši	rád	k6eAd2
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nP
jejich	jejich	k3xOp3gFnPc4
děti	dítě	k1gFnPc4
vyrůstali	vyrůstat	k5eAaImAgMnP
v	v	k7c6
židovské	židovský	k2eAgFnSc6d1
víře	víra	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
s	s	k7c7
ním	on	k3xPp3gMnSc7
nemohla	moct	k5eNaImAgFnS
plánovat	plánovat	k5eAaImF
společnou	společný	k2eAgFnSc4d1
budoucnost	budoucnost	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
budou	být	k5eAaImBp3nP
jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
on	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
věřit	věřit	k5eAaImF
v	v	k7c4
Ježíše	Ježíš	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finn	Finn	k1gMnSc1
souhlasí	souhlasit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
budou	být	k5eAaImBp3nP
vychováni	vychovat	k5eAaPmNgMnP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
si	se	k3xPyFc3
vybrala	vybrat	k5eAaPmAgFnS
a	a	k8xC
tak	tak	k6eAd1
pro	pro	k7c4
potvrzení	potvrzení	k1gNnSc4
její	její	k3xOp3gFnSc2
důvěry	důvěra	k1gFnSc2
a	a	k8xC
uznání	uznání	k1gNnSc2
mu	on	k3xPp3gMnSc3
dovolí	dovolit	k5eAaPmIp3nS
<g/>
,	,	kIx,
dotknout	dotknout	k5eAaPmF
se	se	k3xPyFc4
jejích	její	k3xOp3gNnPc2
ňader	ňadro	k1gNnPc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
mazlí	mazlit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
je	být	k5eAaImIp3nS
Finn	Finn	k1gInSc1
znovu	znovu	k6eAd1
quaterbackem	quaterback	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
stane	stanout	k5eAaPmIp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
nahradí	nahradit	k5eAaPmIp3nS
Sama	sám	k3xTgFnSc1
Evanse	Evanse	k1gFnSc1
(	(	kIx(
<g/>
Chord	chorda	k1gFnPc2
Overstreet	Overstreet	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
během	během	k7c2
hry	hra	k1gFnSc2
vykloubil	vykloubit	k5eAaPmAgMnS
rameno	rameno	k1gNnSc4
a	a	k8xC
nemůže	moct	k5eNaImIp3nS
hrát	hrát	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finn	Finn	k1gMnSc1
se	se	k3xPyFc4
cítí	cítit	k5eAaImIp3nS
zodpovědně	zodpovědně	k6eAd1
a	a	k8xC
přizná	přiznat	k5eAaPmIp3nS
Emmě	Emma	k1gFnSc6
svou	svůj	k3xOyFgFnSc4
vinu	vina	k1gFnSc4
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
mu	on	k3xPp3gMnSc3
ale	ale	k9
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1
komunikovat	komunikovat	k5eAaImF
s	s	k7c7
Bohem	bůh	k1gMnSc7
konkrétně	konkrétně	k6eAd1
pomocí	pomoc	k1gFnPc2
sýrového	sýrový	k2eAgInSc2d1
sendviče	sendvič	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklíčený	sklíčený	k2eAgInSc1d1
Finn	Finn	k1gInSc1
pochybuje	pochybovat	k5eAaImIp3nS
o	o	k7c6
své	svůj	k3xOyFgFnSc6
nově	nově	k6eAd1
nalezené	nalezený	k2eAgFnSc6d1
víře	víra	k1gFnSc6
a	a	k8xC
zpívá	zpívat	k5eAaImIp3nS
"	"	kIx"
<g/>
Losing	Losing	k1gInSc1
My	my	k3xPp1nPc1
Religion	religion	k1gInSc4
<g/>
"	"	kIx"
od	od	k7c2
R.	R.	kA
<g/>
E.	E.	kA
<g/>
M.	M.	kA
<g/>
.	.	kIx.
</s>
<s>
Burt	Burt	k1gInSc1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
v	v	k7c6
bezvědomí	bezvědomí	k1gNnSc6
a	a	k8xC
Kurt	kurt	k1gInSc1
mu	on	k3xPp3gMnSc3
u	u	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
lůžka	lůžko	k1gNnSc2
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
cítí	cítit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
přijmout	přijmout	k5eAaPmF
modlitby	modlitba	k1gFnPc4
od	od	k7c2
jeho	jeho	k3xOp3gMnPc2
přátel	přítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Kurt	Kurt	k1gMnSc1
pláče	plakat	k5eAaImIp3nS
<g/>
,	,	kIx,
začíná	začínat	k5eAaImIp3nS
Burt	Burt	k1gMnSc1
nabírat	nabírat	k5eAaImF
vědomí	vědomí	k1gNnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
zmáčkout	zmáčkout	k5eAaPmF,k5eAaImF
ruku	ruka	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
syna	syn	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
Sue	Sue	k1gMnSc1
navštěvuje	navštěvovat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
sestru	sestra	k1gFnSc4
Jean	Jean	k1gMnSc1
v	v	k7c6
jejím	její	k3xOp3gInSc6
domě	dům	k1gInSc6
a	a	k8xC
spolu	spolu	k6eAd1
si	se	k3xPyFc3
povídají	povídat	k5eAaImIp3nP
o	o	k7c6
Bohu	bůh	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jean	Jean	k1gMnSc1
požádá	požádat	k5eAaPmIp3nS
Sue	Sue	k1gFnSc4
<g/>
,	,	kIx,
jestli	jestli	k8xS
se	se	k3xPyFc4
za	za	k7c7
ní	on	k3xPp3gFnSc7
může	moct	k5eAaImIp3nS
modlit	modlit	k5eAaImF
a	a	k8xC
Sue	Sue	k1gFnSc4
přijímá	přijímat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
celý	celý	k2eAgInSc1d1
sbor	sbor	k1gInSc1
zpívá	zpívat	k5eAaImIp3nS
"	"	kIx"
<g/>
One	One	k1gFnSc1
of	of	k?
Us	Us	k1gFnSc1
<g/>
"	"	kIx"
od	od	k7c2
Joan	Joana	k1gFnPc2
Osborne	Osborn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sue	Sue	k1gMnPc6
sleduje	sledovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnPc4
vystoupení	vystoupení	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
řekne	říct	k5eAaPmIp3nS
Willovi	Will	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
neumožní	umožnit	k5eNaPmIp3nS
už	už	k9
další	další	k2eAgFnSc4d1
náboženskou	náboženský	k2eAgFnSc4d1
píseň	píseň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doma	doma	k6eAd1
Finn	Finn	k1gMnSc1
sní	sníst	k5eAaPmIp3nS,k5eAaImIp3nS
zbytek	zbytek	k1gInSc1
svého	svůj	k3xOyFgInSc2
grilovaného	grilovaný	k2eAgInSc2d1
sýrového	sýrový	k2eAgInSc2d1
sendviče	sendvič	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
písní	píseň	k1gFnPc2
</s>
<s>
"	"	kIx"
<g/>
Only	Onla	k1gFnPc1
the	the	k?
Good	Good	k1gMnSc1
Die	Die	k1gMnSc1
Young	Young	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
I	i	k8xC
Look	Look	k1gMnSc1
to	ten	k3xDgNnSc4
You	You	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Papa	papa	k1gMnSc1
<g/>
,	,	kIx,
Can	Can	k1gMnSc1
You	You	k1gFnSc2
Hear	Hear	k1gMnSc1
Me	Me	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
I	i	k8xC
Want	Want	k1gMnSc1
to	ten	k3xDgNnSc4
Hold	hold	k1gInSc1
Your	Your	k1gMnSc1
Hand	Hand	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Losing	Losing	k1gInSc1
My	my	k3xPp1nPc1
Religion	religion	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Bridge	Bridg	k1gMnSc2
over	over	k1gMnSc1
Troubled	Troubled	k1gMnSc1
Water	Water	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
One	One	k1gFnPc3
of	of	k?
Us	Us	k1gFnSc4
<g/>
"	"	kIx"
</s>
<s>
Hrají	hrát	k5eAaImIp3nP
</s>
<s>
Dianna	Dianna	k1gFnSc1
Agron	Agrona	k1gFnPc2
</s>
<s>
Quinn	Quinn	k1gMnSc1
Fabray	Fabraa	k1gFnSc2
</s>
<s>
Chris	Chris	k1gFnSc1
Colfer	Colfra	k1gFnPc2
</s>
<s>
Kurt	Kurt	k1gMnSc1
Hummel	Hummel	k1gMnSc1
</s>
<s>
Jane	Jan	k1gMnSc5
Lynch	Lynch	k1gMnSc1
</s>
<s>
Sue	Sue	k?
Sylvester	Sylvester	k1gInSc1
</s>
<s>
Jayma	Jayma	k1gFnSc1
Mays	Maysa	k1gFnPc2
</s>
<s>
Emma	Emma	k1gFnSc1
Pillsburry	Pillsburra	k1gFnSc2
</s>
<s>
Kevin	Kevin	k1gMnSc1
McHale	McHala	k1gFnSc3
</s>
<s>
Artie	Artie	k1gFnSc1
Abrams	Abramsa	k1gFnPc2
</s>
<s>
Lea	Lea	k1gFnSc1
Michele	Michel	k1gInSc2
</s>
<s>
Rachel	Rachel	k1gInSc1
Berry	Berra	k1gFnSc2
</s>
<s>
Cory	Cora	k1gFnPc1
Monteith	Monteitha	k1gFnPc2
</s>
<s>
Finn	Finn	k1gMnSc1
Hudson	Hudson	k1gMnSc1
</s>
<s>
Heather	Heathra	k1gFnPc2
Morris	Morris	k1gFnPc2
</s>
<s>
Brittany	Brittan	k1gInPc1
Pierce	Pierec	k1gInSc2
</s>
<s>
Matthew	Matthew	k?
Morrison	Morrison	k1gInSc1
</s>
<s>
William	William	k6eAd1
Schuester	Schuester	k1gInSc1
</s>
<s>
Mike	Mike	k1gFnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Malley	Malle	k2eAgInPc4d1
</s>
<s>
Burt	Burt	k2eAgInSc1d1
Hummel	Hummel	k1gInSc1
</s>
<s>
Amber	ambra	k1gFnPc2
Riley	Rilea	k1gFnSc2
</s>
<s>
Mercedes	mercedes	k1gInSc1
Jones	Jonesa	k1gFnPc2
</s>
<s>
Naya	Nay	k2eAgFnSc1d1
Rivera	Rivera	k1gFnSc1
</s>
<s>
Santana	Santana	k1gFnSc1
Lopez	Lopeza	k1gFnPc2
</s>
<s>
Mark	Mark	k1gMnSc1
Salling	Salling	k1gInSc4
</s>
<s>
Noah	Noah	k1gMnSc1
„	„	k?
<g/>
Puck	Puck	k1gMnSc1
<g/>
“	“	k?
Puckerman	Puckerman	k1gMnSc1
</s>
<s>
Jenna	Jenna	k1gFnSc1
Ushkowitz	Ushkowitza	k1gFnPc2
</s>
<s>
Tina	Tina	k1gFnSc1
Cohen-Chang	Cohen-Changa	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Grilled	Grilled	k1gMnSc1
Cheesus	Cheesus	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Boží	boží	k2eAgInSc1d1
sendvič	sendvič	k1gInSc1
na	na	k7c4
Fox	fox	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Boží	boží	k2eAgInSc1d1
sendvič	sendvič	k1gInSc1
na	na	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
</s>
<s>
Boží	boží	k2eAgInSc1d1
sendvič	sendvič	k1gInSc1
na	na	k7c4
TV	TV	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
