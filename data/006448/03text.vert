<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
pocházející	pocházející	k2eAgFnSc4d1	pocházející
z	z	k7c2	z
hebrejského	hebrejský	k2eAgNnSc2d1	hebrejské
jména	jméno	k1gNnSc2	jméno
ש	ש	k?	ש
<g/>
,	,	kIx,	,
Šošana	Šošana	k1gFnSc1	Šošana
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
lilie	lilie	k1gFnSc1	lilie
(	(	kIx(	(
<g/>
původem	původ	k1gInSc7	původ
možná	možná	k9	možná
z	z	k7c2	z
egyptského	egyptský	k2eAgInSc2d1	egyptský
sšn	sšn	k?	sšn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
"	"	kIx"	"
<g/>
růže	růže	k1gFnSc1	růže
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
občanského	občanský	k2eAgInSc2d1	občanský
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
svátek	svátek	k1gInSc1	svátek
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Varianty	variant	k1gInPc1	variant
jsou	být	k5eAaImIp3nP	být
Suzana	Suzan	k1gMnSc4	Suzan
<g/>
,	,	kIx,	,
Susana	Susan	k1gMnSc4	Susan
<g/>
,	,	kIx,	,
Zusana	Zusan	k1gMnSc4	Zusan
<g/>
,	,	kIx,	,
Zuzanna	Zuzann	k1gMnSc4	Zuzann
<g/>
,	,	kIx,	,
Susanna	Susann	k1gMnSc4	Susann
<g/>
,	,	kIx,	,
Suzanna	Suzann	k1gMnSc4	Suzann
a	a	k8xC	a
Shoshana	Shoshan	k1gMnSc4	Shoshan
<g/>
.	.	kIx.	.
</s>
<s>
Domáckými	domácký	k2eAgFnPc7d1	domácká
variantami	varianta	k1gFnPc7	varianta
jsou	být	k5eAaImIp3nP	být
Zuzka	Zuzka	k1gFnSc1	Zuzka
<g/>
,	,	kIx,	,
Zuzča	Zuzča	k1gFnSc1	Zuzča
<g/>
,	,	kIx,	,
Zuzi	Zuzi	k?	Zuzi
<g/>
,	,	kIx,	,
Zůza	Zůza	k1gMnSc1	Zůza
<g/>
,	,	kIx,	,
Zuzík	Zuzík	k1gMnSc1	Zuzík
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Zuzanka	Zuzanka	k1gFnSc1	Zuzanka
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaImF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+2,2	+2,2	k4	+2,2
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ČSÚ	ČSÚ	kA	ČSÚ
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
17	[number]	k4	17
<g/>
.	.	kIx.	.
nejčastější	častý	k2eAgNnSc4d3	nejčastější
ženské	ženský	k2eAgNnSc4d1	ženské
jméno	jméno	k1gNnSc4	jméno
mezi	mezi	k7c7	mezi
novorozenci	novorozenec	k1gMnPc7	novorozenec
<g/>
.	.	kIx.	.
svatá	svatý	k2eAgFnSc1d1	svatá
Zuzana	Zuzana	k1gFnSc1	Zuzana
–	–	k?	–
též	též	k9	též
svatá	svatý	k2eAgFnSc1d1	svatá
Zuzana	Zuzana	k1gFnSc1	Zuzana
Římská	římský	k2eAgFnSc1d1	římská
<g/>
,	,	kIx,	,
mučednice	mučednice	k1gFnSc1	mučednice
Zuzana	Zuzana	k1gFnSc1	Zuzana
–	–	k?	–
starozákonní	starozákonní	k2eAgFnSc1d1	starozákonní
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
Knihy	kniha	k1gFnSc2	kniha
Danielovy	Danielův	k2eAgFnSc2d1	Danielova
Šošana	Šošana	k1gFnSc1	Šošana
Arbeli-Almozlino	Arbeli-Almozlina	k1gFnSc5	Arbeli-Almozlina
–	–	k?	–
izraelská	izraelský	k2eAgFnSc1d1	izraelská
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
ministryně	ministryně	k1gFnSc1	ministryně
Susan	Susana	k1gFnPc2	Susana
Sarandon	Sarandon	k1gMnSc1	Sarandon
–	–	k?	–
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
Susanna	Susanna	k1gFnSc1	Susanna
Clarke	Clarke	k1gFnSc1	Clarke
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
americká	americký	k2eAgFnSc1d1	americká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Šošana	Šošan	k1gMnSc2	Šošan
Damari	Damar	k1gFnSc2	Damar
–	–	k?	–
izraelská	izraelský	k2eAgFnSc1d1	izraelská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Susanna	Susann	k1gInSc2	Susann
Hoffs	Hoffs	k1gInSc1	Hoffs
–	–	k?	–
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
členka	členka	k1gFnSc1	členka
kapely	kapela	k1gFnSc2	kapela
The	The	k1gMnSc1	The
Bangles	Bangles	k1gMnSc1	Bangles
Suzanne	Suzann	k1gInSc5	Suzann
Vega	Veg	k2eAgFnSc1d1	Vega
–	–	k?	–
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Suzi	Suz	k1gFnSc2	Suz
Quatro	Quatro	k1gNnSc1	Quatro
–	–	k?	–
rocková	rockový	k2eAgFnSc1d1	rocková
hudebnice	hudebnice	k1gFnSc1	hudebnice
Zuzana	Zuzana	k1gFnSc1	Zuzana
Brabcová	Brabcová	k1gFnSc1	Brabcová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Bubílková	Bubílková	k1gFnSc1	Bubílková
–	–	k?	–
česko-slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
novinářka	novinářka	k1gFnSc1	novinářka
a	a	k8xC	a
divadelnice	divadelnice	k1gFnSc1	divadelnice
Zuzana	Zuzana	k1gFnSc1	Zuzana
Burianová	Burianová	k1gFnSc1	Burianová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Bydžovská	Bydžovská	k1gFnSc1	Bydžovská
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
<g />
.	.	kIx.	.
</s>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Čížková	Čížková	k1gFnSc1	Čížková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
sochařka	sochařka	k1gFnSc1	sochařka
a	a	k8xC	a
malířka	malířka	k1gFnSc1	malířka
Šošana	Šošana	k1gFnSc1	Šošana
Kamin	Kamin	k1gInSc1	Kamin
-	-	kIx~	-
israelská	israelský	k2eAgFnSc1d1	israelská
politička	politička	k1gFnSc1	politička
Zuzana	Zuzana	k1gFnSc1	Zuzana
Kocúriková	Kocúrikový	k2eAgFnSc1d1	Kocúriková
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
herečka	herečka	k1gFnSc1	herečka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Koubková	Koubková	k1gFnSc1	Koubková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Kronerová	Kronerová	k1gFnSc1	Kronerová
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
herečka	herečka	k1gFnSc1	herečka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Lapčíková	Lapčíková	k1gFnSc1	Lapčíková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
hudebnice	hudebnice	k1gFnSc1	hudebnice
<g/>
,	,	kIx,	,
folkloristka	folkloristka	k1gFnSc1	folkloristka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Michnová	Michnová	k1gFnSc1	Michnová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Norisová	Norisový	k2eAgFnSc1d1	Norisová
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
slovensko-česká	slovensko-český	k2eAgFnSc1d1	slovensko-česká
herečka	herečka	k1gFnSc1	herečka
Šošana	Šošan	k1gMnSc2	Šošan
Netanjahu	Netanjah	k1gInSc2	Netanjah
-	-	kIx~	-
israelská	israelský	k2eAgFnSc1d1	israelská
právnička	právnička	k1gFnSc1	právnička
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
soudkyně	soudkyně	k1gFnSc1	soudkyně
israelského	israelský	k2eAgInSc2d1	israelský
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Zuzana	Zuzana	k1gFnSc1	Zuzana
Roithová	Roithová	k1gFnSc1	Roithová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
Zuzana	Zuzana	k1gFnSc1	Zuzana
Růžičková	Růžičková	k1gFnSc1	Růžičková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
hudebnice	hudebnice	k1gFnSc1	hudebnice
<g/>
,	,	kIx,	,
čembalistka	čembalistka	k1gFnSc1	čembalistka
a	a	k8xC	a
klavíristka	klavíristka	k1gFnSc1	klavíristka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Stivínová	Stivínová	k1gFnSc1	Stivínová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
ženy	žena	k1gFnPc1	žena
téhož	týž	k3xTgNnSc2	týž
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc2	příjmení
<g/>
,	,	kIx,	,
teta	teta	k1gFnSc1	teta
a	a	k8xC	a
neteř	neteř	k1gFnSc1	neteř
Zuzana	Zuzana	k1gFnSc1	Zuzana
Tvarůžková	tvarůžkový	k2eAgFnSc1d1	Tvarůžková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
novinářka	novinářka	k1gFnSc1	novinářka
<g/>
,	,	kIx,	,
redaktorka	redaktorka	k1gFnSc1	redaktorka
ČT	ČT	kA	ČT
Zuzana	Zuzana	k1gFnSc1	Zuzana
Vejvodová	Vejvodová	k1gFnSc1	Vejvodová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Shoshannah	Shoshannaha	k1gFnPc2	Shoshannaha
Stern	sternum	k1gNnPc2	sternum
–	–	k?	–
americká	americký	k2eAgFnSc1d1	americká
neslyšící	slyšící	k2eNgFnSc1d1	neslyšící
herečka	herečka	k1gFnSc1	herečka
Šošana	Šošana	k1gFnSc1	Šošana
Parsic	Parsic	k1gMnSc1	Parsic
–	–	k?	–
israelská	israelský	k2eAgFnSc1d1	israelská
politička	politička	k1gFnSc1	politička
Shosanna	Shosann	k1gInSc2	Shosann
Dreyfuss	Dreyfuss	k1gInSc1	Dreyfuss
–	–	k?	–
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
válečné	válečný	k2eAgFnSc2d1	válečná
pohádky	pohádka	k1gFnSc2	pohádka
Hanebný	hanebný	k2eAgMnSc1d1	hanebný
pancharti	panchart	k1gMnPc1	panchart
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
ji	on	k3xPp3gFnSc4	on
francouzská	francouzský	k2eAgFnSc1d1	francouzská
herečka	herečka	k1gFnSc1	herečka
Mélanie	Mélanie	k1gFnSc2	Mélanie
Laurent	Laurent	k1gInSc1	Laurent
<g/>
.	.	kIx.	.
</s>
<s>
Susan	Susan	k1gMnSc1	Susan
Pevensie	Pevensie	k1gFnSc2	Pevensie
–	–	k?	–
postava	postava	k1gFnSc1	postava
ze	z	k7c2	z
série	série	k1gFnSc2	série
Letopisů	letopis	k1gInPc2	letopis
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Hrály	hrát	k5eAaImAgFnP	hrát
ji	on	k3xPp3gFnSc4	on
britské	britský	k2eAgFnPc1d1	britská
herečky	herečka	k1gFnPc1	herečka
Anna	Anna	k1gFnSc1	Anna
Popplewell	Popplewell	k1gMnSc1	Popplewell
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sophie	Sophie	k1gFnSc1	Sophie
Wilcox	Wilcox	k1gInSc1	Wilcox
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sophie	Sophie	k1gFnSc1	Sophie
Cook	Cooka	k1gFnPc2	Cooka
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Susan	Susan	k1gMnSc1	Susan
Delgado	Delgada	k1gFnSc5	Delgada
–	–	k?	–
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
knižní	knižní	k2eAgFnSc2d1	knižní
série	série	k1gFnSc2	série
Temná	temnat	k5eAaImIp3nS	temnat
Věž	věž	k1gFnSc1	věž
Stephena	Stephena	k1gFnSc1	Stephena
Kinga	Kinga	k1gFnSc1	Kinga
<g/>
.	.	kIx.	.
</s>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Stohelitská	Stohelitský	k2eAgFnSc1d1	Stohelitská
–	–	k?	–
postava	postava	k1gFnSc1	postava
ze	z	k7c2	z
série	série	k1gFnSc2	série
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
Terryho	Terry	k1gMnSc2	Terry
Pratchetta	Pratchett	k1gMnSc2	Pratchett
<g/>
.	.	kIx.	.
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Suzanne	Suzann	k1gMnSc5	Suzann
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
<g/>
:	:	kIx,	:
Susann	Susann	k1gMnSc1	Susann
<g/>
,	,	kIx,	,
Susan	Susan	k1gMnSc1	Susan
<g/>
,	,	kIx,	,
Sue	Sue	k1gMnSc1	Sue
<g/>
,	,	kIx,	,
Su	Su	k?	Su
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
س	س	k?	س
(	(	kIx(	(
<g/>
Susan	Susan	k1gMnSc1	Susan
<g/>
)	)	kIx)	)
aramejsky	aramejsky	k6eAd1	aramejsky
<g/>
:	:	kIx,	:
Šušan	Šušan	k1gInSc1	Šušan
<g/>
,	,	kIx,	,
Šušaneh	Šušaneh	k1gInSc1	Šušaneh
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g />
.	.	kIx.	.
</s>
<s>
dánsky	dánsky	k6eAd1	dánsky
<g/>
:	:	kIx,	:
Suzanne	Suzann	k1gInSc5	Suzann
estonsky	estonsky	k6eAd1	estonsky
<g/>
:	:	kIx,	:
Susamma	Susamma	k1gNnSc4	Susamma
finsky	finsky	k6eAd1	finsky
<g/>
:	:	kIx,	:
Susanna	Susanna	k1gFnSc1	Susanna
<g/>
,	,	kIx,	,
Sanna	Sanna	k1gFnSc1	Sanna
<g/>
,	,	kIx,	,
Sanni	Sann	k1gMnPc1	Sann
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Suzanne	Suzann	k1gInSc5	Suzann
gruzínsky	gruzínsky	k6eAd1	gruzínsky
<g/>
:	:	kIx,	:
შ	შ	k?	შ
(	(	kIx(	(
<g/>
Šušanik	Šušanik	k1gMnSc1	Šušanik
<g/>
)	)	kIx)	)
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ש	ש	k?	ש
(	(	kIx(	(
<g/>
Šošana	Šošana	k1gFnSc1	Šošana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
krátce	krátce	k6eAd1	krátce
<g/>
:	:	kIx,	:
ש	ש	k?	ש
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Šoš	Šoš	k1gFnSc1	Šoš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ש	ש	k?	ש
(	(	kIx(	(
<g/>
Šoši	Šoš	k1gFnSc2	Šoš
<g/>
)	)	kIx)	)
hornolužicky	hornolužicky	k6eAd1	hornolužicky
<g/>
:	:	kIx,	:
Zusana	Zusana	k1gFnSc1	Zusana
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
:	:	kIx,	:
Suzana	Suzana	k1gFnSc1	Suzana
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Susanna	Susanna	k1gFnSc1	Susanna
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
ス	ス	k?	ス
(	(	kIx(	(
<g/>
Susan	Susan	k1gMnSc1	Susan
<g/>
)	)	kIx)	)
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Xuxa	Xuxa	k1gFnSc1	Xuxa
litevsky	litevsky	k6eAd1	litevsky
<g/>
:	:	kIx,	:
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
,	,	kIx,	,
Zuzane	Zuzan	k1gMnSc5	Zuzan
<g/>
,	,	kIx,	,
Zuze	Zuza	k1gFnSc3	Zuza
<g/>
,	,	kIx,	,
Zune	Zune	k1gNnSc3	Zune
<g />
.	.	kIx.	.
</s>
<s>
lotyšsky	lotyšsky	k6eAd1	lotyšsky
<g/>
:	:	kIx,	:
Zuzanna	Zuzanna	k1gFnSc1	Zuzanna
<g/>
,	,	kIx,	,
Zane	Zane	k1gFnSc1	Zane
<g/>
,	,	kIx,	,
Zuze	Zuza	k1gFnSc6	Zuza
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Zsuzsanna	Zsuzsanna	k1gFnSc1	Zsuzsanna
malajsky	malajsky	k6eAd1	malajsky
<g/>
:	:	kIx,	:
Sosamma	Sosamma	k1gNnSc4	Sosamma
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Susanne	Susann	k1gMnSc5	Susann
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
<g/>
:	:	kIx,	:
Suse	Suse	k1gNnSc7	Suse
<g/>
,	,	kIx,	,
Susi	Susi	k1gNnSc7	Susi
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
:	:	kIx,	:
Susana	Susana	k1gFnSc1	Susana
norsky	norsky	k6eAd1	norsky
<g/>
:	:	kIx,	:
Susanne	Susann	k1gInSc5	Susann
persky	persky	k6eAd1	persky
<g/>
:	:	kIx,	:
س	س	k?	س
(	(	kIx(	(
<g/>
Zuzan	Zuzana	k1gFnPc2	Zuzana
<g/>
)	)	kIx)	)
polsky	polsky	k6eAd1	polsky
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Zuzanna	Zuzanna	k1gFnSc1	Zuzanna
<g/>
,	,	kIx,	,
Zuzia	Zuzia	k1gFnSc1	Zuzia
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
:	:	kIx,	:
Susana	Susana	k1gFnSc1	Susana
<g/>
,	,	kIx,	,
Susianna	Susianen	k2eAgFnSc1d1	Susianen
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
С	С	k?	С
(	(	kIx(	(
<g/>
Sjusana	Sjusan	k1gMnSc2	Sjusan
<g/>
)	)	kIx)	)
slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
<g/>
Zuzana	Zuzana	k1gFnSc1	Zuzana
srbochorvatsky	srbochorvatsky	k6eAd1	srbochorvatsky
<g/>
:	:	kIx,	:
С	С	k?	С
(	(	kIx(	(
<g/>
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
)	)	kIx)	)
srbsky	srbsky	k6eAd1	srbsky
<g/>
:	:	kIx,	:
С	С	k?	С
<g/>
,	,	kIx,	,
С	С	k?	С
(	(	kIx(	(
<g/>
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Žužana	Žužana	k1gFnSc1	Žužana
<g/>
,	,	kIx,	,
Ж	Ж	k?	Ж
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Susana	Susana	k1gFnSc1	Susana
<g/>
,	,	kIx,	,
Susianna	Susianen	k2eAgFnSc1d1	Susianen
švédsky	švédsky	k6eAd1	švédsky
<g/>
:	:	kIx,	:
Susanna	Susanna	k1gFnSc1	Susanna
turecky	turecky	k6eAd1	turecky
<g/>
:	:	kIx,	:
Suzan	Suzan	k1gInSc1	Suzan
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
:	:	kIx,	:
С	С	k?	С
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Zuzana	Zuzana	k1gFnSc1	Zuzana
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Četnost	četnost	k1gFnSc4	četnost
jmen	jméno	k1gNnPc2	jméno
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
MV	MV	kA	MV
ČR	ČR	kA	ČR
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
"	"	kIx"	"
</s>
