<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
pocházející	pocházející	k2eAgFnSc4d1	pocházející
z	z	k7c2	z
hebrejského	hebrejský	k2eAgNnSc2d1	hebrejské
jména	jméno	k1gNnSc2	jméno
ש	ש	k?	ש
<g/>
,	,	kIx,	,
Šošana	Šošana	k1gFnSc1	Šošana
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
lilie	lilie	k1gFnSc1	lilie
(	(	kIx(	(
<g/>
původem	původ	k1gInSc7	původ
možná	možná	k9	možná
z	z	k7c2	z
egyptského	egyptský	k2eAgInSc2d1	egyptský
sšn	sšn	k?	sšn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
"	"	kIx"	"
<g/>
růže	růže	k1gFnSc1	růže
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
