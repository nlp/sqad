<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
85	[number]	k4	85
<g/>
.	.	kIx.	.
sezónou	sezóna	k1gFnSc7	sezóna
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
hokejové	hokejový	k2eAgFnSc2d1	hokejová
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
ročníku	ročník	k1gInSc2	ročník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
tým	tým	k1gInSc1	tým
EC	EC	kA	EC
Red	Red	k1gMnPc2	Red
Bull	bulla	k1gFnPc2	bulla
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herní	herní	k2eAgInSc1d1	herní
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
čtyřkolově	čtyřkolově	k6eAd1	čtyřkolově
každý	každý	k3xTgInSc1	každý
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
(	(	kIx(	(
<g/>
celkem	celek	k1gInSc7	celek
44	[number]	k4	44
kol	kolo	k1gNnPc2	kolo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
nadstavbovou	nadstavbový	k2eAgFnSc7d1	nadstavbová
částí	část	k1gFnSc7	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
týmy	tým	k1gInPc1	tým
rozdělily	rozdělit	k5eAaPmAgInP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
A	A	kA	A
hrálo	hrát	k5eAaImAgNnS	hrát
6	[number]	k4	6
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
týmů	tým	k1gInPc2	tým
a	a	k8xC	a
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
B	B	kA	B
zbylých	zbylý	k2eAgNnPc2d1	zbylé
6	[number]	k4	6
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc1	bod
se	se	k3xPyFc4	se
do	do	k7c2	do
nadstavbových	nadstavbový	k2eAgFnPc2d1	nadstavbová
skupin	skupina	k1gFnPc2	skupina
nepřenášely	přenášet	k5eNaImAgFnP	přenášet
<g/>
,	,	kIx,	,
týmy	tým	k1gInPc1	tým
pouze	pouze	k6eAd1	pouze
dostaly	dostat	k5eAaPmAgInP	dostat
bonusové	bonusový	k2eAgInPc1d1	bonusový
body	bod	k1gInPc1	bod
za	za	k7c4	za
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
A	a	k9	a
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
týmy	tým	k1gInPc4	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
postoupily	postoupit	k5eAaPmAgInP	postoupit
do	do	k7c2	do
playoff	playoff	k1gInSc1	playoff
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
na	na	k7c4	na
4	[number]	k4	4
vítězné	vítězný	k2eAgInPc4d1	vítězný
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinálové	čtvrtfinálový	k2eAgFnPc1d1	čtvrtfinálová
dvojice	dvojice	k1gFnPc1	dvojice
nebyly	být	k5eNaImAgFnP	být
pevně	pevně	k6eAd1	pevně
dány	dát	k5eAaPmNgFnP	dát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vítěz	vítěz	k1gMnSc1	vítěz
nadstavbové	nadstavbový	k2eAgFnSc2d1	nadstavbová
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
vybrat	vybrat	k5eAaPmF	vybrat
z	z	k7c2	z
týmů	tým	k1gInPc2	tým
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
tým	tým	k1gInSc4	tým
volil	volit	k5eAaImAgMnS	volit
ze	z	k7c2	z
zbylých	zbylý	k2eAgInPc2d1	zbylý
týmů	tým	k1gInPc2	tým
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Bodovalo	bodovat	k5eAaImAgNnS	bodovat
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
NHL	NHL	kA	NHL
-	-	kIx~	-
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
obdržel	obdržet	k5eAaPmAgMnS	obdržet
vítěz	vítěz	k1gMnSc1	vítěz
2	[number]	k4	2
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
nebo	nebo	k8xC	nebo
na	na	k7c4	na
nájezdy	nájezd	k1gInPc4	nájezd
1	[number]	k4	1
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
čase	čas	k1gInSc6	čas
pak	pak	k6eAd1	pak
0	[number]	k4	0
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
===	===	k?	===
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc4	výsledek
z	z	k7c2	z
první	první	k4xOgFnSc2	první
fáze	fáze	k1gFnSc2	fáze
se	se	k3xPyFc4	se
do	do	k7c2	do
nadstavby	nadstavba	k1gFnSc2	nadstavba
nezapočítávaly	započítávat	k5eNaImAgInP	započítávat
<g/>
,	,	kIx,	,
týmy	tým	k1gInPc1	tým
si	se	k3xPyFc3	se
pouze	pouze	k6eAd1	pouze
odnesly	odnést	k5eAaPmAgFnP	odnést
bonusové	bonusový	k2eAgInPc4d1	bonusový
body	bod	k1gInPc4	bod
za	za	k7c4	za
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
(	(	kIx(	(
<g/>
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Skupina	skupina	k1gFnSc1	skupina
A	a	k9	a
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Play	play	k0	play
off	off	k?	off
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
===	===	k?	===
</s>
</p>
<p>
<s>
EC	EC	kA	EC
Red	Red	k1gFnSc1	Red
Bull	bulla	k1gFnPc2	bulla
Salzburg	Salzburg	k1gInSc1	Salzburg
-	-	kIx~	-
EC	EC	kA	EC
VSV	VSV	kA	VSV
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
PP	PP	kA	PP
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
PP	PP	kA	PP
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
EHC	EHC	kA	EHC
Black	Black	k1gInSc1	Black
Wings	Wings	k1gInSc1	Wings
Linz	Linz	k1gInSc1	Linz
-	-	kIx~	-
HC	HC	kA	HC
Bolzano	Bolzana	k1gFnSc5	Bolzana
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Orli	orel	k1gMnPc1	orel
Znojmo	Znojmo	k1gNnSc1	Znojmo
-	-	kIx~	-
EC	EC	kA	EC
KAC	KAC	kA	KAC
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
PP	PP	kA	PP
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
PP	PP	kA	PP
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
PP	PP	kA	PP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sapa	sapa	k1gFnSc1	sapa
Fehérvár	Fehérvár	k1gInSc1	Fehérvár
AV19	AV19	k1gFnSc1	AV19
-	-	kIx~	-
Vienna	Vienna	k1gFnSc1	Vienna
Capitals	Capitals	k1gInSc1	Capitals
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
PP	PP	kA	PP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
PP	PP	kA	PP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Semifinále	semifinále	k1gNnPc3	semifinále
===	===	k?	===
</s>
</p>
<p>
<s>
EC	EC	kA	EC
Red	Red	k1gFnSc1	Red
Bull	bulla	k1gFnPc2	bulla
Salzburg	Salzburg	k1gInSc1	Salzburg
-	-	kIx~	-
EC	EC	kA	EC
KAC	KAC	kA	KAC
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
PP	PP	kA	PP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
EHC	EHC	kA	EHC
Black	Black	k1gInSc1	Black
Wings	Wings	k1gInSc1	Wings
Linz	Linz	k1gInSc1	Linz
-	-	kIx~	-
Vienna	Vienna	k1gFnSc1	Vienna
Capitals	Capitals	k1gInSc1	Capitals
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
PP	PP	kA	PP
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
PP	PP	kA	PP
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
PP	PP	kA	PP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Finále	finále	k1gNnSc1	finále
===	===	k?	===
</s>
</p>
<p>
<s>
EC	EC	kA	EC
Red	Red	k1gFnSc1	Red
Bull	bulla	k1gFnPc2	bulla
Salzburg	Salzburg	k1gInSc1	Salzburg
-	-	kIx~	-
Vienna	Vienna	k1gFnSc1	Vienna
Capitals	Capitals	k1gInSc1	Capitals
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
ligy	liga	k1gFnSc2	liga
</s>
</p>
