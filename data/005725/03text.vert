<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
7	[number]	k4	7
a	a	k8xC	a
1	[number]	k4	1
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
mezi	mezi	k7c7	mezi
29	[number]	k4	29
a	a	k8xC	a
36	[number]	k4	36
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
Ježíš	Ježíš	k1gMnSc1	Ježíš
Nazaretský	nazaretský	k2eAgMnSc1d1	nazaretský
či	či	k8xC	či
Ježíš	Ježíš	k1gMnSc1	Ježíš
z	z	k7c2	z
Nazareta	Nazaret	k1gInSc2	Nazaret
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
veřejně	veřejně	k6eAd1	veřejně
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
pocestný	pocestný	k1gMnSc1	pocestný
kazatel	kazatel	k1gMnSc1	kazatel
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
.	.	kIx.	.
</s>
<s>
Hlásal	hlásat	k5eAaImAgInS	hlásat
brzký	brzký	k2eAgInSc1d1	brzký
příchod	příchod	k1gInSc1	příchod
Božího	boží	k2eAgNnSc2d1	boží
království	království	k1gNnSc2	království
a	a	k8xC	a
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
k	k	k7c3	k
obrácení	obrácení	k1gNnSc3	obrácení
či	či	k8xC	či
pokání	pokání	k1gNnSc3	pokání
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
30	[number]	k4	30
jej	on	k3xPp3gInSc4	on
Římané	Říman	k1gMnPc1	Říman
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
ukřižovali	ukřižovat	k5eAaPmAgMnP	ukřižovat
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
považují	považovat	k5eAaImIp3nP	považovat
Ježíše	Ježíš	k1gMnPc4	Ježíš
za	za	k7c4	za
Židy	Žid	k1gMnPc4	Žid
očekávaného	očekávaný	k2eAgMnSc2d1	očekávaný
Mesiáše	Mesiáš	k1gMnSc2	Mesiáš
<g/>
,	,	kIx,	,
Spasitele	spasitel	k1gMnSc2	spasitel
lidstva	lidstvo	k1gNnSc2	lidstvo
a	a	k8xC	a
Božího	boží	k2eAgMnSc2d1	boží
Syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
vyznavači	vyznavač	k1gMnPc1	vyznavač
islámu	islám	k1gInSc2	islám
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
proroků	prorok	k1gMnPc2	prorok
<g/>
.	.	kIx.	.
</s>
<s>
Kritickým	kritický	k2eAgNnSc7d1	kritické
historickým	historický	k2eAgNnSc7d1	historické
bádáním	bádání	k1gNnSc7	bádání
došlo	dojít	k5eAaPmAgNnS	dojít
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
odlišení	odlišení	k1gNnSc3	odlišení
náboženské	náboženský	k2eAgFnSc2d1	náboženská
postavy	postava	k1gFnSc2	postava
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
historické	historický	k2eAgFnSc2d1	historická
osoby	osoba	k1gFnSc2	osoba
Ježíše	Ježíš	k1gMnSc2	Ježíš
Nazaretského	nazaretský	k2eAgMnSc2d1	nazaretský
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nepřesně	přesně	k6eNd1	přesně
vypočítaného	vypočítaný	k2eAgNnSc2d1	vypočítané
data	datum	k1gNnSc2	datum
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
narození	narození	k1gNnSc2	narození
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
léta	léto	k1gNnSc2	léto
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Ježíš	ježit	k5eAaImIp2nS	ježit
(	(	kIx(	(
<g/>
jméno	jméno	k1gNnSc1	jméno
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Ježíš	ježit	k5eAaImIp2nS	ježit
je	on	k3xPp3gFnPc4	on
česká	český	k2eAgFnSc1d1	Česká
podoba	podoba	k1gFnSc1	podoba
řeckého	řecký	k2eAgInSc2d1	řecký
Ί	Ί	k?	Ί
(	(	kIx(	(
<g/>
Iésús	Iésús	k1gInSc1	Iésús
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
helenizací	helenizace	k1gFnSc7	helenizace
vlastního	vlastní	k2eAgNnSc2d1	vlastní
jména	jméno	k1gNnSc2	jméno
י	י	k?	י
<g/>
ְ	ְ	k?	ְ
<g/>
ה	ה	k?	ה
<g/>
ֹ	ֹ	k?	ֹ
<g/>
ש	ש	k?	ש
<g/>
ֻ	ֻ	k?	ֻ
<g/>
ׁ	ׁ	k?	ׁ
<g/>
ע	ע	k?	ע
(	(	kIx(	(
<g/>
Jehošua	Jehošua	k1gMnSc1	Jehošua
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
staženého	stažený	k2eAgInSc2d1	stažený
tvaru	tvar	k1gInSc2	tvar
י	י	k?	י
<g/>
ְ	ְ	k?	ְ
<g/>
ש	ש	k?	ש
<g/>
ׁ	ׁ	k?	ׁ
<g/>
ו	ו	k?	ו
<g/>
ּ	ּ	k?	ּ
<g/>
ע	ע	k?	ע
(	(	kIx(	(
<g/>
Ješua	Ješua	k1gMnSc1	Ješua
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
židovská	židovský	k2eAgFnSc1d1	židovská
forma	forma	k1gFnSc1	forma
jeho	jeho	k3xOp3gNnSc2	jeho
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přívlastkem	přívlastek	k1gInSc7	přívlastek
ha-Nocri	ha-Nocr	k1gFnSc2	ha-Nocr
tj.	tj.	kA	tj.
Nazaretský	nazaretský	k2eAgInSc5d1	nazaretský
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
י	י	k?	י
<g/>
ָ	ָ	k?	ָ
<g/>
ה	ה	k?	ה
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zkrácenou	zkrácený	k2eAgFnSc7d1	zkrácená
formou	forma	k1gFnSc7	forma
Božího	boží	k2eAgNnSc2d1	boží
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
JHVH	JHVH	kA	JHVH
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
sloveso	sloveso	k1gNnSc4	sloveso
י	י	k?	י
<g/>
ָ	ָ	k?	ָ
<g/>
ש	ש	k?	ש
<g/>
ַ	ַ	k?	ַ
<g/>
ע	ע	k?	ע
(	(	kIx(	(
<g/>
jáša	jáša	k1gMnSc1	jáša
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
pomáhat	pomáhat	k5eAaImF	pomáhat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zachraňovat	zachraňovat	k5eAaImF	zachraňovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
spasit	spasit	k5eAaPmF	spasit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
jméno	jméno	k1gNnSc1	jméno
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
záchrana	záchrana	k1gFnSc1	záchrana
přichází	přicházet	k5eAaImIp3nS	přicházet
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přesném	přesný	k2eAgInSc6d1	přesný
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
záchrana	záchrana	k1gFnSc1	záchrana
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přívlastek	přívlastek	k1gInSc1	přívlastek
"	"	kIx"	"
<g/>
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
χ	χ	k?	χ
christos	christos	k1gInSc1	christos
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
doslovným	doslovný	k2eAgInSc7d1	doslovný
překladem	překlad	k1gInSc7	překlad
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
מ	מ	k?	מ
<g/>
ָ	ָ	k?	ָ
<g/>
ש	ש	k?	ש
<g/>
ִ	ִ	k?	ִ
<g/>
ׁ	ׁ	k?	ׁ
<g/>
י	י	k?	י
<g/>
ַ	ַ	k?	ַ
(	(	kIx(	(
<g/>
mašíach	mašíach	k1gMnSc1	mašíach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
pomazaný	pomazaný	k2eAgMnSc1d1	pomazaný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
mesiáš	mesiáš	k1gMnSc1	mesiáš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
židy	žid	k1gMnPc4	žid
očekávaný	očekávaný	k2eAgMnSc1d1	očekávaný
vykupitel	vykupitel	k1gMnSc1	vykupitel
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Přívlastek	přívlastek	k1gInSc4	přívlastek
Nazaretský	nazaretský	k2eAgInSc4d1	nazaretský
se	se	k3xPyFc4	se
v	v	k7c6	v
řeckém	řecký	k2eAgInSc6d1	řecký
originále	originál	k1gInSc6	originál
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
Nazarénos	Nazarénosa	k1gFnPc2	Nazarénosa
a	a	k8xC	a
Nazóraios	Nazóraiosa	k1gFnPc2	Nazóraiosa
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
není	být	k5eNaImIp3nS	být
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
koncilní	koncilní	k2eAgFnSc2d1	koncilní
interpretace	interpretace	k1gFnSc2	interpretace
řecké	řecký	k2eAgFnSc2d1	řecká
přejímky	přejímka	k1gFnSc2	přejímka
Nazoraios	Nazoraiosa	k1gFnPc2	Nazoraiosa
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
Apo	Apo	k1gFnSc1	Apo
Nazaret	Nazaret	k1gInSc1	Nazaret
vychází	vycházet	k5eAaImIp3nS	vycházet
překlad	překlad	k1gInSc4	překlad
"	"	kIx"	"
<g/>
z	z	k7c2	z
Nazaretu	Nazaret	k1gInSc2	Nazaret
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
Nazoraios	Nazoraiosa	k1gFnPc2	Nazoraiosa
snad	snad	k9	snad
etymologicky	etymologicky	k6eAd1	etymologicky
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
aramejského	aramejský	k2eAgNnSc2d1	aramejské
Nasuraia	Nasuraium	k1gNnSc2	Nasuraium
(	(	kIx(	(
<g/>
aram	aram	k6eAd1	aram
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ten	ten	k3xDgInSc1	ten
Hledící	hledící	k2eAgInSc1d1	hledící
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Hlídající	hlídající	k2eAgMnSc1d1	hlídající
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označujícího	označující	k2eAgInSc2d1	označující
původně	původně	k6eAd1	původně
mystické	mystický	k2eAgMnPc4d1	mystický
či	či	k8xC	či
gnostické	gnostický	k2eAgMnPc4d1	gnostický
kazatele	kazatel	k1gMnPc4	kazatel
vysvěcené	vysvěcený	k2eAgFnSc2d1	vysvěcená
Janem	Jan	k1gMnSc7	Jan
Křtitelem	křtitel	k1gMnSc7	křtitel
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
výklad	výklad	k1gInSc1	výklad
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
původ	původ	k1gInSc4	původ
v	v	k7c6	v
nazírství	nazírství	k1gNnSc6	nazírství
<g/>
,	,	kIx,	,
zvláštním	zvláštní	k2eAgNnSc6d1	zvláštní
zasvěcení	zasvěcení	k1gNnSc6	zasvěcení
se	se	k3xPyFc4	se
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historický	historický	k2eAgMnSc1d1	historický
Ježíš	Ježíš	k1gMnSc1	Ježíš
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
historické	historický	k2eAgFnSc2d1	historická
osobnosti	osobnost	k1gFnSc2	osobnost
Ježíše	Ježíš	k1gMnSc2	Ježíš
Nazaretského	nazaretský	k2eAgNnSc2d1	nazaretské
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
textové	textový	k2eAgFnPc4d1	textová
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnPc4d1	historická
a	a	k8xC	a
literární	literární	k2eAgFnPc4d1	literární
kritiky	kritika	k1gFnPc4	kritika
biblických	biblický	k2eAgInPc2d1	biblický
a	a	k8xC	a
souvisejících	související	k2eAgInPc2d1	související
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
další	další	k2eAgInPc1d1	další
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
mimokřesťanské	mimokřesťanský	k2eAgInPc1d1	mimokřesťanský
texty	text	k1gInPc1	text
pocházejí	pocházet	k5eAaImIp3nP	pocházet
buď	buď	k8xC	buď
ze	z	k7c2	z
židovského	židovský	k2eAgMnSc2d1	židovský
(	(	kIx(	(
<g/>
Flavius	Flavius	k1gInSc1	Flavius
Iosephus	Iosephus	k1gInSc1	Iosephus
<g/>
,	,	kIx,	,
Talmud	talmud	k1gInSc1	talmud
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
z	z	k7c2	z
římského	římský	k2eAgNnSc2d1	římské
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
Plinius	Plinius	k1gMnSc1	Plinius
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
Tacitus	Tacitus	k1gMnSc1	Tacitus
<g/>
,	,	kIx,	,
Suetonius	Suetonius	k1gMnSc1	Suetonius
<g/>
)	)	kIx)	)
a	a	k8xC	a
biblická	biblický	k2eAgFnSc1d1	biblická
archeologie	archeologie	k1gFnSc1	archeologie
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
problémem	problém	k1gInSc7	problém
pro	pro	k7c4	pro
historické	historický	k2eAgNnSc4d1	historické
zkoumání	zkoumání	k1gNnSc4	zkoumání
Ježíšova	Ježíšův	k2eAgInSc2d1	Ježíšův
života	život	k1gInSc2	život
jsou	být	k5eAaImIp3nP	být
klíčové	klíčový	k2eAgInPc4d1	klíčový
písemné	písemný	k2eAgInPc4d1	písemný
prameny	pramen	k1gInPc4	pramen
(	(	kIx(	(
<g/>
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
s	s	k7c7	s
časovým	časový	k2eAgInSc7d1	časový
odstupem	odstup	k1gInSc7	odstup
asi	asi	k9	asi
30-70	[number]	k4	30-70
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
s	s	k7c7	s
účelem	účel	k1gInSc7	účel
šířit	šířit	k5eAaImF	šířit
náboženské	náboženský	k2eAgFnPc4d1	náboženská
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
zachytit	zachytit	k5eAaPmF	zachytit
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yRgNnSc1	který
nelze	lze	k6eNd1	lze
dostatečně	dostatečně	k6eAd1	dostatečně
konfrontovat	konfrontovat	k5eAaBmF	konfrontovat
s	s	k7c7	s
prameny	pramen	k1gInPc7	pramen
jiného	jiný	k2eAgInSc2d1	jiný
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
neexistuje	existovat	k5eNaImIp3nS	existovat
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
fyzický	fyzický	k2eAgInSc1d1	fyzický
důkaz	důkaz	k1gInSc1	důkaz
o	o	k7c6	o
Ježíšově	Ježíšův	k2eAgFnSc6d1	Ježíšova
existenci	existence	k1gFnSc6	existence
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
badatelů	badatel	k1gMnPc2	badatel
ji	on	k3xPp3gFnSc4	on
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
galilejského	galilejský	k2eAgMnSc4d1	galilejský
rabína	rabín	k1gMnSc4	rabín
a	a	k8xC	a
židovského	židovský	k2eAgMnSc2d1	židovský
náboženského	náboženský	k2eAgMnSc2d1	náboženský
reformátora	reformátor	k1gMnSc2	reformátor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejistá	jistý	k2eNgFnSc1d1	nejistá
a	a	k8xC	a
hypotetická	hypotetický	k2eAgFnSc1d1	hypotetická
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
příběh	příběh	k1gInSc1	příběh
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
popisuje	popisovat	k5eAaImIp3nS	popisovat
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
v	v	k7c6	v
evangeliích	evangelium	k1gNnPc6	evangelium
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
synoptických	synoptický	k2eAgFnPc6d1	synoptická
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
odlišném	odlišný	k2eAgInSc6d1	odlišný
Janově	Janov	k1gInSc6	Janov
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
podávají	podávat	k5eAaImIp3nP	podávat
apokryfní	apokryfní	k2eAgNnPc1d1	apokryfní
evangelia	evangelium	k1gNnPc1	evangelium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
církve	církev	k1gFnPc1	církev
neuznávají	uznávat	k5eNaImIp3nP	uznávat
za	za	k7c4	za
závaznou	závazný	k2eAgFnSc4d1	závazná
součást	součást	k1gFnSc4	součást
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgNnSc1d1	páně
a	a	k8xC	a
Panenské	panenský	k2eAgNnSc1d1	panenské
početí	početí	k1gNnSc1	početí
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Evangelia	evangelium	k1gNnPc1	evangelium
označují	označovat	k5eAaImIp3nP	označovat
Ježíše	Ježíš	k1gMnPc4	Ježíš
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
syna	syn	k1gMnSc2	syn
Josefova	Josefov	k1gInSc2	Josefov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
syna	syn	k1gMnSc4	syn
Josefova	Josefův	k2eAgMnSc4d1	Josefův
z	z	k7c2	z
Nazareta	Nazaret	k1gInSc2	Nazaret
<g/>
"	"	kIx"	"
a	a	k8xC	a
jako	jako	k9	jako
"	"	kIx"	"
<g/>
syna	syn	k1gMnSc2	syn
Mariina	Mariin	k2eAgMnSc2d1	Mariin
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
byl	být	k5eAaImAgMnS	být
tesařem	tesař	k1gMnSc7	tesař
a	a	k8xC	a
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
spolehlivých	spolehlivý	k2eAgInPc2d1	spolehlivý
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nedožil	dožít	k5eNaPmAgMnS	dožít
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
veřejného	veřejný	k2eAgNnSc2d1	veřejné
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Ježíšově	Ježíšův	k2eAgFnSc6d1	Ježíšova
matce	matka	k1gFnSc6	matka
Marii	Maria	k1gFnSc6	Maria
se	se	k3xPyFc4	se
evangelia	evangelium	k1gNnPc1	evangelium
a	a	k8xC	a
Skutky	skutek	k1gInPc1	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
ještě	ještě	k9	ještě
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
raně	raně	k6eAd1	raně
křesťanské	křesťanský	k2eAgFnSc3d1	křesťanská
obci	obec	k1gFnSc3	obec
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Evangelia	evangelium	k1gNnPc1	evangelium
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
také	také	k9	také
Ježíšovy	Ježíšův	k2eAgMnPc4d1	Ježíšův
bratry	bratr	k1gMnPc4	bratr
či	či	k8xC	či
bratrance	bratranec	k1gMnSc2	bratranec
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
Šimona	Šimon	k1gMnSc2	Šimon
a	a	k8xC	a
Judu	judo	k1gNnSc6	judo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
širšímu	široký	k2eAgNnSc3d2	širší
vymezení	vymezení	k1gNnSc3	vymezení
slova	slovo	k1gNnSc2	slovo
ἀ	ἀ	k?	ἀ
(	(	kIx(	(
<g/>
adelfos	adelfos	k1gMnSc1	adelfos
<g/>
)	)	kIx)	)
nelze	lze	k6eNd1	lze
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Ježíš	ježit	k5eAaImIp2nS	ježit
nějaké	nějaký	k3yIgFnSc3	nějaký
sourozence	sourozenka	k1gFnSc3	sourozenka
měl	mít	k5eAaImAgMnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Markovo	Markův	k2eAgNnSc1d1	Markovo
evangelium	evangelium	k1gNnSc1	evangelium
o	o	k7c6	o
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
dětství	dětství	k1gNnSc6	dětství
mlčí	mlčet	k5eAaImIp3nS	mlčet
a	a	k8xC	a
Janovo	Janův	k2eAgNnSc4d1	Janovo
evangelium	evangelium	k1gNnSc4	evangelium
před	před	k7c4	před
záznam	záznam	k1gInSc4	záznam
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
vystoupení	vystoupení	k1gNnSc4	vystoupení
předřazuje	předřazovat	k5eAaImIp3nS	předřazovat
pouze	pouze	k6eAd1	pouze
básnicko-teologický	básnickoeologický	k2eAgInSc4d1	básnicko-teologický
prolog	prolog	k1gInSc4	prolog
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
řeckého	řecký	k2eAgInSc2d1	řecký
filosofického	filosofický	k2eAgInSc2d1	filosofický
pojmu	pojem	k1gInSc2	pojem
λ	λ	k?	λ
(	(	kIx(	(
<g/>
logos	logos	k1gInSc1	logos
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc1	slovo
<g/>
)	)	kIx)	)
líčí	líčit	k5eAaImIp3nS	líčit
Ježíše	Ježíš	k1gMnSc4	Ježíš
jako	jako	k8xC	jako
odvěkou	odvěký	k2eAgFnSc4d1	odvěká
boží	boží	k2eAgFnSc4d1	boží
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
niž	jenž	k3xRgFnSc4	jenž
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Evangelium	evangelium	k1gNnSc1	evangelium
Matoušovo	Matoušův	k2eAgNnSc1d1	Matoušovo
a	a	k8xC	a
Lukášovo	Lukášův	k2eAgNnSc1d1	Lukášovo
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
pozdějšími	pozdní	k2eAgInPc7d2	pozdější
apokryfními	apokryfní	k2eAgInPc7d1	apokryfní
texty	text	k1gInPc7	text
<g/>
,	,	kIx,	,
věnují	věnovat	k5eAaPmIp3nP	věnovat
Ježíšovu	Ježíšův	k2eAgFnSc4d1	Ježíšova
narození	narození	k1gNnSc6	narození
a	a	k8xC	a
dětství	dětství	k1gNnSc6	dětství
značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
některé	některý	k3yIgInPc4	některý
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
údaje	údaj	k1gInPc4	údaj
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
historicky	historicky	k6eAd1	historicky
věrné	věrný	k2eAgFnPc1d1	věrná
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jejich	jejich	k3xOp3gNnSc1	jejich
líčení	líčení	k1gNnSc1	líčení
často	často	k6eAd1	často
spíše	spíše	k9	spíše
legendární	legendární	k2eAgInSc4d1	legendární
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
historikové	historik	k1gMnPc1	historik
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yRnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
i	i	k9	i
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
nejstarších	starý	k2eAgFnPc6d3	nejstarší
vrstvách	vrstva	k1gFnPc6	vrstva
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
žádné	žádný	k3yNgFnPc1	žádný
informace	informace	k1gFnPc1	informace
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
neobjevují	objevovat	k5eNaImIp3nP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Matoušovo	Matoušův	k2eAgNnSc1d1	Matoušovo
a	a	k8xC	a
Lukášovo	Lukášův	k2eAgNnSc1d1	Lukášovo
líčení	líčení	k1gNnSc1	líčení
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
narození	narození	k1gNnSc2	narození
však	však	k9	však
mělo	mít	k5eAaImAgNnS	mít
bezesporu	bezesporu	k9	bezesporu
nesmírný	smírný	k2eNgInSc4d1	nesmírný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
formování	formování	k1gNnSc4	formování
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
teologie	teologie	k1gFnSc2	teologie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
smyslem	smysl	k1gInSc7	smysl
těchto	tento	k3xDgFnPc2	tento
líčení	líčení	k1gNnSc4	líčení
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
narození	narození	k1gNnSc2	narození
je	být	k5eAaImIp3nS	být
především	především	k9	především
poselství	poselství	k1gNnSc4	poselství
pro	pro	k7c4	pro
čtenáře	čtenář	k1gMnPc4	čtenář
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
předložit	předložit	k5eAaPmF	předložit
<g/>
,	,	kIx,	,
kým	kdo	k3yQnSc7	kdo
Ježíš	ježit	k5eAaImIp2nS	ježit
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
byl	být	k5eAaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Představují	představovat	k5eAaImIp3nP	představovat
tedy	tedy	k9	tedy
ranou	raný	k2eAgFnSc4d1	raná
christologii	christologie	k1gFnSc4	christologie
zpracovanou	zpracovaný	k2eAgFnSc7d1	zpracovaná
pomocí	pomoc	k1gFnSc7	pomoc
vyprávění	vyprávění	k1gNnSc2	vyprávění
<g/>
.	.	kIx.	.
</s>
<s>
Ježíšovy	Ježíšův	k2eAgInPc1d1	Ježíšův
rodokmeny	rodokmen	k1gInPc1	rodokmen
mají	mít	k5eAaImIp3nP	mít
Krista	Kristus	k1gMnSc4	Kristus
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
linie	linie	k1gFnSc2	linie
významných	významný	k2eAgMnPc2d1	významný
náboženských	náboženský	k2eAgMnPc2d1	náboženský
a	a	k8xC	a
světských	světský	k2eAgMnPc2d1	světský
vůdců	vůdce	k1gMnPc2	vůdce
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
o	o	k7c4	o
početí	početí	k1gNnSc4	početí
z	z	k7c2	z
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k2eAgMnSc2d1	svatý
a	a	k8xC	a
narození	narození	k1gNnSc2	narození
z	z	k7c2	z
panny	panna	k1gFnSc2	panna
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Ježíšův	Ježíšův	k2eAgInSc4d1	Ježíšův
božský	božský	k2eAgInSc4d1	božský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vyprávění	vyprávění	k1gNnSc1	vyprávění
o	o	k7c6	o
útěku	útěk	k1gInSc6	útěk
Ježíšovy	Ježíšův	k2eAgFnSc2d1	Ježíšova
rodiny	rodina	k1gFnSc2	rodina
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
před	před	k7c7	před
záští	zášť	k1gFnSc7	zášť
krále	král	k1gMnSc2	král
Heroda	Herod	k1gMnSc2	Herod
Velikého	veliký	k2eAgMnSc2d1	veliký
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
pobyt	pobyt	k1gInSc4	pobyt
izraelského	izraelský	k2eAgInSc2d1	izraelský
národa	národ	k1gInSc2	národ
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Exodus	Exodus	k1gInSc1	Exodus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
boží	boží	k2eAgFnSc4d1	boží
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
Ježíše	Ježíš	k1gMnPc4	Ježíš
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
na	na	k7c4	na
dítě	dítě	k1gNnSc4	dítě
aplikovat	aplikovat	k5eAaBmF	aplikovat
staré	starý	k2eAgInPc4d1	starý
biblické	biblický	k2eAgInPc4d1	biblický
texty	text	k1gInPc4	text
a	a	k8xC	a
přeznačit	přeznačit	k5eAaPmF	přeznačit
je	on	k3xPp3gMnPc4	on
na	na	k7c4	na
předpovědi	předpověď	k1gFnPc4	předpověď
událostí	událost	k1gFnPc2	událost
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
Ježíšem	Ježíš	k1gMnSc7	Ježíš
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgNnPc4d1	historické
svědectví	svědectví	k1gNnPc4	svědectví
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
určit	určit	k5eAaPmF	určit
přesně	přesně	k6eAd1	přesně
datum	datum	k1gNnSc1	datum
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
narození	narození	k1gNnSc2	narození
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Matoušova	Matoušův	k2eAgNnSc2d1	Matoušovo
evangelia	evangelium	k1gNnSc2	evangelium
se	se	k3xPyFc4	se
Ježíš	Ježíš	k1gMnSc1	Ježíš
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
judském	judský	k2eAgNnSc6d1	Judské
městečku	městečko	k1gNnSc6	městečko
Betlémě	Betlém	k1gInSc6	Betlém
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yRnSc6	což
shledává	shledávat	k5eAaImIp3nS	shledávat
naplnění	naplnění	k1gNnSc1	naplnění
starozákonního	starozákonní	k2eAgNnSc2d1	starozákonní
proroctví	proroctví	k1gNnSc2	proroctví
Micheášova	Micheášův	k2eAgFnSc1d1	Micheášův
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
někteří	některý	k3yIgMnPc1	některý
historikové	historik	k1gMnPc1	historik
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yRnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
nepovažují	považovat	k5eNaImIp3nP	považovat
tento	tento	k3xDgInSc4	tento
údaj	údaj	k1gInSc4	údaj
za	za	k7c4	za
zcela	zcela	k6eAd1	zcela
spolehlivý	spolehlivý	k2eAgInSc4d1	spolehlivý
a	a	k8xC	a
domnívají	domnívat	k5eAaImIp3nP	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
legendární	legendární	k2eAgInSc4d1	legendární
údaj	údaj	k1gInSc4	údaj
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
na	na	k7c6	na
základě	základ	k1gInSc6	základ
připodobnění	připodobnění	k1gNnSc2	připodobnění
Ježíše	Ježíš	k1gMnSc2	Ježíš
ke	k	k7c3	k
králi	král	k1gMnSc3	král
Davidovi	David	k1gMnSc3	David
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
právě	právě	k6eAd1	právě
z	z	k7c2	z
Betléma	Betlém	k1gInSc2	Betlém
<g/>
.	.	kIx.	.
</s>
<s>
Údaj	údaj	k1gInSc1	údaj
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
galilejském	galilejský	k2eAgInSc6d1	galilejský
Nazaretě	Nazaret	k1gInSc6	Nazaret
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
dostává	dostávat	k5eAaImIp3nS	dostávat
Ježíš	Ježíš	k1gMnSc1	Ježíš
také	také	k9	také
přídomek	přídomek	k1gInSc4	přídomek
"	"	kIx"	"
<g/>
Nazaretský	nazaretský	k2eAgMnSc1d1	nazaretský
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
Nazorejský	Nazorejský	k2eAgMnSc1d1	Nazorejský
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
obecně	obecně	k6eAd1	obecně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
autentický	autentický	k2eAgInSc4d1	autentický
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
Ježíšově	Ježíšův	k2eAgFnSc6d1	Ježíšova
době	doba	k1gFnSc6	doba
nebyla	být	k5eNaImAgFnS	být
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
místem	místo	k1gNnSc7	místo
spojována	spojovat	k5eAaImNgFnS	spojovat
žádná	žádný	k3yNgNnPc4	žádný
očekávání	očekávání	k1gNnPc4	očekávání
<g/>
.	.	kIx.	.
</s>
<s>
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Matouše	Matouš	k1gMnSc2	Matouš
sice	sice	k8xC	sice
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
údaj	údaj	k1gInSc1	údaj
byl	být	k5eAaImAgInS	být
proroky	prorok	k1gMnPc4	prorok
předpovězen	předpovězen	k2eAgMnSc1d1	předpovězen
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
starozákonní	starozákonní	k2eAgNnSc1d1	starozákonní
proroctví	proroctví	k1gNnSc1	proroctví
tento	tento	k3xDgInSc4	tento
údaj	údaj	k1gInSc4	údaj
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
<g/>
;	;	kIx,	;
snad	snad	k9	snad
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
davidovský	davidovský	k2eAgInSc4d1	davidovský
odkaz	odkaz	k1gInSc4	odkaz
<g/>
,	,	kIx,	,
když	když	k8xS	když
kniha	kniha	k1gFnSc1	kniha
Izajáš	Izajáš	k1gFnSc2	Izajáš
nazývá	nazývat	k5eAaImIp3nS	nazývat
mesiáše	mesiáš	k1gMnPc4	mesiáš
"	"	kIx"	"
<g/>
proutkem	proutek	k1gInSc7	proutek
(	(	kIx(	(
<g/>
נ	נ	k?	נ
néser	néser	k1gInSc1	néser
<g/>
)	)	kIx)	)
z	z	k7c2	z
Jišajova	Jišajův	k2eAgInSc2d1	Jišajův
pařezu	pařez	k1gInSc2	pařez
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Náš	náš	k3xOp1gInSc1	náš
letopočet	letopočet	k1gInSc1	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Biblická	biblický	k2eAgFnSc1d1	biblická
datace	datace	k1gFnSc1	datace
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
narození	narození	k1gNnSc2	narození
není	být	k5eNaImIp3nS	být
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
<g/>
:	:	kIx,	:
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Matouše	Matouš	k1gMnSc2	Matouš
klade	klást	k5eAaImIp3nS	klást
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
narození	narození	k1gNnSc1	narození
před	před	k7c4	před
smrt	smrt	k1gFnSc4	smrt
Heroda	Herod	k1gMnSc2	Herod
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
4	[number]	k4	4
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Lukáše	Lukáš	k1gMnSc2	Lukáš
je	být	k5eAaImIp3nS	být
klade	klást	k5eAaImIp3nS	klást
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
římským	římský	k2eAgMnSc7d1	římský
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
událo	udát	k5eAaPmAgNnS	udát
za	za	k7c2	za
působení	působení	k1gNnSc2	působení
syrského	syrský	k2eAgMnSc4d1	syrský
místodržitele	místodržitel	k1gMnSc4	místodržitel
P.	P.	kA	P.
Sulpicia	Sulpicius	k1gMnSc4	Sulpicius
Quirinia	Quirinium	k1gNnSc2	Quirinium
<g/>
.	.	kIx.	.
</s>
<s>
Doklady	doklad	k1gInPc1	doklad
o	o	k7c6	o
censu	census	k1gInSc6	census
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
roce	rok	k1gInSc6	rok
6	[number]	k4	6
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c4	o
první	první	k4xOgNnSc4	první
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
za	za	k7c4	za
Quiriniova	Quiriniův	k2eAgNnPc4d1	Quiriniův
místodržitelství	místodržitelství	k1gNnPc4	místodržitelství
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
indicii	indicie	k1gFnSc4	indicie
může	moct	k5eAaImIp3nS	moct
představovat	představovat	k5eAaImF	představovat
betlémská	betlémský	k2eAgFnSc1d1	Betlémská
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
snad	snad	k9	snad
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
konjunkce	konjunkce	k1gFnSc1	konjunkce
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
pozorovat	pozorovat	k5eAaImF	pozorovat
roku	rok	k1gInSc2	rok
7	[number]	k4	7
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Porovnáním	porovnání	k1gNnSc7	porovnání
údajů	údaj	k1gInPc2	údaj
o	o	k7c4	o
vystoupení	vystoupení	k1gNnSc4	vystoupení
Ježíšova	Ježíšův	k2eAgMnSc2d1	Ježíšův
předchůdce	předchůdce	k1gMnSc2	předchůdce
(	(	kIx(	(
<g/>
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
v	v	k7c4	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Tiberia	Tiberium	k1gNnSc2	Tiberium
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
roku	rok	k1gInSc2	rok
28	[number]	k4	28
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
samotného	samotný	k2eAgMnSc2d1	samotný
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
třicet	třicet	k4xCc1	třicet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
rok	rok	k1gInSc1	rok
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
narození	narození	k1gNnSc2	narození
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
přelomu	přelom	k1gInSc2	přelom
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
historiků	historik	k1gMnPc2	historik
dnes	dnes	k6eAd1	dnes
klade	klást	k5eAaImIp3nS	klást
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
narození	narození	k1gNnSc1	narození
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
7	[number]	k4	7
a	a	k8xC	a
4	[number]	k4	4
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Rok	rok	k1gInSc1	rok
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
narození	narození	k1gNnSc2	narození
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vypočítat	vypočítat	k5eAaPmF	vypočítat
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
na	na	k7c6	na
základě	základ	k1gInSc6	základ
historických	historický	k2eAgInPc2d1	historický
záznamů	záznam	k1gInPc2	záznam
<g/>
,	,	kIx,	,
dostupných	dostupný	k2eAgInPc2d1	dostupný
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
mnich	mnich	k1gMnSc1	mnich
Dionysius	Dionysius	k1gMnSc1	Dionysius
Exiguus	Exiguus	k1gMnSc1	Exiguus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
však	však	k9	však
tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
neurčil	určit	k5eNaPmAgMnS	určit
přesně	přesně	k6eAd1	přesně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
Dionysiova	Dionysiův	k2eAgInSc2d1	Dionysiův
výpočtu	výpočet	k1gInSc2	výpočet
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
začal	začít	k5eAaPmAgInS	začít
užívat	užívat	k5eAaImF	užívat
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
letopočet	letopočet	k1gInSc1	letopočet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
letopočtem	letopočet	k1gInSc7	letopočet
celosvětově	celosvětově	k6eAd1	celosvětově
nejběžněji	běžně	k6eAd3	běžně
používaným	používaný	k2eAgFnPc3d1	používaná
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
přišel	přijít	k5eAaPmAgInS	přijít
znovu	znovu	k6eAd1	znovu
asi	asi	k9	asi
před	před	k7c7	před
deseti	deset	k4xCc7	deset
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
díky	díky	k7c3	díky
studiím	studie	k1gFnPc3	studie
Giorgia	Giorgium	k1gNnSc2	Giorgium
Fedalta	Fedalt	k1gInSc2	Fedalt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
využil	využít	k5eAaPmAgInS	využít
výsledků	výsledek	k1gInPc2	výsledek
pozorování	pozorování	k1gNnPc4	pozorování
washingtonské	washingtonský	k2eAgInPc4d1	washingtonský
U.	U.	kA	U.
S.	S.	kA	S.
Naval	navalit	k5eAaPmRp2nS	navalit
Observatory	Observator	k1gMnPc7	Observator
a	a	k8xC	a
klade	klást	k5eAaImIp3nS	klást
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
narození	narození	k1gNnSc1	narození
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1	[number]	k4	1
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
datum	datum	k1gNnSc1	datum
oslavy	oslava	k1gFnSc2	oslava
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
(	(	kIx(	(
<g/>
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ustanovuje	ustanovovat	k5eAaImIp3nS	ustanovovat
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
velikonočním	velikonoční	k2eAgInSc7d1	velikonoční
cyklem	cyklus	k1gInSc7	cyklus
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
původ	původ	k1gInSc4	původ
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
však	však	k9	však
zejména	zejména	k9	zejména
mezi	mezi	k7c4	mezi
liturgisty	liturgist	k1gInPc4	liturgist
ujala	ujmout	k5eAaPmAgFnS	ujmout
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
konvenční	konvenční	k2eAgNnSc1d1	konvenční
datum	datum	k1gNnSc1	datum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
zvolili	zvolit	k5eAaPmAgMnP	zvolit
římští	římský	k2eAgMnPc1d1	římský
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
kult	kult	k1gInSc4	kult
nepřemožitelného	přemožitelný	k2eNgNnSc2d1	nepřemožitelné
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
svátek	svátek	k1gInSc4	svátek
Mitrův	Mitrův	k2eAgInSc1d1	Mitrův
nebo	nebo	k8xC	nebo
císařův	císařův	k2eAgInSc1d1	císařův
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
připadal	připadat	k5eAaImAgInS	připadat
na	na	k7c4	na
zimní	zimní	k2eAgInSc4d1	zimní
slunovrat	slunovrat	k1gInSc4	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
Konstantinově	Konstantinův	k2eAgInSc6d1	Konstantinův
ediktu	edikt	k1gInSc6	edikt
by	by	kYmCp3nS	by
církev	církev	k1gFnSc1	církev
mohla	moct	k5eAaImAgFnS	moct
mít	mít	k5eAaImF	mít
touhu	touha	k1gFnSc4	touha
zužitkovat	zužitkovat	k5eAaPmF	zužitkovat
nějaký	nějaký	k3yIgInSc4	nějaký
svátek	svátek	k1gInSc4	svátek
zanikajícího	zanikající	k2eAgNnSc2d1	zanikající
pohanství	pohanství	k1gNnSc2	pohanství
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
vymýšlet	vymýšlet	k5eAaImF	vymýšlet
z	z	k7c2	z
ničeho	nic	k3yNnSc2	nic
nic	nic	k3yNnSc1	nic
datum	datum	k1gInSc4	datum
tak	tak	k6eAd1	tak
zásadního	zásadní	k2eAgInSc2d1	zásadní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
kult	kult	k1gInSc1	kult
nepřemožitelného	přemožitelný	k2eNgNnSc2d1	nepřemožitelné
Slunce	slunce	k1gNnSc2	slunce
zavedl	zavést	k5eAaPmAgMnS	zavést
císař	císař	k1gMnSc1	císař
Aurelianus	Aurelianus	k1gMnSc1	Aurelianus
roku	rok	k1gInSc2	rok
274	[number]	k4	274
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
první	první	k4xOgFnSc4	první
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc6	prosinec
jako	jako	k8xC	jako
dni	den	k1gInSc6	den
Narození	narození	k1gNnSc2	narození
Krista	Kristus	k1gMnSc2	Kristus
nacházíme	nacházet	k5eAaImIp1nP	nacházet
u	u	k7c2	u
Hippolyta	Hippolyt	k1gInSc2	Hippolyt
Římského	římský	k2eAgInSc2d1	římský
roku	rok	k1gInSc2	rok
204	[number]	k4	204
(	(	kIx(	(
<g/>
Komentář	komentář	k1gInSc1	komentář
ke	k	k7c3	k
knize	kniha	k1gFnSc3	kniha
proroka	prorok	k1gMnSc2	prorok
Daniela	Daniel	k1gMnSc2	Daniel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
lze	lze	k6eAd1	lze
připojit	připojit	k5eAaPmF	připojit
také	také	k9	také
homilii	homilie	k1gFnSc4	homilie
Jana	Jan	k1gMnSc2	Jan
Zlatoústého	zlatoústý	k2eAgMnSc2d1	zlatoústý
z	z	k7c2	z
roku	rok	k1gInSc2	rok
386	[number]	k4	386
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
římská	římský	k2eAgFnSc1d1	římská
církev	církev	k1gFnSc1	církev
zná	znát	k5eAaImIp3nS	znát
datum	datum	k1gNnSc4	datum
Kristova	Kristův	k2eAgNnSc2d1	Kristovo
narození	narození	k1gNnSc2	narození
z	z	k7c2	z
archivu	archiv	k1gInSc2	archiv
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
uchovala	uchovat	k5eAaPmAgFnS	uchovat
akta	akta	k1gNnPc4	akta
o	o	k7c4	o
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
provedeném	provedený	k2eAgInSc6d1	provedený
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
císaře	císař	k1gMnSc2	císař
Augusta	August	k1gMnSc2	August
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
narození	narození	k1gNnSc2	narození
a	a	k8xC	a
navazujících	navazující	k2eAgFnPc2d1	navazující
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
útěku	útěk	k1gInSc2	útěk
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
tamním	tamní	k2eAgInSc6d1	tamní
pobytu	pobyt	k1gInSc6	pobyt
a	a	k8xC	a
návratu	návrat	k1gInSc6	návrat
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
další	další	k2eAgFnSc4d1	další
událost	událost	k1gFnSc4	událost
z	z	k7c2	z
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gInSc7	on
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
Lukášova	Lukášův	k2eAgNnSc2d1	Lukášovo
evangelia	evangelium	k1gNnSc2	evangelium
o	o	k7c6	o
dvanáctiletém	dvanáctiletý	k2eAgInSc6d1	dvanáctiletý
Ježíši	Ježíš	k1gMnPc7	Ježíš
v	v	k7c6	v
jeruzalémském	jeruzalémský	k2eAgInSc6d1	jeruzalémský
chrámě	chrám	k1gInSc6	chrám
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
s	s	k7c7	s
učiteli	učitel	k1gMnPc7	učitel
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
o	o	k7c6	o
pesachu	pesach	k1gInSc6	pesach
ztratil	ztratit	k5eAaPmAgInS	ztratit
rodičům	rodič	k1gMnPc3	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
události	událost	k1gFnPc1	událost
a	a	k8xC	a
zázraky	zázrak	k1gInPc1	zázrak
konané	konaný	k2eAgFnSc2d1	konaná
chlapcem	chlapec	k1gMnSc7	chlapec
Ježíšem	Ježíš	k1gMnSc7	Ježíš
popisují	popisovat	k5eAaImIp3nP	popisovat
apokryfní	apokryfní	k2eAgNnPc4d1	apokryfní
evangelia	evangelium	k1gNnPc4	evangelium
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zmíněné	zmíněný	k2eAgNnSc4d1	zmíněné
Pseudo-Tomášovo	Pseudo-Tomášův	k2eAgNnSc4d1	Pseudo-Tomášův
evangelium	evangelium	k1gNnSc4	evangelium
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Hlasatel	hlasatel	k1gMnSc1	hlasatel
pokání	pokání	k1gNnSc2	pokání
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
poblíž	poblíž	k7c2	poblíž
řeky	řeka	k1gFnSc2	řeka
Jordánu	Jordán	k1gInSc2	Jordán
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Bible	bible	k1gFnSc2	bible
<g/>
)	)	kIx)	)
a	a	k8xC	a
křtil	křtít	k5eAaImAgMnS	křtít
zde	zde	k6eAd1	zde
kajícníky	kajícník	k1gMnPc4	kajícník
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
lítosti	lítost	k1gFnSc2	lítost
a	a	k8xC	a
odpuštění	odpuštění	k1gNnSc2	odpuštění
hříchů	hřích	k1gInPc2	hřích
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
zákoně	zákon	k1gInSc6	zákon
líčen	líčen	k2eAgMnSc1d1	líčen
a	a	k8xC	a
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
chápán	chápat	k5eAaImNgMnS	chápat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
předchůdce	předchůdce	k1gMnSc1	předchůdce
Páně	páně	k2eAgMnSc1d1	páně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
Ježíš	Ježíš	k1gMnSc1	Ježíš
sám	sám	k3xTgMnSc1	sám
patřil	patřit	k5eAaImAgMnS	patřit
po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
okruhu	okruh	k1gInSc3	okruh
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
;	;	kIx,	;
přes	přes	k7c4	přes
zjevné	zjevný	k2eAgInPc4d1	zjevný
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
asketou	asketa	k1gMnSc7	asketa
Janem	Jan	k1gMnSc7	Jan
a	a	k8xC	a
Ježíšem	Ježíš	k1gMnSc7	Ježíš
přijímajícím	přijímající	k2eAgMnSc7d1	přijímající
pozvání	pozvání	k1gNnSc4	pozvání
na	na	k7c4	na
hostiny	hostina	k1gFnPc4	hostina
však	však	k9	však
sdíleli	sdílet	k5eAaImAgMnP	sdílet
naléhavou	naléhavý	k2eAgFnSc4d1	naléhavá
<g/>
,	,	kIx,	,
radikální	radikální	k2eAgFnSc4d1	radikální
etiku	etika	k1gFnSc4	etika
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
vizemi	vize	k1gFnPc7	vize
blížícího	blížící	k2eAgMnSc2d1	blížící
se	se	k3xPyFc4	se
konce	konec	k1gInSc2	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Setkání	setkání	k1gNnSc1	setkání
obou	dva	k4xCgMnPc2	dva
mužů	muž	k1gMnPc2	muž
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ježíš	Ježíš	k1gMnSc1	Ježíš
nechal	nechat	k5eAaPmAgMnS	nechat
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
pokřtít	pokřtít	k5eAaPmF	pokřtít
jako	jako	k8xC	jako
další	další	k2eAgMnPc1d1	další
kajícníci	kajícník	k1gMnPc1	kajícník
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Qasr	Qasr	k1gMnSc1	Qasr
al-Jahúd	al-Jahúd	k1gMnSc1	al-Jahúd
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
zpravují	zpravovat	k5eAaImIp3nP	zpravovat
všechna	všechen	k3xTgNnPc4	všechen
čtyři	čtyři	k4xCgNnPc4	čtyři
kanonická	kanonický	k2eAgNnPc4d1	kanonické
evangelia	evangelium	k1gNnPc4	evangelium
a	a	k8xC	a
vykládají	vykládat	k5eAaImIp3nP	vykládat
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
mesiášskou	mesiášský	k2eAgFnSc4d1	mesiášská
iniciaci	iniciace	k1gFnSc4	iniciace
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
jejích	její	k3xOp3gNnPc2	její
líčení	líčení	k1gNnPc2	líčení
se	se	k3xPyFc4	se
při	při	k7c6	při
křtu	křest	k1gInSc6	křest
ozval	ozvat	k5eAaPmAgMnS	ozvat
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
hlas	hlas	k1gInSc1	hlas
a	a	k8xC	a
označil	označit	k5eAaPmAgInS	označit
Ježíše	Ježíš	k1gMnSc4	Ježíš
za	za	k7c4	za
vyvoleného	vyvolený	k2eAgMnSc4d1	vyvolený
božího	boží	k2eAgMnSc4d1	boží
syna	syn	k1gMnSc4	syn
Křest	křest	k1gInSc4	křest
v	v	k7c6	v
Jordánu	Jordán	k1gInSc6	Jordán
je	být	k5eAaImIp3nS	být
historiky	historik	k1gMnPc4	historik
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
věrohodně	věrohodně	k6eAd1	věrohodně
doložený	doložený	k2eAgInSc4d1	doložený
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
shodu	shoda	k1gFnSc4	shoda
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
evangelií	evangelium	k1gNnPc2	evangelium
a	a	k8xC	a
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozdější	pozdní	k2eAgMnPc1d2	pozdější
křesťané	křesťan	k1gMnPc1	křesťan
neměli	mít	k5eNaImAgMnP	mít
důvod	důvod	k1gInSc4	důvod
vymýšlet	vymýšlet	k5eAaImF	vymýšlet
si	se	k3xPyFc3	se
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
staví	stavit	k5eAaBmIp3nS	stavit
Jana	Jan	k1gMnSc4	Jan
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
autority	autorita	k1gFnSc2	autorita
vůči	vůči	k7c3	vůči
Ježíšovi	Ježíš	k1gMnSc3	Ježíš
<g/>
.	.	kIx.	.
</s>
<s>
Křest	křest	k1gInSc1	křest
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
Biblí	bible	k1gFnPc2	bible
líčen	líčen	k2eAgMnSc1d1	líčen
jako	jako	k8xC	jako
začátek	začátek	k1gInSc1	začátek
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
veřejného	veřejný	k2eAgNnSc2d1	veřejné
vystoupení	vystoupení	k1gNnSc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Janova	Janův	k2eAgNnSc2d1	Janovo
evangelia	evangelium	k1gNnSc2	evangelium
Ježíšovi	Ježíšův	k2eAgMnPc1d1	Ježíšův
učedníci	učedník	k1gMnPc1	učedník
poté	poté	k6eAd1	poté
po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
křtili	křtít	k5eAaImAgMnP	křtít
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
přítomnosti	přítomnost	k1gFnSc6	přítomnost
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
o	o	k7c4	o
ně	on	k3xPp3gNnSc4	on
větší	veliký	k2eAgInSc4d2	veliký
zájem	zájem	k1gInSc4	zájem
než	než	k8xS	než
o	o	k7c4	o
Jana	Jan	k1gMnSc4	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
však	však	k9	však
se	se	k3xPyFc4	se
Ježíš	Ježíš	k1gMnSc1	Ježíš
vzdal	vzdát	k5eAaPmAgInS	vzdát
pevného	pevný	k2eAgNnSc2d1	pevné
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
jako	jako	k9	jako
putující	putující	k2eAgMnSc1d1	putující
kazatel	kazatel	k1gMnSc1	kazatel
v	v	k7c6	v
Galileji	Galilea	k1gFnSc6	Galilea
a	a	k8xC	a
okolních	okolní	k2eAgNnPc6d1	okolní
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
strávil	strávit	k5eAaPmAgMnS	strávit
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
v	v	k7c6	v
Galileji	Galilea	k1gFnSc6	Galilea
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Heroda	Herod	k1gMnSc2	Herod
Antipy	Antipa	k1gFnSc2	Antipa
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
třiceti	třicet	k4xCc2	třicet
let	léto	k1gNnPc2	léto
začal	začít	k5eAaPmAgInS	začít
kázat	kázat	k5eAaImF	kázat
a	a	k8xC	a
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
thaumaturg	thaumaturg	k1gMnSc1	thaumaturg
a	a	k8xC	a
vykladač	vykladač	k1gMnSc1	vykladač
Zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vydal	vydat	k5eAaPmAgInS	vydat
svědectví	svědectví	k1gNnSc4	svědectví
pravdě	pravda	k1gFnSc3	pravda
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
18	[number]	k4	18
<g/>
,	,	kIx,	,
37	[number]	k4	37
(	(	kIx(	(
<g/>
Kral	Kral	k1gInSc1	Kral
<g/>
,	,	kIx,	,
ČEP	čep	k1gInSc1	čep
<g/>
))	))	k?	))
Mnohým	mnohý	k2eAgNnSc7d1	mnohé
jednáním	jednání	k1gNnSc7	jednání
Ježíš	ježit	k5eAaImIp2nS	ježit
narušoval	narušovat	k5eAaImAgInS	narušovat
dobové	dobový	k2eAgFnPc4d1	dobová
a	a	k8xC	a
náboženské	náboženský	k2eAgFnPc4d1	náboženská
konvence	konvence	k1gFnPc4	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Porušoval	porušovat	k5eAaImAgMnS	porušovat
některá	některý	k3yIgNnPc4	některý
židovská	židovský	k2eAgNnPc4d1	Židovské
pravidla	pravidlo	k1gNnPc4	pravidlo
(	(	kIx(	(
<g/>
uzdravování	uzdravování	k1gNnSc1	uzdravování
nebo	nebo	k8xC	nebo
sbírání	sbírání	k1gNnSc1	sbírání
klasů	klas	k1gInPc2	klas
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
vysoké	vysoký	k2eAgFnSc3d1	vysoká
hodnotě	hodnota	k1gFnSc3	hodnota
rodinných	rodinný	k2eAgFnPc2d1	rodinná
vazeb	vazba	k1gFnPc2	vazba
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
,	,	kIx,	,
že	že	k8xS	že
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
překonává	překonávat	k5eAaImIp3nS	překonávat
i	i	k9	i
rodinné	rodinný	k2eAgFnPc4d1	rodinná
vazby	vazba	k1gFnPc4	vazba
<g/>
:	:	kIx,	:
např.	např.	kA	např.
opuštění	opuštění	k1gNnSc1	opuštění
rodičů	rodič	k1gMnPc2	rodič
ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
(	(	kIx(	(
<g/>
Mt	Mt	k1gFnSc1	Mt
12	[number]	k4	12
<g/>
,	,	kIx,	,
47	[number]	k4	47
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
oslovení	oslovení	k1gNnSc1	oslovení
<g />
.	.	kIx.	.
</s>
<s>
matky	matka	k1gFnPc1	matka
při	při	k7c6	při
svatbě	svatba	k1gFnSc6	svatba
v	v	k7c6	v
Káni	káně	k1gFnSc6	káně
<g/>
,	,	kIx,	,
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
"	"	kIx"	"
<g/>
Hle	hle	k0	hle
<g/>
,	,	kIx,	,
tvoje	tvůj	k3xOp2gFnSc1	tvůj
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
tvoji	tvůj	k3xOp2gMnPc1	tvůj
bratři	bratr	k1gMnPc1	bratr
jsou	být	k5eAaImIp3nP	být
venku	venku	k6eAd1	venku
a	a	k8xC	a
hledají	hledat	k5eAaImIp3nP	hledat
tě	ty	k3xPp2nSc4	ty
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mk	Mk	k1gFnSc1	Mk
3	[number]	k4	3
<g/>
,	,	kIx,	,
32	[number]	k4	32
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
své	svůj	k3xOyFgMnPc4	svůj
učedníky	učedník	k1gMnPc4	učedník
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
všeho	všecek	k3xTgNnSc2	všecek
pro	pro	k7c4	pro
hlásání	hlásání	k1gNnSc4	hlásání
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
,	,	kIx,	,
narušoval	narušovat	k5eAaImAgInS	narušovat
pojetí	pojetí	k1gNnSc4	pojetí
výlučnosti	výlučnost	k1gFnSc2	výlučnost
Židů	Žid	k1gMnPc2	Žid
atd.	atd.	kA	atd.
Nábožensky	nábožensky	k6eAd1	nábožensky
motivované	motivovaný	k2eAgNnSc4d1	motivované
veřejné	veřejný	k2eAgNnSc4d1	veřejné
očištění	očištění	k1gNnSc4	očištění
chrámu	chrám	k1gInSc2	chrám
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
jeho	jeho	k3xOp3gNnSc4	jeho
zadržení	zadržení	k1gNnSc4	zadržení
před	před	k7c7	před
ukřižováním	ukřižování	k1gNnSc7	ukřižování
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ježíšovy	Ježíšův	k2eAgInPc4d1	Ježíšův
zázraky	zázrak	k1gInPc4	zázrak
<g/>
.	.	kIx.	.
</s>
<s>
Zázraky	zázrak	k1gInPc1	zázrak
nebyly	být	k5eNaImAgInP	být
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
filosofických	filosofický	k2eAgInPc6d1	filosofický
okruzích	okruh	k1gInPc6	okruh
nejsou	být	k5eNaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
vnímány	vnímán	k2eAgFnPc1d1	vnímána
jako	jako	k8xS	jako
porušování	porušování	k1gNnSc1	porušování
přírodních	přírodní	k2eAgInPc2d1	přírodní
zákonů	zákon	k1gInPc2	zákon
(	(	kIx(	(
<g/>
takový	takový	k3xDgInSc1	takový
pojem	pojem	k1gInSc1	pojem
tehdy	tehdy	k6eAd1	tehdy
neexistoval	existovat	k5eNaImAgInS	existovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
znamení	znamení	k1gNnSc1	znamení
Boží	boží	k2eAgFnSc2d1	boží
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
prorockého	prorocký	k2eAgInSc2d1	prorocký
vhledu	vhled	k1gInSc2	vhled
do	do	k7c2	do
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
je	být	k5eAaImIp3nS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
mnoho	mnoho	k4c1	mnoho
případů	případ	k1gInPc2	případ
zázračného	zázračný	k2eAgNnSc2d1	zázračné
uzdravení	uzdravení	k1gNnSc2	uzdravení
jak	jak	k8xS	jak
tělesného	tělesný	k2eAgNnSc2d1	tělesné
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
duševního	duševní	k2eAgNnSc2d1	duševní
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
horečky	horečka	k1gFnPc4	horečka
<g/>
,	,	kIx,	,
slepotu	slepota	k1gFnSc4	slepota
<g/>
,	,	kIx,	,
malomocenství	malomocenství	k1gNnSc4	malomocenství
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
příznaky	příznak	k1gInPc1	příznak
<g/>
,	,	kIx,	,
navrácení	navrácení	k1gNnSc1	navrácení
života	život	k1gInSc2	život
mrtvému	mrtvý	k1gMnSc3	mrtvý
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
uzdravování	uzdravování	k1gNnSc2	uzdravování
bylo	být	k5eAaImAgNnS	být
vyhánění	vyhánění	k1gNnSc4	vyhánění
zlých	zlá	k1gFnPc2	zlá
duchů	duch	k1gMnPc2	duch
<g/>
,	,	kIx,	,
praktikované	praktikovaný	k2eAgFnPc1d1	praktikovaná
některými	některý	k3yIgFnPc7	některý
křesťanskými	křesťanský	k2eAgFnPc7d1	křesťanská
církvemi	církev	k1gFnPc7	církev
dodnes	dodnes	k6eAd1	dodnes
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
exorcismus	exorcismus	k1gInSc1	exorcismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
způsobovat	způsobovat	k5eAaImF	způsobovat
jak	jak	k6eAd1	jak
tělesné	tělesný	k2eAgNnSc4d1	tělesné
tak	tak	k8xC	tak
duševní	duševní	k2eAgNnSc4d1	duševní
trápení	trápení	k1gNnSc4	trápení
a	a	k8xC	a
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uzdravení	uzdravení	k1gNnSc6	uzdravení
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
často	často	k6eAd1	často
též	též	k9	též
k	k	k7c3	k
obrácení	obrácení	k1gNnSc3	obrácení
dotyčného	dotyčný	k2eAgInSc2d1	dotyčný
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
jeho	jeho	k3xOp3gNnSc4	jeho
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
biblických	biblický	k2eAgNnPc2d1	biblické
vyprávění	vyprávění	k1gNnPc2	vyprávění
Ježíš	ježit	k5eAaImIp2nS	ježit
uzdravoval	uzdravovat	k5eAaImAgMnS	uzdravovat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pouhým	pouhý	k2eAgInSc7d1	pouhý
dotykem	dotyk	k1gInSc7	dotyk
či	či	k8xC	či
modlitbou	modlitba	k1gFnSc7	modlitba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
dočíst	dočíst	k5eAaPmF	dočíst
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	ježit	k5eAaImIp2nS	ježit
uzdravoval	uzdravovat	k5eAaImAgInS	uzdravovat
chromé	chromý	k2eAgFnPc4d1	chromá
i	i	k8xC	i
slepé	slepý	k2eAgFnPc4d1	slepá
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
narazil	narazit	k5eAaPmAgMnS	narazit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
J	J	kA	J
4	[number]	k4	4
<g/>
,	,	kIx,	,
43	[number]	k4	43
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
)	)	kIx)	)
uzdraví	uzdravit	k5eAaPmIp3nS	uzdravit
Ježíš	Ježíš	k1gMnSc1	Ježíš
dítě	dítě	k1gNnSc4	dítě
královského	královský	k2eAgMnSc2d1	královský
služebníka	služebník	k1gMnSc2	služebník
jenom	jenom	k6eAd1	jenom
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
na	na	k7c4	na
požádání	požádání	k1gNnSc4	požádání
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
je	být	k5eAaImIp3nS	být
živý	živý	k2eAgMnSc1d1	živý
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
nalezne	nalézt	k5eAaBmIp3nS	nalézt
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
živého	živý	k1gMnSc4	živý
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Apoštol	apoštol	k1gMnSc1	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
veřejného	veřejný	k2eAgNnSc2d1	veřejné
působení	působení	k1gNnSc2	působení
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
něho	on	k3xPp3gMnSc2	on
formoval	formovat	k5eAaImAgInS	formovat
okruh	okruh	k1gInSc4	okruh
přívrženců	přívrženec	k1gMnPc2	přívrženec
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
běžných	běžný	k2eAgMnPc2d1	běžný
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
počtu	počet	k1gInSc2	počet
stovek	stovka	k1gFnPc2	stovka
až	až	k9	až
tisíců	tisíc	k4xCgInPc2	tisíc
docházeli	docházet	k5eAaImAgMnP	docházet
na	na	k7c4	na
Ježíšova	Ježíšův	k2eAgNnPc4d1	Ježíšovo
kázání	kázání	k1gNnPc4	kázání
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
nablízku	nablízku	k6eAd1	nablízku
<g/>
,	,	kIx,	,
a	a	k8xC	a
poskytovali	poskytovat	k5eAaImAgMnP	poskytovat
mu	on	k3xPp3gMnSc3	on
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
pohostinství	pohostinství	k1gNnSc4	pohostinství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyhranila	vyhranit	k5eAaPmAgFnS	vyhranit
skupina	skupina	k1gFnSc1	skupina
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
učedníků	učedník	k1gMnPc2	učedník
<g/>
.	.	kIx.	.
</s>
<s>
Lukášovo	Lukášův	k2eAgNnSc1d1	Lukášovo
evangelium	evangelium	k1gNnSc1	evangelium
zná	znát	k5eAaImIp3nS	znát
širší	široký	k2eAgInSc4d2	širší
okruh	okruh	k1gInSc4	okruh
sedmdesáti	sedmdesát	k4xCc2	sedmdesát
či	či	k8xC	či
dvaasedmdesáti	dvaasedmdesát	k4xCc2	dvaasedmdesát
učedníků	učedník	k1gMnPc2	učedník
<g/>
,	,	kIx,	,
vyslaných	vyslaný	k2eAgMnPc2d1	vyslaný
Ježíšem	Ježíš	k1gMnSc7	Ježíš
jako	jako	k8xC	jako
hlasatelé	hlasatel	k1gMnPc1	hlasatel
božího	boží	k2eAgNnSc2d1	boží
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
však	však	k9	však
Ježíše	Ježíš	k1gMnSc4	Ježíš
následovalo	následovat	k5eAaImAgNnS	následovat
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
cestách	cesta	k1gFnPc6	cesta
"	"	kIx"	"
<g/>
Dvanáct	dvanáct	k4xCc4	dvanáct
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
učedníků	učedník	k1gMnPc2	učedník
dnes	dnes	k6eAd1	dnes
často	často	k6eAd1	často
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k9	jako
dvanáct	dvanáct	k4xCc1	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Šimonem	Šimon	k1gMnSc7	Šimon
Petrem	Petr	k1gMnSc7	Petr
<g/>
,	,	kIx,	,
Janem	Jan	k1gMnSc7	Jan
a	a	k8xC	a
Jakubem	Jakub	k1gMnSc7	Jakub
Starším	starší	k1gMnSc7	starší
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
biblického	biblický	k2eAgNnSc2d1	biblické
podání	podání	k1gNnSc2	podání
je	být	k5eAaImIp3nS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
sám	sám	k3xTgInSc4	sám
vybral	vybrat	k5eAaPmAgMnS	vybrat
a	a	k8xC	a
povolal	povolat	k5eAaPmAgMnS	povolat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
opustili	opustit	k5eAaPmAgMnP	opustit
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
rodinu	rodina	k1gFnSc4	rodina
(	(	kIx(	(
<g/>
přinejmenším	přinejmenším	k6eAd1	přinejmenším
Petr	Petr	k1gMnSc1	Petr
byl	být	k5eAaImAgMnS	být
ženat	ženat	k2eAgMnSc1d1	ženat
<g/>
)	)	kIx)	)
a	a	k8xC	a
putovali	putovat	k5eAaImAgMnP	putovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dobovým	dobový	k2eAgFnPc3d1	dobová
zvyklostem	zvyklost	k1gFnPc3	zvyklost
neobvykle	obvykle	k6eNd1	obvykle
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
byl	být	k5eAaImAgInS	být
Ježíšův	Ježíšův	k2eAgInSc4d1	Ježíšův
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
matky	matka	k1gFnSc2	matka
Marie	Maria	k1gFnSc2	Maria
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
objevují	objevovat	k5eAaImIp3nP	objevovat
Marie	Marie	k1gFnSc1	Marie
Magdalská	Magdalský	k2eAgFnSc1d1	Magdalská
<g/>
,	,	kIx,	,
Lazarovy	Lazarův	k2eAgFnPc1d1	Lazarova
sestry	sestra	k1gFnPc1	sestra
Marie	Marie	k1gFnSc1	Marie
a	a	k8xC	a
Marta	Marta	k1gFnSc1	Marta
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Jakuba	Jakub	k1gMnSc2	Jakub
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Salomé	Salomý	k2eAgFnSc2d1	Salomý
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
prostředků	prostředek	k1gInPc2	prostředek
Ježíše	Ježíš	k1gMnSc2	Ježíš
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
učedníky	učedník	k1gMnPc7	učedník
podporovaly	podporovat	k5eAaImAgFnP	podporovat
a	a	k8xC	a
některé	některý	k3yIgMnPc4	některý
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
doprovázely	doprovázet	k5eAaImAgFnP	doprovázet
<g/>
.	.	kIx.	.
<g/>
Lk	Lk	k1gFnPc2	Lk
8	[number]	k4	8
<g/>
,	,	kIx,	,
1	[number]	k4	1
(	(	kIx(	(
<g/>
Kral	Kral	k1gInSc1	Kral
<g/>
,	,	kIx,	,
ČEP	čep	k1gInSc1	čep
<g/>
)	)	kIx)	)
Ženy	žena	k1gFnPc1	žena
hrají	hrát	k5eAaImIp3nP	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
novozákonních	novozákonní	k2eAgInPc6d1	novozákonní
příbězích	příběh	k1gInPc6	příběh
a	a	k8xC	a
Ježíš	Ježíš	k1gMnSc1	Ježíš
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
jednal	jednat	k5eAaImAgMnS	jednat
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
vážností	vážnost	k1gFnSc7	vážnost
jako	jako	k8xS	jako
s	s	k7c7	s
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Pašije	pašije	k1gFnSc1	pašije
a	a	k8xC	a
Ukřižování	ukřižování	k1gNnSc1	ukřižování
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
kanonická	kanonický	k2eAgNnPc1d1	kanonické
evangelia	evangelium	k1gNnPc1	evangelium
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
na	na	k7c6	na
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
byl	být	k5eAaImAgMnS	být
popraven	popravit	k5eAaPmNgMnS	popravit
ukřižováním	ukřižování	k1gNnSc7	ukřižování
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
při	pře	k1gFnSc4	pře
nebo	nebo	k8xC	nebo
před	před	k7c7	před
židovským	židovský	k2eAgInSc7d1	židovský
svátkem	svátek	k1gInSc7	svátek
Pesach	pesach	k1gInSc1	pesach
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
popravě	poprava	k1gFnSc3	poprava
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c2	za
působení	působení	k1gNnSc2	působení
Pontia	Pontius	k1gMnSc2	Pontius
Piláta	Pilát	k1gMnSc2	Pilát
<g/>
,	,	kIx,	,
prefekta	prefekt	k1gMnSc2	prefekt
judské	judský	k2eAgFnSc2d1	Judská
provincie	provincie	k1gFnSc2	provincie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
26	[number]	k4	26
až	až	k9	až
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
zvláště	zvláště	k6eAd1	zvláště
potupnému	potupný	k2eAgInSc3d1	potupný
způsobu	způsob	k1gInSc3	způsob
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kanonická	kanonický	k2eAgNnPc1d1	kanonické
evangelia	evangelium	k1gNnPc1	evangelium
líčí	líčit	k5eAaImIp3nP	líčit
<g/>
,	,	kIx,	,
považují	považovat	k5eAaImIp3nP	považovat
historikové	historik	k1gMnPc1	historik
Ježíšovo	Ježíšův	k2eAgNnSc4d1	Ježíšovo
ukřižování	ukřižování	k1gNnSc4	ukřižování
za	za	k7c4	za
historicky	historicky	k6eAd1	historicky
zcela	zcela	k6eAd1	zcela
hodnověrné	hodnověrný	k2eAgNnSc1d1	hodnověrné
<g/>
.	.	kIx.	.
</s>
<s>
Svědectví	svědectví	k1gNnSc1	svědectví
je	být	k5eAaImIp3nS	být
podepřeno	podepřít	k5eAaPmNgNnS	podepřít
též	též	k6eAd1	též
židovskými	židovský	k2eAgInPc7d1	židovský
prameny	pramen	k1gInPc7	pramen
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Markovo	Markův	k2eAgNnSc1d1	Markovo
evangelium	evangelium	k1gNnSc1	evangelium
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
začátek	začátek	k1gInSc1	začátek
Pesachu	pesach	k1gInSc2	pesach
<g/>
,	,	kIx,	,
pátek	pátek	k1gInSc1	pátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
nisanu	nisan	k1gInSc2	nisan
<g/>
,	,	kIx,	,
Janovo	Janův	k2eAgNnSc1d1	Janovo
evangelium	evangelium	k1gNnSc1	evangelium
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
odpoledni	odpoledne	k1gNnSc6	odpoledne
14	[number]	k4	14
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
měsíce	měsíc	k1gInSc2	měsíc
nisanu	nisan	k1gInSc2	nisan
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
předvečeru	předvečer	k1gInSc3	předvečer
svátku	svátek	k1gInSc2	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
si	se	k3xPyFc3	se
představit	představit	k5eAaPmF	představit
Ježíšovu	Ježíšův	k2eAgFnSc4d1	Ježíšova
popravu	poprava	k1gFnSc4	poprava
právě	právě	k9	právě
během	během	k7c2	během
největší	veliký	k2eAgFnSc2d3	veliký
ze	z	k7c2	z
židovských	židovská	k1gFnPc2	židovská
slavností	slavnost	k1gFnPc2	slavnost
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
hlediska	hledisko	k1gNnSc2	hledisko
Janovo	Janův	k2eAgNnSc4d1	Janovo
vyprávění	vyprávění	k1gNnSc4	vyprávění
přesnější	přesný	k2eAgMnSc1d2	přesnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
letech	let	k1gInPc6	let
pak	pak	k6eAd1	pak
14	[number]	k4	14
<g/>
.	.	kIx.	.
nisan	nisan	k1gInSc1	nisan
připadal	připadat	k5eAaImAgInS	připadat
na	na	k7c4	na
pátek	pátek	k1gInSc4	pátek
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
30	[number]	k4	30
nebo	nebo	k8xC	nebo
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
možno	možno	k6eAd1	možno
spolehnout	spolehnout	k5eAaPmF	spolehnout
na	na	k7c4	na
údaj	údaj	k1gInSc4	údaj
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
Ježíši	Ježíš	k1gMnSc3	Ježíš
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
jeho	jeho	k3xOp3gNnSc2	jeho
veřejného	veřejný	k2eAgNnSc2d1	veřejné
působení	působení	k1gNnSc2	působení
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
a	a	k8xC	a
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
působení	působení	k1gNnSc1	působení
trvalo	trvat	k5eAaImAgNnS	trvat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
připadá	připadat	k5eAaPmIp3nS	připadat
jako	jako	k9	jako
nejpravděpodobnější	pravděpodobný	k2eAgNnSc1d3	nejpravděpodobnější
datum	datum	k1gNnSc1	datum
Ježíšovy	Ježíšův	k2eAgFnSc2d1	Ježíšova
smrti	smrt	k1gFnSc2	smrt
7	[number]	k4	7
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
roku	rok	k1gInSc2	rok
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
pro	pro	k7c4	pro
obžalobu	obžaloba	k1gFnSc4	obžaloba
Ježíšovu	Ježíšův	k2eAgFnSc4d1	Ježíšova
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
odsouzení	odsouzení	k1gNnPc2	odsouzení
byl	být	k5eAaImAgInS	být
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
rozličné	rozličný	k2eAgInPc4d1	rozličný
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
působení	působení	k1gNnSc6	působení
bylo	být	k5eAaImAgNnS	být
vícero	vícero	k4xRyIgNnSc1	vícero
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
část	část	k1gFnSc4	část
židovského	židovský	k2eAgNnSc2d1	Židovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
těžko	těžko	k6eAd1	těžko
stravitelné	stravitelný	k2eAgNnSc1d1	stravitelné
<g/>
:	:	kIx,	:
vyhnání	vyhnání	k1gNnSc1	vyhnání
penězoměnců	penězoměnec	k1gMnPc2	penězoměnec
z	z	k7c2	z
jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
spory	spor	k1gInPc1	spor
s	s	k7c7	s
farizeji	farizej	k1gMnPc7	farizej
a	a	k8xC	a
saduceji	saducej	k1gMnPc7	saducej
<g/>
,	,	kIx,	,
učení	učení	k1gNnSc3	učení
o	o	k7c6	o
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
těle	tělo	k1gNnSc6	tělo
coby	coby	k?	coby
pokrmu	pokrm	k1gInSc2	pokrm
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Hlásání	hlásání	k1gNnSc1	hlásání
božího	boží	k2eAgNnSc2d1	boží
království	království	k1gNnSc2	království
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
také	také	k9	také
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xS	jako
politicky	politicky	k6eAd1	politicky
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
jak	jak	k8xC	jak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
tak	tak	k9	tak
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nemožné	možný	k2eNgNnSc1d1	nemožné
posoudit	posoudit	k5eAaPmF	posoudit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
sehrál	sehrát	k5eAaPmAgMnS	sehrát
při	při	k7c6	při
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
odsouzení	odsouzení	k1gNnSc6	odsouzení
hlavní	hlavní	k2eAgFnSc4d1	hlavní
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
všech	všecek	k3xTgNnPc2	všecek
evangelií	evangelium	k1gNnPc2	evangelium
bylo	být	k5eAaImAgNnS	být
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
tělo	tělo	k1gNnSc1	tělo
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
sňato	sňat	k2eAgNnSc1d1	sňato
z	z	k7c2	z
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
zabaleno	zabalen	k2eAgNnSc1d1	zabaleno
do	do	k7c2	do
pláten	plátno	k1gNnPc2	plátno
a	a	k8xC	a
položeno	položen	k2eAgNnSc1d1	položeno
do	do	k7c2	do
skalního	skalní	k2eAgInSc2d1	skalní
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
patřil	patřit	k5eAaImAgMnS	patřit
Josefu	Josef	k1gMnSc3	Josef
z	z	k7c2	z
Arimatie	Arimatie	k1gFnSc2	Arimatie
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
rozdílnost	rozdílnost	k1gFnSc4	rozdílnost
vyprávění	vyprávění	k1gNnSc2	vyprávění
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
evangelií	evangelium	k1gNnPc2	evangelium
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
konstatovat	konstatovat	k5eAaBmF	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
ženy	žena	k1gFnPc1	žena
šly	jít	k5eAaImAgFnP	jít
posléze	posléze	k6eAd1	posléze
navštívit	navštívit	k5eAaPmF	navštívit
Ježíšův	Ježíšův	k2eAgInSc4d1	Ježíšův
hrob	hrob	k1gInSc4	hrob
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
tam	tam	k6eAd1	tam
již	již	k6eAd1	již
nenašly	najít	k5eNaPmAgFnP	najít
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
tento	tento	k3xDgInSc4	tento
prázdný	prázdný	k2eAgInSc4d1	prázdný
hrob	hrob	k1gInSc4	hrob
vysvětlovala	vysvětlovat	k5eAaImAgFnS	vysvětlovat
Ježíšovým	Ježíšův	k2eAgNnSc7d1	Ježíšovo
vzkříšením	vzkříšení	k1gNnSc7	vzkříšení
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
velerada	velerada	k1gFnSc1	velerada
vysvětlovala	vysvětlovat	k5eAaImAgFnS	vysvětlovat
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
krádeží	krádež	k1gFnPc2	krádež
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
spáchali	spáchat	k5eAaPmAgMnP	spáchat
jeho	jeho	k3xOp3gMnPc1	jeho
učedníci	učedník	k1gMnPc1	učedník
<g/>
,	,	kIx,	,
když	když	k8xS	když
římská	římský	k2eAgFnSc1d1	římská
hlídka	hlídka	k1gFnSc1	hlídka
u	u	k7c2	u
hrobu	hrob	k1gInSc2	hrob
spala	spát	k5eAaImAgFnS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
církev	církev	k1gFnSc1	církev
nedokazovala	dokazovat	k5eNaImAgFnS	dokazovat
Ježíšovo	Ježíšův	k2eAgNnSc4d1	Ježíšovo
vzkříšení	vzkříšení	k1gNnSc4	vzkříšení
z	z	k7c2	z
faktu	fakt	k1gInSc2	fakt
prázdného	prázdný	k2eAgInSc2d1	prázdný
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
zjeveními	zjevení	k1gNnPc7	zjevení
Ježíše	Ježíš	k1gMnSc2	Ježíš
jeho	on	k3xPp3gInSc4	on
učedníkům	učedník	k1gMnPc3	učedník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
začínají	začínat	k5eAaImIp3nP	začínat
hlásat	hlásat	k5eAaImF	hlásat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
Ježíše	Ježíš	k1gMnSc4	Ježíš
vzkřísil	vzkřísit	k5eAaPmAgMnS	vzkřísit
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
snahy	snaha	k1gFnPc1	snaha
vysvětlovat	vysvětlovat	k5eAaImF	vysvětlovat
tato	tento	k3xDgNnPc4	tento
zjevení	zjevení	k1gNnPc4	zjevení
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc4	důsledek
euforie	euforie	k1gFnSc2	euforie
či	či	k8xC	či
halucinace	halucinace	k1gFnSc2	halucinace
učedníků	učedník	k1gMnPc2	učedník
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jako	jako	k8xC	jako
podvod	podvod	k1gInSc4	podvod
jejich	jejich	k3xOp3gInSc4	jejich
či	či	k8xC	či
pisatelů	pisatel	k1gMnPc2	pisatel
evangelií	evangelium	k1gNnPc2	evangelium
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
novozákonních	novozákonní	k2eAgInPc2d1	novozákonní
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
důvěryhodnost	důvěryhodnost	k1gFnSc1	důvěryhodnost
těchto	tento	k3xDgFnPc2	tento
teorií	teorie	k1gFnPc2	teorie
zpochybňuje	zpochybňovat	k5eAaImIp3nS	zpochybňovat
vícero	vícero	k1gNnSc1	vícero
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
pak	pak	k6eAd1	pak
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
apoštolové	apoštol	k1gMnPc1	apoštol
museli	muset	k5eAaImAgMnP	muset
pro	pro	k7c4	pro
obhajobu	obhajoba	k1gFnSc4	obhajoba
svého	svůj	k3xOyFgNnSc2	svůj
tvrzení	tvrzení	k1gNnSc2	tvrzení
ustát	ustát	k5eAaPmF	ustát
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
podvodu	podvod	k1gInSc2	podvod
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
vzkříšení	vzkříšení	k1gNnSc6	vzkříšení
byly	být	k5eAaImAgInP	být
přesvědčivější	přesvědčivý	k2eAgInPc1d2	přesvědčivější
a	a	k8xC	a
harmoničtější	harmonický	k2eAgInPc1d2	harmoničtější
<g/>
:	:	kIx,	:
rozdíly	rozdíl	k1gInPc1	rozdíl
a	a	k8xC	a
rozpory	rozpor	k1gInPc1	rozpor
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
prameny	pramen	k1gInPc7	pramen
tak	tak	k6eAd1	tak
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
společné	společný	k2eAgNnSc4d1	společné
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nanebevstoupení	nanebevstoupení	k1gNnSc2	nanebevstoupení
Páně	páně	k2eAgNnSc2d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
Matoušovo	Matoušův	k2eAgNnSc1d1	Matoušovo
a	a	k8xC	a
Lukášovo	Lukášův	k2eAgNnSc1d1	Lukášovo
evangelium	evangelium	k1gNnSc1	evangelium
nekončí	končit	k5eNaImIp3nS	končit
prázdným	prázdný	k2eAgInSc7d1	prázdný
hrobem	hrob	k1gInSc7	hrob
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
učedníci	učedník	k1gMnPc1	učedník
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
ženy	žena	k1gFnPc1	žena
setkaly	setkat	k5eAaPmAgFnP	setkat
se	s	k7c7	s
vzkříšeným	vzkříšený	k2eAgMnSc7d1	vzkříšený
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
popisují	popisovat	k5eAaImIp3nP	popisovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
nanebevstoupení	nanebevstoupení	k1gNnSc1	nanebevstoupení
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
tehdy	tehdy	k6eAd1	tehdy
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
ke	k	k7c3	k
svému	svůj	k1gMnSc3	svůj
Otci	otec	k1gMnSc3	otec
a	a	k8xC	a
dal	dát	k5eAaPmAgInS	dát
učedníkům	učedník	k1gMnPc3	učedník
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
velké	velký	k2eAgNnSc4d1	velké
poslání	poslání	k1gNnSc4	poslání
-	-	kIx~	-
jít	jít	k5eAaImF	jít
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
počínajíc	počínajíc	k7c7	počínajíc
Jeruzalémem	Jeruzalém	k1gInSc7	Jeruzalém
<g/>
,	,	kIx,	,
Judskem	Judsko	k1gNnSc7	Judsko
a	a	k8xC	a
Samařím	Samaří	k1gNnSc7	Samaří
<g/>
)	)	kIx)	)
a	a	k8xC	a
zvěstovat	zvěstovat	k5eAaImF	zvěstovat
evangelium	evangelium	k1gNnSc4	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
bude	být	k5eAaImBp3nS	být
vést	vést	k5eAaImF	vést
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
Duch	duch	k1gMnSc1	duch
svatý	svatý	k1gMnSc1	svatý
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jenž	k3xRgNnSc4	jenž
seslání	seslání	k1gNnSc4	seslání
měli	mít	k5eAaImAgMnP	mít
počkat	počkat	k5eAaPmF	počkat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
okamžik	okamžik	k1gInSc1	okamžik
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
začínají	začínat	k5eAaImIp3nP	začínat
dějiny	dějiny	k1gFnPc1	dějiny
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Apokryf	apokryf	k1gInSc4	apokryf
a	a	k8xC	a
Evangelium	evangelium	k1gNnSc4	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Ježíšovi	Ježíš	k1gMnSc6	Ježíš
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
do	do	k7c2	do
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
apokryfní	apokryfní	k2eAgFnPc4d1	apokryfní
a	a	k8xC	a
pseudepigrafní	pseudepigrafní	k2eAgFnPc4d1	pseudepigrafní
knihy	kniha	k1gFnPc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
evangelia	evangelium	k1gNnSc2	evangelium
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
líčí	líčit	k5eAaImIp3nS	líčit
Ježíšův	Ježíšův	k2eAgInSc1d1	Ježíšův
pobyt	pobyt	k1gInSc1	pobyt
s	s	k7c7	s
rodinu	rodina	k1gFnSc4	rodina
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
taková	takový	k3xDgNnPc4	takový
evangelia	evangelium	k1gNnPc4	evangelium
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Tomášovo	Tomášův	k2eAgNnSc1d1	Tomášovo
evangelium	evangelium	k1gNnSc1	evangelium
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravoslavných	pravoslavný	k2eAgFnPc6d1	pravoslavná
církvích	církev	k1gFnPc6	církev
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgFnSc1d1	populární
také	také	k9	také
alternativní	alternativní	k2eAgNnSc1d1	alternativní
evangelium	evangelium	k1gNnSc1	evangelium
o	o	k7c6	o
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
narození	narození	k1gNnSc6	narození
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Jakubovo	Jakubův	k2eAgNnSc1d1	Jakubovo
evangelium	evangelium	k1gNnSc1	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
apokryfy	apokryf	k1gInPc1	apokryf
o	o	k7c6	o
Ježíšovi	Ježíš	k1gMnSc6	Ježíš
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
různá	různý	k2eAgNnPc4d1	různé
logia	logium	k1gNnPc4	logium
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ztracené	ztracený	k2eAgInPc4d1	ztracený
<g/>
"	"	kIx"	"
výroky	výrok	k1gInPc4	výrok
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Filipovo	Filipův	k2eAgNnSc1d1	Filipovo
nebo	nebo	k8xC	nebo
Tomášovo	Tomášův	k2eAgNnSc1d1	Tomášovo
evangelium	evangelium	k1gNnSc1	evangelium
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vyprávění	vyprávění	k1gNnSc1	vyprávění
samotných	samotný	k2eAgMnPc2d1	samotný
apoštolů	apoštol	k1gMnPc2	apoštol
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Bartolomějovo	Bartolomějův	k2eAgNnSc1d1	Bartolomějovo
evangelium	evangelium	k1gNnSc1	evangelium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
některá	některý	k3yIgNnPc4	některý
mystická	mystický	k2eAgNnPc4d1	mystické
učení	učení	k1gNnPc4	učení
raných	raný	k2eAgMnPc2d1	raný
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
křesťanství	křesťanství	k1gNnSc1	křesťanství
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
Ježíšovo	Ježíšův	k2eAgNnSc4d1	Ježíšovo
učení	učení	k1gNnSc4	učení
<g/>
,	,	kIx,	,
důrazy	důraz	k1gInPc4	důraz
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
přesunuly	přesunout	k5eAaPmAgInP	přesunout
a	a	k8xC	a
rozvinuly	rozvinout	k5eAaPmAgInP	rozvinout
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
kulturním	kulturní	k2eAgInSc6d1	kulturní
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
myšlenkové	myšlenkový	k2eAgInPc1d1	myšlenkový
a	a	k8xC	a
společenské	společenský	k2eAgInPc1d1	společenský
proudy	proud	k1gInPc1	proud
vzešlé	vzešlý	k2eAgInPc1d1	vzešlý
z	z	k7c2	z
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
působení	působení	k1gNnSc2	působení
byly	být	k5eAaImAgInP	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
proudem	proud	k1gInSc7	proud
křesťanství	křesťanství	k1gNnSc2	křesťanství
odmítnuty	odmítnout	k5eAaPmNgInP	odmítnout
jako	jako	k9	jako
heretické	heretický	k2eAgInPc1d1	heretický
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
obecně	obecně	k6eAd1	obecně
nejsou	být	k5eNaImIp3nP	být
pokládány	pokládán	k2eAgFnPc1d1	pokládána
za	za	k7c4	za
typicky	typicky	k6eAd1	typicky
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
původní	původní	k2eAgNnSc4d1	původní
Ježíšovo	Ježíšův	k2eAgNnSc4d1	Ježíšovo
učení	učení	k1gNnSc4	učení
<g/>
,	,	kIx,	,
oproštěné	oproštěný	k2eAgFnPc4d1	oproštěná
od	od	k7c2	od
pozdějších	pozdní	k2eAgInPc2d2	pozdější
kulturních	kulturní	k2eAgInPc2d1	kulturní
nánosů	nános	k1gInPc2	nános
a	a	k8xC	a
interpretací	interpretace	k1gFnPc2	interpretace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nesnadným	snadný	k2eNgInSc7d1	nesnadný
úkolem	úkol	k1gInSc7	úkol
<g/>
;	;	kIx,	;
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
způsobů	způsob	k1gInPc2	způsob
tohoto	tento	k3xDgNnSc2	tento
bádání	bádání	k1gNnSc2	bádání
představuje	představovat	k5eAaImIp3nS	představovat
historicko-kritická	historickoritický	k2eAgFnSc1d1	historicko-kritická
metoda	metoda	k1gFnSc1	metoda
četby	četba	k1gFnSc2	četba
spisů	spis	k1gInPc2	spis
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
učení	učení	k1gNnSc1	učení
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
židovské	židovský	k2eAgFnPc4d1	židovská
tradice	tradice	k1gFnPc4	tradice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
<g/>
,	,	kIx,	,
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
prorockou	prorocký	k2eAgFnSc4d1	prorocká
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
hlásá	hlásat	k5eAaImIp3nS	hlásat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
zvěst	zvěst	k1gFnSc4	zvěst
(	(	kIx(	(
<g/>
ε	ε	k?	ε
euangelion	euangelion	k1gInSc1	euangelion
<g/>
,	,	kIx,	,
evangelium	evangelium	k1gNnSc1	evangelium
<g/>
)	)	kIx)	)
o	o	k7c4	o
osvobození	osvobození	k1gNnSc4	osvobození
od	od	k7c2	od
jha	jho	k1gNnSc2	jho
formálních	formální	k2eAgInPc2d1	formální
a	a	k8xC	a
rituálních	rituální	k2eAgInPc2d1	rituální
náboženských	náboženský	k2eAgInPc2d1	náboženský
požadavků	požadavek	k1gInPc2	požadavek
a	a	k8xC	a
znovuobjevení	znovuobjevení	k1gNnSc4	znovuobjevení
a	a	k8xC	a
posílení	posílení	k1gNnSc4	posílení
jejich	jejich	k3xOp3gInPc2	jejich
jednotících	jednotící	k2eAgInPc2d1	jednotící
principů	princip	k1gInPc2	princip
<g/>
:	:	kIx,	:
autentičnosti	autentičnost	k1gFnSc2	autentičnost
<g/>
,	,	kIx,	,
plnosti	plnost	k1gFnSc2	plnost
a	a	k8xC	a
neokázalosti	neokázalost	k1gFnSc2	neokázalost
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
k	k	k7c3	k
bližnímu	bližní	k1gMnSc3	bližní
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
osvobození	osvobození	k1gNnSc1	osvobození
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
pokáním	pokání	k1gNnSc7	pokání
<g/>
,	,	kIx,	,
obrácením	obrácení	k1gNnSc7	obrácení
<g/>
,	,	kIx,	,
změnou	změna	k1gFnSc7	změna
smýšlení	smýšlení	k1gNnSc2	smýšlení
<g/>
.	.	kIx.	.
</s>
<s>
Požadavky	požadavek	k1gInPc1	požadavek
jsou	být	k5eAaImIp3nP	být
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
na	na	k7c4	na
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
a	a	k8xC	a
smýšlení	smýšlení	k1gNnSc4	smýšlení
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
na	na	k7c4	na
vzpouru	vzpoura	k1gFnSc4	vzpoura
vůči	vůči	k7c3	vůči
státnímu	státní	k2eAgMnSc3d1	státní
nebo	nebo	k8xC	nebo
náboženskému	náboženský	k2eAgInSc3d1	náboženský
systému	systém	k1gInSc3	systém
jako	jako	k8xS	jako
celku	celek	k1gInSc3	celek
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
podporuje	podporovat	k5eAaImIp3nS	podporovat
pokorné	pokorný	k2eAgFnPc4d1	pokorná
<g/>
,	,	kIx,	,
upřímné	upřímný	k2eAgFnPc4d1	upřímná
<g/>
,	,	kIx,	,
statečné	statečný	k2eAgFnPc4d1	statečná
<g/>
,	,	kIx,	,
spravedlivé	spravedlivý	k2eAgFnPc4d1	spravedlivá
<g/>
,	,	kIx,	,
utiskované	utiskovaný	k2eAgFnPc4d1	utiskovaná
<g/>
,	,	kIx,	,
nemocné	nemocný	k2eAgFnPc4d1	nemocná
atd.	atd.	kA	atd.
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
hrozí	hrozit	k5eAaImIp3nS	hrozit
pyšným	pyšný	k2eAgMnPc3d1	pyšný
<g/>
,	,	kIx,	,
pokrytcům	pokrytec	k1gMnPc3	pokrytec
<g/>
,	,	kIx,	,
lidem	člověk	k1gMnPc3	člověk
parazitujícím	parazitující	k2eAgNnSc7d1	parazitující
na	na	k7c4	na
náboženství	náboženství	k1gNnSc4	náboženství
k	k	k7c3	k
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
prospěchu	prospěch	k1gInSc3	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
milosrdenství	milosrdenství	k1gNnSc4	milosrdenství
<g/>
,	,	kIx,	,
odpuštění	odpuštění	k1gNnSc4	odpuštění
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
,	,	kIx,	,
sebekritičnost	sebekritičnost	k1gFnSc1	sebekritičnost
<g/>
,	,	kIx,	,
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
za	za	k7c4	za
aktivní	aktivní	k2eAgNnSc4d1	aktivní
využití	využití	k1gNnSc4	využití
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
podobenství	podobenství	k1gNnSc4	podobenství
o	o	k7c6	o
hřivnách	hřivna	k1gFnPc6	hřivna
<g/>
,	,	kIx,	,
Mt	Mt	k1gFnSc1	Mt
25	[number]	k4	25
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
smysl	smysl	k1gInSc4	smysl
sebezáporu	sebezápor	k1gInSc2	sebezápor
<g/>
,	,	kIx,	,
utrpení	utrpení	k1gNnSc4	utrpení
a	a	k8xC	a
snášení	snášení	k1gNnSc4	snášení
obtíží	obtíž	k1gFnPc2	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Etické	etický	k2eAgInPc1d1	etický
principy	princip	k1gInPc1	princip
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
a	a	k8xC	a
milosrdenství	milosrdenství	k1gNnSc4	milosrdenství
k	k	k7c3	k
bližním	bližní	k1gMnPc3	bližní
klade	klást	k5eAaImIp3nS	klást
nad	nad	k7c7	nad
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
<g/>
,	,	kIx,	,
etnickou	etnický	k2eAgFnSc7d1	etnická
či	či	k8xC	či
rodinnou	rodinný	k2eAgFnSc7d1	rodinná
příslušností	příslušnost	k1gFnSc7	příslušnost
i	i	k9	i
nad	nad	k7c4	nad
planou	planý	k2eAgFnSc4d1	planá
servilitu	servilita	k1gFnSc4	servilita
vůči	vůči	k7c3	vůči
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
nejen	nejen	k6eAd1	nejen
ovocem	ovoce	k1gNnSc7	ovoce
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zázraky	zázrak	k1gInPc1	zázrak
a	a	k8xC	a
mocnými	mocný	k2eAgInPc7d1	mocný
činy	čin	k1gInPc7	čin
<g/>
;	;	kIx,	;
současně	současně	k6eAd1	současně
v	v	k7c6	v
podobenstvích	podobenství	k1gNnPc6	podobenství
a	a	k8xC	a
kázáních	kázání	k1gNnPc6	kázání
podporoval	podporovat	k5eAaImAgInS	podporovat
racionální	racionální	k2eAgNnSc4d1	racionální
uvažování	uvažování	k1gNnSc4	uvažování
a	a	k8xC	a
rozeznávání	rozeznávání	k1gNnSc4	rozeznávání
"	"	kIx"	"
<g/>
znamení	znamení	k1gNnSc1	znamení
doby	doba	k1gFnSc2	doba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
novozákonního	novozákonní	k2eAgNnSc2d1	novozákonní
podání	podání	k1gNnSc2	podání
Ježíš	ježit	k5eAaImIp2nS	ježit
cílevědomě	cílevědomě	k6eAd1	cílevědomě
organizoval	organizovat	k5eAaBmAgMnS	organizovat
šíření	šíření	k1gNnSc4	šíření
svého	svůj	k3xOyFgNnSc2	svůj
učení	učení	k1gNnSc2	učení
příkladem	příklad	k1gInSc7	příklad
i	i	k8xC	i
výkladem	výklad	k1gInSc7	výklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
jeruzalémských	jeruzalémský	k2eAgFnPc2d1	Jeruzalémská
událostí	událost	k1gFnPc2	událost
před	před	k7c7	před
Ježíšovým	Ježíšův	k2eAgNnSc7d1	Ježíšovo
ukřižováním	ukřižování	k1gNnSc7	ukřižování
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
myšlenka	myšlenka	k1gFnSc1	myšlenka
výkupné	výkupný	k2eAgFnSc2d1	výkupná
hodnoty	hodnota	k1gFnSc2	hodnota
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
utrpení	utrpení	k1gNnSc2	utrpení
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Ježíšově	Ježíšův	k2eAgInSc6d1	Ježíšův
proslovu	proslov	k1gInSc6	proslov
nad	nad	k7c7	nad
chlebem	chléb	k1gInSc7	chléb
při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
večeři	večeře	k1gFnSc6	večeře
(	(	kIx(	(
<g/>
moje	můj	k3xOp1gNnSc1	můj
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
láme	lámat	k5eAaImIp3nS	lámat
<g/>
/	/	kIx~	/
<g/>
vydává	vydávat	k5eAaPmIp3nS	vydávat
za	za	k7c4	za
vás	vy	k3xPp2nPc4	vy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vývoje	vývoj	k1gInSc2	vývoj
křesťanství	křesťanství	k1gNnSc2	křesťanství
propracována	propracovat	k5eAaPmNgFnS	propracovat
do	do	k7c2	do
ucelené	ucelený	k2eAgFnSc2d1	ucelená
teologie	teologie	k1gFnSc2	teologie
vykoupení	vykoupení	k1gNnSc2	vykoupení
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
navazuje	navazovat	k5eAaImIp3nS	navazovat
i	i	k9	i
teologie	teologie	k1gFnSc1	teologie
svátostí	svátost	k1gFnPc2	svátost
<g/>
.	.	kIx.	.
</s>
<s>
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
sebepojetí	sebepojetí	k1gNnSc1	sebepojetí
není	být	k5eNaImIp3nS	být
z	z	k7c2	z
evangelií	evangelium	k1gNnPc2	evangelium
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
i	i	k9	i
v	v	k7c6	v
teologických	teologický	k2eAgInPc6d1	teologický
sporech	spor	k1gInPc6	spor
raného	raný	k2eAgNnSc2d1	rané
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
je	být	k5eAaImIp3nS	být
křesťany	křesťan	k1gMnPc4	křesťan
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
Božího	boží	k2eAgMnSc4d1	boží
Syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
vtělil	vtělit	k5eAaPmAgMnS	vtělit
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
zabit	zabit	k2eAgInSc1d1	zabit
(	(	kIx(	(
<g/>
ukřižován	ukřižován	k2eAgInSc1d1	ukřižován
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
vstal	vstát	k5eAaPmAgMnS	vstát
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vzkříšen	vzkříšen	k2eAgInSc1d1	vzkříšen
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
zázračná	zázračný	k2eAgFnSc1d1	zázračná
událost	událost	k1gFnSc1	událost
přináší	přinášet	k5eAaImIp3nS	přinášet
všem	všecek	k3xTgMnPc3	všecek
jeho	jeho	k3xOp3gMnPc3	jeho
následovníkům	následovník	k1gMnPc3	následovník
spásu	spása	k1gFnSc4	spása
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
a	a	k8xC	a
působení	působení	k1gNnSc1	působení
je	být	k5eAaImIp3nS	být
středem	střed	k1gInSc7	střed
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
ho	on	k3xPp3gMnSc4	on
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
(	(	kIx(	(
<g/>
v	v	k7c6	v
religionistice	religionistika	k1gFnSc6	religionistika
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
přisuzováním	přisuzování	k1gNnSc7	přisuzování
založení	založení	k1gNnSc4	založení
křesťanství	křesťanství	k1gNnSc2	křesťanství
sv.	sv.	kA	sv.
Pavlovi	Pavel	k1gMnSc6	Pavel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
je	být	k5eAaImIp3nS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
očekávaným	očekávaný	k2eAgMnSc7d1	očekávaný
Mesiášem	Mesiáš	k1gMnSc7	Mesiáš
<g/>
,	,	kIx,	,
Spasitelem	spasitel	k1gMnSc7	spasitel
<g/>
,	,	kIx,	,
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
lidem	člověk	k1gMnPc3	člověk
přinesl	přinést	k5eAaPmAgMnS	přinést
spásu	spása	k1gFnSc4	spása
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
prostředníkem	prostředník	k1gInSc7	prostředník
mezi	mezi	k7c7	mezi
Bohem	bůh	k1gMnSc7	bůh
a	a	k8xC	a
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
Božího	boží	k2eAgNnSc2d1	boží
zjevení	zjevení	k1gNnSc2	zjevení
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgFnPc1	veškerý
dějiny	dějiny	k1gFnPc1	dějiny
spásy	spása	k1gFnSc2	spása
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
svůj	svůj	k3xOyFgInSc4	svůj
střed	střed	k1gInSc4	střed
–	–	k?	–
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
hovořili	hovořit	k5eAaImAgMnP	hovořit
proroci	prorok	k1gMnPc1	prorok
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
Starý	starý	k2eAgInSc4d1	starý
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
království	království	k1gNnSc3	království
směřují	směřovat	k5eAaImIp3nP	směřovat
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Řecky	řecky	k6eAd1	řecky
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
Pantokrátór	Pantokrátór	k1gInSc1	Pantokrátór
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vševládný	vševládný	k2eAgInSc1d1	vševládný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
též	též	k9	též
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
povede	povést	k5eAaPmIp3nS	povést
poslední	poslední	k2eAgInSc1d1	poslední
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
tradice	tradice	k1gFnSc2	tradice
byl	být	k5eAaImAgMnS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
i	i	k8xC	i
dokonalým	dokonalý	k2eAgMnSc7d1	dokonalý
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
jednání	jednání	k1gNnSc4	jednání
křesťana	křesťan	k1gMnSc2	křesťan
a	a	k8xC	a
základem	základ	k1gInSc7	základ
jeho	jeho	k3xOp3gFnSc2	jeho
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
křesťana	křesťan	k1gMnSc2	křesťan
má	mít	k5eAaImIp3nS	mít
odpovídat	odpovídat	k5eAaImF	odpovídat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Ježíš	Ježíš	k1gMnSc1	Ježíš
učil	učít	k5eAaPmAgMnS	učít
a	a	k8xC	a
jak	jak	k6eAd1	jak
jednal	jednat	k5eAaImAgMnS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
se	se	k3xPyFc4	se
latinsky	latinsky	k6eAd1	latinsky
označuje	označovat	k5eAaImIp3nS	označovat
imitatio	imitatio	k6eAd1	imitatio
Christi	Christ	k1gMnPc1	Christ
(	(	kIx(	(
<g/>
napodobování	napodobování	k1gNnSc1	napodobování
Krista	Kristus	k1gMnSc2	Kristus
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obdobou	obdoba	k1gFnSc7	obdoba
judaistického	judaistický	k2eAgInSc2d1	judaistický
imitatio	imitatio	k1gNnSc1	imitatio
Dei	Dei	k1gFnSc1	Dei
(	(	kIx(	(
<g/>
napodobování	napodobování	k1gNnSc1	napodobování
Boha	bůh	k1gMnSc2	bůh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Víra	víra	k1gFnSc1	víra
v	v	k7c4	v
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kým	kdo	k3yInSc7	kdo
Ježíš	ježit	k5eAaImIp2nS	ježit
skutečně	skutečně	k6eAd1	skutečně
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
velmi	velmi	k6eAd1	velmi
intenzivně	intenzivně	k6eAd1	intenzivně
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
etapami	etapa	k1gFnPc7	etapa
tohoto	tento	k3xDgInSc2	tento
vývoje	vývoj	k1gInSc2	vývoj
jsou	být	k5eAaImIp3nP	být
ekumenické	ekumenický	k2eAgInPc1d1	ekumenický
koncily	koncil	k1gInPc1	koncil
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
formulovaly	formulovat	k5eAaImAgInP	formulovat
hlavní	hlavní	k2eAgNnPc4d1	hlavní
christologická	christologický	k2eAgNnPc4d1	christologické
dogmata	dogma	k1gNnPc4	dogma
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
v	v	k7c4	v
Ježíše	Ježíš	k1gMnSc4	Ježíš
Krista	Kristus	k1gMnSc4	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
v	v	k7c4	v
Ježíše	Ježíš	k1gMnSc4	Ježíš
Krista	Kristus	k1gMnSc4	Kristus
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
koncil	koncil	k1gInSc1	koncil
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
451	[number]	k4	451
sešel	sejít	k5eAaPmAgMnS	sejít
v	v	k7c6	v
Chalkedonul	Chalkedonul	k1gFnSc6	Chalkedonul
(	(	kIx(	(
<g/>
451	[number]	k4	451
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
formulace	formulace	k1gFnPc1	formulace
"	"	kIx"	"
<g/>
jedna	jeden	k4xCgFnSc1	jeden
osoba	osoba	k1gFnSc1	osoba
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
přirozenostech	přirozenost	k1gFnPc6	přirozenost
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
božské	božský	k2eAgNnSc1d1	božské
a	a	k8xC	a
lidské	lidský	k2eAgNnSc1d1	lidské
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
smíšení	smíšení	k1gNnSc2	smíšení
<g/>
,	,	kIx,	,
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
oddělení	oddělení	k1gNnSc2	oddělení
a	a	k8xC	a
bez	bez	k7c2	bez
rozloučení	rozloučení	k1gNnSc2	rozloučení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
církve	církev	k1gFnPc1	církev
však	však	k9	však
tento	tento	k3xDgInSc4	tento
koncil	koncil	k1gInSc4	koncil
přijímají	přijímat	k5eAaImIp3nP	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
typologický	typologický	k2eAgInSc1d1	typologický
předobraz	předobraz	k1gInSc1	předobraz
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chápána	chápán	k2eAgFnSc1d1	chápána
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
z	z	k7c2	z
mesiánských	mesiánský	k2eAgFnPc2d1	mesiánská
postav	postava	k1gFnPc2	postava
(	(	kIx(	(
<g/>
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ježíše	Ježíš	k1gMnSc4	Ježíš
líčí	líčit	k5eAaImIp3nS	líčit
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
jako	jako	k8xS	jako
obřezaného	obřezaný	k2eAgMnSc2d1	obřezaný
Žida	Žid	k1gMnSc2	Žid
<g/>
,	,	kIx,	,
znalého	znalý	k2eAgInSc2d1	znalý
náboženského	náboženský	k2eAgInSc2d1	náboženský
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
lidé	člověk	k1gMnPc1	člověk
oslovují	oslovovat	k5eAaImIp3nP	oslovovat
jako	jako	k9	jako
rabbiho	rabbi	k1gMnSc4	rabbi
<g/>
,	,	kIx,	,
učitele	učitel	k1gMnSc4	učitel
Zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
generace	generace	k1gFnSc1	generace
křesťanů	křesťan	k1gMnPc2	křesťan
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
ze	z	k7c2	z
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
prvokřesťanské	prvokřesťanský	k2eAgNnSc1d1	prvokřesťanské
společenství	společenství	k1gNnSc1	společenství
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
dlouho	dlouho	k6eAd1	dlouho
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
četných	četný	k2eAgFnPc2d1	četná
židovských	židovský	k2eAgFnPc2d1	židovská
sekt	sekta	k1gFnPc2	sekta
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
novozákonní	novozákonní	k2eAgFnPc1d1	novozákonní
knihy	kniha	k1gFnPc1	kniha
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
křesťané	křesťan	k1gMnPc1	křesťan
účastnili	účastnit	k5eAaImAgMnP	účastnit
bohoslužebného	bohoslužebný	k2eAgInSc2d1	bohoslužebný
života	život	k1gInSc2	život
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
,	,	kIx,	,
se	s	k7c7	s
židy	žid	k1gMnPc7	žid
se	se	k3xPyFc4	se
stýkali	stýkat	k5eAaImAgMnP	stýkat
a	a	k8xC	a
považovali	považovat	k5eAaImAgMnP	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
následovníky	následovník	k1gMnPc4	následovník
judaismu	judaismus	k1gInSc2	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
existenci	existence	k1gFnSc3	existence
mnoha	mnoho	k4c2	mnoho
řecky	řecky	k6eAd1	řecky
hovořících	hovořící	k2eAgFnPc2d1	hovořící
židovských	židovský	k2eAgFnPc2d1	židovská
komunit	komunita	k1gFnPc2	komunita
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
se	se	k3xPyFc4	se
nové	nový	k2eAgNnSc1d1	nové
náboženství	náboženství	k1gNnSc1	náboženství
mohlo	moct	k5eAaImAgNnS	moct
rychle	rychle	k6eAd1	rychle
šířit	šířit	k5eAaImF	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
skupinami	skupina	k1gFnPc7	skupina
dochází	docházet	k5eAaImIp3nP	docházet
ke	k	k7c3	k
střetům	střet	k1gInPc3	střet
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
dvěma	dva	k4xCgNnPc7	dva
židovskými	židovský	k2eAgNnPc7d1	Židovské
povstáními	povstání	k1gNnPc7	povstání
(	(	kIx(	(
<g/>
66	[number]	k4	66
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
a	a	k8xC	a
133	[number]	k4	133
<g/>
–	–	k?	–
<g/>
135	[number]	k4	135
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
odcizení	odcizení	k1gNnSc1	odcizení
obou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Další	další	k2eAgFnSc1d1	další
generace	generace	k1gFnSc1	generace
židokřesťanů	židokřesťan	k1gMnPc2	židokřesťan
zřejmě	zřejmě	k6eAd1	zřejmě
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
křesťany	křesťan	k1gMnPc7	křesťan
ze	z	k7c2	z
židovství	židovství	k1gNnSc2	židovství
a	a	k8xC	a
z	z	k7c2	z
pohanství	pohanství	k1gNnSc2	pohanství
<g/>
,	,	kIx,	,
a	a	k8xC	a
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
svou	svůj	k3xOyFgFnSc4	svůj
vyhraněně	vyhraněně	k6eAd1	vyhraněně
židovskou	židovský	k2eAgFnSc4d1	židovská
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
<s>
Ponětí	ponětí	k1gNnSc1	ponětí
o	o	k7c6	o
Ježíšově	Ježíšův	k2eAgFnSc6d1	Ježíšova
osobě	osoba	k1gFnSc6	osoba
z	z	k7c2	z
judaismu	judaismus	k1gInSc2	judaismus
však	však	k9	však
prakticky	prakticky	k6eAd1	prakticky
úplně	úplně	k6eAd1	úplně
nevymizela	vymizet	k5eNaPmAgFnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
židy	žid	k1gMnPc4	žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nestali	stát	k5eNaPmAgMnP	stát
křesťany	křesťan	k1gMnPc7	křesťan
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
Ježíš	Ježíš	k1gMnSc1	Ježíš
heretikem	heretik	k1gMnSc7	heretik
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
ovšem	ovšem	k9	ovšem
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ježíšově	Ježíšův	k2eAgFnSc6d1	Ježíšova
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
však	však	k9	však
mezi	mezi	k7c7	mezi
židovským	židovský	k2eAgNnSc7d1	Židovské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
zřejmě	zřejmě	k6eAd1	zřejmě
získalo	získat	k5eAaPmAgNnS	získat
větší	veliký	k2eAgFnSc4d2	veliký
proslulost	proslulost	k1gFnSc4	proslulost
množství	množství	k1gNnSc2	množství
jiných	jiný	k2eAgMnPc2d1	jiný
reformátorů	reformátor	k1gMnPc2	reformátor
židovství	židovství	k1gNnSc2	židovství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
židovství	židovství	k1gNnSc2	židovství
byla	být	k5eAaImAgFnS	být
osoba	osoba	k1gFnSc1	osoba
Ježíše	Ježíš	k1gMnSc2	Ježíš
problematická	problematický	k2eAgFnSc1d1	problematická
nikoli	nikoli	k9	nikoli
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gNnSc3	jeho
učení	učení	k1gNnSc3	učení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
křesťanské	křesťanský	k2eAgFnSc3d1	křesťanská
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
označovala	označovat	k5eAaImAgFnS	označovat
Ježíše	Ježíš	k1gMnSc4	Ježíš
za	za	k7c4	za
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
Židé	Žid	k1gMnPc1	Žid
začali	začít	k5eAaPmAgMnP	začít
o	o	k7c4	o
Ježíše	Ježíš	k1gMnPc4	Ježíš
<g/>
,	,	kIx,	,
coby	coby	k?	coby
učitele	učitel	k1gMnSc4	učitel
Zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
zajímat	zajímat	k5eAaImF	zajímat
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
jeho	jeho	k3xOp3gNnSc4	jeho
učení	učení	k1gNnSc4	učení
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
židovských	židovský	k2eAgFnPc2d1	židovská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nacházejí	nacházet	k5eAaImIp3nP	nacházet
překvapivé	překvapivý	k2eAgFnPc1d1	překvapivá
shody	shoda	k1gFnPc1	shoda
mezi	mezi	k7c7	mezi
Ježíšovým	Ježíšův	k2eAgNnSc7d1	Ježíšovo
učením	učení	k1gNnSc7	učení
a	a	k8xC	a
učením	učení	k1gNnSc7	učení
farizejů	farizej	k1gMnPc2	farizej
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
evangelia	evangelium	k1gNnSc2	evangelium
karikují	karikovat	k5eAaImIp3nP	karikovat
jako	jako	k9	jako
pokrytce	pokrytec	k1gMnSc2	pokrytec
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
autoři	autor	k1gMnPc1	autor
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
podobnosti	podobnost	k1gFnPc4	podobnost
s	s	k7c7	s
židovskou	židovský	k2eAgFnSc7d1	židovská
sektou	sekta	k1gFnSc7	sekta
esejců	esejce	k1gMnPc2	esejce
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
generace	generace	k1gFnSc1	generace
židovských	židovský	k2eAgMnPc2d1	židovský
teologů	teolog	k1gMnPc2	teolog
běžně	běžně	k6eAd1	běžně
vnímá	vnímat	k5eAaImIp3nS	vnímat
Ježíše	Ježíš	k1gMnSc4	Ježíš
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
židovských	židovský	k2eAgFnPc2d1	židovská
dějin	dějiny	k1gFnPc2	dějiny
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
neuznává	uznávat	k5eNaImIp3nS	uznávat
jej	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
syna	syn	k1gMnSc4	syn
Božího	boží	k2eAgMnSc4d1	boží
či	či	k8xC	či
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
pozoruhodného	pozoruhodný	k2eAgMnSc2d1	pozoruhodný
kritika	kritik	k1gMnSc2	kritik
náboženských	náboženský	k2eAgInPc2d1	náboženský
omylů	omyl	k1gInPc2	omyl
a	a	k8xC	a
morálních	morální	k2eAgInPc2d1	morální
poklesků	poklesek	k1gInPc2	poklesek
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Nejkritičtější	kritický	k2eAgInSc4d3	nejkritičtější
postoj	postoj	k1gInSc4	postoj
vůči	vůči	k7c3	vůči
kultu	kult	k1gInSc3	kult
Ježíše	Ježíš	k1gMnSc2	Ježíš
z	z	k7c2	z
Nazaretu	Nazaret	k1gInSc2	Nazaret
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
ortodoxní	ortodoxní	k2eAgMnPc1d1	ortodoxní
židé	žid	k1gMnPc1	žid
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
Ježíše	Ježíš	k1gMnSc4	Ježíš
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gNnSc3	jeho
vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
tvrzení	tvrzení	k1gNnSc3	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
Božím	boží	k2eAgMnSc7d1	boží
a	a	k8xC	a
Mesiášem	Mesiáš	k1gMnSc7	Mesiáš
<g/>
,	,	kIx,	,
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
heretika	heretik	k1gMnSc4	heretik
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pomatence	pomatenec	k1gMnSc4	pomatenec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ale	ale	k8xC	ale
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
současných	současný	k2eAgMnPc2d1	současný
ultraortodoxních	ultraortodoxní	k2eAgMnPc2d1	ultraortodoxní
židů	žid	k1gMnPc2	žid
(	(	kIx(	(
<g/>
Charedim	Charedim	k1gMnSc1	Charedim
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
uzavřených	uzavřený	k2eAgNnPc6d1	uzavřené
společenstvích	společenství	k1gNnPc6	společenství
<g/>
,	,	kIx,	,
a	a	k8xC	a
otázky	otázka	k1gFnSc2	otázka
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
Ježíši	Ježíš	k1gMnSc3	Ježíš
Kristu	Krista	k1gFnSc4	Krista
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
důležité	důležitý	k2eAgNnSc4d1	důležité
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
(	(	kIx(	(
<g/>
arab	arab	k1gMnSc1	arab
<g/>
.	.	kIx.	.
ي	ي	k?	ي
Jasú	Jasú	k1gFnSc1	Jasú
<g/>
'	'	kIx"	'
či	či	k8xC	či
ع	ع	k?	ع
Ísá	Ísá	k1gFnSc1	Ísá
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
muslimy	muslim	k1gMnPc4	muslim
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
velkých	velký	k2eAgMnPc2d1	velký
proroků	prorok	k1gMnPc2	prorok
(	(	kIx(	(
<g/>
Abrahám	Abrahám	k1gMnSc1	Abrahám
<g/>
,	,	kIx,	,
Jákob	Jákob	k1gMnSc1	Jákob
<g/>
,	,	kIx,	,
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
<g/>
,	,	kIx,	,
Ježíš	Ježíš	k1gMnSc1	Ježíš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
předcházeli	předcházet	k5eAaImAgMnP	předcházet
poslednímu	poslední	k2eAgMnSc3d1	poslední
a	a	k8xC	a
největšímu	veliký	k2eAgMnSc3d3	veliký
poslu	posel	k1gMnSc3	posel
Božímu	boží	k2eAgMnSc3d1	boží
<g/>
,	,	kIx,	,
Muhammadovi	Muhammadův	k2eAgMnPc1d1	Muhammadův
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
nevěří	věřit	k5eNaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
ukřižován	ukřižovat	k5eAaPmNgMnS	ukřižovat
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
že	že	k8xS	že
sňal	snít	k5eAaPmAgInS	snít
hříchy	hřích	k1gInPc4	hřích
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
že	že	k8xS	že
by	by	kYmCp3nS	by
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
vzkříšen	vzkříšen	k2eAgInSc1d1	vzkříšen
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
odmítají	odmítat	k5eAaImIp3nP	odmítat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
božím	boží	k2eAgMnSc7d1	boží
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
rouhání	rouhání	k1gNnSc4	rouhání
proti	proti	k7c3	proti
jedinosti	jedinost	k1gFnSc3	jedinost
a	a	k8xC	a
jedinečnosti	jedinečnost	k1gFnSc3	jedinečnost
boží	boží	k2eAgFnSc3d1	boží
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
aspekty	aspekt	k1gInPc1	aspekt
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
christologie	christologie	k1gFnSc2	christologie
však	však	k9	však
muslimové	muslim	k1gMnPc1	muslim
uznávají	uznávat	k5eAaImIp3nP	uznávat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
narození	narození	k1gNnSc1	narození
z	z	k7c2	z
panny	panna	k1gFnSc2	panna
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
rovněž	rovněž	k9	rovněž
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
Ježíš	ježit	k5eAaImIp2nS	ježit
před	před	k7c7	před
dnem	den	k1gInSc7	den
soudu	soud	k1gInSc2	soud
navrátí	navrátit	k5eAaPmIp3nS	navrátit
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
obnoví	obnovit	k5eAaPmIp3nS	obnovit
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
a	a	k8xC	a
porazí	porazit	k5eAaPmIp3nS	porazit
"	"	kIx"	"
<g/>
falešného	falešný	k2eAgMnSc4d1	falešný
proroka	prorok	k1gMnSc4	prorok
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Másih	Másih	k1gMnSc1	Másih
ad-Dadždžál	ad-Dadždžál	k1gMnSc1	ad-Dadždžál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
úcty	úcta	k1gFnSc2	úcta
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
arab	arab	k1gMnSc1	arab
<g/>
.	.	kIx.	.
</s>
<s>
Marjam	Marjam	k1gInSc1	Marjam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mormonismu	mormonismus	k1gInSc6	mormonismus
(	(	kIx(	(
<g/>
americkém	americký	k2eAgNnSc6d1	americké
odvětví	odvětví	k1gNnSc6	odvětví
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
založeném	založený	k2eAgInSc6d1	založený
Josephem	Joseph	k1gInSc7	Joseph
Smithem	Smith	k1gInSc7	Smith
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
3	[number]	k4	3
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
Plán	plán	k1gInSc4	plán
spásy	spása	k1gFnSc2	spása
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
Otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
Duchem	duch	k1gMnSc7	duch
Svatým	svatý	k1gMnSc7	svatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
oddělený	oddělený	k2eAgMnSc1d1	oddělený
od	od	k7c2	od
obou	dva	k4xCgMnPc2	dva
zbylých	zbylý	k2eAgMnPc2d1	zbylý
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
tělo	tělo	k1gNnSc4	tělo
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Bůh	bůh	k1gMnSc1	bůh
Otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
taktéž	taktéž	k?	taktéž
fyzické	fyzický	k2eAgNnSc4d1	fyzické
tělo	tělo	k1gNnSc4	tělo
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
učení	učení	k1gNnSc4	učení
CJKSPD	CJKSPD	kA	CJKSPD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mormonismu	mormonismus	k1gInSc2	mormonismus
nevykonal	vykonat	k5eNaPmAgMnS	vykonat
Ježíš	Ježíš	k1gMnSc1	Ježíš
svou	svůj	k3xOyFgFnSc4	svůj
spásnou	spásný	k2eAgFnSc4d1	spásná
oběť	oběť	k1gFnSc4	oběť
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
započal	započnout	k5eAaPmAgInS	započnout
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
Getsemanské	getsemanský	k2eAgFnSc6d1	Getsemanská
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
Usmíření	usmíření	k1gNnSc1	usmíření
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
byly	být	k5eAaImAgInP	být
naloženy	naložen	k2eAgInPc1d1	naložen
hříchy	hřích	k1gInPc1	hřích
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
musel	muset	k5eAaImAgMnS	muset
donést	donést	k5eAaPmF	donést
až	až	k9	až
na	na	k7c4	na
Golgotu	Golgota	k1gFnSc4	Golgota
k	k	k7c3	k
ukřižování	ukřižování	k1gNnSc3	ukřižování
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vstal	vstát	k5eAaPmAgMnS	vstát
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
národů	národ	k1gInPc2	národ
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Kniha	kniha	k1gFnSc1	kniha
Mormonova	mormonův	k2eAgFnSc1d1	Mormonova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mormoni	mormon	k1gMnPc1	mormon
nepoužívají	používat	k5eNaImIp3nP	používat
krucifixy	krucifix	k1gInPc4	krucifix
ani	ani	k8xC	ani
kříže	kříž	k1gInPc4	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
situacích	situace	k1gFnPc6	situace
a	a	k8xC	a
podobách	podoba	k1gFnPc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
odštepené	odštepený	k2eAgInPc1d1	odštepený
mormonské	mormonský	k2eAgInPc1d1	mormonský
fragmenty	fragment	k1gInPc1	fragment
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
polygamista	polygamista	k1gMnSc1	polygamista
<g/>
)	)	kIx)	)
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
děti	dítě	k1gFnPc4	dítě
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
polygamie	polygamie	k1gFnSc2	polygamie
v	v	k7c6	v
mormonismu	mormonismus	k1gInSc6	mormonismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
