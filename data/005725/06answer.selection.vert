<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
7	[number]	k4	7
a	a	k8xC	a
1	[number]	k4	1
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
mezi	mezi	k7c7	mezi
29	[number]	k4	29
a	a	k8xC	a
36	[number]	k4	36
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
Ježíš	Ježíš	k1gMnSc1	Ježíš
Nazaretský	nazaretský	k2eAgMnSc1d1	nazaretský
či	či	k8xC	či
Ježíš	Ježíš	k1gMnSc1	Ježíš
z	z	k7c2	z
Nazareta	Nazaret	k1gInSc2	Nazaret
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
