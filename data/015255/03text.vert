<s>
Ur	Ur	k1gInSc1
(	(	kIx(
<g/>
kontinent	kontinent	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
pradávném	pradávný	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Ur	Ur	k1gInSc1
<g/>
,	,	kIx,
biblické	biblický	k2eAgNnSc1d1
město	město	k1gNnSc1
v	v	k7c6
Sumeru	Sumer	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Ur	Ur	k1gInSc1
je	být	k5eAaImIp3nS
název	název	k1gInSc1
prvního	první	k4xOgInSc2
známého	známý	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zformoval	zformovat	k5eAaPmAgInS
pravděpodobně	pravděpodobně	k6eAd1
před	před	k7c7
3	#num#	k4
miliardami	miliarda	k4xCgFnPc7
let	léto	k1gNnPc2
v	v	k7c6
eónu	eónus	k1gInSc6
Archaikum	archaikum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
před	před	k7c7
1	#num#	k4
miliardou	miliarda	k4xCgFnSc7
let	léto	k1gNnPc2
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
dalšími	další	k2eAgInPc7d1
dvěma	dva	k4xCgInPc7
kontinenty	kontinent	k1gInPc7
Nenou	Nena	k1gFnSc7
a	a	k8xC
Atlantikou	Atlantika	k1gFnSc7
do	do	k7c2
superkontinentu	superkontinent	k1gInSc2
Rodinie	Rodinie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kontinent	kontinent	k1gInSc1
byl	být	k5eAaImAgInS
složený	složený	k2eAgInSc1d1
z	z	k7c2
částí	část	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
dnes	dnes	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
Afriku	Afrika	k1gFnSc4
<g/>
,	,	kIx,
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
Indický	indický	k2eAgInSc1d1
subkontinent	subkontinent	k1gInSc1
a	a	k8xC
ostrov	ostrov	k1gInSc1
Madagaskar	Madagaskar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
jediný	jediný	k2eAgInSc1d1
kontinent	kontinent	k1gInSc1
na	na	k7c4
Zemi	zem	k1gFnSc4
a	a	k8xC
nedosahoval	dosahovat	k5eNaImAgMnS
ani	ani	k9
velikosti	velikost	k1gFnSc3
dnešní	dnešní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ur	Ur	k1gInSc1
(	(	kIx(
<g/>
kontinent	kontinent	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
In	In	k1gFnSc1
the	the	k?
beginning	beginning	k1gInSc1
<g/>
,	,	kIx,
there	ther	k1gInSc5
was	was	k?
Ur	Ur	k1gInSc1
–	–	k?
Informace	informace	k1gFnPc1
o	o	k7c6
objevu	objev	k1gInSc6
existence	existence	k1gFnSc2
kontinentu	kontinent	k1gInSc2
Ur	Ur	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Světadíly	světadíl	k1gInPc1
Jednotlivé	jednotlivý	k2eAgInPc1d1
kontinenty	kontinent	k1gInPc1
</s>
<s>
Afrika	Afrika	k1gFnSc1
•	•	k?
Antarktida	Antarktida	k1gFnSc1
•	•	k?
Asie	Asie	k1gFnSc2
•	•	k?
Austrálie	Austrálie	k1gFnSc2
•	•	k?
Evropa	Evropa	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
Superkontinenty	Superkontinenta	k1gFnSc2
</s>
<s>
Eurafrasie	Eurafrasie	k1gFnSc1
•	•	k?
Amerika	Amerika	k1gFnSc1
•	•	k?
Eurasie	Eurasie	k1gFnSc2
•	•	k?
Oceánie	Oceánie	k1gFnSc2
Geologické	geologický	k2eAgFnSc2d1
superkontinenty	superkontinenta	k1gFnSc2
</s>
<s>
Gondwana	Gondwana	k1gFnSc1
•	•	k?
Kenorland	Kenorland	k1gInSc1
•	•	k?
Kolumbie	Kolumbie	k1gFnSc2
•	•	k?
Laurasie	Laurasie	k1gFnSc2
•	•	k?
Pangea	Pangea	k1gFnSc1
•	•	k?
Pannotie	Pannotie	k1gFnSc1
•	•	k?
Protogondwana	Protogondwana	k1gFnSc1
•	•	k?
Protolaurasie	Protolaurasie	k1gFnSc2
•	•	k?
Rodinie	Rodinie	k1gFnSc2
•	•	k?
Vaalbara	Vaalbar	k1gMnSc2
Historické	historický	k2eAgInPc1d1
kontinenty	kontinent	k1gInPc1
</s>
<s>
Arktida	Arktida	k1gFnSc1
•	•	k?
Asiamerika	Asiamerik	k1gMnSc2
•	•	k?
Atlantika	Atlantik	k1gMnSc2
•	•	k?
Avalonie	Avalonie	k1gFnSc2
•	•	k?
Baltika	Baltika	k1gFnSc1
•	•	k?
Euramerika	Euramerika	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Čína	Čína	k1gFnSc1
•	•	k?
Kalahari	Kalahar	k1gFnSc2
•	•	k?
Kazachstánie	Kazachstánie	k1gFnSc2
•	•	k?
Kongo	Kongo	k1gNnSc1
•	•	k?
Laurentie	Laurentie	k1gFnSc2
•	•	k?
Severní	severní	k2eAgFnSc1d1
Čína	Čína	k1gFnSc1
•	•	k?
Sibiř	Sibiř	k1gFnSc1
•	•	k?
Ur	Ur	k1gInSc1
</s>
<s>
Pohyb	pohyb	k1gInSc1
světadílů	světadíl	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
desek	deska	k1gFnPc2
</s>
<s>
1100	#num#	k4
<g/>
–	–	k?
<g/>
750	#num#	k4
<g/>
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
zpět	zpět	k6eAd1
<g/>
600	#num#	k4
<g/>
–	–	k?
<g/>
5502000	#num#	k4
</s>
<s>
Světadíly	světadíl	k1gInPc1
<g/>
:	:	kIx,
<g/>
↗	↗	k?
<g/>
Arábie	Arábie	k1gFnSc1
</s>
<s>
↗	↗	k?
<g/>
Madagaskar	Madagaskar	k1gInSc1
</s>
<s>
↗	↗	k?
<g/>
Indie	Indie	k1gFnSc1
</s>
<s>
↗	↗	k?
<g/>
Kongo	Kongo	k1gNnSc1
<g/>
↓	↓	k?
<g/>
↗	↗	k?
<g/>
Afrika	Afrika	k1gFnSc1
<g/>
→	→	k?
<g/>
Afrika	Afrika	k1gFnSc1
</s>
<s>
↗	↗	k?
<g/>
Patagonie	Patagonie	k1gFnSc1
<g/>
↓	↓	k?
<g/>
↗	↗	k?
<g/>
Sibiř	Sibiř	k1gFnSc1
<g/>
↓	↓	k?
<g/>
↗	↗	k?
<g/>
Atlantika	Atlantikum	k1gNnSc2
<g/>
→	→	k?
<g/>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Atlantika	Atlantika	k1gFnSc1
<g/>
↘	↘	k?
<g/>
↗	↗	k?
<g/>
Západní	západní	k2eAgFnSc2d1
Arábie	Arábie	k1gFnSc2
<g/>
↓	↓	k?
<g/>
↗	↗	k?
<g/>
Baltika	Baltikum	k1gNnSc2
<g/>
↘	↘	k?
<g/>
↗	↗	k?
<g/>
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Ur	Ur	k1gInSc1
<g/>
→	→	k?
<g/>
Rodinie	Rodinie	k1gFnSc2
<g/>
→	→	k?
<g/>
Východní	východní	k2eAgFnSc1d1
Gondwana	Gondwana	k1gFnSc1
<g/>
→	→	k?
<g/>
Protogondwana	Protogondwana	k1gFnSc1
<g/>
→	→	k?
<g/>
Pannotie	Pannotie	k1gFnSc2
<g/>
→	→	k?
<g/>
Laurentie	Laurentie	k1gFnSc2
<g/>
→	→	k?
<g/>
Euramerika	Euramerika	k1gFnSc1
(	(	kIx(
<g/>
Laurussie	Laurussie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
→	→	k?
<g/>
Pangea	Pangea	k1gFnSc1
<g/>
→	→	k?
<g/>
Gondwana	Gondwana	k1gFnSc1
<g/>
→	→	k?
<g/>
Antarktida	Antarktida	k1gFnSc1
<g/>
→	→	k?
<g/>
Antarktida	Antarktida	k1gFnSc1
</s>
<s>
Arktida	Arktida	k1gFnSc1
<g/>
→	→	k?
<g/>
Nena	Nena	k1gFnSc1
<g/>
↗	↗	k?
<g/>
↘	↘	k?
<g/>
Západní	západní	k2eAgFnSc1d1
Gondwana	Gondwana	k1gFnSc1
<g/>
→	→	k?
<g/>
Protolaurasie	Protolaurasie	k1gFnSc2
<g/>
↗	↗	k?
<g/>
↘	↘	k?
<g/>
Gondwana	Gondwana	k1gFnSc1
<g/>
↗	↗	k?
<g/>
↘	↘	k?
<g/>
Laurasie	Laurasie	k1gFnSc2
<g/>
→	→	k?
<g/>
Laurentie	Laurentie	k1gFnSc2
<g/>
→	→	k?
<g/>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Baltika	Baltika	k1gFnSc1
<g/>
↗	↗	k?
<g/>
↘	↘	k?
<g/>
Baltika	Baltikum	k1gNnSc2
<g/>
↗	↗	k?
<g/>
↓	↓	k?
<g/>
Avalonie	Avalonie	k1gFnSc2
<g/>
↘	↘	k?
<g/>
Eurasie	Eurasie	k1gFnSc2
</s>
<s>
↘	↘	k?
<g/>
Laurentie	Laurentie	k1gFnSc1
<g/>
↗	↗	k?
<g/>
↓	↓	k?
<g/>
Severní	severní	k2eAgFnSc1d1
Čína	Čína	k1gFnSc1
</s>
<s>
↘	↘	k?
<g/>
Sibiř	Sibiř	k1gFnSc1
<g/>
↗	↗	k?
<g/>
↓	↓	k?
<g/>
Jižní	jižní	k2eAgFnSc1d1
Čína	Čína	k1gFnSc1
</s>
<s>
Oceány	oceán	k1gInPc1
<g/>
:	:	kIx,
<g/>
MiroviaPrototethys	MiroviaPrototethys	k1gInSc1
<g/>
,	,	kIx,
PaleotethysPanthalassa	PaleotethysPanthalassa	k1gFnSc1
<g/>
↘	↘	k?
<g/>
Tethys	Tethys	k1gInSc1
</s>
<s>
svislé	svislý	k2eAgFnPc1d1
šipky	šipka	k1gFnPc1
<g/>
:	:	kIx,
rozdělení	rozdělení	k1gNnSc1
a	a	k8xC
spojení	spojení	k1gNnSc1
•	•	k?
vodorovné	vodorovný	k2eAgFnSc2d1
a	a	k8xC
šikmé	šikmý	k2eAgFnSc2d1
šipky	šipka	k1gFnSc2
<g/>
:	:	kIx,
postupné	postupný	k2eAgNnSc1d1
připojování	připojování	k1gNnSc1
a	a	k8xC
oddělování	oddělování	k1gNnSc1
</s>
