<s>
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
</s>
<s>
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
Narození	narození	k1gNnSc4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1913	#num#	k4
<g/>
Domoušice	Domoušika	k1gFnSc6
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
31	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
19	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
31	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Hudčice	Hudčice	k1gFnSc1
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
Bydliště	bydliště	k1gNnSc2
</s>
<s>
Rožmitál	Rožmitál	k1gInSc1
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
Povolání	povolání	k1gNnSc2
</s>
<s>
lesní	lesní	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
Zaměstnavatel	zaměstnavatel	k1gMnSc1
</s>
<s>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1
statek	statek	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1913	#num#	k4
Domoušice	Domoušice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1945	#num#	k4
Hudčice	Hudčice	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
lesní	lesní	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
odbojového	odbojový	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
a	a	k8xC
ilegálního	ilegální	k2eAgInSc2d1
revolučního	revoluční	k2eAgInSc2d1
výboru	výbor	k1gInSc2
na	na	k7c6
Rožmitálsku	Rožmitálsek	k1gInSc6
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
Domoušisích	Domoušise	k1gFnPc6
v	v	k7c6
katolické	katolický	k2eAgFnSc6d1
rodině	rodina	k1gFnSc6
jako	jako	k8xC,k8xS
jediný	jediný	k2eAgMnSc1d1
syn	syn	k1gMnSc1
četnického	četnický	k2eAgMnSc2d1
strážmistra	strážmistr	k1gMnSc2
Josefa	Josef	k1gMnSc2
Lízla	líznout	k5eAaPmAgFnS
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
ženy	žena	k1gFnPc1
rozené	rozený	k2eAgFnSc2d1
Kudrové	Kudrová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodina	rodina	k1gFnSc1
se	s	k7c7
záhy	záhy	k6eAd1
poté	poté	k6eAd1
odstěhovala	odstěhovat	k5eAaPmAgFnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
Františkova	Františkův	k2eAgNnSc2d1
studia	studio	k1gNnSc2
na	na	k7c6
Vinohradské	vinohradský	k2eAgFnSc6d1
reálce	reálka	k1gFnSc6
mu	on	k3xPp3gMnSc3
otec	otec	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Syna	syn	k1gMnSc2
vychovávala	vychovávat	k5eAaImAgFnS
matka	matka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ho	on	k3xPp3gNnSc4
při	při	k7c6
studiích	studie	k1gFnPc6
vydržovala	vydržovat	k5eAaImAgFnS
z	z	k7c2
malé	malý	k2eAgFnSc2d1
penze	penze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
maturitě	maturita	k1gFnSc6
pokračoval	pokračovat	k5eAaImAgInS
studiem	studio	k1gNnSc7
architektury	architektura	k1gFnSc2
na	na	k7c6
Českém	český	k2eAgNnSc6d1
vysokém	vysoký	k2eAgNnSc6d1
učení	učení	k1gNnSc6
technickém	technický	k2eAgNnSc6d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
vydržel	vydržet	k5eAaPmAgMnS
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
přestoupil	přestoupit	k5eAaPmAgMnS
na	na	k7c4
obor	obor	k1gInSc4
lesního	lesní	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
lépe	dobře	k6eAd2
hodil	hodit	k5eAaImAgInS,k5eAaPmAgInS
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
zájmu	zájem	k1gInSc3
o	o	k7c4
přírodu	příroda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
studia	studio	k1gNnSc2
získal	získat	k5eAaPmAgInS
bohatou	bohatý	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolvoval	absolvovat	k5eAaPmAgMnS
se	s	k7c7
zaměřením	zaměření	k1gNnSc7
na	na	k7c6
lesní	lesní	k2eAgFnSc6d1
taxaci	taxace	k1gFnSc6
a	a	k8xC
studia	studio	k1gNnSc2
dokončil	dokončit	k5eAaPmAgMnS
s	s	k7c7
výborným	výborný	k2eAgInSc7d1
prospěchem	prospěch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
se	se	k3xPyFc4
dobrovolně	dobrovolně	k6eAd1
přihlásil	přihlásit	k5eAaPmAgMnS
do	do	k7c2
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ve	v	k7c6
zkrácené	zkrácený	k2eAgFnSc6d1
důstojnické	důstojnický	k2eAgFnSc6d1
škole	škola	k1gFnSc6
získal	získat	k5eAaPmAgMnS
hodnost	hodnost	k1gFnSc4
podporučíka	podporučík	k1gMnSc2
dělostřelectva	dělostřelectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
armádě	armáda	k1gFnSc6
setrval	setrvat	k5eAaPmAgMnS
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1939	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
demobilizován	demobilizován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
organizace	organizace	k1gFnSc2
„	„	k?
<g/>
záložních	záložní	k2eAgMnPc2d1
důstojníků	důstojník	k1gMnPc2
<g/>
”	”	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ovšem	ovšem	k9
neshodl	shodnout	k5eNaPmAgInS
s	s	k7c7
názorem	názor	k1gInSc7
vedení	vedení	k1gNnSc1
a	a	k8xC
proto	proto	k8xC
z	z	k7c2
této	tento	k3xDgFnSc2
organizace	organizace	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
si	se	k3xPyFc3
vrchní	vrchní	k2eAgMnSc1d1
lesní	lesní	k2eAgMnSc1d1
rada	rada	k1gMnSc1
a	a	k8xC
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
arcibiskupského	arcibiskupský	k2eAgNnSc2d1
panství	panství	k1gNnSc2
Ing.	ing.	kA
Josef	Josef	k1gMnSc1
Šimek	Šimek	k1gMnSc1
<g/>
,	,	kIx,
vybral	vybrat	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
nejlepší	dobrý	k2eAgMnPc4d3
studenty	student	k1gMnPc4
a	a	k8xC
nabídl	nabídnout	k5eAaPmAgMnS
jim	on	k3xPp3gMnPc3
službu	služba	k1gFnSc4
na	na	k7c6
rožmitálském	rožmitálský	k2eAgNnSc6d1
panství	panství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
byl	být	k5eAaImAgMnS
Ing.	ing.	kA
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
<g/>
,	,	kIx,
Ing.	ing.	kA
Karel	Karel	k1gMnSc1
Feld	Feld	k1gMnSc1
a	a	k8xC
Ing.	ing.	kA
Oldřich	Oldřich	k1gMnSc1
Jeřábek	Jeřábek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
byt	byt	k1gInSc4
Na	na	k7c6
Šichtamtě	Šichtamta	k1gFnSc6
–	–	k?
v	v	k7c6
budově	budova	k1gFnSc6
správy	správa	k1gFnSc2
zaniklých	zaniklý	k2eAgFnPc2d1
železáren	železárna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
to	ten	k3xDgNnSc1
tytéž	týž	k3xTgFnPc1
místnosti	místnost	k1gFnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
dříve	dříve	k6eAd2
žila	žít	k5eAaImAgFnS
Bohuslava	Bohuslava	k1gFnSc1
Rajská	rajský	k2eAgFnSc1d1
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
manželka	manželka	k1gFnSc1
F.	F.	kA
L.	L.	kA
Čelakovského	Čelakovský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sem	sem	k6eAd1
také	také	k6eAd1
přestěhoval	přestěhovat	k5eAaPmAgMnS
svoji	svůj	k3xOyFgFnSc4
matku	matka	k1gFnSc4
z	z	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nástupu	nástup	k1gInSc6
byl	být	k5eAaImAgInS
poslán	poslat	k5eAaPmNgInS
na	na	k7c4
louňovické	louňovický	k2eAgNnSc4d1
panství	panství	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
prováděl	provádět	k5eAaImAgInS
měření	měření	k1gNnSc4
pozemků	pozemek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
šesti	šest	k4xCc2
měsících	měsíc	k1gInPc6
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
do	do	k7c2
Rožmitálu	Rožmitál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odbojová	odbojový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Jeřábek	Jeřábek	k1gMnSc1
a	a	k8xC
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
před	před	k7c7
zámkem	zámek	k1gInSc7
v	v	k7c6
Rožmitále	Rožmitál	k1gInSc6
p.	p.	k?
Tř	tř	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
</s>
<s>
Brzy	brzy	k6eAd1
po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Rožmitálu	Rožmitál	k1gInSc2
se	se	k3xPyFc4
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
stal	stát	k5eAaPmAgInS
vůdčí	vůdčí	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
ilegální	ilegální	k2eAgFnSc2d1
odbojové	odbojový	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
na	na	k7c4
Rožmitálsku	Rožmitálska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navázal	navázat	k5eAaPmAgMnS
kontakt	kontakt	k1gInSc4
i	i	k9
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
odbojovými	odbojový	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojení	spojení	k1gNnSc2
navazoval	navazovat	k5eAaImAgInS
osobně	osobně	k6eAd1
a	a	k8xC
dopravoval	dopravovat	k5eAaImAgMnS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
na	na	k7c6
jízdním	jízdní	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgNnSc6,k3yIgNnSc6,k3yRgNnSc6
i	i	k9
objížděl	objíždět	k5eAaImAgMnS
všechny	všechen	k3xTgFnPc4
hájovny	hájovna	k1gFnPc4
a	a	k8xC
fořtovny	fořtovna	k1gFnPc4
v	v	k7c6
arcibiskupském	arcibiskupský	k2eAgNnSc6d1
panství	panství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
nebyla	být	k5eNaImAgFnS
ani	ani	k8xC
jízda	jízda	k1gFnSc1
do	do	k7c2
Písku	Písek	k1gInSc2
nebo	nebo	k8xC
na	na	k7c4
Zbraslav	Zbraslav	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nemohl	moct	k5eNaImAgMnS
přehlédnout	přehlédnout	k5eAaPmF
ředitel	ředitel	k1gMnSc1
panství	panství	k1gNnSc4
a	a	k8xC
lesní	lesní	k2eAgMnSc1d1
rada	rada	k1gMnSc1
Ing.	ing.	kA
Kříž	Kříž	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
Lízlovi	Lízlův	k2eAgMnPc1d1
vytkl	vytknout	k5eAaPmAgMnS
jeho	jeho	k3xOp3gNnSc4
cestování	cestování	k1gNnSc4
na	na	k7c6
kole	kolo	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
vzbudit	vzbudit	k5eAaPmF
podezření	podezření	k1gNnSc4
<g/>
,	,	kIx,
proto	proto	k8xC
mu	on	k3xPp3gMnSc3
nabídl	nabídnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
na	na	k7c4
své	svůj	k3xOyFgFnPc4
cesty	cesta	k1gFnPc4
raději	rád	k6eAd2
používal	používat	k5eAaImAgInS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
automobil	automobil	k1gInSc4
s	s	k7c7
arcibiskupským	arcibiskupský	k2eAgInSc7d1
znakem	znak	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc2
nabídky	nabídka	k1gFnSc2
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
později	pozdě	k6eAd2
využíval	využívat	k5eAaImAgInS,k5eAaPmAgInS
hlavně	hlavně	k9
k	k	k7c3
zásobování	zásobování	k1gNnSc3
ukrývaných	ukrývaný	k2eAgMnPc2d1
zběhů	zběh	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
počátku	počátek	k1gInSc2
skupinu	skupina	k1gFnSc4
tvořil	tvořit	k5eAaImAgInS
úzký	úzký	k2eAgInSc1d1
okruh	okruh	k1gInSc1
Lízlových	Lízlův	k2eAgMnPc2d1
přátel	přítel	k1gMnPc2
<g/>
:	:	kIx,
František	František	k1gMnSc1
Češka	Češka	k1gFnSc1
<g/>
,	,	kIx,
Ing.	ing.	kA
Oldřich	Oldřich	k1gMnSc1
Janota	Janota	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Prokopec	Prokopec	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Sláma	Sláma	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Květoň	Květoň	k1gMnSc1
a	a	k8xC
František	František	k1gMnSc1
Lang	Lang	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
poté	poté	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
blízké	blízký	k2eAgFnSc6d1
obci	obec	k1gFnSc6
Voltuš	Voltuš	k1gMnSc1
druhá	druhý	k4xOgFnSc1
ilegální	ilegální	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
vedli	vést	k5eAaImAgMnP
Jaroslav	Jaroslav	k1gMnSc1
Pompl	Pompl	k1gMnSc1
<g/>
,	,	kIx,
majitel	majitel	k1gMnSc1
zdejší	zdejší	k2eAgFnSc2d1
pily	pila	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
řidič	řidič	k1gMnSc1
Alois	Alois	k1gMnSc1
Hovorka	Hovorka	k1gMnSc1
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
ilegální	ilegální	k2eAgFnSc2d1
KSČ	KSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
let	léto	k1gNnPc2
1941	#num#	k4
a	a	k8xC
1942	#num#	k4
se	se	k3xPyFc4
obě	dva	k4xCgFnPc1
skupiny	skupina	k1gFnPc1
sloučily	sloučit	k5eAaPmAgFnP
v	v	k7c4
jedinou	jediný	k2eAgFnSc4d1
protifašistickou	protifašistický	k2eAgFnSc4d1
odbojovou	odbojový	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
později	pozdě	k6eAd2
dostala	dostat	k5eAaPmAgFnS
název	název	k1gInSc4
„	„	k?
<g/>
Rudá	rudý	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitelem	velitel	k1gMnSc7
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
<g/>
,	,	kIx,
Pompl	Pompl	k1gMnSc1
a	a	k8xC
Hovorka	Hovorka	k1gMnSc1
byli	být	k5eAaImAgMnP
jeho	jeho	k3xOp3gMnPc7
zástupci	zástupce	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
Rožmitále	Rožmitál	k1gInSc6
vytvořen	vytvořit	k5eAaPmNgInS
okresní	okresní	k2eAgInSc1d1
revoluční	revoluční	k2eAgInSc1d1
výbor	výbor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
prvních	první	k4xOgFnPc2
akcí	akce	k1gFnPc2
sloučených	sloučený	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
byla	být	k5eAaImAgFnS
výstavba	výstavba	k1gFnSc1
podzemních	podzemní	k2eAgInPc2d1
úkrytů	úkryt	k1gInPc2
pro	pro	k7c4
zbraně	zbraň	k1gFnPc4
na	na	k7c6
různých	různý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
v	v	k7c6
Rožmitále	Rožmitála	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
již	již	k6eAd1
byl	být	k5eAaImAgInS
navázán	navázán	k2eAgInSc1d1
úzký	úzký	k2eAgInSc1d1
vztah	vztah	k1gInSc1
s	s	k7c7
odbojovou	odbojový	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Obrana	obrana	k1gFnSc1
národa	národ	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
května	květen	k1gInSc2
1942	#num#	k4
se	se	k3xPyFc4
skupině	skupina	k1gFnSc3
podařilo	podařit	k5eAaPmAgNnS
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
řad	řada	k1gFnPc2
získat	získat	k5eAaPmF
dva	dva	k4xCgMnPc4
vojáky	voják	k1gMnPc4
sovětské	sovětský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
uprchlé	uprchlý	k1gMnPc4
zajatce	zajatec	k1gMnSc4
<g/>
,	,	kIx,
kterým	který	k3yRgMnSc7,k3yQgMnSc7,k3yIgMnSc7
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
vyskočit	vyskočit	k5eAaPmF
z	z	k7c2
vlaku	vlak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc4
poručík	poručík	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Pavlovič	Pavlovič	k1gMnSc1
Gvezděněv	Gvezděněv	k1gMnSc1
a	a	k8xC
starší	starý	k2eAgMnSc1d2
poručík	poručík	k1gMnSc1
Miško	Miško	k1gNnSc4
Gluškov	Gluškov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prošli	projít	k5eAaPmAgMnP
brdskými	brdský	k2eAgInPc7d1
lesy	les	k1gInPc7
a	a	k8xC
objevili	objevit	k5eAaPmAgMnP
se	se	k3xPyFc4
nedaleko	nedaleko	k7c2
obce	obec	k1gFnSc2
Hutě	huť	k1gFnSc2
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
s	s	k7c7
nimi	on	k3xPp3gInPc7
rožmitálská	rožmitálský	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
navázala	navázat	k5eAaPmAgFnS
kontakt	kontakt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukryli	ukrýt	k5eAaPmAgMnP
je	on	k3xPp3gNnSc4
v	v	k7c6
lesním	lesní	k2eAgInSc6d1
úkrytu	úkryt	k1gInSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
jim	on	k3xPp3gMnPc3
členové	člen	k1gMnPc1
skupiny	skupina	k1gFnSc2
nosili	nosit	k5eAaImAgMnP
potraviny	potravina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neznámou	známý	k2eNgFnSc7d1
zradou	zrada	k1gFnSc7
se	se	k3xPyFc4
však	však	k9
o	o	k7c6
přítomnosti	přítomnost	k1gFnSc6
sovětských	sovětský	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
dozvědělo	dozvědět	k5eAaPmAgNnS
gestapo	gestapo	k1gNnSc4
a	a	k8xC
jejich	jejich	k3xOp3gInSc4
úkryt	úkryt	k1gInSc4
obklíčilo	obklíčit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gvezděněvovi	Gvezděněva	k1gMnSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
utéct	utéct	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
Gluškov	Gluškov	k1gInSc1
byl	být	k5eAaImAgInS
chycen	chycen	k2eAgMnSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
další	další	k2eAgInPc1d1
osudy	osud	k1gInPc1
nejsou	být	k5eNaImIp3nP
známy	znám	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gvezděněva	Gvezděněvo	k1gNnSc2
pak	pak	k6eAd1
přepravil	přepravit	k5eAaPmAgMnS
Alois	Alois	k1gMnSc1
Hovorka	Hovorka	k1gMnSc1
<g/>
,	,	kIx,
ke	k	k7c3
svým	svůj	k3xOyFgMnPc3
příbuzným	příbuzný	k1gMnPc3
v	v	k7c6
obci	obec	k1gFnSc6
Višňová	višňový	k2eAgFnSc1d1
u	u	k7c2
Příbrami	Příbram	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Gvezděněv	Gvezděněv	k1gFnSc1
nějaký	nějaký	k3yIgInSc4
čas	čas	k1gInSc4
ukrýval	ukrývat	k5eAaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
pak	pak	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
Rožmitáslko	Rožmitáslko	k1gNnSc4
vrátil	vrátit	k5eAaPmAgMnS
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
členem	člen	k1gMnSc7
Lízlovy	Lízlův	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Brdech	Brdy	k1gInPc6
se	se	k3xPyFc4
různě	různě	k6eAd1
objevovali	objevovat	k5eAaImAgMnP
další	další	k2eAgMnPc1d1
sovětští	sovětský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
uprchlí	uprchlý	k1gMnPc1
ze	z	k7c2
zajetí	zajetí	k1gNnSc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
v	v	k7c6
létě	léto	k1gNnSc6
roku	rok	k1gInSc2
1943	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
přibývali	přibývat	k5eAaImAgMnP
i	i	k9
čeští	český	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
stíhaní	stíhaný	k2eAgMnPc1d1
Němci	Němec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
Lízlova	Lízlův	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
rozšířená	rozšířený	k2eAgFnSc1d1
o	o	k7c4
další	další	k2eAgInPc4d1
členy	člen	k1gInPc4
<g/>
,	,	kIx,
především	především	k9
z	z	k7c2
řad	řada	k1gFnPc2
lesních	lesní	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
,	,	kIx,
zahájila	zahájit	k5eAaPmAgFnS
v	v	k7c6
Třemšínských	Třemšínský	k2eAgInPc6d1
lesích	les	k1gInPc6
výstavbu	výstavba	k1gFnSc4
tajných	tajný	k2eAgInPc2d1
obytných	obytný	k2eAgInPc2d1
bunkrů	bunkr	k1gInPc2
–	–	k?
zemljanek	zemljanka	k1gFnPc2
(	(	kIx(
<g/>
bylo	být	k5eAaImAgNnS
jich	on	k3xPp3gFnPc2
zhruba	zhruba	k6eAd1
15	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
už	už	k6eAd1
se	se	k3xPyFc4
skupina	skupina	k1gFnSc1
dala	dát	k5eAaPmAgFnS
nazývat	nazývat	k5eAaImF
partyzánskou	partyzánský	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
starostí	starost	k1gFnSc7
nyní	nyní	k6eAd1
bylo	být	k5eAaImAgNnS
zajištění	zajištění	k1gNnSc1
materiálu	materiál	k1gInSc2
pro	pro	k7c4
stavbu	stavba	k1gFnSc4
bunkrů	bunkr	k1gInPc2
a	a	k8xC
proviantu	proviant	k1gInSc2
pro	pro	k7c4
uprchlé	uprchlý	k2eAgMnPc4d1
sovětské	sovětský	k2eAgMnPc4d1
zajatce	zajatec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
honu	hon	k1gInSc2
na	na	k7c6
Březinách	Březina	k1gFnPc6
u	u	k7c2
Radošic	Radošice	k1gInPc2
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1944	#num#	k4
<g/>
,	,	kIx,
objevil	objevit	k5eAaPmAgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
honců	honec	k1gMnPc2
partyzánský	partyzánský	k2eAgInSc4d1
bunkr	bunkr	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
obýval	obývat	k5eAaImAgInS
sovětský	sovětský	k2eAgMnSc1d1
poručík	poručík	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
civilu	civil	k1gMnSc6
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
Gregor	Gregor	k1gMnSc1
Iljič	Iljič	k1gMnSc1
Pimonovič	Pimonovič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
gestapo	gestapo	k1gNnSc1
bunkr	bunkr	k1gInSc4
obklíčilo	obklíčit	k5eAaPmAgNnS
<g/>
,	,	kIx,
zahájil	zahájit	k5eAaPmAgMnS
palbu	palba	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
poté	poté	k6eAd1
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
vážně	vážně	k6eAd1
zraněn	zraněn	k2eAgMnSc1d1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
ukončit	ukončit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
střelou	střela	k1gFnSc7
do	do	k7c2
spánku	spánek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
na	na	k7c6
sebe	sebe	k3xPyFc4
upoutala	upoutat	k5eAaPmAgFnS
pozornost	pozornost	k1gFnSc1
Němců	Němec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
už	už	k6eAd1
dříve	dříve	k6eAd2
tušili	tušit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
brdských	brdský	k2eAgInPc6d1
lesích	les	k1gInPc6
ukrývají	ukrývat	k5eAaImIp3nP
partyzáni	partyzán	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gestapo	gestapo	k1gNnSc1
s	s	k7c7
úkolem	úkol	k1gInSc7
zajistit	zajistit	k5eAaPmF
stopy	stop	k1gInPc4
o	o	k7c6
odbojové	odbojový	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
<g/>
,	,	kIx,
pozatýkalo	pozatýkat	k5eAaPmAgNnS
mnoho	mnoho	k4c1
lidí	člověk	k1gMnPc2
na	na	k7c4
Rožmitálsku	Rožmitálska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
partyzáni	partyzán	k1gMnPc1
ukrývají	ukrývat	k5eAaImIp3nP
v	v	k7c6
hájovně	hájovna	k1gFnSc6
Na	na	k7c6
Dědku	Dědek	k1gMnSc6
u	u	k7c2
hajného	hajný	k1gMnSc2
Františka	František	k1gMnSc2
Königsmarka	Königsmarka	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
Lízlovy	Lízlův	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tragédie	tragédie	k1gFnSc1
Na	na	k7c6
Dědku	Dědek	k1gMnSc6
</s>
<s>
František	františek	k1gInSc1
Königsmark	Königsmark	k1gInSc1
ml.	ml.	kA
a	a	k8xC
František	František	k1gMnSc1
Königsmark	Königsmark	k1gInSc4
spáchali	spáchat	k5eAaPmAgMnP
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1944	#num#	k4
sebevraždu	sebevražda	k1gFnSc4
v	v	k7c6
hájence	hájenka	k1gFnSc6
Na	na	k7c6
Dědku	Dědek	k1gMnSc6
<g/>
,	,	kIx,
když	když	k8xS
je	on	k3xPp3gFnPc4
obklíčilo	obklíčit	k5eAaPmAgNnS
gestapo	gestapo	k1gNnSc1
</s>
<s>
Dům	dům	k1gInSc1
Habadových	Habadový	k2eAgMnPc2d1
v	v	k7c6
Bezděkově	Bezděkův	k2eAgFnSc6d1
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
k	k	k7c3
uctění	uctění	k1gNnSc3
památky	památka	k1gFnSc2
rodiny	rodina	k1gFnSc2
Habadových	Habadový	k2eAgMnPc2d1
</s>
<s>
Dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1944	#num#	k4
před	před	k7c7
svítáním	svítání	k1gNnSc7
obklíčilo	obklíčit	k5eAaPmAgNnS
gestapo	gestapo	k1gNnSc1
hájovnu	hájovna	k1gFnSc4
Na	na	k7c6
Dědku	Dědek	k1gMnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
pobýval	pobývat	k5eAaImAgMnS
hajný	hajný	k1gMnSc1
František	František	k1gMnSc1
Königsmark	Königsmark	k1gInSc4
se	s	k7c7
ženou	žena	k1gFnSc7
Marií	Maria	k1gFnSc7
a	a	k8xC
devatenáctiletým	devatenáctiletý	k2eAgMnSc7d1
synem	syn	k1gMnSc7
Františkem	František	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gestapu	gestapo	k1gNnSc6
bylo	být	k5eAaImAgNnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
i	i	k8xC
četnictvo	četnictvo	k1gNnSc1
povolané	povolaný	k2eAgNnSc1d1
z	z	k7c2
obcí	obec	k1gFnPc2
Tochovice	Tochovice	k1gFnSc2
<g/>
,	,	kIx,
Bělčice	Bělčice	k1gFnPc4
<g/>
,	,	kIx,
Lnáře	lnář	k1gMnPc4
a	a	k8xC
Hvožďany	Hvožďan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
je	být	k5eAaImIp3nS
hajný	hajný	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
stál	stát	k5eAaImAgMnS
na	na	k7c6
dvoře	dvůr	k1gInSc6
<g/>
,	,	kIx,
zpozoroval	zpozorovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
vběhl	vběhnout	k5eAaPmAgMnS
do	do	k7c2
hájovny	hájovna	k1gFnSc2
<g/>
,	,	kIx,
zamkl	zamknout	k5eAaPmAgMnS
dveře	dveře	k1gFnPc4
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
ozvaly	ozvat	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
střelné	střelný	k2eAgFnPc1d1
rány	rána	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
Otec	otec	k1gMnSc1
i	i	k8xC
syn	syn	k1gMnSc1
raději	rád	k6eAd2
zvolili	zvolit	k5eAaPmAgMnP
smrt	smrt	k1gFnSc4
sebevraždou	sebevražda	k1gFnSc7
<g/>
,	,	kIx,
než	než	k8xS
aby	aby	kYmCp3nP
byli	být	k5eAaImAgMnP
zatčeni	zatknout	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
syn	syn	k1gMnSc1
byl	být	k5eAaImAgMnS
na	na	k7c6
místě	místo	k1gNnSc6
mrtvý	mrtvý	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
stále	stále	k6eAd1
naživu	naživu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádná	žádný	k3yNgNnPc4
pomoc	pomoc	k1gFnSc4
mu	on	k3xPp3gMnSc3
ale	ale	k8xC
nebyla	být	k5eNaImAgFnS
poskytnuta	poskytnout	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
ponechán	ponechat	k5eAaPmNgInS
na	na	k7c6
místě	místo	k1gNnSc6
společně	společně	k6eAd1
se	s	k7c7
svojí	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
dostal	dostat	k5eAaPmAgMnS
za	za	k7c4
úkol	úkol	k1gInSc4
střežit	střežit	k5eAaImF
praporčík	praporčík	k1gMnSc1
Lehečka	Lehečko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Takto	takto	k6eAd1
zůstal	zůstat	k5eAaPmAgMnS
hajný	hajný	k1gMnSc1
Königsmark	Königsmark	k1gInSc4
bez	bez	k7c2
pomoci	pomoc	k1gFnSc2
až	až	k6eAd1
do	do	k7c2
17	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
převezen	převézt	k5eAaPmNgMnS
do	do	k7c2
příbramské	příbramský	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
ráno	ráno	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Königsmarková	Königsmarková	k1gFnSc1
byla	být	k5eAaImAgFnS
poslána	poslat	k5eAaPmNgFnS
do	do	k7c2
Terezína	Terezín	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ještě	ještě	k9
téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
pokračovalo	pokračovat	k5eAaImAgNnS
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
zatýkání	zatýkání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gestapo	gestapo	k1gNnSc1
vyslýchalo	vyslýchat	k5eAaImAgNnS
zejména	zejména	k9
lesní	lesní	k2eAgMnPc4d1
zaměstnance	zaměstnanec	k1gMnPc4
ve	v	k7c6
snaze	snaha	k1gFnSc6
dozvědět	dozvědět	k5eAaPmF
se	se	k3xPyFc4
o	o	k7c6
úkrytech	úkryt	k1gInPc6
partyzánů	partyzán	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
spojení	spojení	k1gNnSc4
s	s	k7c7
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgInSc2
dne	den	k1gInSc2
se	se	k3xPyFc4
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
účastnil	účastnit	k5eAaImAgMnS
výlovu	výlov	k1gInSc3
rybníka	rybník	k1gInSc2
Obžera	Obžera	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
dozvěděl	dozvědět	k5eAaPmAgMnS
o	o	k7c6
události	událost	k1gFnSc6
Na	na	k7c6
Dědku	Dědek	k1gMnSc6
<g/>
,	,	kIx,
okamžitě	okamžitě	k6eAd1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgInS
do	do	k7c2
Rožmitálu	Rožmitál	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
společně	společně	k6eAd1
s	s	k7c7
lékárníkem	lékárník	k1gMnSc7
Jiřím	Jiří	k1gMnSc7
Třasoněm	Třasoň	k1gMnSc7
ukryli	ukrýt	k5eAaPmAgMnP
v	v	k7c6
domě	dům	k1gInSc6
porybného	porybný	k1gMnSc4
Langa	Lang	k1gMnSc4
na	na	k7c6
hrázi	hráz	k1gFnSc6
Sadoňského	Sadoňský	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
celý	celý	k2eAgInSc4d1
den	den	k1gInSc4
hlásil	hlásit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
kdokoli	kdokoli	k3yInSc1
Lízlovi	Lízl	k1gMnSc3
poskytne	poskytnout	k5eAaPmIp3nS
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
potrestán	potrestat	k5eAaPmNgInS
společně	společně	k6eAd1
s	s	k7c7
celou	celý	k2eAgFnSc7d1
rodinou	rodina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lízl	líznout	k5eAaPmAgMnS
se	se	k3xPyFc4
u	u	k7c2
Langů	Lang	k1gMnPc2
cítil	cítit	k5eAaImAgMnS
v	v	k7c6
relativním	relativní	k2eAgNnSc6d1
bezpečí	bezpečí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
syna	syn	k1gMnSc2
porybného	porybný	k1gMnSc2
udržoval	udržovat	k5eAaImAgInS
kontakt	kontakt	k1gInSc4
se	s	k7c7
zbytkem	zbytek	k1gInSc7
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
psal	psát	k5eAaImAgInS
dopisy	dopis	k1gInPc7
své	svůj	k3xOyFgFnSc3
matce	matka	k1gFnSc3
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgFnPc6,k3yRgFnPc6,k3yQgFnPc6
vysvětloval	vysvětlovat	k5eAaImAgMnS
že	že	k8xS
musel	muset	k5eAaImAgMnS
odjet	odjet	k5eAaPmF
služebně	služebně	k6eAd1
mimo	mimo	k7c4
Rožmitál	Rožmitál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopisy	dopis	k1gInPc4
odesílali	odesílat	k5eAaImAgMnP
z	z	k7c2
Prahy	Praha	k1gFnSc2
pomocí	pomocí	k7c2
přátel	přítel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zrada	zrada	k1gFnSc1
</s>
<s>
Odhalení	odhalení	k1gNnSc4
pomníku	pomník	k1gInSc2
Františka	František	k1gMnSc2
Lízla	líznout	k5eAaPmAgFnS
u	u	k7c2
železničního	železniční	k2eAgInSc2d1
přejezdu	přejezd	k1gInSc2
v	v	k7c6
Hudčicích	Hudčice	k1gFnPc6
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1975	#num#	k4
(	(	kIx(
<g/>
byl	být	k5eAaImAgInS
přemístěn	přemístit	k5eAaPmNgInS
19	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Do	do	k7c2
brdských	brdský	k2eAgInPc2d1
lesů	les	k1gInPc2
se	se	k3xPyFc4
z	z	k7c2
Klatov	Klatovy	k1gInPc2
začala	začít	k5eAaPmAgFnS
stahovala	stahovat	k5eAaImAgFnS
tzv.	tzv.	kA
jagdkomanda	jagdkomanda	k1gFnSc1
–	–	k?
speciální	speciální	k2eAgInPc4d1
oddíly	oddíl	k1gInPc4
pro	pro	k7c4
boj	boj	k1gInSc4
s	s	k7c7
partyzány	partyzán	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těm	ten	k3xDgFnPc3
se	se	k3xPyFc4
po	po	k7c6
většinou	většinou	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
včas	včas	k6eAd1
utéct	utéct	k5eAaPmF
a	a	k8xC
před	před	k7c7
gestapem	gestapo	k1gNnSc7
zatím	zatím	k6eAd1
úspěšně	úspěšně	k6eAd1
unikali	unikat	k5eAaImAgMnP
i	i	k9
hlavní	hlavní	k2eAgMnPc1d1
členové	člen	k1gMnPc1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
zatýkání	zatýkání	k1gNnSc1
a	a	k8xC
výslechy	výslech	k1gInPc1
pokračovaly	pokračovat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zatčených	zatčený	k2eAgMnPc2d1
byl	být	k5eAaImAgMnS
i	i	k9
Miroslav	Miroslav	k1gMnSc1
Náhlovský	Náhlovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
na	na	k7c6
jaře	jaro	k1gNnSc6
1943	#num#	k4
začal	začít	k5eAaPmAgMnS
pracovat	pracovat	k5eAaImF
jako	jako	k9
laborant	laborant	k1gMnSc1
v	v	k7c6
lékárně	lékárna	k1gFnSc6
Třasoňových	Třasoňův	k2eAgFnPc2d1
v	v	k7c6
Rožmitále	Rožmitála	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
právě	právě	k6eAd1
Náhlovský	Náhlovský	k2eAgInSc1d1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
způsobil	způsobit	k5eAaPmAgInS
začátek	začátek	k1gInSc4
tragického	tragický	k2eAgInSc2d1
pádu	pád	k1gInSc2
celé	celý	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
případě	případ	k1gInSc6
postupovala	postupovat	k5eAaImAgFnS
velmi	velmi	k6eAd1
nerozvážně	rozvážně	k6eNd1
a	a	k8xC
svěřila	svěřit	k5eAaPmAgFnS
důvěru	důvěra	k1gFnSc4
nevhodné	vhodný	k2eNgFnSc3d1
osobě	osoba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
začátek	začátek	k1gInSc4
byla	být	k5eAaImAgFnS
velká	velký	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Náhlovského	Náhlovský	k2eAgInSc2d1
lékárník	lékárník	k1gMnSc1
Třasoň	Třasoň	k1gFnPc4
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nP
si	se	k3xPyFc3
o	o	k7c6
něm	on	k3xPp3gInSc6
cokoliv	cokoliv	k3yInSc4
ověřil	ověřit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ubytoval	ubytovat	k5eAaPmAgMnS
ve	v	k7c6
vile	vila	k1gFnSc6
Aloisie	Aloisie	k1gFnSc2
Vorzové	Vorzová	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
úkryt	úkryt	k1gInSc1
sovětským	sovětský	k2eAgMnPc3d1
uprchlíkům	uprchlík	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náhlovský	Náhlovský	k2eAgInSc1d1
dokonce	dokonce	k9
jeden	jeden	k4xCgInSc4
čas	čas	k1gInSc4
sdílel	sdílet	k5eAaImAgMnS
pokoj	pokoj	k1gInSc4
i	i	k9
se	s	k7c7
Sergejem	Sergej	k1gMnSc7
Gvezděněvem	Gvezděněv	k1gInSc7
a	a	k8xC
skrze	skrze	k?
něj	on	k3xPp3gMnSc4
i	i	k9
Třasoňovi	Třasoň	k1gMnSc3
se	se	k3xPyFc4
mohl	moct	k5eAaImAgMnS
snadno	snadno	k6eAd1
dozvědět	dozvědět	k5eAaPmF
o	o	k7c6
odboji	odboj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
mladý	mladý	k2eAgMnSc1d1
laborant	laborant	k1gMnSc1
v	v	k7c6
lékárně	lékárna	k1gFnSc6
krade	krást	k5eAaImIp3nS
narkotika	narkotikon	k1gNnPc4
<g/>
,	,	kIx,
především	především	k9
morfium	morfium	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
byl	být	k5eAaImAgInS
závislý	závislý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lékárník	lékárník	k1gMnSc1
donutil	donutit	k5eAaPmAgMnS
Náhlovského	Náhlovský	k2eAgMnSc4d1
odejít	odejít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
později	pozdě	k6eAd2
pracoval	pracovat	k5eAaImAgMnS
ještě	ještě	k6eAd1
v	v	k7c6
lékárně	lékárna	k1gFnSc6
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Veltrusech	Veltrusy	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnPc1
krádeže	krádež	k1gFnPc1
opakovaly	opakovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lékárníka	lékárník	k1gMnSc4
Třasoně	Třasoň	k1gFnSc2
ovšem	ovšem	k9
stále	stále	k6eAd1
příležitostně	příležitostně	k6eAd1
v	v	k7c6
Rožmitále	Rožmitála	k1gFnSc6
navštěvoval	navštěvovat	k5eAaImAgMnS
a	a	k8xC
vyptával	vyptávat	k5eAaImAgMnS
se	se	k3xPyFc4
i	i	k9
na	na	k7c4
odboj	odboj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1944	#num#	k4
Náhlovského	Náhlovský	k2eAgInSc2d1
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
kvůli	kvůli	k7c3
udání	udání	k1gNnSc3
<g/>
,	,	kIx,
zadrželo	zadržet	k5eAaPmAgNnS
gestapo	gestapo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdý	tvrdý	k2eAgInSc1d1
výslech	výslech	k1gInSc1
<g/>
,	,	kIx,
výhrůžky	výhrůžka	k1gFnPc1
a	a	k8xC
nedostupnost	nedostupnost	k1gFnSc1
drogy	droga	k1gFnSc2
zřejmě	zřejmě	k6eAd1
udělaly	udělat	k5eAaPmAgFnP
své	své	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyzradil	vyzradit	k5eAaPmAgMnS
vše	všechen	k3xTgNnSc4
co	co	k9
věděl	vědět	k5eAaImAgMnS
o	o	k7c6
rožmitálském	rožmitálský	k2eAgInSc6d1
odboji	odboj	k1gInSc6
a	a	k8xC
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vyhnul	vyhnout	k5eAaPmAgMnS
soudu	soud	k1gInSc3
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgInS
se	se	k3xPyFc4
konfidentem	konfident	k1gMnSc7
gestapa	gestapo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mělo	mít	k5eAaImAgNnS
především	především	k9
zájem	zájem	k1gInSc4
na	na	k7c4
polapení	polapení	k1gNnSc4
Aloise	Alois	k1gMnSc4
Hovorky	Hovorka	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náhlovský	Náhlovský	k2eAgInSc1d1
se	se	k3xPyFc4
opět	opět	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Rožmitálu	Rožmitál	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
zkontaktoval	zkontaktovat	k5eAaPmAgMnS
s	s	k7c7
lékárníkem	lékárník	k1gMnSc7
Třasoněm	Třasoň	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdělil	sdělit	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
hledá	hledat	k5eAaImIp3nS
gestapo	gestapo	k1gNnSc1
a	a	k8xC
potřebuje	potřebovat	k5eAaImIp3nS
se	se	k3xPyFc4
ukrýt	ukrýt	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třasoň	Třasoň	k1gFnPc2
společně	společně	k6eAd1
s	s	k7c7
Hovorkou	Hovorka	k1gMnSc7
mu	on	k3xPp3gNnSc3
naivně	naivně	k6eAd1
obstarali	obstarat	k5eAaPmAgMnP
úkryt	úkryt	k1gInSc4
v	v	k7c6
Bezděkově	Bezděkův	k2eAgFnSc6d1
u	u	k7c2
rodiny	rodina	k1gFnSc2
Habadů	Habad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
také	také	k9
s	s	k7c7
Hovorkou	Hovorka	k1gMnSc7
sejít	sejít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náhlovský	Náhlovský	k2eAgInSc1d1
vše	všechen	k3xTgNnSc4
sdělil	sdělit	k5eAaPmAgMnS
gestapu	gestapo	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
připravilo	připravit	k5eAaPmAgNnS
plán	plán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvé	prvý	k4xOgFnSc6
řadě	řada	k1gFnSc6
měl	mít	k5eAaImAgInS
Náhlovský	Náhlovský	k2eAgInSc1d1
u	u	k7c2
Habadů	Habad	k1gInPc2
sdělit	sdělit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
dva	dva	k4xCgMnPc4
kamarády	kamarád	k1gMnPc4
–	–	k?
československé	československý	k2eAgMnPc4d1
parašutisty	parašutista	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
utíkají	utíkat	k5eAaImIp3nP
před	před	k7c7
gestapem	gestapo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmi	ten	k3xDgInPc7
byli	být	k5eAaImAgMnP
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
agenti	agent	k1gMnPc1
gestapa	gestapo	k1gNnSc2
–	–	k?
Ladislav	Ladislav	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
a	a	k8xC
Jaroslav	Jaroslav	k1gMnSc1
Panenka	panenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
na	na	k7c6
schůzce	schůzka	k1gFnSc6
zatknout	zatknout	k5eAaPmF
Hovorku	hovorek	k1gInSc3
<g/>
,	,	kIx,
Habadovi	Habadův	k2eAgMnPc1d1
a	a	k8xC
všechny	všechen	k3xTgInPc4
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
s	s	k7c7
Hovorkou	Hovorka	k1gMnSc7
objeví	objevit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třasoň	Třasoň	k1gFnSc1
svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
splnil	splnit	k5eAaPmAgMnS
na	na	k7c4
výbornou	výborná	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1944	#num#	k4
<g/>
,	,	kIx,
asi	asi	k9
v	v	k7c4
8	#num#	k4
hodin	hodina	k1gFnPc2
večer	večer	k6eAd1
v	v	k7c6
domě	dům	k1gInSc6
Habadových	Habadový	k2eAgFnPc2d1
<g/>
,	,	kIx,
zaklepali	zaklepat	k5eAaPmAgMnP
falešní	falešný	k2eAgMnPc1d1
parašutisté	parašutista	k1gMnPc1
a	a	k8xC
po	po	k7c6
dohodnutém	dohodnutý	k2eAgNnSc6d1
hesle	heslo	k1gNnSc6
„	„	k?
<g/>
koupím	koupit	k5eAaPmIp1nS
husu	husa	k1gFnSc4
<g/>
“	“	k?
byli	být	k5eAaImAgMnP
pozvání	pozvání	k1gNnSc4
dovnitř	dovnitř	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Habadová	Habadový	k2eAgFnSc1d1
jim	on	k3xPp3gFnPc3
nedůvěřovala	důvěřovat	k5eNaImAgFnS
<g/>
,	,	kIx,
tvrdila	tvrdit	k5eAaImAgFnS
že	že	k8xS
vypadají	vypadat	k5eAaImIp3nP,k5eAaPmIp3nP
jako	jako	k8xC,k8xS
gestapáci	gestapáci	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přesto	přesto	k8xC
u	u	k7c2
Habadových	Habadový	k2eAgInPc2d1
strávili	strávit	k5eAaPmAgMnP
noc	noc	k1gFnSc4
a	a	k8xC
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
čekali	čekat	k5eAaImAgMnP
na	na	k7c4
večer	večer	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
měla	mít	k5eAaImAgFnS
proběhnout	proběhnout	k5eAaPmF
schůzka	schůzka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
přesně	přesně	k6eAd1
stalo	stát	k5eAaPmAgNnS
v	v	k7c6
Bezděkově	Bezděkův	k2eAgFnSc6d1
v	v	k7c6
domě	dům	k1gInSc6
č.	č.	k?
42	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
výpovědi	výpověď	k1gFnPc1
rozcházejí	rozcházet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lízl	líznout	k5eAaPmAgMnS
už	už	k9
údajně	údajně	k6eAd1
Náhlovského	Náhlovský	k2eAgNnSc2d1
podezříval	podezřívat	k5eAaImAgMnS
<g/>
,	,	kIx,
když	když	k8xS
k	k	k7c3
Habadům	Habad	k1gInPc3
přivedl	přivést	k5eAaPmAgMnS
Štěpánka	Štěpánek	k1gMnSc4
s	s	k7c7
Panenkou	panenka	k1gFnSc7
a	a	k8xC
odboj	odboj	k1gInSc4
varoval	varovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
schůzce	schůzka	k1gFnSc3
přesto	přesto	k8xC
došlo	dojít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
sedmé	sedmý	k4xOgFnSc2
hodiny	hodina	k1gFnSc2
večer	večer	k6eAd1
vešel	vejít	k5eAaPmAgInS
do	do	k7c2
domu	dům	k1gInSc2
starý	starý	k1gMnSc1
Habada	Habada	k1gFnSc1
se	s	k7c7
synem	syn	k1gMnSc7
Karlem	Karel	k1gMnSc7
<g/>
,	,	kIx,
lékárníkem	lékárník	k1gMnSc7
Třasoněm	Třasoň	k1gMnSc7
<g/>
,	,	kIx,
Náhlovským	Náhlovský	k2eAgMnSc7d1
<g/>
,	,	kIx,
Hovorkou	Hovorka	k1gMnSc7
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
ochráncem	ochránce	k1gMnSc7
Janem	Jan	k1gMnSc7
Belasem	Belas	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
výpovědi	výpověď	k1gFnSc2
Štěpánka	Štěpánka	k1gFnSc1
vběhl	vběhnout	k5eAaPmAgInS
Náhlovský	Náhlovský	k2eAgInSc1d1
do	do	k7c2
pokoje	pokoj	k1gInSc2
parašutistů	parašutista	k1gMnPc2
a	a	k8xC
varoval	varovat	k5eAaImAgMnS
je	být	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovala	následovat	k5eAaImAgFnS
přestřelka	přestřelka	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
byl	být	k5eAaImAgMnS
lékárník	lékárník	k1gMnSc1
střelen	střelit	k5eAaPmNgMnS
do	do	k7c2
nohy	noha	k1gFnSc2
a	a	k8xC
do	do	k7c2
ruky	ruka	k1gFnSc2
<g/>
,	,	kIx,
Hovorka	Hovorka	k1gMnSc1
měl	mít	k5eAaImAgMnS
průstřel	průstřel	k1gInSc4
břicha	břicho	k1gNnSc2
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Habada	Habada	k1gFnSc1
průstřel	průstřel	k1gInSc4
plic	plíce	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
otec	otec	k1gMnSc1
průstřel	průstřel	k1gInSc4
nohy	noha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utéct	utéct	k5eAaPmF
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
pouze	pouze	k6eAd1
Belasovi	Belasův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štěpánek	Štěpánka	k1gFnPc2
následně	následně	k6eAd1
poslal	poslat	k5eAaPmAgMnS
Náhlovského	Náhlovský	k2eAgMnSc4d1
k	k	k7c3
místnímu	místní	k2eAgMnSc3d1
starostovi	starosta	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
poslal	poslat	k5eAaPmAgMnS
pro	pro	k7c4
sanitku	sanitka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
pak	pak	k6eAd1
pokračoval	pokračovat	k5eAaImAgInS
na	na	k7c6
kole	kolo	k1gNnSc6
do	do	k7c2
Březnice	Březnice	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
informoval	informovat	k5eAaBmAgMnS
gestapo	gestapo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postřelení	postřelení	k1gNnPc2
byli	být	k5eAaImAgMnP
odvezeni	odvezen	k2eAgMnPc1d1
do	do	k7c2
nemocnice	nemocnice	k1gFnSc2
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lékárníka	lékárník	k1gMnSc2
jako	jako	k8xC,k8xS
jediného	jediné	k1gNnSc2
odvezli	odvézt	k5eAaPmAgMnP
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ostatní	ostatní	k2eAgMnPc1d1
putovali	putovat	k5eAaImAgMnP
do	do	k7c2
věznice	věznice	k1gFnSc2
v	v	k7c6
Klatovech	Klatovy	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
právě	právě	k6eAd1
Trasoň	Trasoň	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
gestapu	gestapo	k1gNnSc3
vyzradil	vyzradit	k5eAaPmAgMnS
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgInS
<g/>
,	,	kIx,
ale	ale	k8xC
udal	udat	k5eAaPmAgMnS
i	i	k9
mnoho	mnoho	k4c4
dalších	další	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
spolupracovaly	spolupracovat	k5eAaImAgFnP
s	s	k7c7
odbojem	odboj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
ledna	leden	k1gInSc2
1945	#num#	k4
gestapo	gestapo	k1gNnSc1
zatklo	zatknout	k5eAaPmAgNnS
Františka	František	k1gMnSc4
Lízla	líznout	k5eAaPmAgFnS
u	u	k7c2
Langů	Lang	k1gMnPc2
a	a	k8xC
odvedlo	odvést	k5eAaPmAgNnS
ho	on	k3xPp3gMnSc4
na	na	k7c4
četnickou	četnický	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
tři	tři	k4xCgFnPc1
skupiny	skupina	k1gFnPc1
zatýkají	zatýkat	k5eAaImIp3nP
rodiny	rodina	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
vyzradil	vyzradit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Třasoň	Třasoň	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gestapo	gestapo	k1gNnSc1
se	se	k3xPyFc4
snažilo	snažit	k5eAaImAgNnS
Lízla	líznout	k5eAaPmAgNnP
vyslýchat	vyslýchat	k5eAaImF
už	už	k9
na	na	k7c4
stanici	stanice	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
stále	stále	k6eAd1
odpovídal	odpovídat	k5eAaImAgMnS
jen	jen	k9
<g/>
,	,	kIx,
že	že	k8xS
nemá	mít	k5eNaImIp3nS
o	o	k7c4
organizaci	organizace	k1gFnSc4
informace	informace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Smrt	smrt	k1gFnSc1
</s>
<s>
Pomník	pomník	k1gInSc1
Františka	František	k1gMnSc2
Lízla	líznout	k5eAaPmAgFnS
u	u	k7c2
Sadoňského	Sadoňský	k2eAgInSc2d1
rybníku	rybník	k1gInSc3
v	v	k7c6
Rožmitále	Rožmitála	k1gFnSc6
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
padlým	padlý	k2eAgMnPc3d1
lesákům	lesák	k1gMnPc3
na	na	k7c6
zámku	zámek	k1gInSc6
Rožmitál	Rožmitála	k1gFnPc2
</s>
<s>
Po	po	k7c6
neúspěšném	úspěšný	k2eNgInSc6d1
výslechu	výslech	k1gInSc6
byl	být	k5eAaImAgInS
František	františek	k1gInSc1
Lízl	líznout	k5eAaPmAgInS
předán	předán	k2eAgInSc1d1
četnické	četnický	k2eAgFnSc3d1
stanici	stanice	k1gFnSc3
v	v	k7c6
Rožmitále	Rožmitála	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgInS
zůstat	zůstat	k5eAaPmF
do	do	k7c2
rána	ráno	k1gNnSc2
<g/>
,	,	kIx,
než	než	k8xS
bude	být	k5eAaImBp3nS
převezen	převézt	k5eAaPmNgMnS
k	k	k7c3
výslechu	výslech	k1gInSc3
do	do	k7c2
Klatov	Klatovy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
gestapa	gestapo	k1gNnSc2
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
Kilián	Kilián	k1gMnSc1
Ruprecht	Ruprecht	k1gMnSc1
a	a	k8xC
Erwin	Erwin	k1gMnSc1
Janisch	Janisch	k1gMnSc1
<g/>
,	,	kIx,
i	i	k9
s	s	k7c7
jejich	jejich	k3xOp3gMnSc7
šoférem	šofér	k1gMnSc7
Hermannem	Hermann	k1gMnSc7
Wodickou	Wodická	k1gFnSc4
oslavovali	oslavovat	k5eAaImAgMnP
a	a	k8xC
zapíjeli	zapíjet	k5eAaImAgMnP
Lízlovo	Lízlův	k2eAgNnSc4d1
zatčení	zatčení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
čtyři	čtyři	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
ráno	ráno	k6eAd1
zajel	zajet	k5eAaPmAgMnS
ne	ne	k9
zcela	zcela	k6eAd1
střízlivý	střízlivý	k2eAgMnSc1d1
šofér	šofér	k1gMnSc1
Wodicka	Wodicko	k1gNnSc2
pro	pro	k7c4
Lízla	líznout	k5eAaPmAgFnS
na	na	k7c4
četnickou	četnický	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
nabral	nabrat	k5eAaPmAgMnS
Ruprechta	Ruprechta	k1gMnSc1
s	s	k7c7
Janischem	Janisch	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruprecht	Ruprecht	k1gInSc1
si	se	k3xPyFc3
sedl	sednout	k5eAaPmAgInS
dopředu	dopředu	k6eAd1
vedle	vedle	k7c2
řidiče	řidič	k1gInSc2
<g/>
,	,	kIx,
Janisch	Janisch	k1gMnSc1
si	se	k3xPyFc3
sedl	sednout	k5eAaPmAgMnS
na	na	k7c4
zadní	zadní	k2eAgNnSc4d1
sedadlo	sedadlo	k1gNnSc4
vedle	vedle	k6eAd1
Lízla	líznout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
počátku	počátek	k1gInSc2
se	se	k3xPyFc4
Janisch	Janisch	k1gMnSc1
pokoušel	pokoušet	k5eAaImAgMnS
s	s	k7c7
Lízlem	Lízl	k1gMnSc7
navázat	navázat	k5eAaPmF
opilecký	opilecký	k2eAgInSc4d1
rozhovor	rozhovor	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
brzy	brzy	k6eAd1
usnul	usnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k6eAd1
Ruprecht	Ruprecht	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Auto	auto	k1gNnSc1
mezitím	mezitím	k6eAd1
vyrazilo	vyrazit	k5eAaPmAgNnS
přes	přes	k7c4
Březnici	Březnice	k1gFnSc4
směrem	směr	k1gInSc7
na	na	k7c4
Mirovice	Mirovice	k1gFnPc4
<g/>
,	,	kIx,
Strakonice	Strakonice	k1gFnPc1
a	a	k8xC
Klatovy	Klatovy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
,	,	kIx,
hustě	hustě	k6eAd1
sněžilo	sněžit	k5eAaImAgNnS
a	a	k8xC
auto	auto	k1gNnSc1
se	se	k3xPyFc4
prodíralo	prodírat	k5eAaImAgNnS
sněhem	sníh	k1gInSc7
pomalu	pomalu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
železničním	železniční	k2eAgInSc7d1
přejezdem	přejezd	k1gInSc7
u	u	k7c2
hudčického	hudčický	k2eAgInSc2d1
kamenolomu	kamenolom	k1gInSc2
probudila	probudit	k5eAaPmAgFnS
Janische	Janische	k1gFnSc1
zima	zima	k1gFnSc1
<g/>
,	,	kIx,
zjistil	zjistit	k5eAaPmAgMnS
že	že	k8xS
místo	místo	k1gNnSc1
vedle	vedle	k7c2
něj	on	k3xPp3gNnSc2
je	být	k5eAaImIp3nS
prázdné	prázdný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavolal	zavolat	k5eAaPmAgMnS
na	na	k7c4
řidiče	řidič	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zastavil	zastavit	k5eAaPmAgInS
a	a	k8xC
to	ten	k3xDgNnSc1
probudilo	probudit	k5eAaPmAgNnS
i	i	k8xC
Ruprechta	Ruprechta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
ještě	ještě	k9
tma	tma	k1gFnSc1
<g/>
,	,	kIx,
Ruprecht	Ruprecht	k1gInSc1
s	s	k7c7
Janischem	Janisch	k1gMnSc7
hledali	hledat	k5eAaImAgMnP
zatčeného	zatčený	k1gMnSc2
půl	půl	k1xP
hodiny	hodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
po	po	k7c4
sto	sto	k4xCgNnSc4
metrech	metr	k1gInPc6
našli	najít	k5eAaPmAgMnP
na	na	k7c6
železniční	železniční	k2eAgFnSc6d1
trati	trať	k1gFnSc6
přejeté	přejetý	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
Františka	František	k1gMnSc2
Lízla	líznout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomu	Tom	k1gMnSc6
se	se	k3xPyFc4
zřejmě	zřejmě	k6eAd1
v	v	k7c6
nepozorované	pozorovaný	k2eNgFnSc6d1
chvíli	chvíle	k1gFnSc6
podařilo	podařit	k5eAaPmAgNnS
otevřít	otevřít	k5eAaPmF
dveře	dveře	k1gFnPc4
auta	auto	k1gNnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
vyskočil	vyskočit	k5eAaPmAgInS
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
vstříc	vstříc	k6eAd1
vlaku	vlak	k1gInSc2
a	a	k8xC
jisté	jistý	k2eAgFnSc2d1
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ruprecht	Ruprecht	k1gInSc1
s	s	k7c7
Janischem	Janisch	k1gInSc7
marně	marně	k6eAd1
hledali	hledat	k5eAaImAgMnP
Františka	František	k1gMnSc4
Lízla	líznout	k5eAaPmAgFnS
skoro	skoro	k6eAd1
půl	půl	k6eAd1
hodny	hoden	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
Ruprecht	Ruprecht	k1gMnSc1
řekl	říct	k5eAaPmAgMnS
že	že	k8xS
ještě	ještě	k6eAd1
prohlédnout	prohlédnout	k5eAaPmF
trať	trať	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
po	po	k7c4
sto	sto	k4xCgNnSc4
metrech	metro	k1gNnPc6
našli	najít	k5eAaPmAgMnP
tělo	tělo	k1gNnSc4
mrtvého	mrtvý	k2eAgMnSc2d1
Františka	František	k1gMnSc2
Lízla	líznout	k5eAaPmAgFnS
v	v	k7c6
kolejišti	kolejiště	k1gNnSc6
<g/>
,	,	kIx,
přejeté	přejetý	k2eAgFnPc4d1
na	na	k7c4
dvě	dva	k4xCgFnPc4
poloviny	polovina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
rázem	ráz	k1gInSc7
vystřízlivěli	vystřízlivět	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dojeli	dojet	k5eAaPmAgMnP
do	do	k7c2
Hudčic	Hudčice	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Ruprecht	Ruprecht	k1gMnSc1
nařídil	nařídit	k5eAaPmAgMnS
starostovi	starosta	k1gMnSc3
Kadlecovi	Kadlec	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nechal	nechat	k5eAaPmAgMnS
z	z	k7c2
tratě	trať	k1gFnSc2
odvézt	odvézt	k5eAaPmF
tělo	tělo	k1gNnSc4
<g/>
,	,	kIx,
nechal	nechat	k5eAaPmAgMnS
na	na	k7c4
něj	on	k3xPp3gNnSc4
zhotovit	zhotovit	k5eAaPmF
bednu	bedna	k1gFnSc4
a	a	k8xC
uložil	uložit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
do	do	k7c2
hasičské	hasičský	k2eAgFnSc2d1
zbrojnice	zbrojnice	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
jej	on	k3xPp3gInSc4
později	pozdě	k6eAd2
vyzvedne	vyzvednout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Hudčic	Hudčice	k1gFnPc2
odjeli	odjet	k5eAaPmAgMnP
do	do	k7c2
Březnice	Březnice	k1gFnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
Ruprecht	Ruprecht	k1gMnSc1
zavolal	zavolat	k5eAaPmAgMnS
na	na	k7c6
velitelství	velitelství	k1gNnSc6
gestapa	gestapo	k1gNnSc2
do	do	k7c2
Klatov	Klatovy	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
o	o	k7c6
celé	celý	k2eAgFnSc6d1
události	událost	k1gFnSc6
podal	podat	k5eAaPmAgMnS
zprávu	zpráva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
nařízeno	nařídit	k5eAaPmNgNnS
odvézt	odvézt	k5eAaPmF
tělo	tělo	k1gNnSc4
Františka	František	k1gMnSc2
Lízla	líznout	k5eAaPmAgFnS
do	do	k7c2
Plzně	Plzeň	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
po	po	k7c6
lékařské	lékařský	k2eAgFnSc6d1
prohlídce	prohlídka	k1gFnSc6
<g/>
,	,	kIx,
předal	předat	k5eAaPmAgInS
tělo	tělo	k1gNnSc4
ke	k	k7c3
spálení	spálení	k1gNnSc3
v	v	k7c6
krematoriu	krematorium	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
celé	celý	k2eAgFnSc6d1
události	událost	k1gFnSc6
vypověděl	vypovědět	k5eAaPmAgInS
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1945	#num#	k4
u	u	k7c2
vyšetřovací	vyšetřovací	k2eAgFnSc2d1
komise	komise	k1gFnSc2
okresního	okresní	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
v	v	k7c6
Rožmitále	Rožmitála	k1gFnSc6
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
řidič	řidič	k1gMnSc1
Hermann	Hermann	k1gMnSc1
Wodicka	Wodicka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
byl	být	k5eAaImAgMnS
Ing.	ing.	kA
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
In	In	k1gFnSc3
memoriam	memoriam	k6eAd1
vyznamenán	vyznamenat	k5eAaPmNgInS
Československým	československý	k2eAgInSc7d1
válečným	válečný	k2eAgInSc7d1
křížem	kříž	k1gInSc7
1939	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Rožmitále	Rožmitál	k1gInSc6
po	po	k7c6
něm	on	k3xPp3gInSc6
byla	být	k5eAaImAgFnS
pojmenována	pojmenován	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
Ing.	ing.	kA
Lízla	líznout	k5eAaPmAgFnS
vedoucí	vedoucí	k1gFnSc1
z	z	k7c2
náměstí	náměstí	k1gNnSc2
k	k	k7c3
zámku	zámek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zámku	zámek	k1gInSc6
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c4
zeď	zeď	k1gFnSc4
Malého	Malého	k2eAgInSc2d1
paláce	palác	k1gInSc2
pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
padlý	padlý	k1gMnSc1
lesákům	lesák	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Sadoňského	Sadoňský	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
vznikl	vzniknout	k5eAaPmAgInS
pomník	pomník	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
navrhl	navrhnout	k5eAaPmAgMnS
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
zrealizovat	zrealizovat	k5eAaPmF
Miroslav	Miroslav	k1gMnSc1
Hásek	Hásek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
pomník	pomník	k1gInSc4
věnovaný	věnovaný	k2eAgInSc4d1
Ing.	ing.	kA
Lízlovi	Lízl	k1gMnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
u	u	k7c2
železničního	železniční	k2eAgInSc2d1
přejezdu	přejezd	k1gInSc2
v	v	k7c6
Hudčicích	Hudčice	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
slavnostně	slavnostně	k6eAd1
odhalen	odhalit	k5eAaPmNgInS
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1975	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
O	o	k7c6
tom	ten	k3xDgNnSc6
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
v	v	k7c6
hájovně	hájovna	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
více	hodně	k6eAd2
verzí	verze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
tvrdí	tvrdit	k5eAaImIp3nP
že	že	k8xS
syna	syn	k1gMnSc4
zastřelil	zastřelit	k5eAaPmAgMnS
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
pokoušel	pokoušet	k5eAaImAgMnS
utéct	utéct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
tvrdí	tvrdit	k5eAaImIp3nP
že	že	k8xS
syna	syn	k1gMnSc4
zastřelilo	zastřelit	k5eAaPmAgNnS
nebo	nebo	k8xC
umučilo	umučit	k5eAaPmAgNnS
gestapo	gestapo	k1gNnSc4
<g/>
.	.	kIx.
↑	↑	k?
Marie	Marie	k1gFnSc1
Königsmarková	Königsmarková	k1gFnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
deníku	deník	k1gInSc6
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
lépe	dobře	k6eAd2
pochovávali	pochovávat	k5eAaImAgMnP
psa	pes	k1gMnSc4
<g/>
,	,	kIx,
než	než	k8xS
byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
její	její	k3xOp3gMnSc1
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poukazuje	poukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
zakazuje	zakazovat	k5eAaImIp3nS
pohřbívat	pohřbívat	k5eAaImF
sebevrahy	sebevrah	k1gMnPc4
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
syn	syn	k1gMnSc1
byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
za	za	k7c7
hřbitovní	hřbitovní	k2eAgFnSc7d1
zdí	zeď	k1gFnSc7
ve	v	k7c6
Hvožďanech	Hvožďan	k1gInPc6
a	a	k8xC
manžel	manžel	k1gMnSc1
za	za	k7c7
hřbitovní	hřbitovní	k2eAgFnSc7d1
zdí	zeď	k1gFnSc7
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
deník	deník	k1gInSc1
se	se	k3xPyFc4
dochoval	dochovat	k5eAaPmAgInS
v	v	k7c6
pozůstalosti	pozůstalost	k1gFnSc6
J.	J.	kA
Filipové	Filipové	k2eAgFnSc1d1
v	v	k7c6
r.	r.	kA
2006	#num#	k4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
SOA	SOA	kA
Příbram	Příbram	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
<g/>
,	,	kIx,
průkaz	průkaz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wikipedia	Wikipedium	k1gNnPc1
Commons	Commonsa	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Pomník	pomník	k1gInSc1
ing	ing	kA
Františka	Františka	k1gFnSc1
Lízla	líznout	k5eAaPmAgFnS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mapy	mapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Smrt	smrt	k1gFnSc1
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
-	-	kIx~
Neznámí	známit	k5eNaImIp3nP
hrdninové	hrdninový	k2eAgInPc1d1
<g/>
,	,	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
HÁLA	Hála	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příspěvek	příspěvek	k1gInSc1
k	k	k7c3
dějinám	dějiny	k1gFnPc3
Rožmitálska	Rožmitálsko	k1gNnSc2
v	v	k7c6
období	období	k1gNnSc6
let	léto	k1gNnPc2
1939	#num#	k4
-	-	kIx~
1948	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
samonáklad	samonáklad	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
244	#num#	k4
s.	s.	k?
↑	↑	k?
ČÁKA	čáka	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brdské	brdský	k2eAgNnSc4d1
toulání	toulání	k1gNnSc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Středočeské	středočeský	k2eAgNnSc1d1
vydavatelství	vydavatelství	k1gNnSc1
a	a	k8xC
knihkupectví	knihkupectví	k1gNnSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
193	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Na	na	k7c6
stezkách	stezka	k1gFnPc6
partyzánů	partyzán	k1gMnPc2
<g/>
,	,	kIx,
s.	s.	k?
152	#num#	k4
<g/>
--	--	k?
<g/>
167	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
KUNCOVÁ	Kuncová	k1gFnSc1
<g/>
,	,	kIx,
Monika	Monika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raději	rád	k6eAd2
smrt	smrt	k1gFnSc4
než	než	k8xS
gestapo	gestapo	k1gNnSc4
<g/>
:	:	kIx,
osudy	osud	k1gInPc4
odbojářů	odbojář	k1gMnPc2
z	z	k7c2
Brd	Brdy	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-5-24	2020-5-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
CELOSTÁTNÍ	celostátní	k2eAgFnSc1d1
KONFERENCE	konference	k1gFnSc1
K	k	k7c3
REGIONÁLNÍM	regionální	k2eAgFnPc3d1
DĚJINÁM	dějiny	k1gFnPc3
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ŘÍJNA	říjen	k1gInSc2
2016	#num#	k4
ČELÁKOVICE	Čelákovice	k1gFnPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HÁLA	Hála	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příspěvek	příspěvek	k1gInSc1
k	k	k7c3
dějinám	dějiny	k1gFnPc3
Rožmitálska	Rožmitálsko	k1gNnSc2
v	v	k7c6
období	období	k1gNnSc6
let	léto	k1gNnPc2
1939	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
samonáklad	samonáklad	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
244	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
ČÁKA	čáka	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brdské	brdský	k2eAgFnPc4d1
toulání	toulání	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Středočeské	středočeský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
a	a	k8xC
knihkupectví	knihkupectví	k1gNnSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
193	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
2360	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
152	#num#	k4
<g/>
-	-	kIx~
<g/>
167	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
