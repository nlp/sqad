<s desamb="1">
Brzy	brzy	k6eAd1
poté	poté	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
blízké	blízký	k2eAgFnSc6d1
obci	obec	k1gFnSc6
Voltuš	Voltuš	k1gMnSc1
druhá	druhý	k4xOgFnSc1
ilegální	ilegální	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
vedli	vést	k5eAaImAgMnP
Jaroslav	Jaroslav	k1gMnSc1
Pompl	Pompl	k1gMnSc1
<g/>
,	,	kIx,
majitel	majitel	k1gMnSc1
zdejší	zdejší	k2eAgFnSc2d1
pily	pila	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
řidič	řidič	k1gMnSc1
Alois	Alois	k1gMnSc1
Hovorka	Hovorka	k1gMnSc1
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
ilegální	ilegální	k2eAgFnSc2d1
KSČ	KSČ	kA
<g/>
.	.	kIx.
</s>
<s>
Brzy	brzy	k6eAd1
po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Rožmitálu	Rožmitál	k1gInSc2
se	se	k3xPyFc4
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
stal	stát	k5eAaPmAgInS
vůdčí	vůdčí	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
ilegální	ilegální	k2eAgFnSc2d1
odbojové	odbojový	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
na	na	k7c4
Rožmitálsku	Rožmitálska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitelem	velitel	k1gMnSc7
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
<g/>
,	,	kIx,
Pompl	Pompl	k1gMnSc1
a	a	k8xC
Hovorka	Hovorka	k1gMnSc1
byli	být	k5eAaImAgMnP
jeho	jeho	k3xOp3gMnPc7
zástupci	zástupce	k1gMnPc7
<g/>
.	.	kIx.
</s>