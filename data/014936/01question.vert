<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
nazývala	nazývat	k5eAaImAgFnS
odbojová	odbojový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
jejíhož	jejíž	k3xOyRp3gNnSc2
čela	čelo	k1gNnSc2
by	by	kYmCp3nS
František	František	k1gMnSc1
Lízl	líznout	k5eAaPmAgMnS
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
let	léto	k1gNnPc2
1941	#num#	k4
a	a	k8xC
1942	#num#	k4
zvolen	zvolit	k5eAaPmNgMnS
<g/>
?	?	kIx.
</s>