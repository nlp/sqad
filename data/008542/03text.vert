<p>
<s>
Mariano	Mariana	k1gFnSc5	Mariana
di	di	k?	di
Jacopo	Jacopa	k1gFnSc5	Jacopa
řečený	řečený	k2eAgInSc1d1	řečený
il	il	k?	il
Taccola	Taccola	k1gFnSc1	Taccola
(	(	kIx(	(
<g/>
Kavka	Kavka	k1gMnSc1	Kavka
<g/>
,	,	kIx,	,
1382	[number]	k4	1382
Siena	Siena	k1gFnSc1	Siena
-	-	kIx~	-
asi	asi	k9	asi
1453	[number]	k4	1453
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
polyhistor	polyhistor	k1gMnSc1	polyhistor
<g/>
,	,	kIx,	,
správce	správce	k1gMnSc1	správce
<g/>
,	,	kIx,	,
výtvarník	výtvarník	k1gMnSc1	výtvarník
a	a	k8xC	a
inženýr	inženýr	k1gMnSc1	inženýr
rané	raný	k2eAgFnSc2d1	raná
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Taccola	Taccola	k1gFnSc1	Taccola
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
technologická	technologický	k2eAgNnPc4d1	Technologické
pojednání	pojednání	k1gNnPc4	pojednání
De	De	k?	De
ingeneis	ingeneis	k1gFnPc7	ingeneis
a	a	k8xC	a
De	De	k?	De
machinis	machinis	k1gInSc1	machinis
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
s	s	k7c7	s
poznámkami	poznámka	k1gFnPc7	poznámka
výkresy	výkres	k1gInPc4	výkres
široké	široký	k2eAgFnSc2d1	široká
řady	řada	k1gFnSc2	řada
inovativních	inovativní	k2eAgInPc2d1	inovativní
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Taccolovy	Taccolův	k2eAgFnSc2d1	Taccolův
práce	práce	k1gFnSc2	práce
byly	být	k5eAaImAgInP	být
studovány	studovat	k5eAaImNgInP	studovat
a	a	k8xC	a
kopírovány	kopírovat	k5eAaImNgInP	kopírovat
pozdějšími	pozdní	k2eAgMnPc7d2	pozdější
inženýry	inženýr	k1gMnPc7	inženýr
a	a	k8xC	a
umělci	umělec	k1gMnPc7	umělec
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Francescem	Francesec	k1gInSc7	Francesec
di	di	k?	di
Giorgio	Giorgio	k6eAd1	Giorgio
<g/>
,	,	kIx,	,
a	a	k8xC	a
možná	možná	k9	možná
i	i	k8xC	i
Leonardem	Leonardo	k1gMnSc7	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Mariano	Mariana	k1gFnSc5	Mariana
Taccola	Taccola	k1gFnSc1	Taccola
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Sieně	Siena	k1gFnSc6	Siena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1382	[number]	k4	1382
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
mládí	mládí	k1gNnSc6	mládí
a	a	k8xC	a
studiích	studie	k1gFnPc6	studie
není	být	k5eNaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
dospělý	dospělý	k1gMnSc1	dospělý
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Sieně	Siena	k1gFnSc6	Siena
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
jako	jako	k9	jako
notář	notář	k1gMnSc1	notář
<g/>
,	,	kIx,	,
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
tajemník	tajemník	k1gMnSc1	tajemník
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
dozorce	dozorce	k1gMnSc1	dozorce
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
jako	jako	k8xS	jako
hydraulický	hydraulický	k2eAgMnSc1d1	hydraulický
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Taccola	Taccola	k1gFnSc1	Taccola
odešel	odejít	k5eAaPmAgInS	odejít
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
státu	stát	k1gInSc2	stát
důchod	důchod	k1gInSc4	důchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1453	[number]	k4	1453
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
Bratrstvu	bratrstvo	k1gNnSc3	bratrstvo
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
(	(	kIx(	(
<g/>
Ordine	Ordin	k1gInSc5	Ordin
di	di	k?	di
San	San	k1gMnSc1	San
Giacomo	Giacoma	k1gFnSc5	Giacoma
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
datu	datum	k1gNnSc6	datum
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Práce	práce	k1gFnSc1	práce
a	a	k8xC	a
styl	styl	k1gInSc1	styl
==	==	k?	==
</s>
</p>
<p>
<s>
Taccola	Taccola	k1gFnSc1	Taccola
zanechal	zanechat	k5eAaPmAgMnS	zanechat
dvě	dva	k4xCgFnPc4	dva
monografie	monografie	k1gFnPc4	monografie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
De	De	k?	De
ingeneis	ingeneis	k1gFnSc1	ingeneis
(	(	kIx(	(
<g/>
O	o	k7c6	o
strojích	stroj	k1gInPc6	stroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
roku	rok	k1gInSc2	rok
1419	[number]	k4	1419
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgMnS	dokončit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1433	[number]	k4	1433
<g/>
.	.	kIx.	.
</s>
<s>
Práci	práce	k1gFnSc4	práce
však	však	k9	však
průběžně	průběžně	k6eAd1	průběžně
doplňoval	doplňovat	k5eAaImAgInS	doplňovat
dalšími	další	k2eAgInPc7d1	další
obrázky	obrázek	k1gInPc7	obrázek
a	a	k8xC	a
poznámkami	poznámka	k1gFnPc7	poznámka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1449	[number]	k4	1449
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
doplněného	doplněný	k2eAgNnSc2d1	doplněné
a	a	k8xC	a
přepracovaného	přepracovaný	k2eAgNnSc2d1	přepracované
díla	dílo	k1gNnSc2	dílo
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
De	De	k?	De
machinis	machinis	k1gInSc1	machinis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Taccola	Taccola	k1gFnSc1	Taccola
kreslil	kreslit	k5eAaImAgMnS	kreslit
černým	černý	k2eAgMnSc7d1	černý
inkoustem	inkoust	k1gMnSc7	inkoust
na	na	k7c4	na
papír	papír	k1gInSc4	papír
a	a	k8xC	a
kresby	kresba	k1gFnPc4	kresba
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
ručně	ručně	k6eAd1	ručně
psanými	psaný	k2eAgFnPc7d1	psaná
poznámkami	poznámka	k1gFnPc7	poznámka
Popisuje	popisovat	k5eAaImIp3nS	popisovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
"	"	kIx"	"
<g/>
důmyslných	důmyslný	k2eAgNnPc2d1	důmyslné
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
"	"	kIx"	"
v	v	k7c6	v
hydrotechnice	hydrotechnika	k1gFnSc6	hydrotechnika
<g/>
,	,	kIx,	,
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
a	a	k8xC	a
také	také	k9	také
řadu	řada	k1gFnSc4	řada
válečných	válečný	k2eAgInPc2d1	válečný
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Taccolovy	Taccolův	k2eAgFnPc1d1	Taccolův
kresby	kresba	k1gFnPc1	kresba
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
mužem	muž	k1gMnSc7	muž
na	na	k7c6	na
předělu	předěl	k1gInSc6	předěl
dvou	dva	k4xCgFnPc2	dva
epoch	epocha	k1gFnPc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gInSc1	jeho
předmět	předmět	k1gInSc1	předmět
předjímá	předjímat	k5eAaImIp3nS	předjímat
práce	práce	k1gFnSc2	práce
pozdějších	pozdní	k2eAgInPc2d2	pozdější
renesančních	renesanční	k2eAgInPc2d1	renesanční
umělců-inženýrů	umělcůnženýr	k1gInPc2	umělců-inženýr
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
způsob	způsob	k1gInSc1	způsob
vyjádření	vyjádření	k1gNnSc2	vyjádření
ještě	ještě	k9	ještě
hodně	hodně	k6eAd1	hodně
připomíná	připomínat	k5eAaImIp3nS	připomínat
středověké	středověký	k2eAgFnPc4d1	středověká
ilustrace	ilustrace	k1gFnPc4	ilustrace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
perspektiva	perspektiva	k1gFnSc1	perspektiva
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
kersbách	kersba	k1gFnPc6	kersba
objevuje	objevovat	k5eAaImIp3nS	objevovat
jen	jen	k9	jen
někde	někde	k6eAd1	někde
<g/>
.	.	kIx.	.
</s>
<s>
Taccola	Taccola	k1gFnSc1	Taccola
-	-	kIx~	-
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
-	-	kIx~	-
byl	být	k5eAaImAgMnS	být
účastníkem	účastník	k1gMnSc7	účastník
probíhající	probíhající	k2eAgFnSc2d1	probíhající
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
perspektivním	perspektivní	k2eAgNnSc6d1	perspektivní
malířství	malířství	k1gNnSc6	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
Taccolovi	Taccol	k1gMnSc6	Taccol
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
samotným	samotný	k2eAgMnSc7d1	samotný
otcem	otec	k1gMnSc7	otec
lineární	lineární	k2eAgFnSc2d1	lineární
perspectivity	perspectivita	k1gFnSc2	perspectivita
<g/>
.	.	kIx.	.
s	s	k7c7	s
Filippo	Filippa	k1gFnSc5	Filippa
Brunelleschim	Brunelleschim	k1gFnPc3	Brunelleschim
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tyto	tento	k3xDgFnPc4	tento
grafické	grafický	k2eAgFnPc4d1	grafická
nesrovnalosti	nesrovnalost	k1gFnPc4	nesrovnalost
je	být	k5eAaImIp3nS	být
Taccolův	Taccolův	k2eAgInSc1d1	Taccolův
styl	styl	k1gInSc1	styl
popisován	popisován	k2eAgInSc1d1	popisován
jako	jako	k8xC	jako
energický	energický	k2eAgMnSc1d1	energický
<g/>
,	,	kIx,	,
autentický	autentický	k2eAgInSc1d1	autentický
a	a	k8xC	a
soustředěný	soustředěný	k2eAgInSc1d1	soustředěný
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zachytit	zachytit	k5eAaPmF	zachytit
podstatné	podstatný	k2eAgNnSc1d1	podstatné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
znovuobjevení	znovuobjevení	k1gNnSc4	znovuobjevení
==	==	k?	==
</s>
</p>
<p>
<s>
Taccola	Taccola	k1gFnSc1	Taccola
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Sienský	sienský	k2eAgMnSc1d1	sienský
Archimedes	Archimedes	k1gMnSc1	Archimedes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
tradice	tradice	k1gFnSc1	tradice
italských	italský	k2eAgMnPc2d1	italský
renesančních	renesanční	k2eAgMnPc2d1	renesanční
umělců-inženýrů	umělcůnženýr	k1gMnPc2	umělců-inženýr
s	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
technologické	technologický	k2eAgFnPc4d1	technologická
otázky	otázka	k1gFnPc4	otázka
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
kresby	kresba	k1gFnPc1	kresba
byly	být	k5eAaImAgFnP	být
kopírovány	kopírován	k2eAgFnPc1d1	kopírována
a	a	k8xC	a
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
inspirace	inspirace	k1gFnSc2	inspirace
konstruktérům	konstruktér	k1gMnPc3	konstruktér
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
Buonacorso	Buonacorsa	k1gFnSc5	Buonacorsa
Ghiberti	Ghibert	k1gMnPc1	Ghibert
<g/>
,	,	kIx,	,
Francesco	Francesco	k1gMnSc1	Francesco
di	di	k?	di
Giorgio	Giorgio	k1gMnSc1	Giorgio
<g/>
,	,	kIx,	,
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
historický	historický	k2eAgInSc4d1	historický
význam	význam	k1gInSc4	význam
mají	mít	k5eAaImIp3nP	mít
kresby	kresba	k1gFnPc4	kresba
jeho	jeho	k3xOp3gNnPc2	jeho
geniálních	geniální	k2eAgNnPc2d1	geniální
zdvihacích	zdvihací	k2eAgNnPc2d1	zdvihací
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
Brunelleschi	Brunelleschi	k1gNnSc4	Brunelleschi
použil	použít	k5eAaPmAgMnS	použít
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
kopule	kopule	k1gFnSc2	kopule
katedrály	katedrála	k1gFnSc2	katedrála
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
svým	svůj	k3xOyFgMnPc3	svůj
rozponem	rozpon	k1gInSc7	rozpon
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
Taccololovo	Taccololův	k2eAgNnSc4d1	Taccololův
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prakticky	prakticky	k6eAd1	prakticky
ustal	ustat	k5eAaPmAgMnS	ustat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
možná	možná	k9	možná
byla	být	k5eAaImAgFnS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
dostupné	dostupný	k2eAgInPc4d1	dostupný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
rukopisných	rukopisný	k2eAgFnPc2d1	rukopisná
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc4	tři
.	.	kIx.	.
</s>
<s>
Rozbor	rozbor	k1gInSc1	rozbor
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
rukopisů	rukopis	k1gInPc2	rukopis
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgFnPc1d1	původní
kresby	kresba	k1gFnPc1	kresba
Taccolovy	Taccolův	k2eAgFnPc1d1	Taccolův
jsou	být	k5eAaImIp3nP	být
detailnější	detailní	k2eAgFnPc1d2	detailnější
než	než	k8xS	než
jeho	jeho	k3xOp3gFnPc1	jeho
kopie	kopie	k1gFnPc1	kopie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
nově	nově	k6eAd1	nově
objeveny	objevit	k5eAaPmNgInP	objevit
a	a	k8xC	a
identifikovány	identifikován	k2eAgInPc1d1	identifikován
další	další	k2eAgInPc1d1	další
Originály	originál	k1gInPc1	originál
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
knihovnách	knihovna	k1gFnPc6	knihovna
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
a	a	k8xC	a
Florenci	Florenc	k1gFnSc6	Florenc
,	,	kIx,	,
což	což	k3yQnSc1	což
dalo	dát	k5eAaPmAgNnS	dát
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
tištěnému	tištěný	k2eAgInSc3d1	tištěný
vydání	vydání	k1gNnSc6	vydání
obou	dva	k4xCgInPc2	dva
spisů	spis	k1gInPc2	spis
De	De	k?	De
ingeneis	ingeneis	k1gInSc1	ingeneis
a	a	k8xC	a
De	De	k?	De
machinis	machinis	k1gInSc1	machinis
</s>
</p>
<p>
<s>
==	==	k?	==
Vydané	vydaný	k2eAgFnSc3d1	vydaná
faximile	faximila	k1gFnSc3	faximila
==	==	k?	==
</s>
</p>
<p>
<s>
TACCOLA	TACCOLA	kA	TACCOLA
<g/>
,	,	kIx,	,
Mariano	Mariana	k1gFnSc5	Mariana
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
Machinis	Machinis	k1gInSc1	Machinis
:	:	kIx,	:
Faksimile	faksimile	k1gNnPc2	faksimile
des	des	k1gNnSc7	des
Codex	Codex	k1gInSc1	Codex
Latinus	Latinus	k1gInSc1	Latinus
Monacensis	Monacensis	k1gFnSc1	Monacensis
28800	[number]	k4	28800
<g/>
,	,	kIx,	,
Teil	Teil	k1gInSc1	Teil
II	II	kA	II
<g/>
.	.	kIx.	.
in	in	k?	in
der	drát	k5eAaImRp2nS	drát
Bayerischen	Bayerischen	k2eAgInSc1d1	Bayerischen
Staatsbibliothek	Staatsbibliothek	k6eAd1	Staatsbibliothek
München	München	k2eAgInSc1d1	München
<g/>
.	.	kIx.	.
</s>
<s>
Wiesbaden	Wiesbaden	k1gInSc1	Wiesbaden
<g/>
:	:	kIx,	:
Ludwig	Ludwig	k1gMnSc1	Ludwig
Reichert	Reichert	k1gMnSc1	Reichert
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
404	[number]	k4	404
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9783920153056	[number]	k4	9783920153056
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TACCOLA	TACCOLA	kA	TACCOLA
<g/>
,	,	kIx,	,
Mariano	Mariana	k1gFnSc5	Mariana
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
rebus	rebus	k1gMnSc1	rebus
militaribus	militaribus	k1gMnSc1	militaribus
:	:	kIx,	:
(	(	kIx(	(
<g/>
De	De	k?	De
machins	machins	k1gInSc1	machins
<g/>
,	,	kIx,	,
1449	[number]	k4	1449
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Baden-Baden	Baden-Baden	k2eAgMnSc1d1	Baden-Baden
<g/>
:	:	kIx,	:
V.	V.	kA	V.
Koerner	Koerner	k1gMnSc1	Koerner
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Taccola	Taccola	k1gFnSc1	Taccola
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Taccola	Taccola	k1gFnSc1	Taccola
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
