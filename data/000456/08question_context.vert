<s>
Lombardie	Lombardie	k1gFnSc1	Lombardie
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Lombardia	Lombardium	k1gNnSc2	Lombardium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgInPc4d1	rozkládající
se	se	k3xPyFc4	se
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
až	až	k9	až
k	k	k7c3	k
údolí	údolí	k1gNnSc3	údolí
řeky	řeka	k1gFnSc2	řeka
Pád	Pád	k1gInSc1	Pád
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nP	sousedit
s	s	k7c7	s
regiony	region	k1gInPc7	region
Piemontem	Piemont	k1gInSc7	Piemont
<g/>
,	,	kIx,	,
Emilií-Romagnou	Emilií-Romagna	k1gFnSc7	Emilií-Romagna
<g/>
,	,	kIx,	,
Jižním	jižní	k2eAgNnSc7d1	jižní
Tyrolskem	Tyrolsko	k1gNnSc7	Tyrolsko
<g/>
,	,	kIx,	,
Veneto	Venet	k2eAgNnSc1d1	Veneto
a	a	k8xC	a
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejbohatší	bohatý	k2eAgInSc4d3	nejbohatší
region	region	k1gInSc4	region
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgInPc2d3	nejbohatší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>

