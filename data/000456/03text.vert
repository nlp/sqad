<s>
Lombardie	Lombardie	k1gFnSc1	Lombardie
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Lombardia	Lombardium	k1gNnSc2	Lombardium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgInPc4d1	rozkládající
se	se	k3xPyFc4	se
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
až	až	k9	až
k	k	k7c3	k
údolí	údolí	k1gNnSc3	údolí
řeky	řeka	k1gFnSc2	řeka
Pád	Pád	k1gInSc1	Pád
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nP	sousedit
s	s	k7c7	s
regiony	region	k1gInPc7	region
Piemontem	Piemont	k1gInSc7	Piemont
<g/>
,	,	kIx,	,
Emilií-Romagnou	Emilií-Romagna	k1gFnSc7	Emilií-Romagna
<g/>
,	,	kIx,	,
Jižním	jižní	k2eAgNnSc7d1	jižní
Tyrolskem	Tyrolsko	k1gNnSc7	Tyrolsko
<g/>
,	,	kIx,	,
Veneto	Venet	k2eAgNnSc1d1	Veneto
a	a	k8xC	a
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejbohatší	bohatý	k2eAgInSc4d3	nejbohatší
region	region	k1gInSc4	region
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgInPc2d3	nejbohatší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Region	region	k1gInSc1	region
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
23	[number]	k4	23
863	[number]	k4	863
km2	km2	k4	km2
<g/>
,	,	kIx,	,
obývá	obývat	k5eAaImIp3nS	obývat
ho	on	k3xPp3gNnSc4	on
10	[number]	k4	10
020	[number]	k4	020
210	[number]	k4	210
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
šestina	šestina	k1gFnSc1	šestina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nP	dělit
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
12	[number]	k4	12
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
největší	veliký	k2eAgFnSc1d3	veliký
konurbace	konurbace	k1gFnSc1	konurbace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Lombardie	Lombardie	k1gFnSc2	Lombardie
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
germánského	germánský	k2eAgInSc2d1	germánský
kmene	kmen	k1gInSc2	kmen
Langobardů	Langobard	k1gMnPc2	Langobard
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sem	sem	k6eAd1	sem
přišel	přijít	k5eAaPmAgMnS	přijít
roku	rok	k1gInSc2	rok
568	[number]	k4	568
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
pobřeží	pobřeží	k1gNnSc2	pobřeží
jižního	jižní	k2eAgInSc2d1	jižní
Baltu	Balt	k1gInSc2	Balt
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
sem	sem	k6eAd1	sem
zamířily	zamířit	k5eAaPmAgInP	zamířit
od	od	k7c2	od
severu	sever	k1gInSc2	sever
nebo	nebo	k8xC	nebo
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usazovali	usazovat	k5eAaImAgMnP	usazovat
Keltové	Kelt	k1gMnPc1	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
oblast	oblast	k1gFnSc1	oblast
tolik	tolik	k6eAd1	tolik
významná	významný	k2eAgFnSc1d1	významná
nebyla	být	k5eNaImAgFnS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
všechny	všechen	k3xTgFnPc1	všechen
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
přednosti	přednost	k1gFnPc1	přednost
Lombardie	Lombardie	k1gFnSc2	Lombardie
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Apeninským	apeninský	k2eAgInSc7d1	apeninský
poloostrovem	poloostrov	k1gInSc7	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
úrodné	úrodný	k2eAgNnSc4d1	úrodné
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dařilo	dařit	k5eAaImAgNnS	dařit
obilí	obilí	k1gNnSc1	obilí
<g/>
,	,	kIx,	,
ovoci	ovoce	k1gNnSc6	ovoce
<g/>
,	,	kIx,	,
vinné	vinný	k2eAgFnSc3d1	vinná
révě	réva	k1gFnSc3	réva
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
rýži	rýže	k1gFnSc4	rýže
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
spojovalo	spojovat	k5eAaImAgNnS	spojovat
s	s	k7c7	s
mocí	moc	k1gFnSc7	moc
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
přinášel	přinášet	k5eAaImAgInS	přinášet
silný	silný	k2eAgInSc1d1	silný
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
obchod	obchod	k1gInSc1	obchod
ze	z	k7c2	z
zaalpských	zaalpský	k2eAgFnPc2d1	zaalpská
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
stal	stát	k5eAaPmAgMnS	stát
centrem	centrum	k1gNnSc7	centrum
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
Lombardie	Lombardie	k1gFnSc2	Lombardie
jeho	jeho	k3xOp3gFnSc7	jeho
zásobárnou	zásobárna	k1gFnSc7	zásobárna
<g/>
;	;	kIx,	;
díky	díky	k7c3	díky
prodeji	prodej	k1gInSc3	prodej
místních	místní	k2eAgInPc2d1	místní
výrobků	výrobek	k1gInPc2	výrobek
se	se	k3xPyFc4	se
i	i	k9	i
ty	ten	k3xDgFnPc1	ten
dostávaly	dostávat	k5eAaImAgFnP	dostávat
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Lombardii	Lombardie	k1gFnSc6	Lombardie
měla	mít	k5eAaImAgFnS	mít
vždy	vždy	k6eAd1	vždy
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
Francie	Francie	k1gFnSc2	Francie
-	-	kIx~	-
naposledy	naposledy	k6eAd1	naposledy
zde	zde	k6eAd1	zde
vládl	vládnout	k5eAaImAgInS	vládnout
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
-	-	kIx~	-
i	i	k9	i
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
císařství	císařství	k1gNnSc4	císařství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
podmanilo	podmanit	k5eAaPmAgNnS	podmanit
Lombardii	Lombardie	k1gFnSc4	Lombardie
na	na	k7c4	na
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
obrovsky	obrovsky	k6eAd1	obrovsky
narůstala	narůstat	k5eAaImAgFnS	narůstat
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
už	už	k6eAd1	už
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
zakládala	zakládat	k5eAaImAgFnS	zakládat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
textilií	textilie	k1gFnPc2	textilie
a	a	k8xC	a
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
Milán	Milán	k1gInSc1	Milán
centrem	centr	k1gInSc7	centr
módy	móda	k1gFnSc2	móda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
i	i	k9	i
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Sardinskému	sardinský	k2eAgNnSc3d1	Sardinské
království	království	k1gNnSc3	království
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Lombardie	Lombardie	k1gFnSc1	Lombardie
stala	stát	k5eAaPmAgFnS	stát
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
centrem	centr	k1gInSc7	centr
italského	italský	k2eAgInSc2d1	italský
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
stěhovalo	stěhovat	k5eAaImAgNnS	stěhovat
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
krajanů	krajan	k1gMnPc2	krajan
z	z	k7c2	z
chudého	chudý	k2eAgInSc2d1	chudý
jihu	jih	k1gInSc2	jih
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Lombardie	Lombardie	k1gFnSc1	Lombardie
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
výsadní	výsadní	k2eAgNnSc1d1	výsadní
postavení	postavení	k1gNnSc1	postavení
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nP	sídlet
ty	ten	k3xDgFnPc1	ten
nejsilnější	silný	k2eAgFnPc1d3	nejsilnější
italské	italský	k2eAgFnPc1d1	italská
firmy	firma	k1gFnPc1	firma
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Mediaset	Mediaset	k1gInSc1	Mediaset
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
odtud	odtud	k6eAd1	odtud
přichází	přicházet	k5eAaImIp3nS	přicházet
odštěpenecké	odštěpenecký	k2eAgFnPc4d1	odštěpenecká
tendence	tendence	k1gFnPc4	tendence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lombardii	Lombardie	k1gFnSc6	Lombardie
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
cenných	cenný	k2eAgFnPc2d1	cenná
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
na	na	k7c6	na
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
i	i	k8xC	i
kulturní	kulturní	k2eAgFnSc6d1	kulturní
špičce	špička	k1gFnSc6	špička
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Lombardie	Lombardie	k1gFnSc2	Lombardie
tvoří	tvořit	k5eAaImIp3nS	tvořit
vysoké	vysoký	k2eAgInPc4d1	vysoký
horské	horský	k2eAgInPc4d1	horský
masivy	masiv	k1gInPc4	masiv
Alp	Alpy	k1gFnPc2	Alpy
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Pádská	pádský	k2eAgFnSc1d1	Pádská
nížina	nížina	k1gFnSc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Hory	hora	k1gFnPc1	hora
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
41	[number]	k4	41
<g/>
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
Lombardie	Lombardie	k1gFnSc2	Lombardie
<g/>
,	,	kIx,	,
vrchovina	vrchovina	k1gFnSc1	vrchovina
12	[number]	k4	12
<g/>
%	%	kIx~	%
a	a	k8xC	a
roviny	rovina	k1gFnPc1	rovina
a	a	k8xC	a
nížiny	nížina	k1gFnPc1	nížina
47	[number]	k4	47
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
území	území	k1gNnSc6	území
Lombardie	Lombardie	k1gFnSc2	Lombardie
<g/>
,	,	kIx,	,
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
Jižním	jižní	k2eAgFnPc3d1	jižní
vápencovým	vápencový	k2eAgFnPc3d1	vápencová
Alpám	Alpy	k1gFnPc3	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
pěti	pět	k4xCc2	pět
až	až	k9	až
šesti	šest	k4xCc2	šest
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
leží	ležet	k5eAaImIp3nP	ležet
Pohoří	pohoří	k1gNnPc1	pohoří
Bernina	Bernin	k2eAgNnPc1d1	Bernin
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
leží	ležet	k5eAaImIp3nS	ležet
skupina	skupina	k1gFnSc1	skupina
Bergamských	Bergamský	k2eAgFnPc2d1	Bergamský
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Pohoří	pohoří	k1gNnSc1	pohoří
Ortles	Ortlesa	k1gFnPc2	Ortlesa
<g/>
,	,	kIx,	,
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
Pohoří	pohořet	k5eAaPmIp3nS	pohořet
Livigno	Livigno	k6eAd1	Livigno
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
skupina	skupina	k1gFnSc1	skupina
Adamello	Adamello	k1gNnSc1	Adamello
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
Brenta	Brenta	k1gFnSc1	Brenta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
jsou	být	k5eAaImIp3nP	být
ledovcového	ledovcový	k2eAgMnSc4d1	ledovcový
původu	původa	k1gMnSc4	původa
<g/>
.	.	kIx.	.
</s>
<s>
Pádská	pádský	k2eAgFnSc1d1	Pádská
nížina	nížina	k1gFnSc1	nížina
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
vodní	vodní	k2eAgInPc4d1	vodní
toky	tok	k1gInPc4	tok
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c4	na
řeky	řeka	k1gFnPc4	řeka
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c4	na
uměle	uměle	k6eAd1	uměle
vystavěné	vystavěný	k2eAgInPc4d1	vystavěný
vodní	vodní	k2eAgInPc4d1	vodní
kanály	kanál	k1gInPc4	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Itálii	Itálie	k1gFnSc4	Itálie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pěstování	pěstování	k1gNnSc2	pěstování
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc2	kukuřice
a	a	k8xC	a
rýže	rýže	k1gFnSc2	rýže
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
lombardským	lombardský	k2eAgNnPc3d1	lombardské
jezerům	jezero	k1gNnPc3	jezero
náleží	náležet	k5eAaImIp3nS	náležet
Lago	Lago	k6eAd1	Lago
di	di	k?	di
Garda	garda	k1gFnSc1	garda
(	(	kIx(	(
<g/>
370	[number]	k4	370
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lago	Laga	k1gMnSc5	Laga
Maggiore	Maggior	k1gMnSc5	Maggior
(	(	kIx(	(
<g/>
212	[number]	k4	212
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
evropské	evropský	k2eAgNnSc1d1	Evropské
jezero	jezero	k1gNnSc1	jezero
Lago	Lago	k1gMnSc1	Lago
di	di	k?	di
Como	Como	k1gMnSc1	Como
(	(	kIx(	(
<g/>
146	[number]	k4	146
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lago	Lago	k1gMnSc1	Lago
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Iseo	Iseo	k1gMnSc1	Iseo
(	(	kIx(	(
<g/>
65	[number]	k4	65
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lago	Lago	k1gMnSc1	Lago
di	di	k?	di
Lugano	Lugana	k1gFnSc5	Lugana
(	(	kIx(	(
<g/>
49	[number]	k4	49
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lago	Lago	k6eAd1	Lago
di	di	k?	di
Varese	Varese	k1gFnSc1	Varese
(	(	kIx(	(
<g/>
15	[number]	k4	15
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lago	Lago	k1gMnSc1	Lago
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Idro	Idro	k1gMnSc1	Idro
(	(	kIx(	(
<g/>
11	[number]	k4	11
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
lombardské	lombardský	k2eAgFnPc1d1	Lombardská
řeky	řeka	k1gFnPc1	řeka
jsou	být	k5eAaImIp3nP	být
Pád	Pád	k1gInSc4	Pád
(	(	kIx(	(
<g/>
652	[number]	k4	652
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Adda	Adda	k1gMnSc1	Adda
(	(	kIx(	(
<g/>
313	[number]	k4	313
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oglio	Oglio	k1gMnSc1	Oglio
(	(	kIx(	(
<g/>
280	[number]	k4	280
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ticino	Ticino	k1gNnSc1	Ticino
(	(	kIx(	(
<g/>
248	[number]	k4	248
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mincio	Mincio	k1gMnSc1	Mincio
(	(	kIx(	(
<g/>
203	[number]	k4	203
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chiese	Chiese	k1gFnSc1	Chiese
(	(	kIx(	(
<g/>
160	[number]	k4	160
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejdůležitějším	důležitý	k2eAgFnPc3d3	nejdůležitější
chráněným	chráněný	k2eAgFnPc3d1	chráněná
oblastem	oblast	k1gFnPc3	oblast
náleží	náležet	k5eAaImIp3nP	náležet
největší	veliký	k2eAgInSc1d3	veliký
italský	italský	k2eAgInSc1d1	italský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Parco	Parco	k6eAd1	Parco
nazionale	nazionale	k6eAd1	nazionale
dello	delnout	k5eAaPmAgNnS	delnout
Stelvio	Stelvio	k6eAd1	Stelvio
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
regionální	regionální	k2eAgInSc4d1	regionální
park	park	k1gInSc4	park
Parco	Parco	k1gMnSc1	Parco
naturale	naturale	k?	naturale
della	della	k1gMnSc1	della
Valle	Valle	k1gFnSc2	Valle
del	del	k?	del
Ticino	Ticino	k1gNnSc1	Ticino
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
typické	typický	k2eAgFnSc2d1	typická
říční	říční	k2eAgFnSc2d1	říční
flóry	flóra	k1gFnSc2	flóra
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Lombardie	Lombardie	k1gFnSc1	Lombardie
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
12	[number]	k4	12
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnPc1	provincie
Bergamo	Bergamo	k1gNnSc1	Bergamo
<g/>
,	,	kIx,	,
Provincie	provincie	k1gFnSc1	provincie
Brescia	Brescia	k1gFnSc1	Brescia
<g/>
,	,	kIx,	,
Provincie	provincie	k1gFnSc1	provincie
Como	Como	k1gMnSc1	Como
<g/>
,	,	kIx,	,
Provincie	provincie	k1gFnSc1	provincie
Cremona	Cremona	k1gFnSc1	Cremona
<g/>
,	,	kIx,	,
Provincie	provincie	k1gFnSc1	provincie
Lecco	lecco	k3yInSc1	lecco
<g/>
,	,	kIx,	,
Provincie	provincie	k1gFnSc2	provincie
Lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
Provincie	provincie	k1gFnSc1	provincie
Mantova	Mantova	k1gFnSc1	Mantova
,	,	kIx,	,
Provincie	provincie	k1gFnSc1	provincie
Milano	Milana	k1gFnSc5	Milana
<g/>
,	,	kIx,	,
Provincie	provincie	k1gFnSc5	provincie
Monza	Monza	k1gFnSc1	Monza
e	e	k0	e
Brianza	Brianza	k1gFnSc1	Brianza
<g/>
,	,	kIx,	,
Provincie	provincie	k1gFnSc1	provincie
Pavia	Pavia	k1gFnSc1	Pavia
<g/>
,	,	kIx,	,
Provincie	provincie	k1gFnSc1	provincie
Sondrio	Sondrio	k1gMnSc1	Sondrio
<g/>
,	,	kIx,	,
Provincie	provincie	k1gFnSc1	provincie
Varese	Varese	k1gFnSc1	Varese
Milán	Milán	k1gInSc1	Milán
Bergamo	Bergamo	k1gNnSc1	Bergamo
Brescia	Brescius	k1gMnSc2	Brescius
Pavia	Pavius	k1gMnSc2	Pavius
Cremona	Cremon	k1gMnSc2	Cremon
Mantova	Mantova	k1gFnSc1	Mantova
Monza	Monza	k1gFnSc1	Monza
Como	Como	k6eAd1	Como
Varese	Varese	k1gFnSc2	Varese
</s>
