<s>
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgInSc1d3	nejmenší
světový	světový	k2eAgInSc1d1	světový
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
nejmenší	malý	k2eAgFnSc4d3	nejmenší
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
hloubku	hloubka	k1gFnSc4	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
severního	severní	k2eAgInSc2d1	severní
zemského	zemský	k2eAgInSc2d1	zemský
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
asi	asi	k9	asi
14	[number]	k4	14
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hloubka	hloubka	k1gFnSc1	hloubka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1328	[number]	k4	1328
m.	m.	k?	m.
Nejhlubší	hluboký	k2eAgMnSc1d3	nejhlubší
místo	místo	k7c2	místo
5449	[number]	k4	5449
m	m	kA	m
(	(	kIx(	(
<g/>
Polární	polární	k2eAgFnSc1d1	polární
hlubokomořská	hlubokomořský	k2eAgFnSc1d1	hlubokomořská
planina	planina	k1gFnSc1	planina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hladinu	hladina	k1gFnSc4	hladina
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
roku	rok	k1gInSc2	rok
silná	silný	k2eAgFnSc1d1	silná
vrstva	vrstva	k1gFnSc1	vrstva
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
po	po	k7c6	po
většině	většina	k1gFnSc6	většina
jeho	jeho	k3xOp3gFnSc2	jeho
plochy	plocha	k1gFnSc2	plocha
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
plavit	plavit	k5eAaImF	plavit
pouze	pouze	k6eAd1	pouze
těžké	těžký	k2eAgInPc4d1	těžký
ledoborce	ledoborec	k1gInPc4	ledoborec
(	(	kIx(	(
<g/>
a	a	k8xC	a
někde	někde	k6eAd1	někde
ani	ani	k8xC	ani
ty	ty	k3xPp2nSc1	ty
ne	ne	k9	ne
<g/>
)	)	kIx)	)
či	či	k8xC	či
lodě	loď	k1gFnPc1	loď
plující	plující	k2eAgFnPc1d1	plující
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
stopách	stopa	k1gFnPc6	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mořský	mořský	k2eAgInSc1d1	mořský
led	led	k1gInSc1	led
vzniká	vznikat	k5eAaImIp3nS	vznikat
ze	z	k7c2	z
zamrzlé	zamrzlý	k2eAgFnSc2d1	zamrzlá
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
polární	polární	k2eAgInSc1d1	polární
-	-	kIx~	-
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
tloušťka	tloušťka	k1gFnSc1	tloušťka
činí	činit	k5eAaImIp3nS	činit
až	až	k9	až
50	[number]	k4	50
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
taje	tát	k5eAaImIp3nS	tát
a	a	k8xC	a
tloušťka	tloušťka	k1gFnSc1	tloušťka
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
až	až	k9	až
na	na	k7c4	na
2	[number]	k4	2
m	m	kA	m
tabulový	tabulový	k2eAgMnSc1d1	tabulový
-	-	kIx~	-
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
tloušťka	tloušťka	k1gFnSc1	tloušťka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
2	[number]	k4	2
m.	m.	k?	m.
Led	led	k1gInSc1	led
je	být	k5eAaImIp3nS	být
polámaný	polámaný	k2eAgInSc1d1	polámaný
a	a	k8xC	a
ledové	ledový	k2eAgFnPc1d1	ledová
kry	kra	k1gFnPc1	kra
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
narážejí	narážet	k5eAaImIp3nP	narážet
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
maximálního	maximální	k2eAgInSc2d1	maximální
rozsahu	rozsah	k1gInSc2	rozsah
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
asi	asi	k9	asi
12	[number]	k4	12
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
rychlý	rychlý	k2eAgInSc1d1	rychlý
led	led	k1gInSc1	led
-	-	kIx~	-
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
tabulovým	tabulový	k2eAgInSc7d1	tabulový
ledem	led	k1gInSc7	led
a	a	k8xC	a
pobřežím	pobřeží	k1gNnSc7	pobřeží
V	v	k7c6	v
současně	současně	k6eAd1	současně
době	doba	k1gFnSc6	doba
tloušťka	tloušťka	k1gFnSc1	tloušťka
i	i	k8xC	i
plocha	plocha	k1gFnSc1	plocha
polárního	polární	k2eAgNnSc2d1	polární
zalednění	zalednění	k1gNnSc2	zalednění
rychle	rychle	k6eAd1	rychle
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
oceánu	oceán	k1gInSc2	oceán
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
obrovská	obrovský	k2eAgFnSc1d1	obrovská
pevninská	pevninský	k2eAgFnSc1d1	pevninská
deska	deska	k1gFnSc1	deska
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
příkop	příkop	k1gInSc4	příkop
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
horské	horský	k2eAgNnSc4d1	horské
pásmo	pásmo	k1gNnSc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Arktida	Arktida	k1gFnSc1	Arktida
je	být	k5eAaImIp3nS	být
kontinent	kontinent	k1gInSc4	kontinent
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
ale	ale	k8xC	ale
americká	americký	k2eAgFnSc1d1	americká
atomová	atomový	k2eAgFnSc1d1	atomová
ponorka	ponorka	k1gFnSc1	ponorka
USS	USS	kA	USS
Nautilus	nautilus	k1gInSc1	nautilus
podplula	podplout	k5eAaPmAgFnS	podplout
severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
a	a	k8xC	a
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc4	žádný
kontinent	kontinent	k1gInSc4	kontinent
Arktida	Arktida	k1gFnSc1	Arktida
neexistuje	existovat	k5eNaImIp3nS	existovat
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
trvale	trvale	k6eAd1	trvale
zamrzlá	zamrzlý	k2eAgFnSc1d1	zamrzlá
hladina	hladina	k1gFnSc1	hladina
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Baffinův	Baffinův	k2eAgInSc4d1	Baffinův
záliv	záliv	k1gInSc4	záliv
Barentsovo	Barentsův	k2eAgNnSc1d1	Barentsovo
moře	moře	k1gNnSc1	moře
Bílé	bílý	k2eAgNnSc1d1	bílé
moře	moře	k1gNnSc1	moře
Pečorské	Pečorský	k2eAgNnSc1d1	Pečorský
moře	moře	k1gNnSc1	moře
Beaufortovo	Beaufortův	k2eAgNnSc1d1	Beaufortovo
moře	moře	k1gNnSc1	moře
Čukotské	čukotský	k2eAgNnSc1d1	Čukotské
moře	moře	k1gNnSc1	moře
Grónské	grónský	k2eAgNnSc1d1	grónské
moře	moře	k1gNnSc1	moře
Karské	karský	k2eAgNnSc1d1	Karské
moře	moře	k1gNnSc1	moře
Moře	moře	k1gNnSc1	moře
Laptěvů	Laptěv	k1gInPc2	Laptěv
Východosibiřské	východosibiřský	k2eAgNnSc4d1	Východosibiřské
moře	moře	k1gNnSc4	moře
Moře	moře	k1gNnSc2	moře
a	a	k8xC	a
oceány	oceán	k1gInPc7	oceán
<g/>
,	,	kIx,	,
Editions	Editions	k1gInSc1	Editions
Atlas	Atlas	k1gInSc1	Atlas
2006	[number]	k4	2006
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Severní	severní	k2eAgNnSc4d1	severní
ledový	ledový	k2eAgInSc4d1	ledový
oceán	oceán	k1gInSc4	oceán
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Severní	severní	k2eAgInSc4d1	severní
ledový	ledový	k2eAgInSc4d1	ledový
oceán	oceán	k1gInSc4	oceán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
