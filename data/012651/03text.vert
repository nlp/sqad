<p>
<s>
Laserová	laserový	k2eAgFnSc1d1	laserová
tiskárna	tiskárna	k1gFnSc1	tiskárna
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
počítačové	počítačový	k2eAgFnSc2d1	počítačová
tiskárny	tiskárna	k1gFnSc2	tiskárna
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgFnSc2d1	pracující
na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
principu	princip	k1gInSc6	princip
jako	jako	k9	jako
kopírka	kopírka	k1gFnSc1	kopírka
<g/>
.	.	kIx.	.
</s>
<s>
Laserový	laserový	k2eAgInSc1d1	laserový
paprsek	paprsek	k1gInSc1	paprsek
vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
obrázek	obrázek	k1gInSc4	obrázek
na	na	k7c4	na
světlocitlivý	světlocitlivý	k2eAgInSc4d1	světlocitlivý
válec	válec	k1gInSc4	válec
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jehož	k3xOyRp3gInSc4	jehož
povrch	povrch	k1gInSc4	povrch
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
nanáší	nanášet	k5eAaImIp3nS	nanášet
toner	toner	k1gInSc4	toner
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
uchytí	uchytit	k5eAaPmIp3nS	uchytit
jen	jen	k9	jen
na	na	k7c6	na
osvětlených	osvětlený	k2eAgNnPc6d1	osvětlené
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
obtiskne	obtisknout	k5eAaPmIp3nS	obtisknout
se	se	k3xPyFc4	se
na	na	k7c4	na
papír	papír	k1gInSc4	papír
a	a	k8xC	a
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
papíru	papír	k1gInSc2	papír
tepelně	tepelně	k6eAd1	tepelně
fixován	fixován	k2eAgInSc1d1	fixován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
laserovou	laserový	k2eAgFnSc4d1	laserová
tiskárnu	tiskárna	k1gFnSc4	tiskárna
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
Gary	Gary	k1gInPc4	Gary
Starkweather	Starkweathra	k1gFnPc2	Starkweathra
v	v	k7c4	v
Xerox	xerox	k1gInSc4	xerox
PARC	PARC	kA	PARC
modifikací	modifikace	k1gFnSc7	modifikace
kopírky	kopírka	k1gFnSc2	kopírka
Xerox	Xerox	kA	Xerox
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
IBM	IBM	kA	IBM
instalovala	instalovat	k5eAaBmAgFnS	instalovat
první	první	k4xOgFnSc4	první
laserovou	laserový	k2eAgFnSc4d1	laserová
tiskárnu	tiskárna	k1gFnSc4	tiskárna
IBM	IBM	kA	IBM
3800	[number]	k4	3800
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Xerox	Xerox	kA	Xerox
dodala	dodat	k5eAaPmAgFnS	dodat
do	do	k7c2	do
komerčního	komerční	k2eAgNnSc2d1	komerční
prostředí	prostředí	k1gNnSc2	prostředí
první	první	k4xOgInSc1	první
model	model	k1gInSc1	model
Xarxs	Xarxs	k1gInSc1	Xarxs
90	[number]	k4	90
<g/>
Dicko	Dicko	k1gNnSc4	Dicko
priest	priest	k1gInSc1	priest
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
domácností	domácnost	k1gFnPc2	domácnost
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
laserovou	laserový	k2eAgFnSc4d1	laserová
tiskárnu	tiskárna	k1gFnSc4	tiskárna
HP	HP	kA	HP
(	(	kIx(	(
<g/>
HP	HP	kA	HP
Laserjet	Laserjet	k1gMnSc1	Laserjet
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
laserová	laserový	k2eAgFnSc1d1	laserová
tiskárna	tiskárna	k1gFnSc1	tiskárna
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
kancelářích	kancelář	k1gFnPc6	kancelář
byl	být	k5eAaImAgInS	být
dodán	dodat	k5eAaPmNgInS	dodat
Xerox	Xerox	kA	Xerox
Star	Star	kA	Star
8010	[number]	k4	8010
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
inovován	inovován	k2eAgInSc1d1	inovován
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
stále	stále	k6eAd1	stále
drahý	drahý	k2eAgInSc1d1	drahý
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
koupilo	koupit	k5eAaPmAgNnS	koupit
jenom	jenom	k6eAd1	jenom
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
osobní	osobní	k2eAgInPc4d1	osobní
počítače	počítač	k1gInPc4	počítač
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
více	hodně	k6eAd2	hodně
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
a	a	k8xC	a
první	první	k4xOgFnSc1	první
laserová	laserový	k2eAgFnSc1d1	laserová
tiskárna	tiskárna	k1gFnSc1	tiskárna
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
masový	masový	k2eAgInSc4d1	masový
trh	trh	k1gInSc4	trh
byla	být	k5eAaImAgFnS	být
Hewlett-Packard	Hewlett-Packard	k1gInSc4	Hewlett-Packard
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
HP	HP	kA	HP
<g/>
)	)	kIx)	)
LaserJet	LaserJet	k1gMnSc1	LaserJet
(	(	kIx(	(
<g/>
tiskla	tisknout	k5eAaImAgFnS	tisknout
8	[number]	k4	8
stránek	stránka	k1gFnPc2	stránka
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Používala	používat	k5eAaImAgFnS	používat
Canon	Canon	kA	Canon
motor	motor	k1gInSc4	motor
řízený	řízený	k2eAgInSc1d1	řízený
softwarem	software	k1gInSc7	software
HP	HP	kA	HP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Laserové	laserový	k2eAgFnPc1d1	laserová
tiskárny	tiskárna	k1gFnPc1	tiskárna
přinesly	přinést	k5eAaPmAgFnP	přinést
vysoce	vysoce	k6eAd1	vysoce
rychlý	rychlý	k2eAgInSc4d1	rychlý
a	a	k8xC	a
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
tisk	tisk	k1gInSc4	tisk
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
elektronických	elektronický	k2eAgNnPc2d1	elektronické
zařízení	zařízení	k1gNnPc2	zařízení
poklesly	poklesnout	k5eAaPmAgFnP	poklesnout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
náklady	náklad	k1gInPc7	náklad
na	na	k7c4	na
laserové	laserový	k2eAgFnPc4d1	laserová
tiskárny	tiskárna	k1gFnPc4	tiskárna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
typ	typ	k1gInSc1	typ
HP	HP	kA	HP
LaserJet	LaserJet	k1gInSc1	LaserJet
byl	být	k5eAaImAgInS	být
prodán	prodat	k5eAaPmNgInS	prodat
za	za	k7c4	za
3500	[number]	k4	3500
USD	USD	kA	USD
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgInS	mít
problém	problém	k1gInSc1	problém
s	s	k7c7	s
nízkým	nízký	k2eAgNnSc7d1	nízké
rozlišením	rozlišení	k1gNnSc7	rozlišení
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
vážil	vážit	k5eAaImAgInS	vážit
32	[number]	k4	32
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
funkce	funkce	k1gFnSc2	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
laserové	laserový	k2eAgFnSc2d1	laserová
tiskárny	tiskárna	k1gFnSc2	tiskárna
je	být	k5eAaImIp3nS	být
kovový	kovový	k2eAgInSc1d1	kovový
válec	válec	k1gInSc1	válec
s	s	k7c7	s
vrstvou	vrstva	k1gFnSc7	vrstva
polovodiče	polovodič	k1gInSc2	polovodič
(	(	kIx(	(
<g/>
např.	např.	kA	např.
selen	selen	k1gInSc1	selen
<g/>
)	)	kIx)	)
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Polovodič	polovodič	k1gInSc1	polovodič
mění	měnit	k5eAaImIp3nS	měnit
při	při	k7c6	při
osvícení	osvícení	k1gNnSc6	osvícení
odpor	odpor	k1gInSc4	odpor
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
Ω	Ω	k?	Ω
při	při	k7c6	při
osvícení	osvícení	k1gNnSc6	osvícení
až	až	k9	až
na	na	k7c4	na
cca	cca	kA	cca
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
MΩ	MΩ	k1gFnPc2	MΩ
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
osvícen	osvítit	k5eAaPmNgInS	osvítit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průběh	průběh	k1gInSc1	průběh
tisku	tisk	k1gInSc2	tisk
===	===	k?	===
</s>
</p>
<p>
<s>
Mechanický	mechanický	k2eAgInSc1d1	mechanický
stěrač	stěrač	k1gInSc1	stěrač
setře	setřít	k5eAaPmIp3nS	setřít
zbytky	zbytek	k1gInPc4	zbytek
toneru	toner	k1gInSc2	toner
a	a	k8xC	a
žárovka	žárovka	k1gFnSc1	žárovka
odstraní	odstranit	k5eAaPmIp3nS	odstranit
náboj	náboj	k1gInSc4	náboj
z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
fáze	fáze	k1gFnSc2	fáze
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
válce	válec	k1gInSc2	válec
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
šířce	šířka	k1gFnSc6	šířka
nabit	nabit	k2eAgInSc4d1	nabit
z	z	k7c2	z
korony	korona	k1gFnSc2	korona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
tisknout	tisknout	k5eAaImF	tisknout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
válec	válec	k1gInSc1	válec
osvícen	osvítit	k5eAaPmNgInS	osvítit
laserem	laser	k1gInSc7	laser
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
odpor	odpor	k1gInSc4	odpor
polovodiče	polovodič	k1gInSc2	polovodič
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
snížen	snížit	k5eAaPmNgInS	snížit
a	a	k8xC	a
náboj	náboj	k1gInSc1	náboj
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
se	se	k3xPyFc4	se
vybije	vybít	k5eAaPmIp3nS	vybít
do	do	k7c2	do
středu	střed	k1gInSc2	střed
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Toner	toner	k1gInSc1	toner
(	(	kIx(	(
<g/>
suchý	suchý	k2eAgInSc1d1	suchý
jemný	jemný	k2eAgInSc1d1	jemný
prášek	prášek	k1gInSc1	prášek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vlivem	vliv	k1gInSc7	vliv
otáčení	otáčení	k1gNnSc2	otáčení
válce	válec	k1gInSc2	válec
nabit	nabít	k5eAaPmNgInS	nabít
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
polaritu	polarita	k1gFnSc4	polarita
jako	jako	k8xS	jako
povrch	povrch	k1gInSc4	povrch
válce	válec	k1gInSc2	válec
a	a	k8xC	a
přilne	přilnout	k5eAaPmIp3nS	přilnout
k	k	k7c3	k
válci	válec	k1gInSc3	válec
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
odstraněn	odstranit	k5eAaPmNgInS	odstranit
náboj	náboj	k1gInSc1	náboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
místech	místo	k1gNnPc6	místo
je	být	k5eAaImIp3nS	být
toner	toner	k1gInSc4	toner
od	od	k7c2	od
válce	válec	k1gInSc2	válec
odpuzován	odpuzován	k2eAgMnSc1d1	odpuzován
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
polaritu	polarita	k1gFnSc4	polarita
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
toner	toner	k1gInSc1	toner
přenese	přenést	k5eAaPmIp3nS	přenést
z	z	k7c2	z
válce	válec	k1gInSc2	válec
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nabit	nabít	k5eAaPmNgInS	nabít
na	na	k7c4	na
opačnou	opačný	k2eAgFnSc4d1	opačná
hodnotu	hodnota	k1gFnSc4	hodnota
než	než	k8xS	než
povrch	povrch	k1gInSc4	povrch
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Papír	papír	k1gInSc1	papír
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
válec	válec	k1gInSc4	válec
dostane	dostat	k5eAaPmIp3nS	dostat
ze	z	k7c2	z
vstupního	vstupní	k2eAgInSc2d1	vstupní
zásobníku	zásobník	k1gInSc2	zásobník
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nabit	nabít	k5eAaPmNgInS	nabít
opačným	opačný	k2eAgInSc7d1	opačný
nábojem	náboj	k1gInSc7	náboj
než	než	k8xS	než
povrch	povrch	k1gInSc4	povrch
válce	válec	k1gInSc2	válec
a	a	k8xC	a
toner	toner	k1gInSc4	toner
<g/>
.	.	kIx.	.
</s>
<s>
Toner	toner	k1gInSc1	toner
se	se	k3xPyFc4	se
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
válci	válec	k1gInSc6	válec
s	s	k7c7	s
neutrálním	neutrální	k2eAgInSc7d1	neutrální
nábojem	náboj	k1gInSc7	náboj
přenese	přenést	k5eAaPmIp3nS	přenést
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nabit	nabít	k5eAaPmNgInS	nabít
nábojem	náboj	k1gInSc7	náboj
opačným	opačný	k2eAgInSc7d1	opačný
(	(	kIx(	(
<g/>
než	než	k8xS	než
toner	toner	k1gInSc4	toner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
toner	toner	k1gInSc4	toner
pomocí	pomocí	k7c2	pomocí
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
(	(	kIx(	(
<g/>
od	od	k7c2	od
180	[number]	k4	180
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
roztaven	roztavit	k5eAaPmNgInS	roztavit
a	a	k8xC	a
zapečen	zapéct	k5eAaPmNgInS	zapéct
do	do	k7c2	do
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
papíru	papír	k1gInSc2	papír
sejmut	sejmut	k2eAgInSc1d1	sejmut
náboj	náboj	k1gInSc1	náboj
a	a	k8xC	a
papír	papír	k1gInSc1	papír
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
do	do	k7c2	do
výstupního	výstupní	k2eAgInSc2d1	výstupní
zásobníku	zásobník	k1gInSc2	zásobník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Laserový	laserový	k2eAgInSc1d1	laserový
paprsek	paprsek	k1gInSc1	paprsek
prochází	procházet	k5eAaImIp3nS	procházet
deflektorem	deflektor	k1gInSc7	deflektor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
součástka	součástka	k1gFnSc1	součástka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
přivedeném	přivedený	k2eAgNnSc6d1	přivedené
napětí	napětí	k1gNnSc6	napětí
propouští	propouštět	k5eAaImIp3nS	propouštět
nebo	nebo	k8xC	nebo
nepropouští	propouštět	k5eNaImIp3nS	propouštět
světlo	světlo	k1gNnSc1	světlo
(	(	kIx(	(
<g/>
laserový	laserový	k2eAgInSc1d1	laserový
paprsek	paprsek	k1gInSc1	paprsek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
přivedené	přivedený	k2eAgNnSc1d1	přivedené
do	do	k7c2	do
deflektoru	deflektor	k1gInSc2	deflektor
je	být	k5eAaImIp3nS	být
obrazem	obraz	k1gInSc7	obraz
bitmapy	bitmapa	k1gFnSc2	bitmapa
tištěné	tištěný	k2eAgFnSc2d1	tištěná
stránky	stránka	k1gFnSc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Rotující	rotující	k2eAgNnSc1d1	rotující
zrcátko	zrcátko	k1gNnSc1	zrcátko
(	(	kIx(	(
<g/>
hranol	hranol	k1gInSc1	hranol
<g/>
)	)	kIx)	)
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
paprsek	paprsek	k1gInSc1	paprsek
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
šířce	šířka	k1gFnSc6	šířka
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příslušenství	příslušenství	k1gNnSc1	příslušenství
==	==	k?	==
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
příslušenství	příslušenství	k1gNnSc2	příslušenství
k	k	k7c3	k
tiskárnám	tiskárna	k1gFnPc3	tiskárna
řeší	řešit	k5eAaImIp3nP	řešit
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
papírem	papír	k1gInSc7	papír
na	na	k7c6	na
vstupní	vstupní	k2eAgFnSc6d1	vstupní
nebo	nebo	k8xC	nebo
výstupní	výstupní	k2eAgFnSc6d1	výstupní
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
kopírovacím	kopírovací	k2eAgInPc3d1	kopírovací
strojům	stroj	k1gInPc3	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podavač	podavač	k1gMnSc1	podavač
</s>
</p>
<p>
<s>
podavač	podavač	k1gMnSc1	podavač
z	z	k7c2	z
balíku	balík	k1gInSc2	balík
papírů	papír	k1gInPc2	papír
</s>
</p>
<p>
<s>
podavač	podavač	k1gInSc1	podavač
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
listy	list	k1gInPc4	list
vkládané	vkládaný	k2eAgInPc4d1	vkládaný
ručně	ručně	k6eAd1	ručně
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
ruční	ruční	k2eAgInSc1d1	ruční
podavač	podavač	k1gInSc1	podavač
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
manual	manual	k1gMnSc1	manual
feeder	feeder	k1gMnSc1	feeder
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podavač	podavač	k1gMnSc1	podavač
pásu	pás	k1gInSc2	pás
papíru	papír	k1gInSc2	papír
</s>
</p>
<p>
<s>
s	s	k7c7	s
podélnou	podélný	k2eAgFnSc7d1	podélná
perforací	perforace	k1gFnSc7	perforace
po	po	k7c6	po
okrajích	okraj	k1gInPc6	okraj
pro	pro	k7c4	pro
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
vedení	vedení	k1gNnSc4	vedení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
traktorový	traktorový	k2eAgInSc4d1	traktorový
<g/>
"	"	kIx"	"
papír	papír	k1gInSc4	papír
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
s	s	k7c7	s
příčnou	příčna	k1gFnSc7	příčna
perforací	perforace	k1gFnPc2	perforace
(	(	kIx(	(
<g/>
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
listy	list	k1gInPc4	list
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
třídicí	třídicí	k2eAgInSc1d1	třídicí
výstupní	výstupní	k2eAgInSc1d1	výstupní
zásobník	zásobník	k1gInSc1	zásobník
</s>
</p>
<p>
<s>
automatické	automatický	k2eAgNnSc1d1	automatické
sešívací	sešívací	k2eAgNnSc1d1	sešívací
zařízení	zařízení	k1gNnSc1	zařízení
</s>
</p>
<p>
<s>
oddělovač	oddělovač	k1gInSc1	oddělovač
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
gilotina	gilotina	k1gFnSc1	gilotina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
typická	typický	k2eAgFnSc1d1	typická
zejména	zejména	k9	zejména
u	u	k7c2	u
termotiskáren	termotiskárna	k1gFnPc2	termotiskárna
používaných	používaný	k2eAgFnPc2d1	používaná
v	v	k7c6	v
elektronických	elektronický	k2eAgInPc6d1	elektronický
pokladnáchJiné	pokladnáchJiné	k2eAgInPc6d1	pokladnáchJiné
druhy	druh	k1gInPc1	druh
příslušenství	příslušenství	k1gNnSc2	příslušenství
souvisí	souviset	k5eAaImIp3nP	souviset
se	s	k7c7	s
zjednodušeným	zjednodušený	k2eAgNnSc7d1	zjednodušené
ovládáním	ovládání	k1gNnSc7	ovládání
u	u	k7c2	u
tiskárny	tiskárna	k1gFnSc2	tiskárna
vyšší	vysoký	k2eAgFnSc2d2	vyšší
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
u	u	k7c2	u
kombinovaných	kombinovaný	k2eAgInPc2d1	kombinovaný
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
displej	displej	k1gInSc1	displej
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
dotykovou	dotykový	k2eAgFnSc7d1	dotyková
verzí	verze	k1gFnSc7	verze
</s>
</p>
<p>
<s>
skener	skener	k1gInSc1	skener
<g/>
,	,	kIx,	,
modem	modem	k1gInSc1	modem
-	-	kIx~	-
takovéto	takovýto	k3xDgNnSc1	takovýto
multifunkční	multifunkční	k2eAgNnSc1d1	multifunkční
zařízení	zařízení	k1gNnSc1	zařízení
potom	potom	k6eAd1	potom
může	moct	k5eAaImIp3nS	moct
plnit	plnit	k5eAaImF	plnit
úlohu	úloha	k1gFnSc4	úloha
kopírky	kopírka	k1gFnSc2	kopírka
a	a	k8xC	a
faxu	fax	k1gInSc2	fax
</s>
</p>
<p>
<s>
čtečky	čtečka	k1gFnPc1	čtečka
paměťových	paměťový	k2eAgFnPc2d1	paměťová
karet	kareta	k1gFnPc2	kareta
a	a	k8xC	a
USB	USB	kA	USB
klíčenek	klíčenka	k1gFnPc2	klíčenka
(	(	kIx(	(
<g/>
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
tiskem	tisk	k1gInSc7	tisk
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
softwarové	softwarový	k2eAgInPc1d1	softwarový
moduly	modul	k1gInPc1	modul
(	(	kIx(	(
<g/>
např.	např.	kA	např.
PostScript	PostScript	k2eAgInSc1d1	PostScript
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
moduly	modul	k1gInPc1	modul
s	s	k7c7	s
fonty	font	k1gInPc7	font
</s>
</p>
<p>
<s>
==	==	k?	==
Varianty	variant	k1gInPc4	variant
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
LED	LED	kA	LED
tiskárně	tiskárna	k1gFnSc6	tiskárna
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
soustava	soustava	k1gFnSc1	soustava
laseru	laser	k1gInSc2	laser
a	a	k8xC	a
příslušné	příslušný	k2eAgFnSc2d1	příslušná
optiky	optika	k1gFnSc2	optika
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
řadou	řada	k1gFnSc7	řada
nebo	nebo	k8xC	nebo
maticí	matice	k1gFnSc7	matice
LED	LED	kA	LED
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
válce	válec	k1gInSc2	válec
a	a	k8xC	a
pokrývající	pokrývající	k2eAgFnSc4d1	pokrývající
celou	celá	k1gFnSc4	celá
jeho	jeho	k3xOp3gFnSc4	jeho
šířku	šířka	k1gFnSc4	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
diod	dioda	k1gFnPc2	dioda
ozařuje	ozařovat	k5eAaImIp3nS	ozařovat
na	na	k7c6	na
válci	válec	k1gInSc6	válec
jeden	jeden	k4xCgInSc1	jeden
bod	bod	k1gInSc1	bod
ze	z	k7c2	z
vstupní	vstupní	k2eAgFnSc2d1	vstupní
bitmapy	bitmapa	k1gFnSc2	bitmapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typické	typický	k2eAgInPc1d1	typický
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
tiskárnou	tiskárna	k1gFnSc7	tiskárna
==	==	k?	==
</s>
</p>
<p>
<s>
nefungující	fungující	k2eNgInSc1d1	nefungující
podavač	podavač	k1gInSc1	podavač
(	(	kIx(	(
<g/>
separátor	separátor	k1gInSc1	separátor
<g/>
)	)	kIx)	)
papíru	papír	k1gInSc2	papír
-	-	kIx~	-
nenabírá	nabírat	k5eNaImIp3nS	nabírat
papír	papír	k1gInSc4	papír
nebo	nebo	k8xC	nebo
nabírá	nabírat	k5eAaImIp3nS	nabírat
více	hodně	k6eAd2	hodně
najednou	najednou	k6eAd1	najednou
</s>
</p>
<p>
<s>
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
degradované	degradovaný	k2eAgFnSc2d1	degradovaná
gumy	guma	k1gFnSc2	guma
v	v	k7c6	v
součástkách	součástka	k1gFnPc6	součástka
podavače	podavač	k1gInSc2	podavač
</s>
</p>
<p>
<s>
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
znečištění	znečištění	k1gNnSc2	znečištění
prachem	prach	k1gInSc7	prach
z	z	k7c2	z
nekvalitního	kvalitní	k2eNgInSc2d1	nekvalitní
papíru	papír	k1gInSc2	papír
</s>
</p>
<p>
<s>
znečištěná	znečištěný	k2eAgFnSc1d1	znečištěná
dráha	dráha	k1gFnSc1	dráha
papíru	papír	k1gInSc2	papír
(	(	kIx(	(
<g/>
prach	prach	k1gInSc1	prach
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
mačkání	mačkání	k1gNnSc1	mačkání
<g/>
,	,	kIx,	,
rozmazávání	rozmazávání	k1gNnSc1	rozmazávání
toneru	toner	k1gInSc2	toner
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Výhody	výhoda	k1gFnPc1	výhoda
==	==	k?	==
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
použití	použití	k1gNnSc3	použití
toneru	toner	k1gInSc2	toner
je	být	k5eAaImIp3nS	být
cena	cena	k1gFnSc1	cena
tisku	tisk	k1gInSc2	tisk
nízká	nízký	k2eAgFnSc1d1	nízká
a	a	k8xC	a
provoz	provoz	k1gInSc1	provoz
tichý	tichý	k2eAgInSc1d1	tichý
a	a	k8xC	a
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
kvalita	kvalita	k1gFnSc1	kvalita
obrazu	obraz	k1gInSc2	obraz
</s>
</p>
<p>
<s>
==	==	k?	==
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
==	==	k?	==
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
pořizovací	pořizovací	k2eAgFnSc1d1	pořizovací
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pořád	pořád	k6eAd1	pořád
klesající	klesající	k2eAgFnPc1d1	klesající
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
tiskárna	tiskárna	k1gFnSc1	tiskárna
</s>
</p>
<p>
<s>
Laser	laser	k1gInSc1	laser
</s>
</p>
<p>
<s>
Kopírka	kopírka	k1gFnSc1	kopírka
</s>
</p>
