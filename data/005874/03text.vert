<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
jsou	být	k5eAaImIp3nP	být
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
svátky	svátek	k1gInPc4	svátek
narození	narození	k1gNnSc2	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Vánoci	Vánoce	k1gFnPc7	Vánoce
souvisí	souviset	k5eAaImIp3nS	souviset
doba	doba	k1gFnSc1	doba
adventní	adventní	k2eAgFnSc1d1	adventní
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jim	on	k3xPp3gMnPc3	on
předchází	předcházet	k5eAaImIp3nS	předcházet
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
příprava	příprava	k1gFnSc1	příprava
na	na	k7c6	na
narození	narození	k1gNnSc6	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
patří	patřit	k5eAaImIp3nP	patřit
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Velikonocemi	Velikonoce	k1gFnPc7	Velikonoce
a	a	k8xC	a
Letnicemi	letnice	k1gFnPc7	letnice
<g/>
)	)	kIx)	)
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
křesťanským	křesťanský	k2eAgMnPc3d1	křesťanský
svátkům	svátek	k1gInPc3	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Slavnost	slavnost	k1gFnSc1	slavnost
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgNnSc2d1	páně
připadá	připadat	k5eAaPmIp3nS	připadat
tradičně	tradičně	k6eAd1	tradičně
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
Vánoce	Vánoce	k1gFnPc1	Vánoce
se	se	k3xPyFc4	se
v	v	k7c6	v
římskokatolické	římskokatolický	k2eAgFnSc6d1	Římskokatolická
tradici	tradice	k1gFnSc6	tradice
slaví	slavit	k5eAaImIp3nS	slavit
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
předvečera	předvečer	k1gInSc2	předvečer
(	(	kIx(	(
<g/>
večer	večer	k6eAd1	večer
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
do	do	k7c2	do
svátku	svátek	k1gInSc2	svátek
Křtu	křest	k1gInSc2	křest
Páně	páně	k2eAgInSc2d1	páně
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc2	první
neděle	neděle	k1gFnSc2	neděle
po	po	k7c4	po
6	[number]	k4	6
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
13	[number]	k4	13
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
záznamy	záznam	k1gInPc1	záznam
oslav	oslava	k1gFnPc2	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
křesťany	křesťan	k1gMnPc4	křesťan
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
převládající	převládající	k2eAgFnSc7d1	převládající
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
tradicí	tradice	k1gFnSc7	tradice
začíná	začínat	k5eAaImIp3nS	začínat
oslava	oslava	k1gFnSc1	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
předvečer	předvečer	k1gInSc1	předvečer
(	(	kIx(	(
<g/>
vigilie	vigilie	k1gFnPc1	vigilie
<g/>
)	)	kIx)	)
vánoční	vánoční	k2eAgFnPc1d1	vánoční
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgFnPc1d1	samotná
Vánoce	Vánoce	k1gFnPc1	Vánoce
podle	podle	k7c2	podle
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
tradice	tradice	k1gFnSc2	tradice
(	(	kIx(	(
<g/>
respektující	respektující	k2eAgFnSc4d1	respektující
starší	starý	k2eAgFnSc4d2	starší
židovskou	židovský	k2eAgFnSc4d1	židovská
zvyklost	zvyklost	k1gFnSc4	zvyklost
počítání	počítání	k1gNnSc2	počítání
nového	nový	k2eAgInSc2d1	nový
dne	den	k1gInSc2	den
od	od	k7c2	od
soumraku	soumrak	k1gInSc2	soumrak
<g/>
)	)	kIx)	)
začínají	začínat	k5eAaImIp3nP	začínat
s	s	k7c7	s
vyjitím	vyjití	k1gNnSc7	vyjití
první	první	k4xOgFnSc2	první
večerní	večerní	k2eAgFnSc2d1	večerní
hvězdy	hvězda	k1gFnSc2	hvězda
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
množství	množství	k1gNnSc1	množství
lidových	lidový	k2eAgFnPc2d1	lidová
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
vánoční	vánoční	k2eAgInSc1d1	vánoční
stromek	stromek	k1gInSc1	stromek
<g/>
,	,	kIx,	,
jesličky	jesličky	k1gFnPc1	jesličky
(	(	kIx(	(
<g/>
betlém	betlém	k1gInSc1	betlém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vánoční	vánoční	k2eAgInPc4d1	vánoční
dárky	dárek	k1gInPc4	dárek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
podle	podle	k7c2	podle
české	český	k2eAgFnSc2d1	Česká
tradice	tradice	k1gFnSc2	tradice
nosí	nosit	k5eAaImIp3nS	nosit
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
<g/>
,	,	kIx,	,
či	či	k8xC	či
vánoční	vánoční	k2eAgNnSc4d1	vánoční
cukroví	cukroví	k1gNnSc4	cukroví
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
Vánoce	Vánoce	k1gFnPc1	Vánoce
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
vnímány	vnímat	k5eAaImNgInP	vnímat
převážně	převážně	k6eAd1	převážně
jako	jako	k8xS	jako
svátky	svátek	k1gInPc4	svátek
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
lidé	člověk	k1gMnPc1	člověk
slaví	slavit	k5eAaImIp3nP	slavit
společně	společně	k6eAd1	společně
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
náboženské	náboženský	k2eAgNnSc4d1	náboženské
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Vánoce	Vánoce	k1gFnPc1	Vánoce
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
staroněmeckého	staroněmecký	k2eAgMnSc2d1	staroněmecký
wā	wā	k?	wā
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Weihnachten	Weihnachten	k2eAgInSc1d1	Weihnachten
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
složeného	složený	k2eAgInSc2d1	složený
z	z	k7c2	z
wī	wī	k?	wī
(	(	kIx(	(
<g/>
světit	světit	k5eAaImF	světit
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nacht	Nacht	k1gInSc4	Nacht
(	(	kIx(	(
<g/>
noc	noc	k1gFnSc4	noc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Machkův	Machkův	k2eAgInSc1d1	Machkův
etymologický	etymologický	k2eAgInSc1d1	etymologický
slovník	slovník	k1gInSc1	slovník
uvádí	uvádět	k5eAaImIp3nS	uvádět
staroněmecký	staroněmecký	k2eAgInSc1d1	staroněmecký
tvar	tvar	k1gInSc1	tvar
wínnahten	wínnahten	k2eAgInSc1d1	wínnahten
a	a	k8xC	a
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
muselo	muset	k5eAaImAgNnS	muset
dojít	dojít	k5eAaPmF	dojít
ještě	ještě	k9	ještě
v	v	k7c6	v
předcyrilometodějské	předcyrilometodějský	k2eAgFnSc6d1	předcyrilometodějský
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
Pfeiferův	Pfeiferův	k2eAgInSc1d1	Pfeiferův
etymologický	etymologický	k2eAgInSc1d1	etymologický
slovník	slovník	k1gInSc1	slovník
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
německé	německý	k2eAgNnSc1d1	německé
weihen	weihen	k1gInSc4	weihen
od	od	k7c2	od
staroindického	staroindický	k2eAgNnSc2d1	staroindické
vinákti	vinákti	k1gNnSc2	vinákti
<g/>
,	,	kIx,	,
třídit	třídit	k5eAaImF	třídit
<g/>
,	,	kIx,	,
oddělovat	oddělovat	k5eAaImF	oddělovat
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
oporu	opora	k1gFnSc4	opora
v	v	k7c6	v
dokladech	doklad	k1gInPc6	doklad
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
lidovou	lidový	k2eAgFnSc7d1	lidová
<g/>
"	"	kIx"	"
etymologií	etymologie	k1gFnSc7	etymologie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
výrazu	výraz	k1gInSc2	výraz
dvě	dva	k4xCgFnPc4	dva
noce	noce	k1gFnPc2	noce
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
označoval	označovat	k5eAaImAgInS	označovat
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
noc	noc	k1gFnSc1	noc
trvala	trvat	k5eAaImAgFnS	trvat
jako	jako	k9	jako
tři	tři	k4xCgInPc4	tři
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Místo	místo	k7c2	místo
termínu	termín	k1gInSc2	termín
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
několikadenní	několikadenní	k2eAgInSc1d1	několikadenní
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
obecně	obecně	k6eAd1	obecně
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc4	označení
Štědrý	štědrý	k2eAgInSc1d1	štědrý
den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
jde	jít	k5eAaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
svátků	svátek	k1gInPc2	svátek
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
hovorovější	hovorový	k2eAgMnSc1d2	hovorový
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
označení	označení	k1gNnSc2	označení
předvečeru	předvečer	k1gInSc3	předvečer
svátku	svátek	k1gInSc2	svátek
je	být	k5eAaImIp3nS	být
používán	používán	k2eAgInSc1d1	používán
termín	termín	k1gInSc1	termín
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
pojmenování	pojmenování	k1gNnSc1	pojmenování
období	období	k1gNnSc3	období
bývá	bývat	k5eAaImIp3nS	bývat
používán	používán	k2eAgInSc4d1	používán
výraz	výraz	k1gInSc4	výraz
Vánoční	vánoční	k2eAgInPc4d1	vánoční
svátky	svátek	k1gInPc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Náboženskou	náboženský	k2eAgFnSc7d1	náboženská
podstatou	podstata	k1gFnSc7	podstata
Vánoc	Vánoce	k1gFnPc2	Vánoce
je	být	k5eAaImIp3nS	být
oslava	oslava	k1gFnSc1	oslava
narození	narození	k1gNnSc2	narození
(	(	kIx(	(
<g/>
vtělení	vtělení	k1gNnSc2	vtělení
<g/>
)	)	kIx)	)
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
a	a	k8xC	a
okolnosti	okolnost	k1gFnPc1	okolnost
narození	narození	k1gNnSc2	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
znázorňovány	znázorňován	k2eAgInPc1d1	znázorňován
pomocí	pomocí	k7c2	pomocí
vánočních	vánoční	k2eAgInPc2d1	vánoční
betlémů	betlém	k1gInPc2	betlém
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
k	k	k7c3	k
početí	početí	k1gNnSc3	početí
a	a	k8xC	a
narození	narození	k1gNnSc4	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
Nazaretského	nazaretský	k2eAgMnSc2d1	nazaretský
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgFnP	popsat
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
zákoně	zákon	k1gInSc6	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
zdroji	zdroj	k1gInPc7	zdroj
jsou	být	k5eAaImIp3nP	být
evangelia	evangelium	k1gNnSc2	evangelium
podle	podle	k7c2	podle
Lukáše	Lukáš	k1gMnSc2	Lukáš
a	a	k8xC	a
podle	podle	k7c2	podle
Matouše	Matouš	k1gMnSc2	Matouš
<g/>
.	.	kIx.	.
</s>
<s>
Biblické	biblický	k2eAgInPc1d1	biblický
příběhy	příběh	k1gInPc1	příběh
narození	narození	k1gNnSc2	narození
Krista	Kristus	k1gMnSc2	Kristus
mají	mít	k5eAaImIp3nP	mít
zčásti	zčásti	k6eAd1	zčásti
legendární	legendární	k2eAgInSc4d1	legendární
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
přesný	přesný	k2eAgInSc4d1	přesný
popis	popis	k1gInSc4	popis
historické	historický	k2eAgFnSc2d1	historická
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
matka	matka	k1gFnSc1	matka
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
,	,	kIx,	,
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
,	,	kIx,	,
v	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
biblického	biblický	k2eAgNnSc2d1	biblické
vyprávění	vyprávění	k1gNnSc2	vyprávění
zjevil	zjevit	k5eAaPmAgMnS	zjevit
archanděl	archanděl	k1gMnSc1	archanděl
Gabriel	Gabriel	k1gMnSc1	Gabriel
v	v	k7c6	v
Nazaretě	Nazaret	k1gInSc6	Nazaret
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
panně	panna	k1gFnSc3	panna
zasnoubené	zasnoubená	k1gFnSc2	zasnoubená
Josefovi	Josef	k1gMnSc3	Josef
<g/>
,	,	kIx,	,
muži	muž	k1gMnSc3	muž
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Davidova	Davidův	k2eAgInSc2d1	Davidův
<g/>
.	.	kIx.	.
</s>
<s>
Anděl	Anděl	k1gMnSc1	Anděl
Marii	Maria	k1gFnSc4	Maria
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
počne	počnout	k5eAaPmIp3nS	počnout
a	a	k8xC	a
porodí	porodit	k5eAaPmIp3nS	porodit
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
bude	být	k5eAaImBp3nS	být
navěky	navěky	k6eAd1	navěky
kralovat	kralovat	k5eAaImF	kralovat
nad	nad	k7c7	nad
rodem	rod	k1gInSc7	rod
Jákobovým	Jákobův	k2eAgInSc7d1	Jákobův
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
království	království	k1gNnSc6	království
bude	být	k5eAaImBp3nS	být
bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
chtěl	chtít	k5eAaImAgMnS	chtít
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marie	Marie	k1gFnSc1	Marie
počala	počnout	k5eAaPmAgFnS	počnout
z	z	k7c2	z
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k1gMnSc2	svatý
<g/>
,	,	kIx,	,
Marii	Maria	k1gFnSc3	Maria
potají	potají	k6eAd1	potají
propustit	propustit	k5eAaPmF	propustit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
anděl	anděl	k1gMnSc1	anděl
a	a	k8xC	a
pravil	pravit	k5eAaBmAgMnS	pravit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Josefe	Josef	k1gMnSc5	Josef
<g/>
,	,	kIx,	,
synu	syn	k1gMnSc3	syn
Davidův	Davidův	k2eAgMnSc1d1	Davidův
<g/>
,	,	kIx,	,
neboj	bát	k5eNaImRp2nS	bát
se	se	k3xPyFc4	se
přijmout	přijmout	k5eAaPmF	přijmout
Marii	Maria	k1gFnSc3	Maria
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
co	co	k3yRnSc1	co
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
počato	počnout	k5eAaPmNgNnS	počnout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Porodí	porodit	k5eAaPmIp3nS	porodit
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
dáš	dát	k5eAaPmIp2nS	dát
mu	on	k3xPp3gMnSc3	on
jméno	jméno	k1gNnSc1	jméno
Ježíš	Ježíš	k1gMnSc1	Ježíš
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
on	on	k3xPp3gMnSc1	on
vysvobodí	vysvobodit	k5eAaPmIp3nS	vysvobodit
svůj	svůj	k3xOyFgInSc4	svůj
lid	lid	k1gInSc4	lid
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
hříchů	hřích	k1gInPc2	hřích
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
císařského	císařský	k2eAgInSc2d1	císařský
příkazu	příkaz	k1gInSc2	příkaz
ke	k	k7c3	k
sčítání	sčítání	k1gNnSc3	sčítání
lidu	lid	k1gInSc2	lid
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
vydali	vydat	k5eAaPmAgMnP	vydat
z	z	k7c2	z
Galileje	Galilea	k1gFnSc2	Galilea
<g/>
,	,	kIx,	,
z	z	k7c2	z
města	město	k1gNnSc2	město
Nazareta	Nazaret	k1gInSc2	Nazaret
<g/>
,	,	kIx,	,
do	do	k7c2	do
Judska	Judsko	k1gNnSc2	Judsko
<g/>
,	,	kIx,	,
do	do	k7c2	do
města	město	k1gNnSc2	město
Davidova	Davidův	k2eAgInSc2d1	Davidův
jménem	jméno	k1gNnSc7	jméno
Betlém	Betlém	k1gInSc1	Betlém
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
měl	mít	k5eAaImAgMnS	mít
nechat	nechat	k5eAaPmF	nechat
zapsat	zapsat	k5eAaPmF	zapsat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
porodila	porodit	k5eAaPmAgFnS	porodit
Marie	Marie	k1gFnSc1	Marie
svého	svůj	k3xOyFgMnSc4	svůj
prvorozeného	prvorozený	k2eAgMnSc4d1	prvorozený
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
zavinula	zavinout	k5eAaPmAgFnS	zavinout
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
plenek	plenka	k1gFnPc2	plenka
a	a	k8xC	a
položila	položit	k5eAaPmAgFnS	položit
do	do	k7c2	do
jeslí	jesle	k1gFnPc2	jesle
v	v	k7c6	v
chlévě	chlév	k1gInSc6	chlév
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nocovali	nocovat	k5eAaImAgMnP	nocovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
nenašlo	najít	k5eNaPmAgNnS	najít
místo	místo	k1gNnSc1	místo
pod	pod	k7c7	pod
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgMnPc1d1	zdejší
pastýři	pastýř	k1gMnPc1	pastýř
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pásli	pásnout	k5eAaImAgMnP	pásnout
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
střídali	střídat	k5eAaImAgMnP	střídat
v	v	k7c6	v
hlídkách	hlídka	k1gFnPc6	hlídka
u	u	k7c2	u
svého	svůj	k3xOyFgNnSc2	svůj
stáda	stádo	k1gNnSc2	stádo
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
noci	noc	k1gFnSc3	noc
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
přistál	přistát	k5eAaPmAgMnS	přistát
anděl	anděl	k1gMnSc1	anděl
Páně	páně	k2eAgMnSc1d1	páně
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
jim	on	k3xPp3gMnPc3	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
narodil	narodit	k5eAaPmAgMnS	narodit
Spasitel	spasitel	k1gMnSc1	spasitel
<g/>
,	,	kIx,	,
Kristus	Kristus	k1gMnSc1	Kristus
Pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
v	v	k7c6	v
městě	město	k1gNnSc6	město
Davidově	Davidův	k2eAgNnSc6d1	Davidovo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vám	vy	k3xPp2nPc3	vy
bude	být	k5eAaImBp3nS	být
znamením	znamení	k1gNnSc7	znamení
<g/>
:	:	kIx,	:
Naleznete	naleznout	k5eAaPmIp2nP	naleznout
děťátko	děťátko	k1gNnSc4	děťátko
v	v	k7c6	v
plenkách	plenka	k1gFnPc6	plenka
<g/>
,	,	kIx,	,
položené	položený	k2eAgFnPc1d1	položená
do	do	k7c2	do
jeslí	jesle	k1gFnPc2	jesle
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pastýři	pastýř	k1gMnPc1	pastýř
spěchali	spěchat	k5eAaImAgMnP	spěchat
do	do	k7c2	do
Betléma	Betlém	k1gInSc2	Betlém
a	a	k8xC	a
nalezli	naleznout	k5eAaPmAgMnP	naleznout
Marii	Maria	k1gFnSc4	Maria
a	a	k8xC	a
Josefa	Josef	k1gMnSc4	Josef
i	i	k8xC	i
to	ten	k3xDgNnSc1	ten
děťátko	děťátko	k1gNnSc1	děťátko
položené	položený	k2eAgNnSc1d1	položené
do	do	k7c2	do
jeslí	jesle	k1gFnPc2	jesle
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	on	k3xPp3gNnSc4	on
spatřili	spatřit	k5eAaPmAgMnP	spatřit
<g/>
,	,	kIx,	,
pověděli	povědět	k5eAaPmAgMnP	povědět
<g/>
,	,	kIx,	,
co	co	k8xS	co
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
dítěti	dítě	k1gNnSc6	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
to	ten	k3xDgNnSc4	ten
uslyšeli	uslyšet	k5eAaPmAgMnP	uslyšet
<g/>
,	,	kIx,	,
užasli	užasnout	k5eAaPmAgMnP	užasnout
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
jim	on	k3xPp3gMnPc3	on
pastýři	pastýř	k1gMnPc1	pastýř
vyprávěli	vyprávět	k5eAaImAgMnP	vyprávět
<g/>
.	.	kIx.	.
</s>
<s>
Narození	narození	k1gNnSc4	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
předpověděla	předpovědět	k5eAaPmAgFnS	předpovědět
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
oslavách	oslava	k1gFnPc6	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
zobrazována	zobrazován	k2eAgFnSc1d1	zobrazována
jako	jako	k8xC	jako
kometa	kometa	k1gFnSc1	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdu	hvězda	k1gFnSc4	hvězda
spatřili	spatřit	k5eAaPmAgMnP	spatřit
mudrci	mudrc	k1gMnPc1	mudrc
z	z	k7c2	z
východu	východ	k1gInSc2	východ
a	a	k8xC	a
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyptávali	vyptávat	k5eAaImAgMnP	vyptávat
po	po	k7c6	po
novorozeném	novorozený	k2eAgInSc6d1	novorozený
králi	král	k1gMnSc3	král
Židů	Žid	k1gMnPc2	Žid
(	(	kIx(	(
<g/>
Ježíšovi	Ježíš	k1gMnSc6	Ježíš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
hvězdu	hvězda	k1gFnSc4	hvězda
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
poklonili	poklonit	k5eAaPmAgMnP	poklonit
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
donesly	donést	k5eAaPmAgInP	donést
králi	král	k1gMnSc6	král
Herodovi	Herod	k1gMnSc6	Herod
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
znepokojil	znepokojit	k5eAaPmAgMnS	znepokojit
<g/>
,	,	kIx,	,
svolal	svolat	k5eAaPmAgMnS	svolat
kneží	knežet	k5eAaImIp3nS	knežet
a	a	k8xC	a
zákoníky	zákoník	k1gMnPc4	zákoník
a	a	k8xC	a
vyptával	vyptávat	k5eAaImAgMnS	vyptávat
se	se	k3xPyFc4	se
jich	on	k3xPp3gNnPc2	on
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
narození	narození	k1gNnSc2	narození
onoho	onen	k3xDgMnSc2	onen
Mesiáše	Mesiáš	k1gMnSc2	Mesiáš
<g/>
.	.	kIx.	.
</s>
<s>
Oni	onen	k3xDgMnPc1	onen
mu	on	k3xPp3gMnSc3	on
odpověděli	odpovědět	k5eAaPmAgMnP	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
judském	judský	k2eAgInSc6d1	judský
Betlémě	Betlém	k1gInSc6	Betlém
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
psáno	psán	k2eAgNnSc1d1	psáno
u	u	k7c2	u
proroka	prorok	k1gMnSc2	prorok
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Herodes	Herodes	k1gMnSc1	Herodes
tedy	tedy	k9	tedy
vyslal	vyslat	k5eAaPmAgMnS	vyslat
mudrce	mudrc	k1gMnSc4	mudrc
do	do	k7c2	do
Betléma	Betlém	k1gInSc2	Betlém
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dítě	dítě	k1gNnSc4	dítě
nalezli	nalézt	k5eAaBmAgMnP	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
mu	on	k3xPp3gMnSc3	on
měli	mít	k5eAaImAgMnP	mít
podat	podat	k5eAaPmF	podat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
také	také	k9	také
přijít	přijít	k5eAaPmF	přijít
poklonit	poklonit	k5eAaPmF	poklonit
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mudrcové	mudrc	k1gMnPc1	mudrc
předtím	předtím	k6eAd1	předtím
spatřili	spatřit	k5eAaPmAgMnP	spatřit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
opět	opět	k6eAd1	opět
zjevila	zjevit	k5eAaPmAgFnS	zjevit
a	a	k8xC	a
šla	jít	k5eAaImAgFnS	jít
před	před	k7c7	před
nimi	on	k3xPp3gInPc7	on
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
zastavila	zastavit	k5eAaPmAgFnS	zastavit
nad	nad	k7c7	nad
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Mudrcové	mudrc	k1gMnPc1	mudrc
tak	tak	k6eAd1	tak
nalezli	naleznout	k5eAaPmAgMnP	naleznout
dům	dům	k1gInSc4	dům
i	i	k8xC	i
Ježíše	Ježíš	k1gMnPc4	Ježíš
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Poklonili	poklonit	k5eAaPmAgMnP	poklonit
se	se	k3xPyFc4	se
Ježíšovi	Ježíšův	k2eAgMnPc1d1	Ježíšův
a	a	k8xC	a
předali	předat	k5eAaPmAgMnP	předat
mu	on	k3xPp3gMnSc3	on
dary	dar	k1gInPc4	dar
<g/>
:	:	kIx,	:
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
kadidlo	kadidlo	k1gNnSc4	kadidlo
a	a	k8xC	a
myrhu	myrha	k1gFnSc4	myrha
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
dostali	dostat	k5eAaPmAgMnP	dostat
napomenutí	napomenutí	k1gNnSc4	napomenutí
ve	v	k7c6	v
snách	sen	k1gInPc6	sen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nevraceli	vracet	k5eNaImAgMnP	vracet
k	k	k7c3	k
Herodovi	Herod	k1gMnSc3	Herod
<g/>
,	,	kIx,	,
vrátili	vrátit	k5eAaPmAgMnP	vrátit
se	se	k3xPyFc4	se
mudrcové	mudrc	k1gMnPc1	mudrc
jinou	jiný	k2eAgFnSc7d1	jiná
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Josefovi	Josef	k1gMnSc3	Josef
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
také	také	k9	také
sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
anděl	anděl	k1gMnSc1	anděl
a	a	k8xC	a
varoval	varovat	k5eAaImAgMnS	varovat
jej	on	k3xPp3gMnSc4	on
před	před	k7c7	před
Herodem	Herod	k1gMnSc7	Herod
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Marie	Marie	k1gFnSc1	Marie
s	s	k7c7	s
Ježíšem	Ježíš	k1gMnSc7	Ježíš
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Herodes	Herodes	k1gMnSc1	Herodes
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mudrcové	mudrc	k1gMnPc1	mudrc
nevrátili	vrátit	k5eNaPmAgMnP	vrátit
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c6	o
novém	nový	k2eAgMnSc6d1	nový
židovském	židovský	k2eAgMnSc6d1	židovský
králi	král	k1gMnSc6	král
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
příkaz	příkaz	k1gInSc1	příkaz
povraždit	povraždit	k5eAaPmF	povraždit
všechny	všechen	k3xTgFnPc4	všechen
děti	dítě	k1gFnPc4	dítě
mladší	mladý	k2eAgFnSc3d2	mladší
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
Bible	bible	k1gFnSc2	bible
bývá	bývat	k5eAaImIp3nS	bývat
během	během	k7c2	během
Vánoc	Vánoce	k1gFnPc2	Vánoce
připomínán	připomínán	k2eAgMnSc1d1	připomínán
a	a	k8xC	a
zobrazován	zobrazován	k2eAgInSc1d1	zobrazován
a	a	k8xC	a
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vánoční	vánoční	k2eAgInSc4d1	vánoční
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vánoce	Vánoce	k1gFnPc1	Vánoce
během	během	k7c2	během
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc4	Vánoce
křesťané	křesťan	k1gMnPc1	křesťan
začali	začít	k5eAaPmAgMnP	začít
slavit	slavit	k5eAaImF	slavit
v	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
až	až	k9	až
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
památkou	památka	k1gFnSc7	památka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	on	k3xPp3gInPc4	on
dosvědčuje	dosvědčovat	k5eAaImIp3nS	dosvědčovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Filokalův	Filokalův	k2eAgInSc1d1	Filokalův
chronograf	chronograf	k1gInSc1	chronograf
z	z	k7c2	z
roku	rok	k1gInSc2	rok
354	[number]	k4	354
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
slavení	slavení	k1gNnSc1	slavení
Vánoc	Vánoce	k1gFnPc2	Vánoce
zřejmě	zřejmě	k6eAd1	zřejmě
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
zavedené	zavedený	k2eAgFnPc4d1	zavedená
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
slavení	slavení	k1gNnSc2	slavení
svátku	svátek	k1gInSc2	svátek
bylo	být	k5eAaImAgNnS	být
krátce	krátce	k6eAd1	krátce
upuštěno	upustit	k5eAaPmNgNnS	upustit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Řehoř	Řehoř	k1gMnSc1	Řehoř
z	z	k7c2	z
Nazianzu	Nazianz	k1gInSc2	Nazianz
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
patriarchy	patriarcha	k1gMnSc2	patriarcha
konstantinopolského	konstantinopolský	k2eAgMnSc2d1	konstantinopolský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
381	[number]	k4	381
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
zavedeno	zavést	k5eAaPmNgNnS	zavést
Janem	Jan	k1gMnSc7	Jan
Zlatoústým	zlatoústý	k2eAgFnPc3d1	Zlatoústá
asi	asi	k9	asi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
byla	být	k5eAaImAgFnS	být
oslava	oslava	k1gFnSc1	oslava
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgFnSc1d1	páně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
méně	málo	k6eAd2	málo
významný	významný	k2eAgInSc4d1	významný
svátek	svátek	k1gInSc4	svátek
než	než	k8xS	než
Epifanie	Epifanie	k1gFnSc1	Epifanie
(	(	kIx(	(
<g/>
Zjevení	zjevení	k1gNnPc2	zjevení
Páně	páně	k2eAgMnPc2d1	páně
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
křesťanství	křesťanství	k1gNnSc6	křesťanství
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
.	.	kIx.	.
</s>
<s>
Významnost	významnost	k1gFnSc1	významnost
Štědrého	štědrý	k2eAgInSc2d1	štědrý
dne	den	k1gInSc2	den
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
zvyšována	zvyšovat	k5eAaImNgFnS	zvyšovat
po	po	k7c6	po
korunovaci	korunovace	k1gFnSc6	korunovace
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
na	na	k7c4	na
císaře	císař	k1gMnSc4	císař
na	na	k7c4	na
Narození	narození	k1gNnPc4	narození
Páně	páně	k2eAgNnSc2d1	páně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
800	[number]	k4	800
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
svátek	svátek	k1gInSc1	svátek
stal	stát	k5eAaPmAgInS	stát
tak	tak	k6eAd1	tak
významným	významný	k2eAgMnPc3d1	významný
<g/>
,	,	kIx,	,
že	že	k8xS	že
kronikáři	kronikář	k1gMnPc1	kronikář
běžně	běžně	k6eAd1	běžně
uváděli	uvádět	k5eAaImAgMnP	uvádět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
různí	různý	k2eAgMnPc1d1	různý
magnáti	magnát	k1gMnPc1	magnát
slavili	slavit	k5eAaImAgMnP	slavit
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Různí	různý	k2eAgMnPc1d1	různý
umělci	umělec	k1gMnPc1	umělec
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byli	být	k5eAaImAgMnP	být
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
za	za	k7c4	za
oplzlé	oplzlý	k2eAgFnPc4d1	oplzlá
koledy	koleda	k1gFnPc4	koleda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
divoké	divoký	k2eAgFnPc4d1	divoká
tradice	tradice	k1gFnPc4	tradice
Saturnálií	saturnálie	k1gFnPc2	saturnálie
a	a	k8xC	a
Yule	Yule	k1gNnSc1	Yule
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
nové	nový	k2eAgFnSc6d1	nová
formě	forma	k1gFnSc6	forma
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Opilost	opilost	k1gFnSc1	opilost
<g/>
,	,	kIx,	,
promiskuita	promiskuita	k1gFnSc1	promiskuita
a	a	k8xC	a
hazardní	hazardní	k2eAgFnPc1d1	hazardní
hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
také	také	k9	také
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
oslavy	oslava	k1gFnSc2	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
veřejné	veřejný	k2eAgInPc4d1	veřejný
svátky	svátek	k1gInPc4	svátek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ozdoby	ozdoba	k1gFnPc1	ozdoba
byly	být	k5eAaImAgFnP	být
používány	používán	k2eAgInPc1d1	používán
břečťan	břečťan	k1gInSc1	břečťan
<g/>
,	,	kIx,	,
cesmína	cesmína	k1gFnSc1	cesmína
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
stálezelené	stálezelený	k2eAgFnPc1d1	stálezelená
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
byl	být	k5eAaImAgInS	být
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
v	v	k7c6	v
období	období	k1gNnSc6	období
středověku	středověk	k1gInSc2	středověk
dodržován	dodržovat	k5eAaImNgInS	dodržovat
zvyk	zvyk	k1gInSc1	zvyk
bdění	bdění	k1gNnSc2	bdění
(	(	kIx(	(
<g/>
vigilie	vigilie	k1gFnSc1	vigilie
<g/>
)	)	kIx)	)
a	a	k8xC	a
půstu	půst	k1gInSc2	půst
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
Narození	narozený	k2eAgMnPc1d1	narozený
Páně	páně	k2eAgMnPc1d1	páně
kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
žáci	žák	k1gMnPc1	žák
chodili	chodit	k5eAaImAgMnP	chodit
a	a	k8xC	a
kalendovali	kalendovat	k5eAaPmAgMnP	kalendovat
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
koledovali	koledovat	k5eAaImAgMnP	koledovat
<g/>
,	,	kIx,	,
po	po	k7c6	po
domech	dům	k1gInPc6	dům
svých	svůj	k3xOyFgMnPc2	svůj
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činili	činit	k5eAaImAgMnP	činit
i	i	k8xC	i
v	v	k7c4	v
osmý	osmý	k4xOgInSc4	osmý
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
oktáv	oktáv	k1gInSc4	oktáv
<g/>
)	)	kIx)	)
téhož	týž	k3xTgInSc2	týž
svátku	svátek	k1gInSc2	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
popisováno	popisován	k2eAgNnSc1d1	popisováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
měly	mít	k5eAaImAgInP	mít
vánoční	vánoční	k2eAgInPc1d1	vánoční
dárky	dárek	k1gInPc1	dárek
charakter	charakter	k1gInSc4	charakter
úplatků	úplatek	k1gInPc2	úplatek
<g/>
.	.	kIx.	.
</s>
<s>
Poddaní	poddaný	k1gMnPc1	poddaný
dávali	dávat	k5eAaImAgMnP	dávat
dárky	dárek	k1gInPc4	dárek
vrchnosti	vrchnost	k1gFnSc2	vrchnost
a	a	k8xC	a
úředníkům	úředník	k1gMnPc3	úředník
<g/>
,	,	kIx,	,
podřízení	podřízení	k1gNnSc1	podřízení
nadřízeným	nadřízený	k1gMnPc3	nadřízený
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývaly	bývat	k5eAaImAgInP	bývat
dárky	dárek	k1gInPc1	dárek
vymáhány	vymáhán	k2eAgInPc1d1	vymáhán
i	i	k9	i
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Oslava	oslava	k1gFnSc1	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
se	se	k3xPyFc4	se
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
oslavováno	oslavován	k2eAgNnSc1d1	oslavováno
narození	narození	k1gNnSc1	narození
Ježíška	ježíšek	k1gInSc2	ježíšek
a	a	k8xC	a
uctíváno	uctíván	k2eAgNnSc1d1	uctíváno
rodinné	rodinný	k2eAgNnSc1d1	rodinné
štěstí	štěstí	k1gNnSc1	štěstí
a	a	k8xC	a
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
pospolitost	pospolitost	k1gFnSc1	pospolitost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
Vánocích	Vánoce	k1gFnPc6	Vánoce
vítal	vítat	k5eAaImAgInS	vítat
návrat	návrat	k1gInSc1	návrat
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
byly	být	k5eAaImAgInP	být
spojeny	spojit	k5eAaPmNgInP	spojit
i	i	k8xC	i
štědrovečerní	štědrovečerní	k2eAgInPc1d1	štědrovečerní
zvyky	zvyk	k1gInPc1	zvyk
a	a	k8xC	a
obyčeje	obyčej	k1gInPc1	obyčej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1644	[number]	k4	1644
<g/>
,	,	kIx,	,
během	během	k7c2	během
anglické	anglický	k2eAgFnSc2d1	anglická
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
angličtí	anglický	k2eAgMnPc1d1	anglický
puritáni	puritán	k1gMnPc1	puritán
ovládající	ovládající	k2eAgFnSc2d1	ovládající
Parlament	parlament	k1gInSc1	parlament
zakázali	zakázat	k5eAaPmAgMnP	zakázat
slavení	slavení	k1gNnSc3	slavení
Vánoc	Vánoce	k1gFnPc2	Vánoce
jako	jako	k8xS	jako
nebiblického	biblický	k2eNgInSc2d1	nebiblický
a	a	k8xC	a
pohanského	pohanský	k2eAgInSc2d1	pohanský
zvyku	zvyk	k1gInSc2	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
protesty	protest	k1gInPc4	protest
proti	proti	k7c3	proti
rušení	rušení	k1gNnSc3	rušení
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
,	,	kIx,	,
zákaz	zákaz	k1gInSc1	zákaz
však	však	k9	však
vydržel	vydržet	k5eAaPmAgInS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1660	[number]	k4	1660
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
zákazy	zákaz	k1gInPc1	zákaz
byly	být	k5eAaImAgInP	být
uplatněny	uplatnit	k5eAaPmNgInP	uplatnit
i	i	k9	i
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
anglických	anglický	k2eAgFnPc6d1	anglická
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
byl	být	k5eAaImAgInS	být
zákaz	zákaz	k1gInSc1	zákaz
slavení	slavení	k1gNnSc2	slavení
Vánoc	Vánoce	k1gFnPc2	Vánoce
zrušen	zrušen	k2eAgInSc1d1	zrušen
roku	rok	k1gInSc2	rok
1686	[number]	k4	1686
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
oficiálním	oficiální	k2eAgInSc7d1	oficiální
svátkem	svátek	k1gInSc7	svátek
se	se	k3xPyFc4	se
Vánoce	Vánoce	k1gFnPc1	Vánoce
staly	stát	k5eAaPmAgFnP	stát
až	až	k9	až
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Anglii	Anglie	k1gFnSc6	Anglie
byla	být	k5eAaImAgFnS	být
oslava	oslava	k1gFnSc1	oslava
zakázána	zakázat	k5eAaPmNgFnS	zakázat
například	například	k6eAd1	například
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1659	[number]	k4	1659
až	až	k9	až
1681	[number]	k4	1681
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
byli	být	k5eAaImAgMnP	být
známi	znám	k2eAgMnPc1d1	znám
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
nadšené	nadšený	k2eAgFnPc4d1	nadšená
oslavy	oslava	k1gFnPc4	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
obyvatelé	obyvatel	k1gMnPc1	obyvatel
měst	město	k1gNnPc2	město
Bethlehem	Bethleh	k1gInSc7	Bethleh
<g/>
,	,	kIx,	,
Nazareth	Nazareth	k1gMnSc1	Nazareth
a	a	k8xC	a
Lititz	Lititz	k1gMnSc1	Lititz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
města	město	k1gNnSc2	město
založili	založit	k5eAaPmAgMnP	založit
moravští	moravský	k2eAgMnPc1d1	moravský
osadníci	osadník	k1gMnPc1	osadník
německé	německý	k2eAgFnSc2d1	německá
i	i	k8xC	i
moravské	moravský	k2eAgFnSc2d1	Moravská
národnosti	národnost	k1gFnSc2	národnost
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
Moravských	moravský	k2eAgMnPc2d1	moravský
bratří	bratr	k1gMnPc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Moravané	Moravan	k1gMnPc1	Moravan
v	v	k7c6	v
Bethlehemu	Bethlehemo	k1gNnSc6	Bethlehemo
měli	mít	k5eAaImAgMnP	mít
první	první	k4xOgNnSc4	první
vánoční	vánoční	k2eAgInPc4d1	vánoční
stromky	stromek	k1gInPc7	stromek
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
první	první	k4xOgInPc4	první
betlémy	betlém	k1gInPc4	betlém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
hospodyňkám	hospodyňka	k1gFnPc3	hospodyňka
povoleno	povolit	k5eAaPmNgNnS	povolit
péct	péct	k5eAaImF	péct
vánočku	vánočka	k1gFnSc4	vánočka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
směli	smět	k5eAaImAgMnP	smět
péct	péct	k5eAaImF	péct
vánočku	vánočka	k1gFnSc4	vánočka
výhradně	výhradně	k6eAd1	výhradně
pekaři	pekař	k1gMnPc1	pekař
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
darů	dar	k1gInPc2	dar
pro	pro	k7c4	pro
hodnostáře	hodnostář	k1gMnPc4	hodnostář
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
obdarovávat	obdarovávat	k5eAaImF	obdarovávat
chudé	chudý	k2eAgMnPc4d1	chudý
a	a	k8xC	a
koledníky	koledník	k1gMnPc4	koledník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
vystavování	vystavování	k1gNnSc1	vystavování
jeslí	jesle	k1gFnPc2	jesle
považováno	považován	k2eAgNnSc1d1	považováno
leckde	leckde	k6eAd1	leckde
za	za	k7c4	za
nedůstojné	důstojný	k2eNgNnSc4d1	nedůstojné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jesle	jesle	k1gFnPc4	jesle
odváděly	odvádět	k5eAaImAgInP	odvádět
pozornost	pozornost	k1gFnSc4	pozornost
mládeže	mládež	k1gFnSc2	mládež
od	od	k7c2	od
kázání	kázání	k1gNnSc2	kázání
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nejprve	nejprve	k6eAd1	nejprve
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
vydali	vydat	k5eAaPmAgMnP	vydat
zákaz	zákaz	k1gInSc4	zákaz
stavění	stavění	k1gNnSc2	stavění
jesliček	jesličky	k1gFnPc2	jesličky
v	v	k7c6	v
kostelích	kostel	k1gInPc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
však	však	k9	však
hledali	hledat	k5eAaImAgMnP	hledat
náhradu	náhrada	k1gFnSc4	náhrada
a	a	k8xC	a
stavěli	stavět	k5eAaImAgMnP	stavět
jesličky	jesličky	k1gFnPc4	jesličky
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
domácích	domácí	k2eAgInPc2d1	domácí
betlémů	betlém	k1gInPc2	betlém
a	a	k8xC	a
jesliček	jesličky	k1gFnPc2	jesličky
se	se	k3xPyFc4	se
masivně	masivně	k6eAd1	masivně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
především	především	k6eAd1	především
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
stavění	stavění	k1gNnSc2	stavění
jesliček	jesličky	k1gFnPc2	jesličky
v	v	k7c6	v
kostelích	kostel	k1gInPc6	kostel
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zrušen	zrušit	k5eAaPmNgInS	zrušit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
spisovatelé	spisovatel	k1gMnPc1	spisovatel
ukazovali	ukazovat	k5eAaImAgMnP	ukazovat
tudorovské	tudorovský	k2eAgFnPc4d1	Tudorovská
Vánoce	Vánoce	k1gFnPc4	Vánoce
jako	jako	k8xS	jako
čas	čas	k1gInSc4	čas
nevázaných	vázaný	k2eNgFnPc2d1	nevázaná
oslav	oslava	k1gFnPc2	oslava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
však	však	k9	však
napsal	napsat	k5eAaPmAgMnS	napsat
Charles	Charles	k1gMnSc1	Charles
Dickens	Dickensa	k1gFnPc2	Dickensa
román	román	k1gInSc1	román
A	a	k8xC	a
Christmas	Christmas	k1gInSc1	Christmas
Carol	Carola	k1gFnPc2	Carola
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
koleda	koleda	k1gFnSc1	koleda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pomohl	pomoct	k5eAaPmAgMnS	pomoct
oživit	oživit	k5eAaPmF	oživit
"	"	kIx"	"
<g/>
duch	duch	k1gMnSc1	duch
<g/>
"	"	kIx"	"
vánočního	vánoční	k2eAgMnSc2d1	vánoční
a	a	k8xC	a
sezónního	sezónní	k2eAgNnSc2d1	sezónní
veselí	veselí	k1gNnSc2	veselí
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
popularita	popularita	k1gFnSc1	popularita
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
sehrála	sehrát	k5eAaPmAgFnS	sehrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
v	v	k7c6	v
představení	představení	k1gNnSc6	představení
Vánoc	Vánoce	k1gFnPc2	Vánoce
jakožto	jakožto	k8xS	jakožto
svátku	svátek	k1gInSc2	svátek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
dobrou	dobrý	k2eAgFnSc4d1	dobrá
vůli	vůle	k1gFnSc4	vůle
a	a	k8xC	a
soucit	soucit	k1gInSc4	soucit
<g/>
.	.	kIx.	.
</s>
<s>
Překrytím	překrytí	k1gNnSc7	překrytí
církevních	církevní	k2eAgFnPc2d1	církevní
oslav	oslava	k1gFnPc2	oslava
svou	svůj	k3xOyFgFnSc7	svůj
světskou	světský	k2eAgFnSc7d1	světská
vizí	vize	k1gFnSc7	vize
svátků	svátek	k1gInPc2	svátek
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
Dickens	Dickens	k1gInSc1	Dickens
mnoho	mnoho	k4c1	mnoho
aspektů	aspekt	k1gInPc2	aspekt
Vánoc	Vánoce	k1gFnPc2	Vánoce
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
rodinné	rodinný	k2eAgFnPc4d1	rodinná
oslavy	oslava	k1gFnPc4	oslava
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgInPc4d1	tradiční
pokrmy	pokrm	k1gInPc4	pokrm
a	a	k8xC	a
pití	pití	k1gNnSc4	pití
<g/>
,	,	kIx,	,
tanec	tanec	k1gInSc1	tanec
<g/>
,	,	kIx,	,
hry	hra	k1gFnPc1	hra
a	a	k8xC	a
pojetí	pojetí	k1gNnSc1	pojetí
Vánoc	Vánoce	k1gFnPc2	Vánoce
jako	jako	k8xC	jako
svátků	svátek	k1gInPc2	svátek
štědrosti	štědrost	k1gFnSc3	štědrost
<g/>
,	,	kIx,	,
velkorysosti	velkorysost	k1gFnPc1	velkorysost
ducha	duch	k1gMnSc2	duch
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
zdobení	zdobení	k1gNnSc2	zdobení
vánočního	vánoční	k2eAgInSc2d1	vánoční
stromku	stromek	k1gInSc2	stromek
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
ozdobeném	ozdobený	k2eAgInSc6d1	ozdobený
osvětleném	osvětlený	k2eAgInSc6d1	osvětlený
stromku	stromek	k1gInSc6	stromek
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
brémské	brémský	k2eAgFnSc6d1	brémská
kronice	kronika	k1gFnSc6	kronika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1570	[number]	k4	1570
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
stromeček	stromeček	k1gInSc4	stromeček
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
postavil	postavit	k5eAaPmAgMnS	postavit
a	a	k8xC	a
ozdobil	ozdobit	k5eAaPmAgMnS	ozdobit
režisér	režisér	k1gMnSc1	režisér
pražského	pražský	k2eAgNnSc2d1	Pražské
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
Leibich	Leibich	k1gMnSc1	Leibich
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
r.	r.	kA	r.
1812	[number]	k4	1812
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ozdoby	ozdoba	k1gFnSc2	ozdoba
stromečku	stromeček	k1gInSc2	stromeček
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
nejen	nejen	k6eAd1	nejen
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
a	a	k8xC	a
voskové	voskový	k2eAgFnPc1d1	vosková
<g/>
,	,	kIx,	,
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
<g/>
,	,	kIx,	,
papírové	papírový	k2eAgFnPc1d1	papírová
nebo	nebo	k8xC	nebo
kovové	kovový	k2eAgFnPc1d1	kovová
ozdoby	ozdoba	k1gFnPc1	ozdoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pečivo	pečivo	k1gNnSc4	pečivo
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
stromeček	stromeček	k1gInSc1	stromeček
zdoben	zdobit	k5eAaImNgInS	zdobit
též	též	k9	též
lojovými	lojový	k2eAgFnPc7d1	lojová
svíčkami	svíčka	k1gFnPc7	svíčka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
vynálezu	vynález	k1gInSc6	vynález
parafínu	parafín	k1gInSc2	parafín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
použití	použití	k1gNnSc1	použití
na	na	k7c4	na
svíčky	svíčka	k1gFnPc4	svíčka
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
parafinové	parafinový	k2eAgFnPc1d1	parafinová
svíčky	svíčka	k1gFnPc1	svíčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
uchytil	uchytit	k5eAaPmAgInS	uchytit
způsob	způsob	k1gInSc1	způsob
kuchyňské	kuchyňský	k2eAgFnSc2d1	kuchyňská
přípravy	příprava	k1gFnSc2	příprava
kapra	kapr	k1gMnSc2	kapr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
dostal	dostat	k5eAaPmAgMnS	dostat
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
v	v	k7c6	v
období	období	k1gNnSc6	období
První	první	k4xOgFnPc1	první
republiky	republika	k1gFnPc1	republika
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
nedostatkovým	dostatkový	k2eNgNnSc7d1	nedostatkové
zbožím	zboží	k1gNnSc7	zboží
nejen	nejen	k6eAd1	nejen
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
i	i	k9	i
kvasnice	kvasnice	k1gFnPc4	kvasnice
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
mnoha	mnoho	k4c2	mnoho
potravin	potravina	k1gFnPc2	potravina
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
jen	jen	k9	jen
náhražky	náhražka	k1gFnPc1	náhražka
<g/>
.	.	kIx.	.
</s>
<s>
Společné	společný	k2eAgNnSc1d1	společné
slavení	slavení	k1gNnSc1	slavení
Štědrého	štědrý	k2eAgInSc2d1	štědrý
večera	večer	k1gInSc2	večer
znepřátelenými	znepřátelený	k2eAgMnPc7d1	znepřátelený
vojáky	voják	k1gMnPc7	voják
je	být	k5eAaImIp3nS	být
popisováno	popisován	k2eAgNnSc1d1	popisováno
jak	jak	k8xC	jak
během	během	k7c2	během
první	první	k4xOgFnSc2	první
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
tradice	tradice	k1gFnPc1	tradice
Vánoc	Vánoce	k1gFnPc2	Vánoce
se	se	k3xPyFc4	se
nacisté	nacista	k1gMnPc1	nacista
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
změnit	změnit	k5eAaPmF	změnit
na	na	k7c6	na
starogermánské	starogermánský	k2eAgFnSc6d1	starogermánská
pohanské	pohanský	k2eAgFnSc6d1	pohanská
"	"	kIx"	"
<g/>
slavnosti	slavnost	k1gFnSc6	slavnost
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
svatý	svatý	k2eAgInSc1d1	svatý
svátek	svátek	k1gInSc1	svátek
Němců	Němec	k1gMnPc2	Němec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
školách	škola	k1gFnPc6	škola
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
slavit	slavit	k5eAaImF	slavit
narození	narození	k1gNnSc4	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
nesměly	smět	k5eNaImAgFnP	smět
se	se	k3xPyFc4	se
zpívat	zpívat	k5eAaImF	zpívat
vánoční	vánoční	k2eAgFnPc4d1	vánoční
koledy	koleda	k1gFnPc4	koleda
s	s	k7c7	s
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
komunistická	komunistický	k2eAgFnSc1d1	komunistická
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
nacisté	nacista	k1gMnPc1	nacista
snažila	snažil	k1gMnSc4	snažil
co	co	k3yRnSc4	co
nejvíce	hodně	k6eAd3	hodně
potlačit	potlačit	k5eAaPmF	potlačit
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
podstatu	podstata	k1gFnSc4	podstata
Vánoc	Vánoce	k1gFnPc2	Vánoce
ve	v	k7c6	v
veřejném	veřejný	k2eAgInSc6d1	veřejný
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Veřejných	veřejný	k2eAgFnPc2d1	veřejná
oslav	oslava	k1gFnPc2	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
jako	jako	k8xC	jako
narození	narození	k1gNnSc2	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
se	se	k3xPyFc4	se
československý	československý	k2eAgMnSc1d1	československý
občan	občan	k1gMnSc1	občan
dočkal	dočkat	k5eAaPmAgMnS	dočkat
znovu	znovu	k6eAd1	znovu
na	na	k7c6	na
Vánoce	Vánoce	k1gFnPc1	Vánoce
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
liturgie	liturgie	k1gFnSc1	liturgie
a	a	k8xC	a
Slavnost	slavnost	k1gFnSc1	slavnost
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgFnSc1d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
pro	pro	k7c4	pro
katolickou	katolický	k2eAgFnSc4d1	katolická
církev	církev	k1gFnSc4	církev
trvají	trvat	k5eAaImIp3nP	trvat
od	od	k7c2	od
prvních	první	k4xOgFnPc2	první
nešpor	nešpora	k1gFnPc2	nešpora
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgNnSc2d1	páně
do	do	k7c2	do
neděle	neděle	k1gFnSc2	neděle
po	po	k7c4	po
Zjevení	zjevení	k1gNnSc4	zjevení
Páně	páně	k2eAgNnSc2d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
římské	římský	k2eAgFnSc2d1	římská
tradice	tradice	k1gFnSc2	tradice
lze	lze	k6eAd1	lze
slavit	slavit	k5eAaImF	slavit
v	v	k7c4	v
den	den	k1gInSc4	den
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgFnSc4d1	páně
mši	mše	k1gFnSc4	mše
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
za	za	k7c4	za
svítání	svítání	k1gNnSc4	svítání
a	a	k8xC	a
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
už	už	k6eAd1	už
za	za	k7c4	za
Řehoře	Řehoř	k1gMnSc4	Řehoř
Velikého	veliký	k2eAgMnSc4d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Mešní	mešní	k2eAgInPc1d1	mešní
texty	text	k1gInPc1	text
z	z	k7c2	z
vigilie	vigilie	k1gFnSc2	vigilie
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgFnSc1d1	páně
jsou	být	k5eAaImIp3nP	být
použity	použit	k2eAgInPc1d1	použit
pro	pro	k7c4	pro
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
buď	buď	k8xC	buď
před	před	k7c7	před
I.	I.	kA	I.
nešporami	nešpora	k1gFnPc7	nešpora
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mše	mše	k1gFnPc4	mše
o	o	k7c6	o
slavnosti	slavnost	k1gFnSc6	slavnost
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgFnSc1d1	páně
jsou	být	k5eAaImIp3nP	být
čtení	čtení	k1gNnSc2	čtení
vybrána	vybrat	k5eAaPmNgFnS	vybrat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
římské	římský	k2eAgFnSc3d1	římská
tradici	tradice	k1gFnSc3	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
evangelické	evangelický	k2eAgNnSc4d1	evangelické
církve	církev	k1gFnSc2	církev
Vánoce	Vánoce	k1gFnPc1	Vánoce
začínají	začínat	k5eAaImIp3nP	začínat
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Liturgické	liturgický	k2eAgInPc1d1	liturgický
texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
Starozákonních	starozákonní	k2eAgNnPc2d1	starozákonní
proroctví	proroctví	k1gNnPc2	proroctví
a	a	k8xC	a
Lukášova	Lukášův	k2eAgNnPc4d1	Lukášovo
evangelia	evangelium	k1gNnPc4	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Episkopální	episkopální	k2eAgFnSc1d1	episkopální
církev	církev	k1gFnSc1	církev
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
Evangelická	evangelický	k2eAgFnSc1d1	evangelická
církev	církev	k1gFnSc1	církev
metodistická	metodistický	k2eAgFnSc1d1	metodistická
a	a	k8xC	a
luteránské	luteránský	k2eAgFnPc1d1	luteránská
církve	církev	k1gFnPc1	církev
používají	používat	k5eAaImIp3nP	používat
společný	společný	k2eAgInSc4d1	společný
lekcionář	lekcionář	k1gInSc4	lekcionář
<g/>
.	.	kIx.	.
</s>
<s>
Anglikánské	anglikánský	k2eAgFnPc1d1	anglikánská
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
blízké	blízký	k2eAgFnSc3d1	blízká
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
slaví	slavit	k5eAaImIp3nP	slavit
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
jsou	být	k5eAaImIp3nP	být
slaveny	slaven	k2eAgFnPc1d1	slavena
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pestrost	pestrost	k1gFnSc1	pestrost
vánočních	vánoční	k2eAgInPc2d1	vánoční
zvyků	zvyk	k1gInPc2	zvyk
odráží	odrážet	k5eAaImIp3nS	odrážet
místní	místní	k2eAgFnPc4d1	místní
tradice	tradice	k1gFnPc4	tradice
a	a	k8xC	a
často	často	k6eAd1	často
začleňuje	začleňovat	k5eAaImIp3nS	začleňovat
pohanské	pohanský	k2eAgInPc4d1	pohanský
zvyky	zvyk	k1gInPc4	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Oslavy	oslava	k1gFnPc1	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
byly	být	k5eAaImAgFnP	být
církvemi	církev	k1gFnPc7	církev
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
temperament	temperament	k1gInSc4	temperament
obyvatel	obyvatel	k1gMnPc2	obyvatel
dané	daný	k2eAgFnSc2d1	daná
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
křesťanství	křesťanství	k1gNnSc1	křesťanství
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
podstata	podstata	k1gFnSc1	podstata
srozumitelnější	srozumitelný	k2eAgFnSc1d2	srozumitelnější
a	a	k8xC	a
bližší	bližší	k1gNnSc1	bližší
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
Vánoce	Vánoce	k1gFnPc1	Vánoce
nejsou	být	k5eNaImIp3nP	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
svátkem	svátek	k1gInSc7	svátek
patří	patřit	k5eAaImIp3nS	patřit
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
<g/>
,	,	kIx,	,
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
<g/>
,	,	kIx,	,
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
,	,	kIx,	,
Bahrajn	Bahrajn	k1gInSc1	Bahrajn
<g/>
,	,	kIx,	,
Bhútán	Bhútán	k1gInSc1	Bhútán
<g/>
,	,	kIx,	,
Kambodža	Kambodža	k1gFnSc1	Kambodža
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Hongkongu	Hongkong	k1gInSc2	Hongkong
a	a	k8xC	a
Macaa	Macao	k1gNnSc2	Macao
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
<g/>
,	,	kIx,	,
Laos	Laos	k1gInSc1	Laos
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
<g/>
,	,	kIx,	,
Maledivy	Maledivy	k1gFnPc1	Maledivy
<g/>
,	,	kIx,	,
Mauritánie	Mauritánie	k1gFnPc1	Mauritánie
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Omán	Omán	k1gInSc1	Omán
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Katar	katar	k1gInSc1	katar
<g/>
,	,	kIx,	,
Saharská	saharský	k2eAgFnSc1d1	saharská
arabská	arabský	k2eAgFnSc1d1	arabská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
Somálsko	Somálsko	k1gNnSc1	Somálsko
<g/>
,	,	kIx,	,
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
Tunisko	Tunisko	k1gNnSc1	Tunisko
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
arabské	arabský	k2eAgInPc1d1	arabský
emiráty	emirát	k1gInPc1	emirát
<g/>
,	,	kIx,	,
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
a	a	k8xC	a
Jemen	Jemen	k1gInSc1	Jemen
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Velikonocemi	Velikonoce	k1gFnPc7	Velikonoce
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obdobím	období	k1gNnSc7	období
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
návštěvností	návštěvnost	k1gFnSc7	návštěvnost
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
katolických	katolický	k2eAgFnPc6d1	katolická
zemích	zem	k1gFnPc6	zem
lidé	člověk	k1gMnPc1	člověk
organizují	organizovat	k5eAaBmIp3nP	organizovat
náboženská	náboženský	k2eAgNnPc4d1	náboženské
procesí	procesí	k1gNnPc4	procesí
ve	v	k7c6	v
dnech	den	k1gInPc6	den
předcházejících	předcházející	k2eAgFnPc2d1	předcházející
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Dárky	dárek	k1gInPc1	dárek
jsou	být	k5eAaImIp3nP	být
předávány	předávat	k5eAaImNgInP	předávat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
slaveny	slaven	k2eAgFnPc1d1	slavena
Vánoce	Vánoce	k1gFnPc1	Vánoce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
slavena	slavit	k5eAaImNgFnS	slavit
také	také	k9	také
afroamerická	afroamerický	k2eAgFnSc1d1	afroamerická
Kwanzaa	Kwanzaa	k1gFnSc1	Kwanzaa
a	a	k8xC	a
židovská	židovský	k2eAgFnSc1d1	židovská
Chanuka	chanuka	k1gFnSc1	chanuka
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vnímají	vnímat	k5eAaImIp3nP	vnímat
vánoční	vánoční	k2eAgInPc1d1	vánoční
svátky	svátek	k1gInPc1	svátek
jako	jako	k8xS	jako
druhý	druhý	k4xOgInSc1	druhý
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
<g/>
,	,	kIx,	,
a	a	k8xC	a
Vánoce	Vánoce	k1gFnPc1	Vánoce
slaví	slavit	k5eAaImIp3nP	slavit
jakožto	jakožto	k8xS	jakožto
svátek	svátek	k1gInSc4	svátek
narození	narození	k1gNnSc2	narození
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
jsou	být	k5eAaImIp3nP	být
Vánoce	Vánoce	k1gFnPc1	Vánoce
slaveny	slaven	k2eAgFnPc1d1	slavena
i	i	k9	i
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgFnPc1d1	celá
Vánoce	Vánoce	k1gFnPc1	Vánoce
se	se	k3xPyFc4	se
berou	brát	k5eAaImIp3nP	brát
u	u	k7c2	u
křesťanů	křesťan	k1gMnPc2	křesťan
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
jako	jako	k9	jako
počátek	počátek	k1gInSc4	počátek
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
putování	putování	k1gNnSc2	putování
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
celému	celý	k2eAgInSc3d1	celý
vánočnímu	vánoční	k2eAgInSc3d1	vánoční
času	čas	k1gInSc3	čas
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
advent	advent	k1gInSc4	advent
a	a	k8xC	a
vánoční	vánoční	k2eAgInPc4d1	vánoční
svátky	svátek	k1gInPc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnPc1d1	vlastní
Vánoce	Vánoce	k1gFnPc1	Vánoce
jsou	být	k5eAaImIp3nP	být
chápány	chápat	k5eAaImNgFnP	chápat
jako	jako	k9	jako
oslavy	oslava	k1gFnPc1	oslava
začínající	začínající	k2eAgFnSc1d1	začínající
v	v	k7c6	v
ČR	ČR	kA	ČR
Štědrým	štědrý	k2eAgInSc7d1	štědrý
večerem	večer	k1gInSc7	večer
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Vánoce	Vánoce	k1gFnPc1	Vánoce
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
začínají	začínat	k5eAaImIp3nP	začínat
27	[number]	k4	27
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
podle	podle	k7c2	podle
prvního	první	k4xOgInSc2	první
adventu	advent	k1gInSc2	advent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
prosinec	prosinec	k1gInSc4	prosinec
byl	být	k5eAaImAgInS	být
také	také	k9	také
zařazen	zařazen	k2eAgInSc1d1	zařazen
svátek	svátek	k1gInSc1	svátek
některých	některý	k3yIgMnPc2	některý
významných	významný	k2eAgMnPc2d1	významný
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
světců	světec	k1gMnPc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
adventu	advent	k1gInSc2	advent
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
svátek	svátek	k1gInSc4	svátek
někteří	některý	k3yIgMnPc1	některý
důležití	důležitý	k2eAgMnPc1d1	důležitý
svatí	svatý	k1gMnPc1	svatý
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sv.	sv.	kA	sv.
Ondřej	Ondřej	k1gMnSc1	Ondřej
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Barbora	Barbora	k1gFnSc1	Barbora
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
<g/>
)	)	kIx)	)
a	a	k8xC	a
sv.	sv.	kA	sv.
Lucie	Lucie	k1gFnSc1	Lucie
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvedená	uvedený	k2eAgNnPc1d1	uvedené
data	datum	k1gNnPc1	datum
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
některé	některý	k3yIgFnPc4	některý
významnější	významný	k2eAgFnPc4d2	významnější
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
slavené	slavený	k2eAgInPc4d1	slavený
svátky	svátek	k1gInPc4	svátek
ve	v	k7c6	v
vánočním	vánoční	k2eAgNnSc6d1	vánoční
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Advent	advent	k1gInSc1	advent
<g/>
.	.	kIx.	.
</s>
<s>
Přípravu	příprava	k1gFnSc4	příprava
na	na	k7c4	na
samotné	samotný	k2eAgFnPc4d1	samotná
Vánoce	Vánoce	k1gFnPc4	Vánoce
představuje	představovat	k5eAaImIp3nS	představovat
advent	advent	k1gInSc1	advent
<g/>
,	,	kIx,	,
období	období	k1gNnSc4	období
čtyř	čtyři	k4xCgInPc2	čtyři
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
Vánoci	Vánoce	k1gFnPc7	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
adventní	adventní	k2eAgFnSc1d1	adventní
neděle	neděle	k1gFnSc1	neděle
(	(	kIx(	(
<g/>
připadající	připadající	k2eAgFnSc1d1	připadající
na	na	k7c4	na
neděli	neděle	k1gFnSc4	neděle
mezi	mezi	k7c7	mezi
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadem	listopad	k1gInSc7	listopad
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
počátek	počátek	k1gInSc4	počátek
církevního	církevní	k2eAgNnSc2d1	církevní
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
liturgického	liturgický	k2eAgInSc2d1	liturgický
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
adventní	adventní	k2eAgFnPc1d1	adventní
neděle	neděle	k1gFnPc1	neděle
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
označovány	označovat	k5eAaImNgInP	označovat
postupně	postupně	k6eAd1	postupně
jako	jako	k8xC	jako
železná	železný	k2eAgFnSc1d1	železná
<g/>
,	,	kIx,	,
bronzová	bronzový	k2eAgFnSc1d1	bronzová
<g/>
,	,	kIx,	,
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
a	a	k8xC	a
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uvedeným	uvedený	k2eAgInPc3d1	uvedený
svátkům	svátek	k1gInPc3	svátek
se	se	k3xPyFc4	se
také	také	k9	také
vážou	vázat	k5eAaImIp3nP	vázat
lidové	lidový	k2eAgFnPc1d1	lidová
nebo	nebo	k8xC	nebo
církevní	církevní	k2eAgFnPc1d1	církevní
tradice	tradice	k1gFnPc1	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
svátek	svátek	k1gInSc1	svátek
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
je	být	k5eAaImIp3nS	být
uctíván	uctívat	k5eAaImNgInS	uctívat
již	již	k6eAd1	již
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
Mikuláš	mikuláš	k1gInSc1	mikuláš
byl	být	k5eAaImAgInS	být
patronem	patron	k1gInSc7	patron
námořníků	námořník	k1gMnPc2	námořník
<g/>
,	,	kIx,	,
lékárníků	lékárník	k1gMnPc2	lékárník
<g/>
,	,	kIx,	,
pekařů	pekař	k1gMnPc2	pekař
<g/>
,	,	kIx,	,
rybářů	rybář	k1gMnPc2	rybář
a	a	k8xC	a
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Oslavy	oslava	k1gFnPc4	oslava
svátku	svátek	k1gInSc2	svátek
svaté	svatý	k2eAgFnSc2d1	svatá
Lucie	Lucie	k1gFnSc2	Lucie
v	v	k7c6	v
severských	severský	k2eAgFnPc6d1	severská
zemích	zem	k1gFnPc6	zem
vychází	vycházet	k5eAaImIp3nS	vycházet
zřejmě	zřejmě	k6eAd1	zřejmě
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
pohanských	pohanský	k2eAgInPc2d1	pohanský
zvyků	zvyk	k1gInPc2	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
významnějších	významný	k2eAgInPc2d2	významnější
svátků	svátek	k1gInPc2	svátek
<g/>
:	:	kIx,	:
sv.	sv.	kA	sv.
Ondřej	Ondřej	k1gMnSc1	Ondřej
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
Barbora	Barbora	k1gFnSc1	Barbora
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
Mikuláš	mikuláš	k1gInSc1	mikuláš
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
Ambrož	Ambrož	k1gMnSc1	Ambrož
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Slavnost	slavnost	k1gFnSc1	slavnost
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
počaté	počatý	k2eAgFnSc2d1	počatá
bez	bez	k7c2	bez
poskvrny	poskvrna	k1gFnSc2	poskvrna
prvotního	prvotní	k2eAgInSc2d1	prvotní
hříchu	hřích	k1gInSc2	hřích
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
Lucie	Lucie	k1gFnSc1	Lucie
(	(	kIx(	(
<g/>
Světluše	světluše	k1gFnSc1	světluše
<g/>
)	)	kIx)	)
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgFnPc1d1	samotná
Vánoce	Vánoce	k1gFnPc1	Vánoce
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
začínají	začínat	k5eAaImIp3nP	začínat
Štědrým	štědrý	k2eAgInSc7d1	štědrý
večerem	večer	k1gInSc7	večer
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
roku	rok	k1gInSc2	rok
567	[number]	k4	567
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
první	první	k4xOgInPc4	první
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
svatých	svatý	k2eAgFnPc2d1	svatá
nocí	noc	k1gFnPc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nový	nový	k2eAgInSc4d1	nový
den	den	k1gInSc4	den
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
začínal	začínat	k5eAaImAgMnS	začínat
nikoli	nikoli	k9	nikoli
půlnocí	půlnoc	k1gFnSc7	půlnoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
večer	večer	k6eAd1	večer
východem	východ	k1gInSc7	východ
první	první	k4xOgFnSc2	první
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Štědrý	štědrý	k2eAgInSc1d1	štědrý
večer	večer	k1gInSc1	večer
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
svátku	svátek	k1gInSc2	svátek
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgNnSc2d1	páně
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Štědrý	štědrý	k2eAgInSc1d1	štědrý
den	den	k1gInSc1	den
ale	ale	k8xC	ale
podle	podle	k7c2	podle
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
ještě	ještě	k9	ještě
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
adventu	advent	k1gInSc2	advent
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
vrcholí	vrcholí	k1gNnSc6	vrcholí
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
dnem	dno	k1gNnSc7	dno
nařízeného	nařízený	k2eAgInSc2d1	nařízený
přísného	přísný	k2eAgInSc2d1	přísný
půstu	půst	k1gInSc2	půst
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
někteří	některý	k3yIgMnPc1	některý
katolíci	katolík	k1gMnPc1	katolík
dodnes	dodnes	k6eAd1	dodnes
dodržují	dodržovat	k5eAaImIp3nP	dodržovat
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
již	již	k9	již
to	ten	k3xDgNnSc4	ten
církev	církev	k1gFnSc1	církev
nestanoví	stanovit	k5eNaPmIp3nS	stanovit
jako	jako	k9	jako
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
řada	řada	k1gFnSc1	řada
zvyklostí	zvyklost	k1gFnPc2	zvyklost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
prasátku	prasátko	k1gNnSc6	prasátko
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
zpříjemnit	zpříjemnit	k5eAaPmF	zpříjemnit
půst	půst	k1gInSc4	půst
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
či	či	k8xC	či
postní	postní	k2eAgNnSc1d1	postní
složení	složení	k1gNnSc1	složení
štědrovečerní	štědrovečerní	k2eAgFnSc2d1	štědrovečerní
večeře	večeře	k1gFnSc2	večeře
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
po	po	k7c6	po
západu	západ	k1gInSc6	západ
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
objevení	objevení	k1gNnSc4	objevení
první	první	k4xOgFnSc2	první
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
)	)	kIx)	)
začínají	začínat	k5eAaImIp3nP	začínat
vánoční	vánoční	k2eAgFnPc1d1	vánoční
oslavy	oslava	k1gFnPc1	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Štědrovečerní	štědrovečerní	k2eAgFnSc1d1	štědrovečerní
večeře	večeře	k1gFnSc1	večeře
je	být	k5eAaImIp3nS	být
produktem	produkt	k1gInSc7	produkt
toho	ten	k3xDgMnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
podle	podle	k7c2	podle
liturgických	liturgický	k2eAgFnPc2d1	liturgická
zvyklostí	zvyklost	k1gFnPc2	zvyklost
přísluší	příslušet	k5eAaImIp3nS	příslušet
doba	doba	k1gFnSc1	doba
po	po	k7c6	po
západu	západ	k1gInSc6	západ
slunce	slunce	k1gNnSc2	slunce
už	už	k6eAd1	už
následujícímu	následující	k2eAgInSc3d1	následující
dni	den	k1gInSc3	den
(	(	kIx(	(
<g/>
po	po	k7c6	po
západu	západ	k1gInSc6	západ
slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
již	již	k6eAd1	již
možno	možno	k6eAd1	možno
oslavovat	oslavovat	k5eAaImF	oslavovat
zítřejší	zítřejší	k2eAgInSc4d1	zítřejší
svátek	svátek	k1gInSc4	svátek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
postní	postní	k2eAgNnPc1d1	postní
pravidla	pravidlo	k1gNnPc1	pravidlo
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
dodržovat	dodržovat	k5eAaImF	dodržovat
až	až	k9	až
do	do	k7c2	do
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
půlnoční	půlnoční	k1gFnSc1	půlnoční
mše	mše	k1gFnSc2	mše
je	být	k5eAaImIp3nS	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
noční	noční	k2eAgFnSc2d1	noční
vigilie	vigilie	k1gFnSc2	vigilie
<g/>
,	,	kIx,	,
bdění	bdění	k1gNnSc2	bdění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
původně	původně	k6eAd1	původně
předcházelo	předcházet	k5eAaImAgNnS	předcházet
všem	všecek	k3xTgInPc3	všecek
významným	významný	k2eAgInPc3d1	významný
křesťanským	křesťanský	k2eAgInPc3d1	křesťanský
svátkům	svátek	k1gInPc3	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgInPc1d1	samotný
vánoční	vánoční	k2eAgInPc1d1	vánoční
svátky	svátek	k1gInPc1	svátek
začínají	začínat	k5eAaImIp3nP	začínat
dnem	den	k1gInSc7	den
narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgNnSc2d1	páně
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Boží	boží	k2eAgInSc1d1	boží
hod	hod	k1gInSc1	hod
vánoční	vánoční	k2eAgInSc1d1	vánoční
<g/>
,	,	kIx,	,
v	v	k7c6	v
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
označený	označený	k2eAgInSc1d1	označený
jako	jako	k9	jako
"	"	kIx"	"
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
svátek	svátek	k1gInSc4	svátek
vánoční	vánoční	k2eAgInSc4d1	vánoční
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
svátek	svátek	k1gInSc1	svátek
sv.	sv.	kA	sv.
Štěpána	Štěpán	k1gMnSc2	Štěpán
prvomučedníka	prvomučedník	k1gMnSc2	prvomučedník
<g/>
,	,	kIx,	,
občanský	občanský	k2eAgMnSc1d1	občanský
"	"	kIx"	"
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
svátek	svátek	k1gInSc4	svátek
vánoční	vánoční	k2eAgInSc4d1	vánoční
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
šestém	šestý	k4xOgNnSc6	šestý
století	století	k1gNnSc6	století
ustanoven	ustanoven	k2eAgMnSc1d1	ustanoven
"	"	kIx"	"
<g/>
svatý	svatý	k2eAgInSc1d1	svatý
dvanáctidenní	dvanáctidenní	k2eAgInSc1d1	dvanáctidenní
čas	čas	k1gInSc1	čas
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
slavnost	slavnost	k1gFnSc1	slavnost
Zjevení	zjevení	k1gNnSc2	zjevení
Páně	páně	k2eAgNnSc2d1	páně
(	(	kIx(	(
<g/>
též	též	k9	též
svátek	svátek	k1gInSc1	svátek
Tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
<g/>
)	)	kIx)	)
–	–	k?	–
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
se	se	k3xPyFc4	se
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
přišli	přijít	k5eAaPmAgMnP	přijít
poklonit	poklonit	k5eAaPmF	poklonit
právě	právě	k6eAd1	právě
narozenému	narozený	k2eAgMnSc3d1	narozený
Ježíšovi	Ježíš	k1gMnSc3	Ježíš
do	do	k7c2	do
Betléma	Betlém	k1gInSc2	Betlém
mudrci	mudrc	k1gMnPc1	mudrc
od	od	k7c2	od
východu	východ	k1gInSc2	východ
a	a	k8xC	a
přinesli	přinést	k5eAaPmAgMnP	přinést
mu	on	k3xPp3gMnSc3	on
dary	dar	k1gInPc4	dar
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
později	pozdě	k6eAd2	pozdě
udělala	udělat	k5eAaPmAgFnS	udělat
z	z	k7c2	z
mudrců	mudrc	k1gMnPc2	mudrc
krále	král	k1gMnSc2	král
a	a	k8xC	a
stanovila	stanovit	k5eAaPmAgFnS	stanovit
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
na	na	k7c4	na
tři	tři	k4xCgMnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Neděli	neděle	k1gFnSc3	neděle
následující	následující	k2eAgFnSc3d1	následující
po	po	k7c6	po
slavnosti	slavnost	k1gFnSc6	slavnost
Zjevení	zjevení	k1gNnSc1	zjevení
Páně	páně	k2eAgMnSc1d1	páně
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
Křest	křest	k1gInSc1	křest
Páně	páně	k2eAgInSc1d1	páně
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vánoční	vánoční	k2eAgNnPc4d1	vánoční
období	období	k1gNnPc4	období
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
církevního	církevní	k2eAgInSc2d1	církevní
roku	rok	k1gInSc2	rok
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
křesťané	křesťan	k1gMnPc1	křesťan
však	však	k8xC	však
Vánoce	Vánoce	k1gFnPc1	Vánoce
slaví	slavit	k5eAaImIp3nS	slavit
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
amerických	americký	k2eAgFnPc6d1	americká
koloniích	kolonie	k1gFnPc6	kolonie
Vánoce	Vánoce	k1gFnPc1	Vánoce
kvůli	kvůli	k7c3	kvůli
možnému	možný	k2eAgInSc3d1	možný
pohanskému	pohanský	k2eAgInSc3d1	pohanský
původu	původ	k1gInSc3	původ
zakázány	zakázat	k5eAaPmNgInP	zakázat
<g/>
;	;	kIx,	;
podobně	podobně	k6eAd1	podobně
některé	některý	k3yIgFnPc1	některý
novodobé	novodobý	k2eAgFnPc1d1	novodobá
skupiny	skupina	k1gFnPc1	skupina
vycházející	vycházející	k2eAgFnPc1d1	vycházející
z	z	k7c2	z
křesťanství	křesťanství	k1gNnSc4	křesťanství
Vánoce	Vánoce	k1gFnPc1	Vánoce
neslaví	slavit	k5eNaImIp3nP	slavit
ani	ani	k8xC	ani
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
Bible	bible	k1gFnSc2	bible
totiž	totiž	k9	totiž
nelze	lze	k6eNd1	lze
slavení	slavení	k1gNnSc1	slavení
Vánoc	Vánoce	k1gFnPc2	Vánoce
doložit	doložit	k5eAaPmF	doložit
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
významnějších	významný	k2eAgInPc2d2	významnější
svátků	svátek	k1gInPc2	svátek
<g/>
:	:	kIx,	:
Slavnost	slavnost	k1gFnSc1	slavnost
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgFnSc2d1	páně
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
Štěpán	Štěpán	k1gMnSc1	Štěpán
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
svátek	svátek	k1gInSc1	svátek
Mláďátek	mláďátko	k1gNnPc2	mláďátko
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
Slavnost	slavnost	k1gFnSc1	slavnost
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
slavnost	slavnost	k1gFnSc1	slavnost
Zjevení	zjevení	k1gNnSc2	zjevení
Páně	páně	k2eAgNnSc2d1	páně
<g/>
,	,	kIx,	,
oslava	oslava	k1gFnSc1	oslava
Tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
svátek	svátek	k1gInSc1	svátek
Křtu	křest	k1gInSc2	křest
Páně	páně	k2eAgFnSc2d1	páně
–	–	k?	–
první	první	k4xOgFnSc2	první
neděle	neděle	k1gFnSc2	neděle
po	po	k7c4	po
6	[number]	k4	6
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Adventní	adventní	k2eAgFnSc1d1	adventní
a	a	k8xC	a
vánoční	vánoční	k2eAgFnSc1d1	vánoční
vazba	vazba	k1gFnSc1	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Adventní	adventní	k2eAgFnSc1d1	adventní
a	a	k8xC	a
vánoční	vánoční	k2eAgFnSc1d1	vánoční
vazba	vazba	k1gFnSc1	vazba
je	být	k5eAaImIp3nS	být
zdobena	zdoben	k2eAgFnSc1d1	zdobena
přízdobami	přízdoba	k1gFnPc7	přízdoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
nejen	nejen	k6eAd1	nejen
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Adventní	adventní	k2eAgFnSc1d1	adventní
a	a	k8xC	a
vánoční	vánoční	k2eAgFnSc1d1	vánoční
vazba	vazba	k1gFnSc1	vazba
také	také	k9	také
bývá	bývat	k5eAaImIp3nS	bývat
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
církevní	církevní	k2eAgFnSc2d1	církevní
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
ale	ale	k9	ale
především	především	k6eAd1	především
k	k	k7c3	k
dekoraci	dekorace	k1gFnSc3	dekorace
obydlí	obydlí	k1gNnSc2	obydlí
<g/>
,	,	kIx,	,
společenských	společenský	k2eAgFnPc2d1	společenská
prostor	prostora	k1gFnPc2	prostora
i	i	k8xC	i
veřejných	veřejný	k2eAgFnPc2d1	veřejná
prostranství	prostranství	k1gNnSc4	prostranství
během	během	k7c2	během
adventu	advent	k1gInSc2	advent
a	a	k8xC	a
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Jakékoliv	jakýkoliv	k3yIgFnPc1	jakýkoliv
vazby	vazba	k1gFnPc1	vazba
a	a	k8xC	a
aranžmá	aranžmá	k1gNnSc1	aranžmá
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
smuteční	smuteční	k2eAgFnSc2d1	smuteční
a	a	k8xC	a
dárkové	dárkový	k2eAgFnSc2d1	dárková
vazby	vazba	k1gFnSc2	vazba
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
ovlivněny	ovlivněn	k2eAgFnPc1d1	ovlivněna
tradicemi	tradice	k1gFnPc7	tradice
a	a	k8xC	a
atmosférou	atmosféra	k1gFnSc7	atmosféra
svátků	svátek	k1gInPc2	svátek
a	a	k8xC	a
zohledněno	zohledněn	k2eAgNnSc4d1	zohledněno
provedení	provedení	k1gNnSc4	provedení
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
uvedenými	uvedený	k2eAgInPc7d1	uvedený
svátečními	sváteční	k2eAgInPc7d1	sváteční
zvyky	zvyk	k1gInPc7	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
uvedených	uvedený	k2eAgFnPc2d1	uvedená
dekorací	dekorace	k1gFnPc2	dekorace
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
se	s	k7c7	s
zapálenými	zapálený	k2eAgFnPc7d1	zapálená
svíčkami	svíčka	k1gFnPc7	svíčka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bývá	bývat	k5eAaImIp3nS	bývat
zdrojem	zdroj	k1gInSc7	zdroj
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
rizik	riziko	k1gNnPc2	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
a	a	k8xC	a
adventní	adventní	k2eAgFnPc1d1	adventní
dekorace	dekorace	k1gFnPc1	dekorace
si	se	k3xPyFc3	se
v	v	k7c6	v
ČR	ČR	kA	ČR
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
často	často	k6eAd1	často
připravují	připravovat	k5eAaImIp3nP	připravovat
i	i	k9	i
sami	sám	k3xTgMnPc1	sám
uživatelé	uživatel	k1gMnPc1	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
dekorace	dekorace	k1gFnPc1	dekorace
mají	mít	k5eAaImIp3nP	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
bylo	být	k5eAaImAgNnS	být
vánočním	vánoční	k2eAgInSc7d1	vánoční
zvykem	zvyk	k1gInSc7	zvyk
každý	každý	k3xTgInSc4	každý
dům	dům	k1gInSc1	dům
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
farní	farní	k2eAgInPc4d1	farní
kostely	kostel	k1gInPc4	kostel
zdobit	zdobit	k5eAaImF	zdobit
cesmínou	cesmína	k1gFnSc7	cesmína
<g/>
,	,	kIx,	,
břečťanem	břečťan	k1gInSc7	břečťan
<g/>
,	,	kIx,	,
chvojím	chvojí	k1gNnSc7	chvojí
<g/>
,	,	kIx,	,
a	a	k8xC	a
stálezelenými	stálezelený	k2eAgFnPc7d1	stálezelená
dřevinami	dřevina	k1gFnPc7	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
srdčitých	srdčitý	k2eAgInPc2d1	srdčitý
listů	list	k1gInPc2	list
břečťanu	břečťan	k1gInSc2	břečťan
měl	mít	k5eAaImAgMnS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
příchod	příchod	k1gInSc4	příchod
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
cesmína	cesmína	k1gFnSc1	cesmína
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
ochranou	ochrana	k1gFnSc7	ochrana
proti	proti	k7c3	proti
pohanům	pohan	k1gMnPc3	pohan
a	a	k8xC	a
čarodějnicím	čarodějnice	k1gFnPc3	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
Trny	trn	k1gInPc1	trn
a	a	k8xC	a
červené	červený	k2eAgFnPc1d1	červená
bobule	bobule	k1gFnPc1	bobule
cesmíny	cesmína	k1gFnSc2	cesmína
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
trnovou	trnový	k2eAgFnSc4d1	Trnová
korunu	koruna	k1gFnSc4	koruna
ukřižovaného	ukřižovaný	k2eAgMnSc2d1	ukřižovaný
Spasitele	spasitel	k1gMnSc2	spasitel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Purpura	purpura	k1gFnSc1	purpura
<g/>
,	,	kIx,	,
kadidlo	kadidlo	k1gNnSc1	kadidlo
a	a	k8xC	a
františek	františek	k1gInSc1	františek
jsou	být	k5eAaImIp3nP	být
názvy	název	k1gInPc4	název
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
aromatické	aromatický	k2eAgFnPc4d1	aromatická
směsi	směs	k1gFnPc4	směs
nebo	nebo	k8xC	nebo
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	s	k7c7	s
pražením	pražení	k1gNnSc7	pražení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pomalým	pomalý	k2eAgNnSc7d1	pomalé
hořením	hoření	k1gNnSc7	hoření
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
plamenů	plamen	k1gInPc2	plamen
<g/>
)	)	kIx)	)
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
provonění	provonění	k1gNnSc3	provonění
příbytků	příbytek	k1gInPc2	příbytek
nebo	nebo	k8xC	nebo
kostelů	kostel	k1gInPc2	kostel
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
vonným	vonný	k2eAgInSc7d1	vonný
kouřem	kouř	k1gInSc7	kouř
<g/>
.	.	kIx.	.
</s>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
barev	barva	k1gFnPc2	barva
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
módou	móda	k1gFnSc7	móda
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
církevně	církevně	k6eAd1	církevně
dána	dát	k5eAaPmNgFnS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnPc1d1	tradiční
barvy	barva	k1gFnPc1	barva
vánočních	vánoční	k2eAgFnPc2d1	vánoční
ozdob	ozdoba	k1gFnPc2	ozdoba
jsou	být	k5eAaImIp3nP	být
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
a	a	k8xC	a
zlaté	zlatý	k2eAgFnPc1d1	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
trendy	trend	k1gInPc1	trend
vánočních	vánoční	k2eAgFnPc2d1	vánoční
dekorací	dekorace	k1gFnPc2	dekorace
každoročně	každoročně	k6eAd1	každoročně
určuje	určovat	k5eAaImIp3nS	určovat
Evropská	evropský	k2eAgFnSc1d1	Evropská
asociace	asociace	k1gFnSc1	asociace
floristů	florista	k1gMnPc2	florista
a	a	k8xC	a
výrobců	výrobce	k1gMnPc2	výrobce
dekorací	dekorace	k1gFnPc2	dekorace
(	(	kIx(	(
<g/>
EFSA	EFSA	kA	EFSA
–	–	k?	–
European	European	k1gMnSc1	European
Floral	Floral	k1gMnSc1	Floral
and	and	k?	and
Lifestyle	Lifestyl	k1gInSc5	Lifestyl
Product	Product	k1gInSc4	Product
Suppliers	Suppliers	k1gInSc1	Suppliers
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
pokyny	pokyn	k1gInPc1	pokyn
bezvýhradně	bezvýhradně	k6eAd1	bezvýhradně
obecně	obecně	k6eAd1	obecně
přijímány	přijímat	k5eAaImNgInP	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
Červená	Červená	k1gFnSc1	Červená
často	často	k6eAd1	často
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
oheň	oheň	k1gInSc4	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Vánoc	Vánoce	k1gFnPc2	Vánoce
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
krev	krev	k1gFnSc4	krev
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
prolita	prolít	k5eAaPmNgFnS	prolít
při	při	k7c6	při
jeho	jeho	k3xOp3gMnPc6	jeho
smrti	smrt	k1gFnSc6	smrt
ukřižováním	ukřižování	k1gNnSc7	ukřižování
za	za	k7c4	za
vykoupení	vykoupení	k1gNnSc4	vykoupení
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	Zelená	k1gFnSc1	Zelená
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
věčný	věčný	k2eAgInSc4d1	věčný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
od	od	k7c2	od
předkřesťanských	předkřesťanský	k2eAgFnPc2d1	předkřesťanská
dob	doba	k1gFnPc2	doba
používány	používat	k5eAaImNgFnP	používat
větve	větev	k1gFnPc1	větev
jehličnanů	jehličnan	k1gInPc2	jehličnan
a	a	k8xC	a
stálezelených	stálezelený	k2eAgFnPc2d1	stálezelená
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc4	ten
své	svůj	k3xOyFgInPc4	svůj
listy	list	k1gInPc4	list
(	(	kIx(	(
<g/>
jehličí	jehličí	k1gNnSc2	jehličí
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nejsou	být	k5eNaImIp3nP	být
opadané	opadaný	k2eAgFnPc1d1	opadaná
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
stále	stále	k6eAd1	stále
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
věčně	věčně	k6eAd1	věčně
<g/>
"	"	kIx"	"
živé	živý	k2eAgFnSc6d1	živá
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
už	už	k9	už
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
daného	daný	k2eAgNnSc2d1	dané
období	období	k1gNnSc2	období
tak	tak	k9	tak
je	on	k3xPp3gMnPc4	on
jí	jíst	k5eAaImIp3nS	jíst
popisována	popisován	k2eAgFnSc1d1	popisována
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
ušlechtilosti	ušlechtilost	k1gFnSc2	ušlechtilost
<g/>
,	,	kIx,	,
velkorysosti	velkorysost	k1gFnSc2	velkorysost
<g/>
,	,	kIx,	,
idealismu	idealismus	k1gInSc2	idealismus
<g/>
,	,	kIx,	,
vznešenosti	vznešenost	k1gFnSc2	vznešenost
<g/>
,	,	kIx,	,
moudrosti	moudrost	k1gFnSc2	moudrost
nebo	nebo	k8xC	nebo
prostě	prostě	k9	prostě
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
hojnosti	hojnost	k1gFnSc2	hojnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
spojenou	spojený	k2eAgFnSc7d1	spojená
s	s	k7c7	s
Vánocemi	Vánoce	k1gFnPc7	Vánoce
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc4	zlato
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
darů	dar	k1gInPc2	dar
Tří	tři	k4xCgNnPc2	tři
králů	král	k1gMnPc2	král
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
Ježíšův	Ježíšův	k2eAgInSc4d1	Ježíšův
královský	královský	k2eAgInSc4d1	královský
původ	původ	k1gInSc4	původ
(	(	kIx(	(
<g/>
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Davidova	Davidův	k2eAgInSc2d1	Davidův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
potvrzení	potvrzení	k1gNnSc4	potvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
za	za	k7c4	za
Božího	boží	k2eAgMnSc4d1	boží
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
během	během	k7c2	během
Vánoc	Vánoce	k1gFnPc2	Vánoce
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
nevinnosti	nevinnost	k1gFnSc2	nevinnost
<g/>
,	,	kIx,	,
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
čistoty	čistota	k1gFnSc2	čistota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
adventním	adventní	k2eAgInSc6d1	adventní
věnci	věnec	k1gInSc6	věnec
je	být	k5eAaImIp3nS	být
svíčka	svíčka	k1gFnSc1	svíčka
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
symbolem	symbol	k1gInSc7	symbol
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
nebo	nebo	k8xC	nebo
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
a	a	k8xC	a
Panna	Panna	k1gFnSc1	Panna
Marie	Marie	k1gFnSc1	Marie
jsou	být	k5eAaImIp3nP	být
dávány	dáván	k2eAgInPc1d1	dáván
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
panenstvím	panenství	k1gNnSc7	panenství
a	a	k8xC	a
nevinností	nevinnost	k1gFnSc7	nevinnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
zázraku	zázrak	k1gInSc2	zázrak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
pilířů	pilíř	k1gInPc2	pilíř
katolické	katolický	k2eAgFnSc2d1	katolická
víry	víra	k1gFnSc2	víra
(	(	kIx(	(
<g/>
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fialová	Fialová	k1gFnSc1	Fialová
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
během	během	k7c2	během
vánočního	vánoční	k2eAgNnSc2d1	vánoční
období	období	k1gNnSc2	období
od	od	k7c2	od
adventu	advent	k1gInSc2	advent
do	do	k7c2	do
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
barvu	barva	k1gFnSc4	barva
postu	post	k1gInSc2	post
a	a	k8xC	a
pokání	pokání	k1gNnSc2	pokání
za	za	k7c4	za
hříchy	hřích	k1gInPc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
postu	post	k1gInSc2	post
jsou	být	k5eAaImIp3nP	být
málo	málo	k6eAd1	málo
používány	používat	k5eAaImNgInP	používat
i	i	k8xC	i
ozdoby	ozdoba	k1gFnPc1	ozdoba
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
adventní	adventní	k2eAgInPc4d1	adventní
věnce	věnec	k1gInPc4	věnec
v	v	k7c6	v
katolických	katolický	k2eAgInPc6d1	katolický
kostelích	kostel	k1gInPc6	kostel
jsou	být	k5eAaImIp3nP	být
nezdobené	zdobený	k2eNgFnPc1d1	nezdobená
<g/>
,	,	kIx,	,
zdobené	zdobený	k2eAgFnPc1d1	zdobená
bývají	bývat	k5eAaImIp3nP	bývat
pouze	pouze	k6eAd1	pouze
kněžská	kněžský	k2eAgNnPc4d1	kněžské
roucha	roucho	k1gNnPc4	roucho
<g/>
.	.	kIx.	.
</s>
<s>
Růžová	růžový	k2eAgFnSc1d1	růžová
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
radost	radost	k1gFnSc4	radost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
neděle	neděle	k1gFnSc2	neděle
Gaudete	Gaudete	k1gFnSc2	Gaudete
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
například	například	k6eAd1	například
pro	pro	k7c4	pro
svíce	svíce	k1gFnPc4	svíce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
rozžíhají	rozžíhat	k5eAaImIp3nP	rozžíhat
o	o	k7c6	o
třetí	třetí	k4xOgFnSc4	třetí
naděli	naděli	k?	naděli
adventní	adventní	k2eAgNnSc1d1	adventní
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
od	od	k7c2	od
liturgické	liturgický	k2eAgFnSc2d1	liturgická
reformy	reforma	k1gFnSc2	reforma
provedné	provedný	k2eAgFnSc2d1	provedná
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
tradiční	tradiční	k2eAgFnSc7d1	tradiční
slavnostní	slavnostní	k2eAgFnSc7d1	slavnostní
vánoční	vánoční	k2eAgFnSc7d1	vánoční
barvou	barva	k1gFnSc7	barva
pro	pro	k7c4	pro
protestanty	protestant	k1gMnPc4	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
informací	informace	k1gFnPc2	informace
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
již	již	k6eAd1	již
symbolem	symbol	k1gInSc7	symbol
"	"	kIx"	"
<g/>
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Ježíše	Ježíš	k1gMnPc4	Ježíš
nosila	nosit	k5eAaImAgFnS	nosit
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
lůně	lůno	k1gNnSc6	lůno
a	a	k8xC	a
zrodila	zrodit	k5eAaPmAgFnS	zrodit
ho	on	k3xPp3gMnSc4	on
celému	celý	k2eAgInSc3d1	celý
světu	svět	k1gInSc3	svět
právě	právě	k9	právě
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
není	být	k5eNaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
dříve	dříve	k6eAd2	dříve
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
používali	používat	k5eAaImAgMnP	používat
protestanti	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
Pannu	Panna	k1gFnSc4	Panna
Marii	Maria	k1gFnSc3	Maria
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
liturgii	liturgie	k1gFnSc6	liturgie
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Názor	názor	k1gInSc1	názor
protestantů	protestant	k1gMnPc2	protestant
na	na	k7c4	na
mariánský	mariánský	k2eAgInSc4d1	mariánský
kult	kult	k1gInSc4	kult
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
vše	všechen	k3xTgNnSc4	všechen
nadpozemské	nadpozemský	k2eAgNnSc4d1	nadpozemské
<g/>
,	,	kIx,	,
nebeské	nebeský	k2eAgNnSc4d1	nebeské
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
například	například	k6eAd1	například
víru	víra	k1gFnSc4	víra
nebo	nebo	k8xC	nebo
klid	klid	k1gInSc4	klid
a	a	k8xC	a
ztišení	ztišení	k1gNnSc4	ztišení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vánoční	vánoční	k2eAgInSc4d1	vánoční
stromek	stromek	k1gInSc4	stromek
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInSc1d1	vánoční
strom	strom	k1gInSc1	strom
je	být	k5eAaImIp3nS	být
někým	někdo	k3yInSc7	někdo
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
křesťany	křesťan	k1gMnPc4	křesťan
převzatou	převzatý	k2eAgFnSc4d1	převzatá
pohanskou	pohanský	k2eAgFnSc4d1	pohanská
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc4d1	pocházející
z	z	k7c2	z
rituálu	rituál	k1gInSc2	rituál
zimního	zimní	k2eAgInSc2d1	zimní
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
použití	použití	k1gNnSc4	použití
stálezelených	stálezelený	k2eAgFnPc2d1	stálezelená
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
s	s	k7c7	s
oledem	oled	k1gInSc7	oled
na	na	k7c4	na
pohanské	pohanský	k2eAgNnSc4d1	pohanské
uctívání	uctívání	k1gNnSc4	uctívání
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
životopisů	životopis	k1gInPc2	životopis
z	z	k7c2	z
osmého	osmý	k4xOgNnSc2	osmý
století	století	k1gNnSc2	století
Æ	Æ	k?	Æ
Stephanus	Stephanus	k1gMnSc1	Stephanus
<g/>
,	,	kIx,	,
svatý	svatý	k2eAgMnSc1d1	svatý
Bonifác	Bonifác	k1gMnSc1	Bonifác
(	(	kIx(	(
<g/>
634	[number]	k4	634
<g/>
–	–	k?	–
<g/>
709	[number]	k4	709
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
misionář	misionář	k1gMnSc1	misionář
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
zaťal	zatít	k5eAaPmAgMnS	zatít
sekeru	sekera	k1gFnSc4	sekera
do	do	k7c2	do
dubu	dub	k1gInSc2	dub
věnovaného	věnovaný	k2eAgInSc2d1	věnovaný
Thorovi	Thor	k1gMnSc3	Thor
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgInS	ukázat
na	na	k7c4	na
jedli	jedle	k1gFnSc4	jedle
<g/>
,	,	kIx,	,
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgInSc4d2	vhodnější
objekt	objekt	k1gInSc4	objekt
uctívání	uctívání	k1gNnSc2	uctívání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
nebe	nebe	k1gNnSc4	nebe
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
tradice	tradice	k1gFnSc1	tradice
vánočního	vánoční	k2eAgInSc2d1	vánoční
stromu	strom	k1gInSc2	strom
podle	podle	k7c2	podle
dostupných	dostupný	k2eAgInPc2d1	dostupný
zdrojů	zdroj	k1gInPc2	zdroj
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
mnozí	mnohý	k2eAgMnPc1d1	mnohý
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
založil	založit	k5eAaPmAgMnS	založit
tuto	tento	k3xDgFnSc4	tento
tradici	tradice	k1gFnSc4	tradice
již	již	k6eAd1	již
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zíbrt	Zíbrt	k1gInSc1	Zíbrt
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Hoj	hojit	k5eAaImRp2nS	hojit
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc4	ten
štědrý	štědrý	k2eAgInSc1d1	štědrý
večere	večer	k1gInSc5	večer
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
vánočním	vánoční	k2eAgInSc6d1	vánoční
stromku	stromek	k1gInSc6	stromek
až	až	k9	až
do	do	k7c2	do
věku	věk	k1gInSc2	věk
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
potuchy	potucha	k1gFnSc2	potucha
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
možná	možná	k9	možná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obyčej	obyčej	k1gInSc1	obyčej
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
vykládají	vykládat	k5eAaImIp3nP	vykládat
<g/>
,	,	kIx,	,
pohanského	pohanský	k2eAgInSc2d1	pohanský
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
ušel	ujít	k5eAaPmAgInS	ujít
pozornosti	pozornost	k1gFnSc3	pozornost
mravokárců	mravokárce	k1gMnPc2	mravokárce
věku	věk	k1gInSc2	věk
XV	XV	kA	XV
<g/>
.	.	kIx.	.
a	a	k8xC	a
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
skladbách	skladba	k1gFnPc6	skladba
tepou	tepat	k5eAaImIp3nP	tepat
kde	kde	k6eAd1	kde
jakou	jaký	k3yRgFnSc4	jaký
pověru	pověra	k1gFnSc4	pověra
a	a	k8xC	a
obyčej	obyčej	k1gInSc4	obyčej
<g/>
.	.	kIx.	.
...	...	k?	...
<g/>
Teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
věku	věk	k1gInSc2	věk
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
kárá	kárat	k5eAaImIp3nS	kárat
Dannhauer	Dannhauer	k1gMnSc1	Dannhauer
v	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
strojení	strojení	k1gNnSc2	strojení
jedlového	jedlový	k2eAgInSc2d1	jedlový
stromku	stromek	k1gInSc2	stromek
vánočního	vánoční	k2eAgInSc2d1	vánoční
<g/>
,	,	kIx,	,
ověšeného	ověšený	k2eAgInSc2d1	ověšený
loutkami	loutka	k1gFnPc7	loutka
a	a	k8xC	a
cukrovinkami	cukrovinka	k1gFnPc7	cukrovinka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
posavadních	posavadní	k2eAgInPc2d1	posavadní
výzkumů	výzkum	k1gInPc2	výzkum
jediná	jediný	k2eAgFnSc1d1	jediná
výslovná	výslovný	k2eAgFnSc1d1	výslovná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vánočním	vánoční	k2eAgInSc6d1	vánoční
stromku	stromek	k1gInSc6	stromek
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
starších	starý	k2eAgMnPc2d2	starší
<g/>
...	...	k?	...
<g/>
Veškeré	veškerý	k3xTgInPc1	veškerý
dohady	dohad	k1gInPc1	dohad
o	o	k7c6	o
dobách	doba	k1gFnPc6	doba
hluboko	hluboko	k6eAd1	hluboko
před	před	k7c7	před
tímto	tento	k3xDgNnSc7	tento
datem	datum	k1gNnSc7	datum
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nepravděpodobné	pravděpodobný	k2eNgNnSc4d1	nepravděpodobné
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
rostlina	rostlina	k1gFnSc1	rostlina
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
Vánocemi	Vánoce	k1gFnPc7	Vánoce
spojována	spojovat	k5eAaImNgFnS	spojovat
již	již	k6eAd1	již
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
populární	populární	k2eAgFnPc4d1	populární
vánoční	vánoční	k2eAgFnPc4d1	vánoční
dekorativní	dekorativní	k2eAgFnPc4d1	dekorativní
rostliny	rostlina	k1gFnPc4	rostlina
patří	patřit	k5eAaImIp3nS	patřit
cesmína	cesmína	k1gFnSc1	cesmína
<g/>
,	,	kIx,	,
jmelí	jmelí	k1gNnSc1	jmelí
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
amarylis	amarylis	k1gInSc1	amarylis
a	a	k8xC	a
vánoční	vánoční	k2eAgInSc1d1	vánoční
kaktus	kaktus	k1gInSc1	kaktus
<g/>
,	,	kIx,	,
brambořík	brambořík	k1gInSc1	brambořík
Cyclamen	Cyclamen	k2eAgInSc4d1	Cyclamen
persicum	persicum	k1gInSc4	persicum
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
domu	dům	k1gInSc2	dům
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ozdoben	ozdobit	k5eAaPmNgInS	ozdobit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vánočním	vánoční	k2eAgInSc7d1	vánoční
stromečkem	stromeček	k1gInSc7	stromeček
<g/>
,	,	kIx,	,
těmito	tento	k3xDgFnPc7	tento
rostlinami	rostlina	k1gFnPc7	rostlina
a	a	k8xC	a
girlandami	girlanda	k1gFnPc7	girlanda
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
dekoracemi	dekorace	k1gFnPc7	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Popisovány	popisován	k2eAgInPc1d1	popisován
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
barborky	barborka	k1gFnPc1	barborka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uvádí	uvádět	k5eAaImIp3nS	uvádět
už	už	k9	už
B.	B.	kA	B.
Němcová	Němcová	k1gFnSc1	Němcová
zvyk	zvyk	k1gInSc1	zvyk
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
na	na	k7c6	na
sv.	sv.	kA	sv.
Barboru	Barbora	k1gFnSc4	Barbora
huřízne	huříznout	k5eAaPmIp3nS	huříznout
si	se	k3xPyFc3	se
dívčí	dívčí	k2eAgFnSc4d1	dívčí
višnovú	višnovú	k?	višnovú
nebo	nebo	k8xC	nebo
křešňovú	křešňovú	k?	křešňovú
větvičku	větvička	k1gFnSc4	větvička
<g/>
.	.	kIx.	.
</s>
<s>
Barborky	Barborka	k1gFnPc1	Barborka
se	se	k3xPyFc4	se
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
používaly	používat	k5eAaImAgFnP	používat
k	k	k7c3	k
hledání	hledání	k1gNnSc3	hledání
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Hoj	hojit	k5eAaImRp2nS	hojit
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc4	ten
štědrý	štědrý	k2eAgInSc1d1	štědrý
večere	večer	k1gInSc5	večer
popisuje	popisovat	k5eAaImIp3nS	popisovat
Zíbrt	Zíbrta	k1gFnPc2	Zíbrta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tu	tu	k6eAd1	tu
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
květinovou	květinový	k2eAgFnSc4d1	květinová
výzdobu	výzdoba	k1gFnSc4	výzdoba
a	a	k8xC	a
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
si	se	k3xPyFc3	se
měšťák	měšťák	k1gMnSc1	měšťák
pořídí	pořídit	k5eAaPmIp3nS	pořídit
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
z	z	k7c2	z
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
obchodnicky	obchodnicky	k6eAd1	obchodnicky
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
<g/>
.	.	kIx.	.
...	...	k?	...
<g/>
přičítal	přičítat	k5eAaImAgMnS	přičítat
jim	on	k3xPp3gMnPc3	on
(	(	kIx(	(
<g/>
lid	lid	k1gInSc1	lid
vybraným	vybraný	k2eAgNnSc7d1	vybrané
vánočním	vánoční	k2eAgNnSc7d1	vánoční
<g />
.	.	kIx.	.
</s>
<s>
květinám	květina	k1gFnPc3	květina
<g/>
)	)	kIx)	)
moc	moc	k6eAd1	moc
čarovnou	čarovný	k2eAgFnSc7d1	čarovná
<g/>
,	,	kIx,	,
věštebnou	věštebný	k2eAgFnSc7d1	věštebná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Popisuje	popisovat	k5eAaImIp3nS	popisovat
dávný	dávný	k2eAgInSc1d1	dávný
zvyk	zvyk	k1gInSc1	zvyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
Ruože	Ruož	k1gFnSc2	Ruož
Jerychuntské	Jerychuntský	k2eAgNnSc1d1	Jerychuntský
(	(	kIx(	(
<g/>
choulivka	choulivka	k1gFnSc1	choulivka
jerišská	jerišský	k2eAgFnSc1d1	jerišská
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
růže	růže	k1gFnSc1	růže
z	z	k7c2	z
Jericha	Jericho	k1gNnSc2	Jericho
(	(	kIx(	(
<g/>
Anastatica	Anastatic	k2eAgFnSc1d1	Anastatica
hierochontica	hierochontica	k1gFnSc1	hierochontica
L.	L.	kA	L.
<g/>
)	)	kIx)	)
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
<g/>
...	...	k?	...
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Palestině	Palestina	k1gFnSc6	Palestina
<g/>
,	,	kIx,	,
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
větve	větev	k1gFnPc1	větev
<g/>
...	...	k?	...
uschnou	uschnout	k5eAaPmIp3nP	uschnout
<g/>
,	,	kIx,	,
stvrdnou	stvrdnout	k5eAaPmIp3nP	stvrdnout
a	a	k8xC	a
schoulí	schoulit	k5eAaPmIp3nP	schoulit
se	se	k3xPyFc4	se
dohromady	dohromady	k6eAd1	dohromady
v	v	k7c4	v
kulaté	kulatý	k2eAgNnSc4d1	kulaté
klubko	klubko	k1gNnSc4	klubko
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
květina	květina	k1gFnSc1	květina
<g/>
)	)	kIx)	)
upoutala	upoutat	k5eAaPmAgFnS	upoutat
pozornost	pozornost	k1gFnSc1	pozornost
poutníků	poutník	k1gMnPc2	poutník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
válek	válka	k1gFnPc2	válka
křižáckých	křižácký	k2eAgFnPc2d1	křižácká
přinášeli	přinášet	k5eAaImAgMnP	přinášet
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xS	jako
památku	památka	k1gFnSc4	památka
z	z	k7c2	z
pouti	pouť	k1gFnSc2	pouť
na	na	k7c4	na
Východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dokument	dokument	k1gInSc1	dokument
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1554	[number]	k4	1554
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Tato	tento	k3xDgFnSc1	tento
růže	růže	k1gFnSc1	růže
do	do	k7c2	do
vína	víno	k1gNnSc2	víno
v	v	k7c6	v
sklenici	sklenice	k1gFnSc6	sklenice
od	od	k7c2	od
ženy	žena	k1gFnSc2	žena
vundaná	vundaná	k1gFnSc1	vundaná
<g/>
,	,	kIx,	,
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
poroditi	porodit	k5eAaPmF	porodit
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
otevře	otevřít	k5eAaPmIp3nS	otevřít
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
a	a	k8xC	a
pakli	pakli	k8xS	pakli
děvečku	děvečka	k1gFnSc4	děvečka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zuostane	zuostat	k5eAaPmIp3nS	zuostat
zavřena	zavřít	k5eAaPmNgFnS	zavřít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
den	den	k1gInSc4	den
Božího	boží	k2eAgNnSc2d1	boží
narození	narození	k1gNnSc2	narození
též	též	k6eAd1	též
se	se	k3xPyFc4	se
otvírá	otvírat	k5eAaImIp3nS	otvírat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jesličky	jesličky	k1gFnPc1	jesličky
<g/>
.	.	kIx.	.
</s>
<s>
Betlémy	Betlém	k1gInPc1	Betlém
byly	být	k5eAaImAgInP	být
vytvářeny	vytvářet	k5eAaImNgInP	vytvářet
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
propagovány	propagován	k2eAgMnPc4d1	propagován
svatým	svatý	k2eAgInSc7d1	svatý
Františkem	františek	k1gInSc7	františek
z	z	k7c2	z
Asissi	Asisse	k1gFnSc4	Asisse
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1223	[number]	k4	1223
<g/>
,	,	kIx,	,
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
šířily	šířit	k5eAaImAgInP	šířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Typy	typ	k1gInPc1	typ
dekorací	dekorace	k1gFnPc2	dekorace
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
symboliky	symbolika	k1gFnSc2	symbolika
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
místních	místní	k2eAgFnPc2d1	místní
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
zdrojů	zdroj	k1gInPc2	zdroj
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
však	však	k9	však
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
živé	živý	k2eAgInPc1d1	živý
betlémy	betlém	k1gInPc1	betlém
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
svatého	svatý	k2eAgMnSc4d1	svatý
Františka	František	k1gMnSc4	František
populární	populární	k2eAgFnSc7d1	populární
alternativou	alternativa	k1gFnSc7	alternativa
k	k	k7c3	k
jesličkám	jesličky	k1gFnPc3	jesličky
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
komerčně	komerčně	k6eAd1	komerčně
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
dekorace	dekorace	k1gFnPc1	dekorace
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
inspirovány	inspirován	k2eAgInPc1d1	inspirován
papírovými	papírový	k2eAgInPc7d1	papírový
řetězy	řetěz	k1gInPc7	řetěz
tvořenými	tvořený	k2eAgFnPc7d1	tvořená
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
předvádění	předvádění	k1gNnSc1	předvádění
Betléma	Betlém	k1gInSc2	Betlém
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vyzýváni	vyzývat	k5eAaImNgMnP	vyzývat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
sami	sám	k3xTgMnPc1	sám
nejvíce	nejvíce	k6eAd1	nejvíce
originální	originální	k2eAgInSc4d1	originální
nebo	nebo	k8xC	nebo
realistický	realistický	k2eAgInSc4d1	realistický
Betlém	Betlém	k1gInSc4	Betlém
či	či	k8xC	či
doplněk	doplněk	k1gInSc4	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
rodinách	rodina	k1gFnPc6	rodina
jsou	být	k5eAaImIp3nP	být
doplňky	doplněk	k1gInPc1	doplněk
používané	používaný	k2eAgInPc1d1	používaný
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
Betléma	Betlém	k1gInSc2	Betlém
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
cenné	cenný	k2eAgNnSc4d1	cenné
rodinné	rodinný	k2eAgNnSc4d1	rodinné
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Betlémská	betlémský	k2eAgFnSc1d1	Betlémská
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Betlémská	betlémský	k2eAgFnSc1d1	Betlémská
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
popisovaná	popisovaný	k2eAgFnSc1d1	popisovaná
v	v	k7c6	v
Matoušově	Matoušův	k2eAgNnSc6d1	Matoušovo
evangeliu	evangelium	k1gNnSc6	evangelium
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
kometu	kometa	k1gFnSc4	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
kometu	kometa	k1gFnSc4	kometa
zobrazil	zobrazit	k5eAaPmAgInS	zobrazit
Betlémskou	betlémský	k2eAgFnSc4d1	Betlémská
hvězdu	hvězda	k1gFnSc4	hvězda
do	do	k7c2	do
vánoční	vánoční	k2eAgFnSc2d1	vánoční
scény	scéna	k1gFnSc2	scéna
poprvé	poprvé	k6eAd1	poprvé
zřejmě	zřejmě	k6eAd1	zřejmě
italský	italský	k2eAgMnSc1d1	italský
malíř	malíř	k1gMnSc1	malíř
Giotto	Giott	k2eAgNnSc1d1	Giotto
di	di	k?	di
Bondone	Bondon	k1gInSc5	Bondon
(	(	kIx(	(
<g/>
1267	[number]	k4	1267
<g/>
–	–	k?	–
<g/>
1337	[number]	k4	1337
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1304	[number]	k4	1304
na	na	k7c6	na
fresce	freska	k1gFnSc6	freska
Klanění	klanění	k1gNnSc2	klanění
pro	pro	k7c4	pro
padovskou	padovský	k2eAgFnSc4d1	Padovská
kapli	kaple	k1gFnSc4	kaple
Scrovegni	Scrovegeň	k1gFnSc3	Scrovegeň
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
úkazem	úkaz	k1gInSc7	úkaz
Halleyovy	Halleyův	k2eAgFnPc1d1	Halleyova
komety	kometa	k1gFnPc1	kometa
v	v	k7c6	v
září	září	k1gNnSc6	září
1301	[number]	k4	1301
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
středověkých	středověký	k2eAgInPc6d1	středověký
obrazech	obraz	k1gInPc6	obraz
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
třeboňském	třeboňský	k2eAgNnSc6d1	Třeboňské
Narození	narození	k1gNnSc6	narození
Páně	páně	k2eAgFnSc1d1	páně
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
Betlémská	betlémský	k2eAgFnSc1d1	Betlémská
hvězda	hvězda	k1gFnSc1	hvězda
zobrazena	zobrazit	k5eAaPmNgFnS	zobrazit
mnohem	mnohem	k6eAd1	mnohem
nenápadnější	nápadní	k2eNgFnSc1d2	nápadní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
tradice	tradice	k1gFnSc2	tradice
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
vánočního	vánoční	k2eAgInSc2d1	vánoční
Betléma	Betlém	k1gInSc2	Betlém
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgInS	ustálit
i	i	k9	i
zvyk	zvyk	k1gInSc1	zvyk
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
komety	kometa	k1gFnPc1	kometa
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
scenérií	scenérie	k1gFnSc7	scenérie
betlémského	betlémský	k2eAgInSc2d1	betlémský
chléva	chlév	k1gInSc2	chlév
s	s	k7c7	s
jesličkami	jesličky	k1gFnPc7	jesličky
a	a	k8xC	a
Svatou	svatý	k2eAgFnSc7d1	svatá
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Betlémská	betlémský	k2eAgFnSc1d1	Betlémská
hvězda	hvězda	k1gFnSc1	hvězda
nebo	nebo	k8xC	nebo
vánoční	vánoční	k2eAgFnSc1d1	vánoční
hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
zcela	zcela	k6eAd1	zcela
specifický	specifický	k2eAgInSc4d1	specifický
symbol	symbol	k1gInSc4	symbol
používána	používán	k2eAgFnSc1d1	používána
a	a	k8xC	a
vytvářena	vytvářen	k2eAgFnSc1d1	vytvářena
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
ve	v	k7c6	v
zcela	zcela	k6eAd1	zcela
ustálené	ustálený	k2eAgFnSc6d1	ustálená
tradiční	tradiční	k2eAgFnSc6d1	tradiční
podobě	podoba	k1gFnSc6	podoba
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Fröbelstern	Fröbelstern	k1gNnSc1	Fröbelstern
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nebo	nebo	k8xC	nebo
Moravian	Moravian	k1gInSc1	Moravian
star	star	k1gFnSc2	star
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Adventní	adventní	k2eAgInSc4d1	adventní
věnec	věnec	k1gInSc4	věnec
<g/>
.	.	kIx.	.
</s>
<s>
Adventní	adventní	k2eAgInSc1d1	adventní
věnec	věnec	k1gInSc1	věnec
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
symbol	symbol	k1gInSc1	symbol
západní	západní	k2eAgFnSc2d1	západní
církve	církev	k1gFnSc2	církev
sloužící	sloužící	k1gFnSc2	sloužící
k	k	k7c3	k
symbolickému	symbolický	k2eAgNnSc3d1	symbolické
odpočítávání	odpočítávání	k1gNnSc3	odpočítávání
čtyř	čtyři	k4xCgInPc2	čtyři
týdnů	týden	k1gInPc2	týden
adventu	advent	k1gInSc2	advent
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
věnce	věnec	k1gInSc2	věnec
z	z	k7c2	z
jehličnatých	jehličnatý	k2eAgFnPc2d1	jehličnatá
větví	větev	k1gFnPc2	větev
ozdobeného	ozdobený	k2eAgInSc2d1	ozdobený
čtyřmi	čtyři	k4xCgFnPc7	čtyři
svícemi	svíce	k1gFnPc7	svíce
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
adventní	adventní	k2eAgFnSc4d1	adventní
neděli	neděle	k1gFnSc4	neděle
se	se	k3xPyFc4	se
zapaluje	zapalovat	k5eAaImIp3nS	zapalovat
další	další	k2eAgFnSc1d1	další
svíčka	svíčka	k1gFnSc1	svíčka
<g/>
.	.	kIx.	.
</s>
<s>
Svíček	svíčka	k1gFnPc2	svíčka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ale	ale	k8xC	ale
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zvyklostech	zvyklost	k1gFnPc6	zvyklost
a	a	k8xC	a
trendech	trend	k1gInPc6	trend
<g/>
.	.	kIx.	.
</s>
<s>
Svíce	svíce	k1gFnPc1	svíce
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
různě	různě	k6eAd1	různě
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
symetricky	symetricky	k6eAd1	symetricky
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
asymetricky	asymetricky	k6eAd1	asymetricky
rozmístěné	rozmístěný	k2eAgNnSc1d1	rozmístěné
<g/>
.	.	kIx.	.
</s>
<s>
Zdobené	zdobený	k2eAgInPc1d1	zdobený
věnce	věnec	k1gInPc1	věnec
bez	bez	k7c2	bez
svíček	svíčka	k1gFnPc2	svíčka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
nazývány	nazýván	k2eAgInPc4d1	nazýván
adventní	adventní	k2eAgInPc4d1	adventní
věnce	věnec	k1gInPc4	věnec
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
k	k	k7c3	k
ozdobení	ozdobení	k1gNnSc3	ozdobení
vchodových	vchodový	k2eAgFnPc2d1	vchodová
dveří	dveře	k1gFnPc2	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgInPc1d1	klasický
adventní	adventní	k2eAgInPc1d1	adventní
věnce	věnec	k1gInPc1	věnec
byly	být	k5eAaImAgInP	být
zavěšované	zavěšovaný	k2eAgNnSc4d1	zavěšovaný
na	na	k7c6	na
stuhách	stuha	k1gFnPc6	stuha
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	on	k3xPp3gMnPc4	on
nahradily	nahradit	k5eAaPmAgInP	nahradit
věnce	věnec	k1gInPc1	věnec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
kladou	klást	k5eAaImIp3nP	klást
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
svátečně	svátečně	k6eAd1	svátečně
upravený	upravený	k2eAgInSc4d1	upravený
stůl	stůl	k1gInSc4	stůl
<g/>
.	.	kIx.	.
</s>
<s>
Věnce	věnec	k1gInPc1	věnec
na	na	k7c4	na
zavěšení	zavěšení	k1gNnSc4	zavěšení
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
svíček	svíčka	k1gFnPc2	svíčka
<g/>
)	)	kIx)	)
používané	používaný	k2eAgInPc1d1	používaný
k	k	k7c3	k
vánoční	vánoční	k2eAgFnSc3d1	vánoční
a	a	k8xC	a
adventní	adventní	k2eAgFnSc3d1	adventní
výzdobě	výzdoba	k1gFnSc3	výzdoba
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
někdy	někdy	k6eAd1	někdy
nazývány	nazýván	k2eAgInPc1d1	nazýván
adventní	adventní	k2eAgInPc1d1	adventní
věnce	věnec	k1gInPc1	věnec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
či	či	k8xC	či
okna	okno	k1gNnSc2	okno
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
říká	říkat	k5eAaImIp3nS	říkat
domovní	domovní	k2eAgNnSc4d1	domovní
znamení	znamení	k1gNnSc4	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Adventní	adventní	k2eAgInSc1d1	adventní
parter	parter	k1gInSc1	parter
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
podlouhlé	podlouhlý	k2eAgNnSc4d1	podlouhlé
aranžmá	aranžmá	k1gNnSc4	aranžmá
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
nejméně	málo	k6eAd3	málo
tři	tři	k4xCgFnPc4	tři
svíčky	svíčka	k1gFnPc4	svíčka
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
svíček	svíčka	k1gFnPc2	svíčka
je	být	k5eAaImIp3nS	být
upraveno	upraven	k2eAgNnSc1d1	upraveno
chvojím	chvojí	k1gNnSc7	chvojí
a	a	k8xC	a
dekoracemi	dekorace	k1gFnPc7	dekorace
jako	jako	k8xC	jako
stuhy	stuha	k1gFnPc1	stuha
<g/>
,	,	kIx,	,
baňky	baňka	k1gFnPc1	baňka
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Svíčky	svíčka	k1gFnPc1	svíčka
v	v	k7c6	v
adventním	adventní	k2eAgInSc6d1	adventní
parteru	parter	k1gInSc6	parter
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
různě	různě	k6eAd1	různě
seskupeny	seskupit	k5eAaPmNgFnP	seskupit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rozmístěny	rozmístěn	k2eAgInPc1d1	rozmístěn
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Adventní	adventní	k2eAgInSc4d1	adventní
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
výzdoby	výzdoba	k1gFnSc2	výzdoba
zejména	zejména	k9	zejména
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Kalendář	kalendář	k1gInSc1	kalendář
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
odpočítávání	odpočítávání	k1gNnSc3	odpočítávání
dnů	den	k1gInPc2	den
adventu	advent	k1gInSc2	advent
ke	k	k7c3	k
Štědrému	štědrý	k2eAgNnSc3d1	štědré
dnu	dno	k1gNnSc3	dno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
zhotovován	zhotovován	k2eAgMnSc1d1	zhotovován
jako	jako	k8xS	jako
celek	celek	k1gInSc1	celek
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
označujícími	označující	k2eAgNnPc7d1	označující
dny	dna	k1gFnSc2	dna
do	do	k7c2	do
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
skrývají	skrývat	k5eAaImIp3nP	skrývat
sladkosti	sladkost	k1gFnPc1	sladkost
nebo	nebo	k8xC	nebo
hračky	hračka	k1gFnPc1	hračka
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
kalendáře	kalendář	k1gInSc2	kalendář
zhotoveny	zhotoven	k2eAgMnPc4d1	zhotoven
floristy	florista	k1gMnPc4	florista
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
skrývání	skrývání	k1gNnSc3	skrývání
objektů	objekt	k1gInPc2	objekt
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
malé	malý	k2eAgFnPc1d1	malá
krabičky	krabička	k1gFnPc1	krabička
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
konstrukce	konstrukce	k1gFnSc1	konstrukce
bývá	bývat	k5eAaImIp3nS	bývat
z	z	k7c2	z
drátěného	drátěný	k2eAgNnSc2d1	drátěné
pletiva	pletivo	k1gNnSc2	pletivo
či	či	k8xC	či
pevného	pevný	k2eAgInSc2d1	pevný
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInPc1d1	vánoční
oděvy	oděv	k1gInPc1	oděv
a	a	k8xC	a
kostýmy	kostým	k1gInPc1	kostým
jsou	být	k5eAaImIp3nP	být
dány	dát	k5eAaPmNgFnP	dát
jednak	jednak	k8xC	jednak
daným	daný	k2eAgInSc7d1	daný
svátkem	svátek	k1gInSc7	svátek
a	a	k8xC	a
úlohou	úloha	k1gFnSc7	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svátků	svátek	k1gInPc2	svátek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
měněny	měněn	k2eAgFnPc4d1	měněna
mešní	mešní	k2eAgNnPc4d1	mešní
roucha	roucho	k1gNnPc4	roucho
v	v	k7c6	v
katolickém	katolický	k2eAgInSc6d1	katolický
kostele	kostel	k1gInSc6	kostel
<g/>
,	,	kIx,	,
adventní	adventní	k2eAgNnSc1d1	adventní
fialové	fialový	k2eAgNnSc1d1	fialové
za	za	k7c4	za
růžové	růžový	k2eAgNnSc4d1	růžové
o	o	k7c4	o
gaudette	gaudette	k5eAaPmIp2nP	gaudette
neděli	neděle	k1gFnSc4	neděle
a	a	k8xC	a
bílé	bílý	k2eAgFnPc1d1	bílá
nebo	nebo	k8xC	nebo
zlaté	zlatý	k2eAgFnPc1d1	zlatá
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
má	mít	k5eAaImIp3nS	mít
připomínat	připomínat	k5eAaImF	připomínat
světce	světec	k1gMnPc4	světec
<g/>
,	,	kIx,	,
vysokého	vysoký	k2eAgMnSc2d1	vysoký
katolického	katolický	k2eAgMnSc2d1	katolický
církevního	církevní	k2eAgMnSc2d1	církevní
hodnostáře	hodnostář	k1gMnSc2	hodnostář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
rozdává	rozdávat	k5eAaImIp3nS	rozdávat
nezletilým	zletilý	k2eNgFnPc3d1	nezletilá
dětem	dítě	k1gFnPc3	dítě
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svátků	svátek	k1gInPc2	svátek
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
má	mít	k5eAaImIp3nS	mít
postava	postava	k1gFnSc1	postava
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
roucho	roucho	k1gNnSc4	roucho
napodobující	napodobující	k2eAgInSc1d1	napodobující
oděv	oděv	k1gInSc1	oděv
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
oděv	oděv	k1gInSc1	oděv
se	s	k7c7	s
zlatými	zlatý	k2eAgInPc7d1	zlatý
znaky	znak	k1gInPc7	znak
a	a	k8xC	a
lemy	lem	k1gInPc7	lem
a	a	k8xC	a
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
mitra	mitra	k1gFnSc1	mitra
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
zlacená	zlacený	k2eAgFnSc1d1	zlacená
<g/>
,	,	kIx,	,
berla	berla	k1gFnSc1	berla
se	s	k7c7	s
spirálovitým	spirálovitý	k2eAgNnSc7d1	spirálovité
zakončením	zakončení	k1gNnSc7	zakončení
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgInPc1d1	bílý
vlnité	vlnitý	k2eAgInPc1d1	vlnitý
vlasy	vlas	k1gInPc1	vlas
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgInSc4d2	delší
bílý	bílý	k2eAgInSc4d1	bílý
plnovous	plnovous	k1gInSc4	plnovous
a	a	k8xC	a
huňaté	huňatý	k2eAgNnSc4d1	huňaté
bílé	bílý	k2eAgNnSc4d1	bílé
obočí	obočí	k1gNnSc4	obočí
<g/>
.	.	kIx.	.
</s>
<s>
Čert	čert	k1gMnSc1	čert
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
postavu	postava	k1gFnSc4	postava
z	z	k7c2	z
podzemí	podzemí	k1gNnSc2	podzemí
plného	plný	k2eAgInSc2d1	plný
kouře	kouř	k1gInSc2	kouř
a	a	k8xC	a
plamenů	plamen	k1gInPc2	plamen
<g/>
,	,	kIx,	,
pekla	peklo	k1gNnSc2	peklo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gInSc3	on
přisuzována	přisuzovat	k5eAaImNgFnS	přisuzovat
impulsivní	impulsivní	k2eAgFnSc1d1	impulsivní
agresivita	agresivita	k1gFnSc1	agresivita
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
temnou	temný	k2eAgFnSc7d1	temná
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Čert	čert	k1gMnSc1	čert
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
bývá	bývat	k5eAaImIp3nS	bývat
upraven	upraven	k2eAgInSc1d1	upraven
jako	jako	k8xC	jako
děsivá	děsivý	k2eAgFnSc1d1	děsivá
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
stylizovaná	stylizovaný	k2eAgFnSc1d1	stylizovaná
rohatá	rohatý	k2eAgFnSc1d1	rohatá
nestvůra	nestvůra	k1gFnSc1	nestvůra
s	s	k7c7	s
řetězy	řetěz	k1gInPc7	řetěz
napodobující	napodobující	k2eAgMnSc1d1	napodobující
zvíře	zvíře	k1gNnSc4	zvíře
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
nohou	noha	k1gFnPc6	noha
<g/>
,	,	kIx,	,
špinavé	špinavý	k2eAgFnPc1d1	špinavá
od	od	k7c2	od
sazí	saze	k1gFnPc2	saze
<g/>
.	.	kIx.	.
</s>
<s>
Andělé	anděl	k1gMnPc1	anděl
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
nevinnost	nevinnost	k1gFnSc4	nevinnost
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nebeské	nebeský	k2eAgFnPc1d1	nebeská
bytosti	bytost	k1gFnPc1	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Andělé	anděl	k1gMnPc1	anděl
bývají	bývat	k5eAaImIp3nP	bývat
zpodobněni	zpodobnit	k5eAaPmNgMnP	zpodobnit
jako	jako	k8xS	jako
postavy	postava	k1gFnPc1	postava
v	v	k7c6	v
bílých	bílý	k2eAgInPc6d1	bílý
rozvolněných	rozvolněný	k2eAgInPc6d1	rozvolněný
šatech	šat	k1gInPc6	šat
(	(	kIx(	(
<g/>
dívky	dívka	k1gFnPc1	dívka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
suknicích	suknice	k1gFnPc6	suknice
(	(	kIx(	(
<g/>
chlapci	chlapec	k1gMnSc6	chlapec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
vlasy	vlas	k1gInPc7	vlas
<g/>
,	,	kIx,	,
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgFnPc1d1	vhodná
jsou	být	k5eAaImIp3nP	být
zlaté	zlatý	k2eAgFnPc1d1	zlatá
paruky	paruka	k1gFnPc1	paruka
nebo	nebo	k8xC	nebo
blond	blond	k2eAgInPc1d1	blond
vlasy	vlas	k1gInPc1	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
jasně	jasně	k6eAd1	jasně
vyznačeni	vyznačen	k2eAgMnPc1d1	vyznačen
i	i	k9	i
křídly	křídlo	k1gNnPc7	křídlo
umístěnými	umístěný	k2eAgFnPc7d1	umístěná
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
andělů	anděl	k1gMnPc2	anděl
mohou	moct	k5eAaImIp3nP	moct
připomínat	připomínat	k5eAaImF	připomínat
holubí	holubí	k2eAgInPc1d1	holubí
<g/>
,	,	kIx,	,
nevhodná	vhodný	k2eNgNnPc1d1	nevhodné
jsou	být	k5eAaImIp3nP	být
dravčí	dravčí	k2eAgNnPc1d1	dravčí
<g/>
,	,	kIx,	,
hmyzí	hmyzí	k2eAgNnPc1d1	hmyzí
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgNnPc1d1	jiné
křídla	křídlo	k1gNnPc1	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
bývali	bývat	k5eAaImAgMnP	bývat
veselí	veselit	k5eAaImIp3nP	veselit
mladíci	mladík	k1gMnPc1	mladík
převlečení	převlečení	k1gNnSc2	převlečení
za	za	k7c4	za
dívky	dívka	k1gFnPc4	dívka
<g/>
,	,	kIx,	,
oblečené	oblečený	k2eAgFnPc4d1	oblečená
v	v	k7c6	v
bílém	bílý	k2eAgInSc6d1	bílý
splývavém	splývavý	k2eAgInSc6d1	splývavý
kostýmu	kostým	k1gInSc6	kostým
s	s	k7c7	s
maskou	maska	k1gFnSc7	maska
a	a	k8xC	a
nosem	nos	k1gInSc7	nos
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
v	v	k7c6	v
maskách	maska	k1gFnPc6	maska
<g/>
,	,	kIx,	,
podobných	podobný	k2eAgFnPc6d1	podobná
čertům	čert	k1gMnPc3	čert
či	či	k8xC	či
drakům	drak	k1gMnPc3	drak
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
nenosí	nosit	k5eNaImIp3nP	nosit
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
věnec	věnec	k1gInSc4	věnec
se	s	k7c7	s
svíčkami	svíčka	k1gFnPc7	svíčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severských	severský	k2eAgFnPc6d1	severská
zemích	zem	k1gFnPc6	zem
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dívky	dívka	k1gFnPc4	dívka
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
říze	říza	k1gFnSc6	říza
s	s	k7c7	s
věncem	věnec	k1gInSc7	věnec
a	a	k8xC	a
svíčkami	svíčka	k1gFnPc7	svíčka
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
je	být	k5eAaImIp3nS	být
představován	představovat	k5eAaImNgInS	představovat
postavou	postava	k1gFnSc7	postava
obvykle	obvykle	k6eAd1	obvykle
lehce	lehko	k6eAd1	lehko
obézního	obézní	k2eAgMnSc4d1	obézní
muže	muž	k1gMnSc4	muž
v	v	k7c6	v
jasně	jasně	k6eAd1	jasně
červeném	červený	k2eAgNnSc6d1	červené
teplém	teplý	k2eAgNnSc6d1	teplé
oblečení	oblečení	k1gNnSc6	oblečení
se	s	k7c7	s
širokými	široký	k2eAgInPc7d1	široký
bílými	bílý	k2eAgInPc7d1	bílý
lemy	lem	k1gInPc7	lem
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
převisající	převisající	k2eAgFnSc1d1	převisající
čepice	čepice	k1gFnSc1	čepice
<g/>
.	.	kIx.	.
</s>
<s>
Doplňkem	doplněk	k1gInSc7	doplněk
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgFnPc4d1	vysoká
kožené	kožený	k2eAgFnPc4d1	kožená
zimní	zimní	k2eAgFnPc4d1	zimní
boty	bota	k1gFnPc4	bota
a	a	k8xC	a
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgInPc1d1	bílý
vlnité	vlnitý	k2eAgInPc1d1	vlnitý
vlasy	vlas	k1gInPc1	vlas
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgInSc4d2	delší
bílý	bílý	k2eAgInSc4d1	bílý
plnovous	plnovous	k1gInSc4	plnovous
a	a	k8xC	a
huňaté	huňatý	k2eAgNnSc4d1	huňaté
bílé	bílý	k2eAgNnSc4d1	bílé
obočí	obočí	k1gNnSc4	obočí
<g/>
.	.	kIx.	.
</s>
<s>
Postavu	postava	k1gFnSc4	postava
někdy	někdy	k6eAd1	někdy
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
sobí	sobí	k2eAgNnSc1d1	sobí
spřežení	spřežení	k1gNnSc1	spřežení
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
stylizované	stylizovaný	k2eAgInPc1d1	stylizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Maska	maska	k1gFnSc1	maska
Santa	Santa	k1gFnSc1	Santa
Clause	Clause	k1gFnSc1	Clause
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
pomůckou	pomůcka	k1gFnSc7	pomůcka
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
zábavných	zábavný	k2eAgFnPc2d1	zábavná
vánočních	vánoční	k2eAgFnPc2d1	vánoční
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
parodování	parodování	k1gNnSc2	parodování
a	a	k8xC	a
erotických	erotický	k2eAgNnPc2d1	erotické
představení	představení	k1gNnPc2	představení
a	a	k8xC	a
reklamních	reklamní	k2eAgInPc2d1	reklamní
účelů	účel	k1gInPc2	účel
<g/>
.	.	kIx.	.
</s>
<s>
Zpodobnění	zpodobnění	k1gNnSc4	zpodobnění
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
kostýmech	kostým	k1gInPc6	kostým
Santa	Santo	k1gNnSc2	Santo
Clause	Clause	k1gFnSc2	Clause
<g/>
,	,	kIx,	,
veselé	veselý	k2eAgInPc1d1	veselý
žerty	žert	k1gInPc1	žert
nebo	nebo	k8xC	nebo
karikatury	karikatura	k1gFnPc1	karikatura
nejsou	být	k5eNaImIp3nP	být
chápány	chápán	k2eAgFnPc1d1	chápána
jako	jako	k8xC	jako
urážející	urážející	k2eAgFnPc1d1	urážející
<g/>
.	.	kIx.	.
</s>
<s>
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
je	být	k5eAaImIp3nS	být
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
bytost	bytost	k1gFnSc1	bytost
a	a	k8xC	a
nebývá	bývat	k5eNaImIp3nS	bývat
proto	proto	k8xC	proto
nikdy	nikdy	k6eAd1	nikdy
zpodobňován	zpodobňovat	k5eAaImNgMnS	zpodobňovat
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
králové	král	k1gMnPc1	král
jsou	být	k5eAaImIp3nP	být
zpodobňováni	zpodobňován	k2eAgMnPc1d1	zpodobňován
jako	jako	k8xC	jako
postavy	postava	k1gFnPc1	postava
s	s	k7c7	s
korunami	koruna	k1gFnPc7	koruna
a	a	k8xC	a
dary	dar	k1gInPc7	dar
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
také	také	k9	také
s	s	k7c7	s
hvězdou	hvězda	k1gFnSc7	hvězda
na	na	k7c6	na
tyči	tyč	k1gFnSc6	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
obličej	obličej	k1gInSc4	obličej
symbolicky	symbolicky	k6eAd1	symbolicky
nebo	nebo	k8xC	nebo
věrohodně	věrohodně	k6eAd1	věrohodně
nabarven	nabarvit	k5eAaPmNgInS	nabarvit
tmavou	tmavý	k2eAgFnSc7d1	tmavá
nebo	nebo	k8xC	nebo
černou	černý	k2eAgFnSc7d1	černá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
ten	ten	k3xDgMnSc1	ten
černý	černý	k1gMnSc1	černý
vzadu	vzadu	k6eAd1	vzadu
<g/>
"	"	kIx"	"
toto	tento	k3xDgNnSc1	tento
zpodobnění	zpodobnění	k1gNnSc1	zpodobnění
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
rasistický	rasistický	k2eAgInSc4d1	rasistický
podtext	podtext	k1gInSc4	podtext
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
významný	významný	k2eAgInSc4d1	významný
a	a	k8xC	a
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
jako	jako	k8xC	jako
zbylé	zbylý	k2eAgFnPc4d1	zbylá
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Girlanda	girlanda	k1gFnSc1	girlanda
<g/>
.	.	kIx.	.
</s>
<s>
Girlandy	girlanda	k1gFnPc1	girlanda
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
věncoví	věncoví	k1gNnSc2	věncoví
<g/>
)	)	kIx)	)
bývaly	bývat	k5eAaImAgFnP	bývat
častou	častý	k2eAgFnSc7d1	častá
vánoční	vánoční	k2eAgFnSc7d1	vánoční
ozdobou	ozdoba	k1gFnSc7	ozdoba
exteriéru	exteriér	k1gInSc2	exteriér
i	i	k8xC	i
interiéru	interiér	k1gInSc2	interiér
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
girlandy	girlanda	k1gFnPc1	girlanda
se	se	k3xPyFc4	se
dělaly	dělat	k5eAaImAgFnP	dělat
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
chvojí	chvojí	k1gNnSc2	chvojí
nebo	nebo	k8xC	nebo
z	z	k7c2	z
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
namotáváním	namotávání	k1gNnSc7	namotávání
materiálu	materiál	k1gInSc2	materiál
na	na	k7c4	na
šňůru	šňůra	k1gFnSc4	šňůra
<g/>
,	,	kIx,	,
lano	lano	k1gNnSc4	lano
nebo	nebo	k8xC	nebo
drát	drát	k1gInSc4	drát
<g/>
.	.	kIx.	.
</s>
<s>
Girlandy	girlanda	k1gFnPc1	girlanda
jsou	být	k5eAaImIp3nP	být
výrazným	výrazný	k2eAgInSc7d1	výrazný
estetickým	estetický	k2eAgInSc7d1	estetický
doplňkem	doplněk	k1gInSc7	doplněk
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
použití	použití	k1gNnSc4	použití
takových	takový	k3xDgFnPc2	takový
ozdob	ozdoba	k1gFnPc2	ozdoba
je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
zvážit	zvážit	k5eAaPmF	zvážit
a	a	k8xC	a
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
výběr	výběr	k1gInSc4	výběr
materiálu	materiál	k1gInSc2	materiál
záměru	záměr	k1gInSc2	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Svícny	svícen	k1gInPc1	svícen
jsou	být	k5eAaImIp3nP	být
určeny	určen	k2eAgInPc1d1	určen
jako	jako	k8xC	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
hrobu	hrob	k1gInSc2	hrob
nebo	nebo	k8xC	nebo
interiéru	interiér	k1gInSc2	interiér
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
a	a	k8xC	a
konvence	konvence	k1gFnSc1	konvence
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zohlednit	zohlednit	k5eAaPmF	zohlednit
způsob	způsob	k1gInSc4	způsob
provedení	provedení	k1gNnSc2	provedení
a	a	k8xC	a
dekoraci	dekorace	k1gFnSc4	dekorace
svícnu	svícen	k1gInSc2	svícen
<g/>
.	.	kIx.	.
</s>
<s>
Dekorace	dekorace	k1gFnSc1	dekorace
andělské	andělský	k2eAgNnSc1d1	andělské
zvonění	zvonění	k1gNnSc1	zvonění
(	(	kIx(	(
<g/>
také	také	k9	také
jako	jako	k9	jako
andělské	andělský	k2eAgInPc4d1	andělský
zvonky	zvonek	k1gInPc4	zvonek
<g/>
,	,	kIx,	,
vánoční	vánoční	k2eAgInSc4d1	vánoční
kolotoč	kolotoč	k1gInSc4	kolotoč
nebo	nebo	k8xC	nebo
také	také	k9	také
vánoční	vánoční	k2eAgFnSc1d1	vánoční
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
konstrukce	konstrukce	k1gFnSc1	konstrukce
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
vlivem	vliv	k1gInSc7	vliv
proudění	proudění	k1gNnSc2	proudění
teplého	teplý	k2eAgInSc2d1	teplý
vzduchu	vzduch	k1gInSc2	vzduch
od	od	k7c2	od
zapálených	zapálený	k2eAgFnPc2d1	zapálená
svíček	svíčka	k1gFnPc2	svíčka
přes	přes	k7c4	přes
listy	list	k1gInPc4	list
vrtule	vrtule	k1gFnSc2	vrtule
upevněné	upevněný	k2eAgFnSc2d1	upevněná
na	na	k7c6	na
hřídeli	hřídel	k1gFnSc6	hřídel
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
konstrukce	konstrukce	k1gFnSc2	konstrukce
bývá	bývat	k5eAaImIp3nS	bývat
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
zvukem	zvuk	k1gInSc7	zvuk
<g/>
,	,	kIx,	,
cinkáním	cinkání	k1gNnSc7	cinkání
visících	visící	k2eAgFnPc2d1	visící
pohybujících	pohybující	k2eAgFnPc2d1	pohybující
se	se	k3xPyFc4	se
kovových	kovový	k2eAgFnPc2d1	kovová
částí	část	k1gFnPc2	část
o	o	k7c4	o
pevné	pevný	k2eAgFnPc4d1	pevná
kovové	kovový	k2eAgFnPc4d1	kovová
části	část	k1gFnPc4	část
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
konstrukcí	konstrukce	k1gFnPc2	konstrukce
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pouze	pouze	k6eAd1	pouze
vrtule	vrtule	k1gFnSc1	vrtule
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
zavěšeny	zavěšen	k2eAgInPc4d1	zavěšen
cinkající	cinkající	k2eAgInPc4d1	cinkající
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
tradicí	tradice	k1gFnSc7	tradice
vytvářet	vytvářet	k5eAaImF	vytvářet
velké	velká	k1gFnPc4	velká
bohatě	bohatě	k6eAd1	bohatě
zdobené	zdobený	k2eAgFnPc4d1	zdobená
dekorace	dekorace	k1gFnPc4	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
byl	být	k5eAaImAgInS	být
patentován	patentovat	k5eAaBmNgInS	patentovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Prskavka	prskavka	k1gFnSc1	prskavka
<g/>
.	.	kIx.	.
</s>
<s>
Prskavka	prskavka	k1gFnSc1	prskavka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
zábavní	zábavní	k2eAgFnSc2d1	zábavní
pyrotechniky	pyrotechnika	k1gFnSc2	pyrotechnika
<g/>
,	,	kIx,	,
používané	používaný	k2eAgFnSc2d1	používaná
během	během	k7c2	během
oslavy	oslava	k1gFnSc2	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
v	v	k7c6	v
ČR	ČR	kA	ČR
běžně	běžně	k6eAd1	běžně
jako	jako	k8xS	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hoření	hoření	k2eAgFnSc2d1	hoření
pyrotechniky	pyrotechnika	k1gFnSc2	pyrotechnika
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
chemické	chemický	k2eAgFnSc3d1	chemická
reakci	reakce	k1gFnSc3	reakce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikají	vznikat	k5eAaImIp3nP	vznikat
drobné	drobný	k2eAgFnPc4d1	drobná
hořící	hořící	k2eAgFnPc4d1	hořící
částečky	částečka	k1gFnPc4	částečka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
části	část	k1gFnPc1	část
odpadávají	odpadávat	k5eAaImIp3nP	odpadávat
od	od	k7c2	od
vlastního	vlastní	k2eAgNnSc2d1	vlastní
tělesa	těleso	k1gNnSc2	těleso
prskavky	prskavka	k1gFnSc2	prskavka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
vizuální	vizuální	k2eAgInSc4d1	vizuální
vjem	vjem	k1gInSc4	vjem
odpadávajících	odpadávající	k2eAgFnPc2d1	odpadávající
jisker	jiskra	k1gFnPc2	jiskra
<g/>
.	.	kIx.	.
</s>
<s>
Prodávány	prodáván	k2eAgFnPc1d1	prodávána
jsou	být	k5eAaImIp3nP	být
prskavky	prskavka	k1gFnPc1	prskavka
různé	různý	k2eAgFnSc2d1	různá
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
barvy	barva	k1gFnSc2	barva
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
domů	dům	k1gInPc2	dům
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ozdobeno	ozdoben	k2eAgNnSc1d1	ozdobeno
světly	světlo	k1gNnPc7	světlo
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
osvětlené	osvětlený	k2eAgFnPc1d1	osvětlená
saně	saně	k1gFnPc1	saně
se	s	k7c7	s
soby	sob	k1gMnPc7	sob
<g/>
,	,	kIx,	,
sněhuláci	sněhulák	k1gMnPc1	sněhulák
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
vánoční	vánoční	k2eAgFnPc1d1	vánoční
postavy	postava	k1gFnPc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
tradiční	tradiční	k2eAgFnPc4d1	tradiční
dekorace	dekorace	k1gFnPc4	dekorace
patří	patřit	k5eAaImIp3nS	patřit
zvonky	zvonek	k1gInPc4	zvonek
<g/>
,	,	kIx,	,
svíčky	svíčka	k1gFnPc4	svíčka
<g/>
,	,	kIx,	,
cukrové	cukrový	k2eAgFnPc4d1	cukrová
homole	homole	k1gFnPc4	homole
<g/>
,	,	kIx,	,
punčochy	punčocha	k1gFnPc4	punčocha
<g/>
,	,	kIx,	,
věnce	věnec	k1gInPc4	věnec
a	a	k8xC	a
anděly	anděl	k1gMnPc4	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Zavěšené	zavěšený	k2eAgInPc4d1	zavěšený
věnce	věnec	k1gInPc4	věnec
a	a	k8xC	a
svíčky	svíčka	k1gFnPc4	svíčka
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
okně	okno	k1gNnSc6	okno
jsou	být	k5eAaImIp3nP	být
nejtradičnější	tradiční	k2eAgInPc4d3	nejtradičnější
vánoční	vánoční	k2eAgInPc4d1	vánoční
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
svíčky	svíčka	k1gFnPc1	svíčka
mají	mít	k5eAaImIp3nP	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
křesťanů	křesťan	k1gMnPc2	křesťan
světlem	světlo	k1gNnSc7	světlo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
a	a	k8xC	a
v	v	k7c6	v
nákupních	nákupní	k2eAgNnPc6d1	nákupní
centrech	centrum	k1gNnPc6	centrum
použití	použití	k1gNnSc3	použití
vánočního	vánoční	k2eAgNnSc2d1	vánoční
osvětlení	osvětlení	k1gNnSc2	osvětlení
a	a	k8xC	a
dekorací	dekorace	k1gFnPc2	dekorace
zavěšených	zavěšený	k2eAgInPc2d1	zavěšený
podél	podél	k7c2	podél
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
pouštění	pouštění	k1gNnSc4	pouštění
vánoční	vánoční	k2eAgFnSc2d1	vánoční
hudby	hudba	k1gFnSc2	hudba
z	z	k7c2	z
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
,	,	kIx,	,
umístění	umístění	k1gNnSc1	umístění
vánočních	vánoční	k2eAgInPc2d1	vánoční
stromků	stromek	k1gInPc2	stromek
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
způsob	způsob	k1gInSc4	způsob
navození	navození	k1gNnSc2	navození
vánoční	vánoční	k2eAgFnPc4d1	vánoční
nálady	nálada	k1gFnPc4	nálada
vhodné	vhodný	k2eAgFnPc1d1	vhodná
k	k	k7c3	k
nákupům	nákup	k1gInPc3	nákup
dárků	dárek	k1gInPc2	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
pestrobarevných	pestrobarevný	k2eAgFnPc2d1	pestrobarevná
papíru	papír	k1gInSc2	papír
se	s	k7c7	s
sekulárními	sekulární	k2eAgInPc7d1	sekulární
či	či	k8xC	či
náboženskými	náboženský	k2eAgInPc7d1	náboženský
vánočními	vánoční	k2eAgInPc7d1	vánoční
motivy	motiv	k1gInPc7	motiv
jsou	být	k5eAaImIp3nP	být
vyrobeny	vyroben	k2eAgInPc1d1	vyroben
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
balení	balení	k1gNnSc2	balení
dárků	dárek	k1gInPc2	dárek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
jsou	být	k5eAaImIp3nP	být
vánoční	vánoční	k2eAgFnPc1d1	vánoční
ozdoby	ozdoba	k1gFnPc1	ozdoba
tradičně	tradičně	k6eAd1	tradičně
sklízeny	sklízen	k2eAgFnPc1d1	sklízena
na	na	k7c4	na
Večer	večer	k1gInSc4	večer
tříkrálový	tříkrálový	k2eAgInSc4d1	tříkrálový
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Paradeisl	Paradeisl	k1gInSc1	Paradeisl
<g/>
.	.	kIx.	.
</s>
<s>
Paradeisl	Paradeisl	k1gInSc1	Paradeisl
nebo	nebo	k8xC	nebo
také	také	k9	také
Paradeiser	Paradeiser	k1gInSc1	Paradeiser
<g/>
,	,	kIx,	,
Paradeiserl	Paradeiserl	k1gInSc1	Paradeiserl
nebo	nebo	k8xC	nebo
Klausbaum	Klausbaum	k1gInSc1	Klausbaum
je	být	k5eAaImIp3nS	být
adventní	adventní	k2eAgFnSc1d1	adventní
i	i	k8xC	i
vánoční	vánoční	k2eAgFnSc1d1	vánoční
dekorace	dekorace	k1gFnSc1	dekorace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
svátečního	sváteční	k2eAgInSc2d1	sváteční
stolu	stol	k1gInSc2	stol
a	a	k8xC	a
při	při	k7c6	při
modlitbách	modlitba	k1gFnPc6	modlitba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
především	především	k9	především
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Paradeisl	Paradeisl	k1gInSc1	Paradeisl
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
adventního	adventní	k2eAgInSc2d1	adventní
věnce	věnec	k1gInSc2	věnec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
katolickou	katolický	k2eAgFnSc4d1	katolická
verzi	verze	k1gFnSc4	verze
protestantského	protestantský	k2eAgInSc2d1	protestantský
adventního	adventní	k2eAgInSc2d1	adventní
věnce	věnec	k1gInSc2	věnec
<g/>
.	.	kIx.	.
</s>
<s>
Adventních	adventní	k2eAgInPc2d1	adventní
věnce	věnec	k1gInPc4	věnec
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
používány	používán	k2eAgInPc1d1	používán
při	při	k7c6	při
slavení	slavení	k1gNnSc6	slavení
svátků	svátek	k1gInPc2	svátek
protestanty	protestant	k1gMnPc7	protestant
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
paradeislu	paradeisl	k1gInSc2	paradeisl
je	být	k5eAaImIp3nS	být
původ	původ	k1gInSc1	původ
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
známa	znám	k2eAgFnSc1d1	známa
forma	forma	k1gFnSc1	forma
ozdoby	ozdoba	k1gFnSc2	ozdoba
paradeisl	paradeisl	k1gInSc1	paradeisl
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
České	český	k2eAgFnPc1d1	Česká
Vánoce	Vánoce	k1gFnPc1	Vánoce
popisována	popisován	k2eAgMnSc4d1	popisován
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
zahrádka	zahrádka	k1gFnSc1	zahrádka
<g/>
"	"	kIx"	"
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
jablíček	jablíčko	k1gNnPc2	jablíčko
umístěny	umístit	k5eAaPmNgFnP	umístit
svíčky	svíčka	k1gFnPc1	svíčka
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
hezký	hezký	k2eAgInSc4d1	hezký
svícen	svícen	k1gInSc4	svícen
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
ozdoby	ozdoba	k1gFnPc1	ozdoba
byly	být	k5eAaImAgFnP	být
časté	častý	k2eAgFnPc1d1	častá
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
přímo	přímo	k6eAd1	přímo
vánoční	vánoční	k2eAgFnSc1d1	vánoční
hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgFnPc4d1	Latinská
písně	píseň	k1gFnPc4	píseň
jako	jako	k8xC	jako
Veni	Ven	k1gFnPc4	Ven
Redemptor	Redemptor	k1gInSc4	Redemptor
gentium	gentium	k1gNnSc1	gentium
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Ambrož	Ambrož	k1gMnSc1	Ambrož
Milánský	milánský	k2eAgMnSc1d1	milánský
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
strohými	strohý	k2eAgInPc7d1	strohý
prohlášeními	prohlášení	k1gNnPc7	prohlášení
teologické	teologický	k2eAgFnPc1d1	teologická
doktríny	doktrína	k1gFnSc2	doktrína
vtělení	vtělení	k1gNnSc2	vtělení
vymezující	vymezující	k2eAgFnSc1d1	vymezující
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
tehdy	tehdy	k6eAd1	tehdy
rozšířenému	rozšířený	k2eAgNnSc3d1	rozšířené
ariánství	ariánství	k1gNnSc3	ariánství
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
Corde	Cord	k1gInSc5	Cord
natus	natus	k1gMnSc1	natus
ex	ex	k6eAd1	ex
Parentis	Parentis	k1gFnSc4	Parentis
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
složil	složit	k5eAaPmAgMnS	složit
španělský	španělský	k2eAgMnSc1d1	španělský
básník	básník	k1gMnSc1	básník
Prudentius	Prudentius	k1gMnSc1	Prudentius
(	(	kIx(	(
<g/>
†	†	k?	†
kolem	kolem	k7c2	kolem
410	[number]	k4	410
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
zpívána	zpívat	k5eAaImNgFnS	zpívat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
církvích	církev	k1gFnPc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
vánoční	vánoční	k2eAgFnPc1d1	vánoční
písně	píseň	k1gFnPc1	píseň
předváděny	předvádět	k5eAaImNgFnP	předvádět
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
klášterech	klášter	k1gInPc6	klášter
a	a	k8xC	a
rozvíjeny	rozvíjen	k2eAgFnPc4d1	rozvíjena
Bernardem	Bernard	k1gMnSc7	Bernard
z	z	k7c2	z
Clairvaux	Clairvaux	k1gInSc4	Clairvaux
v	v	k7c6	v
rýmovaných	rýmovaný	k2eAgFnPc6d1	rýmovaná
slokách	sloka	k1gFnPc6	sloka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pařížský	pařížský	k2eAgMnSc1d1	pařížský
mnich	mnich	k1gMnSc1	mnich
Adam	Adam	k1gMnSc1	Adam
St.	st.	kA	st.
Victor	Victor	k1gMnSc1	Victor
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
hudbu	hudba	k1gFnSc4	hudba
z	z	k7c2	z
populárních	populární	k2eAgFnPc2d1	populární
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
tak	tak	k6eAd1	tak
již	již	k6eAd1	již
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
hudbu	hudba	k1gFnSc4	hudba
blízkou	blízký	k2eAgFnSc4d1	blízká
tradičním	tradiční	k2eAgFnPc3d1	tradiční
vánočním	vánoční	k2eAgFnPc3d1	vánoční
koledám	koleda	k1gFnPc3	koleda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Františka	František	k1gMnSc2	František
z	z	k7c2	z
Assisi	Assise	k1gFnSc6	Assise
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
a	a	k8xC	a
rozvíjena	rozvíjet	k5eAaImNgFnS	rozvíjet
tradice	tradice	k1gFnSc1	tradice
zpěvu	zpěv	k1gInSc2	zpěv
populárních	populární	k2eAgFnPc2d1	populární
vánočních	vánoční	k2eAgFnPc2d1	vánoční
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
koledy	koleda	k1gFnPc1	koleda
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1426	[number]	k4	1426
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
koledy	koleda	k1gFnPc1	koleda
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
komunální	komunální	k2eAgFnPc1d1	komunální
lidové	lidový	k2eAgFnPc1d1	lidová
písně	píseň	k1gFnPc1	píseň
zpívané	zpívaný	k2eAgFnPc1d1	zpívaná
při	při	k7c6	při
oslavách	oslava	k1gFnPc6	oslava
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
dožínky	dožínek	k1gInPc1	dožínek
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
koledy	koleda	k1gFnPc1	koleda
začaly	začít	k5eAaPmAgFnP	začít
zpívat	zpívat	k5eAaImF	zpívat
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
jsou	být	k5eAaImIp3nP	být
koledy	koleda	k1gFnPc4	koleda
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
středověkých	středověký	k2eAgInPc6d1	středověký
akordech	akord	k1gInPc6	akord
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
jim	on	k3xPp3gMnPc3	on
dávají	dávat	k5eAaImIp3nP	dávat
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
hudební	hudební	k2eAgInSc4d1	hudební
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
některých	některý	k3yIgFnPc2	některý
koled	koleda	k1gFnPc2	koleda
jako	jako	k8xC	jako
Personent	Personent	k1gInSc1	Personent
hodie	hodie	k1gFnSc2	hodie
<g/>
,	,	kIx,	,
Good	Good	k1gMnSc1	Good
King	King	k1gMnSc1	King
Wenceslas	Wenceslas	k1gMnSc1	Wenceslas
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
<g/>
)	)	kIx)	)
a	a	k8xC	a
Holly	Holla	k1gFnPc4	Holla
a	a	k8xC	a
Ivy	Iva	k1gFnPc4	Iva
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaPmF	vysledovat
až	až	k9	až
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
hudební	hudební	k2eAgNnPc1d1	hudební
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnPc4d3	nejstarší
hudební	hudební	k2eAgFnPc4d1	hudební
skladby	skladba	k1gFnPc4	skladba
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
pravidelně	pravidelně	k6eAd1	pravidelně
zpívány	zpíván	k2eAgFnPc1d1	zpívána
<g/>
.	.	kIx.	.
</s>
<s>
Adeste	Adesit	k5eAaImRp2nP	Adesit
fideles	fideles	k1gInSc4	fideles
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
slova	slovo	k1gNnPc1	slovo
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
již	již	k6eAd1	již
v	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
reformaci	reformace	k1gFnSc6	reformace
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
zpívání	zpívání	k1gNnSc2	zpívání
koled	koleda	k1gFnPc2	koleda
zpočátku	zpočátku	k6eAd1	zpočátku
upadlo	upadnout	k5eAaPmAgNnS	upadnout
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
reformátoři	reformátor	k1gMnPc1	reformátor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
,	,	kIx,	,
však	však	k9	však
zpěv	zpěv	k1gInSc1	zpěv
koled	koleda	k1gFnPc2	koleda
podporovali	podporovat	k5eAaImAgMnP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Koledy	koleda	k1gFnPc1	koleda
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
přežily	přežít	k5eAaPmAgFnP	přežít
ve	v	k7c6	v
venkovských	venkovský	k2eAgFnPc6d1	venkovská
komunitách	komunita	k1gFnPc6	komunita
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
lidové	lidový	k2eAgFnPc4d1	lidová
písně	píseň	k1gFnPc4	píseň
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
anglický	anglický	k2eAgMnSc1d1	anglický
reformátor	reformátor	k1gMnSc1	reformátor
Charles	Charles	k1gMnSc1	Charles
Wesley	Weslea	k1gFnSc2	Weslea
pochopil	pochopit	k5eAaPmAgMnS	pochopit
význam	význam	k1gInSc4	význam
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
náboženství	náboženství	k1gNnSc6	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
žalmů	žalm	k1gInPc2	žalm
na	na	k7c4	na
melodie	melodie	k1gFnPc4	melodie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
náboženská	náboženský	k2eAgNnPc4d1	náboženské
hnutí	hnutí	k1gNnPc4	hnutí
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
texty	text	k1gInPc4	text
pro	pro	k7c4	pro
nejméně	málo	k6eAd3	málo
tři	tři	k4xCgFnPc4	tři
vánoční	vánoční	k2eAgFnPc4d1	vánoční
koledy	koleda	k1gFnPc4	koleda
<g/>
.	.	kIx.	.
</s>
<s>
Felix	Felix	k1gMnSc1	Felix
Mendelssohn-Bartholdy	Mendelssohn-Bartholda	k1gFnSc2	Mendelssohn-Bartholda
napsal	napsat	k5eAaBmAgMnS	napsat
melodii	melodie	k1gFnSc4	melodie
přizpůsobenou	přizpůsobený	k2eAgFnSc4d1	přizpůsobená
Wesleyho	Wesleyha	k1gFnSc5	Wesleyha
slovům	slovo	k1gNnPc3	slovo
koledy	koleda	k1gFnSc2	koleda
Hark	Hark	k1gMnSc1	Hark
<g/>
!	!	kIx.	!
</s>
<s>
the	the	k?	the
Herald	Herald	k1gInSc1	Herald
Angels	Angelsa	k1gFnPc2	Angelsa
Sing	Singa	k1gFnPc2	Singa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
Mohr	Mohra	k1gFnPc2	Mohra
a	a	k8xC	a
Gruber	Gruber	k1gMnSc1	Gruber
složili	složit	k5eAaPmAgMnP	složit
píseň	píseň	k1gFnSc4	píseň
Stille	Stille	k1gNnSc2	Stille
Nacht	Nachta	k1gFnPc2	Nachta
(	(	kIx(	(
<g/>
Tichá	Tichá	k1gFnSc1	Tichá
noc	noc	k1gFnSc1	noc
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
kostel	kostel	k1gInSc4	kostel
v	v	k7c6	v
Oberndorfu	Oberndorf	k1gInSc6	Oberndorf
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
Williama	William	k1gMnSc2	William
Sandyse	Sandyse	k1gFnSc2	Sandyse
Christmas	Christmas	k1gMnSc1	Christmas
Carols	Carolsa	k1gFnPc2	Carolsa
Ancient	Ancient	k1gMnSc1	Ancient
and	and	k?	and
Modern	Modern	k1gMnSc1	Modern
(	(	kIx(	(
<g/>
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
koledy	koleda	k1gFnPc1	koleda
starověké	starověký	k2eAgFnPc1d1	starověká
a	a	k8xC	a
moderní	moderní	k2eAgFnPc1d1	moderní
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
tištěnou	tištěný	k2eAgFnSc7d1	tištěná
verzí	verze	k1gFnSc7	verze
mnoha	mnoho	k4c2	mnoho
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
klasických	klasický	k2eAgFnPc2d1	klasická
anglických	anglický	k2eAgFnPc2d1	anglická
koled	koleda	k1gFnPc2	koleda
a	a	k8xC	a
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
viktoriánského	viktoriánský	k2eAgInSc2d1	viktoriánský
svátku	svátek	k1gInSc2	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
světské	světský	k2eAgFnPc4d1	světská
vánoční	vánoční	k2eAgFnPc4d1	vánoční
písně	píseň	k1gFnPc4	píseň
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Deck	Deck	k1gInSc1	Deck
The	The	k1gMnSc1	The
Halls	Halls	k1gInSc1	Halls
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
píseň	píseň	k1gFnSc1	píseň
Jingle	Jingle	k1gFnSc2	Jingle
Bells	Bellsa	k1gFnPc2	Bellsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
mnoha	mnoho	k4c2	mnoho
verzí	verze	k1gFnPc2	verze
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
autorsky	autorsky	k6eAd1	autorsky
chráněna	chránit	k5eAaImNgFnS	chránit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
africké	africký	k2eAgInPc1d1	africký
americké	americký	k2eAgInPc1d1	americký
spirituály	spirituál	k1gInPc1	spirituál
a	a	k8xC	a
písně	píseň	k1gFnPc1	píseň
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
tradici	tradice	k1gFnSc6	tradice
spirituálů	spirituál	k1gInPc2	spirituál
<g/>
,	,	kIx,	,
staly	stát	k5eAaPmAgFnP	stát
šířeji	šířej	k1gMnPc7	šířej
známými	známý	k2eAgMnPc7d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
písní	píseň	k1gFnPc2	píseň
vztahujících	vztahující	k2eAgFnPc2d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
byl	být	k5eAaImAgInS	být
komerčně	komerčně	k6eAd1	komerčně
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
v	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jazzových	jazzový	k2eAgFnPc2d1	jazzová
a	a	k8xC	a
bluesových	bluesový	k2eAgFnPc2d1	bluesová
variant	varianta	k1gFnPc2	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
přišlo	přijít	k5eAaPmAgNnS	přijít
oživení	oživení	k1gNnSc1	oživení
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
starou	starý	k2eAgFnSc4d1	stará
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
skupiny	skupina	k1gFnSc2	skupina
zpěváků	zpěvák	k1gMnPc2	zpěvák
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
interpretů	interpret	k1gMnPc2	interpret
raně	raně	k6eAd1	raně
středověké	středověký	k2eAgFnSc2d1	středověká
a	a	k8xC	a
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgFnPc4d1	vánoční
písně	píseň	k1gFnPc4	píseň
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
romantikou	romantika	k1gFnSc7	romantika
a	a	k8xC	a
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
tematikou	tematika	k1gFnSc7	tematika
jsou	být	k5eAaImIp3nP	být
častým	častý	k2eAgInSc7d1	častý
terčem	terč	k1gInSc7	terč
zesměšňování	zesměšňování	k1gNnSc2	zesměšňování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečných	závěrečný	k2eAgFnPc6d1	závěrečná
sekvencích	sekvence	k1gFnPc6	sekvence
filmu	film	k1gInSc2	film
Monty	Monta	k1gFnSc2	Monta
Python	Python	k1gMnSc1	Python
<g/>
:	:	kIx,	:
Smysl	smysl	k1gInSc1	smysl
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
scénu	scéna	k1gFnSc4	scéna
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Graham	Graham	k1gMnSc1	Graham
Chapman	Chapman	k1gMnSc1	Chapman
pěje	pět	k5eAaImIp3nS	pět
píseň	píseň	k1gFnSc4	píseň
Vánoce	Vánoce	k1gFnPc1	Vánoce
v	v	k7c6	v
nebi	nebe	k1gNnSc6	nebe
s	s	k7c7	s
Třemi	tři	k4xCgInPc7	tři
králi	král	k1gMnPc7	král
vyzbrojenými	vyzbrojený	k2eAgInPc7d1	vyzbrojený
nákupními	nákupní	k2eAgInPc7d1	nákupní
košíky	košík	k1gInPc7	košík
ze	z	k7c2	z
supermarketů	supermarket	k1gInPc2	supermarket
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
vánočním	vánoční	k2eAgInSc7d1	vánoční
hitem	hit	k1gInSc7	hit
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
zisků	zisk	k1gInPc2	zisk
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
nosičů	nosič	k1gInPc2	nosič
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
White	Whit	k1gInSc5	Whit
Christmas	Christmasa	k1gFnPc2	Christmasa
(	(	kIx(	(
<g/>
Bílé	bílý	k2eAgFnPc1d1	bílá
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
)	)	kIx)	)
od	od	k7c2	od
Binga	bingo	k1gNnSc2	bingo
Crosbyho	Crosby	k1gMnSc2	Crosby
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc4	píseň
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
50	[number]	k4	50
miliónů	milión	k4xCgInPc2	milión
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
coververzemi	coververze	k1gFnPc7	coververze
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
hodně	hodně	k6eAd1	hodně
přes	přes	k7c4	přes
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
určené	určený	k2eAgFnPc1d1	určená
jako	jako	k9	jako
koledy	koleda	k1gFnPc1	koleda
pro	pro	k7c4	pro
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
na	na	k7c4	na
album	album	k1gNnSc4	album
vydané	vydaný	k2eAgNnSc4d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Enya	Enyum	k1gNnSc2	Enyum
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
jako	jako	k8xC	jako
průměrné	průměrný	k2eAgFnPc1d1	průměrná
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
vánoční	vánoční	k2eAgFnSc6d1	vánoční
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
v	v	k7c6	v
takzvaných	takzvaný	k2eAgFnPc6d1	takzvaná
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
chválách	chvála	k1gFnPc6	chvála
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
až	až	k9	až
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
záznamy	záznam	k1gInPc1	záznam
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
rukopisu	rukopis	k1gInSc6	rukopis
z	z	k7c2	z
knihovny	knihovna	k1gFnSc2	knihovna
Metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
kapituly	kapitula	k1gFnSc2	kapitula
pražské	pražský	k2eAgFnSc2d1	Pražská
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
vánoční	vánoční	k2eAgFnPc1d1	vánoční
koledy	koleda	k1gFnPc1	koleda
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měly	mít	k5eAaImAgFnP	mít
podobu	podoba	k1gFnSc4	podoba
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
době	doba	k1gFnSc6	doba
adventu	advent	k1gInSc2	advent
byly	být	k5eAaImAgFnP	být
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
písně	píseň	k1gFnPc1	píseň
s	s	k7c7	s
náboženskými	náboženský	k2eAgInPc7d1	náboženský
motivy	motiv	k1gInPc7	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
některé	některý	k3yIgFnPc1	některý
náboženské	náboženský	k2eAgFnPc1d1	náboženská
písně	píseň	k1gFnPc1	píseň
zlidověly	zlidovět	k5eAaPmAgFnP	zlidovět
do	do	k7c2	do
podob	podoba	k1gFnPc2	podoba
koledy	koleda	k1gFnSc2	koleda
<g/>
.	.	kIx.	.
</s>
<s>
Potulní	potulný	k2eAgMnPc1d1	potulný
žáci	žák	k1gMnPc1	žák
si	se	k3xPyFc3	se
koledami	koleda	k1gFnPc7	koleda
přivydělávali	přivydělávat	k5eAaImAgMnP	přivydělávat
a	a	k8xC	a
šířili	šířit	k5eAaImAgMnP	šířit
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pochází	pocházet	k5eAaImIp3nS	pocházet
píseň	píseň	k1gFnSc1	píseň
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jest	být	k5eAaImIp3nS	být
věc	věc	k1gFnSc1	věc
divná	divný	k2eAgFnSc1d1	divná
<g/>
,	,	kIx,	,
Panna	Panna	k1gFnSc1	Panna
syna	syn	k1gMnSc2	syn
porodila	porodit	k5eAaPmAgFnS	porodit
<g/>
,	,	kIx,	,
beze	beze	k7c2	beze
vší	všecek	k3xTgFnSc2	všecek
strasti	strast	k1gFnSc2	strast
tělesné	tělesný	k2eAgInPc1d1	tělesný
(	(	kIx(	(
<g/>
zápis	zápis	k1gInSc1	zápis
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
v	v	k7c6	v
Kodexu	kodex	k1gInSc6	kodex
vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
Emanuel	Emanuel	k1gMnSc1	Emanuel
(	(	kIx(	(
<g/>
rukopis	rukopis	k1gInSc1	rukopis
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Kozla	Kozel	k1gMnSc2	Kozel
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ježíš	ježit	k5eAaImIp2nS	ježit
náš	náš	k3xOp1gMnSc1	náš
spasitel	spasitel	k1gMnSc1	spasitel
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc3	století
Jednota	jednota	k1gFnSc1	jednota
bratrská	bratrský	k2eAgFnSc1d1	bratrská
dala	dát	k5eAaPmAgFnS	dát
vytisknout	vytisknout	k5eAaPmF	vytisknout
kancionály	kancionál	k1gInPc4	kancionál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mezi	mezi	k7c7	mezi
mnoha	mnoho	k4c7	mnoho
písněmi	píseň	k1gFnPc7	píseň
k	k	k7c3	k
adventu	advent	k1gInSc3	advent
a	a	k8xC	a
Vánocům	Vánoce	k1gFnPc3	Vánoce
byla	být	k5eAaImAgFnS	být
i	i	k9	i
píseň	píseň	k1gFnSc1	píseň
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
Kristus	Kristus	k1gMnSc1	Kristus
Pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
veselme	veselit	k5eAaImRp1nP	veselit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
renesance	renesance	k1gFnSc2	renesance
vzniká	vznikat	k5eAaImIp3nS	vznikat
dílo	dílo	k1gNnSc1	dílo
Jana	Jan	k1gMnSc2	Jan
Campana	Campan	k1gMnSc2	Campan
Vodňanského	vodňanský	k2eAgMnSc2d1	vodňanský
Panna	Panna	k1gFnSc1	Panna
syna	syn	k1gMnSc2	syn
porodila	porodit	k5eAaPmAgFnS	porodit
<g/>
.	.	kIx.	.
</s>
<s>
Vánočním	vánoční	k2eAgInPc3d1	vánoční
dílům	díl	k1gInPc3	díl
české	český	k2eAgFnSc2d1	Česká
hudebního	hudební	k2eAgNnSc2d1	hudební
baroka	baroko	k1gNnSc2	baroko
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
Adam	Adam	k1gMnSc1	Adam
Václav	Václav	k1gMnSc1	Václav
Michna	Michna	k1gMnSc1	Michna
z	z	k7c2	z
Otradovic	Otradovice	k1gFnPc2	Otradovice
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
i	i	k8xC	i
autorem	autor	k1gMnSc7	autor
písně	píseň	k1gFnSc2	píseň
Chtíc	chtíc	k6eAd1	chtíc
aby	aby	kYmCp3nS	aby
spal	spát	k5eAaImAgMnS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
období	období	k1gNnSc2	období
baroka	baroko	k1gNnSc2	baroko
spadá	spadat	k5eAaPmIp3nS	spadat
i	i	k9	i
vznik	vznik	k1gInSc1	vznik
českých	český	k2eAgFnPc2d1	Česká
pastorel	pastorela	k1gFnPc2	pastorela
–	–	k?	–
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
sóla	sólo	k1gNnPc4	sólo
a	a	k8xC	a
sbor	sbor	k1gInSc4	sbor
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pastorely	pastorela	k1gFnPc4	pastorela
patří	patřit	k5eAaImIp3nS	patřit
dílo	dílo	k1gNnSc4	dílo
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Poddaného	poddaný	k1gMnSc2	poddaný
z	z	k7c2	z
Rožmitálu	Rožmitál	k1gInSc2	Rožmitál
Sem	sem	k6eAd1	sem
<g/>
,	,	kIx,	,
sem	sem	k6eAd1	sem
pastýři	pastýř	k1gMnPc1	pastýř
nebo	nebo	k8xC	nebo
Josefa	Josef	k1gMnSc2	Josef
Antona	Anton	k1gMnSc2	Anton
Sehlinga	Sehling	k1gMnSc2	Sehling
Přeju	přát	k5eAaImIp1nS	přát
tobě	ty	k3xPp2nSc3	ty
dobrou	dobrý	k2eAgFnSc4d1	dobrá
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
baroku	barok	k1gInSc6	barok
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
i	i	k9	i
žánr	žánr	k1gInSc4	žánr
vánoční	vánoční	k2eAgFnSc2d1	vánoční
mše	mše	k1gFnSc2	mše
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgNnSc7d3	nejznámější
hudebním	hudební	k2eAgNnSc7d1	hudební
ztvárněním	ztvárnění	k1gNnSc7	ztvárnění
vánoční	vánoční	k2eAgFnSc2d1	vánoční
mše	mše	k1gFnSc2	mše
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
Česká	český	k2eAgFnSc1d1	Česká
mše	mše	k1gFnSc1	mše
vánoční	vánoční	k2eAgFnSc1d1	vánoční
Jakuba	Jakub	k1gMnSc4	Jakub
Jana	Jan	k1gMnSc2	Jan
Ryby	Ryba	k1gMnSc2	Ryba
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
uváděna	uvádět	k5eAaImNgFnS	uvádět
i	i	k9	i
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Hej	hej	k0	hej
mistře	mistr	k1gMnSc5	mistr
<g/>
,	,	kIx,	,
vstaň	vstát	k5eAaPmRp2nS	vstát
bystře	bystro	k6eAd1	bystro
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
napsal	napsat	k5eAaPmAgMnS	napsat
skladbu	skladba	k1gFnSc4	skladba
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
O	o	k7c6	o
štědrém	štědrý	k2eAgInSc6d1	štědrý
večeru	večer	k1gInSc6	večer
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k1gMnSc1	Martinů
klavírní	klavírní	k2eAgInSc1d1	klavírní
cyklus	cyklus	k1gInSc1	cyklus
Vánoce	Vánoce	k1gFnPc1	Vánoce
a	a	k8xC	a
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Novák	Novák	k1gMnSc1	Novák
Písně	píseň	k1gFnSc2	píseň
zimních	zimní	k2eAgFnPc2d1	zimní
nocí	noc	k1gFnPc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
jsou	být	k5eAaImIp3nP	být
oblíbeny	oblíben	k2eAgFnPc1d1	oblíbena
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
,	,	kIx,	,
Vánoce	Vánoce	k1gFnPc1	Vánoce
přicházejí	přicházet	k5eAaImIp3nP	přicházet
(	(	kIx(	(
<g/>
J.	J.	kA	J.
Vomáčka	Vomáčka	k1gMnSc1	Vomáčka
a	a	k8xC	a
Z.	Z.	kA	Z.
Borovec	Borovec	k1gInSc1	Borovec
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Rolničky	rolnička	k1gFnPc1	rolnička
<g/>
,	,	kIx,	,
rolničky	rolnička	k1gFnPc1	rolnička
(	(	kIx(	(
<g/>
Jingle	Jingle	k1gNnPc1	Jingle
Bells	Bellsa	k1gFnPc2	Bellsa
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
lidová	lidový	k2eAgFnSc1d1	lidová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
zpívány	zpíván	k2eAgFnPc1d1	zpívána
koledy	koleda	k1gFnPc1	koleda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Tichá	Tichá	k1gFnSc1	Tichá
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
Kristus	Kristus	k1gMnSc1	Kristus
Pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
Nesem	Nesem	k?	Nesem
vám	vy	k3xPp2nPc3	vy
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
Veselé	Veselé	k2eAgInPc4d1	Veselé
vánoční	vánoční	k2eAgInPc4d1	vánoční
hody	hod	k1gInPc4	hod
<g/>
,	,	kIx,	,
Půjdem	Půjdem	k?	Půjdem
spolu	spolu	k6eAd1	spolu
do	do	k7c2	do
Betléma	Betlém	k1gInSc2	Betlém
<g/>
,	,	kIx,	,
Pásli	pásnout	k5eAaImAgMnP	pásnout
ovce	ovce	k1gFnPc4	ovce
Valaši	Valach	k1gMnPc1	Valach
<g/>
,	,	kIx,	,
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
nastal	nastat	k5eAaPmAgInS	nastat
<g/>
,	,	kIx,	,
Dej	dát	k5eAaPmRp2nS	dát
Bůh	bůh	k1gMnSc1	bůh
štěstí	štěstí	k1gNnSc2	štěstí
atd.	atd.	kA	atd.
Podle	podle	k7c2	podle
hlasování	hlasování	k1gNnSc2	hlasování
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
přes	přes	k7c4	přes
17	[number]	k4	17
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
Čechů	Čech	k1gMnPc2	Čech
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
vánoční	vánoční	k2eAgFnSc4d1	vánoční
hudbu	hudba	k1gFnSc4	hudba
britský	britský	k2eAgInSc4d1	britský
popový	popový	k2eAgInSc4d1	popový
hit	hit	k1gInSc4	hit
Last	Last	k1gMnSc1	Last
Christmas	Christmas	k1gMnSc1	Christmas
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
od	od	k7c2	od
George	Georg	k1gMnSc2	Georg
Michaela	Michael	k1gMnSc2	Michael
a	a	k8xC	a
skupiny	skupina	k1gFnSc2	skupina
Wham	Whama	k1gFnPc2	Whama
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
22	[number]	k4	22
procent	procento	k1gNnPc2	procento
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
další	další	k2eAgFnPc4d1	další
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc4	píseň
Happy	Happa	k1gFnSc2	Happa
Xmas	Xmasa	k1gFnPc2	Xmasa
od	od	k7c2	od
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Rakušané	Rakušan	k1gMnPc1	Rakušan
preferují	preferovat	k5eAaImIp3nP	preferovat
píseň	píseň	k1gFnSc1	píseň
Tichá	Tichá	k1gFnSc1	Tichá
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
Stille	Stille	k1gFnSc1	Stille
Nacht	Nacht	k1gMnSc1	Nacht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Čechů	Čech	k1gMnPc2	Čech
si	se	k3xPyFc3	se
ale	ale	k9	ale
neuvědomuje	uvědomovat	k5eNaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejde	jít	k5eNaImIp3nS	jít
ani	ani	k8xC	ani
o	o	k7c4	o
českou	český	k2eAgFnSc4d1	Česká
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
o	o	k7c4	o
lidovou	lidový	k2eAgFnSc4d1	lidová
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
českou	český	k2eAgFnSc4d1	Česká
vánoční	vánoční	k2eAgFnSc4d1	vánoční
hudbu	hudba	k1gFnSc4	hudba
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
některá	některý	k3yIgNnPc1	některý
díla	dílo	k1gNnPc1	dílo
známých	známý	k2eAgMnPc2d1	známý
popových	popový	k2eAgMnPc2d1	popový
interpretů	interpret	k1gMnPc2	interpret
<g/>
,	,	kIx,	,
takových	takový	k3xDgMnPc2	takový
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Helena	Helena	k1gFnSc1	Helena
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
jdou	jít	k5eAaImIp3nP	jít
cestou	cesta	k1gFnSc7	cesta
nového	nový	k2eAgNnSc2d1	nové
přezpívání	přezpívání	k1gNnSc2	přezpívání
osvědčených	osvědčený	k2eAgInPc2d1	osvědčený
světových	světový	k2eAgInPc2d1	světový
hitů	hit	k1gInPc2	hit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
albu	album	k1gNnSc6	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kouzlo	kouzlo	k1gNnSc4	kouzlo
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Vánočním	vánoční	k2eAgInSc7d1	vánoční
hitem	hit	k1gInSc7	hit
hudebního	hudební	k2eAgNnSc2d1	hudební
tělesa	těleso	k1gNnSc2	těleso
Sodoma	Sodoma	k1gFnSc1	Sodoma
Gomora	Gomora	k1gFnSc1	Gomora
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc4	píseň
Zasraný	zasraný	k2eAgInSc4d1	zasraný
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
autorů	autor	k1gMnPc2	autor
odráží	odrážet	k5eAaImIp3nS	odrážet
soudobou	soudobý	k2eAgFnSc4d1	soudobá
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
Klasickou	klasický	k2eAgFnSc4d1	klasická
vánoční	vánoční	k2eAgFnSc4d1	vánoční
hudbu	hudba	k1gFnSc4	hudba
lze	lze	k6eAd1	lze
obvykle	obvykle	k6eAd1	obvykle
najít	najít	k5eAaPmF	najít
na	na	k7c6	na
hudebních	hudební	k2eAgInPc6d1	hudební
koncertech	koncert	k1gInPc6	koncert
pořádaných	pořádaný	k2eAgInPc6d1	pořádaný
v	v	k7c6	v
kostelech	kostel	k1gInPc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
pohlednice	pohlednice	k1gFnPc1	pohlednice
jsou	být	k5eAaImIp3nP	být
ilustrované	ilustrovaný	k2eAgFnPc1d1	ilustrovaná
pohlednice	pohlednice	k1gFnPc1	pohlednice
zasílané	zasílaný	k2eAgFnPc1d1	zasílaná
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
mezi	mezi	k7c7	mezi
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
členy	člen	k1gMnPc7	člen
rodiny	rodina	k1gFnSc2	rodina
během	během	k7c2	během
týdnů	týden	k1gInPc2	týden
předcházejících	předcházející	k2eAgInPc2d1	předcházející
Vánocům	Vánoce	k1gFnPc3	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
forma	forma	k1gFnSc1	forma
pozdravu	pozdrav	k1gInSc2	pozdrav
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Přejeme	přát	k5eAaImIp1nP	přát
Vám	vy	k3xPp2nPc3	vy
veselé	veselý	k2eAgFnPc1d1	veselá
Vánoce	Vánoce	k1gFnPc1	Vánoce
a	a	k8xC	a
šťastný	šťastný	k2eAgInSc1d1	šťastný
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
podobnou	podobný	k2eAgFnSc4d1	podobná
formulaci	formulace	k1gFnSc4	formulace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
na	na	k7c4	na
první	první	k4xOgNnPc4	první
komerční	komerční	k2eAgNnPc4d1	komerční
vánoční	vánoční	k2eAgNnPc4d1	vánoční
přání	přání	k1gNnPc4	přání
<g/>
,	,	kIx,	,
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
pro	pro	k7c4	pro
Henryho	Henry	k1gMnSc4	Henry
Colea	Cole	k1gInSc2	Cole
<g/>
,	,	kIx,	,
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
<g/>
.	.	kIx.	.
</s>
<s>
Zvyk	zvyk	k1gInSc4	zvyk
posílání	posílání	k1gNnSc3	posílání
pohlednic	pohlednice	k1gFnPc2	pohlednice
byl	být	k5eAaImAgInS	být
populární	populární	k2eAgInSc1d1	populární
v	v	k7c6	v
ČR	ČR	kA	ČR
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zasílání	zasílání	k1gNnSc1	zasílání
desítky	desítka	k1gFnSc2	desítka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
více	hodně	k6eAd2	hodně
pohlednic	pohlednice	k1gFnPc2	pohlednice
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
"	"	kIx"	"
<g/>
Přejeme	přát	k5eAaImIp1nP	přát
Vám	vy	k3xPp2nPc3	vy
veselé	veselý	k2eAgFnPc1d1	veselá
Vánoce	Vánoce	k1gFnPc1	Vánoce
a	a	k8xC	a
šťastný	šťastný	k2eAgInSc1d1	šťastný
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
<g/>
"	"	kIx"	"
patřilo	patřit	k5eAaImAgNnS	patřit
téměř	téměř	k6eAd1	téměř
mezi	mezi	k7c4	mezi
společenskou	společenský	k2eAgFnSc4d1	společenská
nutnost	nutnost	k1gFnSc4	nutnost
a	a	k8xC	a
nezaslání	nezaslání	k1gNnSc4	nezaslání
pohlednice	pohlednice	k1gFnSc2	pohlednice
spřízněné	spřízněný	k2eAgFnSc2d1	spřízněná
osobě	osoba	k1gFnSc3	osoba
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
nevhodné	vhodný	k2eNgFnPc4d1	nevhodná
<g/>
,	,	kIx,	,
znak	znak	k1gInSc1	znak
nezájmu	nezájem	k1gInSc2	nezájem
a	a	k8xC	a
pro	pro	k7c4	pro
zklamaného	zklamaný	k2eAgMnSc4d1	zklamaný
příjemce	příjemce	k1gMnSc4	příjemce
velmi	velmi	k6eAd1	velmi
deprimující	deprimující	k2eAgFnSc1d1	deprimující
<g/>
.	.	kIx.	.
</s>
<s>
Příjemcem	příjemce	k1gMnSc7	příjemce
a	a	k8xC	a
návštěvami	návštěva	k1gFnPc7	návštěva
během	během	k7c2	během
vánočních	vánoční	k2eAgInPc2d1	vánoční
svátků	svátek	k1gInPc2	svátek
byla	být	k5eAaImAgFnS	být
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
slohová	slohový	k2eAgFnSc1d1	slohová
kvalita	kvalita	k1gFnSc1	kvalita
textu	text	k1gInSc2	text
přání	přání	k1gNnSc2	přání
<g/>
,	,	kIx,	,
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
krasopis	krasopis	k1gInSc1	krasopis
<g/>
,	,	kIx,	,
úprava	úprava	k1gFnSc1	úprava
(	(	kIx(	(
<g/>
zarovnání	zarovnání	k1gNnSc1	zarovnání
textu	text	k1gInSc2	text
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případné	případný	k2eAgNnSc4d1	případné
znečištění	znečištění	k1gNnSc4	znečištění
nebo	nebo	k8xC	nebo
poškození	poškození	k1gNnSc4	poškození
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
výběr	výběr	k1gInSc4	výběr
motivu	motiv	k1gInSc2	motiv
pohlednice	pohlednice	k1gFnSc2	pohlednice
<g/>
,	,	kIx,	,
u	u	k7c2	u
vánočních	vánoční	k2eAgInPc2d1	vánoční
pozdravů	pozdrav	k1gInPc2	pozdrav
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
během	během	k7c2	během
Vánoc	Vánoce	k1gFnPc2	Vánoce
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
postupně	postupně	k6eAd1	postupně
přicházely	přicházet	k5eAaImAgFnP	přicházet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uvedených	uvedený	k2eAgInPc2d1	uvedený
znaků	znak	k1gInPc2	znak
bylo	být	k5eAaImAgNnS	být
usuzováno	usuzován	k2eAgNnSc1d1	usuzováno
na	na	k7c6	na
schopnosti	schopnost	k1gFnSc6	schopnost
nebo	nebo	k8xC	nebo
neschopnosti	neschopnost	k1gFnSc6	neschopnost
odesílatele	odesílatel	k1gMnSc2	odesílatel
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
zábavě	zábava	k1gFnSc3	zábava
během	během	k7c2	během
návštěv	návštěva	k1gFnPc2	návštěva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
nebo	nebo	k8xC	nebo
snížení	snížení	k1gNnSc3	snížení
prestiže	prestiž	k1gFnSc2	prestiž
odesílatele	odesílatel	k1gMnSc2	odesílatel
u	u	k7c2	u
příjemce	příjemce	k1gMnSc2	příjemce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozstříhaných	rozstříhaný	k2eAgFnPc2d1	rozstříhaná
pohlednic	pohlednice	k1gFnPc2	pohlednice
byly	být	k5eAaImAgFnP	být
dětmi	dítě	k1gFnPc7	dítě
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
vytvářeny	vytvářen	k2eAgFnPc4d1	vytvářena
koláže	koláž	k1gFnPc4	koláž
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
nakoupit	nakoupit	k5eAaPmF	nakoupit
vánoční	vánoční	k2eAgFnPc4d1	vánoční
pohlednice	pohlednice	k1gFnPc4	pohlednice
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
byly	být	k5eAaImAgInP	být
před	před	k7c7	před
Vánocemi	Vánoce	k1gFnPc7	Vánoce
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
"	"	kIx"	"
<g/>
nedostatkové	dostatkový	k2eNgNnSc4d1	nedostatkové
zboží	zboží	k1gNnSc4	zboží
<g/>
"	"	kIx"	"
a	a	k8xC	a
hezké	hezký	k2eAgFnPc4d1	hezká
pohlednice	pohlednice	k1gFnPc4	pohlednice
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
s	s	k7c7	s
oblíbenými	oblíbený	k2eAgInPc7d1	oblíbený
ladovskými	ladovský	k2eAgInPc7d1	ladovský
motivy	motiv	k1gInPc7	motiv
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
podpultovým	podpultový	k2eAgNnSc7d1	podpultové
zbožím	zboží	k1gNnSc7	zboží
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Posílat	posílat	k5eAaImF	posílat
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
vytvořené	vytvořený	k2eAgFnPc4d1	vytvořená
pohlednice	pohlednice	k1gFnPc4	pohlednice
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
jako	jako	k8xC	jako
technicky	technicky	k6eAd1	technicky
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
a	a	k8xC	a
i	i	k9	i
později	pozdě	k6eAd2	pozdě
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
nebyl	být	k5eNaImAgInS	být
doporučován	doporučovat	k5eAaImNgInS	doporučovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
vyrobených	vyrobený	k2eAgFnPc2d1	vyrobená
pohlednic	pohlednice	k1gFnPc2	pohlednice
běžná	běžný	k2eAgFnSc1d1	běžná
a	a	k8xC	a
taková	takový	k3xDgFnSc1	takový
pohlednice	pohlednice	k1gFnSc1	pohlednice
pak	pak	k6eAd1	pak
velmi	velmi	k6eAd1	velmi
ceněná	ceněný	k2eAgFnSc1d1	ceněná
příjemci	příjemce	k1gMnSc3	příjemce
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
a	a	k8xC	a
s	s	k7c7	s
prudce	prudko	k6eAd1	prudko
stoupající	stoupající	k2eAgFnSc7d1	stoupající
cenou	cena	k1gFnSc7	cena
poštovních	poštovní	k2eAgFnPc2d1	poštovní
služeb	služba	k1gFnPc2	služba
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInSc1d1	moderní
trend	trend	k1gInSc1	trend
zasílání	zasílání	k1gNnSc2	zasílání
elektronických	elektronický	k2eAgFnPc2d1	elektronická
vánočních	vánoční	k2eAgFnPc2d1	vánoční
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
často	často	k6eAd1	často
o	o	k7c4	o
odesílatelem	odesílatel	k1gMnSc7	odesílatel
zhotovený	zhotovený	k2eAgInSc4d1	zhotovený
nebo	nebo	k8xC	nebo
upravený	upravený	k2eAgInSc4d1	upravený
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
tematický	tematický	k2eAgInSc4d1	tematický
obrázek	obrázek	k1gInSc4	obrázek
nebo	nebo	k8xC	nebo
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nostalgická	nostalgický	k2eAgFnSc1d1	nostalgická
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
minulost	minulost	k1gFnSc4	minulost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
jenž	jenž	k3xRgMnSc1	jenž
nevyužívají	využívat	k5eNaImIp3nP	využívat
internet	internet	k1gInSc4	internet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
posílány	posílán	k2eAgInPc1d1	posílán
i	i	k9	i
ve	v	k7c4	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
papírové	papírový	k2eAgFnSc2d1	papírová
pohlednice	pohlednice	k1gFnSc2	pohlednice
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
pohlednice	pohlednice	k1gFnPc1	pohlednice
byly	být	k5eAaImAgFnP	být
nakupovány	nakupovat	k5eAaBmNgFnP	nakupovat
ve	v	k7c6	v
značných	značný	k2eAgNnPc6d1	značné
množstvích	množství	k1gNnPc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
zobrazovaly	zobrazovat	k5eAaImAgFnP	zobrazovat
umělecká	umělecký	k2eAgNnPc4d1	umělecké
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgInPc4d1	náboženský
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
byly	být	k5eAaImAgFnP	být
komerčně	komerčně	k6eAd1	komerčně
upraveny	upravit	k5eAaPmNgInP	upravit
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
dané	daný	k2eAgNnSc4d1	dané
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
zobrazení	zobrazení	k1gNnSc2	zobrazení
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vztahovat	vztahovat	k5eAaImF	vztahovat
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
příběhu	příběh	k1gInSc3	příběh
Vánoc	Vánoce	k1gFnPc2	Vánoce
s	s	k7c7	s
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
Narození	narození	k1gNnSc2	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mohl	moct	k5eAaImAgInS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
symboliku	symbolika	k1gFnSc4	symbolika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Betlémská	betlémský	k2eAgFnSc1d1	Betlémská
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bílou	bílý	k2eAgFnSc4d1	bílá
holubici	holubice	k1gFnSc4	holubice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
představovat	představovat	k5eAaImF	představovat
Ducha	duch	k1gMnSc4	duch
svatého	svatý	k2eAgMnSc4d1	svatý
nebo	nebo	k8xC	nebo
mír	mír	k1gInSc1	mír
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sekulární	sekulární	k2eAgNnPc1d1	sekulární
vánoční	vánoční	k2eAgNnPc1d1	vánoční
přání	přání	k1gNnPc1	přání
mohla	moct	k5eAaImAgNnP	moct
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
vánoční	vánoční	k2eAgFnPc4d1	vánoční
tradice	tradice	k1gFnPc4	tradice
<g/>
,	,	kIx,	,
mýtické	mýtický	k2eAgFnPc4d1	mýtická
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
či	či	k8xC	či
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
<g/>
,	,	kIx,	,
předměty	předmět	k1gInPc1	předmět
přímo	přímo	k6eAd1	přímo
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
Vánocemi	Vánoce	k1gFnPc7	Vánoce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
svíčky	svíčka	k1gFnPc4	svíčka
<g/>
,	,	kIx,	,
cesmíny	cesmín	k1gInPc4	cesmín
a	a	k8xC	a
ozdoby	ozdoba	k1gFnPc4	ozdoba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
různé	různý	k2eAgInPc4d1	různý
motivy	motiv	k1gInPc4	motiv
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
obdobím	období	k1gNnSc7	období
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
scény	scéna	k1gFnSc2	scéna
se	s	k7c7	s
sněhem	sníh	k1gInSc7	sníh
nebo	nebo	k8xC	nebo
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc4d1	žijící
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
dokonce	dokonce	k9	dokonce
vytvářeny	vytvářet	k5eAaImNgInP	vytvářet
i	i	k8xC	i
vtipné	vtipný	k2eAgInPc1d1	vtipný
pohlednice	pohlednice	k1gFnPc4	pohlednice
a	a	k8xC	a
pohlednice	pohlednice	k1gFnPc4	pohlednice
zobrazující	zobrazující	k2eAgFnSc2d1	zobrazující
nostalgické	nostalgický	k2eAgFnSc2d1	nostalgická
scény	scéna	k1gFnSc2	scéna
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
nakupující	nakupující	k2eAgFnPc1d1	nakupující
dámy	dáma	k1gFnPc1	dáma
v	v	k7c6	v
krinolínách	krinolína	k1gFnPc6	krinolína
na	na	k7c6	na
idealizovaných	idealizovaný	k2eAgFnPc6d1	idealizovaná
ilustracích	ilustrace	k1gFnPc6	ilustrace
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
osoby	osoba	k1gFnPc1	osoba
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
zasílání	zasílání	k1gNnSc2	zasílání
pohlednice	pohlednice	k1gFnSc2	pohlednice
s	s	k7c7	s
básní	báseň	k1gFnSc7	báseň
<g/>
,	,	kIx,	,
modlitbou	modlitba	k1gFnSc7	modlitba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
biblickými	biblický	k2eAgInPc7d1	biblický
verši	verš	k1gInPc7	verš
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
texty	text	k1gInPc1	text
vánočních	vánoční	k2eAgNnPc2d1	vánoční
přání	přání	k1gNnPc2	přání
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
zmínce	zmínka	k1gFnSc3	zmínka
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
nebo	nebo	k8xC	nebo
náboženství	náboženství	k1gNnSc1	náboženství
s	s	k7c7	s
texty	text	k1gInPc7	text
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Všechno	všechen	k3xTgNnSc1	všechen
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
přeje	přát	k5eAaImIp3nS	přát
<g/>
...	...	k?	...
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
vydávají	vydávat	k5eAaImIp3nP	vydávat
pamětní	pamětní	k2eAgFnPc4d1	pamětní
známky	známka	k1gFnPc4	známka
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Zákazníci	zákazník	k1gMnPc1	zákazník
pošty	pošta	k1gFnSc2	pošta
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
často	často	k6eAd1	často
používat	používat	k5eAaImF	používat
tyto	tento	k3xDgFnPc4	tento
známky	známka	k1gFnPc4	známka
na	na	k7c4	na
vánoční	vánoční	k2eAgNnPc4d1	vánoční
přání	přání	k1gNnPc4	přání
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
u	u	k7c2	u
sběratelů	sběratel	k1gMnPc2	sběratel
pohlednic	pohlednice	k1gFnPc2	pohlednice
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
regulérní	regulérní	k2eAgFnPc4d1	regulérní
poštovní	poštovní	k2eAgFnPc4d1	poštovní
známky	známka	k1gFnPc4	známka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
platné	platný	k2eAgNnSc4d1	platné
jako	jako	k8xS	jako
označení	označení	k1gNnSc4	označení
zaplacení	zaplacení	k1gNnSc2	zaplacení
poštovného	poštovné	k1gNnSc2	poštovné
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
mezi	mezi	k7c7	mezi
počátkem	počátek	k1gInSc7	počátek
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vytištěny	vytisknout	k5eAaPmNgFnP	vytisknout
ve	v	k7c6	v
značných	značný	k2eAgNnPc6d1	značné
množstvích	množství	k1gNnPc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
kanadská	kanadský	k2eAgFnSc1d1	kanadská
známka	známka	k1gFnSc1	známka
s	s	k7c7	s
mapou	mapa	k1gFnSc7	mapa
světa	svět	k1gInSc2	svět
a	a	k8xC	a
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
XMAS	XMAS	kA	XMAS
1898	[number]	k4	1898
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
VÁNOCE	Vánoce	k1gFnPc4	Vánoce
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
vydalo	vydat	k5eAaPmAgNnS	vydat
Rakousko	Rakousko	k1gNnSc1	Rakousko
dvě	dva	k4xCgFnPc4	dva
známky	známka	k1gFnPc4	známka
pro	pro	k7c4	pro
vánoční	vánoční	k2eAgInPc4d1	vánoční
pozdravy	pozdrav	k1gInPc4	pozdrav
s	s	k7c7	s
růží	růž	k1gFnSc7	růž
a	a	k8xC	a
znamením	znamení	k1gNnSc7	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
vydala	vydat	k5eAaPmAgFnS	vydat
Brazílie	Brazílie	k1gFnSc1	Brazílie
čtyři	čtyři	k4xCgFnPc4	čtyři
poštovní	poštovní	k2eAgFnPc4d1	poštovní
známky	známka	k1gFnPc4	známka
se	s	k7c7	s
zobrazením	zobrazení	k1gNnSc7	zobrazení
představujícím	představující	k2eAgNnSc7d1	představující
tři	tři	k4xCgMnPc4	tři
krále	král	k1gMnPc4	král
a	a	k8xC	a
betlémskou	betlémský	k2eAgFnSc4d1	Betlémská
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
anděly	anděl	k1gMnPc4	anděl
a	a	k8xC	a
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgInSc4d1	jižní
kříž	kříž	k1gInSc4	kříž
a	a	k8xC	a
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
matku	matka	k1gFnSc4	matka
a	a	k8xC	a
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Dárky	dárek	k1gInPc1	dárek
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
atributů	atribut	k1gInPc2	atribut
moderních	moderní	k2eAgFnPc2d1	moderní
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejziskovější	ziskový	k2eAgInPc1d3	nejziskovější
období	období	k1gNnSc4	období
roku	rok	k1gInSc2	rok
pro	pro	k7c4	pro
maloobchodníky	maloobchodník	k1gMnPc4	maloobchodník
a	a	k8xC	a
podniky	podnik	k1gInPc4	podnik
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Předávání	předávání	k1gNnSc4	předávání
darů	dar	k1gInPc2	dar
byl	být	k5eAaImAgInS	být
běžný	běžný	k2eAgInSc1d1	běžný
zvyk	zvyk	k1gInSc1	zvyk
u	u	k7c2	u
římské	římský	k2eAgFnSc2d1	římská
oslavy	oslava	k1gFnSc2	oslava
Saturnálií	saturnálie	k1gFnPc2	saturnálie
<g/>
,	,	kIx,	,
starobylého	starobylý	k2eAgInSc2d1	starobylý
svátku	svátek	k1gInSc2	svátek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
inspirovat	inspirovat	k5eAaBmF	inspirovat
vánoční	vánoční	k2eAgInPc4d1	vánoční
zvyky	zvyk	k1gInPc4	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
dávají	dávat	k5eAaImIp3nP	dávat
dárky	dárek	k1gInPc1	dárek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
postavou	postava	k1gFnSc7	postava
Svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
jinými	jiný	k2eAgFnPc7d1	jiná
postavami	postava	k1gFnPc7	postava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tři	tři	k4xCgMnPc1	tři
mudrcové	mudrc	k1gMnPc1	mudrc
z	z	k7c2	z
Východu	východ	k1gInSc2	východ
přinesli	přinést	k5eAaPmAgMnP	přinést
novorozenému	novorozený	k2eAgMnSc3d1	novorozený
Ježíšovi	Ježíš	k1gMnSc3	Ježíš
ke	k	k7c3	k
kolébce	kolébka	k1gFnSc3	kolébka
jako	jako	k9	jako
dary	dar	k1gInPc1	dar
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
kadidlo	kadidlo	k1gNnSc4	kadidlo
a	a	k8xC	a
myrhu	myrha	k1gFnSc4	myrha
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vánoční	vánoční	k2eAgInPc4d1	vánoční
zvyky	zvyk	k1gInPc4	zvyk
a	a	k8xC	a
pověry	pověra	k1gFnPc4	pověra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tradiční	tradiční	k2eAgFnPc4d1	tradiční
vánoční	vánoční	k2eAgFnPc4d1	vánoční
postavy	postava	k1gFnPc4	postava
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patří	patřit	k5eAaImIp3nS	patřit
průvod	průvod	k1gInSc1	průvod
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
,	,	kIx,	,
především	především	k9	především
čertem	čert	k1gMnSc7	čert
a	a	k8xC	a
andělem	anděl	k1gMnSc7	anděl
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
tři	tři	k4xCgFnPc1	tři
králové	králová	k1gFnPc1	králová
na	na	k7c6	na
konci	konec	k1gInSc6	konec
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zvyky	zvyk	k1gInPc1	zvyk
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
lokálně	lokálně	k6eAd1	lokálně
odlišné	odlišný	k2eAgInPc4d1	odlišný
nebo	nebo	k8xC	nebo
kombinovány	kombinován	k2eAgInPc4d1	kombinován
<g/>
.	.	kIx.	.
</s>
<s>
Bukači	bukač	k1gMnPc1	bukač
jsou	být	k5eAaImIp3nP	být
osoby	osoba	k1gFnPc4	osoba
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnPc4	skupina
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
koledující	koledující	k2eAgInSc1d1	koledující
s	s	k7c7	s
bukálem	bukál	k1gInSc7	bukál
(	(	kIx(	(
<g/>
brumbálem	brumbál	k1gMnSc7	brumbál
<g/>
,	,	kIx,	,
fanfrnochem	fanfrnoch	k1gInSc7	fanfrnoch
<g/>
)	)	kIx)	)
den	den	k1gInSc1	den
po	po	k7c6	po
Silvestru	Silvestr	k1gMnSc6	Silvestr
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
na	na	k7c4	na
Tři	tři	k4xCgMnPc4	tři
krále	král	k1gMnPc4	král
<g/>
,	,	kIx,	,
na	na	k7c6	na
Plzeňsku	Plzeňsko	k1gNnSc6	Plzeňsko
například	například	k6eAd1	například
koledovali	koledovat	k5eAaImAgMnP	koledovat
často	často	k6eAd1	často
tři	tři	k4xCgFnPc4	tři
králové	králová	k1gFnPc1	králová
s	s	k7c7	s
nástrojem	nástroj	k1gInSc7	nástroj
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
bukač	bukač	k1gMnSc1	bukač
<g/>
,	,	kIx,	,
nástrojem	nástroj	k1gInSc7	nástroj
podobným	podobný	k2eAgInSc7d1	podobný
bubnu	buben	k1gInSc3	buben
nebo	nebo	k8xC	nebo
bubínku	bubínek	k1gInSc3	bubínek
<g/>
,	,	kIx,	,
vyluzujícímu	vyluzující	k2eAgMnSc3d1	vyluzující
hluboký	hluboký	k2eAgInSc4d1	hluboký
zvuk	zvuk	k1gInSc4	zvuk
po	po	k7c6	po
zatahání	zatahání	k1gNnSc6	zatahání
za	za	k7c4	za
žíně	žíně	k1gFnPc4	žíně
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
Svaté	svatý	k2eAgFnSc2d1	svatá
Lucie	Lucie	k1gFnSc2	Lucie
je	být	k5eAaImIp3nS	být
oslavován	oslavován	k2eAgInSc1d1	oslavován
třináctého	třináctý	k4xOgInSc2	třináctý
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
byla	být	k5eAaImAgFnS	být
zpodobněna	zpodobnit	k5eAaPmNgFnS	zpodobnit
jako	jako	k8xS	jako
bíle	bíle	k6eAd1	bíle
oblečená	oblečený	k2eAgFnSc1d1	oblečená
osoba	osoba	k1gFnSc1	osoba
mlčící	mlčící	k2eAgFnSc2d1	mlčící
se	s	k7c7	s
zamoučeným	zamoučený	k2eAgInSc7d1	zamoučený
obličejem	obličej	k1gInSc7	obličej
nebo	nebo	k8xC	nebo
maskou	maska	k1gFnSc7	maska
<g/>
,	,	kIx,	,
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
masky	maska	k1gFnSc2	maska
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
ční	čnět	k5eAaImIp3nP	čnět
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
hrot	hrot	k1gInSc4	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
chodila	chodit	k5eAaImAgFnS	chodit
kobyla	kobyla	k1gFnSc1	kobyla
Lucka	Lucka	k1gFnSc1	Lucka
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
bujarým	bujarý	k2eAgInSc7d1	bujarý
průvodem	průvod	k1gInSc7	průvod
který	který	k3yRgMnSc1	který
prováděl	provádět	k5eAaImAgInS	provádět
různé	různý	k2eAgFnPc4d1	různá
neplechy	neplecha	k1gFnPc4	neplecha
<g/>
,	,	kIx,	,
loutka	loutko	k1gNnPc4	loutko
s	s	k7c7	s
koňskou	koňský	k2eAgFnSc7d1	koňská
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Jinde	jinde	k6eAd1	jinde
je	být	k5eAaImIp3nS	být
Lucie	Lucie	k1gFnSc1	Lucie
popisována	popisován	k2eAgFnSc1d1	popisována
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
králičí	králičí	k2eAgFnSc4d1	králičí
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
obrácený	obrácený	k2eAgInSc4d1	obrácený
kožich	kožich	k1gInSc4	kožich
<g/>
,	,	kIx,	,
v	v	k7c6	v
pokrvácené	pokrvácený	k2eAgFnSc6d1	pokrvácený
ruce	ruka	k1gFnSc6	ruka
nůž	nůž	k1gInSc1	nůž
<g/>
...	...	k?	...
...	...	k?	...
<g/>
jak	jak	k8xS	jak
hrozně	hrozně	k6eAd1	hrozně
vypadala	vypadat	k5eAaPmAgFnS	vypadat
tato	tento	k3xDgFnSc1	tento
obluda	obluda	k1gFnSc1	obluda
v	v	k7c6	v
přítmí	přítmí	k1gNnSc6	přítmí
blikavého	blikavý	k2eAgNnSc2d1	blikavé
světla	světlo	k1gNnSc2	světlo
za	za	k7c2	za
zimního	zimní	k2eAgInSc2d1	zimní
večera	večer	k1gInSc2	večer
v	v	k7c6	v
nízké	nízký	k2eAgFnSc6d1	nízká
světnici	světnice	k1gFnSc6	světnice
venkovské	venkovský	k2eAgFnPc1d1	venkovská
pro	pro	k7c4	pro
strach	strach	k1gInSc4	strach
dětem	dítě	k1gFnPc3	dítě
i	i	k8xC	i
dospělým	dospělí	k1gMnPc3	dospělí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
.	.	kIx.	.
</s>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
bez	bez	k7c2	bez
věnce	věnec	k1gInSc2	věnec
se	s	k7c7	s
svícemi	svíce	k1gFnPc7	svíce
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
severské	severský	k2eAgFnSc2d1	severská
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
svátek	svátek	k1gInSc1	svátek
svaté	svatý	k2eAgFnSc2d1	svatá
Lucie	Lucie	k1gFnSc2	Lucie
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
přesunut	přesunout	k5eAaPmNgInS	přesunout
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
na	na	k7c4	na
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
Lucie	Lucie	k1gFnSc1	Lucie
nosila	nosit	k5eAaImAgFnS	nosit
peroutku	peroutka	k1gFnSc4	peroutka
a	a	k8xC	a
vařečku	vařečka	k1gFnSc4	vařečka
nebo	nebo	k8xC	nebo
velký	velký	k2eAgInSc4d1	velký
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
nůž	nůž	k1gInSc4	nůž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
tradice	tradice	k1gFnSc2	tradice
je	být	k5eAaImIp3nS	být
popsaná	popsaný	k2eAgFnSc1d1	popsaná
poměrně	poměrně	k6eAd1	poměrně
velká	velký	k2eAgFnSc1d1	velká
různorodost	různorodost	k1gFnSc1	různorodost
<g/>
.	.	kIx.	.
</s>
<s>
Lucky	Lucka	k1gFnPc1	Lucka
kontrolovaly	kontrolovat	k5eAaImAgFnP	kontrolovat
pořádek	pořádek	k1gInSc4	pořádek
<g/>
,	,	kIx,	,
neposlušné	poslušný	k2eNgFnPc4d1	neposlušná
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
dodržování	dodržování	k1gNnSc4	dodržování
půstu	půst	k1gInSc2	půst
a	a	k8xC	a
zvyku	zvyk	k1gInSc2	zvyk
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
13	[number]	k4	13
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
nesmí	smět	k5eNaImIp3nS	smět
drát	drát	k5eAaImF	drát
peří	peří	k1gNnSc4	peří
ani	ani	k8xC	ani
příst	příst	k5eAaImF	příst
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
zákaz	zákaz	k1gInSc4	zákaz
porušil	porušit	k5eAaPmAgMnS	porušit
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc4	ten
přišly	přijít	k5eAaPmAgFnP	přijít
Lucie	Lucie	k1gFnPc1	Lucie
klepnout	klepnout	k5eAaPmF	klepnout
přes	přes	k7c4	přes
prsty	prst	k1gInPc4	prst
vařečkou	vařečka	k1gFnSc7	vařečka
a	a	k8xC	a
pocuchat	pocuchat	k5eAaPmF	pocuchat
přádlo	přádlo	k1gNnSc4	přádlo
<g/>
.	.	kIx.	.
</s>
<s>
Nehodné	hodný	k2eNgFnPc1d1	nehodná
děti	dítě	k1gFnPc1	dítě
svaté	svatý	k2eAgFnSc2d1	svatá
Lucie	Lucie	k1gFnSc2	Lucie
strašily	strašit	k5eAaImAgFnP	strašit
<g/>
,	,	kIx,	,
vytahovaly	vytahovat	k5eAaImAgFnP	vytahovat
z	z	k7c2	z
postelí	postel	k1gFnPc2	postel
a	a	k8xC	a
cpaly	cpát	k5eAaImAgFnP	cpát
do	do	k7c2	do
džberu	džber	k1gInSc2	džber
(	(	kIx(	(
<g/>
menší	malý	k2eAgFnPc1d2	menší
kádě	káď	k1gFnPc1	káď
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chtěly	chtít	k5eAaImAgFnP	chtít
jim	on	k3xPp3gMnPc3	on
dloubat	dloubat	k5eAaImF	dloubat
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
propíchnout	propíchnout	k5eAaPmF	propíchnout
břicho	břicho	k1gNnSc4	břicho
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
odnést	odnést	k5eAaPmF	odnést
<g/>
.	.	kIx.	.
</s>
<s>
Poslušným	poslušný	k2eAgFnPc3d1	poslušná
dětem	dítě	k1gFnPc3	dítě
Lucie	Lucie	k1gFnSc2	Lucie
nadělovaly	nadělovat	k5eAaImAgFnP	nadělovat
do	do	k7c2	do
punčoch	punčocha	k1gFnPc2	punčocha
<g/>
.	.	kIx.	.
</s>
<s>
Peroutkou	peroutka	k1gFnSc7	peroutka
z	z	k7c2	z
husího	husí	k2eAgNnSc2d1	husí
křídla	křídlo	k1gNnSc2	křídlo
ometla	ometlo	k1gNnSc2	ometlo
ve	v	k7c6	v
světnici	světnice	k1gFnSc6	světnice
prach	prach	k1gInSc4	prach
<g/>
,	,	kIx,	,
vyčistila	vyčistit	k5eAaPmAgFnS	vyčistit
police	police	k1gFnSc1	police
a	a	k8xC	a
skříně	skříň	k1gFnPc1	skříň
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
učesala	učesat	k5eAaPmAgFnS	učesat
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
.	.	kIx.	.
na	na	k7c6	na
Přerovsku	Přerovsko	k1gNnSc6	Přerovsko
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
Lucky	lucky	k6eAd1	lucky
<g/>
"	"	kIx"	"
prohánějící	prohánějící	k2eAgFnSc1d1	prohánějící
hospodyně	hospodyně	k1gFnSc1	hospodyně
a	a	k8xC	a
vymetající	vymetající	k2eAgInSc1d1	vymetající
dům	dům	k1gInSc1	dům
koštětem	koště	k1gNnSc7	koště
doménou	doména	k1gFnSc7	doména
rozverných	rozverný	k2eAgMnPc2d1	rozverný
mládenců	mládenec	k1gMnPc2	mládenec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
vesničané	vesničan	k1gMnPc1	vesničan
věřili	věřit	k5eAaImAgMnP	věřit
se	se	k3xPyFc4	se
na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
svaté	svatý	k2eAgFnSc2d1	svatá
Lucie	Lucie	k1gFnSc2	Lucie
scházejí	scházet	k5eAaImIp3nP	scházet
čarodějnice	čarodějnice	k1gFnPc1	čarodějnice
aby	aby	kYmCp3nP	aby
škodily	škodit	k5eAaImAgFnP	škodit
dobrým	dobrý	k2eAgMnPc3d1	dobrý
křesťanům	křesťan	k1gMnPc3	křesťan
<g/>
.	.	kIx.	.
tradice	tradice	k1gFnSc1	tradice
se	se	k3xPyFc4	se
ale	ale	k9	ale
různě	různě	k6eAd1	různě
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
Jan	Jan	k1gMnSc1	Jan
Ryba	Ryba	k1gMnSc1	Ryba
o	o	k7c6	o
Lucii	Lucie	k1gFnSc6	Lucie
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Jak	jak	k8xS	jak
ta	ten	k3xDgFnSc1	ten
vyhlíží	vyhlížet	k5eAaImIp3nS	vyhlížet
<g/>
!	!	kIx.	!
</s>
<s>
Ó	ó	k0	ó
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
jest	být	k5eAaImIp3nS	být
strašlivá	strašlivý	k2eAgFnSc1d1	strašlivá
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
bere	brát	k5eAaImIp3nS	brát
vám	vy	k3xPp2nPc3	vy
neposlušné	poslušný	k2eNgNnSc1d1	neposlušné
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
břicha	břicho	k1gNnSc2	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Oho	oho	k0	oho
<g/>
,	,	kIx,	,
toť	toť	k?	toť
by	by	kYmCp3nP	by
muselo	muset	k5eAaImAgNnS	muset
býti	být	k5eAaImF	být
břicho	břicho	k1gNnSc4	břicho
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Perchty	Perchta	k1gFnPc4	Perchta
(	(	kIx(	(
<g/>
také	také	k9	také
Peruchta	peruchta	k1gFnSc1	peruchta
<g/>
,	,	kIx,	,
Šperchta	Šperchta	k1gMnSc1	Šperchta
<g/>
,	,	kIx,	,
Parychta	Parychta	k1gMnSc1	Parychta
<g/>
,	,	kIx,	,
Šperechta	Šperechta	k1gMnSc1	Šperechta
<g/>
)	)	kIx)	)
kontrolovaly	kontrolovat	k5eAaImAgFnP	kontrolovat
dodržování	dodržování	k1gNnSc4	dodržování
půstu	půst	k1gInSc2	půst
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
postava	postava	k1gFnSc1	postava
není	být	k5eNaImIp3nS	být
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
Bílou	bílý	k2eAgFnSc7d1	bílá
Paní	paní	k1gFnSc7	paní
<g/>
,	,	kIx,	,
Perchtou	Perchta	k1gFnSc7	Perchta
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
žádná	žádný	k3yNgFnSc1	žádný
souvislost	souvislost	k1gFnSc1	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
strašidelné	strašidelný	k2eAgFnSc6d1	strašidelná
bytosti	bytost	k1gFnSc6	bytost
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
označení	označení	k1gNnSc6	označení
již	již	k6eAd1	již
od	od	k7c2	od
keltských	keltský	k2eAgInPc2d1	keltský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Představovaly	představovat	k5eAaImAgFnP	představovat
pokání	pokání	k1gNnSc4	pokání
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc4	symbol
zla	zlo	k1gNnSc2	zlo
s	s	k7c7	s
děsivým	děsivý	k2eAgInSc7d1	děsivý
obličejem	obličej	k1gInSc7	obličej
a	a	k8xC	a
s	s	k7c7	s
vyplazeným	vyplazený	k2eAgInSc7d1	vyplazený
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
nosily	nosit	k5eAaImAgFnP	nosit
škopíček	škopíček	k1gInSc4	škopíček
na	na	k7c4	na
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
ruce	ruka	k1gFnSc6	ruka
nůž	nůž	k1gInSc4	nůž
a	a	k8xC	a
hrozily	hrozit	k5eAaImAgFnP	hrozit
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
když	když	k8xS	když
budou	být	k5eAaImBp3nP	být
zlobit	zlobit	k5eAaImF	zlobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
rozpářou	rozpárat	k5eAaPmIp3nP	rozpárat
břicho	břicho	k1gNnSc4	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc4	břicho
vycpávaly	vycpávat	k5eAaImAgFnP	vycpávat
koudelí	koudel	k1gFnSc7	koudel
nebo	nebo	k8xC	nebo
hrachem	hrách	k1gInSc7	hrách
<g/>
,	,	kIx,	,
ukázky	ukázka	k1gFnPc1	ukázka
operace	operace	k1gFnSc2	operace
v	v	k7c6	v
zábavné	zábavný	k2eAgFnSc6d1	zábavná
formě	forma	k1gFnSc6	forma
předváděly	předvádět	k5eAaImAgFnP	předvádět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rukopisu	rukopis	k1gInSc6	rukopis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1418	[number]	k4	1418
je	být	k5eAaImIp3nS	být
popisována	popisován	k2eAgFnSc1d1	popisována
strašidelná	strašidelný	k2eAgFnSc1d1	strašidelná
Perchta	Perchta	k1gFnSc1	Perchta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prý	prý	k9	prý
má	mít	k5eAaImIp3nS	mít
zlatou	zlatý	k2eAgFnSc4d1	zlatá
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
cínové	cínový	k2eAgNnSc4d1	cínové
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
měděné	měděný	k2eAgFnPc4d1	měděná
uši	ucho	k1gNnPc1	ucho
<g/>
,	,	kIx,	,
železný	železný	k2eAgInSc1d1	železný
nos	nos	k1gInSc1	nos
<g/>
,	,	kIx,	,
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
bradu	brada	k1gFnSc4	brada
(	(	kIx(	(
<g/>
vous	vous	k1gInSc1	vous
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
olověný	olověný	k2eAgInSc4d1	olověný
krk	krk	k1gInSc4	krk
<g/>
.	.	kIx.	.
</s>
<s>
Šprecht	Šprecht	k1gInSc1	Šprecht
je	být	k5eAaImIp3nS	být
zmiňován	zmiňovat	k5eAaImNgInS	zmiňovat
v	v	k7c6	v
křesťanských	křesťanský	k2eAgFnPc6d1	křesťanská
dobách	doba	k1gFnPc6	doba
jako	jako	k8xC	jako
postava	postava	k1gFnSc1	postava
kontrolující	kontrolující	k2eAgNnSc4d1	kontrolující
dodržování	dodržování	k1gNnSc4	dodržování
půstu	půst	k1gInSc2	půst
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc1	břicho
dětem	dítě	k1gFnPc3	dítě
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
probodnutím	probodnutí	k1gNnSc7	probodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Žber	Žber	k1gInSc1	Žber
je	být	k5eAaImIp3nS	být
popisovaný	popisovaný	k2eAgInSc1d1	popisovaný
Č.	Č.	kA	Č.
Zíbrtem	Zíbrt	k1gMnSc7	Zíbrt
jako	jako	k8xC	jako
veliká	veliký	k2eAgFnSc1d1	veliká
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
koudelí	koudel	k1gFnSc7	koudel
<g/>
.	.	kIx.	.
</s>
<s>
Žberem	Žberem	k6eAd1	Žberem
rodiče	rodič	k1gMnPc1	rodič
strašili	strašit	k5eAaImAgMnP	strašit
děti	dítě	k1gFnPc4	dítě
velmi	velmi	k6eAd1	velmi
účinně	účinně	k6eAd1	účinně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
nedobré	dobrý	k2eNgFnPc4d1	nedobrá
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
,	,	kIx,	,
vešel	vejít	k5eAaPmAgInS	vejít
do	do	k7c2	do
jizby	jizba	k1gFnSc2	jizba
a	a	k8xC	a
nepoddanci	nepoddance	k1gFnSc4	nepoddance
prý	prý	k9	prý
rozřezal	rozřezat	k5eAaPmAgMnS	rozřezat
břicho	břicho	k1gNnSc4	břicho
<g/>
,	,	kIx,	,
vnitřnosti	vnitřnost	k1gFnPc4	vnitřnost
vybral	vybrat	k5eAaPmAgMnS	vybrat
a	a	k8xC	a
místo	místo	k7c2	místo
nich	on	k3xPp3gMnPc2	on
vložil	vložit	k5eAaPmAgMnS	vložit
koudel	koudel	k1gFnSc4	koudel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
popisuje	popisovat	k5eAaImIp3nS	popisovat
Č.	Č.	kA	Č.
Zíbrt	Zíbrt	k1gInSc1	Zíbrt
pověst	pověst	k1gFnSc1	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
přicházela	přicházet	k5eAaImAgFnS	přicházet
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
vozila	vozit	k5eAaImAgFnS	vozit
s	s	k7c7	s
sebou	se	k3xPyFc7	se
na	na	k7c6	na
vozíku	vozík	k1gInSc6	vozík
taženém	tažený	k2eAgInSc6d1	tažený
dvěma	dva	k4xCgFnPc7	dva
koni	kůň	k1gMnPc7	kůň
nebo	nebo	k8xC	nebo
osly	osel	k1gMnPc7	osel
pytle	pytel	k1gInSc2	pytel
koudelí	koudel	k1gFnPc2	koudel
<g/>
.	.	kIx.	.
</s>
<s>
Čaputa	Čaput	k1gMnSc4	Čaput
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
po	po	k7c6	po
dědinách	dědina	k1gFnPc6	dědina
již	již	k9	již
na	na	k7c4	na
štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rozpustilostech	rozpustilost	k1gFnPc6	rozpustilost
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
člověk	člověk	k1gMnSc1	člověk
malého	malý	k2eAgInSc2d1	malý
vzrůstu	vzrůst	k1gInSc2	vzrůst
oděný	oděný	k2eAgInSc4d1	oděný
do	do	k7c2	do
napolo	napolo	k6eAd1	napolo
do	do	k7c2	do
mužských	mužský	k2eAgFnPc2d1	mužská
a	a	k8xC	a
napolo	napolo	k6eAd1	napolo
do	do	k7c2	do
ženských	ženský	k2eAgInPc2d1	ženský
šatů	šat	k1gInPc2	šat
<g/>
,	,	kIx,	,
vycpaných	vycpaný	k2eAgInPc2d1	vycpaný
slámou	sláma	k1gFnSc7	sláma
a	a	k8xC	a
senem	seno	k1gNnSc7	seno
<g/>
,	,	kIx,	,
s	s	k7c7	s
řešetem	řešeto	k1gNnSc7	řešeto
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
býkovcem	býkovec	k1gInSc7	býkovec
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
potká	potkat	k5eAaPmIp3nS	potkat
výrostka	výrostek	k1gMnSc2	výrostek
<g/>
,	,	kIx,	,
bije	bít	k5eAaImIp3nS	bít
ho	on	k3xPp3gNnSc2	on
býkovcem	býkovec	k1gInSc7	býkovec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
prý	prý	k9	prý
lépe	dobře	k6eAd2	dobře
rostl	růst	k5eAaImAgInS	růst
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
líný	líný	k2eAgMnSc1d1	líný
<g/>
.	.	kIx.	.
</s>
<s>
Ometačky	Ometačka	k1gFnPc1	Ometačka
chodívaly	chodívat	k5eAaImAgFnP	chodívat
koledovat	koledovat	k5eAaImF	koledovat
o	o	k7c6	o
silvestrovské	silvestrovský	k2eAgFnSc6d1	silvestrovská
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
prosí	prosit	k5eAaImIp3nS	prosit
o	o	k7c4	o
almužnu	almužna	k1gFnSc4	almužna
<g/>
.	.	kIx.	.
</s>
<s>
Zpodobňují	zpodobňovat	k5eAaImIp3nP	zpodobňovat
je	on	k3xPp3gFnPc4	on
staré	starý	k2eAgFnPc4d1	stará
ženštiny	ženština	k1gFnPc4	ženština
černě	černě	k6eAd1	černě
oděné	oděný	k2eAgFnPc4d1	oděná
<g/>
,	,	kIx,	,
jen	jen	k9	jen
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
šátkem	šátek	k1gInSc7	šátek
zavázaným	zavázaný	k2eAgInSc7d1	zavázaný
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
obličej	obličej	k1gInSc4	obličej
není	být	k5eNaImIp3nS	být
skoro	skoro	k6eAd1	skoro
vidět	vidět	k5eAaImF	vidět
mimo	mimo	k7c4	mimo
velký	velký	k2eAgInSc4d1	velký
hrot	hrot	k1gInSc4	hrot
vpředu	vpředu	k6eAd1	vpředu
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
zaklepá	zaklepat	k5eAaPmIp3nS	zaklepat
třikrát	třikrát	k6eAd1	třikrát
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
nemluví	mluvit	k5eNaImIp3nS	mluvit
dokud	dokud	k8xS	dokud
nedojde	dojít	k5eNaPmIp3nS	dojít
ke	k	k7c3	k
kamnům	kamna	k1gNnPc3	kamna
které	který	k3yRgFnPc4	který
třikrát	třikrát	k6eAd1	třikrát
přejede	přejet	k5eAaPmIp3nS	přejet
březovou	březový	k2eAgFnSc4d1	Březová
metličku	metlička	k1gFnSc4	metlička
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
Otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Syna	syn	k1gMnSc2	syn
a	a	k8xC	a
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
"	"	kIx"	"
a	a	k8xC	a
popřeje	popřát	k5eAaPmIp3nS	popřát
hospodáři	hospodář	k1gMnPc1	hospodář
"	"	kIx"	"
<g/>
Aby	aby	kYmCp3nP	aby
jim	on	k3xPp3gMnPc3	on
Pambů	Pamb	k1gInPc2	Pamb
dal	dát	k5eAaPmAgInS	dát
šťastnej	šťastnej	k?	šťastnej
novej	novej	k?	novej
rok	rok	k1gInSc4	rok
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Baby	baby	k1gNnSc4	baby
byla	být	k5eAaImAgFnS	být
dvojice	dvojice	k1gFnSc1	dvojice
osob	osoba	k1gFnPc2	osoba
představují	představovat	k5eAaImIp3nP	představovat
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
celé	celý	k2eAgFnPc1d1	celá
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
hlavy	hlava	k1gFnPc4	hlava
zahalené	zahalený	k2eAgFnPc4d1	zahalená
šátky	šátek	k1gInPc4	šátek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ruce	ruka	k1gFnSc6	ruka
držely	držet	k5eAaImAgFnP	držet
husí	husí	k2eAgNnSc4d1	husí
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
hrneček	hrneček	k1gInSc1	hrneček
s	s	k7c7	s
kolomazí	kolomaz	k1gFnSc7	kolomaz
<g/>
.	.	kIx.	.
</s>
<s>
Chodily	chodit	k5eAaImAgInP	chodit
o	o	k7c4	o
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
i	i	k9	i
před	před	k7c7	před
Novým	nový	k2eAgInSc7d1	nový
rokem	rok	k1gInSc7	rok
i	i	k9	i
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
se	s	k7c7	s
strašidelným	strašidelný	k2eAgInSc7d1	strašidelný
hlasem	hlas	k1gInSc7	hlas
a	a	k8xC	a
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Dejte	dát	k5eAaPmRp2nP	dát
holky	holka	k1gFnPc4	holka
nebo	nebo	k8xC	nebo
vdolky	vdolek	k1gInPc4	vdolek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
podarovány	podarován	k2eAgInPc4d1	podarován
vdolky	vdolek	k1gInPc4	vdolek
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
z	z	k7c2	z
Bystřice	Bystřice	k1gFnSc2	Bystřice
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
<g/>
.	.	kIx.	.
</s>
<s>
Štědrá	štědrý	k2eAgFnSc1d1	štědrá
bába	bába	k1gFnSc1	bába
<g/>
,	,	kIx,	,
klokavá	klokavý	k2eAgFnSc1d1	klokavý
<g/>
,	,	kIx,	,
klovcová	klovcový	k2eAgFnSc1d1	klovcový
<g/>
,	,	kIx,	,
šťuchavá	šťuchavý	k2eAgFnSc1d1	šťuchavý
<g/>
,	,	kIx,	,
vrtibába	vrtibába	k1gFnSc1	vrtibába
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
bíle	bíle	k6eAd1	bíle
zahalená	zahalený	k2eAgFnSc1d1	zahalená
postava	postava	k1gFnSc1	postava
s	s	k7c7	s
maskou	maska	k1gFnSc7	maska
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
nosem	nos	k1gInSc7	nos
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
vymýšlela	vymýšlet	k5eAaImAgFnS	vymýšlet
různé	různý	k2eAgInPc4d1	různý
žertíky	žertík	k1gInPc4	žertík
a	a	k8xC	a
hrozila	hrozit	k5eAaImAgFnS	hrozit
metlou	metla	k1gFnSc7	metla
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
tajně	tajně	k6eAd1	tajně
nadělovala	nadělovat	k5eAaImAgFnS	nadělovat
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
klovcová	klovcový	k2eAgFnSc1d1	klovcový
bába	bába	k1gFnSc1	bába
a	a	k8xC	a
chodím	chodit	k5eAaImIp1nS	chodit
mezi	mezi	k7c7	mezi
Štědrým	štědrý	k2eAgInSc7d1	štědrý
dnem	den	k1gInSc7	den
a	a	k8xC	a
Novým	nový	k2eAgInSc7d1	nový
rokem	rok	k1gInSc7	rok
a	a	k8xC	a
mučím	mučet	k5eAaImIp1nS	mučet
zlobivé	zlobivý	k2eAgFnPc4d1	zlobivá
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgNnSc1	některý
stačí	stačit	k5eAaBmIp3nS	stačit
jen	jen	k9	jen
tak	tak	k6eAd1	tak
vyděsit	vyděsit	k5eAaPmF	vyděsit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
bály	bát	k5eAaImAgInP	bát
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
uvádí	uvádět	k5eAaImIp3nS	uvádět
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
postava	postava	k1gFnSc1	postava
ve	v	k7c6	v
skanzenu	skanzen	k1gInSc6	skanzen
v	v	k7c6	v
Kouřimi	Kouřim	k1gFnSc6	Kouřim
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláška	Mikuláška	k1gFnSc1	Mikuláška
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
postavou	postava	k1gFnSc7	postava
nebo	nebo	k8xC	nebo
v	v	k7c6	v
průvodu	průvod	k1gInSc6	průvod
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
popisu	popis	k1gInSc2	popis
březovými	březový	k2eAgInPc7d1	březový
proutky	proutek	k1gInPc7	proutek
šlehala	šlehat	k5eAaImAgFnS	šlehat
po	po	k7c6	po
oknech	okno	k1gNnPc6	okno
a	a	k8xC	a
nosila	nosit	k5eAaImAgFnS	nosit
košík	košík	k1gInSc4	košík
s	s	k7c7	s
dárky	dárek	k1gInPc7	dárek
pro	pro	k7c4	pro
hodné	hodný	k2eAgFnPc4d1	hodná
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
také	také	k9	také
s	s	k7c7	s
cepem	cep	k1gInSc7	cep
<g/>
,	,	kIx,	,
kopistem	kopist	k1gInSc7	kopist
či	či	k8xC	či
srpem	srp	k1gInSc7	srp
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
tradici	tradice	k1gFnSc6	tradice
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
v	v	k7c6	v
říkadle	říkadlo	k1gNnSc6	říkadlo
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
ztratil	ztratit	k5eAaPmAgMnS	ztratit
plášť	plášť	k1gInSc4	plášť
<g/>
,	,	kIx,	,
Mikuláška	Mikuláška	k1gFnSc1	Mikuláška
sukni	sukně	k1gFnSc4	sukně
<g/>
,	,	kIx,	,
hledali	hledat	k5eAaImAgMnP	hledat
<g/>
,	,	kIx,	,
nenašli	najít	k5eNaPmAgMnP	najít
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc4	dva
smutní	smutný	k2eAgMnPc1d1	smutný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
Seničce	Senička	k1gFnSc6	Senička
rodiče	rodič	k1gMnPc1	rodič
straší	strašit	k5eAaImIp3nP	strašit
děti	dítě	k1gFnPc4	dítě
zlou	zlá	k1gFnSc7	zlá
Mikuláškou	Mikuláška	k1gFnSc7	Mikuláška
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sbírá	sbírat	k5eAaImIp3nS	sbírat
dárky	dárek	k1gInPc4	dárek
od	od	k7c2	od
sv.	sv.	kA	sv.
Mikuláše	mikuláš	k1gInSc2	mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
bílý	bílý	k2eAgInSc4d1	bílý
oděv	oděv	k1gInSc4	oděv
a	a	k8xC	a
krvavé	krvavý	k2eAgFnPc4d1	krvavá
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Kališové	Kališ	k1gMnPc1	Kališ
(	(	kIx(	(
<g/>
kalašové	kalašové	k2eAgMnPc1d1	kalašové
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
koledníci	koledník	k1gMnPc1	koledník
nebo	nebo	k8xC	nebo
postavy	postava	k1gFnPc1	postava
koledující	koledující	k2eAgFnPc1d1	koledující
o	o	k7c6	o
Štědrém	štědrý	k2eAgInSc6d1	štědrý
dni	den	k1gInSc6	den
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
zpráv	zpráva	k1gFnPc2	zpráva
ještě	ještě	k9	ještě
před	před	k7c7	před
východem	východ	k1gInSc7	východ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
podle	podle	k7c2	podle
dobových	dobový	k2eAgFnPc2d1	dobová
zpráv	zpráva	k1gFnPc2	zpráva
chodil	chodit	k5eAaImAgMnS	chodit
kališ	kališ	k1gInSc4	kališ
s	s	k7c7	s
peroutkou	peroutka	k1gFnSc7	peroutka
(	(	kIx(	(
<g/>
bílou	bílý	k2eAgFnSc7d1	bílá
nebo	nebo	k8xC	nebo
zlatou	zlatý	k2eAgFnSc7d1	zlatá
<g/>
)	)	kIx)	)
a	a	k8xC	a
oprašoval	oprašovat	k5eAaImAgMnS	oprašovat
nábytek	nábytek	k1gInSc4	nábytek
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Koledou	koleda	k1gFnSc7	koleda
si	se	k3xPyFc3	se
přivydělávali	přivydělávat	k5eAaImAgMnP	přivydělávat
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
učitelé	učitel	k1gMnPc1	učitel
<g/>
,	,	kIx,	,
rektoři	rektor	k1gMnPc1	rektor
<g/>
,	,	kIx,	,
kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
správcové	správce	k1gMnPc1	správce
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
koledou	koleda	k1gFnSc7	koleda
chodili	chodit	k5eAaImAgMnP	chodit
i	i	k9	i
řezničtí	řeznický	k2eAgMnPc1d1	řeznický
učni	učeň	k1gMnPc1	učeň
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nikoliv	nikoliv	k9	nikoliv
žebráci	žebrák	k1gMnPc1	žebrák
<g/>
.	.	kIx.	.
</s>
<s>
Kališ	kališ	k1gInSc1	kališ
je	být	k5eAaImIp3nS	být
popisován	popisovat	k5eAaImNgInS	popisovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
oděný	oděný	k2eAgInSc1d1	oděný
v	v	k7c4	v
bílý	bílý	k2eAgInSc4d1	bílý
rubáš	rubáš	k1gInSc4	rubáš
<g/>
,	,	kIx,	,
s	s	k7c7	s
korunkou	korunka	k1gFnSc7	korunka
z	z	k7c2	z
pozlaceného	pozlacený	k2eAgInSc2d1	pozlacený
papíru	papír	k1gInSc2	papír
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
měl	mít	k5eAaImAgMnS	mít
kosinku	kosinka	k1gFnSc4	kosinka
rovněž	rovněž	k9	rovněž
pozlacenou	pozlacený	k2eAgFnSc4d1	pozlacená
a	a	k8xC	a
zpívaje	zpívat	k5eAaImSgInS	zpívat
písně	píseň	k1gFnPc4	píseň
vánoční	vánoční	k2eAgFnSc7d1	vánoční
obešel	obejít	k5eAaPmAgMnS	obejít
každou	každý	k3xTgFnSc4	každý
zelenářku	zelenářka	k1gFnSc4	zelenářka
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
kosův	kosův	k2eAgInSc1d1	kosův
a	a	k8xC	a
pak	pak	k6eAd1	pak
obou	dva	k4xCgFnPc2	dva
tváří	tvář	k1gFnPc2	tvář
se	se	k3xPyFc4	se
dotýkal	dotýkat	k5eAaImAgInS	dotýkat
tou	ten	k3xDgFnSc7	ten
kosinkou	kosinka	k1gFnSc7	kosinka
pro	pro	k7c4	pro
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
zpráv	zpráva	k1gFnPc2	zpráva
byl	být	k5eAaImAgInS	být
kališ	kališ	k1gInSc1	kališ
oblečen	obléct	k5eAaPmNgInS	obléct
pestře	pestro	k6eAd1	pestro
<g/>
,	,	kIx,	,
s	s	k7c7	s
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
ptačím	ptačí	k2eAgNnSc7d1	ptačí
perem	pero	k1gNnSc7	pero
za	za	k7c7	za
vysokým	vysoký	k2eAgInSc7d1	vysoký
špičatým	špičatý	k2eAgInSc7d1	špičatý
kloboukem	klobouk	k1gInSc7	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Odměnou	odměna	k1gFnSc7	odměna
je	být	k5eAaImIp3nS	být
zboží	zboží	k1gNnSc1	zboží
které	který	k3yIgNnSc1	který
obchod	obchod	k1gInSc4	obchod
nabízí	nabízet	k5eAaImIp3nS	nabízet
nebo	nebo	k8xC	nebo
pár	pár	k4xCyI	pár
grošů	groš	k1gInPc2	groš
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Ambrož	Ambrož	k1gMnSc1	Ambrož
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
v	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
hábitu	hábit	k1gInSc2	hábit
se	s	k7c7	s
špičatou	špičatý	k2eAgFnSc7d1	špičatá
černou	černý	k2eAgFnSc7d1	černá
čepicí	čepice	k1gFnSc7	čepice
a	a	k8xC	a
závojem	závoj	k1gInSc7	závoj
přes	přes	k7c4	přes
obličej	obličej	k1gInSc4	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Působila	působit	k5eAaImAgFnS	působit
kolem	kolem	k7c2	kolem
kostelů	kostel	k1gInPc2	kostel
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
lákala	lákat	k5eAaImAgFnS	lákat
děti	dítě	k1gFnPc4	dítě
na	na	k7c4	na
dobroty	dobrota	k1gFnPc4	dobrota
a	a	k8xC	a
potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
strašila	strašit	k5eAaImAgFnS	strašit
a	a	k8xC	a
honila	honit	k5eAaImAgFnS	honit
s	s	k7c7	s
metlou	metla	k1gFnSc7	metla
polepenou	polepený	k2eAgFnSc4d1	polepená
bílým	bílý	k2eAgInSc7d1	bílý
papírem	papír	k1gInSc7	papír
<g/>
.	.	kIx.	.
</s>
<s>
Barbory	Barbora	k1gFnPc1	Barbora
nadělovaly	nadělovat	k5eAaImAgFnP	nadělovat
dětem	dítě	k1gFnPc3	dítě
drobné	drobný	k2eAgInPc4d1	drobný
dárečky	dáreček	k1gInPc7	dáreček
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
bonbóny	bonbón	k1gInPc4	bonbón
<g/>
.	.	kIx.	.
</s>
<s>
Vystupovaly	vystupovat	k5eAaImAgFnP	vystupovat
jako	jako	k9	jako
kladné	kladný	k2eAgFnPc1d1	kladná
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
nezastrašovaly	zastrašovat	k5eNaImAgFnP	zastrašovat
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
uvést	uvést	k5eAaPmF	uvést
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
míru	míra	k1gFnSc4	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
zvyky	zvyk	k1gInPc1	zvyk
byly	být	k5eAaImAgInP	být
obvykle	obvykle	k6eAd1	obvykle
prováděny	provádět	k5eAaImNgInP	provádět
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
pobavení	pobavení	k1gNnSc2	pobavení
zúčastněných	zúčastněný	k2eAgMnPc2d1	zúčastněný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
koledníci	koledník	k1gMnPc1	koledník
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
snažili	snažit	k5eAaImAgMnP	snažit
naplňovat	naplňovat	k5eAaImF	naplňovat
žerty	žert	k1gInPc7	žert
<g/>
,	,	kIx,	,
narážkami	narážka	k1gFnPc7	narážka
<g/>
,	,	kIx,	,
změnou	změna	k1gFnSc7	změna
hlasu	hlas	k1gInSc2	hlas
a	a	k8xC	a
vtipnými	vtipný	k2eAgFnPc7d1	vtipná
modifikacemi	modifikace	k1gFnPc7	modifikace
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zábavu	zábava	k1gFnSc4	zábava
byli	být	k5eAaImAgMnP	být
odměňování	odměňování	k1gNnSc1	odměňování
spíše	spíše	k9	spíše
než	než	k8xS	než
za	za	k7c4	za
strašení	strašení	k1gNnSc4	strašení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
děsivé	děsivý	k2eAgFnPc1d1	děsivá
lucie	lucie	k1gFnPc1	lucie
a	a	k8xC	a
Perchty	Perchta	k1gFnPc1	Perchta
byly	být	k5eAaImAgFnP	být
představením	představení	k1gNnSc7	představení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
pobavit	pobavit	k5eAaPmF	pobavit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
v	v	k7c6	v
křesťanských	křesťanský	k2eAgFnPc6d1	křesťanská
dobách	doba	k1gFnPc6	doba
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
staré	starý	k2eAgInPc1d1	starý
kulty	kult	k1gInPc1	kult
božích	boží	k2eAgMnPc2d1	boží
poslů	posel	k1gMnPc2	posel
"	"	kIx"	"
<g/>
divé	divý	k2eAgFnSc2d1	divá
ženy	žena	k1gFnSc2	žena
<g/>
"	"	kIx"	"
obcházející	obcházející	k2eAgMnSc1d1	obcházející
na	na	k7c4	na
Silvestra	Silvestr	k1gMnSc4	Silvestr
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
je	být	k5eAaImIp3nS	být
uchystáno	uchystat	k5eAaPmNgNnS	uchystat
něco	něco	k6eAd1	něco
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
vdolek	vdolek	k1gInSc1	vdolek
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
divé	divý	k2eAgFnPc1d1	divá
ženy	žena	k1gFnPc1	žena
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgFnP	být
neviditelné	viditelný	k2eNgFnPc1d1	neviditelná
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
spíše	spíše	k9	spíše
o	o	k7c4	o
úlitbu	úlitba	k1gFnSc4	úlitba
bůžkům	bůžek	k1gMnPc3	bůžek
než	než	k8xS	než
zábavu	zábava	k1gFnSc4	zábava
či	či	k8xC	či
koledu	koleda	k1gFnSc4	koleda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Vánocemi	Vánoce	k1gFnPc7	Vánoce
a	a	k8xC	a
rozdáváním	rozdávání	k1gNnSc7	rozdávání
dárků	dárek	k1gInPc2	dárek
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
sv.	sv.	kA	sv.
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
(	(	kIx(	(
<g/>
St.	st.	kA	st.
Nicholas	Nicholas	k1gMnSc1	Nicholas
<g/>
)	)	kIx)	)
i	i	k9	i
postavy	postava	k1gFnPc4	postava
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
odvozené	odvozený	k2eAgNnSc1d1	odvozené
–	–	k?	–
anglický	anglický	k2eAgMnSc1d1	anglický
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
Pè	Pè	k1gMnSc1	Pè
Noël	Noël	k1gMnSc1	Noël
<g/>
,	,	kIx,	,
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
Sinterklaas	Sinterklaas	k1gMnSc1	Sinterklaas
či	či	k8xC	či
německý	německý	k2eAgMnSc1d1	německý
Weihnachtsmann	Weihnachtsmann	k1gMnSc1	Weihnachtsmann
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
rozdává	rozdávat	k5eAaImIp3nS	rozdávat
dárky	dárek	k1gInPc4	dárek
Christkind	Christkinda	k1gFnPc2	Christkinda
čili	čili	k8xC	čili
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Děda	děda	k1gMnSc1	děda
Mráz	Mráz	k1gMnSc1	Mráz
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
Joulupukki	Joulupukk	k1gFnSc2	Joulupukk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
skřítek	skřítek	k1gMnSc1	skřítek
Jultomten	Jultomtno	k1gNnPc2	Jultomtno
<g/>
,	,	kIx,	,
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
jiný	jiný	k2eAgMnSc1d1	jiný
skřítek	skřítek	k1gMnSc1	skřítek
Julenissen	Julenissna	k1gFnPc2	Julenissna
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
nejznámější	známý	k2eAgInSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
červeně	červeně	k6eAd1	červeně
oblečený	oblečený	k2eAgMnSc1d1	oblečený
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaPmF	vysledovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
holandského	holandský	k2eAgInSc2d1	holandský
výrazu	výraz	k1gInSc2	výraz
Sinterklaas	Sinterklaasa	k1gFnPc2	Sinterklaasa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
pojmenování	pojmenování	k1gNnSc1	pojmenování
pro	pro	k7c4	pro
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	mikuláš	k1gInSc1	mikuláš
byl	být	k5eAaImAgInS	být
biskupem	biskup	k1gInSc7	biskup
z	z	k7c2	z
Myry	Myra	k1gFnSc2	Myra
<g/>
,	,	kIx,	,
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
během	během	k7c2	během
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
péči	péče	k1gFnSc3	péče
o	o	k7c4	o
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
štědrostí	štědrost	k1gFnSc7	štědrost
a	a	k8xC	a
dáváním	dávání	k1gNnSc7	dávání
dárků	dárek	k1gInPc2	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
svátek	svátek	k1gInSc1	svátek
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
slaven	slavit	k5eAaImNgInS	slavit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
dáváním	dávání	k1gNnSc7	dávání
darů	dar	k1gInPc2	dar
<g/>
.	.	kIx.	.
</s>
<s>
Saint	Saint	k1gMnSc1	Saint
Nicholas	Nicholas	k1gMnSc1	Nicholas
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
představován	představovat	k5eAaImNgInS	představovat
postavou	postava	k1gFnSc7	postava
v	v	k7c6	v
biskupském	biskupský	k2eAgInSc6d1	biskupský
oděvu	oděv	k1gInSc6	oděv
<g/>
,	,	kIx,	,
doprovázenou	doprovázený	k2eAgFnSc7d1	doprovázená
pomocníky	pomocník	k1gMnPc7	pomocník
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
(	(	kIx(	(
<g/>
po	po	k7c6	po
svých	svůj	k3xOyFgMnPc6	svůj
pomocnících	pomocník	k1gMnPc6	pomocník
<g/>
)	)	kIx)	)
informace	informace	k1gFnPc1	informace
o	o	k7c4	o
chování	chování	k1gNnSc4	chování
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
minulého	minulý	k2eAgInSc2d1	minulý
roku	rok	k1gInSc2	rok
před	před	k7c7	před
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
dárek	dárek	k1gInSc4	dárek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
svatý	svatý	k2eAgMnSc1d1	svatý
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
dobře	dobře	k6eAd1	dobře
znám	znát	k5eAaImIp1nS	znát
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvyk	zvyk	k1gInSc1	zvyk
dávat	dávat	k5eAaImF	dávat
dary	dar	k1gInPc4	dar
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
jméně	jméno	k1gNnSc6	jméno
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
části	část	k1gFnSc6	část
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
reforem	reforma	k1gFnPc2	reforma
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
mnoho	mnoho	k4c1	mnoho
protestantů	protestant	k1gMnPc2	protestant
změnilo	změnit	k5eAaPmAgNnS	změnit
jméno	jméno	k1gNnSc4	jméno
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nosí	nosit	k5eAaImIp3nS	nosit
dárky	dárek	k1gInPc4	dárek
z	z	k7c2	z
Ježíška	ježíšek	k1gInSc2	ježíšek
neboli	neboli	k8xC	neboli
Christkind	Christkinda	k1gFnPc2	Christkinda
na	na	k7c4	na
anglosasky	anglosasky	k6eAd1	anglosasky
deformovanou	deformovaný	k2eAgFnSc4d1	deformovaná
verzi	verze	k1gFnSc4	verze
Kris	kris	k1gInSc1	kris
Kringle	Kringle	k1gFnPc1	Kringle
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
změněno	změnit	k5eAaPmNgNnS	změnit
datum	datum	k1gNnSc1	datum
rozdávání	rozdávání	k1gNnSc2	rozdávání
dárků	dárek	k1gInPc2	dárek
z	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
populární	populární	k2eAgFnSc1d1	populární
postava	postava	k1gFnSc1	postava
Santa	Santa	k1gFnSc1	Santa
Clause	Clause	k1gFnSc1	Clause
byla	být	k5eAaImAgFnS	být
nicméně	nicméně	k8xC	nicméně
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
proměně	proměna	k1gFnSc6	proměna
postavy	postava	k1gFnSc2	postava
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
Washington	Washington	k1gInSc1	Washington
Irving	Irving	k1gInSc1	Irving
(	(	kIx(	(
<g/>
1783	[number]	k4	1783
<g/>
–	–	k?	–
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
a	a	k8xC	a
německo-americký	německomerický	k2eAgMnSc1d1	německo-americký
karikaturista	karikaturista	k1gMnSc1	karikaturista
Thomas	Thomas	k1gMnSc1	Thomas
Nast	Nast	k1gMnSc1	Nast
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
americké	americký	k2eAgFnSc6d1	americká
revoluci	revoluce	k1gFnSc6	revoluce
někteří	některý	k3yIgMnPc1	některý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
hledali	hledat	k5eAaImAgMnP	hledat
symboly	symbol	k1gInPc4	symbol
neanglické	anglický	k2eNgInPc4d1	neanglický
<g/>
,	,	kIx,	,
americké	americký	k2eAgFnSc3d1	americká
minulosti	minulost	k1gFnSc3	minulost
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
založen	založit	k5eAaPmNgInS	založit
jako	jako	k8xC	jako
holandská	holandský	k2eAgFnSc1d1	holandská
kolonie	kolonie	k1gFnSc1	kolonie
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
nesl	nést	k5eAaImAgMnS	nést
název	název	k1gInSc4	název
New	New	k1gFnSc3	New
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
)	)	kIx)	)
a	a	k8xC	a
obyvatelé	obyvatel	k1gMnPc1	obyvatel
si	se	k3xPyFc3	se
vzpomněli	vzpomnít	k5eAaPmAgMnP	vzpomnít
na	na	k7c4	na
holandské	holandský	k2eAgFnPc4d1	holandská
tradice	tradice	k1gFnPc4	tradice
a	a	k8xC	a
postavu	postava	k1gFnSc4	postava
Sinterklaase	Sinterklaasa	k1gFnSc3	Sinterklaasa
<g/>
.	.	kIx.	.
</s>
<s>
St.	st.	kA	st.
Nicholas	Nicholas	k1gInSc1	Nicholas
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
patronem	patron	k1gMnSc7	patron
newyorské	newyorský	k2eAgFnSc2d1	newyorská
Historické	historický	k2eAgFnSc2d1	historická
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Historical	Historical	k1gFnSc2	Historical
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Washington	Washington	k1gInSc1	Washington
Irving	Irving	k1gInSc1	Irving
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
vydal	vydat	k5eAaPmAgInS	vydat
satirický	satirický	k2eAgInSc1d1	satirický
spis	spis	k1gInSc1	spis
Knickerbocker	Knickerbockra	k1gFnPc2	Knickerbockra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
History	Histor	k1gInPc7	Histor
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
from	from	k1gMnSc1	from
The	The	k1gFnPc2	The
Beginning	Beginning	k1gInSc4	Beginning
of	of	k?	of
the	the	k?	the
World	Worlda	k1gFnPc2	Worlda
To	to	k9	to
the	the	k?	the
End	End	k1gMnSc1	End
of	of	k?	of
The	The	k1gMnSc1	The
Dutch	Dutch	k1gMnSc1	Dutch
Dynasty	dynasta	k1gMnSc2	dynasta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
St.	st.	kA	st.
Nicholase	Nicholasa	k1gFnSc6	Nicholasa
vyobrazil	vyobrazit	k5eAaPmAgMnS	vyobrazit
jako	jako	k8xC	jako
tlustého	tlustý	k2eAgMnSc2d1	tlustý
Holanďana	Holanďan	k1gMnSc2	Holanďan
s	s	k7c7	s
dýmkou	dýmka	k1gFnSc7	dýmka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ze	z	k7c2	z
z	z	k7c2	z
ctihodného	ctihodný	k2eAgMnSc2d1	ctihodný
biskupa	biskup	k1gMnSc2	biskup
St.	st.	kA	st.
Nicholase	Nicholas	k1gInSc5	Nicholas
stal	stát	k5eAaPmAgInS	stát
Father	Fathra	k1gFnPc2	Fathra
Christmas	Christmas	k1gMnSc1	Christmas
(	(	kIx(	(
<g/>
Otec	otec	k1gMnSc1	otec
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
silné	silný	k2eAgFnSc2d1	silná
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
plášti	plášť	k1gInSc6	plášť
<g/>
,	,	kIx,	,
se	s	k7c7	s
saněmi	saně	k1gFnPc7	saně
a	a	k8xC	a
soby	sob	k1gMnPc4	sob
<g/>
.	.	kIx.	.
</s>
<s>
Nast	Nast	k1gMnSc1	Nast
kreslil	kreslit	k5eAaImAgMnS	kreslit
nový	nový	k2eAgInSc4d1	nový
obraz	obraz	k1gInSc4	obraz
Otce	otec	k1gMnSc2	otec
Vánoc	Vánoce	k1gFnPc2	Vánoce
každoročně	každoročně	k6eAd1	každoročně
pro	pro	k7c4	pro
Harper	Harper	k1gInSc4	Harper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Weekly	Weekl	k1gInPc7	Weekl
Magazine	Magazin	k1gMnSc5	Magazin
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1863	[number]	k4	1863
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
pojmenování	pojmenování	k1gNnSc2	pojmenování
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
smíšenina	smíšenina	k1gFnSc1	smíšenina
holandského	holandský	k2eAgInSc2d1	holandský
Sinterklaas	Sinterklaasa	k1gFnPc2	Sinterklaasa
a	a	k8xC	a
německého	německý	k2eAgInSc2d1	německý
Sankt	Sankt	k1gInSc1	Sankt
Niklaus	Niklaus	k1gInSc1	Niklaus
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
standardizován	standardizován	k2eAgInSc1d1	standardizován
inzerenty	inzerent	k1gMnPc7	inzerent
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
a	a	k8xC	a
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
dnešní	dnešní	k2eAgFnSc2d1	dnešní
<g/>
.	.	kIx.	.
</s>
<s>
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
zobrazován	zobrazován	k2eAgInSc1d1	zobrazován
jako	jako	k8xS	jako
mírně	mírně	k6eAd1	mírně
obézní	obézní	k2eAgMnSc1d1	obézní
muž	muž	k1gMnSc1	muž
středního	střední	k2eAgInSc2d1	střední
věku	věk	k1gInSc2	věk
s	s	k7c7	s
vousy	vous	k1gInPc7	vous
a	a	k8xC	a
veselou	veselá	k1gFnSc7	veselá
povahou	povaha	k1gFnSc7	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
známosti	známost	k1gFnSc2	známost
a	a	k8xC	a
popularity	popularita	k1gFnSc2	popularita
Santa	Santo	k1gNnSc2	Santo
Clause	Clause	k1gFnSc2	Clause
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
významně	významně	k6eAd1	významně
přispěla	přispět	k5eAaPmAgFnS	přispět
firma	firma	k1gFnSc1	firma
The	The	k1gFnSc1	The
Coca-Cola	cocaola	k1gFnSc1	coca-cola
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
hojně	hojně	k6eAd1	hojně
vyobrazovala	vyobrazovat	k5eAaImAgFnS	vyobrazovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
vánočních	vánoční	k2eAgInPc6d1	vánoční
inzerátech	inzerát	k1gInPc6	inzerát
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1931	[number]	k4	1931
až	až	k9	až
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
Santa	Sant	k1gInSc2	Sant
Clause	Clause	k1gFnSc2	Clause
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
Knecht	knecht	k1gMnSc1	knecht
Ruprecht	Ruprecht	k1gMnSc1	Ruprecht
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Černý	Černý	k1gMnSc1	Černý
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
verzích	verze	k1gFnPc6	verze
elfové	elf	k1gMnPc1	elf
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
hračky	hračka	k1gFnPc4	hračka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
bývá	bývat	k5eAaImIp3nS	bývat
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Mrs	Mrs	k1gFnSc1	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Claus	Claus	k1gMnSc1	Claus
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
jistý	jistý	k2eAgInSc4d1	jistý
rozpor	rozpor	k1gInSc4	rozpor
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
naznačené	naznačený	k2eAgFnSc6d1	naznačená
historii	historie	k1gFnSc6	historie
proměny	proměna	k1gFnSc2	proměna
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
na	na	k7c4	na
moderního	moderní	k2eAgMnSc4d1	moderní
Santa	Sant	k1gMnSc4	Sant
Clause	Clause	k1gFnSc2	Clause
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
The	The	k1gFnSc2	The
Saint	Sainta	k1gFnPc2	Sainta
Nicholas	Nicholas	k1gMnSc1	Nicholas
Society	societa	k1gFnSc2	societa
of	of	k?	of
the	the	k?	the
City	City	k1gFnSc2	City
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímž	jejíž	k3xOyRp3gMnPc3	jejíž
zakladatelům	zakladatel	k1gMnPc3	zakladatel
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
Washington	Washington	k1gInSc1	Washington
Irving	Irving	k1gInSc1	Irving
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
téměř	téměř	k6eAd1	téměř
půl	půl	k6eAd1	půl
století	století	k1gNnSc2	století
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
americké	americký	k2eAgFnSc2d1	americká
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
studie	studie	k1gFnPc1	studie
"	"	kIx"	"
<g/>
dětských	dětský	k2eAgFnPc2d1	dětská
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
periodik	periodikum	k1gNnPc2	periodikum
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
<g/>
"	"	kIx"	"
vydávaných	vydávaný	k2eAgInPc2d1	vydávaný
v	v	k7c6	v
New	New	k1gFnSc6	New
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
<g/>
,	,	kIx,	,
provedená	provedený	k2eAgFnSc1d1	provedená
Charlesem	Charles	k1gMnSc7	Charles
Jonesem	Jones	k1gMnSc7	Jones
<g/>
,	,	kIx,	,
neodhalila	odhalit	k5eNaPmAgFnS	odhalit
žádné	žádný	k3yNgInPc4	žádný
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
svatého	svatý	k2eAgMnSc4d1	svatý
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
nebo	nebo	k8xC	nebo
Sinterklaase	Sinterklaas	k1gInSc6	Sinterklaas
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
odborníci	odborník	k1gMnPc1	odborník
ovšem	ovšem	k9	ovšem
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
s	s	k7c7	s
jeho	jeho	k3xOp3gNnPc7	jeho
zjištěními	zjištění	k1gNnPc7	zjištění
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
uvedl	uvést	k5eAaPmAgMnS	uvést
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Howard	Howard	k1gMnSc1	Howard
G.	G.	kA	G.
Hageman	Hageman	k1gMnSc1	Hageman
z	z	k7c2	z
New	New	k1gFnSc2	New
Brunswického	Brunswický	k2eAgInSc2d1	Brunswický
teologického	teologický	k2eAgInSc2d1	teologický
semináře	seminář	k1gInSc2	seminář
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tradice	tradice	k1gFnSc1	tradice
slavení	slavení	k1gNnSc2	slavení
Sinterklaase	Sinterklaasa	k1gFnSc6	Sinterklaasa
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
byla	být	k5eAaImAgFnS	být
živá	živý	k2eAgFnSc1d1	živá
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
osídlení	osídlení	k1gNnSc2	osídlení
údolí	údolí	k1gNnSc2	údolí
Hudsonu	Hudson	k1gInSc2	Hudson
<g/>
.	.	kIx.	.
</s>
<s>
Father	Fathra	k1gFnPc2	Fathra
Christmas	Christmasa	k1gFnPc2	Christmasa
(	(	kIx(	(
<g/>
také	také	k9	také
česky	česky	k6eAd1	česky
Duch	duch	k1gMnSc1	duch
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
jako	jako	k8xS	jako
veselý	veselý	k2eAgMnSc1d1	veselý
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
živený	živený	k2eAgMnSc1d1	živený
<g/>
,	,	kIx,	,
vousatý	vousatý	k2eAgMnSc1d1	vousatý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zosobňuje	zosobňovat	k5eAaImIp3nS	zosobňovat
ducha	duch	k1gMnSc4	duch
dobré	dobrý	k2eAgFnSc2d1	dobrá
nálady	nálada	k1gFnSc2	nálada
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
vzezřením	vzezření	k1gNnSc7	vzezření
tak	tak	k6eAd1	tak
předcházel	předcházet	k5eAaImAgMnS	předcházet
symbolu	symbol	k1gInSc2	symbol
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
je	být	k5eAaImIp3nS	být
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
spojován	spojovat	k5eAaImNgInS	spojovat
se	s	k7c7	s
sváteční	sváteční	k2eAgFnSc7d1	sváteční
náladou	nálada	k1gFnSc7	nálada
<g/>
,	,	kIx,	,
bujarou	bujarý	k2eAgFnSc7d1	bujará
veselostí	veselost	k1gFnSc7	veselost
a	a	k8xC	a
opilostí	opilost	k1gFnSc7	opilost
spíše	spíše	k9	spíše
než	než	k8xS	než
s	s	k7c7	s
rozdáváním	rozdávání	k1gNnSc7	rozdávání
dárků	dárek	k1gInPc2	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
viktoriánské	viktoriánský	k2eAgFnSc6d1	viktoriánská
Británii	Británie	k1gFnSc6	Británie
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
přepracován	přepracovat	k5eAaPmNgInS	přepracovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
Santa	Santa	k1gMnSc1	Santa
Clausovi	Claus	k1gMnSc3	Claus
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
Pè	Pè	k1gMnSc1	Pè
Noël	Noël	k1gMnSc1	Noël
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
z	z	k7c2	z
podobného	podobný	k2eAgInSc2d1	podobný
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
přijetím	přijetí	k1gNnSc7	přijetí
obrazu	obraz	k1gInSc2	obraz
Santa	Santo	k1gNnSc2	Santo
Clause	Clause	k1gFnSc2	Clause
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
Babbo	Babba	k1gFnSc5	Babba
Natale	Natal	k1gInSc5	Natal
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
La	la	k1gNnSc7	la
Befana	Befana	k1gFnSc1	Befana
je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
darů	dar	k1gInPc2	dar
a	a	k8xC	a
přichází	přicházet	k5eAaImIp3nS	přicházet
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
Zjevení	zjevení	k1gNnSc2	zjevení
Páně	páně	k2eAgNnSc2d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
La	la	k1gNnSc4	la
Befana	Befan	k1gMnSc2	Befan
měla	mít	k5eAaImAgFnS	mít
přinést	přinést	k5eAaPmF	přinést
Ježíškovi	Ježíšek	k1gMnSc3	Ježíšek
dárky	dárek	k1gInPc4	dárek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ztratila	ztratit	k5eAaPmAgFnS	ztratit
se	se	k3xPyFc4	se
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
přináší	přinášet	k5eAaImIp3nS	přinášet
dárky	dárek	k1gInPc4	dárek
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
v	v	k7c6	v
několika	několik	k4yIc6	několik
latinskoamerických	latinskoamerický	k2eAgFnPc6d1	latinskoamerická
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Venezuela	Venezuela	k1gFnSc1	Venezuela
a	a	k8xC	a
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
)	)	kIx)	)
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
dělá	dělat	k5eAaImIp3nS	dělat
hračky	hračka	k1gFnPc4	hračka
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	on	k3xPp3gInPc4	on
dává	dávat	k5eAaImIp3nS	dávat
Ježíškovi	Ježíšek	k1gMnSc3	Ježíšek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
roznáší	roznášet	k5eAaImIp3nP	roznášet
dárky	dárek	k1gInPc4	dárek
do	do	k7c2	do
dětských	dětský	k2eAgInPc2d1	dětský
domovů	domov	k1gInPc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
smířením	smíření	k1gNnSc7	smíření
tradiční	tradiční	k2eAgFnSc2d1	tradiční
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
ikonografie	ikonografie	k1gFnSc2	ikonografie
Santa	Santo	k1gNnSc2	Santo
Clause	Clause	k1gFnSc2	Clause
importované	importovaný	k2eAgFnSc2d1	importovaná
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižním	jižní	k2eAgNnSc6d1	jižní
Tyrolsku	Tyrolsko	k1gNnSc6	Tyrolsko
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
jižním	jižní	k2eAgNnSc6d1	jižní
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Lichtenštejnsku	Lichtenštejnsko	k1gNnSc6	Lichtenštejnsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
přináší	přinášet	k5eAaImIp3nP	přinášet
dárky	dárek	k1gInPc4	dárek
Christkind	Christkinda	k1gFnPc2	Christkinda
(	(	kIx(	(
<g/>
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
Jézuska	Jézuska	k1gFnSc1	Jézuska
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
a	a	k8xC	a
Ježiško	Ježiška	k1gFnSc5	Ježiška
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgFnPc1d1	řecká
děti	dítě	k1gFnPc1	dítě
dostávají	dostávat	k5eAaImIp3nP	dostávat
dárky	dárek	k1gInPc7	dárek
od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Basila	Basil	k1gMnSc2	Basil
na	na	k7c4	na
Silvestra	Silvestr	k1gMnSc4	Silvestr
<g/>
,	,	kIx,	,
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
liturgické	liturgický	k2eAgFnSc2d1	liturgická
slavnosti	slavnost	k1gFnSc2	slavnost
tohoto	tento	k3xDgMnSc2	tento
světce	světec	k1gMnSc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
St.	st.	kA	st.
Nikolaus	Nikolaus	k1gInSc1	Nikolaus
není	být	k5eNaImIp3nS	být
totožný	totožný	k2eAgInSc1d1	totožný
s	s	k7c7	s
Weihnachtsmannem	Weihnachtsmann	k1gInSc7	Weihnachtsmann
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
německou	německý	k2eAgFnSc7d1	německá
verzí	verze	k1gFnSc7	verze
Santa	Santa	k1gMnSc1	Santa
Clause	Clause	k1gFnSc2	Clause
nebo	nebo	k8xC	nebo
Otce	otec	k1gMnSc2	otec
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
St.	st.	kA	st.
Nikolaus	Nikolaus	k1gInSc1	Nikolaus
nosí	nosit	k5eAaImIp3nS	nosit
biskupský	biskupský	k2eAgInSc1d1	biskupský
oděv	oděv	k1gInSc1	oděv
a	a	k8xC	a
stále	stále	k6eAd1	stále
přináší	přinášet	k5eAaImIp3nS	přinášet
drobné	drobný	k2eAgInPc4d1	drobný
dárky	dárek	k1gInPc4	dárek
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
bonbóny	bonbón	k1gInPc4	bonbón
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc4	ořech
a	a	k8xC	a
ovoce	ovoce	k1gNnPc4	ovoce
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
Knechtem	knecht	k1gMnSc7	knecht
Ruprechtem	Ruprecht	k1gMnSc7	Ruprecht
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
když	když	k8xS	když
mnoho	mnoho	k4c1	mnoho
rodičů	rodič	k1gMnPc2	rodič
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
běžně	běžně	k6eAd1	běžně
učí	učit	k5eAaImIp3nS	učit
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
o	o	k7c6	o
Santa	Santa	k1gMnSc1	Santa
Clausovi	Clausův	k2eAgMnPc1d1	Clausův
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
nositelích	nositel	k1gMnPc6	nositel
dárků	dárek	k1gInPc2	dárek
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
odmítají	odmítat	k5eAaImIp3nP	odmítat
tuto	tento	k3xDgFnSc4	tento
praktiku	praktika	k1gFnSc4	praktika
jako	jako	k8xS	jako
podvod	podvod	k1gInSc4	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Tři	tři	k4xCgFnPc4	tři
králové	králová	k1gFnPc4	králová
a	a	k8xC	a
Tříkrálová	tříkrálový	k2eAgFnSc1d1	Tříkrálová
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Mudrci	mudrc	k1gMnPc1	mudrc
z	z	k7c2	z
Východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
označovaní	označovaný	k2eAgMnPc1d1	označovaný
jako	jako	k8xC	jako
Tři	tři	k4xCgMnPc1	tři
králové	král	k1gMnPc1	král
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
postavy	postava	k1gFnPc1	postava
z	z	k7c2	z
Matoušova	Matoušův	k2eAgNnSc2d1	Matoušovo
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
mudrcové	mudrc	k1gMnPc1	mudrc
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Ježíše	Ježíš	k1gMnSc4	Ježíš
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
a	a	k8xC	a
přinesli	přinést	k5eAaPmAgMnP	přinést
mu	on	k3xPp3gMnSc3	on
dary	dar	k1gInPc1	dar
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
kadidlo	kadidlo	k1gNnSc4	kadidlo
a	a	k8xC	a
myrhu	myrha	k1gFnSc4	myrha
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
mudrcové	mudrc	k1gMnPc1	mudrc
byli	být	k5eAaImAgMnP	být
tři	tři	k4xCgMnPc1	tři
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
králové	král	k1gMnPc1	král
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
uváděná	uváděný	k2eAgNnPc1d1	uváděné
jako	jako	k8xC	jako
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Melichar	Melichar	k1gMnSc1	Melichar
a	a	k8xC	a
Baltazar	Baltazar	k1gMnSc1	Baltazar
<g/>
,	,	kIx,	,
přinesla	přinést	k5eAaPmAgFnS	přinést
až	až	k9	až
pozdější	pozdní	k2eAgFnPc4d2	pozdější
tradice	tradice	k1gFnPc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původních	původní	k2eAgInPc6d1	původní
řeckojazyčných	řeckojazyčný	k2eAgInPc6d1	řeckojazyčný
textech	text	k1gInPc6	text
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
mágy	mág	k1gMnPc4	mág
(	(	kIx(	(
<g/>
magoi	magoi	k6eAd1	magoi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
Vánočních	vánoční	k2eAgInPc2d1	vánoční
svátků	svátek	k1gInPc2	svátek
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nS	chodit
ještě	ještě	k9	ještě
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
koledovat	koledovat	k5eAaImF	koledovat
postavy	postava	k1gFnPc4	postava
imitující	imitující	k2eAgFnPc4d1	imitující
tyto	tento	k3xDgMnPc4	tento
tři	tři	k4xCgMnPc4	tři
mudrce	mudrc	k1gMnPc4	mudrc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolických	katolický	k2eAgFnPc6d1	katolická
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
o	o	k7c6	o
svátku	svátek	k1gInSc6	svátek
Zjevení	zjevení	k1gNnSc1	zjevení
Páně	páně	k2eAgFnSc2d1	páně
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
píše	psát	k5eAaImIp3nS	psát
posvěcenou	posvěcený	k2eAgFnSc7d1	posvěcená
křídou	křída	k1gFnSc7	křída
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
domů	dům	k1gInPc2	dům
a	a	k8xC	a
chlévů	chlév	k1gInPc2	chlév
zkratka	zkratka	k1gFnSc1	zkratka
K	k	k7c3	k
†	†	k?	†
M	M	kA	M
†	†	k?	†
B	B	kA	B
†	†	k?	†
nebo	nebo	k8xC	nebo
latinský	latinský	k2eAgInSc1d1	latinský
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
C	C	kA	C
†	†	k?	†
M	M	kA	M
†	†	k?	†
B	B	kA	B
†	†	k?	†
<g/>
.	.	kIx.	.
</s>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
se	se	k3xPyFc4	se
mylně	mylně	k6eAd1	mylně
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k8xC	jako
iniciály	iniciála	k1gFnPc4	iniciála
tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
(	(	kIx(	(
<g/>
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Melichar	Melichar	k1gMnSc1	Melichar
<g/>
,	,	kIx,	,
Baltazar	Baltazar	k1gMnSc1	Baltazar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
text	text	k1gInSc1	text
ovšem	ovšem	k9	ovšem
zní	znět	k5eAaImIp3nS	znět
Christus	Christus	k1gInSc1	Christus
mansionem	mansion	k1gInSc7	mansion
benedicat	benedicat	k5eAaPmF	benedicat
–	–	k?	–
Kristus	Kristus	k1gMnSc1	Kristus
ať	ať	k8xC	ať
obydlí	obydlí	k1gNnSc1	obydlí
žehná	žehnat	k5eAaImIp3nS	žehnat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Jesličky	jesličky	k1gFnPc1	jesličky
a	a	k8xC	a
Vánoční	vánoční	k2eAgInSc1d1	vánoční
stromek	stromek	k1gInSc1	stromek
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
svátek	svátek	k1gInSc4	svátek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
i	i	k9	i
pro	pro	k7c4	pro
nekřesťany	nekřesťan	k1gMnPc4	nekřesťan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jej	on	k3xPp3gInSc4	on
spojují	spojovat	k5eAaImIp3nP	spojovat
se	s	k7c7	s
zakončením	zakončení	k1gNnSc7	zakončení
roku	rok	k1gInSc2	rok
a	a	k8xC	a
s	s	k7c7	s
trávením	trávení	k1gNnSc7	trávení
času	čas	k1gInSc2	čas
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
kruhu	kruh	k1gInSc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
jsou	být	k5eAaImIp3nP	být
slaveny	slaven	k2eAgInPc4d1	slaven
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předchází	předcházet	k5eAaImIp3nS	předcházet
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
pojetí	pojetí	k1gNnSc2	pojetí
čtyřtýdenní	čtyřtýdenní	k2eAgNnSc4d1	čtyřtýdenní
období	období	k1gNnSc4	období
zvané	zvaný	k2eAgFnSc2d1	zvaná
advent	advent	k1gInSc4	advent
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
dny	den	k1gInPc1	den
jsou	být	k5eAaImIp3nP	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
státním	státní	k2eAgInSc7d1	státní
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vánočním	vánoční	k2eAgNnSc7d1	vánoční
slavením	slavení	k1gNnSc7	slavení
je	být	k5eAaImIp3nS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
množství	množství	k1gNnSc1	množství
místních	místní	k2eAgFnPc2d1	místní
či	či	k8xC	či
národních	národní	k2eAgFnPc2d1	národní
zvyklostí	zvyklost	k1gFnPc2	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
bylo	být	k5eAaImAgNnS	být
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
běžné	běžný	k2eAgNnSc1d1	běžné
strojení	strojení	k1gNnSc1	strojení
vánočního	vánoční	k2eAgInSc2d1	vánoční
stromku	stromek	k1gInSc2	stromek
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
dekorace	dekorace	k1gFnSc2	dekorace
stalo	stát	k5eAaPmAgNnS	stát
i	i	k9	i
osvětlení	osvětlení	k1gNnSc1	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
zvykem	zvyk	k1gInSc7	zvyk
je	být	k5eAaImIp3nS	být
rodinná	rodinný	k2eAgFnSc1d1	rodinná
nebo	nebo	k8xC	nebo
společná	společný	k2eAgFnSc1d1	společná
podoba	podoba	k1gFnSc1	podoba
oslavy	oslava	k1gFnSc2	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
tráví	trávit	k5eAaImIp3nP	trávit
svátky	svátek	k1gInPc4	svátek
společně	společně	k6eAd1	společně
a	a	k8xC	a
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
pohodě	pohoda	k1gFnSc6	pohoda
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
bývala	bývat	k5eAaImAgFnS	bývat
návštěva	návštěva	k1gFnSc1	návštěva
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
obstarán	obstarán	k2eAgInSc4d1	obstarán
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
poklizení	poklizení	k1gNnSc6	poklizení
a	a	k8xC	a
odzvonění	odzvonění	k1gNnSc6	odzvonění
klekání	klekání	k1gNnSc2	klekání
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
sešla	sejít	k5eAaPmAgFnS	sejít
u	u	k7c2	u
stolu	stol	k1gInSc2	stol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
poprvé	poprvé	k6eAd1	poprvé
vánoční	vánoční	k2eAgInSc4d1	vánoční
stromek	stromek	k1gInSc4	stromek
připravil	připravit	k5eAaPmAgMnS	připravit
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
ředitel	ředitel	k1gMnSc1	ředitel
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
J.	J.	kA	J.
K.	K.	kA	K.
Liebich	Liebich	k1gMnSc1	Liebich
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
libeňském	libeňský	k2eAgInSc6d1	libeňský
zámečku	zámeček	k1gInSc6	zámeček
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zvyk	zvyk	k1gInSc1	zvyk
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
v	v	k7c6	v
bohatých	bohatý	k2eAgFnPc6d1	bohatá
pražských	pražský	k2eAgFnPc6d1	Pražská
měšťanských	měšťanský	k2eAgFnPc6d1	měšťanská
rodinách	rodina	k1gFnPc6	rodina
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
i	i	k9	i
do	do	k7c2	do
venkovských	venkovský	k2eAgNnPc2d1	venkovské
stavení	stavení	k1gNnPc2	stavení
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
bývá	bývat	k5eAaImIp3nS	bývat
zvykem	zvyk	k1gInSc7	zvyk
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
stavět	stavět	k5eAaImF	stavět
jesličky	jesličky	k1gFnPc4	jesličky
neboli	neboli	k8xC	neboli
betlém	betlém	k1gInSc4	betlém
<g/>
:	:	kIx,	:
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
zvyk	zvyk	k1gInSc1	zvyk
objevuje	objevovat	k5eAaImIp3nS	objevovat
roku	rok	k1gInSc2	rok
1560	[number]	k4	1560
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Klimenta	Kliment	k1gMnSc2	Kliment
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Charakter	charakter	k1gInSc1	charakter
Vánoc	Vánoce	k1gFnPc2	Vánoce
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
také	také	k9	také
koledy	koleda	k1gFnPc1	koleda
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
tradice	tradice	k1gFnSc1	tradice
koled	koleda	k1gFnPc2	koleda
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zvláště	zvláště	k6eAd1	zvláště
bohatá	bohatý	k2eAgFnSc1d1	bohatá
<g/>
.	.	kIx.	.
</s>
<s>
Koledování	koledování	k1gNnSc1	koledování
se	se	k3xPyFc4	se
nezúčastňovaly	zúčastňovat	k5eNaImAgFnP	zúčastňovat
děti	dítě	k1gFnPc1	dítě
z	z	k7c2	z
bohatších	bohatý	k2eAgFnPc2d2	bohatší
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gMnPc1	jejich
rodiče	rodič	k1gMnPc1	rodič
jim	on	k3xPp3gMnPc3	on
nedovolovaly	dovolovat	k5eNaImAgFnP	dovolovat
"	"	kIx"	"
<g/>
žebrotu	žebrota	k1gFnSc4	žebrota
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Tři	tři	k4xCgMnPc4	tři
krále	král	k1gMnPc4	král
byly	být	k5eAaImAgFnP	být
předváděny	předvádět	k5eAaImNgFnP	předvádět
dětmi	dítě	k1gFnPc7	dítě
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
vánoční	vánoční	k2eAgInPc4d1	vánoční
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
odměnou	odměna	k1gFnSc7	odměna
byly	být	k5eAaImAgFnP	být
nejčastěji	často	k6eAd3	často
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vánoční	vánoční	k2eAgInPc4d1	vánoční
zvyky	zvyk	k1gInPc4	zvyk
a	a	k8xC	a
pověry	pověra	k1gFnPc4	pověra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
některé	některý	k3yIgFnPc4	některý
pranostiky	pranostika	k1gFnPc4	pranostika
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Zelené	Zelené	k2eAgFnPc1d1	Zelené
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgNnPc1d1	bílé
Velkonoc	velkonoce	k1gFnPc2	velkonoce
<g/>
.	.	kIx.	.
</s>
<s>
Štědrý	štědrý	k2eAgInSc1d1	štědrý
večer	večer	k1gInSc1	večer
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
lidmi	člověk	k1gMnPc7	člověk
odedávna	odedávna	k6eAd1	odedávna
za	za	k7c4	za
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
působí	působit	k5eAaImIp3nS	působit
silněji	silně	k6eAd2	silně
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
naplňovat	naplňovat	k5eAaImF	naplňovat
určité	určitý	k2eAgInPc4d1	určitý
magické	magický	k2eAgInPc4d1	magický
rituály	rituál	k1gInPc4	rituál
a	a	k8xC	a
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dopadu	dopad	k1gInSc3	dopad
zlých	zlý	k1gMnPc2	zlý
pověr	pověra	k1gFnPc2	pověra
a	a	k8xC	a
naklonit	naklonit	k5eAaPmF	naklonit
si	se	k3xPyFc3	se
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
síly	síla	k1gFnSc2	síla
ovládající	ovládající	k2eAgFnSc2d1	ovládající
věci	věc	k1gFnSc2	věc
budoucí	budoucí	k2eAgFnSc2d1	budoucí
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
prý	prý	k9	prý
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
snadněji	snadno	k6eAd2	snadno
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Rituály	rituál	k1gInPc1	rituál
pocházející	pocházející	k2eAgInPc1d1	pocházející
z	z	k7c2	z
předkřesťanských	předkřesťanský	k2eAgFnPc2d1	předkřesťanská
dob	doba	k1gFnPc2	doba
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
co	co	k9	co
jiného	jiný	k2eAgInSc2d1	jiný
<g/>
,	,	kIx,	,
nejhlubší	hluboký	k2eAgFnSc2d3	nejhlubší
lidské	lidský	k2eAgFnSc2d1	lidská
touhy	touha	k1gFnSc2	touha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zadělávání	zadělávání	k1gNnSc6	zadělávání
kynutého	kynutý	k2eAgNnSc2d1	kynuté
těsta	těsto	k1gNnSc2	těsto
měla	mít	k5eAaImAgFnS	mít
hospodyně	hospodyně	k1gFnSc1	hospodyně
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
rukama	ruka	k1gFnPc7	ruka
hladit	hladit	k5eAaImF	hladit
ovocné	ovocný	k2eAgInPc4d1	ovocný
stromy	strom	k1gInPc4	strom
a	a	k8xC	a
volat	volat	k5eAaImF	volat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stromečku	stromeček	k1gInSc2	stromeček
<g/>
,	,	kIx,	,
obroď	obrodit	k5eAaPmRp2nS	obrodit
<g/>
,	,	kIx,	,
obroď	obrodit	k5eAaPmRp2nS	obrodit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
plodné	plodný	k2eAgFnPc1d1	plodná
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
zvyky	zvyk	k1gInPc4	zvyk
se	s	k7c7	s
zřejmě	zřejmě	k6eAd1	zřejmě
pohanskými	pohanský	k2eAgInPc7d1	pohanský
kořeny	kořen	k1gInPc7	kořen
patří	patřit	k5eAaImIp3nS	patřit
zvyk	zvyk	k1gInSc1	zvyk
bdění	bdění	k1gNnSc2	bdění
(	(	kIx(	(
<g/>
vigilie	vigilie	k1gFnSc1	vigilie
<g/>
)	)	kIx)	)
a	a	k8xC	a
půstu	půst	k1gInSc2	půst
<g/>
.	.	kIx.	.
</s>
<s>
Přinejmenším	přinejmenším	k6eAd1	přinejmenším
mezi	mezi	k7c7	mezi
večeří	večeře	k1gFnSc7	večeře
a	a	k8xC	a
půlnocí	půlnoc	k1gFnSc7	půlnoc
jsou	být	k5eAaImIp3nP	být
hrány	hrát	k5eAaImNgFnP	hrát
karty	karta	k1gFnPc1	karta
a	a	k8xC	a
kostky	kostka	k1gFnPc1	kostka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hráči	hráč	k1gMnPc1	hráč
zjistili	zjistit	k5eAaPmAgMnP	zjistit
jaké	jaký	k3yIgNnSc4	jaký
štěstí	štěstí	k1gNnSc4	štěstí
je	být	k5eAaImIp3nS	být
čeká	čekat	k5eAaImIp3nS	čekat
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jim	on	k3xPp3gMnPc3	on
půjde	jít	k5eAaImIp3nS	jít
karta	karta	k1gFnSc1	karta
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
moderním	moderní	k2eAgInSc7d1	moderní
zvykem	zvyk	k1gInSc7	zvyk
je	být	k5eAaImIp3nS	být
procházka	procházka	k1gFnSc1	procházka
před	před	k7c7	před
večeří	večeře	k1gFnSc7	večeře
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
bývá	bývat	k5eAaImIp3nS	bývat
procházka	procházka	k1gFnSc1	procházka
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
moderním	moderní	k2eAgInSc7d1	moderní
vánočním	vánoční	k2eAgInSc7d1	vánoční
zvykem	zvyk	k1gInSc7	zvyk
pustit	pustit	k5eAaPmF	pustit
vánočního	vánoční	k2eAgMnSc4d1	vánoční
kapra	kapr	k1gMnSc4	kapr
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bývá	bývat	k5eAaImIp3nS	bývat
někým	někdo	k3yInSc7	někdo
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
záchranu	záchrana	k1gFnSc4	záchrana
pro	pro	k7c4	pro
uvedeného	uvedený	k2eAgMnSc4d1	uvedený
kapra	kapr	k1gMnSc4	kapr
<g/>
,	,	kIx,	,
odborníky	odborník	k1gMnPc4	odborník
však	však	k9	však
za	za	k7c4	za
rozsudek	rozsudek	k1gInSc4	rozsudek
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Štědrém	štědrý	k2eAgInSc6d1	štědrý
večeru	večer	k1gInSc6	večer
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
kapsy	kapsa	k1gFnSc2	kapsa
hospodář	hospodář	k1gMnSc1	hospodář
otevřené	otevřený	k2eAgFnSc2d1	otevřená
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
napadalo	napadat	k5eAaPmAgNnS	napadat
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přišel	přijít	k5eAaPmAgMnS	přijít
hospodář	hospodář	k1gMnSc1	hospodář
k	k	k7c3	k
bohatství	bohatství	k1gNnSc3	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Prát	prát	k5eAaImF	prát
prádlo	prádlo	k1gNnSc4	prádlo
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
přináší	přinášet	k5eAaImIp3nS	přinášet
smůlu	smůla	k1gFnSc4	smůla
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
rozhodně	rozhodně	k6eAd1	rozhodně
věšet	věšet	k5eAaImF	věšet
prádlo	prádlo	k1gNnSc4	prádlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
během	během	k7c2	během
roku	rok	k1gInSc2	rok
mohl	moct	k5eAaImAgMnS	moct
někdo	někdo	k3yInSc1	někdo
oběsit	oběsit	k5eAaPmF	oběsit
<g/>
.	.	kIx.	.
</s>
<s>
Zvykem	zvyk	k1gInSc7	zvyk
bylo	být	k5eAaImAgNnS	být
rozkrajování	rozkrajování	k1gNnSc1	rozkrajování
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
věštilo	věštit	k5eAaImAgNnS	věštit
zdraví	zdraví	k1gNnSc1	zdraví
či	či	k8xC	či
nemoc	nemoc	k1gFnSc1	nemoc
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
čistit	čistit	k5eAaImF	čistit
chlévy	chlév	k1gInPc4	chlév
a	a	k8xC	a
stáje	stáj	k1gFnPc4	stáj
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
dobytek	dobytek	k1gInSc1	dobytek
kulhal	kulhat	k5eAaImAgInS	kulhat
<g/>
.	.	kIx.	.
</s>
<s>
Nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
zametat	zametat	k5eAaImF	zametat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
domu	dům	k1gInSc2	dům
nebyli	být	k5eNaImAgMnP	být
vymeteni	vymeten	k2eAgMnPc1d1	vymeten
duchové	duch	k1gMnPc1	duch
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c4	v
tyto	tento	k3xDgFnPc4	tento
dny	dna	k1gFnPc4	dna
přicházejí	přicházet	k5eAaImIp3nP	přicházet
navštívit	navštívit	k5eAaPmF	navštívit
živé	živý	k2eAgInPc1d1	živý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
večeři	večeře	k1gFnSc6	večeře
jsou	být	k5eAaImIp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
svátečně	svátečně	k6eAd1	svátečně
oblečeni	oblečen	k2eAgMnPc1d1	oblečen
a	a	k8xC	a
jídelna	jídelna	k1gFnSc1	jídelna
je	být	k5eAaImIp3nS	být
vyzdobena	vyzdoben	k2eAgFnSc1d1	vyzdobena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stole	stol	k1gInSc6	stol
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ubrus	ubrus	k1gInSc4	ubrus
s	s	k7c7	s
vánočním	vánoční	k2eAgInSc7d1	vánoční
motivem	motiv	k1gInSc7	motiv
a	a	k8xC	a
zapálené	zapálený	k2eAgFnPc4d1	zapálená
svíce	svíce	k1gFnPc4	svíce
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pojídáním	pojídání	k1gNnSc7	pojídání
jídla	jídlo	k1gNnSc2	jídlo
se	se	k3xPyFc4	se
čeká	čekat	k5eAaImIp3nS	čekat
až	až	k8xS	až
bude	být	k5eAaImBp3nS	být
shromážděna	shromážděn	k2eAgFnSc1d1	shromážděna
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
večeří	večeře	k1gFnSc7	večeře
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
společně	společně	k6eAd1	společně
nahlas	nahlas	k6eAd1	nahlas
pomodlí	pomodlit	k5eAaPmIp3nS	pomodlit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
pronášeny	pronášen	k2eAgFnPc4d1	pronášena
přímluvy	přímluva	k1gFnPc4	přímluva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
večeři	večeře	k1gFnSc6	večeře
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
ryba	ryba	k1gFnSc1	ryba
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
kapr	kapr	k1gMnSc1	kapr
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
večeře	večeře	k1gFnSc2	večeře
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
země	zem	k1gFnSc2	zem
dodržují	dodržovat	k5eAaImIp3nP	dodržovat
některé	některý	k3yIgFnPc4	některý
zvyklosti	zvyklost	k1gFnPc4	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
rodinách	rodina	k1gFnPc6	rodina
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
o	o	k7c4	o
talíř	talíř	k1gInSc4	talíř
více	hodně	k6eAd2	hodně
pro	pro	k7c4	pro
nečekanou	čekaný	k2eNgFnSc4d1	nečekaná
návštěvu	návštěva	k1gFnSc4	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
zvyk	zvyk	k1gInSc1	zvyk
položit	položit	k5eAaPmF	položit
pod	pod	k7c4	pod
talíř	talíř	k1gInSc4	talíř
minci	mince	k1gFnSc4	mince
či	či	k8xC	či
šupinu	šupina	k1gFnSc4	šupina
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
zvyk	zvyk	k1gInSc1	zvyk
měl	mít	k5eAaImAgInS	mít
udržet	udržet	k5eAaPmF	udržet
peníze	peníz	k1gInPc4	peníz
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
přinést	přinést	k5eAaPmF	přinést
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
večeře	večeře	k1gFnSc2	večeře
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
luštěniny	luštěnina	k1gFnPc1	luštěnina
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
při	při	k7c6	při
večeři	večeře	k1gFnSc6	večeře
zkonzumována	zkonzumován	k2eAgFnSc1d1	zkonzumována
lžíce	lžíce	k1gFnSc1	lžíce
vařené	vařený	k2eAgFnSc2d1	vařená
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
dobrá	dobrý	k2eAgFnSc1d1	dobrá
úroda	úroda	k1gFnSc1	úroda
a	a	k8xC	a
spousta	spousta	k1gFnSc1	spousta
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
nedá	dát	k5eNaPmIp3nS	dát
nikomu	nikdo	k3yNnSc3	nikdo
dar	dar	k1gInSc4	dar
<g/>
,	,	kIx,	,
do	do	k7c2	do
roka	rok	k1gInSc2	rok
prý	prý	k9	prý
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
bídě	bída	k1gFnSc6	bída
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
večeři	večeře	k1gFnSc6	večeře
bývá	bývat	k5eAaImIp3nS	bývat
zvykem	zvyk	k1gInSc7	zvyk
rozbalovat	rozbalovat	k5eAaImF	rozbalovat
vánoční	vánoční	k2eAgInPc4d1	vánoční
dárky	dárek	k1gInPc4	dárek
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
zapalovány	zapalován	k2eAgFnPc4d1	zapalována
svíčky	svíčka	k1gFnPc4	svíčka
v	v	k7c6	v
lodičkách	lodička	k1gFnPc6	lodička
z	z	k7c2	z
ořechových	ořechový	k2eAgFnPc2d1	ořechová
skořápek	skořápka	k1gFnPc2	skořápka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pouštěny	pouštět	k5eAaImNgInP	pouštět
v	v	k7c6	v
lavóru	lavóru	k?	lavóru
(	(	kIx(	(
<g/>
velké	velký	k2eAgFnSc3d1	velká
míse	mísa	k1gFnSc3	mísa
sloužící	sloužící	k1gFnSc4	sloužící
k	k	k7c3	k
mytí	mytí	k1gNnSc3	mytí
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
na	na	k7c4	na
metr	metr	k1gInSc4	metr
vysokém	vysoký	k2eAgInSc6d1	vysoký
samostatném	samostatný	k2eAgInSc6d1	samostatný
stojanu	stojan	k1gInSc6	stojan
<g/>
)	)	kIx)	)
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Rituálem	rituál	k1gInSc7	rituál
bylo	být	k5eAaImAgNnS	být
házení	házení	k1gNnSc1	házení
střevíce	střevíc	k1gInSc2	střevíc
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
dívka	dívka	k1gFnSc1	dívka
další	další	k2eAgFnSc1d1	další
rok	rok	k1gInSc4	rok
vyvdá	vyvdat	k5eAaPmIp3nS	vyvdat
<g/>
,	,	kIx,	,
halekání	halekání	k1gNnSc2	halekání
z	z	k7c2	z
brázdy	brázda	k1gFnSc2	brázda
<g/>
,	,	kIx,	,
třesení	třesení	k1gNnSc2	třesení
bezem	bez	k1gInSc7	bez
nebo	nebo	k8xC	nebo
plotem	plot	k1gInSc7	plot
a	a	k8xC	a
poslouchání	poslouchání	k1gNnSc1	poslouchání
z	z	k7c2	z
které	který	k3yIgFnSc2	který
strany	strana	k1gFnSc2	strana
štěká	štěkat	k5eAaImIp3nS	štěkat
pes	pes	k1gMnSc1	pes
<g/>
:	:	kIx,	:
Města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
vánočně	vánočně	k6eAd1	vánočně
vyzdobena	vyzdoben	k2eAgNnPc1d1	vyzdobeno
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
domy	dům	k1gInPc1	dům
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
okna	okno	k1gNnPc1	okno
a	a	k8xC	a
dveře	dveře	k1gFnPc1	dveře
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Adventní	adventní	k2eAgFnSc1d1	adventní
svíce	svíce	k1gFnSc1	svíce
na	na	k7c6	na
adventním	adventní	k2eAgInSc6d1	adventní
věnci	věnec	k1gInSc6	věnec
se	se	k3xPyFc4	se
zapalují	zapalovat	k5eAaImIp3nP	zapalovat
postupně	postupně	k6eAd1	postupně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Soboty	sobota	k1gFnPc4	sobota
a	a	k8xC	a
neděle	neděle	k1gFnPc4	neděle
tráví	trávit	k5eAaImIp3nP	trávit
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
kteří	který	k3yQgMnPc1	který
tyto	tento	k3xDgInPc4	tento
dny	den	k1gInPc4	den
nemusí	muset	k5eNaImIp3nS	muset
trávit	trávit	k5eAaImF	trávit
prací	práce	k1gFnSc7	práce
<g/>
,	,	kIx,	,
svátečně	svátečně	k6eAd1	svátečně
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
vycházkami	vycházka	k1gFnPc7	vycházka
do	do	k7c2	do
přírody	příroda	k1gFnSc2	příroda
nebo	nebo	k8xC	nebo
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
trhy	trh	k1gInPc1	trh
<g/>
,	,	kIx,	,
muzea	muzeum	k1gNnPc1	muzeum
<g/>
,	,	kIx,	,
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
galerie	galerie	k1gFnPc1	galerie
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
hezká	hezký	k2eAgNnPc1d1	hezké
místa	místo	k1gNnPc1	místo
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
období	období	k1gNnSc1	období
před	před	k7c7	před
Vánocemi	Vánoce	k1gFnPc7	Vánoce
má	mít	k5eAaImIp3nS	mít
sváteční	sváteční	k2eAgInSc1d1	sváteční
nádech	nádech	k1gInSc1	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
před	před	k7c7	před
Vánocemi	Vánoce	k1gFnPc7	Vánoce
obyvatelé	obyvatel	k1gMnPc1	obyvatel
uklidí	uklidit	k5eAaPmIp3nP	uklidit
a	a	k8xC	a
vyčistí	vyčistit	k5eAaPmIp3nP	vyčistit
celý	celý	k2eAgInSc4d1	celý
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
nakupují	nakupovat	k5eAaBmIp3nP	nakupovat
dárky	dárek	k1gInPc4	dárek
a	a	k8xC	a
pečou	péct	k5eAaImIp3nP	péct
cukroví	cukroví	k1gNnSc4	cukroví
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vánočním	vánoční	k2eAgNnSc6d1	vánoční
období	období	k1gNnSc6	období
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ČR	ČR	kA	ČR
časté	častý	k2eAgInPc4d1	častý
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
mají	mít	k5eAaImIp3nP	mít
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
učitelek	učitelka	k1gFnPc2	učitelka
vystoupení	vystoupení	k1gNnSc2	vystoupení
se	s	k7c7	s
zpěvem	zpěv	k1gInSc7	zpěv
a	a	k8xC	a
hrami	hra	k1gFnPc7	hra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
školkách	školka	k1gFnPc6	školka
ukázky	ukázka	k1gFnSc2	ukázka
prací	práce	k1gFnPc2	práce
s	s	k7c7	s
vánoční	vánoční	k2eAgFnSc7d1	vánoční
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
připravovány	připravován	k2eAgInPc1d1	připravován
betlémy	betlém	k1gInPc1	betlém
<g/>
,	,	kIx,	,
jesličky	jesličky	k1gFnPc1	jesličky
často	často	k6eAd1	často
se	s	k7c7	s
živými	živý	k2eAgMnPc7d1	živý
herci	herec	k1gMnPc7	herec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Vánocích	Vánoce	k1gFnPc6	Vánoce
chodí	chodit	k5eAaImIp3nP	chodit
děti	dítě	k1gFnPc1	dítě
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
katechetky	katechetka	k1gFnSc2	katechetka
nebo	nebo	k8xC	nebo
katecheta	katecheta	k1gMnSc1	katecheta
dům	dům	k1gInSc4	dům
od	od	k7c2	od
domu	dům	k1gInSc2	dům
vybírat	vybírat	k5eAaImF	vybírat
peníze	peníz	k1gInPc1	peníz
na	na	k7c4	na
Tříkrálovou	tříkrálový	k2eAgFnSc4d1	Tříkrálová
sbírku	sbírka	k1gFnSc4	sbírka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
Katolická	katolický	k2eAgFnSc1d1	katolická
Charita	charita	k1gFnSc1	charita
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
obdarování	obdarování	k1gNnSc6	obdarování
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
již	již	k6eAd1	již
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
Vánoc	Vánoce	k1gFnPc2	Vánoce
se	se	k3xPyFc4	se
však	však	k9	však
dárky	dárek	k1gInPc1	dárek
staly	stát	k5eAaPmAgInP	stát
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
obdarovávat	obdarovávat	k5eAaImF	obdarovávat
členy	člen	k1gInPc4	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
služebnictvo	služebnictvo	k1gNnSc1	služebnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
chasa	chasa	k1gFnSc1	chasa
například	například	k6eAd1	například
dostávala	dostávat	k5eAaImAgFnS	dostávat
ošacení	ošacení	k1gNnSc4	ošacení
nebo	nebo	k8xC	nebo
malou	malý	k2eAgFnSc7d1	malá
finanční	finanční	k2eAgFnSc7d1	finanční
částkou	částka	k1gFnSc7	částka
na	na	k7c4	na
přilepšení	přilepšení	k1gNnSc4	přilepšení
k	k	k7c3	k
celoroční	celoroční	k2eAgFnSc3d1	celoroční
mzdě	mzda	k1gFnSc3	mzda
<g/>
.	.	kIx.	.
</s>
<s>
Dárky	dárek	k1gInPc1	dárek
se	se	k3xPyFc4	se
dávaly	dávat	k5eAaImAgInP	dávat
také	také	k9	také
žebrákům	žebrák	k1gMnPc3	žebrák
nebo	nebo	k8xC	nebo
tulákům	tulák	k1gMnPc3	tulák
bez	bez	k7c2	bez
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
svátků	svátek	k1gInPc2	svátek
zaklepali	zaklepat	k5eAaPmAgMnP	zaklepat
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
bohatších	bohatý	k2eAgFnPc2d2	bohatší
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vánočním	vánoční	k2eAgInSc7d1	vánoční
stromkem	stromek	k1gInSc7	stromek
vyjma	vyjma	k7c2	vyjma
potřebných	potřebný	k2eAgFnPc2d1	potřebná
věcí	věc	k1gFnPc2	věc
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
objevovaly	objevovat	k5eAaImAgFnP	objevovat
drobnosti	drobnost	k1gFnPc1	drobnost
a	a	k8xC	a
hračky	hračka	k1gFnPc1	hračka
jen	jen	k9	jen
pro	pro	k7c4	pro
radost	radost	k1gFnSc4	radost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chudých	chudý	k2eAgInPc6d1	chudý
letech	let	k1gInPc6	let
mezi	mezi	k7c7	mezi
dárky	dárek	k1gInPc7	dárek
byly	být	k5eAaImAgFnP	být
i	i	k9	i
oděvní	oděvní	k2eAgFnPc1d1	oděvní
součástky	součástka	k1gFnPc1	součástka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vánoční	vánoční	k2eAgInPc4d1	vánoční
pokrmy	pokrm	k1gInPc4	pokrm
a	a	k8xC	a
nápoje	nápoj	k1gInPc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
matka	matka	k1gFnSc1	matka
organizovala	organizovat	k5eAaBmAgFnS	organizovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dcery	dcera	k1gFnPc1	dcera
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
pekly	péct	k5eAaImAgFnP	péct
z	z	k7c2	z
"	"	kIx"	"
<g/>
bílé	bílý	k2eAgFnSc2d1	bílá
mouky	mouka	k1gFnSc2	mouka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
koláče	koláč	k1gInPc1	koláč
"	"	kIx"	"
<g/>
pecaky	pecak	k1gInPc1	pecak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
lopaťaky	lopaťak	k1gMnPc7	lopaťak
<g/>
"	"	kIx"	"
hruškové	hruškový	k2eAgNnSc4d1	hruškové
<g/>
,	,	kIx,	,
zelné	zelný	k2eAgNnSc4d1	zelné
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
makovníky	makovníky	k?	makovníky
<g/>
,	,	kIx,	,
makové	makový	k2eAgFnPc1d1	maková
bábovky	bábovka	k1gFnPc1	bábovka
(	(	kIx(	(
<g/>
buchty	buchta	k1gFnPc1	buchta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
jablečný	jablečný	k2eAgInSc1d1	jablečný
závin	závin	k1gInSc1	závin
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
bývá	bývat	k5eAaImIp3nS	bývat
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
na	na	k7c6	na
slavnostní	slavnostní	k2eAgFnSc6d1	slavnostní
večeři	večeře	k1gFnSc6	večeře
<g/>
.	.	kIx.	.
</s>
<s>
Společná	společný	k2eAgFnSc1d1	společná
vánoční	vánoční	k2eAgFnSc1d1	vánoční
rodinná	rodinný	k2eAgFnSc1d1	rodinná
večeře	večeře	k1gFnSc1	večeře
se	s	k7c7	s
speciálním	speciální	k2eAgNnSc7d1	speciální
jídlem	jídlo	k1gNnSc7	jídlo
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
oslav	oslava	k1gFnPc2	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
svátků	svátek	k1gInPc2	svátek
se	se	k3xPyFc4	se
podávají	podávat	k5eAaImIp3nP	podávat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
tradiční	tradiční	k2eAgInPc4d1	tradiční
pokrmy	pokrm	k1gInPc4	pokrm
než	než	k8xS	než
obvyklé	obvyklý	k2eAgInPc4d1	obvyklý
dny	den	k1gInPc4	den
nebo	nebo	k8xC	nebo
svátky	svátek	k1gInPc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
večeře	večeře	k1gFnSc1	večeře
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
rybí	rybí	k2eAgFnSc2d1	rybí
polévky	polévka	k1gFnSc2	polévka
<g/>
,	,	kIx,	,
bramborového	bramborový	k2eAgInSc2d1	bramborový
salátu	salát	k1gInSc2	salát
s	s	k7c7	s
kaprem	kapr	k1gMnSc7	kapr
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
příprav	příprava	k1gFnPc2	příprava
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
peče	péct	k5eAaImIp3nS	péct
vánoční	vánoční	k2eAgNnSc4d1	vánoční
cukroví	cukroví	k1gNnSc4	cukroví
<g/>
,	,	kIx,	,
vánočka	vánočka	k1gFnSc1	vánočka
a	a	k8xC	a
štóla	štóla	k1gFnSc1	štóla
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
vánočních	vánoční	k2eAgNnPc2d1	vánoční
cukroví	cukroví	k1gNnSc2	cukroví
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
másla	máslo	k1gNnSc2	máslo
<g/>
,	,	kIx,	,
kakaa	kakao	k1gNnSc2	kakao
<g/>
,	,	kIx,	,
čokolády	čokoláda	k1gFnSc2	čokoláda
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
ořechů	ořech	k1gInPc2	ořech
a	a	k8xC	a
kandovaného	kandovaný	k2eAgNnSc2d1	kandované
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
typické	typický	k2eAgNnSc4d1	typické
patří	patřit	k5eAaImIp3nS	patřit
cukroví	cukroví	k1gNnSc4	cukroví
z	z	k7c2	z
lineckého	linecký	k2eAgNnSc2d1	linecké
těsta	těsto	k1gNnSc2	těsto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
navíc	navíc	k6eAd1	navíc
ochutit	ochutit	k5eAaPmF	ochutit
strouhaným	strouhaný	k2eAgInSc7d1	strouhaný
kokosem	kokos	k1gInSc7	kokos
nebo	nebo	k8xC	nebo
kakaem	kakao	k1gNnSc7	kakao
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgFnPc7d1	typická
vůněmi	vůně	k1gFnPc7	vůně
jsou	být	k5eAaImIp3nP	být
vanilka	vanilka	k1gFnSc1	vanilka
a	a	k8xC	a
rum	rum	k1gInSc1	rum
<g/>
,	,	kIx,	,
typickým	typický	k2eAgNnSc7d1	typické
kořením	koření	k1gNnSc7	koření
skořice	skořice	k1gFnSc2	skořice
<g/>
.	.	kIx.	.
</s>
<s>
Vánočka	vánočka	k1gFnSc1	vánočka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
sladkého	sladký	k2eAgNnSc2d1	sladké
pleteného	pletený	k2eAgNnSc2d1	pletené
pečiva	pečivo	k1gNnSc2	pečivo
z	z	k7c2	z
kynutého	kynutý	k2eAgNnSc2d1	kynuté
těsta	těsto	k1gNnSc2	těsto
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
připomínat	připomínat	k5eAaImF	připomínat
malého	malý	k2eAgMnSc4d1	malý
Ježíška	Ježíšek	k1gMnSc4	Ježíšek
zabaleného	zabalený	k2eAgMnSc4d1	zabalený
v	v	k7c6	v
peřince	peřinka	k1gFnSc6	peřinka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
symbolem	symbol	k1gInSc7	symbol
nového	nový	k2eAgInSc2d1	nový
života	život	k1gInSc2	život
a	a	k8xC	a
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Štóla	štóla	k1gFnSc1	štóla
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
pečeného	pečený	k2eAgInSc2d1	pečený
vánočního	vánoční	k2eAgInSc2d1	vánoční
moučníku	moučník	k1gInSc2	moučník
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
bochníku	bochník	k1gInSc2	bochník
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
zapečené	zapečený	k2eAgNnSc1d1	zapečené
například	například	k6eAd1	například
sušené	sušený	k2eAgNnSc1d1	sušené
ovoce	ovoce	k1gNnSc1	ovoce
a	a	k8xC	a
ořechy	ořech	k1gInPc1	ořech
a	a	k8xC	a
z	z	k7c2	z
vrchu	vrch	k1gInSc2	vrch
je	být	k5eAaImIp3nS	být
štóla	štóla	k1gFnSc1	štóla
posypaná	posypaný	k2eAgFnSc1d1	posypaná
moučkovým	moučkový	k2eAgInSc7d1	moučkový
cukrem	cukr	k1gInSc7	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
zvyku	zvyk	k1gInSc2	zvyk
nemá	mít	k5eNaImIp3nS	mít
do	do	k7c2	do
štědrovečerní	štědrovečerní	k2eAgFnSc2d1	štědrovečerní
večeře	večeře	k1gFnSc2	večeře
jíst	jíst	k5eAaImF	jíst
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
polední	polední	k2eAgNnSc1d1	polední
jídlo	jídlo	k1gNnSc1	jídlo
se	se	k3xPyFc4	se
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
podává	podávat	k5eAaImIp3nS	podávat
kuba	kuba	k1gNnSc1	kuba
(	(	kIx(	(
<g/>
také	také	k6eAd1	také
staročeský	staročeský	k2eAgInSc1d1	staročeský
kuba	kuba	k1gNnSc4	kuba
<g/>
,	,	kIx,	,
černý	černý	k1gMnSc1	černý
kuba	kuba	k1gNnSc2	kuba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tradiční	tradiční	k2eAgNnSc4d1	tradiční
české	český	k2eAgNnSc4d1	české
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
základem	základ	k1gInSc7	základ
jsou	být	k5eAaImIp3nP	být
kroupy	kroupa	k1gFnPc4	kroupa
a	a	k8xC	a
houby	houba	k1gFnPc4	houba
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
stroček	stroček	k1gInSc1	stroček
trubkovitý	trubkovitý	k2eAgInSc1d1	trubkovitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
chodem	chod	k1gInSc7	chod
štědrovečerní	štědrovečerní	k2eAgFnSc2d1	štědrovečerní
večeře	večeře	k1gFnSc2	večeře
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
tradičně	tradičně	k6eAd1	tradičně
kapr	kapr	k1gMnSc1	kapr
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
obalovaný	obalovaný	k2eAgInSc1d1	obalovaný
a	a	k8xC	a
osmažený	osmažený	k2eAgInSc1d1	osmažený
s	s	k7c7	s
bramborovým	bramborový	k2eAgInSc7d1	bramborový
salátem	salát	k1gInSc7	salát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
některých	některý	k3yIgFnPc2	některý
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
jikry	jikra	k1gFnPc1	jikra
<g/>
,	,	kIx,	,
mlíčí	mlíčí	k1gNnSc1	mlíčí
<g/>
,	,	kIx,	,
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc1	kost
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
masa	maso	k1gNnSc2	maso
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
chutná	chutný	k2eAgFnSc1d1	chutná
tradiční	tradiční	k2eAgFnSc1d1	tradiční
polévka	polévka	k1gFnSc1	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc7d1	tradiční
uzeninou	uzenina	k1gFnSc7	uzenina
je	být	k5eAaImIp3nS	být
vinná	vinný	k2eAgFnSc1d1	vinná
klobása	klobása	k1gFnSc1	klobása
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
také	také	k9	také
smaží	smažit	k5eAaImIp3nS	smažit
vepřové	vepřový	k2eAgNnSc1d1	vepřové
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kuřecí	kuřecí	k2eAgInPc4d1	kuřecí
řízky	řízek	k1gInPc4	řízek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oblíbeným	oblíbený	k2eAgInPc3d1	oblíbený
nápojům	nápoj	k1gInPc3	nápoj
patří	patřit	k5eAaImIp3nS	patřit
svařené	svařený	k2eAgNnSc4d1	svařené
víno	víno	k1gNnSc4	víno
a	a	k8xC	a
vaječný	vaječný	k2eAgInSc4d1	vaječný
koňak	koňak	k1gInSc4	koňak
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
novodobých	novodobý	k2eAgFnPc2d1	novodobá
vánočních	vánoční	k2eAgFnPc2d1	vánoční
tradic	tradice	k1gFnPc2	tradice
je	být	k5eAaImIp3nS	být
betlémské	betlémský	k2eAgNnSc4d1	Betlémské
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
rozvážejí	rozvážet	k5eAaImIp3nP	rozvážet
skauti	skaut	k1gMnPc1	skaut
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
zapaluje	zapalovat	k5eAaImIp3nS	zapalovat
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
putuje	putovat	k5eAaImIp3nS	putovat
napříč	napříč	k7c7	napříč
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
betlémské	betlémský	k2eAgNnSc1d1	Betlémské
světlo	světlo	k1gNnSc1	světlo
putuje	putovat	k5eAaImIp3nS	putovat
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
rakouští	rakouský	k2eAgMnPc1d1	rakouský
skauti	skaut	k1gMnPc1	skaut
předávají	předávat	k5eAaImIp3nP	předávat
o	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
neděli	neděle	k1gFnSc6	neděle
adventní	adventní	k2eAgFnSc6d1	adventní
(	(	kIx(	(
<g/>
Gaudete	Gaudete	k1gFnSc6	Gaudete
<g/>
)	)	kIx)	)
delegacím	delegace	k1gFnPc3	delegace
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
jej	on	k3xPp3gInSc2	on
přebírají	přebírat	k5eAaImIp3nP	přebírat
brněnští	brněnský	k2eAgMnPc1d1	brněnský
skauti	skaut	k1gMnPc1	skaut
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
rozvážejí	rozvážet	k5eAaImIp3nP	rozvážet
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
před	před	k7c7	před
4	[number]	k4	4
<g/>
.	.	kIx.	.
nedělí	dělit	k5eNaImIp3nS	dělit
<g />
.	.	kIx.	.
</s>
<s>
adventní	adventní	k2eAgNnSc1d1	adventní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
kostelech	kostel	k1gInPc6	kostel
<g/>
,	,	kIx,	,
na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
<g/>
,	,	kIx,	,
vánočních	vánoční	k2eAgInPc6d1	vánoční
trzích	trh	k1gInPc6	trh
<g/>
,	,	kIx,	,
knihovnách	knihovna	k1gFnPc6	knihovna
apod.	apod.	kA	apod.
Vánoce	Vánoce	k1gFnPc1	Vánoce
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
křesťanství	křesťanství	k1gNnSc1	křesťanství
nemá	mít	k5eNaImIp3nS	mít
silnou	silný	k2eAgFnSc4d1	silná
tradici	tradice	k1gFnSc4	tradice
či	či	k8xC	či
stoupence	stoupenec	k1gMnPc4	stoupenec
<g/>
;	;	kIx,	;
slaví	slavit	k5eAaImIp3nS	slavit
se	se	k3xPyFc4	se
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
Asii	Asie	k1gFnSc3	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
oslava	oslava	k1gFnSc1	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
se	se	k3xPyFc4	se
však	však	k9	však
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
historické	historický	k2eAgFnPc4d1	historická
tradice	tradice	k1gFnPc4	tradice
či	či	k8xC	či
víru	víra	k1gFnSc4	víra
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
jsou	být	k5eAaImIp3nP	být
Vánoce	Vánoce	k1gFnPc1	Vánoce
slaveny	slaven	k2eAgFnPc1d1	slavena
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
náboženské	náboženský	k2eAgFnSc2d1	náboženská
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
obdarováváním	obdarovávání	k1gNnSc7	obdarovávání
bližních	bližní	k1gMnPc2	bližní
dárky	dárek	k1gInPc4	dárek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
rozbalovány	rozbalován	k2eAgInPc1d1	rozbalován
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
ráno	ráno	k1gNnSc1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
nesmí	smět	k5eNaImIp3nS	smět
chybět	chybět	k5eAaImF	chybět
vánoční	vánoční	k2eAgInSc4d1	vánoční
stromek	stromek	k1gInSc4	stromek
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
též	též	k9	též
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
venkovní	venkovní	k2eAgNnSc4d1	venkovní
zdobení	zdobení	k1gNnSc4	zdobení
domů	dům	k1gInPc2	dům
vánoční	vánoční	k2eAgFnSc7d1	vánoční
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
,	,	kIx,	,
především	především	k9	především
osvětlením	osvětlení	k1gNnSc7	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
štědrovečerním	štědrovečerní	k2eAgInSc7d1	štědrovečerní
pokrmem	pokrm	k1gInSc7	pokrm
je	být	k5eAaImIp3nS	být
nadívaný	nadívaný	k2eAgMnSc1d1	nadívaný
krocan	krocan	k1gMnSc1	krocan
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jiná	jiný	k2eAgFnSc1d1	jiná
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
s	s	k7c7	s
pečenými	pečený	k2eAgFnPc7d1	pečená
bramborami	brambora	k1gFnPc7	brambora
<g/>
,	,	kIx,	,
zeleninou	zelenina	k1gFnSc7	zelenina
a	a	k8xC	a
brusinkovou	brusinkový	k2eAgFnSc7d1	brusinková
omáčkou	omáčka	k1gFnSc7	omáčka
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
dezertem	dezert	k1gInSc7	dezert
je	být	k5eAaImIp3nS	být
vánoční	vánoční	k2eAgInSc1d1	vánoční
pudink	pudink	k1gInSc1	pudink
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
Vánocemi	Vánoce	k1gFnPc7	Vánoce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
alkoholu	alkohol	k1gInSc2	alkohol
velmi	velmi	k6eAd1	velmi
trvanlivý	trvanlivý	k2eAgInSc1d1	trvanlivý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
vyrobený	vyrobený	k2eAgMnSc1d1	vyrobený
z	z	k7c2	z
oříšků	oříšek	k1gInPc2	oříšek
<g/>
,	,	kIx,	,
hrozinek	hrozinka	k1gFnPc2	hrozinka
<g/>
,	,	kIx,	,
rumu	rum	k1gInSc2	rum
<g/>
,	,	kIx,	,
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
strouhanky	strouhanka	k1gFnSc2	strouhanka
<g/>
,	,	kIx,	,
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
citronů	citron	k1gInPc2	citron
a	a	k8xC	a
švestek	švestka	k1gFnPc2	švestka
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
s	s	k7c7	s
koňakem	koňak	k1gInSc7	koňak
nebo	nebo	k8xC	nebo
punčem	punč	k1gInSc7	punč
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
nadívaný	nadívaný	k2eAgMnSc1d1	nadívaný
krocan	krocan	k1gMnSc1	krocan
tradičním	tradiční	k2eAgInSc7d1	tradiční
pokrmem	pokrm	k1gInSc7	pokrm
též	též	k9	též
na	na	k7c4	na
Den	den	k1gInSc4	den
díkůvzdání	díkůvzdání	k1gNnSc2	díkůvzdání
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
zde	zde	k6eAd1	zde
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
u	u	k7c2	u
štědrovečerní	štědrovečerní	k2eAgFnSc2d1	štědrovečerní
večeře	večeře	k1gFnSc2	večeře
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
jiným	jiný	k2eAgNnSc7d1	jiné
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
se	se	k3xPyFc4	se
podávají	podávat	k5eAaImIp3nP	podávat
ústřice	ústřice	k1gFnPc1	ústřice
<g/>
,	,	kIx,	,
šunkové	šunkový	k2eAgFnPc1d1	šunková
placky	placka	k1gFnPc1	placka
a	a	k8xC	a
nadýchané	nadýchaný	k2eAgInPc1d1	nadýchaný
oplatky	oplatek	k1gInPc1	oplatek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
středozápadě	středozápad	k1gInSc6	středozápad
s	s	k7c7	s
původně	původně	k6eAd1	původně
skandinávským	skandinávský	k2eAgNnSc7d1	skandinávské
osídlením	osídlení	k1gNnSc7	osídlení
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
tzv.	tzv.	kA	tzv.
lutefisk	lutefisk	k1gInSc1	lutefisk
(	(	kIx(	(
<g/>
speciálně	speciálně	k6eAd1	speciálně
upravená	upravený	k2eAgFnSc1d1	upravená
ryba	ryba	k1gFnSc1	ryba
<g/>
)	)	kIx)	)
s	s	k7c7	s
tuřínem	tuřín	k1gInSc7	tuřín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
venkovských	venkovský	k2eAgFnPc6d1	venkovská
oblastech	oblast	k1gFnPc6	oblast
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
též	též	k9	též
podáváno	podáván	k2eAgNnSc4d1	podáváno
maso	maso	k1gNnSc4	maso
ze	z	k7c2	z
soba	sob	k1gMnSc2	sob
<g/>
,	,	kIx,	,
vačice	vačice	k1gFnSc2	vačice
či	či	k8xC	či
křepela	křepel	k1gMnSc2	křepel
<g/>
.	.	kIx.	.
</s>
<s>
Dárky	dárek	k1gInPc1	dárek
v	v	k7c6	v
anglosaských	anglosaský	k2eAgFnPc6d1	anglosaská
zemích	zem	k1gFnPc6	zem
roznáší	roznášet	k5eAaImIp3nS	roznášet
starší	starý	k2eAgFnSc1d2	starší
mužská	mužský	k2eAgFnSc1d1	mužská
postava	postava	k1gFnSc1	postava
jménem	jméno	k1gNnSc7	jméno
Santa	Santo	k1gNnSc2	Santo
Claus	Claus	k1gInSc1	Claus
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
též	též	k9	též
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Father	Fathra	k1gFnPc2	Fathra
Christmas	Christmas	k1gInSc1	Christmas
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Otec	otec	k1gMnSc1	otec
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vozí	vozit	k5eAaImIp3nS	vozit
dárky	dárek	k1gInPc4	dárek
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
podáních	podání	k1gNnPc6	podání
tažených	tažený	k2eAgMnPc2d1	tažený
soby	soba	k1gFnSc2	soba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc4	dítě
prožívají	prožívat	k5eAaImIp3nP	prožívat
více	hodně	k6eAd2	hodně
svátek	svátek	k1gInSc4	svátek
Sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
než	než	k8xS	než
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
totiž	totiž	k9	totiž
ty	ten	k3xDgInPc1	ten
hodnější	hodný	k2eAgInPc1d2	hodnější
dostávají	dostávat	k5eAaImIp3nP	dostávat
dárky	dárek	k1gInPc1	dárek
od	od	k7c2	od
Sinterklaase	Sinterklaasa	k1gFnSc6	Sinterklaasa
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Vlámsku	Vlámsek	k1gInSc6	Vlámsek
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Saint	Saint	k1gInSc1	Saint
Nicholase	Nicholasa	k1gFnSc3	Nicholasa
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Valonsku	Valonsko	k1gNnSc6	Valonsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
zlobivějším	zlobivý	k2eAgMnPc3d2	zlobivější
pak	pak	k6eAd1	pak
hrozí	hrozit	k5eAaImIp3nP	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
odnese	odnést	k5eAaPmIp3nS	odnést
Černý	Černý	k1gMnSc1	Černý
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
prý	prý	k9	prý
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInPc1d1	vánoční
svátky	svátek	k1gInPc1	svátek
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
zasvěceny	zasvětit	k5eAaPmNgInP	zasvětit
spíše	spíše	k9	spíše
setkávání	setkávání	k1gNnPc2	setkávání
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
namísto	namísto	k7c2	namísto
rozdávání	rozdávání	k1gNnSc2	rozdávání
dárků	dárek	k1gInPc2	dárek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
však	však	k8xC	však
i	i	k9	i
Belgie	Belgie	k1gFnSc1	Belgie
podléhá	podléhat	k5eAaImIp3nS	podléhat
komerčním	komerční	k2eAgInPc3d1	komerční
vlivům	vliv	k1gInPc3	vliv
a	a	k8xC	a
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
obvykle	obvykle	k6eAd1	obvykle
pod	pod	k7c4	pod
vánoční	vánoční	k2eAgInSc4d1	vánoční
stromek	stromek	k1gInSc4	stromek
či	či	k8xC	či
do	do	k7c2	do
punčoch	punčocha	k1gFnPc2	punčocha
zavěšených	zavěšený	k2eAgFnPc2d1	zavěšená
u	u	k7c2	u
krbu	krb	k1gInSc2	krb
něco	něco	k3yInSc4	něco
nadělí	nadělit	k5eAaPmIp3nS	nadělit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tradičním	tradiční	k2eAgNnPc3d1	tradiční
vánočním	vánoční	k2eAgNnPc3d1	vánoční
jídlům	jídlo	k1gNnPc3	jídlo
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
ryby	ryba	k1gFnSc2	ryba
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgInPc1d1	mořský
plody	plod	k1gInPc1	plod
a	a	k8xC	a
plněný	plněný	k2eAgMnSc1d1	plněný
krocan	krocan	k1gMnSc1	krocan
<g/>
.	.	kIx.	.
</s>
<s>
Nejtypičtější	typický	k2eAgFnSc7d3	nejtypičtější
pochoutkou	pochoutka	k1gFnSc7	pochoutka
vyhrazenou	vyhrazený	k2eAgFnSc4d1	vyhrazená
jen	jen	k6eAd1	jen
pro	pro	k7c4	pro
vánoční	vánoční	k2eAgFnSc4d1	vánoční
tabuli	tabule	k1gFnSc4	tabule
je	být	k5eAaImIp3nS	být
sladké	sladký	k2eAgNnSc1d1	sladké
"	"	kIx"	"
<g/>
vánoční	vánoční	k2eAgNnSc4d1	vánoční
poleno	poleno	k1gNnSc4	poleno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlámštině	vlámština	k1gFnSc6	vlámština
"	"	kIx"	"
<g/>
kerststronk	kerststronk	k6eAd1	kerststronk
<g/>
"	"	kIx"	"
a	a	k8xC	a
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
"	"	kIx"	"
<g/>
la	la	k1gNnSc1	la
bû	bû	k?	bû
de	de	k?	de
Noël	Noël	k1gInSc1	Noël
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
měkkou	měkký	k2eAgFnSc4d1	měkká
<g/>
,	,	kIx,	,
pěnovitou	pěnovitý	k2eAgFnSc4d1	pěnovitá
roládu	roláda	k1gFnSc4	roláda
naplněnou	naplněný	k2eAgFnSc4d1	naplněná
krémem	krém	k1gInSc7	krém
a	a	k8xC	a
zvenčí	zvenčí	k6eAd1	zvenčí
ozdobenou	ozdobený	k2eAgFnSc7d1	ozdobená
čokoládou	čokoláda	k1gFnSc7	čokoláda
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
opravdu	opravdu	k6eAd1	opravdu
připomíná	připomínat	k5eAaImIp3nS	připomínat
poleno	poleno	k1gNnSc4	poleno
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
si	se	k3xPyFc3	se
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
zdobí	zdobit	k5eAaImIp3nS	zdobit
dům	dům	k1gInSc1	dům
postavičkami	postavička	k1gFnPc7	postavička
skřítků	skřítek	k1gMnPc2	skřítek
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
nisser	nisser	k1gInSc1	nisser
.	.	kIx.	.
</s>
<s>
Můžou	Můžou	k?	Můžou
být	být	k5eAaImF	být
různě	různě	k6eAd1	různě
velcí	velký	k2eAgMnPc1d1	velký
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
špičatou	špičatý	k2eAgFnSc4d1	špičatá
čepičku	čepička	k1gFnSc4	čepička
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
oblečeni	obléct	k5eAaPmNgMnP	obléct
do	do	k7c2	do
dánských	dánský	k2eAgFnPc2d1	dánská
národních	národní	k2eAgFnPc2d1	národní
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
červené	červený	k2eAgFnPc1d1	červená
a	a	k8xC	a
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
tito	tento	k3xDgMnPc1	tento
skřítci	skřítek	k1gMnPc1	skřítek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
večer	večer	k6eAd1	večer
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
nadělují	nadělovat	k5eAaImIp3nP	nadělovat
dárky	dárek	k1gInPc1	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Drobné	drobný	k2eAgMnPc4d1	drobný
dárečky	dáreček	k1gInPc4	dáreček
a	a	k8xC	a
sladkosti	sladkost	k1gFnPc4	sladkost
se	se	k3xPyFc4	se
zavěšují	zavěšovat	k5eAaImIp3nP	zavěšovat
na	na	k7c4	na
větve	větev	k1gFnPc4	větev
vánočního	vánoční	k2eAgInSc2d1	vánoční
stromečku	stromeček	k1gInSc2	stromeček
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
lidé	člověk	k1gMnPc1	člověk
často	často	k6eAd1	často
chodí	chodit	k5eAaImIp3nP	chodit
zapálit	zapálit	k5eAaPmF	zapálit
svíčku	svíčka	k1gFnSc4	svíčka
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
panuje	panovat	k5eAaImIp3nS	panovat
také	také	k9	také
zvyk	zvyk	k1gInSc1	zvyk
vítání	vítání	k1gNnSc2	vítání
"	"	kIx"	"
<g/>
vánočního	vánoční	k2eAgMnSc2d1	vánoční
posla	posel	k1gMnSc2	posel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zaklepe	zaklepat	k5eAaPmIp3nS	zaklepat
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
a	a	k8xC	a
každému	každý	k3xTgMnSc3	každý
dá	dát	k5eAaPmIp3nS	dát
vtipný	vtipný	k2eAgInSc1d1	vtipný
dáreček	dáreček	k1gInSc1	dáreček
charakterizující	charakterizující	k2eAgFnSc4d1	charakterizující
obdarovanou	obdarovaný	k2eAgFnSc4d1	obdarovaná
osobu	osoba	k1gFnSc4	osoba
–	–	k?	–
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
ctnost	ctnost	k1gFnSc4	ctnost
nebo	nebo	k8xC	nebo
vadu	vada	k1gFnSc4	vada
<g/>
,	,	kIx,	,
např.	např.	kA	např.
tlouštíci	tlouštík	k1gMnPc1	tlouštík
dostávají	dostávat	k5eAaImIp3nP	dostávat
špejli	špejle	k1gFnSc4	špejle
od	od	k7c2	od
jitrnice	jitrnice	k1gFnSc2	jitrnice
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
sejde	sejít	k5eAaPmIp3nS	sejít
u	u	k7c2	u
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
nesmí	smět	k5eNaImIp3nS	smět
chybět	chybět	k5eAaImF	chybět
husa	husa	k1gFnSc1	husa
<g/>
,	,	kIx,	,
kachna	kachna	k1gFnSc1	kachna
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
vepřová	vepřový	k2eAgFnSc1d1	vepřová
pečeně	pečeně	k1gFnSc1	pečeně
s	s	k7c7	s
červeným	červený	k2eAgNnSc7d1	červené
zelím	zelí	k1gNnSc7	zelí
jako	jako	k9	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
chod	chod	k1gInSc1	chod
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
opepřená	opepřený	k2eAgFnSc1d1	opepřená
treska	treska	k1gFnSc1	treska
s	s	k7c7	s
ředkvičkou	ředkvička	k1gFnSc7	ředkvička
<g/>
,	,	kIx,	,
pivní	pivní	k2eAgInSc1d1	pivní
chléb	chléb	k1gInSc1	chléb
<g/>
,	,	kIx,	,
teplá	teplý	k2eAgFnSc1d1	teplá
šunka	šunka	k1gFnSc1	šunka
<g/>
,	,	kIx,	,
hnědé	hnědý	k2eAgInPc1d1	hnědý
koláčky	koláček	k1gInPc1	koláček
nebo	nebo	k8xC	nebo
horká	horký	k2eAgFnSc1d1	horká
rýže	rýže	k1gFnSc1	rýže
zalitá	zalitý	k2eAgFnSc1d1	zalitá
studeným	studený	k2eAgNnSc7d1	studené
mlékem	mléko	k1gNnSc7	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
dezertem	dezert	k1gInSc7	dezert
je	být	k5eAaImIp3nS	být
rýžový	rýžový	k2eAgInSc1d1	rýžový
nákyp	nákyp	k1gInSc1	nákyp
se	s	k7c7	s
zapečenou	zapečený	k2eAgFnSc7d1	zapečená
mandlí	mandle	k1gFnSc7	mandle
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
nálezce	nálezce	k1gMnSc1	nálezce
dostane	dostat	k5eAaPmIp3nS	dostat
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
dárek	dárek	k1gInSc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
večeři	večeře	k1gFnSc6	večeře
rodina	rodina	k1gFnSc1	rodina
přesune	přesunout	k5eAaPmIp3nS	přesunout
ke	k	k7c3	k
stromečku	stromeček	k1gInSc3	stromeček
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kterého	který	k3yRgNnSc2	který
tančí	tančit	k5eAaImIp3nS	tančit
dokola	dokola	k6eAd1	dokola
držíc	držet	k5eAaImSgFnS	držet
se	se	k3xPyFc4	se
za	za	k7c4	za
ruce	ruka	k1gFnPc4	ruka
a	a	k8xC	a
zpívajíc	zpívat	k5eAaImSgFnS	zpívat
koledy	koleda	k1gFnPc4	koleda
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
vánočním	vánoční	k2eAgInSc7d1	vánoční
svátkem	svátek	k1gInSc7	svátek
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
Den	den	k1gInSc1	den
sv.	sv.	kA	sv.
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
českému	český	k2eAgMnSc3d1	český
Mikuláši	Mikuláš	k1gMnSc3	Mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
v	v	k7c6	v
převlečení	převlečení	k1gNnSc6	převlečení
za	za	k7c4	za
anděly	anděl	k1gMnPc4	anděl
a	a	k8xC	a
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
ozdobení	ozdobení	k1gNnSc6	ozdobení
korunou	koruna	k1gFnSc7	koruna
s	s	k7c7	s
hořícími	hořící	k2eAgFnPc7d1	hořící
svíčkami	svíčka	k1gFnPc7	svíčka
vydávají	vydávat	k5eAaImIp3nP	vydávat
nadělovat	nadělovat	k5eAaImF	nadělovat
dětem	dítě	k1gFnPc3	dítě
cukroví	cukroví	k1gNnSc2	cukroví
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
vánoční	vánoční	k2eAgFnSc7d1	vánoční
ozdobou	ozdoba	k1gFnSc7	ozdoba
je	být	k5eAaImIp3nS	být
slaměná	slaměný	k2eAgFnSc1d1	slaměná
postavička	postavička	k1gFnSc1	postavička
kozla	kozel	k1gMnSc2	kozel
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
nebo	nebo	k8xC	nebo
k	k	k7c3	k
zavěšení	zavěšení	k1gNnSc3	zavěšení
na	na	k7c4	na
stromeček	stromeček	k1gInSc4	stromeček
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
Tři	tři	k4xCgMnPc4	tři
krále	král	k1gMnPc4	král
hází	házet	k5eAaImIp3nS	házet
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
chodem	chod	k1gInSc7	chod
sváteční	sváteční	k2eAgFnSc1d1	sváteční
večeře	večeře	k1gFnSc1	večeře
je	být	k5eAaImIp3nS	být
sušená	sušený	k2eAgFnSc1d1	sušená
treska	treska	k1gFnSc1	treska
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
omáčkou	omáčka	k1gFnSc7	omáčka
<g/>
,	,	kIx,	,
dalším	další	k2eAgInSc7d1	další
tradičním	tradiční	k2eAgInSc7d1	tradiční
pokrmem	pokrm	k1gInSc7	pokrm
je	být	k5eAaImIp3nS	být
vörtbröt	vörtbröt	k5eAaPmF	vörtbröt
–	–	k?	–
kořeněný	kořeněný	k2eAgInSc4d1	kořeněný
chléb	chléb	k1gInSc4	chléb
s	s	k7c7	s
hřebíčky	hřebíček	k1gInPc7	hřebíček
a	a	k8xC	a
zázvorem	zázvor	k1gInSc7	zázvor
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vánoční	vánoční	k2eAgFnSc1d1	vánoční
specialita	specialita	k1gFnSc1	specialita
se	se	k3xPyFc4	se
podávají	podávat	k5eAaImIp3nP	podávat
i	i	k9	i
vařené	vařený	k2eAgFnPc1d1	vařená
fazole	fazole	k1gFnPc1	fazole
se	s	k7c7	s
slaninou	slanina	k1gFnSc7	slanina
a	a	k8xC	a
cibulí	cibule	k1gFnSc7	cibule
nebo	nebo	k8xC	nebo
malé	malý	k2eAgInPc4d1	malý
klobásky	klobásek	k1gInPc4	klobásek
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
julkorv	julkorv	k1gInSc1	julkorv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
večeři	večeře	k1gFnSc6	večeře
přichází	přicházet	k5eAaImIp3nS	přicházet
nadělovat	nadělovat	k5eAaImF	nadělovat
dárky	dárek	k1gInPc4	dárek
přihrblý	přihrblý	k2eAgMnSc1d1	přihrblý
dědeček	dědeček	k1gMnSc1	dědeček
Jultomten	Jultomten	k2eAgMnSc1d1	Jultomten
(	(	kIx(	(
<g/>
Vánoční	vánoční	k2eAgMnSc1d1	vánoční
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
skřítků	skřítek	k1gMnPc2	skřítek
Julnissarů	Julnissar	k1gInPc2	Julnissar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
na	na	k7c4	na
dárky	dárek	k1gInPc4	dárek
místo	místo	k7c2	místo
jména	jméno	k1gNnSc2	jméno
obdarovaného	obdarovaný	k2eAgInSc2d1	obdarovaný
napsat	napsat	k5eAaPmF	napsat
krátký	krátký	k2eAgInSc4d1	krátký
verš	verš	k1gInSc4	verš
charakterizující	charakterizující	k2eAgFnSc4d1	charakterizující
danou	daný	k2eAgFnSc4d1	daná
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
dárek	dárek	k1gInSc4	dárek
sama	sám	k3xTgFnSc1	sám
přihlásit	přihlásit	k5eAaPmF	přihlásit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
začínají	začínat	k5eAaImIp3nP	začínat
Vánoce	Vánoce	k1gFnPc1	Vánoce
večer	večer	k6eAd1	večer
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
objeví	objevit	k5eAaPmIp3nS	objevit
první	první	k4xOgFnSc1	první
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Poláci	Polák	k1gMnPc1	Polák
slaví	slavit	k5eAaImIp3nP	slavit
svátek	svátek	k1gInSc4	svátek
sv.	sv.	kA	sv.
Mikuláše	mikuláš	k1gInSc2	mikuláš
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
přináší	přinášet	k5eAaImIp3nS	přinášet
dárky	dárek	k1gInPc4	dárek
jen	jen	k9	jen
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
o	o	k7c6	o
Štědrém	štědrý	k2eAgInSc6d1	štědrý
večeru	večer	k1gInSc6	večer
Mikuláš	mikuláš	k1gInSc1	mikuláš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
častěji	často	k6eAd2	často
Dzieciątko	Dzieciątka	k1gFnSc5	Dzieciątka
(	(	kIx(	(
<g/>
dítě	dítě	k1gNnSc1	dítě
Ježíš	ježit	k5eAaImIp2nS	ježit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aniołek	Aniołek	k1gMnSc1	Aniołek
(	(	kIx(	(
<g/>
Anděl	Anděl	k1gMnSc1	Anděl
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
Gwiazdor	Gwiazdor	k1gMnSc1	Gwiazdor
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Velkopolsku	Velkopolsko	k1gNnSc6	Velkopolsko
<g/>
)	)	kIx)	)
nechává	nechávat	k5eAaImIp3nS	nechávat
pod	pod	k7c7	pod
stromečkem	stromeček	k1gInSc7	stromeček
dárky	dárek	k1gInPc4	dárek
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Odedávna	odedávna	k6eAd1	odedávna
zvykem	zvyk	k1gInSc7	zvyk
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
jesličky	jesličky	k1gFnPc1	jesličky
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
pozadí	pozadí	k1gNnSc6	pozadí
se	se	k3xPyFc4	se
často	často	k6eAd1	často
místo	místo	k7c2	místo
betlémské	betlémský	k2eAgFnSc2d1	Betlémská
krajiny	krajina	k1gFnSc2	krajina
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
polská	polský	k2eAgNnPc1d1	polské
města	město	k1gNnPc1	město
a	a	k8xC	a
historické	historický	k2eAgFnPc1d1	historická
stavby	stavba	k1gFnPc1	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svátku	svátek	k1gInSc2	svátek
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
tyto	tento	k3xDgInPc4	tento
betlémy	betlém	k1gInPc4	betlém
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Sváteční	sváteční	k2eAgInSc4d1	sváteční
stůl	stůl	k1gInSc4	stůl
nebo	nebo	k8xC	nebo
i	i	k9	i
podlaha	podlaha	k1gFnSc1	podlaha
se	se	k3xPyFc4	se
zdobí	zdobit	k5eAaImIp3nS	zdobit
trochou	trocha	k1gFnSc7	trocha
slámy	sláma	k1gFnSc2	sláma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
připomínat	připomínat	k5eAaImF	připomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
chlévě	chlév	k1gInSc6	chlév
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
štědrovečerní	štědrovečerní	k2eAgFnSc2d1	štědrovečerní
večeře	večeře	k1gFnSc2	večeře
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
a	a	k8xC	a
hosté	host	k1gMnPc1	host
dělí	dělit	k5eAaImIp3nP	dělit
oplatkou	oplatka	k1gFnSc7	oplatka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
symbol	symbol	k1gInSc1	symbol
smíření	smíření	k1gNnSc2	smíření
a	a	k8xC	a
příležitost	příležitost	k1gFnSc4	příležitost
přát	přát	k5eAaImF	přát
si	se	k3xPyFc3	se
všechno	všechen	k3xTgNnSc4	všechen
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Zvykem	zvyk	k1gInSc7	zvyk
je	být	k5eAaImIp3nS	být
prostírat	prostírat	k5eAaImF	prostírat
u	u	k7c2	u
štědrovečerní	štědrovečerní	k2eAgFnSc2d1	štědrovečerní
večeře	večeře	k1gFnSc2	večeře
jeden	jeden	k4xCgInSc4	jeden
talíř	talíř	k1gInSc4	talíř
s	s	k7c7	s
příborem	příbor	k1gInSc7	příbor
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
případného	případný	k2eAgMnSc4d1	případný
hosta	host	k1gMnSc4	host
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
aluzi	aluze	k1gFnSc4	aluze
na	na	k7c4	na
židovskou	židovský	k2eAgFnSc4d1	židovská
tradici	tradice	k1gFnSc4	tradice
vyhrazení	vyhrazení	k1gNnSc2	vyhrazení
místa	místo	k1gNnSc2	místo
u	u	k7c2	u
pesachového	pesachový	k2eAgInSc2d1	pesachový
stolu	stol	k1gInSc2	stol
proroku	prorok	k1gMnSc3	prorok
Eliášovi	Eliáš	k1gMnSc3	Eliáš
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
večeře	večeře	k1gFnSc1	večeře
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
chodů	chod	k1gInPc2	chod
symbolizujících	symbolizující	k2eAgInPc2d1	symbolizující
dvanáct	dvanáct	k4xCc4	dvanáct
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
jídlem	jídlo	k1gNnSc7	jídlo
je	být	k5eAaImIp3nS	být
smažený	smažený	k2eAgMnSc1d1	smažený
kapr	kapr	k1gMnSc1	kapr
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgFnSc1d1	jiná
ryba	ryba	k1gFnSc1	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
podává	podávat	k5eAaImIp3nS	podávat
červený	červený	k2eAgInSc1d1	červený
boršč	boršč	k1gInSc1	boršč
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
uszkami	uszka	k1gFnPc7	uszka
(	(	kIx(	(
<g/>
taštičky	taštička	k1gFnPc1	taštička
s	s	k7c7	s
houbami	houba	k1gFnPc7	houba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
připomínají	připomínat	k5eAaImIp3nP	připomínat
tvarem	tvar	k1gInSc7	tvar
uši	ucho	k1gNnPc1	ucho
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pierogi	pierog	k1gMnPc1	pierog
tj.	tj.	kA	tj.
taštičky	taštička	k1gFnPc4	taštička
se	s	k7c7	s
zelím	zelí	k1gNnSc7	zelí
a	a	k8xC	a
houbami	houba	k1gFnPc7	houba
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
postní	postní	k2eAgNnPc4d1	postní
jídla	jídlo	k1gNnPc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
Polska	Polsko	k1gNnSc2	Polsko
se	se	k3xPyFc4	se
jedí	jíst	k5eAaImIp3nP	jíst
různé	různý	k2eAgFnPc1d1	různá
vánoční	vánoční	k2eAgFnPc1d1	vánoční
pochoutky	pochoutka	k1gFnPc1	pochoutka
<g/>
:	:	kIx,	:
např.	např.	kA	např.
pro	pro	k7c4	pro
Slezsko	Slezsko	k1gNnSc4	Slezsko
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
moczka	moczka	k1gFnSc1	moczka
<g/>
,	,	kIx,	,
namočený	namočený	k2eAgInSc1d1	namočený
perník	perník	k1gInSc1	perník
s	s	k7c7	s
rozinkami	rozinka	k1gFnPc7	rozinka
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
<g/>
</s>
<s>
,	,	kIx,	,
mandlí	mandle	k1gFnSc7	mandle
a	a	k8xC	a
sušeným	sušený	k2eAgNnSc7d1	sušené
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
makůvky	makůvka	k1gFnSc2	makůvka
–	–	k?	–
rohlíky	rohlík	k1gInPc1	rohlík
máčené	máčený	k2eAgInPc1d1	máčený
ve	v	k7c6	v
sladkém	sladký	k2eAgNnSc6d1	sladké
mléce	mléko	k1gNnSc6	mléko
a	a	k8xC	a
obalované	obalovaný	k2eAgInPc4d1	obalovaný
v	v	k7c6	v
máku	mák	k1gInSc6	mák
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
také	také	k9	také
kutia	kutia	k1gFnSc1	kutia
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
části	část	k1gFnSc3	část
východní	východní	k2eAgFnSc4d1	východní
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
med	med	k1gInSc4	med
<g/>
,	,	kIx,	,
rozinky	rozinka	k1gFnPc4	rozinka
<g/>
,	,	kIx,	,
mandle	mandle	k1gFnPc4	mandle
apod.	apod.	kA	apod.
Na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
se	se	k3xPyFc4	se
také	také	k6eAd1	také
peče	péct	k5eAaImIp3nS	péct
perník	perník	k1gInSc1	perník
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
půlnoční	půlnoční	k2eAgFnSc2d1	půlnoční
mše	mše	k1gFnSc2	mše
–	–	k?	–
pasterka	pasterka	k1gFnSc1	pasterka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
pastýře	pastýř	k1gMnPc4	pastýř
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Ježíše	Ježíš	k1gMnPc4	Ježíš
<g/>
.	.	kIx.	.
</s>
<s>
Zpívají	zpívat	k5eAaImIp3nP	zpívat
se	se	k3xPyFc4	se
také	také	k9	také
koledy	koleda	k1gFnPc1	koleda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
bývají	bývat	k5eAaImIp3nP	bývat
hlavním	hlavní	k2eAgInSc7d1	hlavní
symbolem	symbol	k1gInSc7	symbol
Vánoc	Vánoce	k1gFnPc2	Vánoce
jesličky	jesličky	k1gFnPc4	jesličky
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
ale	ale	k9	ale
nechybí	chybět	k5eNaImIp3nS	chybět
ani	ani	k8xC	ani
ozdobený	ozdobený	k2eAgInSc4d1	ozdobený
vánoční	vánoční	k2eAgInSc4d1	vánoční
stromeček	stromeček	k1gInSc4	stromeček
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
jedle	jedle	k6eAd1	jedle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
zvyky	zvyk	k1gInPc4	zvyk
a	a	k8xC	a
tradice	tradice	k1gFnPc4	tradice
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
dle	dle	k7c2	dle
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
zejména	zejména	k9	zejména
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
měst	město	k1gNnPc2	město
a	a	k8xC	a
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
rodiny	rodina	k1gFnPc1	rodina
mají	mít	k5eAaImIp3nP	mít
jesličky	jesličky	k1gFnPc4	jesličky
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
ty	ten	k3xDgFnPc4	ten
v	v	k7c6	v
životní	životní	k2eAgFnSc6d1	životní
velikosti	velikost	k1gFnSc6	velikost
se	se	k3xPyFc4	se
chodí	chodit	k5eAaImIp3nP	chodit
dívat	dívat	k5eAaImF	dívat
do	do	k7c2	do
kostelů	kostel	k1gInPc2	kostel
nebo	nebo	k8xC	nebo
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Rodinné	rodinný	k2eAgInPc1d1	rodinný
betlémy	betlém	k1gInPc1	betlém
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dědí	dědit	k5eAaImIp3nS	dědit
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Vánočním	vánoční	k2eAgInSc7d1	vánoční
dnem	den	k1gInSc7	den
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
slavnostní	slavnostní	k2eAgInPc1d1	slavnostní
pokrmy	pokrm	k1gInPc1	pokrm
o	o	k7c6	o
několika	několik	k4yIc6	několik
chodech	chod	k1gInPc6	chod
se	se	k3xPyFc4	se
podávají	podávat	k5eAaImIp3nP	podávat
při	při	k7c6	při
večeři	večeře	k1gFnSc6	večeře
24	[number]	k4	24
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
při	při	k7c6	při
obědě	oběd	k1gInSc6	oběd
25	[number]	k4	25
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
scházejí	scházet	k5eAaImIp3nP	scházet
celé	celý	k2eAgFnSc2d1	celá
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Dárky	dárek	k1gInPc1	dárek
se	se	k3xPyFc4	se
rozbalují	rozbalovat	k5eAaImIp3nP	rozbalovat
po	po	k7c6	po
večeři	večeře	k1gFnSc6	večeře
24	[number]	k4	24
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
čeká	čekat	k5eAaImIp3nS	čekat
se	se	k3xPyFc4	se
do	do	k7c2	do
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začne	začít	k5eAaPmIp3nS	začít
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
leží	ležet	k5eAaImIp3nP	ležet
vedle	vedle	k7c2	vedle
jesliček	jesličky	k1gFnPc2	jesličky
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
stromečkem	stromeček	k1gInSc7	stromeček
<g/>
.	.	kIx.	.
</s>
<s>
Dárky	dárek	k1gInPc1	dárek
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
italské	italský	k2eAgFnSc2d1	italská
obdoby	obdoba	k1gFnSc2	obdoba
Santa	Santa	k1gMnSc1	Santa
Clause	Clause	k1gFnSc2	Clause
–	–	k?	–
Babbo	Babba	k1gFnSc5	Babba
Natale	Natal	k1gInSc5	Natal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
snídani	snídaně	k1gFnSc6	snídaně
jde	jít	k5eAaImIp3nS	jít
zpravidla	zpravidla	k6eAd1	zpravidla
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
vánoční	vánoční	k2eAgFnSc6d1	vánoční
hostině	hostina	k1gFnSc6	hostina
nesmí	smět	k5eNaImIp3nS	smět
chybět	chybět	k5eAaImF	chybět
panettone	panetton	k1gInSc5	panetton
<g/>
,	,	kIx,	,
typická	typický	k2eAgFnSc1d1	typická
vánoční	vánoční	k2eAgFnSc1d1	vánoční
bábovka	bábovka	k1gFnSc1	bábovka
s	s	k7c7	s
hrozinkami	hrozinka	k1gFnPc7	hrozinka
a	a	k8xC	a
kandovaným	kandovaný	k2eAgNnSc7d1	kandované
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
jednodušší	jednoduchý	k2eAgFnSc5d2	jednodušší
pandoro	pandora	k1gFnSc5	pandora
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
jídla	jídlo	k1gNnPc1	jídlo
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tradicích	tradice	k1gFnPc6	tradice
daného	daný	k2eAgInSc2d1	daný
regionu	region	k1gInSc2	region
a	a	k8xC	a
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jehněčí	jehněčí	k1gNnSc4	jehněčí
s	s	k7c7	s
rozmarýnovými	rozmarýnový	k2eAgFnPc7d1	Rozmarýnová
bramborami	brambora	k1gFnPc7	brambora
a	a	k8xC	a
artyčoky	artyčok	k1gInPc7	artyčok
<g/>
,	,	kIx,	,
či	či	k8xC	či
večeře	večeře	k1gFnSc1	večeře
o	o	k7c6	o
několika	několik	k4yIc6	několik
chodech	chod	k1gInPc6	chod
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
mořských	mořský	k2eAgFnPc2d1	mořská
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
ještě	ještě	k9	ještě
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
svátek	svátek	k1gInSc1	svátek
Epifanie	Epifanie	k1gFnSc2	Epifanie
<g/>
.	.	kIx.	.
</s>
<s>
Čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
La	la	k1gNnSc2	la
Befana	Befana	k1gFnSc1	Befana
sestoupí	sestoupit	k5eAaPmIp3nP	sestoupit
komínem	komín	k1gInSc7	komín
do	do	k7c2	do
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zanechá	zanechat	k5eAaPmIp3nS	zanechat
dětem	dítě	k1gFnPc3	dítě
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
velká	velký	k2eAgFnSc1d1	velká
loutka	loutka	k1gFnSc1	loutka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
konce	konec	k1gInSc2	konec
svátků	svátek	k1gInPc2	svátek
zapálí	zapálit	k5eAaPmIp3nS	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
asijských	asijský	k2eAgFnPc6d1	asijská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
slaveny	slavit	k5eAaImNgFnP	slavit
především	především	k9	především
pravoslavné	pravoslavný	k2eAgFnPc1d1	pravoslavná
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
se	se	k3xPyFc4	se
v	v	k7c6	v
pravoslaví	pravoslaví	k1gNnSc6	pravoslaví
začínají	začínat	k5eAaImIp3nP	začínat
slavit	slavit	k5eAaImF	slavit
až	až	k9	až
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
posunu	posun	k1gInSc2	posun
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
církvích	církev	k1gFnPc6	církev
stále	stále	k6eAd1	stále
používaného	používaný	k2eAgInSc2d1	používaný
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
o	o	k7c4	o
13	[number]	k4	13
dnů	den	k1gInPc2	den
oproti	oproti	k7c3	oproti
západnímu	západní	k2eAgInSc3d1	západní
gregoriánskému	gregoriánský	k2eAgInSc3d1	gregoriánský
kalendáři	kalendář	k1gInSc3	kalendář
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
tak	tak	k6eAd1	tak
spadá	spadat	k5eAaImIp3nS	spadat
až	až	k9	až
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
gregoriánského	gregoriánský	k2eAgMnSc2d1	gregoriánský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
byly	být	k5eAaImAgFnP	být
pravoslavné	pravoslavný	k2eAgFnPc1d1	pravoslavná
Vánoce	Vánoce	k1gFnPc1	Vánoce
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
upadly	upadnout	k5eAaPmAgFnP	upadnout
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
do	do	k7c2	do
zapomnění	zapomnění	k1gNnSc2	zapomnění
po	po	k7c6	po
oficiálním	oficiální	k2eAgInSc6d1	oficiální
zákazu	zákaz	k1gInSc6	zákaz
Vánoc	Vánoce	k1gFnPc2	Vánoce
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
Ruské	ruský	k2eAgFnSc6d1	ruská
federaci	federace	k1gFnSc6	federace
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
oslavy	oslava	k1gFnPc1	oslava
spojeny	spojen	k2eAgFnPc1d1	spojena
spíše	spíše	k9	spíše
s	s	k7c7	s
Novým	nový	k2eAgInSc7d1	nový
rokem	rok	k1gInSc7	rok
s	s	k7c7	s
jolkou	jolka	k1gFnSc7	jolka
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
nepravoslavnou	pravoslavný	k2eNgFnSc7d1	nepravoslavná
(	(	kIx(	(
<g/>
pohádkovou	pohádkový	k2eAgFnSc7d1	pohádková
<g/>
)	)	kIx)	)
postavou	postava	k1gFnSc7	postava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
pravoslavných	pravoslavný	k2eAgFnPc6d1	pravoslavná
Vánocích	Vánoce	k1gFnPc6	Vánoce
podobnou	podobný	k2eAgFnSc7d1	podobná
funkcí	funkce	k1gFnSc7	funkce
jako	jako	k8xS	jako
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
vánoční	vánoční	k2eAgFnSc1d1	vánoční
postava	postava	k1gFnSc1	postava
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
novoroční	novoroční	k2eAgMnSc1d1	novoroční
Děda	děda	k1gMnSc1	děda
Mráz	Mráz	k1gMnSc1	Mráz
<g/>
.	.	kIx.	.
</s>
<s>
Děda	děda	k1gMnSc1	děda
Mráz	Mráz	k1gMnSc1	Mráz
roznáší	roznášet	k5eAaImIp3nS	roznášet
dárky	dárek	k1gInPc4	dárek
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ruských	ruský	k2eAgFnPc6d1	ruská
rodinách	rodina	k1gFnPc6	rodina
Děda	děda	k1gMnSc1	děda
Mráz	Mráz	k1gMnSc1	Mráz
"	"	kIx"	"
<g/>
roznáší	roznášet	k5eAaImIp3nS	roznášet
<g/>
"	"	kIx"	"
dárky	dárek	k1gInPc4	dárek
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
oslavě	oslava	k1gFnSc6	oslava
Silvestra	Silvestr	k1gMnSc2	Silvestr
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
rozbalovat	rozbalovat	k5eAaImF	rozbalovat
společně	společně	k6eAd1	společně
novoroční	novoroční	k2eAgInPc4d1	novoroční
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
shromáždí	shromáždit	k5eAaPmIp3nP	shromáždit
kolem	kolem	k7c2	kolem
vánočního	vánoční	k2eAgInSc2d1	vánoční
stromku	stromek	k1gInSc2	stromek
<g/>
,	,	kIx,	,
tančí	tančit	k5eAaImIp3nP	tančit
a	a	k8xC	a
zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vánoce	Vánoce	k1gFnPc1	Vánoce
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
většina	většina	k1gFnSc1	většina
věřících	věřící	k1gMnPc2	věřící
používá	používat	k5eAaImIp3nS	používat
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
vánoční	vánoční	k2eAgInPc4d1	vánoční
svátky	svátek	k1gInPc4	svátek
začínají	začínat	k5eAaImIp3nP	začínat
až	až	k9	až
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Svatý	svatý	k2eAgInSc4d1	svatý
večer	večer	k1gInSc4	večer
(	(	kIx(	(
<g/>
Svjatyj	Svjatyj	k1gMnSc1	Svjatyj
večir	večir	k1gMnSc1	večir
<g/>
)	)	kIx)	)
se	s	k7c7	s
Svatou	svatý	k2eAgFnSc7d1	svatá
večeří	večeře	k1gFnSc7	večeře
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
obdoba	obdoba	k1gFnSc1	obdoba
české	český	k2eAgFnSc2d1	Česká
štědrovečerní	štědrovečerní	k2eAgFnSc2d1	štědrovečerní
večeře	večeře	k1gFnSc2	večeře
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
dvanáct	dvanáct	k4xCc1	dvanáct
jídel	jídlo	k1gNnPc2	jídlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
dvanáct	dvanáct	k4xCc4	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
jídlem	jídlo	k1gNnSc7	jídlo
je	být	k5eAaImIp3nS	být
kuťa	kuťa	k6eAd1	kuťa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
bezmasý	bezmasý	k2eAgInSc1d1	bezmasý
pokrm	pokrm	k1gInSc1	pokrm
z	z	k7c2	z
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
medu	med	k1gInSc2	med
<g/>
,	,	kIx,	,
máku	mák	k1gInSc2	mák
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
ingrediencí	ingredience	k1gFnPc2	ingredience
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
další	další	k2eAgNnPc1d1	další
jídla	jídlo	k1gNnPc1	jídlo
jsou	být	k5eAaImIp3nP	být
postní	postní	k2eAgNnPc1d1	postní
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
bezmasá	bezmasý	k2eAgNnPc1d1	bezmasé
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
lívance	lívanec	k1gInPc1	lívanec
<g/>
,	,	kIx,	,
houbový	houbový	k2eAgInSc1d1	houbový
boršč	boršč	k1gInSc1	boršč
či	či	k8xC	či
pirohy	piroh	k1gInPc1	piroh
<g/>
.	.	kIx.	.
</s>
<s>
Večeře	večeře	k1gFnSc1	večeře
často	často	k6eAd1	často
trvá	trvat	k5eAaImIp3nS	trvat
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
večeři	večeře	k1gFnSc6	večeře
se	se	k3xPyFc4	se
koleduje	koledovat	k5eAaImIp3nS	koledovat
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
narození	narození	k1gNnSc1	narození
Krista	Kristus	k1gMnSc2	Kristus
(	(	kIx(	(
<g/>
Ridzvo	Ridzvo	k1gNnSc1	Ridzvo
Christovo	Christův	k2eAgNnSc1d1	Christovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
svátek	svátek	k1gInSc1	svátek
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
pak	pak	k6eAd1	pak
svátek	svátek	k1gInSc1	svátek
sv.	sv.	kA	sv.
Štěpána	Štěpána	k1gFnSc1	Štěpána
(	(	kIx(	(
<g/>
Svjatyj	Svjatyj	k1gMnSc1	Svjatyj
Stepan	Stepan	k1gMnSc1	Stepan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
pak	pak	k6eAd1	pak
přichází	přicházet	k5eAaImIp3nS	přicházet
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
(	(	kIx(	(
<g/>
Ščedryj	Ščedryj	k1gMnSc1	Ščedryj
večir	večir	k1gMnSc1	večir
<g/>
)	)	kIx)	)
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
bohatou	bohatý	k2eAgFnSc7d1	bohatá
Svatou	svatý	k2eAgFnSc7d1	svatá
večeří	večeře	k1gFnSc7	večeře
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
je	být	k5eAaImIp3nS	být
svátek	svátek	k1gInSc1	svátek
sv.	sv.	kA	sv.
Vasyľa	Vasyľa	k1gFnSc1	Vasyľa
(	(	kIx(	(
<g/>
Svjatyj	Svjatyj	k1gMnSc1	Svjatyj
Vasyľ	Vasyľ	k1gMnSc1	Vasyľ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
juliánský	juliánský	k2eAgInSc4d1	juliánský
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
obětuje	obětovat	k5eAaBmIp3nS	obětovat
prase	prase	k1gNnSc1	prase
a	a	k8xC	a
pořádá	pořádat	k5eAaImIp3nS	pořádat
se	se	k3xPyFc4	se
zabijačka	zabijačka	k1gFnSc1	zabijačka
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInPc4d1	vánoční
svátky	svátek	k1gInPc4	svátek
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
končí	končit	k5eAaImIp3nS	končit
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
svátkem	svátek	k1gInSc7	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
(	(	kIx(	(
<g/>
Svjatyj	Svjatyj	k1gMnSc1	Svjatyj
Ivan	Ivan	k1gMnSc1	Ivan
Chrestyteľ	Chrestyteľ	k1gMnSc1	Chrestyteľ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
světí	světit	k5eAaImIp3nS	světit
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInSc1d1	vánoční
stromek	stromek	k1gInSc1	stromek
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
zdobí	zdobit	k5eAaImIp3nS	zdobit
již	již	k6eAd1	již
na	na	k7c4	na
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
zdobíval	zdobívat	k5eAaImAgInS	zdobívat
až	až	k6eAd1	až
odpoledne	odpoledne	k1gNnSc4	odpoledne
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
také	také	k9	také
rozdávají	rozdávat	k5eAaImIp3nP	rozdávat
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Novoroční	novoroční	k2eAgNnSc1d1	novoroční
<g/>
"	"	kIx"	"
rozdávání	rozdávání	k1gNnSc1	rozdávání
dárků	dárek	k1gInPc2	dárek
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
sovětských	sovětský	k2eAgFnPc2d1	sovětská
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
se	se	k3xPyFc4	se
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
přímořských	přímořský	k2eAgFnPc6d1	přímořská
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
zdobily	zdobit	k5eAaImAgFnP	zdobit
lodě	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInSc1d1	vánoční
stromeček	stromeček	k1gInSc1	stromeček
tam	tam	k6eAd1	tam
začal	začít	k5eAaPmAgInS	začít
pronikat	pronikat	k5eAaImF	pronikat
teprve	teprve	k6eAd1	teprve
před	před	k7c7	před
několika	několik	k4yIc7	několik
desetiletími	desetiletí	k1gNnPc7	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vánoční	vánoční	k2eAgFnPc4d1	vánoční
tradice	tradice	k1gFnPc4	tradice
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
žehnání	žehnání	k1gNnSc4	žehnání
domů	dům	k1gInPc2	dům
svěcenou	svěcený	k2eAgFnSc7d1	svěcená
vodou	voda	k1gFnSc7	voda
proti	proti	k7c3	proti
zlým	zlý	k2eAgMnPc3d1	zlý
skřítkům	skřítek	k1gMnPc3	skřítek
nebo	nebo	k8xC	nebo
dětské	dětský	k2eAgInPc1d1	dětský
průvody	průvod	k1gInPc1	průvod
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
chodí	chodit	k5eAaImIp3nP	chodit
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
koledovat	koledovat	k5eAaImF	koledovat
s	s	k7c7	s
triangly	triangl	k1gInPc7	triangl
a	a	k8xC	a
bubínky	bubínek	k1gInPc1	bubínek
ke	k	k7c3	k
známým	známý	k1gMnPc3	známý
a	a	k8xC	a
sousedům	soused	k1gMnPc3	soused
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
koná	konat	k5eAaImIp3nS	konat
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgFnPc2d3	nejbohatší
řeckých	řecký	k2eAgFnPc2d1	řecká
hostin	hostina	k1gFnPc2	hostina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
krůta	krůta	k1gFnSc1	krůta
s	s	k7c7	s
kaštanovou	kaštanový	k2eAgFnSc7d1	Kaštanová
nádivkou	nádivka	k1gFnSc7	nádivka
<g/>
,	,	kIx,	,
mleté	mletý	k2eAgNnSc1d1	mleté
maso	maso	k1gNnSc1	maso
a	a	k8xC	a
piniové	piniový	k2eAgInPc1d1	piniový
oříšky	oříšek	k1gInPc1	oříšek
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
peče	péct	k5eAaImIp3nS	péct
sele	sele	k1gNnSc1	sele
<g/>
,	,	kIx,	,
jehně	jehně	k1gNnSc1	jehně
nebo	nebo	k8xC	nebo
ryby	ryba	k1gFnPc1	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Dárky	dárek	k1gInPc1	dárek
nosí	nosit	k5eAaImIp3nP	nosit
Mikuláš	mikuláš	k1gInSc4	mikuláš
až	až	k9	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
zachraňoval	zachraňovat	k5eAaImAgMnS	zachraňovat
potápějící	potápějící	k2eAgMnPc4d1	potápějící
se	se	k3xPyFc4	se
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
mu	on	k3xPp3gInSc3	on
z	z	k7c2	z
vousů	vous	k1gInPc2	vous
crčí	crčet	k5eAaImIp3nS	crčet
slaná	slaný	k2eAgFnSc1d1	slaná
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vánoce	Vánoce	k1gFnPc1	Vánoce
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazení	zobrazení	k1gNnSc1	zobrazení
Vánoc	Vánoce	k1gFnPc2	Vánoce
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
doménou	doména	k1gFnSc7	doména
malby	malba	k1gFnSc2	malba
<g/>
.	.	kIx.	.
</s>
<s>
Sochařské	sochařský	k2eAgNnSc1d1	sochařské
zpracování	zpracování	k1gNnSc1	zpracování
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
reliéfního	reliéfní	k2eAgNnSc2d1	reliéfní
podání	podání	k1gNnSc2	podání
a	a	k8xC	a
lidových	lidový	k2eAgInPc2d1	lidový
betlémů	betlém	k1gInPc2	betlém
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Umělcem	umělec	k1gMnSc7	umělec
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
známým	známý	k2eAgInSc7d1	známý
malbami	malba	k1gFnPc7	malba
vánočních	vánoční	k2eAgInPc2d1	vánoční
motivů	motiv	k1gInPc2	motiv
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
Josef	Josefa	k1gFnPc2	Josefa
Lada	Lada	k1gFnSc1	Lada
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
typická	typický	k2eAgFnSc1d1	typická
a	a	k8xC	a
často	často	k6eAd1	často
užívaná	užívaný	k2eAgNnPc4d1	užívané
díla	dílo	k1gNnPc4	dílo
však	však	k9	však
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
upadly	upadnout	k5eAaPmAgFnP	upadnout
u	u	k7c2	u
mladé	mladý	k2eAgFnSc2d1	mladá
generace	generace	k1gFnSc2	generace
v	v	k7c6	v
zapomenutí	zapomenutí	k1gNnSc6	zapomenutí
kvůli	kvůli	k7c3	kvůli
nákladům	náklad	k1gInPc3	náklad
na	na	k7c4	na
copyright	copyright	k1gNnSc4	copyright
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
</s>
<s>
poměrně	poměrně	k6eAd1	poměrně
známé	známý	k2eAgFnPc1d1	známá
patří	patřit	k5eAaImIp3nP	patřit
disneyovské	disneyovský	k2eAgFnPc1d1	disneyovská
vánoční	vánoční	k2eAgFnPc1d1	vánoční
postavy	postava	k1gFnPc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
druhem	druh	k1gInSc7	druh
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
s	s	k7c7	s
vánočními	vánoční	k2eAgInPc7d1	vánoční
motivy	motiv	k1gInPc7	motiv
bylo	být	k5eAaImAgNnS	být
lidové	lidový	k2eAgNnSc1d1	lidové
řezbářství	řezbářství	k1gNnSc1	řezbářství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
období	období	k1gNnSc4	období
Vánoc	Vánoce	k1gFnPc2	Vánoce
zaměřovalo	zaměřovat	k5eAaImAgNnS	zaměřovat
na	na	k7c4	na
loutky	loutka	k1gFnPc4	loutka
a	a	k8xC	a
betlémy	betlém	k1gInPc4	betlém
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgNnSc7d1	základní
vánočním	vánoční	k2eAgNnSc7d1	vánoční
tématem	téma	k1gNnSc7	téma
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
je	být	k5eAaImIp3nS	být
Narození	narození	k1gNnSc4	narození
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
zobrazováno	zobrazovat	k5eAaImNgNnS	zobrazovat
již	již	k9	již
v	v	k7c6	v
raněkřesťanském	raněkřesťanský	k2eAgNnSc6d1	raněkřesťanský
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zpravidla	zpravidla	k6eAd1	zpravidla
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
christologických	christologický	k2eAgInPc2d1	christologický
cyklů	cyklus	k1gInPc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
narození	narození	k1gNnSc2	narození
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
biblickém	biblický	k2eAgInSc6d1	biblický
textu	text	k1gInSc6	text
nikterak	nikterak	k6eAd1	nikterak
popsáno	popsat	k5eAaPmNgNnS	popsat
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
indicií	indicie	k1gFnSc7	indicie
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
narozený	narozený	k2eAgMnSc1d1	narozený
Ježíš	Ježíš	k1gMnSc1	Ježíš
byl	být	k5eAaImAgMnS	být
položen	položit	k5eAaPmNgMnS	položit
do	do	k7c2	do
jeslí	jesle	k1gFnPc2	jesle
<g/>
.	.	kIx.	.
</s>
<s>
Kristovo	Kristův	k2eAgNnSc4d1	Kristovo
narození	narození	k1gNnSc4	narození
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
zpravidla	zpravidla	k6eAd1	zpravidla
situováno	situován	k2eAgNnSc1d1	situováno
do	do	k7c2	do
chléva	chlév	k1gInSc2	chlév
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
do	do	k7c2	do
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
přístřešku	přístřešek	k1gInSc2	přístřešek
postaveného	postavený	k2eAgMnSc4d1	postavený
pro	pro	k7c4	pro
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
často	často	k6eAd1	často
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
gotice	gotika	k1gFnSc6	gotika
a	a	k8xC	a
rané	raný	k2eAgFnSc3d1	raná
renesanci	renesance	k1gFnSc3	renesance
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
vestavěný	vestavěný	k2eAgMnSc1d1	vestavěný
do	do	k7c2	do
trosek	troska	k1gFnPc2	troska
rozměrné	rozměrný	k2eAgFnSc2d1	rozměrná
antické	antický	k2eAgFnSc2d1	antická
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
působení	působení	k1gNnSc2	působení
Ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
církve	církev	k1gFnSc2	církev
se	se	k3xPyFc4	se
Kristovo	Kristův	k2eAgNnSc1d1	Kristovo
narození	narození	k1gNnSc1	narození
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
svaté	svatý	k2eAgFnSc2d1	svatá
rodiny	rodina	k1gFnSc2	rodina
bývají	bývat	k5eAaImIp3nP	bývat
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
součástí	součást	k1gFnSc7	součást
scény	scéna	k1gFnSc2	scéna
narození	narození	k1gNnSc2	narození
Krista	Kristus	k1gMnSc2	Kristus
klanění	klanění	k1gNnSc2	klanění
pastýřů	pastýř	k1gMnPc2	pastýř
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
scény	scéna	k1gFnSc2	scéna
také	také	k9	také
andělé	anděl	k1gMnPc1	anděl
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
buďto	buďto	k8xC	buďto
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
výjevu	výjev	k1gInSc2	výjev
zvěstují	zvěstovat	k5eAaImIp3nP	zvěstovat
narození	narození	k1gNnSc4	narození
Spasitele	spasitel	k1gMnSc4	spasitel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
přítomni	přítomen	k2eAgMnPc1d1	přítomen
samotnému	samotný	k2eAgInSc3d1	samotný
betlémskému	betlémský	k2eAgInSc3d1	betlémský
výjevu	výjev	k1gInSc3	výjev
<g/>
.	.	kIx.	.
</s>
<s>
Námětově	námětově	k6eAd1	námětově
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
také	také	k9	také
Klanění	klanění	k1gNnPc4	klanění
tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
již	již	k6eAd1	již
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
církvi	církev	k1gFnSc6	církev
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
umělecké	umělecký	k2eAgInPc1d1	umělecký
cykly	cyklus	k1gInPc1	cyklus
zobrazující	zobrazující	k2eAgInSc1d1	zobrazující
celý	celý	k2eAgInSc4d1	celý
legendický	legendický	k2eAgInSc4d1	legendický
příběh	příběh	k1gInSc4	příběh
tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
zobrazovaných	zobrazovaný	k2eAgInPc2d1	zobrazovaný
detailů	detail	k1gInPc2	detail
klanění	klanění	k1gNnSc2	klanění
králů	král	k1gMnPc2	král
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
apokryfních	apokryfní	k2eAgFnPc2d1	apokryfní
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
nejenže	nejenže	k6eAd1	nejenže
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
jména	jméno	k1gNnPc4	jméno
"	"	kIx"	"
<g/>
králů	král	k1gMnPc2	král
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
biblického	biblický	k2eAgInSc2d1	biblický
textu	text	k1gInSc2	text
má	mít	k5eAaImIp3nS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
mágy	mág	k1gMnPc4	mág
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
nezná	neznat	k5eAaImIp3nS	neznat
ani	ani	k8xC	ani
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
počtu	počet	k1gInSc2	počet
darů	dar	k1gInPc2	dar
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
může	moct	k5eAaImIp3nS	moct
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
tři	tři	k4xCgInPc4	tři
tehdy	tehdy	k6eAd1	tehdy
známé	známý	k2eAgInPc4d1	známý
světadíly	světadíl	k1gInPc4	světadíl
(	(	kIx(	(
<g/>
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
i	i	k8xC	i
evropském	evropský	k2eAgNnSc6d1	Evropské
gotickém	gotický	k2eAgNnSc6d1	gotické
umění	umění	k1gNnSc6	umění
byly	být	k5eAaImAgFnP	být
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
události	událost	k1gFnPc1	událost
zpravidla	zpravidla	k6eAd1	zpravidla
zobrazovány	zobrazován	k2eAgFnPc1d1	zobrazována
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
christologických	christologický	k2eAgInPc2d1	christologický
cyklů	cyklus	k1gInPc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Vyšebrodský	vyšebrodský	k2eAgInSc1d1	vyšebrodský
cyklus	cyklus	k1gInSc1	cyklus
od	od	k7c2	od
Mistra	mistr	k1gMnSc2	mistr
vyšebrodského	vyšebrodský	k2eAgInSc2d1	vyšebrodský
oltáře	oltář	k1gInSc2	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Narození	narození	k1gNnSc1	narození
Krista	Kristus	k1gMnSc2	Kristus
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
cyklu	cyklus	k1gInSc6	cyklus
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
ikonografických	ikonografický	k2eAgInPc2d1	ikonografický
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
gotické	gotický	k2eAgNnSc4d1	gotické
umění	umění	k1gNnSc4	umění
obvyklé	obvyklý	k2eAgNnSc4d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
zde	zde	k6eAd1	zde
mj.	mj.	kA	mj.
líbá	líbat	k5eAaImIp3nS	líbat
malého	malý	k2eAgMnSc4d1	malý
Ježíše	Ježíš	k1gMnSc4	Ježíš
jako	jako	k8xS	jako
poukaz	poukaz	k1gInSc4	poukaz
na	na	k7c6	na
paradoxní	paradoxní	k2eAgFnSc6d1	paradoxní
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
katolická	katolický	k2eAgFnSc1d1	katolická
teologie	teologie	k1gFnSc1	teologie
chápe	chápat	k5eAaImIp3nS	chápat
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
Kristovu	Kristův	k2eAgFnSc4d1	Kristova
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
církve	církev	k1gFnSc2	církev
tj.	tj.	kA	tj.
nevěstu	nevěsta	k1gFnSc4	nevěsta
Kristovu	Kristův	k2eAgFnSc4d1	Kristova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
výjevu	výjev	k1gInSc2	výjev
je	být	k5eAaImIp3nS	být
zobrazen	zobrazen	k2eAgMnSc1d1	zobrazen
donátor	donátor	k1gMnSc1	donátor
a	a	k8xC	a
také	také	k9	také
nezvyklý	zvyklý	k2eNgInSc4d1	nezvyklý
apokryfní	apokryfní	k2eAgInSc4d1	apokryfní
motiv	motiv	k1gInSc4	motiv
lázně	lázeň	k1gFnSc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
často	často	k6eAd1	často
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
sílícího	sílící	k2eAgInSc2d1	sílící
dobového	dobový	k2eAgInSc2d1	dobový
důrazu	důraz	k1gInSc2	důraz
na	na	k7c4	na
Krista	Kristus	k1gMnSc4	Kristus
objevuje	objevovat	k5eAaImIp3nS	objevovat
motiv	motiv	k1gInSc1	motiv
Adorace	adorace	k1gFnSc2	adorace
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Marie	Marie	k1gFnSc1	Marie
malému	malý	k1gMnSc3	malý
Ježíši	Ježíš	k1gMnSc3	Ježíš
klaní	klanit	k5eAaImIp3nS	klanit
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Adorace	adorace	k1gFnSc1	adorace
z	z	k7c2	z
Hluboké	Hluboká	k1gFnSc2	Hluboká
od	od	k7c2	od
Mistra	mistr	k1gMnSc2	mistr
třeboňského	třeboňský	k2eAgInSc2d1	třeboňský
oltáře	oltář	k1gInSc2	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
Klanění	klanění	k1gNnSc2	klanění
tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
od	od	k7c2	od
Giotta	Giott	k1gInSc2	Giott
di	di	k?	di
Bondone	Bondon	k1gInSc5	Bondon
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1304	[number]	k4	1304
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
zobrazení	zobrazení	k1gNnSc1	zobrazení
betlémské	betlémský	k2eAgFnSc2d1	Betlémská
hvězdy	hvězda	k1gFnSc2	hvězda
jako	jako	k8xC	jako
komety	kometa	k1gFnSc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
gotice	gotika	k1gFnSc6	gotika
a	a	k8xC	a
rané	raný	k2eAgFnSc3d1	raná
renesanci	renesance	k1gFnSc3	renesance
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
spojovány	spojovat	k5eAaImNgInP	spojovat
motivy	motiv	k1gInPc7	motiv
klanění	klanění	k1gNnSc2	klanění
pastýřů	pastýř	k1gMnPc2	pastýř
a	a	k8xC	a
příjezdu	příjezd	k1gInSc2	příjezd
tří	tři	k4xCgNnPc2	tři
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Výjevy	výjev	k1gInPc1	výjev
mívají	mívat	k5eAaImIp3nP	mívat
mnohafigurový	mnohafigurový	k2eAgInSc4d1	mnohafigurový
komparz	komparz	k1gInSc4	komparz
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
a	a	k8xC	a
podrobně	podrobně	k6eAd1	podrobně
zobrazenou	zobrazený	k2eAgFnSc4d1	zobrazená
krajinu	krajina	k1gFnSc4	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
nedokončené	dokončený	k2eNgNnSc1d1	nedokončené
Klanění	klanění	k1gNnSc1	klanění
tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
od	od	k7c2	od
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
objevují	objevovat	k5eAaImIp3nP	objevovat
klečící	klečící	k2eAgMnPc1d1	klečící
donátoři	donátor	k1gMnPc1	donátor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
klanění	klanění	k1gNnSc4	klanění
tří	tři	k4xCgInPc2	tři
králů	král	k1gMnPc2	král
mívají	mívat	k5eAaImIp3nP	mívat
dokonce	dokonce	k9	dokonce
někdy	někdy	k6eAd1	někdy
podobu	podoba	k1gFnSc4	podoba
některého	některý	k3yIgMnSc2	některý
z	z	k7c2	z
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholně	vrcholně	k6eAd1	vrcholně
renesanční	renesanční	k2eAgFnSc1d1	renesanční
a	a	k8xC	a
především	především	k6eAd1	především
barokní	barokní	k2eAgNnSc4d1	barokní
zobrazení	zobrazení	k1gNnSc4	zobrazení
směřují	směřovat	k5eAaImIp3nP	směřovat
opět	opět	k6eAd1	opět
k	k	k7c3	k
intimitě	intimita	k1gFnSc3	intimita
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
postavami	postava	k1gFnPc7	postava
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c6	na
Klanění	klanění	k1gNnSc6	klanění
tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
ze	z	k7c2	z
Smiřic	Smiřice	k1gFnPc2	Smiřice
od	od	k7c2	od
Petra	Petr	k1gMnSc2	Petr
Brandla	Brandla	k1gMnSc2	Brandla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pobarokním	pobarokní	k2eAgInSc6d1	pobarokní
sekularizujícím	sekularizující	k2eAgInSc6d1	sekularizující
se	s	k7c7	s
prostředí	prostředí	k1gNnSc2	prostředí
se	se	k3xPyFc4	se
zobrazení	zobrazení	k1gNnSc1	zobrazení
Vánočních	vánoční	k2eAgFnPc2d1	vánoční
událostí	událost	k1gFnPc2	událost
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
do	do	k7c2	do
sentimentální	sentimentální	k2eAgFnSc2d1	sentimentální
polohy	poloha	k1gFnSc2	poloha
lidového	lidový	k2eAgNnSc2d1	lidové
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vyřezávaných	vyřezávaný	k2eAgInPc2d1	vyřezávaný
betlémů	betlém	k1gInPc2	betlém
<g/>
,	,	kIx,	,
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
často	často	k6eAd1	často
i	i	k9	i
stovky	stovka	k1gFnPc1	stovka
figurek	figurka	k1gFnPc2	figurka
<g/>
,	,	kIx,	,
zobrazení	zobrazení	k1gNnSc4	zobrazení
řady	řada	k1gFnSc2	řada
periferních	periferní	k2eAgNnPc2d1	periferní
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
anachronických	anachronický	k2eAgInPc2d1	anachronický
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
dějů	děj	k1gInPc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
přitom	přitom	k6eAd1	přitom
nejsou	být	k5eNaImIp3nP	být
pohyblivé	pohyblivý	k2eAgInPc4d1	pohyblivý
betlémy	betlém	k1gInPc4	betlém
především	především	k6eAd1	především
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
"	"	kIx"	"
<g/>
vysokém	vysoký	k2eAgNnSc6d1	vysoké
<g/>
"	"	kIx"	"
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
spíše	spíše	k9	spíše
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
například	například	k6eAd1	například
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
nazarénů	nazarén	k1gMnPc2	nazarén
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
některých	některý	k3yIgInPc2	některý
katolicky	katolicky	k6eAd1	katolicky
orientovaných	orientovaný	k2eAgMnPc2d1	orientovaný
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
například	například	k6eAd1	například
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Reynka	Reynek	k1gMnSc4	Reynek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
literatuře	literatura	k1gFnSc6	literatura
uvedl	uvést	k5eAaPmAgMnS	uvést
Vánoce	Vánoce	k1gFnPc4	Vánoce
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
Charles	Charles	k1gMnSc1	Charles
Dickens	Dickens	k1gInSc1	Dickens
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
A	a	k9	a
Christmas	Christmas	k1gMnSc1	Christmas
Carol	Carol	k1gInSc1	Carol
(	(	kIx(	(
<g/>
č.	č.	k?	č.
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
koleda	koleda	k1gFnSc1	koleda
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
lakomém	lakomý	k2eAgNnSc6d1	lakomé
<g/>
,	,	kIx,	,
studeném	studený	k2eAgNnSc6d1	studené
<g/>
,	,	kIx,	,
necitelném	citelný	k2eNgNnSc6d1	necitelné
a	a	k8xC	a
mrzoutském	mrzoutský	k2eAgMnSc6d1	mrzoutský
Ebenezeru	Ebenezer	k1gMnSc6	Ebenezer
Scroogeovi	Scroogeus	k1gMnSc6	Scroogeus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
nápravě	náprava	k1gFnSc3	náprava
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ho	on	k3xPp3gMnSc4	on
navštíví	navštívit	k5eAaPmIp3nP	navštívit
tři	tři	k4xCgMnPc1	tři
duchové	duch	k1gMnPc1	duch
o	o	k7c6	o
Štědrém	štědrý	k2eAgInSc6d1	štědrý
večeru	večer	k1gInSc6	večer
redefinoval	redefinovat	k5eAaImAgInS	redefinovat
ducha	duch	k1gMnSc4	duch
a	a	k8xC	a
význam	význam	k1gInSc4	význam
Vánoc	Vánoce	k1gFnPc2	Vánoce
a	a	k8xC	a
vyvolal	vyvolat	k5eAaPmAgMnS	vyvolat
znovuzrození	znovuzrození	k1gNnSc4	znovuzrození
svátečního	sváteční	k2eAgNnSc2d1	sváteční
veselí	veselí	k1gNnSc2	veselí
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
puritánské	puritánský	k2eAgFnSc2d1	puritánská
autority	autorita	k1gFnSc2	autorita
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
potlačily	potlačit	k5eAaPmAgInP	potlačit
předkřesťanské	předkřesťanský	k2eAgInPc4d1	předkřesťanský
rituály	rituál	k1gInPc4	rituál
spojené	spojený	k2eAgInPc4d1	spojený
se	s	k7c7	s
svátky	svátek	k1gInPc7	svátek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
počátek	počátek	k1gInSc1	počátek
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
zdařilých	zdařilý	k2eAgFnPc2d1	zdařilá
pohádek	pohádka	k1gFnPc2	pohádka
Hanse	Hans	k1gMnSc2	Hans
Christiana	Christian	k1gMnSc2	Christian
Andersena	Andersen	k1gMnSc2	Andersen
<g/>
,	,	kIx,	,
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
královna	královna	k1gFnSc1	královna
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
jeho	jeho	k3xOp3gNnSc1	jeho
čtivo	čtivo	k1gNnSc1	čtivo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgNnSc6d1	rozhlasové
zpracování	zpracování	k1gNnSc6	zpracování
také	také	k9	také
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
literatuře	literatura	k1gFnSc6	literatura
najdeme	najít	k5eAaPmIp1nP	najít
například	například	k6eAd1	například
novelu	novela	k1gFnSc4	novela
Bergkristall	Bergkristalla	k1gFnPc2	Bergkristalla
(	(	kIx(	(
<g/>
č.	č.	k?	č.
Horský	horský	k2eAgInSc4d1	horský
křišťál	křišťál	k1gInSc4	křišťál
<g/>
,	,	kIx,	,
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
od	od	k7c2	od
šumavského	šumavský	k2eAgMnSc2d1	šumavský
rodáka	rodák	k1gMnSc2	rodák
Adalberta	Adalbert	k1gMnSc2	Adalbert
Stiftera	Stifter	k1gMnSc2	Stifter
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
příběh	příběh	k1gInSc4	příběh
dvou	dva	k4xCgFnPc2	dva
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
vánici	vánice	k1gFnSc6	vánice
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
May	May	k1gMnSc1	May
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
tématu	téma	k1gNnSc2	téma
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Weihnacht	Weihnachta	k1gFnPc2	Weihnachta
(	(	kIx(	(
<g/>
čes.	čes.	k?	čes.
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
dobrodružný	dobrodružný	k2eAgInSc4d1	dobrodružný
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
Old	Olda	k1gFnPc2	Olda
Shatterhanda	Shatterhando	k1gNnSc2	Shatterhando
jako	jako	k9	jako
hluboce	hluboko	k6eAd1	hluboko
věřícího	věřící	k2eAgMnSc4d1	věřící
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
neustále	neustále	k6eAd1	neustále
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
význam	význam	k1gInSc1	význam
víry	víra	k1gFnSc2	víra
v	v	k7c6	v
životě	život	k1gInSc6	život
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Vánocemi	Vánoce	k1gFnPc7	Vánoce
a	a	k8xC	a
způsobem	způsob	k1gInSc7	způsob
jejich	jejich	k3xOp3gFnSc2	jejich
davové	davový	k2eAgFnSc2d1	davová
oslavy	oslava	k1gFnSc2	oslava
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
důvody	důvod	k1gInPc4	důvod
neslavení	neslavení	k1gNnPc2	neslavení
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
humorně	humorně	k6eAd1	humorně
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
John	John	k1gMnSc1	John
Grisham	Grisham	k1gInSc1	Grisham
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
románu	román	k1gInSc6	román
Skipping	Skipping	k1gInSc1	Skipping
Christmas	Christmas	k1gInSc1	Christmas
(	(	kIx(	(
<g/>
č.	č.	k?	č.
Vánoce	Vánoce	k1gFnPc1	Vánoce
nebudou	být	k5eNaImBp3nP	být
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
<s>
<g/>
Téma	téma	k1gNnSc1	téma
Vánoc	Vánoce	k1gFnPc2	Vánoce
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
především	především	k9	především
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
tak	tak	k6eAd1	tak
šířili	šířit	k5eAaImAgMnP	šířit
české	český	k2eAgNnSc4d1	české
povědomí	povědomí	k1gNnSc4	povědomí
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
tradice	tradice	k1gFnPc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc4	popis
Vánoc	Vánoce	k1gFnPc2	Vánoce
či	či	k8xC	či
vánoční	vánoční	k2eAgFnSc4d1	vánoční
tematiku	tematika	k1gFnSc4	tematika
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
v	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
i	i	k8xC	i
próze	próza	k1gFnSc6	próza
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
sbírky	sbírka	k1gFnSc2	sbírka
Kytice	kytice	k1gFnSc2	kytice
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
je	být	k5eAaImIp3nS	být
i	i	k9	i
balada	balada	k1gFnSc1	balada
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
dívky	dívka	k1gFnPc1	dívka
se	se	k3xPyFc4	se
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
vydají	vydat	k5eAaPmIp3nP	vydat
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
podle	podle	k7c2	podle
staré	starý	k2eAgFnSc2d1	stará
pověry	pověra	k1gFnSc2	pověra
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
jezera	jezero	k1gNnSc2	jezero
spatřily	spatřit	k5eAaPmAgInP	spatřit
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
uvidí	uvidět	k5eAaPmIp3nS	uvidět
svatbu	svatba	k1gFnSc4	svatba
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Obé	obé	k1gNnSc1	obé
se	se	k3xPyFc4	se
do	do	k7c2	do
roka	rok	k1gInSc2	rok
vyplní	vyplnit	k5eAaPmIp3nS	vyplnit
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
básní	báseň	k1gFnPc2	báseň
Romance	romance	k1gFnSc2	romance
štědrovečerní	štědrovečerní	k2eAgFnSc2d1	štědrovečerní
(	(	kIx(	(
<g/>
Balady	balada	k1gFnSc2	balada
a	a	k8xC	a
romance	romance	k1gFnSc2	romance
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
vánoční	vánoční	k2eAgFnSc1d1	vánoční
(	(	kIx(	(
<g/>
Zpěvy	zpěv	k1gInPc1	zpěv
páteční	páteční	k2eAgInPc1d1	páteční
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Známou	známý	k2eAgFnSc4d1	známá
báseň	báseň	k1gFnSc4	báseň
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
Co	co	k3yRnSc4	co
život	život	k1gInSc1	život
dal	dát	k5eAaPmAgInS	dát
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaPmAgMnS	napsat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
pobytů	pobyt	k1gInPc2	pobyt
v	v	k7c6	v
Čisté	čistá	k1gFnSc6	čistá
u	u	k7c2	u
Rakovníka	Rakovník	k1gInSc2	Rakovník
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
a	a	k8xC	a
vánoční	vánoční	k2eAgInPc1d1	vánoční
zvyky	zvyk	k1gInPc1	zvyk
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
venkově	venkov	k1gInSc6	venkov
líčí	líčit	k5eAaImIp3nS	líčit
i	i	k9	i
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Babičce	babička	k1gFnSc6	babička
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
kupeckého	kupecký	k2eAgMnSc2d1	kupecký
učně	učeň	k1gMnSc2	učeň
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
městského	městský	k2eAgMnSc2d1	městský
obchodníka	obchodník	k1gMnSc2	obchodník
popisuje	popisovat	k5eAaImIp3nS	popisovat
Ignát	Ignát	k1gMnSc1	Ignát
Herrmann	Herrmann	k1gMnSc1	Herrmann
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
povídce	povídka	k1gFnSc6	povídka
Tobiáškův	Tobiáškův	k2eAgInSc4d1	Tobiáškův
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Mrštíkové	Mrštíková	k1gFnSc2	Mrštíková
věnovali	věnovat	k5eAaImAgMnP	věnovat
Vánocům	Vánoce	k1gFnPc3	Vánoce
kapitolu	kapitola	k1gFnSc4	kapitola
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Rok	rok	k1gInSc1	rok
na	na	k7c6	na
vsi	ves	k1gFnSc6	ves
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Popisují	popisovat	k5eAaImIp3nP	popisovat
zde	zde	k6eAd1	zde
Vánoce	Vánoce	k1gFnPc1	Vánoce
na	na	k7c6	na
Slovácku	Slovácko	k1gNnSc6	Slovácko
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
tradice	tradice	k1gFnPc4	tradice
spojené	spojený	k2eAgFnPc4d1	spojená
se	s	k7c7	s
slavením	slavení	k1gNnSc7	slavení
jsou	být	k5eAaImIp3nP	být
vydávány	vydáván	k2eAgFnPc1d1	vydávána
knihy	kniha	k1gFnPc1	kniha
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
koled	koleda	k1gFnPc2	koleda
(	(	kIx(	(
<g/>
např	např	kA	např
kniha	kniha	k1gFnSc1	kniha
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
koledy	koleda	k1gFnPc1	koleda
s	s	k7c7	s
nahrávkami	nahrávka	k1gFnPc7	nahrávka
a	a	k8xC	a
notovým	notový	k2eAgInSc7d1	notový
zápisem	zápis	k1gInSc7	zápis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vaření	vaření	k1gNnSc1	vaření
vánočních	vánoční	k2eAgInPc2d1	vánoční
pokrmů	pokrm	k1gInPc2	pokrm
(	(	kIx(	(
<g/>
např	např	kA	např
kniha	kniha	k1gFnSc1	kniha
Nejchutnější	chutný	k2eAgFnSc1d3	nejchutnější
vánoční	vánoční	k2eAgNnSc4d1	vánoční
cukroví	cukroví	k1gNnSc4	cukroví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zdobení	zdobení	k1gNnSc1	zdobení
interiérů	interiér	k1gInPc2	interiér
(	(	kIx(	(
<g/>
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
inspirace	inspirace	k1gFnSc1	inspirace
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Markéta	Markéta	k1gFnSc1	Markéta
Lukášová	Lukášová	k1gFnSc1	Lukášová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zdařilé	zdařilý	k2eAgFnPc4d1	zdařilá
jsou	být	k5eAaImIp3nP	být
prodejci	prodejce	k1gMnPc1	prodejce
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zařazovány	zařazovat	k5eAaImNgFnP	zařazovat
díla	dílo	k1gNnPc4	dílo
jako	jako	k8xC	jako
dílo	dílo	k1gNnSc4	dílo
Lucie	Lucie	k1gFnSc1	Lucie
Kochové	Kochové	k2eAgFnSc1d1	Kochové
Velká	velký	k2eAgFnSc1d1	velká
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
nebo	nebo	k8xC	nebo
Česká	český	k2eAgFnSc1d1	Česká
vánoční	vánoční	k2eAgFnSc1d1	vánoční
kniha	kniha	k1gFnSc1	kniha
Michaely	Michaela	k1gFnSc2	Michaela
Zindelové	Zindelová	k1gFnSc2	Zindelová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
vydávány	vydáván	k2eAgFnPc1d1	vydávána
knihy	kniha	k1gFnPc1	kniha
obsahující	obsahující	k2eAgFnSc2d1	obsahující
sbírky	sbírka	k1gFnSc2	sbírka
povídek	povídka	k1gFnPc2	povídka
nebo	nebo	k8xC	nebo
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
s	s	k7c7	s
vánoční	vánoční	k2eAgFnSc7d1	vánoční
tematikou	tematika	k1gFnSc7	tematika
od	od	k7c2	od
renomovaných	renomovaný	k2eAgMnPc2d1	renomovaný
literárních	literární	k2eAgMnPc2d1	literární
autorů	autor	k1gMnPc2	autor
minulých	minulý	k2eAgNnPc2d1	Minulé
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgNnSc4d1	vánoční
a	a	k8xC	a
předvánoční	předvánoční	k2eAgNnSc4d1	předvánoční
období	období	k1gNnSc4	období
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
USA	USA	kA	USA
i	i	k9	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
konají	konat	k5eAaImIp3nP	konat
premiéry	premiéra	k1gFnPc1	premiéra
vysokorozpočtových	vysokorozpočtův	k2eAgInPc2d1	vysokorozpočtův
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1	tvůrce
si	se	k3xPyFc3	se
od	od	k7c2	od
období	období	k1gNnSc2	období
svátků	svátek	k1gInPc2	svátek
slibují	slibovat	k5eAaImIp3nP	slibovat
větší	veliký	k2eAgInPc1d2	veliký
zisky	zisk	k1gInPc1	zisk
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
blízkosti	blízkost	k1gFnSc3	blízkost
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
i	i	k8xC	i
větší	veliký	k2eAgFnSc4d2	veliký
naději	naděje	k1gFnSc4	naděje
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
nominace	nominace	k1gFnPc4	nominace
na	na	k7c4	na
filmovou	filmový	k2eAgFnSc4d1	filmová
cenu	cena	k1gFnSc4	cena
Oscar	Oscar	k1gMnSc1	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
filmů	film	k1gInPc2	film
s	s	k7c7	s
vánoční	vánoční	k2eAgFnSc7d1	vánoční
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
A	a	k9	a
Christmas	Christmas	k1gInSc1	Christmas
Carol	Carola	k1gFnPc2	Carola
od	od	k7c2	od
Charlese	Charles	k1gMnSc2	Charles
Dickense	Dickens	k1gMnSc2	Dickens
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
hned	hned	k9	hned
několika	několik	k4yIc2	několik
filmových	filmový	k2eAgFnPc2d1	filmová
adaptací	adaptace	k1gFnPc2	adaptace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
velkou	velký	k2eAgFnSc4d1	velká
popularitu	popularita	k1gFnSc4	popularita
získal	získat	k5eAaPmAgInS	získat
film	film	k1gInSc1	film
Zázrak	zázrak	k1gInSc1	zázrak
v	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
populární	populární	k2eAgFnSc1d1	populární
série	série	k1gFnSc1	série
filmů	film	k1gInPc2	film
Sám	sám	k3xTgMnSc1	sám
doma	doma	k6eAd1	doma
(	(	kIx(	(
<g/>
Sám	sám	k3xTgInSc1	sám
doma	doma	k6eAd1	doma
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sám	sám	k3xTgMnSc1	sám
doma	doma	k6eAd1	doma
2	[number]	k4	2
<g/>
:	:	kIx,	:
Ztracen	ztracen	k2eAgInSc1d1	ztracen
v	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
méně	málo	k6eAd2	málo
známé	známý	k2eAgInPc4d1	známý
filmy	film	k1gInPc4	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizi	televize	k1gFnSc6	televize
se	se	k3xPyFc4	se
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
tradičně	tradičně	k6eAd1	tradičně
vysílají	vysílat	k5eAaImIp3nP	vysílat
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
či	či	k8xC	či
show	show	k1gFnSc1	show
s	s	k7c7	s
vánoční	vánoční	k2eAgFnSc7d1	vánoční
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejoblíbenějším	oblíbený	k2eAgInPc3d3	nejoblíbenější
vánočním	vánoční	k2eAgInPc3d1	vánoční
pořadům	pořad	k1gInPc3	pořad
výpravné	výpravný	k2eAgFnSc2d1	výpravná
filmové	filmový	k2eAgFnSc2d1	filmová
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
z	z	k7c2	z
klasických	klasický	k2eAgInPc2d1	klasický
například	například	k6eAd1	například
Pyšná	pyšný	k2eAgFnSc1d1	pyšná
princezna	princezna	k1gFnSc1	princezna
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
hvězdou	hvězda	k1gFnSc7	hvězda
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tři	tři	k4xCgInPc1	tři
oříšky	oříšek	k1gInPc1	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
či	či	k8xC	či
ruský	ruský	k2eAgInSc1d1	ruský
Mrazík	mrazík	k1gInSc1	mrazík
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
modernějších	moderní	k2eAgMnPc2d2	modernější
pak	pak	k6eAd1	pak
třeba	třeba	k6eAd1	třeba
Nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
teta	teta	k1gFnSc1	teta
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
ze	z	k7c2	z
mlejna	mlejn	k1gInSc2	mlejn
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
ze	z	k7c2	z
mlejna	mlejn	k1gInSc2	mlejn
2	[number]	k4	2
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
či	či	k8xC	či
Anděl	Anděla	k1gFnPc2	Anděla
Páně	páně	k2eAgFnSc1d1	páně
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
mezi	mezi	k7c4	mezi
zdařilé	zdařilý	k2eAgInPc4d1	zdařilý
počiny	počin	k1gInPc4	počin
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
patří	patřit	k5eAaImIp3nS	patřit
televizní	televizní	k2eAgNnSc4d1	televizní
zpracování	zpracování	k1gNnSc4	zpracování
novely	novela	k1gFnSc2	novela
Charlese	Charles	k1gMnSc2	Charles
Dickense	Dickens	k1gMnSc2	Dickens
A	a	k8xC	a
Christmas	Christmas	k1gInSc4	Christmas
Carol	Carola	k1gFnPc2	Carola
s	s	k7c7	s
názvem	název	k1gInSc7	název
Duch	duch	k1gMnSc1	duch
Vánoc	Vánoce	k1gFnPc2	Vánoce
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veršovaný	veršovaný	k2eAgInSc1d1	veršovaný
Grinch	Grinch	k1gInSc1	Grinch
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
s	s	k7c7	s
Jimem	Jimum	k1gNnSc7	Jimum
Carreym	Carreym	k1gInSc1	Carreym
<g/>
,	,	kIx,	,
animovaný	animovaný	k2eAgInSc1d1	animovaný
Polární	polární	k2eAgInSc1d1	polární
expres	expres	k1gInSc1	expres
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Otec	otec	k1gMnSc1	otec
prasátek	prasátko	k1gNnPc2	prasátko
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dětmi	dítě	k1gFnPc7	dítě
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
Ledové	ledový	k2eAgNnSc1d1	ledové
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
řádných	řádný	k2eAgFnPc2d1	řádná
dvacetiminutových	dvacetiminutový	k2eAgFnPc2d1	dvacetiminutová
epizod	epizoda	k1gFnPc2	epizoda
seriálu	seriál	k1gInSc2	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Vánoce	Vánoce	k1gFnPc1	Vánoce
u	u	k7c2	u
Simpsonových	Simpsonová	k1gFnPc2	Simpsonová
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
jsou	být	k5eAaImIp3nP	být
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
tématem	téma	k1gNnSc7	téma
sitcomů	sitcom	k1gInPc2	sitcom
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
vánoční	vánoční	k2eAgInSc1d1	vánoční
speciál	speciál	k1gInSc1	speciál
sitcomu	sitcom	k1gInSc2	sitcom
Ženatý	ženatý	k2eAgMnSc1d1	ženatý
se	s	k7c7	s
závazky	závazek	k1gInPc7	závazek
–	–	k?	–
It	It	k1gFnPc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
A	A	kA	A
Bundyful	Bundyful	k1gInSc1	Bundyful
Life	Life	k1gFnSc1	Life
(	(	kIx(	(
<g/>
dvoudílná	dvoudílný	k2eAgFnSc1d1	dvoudílná
epizoda	epizoda	k1gFnSc1	epizoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
jsou	být	k5eAaImIp3nP	být
nakloněny	naklonit	k5eAaPmNgFnP	naklonit
ztřeštěným	ztřeštěný	k2eAgFnPc3d1	ztřeštěná
taškařicím	taškařice	k1gFnPc3	taškařice
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
veselými	veselý	k2eAgNnPc7d1	veselé
nedorozuměními	nedorozumění	k1gNnPc7	nedorozumění
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
komedie	komedie	k1gFnPc1	komedie
s	s	k7c7	s
vánoční	vánoční	k2eAgFnSc7d1	vánoční
tematikou	tematika	k1gFnSc7	tematika
jsou	být	k5eAaImIp3nP	být
oprávněně	oprávněně	k6eAd1	oprávněně
řazeny	řadit	k5eAaImNgInP	řadit
mezi	mezi	k7c7	mezi
horory	horor	k1gInPc7	horor
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
propagované	propagovaný	k2eAgFnPc4d1	propagovaná
vánoční	vánoční	k2eAgFnPc4d1	vánoční
komedie	komedie	k1gFnPc4	komedie
patří	patřit	k5eAaImIp3nS	patřit
tematická	tematický	k2eAgFnSc1d1	tematická
řada	řada	k1gFnSc1	řada
Sám	sám	k3xTgMnSc1	sám
doma	doma	k6eAd1	doma
(	(	kIx(	(
<g/>
pět	pět	k4xCc1	pět
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Strašidelné	strašidelný	k2eAgFnPc1d1	strašidelná
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
prázdniny	prázdniny	k1gFnPc1	prázdniny
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rolničky	rolnička	k1gFnSc2	rolnička
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
podíváš	podívat	k5eAaPmIp2nS	podívat
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tehdy	tehdy	k6eAd1	tehdy
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Santa	Santa	k1gMnSc1	Santa
je	být	k5eAaImIp3nS	být
úchyl	úchyl	k1gInSc4	úchyl
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ukradené	ukradený	k2eAgFnPc1d1	ukradená
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
nebeská	nebeský	k2eAgFnSc1d1	nebeská
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vánoce	Vánoce	k1gFnPc1	Vánoce
naruby	naruby	k6eAd1	naruby
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
jsou	být	k5eAaImIp3nP	být
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vezmeš	vzít	k5eAaPmIp2nS	vzít
si	se	k3xPyFc3	se
mě	já	k3xPp1nSc2	já
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čtvery	čtvero	k4xRgFnPc1	čtvero
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
vánoční	vánoční	k2eAgFnSc1d1	vánoční
jízda	jízda	k1gFnSc1	jízda
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
žánru	žánr	k1gInSc2	žánr
fantasy	fantas	k1gInPc4	fantas
spadá	spadat	k5eAaImIp3nS	spadat
filmové	filmový	k2eAgNnSc4d1	filmové
zpracování	zpracování	k1gNnSc4	zpracování
románu	román	k1gInSc2	román
Terryho	Terry	k1gMnSc2	Terry
Pratchetta	Pratchett	k1gInSc2	Pratchett
Otec	otec	k1gMnSc1	otec
prasátek	prasátko	k1gNnPc2	prasátko
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
vánoční	vánoční	k2eAgInPc4d1	vánoční
speciály	speciál	k1gInPc4	speciál
známých	známý	k2eAgFnPc2d1	známá
sérií	série	k1gFnPc2	série
jako	jako	k8xS	jako
Shrekovy	Shrekův	k2eAgFnPc1d1	Shrekova
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Doba	doba	k1gFnSc1	doba
ledová	ledový	k2eAgFnSc1d1	ledová
<g/>
:	:	kIx,	:
Mamutí	mamutí	k2eAgFnSc1d1	mamutí
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
či	či	k8xC	či
Šmoulokoleda	Šmoulokoleda	k1gMnSc1	Šmoulokoleda
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Horory	horor	k1gInPc1	horor
na	na	k7c4	na
vánoční	vánoční	k2eAgNnSc4d1	vánoční
téma	téma	k1gNnSc4	téma
jsou	být	k5eAaImIp3nP	být
výrazné	výrazný	k2eAgFnPc4d1	výrazná
mísením	mísení	k1gNnPc3	mísení
očekáváním	očekávání	k1gNnSc7	očekávání
sladkostí	sladkost	k1gFnPc2	sladkost
a	a	k8xC	a
klidu	klid	k1gInSc2	klid
s	s	k7c7	s
příběhem	příběh	k1gInSc7	příběh
plným	plný	k2eAgNnSc7d1	plné
brutálního	brutální	k2eAgInSc2d1	brutální
násilím	násilí	k1gNnSc7	násilí
a	a	k8xC	a
lekačkami	lekačka	k1gFnPc7	lekačka
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
na	na	k7c6	na
sněhu	sníh	k1gInSc6	sníh
esteticky	esteticky	k6eAd1	esteticky
vyniká	vynikat	k5eAaImIp3nS	vynikat
výrazným	výrazný	k2eAgInSc7d1	výrazný
kontrastem	kontrast	k1gInSc7	kontrast
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kombinování	kombinování	k1gNnSc1	kombinování
hororu	horor	k1gInSc2	horor
a	a	k8xC	a
Vánoc	Vánoce	k1gFnPc2	Vánoce
jednoduše	jednoduše	k6eAd1	jednoduše
komerčně	komerčně	k6eAd1	komerčně
většinou	většinou	k6eAd1	většinou
nefunguje	fungovat	k5eNaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
vánoční	vánoční	k2eAgInPc1d1	vánoční
horory	horor	k1gInPc1	horor
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
kombinovat	kombinovat	k5eAaImF	kombinovat
ve	v	k7c6	v
vánočním	vánoční	k2eAgNnSc6d1	vánoční
aranžmá	aranžmá	k1gNnSc6	aranžmá
komedii	komedie	k1gFnSc4	komedie
a	a	k8xC	a
strašidelný	strašidelný	k2eAgInSc4d1	strašidelný
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
téta	tét	k1gInSc2	tét
kategorie	kategorie	k1gFnSc2	kategorie
patří	patřit	k5eAaImIp3nP	patřit
filmy	film	k1gInPc1	film
jako	jako	k8xC	jako
Černé	Černé	k2eAgFnPc1d1	Černé
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
a	a	k8xC	a
remake	remake	k6eAd1	remake
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vánoční	vánoční	k2eAgNnSc4d1	vánoční
zlo	zlo	k1gNnSc4	zlo
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gremlins	Gremlins	k1gInSc1	Gremlins
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Krvavé	krvavý	k2eAgFnPc1d1	krvavá
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Silent	Silent	k1gMnSc1	Silent
Night	Night	k1gMnSc1	Night
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Deadly	Deadnout	k5eAaPmAgFnP	Deadnout
Night	Night	k1gMnSc1	Night
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vánoční	vánoční	k2eAgInSc1d1	vánoční
teror	teror	k1gInSc1	teror
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Santa	Santa	k1gMnSc1	Santa
Claws	Claws	k1gInSc1	Claws
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Frost	Frost	k1gMnSc1	Frost
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ďábelský	ďábelský	k2eAgMnSc1d1	ďábelský
Santa	Santa	k1gMnSc1	Santa
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
P2	P2	k1gMnSc1	P2
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vzácný	vzácný	k2eAgInSc1d1	vzácný
export	export	k1gInSc1	export
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mrtvoly	mrtvola	k1gFnPc4	mrtvola
přicházejí	přicházet	k5eAaImIp3nP	přicházet
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Romantika	romantika	k1gFnSc1	romantika
Vánoc	Vánoce	k1gFnPc2	Vánoce
podtrhuje	podtrhovat	k5eAaImIp3nS	podtrhovat
happyendy	happyend	k1gInPc4	happyend
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
vytvořit	vytvořit	k5eAaPmF	vytvořit
tu	ten	k3xDgFnSc4	ten
správnou	správný	k2eAgFnSc4d1	správná
atmosféru	atmosféra	k1gFnSc4	atmosféra
pro	pro	k7c4	pro
zamilované	zamilovaná	k1gFnPc4	zamilovaná
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Láska	láska	k1gFnSc1	láska
nebeská	nebeský	k2eAgFnSc1d1	nebeská
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
<g/>
,	,	kIx,	,
Katrin	Katrin	k1gInSc1	Katrin
a	a	k8xC	a
Vánoce	Vánoce	k1gFnPc1	Vánoce
se	se	k3xPyFc4	se
psem	pes	k1gMnSc7	pes
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Bláznivé	bláznivý	k2eAgFnPc1d1	bláznivá
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
význam	význam	k1gInSc1	význam
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
jsou	být	k5eAaImIp3nP	být
každoročním	každoroční	k2eAgInSc7d1	každoroční
vrcholem	vrchol	k1gInSc7	vrchol
prodejního	prodejní	k2eAgNnSc2d1	prodejní
období	období	k1gNnSc2	období
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
maloobchodníků	maloobchodník	k1gMnPc2	maloobchodník
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Prodej	prodej	k1gInSc1	prodej
se	se	k3xPyFc4	se
dramaticky	dramaticky	k6eAd1	dramaticky
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidé	člověk	k1gMnPc1	člověk
nakupují	nakupovat	k5eAaBmIp3nP	nakupovat
dárky	dárek	k1gInPc4	dárek
<g/>
,	,	kIx,	,
dekorace	dekorace	k1gFnPc1	dekorace
a	a	k8xC	a
zásoby	zásoba	k1gFnPc1	zásoba
k	k	k7c3	k
oslavám	oslava	k1gFnPc3	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
USA	USA	kA	USA
"	"	kIx"	"
<g/>
vánoční	vánoční	k2eAgFnSc1d1	vánoční
nákupní	nákupní	k2eAgFnSc1d1	nákupní
sezóna	sezóna	k1gFnSc1	sezóna
<g/>
"	"	kIx"	"
začíná	začínat	k5eAaImIp3nS	začínat
již	již	k6eAd1	již
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
nárůst	nárůst	k1gInSc4	nárůst
ceny	cena	k1gFnSc2	cena
objednávek	objednávka	k1gFnPc2	objednávka
internetových	internetový	k2eAgInPc2d1	internetový
obchodů	obchod	k1gInPc2	obchod
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
běžným	běžný	k2eAgInPc3d1	běžný
měsícům	měsíc	k1gInPc3	měsíc
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
při	při	k7c6	při
vánočním	vánoční	k2eAgInSc6d1	vánoční
prodeji	prodej	k1gInSc6	prodej
o	o	k7c4	o
desítky	desítka	k1gFnPc4	desítka
procent	procento	k1gNnPc2	procento
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
asi	asi	k9	asi
o	o	k7c4	o
300	[number]	k4	300
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
o	o	k7c4	o
600	[number]	k4	600
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
tento	tento	k3xDgInSc4	tento
nárůst	nárůst	k1gInSc4	nárůst
skokově	skokově	k6eAd1	skokově
43	[number]	k4	43
%	%	kIx~	%
a	a	k8xC	a
září	září	k1gNnSc4	září
a	a	k8xC	a
říjen	říjen	k1gInSc4	říjen
byly	být	k5eAaImAgFnP	být
podprůměrné	podprůměrný	k2eAgFnPc1d1	podprůměrná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
Češi	Čech	k1gMnPc1	Čech
utratili	utratit	k5eAaPmAgMnP	utratit
za	za	k7c4	za
vánoční	vánoční	k2eAgInPc4d1	vánoční
dárky	dárek	k1gInPc4	dárek
15	[number]	k4	15
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
společnosti	společnost	k1gFnSc2	společnost
Era	Era	k1gFnSc2	Era
odhadoval	odhadovat	k5eAaImAgMnS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
česká	český	k2eAgFnSc1d1	Česká
domácnost	domácnost	k1gFnSc1	domácnost
utratí	utratit	k5eAaPmIp3nS	utratit
za	za	k7c4	za
dárky	dárek	k1gInPc4	dárek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
kolem	kolem	k7c2	kolem
pěti	pět	k4xCc2	pět
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
cena	cena	k1gFnSc1	cena
dárku	dárek	k1gInSc2	dárek
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
téměř	téměř	k6eAd1	téměř
devět	devět	k4xCc1	devět
set	sto	k4xCgNnPc2	sto
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rozepře	rozepře	k1gFnSc2	rozepře
o	o	k7c4	o
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
byly	být	k5eAaImAgFnP	být
během	během	k7c2	během
času	čas	k1gInSc2	čas
předmětem	předmět	k1gInSc7	předmět
diskusí	diskuse	k1gFnPc2	diskuse
a	a	k8xC	a
útoků	útok	k1gInPc2	útok
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc4	zákaz
slavení	slavení	k1gNnSc3	slavení
Vánoc	Vánoce	k1gFnPc2	Vánoce
puritány	puritán	k1gMnPc4	puritán
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Skotsku	Skotsko	k1gNnSc6	Skotsko
a	a	k8xC	a
anglických	anglický	k2eAgFnPc6d1	anglická
koloniích	kolonie	k1gFnPc6	kolonie
byl	být	k5eAaImAgInS	být
veden	vést	k5eAaImNgInS	vést
snahou	snaha	k1gFnSc7	snaha
odstranit	odstranit	k5eAaPmF	odstranit
zbývající	zbývající	k2eAgInPc4d1	zbývající
pohanské	pohanský	k2eAgInPc4d1	pohanský
prvky	prvek	k1gInPc4	prvek
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
vánočních	vánoční	k2eAgFnPc2d1	vánoční
dekorací	dekorace	k1gFnPc2	dekorace
radními	radní	k2eAgInPc7d1	radní
města	město	k1gNnSc2	město
Oxford	Oxford	k1gInSc1	Oxford
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
Vánoce	Vánoce	k1gFnPc4	Vánoce
na	na	k7c4	na
'	'	kIx"	'
<g/>
Winter	Winter	k1gMnSc1	Winter
Light	Light	k1gMnSc1	Light
Festival	festival	k1gInSc1	festival
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
Svátek	svátek	k1gInSc1	svátek
zimních	zimní	k2eAgNnPc2d1	zimní
světel	světlo	k1gNnPc2	světlo
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
vedeno	veden	k2eAgNnSc1d1	vedeno
snahou	snaha	k1gFnSc7	snaha
o	o	k7c4	o
politickou	politický	k2eAgFnSc4d1	politická
korektnost	korektnost	k1gFnSc4	korektnost
a	a	k8xC	a
přesné	přesný	k2eAgNnSc4d1	přesné
dodržování	dodržování	k1gNnSc4	dodržování
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Školy	škola	k1gFnPc1	škola
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
USA	USA	kA	USA
byly	být	k5eAaImAgFnP	být
terčem	terč	k1gInSc7	terč
protestů	protest	k1gInPc2	protest
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejněn	k2eAgFnPc4d1	zveřejněna
informace	informace	k1gFnPc4	informace
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
už	už	k6eAd1	už
jen	jen	k9	jen
podezření	podezření	k1gNnSc4	podezření
či	či	k8xC	či
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
zpívat	zpívat	k5eAaImF	zpívat
a	a	k8xC	a
hrát	hrát	k5eAaImF	hrát
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
koledy	koleda	k1gFnPc4	koleda
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gInSc3	jejich
náboženskému	náboženský	k2eAgInSc3d1	náboženský
podtextu	podtext	k1gInSc3	podtext
<g/>
.	.	kIx.	.
</s>
<s>
Iniciativa	iniciativa	k1gFnSc1	iniciativa
vegetariánů	vegetarián	k1gMnPc2	vegetarián
a	a	k8xC	a
veganů	vegan	k1gMnPc2	vegan
"	"	kIx"	"
<g/>
Vánoce	Vánoce	k1gFnPc1	Vánoce
bez	bez	k7c2	bez
násilí	násilí	k1gNnSc2	násilí
<g/>
"	"	kIx"	"
pořádá	pořádat	k5eAaImIp3nS	pořádat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
každoročně	každoročně	k6eAd1	každoročně
akce	akce	k1gFnPc1	akce
směřované	směřovaný	k2eAgFnPc1d1	směřovaná
proti	proti	k7c3	proti
chovu	chov	k1gInSc3	chov
kaprů	kapr	k1gMnPc2	kapr
(	(	kIx(	(
<g/>
jakožto	jakožto	k8xS	jakožto
i	i	k9	i
jiných	jiný	k2eAgNnPc2d1	jiné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
vánoční	vánoční	k2eAgFnSc4d1	vánoční
večeři	večeře	k1gFnSc4	večeře
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
"	"	kIx"	"
<g/>
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
Ježíška	Ježíšek	k1gMnSc4	Ježíšek
<g/>
"	"	kIx"	"
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
využívání	využívání	k1gNnSc4	využívání
"	"	kIx"	"
<g/>
nečeských	český	k2eNgInPc2d1	nečeský
<g/>
"	"	kIx"	"
vánočních	vánoční	k2eAgInPc2d1	vánoční
symbolů	symbol	k1gInPc2	symbol
v	v	k7c6	v
kampaních	kampaň	k1gFnPc6	kampaň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
cílené	cílený	k2eAgFnPc1d1	cílená
především	především	k9	především
na	na	k7c4	na
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vánoční	vánoční	k2eAgNnSc4d1	vánoční
příměří	příměří	k1gNnSc4	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vojáci	voják	k1gMnPc1	voják
bojujících	bojující	k2eAgFnPc2d1	bojující
stran	strana	k1gFnPc2	strana
setkávali	setkávat	k5eAaImAgMnP	setkávat
a	a	k8xC	a
společně	společně	k6eAd1	společně
slavili	slavit	k5eAaImAgMnP	slavit
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
někde	někde	k6eAd1	někde
sehráli	sehrát	k5eAaPmAgMnP	sehrát
vánoční	vánoční	k2eAgNnSc4d1	vánoční
fotbalové	fotbalový	k2eAgNnSc4d1	fotbalové
utkání	utkání	k1gNnSc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
si	se	k3xPyFc3	se
vyměňovali	vyměňovat	k5eAaImAgMnP	vyměňovat
dary	dar	k1gInPc4	dar
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
adresy	adresa	k1gFnSc2	adresa
<g/>
,	,	kIx,	,
a	a	k8xC	a
pili	pít	k5eAaImAgMnP	pít
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
příměří	příměří	k1gNnSc1	příměří
začalo	začít	k5eAaPmAgNnS	začít
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
pohřbít	pohřbít	k5eAaPmF	pohřbít
mrtvé	mrtvý	k2eAgMnPc4d1	mrtvý
kamarády	kamarád	k1gMnPc4	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
upravován	upravovat	k5eAaImNgInS	upravovat
jako	jako	k8xS	jako
dramatické	dramatický	k2eAgNnSc1d1	dramatické
vyprávění	vyprávění	k1gNnSc1	vyprávění
v	v	k7c6	v
krátkých	krátký	k2eAgNnPc6d1	krátké
literárních	literární	k2eAgNnPc6d1	literární
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Betlémská	betlémský	k2eAgFnSc1d1	Betlémská
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Betlémská	betlémský	k2eAgFnSc1d1	Betlémská
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
popisovaná	popisovaný	k2eAgFnSc1d1	popisovaná
v	v	k7c6	v
Matoušově	Matoušův	k2eAgNnSc6d1	Matoušovo
evangeliu	evangelium	k1gNnSc6	evangelium
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
kometu	kometa	k1gFnSc4	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
jít	jít	k5eAaImF	jít
i	i	k9	i
o	o	k7c4	o
novou	nový	k2eAgFnSc4d1	nová
hvězdu	hvězda	k1gFnSc4	hvězda
(	(	kIx(	(
<g/>
novu	nova	k1gFnSc4	nova
či	či	k8xC	či
supernovu	supernova	k1gFnSc4	supernova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konjunkcím	konjunkce	k1gFnPc3	konjunkce
planet	planeta	k1gFnPc2	planeta
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
přikládán	přikládat	k5eAaImNgInS	přikládat
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
i	i	k9	i
mezi	mezi	k7c7	mezi
laiky	laik	k1gMnPc7	laik
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jistá	jistý	k2eAgFnSc1d1	jistá
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
konjunkce	konjunkce	k1gFnSc1	konjunkce
planet	planeta	k1gFnPc2	planeta
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
také	také	k9	také
jevem	jev	k1gInSc7	jev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
upoutal	upoutat	k5eAaPmAgInS	upoutat
pozornost	pozornost	k1gFnSc4	pozornost
vědců	vědec	k1gMnPc2	vědec
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
konjunkci	konjunkce	k1gFnSc4	konjunkce
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
mylně	mylně	k6eAd1	mylně
interpretována	interpretován	k2eAgFnSc1d1	interpretována
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
oblíbeným	oblíbený	k2eAgMnPc3d1	oblíbený
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ostatní	ostatní	k2eAgFnPc1d1	ostatní
možnosti	možnost	k1gFnPc1	možnost
o	o	k7c4	o
jaký	jaký	k3yIgInSc4	jaký
úkaz	úkaz	k1gInSc4	úkaz
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
vlastně	vlastně	k9	vlastně
šlo	jít	k5eAaImAgNnS	jít
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Kometa	kometa	k1gFnSc1	kometa
zobrazovaná	zobrazovaný	k2eAgFnSc1d1	zobrazovaná
na	na	k7c6	na
lidových	lidový	k2eAgInPc6d1	lidový
betlémech	betlém	k1gInPc6	betlém
byla	být	k5eAaImAgFnS	být
vytvářena	vytvářen	k2eAgFnSc1d1	vytvářena
až	až	k9	až
podle	podle	k7c2	podle
Halleyovy	Halleyův	k2eAgFnSc2d1	Halleyova
komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1301	[number]	k4	1301
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
však	však	k9	však
nemohou	moct	k5eNaImIp3nP	moct
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohla	moct	k5eAaImAgFnS	moct
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Kristova	Kristův	k2eAgNnSc2d1	Kristovo
narození	narození	k1gNnSc2	narození
být	být	k5eAaImF	být
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
viditelná	viditelný	k2eAgFnSc1d1	viditelná
jiná	jiný	k2eAgFnSc1d1	jiná
kometa	kometa	k1gFnSc1	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
zastává	zastávat	k5eAaImIp3nS	zastávat
například	například	k6eAd1	například
A.	A.	kA	A.
A.	A.	kA	A.
Borett	Borett	k1gMnSc1	Borett
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
nová	nový	k2eAgFnSc1d1	nová
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
astronomické	astronomický	k2eAgFnSc6d1	astronomická
terminologii	terminologie	k1gFnSc6	terminologie
buď	buď	k8xC	buď
nova	nova	k1gFnSc1	nova
nebo	nebo	k8xC	nebo
supernova	supernova	k1gFnSc1	supernova
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
pravděpodobnější	pravděpodobný	k2eAgFnSc4d2	pravděpodobnější
<g/>
.	.	kIx.	.
</s>
<s>
Zastáncem	zastánce	k1gMnSc7	zastánce
této	tento	k3xDgFnSc2	tento
možnosti	možnost	k1gFnSc2	možnost
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
A.	A.	kA	A.
J.	J.	kA	J.
Morehouse	Morehouse	k1gFnSc1	Morehouse
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
astronomů	astronom	k1gMnPc2	astronom
podporuje	podporovat	k5eAaImIp3nS	podporovat
názor	názor	k1gInSc1	názor
Johannese	Johannese	k1gFnSc2	Johannese
Keplera	Kepler	k1gMnSc2	Kepler
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
konjunkci	konjunkce	k1gFnSc4	konjunkce
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
tomuto	tento	k3xDgInSc3	tento
úkazu	úkaz	k1gInSc3	úkaz
připisuje	připisovat	k5eAaImIp3nS	připisovat
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
dřívější	dřívější	k2eAgFnSc6d1	dřívější
době	doba	k1gFnSc6	doba
s	s	k7c7	s
astronomií	astronomie	k1gFnSc7	astronomie
nerozlučně	rozlučně	k6eNd1	rozlučně
spjata	spjat	k2eAgFnSc1d1	spjata
<g/>
.	.	kIx.	.
</s>
<s>
Mudrci	mudrc	k1gMnPc1	mudrc
od	od	k7c2	od
východu	východ	k1gInSc2	východ
tedy	tedy	k9	tedy
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
astrology	astrolog	k1gMnPc4	astrolog
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vykládali	vykládat	k5eAaImAgMnP	vykládat
ze	z	k7c2	z
"	"	kIx"	"
<g/>
znamení	znamení	k1gNnSc2	znamení
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
<g/>
"	"	kIx"	"
věštby	věštba	k1gFnPc4	věštba
<g/>
.	.	kIx.	.
</s>
<s>
Konjunkce	konjunkce	k1gFnSc1	konjunkce
je	být	k5eAaImIp3nS	být
mohla	moct	k5eAaImAgFnS	moct
zavést	zavést	k5eAaPmF	zavést
do	do	k7c2	do
Betléma	Betlém	k1gInSc2	Betlém
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Johanese	Johanes	k1gMnSc5	Johanes
Keplera	Kepler	k1gMnSc4	Kepler
v	v	k7c6	v
roce	rok	k1gInSc6	rok
7	[number]	k4	7
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
došlo	dojít	k5eAaPmAgNnS	dojít
dokonce	dokonce	k9	dokonce
k	k	k7c3	k
trojnásobné	trojnásobný	k2eAgFnSc3d1	trojnásobná
konjunkci	konjunkce	k1gFnSc3	konjunkce
planet	planeta	k1gFnPc2	planeta
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Ryb	Ryby	k1gFnPc2	Ryby
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
i	i	k9	i
moderní	moderní	k2eAgInPc1d1	moderní
výpočty	výpočet	k1gInPc1	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úhlovému	úhlový	k2eAgNnSc3d1	úhlové
přiblížení	přiblížení	k1gNnSc3	přiblížení
Jupiteru	Jupiter	k1gInSc3	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc3	Saturn
došlo	dojít	k5eAaPmAgNnS	dojít
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
výpočtů	výpočet	k1gInPc2	výpočet
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
neobyčejně	obyčejně	k6eNd1	obyčejně
nápadný	nápadný	k2eAgInSc4d1	nápadný
úkaz	úkaz	k1gInSc4	úkaz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
narodil	narodit	k5eAaPmAgMnS	narodit
koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
či	či	k8xC	či
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
7	[number]	k4	7
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc4d1	jiná
verzi	verze	k1gFnSc4	verze
konjunkce	konjunkce	k1gFnSc2	konjunkce
jako	jako	k8xS	jako
Betlémské	betlémský	k2eAgFnSc2d1	Betlémská
hvězdy	hvězda	k1gFnSc2	hvězda
uvádí	uvádět	k5eAaImIp3nS	uvádět
R.	R.	kA	R.
W.	W.	kA	W.
Sinnott	Sinnott	k1gMnSc1	Sinnott
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vánoční	vánoční	k2eAgInSc4d1	vánoční
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInSc1d1	vánoční
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
zámořským	zámořský	k2eAgNnSc7d1	zámořské
územím	území	k1gNnSc7	území
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
mořeplavci	mořeplavec	k1gMnPc7	mořeplavec
znám	znát	k5eAaImIp1nS	znát
již	již	k6eAd1	již
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
ostrova	ostrov	k1gInSc2	ostrov
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
kapitána	kapitán	k1gMnSc2	kapitán
Williama	William	k1gMnSc2	William
Mynorse	Mynorse	k1gFnSc2	Mynorse
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
tak	tak	k6eAd1	tak
ostrov	ostrov	k1gInSc4	ostrov
nazval	nazvat	k5eAaBmAgMnS	nazvat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gNnSc2	on
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1643	[number]	k4	1643
plul	plout	k5eAaImAgMnS	plout
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vánoční	vánoční	k2eAgInSc1d1	vánoční
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
Kiritimati	Kiritimati	k1gFnSc1	Kiritimati
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kiritimati	Kiritimat	k5eAaImF	Kiritimat
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
jako	jako	k9	jako
Vánoční	vánoční	k2eAgInSc4d1	vánoční
ostrov	ostrov	k1gInSc4	ostrov
kapitán	kapitán	k1gMnSc1	kapitán
James	James	k1gMnSc1	James
Cook	Cook	k1gMnSc1	Cook
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jej	on	k3xPp3gMnSc4	on
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
atol	atol	k1gInSc1	atol
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
řetězci	řetězec	k1gInSc6	řetězec
Liniových	liniový	k2eAgInPc2d1	liniový
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgInSc1d3	veliký
korálový	korálový	k2eAgInSc1d1	korálový
atol	atol	k1gInSc1	atol
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
GfK	GfK	k1gFnSc2	GfK
České	český	k2eAgFnSc2d1	Česká
Vánoce	Vánoce	k1gFnPc1	Vánoce
ve	v	k7c6	v
statistice	statistika	k1gFnSc6	statistika
umělý	umělý	k2eAgInSc4d1	umělý
vánoční	vánoční	k2eAgInSc4d1	vánoční
strom	strom	k1gInSc4	strom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
preferovalo	preferovat	k5eAaImAgNnS	preferovat
36	[number]	k4	36
%	%	kIx~	%
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
2	[number]	k4	2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
ČR	ČR	kA	ČR
vánoční	vánoční	k2eAgInSc1d1	vánoční
strom	strom	k1gInSc1	strom
vůbec	vůbec	k9	vůbec
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stole	stol	k1gInSc6	stol
k	k	k7c3	k
sváteční	sváteční	k2eAgFnSc3d1	sváteční
večeři	večeře	k1gFnSc3	večeře
mělo	mít	k5eAaImAgNnS	mít
bramborový	bramborový	k2eAgInSc4d1	bramborový
salát	salát	k1gInSc4	salát
89	[number]	k4	89
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
ČR	ČR	kA	ČR
a	a	k8xC	a
kapra	kapr	k1gMnSc2	kapr
67	[number]	k4	67
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
tráví	trávit	k5eAaImIp3nS	trávit
94	[number]	k4	94
%	%	kIx~	%
Čechů	Čech	k1gMnPc2	Čech
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Vánočních	vánoční	k2eAgInPc2d1	vánoční
zvyků	zvyk	k1gInPc2	zvyk
téměř	téměř	k6eAd1	téměř
třetina	třetina	k1gFnSc1	třetina
Čechů	Čech	k1gMnPc2	Čech
udržuje	udržovat	k5eAaImIp3nS	udržovat
tradici	tradice	k1gFnSc4	tradice
půstu	půst	k1gInSc2	půst
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
třetina	třetina	k1gFnSc1	třetina
Čechů	Čech	k1gMnPc2	Čech
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
také	také	k9	také
hlásí	hlásit	k5eAaImIp3nP	hlásit
k	k	k7c3	k
nějakému	nějaký	k3yIgInSc3	nějaký
směru	směr	k1gInSc3	směr
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
navštíví	navštívit	k5eAaPmIp3nS	navštívit
vánoční	vánoční	k2eAgFnSc4d1	vánoční
mši	mše	k1gFnSc4	mše
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
dlouholetém	dlouholetý	k2eAgNnSc6d1	dlouholeté
potlačování	potlačování	k1gNnSc6	potlačování
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
ke	k	k7c3	k
křesťanské	křesťanský	k2eAgFnSc3d1	křesťanská
víře	víra	k1gFnSc3	víra
hlásilo	hlásit	k5eAaImAgNnS	hlásit
39	[number]	k4	39
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
statistiky	statistika	k1gFnSc2	statistika
zaznamenán	zaznamenán	k2eAgInSc1d1	zaznamenán
další	další	k2eAgInSc1d1	další
pokles	pokles	k1gInSc1	pokles
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
že	že	k8xS	že
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
určitě	určitě	k6eAd1	určitě
nevěří	věřit	k5eNaImIp3nS	věřit
na	na	k7c4	na
42	[number]	k4	42
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
návštěva	návštěva	k1gFnSc1	návštěva
kostela	kostel	k1gInSc2	kostel
vánočním	vánoční	k2eAgInSc7d1	vánoční
zvykem	zvyk	k1gInSc7	zvyk
i	i	k9	i
v	v	k7c6	v
necelé	celý	k2eNgFnSc6d1	necelá
třetině	třetina	k1gFnSc6	třetina
rodin	rodina	k1gFnPc2	rodina
kde	kde	k6eAd1	kde
spíše	spíše	k9	spíše
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
nevěří	věřit	k5eNaImIp3nS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgNnSc1d1	vánoční
jmelí	jmelí	k1gNnSc1	jmelí
má	mít	k5eAaImIp3nS	mít
doma	doma	k6eAd1	doma
a	a	k8xC	a
tradici	tradice	k1gFnSc4	tradice
s	s	k7c7	s
kapří	kapří	k2eAgFnSc7d1	kapří
šupinou	šupina	k1gFnSc7	šupina
pod	pod	k7c7	pod
talířem	talíř	k1gInSc7	talíř
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Jmelí	jmelí	k1gNnSc1	jmelí
věší	věšet	k5eAaImIp3nS	věšet
častěji	často	k6eAd2	často
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgNnSc1d1	vánoční
cukroví	cukroví	k1gNnSc1	cukroví
peče	péct	k5eAaImIp3nS	péct
61	[number]	k4	61
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
je	být	k5eAaImIp3nS	být
průzkumem	průzkum	k1gInSc7	průzkum
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
pověrčivé	pověrčivý	k2eAgNnSc4d1	pověrčivé
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
osoby	osoba	k1gFnPc1	osoba
nevěší	věšet	k5eNaImIp3nP	věšet
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
prádlo	prádlo	k1gNnSc1	prádlo
<g/>
,	,	kIx,	,
nechodí	chodit	k5eNaImIp3nS	chodit
do	do	k7c2	do
hospody	hospody	k?	hospody
a	a	k8xC	a
nehrají	hrát	k5eNaImIp3nP	hrát
karty	karta	k1gFnPc1	karta
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInPc1d1	vánoční
zvyky	zvyk	k1gInPc1	zvyk
jsou	být	k5eAaImIp3nP	být
častěji	často	k6eAd2	často
praktikovány	praktikovat	k5eAaImNgInP	praktikovat
v	v	k7c6	v
rodinách	rodina	k1gFnPc6	rodina
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
70	[number]	k4	70
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
rodina	rodina	k1gFnSc1	rodina
svolává	svolávat	k5eAaImIp3nS	svolávat
zvonečkem	zvoneček	k1gInSc7	zvoneček
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nalezli	naleznout	k5eAaPmAgMnP	naleznout
a	a	k8xC	a
rozbalovali	rozbalovat	k5eAaImAgMnP	rozbalovat
dárky	dárek	k1gInPc4	dárek
pod	pod	k7c7	pod
stromkem	stromek	k1gInSc7	stromek
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
asi	asi	k9	asi
třetina	třetina	k1gFnSc1	třetina
mužů	muž	k1gMnPc2	muž
zpívá	zpívat	k5eAaImIp3nS	zpívat
u	u	k7c2	u
stromečku	stromeček	k1gInSc2	stromeček
koledy	koleda	k1gFnSc2	koleda
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
házení	házení	k1gNnSc2	házení
střevícem	střevíc	k1gInSc7	střevíc
přes	přes	k7c4	přes
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
lití	lití	k1gNnSc4	lití
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
pověrou	pověra	k1gFnSc7	pověra
věštění	věštění	k1gNnSc4	věštění
ženicha	ženich	k1gMnSc2	ženich
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
praktikovány	praktikovat	k5eAaImNgInP	praktikovat
nejméně	málo	k6eAd3	málo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistických	statistický	k2eAgInPc2d1	statistický
přehledů	přehled	k1gInPc2	přehled
dat	datum	k1gNnPc2	datum
na	na	k7c6	na
síti	síť	k1gFnSc6	síť
Facebook	Facebook	k1gInSc4	Facebook
a	a	k8xC	a
Twitter	Twitter	k1gInSc4	Twitter
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
přenášených	přenášený	k2eAgNnPc2d1	přenášené
dat	datum	k1gNnPc2	datum
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
silný	silný	k2eAgInSc4d1	silný
pokles	pokles	k1gInSc4	pokles
mezi	mezi	k7c7	mezi
denní	denní	k2eAgFnSc7d1	denní
špičkou	špička	k1gFnSc7	špička
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nastává	nastávat	k5eAaImIp3nS	nastávat
kolem	kolem	k7c2	kolem
14	[number]	k4	14
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
až	až	k9	až
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
přenášených	přenášený	k2eAgNnPc2d1	přenášené
dat	datum	k1gNnPc2	datum
poté	poté	k6eAd1	poté
opět	opět	k6eAd1	opět
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
špičkou	špička	k1gFnSc7	špička
mezi	mezi	k7c7	mezi
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
hodinou	hodina	k1gFnSc7	hodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
výrazněji	výrazně	k6eAd2	výrazně
klesá	klesat	k5eAaImIp3nS	klesat
až	až	k9	až
kolem	kolem	k7c2	kolem
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
18	[number]	k4	18
<g/>
.	.	kIx.	.
hodině	hodina	k1gFnSc6	hodina
také	také	k6eAd1	také
silně	silně	k6eAd1	silně
narůstá	narůstat	k5eAaImIp3nS	narůstat
objem	objem	k1gInSc1	objem
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
online	onlinout	k5eAaPmIp3nS	onlinout
her	hra	k1gFnPc2	hra
a	a	k8xC	a
internetového	internetový	k2eAgNnSc2d1	internetové
telefonování	telefonování	k1gNnSc2	telefonování
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
přenesených	přenesený	k2eAgNnPc2d1	přenesené
dat	datum	k1gNnPc2	datum
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
o	o	k7c6	o
běžném	běžný	k2eAgInSc6d1	běžný
víkendu	víkend	k1gInSc6	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Silvestra	Silvestr	k1gMnSc4	Silvestr
objem	objem	k1gInSc4	objem
přenesených	přenesený	k2eAgNnPc2d1	přenesené
dat	datum	k1gNnPc2	datum
postupně	postupně	k6eAd1	postupně
klesá	klesat	k5eAaImIp3nS	klesat
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnPc1	hodina
se	se	k3xPyFc4	se
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
propadem	propad	k1gInSc7	propad
objemu	objem	k1gInSc2	objem
přenesených	přenesený	k2eAgNnPc2d1	přenesené
dat	datum	k1gNnPc2	datum
okolo	okolo	k7c2	okolo
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
přenesených	přenesený	k2eAgNnPc2d1	přenesené
dat	datum	k1gNnPc2	datum
na	na	k7c4	na
Silvestra	Silvestr	k1gMnSc4	Silvestr
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
15	[number]	k4	15
procent	procento	k1gNnPc2	procento
nižší	nízký	k2eAgMnSc1d2	nižší
oproti	oproti	k7c3	oproti
běžnému	běžný	k2eAgInSc3d1	běžný
víkendu	víkend	k1gInSc3	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Trend	trend	k1gInSc1	trend
rozložení	rozložení	k1gNnSc1	rozložení
sebevražd	sebevražda	k1gFnPc2	sebevražda
podle	podle	k7c2	podle
měsíců	měsíc	k1gInPc2	měsíc
platil	platit	k5eAaImAgInS	platit
za	za	k7c4	za
roky	rok	k1gInPc4	rok
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
stejně	stejně	k6eAd1	stejně
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
i	i	k8xC	i
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgInSc1d3	nejnižší
počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Štědrého	štědrý	k2eAgInSc2d1	štědrý
večera	večer	k1gInSc2	večer
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
podprůměrný	podprůměrný	k2eAgInSc1d1	podprůměrný
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
poloviční	poloviční	k2eAgFnSc2d1	poloviční
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
dnům	den	k1gInPc3	den
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgInS	být
Silvestr	Silvestr	k1gInSc1	Silvestr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
v	v	k7c6	v
uvedených	uvedený	k2eAgNnPc6d1	uvedené
letech	léto	k1gNnPc6	léto
nejméně	málo	k6eAd3	málo
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
oproti	oproti	k7c3	oproti
běžnému	běžný	k2eAgInSc3d1	běžný
dennímu	denní	k2eAgInSc3d1	denní
porůměru	porůměr	k1gInSc3	porůměr
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
první	první	k4xOgInPc4	první
dny	den	k1gInPc4	den
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
páchány	páchán	k2eAgFnPc1d1	páchána
sebevraždy	sebevražda	k1gFnPc1	sebevražda
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
statistických	statistický	k2eAgFnPc6d1	statistická
hranicích	hranice	k1gFnPc6	hranice
běžného	běžný	k2eAgInSc2d1	běžný
denního	denní	k2eAgInSc2d1	denní
průměru	průměr	k1gInSc2	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
slaví	slavit	k5eAaImIp3nP	slavit
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
,	,	kIx,	,
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Starobylé	starobylý	k2eAgFnPc1d1	starobylá
komunity	komunita	k1gFnPc1	komunita
křesťanů	křesťan	k1gMnPc2	křesťan
se	se	k3xPyFc4	se
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
jinam	jinam	k6eAd1	jinam
<g/>
,	,	kIx,	,
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
cílem	cíl	k1gInSc7	cíl
vraždění	vraždění	k1gNnSc2	vraždění
<g/>
,	,	kIx,	,
útoků	útok	k1gInPc2	útok
<g/>
,	,	kIx,	,
znásilňování	znásilňování	k1gNnSc1	znásilňování
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
ČR	ČR	kA	ČR
si	se	k3xPyFc3	se
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
půjčují	půjčovat	k5eAaImIp3nP	půjčovat
průměrně	průměrně	k6eAd1	průměrně
7	[number]	k4	7
300	[number]	k4	300
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
půjčkou	půjčka	k1gFnSc7	půjčka
u	u	k7c2	u
nebankovní	bankovní	k2eNgFnSc2d1	nebankovní
instituce	instituce	k1gFnSc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
půjčky	půjčka	k1gFnPc1	půjčka
stahují	stahovat	k5eAaImIp3nP	stahovat
někdy	někdy	k6eAd1	někdy
obyvatele	obyvatel	k1gMnPc4	obyvatel
do	do	k7c2	do
dluhové	dluhový	k2eAgFnSc2d1	dluhová
pasti	past	k1gFnSc2	past
<g/>
,	,	kIx,	,
a	a	k8xC	a
zaplatí	zaplatit	k5eAaPmIp3nP	zaplatit
někdy	někdy	k6eAd1	někdy
věřiteli	věřitel	k1gMnPc7	věřitel
násobně	násobně	k6eAd1	násobně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
činí	činit	k5eAaImIp3nS	činit
půjčená	půjčený	k2eAgFnSc1d1	půjčená
částka	částka	k1gFnSc1	částka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
2011	[number]	k4	2011
hasiči	hasič	k1gMnPc1	hasič
evidují	evidovat	k5eAaImIp3nP	evidovat
54	[number]	k4	54
požárů	požár	k1gInPc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
z	z	k7c2	z
vánočních	vánoční	k2eAgInPc2d1	vánoční
požárů	požár	k1gInPc2	požár
pravidelně	pravidelně	k6eAd1	pravidelně
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
svíčky	svíčka	k1gFnPc4	svíčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
Asociace	asociace	k1gFnSc2	asociace
českých	český	k2eAgFnPc2d1	Česká
cestovních	cestovní	k2eAgFnPc2d1	cestovní
kanceláří	kancelář	k1gFnPc2	kancelář
a	a	k8xC	a
agentur	agentura	k1gFnPc2	agentura
<g/>
,	,	kIx,	,
strávit	strávit	k5eAaPmF	strávit
Vánoce	Vánoce	k1gFnPc4	Vánoce
přes	přes	k7c4	přes
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
mělo	mít	k5eAaImAgNnS	mít
odcestovat	odcestovat	k5eAaPmF	odcestovat
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
mělo	mít	k5eAaImAgNnS	mít
strávit	strávit	k5eAaPmF	strávit
Vánoce	Vánoce	k1gFnPc1	Vánoce
2012	[number]	k4	2012
asi	asi	k9	asi
180	[number]	k4	180
tisíc	tisíc	k4xCgInPc2	tisíc
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
čtyřicet	čtyřicet	k4xCc4	čtyřicet
let	léto	k1gNnPc2	léto
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
bílé	bílý	k2eAgFnPc4d1	bílá
Vánoce	Vánoce	k1gFnPc4	Vánoce
devětkrát	devětkrát	k6eAd1	devětkrát
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
Vánoce	Vánoce	k1gFnPc1	Vánoce
na	na	k7c6	na
blátě	bláto	k1gNnSc6	bláto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Christmas	Christmasa	k1gFnPc2	Christmasa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Weihnachten	Weihnachtno	k1gNnPc2	Weihnachtno
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
Vánoční	vánoční	k2eAgInSc1d1	vánoční
stromek	stromek	k1gInSc1	stromek
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
hvězda	hvězda	k1gFnSc1	hvězda
Jesličky	jesličky	k1gFnPc1	jesličky
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
Liturgický	liturgický	k2eAgInSc4d1	liturgický
rok	rok	k1gInSc4	rok
Křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
Vánoce	Vánoce	k1gFnPc1	Vánoce
Yule	Yul	k1gFnPc1	Yul
(	(	kIx(	(
<g/>
germánské	germánský	k2eAgFnPc1d1	germánská
oslavy	oslava	k1gFnPc1	oslava
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
)	)	kIx)	)
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
trhy	trh	k1gInPc7	trh
Kračun	Kračun	k1gNnSc1	Kračun
VAVŘINOVÁ	Vavřinová	k1gFnSc1	Vavřinová
<g/>
,	,	kIx,	,
Valburga	Valburga	k1gFnSc1	Valburga
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
133	[number]	k4	133
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
WINTER	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
starodávných	starodávný	k2eAgFnPc2d1	starodávná
radnic	radnice	k1gFnPc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
asi	asi	k9	asi
1917	[number]	k4	1917
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Wintra	Winter	k1gMnSc2	Winter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
–	–	k?	–
kapitola	kapitola	k1gFnSc1	kapitola
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
v	v	k7c6	v
radnici	radnice	k1gFnSc6	radnice
staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
<g/>
.	.	kIx.	.
</s>
<s>
PAVEL	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
Körber	Körber	k1gMnSc1	Körber
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
ve	v	k7c6	v
dne	den	k1gInSc2	den
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
:	:	kIx,	:
Líčení	líčení	k1gNnSc1	líčení
pražského	pražský	k2eAgInSc2d1	pražský
života	život	k1gInSc2	život
slovem	slovo	k1gNnSc7	slovo
i	i	k8xC	i
obrazem	obraz	k1gInSc7	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
1	[number]	k4	1
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
:	:	kIx,	:
Körber	Körber	k1gInSc1	Körber
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Hoj	hojit	k5eAaImRp2nS	hojit
ty	ty	k3xPp2nSc1	ty
štědrý	štědrý	k2eAgInSc1d1	štědrý
večere	večer	k1gInSc5	večer
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
slavily	slavit	k5eAaImAgFnP	slavit
Vánoce	Vánoce	k1gFnPc1	Vánoce
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
–	–	k?	–
kapitola	kapitola	k1gFnSc1	kapitola
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
ZÍBRT	ZÍBRT	kA	ZÍBRT
<g/>
,	,	kIx,	,
Čeněk	Čeněk	k1gMnSc1	Čeněk
<g/>
.	.	kIx.	.
</s>
<s>
Hoj	hojit	k5eAaImRp2nS	hojit
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc4	ten
štědrý	štědrý	k2eAgInSc1d1	štědrý
večere	večer	k1gInSc5	večer
:	:	kIx,	:
Od	od	k7c2	od
Vánoc	Vánoce	k1gFnPc2	Vánoce
koledou	koleda	k1gFnSc7	koleda
do	do	k7c2	do
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Publikace	publikace	k1gFnSc1	publikace
českého	český	k2eAgMnSc2d1	český
kulturního	kulturní	k2eAgMnSc2d1	kulturní
historika	historik	k1gMnSc2	historik
<g/>
,	,	kIx,	,
folkloristy	folklorista	k1gMnSc2	folklorista
a	a	k8xC	a
etnografa	etnograf	k1gMnSc2	etnograf
zachycující	zachycující	k2eAgInPc1d1	zachycující
obyčeje	obyčej	k1gInPc1	obyčej
<g/>
,	,	kIx,	,
pověry	pověra	k1gFnPc1	pověra
<g/>
,	,	kIx,	,
zábavy	zábava	k1gFnPc1	zábava
a	a	k8xC	a
slavnosti	slavnost	k1gFnPc1	slavnost
českého	český	k2eAgInSc2d1	český
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Vánoc	Vánoce	k1gFnPc2	Vánoce
od	od	k7c2	od
Štědrého	štědrý	k2eAgInSc2d1	štědrý
dne	den	k1gInSc2	den
do	do	k7c2	do
svátku	svátek	k1gInSc2	svátek
Mláďátek	mláďátko	k1gNnPc2	mláďátko
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vánoce	Vánoce	k1gFnPc1	Vánoce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Vánoce	Vánoce	k1gFnPc1	Vánoce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Vánoce	Vánoce	k1gFnPc1	Vánoce
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
Vánoce	Vánoce	k1gFnPc1	Vánoce
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Tematické	tematický	k2eAgInPc1d1	tematický
texty	text	k1gInPc1	text
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
(	(	kIx(	(
<g/>
pastorace	pastorace	k1gFnSc1	pastorace
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
L.	L.	kA	L.
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
:	:	kIx,	:
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
doba	doba	k1gFnSc1	doba
Jak	jak	k6eAd1	jak
popřát	popřát	k5eAaPmF	popřát
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
jiných	jiný	k2eAgInPc2d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
Jiří	Jiří	k1gMnSc1	Jiří
Grygar	Grygar	k1gMnSc1	Grygar
<g/>
:	:	kIx,	:
Astronomie	astronomie	k1gFnPc1	astronomie
a	a	k8xC	a
data	datum	k1gNnPc1	datum
biblických	biblický	k2eAgFnPc2d1	biblická
událostí	událost	k1gFnPc2	událost
Vánoce	Vánoce	k1gFnPc1	Vánoce
–	–	k?	–
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
Čeněk	Čeněk	k1gMnSc1	Čeněk
Zíbrt	Zíbrta	k1gFnPc2	Zíbrta
Den	den	k1gInSc4	den
se	s	k7c7	s
krátí	krátit	k5eAaImIp3nS	krátit
<g/>
,	,	kIx,	,
noc	noc	k1gFnSc1	noc
se	se	k3xPyFc4	se
dlouží	dloužit	k5eAaImIp3nS	dloužit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Unwrapping	Unwrapping	k1gInSc1	Unwrapping
of	of	k?	of
Christmas	Christmas	k1gInSc1	Christmas
<g/>
:	:	kIx,	:
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Mystery	Myster	k1gInPc7	Myster
<g/>
,	,	kIx,	,
Myths	Myths	k1gInSc1	Myths
&	&	k?	&
Traditions	Traditions	k1gInSc1	Traditions
<g/>
,	,	kIx,	,
Jeremiah	Jeremiah	k1gInSc1	Jeremiah
Productions	Productions	k1gInSc1	Productions
–	–	k?	–
o	o	k7c6	o
původu	původ	k1gInSc6	původ
a	a	k8xC	a
historii	historie	k1gFnSc6	historie
Vánoc	Vánoce	k1gFnPc2	Vánoce
–	–	k?	–
neplatný	platný	k2eNgInSc1d1	neplatný
odkaz	odkaz	k1gInSc1	odkaz
!	!	kIx.	!
</s>
