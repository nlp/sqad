<s>
Symptom	symptom	k1gInSc1	symptom
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
σ	σ	k?	σ
–	–	k?	–
'	'	kIx"	'
<g/>
shoda	shoda	k1gFnSc1	shoda
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
příznak	příznak	k1gInSc4	příznak
<g/>
,	,	kIx,	,
vnější	vnější	k2eAgInSc4d1	vnější
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
průvodní	průvodní	k2eAgInSc4d1	průvodní
jev	jev	k1gInSc4	jev
nějakého	nějaký	k3yIgInSc2	nějaký
jinak	jinak	k6eAd1	jinak
obtížně	obtížně	k6eAd1	obtížně
pozorovatelného	pozorovatelný	k2eAgInSc2d1	pozorovatelný
děje	děj	k1gInSc2	děj
<g/>
,	,	kIx,	,
stavu	stav	k1gInSc2	stav
nebo	nebo	k8xC	nebo
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tedy	tedy	k9	tedy
rozpoznání	rozpoznání	k1gNnSc4	rozpoznání
čili	čili	k8xC	čili
diagnózu	diagnóza	k1gFnSc4	diagnóza
například	například	k6eAd1	například
určité	určitý	k2eAgFnSc2d1	určitá
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
její	její	k3xOp3gFnSc7	její
příčinou	příčina	k1gFnSc7	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
teplota	teplota	k1gFnSc1	teplota
symptomem	symptom	k1gInSc7	symptom
zánětlivého	zánětlivý	k2eAgNnSc2d1	zánětlivé
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
zorničky	zornička	k1gFnSc2	zornička
symptomem	symptom	k1gInSc7	symptom
vzrušení	vzrušení	k1gNnSc1	vzrušení
<g/>
,	,	kIx,	,
kouř	kouř	k1gInSc1	kouř
symptomem	symptom	k1gInSc7	symptom
ohně	oheň	k1gInPc4	oheň
<g/>
,	,	kIx,	,
trhliny	trhlina	k1gFnPc4	trhlina
v	v	k7c6	v
omítce	omítka	k1gFnSc6	omítka
symptomem	symptom	k1gInSc7	symptom
narušené	narušený	k2eAgFnPc4d1	narušená
statiky	statika	k1gFnPc4	statika
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
také	také	k9	také
běžné	běžný	k2eAgNnSc4d1	běžné
označení	označení	k1gNnSc4	označení
symptomatický	symptomatický	k2eAgMnSc1d1	symptomatický
–	–	k?	–
příznačný	příznačný	k2eAgInSc1d1	příznačný
<g/>
.	.	kIx.	.
</s>
<s>
Symptomatická	symptomatický	k2eAgFnSc1d1	symptomatická
léčba	léčba	k1gFnSc1	léčba
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
léčbu	léčba	k1gFnSc4	léčba
příznaků	příznak	k1gInPc2	příznak
<g/>
,	,	kIx,	,
ne	ne	k9	ne
(	(	kIx(	(
<g/>
nutně	nutně	k6eAd1	nutně
<g/>
)	)	kIx)	)
příčin	příčina	k1gFnPc2	příčina
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Symptomy	symptom	k1gInPc1	symptom
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
symptomy	symptom	k1gInPc4	symptom
subjektivní	subjektivní	k2eAgNnSc1d1	subjektivní
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
zpozorovat	zpozorovat	k5eAaPmF	zpozorovat
jen	jen	k9	jen
pacient	pacient	k1gMnSc1	pacient
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
a	a	k8xC	a
symptomy	symptom	k1gInPc1	symptom
vnější	vnější	k2eAgInPc1d1	vnější
<g/>
,	,	kIx,	,
intersubjektivní	intersubjektivní	k2eAgInPc1d1	intersubjektivní
<g/>
.	.	kIx.	.
</s>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
vnějších	vnější	k2eAgInPc2d1	vnější
symptomů	symptom	k1gInPc2	symptom
tvoří	tvořit	k5eAaImIp3nS	tvořit
klinický	klinický	k2eAgInSc1d1	klinický
obraz	obraz	k1gInSc1	obraz
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Symptomy	symptom	k1gInPc1	symptom
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
specifické	specifický	k2eAgInPc4d1	specifický
(	(	kIx(	(
<g/>
svědčící	svědčící	k2eAgInSc1d1	svědčící
pro	pro	k7c4	pro
určitou	určitý	k2eAgFnSc4d1	určitá
nemoc	nemoc	k1gFnSc4	nemoc
nebo	nebo	k8xC	nebo
skupinu	skupina	k1gFnSc4	skupina
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
)	)	kIx)	)
a	a	k8xC	a
nespecifické	specifický	k2eNgNnSc1d1	nespecifické
(	(	kIx(	(
<g/>
nesvědčící	svědčící	k2eNgMnSc1d1	svědčící
pro	pro	k7c4	pro
určitou	určitý	k2eAgFnSc4d1	určitá
nemoc	nemoc	k1gFnSc4	nemoc
nebo	nebo	k8xC	nebo
skupinu	skupina	k1gFnSc4	skupina
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
specifické	specifický	k2eAgInPc1d1	specifický
symptomy	symptom	k1gInPc1	symptom
už	už	k6eAd1	už
samy	sám	k3xTgInPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
stačí	stačit	k5eAaBmIp3nS	stačit
pro	pro	k7c4	pro
diagnózu	diagnóza	k1gFnSc4	diagnóza
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
hodnotit	hodnotit	k5eAaImF	hodnotit
celý	celý	k2eAgInSc4d1	celý
soubor	soubor	k1gInSc4	soubor
symptomů	symptom	k1gInPc2	symptom
a	a	k8xC	a
hledat	hledat	k5eAaImF	hledat
ještě	ještě	k6eAd1	ještě
další	další	k2eAgNnSc4d1	další
potvrzení	potvrzení	k1gNnSc4	potvrzení
např.	např.	kA	např.
laboratorními	laboratorní	k2eAgInPc7d1	laboratorní
testy	test	k1gInPc7	test
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
laické	laický	k2eAgNnSc4d1	laické
hodnocení	hodnocení	k1gNnSc4	hodnocení
symptomů	symptom	k1gInPc2	symptom
často	často	k6eAd1	často
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
hypochondrii	hypochondrie	k1gFnSc4	hypochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
symptomů	symptom	k1gInPc2	symptom
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplex	komplex	k1gInSc4	komplex
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
známa	znám	k2eAgFnSc1d1	známa
skutečná	skutečný	k2eAgFnSc1d1	skutečná
příčina	příčina	k1gFnSc1	příčina
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
syndrom	syndrom	k1gInSc1	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
v	v	k7c6	v
psychologii	psychologie	k1gFnSc6	psychologie
i	i	k8xC	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Prodrom	prodrom	k1gInSc1	prodrom
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
prodromos	prodromos	k1gInSc1	prodromos
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
počáteční	počáteční	k2eAgMnSc1d1	počáteční
<g/>
,	,	kIx,	,
nespecifický	specifický	k2eNgInSc1d1	nespecifický
příznak	příznak	k1gInSc1	příznak
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Filosof	filosof	k1gMnSc1	filosof
Ernst	Ernst	k1gMnSc1	Ernst
Cassirer	Cassirer	k1gMnSc1	Cassirer
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
symptomy	symptom	k1gInPc7	symptom
jako	jako	k8xS	jako
nechtěnými	chtěný	k2eNgInPc7d1	nechtěný
průvodními	průvodní	k2eAgInPc7d1	průvodní
jevy	jev	k1gInPc7	jev
(	(	kIx(	(
<g/>
kouř	kouř	k1gInSc1	kouř
–	–	k?	–
oheň	oheň	k1gInSc1	oheň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
signály	signál	k1gInPc7	signál
jako	jako	k8xS	jako
záměrnými	záměrný	k2eAgInPc7d1	záměrný
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
samy	sám	k3xTgInPc1	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
žádný	žádný	k3yNgInSc4	žádný
význam	význam	k1gInSc4	význam
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
a	a	k8xC	a
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
kromě	kromě	k7c2	kromě
svého	svůj	k3xOyFgNnSc2	svůj
bezprostředního	bezprostřední	k2eAgNnSc2d1	bezprostřední
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc4d1	další
významy	význam	k1gInPc4	význam
<g/>
.	.	kIx.	.
</s>
