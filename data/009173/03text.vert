<p>
<s>
Hry	hra	k1gFnPc1	hra
o	o	k7c4	o
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
H.	H.	kA	H.
236	[number]	k4	236
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
opera	opera	k1gFnSc1	opera
skladatele	skladatel	k1gMnSc2	skladatel
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Martinů	Martinů	k1gMnSc2	Martinů
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Martinů	Martinů	k1gMnPc1	Martinů
<g/>
,	,	kIx,	,
prodlužující	prodlužující	k2eAgMnPc1d1	prodlužující
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
od	od	k7c2	od
francouzské	francouzský	k2eAgFnSc2d1	francouzská
avantgardy	avantgarda	k1gFnSc2	avantgarda
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
komponovat	komponovat	k5eAaImF	komponovat
pro	pro	k7c4	pro
české	český	k2eAgMnPc4d1	český
scény	scéna	k1gFnSc2	scéna
jevištní	jevištní	k2eAgNnPc1d1	jevištní
díla	dílo	k1gNnPc1	dílo
na	na	k7c4	na
národní	národní	k2eAgFnPc4d1	národní
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Špalíčku	špalíček	k1gInSc6	špalíček
začal	začít	k5eAaPmAgInS	začít
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
různé	různý	k2eAgInPc4d1	různý
mariánské	mariánský	k2eAgInPc4d1	mariánský
a	a	k8xC	a
v	v	k7c6	v
širší	široký	k2eAgFnSc6d2	širší
rovině	rovina	k1gFnSc6	rovina
náboženské	náboženský	k2eAgInPc4d1	náboženský
příběhy	příběh	k1gInPc4	příběh
do	do	k7c2	do
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
příznačně	příznačně	k6eAd1	příznačně
nazval	nazvat	k5eAaBmAgInS	nazvat
opera-špalíček	opera-špalíček	k1gInSc1	opera-špalíček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
dočkala	dočkat	k5eAaPmAgFnS	dočkat
velkého	velký	k2eAgInSc2d1	velký
zájmu	zájem	k1gInSc2	zájem
historická	historický	k2eAgFnSc1d1	historická
divadelní	divadelní	k2eAgFnSc1d1	divadelní
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
práce	práce	k1gFnSc2	práce
teoretika	teoretik	k1gMnSc4	teoretik
Gastona	Gaston	k1gMnSc4	Gaston
Batyho	Baty	k1gMnSc4	Baty
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
za	za	k7c4	za
prvotní	prvotní	k2eAgInPc4d1	prvotní
začátky	začátek	k1gInPc4	začátek
evropského	evropský	k2eAgNnSc2d1	Evropské
divadla	divadlo	k1gNnSc2	divadlo
označovaly	označovat	k5eAaImAgFnP	označovat
hry	hra	k1gFnPc1	hra
ze	z	k7c2	z
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
znalcem	znalec	k1gMnSc7	znalec
byl	být	k5eAaImAgMnS	být
i	i	k9	i
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
divadelník	divadelník	k1gMnSc1	divadelník
Jindřich	Jindřich	k1gMnSc1	Jindřich
Honzl	Honzl	k1gMnSc1	Honzl
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
Martinů	Martinů	k1gFnSc7	Martinů
své	svůj	k3xOyFgInPc4	svůj
záměry	záměr	k1gInPc4	záměr
neustále	neustále	k6eAd1	neustále
konzultoval	konzultovat	k5eAaImAgInS	konzultovat
při	při	k7c6	při
komponování	komponování	k1gNnSc6	komponování
v	v	k7c6	v
letech	let	k1gInPc6	let
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
nakonec	nakonec	k6eAd1	nakonec
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čtyři	čtyři	k4xCgInPc4	čtyři
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
literární	literární	k2eAgFnSc2d1	literární
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
také	také	k9	také
jinak	jinak	k6eAd1	jinak
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
kratší	krátký	k2eAgInSc4d2	kratší
prolog	prolog	k1gInSc4	prolog
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
velkou	velký	k2eAgFnSc4d1	velká
hru	hra	k1gFnSc4	hra
(	(	kIx(	(
<g/>
mirákl	mirákl	k1gInSc4	mirákl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
opery	opera	k1gFnSc2	opera
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
===	===	k?	===
</s>
</p>
<p>
<s>
Panny	Panna	k1gFnSc2	Panna
moudré	moudrý	k2eAgFnSc2d1	moudrá
a	a	k8xC	a
panny	panna	k1gFnSc2	panna
pošetilé	pošetilý	k2eAgInPc1d1	pošetilý
Liturgickou	liturgický	k2eAgFnSc4d1	liturgická
hru	hra	k1gFnSc4	hra
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přeložil	přeložit	k5eAaPmAgMnS	přeložit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spíš	spíš	k9	spíš
na	na	k7c4	na
její	její	k3xOp3gInPc4	její
náměty	námět	k1gInPc4	námět
nově	nově	k6eAd1	nově
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
deseti	deset	k4xCc6	deset
pannách	panna	k1gFnPc6	panna
(	(	kIx(	(
<g/>
Mt	Mt	k1gFnSc1	Mt
25,1	[number]	k4	25,1
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
očekávají	očekávat	k5eAaImIp3nP	očekávat
příchod	příchod	k1gInSc4	příchod
Pána	pán	k1gMnSc2	pán
<g/>
.	.	kIx.	.
</s>
<s>
Pošetilé	pošetilý	k2eAgFnPc1d1	pošetilá
ale	ale	k8xC	ale
vezmou	vzít	k5eAaPmIp3nP	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
málo	málo	k6eAd1	málo
oleje	olej	k1gInPc1	olej
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
lamp	lampa	k1gFnPc2	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Pán	pán	k1gMnSc1	pán
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
lampy	lampa	k1gFnPc1	lampa
nehoří	hořet	k5eNaImIp3nP	hořet
<g/>
.	.	kIx.	.
</s>
<s>
Pošetilé	pošetilý	k2eAgFnPc1d1	pošetilá
panny	panna	k1gFnPc1	panna
shánějí	shánět	k5eAaImIp3nP	shánět
olej	olej	k1gInSc4	olej
a	a	k8xC	a
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
ty	ten	k3xDgInPc4	ten
prozíravé	prozíravý	k2eAgInPc4d1	prozíravý
i	i	k8xC	i
s	s	k7c7	s
hořícími	hořící	k2eAgFnPc7d1	hořící
lampami	lampa	k1gFnPc7	lampa
vejdou	vejít	k5eAaPmIp3nP	vejít
s	s	k7c7	s
Pánem	pán	k1gMnSc7	pán
na	na	k7c4	na
hostinu	hostina	k1gFnSc4	hostina
a	a	k8xC	a
dveře	dveře	k1gFnPc4	dveře
se	se	k3xPyFc4	se
zavřou	zavřít	k5eAaPmIp3nP	zavřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mariken	Mariken	k1gInSc1	Mariken
z	z	k7c2	z
Nimégue	Nimégu	k1gFnSc2	Nimégu
Vlámský	vlámský	k2eAgInSc4d1	vlámský
námět	námět	k1gInSc4	námět
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
Martinů	Martinů	k2eAgMnSc1d1	Martinů
původně	původně	k6eAd1	původně
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgInSc2d1	francouzský
překladu	překlad	k1gInSc2	překlad
belgického	belgický	k2eAgMnSc2d1	belgický
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
dramatika	dramatik	k1gMnSc2	dramatik
Henriho	Henri	k1gMnSc4	Henri
Ghéona	Ghéon	k1gMnSc4	Ghéon
<g/>
,	,	kIx,	,
až	až	k9	až
později	pozdě	k6eAd2	pozdě
dostal	dostat	k5eAaPmAgMnS	dostat
překlad	překlad	k1gInSc4	překlad
Viléma	Vilém	k1gMnSc2	Vilém
Závady	závada	k1gFnSc2	závada
a	a	k8xC	a
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
hotovou	hotová	k1gFnSc4	hotová
partituru	partitura	k1gFnSc4	partitura
upravil	upravit	k5eAaPmAgMnS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Krásná	krásný	k2eAgFnSc1d1	krásná
Mariken	Mariken	k1gInSc1	Mariken
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
svést	svést	k5eAaPmF	svést
ďáblem	ďábel	k1gMnSc7	ďábel
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
městečku	městečko	k1gNnSc6	městečko
ale	ale	k8xC	ale
vidí	vidět	k5eAaImIp3nS	vidět
hru	hra	k1gFnSc4	hra
o	o	k7c6	o
Maškaronovi	Maškaron	k1gMnSc6	Maškaron
a	a	k8xC	a
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
se	se	k3xPyFc4	se
na	na	k7c4	na
pokání	pokání	k1gNnSc4	pokání
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
duše	duše	k1gFnSc1	duše
vykoupena	vykoupit	k5eAaPmNgFnS	vykoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
===	===	k?	===
</s>
</p>
<p>
<s>
Narození	narození	k1gNnPc4	narození
Páně	páně	k2eAgFnSc1d1	páně
Montáž	montáž	k1gFnSc1	montáž
z	z	k7c2	z
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
lidových	lidový	k2eAgInPc2d1	lidový
textů	text	k1gInPc2	text
řady	řada	k1gFnSc2	řada
vánočních	vánoční	k2eAgFnPc2d1	vánoční
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sestra	sestra	k1gFnSc1	sestra
Paskalina	Paskalin	k2eAgFnSc1d1	Paskalina
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
Martinů	Martinů	k2eAgFnSc4d1	Martinů
legendu	legenda	k1gFnSc4	legenda
o	o	k7c6	o
sestře	sestra	k1gFnSc6	sestra
Paskalině	Paskalin	k2eAgFnSc6d1	Paskalina
<g/>
,	,	kIx,	,
přeloženou	přeložený	k2eAgFnSc4d1	přeložená
Juliem	Julius	k1gMnSc7	Julius
Zeyerem	Zeyer	k1gMnSc7	Zeyer
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doplnil	doplnit	k5eAaPmAgMnS	doplnit
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
dalších	další	k2eAgInPc2d1	další
zdrojů	zdroj	k1gInPc2	zdroj
-	-	kIx~	-
o	o	k7c4	o
komentující	komentující	k2eAgInPc4d1	komentující
texty	text	k1gInPc4	text
z	z	k7c2	z
latinské	latinský	k2eAgFnSc2d1	Latinská
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
moravské	moravský	k2eAgFnSc2d1	Moravská
lidové	lidový	k2eAgFnSc2d1	lidová
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Paskalina	Paskalin	k2eAgFnSc1d1	Paskalina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odešla	odejít	k5eAaPmAgFnS	odejít
z	z	k7c2	z
kláštera	klášter	k1gInSc2	klášter
pro	pro	k7c4	pro
světskou	světský	k2eAgFnSc4d1	světská
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
nespáchanou	spáchaný	k2eNgFnSc4d1	spáchaný
vraždu	vražda	k1gFnSc4	vražda
svého	svůj	k3xOyFgMnSc4	svůj
milého	milý	k2eAgMnSc4d1	milý
odsouzena	odsouzet	k5eAaImNgFnS	odsouzet
k	k	k7c3	k
upálení	upálení	k1gNnSc3	upálení
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
zachraňuje	zachraňovat	k5eAaImIp3nS	zachraňovat
milost	milost	k1gFnSc4	milost
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uvedení	uvedení	k1gNnSc4	uvedení
==	==	k?	==
</s>
</p>
<p>
<s>
Martinů	Martinů	k1gMnSc1	Martinů
původně	původně	k6eAd1	původně
plánoval	plánovat	k5eAaImAgMnS	plánovat
věnovat	věnovat	k5eAaImF	věnovat
operu	opera	k1gFnSc4	opera
k	k	k7c3	k
dirigování	dirigování	k1gNnSc4	dirigování
Václavu	Václav	k1gMnSc3	Václav
Talichovi	Talich	k1gMnSc3	Talich
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgMnS	být
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
tamní	tamní	k2eAgFnSc2d1	tamní
přípravy	příprava	k1gFnSc2	příprava
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
mírně	mírně	k6eAd1	mírně
zdržely	zdržet	k5eAaPmAgInP	zdržet
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
Hrám	hra	k1gFnPc3	hra
nečekaně	nečekaně	k6eAd1	nečekaně
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
o	o	k7c6	o
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
vyhledávaného	vyhledávaný	k2eAgMnSc4d1	vyhledávaný
skladatele	skladatel	k1gMnSc4	skladatel
projevili	projevit	k5eAaPmAgMnP	projevit
zájem	zájem	k1gInSc4	zájem
i	i	k9	i
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
premiéry	premiéra	k1gFnPc1	premiéra
se	se	k3xPyFc4	se
chystaly	chystat	k5eAaImAgFnP	chystat
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
scénách	scéna	k1gFnPc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
ale	ale	k8xC	ale
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
nakonec	nakonec	k6eAd1	nakonec
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
opery	opera	k1gFnSc2	opera
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1935	[number]	k4	1935
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
Martinů	Martinů	k1gFnSc2	Martinů
v	v	k7c6	v
brněnském	brněnský	k2eAgNnSc6d1	brněnské
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Waltera	Walter	k1gMnSc2	Walter
a	a	k8xC	a
za	za	k7c2	za
řízení	řízení	k1gNnSc2	řízení
dirigenta	dirigent	k1gMnSc2	dirigent
Antonína	Antonín	k1gMnSc2	Antonín
Balatky	balatka	k1gFnSc2	balatka
se	s	k7c7	s
Zorou	Zora	k1gFnSc7	Zora
Šemberovou	Šemberův	k2eAgFnSc7d1	Šemberova
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
tanečních	taneční	k2eAgFnPc6d1	taneční
rolích	role	k1gFnPc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Výpravu	výprava	k1gFnSc4	výprava
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
za	za	k7c2	za
úzké	úzký	k2eAgFnSc2d1	úzká
skladatelovy	skladatelův	k2eAgFnSc2d1	skladatelova
součinnosti	součinnost	k1gFnSc2	součinnost
František	František	k1gMnSc1	František
Muzika	muzika	k1gFnSc1	muzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Martinů	Martinů	k1gMnSc1	Martinů
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
operu	opera	k1gFnSc4	opera
státní	státní	k2eAgFnSc4d1	státní
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
úspěchu	úspěch	k1gInSc3	úspěch
se	se	k3xPyFc4	se
opera	opera	k1gFnSc1	opera
později	pozdě	k6eAd2	pozdě
hrála	hrát	k5eAaImAgFnS	hrát
i	i	k9	i
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
jiných	jiný	k2eAgFnPc2d1	jiná
scén	scéna	k1gFnPc2	scéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ŠAFRÁNEK	Šafránek	k1gMnSc1	Šafránek
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Martinů	Martinů	k1gMnSc2	Martinů
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Editio	Editio	k6eAd1	Editio
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
59	[number]	k4	59
<g/>
–	–	k?	–
<g/>
63	[number]	k4	63
<g/>
,	,	kIx,	,
187	[number]	k4	187
<g/>
–	–	k?	–
<g/>
224	[number]	k4	224
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MIHULE	mihule	k1gFnSc1	mihule
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Martinů	Martinů	k2eAgInSc1d1	Martinů
–	–	k?	–
osud	osud	k1gInSc1	osud
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
426	[number]	k4	426
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
207	[number]	k4	207
ad	ad	k7c4	ad
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
ROZTOČILOVÁ	Roztočilová	k1gFnSc1	Roztočilová
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
Martinů	Martinů	k1gFnSc1	Martinů
"	"	kIx"	"
<g/>
Hry	hra	k1gFnPc1	hra
o	o	k7c6	o
Marii	Maria	k1gFnSc6	Maria
<g/>
"	"	kIx"	"
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
sbormistra	sbormistr	k1gMnSc2	sbormistr
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1	Janáčkova
akademie	akademie	k1gFnSc1	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Hudební	hudební	k2eAgFnSc1d1	hudební
fakulta	fakulta	k1gFnSc1	fakulta
–	–	k?	–
Katedra	katedra	k1gFnSc1	katedra
kompozice	kompozice	k1gFnSc1	kompozice
<g/>
,	,	kIx,	,
dirigování	dirigování	k1gNnSc1	dirigování
a	a	k8xC	a
operní	operní	k2eAgFnSc1d1	operní
režie	režie	k1gFnSc1	režie
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Josef	Josef	k1gMnSc1	Josef
Pančík	Pančík	k1gMnSc1	Pančík
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOSTOMSKÁ	HOSTOMSKÁ	kA	HOSTOMSKÁ
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
–	–	k?	–
Průvodce	průvodka	k1gFnSc6	průvodka
operní	operní	k2eAgFnSc7d1	operní
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NS	NS	kA	NS
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
1466	[number]	k4	1466
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
637	[number]	k4	637
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
850	[number]	k4	850
<g/>
–	–	k?	–
<g/>
853	[number]	k4	853
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Hry	hra	k1gFnPc1	hra
o	o	k7c6	o
Marii	Maria	k1gFnSc6	Maria
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
děl	dít	k5eAaImAgInS	dít
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Martinů	Martinů	k1gMnSc4	Martinů
</s>
</p>
<p>
<s>
Hry	hra	k1gFnPc1	hra
o	o	k7c6	o
Marii	Maria	k1gFnSc6	Maria
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
