<s>
Kra	kra	k1gFnSc1	kra
(	(	kIx(	(
<g/>
ledovec	ledovec	k1gInSc1	ledovec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
kus	kus	k1gInSc4	kus
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
odlomil	odlomit	k5eAaPmAgInS	odlomit
od	od	k7c2	od
šelfového	šelfový	k2eAgInSc2d1	šelfový
nebo	nebo	k8xC	nebo
pevninského	pevninský	k2eAgInSc2d1	pevninský
ledovce	ledovec	k1gInSc2	ledovec
a	a	k8xC	a
plave	plavat	k5eAaImIp3nS	plavat
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mořského	mořský	k2eAgInSc2d1	mořský
ledu	led	k1gInSc2	led
jsou	být	k5eAaImIp3nP	být
kry	kra	k1gFnPc1	kra
tvořeny	tvořen	k2eAgFnPc1d1	tvořena
sladkovodním	sladkovodní	k2eAgInSc7d1	sladkovodní
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
sladkovodního	sladkovodní	k2eAgInSc2d1	sladkovodní
ledu	led	k1gInSc2	led
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
920	[number]	k4	920
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hustota	hustota	k1gFnSc1	hustota
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1025	[number]	k4	1025
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
rozdíl	rozdíl	k1gInSc1	rozdíl
hustoty	hustota	k1gFnSc2	hustota
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
kra	kra	k1gFnSc1	kra
nadnášena	nadnášen	k2eAgFnSc1d1	nadnášena
jen	jen	k9	jen
relativně	relativně	k6eAd1	relativně
malou	malý	k2eAgFnSc7d1	malá
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
kolem	kolem	k7c2	kolem
90	[number]	k4	90
%	%	kIx~	%
jejího	její	k3xOp3gInSc2	její
objemu	objem	k1gInSc2	objem
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ponořeno	ponořen	k2eAgNnSc1d1	ponořeno
pod	pod	k7c7	pod
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyčnívající	vyčnívající	k2eAgFnSc2d1	vyčnívající
části	část	k1gFnSc2	část
nelze	lze	k6eNd1	lze
určit	určit	k5eAaPmF	určit
tvar	tvar	k1gInSc4	tvar
ponořené	ponořený	k2eAgFnSc2d1	ponořená
části	část	k1gFnSc2	část
ledové	ledový	k2eAgFnSc2d1	ledová
kry	kra	k1gFnSc2	kra
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
velmi	velmi	k6eAd1	velmi
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
pro	pro	k7c4	pro
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odolný	odolný	k2eAgInSc1d1	odolný
led	led	k1gInSc1	led
může	moct	k5eAaImIp3nS	moct
poškodit	poškodit	k5eAaPmF	poškodit
i	i	k9	i
lodní	lodní	k2eAgInSc4d1	lodní
trup	trup	k1gInSc4	trup
konstruovaný	konstruovaný	k2eAgInSc4d1	konstruovaný
z	z	k7c2	z
ocelových	ocelový	k2eAgInPc2d1	ocelový
plátů	plát	k1gInPc2	plát
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámější	známý	k2eAgFnSc7d3	nejznámější
katastrofou	katastrofa	k1gFnSc7	katastrofa
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
srážkou	srážka	k1gFnSc7	srážka
s	s	k7c7	s
ledovou	ledový	k2eAgFnSc7d1	ledová
krou	kra	k1gFnSc7	kra
je	být	k5eAaImIp3nS	být
potopení	potopení	k1gNnSc1	potopení
parníku	parník	k1gInSc2	parník
Titanic	Titanic	k1gInSc1	Titanic
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Přeneseně	přeneseně	k6eAd1	přeneseně
se	se	k3xPyFc4	se
obratu	obrat	k1gInSc6	obrat
špička	špička	k1gFnSc1	špička
ledovce	ledovec	k1gInSc2	ledovec
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnPc1d2	veliký
než	než	k8xS	než
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
.	.	kIx.	.
</s>
<s>
Ledové	ledový	k2eAgFnPc1d1	ledová
kry	kra	k1gFnPc1	kra
jsou	být	k5eAaImIp3nP	být
problémem	problém	k1gInSc7	problém
i	i	k9	i
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
období	období	k1gNnSc6	období
jarního	jarní	k2eAgNnSc2d1	jarní
tání	tání	k1gNnSc2	tání
mohou	moct	k5eAaImIp3nP	moct
poškozovat	poškozovat	k5eAaImF	poškozovat
konstrukce	konstrukce	k1gFnPc4	konstrukce
mostů	most	k1gInPc2	most
nebo	nebo	k8xC	nebo
svým	svůj	k3xOyFgNnSc7	svůj
nahromaděním	nahromadění	k1gNnSc7	nahromadění
přehradit	přehradit	k5eAaPmF	přehradit
řeku	řeka	k1gFnSc4	řeka
a	a	k8xC	a
způsobit	způsobit	k5eAaPmF	způsobit
povodně	povodně	k6eAd1	povodně
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
výška	výška	k1gFnSc1	výška
ledových	ledový	k2eAgFnPc2d1	ledová
ker	kra	k1gFnPc2	kra
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
1	[number]	k4	1
metru	metr	k1gInSc2	metr
nad	nad	k7c7	nad
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
až	až	k9	až
po	po	k7c4	po
75	[number]	k4	75
m	m	kA	m
nad	nad	k7c7	nad
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
Ledové	ledový	k2eAgFnPc1d1	ledová
kry	kra	k1gFnPc1	kra
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
ledem	led	k1gInSc7	led
ze	z	k7c2	z
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
90	[number]	k4	90
%	%	kIx~	%
ledových	ledový	k2eAgFnPc2d1	ledová
ker	kra	k1gFnPc2	kra
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Newfoundlandu	Newfoundland	k1gInSc2	Newfoundland
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
ledovců	ledovec	k1gInPc2	ledovec
západního	západní	k2eAgNnSc2d1	západní
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k6eAd1	až
7	[number]	k4	7
km	km	kA	km
za	za	k7c4	za
rok	rok	k1gInSc4	rok
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
ledových	ledový	k2eAgFnPc2d1	ledová
ker	kra	k1gFnPc2	kra
Led	led	k1gInSc1	led
v	v	k7c6	v
ledových	ledový	k2eAgFnPc6d1	ledová
krách	kra	k1gFnPc6	kra
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
stáří	stáří	k1gNnSc1	stáří
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
ker	kra	k1gFnPc2	kra
činí	činit	k5eAaImIp3nS	činit
100	[number]	k4	100
000	[number]	k4	000
až	až	k9	až
200	[number]	k4	200
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
odpovídajících	odpovídající	k2eAgFnPc2d1	odpovídající
rozměrům	rozměr	k1gInPc3	rozměr
patnácti	patnáct	k4xCc7	patnáct
patrové	patrový	k2eAgFnPc4d1	patrová
budovy	budova	k1gFnSc2	budova
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
rozmezí	rozmezí	k1gNnSc1	rozmezí
−	−	k?	−
až	až	k8xS	až
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
Největší	veliký	k2eAgFnSc1d3	veliký
ledová	ledový	k2eAgFnSc1d1	ledová
kra	kra	k1gFnSc1	kra
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severního	severní	k2eAgInSc2d1	severní
Atlantiku	Atlantik	k1gInSc2	Atlantik
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
měřila	měřit	k5eAaImAgFnS	měřit
168	[number]	k4	168
m	m	kA	m
Jules	Julesa	k1gFnPc2	Julesa
Verne	Vern	k1gInSc5	Vern
<g/>
:	:	kIx,	:
Oceánem	oceán	k1gInSc7	oceán
na	na	k7c6	na
kře	kra	k1gFnSc6	kra
ledové	ledový	k2eAgFnSc2d1	ledová
František	František	k1gMnSc1	František
Běhounek	běhounek	k1gInSc1	běhounek
<g/>
:	:	kIx,	:
Trosečníci	trosečník	k1gMnPc1	trosečník
na	na	k7c6	na
kře	kra	k1gFnSc6	kra
ledové	ledový	k2eAgNnSc1d1	ledové
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trosečníci	trosečník	k1gMnPc1	trosečník
polárního	polární	k2eAgNnSc2d1	polární
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
–	–	k?	–
cestopis	cestopis	k1gInSc1	cestopis
popisující	popisující	k2eAgInSc1d1	popisující
skutečné	skutečný	k2eAgFnSc3d1	skutečná
události	událost	k1gFnSc3	událost
ztroskotání	ztroskotání	k1gNnSc2	ztroskotání
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
Italia	Italius	k1gMnSc2	Italius
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kra	kra	k1gFnSc1	kra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kra	kra	k1gFnSc1	kra
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
