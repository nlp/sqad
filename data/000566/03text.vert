<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
(	(	kIx(	(
<g/>
*	*	kIx~	*
14.	[number]	k4	14.
července	červenec	k1gInSc2	červenec
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
šlágrový	šlágrový	k2eAgMnSc1d1	šlágrový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mnohonásobný	mnohonásobný	k2eAgMnSc1d1	mnohonásobný
držitel	držitel	k1gMnSc1	držitel
ocenění	ocenění	k1gNnSc2	ocenění
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
jich	on	k3xPp3gNnPc2	on
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
i	i	k8xC	i
s	s	k7c7	s
posledním	poslední	k2eAgInSc7d1	poslední
získaným	získaný	k2eAgInSc7d1	získaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
42.	[number]	k4	42.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
nosičů	nosič	k1gInPc2	nosič
desek	deska	k1gFnPc2	deska
Karla	Karel	k1gMnSc4	Karel
Gotta	Gott	k1gMnSc4	Gott
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
nejúspěšnějšího	úspěšný	k2eAgMnSc4d3	nejúspěšnější
českého	český	k2eAgMnSc4d1	český
interpreta	interpret	k1gMnSc4	interpret
dosud	dosud	k6eAd1	dosud
(	(	kIx(	(
<g/>
aktuální	aktuální	k2eAgInSc1d1	aktuální
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
vydal	vydat	k5eAaPmAgInS	vydat
293	[number]	k4	293
sólových	sólový	k2eAgFnPc2d1	sólová
alb	alba	k1gFnPc2	alba
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
i	i	k8xC	i
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Slavíků	slavík	k1gInPc2	slavík
dostal	dostat	k5eAaPmAgMnS	dostat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
televizních	televizní	k2eAgNnPc2d1	televizní
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
8	[number]	k4	8
zlatých	zlatý	k2eAgFnPc2d1	zlatá
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
1	[number]	k4	1
diamantovou	diamantový	k2eAgFnSc4d1	Diamantová
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
opakovaně	opakovaně	k6eAd1	opakovaně
se	se	k3xPyFc4	se
umisťoval	umisťovat	k5eAaImAgMnS	umisťovat
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
příčkách	příčka	k1gFnPc6	příčka
hitparád	hitparáda	k1gFnPc2	hitparáda
a	a	k8xC	a
prodeje	prodej	k1gInSc2	prodej
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
celkem	celkem	k6eAd1	celkem
978	[number]	k4	978
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
významnými	významný	k2eAgMnPc7d1	významný
umělci	umělec	k1gMnPc7	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
14.	[number]	k4	14.
července	červenec	k1gInSc2	červenec
1939	[number]	k4	1939
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
potomek	potomek	k1gMnSc1	potomek
Karla	Karel	k1gMnSc4	Karel
Gotta	Gott	k1gMnSc4	Gott
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1911	[number]	k4	1911
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Valešové	Valešová	k1gFnSc2	Valešová
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1910	[number]	k4	1910
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
malířem	malíř	k1gMnSc7	malíř
a	a	k8xC	a
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
školní	školní	k2eAgFnSc2d1	školní
docházky	docházka	k1gFnSc2	docházka
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
neúspěšně	úspěšně	k6eNd1	úspěšně
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nebyl	být	k5eNaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
(	(	kIx(	(
<g/>
ČKD	ČKD	kA	ČKD
<g/>
)	)	kIx)	)
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
elektromontér	elektromontér	k1gMnSc1	elektromontér
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
učňovských	učňovský	k2eAgNnPc2d1	učňovské
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
své	svůj	k3xOyFgFnPc4	svůj
druhé	druhý	k4xOgFnSc3	druhý
velké	velký	k2eAgFnSc3d1	velká
lásce	láska	k1gFnSc3	láska
–	–	k?	–
hudbě	hudba	k1gFnSc3	hudba
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
,	,	kIx,	,
šel	jít	k5eAaImAgMnS	jít
na	na	k7c4	na
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50.	[number]	k4	50.
letech	léto	k1gNnPc6	léto
příležitostně	příležitostně	k6eAd1	příležitostně
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k9	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
–	–	k?	–
amatér	amatér	k1gMnSc1	amatér
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
různých	různý	k2eAgFnPc2d1	různá
pěveckých	pěvecký	k2eAgFnPc2d1	pěvecká
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
upozornil	upozornit	k5eAaPmAgMnS	upozornit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
doznívala	doznívat	k5eAaImAgFnS	doznívat
éra	éra	k1gFnSc1	éra
swingu	swing	k1gInSc2	swing
a	a	k8xC	a
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
roll	rollum	k1gNnPc2	rollum
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
slyšet	slyšet	k5eAaImF	slyšet
jen	jen	k9	jen
v	v	k7c6	v
klubech	klub	k1gInPc6	klub
(	(	kIx(	(
<g/>
Akord	akord	k1gInSc1	akord
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Reduta	reduta	k1gFnSc1	reduta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
poroty	porota	k1gFnSc2	porota
sice	sice	k8xC	sice
zcela	zcela	k6eAd1	zcela
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dostal	dostat	k5eAaPmAgMnS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
pražských	pražský	k2eAgFnPc6d1	Pražská
tanečních	taneční	k2eAgFnPc6d1	taneční
kavárnách	kavárna	k1gFnPc6	kavárna
s	s	k7c7	s
orchestry	orchestr	k1gInPc7	orchestr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Karel	Karel	k1gMnSc1	Karel
Krautgartner	Krautgartner	k1gMnSc1	Krautgartner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
svého	svůj	k3xOyFgNnSc2	svůj
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
v	v	k7c6	v
ČKD	ČKD	kA	ČKD
začal	začít	k5eAaPmAgInS	začít
poloprofesionálně	poloprofesionálně	k6eAd1	poloprofesionálně
vystupovat	vystupovat	k5eAaImF	vystupovat
jako	jako	k9	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
jeho	on	k3xPp3gNnSc2	on
působení	působení	k1gNnSc2	působení
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
na	na	k7c4	na
soutěž	soutěž	k1gFnSc4	soutěž
amatérských	amatérský	k2eAgMnPc2d1	amatérský
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
častěji	často	k6eAd2	často
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kavárnách	kavárna	k1gFnPc6	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
večerní	večerní	k2eAgNnPc1d1	večerní
vystoupení	vystoupení	k1gNnPc1	vystoupení
se	se	k3xPyFc4	se
dostávala	dostávat	k5eAaImAgFnS	dostávat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
zaměstnáním	zaměstnání	k1gNnSc7	zaměstnání
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
definitivně	definitivně	k6eAd1	definitivně
odejít	odejít	k5eAaPmF	odejít
z	z	k7c2	z
ČKD	ČKD	kA	ČKD
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaImF	věnovat
profesionálně	profesionálně	k6eAd1	profesionálně
zpěvu	zpěv	k1gInSc2	zpěv
(	(	kIx(	(
<g/>
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pak	pak	k6eAd1	pak
míval	mívat	k5eAaImAgInS	mívat
časté	častý	k2eAgInPc4d1	častý
špatné	špatný	k2eAgInPc4d1	špatný
sny	sen	k1gInPc4	sen
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zpívá	zpívat	k5eAaImIp3nS	zpívat
před	před	k7c7	před
sálem	sál	k1gInSc7	sál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sedí	sedit	k5eAaImIp3nS	sedit
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gMnSc1	jeho
mistr	mistr	k1gMnSc1	mistr
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Proč	proč	k6eAd1	proč
jen	jen	k6eAd1	jen
se	se	k3xPyFc4	se
ten	ten	k3xDgMnSc1	ten
chlapec	chlapec	k1gMnSc1	chlapec
raději	rád	k6eAd2	rád
nedržel	držet	k5eNaImAgMnS	držet
řemesla	řemeslo	k1gNnPc4	řemeslo
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
státní	státní	k2eAgFnSc6d1	státní
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
operní	operní	k2eAgInSc1d1	operní
zpěv	zpěv	k1gInSc1	zpěv
u	u	k7c2	u
prof.	prof.	kA	prof.
Konstantina	Konstantin	k1gMnSc4	Konstantin
Karenina	Karenina	k1gFnSc1	Karenina
(	(	kIx(	(
<g/>
žák	žák	k1gMnSc1	žák
ruského	ruský	k2eAgMnSc2d1	ruský
pěvce	pěvec	k1gMnSc2	pěvec
Šaljapina	Šaljapin	k2eAgMnSc2d1	Šaljapin
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
studoval	studovat	k5eAaImAgMnS	studovat
dálkově	dálkově	k6eAd1	dálkově
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
u	u	k7c2	u
publika	publikum	k1gNnSc2	publikum
budil	budit	k5eAaImAgMnS	budit
smíšené	smíšený	k2eAgInPc1d1	smíšený
a	a	k8xC	a
kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
pocity	pocit	k1gInPc1	pocit
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgInPc1d1	způsobený
jeho	jeho	k3xOp3gInSc7	jeho
projevem	projev	k1gInSc7	projev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
vymykal	vymykat	k5eAaImAgMnS	vymykat
zažitým	zažitý	k2eAgInPc3d1	zažitý
trendům	trend	k1gInPc3	trend
ze	z	k7c2	z
začátků	začátek	k1gInPc2	začátek
60.	[number]	k4	60.
a	a	k8xC	a
celých	celý	k2eAgNnPc2d1	celé
50.	[number]	k4	50.
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
éra	éra	k1gFnSc1	éra
twistu	twist	k1gInSc2	twist
v	v	k7c6	v
letech	let	k1gInPc6	let
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
64	[number]	k4	64
přinesla	přinést	k5eAaPmAgFnS	přinést
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
výrazné	výrazný	k2eAgNnSc4d1	výrazné
oživení	oživení	k1gNnSc4	oživení
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
také	také	k9	také
prosazovat	prosazovat	k5eAaImF	prosazovat
divadla	divadlo	k1gNnPc4	divadlo
malých	malý	k2eAgFnPc2d1	malá
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
rodila	rodit	k5eAaImAgFnS	rodit
moderní	moderní	k2eAgFnSc1d1	moderní
česká	český	k2eAgFnSc1d1	Česká
populární	populární	k2eAgFnSc1d1	populární
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jeho	on	k3xPp3gInSc4	on
duet	duet	k1gInSc4	duet
s	s	k7c7	s
Vlastou	Vlasta	k1gFnSc7	Vlasta
Průchovou	Průchová	k1gFnSc4	Průchová
hitem	hit	k1gInSc7	hit
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Milníkem	milník	k1gInSc7	milník
kariéry	kariéra	k1gFnSc2	kariéra
Karla	Karel	k1gMnSc2	Karel
Gotta	Gott	k1gMnSc2	Gott
byla	být	k5eAaImAgFnS	být
nabídka	nabídka	k1gFnSc1	nabídka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
na	na	k7c4	na
angažmá	angažmá	k1gNnSc4	angažmá
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
od	od	k7c2	od
Jiřího	Jiří	k1gMnSc2	Jiří
Suchého	Suchý	k1gMnSc2	Suchý
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc2	Jiří
Šlitra	Šlitr	k1gMnSc2	Šlitr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dvojice	dvojice	k1gFnSc1	dvojice
mu	on	k3xPp3gMnSc3	on
napsala	napsat	k5eAaBmAgFnS	napsat
píseň	píseň	k1gFnSc4	píseň
Oči	oko	k1gNnPc4	oko
sněhem	sníh	k1gInSc7	sníh
zaváté	zavátý	k2eAgFnSc2d1	zavátá
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
československých	československý	k2eAgFnPc2d1	Československá
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
hitem	hit	k1gInSc7	hit
roku	rok	k1gInSc2	rok
a	a	k8xC	a
píseň	píseň	k1gFnSc4	píseň
Karlu	Karel	k1gMnSc3	Karel
Gottovi	Gott	k1gMnSc3	Gott
dopomohla	dopomoct	k5eAaPmAgFnS	dopomoct
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
prvního	první	k4xOgInSc2	první
Zlatého	zlatý	k1gInSc2	zlatý
slavíka	slavík	k1gInSc2	slavík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Růst	růst	k1gInSc1	růst
popularity	popularita	k1gFnSc2	popularita
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
ze	z	k7c2	z
Semaforu	Semafor	k1gInSc2	Semafor
založil	založit	k5eAaPmAgMnS	založit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
novými	nový	k2eAgMnPc7d1	nový
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
<g/>
,	,	kIx,	,
bratry	bratr	k1gMnPc7	bratr
Štaidlovými	Štaidlový	k2eAgFnPc7d1	Štaidlová
(	(	kIx(	(
<g/>
textařem	textař	k1gMnSc7	textař
a	a	k8xC	a
skladatelem	skladatel	k1gMnSc7	skladatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc4d1	vlastní
divadelní	divadelní	k2eAgFnSc4d1	divadelní
scénu	scéna	k1gFnSc4	scéna
Apollo	Apollo	k1gMnSc1	Apollo
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hvězdou	hvězda	k1gFnSc7	hvězda
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
divadlo	divadlo	k1gNnSc1	divadlo
rozpuštěno	rozpustit	k5eAaPmNgNnS	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ho	on	k3xPp3gInSc4	on
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
koncertech	koncert	k1gInPc6	koncert
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
nově	nově	k6eAd1	nově
založená	založený	k2eAgFnSc1d1	založená
Skupina	skupina	k1gFnSc1	skupina
Ladislava	Ladislav	k1gMnSc2	Ladislav
Štaidla	Štaidlo	k1gNnSc2	Štaidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
také	také	k9	také
vydal	vydat	k5eAaPmAgMnS	vydat
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollovým	rollův	k2eAgInSc7d1	rollův
hitem	hit	k1gInSc7	hit
–	–	k?	–
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
píseň	píseň	k1gFnSc4	píseň
Trezor	trezor	k1gInSc1	trezor
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgNnPc4d1	známé
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
lyra	lyra	k1gFnSc1	lyra
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
zlatou	zlatý	k2eAgFnSc4d1	zlatá
Bratislavskou	bratislavský	k2eAgFnSc4d1	Bratislavská
lyru	lyra	k1gFnSc4	lyra
za	za	k7c4	za
píseň	píseň	k1gFnSc4	píseň
Mám	mít	k5eAaImIp1nS	mít
rozprávkový	rozprávkový	k2eAgMnSc1d1	rozprávkový
dom	dom	k?	dom
(	(	kIx(	(
<g/>
Vieroslav	Vieroslav	k1gMnSc1	Vieroslav
Matušík	Matušík	k1gMnSc1	Matušík
<g/>
/	/	kIx~	/
<g/>
Eliška	Eliška	k1gFnSc1	Eliška
Jelínková	Jelínková	k1gFnSc1	Jelínková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
se	s	k7c7	s
skladatelem	skladatel	k1gMnSc7	skladatel
Karlem	Karel	k1gMnSc7	Karel
Svobodou	Svoboda	k1gMnSc7	Svoboda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
napsal	napsat	k5eAaPmAgInS	napsat
několik	několik	k4yIc4	několik
výrazných	výrazný	k2eAgFnPc2d1	výrazná
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
bratry	bratr	k1gMnPc7	bratr
Štaidlovými	Štaidlový	k2eAgFnPc7d1	Štaidlová
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
odletěli	odletět	k5eAaPmAgMnP	odletět
na	na	k7c4	na
7	[number]	k4	7
měsíců	měsíc	k1gInPc2	měsíc
trvající	trvající	k2eAgNnSc4d1	trvající
angažmá	angažmá	k1gNnSc4	angažmá
do	do	k7c2	do
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc3	jeho
největšímu	veliký	k2eAgInSc3d3	veliký
"	"	kIx"	"
<g/>
rivalovi	rival	k1gMnSc3	rival
<g/>
"	"	kIx"	"
Waldemarovi	Waldemar	k1gMnSc3	Waldemar
Matuškovi	Matuška	k1gMnSc3	Matuška
podařilo	podařit	k5eAaPmAgNnS	podařit
opět	opět	k6eAd1	opět
zvítězit	zvítězit	k5eAaPmF	zvítězit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
Gott	Gott	k1gInSc1	Gott
vrátil	vrátit	k5eAaPmAgInS	vrátit
jako	jako	k9	jako
sebevědomý	sebevědomý	k2eAgMnSc1d1	sebevědomý
profesionál	profesionál	k1gMnSc1	profesionál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěších	úspěch	k1gInPc6	úspěch
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc6d1	východní
i	i	k8xC	i
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jeho	jeho	k3xOp3gMnSc1	jeho
první	první	k4xOgMnSc1	první
(	(	kIx(	(
<g/>
a	a	k8xC	a
hned	hned	k6eAd1	hned
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
<g/>
)	)	kIx)	)
album	album	k1gNnSc4	album
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
začal	začít	k5eAaPmAgMnS	začít
zdobit	zdobit	k5eAaImF	zdobit
ozdobný	ozdobný	k2eAgInSc4d1	ozdobný
přívlastek	přívlastek	k1gInSc4	přívlastek
"	"	kIx"	"
<g/>
Zlatý	zlatý	k2eAgInSc4d1	zlatý
hlas	hlas	k1gInSc4	hlas
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
práce	práce	k1gFnSc2	práce
také	také	k9	také
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
Rakousko	Rakousko	k1gNnSc4	Rakousko
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
prestižní	prestižní	k2eAgFnSc6d1	prestižní
hudební	hudební	k2eAgFnSc6d1	hudební
soutěži	soutěž	k1gFnSc6	soutěž
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gInSc1	Contest
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Tausend	Tausend	k1gMnSc1	Tausend
Fenster	Fenster	k1gMnSc1	Fenster
<g/>
.	.	kIx.	.
</s>
<s>
Skončil	skončit	k5eAaPmAgMnS	skončit
třináctý	třináctý	k4xOgInSc1	třináctý
a	a	k8xC	a
díky	díky	k7c3	díky
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
soutěží	soutěžit	k5eAaImIp3nS	soutěžit
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
oblíbeným	oblíbený	k2eAgInPc3d1	oblíbený
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
nastartovala	nastartovat	k5eAaPmAgFnS	nastartovat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc7	jeho
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
mu	on	k3xPp3gMnSc3	on
složil	složit	k5eAaPmAgMnS	složit
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
jeho	jeho	k3xOp3gFnSc4	jeho
nejúspěšnější	úspěšný	k2eAgFnSc4d3	nejúspěšnější
píseň	píseň	k1gFnSc4	píseň
Lady	lady	k1gFnSc1	lady
Carneval	Carneval	k1gFnSc1	Carneval
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
třinácti	třináct	k4xCc2	třináct
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
získal	získat	k5eAaPmAgInS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
Bratislavskou	bratislavský	k2eAgFnSc4d1	Bratislavská
lyru	lyra	k1gFnSc4	lyra
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Hej	hej	k6eAd1	hej
<g/>
,	,	kIx,	,
páni	pan	k1gMnPc1	pan
konšelé	konšel	k1gMnPc1	konšel
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
následně	následně	k6eAd1	následně
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
slovy	slovo	k1gNnPc7	slovo
narážejícími	narážející	k2eAgFnPc7d1	narážející
na	na	k7c4	na
text	text	k1gInSc4	text
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
ráj	ráj	k1gInSc4	ráj
<g/>
,	,	kIx,	,
žít	žít	k5eAaImF	žít
jako	jako	k8xS	jako
v	v	k7c6	v
kapitalizmu	kapitalizmus	k1gInSc6	kapitalizmus
a	a	k8xC	a
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
v	v	k7c6	v
socializmu	socializmus	k1gInSc6	socializmus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
rozměru	rozměr	k1gInSc2	rozměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
70.	[number]	k4	70.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
s	s	k7c7	s
1972	[number]	k4	1972
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
dvě	dva	k4xCgNnPc4	dva
velká	velký	k2eAgNnPc4d1	velké
turné	turné	k1gNnPc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
s	s	k7c7	s
názvem	název	k1gInSc7	název
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
'	'	kIx"	'
<g/>
70	[number]	k4	70
čítalo	čítat	k5eAaImAgNnS	čítat
22	[number]	k4	22
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
druhé	druhý	k4xOgFnPc1	druhý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Evropské	evropský	k2eAgNnSc1d1	Evropské
turné	turné	k1gNnSc1	turné
mělo	mít	k5eAaImAgNnS	mít
64	[number]	k4	64
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Ono	onen	k3xDgNnSc1	onen
turné	turné	k1gNnSc1	turné
bylo	být	k5eAaImAgNnS	být
zakončeno	zakončit	k5eAaPmNgNnS	zakončit
koncertem	koncert	k1gInSc7	koncert
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
dále	daleko	k6eAd2	daleko
sbíral	sbírat	k5eAaImAgInS	sbírat
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
vyprodával	vyprodávat	k5eAaImAgMnS	vyprodávat
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
pravidelně	pravidelně	k6eAd1	pravidelně
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
získal	získat	k5eAaPmAgMnS	získat
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
Supraphonu	supraphon	k1gInSc2	supraphon
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
alba	album	k1gNnSc2	album
My	my	k3xPp1nPc1	my
Czech	Czech	k1gInSc1	Czech
Favourites	Favourites	k1gInSc1	Favourites
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Supraphon	supraphon	k1gInSc1	supraphon
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gNnSc7	jeho
domácím	domácí	k2eAgNnSc7d1	domácí
hudebním	hudební	k2eAgNnSc7d1	hudební
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
mu	on	k3xPp3gMnSc3	on
alba	album	k1gNnPc4	album
vydával	vydávat	k5eAaPmAgInS	vydávat
Polydor	Polydor	k1gInSc1	Polydor
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
se	se	k3xPyFc4	se
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
trendům	trend	k1gInPc3	trend
a	a	k8xC	a
v	v	k7c6	v
70.	[number]	k4	70.
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgInS	nechat
narůst	narůst	k5eAaPmF	narůst
delší	dlouhý	k2eAgInPc4d2	delší
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
kotlety	kotleta	k1gFnPc4	kotleta
<g/>
,	,	kIx,	,
typická	typický	k2eAgFnSc1d1	typická
také	také	k9	také
byla	být	k5eAaImAgFnS	být
saka	sako	k1gNnSc2	sako
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
límci	límec	k1gInPc7	límec
či	či	k8xC	či
vysoko	vysoko	k6eAd1	vysoko
kalhoty	kalhoty	k1gFnPc4	kalhoty
s	s	k7c7	s
širokým	široký	k2eAgNnSc7d1	široké
zakončením	zakončení	k1gNnSc7	zakončení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
hudebních	hudební	k2eAgInPc2d1	hudební
pořadů	pořad	k1gInPc2	pořad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgInS	hrát
a	a	k8xC	a
zpíval	zpívat	k5eAaImAgInS	zpívat
–	–	k?	–
např.	např.	kA	např.
Hvězda	Hvězda	k1gMnSc1	Hvězda
padá	padat	k5eAaImIp3nS	padat
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
Išel	Išel	k1gMnSc1	Išel
macek	macek	k1gMnSc1	macek
<g/>
,	,	kIx,	,
Chléb	chléb	k1gInSc1	chléb
a	a	k8xC	a
sůl	sůl	k1gFnSc1	sůl
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Karla	Karel	k1gMnSc4	Karel
Gotta	Gott	k1gMnSc4	Gott
také	také	k9	také
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
doprovázelo	doprovázet	k5eAaImAgNnS	doprovázet
vokální	vokální	k2eAgNnSc1d1	vokální
trio	trio	k1gNnSc1	trio
–	–	k?	–
Jitka	Jitka	k1gFnSc1	Jitka
Zelenková	Zelenková	k1gFnSc1	Zelenková
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Kahovcová	Kahovcový	k2eAgFnSc1d1	Kahovcová
a	a	k8xC	a
Jarmila	Jarmila	k1gFnSc1	Jarmila
Šulerová	Šulerová	k1gFnSc1	Šulerová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
a	a	k8xC	a
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Lennonem	Lennon	k1gInSc7	Lennon
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
plánovali	plánovat	k5eAaImAgMnP	plánovat
společný	společný	k2eAgInSc4d1	společný
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
–	–	k?	–
Lennon	Lennon	k1gInSc1	Lennon
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
prý	prý	k9	prý
propagandou	propaganda	k1gFnSc7	propaganda
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgMnS	natáčet
pořad	pořad	k1gInSc4	pořad
Zpívá	zpívat	k5eAaImIp3nS	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
byly	být	k5eAaImAgInP	být
zaznamenávány	zaznamenávat	k5eAaImNgInP	zaznamenávat
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
koncerty	koncert	k1gInPc1	koncert
Karla	Karel	k1gMnSc2	Karel
Gotta	Gott	k1gMnSc2	Gott
ve	v	k7c6	v
Slaném	Slaný	k1gInSc6	Slaný
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pořadu	pořad	k1gInSc2	pořad
si	se	k3xPyFc3	se
zval	zvát	k5eAaImAgMnS	zvát
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
různé	různý	k2eAgFnPc4d1	různá
hosty	host	k1gMnPc7	host
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
zpívali	zpívat	k5eAaImAgMnP	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc1	pořad
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1973	[number]	k4	1973
–	–	k?	–
1978	[number]	k4	1978
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
To	to	k9	to
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
z	z	k7c2	z
Zpívá	zpívat	k5eAaImIp3nS	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70.	[number]	k4	70.
letech	léto	k1gNnPc6	léto
slavil	slavit	k5eAaImAgMnS	slavit
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
velké	velký	k2eAgInPc4d1	velký
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
rok	rok	k1gInSc4	rok
vydával	vydávat	k5eAaPmAgInS	vydávat
minimálně	minimálně	k6eAd1	minimálně
jednu	jeden	k4xCgFnSc4	jeden
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c4	mnoho
hitů	hit	k1gInPc2	hit
–	–	k?	–
Kávu	káva	k1gFnSc4	káva
si	se	k3xPyFc3	se
osladím	osladit	k5eAaPmIp1nS	osladit
<g/>
,	,	kIx,	,
Kdepak	kdepak	k9	kdepak
ty	ten	k3xDgFnPc1	ten
ptáčku	ptáček	k1gInSc2	ptáček
hnízdo	hnízdo	k1gNnSc1	hnízdo
máš	mít	k5eAaImIp2nS	mít
<g/>
,	,	kIx,	,
Je	být	k5eAaImIp3nS	být
jaká	jaký	k3yQgFnSc1	jaký
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
Cítím	cítit	k5eAaImIp1nS	cítit
<g/>
,	,	kIx,	,
Zloděj	zloděj	k1gMnSc1	zloděj
dobytka	dobytek	k1gInSc2	dobytek
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
začal	začít	k5eAaPmAgMnS	začít
tradici	tradice	k1gFnSc4	tradice
svých	svůj	k3xOyFgMnPc2	svůj
Vánočních	vánoční	k2eAgMnPc2d1	vánoční
koncertů	koncert	k1gInPc2	koncert
–	–	k?	–
ty	ten	k3xDgMnPc4	ten
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
každé	každý	k3xTgInPc1	každý
dva	dva	k4xCgInPc1	dva
roky	rok	k1gInPc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
píseň	píseň	k1gFnSc4	píseň
Kam	kam	k6eAd1	kam
tenkrát	tenkrát	k6eAd1	tenkrát
šel	jít	k5eAaImAgMnS	jít
můj	můj	k1gMnSc1	můj
bratra	bratr	k1gMnSc2	bratr
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
cover	cover	k1gInSc4	cover
verzi	verze	k1gFnSc4	verze
hitu	hit	k1gInSc2	hit
All	All	k1gMnSc1	All
by	by	kYmCp3nS	by
Myself	Myself	k1gInSc4	Myself
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
věnována	věnován	k2eAgFnSc1d1	věnována
Janu	Jan	k1gMnSc3	Jan
Palachovi	Palach	k1gMnSc3	Palach
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Antichartu	anticharta	k1gFnSc4	anticharta
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
text	text	k1gInSc4	text
četl	číst	k5eAaImAgMnS	číst
4.	[number]	k4	4.
února	únor	k1gInSc2	únor
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
to	ten	k3xDgNnSc4	ten
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
dál	daleko	k6eAd2	daleko
legálně	legálně	k6eAd1	legálně
zpívat	zpívat	k5eAaImF	zpívat
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
<g/>
,	,	kIx,	,
tak	tak	k9	tak
to	ten	k3xDgNnSc4	ten
prostě	prostě	k6eAd1	prostě
podepsat	podepsat	k5eAaPmF	podepsat
musel	muset	k5eAaImAgMnS	muset
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
Zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
kromě	kromě	k7c2	kromě
klasických	klasický	k2eAgFnPc2d1	klasická
písní	píseň	k1gFnPc2	píseň
vydal	vydat	k5eAaPmAgInS	vydat
i	i	k9	i
album	album	k1gNnSc4	album
Išel	Išel	k1gMnSc1	Išel
macek	macek	k1gMnSc1	macek
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
lidové	lidový	k2eAgFnPc4d1	lidová
písně	píseň	k1gFnPc4	píseň
jím	jíst	k5eAaImIp1nS	jíst
přezpívané	přezpívaný	k2eAgNnSc1d1	přezpívaný
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1977	[number]	k4	1977
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
přinesl	přinést	k5eAaPmAgInS	přinést
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
soutěží	soutěž	k1gFnPc2	soutěž
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zpíval	zpívat	k5eAaImAgMnS	zpívat
píseň	píseň	k1gFnSc4	píseň
Jdi	jít	k5eAaImRp2nS	jít
za	za	k7c7	za
štěstím	štěstí	k1gNnSc7	štěstí
<g/>
,	,	kIx,	,
složenou	složený	k2eAgFnSc4d1	složená
jeho	jeho	k3xOp3gMnSc7	jeho
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
skladatelem	skladatel	k1gMnSc7	skladatel
Karlem	Karel	k1gMnSc7	Karel
Svobodou	Svoboda	k1gMnSc7	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
filmové	filmový	k2eAgFnSc6d1	filmová
pohádce	pohádka	k1gFnSc6	pohádka
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
budí	budit	k5eAaImIp3nS	budit
princezny	princezna	k1gFnSc2	princezna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
vysílán	vysílat	k5eAaImNgInS	vysílat
jeho	jeho	k3xOp3gInSc1	jeho
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
éra	éra	k1gFnSc1	éra
Karla	Karel	k1gMnSc2	Karel
Gotta	Gott	k1gMnSc2	Gott
(	(	kIx(	(
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90.	[number]	k4	90.
léta	léto	k1gNnSc2	léto
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
80.	[number]	k4	80.
letech	léto	k1gNnPc6	léto
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
i	i	k8xC	i
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
repertoár	repertoár	k1gInSc1	repertoár
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
od	od	k7c2	od
popu	pop	k1gInSc2	pop
přes	přes	k7c4	přes
"	"	kIx"	"
<g/>
lidovky	lidovky	k1gFnPc4	lidovky
<g/>
"	"	kIx"	"
až	až	k9	až
po	po	k7c4	po
upravené	upravený	k2eAgFnPc4d1	upravená
skladby	skladba	k1gFnPc4	skladba
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
několika	několik	k4yIc6	několik
celovečerních	celovečerní	k2eAgInPc6d1	celovečerní
hraných	hraný	k2eAgInPc6d1	hraný
filmech	film	k1gInPc6	film
a	a	k8xC	a
v	v	k7c6	v
krátkých	krátký	k2eAgInPc6d1	krátký
dokumentárních	dokumentární	k2eAgInPc6d1	dokumentární
snímcích	snímek	k1gInPc6	snímek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizi	televize	k1gFnSc6	televize
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vysílaly	vysílat	k5eAaImAgInP	vysílat
záznamy	záznam	k1gInPc1	záznam
jeho	jeho	k3xOp3gInPc2	jeho
koncertů	koncert	k1gInPc2	koncert
z	z	k7c2	z
Lucerny	lucerna	k1gFnSc2	lucerna
(	(	kIx(	(
<g/>
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
a	a	k8xC	a
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
sálů	sál	k1gInPc2	sál
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
kromě	kromě	k7c2	kromě
množství	množství	k1gNnSc2	množství
alb	alba	k1gFnPc2	alba
vydal	vydat	k5eAaPmAgMnS	vydat
i	i	k9	i
vánoční	vánoční	k2eAgInSc4d1	vánoční
album	album	k1gNnSc1	album
Bílé	bílý	k2eAgFnPc1d1	bílá
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
navázalo	navázat	k5eAaPmAgNnS	navázat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
vánoční	vánoční	k2eAgNnSc4d1	vánoční
album	album	k1gNnSc4	album
Vánoce	Vánoce	k1gFnPc1	Vánoce
ve	v	k7c6	v
zlaté	zlatý	k2eAgFnSc6d1	zlatá
Praze	Praha	k1gFnSc6	Praha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
jím	jíst	k5eAaImIp1nS	jíst
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
každoročně	každoročně	k6eAd1	každoročně
pořádal	pořádat	k5eAaImAgMnS	pořádat
několik	několik	k4yIc4	několik
turné	turné	k1gNnPc2	turné
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
počet	počet	k1gInSc1	počet
koncertů	koncert	k1gInPc2	koncert
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
odhadoval	odhadovat	k5eAaImAgMnS	odhadovat
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
360	[number]	k4	360
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
hudebních	hudební	k2eAgInPc6d1	hudební
pořadech	pořad	k1gInPc6	pořad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
představoval	představovat	k5eAaImAgMnS	představovat
své	svůj	k3xOyFgFnPc4	svůj
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
popularita	popularita	k1gFnSc1	popularita
Karla	Karla	k1gFnSc1	Karla
Gotta	Gotta	k1gFnSc1	Gotta
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vrcholu	vrchol	k1gInSc3	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
scéně	scéna	k1gFnSc6	scéna
mu	on	k3xPp3gNnSc3	on
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
konkurence	konkurence	k1gFnSc1	konkurence
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
mladých	mladý	k2eAgMnPc2d1	mladý
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
jen	jen	k6eAd1	jen
nakrátko	nakrátko	k6eAd1	nakrátko
přerušila	přerušit	k5eAaPmAgFnS	přerušit
jeho	jeho	k3xOp3gNnSc4	jeho
kralování	kralování	k1gNnSc4	kralování
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
byl	být	k5eAaImAgInS	být
sesazen	sesadit	k5eAaPmNgMnS	sesadit
z	z	k7c2	z
první	první	k4xOgFnSc2	první
příčky	příčka	k1gFnSc2	příčka
ankety	anketa	k1gFnSc2	anketa
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
Slavík	Slavík	k1gMnSc1	Slavík
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
oslavil	oslavit	k5eAaPmAgInS	oslavit
20	[number]	k4	20
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgFnSc4d1	trvající
spolupráci	spolupráce	k1gFnSc4	spolupráce
se	s	k7c7	s
Skupinou	skupina	k1gFnSc7	skupina
Ladislava	Ladislav	k1gMnSc2	Ladislav
Štaidla	Štaidlo	k1gNnSc2	Štaidlo
dvojkoncertem	dvojkoncert	k1gInSc7	dvojkoncert
ve	v	k7c6	v
vyprodané	vyprodaný	k2eAgFnSc6d1	vyprodaná
pražské	pražský	k2eAgFnSc6d1	Pražská
Sportovní	sportovní	k2eAgFnSc6d1	sportovní
hale	hala	k1gFnSc6	hala
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
získal	získat	k5eAaPmAgInS	získat
tehdy	tehdy	k6eAd1	tehdy
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
možné	možný	k2eAgNnSc4d1	možné
ocenění	ocenění	k1gNnSc4	ocenění
Národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
–	–	k?	–
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
zpěvák	zpěvák	k1gMnSc1	zpěvák
pop	pop	k1gMnSc1	pop
music	music	k1gMnSc1	music
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vysílala	vysílat	k5eAaImAgFnS	vysílat
německá	německý	k2eAgFnSc1d1	německá
show	show	k1gFnSc1	show
Karla	Karel	k1gMnSc2	Karel
Gotta	Gotta	k1gFnSc1	Gotta
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
Hits	Hits	k1gInSc1	Hits
und	und	k?	und
Karneval	karneval	k1gInSc1	karneval
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slavila	slavit	k5eAaImAgFnS	slavit
velké	velký	k2eAgInPc4d1	velký
úspěchy	úspěch	k1gInPc4	úspěch
i	i	k9	i
televizní	televizní	k2eAgMnSc1d1	televizní
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Koncem	koncem	k7c2	koncem
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
opět	opět	k6eAd1	opět
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
ve	v	k7c6	v
sportovních	sportovní	k2eAgFnPc6d1	sportovní
halách	hala	k1gFnPc6	hala
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vyprodával	vyprodávat	k5eAaImAgMnS	vyprodávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
koncerty	koncert	k1gInPc4	koncert
ze	z	k7c2	z
sportovní	sportovní	k2eAgFnSc2d1	sportovní
haly	hala	k1gFnSc2	hala
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
tři	tři	k4xCgNnPc4	tři
alba	album	k1gNnPc4	album
zaměřená	zaměřený	k2eAgNnPc4d1	zaměřené
na	na	k7c4	na
romantické	romantický	k2eAgFnPc4d1	romantická
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgNnPc2d1	další
alb	album	k1gNnPc2	album
–	–	k?	–
například	například	k6eAd1	například
alba	album	k1gNnSc2	album
Muzika	muzika	k1gFnSc1	muzika
<g/>
,	,	kIx,	,
Loď	loď	k1gFnSc1	loď
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
Hrátky	hrátky	k1gFnPc1	hrátky
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
<g/>
,	,	kIx,	,
Posel	posel	k1gMnSc1	posel
dobrých	dobrý	k2eAgFnPc2d1	dobrá
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
neusiloval	usilovat	k5eNaImAgMnS	usilovat
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
o	o	k7c4	o
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
a	a	k8xC	a
dával	dávat	k5eAaImAgMnS	dávat
přednost	přednost	k1gFnSc4	přednost
rozmanitějšímu	rozmanitý	k2eAgInSc3d2	rozmanitější
výběru	výběr	k1gInSc3	výběr
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
podle	podle	k7c2	podle
něho	on	k3xPp3gNnSc2	on
jeho	on	k3xPp3gNnSc2	on
posluchače	posluchač	k1gMnSc2	posluchač
mohly	moct	k5eAaImAgFnP	moct
emocionálně	emocionálně	k6eAd1	emocionálně
nabít	nabít	k5eAaPmF	nabít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
Sametovou	sametový	k2eAgFnSc7d1	sametová
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
,	,	kIx,	,
oslavil	oslavit	k5eAaPmAgMnS	oslavit
své	svůj	k3xOyFgNnSc4	svůj
50.	[number]	k4	50.
narozeniny	narozeniny	k1gFnPc4	narozeniny
vyprodaným	vyprodaný	k2eAgInSc7d1	vyprodaný
koncertem	koncert	k1gInSc7	koncert
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
tehdejší	tehdejší	k2eAgFnPc1d1	tehdejší
hvězdy	hvězda	k1gFnPc1	hvězda
československé	československý	k2eAgFnPc1d1	Československá
pop	pop	k1gInSc4	pop
music	musice	k1gFnPc2	musice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
ukončil	ukončit	k5eAaPmAgMnS	ukončit
Ladislav	Ladislav	k1gMnSc1	Ladislav
Štaidl	Štaidl	k1gFnSc4	Štaidl
Skupinu	skupina	k1gFnSc4	skupina
Ladislava	Ladislav	k1gMnSc2	Ladislav
Štaidla	Štaidlo	k1gNnSc2	Štaidlo
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Gotta	Gott	k1gMnSc2	Gott
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
založený	založený	k2eAgInSc4d1	založený
Orchestr	orchestr	k1gInSc4	orchestr
Karla	Karel	k1gMnSc2	Karel
Gotta	Gott	k1gMnSc2	Gott
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Pavla	Pavel	k1gMnSc2	Pavel
Větrovce	Větrovec	k1gMnSc2	Větrovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
ukončit	ukončit	k5eAaPmF	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
rozloučení	rozloučení	k1gNnSc4	rozloučení
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
mamutí	mamutí	k2eAgNnSc4d1	mamutí
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
sportovních	sportovní	k2eAgFnPc6d1	sportovní
halách	hala	k1gFnPc6	hala
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
turné	turné	k1gNnSc2	turné
Karla	Karel	k1gMnSc4	Karel
Gotta	Gott	k1gMnSc4	Gott
donutil	donutit	k5eAaPmAgMnS	donutit
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
přehodnotit	přehodnotit	k5eAaPmF	přehodnotit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
Anděl	Anděla	k1gFnPc2	Anděla
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
album	album	k1gNnSc1	album
Když	když	k8xS	když
muž	muž	k1gMnSc1	muž
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
snídá	snídat	k5eAaImIp3nS	snídat
stalo	stát	k5eAaPmAgNnS	stát
nejprodávanější	prodávaný	k2eAgFnSc7d3	nejprodávanější
deskou	deska	k1gFnSc7	deska
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
stejný	stejný	k2eAgInSc1d1	stejný
úspěch	úspěch	k1gInSc1	úspěch
slavil	slavit	k5eAaImAgInS	slavit
ještě	ještě	k9	ještě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
a	a	k8xC	a
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
90.	[number]	k4	90.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Gott	Gott	k1gInSc1	Gott
věnoval	věnovat	k5eAaImAgInS	věnovat
také	také	k9	také
stále	stále	k6eAd1	stále
víc	hodně	k6eAd2	hodně
své	svůj	k3xOyFgFnSc3	svůj
druhé	druhý	k4xOgFnSc3	druhý
lásce	láska	k1gFnSc3	láska
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
malování	malování	k1gNnSc1	malování
<g/>
,	,	kIx,	,
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
obrazy	obraz	k1gInPc4	obraz
úspěšně	úspěšně	k6eAd1	úspěšně
vystavoval	vystavovat	k5eAaImAgInS	vystavovat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Janečkem	Janeček	k1gMnSc7	Janeček
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1993	[number]	k4	1993
agenturu	agentura	k1gFnSc4	agentura
GOJA	GOJA	kA	GOJA
<g/>
[	[	kIx(	[
<g/>
11	[number]	k4	11
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Supraphon	supraphon	k1gInSc4	supraphon
ve	v	k7c6	v
vydávání	vydávání	k1gNnSc6	vydávání
alb	alba	k1gFnPc2	alba
Karla	Karel	k1gMnSc2	Karel
Gotta	Gott	k1gMnSc2	Gott
a	a	k8xC	a
stála	stát	k5eAaImAgFnS	stát
i	i	k9	i
za	za	k7c4	za
jeho	jeho	k3xOp3gInPc4	jeho
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1993	[number]	k4	1993
oznámil	oznámit	k5eAaPmAgMnS	oznámit
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
založení	založení	k1gNnSc2	založení
Nadace	nadace	k1gFnSc2	nadace
Karla	Karel	k1gMnSc2	Karel
Gotta	Gott	k1gMnSc2	Gott
Interpo	Interpa	k1gFnSc5	Interpa
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
12	[number]	k4	12
<g/>
]	]	kIx)	]
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
pomáhat	pomáhat	k5eAaImF	pomáhat
dětem	dítě	k1gFnPc3	dítě
policistů	policista	k1gMnPc2	policista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Gott	Gott	k1gMnSc1	Gott
nadaci	nadace	k1gFnSc4	nadace
věnoval	věnovat	k5eAaImAgMnS	věnovat
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
založení	založení	k1gNnSc6	založení
čtyři	čtyři	k4xCgInPc1	čtyři
miliony	milion	k4xCgInPc4	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
partneři	partner	k1gMnPc1	partner
(	(	kIx(	(
<g/>
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
podnikatel	podnikatel	k1gMnSc1	podnikatel
František	František	k1gMnSc1	František
Mrázek	Mrázek	k1gMnSc1	Mrázek
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Provod	provoda	k1gFnPc2	provoda
<g/>
)	)	kIx)	)
dali	dát	k5eAaPmAgMnP	dát
každý	každý	k3xTgMnSc1	každý
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
Úspěch	úspěch	k1gInSc1	úspěch
sklidil	sklidit	k5eAaPmAgInS	sklidit
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
'	'	kIx"	'
<g/>
94	[number]	k4	94
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zakončil	zakončit	k5eAaPmAgMnS	zakončit
koncertem	koncert	k1gInSc7	koncert
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
–	–	k?	–
koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
natáčen	natáčet	k5eAaImNgInS	natáčet
do	do	k7c2	do
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Podobných	podobný	k2eAgNnPc2d1	podobné
turné	turné	k1gNnPc2	turné
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
měl	mít	k5eAaImAgMnS	mít
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
ještě	ještě	k6eAd1	ještě
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obnovené	obnovený	k2eAgFnSc6d1	obnovená
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgMnSc1d1	český
slavík	slavík	k1gMnSc1	slavík
navazující	navazující	k2eAgMnSc1d1	navazující
na	na	k7c4	na
legendárního	legendární	k2eAgMnSc4d1	legendární
československého	československý	k2eAgMnSc4d1	československý
Zlatého	zlatý	k2eAgMnSc4d1	zlatý
slavíka	slavík	k1gMnSc4	slavík
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
náskokem	náskok	k1gInSc7	náskok
opět	opět	k6eAd1	opět
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
roků	rok	k1gInPc2	rok
1998	[number]	k4	1998
a	a	k8xC	a
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
udržel	udržet	k5eAaPmAgInS	udržet
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
společné	společný	k2eAgNnSc1d1	společné
album	album	k1gNnSc1	album
s	s	k7c7	s
Lucií	Lucie	k1gFnSc7	Lucie
Bílou	bílý	k2eAgFnSc4d1	bílá
s	s	k7c7	s
názvem	název	k1gInSc7	název
Duety	duet	k1gInPc4	duet
stalo	stát	k5eAaPmAgNnS	stát
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
představil	představit	k5eAaPmAgMnS	představit
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
společném	společný	k2eAgNnSc6d1	společné
turné	turné	k1gNnSc6	turné
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
vánoční	vánoční	k2eAgInSc4d1	vánoční
koncert	koncert	k1gInSc4	koncert
do	do	k7c2	do
Lucerny	lucerna	k1gFnSc2	lucerna
pozval	pozvat	k5eAaPmAgMnS	pozvat
jako	jako	k9	jako
hosta	host	k1gMnSc4	host
italského	italský	k2eAgMnSc2d1	italský
zpěváka	zpěvák	k1gMnSc2	zpěvák
Albana	Alban	k1gMnSc2	Alban
Carrisiho	Carrisi	k1gMnSc2	Carrisi
<g/>
.	.	kIx.	.
90.	[number]	k4	90.
léta	léto	k1gNnSc2	léto
završil	završit	k5eAaPmAgInS	završit
koncertem	koncert	k1gInSc7	koncert
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
arabských	arabský	k2eAgInPc6d1	arabský
emirátech	emirát	k1gInPc6	emirát
a	a	k8xC	a
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
před	před	k7c7	před
600	[number]	k4	600
miliony	milion	k4xCgInPc7	milion
diváky	divák	k1gMnPc7	divák
Eurovize	Eurovize	k1gFnSc2	Eurovize
a	a	k8xC	a
v	v	k7c4	v
Carnegie	Carnegie	k1gFnPc4	Carnegie
Hall	Halla	k1gFnPc2	Halla
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
druhý	druhý	k4xOgInSc4	druhý
koncert	koncert	k1gInSc4	koncert
v	v	k7c4	v
Carnegie	Carnegie	k1gFnPc4	Carnegie
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
vzal	vzít	k5eAaPmAgMnS	vzít
jako	jako	k8xC	jako
hosta	host	k1gMnSc4	host
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Helenu	Helena	k1gFnSc4	Helena
Vondráčkovou	Vondráčková	k1gFnSc4	Vondráčková
<g/>
.	.	kIx.	.
</s>
<s>
Ukončil	ukončit	k5eAaPmAgMnS	ukončit
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
agenturou	agentura	k1gFnSc7	agentura
GOJA	GOJA	kA	GOJA
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
kvůli	kvůli	k7c3	kvůli
neshodám	neshoda	k1gFnPc3	neshoda
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Janečkem	Janeček	k1gMnSc7	Janeček
<g/>
,	,	kIx,	,
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
Supraphonu	supraphon	k1gInSc3	supraphon
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
mateřskému	mateřský	k2eAgNnSc3d1	mateřské
vydavatelství	vydavatelství	k1gNnSc3	vydavatelství
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
album	album	k1gNnSc1	album
Pokaždé	pokaždé	k6eAd1	pokaždé
stalo	stát	k5eAaPmAgNnS	stát
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
byla	být	k5eAaImAgFnS	být
také	také	k9	také
turné	turné	k1gNnSc4	turné
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
zmiňované	zmiňovaný	k2eAgNnSc1d1	zmiňované
turné	turné	k1gNnSc1	turné
jménem	jméno	k1gNnSc7	jméno
Zpátky	zpátky	k6eAd1	zpátky
si	se	k3xPyFc3	se
dám	dát	k5eAaPmIp1nS	dát
tenhle	tenhle	k3xDgInSc4	tenhle
film	film	k1gInSc4	film
zakončil	zakončit	k5eAaPmAgMnS	zakončit
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
koncertem	koncert	k1gInSc7	koncert
ve	v	k7c4	v
vyprodané	vyprodaný	k2eAgInPc4d1	vyprodaný
Tipsport	Tipsport	k1gInSc4	Tipsport
aréně	aréna	k1gFnSc6	aréna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
získal	získat	k5eAaPmAgInS	získat
3	[number]	k4	3
<g/>
×	×	k?	×
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
Supraphonu	supraphon	k1gInSc2	supraphon
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
svých	svůj	k3xOyFgFnPc2	svůj
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
galakoncert	galakoncert	k1gInSc4	galakoncert
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
70.	[number]	k4	70.
narozeninám	narozeniny	k1gFnPc3	narozeniny
přišlo	přijít	k5eAaPmAgNnS	přijít
popřát	popřát	k5eAaPmF	popřát
a	a	k8xC	a
zazpívat	zazpívat	k5eAaPmF	zazpívat
do	do	k7c2	do
vyprodané	vyprodaný	k2eAgFnSc2d1	vyprodaná
O2	O2	k1gFnSc2	O2
areny	arena	k1gFnSc2	arena
130	[number]	k4	130
zpěváků	zpěvák	k1gMnPc2	zpěvák
a	a	k8xC	a
15 000	[number]	k4	15 000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgFnPc2	tento
narozenin	narozeniny	k1gFnPc2	narozeniny
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
vydal	vydat	k5eAaPmAgMnS	vydat
kompilační	kompilační	k2eAgNnSc4d1	kompilační
album	album	k1gNnSc4	album
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
–	–	k?	–
Mé	můj	k3xOp1gFnPc4	můj
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
box	box	k1gInSc4	box
obsahující	obsahující	k2eAgInSc4d1	obsahující
všechna	všechen	k3xTgNnPc4	všechen
jeho	jeho	k3xOp3gNnPc4	jeho
alba	album	k1gNnPc4	album
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1965	[number]	k4	1965
–	–	k?	–
1992	[number]	k4	1992
vydaná	vydaný	k2eAgFnSc1d1	vydaná
jeho	jeho	k3xOp3gNnSc7	jeho
mateřským	mateřský	k2eAgNnSc7d1	mateřské
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
také	také	k9	také
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
kompilační	kompilační	k2eAgNnSc1d1	kompilační
album	album	k1gNnSc1	album
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
–	–	k?	–
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
byl	být	k5eAaImAgMnS	být
tenkrát	tenkrát	k6eAd1	tenkrát
kluk	kluk	k1gMnSc1	kluk
aneb	aneb	k?	aneb
70	[number]	k4	70
hitů	hit	k1gInPc2	hit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tato	tento	k3xDgNnPc4	tento
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
získal	získat	k5eAaPmAgInS	získat
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
Supraphonu	supraphon	k1gInSc2	supraphon
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
zmiňované	zmiňovaný	k2eAgNnSc1d1	zmiňované
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
pražské	pražský	k2eAgFnSc2d1	Pražská
Lucerny	lucerna	k1gFnSc2	lucerna
na	na	k7c4	na
společný	společný	k2eAgInSc4d1	společný
koncert	koncert	k1gInSc4	koncert
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
Urbanovou	Urbanová	k1gFnSc7	Urbanová
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
na	na	k7c6	na
DVD	DVD	kA	DVD
a	a	k8xC	a
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
turné	turné	k1gNnSc4	turné
zakončené	zakončený	k2eAgNnSc4d1	zakončené
ve	v	k7c4	v
vyprodané	vyprodaný	k2eAgFnPc4d1	vyprodaná
O2	O2	k1gFnPc4	O2
areně	areeň	k1gFnSc2	areeň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncert	koncert	k1gInSc1	koncert
nahradil	nahradit	k5eAaPmAgInS	nahradit
tradiční	tradiční	k2eAgInSc1d1	tradiční
Vánoční	vánoční	k2eAgInSc1d1	vánoční
koncert	koncert	k1gInSc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
další	další	k2eAgNnSc4d1	další
turné	turné	k1gNnSc4	turné
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Tour	Tour	k1gInSc4	Tour
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zakončil	zakončit	k5eAaPmAgMnS	zakončit
tentokrát	tentokrát	k6eAd1	tentokrát
dvěma	dva	k4xCgInPc7	dva
koncerty	koncert	k1gInPc7	koncert
v	v	k7c6	v
O2	O2	k1gFnSc6	O2
areně	areeň	k1gFnSc2	areeň
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
vydal	vydat	k5eAaPmAgMnS	vydat
6	[number]	k4	6
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zdravotních	zdravotní	k2eAgFnPc6d1	zdravotní
potížích	potíž	k1gFnPc6	potíž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
pódia	pódium	k1gNnPc4	pódium
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
vydal	vydat	k5eAaPmAgInS	vydat
album	album	k1gNnSc4	album
40	[number]	k4	40
slavíků	slavík	k1gInPc2	slavík
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
poděkováním	poděkování	k1gNnSc7	poděkování
za	za	k7c4	za
čtyřicet	čtyřicet	k4xCc4	čtyřicet
prvních	první	k4xOgNnPc2	první
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
anketách	anketa	k1gFnPc6	anketa
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
a	a	k8xC	a
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
koncertní	koncertní	k2eAgFnSc4d1	koncertní
šňůru	šňůra	k1gFnSc4	šňůra
zakončenou	zakončený	k2eAgFnSc4d1	zakončená
vyprodaným	vyprodaný	k2eAgInSc7d1	vyprodaný
koncertem	koncert	k1gInSc7	koncert
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
vydal	vydat	k5eAaPmAgMnS	vydat
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Ta	ten	k3xDgFnSc1	ten
pravá	pravá	k1gFnSc1	pravá
a	a	k8xC	a
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
koncert	koncert	k1gInSc4	koncert
ve	v	k7c6	v
vyprodané	vyprodaný	k2eAgFnSc6d1	vyprodaná
O2	O2	k1gFnSc6	O2
areně	areně	k6eAd1	areně
před	před	k7c7	před
20 000	[number]	k4	20 000
diváky	divák	k1gMnPc7	divák
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
Ta	ten	k3xDgFnSc1	ten
pravá	pravý	k2eAgFnSc1d1	pravá
bezprostředně	bezprostředně	k6eAd1	bezprostředně
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
nad	nad	k7c7	nad
konkurenčními	konkurenční	k2eAgNnPc7d1	konkurenční
alby	album	k1gNnPc7	album
kolegů	kolega	k1gMnPc2	kolega
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
Když	když	k8xS	když
draka	drak	k1gMnSc2	drak
bolí	bolet	k5eAaImIp3nS	bolet
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
dcery	dcera	k1gFnPc1	dcera
Charlotta	Charlotta	k1gFnSc1	Charlotta
a	a	k8xC	a
Nelly	Nella	k1gFnSc2	Nella
Gottová	Gottová	k1gFnSc1	Gottová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
magazín	magazín	k1gInSc4	magazín
iDNES	iDNES	k?	iDNES
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
jeho	jeho	k3xOp3gNnPc2	jeho
nadcházejících	nadcházející	k2eAgNnPc2d1	nadcházející
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
narozenin	narozeniny	k1gFnPc2	narozeniny
plánuje	plánovat	k5eAaImIp3nS	plánovat
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
O2	O2	k1gFnSc6	O2
areně	areeň	k1gFnSc2	areeň
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
autobiografii	autobiografie	k1gFnSc4	autobiografie
a	a	k8xC	a
točí	točit	k5eAaImIp3nS	točit
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
O2	O2	k1gFnSc6	O2
areně	areně	k6eAd1	areně
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
zrušil	zrušit	k5eAaPmAgInS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jaro	jaro	k6eAd1	jaro
2019	[number]	k4	2019
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Gotta	Gott	k1gMnSc4	Gott
neslo	nést	k5eAaImAgNnS	nést
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
velkých	velký	k2eAgInPc2d1	velký
samostatných	samostatný	k2eAgInPc2d1	samostatný
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
Žofíně	Žofín	k1gInSc6	Žofín
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Příbrami	Příbram	k1gFnSc6	Příbram
a	a	k8xC	a
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Benátská	benátský	k2eAgFnSc1d1	Benátská
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
poslouží	posloužit	k5eAaPmIp3nS	posloužit
jako	jako	k9	jako
veliká	veliký	k2eAgFnSc1d1	veliká
oslava	oslava	k1gFnSc1	oslava
nadcházejících	nadcházející	k2eAgFnPc2d1	nadcházející
osmdesátin	osmdesátina	k1gFnPc2	osmdesátina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Gotta	Gott	k1gInSc2	Gott
samotného	samotný	k2eAgInSc2d1	samotný
zde	zde	k6eAd1	zde
zazpívají	zazpívat	k5eAaPmIp3nP	zazpívat
přední	přední	k2eAgMnPc1d1	přední
čeští	český	k2eAgMnPc1d1	český
hudebníci	hudebník	k1gMnPc1	hudebník
právě	právě	k9	právě
písně	píseň	k1gFnSc2	píseň
Karla	Karel	k1gMnSc4	Karel
Gotta	Gott	k1gMnSc4	Gott
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zlatý	zlatý	k2eAgInSc1d1	zlatý
a	a	k8xC	a
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgNnSc4	svůj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesátileté	padesátiletý	k2eAgFnSc2d1	padesátiletá
zpěvácké	zpěvácký	k2eAgFnSc2d1	zpěvácká
kariéry	kariéra	k1gFnSc2	kariéra
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
42krát	42krát	k1gInSc4	42krát
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
letech	let	k1gInPc6	let
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
pětkrát	pětkrát	k6eAd1	pětkrát
skončil	skončit	k5eAaPmAgInS	skončit
druhý	druhý	k4xOgMnSc1	druhý
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
a	a	k8xC	a
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
třikrát	třikrát	k6eAd1	třikrát
třetí	třetí	k4xOgFnSc2	třetí
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
a	a	k8xC	a
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
se	se	k3xPyFc4	se
ankety	anketa	k1gFnSc2	anketa
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
ani	ani	k8xC	ani
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
nekonaly	konat	k5eNaImAgInP	konat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Televizní	televizní	k2eAgFnPc1d1	televizní
ceny	cena	k1gFnPc1	cena
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
o	o	k7c4	o
nejoblíbenější	oblíbený	k2eAgFnPc4d3	nejoblíbenější
tváře	tvář	k1gFnPc4	tvář
a	a	k8xC	a
pořady	pořad	k1gInPc4	pořad
televizních	televizní	k2eAgFnPc2d1	televizní
obrazovek	obrazovka	k1gFnPc2	obrazovka
TýTý	TýTý	k1gFnSc2	TýTý
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
celkem	celkem	k6eAd1	celkem
31krát	31krát	k1gInSc4	31krát
(	(	kIx(	(
<g/>
22krát	22krát	k1gInSc4	22krát
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
v	v	k7c6	v
ročnících	ročník	k1gInPc6	ročník
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
a	a	k8xC	a
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
a	a	k8xC	a
9krát	9krát	k1gMnSc1	9krát
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Absolutní	absolutní	k2eAgMnSc1d1	absolutní
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ročnících	ročník	k1gInPc6	ročník
2010	[number]	k4	2010
a	a	k8xC	a
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
3.	[number]	k4	3.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
ANNO	Anna	k1gFnSc5	Anna
2009	[number]	k4	2009
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
cenu	cena	k1gFnSc4	cena
televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vyznamenání	vyznamenání	k1gNnPc4	vyznamenání
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
ocenění	ocenění	k1gNnPc4	ocenění
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
3.	[number]	k4	3.
května	květen	k1gInSc2	květen
1977	[number]	k4	1977
byl	být	k5eAaImAgMnS	být
Karlu	Karel	k1gMnSc3	Karel
Gottovi	Gott	k1gMnSc3	Gott
propůjčen	propůjčen	k2eAgInSc4d1	propůjčen
titul	titul	k1gInSc4	titul
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
a	a	k8xC	a
30.	[number]	k4	30.
dubna	duben	k1gInSc2	duben
1985	[number]	k4	1985
pak	pak	k6eAd1	pak
titul	titul	k1gInSc4	titul
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tehdy	tehdy	k6eAd1	tehdy
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
uděleno	udělit	k5eAaPmNgNnS	udělit
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Karla	Karel	k1gMnSc2	Karel
Gotta	Gott	k1gMnSc2	Gott
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
reprezentaci	reprezentace	k1gFnSc4	reprezentace
Československa	Československo	k1gNnSc2	Československo
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
umělecké	umělecký	k2eAgFnPc4d1	umělecká
zásluhy	zásluha	k1gFnPc4	zásluha
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
co	co	k9	co
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
umělecké	umělecký	k2eAgNnSc4d1	umělecké
ocenění	ocenění	k1gNnSc4	ocenění
získal	získat	k5eAaPmAgMnS	získat
zpěvák	zpěvák	k1gMnSc1	zpěvák
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
28.	[number]	k4	28.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
obdržel	obdržet	k5eAaPmAgInS	obdržet
Gott	Gott	k1gInSc1	Gott
od	od	k7c2	od
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
Medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
I.	I.	kA	I.
stupně	stupeň	k1gInPc1	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Praha	Praha	k1gFnSc1	Praha
5	[number]	k4	5
mu	on	k3xPp3gMnSc3	on
jako	jako	k8xC	jako
významnému	významný	k2eAgMnSc3d1	významný
občanu	občan	k1gMnSc3	občan
udělila	udělit	k5eAaPmAgFnS	udělit
28.	[number]	k4	28.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
Portheimka	Portheimek	k1gMnSc2	Portheimek
Záslužnou	záslužný	k2eAgFnSc4d1	záslužná
medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
2.	[number]	k4	2.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
svého	své	k1gNnSc2	své
rodného	rodný	k2eAgNnSc2d1	rodné
města	město	k1gNnSc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
hejtmankou	hejtmanka	k1gFnSc7	hejtmanka
Miladou	Milada	k1gFnSc7	Milada
Emmerovou	Emmerová	k1gFnSc7	Emmerová
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
Dvorany	dvorana	k1gFnSc2	dvorana
slávy	sláva	k1gFnSc2	sláva
Plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
udělila	udělit	k5eAaPmAgFnS	udělit
Asociace	asociace	k1gFnSc1	asociace
českých	český	k2eAgMnPc2d1	český
rusistů	rusista	k1gMnPc2	rusista
Karlu	Karel	k1gMnSc3	Karel
Gottovi	Gottův	k2eAgMnPc1d1	Gottův
pamětní	pamětní	k2eAgFnPc4d1	pamětní
Medaili	medaile	k1gFnSc4	medaile
Josefa	Josef	k1gMnSc2	Josef
Jungmanna	Jungmann	k1gMnSc2	Jungmann
za	za	k7c4	za
propagaci	propagace	k1gFnSc4	propagace
česko-ruských	českouský	k2eAgInPc2d1	česko-ruský
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
12.	[number]	k4	12.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
převzal	převzít	k5eAaPmAgInS	převzít
Gott	Gott	k1gInSc1	Gott
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
Řád	řád	k1gInSc1	řád
Jurije	Jurije	k1gFnSc7	Jurije
Gagarina	Gagarin	k2eAgMnSc2d1	Gagarin
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
mu	on	k3xPp3gMnSc3	on
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
udělil	udělit	k5eAaPmAgInS	udělit
ocenění	ocenění	k1gNnPc2	ocenění
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
vybrali	vybrat	k5eAaPmAgMnP	vybrat
posluchači	posluchač	k1gMnPc1	posluchač
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
"	"	kIx"	"
<g/>
100	[number]	k4	100
hitů	hit	k1gInPc2	hit
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
stého	stý	k4xOgNnSc2	stý
výročí	výročí	k1gNnSc2	výročí
založení	založení	k1gNnSc2	založení
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Gott	Gott	k1gInSc1	Gott
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
získal	získat	k5eAaPmAgMnS	získat
také	také	k9	také
5	[number]	k4	5
zlatých	zlatý	k2eAgFnPc2d1	zlatá
rolniček	rolnička	k1gFnPc2	rolnička
<g/>
,	,	kIx,	,
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
jehlu	jehla	k1gFnSc4	jehla
Polydoru	Polydor	k1gInSc2	Polydor
<g/>
,	,	kIx,	,
2	[number]	k4	2
Zlaté	zlatý	k2eAgInPc1d1	zlatý
klíče	klíč	k1gInPc1	klíč
festivalu	festival	k1gInSc2	festival
Intervize	Intervize	k1gFnSc2	Intervize
<g/>
,	,	kIx,	,
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
a	a	k8xC	a
Bronzovou	bronzový	k2eAgFnSc4d1	bronzová
Lyru	lyra	k1gFnSc4	lyra
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
Stříbrného	stříbrný	k2eAgMnSc4d1	stříbrný
a	a	k8xC	a
Bronzového	bronzový	k2eAgMnSc4d1	bronzový
lva	lev	k1gMnSc4	lev
rádia	rádius	k1gInSc2	rádius
Luxemburk	Luxemburk	k1gInSc1	Luxemburk
<g/>
,	,	kIx,	,
2	[number]	k4	2
Zlaté	zlatý	k2eAgFnPc1d1	zlatá
ladičky	ladička	k1gFnPc1	ladička
festivalu	festival	k1gInSc2	festival
Tag	tag	k1gInSc1	tag
des	des	k1gNnSc2	des
deutschen	deutschen	k2eAgInSc1d1	deutschen
Schlagers	Schlagers	k1gInSc1	Schlagers
<g/>
,	,	kIx,	,
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
<g />
.	.	kIx.	.
</s>
<s>
harfu	harfa	k1gFnSc4	harfa
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
×	×	k?	×
Trophée	Trophé	k1gInSc2	Trophé
nationale	nationale	k6eAd1	nationale
na	na	k7c6	na
přehlídce	přehlídka	k1gFnSc6	přehlídka
MIDEM	MIDEM	kA	MIDEM
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
<g/>
,	,	kIx,	,
cenu	cena	k1gFnSc4	cena
Třešňové	třešňový	k2eAgFnSc2d1	třešňová
květiny	květina	k1gFnSc2	květina
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
jehlu	jehla	k1gFnSc4	jehla
Pragakoncertu	Pragakoncert	k1gInSc2	Pragakoncert
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
zlatých	zlatý	k2eAgFnPc2d1	zlatá
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
platinové	platinový	k2eAgFnPc1d1	platinová
desky	deska	k1gFnPc1	deska
za	za	k7c2	za
nejprodávanějšího	prodávaný	k2eAgMnSc2d3	nejprodávanější
interpreta	interpret	k1gMnSc2	interpret
firem	firma	k1gFnPc2	firma
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
Polydor	Polydor	k1gInSc1	Polydor
a	a	k8xC	a
Polygram	Polygram	k1gInSc1	Polygram
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgNnPc2d1	další
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
diagnostikováno	diagnostikovat	k5eAaBmNgNnS	diagnostikovat
zhoubné	zhoubný	k2eAgNnSc1d1	zhoubné
nádorové	nádorový	k2eAgNnSc1d1	nádorové
onemocnění	onemocnění	k1gNnSc1	onemocnění
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18.	[number]	k4	18.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
média	médium	k1gNnSc2	médium
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
nad	nad	k7c7	nad
rakovinou	rakovina	k1gFnSc7	rakovina
definitivně	definitivně	k6eAd1	definitivně
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Písně	píseň	k1gFnPc1	píseň
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Známé	známý	k2eAgFnPc1d1	známá
písně	píseň	k1gFnPc1	píseň
====	====	k?	====
</s>
</p>
<p>
<s>
Oči	oko	k1gNnPc1	oko
sněhem	sníh	k1gInSc7	sníh
zaváté	zavátý	k2eAgInPc4d1	zavátý
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trezor	trezor	k1gInSc1	trezor
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
rájem	ráj	k1gInSc7	ráj
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bum	bum	k0	bum
<g/>
,	,	kIx,	,
bum	bum	k0	bum
<g/>
,	,	kIx,	,
bum	bum	k0	bum
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
est	est	k?	est
la	la	k1gNnSc2	la
vie	vie	k?	vie
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čas	čas	k1gInSc1	čas
růží	růž	k1gFnPc2	růž
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Carneval	Carneval	k1gFnSc1	Carneval
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
první	první	k4xOgFnSc1	první
láska	láska	k1gFnSc1	láska
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
vdává	vdávat	k5eAaImIp3nS	vdávat
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kávu	káva	k1gFnSc4	káva
si	se	k3xPyFc3	se
osladím	osladit	k5eAaPmIp1nS	osladit
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přijela	přijet	k5eAaPmAgFnS	přijet
pouť	pouť	k1gFnSc1	pouť
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
jaká	jaký	k3yIgFnSc1	jaký
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Léta	léto	k1gNnPc1	léto
prázdnin	prázdniny	k1gFnPc2	prázdniny
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kdepak	kdepak	k9	kdepak
ty	ten	k3xDgFnPc1	ten
ptáčku	ptáček	k1gInSc2	ptáček
hnízdo	hnízdo	k1gNnSc1	hnízdo
máš	mít	k5eAaImIp2nS	mít
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jen	jen	k6eAd1	jen
se	se	k3xPyFc4	se
hádej	hádat	k5eAaImRp2nS	hádat
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jdi	jít	k5eAaImRp2nS	jít
za	za	k7c4	za
štěstím	štěstí	k1gNnSc7	štěstí
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Včelka	včelka	k1gFnSc1	včelka
Mája	Mája	k1gFnSc1	Mája
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Když	když	k8xS	když
milenky	milenka	k1gFnPc1	milenka
pláčou	plakat	k5eAaImIp3nP	plakat
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
musím	muset	k5eAaImIp1nS	muset
zvládnout	zvládnout	k5eAaPmF	zvládnout
sám	sám	k3xTgInSc1	sám
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zvonky	zvonek	k1gInPc1	zvonek
štěstí	štěstí	k1gNnSc2	štěstí
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ráno	ráno	k6eAd1	ráno
jedu	jet	k5eAaImIp1nS	jet
dál	daleko	k6eAd2	daleko
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zůstanu	zůstat	k5eAaPmIp1nS	zůstat
svůj	svůj	k3xOyFgInSc4	svůj
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pábitelé	pábitel	k1gMnPc1	pábitel
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Když	když	k8xS	když
muž	muž	k1gMnSc1	muž
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
snídá	snídat	k5eAaImIp3nS	snídat
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Být	být	k5eAaImF	být
stále	stále	k6eAd1	stále
mlád	mlád	k2eAgMnSc1d1	mlád
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Filmové	filmový	k2eAgFnPc4d1	filmová
písně	píseň	k1gFnPc4	píseň
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgMnPc2d1	jiný
titulní	titulní	k2eAgFnSc4d1	titulní
písničku	písnička	k1gFnSc4	písnička
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnSc2d1	německá
i	i	k8xC	i
slovenské	slovenský	k2eAgFnSc2d1	slovenská
verze	verze	k1gFnSc2	verze
populárního	populární	k2eAgInSc2d1	populární
animovaného	animovaný	k2eAgInSc2d1	animovaný
seriálu	seriál	k1gInSc2	seriál
Včelka	včelka	k1gFnSc1	včelka
Mája	Mája	k1gFnSc1	Mája
(	(	kIx(	(
<g/>
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
skladatele	skladatel	k1gMnSc2	skladatel
Karla	Karel	k1gMnSc2	Karel
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
interpretem	interpret	k1gMnSc7	interpret
filmových	filmový	k2eAgFnPc2d1	filmová
písní	píseň	k1gFnPc2	píseň
Jdi	jít	k5eAaImRp2nS	jít
za	za	k7c7	za
štěstím	štěstí	k1gNnSc7	štěstí
z	z	k7c2	z
pohádky	pohádka	k1gFnSc2	pohádka
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
budí	budit	k5eAaImIp3nS	budit
princezny	princezna	k1gFnSc2	princezna
<g/>
,	,	kIx,	,
Kdepak	kdepak	k6eAd1	kdepak
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
ptáčku	ptáček	k1gInSc6	ptáček
<g/>
,	,	kIx,	,
hnízdo	hnízdo	k1gNnSc1	hnízdo
máš	mít	k5eAaImIp2nS	mít
<g/>
?	?	kIx.	?
</s>
<s>
ze	z	k7c2	z
slavné	slavný	k2eAgFnSc2d1	slavná
pohádky	pohádka	k1gFnSc2	pohádka
Tři	tři	k4xCgInPc4	tři
oříšky	oříšek	k1gInPc4	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
a	a	k8xC	a
také	také	k9	také
písně	píseň	k1gFnPc4	píseň
Krev	krev	k1gFnSc1	krev
toulavá	toulavý	k2eAgFnSc1d1	Toulavá
z	z	k7c2	z
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
Cirkus	cirkus	k1gInSc1	cirkus
Humberto	Humberta	k1gFnSc5	Humberta
a	a	k8xC	a
také	také	k9	také
písně	píseň	k1gFnPc4	píseň
Zdráv	zdráv	k2eAgMnSc1d1	zdráv
buď	buď	k8xC	buď
<g/>
,	,	kIx,	,
Robinsone	Robinson	k1gMnSc5	Robinson
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
Robinsona	Robinson	k1gMnSc4	Robinson
Crusoe	Cruso	k1gMnSc4	Cruso
<g/>
,	,	kIx,	,
námořníka	námořník	k1gMnSc4	námořník
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
zpíval	zpívat	k5eAaImAgMnS	zpívat
i	i	k9	i
melodii	melodie	k1gFnSc3	melodie
So	So	kA	So
Far	fara	k1gFnPc2	fara
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
aneb	aneb	k?	aneb
Koňská	koňský	k2eAgFnSc1d1	koňská
opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgInS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
dudáka	dudák	k1gInSc2	dudák
Švandy	švanda	k1gFnSc2	švanda
v	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
filmu	film	k1gInSc6	film
Hvězda	Hvězda	k1gMnSc1	Hvězda
padá	padat	k5eAaImIp3nS	padat
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
brněnským	brněnský	k2eAgNnSc7d1	brněnské
divadlem	divadlo	k1gNnSc7	divadlo
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc4	píseň
Balada	balada	k1gFnSc1	balada
pro	pro	k7c4	pro
banditu	bandita	k1gMnSc4	bandita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc2	film
Báječná	báječný	k2eAgNnPc1d1	báječné
léta	léto	k1gNnPc1	léto
pod	pod	k7c7	pod
psa	pes	k1gMnSc2	pes
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgNnP	objevit
jeho	jeho	k3xOp3gFnSc4	jeho
píseň	píseň	k1gFnSc4	píseň
Mám	mít	k5eAaImIp1nS	mít
rozprávkový	rozprávkový	k2eAgInSc1d1	rozprávkový
dom	dom	k?	dom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
Voľnosť	Voľnosť	k1gFnSc2	Voľnosť
do	do	k7c2	do
filmu	film	k1gInSc2	film
Cinka	Cinka	k1gFnSc1	Cinka
Panna	Panna	k1gFnSc1	Panna
od	od	k7c2	od
režiséra	režisér	k1gMnSc2	režisér
Dušana	Dušan	k1gMnSc2	Dušan
Rapoše	Rapoš	k1gMnSc2	Rapoš
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
složil	složit	k5eAaPmAgMnS	složit
Václav	Václav	k1gMnSc1	Václav
Patejdl	Patejdl	k1gFnSc2	Patejdl
a	a	k8xC	a
text	text	k1gInSc4	text
napsal	napsat	k5eAaPmAgMnS	napsat
Kamil	Kamil	k1gMnSc1	Kamil
Peteraj	Peteraj	k1gMnSc1	Peteraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Filmy	film	k1gInPc1	film
a	a	k8xC	a
seriály	seriál	k1gInPc1	seriál
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
filmech	film	k1gInPc6	film
a	a	k8xC	a
seriálech	seriál	k1gInPc6	seriál
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
zatím	zatím	k6eAd1	zatím
nepojmenovaný	pojmenovaný	k2eNgInSc4d1	Nepojmenovaný
dokument	dokument	k1gInSc4	dokument
o	o	k7c6	o
Karlu	Karel	k1gMnSc6	Karel
Gottovi	Gott	k1gMnSc6	Gott
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
7.	[number]	k4	7.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
s	s	k7c7	s
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
partnerkou	partnerka	k1gFnSc7	partnerka
Ivanou	Ivana	k1gFnSc7	Ivana
Macháčkovou	Macháčková	k1gFnSc7	Macháčková
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Charlottu	Charlotta	k1gFnSc4	Charlotta
Ellu	Ellus	k1gInSc2	Ellus
a	a	k8xC	a
od	od	k7c2	od
28.	[number]	k4	28.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
druhou	druhý	k4xOgFnSc4	druhý
dceru	dcera	k1gFnSc4	dcera
Nelly	Nella	k1gMnSc2	Nella
Sofii	Sofia	k1gFnSc6	Sofia
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
předchozích	předchozí	k2eAgInPc2d1	předchozí
vztahů	vztah	k1gInPc2	vztah
má	mít	k5eAaImIp3nS	mít
dcery	dcera	k1gFnPc4	dcera
Dominiku	Dominik	k1gMnSc6	Dominik
a	a	k8xC	a
Lucii	Lucie	k1gFnSc6	Lucie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8.	[number]	k4	8.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
dědečkem	dědeček	k1gMnSc7	dědeček
syna	syn	k1gMnSc2	syn
dcery	dcera	k1gFnSc2	dcera
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vztahy	vztah	k1gInPc1	vztah
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
</s>
</p>
<p>
</p>
<p>
<s>
Ivana	Ivana	k1gFnSc1	Ivana
Gottová	Gottová	k1gFnSc1	Gottová
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Macháčková	Macháčková	k1gFnSc1	Macháčková
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
20.	[number]	k4	20.
ledna	leden	k1gInSc2	leden
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
–	–	k?	–
manželka	manželka	k1gFnSc1	manželka
od	od	k7c2	od
7.	[number]	k4	7.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP	Charlotte
Ella	Ell	k2eAgFnSc1d1	Ella
Gottová	Gottová	k1gFnSc1	Gottová
(	(	kIx(	(
<g/>
*	*	kIx~	*
30.	[number]	k4	30.
duben	duben	k1gInSc1	duben
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nelly	Nella	k1gFnPc1	Nella
Sofie	Sofia	k1gFnSc2	Sofia
Gottová	Gottová	k1gFnSc1	Gottová
(	(	kIx(	(
<g/>
*	*	kIx~	*
28.	[number]	k4	28.
květen	květen	k1gInSc1	květen
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Antonie	Antonie	k1gFnSc1	Antonie
Zacpalová	Zacpalová	k1gFnSc1	Zacpalová
</s>
</p>
<p>
<s>
Dominika	Dominik	k1gMnSc4	Dominik
Gottová	Gottová	k1gFnSc1	Gottová
(	(	kIx(	(
<g/>
*	*	kIx~	*
25.	[number]	k4	25.
červen	červen	k1gInSc1	červen
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Iveta	Iveta	k1gFnSc1	Iveta
Kolářová	Kolářová	k1gFnSc1	Kolářová
(	(	kIx(	(
<g/>
*	*	kIx~	*
14.	[number]	k4	14.
duben	duben	k1gInSc1	duben
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
Kovaříková	Kovaříková	k1gFnSc1	Kovaříková
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Kolářová	Kolářová	k1gFnSc1	Kolářová
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
–	–	k?	–
manžel	manžel	k1gMnSc1	manžel
Jan	Jan	k1gMnSc1	Jan
Kovařík	Kovařík	k1gMnSc1	Kovařík
od	od	k7c2	od
3.	[number]	k4	3.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kovařík	Kovařík	k1gMnSc1	Kovařík
(	(	kIx(	(
<g/>
*	*	kIx~	*
11.	[number]	k4	11.
květen	květen	k1gInSc1	květen
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kovařík	Kovařík	k1gMnSc1	Kovařík
(	(	kIx(	(
<g/>
*	*	kIx~	*
6.	[number]	k4	6.
únor	únor	k1gInSc1	únor
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
režimem	režim	k1gInSc7	režim
a	a	k8xC	a
kauza	kauza	k1gFnSc1	kauza
s	s	k7c7	s
psychiatrem	psychiatr	k1gMnSc7	psychiatr
==	==	k?	==
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
jako	jako	k8xS	jako
nezletilý	nezletilý	k1gMnSc1	nezletilý
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
spolužákem	spolužák	k1gMnSc7	spolužák
Oldou	Olda	k1gMnSc7	Olda
Bukovanským	Bukovanský	k2eAgMnSc7d1	Bukovanský
pokusili	pokusit	k5eAaPmAgMnP	pokusit
emigrovat	emigrovat	k5eAaBmF	emigrovat
přes	přes	k7c4	přes
NDR	NDR	kA	NDR
na	na	k7c4	na
kapitalistický	kapitalistický	k2eAgInSc4d1	kapitalistický
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Vypracovali	vypracovat	k5eAaPmAgMnP	vypracovat
emigrační	emigrační	k2eAgInSc4d1	emigrační
plán	plán	k1gInSc4	plán
pod	pod	k7c7	pod
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Sever	sever	k1gInSc4	sever
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plížení	plížení	k1gNnSc6	plížení
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Cínovec	Cínovec	k1gInSc1	Cínovec
–	–	k?	–
Zinnwald	Zinnwalda	k1gFnPc2	Zinnwalda
však	však	k9	však
byli	být	k5eAaImAgMnP	být
zadrženi	zadržet	k5eAaPmNgMnP	zadržet
orgány	orgán	k1gInPc7	orgán
SNB	SNB	kA	SNB
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dostupných	dostupný	k2eAgInPc2d1	dostupný
zdrojů	zdroj	k1gInPc2	zdroj
nelze	lze	k6eNd1	lze
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tento	tento	k3xDgInSc1	tento
případ	případ	k1gInSc1	případ
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
agent	agent	k1gMnSc1	agent
československé	československý	k2eAgFnSc2d1	Československá
rozvědky	rozvědka	k1gFnSc2	rozvědka
Josef	Josef	k1gMnSc1	Josef
Frolík	Frolík	k1gMnSc1	Frolík
pronesl	pronést	k5eAaPmAgMnS	pronést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Senátu	senát	k1gInSc6	senát
projev	projev	k1gInSc1	projev
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
publikovaný	publikovaný	k2eAgInSc1d1	publikovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
populární	populární	k2eAgMnSc1d1	populární
československý	československý	k2eAgMnSc1d1	československý
zpěvák	zpěvák	k1gMnSc1	zpěvák
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
je	být	k5eAaImIp3nS	být
agentem	agent	k1gMnSc7	agent
StB	StB	k1gMnSc7	StB
<g/>
,	,	kIx,	,
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
naverbovat	naverbovat	k5eAaPmF	naverbovat
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kompromitujících	kompromitující	k2eAgFnPc2d1	kompromitující
skutečností	skutečnost	k1gFnPc2	skutečnost
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
usvědčení	usvědčení	k1gNnSc1	usvědčení
z	z	k7c2	z
exhibicionismu	exhibicionismus	k1gInSc2	exhibicionismus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
server	server	k1gInSc1	server
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
koluje	kolovat	k5eAaImIp3nS	kolovat
po	po	k7c6	po
internetu	internet	k1gInSc6	internet
sken	sken	k1gMnSc1	sken
údajného	údajný	k2eAgInSc2d1	údajný
policejního	policejní	k2eAgInSc2d1	policejní
protokolu	protokol	k1gInSc2	protokol
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
popisujícího	popisující	k2eAgInSc2d1	popisující
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
23letý	23letý	k2eAgMnSc1d1	23letý
konzervatorista	konzervatorista	k1gMnSc1	konzervatorista
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
8.	[number]	k4	8.
června	červen	k1gInSc2	červen
1962	[number]	k4	1962
kolem	kolem	k7c2	kolem
15.	[number]	k4	15.
hodiny	hodina	k1gFnSc2	hodina
ve	v	k7c6	v
voze	vůz	k1gInSc6	vůz
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
linky	linka	k1gFnSc2	linka
č	č	k0	č
<g/>
.	.	kIx.	.
3	[number]	k4	3
přistižen	přistihnout	k5eAaPmNgMnS	přistihnout
při	při	k7c6	při
veřejném	veřejný	k2eAgNnSc6d1	veřejné
onanování	onanování	k1gNnSc6	onanování
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
slečny	slečna	k1gFnPc4	slečna
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
zastávce	zastávka	k1gFnSc6	zastávka
Pelc-Tyrolka	Pelc-Tyrolka	k1gFnSc1	Pelc-Tyrolka
byl	být	k5eAaImAgInS	být
proto	proto	k6eAd1	proto
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
z	z	k7c2	z
přepravy	přeprava	k1gFnSc2	přeprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témž	týž	k3xTgInSc6	týž
protokolu	protokol	k1gInSc6	protokol
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
exhibicionismu	exhibicionismus	k1gInSc2	exhibicionismus
byl	být	k5eAaImAgMnS	být
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
vyšetřován	vyšetřován	k2eAgMnSc1d1	vyšetřován
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
k	k	k7c3	k
činu	čin	k1gInSc3	čin
přiznal	přiznat	k5eAaPmAgMnS	přiznat
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
již	již	k6eAd1	již
vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
léčí	léčit	k5eAaImIp3nP	léčit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
veřejnosti	veřejnost	k1gFnSc2	veřejnost
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokument	dokument	k1gInSc1	dokument
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
až	až	k9	až
v	v	k7c6	v
70.	[number]	k4	70.
letech	let	k1gInPc6	let
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
chtěla	chtít	k5eAaImAgFnS	chtít
donutit	donutit	k5eAaPmF	donutit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
k	k	k7c3	k
diskusím	diskuse	k1gFnPc3	diskuse
o	o	k7c6	o
pravosti	pravost	k1gFnSc6	pravost
dokumentu	dokument	k1gInSc2	dokument
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2009	[number]	k4	2009
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
server	server	k1gInSc1	server
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
i	i	k9	i
další	další	k2eAgNnPc4d1	další
média	médium	k1gNnPc4	médium
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
spisu	spis	k1gInSc6	spis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
o	o	k7c6	o
Karlu	Karel	k1gMnSc3	Karel
Gottovi	Gott	k1gMnSc3	Gott
vedla	vést	k5eAaImAgFnS	vést
Státní	státní	k2eAgFnSc4d1	státní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
společně	společně	k6eAd1	společně
s	s	k7c7	s
bratry	bratr	k1gMnPc7	bratr
Štaidlovými	Štaidlový	k2eAgMnPc7d1	Štaidlový
pobývali	pobývat	k5eAaImAgMnP	pobývat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
pracovně	pracovně	k6eAd1	pracovně
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
po	po	k7c6	po
vypršení	vypršení	k1gNnSc6	vypršení
platnosti	platnost	k1gFnSc2	platnost
víza	vízo	k1gNnSc2	vízo
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1971	[number]	k4	1971
se	se	k3xPyFc4	se
však	však	k9	však
nevrátili	vrátit	k5eNaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dochované	dochovaný	k2eAgFnSc2d1	dochovaná
složky	složka	k1gFnSc2	složka
StB	StB	k1gMnSc2	StB
poté	poté	k6eAd1	poté
Státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
provedla	provést	k5eAaPmAgFnS	provést
prohlídku	prohlídka	k1gFnSc4	prohlídka
jeho	jeho	k3xOp3gFnSc2	jeho
vily	vila	k1gFnSc2	vila
v	v	k7c6	v
Jevanech	Jevan	k1gInPc6	Jevan
a	a	k8xC	a
do	do	k7c2	do
spisu	spis	k1gInSc2	spis
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gott	Gott	k1gInSc1	Gott
svým	svůj	k3xOyFgInSc7	svůj
postupem	postup	k1gInSc7	postup
dovedl	dovést	k5eAaPmAgMnS	dovést
k	k	k7c3	k
trestné	trestný	k2eAgFnSc3d1	trestná
činnosti	činnost	k1gFnSc3	činnost
i	i	k9	i
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	s	k7c7	s
převodem	převod	k1gInSc7	převod
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
finančních	finanční	k2eAgFnPc2d1	finanční
částek	částka	k1gFnPc2	částka
prakticky	prakticky	k6eAd1	prakticky
dopustili	dopustit	k5eAaPmAgMnP	dopustit
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
nadržování	nadržování	k1gNnSc1	nadržování
<g/>
.	.	kIx.	.
</s>
<s>
StB	StB	k?	StB
vyslechla	vyslechnout	k5eAaPmAgFnS	vyslechnout
Gottovy	Gottův	k2eAgMnPc4d1	Gottův
rodiče	rodič	k1gMnPc4	rodič
a	a	k8xC	a
kolem	kolem	k7c2	kolem
dvaceti	dvacet	k4xCc2	dvacet
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
známých	známý	k1gMnPc2	známý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Gottových	Gottův	k2eAgMnPc2d1	Gottův
rodičů	rodič	k1gMnPc2	rodič
se	se	k3xPyFc4	se
StB	StB	k1gMnPc1	StB
při	při	k7c6	při
výslechu	výslech	k1gInSc6	výslech
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
i	i	k9	i
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
psychiatrem	psychiatr	k1gMnSc7	psychiatr
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Dytrychem	Dytrych	k1gMnSc7	Dytrych
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
proto	proto	k6eAd1	proto
také	také	k9	také
vyslechla	vyslechnout	k5eAaPmAgFnS	vyslechnout
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dochovaného	dochovaný	k2eAgInSc2d1	dochovaný
protokolu	protokol	k1gInSc2	protokol
Dytrych	Dytrych	k1gMnSc1	Dytrych
při	při	k7c6	při
výslechu	výslech	k1gInSc6	výslech
11.	[number]	k4	11.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
zmínil	zmínit	k5eAaPmAgInS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Karla	Karel	k1gMnSc4	Karel
Gotta	Gott	k1gMnSc4	Gott
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
"	"	kIx"	"
<g/>
sexuální	sexuální	k2eAgFnSc4d1	sexuální
úchylku	úchylka	k1gFnSc4	úchylka
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
trestného	trestný	k2eAgNnSc2d1	trestné
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
diskusí	diskuse	k1gFnSc7	diskuse
s	s	k7c7	s
Gottovými	Gottův	k2eAgMnPc7d1	Gottův
rodiči	rodič	k1gMnPc7	rodič
docílil	docílit	k5eAaPmAgMnS	docílit
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvedl	uvést	k5eAaPmAgMnS	uvést
Gotta	Gott	k1gMnSc4	Gott
na	na	k7c4	na
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
viděl	vidět	k5eAaImAgMnS	vidět
jako	jako	k9	jako
jediné	jediný	k2eAgNnSc4d1	jediné
východisko	východisko	k1gNnSc4	východisko
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
Gotta	Gott	k1gMnSc4	Gott
<g/>
.	.	kIx.	.
</s>
<s>
Psychiatr	psychiatr	k1gMnSc1	psychiatr
Dytrych	Dytrych	k1gMnSc1	Dytrych
byl	být	k5eAaImAgMnS	být
veden	vést	k5eAaImNgMnS	vést
jako	jako	k8xS	jako
agent	agent	k1gMnSc1	agent
StB	StB	k1gMnPc1	StB
s	s	k7c7	s
krycími	krycí	k2eAgNnPc7d1	krycí
jmény	jméno	k1gNnPc7	jméno
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Tantal-1	Tantal-1	k1gMnSc1	Tantal-1
<g/>
.	.	kIx.	.
<g/>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
novinářům	novinář	k1gMnPc3	novinář
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
ČKD	ČKD	kA	ČKD
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
s	s	k7c7	s
doktorem	doktor	k1gMnSc7	doktor
Dytrychem	Dytrych	k1gMnSc7	Dytrych
znal	znát	k5eAaImAgMnS	znát
a	a	k8xC	a
radil	radit	k5eAaImAgMnS	radit
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
o	o	k7c6	o
osobních	osobní	k2eAgFnPc6d1	osobní
věcech	věc	k1gFnPc6	věc
a	a	k8xC	a
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
tento	tento	k3xDgInSc4	tento
lékař	lékař	k1gMnSc1	lékař
diagnózou	diagnóza	k1gFnSc7	diagnóza
pomohl	pomoct	k5eAaPmAgMnS	pomoct
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
z	z	k7c2	z
fabriky	fabrika	k1gFnSc2	fabrika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebylo	být	k5eNaImAgNnS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Gotta	Gott	k1gMnSc2	Gott
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
jediná	jediný	k2eAgFnSc1d1	jediná
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
od	od	k7c2	od
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Gott	Gott	k1gMnSc1	Gott
novinářům	novinář	k1gMnPc3	novinář
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
nevadí	vadit	k5eNaImIp3nS	vadit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dytrych	Dytrych	k1gMnSc1	Dytrych
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
záležitosti	záležitost	k1gFnSc6	záležitost
před	před	k7c7	před
tajnou	tajný	k2eAgFnSc7d1	tajná
policií	policie	k1gFnSc7	policie
mluvil	mluvit	k5eAaImAgMnS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
údajnou	údajný	k2eAgFnSc4d1	údajná
úchylku	úchylka	k1gFnSc4	úchylka
označil	označit	k5eAaPmAgMnS	označit
Gott	Gott	k1gMnSc1	Gott
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
za	za	k7c4	za
fiktivní	fiktivní	k2eAgInPc4d1	fiktivní
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
psychickou	psychický	k2eAgFnSc7d1	psychická
chorobou	choroba	k1gFnSc7	choroba
nikdy	nikdy	k6eAd1	nikdy
neléčil	léčit	k5eNaImAgInS	léčit
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
mohly	moct	k5eAaImAgInP	moct
tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k8xC	jako
snadný	snadný	k2eAgInSc4d1	snadný
nástroj	nástroj	k1gInSc4	nástroj
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vydírání	vydírání	k1gNnSc3	vydírání
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
úchylce	úchylka	k1gFnSc6	úchylka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
fiktivní	fiktivní	k2eAgInSc4d1	fiktivní
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nemohla	moct	k5eNaImAgFnS	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vydírání	vydírání	k1gNnSc3	vydírání
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
(	(	kIx(	(
<g/>
StB	StB	k1gMnPc1	StB
<g/>
)	)	kIx)	)
ho	on	k3xPp3gNnSc4	on
nikdy	nikdy	k6eAd1	nikdy
nevydírali	vydírat	k5eNaImAgMnP	vydírat
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
nic	nic	k3yNnSc1	nic
nechtěli	chtít	k5eNaImAgMnP	chtít
a	a	k8xC	a
neprojevovali	projevovat	k5eNaImAgMnP	projevovat
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
<g />
.	.	kIx.	.
</s>
<s>
jim	on	k3xPp3gMnPc3	on
mohl	moct	k5eAaImAgInS	moct
nějak	nějak	k6eAd1	nějak
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
<g/>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
údajně	údajně	k6eAd1	údajně
napsal	napsat	k5eAaBmAgInS	napsat
komunistickému	komunistický	k2eAgMnSc3d1	komunistický
generálnímu	generální	k2eAgMnSc3d1	generální
tajemníkovi	tajemník	k1gMnSc3	tajemník
Gustávu	Gustáv	k1gMnSc3	Gustáv
Husákovi	Husák	k1gMnSc3	Husák
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
shovívavost	shovívavost	k1gFnSc4	shovívavost
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
však	však	k9	však
pravost	pravost	k1gFnSc4	pravost
zveřejněné	zveřejněný	k2eAgFnSc2d1	zveřejněná
podoby	podoba	k1gFnSc2	podoba
dopisu	dopis	k1gInSc2	dopis
popřel	popřít	k5eAaPmAgInS	popřít
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
s	s	k7c7	s
Husákovým	Husákův	k2eAgInSc7d1	Husákův
příslibem	příslib	k1gInSc7	příslib
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
trestán	trestat	k5eAaImNgMnS	trestat
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
dál	daleko	k6eAd2	daleko
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c4	na
zahraniční	zahraniční	k2eAgNnSc4d1	zahraniční
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
a	a	k8xC	a
neexistuje	existovat	k5eNaImIp3nS	existovat
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
něco	něco	k6eAd1	něco
musel	muset	k5eAaImAgInS	muset
slíbit	slíbit	k5eAaPmF	slíbit
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
do	do	k7c2	do
role	role	k1gFnSc2	role
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
vůdčích	vůdčí	k2eAgFnPc2d1	vůdčí
postav	postava	k1gFnPc2	postava
Anticharty	anticharta	k1gFnSc2	anticharta
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
zasloužilým	zasloužilý	k2eAgMnSc7d1	zasloužilý
umělcem	umělec	k1gMnSc7	umělec
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
národním	národní	k2eAgMnSc7d1	národní
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nepodepsal	podepsat	k5eNaPmAgMnS	podepsat
k	k	k7c3	k
režimu	režim	k1gInSc6	režim
kritickou	kritický	k2eAgFnSc4d1	kritická
petici	petice	k1gFnSc4	petice
Několik	několik	k4yIc4	několik
vět	věta	k1gFnPc2	věta
<g/>
.	.	kIx.	.
<g/>
Naopak	naopak	k6eAd1	naopak
například	například	k6eAd1	například
protlačil	protlačit	k5eAaPmAgMnS	protlačit
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
album	album	k1gNnSc4	album
písně	píseň	k1gFnSc2	píseň
Jiřího	Jiří	k1gMnSc2	Jiří
Suchého	Suchý	k1gMnSc2	Suchý
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
některé	některý	k3yIgFnPc4	některý
Suchého	Suchého	k2eAgFnPc4d1	Suchého
písně	píseň	k1gFnPc4	píseň
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
zpívat	zpívat	k5eAaImF	zpívat
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
od	od	k7c2	od
Suchého	Suchý	k1gMnSc2	Suchý
desku	deska	k1gFnSc4	deska
pokřtít	pokřtít	k5eAaPmF	pokřtít
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
televize	televize	k1gFnSc1	televize
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
křest	křest	k1gInSc4	křest
desky	deska	k1gFnSc2	deska
přenášet	přenášet	k5eAaImF	přenášet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
archivech	archiv	k1gInPc6	archiv
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
též	též	k9	též
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Gott	Gott	k1gInSc1	Gott
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
obořil	obořit	k5eAaPmAgInS	obořit
na	na	k7c4	na
muže	muž	k1gMnPc4	muž
s	s	k7c7	s
odznakem	odznak	k1gInSc7	odznak
Lenina	Lenin	k1gMnSc2	Lenin
na	na	k7c6	na
klopě	klopa	k1gFnSc6	klopa
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
mu	on	k3xPp3gMnSc3	on
spílal	spílat	k5eAaImAgMnS	spílat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
hlásit	hlásit	k5eAaImF	hlásit
ke	k	k7c3	k
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
tanky	tank	k1gInPc1	tank
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1989	[number]	k4	1989
Gott	Gott	k1gMnSc1	Gott
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
odsoudit	odsoudit	k5eAaPmF	odsoudit
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
palachovské	palachovský	k2eAgFnSc2d1	palachovská
demonstrace	demonstrace	k1gFnSc2	demonstrace
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
zazpíval	zazpívat	k5eAaPmAgInS	zazpívat
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
hymnu	hymna	k1gFnSc4	hymna
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
jiní	jiný	k2eAgMnPc1d1	jiný
režimem	režim	k1gInSc7	režim
uznávaní	uznávaný	k2eAgMnPc1d1	uznávaný
zpěváci	zpěvák	k1gMnPc1	zpěvák
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vidí	vidět	k5eAaImIp3nS	vidět
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
jako	jako	k9	jako
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
a	a	k8xC	a
rozporuplnou	rozporuplný	k2eAgFnSc4d1	rozporuplná
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
uctívaná	uctívaný	k2eAgFnSc1d1	uctívaná
spíše	spíše	k9	spíše
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
než	než	k8xS	než
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
<g/>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
spisů	spis	k1gInPc2	spis
StB	StB	k1gMnPc2	StB
nikdy	nikdy	k6eAd1	nikdy
nenahlédl	nahlédnout	k5eNaPmAgMnS	nahlédnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
úplně	úplně	k6eAd1	úplně
ztratil	ztratit	k5eAaPmAgMnS	ztratit
iluze	iluze	k1gFnPc4	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Zveřejňování	zveřejňování	k1gNnSc1	zveřejňování
spisů	spis	k1gInPc2	spis
StB	StB	k1gFnSc2	StB
označil	označit	k5eAaPmAgInS	označit
Gott	Gott	k1gInSc1	Gott
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
za	za	k7c4	za
vykonstruovanou	vykonstruovaný	k2eAgFnSc4d1	vykonstruovaná
kauzu	kauza	k1gFnSc4	kauza
a	a	k8xC	a
provokaci	provokace	k1gFnSc4	provokace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
někdo	někdo	k3yInSc1	někdo
po	po	k7c6	po
tolika	tolik	k4yIc6	tolik
letech	léto	k1gNnPc6	léto
zajímal	zajímat	k5eAaImAgMnS	zajímat
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
archívech	archív	k1gInPc6	archív
StB	StB	k1gFnSc2	StB
vedeno	vést	k5eAaImNgNnS	vést
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
vylovil	vylovit	k5eAaPmAgMnS	vylovit
tyto	tento	k3xDgFnPc4	tento
podivné	podivný	k2eAgFnPc4d1	podivná
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Gotta	Gott	k1gInSc2	Gott
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
připravená	připravený	k2eAgFnSc1d1	připravená
provokace	provokace	k1gFnSc1	provokace
<g/>
,	,	kIx,	,
a	a	k8xC	a
jinak	jinak	k6eAd1	jinak
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
neumí	umět	k5eNaImIp3nS	umět
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgFnSc7d1	právní
cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
ale	ale	k9	ale
bránit	bránit	k5eAaImF	bránit
nehodlal	hodlat	k5eNaImAgMnS	hodlat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tím	ten	k3xDgNnSc7	ten
vůbec	vůbec	k9	vůbec
ničeho	nic	k3yNnSc2	nic
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
věnovat	věnovat	k5eAaImF	věnovat
pouze	pouze	k6eAd1	pouze
svojí	svojit	k5eAaImIp3nP	svojit
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
svojí	svojit	k5eAaImIp3nS	svojit
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
nemá	mít	k5eNaImIp3nS	mít
smysl	smysl	k1gInSc1	smysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Polemika	polemika	k1gFnSc1	polemika
klubu	klub	k1gInSc2	klub
skeptiků	skeptik	k1gMnPc2	skeptik
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Jak	jak	k6eAd1	jak
to	ten	k3xDgNnSc4	ten
vidí	vidět	k5eAaImIp3nS	vidět
Gott	Gott	k1gInSc1	Gott
dle	dle	k7c2	dle
Českého	český	k2eAgInSc2d1	český
klubu	klub	k1gInSc2	klub
skeptiků	skeptik	k1gMnPc2	skeptik
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
bral	brát	k5eAaImAgMnS	brát
podvědomě	podvědomě	k6eAd1	podvědomě
lidem	lid	k1gInSc7	lid
energii	energie	k1gFnSc4	energie
ale	ale	k8xC	ale
už	už	k6eAd1	už
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
umí	umět	k5eAaImIp3nS	umět
manipulovat	manipulovat	k5eAaImF	manipulovat
<g/>
,	,	kIx,	,
ví	vědět	k5eAaImIp3nS	vědět
leccos	leccos	k3yInSc4	leccos
o	o	k7c6	o
vyzařování	vyzařování	k1gNnSc6	vyzařování
biotronu	biotron	k1gInSc2	biotron
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
nepodřekl	podřeknout	k5eNaPmAgMnS	podřeknout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
to	ten	k3xDgNnSc4	ten
vlastně	vlastně	k9	vlastně
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
zná	znát	k5eAaImIp3nS	znát
Nostradamovy	Nostradamův	k2eAgFnPc4d1	Nostradamova
předpovědi	předpověď	k1gFnPc4	předpověď
přeložené	přeložený	k2eAgFnPc4d1	přeložená
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
terminologie	terminologie	k1gFnSc2	terminologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gott	Gott	k1gMnSc1	Gott
je	být	k5eAaImIp3nS	být
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc4	svět
řídí	řídit	k5eAaImIp3nP	řídit
"	"	kIx"	"
<g/>
vysoké	vysoký	k2eAgInPc1d1	vysoký
nadstátní	nadstátní	k2eAgInPc1d1	nadstátní
kruhy	kruh	k1gInPc1	kruh
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
ilumináti	iluminát	k1gMnPc1	iluminát
nebo	nebo	k8xC	nebo
osvícenci	osvícenec	k1gMnPc1	osvícenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
svaté	svatý	k2eAgInPc4d1	svatý
řády	řád	k1gInPc4	řád
a	a	k8xC	a
lóže	lóže	k1gFnPc4	lóže
až	až	k9	až
po	po	k7c4	po
mysticko-okultní	mystickokultní	k2eAgFnPc4d1	mysticko-okultní
organizace	organizace	k1gFnPc4	organizace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Varuje	varovat	k5eAaImIp3nS	varovat
před	před	k7c7	před
globalizací	globalizace	k1gFnSc7	globalizace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
přerůst	přerůst	k5eAaPmF	přerůst
až	až	k9	až
v	v	k7c4	v
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
totalitu	totalita	k1gFnSc4	totalita
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zavedením	zavedení	k1gNnSc7	zavedení
jednotné	jednotný	k2eAgFnSc2d1	jednotná
měny	měna	k1gFnSc2	měna
a	a	k8xC	a
kontroly	kontrola	k1gFnSc2	kontrola
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
pomocí	pomocí	k7c2	pomocí
čipů	čip	k1gInPc2	čip
zabudovaných	zabudovaný	k2eAgInPc2d1	zabudovaný
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
Neděle	neděle	k1gFnSc1	neděle
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
z	z	k7c2	z
26.	[number]	k4	26.
října	říjen	k1gInSc2	říjen
2002	[number]	k4	2002
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
planetě	planeta	k1gFnSc6	planeta
přibývají	přibývat	k5eAaImIp3nP	přibývat
lidmi	člověk	k1gMnPc7	člověk
způsobené	způsobený	k2eAgFnSc2d1	způsobená
geofyzikální	geofyzikální	k2eAgFnSc2d1	geofyzikální
anomálie	anomálie	k1gFnSc2	anomálie
<g/>
:	:	kIx,	:
Například	například	k6eAd1	například
že	že	k8xS	že
se	s	k7c7	s
"	"	kIx"	"
<g/>
vibrace	vibrace	k1gFnPc4	vibrace
Země	zem	k1gFnSc2	zem
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
vibracemi	vibrace	k1gFnPc7	vibrace
živých	živý	k2eAgMnPc2d1	živý
organismů	organismus	k1gInPc2	organismus
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
kmitů	kmit	k1gInPc2	kmit
za	za	k7c4	za
vteřinu	vteřina	k1gFnSc4	vteřina
na	na	k7c4	na
jedenáct	jedenáct	k4xCc4	jedenáct
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ztenčování	ztenčování	k1gNnSc1	ztenčování
ozonové	ozonový	k2eAgFnSc2d1	ozonová
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
či	či	k8xC	či
postupné	postupný	k2eAgFnPc1d1	postupná
změny	změna	k1gFnPc1	změna
úhlu	úhel	k1gInSc2	úhel
vychýlení	vychýlení	k1gNnSc2	vychýlení
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Gotta	Gott	k1gInSc2	Gott
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
"	"	kIx"	"
<g/>
výbuchy	výbuch	k1gInPc7	výbuch
vulkánů	vulkán	k1gInPc2	vulkán
pod	pod	k7c7	pod
ledy	led	k1gInPc7	led
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odtržení	odtržení	k1gNnSc3	odtržení
části	část	k1gFnSc2	část
pokrývky	pokrývka	k1gFnSc2	pokrývka
Antarktidy	Antarktida	k1gFnSc2	Antarktida
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
tisíc	tisíc	k4xCgInSc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
něj	on	k3xPp3gMnSc2	on
raněná	raněný	k2eAgFnSc1d1	raněná
nejen	nejen	k6eAd1	nejen
fyzicky	fyzicky	k6eAd1	fyzicky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
její	její	k3xOp3gFnSc1	její
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
oslabená	oslabený	k2eAgFnSc1d1	oslabená
i	i	k9	i
psychicky	psychicky	k6eAd1	psychicky
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
tato	tento	k3xDgNnPc4	tento
vyjádření	vyjádření	k1gNnPc4	vyjádření
udělil	udělit	k5eAaPmAgMnS	udělit
Gottovi	Gott	k1gMnSc3	Gott
Český	český	k2eAgInSc4d1	český
klub	klub	k1gInSc4	klub
skeptiků	skeptik	k1gMnPc2	skeptik
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
bronzový	bronzový	k2eAgInSc4d1	bronzový
Bludný	bludný	k2eAgInSc4d1	bludný
balvan	balvan	k1gInSc4	balvan
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
za	za	k7c7	za
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
vážná	vážný	k2eAgNnPc4d1	vážné
varování	varování	k1gNnPc4	varování
vyjevená	vyjevený	k2eAgNnPc4d1	vyjevené
se	se	k3xPyFc4	se
salonní	salonní	k2eAgFnSc7d1	salonní
nonšalancí	nonšalance	k1gFnSc7	nonšalance
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
předvídání	předvídání	k1gNnSc4	předvídání
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
Gott	Gotta	k1gFnPc2	Gotta
pro	pro	k7c4	pro
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
ničivou	ničivý	k2eAgFnSc4d1	ničivá
tsunami	tsunami	k1gNnPc3	tsunami
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
"	"	kIx"	"
<g/>
stály	stát	k5eAaImAgFnP	stát
nějaké	nějaký	k3yIgFnPc1	nějaký
temné	temný	k2eAgFnPc1d1	temná
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
katastrofa	katastrofa	k1gFnSc1	katastrofa
hodila	hodit	k5eAaPmAgFnS	hodit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
:	:	kIx,	:
91/92	[number]	k4	91/92
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
federální	federální	k2eAgInPc4d1	federální
orgány	orgán	k1gInPc4	orgán
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
–	–	k?	–
<g/>
M.	M.	kA	M.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
<g/>
,	,	kIx,	,
1991.	[number]	k4	1991.
636	[number]	k4	636
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-901103-0-4	[number]	k4	80-901103-0-4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
228.	[number]	k4	228.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yRnSc1	kdo
=	=	kIx~	=
Who	Who	k1gMnPc1	Who
is	is	k?	is
who	who	k?	who
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc4	osobnost
české	český	k2eAgFnSc2d1	Česká
současnosti	současnost	k1gFnSc2	současnost
:	:	kIx,	:
5000	[number]	k4	5000
životopisů	životopis	k1gInPc2	životopis
/	/	kIx~	/
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Třeštík	Třeštík	k1gMnSc1	Třeštík
editor	editor	k1gMnSc1	editor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5.	[number]	k4	5.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yQnSc1	kdo
<g/>
,	,	kIx,	,
2005.	[number]	k4	2005.
775	[number]	k4	775
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-902586-9-7	[number]	k4	80-902586-9-7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
161.	[number]	k4	161.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
–	–	k?	–
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008.	[number]	k4	2008.
823	[number]	k4	823
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7360-796-8	[number]	k4	978-80-7360-796-8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
185.	[number]	k4	185.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999.	[number]	k4	1999.
634	[number]	k4	634
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7185-245-7	[number]	k4	80-7185-245-7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
372	[number]	k4	372
<g/>
–	–	k?	–
<g/>
373	[number]	k4	373
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k2eAgMnSc1d1	Gott
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
na	na	k7c4	na
Kinoboxu.cz	Kinoboxu.cz	k1gInSc4	Kinoboxu.cz
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Impulsy	impuls	k1gInPc4	impuls
Václava	Václav	k1gMnSc4	Václav
Moravce	Moravec	k1gMnSc2	Moravec
</s>
</p>
<p>
<s>
Hlas	hlas	k1gInSc1	hlas
K.	K.	kA	K.
Gotta	Gott	k1gMnSc2	Gott
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
archivu	archiv	k1gInSc6	archiv
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
–	–	k?	–
osoba	osoba	k1gFnSc1	osoba
<g/>
:	:	kIx,	:
Gott	Gott	k1gMnSc1	Gott
Karel	Karel	k1gMnSc1	Karel
</s>
</p>
