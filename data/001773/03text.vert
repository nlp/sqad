<s>
Charles	Charles	k1gMnSc1	Charles
Lutwidge	Lutwidg	k1gFnSc2	Lutwidg
Dodgson	Dodgson	k1gMnSc1	Dodgson
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1832	[number]	k4	1832
v	v	k7c4	v
Daresbury	Daresbur	k1gInPc4	Daresbur
<g/>
,	,	kIx,	,
Cheshire	Cheshir	k1gMnSc5	Cheshir
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1898	[number]	k4	1898
v	v	k7c6	v
Guildfordu	Guildfordo	k1gNnSc6	Guildfordo
<g/>
,	,	kIx,	,
Surrey	Surrey	k1gInPc1	Surrey
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Lewis	Lewis	k1gInSc1	Lewis
Carroll	Carrolla	k1gFnPc2	Carrolla
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
logik	logik	k1gMnSc1	logik
<g/>
,	,	kIx,	,
učenec	učenec	k1gMnSc1	učenec
<g/>
,	,	kIx,	,
anglikánský	anglikánský	k2eAgMnSc1d1	anglikánský
diakon	diakon	k1gMnSc1	diakon
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nejznámější	známý	k2eAgFnSc1d3	nejznámější
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
Alenčina	Alenčin	k2eAgNnPc4d1	Alenčino
dobrodružství	dobrodružství	k1gNnPc4	dobrodružství
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
pokračování	pokračování	k1gNnSc4	pokračování
Za	za	k7c7	za
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
a	a	k8xC	a
co	co	k9	co
tam	tam	k6eAd1	tam
Alenka	Alenka	k1gFnSc1	Alenka
našla	najít	k5eAaPmAgFnS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Pokračování	pokračování	k1gNnSc1	pokračování
Alenčiných	Alenčin	k2eAgInPc2d1	Alenčin
příběhů	příběh	k1gInPc2	příběh
Alenka	Alenka	k1gFnSc1	Alenka
za	za	k7c7	za
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
(	(	kIx(	(
<g/>
Through	Through	k1gInSc1	Through
the	the	k?	the
Looking-Glass	Looking-Glass	k1gInSc1	Looking-Glass
and	and	k?	and
What	What	k1gInSc1	What
Alice	Alice	k1gFnSc1	Alice
Found	Found	k1gMnSc1	Found
There	Ther	k1gMnSc5	Ther
<g/>
)	)	kIx)	)
vychází	vycházet	k5eAaImIp3nS	vycházet
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
např.	např.	kA	např.
nachází	nacházet	k5eAaImIp3nS	nacházet
slavná	slavný	k2eAgFnSc1d1	slavná
báseň	báseň	k1gFnSc1	báseň
Žvahlav	Žvahlav	k1gMnSc1	Žvahlav
(	(	kIx(	(
<g/>
Tlachapoud	Tlachapoud	k1gMnSc1	Tlachapoud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jeho	on	k3xPp3gInSc4	on
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
hravost	hravost	k1gFnSc4	hravost
a	a	k8xC	a
fantazii	fantazie	k1gFnSc4	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
třetím	třetí	k4xOgInSc7	třetí
nejproslulejším	proslulý	k2eAgInSc7d3	nejproslulejší
dílem	díl	k1gInSc7	díl
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
Alenkách	Alenka	k1gFnPc6	Alenka
je	být	k5eAaImIp3nS	být
alegorická	alegorický	k2eAgFnSc1d1	alegorická
báseň	báseň	k1gFnSc1	báseň
The	The	k1gFnSc2	The
Hunting	Hunting	k1gInSc1	Hunting
of	of	k?	of
the	the	k?	the
Snark	Snark	k1gInSc1	Snark
(	(	kIx(	(
<g/>
Lovení	lovení	k1gNnSc1	lovení
Snárka	Snárk	k1gInSc2	Snárk
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
Carrollovým	Carrollův	k2eAgInSc7d1	Carrollův
románem	román	k1gInSc7	román
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
Sylvie	Sylvie	k1gFnSc2	Sylvie
and	and	k?	and
Bruno	Bruno	k1gMnSc1	Bruno
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
publikoval	publikovat	k5eAaBmAgMnS	publikovat
odborné	odborný	k2eAgInPc4d1	odborný
texty	text	k1gInPc4	text
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
Dodgsonů	Dodgson	k1gMnPc2	Dodgson
pocházela	pocházet	k5eAaImAgFnS	pocházet
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Předkové	předek	k1gMnPc1	předek
Charlese	Charles	k1gMnSc2	Charles
Dodgsona	Dodgson	k1gMnSc2	Dodgson
byli	být	k5eAaImAgMnP	být
většinou	většinou	k6eAd1	většinou
lidé	člověk	k1gMnPc1	člověk
konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
a	a	k8xC	a
činní	činný	k2eAgMnPc1d1	činný
v	v	k7c6	v
anglikánské	anglikánský	k2eAgFnSc6d1	anglikánská
církvi	církev	k1gFnSc6	církev
nebo	nebo	k8xC	nebo
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
pradědeček	pradědeček	k1gMnSc1	pradědeček
Charles	Charles	k1gMnSc1	Charles
Dodgson	Dodgson	k1gMnSc1	Dodgson
byl	být	k5eAaImAgMnS	být
anglikánským	anglikánský	k2eAgMnSc7d1	anglikánský
biskupem	biskup	k1gMnSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Dědeček	dědeček	k1gMnSc1	dědeček
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
důstojníkem	důstojník	k1gMnSc7	důstojník
a	a	k8xC	a
zahynul	zahynout	k5eAaPmAgMnS	zahynout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
synové	syn	k1gMnPc1	syn
byli	být	k5eAaImAgMnP	být
ještě	ještě	k6eAd1	ještě
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgMnPc1d2	starší
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
synů	syn	k1gMnPc2	syn
-	-	kIx~	-
opět	opět	k6eAd1	opět
Charles	Charles	k1gMnSc1	Charles
-	-	kIx~	-
byl	být	k5eAaImAgMnS	být
Carrollův	Carrollův	k2eAgMnSc1d1	Carrollův
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Navrátil	Navrátil	k1gMnSc1	Navrátil
se	se	k3xPyFc4	se
k	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
rodinné	rodinný	k2eAgFnSc3d1	rodinná
profesi	profes	k1gFnSc3	profes
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
duchovním	duchovní	k2eAgMnSc7d1	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
Rugby	rugby	k1gNnSc2	rugby
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
potom	potom	k6eAd1	potom
kolej	kolej	k1gFnSc1	kolej
Christ	Christ	k1gMnSc1	Christ
Church	Church	k1gMnSc1	Church
na	na	k7c6	na
Oxfordské	oxfordský	k2eAgFnSc6d1	Oxfordská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
talent	talent	k1gInSc1	talent
pro	pro	k7c4	pro
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
dva	dva	k4xCgInPc4	dva
bakalářské	bakalářský	k2eAgInPc4d1	bakalářský
tituly	titul	k1gInPc4	titul
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
dobrý	dobrý	k2eAgInSc4d1	dobrý
začátek	začátek	k1gInSc4	začátek
vynikající	vynikající	k2eAgFnSc2d1	vynikající
akademické	akademický	k2eAgFnSc2d1	akademická
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
oženil	oženit	k5eAaPmAgInS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestřenicí	sestřenice	k1gFnSc7	sestřenice
a	a	k8xC	a
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
jako	jako	k8xC	jako
venkovský	venkovský	k2eAgMnSc1d1	venkovský
kněz	kněz	k1gMnSc1	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Charlesův	Charlesův	k2eAgMnSc1d1	Charlesův
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
duchovní	duchovní	k2eAgFnSc2d1	duchovní
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Zapojoval	zapojovat	k5eAaImAgMnS	zapojovat
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
významně	významně	k6eAd1	významně
<g/>
)	)	kIx)	)
do	do	k7c2	do
ostrých	ostrý	k2eAgFnPc2d1	ostrá
náboženských	náboženský	k2eAgFnPc2d1	náboženská
polemik	polemika	k1gFnPc2	polemika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rozdělovaly	rozdělovat	k5eAaImAgFnP	rozdělovat
anglikánskou	anglikánský	k2eAgFnSc4d1	anglikánská
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
anglikánské	anglikánský	k2eAgFnSc3d1	anglikánská
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vést	vést	k5eAaImF	vést
i	i	k9	i
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Charles	Charles	k1gMnSc1	Charles
si	se	k3xPyFc3	se
ale	ale	k9	ale
k	k	k7c3	k
otcovým	otcův	k2eAgFnPc3d1	otcova
hodnotám	hodnota	k1gFnPc3	hodnota
a	a	k8xC	a
celé	celý	k2eAgFnSc3d1	celá
anglikánské	anglikánský	k2eAgFnSc3d1	anglikánská
církvi	církev	k1gFnSc3	církev
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
rozporuplný	rozporuplný	k2eAgInSc1d1	rozporuplný
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Dodgson	Dodgson	k1gMnSc1	Dodgson
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
faře	fara	k1gFnSc6	fara
v	v	k7c4	v
Daresbury	Daresbur	k1gInPc4	Daresbur
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Cheshire	Cheshir	k1gInSc5	Cheshir
jako	jako	k8xC	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
chlapec	chlapec	k1gMnSc1	chlapec
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
už	už	k9	už
třetí	třetí	k4xOgNnSc4	třetí
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
osm	osm	k4xCc1	osm
dalších	další	k2eAgFnPc2d1	další
dětí	dítě	k1gFnPc2	dítě
<g/>
;	;	kIx,	;
všechny	všechen	k3xTgFnPc1	všechen
(	(	kIx(	(
<g/>
sedm	sedm	k4xCc4	sedm
dívek	dívka	k1gFnPc2	dívka
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc1	čtyři
chlapci	chlapec	k1gMnPc1	chlapec
<g/>
)	)	kIx)	)
přežily	přežít	k5eAaPmAgFnP	přežít
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
<g/>
,	,	kIx,	,
až	až	k8xS	až
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Charlesovi	Charles	k1gMnSc6	Charles
jedenáct	jedenáct	k4xCc1	jedenáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
přeložen	přeložit	k5eAaPmNgMnS	přeložit
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
Croft-on-Tees	Croftn-Teesa	k1gFnPc2	Croft-on-Teesa
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Yorkshire	Yorkshir	k1gMnSc5	Yorkshir
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
na	na	k7c4	na
zdejší	zdejší	k2eAgFnSc4d1	zdejší
prostornou	prostorný	k2eAgFnSc4d1	prostorná
faru	fara	k1gFnSc4	fara
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
jejich	jejich	k3xOp3gInSc7	jejich
domovem	domov	k1gInSc7	domov
příštích	příští	k2eAgFnPc2d1	příští
dvacet	dvacet	k4xCc4	dvacet
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
raného	raný	k2eAgNnSc2d1	rané
období	období	k1gNnSc2	období
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
vzdělávali	vzdělávat	k5eAaImAgMnP	vzdělávat
rodiče	rodič	k1gMnSc4	rodič
Charlese	Charles	k1gMnSc4	Charles
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
"	"	kIx"	"
<g/>
čtenářský	čtenářský	k2eAgInSc1d1	čtenářský
deník	deník	k1gInSc1	deník
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
rodina	rodina	k1gFnSc1	rodina
uchovala	uchovat	k5eAaPmAgFnS	uchovat
<g/>
,	,	kIx,	,
vypovídá	vypovídat	k5eAaImIp3nS	vypovídat
o	o	k7c6	o
předčasně	předčasně	k6eAd1	předčasně
rozvinutém	rozvinutý	k2eAgInSc6d1	rozvinutý
intelektu	intelekt	k1gInSc6	intelekt
-	-	kIx~	-
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
přečetl	přečíst	k5eAaPmAgMnS	přečíst
Bunyanovu	Bunyanův	k2eAgFnSc4d1	Bunyanův
Poutníkovu	poutníkův	k2eAgFnSc4d1	Poutníkova
cestu	cesta	k1gFnSc4	cesta
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
do	do	k7c2	do
světa	svět	k1gInSc2	svět
budoucího	budoucí	k2eAgInSc2d1	budoucí
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gNnSc4	jeho
sourozenci	sourozenec	k1gMnSc3	sourozenec
trpěl	trpět	k5eAaImAgMnS	trpět
koktavostí	koktavost	k1gFnSc7	koktavost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
často	často	k6eAd1	často
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
jeho	jeho	k3xOp3gInSc4	jeho
společenský	společenský	k2eAgInSc4d1	společenský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
poslán	poslán	k2eAgInSc1d1	poslán
na	na	k7c4	na
malou	malý	k2eAgFnSc4d1	malá
soukromou	soukromý	k2eAgFnSc4d1	soukromá
školu	škola	k1gFnSc4	škola
poblíž	poblíž	k7c2	poblíž
Richmondu	Richmond	k1gInSc2	Richmond
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zdál	zdát	k5eAaImAgInS	zdát
být	být	k5eAaImF	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
a	a	k8xC	a
spokojený	spokojený	k2eAgMnSc1d1	spokojený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
ale	ale	k8xC	ale
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
Rugby	rugby	k1gNnSc4	rugby
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
očividně	očividně	k6eAd1	očividně
spokojený	spokojený	k2eAgMnSc1d1	spokojený
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
co	co	k9	co
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nemohu	moct	k5eNaImIp1nS	moct
říci	říct	k5eAaPmF	říct
...	...	k?	...
že	že	k8xS	že
by	by	kYmCp3nS	by
mě	já	k3xPp1nSc4	já
cokoli	cokoli	k3yInSc4	cokoli
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
prožít	prožít	k5eAaPmF	prožít
si	se	k3xPyFc3	se
ty	ten	k3xDgInPc4	ten
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
znovu	znovu	k6eAd1	znovu
<g />
.	.	kIx.	.
</s>
<s>
...	...	k?	...
Mohu	moct	k5eAaImIp1nS	moct
upřímně	upřímně	k6eAd1	upřímně
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdybych	kdyby	kYmCp1nS	kdyby
byl	být	k5eAaImAgMnS	být
býval	bývat	k5eAaImAgMnS	bývat
<g/>
...	...	k?	...
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
klid	klid	k1gInSc4	klid
<g/>
,	,	kIx,	,
těžkosti	těžkost	k1gFnPc4	těžkost
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
snášely	snášet	k5eAaImAgFnP	snášet
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jako	jako	k8xS	jako
žák	žák	k1gMnSc1	žák
ale	ale	k9	ale
vynikal	vynikat	k5eAaImAgMnS	vynikat
s	s	k7c7	s
očividnou	očividný	k2eAgFnSc7d1	očividná
lehkostí	lehkost	k1gFnSc7	lehkost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jsem	být	k5eAaImIp1nS	být
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Rugby	rugby	k1gNnSc2	rugby
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
neučil	učít	k5eNaPmAgMnS	učít
slibnějšího	slibní	k2eAgMnSc4d2	slibní
chlapce	chlapec	k1gMnSc4	chlapec
jeho	jeho	k3xOp3gInSc2	jeho
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
R.	R.	kA	R.
<g/>
B.	B.	kA	B.
Mayor	Mayor	k1gMnSc1	Mayor
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Rugby	rugby	k1gNnSc4	rugby
School	Schoola	k1gFnPc2	Schoola
opustil	opustit	k5eAaPmAgInS	opustit
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
následuje	následovat	k5eAaImIp3nS	následovat
určitá	určitý	k2eAgFnSc1d1	určitá
přestávka	přestávka	k1gFnSc1	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
ale	ale	k8xC	ale
pořádně	pořádně	k6eAd1	pořádně
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
vlastně	vlastně	k9	vlastně
dělal	dělat	k5eAaImAgMnS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
přestávce	přestávka	k1gFnSc6	přestávka
šel	jít	k5eAaImAgMnS	jít
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1851	[number]	k4	1851
do	do	k7c2	do
Oxfordu	Oxford	k1gInSc2	Oxford
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
kolej	kolej	k1gFnSc4	kolej
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
-	-	kIx~	-
Christ	Christ	k1gMnSc1	Christ
Church	Church	k1gMnSc1	Church
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pouhých	pouhý	k2eAgInPc6d1	pouhý
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
musel	muset	k5eAaImAgInS	muset
jet	jet	k5eAaImF	jet
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
ve	v	k7c6	v
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
"	"	kIx"	"
<g/>
zápal	zápal	k1gInSc4	zápal
mozku	mozek	k1gInSc2	mozek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
meningitida	meningitida	k1gFnSc1	meningitida
nebo	nebo	k8xC	nebo
mrtvice	mrtvice	k1gFnSc1	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
jeho	jeho	k3xOp3gFnSc2	jeho
akademické	akademický	k2eAgFnSc2d1	akademická
dráhy	dráha	k1gFnSc2	dráha
se	se	k3xPyFc4	se
střídaly	střídat	k5eAaImAgFnP	střídat
nadšené	nadšený	k2eAgFnPc1d1	nadšená
naděje	naděje	k1gFnPc1	naděje
a	a	k8xC	a
neodolatelné	odolatelný	k2eNgInPc1d1	neodolatelný
zábavy	zábav	k1gInPc1	zábav
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
pracoval	pracovat	k5eAaImAgMnS	pracovat
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
nadaný	nadaný	k2eAgMnSc1d1	nadaný
a	a	k8xC	a
úspěchů	úspěch	k1gInPc2	úspěch
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
snadno	snadno	k6eAd1	snadno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
matematický	matematický	k2eAgInSc1d1	matematický
talent	talent	k1gInSc1	talent
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
zajistil	zajistit	k5eAaPmAgMnS	zajistit
místo	místo	k1gNnSc4	místo
učitele	učitel	k1gMnSc2	učitel
na	na	k7c4	na
Christ	Christ	k1gInSc4	Christ
Church	Churcha	k1gFnPc2	Churcha
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
práce	práce	k1gFnSc1	práce
dobře	dobře	k6eAd1	dobře
placená	placený	k2eAgFnSc1d1	placená
<g/>
,	,	kIx,	,
Dodgsona	Dodgsona	k1gFnSc1	Dodgsona
nudila	nudit	k5eAaImAgFnS	nudit
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
žáků	žák	k1gMnPc2	žák
bylo	být	k5eAaImAgNnS	být
starších	starý	k2eAgMnPc2d2	starší
a	a	k8xC	a
bohatších	bohatý	k2eAgMnPc2d2	bohatší
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
málokoho	málokdo	k3yInSc4	málokdo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
studium	studium	k1gNnSc1	studium
zajímalo	zajímat	k5eAaImAgNnS	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
počáteční	počáteční	k2eAgNnSc4d1	počáteční
znechucení	znechucení	k1gNnSc4	znechucení
ale	ale	k8xC	ale
Dodgson	Dodgson	k1gNnSc4	Dodgson
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
funkcích	funkce	k1gFnPc6	funkce
na	na	k7c4	na
Christ	Christ	k1gInSc4	Christ
Church	Church	k1gMnSc1	Church
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Charles	Charles	k1gMnSc1	Charles
Dodgson	Dodgson	k1gMnSc1	Dodgson
byl	být	k5eAaImAgMnS	být
asi	asi	k9	asi
180	[number]	k4	180
cm	cm	kA	cm
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
a	a	k8xC	a
hezký	hezký	k2eAgMnSc1d1	hezký
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
vlnité	vlnitý	k2eAgInPc4d1	vlnitý
hnědé	hnědý	k2eAgInPc4d1	hnědý
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
modré	modrý	k2eAgFnPc4d1	modrá
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
popisován	popisovat	k5eAaImNgInS	popisovat
jako	jako	k8xS	jako
trochu	trochu	k6eAd1	trochu
asymetrický	asymetrický	k2eAgMnSc1d1	asymetrický
a	a	k8xC	a
říkalo	říkat	k5eAaImAgNnS	říkat
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
poněkud	poněkud	k6eAd1	poněkud
toporně	toporně	k6eAd1	toporně
a	a	k8xC	a
těžkopádně	těžkopádně	k6eAd1	těžkopádně
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
toto	tento	k3xDgNnSc1	tento
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
zapříčiněno	zapříčiněn	k2eAgNnSc1d1	zapříčiněno
nějakým	nějaký	k3yIgInSc7	nějaký
úrazem	úraz	k1gInSc7	úraz
kolena	koleno	k1gNnSc2	koleno
utrpěným	utrpěný	k2eAgInSc7d1	utrpěný
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ještě	ještě	k9	ještě
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc4d1	malé
dítě	dítě	k1gNnSc4	dítě
prodělal	prodělat	k5eAaPmAgInS	prodělat
horečku	horečka	k1gFnSc4	horečka
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
následkem	následek	k1gInSc7	následek
ohluchl	ohluchnout	k5eAaPmAgMnS	ohluchnout
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
ucho	ucho	k1gNnSc4	ucho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
měl	mít	k5eAaImAgMnS	mít
několik	několik	k4yIc1	několik
záchvatů	záchvat	k1gInPc2	záchvat
černého	černý	k2eAgInSc2d1	černý
kašle	kašel	k1gInSc2	kašel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
nejspíše	nejspíše	k9	nejspíše
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
chronickou	chronický	k2eAgFnSc4d1	chronická
slabost	slabost	k1gFnSc4	slabost
hrudníku	hrudník	k1gInSc2	hrudník
v	v	k7c6	v
pozdějších	pozdní	k2eAgInPc6d2	pozdější
letech	let	k1gInPc6	let
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
zjevná	zjevný	k2eAgFnSc1d1	zjevná
vada	vada	k1gFnSc1	vada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
čemu	co	k3yInSc3	co
říkal	říkat	k5eAaImAgMnS	říkat
svoje	svůj	k3xOyFgInPc4	svůj
"	"	kIx"	"
<g/>
rozpaky	rozpak	k1gInPc4	rozpak
<g/>
"	"	kIx"	"
-	-	kIx~	-
koktavost	koktavost	k1gFnSc1	koktavost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
projevovat	projevovat	k5eAaImF	projevovat
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
sužovala	sužovat	k5eAaImAgFnS	sužovat
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Koktavost	koktavost	k1gFnSc1	koktavost
vždy	vždy	k6eAd1	vždy
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
lidem	člověk	k1gMnPc3	člověk
při	při	k7c6	při
vyslovení	vyslovení	k1gNnSc6	vyslovení
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
Lewis	Lewis	k1gFnSc1	Lewis
Carroll	Carrolla	k1gFnPc2	Carrolla
<g/>
"	"	kIx"	"
vybavilo	vybavit	k5eAaPmAgNnS	vybavit
<g/>
.	.	kIx.	.
</s>
<s>
Panuje	panovat	k5eAaImIp3nS	panovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
koktal	koktat	k5eAaImAgMnS	koktat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
dospělých	dospělí	k1gMnPc2	dospělí
a	a	k8xC	a
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
mluvil	mluvit	k5eAaImAgMnS	mluvit
plynně	plynně	k6eAd1	plynně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
této	tento	k3xDgFnSc2	tento
myšlenky	myšlenka	k1gFnSc2	myšlenka
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc4	žádný
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gMnSc4	on
znalo	znát	k5eAaImAgNnS	znát
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gFnSc4	jeho
koktavost	koktavost	k1gFnSc4	koktavost
pamatovalo	pamatovat	k5eAaImAgNnS	pamatovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hodně	hodně	k6eAd1	hodně
dospělých	dospělí	k1gMnPc2	dospělí
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc3	on
nevšimlo	všimnout	k5eNaPmAgNnS	všimnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
mizela	mizet	k5eAaImAgFnS	mizet
a	a	k8xC	a
zase	zase	k9	zase
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
projev	projev	k1gInSc1	projev
strachu	strach	k1gInSc2	strach
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
často	často	k6eAd1	často
říká	říkat	k5eAaImIp3nS	říkat
<g/>
.	.	kIx.	.
</s>
<s>
Dodgson	Dodgson	k1gMnSc1	Dodgson
sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
asi	asi	k9	asi
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
mnohem	mnohem	k6eAd1	mnohem
intenzivněji	intenzivně	k6eAd2	intenzivně
než	než	k8xS	než
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
obtíží	obtíž	k1gFnPc2	obtíž
vyslovit	vyslovit	k5eAaPmF	vyslovit
své	své	k1gNnSc4	své
příjmení	příjmení	k1gNnSc2	příjmení
karikoval	karikovat	k5eAaImAgMnS	karikovat
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
jako	jako	k9	jako
Doda	Doda	k1gFnSc1	Doda
v	v	k7c6	v
Alenčiných	Alenčin	k2eAgNnPc6d1	Alenčino
dobrodružstvích	dobrodružství	k1gNnPc6	dobrodružství
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
těch	ten	k3xDgInPc2	ten
"	"	kIx"	"
<g/>
faktů	fakt	k1gInPc2	fakt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
opakují	opakovat	k5eAaImIp3nP	opakovat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc4	žádný
přesvědčivé	přesvědčivý	k2eAgInPc4d1	přesvědčivý
podklady	podklad	k1gInPc4	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Opravdu	opravdu	k6eAd1	opravdu
o	o	k7c6	o
sobě	se	k3xPyFc3	se
mluvil	mluvit	k5eAaImAgMnS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
dodovi	doda	k1gMnSc6	doda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
koktavostí	koktavost	k1gFnSc7	koktavost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Dodgsona	Dodgsona	k1gFnSc1	Dodgsona
koktavost	koktavost	k1gFnSc1	koktavost
trápila	trápit	k5eAaImAgFnS	trápit
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
rušivá	rušivý	k2eAgFnSc1d1	rušivá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
nemohl	moct	k5eNaImAgMnS	moct
využít	využít	k5eAaPmF	využít
své	svůj	k3xOyFgFnPc4	svůj
jiné	jiný	k2eAgFnPc4d1	jiná
přednosti	přednost	k1gFnPc4	přednost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
běžně	běžně	k6eAd1	běžně
zařizovali	zařizovat	k5eAaImAgMnP	zařizovat
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zábavu	zábava	k1gFnSc4	zábava
a	a	k8xC	a
zpěv	zpěv	k1gInSc4	zpěv
a	a	k8xC	a
recitace	recitace	k1gFnSc1	recitace
byly	být	k5eAaImAgFnP	být
požadovanými	požadovaný	k2eAgFnPc7d1	požadovaná
společenskými	společenský	k2eAgFnPc7d1	společenská
dovednostmi	dovednost	k1gFnPc7	dovednost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Dodgson	Dodgson	k1gMnSc1	Dodgson
dobrým	dobrý	k2eAgMnSc7d1	dobrý
bavičem	bavič	k1gMnSc7	bavič
<g/>
.	.	kIx.	.
</s>
<s>
Zpíval	zpívat	k5eAaImAgMnS	zpívat
docela	docela	k6eAd1	docela
přijatelně	přijatelně	k6eAd1	přijatelně
a	a	k8xC	a
před	před	k7c7	před
publikem	publikum	k1gNnSc7	publikum
se	se	k3xPyFc4	se
nebál	bát	k5eNaImAgMnS	bát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
mistr	mistr	k1gMnSc1	mistr
v	v	k7c4	v
imitování	imitování	k1gNnSc4	imitování
a	a	k8xC	a
vyprávění	vyprávění	k1gNnSc4	vyprávění
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
i	i	k9	i
docela	docela	k6eAd1	docela
dobrý	dobrý	k2eAgInSc1d1	dobrý
v	v	k7c6	v
šarádách	šaráda	k1gFnPc6	šaráda
<g/>
.	.	kIx.	.
</s>
<s>
Dodgson	Dodgson	k1gMnSc1	Dodgson
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
jisté	jistý	k2eAgFnPc4d1	jistá
společenské	společenský	k2eAgFnPc4d1	společenská
ambice	ambice	k1gFnPc4	ambice
a	a	k8xC	a
horlivě	horlivě	k6eAd1	horlivě
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
světě	svět	k1gInSc6	svět
zanechal	zanechat	k5eAaPmAgMnS	zanechat
svůj	svůj	k3xOyFgInSc4	svůj
otisk	otisk	k1gInSc4	otisk
jako	jako	k9	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
nebo	nebo	k8xC	nebo
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
časně	časně	k6eAd1	časně
vydanými	vydaný	k2eAgFnPc7d1	vydaná
pracemi	práce	k1gFnPc7	práce
a	a	k8xC	a
úspěchem	úspěch	k1gInSc7	úspěch
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
Alence	Alenka	k1gFnSc6	Alenka
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
prerafaelitských	prerafaelitský	k2eAgInPc6d1	prerafaelitský
společenských	společenský	k2eAgInPc6d1	společenský
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
akademická	akademický	k2eAgFnSc1d1	akademická
kariéra	kariéra	k1gFnSc1	kariéra
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zamýšlena	zamýšlet	k5eAaImNgFnS	zamýšlet
jen	jen	k9	jen
jako	jako	k9	jako
prozatímní	prozatímní	k2eAgNnSc4d1	prozatímní
řešení	řešení	k1gNnSc4	řešení
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
mnohem	mnohem	k6eAd1	mnohem
zajímavějším	zajímavý	k2eAgInPc3d2	zajímavější
úspěchům	úspěch	k1gInPc3	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Johna	John	k1gMnSc4	John
Ruskina	Ruskin	k1gMnSc4	Ruskin
poprvé	poprvé	k6eAd1	poprvé
potkal	potkat	k5eAaPmAgMnS	potkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
a	a	k8xC	a
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Dante	Dante	k1gMnSc1	Dante
Gabrielem	Gabriel	k1gMnSc7	Gabriel
Rossettim	Rossetti	k1gNnSc7	Rossetti
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
z	z	k7c2	z
ostatních	ostatní	k2eAgMnPc2d1	ostatní
umělců	umělec	k1gMnPc2	umělec
znal	znát	k5eAaImAgMnS	znát
také	také	k9	také
Williama	William	k1gMnSc4	William
Holmana	Holman	k1gMnSc4	Holman
<g/>
,	,	kIx,	,
Johna	John	k1gMnSc4	John
Everetta	Everett	k1gInSc2	Everett
Millaise	Millaise	k1gFnSc1	Millaise
a	a	k8xC	a
Arthura	Arthura	k1gFnSc1	Arthura
Hughese	Hughese	k1gFnSc1	Hughese
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
také	také	k9	také
znal	znát	k5eAaImAgMnS	znát
autora	autor	k1gMnSc4	autor
pohádek	pohádka	k1gFnPc2	pohádka
George	Georg	k1gMnSc4	Georg
MacDonalda	Macdonald	k1gMnSc4	Macdonald
-	-	kIx~	-
právě	právě	k9	právě
nadšená	nadšený	k2eAgFnSc1d1	nadšená
reakce	reakce	k1gFnSc1	reakce
malých	malý	k2eAgFnPc2d1	malá
MacDonaldových	Macdonaldových	k2eAgFnPc2d1	Macdonaldových
dětí	dítě	k1gFnPc2	dítě
na	na	k7c4	na
Alenku	Alenka	k1gFnSc4	Alenka
přiměla	přimět	k5eAaPmAgFnS	přimět
Dodgsona	Dodgsona	k1gFnSc1	Dodgsona
dílo	dílo	k1gNnSc4	dílo
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
věku	věk	k1gInSc6	věk
psal	psát	k5eAaImAgInS	psát
Dodgson	Dodgson	k1gInSc1	Dodgson
poezii	poezie	k1gFnSc4	poezie
a	a	k8xC	a
povídky	povídka	k1gFnPc4	povídka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
úspěchem	úspěch	k1gInSc7	úspěch
posílal	posílat	k5eAaImAgMnS	posílat
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1856	[number]	k4	1856
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
objevila	objevit	k5eAaPmAgNnP	objevit
jak	jak	k6eAd1	jak
v	v	k7c6	v
celostátních	celostátní	k2eAgFnPc6d1	celostátní
publikacích	publikace	k1gFnPc6	publikace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
The	The	k1gFnSc1	The
Comix	Comix	k1gInSc1	Comix
a	a	k8xC	a
The	The	k1gFnSc1	The
Train	Traina	k1gFnPc2	Traina
<g/>
)	)	kIx)	)
tak	tak	k9	tak
v	v	k7c6	v
menších	malý	k2eAgInPc6d2	menší
časopisech	časopis	k1gInPc6	časopis
jako	jako	k8xC	jako
Whitby	Whitba	k1gFnPc1	Whitba
Gazette	Gazett	k1gInSc5	Gazett
a	a	k8xC	a
Oxford	Oxford	k1gInSc4	Oxford
Critic	Critice	k1gFnPc2	Critice
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Dodgsonových	Dodgsonův	k2eAgNnPc2d1	Dodgsonův
děl	dělo	k1gNnPc2	dělo
byla	být	k5eAaImAgFnS	být
humorná	humorný	k2eAgFnSc1d1	humorná
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
satirická	satirický	k2eAgFnSc1d1	satirická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnPc1	jeho
normy	norma	k1gFnPc1	norma
a	a	k8xC	a
ambice	ambice	k1gFnPc1	ambice
byly	být	k5eAaImAgFnP	být
náročné	náročný	k2eAgFnPc1d1	náročná
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nemyslím	myslet	k5eNaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
napsal	napsat	k5eAaPmAgMnS	napsat
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
opravdu	opravdu	k6eAd1	opravdu
stálo	stát	k5eAaImAgNnS	stát
za	za	k7c4	za
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
v	v	k7c6	v
pořádné	pořádný	k2eAgFnSc6d1	pořádná
publikaci	publikace	k1gFnSc6	publikace
(	(	kIx(	(
<g/>
do	do	k7c2	do
čehož	což	k3yQnSc2	což
nepočítám	počítat	k5eNaImIp1nS	počítat
Whitby	Whitba	k1gFnPc1	Whitba
Gazette	Gazett	k1gInSc5	Gazett
a	a	k8xC	a
Oxonian	Oxonian	k1gMnSc1	Oxonian
Advertiser	Advertiser	k1gMnSc1	Advertiser
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebojím	bát	k5eNaImIp1nS	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
bude	být	k5eAaImBp3nS	být
navždy	navždy	k6eAd1	navždy
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
poprvé	poprvé	k6eAd1	poprvé
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ho	on	k3xPp3gMnSc4	on
později	pozdě	k6eAd2	pozdě
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Romantická	romantický	k2eAgFnSc1d1	romantická
báseň	báseň	k1gFnSc1	báseň
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Solitude	Solitud	k1gInSc5	Solitud
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Osamělost	osamělost	k1gFnSc1	osamělost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
The	The	k1gMnSc1	The
Train	Train	k1gMnSc1	Train
jako	jako	k8xS	jako
tvorba	tvorba	k1gFnSc1	tvorba
"	"	kIx"	"
<g/>
Lewise	Lewise	k1gFnSc1	Lewise
Carrolla	Carroll	k1gMnSc2	Carroll
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pseudonym	pseudonym	k1gInSc1	pseudonym
byl	být	k5eAaImAgInS	být
hříčkou	hříčka	k1gFnSc7	hříčka
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
pravým	pravý	k2eAgNnSc7d1	pravé
jménem	jméno	k1gNnSc7	jméno
<g/>
:	:	kIx,	:
Lewis	Lewis	k1gFnSc2	Lewis
je	být	k5eAaImIp3nS	být
poangličtěná	poangličtěný	k2eAgFnSc1d1	poangličtěná
forma	forma	k1gFnSc1	forma
jména	jméno	k1gNnSc2	jméno
Ludovicus	Ludovicus	k1gInSc1	Ludovicus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
latinská	latinský	k2eAgFnSc1d1	Latinská
verze	verze	k1gFnSc1	verze
jména	jméno	k1gNnSc2	jméno
Lutwidge	Lutwidg	k1gInSc2	Lutwidg
<g/>
,	,	kIx,	,
a	a	k8xC	a
Carroll	Carroll	k1gInSc1	Carroll
je	být	k5eAaImIp3nS	být
poangličtěná	poangličtěný	k2eAgFnSc1d1	poangličtěná
verze	verze	k1gFnSc1	verze
jména	jméno	k1gNnSc2	jméno
Carolus	Carolus	k1gInSc1	Carolus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
latinská	latinský	k2eAgFnSc1d1	Latinská
verze	verze	k1gFnSc1	verze
jména	jméno	k1gNnSc2	jméno
Charles	Charles	k1gMnSc1	Charles
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
na	na	k7c4	na
kolej	kolej	k1gFnSc4	kolej
Christ	Christ	k1gMnSc1	Christ
Church	Church	k1gMnSc1	Church
přijel	přijet	k5eAaPmAgMnS	přijet
nový	nový	k2eAgMnSc1d1	nový
děkan	děkan	k1gMnSc1	děkan
Henry	Henry	k1gMnSc1	Henry
Liddell	Liddell	k1gMnSc1	Liddell
a	a	k8xC	a
přivedl	přivést	k5eAaPmAgMnS	přivést
s	s	k7c7	s
sebou	se	k3xPyFc7	se
svou	svůj	k3xOyFgFnSc4	svůj
mladou	mladý	k2eAgFnSc4d1	mladá
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
hráli	hrát	k5eAaImAgMnP	hrát
významné	významný	k2eAgFnPc4d1	významná
role	role	k1gFnPc4	role
v	v	k7c6	v
Dodgsonově	Dodgsonův	k2eAgInSc6d1	Dodgsonův
životě	život	k1gInSc6	život
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
výrazně	výrazně	k6eAd1	výrazně
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
jeho	jeho	k3xOp3gFnSc4	jeho
spisovatelskou	spisovatelský	k2eAgFnSc4d1	spisovatelská
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Dodgson	Dodgson	k1gMnSc1	Dodgson
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
blízkým	blízký	k2eAgMnSc7d1	blízký
přítelem	přítel	k1gMnSc7	přítel
Liddellovy	Liddellův	k2eAgFnSc2d1	Liddellův
manželky	manželka	k1gFnSc2	manželka
Loriny	Lorina	k1gFnSc2	Lorina
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
tří	tři	k4xCgFnPc2	tři
sester	sestra	k1gFnPc2	sestra
<g/>
:	:	kIx,	:
Loriny	Lorina	k1gFnPc1	Lorina
<g/>
,	,	kIx,	,
Edith	Edith	k1gInSc1	Edith
a	a	k8xC	a
Alice	Alice	k1gFnSc1	Alice
Liddellových	Liddellových	k2eAgFnSc1d1	Liddellových
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svou	svůj	k3xOyFgFnSc4	svůj
"	"	kIx"	"
<g/>
Alenku	Alenka	k1gFnSc4	Alenka
<g/>
"	"	kIx"	"
odvodil	odvodit	k5eAaPmAgMnS	odvodit
od	od	k7c2	od
Alice	Alice	k1gFnSc2	Alice
Liddellové	Liddellová	k1gFnSc2	Liddellová
<g/>
.	.	kIx.	.
</s>
<s>
Napovídá	napovídat	k5eAaBmIp3nS	napovídat
tomu	ten	k3xDgMnSc3	ten
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
verše	verš	k1gInPc1	verš
akrostichu	akrostich	k1gInSc2	akrostich
na	na	k7c6	na
konci	konec	k1gInSc6	konec
Za	za	k7c7	za
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
tvoří	tvořit	k5eAaImIp3nS	tvořit
její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
a	a	k8xC	a
v	v	k7c6	v
textech	text	k1gInPc6	text
obou	dva	k4xCgFnPc2	dva
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
skryto	skryt	k2eAgNnSc1d1	skryto
množství	množství	k1gNnSc1	množství
zběžných	zběžný	k2eAgFnPc2d1	zběžná
zmínek	zmínka	k1gFnPc2	zmínka
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Dodgson	Dodgson	k1gMnSc1	Dodgson
sám	sám	k3xTgMnSc1	sám
ale	ale	k8xC	ale
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
životě	život	k1gInSc6	život
opakovaně	opakovaně	k6eAd1	opakovaně
popíral	popírat	k5eAaImAgMnS	popírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jeho	jeho	k3xOp3gFnSc1	jeho
"	"	kIx"	"
<g/>
malá	malý	k2eAgFnSc1d1	malá
hrdinka	hrdinka	k1gFnSc1	hrdinka
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
skutečném	skutečný	k2eAgNnSc6d1	skutečné
dítěti	dítě	k1gNnSc6	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
často	často	k6eAd1	často
věnoval	věnovat	k5eAaImAgInS	věnovat
dívkám	dívka	k1gFnPc3	dívka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
znal	znát	k5eAaImAgMnS	znát
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
jména	jméno	k1gNnPc4	jméno
vkládal	vkládat	k5eAaImAgMnS	vkládat
do	do	k7c2	do
akrostichů	akrostich	k1gInPc2	akrostich
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Gertrudy	Gertruda	k1gFnSc2	Gertruda
Chatawayové	Chatawayové	k2eAgFnSc2d1	Chatawayové
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
formě	forma	k1gFnSc6	forma
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
předmluvě	předmluva	k1gFnSc6	předmluva
<g/>
/	/	kIx~	/
<g/>
věnování	věnování	k1gNnSc1	věnování
k	k	k7c3	k
básni	báseň	k1gFnSc3	báseň
The	The	k1gFnSc2	The
Hunting	Hunting	k1gInSc1	Hunting
of	of	k?	of
the	the	k?	the
Snark	Snark	k1gInSc1	Snark
(	(	kIx(	(
<g/>
Lovení	lovení	k1gNnSc1	lovení
Snárka	Snárk	k1gInSc2	Snárk
<g/>
)	)	kIx)	)
a	a	k8xC	a
přesto	přesto	k8xC	přesto
nikdy	nikdy	k6eAd1	nikdy
nikdo	nikdo	k3yNnSc1	nikdo
nepředpokládal	předpokládat	k5eNaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
některá	některý	k3yIgFnSc1	některý
postava	postava	k1gFnSc1	postava
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nedostatečné	dostatečný	k2eNgFnPc4d1	nedostatečná
informace	informace	k1gFnPc4	informace
(	(	kIx(	(
<g/>
Dodgsonovy	Dodgsonův	k2eAgInPc1d1	Dodgsonův
deníky	deník	k1gInPc1	deník
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1862	[number]	k4	1862
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
přátelství	přátelství	k1gNnSc1	přátelství
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Liddellových	Liddellová	k1gFnPc2	Liddellová
bylo	být	k5eAaImAgNnS	být
koncem	koncem	k7c2	koncem
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
důležitou	důležitý	k2eAgFnSc4d1	důležitá
součástí	součást	k1gFnSc7	součást
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Zvykl	zvyknout	k5eAaPmAgMnS	zvyknout
si	se	k3xPyFc3	se
brát	brát	k5eAaImF	brát
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
nejdřív	dříve	k6eAd3	dříve
chlapce	chlapec	k1gMnSc2	chlapec
Harryho	Harry	k1gMnSc2	Harry
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
dívky	dívka	k1gFnPc1	dívka
<g/>
)	)	kIx)	)
na	na	k7c4	na
výlety	výlet	k1gInPc1	výlet
loďkou	loďka	k1gFnSc7	loďka
do	do	k7c2	do
blízkých	blízký	k2eAgFnPc2d1	blízká
vesnic	vesnice	k1gFnPc2	vesnice
Nuneham	Nuneham	k1gInSc1	Nuneham
Courtenay	Courtenaa	k1gFnPc1	Courtenaa
a	a	k8xC	a
Godstow	Godstow	k1gFnPc1	Godstow
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
když	když	k8xS	když
vzal	vzít	k5eAaPmAgMnS	vzít
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Robinsonem	Robinson	k1gMnSc7	Robinson
Duckworthem	Duckworth	k1gInSc7	Duckworth
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1862	[number]	k4	1862
dívky	dívka	k1gFnPc4	dívka
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
po	po	k7c6	po
Temži	Temže	k1gFnSc6	Temže
<g/>
,	,	kIx,	,
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Dodgson	Dodgson	k1gMnSc1	Dodgson
základ	základ	k1gInSc4	základ
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stal	stát	k5eAaPmAgInS	stát
jeho	jeho	k3xOp3gInSc1	jeho
prvním	první	k4xOgMnSc6	první
a	a	k8xC	a
největším	veliký	k2eAgInSc7d3	veliký
komerčním	komerční	k2eAgInSc7d1	komerční
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféru	atmosféra	k1gFnSc4	atmosféra
tohoto	tento	k3xDgNnSc2	tento
odpoledne	odpoledne	k1gNnSc2	odpoledne
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
v	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
básni	báseň	k1gFnSc6	báseň
Alenky	Alenka	k1gFnSc2	Alenka
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
onom	onen	k3xDgNnSc6	onen
"	"	kIx"	"
<g/>
golden	goldno	k1gNnPc2	goldno
afternoon	afternoona	k1gFnPc2	afternoona
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
odpoledni	odpoledne	k1gNnSc6	odpoledne
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dívky	dívka	k1gFnPc1	dívka
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
označeny	označit	k5eAaPmNgInP	označit
jmény	jméno	k1gNnPc7	jméno
Prima	primo	k1gNnSc2	primo
<g/>
,	,	kIx,	,
Secunda	Secunda	k1gFnSc1	Secunda
a	a	k8xC	a
Tertia	Tertia	k1gFnSc1	Tertia
<g/>
.	.	kIx.	.
</s>
<s>
Loďka	loďka	k1gFnSc1	loďka
plula	plout	k5eAaImAgFnS	plout
po	po	k7c6	po
Temži	Temže	k1gFnSc6	Temže
až	až	k9	až
k	k	k7c3	k
Godstow	Godstow	k1gFnSc3	Godstow
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
deníku	deník	k1gInSc6	deník
si	se	k3xPyFc3	se
Carroll	Carroll	k1gMnSc1	Carroll
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
dali	dát	k5eAaPmAgMnP	dát
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
čaj	čaj	k1gInSc1	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Christ	Christ	k1gMnSc1	Christ
Church	Churcha	k1gFnPc2	Churcha
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k9	až
po	po	k7c4	po
čtvrt	čtvrt	k1gFnSc4	čtvrt
na	na	k7c4	na
devět	devět	k4xCc4	devět
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
muži	muž	k1gMnPc1	muž
vzali	vzít	k5eAaPmAgMnP	vzít
dívky	dívka	k1gFnPc4	dívka
do	do	k7c2	do
Carrollova	Carrollův	k2eAgInSc2d1	Carrollův
bytu	byt	k1gInSc2	byt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
prohlédly	prohlédnout	k5eAaPmAgFnP	prohlédnout
jeho	jeho	k3xOp3gFnSc4	jeho
sbírku	sbírka	k1gFnSc4	sbírka
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
a	a	k8xC	a
dívky	dívka	k1gFnPc1	dívka
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgFnP	vrátit
domů	domů	k6eAd1	domů
před	před	k7c7	před
devátou	devátý	k4xOgFnSc7	devátý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
záznamu	záznam	k1gInSc3	záznam
připsal	připsat	k5eAaPmAgMnS	připsat
Dodgson	Dodgson	k1gMnSc1	Dodgson
o	o	k7c6	o
sedm	sedm	k4xCc4	sedm
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
poznámku	poznámka	k1gFnSc4	poznámka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
jsem	být	k5eAaImIp1nS	být
jim	on	k3xPp3gMnPc3	on
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
pohádku	pohádka	k1gFnSc4	pohádka
o	o	k7c6	o
Alenčiných	Alenčin	k2eAgNnPc6d1	Alenčino
dobrodružstvích	dobrodružství	k1gNnPc6	dobrodružství
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Alice	Alice	k1gFnSc1	Alice
Liddellová	Liddellová	k1gFnSc1	Liddellová
Dodgsona	Dodgsona	k1gFnSc1	Dodgsona
prosila	prosít	k5eAaPmAgFnS	prosít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pohádku	pohádka	k1gFnSc4	pohádka
sepsal	sepsat	k5eAaPmAgMnS	sepsat
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
jí	jíst	k5eAaImIp3nS	jíst
nakonec	nakonec	k6eAd1	nakonec
(	(	kIx(	(
<g/>
po	po	k7c6	po
spoustě	spousta	k1gFnSc6	spousta
odkládání	odkládání	k1gNnPc2	odkládání
<g/>
)	)	kIx)	)
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1864	[number]	k4	1864
dal	dát	k5eAaPmAgInS	dát
ilustrovaný	ilustrovaný	k2eAgInSc1d1	ilustrovaný
rukopis	rukopis	k1gInSc1	rukopis
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Alenčina	Alenčin	k2eAgNnSc2d1	Alenčino
dobrodružství	dobrodružství	k1gNnSc3	dobrodružství
v	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zárodek	zárodek	k1gInSc1	zárodek
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Carroll	Carroll	k1gInSc1	Carroll
ještě	ještě	k9	ještě
později	pozdě	k6eAd2	pozdě
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
četla	číst	k5eAaImAgFnS	číst
Dodgsonův	Dodgsonův	k2eAgInSc4d1	Dodgsonův
nedokončený	dokončený	k2eNgInSc4d1	nedokončený
rukopis	rukopis	k1gInSc4	rukopis
rodina	rodina	k1gFnSc1	rodina
přítele	přítel	k1gMnSc2	přítel
a	a	k8xC	a
učitele	učitel	k1gMnSc2	učitel
George	Georg	k1gMnSc2	Georg
MacDonalda	Macdonald	k1gMnSc2	Macdonald
a	a	k8xC	a
nadšení	nadšení	k1gNnSc1	nadšení
MacDonaldových	Macdonaldových	k2eAgFnPc2d1	Macdonaldových
dětí	dítě	k1gFnPc2	dítě
povzbudilo	povzbudit	k5eAaPmAgNnS	povzbudit
Dodgsona	Dodgsona	k1gFnSc1	Dodgsona
k	k	k7c3	k
uveřejnění	uveřejnění	k1gNnSc3	uveřejnění
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
ukázal	ukázat	k5eAaPmAgInS	ukázat
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
rukopis	rukopis	k1gInSc1	rukopis
nakladateli	nakladatel	k1gMnSc3	nakladatel
Macmillanovi	Macmillan	k1gMnSc3	Macmillan
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
zalíbil	zalíbit	k5eAaPmAgMnS	zalíbit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
byly	být	k5eAaImAgInP	být
zamítnuty	zamítnout	k5eAaPmNgInP	zamítnout
možné	možný	k2eAgInPc1d1	možný
alternativní	alternativní	k2eAgInPc1d1	alternativní
názvy	název	k1gInPc1	název
"	"	kIx"	"
<g/>
Alenka	Alenka	k1gFnSc1	Alenka
mezi	mezi	k7c7	mezi
skřítky	skřítek	k1gMnPc7	skřítek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Alice	Alice	k1gFnSc1	Alice
Among	Among	k1gInSc1	Among
the	the	k?	the
Fairies	Fairies	k1gInSc1	Fairies
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Alenčina	Alenčin	k2eAgFnSc1d1	Alenčina
zlatá	zlatý	k2eAgFnSc1d1	zlatá
hodina	hodina	k1gFnSc1	hodina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Alice	Alice	k1gFnSc1	Alice
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Golden	Goldno	k1gNnPc2	Goldno
Hour	Hour	k1gInSc1	Hour
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
vydáno	vydat	k5eAaPmNgNnS	vydat
jako	jako	k9	jako
Alenčina	Alenčin	k2eAgNnPc1d1	Alenčino
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
pod	pod	k7c7	pod
literárním	literární	k2eAgInSc7d1	literární
pseudonymem	pseudonym	k1gInSc7	pseudonym
Lewis	Lewis	k1gFnSc2	Lewis
Carroll	Carroll	k1gInSc1	Carroll
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Dodgson	Dodgson	k1gInSc1	Dodgson
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgInS	použít
asi	asi	k9	asi
o	o	k7c4	o
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnPc1	ilustrace
byly	být	k5eAaImAgFnP	být
tentokrát	tentokrát	k6eAd1	tentokrát
Johna	John	k1gMnSc4	John
Tenniela	Tenniel	k1gMnSc4	Tenniel
<g/>
.	.	kIx.	.
</s>
<s>
Dodgson	Dodgson	k1gMnSc1	Dodgson
si	se	k3xPyFc3	se
očividně	očividně	k6eAd1	očividně
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydaná	vydaný	k2eAgFnSc1d1	vydaná
kniha	kniha	k1gFnSc1	kniha
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
dovednosti	dovednost	k1gFnSc3	dovednost
profesionálního	profesionální	k2eAgMnSc2d1	profesionální
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Ohromný	ohromný	k2eAgInSc4d1	ohromný
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
první	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
o	o	k7c6	o
Alence	Alenka	k1gFnSc6	Alenka
v	v	k7c6	v
mnoha	mnoho	k4c2	mnoho
ohledech	ohled	k1gInPc6	ohled
změnil	změnit	k5eAaPmAgInS	změnit
Dodgsonův	Dodgsonův	k2eAgInSc4d1	Dodgsonův
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Sláva	Sláva	k1gFnSc1	Sláva
jeho	jeho	k3xOp3gFnPc2	jeho
alter	altra	k1gFnPc2	altra
ega	ego	k1gNnSc2	ego
"	"	kIx"	"
<g/>
Lewis	Lewis	k1gFnSc1	Lewis
Carroll	Carroll	k1gMnSc1	Carroll
<g/>
"	"	kIx"	"
brzy	brzy	k6eAd1	brzy
obletěla	obletět	k5eAaPmAgFnS	obletět
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dodgsona	Dodgsona	k1gFnSc1	Dodgsona
zaplavily	zaplavit	k5eAaPmAgInP	zaplavit
dopisy	dopis	k1gInPc4	dopis
obdivovatelů	obdivovatel	k1gMnPc2	obdivovatel
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostávalo	dostávat	k5eAaImAgNnS	dostávat
nevítané	vítaný	k2eNgFnSc3d1	nevítaná
pozornosti	pozornost	k1gFnSc3	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
vydělávat	vydělávat	k5eAaImF	vydělávat
značné	značný	k2eAgFnPc4d1	značná
sumy	suma	k1gFnPc4	suma
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
příjem	příjem	k1gInSc1	příjem
ale	ale	k9	ale
nevyužil	využít	k5eNaPmAgInS	využít
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
svého	svůj	k3xOyFgInSc2	svůj
zdánlivě	zdánlivě	k6eAd1	zdánlivě
neoblíbeného	oblíbený	k2eNgNnSc2d1	neoblíbené
místa	místo	k1gNnSc2	místo
v	v	k7c4	v
Christ	Christ	k1gInSc4	Christ
Church	Churcha	k1gFnPc2	Churcha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pokračování	pokračování	k1gNnSc1	pokračování
-	-	kIx~	-
Za	za	k7c7	za
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
a	a	k8xC	a
co	co	k9	co
tam	tam	k6eAd1	tam
Alenka	Alenka	k1gFnSc1	Alenka
našla	najít	k5eAaPmAgFnS	najít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
poněkud	poněkud	k6eAd1	poněkud
temnější	temný	k2eAgFnSc6d2	temnější
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	se	k3xPyFc4	se
zrcadlily	zrcadlit	k5eAaImAgFnP	zrcadlit
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
Dodgsonově	Dodgsonův	k2eAgInSc6d1	Dodgsonův
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Nedávná	dávný	k2eNgFnSc1d1	nedávná
otcova	otcův	k2eAgFnSc1d1	otcova
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
ho	on	k3xPp3gMnSc4	on
uvrhla	uvrhnout	k5eAaPmAgFnS	uvrhnout
do	do	k7c2	do
deprese	deprese	k1gFnSc2	deprese
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
trvat	trvat	k5eAaImF	trvat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
Dodgson	Dodgson	k1gInSc1	Dodgson
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
velké	velký	k2eAgNnSc4d1	velké
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
knihu	kniha	k1gFnSc4	kniha
The	The	k1gFnSc2	The
Hunting	Hunting	k1gInSc1	Hunting
of	of	k?	of
the	the	k?	the
Snark	Snark	k1gInSc4	Snark
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
věnoval	věnovat	k5eAaPmAgMnS	věnovat
své	svůj	k3xOyFgFnPc4	svůj
dětské	dětský	k2eAgFnPc4d1	dětská
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Gertrude	Gertrud	k1gInSc5	Gertrud
Chatawayové	Chatawayové	k2eAgMnSc5d1	Chatawayové
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fantaskní	fantaskní	k2eAgFnSc1d1	fantaskní
"	"	kIx"	"
<g/>
nesmyslná	smyslný	k2eNgFnSc1d1	nesmyslná
<g/>
"	"	kIx"	"
báseň	báseň	k1gFnSc1	báseň
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
dobrodružstvích	dobrodružství	k1gNnPc6	dobrodružství
bizarní	bizarní	k2eAgFnSc2d1	bizarní
skupiny	skupina	k1gFnSc2	skupina
různorodých	různorodý	k2eAgMnPc2d1	různorodý
neschopných	schopný	k2eNgMnPc2d1	neschopný
tvorů	tvor	k1gMnPc2	tvor
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
bobra	bobr	k1gMnSc2	bobr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
hledat	hledat	k5eAaImF	hledat
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
Snárka	Snárk	k1gInSc2	Snárk
<g/>
.	.	kIx.	.
</s>
<s>
Malíř	malíř	k1gMnSc1	malíř
Dante	Dante	k1gMnSc1	Dante
Gabriel	Gabriel	k1gMnSc1	Gabriel
Rossetti	Rossetti	k1gNnPc2	Rossetti
byl	být	k5eAaImAgMnS	být
údajně	údajně	k6eAd1	údajně
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
báseň	báseň	k1gFnSc1	báseň
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
něm.	něm.	k?	něm.
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
ji	on	k3xPp3gFnSc4	on
přeložil	přeložit	k5eAaPmAgMnS	přeložit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Václav	Václav	k1gMnSc1	Václav
Z.	Z.	kA	Z.
J.	J.	kA	J.
Pinkava	Pinkava	k?	Pinkava
jako	jako	k8xC	jako
Lovení	lovení	k1gNnSc1	lovení
Snárka	Snárk	k1gInSc2	Snárk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
se	se	k3xPyFc4	se
Dodgson	Dodgson	k1gMnSc1	Dodgson
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaPmF	věnovat
nové	nový	k2eAgFnSc3d1	nová
formě	forma	k1gFnSc3	forma
umění	umění	k1gNnSc2	umění
-	-	kIx~	-
fotografování	fotografování	k1gNnSc4	fotografování
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
Skeffingtona	Skeffington	k1gMnSc2	Skeffington
Lutwidgeho	Lutwidge	k1gMnSc2	Lutwidge
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
svého	svůj	k3xOyFgMnSc4	svůj
oxfordského	oxfordský	k2eAgMnSc4d1	oxfordský
přítele	přítel	k1gMnSc4	přítel
Reginalda	Reginald	k1gMnSc4	Reginald
Southeyho	Southey	k1gMnSc4	Southey
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
umění	umění	k1gNnSc6	umění
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgMnSc1d1	dobrý
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
známým	známý	k2eAgMnSc7d1	známý
fotografem	fotograf	k1gMnSc7	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
letech	let	k1gInPc6	let
si	se	k3xPyFc3	se
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
pohrával	pohrávat	k5eAaImAgMnS	pohrávat
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
fotografováním	fotografování	k1gNnSc7	fotografování
bude	být	k5eAaImBp3nS	být
živit	živit	k5eAaImF	živit
<g/>
.	.	kIx.	.
</s>
<s>
Nedávná	dávný	k2eNgFnSc1d1	nedávná
studie	studie	k1gFnSc1	studie
Rogera	Roger	k1gMnSc2	Roger
Taylora	Taylor	k1gMnSc2	Taylor
a	a	k8xC	a
Edwarda	Edward	k1gMnSc2	Edward
Wakelinga	Wakeling	k1gMnSc2	Wakeling
podává	podávat	k5eAaImIp3nS	podávat
vyčerpávající	vyčerpávající	k2eAgInSc4d1	vyčerpávající
výčet	výčet	k1gInSc4	výčet
všech	všecek	k3xTgFnPc2	všecek
zachovaných	zachovaný	k2eAgFnPc2d1	zachovaná
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
Taylor	Taylor	k1gMnSc1	Taylor
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
něco	něco	k3yInSc1	něco
přes	přes	k7c4	přes
50	[number]	k4	50
<g/>
%	%	kIx~	%
jeho	jeho	k3xOp3gNnPc2	jeho
zachovaných	zachovaný	k2eAgNnPc2d1	zachované
děl	dělo	k1gNnPc2	dělo
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
mladé	mladý	k2eAgFnPc4d1	mladá
dívky	dívka	k1gFnPc4	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
mnoho	mnoho	k4c1	mnoho
svých	svůj	k3xOyFgFnPc2	svůj
fotografií	fotografia	k1gFnPc2	fotografia
dětí	dítě	k1gFnPc2	dítě
použil	použít	k5eAaPmAgInS	použít
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
svými	svůj	k3xOyFgNnPc7	svůj
literárními	literární	k2eAgNnPc7d1	literární
díly	dílo	k1gNnPc7	dílo
jako	jako	k9	jako
ilustrace	ilustrace	k1gFnPc1	ilustrace
<g/>
.	.	kIx.	.
</s>
<s>
Alexandra	Alexandra	k1gFnSc1	Alexandra
Kitchinová	Kitchinová	k1gFnSc1	Kitchinová
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Xie	Xie	k1gFnSc1	Xie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vyslovováno	vyslovován	k2eAgNnSc4d1	vyslovováno
"	"	kIx"	"
<g/>
Iksi	Iksi	k1gNnSc4	Iksi
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jeho	on	k3xPp3gMnSc4	on
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
objektem	objekt	k1gInSc7	objekt
pro	pro	k7c4	pro
fotografování	fotografování	k1gNnSc4	fotografování
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
fotografování	fotografování	k1gNnSc4	fotografování
nechal	nechat	k5eAaPmAgMnS	nechat
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
Dodgson	Dodgson	k1gInSc1	Dodgson
vyfotografoval	vyfotografovat	k5eAaPmAgInS	vyfotografovat
alespoň	alespoň	k9	alespoň
padesátkrát	padesátkrát	k6eAd1	padesátkrát
<g/>
,	,	kIx,	,
naposled	naposled	k6eAd1	naposled
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
jejími	její	k3xOp3gFnPc7	její
šestnáctými	šestnáctý	k4xOgFnPc7	šestnáctý
narozeninami	narozeniny	k1gFnPc7	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
původního	původní	k2eAgNnSc2d1	původní
portfolia	portfolio	k1gNnSc2	portfolio
ale	ale	k8xC	ale
zbyla	zbýt	k5eAaPmAgFnS	zbýt
méně	málo	k6eAd2	málo
než	než	k8xS	než
třetina	třetina	k1gFnSc1	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Dodgson	Dodgson	k1gMnSc1	Dodgson
také	také	k9	také
zachytil	zachytit	k5eAaPmAgMnS	zachytit
mnoho	mnoho	k4c4	mnoho
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
chlapců	chlapec	k1gMnPc2	chlapec
a	a	k8xC	a
krajin	krajina	k1gFnPc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
objekty	objekt	k1gInPc1	objekt
byly	být	k5eAaImAgInP	být
také	také	k9	také
kostry	kostra	k1gFnSc2	kostra
<g/>
,	,	kIx,	,
loutky	loutka	k1gFnPc1	loutka
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
učenci	učenec	k1gMnPc1	učenec
<g/>
,	,	kIx,	,
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
staří	starý	k2eAgMnPc1d1	starý
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
malé	malý	k2eAgFnPc1d1	malá
dívenky	dívenka	k1gFnPc1	dívenka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
skici	skica	k1gFnPc1	skica
nahých	nahý	k2eAgFnPc2d1	nahá
dětí	dítě	k1gFnPc2	dítě
byly	být	k5eAaImAgInP	být
dlouho	dlouho	k6eAd1	dlouho
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
ztracené	ztracený	k2eAgNnSc4d1	ztracené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
šest	šest	k4xCc1	šest
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
po	po	k7c6	po
čase	čas	k1gInSc6	čas
našlo	najít	k5eAaPmAgNnS	najít
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
čtyři	čtyři	k4xCgFnPc1	čtyři
byly	být	k5eAaImAgFnP	být
uveřejněny	uveřejnit	k5eAaPmNgFnP	uveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Fotografování	fotografování	k1gNnSc1	fotografování
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
také	také	k6eAd1	také
bylo	být	k5eAaImAgNnS	být
vstupní	vstupní	k2eAgFnSc7d1	vstupní
branou	brána	k1gFnSc7	brána
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
nejplodnějšího	plodný	k2eAgNnSc2d3	nejplodnější
období	období	k1gNnSc2	období
pořídil	pořídit	k5eAaPmAgInS	pořídit
portréty	portrét	k1gInPc4	portrét
významných	významný	k2eAgMnPc2d1	významný
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Johna	John	k1gMnSc4	John
Everetta	Everetto	k1gNnSc2	Everetto
Millaise	Millaise	k1gFnSc2	Millaise
<g/>
,	,	kIx,	,
Ellen	Ellen	k1gInSc4	Ellen
Terryové	Terryová	k1gFnSc2	Terryová
<g/>
,	,	kIx,	,
Dante	Dante	k1gMnSc1	Dante
Gabriela	Gabriela	k1gFnSc1	Gabriela
Rossettiho	Rossetti	k1gMnSc2	Rossetti
<g/>
,	,	kIx,	,
fotografky	fotografka	k1gFnSc2	fotografka
Julie	Julie	k1gFnSc2	Julie
Margarety	Margareta	k1gFnSc2	Margareta
Cameronové	Cameronový	k2eAgFnSc2d1	Cameronová
<g/>
,	,	kIx,	,
Michaela	Michael	k1gMnSc2	Michael
Faradaye	Faraday	k1gMnSc2	Faraday
a	a	k8xC	a
Lorda	lord	k1gMnSc2	lord
Alfreda	Alfred	k1gMnSc2	Alfred
Tennysona	Tennyson	k1gMnSc2	Tennyson
<g/>
.	.	kIx.	.
</s>
<s>
Inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
také	také	k9	také
dílem	díl	k1gInSc7	díl
fotografky	fotografka	k1gFnSc2	fotografka
vikomtesy	vikomtesa	k1gFnSc2	vikomtesa
Clementiny	Clementina	k1gFnSc2	Clementina
Hawarden	Hawardna	k1gFnPc2	Hawardna
<g/>
,	,	kIx,	,
od	od	k7c2	od
níž	jenž	k3xRgFnSc2	jenž
také	také	k9	také
několik	několik	k4yIc4	několik
jejích	její	k3xOp3gNnPc2	její
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
<g/>
.	.	kIx.	.
</s>
<s>
Dodgson	Dodgson	k1gInSc1	Dodgson
s	s	k7c7	s
fotografováním	fotografování	k1gNnSc7	fotografování
náhle	náhle	k6eAd1	náhle
přestal	přestat	k5eAaPmAgInS	přestat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
24	[number]	k4	24
let	léto	k1gNnPc2	léto
toto	tento	k3xDgNnSc4	tento
médium	médium	k1gNnSc4	médium
mistrovsky	mistrovsky	k6eAd1	mistrovsky
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
v	v	k7c4	v
Christ	Christ	k1gInSc4	Christ
Church	Church	k1gMnSc1	Church
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
studio	studio	k1gNnSc4	studio
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
a	a	k8xC	a
záměrné	záměrný	k2eAgNnSc1d1	záměrné
ničení	ničení	k1gNnSc1	ničení
přečkalo	přečkat	k5eAaPmAgNnS	přečkat
méně	málo	k6eAd2	málo
než	než	k8xS	než
tisíc	tisíc	k4xCgInSc4	tisíc
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
fotografování	fotografování	k1gNnSc4	fotografování
nechal	nechat	k5eAaPmAgMnS	nechat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nejisté	jistý	k2eNgInPc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
modernismu	modernismus	k1gInSc2	modernismus
se	se	k3xPyFc4	se
vkus	vkus	k1gInSc1	vkus
změnil	změnit	k5eAaPmAgInS	změnit
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
fotografie	fotografia	k1gFnPc1	fotografia
byly	být	k5eAaImAgFnP	být
asi	asi	k9	asi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
až	až	k8xS	až
do	do	k7c2	do
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
zapomenuty	zapomenut	k2eAgFnPc1d1	zapomenuta
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
viktoriánských	viktoriánský	k2eAgMnPc2d1	viktoriánský
fotografů	fotograf	k1gMnPc2	fotograf
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
některé	některý	k3yIgMnPc4	některý
moderní	moderní	k2eAgMnPc4d1	moderní
fotografy	fotograf	k1gMnPc4	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
snímky	snímek	k1gInPc1	snímek
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
Art	Art	k1gFnSc2	Art
Institute	institut	k1gInSc5	institut
of	of	k?	of
Chicago	Chicago	k1gNnSc1	Chicago
v	v	k7c6	v
Illinois	Illinois	k1gFnPc6	Illinois
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
prominentů	prominent	k1gMnPc2	prominent
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1867	[number]	k4	1867
mu	on	k3xPp3gMnSc3	on
jeho	jeho	k3xOp3gFnSc6	jeho
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
kolega	kolega	k1gMnSc1	kolega
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
Henry	Henry	k1gMnSc1	Henry
Liddon	Liddon	k1gMnSc1	Liddon
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
společnou	společný	k2eAgFnSc4d1	společná
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
hned	hned	k6eAd1	hned
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Cestovali	cestovat	k5eAaImAgMnP	cestovat
přes	přes	k7c4	přes
Brusel	Brusel	k1gInSc4	Brusel
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
a	a	k8xC	a
Kaliningrad	Kaliningrad	k1gInSc1	Kaliningrad
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
měsíce	měsíc	k1gInSc2	měsíc
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Petrohrad	Petrohrad	k1gInSc4	Petrohrad
<g/>
,	,	kIx,	,
Moskvu	Moskva	k1gFnSc4	Moskva
<g/>
,	,	kIx,	,
pravoslavný	pravoslavný	k2eAgInSc4d1	pravoslavný
klášter	klášter	k1gInSc4	klášter
Trojicko-sergijevská	Trojickoergijevský	k2eAgFnSc1d1	Trojicko-sergijevský
lávra	lávra	k1gFnSc1	lávra
v	v	k7c6	v
Sergijev	Sergijev	k1gFnSc6	Sergijev
Posadu	posad	k1gInSc2	posad
a	a	k8xC	a
Nižnij	Nižnij	k1gFnSc2	Nižnij
Novgorod	Novgorod	k1gInSc1	Novgorod
<g/>
.	.	kIx.	.
</s>
<s>
Domů	domů	k6eAd1	domů
se	se	k3xPyFc4	se
vraceli	vracet	k5eAaImAgMnP	vracet
přes	přes	k7c4	přes
Varšavu	Varšava	k1gFnSc4	Varšava
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gFnSc4	Vratislav
<g/>
,	,	kIx,	,
Drážďany	Drážďany	k1gInPc1	Drážďany
<g/>
,	,	kIx,	,
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
a	a	k8xC	a
Calais	Calais	k1gNnSc1	Calais
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
dojmy	dojem	k1gInPc4	dojem
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
cestopise	cestopis	k1gInSc6	cestopis
Russian	Russian	k1gMnSc1	Russian
Journal	Journal	k1gMnSc1	Journal
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
publikován	publikovat	k5eAaBmNgInS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
jedinou	jediný	k2eAgFnSc4d1	jediná
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
dvaceti	dvacet	k4xCc2	dvacet
zůstal	zůstat	k5eAaPmAgInS	zůstat
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
i	i	k9	i
přes	přes	k7c4	přes
vzrůstající	vzrůstající	k2eAgNnSc4d1	vzrůstající
bohatství	bohatství	k1gNnSc4	bohatství
a	a	k8xC	a
slávu	sláva	k1gFnSc4	sláva
téměř	téměř	k6eAd1	téměř
nezměněn	změnit	k5eNaPmNgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
učil	učít	k5eAaPmAgInS	učít
na	na	k7c4	na
Christ	Christ	k1gInSc4	Christ
Church	Church	k1gMnSc1	Church
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
zde	zde	k6eAd1	zde
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
poslední	poslední	k2eAgInSc1d1	poslední
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
román	román	k1gInSc1	román
Sylvie	Sylvie	k1gFnSc2	Sylvie
a	a	k8xC	a
Bruno	Bruno	k1gMnSc1	Bruno
byl	být	k5eAaImAgMnS	být
vydán	vydat	k5eAaPmNgMnS	vydat
po	po	k7c6	po
dílech	dílo	k1gNnPc6	dílo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1889	[number]	k4	1889
a	a	k8xC	a
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
komplikovanost	komplikovanost	k1gFnSc1	komplikovanost
a	a	k8xC	a
očividná	očividný	k2eAgFnSc1d1	očividná
zmatenost	zmatenost	k1gFnSc1	zmatenost
většinu	většina	k1gFnSc4	většina
čtenářů	čtenář	k1gMnPc2	čtenář
vyvedla	vyvést	k5eAaPmAgFnS	vyvést
z	z	k7c2	z
konceptu	koncept	k1gInSc2	koncept
a	a	k8xC	a
román	román	k1gInSc4	román
neměl	mít	k5eNaImAgMnS	mít
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1898	[number]	k4	1898
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
po	po	k7c6	po
chřipce	chřipka	k1gFnSc6	chřipka
v	v	k7c6	v
domě	dům	k1gInSc6	dům
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
v	v	k7c6	v
Guildfordu	Guildford	k1gInSc6	Guildford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k8xC	i
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Dodgson	Dodgson	k1gInSc1	Dodgson
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
věku	věk	k1gInSc2	věk
vyučován	vyučovat	k5eAaImNgInS	vyučovat
pro	pro	k7c4	pro
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
anglikánské	anglikánský	k2eAgFnSc6d1	anglikánská
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
očekávalo	očekávat	k5eAaImAgNnS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pravidly	pravidlo	k1gNnPc7	pravidlo
Christ	Christ	k1gMnSc1	Christ
Church	Church	k1gMnSc1	Church
bude	být	k5eAaImBp3nS	být
vysvěcen	vysvěcen	k2eAgMnSc1d1	vysvěcen
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
do	do	k7c2	do
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Dodgson	Dodgson	k1gMnSc1	Dodgson
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
očividně	očividně	k6eAd1	očividně
začal	začít	k5eAaPmAgMnS	začít
zdráhat	zdráhat	k5eAaImF	zdráhat
<g/>
.	.	kIx.	.
</s>
<s>
Nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
to	ten	k3xDgNnSc4	ten
odkládal	odkládat	k5eAaImAgMnS	odkládat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1861	[number]	k4	1861
vysvěcen	vysvětit	k5eAaPmNgMnS	vysvětit
na	na	k7c4	na
jáhna	jáhen	k1gMnSc4	jáhen
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k9	ale
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
měl	mít	k5eAaImAgInS	mít
postoupit	postoupit	k5eAaPmF	postoupit
k	k	k7c3	k
vysvěcení	vysvěcení	k1gNnSc3	vysvěcení
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
děkana	děkan	k1gMnSc4	děkan
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
v	v	k7c6	v
kněžské	kněžský	k2eAgFnSc6d1	kněžská
dráze	dráha	k1gFnSc6	dráha
nepokračovat	pokračovat	k5eNaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
proti	proti	k7c3	proti
pravidlům	pravidlo	k1gNnPc3	pravidlo
koleje	kolej	k1gFnSc2	kolej
a	a	k8xC	a
děkan	děkan	k1gMnSc1	děkan
Liddell	Liddell	k1gMnSc1	Liddell
mu	on	k3xPp3gMnSc3	on
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
být	být	k5eAaImF	být
vysvěcen	vysvětit	k5eAaPmNgMnS	vysvětit
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
místa	místo	k1gNnSc2	místo
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
Dodgsonovi	Dodgson	k1gMnSc3	Dodgson
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
poradit	poradit	k5eAaPmF	poradit
s	s	k7c7	s
kolejní	kolejní	k2eAgFnSc7d1	kolejní
radou	rada	k1gFnSc7	rada
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
téměř	téměř	k6eAd1	téměř
nepochybně	pochybně	k6eNd1	pochybně
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vyloučení	vyloučení	k1gNnSc3	vyloučení
z	z	k7c2	z
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
Liddell	Liddell	k1gMnSc1	Liddell
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
rozmyslel	rozmyslet	k5eAaPmAgInS	rozmyslet
a	a	k8xC	a
dovolil	dovolit	k5eAaPmAgInS	dovolit
Dodgsonovi	Dodgson	k1gMnSc3	Dodgson
zůstat	zůstat	k5eAaPmF	zůstat
i	i	k9	i
přes	přes	k7c4	přes
porušení	porušení	k1gNnSc4	porušení
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Dodgson	Dodgson	k1gMnSc1	Dodgson
se	s	k7c7	s
knězem	kněz	k1gMnSc7	kněz
nikdy	nikdy	k6eAd1	nikdy
nestal	stát	k5eNaPmAgMnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc4	žádný
přesvědčivé	přesvědčivý	k2eAgInPc4d1	přesvědčivý
podklady	podklad	k1gInPc4	podklad
pro	pro	k7c4	pro
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Dodgson	Dodgson	k1gMnSc1	Dodgson
kněžství	kněžství	k1gNnSc2	kněžství
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
koktavosti	koktavost	k1gFnSc3	koktavost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
kázání	kázání	k1gNnSc4	kázání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vedle	vedle	k7c2	vedle
jeho	jeho	k3xOp3gFnSc2	jeho
ochoty	ochota	k1gFnSc2	ochota
vystupovat	vystupovat	k5eAaImF	vystupovat
před	před	k7c7	před
publikem	publikum	k1gNnSc7	publikum
(	(	kIx(	(
<g/>
při	při	k7c6	při
vyprávění	vyprávění	k1gNnSc6	vyprávění
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
recitaci	recitace	k1gFnSc4	recitace
<g/>
,	,	kIx,	,
představeních	představení	k1gNnPc6	představení
s	s	k7c7	s
magickými	magický	k2eAgFnPc7d1	magická
lucernami	lucerna	k1gFnPc7	lucerna
<g/>
)	)	kIx)	)
a	a	k8xC	a
skutečnosti	skutečnost	k1gFnPc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
životě	život	k1gInSc6	život
opravdu	opravdu	k6eAd1	opravdu
kázal	kázat	k5eAaImAgMnS	kázat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
jako	jako	k9	jako
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vypadá	vypadat	k5eAaPmIp3nS	vypadat
nepravděpodobně	pravděpodobně	k6eNd1	pravděpodobně
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
soudí	soudit	k5eAaImIp3nP	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
k	k	k7c3	k
anglikánské	anglikánský	k2eAgFnSc3d1	anglikánská
církvi	církev	k1gFnSc3	církev
vážné	vážný	k2eAgFnSc2d1	vážná
výhrady	výhrada	k1gFnSc2	výhrada
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
křesťanské	křesťanský	k2eAgFnPc4d1	křesťanská
minority	minorita	k1gFnPc4	minorita
(	(	kIx(	(
<g/>
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
teologa	teolog	k1gMnSc2	teolog
F.	F.	kA	F.
D.	D.	kA	D.
Maurice	Maurika	k1gFnSc6	Maurika
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
alternativní	alternativní	k2eAgNnPc4d1	alternativní
<g/>
"	"	kIx"	"
náboženství	náboženství	k1gNnPc4	náboženství
(	(	kIx(	(
<g/>
teosofii	teosofie	k1gFnSc4	teosofie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dodgsona	Dodgsona	k1gFnSc1	Dodgsona
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
začátek	začátek	k1gInSc1	začátek
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
trápil	trápit	k5eAaImAgMnS	trápit
nevyjasněný	vyjasněný	k2eNgInSc4d1	nevyjasněný
pocit	pocit	k1gInSc4	pocit
hříchu	hřích	k1gInSc2	hřích
a	a	k8xC	a
viny	vina	k1gFnSc2	vina
a	a	k8xC	a
často	často	k6eAd1	často
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
deníků	deník	k1gInPc2	deník
psal	psát	k5eAaImAgInS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
špatný	špatný	k2eAgInSc1d1	špatný
a	a	k8xC	a
bezcenný	bezcenný	k2eAgMnSc1d1	bezcenný
<g/>
"	"	kIx"	"
hříšník	hříšník	k1gMnSc1	hříšník
a	a	k8xC	a
nezaslouží	zasloužit	k5eNaPmIp3nS	zasloužit
si	se	k3xPyFc3	se
být	být	k5eAaImF	být
knězem	kněz	k1gMnSc7	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
třinácti	třináct	k4xCc2	třináct
Dodgsonových	Dodgsonův	k2eAgInPc2d1	Dodgsonův
deníků	deník	k1gInPc2	deník
alespoň	alespoň	k9	alespoň
čtyři	čtyři	k4xCgFnPc4	čtyři
celé	celý	k2eAgFnPc4d1	celá
knihy	kniha	k1gFnPc4	kniha
a	a	k8xC	a
asi	asi	k9	asi
sedm	sedm	k4xCc1	sedm
stránek	stránka	k1gFnPc2	stránka
textu	text	k1gInSc2	text
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
knihy	kniha	k1gFnPc1	kniha
ztratily	ztratit	k5eAaPmAgFnP	ztratit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
někdo	někdo	k3yInSc1	někdo
záměrně	záměrně	k6eAd1	záměrně
odstranil	odstranit	k5eAaPmAgMnS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
odborníků	odborník	k1gMnPc2	odborník
zastává	zastávat	k5eAaImIp3nS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
deníky	deník	k1gInPc4	deník
odstranili	odstranit	k5eAaPmAgMnP	odstranit
rodinní	rodinní	k2eAgMnPc1d1	rodinní
příslušníci	příslušník	k1gMnPc1	příslušník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nepoškodily	poškodit	k5eNaPmAgInP	poškodit
pověst	pověst	k1gFnSc4	pověst
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dokázáno	dokázán	k2eAgNnSc1d1	dokázáno
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
chybějící	chybějící	k2eAgInPc4d1	chybějící
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jediné	jediný	k2eAgFnSc2d1	jediná
stránky	stránka	k1gFnSc2	stránka
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
mezi	mezi	k7c7	mezi
rokem	rok	k1gInSc7	rok
1853	[number]	k4	1853
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Dodgsonovi	Dodgsonův	k2eAgMnPc1d1	Dodgsonův
22	[number]	k4	22
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
1863	[number]	k4	1863
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
32	[number]	k4	32
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
ztráty	ztráta	k1gFnSc2	ztráta
těchto	tento	k3xDgInPc2	tento
materiálů	materiál	k1gInPc2	materiál
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c4	mnoho
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
pro	pro	k7c4	pro
jednu	jeden	k4xCgFnSc4	jeden
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
chybějící	chybějící	k2eAgFnSc4d1	chybějící
stránku	stránka	k1gFnSc4	stránka
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
vytržena	vytržen	k2eAgFnSc1d1	vytržena
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dodgson	Dodgson	k1gMnSc1	Dodgson
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
jedenáctileté	jedenáctiletý	k2eAgFnSc2d1	jedenáctiletá
Alice	Alice	k1gFnSc2	Alice
Liddellové	Liddellová	k1gFnSc2	Liddellová
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
ale	ale	k8xC	ale
nebyly	být	k5eNaImAgInP	být
žádné	žádný	k3yNgInPc1	žádný
podklady	podklad	k1gInPc1	podklad
a	a	k8xC	a
list	list	k1gInSc1	list
nalezený	nalezený	k2eAgInSc1d1	nalezený
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Dodgsonovy	Dodgsonův	k2eAgFnSc2d1	Dodgsonův
rodiny	rodina	k1gFnSc2	rodina
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
opak	opak	k1gInSc1	opak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deníku	deník	k1gInSc6	deník
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
Dodgson	Dodgson	k1gInSc1	Dodgson
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
záchvat	záchvat	k1gInSc4	záchvat
migrény	migréna	k1gFnSc2	migréna
s	s	k7c7	s
aurou	aura	k1gFnSc7	aura
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
popsal	popsat	k5eAaPmAgMnS	popsat
průběh	průběh	k1gInSc4	průběh
"	"	kIx"	"
<g/>
hýbajících	hýbající	k2eAgInPc2d1	hýbající
se	se	k3xPyFc4	se
hradů	hrad	k1gInPc2	hrad
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
příznak	příznak	k1gInSc1	příznak
fáze	fáze	k1gFnSc2	fáze
aury	aura	k1gFnSc2	aura
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
příznaků	příznak	k1gInPc2	příznak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
opakované	opakovaný	k2eAgInPc4d1	opakovaný
záchvaty	záchvat	k1gInPc4	záchvat
běžnější	běžný	k2eAgFnSc2d2	běžnější
formy	forma	k1gFnSc2	forma
migrény	migréna	k1gFnSc2	migréna
<g/>
:	:	kIx,	:
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
nevolnosti	nevolnost	k1gFnSc2	nevolnost
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
či	či	k8xC	či
tak	tak	k9	tak
ale	ale	k8xC	ale
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
skutečný	skutečný	k2eAgInSc4d1	skutečný
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
několik	několik	k4yIc1	několik
lidí	člověk	k1gMnPc2	člověk
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podivné	podivný	k2eAgInPc1d1	podivný
Alenčiny	Alenčin	k2eAgInPc1d1	Alenčin
zážitky	zážitek	k1gInPc1	zážitek
z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
příběhů	příběh	k1gInPc2	příběh
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
inspirovány	inspirován	k2eAgInPc1d1	inspirován
symptomy	symptom	k1gInPc1	symptom
podobnými	podobný	k2eAgInPc7d1	podobný
migréně	migréna	k1gFnSc6	migréna
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
dokonce	dokonce	k9	dokonce
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
psychická	psychický	k2eAgFnSc1d1	psychická
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
syndrom	syndrom	k1gInSc1	syndrom
Alenky	Alenka	k1gFnSc2	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
.	.	kIx.	.
</s>
<s>
Nemocný	nemocný	k2eAgMnSc1d1	nemocný
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
například	například	k6eAd1	například
může	moct	k5eAaImIp3nS	moct
dívat	dívat	k5eAaImF	dívat
na	na	k7c4	na
větší	veliký	k2eAgInSc4d2	veliký
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
basketbalový	basketbalový	k2eAgInSc4d1	basketbalový
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
a	a	k8xC	a
vidět	vidět	k5eAaImF	vidět
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
jako	jako	k8xS	jako
myš	myš	k1gFnSc1	myš
<g/>
.	.	kIx.	.
</s>
<s>
Dodgson	Dodgson	k1gMnSc1	Dodgson
také	také	k6eAd1	také
zažil	zažít	k5eAaPmAgMnS	zažít
dva	dva	k4xCgInPc4	dva
záchvaty	záchvat	k1gInPc4	záchvat
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgFnPc6	který
ztratil	ztratit	k5eAaPmAgMnS	ztratit
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
diagnózu	diagnóza	k1gFnSc4	diagnóza
určovali	určovat	k5eAaImAgMnP	určovat
dva	dva	k4xCgMnPc1	dva
lékaři	lékař	k1gMnPc1	lékař
<g/>
:	:	kIx,	:
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Morshead	Morshead	k1gInSc1	Morshead
považoval	považovat	k5eAaImAgInS	považovat
záchvat	záchvat	k1gInSc4	záchvat
za	za	k7c4	za
"	"	kIx"	"
<g/>
něco	něco	k3yInSc4	něco
připomínajícího	připomínající	k2eAgNnSc2d1	připomínající
epilepsii	epilepsie	k1gFnSc3	epilepsie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
vyvodili	vyvodit	k5eAaPmAgMnP	vyvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
epilepsií	epilepsie	k1gFnSc7	epilepsie
trpěl	trpět	k5eAaImAgMnS	trpět
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
denících	deník	k1gInPc6	deník
ani	ani	k8xC	ani
dopisech	dopis	k1gInPc6	dopis
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc4	žádný
důkaz	důkaz	k1gInSc4	důkaz
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
příliš	příliš	k6eAd1	příliš
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
opravdu	opravdu	k6eAd1	opravdu
míval	mívat	k5eAaImAgMnS	mívat
časté	častý	k2eAgInPc4d1	častý
záchvaty	záchvat	k1gInPc4	záchvat
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
podle	podle	k7c2	podle
Sadiho	Sadi	k1gMnSc2	Sadi
Ranson-Polizzottiho	Ranson-Polizzotti	k1gMnSc2	Ranson-Polizzotti
mohl	moct	k5eAaImAgMnS	moct
Carroll	Carroll	k1gMnSc1	Carroll
trpět	trpět	k5eAaImF	trpět
epilepsií	epilepsie	k1gFnSc7	epilepsie
spánkového	spánkový	k2eAgInSc2d1	spánkový
laloku	lalok	k1gInSc2	lalok
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úplné	úplný	k2eAgFnSc3d1	úplná
ztrátě	ztráta	k1gFnSc3	ztráta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
změně	změna	k1gFnSc3	změna
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pacient	pacient	k1gMnSc1	pacient
prožívá	prožívat	k5eAaImIp3nS	prožívat
mnoho	mnoho	k4c1	mnoho
obdobných	obdobný	k2eAgInPc2d1	obdobný
zážitků	zážitek	k1gInPc2	zážitek
jako	jako	k8xS	jako
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
.	.	kIx.	.
</s>
