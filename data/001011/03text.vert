<s>
Obec	obec	k1gFnSc1	obec
Kanice	Kanice	k1gFnPc4	Kanice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Drahanské	Drahanský	k2eAgFnSc6d1	Drahanská
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
,	,	kIx,	,
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
CHKO	CHKO	kA	CHKO
Moravský	moravský	k2eAgInSc4d1	moravský
kras	kras	k1gInSc4	kras
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
než	než	k8xS	než
850	[number]	k4	850
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Katastr	katastr	k1gInSc1	katastr
Kanic	Kanice	k1gFnPc2	Kanice
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
tvar	tvar	k1gInSc1	tvar
<g/>
;	;	kIx,	;
samotná	samotný	k2eAgFnSc1d1	samotná
obec	obec	k1gFnSc1	obec
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
cípu	cíp	k1gInSc6	cíp
a	a	k8xC	a
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
protažen	protažen	k2eAgInSc1d1	protažen
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
výběžkem	výběžek	k1gInSc7	výběžek
do	do	k7c2	do
území	území	k1gNnSc2	území
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
mezi	mezi	k7c7	mezi
čtvrtěmi	čtvrt	k1gFnPc7	čtvrt
Obřany	Obřana	k1gFnPc4	Obřana
a	a	k8xC	a
Líšeň	Líšeň	k1gFnSc4	Líšeň
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
zalesněné	zalesněný	k2eAgNnSc4d1	zalesněné
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mj.	mj.	kA	mj.
archiv	archiv	k1gInSc1	archiv
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
(	(	kIx(	(
<g/>
dostupný	dostupný	k2eAgInSc1d1	dostupný
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Ochoz	ochoz	k1gInSc1	ochoz
<g/>
,	,	kIx,	,
lokalita	lokalita	k1gFnSc1	lokalita
Kaničky	kanička	k1gFnSc2	kanička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hádecká	hádecký	k2eAgFnSc1d1	Hádecká
planinka	planinka	k1gFnSc1	planinka
<g/>
,	,	kIx,	,
lokalita	lokalita	k1gFnSc1	lokalita
Šumbera	Šumbera	k1gFnSc1	Šumbera
s	s	k7c7	s
pomníkem	pomník	k1gInSc7	pomník
a	a	k8xC	a
vrch	vrch	k1gInSc4	vrch
Hády	hádes	k1gInPc1	hádes
s	s	k7c7	s
televizním	televizní	k2eAgInSc7d1	televizní
vysílačem	vysílač	k1gInSc7	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
osada	osada	k1gFnSc1	osada
Kanice	Kanice	k1gFnPc4	Kanice
náležela	náležet	k5eAaImAgFnS	náležet
k	k	k7c3	k
panství	panství	k1gNnSc3	panství
Nového	Nového	k2eAgInSc2d1	Nového
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
Čeněk	Čeněk	k1gMnSc1	Čeněk
Krušina	krušina	k1gFnSc1	krušina
z	z	k7c2	z
Lichtenburku	Lichtenburk	k1gInSc2	Lichtenburk
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
hájovna	hájovna	k1gFnSc1	hájovna
a	a	k8xC	a
několik	několik	k4yIc4	několik
domů	dům	k1gInPc2	dům
pro	pro	k7c4	pro
sedláky	sedlák	k1gMnPc4	sedlák
<g/>
,	,	kIx,	,
uhlíře	uhlíř	k1gMnPc4	uhlíř
<g/>
,	,	kIx,	,
dřevaře	dřevař	k1gMnPc4	dřevař
a	a	k8xC	a
paliče	palič	k1gMnPc4	palič
vápna	vápno	k1gNnSc2	vápno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
a	a	k8xC	a
vypálení	vypálení	k1gNnSc4	vypálení
hradu	hrad	k1gInSc2	hrad
švédskými	švédský	k2eAgNnPc7d1	švédské
vojsky	vojsko	k1gNnPc7	vojsko
v	v	k7c4	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
řízení	řízení	k1gNnSc1	řízení
osady	osada	k1gFnSc2	osada
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
Pozořic	Pozořice	k1gFnPc2	Pozořice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
osada	osada	k1gFnSc1	osada
Kanice	Kanice	k1gFnPc1	Kanice
spojena	spojit	k5eAaPmNgFnS	spojit
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
správní	správní	k2eAgFnSc4d1	správní
obec	obec	k1gFnSc4	obec
s	s	k7c7	s
osadou	osada	k1gFnSc7	osada
Řícmanice	Řícmanice	k1gFnSc2	Řícmanice
se	s	k7c7	s
společnou	společný	k2eAgFnSc7d1	společná
pečetí	pečeť	k1gFnSc7	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
se	se	k3xPyFc4	se
Kanice	Kanice	k1gFnPc1	Kanice
staly	stát	k5eAaPmAgFnP	stát
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
obcí	obec	k1gFnSc7	obec
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
starostou	starosta	k1gMnSc7	starosta
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Jan	Jan	k1gMnSc1	Jan
Ševčík	Ševčík	k1gMnSc1	Ševčík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
měla	mít	k5eAaImAgFnS	mít
obec	obec	k1gFnSc1	obec
50	[number]	k4	50
domů	dům	k1gInPc2	dům
a	a	k8xC	a
354	[number]	k4	354
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
katastru	katastr	k1gInSc2	katastr
Kanic	Kanice	k1gFnPc2	Kanice
i	i	k9	i
areál	areál	k1gInSc4	areál
hotelu	hotel	k1gInSc2	hotel
Velká	velká	k1gFnSc1	velká
Klajdovka	Klajdovka	k1gFnSc1	Klajdovka
s	s	k7c7	s
přilehlou	přilehlý	k2eAgFnSc7d1	přilehlá
hájenkou	hájenka	k1gFnSc7	hájenka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nyní	nyní	k6eAd1	nyní
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
brněnské	brněnský	k2eAgFnSc3d1	brněnská
městské	městský	k2eAgFnSc3d1	městská
části	část	k1gFnSc3	část
Brno-Vinohrady	Brno-Vinohrada	k1gFnSc2	Brno-Vinohrada
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
Židenice	Židenice	k1gFnSc2	Židenice
<g/>
.	.	kIx.	.
</s>
<s>
Kanický	Kanický	k2eAgInSc1d1	Kanický
buk	buk	k1gInSc1	buk
-	-	kIx~	-
Blízko	blízko	k7c2	blízko
kraje	kraj	k1gInSc2	kraj
lesa	les	k1gInSc2	les
severně	severně	k6eAd1	severně
od	od	k7c2	od
Kanic	Kanice	k1gFnPc2	Kanice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
silnice	silnice	k1gFnSc2	silnice
k	k	k7c3	k
Babicím	Babice	k1gFnPc3	Babice
lesní	lesní	k2eAgFnSc1d1	lesní
cestou	cesta	k1gFnSc7	cesta
podél	podél	k7c2	podél
kraje	kraj	k1gInSc2	kraj
lesa	les	k1gInSc2	les
asi	asi	k9	asi
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Památný	památný	k2eAgInSc1d1	památný
strom	strom	k1gInSc1	strom
Masarykova	Masarykův	k2eAgInSc2d1	Masarykův
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
300	[number]	k4	300
<g/>
letý	letý	k2eAgMnSc1d1	letý
veterán	veterán	k1gMnSc1	veterán
s	s	k7c7	s
obvodem	obvod	k1gInSc7	obvod
boulovatého	boulovatý	k2eAgInSc2d1	boulovatý
kmene	kmen	k1gInSc2	kmen
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
přes	přes	k7c4	přes
odborné	odborný	k2eAgNnSc4d1	odborné
ošetření	ošetření	k1gNnSc4	ošetření
uschlý	uschlý	k2eAgInSc4d1	uschlý
<g/>
.	.	kIx.	.
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Obřany	Obřana	k1gFnSc2	Obřana
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Babice	babice	k1gFnSc2	babice
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kanice	Kanice	k1gFnPc1	Kanice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
