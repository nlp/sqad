<s>
REM	REM	kA	REM
fáze	fáze	k1gFnSc1	fáze
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
sny	sen	k1gInPc1	sen
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
jinak	jinak	k6eAd1	jinak
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
–	–	k?	–
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
REM	REM	kA	REM
fázi	fáze	k1gFnSc4	fáze
má	mít	k5eAaImIp3nS	mít
ptakopysk	ptakopysk	k1gMnSc1	ptakopysk
(	(	kIx(	(
<g/>
57	[number]	k4	57
%	%	kIx~	%
spánku	spánek	k1gInSc2	spánek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejkratší	krátký	k2eAgMnSc1d3	nejkratší
delfín	delfín	k1gMnSc1	delfín
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
spánku	spánek	k1gInSc2	spánek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
