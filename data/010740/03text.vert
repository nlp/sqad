<p>
<s>
Obloha	obloha	k1gFnSc1	obloha
nebo	nebo	k8xC	nebo
také	také	k9	také
nebe	nebe	k1gNnSc2	nebe
(	(	kIx(	(
<g/>
knižně	knižně	k6eAd1	knižně
nebo	nebo	k8xC	nebo
básnicky	básnicky	k6eAd1	básnicky
firmament	firmament	k1gInSc4	firmament
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
firmamentum	firmamentum	k1gNnSc1	firmamentum
vzpěra	vzpěra	k1gFnSc1	vzpěra
<g/>
,	,	kIx,	,
sloup	sloup	k1gInSc1	sloup
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nebeská	nebeský	k2eAgFnSc1d1	nebeská
klenba	klenba	k1gFnSc1	klenba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
nebo	nebo	k8xC	nebo
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
planety	planeta	k1gFnPc1	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
letadla	letadlo	k1gNnPc1	letadlo
létají	létat	k5eAaImIp3nP	létat
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Přesnější	přesný	k2eAgFnSc1d2	přesnější
definice	definice	k1gFnSc1	definice
je	být	k5eAaImIp3nS	být
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
denním	denní	k2eAgNnSc6d1	denní
světle	světlo	k1gNnSc6	světlo
je	být	k5eAaImIp3nS	být
obloha	obloha	k1gFnSc1	obloha
světle	světle	k6eAd1	světle
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
rozptylu	rozptyl	k1gInSc2	rozptyl
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Zemí	zem	k1gFnSc7	zem
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc4	žádný
modrý	modrý	k2eAgInSc4d1	modrý
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bychom	by	kYmCp1nP	by
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
slunečného	slunečný	k2eAgNnSc2d1	slunečné
počasí	počasí	k1gNnSc2	počasí
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
Rayleighovu	Rayleighův	k2eAgInSc3d1	Rayleighův
rozptylu	rozptyl	k1gInSc3	rozptyl
tmavší	tmavý	k2eAgFnSc2d2	tmavší
v	v	k7c6	v
zenitu	zenit	k1gInSc6	zenit
a	a	k8xC	a
světlejší	světlý	k2eAgInSc1d2	světlejší
na	na	k7c6	na
horizontu	horizont	k1gInSc6	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
východu	východ	k1gInSc2	východ
a	a	k8xC	a
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
červená	červenat	k5eAaImIp3nS	červenat
a	a	k8xC	a
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
je	být	k5eAaImIp3nS	být
obloha	obloha	k1gFnSc1	obloha
černá	černý	k2eAgFnSc1d1	černá
a	a	k8xC	a
posetá	posetý	k2eAgFnSc1d1	posetá
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
zataženo	zatažen	k2eAgNnSc1d1	zataženo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
vidět	vidět	k5eAaImF	vidět
Slunce	slunce	k1gNnSc4	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možno	možno	k6eAd1	možno
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
spatřit	spatřit	k5eAaPmF	spatřit
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
planety	planeta	k1gFnPc4	planeta
a	a	k8xC	a
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Měsíc	měsíc	k1gInSc1	měsíc
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
často	často	k6eAd1	často
i	i	k8xC	i
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
planety	planeta	k1gFnPc1	planeta
pak	pak	k6eAd1	pak
za	za	k7c2	za
výjimečných	výjimečný	k2eAgFnPc2d1	výjimečná
podmínek	podmínka	k1gFnPc2	podmínka
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Při	při	k7c6	při
svítání	svítání	k1gNnSc6	svítání
a	a	k8xC	a
soumraku	soumrak	k1gInSc2	soumrak
bývá	bývat	k5eAaImIp3nS	bývat
vidět	vidět	k5eAaImF	vidět
planeta	planeta	k1gFnSc1	planeta
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
poprvé	poprvé	k6eAd1	poprvé
jako	jako	k9	jako
Jitřenka	Jitřenka	k1gFnSc1	Jitřenka
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
jako	jako	k9	jako
Večernice	večernice	k1gFnSc1	večernice
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
ranní	ranní	k2eAgInSc4d1	ranní
nebo	nebo	k8xC	nebo
večerní	večerní	k2eAgInSc4d1	večerní
červánky	červánek	k1gInPc4	červánek
(	(	kIx(	(
<g/>
Zora	Zora	k1gFnSc1	Zora
nebo	nebo	k8xC	nebo
zastarale	zastarale	k6eAd1	zastarale
zoře	zorat	k5eAaPmIp3nS	zorat
je	on	k3xPp3gNnSc4	on
slovanské	slovanský	k2eAgNnSc4d1	slovanské
označení	označení	k1gNnSc4	označení
jak	jak	k8xS	jak
pro	pro	k7c4	pro
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
<g/>
)	)	kIx)	)
Jitřenku	Jitřenka	k1gFnSc4	Jitřenka
<g/>
,	,	kIx,	,
ranní	ranní	k2eAgInPc4d1	ranní
červánky	červánek	k1gInPc4	červánek
<g/>
,	,	kIx,	,
úsvit	úsvit	k1gInSc4	úsvit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
večerní	večerní	k2eAgInPc4d1	večerní
červánky	červánek	k1gInPc4	červánek
a	a	k8xC	a
Večernici	večernice	k1gFnSc4	večernice
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnSc1	bohyně
Zora	Zora	k1gFnSc1	Zora
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
římské	římský	k2eAgFnSc3d1	římská
Auroře	Aurora	k1gFnSc3	Aurora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
spatřit	spatřit	k5eAaPmF	spatřit
mnoho	mnoho	k4c4	mnoho
přírodních	přírodní	k2eAgInPc2d1	přírodní
jevů	jev	k1gInPc2	jev
jako	jako	k8xC	jako
oblaka	oblaka	k1gNnPc4	oblaka
<g/>
,	,	kIx,	,
duhu	duha	k1gFnSc4	duha
<g/>
,	,	kIx,	,
polární	polární	k2eAgFnSc4d1	polární
záři	záře	k1gFnSc4	záře
nebo	nebo	k8xC	nebo
blesk	blesk	k1gInSc4	blesk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
se	se	k3xPyFc4	se
jako	jako	k9	jako
obloha	obloha	k1gFnSc1	obloha
označuje	označovat	k5eAaImIp3nS	označovat
nebeská	nebeský	k2eAgFnSc1d1	nebeská
sféra	sféra	k1gFnSc1	sféra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
plocha	plocha	k1gFnSc1	plocha
nad	nad	k7c7	nad
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
Slunce	slunce	k1gNnPc1	slunce
<g/>
,	,	kIx,	,
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
planety	planeta	k1gFnPc1	planeta
a	a	k8xC	a
měsíce	měsíc	k1gInPc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
sféra	sféra	k1gFnSc1	sféra
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c6	na
části	část	k1gFnSc6	část
nazývané	nazývaný	k2eAgNnSc4d1	nazývané
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obloha	obloha	k1gFnSc1	obloha
v	v	k7c6	v
náboženstvích	náboženství	k1gNnPc6	náboženství
==	==	k?	==
</s>
</p>
<p>
<s>
Obloha	obloha	k1gFnSc1	obloha
hraje	hrát	k5eAaImIp3nS	hrát
silnou	silný	k2eAgFnSc4d1	silná
roli	role	k1gFnSc4	role
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
náboženstvích	náboženství	k1gNnPc6	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
nebem	nebe	k1gNnSc7	nebe
–	–	k?	–
místem	místo	k1gNnSc7	místo
posmrtného	posmrtný	k2eAgInSc2d1	posmrtný
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nP	sídlet
božstva	božstvo	k1gNnPc4	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Číňany	Číňan	k1gMnPc4	Číňan
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
bohem	bůh	k1gMnSc7	bůh
stírá	stírat	k5eAaImIp3nS	stírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bohové	bůh	k1gMnPc1	bůh
nebe	nebe	k1gNnSc2	nebe
===	===	k?	===
</s>
</p>
<p>
<s>
Úranos	Úranos	k1gInSc1	Úranos
<g/>
,	,	kIx,	,
Zeus	Zeus	k1gInSc1	Zeus
(	(	kIx(	(
<g/>
Řecká	řecký	k2eAgFnSc1d1	řecká
mytologie	mytologie	k1gFnSc1	mytologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ouranos	Ouranos	k1gMnSc1	Ouranos
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
Římská	římský	k2eAgFnSc1d1	římská
mytologie	mytologie	k1gFnSc1	mytologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ukko	Ukko	k1gNnSc1	Ukko
(	(	kIx(	(
<g/>
Finská	finský	k2eAgFnSc1d1	finská
mytologie	mytologie	k1gFnSc1	mytologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nut	Nut	k?	Nut
(	(	kIx(	(
<g/>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
mytologie	mytologie	k1gFnSc1	mytologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
obloha	obloha	k1gFnSc1	obloha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
obloha	obloha	k1gFnSc1	obloha
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Nebe	nebe	k1gNnSc1	nebe
</s>
</p>
