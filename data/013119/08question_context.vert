<s>
Rita	Rita	k1gFnSc1	Rita
Reys	Reysa	k1gFnPc2	Reysa
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1924	[number]	k4	1924
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
−	−	k?	−
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
Breukelen	Breukelna	k1gFnPc2	Breukelna
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
jazzová	jazzový	k2eAgFnSc1d1	jazzová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nedostala	dostat	k5eNaPmAgFnS	dostat
k	k	k7c3	k
jazzu	jazz	k1gInSc3	jazz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejprve	nejprve	k6eAd1	nejprve
ke	k	k7c3	k
klasické	klasický	k2eAgFnSc3d1	klasická
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
potkala	potkat	k5eAaPmAgFnS	potkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
bubeníkem	bubeník	k1gMnSc7	bubeník
Wesselem	Wessel	k1gMnSc7	Wessel
Ilckenem	Ilcken	k1gMnSc7	Ilcken
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
vdala	vdát	k5eAaPmAgFnS	vdát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
