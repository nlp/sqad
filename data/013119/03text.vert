<p>
<s>
Rita	Rita	k1gFnSc1	Rita
Reys	Reysa	k1gFnPc2	Reysa
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1924	[number]	k4	1924
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
−	−	k?	−
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
Breukelen	Breukelna	k1gFnPc2	Breukelna
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
jazzová	jazzový	k2eAgFnSc1d1	jazzová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nedostala	dostat	k5eNaPmAgFnS	dostat
k	k	k7c3	k
jazzu	jazz	k1gInSc3	jazz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejprve	nejprve	k6eAd1	nejprve
ke	k	k7c3	k
klasické	klasický	k2eAgFnSc3d1	klasická
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
potkala	potkat	k5eAaPmAgFnS	potkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
bubeníkem	bubeník	k1gMnSc7	bubeník
Wesselem	Wessel	k1gMnSc7	Wessel
Ilckenem	Ilcken	k1gMnSc7	Ilcken
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
vdala	vdát	k5eAaPmAgFnS	vdát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
hrála	hrát	k5eAaImAgFnS	hrát
s	s	k7c7	s
Ilckenovou	Ilckenový	k2eAgFnSc7d1	Ilckenový
kapelou	kapela	k1gFnSc7	kapela
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
měla	mít	k5eAaImAgFnS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
soubor	soubor	k1gInSc4	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
Donald	Donald	k1gMnSc1	Donald
Byrd	Byrd	k1gMnSc1	Byrd
<g/>
,	,	kIx,	,
Hank	Hank	k1gMnSc1	Hank
Mobley	Moblea	k1gFnSc2	Moblea
nebo	nebo	k8xC	nebo
Horace	Horace	k1gFnSc2	Horace
Silver	Silvra	k1gFnPc2	Silvra
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
klavírista	klavírista	k1gMnSc1	klavírista
Pim	Pim	k1gMnSc1	Pim
Jacobs	Jacobs	k1gInSc4	Jacobs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rita	rito	k1gNnSc2	rito
Reys	Reysa	k1gFnPc2	Reysa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
