<s>
Harry	Harr	k1gInPc1	Harr
Sinclair	Sinclaira	k1gFnPc2	Sinclaira
Lewis	Lewis	k1gFnPc2	Lewis
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
Sauk	Sauk	k1gInSc1	Sauk
Centre	centr	k1gInSc5	centr
<g/>
,	,	kIx,	,
Minnesota	Minnesota	k1gFnSc1	Minnesota
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
Sinclair	Sinclair	k1gInSc1	Sinclair
Lewis	Lewis	k1gFnSc2	Lewis
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
v	v	k7c6	v
Sauk	Sauka	k1gFnPc2	Sauka
Centre	centr	k1gInSc5	centr
v	v	k7c6	v
Minnesotě	Minnesota	k1gFnSc6	Minnesota
jako	jako	k8xC	jako
třetí	třetí	k4xOgMnSc1	třetí
syn	syn	k1gMnSc1	syn
venkovského	venkovský	k2eAgMnSc2d1	venkovský
doktora	doktor	k1gMnSc2	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
třinácti	třináct	k4xCc2	třináct
let	léto	k1gNnPc2	léto
utekl	utéct	k5eAaPmAgInS	utéct
z	z	k7c2	z
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
stát	stát	k5eAaPmF	stát
bubeníkem	bubeník	k1gMnSc7	bubeník
ve	v	k7c6	v
španělsko-americké	španělskomerický	k2eAgFnSc6d1	španělsko-americká
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
jej	on	k3xPp3gMnSc4	on
brzy	brzy	k6eAd1	brzy
našel	najít	k5eAaPmAgMnS	najít
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
železničním	železniční	k2eAgNnSc6d1	železniční
skladišti	skladiště	k1gNnSc6	skladiště
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Oberlinově	Oberlinově	k1gFnSc6	Oberlinově
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
na	na	k7c6	na
Yaleově	Yaleův	k2eAgFnSc6d1	Yaleova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
přispívat	přispívat	k5eAaImF	přispívat
do	do	k7c2	do
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
časopisu	časopis	k1gInSc2	časopis
Yale	Yale	k1gNnSc2	Yale
Literary	Literara	k1gFnSc2	Literara
Magazine	Magazin	k1gInSc5	Magazin
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
studií	studie	k1gFnPc2	studie
cestoval	cestovat	k5eAaImAgInS	cestovat
(	(	kIx(	(
<g/>
navštívil	navštívit	k5eAaPmAgInS	navštívit
Anglii	Anglie	k1gFnSc4	Anglie
a	a	k8xC	a
Panamu	Panama	k1gFnSc4	Panama
<g/>
)	)	kIx)	)
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
zřízenec	zřízenec	k1gMnSc1	zřízenec
v	v	k7c6	v
utopické	utopický	k2eAgFnSc6d1	utopická
osadě	osada	k1gFnSc6	osada
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Uptona	Upton	k1gMnSc2	Upton
Sinclaira	Sinclair	k1gMnSc2	Sinclair
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
studia	studio	k1gNnSc2	studio
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
žurnalista	žurnalista	k1gMnSc1	žurnalista
<g/>
,	,	kIx,	,
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
USA	USA	kA	USA
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgInS	psát
nepříliš	příliš	k6eNd1	příliš
hodnotné	hodnotný	k2eAgInPc4d1	hodnotný
romantické	romantický	k2eAgInPc4d1	romantický
příběhy	příběh	k1gInPc4	příběh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
o	o	k7c6	o
rytířích	rytíř	k1gMnPc6	rytíř
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
své	své	k1gNnSc4	své
literární	literární	k2eAgInPc4d1	literární
nápady	nápad	k1gInPc4	nápad
prodával	prodávat	k5eAaImAgMnS	prodávat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jacku	Jacek	k1gMnSc3	Jacek
Londonovi	London	k1gMnSc3	London
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
publikovanou	publikovaný	k2eAgFnSc7d1	publikovaná
knihou	kniha	k1gFnSc7	kniha
byl	být	k5eAaImAgInS	být
chlapecký	chlapecký	k2eAgInSc1d1	chlapecký
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
příběh	příběh	k1gInSc1	příběh
Výlet	výlet	k1gInSc1	výlet
a	a	k8xC	a
letadlo	letadlo	k1gNnSc1	letadlo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vydal	vydat	k5eAaPmAgInS	vydat
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Tom	Tom	k1gMnSc1	Tom
Graham	Graham	k1gMnSc1	Graham
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
usídlil	usídlit	k5eAaPmAgMnS	usídlit
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
redaktorkou	redaktorka	k1gFnSc7	redaktorka
Grace	Grace	k1gFnSc2	Grace
Livingston	Livingston	k1gInSc1	Livingston
Heggerovou	Heggerová	k1gFnSc4	Heggerová
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Wells	Wellsa	k1gFnPc2	Wellsa
(	(	kIx(	(
<g/>
manželství	manželství	k1gNnSc1	manželství
však	však	k9	však
skončilo	skončit	k5eAaPmAgNnS	skončit
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
rozvodem	rozvod	k1gInSc7	rozvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
novinářkou	novinářka	k1gFnSc7	novinářka
Dorothy	Dorotha	k1gFnSc2	Dorotha
Thompson	Thompson	k1gInSc1	Thompson
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
syna	syn	k1gMnSc4	syn
Michaela	Michael	k1gMnSc4	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
vydal	vydat	k5eAaPmAgInS	vydat
Lewis	Lewis	k1gInSc4	Lewis
pět	pět	k4xCc4	pět
humorných	humorný	k2eAgInPc2d1	humorný
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
satirických	satirický	k2eAgInPc2d1	satirický
románů	román	k1gInPc2	román
napsaných	napsaný	k2eAgInPc2d1	napsaný
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
H.	H.	kA	H.
G.	G.	kA	G.
Wellse	Wells	k1gMnSc2	Wells
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
líčí	líčit	k5eAaImIp3nS	líčit
příběhy	příběh	k1gInPc4	příběh
drobných	drobný	k2eAgMnPc2d1	drobný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jízlivý	jízlivý	k2eAgMnSc1d1	jízlivý
satirik	satirik	k1gMnSc1	satirik
<g/>
,	,	kIx,	,
kritizující	kritizující	k2eAgFnSc6d1	kritizující
neřesti	neřest	k1gFnSc6	neřest
střední	střední	k1gMnSc1	střední
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
až	až	k9	až
v	v	k7c6	v
románech	román	k1gInPc6	román
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
Babbitt	Babbitt	k1gMnSc1	Babbitt
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
zaslouženou	zasloužený	k2eAgFnSc4d1	zasloužená
senzaci	senzace	k1gFnSc4	senzace
a	a	k8xC	a
Lewis	Lewis	k1gInSc1	Lewis
byl	být	k5eAaImAgInS	být
literární	literární	k2eAgFnSc7d1	literární
kritikou	kritika	k1gFnSc7	kritika
společně	společně	k6eAd1	společně
s	s	k7c7	s
Sherwoodem	Sherwood	k1gMnSc7	Sherwood
Andersonem	Anderson	k1gMnSc7	Anderson
a	a	k8xC	a
Edgarem	Edgar	k1gMnSc7	Edgar
Lee	Lea	k1gFnSc6	Lea
Mastersem	Masterso	k1gNnSc7	Masterso
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
line	linout	k5eAaImIp3nS	linout
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vzpoury	vzpoura	k1gFnSc2	vzpoura
proti	proti	k7c3	proti
maloměstu	maloměsto	k1gNnSc3	maloměsto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovaly	následovat	k5eAaImAgInP	následovat
romány	román	k1gInPc1	román
Arrowsmith	Arrowsmith	k1gInSc1	Arrowsmith
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
z	z	k7c2	z
lékařského	lékařský	k2eAgNnSc2d1	lékařské
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
obdržel	obdržet	k5eAaPmAgInS	obdržet
za	za	k7c2	za
něj	on	k3xPp3gMnSc2	on
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1	Pulitzerova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
)	)	kIx)	)
a	a	k8xC	a
Elmer	Elmer	k1gInSc1	Elmer
Gantry	Gantr	k1gInPc1	Gantr
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
vysmál	vysmát	k5eAaPmAgMnS	vysmát
klerikálnímu	klerikální	k2eAgNnSc3d1	klerikální
pokrytectví	pokrytectví	k1gNnSc3	pokrytectví
<g/>
.	.	kIx.	.
</s>
<s>
Díla	dílo	k1gNnPc1	dílo
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
znal	znát	k5eAaImAgMnS	znát
prezidenta	prezident	k1gMnSc4	prezident
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
a	a	k8xC	a
Továrník	továrník	k1gMnSc1	továrník
Dodsworth	Dodsworth	k1gMnSc1	Dodsworth
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
již	již	k6eAd1	již
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
humornějším	humorný	k2eAgInSc7d2	humorný
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
zobrazením	zobrazení	k1gNnSc7	zobrazení
kladných	kladný	k2eAgInPc2d1	kladný
středostavovských	středostavovský	k2eAgInPc2d1	středostavovský
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
prvnímu	první	k4xOgMnSc3	první
americkému	americký	k2eAgMnSc3d1	americký
spisovateli	spisovatel	k1gMnSc3	spisovatel
<g/>
,	,	kIx,	,
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
působivé	působivý	k2eAgNnSc1d1	působivé
<g/>
,	,	kIx,	,
živé	živý	k2eAgNnSc1d1	živé
umělecké	umělecký	k2eAgNnSc1d1	umělecké
líčení	líčení	k1gNnSc1	líčení
a	a	k8xC	a
nadání	nadání	k1gNnSc1	nadání
s	s	k7c7	s
vtipem	vtip	k1gInSc7	vtip
a	a	k8xC	a
humorem	humor	k1gInSc7	humor
vytvářet	vytvářet	k5eAaImF	vytvářet
nové	nový	k2eAgFnPc4d1	nová
postavy	postava	k1gFnPc4	postava
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sinclair	Sinclair	k1gInSc1	Sinclair
Lewis	Lewis	k1gFnSc2	Lewis
napsal	napsat	k5eAaBmAgInS	napsat
celkem	celkem	k6eAd1	celkem
dvacet	dvacet	k4xCc4	dvacet
dva	dva	k4xCgInPc4	dva
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgNnPc1d2	pozdější
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
již	již	k6eAd1	již
méně	málo	k6eAd2	málo
výrazná	výrazný	k2eAgFnSc1d1	výrazná
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
Anna	Anna	k1gFnSc1	Anna
Vickersová	Vickersová	k1gFnSc1	Vickersová
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
o	o	k7c6	o
průbojné	průbojný	k2eAgFnSc6d1	průbojná
feministce	feministka	k1gFnSc6	feministka
<g/>
,	,	kIx,	,
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
stát	stát	k5eAaPmF	stát
nemůže	moct	k5eNaImIp3nS	moct
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
varující	varující	k2eAgNnSc1d1	varující
před	před	k7c7	před
růstem	růst	k1gInSc7	růst
fašismu	fašismus	k1gInSc2	fašismus
a	a	k8xC	a
Krev	krev	k1gFnSc1	krev
královská	královský	k2eAgFnSc1d1	královská
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kritizující	kritizující	k2eAgInSc1d1	kritizující
rasistický	rasistický	k2eAgInSc4d1	rasistický
útisk	útisk	k1gInSc4	útisk
černochů	černoch	k1gMnPc2	černoch
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nesmírným	smírný	k2eNgInSc7d1	nesmírný
veřejným	veřejný	k2eAgInSc7d1	veřejný
ohlasem	ohlas	k1gInSc7	ohlas
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
kontrastovala	kontrastovat	k5eAaImAgFnS	kontrastovat
autorova	autorův	k2eAgFnSc1d1	autorova
osobní	osobní	k2eAgFnSc1d1	osobní
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
a	a	k8xC	a
pocit	pocit	k1gInSc1	pocit
nezakotvenosti	nezakotvenost	k1gFnSc2	nezakotvenost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gInSc4	on
hnal	hnát	k5eAaImAgInS	hnát
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1915	[number]	k4	1915
až	až	k6eAd1	až
1930	[number]	k4	1930
navštívil	navštívit	k5eAaPmAgMnS	navštívit
čtyřicet	čtyřicet	k4xCc1	čtyřicet
států	stát	k1gInPc2	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
,	,	kIx,	,
čtrnáct	čtrnáct	k4xCc4	čtrnáct
evropských	evropský	k2eAgFnPc2d1	Evropská
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
jihoamerické	jihoamerický	k2eAgFnPc4d1	jihoamerická
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
skončilo	skončit	k5eAaPmAgNnS	skončit
rozvodem	rozvod	k1gInSc7	rozvod
i	i	k9	i
jeho	jeho	k3xOp3gNnSc1	jeho
druhé	druhý	k4xOgNnSc1	druhý
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
reportérkou	reportérka	k1gFnSc7	reportérka
Dorothy	Dorotha	k1gFnSc2	Dorotha
Thopsonovu	Thopsonův	k2eAgFnSc4d1	Thopsonův
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
padl	padnout	k5eAaPmAgInS	padnout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Wells	Wellsa	k1gFnPc2	Wellsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lewisově	Lewisův	k2eAgInSc6d1	Lewisův
životě	život	k1gInSc6	život
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
stále	stále	k6eAd1	stále
významnější	významný	k2eAgFnSc4d2	významnější
roli	role	k1gFnSc4	role
alkohol	alkohol	k1gInSc4	alkohol
a	a	k8xC	a
svá	svůj	k3xOyFgNnPc4	svůj
poslední	poslední	k2eAgNnPc4d1	poslední
léta	léto	k1gNnPc4	léto
prožíval	prožívat	k5eAaImAgMnS	prožívat
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
neklidném	klidný	k2eNgNnSc6d1	neklidné
cestování	cestování	k1gNnSc6	cestování
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
na	na	k7c4	na
srdeční	srdeční	k2eAgFnSc4d1	srdeční
chorobu	choroba	k1gFnSc4	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Launcelot	Launcelot	k1gInSc1	Launcelot
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Lancelot	Lancelot	k1gInSc1	Lancelot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
Hike	Hike	k1gFnSc1	Hike
and	and	k?	and
the	the	k?	the
Aeroplane	Aeroplan	k1gMnSc5	Aeroplan
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Výlet	výlet	k1gInSc4	výlet
a	a	k8xC	a
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
Lewisova	Lewisův	k2eAgFnSc1d1	Lewisova
vydaná	vydaný	k2eAgFnSc1d1	vydaná
kniha	kniha	k1gFnSc1	kniha
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Tom	Tom	k1gMnSc1	Tom
Graham	Graham	k1gMnSc1	Graham
<g/>
.	.	kIx.	.
</s>
<s>
Our	Our	k?	Our
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Wrenn	Wrenn	k1gNnSc1	Wrenn
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Náš	náš	k3xOp1gMnSc1	náš
pan	pan	k1gMnSc1	pan
Wrenn	Wrenn	k1gMnSc1	Wrenn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
romantická	romantický	k2eAgFnSc1d1	romantická
dobrodružství	dobrodružství	k1gNnSc3	dobrodružství
mírného	mírný	k2eAgMnSc2d1	mírný
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Trial	trial	k1gInSc1	trial
of	of	k?	of
the	the	k?	the
Hawk	Hawk	k1gInSc1	Hawk
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
Jestřábí	jestřábí	k2eAgInSc1d1	jestřábí
stopou	stopa	k1gFnSc7	stopa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
vážnosti	vážnost	k1gFnSc6	vážnost
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Innocents	Innocentsa	k1gFnPc2	Innocentsa
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Neviňátka	neviňátko	k1gNnSc2	neviňátko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Ghost	Ghost	k1gFnSc1	Ghost
Patrol	Patrol	k?	Patrol
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Hlídka	hlídka	k1gFnSc1	hlídka
duchů	duch	k1gMnPc2	duch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Job	Job	k1gMnSc1	Job
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Free	Free	k1gFnSc1	Free
Air	Air	k1gFnSc1	Air
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Vzduch	vzduch	k1gInSc1	vzduch
zdarma	zdarma	k6eAd1	zdarma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úsměvný	úsměvný	k2eAgInSc1d1	úsměvný
románek	románek	k1gInSc1	románek
<g/>
,	,	kIx,	,
jakési	jakýsi	k3yIgNnSc4	jakýsi
pikareskní	pikareskní	k2eAgNnSc4d1	pikareskní
zřetězení	zřetězení	k1gNnSc4	zřetězení
epizod	epizoda	k1gFnPc2	epizoda
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
přes	přes	k7c4	přes
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Moths	Moths	k1gInSc1	Moths
in	in	k?	in
the	the	k?	the
Arc	Arc	k1gMnSc1	Arc
Light	Light	k1gMnSc1	Light
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Motýli	motýl	k1gMnPc1	motýl
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
obloukovek	obloukovka	k1gFnPc2	obloukovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
Main	Main	k1gMnSc1	Main
Street	Street	k1gMnSc1	Street
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
autorových	autorův	k2eAgInPc2d1	autorův
stěžejních	stěžejní	k2eAgInPc2d1	stěžejní
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
společenská	společenský	k2eAgFnSc1d1	společenská
studie	studie	k1gFnSc1	studie
o	o	k7c6	o
americkém	americký	k2eAgNnSc6d1	americké
maloměšťáctví	maloměšťáctví	k1gNnSc6	maloměšťáctví
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
za	za	k7c4	za
Paní	paní	k1gFnSc4	paní
Bovaryovou	Bovaryová	k1gFnSc4	Bovaryová
americké	americký	k2eAgFnSc2d1	americká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
vysokoškolačka	vysokoškolačka	k1gFnSc1	vysokoškolačka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
ideálů	ideál	k1gInPc2	ideál
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
provdá	provdat	k5eAaPmIp3nS	provdat
za	za	k7c4	za
průměrného	průměrný	k2eAgMnSc4d1	průměrný
lékaře	lékař	k1gMnSc4	lékař
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
zapadlého	zapadlý	k2eAgNnSc2d1	zapadlé
městečka	městečko	k1gNnSc2	městečko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
intelektuálně	intelektuálně	k6eAd1	intelektuálně
pozvednout	pozvednout	k5eAaPmF	pozvednout
místní	místní	k2eAgFnSc4d1	místní
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
její	její	k3xOp3gFnSc1	její
snaha	snaha	k1gFnSc1	snaha
ztroskotává	ztroskotávat	k5eAaImIp3nS	ztroskotávat
na	na	k7c6	na
pokrytecké	pokrytecký	k2eAgFnSc6d1	pokrytecká
morálce	morálka	k1gFnSc6	morálka
a	a	k8xC	a
omezenosti	omezenost	k1gFnSc6	omezenost
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
končí	končit	k5eAaImIp3nS	končit
hořkým	hořký	k2eAgInSc7d1	hořký
kompromisem	kompromis	k1gInSc7	kompromis
<g/>
.	.	kIx.	.
</s>
<s>
Babbitt	Babbitt	k1gInSc1	Babbitt
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrcholný	vrcholný	k2eAgInSc1d1	vrcholný
autorův	autorův	k2eAgInSc4d1	autorův
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
typ	typ	k1gInSc1	typ
průměrného	průměrný	k2eAgMnSc2d1	průměrný
měšťáka	měšťák	k1gMnSc2	měšťák
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
hlavním	hlavní	k2eAgInSc7d1	hlavní
ideálem	ideál	k1gInSc7	ideál
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
bydlo	bydlo	k1gNnSc1	bydlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInPc6	jehož
osudech	osud	k1gInPc6	osud
a	a	k8xC	a
myšlení	myšlení	k1gNnSc2	myšlení
zpodobnil	zpodobnit	k5eAaPmAgInS	zpodobnit
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
názory	názor	k1gInPc7	názor
dobře	dobře	k6eAd1	dobře
situovaných	situovaný	k2eAgMnPc2d1	situovaný
občanů	občan	k1gMnPc2	občan
amerického	americký	k2eAgNnSc2d1	americké
maloměsta	maloměsto	k1gNnSc2	maloměsto
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
babbitt	babbitt	k1gInSc1	babbitt
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
slovníku	slovník	k1gInSc2	slovník
americké	americký	k2eAgFnSc2d1	americká
angličtiny	angličtina	k1gFnSc2	angličtina
jako	jako	k8xS	jako
označení	označení	k1gNnSc2	označení
šosáka	šosák	k1gMnSc2	šosák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
průměrný	průměrný	k2eAgMnSc1d1	průměrný
obchodník	obchodník	k1gMnSc1	obchodník
začne	začít	k5eAaPmIp3nS	začít
bouřit	bouřit	k5eAaImF	bouřit
proti	proti	k7c3	proti
zištnosti	zištnost	k1gFnSc3	zištnost
<g/>
,	,	kIx,	,
samolibosti	samolibost	k1gFnSc3	samolibost
<g/>
,	,	kIx,	,
duchaprázdnému	duchaprázdný	k2eAgNnSc3d1	duchaprázdné
požitkářství	požitkářství	k1gNnSc3	požitkářství
a	a	k8xC	a
falešnému	falešný	k2eAgInSc3d1	falešný
optimismu	optimismus	k1gInSc3	optimismus
svého	svůj	k3xOyFgNnSc2	svůj
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
marně	marně	k6eAd1	marně
<g/>
.	.	kIx.	.
</s>
<s>
Arrowsmith	Arrowsmith	k1gInSc1	Arrowsmith
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
o	o	k7c6	o
lékaři	lékař	k1gMnSc6	lékař
<g/>
,	,	kIx,	,
bakteriologu	bakteriolog	k1gMnSc6	bakteriolog
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
životní	životní	k2eAgFnSc1d1	životní
vášní	vášeň	k1gFnSc7	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
s	s	k7c7	s
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
znalostí	znalost	k1gFnSc7	znalost
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
opřen	opřen	k2eAgMnSc1d1	opřen
o	o	k7c4	o
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
pomoc	pomoc	k1gFnSc4	pomoc
známého	známý	k2eAgMnSc2d1	známý
autora	autor	k1gMnSc2	autor
populárně	populárně	k6eAd1	populárně
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
prací	práce	k1gFnPc2	práce
Paula	Paul	k1gMnSc2	Paul
de	de	k?	de
Kruifa	Kruif	k1gMnSc2	Kruif
<g/>
,	,	kIx,	,
líčí	líčit	k5eAaImIp3nS	líčit
životní	životní	k2eAgInSc4d1	životní
a	a	k8xC	a
vědecký	vědecký	k2eAgInSc4d1	vědecký
vzestup	vzestup	k1gInSc4	vzestup
mladého	mladý	k2eAgMnSc2d1	mladý
<g/>
,	,	kIx,	,
energického	energický	k2eAgMnSc2d1	energický
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vynalezeným	vynalezený	k2eAgInSc7d1	vynalezený
preparátem	preparát	k1gInSc7	preparát
zdolá	zdolat	k5eAaPmIp3nS	zdolat
epidemii	epidemie	k1gFnSc4	epidemie
moru	mor	k1gInSc2	mor
na	na	k7c6	na
tropickém	tropický	k2eAgInSc6d1	tropický
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
bránit	bránit	k5eAaImF	bránit
proti	proti	k7c3	proti
možnému	možný	k2eAgNnSc3d1	možné
zneužití	zneužití	k1gNnSc3	zneužití
výsledků	výsledek	k1gInPc2	výsledek
svých	svůj	k3xOyFgInPc2	svůj
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
díle	dílo	k1gNnSc6	dílo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
autor	autor	k1gMnSc1	autor
kritikem	kritik	k1gMnSc7	kritik
některých	některý	k3yIgInPc2	některý
aspektů	aspekt	k1gInPc2	aspekt
amerického	americký	k2eAgInSc2d1	americký
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
celková	celkový	k2eAgFnSc1d1	celková
koncepce	koncepce	k1gFnSc1	koncepce
není	být	k5eNaImIp3nS	být
již	již	k6eAd1	již
tak	tak	k6eAd1	tak
výrazně	výrazně	k6eAd1	výrazně
kritická	kritický	k2eAgFnSc1d1	kritická
<g/>
.	.	kIx.	.
</s>
<s>
Mantrap	Mantrap	k1gInSc1	Mantrap
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Past	past	k1gFnSc1	past
na	na	k7c4	na
muže	muž	k1gMnPc4	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
román	román	k1gInSc1	román
o	o	k7c6	o
dvou	dva	k4xCgMnPc6	dva
Newyorčanech	Newyorčan	k1gMnPc6	Newyorčan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
do	do	k7c2	do
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Kanady	Kanada	k1gFnSc2	Kanada
na	na	k7c4	na
kanoistickou	kanoistický	k2eAgFnSc4d1	kanoistická
túru	túra	k1gFnSc4	túra
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Mantrap	Mantrap	k1gInSc4	Mantrap
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
název	název	k1gInSc1	název
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
Past	past	k1gFnSc1	past
na	na	k7c4	na
muže	muž	k1gMnPc4	muž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Domnívali	domnívat	k5eAaImAgMnP	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
setkají	setkat	k5eAaPmIp3nP	setkat
s	s	k7c7	s
opravdovými	opravdový	k2eAgMnPc7d1	opravdový
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
hříčkou	hříčka	k1gFnSc7	hříčka
obhroublých	obhroublý	k2eAgMnPc2d1	obhroublý
zálesáků	zálesák	k1gMnPc2	zálesák
a	a	k8xC	a
lovců	lovec	k1gMnPc2	lovec
kožešin	kožešina	k1gFnPc2	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Elmer	Elmer	k1gInSc1	Elmer
Gantry	Gantr	k1gInPc1	Gantr
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vtipná	vtipný	k2eAgFnSc1d1	vtipná
i	i	k8xC	i
příkrá	příkrý	k2eAgFnSc1d1	příkrá
románová	románový	k2eAgFnSc1d1	románová
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
hrabivé	hrabivý	k2eAgFnPc4d1	hrabivá
americké	americký	k2eAgFnPc4d1	americká
církve	církev	k1gFnPc4	církev
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
bůh	bůh	k1gMnSc1	bůh
není	být	k5eNaImIp3nS	být
ničím	ničí	k3xOyNgNnSc7	ničí
jiným	jiné	k1gNnSc7	jiné
než	než	k8xS	než
prostředkem	prostředek	k1gInSc7	prostředek
k	k	k7c3	k
naprosto	naprosto	k6eAd1	naprosto
bezbožnému	bezbožný	k2eAgNnSc3d1	bezbožné
obchodování	obchodování	k1gNnSc3	obchodování
<g/>
,	,	kIx,	,
na	na	k7c4	na
náboženský	náboženský	k2eAgInSc4d1	náboženský
fanatismus	fanatismus	k1gInSc4	fanatismus
<g/>
,	,	kIx,	,
klerikální	klerikální	k2eAgNnSc4d1	klerikální
pokrytectví	pokrytectví	k1gNnSc4	pokrytectví
a	a	k8xC	a
kariérnictví	kariérnictví	k1gNnSc4	kariérnictví
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kandidát	kandidát	k1gMnSc1	kandidát
kněžství	kněžství	k1gNnSc2	kněžství
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
kněz	kněz	k1gMnSc1	kněz
Elmer	Elmer	k1gMnSc1	Elmer
Gantry	Gantr	k1gInPc4	Gantr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
neodřekne	odřeknout	k5eNaPmIp3nS	odřeknout
žádnou	žádný	k3yNgFnSc4	žádný
z	z	k7c2	z
radostí	radost	k1gFnPc2	radost
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
prožívá	prožívat	k5eAaImIp3nS	prožívat
četná	četný	k2eAgNnPc4d1	četné
milostná	milostný	k2eAgNnPc4d1	milostné
dobrodružství	dobrodružství	k1gNnPc4	dobrodružství
a	a	k8xC	a
bezohledně	bezohledně	k6eAd1	bezohledně
jde	jít	k5eAaImIp3nS	jít
za	za	k7c7	za
mocí	moc	k1gFnSc7	moc
a	a	k8xC	a
slávou	sláva	k1gFnSc7	sláva
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
duchovních	duchovní	k2eAgFnPc2d1	duchovní
aspirací	aspirace	k1gFnPc2	aspirace
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Man	Man	k1gMnSc1	Man
Who	Who	k1gMnSc2	Who
Knew	Knew	k1gMnSc2	Knew
Coolidge	Coolidg	k1gMnSc2	Coolidg
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
znal	znát	k5eAaImAgMnS	znát
prezidenta	prezident	k1gMnSc4	prezident
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
románová	románový	k2eAgFnSc1d1	románová
burleska	burleska	k1gFnSc1	burleska
<g/>
,	,	kIx,	,
zachycující	zachycující	k2eAgFnSc4d1	zachycující
groteskní	groteskní	k2eAgFnSc4d1	groteskní
postavičku	postavička	k1gFnSc4	postavička
úspěšného	úspěšný	k2eAgMnSc2d1	úspěšný
amerického	americký	k2eAgMnSc2d1	americký
byznysmena	byznysmen	k1gMnSc2	byznysmen
a	a	k8xC	a
neúnavného	únavný	k2eNgMnSc2d1	neúnavný
žvanila	žvanil	k1gMnSc2	žvanil
<g/>
.	.	kIx.	.
</s>
<s>
Formou	forma	k1gFnSc7	forma
jeho	on	k3xPp3gInSc2	on
monologu	monolog	k1gInSc2	monolog
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
díle	díl	k1gInSc6	díl
podána	podat	k5eAaPmNgFnS	podat
sžíravá	sžíravý	k2eAgFnSc1d1	sžíravá
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
amerického	americký	k2eAgMnSc4d1	americký
měšťáka	měšťák	k1gMnSc4	měšťák
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
dolarovou	dolarový	k2eAgFnSc4d1	dolarová
"	"	kIx"	"
<g/>
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
ubohost	ubohost	k1gFnSc4	ubohost
<g/>
,	,	kIx,	,
pokrytectví	pokrytectví	k1gNnSc4	pokrytectví
<g/>
,	,	kIx,	,
dunivé	dunivý	k2eAgNnSc4d1	dunivé
vlastenectví	vlastenectví	k1gNnSc4	vlastenectví
a	a	k8xC	a
předstíranou	předstíraný	k2eAgFnSc4d1	předstíraná
demokratičnost	demokratičnost	k1gFnSc4	demokratičnost
<g/>
.	.	kIx.	.
</s>
<s>
Dodsworth	Dodsworth	k1gInSc1	Dodsworth
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Továrník	továrník	k1gMnSc1	továrník
Dodsworth	Dodsworth	k1gMnSc1	Dodsworth
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
A	a	k8xC	a
Letter	Letter	k1gMnSc1	Letter
from	from	k1gMnSc1	from
the	the	k?	the
Queen	Queen	k1gInSc1	Queen
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Dopis	dopis	k1gInSc1	dopis
od	od	k7c2	od
královny	královna	k1gFnSc2	královna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
Little	Little	k1gFnSc1	Little
Bear	Bear	k1gInSc1	Bear
Bongo	bongo	k1gNnSc1	bongo
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Medvídek	medvídek	k1gMnSc1	medvídek
Bongo	bongo	k1gNnSc4	bongo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Let	let	k1gInSc1	let
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Play	play	k0	play
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
na	na	k7c6	na
toulkách	toulka	k1gFnPc6	toulka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
Ann	Ann	k1gFnSc1	Ann
Vickers	Vickersa	k1gFnPc2	Vickersa
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Vickersová	Vickersová	k1gFnSc1	Vickersová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
líčící	líčící	k2eAgFnSc2d1	líčící
sociální	sociální	k2eAgFnSc2d1	sociální
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
poměry	poměra	k1gFnSc2	poměra
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
letech	let	k1gInPc6	let
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
hrdinkou	hrdinka	k1gFnSc7	hrdinka
je	být	k5eAaImIp3nS	být
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
,	,	kIx,	,
energická	energický	k2eAgFnSc1d1	energická
a	a	k8xC	a
průbojná	průbojný	k2eAgFnSc1d1	průbojná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nalézá	nalézat	k5eAaImIp3nS	nalézat
uspokojení	uspokojení	k1gNnSc4	uspokojení
ve	v	k7c6	v
feministickém	feministický	k2eAgNnSc6d1	feministické
hnutí	hnutí	k1gNnSc6	hnutí
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
sociální	sociální	k2eAgFnSc7d1	sociální
pracovnicí	pracovnice	k1gFnSc7	pracovnice
<g/>
,	,	kIx,	,
kriminalistkou	kriminalistka	k1gFnSc7	kriminalistka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ředitelkou	ředitelka	k1gFnSc7	ředitelka
ženské	ženský	k2eAgFnSc2d1	ženská
donucovací	donucovací	k2eAgFnSc2d1	donucovací
pracovny	pracovna	k1gFnSc2	pracovna
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
citový	citový	k2eAgInSc1d1	citový
život	život	k1gInSc1	život
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
bojem	boj	k1gInSc7	boj
za	za	k7c4	za
základní	základní	k2eAgNnPc4d1	základní
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
věnuje	věnovat	k5eAaPmIp3nS	věnovat
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
léta	léto	k1gNnSc2	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Work	Work	k1gMnSc1	Work
of	of	k?	of
Art	Art	k1gMnSc1	Art
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
dílo	dílo	k1gNnSc1	dílo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
obchodního	obchodní	k2eAgInSc2d1	obchodní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
z	z	k7c2	z
hotelnictví	hotelnictví	k1gNnSc2	hotelnictví
<g/>
.	.	kIx.	.
</s>
<s>
Jayhawker	Jayhawker	k1gInSc1	Jayhawker
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
Selected	Selected	k1gMnSc1	Selected
Short	Shorta	k1gFnPc2	Shorta
Stories	Stories	k1gMnSc1	Stories
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
povídky	povídka	k1gFnPc1	povídka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
It	It	k1gMnSc1	It
Can	Can	k1gMnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Happen	Happen	k2eAgInSc4d1	Happen
Here	Here	k1gInSc4	Here
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
stát	stát	k5eAaImF	stát
nemůže	moct	k5eNaImIp3nS	moct
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
románová	románový	k2eAgFnSc1d1	románová
utopie	utopie	k1gFnSc1	utopie
<g/>
,	,	kIx,	,
napsaná	napsaný	k2eAgFnSc1d1	napsaná
v	v	k7c6	v
obavách	obava	k1gFnPc6	obava
z	z	k7c2	z
růstu	růst	k1gInSc2	růst
italského	italský	k2eAgInSc2d1	italský
a	a	k8xC	a
německého	německý	k2eAgInSc2d1	německý
fašismu	fašismus	k1gInSc2	fašismus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
podobný	podobný	k2eAgInSc4d1	podobný
vývoj	vývoj	k1gInSc4	vývoj
i	i	k9	i
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
maskovaný	maskovaný	k2eAgMnSc1d1	maskovaný
falešně	falešně	k6eAd1	falešně
demokratickými	demokratický	k2eAgNnPc7d1	demokratické
hesly	heslo	k1gNnPc7	heslo
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Prodigal	Prodigal	k1gInSc1	Prodigal
Parents	Parents	k1gInSc1	Parents
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Marnotratní	marnotratnit	k5eAaImIp3nP	marnotratnit
rodiče	rodič	k1gMnPc1	rodič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Bethel	Bethel	k1gInSc1	Bethel
Merriday	Merridaa	k1gFnSc2	Merridaa
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Bethel	Bethel	k1gInSc1	Bethel
Merridayová	Merridayová	k1gFnSc1	Merridayová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
zachycující	zachycující	k2eAgInSc1d1	zachycující
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
společnosti	společnost	k1gFnPc1	společnost
několika	několik	k4yIc2	několik
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
horujících	horující	k2eAgMnPc2d1	horující
pro	pro	k7c4	pro
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc4	příběh
americké	americký	k2eAgFnSc2d1	americká
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
touží	toužit	k5eAaImIp3nS	toužit
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
divadlu	divadlo	k1gNnSc3	divadlo
a	a	k8xC	a
podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
reminiscencí	reminiscence	k1gFnSc7	reminiscence
na	na	k7c4	na
osudové	osudový	k2eAgNnSc4d1	osudové
setkání	setkání	k1gNnSc4	setkání
stárnoucího	stárnoucí	k2eAgMnSc2d1	stárnoucí
autora	autor	k1gMnSc2	autor
s	s	k7c7	s
mladou	mladý	k2eAgFnSc7d1	mladá
krásnou	krásný	k2eAgFnSc7d1	krásná
herečkou	herečka	k1gFnSc7	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Gideon	Gideon	k1gMnSc1	Gideon
Planish	Planish	k1gMnSc1	Planish
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
This	This	k1gInSc1	This
Is	Is	k1gFnSc2	Is
the	the	k?	the
Life	Lif	k1gFnSc2	Lif
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
Cass	Cass	k1gInSc1	Cass
Timberlane	Timberlan	k1gMnSc5	Timberlan
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Manželství	manželství	k1gNnSc1	manželství
soudce	soudce	k1gMnSc2	soudce
Timberlanea	Timberlaneus	k1gMnSc2	Timberlaneus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Kingsblood	Kingsblood	k1gInSc1	Kingsblood
Royal	Royal	k1gInSc1	Royal
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Krev	krev	k1gFnSc1	krev
královská	královský	k2eAgFnSc1d1	královská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
satirické	satirický	k2eAgNnSc1d1	satirické
zpodobnění	zpodobnění	k1gNnSc1	zpodobnění
rasové	rasový	k2eAgFnSc2d1	rasová
diskriminace	diskriminace	k1gFnSc2	diskriminace
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
mladého	mladý	k2eAgMnSc2d1	mladý
bankovního	bankovní	k2eAgMnSc2d1	bankovní
úředníka	úředník	k1gMnSc2	úředník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
při	při	k7c6	při
pátrání	pátrání	k1gNnSc6	pátrání
po	po	k7c6	po
svých	svůj	k3xOyFgInPc6	svůj
údajně	údajně	k6eAd1	údajně
královských	královský	k2eAgInPc6d1	královský
předcích	předek	k1gInPc6	předek
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgMnS	být
černoch	černoch	k1gMnSc1	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
podle	podle	k7c2	podle
amerických	americký	k2eAgInPc2d1	americký
zákonů	zákon	k1gInPc2	zákon
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
on	on	k3xPp3gMnSc1	on
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
dcerka	dcerka	k1gFnSc1	dcerka
černoši	černoch	k1gMnPc1	černoch
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
nejprve	nejprve	k6eAd1	nejprve
pomlčet	pomlčet	k5eAaPmF	pomlčet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
začne	začít	k5eAaPmIp3nS	začít
všímat	všímat	k5eAaImF	všímat
života	život	k1gInSc2	život
černochů	černoch	k1gMnPc2	černoch
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc2	jejich
neradostného	radostný	k2eNgInSc2d1	neradostný
údělu	úděl	k1gInSc2	úděl
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
řadu	řada	k1gFnSc4	řada
skvělých	skvělý	k2eAgMnPc2d1	skvělý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otřesen	otřást	k5eAaPmNgMnS	otřást
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
dosavadním	dosavadní	k2eAgNnSc6d1	dosavadní
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
o	o	k7c6	o
bělošské	bělošský	k2eAgFnSc6d1	bělošská
nadřazenosti	nadřazenost	k1gFnSc6	nadřazenost
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
prozradí	prozradit	k5eAaPmIp3nP	prozradit
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
a	a	k8xC	a
statečně	statečně	k6eAd1	statečně
nese	nést	k5eAaImIp3nS	nést
všechny	všechen	k3xTgInPc4	všechen
následky	následek	k1gInPc4	následek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Godseeker	Godseeker	k1gInSc1	Godseeker
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Hledač	hledač	k1gMnSc1	hledač
Boha	bůh	k1gMnSc2	bůh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
World	World	k1gInSc1	World
So	So	kA	So
Wide	Wid	k1gInPc1	Wid
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
Širý	širý	k2eAgInSc1d1	širý
svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
vydaný	vydaný	k2eAgInSc4d1	vydaný
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Nature	Natur	k1gMnSc5	Natur
Incorporated	Incorporated	k1gMnSc1	Incorporated
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
William	William	k1gInSc1	William
Worthington	Worthington	k1gInSc1	Worthington
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Unpainted	Unpainted	k1gMnSc1	Unpainted
Woman	Woman	k1gMnSc1	Woman
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Nenamalovaná	namalovaný	k2eNgFnSc1d1	nenamalovaná
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Tod	Tod	k1gMnSc1	Tod
Browning	browning	k1gInSc1	browning
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Free	Free	k1gFnSc1	Free
Air	Air	k1gFnSc1	Air
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Vzduch	vzduch	k1gInSc1	vzduch
zdarma	zdarma	k6eAd1	zdarma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Edward	Edward	k1gMnSc1	Edward
H.	H.	kA	H.
Griffith	Griffith	k1gInSc1	Griffith
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Ghost	Ghost	k1gInSc1	Ghost
Patrol	Patrol	k?	Patrol
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Hlídka	hlídka	k1gFnSc1	hlídka
duchů	duch	k1gMnPc2	duch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Nat	Nat	k1gMnSc1	Nat
Ross	Ross	k1gInSc1	Ross
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Main	Main	k1gMnSc1	Main
Street	Street	k1gMnSc1	Street
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Harry	Harra	k1gFnSc2	Harra
Beaumont	Beaumont	k1gInSc1	Beaumont
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Babbitt	Babbitt	k1gInSc1	Babbitt
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Harry	Harra	k1gFnSc2	Harra
Beaumont	Beaumont	k1gInSc1	Beaumont
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Mantrap	Mantrap	k1gInSc1	Mantrap
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Past	past	k1gFnSc1	past
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
muže	muž	k1gMnPc4	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Victor	Victor	k1gMnSc1	Victor
Fleming	Fleming	k1gInSc1	Fleming
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Newly	Newl	k1gInPc1	Newl
Rich	Richa	k1gFnPc2	Richa
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Zbohatlý	zbohatlý	k2eAgMnSc1d1	zbohatlý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Norman	Norman	k1gMnSc1	Norman
Taurog	Taurog	k1gMnSc1	Taurog
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Let	léto	k1gNnPc2	léto
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Play	play	k0	play
King	King	k1gMnSc1	King
<g/>
,	,	kIx,	,
Arrowsmith	Arrowsmith	k1gMnSc1	Arrowsmith
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
John	John	k1gMnSc1	John
Ford	ford	k1gInSc1	ford
<g/>
,	,	kIx,	,
Ann	Ann	k1gFnSc1	Ann
Vickers	Vickersa	k1gFnPc2	Vickersa
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Vickersová	Vickersová	k1gFnSc1	Vickersová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
John	John	k1gMnSc1	John
Cromwell	Cromwell	k1gMnSc1	Cromwell
<g/>
,	,	kIx,	,
Babbitt	Babbitt	k1gMnSc1	Babbitt
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
William	William	k1gInSc1	William
Keighley	Keighlea	k1gFnSc2	Keighlea
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Dodsworth	Dodsworth	k1gMnSc1	Dodsworth
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
William	William	k1gInSc1	William
Wyler	Wyler	k1gInSc1	Wyler
<g/>
,	,	kIx,	,
I	i	k9	i
Married	Married	k1gMnSc1	Married
a	a	k8xC	a
Doctor	Doctor	k1gMnSc1	Doctor
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Vzala	vzít	k5eAaPmAgFnS	vzít
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
doktora	doktor	k1gMnSc4	doktor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Archie	Archie	k1gFnSc2	Archie
Mayo	Mayo	k6eAd1	Mayo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
Untamed	Untamed	k1gInSc1	Untamed
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Nezkrotní	zkrotný	k2eNgMnPc1d1	nezkrotný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
George	George	k1gNnSc2	George
Archainbaud	Archainbauda	k1gFnPc2	Archainbauda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Past	pasta	k1gFnPc2	pasta
na	na	k7c4	na
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
This	This	k1gInSc1	This
is	is	k?	is
the	the	k?	the
Life	Life	k1gInSc1	Life
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Felix	Felix	k1gMnSc1	Felix
E.	E.	kA	E.
Feist	Feist	k1gMnSc1	Feist
<g/>
,	,	kIx,	,
Fun	Fun	k1gMnSc1	Fun
<g />
.	.	kIx.	.
</s>
<s>
and	and	k?	and
Fancy	Fancy	k1gInPc1	Fancy
Free	Free	k1gInSc1	Free
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
z	z	k7c2	z
produkce	produkce	k1gFnSc2	produkce
Walta	Walt	k1gMnSc2	Walt
Disneyho	Disney	k1gMnSc2	Disney
obsahující	obsahující	k2eAgFnSc4d1	obsahující
epizodu	epizoda	k1gFnSc4	epizoda
Bongo	bongo	k1gNnSc1	bongo
režiséra	režisér	k1gMnSc2	režisér
Jacka	Jacek	k1gMnSc2	Jacek
Kinleyho	Kinley	k1gMnSc2	Kinley
<g/>
,	,	kIx,	,
Cass	Cass	k1gInSc1	Cass
Timberlane	Timberlan	k1gMnSc5	Timberlan
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
George	Georg	k1gMnSc2	Georg
Sidney	Sidnea	k1gMnSc2	Sidnea
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Spencer	Spencra	k1gFnPc2	Spencra
Tracy	Traca	k1gFnSc2	Traca
a	a	k8xC	a
Lana	lano	k1gNnSc2	lano
Turnerová	Turnerová	k1gFnSc1	Turnerová
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Majestät	Majestät	k2eAgInSc1d1	Majestät
auf	auf	k?	auf
Abwegen	Abwegen	k1gInSc1	Abwegen
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
na	na	k7c6	na
toulkách	toulka	k1gFnPc6	toulka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Robert	Robert	k1gMnSc1	Robert
A.	A.	kA	A.
Stemmle	Stemmle	k1gInSc1	Stemmle
<g/>
,	,	kIx,	,
Elmer	Elmer	k1gInSc1	Elmer
Gantry	Gantr	k1gInPc1	Gantr
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Richard	Richard	k1gMnSc1	Richard
Brooks	Brooks	k1gInSc1	Brooks
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Burt	Burt	k1gMnSc1	Burt
Lancaster	Lancaster	k1gMnSc1	Lancaster
<g/>
,	,	kIx,	,
Arrowsmith	Arrowsmith	k1gMnSc1	Arrowsmith
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Zelenka	Zelenka	k1gMnSc1	Zelenka
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Jan	Jan	k1gMnSc1	Jan
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
V.	V.	kA	V.
A.	A.	kA	A.
Jung	Jung	k1gMnSc1	Jung
<g/>
,	,	kIx,	,
Babbitt	Babbitt	k1gMnSc1	Babbitt
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Miloslav	Miloslav	k1gMnSc1	Miloslav
Houska	houska	k1gFnSc1	houska
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Arrowsmith	Arrowsmith	k1gInSc1	Arrowsmith
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gInSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
F.	F.	kA	F.
J.	J.	kA	J.
Netušil	Netušil	k1gMnSc1	Netušil
a	a	k8xC	a
Aloys	Aloysa	k1gFnPc2	Aloysa
Skoumal	Skoumal	k1gMnSc1	Skoumal
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1931	[number]	k4	1931
a	a	k8xC	a
Nakladatelské	nakladatelský	k2eAgNnSc4d1	nakladatelské
družstvo	družstvo	k1gNnSc4	družstvo
Máje	máj	k1gFnSc2	máj
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
a	a	k8xC	a
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Mantrap	Mantrap	k1gMnSc1	Mantrap
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
O.	O.	kA	O.
F.	F.	kA	F.
Babler	Babler	k1gMnSc1	Babler
<g/>
,	,	kIx,	,
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
znal	znát	k5eAaImAgMnS	znát
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Vajtauer	Vajtauer	k1gMnSc1	Vajtauer
<g/>
,	,	kIx,	,
Náš	náš	k3xOp1gMnSc1	náš
pan	pan	k1gMnSc1	pan
Wrenn	Wrenn	k1gMnSc1	Wrenn
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
Hofmanová	Hofmanová	k1gFnSc1	Hofmanová
<g/>
,	,	kIx,	,
Továrník	továrník	k1gMnSc1	továrník
Dodsworth	Dodsworth	k1gMnSc1	Dodsworth
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gInSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Eva	Eva	k1gFnSc1	Eva
Horlivá	horlivý	k2eAgFnSc1d1	horlivá
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
ctnosti	ctnost	k1gFnPc1	ctnost
Reverenda	reverend	k1gMnSc2	reverend
Gantryho	Gantry	k1gMnSc2	Gantry
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Timotheus	Timotheus	k1gMnSc1	Timotheus
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
,	,	kIx,	,
Jestřábí	jestřábí	k2eAgNnSc1d1	jestřábí
stopou	stopa	k1gFnSc7	stopa
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
K.	K.	kA	K.
V.	V.	kA	V.
Pavlinec	Pavlinec	k1gMnSc1	Pavlinec
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
Hofmanová	Hofmanová	k1gFnSc1	Hofmanová
<g/>
,	,	kIx,	,
Vzduch	vzduch	k1gInSc1	vzduch
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Miroslav	Miroslav	k1gMnSc1	Miroslav
Houska	houska	k1gFnSc1	houska
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Razil	razit	k5eAaImAgMnS	razit
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Vickersová	Vickersová	k1gFnSc1	Vickersová
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Eva	Eva	k1gFnSc1	Eva
Horlivá	horlivý	k2eAgFnSc1d1	horlivá
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
a	a	k8xC	a
Lidové	lidový	k2eAgNnSc4d1	lidové
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
Kvasnička	kvasnička	k1gFnSc1	kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
K.	K.	kA	K.
V.	V.	kA	V.
Pavlinec	Pavlinec	k1gMnSc1	Pavlinec
<g/>
,	,	kIx,	,
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
stát	stát	k5eAaPmF	stát
nemůže	moct	k5eNaImIp3nS	moct
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
A.	A.	kA	A.
A.	A.	kA	A.
Hoch	hoch	k1gMnSc1	hoch
a	a	k8xC	a
J.	J.	kA	J.
Podroužek	Podroužek	k1gMnSc1	Podroužek
<g/>
.	.	kIx.	.
</s>
<s>
Dopis	dopis	k1gInSc1	dopis
od	od	k7c2	od
královny	královna	k1gFnSc2	královna
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Karla	Karel	k1gMnSc4	Karel
Blažková	Blažková	k1gFnSc1	Blažková
<g/>
,	,	kIx,	,
Marnotratní	marnotratný	k2eAgMnPc1d1	marnotratný
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
Sfinx	sfinx	k1gInSc1	sfinx
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
A.	A.	kA	A.
J.	J.	kA	J.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
na	na	k7c6	na
toulkách	toulka	k1gFnPc6	toulka
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Karla	Karel	k1gMnSc4	Karel
Blažková	Blažková	k1gFnSc1	Blažková
<g/>
,	,	kIx,	,
Manželství	manželství	k1gNnSc1	manželství
soudce	soudce	k1gMnSc1	soudce
Timberlanea	Timberlanea	k1gMnSc1	Timberlanea
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
Hofmanová	Hofmanová	k1gFnSc1	Hofmanová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Bethel	Bethel	k1gInSc1	Bethel
Merridayová	Merridayová	k1gFnSc1	Merridayová
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Eva	Eva	k1gFnSc1	Eva
Horlivá	horlivý	k2eAgFnSc1d1	horlivá
<g/>
,	,	kIx,	,
Krev	krev	k1gFnSc1	krev
královská	královský	k2eAgFnSc1d1	královská
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
A.	A.	kA	A.
J.	J.	kA	J.
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Odeon	odeon	k1gInSc1	odeon
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
znal	znát	k5eAaImAgMnS	znát
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Eva	Eva	k1gFnSc1	Eva
Kondrysová	Kondrysová	k1gFnSc1	Kondrysová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1975	[number]	k4	1975
a	a	k8xC	a
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Babbitt	Babbitt	k1gMnSc1	Babbitt
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Arrowsmith	Arrowsmith	k1gMnSc1	Arrowsmith
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Aloys	Aloys	k1gInSc4	Aloys
Skoumal	Skoumal	k1gMnSc1	Skoumal
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1984	[number]	k4	1984
a	a	k8xC	a
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Elmer	Elmer	k1gMnSc1	Elmer
Gantry	Gantr	k1gInPc4	Gantr
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Nenadál	nadát	k5eNaBmAgInS	nadát
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Bethel	Bethel	k1gInSc1	Bethel
Merridayová	Merridayová	k1gFnSc1	Merridayová
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Mareš	Mareš	k1gMnSc1	Mareš
<g/>
,	,	kIx,	,
Vzduch	vzduch	k1gInSc1	vzduch
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
znal	znát	k5eAaImAgMnS	znát
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
Motýli	motýl	k1gMnPc1	motýl
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
obloukovek	obloukovka	k1gFnPc2	obloukovka
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Frölich	Frölich	k1gMnSc1	Frölich
<g/>
,	,	kIx,	,
Past	past	k1gFnSc1	past
na	na	k7c4	na
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Olga	Olga	k1gFnSc1	Olga
Fialová	Fialová	k1gFnSc1	Fialová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Dekon	Dekon	k1gNnSc1	Dekon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Babbitt	Babbitt	k1gInSc1	Babbitt
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
J	J	kA	J
&	&	k?	&
J	J	kA	J
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
