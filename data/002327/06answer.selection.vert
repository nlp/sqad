<s>
Bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
skvrnitost	skvrnitost	k1gFnSc1	skvrnitost
čiroku	čirok	k1gInSc2	čirok
je	být	k5eAaImIp3nS	být
choroba	choroba	k1gFnSc1	choroba
způsobovaná	způsobovaný	k2eAgFnSc1d1	způsobovaná
bakterií	bakterie	k1gFnSc7	bakterie
Pseudomonas	Pseudomonasa	k1gFnPc2	Pseudomonasa
syrigae	syrigaat	k5eAaPmIp3nS	syrigaat
napadající	napadající	k2eAgFnPc4d1	napadající
rostliny	rostlina	k1gFnPc4	rostlina
rodu	rod	k1gInSc2	rod
čirok	čirok	k1gInSc1	čirok
(	(	kIx(	(
<g/>
Sorghum	Sorghum	k1gInSc1	Sorghum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
