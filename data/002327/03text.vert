<s>
Bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
skvrnitost	skvrnitost	k1gFnSc1	skvrnitost
čiroku	čirok	k1gInSc2	čirok
je	být	k5eAaImIp3nS	být
choroba	choroba	k1gFnSc1	choroba
způsobovaná	způsobovaný	k2eAgFnSc1d1	způsobovaná
bakterií	bakterie	k1gFnSc7	bakterie
Pseudomonas	Pseudomonasa	k1gFnPc2	Pseudomonasa
syrigae	syrigaat	k5eAaPmIp3nS	syrigaat
napadající	napadající	k2eAgFnPc4d1	napadající
rostliny	rostlina	k1gFnPc4	rostlina
rodu	rod	k1gInSc2	rod
čirok	čirok	k1gInSc1	čirok
(	(	kIx(	(
<g/>
Sorghum	Sorghum	k1gInSc1	Sorghum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rezervoárem	rezervoár	k1gInSc7	rezervoár
je	být	k5eAaImIp3nS	být
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
posklizňové	posklizňový	k2eAgInPc1d1	posklizňový
zbytky	zbytek	k1gInPc1	zbytek
<g/>
,	,	kIx,	,
osivo	osivo	k1gNnSc1	osivo
a	a	k8xC	a
čirok	čirok	k1gInSc1	čirok
halapenský	halapenský	k2eAgInSc1d1	halapenský
(	(	kIx(	(
<g/>
Sorghum	Sorghum	k1gInSc1	Sorghum
halapensis	halapensis	k1gFnSc2	halapensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šíří	šířit	k5eAaImIp3nS	šířit
se	s	k7c7	s
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
deštěm	dešť	k1gInSc7	dešť
<g/>
;	;	kIx,	;
do	do	k7c2	do
rostlin	rostlina	k1gFnPc2	rostlina
proniká	pronikat	k5eAaImIp3nS	pronikat
průduchy	průduch	k1gInPc7	průduch
a	a	k8xC	a
ranami	rána	k1gFnPc7	rána
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgFnPc1d1	drobná
kulovité	kulovitý	k2eAgFnPc1d1	kulovitá
skvrnky	skvrnka	k1gFnPc1	skvrnka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
později	pozdě	k6eAd2	pozdě
přecházejí	přecházet	k5eAaImIp3nP	přecházet
v	v	k7c4	v
tmavě	tmavě	k6eAd1	tmavě
zelené	zelený	k2eAgFnPc4d1	zelená
vodnaté	vodnatý	k2eAgFnPc4d1	vodnatá
skvrny	skvrna	k1gFnPc4	skvrna
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
okrajem	okraj	k1gInSc7	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
setí	setí	k1gNnSc1	setí
zdravého	zdravý	k2eAgNnSc2d1	zdravé
osiva	osivo	k1gNnSc2	osivo
<g/>
,	,	kIx,	,
likvidace	likvidace	k1gFnSc2	likvidace
posklizňových	posklizňový	k2eAgInPc2d1	posklizňový
zbytků	zbytek	k1gInPc2	zbytek
a	a	k8xC	a
rezervoárových	rezervoárův	k2eAgInPc2d1	rezervoárův
porostů	porost	k1gInPc2	porost
čiroku	čirok	k1gInSc2	čirok
halapenského	halapenský	k2eAgInSc2d1	halapenský
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dodržovat	dodržovat	k5eAaImF	dodržovat
přiměřený	přiměřený	k2eAgInSc4d1	přiměřený
osevní	osevní	k2eAgInSc4d1	osevní
postup	postup	k1gInSc4	postup
s	s	k7c7	s
rotací	rotace	k1gFnSc7	rotace
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
