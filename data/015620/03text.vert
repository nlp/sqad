<s>
Náhon	náhon	k1gInSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Náhon	náhon	k1gInSc4
Křižovatka	křižovatka	k1gFnSc1
ulic	ulice	k1gFnPc2
Úzké	úzký	k2eAgFnSc2d1
a	a	k8xC
Dornych	Dornych	k1gInSc1
<g/>
,	,	kIx,
prostor	prostor	k1gInSc1
bývalého	bývalý	k2eAgNnSc2d1
předměstí	předměstí	k1gNnSc2
NáhonLokalita	NáhonLokalita	k1gFnSc1
Charakter	charakter	k1gInSc1
</s>
<s>
městská	městský	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
</s>
<s>
Brno-střed	Brno-střed	k1gInSc1
Obec	obec	k1gFnSc1
</s>
<s>
Brno	Brno	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Brno-město	Brno-město	k6eAd1
Kraj	kraj	k1gInSc1
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
24	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
<g/>
53	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Trnitá	trnitý	k2eAgFnSc1d1
PSČ	PSČ	kA
</s>
<s>
602	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
pošta	pošta	k1gFnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
Náhon	náhon	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Náhon	náhon	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
Na	na	k7c6
Příkopech	příkop	k1gInPc6
<g/>
,	,	kIx,
německy	německy	k6eAd1
Mühlgraben	Mühlgraben	k2eAgMnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
předměstská	předměstský	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1941	#num#	k4
také	také	k9
samostatným	samostatný	k2eAgNnSc7d1
katastrálním	katastrální	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvořilo	tvořit	k5eAaImAgNnS
ji	on	k3xPp3gFnSc4
okolí	okolí	k1gNnSc1
dnešních	dnešní	k2eAgFnPc2d1
ulic	ulice	k1gFnPc2
Spálené	spálený	k2eAgNnSc1d1
<g/>
,	,	kIx,
Přízovy	Přízovy	k?
<g/>
,	,	kIx,
Úzké	úzký	k2eAgFnSc6d1
a	a	k8xC
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
Dornychu	Dornych	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
součástí	součást	k1gFnSc7
Trnité	trnitý	k2eAgFnSc2d1
a	a	k8xC
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-střed	Brno-střed	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Předměstské	předměstský	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
v	v	k7c6
prostoru	prostor	k1gInSc6
pozdější	pozdní	k2eAgFnSc2d2
čtvrti	čtvrt	k1gFnSc2
Náhon	náhon	k1gInSc1
<g/>
,	,	kIx,
jižně	jižně	k6eAd1
od	od	k7c2
brněnské	brněnský	k2eAgFnSc2d1
Židovské	židovský	k2eAgFnSc2d1
brány	brána	k1gFnSc2
<g/>
,	,	kIx,
existovalo	existovat	k5eAaImAgNnS
již	již	k6eAd1
ve	v	k7c6
středověku	středověk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
podél	podél	k7c2
dnešních	dnešní	k2eAgFnPc2d1
ulic	ulice	k1gFnPc2
Úzké	úzký	k2eAgFnSc2d1
a	a	k8xC
Dornych	Dornych	k1gInSc1
probíhal	probíhat	k5eAaImAgInS
Svratecký	svratecký	k2eAgInSc1d1
náhon	náhon	k1gInSc1
<g/>
,	,	kIx,
přes	přes	k7c4
nějž	jenž	k3xRgMnSc4
vedl	vést	k5eAaImAgMnS
v	v	k7c6
trase	trasa	k1gFnSc6
druhé	druhý	k4xOgNnSc4
zmíněné	zmíněný	k2eAgNnSc4d1
(	(	kIx(
<g/>
na	na	k7c6
dnešní	dnešní	k2eAgFnSc6d1
křižovatce	křižovatka	k1gFnSc6
Úzké	úzký	k2eAgFnSc2d1
a	a	k8xC
Dornychu	Dornycha	k1gFnSc4
<g/>
)	)	kIx)
Sladovnický	sladovnický	k2eAgInSc1d1
most	most	k1gInSc1
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
cesta	cesta	k1gFnSc1
směřovala	směřovat	k5eAaImAgFnS
do	do	k7c2
osady	osada	k1gFnSc2
Dornych	Dornycha	k1gFnPc2
a	a	k8xC
dále	daleko	k6eAd2
na	na	k7c4
Komárov	Komárov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západně	západně	k6eAd1
od	od	k7c2
něj	on	k3xPp3gInSc2
překračoval	překračovat	k5eAaImAgInS
náhon	náhon	k1gInSc1
druhý	druhý	k4xOgInSc1
most	most	k1gInSc1
s	s	k7c7
cestou	cesta	k1gFnSc7
do	do	k7c2
osady	osada	k1gFnSc2
Trnitá	trnitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
ulice	ulice	k1gFnSc1
Trnitá	trnitý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
vodoteče	vodoteč	k1gFnSc2
se	se	k3xPyFc4
vyvinula	vyvinout	k5eAaPmAgFnS
zástavba	zástavba	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
byl	být	k5eAaImAgInS
mlýn	mlýn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvoj	rozvoj	k1gInSc1
na	na	k7c6
konci	konec	k1gInSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
znamenal	znamenat	k5eAaImAgInS
pro	pro	k7c4
čtvrť	čtvrť	k1gFnSc4
Náhon	náhon	k1gInSc4
vznik	vznik	k1gInSc4
velkých	velká	k1gFnPc2
průmyslových	průmyslový	k2eAgInPc2d1
areálů	areál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pravobřežním	pravobřežní	k2eAgInSc6d1
výběžku	výběžek	k1gInSc6
katastru	katastr	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
místě	místo	k1gNnSc6
pozdějšího	pozdní	k2eAgInSc2d2
obchodního	obchodní	k2eAgInSc2d1
domu	dům	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1786	#num#	k4
vybudována	vybudován	k2eAgFnSc1d1
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
Offermannova	Offermannův	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
na	na	k7c4
vojenská	vojenský	k2eAgNnPc4d1
sukna	sukno	k1gNnPc4
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
níž	jenž	k3xRgFnSc6
zanikla	zaniknout	k5eAaPmAgFnS
většina	většina	k1gFnSc1
zdejší	zdejší	k2eAgFnSc2d1
zástavby	zástavba	k1gFnSc2
<g/>
,	,	kIx,
další	další	k2eAgInSc1d1
tovární	tovární	k2eAgInSc1d1
provoz	provoz	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
východně	východně	k6eAd1
od	od	k7c2
Sladovnického	sladovnický	k2eAgInSc2d1
mostu	most	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
polovinou	polovina	k1gFnSc7
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
urbanizaci	urbanizace	k1gFnSc3
východní	východní	k2eAgFnSc2d1
části	část	k1gFnSc2
území	území	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zde	zde	k6eAd1
byly	být	k5eAaImAgFnP
v	v	k7c6
sousedství	sousedství	k1gNnSc6
předměstí	předměstí	k1gNnSc2
Křenová	křenový	k2eAgFnSc1d1
vytyčeny	vytyčen	k2eAgFnPc1d1
a	a	k8xC
postupně	postupně	k6eAd1
obytnými	obytný	k2eAgInPc7d1
domy	dům	k1gInPc7
zastavovány	zastavován	k2eAgFnPc1d1
dnešní	dnešní	k2eAgFnPc1d1
ulice	ulice	k1gFnPc1
Spálená	spálený	k2eAgFnSc1d1
a	a	k8xC
Přízova	Přízův	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
jižním	jižní	k2eAgInSc6d1
konci	konec	k1gInSc6
stál	stát	k5eAaImAgInS
nový	nový	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
Náhonu	náhon	k1gInSc3
patřila	patřit	k5eAaImAgFnS
i	i	k9
jižní	jižní	k2eAgFnSc1d1
uliční	uliční	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
dnešní	dnešní	k2eAgFnSc2d1
Mlýnské	mlýnský	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
mezi	mezi	k7c7
Dornychem	Dornych	k1gInSc7
a	a	k8xC
Přízovou	přízový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
zde	zde	k6eAd1
<g/>
,	,	kIx,
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
Náhonu	náhon	k1gInSc2
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
velkoměstská	velkoměstský	k2eAgFnSc1d1
činžovní	činžovní	k2eAgFnSc1d1
zástavba	zástavba	k1gFnSc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
dalších	další	k2eAgInPc2d1
průmyslových	průmyslový	k2eAgInPc2d1
areálů	areál	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1855	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
nároží	nároží	k1gNnSc6
ulic	ulice	k1gFnPc2
Spálené	spálený	k2eAgFnSc2d1
a	a	k8xC
Přízovy	Přízovy	k?
postavena	postavit	k5eAaPmNgFnS
první	první	k4xOgFnSc1
novodobá	novodobý	k2eAgFnSc1d1
brněnská	brněnský	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
–	–	k?
Velká	velký	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
vypálena	vypálen	k2eAgFnSc1d1
a	a	k8xC
následně	následně	k6eAd1
zbořena	zbořen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1870	#num#	k4
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
Náhonu	náhon	k1gInSc2
rozděleno	rozdělit	k5eAaPmNgNnS
náspem	násep	k1gInSc7
železniční	železniční	k2eAgFnSc2d1
spojovací	spojovací	k2eAgFnSc2d1
trati	trať	k1gFnSc2
mezi	mezi	k7c7
dolním	dolní	k2eAgNnSc7d1
a	a	k8xC
hlavním	hlavní	k2eAgNnSc7d1
nádražím	nádraží	k1gNnSc7
(	(	kIx(
<g/>
podél	podél	k7c2
ulic	ulice	k1gFnPc2
Plotní	plotní	k2eAgMnSc1d1
a	a	k8xC
Dornych	Dornych	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1901	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c6
ulici	ulice	k1gFnSc6
Dornych	Dornycha	k1gFnPc2
zahájen	zahájit	k5eAaPmNgInS
provoz	provoz	k1gInSc1
tramvají	tramvaj	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
k	k	k7c3
radikálním	radikální	k2eAgFnPc3d1
urbanistickým	urbanistický	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
<g/>
,	,	kIx,
při	při	k7c6
nichž	jenž	k3xRgInPc6
byla	být	k5eAaImAgFnS
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
zbořena	zbořit	k5eAaPmNgFnS
téměř	téměř	k6eAd1
veškerá	veškerý	k3xTgFnSc1
zástavba	zástavba	k1gFnSc1
podél	podél	k7c2
ulic	ulice	k1gFnPc2
Dornych	Dornycha	k1gFnPc2
a	a	k8xC
Úzká	úzký	k2eAgFnSc1d1
<g/>
,	,	kIx,
včetně	včetně	k7c2
Offermannovy	Offermannův	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
místě	místo	k1gNnSc6
továrny	továrna	k1gFnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
vybudováno	vybudovat	k5eAaPmNgNnS
obchodní	obchodní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Dornych	Dornycha	k1gFnPc2
a	a	k8xC
z	z	k7c2
ulic	ulice	k1gFnPc2
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
hlavní	hlavní	k2eAgInPc1d1
silniční	silniční	k2eAgInPc1d1
tahy	tah	k1gInPc1
rozšířené	rozšířený	k2eAgInPc1d1
na	na	k7c6
několik	několik	k4yIc4
pruhů	pruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
prostoru	prostor	k1gInSc6
čtvrti	čtvrt	k1gFnSc2
zatrubněn	zatrubněn	k2eAgInSc4d1
Svratecký	svratecký	k2eAgInSc4d1
náhon	náhon	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
dalších	další	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
i	i	k9
železniční	železniční	k2eAgFnSc1d1
spojka	spojka	k1gFnSc1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
její	její	k3xOp3gInSc1
násep	násep	k1gInSc1
zůstal	zůstat	k5eAaPmAgInS
zachován	zachovat	k5eAaPmNgInS
až	až	k6eAd1
do	do	k7c2
počátku	počátek	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
zbořen	zbořen	k2eAgInSc4d1
areál	areál	k1gInSc4
podniku	podnik	k1gInSc2
Vlněna	vlněn	k2eAgFnSc1d1
<g/>
,	,	kIx,
tvořící	tvořící	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Přízovy	Přízovy	k?
ulice	ulice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachován	zachován	k2eAgInSc1d1
zůstal	zůstat	k5eAaPmAgInS
jen	jen	k9
palác	palác	k1gInSc1
Bochnerů	Bochner	k1gMnPc2
ze	z	k7c2
Stražiska	Stražisko	k1gNnSc2
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
kancelářskými	kancelářský	k2eAgFnPc7d1
budovami	budova	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Náhon	náhon	k1gInSc1
byl	být	k5eAaImAgInS
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ve	v	k7c6
správě	správa	k1gFnSc6
brněnského	brněnský	k2eAgInSc2d1
magistrátu	magistrát	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
tzv.	tzv.	kA
magistrátním	magistrátní	k2eAgNnSc7d1
předměstím	předměstí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
Brna	Brno	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1850	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katastrální	katastrální	k2eAgInSc1d1
území	území	k1gNnSc6
Náhonu	náhon	k1gInSc6
bylo	být	k5eAaImAgNnS
zrušeno	zrušit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
při	při	k7c6
katastrální	katastrální	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
katastru	katastr	k1gInSc2
Trnité	trnitý	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Svratecký	svratecký	k2eAgInSc1d1
náhon	náhon	k1gInSc1
z	z	k7c2
mostu	most	k1gInSc2
na	na	k7c4
Dornychu	Dornycha	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
pozadí	pozadí	k1gNnSc6
most	most	k1gInSc1
železniční	železniční	k2eAgFnSc2d1
spojky	spojka	k1gFnSc2
a	a	k8xC
Offermannova	Offermannův	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
<g/>
,	,	kIx,
zbořená	zbořený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
</s>
<s>
Činžovní	činžovní	k2eAgFnSc1d1
zástavba	zástavba	k1gFnSc1
na	na	k7c6
nároží	nároží	k1gNnSc6
Dornychu	Dornych	k1gInSc2
a	a	k8xC
Přízovy	Přízovy	k?
</s>
<s>
Obchodní	obchodní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Dornych	Dornycha	k1gFnPc2
</s>
<s>
Areál	areál	k1gInSc1
podniku	podnik	k1gInSc2
Vlněna	vlnit	k5eAaImNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
,	,	kIx,
vpravo	vpravo	k6eAd1
palác	palác	k1gInSc1
Bochnerů	Bochner	k1gMnPc2
ze	z	k7c2
Stražiska	Stražisko	k1gNnSc2
</s>
<s>
Zbořený	zbořený	k2eAgInSc4d1
areál	areál	k1gInSc4
Vlněny	vlněn	k2eAgFnPc1d1
<g/>
,	,	kIx,
zachován	zachovat	k5eAaPmNgInS
zůstal	zůstat	k5eAaPmAgInS
palác	palác	k1gInSc1
Bochnerů	Bochner	k1gMnPc2
ze	z	k7c2
Stražiska	Stražisko	k1gNnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
KUČA	KUČA	k?
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc4
–	–	k?
vývoj	vývoj	k1gInSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
předměstí	předměstí	k1gNnSc2
a	a	k8xC
připojených	připojený	k2eAgFnPc2d1
vesnic	vesnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Baset	Baset	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86223	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
461	#num#	k4
<g/>
–	–	k?
<g/>
462	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠEDA	Šeda	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
Vlněna	vlnit	k5eAaImNgFnS
roste	růst	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Necelé	celý	k2eNgInPc4d1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
od	od	k7c2
demolice	demolice	k1gFnSc2
textilky	textilka	k1gFnSc2
dokončuje	dokončovat	k5eAaImIp3nS
developer	developer	k1gMnSc1
první	první	k4xOgFnPc4
tři	tři	k4xCgFnPc4
budovy	budova	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brnenskadrbna	Brnenskadrbna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-03-13	2018-03-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SOUČEK	Souček	k1gMnSc1
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrospektivní	retrospektivní	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
verze	verze	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
úřad	úřad	k1gInSc1
zeměměřický	zeměměřický	k2eAgInSc1d1
a	a	k8xC
katastrální	katastrální	k2eAgMnSc1d1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
3	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Náhon	náhon	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normat	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
<g/>
}	}	kIx)
Statutární	statutární	k2eAgNnSc4d1
město	město	k1gNnSc4
Brno	Brno	k1gNnSc4
Městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
</s>
<s>
Brno-Bohunice	Brno-Bohunice	k1gFnSc1
</s>
<s>
Brno-Bosonohy	Brno-Bosonoh	k1gInPc1
</s>
<s>
Brno-Bystrc	Brno-Bystrc	k6eAd1
</s>
<s>
Brno-Černovice	Brno-Černovice	k1gFnSc1
</s>
<s>
Brno-Chrlice	Brno-Chrlice	k1gFnSc1
</s>
<s>
Brno-Ivanovice	Brno-Ivanovice	k1gFnSc1
</s>
<s>
Brno-Jehnice	Brno-Jehnice	k1gFnSc1
</s>
<s>
Brno-jih	Brno-jih	k1gMnSc1
</s>
<s>
Brno-Jundrov	Brno-Jundrov	k1gInSc1
</s>
<s>
Brno-Kníničky	Brno-Knínička	k1gFnPc1
</s>
<s>
Brno-Kohoutovice	Brno-Kohoutovice	k1gFnSc1
</s>
<s>
Brno-Komín	Brno-Komín	k1gMnSc1
</s>
<s>
Brno-Královo	Brno-Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
</s>
<s>
Brno-Líšeň	Brno-Líšeň	k1gFnSc1
</s>
<s>
Brno-Maloměřice	Brno-Maloměřice	k1gFnPc1
a	a	k8xC
Obřany	Obřana	k1gFnPc1
</s>
<s>
Brno-Medlánky	Brno-Medlánka	k1gFnPc1
</s>
<s>
Brno-Nový	Brno-Nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Brno-Ořešín	Brno-Ořešín	k1gMnSc1
</s>
<s>
Brno-Řečkovice	Brno-Řečkovice	k1gFnSc1
a	a	k8xC
Mokrá	mokrý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Brno-sever	Brno-sever	k1gMnSc1
</s>
<s>
Brno-Slatina	Brno-Slatina	k1gFnSc1
</s>
<s>
Brno-Starý	Brno-Starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Brno-střed	Brno-střed	k1gMnSc1
</s>
<s>
Brno-Tuřany	Brno-Tuřan	k1gMnPc4
</s>
<s>
Brno-Útěchov	Brno-Útěchov	k1gInSc1
</s>
<s>
Brno-Vinohrady	Brno-Vinohrada	k1gFnPc1
</s>
<s>
Brno-Žabovřesky	Brno-Žabovřesky	k6eAd1
</s>
<s>
Brno-Žebětín	Brno-Žebětín	k1gMnSc1
</s>
<s>
Brno-Židenice	Brno-Židenice	k1gFnSc1
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
akatastrální	akatastrální	k2eAgFnSc4d1
území	území	k1gNnPc2
</s>
<s>
Bohunice	Bohunice	k1gFnPc1
</s>
<s>
Bosonohy	Bosonohy	k?
</s>
<s>
Brněnské	brněnský	k2eAgFnPc4d1
Ivanovice	Ivanovice	k1gFnPc4
</s>
<s>
Brno-město	Brno-město	k6eAd1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Město	město	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Bystrc	Bystrc	k1gFnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Pole	pole	k1gFnSc1
</s>
<s>
Černovice	Černovice	k1gFnSc1
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
Heršpice	Heršpice	k1gFnPc1
</s>
<s>
Dvorska	Dvorska	k1gFnSc1
</s>
<s>
Holásky	Holásek	k1gMnPc4
</s>
<s>
Horní	horní	k2eAgFnPc1d1
Heršpice	Heršpice	k1gFnPc1
</s>
<s>
Husovice	Husovice	k1gFnPc1
</s>
<s>
Chrlice	Chrlice	k1gFnPc1
</s>
<s>
Ivanovice	Ivanovice	k1gFnPc1
</s>
<s>
Jehnice	jehnice	k1gFnSc1
</s>
<s>
Jundrov	Jundrov	k1gInSc1
</s>
<s>
Kníničky	Knínička	k1gFnPc1
</s>
<s>
Kohoutovice	Kohoutovice	k1gFnPc1
</s>
<s>
Komárov	Komárov	k1gInSc1
</s>
<s>
Komín	Komín	k1gInSc1
</s>
<s>
Královo	Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
</s>
<s>
Lesná	lesný	k2eAgFnSc1d1
</s>
<s>
Líšeň	Líšeň	k1gFnSc1
</s>
<s>
Maloměřice	Maloměřice	k1gFnPc1
</s>
<s>
Medlánky	Medlánka	k1gFnPc1
</s>
<s>
Mokrá	mokrý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Obřany	Obřana	k1gFnPc1
</s>
<s>
Ořešín	Ořešín	k1gMnSc1
</s>
<s>
Pisárky	Pisárka	k1gFnPc1
</s>
<s>
Ponava	Ponava	k1gFnSc1
</s>
<s>
Přízřenice	Přízřenice	k1gFnSc1
</s>
<s>
Řečkovice	Řečkovice	k1gFnSc1
</s>
<s>
Sadová	sadový	k2eAgFnSc1d1
</s>
<s>
Slatina	slatina	k1gFnSc1
</s>
<s>
Soběšice	Soběšice	k1gFnSc1
</s>
<s>
Staré	Staré	k2eAgNnSc1d1
Brno	Brno	k1gNnSc1
</s>
<s>
Starý	starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Stránice	Stránice	k1gFnSc1
</s>
<s>
Štýřice	Štýřice	k1gFnSc1
</s>
<s>
Trnitá	trnitý	k2eAgFnSc1d1
</s>
<s>
Tuřany	Tuřana	k1gFnPc1
</s>
<s>
Útěchov	Útěchov	k1gInSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Útěchov	Útěchov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Veveří	veveří	k2eAgNnSc4d1
</s>
<s>
Zábrdovice	Zábrdovice	k1gFnPc1
</s>
<s>
Žabovřesky	Žabovřesky	k1gFnPc1
</s>
<s>
Žebětín	Žebětín	k1gMnSc1
</s>
<s>
Židenice	Židenice	k1gFnPc1
Další	další	k2eAgFnPc1d1
čtvrtě	čtvrt	k1gFnPc1
</s>
<s>
Cacovice	Cacovice	k1gFnSc1
</s>
<s>
Černovičky	Černovička	k1gFnPc1
</s>
<s>
Divišova	Divišův	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
</s>
<s>
Jabloňová	jabloňový	k2eAgFnSc1d1
</s>
<s>
Juliánov	Juliánov	k1gInSc1
</s>
<s>
Kamechy	Kamech	k1gInPc1
</s>
<s>
Kamenná	kamenný	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1
Vrch	vrch	k1gInSc1
</s>
<s>
Kandie	Kandie	k1gFnSc1
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
Černovice	Černovice	k1gFnPc1
</s>
<s>
Nové	Nové	k2eAgMnPc4d1
Moravany	Moravan	k1gMnPc4
</s>
<s>
Nový	nový	k2eAgInSc1d1
dům	dům	k1gInSc1
</s>
<s>
Písečník	písečník	k1gInSc1
</s>
<s>
Pod	pod	k7c7
vodojemem	vodojem	k1gInSc7
</s>
<s>
Slatinka	Slatinka	k1gFnSc1
</s>
<s>
Štefánikova	Štefánikův	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
Historická	historický	k2eAgFnSc1d1
předměstí	předměstí	k1gNnSc4
</s>
<s>
Augustiniánská	augustiniánský	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Červená	červený	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
a	a	k8xC
Horní	horní	k2eAgInSc1d1
Cejl	Cejl	k1gInSc1
</s>
<s>
Josefov	Josefov	k1gInSc1
</s>
<s>
Kožená	kožený	k2eAgFnSc1d1
</s>
<s>
Křenová	křenový	k2eAgFnSc1d1
</s>
<s>
Křížová	Křížová	k1gFnSc1
</s>
<s>
Lužánky	Lužánka	k1gFnPc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Mariacela	Mariacela	k1gFnSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Na	na	k7c4
Hrázi	hráze	k1gFnSc4
a	a	k8xC
Příkop	příkop	k1gInSc4
</s>
<s>
Náhon	náhon	k1gInSc1
</s>
<s>
Nové	Nové	k2eAgInPc1d1
Sady	sad	k1gInPc1
</s>
<s>
Pekařská	pekařský	k2eAgFnSc1d1
</s>
<s>
Růžový	růžový	k2eAgInSc1d1
</s>
<s>
Silniční	silniční	k2eAgInSc1d1
</s>
<s>
Špilberk	Špilberk	k1gInSc1
</s>
<s>
Švábka	švábka	k1gFnSc1
</s>
<s>
U	u	k7c2
Svaté	svatý	k2eAgFnSc2d1
Anny	Anna	k1gFnSc2
</s>
<s>
Ugartov	Ugartov	k1gInSc1
</s>
<s>
V	v	k7c6
Jirchářích	jirchář	k1gMnPc6
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Vinohrádky	vinohrádek	k1gInPc1
Zaniklé	zaniklý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
</s>
<s>
Kníničky	Knínička	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
</s>
