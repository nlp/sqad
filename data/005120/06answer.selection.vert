<s>
Ludwig	Ludwig	k1gMnSc1	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
(	(	kIx(	(
<g/>
křest	křest	k1gInSc1	křest
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1770	[number]	k4	1770
<g/>
,	,	kIx,	,
Bonn	Bonn	k1gInSc1	Bonn
–	–	k?	–
úmrtí	úmrtí	k1gNnSc1	úmrtí
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1827	[number]	k4	1827
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
Alservorstadt	Alservorstadt	k1gMnSc1	Alservorstadt
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
dovršitel	dovršitel	k1gMnSc1	dovršitel
první	první	k4xOgFnSc2	první
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
školy	škola	k1gFnSc2	škola
klasicistní	klasicistní	k2eAgFnSc2d1	klasicistní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
