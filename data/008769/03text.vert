<p>
<s>
Loštice	Loštice	k1gFnSc1	Loštice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Loschitz	Loschitz	k1gInSc1	Loschitz
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Třebůvce	Třebůvka	k1gFnSc6	Třebůvka
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
okresu	okres	k1gInSc2	okres
Šumperk	Šumperk	k1gInSc1	Šumperk
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
jako	jako	k8xS	jako
trhová	trhový	k2eAgFnSc1d1	trhová
ves	ves	k1gFnSc1	ves
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
vesnice	vesnice	k1gFnSc2	vesnice
Žádlovice	Žádlovice	k1gFnSc2	Žádlovice
a	a	k8xC	a
Vlčice	vlčice	k1gFnSc2	vlčice
<g/>
.	.	kIx.	.
</s>
<s>
Loštice	Loštice	k1gFnPc1	Loštice
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
tradici	tradice	k1gFnSc3	tradice
výroby	výroba	k1gFnSc2	výroba
olomouckých	olomoucký	k2eAgInPc2d1	olomoucký
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
Loštice	Loštice	k1gFnSc1	Loštice
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
jako	jako	k8xS	jako
trhová	trhový	k2eAgFnSc1d1	trhová
ves	ves	k1gFnSc1	ves
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
nacházela	nacházet	k5eAaImAgFnS	nacházet
stará	starý	k2eAgFnSc1d1	stará
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
osada	osada	k1gFnSc1	osada
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1267	[number]	k4	1267
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
biskup	biskup	k1gMnSc1	biskup
Bruno	Bruno	k1gMnSc1	Bruno
ze	z	k7c2	z
Schauenburku	Schauenburk	k1gInSc2	Schauenburk
přikoupil	přikoupit	k5eAaPmAgMnS	přikoupit
od	od	k7c2	od
Petra	Petr	k1gMnSc2	Petr
z	z	k7c2	z
Loštic	Loštice	k1gFnPc2	Loštice
popluží	popluží	k1gNnSc2	popluží
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
Loštice	Loštice	k1gFnPc1	Loštice
označovány	označován	k2eAgFnPc1d1	označována
jako	jako	k8xS	jako
městečko	městečko	k1gNnSc1	městečko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
konaly	konat	k5eAaImAgInP	konat
trhy	trh	k1gInPc1	trh
a	a	k8xC	a
výroční	výroční	k2eAgFnPc1d1	výroční
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
židovskou	židovský	k2eAgFnSc7d1	židovská
komunitou	komunita	k1gFnSc7	komunita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lošticích	Loštik	k1gMnPc6	Loštik
Židé	Žid	k1gMnPc1	Žid
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1544	[number]	k4	1544
hřbitov	hřbitov	k1gInSc4	hřbitov
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1571	[number]	k4	1571
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
město	město	k1gNnSc1	město
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
požár	požár	k1gInSc1	požár
a	a	k8xC	a
letní	letní	k2eAgFnPc1d1	letní
povodně	povodeň	k1gFnPc1	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
Lošticích	Loštice	k1gFnPc6	Loštice
žilo	žít	k5eAaImAgNnS	žít
téměř	téměř	k6eAd1	téměř
dva	dva	k4xCgInPc1	dva
tisíce	tisíc	k4xCgInPc1	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
města	město	k1gNnSc2	město
–	–	k?	–
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
autobusové	autobusový	k2eAgNnSc1d1	autobusové
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
vesnicemi	vesnice	k1gFnPc7	vesnice
<g/>
,	,	kIx,	,
založen	založen	k2eAgInSc1d1	založen
Klub	klub	k1gInSc1	klub
čs	čs	kA	čs
<g/>
.	.	kIx.	.
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
cvičiště	cvičiště	k1gNnSc1	cvičiště
<g/>
,	,	kIx,	,
postavena	postaven	k2eAgFnSc1d1	postavena
hasičská	hasičský	k2eAgFnSc1d1	hasičská
zbrojnice	zbrojnice	k1gFnSc1	zbrojnice
a	a	k8xC	a
založen	založit	k5eAaPmNgInS	založit
nový	nový	k2eAgInSc1d1	nový
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
v	v	k7c6	v
okolích	okolí	k1gNnPc6	okolí
Loštic	Loštice	k1gFnPc2	Loštice
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
henleinovské	henleinovský	k2eAgNnSc1d1	henleinovské
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
byla	být	k5eAaImAgFnS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
činnost	činnost	k1gFnSc1	činnost
Sokola	Sokol	k1gMnSc2	Sokol
<g/>
,	,	kIx,	,
Orla	Orel	k1gMnSc2	Orel
a	a	k8xC	a
Skauta	skaut	k1gMnSc2	skaut
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
založen	založit	k5eAaPmNgInS	založit
Svaz	svaz	k1gInSc1	svaz
přátel	přítel	k1gMnPc2	přítel
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
zrušena	zrušen	k2eAgFnSc1d1	zrušena
loštická	loštický	k2eAgFnSc1d1	loštická
židovská	židovský	k2eAgFnSc1d1	židovská
obec	obec	k1gFnSc1	obec
a	a	k8xC	a
1949	[number]	k4	1949
začala	začít	k5eAaPmAgFnS	začít
hromadná	hromadný	k2eAgFnSc1d1	hromadná
likvidace	likvidace	k1gFnSc1	likvidace
živnostníků	živnostník	k1gMnPc2	živnostník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žije	žít	k5eAaImIp3nS	žít
přes	přes	k7c4	přes
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
Loštic	Loštice	k1gFnPc2	Loštice
jsou	být	k5eAaImIp3nP	být
olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zde	zde	k6eAd1	zde
firma	firma	k1gFnSc1	firma
A.	A.	kA	A.
W.	W.	kA	W.
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
žije	žít	k5eAaImIp3nS	žít
také	také	k6eAd1	také
kulturní	kulturní	k2eAgInPc1d1	kulturní
životem	život	k1gInSc7	život
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
výroby	výroba	k1gFnSc2	výroba
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
probíhají	probíhat	k5eAaImIp3nP	probíhat
každoročně	každoročně	k6eAd1	každoročně
Slavnosti	slavnost	k1gFnPc1	slavnost
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
<g/>
)	)	kIx)	)
a	a	k8xC	a
turistický	turistický	k2eAgInSc1d1	turistický
Pochod	pochod	k1gInSc1	pochod
za	za	k7c7	za
Loštickým	Loštický	k2eAgInSc7d1	Loštický
tvarůžkem	tvarůžek	k1gInSc7	tvarůžek
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Loštické	Loštický	k2eAgInPc4d1	Loštický
poháry	pohár	k1gInPc4	pohár
===	===	k?	===
</s>
</p>
<p>
<s>
Unikátní	unikátní	k2eAgInPc4d1	unikátní
poháry	pohár	k1gInPc4	pohár
hnědé	hnědý	k2eAgFnSc2d1	hnědá
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
pokrytým	pokrytý	k2eAgInSc7d1	pokrytý
puchýřky	puchýřek	k1gInPc4	puchýřek
se	se	k3xPyFc4	se
od	od	k7c2	od
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
v	v	k7c6	v
Lošticích	Loštice	k1gFnPc6	Loštice
a	a	k8xC	a
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
výrobě	výroba	k1gFnSc3	výroba
používali	používat	k5eAaImAgMnP	používat
hrnčíři	hrnčíř	k1gMnPc1	hrnčíř
specifické	specifický	k2eAgFnSc2d1	specifická
hlíny	hlína	k1gFnSc2	hlína
těžené	těžený	k2eAgFnSc2d1	těžená
poblíž	poblíž	k6eAd1	poblíž
města	město	k1gNnSc2	město
a	a	k8xC	a
vypalovali	vypalovat	k5eAaImAgMnP	vypalovat
je	on	k3xPp3gNnSc4	on
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
1200	[number]	k4	1200
°	°	k?	°
<g/>
C.	C.	kA	C.
Tyto	tento	k3xDgInPc1	tento
poháry	pohár	k1gInPc1	pohár
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
obchodním	obchodní	k2eAgInSc7d1	obchodní
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
je	on	k3xPp3gFnPc4	on
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
hradech	hrad	k1gInPc6	hrad
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
pohárů	pohár	k1gInPc2	pohár
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
zpodobněn	zpodobnit	k5eAaPmNgInS	zpodobnit
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Bosche	Bosch	k1gInSc2	Bosch
Zahrada	zahrada	k1gFnSc1	zahrada
pozemských	pozemský	k2eAgFnPc2d1	pozemská
rozkoší	rozkoš	k1gFnPc2	rozkoš
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Městská	městský	k2eAgFnSc1d1	městská
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
střední	střední	k2eAgNnSc4d1	střední
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
města	město	k1gNnSc2	město
Loštice	Loštice	k1gFnSc2	Loštice
má	mít	k5eAaImIp3nS	mít
15	[number]	k4	15
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
poté	poté	k6eAd1	poté
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
volí	volit	k5eAaImIp3nP	volit
Radu	rada	k1gFnSc4	rada
města	město	k1gNnSc2	město
Loštice	Loštice	k1gFnSc2	Loštice
<g/>
.	.	kIx.	.
</s>
<s>
Radu	rada	k1gFnSc4	rada
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
:	:	kIx,	:
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
místostarosta	místostarosta	k1gMnSc1	místostarosta
a	a	k8xC	a
3	[number]	k4	3
radní	radní	k1gMnSc1	radní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
zastupitelsta	zastupitelst	k1gMnSc2	zastupitelst
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
konaných	konaný	k2eAgNnPc2d1	konané
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
volební	volební	k2eAgFnSc1d1	volební
účast	účast	k1gFnSc1	účast
byla	být	k5eAaImAgFnS	být
52,76	[number]	k4	52,76
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volby	volba	k1gFnSc2	volba
2014	[number]	k4	2014
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Loštice	Loštice	k1gFnPc1	Loštice
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
==	==	k?	==
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
olomouckých	olomoucký	k2eAgInPc2d1	olomoucký
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nabízí	nabízet	k5eAaImIp3nP	nabízet
vhled	vhled	k1gInSc4	vhled
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
výroby	výroba	k1gFnSc2	výroba
Olomouckých	olomoucký	k2eAgInPc2d1	olomoucký
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
a	a	k8xC	a
podniková	podnikový	k2eAgFnSc1d1	podniková
prodejna	prodejna	k1gFnSc1	prodejna
firmy	firma	k1gFnSc2	firma
A.	A.	kA	A.
W.	W.	kA	W.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
široký	široký	k2eAgInSc4d1	široký
sortiment	sortiment	k1gInSc4	sortiment
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvarůžková	tvarůžkový	k2eAgFnSc1d1	Tvarůžková
cukrárna	cukrárna	k1gFnSc1	cukrárna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
již	již	k9	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
originální	originální	k2eAgInPc1d1	originální
Poštulkovy	Poštulkův	k2eAgInPc1d1	Poštulkův
tvarůžkové	tvarůžkový	k2eAgInPc1d1	tvarůžkový
moučníky	moučník	k1gInPc1	moučník
–	–	k?	–
slané	slaný	k2eAgFnSc2d1	slaná
zákusky	zákuska	k1gFnSc2	zákuska
s	s	k7c7	s
jemnou	jemný	k2eAgFnSc7d1	jemná
tvarůžkovou	tvarůžkový	k2eAgFnSc7d1	Tvarůžková
pěnou	pěna	k1gFnSc7	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
také	také	k9	také
rychlé	rychlý	k2eAgNnSc4d1	rychlé
občerstvení	občerstvení	k1gNnSc4	občerstvení
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tvarůžkový	tvarůžkový	k2eAgInSc1d1	tvarůžkový
bramboráček	bramboráček	k1gInSc1	bramboráček
<g/>
,	,	kIx,	,
mls	mls	k1gInSc1	mls
či	či	k8xC	či
hot	hot	k0	hot
dog	doga	k1gFnPc2	doga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Restaurace	restaurace	k1gFnSc1	restaurace
a	a	k8xC	a
hotel	hotel	k1gInSc1	hotel
U	u	k7c2	u
Coufalů	Coufal	k1gMnPc2	Coufal
s	s	k7c7	s
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
tradicí	tradice	k1gFnSc7	tradice
nabízí	nabízet	k5eAaImIp3nS	nabízet
širokou	široký	k2eAgFnSc4d1	široká
nabídku	nabídka	k1gFnSc4	nabídka
specialit	specialita	k1gFnPc2	specialita
připravovaných	připravovaný	k2eAgFnPc2d1	připravovaná
z	z	k7c2	z
Olomouckých	olomoucký	k2eAgInPc2d1	olomoucký
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
restaurace	restaurace	k1gFnSc2	restaurace
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
také	také	k9	také
automat	automat	k1gInSc1	automat
na	na	k7c4	na
tvarůžky	tvarůžek	k1gInPc4	tvarůžek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Památník	památník	k1gInSc1	památník
Adolfa	Adolf	k1gMnSc2	Adolf
Kašpara	Kašpar	k1gMnSc2	Kašpar
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgMnS	umístit
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
trávil	trávit	k5eAaImAgMnS	trávit
pravidelně	pravidelně	k6eAd1	pravidelně
letní	letní	k2eAgInPc4d1	letní
měsíce	měsíc	k1gInPc4	měsíc
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Umístěna	umístěn	k2eAgFnSc1d1	umístěna
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
expozice	expozice	k1gFnSc1	expozice
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
města	město	k1gNnSc2	město
Loštice	Loštice	k1gFnSc2	Loštice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
<g/>
,	,	kIx,	,
a	a	k8xC	a
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
</s>
</p>
<p>
<s>
Ateliér	ateliér	k1gInSc1	ateliér
řezbáře	řezbář	k1gMnSc2	řezbář
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnSc4	jeho
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
betlém	betlém	k1gInSc4	betlém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ho	on	k3xPp3gMnSc4	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
na	na	k7c4	na
190	[number]	k4	190
figurek	figurka	k1gFnPc2	figurka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tradičních	tradiční	k2eAgFnPc2d1	tradiční
biblických	biblický	k2eAgFnPc2d1	biblická
postav	postava	k1gFnPc2	postava
zachytil	zachytit	k5eAaPmAgMnS	zachytit
řezbář	řezbář	k1gMnSc1	řezbář
i	i	k9	i
skutečné	skutečný	k2eAgFnPc4d1	skutečná
postavy	postava	k1gFnPc4	postava
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
i	i	k8xC	i
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
koupaliště	koupaliště	k1gNnSc1	koupaliště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrajové	okrajový	k2eAgFnSc6d1	okrajová
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
malý	malý	k2eAgInSc1d1	malý
bazén	bazén	k1gInSc1	bazén
se	s	k7c7	s
skluzavkou	skluzavka	k1gFnSc7	skluzavka
<g/>
,	,	kIx,	,
travnaté	travnatý	k2eAgNnSc4d1	travnaté
hřiště	hřiště	k1gNnSc4	hřiště
se	s	k7c7	s
sítí	síť	k1gFnSc7	síť
na	na	k7c4	na
volejbal	volejbal	k1gInSc4	volejbal
<g/>
,	,	kIx,	,
wi-fi	wii	k1gNnPc4	wi-fi
<g/>
,	,	kIx,	,
převlékárny	převlékárna	k1gFnPc4	převlékárna
a	a	k8xC	a
občerstvení	občerstvení	k1gNnPc4	občerstvení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Narodili	narodit	k5eAaPmAgMnP	narodit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Havelka	Havelka	k1gMnSc1	Havelka
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Barvič	barvič	k1gMnSc1	barvič
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Adam	Adam	k1gMnSc1	Adam
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Wessels	Wesselsa	k1gFnPc2	Wesselsa
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
loštických	loštický	k2eAgFnPc2d1	loštická
tvarůžkáren	tvarůžkárna	k1gFnPc2	tvarůžkárna
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
advokát	advokát	k1gMnSc1	advokát
<g/>
,	,	kIx,	,
žurnalista	žurnalista	k1gMnSc1	žurnalista
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Olomouce	Olomouc	k1gFnSc2	Olomouc
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Bieberle	Bieberle	k1gFnSc2	Bieberle
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Hekele	Hekel	k1gInSc2	Hekel
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
silniční	silniční	k2eAgFnSc1d1	silniční
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
,	,	kIx,	,
cyklokros	cyklokros	k1gInSc1	cyklokros
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
jsou	být	k5eAaImIp3nP	být
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Prokopa	Prokop	k1gMnSc2	Prokop
–	–	k?	–
se	s	k7c7	s
zachovalým	zachovalý	k2eAgNnSc7d1	zachovalé
románským	románský	k2eAgNnSc7d1	románské
jádrem	jádro	k1gNnSc7	jádro
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
původního	původní	k2eAgInSc2d1	původní
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1208	[number]	k4	1208
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
renesančně	renesančně	k6eAd1	renesančně
přestavěný	přestavěný	k2eAgMnSc1d1	přestavěný
<g/>
,	,	kIx,	,
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
upravený	upravený	k2eAgInSc4d1	upravený
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
k	k	k7c3	k
areálu	areál	k1gInSc3	areál
kostela	kostel	k1gInSc2	kostel
patří	patřit	k5eAaImIp3nS	patřit
dále	daleko	k6eAd2	daleko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kříž	kříž	k1gInSc1	kříž
–	–	k?	–
klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
kamenická	kamenický	k2eAgFnSc1d1	kamenická
práce	práce	k1gFnSc1	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
</s>
</p>
<p>
<s>
náhrobek	náhrobek	k1gInSc1	náhrobek
Rosalie	Rosalie	k1gFnSc2	Rosalie
Dworzakové	Dworzakový	k2eAgFnSc2d1	Dworzakový
–	–	k?	–
empírový	empírový	k2eAgInSc4d1	empírový
náhrobek	náhrobek	k1gInSc4	náhrobek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
</s>
</p>
<p>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
synagoga	synagoga	k1gFnSc1	synagoga
čp.	čp.	k?	čp.
619	[number]	k4	619
–	–	k?	–
klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
synagoga	synagoga	k1gFnSc1	synagoga
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
zachovalou	zachovalý	k2eAgFnSc7d1	zachovalá
dispozicí	dispozice	k1gFnSc7	dispozice
a	a	k8xC	a
štukovou	štukový	k2eAgFnSc7d1	štuková
výzdobou	výzdoba	k1gFnSc7	výzdoba
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
prostoře	prostora	k1gFnSc6	prostora
<g/>
;	;	kIx,	;
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
moravských	moravský	k2eAgFnPc2d1	Moravská
synagog	synagoga	k1gFnPc2	synagoga
</s>
</p>
<p>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
s	s	k7c7	s
náhrobky	náhrobek	k1gInPc7	náhrobek
–	–	k?	–
založený	založený	k2eAgInSc1d1	založený
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1544	[number]	k4	1544
<g/>
;	;	kIx,	;
nejstarší	starý	k2eAgInPc1d3	nejstarší
náhrobky	náhrobek	k1gInPc1	náhrobek
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
116	[number]	k4	116
–	–	k?	–
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
renesanční	renesanční	k2eAgInSc4d1	renesanční
měšťanský	měšťanský	k2eAgInSc4d1	měšťanský
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
upravený	upravený	k2eAgInSc4d1	upravený
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
bohatě	bohatě	k6eAd1	bohatě
členěnou	členěný	k2eAgFnSc7d1	členěná
fasádou	fasáda	k1gFnSc7	fasáda
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
607	[number]	k4	607
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
sladovna	sladovna	k1gFnSc1	sladovna
–	–	k?	–
pozdně	pozdně	k6eAd1	pozdně
empírový	empírový	k2eAgInSc4d1	empírový
měšťanský	měšťanský	k2eAgInSc4d1	měšťanský
dům	dům	k1gInSc4	dům
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
se	s	k7c7	s
starším	starý	k2eAgNnSc7d2	starší
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
bývalá	bývalý	k2eAgFnSc1d1	bývalá
sladovna	sladovna	k1gFnSc1	sladovna
někdejší	někdejší	k2eAgFnSc2d1	někdejší
židovské	židovský	k2eAgFnSc2d1	židovská
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
,	,	kIx,	,
s	s	k7c7	s
terakotovými	terakotový	k2eAgInPc7d1	terakotový
doplňky	doplněk	k1gInPc7	doplněk
fasády	fasáda	k1gFnSc2	fasáda
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
81	[number]	k4	81
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
koželužna	koželužna	k1gFnSc1	koželužna
–	–	k?	–
předměstský	předměstský	k2eAgInSc4d1	předměstský
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
koželužna	koželužna	k1gFnSc1	koželužna
<g/>
,	,	kIx,	,
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
upravený	upravený	k2eAgInSc1d1	upravený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
<g/>
;	;	kIx,	;
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
původní	původní	k2eAgFnSc1d1	původní
dispozice	dispozice	k1gFnSc1	dispozice
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgInPc1d1	barokní
a	a	k8xC	a
rokokové	rokokový	k2eAgInPc1d1	rokokový
prvky	prvek	k1gInPc1	prvek
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
</s>
</p>
<p>
<s>
Kašna	kašna	k1gFnSc1	kašna
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
(	(	kIx(	(
<g/>
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
)	)	kIx)	)
–	–	k?	–
kamenická	kamenický	k2eAgFnSc1d1	kamenická
práce	práce	k1gFnSc1	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
maletínského	maletínský	k2eAgMnSc2d1	maletínský
kameníka	kameník	k1gMnSc2	kameník
Steigera	Steiger	k1gMnSc2	Steiger
a	a	k8xC	a
sochaře	sochař	k1gMnSc2	sochař
Rottra	Rottr	k1gMnSc2	Rottr
z	z	k7c2	z
Orlice	Orlice	k1gFnSc2	Orlice
u	u	k7c2	u
Králík	Králík	k1gMnSc1	Králík
</s>
</p>
<p>
<s>
Sloup	sloup	k1gInSc1	sloup
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
(	(	kIx(	(
<g/>
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
)	)	kIx)	)
–	–	k?	–
kamenická	kamenický	k2eAgFnSc1d1	kamenická
práce	práce	k1gFnSc1	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
maletínského	maletínský	k2eAgMnSc2d1	maletínský
kameníka	kameník	k1gMnSc2	kameník
Steigera	Steiger	k1gMnSc2	Steiger
a	a	k8xC	a
sochaře	sochař	k1gMnSc2	sochař
Rottra	Rottr	k1gMnSc2	Rottr
<g/>
,	,	kIx,	,
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
starší	starý	k2eAgFnSc2d2	starší
barokní	barokní	k2eAgFnSc2d1	barokní
skulptury	skulptura	k1gFnSc2	skulptura
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
trojice	trojice	k1gFnSc2	trojice
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
celku	celek	k1gInSc2	celek
o	o	k7c4	o
sochy	socha	k1gFnPc4	socha
sv.	sv.	kA	sv.
Šebestiána	Šebestián	k1gMnSc2	Šebestián
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Rocha	Roch	k1gMnSc4	Roch
<g/>
,	,	kIx,	,
světce	světec	k1gMnSc4	světec
bez	bez	k7c2	bez
atributů	atribut	k1gInPc2	atribut
a	a	k8xC	a
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
(	(	kIx(	(
<g/>
u	u	k7c2	u
mostu	most	k1gInSc2	most
při	při	k7c6	při
boku	bok	k1gInSc6	bok
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
247	[number]	k4	247
<g/>
)	)	kIx)	)
–	–	k?	–
barokní	barokní	k2eAgFnSc1d1	barokní
sochařská	sochařský	k2eAgFnSc1d1	sochařská
práce	práce	k1gFnSc1	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1735	[number]	k4	1735
<g/>
,	,	kIx,	,
doplněná	doplněný	k2eAgFnSc1d1	doplněná
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Sousoší	sousoší	k1gNnSc1	sousoší
Piety	pieta	k1gFnSc2	pieta
(	(	kIx(	(
<g/>
za	za	k7c7	za
hřbitovem	hřbitov	k1gInSc7	hřbitov
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
silnice	silnice	k1gFnSc2	silnice
na	na	k7c4	na
Moravičany	Moravičan	k1gMnPc4	Moravičan
<g/>
)	)	kIx)	)
–	–	k?	–
sochařská	sochařský	k2eAgFnSc1d1	sochařská
práce	práce	k1gFnSc1	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1735	[number]	k4	1735
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
343	[number]	k4	343
–	–	k?	–
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
malíře	malíř	k1gMnSc2	malíř
Adolfa	Adolf	k1gMnSc2	Adolf
KašparaNavržená	KašparaNavržený	k2eAgFnSc1d1	KašparaNavržený
na	na	k7c4	na
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Fara	fara	k1gFnSc1	fara
čp.	čp.	k?	čp.
22	[number]	k4	22
(	(	kIx(	(
<g/>
poblíž	poblíž	k7c2	poblíž
kostela	kostel	k1gInSc2	kostel
<g/>
)	)	kIx)	)
–	–	k?	–
jednopatrová	jednopatrový	k2eAgFnSc1d1	jednopatrová
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
stoletíDalší	stoletíDalší	k2eAgFnSc2d1	stoletíDalší
stavby	stavba	k1gFnSc2	stavba
</s>
</p>
<p>
<s>
Husův	Husův	k2eAgInSc1d1	Husův
sbor	sbor	k1gInSc1	sbor
–	–	k?	–
novokonstruktivistická	novokonstruktivistický	k2eAgFnSc1d1	novokonstruktivistický
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
</s>
</p>
<p>
<s>
==	==	k?	==
Spolky	spolek	k1gInPc1	spolek
a	a	k8xC	a
organizace	organizace	k1gFnPc1	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
Sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
</s>
</p>
<p>
<s>
Junák	junák	k1gMnSc1	junák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
<g/>
,	,	kIx,	,
středisko	středisko	k1gNnSc1	středisko
Loštice	Loštice	k1gFnSc2	Loštice
</s>
</p>
<p>
<s>
TJ	tj	kA	tj
Slavoj	Slavoj	k1gMnSc1	Slavoj
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
Loštice	Loštice	k1gFnSc2	Loštice
</s>
</p>
<p>
<s>
TJ	tj	kA	tj
Invaclub	Invaclub	k1gMnSc1	Invaclub
Loštice	Loštice	k1gFnSc2	Loštice
</s>
</p>
<p>
<s>
Jezdecký	jezdecký	k2eAgInSc1d1	jezdecký
klub	klub	k1gInSc1	klub
Loštice	Loštice	k1gFnSc2	Loštice
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
rybářský	rybářský	k2eAgInSc1d1	rybářský
svaz	svaz	k1gInSc1	svaz
–	–	k?	–
Loštice	Loštice	k1gFnSc2	Loštice
</s>
</p>
<p>
<s>
Jednota	jednota	k1gFnSc1	jednota
divadelních	divadelní	k2eAgMnPc2d1	divadelní
ochotníků	ochotník	k1gMnPc2	ochotník
</s>
</p>
<p>
<s>
Domov	domov	k1gInSc1	domov
u	u	k7c2	u
Třebůvky	Třebůvka	k1gFnSc2	Třebůvka
Loštice	Loštice	k1gFnSc2	Loštice
<g/>
,	,	kIx,	,
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc1	organizace
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
</s>
</p>
<p>
<s>
řeka	řeka	k1gFnSc1	řeka
Třebůvka	Třebůvka	k1gFnSc1	Třebůvka
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Loštice	Loštice	k1gFnSc2	Loštice
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
Loštice	Loštice	k1gFnSc1	Loštice
1923	[number]	k4	1923
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Loštice	Loštice	k1gFnSc2	Loštice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Loštice	Loštice	k1gFnSc2	Loštice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Region	region	k1gInSc1	region
Mohelnice	Mohelnice	k1gFnSc2	Mohelnice
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Olomoucké	olomoucký	k2eAgFnPc1d1	olomoucká
tvarůžky	tvarůžek	k1gInPc4	tvarůžek
</s>
</p>
