<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
a	a	k8xC	a
Kalicha	kalich	k1gInSc2	kalich
(	(	kIx(	(
<g/>
kolem	kolem	k6eAd1	kolem
1360	[number]	k4	1360
Trocnov	Trocnov	k1gInSc4	Trocnov
–	–	k?	–
11.	[number]	k4	11.
října	říjen	k1gInSc2	říjen
1424	[number]	k4	1424
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
husitský	husitský	k2eAgMnSc1d1	husitský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
,	,	kIx,	,
pokládaný	pokládaný	k2eAgMnSc1d1	pokládaný
za	za	k7c4	za
otce	otec	k1gMnSc4	otec
husitské	husitský	k2eAgFnSc2d1	husitská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
doktríny	doktrína	k1gFnSc2	doktrína
a	a	k8xC	a
autora	autor	k1gMnSc2	autor
či	či	k8xC	či
prvního	první	k4xOgMnSc2	první
uživatele	uživatel	k1gMnSc2	uživatel
defenzivní	defenzivní	k2eAgFnSc2d1	defenzivní
bojové	bojový	k2eAgFnSc2d1	bojová
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vozové	vozový	k2eAgFnSc2d1	vozová
hradby	hradba	k1gFnSc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
podrobněji	podrobně	k6eAd2	podrobně
zmapováno	zmapovat	k5eAaPmNgNnS	zmapovat
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc1	šest
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
Žižkových	Žižkových	k2eAgInPc6d1	Žižkových
předchozích	předchozí	k2eAgInPc6d1	předchozí
osudech	osud	k1gInPc6	osud
jsou	být	k5eAaImIp3nP	být
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
<g/>
,	,	kIx,	,
vycházet	vycházet	k5eAaImF	vycházet
lze	lze	k6eAd1	lze
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
kusých	kusý	k2eAgFnPc2d1	kusá
zmínek	zmínka	k1gFnPc2	zmínka
z	z	k7c2	z
několika	několik	k4yIc2	několik
náhodou	náhodou	k6eAd1	náhodou
zachovalých	zachovalý	k2eAgFnPc2d1	zachovalá
listin	listina	k1gFnPc2	listina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ani	ani	k9	ani
odborníci	odborník	k1gMnPc1	odborník
nemohou	moct	k5eNaImIp3nP	moct
shodnout	shodnout	k5eAaPmF	shodnout
a	a	k8xC	a
v	v	k7c6	v
biografických	biografický	k2eAgFnPc6d1	biografická
pracích	práce	k1gFnPc6	práce
věnovaných	věnovaný	k2eAgFnPc6d1	věnovaná
táborskému	táborský	k2eAgInSc3d1	táborský
hejtmanovi	hejtmanův	k2eAgMnPc1d1	hejtmanův
zastávají	zastávat	k5eAaImIp3nP	zastávat
často	často	k6eAd1	často
i	i	k9	i
vzájemně	vzájemně	k6eAd1	vzájemně
protichůdné	protichůdný	k2eAgInPc1d1	protichůdný
názory	názor	k1gInPc1	názor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1408	[number]	k4	1408
Žižka	Žižka	k1gMnSc1	Žižka
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
Rožmberkům	Rožmberk	k1gInPc3	Rožmberk
a	a	k8xC	a
královskému	královský	k2eAgNnSc3d1	královské
městu	město	k1gNnSc3	město
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
a	a	k8xC	a
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
záškodnické	záškodnický	k2eAgFnSc6d1	záškodnická
rotě	rota	k1gFnSc6	rota
jistého	jistý	k2eAgMnSc2d1	jistý
Matěje	Matěj	k1gMnSc2	Matěj
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
zločinů	zločin	k1gInPc2	zločin
králem	král	k1gMnSc7	král
Václavem	Václav	k1gMnSc7	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
omilostněn	omilostněn	k2eAgInSc4d1	omilostněn
a	a	k8xC	a
poté	poté	k6eAd1	poté
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
polského	polský	k2eAgMnSc2d1	polský
panovníka	panovník	k1gMnSc2	panovník
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagella	Jagella	k6eAd1	Jagella
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Sokola	Sokol	k1gMnSc2	Sokol
z	z	k7c2	z
Lamberka	Lamberka	k1gFnSc1	Lamberka
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
řádu	řád	k1gInSc3	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
historicky	historicky	k6eAd1	historicky
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bojoval	bojovat	k5eAaImAgMnS	bojovat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Grunwaldu	Grunwald	k1gInSc2	Grunwald
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
pobýval	pobývat	k5eAaImAgMnS	pobývat
jako	jako	k8xC	jako
královský	královský	k2eAgMnSc1d1	královský
čeledín	čeledín	k1gMnSc1	čeledín
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
kázáním	kázání	k1gNnSc7	kázání
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1419	[number]	k4	1419
byl	být	k5eAaImAgMnS	být
Žižka	Žižka	k1gMnSc1	Žižka
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
čelných	čelný	k2eAgMnPc2d1	čelný
účastníků	účastník	k1gMnPc2	účastník
první	první	k4xOgFnSc2	první
pražské	pražský	k2eAgFnSc2d1	Pražská
defenestrace	defenestrace	k1gFnSc2	defenestrace
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
kolísavou	kolísavý	k2eAgFnSc7d1	kolísavá
politikou	politika	k1gFnSc7	politika
pražské	pražský	k2eAgFnSc2d1	Pražská
radnice	radnice	k1gFnSc2	radnice
byla	být	k5eAaImAgFnS	být
příčinou	příčina	k1gFnSc7	příčina
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
metropoli	metropol	k1gFnSc3	metropol
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
svého	svůj	k3xOyFgMnSc2	svůj
prvního	první	k4xOgMnSc2	první
známého	známý	k1gMnSc2	známý
vítězství	vítězství	k1gNnSc2	vítězství
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vozové	vozový	k2eAgFnSc2d1	vozová
formace	formace	k1gFnSc2	formace
<g/>
,	,	kIx,	,
po	po	k7c6	po
pěti	pět	k4xCc6	pět
měsících	měsíc	k1gInPc6	měsíc
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
šlechtou	šlechta	k1gFnSc7	šlechta
byl	být	k5eAaImAgInS	být
nicméně	nicméně	k8xC	nicméně
nucen	nutit	k5eAaImNgMnS	nutit
město	město	k1gNnSc1	město
přenechat	přenechat	k5eAaPmF	přenechat
nepříteli	nepřítel	k1gMnSc3	nepřítel
a	a	k8xC	a
probít	probít	k5eAaPmF	probít
se	se	k3xPyFc4	se
k	k	k7c3	k
nově	nově	k6eAd1	nově
vznikajícímu	vznikající	k2eAgNnSc3d1	vznikající
Hradišti	Hradiště	k1gNnSc3	Hradiště
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Tábor	Tábor	k1gInSc1	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Táborská	táborský	k2eAgFnSc1d1	táborská
městská	městský	k2eAgFnSc1d1	městská
obec	obec	k1gFnSc1	obec
jej	on	k3xPp3gInSc4	on
záhy	záhy	k6eAd1	záhy
zvolila	zvolit	k5eAaPmAgFnS	zvolit
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
hejtmanů	hejtman	k1gMnPc2	hejtman
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
patrně	patrně	k6eAd1	patrně
náležel	náležet	k5eAaImAgMnS	náležet
post	post	k1gInSc4	post
vojenského	vojenský	k2eAgMnSc2d1	vojenský
velitele	velitel	k1gMnSc2	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
oko	oko	k1gNnSc4	oko
již	již	k6eAd1	již
slepý	slepý	k2eAgMnSc1d1	slepý
Žižka	Žižka	k1gMnSc1	Žižka
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
pokračujících	pokračující	k2eAgInPc2d1	pokračující
bojů	boj	k1gInPc2	boj
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
poranění	poranění	k1gNnSc2	poranění
druhého	druhý	k4xOgNnSc2	druhý
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
zcela	zcela	k6eAd1	zcela
oslepl	oslepnout	k5eAaPmAgInS	oslepnout
(	(	kIx(	(
<g/>
doloženo	doložen	k2eAgNnSc4d1	doloženo
od	od	k7c2	od
obléhání	obléhání	k1gNnSc2	obléhání
hradu	hrad	k1gInSc2	hrad
Rabí	rabí	k1gMnSc1	rabí
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1421	[number]	k4	1421
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
toto	tento	k3xDgNnSc4	tento
postižení	postižení	k1gNnSc4	postižení
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nezabránilo	zabránit	k5eNaPmAgNnS	zabránit
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
husitských	husitský	k2eAgInPc2d1	husitský
svazů	svaz	k1gInPc2	svaz
odrazil	odrazit	k5eAaPmAgInS	odrazit
vojska	vojsko	k1gNnSc2	vojsko
druhé	druhý	k4xOgFnSc2	druhý
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
domácím	domácí	k2eAgMnSc7d1	domácí
i	i	k8xC	i
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1423	[number]	k4	1423
se	se	k3xPyFc4	se
za	za	k7c2	za
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
jasných	jasný	k2eAgFnPc2d1	jasná
okolností	okolnost	k1gFnPc2	okolnost
rozešel	rozejít	k5eAaPmAgInS	rozejít
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
představiteli	představitel	k1gMnPc7	představitel
Tábora	Tábor	k1gInSc2	Tábor
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
východních	východní	k2eAgFnPc2d1	východní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
formovat	formovat	k5eAaImF	formovat
nové	nový	k2eAgNnSc4d1	nové
bratrstvo	bratrstvo	k1gNnSc4	bratrstvo
(	(	kIx(	(
<g/>
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Nový	nový	k2eAgInSc1d1	nový
nebo	nebo	k8xC	nebo
Menší	malý	k2eAgInSc1d2	menší
Tábor	Tábor	k1gInSc1	Tábor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vzrůstající	vzrůstající	k2eAgInSc1d1	vzrůstající
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
úspěchy	úspěch	k1gInPc4	úspěch
však	však	k9	však
brzy	brzy	k6eAd1	brzy
narazily	narazit	k5eAaPmAgFnP	narazit
na	na	k7c4	na
zájmy	zájem	k1gInPc4	zájem
pražanů	pražan	k1gMnPc2	pražan
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
měsíce	měsíc	k1gInPc1	měsíc
se	se	k3xPyFc4	se
nesly	nést	k5eAaImAgInP	nést
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
bojů	boj	k1gInPc2	boj
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
husitskými	husitský	k2eAgFnPc7d1	husitská
frakcemi	frakce	k1gFnPc7	frakce
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
nakonec	nakonec	k6eAd1	nakonec
eskalovala	eskalovat	k5eAaImAgFnS	eskalovat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
Žižka	Žižka	k1gMnSc1	Žižka
českou	český	k2eAgFnSc4d1	Česká
metropoli	metropole	k1gFnSc4	metropole
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
a	a	k8xC	a
přinutil	přinutit	k5eAaPmAgInS	přinutit
pražany	pražan	k1gMnPc4	pražan
vést	vést	k5eAaImF	vést
mírové	mírový	k2eAgInPc4d1	mírový
rozhovory	rozhovor	k1gInPc4	rozhovor
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
1424	[number]	k4	1424
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
urovnání	urovnání	k1gNnSc6	urovnání
konfliktu	konflikt	k1gInSc2	konflikt
ujednaly	ujednat	k5eAaPmAgInP	ujednat
oba	dva	k4xCgInPc1	dva
husitské	husitský	k2eAgInPc1d1	husitský
tábory	tábor	k1gInPc1	tábor
výpravu	výprava	k1gFnSc4	výprava
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
průběhu	průběh	k1gInSc6	průběh
Žižkova	Žižkův	k2eAgFnSc1d1	Žižkova
část	část	k1gFnSc1	část
vojska	vojsko	k1gNnSc2	vojsko
oblehla	oblehnout	k5eAaPmAgFnS	oblehnout
hrad	hrad	k1gInSc4	hrad
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
husitský	husitský	k2eAgMnSc1d1	husitský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
"	"	kIx"	"
<g/>
hlíznatému	hlíznatý	k2eAgMnSc3d1	hlíznatý
<g/>
"	"	kIx"	"
onemocnění	onemocnění	k1gNnSc3	onemocnění
(	(	kIx(	(
<g/>
snad	snad	k9	snad
karbunkl	karbunkl	k1gInSc1	karbunkl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
příčinou	příčina	k1gFnSc7	příčina
byl	být	k5eAaImAgInS	být
patrně	patrně	k6eAd1	patrně
neléčený	léčený	k2eNgInSc1d1	neléčený
zánětlivý	zánětlivý	k2eAgInSc1d1	zánětlivý
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
objevem	objev	k1gInSc7	objev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožnil	umožnit	k5eAaPmAgInS	umožnit
alespoň	alespoň	k9	alespoň
částečné	částečný	k2eAgNnSc4d1	částečné
zodpovězení	zodpovězený	k2eAgMnPc1d1	zodpovězený
otázek	otázka	k1gFnPc2	otázka
spjatých	spjatý	k2eAgFnPc6d1	spjatá
s	s	k7c7	s
Žižkovou	Žižkův	k2eAgFnSc7d1	Žižkova
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nález	nález	k1gInSc1	nález
tzv.	tzv.	kA	tzv.
čáslavské	čáslavský	k2eAgFnSc2d1	čáslavská
kalvy	kalva	k1gFnSc2	kalva
při	při	k7c6	při
archeologickém	archeologický	k2eAgInSc6d1	archeologický
průzkumu	průzkum	k1gInSc6	průzkum
prováděném	prováděný	k2eAgInSc6d1	prováděný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
v	v	k7c6	v
Čáslavi	Čáslav	k1gFnSc6	Čáslav
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
analýzách	analýza	k1gFnPc6	analýza
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
část	část	k1gFnSc4	část
kostry	kostra	k1gFnSc2	kostra
husitského	husitský	k2eAgMnSc2d1	husitský
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
(	(	kIx(	(
<g/>
1360	[number]	k4	1360
<g/>
–	–	k?	–
<g/>
1400	[number]	k4	1400
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
historicky	historicky	k6eAd1	historicky
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
spatřil	spatřit	k5eAaPmAgMnS	spatřit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1360	[number]	k4	1360
na	na	k7c6	na
menším	malý	k2eAgNnSc6d2	menší
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
poplužních	poplužní	k2eAgInPc2d1	poplužní
dvorců	dvorec	k1gInPc2	dvorec
na	na	k7c6	na
Trocnově	Trocnov	k1gInSc6	Trocnov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
narození	narození	k1gNnSc3	narození
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
Žižkově	Žižkov	k1gInSc6	Žižkov
dubu	dub	k1gInSc6	dub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1360	[number]	k4	1360
<g/>
–	–	k?	–
<g/>
1400	[number]	k4	1400
se	se	k3xPyFc4	se
o	o	k7c6	o
budoucím	budoucí	k2eAgMnSc6d1	budoucí
husitském	husitský	k2eAgMnSc6d1	husitský
vojevůdci	vojevůdce	k1gMnSc6	vojevůdce
uchovalo	uchovat	k5eAaPmAgNnS	uchovat
jen	jen	k9	jen
nepatrné	patrný	k2eNgNnSc1d1	nepatrné
množství	množství	k1gNnSc1	množství
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Enea	Enea	k6eAd1	Enea
Silvio	Silvio	k6eAd1	Silvio
Piccolomini	Piccolomin	k2eAgMnPc1d1	Piccolomin
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
své	svůj	k3xOyFgFnPc4	svůj
zprávy	zpráva	k1gFnPc4	zpráva
od	od	k7c2	od
přímých	přímý	k2eAgMnPc2d1	přímý
účastníků	účastník	k1gMnPc2	účastník
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
od	od	k7c2	od
Oldřicha	Oldřich	k1gMnSc2	Oldřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
)	)	kIx)	)
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Historie	historie	k1gFnSc1	historie
česká	český	k2eAgFnSc1d1	Česká
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Žižka	Žižka	k1gMnSc1	Žižka
byl	být	k5eAaImAgMnS	být
chudý	chudý	k2eAgMnSc1d1	chudý
a	a	k8xC	a
při	při	k7c6	při
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
živený	živený	k2eAgInSc4d1	živený
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zpráva	zpráva	k1gFnSc1	zpráva
italského	italský	k2eAgMnSc2d1	italský
humanisty	humanista	k1gMnSc2	humanista
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
reálném	reálný	k2eAgInSc6d1	reálný
základě	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ke	k	k7c3	k
dvoru	dvůr	k1gInSc3	dvůr
dostal	dostat	k5eAaPmAgMnS	dostat
ještě	ještě	k9	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
panování	panování	k1gNnPc2	panování
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc1	kolik
let	léto	k1gNnPc2	léto
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
strávil	strávit	k5eAaPmAgMnS	strávit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
pouze	pouze	k6eAd1	pouze
dohadovat	dohadovat	k5eAaImF	dohadovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
první	první	k4xOgFnSc1	první
autentická	autentický	k2eAgFnSc1d1	autentická
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Žižkově	Žižkův	k2eAgFnSc6d1	Žižkova
osobě	osoba	k1gFnSc6	osoba
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
3.	[number]	k4	3.
dubna	duben	k1gInSc2	duben
1378	[number]	k4	1378
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
z	z	k7c2	z
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
mu	on	k3xPp3gNnSc3	on
být	být	k5eAaImF	být
zhruba	zhruba	k6eAd1	zhruba
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
prvním	první	k4xOgInSc7	první
dokumentem	dokument	k1gInSc7	dokument
je	být	k5eAaImIp3nS	být
úřední	úřední	k2eAgFnSc1d1	úřední
listina	listina	k1gFnSc1	listina
sepsaná	sepsaný	k2eAgFnSc1d1	sepsaná
v	v	k7c6	v
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Mikuláš	mikuláš	k1gInSc1	mikuláš
řečený	řečený	k2eAgInSc1d1	řečený
Plachta	plachta	k1gFnSc1	plachta
z	z	k7c2	z
Boršova	Boršův	k2eAgNnSc2d1	Boršův
urovnává	urovnávat	k5eAaImIp3nS	urovnávat
majetkové	majetkový	k2eAgFnPc4d1	majetková
náležitosti	náležitost	k1gFnPc4	náležitost
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
svědek	svědek	k1gMnSc1	svědek
zde	zde	k6eAd1	zde
figuruje	figurovat	k5eAaImIp3nS	figurovat
Johanes	Johanes	k1gMnSc1	Johanes
dictus	dictus	k1gMnSc1	dictus
(	(	kIx(	(
<g/>
řečený	řečený	k2eAgMnSc1d1	řečený
<g/>
)	)	kIx)	)
Zizka	Zizka	k1gMnSc1	Zizka
de	de	k?	de
Trucznow	Trucznow	k1gMnSc1	Trucznow
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
písemný	písemný	k2eAgInSc1d1	písemný
pramen	pramen	k1gInSc1	pramen
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
11.	[number]	k4	11.
července	červenec	k1gInSc2	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dlužním	dlužní	k2eAgInSc6d1	dlužní
úpisu	úpis	k1gInSc6	úpis
se	se	k3xPyFc4	se
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
z	z	k7c2	z
Krupé	Krupý	k2eAgFnSc2d1	Krupá
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
splatit	splatit	k5eAaPmF	splatit
dvěma	dva	k4xCgFnPc3	dva
budějovickým	budějovický	k2eAgFnPc3d1	Budějovická
Židovkám	Židovka	k1gFnPc3	Židovka
dluh	dluh	k1gInSc4	dluh
3	[number]	k4	3
kopy	kopa	k1gFnSc2	kopa
a	a	k8xC	a
10	[number]	k4	10
grošů	groš	k1gInPc2	groš
(	(	kIx(	(
<g/>
asi	asi	k9	asi
380	[number]	k4	380
zlatých	zlatý	k2eAgFnPc2d1	zlatá
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
a	a	k8xC	a
Žižka	Žižka	k1gMnSc1	Žižka
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
rukojmě	rukojmě	k6eAd1	rukojmě
sám	sám	k3xTgMnSc1	sám
druhý	druhý	k4xOgMnSc1	druhý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
spoludlužník	spoludlužník	k1gMnSc1	spoludlužník
či	či	k8xC	či
druhý	druhý	k4xOgInSc4	druhý
rukojmí	rukojmí	k1gNnPc2	rukojmí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
veřejné	veřejný	k2eAgFnSc6d1	veřejná
listině	listina	k1gFnSc6	listina
<g/>
,	,	kIx,	,
sepsané	sepsaný	k2eAgFnSc6d1	sepsaná
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
v	v	k7c6	v
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
figuruje	figurovat	k5eAaImIp3nS	figurovat
jako	jako	k9	jako
třetí	třetí	k4xOgMnSc1	třetí
svědek	svědek	k1gMnSc1	svědek
při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
zboží	zboží	k1gNnSc2	zboží
Matěje	Matěj	k1gMnSc2	Matěj
z	z	k7c2	z
Holkova	Holkov	k1gInSc2	Holkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápisu	zápis	k1gInSc6	zápis
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k9	jako
Ješek	Ješek	k6eAd1	Ješek
řečený	řečený	k2eAgMnSc1d1	řečený
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
listině	listina	k1gFnSc6	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1381	[number]	k4	1381
se	se	k3xPyFc4	se
jistý	jistý	k2eAgMnSc1d1	jistý
Jesco	Jesco	k1gMnSc1	Jesco
de	de	k?	de
Trocznow	Trocznow	k1gMnSc1	Trocznow
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Ješek	Ješek	k6eAd1	Ješek
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
<g/>
,	,	kIx,	,
neúspěšně	úspěšně	k6eNd1	úspěšně
uchází	ucházet	k5eAaImIp3nS	ucházet
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
po	po	k7c6	po
jakémsi	jakýsi	k3yIgMnSc6	jakýsi
Mikešovi	Mikeš	k1gMnSc6	Mikeš
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bez	bez	k7c2	bez
přímých	přímý	k2eAgMnPc2d1	přímý
dědiců	dědic	k1gMnPc2	dědic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
písemnosti	písemnost	k1gFnSc6	písemnost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1384	[number]	k4	1384
Johanes	Johanes	k1gMnSc1	Johanes
dictus	dictus	k1gMnSc1	dictus
Zyzka	Zyzka	k1gMnSc1	Zyzka
de	de	k?	de
Trocznow	Trocznow	k1gMnSc1	Trocznow
prodává	prodávat	k5eAaImIp3nS	prodávat
lán	lán	k1gInSc4	lán
v	v	k7c6	v
Čeřejově	Čeřejův	k2eAgInSc6d1	Čeřejův
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
věnem	věno	k1gNnSc7	věno
náležel	náležet	k5eAaImAgMnS	náležet
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
Kateřině	Kateřina	k1gFnSc3	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
poslední	poslední	k2eAgInSc1d1	poslední
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1390	[number]	k4	1390
<g/>
–	–	k?	–
<g/>
1400	[number]	k4	1400
v	v	k7c6	v
nekrologiu	nekrologium	k1gNnSc6	nekrologium
krumlovského	krumlovský	k2eAgInSc2d1	krumlovský
konventu	konvent	k1gInSc2	konvent
řádu	řád	k1gInSc2	řád
minoritů	minorita	k1gMnPc2	minorita
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
doslova	doslova	k6eAd1	doslova
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Den	den	k1gInSc1	den
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
biskupa	biskup	k1gMnSc2	biskup
a	a	k8xC	a
vyznavače	vyznavač	k1gMnSc2	vyznavač
<g/>
.	.	kIx.	.
</s>
<s>
Ješek	Ješek	k1gMnSc1	Ješek
sládek	sládek	k1gMnSc1	sládek
z	z	k7c2	z
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
Peška	Peška	k1gMnSc1	Peška
<g/>
,	,	kIx,	,
lovčí	lovčí	k1gMnSc1	lovčí
páně	páně	k2eAgMnSc1d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
Řehoř	Řehoř	k1gMnSc1	Řehoř
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Jana	Jan	k1gMnSc2	Jan
řečeného	řečený	k2eAgMnSc2d1	řečený
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Johana	Johana	k1gFnSc1	Johana
a	a	k8xC	a
manželky	manželka	k1gFnPc1	manželka
obě	dva	k4xCgFnPc1	dva
jeho	jeho	k3xOp3gFnPc1	jeho
Kateřiny	Kateřina	k1gFnPc1	Kateřina
<g/>
,	,	kIx,	,
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
předky	předek	k1gInPc7	předek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tato	tento	k3xDgFnSc1	tento
věta	věta	k1gFnSc1	věta
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
v	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgMnS	mít
biskup	biskup	k1gMnSc1	biskup
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
svátek	svátek	k1gInSc4	svátek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
zádušní	zádušní	k2eAgFnSc1d1	zádušní
mše	mše	k1gFnSc1	mše
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
trocnovskou	trocnovský	k2eAgFnSc4d1	trocnovská
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Minority	minorita	k1gFnPc1	minorita
o	o	k7c4	o
zápis	zápis	k1gInSc4	zápis
zřejmě	zřejmě	k6eAd1	zřejmě
požádal	požádat	k5eAaPmAgMnS	požádat
sám	sám	k3xTgMnSc1	sám
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
.	.	kIx.	.
<g/>
Událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
historikům	historik	k1gMnPc3	historik
pomohla	pomoct	k5eAaPmAgNnP	pomoct
alespoň	alespoň	k9	alespoň
nepatrně	patrně	k6eNd1	patrně
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
do	do	k7c2	do
raných	raný	k2eAgNnPc2d1	rané
let	léto	k1gNnPc2	léto
Žižkova	Žižkův	k2eAgInSc2d1	Žižkův
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
objev	objev	k1gInSc1	objev
čáslavské	čáslavský	k2eAgFnSc2d1	čáslavská
kalvy	kalva	k1gFnSc2	kalva
<g/>
.	.	kIx.	.
</s>
<s>
Antropologickým	antropologický	k2eAgNnSc7d1	antropologické
ohledáním	ohledání	k1gNnSc7	ohledání
bylo	být	k5eAaImAgNnS	být
zjištěno	zjištěn	k2eAgNnSc1d1	zjištěno
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
očnici	očnice	k1gFnSc6	očnice
je	být	k5eAaImIp3nS	být
snížen	snížit	k5eAaPmNgInS	snížit
její	její	k3xOp3gInSc1	její
strop	strop	k1gInSc1	strop
jako	jako	k8xC	jako
následek	následek	k1gInSc1	následek
poranění	poranění	k1gNnSc2	poranění
krajiny	krajina	k1gFnSc2	krajina
levého	levý	k2eAgNnSc2d1	levé
oka	oko	k1gNnSc2	oko
utrpěného	utrpěný	k2eAgNnSc2d1	utrpěné
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
růstu	růst	k1gInSc2	růst
individua	individuum	k1gNnSc2	individuum
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
kost	kost	k1gFnSc1	kost
skutečně	skutečně	k6eAd1	skutečně
náleží	náležet	k5eAaImIp3nS	náležet
husitskému	husitský	k2eAgMnSc3d1	husitský
vojevůdci	vojevůdce	k1gMnSc3	vojevůdce
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
dovodit	dovodit	k5eAaPmF	dovodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c4	o
oko	oko	k1gNnSc4	oko
přišel	přijít	k5eAaPmAgInS	přijít
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
let	léto	k1gNnPc2	léto
úderem	úder	k1gInSc7	úder
sečné	sečný	k2eAgFnPc4d1	sečná
zbraně	zbraň	k1gFnPc4	zbraň
do	do	k7c2	do
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Zranění	zranění	k1gNnSc2	zranění
Žižkův	Žižkův	k2eAgInSc4d1	Žižkův
obličej	obličej	k1gInSc4	obličej
zdeformovalo	zdeformovat	k5eAaPmAgNnS	zdeformovat
a	a	k8xC	a
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
pokles	pokles	k1gInSc4	pokles
levého	levý	k2eAgNnSc2d1	levé
oka	oko	k1gNnSc2	oko
oproti	oproti	k7c3	oproti
oku	oko	k1gNnSc3	oko
pravému	pravý	k2eAgNnSc3d1	pravé
o	o	k7c6	o
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
4	[number]	k4	4
mm	mm	kA	mm
a	a	k8xC	a
vyhnulo	vyhnout	k5eAaPmAgNnS	vyhnout
jeho	on	k3xPp3gInSc4	on
orlí	orlí	k2eAgInSc4d1	orlí
nos	nos	k1gInSc4	nos
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zbojnická	zbojnický	k2eAgNnPc4d1	zbojnické
léta	léto	k1gNnPc4	léto
(	(	kIx(	(
<g/>
1408	[number]	k4	1408
<g/>
–	–	k?	–
<g/>
1409	[number]	k4	1409
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
dochované	dochovaný	k2eAgFnPc1d1	dochovaná
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
Janu	Jan	k1gMnSc6	Jan
Žižkovi	Žižka	k1gMnSc6	Žižka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1408	[number]	k4	1408
<g/>
–	–	k?	–
<g/>
1409	[number]	k4	1409
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
o	o	k7c4	o
zápisy	zápis	k1gInPc4	zápis
v	v	k7c6	v
Popravčí	popravčí	k2eAgFnSc6d1	popravčí
knize	kniha	k1gFnSc6	kniha
pánův	pánův	k2eAgInSc4d1	pánův
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
a	a	k8xC	a
v	v	k7c6	v
Jihlavské	jihlavský	k2eAgFnSc6d1	Jihlavská
popravčí	popravčí	k2eAgFnSc6d1	popravčí
knize	kniha	k1gFnSc6	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
archiválií	archiválie	k1gFnPc2	archiválie
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenán	k2eAgFnPc4d1	zaznamenána
výpovědi	výpověď	k1gFnPc4	výpověď
zločinců	zločinec	k1gMnPc2	zločinec
a	a	k8xC	a
zemských	zemský	k2eAgMnPc2d1	zemský
škůdců	škůdce	k1gMnPc2	škůdce
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
15.	[number]	k4	15.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1408	[number]	k4	1408
Žižka	Žižka	k1gMnSc1	Žižka
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
Jindřichovi	Jindřich	k1gMnSc6	Jindřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
a	a	k8xC	a
královskému	královský	k2eAgNnSc3d1	královské
městu	město	k1gNnSc3	město
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
zbojnické	zbojnický	k2eAgFnSc2d1	Zbojnická
roty	rota	k1gFnSc2	rota
muže	muž	k1gMnSc2	muž
známého	známý	k1gMnSc2	známý
jako	jako	k8xS	jako
Matěj	Matěj	k1gMnSc1	Matěj
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rožmberské	rožmberský	k2eAgFnSc6d1	Rožmberská
popravčí	popravčí	k2eAgFnSc6d1	popravčí
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
týkají	týkat	k5eAaImIp3nP	týkat
dva	dva	k4xCgInPc4	dva
zápisy	zápis	k1gInPc4	zápis
<g/>
.	.	kIx.	.
</s>
<s>
Výpověď	výpověď	k1gFnSc1	výpověď
jistého	jistý	k2eAgInSc2d1	jistý
Pivce	Pivec	k1gInSc2	Pivec
a	a	k8xC	a
doznání	doznání	k1gNnSc1	doznání
samotného	samotný	k2eAgMnSc2d1	samotný
Matěje	Matěj	k1gMnSc2	Matěj
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
právu	právo	k1gNnSc3	právo
útrpnému	útrpný	k2eAgInSc3d1	útrpný
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1409	[number]	k4	1409
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obsahů	obsah	k1gInPc2	obsah
obou	dva	k4xCgNnPc2	dva
prohlášení	prohlášení	k1gNnPc2	prohlášení
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
rota	rota	k1gFnSc1	rota
nejednala	jednat	k5eNaImAgFnS	jednat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
také	také	k9	také
práci	práce	k1gFnSc4	práce
sjednanou	sjednaný	k2eAgFnSc4d1	sjednaná
a	a	k8xC	a
placenou	placený	k2eAgFnSc4d1	placená
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejím	její	k3xOp3gMnPc3	její
významným	významný	k2eAgMnPc3d1	významný
zaměstnavatelům	zaměstnavatel	k1gMnPc3	zaměstnavatel
se	se	k3xPyFc4	se
počítali	počítat	k5eAaImAgMnP	počítat
zejména	zejména	k9	zejména
Litol	Litol	k1gInSc4	Litol
a	a	k8xC	a
Albrecht	Albrecht	k1gMnSc1	Albrecht
Bítovští	Bítovský	k1gMnPc1	Bítovský
z	z	k7c2	z
Lichtenburku	Lichtenburk	k1gInSc2	Lichtenburk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
roli	role	k1gFnSc6	role
lapky	lapka	k1gMnPc4	lapka
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
setkal	setkat	k5eAaPmAgMnS	setkat
<g/>
,	,	kIx,	,
patřili	patřit	k5eAaImAgMnP	patřit
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Jan	Jan	k1gMnSc1	Jan
Sokol	Sokol	k1gMnSc1	Sokol
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
<g/>
,	,	kIx,	,
Hynek	Hynek	k1gMnSc1	Hynek
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Suchý	Suchý	k1gMnSc1	Suchý
Čert	čert	k1gMnSc1	čert
či	či	k8xC	či
Racek	racek	k1gMnSc1	racek
Kobyla	kobyla	k1gFnSc1	kobyla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záznamy	záznam	k1gInPc1	záznam
v	v	k7c6	v
Jihlavské	jihlavský	k2eAgFnSc6d1	Jihlavská
popravčí	popravčí	k2eAgFnSc6d1	popravčí
knize	kniha	k1gFnSc6	kniha
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
obsáhlé	obsáhlý	k2eAgInPc1d1	obsáhlý
než	než	k8xS	než
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
rožmberské	rožmberský	k2eAgFnSc6d1	Rožmberská
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
jméno	jméno	k1gNnSc1	jméno
Žižka	Žižka	k1gMnSc1	Žižka
uváděno	uvádět	k5eAaImNgNnS	uvádět
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
podobách	podoba	k1gFnPc6	podoba
–	–	k?	–
Schysska	Schyssek	k1gMnSc2	Schyssek
a	a	k8xC	a
Zyzka	Zyzek	k1gMnSc2	Zyzek
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
dva	dva	k4xCgMnPc4	dva
rozdílné	rozdílný	k2eAgMnPc4d1	rozdílný
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
však	však	k9	však
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemanem	zeman	k1gMnSc7	zeman
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
je	být	k5eAaImIp3nS	být
prve	prve	k6eAd1	prve
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Schysska	Schysska	k1gFnSc1	Schysska
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
budoucím	budoucí	k2eAgMnSc6d1	budoucí
husitském	husitský	k2eAgMnSc6d1	husitský
vojevůdci	vojevůdce	k1gMnSc6	vojevůdce
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Janka	Janek	k1gMnSc2	Janek
<g/>
,	,	kIx,	,
pacholka	pacholek	k1gMnSc2	pacholek
Kolúchova	Kolúchův	k2eAgMnSc2d1	Kolúchův
(	(	kIx(	(
<g/>
pravděp	pravděp	k1gInSc1	pravděp
<g/>
.	.	kIx.	.
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Jana	Jan	k1gMnSc4	Jan
Kolúcha	Kolúch	k1gMnSc4	Kolúch
z	z	k7c2	z
Pňové	Pňový	k2eAgFnSc2d1	Pňový
Lhoty	Lhota	k1gFnSc2	Lhota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
oběšen	oběsit	k5eAaPmNgInS	oběsit
ve	v	k7c6	v
Stráži	stráž	k1gFnSc6	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mučidlech	mučidlo	k1gNnPc6	mučidlo
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Matěj	Matěj	k1gMnSc1	Matěj
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
,	,	kIx,	,
a	a	k8xC	a
Byňovec	Byňovec	k1gInSc4	Byňovec
chtěli	chtít	k5eAaImAgMnP	chtít
zradit	zradit	k5eAaPmF	zradit
panu	pan	k1gMnSc3	pan
Alšíkovi	Alšík	k1gMnSc3	Alšík
z	z	k7c2	z
Bítova	Bítov	k1gInSc2	Bítov
a	a	k8xC	a
mladému	mladý	k1gMnSc3	mladý
panu	pan	k1gMnSc3	pan
Krojíři	krojíř	k1gMnSc3	krojíř
<g/>
,	,	kIx,	,
Žižkovi	Žižka	k1gMnSc3	Žižka
a	a	k8xC	a
Kolúchovi	Kolúch	k1gMnSc3	Kolúch
Nové	Nové	k2eAgFnPc1d1	Nové
Hrady	hrad	k1gInPc4	hrad
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc4	hrad
i	i	k8xC	i
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
pan	pan	k1gMnSc1	pan
Alšík	Alšík	k1gInSc4	Alšík
slíbil	slíbit	k5eAaPmAgMnS	slíbit
a	a	k8xC	a
zavázal	zavázat	k5eAaPmAgMnS	zavázat
se	se	k3xPyFc4	se
postoupiti	postoupit	k5eAaPmF	postoupit
nějaké	nějaký	k3yIgFnPc4	nějaký
tvrze	tvrz	k1gFnPc4	tvrz
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
chtěli	chtít	k5eAaImAgMnP	chtít
jmenovaní	jmenovaný	k1gMnPc1	jmenovaný
zraditi	zradit	k5eAaPmF	zradit
panu	pan	k1gMnSc3	pan
Plšíkovi	plšík	k1gMnSc3	plšík
i	i	k8xC	i
město	město	k1gNnSc4	město
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
.	.	kIx.	.
</s>
<s>
Tovaryši	tovaryš	k1gMnPc1	tovaryš
jejich	jejich	k3xOp3gMnSc4	jejich
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Žižka	Žižka	k1gMnSc1	Žižka
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
Schysska	Schyssko	k1gNnSc2	Schyssko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Janech	Jan	k1gMnPc6	Jan
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Brada	Brada	k1gMnSc1	Brada
<g/>
,	,	kIx,	,
Matěj	Matěj	k1gMnSc1	Matěj
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
,	,	kIx,	,
Janek	Janek	k1gMnSc1	Janek
jeho	jeho	k3xOp3gMnSc1	jeho
Bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
Jiřiňk	Jiřiňk	k1gMnSc1	Jiřiňk
z	z	k7c2	z
Plzeňska	Plzeňsko	k1gNnSc2	Plzeňsko
<g/>
,	,	kIx,	,
Holba	holba	k1gFnSc1	holba
z	z	k7c2	z
Chotěboře	Chotěboř	k1gFnSc2	Chotěboř
aj.	aj.	kA	aj.
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
zápisu	zápis	k1gInSc6	zápis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
jistý	jistý	k2eAgMnSc1d1	jistý
Zikmund	Zikmund	k1gMnSc1	Zikmund
z	z	k7c2	z
Kupergu	Kuperg	k1gInSc2	Kuperg
vypovídal	vypovídat	k5eAaPmAgMnS	vypovídat
v	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
členů	člen	k1gInPc2	člen
tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Jana	Jan	k1gMnSc2	Jan
Sokola	Sokol	k1gMnSc2	Sokol
z	z	k7c2	z
Lamberka	Lamberka	k1gFnSc1	Lamberka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
Rossak	Rossak	k1gInSc1	Rossak
<g/>
,	,	kIx,	,
Paleček	Paleček	k1gMnSc1	Paleček
<g/>
,	,	kIx,	,
Žižka	Žižka	k1gMnSc1	Žižka
(	(	kIx(	(
<g/>
Zyzka	Zyzka	k1gMnSc1	Zyzka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Šiška	šiška	k1gFnSc1	šiška
(	(	kIx(	(
<g/>
Schysska	Schysska	k1gFnSc1	Schysska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Veliký	veliký	k2eAgMnSc1d1	veliký
obrali	obrat	k5eAaPmAgMnP	obrat
dva	dva	k4xCgInPc4	dva
jihlavské	jihlavský	k2eAgMnPc4d1	jihlavský
měšťany	měšťan	k1gMnPc4	měšťan
<g/>
.	.	kIx.	.
</s>
<s>
Znova	znova	k6eAd1	znova
se	se	k3xPyFc4	se
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
jméno	jméno	k1gNnSc1	jméno
objevuje	objevovat	k5eAaImIp3nS	objevovat
až	až	k9	až
v	v	k7c6	v
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
seznamu	seznam	k1gInSc6	seznam
lapků	lapka	k1gMnPc2	lapka
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
výslovně	výslovně	k6eAd1	výslovně
napsáno	napsat	k5eAaPmNgNnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
u	u	k7c2	u
Sokola	Sokol	k1gMnSc2	Sokol
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Lapkovská	lapkovský	k2eAgFnSc1d1	lapkovská
dráha	dráha	k1gFnSc1	dráha
trocnovského	trocnovský	k2eAgMnSc2d1	trocnovský
zemana	zeman	k1gMnSc2	zeman
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
25.	[number]	k4	25.
dubna	duben	k1gInSc2	duben
1409	[number]	k4	1409
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c4	v
jeho	jeho	k3xOp3gInSc4	jeho
prospěch	prospěch	k1gInSc4	prospěch
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Žebráku	Žebrák	k1gInSc2	Žebrák
vydal	vydat	k5eAaPmAgInS	vydat
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
deklaroval	deklarovat	k5eAaBmAgMnS	deklarovat
purkmistrovi	purkmistr	k1gMnSc3	purkmistr
a	a	k8xC	a
konšelům	konšel	k1gMnPc3	konšel
města	město	k1gNnSc2	město
Budějovice	Budějovice	k1gInPc4	Budějovice
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
je	být	k5eAaImIp3nS	být
uděleno	udělen	k2eAgNnSc4d1	uděleno
svolení	svolení	k1gNnSc4	svolení
usmířit	usmířit	k5eAaPmF	usmířit
se	se	k3xPyFc4	se
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Žižkou	Žižka	k1gMnSc7	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
nechal	nechat	k5eAaPmAgMnS	nechat
král	král	k1gMnSc1	král
na	na	k7c6	na
Točníku	Točník	k1gInSc6	Točník
vyhotovit	vyhotovit	k5eAaPmF	vyhotovit
nový	nový	k2eAgInSc4d1	nový
amnestující	amnestující	k2eAgInSc4d1	amnestující
list	list	k1gInSc4	list
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
Budějovickým	budějovický	k2eAgMnPc3d1	budějovický
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijal	přijmout	k5eAaPmAgMnS	přijmout
na	na	k7c6	na
milosti	milost	k1gFnSc6	milost
Jana	Jan	k1gMnSc4	Jan
řečeného	řečený	k2eAgMnSc4d1	řečený
Žižku	Žižka	k1gMnSc4	Žižka
<g/>
,	,	kIx,	,
svého	svůj	k1gMnSc4	svůj
milého	milý	k2eAgMnSc4d1	milý
věrného	věrný	k2eAgMnSc4d1	věrný
<g/>
,	,	kIx,	,
odpouštěje	odpouštět	k5eAaImSgInS	odpouštět
jemu	on	k3xPp3gMnSc3	on
milostivě	milostivě	k6eAd1	milostivě
všechny	všechen	k3xTgFnPc1	všechen
výtržnosti	výtržnost	k1gFnPc1	výtržnost
učiněné	učiněný	k2eAgFnPc1d1	učiněná
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
proti	proti	k7c3	proti
koruně	koruna	k1gFnSc3	koruna
království	království	k1gNnSc2	království
Českého	český	k2eAgNnSc2d1	české
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Žoldnéř	žoldnéř	k1gMnSc1	žoldnéř
a	a	k8xC	a
královský	královský	k2eAgMnSc1d1	královský
čeledín	čeledín	k1gMnSc1	čeledín
(	(	kIx(	(
<g/>
1410	[number]	k4	1410
<g/>
–	–	k?	–
<g/>
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
dalších	další	k2eAgInPc6d1	další
osudech	osud	k1gInPc6	osud
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
informuje	informovat	k5eAaBmIp3nS	informovat
dílo	dílo	k1gNnSc4	dílo
polského	polský	k2eAgMnSc2d1	polský
kronikáře	kronikář	k1gMnSc2	kronikář
Jana	Jan	k1gMnSc2	Jan
Długosze	Długosze	k1gFnSc1	Długosze
Annales	Annales	k1gMnSc1	Annales
Poloniae	Poloniae	k1gInSc1	Poloniae
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jediného	jediný	k2eAgInSc2d1	jediný
zápisu	zápis	k1gInSc2	zápis
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1409	[number]	k4	1409
<g/>
–	–	k?	–
<g/>
1411	[number]	k4	1411
jako	jako	k8xC	jako
žoldnéř	žoldnéř	k1gMnSc1	žoldnéř
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagella	Jagella	k1gMnSc1	Jagella
účastnil	účastnit	k5eAaImAgMnS	účastnit
bojů	boj	k1gInPc2	boj
proti	proti	k7c3	proti
Řádu	řád	k1gInSc3	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
souvětí	souvětí	k1gNnSc2	souvětí
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byl	být	k5eAaImAgInS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Grunwaldu	Grunwald	k1gInSc2	Grunwald
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zmínka	zmínka	k1gFnSc1	zmínka
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
teprve	teprve	k6eAd1	teprve
k	k	k7c3	k
válečným	válečný	k2eAgFnPc3d1	válečná
operacím	operace	k1gFnPc3	operace
po	po	k7c6	po
grunwaldském	grunwaldský	k2eAgNnSc6d1	grunwaldský
vítězství	vítězství	k1gNnSc6	vítězství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vojsko	vojsko	k1gNnSc1	vojsko
polského	polský	k2eAgMnSc2d1	polský
panovníka	panovník	k1gMnSc2	panovník
obléhalo	obléhat	k5eAaImAgNnS	obléhat
a	a	k8xC	a
dobývalo	dobývat	k5eAaImAgNnS	dobývat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
řádové	řádový	k2eAgFnPc4d1	řádová
pevnosti	pevnost	k1gFnPc4	pevnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
Žižka	Žižka	k1gMnSc1	Žižka
(	(	kIx(	(
<g/>
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
<g/>
)	)	kIx)	)
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
středověkých	středověký	k2eAgInPc6d1	středověký
pramenech	pramen	k1gInPc6	pramen
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1414	[number]	k4	1414
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soudní	soudní	k2eAgFnSc6d1	soudní
knize	kniha	k1gFnSc6	kniha
novoměstské	novoměstský	k2eAgFnSc6d1	Novoměstská
je	on	k3xPp3gMnPc4	on
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
jistý	jistý	k2eAgMnSc1d1	jistý
Janek	Janek	k1gMnSc1	Janek
Jednooký	jednooký	k2eAgMnSc1d1	jednooký
<g/>
,	,	kIx,	,
vrátný	vrátný	k1gMnSc1	vrátný
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
od	od	k7c2	od
Václava	Václav	k1gMnSc2	Václav
Stoherky	Stoherka	k1gFnSc2	Stoherka
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
padesáti	padesát	k4xCc2	padesát
kop	kopa	k1gFnPc2	kopa
grošů	groš	k1gInPc2	groš
dům	dům	k1gInSc4	dům
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
Příkopě	příkop	k1gInSc6	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
se	se	k3xPyFc4	se
však	však	k9	však
dlouho	dlouho	k6eAd1	dlouho
netěšil	těšit	k5eNaImAgInS	těšit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1416	[number]	k4	1416
jej	on	k3xPp3gMnSc4	on
prodal	prodat	k5eAaPmAgInS	prodat
a	a	k8xC	a
za	za	k7c4	za
částku	částka	k1gFnSc4	částka
13	[number]	k4	13
kop	kopa	k1gFnPc2	kopa
grošů	groš	k1gInPc2	groš
si	se	k3xPyFc3	se
pořídil	pořídit	k5eAaPmAgMnS	pořídit
rezidenci	rezidence	k1gFnSc4	rezidence
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
nebyl	být	k5eNaImAgMnS	být
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
vrátného	vrátné	k1gNnSc2	vrátné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
soudní	soudní	k2eAgFnSc1d1	soudní
kniha	kniha	k1gFnSc1	kniha
nazývá	nazývat	k5eAaImIp3nS	nazývat
Janka	Janka	k1gFnSc1	Janka
Jednookého	jednooký	k2eAgInSc2d1	jednooký
panošem	panoš	k1gMnSc7	panoš
od	od	k7c2	od
dvora	dvůr	k1gInSc2	dvůr
králova	králův	k2eAgInSc2d1	králův
<g/>
.	.	kIx.	.
<g/>
Co	co	k3yRnSc1	co
přesně	přesně	k6eAd1	přesně
dělal	dělat	k5eAaImAgMnS	dělat
Žižka	Žižka	k1gMnSc1	Žižka
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
polským	polský	k2eAgNnSc7d1	polské
tažením	tažení	k1gNnSc7	tažení
a	a	k8xC	a
počátkem	počátek	k1gInSc7	počátek
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
dalších	další	k2eAgInPc2d1	další
dohadů	dohad	k1gInPc2	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
anglické	anglický	k2eAgInPc1d1	anglický
slovníky	slovník	k1gInPc1	slovník
vydané	vydaný	k2eAgInPc1d1	vydaný
v	v	k7c4	v
19.	[number]	k4	19.
století	století	k1gNnPc2	století
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
neověřené	ověřený	k2eNgFnPc4d1	neověřená
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Jindřicha	Jindřich	k1gMnSc2	Jindřich
V.	V.	kA	V.
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přímým	přímý	k2eAgMnSc7d1	přímý
aktérem	aktér	k1gMnSc7	aktér
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Azincourtu	Azincourt	k1gInSc2	Azincourt
<g/>
.	.	kIx.	.
</s>
<s>
Nepodložená	podložený	k2eNgNnPc1d1	nepodložené
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
tvrzení	tvrzení	k1gNnSc4	tvrzení
dalších	další	k2eAgFnPc2d1	další
moderních	moderní	k2eAgFnPc2d1	moderní
encyklopedií	encyklopedie	k1gFnPc2	encyklopedie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
voják	voják	k1gMnSc1	voják
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Sasku	Sasko	k1gNnSc6	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
nelze	lze	k6eNd1	lze
doložit	doložit	k5eAaPmF	doložit
ani	ani	k8xC	ani
zažitou	zažitý	k2eAgFnSc4d1	zažitá
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
službě	služba	k1gFnSc6	služba
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
komorníka	komorník	k1gMnSc2	komorník
královny	královna	k1gFnSc2	královna
Žofie	Žofie	k1gFnSc2	Žofie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Husitský	husitský	k2eAgMnSc1d1	husitský
vůdce	vůdce	k1gMnSc1	vůdce
(	(	kIx(	(
<g/>
1419	[number]	k4	1419
<g/>
–	–	k?	–
<g/>
1424	[number]	k4	1424
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rok	rok	k1gInSc1	rok
1419	[number]	k4	1419
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
jaký	jaký	k3yIgInSc4	jaký
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
vztah	vztah	k1gInSc4	vztah
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Husovi	Hus	k1gMnSc3	Hus
a	a	k8xC	a
jakým	jaký	k3yQgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
probíhal	probíhat	k5eAaImAgInS	probíhat
jeho	jeho	k3xOp3gInSc1	jeho
přerod	přerod	k1gInSc1	přerod
v	v	k7c4	v
ortodoxního	ortodoxní	k2eAgMnSc4d1	ortodoxní
zastánce	zastánce	k1gMnSc4	zastánce
husitského	husitský	k2eAgNnSc2d1	husitské
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
opět	opět	k6eAd1	opět
chybí	chybit	k5eAaPmIp3nS	chybit
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
by	by	kYmCp3nS	by
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
kteroukoliv	kterýkoliv	k3yIgFnSc4	kterýkoliv
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
nedostatek	nedostatek	k1gInSc4	nedostatek
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
procesu	proces	k1gInSc3	proces
nezbytně	zbytně	k6eNd1	zbytně
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
písemné	písemný	k2eAgInPc1d1	písemný
prameny	pramen	k1gInPc1	pramen
informují	informovat	k5eAaBmIp3nP	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
30.	[number]	k4	30.
července	červenec	k1gInSc2	červenec
1419	[number]	k4	1419
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
účastníků	účastník	k1gMnPc2	účastník
události	událost	k1gFnSc2	událost
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xC	jako
první	první	k4xOgFnSc2	první
pražské	pražský	k2eAgFnSc2d1	Pražská
defenestrace	defenestrace	k1gFnSc2	defenestrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Podle	podle	k7c2	podle
záznamu	záznam	k1gInSc2	záznam
ve	v	k7c6	v
Starých	Starých	k2eAgInPc6d1	Starých
letopisech	letopis	k1gInPc6	letopis
českých	český	k2eAgInPc6d1	český
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
předem	předem	k6eAd1	předem
připravenou	připravený	k2eAgFnSc4d1	připravená
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
častým	častý	k2eAgInPc3d1	častý
předpokladům	předpoklad	k1gInPc3	předpoklad
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Josef	Josef	k1gMnSc1	Josef
Pekař	Pekař	k1gMnSc1	Pekař
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
účastnil	účastnit	k5eAaImAgInS	účastnit
jako	jako	k9	jako
světský	světský	k2eAgMnSc1d1	světský
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
domněnce	domněnka	k1gFnSc3	domněnka
vede	vést	k5eAaImIp3nS	vést
zápis	zápis	k1gInSc1	zápis
literáta	literát	k1gMnSc2	literát
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
Husitské	husitský	k2eAgFnSc6d1	husitská
kronice	kronika	k1gFnSc6	kronika
k	k	k7c3	k
události	událost	k1gFnSc3	událost
poněkud	poněkud	k6eAd1	poněkud
lakonicky	lakonicky	k6eAd1	lakonicky
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
a	a	k8xC	a
k	k	k7c3	k
té	ten	k3xDgFnSc3	ten
obci	obec	k1gFnSc3	obec
přidal	přidat	k5eAaPmAgMnS	přidat
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
svrchujmenovaného	svrchujmenovaný	k2eAgMnSc2d1	svrchujmenovaný
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
navíc	navíc	k6eAd1	navíc
další	další	k2eAgFnSc1d1	další
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
Žižkově	Žižkův	k2eAgFnSc6d1	Žižkova
osobě	osoba	k1gFnSc6	osoba
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
až	až	k9	až
k	k	k7c3	k
25.	[number]	k4	25.
říjnu	říjen	k1gInSc3	říjen
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ke	k	k7c3	k
dni	den	k1gInSc3	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
novoměstská	novoměstský	k2eAgFnSc1d1	Novoměstská
obec	obec	k1gFnSc1	obec
obsadila	obsadit	k5eAaPmAgFnS	obsadit
pevnost	pevnost	k1gFnSc4	pevnost
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Pokračujících	pokračující	k2eAgInPc2d1	pokračující
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
i	i	k9	i
mezi	mezi	k7c7	mezi
hradbami	hradba	k1gFnPc7	hradba
české	český	k2eAgFnSc2d1	Česká
metropole	metropol	k1gFnSc2	metropol
se	se	k3xPyFc4	se
jednooký	jednooký	k2eAgMnSc1d1	jednooký
zeman	zeman	k1gMnSc1	zeman
patrně	patrně	k6eAd1	patrně
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
uzavření	uzavření	k1gNnSc2	uzavření
listopadového	listopadový	k2eAgNnSc2d1	listopadové
půlročního	půlroční	k2eAgNnSc2d1	půlroční
příměří	příměří	k1gNnSc2	příměří
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
velitele	velitel	k1gMnSc2	velitel
vyšehradské	vyšehradský	k2eAgFnSc2d1	Vyšehradská
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
<g/>
Snad	snad	k9	snad
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1419	[number]	k4	1419
Žižka	Žižka	k1gMnSc1	Žižka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Břeňkem	Břeněk	k1gInSc7	Břeněk
Švihovským	Švihovský	k2eAgInSc7d1	Švihovský
z	z	k7c2	z
Dolan	Dolany	k1gInPc2	Dolany
<g/>
,	,	kIx,	,
Valkounem	Valkoun	k1gInSc7	Valkoun
z	z	k7c2	z
Adlaru	Adlar	k1gInSc2	Adlar
a	a	k8xC	a
ozbrojeným	ozbrojený	k2eAgInSc7d1	ozbrojený
houfem	houf	k1gInSc7	houf
opustil	opustit	k5eAaPmAgInS	opustit
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
působiště	působiště	k1gNnSc4	působiště
kněze	kněz	k1gMnSc2	kněz
Korandy	Koranda	k1gFnSc2	Koranda
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
jejich	jejich	k3xOp3gInSc2	jejich
odchodu	odchod	k1gInSc2	odchod
byla	být	k5eAaImAgFnS	být
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
kolísavou	kolísavý	k2eAgFnSc7d1	kolísavá
politikou	politika	k1gFnSc7	politika
pražské	pražský	k2eAgFnSc2d1	Pražská
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
potřeba	potřeba	k6eAd1	potřeba
zázemí	zázemí	k1gNnSc1	zázemí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jim	on	k3xPp3gMnPc3	on
mohlo	moct	k5eAaImAgNnS	moct
město	město	k1gNnSc1	město
poskytnout	poskytnout	k5eAaPmF	poskytnout
pro	pro	k7c4	pro
vedení	vedení	k1gNnSc4	vedení
kampaně	kampaň	k1gFnSc2	kampaň
proti	proti	k7c3	proti
královně	královna	k1gFnSc3	královna
Žofii	Žofie	k1gFnSc4	Žofie
a	a	k8xC	a
královským	královský	k2eAgMnPc3d1	královský
stoupencům	stoupenec	k1gMnPc3	stoupenec
<g/>
,	,	kIx,	,
nechtějíce	chtít	k5eNaImSgFnP	chtít
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
nijak	nijak	k6eAd1	nijak
ujednati	ujednat	k5eAaPmF	ujednat
příměří	příměří	k1gNnSc4	příměří
jakožto	jakožto	k8xS	jakožto
s	s	k7c7	s
nepřáteli	nepřítel	k1gMnPc7	nepřítel
Boha	bůh	k1gMnSc2	bůh
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
Žižka	Žižka	k1gMnSc1	Žižka
strávil	strávit	k5eAaPmAgMnS	strávit
pět	pět	k4xCc4	pět
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
zde	zde	k6eAd1	zde
připadla	připadnout	k5eAaPmAgFnS	připadnout
neformální	formální	k2eNgFnSc1d1	neformální
role	role	k1gFnSc1	role
autoritativního	autoritativní	k2eAgMnSc2d1	autoritativní
vojenského	vojenský	k2eAgMnSc2d1	vojenský
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
pobytu	pobyt	k1gInSc2	pobyt
se	se	k3xPyFc4	se
po	po	k7c6	po
boku	bok	k1gInSc6	bok
svých	svůj	k3xOyFgMnPc2	svůj
společníků	společník	k1gMnPc2	společník
a	a	k8xC	a
místních	místní	k2eAgMnPc2d1	místní
husitů	husita	k1gMnPc2	husita
účastnil	účastnit	k5eAaImAgInS	účastnit
vypuzení	vypuzení	k1gNnSc4	vypuzení
části	část	k1gFnSc2	část
katolického	katolický	k2eAgNnSc2d1	katolické
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dnech	den	k1gInPc6	den
následujících	následující	k2eAgInPc6d1	následující
pak	pak	k6eAd1	pak
podnikal	podnikat	k5eAaImAgMnS	podnikat
výpady	výpad	k1gInPc7	výpad
proti	proti	k7c3	proti
drobným	drobný	k2eAgFnPc3d1	drobná
základnám	základna	k1gFnPc3	základna
nepřítele	nepřítel	k1gMnSc2	nepřítel
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
namířena	namířit	k5eAaPmNgFnS	namířit
proti	proti	k7c3	proti
tvrzi	tvrz	k1gFnSc3	tvrz
Nekmíř	Nekmíř	k1gFnSc1	Nekmíř
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
tři	tři	k4xCgFnPc1	tři
stovky	stovka	k1gFnPc1	stovka
jeho	jeho	k3xOp3gMnPc2	jeho
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
sedmi	sedm	k4xCc2	sedm
vozů	vůz	k1gInPc2	vůz
přepadeny	přepaden	k2eAgFnPc1d1	přepadena
jízdou	jízda	k1gFnSc7	jízda
a	a	k8xC	a
pěchotou	pěchota	k1gFnSc7	pěchota
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
landfrýdu	landfrýd	k1gInSc2	landfrýd
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
<g/>
.	.	kIx.	.
</s>
<s>
Střetnutí	střetnutí	k1gNnSc1	střetnutí
u	u	k7c2	u
Nekmíře	Nekmíř	k1gInSc2	Nekmíř
skýtalo	skýtat	k5eAaImAgNnS	skýtat
podobný	podobný	k2eAgInSc4d1	podobný
obrázek	obrázek	k1gInSc4	obrázek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
opakoval	opakovat	k5eAaImAgInS	opakovat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
Žižkových	Žižkových	k2eAgFnPc6d1	Žižkových
bitvách	bitva	k1gFnPc6	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
srazili	srazit	k5eAaPmAgMnP	srazit
vozy	vůz	k1gInPc4	vůz
do	do	k7c2	do
hradby	hradba	k1gFnSc2	hradba
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
odrazili	odrazit	k5eAaPmAgMnP	odrazit
početně	početně	k6eAd1	početně
mnohem	mnohem	k6eAd1	mnohem
silnějšího	silný	k2eAgNnSc2d2	silnější
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
vyzbrojeného	vyzbrojený	k2eAgMnSc4d1	vyzbrojený
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Starých	Starých	k2eAgInPc2d1	Starých
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgInPc2d1	český
trocnovský	trocnovský	k2eAgMnSc1d1	trocnovský
zeman	zeman	k1gMnSc1	zeman
sebevědomě	sebevědomě	k6eAd1	sebevědomě
využil	využít	k5eAaPmAgMnS	využít
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
táhl	táhnout	k5eAaImAgMnS	táhnout
dál	daleko	k6eAd2	daleko
svou	svůj	k3xOyFgFnSc7	svůj
cestou	cesta	k1gFnSc7	cesta
a	a	k8xC	a
tu	ten	k3xDgFnSc4	ten
noc	noc	k1gFnSc4	noc
pobořil	pobořit	k5eAaPmAgMnS	pobořit
tři	tři	k4xCgFnPc4	tři
opevněné	opevněný	k2eAgFnPc4d1	opevněná
tvrze	tvrz	k1gFnPc4	tvrz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rok	rok	k1gInSc1	rok
1420	[number]	k4	1420
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Nekmíře	Nekmíř	k1gMnSc2	Nekmíř
muže	muž	k1gMnSc2	muž
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
landfrýdu	landfrýd	k1gInSc2	landfrýd
posílily	posílit	k5eAaPmAgInP	posílit
oddíly	oddíl	k1gInPc1	oddíl
Václava	Václav	k1gMnSc2	Václav
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
,	,	kIx,	,
vojenského	vojenský	k2eAgMnSc2d1	vojenský
náměstka	náměstek	k1gMnSc2	náměstek
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
královská	královský	k2eAgNnPc1d1	královské
vojska	vojsko	k1gNnPc1	vojsko
posléze	posléze	k6eAd1	posléze
Plzeň	Plzeň	k1gFnSc1	Plzeň
oblehla	oblehnout	k5eAaPmAgFnS	oblehnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dalších	další	k2eAgInPc2d1	další
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
krvavým	krvavý	k2eAgFnPc3d1	krvavá
srážkám	srážka	k1gFnPc3	srážka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nepřinesly	přinést	k5eNaPmAgFnP	přinést
výhodu	výhoda	k1gFnSc4	výhoda
ani	ani	k8xC	ani
jedné	jeden	k4xCgFnSc3	jeden
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
octli	octnout	k5eAaPmAgMnP	octnout
ve	v	k7c6	v
zjevné	zjevný	k2eAgFnSc6d1	zjevná
nevýhodě	nevýhoda	k1gFnSc6	nevýhoda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
příslušníci	příslušník	k1gMnPc1	příslušník
místní	místní	k2eAgFnSc2d1	místní
katolické	katolický	k2eAgFnSc2d1	katolická
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
opozice	opozice	k1gFnSc2	opozice
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
začali	začít	k5eAaPmAgMnP	začít
opevňovat	opevňovat	k5eAaImF	opevňovat
své	svůj	k3xOyFgInPc4	svůj
domy	dům	k1gInPc4	dům
a	a	k8xC	a
město	město	k1gNnSc4	město
již	již	k6eAd1	již
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
března	březno	k1gNnSc2	březno
svíralo	svírat	k5eAaImAgNnS	svírat
několik	několik	k4yIc1	několik
narychlo	narychlo	k6eAd1	narychlo
vybudovaných	vybudovaný	k2eAgFnPc2d1	vybudovaná
bašt	bašta	k1gFnPc2	bašta
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
okolností	okolnost	k1gFnPc2	okolnost
nakonec	nakonec	k6eAd1	nakonec
přistoupili	přistoupit	k5eAaPmAgMnP	přistoupit
na	na	k7c4	na
návrhy	návrh	k1gInPc4	návrh
pražanů	pražan	k1gMnPc2	pražan
a	a	k8xC	a
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
s	s	k7c7	s
královskými	královský	k2eAgMnPc7d1	královský
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
zaručit	zaručit	k5eAaPmF	zaručit
obyvatelům	obyvatel	k1gMnPc3	obyvatel
svobodu	svoboda	k1gFnSc4	svoboda
přijímání	přijímání	k1gNnPc2	přijímání
z	z	k7c2	z
kalicha	kalich	k1gInSc2	kalich
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
volného	volný	k2eAgInSc2d1	volný
odchodu	odchod	k1gInSc2	odchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejspíš	nejspíš	k9	nejspíš
23.	[number]	k4	23.
března	březen	k1gInSc2	březen
vyrazily	vyrazit	k5eAaPmAgFnP	vyrazit
zhruba	zhruba	k6eAd1	zhruba
čtyři	čtyři	k4xCgFnPc4	čtyři
stovky	stovka	k1gFnPc1	stovka
pěších	pěší	k1gMnPc2	pěší
obojího	obojí	k4xRgNnSc2	obojí
pohlaví	pohlaví	k1gNnSc2	pohlaví
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
dvanácti	dvanáct	k4xCc2	dvanáct
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
devíti	devět	k4xCc2	devět
jezdců	jezdec	k1gInPc2	jezdec
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
vznikajícímu	vznikající	k2eAgNnSc3d1	vznikající
Hradišti	Hradiště	k1gNnSc3	Hradiště
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Tábor	Tábor	k1gInSc1	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25.	[number]	k4	25.
března	březen	k1gInSc2	březen
byla	být	k5eAaImAgFnS	být
kolona	kolona	k1gFnSc1	kolona
vedená	vedený	k2eAgFnSc1d1	vedená
Břeňkem	Břeněk	k1gInSc7	Břeněk
Švihovským	Švihovský	k2eAgMnSc7d1	Švihovský
z	z	k7c2	z
Dolan	Dolany	k1gInPc2	Dolany
<g/>
,	,	kIx,	,
Valkounem	Valkoun	k1gInSc7	Valkoun
z	z	k7c2	z
Adlaru	Adlar	k1gInSc2	Adlar
a	a	k8xC	a
Janem	Jan	k1gMnSc7	Jan
Žižkou	Žižka	k1gMnSc7	Žižka
napadena	napadnout	k5eAaPmNgFnS	napadnout
silnými	silný	k2eAgInPc7d1	silný
oddíly	oddíl	k1gInPc7	oddíl
nepřátel	nepřítel	k1gMnPc2	nepřítel
operujícími	operující	k2eAgFnPc7d1	operující
v	v	k7c6	v
píseckém	písecký	k2eAgInSc6d1	písecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
taktického	taktický	k2eAgMnSc2d1	taktický
velitele	velitel	k1gMnSc2	velitel
při	při	k7c6	při
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Sudoměře	Sudoměř	k1gFnSc2	Sudoměř
zřejmě	zřejmě	k6eAd1	zřejmě
zastával	zastávat	k5eAaImAgMnS	zastávat
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
za	za	k7c4	za
využití	využití	k1gNnSc4	využití
vozové	vozový	k2eAgFnSc2d1	vozová
hradby	hradba	k1gFnSc2	hradba
a	a	k8xC	a
díky	díky	k7c3	díky
příhodným	příhodný	k2eAgFnPc3d1	příhodná
terénním	terénní	k2eAgFnPc3d1	terénní
podmínkám	podmínka	k1gFnPc3	podmínka
podařilo	podařit	k5eAaPmAgNnS	podařit
úspěšně	úspěšně	k6eAd1	úspěšně
odrazit	odrazit	k5eAaPmF	odrazit
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
nepřátelských	přátelský	k2eNgMnPc2d1	nepřátelský
ozbrojenců	ozbrojenec	k1gMnPc2	ozbrojenec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vítězství	vítězství	k1gNnSc6	vítězství
husité	husita	k1gMnPc1	husita
přenocovali	přenocovat	k5eAaPmAgMnP	přenocovat
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
bez	bez	k7c2	bez
dalšího	další	k2eAgNnSc2d1	další
atakování	atakování	k1gNnSc2	atakování
protivníkem	protivník	k1gMnSc7	protivník
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
k	k	k7c3	k
vytčenému	vytčený	k2eAgInSc3d1	vytčený
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Žižkovým	Žižkův	k2eAgInSc7d1	Žižkův
příchodem	příchod	k1gInSc7	příchod
na	na	k7c4	na
Tábor	Tábor	k1gInSc4	Tábor
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
dokonána	dokonat	k5eAaPmNgFnS	dokonat
první	první	k4xOgFnSc1	první
etapa	etapa	k1gFnSc1	etapa
sjednocení	sjednocení	k1gNnSc2	sjednocení
husitských	husitský	k2eAgFnPc2d1	husitská
sil	síla	k1gFnPc2	síla
z	z	k7c2	z
Jižních	jižní	k2eAgFnPc2d1	jižní
a	a	k8xC	a
Západních	západní	k2eAgFnPc2d1	západní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
<g/>
Táborská	táborský	k2eAgFnSc1d1	táborská
městská	městský	k2eAgFnSc1d1	městská
obec	obec	k1gFnSc1	obec
si	se	k3xPyFc3	se
již	již	k6eAd1	již
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
čela	čelo	k1gNnSc2	čelo
zvolila	zvolit	k5eAaPmAgFnS	zvolit
čtyři	čtyři	k4xCgMnPc4	čtyři
hejtmany	hejtman	k1gMnPc4	hejtman
<g/>
.	.	kIx.	.
</s>
<s>
Čelné	čelný	k2eAgNnSc4d1	čelné
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
z	z	k7c2	z
Husi	Hus	k1gFnSc2	Hus
<g/>
,	,	kIx,	,
dalšími	další	k1gNnPc7	další
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
z	z	k7c2	z
Buchova	Buchův	k2eAgNnSc2d1	Buchův
<g/>
,	,	kIx,	,
Chval	chválit	k5eAaImRp2nS	chválit
z	z	k7c2	z
Řepice	řepice	k1gFnSc2	řepice
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podle	podle	k7c2	podle
všech	všecek	k3xTgMnPc2	všecek
náznaků	náznak	k1gInPc2	náznak
zřejmě	zřejmě	k6eAd1	zřejmě
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
dní	den	k1gInPc2	den
pobytu	pobyt	k1gInSc2	pobyt
zastával	zastávat	k5eAaImAgMnS	zastávat
post	post	k1gInSc4	post
vojenského	vojenský	k2eAgMnSc2d1	vojenský
velitele	velitel	k1gMnSc2	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc1	závěr
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
akce	akce	k1gFnSc1	akce
z	z	k7c2	z
5.	[number]	k4	5.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
brzkých	brzký	k2eAgFnPc6d1	brzká
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
překvapivě	překvapivě	k6eAd1	překvapivě
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
na	na	k7c4	na
vojenský	vojenský	k2eAgInSc4d1	vojenský
tábor	tábor	k1gInSc4	tábor
u	u	k7c2	u
tvrze	tvrz	k1gFnSc2	tvrz
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Vožici	Vožice	k1gFnSc6	Vožice
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Mladé	mladý	k2eAgFnSc2d1	mladá
Vožice	Vožice	k1gFnSc2	Vožice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
střetnutí	střetnutí	k1gNnSc6	střetnutí
se	se	k3xPyFc4	se
ozbrojencům	ozbrojenec	k1gMnPc3	ozbrojenec
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInPc7	jeho
velením	velení	k1gNnSc7	velení
podařilo	podařit	k5eAaPmAgNnS	podařit
pobít	pobít	k5eAaPmF	pobít
množství	množství	k1gNnSc1	množství
železných	železný	k2eAgMnPc2d1	železný
pánů	pan	k1gMnPc2	pan
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
bohatou	bohatý	k2eAgFnSc4d1	bohatá
kořist	kořist	k1gFnSc4	kořist
jak	jak	k8xS	jak
v	v	k7c6	v
drahých	drahý	k2eAgInPc6d1	drahý
kovech	kov	k1gInPc6	kov
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
zbraních	zbraň	k1gFnPc6	zbraň
a	a	k8xC	a
koních	kůň	k1gMnPc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tito	tento	k3xDgMnPc1	tento
koně	kůň	k1gMnPc1	kůň
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
tvořit	tvořit	k5eAaImF	tvořit
základ	základ	k1gInSc1	základ
husitské	husitský	k2eAgFnSc2d1	husitská
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
posléze	posléze	k6eAd1	posléze
popisuje	popisovat	k5eAaImIp3nS	popisovat
řadu	řada	k1gFnSc4	řada
vojenských	vojenský	k2eAgInPc2d1	vojenský
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
husité	husita	k1gMnPc1	husita
pod	pod	k7c7	pod
Žižkovým	Žižkův	k2eAgNnSc7d1	Žižkovo
velením	velení	k1gNnSc7	velení
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
od	od	k7c2	od
23.	[number]	k4	23.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
nadmíru	nadmíru	k6eAd1	nadmíru
odvážný	odvážný	k2eAgInSc1d1	odvážný
a	a	k8xC	a
statečný	statečný	k2eAgMnSc1d1	statečný
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
řízením	řízení	k1gNnSc7	řízení
se	se	k3xPyFc4	se
hotovilo	hotovit	k5eAaImAgNnS	hotovit
všechno	všechen	k3xTgNnSc1	všechen
vojsko	vojsko	k1gNnSc1	vojsko
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
šli	jít	k5eAaImAgMnP	jít
všichni	všechen	k3xTgMnPc1	všechen
sedláci	sedlák	k1gMnPc1	sedlák
<g/>
,	,	kIx,	,
třebas	třebas	k8wRxS	třebas
bez	bez	k7c2	bez
zbroje	zbroj	k1gFnSc2	zbroj
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
s	s	k7c7	s
cepy	cep	k1gInPc7	cep
<g/>
,	,	kIx,	,
palicemi	palice	k1gFnPc7	palice
<g/>
,	,	kIx,	,
samostříly	samostříl	k1gInPc7	samostříl
a	a	k8xC	a
sudlicemi	sudlice	k1gFnPc7	sudlice
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
ochotně	ochotně	k6eAd1	ochotně
ho	on	k3xPp3gMnSc4	on
poslouchali	poslouchat	k5eAaImAgMnP	poslouchat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
1.	[number]	k4	1.
března	březen	k1gInSc2	březen
1420	[number]	k4	1420
vydal	vydat	k5eAaPmAgMnS	vydat
papež	papež	k1gMnSc1	papež
Martin	Martin	k1gMnSc1	Martin
V.	V.	kA	V.
bulu	bula	k1gFnSc4	bula
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
proti	proti	k7c3	proti
kacířským	kacířský	k2eAgMnPc3d1	kacířský
Čechům	Čech	k1gMnPc3	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Narychlo	narychlo	k6eAd1	narychlo
sebrané	sebraný	k2eAgNnSc1d1	sebrané
vojsko	vojsko	k1gNnSc1	vojsko
brzy	brzy	k6eAd1	brzy
vpadlo	vpadnout	k5eAaPmAgNnS	vpadnout
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
3.	[number]	k4	3.
května	květen	k1gInSc2	květen
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
Hradec	Hradec	k1gInSc4	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
pak	pak	k6eAd1	pak
i	i	k9	i
Kutnou	kutný	k2eAgFnSc4d1	Kutná
Horu	hora	k1gFnSc4	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhaly	probíhat	k5eAaImAgFnP	probíhat
přípravy	příprava	k1gFnSc2	příprava
k	k	k7c3	k
tažení	tažení	k1gNnSc3	tažení
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ohrožení	ohrožený	k2eAgMnPc1d1	ohrožený
pražané	pražan	k1gMnPc1	pražan
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
s	s	k7c7	s
králem	král	k1gMnSc7	král
Zikmundem	Zikmund	k1gMnSc7	Zikmund
vést	vést	k5eAaImF	vést
jednání	jednání	k1gNnPc1	jednání
o	o	k7c6	o
kompromisu	kompromis	k1gInSc6	kompromis
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jím	jíst	k5eAaImIp1nS	jíst
vznesené	vznesený	k2eAgFnPc4d1	vznesená
podmínky	podmínka	k1gFnPc4	podmínka
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
české	český	k2eAgFnSc2d1	Česká
metropole	metropol	k1gFnSc2	metropol
nepřijatelné	přijatelný	k2eNgNnSc1d1	nepřijatelné
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
táborskou	táborský	k2eAgFnSc4d1	táborská
obec	obec	k1gFnSc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Vyčleněný	vyčleněný	k2eAgInSc1d1	vyčleněný
oddíl	oddíl	k1gInSc1	oddíl
vyrazil	vyrazit	k5eAaPmAgInS	vyrazit
z	z	k7c2	z
Hradiště	Hradiště	k1gNnSc2	Hradiště
18.	[number]	k4	18.
května	květen	k1gInSc2	květen
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vítán	vítat	k5eAaImNgMnS	vítat
procesím	procesí	k1gNnSc7	procesí
Pražanů	Pražan	k1gMnPc2	Pražan
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Trasou	trasa	k1gFnSc7	trasa
táborské	táborský	k2eAgNnSc1d1	táborské
vojsko	vojsko	k1gNnSc1	vojsko
vypálilo	vypálit	k5eAaPmAgNnS	vypálit
Benešov	Benešov	k1gInSc4	Benešov
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
nuceno	nucen	k2eAgNnSc1d1	nuceno
otevřít	otevřít	k5eAaPmF	otevřít
si	se	k3xPyFc3	se
cestu	cesta	k1gFnSc4	cesta
z	z	k7c2	z
obklíčení	obklíčení	k1gNnSc2	obklíčení
skrze	skrze	k?	skrze
jednotky	jednotka	k1gFnSc2	jednotka
českých	český	k2eAgMnPc2d1	český
katolických	katolický	k2eAgMnPc2d1	katolický
pánů	pan	k1gMnPc2	pan
a	a	k8xC	a
oddíly	oddíl	k1gInPc1	oddíl
křižáckého	křižácký	k2eAgNnSc2d1	křižácké
vojska	vojsko	k1gNnSc2	vojsko
ve	v	k7c6	v
střetnutí	střetnutí	k1gNnSc6	střetnutí
u	u	k7c2	u
Poříčí	Poříčí	k1gNnSc2	Poříčí
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Takticko	Takticko	k6eAd1	Takticko
<g/>
–	–	k?	–
<g/>
operační	operační	k2eAgNnSc4d1	operační
zvládnutí	zvládnutí	k1gNnSc4	zvládnutí
přesunu	přesunout	k5eAaPmIp1nS	přesunout
i	i	k9	i
defenzivní	defenzivní	k2eAgFnSc1d1	defenzivní
taktika	taktika	k1gFnSc1	taktika
vedení	vedení	k1gNnSc2	vedení
boje	boj	k1gInSc2	boj
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrchní	vrchní	k2eAgNnSc1d1	vrchní
velení	velení	k1gNnSc1	velení
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Janu	Jana	k1gFnSc4	Jana
Žižkovi	Žižka	k1gMnSc3	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
akcemi	akce	k1gFnPc7	akce
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
zmiňováno	zmiňovat	k5eAaImNgNnS	zmiňovat
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
připravoval	připravovat	k5eAaImAgMnS	připravovat
zahájit	zahájit	k5eAaPmF	zahájit
bitvu	bitva	k1gFnSc4	bitva
a	a	k8xC	a
obsadit	obsadit	k5eAaPmF	obsadit
vrch	vrch	k1gInSc4	vrch
Vítkov	Vítkov	k1gInSc1	Vítkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
bitvou	bitva	k1gFnSc7	bitva
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
a	a	k8xC	a
bojem	boj	k1gInSc7	boj
o	o	k7c4	o
sruby	srub	k1gInPc4	srub
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
dále	daleko	k6eAd2	daleko
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
i	i	k8xC	i
Žižka	Žižka	k1gMnSc1	Žižka
přišed	přijít	k5eAaPmDgInS	přijít
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgInS	být
by	by	kYmCp3nS	by
sám	sám	k3xTgInSc1	sám
zabit	zabit	k2eAgInSc1d1	zabit
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
ho	on	k3xPp3gNnSc4	on
nebyli	být	k5eNaImAgMnP	být
jeho	jeho	k3xOp3gMnPc1	jeho
lidé	člověk	k1gMnPc1	člověk
cepy	cep	k1gInPc4	cep
vyrvali	vyrvat	k5eAaPmAgMnP	vyrvat
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kronika	kronika	k1gFnSc1	kronika
velmi	velmi	k6eAd1	velmi
pěkná	pěkný	k2eAgFnSc1d1	pěkná
o	o	k7c4	o
Janu	Jana	k1gFnSc4	Jana
Žižkovi	Žižka	k1gMnSc3	Žižka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
Žižkovi	Žižka	k1gMnSc3	Žižka
velmi	velmi	k6eAd1	velmi
těžko	těžko	k6eAd1	těžko
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
s	s	k7c7	s
božím	boží	k2eAgNnSc7d1	boží
tělem	tělo	k1gNnSc7	tělo
pojide	pojid	k1gInSc5	pojid
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
jsů	jsů	k?	jsů
strachem	strach	k1gInSc7	strach
hlavy	hlava	k1gFnSc2	hlava
lámali	lámat	k5eAaImAgMnP	lámat
i	i	k9	i
s	s	k7c7	s
koňmi	kůň	k1gMnPc7	kůň
utiekajíc	utiekat	k5eAaImSgFnS	utiekat
skákali	skákat	k5eAaImAgMnP	skákat
s	s	k7c7	s
huory	huor	k1gMnPc7	huor
doluv	doluva	k1gFnPc2	doluva
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
po	po	k7c6	po
husitském	husitský	k2eAgNnSc6d1	husitské
vítězství	vítězství	k1gNnSc6	vítězství
pak	pak	k6eAd1	pak
prý	prý	k9	prý
hejtman	hejtman	k1gMnSc1	hejtman
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
svolav	svolat	k5eAaPmDgInS	svolat
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
množství	množství	k1gNnSc2	množství
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
dívek	dívka	k1gFnPc2	dívka
a	a	k8xC	a
lidu	lid	k1gInSc2	lid
světského	světský	k2eAgInSc2d1	světský
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
ohradil	ohradit	k5eAaPmAgMnS	ohradit
tvrz	tvrz	k1gFnSc4	tvrz
častěji	často	k6eAd2	často
jmenovanou	jmenovaná	k1gFnSc7	jmenovaná
četnými	četný	k2eAgInPc7d1	četný
většími	veliký	k2eAgInPc7d2	veliký
a	a	k8xC	a
hlubšími	hluboký	k2eAgInPc7d2	hlubší
příkopy	příkop	k1gInPc7	příkop
<g/>
,	,	kIx,	,
zdmi	zeď	k1gFnPc7	zeď
a	a	k8xC	a
četnými	četný	k2eAgInPc7d1	četný
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
sruby	srub	k1gInPc7	srub
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
lze	lze	k6eAd1	lze
vskutku	vskutku	k9	vskutku
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
tomu	ten	k3xDgInSc3	ten
vrchu	vrch	k1gInSc3	vrch
a	a	k8xC	a
místu	místo	k1gNnSc3	místo
někteří	některý	k3yIgMnPc1	některý
dali	dát	k5eAaPmAgMnP	dát
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
zakladatelova	zakladatelův	k2eAgNnSc2d1	zakladatelovo
<g/>
...	...	k?	...
<g/>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
a	a	k8xC	a
ústupu	ústup	k1gInSc2	ústup
jejích	její	k3xOp3gMnPc2	její
účastníků	účastník	k1gMnPc2	účastník
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Prahy	Praha	k1gFnSc2	Praha
získalo	získat	k5eAaPmAgNnS	získat
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
doposud	doposud	k6eAd1	doposud
spíš	spíš	k9	spíš
jen	jen	k6eAd1	jen
pověstné	pověstný	k2eAgMnPc4d1	pověstný
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
věhlasu	věhlas	k1gInSc2	věhlas
a	a	k8xC	a
autority	autorita	k1gFnSc2	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Dochované	dochovaný	k2eAgInPc1d1	dochovaný
prameny	pramen	k1gInPc1	pramen
nehovoří	hovořit	k5eNaImIp3nP	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
úlohu	úloha	k1gFnSc4	úloha
trocnovský	trocnovský	k2eAgMnSc1d1	trocnovský
zeman	zeman	k1gMnSc1	zeman
sehrál	sehrát	k5eAaPmAgMnS	sehrát
v	v	k7c6	v
následných	následný	k2eAgFnPc6d1	následná
neshodách	neshoda	k1gFnPc6	neshoda
Tábora	tábor	k1gMnSc2	tábor
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
či	či	k8xC	či
jaká	jaký	k3yRgFnSc1	jaký
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
kampani	kampaň	k1gFnSc6	kampaň
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
ničení	ničení	k1gNnSc4	ničení
klášterů	klášter	k1gInPc2	klášter
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
české	český	k2eAgFnSc2d1	Česká
metropole	metropol	k1gFnSc2	metropol
a	a	k8xC	a
pobíjení	pobíjení	k1gNnSc2	pobíjení
místních	místní	k2eAgMnPc2d1	místní
kněží	kněz	k1gMnPc2	kněz
a	a	k8xC	a
mnichů	mnich	k1gMnPc2	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
zmiňováno	zmiňován	k2eAgNnSc1d1	zmiňováno
teprve	teprve	k6eAd1	teprve
počátkem	počátkem	k7c2	počátkem
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jménem	jméno	k1gNnSc7	jméno
Táborů	tábor	k1gMnPc2	tábor
připojil	připojit	k5eAaPmAgInS	připojit
svou	svůj	k3xOyFgFnSc4	svůj
pečeť	pečeť	k1gFnSc4	pečeť
k	k	k7c3	k
plné	plný	k2eAgFnSc3d1	plná
moci	moc	k1gFnSc3	moc
husitů	husita	k1gMnPc2	husita
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vyslali	vyslat	k5eAaPmAgMnP	vyslat
poselstvo	poselstvo	k1gNnSc4	poselstvo
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
<g/>
-li	i	k?	-li
nabídnout	nabídnout	k5eAaPmF	nabídnout
českou	český	k2eAgFnSc4d1	Česká
korunu	koruna	k1gFnSc4	koruna
jeho	jeho	k3xOp3gMnSc3	jeho
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
.	.	kIx.	.
22.	[number]	k4	22.
srpna	srpen	k1gInSc2	srpen
pak	pak	k6eAd1	pak
opustil	opustit	k5eAaPmAgMnS	opustit
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
zamířil	zamířit	k5eAaPmAgMnS	zamířit
do	do	k7c2	do
Jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
značné	značný	k2eAgFnPc4d1	značná
ztráty	ztráta	k1gFnPc4	ztráta
na	na	k7c6	na
světských	světský	k2eAgInPc6d1	světský
statcích	statek	k1gInPc6	statek
<g/>
,	,	kIx,	,
tak	tak	k9	tak
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c4	na
panství	panství	k1gNnSc4	panství
pana	pan	k1gMnSc2	pan
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
výpadu	výpad	k1gInSc6	výpad
osobně	osobně	k6eAd1	osobně
velel	velet	k5eAaImAgInS	velet
dobytí	dobytí	k1gNnSc4	dobytí
města	město	k1gNnSc2	město
Vodňany	Vodňan	k1gMnPc4	Vodňan
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Vavákem	Vavák	k1gMnSc7	Vavák
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
získal	získat	k5eAaPmAgMnS	získat
Lomnici	Lomnice	k1gFnSc4	Lomnice
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
zdevastoval	zdevastovat	k5eAaPmAgInS	zdevastovat
Novou	nový	k2eAgFnSc4d1	nová
Bystřici	Bystřice	k1gFnSc4	Bystřice
a	a	k8xC	a
uštědřil	uštědřit	k5eAaPmAgMnS	uštědřit
pánům	pan	k1gMnPc3	pan
z	z	k7c2	z
plzeňského	plzeňské	k1gNnSc2	plzeňské
kraje	kraj	k1gInSc2	kraj
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Oldřichem	Oldřich	k1gMnSc7	Oldřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Panského	panský	k2eAgInSc2d1	panský
Boru	bor	k1gInSc2	bor
<g/>
.	.	kIx.	.
12.	[number]	k4	12.
listopadu	listopad	k1gInSc2	listopad
pak	pak	k6eAd1	pak
dobyl	dobýt	k5eAaPmAgInS	dobýt
Prachatice	Prachatice	k1gFnPc4	Prachatice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
povraždil	povraždit	k5eAaPmAgMnS	povraždit
všechny	všechen	k3xTgMnPc4	všechen
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
ženy	žena	k1gFnPc4	žena
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
dal	dát	k5eAaPmAgMnS	dát
vyhnat	vyhnat	k5eAaPmF	vyhnat
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
18.	[number]	k4	18.
listopadu	listopad	k1gInSc2	listopad
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
příměří	příměří	k1gNnSc4	příměří
s	s	k7c7	s
Oldřichem	Oldřich	k1gMnSc7	Oldřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ukončil	ukončit	k5eAaPmAgMnS	ukončit
první	první	k4xOgFnSc4	první
fázi	fáze	k1gFnSc4	fáze
táborské	táborský	k2eAgFnSc2d1	táborská
podzimní	podzimní	k2eAgFnSc2d1	podzimní
ofenzivy	ofenziva	k1gFnSc2	ofenziva
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
se	se	k3xPyFc4	se
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
vojsko	vojsko	k1gNnSc1	vojsko
vydalo	vydat	k5eAaPmAgNnS	vydat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
Říčanech	Říčany	k1gInPc6	Říčany
se	se	k3xPyFc4	se
spojilo	spojit	k5eAaPmAgNnS	spojit
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
táborskými	táborský	k2eAgInPc7d1	táborský
odřady	odřad	k1gInPc7	odřad
a	a	k8xC	a
vyčkalo	vyčkat	k5eAaPmAgNnS	vyčkat
kapitulace	kapitulace	k1gFnSc2	kapitulace
pevnosti	pevnost	k1gFnSc2	pevnost
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
posádka	posádka	k1gFnSc1	posádka
otevřela	otevřít	k5eAaPmAgFnS	otevřít
brány	brána	k1gFnPc4	brána
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
vyvést	vyvést	k5eAaPmF	vyvést
devět	devět	k4xCc4	devět
kněží	kněz	k1gMnPc2	kněz
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
svým	svůj	k3xOyFgNnPc3	svůj
práčatům	práče	k1gNnPc3	práče
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gInPc4	on
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
dvěma	dva	k4xCgMnPc7	dva
dříve	dříve	k6eAd2	dříve
zajatými	zajatý	k2eAgInPc7d1	zajatý
duchovními	duchovní	k2eAgInPc7d1	duchovní
upálily	upálit	k5eAaPmAgFnP	upálit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rok	rok	k1gInSc1	rok
1421	[number]	k4	1421
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
Novém	nový	k2eAgInSc6d1	nový
roce	rok	k1gInSc6	rok
táborité	táborita	k1gMnPc1	táborita
pod	pod	k7c7	pod
Žižkovým	Žižkův	k2eAgNnSc7d1	Žižkovo
velením	velení	k1gNnSc7	velení
obsadili	obsadit	k5eAaPmAgMnP	obsadit
kláštery	klášter	k1gInPc4	klášter
v	v	k7c6	v
Kladrubech	Kladruby	k1gInPc6	Kladruby
a	a	k8xC	a
Chotěšově	Chotěšův	k2eAgFnSc6d1	Chotěšova
a	a	k8xC	a
dobyli	dobýt	k5eAaPmAgMnP	dobýt
hrad	hrad	k1gInSc4	hrad
Krasíkov	Krasíkov	k1gInSc4	Krasíkov
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgMnSc6	jenž
zajali	zajmout	k5eAaPmAgMnP	zajmout
významného	významný	k2eAgMnSc4d1	významný
nepřítele	nepřítel	k1gMnSc4	nepřítel
husitství	husitství	k1gNnSc2	husitství
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
<g/>
.	.	kIx.	.
2.	[number]	k4	2.
února	únor	k1gInSc2	únor
přitáhli	přitáhnout	k5eAaPmAgMnP	přitáhnout
k	k	k7c3	k
Tachovu	Tachov	k1gInSc3	Tachov
a	a	k8xC	a
vypálili	vypálit	k5eAaPmAgMnP	vypálit
jeho	jeho	k3xOp3gNnSc4	jeho
předměstí	předměstí	k1gNnSc4	předměstí
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
koncentraci	koncentrace	k1gFnSc3	koncentrace
vojsk	vojsko	k1gNnPc2	vojsko
českého	český	k2eAgMnSc4d1	český
a	a	k8xC	a
uherského	uherský	k2eAgMnSc4d1	uherský
krále	král	k1gMnSc4	král
nepřikročili	přikročit	k5eNaPmAgMnP	přikročit
ke	k	k7c3	k
generálnímu	generální	k2eAgInSc3d1	generální
útoku	útok	k1gInSc3	útok
a	a	k8xC	a
vrátili	vrátit	k5eAaPmAgMnP	vrátit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
posily	posila	k1gFnPc4	posila
k	k	k7c3	k
Táboru	Tábor	k1gInSc3	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Žižka	Žižka	k1gMnSc1	Žižka
nepokračuje	pokračovat	k5eNaImIp3nS	pokračovat
v	v	k7c6	v
ofenzivě	ofenziva	k1gFnSc6	ofenziva
<g/>
,	,	kIx,	,
neváhal	váhat	k5eNaImAgMnS	váhat
a	a	k8xC	a
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
silou	síla	k1gFnSc7	síla
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
klášter	klášter	k1gInSc4	klášter
v	v	k7c6	v
Kladrubech	Kladruby	k1gInPc6	Kladruby
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vypuzení	vypuzení	k1gNnSc6	vypuzení
husitské	husitský	k2eAgFnSc2d1	husitská
posádky	posádka	k1gFnSc2	posádka
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
pražané	pražan	k1gMnPc1	pražan
spojili	spojit	k5eAaPmAgMnP	spojit
svá	svůj	k3xOyFgNnPc4	svůj
vojska	vojsko	k1gNnPc4	vojsko
u	u	k7c2	u
Dobříše	Dobříš	k1gFnSc2	Dobříš
a	a	k8xC	a
sotva	sotva	k6eAd1	sotva
jejich	jejich	k3xOp3gInPc4	jejich
vozy	vůz	k1gInPc4	vůz
stály	stát	k5eAaImAgFnP	stát
pět	pět	k4xCc4	pět
mil	míle	k1gFnPc2	míle
od	od	k7c2	od
obležených	obležený	k2eAgMnPc2d1	obležený
<g/>
,	,	kIx,	,
Zikmund	Zikmund	k1gMnSc1	Zikmund
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
své	svůj	k3xOyFgInPc4	svůj
oddíly	oddíl	k1gInPc4	oddíl
a	a	k8xC	a
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
k	k	k7c3	k
Litoměřicím	Litoměřice	k1gInPc3	Litoměřice
<g/>
.	.	kIx.	.
14.	[number]	k4	14.
února	únor	k1gInSc2	únor
obě	dva	k4xCgNnPc1	dva
husitská	husitský	k2eAgNnPc1d1	husitské
vojska	vojsko	k1gNnPc1	vojsko
oblehla	oblehnout	k5eAaPmAgNnP	oblehnout
Plzeň	Plzeň	k1gFnSc4	Plzeň
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
stala	stát	k5eAaPmAgFnS	stát
hlavou	hlava	k1gFnSc7	hlava
landfrýdu	landfrýd	k1gInSc2	landfrýd
<g/>
,	,	kIx,	,
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Bojové	bojový	k2eAgFnPc1d1	bojová
operace	operace	k1gFnPc1	operace
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
měly	mít	k5eAaImAgInP	mít
město	město	k1gNnSc4	město
přinutit	přinutit	k5eAaPmF	přinutit
ke	k	k7c3	k
kapitulaci	kapitulace	k1gFnSc3	kapitulace
<g/>
,	,	kIx,	,
trvaly	trvat	k5eAaImAgInP	trvat
čtyři	čtyři	k4xCgInPc1	čtyři
týdny	týden	k1gInPc1	týden
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
uplynutí	uplynutí	k1gNnSc6	uplynutí
plzeňští	plzeňský	k2eAgMnPc1d1	plzeňský
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
příměří	příměří	k1gNnSc4	příměří
do	do	k7c2	do
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
zavazovali	zavazovat	k5eAaImAgMnP	zavazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
měsíce	měsíc	k1gInSc2	měsíc
přistoupí	přistoupit	k5eAaPmIp3nS	přistoupit
ke	k	k7c3	k
kalichu	kalich	k1gInSc3	kalich
a	a	k8xC	a
čtyřem	čtyři	k4xCgInPc3	čtyři
artikulům	artikul	k1gInPc3	artikul
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
skutečně	skutečně	k6eAd1	skutečně
ratifikována	ratifikován	k2eAgFnSc1d1	ratifikována
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
její	její	k3xOp3gInSc1	její
obsah	obsah	k1gInSc1	obsah
nebyl	být	k5eNaImAgInS	být
plzeňskými	plzeňský	k2eAgFnPc7d1	Plzeňská
nikdy	nikdy	k6eAd1	nikdy
dodržen	dodržet	k5eAaPmNgMnS	dodržet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
akci	akce	k1gFnSc6	akce
Žižka	Žižka	k1gMnSc1	Žižka
své	svůj	k3xOyFgInPc4	svůj
tábory	tábor	k1gInPc4	tábor
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
Chomutovu	Chomutov	k1gInSc3	Chomutov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c4	na
Květnou	květný	k2eAgFnSc4d1	Květná
neděli	neděle	k1gFnSc4	neděle
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
dobyl	dobýt	k5eAaPmAgInS	dobýt
útokem	útok	k1gInSc7	útok
<g/>
,	,	kIx,	,
vypálil	vypálit	k5eAaPmAgMnS	vypálit
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
vojáci	voják	k1gMnPc1	voják
zde	zde	k6eAd1	zde
zmasakrovali	zmasakrovat	k5eAaPmAgMnP	zmasakrovat
na	na	k7c4	na
2 400	[number]	k4	2 400
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
měl	mít	k5eAaImAgMnS	mít
po	po	k7c6	po
předešlém	předešlý	k2eAgNnSc6d1	předešlé
ujednání	ujednání	k1gNnSc6	ujednání
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
místním	místní	k2eAgNnSc7d1	místní
vojskem	vojsko	k1gNnSc7	vojsko
vést	vést	k5eAaImF	vést
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Beroun	Beroun	k1gInSc4	Beroun
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
padlo	padnout	k5eAaImAgNnS	padnout
1.	[number]	k4	1.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
trocnovský	trocnovský	k2eAgMnSc1d1	trocnovský
zeman	zeman	k1gMnSc1	zeman
zde	zde	k6eAd1	zde
nechal	nechat	k5eAaPmAgMnS	nechat
upálit	upálit	k5eAaPmF	upálit
přes	přes	k7c4	přes
čtyřicet	čtyřicet	k4xCc4	čtyřicet
duchovních	duchovní	k1gMnPc2	duchovní
včetně	včetně	k7c2	včetně
tří	tři	k4xCgMnPc2	tři
odpadlých	odpadlý	k2eAgMnPc2d1	odpadlý
mistrů	mistr	k1gMnPc2	mistr
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
zamířil	zamířit	k5eAaPmAgMnS	zamířit
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Táboru	Tábor	k1gInSc3	Tábor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vypořádal	vypořádat	k5eAaPmAgInS	vypořádat
s	s	k7c7	s
husitskými	husitský	k2eAgMnPc7d1	husitský
heretiky	heretik	k1gMnPc7	heretik
zvanými	zvaný	k2eAgFnPc7d1	zvaná
pikarti	pikart	k1gMnPc1	pikart
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
opevnili	opevnit	k5eAaPmAgMnP	opevnit
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Příběnice	Příběnika	k1gFnSc3	Příběnika
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Tábora	Tábor	k1gInSc2	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zajal	zajmout	k5eAaPmAgMnS	zajmout
na	na	k7c4	na
padesát	padesát	k4xCc4	padesát
příslušníků	příslušník	k1gMnPc2	příslušník
sekty	sekt	k1gInPc4	sekt
obojího	obojí	k4xRgNnSc2	obojí
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
je	být	k5eAaImIp3nS	být
upálit	upálit	k5eAaPmF	upálit
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Klokoty	klokot	k1gInPc4	klokot
<g/>
.	.	kIx.	.
26.	[number]	k4	26.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
Žižka	Žižka	k1gMnSc1	Žižka
opět	opět	k6eAd1	opět
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
pražany	pražan	k1gMnPc7	pražan
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
tažení	tažení	k1gNnSc6	tažení
k	k	k7c3	k
Chrudimi	Chrudim	k1gFnSc3	Chrudim
a	a	k8xC	a
během	během	k7c2	během
pochodu	pochod	k1gInSc2	pochod
prý	prý	k9	prý
spálil	spálit	k5eAaPmAgInS	spálit
přemnoho	přemnoho	k6eAd1	přemnoho
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
vojsko	vojsko	k1gNnSc1	vojsko
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
mnohá	mnohý	k2eAgNnPc4d1	mnohé
česká	český	k2eAgNnPc4d1	české
města	město	k1gNnPc4	město
<g/>
;	;	kIx,	;
včetně	včetně	k7c2	včetně
Chrudimi	Chrudim	k1gFnSc2	Chrudim
také	také	k6eAd1	také
Vysoké	vysoký	k2eAgNnSc4d1	vysoké
Mýto	mýto	k1gNnSc4	mýto
<g/>
,	,	kIx,	,
Poličku	Polička	k1gFnSc4	Polička
<g/>
,	,	kIx,	,
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
a	a	k8xC	a
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Žižka	Žižka	k1gMnSc1	Žižka
ukončil	ukončit	k5eAaPmAgMnS	ukončit
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
pražany	pražan	k1gMnPc7	pražan
a	a	k8xC	a
zamířil	zamířit	k5eAaPmAgMnS	zamířit
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
Trutnova	Trutnov	k1gInSc2	Trutnov
a	a	k8xC	a
Dvora	Dvůr	k1gInSc2	Dvůr
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
uhnul	uhnout	k5eAaPmAgMnS	uhnout
na	na	k7c4	na
Mladou	mladá	k1gFnSc4	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
připojil	připojit	k5eAaPmAgMnS	připojit
Jan	Jan	k1gMnSc1	Jan
Smiřický	smiřický	k2eAgMnSc1d1	smiřický
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
zamířili	zamířit	k5eAaPmAgMnP	zamířit
k	k	k7c3	k
Litoměřicím	Litoměřice	k1gInPc3	Litoměřice
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
obklíčili	obklíčit	k5eAaPmAgMnP	obklíčit
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Žižka	Žižka	k1gMnSc1	Žižka
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
hoře	hora	k1gFnSc6	hora
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
jednoho	jeden	k4xCgInSc2	jeden
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
srubu	srub	k1gInSc2	srub
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
dal	dát	k5eAaPmAgInS	dát
jméno	jméno	k1gNnSc4	jméno
Kalich	kalich	k1gInSc4	kalich
<g/>
...	...	k?	...
Toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
u	u	k7c2	u
Třebušína	Třebušín	k1gInSc2	Třebušín
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
přestavět	přestavět	k5eAaPmF	přestavět
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
sídlo	sídlo	k1gNnSc4	sídlo
a	a	k8xC	a
titulem	titul	k1gInSc7	titul
z	z	k7c2	z
Kalicha	kalich	k1gInSc2	kalich
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
predikát	predikát	k1gInSc4	predikát
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
formálně	formálně	k6eAd1	formálně
vzdaly	vzdát	k5eAaPmAgFnP	vzdát
pražanům	pražan	k1gMnPc3	pražan
<g/>
,	,	kIx,	,
tvrdě	tvrdě	k6eAd1	tvrdě
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
udeřil	udeřit	k5eAaPmAgMnS	udeřit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gMnPc1	jeho
muži	muž	k1gMnPc1	muž
byli	být	k5eAaImAgMnP	být
se	s	k7c7	s
ztrátami	ztráta	k1gFnPc7	ztráta
odraženi	odražen	k2eAgMnPc1d1	odražen
a	a	k8xC	a
Žižka	Žižka	k1gMnSc1	Žižka
od	od	k7c2	od
dalších	další	k2eAgFnPc2d1	další
akcí	akce	k1gFnPc2	akce
upustil	upustit	k5eAaPmAgInS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ústupu	ústup	k1gInSc6	ústup
z	z	k7c2	z
Litoměřicka	Litoměřicko	k1gNnSc2	Litoměřicko
táborské	táborský	k2eAgNnSc1d1	táborské
vojsko	vojsko	k1gNnSc1	vojsko
prošlo	projít	k5eAaPmAgNnS	projít
Roudnicí	Roudnice	k1gFnSc7	Roudnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
částečně	částečně	k6eAd1	částečně
zbořilo	zbořit	k5eAaPmAgNnS	zbořit
místní	místní	k2eAgInSc4d1	místní
klášter	klášter	k1gInSc4	klášter
a	a	k8xC	a
pobralo	pobrat	k5eAaPmAgNnS	pobrat
bohoslužebné	bohoslužebný	k2eAgNnSc1d1	bohoslužebné
nářadí	nářadí	k1gNnSc1	nářadí
<g/>
.	.	kIx.	.
<g/>
Zpáteční	zpáteční	k2eAgFnSc4d1	zpáteční
cestu	cesta	k1gFnSc4	cesta
jednooký	jednooký	k2eAgMnSc1d1	jednooký
hejtman	hejtman	k1gMnSc1	hejtman
podnikal	podnikat	k5eAaImAgMnS	podnikat
zřejmě	zřejmě	k6eAd1	zřejmě
nakvap	nakvap	k6eAd1	nakvap
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
dnech	den	k1gInPc6	den
3.	[number]	k4	3.
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
účastnil	účastnit	k5eAaImAgMnS	účastnit
sněmu	sněm	k1gInSc2	sněm
v	v	k7c6	v
Čáslavi	Čáslav	k1gFnSc6	Čáslav
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
sněmovního	sněmovní	k2eAgNnSc2d1	sněmovní
usnesení	usnesení	k1gNnSc2	usnesení
pak	pak	k6eAd1	pak
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
k	k	k7c3	k
hradu	hrad	k1gInSc2	hrad
Rabí	rabí	k1gMnPc4	rabí
<g/>
,	,	kIx,	,
při	při	k7c6	při
jehož	jehož	k3xOyRp3gInSc6	jehož
obléhání	obléhání	k1gNnSc6	obléhání
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
těžké	těžký	k2eAgNnSc1d1	těžké
poranění	poranění	k1gNnSc1	poranění
druhého	druhý	k4xOgNnSc2	druhý
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
Kroniky	kronika	k1gFnSc2	kronika
velmi	velmi	k6eAd1	velmi
pěkné	pěkný	k2eAgFnPc1d1	pěkná
o	o	k7c4	o
Janu	Jan	k1gMnSc3	Jan
Žižkovi	Žižka	k1gMnSc3	Žižka
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Potom	potom	k8xC	potom
táhl	táhnout	k5eAaImAgMnS	táhnout
jest	být	k5eAaImIp3nS	být
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
obehnal	obehnat	k5eAaPmAgMnS	obehnat
jest	být	k5eAaImIp3nS	být
Rabí	rabí	k1gMnPc4	rabí
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
a	a	k8xC	a
tu	tu	k6eAd1	tu
mu	on	k3xPp3gMnSc3	on
jest	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
oko	oko	k1gNnSc1	oko
vystřeleno	vystřelit	k5eAaPmNgNnS	vystřelit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
byl	být	k5eAaImAgMnS	být
jest	být	k5eAaImIp3nS	být
slep	slepit	k5eAaBmRp2nS	slepit
a	a	k8xC	a
ledva	ledva	k?	ledva
živ	živ	k2eAgMnSc1d1	živ
zoustal	zoustat	k5eAaImAgMnS	zoustat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Enea	Enea	k6eAd1	Enea
Silvio	Silvio	k6eAd1	Silvio
Piccolomini	Piccolomin	k2eAgMnPc1d1	Piccolomin
v	v	k7c6	v
Historii	historie	k1gFnSc6	historie
české	český	k2eAgFnSc6d1	Česká
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
svolav	svolat	k5eAaPmDgInS	svolat
bratry	bratr	k1gMnPc4	bratr
Táborské	Táborské	k2eAgNnSc1d1	Táborské
a	a	k8xC	a
Orebské	orebský	k2eAgNnSc1d1	Orebské
<g/>
,	,	kIx,	,
ležel	ležet	k5eAaImAgMnS	ležet
před	před	k7c7	před
zámkem	zámek	k1gInSc7	zámek
<g/>
,	,	kIx,	,
kterémuž	kterémuž	k?	kterémuž
Rábí	Rábí	k1gNnSc1	Rábí
jméno	jméno	k1gNnSc4	jméno
jest	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
obležených	obležený	k2eAgFnPc2d1	obležená
všie	všie	k1gFnPc2	všie
mocí	moc	k1gFnPc2	moc
dobýval	dobývat	k5eAaImAgInS	dobývat
<g/>
,	,	kIx,	,
v	v	k7c6	v
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
kterýmž	kterýmž	k?	kterýmž
jediným	jediný	k2eAgInSc7d1	jediný
světlo	světlo	k1gNnSc4	světlo
nebeské	nebeský	k2eAgFnSc2d1	nebeská
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
strelů	strel	k1gInPc2	strel
uhozen	uhodit	k5eAaPmNgInS	uhodit
jsa	být	k5eAaImSgMnS	být
<g/>
,	,	kIx,	,
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
k	k	k7c3	k
lékaruom	lékaruom	k1gInSc1	lékaruom
dovezen	dovezen	k2eAgInSc1d1	dovezen
jsa	být	k5eAaImSgMnS	být
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
na	na	k7c6	na
ránu	ráno	k1gNnSc6	ráno
ulečený	ulečený	k2eAgInSc4d1	ulečený
<g/>
,	,	kIx,	,
při	při	k7c6	při
životě	život	k1gInSc6	život
zůstal	zůstat	k5eAaPmAgMnS	zůstat
<g/>
,	,	kIx,	,
zraku	zrak	k1gInSc2	zrak
však	však	k9	však
nikoli	nikoli	k9	nikoli
jest	být	k5eAaImIp3nS	být
nenabyl	nabýt	k5eNaPmAgMnS	nabýt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
I	i	k9	i
přes	přes	k7c4	přes
indicie	indicie	k1gFnPc4	indicie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
vážné	vážný	k2eAgNnSc4d1	vážné
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Žižka	Žižka	k1gMnSc1	Žižka
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
metropoli	metropol	k1gFnSc6	metropol
dlouho	dlouho	k6eAd1	dlouho
nezdržel	zdržet	k5eNaPmAgMnS	zdržet
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
6.	[number]	k4	6.
srpna	srpen	k1gInSc2	srpen
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
pražanům	pražan	k1gMnPc3	pražan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
táhli	táhnout	k5eAaImAgMnP	táhnout
proti	proti	k7c3	proti
Mostu	most	k1gInSc3	most
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
aspoň	aspoň	k9	aspoň
nahnal	nahnat	k5eAaPmAgMnS	nahnat
Němcům	Němec	k1gMnPc3	Němec
strachu	strach	k1gInSc2	strach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
se	se	k3xPyFc4	se
hejtman	hejtman	k1gMnSc1	hejtman
Žižka	Žižka	k1gMnSc1	Žižka
zabýval	zabývat	k5eAaImAgMnS	zabývat
v	v	k7c6	v
týdnech	týden	k1gInPc6	týden
po	po	k7c6	po
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgNnSc2	tento
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
list	list	k1gInSc1	list
Domažlickým	domažlický	k2eAgFnPc3d1	Domažlická
je	být	k5eAaImIp3nS	být
opatřen	opatřit	k5eAaPmNgInS	opatřit
datem	datum	k1gNnSc7	datum
12.	[number]	k4	12.
září	zářit	k5eAaImIp3nP	zářit
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
historiky	historik	k1gMnPc7	historik
datován	datován	k2eAgInSc4d1	datován
správně	správně	k6eAd1	správně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nasnadě	nasnadě	k6eAd1	nasnadě
<g/>
,	,	kIx,	,
že	že	k8xS	že
formoval	formovat	k5eAaImAgMnS	formovat
nové	nový	k2eAgFnPc4d1	nová
jednotky	jednotka	k1gFnPc4	jednotka
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
vojskům	vojsko	k1gNnPc3	vojsko
druhé	druhý	k4xOgFnSc2	druhý
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Husitská	husitský	k2eAgFnSc1d1	husitská
kronika	kronika	k1gFnSc1	kronika
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
teprve	teprve	k6eAd1	teprve
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyprostil	vyprostit	k5eAaPmAgMnS	vyprostit
z	z	k7c2	z
obležení	obležení	k1gNnSc2	obležení
Lomnici	Lomnice	k1gFnSc4	Lomnice
<g/>
,	,	kIx,	,
vypálil	vypálit	k5eAaPmAgMnS	vypálit
některé	některý	k3yIgFnPc4	některý
tvrze	tvrz	k1gFnPc4	tvrz
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
dobyl	dobýt	k5eAaPmAgInS	dobýt
Soběslav	Soběslav	k1gFnSc4	Soběslav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vypálil	vypálit	k5eAaPmAgMnS	vypálit
rožmberský	rožmberský	k2eAgInSc4d1	rožmberský
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
pobořil	pobořit	k5eAaPmAgMnS	pobořit
zdi	zeď	k1gFnPc4	zeď
města	město	k1gNnSc2	město
i	i	k8xC	i
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
14.	[number]	k4	14.
října	říjen	k1gInSc2	říjen
pak	pak	k6eAd1	pak
udeřil	udeřit	k5eAaPmAgInS	udeřit
na	na	k7c4	na
tvrz	tvrz	k1gFnSc4	tvrz
Ostrov	ostrov	k1gInSc1	ostrov
ležící	ležící	k2eAgMnSc1d1	ležící
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Nežárce	Nežárka	k1gFnSc6	Nežárka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
létě	léto	k1gNnSc6	léto
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
adamité	adamita	k1gMnPc1	adamita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
boji	boj	k1gInSc6	boj
zde	zde	k6eAd1	zde
zajal	zajmout	k5eAaPmAgInS	zajmout
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
desítky	desítka	k1gFnPc4	desítka
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
dal	dát	k5eAaPmAgMnS	dát
zanedlouho	zanedlouho	k6eAd1	zanedlouho
upálit	upálit	k5eAaPmF	upálit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
Žižkovu	Žižkův	k2eAgFnSc4d1	Žižkova
pozornost	pozornost	k1gFnSc4	pozornost
vysloužil	vysloužit	k5eAaPmAgInS	vysloužit
hrad	hrad	k1gInSc1	hrad
Krasíkov	Krasíkov	k1gInSc1	Krasíkov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
oblehly	oblehnout	k5eAaPmAgFnP	oblehnout
jednotky	jednotka	k1gFnPc4	jednotka
plzeňského	plzeňské	k1gNnSc2	plzeňské
landfrýdu	landfrýd	k1gInSc2	landfrýd
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
pevnost	pevnost	k1gFnSc4	pevnost
uvolnit	uvolnit	k5eAaPmF	uvolnit
<g/>
,	,	kIx,	,
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
oblehatelům	oblehatel	k1gMnPc3	oblehatel
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
oddíl	oddíl	k1gInSc1	oddíl
Jindřicha	Jindřich	k1gMnSc2	Jindřich
z	z	k7c2	z
Plavna	Plavno	k1gNnSc2	Plavno
a	a	k8xC	a
jediné	jediný	k2eAgFnSc6d1	jediná
<g/>
,	,	kIx,	,
čeho	co	k3yRnSc2	co
se	se	k3xPyFc4	se
husitům	husita	k1gMnPc3	husita
podařilo	podařit	k5eAaPmAgNnS	podařit
před	před	k7c7	před
ústupem	ústup	k1gInSc7	ústup
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dodávka	dodávka	k1gFnSc1	dodávka
zásob	zásoba	k1gFnPc2	zásoba
obleženým	obležený	k2eAgMnSc7d1	obležený
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nezdaru	nezdar	k1gInSc6	nezdar
se	se	k3xPyFc4	se
Žižka	Žižka	k1gMnSc1	Žižka
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
ke	k	k7c3	k
Klatovům	Klatovy	k1gInPc3	Klatovy
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
opustil	opustit	k5eAaPmAgMnS	opustit
10.	[number]	k4	10.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
1500	[number]	k4	1500
pěších	pěší	k1gMnPc2	pěší
a	a	k8xC	a
400	[number]	k4	400
jízdních	jízdní	k2eAgMnPc2d1	jízdní
ozbrojenců	ozbrojenec	k1gMnPc2	ozbrojenec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
pramenů	pramen	k1gInPc2	pramen
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
vpád	vpád	k1gInSc4	vpád
směřující	směřující	k2eAgInSc4d1	směřující
na	na	k7c4	na
Plzeňsko	Plzeňsko	k1gNnSc4	Plzeňsko
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
odporem	odpor	k1gInSc7	odpor
připraveného	připravený	k2eAgMnSc2d1	připravený
nepřítele	nepřítel	k1gMnSc2	nepřítel
a	a	k8xC	a
Žižkovi	Žižka	k1gMnSc3	Žižka
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
než	než	k8xS	než
se	se	k3xPyFc4	se
stahovat	stahovat	k5eAaImF	stahovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Žatci	Žatec	k1gInSc3	Žatec
<g/>
.	.	kIx.	.
</s>
<s>
Kroniky	kronika	k1gFnPc1	kronika
dále	daleko	k6eAd2	daleko
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c2	za
neustálých	neustálý	k2eAgFnPc2d1	neustálá
šarvátek	šarvátka	k1gFnPc2	šarvátka
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgInS	nutit
západně	západně	k6eAd1	západně
od	od	k7c2	od
Žlutic	Žlutice	k1gFnPc2	Žlutice
vyhledat	vyhledat	k5eAaPmF	vyhledat
útočiště	útočiště	k1gNnSc4	útočiště
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
jménem	jméno	k1gNnSc7	jméno
Vladař	vladař	k1gMnSc1	vladař
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
vozové	vozový	k2eAgFnSc2d1	vozová
hradby	hradba	k1gFnSc2	hradba
po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
odolával	odolávat	k5eAaImAgMnS	odolávat
protivníkovu	protivníkův	k2eAgInSc3d1	protivníkův
náporu	nápor	k1gInSc3	nápor
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
jeho	jeho	k3xOp3gInPc1	jeho
oddíly	oddíl	k1gInPc1	oddíl
podnikly	podniknout	k5eAaPmAgInP	podniknout
zdařilý	zdařilý	k2eAgInSc4d1	zdařilý
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
průlom	průlom	k1gInSc4	průlom
a	a	k8xC	a
probily	probít	k5eAaPmAgFnP	probít
se	se	k3xPyFc4	se
k	k	k7c3	k
posilám	posila	k1gFnPc3	posila
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jim	on	k3xPp3gMnPc3	on
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
vyslali	vyslat	k5eAaPmAgMnP	vyslat
žatečtí	žatecký	k2eAgMnPc1d1	žatecký
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Žlutic	Žlutice	k1gFnPc2	Žlutice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
dorazil	dorazit	k5eAaPmAgMnS	dorazit
Žižka	Žižka	k1gMnSc1	Žižka
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
své	svůj	k3xOyFgFnSc2	svůj
brané	braný	k2eAgFnSc2d1	braná
moci	moc	k1gFnSc2	moc
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
přivítán	přivítat	k5eAaPmNgInS	přivítat
vyzváněním	vyzvánění	k1gNnSc7	vyzvánění
zvonů	zvon	k1gInPc2	zvon
a	a	k8xC	a
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
jako	jako	k8xS	jako
kníže	kníže	k1gMnSc1	kníže
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
příchodem	příchod	k1gInSc7	příchod
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
řady	řada	k1gFnPc4	řada
husitských	husitský	k2eAgMnPc2d1	husitský
ozbrojených	ozbrojený	k2eAgMnPc2d1	ozbrojený
svazů	svaz	k1gInPc2	svaz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
shromažďovaly	shromažďovat	k5eAaImAgInP	shromažďovat
proti	proti	k7c3	proti
vpádu	vpád	k1gInSc3	vpád
křižáckých	křižácký	k2eAgNnPc2d1	křižácké
vojsk	vojsko	k1gNnPc2	vojsko
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
jednal	jednat	k5eAaImAgInS	jednat
s	s	k7c7	s
Pražany	Pražan	k1gMnPc7	Pražan
<g/>
,	,	kIx,	,
8.	[number]	k4	8.
prosince	prosinec	k1gInSc2	prosinec
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gMnPc1	jeho
táborité	táborita	k1gMnPc1	táborita
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
zajistit	zajistit	k5eAaPmF	zajistit
prostor	prostor	k1gInSc4	prostor
u	u	k7c2	u
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
;	;	kIx,	;
ostatní	ostatní	k2eAgNnSc1d1	ostatní
vojsko	vojsko	k1gNnSc1	vojsko
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
dne	den	k1gInSc2	den
následujícího	následující	k2eAgInSc2d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Kroniky	kronika	k1gFnSc2	kronika
velmi	velmi	k6eAd1	velmi
pěkné	pěkná	k1gFnSc2	pěkná
o	o	k7c6	o
Janu	Jan	k1gMnSc6	Jan
Žižkovi	Žižka	k1gMnSc6	Žižka
operace	operace	k1gFnSc1	operace
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
řídil	řídit	k5eAaImAgMnS	řídit
osobně	osobně	k6eAd1	osobně
slepý	slepý	k2eAgMnSc1d1	slepý
táborský	táborský	k2eAgMnSc1d1	táborský
hejtman	hejtman	k1gMnSc1	hejtman
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
formálně	formálně	k6eAd1	formálně
převzal	převzít	k5eAaPmAgMnS	převzít
velení	velení	k1gNnSc2	velení
všech	všecek	k3xTgNnPc2	všecek
shromážděných	shromážděný	k2eAgNnPc2d1	shromážděné
husitských	husitský	k2eAgNnPc2d1	husitské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
střetnutí	střetnutí	k1gNnSc2	střetnutí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
21.	[number]	k4	21.
a	a	k8xC	a
22.	[number]	k4	22.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
víceméně	víceméně	k9	víceméně
neúspěšná	úspěšný	k2eNgNnPc1d1	neúspěšné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
husité	husita	k1gMnPc1	husita
ztratili	ztratit	k5eAaPmAgMnP	ztratit
město	město	k1gNnSc4	město
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
byli	být	k5eAaImAgMnP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
se	se	k3xPyFc4	se
probít	probít	k5eAaPmF	probít
z	z	k7c2	z
obklíčení	obklíčení	k1gNnSc2	obklíčení
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Kolín	Kolín	k1gInSc4	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
průnik	průnik	k1gInSc1	průnik
křižáckým	křižácký	k2eAgNnSc7d1	křižácké
vojskem	vojsko	k1gNnSc7	vojsko
byl	být	k5eAaImAgMnS	být
však	však	k8xC	však
natolik	natolik	k6eAd1	natolik
důrazný	důrazný	k2eAgMnSc1d1	důrazný
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
neodhodlal	odhodlat	k5eNaPmAgMnS	odhodlat
k	k	k7c3	k
pronásledování	pronásledování	k1gNnSc3	pronásledování
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
původních	původní	k2eAgFnPc6d1	původní
pozicích	pozice	k1gFnPc6	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rok	rok	k1gInSc1	rok
1422	[number]	k4	1422
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
dnech	den	k1gInPc6	den
starého	starý	k2eAgInSc2d1	starý
roku	rok	k1gInSc2	rok
hejtman	hejtman	k1gMnSc1	hejtman
Žižka	Žižka	k1gMnSc1	Žižka
doplnil	doplnit	k5eAaPmAgMnS	doplnit
své	svůj	k3xOyFgNnSc4	svůj
vojsko	vojsko	k1gNnSc4	vojsko
v	v	k7c6	v
Jičínském	jičínský	k2eAgInSc6d1	jičínský
a	a	k8xC	a
Turnovském	turnovský	k2eAgInSc6d1	turnovský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
6.	[number]	k4	6.
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
Nebovidech	Nebovid	k1gInPc6	Nebovid
nedaleko	nedaleko	k7c2	nedaleko
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
křižáci	křižák	k1gMnPc1	křižák
protiútok	protiútok	k1gInSc4	protiútok
neočekávali	očekávat	k5eNaImAgMnP	očekávat
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
rozptýleni	rozptýlit	k5eAaPmNgMnP	rozptýlit
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
na	na	k7c4	na
účinnou	účinný	k2eAgFnSc4d1	účinná
obranu	obrana	k1gFnSc4	obrana
nemohli	moct	k5eNaImAgMnP	moct
pomýšlet	pomýšlet	k5eAaImF	pomýšlet
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Zikmund	Zikmund	k1gMnSc1	Zikmund
dal	dát	k5eAaPmAgMnS	dát
proto	proto	k8xC	proto
rozkaz	rozkaz	k1gInSc4	rozkaz
k	k	k7c3	k
zapálení	zapálení	k1gNnSc3	zapálení
města	město	k1gNnSc2	město
a	a	k8xC	a
ústupu	ústup	k1gInSc2	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
demoralizované	demoralizovaný	k2eAgFnPc1d1	demoralizovaná
protivníkovy	protivníkův	k2eAgFnPc1d1	protivníkova
síly	síla	k1gFnPc1	síla
stát	stát	k5eAaImF	stát
Žižkovou	Žižková	k1gFnSc4	Žižková
snadnou	snadný	k2eAgFnSc7d1	snadná
kořistí	kořist	k1gFnSc7	kořist
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
jeho	jeho	k3xOp3gFnSc7	jeho
snahou	snaha	k1gFnSc7	snaha
byla	být	k5eAaImAgFnS	být
likvidace	likvidace	k1gFnSc1	likvidace
požáru	požár	k1gInSc2	požár
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
zvládnutí	zvládnutí	k1gNnSc6	zvládnutí
tohoto	tento	k3xDgInSc2	tento
úkolu	úkol	k1gInSc2	úkol
vyrazil	vyrazit	k5eAaPmAgInS	vyrazit
rychlým	rychlý	k2eAgInSc7d1	rychlý
pochodem	pochod	k1gInSc7	pochod
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
křižáckého	křižácký	k2eAgNnSc2d1	křižácké
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
8.	[number]	k4	8.
ledna	leden	k1gInSc2	leden
dostihl	dostihnout	k5eAaPmAgMnS	dostihnout
jeho	jeho	k3xOp3gInSc4	jeho
zadní	zadní	k2eAgInSc4d1	zadní
voj	voj	k1gInSc4	voj
u	u	k7c2	u
městyse	městys	k1gInSc2	městys
Habry	habr	k1gInPc1	habr
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
soustředěný	soustředěný	k2eAgInSc4d1	soustředěný
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
patrně	patrně	k6eAd1	patrně
organizoval	organizovat	k5eAaBmAgMnS	organizovat
italský	italský	k2eAgMnSc1d1	italský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Filippo	Filippa	k1gFnSc5	Filippa
Scollari	Scollar	k1gFnSc5	Scollar
<g/>
,	,	kIx,	,
však	však	k9	však
skončil	skončit	k5eAaPmAgInS	skončit
panickým	panický	k2eAgInSc7d1	panický
útěkem	útěk	k1gInSc7	útěk
všech	všecek	k3xTgInPc2	všecek
zúčastněných	zúčastněný	k2eAgInPc2d1	zúčastněný
křižáků	křižák	k1gInPc2	křižák
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Habrů	habr	k1gInPc2	habr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
husité	husita	k1gMnPc1	husita
protivníka	protivník	k1gMnSc4	protivník
pronásledovali	pronásledovat	k5eAaImAgMnP	pronásledovat
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
důrazem	důraz	k1gInSc7	důraz
a	a	k8xC	a
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
dorazili	dorazit	k5eAaPmAgMnP	dorazit
k	k	k7c3	k
branám	brána	k1gFnPc3	brána
zhruba	zhruba	k6eAd1	zhruba
19	[number]	k4	19
kilometrů	kilometr	k1gInPc2	kilometr
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
křižáci	křižák	k1gMnPc1	křižák
patrně	patrně	k6eAd1	patrně
ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
neúspěšně	úspěšně	k6eNd1	úspěšně
střetli	střetnout	k5eAaPmAgMnP	střetnout
s	s	k7c7	s
Žižkou	Žižka	k1gMnSc7	Žižka
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
bitvě	bitva	k1gFnSc6	bitva
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
stovky	stovka	k1gFnPc1	stovka
jich	on	k3xPp3gMnPc2	on
zahynuly	zahynout	k5eAaPmAgFnP	zahynout
při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
přes	přes	k7c4	přes
Sázavu	Sázava	k1gFnSc4	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
tragickou	tragický	k2eAgFnSc4d1	tragická
epizodu	epizoda	k1gFnSc4	epizoda
kruciáty	kruciáta	k1gFnSc2	kruciáta
zůstala	zůstat	k5eAaPmAgFnS	zůstat
mezi	mezi	k7c7	mezi
hradbami	hradba	k1gFnPc7	hradba
města	město	k1gNnSc2	město
část	část	k1gFnSc1	část
velitelů	velitel	k1gMnPc2	velitel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
odhodlaní	odhodlaný	k2eAgMnPc1d1	odhodlaný
alespoň	alespoň	k9	alespoň
dočasně	dočasně	k6eAd1	dočasně
udržet	udržet	k5eAaPmF	udržet
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
krytí	krytí	k1gNnSc4	krytí
králi	král	k1gMnSc3	král
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
i	i	k8xC	i
prchajícím	prchající	k2eAgNnPc3d1	prchající
vojskům	vojsko	k1gNnPc3	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Povel	povel	k1gInSc1	povel
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
vydal	vydat	k5eAaPmAgMnS	vydat
Žižka	Žižka	k1gMnSc1	Žižka
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
oboustranně	oboustranně	k6eAd1	oboustranně
silné	silný	k2eAgFnSc2d1	silná
dělostřelby	dělostřelba	k1gFnSc2	dělostřelba
se	se	k3xPyFc4	se
husité	husita	k1gMnPc1	husita
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zdolat	zdolat	k5eAaPmF	zdolat
opevnění	opevněný	k2eAgMnPc1d1	opevněný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
obráncům	obránce	k1gMnPc3	obránce
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
podařilo	podařit	k5eAaPmAgNnS	podařit
odrážet	odrážet	k5eAaImF	odrážet
až	až	k9	až
do	do	k7c2	do
podvečerních	podvečerní	k2eAgFnPc2d1	podvečerní
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
10.	[number]	k4	10.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgInS	zahájit
Žižkův	Žižkův	k2eAgInSc1d1	Žižkův
štáb	štáb	k1gInSc1	štáb
s	s	k7c7	s
veliteli	velitel	k1gMnPc7	velitel
města	město	k1gNnSc2	město
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
o	o	k7c4	o
kapitulaci	kapitulace	k1gFnSc4	kapitulace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
bylo	být	k5eAaImAgNnS	být
patrně	patrně	k6eAd1	patrně
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
část	část	k1gFnSc1	část
husitů	husita	k1gMnPc2	husita
nerespektovala	respektovat	k5eNaImAgFnS	respektovat
a	a	k8xC	a
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
slepého	slepý	k2eAgMnSc2d1	slepý
hejtmana	hejtman	k1gMnSc2	hejtman
vnikla	vniknout	k5eAaPmAgFnS	vniknout
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
hodin	hodina	k1gFnPc2	hodina
prý	prý	k9	prý
bylo	být	k5eAaImAgNnS	být
touto	tento	k3xDgFnSc7	tento
skupinou	skupina	k1gFnSc7	skupina
zabito	zabit	k2eAgNnSc4d1	zabito
na	na	k7c4	na
1500	[number]	k4	1500
přítomných	přítomný	k2eAgMnPc2d1	přítomný
vojáků	voják	k1gMnPc2	voják
i	i	k8xC	i
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgMnPc3	tento
zvěrstvům	zvěrstvo	k1gNnPc3	zvěrstvo
nejspíš	nejspíš	k9	nejspíš
nahrál	nahrát	k5eAaPmAgInS	nahrát
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodil	narodit	k5eAaPmAgMnS	narodit
(	(	kIx(	(
<g/>
a	a	k8xC	a
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
husitů	husita	k1gMnPc2	husita
skrýval	skrývat	k5eAaImAgMnS	skrývat
<g/>
)	)	kIx)	)
Husův	Husův	k2eAgMnSc1d1	Husův
odpůrce	odpůrce	k1gMnSc1	odpůrce
Michal	Michal	k1gMnSc1	Michal
de	de	k?	de
Causis	Causis	k1gInSc1	Causis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Husa	husa	k1gFnSc1	husa
na	na	k7c6	na
kostnickém	kostnický	k2eAgInSc6d1	kostnický
koncilu	koncil	k1gInSc6	koncil
obvinil	obvinit	k5eAaPmAgInS	obvinit
z	z	k7c2	z
bludařství	bludařství	k1gNnSc2	bludařství
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
František	František	k1gMnSc1	František
Šmahel	Šmahel	k1gMnSc1	Šmahel
k	k	k7c3	k
události	událost	k1gFnSc3	událost
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
útok	útok	k1gInSc1	útok
na	na	k7c4	na
pobořené	pobořený	k2eAgFnPc4d1	pobořená
hradby	hradba	k1gFnPc4	hradba
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
byl	být	k5eAaImAgMnS	být
porušením	porušení	k1gNnSc7	porušení
válečné	válečný	k2eAgFnSc2d1	válečná
konvence	konvence	k1gFnSc2	konvence
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
činu	čin	k1gInSc6	čin
ještě	ještě	k6eAd1	ještě
po	po	k7c6	po
roce	rok	k1gInSc6	rok
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
úhonu	úhona	k1gFnSc4	úhona
na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
cti	čest	k1gFnSc6	čest
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
letopisy	letopis	k1gInPc1	letopis
české	český	k2eAgInPc1d1	český
dále	daleko	k6eAd2	daleko
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
"	"	kIx"	"
<g/>
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
před	před	k7c7	před
oktávem	oktáv	k1gInSc7	oktáv
Zjevení	zjevení	k1gNnSc2	zjevení
Páně	páně	k2eAgNnSc2d1	páně
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
bratr	bratr	k1gMnSc1	bratr
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
stal	stát	k5eAaPmAgMnS	stát
rytířem	rytíř	k1gMnSc7	rytíř
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
jakým	jaký	k3yRgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
Žižka	Žižka	k1gMnSc1	Žižka
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
revolučního	revoluční	k2eAgNnSc2d1	revoluční
dění	dění	k1gNnSc2	dění
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
prameny	pramen	k1gInPc1	pramen
opět	opět	k6eAd1	opět
mlčí	mlčet	k5eAaImIp3nP	mlčet
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
jeho	jeho	k3xOp3gFnPc6	jeho
aktivitách	aktivita	k1gFnPc6	aktivita
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
nejspíš	nejspíš	k9	nejspíš
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sváděl	svádět	k5eAaImAgMnS	svádět
boje	boj	k1gInPc4	boj
s	s	k7c7	s
plzeňským	plzeňský	k2eAgInSc7d1	plzeňský
landfrýdem	landfrýd	k1gInSc7	landfrýd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
dorazil	dorazit	k5eAaPmAgMnS	dorazit
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
litevský	litevský	k2eAgMnSc1d1	litevský
kníže	kníže	k1gMnSc1	kníže
Zikmund	Zikmund	k1gMnSc1	Zikmund
Korybutovič	Korybutovič	k1gMnSc1	Korybutovič
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterého	který	k3yQgMnSc4	který
Čechové	Čech	k1gMnPc1	Čech
velmi	velmi	k6eAd1	velmi
usilovali	usilovat	k5eAaImAgMnP	usilovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vládl	vládnout	k5eAaImAgInS	vládnout
české	český	k2eAgFnSc3d1	Česká
zemi	zem	k1gFnSc3	zem
proti	proti	k7c3	proti
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
<g/>
,	,	kIx,	,
králi	král	k1gMnSc3	král
uherskému	uherský	k2eAgMnSc3d1	uherský
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Korybutovič	Korybutovič	k1gMnSc1	Korybutovič
zanedlouho	zanedlouho	k6eAd1	zanedlouho
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přijal	přijmout	k5eAaPmAgInS	přijmout
titul	titul	k1gInSc1	titul
zemského	zemský	k2eAgMnSc2d1	zemský
správce	správce	k1gMnSc2	správce
<g/>
.	.	kIx.	.
11.	[number]	k4	11.
června	červen	k1gInSc2	červen
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
táborští	táborští	k1gMnPc1	táborští
písemně	písemně	k6eAd1	písemně
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
pražanům	pražan	k1gMnPc3	pražan
<g/>
,	,	kIx,	,
že	že	k8xS	že
litevského	litevský	k2eAgMnSc2d1	litevský
knížete	kníže	k1gMnSc2	kníže
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
správce	správce	k1gMnSc4	správce
akceptují	akceptovat	k5eAaBmIp3nP	akceptovat
<g/>
.	.	kIx.	.
</s>
<s>
Budoucnost	budoucnost	k1gFnSc1	budoucnost
však	však	k9	však
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc4	jejich
slib	slib	k1gInSc4	slib
pomoci	pomoc	k1gFnSc2	pomoc
a	a	k8xC	a
poslušenství	poslušenství	k1gNnSc2	poslušenství
nebyl	být	k5eNaImAgInS	být
přímo	přímo	k6eAd1	přímo
dodržen	dodržen	k2eAgInSc1d1	dodržen
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
za	za	k7c4	za
dosud	dosud	k6eAd1	dosud
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
jasných	jasný	k2eAgFnPc2d1	jasná
okolností	okolnost	k1gFnPc2	okolnost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
roztržce	roztržka	k1gFnSc3	roztržka
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Táborem	Tábor	k1gInSc7	Tábor
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
Zikmundem	Zikmund	k1gMnSc7	Zikmund
Korybutem	Korybut	k1gMnSc7	Korybut
a	a	k8xC	a
Žižkou	Žižka	k1gMnSc7	Žižka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
oddíl	oddíl	k1gInSc1	oddíl
Táboritů	táborita	k1gMnPc2	táborita
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Jana	Jan	k1gMnSc2	Jan
Hvězdy	Hvězda	k1gMnSc2	Hvězda
z	z	k7c2	z
Vícemilic	Vícemilice	k1gFnPc2	Vícemilice
(	(	kIx(	(
<g/>
známého	známý	k2eAgNnSc2d1	známé
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Bzdinka	bzdinka	k1gFnSc1	bzdinka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zmocnit	zmocnit	k5eAaPmF	zmocnit
české	český	k2eAgFnSc2d1	Česká
metropole	metropol	k1gFnSc2	metropol
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
análů	anály	k1gInPc2	anály
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
zda	zda	k8xS	zda
s	s	k7c7	s
tichým	tichý	k2eAgInSc7d1	tichý
souhlasem	souhlas	k1gInSc7	souhlas
či	či	k8xC	či
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
podzimu	podzim	k1gInSc2	podzim
slepý	slepý	k2eAgMnSc1d1	slepý
táborský	táborský	k2eAgMnSc1d1	táborský
hejtman	hejtman	k1gMnSc1	hejtman
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
řídil	řídit	k5eAaImAgMnS	řídit
obléhání	obléhání	k1gNnSc4	obléhání
Bechyně	Bechyně	k1gFnSc2	Bechyně
a	a	k8xC	a
Strakonic	Strakonice	k1gFnPc2	Strakonice
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgNnPc2d1	další
opevněných	opevněný	k2eAgNnPc2d1	opevněné
sídel	sídlo	k1gNnPc2	sídlo
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
panství	panství	k1gNnSc6	panství
Oldřicha	Oldřich	k1gMnSc2	Oldřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rok	rok	k1gInSc1	rok
1423	[number]	k4	1423
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Další	další	k2eAgInPc4d1	další
dokumenty	dokument	k1gInPc4	dokument
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
z	z	k7c2	z
Kalichu	kalich	k1gInSc2	kalich
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
listy	list	k1gInPc4	list
odeslané	odeslaný	k2eAgNnSc1d1	odeslané
z	z	k7c2	z
kláštera	klášter	k1gInSc2	klášter
ve	v	k7c6	v
Vilémově	Vilémův	k2eAgFnSc6d1	Vilémova
datované	datovaný	k2eAgInPc1d1	datovaný
k	k	k7c3	k
26.	[number]	k4	26.
březnu	březen	k1gInSc3	březen
a	a	k8xC	a
1.	[number]	k4	1.
dubnu	duben	k1gInSc6	duben
1423	[number]	k4	1423
<g/>
.	.	kIx.	.
</s>
<s>
Slepý	slepý	k2eAgMnSc1d1	slepý
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
své	svůj	k3xOyFgMnPc4	svůj
straníky	straník	k1gMnPc4	straník
z	z	k7c2	z
orebského	orebský	k2eAgInSc2d1	orebský
svazu	svaz	k1gInSc2	svaz
k	k	k7c3	k
poradě	porada	k1gFnSc3	porada
naplánované	naplánovaný	k2eAgFnSc2d1	naplánovaná
na	na	k7c6	na
7.	[number]	k4	7.
či	či	k8xC	či
8.	[number]	k4	8.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dopisů	dopis	k1gInPc2	dopis
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
dokonaleji	dokonale	k6eAd2	dokonale
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
husitskou	husitský	k2eAgFnSc4d1	husitská
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
domácím	domácí	k2eAgMnSc7d1	domácí
i	i	k8xC	i
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Čtrnáct	čtrnáct	k4xCc4	čtrnáct
dní	den	k1gInPc2	den
po	po	k7c6	po
sněmu	sněm	k1gInSc6	sněm
Žižka	Žižka	k1gMnSc1	Žižka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
orebity	orebita	k1gMnPc7	orebita
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c4	v
válce	válec	k1gInPc4	válec
se	s	k7c7	s
spojenci	spojenec	k1gMnPc7	spojenec
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
Bydžovsku	Bydžovsko	k1gNnSc6	Bydžovsko
s	s	k7c7	s
Čeňkem	Čeněk	k1gMnSc7	Čeněk
z	z	k7c2	z
Vartenberka	Vartenberka	k1gFnSc1	Vartenberka
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jeho	jeho	k3xOp3gMnPc7	jeho
leníky	leník	k1gMnPc7	leník
a	a	k8xC	a
spojenci	spojenec	k1gMnPc7	spojenec
porazil	porazit	k5eAaPmAgInS	porazit
20.	[number]	k4	20.
nebo	nebo	k8xC	nebo
23.	[number]	k4	23.
dubna	duben	k1gInSc2	duben
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hořic	Hořice	k1gFnPc2	Hořice
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
hradě	hrad	k1gInSc6	hrad
Kalichu	kalich	k1gInSc2	kalich
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
oblehl	oblehnout	k5eAaPmAgInS	oblehnout
hrad	hrad	k1gInSc1	hrad
Pannu	Panna	k1gFnSc4	Panna
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
tvořil	tvořit	k5eAaImAgInS	tvořit
Kalichu	kalich	k1gInSc2	kalich
jeho	jeho	k3xOp3gFnSc4	jeho
katolickou	katolický	k2eAgFnSc4d1	katolická
protiváhu	protiváha	k1gFnSc4	protiváha
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
26.	[number]	k4	26.
června	červen	k1gInSc2	červen
sjednali	sjednat	k5eAaPmAgMnP	sjednat
Táborité	táborita	k1gMnPc1	táborita
a	a	k8xC	a
Pražané	Pražan	k1gMnPc1	Pražan
mír	mír	k1gInSc1	mír
u	u	k7c2	u
Konopiště	Konopiště	k1gNnSc2	Konopiště
<g/>
,	,	kIx,	,
tohoto	tento	k3xDgNnSc2	tento
jednání	jednání	k1gNnSc2	jednání
se	se	k3xPyFc4	se
však	však	k9	však
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
výsledek	výsledek	k1gInSc4	výsledek
Konopišťské	konopišťský	k2eAgFnSc2d1	Konopišťská
smlouvy	smlouva	k1gFnSc2	smlouva
koncem	koncem	k7c2	koncem
měsíce	měsíc	k1gInSc2	měsíc
zamířil	zamířit	k5eAaPmAgMnS	zamířit
k	k	k7c3	k
Hradci	Hradec	k1gInSc3	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zůstával	zůstávat	k5eAaImAgInS	zůstávat
od	od	k7c2	od
návštěvy	návštěva	k1gFnSc2	návštěva
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Korybutoviče	Korybutovič	k1gMnSc2	Korybutovič
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
vlivu	vliv	k1gInSc2	vliv
pražanů	pražan	k1gMnPc2	pražan
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
mezi	mezi	k7c7	mezi
Žižkou	Žižka	k1gMnSc7	Žižka
a	a	k8xC	a
stranou	strana	k1gFnSc7	strana
radikálního	radikální	k2eAgMnSc2d1	radikální
kněze	kněz	k1gMnSc2	kněz
Ambrože	Ambrož	k1gMnSc2	Ambrož
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
mu	on	k3xPp3gMnSc3	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
město	město	k1gNnSc4	město
obsadit	obsadit	k5eAaPmF	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
táborští	táborští	k1gMnPc1	táborští
a	a	k8xC	a
orebité	orebita	k1gMnPc1	orebita
zasadili	zasadit	k5eAaPmAgMnP	zasadit
pražanům	pražan	k1gMnPc3	pražan
další	další	k2eAgFnSc4d1	další
ránu	rána	k1gFnSc4	rána
<g/>
,	,	kIx,	,
když	když	k8xS	když
opanovali	opanovat	k5eAaPmAgMnP	opanovat
i	i	k9	i
Jaroměř	Jaroměř	k1gFnSc4	Jaroměř
a	a	k8xC	a
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
počínání	počínání	k1gNnSc1	počínání
nezbytně	nezbytně	k6eAd1	nezbytně
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
ozbrojenému	ozbrojený	k2eAgInSc3d1	ozbrojený
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nacházelo	nacházet	k5eAaImAgNnS	nacházet
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
4.	[number]	k4	4.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
husitské	husitský	k2eAgFnPc1d1	husitská
armády	armáda	k1gFnPc1	armáda
utkaly	utkat	k5eAaPmAgFnP	utkat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Strachova	Strachův	k2eAgInSc2d1	Strachův
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Slovy	slovo	k1gNnPc7	slovo
kronikáře	kronikář	k1gMnSc2	kronikář
šla	jít	k5eAaImAgFnS	jít
archa	archa	k1gFnSc1	archa
proti	proti	k7c3	proti
arše	archa	k1gFnSc3	archa
a	a	k8xC	a
Žižka	Žižka	k1gMnSc1	Žižka
při	při	k7c6	při
střetnutí	střetnutí	k1gNnSc6	střetnutí
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
drtivého	drtivý	k2eAgNnSc2d1	drtivé
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
boje	boj	k1gInSc2	boj
prý	prý	k9	prý
palcátem	palcát	k1gInSc7	palcát
zabil	zabít	k5eAaPmAgMnS	zabít
kněze	kněz	k1gMnSc2	kněz
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
straně	strana	k1gFnSc6	strana
nesl	nést	k5eAaImAgMnS	nést
svátost	svátost	k1gFnSc1	svátost
oltářní	oltářní	k2eAgFnSc1d1	oltářní
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnSc1	informace
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
naznačovat	naznačovat	k5eAaImF	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hejtmanovi	hejtmanův	k2eAgMnPc1d1	hejtmanův
po	po	k7c6	po
zranění	zranění	k1gNnSc6	zranění
u	u	k7c2	u
hradu	hrad	k1gInSc2	hrad
Rabí	rabí	k1gMnSc1	rabí
zůstal	zůstat	k5eAaPmAgMnS	zůstat
alespoň	alespoň	k9	alespoň
nepatrný	patrný	k2eNgInSc4d1	patrný
zbytek	zbytek	k1gInSc4	zbytek
zraku	zrak	k1gInSc2	zrak
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
slepecké	slepecký	k2eAgNnSc4d1	slepecké
vidění	vidění	k1gNnSc4	vidění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mu	on	k3xPp3gMnSc3	on
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
vnímat	vnímat	k5eAaImF	vnímat
obrysy	obrys	k1gInPc4	obrys
okolí	okolí	k1gNnSc2	okolí
do	do	k7c2	do
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
<g/>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
Žižka	Žižka	k1gMnSc1	Žižka
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
Čáslav	Čáslav	k1gFnSc4	Čáslav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
patrně	patrně	k6eAd1	patrně
připraven	připravit	k5eAaPmNgInS	připravit
stejný	stejný	k2eAgInSc1d1	stejný
převrat	převrat	k1gInSc1	převrat
jako	jako	k8xC	jako
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
pak	pak	k6eAd1	pak
pronikl	proniknout	k5eAaPmAgInS	proniknout
ke	k	k7c3	k
Kutné	kutný	k2eAgFnSc3d1	Kutná
Hoře	hora	k1gFnSc3	hora
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosadil	dosadit	k5eAaPmAgInS	dosadit
losem	los	k1gInSc7	los
vybranou	vybraný	k2eAgFnSc4d1	vybraná
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Čáslavi	Čáslav	k1gFnSc3	Čáslav
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
záhy	záhy	k6eAd1	záhy
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
znovu	znovu	k6eAd1	znovu
zformovaní	zformovaný	k2eAgMnPc1d1	zformovaný
Pražané	Pražan	k1gMnPc1	Pražan
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
akce	akce	k1gFnSc1	akce
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
několikadenních	několikadenní	k2eAgFnPc6d1	několikadenní
šarvátkách	šarvátka	k1gFnPc6	šarvátka
ustoupili	ustoupit	k5eAaPmAgMnP	ustoupit
ke	k	k7c3	k
Kutné	kutný	k2eAgFnSc3d1	Kutná
Hoře	hora	k1gFnSc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zanedlouho	zanedlouho	k6eAd1	zanedlouho
nato	nato	k6eAd1	nato
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tato	tento	k3xDgFnSc1	tento
ztráta	ztráta	k1gFnSc1	ztráta
nebyla	být	k5eNaImAgFnS	být
pro	pro	k7c4	pro
východočeské	východočeský	k2eAgMnPc4d1	východočeský
husity	husita	k1gMnPc4	husita
nijak	nijak	k6eAd1	nijak
výrazná	výrazný	k2eAgFnSc1d1	výrazná
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dalším	další	k2eAgNnSc6d1	další
dění	dění	k1gNnSc6	dění
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
soudobé	soudobý	k2eAgInPc4d1	soudobý
prameny	pramen	k1gInPc4	pramen
mlčí	mlčet	k5eAaImIp3nS	mlčet
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
právě	právě	k9	právě
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
je	být	k5eAaImIp3nS	být
kladeno	kladen	k2eAgNnSc1d1	kladeno
přijetí	přijetí	k1gNnSc1	přijetí
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
manifestu	manifest	k1gInSc2	manifest
nového	nový	k2eAgNnSc2d1	nové
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
Žižkova	Žižkov	k1gInSc2	Žižkov
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známého	známý	k2eAgNnSc2d1	známé
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Žižkův	Žižkův	k2eAgInSc4d1	Žižkův
vojenský	vojenský	k2eAgInSc4d1	vojenský
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Znova	znova	k6eAd1	znova
o	o	k7c6	o
slepém	slepý	k2eAgMnSc6d1	slepý
hejtmanovi	hejtman	k1gMnSc6	hejtman
hovoří	hovořit	k5eAaImIp3nP	hovořit
několik	několik	k4yIc1	několik
pozdějších	pozdní	k2eAgInPc2d2	pozdější
textů	text	k1gInPc2	text
Starých	Starých	k2eAgInPc2d1	Starých
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgInPc2d1	český
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výpravou	výprava	k1gFnSc7	výprava
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
nejspíš	nejspíš	k9	nejspíš
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
měsíce	měsíc	k1gInSc2	měsíc
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
lakonických	lakonický	k2eAgInPc2d1	lakonický
textů	text	k1gInPc2	text
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dozvědět	dozvědět	k5eAaPmF	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
husité	husita	k1gMnPc1	husita
odrazili	odrazit	k5eAaPmAgMnP	odrazit
výpad	výpad	k1gInSc4	výpad
jihlavanů	jihlavan	k1gMnPc2	jihlavan
a	a	k8xC	a
Žižka	Žižka	k1gMnSc1	Žižka
je	být	k5eAaImIp3nS	být
bil	bít	k5eAaImAgMnS	bít
a	a	k8xC	a
pronásledoval	pronásledovat	k5eAaImAgInS	pronásledovat
až	až	k9	až
k	k	k7c3	k
příkopu	příkop	k1gInSc3	příkop
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
19.	[number]	k4	19.
září	září	k1gNnSc2	září
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
Telč	Telč	k1gFnSc4	Telč
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
letopisy	letopis	k1gInPc1	letopis
posléze	posléze	k6eAd1	posléze
velmi	velmi	k6eAd1	velmi
podrobně	podrobně	k6eAd1	podrobně
popisují	popisovat	k5eAaImIp3nP	popisovat
Žižkovo	Žižkův	k2eAgNnSc4d1	Žižkovo
tažení	tažení	k1gNnSc4	tažení
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přední	přední	k2eAgMnPc1d1	přední
odborníci	odborník	k1gMnPc1	odborník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
husitských	husitský	k2eAgFnPc2d1	husitská
dějin	dějiny	k1gFnPc2	dějiny
kladou	klást	k5eAaImIp3nP	klást
vznik	vznik	k1gInSc4	vznik
tohoto	tento	k3xDgInSc2	tento
záznamu	záznam	k1gInSc2	záznam
do	do	k7c2	do
období	období	k1gNnSc2	období
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1525	[number]	k4	1525
a	a	k8xC	a
shodují	shodovat	k5eAaImIp3nP	shodovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
vybájená	vybájený	k2eAgFnSc1d1	vybájená
nebo	nebo	k8xC	nebo
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
popisuje	popisovat	k5eAaImIp3nS	popisovat
tažení	tažení	k1gNnSc1	tažení
Sirotků	Sirotek	k1gMnPc2	Sirotek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1431	[number]	k4	1431
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
činnosti	činnost	k1gFnSc6	činnost
slepého	slepý	k2eAgMnSc2d1	slepý
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
roku	rok	k1gInSc2	rok
opět	opět	k6eAd1	opět
chybí	chybit	k5eAaPmIp3nS	chybit
jakékoliv	jakýkoliv	k3yIgInPc1	jakýkoliv
záznamy	záznam	k1gInPc1	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
pouze	pouze	k6eAd1	pouze
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
mu	on	k3xPp3gMnSc3	on
Hradečtí	Hradecký	k1gMnPc1	Hradecký
dávají	dávat	k5eAaImIp3nP	dávat
výstrahu	výstraha	k1gFnSc4	výstraha
před	před	k7c7	před
úkladným	úkladný	k2eAgMnSc7d1	úkladný
vrahem	vrah	k1gMnSc7	vrah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rok	rok	k1gInSc1	rok
1424	[number]	k4	1424
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Žižkovo	Žižkův	k2eAgNnSc4d1	Žižkovo
jméno	jméno	k1gNnSc4	jméno
kroniky	kronika	k1gFnSc2	kronika
opět	opět	k6eAd1	opět
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Novém	nový	k2eAgInSc6d1	nový
roce	rok	k1gInSc6	rok
1424	[number]	k4	1424
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
záznamů	záznam	k1gInPc2	záznam
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hradeckými	Hradecká	k1gFnPc7	Hradecká
proti	proti	k7c3	proti
nejbližším	blízký	k2eAgMnPc3d3	nejbližší
katolickým	katolický	k2eAgMnPc3d1	katolický
pánům	pan	k1gMnPc3	pan
a	a	k8xC	a
nedaleko	nedaleko	k7c2	nedaleko
Jaroměře	Jaroměř	k1gFnSc2	Jaroměř
je	být	k5eAaImIp3nS	být
porazil	porazit	k5eAaPmAgMnS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Skalice	Skalice	k1gFnSc2	Skalice
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
5.	[number]	k4	5.
března	březen	k1gInSc2	březen
pokusil	pokusit	k5eAaPmAgMnS	pokusit
podrobit	podrobit	k5eAaPmF	podrobit
Hostinné	hostinný	k2eAgFnPc4d1	hostinná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gInSc1	jeho
útok	útok	k1gInSc1	útok
na	na	k7c4	na
hradby	hradba	k1gFnPc4	hradba
se	se	k3xPyFc4	se
nezdařil	zdařit	k5eNaPmAgInS	zdařit
<g/>
,	,	kIx,	,
od	od	k7c2	od
dalších	další	k2eAgFnPc2d1	další
operací	operace	k1gFnPc2	operace
proti	proti	k7c3	proti
městu	město	k1gNnSc3	město
proto	proto	k6eAd1	proto
upustil	upustit	k5eAaPmAgMnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
Žižka	Žižka	k1gMnSc1	Žižka
dobyl	dobýt	k5eAaPmAgMnS	dobýt
tvrz	tvrz	k1gFnSc4	tvrz
Mlázovice	Mlázovice	k1gFnSc2	Mlázovice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zabil	zabít	k5eAaPmAgMnS	zabít
Jana	Jan	k1gMnSc4	Jan
Černína	Černín	k1gMnSc4	Černín
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Týnce	Týnec	k1gInSc2	Týnec
údajně	údajně	k6eAd1	údajně
vlastní	vlastní	k2eAgFnSc7d1	vlastní
rukou	ruka	k1gFnSc7	ruka
usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
Matěje	Matěj	k1gMnSc4	Matěj
Lupáka	Lupák	k1gMnSc4	Lupák
<g/>
,	,	kIx,	,
blízkého	blízký	k2eAgMnSc4d1	blízký
spolupracovníka	spolupracovník	k1gMnSc4	spolupracovník
kněze	kněz	k1gMnSc2	kněz
Ambrože	Ambrož	k1gMnSc2	Ambrož
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
ke	k	k7c3	k
Smidarům	Smidar	k1gInPc3	Smidar
<g/>
,	,	kIx,	,
dobyli	dobýt	k5eAaPmAgMnP	dobýt
je	on	k3xPp3gFnPc4	on
a	a	k8xC	a
vypálili	vypálit	k5eAaPmAgMnP	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
měsíce	měsíc	k1gInSc2	měsíc
května	květen	k1gInSc2	květen
proti	proti	k7c3	proti
Žižkovi	Žižka	k1gMnSc3	Žižka
vytáhla	vytáhnout	k5eAaPmAgFnS	vytáhnout
vojska	vojsko	k1gNnPc1	vojsko
svatohavelské	svatohavelský	k2eAgFnSc2d1	Svatohavelská
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
usmířením	usmíření	k1gNnSc7	usmíření
katolického	katolický	k2eAgMnSc2d1	katolický
a	a	k8xC	a
kališnického	kališnický	k2eAgNnSc2d1	kališnické
panstva	panstvo	k1gNnSc2	panstvo
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Slepý	slepý	k2eAgMnSc1d1	slepý
hejtman	hejtman	k1gMnSc1	hejtman
se	se	k3xPyFc4	se
opevnil	opevnit	k5eAaPmAgMnS	opevnit
v	v	k7c6	v
Kostelci	Kostelec	k1gInSc6	Kostelec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
obklíčen	obklíčit	k5eAaPmNgInS	obklíčit
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
ústupovou	ústupový	k2eAgFnSc4d1	ústupová
cestu	cesta	k1gFnSc4	cesta
mu	on	k3xPp3gMnSc3	on
blokovalo	blokovat	k5eAaImAgNnS	blokovat
Labe	Labe	k1gNnSc4	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Obležení	obležení	k1gNnSc1	obležení
trvalo	trvat	k5eAaImAgNnS	trvat
snad	snad	k9	snad
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
než	než	k8xS	než
jej	on	k3xPp3gInSc4	on
ze	z	k7c2	z
svízelného	svízelný	k2eAgNnSc2d1	svízelné
postavení	postavení	k1gNnSc2	postavení
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
vyprostil	vyprostit	k5eAaPmAgMnS	vyprostit
Hynek	Hynek	k1gMnSc1	Hynek
Boček	Boček	k1gMnSc1	Boček
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Pražané	Pražan	k1gMnPc1	Pražan
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
spojenci	spojenec	k1gMnPc1	spojenec
však	však	k9	však
v	v	k7c6	v
pronásledování	pronásledování	k1gNnSc6	pronásledování
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
a	a	k8xC	a
7.	[number]	k4	7.
června	červen	k1gInSc2	červen
Žižkovo	Žižkův	k2eAgNnSc4d1	Žižkovo
vojsko	vojsko	k1gNnSc4	vojsko
dostihli	dostihnout	k5eAaPmAgMnP	dostihnout
u	u	k7c2	u
Malešova	Malešův	k2eAgInSc2d1	Malešův
poblíž	poblíž	k7c2	poblíž
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Bitvu	bitva	k1gFnSc4	bitva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
strhla	strhnout	k5eAaPmAgFnS	strhnout
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
však	však	k9	však
slepý	slepý	k2eAgMnSc1d1	slepý
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
proměnil	proměnit	k5eAaPmAgMnS	proměnit
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
vojenský	vojenský	k2eAgInSc4d1	vojenský
triumf	triumf	k1gInSc4	triumf
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
do	do	k7c2	do
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
ji	on	k3xPp3gFnSc4	on
zcela	zcela	k6eAd1	zcela
vypálil	vypálit	k5eAaPmAgMnS	vypálit
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Kroniky	kronika	k1gFnSc2	kronika
starého	starý	k2eAgMnSc2d1	starý
pražského	pražský	k2eAgMnSc2d1	pražský
kolegiáta	kolegiát	k1gMnSc2	kolegiát
se	se	k3xPyFc4	se
prý	prý	k9	prý
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
Žižkovi	Žižka	k1gMnSc3	Žižka
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
vzdala	vzdát	k5eAaPmAgNnP	vzdát
i	i	k9	i
další	další	k2eAgNnPc1d1	další
města	město	k1gNnPc1	město
pražského	pražský	k2eAgInSc2d1	pražský
svazu	svaz	k1gInSc2	svaz
–	–	k?	–
Kouřim	Kouřim	k1gFnSc1	Kouřim
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Brod	Brod	k1gInSc1	Brod
a	a	k8xC	a
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
u	u	k7c2	u
Malešova	Malešův	k2eAgNnSc2d1	Malešův
se	se	k3xPyFc4	se
Žižka	Žižka	k1gMnSc1	Žižka
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
přesunul	přesunout	k5eAaPmAgMnS	přesunout
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vypálil	vypálit	k5eAaPmAgMnS	vypálit
mnoho	mnoho	k4c4	mnoho
vsí	ves	k1gFnPc2	ves
a	a	k8xC	a
městeček	městečko	k1gNnPc2	městečko
a	a	k8xC	a
v	v	k7c6	v
turnovském	turnovský	k2eAgInSc6d1	turnovský
klášteře	klášter	k1gInSc6	klášter
upálil	upálit	k5eAaPmAgMnS	upálit
mnichy	mnich	k1gMnPc4	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
se	se	k3xPyFc4	se
úmyslně	úmyslně	k6eAd1	úmyslně
vyhýbal	vyhýbat	k5eAaImAgMnS	vyhýbat
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
větší	veliký	k2eAgNnPc4d2	veliký
opevněná	opevněný	k2eAgNnPc4d1	opevněné
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
shromažďoval	shromažďovat	k5eAaImAgMnS	shromažďovat
své	svůj	k3xOyFgMnPc4	svůj
straníky	straník	k1gMnPc4	straník
k	k	k7c3	k
tažení	tažení	k1gNnSc3	tažení
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c4	před
hradby	hradba	k1gFnPc4	hradba
metropole	metropol	k1gFnSc2	metropol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
nacházel	nacházet	k5eAaImAgMnS	nacházet
i	i	k9	i
litevský	litevský	k2eAgMnSc1d1	litevský
kníže	kníže	k1gMnSc1	kníže
Korybutovič	Korybutovič	k1gMnSc1	Korybutovič
<g/>
,	,	kIx,	,
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
husity	husita	k1gMnPc7	husita
z	z	k7c2	z
Žatce	Žatec	k1gInSc2	Žatec
<g/>
,	,	kIx,	,
Loun	Louny	k1gInPc2	Louny
<g/>
,	,	kIx,	,
Klatov	Klatovy	k1gInPc2	Klatovy
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
měst	město	k1gNnPc2	město
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
měsíce	měsíc	k1gInSc2	měsíc
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
tábor	tábor	k1gInSc4	tábor
rozložili	rozložit	k5eAaPmAgMnP	rozložit
u	u	k7c2	u
Libně	Libeň	k1gFnSc2	Libeň
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
vrcholné	vrcholný	k2eAgFnSc6d1	vrcholná
fázi	fáze	k1gFnSc6	fáze
Žižkova	Žižkov	k1gInSc2	Žižkov
válečného	válečný	k2eAgNnSc2d1	válečné
a	a	k8xC	a
politického	politický	k2eAgNnSc2d1	politické
úsilí	úsilí	k1gNnSc2	úsilí
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
frontě	fronta	k1gFnSc6	fronta
se	se	k3xPyFc4	se
podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
zprávy	zpráva	k1gFnPc1	zpráva
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgMnSc1d1	jediný
ze	z	k7c2	z
starých	starý	k2eAgMnPc2d1	starý
letopisců	letopisec	k1gMnPc2	letopisec
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
v	v	k7c4	v
den	den	k1gInSc4	den
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
usmíření	usmíření	k1gNnSc3	usmíření
mezi	mezi	k7c7	mezi
knížetem	kníže	k1gMnSc7	kníže
Zikmundem	Zikmund	k1gMnSc7	Zikmund
a	a	k8xC	a
Pražany	Pražan	k1gMnPc4	Pražan
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
Žižkou	Žižka	k1gMnSc7	Žižka
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
měl	mít	k5eAaImAgMnS	mít
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
prohlásit	prohlásit	k5eAaPmF	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ten	ten	k3xDgInSc1	ten
mír	mír	k1gInSc1	mír
bude	být	k5eAaImBp3nS	být
trvat	trvat	k5eAaImF	trvat
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
jako	jako	k8xC	jako
smír	smír	k1gInSc4	smír
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
na	na	k7c6	na
Konopišti	Konopiště	k1gNnSc6	Konopiště
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
smlouvy	smlouva	k1gFnSc2	smlouva
pak	pak	k6eAd1	pak
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
u	u	k7c2	u
sv.	sv.	kA	sv.
Ambrože	Ambrož	k1gMnSc2	Ambrož
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
navršily	navršit	k5eAaPmAgFnP	navršit
hromadu	hromadu	k6eAd1	hromadu
kamení	kamení	k1gNnSc4	kamení
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgMnSc1d1	starý
kolegiát	kolegiát	k1gMnSc1	kolegiát
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
smíru	smír	k1gInSc3	smír
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
vysoký	vysoký	k2eAgInSc4d1	vysoký
základ	základ	k1gInSc4	základ
14 000	[number]	k4	14 000
kop	kop	k1gInSc4	kop
"	"	kIx"	"
<g/>
drobných	drobný	k2eAgInPc2d1	drobný
<g/>
"	"	kIx"	"
grošů	groš	k1gInPc2	groš
a	a	k8xC	a
že	že	k8xS	že
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
toho	ten	k3xDgNnSc2	ten
snesli	snést	k5eAaPmAgMnP	snést
velkou	velký	k2eAgFnSc4d1	velká
hromadu	hromada	k1gFnSc4	hromada
kamení	kamení	k1gNnSc2	kamení
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
špitálském	špitálský	k2eAgNnSc6d1	Špitálské
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
Enea	Enea	k1gMnSc1	Enea
Silvio	Silvio	k1gMnSc1	Silvio
Piccolomini	Piccolomin	k2eAgMnPc1d1	Piccolomin
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
samým	samý	k3xTgNnSc7	samý
započetím	započetí	k1gNnSc7	započetí
boje	boj	k1gInSc2	boj
vyjel	vyjet	k5eAaPmAgMnS	vyjet
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
měšťanů	měšťan	k1gMnPc2	měšťan
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
kněz	kněz	k1gMnSc1	kněz
Jan	Jan	k1gMnSc1	Jan
Rokycana	Rokycana	k1gFnSc1	Rokycana
a	a	k8xC	a
odebrav	odebrat	k5eAaPmDgInS	odebrat
se	se	k3xPyFc4	se
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
naklonil	naklonit	k5eAaPmAgMnS	naklonit
Žižku	Žižka	k1gMnSc4	Žižka
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
větě	věta	k1gFnSc3	věta
však	však	k9	však
předchází	předcházet	k5eAaImIp3nS	předcházet
poměrně	poměrně	k6eAd1	poměrně
obsáhlý	obsáhlý	k2eAgInSc1d1	obsáhlý
odstavec	odstavec	k1gInSc1	odstavec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
italský	italský	k2eAgMnSc1d1	italský
humanista	humanista	k1gMnSc1	humanista
líčí	líčit	k5eAaImIp3nS	líčit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byli	být	k5eAaImAgMnP	být
mnozí	mnohý	k2eAgMnPc1d1	mnohý
i	i	k9	i
ve	v	k7c6	v
městě	město	k1gNnSc6	město
i	i	k8xC	i
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
vojsku	vojsko	k1gNnSc6	vojsko
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zatracovali	zatracovat	k5eAaImAgMnP	zatracovat
tuto	tento	k3xDgFnSc4	tento
roztržku	roztržka	k1gFnSc4	roztržka
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedni	jeden	k4xCgMnPc1	jeden
kladli	klást	k5eAaImAgMnP	klást
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
vinu	vina	k1gFnSc4	vina
Žižkovi	Žižkův	k2eAgMnPc1d1	Žižkův
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
pražanům	pražan	k1gMnPc3	pražan
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
bouřilo	bouřit	k5eAaImAgNnS	bouřit
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
<g/>
,	,	kIx,	,
říkajíc	říkat	k5eAaImSgNnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nesluší	slušet	k5eNaImIp3nS	slušet
obléhati	obléhat	k5eAaImF	obléhat
to	ten	k3xDgNnSc4	ten
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
jest	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
království	království	k1gNnSc2	království
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
brzy	brzy	k6eAd1	brzy
upadne	upadnout	k5eAaPmIp3nS	upadnout
moc	moc	k6eAd1	moc
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
nepřátelé	nepřítel	k1gMnPc1	nepřítel
poznají	poznat	k5eAaPmIp3nP	poznat
jejich	jejich	k3xOp3gNnSc4	jejich
rozdvojení	rozdvojení	k1gNnSc4	rozdvojení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sotva	sotva	k6eAd1	sotva
se	se	k3xPyFc4	se
slepý	slepý	k2eAgMnSc1d1	slepý
hejtman	hejtman	k1gMnSc1	hejtman
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
čem	co	k3yInSc6	co
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
vojsku	vojsko	k1gNnSc6	vojsko
otevřeně	otevřeně	k6eAd1	otevřeně
hovoří	hovořit	k5eAaImIp3nS	hovořit
<g/>
,	,	kIx,	,
učinil	učinit	k5eAaPmAgMnS	učinit
prý	prý	k9	prý
veřejné	veřejný	k2eAgNnSc4d1	veřejné
prohlášení	prohlášení	k1gNnSc4	prohlášení
a	a	k8xC	a
naprosto	naprosto	k6eAd1	naprosto
změnil	změnit	k5eAaPmAgInS	změnit
smýšlení	smýšlení	k1gNnSc4	smýšlení
svého	svůj	k3xOyFgInSc2	svůj
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Smír	smír	k1gInSc1	smír
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
tábory	tábor	k1gInPc7	tábor
<g/>
,	,	kIx,	,
sjednaný	sjednaný	k2eAgInSc1d1	sjednaný
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Jana	Jan	k1gMnSc2	Jan
Rokycany	Rokycany	k1gInPc4	Rokycany
<g/>
,	,	kIx,	,
husity	husita	k1gMnPc4	husita
opět	opět	k6eAd1	opět
stmelil	stmelit	k5eAaPmAgInS	stmelit
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
pak	pak	k6eAd1	pak
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
dohodě	dohoda	k1gFnSc3	dohoda
o	o	k7c6	o
společné	společný	k2eAgFnSc6d1	společná
výpravě	výprava	k1gFnSc6	výprava
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
operovala	operovat	k5eAaImAgNnP	operovat
vojska	vojsko	k1gNnPc1	vojsko
vévody	vévoda	k1gMnSc2	vévoda
Albrechta	Albrecht	k1gMnSc2	Albrecht
<g/>
,	,	kIx,	,
zetě	zeť	k1gMnSc2	zeť
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Smrt	smrt	k1gFnSc1	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Husitské	husitský	k2eAgNnSc1d1	husitské
vojsko	vojsko	k1gNnSc1	vojsko
vyrazilo	vyrazit	k5eAaPmAgNnS	vyrazit
na	na	k7c4	na
moravskou	moravský	k2eAgFnSc4d1	Moravská
výpravu	výprava	k1gFnSc4	výprava
23.	[number]	k4	23.
září	září	k1gNnSc2	září
1424	[number]	k4	1424
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
odřady	odřad	k1gInPc1	odřad
patrně	patrně	k6eAd1	patrně
postupovaly	postupovat	k5eAaImAgInP	postupovat
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Žižka	Žižka	k1gMnSc1	Žižka
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tažení	tažení	k1gNnSc2	tažení
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
bylo	být	k5eAaImAgNnS	být
obležení	obležení	k1gNnSc1	obležení
hradu	hrad	k1gInSc2	hrad
u	u	k7c2	u
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
11.	[number]	k4	11.
října	říjen	k1gInSc2	říjen
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
následkům	následek	k1gInPc3	následek
nedostatečně	dostatečně	k6eNd1	dostatečně
léčeného	léčený	k2eAgNnSc2d1	léčené
"	"	kIx"	"
<g/>
hlíznatého	hlíznatý	k2eAgNnSc2d1	hlíznatý
<g/>
"	"	kIx"	"
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
historických	historický	k2eAgInPc2d1	historický
záznamů	záznam	k1gInPc2	záznam
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
slepý	slepý	k2eAgMnSc1d1	slepý
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
hlízy	hlíza	k1gFnSc2	hlíza
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
symptom	symptom	k1gInSc4	symptom
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
formou	forma	k1gFnSc7	forma
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
hlízy	hlíza	k1gFnPc1	hlíza
tvoří	tvořit	k5eAaImIp3nP	tvořit
zejména	zejména	k9	zejména
v	v	k7c6	v
tříslech	tříslo	k1gNnPc6	tříslo
a	a	k8xC	a
podpaží	podpažit	k5eAaPmIp3nP	podpažit
(	(	kIx(	(
<g/>
z	z	k7c2	z
lékařského	lékařský	k2eAgNnSc2d1	lékařské
hlediska	hledisko	k1gNnSc2	hledisko
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zduření	zduření	k1gNnSc4	zduření
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
zánětlivém	zánětlivý	k2eAgInSc6d1	zánětlivý
procesu	proces	k1gInSc6	proces
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
tuto	tento	k3xDgFnSc4	tento
spojitost	spojitost	k1gFnSc4	spojitost
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
faktů	fakt	k1gInPc2	fakt
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc4	mor
byla	být	k5eAaImAgFnS	být
nemoc	nemoc	k1gFnSc1	nemoc
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
nakažlivostí	nakažlivost	k1gFnSc7	nakažlivost
a	a	k8xC	a
kronikáři	kronikář	k1gMnPc1	kronikář
příchod	příchod	k1gInSc1	příchod
epidemie	epidemie	k1gFnSc2	epidemie
tradičně	tradičně	k6eAd1	tradičně
zaznamenávali	zaznamenávat	k5eAaImAgMnP	zaznamenávat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1424	[number]	k4	1424
neexistuje	existovat	k5eNaImIp3nS	existovat
jediný	jediný	k2eAgInSc1d1	jediný
záznam	záznam	k1gInSc1	záznam
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nS	by
dosvědčoval	dosvědčovat	k5eAaImAgInS	dosvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
morová	morový	k2eAgFnSc1d1	morová
nákaza	nákaza	k1gFnSc1	nákaza
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
<g/>
;	;	kIx,	;
Staré	Staré	k2eAgInPc1d1	Staré
letopisy	letopis	k1gInPc1	letopis
ji	on	k3xPp3gFnSc4	on
dokládají	dokládat	k5eAaImIp3nP	dokládat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
nastávajícím	nastávající	k2eAgInSc6d1	nastávající
<g/>
.	.	kIx.	.
</s>
<s>
Písemné	písemný	k2eAgInPc1d1	písemný
prameny	pramen	k1gInPc1	pramen
dále	daleko	k6eAd2	daleko
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
lože	lože	k1gNnSc2	lože
umírajícího	umírající	k1gMnSc2	umírající
se	se	k3xPyFc4	se
nacházeli	nacházet	k5eAaImAgMnP	nacházet
Žižkovi	Žižkův	k2eAgMnPc1d1	Žižkův
spojenci	spojenec	k1gMnPc1	spojenec
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
významnými	významný	k2eAgFnPc7d1	významná
osobnostmi	osobnost	k1gFnPc7	osobnost
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
z	z	k7c2	z
epidemiologického	epidemiologický	k2eAgNnSc2d1	epidemiologické
hlediska	hledisko	k1gNnSc2	hledisko
není	být	k5eNaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
podobné	podobný	k2eAgNnSc1d1	podobné
onemocnění	onemocnění	k1gNnSc1	onemocnění
uprostřed	uprostřed	k7c2	uprostřed
celého	celý	k2eAgNnSc2d1	celé
vojska	vojsko	k1gNnSc2	vojsko
týkalo	týkat	k5eAaImAgNnS	týkat
pouze	pouze	k6eAd1	pouze
jediné	jediný	k2eAgFnSc2d1	jediná
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
přítomných	přítomný	k1gMnPc2	přítomný
u	u	k7c2	u
Žižkova	Žižkov	k1gInSc2	Žižkov
skonu	skon	k1gInSc2	skon
později	pozdě	k6eAd2	pozdě
neonemocněl	onemocnět	k5eNaPmAgInS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
alternativou	alternativa	k1gFnSc7	alternativa
vojevůdcova	vojevůdcův	k2eAgNnSc2d1	vojevůdcův
úmrtí	úmrtí	k1gNnSc2	úmrtí
byla	být	k5eAaImAgFnS	být
otrava	otrava	k1gFnSc1	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Hovořilo	hovořit	k5eAaImAgNnS	hovořit
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c6	o
arseniku	arsenik	k1gInSc6	arsenik
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
polním	polní	k2eAgNnSc6d1	polní
tažení	tažení	k1gNnSc6	tažení
všichni	všechen	k3xTgMnPc1	všechen
jedli	jíst	k5eAaImAgMnP	jíst
totéž	týž	k3xTgNnSc4	týž
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
otrava	otrava	k1gFnSc1	otrava
týkala	týkat	k5eAaImAgFnS	týkat
opět	opět	k6eAd1	opět
pouze	pouze	k6eAd1	pouze
jediného	jediný	k2eAgMnSc4d1	jediný
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
byl	být	k5eAaImAgInS	být
zjišťován	zjišťovat	k5eAaImNgInS	zjišťovat
obsah	obsah	k1gInSc1	obsah
arsenu	arsen	k1gInSc2	arsen
i	i	k9	i
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
tkáni	tkáň	k1gFnSc6	tkáň
čáslavské	čáslavský	k2eAgFnSc2d1	čáslavská
kalvy	kalva	k1gFnSc2	kalva
<g/>
.	.	kIx.	.
</s>
<s>
Stanovení	stanovení	k1gNnSc1	stanovení
látky	látka	k1gFnSc2	látka
bylo	být	k5eAaImAgNnS	být
prováděno	provádět	k5eAaImNgNnS	provádět
metodou	metoda	k1gFnSc7	metoda
atomové	atomový	k2eAgFnSc2d1	atomová
absorpční	absorpční	k2eAgFnSc2d1	absorpční
fotometrie	fotometrie	k1gFnSc2	fotometrie
s	s	k7c7	s
generováním	generování	k1gNnSc7	generování
hydridů	hydrid	k1gInPc2	hydrid
a	a	k8xC	a
výsledek	výsledek	k1gInSc1	výsledek
vyšetření	vyšetření	k1gNnSc2	vyšetření
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
kosterního	kosterní	k2eAgInSc2d1	kosterní
pozůstatku	pozůstatek	k1gInSc2	pozůstatek
otravu	otrava	k1gFnSc4	otrava
arsenem	arsen	k1gInSc7	arsen
vyloučil	vyloučit	k5eAaPmAgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
odborníci	odborník	k1gMnPc1	odborník
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
medicíny	medicína	k1gFnSc2	medicína
převážně	převážně	k6eAd1	převážně
ztotožňují	ztotožňovat	k5eAaImIp3nP	ztotožňovat
s	s	k7c7	s
výkladem	výklad	k1gInSc7	výklad
profesora	profesor	k1gMnSc2	profesor
Josefa	Josef	k1gMnSc2	Josef
Thomayera	Thomayer	k1gMnSc2	Thomayer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
hlíza	hlíza	k1gFnSc1	hlíza
<g/>
"	"	kIx"	"
chápal	chápat	k5eAaImAgInS	chápat
jako	jako	k9	jako
nežit	nežit	k1gInSc1	nežit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
oblasti	oblast	k1gFnSc6	oblast
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
více	hodně	k6eAd2	hodně
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
nežit	nežit	k1gInSc4	nežit
až	až	k9	až
velikosti	velikost	k1gFnSc2	velikost
pěsti	pěst	k1gFnSc2	pěst
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
karbunkl	karbunkl	k1gInSc1	karbunkl
<g/>
.	.	kIx.	.
</s>
<s>
Léčení	léčení	k1gNnSc1	léčení
takového	takový	k3xDgInSc2	takový
zánětlivého	zánětlivý	k2eAgInSc2d1	zánětlivý
procesu	proces	k1gInSc2	proces
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
chirurgické	chirurgický	k2eAgNnSc4d1	chirurgické
ošetření	ošetření	k1gNnSc4	ošetření
a	a	k8xC	a
podání	podání	k1gNnSc4	podání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Thomayer	Thomayer	k1gMnSc1	Thomayer
svou	svůj	k3xOyFgFnSc4	svůj
úvahu	úvaha	k1gFnSc4	úvaha
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
s	s	k7c7	s
jistou	jistý	k2eAgFnSc7d1	jistá
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
celkovou	celkový	k2eAgFnSc4d1	celková
sepsi	sepse	k1gFnSc4	sepse
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
po	po	k7c6	po
nezvládnutelném	zvládnutelný	k2eNgInSc6d1	nezvládnutelný
karbunklu	karbunkl	k1gInSc6	karbunkl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Příbuzenstvo	příbuzenstvo	k1gNnSc1	příbuzenstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
výše	výše	k1gFnSc2	výše
citovaného	citovaný	k2eAgInSc2d1	citovaný
záznamu	záznam	k1gInSc2	záznam
z	z	k7c2	z
nekrologia	nekrologium	k1gNnSc2	nekrologium
krumlovského	krumlovský	k2eAgInSc2d1	krumlovský
konventu	konvent	k1gInSc2	konvent
minoritů	minorita	k1gMnPc2	minorita
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Žižkovi	Žižkův	k2eAgMnPc1d1	Žižkův
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
jmenovali	jmenovat	k5eAaImAgMnP	jmenovat
Řehoř	Řehoř	k1gMnSc1	Řehoř
a	a	k8xC	a
Johana	Johana	k1gFnSc1	Johana
a	a	k8xC	a
že	že	k8xS	že
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
pojal	pojmout	k5eAaPmAgMnS	pojmout
za	za	k7c2	za
manželky	manželka	k1gFnSc2	manželka
dvě	dva	k4xCgFnPc4	dva
ženy	žena	k1gFnPc1	žena
s	s	k7c7	s
totožným	totožný	k2eAgNnSc7d1	totožné
jménem	jméno	k1gNnSc7	jméno
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
měl	mít	k5eAaImAgMnS	mít
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
jsou	být	k5eAaImIp3nP	být
doloženi	doložen	k2eAgMnPc1d1	doložen
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spravoval	spravovat	k5eAaImAgMnS	spravovat
hrad	hrad	k1gInSc4	hrad
Kalich	kalich	k1gInSc1	kalich
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
snad	snad	k9	snad
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Anežka	Anežka	k1gFnSc1	Anežka
<g/>
.	.	kIx.	.
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
dále	daleko	k6eAd2	daleko
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Žižka	Žižka	k1gMnSc1	Žižka
měl	mít	k5eAaImAgMnS	mít
minimálně	minimálně	k6eAd1	minimálně
jednoho	jeden	k4xCgMnSc4	jeden
potomka	potomek	k1gMnSc4	potomek
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
manžela	manžel	k1gMnSc4	manžel
muže	muž	k1gMnSc4	muž
jménem	jméno	k1gNnSc7	jméno
Jindřich	Jindřich	k1gMnSc1	Jindřich
či	či	k8xC	či
Ondřej	Ondřej	k1gMnSc1	Ondřej
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
Žižkův	Žižkův	k2eAgMnSc1d1	Žižkův
zeť	zeť	k1gMnSc1	zeť
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Starých	Starých	k2eAgInPc2d1	Starých
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgInPc2d1	český
<g/>
,	,	kIx,	,
zahynul	zahynout	k5eAaPmAgMnS	zahynout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Malešova	Malešův	k2eAgNnSc2d1	Malešův
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
písemných	písemný	k2eAgInPc2d1	písemný
pramenů	pramen	k1gInPc2	pramen
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1429	[number]	k4	1429
<g/>
–	–	k?	–
<g/>
1434	[number]	k4	1434
darovali	darovat	k5eAaPmAgMnP	darovat
pražští	pražský	k2eAgMnPc1d1	pražský
novoměstští	novoměstský	k2eAgMnPc1d1	novoměstský
konšelé	konšel	k1gMnPc1	konšel
dům	dům	k1gInSc4	dům
v	v	k7c6	v
Panské	panský	k2eAgFnSc6d1	Panská
ulici	ulice	k1gFnSc6	ulice
Žižkově	Žižkův	k2eAgFnSc6d1	Žižkova
příbuzné	příbuzná	k1gFnSc2	příbuzná
jménem	jméno	k1gNnSc7	jméno
Anna	Anna	k1gFnSc1	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
tetu	teta	k1gFnSc4	teta
či	či	k8xC	či
sestřenici	sestřenice	k1gFnSc4	sestřenice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
získala	získat	k5eAaPmAgFnS	získat
dům	dům	k1gInSc4	dům
Žižkova	Žižkov	k1gInSc2	Žižkov
sestra	sestra	k1gFnSc1	sestra
Anežka	Anežka	k1gFnSc1	Anežka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Osobnost	osobnost	k1gFnSc1	osobnost
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
hodnocení	hodnocení	k1gNnSc1	hodnocení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
podrobné	podrobný	k2eAgFnPc1d1	podrobná
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
kontroverzní	kontroverzní	k2eAgFnSc7d1	kontroverzní
historickou	historický	k2eAgFnSc7d1	historická
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Pozdní	pozdní	k2eAgFnSc1d1	pozdní
kališnická	kališnický	k2eAgFnSc1d1	kališnická
tradice	tradice	k1gFnSc1	tradice
jej	on	k3xPp3gInSc4	on
vnímala	vnímat	k5eAaImAgFnS	vnímat
jako	jako	k8xS	jako
Božího	boží	k2eAgMnSc4d1	boží
rytíře	rytíř	k1gMnSc4	rytíř
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
sám	sám	k3xTgMnSc1	sám
Kristus	Kristus	k1gMnSc1	Kristus
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
zásluhy	zásluha	k1gFnSc2	zásluha
svěřil	svěřit	k5eAaPmAgInS	svěřit
klíče	klíč	k1gInPc4	klíč
od	od	k7c2	od
nebeských	nebeský	k2eAgFnPc2d1	nebeská
bran	brána	k1gFnPc2	brána
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
katolickou	katolický	k2eAgFnSc4d1	katolická
stranu	strana	k1gFnSc4	strana
byl	být	k5eAaImAgInS	být
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
pekelných	pekelný	k2eAgFnPc2d1	pekelná
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
heretikem	heretik	k1gMnSc7	heretik
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
slepota	slepota	k1gFnSc1	slepota
byla	být	k5eAaImAgFnS	být
výrazem	výraz	k1gInSc7	výraz
duchovní	duchovní	k2eAgFnSc2d1	duchovní
zaslepenosti	zaslepenost	k1gFnSc2	zaslepenost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
paradoxní	paradoxní	k2eAgNnSc1d1	paradoxní
<g/>
,	,	kIx,	,
že	že	k8xS	že
značnou	značný	k2eAgFnSc4d1	značná
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c4	v
přetrvání	přetrvání	k1gNnSc4	přetrvání
jeho	on	k3xPp3gInSc2	on
odkazu	odkaz	k1gInSc2	odkaz
sehrála	sehrát	k5eAaPmAgFnS	sehrát
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
výše	vysoce	k6eAd2	vysoce
citovaná	citovaný	k2eAgFnSc1d1	citovaná
Historie	historie	k1gFnSc1	historie
česká	český	k2eAgFnSc1d1	Česká
od	od	k7c2	od
katolického	katolický	k2eAgMnSc2d1	katolický
humanisty	humanista	k1gMnSc2	humanista
Enea	Enea	k1gMnSc1	Enea
Silvia	Silvia	k1gFnSc1	Silvia
Piccolominiho	Piccolomini	k1gMnSc2	Piccolomini
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
zaštítěné	zaštítěný	k2eAgNnSc1d1	zaštítěné
papežskou	papežský	k2eAgFnSc7d1	Papežská
autoritou	autorita	k1gFnSc7	autorita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nejen	nejen	k6eAd1	nejen
opakovaných	opakovaný	k2eAgNnPc2d1	opakované
vydání	vydání	k1gNnPc2	vydání
v	v	k7c6	v
originále	originál	k1gInSc6	originál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
16.	[number]	k4	16.
století	století	k1gNnSc6	století
také	také	k9	také
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
tak	tak	k9	tak
velmi	velmi	k6eAd1	velmi
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
Žižky	Žižka	k1gMnSc2	Žižka
v	v	k7c6	v
povědomí	povědomí	k1gNnSc6	povědomí
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Husitská	husitský	k2eAgFnSc1d1	husitská
kronika	kronika	k1gFnSc1	kronika
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
a	a	k8xC	a
Kronika	kronika	k1gFnSc1	kronika
velmi	velmi	k6eAd1	velmi
pěkná	pěkný	k2eAgFnSc1d1	pěkná
o	o	k7c4	o
Janu	Jana	k1gFnSc4	Jana
Žižkovi	Žižka	k1gMnSc3	Žižka
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
šířeny	šířen	k2eAgInPc1d1	šířen
jen	jen	k6eAd1	jen
méně	málo	k6eAd2	málo
účinnou	účinný	k2eAgFnSc7d1	účinná
formou	forma	k1gFnSc7	forma
opisů	opis	k1gInPc2	opis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
snad	snad	k9	snad
i	i	k9	i
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
slepý	slepý	k2eAgMnSc1d1	slepý
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
vnímán	vnímán	k2eAgMnSc1d1	vnímán
spíš	spíš	k9	spíš
negativně	negativně	k6eAd1	negativně
<g/>
;	;	kIx,	;
své	svůj	k3xOyFgNnSc4	svůj
sehrál	sehrát	k5eAaPmAgMnS	sehrát
i	i	k9	i
přetrvávající	přetrvávající	k2eAgInSc4d1	přetrvávající
odpor	odpor	k1gInSc4	odpor
ke	k	k7c3	k
krutostem	krutost	k1gFnPc3	krutost
Táborů	tábor	k1gMnPc2	tábor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
převyšoval	převyšovat	k5eAaImAgMnS	převyšovat
sympatie	sympatie	k1gFnSc2	sympatie
k	k	k7c3	k
Žižkovým	Žižkův	k2eAgNnPc3d1	Žižkovo
vítězstvím	vítězství	k1gNnPc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
památka	památka	k1gFnSc1	památka
na	na	k7c4	na
trocnovského	trocnovský	k2eAgMnSc4d1	trocnovský
rodáka	rodák	k1gMnSc4	rodák
v	v	k7c6	v
době	doba	k1gFnSc6	doba
předbělohorské	předbělohorský	k2eAgFnSc6d1	předbělohorská
a	a	k8xC	a
pobělohorské	pobělohorský	k2eAgFnSc6d1	pobělohorská
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
tradici	tradice	k1gFnSc6	tradice
i	i	k8xC	i
pověstech	pověst	k1gFnPc6	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Místa	místo	k1gNnPc1	místo
poblíž	poblíž	k7c2	poblíž
trasy	trasa	k1gFnSc2	trasa
pochodu	pochod	k1gInSc2	pochod
jeho	jeho	k3xOp3gNnPc2	jeho
vojsk	vojsko	k1gNnPc2	vojsko
často	často	k6eAd1	často
dostávala	dostávat	k5eAaImAgNnP	dostávat
jména	jméno	k1gNnPc1	jméno
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
kde	kde	k6eAd1	kde
obědval	obědvat	k5eAaImAgMnS	obědvat
(	(	kIx(	(
<g/>
Žižkovy	Žižkův	k2eAgInPc4d1	Žižkův
stoly	stol	k1gInPc4	stol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tábořil	tábořit	k5eAaImAgMnS	tábořit
(	(	kIx(	(
<g/>
Žižkovy	Žižkův	k2eAgFnPc1d1	Žižkova
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
kde	kde	k6eAd1	kde
stávaly	stávat	k5eAaImAgFnP	stávat
význačné	význačný	k2eAgInPc4d1	význačný
stromy	strom	k1gInPc4	strom
pod	pod	k7c7	pod
nimiž	jenž	k3xRgFnPc7	jenž
prý	prý	k9	prý
odpočíval	odpočívat	k5eAaImAgInS	odpočívat
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
zasadil	zasadit	k5eAaPmAgMnS	zasadit
(	(	kIx(	(
<g/>
Žižkovy	Žižkův	k2eAgInPc4d1	Žižkův
duby	dub	k1gInPc4	dub
<g/>
,	,	kIx,	,
Žižkovy	Žižkův	k2eAgFnPc4d1	Žižkova
lípy	lípa	k1gFnPc4	lípa
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
lidový	lidový	k2eAgMnSc1d1	lidový
hrdina	hrdina	k1gMnSc1	hrdina
dokonce	dokonce	k9	dokonce
dostal	dostat	k5eAaPmAgMnS	dostat
čestné	čestný	k2eAgNnSc4d1	čestné
místo	místo	k1gNnSc4	místo
po	po	k7c6	po
boku	bok	k1gInSc6	bok
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
vyčkávajícího	vyčkávající	k2eAgMnSc2d1	vyčkávající
v	v	k7c6	v
hoře	hora	k1gFnSc6	hora
Blaník	Blaník	k1gInSc1	Blaník
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
v	v	k7c6	v
historických	historický	k2eAgNnPc6d1	historické
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
Silviovy	Silviův	k2eAgFnSc2d1	Silviův
práce	práce	k1gFnSc2	práce
dočkal	dočkat	k5eAaPmAgMnS	dočkat
s	s	k7c7	s
delším	dlouhý	k2eAgInSc7d2	delší
časovým	časový	k2eAgInSc7d1	časový
odstupem	odstup	k1gInSc7	odstup
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
autorů	autor	k1gMnPc2	autor
Starých	Starých	k2eAgInPc2d1	Starých
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgMnPc2d1	český
lze	lze	k6eAd1	lze
mezi	mezi	k7c7	mezi
významnějšími	významný	k2eAgMnPc7d2	významnější
jmenovat	jmenovat	k5eAaImF	jmenovat
Václava	Václav	k1gMnSc4	Václav
Hájka	Hájek	k1gMnSc4	Hájek
z	z	k7c2	z
Libočan	Libočan	k1gMnSc1	Libočan
(	(	kIx(	(
<g/>
Kronika	kronika	k1gFnSc1	kronika
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
1541	[number]	k4	1541
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Dubravia	Dubravius	k1gMnSc2	Dubravius
(	(	kIx(	(
<g/>
1552	[number]	k4	1552
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zachariáše	Zachariáš	k1gMnSc2	Zachariáš
Theobalda	Theobald	k1gMnSc2	Theobald
(	(	kIx(	(
<g/>
Husitenkrieg	Husitenkrieg	k1gInSc1	Husitenkrieg
1621	[number]	k4	1621
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Dačického	dačický	k2eAgMnSc2d1	dačický
(	(	kIx(	(
<g/>
Paměti	paměť	k1gFnSc2	paměť
1628	[number]	k4	1628
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pavla	Pavel	k1gMnSc2	Pavel
Stránského	Stránský	k1gMnSc2	Stránský
(	(	kIx(	(
<g/>
1634	[number]	k4	1634
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc4	Jan
Kořínka	Kořínek	k1gMnSc4	Kořínek
(	(	kIx(	(
<g/>
Paměti	paměť	k1gFnSc2	paměť
kutnohorské	kutnohorský	k2eAgFnSc2d1	Kutnohorská
1675	[number]	k4	1675
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Františka	Františka	k1gFnSc1	Františka
Beckovského	Beckovský	k2eAgInSc2d1	Beckovský
(	(	kIx(	(
<g/>
1700	[number]	k4	1700
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
svým	svůj	k3xOyFgMnPc3	svůj
čtenářům	čtenář	k1gMnPc3	čtenář
Žižkův	Žižkův	k2eAgInSc4d1	Žižkův
obraz	obraz	k1gInSc4	obraz
zpravidla	zpravidla	k6eAd1	zpravidla
líčili	líčit	k5eAaImAgMnP	líčit
podle	podle	k7c2	podle
momentální	momentální	k2eAgFnSc2d1	momentální
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
vlastní	vlastní	k2eAgFnSc2d1	vlastní
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
slepý	slepý	k2eAgMnSc1d1	slepý
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
spojován	spojovat	k5eAaImNgMnS	spojovat
s	s	k7c7	s
radikální	radikální	k2eAgFnSc7d1	radikální
českou	český	k2eAgFnSc7d1	Česká
reformací	reformace	k1gFnSc7	reformace
a	a	k8xC	a
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
lidového	lidový	k2eAgMnSc4d1	lidový
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1539	[number]	k4	1539
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Kronice	kronika	k1gFnSc6	kronika
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
země	zem	k1gFnSc2	zem
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
prvních	první	k4xOgNnPc6	první
obyvatelích	obyvatel	k1gMnPc6	obyvatel
jejích	její	k3xOp3gNnPc2	její
Martin	Martin	k1gMnSc1	Martin
Kuthen	Kuthen	k1gInSc4	Kuthen
zařadil	zařadit	k5eAaPmAgMnS	zařadit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Husem	Hus	k1gMnSc7	Hus
mezi	mezi	k7c4	mezi
obrazy	obraz	k1gInPc4	obraz
českých	český	k2eAgMnPc2d1	český
knížat	kníže	k1gMnPc2wR	kníže
a	a	k8xC	a
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
moderní	moderní	k2eAgFnSc1d1	moderní
práce	práce	k1gFnSc1	práce
o	o	k7c6	o
Janu	Jan	k1gMnSc6	Jan
Žižkovi	Žižka	k1gMnSc6	Žižka
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
19.	[number]	k4	19.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyšly	vyjít	k5eAaPmAgFnP	vyjít
Dějiny	dějiny	k1gFnPc1	dějiny
národu	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
od	od	k7c2	od
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
slepého	slepý	k2eAgMnSc4d1	slepý
vojevůdce	vojevůdce	k1gMnSc4	vojevůdce
sice	sice	k8xC	sice
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
pro	pro	k7c4	pro
jeho	on	k3xPp3gMnSc4	on
válečného	válečný	k2eAgMnSc4d1	válečný
génia	génius	k1gMnSc4	génius
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítal	odmítat	k5eAaImAgMnS	odmítat
jeho	jeho	k3xOp3gInSc4	jeho
fanatismus	fanatismus	k1gInSc4	fanatismus
a	a	k8xC	a
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
pak	pak	k6eAd1	pak
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
měřítku	měřítko	k1gNnSc6	měřítko
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
vítanou	vítaný	k2eAgFnSc4d1	vítaná
a	a	k8xC	a
prospěšnou	prospěšný	k2eAgFnSc4d1	prospěšná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
od	od	k7c2	od
Palackého	Palackého	k2eAgMnSc2d1	Palackého
konzervativního	konzervativní	k2eAgMnSc2d1	konzervativní
ideového	ideový	k2eAgMnSc2d1	ideový
odpůrce	odpůrce	k1gMnSc2	odpůrce
V.	V.	kA	V.
V.	V.	kA	V.
Tomka	Tomek	k1gMnSc2	Tomek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
Žižka	Žižka	k1gMnSc1	Žižka
byl	být	k5eAaImAgMnS	být
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
vlastencem	vlastenec	k1gMnSc7	vlastenec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sice	sice	k8xC	sice
v	v	k7c6	v
náboženském	náboženský	k2eAgInSc6d1	náboženský
zápalu	zápal	k1gInSc6	zápal
neúprosně	úprosně	k6eNd1	úprosně
potíral	potírat	k5eAaImAgMnS	potírat
katolické	katolický	k2eAgNnSc4d1	katolické
duchovenstvo	duchovenstvo	k1gNnSc4	duchovenstvo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byl	být	k5eAaImAgMnS	být
obráncem	obránce	k1gMnSc7	obránce
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
proti	proti	k7c3	proti
cizozemcům	cizozemec	k1gMnPc3	cizozemec
<g/>
,	,	kIx,	,
toužil	toužit	k5eAaImAgInS	toužit
zavést	zavést	k5eAaPmF	zavést
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
pevný	pevný	k2eAgInSc4d1	pevný
řád	řád	k1gInSc4	řád
a	a	k8xC	a
obnovit	obnovit	k5eAaPmF	obnovit
královskou	královský	k2eAgFnSc4d1	královská
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jen	jen	k6eAd1	jen
náhodným	náhodný	k2eAgMnSc7d1	náhodný
pomocníkem	pomocník	k1gMnSc7	pomocník
při	při	k7c6	při
událostech	událost	k1gFnPc6	událost
z	z	k7c2	z
30.	[number]	k4	30.
července	červenec	k1gInSc2	červenec
1419	[number]	k4	1419
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
pádu	pád	k1gInSc6	pád
Želivského	želivský	k2eAgInSc2d1	želivský
atd.	atd.	kA	atd.
Tomkův	Tomkův	k2eAgInSc1d1	Tomkův
obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
nadšeně	nadšeně	k6eAd1	nadšeně
přijat	přijmout	k5eAaPmNgInS	přijmout
většinou	většina	k1gFnSc7	většina
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
přetrval	přetrvat	k5eAaPmAgMnS	přetrvat
v	v	k7c6	v
povědomí	povědomí	k1gNnSc6	povědomí
Čechů	Čech	k1gMnPc2	Čech
po	po	k7c4	po
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	s	k7c7	s
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
prozaická	prozaický	k2eAgNnPc4d1	prozaické
díla	dílo	k1gNnPc4	dílo
závěru	závěr	k1gInSc3	závěr
19.	[number]	k4	19.
a	a	k8xC	a
následujícího	následující	k2eAgNnSc2d1	následující
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
inspirovat	inspirovat	k5eAaBmF	inspirovat
i	i	k9	i
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
románu	román	k1gInSc2	román
Proti	proti	k7c3	proti
všem	všecek	k3xTgFnPc3	všecek
a	a	k8xC	a
trilogie	trilogie	k1gFnPc4	trilogie
Mezi	mezi	k7c7	mezi
proudy	proud	k1gInPc7	proud
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jejichž	jejichž	k3xOyRp3gInPc2	jejichž
motivů	motiv	k1gInPc2	motiv
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
husitská	husitský	k2eAgFnSc1d1	husitská
revoluční	revoluční	k2eAgFnSc1d1	revoluční
trilogie	trilogie	k1gFnSc1	trilogie
režiséra	režisér	k1gMnSc2	režisér
Otakara	Otakar	k1gMnSc2	Otakar
Vávry	Vávra	k1gMnSc2	Vávra
<g/>
.	.	kIx.	.
</s>
<s>
Tomkova	Tomkův	k2eAgNnSc2d1	Tomkovo
pojetí	pojetí	k1gNnSc2	pojetí
se	se	k3xPyFc4	se
přidrželi	přidržet	k5eAaPmAgMnP	přidržet
i	i	k9	i
další	další	k2eAgMnPc1d1	další
přední	přední	k2eAgMnPc1d1	přední
historici	historik	k1gMnPc1	historik
husitské	husitský	k2eAgFnSc2d1	husitská
epochy	epocha	k1gFnSc2	epocha
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Urbánek	Urbánek	k1gMnSc1	Urbánek
a	a	k8xC	a
F.	F.	kA	F.
M.	M.	kA	M.
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
pochopitelných	pochopitelný	k2eAgInPc2d1	pochopitelný
důvodů	důvod	k1gInPc2	důvod
i	i	k8xC	i
"	"	kIx"	"
<g/>
marxistická	marxistický	k2eAgFnSc1d1	marxistická
historiografie	historiografie	k1gFnSc1	historiografie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
zjevným	zjevný	k2eAgInSc7d1	zjevný
ideologickým	ideologický	k2eAgInSc7d1	ideologický
záměrem	záměr	k1gInSc7	záměr
učinila	učinit	k5eAaImAgFnS	učinit
z	z	k7c2	z
Žižky	Žižka	k1gMnSc2	Žižka
nezištného	zištný	k2eNgMnSc2d1	nezištný
vůdce	vůdce	k1gMnSc2	vůdce
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
symbol	symbol	k1gInSc1	symbol
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
feudalismu	feudalismus	k1gInSc3	feudalismus
<g/>
,	,	kIx,	,
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
kontrarevoluci	kontrarevoluce	k1gFnSc6	kontrarevoluce
<g/>
,	,	kIx,	,
přítele	přítel	k1gMnSc4	přítel
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
bojovníka	bojovník	k1gMnSc4	bojovník
za	za	k7c4	za
lidskou	lidský	k2eAgFnSc4d1	lidská
důstojnost	důstojnost	k1gFnSc4	důstojnost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
názorem	názor	k1gInSc7	názor
blízkým	blízký	k2eAgFnPc3d1	blízká
Palackému	Palacký	k1gMnSc3	Palacký
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
letech	let	k1gInPc6	let
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
dílem	dílem	k6eAd1	dílem
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
historik	historik	k1gMnSc1	historik
Josef	Josef	k1gMnSc1	Josef
Pekař	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
považoval	považovat	k5eAaImAgMnS	považovat
Tomkovo	Tomkův	k2eAgNnSc4d1	Tomkovo
pojetí	pojetí	k1gNnSc4	pojetí
Žižkovy	Žižkův	k2eAgFnSc2d1	Žižkova
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
významu	význam	k1gInSc2	význam
za	za	k7c4	za
mylné	mylný	k2eAgNnSc4d1	mylné
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nevylučoval	vylučovat	k5eNaImAgMnS	vylučovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
člověkem	člověk	k1gMnSc7	člověk
dobrého	dobrý	k2eAgNnSc2d1	dobré
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
čeští	český	k2eAgMnPc1d1	český
historikové	historik	k1gMnPc1	historik
názorově	názorově	k6eAd1	názorově
přiklání	přiklánět	k5eAaImIp3nP	přiklánět
buď	buď	k8xC	buď
k	k	k7c3	k
Palackému	Palacký	k1gMnSc3	Palacký
a	a	k8xC	a
Pekařovi	Pekař	k1gMnSc3	Pekař
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Šmahel	Šmahel	k1gMnSc1	Šmahel
<g/>
,	,	kIx,	,
Amedeo	Amedeo	k1gMnSc1	Amedeo
Molnár	Molnár	k1gMnSc1	Molnár
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
k	k	k7c3	k
Tomkovi	Tomek	k1gMnSc3	Tomek
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Kejř	Kejř	k1gMnSc1	Kejř
<g/>
)	)	kIx)	)
a	a	k8xC	a
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
tak	tak	k6eAd1	tak
názorovou	názorový	k2eAgFnSc4d1	názorová
nejednotnost	nejednotnost	k1gFnSc4	nejednotnost
odborníků	odborník	k1gMnPc2	odborník
zapříčiněnou	zapříčiněný	k2eAgFnSc4d1	zapříčiněná
nedostatkem	nedostatek	k1gInSc7	nedostatek
písemných	písemný	k2eAgInPc2d1	písemný
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
současných	současný	k2eAgMnPc2d1	současný
historiků	historik	k1gMnPc2	historik
pak	pak	k6eAd1	pak
například	například	k6eAd1	například
profesor	profesor	k1gMnSc1	profesor
Petr	Petr	k1gMnSc1	Petr
Čornej	Čornej	k1gMnSc1	Čornej
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nejnovějších	nový	k2eAgInPc2d3	nejnovější
výzkumů	výzkum	k1gInPc2	výzkum
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
svazku	svazek	k1gInSc6	svazek
Velkých	velký	k2eAgFnPc2d1	velká
dějin	dějiny	k1gFnPc2	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgNnSc4d1	české
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
Žižkovi	Žižka	k1gMnSc6	Žižka
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
vynikajícího	vynikající	k2eAgMnSc2d1	vynikající
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
a	a	k8xC	a
vnitřně	vnitřně	k6eAd1	vnitřně
čestného	čestný	k2eAgMnSc2d1	čestný
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
zaujetí	zaujetí	k1gNnSc4	zaujetí
pro	pro	k7c4	pro
čtyři	čtyři	k4xCgInPc4	čtyři
artikuly	artikul	k1gInPc4	artikul
pražské	pražský	k2eAgFnSc2d1	Pražská
a	a	k8xC	a
vítězství	vítězství	k1gNnSc2	vítězství
božího	boží	k2eAgInSc2d1	boží
zákona	zákon	k1gInSc2	zákon
bylo	být	k5eAaImAgNnS	být
natolik	natolik	k6eAd1	natolik
upřímné	upřímný	k2eAgNnSc1d1	upřímné
a	a	k8xC	a
důsledné	důsledný	k2eAgNnSc1d1	důsledné
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastně	vlastně	k9	vlastně
vylučovalo	vylučovat	k5eAaImAgNnS	vylučovat
kompromis	kompromis	k1gInSc4	kompromis
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
skvělým	skvělý	k2eAgInSc7d1	skvělý
a	a	k8xC	a
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
velitelem	velitel	k1gMnSc7	velitel
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
věhlas	věhlas	k1gInSc1	věhlas
přerostl	přerůst	k5eAaPmAgInS	přerůst
v	v	k7c4	v
legendu	legenda	k1gFnSc4	legenda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
politik	politik	k1gMnSc1	politik
budil	budit	k5eAaImAgMnS	budit
často	často	k6eAd1	často
rozpaky	rozpak	k1gInPc4	rozpak
i	i	k8xC	i
na	na	k7c6	na
husitské	husitský	k2eAgFnSc6d1	husitská
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
jeho	jeho	k3xOp3gInSc2	jeho
postoje	postoj	k1gInSc2	postoj
by	by	kYmCp3nS	by
však	však	k9	však
revoluce	revoluce	k1gFnSc1	revoluce
zdaleka	zdaleka	k6eAd1	zdaleka
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
svých	svůj	k3xOyFgMnPc2	svůj
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
komplexní	komplexní	k2eAgFnSc1d1	komplexní
struktura	struktura	k1gFnSc1	struktura
povahových	povahový	k2eAgInPc2d1	povahový
rysů	rys	k1gInPc2	rys
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
pro	pro	k7c4	pro
historiky	historik	k1gMnPc4	historik
předmětem	předmět	k1gInSc7	předmět
odborných	odborný	k2eAgInPc2d1	odborný
sporů	spor	k1gInPc2	spor
a	a	k8xC	a
spekulací	spekulace	k1gFnPc2	spekulace
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
z	z	k7c2	z
narativních	narativní	k2eAgInPc2d1	narativní
pramenů	pramen	k1gInPc2	pramen
dozvědět	dozvědět	k5eAaPmF	dozvědět
alespoň	alespoň	k9	alespoň
o	o	k7c6	o
některých	některý	k3yIgInPc6	některý
jeho	jeho	k3xOp3gInPc6	jeho
charakterových	charakterový	k2eAgInPc6d1	charakterový
atributech	atribut	k1gInPc6	atribut
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
artistické	artistický	k2eAgFnSc2d1	artistická
fakulty	fakulta	k1gFnSc2	fakulta
pražské	pražský	k2eAgFnSc2d1	Pražská
university	universita	k1gFnSc2	universita
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
znal	znát	k5eAaImAgMnS	znát
husitského	husitský	k2eAgMnSc4d1	husitský
hejtmana	hejtman	k1gMnSc4	hejtman
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
v	v	k7c6	v
Husitské	husitský	k2eAgFnSc6d1	husitská
kronice	kronika	k1gFnSc6	kronika
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
nad	nad	k7c4	nad
obyčej	obyčej	k1gInSc4	obyčej
smělý	smělý	k2eAgMnSc1d1	smělý
<g/>
,	,	kIx,	,
statečný	statečný	k2eAgMnSc1d1	statečný
a	a	k8xC	a
udatný	udatný	k2eAgMnSc1d1	udatný
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
supra	supra	k1gNnSc2	supra
modum	modum	k1gNnSc1	modum
audax	audax	k1gInSc1	audax
et	et	k?	et
strenuus	strenuus	k1gInSc1	strenuus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
"	"	kIx"	"
<g/>
nad	nad	k7c4	nad
obyčej	obyčej	k1gInSc4	obyčej
smělý	smělý	k2eAgInSc4d1	smělý
a	a	k8xC	a
udatný	udatný	k2eAgInSc4d1	udatný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Žižkovi	Žižka	k1gMnSc6	Žižka
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
přímočarého	přímočarý	k2eAgMnSc4d1	přímočarý
bojovníka	bojovník	k1gMnSc4	bojovník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nezastaví	zastavit	k5eNaPmIp3nS	zastavit
před	před	k7c7	před
ničím	nic	k3yNnSc7	nic
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
použitého	použitý	k2eAgInSc2d1	použitý
adjektivu	adjektivum	k1gNnSc6	adjektivum
strenuus	strenuus	k1gInSc1	strenuus
lze	lze	k6eAd1	lze
dále	daleko	k6eAd2	daleko
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
autor	autor	k1gMnSc1	autor
Husitské	husitský	k2eAgFnSc2d1	husitská
kroniky	kronika	k1gFnSc2	kronika
trocnovského	trocnovský	k2eAgMnSc2d1	trocnovský
rodáka	rodák	k1gMnSc2	rodák
považoval	považovat	k5eAaImAgInS	považovat
nejen	nejen	k6eAd1	nejen
za	za	k7c2	za
chrabrého	chrabrý	k2eAgNnSc2d1	chrabré
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
za	za	k7c2	za
čestného	čestný	k2eAgMnSc2d1	čestný
a	a	k8xC	a
poctivého	poctivý	k2eAgMnSc2d1	poctivý
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ve	v	k7c6	v
středověkém	středověký	k2eAgInSc6d1	středověký
chápání	chápání	k1gNnSc2	chápání
všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
přívlastky	přívlastek	k1gInPc1	přívlastek
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
pojem	pojem	k1gInSc1	pojem
statečný	statečný	k2eAgMnSc1d1	statečný
člověk	člověk	k1gMnSc1	člověk
(	(	kIx(	(
<g/>
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
Žižka	Žižka	k1gMnSc1	Žižka
čestným	čestný	k2eAgNnPc3d1	čestné
<g/>
,	,	kIx,	,
vnitřně	vnitřně	k6eAd1	vnitřně
poctivým	poctivý	k2eAgInSc7d1	poctivý
<g/>
,	,	kIx,	,
odvážným	odvážný	k2eAgInSc7d1	odvážný
<g/>
,	,	kIx,	,
statečným	statečný	k2eAgInSc7d1	statečný
<g/>
,	,	kIx,	,
hluboce	hluboko	k6eAd1	hluboko
věřícím	věřící	k1gFnPc3	věřící
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
přímočarým	přímočarý	k2eAgMnSc7d1	přímočarý
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgInSc6	ten
jak	jak	k6eAd1	jak
byl	být	k5eAaImAgMnS	být
Žižka	Žižka	k1gMnSc1	Žižka
vnímán	vnímat	k5eAaImNgMnS	vnímat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
zanechali	zanechat	k5eAaPmAgMnP	zanechat
svědectví	svědectví	k1gNnSc4	svědectví
i	i	k8xC	i
další	další	k2eAgMnPc1d1	další
literáti	literát	k1gMnPc1	literát
jak	jak	k6eAd1	jak
z	z	k7c2	z
křídla	křídlo	k1gNnSc2	křídlo
katolického	katolický	k2eAgNnSc2d1	katolické
<g/>
,	,	kIx,	,
tak	tak	k9	tak
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
kališníků	kališník	k1gMnPc2	kališník
<g/>
.	.	kIx.	.
</s>
<s>
Opat	opat	k1gMnSc1	opat
augustiniánského	augustiniánský	k2eAgInSc2d1	augustiniánský
kláštera	klášter	k1gInSc2	klášter
Ludolf	Ludolf	k1gMnSc1	Ludolf
Zaháňský	zaháňský	k2eAgMnSc1d1	zaháňský
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Žižka	Žižka	k1gMnSc1	Žižka
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
jediný	jediný	k2eAgMnSc1d1	jediný
má	mít	k5eAaImIp3nS	mít
víru	víra	k1gFnSc4	víra
pravou	pravý	k2eAgFnSc4d1	pravá
a	a	k8xC	a
neporušenou	porušený	k2eNgFnSc4d1	neporušená
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnSc4d1	ostatní
pak	pak	k6eAd1	pak
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
syny	syn	k1gMnPc7	syn
nevěry	nevěra	k1gFnSc2	nevěra
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Příbramě	Příbram	k1gFnSc2	Příbram
<g/>
,	,	kIx,	,
kritik	kritik	k1gMnSc1	kritik
válečné	válečný	k2eAgFnSc2d1	válečná
politiky	politika	k1gFnSc2	politika
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
podobném	podobný	k2eAgMnSc6d1	podobný
duchu	duch	k1gMnSc6	duch
<g/>
,	,	kIx,	,
když	když	k8xS	když
zapsal	zapsat	k5eAaPmAgMnS	zapsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slepý	slepý	k2eAgMnSc1d1	slepý
hejtman	hejtman	k1gMnSc1	hejtman
nenáviděl	návidět	k5eNaImAgMnS	návidět
některé	některý	k3yIgMnPc4	některý
táborské	táborští	k1gMnPc4	táborští
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
Korandu	Koranda	k1gFnSc4	Koranda
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
mu	on	k3xPp3gMnSc3	on
dával	dávat	k5eAaImAgMnS	dávat
za	za	k7c4	za
vinu	vina	k1gFnSc4	vina
pikarty	pikart	k1gMnPc4	pikart
a	a	k8xC	a
kacíře	kacíř	k1gMnPc4	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
historiků	historik	k1gMnPc2	historik
zabývajících	zabývající	k2eAgMnPc2d1	zabývající
se	s	k7c7	s
Žižkovým	Žižkův	k2eAgInSc7d1	Žižkův
charakterem	charakter	k1gInSc7	charakter
<g/>
,	,	kIx,	,
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slepý	slepý	k2eAgMnSc1d1	slepý
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
byl	být	k5eAaImAgMnS	být
fanatik	fanatik	k1gMnSc1	fanatik
pro	pro	k7c4	pro
pobožnost	pobožnost	k1gFnSc4	pobožnost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
hypotézou	hypotéza	k1gFnSc7	hypotéza
se	se	k3xPyFc4	se
ztotožnil	ztotožnit	k5eAaPmAgMnS	ztotožnit
i	i	k9	i
historik	historik	k1gMnSc1	historik
Josef	Josef	k1gMnSc1	Josef
Pekař	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
slovo	slovo	k1gNnSc4	slovo
fanatik	fanatik	k1gMnSc1	fanatik
nahradil	nahradit	k5eAaPmAgInS	nahradit
termínem	termín	k1gInSc7	termín
bojovník	bojovník	k1gMnSc1	bojovník
boží	božit	k5eAaImIp3nS	božit
<g/>
.	.	kIx.	.
</s>
<s>
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
dílo	dílo	k1gNnSc1	dílo
zkázy	zkáza	k1gFnSc2	zkáza
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
legitimizoval	legitimizovat	k5eAaBmAgInS	legitimizovat
příklad	příklad	k1gInSc1	příklad
Hospodina	Hospodin	k1gMnSc2	Hospodin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
nelítostným	lítostný	k2eNgMnSc7d1	nelítostný
starozákonním	starozákonní	k2eAgMnSc7d1	starozákonní
trestatelem	trestatel	k1gMnSc7	trestatel
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
významní	významný	k2eAgMnPc1d1	významný
čeští	český	k2eAgMnPc1d1	český
historici	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
však	však	k9	však
rozcházeli	rozcházet	k5eAaImAgMnP	rozcházet
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byl	být	k5eAaImAgMnS	být
Žižka	Žižka	k1gMnSc1	Žižka
demokrat	demokrat	k1gMnSc1	demokrat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
potíral	potírat	k5eAaImAgInS	potírat
rozdíly	rozdíl	k1gInPc4	rozdíl
stavů	stav	k1gInPc2	stav
a	a	k8xC	a
nenáviděl	nenávidět	k5eAaImAgMnS	nenávidět
feudální	feudální	k2eAgNnSc4d1	feudální
panstvo	panstvo	k1gNnSc4	panstvo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
že	že	k8xS	že
bojoval	bojovat	k5eAaImAgMnS	bojovat
za	za	k7c4	za
rovnost	rovnost	k1gFnSc4	rovnost
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
sice	sice	k8xC	sice
mnohdy	mnohdy	k6eAd1	mnohdy
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sedláci	sedlák	k1gMnPc1	sedlák
a	a	k8xC	a
prostý	prostý	k2eAgInSc1d1	prostý
lid	lid	k1gInSc1	lid
Žižku	Žižka	k1gMnSc4	Žižka
rádi	rád	k2eAgMnPc1d1	rád
následovali	následovat	k5eAaImAgMnP	následovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zároveň	zároveň	k6eAd1	zároveň
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
žádným	žádný	k3yNgInSc7	žádný
očividným	očividný	k2eAgInSc7d1	očividný
způsobem	způsob	k1gInSc7	způsob
nesnažil	snažit	k5eNaImAgInS	snažit
rozbít	rozbít	k5eAaPmF	rozbít
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
feudální	feudální	k2eAgNnSc4d1	feudální
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
připojil	připojit	k5eAaPmAgInS	připojit
svou	svůj	k3xOyFgFnSc4	svůj
pečeť	pečeť	k1gFnSc4	pečeť
k	k	k7c3	k
žádosti	žádost	k1gFnSc3	žádost
Pražanů	Pražan	k1gMnPc2	Pražan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nabízeli	nabízet	k5eAaImAgMnP	nabízet
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
polskému	polský	k2eAgMnSc3d1	polský
panovníkovi	panovník	k1gMnSc3	panovník
Vladislavovi	Vladislav	k1gMnSc3	Vladislav
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
litevskému	litevský	k2eAgMnSc3d1	litevský
velkoknížeti	velkokníže	k1gMnSc3	velkokníže
Vitoldovi	Vitold	k1gMnSc3	Vitold
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vojenském	vojenský	k2eAgInSc6d1	vojenský
řádu	řád	k1gInSc6	řád
oslovuje	oslovovat	k5eAaImIp3nS	oslovovat
osoby	osoba	k1gFnPc4	osoba
všech	všecek	k3xTgInPc2	všecek
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
knížata	kníže	k1gMnPc4wR	kníže
<g/>
,	,	kIx,	,
rytíře	rytíř	k1gMnPc4	rytíř
<g/>
,	,	kIx,	,
panoše	panoš	k1gMnPc4	panoš
<g/>
,	,	kIx,	,
měšťany	měšťan	k1gMnPc4	měšťan
<g/>
,	,	kIx,	,
řemeslníky	řemeslník	k1gMnPc4	řemeslník
<g/>
,	,	kIx,	,
robotěz	robotěz	k?	robotěz
i	i	k8xC	i
sedláky	sedlák	k1gInPc1	sedlák
a	a	k8xC	a
na	na	k7c6	na
stavovském	stavovský	k2eAgInSc6d1	stavovský
základě	základ	k1gInSc6	základ
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
i	i	k9	i
jeho	jeho	k3xOp3gNnSc1	jeho
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
panské	panský	k2eAgFnSc2d1	Panská
<g/>
,	,	kIx,	,
rytířské	rytířský	k2eAgFnSc2d1	rytířská
<g/>
,	,	kIx,	,
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
i	i	k9	i
selské	selský	k2eAgInPc1d1	selský
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
jeho	jeho	k3xOp3gInSc2	jeho
programu	program	k1gInSc2	program
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nebyl	být	k5eNaImAgInS	být
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
pánům	pan	k1gMnPc3	pan
jako	jako	k8xS	jako
takovým	takový	k3xDgMnPc3	takový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proti	proti	k7c3	proti
nepřátelům	nepřítel	k1gMnPc3	nepřítel
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nebo	nebo	k8xC	nebo
onomu	onen	k3xDgInSc3	onen
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bojoval	bojovat	k5eAaImAgMnS	bojovat
za	za	k7c4	za
rovnost	rovnost	k1gFnSc4	rovnost
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k8xS	jak
mu	on	k3xPp3gMnSc3	on
přisuzovali	přisuzovat	k5eAaImAgMnP	přisuzovat
mnozí	mnohý	k2eAgMnPc1d1	mnohý
historici	historik	k1gMnPc1	historik
<g/>
,	,	kIx,	,
tak	tak	k9	tak
především	především	k9	především
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
rovnost	rovnost	k1gFnSc4	rovnost
před	před	k7c7	před
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
mimo	mimo	k7c4	mimo
dobový	dobový	k2eAgInSc4d1	dobový
kontext	kontext	k1gInSc4	kontext
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
posuzovány	posuzován	k2eAgFnPc4d1	posuzována
Žižkovy	Žižkův	k2eAgFnPc4d1	Žižkova
válečné	válečný	k2eAgFnPc4d1	válečná
krutosti	krutost	k1gFnPc4	krutost
doprovázené	doprovázený	k2eAgFnPc4d1	doprovázená
obrazoborectvím	obrazoborectví	k1gNnSc7	obrazoborectví
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
barbarské	barbarský	k2eAgFnSc2d1	barbarská
metody	metoda	k1gFnSc2	metoda
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgNnSc1d1	zahrnující
vraždění	vraždění	k1gNnSc1	vraždění
<g/>
,	,	kIx,	,
pálení	pálení	k1gNnSc1	pálení
bezbranných	bezbranný	k2eAgMnPc2d1	bezbranný
po	po	k7c6	po
stovkách	stovka	k1gFnPc6	stovka
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nebyly	být	k5eNaImAgFnP	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
unikum	unikum	k1gNnSc1	unikum
Žižky	Žižka	k1gMnSc2	Žižka
nebo	nebo	k8xC	nebo
Táboritů	táborita	k1gMnPc2	táborita
<g/>
,	,	kIx,	,
totožná	totožný	k2eAgFnSc1d1	totožná
praxe	praxe	k1gFnSc1	praxe
byla	být	k5eAaImAgFnS	být
běžná	běžný	k2eAgFnSc1d1	běžná
i	i	k9	i
v	v	k7c6	v
zahraničním	zahraniční	k2eAgNnSc6d1	zahraniční
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
tématu	téma	k1gNnSc3	téma
profesor	profesor	k1gMnSc1	profesor
Čornej	Čornej	k1gFnSc4	Čornej
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
dějinné	dějinný	k2eAgNnSc1d1	dějinné
účinkování	účinkování	k1gNnSc1	účinkování
skutečně	skutečně	k6eAd1	skutečně
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
značným	značný	k2eAgNnSc7d1	značné
nahromaděním	nahromadění	k1gNnSc7	nahromadění
hrůzných	hrůzný	k2eAgInPc2d1	hrůzný
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
přesahujících	přesahující	k2eAgMnPc2d1	přesahující
dobovou	dobový	k2eAgFnSc4d1	dobová
míru	míra	k1gFnSc4	míra
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rozdíl	rozdíl	k1gInSc1	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
válečných	válečný	k2eAgInPc2d1	válečný
konfliktů	konflikt	k1gInPc2	konflikt
éry	éra	k1gFnSc2	éra
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
středověku	středověk	k1gInSc2	středověk
spočíval	spočívat	k5eAaImAgInS	spočívat
spíše	spíše	k9	spíše
v	v	k7c6	v
kvantitě	kvantita	k1gFnSc6	kvantita
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
důslednosti	důslednost	k1gFnPc1	důslednost
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
nevidomý	nevidomý	k1gMnSc1	nevidomý
hejtman	hejtman	k1gMnSc1	hejtman
postupoval	postupovat	k5eAaImAgMnS	postupovat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
shledává	shledávat	k5eAaImIp3nS	shledávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Žižkův	Žižkův	k2eAgInSc1d1	Žižkův
postup	postup	k1gInSc1	postup
byl	být	k5eAaImAgInS	být
někdy	někdy	k6eAd1	někdy
jen	jen	k9	jen
odvetou	odveta	k1gFnSc7	odveta
za	za	k7c4	za
krutost	krutost	k1gFnSc4	krutost
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
vcelku	vcelku	k6eAd1	vcelku
stabilně	stabilně	k6eAd1	stabilně
řídil	řídit	k5eAaImAgInS	řídit
tradičními	tradiční	k2eAgInPc7d1	tradiční
vzorci	vzorec	k1gInPc7	vzorec
rytířského	rytířský	k2eAgNnSc2d1	rytířské
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Písemné	písemný	k2eAgInPc1d1	písemný
prameny	pramen	k1gInPc1	pramen
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
spokojil	spokojit	k5eAaPmAgMnS	spokojit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepřátele	nepřítel	k1gMnPc4	nepřítel
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přistoupili	přistoupit	k5eAaPmAgMnP	přistoupit
ke	k	k7c3	k
čtyřem	čtyři	k4xCgInPc3	čtyři
artikulům	artikul	k1gInPc3	artikul
<g/>
,	,	kIx,	,
když	když	k8xS	když
tak	tak	k9	tak
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
neučinili	učinit	k5eNaPmAgMnP	učinit
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
pokyn	pokyn	k1gInSc4	pokyn
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
exekuce	exekuce	k1gFnSc2	exekuce
či	či	k8xC	či
zahájení	zahájení	k1gNnSc2	zahájení
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Svědomí	svědomí	k1gNnSc2	svědomí
měl	mít	k5eAaImAgMnS	mít
čisté	čistý	k2eAgNnSc1d1	čisté
navzdory	navzdory	k7c3	navzdory
výtkám	výtka	k1gFnPc3	výtka
mistra	mistr	k1gMnSc2	mistr
Jakoubka	Jakoubek	k1gMnSc4	Jakoubek
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
i	i	k8xC	i
Petra	Petr	k1gMnSc2	Petr
Chelčického	Chelčický	k2eAgMnSc2d1	Chelčický
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
reformní	reformní	k2eAgInSc1d1	reformní
boj	boj	k1gInSc1	boj
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
vést	vést	k5eAaImF	vést
pouze	pouze	k6eAd1	pouze
duchovní	duchovní	k2eAgFnSc7d1	duchovní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
válečné	válečný	k2eAgFnSc6d1	válečná
krutosti	krutost	k1gFnSc6	krutost
<g/>
,	,	kIx,	,
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obšírně	obšírně	k6eAd1	obšírně
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
množství	množství	k1gNnSc4	množství
zločinů	zločin	k1gInPc2	zločin
Táborů	tábor	k1gMnPc2	tábor
<g/>
,	,	kIx,	,
u	u	k7c2	u
popisovaných	popisovaný	k2eAgFnPc2d1	popisovaná
krutostí	krutost	k1gFnPc2	krutost
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
jméno	jméno	k1gNnSc1	jméno
ohleduplně	ohleduplně	k6eAd1	ohleduplně
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Příbrami	Příbram	k1gFnSc2	Příbram
<g/>
,	,	kIx,	,
velký	velký	k2eAgMnSc1d1	velký
žalobce	žalobce	k1gMnSc1	žalobce
Tábora	Tábor	k1gInSc2	Tábor
<g/>
,	,	kIx,	,
nikde	nikde	k6eAd1	nikde
neoznačuje	označovat	k5eNaImIp3nS	označovat
Žižku	Žižka	k1gMnSc4	Žižka
jako	jako	k8xS	jako
viníka	viník	k1gMnSc4	viník
nebo	nebo	k8xC	nebo
spoluviníka	spoluviník	k1gMnSc4	spoluviník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
své	svůj	k3xOyFgInPc4	svůj
útoky	útok	k1gInPc4	útok
směřuje	směřovat	k5eAaImIp3nS	směřovat
proti	proti	k7c3	proti
táborským	táborský	k2eAgMnPc3d1	táborský
kněžím	kněz	k1gMnPc3	kněz
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Korandovi	Korandův	k2eAgMnPc1d1	Korandův
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
písemných	písemný	k2eAgInPc2d1	písemný
pramenů	pramen	k1gInPc2	pramen
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolubojovníci	spolubojovník	k1gMnPc1	spolubojovník
chovali	chovat	k5eAaImAgMnP	chovat
k	k	k7c3	k
Žižkovi	Žižka	k1gMnSc3	Žižka
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
oddanost	oddanost	k1gFnSc4	oddanost
a	a	k8xC	a
značný	značný	k2eAgInSc4d1	značný
respekt	respekt	k1gInSc4	respekt
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Pekař	Pekař	k1gMnSc1	Pekař
dokonce	dokonce	k9	dokonce
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jimi	on	k3xPp3gInPc7	on
byl	být	k5eAaImAgInS	být
milován	milován	k2eAgMnSc1d1	milován
<g/>
,	,	kIx,	,
nepochybně	pochybně	k6eNd1	pochybně
pro	pro	k7c4	pro
dobré	dobrý	k2eAgFnPc4d1	dobrá
stránky	stránka	k1gFnPc4	stránka
své	svůj	k3xOyFgFnSc2	svůj
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Sympatie	sympatie	k1gFnSc1	sympatie
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
jistě	jistě	k6eAd1	jistě
vzbuzovala	vzbuzovat	k5eAaImAgFnS	vzbuzovat
i	i	k9	i
nezištnost	nezištnost	k1gFnSc1	nezištnost
s	s	k7c7	s
jakou	jaký	k3yIgFnSc7	jaký
sloužil	sloužit	k5eAaImAgMnS	sloužit
své	svůj	k3xOyFgFnSc3	svůj
myšlence	myšlenka	k1gFnSc3	myšlenka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
věrný	věrný	k2eAgMnSc1d1	věrný
své	svůj	k3xOyFgFnSc3	svůj
cti	čest	k1gFnSc3	čest
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgInPc4	svůj
úspěchy	úspěch	k1gInPc4	úspěch
na	na	k7c6	na
válečném	válečný	k2eAgNnSc6d1	válečné
poli	pole	k1gNnSc6	pole
nijak	nijak	k6eAd1	nijak
neobohatil	obohatit	k5eNaPmAgInS	obohatit
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
jeho	jeho	k3xOp3gInSc7	jeho
ziskem	zisk	k1gInSc7	zisk
byl	být	k5eAaImAgInS	být
malý	malý	k2eAgInSc1d1	malý
hrádek	hrádek	k1gInSc1	hrádek
Kalich	kalich	k1gInSc1	kalich
u	u	k7c2	u
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
náklonnosti	náklonnost	k1gFnSc6	náklonnost
jednoznačně	jednoznačně	k6eAd1	jednoznačně
hovoří	hovořit	k5eAaImIp3nS	hovořit
i	i	k9	i
chování	chování	k1gNnSc1	chování
orebitů	orebita	k1gMnPc2	orebita
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
po	po	k7c6	po
hejtmanově	hejtmanův	k2eAgFnSc6d1	hejtmanova
smrti	smrt	k1gFnSc6	smrt
přijali	přijmout	k5eAaPmAgMnP	přijmout
jméno	jméno	k1gNnSc4	jméno
sirotci	sirotek	k1gMnPc1	sirotek
a	a	k8xC	a
gesto	gesto	k1gNnSc1	gesto
Hradeckých	Hradecký	k1gMnPc2	Hradecký
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prý	prý	k9	prý
podle	podle	k7c2	podle
Kroniky	kronika	k1gFnSc2	kronika
velmi	velmi	k6eAd1	velmi
pěkné	pěkná	k1gFnSc2	pěkná
o	o	k7c6	o
Janu	Jan	k1gMnSc6	Jan
Žižkovi	Žižka	k1gMnSc6	Žižka
Žižku	Žižka	k1gMnSc4	Žižka
na	na	k7c6	na
bílém	bílé	k1gNnSc6	bílé
koni	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
v	v	k7c6	v
rytířské	rytířský	k2eAgFnSc6d1	rytířská
zbroji	zbroj	k1gFnSc6	zbroj
a	a	k8xC	a
s	s	k7c7	s
palcátem	palcát	k1gInSc7	palcát
<g/>
,	,	kIx,	,
namalovali	namalovat	k5eAaPmAgMnP	namalovat
na	na	k7c4	na
prapor	prapor	k1gInSc4	prapor
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
následujících	následující	k2eAgFnPc2d1	následující
válečných	válečný	k2eAgFnPc2d1	válečná
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Ústředními	ústřední	k2eAgFnPc7d1	ústřední
devizami	deviza	k1gFnPc7	deviza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
spjaty	spjat	k2eAgInPc1d1	spjat
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
,	,	kIx,	,
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
jeho	jeho	k3xOp3gFnPc1	jeho
válečnické	válečnický	k2eAgFnPc1d1	válečnická
dovednosti	dovednost	k1gFnPc1	dovednost
a	a	k8xC	a
inovace	inovace	k1gFnSc1	inovace
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
vojenského	vojenský	k2eAgInSc2d1	vojenský
potenciálu	potenciál	k1gInSc2	potenciál
vojska	vojsko	k1gNnSc2	vojsko
rekrutovaného	rekrutovaný	k2eAgNnSc2d1	rekrutované
především	především	k9	především
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
městské	městský	k2eAgFnSc2d1	městská
a	a	k8xC	a
venkovské	venkovský	k2eAgFnSc2d1	venkovská
chudiny	chudina	k1gFnSc2	chudina
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
mu	on	k3xPp3gMnSc3	on
bývá	bývat	k5eAaImIp3nS	bývat
připisováno	připisován	k2eAgNnSc4d1	připisováno
autorství	autorství	k1gNnSc4	autorství
defenzivní	defenzivní	k2eAgFnSc2d1	defenzivní
taktiky	taktika	k1gFnSc2	taktika
tzv.	tzv.	kA	tzv.
vozové	vozový	k2eAgFnSc2d1	vozová
hradby	hradba	k1gFnSc2	hradba
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
méně	málo	k6eAd2	málo
sofistikovanému	sofistikovaný	k2eAgNnSc3d1	sofistikované
použití	použití	k1gNnSc3	použití
docházelo	docházet	k5eAaImAgNnS	docházet
sporadicky	sporadicky	k6eAd1	sporadicky
i	i	k9	i
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
teprve	teprve	k6eAd1	teprve
myšlenka	myšlenka	k1gFnSc1	myšlenka
trocnovského	trocnovský	k2eAgMnSc2d1	trocnovský
hejtmana	hejtman	k1gMnSc2	hejtman
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ze	z	k7c2	z
selských	selský	k2eAgInPc2d1	selský
vozů	vůz	k1gInPc2	vůz
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
pohyblivou	pohyblivý	k2eAgFnSc4d1	pohyblivá
hradbu	hradba	k1gFnSc4	hradba
umožňující	umožňující	k2eAgNnSc1d1	umožňující
vedení	vedení	k1gNnSc1	vedení
ofenzivně-defenzivních	ofenzivněefenzivní	k2eAgFnPc2d1	ofenzivně-defenzivní
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
schopnou	schopný	k2eAgFnSc7d1	schopná
odolat	odolat	k5eAaPmF	odolat
útoku	útok	k1gInSc2	útok
těžké	těžký	k2eAgFnSc2d1	těžká
rytířské	rytířský	k2eAgFnSc2d1	rytířská
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vojenský	vojenský	k2eAgInSc1d1	vojenský
prvek	prvek	k1gInSc1	prvek
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
výzbroje	výzbroj	k1gFnSc2	výzbroj
záhy	záhy	k6eAd1	záhy
převzaly	převzít	k5eAaPmAgInP	převzít
i	i	k9	i
další	další	k2eAgInPc1d1	další
husitské	husitský	k2eAgInPc1d1	husitský
svazy	svaz	k1gInPc1	svaz
a	a	k8xC	a
protivníci	protivník	k1gMnPc1	protivník
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
sféře	sféra	k1gFnSc6	sféra
se	se	k3xPyFc4	se
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
následující	následující	k2eAgNnSc4d1	následující
století	století	k1gNnSc4	století
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
zcela	zcela	k6eAd1	zcela
ojedinělým	ojedinělý	k2eAgInSc7d1	ojedinělý
počinem	počin	k1gInSc7	počin
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
vytvoření	vytvoření	k1gNnSc1	vytvoření
profesionalizovaných	profesionalizovaný	k2eAgFnPc2d1	profesionalizovaná
armád	armáda	k1gFnPc2	armáda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
členěny	členěn	k2eAgInPc1d1	členěn
na	na	k7c4	na
nižší	nízký	k2eAgInPc4d2	nižší
organizační	organizační	k2eAgInPc4d1	organizační
celky	celek	k1gInPc4	celek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgFnP	být
schopny	schopen	k2eAgInPc1d1	schopen
vzájemně	vzájemně	k6eAd1	vzájemně
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
nebo	nebo	k8xC	nebo
plnit	plnit	k5eAaImF	plnit
samostatné	samostatný	k2eAgInPc4d1	samostatný
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
doktrínu	doktrína	k1gFnSc4	doktrína
Žižka	Žižka	k1gMnSc1	Žižka
zakotvil	zakotvit	k5eAaPmAgMnS	zakotvit
ve	v	k7c6	v
vojenském	vojenský	k2eAgInSc6d1	vojenský
řádu	řád	k1gInSc6	řád
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
částečně	částečně	k6eAd1	částečně
unifikoval	unifikovat	k5eAaImAgInS	unifikovat
i	i	k9	i
výzbroj	výzbroj	k1gInSc1	výzbroj
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
propracování	propracování	k1gNnSc3	propracování
systému	systém	k1gInSc2	systém
finančního	finanční	k2eAgNnSc2d1	finanční
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
a	a	k8xC	a
zásobování	zásobování	k1gNnSc2	zásobování
<g/>
.	.	kIx.	.
</s>
<s>
Zevrubné	zevrubný	k2eAgNnSc1d1	zevrubné
studium	studium	k1gNnSc1	studium
Žižkových	Žižkových	k2eAgNnSc2d1	Žižkových
tažení	tažení	k1gNnSc2	tažení
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
takticko	takticko	k1gNnSc1	takticko
<g/>
–	–	k?	–
<g/>
strategické	strategický	k2eAgFnSc2d1	strategická
<g/>
,	,	kIx,	,
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
mnohdy	mnohdy	k6eAd1	mnohdy
početnějším	početní	k2eAgMnSc7d2	početnější
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
vyzbrojeným	vyzbrojený	k2eAgMnSc7d1	vyzbrojený
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
využít	využít	k5eAaPmF	využít
veškerých	veškerý	k3xTgInPc2	veškerý
možných	možný	k2eAgInPc2d1	možný
prostředků	prostředek	k1gInPc2	prostředek
lsti	lest	k1gFnSc2	lest
<g/>
,	,	kIx,	,
výhod	výhoda	k1gFnPc2	výhoda
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
překvapení	překvapení	k1gNnSc2	překvapení
<g/>
,	,	kIx,	,
rychlosti	rychlost	k1gFnSc2	rychlost
pohybu	pohyb	k1gInSc2	pohyb
i	i	k8xC	i
nenadálého	nenadálý	k2eAgInSc2d1	nenadálý
přepadu	přepad	k1gInSc2	přepad
nebo	nebo	k8xC	nebo
zrady	zrada	k1gFnSc2	zrada
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
akcích	akce	k1gFnPc6	akce
nepochybně	pochybně	k6eNd1	pochybně
zúročil	zúročit	k5eAaPmAgMnS	zúročit
i	i	k9	i
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
společníky	společník	k1gMnPc7	společník
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pocházejícími	pocházející	k2eAgInPc7d1	pocházející
s	s	k7c7	s
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
vrstev	vrstva	k1gFnPc2	vrstva
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgInS	vést
drobnou	drobný	k2eAgFnSc4d1	drobná
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
a	a	k8xC	a
královskému	královský	k2eAgNnSc3d1	královské
městu	město	k1gNnSc3	město
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Josefa	Josef	k1gMnSc2	Josef
Pekaře	Pekař	k1gMnSc2	Pekař
však	však	k9	však
za	za	k7c7	za
Žižkovými	Žižkův	k2eAgInPc7d1	Žižkův
úspěchy	úspěch	k1gInPc7	úspěch
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
nestál	stát	k5eNaImAgInS	stát
moment	moment	k1gInSc4	moment
technický	technický	k2eAgInSc4d1	technický
nebo	nebo	k8xC	nebo
taktický	taktický	k2eAgInSc4d1	taktický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
psychologický	psychologický	k2eAgInSc1d1	psychologický
<g/>
.	.	kIx.	.
.	.	kIx.	.
<g/>
..	..	k?	..
<g/>
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
mohutná	mohutný	k2eAgFnSc1d1	mohutná
sebedůvěra	sebedůvěra	k1gFnSc1	sebedůvěra
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
vyzbrojil	vyzbrojit	k5eAaPmAgInS	vyzbrojit
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
bojovníky	bojovník	k1gMnPc4	bojovník
božími	boží	k2eAgNnPc7d1	boží
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jistota	jistota	k1gFnSc1	jistota
nadpřirozené	nadpřirozený	k2eAgFnSc2d1	nadpřirozená
pomoci	pomoc	k1gFnSc2	pomoc
a	a	k8xC	a
jí	jíst	k5eAaImIp3nS	jíst
daný	daný	k2eAgInSc1d1	daný
fanatický	fanatický	k2eAgInSc1d1	fanatický
elán	elán	k1gInSc1	elán
v	v	k7c6	v
obraně	obrana	k1gFnSc6	obrana
i	i	k8xC	i
útoku	útok	k1gInSc6	útok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Čáslavské	Čáslavské	k2eAgInPc1d1	Čáslavské
kosterní	kosterní	k2eAgInPc1d1	kosterní
ostatky	ostatek	k1gInPc1	ostatek
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
ze	z	k7c2	z
zápisu	zápis	k1gInSc2	zápis
ve	v	k7c6	v
Starých	Starých	k2eAgInPc6d1	Starých
letopisech	letopis	k1gInPc6	letopis
českých	český	k2eAgFnPc2d1	Česká
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Ducha	duch	k1gMnSc2	duch
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
přeneseno	přenést	k5eAaPmNgNnS	přenést
do	do	k7c2	do
Čáslavi	Čáslav	k1gFnSc2	Čáslav
<g/>
,	,	kIx,	,
zůstával	zůstávat	k5eAaImAgInS	zůstávat
osud	osud	k1gInSc1	osud
ostatků	ostatek	k1gInPc2	ostatek
husitského	husitský	k2eAgMnSc2d1	husitský
hejtmana	hejtman	k1gMnSc2	hejtman
značně	značně	k6eAd1	značně
diskutabilní	diskutabilní	k2eAgFnSc1d1	diskutabilní
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgInS	být
především	především	k9	především
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
ze	z	k7c2	z
starých	starý	k2eAgMnPc2d1	starý
letopisců	letopisec	k1gMnPc2	letopisec
a	a	k8xC	a
středověkých	středověký	k2eAgMnPc2d1	středověký
kronikářů	kronikář	k1gMnPc2	kronikář
uváděli	uvádět	k5eAaImAgMnP	uvádět
stejné	stejný	k2eAgFnPc4d1	stejná
datum	datum	k1gInSc1	datum
prvního	první	k4xOgInSc2	první
pohřbu	pohřeb	k1gInSc2	pohřeb
a	a	k8xC	a
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
skutečnost	skutečnost	k1gFnSc4	skutečnost
o	o	k7c6	o
přenesení	přenesení	k1gNnSc6	přenesení
kostí	kost	k1gFnPc2	kost
do	do	k7c2	do
Čáslavi	Čáslav	k1gFnSc2	Čáslav
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
čeští	český	k2eAgMnPc1d1	český
historici	historik	k1gMnPc1	historik
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Šimák	Šimák	k1gMnSc1	Šimák
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Guth	Guth	k1gMnSc1	Guth
<g/>
)	)	kIx)	)
proto	proto	k8xC	proto
tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
z	z	k7c2	z
historiků	historik	k1gMnPc2	historik
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
zastávali	zastávat	k5eAaImAgMnP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
tělo	tělo	k1gNnSc1	tělo
husitského	husitský	k2eAgMnSc2d1	husitský
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
bylo	být	k5eAaImAgNnS	být
pochováno	pochovat	k5eAaPmNgNnS	pochovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Čáslavi	Čáslav	k1gFnSc6	Čáslav
a	a	k8xC	a
královéhradecký	královéhradecký	k2eAgInSc4d1	královéhradecký
pohřeb	pohřeb	k1gInSc4	pohřeb
byl	být	k5eAaImAgMnS	být
fraška	fraška	k1gFnSc1	fraška
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
k	k	k7c3	k
převozu	převoz	k1gInSc3	převoz
těla	tělo	k1gNnSc2	tělo
skutečně	skutečně	k6eAd1	skutečně
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestalo	stát	k5eNaPmAgNnS	stát
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
15.	[number]	k4	15.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
sepsání	sepsání	k1gNnSc3	sepsání
Kroniky	kronika	k1gFnSc2	kronika
velmi	velmi	k6eAd1	velmi
pěkné	pěkná	k1gFnSc2	pěkná
o	o	k7c6	o
Janu	Jan	k1gMnSc6	Jan
Žižkovi	Žižka	k1gMnSc6	Žižka
v	v	k7c6	v
níž	jenž	k3xRgFnSc3	jenž
autor	autor	k1gMnSc1	autor
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
I	i	k9	i
pochovali	pochovat	k5eAaPmAgMnP	pochovat
jsú	jsú	k?	jsú
jeho	jeho	k3xOp3gMnPc3	jeho
(	(	kIx(	(
<g/>
Žižku	Žižka	k1gMnSc4	Žižka
<g/>
)	)	kIx)	)
v	v	k7c6	v
Králové	Králové	k2eAgInSc6d1	Králové
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
a	a	k8xC	a
tu	tu	k6eAd1	tu
ležie	ležie	k1gFnSc1	ležie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Některé	některý	k3yIgFnPc1	některý
verze	verze	k1gFnPc1	verze
(	(	kIx(	(
<g/>
R	R	kA	R
a	a	k8xC	a
G	G	kA	G
<g/>
)	)	kIx)	)
Starých	Starých	k2eAgInPc2d1	Starých
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgInPc2d1	český
pak	pak	k6eAd1	pak
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1471	[number]	k4	1471
již	již	k6eAd1	již
byli	být	k5eAaImAgMnP	být
autoři	autor	k1gMnPc1	autor
s	s	k7c7	s
přemístěním	přemístění	k1gNnSc7	přemístění
ostatků	ostatek	k1gInPc2	ostatek
obeznámeni	obeznámen	k2eAgMnPc1d1	obeznámen
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
exhumace	exhumace	k1gFnSc2	exhumace
prozatím	prozatím	k6eAd1	prozatím
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
indicie	indicie	k1gFnPc4	indicie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
převoz	převoz	k1gInSc4	převoz
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
do	do	k7c2	do
Čáslavi	Čáslav	k1gFnSc2	Čáslav
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
především	především	k6eAd1	především
opakované	opakovaný	k2eAgFnSc2d1	opakovaná
zmínky	zmínka	k1gFnSc2	zmínka
kronikářů	kronikář	k1gMnPc2	kronikář
o	o	k7c6	o
truhle	truhla	k1gFnSc6	truhla
s	s	k7c7	s
ostatky	ostatek	k1gInPc7	ostatek
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
kamenného	kamenný	k2eAgInSc2d1	kamenný
kenotafu	kenotaf	k1gInSc2	kenotaf
a	a	k8xC	a
vědomosti	vědomost	k1gFnPc4	vědomost
o	o	k7c6	o
návštěvách	návštěva	k1gFnPc6	návštěva
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
památníku	památník	k1gInSc2	památník
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
čtyři	čtyři	k4xCgFnPc1	čtyři
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
zničení	zničení	k1gNnSc6	zničení
náhrobku	náhrobek	k1gInSc2	náhrobek
a	a	k8xC	a
pokusech	pokus	k1gInPc6	pokus
o	o	k7c4	o
nalezení	nalezení	k1gNnSc4	nalezení
a	a	k8xC	a
zničení	zničení	k1gNnSc4	zničení
veškerých	veškerý	k3xTgFnPc2	veškerý
památek	památka	k1gFnPc2	památka
na	na	k7c4	na
Jana	Jan	k1gMnSc4	Jan
Žižku	Žižka	k1gMnSc4	Žižka
v	v	k7c6	v
Čáslavi	Čáslav	k1gFnSc6	Čáslav
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
pobělohorská	pobělohorský	k2eAgFnSc1d1	pobělohorská
protireformační	protireformační	k2eAgFnSc1d1	protireformační
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
stoupenci	stoupenec	k1gMnPc1	stoupenec
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Žižkovy	Žižkův	k2eAgFnPc1d1	Žižkova
kosti	kost	k1gFnPc1	kost
byly	být	k5eAaImAgFnP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
kenotafu	kenotaf	k1gInSc6	kenotaf
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
byl	být	k5eAaImAgInS	být
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
neúspěchu	neúspěch	k1gInSc6	neúspěch
se	se	k3xPyFc4	se
je	on	k3xPp3gNnSc4	on
pokusili	pokusit	k5eAaPmAgMnP	pokusit
nalézt	nalézt	k5eAaPmF	nalézt
uvnitř	uvnitř	k7c2	uvnitř
čáslavského	čáslavský	k2eAgInSc2d1	čáslavský
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1741	[number]	k4	1741
zde	zde	k6eAd1	zde
objevili	objevit	k5eAaPmAgMnP	objevit
několik	několik	k4yIc4	několik
kosterních	kosterní	k2eAgInPc2d1	kosterní
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nechali	nechat	k5eAaPmAgMnP	nechat
katem	kat	k1gInSc7	kat
veřejně	veřejně	k6eAd1	veřejně
spálit	spálit	k5eAaPmF	spálit
na	na	k7c6	na
čáslavském	čáslavský	k2eAgNnSc6d1	čáslavské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
nalezení	nalezení	k1gNnSc4	nalezení
místa	místo	k1gNnSc2	místo
Žižkova	Žižkov	k1gInSc2	Žižkov
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
následovaly	následovat	k5eAaImAgFnP	následovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1801	[number]	k4	1801
<g/>
,	,	kIx,	,
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
1880	[number]	k4	1880
a	a	k8xC	a
1901.	[number]	k4	1901.
</s>
</p>
<p>
</p>
<p>
<s>
Nečekaný	čekaný	k2eNgInSc1d1	nečekaný
úspěch	úspěch	k1gInSc1	úspěch
se	se	k3xPyFc4	se
dostavil	dostavit	k5eAaPmAgInS	dostavit
21.	[number]	k4	21.
listopadu	listopad	k1gInSc2	listopad
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
rekonstrukčních	rekonstrukční	k2eAgFnPc6d1	rekonstrukční
pracích	práce	k1gFnPc6	práce
uvnitř	uvnitř	k7c2	uvnitř
chrámu	chrám	k1gInSc2	chrám
nalezen	nalezen	k2eAgInSc4d1	nalezen
115	[number]	k4	115
cm	cm	kA	cm
vysoký	vysoký	k2eAgMnSc1d1	vysoký
a	a	k8xC	a
84	[number]	k4	84
cm	cm	kA	cm
hluboký	hluboký	k2eAgMnSc1d1	hluboký
a	a	k8xC	a
široký	široký	k2eAgInSc1d1	široký
výklenek	výklenek	k1gInSc1	výklenek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
architekta	architekt	k1gMnSc2	architekt
Kamila	Kamil	k1gMnSc2	Kamil
Hilberta	Hilbert	k1gMnSc2	Hilbert
vestavěný	vestavěný	k2eAgInSc4d1	vestavěný
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
kostela	kostel	k1gInSc2	kostel
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
věže	věž	k1gFnSc2	věž
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1450	[number]	k4	1450
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
výklenku	výklenek	k1gInSc2	výklenek
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
dvě	dva	k4xCgFnPc1	dva
zkřížené	zkřížený	k2eAgFnPc1d1	zkřížená
stehenní	stehenní	k2eAgFnPc1d1	stehenní
kosti	kost	k1gFnPc1	kost
<g/>
,	,	kIx,	,
poškozená	poškozený	k2eAgFnSc1d1	poškozená
kalva	kalva	k1gFnSc1	kalva
a	a	k8xC	a
kus	kus	k1gInSc1	kus
kameninové	kameninový	k2eAgFnSc2d1	kameninová
mísy	mísa	k1gFnSc2	mísa
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
zkřížených	zkřížený	k2eAgFnPc2d1	zkřížená
kostí	kost	k1gFnPc2	kost
byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
další	další	k2eAgInPc1d1	další
zbytky	zbytek	k1gInPc1	zbytek
kostí	kost	k1gFnPc2	kost
lebky	lebka	k1gFnPc4	lebka
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc4d1	ostatní
kostry	kostra	k1gFnPc4	kostra
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
destičky	destička	k1gFnPc4	destička
a	a	k8xC	a
chuchvalec	chuchvalec	k1gInSc4	chuchvalec
ztrouchnivělého	ztrouchnivělý	k2eAgNnSc2d1	ztrouchnivělé
tkaniva	tkanivo	k1gNnSc2	tkanivo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
místa	místo	k1gNnSc2	místo
byly	být	k5eAaImAgFnP	být
přizváni	přizván	k2eAgMnPc1d1	přizván
experti	expert	k1gMnPc1	expert
archeologické	archeologický	k2eAgFnSc2d1	archeologická
komise	komise	k1gFnSc2	komise
při	při	k7c6	při
Akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
nálezu	nález	k1gInSc6	nález
později	pozdě	k6eAd2	pozdě
referoval	referovat	k5eAaBmAgMnS	referovat
profesor	profesor	k1gMnSc1	profesor
Jindřich	Jindřich	k1gMnSc1	Jindřich
Matiegka	Matiegka	k1gMnSc1	Matiegka
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nalezené	nalezený	k2eAgFnPc4d1	nalezená
kosti	kost	k1gFnPc4	kost
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
po	po	k7c6	po
Janu	Jan	k1gMnSc6	Jan
Žižkovi	Žižka	k1gMnSc6	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
závěr	závěr	k1gInSc4	závěr
učinil	učinit	k5eAaPmAgMnS	učinit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
morfologických	morfologický	k2eAgFnPc2d1	morfologická
vlastností	vlastnost	k1gFnPc2	vlastnost
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jednookosti	jednookost	k1gFnPc1	jednookost
objevené	objevený	k2eAgFnSc2d1	objevená
mozkovny	mozkovna	k1gFnSc2	mozkovna
<g/>
,	,	kIx,	,
nálezovým	nálezový	k2eAgFnPc3d1	nálezová
okolnostem	okolnost	k1gFnPc3	okolnost
a	a	k8xC	a
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
nejstarším	starý	k2eAgNnPc3d3	nejstarší
vyobrazením	vyobrazení	k1gNnPc3	vyobrazení
husitského	husitský	k2eAgMnSc2d1	husitský
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
výzkum	výzkum	k1gInSc4	výzkum
však	však	k9	však
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
jistá	jistý	k2eAgFnSc1d1	jistá
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pramenila	pramenit	k5eAaImAgFnS	pramenit
z	z	k7c2	z
aktivity	aktivita	k1gFnSc2	aktivita
čáslavských	čáslavský	k2eAgMnPc2d1	čáslavský
patriotů	patriot	k1gMnPc2	patriot
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
nález	nález	k1gInSc1	nález
historicky	historicky	k6eAd1	historicky
podepřít	podepřít	k5eAaPmF	podepřít
umístili	umístit	k5eAaPmAgMnP	umístit
do	do	k7c2	do
výklenku	výklenek	k1gInSc2	výklenek
v	v	k7c6	v
protilehlé	protilehlý	k2eAgFnSc6d1	protilehlá
straně	strana	k1gFnSc6	strana
podvěží	podvěžit	k5eAaPmIp3nS	podvěžit
nápis	nápis	k1gInSc1	nápis
na	na	k7c6	na
deskách	deska	k1gFnPc6	deska
staré	starý	k2eAgFnSc2d1	stará
knihy	kniha	k1gFnSc2	kniha
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kosti	kost	k1gFnPc1	kost
slavného	slavný	k2eAgMnSc2d1	slavný
vůdce	vůdce	k1gMnSc2	vůdce
českého	český	k2eAgMnSc2d1	český
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
v	v	k7c6	v
protější	protější	k2eAgFnSc6d1	protější
zdi	zeď	k1gFnSc6	zeď
tajně	tajně	k6eAd1	tajně
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
podvrhu	podvrh	k1gInSc3	podvrh
byl	být	k5eAaImAgMnS	být
celý	celý	k2eAgInSc4d1	celý
výzkum	výzkum	k1gInSc4	výzkum
na	na	k7c4	na
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
čtyři	čtyři	k4xCgFnPc4	čtyři
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
zpochybněn	zpochybněn	k2eAgInSc1d1	zpochybněn
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
vyšetřil	vyšetřit	k5eAaPmAgMnS	vyšetřit
tzv.	tzv.	kA	tzv.
čáslavskou	čáslavský	k2eAgFnSc4d1	čáslavská
kalvu	kalva	k1gFnSc4	kalva
profesor	profesor	k1gMnSc1	profesor
Otakar	Otakar	k1gMnSc1	Otakar
Hněvkovský	Hněvkovský	k2eAgMnSc1d1	Hněvkovský
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
byla	být	k5eAaImAgFnS	být
prozkoumána	prozkoumat	k5eAaPmNgFnS	prozkoumat
odborníky	odborník	k1gMnPc7	odborník
z	z	k7c2	z
Archeologického	archeologický	k2eAgInSc2d1	archeologický
ústavu	ústav	k1gInSc2	ústav
ČSAV	ČSAV	kA	ČSAV
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
antropologem	antropolog	k1gMnSc7	antropolog
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Vlčkem	Vlček	k1gMnSc7	Vlček
<g/>
.	.	kIx.	.
</s>
<s>
Učiněné	učiněný	k2eAgInPc1d1	učiněný
závěry	závěr	k1gInPc1	závěr
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kosterní	kosterní	k2eAgInSc4d1	kosterní
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
jedince	jedinko	k6eAd1	jedinko
mužského	mužský	k2eAgNnSc2d1	mužské
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
stáří	stáří	k1gNnSc1	stáří
se	se	k3xPyFc4	se
pohybovalo	pohybovat	k5eAaImAgNnS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
40	[number]	k4	40
±	±	k?	±
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc4	tento
údaj	údaj	k1gInSc4	údaj
Emanuel	Emanuel	k1gMnSc1	Emanuel
Vlček	Vlček	k1gMnSc1	Vlček
později	pozdě	k6eAd2	pozdě
pozměnil	pozměnit	k5eAaPmAgMnS	pozměnit
na	na	k7c6	na
50	[number]	k4	50
±	±	k?	±
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
krajiny	krajina	k1gFnPc1	krajina
obou	dva	k4xCgFnPc2	dva
očnic	očnice	k1gFnPc2	očnice
nesou	nést	k5eAaImIp3nP	nést
vyhojená	vyhojený	k2eAgNnPc1d1	vyhojené
poranění	poranění	k1gNnPc1	poranění
<g/>
.	.	kIx.	.
</s>
<s>
Levé	levý	k2eAgNnSc1d1	levé
oko	oko	k1gNnSc1	oko
bylo	být	k5eAaImAgNnS	být
poraněno	poranit	k5eAaPmNgNnS	poranit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
úderem	úder	k1gInSc7	úder
sečné	sečný	k2eAgFnPc4d1	sečná
zbraně	zbraň	k1gFnPc4	zbraň
před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
růstu	růst	k1gInSc2	růst
individua	individuum	k1gNnSc2	individuum
<g/>
,	,	kIx,	,
pravý	pravý	k2eAgInSc1d1	pravý
nadočnicový	nadočnicový	k2eAgInSc1d1	nadočnicový
oblouk	oblouk	k1gInSc1	oblouk
nese	nést	k5eAaImIp3nS	nést
známky	známka	k1gFnPc4	známka
procesů	proces	k1gInPc2	proces
probíhajících	probíhající	k2eAgInPc2d1	probíhající
při	při	k7c6	při
hojení	hojení	k1gNnSc6	hojení
opouzdřeného	opouzdřený	k2eAgInSc2d1	opouzdřený
hematomu	hematom	k1gInSc2	hematom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
tupým	tupý	k2eAgNnSc7d1	tupé
násilím	násilí	k1gNnSc7	násilí
vedeným	vedený	k2eAgNnSc7d1	vedené
na	na	k7c4	na
krajinu	krajina	k1gFnSc4	krajina
pravého	pravý	k2eAgNnSc2d1	pravé
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hojení	hojení	k1gNnSc1	hojení
probíhalo	probíhat	k5eAaImAgNnS	probíhat
nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
jedince	jedinec	k1gMnSc2	jedinec
a	a	k8xC	a
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
kalva	kalva	k1gFnSc1	kalva
skutečně	skutečně	k6eAd1	skutečně
patřila	patřit	k5eAaImAgFnS	patřit
Janu	Jana	k1gFnSc4	Jana
Žižkovi	Žižka	k1gMnSc3	Žižka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc7	jeho
příčinou	příčina	k1gFnSc7	příčina
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
poranění	poranění	k1gNnSc1	poranění
u	u	k7c2	u
hradu	hrad	k1gInSc2	hrad
Rábí	Rábí	k1gNnSc2	Rábí
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
80.	[number]	k4	80.
letech	léto	k1gNnPc6	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
historika	historik	k1gMnSc2	historik
Miroslava	Miroslav	k1gMnSc2	Miroslav
Ivanova	Ivanův	k2eAgMnSc2d1	Ivanův
čáslavský	čáslavský	k2eAgInSc4d1	čáslavský
nález	nález	k1gInSc4	nález
znova	znova	k6eAd1	znova
detailně	detailně	k6eAd1	detailně
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
odbornými	odborný	k2eAgFnPc7d1	odborná
pracovníky	pracovník	k1gMnPc4	pracovník
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
soudního	soudní	k2eAgNnSc2d1	soudní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
antropologie	antropologie	k1gFnSc2	antropologie
a	a	k8xC	a
makromolekulární	makromolekulární	k2eAgFnSc2d1	makromolekulární
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Nejprůkaznější	průkazní	k2eAgInPc1d3	nejprůkaznější
výsledky	výsledek	k1gInPc1	výsledek
však	však	k9	však
přinesla	přinést	k5eAaPmAgFnS	přinést
tehdy	tehdy	k6eAd1	tehdy
ojedinělá	ojedinělý	k2eAgFnSc1d1	ojedinělá
paleosérologická	paleosérologický	k2eAgFnSc1d1	paleosérologický
metoda	metoda	k1gFnSc1	metoda
prováděná	prováděný	k2eAgFnSc1d1	prováděná
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
Dr.	dr.	kA	dr.
Imre	Imre	k1gInSc4	Imre
Lengyelem	Lengyel	k1gInSc7	Lengyel
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nalezená	nalezený	k2eAgFnSc1d1	nalezená
část	část	k1gFnSc1	část
pravého	pravý	k2eAgNnSc2d1	pravé
žebra	žebro	k1gNnSc2	žebro
<g/>
,	,	kIx,	,
levá	levý	k2eAgFnSc1d1	levá
stehenní	stehenní	k2eAgFnSc1d1	stehenní
kost	kost	k1gFnSc1	kost
a	a	k8xC	a
kalva	kalva	k1gFnSc1	kalva
patřily	patřit	k5eAaImAgFnP	patřit
muži	muž	k1gMnSc3	muž
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
bylo	být	k5eAaImAgNnS	být
ve	v	k7c4	v
chvíli	chvíle	k1gFnSc4	chvíle
smrti	smrt	k1gFnSc2	smrt
61	[number]	k4	61
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výsledek	výsledek	k1gInSc1	výsledek
přesně	přesně	k6eAd1	přesně
korespondoval	korespondovat	k5eAaImAgInS	korespondovat
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
Žižkova	Žižkov	k1gInSc2	Žižkov
narození	narození	k1gNnSc2	narození
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1360	[number]	k4	1360
a	a	k8xC	a
datem	datum	k1gNnSc7	datum
úmrtí	úmrtí	k1gNnSc2	úmrtí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1424.	[number]	k4	1424.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Tradice	tradice	k1gFnSc1	tradice
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
a	a	k8xC	a
1917	[number]	k4	1917
byl	být	k5eAaImAgInS	být
nově	nově	k6eAd1	nově
vznikající	vznikající	k2eAgInSc1d1	vznikající
3.	[number]	k4	3.
střelecký	střelecký	k2eAgInSc1d1	střelecký
pluk	pluk	k1gInSc1	pluk
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
na	na	k7c6	na
Rusi	Rus	k1gFnSc6	Rus
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
"	"	kIx"	"
<g/>
pluk	pluk	k1gInSc1	pluk
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BiografieČEČETKA	BiografieČEČETKA	k?	BiografieČEČETKA
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Elstner	Elstner	k1gMnSc1	Elstner
<g/>
,	,	kIx,	,
1930.	[number]	k4	1930.
206	[number]	k4	206
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IVANOV	IVANOV	kA	IVANOV
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Kdy	kdy	k6eAd1	kdy
umírá	umírat	k5eAaImIp3nS	umírat
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1983.	[number]	k4	1983.
416	[number]	k4	416
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-207-0385-3	[number]	k4	80-207-0385-3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KROFTA	KROFTA	kA	KROFTA
<g/>
,	,	kIx,	,
Kamil	Kamil	k1gMnSc1	Kamil
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1936.	[number]	k4	1936.
184	[number]	k4	184
s	s	k7c7	s
<g/>
.	.	kIx.	.
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEKAŘ	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1992.	[number]	k4	1992.
1191	[number]	k4	1191
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-207-0385-3	[number]	k4	80-207-0385-3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEKAŘ	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
prvý	prvý	k4xOgInSc1	prvý
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
zřetelem	zřetel	k1gInSc7	zřetel
k	k	k7c3	k
Táboru	Tábor	k1gInSc3	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
1927.	[number]	k4	1927.
283	[number]	k4	283
s	s	k7c7	s
<g/>
.	.	kIx.	.
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEKAŘ	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
1928.	[number]	k4	1928.
281	[number]	k4	281
s	s	k7c7	s
<g/>
.	.	kIx.	.
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEKAŘ	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
třetí	třetí	k4xOgInSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
1930.	[number]	k4	1930.
330	[number]	k4	330
s	s	k7c7	s
<g/>
.	.	kIx.	.
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEKAŘ	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
k	k	k7c3	k
dílu	dílo	k1gNnSc3	dílo
třetímu	třetí	k4xOgMnSc3	třetí
<g/>
,	,	kIx,	,
opravy	oprava	k1gFnPc4	oprava
a	a	k8xC	a
dodatky	dodatek	k1gInPc4	dodatek
<g/>
,	,	kIx,	,
příloha	příloha	k1gFnSc1	příloha
-	-	kIx~	-
rejstříky	rejstřík	k1gInPc1	rejstřík
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
1933.	[number]	k4	1933.
295	[number]	k4	295
s	s	k7c7	s
<g/>
.	.	kIx.	.
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1969.	[number]	k4	1969.
264	[number]	k4	264
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEK	Tomek	k1gMnSc1	Tomek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Vladivoj	Vladivoj	k1gInSc1	Vladivoj
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
V	v	k7c6	v
ráji	ráj	k1gInSc6	ráj
<g/>
,	,	kIx,	,
1993.	[number]	k4	1993.
228	[number]	k4	228
s	s	k7c7	s
<g/>
.	.	kIx.	.
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-900875-7-4	[number]	k4	80-900875-7-4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
URBÁNEK	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
1925.	[number]	k4	1925.
313	[number]	k4	313
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VYBÍRAL	Vybíral	k1gMnSc1	Vybíral
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
:	:	kIx,	:
1360	[number]	k4	1360
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1424	[number]	k4	1424
:	:	kIx,	:
o	o	k7c6	o
táborském	táborský	k2eAgMnSc6d1	táborský
hejtmanu	hejtman	k1gMnSc6	hejtman
a	a	k8xC	a
husitském	husitský	k2eAgMnSc6d1	husitský
vojevůdci	vojevůdce	k1gMnSc6	vojevůdce
<g/>
.	.	kIx.	.
1.	[number]	k4	1.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Tábor	Tábor	k1gInSc1	Tábor
<g/>
:	:	kIx,	:
Město	město	k1gNnSc1	město
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
odbor	odbor	k1gInSc1	odbor
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
,	,	kIx,	,
2014.	[number]	k4	2014.
173	[number]	k4	173
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-260-6822-8	[number]	k4	978-80-260-6822-8
<g/>
.	.	kIx.	.
</s>
<s>
PramenyBŘEZOVÉ	PramenyBŘEZOVÉ	k?	PramenyBŘEZOVÉ
<g/>
,	,	kIx,	,
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
z	z	k7c2	z
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
kronika	kronika	k1gFnSc1	kronika
<g/>
;	;	kIx,	;
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c4	o
vítězství	vítězství	k1gNnSc4	vítězství
u	u	k7c2	u
Domažlic	Domažlice	k1gFnPc2	Domažlice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1979.	[number]	k4	1979.
427	[number]	k4	427
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SILVIO	SILVIO	kA	SILVIO
<g/>
,	,	kIx,	,
Enea	Eneum	k1gNnSc2	Eneum
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
:	:	kIx,	:
Dialog	dialog	k1gInSc1	dialog
<g/>
,	,	kIx,	,
2010.	[number]	k4	2010.
160	[number]	k4	160
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7382-136-4	[number]	k4	978-80-7382-136-4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VLČEK	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
Kalichu	kalich	k1gInSc2	kalich
a	a	k8xC	a
antropologicko	antropologicko	k6eAd1	antropologicko
lékařský	lékařský	k2eAgInSc1d1	lékařský
průzkum	průzkum	k1gInSc1	průzkum
tzv.	tzv.	kA	tzv.
čáslavské	čáslavský	k2eAgFnSc2d1	čáslavská
kalvy	kalva	k1gFnSc2	kalva
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
1981.	[number]	k4	1981.
30	[number]	k4	30
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Archiv	archiv	k1gInSc4	archiv
český	český	k2eAgInSc4d1	český
XXXVIII	XXXVIII	kA	XXXVIII
-	-	kIx~	-
Popravčí	popravčí	k2eAgInPc4d1	popravčí
a	a	k8xC	a
psanecké	psanecký	k2eAgInPc4d1	psanecký
zápisy	zápis	k1gInPc4	zápis
jihlavské	jihlavský	k2eAgInPc4d1	jihlavský
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1405-1457	[number]	k4	1405-1457
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Filosofia	Filosofia	k1gFnSc1	Filosofia
<g/>
,	,	kIx,	,
2001.	[number]	k4	2001.
260	[number]	k4	260
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7007-146	[number]	k4	80-7007-146
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
množství	množství	k1gNnSc6	množství
nehleďte	hledět	k5eNaImRp2nP	hledět
-	-	kIx~	-
Kronika	kronika	k1gFnSc1	kronika
-	-	kIx~	-
řád	řád	k1gInSc1	řád
-	-	kIx~	-
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1946.	[number]	k4	1946.
132	[number]	k4	132
s	s	k7c7	s
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Kronika	kronika	k1gFnSc1	kronika
velmi	velmi	k6eAd1	velmi
pěkná	pěkný	k2eAgFnSc1d1	pěkná
o	o	k7c4	o
Janu	Jana	k1gFnSc4	Jana
Žižkovi	Žižka	k1gMnSc3	Žižka
<g/>
,	,	kIx,	,
Žižkovy	Žižkův	k2eAgInPc1d1	Žižkův
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
Žižkův	Žižkův	k2eAgInSc1d1	Žižkův
vojenský	vojenský	k2eAgInSc1d1	vojenský
řád	řád	k1gInSc1	řád
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Popravčí	popravčí	k2eAgFnSc1d1	popravčí
kniha	kniha	k1gFnSc1	kniha
pánův	pánův	k2eAgInSc1d1	pánův
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Mareš	Mareš	k1gMnSc1	Mareš
<g/>
,	,	kIx,	,
1878.	[number]	k4	1878.
64	[number]	k4	64
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prameny	pramen	k1gInPc1	pramen
dějin	dějiny	k1gFnPc2	dějiny
českých	český	k2eAgFnPc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Josef	Josef	k1gMnSc1	Josef
Emler	Emler	k1gMnSc1	Emler
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
V	v	k7c6	v
komisi	komise	k1gFnSc6	komise
Dr.	dr.	kA	dr.
Grégr	Grégr	k1gInSc1	Grégr
a	a	k8xC	a
Ferd	Ferd	k1gInSc1	Ferd
<g/>
.	.	kIx.	.
</s>
<s>
Dattel	Dattel	k1gInSc1	Dattel
258	[number]	k4	258
s	s	k7c7	s
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Kronika	kronika	k1gFnSc1	kronika
starého	starý	k2eAgMnSc2d1	starý
kolegiáta	kolegiát	k1gMnSc2	kolegiát
pražského	pražský	k2eAgMnSc2d1	pražský
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
starých	starý	k2eAgInPc2d1	starý
letopisů	letopis	k1gInPc2	letopis
českých	český	k2eAgInPc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1980.	[number]	k4	1980.
580	[number]	k4	580
s	s	k7c7	s
<g/>
.	.	kIx.	.
Sekundární	sekundární	k2eAgFnSc7d1	sekundární
literaturaBARTOŠ	literaturaBARTOŠ	k?	literaturaBARTOŠ
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
Žižkova	Žižkov	k1gInSc2	Žižkov
1415-1426	[number]	k4	1415-1426
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1965.	[number]	k4	1965.
236	[number]	k4	236
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Světla	světlo	k1gNnPc1	světlo
a	a	k8xC	a
stíny	stín	k1gInPc1	stín
husitství	husitství	k1gNnSc2	husitství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2011.	[number]	k4	2011.
482	[number]	k4	482
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7422-084-5	[number]	k4	978-80-7422-084-5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
českých	český	k2eAgFnPc2d1	Česká
kronik	kronika	k1gFnPc2	kronika
:	:	kIx,	:
cesty	cesta	k1gFnSc2	cesta
ke	k	k7c3	k
kořenům	kořen	k1gInPc3	kořen
husitské	husitský	k2eAgFnSc2d1	husitská
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
2.	[number]	k4	2.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2003.	[number]	k4	2003.
456	[number]	k4	456
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7185-590-1	[number]	k4	80-7185-590-1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
V.	V.	kA	V.
1402-1437	[number]	k4	1402-1437
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000.	[number]	k4	2000.
790	[number]	k4	790
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7185-296-1	[number]	k4	80-7185-296-1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DURDÍK	DURDÍK	kA	DURDÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Husitské	husitský	k2eAgNnSc1d1	husitské
vojenství	vojenství	k1gNnSc1	vojenství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
1953.	[number]	k4	1953.
210	[number]	k4	210
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KEJŘ	KEJŘ	kA	KEJŘ
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1984.	[number]	k4	1984.
265	[number]	k4	265
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
11-085-84	[number]	k4	11-085-84
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUFFNER	KUFFNER	kA	KUFFNER
<g/>
,	,	kIx,	,
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
.	.	kIx.	.
</s>
<s>
Husitské	husitský	k2eAgFnPc1d1	husitská
vojny	vojna	k1gFnPc1	vojna
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgInPc1d1	královský
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
:	:	kIx,	:
vl	vl	k?	vl
<g/>
.	.	kIx.	.
<g/>
n	n	k0	n
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2014-04-30	[number]	k4	2014-04-30
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Tábor	Tábor	k1gInSc1	Tábor
v	v	k7c6	v
husitském	husitský	k2eAgNnSc6d1	husitské
revolučním	revoluční	k2eAgNnSc6d1	revoluční
hnutí	hnutí	k1gNnSc6	hnutí
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
1952.	[number]	k4	1952.
453	[number]	k4	453
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Tábor	Tábor	k1gInSc1	Tábor
v	v	k7c6	v
husitském	husitský	k2eAgNnSc6d1	husitské
revolučním	revoluční	k2eAgNnSc6d1	revoluční
hnutí	hnutí	k1gNnSc6	hnutí
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
1955.	[number]	k4	1955.
430	[number]	k4	430
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Husitské	husitský	k2eAgNnSc1d1	husitské
revoluční	revoluční	k2eAgNnSc1d1	revoluční
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
1952.	[number]	k4	1952.
203	[number]	k4	203
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PALACKÝ	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
národu	národ	k1gInSc2	národ
Českého	český	k2eAgInSc2d1	český
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
B.	B.	kA	B.
Kočí	Kočí	k1gMnSc1	Kočí
<g/>
,	,	kIx,	,
1907.	[number]	k4	1907.
1279	[number]	k4	1279
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
vymknutá	vymknutý	k2eAgFnSc1d1	vymknutá
z	z	k7c2	z
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1995.	[number]	k4	1995.
498	[number]	k4	498
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7184-073-4	[number]	k4	80-7184-073-4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
české	český	k2eAgFnSc2d1	Česká
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1996.	[number]	k4	1996.
364	[number]	k4	364
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7184-074-2	[number]	k4	80-7184-074-2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
válečných	válečný	k2eAgNnPc2d1	válečné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1996.	[number]	k4	1996.
420	[number]	k4	420
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7184-075-0	[number]	k4	80-7184-075-0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Epilog	epilog	k1gInSc1	epilog
bouřlivého	bouřlivý	k2eAgInSc2d1	bouřlivý
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1996.	[number]	k4	1996.
550	[number]	k4	550
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7184-076-9	[number]	k4	80-7184-076-9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VYBÍRAL	Vybíral	k1gMnSc1	Vybíral
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
za	za	k7c7	za
poznáním	poznání	k1gNnSc7	poznání
husitského	husitský	k2eAgInSc2d1	husitský
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Tábor	Tábor	k1gInSc1	Tábor
<g/>
:	:	kIx,	:
Husitské	husitský	k2eAgNnSc1d1	husitské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
<g/>
,	,	kIx,	,
2011.	[number]	k4	2011.
207	[number]	k4	207
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-87516-00-3	[number]	k4	978-80-87516-00-3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kalich	kalich	k1gInSc1	kalich
(	(	kIx(	(
<g/>
hrad	hrad	k1gInSc1	hrad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vítkov	Vítkov	k1gInSc1	Vítkov
(	(	kIx(	(
<g/>
Pražská	pražský	k2eAgFnSc1d1	Pražská
plošina	plošina	k1gFnSc1	plošina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Táborský	táborský	k2eAgInSc1d1	táborský
svaz	svaz	k1gInSc1	svaz
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
husitských	husitský	k2eAgFnPc2d1	husitská
bitev	bitva	k1gFnPc2	bitva
</s>
</p>
<p>
<s>
Žižkův	Žižkův	k2eAgInSc1d1	Žižkův
dub	dub	k1gInSc1	dub
(	(	kIx(	(
<g/>
Trocnov	Trocnov	k1gInSc1	Trocnov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
na	na	k7c4	na
www.husitstvi.cz	www.husitstvi.cz	k1gInSc4	www.husitstvi.cz
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Ešner	Ešner	k1gMnSc1	Ešner
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Pekař	Pekař	k1gMnSc1	Pekař
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc1	hodnocení
husitství	husitství	k1gNnSc2	husitství
</s>
</p>
<p>
<s>
Terebelský	Terebelský	k2eAgMnSc1d1	Terebelský
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
:	:	kIx,	:
Život	život	k1gInSc1	život
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
z	z	k7c2	z
Trocnowa	Trocnow	k1gInSc2	Trocnow
<g/>
,	,	kIx,	,
slowútného	slowútný	k2eAgInSc2d1	slowútný
wůdce	wůdec	k1gInSc2	wůdec
Táborských	Táborských	k2eAgInSc1d1	Táborských
bratřj	bratřj	k1gInSc1	bratřj
<g/>
,	,	kIx,	,
1850.	[number]	k4	1850.
</s>
</p>
