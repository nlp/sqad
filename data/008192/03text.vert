<p>
<s>
Sto	sto	k4xCgNnSc1	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Cien	Cien	k1gInSc1	Cien
añ	añ	k?	añ
de	de	k?	de
soledad	soledad	k1gInSc1	soledad
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
román	román	k1gInSc1	román
kolumbijského	kolumbijský	k2eAgMnSc2d1	kolumbijský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Gabriela	Gabriel	k1gMnSc2	Gabriel
Garcíi	Garcí	k1gFnSc2	Garcí
Márqueze	Márqueze	k1gFnSc2	Márqueze
<g/>
,	,	kIx,	,
držitele	držitel	k1gMnSc2	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
několika	několik	k4yIc6	několik
generacích	generace	k1gFnPc6	generace
rozvětvené	rozvětvený	k2eAgFnSc2d1	rozvětvená
rodiny	rodina	k1gFnSc2	rodina
Buendíů	Buendí	k1gInPc2	Buendí
a	a	k8xC	a
osudech	osud	k1gInPc6	osud
jihoamerické	jihoamerický	k2eAgFnSc2d1	jihoamerická
vesničky	vesnička	k1gFnSc2	vesnička
Macondo	Macondo	k6eAd1	Macondo
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Buendíů	Buendí	k1gInPc2	Buendí
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
podobné	podobný	k2eAgInPc4d1	podobný
povahové	povahový	k2eAgInPc4d1	povahový
rysy	rys	k1gInPc4	rys
<g/>
,	,	kIx,	,
především	především	k9	především
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
osamělosti	osamělost	k1gFnSc3	osamělost
a	a	k8xC	a
nedostatku	nedostatek	k1gInSc3	nedostatek
citu	cit	k1gInSc2	cit
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vydán	vydán	k2eAgInSc1d1	vydán
roku	rok	k1gInSc3	rok
1967	[number]	k4	1967
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšel	vyjít	k5eAaPmAgMnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
prozatím	prozatím	k6eAd1	prozatím
naposledy	naposledy	k6eAd1	naposledy
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Medka	Medek	k1gMnSc2	Medek
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
stěžejním	stěžejní	k2eAgNnPc3d1	stěžejní
dílům	dílo	k1gNnPc3	dílo
magického	magický	k2eAgInSc2d1	magický
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
osudy	osud	k1gInPc1	osud
šesti	šest	k4xCc2	šest
generací	generace	k1gFnPc2	generace
rodu	rod	k1gInSc2	rod
Buendíů	Buendí	k1gMnPc2	Buendí
<g/>
,	,	kIx,	,
obývajícího	obývající	k2eAgInSc2d1	obývající
jihoamerické	jihoamerický	k2eAgInPc4d1	jihoamerický
městečko	městečko	k1gNnSc1	městečko
Macondo	Macondo	k1gNnSc1	Macondo
(	(	kIx(	(
<g/>
smyšlené	smyšlený	k2eAgNnSc1d1	smyšlené
městečko	městečko	k1gNnSc1	městečko
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgNnSc2	který
Márquez	Márquez	k1gInSc1	Márquez
zasadil	zasadit	k5eAaPmAgInS	zasadit
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgInPc2	svůj
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
ztotožňováno	ztotožňován	k2eAgNnSc1d1	ztotožňováno
s	s	k7c7	s
Aracatacou	Aracataca	k1gFnSc7	Aracataca
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
autor	autor	k1gMnSc1	autor
trávil	trávit	k5eAaImAgMnS	trávit
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
u	u	k7c2	u
prarodičů	prarodič	k1gMnPc2	prarodič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
datován	datován	k2eAgMnSc1d1	datován
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
podle	podle	k7c2	podle
vývoje	vývoj	k1gInSc2	vývoj
událostí	událost	k1gFnPc2	událost
lze	lze	k6eAd1	lze
soudit	soudit	k5eAaImF	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
století	století	k1gNnSc2	století
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obrazem	obraz	k1gInSc7	obraz
latinskoamerických	latinskoamerický	k2eAgFnPc2d1	latinskoamerická
dějin	dějiny	k1gFnPc2	dějiny
od	od	k7c2	od
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
dob	doba	k1gFnPc2	doba
(	(	kIx(	(
<g/>
retrospektivy	retrospektiva	k1gFnPc1	retrospektiva
až	až	k9	až
do	do	k7c2	do
časů	čas	k1gInPc2	čas
Sira	sir	k1gMnSc2	sir
Francise	Francise	k1gFnSc2	Francise
Drakea	Drakeus	k1gMnSc2	Drakeus
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
období	období	k1gNnSc4	období
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
plukovník	plukovník	k1gMnSc1	plukovník
Aureliáno	Aurelián	k2eAgNnSc4d1	Aurelián
Buendía	Buendíum	k1gNnSc2	Buendíum
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
pronikání	pronikání	k1gNnSc4	pronikání
amerického	americký	k2eAgInSc2d1	americký
kapitálu	kapitál	k1gInSc2	kapitál
(	(	kIx(	(
<g/>
banánová	banánový	k2eAgFnSc1d1	banánová
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plynoucí	plynoucí	k2eAgInPc1d1	plynoucí
sociální	sociální	k2eAgInPc1d1	sociální
rozpory	rozpor	k1gInPc1	rozpor
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
banánový	banánový	k2eAgInSc1d1	banánový
masakr	masakr	k1gInSc1	masakr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
proplétají	proplétat	k5eAaImIp3nP	proplétat
reálné	reálný	k2eAgInPc1d1	reálný
prvky	prvek	k1gInPc1	prvek
s	s	k7c7	s
magičnem	magično	k1gNnSc7	magično
<g/>
,	,	kIx,	,
nadpřirozené	nadpřirozený	k2eAgInPc1d1	nadpřirozený
jevy	jev	k1gInPc1	jev
(	(	kIx(	(
<g/>
např.	např.	kA	např.
létající	létající	k2eAgInPc1d1	létající
koberce	koberec	k1gInPc1	koberec
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
popisovány	popisovat	k5eAaImNgInP	popisovat
zcela	zcela	k6eAd1	zcela
samozřejmě	samozřejmě	k6eAd1	samozřejmě
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
například	například	k6eAd1	například
led	led	k1gInSc1	led
je	být	k5eAaImIp3nS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
jako	jako	k8xC	jako
naprostý	naprostý	k2eAgInSc1d1	naprostý
zázrak	zázrak	k1gInSc1	zázrak
<g/>
.	.	kIx.	.
</s>
<s>
Záležitosti	záležitost	k1gFnPc1	záležitost
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
proplétají	proplétat	k5eAaImIp3nP	proplétat
s	s	k7c7	s
dějinami	dějiny	k1gFnPc7	dějiny
jihoamerického	jihoamerický	k2eAgInSc2d1	jihoamerický
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
postavy	postava	k1gFnPc1	postava
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
nepřirozeně	přirozeně	k6eNd1	přirozeně
vysokého	vysoký	k2eAgInSc2d1	vysoký
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
vyprávění	vyprávění	k1gNnSc1	vyprávění
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vrací	vracet	k5eAaImIp3nS	vracet
nazpět	nazpět	k6eAd1	nazpět
<g/>
,	,	kIx,	,
časové	časový	k2eAgFnPc1d1	časová
roviny	rovina	k1gFnPc1	rovina
se	se	k3xPyFc4	se
proplétají	proplétat	k5eAaImIp3nP	proplétat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyvolána	vyvolán	k2eAgFnSc1d1	vyvolána
iluze	iluze	k1gFnSc1	iluze
věčného	věčný	k2eAgNnSc2d1	věčné
opakování	opakování	k1gNnSc2	opakování
<g/>
,	,	kIx,	,
návratu	návrat	k1gInSc6	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
rodu	rod	k1gInSc2	rod
i	i	k8xC	i
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
předem	předem	k6eAd1	předem
popsána	popsat	k5eAaPmNgFnS	popsat
na	na	k7c6	na
pergamenech	pergamen	k1gInPc6	pergamen
cikánského	cikánský	k2eAgMnSc4d1	cikánský
kouzelníka	kouzelník	k1gMnSc4	kouzelník
Melquíadese	Melquíadese	k1gFnSc2	Melquíadese
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
předčasnému	předčasný	k2eAgMnSc3d1	předčasný
vyzrazení	vyzrazení	k1gNnPc2	vyzrazení
osudu	osud	k1gInSc2	osud
Maconda	Macond	k1gMnSc2	Macond
zabránil	zabránit	k5eAaPmAgMnS	zabránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
své	svůj	k3xOyFgNnSc4	svůj
proroctví	proroctví	k1gNnSc4	proroctví
zapsal	zapsat	k5eAaPmAgMnS	zapsat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
rodném	rodný	k2eAgInSc6d1	rodný
sanskrtu	sanskrt	k1gInSc6	sanskrt
<g/>
,	,	kIx,	,
zašifroval	zašifrovat	k5eAaPmAgMnS	zašifrovat
a	a	k8xC	a
sestavil	sestavit	k5eAaPmAgMnS	sestavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
významné	významný	k2eAgFnSc2d1	významná
události	událost	k1gFnSc2	událost
proložil	proložit	k5eAaPmAgInS	proložit
jevy	jev	k1gInPc4	jev
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
osudy	osud	k1gInPc4	osud
hrdinů	hrdina	k1gMnPc2	hrdina
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
rodové	rodový	k2eAgNnSc1d1	rodové
prokletí	prokletí	k1gNnSc1	prokletí
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
je	být	k5eAaImIp3nS	být
chronická	chronický	k2eAgFnSc1d1	chronická
samota	samota	k1gFnSc1	samota
všech	všecek	k3xTgMnPc2	všecek
jeho	jeho	k3xOp3gMnPc2	jeho
příslušníků	příslušník	k1gMnPc2	příslušník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
obklopeni	obklopen	k2eAgMnPc1d1	obklopen
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
překonat	překonat	k5eAaPmF	překonat
své	svůj	k3xOyFgFnPc4	svůj
osamělé	osamělý	k2eAgFnPc4d1	osamělá
vášně	vášeň	k1gFnPc4	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
zakladatel	zakladatel	k1gMnSc1	zakladatel
Maconda	Macond	k1gMnSc2	Macond
José	José	k1gNnSc2	José
Arcadio	Arcadio	k1gMnSc1	Arcadio
Buendía	Buendía	k1gMnSc1	Buendía
je	být	k5eAaImIp3nS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
své	svůj	k3xOyFgFnSc2	svůj
bezbřehé	bezbřehý	k2eAgFnSc2d1	bezbřehá
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něco	něco	k3yInSc4	něco
nadchne	nadchnout	k5eAaPmIp3nS	nadchnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nic	nic	k3yNnSc1	nic
nedotáhne	dotáhnout	k5eNaPmIp3nS	dotáhnout
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
osamocenost	osamocenost	k1gFnSc1	osamocenost
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
šílenstvím	šílenství	k1gNnSc7	šílenství
a	a	k8xC	a
posledních	poslední	k2eAgNnPc2d1	poslední
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
stráví	strávit	k5eAaPmIp3nS	strávit
venku	venku	k6eAd1	venku
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
přivázaný	přivázaný	k2eAgInSc1d1	přivázaný
ke	k	k7c3	k
stromu	strom	k1gInSc3	strom
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Úrsula	Úrsula	k1gFnSc1	Úrsula
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Iguaránová	Iguaránová	k1gFnSc1	Iguaránová
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
zcela	zcela	k6eAd1	zcela
pohlcena	pohltit	k5eAaPmNgFnS	pohltit
pozemskými	pozemský	k2eAgFnPc7d1	pozemská
starostmi	starost	k1gFnPc7	starost
a	a	k8xC	a
vyděláváním	vydělávání	k1gNnSc7	vydělávání
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
plukovník	plukovník	k1gMnSc1	plukovník
Aureliano	Aureliana	k1gFnSc5	Aureliana
Buendía	Buendí	k2eAgNnPc4d1	Buendí
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgInS	mít
nikdy	nikdy	k6eAd1	nikdy
nikoho	nikdo	k3yNnSc4	nikdo
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
zbytečné	zbytečný	k2eAgFnSc2d1	zbytečná
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
plodil	plodit	k5eAaImAgInS	plodit
děti	dítě	k1gFnPc4	dítě
se	s	k7c7	s
ženami	žena	k1gFnPc7	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
nemiloval	milovat	k5eNaImAgMnS	milovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
propadal	propadat	k5eAaPmAgMnS	propadat
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
pýchy	pýcha	k1gFnSc2	pýcha
a	a	k8xC	a
samoty	samota	k1gFnSc2	samota
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
postavy	postav	k1gInPc1	postav
jsou	být	k5eAaImIp3nP	být
něčím	něco	k3yInSc7	něco
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
až	až	k6eAd1	až
na	na	k7c6	na
Krásnou	krásný	k2eAgFnSc7d1	krásná
Remedios	Remediosa	k1gFnPc2	Remediosa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zaživa	zaživa	k6eAd1	zaživa
vzata	vzít	k5eAaPmNgFnS	vzít
na	na	k7c4	na
nebesa	nebesa	k1gNnPc4	nebesa
<g/>
,	,	kIx,	,
trpí	trpět	k5eAaImIp3nP	trpět
nějakou	nějaký	k3yIgFnSc7	nějaký
vášní	vášeň	k1gFnSc7	vášeň
nebo	nebo	k8xC	nebo
obsesí	obsese	k1gFnSc7	obsese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
velké	velký	k2eAgNnSc4d1	velké
podobenství	podobenství	k1gNnSc4	podobenství
o	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
samotě	samota	k1gFnSc6	samota
<g/>
,	,	kIx,	,
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
o	o	k7c6	o
stárnutí	stárnutí	k1gNnSc6	stárnutí
a	a	k8xC	a
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodokmen	rodokmen	k1gInSc1	rodokmen
rodiny	rodina	k1gFnSc2	rodina
Buendíů	Buendí	k1gInPc2	Buendí
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Marquéz	Marquéza	k1gFnPc2	Marquéza
<g/>
,	,	kIx,	,
Gabriel	Gabriel	k1gMnSc1	Gabriel
García	García	k1gMnSc1	García
<g/>
.	.	kIx.	.
</s>
<s>
Sto	sto	k4xCgNnSc1	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
španělštiny	španělština	k1gFnSc2	španělština
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Medek	Medek	k1gMnSc1	Medek
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
320	[number]	k4	320
S.	S.	kA	S.
ISBN	ISBN	kA	ISBN
978-80-207-1438-1	[number]	k4	978-80-207-1438-1
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Láska	láska	k1gFnSc1	láska
za	za	k7c2	za
časů	čas	k1gInPc2	čas
cholery	cholera	k1gFnSc2	cholera
</s>
</p>
<p>
<s>
Kronika	kronika	k1gFnSc1	kronika
ohlášené	ohlášený	k2eAgFnSc2d1	ohlášená
smrti	smrt	k1gFnSc2	smrt
</s>
</p>
<p>
<s>
Magický	magický	k2eAgInSc1d1	magický
realismus	realismus	k1gInSc1	realismus
</s>
</p>
<p>
<s>
100	[number]	k4	100
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
knih	kniha	k1gFnPc2	kniha
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
Le	Le	k1gFnSc2	Le
Monde	Mond	k1gMnSc5	Mond
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Rozbor	rozbor	k1gInSc1	rozbor
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
(	(	kIx(	(
<g/>
prof.	prof.	kA	prof.
A.	A.	kA	A.
Housková	houskový	k2eAgFnSc1d1	Housková
<g/>
)	)	kIx)	)
</s>
</p>
