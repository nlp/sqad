<s>
Ligurské	ligurský	k2eAgNnSc1d1	Ligurské
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Mer	Mer	k1gMnSc5	Mer
Ligure	Ligur	k1gMnSc5	Ligur
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
Mar	Mar	k1gMnSc5	Mar
Ligure	Ligur	k1gMnSc5	Ligur
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
