<s>
Ligurské	ligurský	k2eAgNnSc1d1	Ligurské
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Mer	Mer	k1gMnSc5	Mer
Ligure	Ligur	k1gMnSc5	Ligur
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
Mar	Mar	k1gMnSc5	Mar
Ligure	Ligur	k1gMnSc5	Ligur
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
mezi	mezi	k7c7	mezi
ligurským	ligurský	k2eAgNnSc7d1	Ligurské
pobřežím	pobřeží	k1gNnSc7	pobřeží
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Korsikou	Korsika	k1gFnSc7	Korsika
<g/>
,	,	kIx,	,
Elbou	Elbý	k2eAgFnSc7d1	Elbý
a	a	k8xC	a
východní	východní	k2eAgFnSc7d1	východní
částí	část	k1gFnSc7	část
Azurového	azurový	k2eAgNnSc2d1	azurové
pobřeží	pobřeží	k1gNnSc2	pobřeží
(	(	kIx(	(
<g/>
francouzské	francouzský	k2eAgFnSc2d1	francouzská
riviéry	riviéra	k1gFnSc2	riviéra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
plochu	plocha	k1gFnSc4	plocha
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
000	[number]	k4	000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
největší	veliký	k2eAgFnSc1d3	veliký
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
569	[number]	k4	569
m.	m.	k?	m.
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
přístavy	přístav	k1gInPc4	přístav
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
patří	patřit	k5eAaImIp3nS	patřit
Janov	Janov	k1gInSc1	Janov
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
Spezia	Spezia	k1gFnSc1	Spezia
a	a	k8xC	a
Livorno	Livorno	k1gNnSc1	Livorno
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ligurské	ligurský	k2eAgFnSc2d1	Ligurská
moře	moře	k1gNnSc4	moře
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
