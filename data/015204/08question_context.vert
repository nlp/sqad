<s desamb="1">
Krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
již	již	k6eAd1
s	s	k7c7
příchodem	příchod	k1gInSc7
zemědělství	zemědělství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
nejstarší	starý	k2eAgFnPc4d3
zásady	zásada	k1gFnPc4
krevní	krevní	k2eAgFnSc2d1
msty	msta	k1gFnSc2
patří	patřit	k5eAaImIp3nS
„	„	k?
<g/>
krev	krev	k1gFnSc4
za	za	k7c4
krev	krev	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
takže	takže	k8xS
vražda	vražda	k1gFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
pomstěna	pomstěn	k2eAgFnSc1d1
vraždou	vražda	k1gFnSc7
a	a	k8xC
„	„	k?
<g/>
všichni	všechen	k3xTgMnPc1
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k8xC
vražda	vražda	k1gFnSc1
je	být	k5eAaImIp3nS
kolektivní	kolektivní	k2eAgFnSc7d1
vinou	vina	k1gFnSc7
celého	celý	k2eAgInSc2d1
rodu	rod	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
nabývala	nabývat	k5eAaImAgFnS
krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
rázu	ráz	k1gInSc2
války	válka	k1gFnSc2
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
rody	rod	k1gInPc7
a	a	k8xC
vražda	vražda	k1gFnSc1
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
vykoupena	vykoupen	k2eAgFnSc1d1
více	hodně	k6eAd2
vraždami	vražda	k1gFnPc7
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>