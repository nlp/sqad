<s>
Max	Max	k1gMnSc1	Max
Karl	Karl	k1gMnSc1	Karl
Ernst	Ernst	k1gMnSc1	Ernst
Ludwig	Ludwig	k1gMnSc1	Ludwig
Planck	Planck	k1gMnSc1	Planck
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1858	[number]	k4	1858
Kiel	Kiel	k1gInSc1	Kiel
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1947	[number]	k4	1947
Göttingen	Göttingen	k1gNnSc2	Göttingen
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
