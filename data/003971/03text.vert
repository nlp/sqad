<s>
Slzy	slza	k1gFnPc1	slza
jsou	být	k5eAaImIp3nP	být
produkty	produkt	k1gInPc7	produkt
slzných	slzný	k2eAgFnPc2d1	slzná
žláz	žláza	k1gFnPc2	žláza
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ostatních	ostatní	k2eAgMnPc2d1	ostatní
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
dalších	další	k2eAgMnPc2d1	další
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
především	především	k9	především
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vyplavují	vyplavovat	k5eAaImIp3nP	vyplavovat
z	z	k7c2	z
oka	oko	k1gNnSc2	oko
nečistoty	nečistota	k1gFnSc2	nečistota
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
je	on	k3xPp3gInPc4	on
před	před	k7c4	před
mikroby	mikrob	k1gInPc4	mikrob
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
mají	mít	k5eAaImIp3nP	mít
funkci	funkce	k1gFnSc4	funkce
vyživovací	vyživovací	k2eAgFnSc4d1	vyživovací
pro	pro	k7c4	pro
oko	oko	k1gNnSc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kapky	kapka	k1gFnPc4	kapka
slané	slaný	k2eAgFnSc2d1	slaná
vody	voda	k1gFnSc2	voda
obsahující	obsahující	k2eAgInSc4d1	obsahující
malý	malý	k2eAgInSc4d1	malý
podíl	podíl	k1gInSc4	podíl
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Slzení	slzení	k1gNnSc1	slzení
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyvoláno	vyvolat	k5eAaPmNgNnS	vyvolat
pocitem	pocit	k1gInSc7	pocit
smutku	smutek	k1gInSc2	smutek
<g/>
,	,	kIx,	,
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
slzení	slzení	k1gNnSc1	slzení
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
pláč	pláč	k1gInSc4	pláč
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
pláč	pláč	k1gInSc1	pláč
vyvolán	vyvolat	k5eAaPmNgInS	vyvolat
jinou	jiný	k2eAgFnSc7d1	jiná
silnou	silný	k2eAgFnSc7d1	silná
emocí	emoce	k1gFnSc7	emoce
nebo	nebo	k8xC	nebo
pocitem	pocit	k1gInSc7	pocit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
smíchem	smích	k1gInSc7	smích
<g/>
,	,	kIx,	,
vztekem	vztek	k1gInSc7	vztek
<g/>
,	,	kIx,	,
zoufalstvím	zoufalství	k1gNnSc7	zoufalství
nebo	nebo	k8xC	nebo
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
slzení	slzení	k1gNnSc1	slzení
vyvoláno	vyvolán	k2eAgNnSc1d1	vyvoláno
chemickým	chemický	k2eAgNnSc7d1	chemické
podrážděním	podráždění	k1gNnSc7	podráždění
slzných	slzný	k2eAgFnPc2d1	slzná
žláz	žláza	k1gFnPc2	žláza
nějakou	nějaký	k3yIgFnSc7	nějaký
chemickou	chemický	k2eAgFnSc7d1	chemická
látkou	látka	k1gFnSc7	látka
např.	např.	kA	např.
slzným	slzný	k2eAgInSc7d1	slzný
plynem	plyn	k1gInSc7	plyn
(	(	kIx(	(
<g/>
bromaceton	bromaceton	k1gInSc1	bromaceton
<g/>
,	,	kIx,	,
bromacetofenon	bromacetofenon	k1gMnSc1	bromacetofenon
nebo	nebo	k8xC	nebo
chloracetofenon	chloracetofenon	k1gMnSc1	chloracetofenon
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Běžně	běžně	k6eAd1	běžně
slzy	slza	k1gFnPc4	slza
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
pach	pach	k1gInSc1	pach
cibule	cibule	k1gFnSc2	cibule
nebo	nebo	k8xC	nebo
požití	požití	k1gNnSc1	požití
velmi	velmi	k6eAd1	velmi
pálivé	pálivý	k2eAgFnPc4d1	pálivá
potraviny	potravina	k1gFnPc4	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Slzy	slza	k1gFnPc1	slza
mají	mít	k5eAaImIp3nP	mít
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
látkové	látkový	k2eAgFnSc6d1	látková
výměně	výměna	k1gFnSc6	výměna
rohovky	rohovka	k1gFnSc2	rohovka
a	a	k8xC	a
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
průhlednosti	průhlednost	k1gFnSc6	průhlednost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
během	během	k7c2	během
slzení	slzení	k1gNnSc2	slzení
přivádí	přivádět	k5eAaImIp3nS	přivádět
vlivem	vliv	k1gInSc7	vliv
slzného	slzný	k2eAgInSc2d1	slzný
filmu	film	k1gInSc2	film
kyslík	kyslík	k1gInSc1	kyslík
získávaný	získávaný	k2eAgInSc1d1	získávaný
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Průhlednost	průhlednost	k1gFnSc1	průhlednost
je	být	k5eAaImIp3nS	být
udržována	udržovat	k5eAaImNgFnS	udržovat
odstraňováním	odstraňování	k1gNnSc7	odstraňování
drobných	drobný	k2eAgFnPc2d1	drobná
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
rozprostření	rozprostření	k1gNnSc1	rozprostření
po	po	k7c6	po
oku	oko	k1gNnSc6	oko
také	také	k9	také
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
jeho	jeho	k3xOp3gFnPc4	jeho
nepravidelnosti	nepravidelnost	k1gFnPc4	nepravidelnost
a	a	k8xC	a
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
viditelnost	viditelnost	k1gFnSc4	viditelnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
omývání	omývání	k1gNnSc1	omývání
přední	přední	k2eAgFnSc4d1	přední
plochu	plocha	k1gFnSc4	plocha
oční	oční	k2eAgFnSc2d1	oční
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1	lidská
slzy	slza	k1gFnPc1	slza
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
99	[number]	k4	99
%	%	kIx~	%
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
smíšena	smísit	k5eAaPmNgFnS	smísit
přibližně	přibližně	k6eAd1	přibližně
s	s	k7c7	s
1	[number]	k4	1
%	%	kIx~	%
soli	sůl	k1gFnSc2	sůl
a	a	k8xC	a
0,2	[number]	k4	0,2
až	až	k9	až
0,5	[number]	k4	0,5
%	%	kIx~	%
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
glukóza	glukóza	k1gFnSc1	glukóza
<g/>
,	,	kIx,	,
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
a	a	k8xC	a
fermenty	ferment	k1gInPc1	ferment
<g/>
.	.	kIx.	.
<g/>
Chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
slz	slza	k1gFnPc2	slza
má	mít	k5eAaImIp3nS	mít
antibakteriální	antibakteriální	k2eAgInPc4d1	antibakteriální
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ničení	ničení	k1gNnSc3	ničení
bakterií	bakterie	k1gFnPc2	bakterie
na	na	k7c6	na
oku	oko	k1gNnSc6	oko
i	i	k8xC	i
v	v	k7c6	v
spojivkovém	spojivkový	k2eAgInSc6d1	spojivkový
vaku	vak	k1gInSc6	vak
<g/>
.	.	kIx.	.
</s>
<s>
Slzné	slzný	k2eAgNnSc1d1	slzné
ústrojí	ústrojí	k1gNnSc1	ústrojí
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
blíže	blízce	k6eAd2	blízce
nosu	nos	k1gInSc2	nos
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
složeno	složen	k2eAgNnSc1d1	složeno
ze	z	k7c2	z
slzné	slzný	k2eAgFnSc2d1	slzná
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
přídavnách	přídavna	k1gFnPc6	přídavna
slzných	slzný	k2eAgFnPc2d1	slzná
žlázek	žlázka	k1gFnPc2	žlázka
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
a	a	k8xC	a
dolním	dolní	k2eAgNnSc6d1	dolní
víčku	víčko	k1gNnSc6	víčko
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
žlázek	žlázka	k1gFnPc2	žlázka
obsažených	obsažený	k2eAgFnPc2d1	obsažená
ve	v	k7c6	v
spojivce	spojivka	k1gFnSc6	spojivka
<g/>
,	,	kIx,	,
slzných	slzný	k2eAgInPc2d1	slzný
kanálků	kanálek	k1gInPc2	kanálek
a	a	k8xC	a
slzného	slzný	k2eAgInSc2d1	slzný
vaku	vak	k1gInSc2	vak
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
slzy	slza	k1gFnPc1	slza
hromadí	hromadit	k5eAaImIp3nP	hromadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
jsou	být	k5eAaImIp3nP	být
vypouštěny	vypouštět	k5eAaImNgFnP	vypouštět
slzovodem	slzovod	k1gInSc7	slzovod
do	do	k7c2	do
horního	horní	k2eAgInSc2d1	horní
nosního	nosní	k2eAgInSc2d1	nosní
průchodu	průchod	k1gInSc2	průchod
<g/>
.	.	kIx.	.
</s>
<s>
Slzy	slza	k1gFnPc1	slza
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
neustále	neustále	k6eAd1	neustále
nezávisle	závisle	k6eNd1	závisle
průběžně	průběžně	k6eAd1	průběžně
na	na	k7c4	na
vůli	vůle	k1gFnSc4	vůle
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
soustavně	soustavně	k6eAd1	soustavně
vylučovány	vylučovat	k5eAaImNgInP	vylučovat
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
tato	tento	k3xDgFnSc1	tento
sekrece	sekrece	k1gFnSc1	sekrece
ustává	ustávat	k5eAaImIp3nS	ustávat
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
různých	různý	k2eAgFnPc2d1	různá
příčin	příčina	k1gFnPc2	příčina
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
sekrece	sekrece	k1gFnSc1	sekrece
slz	slza	k1gFnPc2	slza
zvýšit	zvýšit	k5eAaPmF	zvýšit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
emociální	emociální	k2eAgInSc1d1	emociální
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Slovo	slovo	k1gNnSc1	slovo
slza	slza	k1gFnSc1	slza
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
někdy	někdy	k6eAd1	někdy
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
nějaké	nějaký	k3yIgFnSc2	nějaký
kapaliny	kapalina	k1gFnSc2	kapalina
(	(	kIx(	(
<g/>
špetka	špetka	k1gFnSc1	špetka
<g/>
,	,	kIx,	,
ždibec	ždibec	k?	ždibec
<g/>
,	,	kIx,	,
kapka	kapka	k1gFnSc1	kapka
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smích	smích	k1gInSc1	smích
Pláč	pláč	k1gInSc1	pláč
Smutek	smutek	k1gInSc1	smutek
Slzivost	Slzivost	k1gFnSc1	Slzivost
a	a	k8xC	a
vliv	vliv	k1gInSc1	vliv
slz	slza	k1gFnPc2	slza
na	na	k7c4	na
nošení	nošení	k1gNnSc4	nošení
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
slzy	slza	k1gFnSc2	slza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
slza	slza	k1gFnSc1	slza
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
VONDRÁČEK	Vondráček	k1gMnSc1	Vondráček
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
;	;	kIx,	;
HOLUB	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Fantastické	fantastický	k2eAgInPc1d1	fantastický
a	a	k8xC	a
magické	magický	k2eAgInPc1d1	magický
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Columbus	Columbus	k1gMnSc1	Columbus
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7136	[number]	k4	7136
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
