<s>
Minaret	minaret	k1gInSc1	minaret
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
mizana	mizan	k1gMnSc2	mizan
<g/>
,	,	kIx,	,
manára	manár	k1gMnSc2	manár
<g/>
,	,	kIx,	,
מ	מ	k?	מ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
mešity	mešita	k1gFnSc2	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
věžovitou	věžovitý	k2eAgFnSc4d1	věžovitá
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
štíhlého	štíhlý	k2eAgInSc2d1	štíhlý
okurkovitého	okurkovitý	k2eAgInSc2d1	okurkovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc4d1	vysoká
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
okolo	okolo	k7c2	okolo
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
mešit	mešita	k1gFnPc2	mešita
i	i	k9	i
přes	přes	k7c4	přes
200	[number]	k4	200
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
minarety	minaret	k1gInPc4	minaret
má	mít	k5eAaImIp3nS	mít
Mešita	mešita	k1gFnSc1	mešita
Hasana	Hasana	k1gFnSc1	Hasana
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
Casablance	Casablanca	k1gFnSc6	Casablanca
(	(	kIx(	(
<g/>
210	[number]	k4	210
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
minarety	minaret	k1gInPc1	minaret
Velké	velký	k2eAgFnSc2d1	velká
mešity	mešita	k1gFnSc2	mešita
v	v	k7c6	v
Teheránu	Teherán	k1gInSc6	Teherán
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
mít	mít	k5eAaImF	mít
230	[number]	k4	230
m.	m.	k?	m.
K	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
i	i	k9	i
minarety	minaret	k1gInPc1	minaret
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgInPc1d1	malý
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
horských	horský	k2eAgFnPc6d1	horská
vesničkách	vesnička	k1gFnPc6	vesnička
nepřesahují	přesahovat	k5eNaImIp3nP	přesahovat
5	[number]	k4	5
m.	m.	k?	m.
Nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
kamenné	kamenný	k2eAgInPc1d1	kamenný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
sklo	sklo	k1gNnSc1	sklo
s	s	k7c7	s
kovovou	kovový	k2eAgFnSc7d1	kovová
konstrukcí	konstrukce	k1gFnSc7	konstrukce
není	být	k5eNaImIp3nS	být
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
jsou	být	k5eAaImIp3nP	být
četné	četný	k2eAgInPc1d1	četný
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Minaret	minaret	k1gInSc1	minaret
má	mít	k5eAaImIp3nS	mít
sloužit	sloužit	k5eAaImF	sloužit
ke	k	k7c3	k
svolávání	svolávání	k1gNnSc3	svolávání
k	k	k7c3	k
modlitbě	modlitba	k1gFnSc3	modlitba
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
mešity	mešita	k1gFnSc2	mešita
samotné	samotný	k2eAgFnSc2d1	samotná
nebo	nebo	k8xC	nebo
stojí	stát	k5eAaImIp3nS	stát
samostatně	samostatně	k6eAd1	samostatně
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
chrámu	chrám	k1gInSc2	chrám
Hagia	Hagius	k1gMnSc2	Hagius
Sophia	Sophius	k1gMnSc2	Sophius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nahoře	nahoře	k6eAd1	nahoře
je	být	k5eAaImIp3nS	být
ochoz	ochoz	k1gInSc1	ochoz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dříve	dříve	k6eAd2	dříve
stával	stávat	k5eAaImAgInS	stávat
muezzin	muezzin	k1gMnSc1	muezzin
a	a	k8xC	a
svolával	svolávat	k5eAaImAgMnS	svolávat
věřící	věřící	k1gFnSc4	věřící
k	k	k7c3	k
modlitbám	modlitba	k1gFnPc3	modlitba
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
byla	být	k5eAaImAgFnS	být
malá	malý	k2eAgFnSc1d1	malá
stříška	stříška	k1gFnSc1	stříška
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgInS	být
posazen	posazen	k2eAgInSc4d1	posazen
půlměsíc	půlměsíc	k1gInSc4	půlměsíc
jako	jako	k8xS	jako
symbol	symbol	k1gInSc4	symbol
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
muezzini	muezzin	k1gMnPc1	muezzin
nevolají	volat	k5eNaImIp3nP	volat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nahradily	nahradit	k5eAaPmAgInP	nahradit
je	on	k3xPp3gFnPc4	on
moderní	moderní	k2eAgInPc1d1	moderní
reproduktory	reproduktor	k1gInPc1	reproduktor
a	a	k8xC	a
megafony	megafon	k1gInPc1	megafon
<g/>
,	,	kIx,	,
výzva	výzva	k1gFnSc1	výzva
k	k	k7c3	k
modlitbě	modlitba	k1gFnSc3	modlitba
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
muezzinů	muezzin	k1gMnPc2	muezzin
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
mezi	mezi	k7c7	mezi
muslimy	muslim	k1gMnPc7	muslim
o	o	k7c4	o
prestižní	prestižní	k2eAgFnSc4d1	prestižní
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
jeden	jeden	k4xCgInSc4	jeden
-	-	kIx~	-
mívá	mívat	k5eAaImIp3nS	mívat
většina	většina	k1gFnSc1	většina
malých	malý	k2eAgFnPc2d1	malá
mešit	mešita	k1gFnPc2	mešita
dva	dva	k4xCgMnPc1	dva
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnPc4d2	veliký
mešity	mešita	k1gFnPc4	mešita
ve	v	k7c6	v
městech	město	k1gNnPc6	město
čtyři	čtyři	k4xCgFnPc1	čtyři
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
Hagia	Hagia	k1gFnSc1	Hagia
Sofia	Sofia	k1gFnSc1	Sofia
<g/>
,	,	kIx,	,
Sulejmanova	Sulejmanův	k2eAgFnSc1d1	Sulejmanova
mešita	mešita	k1gFnSc1	mešita
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgFnPc4d1	významná
mešity	mešita	k1gFnPc4	mešita
šest	šest	k4xCc1	šest
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
Modrá	modrý	k2eAgFnSc1d1	modrá
mešita	mešita	k1gFnSc1	mešita
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
porušení	porušení	k1gNnSc4	porušení
zvyklostí	zvyklost	k1gFnPc2	zvyklost
devět	devět	k4xCc1	devět
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
mešita	mešita	k1gFnSc1	mešita
v	v	k7c6	v
Mekce	Mekka	k1gFnSc6	Mekka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
muslimů	muslim	k1gMnPc2	muslim
deset	deset	k4xCc1	deset
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
Prorokova	prorokův	k2eAgFnSc1d1	Prorokova
mešita	mešita	k1gFnSc1	mešita
v	v	k7c6	v
Medině	Medina	k1gFnSc6	Medina
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Minaret	minaret	k1gInSc1	minaret
(	(	kIx(	(
<g/>
Lednicko-valtický	lednickoaltický	k2eAgInSc1d1	lednicko-valtický
areál	areál	k1gInSc1	areál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Minaret	minaret	k1gInSc1	minaret
je	být	k5eAaImIp3nS	být
také	také	k9	také
prvek	prvek	k1gInSc1	prvek
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
sadovnické	sadovnický	k2eAgFnSc6d1	Sadovnická
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
minaret	minaret	k1gInSc1	minaret
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Lednicko-valtickém	lednickoaltický	k2eAgInSc6d1	lednicko-valtický
areálu	areál	k1gInSc6	areál
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
jako	jako	k8xC	jako
dekorace	dekorace	k1gFnSc1	dekorace
v	v	k7c6	v
době	doba	k1gFnSc6	doba
romantismu	romantismus	k1gInSc2	romantismus
společně	společně	k6eAd1	společně
s	s	k7c7	s
umělými	umělý	k2eAgFnPc7d1	umělá
hradními	hradní	k2eAgFnPc7d1	hradní
zříceninami	zřícenina	k1gFnPc7	zřícenina
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
stavbami	stavba	k1gFnPc7	stavba
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
turistická	turistický	k2eAgFnSc1d1	turistická
atrakce	atrakce	k1gFnSc1	atrakce
a	a	k8xC	a
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
několika	několik	k4yIc7	několik
lety	léto	k1gNnPc7	léto
existovaly	existovat	k5eAaImAgInP	existovat
plány	plán	k1gInPc1	plán
na	na	k7c4	na
výstavby	výstavba	k1gFnSc2	výstavba
několika	několik	k4yIc2	několik
mešit	mešita	k1gFnPc2	mešita
s	s	k7c7	s
minarety	minaret	k1gInPc7	minaret
i	i	k8xC	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
Teplice	teplice	k1gFnSc1	teplice
<g/>
,	,	kIx,	,
Orlová	Orlová	k1gFnSc1	Orlová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byly	být	k5eAaImAgFnP	být
zamítnuty	zamítnut	k2eAgFnPc1d1	zamítnuta
nebo	nebo	k8xC	nebo
zkrachovaly	zkrachovat	k5eAaPmAgFnP	zkrachovat
samy	sám	k3xTgFnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
mešita	mešita	k1gFnSc1	mešita
a	a	k8xC	a
Pražská	pražský	k2eAgFnSc1d1	Pražská
mešita	mešita	k1gFnSc1	mešita
minarety	minaret	k1gInPc4	minaret
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
minaret	minaret	k1gInSc1	minaret
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
minaretů	minaret	k1gInPc2	minaret
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
