<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
věžovitou	věžovitý	k2eAgFnSc4d1	věžovitá
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
štíhlého	štíhlý	k2eAgInSc2d1	štíhlý
okurkovitého	okurkovitý	k2eAgInSc2d1	okurkovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc4d1	vysoká
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
okolo	okolo	k7c2	okolo
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
mešit	mešita	k1gFnPc2	mešita
i	i	k9	i
přes	přes	k7c4	přes
200	[number]	k4	200
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
