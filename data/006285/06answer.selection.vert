<s>
Pohřbu	pohřeb	k1gInSc2	pohřeb
prezidenta	prezident	k1gMnSc2	prezident
Johna	John	k1gMnSc2	John
Fitzgeralda	Fitzgerald	k1gMnSc2	Fitzgerald
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
na	na	k7c4	na
220	[number]	k4	220
státníků	státník	k1gMnPc2	státník
ze	z	k7c2	z
102	[number]	k4	102
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
