<s>
Telepatie	telepatie	k1gFnSc1	telepatie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
τ	τ	k?	τ
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
a	a	k8xC	a
π	π	k?	π
cítění	cítění	k1gNnSc1	cítění
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
údajný	údajný	k2eAgInSc1d1	údajný
přenos	přenos	k1gInSc1	přenos
informací	informace	k1gFnPc2	informace
umožněný	umožněný	k2eAgMnSc1d1	umožněný
mimosmyslovými	mimosmyslový	k2eAgFnPc7d1	mimosmyslová
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nevyužívající	využívající	k2eNgFnPc4d1	nevyužívající
technické	technický	k2eAgFnPc4d1	technická
pomůcky	pomůcka	k1gFnPc4	pomůcka
ani	ani	k8xC	ani
známé	známý	k2eAgInPc4d1	známý
smysly	smysl	k1gInPc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jev	jev	k1gInSc1	jev
ji	on	k3xPp3gFnSc4	on
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
parapsychologie	parapsychologie	k1gFnSc1	parapsychologie
či	či	k8xC	či
psychotronika	psychotronika	k1gFnSc1	psychotronika
<g/>
.	.	kIx.	.
</s>
<s>
Zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
vykládají	vykládat	k5eAaImIp3nP	vykládat
jako	jako	k9	jako
telepatii	telepatie	k1gFnSc4	telepatie
<g/>
,	,	kIx,	,
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
také	také	k9	také
psychiatrie	psychiatrie	k1gFnSc1	psychiatrie
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc1	psychologie
a	a	k8xC	a
hlubinná	hlubinný	k2eAgFnSc1d1	hlubinná
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
některých	některý	k3yIgNnPc2	některý
nových	nový	k2eAgNnPc2d1	nové
náboženských	náboženský	k2eAgNnPc2d1	náboženské
hnutí	hnutí	k1gNnPc2	hnutí
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gNnSc1	jejich
učení	učení	k1gNnSc1	učení
předáno	předat	k5eAaPmNgNnS	předat
rovněž	rovněž	k6eAd1	rovněž
telepatickou	telepatický	k2eAgFnSc7d1	telepatická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Víra	víra	k1gFnSc1	víra
v	v	k7c6	v
telepatii	telepatie	k1gFnSc6	telepatie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
společnostech	společnost	k1gFnPc6	společnost
poměrně	poměrně	k6eAd1	poměrně
běžná	běžný	k2eAgFnSc1d1	běžná
a	a	k8xC	a
telepatie	telepatie	k1gFnSc1	telepatie
je	být	k5eAaImIp3nS	být
časté	častý	k2eAgNnSc1d1	časté
téma	téma	k1gNnSc1	téma
i	i	k9	i
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Existence	existence	k1gFnSc1	existence
telepatie	telepatie	k1gFnSc1	telepatie
není	být	k5eNaImIp3nS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
přijímaná	přijímaný	k2eAgFnSc1d1	přijímaná
vědecká	vědecký	k2eAgFnSc1d1	vědecká
teorie	teorie	k1gFnSc1	teorie
nevysvětluje	vysvětlovat	k5eNaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
telepatie	telepatie	k1gFnSc1	telepatie
mohla	moct	k5eAaImAgFnS	moct
fungovat	fungovat	k5eAaImF	fungovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokusy	pokus	k1gInPc1	pokus
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
experimentální	experimentální	k2eAgFnSc2d1	experimentální
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
konaných	konaný	k2eAgFnPc2d1	konaná
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
zakázku	zakázka	k1gFnSc4	zakázka
<g/>
,	,	kIx,	,
nenašly	najít	k5eNaPmAgFnP	najít
empirický	empirický	k2eAgInSc4d1	empirický
důkaz	důkaz	k1gInSc4	důkaz
existence	existence	k1gFnSc2	existence
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
obtížné	obtížný	k2eAgFnSc6d1	obtížná
telepatii	telepatie	k1gFnSc6	telepatie
definičně	definičně	k6eAd1	definičně
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
parapsychologických	parapsychologický	k2eAgInPc2d1	parapsychologický
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jasnozřivosti	jasnozřivost	k1gFnSc2	jasnozřivost
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
telepatie	telepatie	k1gFnSc2	telepatie
zavedl	zavést	k5eAaPmAgInS	zavést
Frederic	Frederic	k1gMnSc1	Frederic
W.	W.	kA	W.
H.	H.	kA	H.
Myers	Myers	k1gInSc4	Myers
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
Society	societa	k1gFnSc2	societa
for	forum	k1gNnPc2	forum
Psychical	Psychical	k1gMnSc1	Psychical
Research	Research	k1gMnSc1	Research
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
původně	původně	k6eAd1	původně
vyjadřoval	vyjadřovat	k5eAaImAgInS	vyjadřovat
schopnost	schopnost	k1gFnSc4	schopnost
lidského	lidský	k2eAgNnSc2d1	lidské
myšlení	myšlení	k1gNnSc2	myšlení
působit	působit	k5eAaImF	působit
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
zjevné	zjevný	k2eAgFnSc2d1	zjevná
vnější	vnější	k2eAgFnSc2d1	vnější
vibrace	vibrace	k1gFnSc2	vibrace
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
telepatie	telepatie	k1gFnSc2	telepatie
se	se	k3xPyFc4	se
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
metapsychologií	metapsychologie	k1gFnSc7	metapsychologie
<g/>
.	.	kIx.	.
</s>
<s>
Propagátory	propagátor	k1gMnPc4	propagátor
metapsychologie	metapsychologie	k1gFnSc2	metapsychologie
byli	být	k5eAaImAgMnP	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
Viktor	Viktor	k1gMnSc1	Viktor
Mikuška	Mikuška	k1gMnSc1	Mikuška
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Kuchynka	Kuchynka	k1gMnSc1	Kuchynka
<g/>
.	.	kIx.	.
</s>
<s>
Fenomén	fenomén	k1gInSc1	fenomén
telepatie	telepatie	k1gFnSc2	telepatie
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
fenoménem	fenomén	k1gInSc7	fenomén
jasnovidectví	jasnovidectví	k1gNnSc2	jasnovidectví
studován	studován	k2eAgInSc1d1	studován
okultisty	okultista	k1gMnPc7	okultista
tzv.	tzv.	kA	tzv.
metapsychologií	metapsychologie	k1gFnPc2	metapsychologie
subjektivní	subjektivní	k2eAgFnSc1d1	subjektivní
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pojednávala	pojednávat	k5eAaImAgFnS	pojednávat
o	o	k7c6	o
psychických	psychický	k2eAgFnPc6d1	psychická
manifestacích	manifestace	k1gFnPc6	manifestace
vycházejících	vycházející	k2eAgFnPc2d1	vycházející
z	z	k7c2	z
nevědomí	nevědomí	k1gNnSc2	nevědomí
a	a	k8xC	a
podvědomí	podvědomí	k1gNnSc2	podvědomí
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
telepatie	telepatie	k1gFnSc1	telepatie
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
rozuměl	rozumět	k5eAaImAgInS	rozumět
přenos	přenos	k1gInSc1	přenos
dojmů	dojem	k1gInPc2	dojem
(	(	kIx(	(
<g/>
vjemů	vjem	k1gInPc2	vjem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
představ	představa	k1gFnPc2	představa
<g/>
,	,	kIx,	,
myšlenek	myšlenka	k1gFnPc2	myšlenka
atd.	atd.	kA	atd.
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
osoby	osoba	k1gFnSc2	osoba
ke	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
prostřednictví	prostřednictví	k1gNnSc2	prostřednictví
známých	známý	k2eAgInPc2d1	známý
smyslů	smysl	k1gInPc2	smysl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
telepatií	telepatie	k1gFnSc7	telepatie
byly	být	k5eAaImAgInP	být
konány	konán	k2eAgInPc1d1	konán
pokusy	pokus	k1gInPc1	pokus
ve	v	k7c6	v
vojenských	vojenský	k2eAgFnPc6d1	vojenská
laboratořích	laboratoř	k1gFnPc6	laboratoř
USA	USA	kA	USA
i	i	k8xC	i
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
prokázán	prokázat	k5eAaPmNgInS	prokázat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
spíš	spát	k5eAaImIp2nS	spát
shledán	shledat	k5eAaPmNgInS	shledat
vojensky	vojensky	k6eAd1	vojensky
použitelným	použitelný	k2eAgInSc7d1	použitelný
<g/>
.	.	kIx.	.
</s>
<s>
Skeptici	skeptik	k1gMnPc1	skeptik
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
případy	případ	k1gInPc1	případ
telepatie	telepatie	k1gFnSc2	telepatie
jsou	být	k5eAaImIp3nP	být
domnělé	domnělý	k2eAgFnPc1d1	domnělá
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
jako	jako	k9	jako
výsledek	výsledek	k1gInSc4	výsledek
podvodu	podvod	k1gInSc2	podvod
<g/>
,	,	kIx,	,
sebeklamu	sebeklam	k1gInSc2	sebeklam
anebo	anebo	k8xC	anebo
halucinace	halucinace	k1gFnSc2	halucinace
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
není	být	k5eNaImIp3nS	být
důvod	důvod	k1gInSc1	důvod
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
mimosmyslového	mimosmyslový	k2eAgNnSc2d1	mimosmyslové
vnímání	vnímání	k1gNnSc2	vnímání
nebo	nebo	k8xC	nebo
paranormálních	paranormální	k2eAgFnPc2d1	paranormální
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
nesvědčí	svědčit	k5eNaImIp3nP	svědčit
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nebyly	být	k5eNaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
biologie	biologie	k1gFnSc2	biologie
lze	lze	k6eAd1	lze
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
mimosmyslové	mimosmyslový	k2eAgNnSc1d1	mimosmyslové
vnímání	vnímání	k1gNnSc1	vnímání
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
nalezen	naleznout	k5eAaPmNgInS	naleznout
emitor	emitor	k1gInSc1	emitor
ani	ani	k8xC	ani
receptor	receptor	k1gInSc1	receptor
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
informatiky	informatika	k1gFnSc2	informatika
nelze	lze	k6eNd1	lze
dálkový	dálkový	k2eAgInSc4d1	dálkový
mimosmyslový	mimosmyslový	k2eAgInSc4d1	mimosmyslový
přenos	přenos	k1gInSc4	přenos
realizovat	realizovat	k5eAaBmF	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Antropologického	antropologický	k2eAgInSc2d1	antropologický
slovníku	slovník	k1gInSc2	slovník
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
telepat	telepat	k1gMnSc1	telepat
je	být	k5eAaImIp3nS	být
nadán	nadat	k5eAaPmNgInS	nadat
schopností	schopnost	k1gFnSc7	schopnost
přijímat	přijímat	k5eAaImF	přijímat
elektromagnetické	elektromagnetický	k2eAgInPc4d1	elektromagnetický
signály	signál	k1gInPc4	signál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
pracujícím	pracující	k2eAgInSc6d1	pracující
lidském	lidský	k2eAgInSc6d1	lidský
mozku	mozek	k1gInSc6	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
elektromagnetické	elektromagnetický	k2eAgFnPc1d1	elektromagnetická
vlny	vlna	k1gFnPc1	vlna
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
slabé	slabý	k2eAgFnPc1d1	slabá
<g/>
,	,	kIx,	,
a	a	k8xC	a
zanikají	zanikat	k5eAaImIp3nP	zanikat
několik	několik	k4yIc4	několik
centimetrů	centimetr	k1gInPc2	centimetr
od	od	k7c2	od
lidské	lidský	k2eAgFnSc2d1	lidská
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
zanikají	zanikat	k5eAaImIp3nP	zanikat
v	v	k7c6	v
elektromagnetickém	elektromagnetický	k2eAgInSc6d1	elektromagnetický
šumu	šum	k1gInSc6	šum
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Mornsteina	Mornstein	k1gMnSc2	Mornstein
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
obsah	obsah	k1gInSc4	obsah
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nebude	být	k5eNaImBp3nS	být
možno	možno	k6eAd1	možno
nikdy	nikdy	k6eAd1	nikdy
jednoznačně	jednoznačně	k6eAd1	jednoznačně
dešifrovat	dešifrovat	k5eAaBmF	dešifrovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
vypadají	vypadat	k5eAaImIp3nP	vypadat
často	často	k6eAd1	často
skoro	skoro	k6eAd1	skoro
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
když	když	k8xS	když
myslíme	myslet	k5eAaImIp1nP	myslet
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
dopravy	doprava	k1gFnSc2	doprava
k	k	k7c3	k
tetě	teta	k1gFnSc3	teta
<g/>
,	,	kIx,	,
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
řešíme	řešit	k5eAaImIp1nP	řešit
diferenciální	diferenciální	k2eAgFnSc4d1	diferenciální
rovnici	rovnice	k1gFnSc4	rovnice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Milana	Milan	k1gMnSc2	Milan
Nakonečného	Nakonečný	k2eAgInSc2d1	Nakonečný
představuje	představovat	k5eAaImIp3nS	představovat
telepatie	telepatie	k1gFnSc1	telepatie
identifikaci	identifikace	k1gFnSc4	identifikace
právě	právě	k6eAd1	právě
probíhajících	probíhající	k2eAgFnPc2d1	probíhající
událostí	událost	k1gFnPc2	událost
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
určité	určitý	k2eAgFnSc2d1	určitá
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
s	s	k7c7	s
vyloučením	vyloučení	k1gNnSc7	vyloučení
přímého	přímý	k2eAgNnSc2d1	přímé
vnímání	vnímání	k1gNnSc2	vnímání
(	(	kIx(	(
<g/>
např.	např.	kA	např.
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
někomu	někdo	k3yInSc3	někdo
přihodila	přihodit	k5eAaPmAgFnS	přihodit
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
subjektu	subjekt	k1gInSc2	subjekt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
vizi	vize	k1gFnSc4	vize
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
identifikace	identifikace	k1gFnSc1	identifikace
představ	představa	k1gFnPc2	představa
a	a	k8xC	a
pocitů	pocit	k1gInPc2	pocit
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
osoby	osoba	k1gFnSc2	osoba
(	(	kIx(	(
<g/>
nesprávně	správně	k6eNd1	správně
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
čtení	čtení	k1gNnSc1	čtení
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
"	"	kIx"	"
druhého	druhý	k4xOgMnSc4	druhý
<g/>
)	)	kIx)	)
s	s	k7c7	s
vyloučením	vyloučení	k1gNnSc7	vyloučení
známých	známý	k2eAgFnPc2d1	známá
možností	možnost	k1gFnPc2	možnost
identifikace	identifikace	k1gFnSc2	identifikace
těchto	tento	k3xDgInPc2	tento
mentálních	mentální	k2eAgInPc2d1	mentální
obsahů	obsah	k1gInPc2	obsah
–	–	k?	–
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
tu	tu	k6eAd1	tu
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
fenomén	fenomén	k1gInSc4	fenomén
"	"	kIx"	"
<g/>
paranormální	paranormální	k2eAgFnSc1d1	paranormální
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
komunikace	komunikace	k1gFnSc1	komunikace
nevysvětlitelné	vysvětlitelný	k2eNgInPc1d1	nevysvětlitelný
známými	známý	k2eAgFnPc7d1	známá
zákonitostmi	zákonitost	k1gFnPc7	zákonitost
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
Eysenck	Eysenck	k1gMnSc1	Eysenck
a	a	k8xC	a
Carl	Carl	k1gMnSc1	Carl
Sargent	Sargent	k1gMnSc1	Sargent
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
bohaté	bohatý	k2eAgInPc4d1	bohatý
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
dosvědčují	dosvědčovat	k5eAaImIp3nP	dosvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
paranormální	paranormální	k2eAgInPc1d1	paranormální
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
i	i	k9	i
telepatie	telepatie	k1gFnPc4	telepatie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
skutečné	skutečný	k2eAgFnPc1d1	skutečná
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
schopnosti	schopnost	k1gFnPc1	schopnost
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
tak	tak	k6eAd1	tak
zřetelnou	zřetelný	k2eAgFnSc7d1	zřetelná
<g/>
,	,	kIx,	,
částí	část	k1gFnSc7	část
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
inteligence	inteligence	k1gFnPc4	inteligence
a	a	k8xC	a
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Telepatie	telepatie	k1gFnSc1	telepatie
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
L.	L.	kA	L.
L.	L.	kA	L.
Vasiljeva	Vasiljeva	k1gFnSc1	Vasiljeva
"	"	kIx"	"
<g/>
sugescí	sugesce	k1gFnSc7	sugesce
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vasiljev	Vasiljet	k5eAaPmDgInS	Vasiljet
provedl	provést	k5eAaPmAgInS	provést
analýzu	analýza	k1gFnSc4	analýza
spontánních	spontánní	k2eAgInPc2d1	spontánní
jevů	jev	k1gInPc2	jev
telepatie	telepatie	k1gFnSc2	telepatie
a	a	k8xC	a
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
pěti	pět	k4xCc2	pět
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
stupeň	stupeň	k1gInSc1	stupeň
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
neurčitým	určitý	k2eNgInSc7d1	neurčitý
emocionálním	emocionální	k2eAgInSc7d1	emocionální
neklidem	neklid	k1gInSc7	neklid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
stupni	stupeň	k1gInSc6	stupeň
již	již	k6eAd1	již
lze	lze	k6eAd1	lze
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
identifikovat	identifikovat	k5eAaBmF	identifikovat
emocionální	emocionální	k2eAgInSc1d1	emocionální
impuls	impuls	k1gInSc1	impuls
s	s	k7c7	s
konkrétní	konkrétní	k2eAgFnSc7d1	konkrétní
osobou	osoba	k1gFnSc7	osoba
induktora	induktor	k1gMnSc2	induktor
(	(	kIx(	(
<g/>
vysílatele	vysílatel	k1gMnSc2	vysílatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
stupni	stupeň	k1gInSc6	stupeň
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uvědomění	uvědomění	k1gNnSc3	uvědomění
podrobností	podrobnost	k1gFnPc2	podrobnost
proběhlé	proběhlý	k2eAgFnSc2d1	proběhlá
události	událost	k1gFnSc2	událost
u	u	k7c2	u
vysílatele	vysílatel	k1gMnSc2	vysílatel
percipientem	percipient	k1gMnSc7	percipient
(	(	kIx(	(
<g/>
přijímatelem	přijímatel	k1gMnSc7	přijímatel
<g/>
)	)	kIx)	)
s	s	k7c7	s
nepřesnostmi	nepřesnost	k1gFnPc7	nepřesnost
v	v	k7c6	v
symbolické	symbolický	k2eAgFnSc6d1	symbolická
formě	forma	k1gFnSc6	forma
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
jeho	jeho	k3xOp3gFnPc7	jeho
psychickými	psychický	k2eAgFnPc7d1	psychická
reakcemi	reakce	k1gFnPc7	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
stupeň	stupeň	k1gInSc1	stupeň
má	mít	k5eAaImIp3nS	mít
formu	forma	k1gFnSc4	forma
zřetelné	zřetelný	k2eAgFnSc2d1	zřetelná
halucinace	halucinace	k1gFnSc2	halucinace
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
telepatická	telepatický	k2eAgFnSc1d1	telepatická
halucinace	halucinace	k1gFnSc1	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Přijímatel	přijímatel	k1gMnSc1	přijímatel
vidí	vidět	k5eAaImIp3nS	vidět
přízraky	přízrak	k1gInPc4	přízrak
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
"	"	kIx"	"
<g/>
role	role	k1gFnSc1	role
<g/>
"	"	kIx"	"
v	v	k7c6	v
událostech	událost	k1gFnPc6	událost
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
nehody	nehoda	k1gFnPc1	nehoda
<g/>
,	,	kIx,	,
úmrtí	úmrtí	k1gNnPc1	úmrtí
<g/>
,	,	kIx,	,
zrození	zrození	k1gNnSc1	zrození
života	život	k1gInSc2	život
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
extrémní	extrémní	k2eAgFnPc4d1	extrémní
dobré	dobrý	k2eAgFnPc4d1	dobrá
i	i	k8xC	i
špatné	špatný	k2eAgFnPc4d1	špatná
události	událost	k1gFnPc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
nemají	mít	k5eNaImIp3nP	mít
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeví	jevit	k5eAaImIp3nS	jevit
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
skutečné	skutečný	k2eAgNnSc4d1	skutečné
s	s	k7c7	s
jednoznačným	jednoznačný	k2eAgInSc7d1	jednoznačný
výkladem	výklad	k1gInSc7	výklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
stupni	stupeň	k1gInSc6	stupeň
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
přijímatel	přijímatel	k1gMnSc1	přijímatel
svědkem	svědek	k1gMnSc7	svědek
a	a	k8xC	a
účastníkem	účastník	k1gMnSc7	účastník
události	událost	k1gFnSc2	událost
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
telepatie	telepatie	k1gFnSc2	telepatie
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
stupni	stupeň	k1gInSc6	stupeň
je	být	k5eAaImIp3nS	být
smazán	smazán	k2eAgInSc1d1	smazán
rozdíl	rozdíl	k1gInSc1	rozdíl
s	s	k7c7	s
jevem	jev	k1gInSc7	jev
jasnovidnosti	jasnovidnost	k1gFnSc2	jasnovidnost
<g/>
.	.	kIx.	.
</s>
<s>
Ganzfeld	Ganzfeld	k1gInSc1	Ganzfeld
experiment	experiment	k1gInSc1	experiment
(	(	kIx(	(
<g/>
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
celé	celý	k2eAgNnSc4d1	celé
pole	pole	k1gNnSc4	pole
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
parapsychologický	parapsychologický	k2eAgInSc1d1	parapsychologický
experiment	experiment	k1gInSc1	experiment
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
prokázat	prokázat	k5eAaPmF	prokázat
existenci	existence	k1gFnSc4	existence
mimosmyslových	mimosmyslový	k2eAgInPc2d1	mimosmyslový
schopnosti	schopnost	k1gFnSc3	schopnost
a	a	k8xC	a
přenosu	přenos	k1gInSc3	přenos
síly	síla	k1gFnSc2	síla
psí	psí	k2eAgFnSc2d1	psí
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
např.	např.	kA	např.
jasnovidectví	jasnovidectví	k1gNnSc1	jasnovidectví
nebo	nebo	k8xC	nebo
telepatie	telepatie	k1gFnSc1	telepatie
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
ganzfeld	ganzfelda	k1gFnPc2	ganzfelda
postupu	postup	k1gInSc2	postup
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oddělení	oddělení	k1gNnSc6	oddělení
osoby	osoba	k1gFnSc2	osoba
"	"	kIx"	"
<g/>
vysílající	vysílající	k2eAgFnSc1d1	vysílající
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
přijímající	přijímající	k2eAgFnSc1d1	přijímající
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
první	první	k4xOgFnSc6	první
osobě	osoba	k1gFnSc6	osoba
se	se	k3xPyFc4	se
překryjí	překrýt	k5eAaPmIp3nP	překrýt
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
ozáří	ozářit	k5eAaPmIp3nS	ozářit
se	se	k3xPyFc4	se
červeným	červený	k2eAgNnSc7d1	červené
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
uší	ucho	k1gNnPc2	ucho
se	se	k3xPyFc4	se
pustí	pustit	k5eAaPmIp3nS	pustit
ze	z	k7c2	z
sluchátek	sluchátko	k1gNnPc2	sluchátko
šum	šuma	k1gFnPc2	šuma
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
osoba	osoba	k1gFnSc1	osoba
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
snaží	snažit	k5eAaImIp3nS	snažit
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
vyslat	vyslat	k5eAaPmF	vyslat
telepaticky	telepaticky	k6eAd1	telepaticky
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
obrázku	obrázek	k1gInSc6	obrázek
<g/>
,	,	kIx,	,
videosekvenci	videosekvence	k1gFnSc6	videosekvence
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
osoba	osoba	k1gFnSc1	osoba
pak	pak	k6eAd1	pak
popisuje	popisovat	k5eAaImIp3nS	popisovat
co	co	k3yQnSc1	co
"	"	kIx"	"
<g/>
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
"	"	kIx"	"
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
homogenním	homogenní	k2eAgNnSc6d1	homogenní
zorném	zorný	k2eAgNnSc6d1	zorné
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
hodnotitelé	hodnotitel	k1gMnPc1	hodnotitel
pak	pak	k6eAd1	pak
určí	určit	k5eAaPmIp3nP	určit
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
obrazů	obraz	k1gInPc2	obraz
je	být	k5eAaImIp3nS	být
popis	popis	k1gInSc1	popis
nejpodobnější	podobný	k2eAgInSc1d3	nejpodobnější
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotitelé	hodnotitel	k1gMnPc1	hodnotitel
neznají	znát	k5eNaImIp3nP	znát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
obrazů	obraz	k1gInPc2	obraz
je	být	k5eAaImIp3nS	být
správný	správný	k2eAgInSc1d1	správný
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mimosmyslový	mimosmyslový	k2eAgInSc1d1	mimosmyslový
přenos	přenos	k1gInSc1	přenos
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
uhádne	uhádnout	k5eAaPmIp3nS	uhádnout
zkoušející	zkoušející	k2eAgFnSc1d1	zkoušející
osoba	osoba	k1gFnSc1	osoba
správný	správný	k2eAgInSc1d1	správný
obrázek	obrázek	k1gInSc1	obrázek
ve	v	k7c6	v
25	[number]	k4	25
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
hodnota	hodnota	k1gFnSc1	hodnota
má	mít	k5eAaImIp3nS	mít
dokázat	dokázat	k5eAaPmF	dokázat
existenci	existence	k1gFnSc4	existence
přenosu	přenos	k1gInSc2	přenos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Bem	Bem	k1gFnSc2	Bem
a	a	k8xC	a
Honorton	Honorton	k1gInSc1	Honorton
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
28	[number]	k4	28
studií	studio	k1gNnPc2	studio
o	o	k7c4	o
835	[number]	k4	835
ganzfeld	ganzfelda	k1gFnPc2	ganzfelda
experimentech	experiment	k1gInPc6	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
(	(	kIx(	(
<g/>
43	[number]	k4	43
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
statisticky	statisticky	k6eAd1	statisticky
významné	významný	k2eAgInPc4d1	významný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
studie	studie	k1gFnPc4	studie
podrobili	podrobit	k5eAaPmAgMnP	podrobit
souhrnné	souhrnný	k2eAgFnSc3d1	souhrnná
analýze	analýza	k1gFnSc3	analýza
(	(	kIx(	(
<g/>
metaanalýze	metaanalýza	k1gFnSc3	metaanalýza
<g/>
)	)	kIx)	)
a	a	k8xC	a
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
výsledku	výsledek	k1gInSc3	výsledek
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
přijímatelé	přijímatel	k1gMnPc1	přijímatel
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
v	v	k7c4	v
38	[number]	k4	38
<g/>
%	%	kIx~	%
případů	případ	k1gInPc2	případ
oproti	oproti	k7c3	oproti
očekávaným	očekávaný	k2eAgInPc3d1	očekávaný
25	[number]	k4	25
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Statisticky	statisticky	k6eAd1	statisticky
tento	tento	k3xDgInSc4	tento
výsledek	výsledek	k1gInSc4	výsledek
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
vysoce	vysoce	k6eAd1	vysoce
významný	významný	k2eAgInSc4d1	významný
a	a	k8xC	a
o	o	k7c6	o
pravděpodobnosti	pravděpodobnost	k1gFnSc6	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
náhodně	náhodně	k6eAd1	náhodně
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
k	k	k7c3	k
miliardě	miliarda	k4xCgFnSc3	miliarda
(	(	kIx(	(
<g/>
Bem	Bem	k1gFnSc1	Bem
a	a	k8xC	a
Honorton	Honorton	k1gInSc1	Honorton
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc4	pozornost
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
práce	práce	k1gFnSc1	práce
Bema	Bema	k1gFnSc1	Bema
a	a	k8xC	a
Hortona	Hortona	k1gFnSc1	Hortona
z	z	k7c2	z
r.	r.	kA	r.
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
výsledky	výsledek	k1gInPc1	výsledek
svědčily	svědčit	k5eAaImAgInP	svědčit
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
mimosmyslového	mimosmyslový	k2eAgNnSc2d1	mimosmyslové
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
analýzy	analýza	k1gFnSc2	analýza
Bema	Bemum	k1gNnSc2	Bemum
a	a	k8xC	a
Honortona	Honorton	k1gMnSc4	Honorton
tvrdě	tvrdě	k6eAd1	tvrdě
napadeny	napaden	k2eAgInPc1d1	napaden
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
skeptiků	skeptik	k1gMnPc2	skeptik
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
prof.	prof.	kA	prof.
Ray	Ray	k1gFnSc1	Ray
Hymanem	Hyman	k1gInSc7	Hyman
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
společných	společný	k2eAgInPc2d1	společný
exaktních	exaktní	k2eAgInPc2d1	exaktní
pokusů	pokus	k1gInPc2	pokus
Honorton	Honorton	k1gInSc1	Honorton
-	-	kIx~	-
Hyman	Hyman	k1gInSc1	Hyman
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Heřt	Heřt	k1gMnSc1	Heřt
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
pokusy	pokus	k1gInPc1	pokus
vyzněly	vyznět	k5eAaImAgInP	vyznět
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesně	přesně	k6eAd1	přesně
koncipovaný	koncipovaný	k2eAgInSc1d1	koncipovaný
zásadní	zásadní	k2eAgInSc1d1	zásadní
experiment	experiment	k1gInSc1	experiment
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
iniciován	iniciovat	k5eAaBmNgInS	iniciovat
zástupcem	zástupce	k1gMnSc7	zástupce
parapsychologů	parapsycholog	k1gMnPc2	parapsycholog
Honortonem	Honorton	k1gInSc7	Honorton
a	a	k8xC	a
zástupcem	zástupce	k1gMnSc7	zástupce
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
psychologem	psycholog	k1gMnSc7	psycholog
Hymanem	Hyman	k1gMnSc7	Hyman
<g/>
,	,	kIx,	,
došel	dojít	k5eAaPmAgInS	dojít
v	v	k7c6	v
r.	r.	kA	r.
1996	[number]	k4	1996
k	k	k7c3	k
negativním	negativní	k2eAgMnPc3d1	negativní
výsledkům	výsledek	k1gInPc3	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
"	"	kIx"	"
<g/>
pozitivních	pozitivní	k2eAgInPc6d1	pozitivní
<g/>
"	"	kIx"	"
experimentech	experiment	k1gInPc6	experiment
byly	být	k5eAaImAgFnP	být
prokázány	prokázán	k2eAgFnPc1d1	prokázána
statistické	statistický	k2eAgFnPc1d1	statistická
chyby	chyba	k1gFnPc1	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Atkinson	Atkinson	k1gMnSc1	Atkinson
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Hyman	Hyman	k1gInSc1	Hyman
a	a	k8xC	a
Honorton	Honorton	k1gInSc1	Honorton
v	v	k7c6	v
diskusi	diskuse	k1gFnSc6	diskuse
o	o	k7c6	o
výsledcích	výsledek	k1gInPc6	výsledek
metaanalýzy	metaanalýza	k1gFnSc2	metaanalýza
v	v	k7c6	v
základních	základní	k2eAgInPc6d1	základní
kvantitativních	kvantitativní	k2eAgInPc6d1	kvantitativní
výsledcích	výsledek	k1gInPc6	výsledek
shodli	shodnout	k5eAaPmAgMnP	shodnout
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
byla	být	k5eAaImAgFnS	být
především	především	k9	především
interpretace	interpretace	k1gFnSc1	interpretace
výsledků	výsledek	k1gInPc2	výsledek
provedených	provedený	k2eAgInPc2d1	provedený
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
diskuse	diskuse	k1gFnPc1	diskuse
byly	být	k5eAaImAgFnP	být
problémy	problém	k1gInPc4	problém
opakovatelnosti	opakovatelnost	k1gFnSc2	opakovatelnost
(	(	kIx(	(
<g/>
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
kontroly	kontrola	k1gFnPc1	kontrola
(	(	kIx(	(
<g/>
možnost	možnost	k1gFnSc1	možnost
senzorického	senzorický	k2eAgNnSc2d1	senzorické
prosakování	prosakování	k1gNnSc2	prosakování
<g/>
,	,	kIx,	,
nedostatečné	dostatečný	k2eNgInPc4d1	nedostatečný
postupy	postup	k1gInPc4	postup
náhodného	náhodný	k2eAgInSc2d1	náhodný
výběru	výběr	k1gInSc2	výběr
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
problém	problém	k1gInSc1	problém
zásuvky	zásuvka	k1gFnSc2	zásuvka
(	(	kIx(	(
<g/>
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyly	být	k5eNaImAgFnP	být
zohledněny	zohlednit	k5eAaPmNgFnP	zohlednit
nezveřejněné	zveřejněný	k2eNgFnPc1d1	nezveřejněná
negativní	negativní	k2eAgFnPc1d1	negativní
studie	studie	k1gFnPc1	studie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anekdotické	anekdotický	k2eAgInPc4d1	anekdotický
doklady	doklad	k1gInPc4	doklad
(	(	kIx(	(
<g/>
doklady	doklad	k1gInPc4	doklad
telepatie	telepatie	k1gFnPc1	telepatie
vycházející	vycházející	k2eAgFnPc1d1	vycházející
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
ústního	ústní	k2eAgInSc2d1	ústní
popisu	popis	k1gInSc2	popis
osobní	osobní	k2eAgFnSc2d1	osobní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
nevěrohodné	věrohodný	k2eNgFnSc2d1	nevěrohodná
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Spor	spor	k1gInSc1	spor
neskončil	skončit	k5eNaPmAgInS	skončit
a	a	k8xC	a
parapsychologové	parapsycholog	k1gMnPc1	parapsycholog
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
publikovali	publikovat	k5eAaBmAgMnP	publikovat
desítky	desítka	k1gFnPc4	desítka
dalších	další	k2eAgFnPc2d1	další
ganzfeld	ganzfelda	k1gFnPc2	ganzfelda
experimentů	experiment	k1gInPc2	experiment
včetně	včetně	k7c2	včetně
metaanalýz	metaanalýza	k1gFnPc2	metaanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
sporné	sporný	k2eAgInPc1d1	sporný
a	a	k8xC	a
statisticky	statisticky	k6eAd1	statisticky
neprůkazné	průkazný	k2eNgNnSc1d1	neprůkazné
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
je	být	k5eAaImIp3nS	být
samotný	samotný	k2eAgInSc1d1	samotný
předpoklad	předpoklad	k1gInSc1	předpoklad
experimentu	experiment	k1gInSc2	experiment
<g/>
,	,	kIx,	,
že	že	k8xS	že
statistická	statistický	k2eAgFnSc1d1	statistická
odchylka	odchylka	k1gFnSc1	odchylka
od	od	k7c2	od
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
je	být	k5eAaImIp3nS	být
důkazem	důkaz	k1gInSc7	důkaz
telepatie	telepatie	k1gFnSc2	telepatie
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
předpoklad	předpoklad	k1gInSc1	předpoklad
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojován	spojovat	k5eAaImNgMnS	spojovat
s	s	k7c7	s
argumentem	argument	k1gInSc7	argument
známým	známý	k1gMnSc7	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
God	God	k1gFnSc2	God
of	of	k?	of
the	the	k?	the
Gaps	Gaps	k1gInSc1	Gaps
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bohu	bůh	k1gMnSc3	bůh
vyplňujícím	vyplňující	k2eAgFnPc3d1	vyplňující
mezery	mezera	k1gFnPc4	mezera
<g/>
"	"	kIx"	"
-	-	kIx~	-
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
<g/>
:	:	kIx,	:
někdo	někdo	k3yInSc1	někdo
konfrontuje	konfrontovat	k5eAaBmIp3nS	konfrontovat
vědce	vědec	k1gMnSc4	vědec
s	s	k7c7	s
hádankou	hádanka	k1gFnSc7	hádanka
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
vědecky	vědecky	k6eAd1	vědecky
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
nalezli	naleznout	k5eAaPmAgMnP	naleznout
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přísně	přísně	k6eAd1	přísně
vzato	vzít	k5eAaPmNgNnS	vzít
<g/>
,	,	kIx,	,
odchylka	odchylka	k1gFnSc1	odchylka
od	od	k7c2	od
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
důkazem	důkaz	k1gInSc7	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
,	,	kIx,	,
statisticky	statisticky	k6eAd1	statisticky
nepravděpodobný	pravděpodobný	k2eNgInSc1d1	nepravděpodobný
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
důkaz	důkaz	k1gInSc1	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
něco	něco	k3yInSc1	něco
<g/>
"	"	kIx"	"
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
náhodný	náhodný	k2eAgInSc4d1	náhodný
výběr	výběr	k1gInSc4	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Předpoklad	předpoklad	k1gInSc1	předpoklad
experimentu	experiment	k1gInSc2	experiment
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
odchylkou	odchylka	k1gFnSc7	odchylka
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
telepatie	telepatie	k1gFnPc4	telepatie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
klamný	klamný	k2eAgMnSc1d1	klamný
<g/>
.	.	kIx.	.
</s>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
výkladový	výkladový	k2eAgInSc1d1	výkladový
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgMnPc2d1	český
skeptiků	skeptik	k1gMnPc2	skeptik
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
ganzfeld	ganzfeld	k6eAd1	ganzfeld
experimenty	experiment	k1gInPc4	experiment
prováděli	provádět	k5eAaImAgMnP	provádět
skeptici	skeptik	k1gMnPc1	skeptik
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
negativní	negativní	k2eAgInSc1d1	negativní
právě	právě	k9	právě
tak	tak	k9	tak
jako	jako	k9	jako
výsledky	výsledek	k1gInPc1	výsledek
jiných	jiný	k2eAgInPc2d1	jiný
analogických	analogický	k2eAgInPc2d1	analogický
pokusů	pokus	k1gInPc2	pokus
parapsychologů	parapsycholog	k1gMnPc2	parapsycholog
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hádání	hádání	k1gNnSc4	hádání
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
vidění	vidění	k1gNnSc2	vidění
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
nebo	nebo	k8xC	nebo
ovlivňování	ovlivňování	k1gNnSc4	ovlivňování
generátorů	generátor	k1gInPc2	generátor
náhodných	náhodný	k2eAgNnPc2d1	náhodné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Víra	víra	k1gFnSc1	víra
v	v	k7c6	v
telepatii	telepatie	k1gFnSc6	telepatie
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
diagnostických	diagnostický	k2eAgNnPc2d1	diagnostické
kritérií	kritérion	k1gNnPc2	kritérion
pro	pro	k7c4	pro
schizotypní	schizotypnit	k5eAaPmIp3nS	schizotypnit
poruchu	porucha	k1gFnSc4	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
(	(	kIx(	(
<g/>
kód	kód	k1gInSc4	kód
301.22	[number]	k4	301.22
podle	podle	k7c2	podle
DSM-IV	DSM-IV	k1gFnSc2	DSM-IV
<g/>
,	,	kIx,	,
kód	kód	k1gInSc4	kód
F21	F21	k1gFnPc2	F21
podle	podle	k7c2	podle
MKN-	MKN-	k1gFnSc2	MKN-
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
kritérium	kritérium	k1gNnSc1	kritérium
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
obsahové	obsahový	k2eAgFnPc4d1	obsahová
poruchy	porucha	k1gFnPc4	porucha
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Uvedená	uvedený	k2eAgFnSc1d1	uvedená
porucha	porucha	k1gFnSc1	porucha
myšlení	myšlení	k1gNnSc2	myšlení
bývá	bývat	k5eAaImIp3nS	bývat
spojována	spojován	k2eAgFnSc1d1	spojována
s	s	k7c7	s
magickým	magický	k2eAgNnSc7d1	magické
myšlením	myšlení	k1gNnSc7	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
psychoterapeutky	psychoterapeutka	k1gFnSc2	psychoterapeutka
Barbory	Barbora	k1gFnSc2	Barbora
Janečkové	Janečková	k1gFnSc2	Janečková
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
telepatie	telepatie	k1gFnSc1	telepatie
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
bludů	blud	k1gInPc2	blud
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
)	)	kIx)	)
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symptomů	symptom	k1gInPc2	symptom
jedné	jeden	k4xCgFnSc2	jeden
formy	forma	k1gFnSc2	forma
psychospirituální	psychospirituální	k2eAgFnSc2d1	psychospirituální
krize	krize	k1gFnSc2	krize
(	(	kIx(	(
<g/>
DSM-IV	DSM-IV	k1gMnSc1	DSM-IV
<g/>
/	/	kIx~	/
<g/>
V	v	k7c6	v
<g/>
62.89	[number]	k4	62.89
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nazývá	nazývat	k5eAaImIp3nS	nazývat
otevření	otevření	k1gNnSc1	otevření
mimosmyslového	mimosmyslový	k2eAgNnSc2d1	mimosmyslové
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
.	.	kIx.	.
</s>
<s>
Psychospirituální	Psychospirituální	k2eAgFnSc4d1	Psychospirituální
krizi	krize	k1gFnSc4	krize
samotnou	samotný	k2eAgFnSc4d1	samotná
nepokládá	pokládat	k5eNaImIp3nS	pokládat
za	za	k7c4	za
psychopatologické	psychopatologický	k2eAgNnSc4d1	psychopatologické
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
lékařské	lékařský	k2eAgNnSc4d1	lékařské
vyšetření	vyšetření	k1gNnSc4	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
Mimosmyslové	mimosmyslový	k2eAgFnPc1d1	mimosmyslová
schopnosti	schopnost	k1gFnPc1	schopnost
Parapsychologie	parapsychologie	k1gFnSc2	parapsychologie
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
telepatie	telepatie	k1gFnSc2	telepatie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
VĚDECKÁ	vědecký	k2eAgFnSc1d1	vědecká
TABU	tabu	k2eAgFnSc1d1	tabu
<g/>
:	:	kIx,	:
Existuje	existovat	k5eAaImIp3nS	existovat
důkaz	důkaz	k1gInSc4	důkaz
telepatie	telepatie	k1gFnSc2	telepatie
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Koukal	Koukal	k1gMnSc1	Koukal
Rejstřík	rejstřík	k1gInSc4	rejstřík
pojmů	pojem	k1gInPc2	pojem
-	-	kIx~	-
Biokomunikace	Biokomunikace	k1gFnSc1	Biokomunikace
(	(	kIx(	(
<g/>
telepatie	telepatie	k1gFnSc1	telepatie
<g/>
)	)	kIx)	)
psychotronika	psychotronika	k1gFnSc1	psychotronika
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Telepatie	telepatie	k1gFnSc1	telepatie
zahady	zahada	k1gFnSc2	zahada
<g/>
.	.	kIx.	.
<g/>
mysteria	mysterium	k1gNnSc2	mysterium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Roman	Roman	k1gMnSc1	Roman
Přikryl	Přikryl	k1gMnSc1	Přikryl
<g/>
:	:	kIx,	:
Telepatie	telepatie	k1gFnSc1	telepatie
-	-	kIx~	-
přenos	přenos	k1gInSc1	přenos
myšlenek	myšlenka	k1gFnPc2	myšlenka
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
zivotni-energie	zivotninergie	k1gFnSc2	zivotni-energie
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
21.02	[number]	k4	21.02
<g/>
.2007	.2007	k4	.2007
Stručný	stručný	k2eAgInSc4d1	stručný
výkladový	výkladový	k2eAgInSc4d1	výkladový
slovník	slovník	k1gInSc4	slovník
českých	český	k2eAgMnPc2d1	český
skeptiků	skeptik	k1gMnPc2	skeptik
-	-	kIx~	-
Telepatie	telepatie	k1gFnSc1	telepatie
sysifos	sysifos	k1gMnSc1	sysifos
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
6.09	[number]	k4	6.09
<g/>
.2007	.2007	k4	.2007
The	The	k1gFnPc2	The
Sceptic	Sceptice	k1gFnPc2	Sceptice
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
dictionary	dictionar	k1gInPc7	dictionar
-	-	kIx~	-
Telepathy	Telepatha	k1gMnSc2	Telepatha
skepdic	skepdic	k1gMnSc1	skepdic
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
