<s>
Otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
</s>
<s>
Schématické	schématický	k2eAgNnSc1d1
znázornění	znázornění	k1gNnSc1
přispívání	přispívání	k1gNnSc2
do	do	k7c2
otevřených	otevřený	k2eAgFnPc2d1
dat	datum	k1gNnPc2
</s>
<s>
Otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Open	Open	k1gNnSc1
data	datum	k1gNnSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
informace	informace	k1gFnPc1
a	a	k8xC
data	datum	k1gNnPc1
zveřejněná	zveřejněný	k2eAgNnPc1d1
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
úplná	úplný	k2eAgNnPc1d1
<g/>
,	,	kIx,
snadno	snadno	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
<g/>
,	,	kIx,
strojově	strojově	k6eAd1
čitelná	čitelný	k2eAgFnSc1d1
<g/>
,	,	kIx,
používající	používající	k2eAgInPc4d1
standardy	standard	k1gInPc4
s	s	k7c7
volně	volně	k6eAd1
dostupnou	dostupný	k2eAgFnSc7d1
specifikací	specifikace	k1gFnSc7
<g/>
,	,	kIx,
zpřístupněná	zpřístupněný	k2eAgFnSc1d1
za	za	k7c2
jasně	jasně	k6eAd1
definovaných	definovaný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
užití	užití	k1gNnSc4
dat	datum	k1gNnPc2
s	s	k7c7
minimem	minimum	k1gNnSc7
omezení	omezení	k1gNnSc2
a	a	k8xC
dostupná	dostupný	k2eAgFnSc1d1
uživatelům	uživatel	k1gMnPc3
při	při	k7c6
vynaložení	vynaložení	k1gNnSc6
minima	minimum	k1gNnSc2
možných	možný	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
například	například	k6eAd1
o	o	k7c4
jízdní	jízdní	k2eAgInPc4d1
řády	řád	k1gInPc4
<g/>
,	,	kIx,
příjmy	příjem	k1gInPc4
států	stát	k1gInPc2
<g/>
,	,	kIx,
rozpočty	rozpočet	k1gInPc4
<g/>
,	,	kIx,
databáze	databáze	k1gFnPc4
<g/>
,	,	kIx,
seznam	seznam	k1gInSc1
poskytovatelů	poskytovatel	k1gMnPc2
sociálních	sociální	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
kalendář	kalendář	k1gInSc4
ministra	ministr	k1gMnSc2
nebo	nebo	k8xC
měření	měření	k1gNnSc1
čistoty	čistota	k1gFnSc2
ovzduší	ovzduší	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
univerzit	univerzita	k1gFnPc2
<g/>
,	,	kIx,
nevládních	vládní	k2eNgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
soukromých	soukromý	k2eAgFnPc2d1
firem	firma	k1gFnPc2
nebo	nebo	k8xC
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Definice	definice	k1gFnSc1
</s>
<s>
Otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
jsou	být	k5eAaImIp3nP
data	datum	k1gNnPc4
a	a	k8xC
údaje	údaj	k1gInPc4
zveřejněná	zveřejněný	k2eAgFnSc1d1
na	na	k7c6
Internetu	Internet	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
úplná	úplný	k2eAgFnSc1d1
<g/>
,	,	kIx,
</s>
<s>
snadno	snadno	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
<g/>
,	,	kIx,
</s>
<s>
strojově	strojově	k6eAd1
čitelná	čitelný	k2eAgFnSc1d1
<g/>
,	,	kIx,
</s>
<s>
používající	používající	k2eAgInPc4d1
standardy	standard	k1gInPc4
s	s	k7c7
volně	volně	k6eAd1
dostupnou	dostupný	k2eAgFnSc7d1
specifikací	specifikace	k1gFnSc7
<g/>
,	,	kIx,
</s>
<s>
zpřístupněna	zpřístupněn	k2eAgFnSc1d1
za	za	k7c2
jasně	jasně	k6eAd1
definovaných	definovaný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
užití	užití	k1gNnSc4
dat	datum	k1gNnPc2
s	s	k7c7
minimem	minimum	k1gNnSc7
omezení	omezení	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
dostupná	dostupný	k2eAgFnSc1d1
uživatelům	uživatel	k1gMnPc3
při	při	k7c6
vynaložení	vynaložení	k1gNnSc6
minima	minimum	k1gNnSc2
možných	možný	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Požadavky	požadavek	k1gInPc1
na	na	k7c4
podmínky	podmínka	k1gFnPc4
užití	užití	k1gNnSc2
otevřených	otevřený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
</s>
<s>
neomezují	omezovat	k5eNaImIp3nP
jejich	jejich	k3xOp3gMnPc4
uživatele	uživatel	k1gMnPc4
ve	v	k7c6
způsobu	způsob	k1gInSc6
použití	použití	k1gNnSc2
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
</s>
<s>
opravňují	opravňovat	k5eAaImIp3nP
uživatele	uživatel	k1gMnSc4
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
dalšímu	další	k2eAgNnSc3d1
šíření	šíření	k1gNnSc3
<g/>
,	,	kIx,
</s>
<s>
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
uveden	uvést	k5eAaPmNgMnS
autor	autor	k1gMnSc1
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
i	i	k9
při	při	k7c6
dalším	další	k2eAgNnSc6d1
šíření	šíření	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
při	při	k7c6
dalším	další	k2eAgNnSc6d1
šíření	šíření	k1gNnSc6
musí	muset	k5eAaImIp3nP
i	i	k9
ostatní	ostatní	k2eAgMnPc1d1
uživatelé	uživatel	k1gMnPc1
mít	mít	k5eAaImF
stejná	stejný	k2eAgNnPc4d1
oprávnění	oprávnění	k1gNnPc4
s	s	k7c7
daty	datum	k1gNnPc7
nakládat	nakládat	k5eAaImF
-	-	kIx~
během	během	k7c2
šíření	šíření	k1gNnSc2
dat	datum	k1gNnPc2
nesmí	smět	k5eNaImIp3nS
dojít	dojít	k5eAaPmF
např.	např.	kA
k	k	k7c3
omezení	omezení	k1gNnSc3
jejich	jejich	k3xOp3gNnSc2
využití	využití	k1gNnSc2
pouze	pouze	k6eAd1
pro	pro	k7c4
nekomerční	komerční	k2eNgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Vymezení	vymezení	k1gNnSc1
</s>
<s>
Podle	podle	k7c2
nejrozšířenější	rozšířený	k2eAgFnSc2d3
platné	platný	k2eAgFnSc2d1
definice	definice	k1gFnSc2
jsou	být	k5eAaImIp3nP
otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
taková	takový	k3xDgNnPc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
splňují	splňovat	k5eAaImIp3nP
následující	následující	k2eAgFnSc4d1
podmínku	podmínka	k1gFnSc4
<g/>
:	:	kIx,
"	"	kIx"
<g/>
data	datum	k1gNnPc4
nebo	nebo	k8xC
datové	datový	k2eAgInPc4d1
sety	set	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
každému	každý	k3xTgMnSc3
k	k	k7c3
dispozici	dispozice	k1gFnSc3
svobodně	svobodně	k6eAd1
k	k	k7c3
užívání	užívání	k1gNnSc3
<g/>
,	,	kIx,
modifikaci	modifikace	k1gFnSc3
a	a	k8xC
distribuci	distribuce	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
bez	bez	k7c2
jakéhokoliv	jakýkoliv	k3yIgNnSc2
autorskoprávního	autorskoprávní	k2eAgNnSc2d1
omezení	omezení	k1gNnSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
výjimkou	výjimka	k1gFnSc7
v	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
atribuce	atribuce	k1gFnSc1
například	například	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
licence	licence	k1gFnSc2
Creative	Creativ	k1gInSc5
Commons	Commons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
jsou	být	k5eAaImIp3nP
interoperabilní	interoperabilní	k2eAgNnPc1d1
<g/>
;	;	kIx,
fungují	fungovat	k5eAaImIp3nP
tedy	tedy	k9
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
použitou	použitý	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gMnPc7
pracuje	pracovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejrozšířenějšími	rozšířený	k2eAgFnPc7d3
otevřenými	otevřený	k2eAgFnPc7d1
daty	datum	k1gNnPc7
jsou	být	k5eAaImIp3nP
data	datum	k1gNnPc1
textová	textový	k2eAgNnPc1d1
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
jsou	být	k5eAaImIp3nP
uspořádaná	uspořádaný	k2eAgFnSc1d1
do	do	k7c2
tabulek	tabulka	k1gFnPc2
a	a	k8xC
datových	datový	k2eAgInPc2d1
setů	set	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
se	se	k3xPyFc4
však	však	k9
setkat	setkat	k5eAaPmF
velmi	velmi	k6eAd1
často	často	k6eAd1
i	i	k9
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
typy	typ	k1gInPc7
materiálů	materiál	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
mapy	mapa	k1gFnPc1
<g/>
,	,	kIx,
genetické	genetický	k2eAgFnPc1d1
informace	informace	k1gFnPc1
<g/>
,	,	kIx,
chemické	chemický	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
<g/>
,	,	kIx,
matematické	matematický	k2eAgInPc1d1
a	a	k8xC
vědecké	vědecký	k2eAgInPc1d1
vzorce	vzorec	k1gInPc1
<g/>
,	,	kIx,
lékařská	lékařský	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
,	,	kIx,
apod.	apod.	kA
</s>
<s>
Otevřenost	otevřenost	k1gFnSc1
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1
World	Worlda	k1gFnPc2
Wide	Wide	k1gInSc1
Webu	web	k1gInSc2
Tim	Tim	k?
Berners-Lee	Berners-Lee	k1gInSc1
sestavil	sestavit	k5eAaPmAgInS
pětistupňovou	pětistupňový	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
otevřenosti	otevřenost	k1gFnSc2
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
škála	škála	k1gFnSc1
porovnává	porovnávat	k5eAaImIp3nS
data	datum	k1gNnPc4
od	od	k7c2
nejuzavřenějších	uzavřený	k2eAgFnPc2d3
po	po	k7c6
nejotevřenější	otevřený	k2eAgFnSc6d3
(	(	kIx(
<g/>
a	a	k8xC
tedy	tedy	k9
nejpoužitelnější	použitelný	k2eAgMnSc1d3
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Data	datum	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
nejsou	být	k5eNaImIp3nP
ve	v	k7c6
strojově	strojově	k6eAd1
čitelném	čitelný	k2eAgInSc6d1
formátu	formát	k1gInSc6
(	(	kIx(
<g/>
např.	např.	kA
PDF	PDF	kA
<g/>
)	)	kIx)
</s>
<s>
Data	datum	k1gNnPc1
přístupná	přístupný	k2eAgNnPc1d1
ve	v	k7c6
strojově	strojově	k6eAd1
čitelném	čitelný	k2eAgInSc6d1
formátu	formát	k1gInSc6
(	(	kIx(
<g/>
např.	např.	kA
XLS	XLS	kA
<g/>
)	)	kIx)
</s>
<s>
Data	datum	k1gNnPc1
přístupná	přístupný	k2eAgNnPc1d1
ve	v	k7c6
formátu	formát	k1gInSc6
se	s	k7c7
svobodnou	svobodný	k2eAgFnSc7d1
specifikací	specifikace	k1gFnSc7
nebo	nebo	k8xC
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
jednoduše	jednoduše	k6eAd1
čitelná	čitelný	k2eAgFnSc1d1
(	(	kIx(
<g/>
např.	např.	kA
CSV	CSV	kA
<g/>
)	)	kIx)
</s>
<s>
Data	datum	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
kromě	kromě	k7c2
svobodné	svobodný	k2eAgFnSc2d1
specifikace	specifikace	k1gFnSc2
a	a	k8xC
jednoduché	jednoduchý	k2eAgFnPc1d1
čitelnosti	čitelnost	k1gFnPc1
mají	mít	k5eAaImIp3nP
také	také	k6eAd1
vlastní	vlastní	k2eAgFnSc4d1
URL	URL	kA
adresu	adresa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Data	datum	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
kromě	kromě	k7c2
svobodné	svobodný	k2eAgFnSc2d1
specifikace	specifikace	k1gFnSc2
<g/>
,	,	kIx,
jednoduché	jednoduchý	k2eAgFnSc2d1
čitelnosti	čitelnost	k1gFnSc2
a	a	k8xC
vlastní	vlastní	k2eAgFnSc2d1
URL	URL	kA
adresy	adresa	k1gFnSc2
i	i	k8xC
systematicky	systematicky	k6eAd1
propojená	propojený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Jsou	být	k5eAaImIp3nP
<g/>
-li	-li	k?
data	datum	k1gNnPc4
zveřejňována	zveřejňován	k2eAgNnPc4d1
daným	daný	k2eAgInSc7d1
nejsvobodnějším	svobodný	k2eAgInSc7d3
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
lze	lze	k6eAd1
mluvit	mluvit	k5eAaImF
o	o	k7c6
tzv.	tzv.	kA
propojitelných	propojitelný	k2eAgNnPc6d1
datech	datum	k1gNnPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
linked	linked	k1gInSc1
data	datum	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgNnPc4,k3yRgNnPc4,k3yIgNnPc4
lze	lze	k6eAd1
používat	používat	k5eAaImF
například	například	k6eAd1
v	v	k7c6
různých	různý	k2eAgFnPc6d1
aplikacích	aplikace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Data	datum	k1gNnPc1
tak	tak	k8xS,k8xC
přicházejí	přicházet	k5eAaImIp3nP
například	například	k6eAd1
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
nebo	nebo	k8xC
v	v	k7c6
jednotném	jednotný	k2eAgInSc6d1
formátu	formát	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
využít	využít	k5eAaPmF
v	v	k7c6
řadě	řada	k1gFnSc6
různých	různý	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
mobilních	mobilní	k2eAgInPc6d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
</s>
<s>
Otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
různých	různý	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Od	od	k7c2
jednotlivců	jednotlivec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
spolupracují	spolupracovat	k5eAaImIp3nP
na	na	k7c6
jednom	jeden	k4xCgInSc6
velkém	velký	k2eAgInSc6d1
projektu	projekt	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nS
přesných	přesný	k2eAgFnPc2d1
specifikací	specifikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
organizací	organizace	k1gFnPc2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
nebo	nebo	k8xC
neziskových	ziskový	k2eNgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
data	datum	k1gNnPc1
vytvářejí	vytvářet	k5eAaImIp3nP
v	v	k7c6
rámci	rámec	k1gInSc6
svých	svůj	k3xOyFgFnPc2
aktivit	aktivita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
firem	firma	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
efektivnější	efektivní	k2eAgNnPc4d2
data	datum	k1gNnPc4
zveřejnit	zveřejnit	k5eAaPmF
než	než	k8xS
omezovat	omezovat	k5eAaImF
jejich	jejich	k3xOp3gNnSc4
užití	užití	k1gNnSc4
prostřednictvím	prostřednictvím	k7c2
technologických	technologický	k2eAgFnPc2d1
překážek	překážka	k1gFnPc2
a	a	k8xC
licenční	licenční	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jednotlivci	jednotlivec	k1gMnPc1
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
kolaborativních	kolaborativní	k2eAgInPc2d1
projektů	projekt	k1gInPc2
často	často	k6eAd1
vznikají	vznikat	k5eAaImIp3nP
rozsáhlé	rozsáhlý	k2eAgInPc1d1
databáze	databáze	k1gFnPc4
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
většinou	většinou	k6eAd1
pod	pod	k7c7
nějakou	nějaký	k3yIgFnSc7
ze	z	k7c2
svobodných	svobodný	k2eAgFnPc2d1
licencí	licence	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
Creative	Creativ	k1gInSc5
Commons	Commons	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svobodná	svobodný	k2eAgFnSc1d1
licenční	licenční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
do	do	k7c2
databáze	databáze	k1gFnSc2
přispíval	přispívat	k5eAaImAgInS
neomezený	omezený	k2eNgInSc1d1
počet	počet	k1gInSc1
dobrovolníků	dobrovolník	k1gMnPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
datové	datový	k2eAgFnPc1d1
sady	sada	k1gFnPc1
neustále	neustále	k6eAd1
rozšiřují	rozšiřovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
projekt	projekt	k1gInSc4
nadace	nadace	k1gFnSc2
Wikimedia	Wikimedium	k1gNnSc2
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
Wikidata	Wikidata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1
správa	správa	k1gFnSc1
</s>
<s>
Otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
podle	podle	k7c2
regionů	region	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeleně	zeleně	k6eAd1
zobrazené	zobrazený	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
veškerá	veškerý	k3xTgNnPc1
data	datum	k1gNnPc1
poskytují	poskytovat	k5eAaImIp3nP
veřejnosti	veřejnost	k1gFnPc1
<g/>
,	,	kIx,
modře	modř	k1gFnPc1
vyznačené	vyznačený	k2eAgFnPc1d1
jen	jen	k9
částečně	částečně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zpřístupňování	zpřístupňování	k1gNnSc1
dat	datum	k1gNnPc2
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
je	být	k5eAaImIp3nS
dvojsečnou	dvojsečný	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
se	se	k3xPyFc4
lze	lze	k6eAd1
domnívat	domnívat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
zveřejnění	zveřejnění	k1gNnSc1
materiálů	materiál	k1gInPc2
institucí	instituce	k1gFnPc2
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
jistě	jistě	k6eAd1
přínosné	přínosný	k2eAgNnSc1d1
pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
transparentnosti	transparentnost	k1gFnSc2
často	často	k6eAd1
problematického	problematický	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
<g/>
,	,	kIx,
rizikem	riziko	k1gNnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
využití	využití	k1gNnSc2
dat	datum	k1gNnPc2
soukromým	soukromý	k2eAgInSc7d1
sektorem	sektor	k1gInSc7
a	a	k8xC
případný	případný	k2eAgInSc1d1
únik	únik	k1gInSc1
příjmů	příjem	k1gInPc2
státnímu	státní	k2eAgInSc3d1
subjektu	subjekt	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ztratí	ztratit	k5eAaPmIp3nS
výhradní	výhradní	k2eAgNnPc4d1
práva	právo	k1gNnPc4
používat	používat	k5eAaImF
vlastní	vlastní	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soukromý	soukromý	k2eAgInSc1d1
sektor	sektor	k1gInSc1
tak	tak	k6eAd1
může	moct	k5eAaImIp3nS
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
převzít	převzít	k5eAaPmF
roli	role	k1gFnSc4
sektoru	sektor	k1gInSc2
státního	státní	k2eAgInSc2d1
<g/>
;	;	kIx,
což	což	k3yQnSc1,k3yRnSc1
vede	vést	k5eAaImIp3nS
poté	poté	k6eAd1
ke	k	k7c3
snížení	snížení	k1gNnSc3
nákladů	náklad	k1gInPc2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
na	na	k7c4
provoz	provoz	k1gInSc4
daných	daný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
se	se	k3xPyFc4
ale	ale	k9
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
riziko	riziko	k1gNnSc4
nedostatečné	dostatečný	k2eNgFnSc2d1
kvality	kvalita	k1gFnSc2
a	a	k8xC
chybějící	chybějící	k2eAgFnSc2d1
odpovědnosti	odpovědnost	k1gFnSc2
za	za	k7c4
aplikace	aplikace	k1gFnPc4
a	a	k8xC
nástroje	nástroj	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
připraví	připravit	k5eAaPmIp3nP
veřejný	veřejný	k2eAgInSc4d1
sektor	sektor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
data	datum	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
veřejná	veřejný	k2eAgFnSc1d1
správa	správa	k1gFnSc1
vznikají	vznikat	k5eAaImIp3nP
víceméně	víceméně	k9
z	z	k7c2
prostředků	prostředek	k1gInPc2
daňových	daňový	k2eAgMnPc2d1
poplatníků	poplatník	k1gMnPc2
<g/>
,	,	kIx,
existuje	existovat	k5eAaImIp3nS
rozšířený	rozšířený	k2eAgInSc4d1
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
tato	tento	k3xDgNnPc1
data	datum	k1gNnPc1
právě	právě	k6eAd1
proto	proto	k8xC
veřejně	veřejně	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
a	a	k8xC
zproštěná	zproštěný	k2eAgFnSc1d1
autorskoprávní	autorskoprávní	k2eAgFnPc4d1
ochrany	ochrana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
a	a	k8xC
další	další	k2eAgFnPc4d1
skutečnosti	skutečnost	k1gFnPc4
pak	pak	k6eAd1
vede	vést	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
řada	řada	k1gFnSc1
národních	národní	k2eAgFnPc2d1
i	i	k8xC
regionálních	regionální	k2eAgFnPc2d1
vlád	vláda	k1gFnPc2
v	v	k7c6
průběhu	průběh	k1gInSc6
druhé	druhý	k4xOgFnSc2
dekády	dekáda	k1gFnSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
začala	začít	k5eAaPmAgFnS
věnovat	věnovat	k5eAaPmF,k5eAaImF
pozornost	pozornost	k1gFnSc4
možnosti	možnost	k1gFnSc2
otevírat	otevírat	k5eAaImF
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
je	být	k5eAaImIp3nS
však	však	k9
úspora	úspora	k1gFnSc1
a	a	k8xC
možnost	možnost	k1gFnSc1
outsourcingu	outsourcing	k1gInSc2
jejich	jejich	k3xOp3gNnSc2
zpracování	zpracování	k1gNnSc2
buď	buď	k8xC
veřejností	veřejnost	k1gFnSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
trhem	trh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Různé	různý	k2eAgFnPc1d1
národní	národní	k2eAgFnPc1d1
vlády	vláda	k1gFnPc1
a	a	k8xC
regionální	regionální	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
iniciovaly	iniciovat	k5eAaBmAgFnP
vznik	vznik	k1gInSc4
portálů	portál	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
nabízejí	nabízet	k5eAaImIp3nP
vlastní	vlastní	k2eAgNnPc4d1
otevřená	otevřený	k2eAgNnPc4d1
data	datum	k1gNnPc4
k	k	k7c3
volnému	volný	k2eAgNnSc3d1
stažení	stažení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
portál	portál	k1gInSc1
na	na	k7c4
otevřená	otevřený	k2eAgNnPc4d1
data	datum	k1gNnPc4
mají	mít	k5eAaImIp3nP
jak	jak	k9
vlády	vláda	k1gFnPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
například	například	k6eAd1
také	také	k9
Evropská	evropský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
poskytuje	poskytovat	k5eAaImIp3nS
například	například	k6eAd1
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
<g/>
,	,	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
financí	finance	k1gFnPc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Institut	institut	k1gInSc1
plánování	plánování	k1gNnSc2
a	a	k8xC
rozvoje	rozvoj	k1gInSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Děčín	Děčín	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Soukromá	soukromý	k2eAgFnSc1d1
sféra	sféra	k1gFnSc1
</s>
<s>
Řada	řada	k1gFnSc1
společností	společnost	k1gFnPc2
pracuje	pracovat	k5eAaImIp3nS
s	s	k7c7
otevřenými	otevřený	k2eAgNnPc7d1
daty	datum	k1gNnPc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
otevřeně	otevřeně	k6eAd1
výsledky	výsledek	k1gInPc4
své	svůj	k3xOyFgFnSc2
práce	práce	k1gFnSc2
zveřejňuje	zveřejňovat	k5eAaImIp3nS
a	a	k8xC
šíří	šířit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Činí	činit	k5eAaImIp3nS
tak	tak	k6eAd1
z	z	k7c2
důvodu	důvod	k1gInSc2
získat	získat	k5eAaPmF
potřebnou	potřebný	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
a	a	k8xC
zpětnou	zpětný	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
<g/>
;	;	kIx,
každý	každý	k3xTgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
data	datum	k1gNnPc4
zobrazit	zobrazit	k5eAaPmF
a	a	k8xC
osobně	osobně	k6eAd1
zkontrolovat	zkontrolovat	k5eAaPmF
<g/>
,	,	kIx,
také	také	k9
může	moct	k5eAaImIp3nS
odhalit	odhalit	k5eAaPmF
chyby	chyba	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
mít	mít	k5eAaImF
pro	pro	k7c4
komerční	komerční	k2eAgInPc4d1
subjekty	subjekt	k1gInPc4
nedozírné	dozírný	k2eNgInPc4d1
následky	následek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
veřejnou	veřejný	k2eAgFnSc7d1
správou	správa	k1gFnSc7
slouží	sloužit	k5eAaImIp3nP
otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
různých	různý	k2eAgFnPc2d1
firem	firma	k1gFnPc2
také	také	k9
ke	k	k7c3
společenské	společenský	k2eAgFnSc3d1
kontrole	kontrola	k1gFnSc3
<g/>
,	,	kIx,
neboť	neboť	k8xC
jednotlivci	jednotlivec	k1gMnPc1
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
snadno	snadno	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
,	,	kIx,
jestli	jestli	k8xS
dané	daný	k2eAgInPc1d1
subjekty	subjekt	k1gInPc1
splňují	splňovat	k5eAaImIp3nP
vládní	vládní	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
a	a	k8xC
regulace	regulace	k1gFnSc1
<g/>
,	,	kIx,
či	či	k8xC
nikoliv	nikoliv	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
Walmart	Walmarta	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zveřejnila	zveřejnit	k5eAaPmAgFnS
řadu	řada	k1gFnSc4
klíčových	klíčový	k2eAgFnPc2d1
informací	informace	k1gFnPc2
o	o	k7c6
svých	svůj	k3xOyFgFnPc6
továrnách	továrna	k1gFnPc6
ve	v	k7c6
snaze	snaha	k1gFnSc6
zvýšit	zvýšit	k5eAaPmF
transparentnost	transparentnost	k1gFnSc4
v	v	k7c6
kontrolním	kontrolní	k2eAgInSc6d1
procesu	proces	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Legislativa	legislativa	k1gFnSc1
ČR	ČR	kA
</s>
<s>
Otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
v	v	k7c6
ČR	ČR	kA
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
Směrnice	směrnice	k1gFnSc2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
a	a	k8xC
Rady	rada	k1gFnSc2
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
/	/	kIx~
<g/>
ES	ES	kA
ze	z	k7c2
dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2003	#num#	k4
o	o	k7c6
opakovaném	opakovaný	k2eAgNnSc6d1
použití	použití	k1gNnSc6
informací	informace	k1gFnPc2
veřejného	veřejný	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
ve	v	k7c6
znění	znění	k1gNnSc6
Směrnice	směrnice	k1gFnSc2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
a	a	k8xC
Rady	rada	k1gFnSc2
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
/	/	kIx~
<g/>
EU	EU	kA
ze	z	k7c2
dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
směrnice	směrnice	k1gFnSc1
je	být	k5eAaImIp3nS
do	do	k7c2
české	český	k2eAgFnSc2d1
legislativy	legislativa	k1gFnSc2
implementována	implementován	k2eAgFnSc1d1
zákonem	zákon	k1gInSc7
106	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
svobodném	svobodný	k2eAgInSc6d1
přístupu	přístup	k1gInSc6
k	k	k7c3
informacím	informace	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podrobnější	podrobný	k2eAgInSc1d2
rámec	rámec	k1gInSc1
pro	pro	k7c4
otevřená	otevřený	k2eAgNnPc4d1
data	datum	k1gNnPc4
má	mít	k5eAaImIp3nS
od	od	k7c2
1.1	1.1	k4
<g/>
.2017	.2017	k4
poskytnout	poskytnout	k5eAaPmF
novela	novela	k1gFnSc1
zákona	zákon	k1gInSc2
č.	č.	k?
106	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
svobodném	svobodný	k2eAgInSc6d1
přístupu	přístup	k1gInSc6
k	k	k7c3
informacím	informace	k1gFnPc3
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Data	datum	k1gNnPc1
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
otevřených	otevřený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
v	v	k7c6
ČR	ČR	kA
je	být	k5eAaImIp3nS
v	v	k7c6
takzvaném	takzvaný	k2eAgInSc6d1
Národním	národní	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
otevřených	otevřený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
provozuje	provozovat	k5eAaImIp3nS
Ministerstvo	ministerstvo	k1gNnSc4
vnitra	vnitro	k1gNnSc2
od	od	k7c2
června	červen	k1gInSc2
2018	#num#	k4
na	na	k7c6
nové	nový	k2eAgFnSc6d1
adrese	adresa	k1gFnSc6
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metodický	metodický	k2eAgInSc1d1
rámec	rámec	k1gInSc1
a	a	k8xC
standardy	standard	k1gInPc1
publikování	publikování	k1gNnSc2
otevřených	otevřený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
stanoví	stanovit	k5eAaPmIp3nS
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
na	na	k7c6
portálu	portál	k1gInSc6
opendata	opendata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.otevrenadata.cz	www.otevrenadata.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.mvcr.cz	www.mvcr.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
opendatahandbook	opendatahandbook	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
↑	↑	k?
Tisková	tiskový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
na	na	k7c6
portálu	portál	k1gInSc6
Bílého	bílý	k2eAgInSc2d1
domu	dům	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
linuxexpres	linuxexpresa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
otevrenadata	otevrenadata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.otevrenadata.cz	www.otevrenadata.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zpráva	zpráva	k1gFnSc1
o	o	k7c6
otevřených	otevřený	k2eAgNnPc6d1
datech	datum	k1gNnPc6
Institutu	institut	k1gInSc2
plánování	plánování	k1gNnSc2
a	a	k8xC
rozvoje	rozvoj	k1gInSc2
<g/>
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
Datová	datový	k2eAgFnSc1d1
žurnalistika	žurnalistika	k1gFnSc1
<g/>
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
geoportalpraha	geoportalpraha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
společnosti	společnost	k1gFnSc2
Walmart	Walmarta	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
walmart	walmart	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Prezentace	prezentace	k1gFnSc1
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
o	o	k7c6
otevřených	otevřený	k2eAgNnPc6d1
datech	datum	k1gNnPc6
z	z	k7c2
5.4	5.4	k4
<g/>
.2016	.2016	k4
<g/>
↑	↑	k?
Otevřená	otevřený	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
↑	↑	k?
Otevřená	otevřený	k2eAgNnPc4d1
data	datum	k1gNnPc4
v	v	k7c6
ČR	ČR	kA
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
pro	pro	k7c4
poskytovatele	poskytovatel	k1gMnPc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Otevřená	otevřený	k2eAgNnPc1d1
data	datum	k1gNnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
Webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
o	o	k7c6
otevřených	otevřený	k2eAgNnPc6d1
datech	datum	k1gNnPc6
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1064023886	#num#	k4
</s>
