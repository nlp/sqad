<s>
Kritická	kritický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
</s>
<s>
Horkheimer	Horkheimer	k1gInSc1
<g/>
,	,	kIx,
Adorno	Adorno	k1gNnSc1
a	a	k8xC
(	(	kIx(
<g/>
v	v	k7c6
pozadí	pozadí	k1gNnSc6
<g/>
)	)	kIx)
Habermas	Habermas	k1gMnSc1
(	(	kIx(
<g/>
1965	#num#	k4
v	v	k7c6
Heidelbergu	Heidelberg	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Kritická	kritický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
je	být	k5eAaImIp3nS
souhrnné	souhrnný	k2eAgNnSc4d1
označení	označení	k1gNnSc4
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
emancipační	emancipační	k2eAgInPc4d1
proudy	proud	k1gInPc4
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
literární	literární	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
společenských	společenský	k2eAgFnPc2d1
věd	věda	k1gFnPc2
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všem	všecek	k3xTgMnSc6
je	být	k5eAaImIp3nS
společná	společný	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
moderní	moderní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
i	i	k8xC
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
opřená	opřený	k2eAgFnSc1d1
o	o	k7c4
formu	forma	k1gFnSc4
marxismu	marxismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozornost	pozornost	k1gFnSc1
je	být	k5eAaImIp3nS
věnovaná	věnovaný	k2eAgFnSc1d1
psychoanalýze	psychoanalýza	k1gFnSc3
a	a	k8xC
má	mít	k5eAaImIp3nS
tendenci	tendence	k1gFnSc4
ke	k	k7c3
společenské	společenský	k2eAgFnSc3d1
praxi	praxe	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
zejména	zejména	k9
<g/>
:	:	kIx,
</s>
<s>
emancipace	emancipace	k1gFnSc1
člověka	člověk	k1gMnSc2
od	od	k7c2
společenských	společenský	k2eAgInPc2d1
tlaků	tlak	k1gInPc2
<g/>
,	,	kIx,
</s>
<s>
odhalování	odhalování	k1gNnSc1
a	a	k8xC
demystifikace	demystifikace	k1gFnSc1
ideologických	ideologický	k2eAgInPc2d1
argumentů	argument	k1gInPc2
konservativních	konservativní	k2eAgMnPc2d1
obhájců	obhájce	k1gMnPc2
kapitalismu	kapitalismus	k1gInSc2
a	a	k8xC
liberalismu	liberalismus	k1gInSc2
<g/>
,	,	kIx,
</s>
<s>
příprava	příprava	k1gFnSc1
radikálních	radikální	k2eAgFnPc2d1
společenských	společenský	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Kritická	kritický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
chce	chtít	k5eAaImIp3nS
využívat	využívat	k5eAaImF,k5eAaPmF
vysvětlující	vysvětlující	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
stanovit	stanovit	k5eAaPmF
normativní	normativní	k2eAgInPc4d1
cíle	cíl	k1gInPc4
a	a	k8xC
přispívat	přispívat	k5eAaImF
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
prosazování	prosazování	k1gNnSc3
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
ke	k	k7c3
společenským	společenský	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
jsou	být	k5eAaImIp3nP
sociální	sociální	k2eAgFnSc1d1
a	a	k8xC
politická	politický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
a	a	k8xC
literární	literární	k2eAgFnSc1d1
věda	věda	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnSc3,k3yRgFnSc3,k3yQgFnSc3
se	se	k3xPyFc4
však	však	k9
od	od	k7c2
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
různě	různě	k6eAd1
překrývají	překrývat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Předchůdci	předchůdce	k1gMnPc1
</s>
<s>
Mezi	mezi	k7c4
důležité	důležitý	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
kritické	kritický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
patří	patřit	k5eAaImIp3nS
zejména	zejména	k9
<g/>
:	:	kIx,
</s>
<s>
Jean-Jacques	Jean-Jacques	k1gMnSc1
Rousseau	Rousseau	k1gMnSc1
kritikou	kritika	k1gFnSc7
společnosti	společnost	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
„	„	k?
<g/>
člověk	člověk	k1gMnSc1
se	se	k3xPyFc4
rodí	rodit	k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
všude	všude	k6eAd1
žije	žít	k5eAaImIp3nS
v	v	k7c6
okovech	okov	k1gInPc6
<g/>
“	“	k?
<g/>
;	;	kIx,
</s>
<s>
Immanuel	Immanuel	k1gMnSc1
Kant	Kant	k1gMnSc1
svým	svůj	k3xOyFgNnSc7
pojetím	pojetí	k1gNnSc7
autonomie	autonomie	k1gFnSc2
člověka	člověk	k1gMnSc2
a	a	k8xC
racionální	racionální	k2eAgFnSc2d1
kritiky	kritika	k1gFnSc2
jako	jako	k8xC,k8xS
prostředku	prostředek	k1gInSc2
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
osvobození	osvobození	k1gNnSc4
<g/>
;	;	kIx,
</s>
<s>
Georg	Georg	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Hegel	Hegel	k1gMnSc1
pojetím	pojetí	k1gNnSc7
světa	svět	k1gInSc2
jako	jako	k8xS,k8xC
dějinné	dějinný	k2eAgFnSc2d1
dialektiky	dialektika	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
metodou	metoda	k1gFnSc7
historického	historický	k2eAgInSc2d1
materialismu	materialismus	k1gInSc2
a	a	k8xC
odhalování	odhalování	k1gNnSc2
mocenských	mocenský	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
<g/>
;	;	kIx,
</s>
<s>
Sigmund	Sigmund	k1gMnSc1
Freud	Freud	k1gInSc4
metodou	metoda	k1gFnSc7
odhalování	odhalování	k1gNnSc2
falešného	falešný	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
bezprostřední	bezprostřední	k2eAgMnPc4d1
předchůdce	předchůdce	k1gMnPc4
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
maďarský	maďarský	k2eAgMnSc1d1
literární	literární	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
György	Györg	k1gInPc4
Lukács	Lukácsa	k1gFnPc2
<g/>
,	,	kIx,
italský	italský	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Gramsci	Gramsek	k1gMnPc1
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
literární	literární	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
Walter	Walter	k1gMnSc1
Benjamin	Benjamin	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Frankfurtská	frankfurtský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Frankfurtská	frankfurtský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pojem	pojem	k1gInSc1
kritická	kritický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
v	v	k7c6
užším	úzký	k2eAgNnSc6d2
a	a	k8xC
v	v	k7c6
širším	široký	k2eAgInSc6d2
smyslu	smysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
užším	úzký	k2eAgInSc6d2
smyslu	smysl	k1gInSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
koncept	koncept	k1gInSc4
společenské	společenský	k2eAgFnSc2d1
kritiky	kritika	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
od	od	k7c2
založení	založení	k1gNnSc2
„	„	k?
<g/>
Ústavu	ústav	k1gInSc2
pro	pro	k7c4
společenský	společenský	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
<g/>
“	“	k?
ve	v	k7c6
Frankfurtu	Frankfurt	k1gInSc6
(	(	kIx(
<g/>
Institut	institut	k1gInSc1
für	für	k?
Sozialforschung	Sozialforschung	k1gInSc1
<g/>
)	)	kIx)
roku	rok	k1gInSc2
1931	#num#	k4
rozvíjela	rozvíjet	k5eAaImAgFnS
takzvaná	takzvaný	k2eAgFnSc1d1
Frankfurtská	frankfurtský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
(	(	kIx(
<g/>
Max	Max	k1gMnSc1
Horkheimer	Horkheimer	k1gMnSc1
<g/>
,	,	kIx,
Theodor	Theodor	k1gMnSc1
Adorno	Adorno	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gMnPc1
pokračovatelé	pokračovatel	k1gMnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
Herbert	Herbert	k1gMnSc1
Marcuse	Marcuse	k1gFnSc2
a	a	k8xC
Jürgen	Jürgen	k1gInSc4
Habermas	Habermasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
podle	podle	k7c2
Horkheimera	Horkheimero	k1gNnSc2
je	být	k5eAaImIp3nS
„	„	k?
<g/>
vysvobodit	vysvobodit	k5eAaPmF
lidské	lidský	k2eAgFnSc2d1
bytosti	bytost	k1gFnSc2
z	z	k7c2
okolností	okolnost	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	on	k3xPp3gInPc4
zotročují	zotročovat	k5eAaImIp3nP
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působila	působit	k5eAaImAgFnS
hlavně	hlavně	k9
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
americkém	americký	k2eAgInSc6d1
exilu	exil	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
její	její	k3xOp3gInSc1
vliv	vliv	k1gInSc1
rozšířil	rozšířit	k5eAaPmAgInS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
a	a	k8xC
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
i	i	k9
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Předválečná	předválečný	k2eAgFnSc1d1
kritická	kritický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
se	se	k3xPyFc4
netajila	tajit	k5eNaImAgFnS
svými	svůj	k3xOyFgInPc7
sympatiemi	sympatie	k1gFnPc7
k	k	k7c3
radikální	radikální	k2eAgFnSc3d1
politické	politický	k2eAgFnSc3d1
levici	levice	k1gFnSc3
a	a	k8xC
často	často	k6eAd1
kritizovala	kritizovat	k5eAaImAgFnS
pokryteckou	pokrytecký	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
liberální	liberální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děsivá	děsivý	k2eAgFnSc1d1
zkušenost	zkušenost	k1gFnSc1
s	s	k7c7
nacismem	nacismus	k1gInSc7
ji	on	k3xPp3gFnSc4
v	v	k7c6
tom	ten	k3xDgNnSc6
zdánlivě	zdánlivě	k6eAd1
podpořila	podpořit	k5eAaPmAgFnS
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
však	však	k9
také	také	k9
upozornila	upozornit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
hodnoty	hodnota	k1gFnPc1
svobody	svoboda	k1gFnSc2
nejsou	být	k5eNaImIp3nP
tak	tak	k6eAd1
samozřejmé	samozřejmý	k2eAgFnPc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
dříve	dříve	k6eAd2
zdálo	zdát	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
odhalení	odhalení	k1gNnSc1
zločinů	zločin	k1gInPc2
komunistických	komunistický	k2eAgInPc2d1
režimů	režim	k1gInPc2
a	a	k8xC
rozpad	rozpad	k1gInSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
však	však	k9
vedly	vést	k5eAaImAgInP
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
začala	začít	k5eAaPmAgFnS
více	hodně	k6eAd2
vážit	vážit	k5eAaImF
hodnot	hodnota	k1gFnPc2
politické	politický	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
a	a	k8xC
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
od	od	k7c2
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
věnují	věnovat	k5eAaPmIp3nP,k5eAaImIp3nP
představitelé	představitel	k1gMnPc1
kritických	kritický	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
stále	stále	k6eAd1
větší	veliký	k2eAgFnSc4d2
pozornost	pozornost	k1gFnSc4
„	„	k?
<g/>
skutečné	skutečný	k2eAgFnSc3d1
demokracii	demokracie	k1gFnSc3
<g/>
“	“	k?
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
nyní	nyní	k6eAd1
chápou	chápat	k5eAaImIp3nP
jako	jako	k9
uspořádání	uspořádání	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
„	„	k?
<g/>
všechny	všechen	k3xTgFnPc4
společenské	společenský	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
lidských	lidský	k2eAgFnPc6d1
rukou	ruka	k1gFnPc6
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
záviset	záviset	k5eAaImF
na	na	k7c6
reálném	reálný	k2eAgInSc6d1
souhlasu	souhlas	k1gInSc6
všech	všecek	k3xTgMnPc2
<g/>
“	“	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
tím	ten	k3xDgNnSc7
pak	pak	k6eAd1
úzce	úzko	k6eAd1
souvisí	souviset	k5eAaImIp3nS
i	i	k9
koncept	koncept	k1gInSc1
„	„	k?
<g/>
komunikativního	komunikativní	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
diskurzivní	diskurzivní	k2eAgFnSc2d1
etiky	etika	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
ji	on	k3xPp3gFnSc4
spolu	spolu	k6eAd1
s	s	k7c7
K.	K.	kA
<g/>
-	-	kIx~
<g/>
O.	O.	kA
Apelem	apel	k1gInSc7
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
vypracoval	vypracovat	k5eAaPmAgMnS
J.	J.	kA
Habermas	Habermas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1
společným	společný	k2eAgInSc7d1
rysem	rys	k1gInSc7
kritických	kritický	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
je	být	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
normativnost	normativnost	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
odmítání	odmítání	k1gNnSc1
všech	všecek	k3xTgFnPc2
forem	forma	k1gFnPc2
skepse	skepse	k1gFnSc2
a	a	k8xC
relativismu	relativismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Módní	módní	k2eAgFnSc1d1
relativistická	relativistický	k2eAgFnSc1d1
skepse	skepse	k1gFnSc1
je	být	k5eAaImIp3nS
konec	konec	k1gInSc1
konců	konec	k1gInPc2
jen	jen	k6eAd1
pohodlnou	pohodlný	k2eAgFnSc7d1
rezignací	rezignace	k1gFnSc7
salonních	salonní	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
odpovědnost	odpovědnost	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
i	i	k9
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakkoli	jakkoli	k8xS
je	být	k5eAaImIp3nS
každé	každý	k3xTgNnSc1
lidské	lidský	k2eAgNnSc1d1
poznání	poznání	k1gNnSc1
omezené	omezený	k2eAgNnSc1d1
a	a	k8xC
nejisté	jistý	k2eNgNnSc1d1
<g/>
,	,	kIx,
jakkoli	jakkoli	k8xS
potřebuje	potřebovat	k5eAaImIp3nS
kritiku	kritika	k1gFnSc4
<g/>
,	,	kIx,
svobodný	svobodný	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
má	mít	k5eAaImIp3nS
a	a	k8xC
musí	muset	k5eAaImIp3nS
jednat	jednat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
základě	základ	k1gInSc6
norem	norma	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
obstály	obstát	k5eAaPmAgFnP
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc7
nikdo	nikdo	k3yNnSc1
nebyl	být	k5eNaImAgMnS
vyloučen	vyloučit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nich	on	k3xPp3gFnPc2
je	být	k5eAaImIp3nS
i	i	k9
přesvědčení	přesvědčení	k1gNnSc4
o	o	k7c4
lidské	lidský	k2eAgFnPc4d1
rovnosti	rovnost	k1gFnPc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
kritické	kritický	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
zároveň	zároveň	k6eAd1
univerzalistické	univerzalistický	k2eAgFnPc1d1
a	a	k8xC
odmítají	odmítat	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc4
formy	forma	k1gFnPc4
společenské	společenský	k2eAgFnSc2d1
separace	separace	k1gFnSc2
a	a	k8xC
diskriminace	diskriminace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c7
nejvýznamnější	významný	k2eAgFnSc7d3
představitele	představitel	k1gMnSc4
třetí	třetí	k4xOgFnSc2
generace	generace	k1gFnSc2
kritické	kritický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
patří	patřit	k5eAaImIp3nS
Axel	Axel	k1gMnSc1
Honneth	Honneth	k1gMnSc1
(	(	kIx(
<g/>
Frankfurt	Frankfurt	k1gInSc1
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nancy	Nancy	k1gFnSc1
Fraser	Fraser	k1gMnSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Iris	iris	k1gInSc1
Marion	Marion	k1gInSc1
Young	Young	k1gInSc1
(	(	kIx(
<g/>
Chicago	Chicago	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alessandro	Alessandra	k1gFnSc5
Ferrara	Ferrara	k1gFnSc1
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
či	či	k8xC
Robert	Robert	k1gMnSc1
Fine	Fin	k1gMnSc5
(	(	kIx(
<g/>
Warwick	Warwicko	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
</s>
<s>
Theodor	Theodor	k1gMnSc1
W.	W.	kA
Adorno	Adorno	k1gNnSc1
</s>
<s>
Walter	Walter	k1gMnSc1
Benjamin	Benjamin	k1gMnSc1
</s>
<s>
Erich	Erich	k1gMnSc1
Fromm	Fromm	k1gMnSc1
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
do	do	k7c2
1940	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jürgen	Jürgen	k1gInSc1
Habermas	Habermasa	k1gFnPc2
</s>
<s>
Axel	Axel	k1gMnSc1
Honneth	Honneth	k1gMnSc1
</s>
<s>
Max	Max	k1gMnSc1
Horkheimer	Horkheimer	k1gMnSc1
</s>
<s>
Herbert	Herbert	k1gMnSc1
Marcuse	Marcuse	k1gFnSc2
</s>
<s>
Franz	Franz	k1gMnSc1
Leopold	Leopold	k1gMnSc1
Neumann	Neumann	k1gMnSc1
</s>
<s>
Širší	široký	k2eAgFnPc1d2
souvislosti	souvislost	k1gFnPc1
</s>
<s>
Množství	množství	k1gNnSc1
významných	významný	k2eAgMnPc2d1
myslitelů	myslitel	k1gMnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
buď	buď	k8xC
přímo	přímo	k6eAd1
ovlivněno	ovlivnit	k5eAaPmNgNnS
Kritickou	kritický	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
<g/>
,	,	kIx,
anebo	anebo	k8xC
dospěli	dochvít	k5eAaPmAgMnP
k	k	k7c3
podobným	podobný	k2eAgInPc3d1
názorům	názor	k1gInPc3
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
sem	sem	k6eAd1
například	například	k6eAd1
Jean	Jean	k1gMnSc1
Baudrillard	Baudrillard	k1gMnSc1
<g/>
,	,	kIx,
Roland	Roland	k1gInSc1
Barthes	Barthes	k1gInSc1
<g/>
,	,	kIx,
Jacques	Jacques	k1gMnSc1
Derrida	Derrida	k1gFnSc1
<g/>
,	,	kIx,
Michel	Michel	k1gMnSc1
Foucault	Foucault	k1gMnSc1
<g/>
,	,	kIx,
Anthony	Anthona	k1gFnPc1
Giddens	Giddens	k1gInSc1
<g/>
,	,	kIx,
Jacques	Jacques	k1gMnSc1
Lacan	Lacan	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
českých	český	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
byl	být	k5eAaImAgInS
kritické	kritický	k2eAgFnSc3d1
teorii	teorie	k1gFnSc3
blízko	blízko	k6eAd1
Karel	Karel	k1gMnSc1
Kosík	Kosík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Kritická	kritický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
její	její	k3xOp3gNnPc4
východiska	východisko	k1gNnPc4
a	a	k8xC
metody	metoda	k1gFnPc4
<g/>
,	,	kIx,
měly	mít	k5eAaImAgInP
značný	značný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
i	i	k9
na	na	k7c4
další	další	k2eAgNnPc4d1
emancipační	emancipační	k2eAgNnPc4d1
hnutí	hnutí	k1gNnPc4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
byl	být	k5eAaImAgMnS
odpor	odpor	k1gInSc4
proti	proti	k7c3
kolonialismu	kolonialismus	k1gInSc3
<g/>
,	,	kIx,
proti	proti	k7c3
rasové	rasový	k2eAgFnSc3d1
diskriminaci	diskriminace	k1gFnSc3
nebo	nebo	k8xC
třeba	třeba	k6eAd1
feministické	feministický	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
společenské	společenský	k2eAgNnSc1d1
působení	působení	k1gNnSc1
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
ve	v	k7c6
studentských	studentský	k2eAgFnPc6d1
bouřích	bouř	k1gFnPc6
roku	rok	k1gInSc2
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
tak	tak	k6eAd1
zvaná	zvaný	k2eAgFnSc1d1
hlubinná	hlubinný	k2eAgFnSc1d1
ekologie	ekologie	k1gFnSc1
nese	nést	k5eAaImIp3nS
jisté	jistý	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
tohoto	tento	k3xDgInSc2
vlivu	vliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
M.	M.	kA
Horkheimer	Horkheimer	k1gMnSc1
<g/>
,	,	kIx,
Critical	Critical	k1gMnSc1
theory	theora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
249	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Heslo	heslo	k1gNnSc4
Critical	Critical	k1gFnSc2
theory	theora	k1gFnSc2
ve	v	k7c4
Stanford	Stanford	k1gInSc4
encyclopedia	encyclopedium	k1gNnSc2
of	of	k?
philosophy	philosoph	k1gInPc1
<g/>
,	,	kIx,
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jürgen	Jürgen	k1gInSc1
Habermas	Habermas	k1gInSc1
<g/>
,	,	kIx,
Problémy	problém	k1gInPc1
legitimity	legitimita	k1gFnSc2
v	v	k7c6
pozdním	pozdní	k2eAgInSc6d1
kapitalismu	kapitalismus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7007-130-3	80-7007-130-3	k4
.	.	kIx.
</s>
<s>
Jürgen	Jürgen	k1gInSc1
Habermas	Habermas	k1gInSc1
<g/>
,	,	kIx,
Strukturální	strukturální	k2eAgFnSc1d1
přeměna	přeměna	k1gFnSc1
veřejnosti	veřejnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
134	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jürgen	Jürgen	k1gInSc1
Habermas	Habermas	k1gInSc1
<g/>
,	,	kIx,
Budoucnost	budoucnost	k1gFnSc1
lidské	lidský	k2eAgFnSc2d1
přirozenosti	přirozenost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
174	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Axel	Axel	k1gMnSc1
Honneth	Honneth	k1gMnSc1
<g/>
,	,	kIx,
Sociální	sociální	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
a	a	k8xC
postmoderní	postmoderní	k2eAgFnSc1d1
etika	etika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
82	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Axel	Axel	k1gMnSc1
Honneth	Honneth	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Zbavovat	zbavovat	k5eAaImF
se	se	k3xPyFc4
svéprávnosti	svéprávnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paradoxy	paradox	k1gInPc4
současného	současný	k2eAgInSc2d1
kapitalismu	kapitalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
269	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nancy	Nancy	k1gFnSc1
Fraserová	Fraserová	k1gFnSc1
<g/>
,	,	kIx,
Axel	Axel	k1gMnSc1
Honneth	Honneth	k1gMnSc1
<g/>
,	,	kIx,
Přerozdělování	přerozdělování	k1gNnSc1
nebo	nebo	k8xC
uznání	uznání	k1gNnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nancy	Nancy	k1gFnSc1
Fraser	Frasra	k1gFnPc2
<g/>
,	,	kIx,
Rozvíjení	rozvíjení	k1gNnSc4
radikální	radikální	k2eAgFnSc2d1
imaginace	imaginace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hans-Herbert	Hans-Herbert	k1gMnSc1
Kögler	Kögler	k1gMnSc1
<g/>
,	,	kIx,
Kultura	kultura	k1gFnSc1
<g/>
,	,	kIx,
kritika	kritika	k1gFnSc1
<g/>
,	,	kIx,
dialog	dialog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
238	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Alessandro	Alessandra	k1gFnSc5
Ferrara	Ferrara	k1gFnSc1
<g/>
,	,	kIx,
Nedostatek	nedostatek	k1gInSc1
soudnosti	soudnost	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
a	a	k8xC
kosmopolitní	kosmopolitní	k2eAgFnSc1d1
otázka	otázka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Robert	Robert	k1gMnSc1
Fine	Fin	k1gMnSc5
<g/>
,	,	kIx,
Kosmopolitismus	kosmopolitismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
346	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Iris	iris	k1gInSc1
Marion	Marion	k1gInSc1
Young	Young	k1gInSc4
<g/>
,	,	kIx,
Proti	proti	k7c3
útlaku	útlak	k1gInSc3
a	a	k8xC
nadvládě	nadvláda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transnacionální	Transnacionální	k2eAgFnSc2d1
výzvy	výzva	k1gFnSc2
politické	politický	k2eAgFnSc6d1
a	a	k8xC
feministické	feministický	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
341	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
kritická	kritický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Hrubec	hrubec	k1gMnSc1
<g/>
,	,	kIx,
Od	od	k7c2
zneuznání	zneuznání	k1gNnSc2
ke	k	k7c3
spravedlnosti	spravedlnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritická	kritický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
globální	globální	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Martin	Martin	k1gMnSc1
Beck	Beck	k1gMnSc1
Matuštík	Matuštík	k1gMnSc1
<g/>
,	,	kIx,
Neklid	neklid	k1gInSc4
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7007	#num#	k4
<g/>
-	-	kIx~
<g/>
240	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Frankfurtská	frankfurtský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Marxismus	marxismus	k1gInSc1
</s>
<s>
Psychoanalýza	psychoanalýza	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kritická	kritický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Heslo	heslo	k1gNnSc1
Critical	Critical	k1gFnSc2
theory	theora	k1gFnSc2
ve	v	k7c4
Stanford	Stanford	k1gInSc4
encyclopedia	encyclopedium	k1gNnSc2
of	of	k?
philosophy	philosoph	k1gInPc7
</s>
<s>
Portál	portál	k1gInSc1
ke	k	k7c3
kritické	kritický	k2eAgFnSc3d1
teorii	teorie	k1gFnSc3
</s>
<s>
Portál	portál	k1gInSc1
k	k	k7c3
současné	současný	k2eAgFnSc3d1
filosofii	filosofie	k1gFnSc3
<g/>
,	,	kIx,
kritické	kritický	k2eAgFnSc3d1
teorii	teorie	k1gFnSc3
a	a	k8xC
postmoderně	postmoderna	k1gFnSc3
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
zčásti	zčásti	k6eAd1
na	na	k7c6
informacích	informace	k1gFnPc6
z	z	k7c2
odpovídajícího	odpovídající	k2eAgInSc2d1
článku	článek	k1gInSc2
anglické	anglický	k2eAgFnSc2d1
a	a	k8xC
německé	německý	k2eAgFnSc2d1
Wikipedie	Wikipedie	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
odkazy	odkaz	k1gInPc7
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Filosofie	filosofie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4073840-1	4073840-1	k4
</s>
