<s>
Prokrastinace	Prokrastinace	k1gFnSc1	Prokrastinace
je	být	k5eAaImIp3nS	být
výrazná	výrazný	k2eAgFnSc1d1	výrazná
a	a	k8xC	a
chronická	chronický	k2eAgFnSc1d1	chronická
tendence	tendence	k1gFnSc1	tendence
odkládat	odkládat	k5eAaImF	odkládat
plnění	plnění	k1gNnSc4	plnění
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
administrativních	administrativní	k2eAgFnPc2d1	administrativní
či	či	k8xC	či
psychicky	psychicky	k6eAd1	psychicky
náročných	náročný	k2eAgFnPc2d1	náročná
<g/>
)	)	kIx)	)
povinností	povinnost	k1gFnPc2	povinnost
a	a	k8xC	a
úkolů	úkol	k1gInPc2	úkol
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
těch	ten	k3xDgFnPc2	ten
nepříjemných	příjemný	k2eNgFnPc2d1	nepříjemná
<g/>
)	)	kIx)	)
na	na	k7c4	na
pozdější	pozdní	k2eAgFnSc4d2	pozdější
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
představovat	představovat	k5eAaImF	představovat
rizikový	rizikový	k2eAgInSc1d1	rizikový
fenomén	fenomén	k1gInSc1	fenomén
pro	pro	k7c4	pro
duševní	duševní	k2eAgNnSc4d1	duševní
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Psychologové	psycholog	k1gMnPc1	psycholog
často	často	k6eAd1	často
popisují	popisovat	k5eAaImIp3nP	popisovat
takové	takový	k3xDgNnSc4	takový
chování	chování	k1gNnSc4	chování
jako	jako	k8xS	jako
mechanismus	mechanismus	k1gInSc4	mechanismus
pomáhající	pomáhající	k2eAgMnPc1d1	pomáhající
jedinci	jedinec	k1gMnPc1	jedinec
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
se	se	k3xPyFc4	se
s	s	k7c7	s
úzkostí	úzkost	k1gFnSc7	úzkost
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
dané	daný	k2eAgFnSc2d1	daná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
vědců	vědec	k1gMnPc2	vědec
jsou	být	k5eAaImIp3nP	být
kritéria	kritérion	k1gNnPc4	kritérion
definující	definující	k2eAgFnSc2d1	definující
prokrastinaci	prokrastinace	k1gFnSc3	prokrastinace
následující	následující	k2eAgFnSc3d1	následující
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
kontraproduktivní	kontraproduktivní	k2eAgNnSc1d1	kontraproduktivní
<g/>
,	,	kIx,	,
zbytečná	zbytečný	k2eAgFnSc1d1	zbytečná
a	a	k8xC	a
zdržující	zdržující	k2eAgFnSc1d1	zdržující
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
prokrastinací	prokrastinace	k1gFnSc7	prokrastinace
je	být	k5eAaImIp3nS	být
spojena	spojen	k2eAgFnSc1d1	spojena
rozhodovací	rozhodovací	k2eAgFnSc1d1	rozhodovací
paralýza	paralýza	k1gFnSc1	paralýza
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
složitější	složitý	k2eAgInSc1d2	složitější
výběr	výběr	k1gInSc1	výběr
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
odkládání	odkládání	k1gNnSc3	odkládání
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Prokrastinace	Prokrastinace	k1gFnSc1	Prokrastinace
může	moct	k5eAaImIp3nS	moct
vyústit	vyústit	k5eAaPmF	vyústit
ve	v	k7c4	v
stres	stres	k1gInSc4	stres
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc4	pocit
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
psychickou	psychický	k2eAgFnSc4d1	psychická
krizi	krize	k1gFnSc4	krize
a	a	k8xC	a
ztrátu	ztráta	k1gFnSc4	ztráta
produktivity	produktivita	k1gFnSc2	produktivita
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
těchto	tento	k3xDgInPc2	tento
pocitů	pocit	k1gInPc2	pocit
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
prokrastinaci	prokrastinace	k1gFnSc3	prokrastinace
<g/>
,	,	kIx,	,
vytvářejíc	vytvářet	k5eAaImSgNnS	vytvářet
tak	tak	k6eAd1	tak
určitý	určitý	k2eAgInSc4d1	určitý
kruh	kruh	k1gInSc4	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Prokrastinace	Prokrastinace	k1gFnSc1	Prokrastinace
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
normální	normální	k2eAgNnSc4d1	normální
psychologické	psychologický	k2eAgNnSc4d1	psychologické
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Chronická	chronický	k2eAgFnSc1d1	chronická
prokrastinace	prokrastinace	k1gFnSc1	prokrastinace
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příznakem	příznak	k1gInSc7	příznak
vážné	vážný	k2eAgFnSc2d1	vážná
mentální	mentální	k2eAgFnSc2d1	mentální
poruchy	porucha	k1gFnSc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
postižení	postižený	k2eAgMnPc1d1	postižený
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
strach	strach	k1gInSc4	strach
vyhledat	vyhledat	k5eAaPmF	vyhledat
pomoc	pomoc	k1gFnSc4	pomoc
kvůli	kvůli	k7c3	kvůli
sociálnímu	sociální	k2eAgNnSc3d1	sociální
stigmatu	stigma	k1gNnSc3	stigma
a	a	k8xC	a
rozšířenému	rozšířený	k2eAgInSc3d1	rozšířený
omylu	omyl	k1gInSc3	omyl
<g/>
,	,	kIx,	,
že	že	k8xS	že
prokrastinace	prokrastinace	k1gFnSc1	prokrastinace
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
leností	lenost	k1gFnSc7	lenost
<g/>
,	,	kIx,	,
nízkou	nízký	k2eAgFnSc7d1	nízká
silou	síla	k1gFnSc7	síla
vůle	vůle	k1gFnSc2	vůle
nebo	nebo	k8xC	nebo
malými	malý	k2eAgFnPc7d1	malá
ambicemi	ambice	k1gFnPc7	ambice
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
výraz	výraz	k1gInSc1	výraz
prokrastinace	prokrastinace	k1gFnSc2	prokrastinace
je	být	k5eAaImIp3nS	být
přejatý	přejatý	k2eAgInSc1d1	přejatý
přes	přes	k7c4	přes
anglické	anglický	k2eAgInPc4d1	anglický
procrastination	procrastination	k1gInSc4	procrastination
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
procrastinatus	procrastinatus	k1gInSc1	procrastinatus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
odvozené	odvozený	k2eAgNnSc1d1	odvozené
od	od	k7c2	od
minulého	minulý	k2eAgNnSc2d1	Minulé
příčestí	příčestí	k1gNnSc2	příčestí
slova	slovo	k1gNnSc2	slovo
procrastinare	procrastinar	k1gMnSc5	procrastinar
složeného	složený	k2eAgNnSc2d1	složené
z	z	k7c2	z
pro-	pro-	k?	pro-
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
<g/>
,	,	kIx,	,
na	na	k7c4	na
<g/>
)	)	kIx)	)
a	a	k8xC	a
crastinus	crastinus	k1gInSc4	crastinus
(	(	kIx(	(
<g/>
zítřejší	zítřejší	k2eAgFnSc4d1	zítřejší
<g/>
)	)	kIx)	)
odvozeného	odvozený	k2eAgInSc2d1	odvozený
z	z	k7c2	z
cras	crasa	k1gFnPc2	crasa
(	(	kIx(	(
<g/>
zítra	zítra	k6eAd1	zítra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
LUDWIG	LUDWIG	kA	LUDWIG
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
Konec	konec	k1gInSc1	konec
prokrastinace	prokrastinace	k1gFnSc2	prokrastinace
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
přestat	přestat	k5eAaPmF	přestat
odkládat	odkládat	k5eAaImF	odkládat
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
žít	žít	k5eAaImF	žít
naplno	naplno	k6eAd1	naplno
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Melvil	Melvil	k1gFnSc2	Melvil
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87270	[number]	k4	87270
<g/>
-	-	kIx~	-
<g/>
51	[number]	k4	51
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
GRUBER	Gruber	k1gMnSc1	Gruber
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
:	:	kIx,	:
Prokrastinace	Prokrastinace	k1gFnSc1	Prokrastinace
-	-	kIx~	-
odklad	odklad	k1gInSc1	odklad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
mrzí	mrzet	k5eAaImIp3nS	mrzet
<g/>
.	.	kIx.	.
</s>
<s>
Gruber-TDP	Gruber-TDP	k?	Gruber-TDP
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-85624-84-7	[number]	k4	978-80-85624-84-7
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
prokrastinace	prokrastinace	k1gFnSc2	prokrastinace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
prokrastinace	prokrastinace	k1gFnSc2	prokrastinace
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
prokrastinace	prokrastinace	k1gFnSc1	prokrastinace
na	na	k7c6	na
webu	web	k1gInSc6	web
centra	centrum	k1gNnSc2	centrum
adiktologie	adiktologie	k1gFnSc2	adiktologie
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
LF	LF	kA	LF
UK	UK	kA	UK
Až	až	k9	až
třetina	třetina	k1gFnSc1	třetina
studentů	student	k1gMnPc2	student
trpí	trpět	k5eAaImIp3nS	trpět
chorobným	chorobný	k2eAgNnSc7d1	chorobné
odkládáním	odkládání	k1gNnSc7	odkládání
povinností	povinnost	k1gFnPc2	povinnost
Prokrastinace	Prokrastinace	k1gFnSc2	Prokrastinace
<g/>
?	?	kIx.	?
</s>
<s>
Ohroženi	ohrožen	k2eAgMnPc1d1	ohrožen
jsou	být	k5eAaImIp3nP	být
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
a	a	k8xC	a
kreativní	kreativní	k2eAgMnPc1d1	kreativní
lidé	člověk	k1gMnPc1	člověk
VIDEO	video	k1gNnSc1	video
<g/>
:	:	kIx,	:
Jak	jak	k6eAd1	jak
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
prokrastinací	prokrastinace	k1gFnSc7	prokrastinace
<g/>
?	?	kIx.	?
</s>
<s>
VIDEO	video	k1gNnSc1	video
<g/>
:	:	kIx,	:
Co	co	k3yRnSc1	co
znamená	znamenat	k5eAaImIp3nS	znamenat
prokrastinace	prokrastinace	k1gFnSc1	prokrastinace
Ig	Ig	k1gFnSc2	Ig
nobelova	nobelův	k2eAgFnSc1d1	nobelova
cena	cena	k1gFnSc1	cena
(	(	kIx(	(
<g/>
antinobelova	antinobelův	k2eAgFnSc1d1	antinobelův
cena	cena	k1gFnSc1	cena
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
Johna	John	k1gMnSc4	John
Perryho	Perry	k1gMnSc4	Perry
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
článek	článek	k1gInSc1	článek
o	o	k7c6	o
prokrastinaci	prokrastinace	k1gFnSc6	prokrastinace
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
