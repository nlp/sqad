<s>
Prokrastinace	Prokrastinace	k1gFnSc1	Prokrastinace
je	být	k5eAaImIp3nS	být
výrazná	výrazný	k2eAgFnSc1d1	výrazná
a	a	k8xC	a
chronická	chronický	k2eAgFnSc1d1	chronická
tendence	tendence	k1gFnSc1	tendence
odkládat	odkládat	k5eAaImF	odkládat
plnění	plnění	k1gNnSc4	plnění
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
administrativních	administrativní	k2eAgFnPc2d1	administrativní
či	či	k8xC	či
psychicky	psychicky	k6eAd1	psychicky
náročných	náročný	k2eAgFnPc2d1	náročná
<g/>
)	)	kIx)	)
povinností	povinnost	k1gFnPc2	povinnost
a	a	k8xC	a
úkolů	úkol	k1gInPc2	úkol
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
těch	ten	k3xDgFnPc2	ten
nepříjemných	příjemný	k2eNgFnPc2d1	nepříjemná
<g/>
)	)	kIx)	)
na	na	k7c4	na
pozdější	pozdní	k2eAgFnSc4d2	pozdější
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
