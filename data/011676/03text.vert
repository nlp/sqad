<p>
<s>
Tuchlovický	Tuchlovický	k2eAgInSc1d1	Tuchlovický
potok	potok	k1gInSc1	potok
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Rynholec	Rynholec	k1gInSc4	Rynholec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzniká	vznikat	k5eAaImIp3nS	vznikat
několika	několik	k4yIc7	několik
přítoky	přítok	k1gInPc7	přítok
dešťové	dešťový	k2eAgFnSc2d1	dešťová
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
přitéká	přitékat	k5eAaImIp3nS	přitékat
vyčištěná	vyčištěný	k2eAgFnSc1d1	vyčištěná
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
rynholecké	rynholecký	k2eAgFnSc2d1	rynholecký
čističky	čistička	k1gFnSc2	čistička
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
protéká	protékat	k5eAaImIp3nS	protékat
několika	několik	k4yIc7	několik
mokřady	mokřad	k1gInPc7	mokřad
poblíž	poblíž	k7c2	poblíž
obce	obec	k1gFnSc2	obec
Vašírov	Vašírov	k1gInSc1	Vašírov
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
přítok	přítok	k1gInSc1	přítok
tvoří	tvořit	k5eAaImIp3nS	tvořit
Zámecký	zámecký	k2eAgInSc1d1	zámecký
potok	potok	k1gInSc1	potok
který	který	k3yRgInSc1	který
teče	téct	k5eAaImIp3nS	téct
z	z	k7c2	z
Lán	lán	k1gInSc4	lán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Tuchlovice	Tuchlovice	k1gFnSc2	Tuchlovice
přitéká	přitékat	k5eAaImIp3nS	přitékat
do	do	k7c2	do
potoka	potok	k1gInSc2	potok
několik	několik	k4yIc4	několik
vývodů	vývod	k1gInPc2	vývod
z	z	k7c2	z
dešťové	dešťový	k2eAgFnSc2d1	dešťová
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
tvoří	tvořit	k5eAaImIp3nS	tvořit
významný	významný	k2eAgInSc4d1	významný
přítok	přítok	k1gInSc4	přítok
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Tuchlovicemi	Tuchlovice	k1gFnPc7	Tuchlovice
přitéká	přitékat	k5eAaImIp3nS	přitékat
ještě	ještě	k6eAd1	ještě
vyčištěná	vyčištěný	k2eAgFnSc1d1	vyčištěná
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
tuchlovické	tuchlovický	k2eAgFnSc2d1	tuchlovický
čističky	čistička	k1gFnSc2	čistička
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
a	a	k8xC	a
několik	několik	k4yIc1	několik
menších	malý	k2eAgInPc2d2	menší
přítoků	přítok	k1gInPc2	přítok
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
obce	obec	k1gFnSc2	obec
Srby	Srba	k1gMnSc2	Srba
přitéká	přitékat	k5eAaImIp3nS	přitékat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Loděnickým	Loděnický	k2eAgInSc7d1	Loděnický
potokem	potok	k1gInSc7	potok
do	do	k7c2	do
Turyňského	Turyňský	k2eAgInSc2d1	Turyňský
rybníku	rybník	k1gInSc6	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
670	[number]	k4	670
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
