<s>
Škrkavka	škrkavka	k1gFnSc1	škrkavka
dětská	dětský	k2eAgFnSc1d1	dětská
(	(	kIx(	(
<g/>
Ascaris	Ascaris	k1gFnSc1	Ascaris
lumbricoides	lumbricoides	k1gMnSc1	lumbricoides
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
rozšířený	rozšířený	k2eAgMnSc1d1	rozšířený
parazit	parazit	k1gMnSc1	parazit
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
lidoopů	lidoop	k1gMnPc2	lidoop
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
hlístici	hlístice	k1gFnSc6	hlístice
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
vývojovým	vývojový	k2eAgInSc7d1	vývojový
cyklem	cyklus	k1gInSc7	cyklus
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
dospělci	dospělec	k1gMnPc1	dospělec
měří	měřit	k5eAaImIp3nP	měřit
15-20	[number]	k4	15-20
cm	cm	kA	cm
a	a	k8xC	a
lokalizují	lokalizovat	k5eAaBmIp3nP	lokalizovat
se	se	k3xPyFc4	se
v	v	k7c6	v
tenkém	tenký	k2eAgNnSc6d1	tenké
střevě	střevo	k1gNnSc6	střevo
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejběžnějších	běžný	k2eAgMnPc2d3	Nejběžnější
parazitů	parazit	k1gMnPc2	parazit
člověka	člověk	k1gMnSc2	člověk
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
je	být	k5eAaImIp3nS	být
infikováno	infikovat	k5eAaBmNgNnS	infikovat
kolem	kolem	k7c2	kolem
1,4	[number]	k4	1,4
miliardy	miliarda	k4xCgFnSc2	miliarda
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
a	a	k8xC	a
subtropických	subtropický	k2eAgFnPc6d1	subtropická
oblastech	oblast	k1gFnPc6	oblast
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
patogenita	patogenita	k1gFnSc1	patogenita
škrkavky	škrkavka	k1gFnSc2	škrkavka
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
fatální	fatální	k2eAgFnPc1d1	fatální
komplikace	komplikace	k1gFnPc1	komplikace
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
perforace	perforace	k1gFnSc2	perforace
nebo	nebo	k8xC	nebo
obturace	obturace	k1gFnSc2	obturace
(	(	kIx(	(
<g/>
ucpání	ucpání	k1gNnSc2	ucpání
<g/>
)	)	kIx)	)
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
či	či	k8xC	či
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
během	během	k7c2	během
migrace	migrace	k1gFnSc2	migrace
larválních	larvální	k2eAgNnPc2d1	larvální
stádií	stádium	k1gNnPc2	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
komplikace	komplikace	k1gFnPc4	komplikace
ročně	ročně	k6eAd1	ročně
na	na	k7c6	na
světě	svět	k1gInSc6	svět
umírá	umírat	k5eAaImIp3nS	umírat
8	[number]	k4	8
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
příbuzným	příbuzný	k2eAgMnSc7d1	příbuzný
druhem	druh	k1gMnSc7	druh
je	být	k5eAaImIp3nS	být
Ascaris	Ascaris	k1gInSc4	Ascaris
suum	suum	k6eAd1	suum
parazitující	parazitující	k2eAgMnSc1d1	parazitující
u	u	k7c2	u
prasat	prase	k1gNnPc2	prase
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
vznikají	vznikat	k5eAaImIp3nP	vznikat
oplozená	oplozený	k2eAgNnPc1d1	oplozené
vajíčka	vajíčko	k1gNnPc1	vajíčko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
hostitelského	hostitelský	k2eAgMnSc2d1	hostitelský
jedince	jedinec	k1gMnSc2	jedinec
ven	ven	k6eAd1	ven
výkaly	výkal	k1gInPc4	výkal
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
asi	asi	k9	asi
200	[number]	k4	200
000	[number]	k4	000
vajíček	vajíčko	k1gNnPc2	vajíčko
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
ve	v	k7c6	v
vajíčku	vajíčko	k1gNnSc6	vajíčko
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
larva	larva	k1gFnSc1	larva
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vajíčko	vajíčko	k1gNnSc1	vajíčko
stává	stávat	k5eAaImIp3nS	stávat
infekční	infekční	k2eAgNnSc1d1	infekční
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
pozření	pozření	k1gNnSc1	pozření
infekčních	infekční	k2eAgNnPc2d1	infekční
vajíček	vajíčko	k1gNnPc2	vajíčko
člověkem	člověk	k1gMnSc7	člověk
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
kvůli	kvůli	k7c3	kvůli
nedostatečné	dostatečný	k2eNgFnSc3d1	nedostatečná
hygieně	hygiena	k1gFnSc3	hygiena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
larva	larva	k1gFnSc1	larva
škrkavky	škrkavka	k1gFnSc2	škrkavka
putuje	putovat	k5eAaImIp3nS	putovat
do	do	k7c2	do
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Larva	larva	k1gFnSc1	larva
škrkavky	škrkavka	k1gFnSc2	škrkavka
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
krevním	krevní	k2eAgInSc7d1	krevní
oběhem	oběh	k1gInSc7	oběh
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
kašel	kašel	k1gInSc4	kašel
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
vykašlání	vykašlání	k1gNnSc1	vykašlání
hlenu	hlen	k1gInSc2	hlen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
spolknutí	spolknutí	k1gNnSc6	spolknutí
opět	opět	k6eAd1	opět
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
žaludku	žaludek	k1gInSc2	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
larva	larva	k1gFnSc1	larva
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
asi	asi	k9	asi
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
týdny	týden	k1gInPc7	týden
<g/>
.	.	kIx.	.
</s>
<s>
Zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
žaludku	žaludek	k1gInSc2	žaludek
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
larvy	larva	k1gFnPc1	larva
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
tenkého	tenký	k2eAgNnSc2d1	tenké
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
jedinec	jedinec	k1gMnSc1	jedinec
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Askarióza	Askarióza	k1gFnSc1	Askarióza
</s>
