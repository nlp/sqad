<s>
Leonardovy	Leonardův	k2eAgInPc1d1	Leonardův
rukopisy	rukopis	k1gInPc1	rukopis
a	a	k8xC	a
kresby	kresba	k1gFnPc1	kresba
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
zpřístupněny	zpřístupněn	k2eAgInPc4d1	zpřístupněn
zdarma	zdarma	k6eAd1	zdarma
na	na	k7c6	na
digitálním	digitální	k2eAgInSc6d1	digitální
archivu	archiv	k1gInSc6	archiv
E-Leo	E-Leo	k6eAd1	E-Leo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
založila	založit	k5eAaPmAgFnS	založit
Knihovna	knihovna	k1gFnSc1	knihovna
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinciho	Vinci	k1gMnSc2	Vinci
ve	v	k7c4	v
Vinci	Vinca	k1gMnPc4	Vinca
financovaný	financovaný	k2eAgInSc1d1	financovaný
společně	společně	k6eAd1	společně
Leonardovou	Leonardův	k2eAgFnSc7d1	Leonardova
knihovnou	knihovna	k1gFnSc7	knihovna
a	a	k8xC	a
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
