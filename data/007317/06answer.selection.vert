<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
publikace	publikace	k1gFnPc1	publikace
Philosophiæ	Philosophiæ	k1gFnSc2	Philosophiæ
Naturalis	Naturalis	k1gFnSc2	Naturalis
Principia	principium	k1gNnSc2	principium
Mathematica	Mathematic	k2eAgFnSc1d1	Mathematica
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1687	[number]	k4	1687
<g/>
,	,	kIx,	,
položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
bývá	bývat	k5eAaImIp3nS	bývat
řazena	řadit	k5eAaImNgFnS	řadit
mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
knihy	kniha	k1gFnPc4	kniha
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
