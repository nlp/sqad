<s>
High-definition	High-definition	k1gInSc1
television	television	k1gInSc1
(	(	kIx(
<g/>
HDTV	HDTV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neboli	neboli	k8xC
televize	televize	k1gFnSc1
s	s	k7c7
vysokým	vysoký	k2eAgNnSc7d1
rozlišením	rozlišení	k1gNnSc7
označuje	označovat	k5eAaImIp3nS
formát	formát	k1gInSc1
vysílání	vysílání	k1gNnSc2
televizního	televizní	k2eAgInSc2d1
signálu	signál	k1gInSc2
s	s	k7c7
výrazně	výrazně	k6eAd1
vyšším	vysoký	k2eAgNnSc7d2
rozlišením	rozlišení	k1gNnSc7
<g/>
,	,	kIx,
než	než	k8xS
jaké	jaký	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
umožňují	umožňovat	k5eAaImIp3nP
tradiční	tradiční	k2eAgInPc1d1
formáty	formát	k1gInPc1
(	(	kIx(
<g/>
PAL	pal	k1gInSc1
<g/>
,	,	kIx,
SECAM	SECAM	kA
<g/>
,	,	kIx,
NTSC	NTSC	kA
<g/>
)	)	kIx)
a	a	k8xC
SDTV	SDTV	kA
<g/>
.	.	kIx.
</s>