<s>
High-definition	High-definition	k1gInSc1
television	television	k1gInSc1
</s>
<s>
High-definition	High-definition	k1gInSc1
television	television	k1gInSc1
(	(	kIx(
<g/>
HDTV	HDTV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neboli	neboli	k8xC
televize	televize	k1gFnSc1
s	s	k7c7
vysokým	vysoký	k2eAgNnSc7d1
rozlišením	rozlišení	k1gNnSc7
označuje	označovat	k5eAaImIp3nS
formát	formát	k1gInSc1
vysílání	vysílání	k1gNnSc2
televizního	televizní	k2eAgInSc2d1
signálu	signál	k1gInSc2
s	s	k7c7
výrazně	výrazně	k6eAd1
vyšším	vysoký	k2eAgNnSc7d2
rozlišením	rozlišení	k1gNnSc7
<g/>
,	,	kIx,
než	než	k8xS
jaké	jaký	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
umožňují	umožňovat	k5eAaImIp3nP
tradiční	tradiční	k2eAgInPc1d1
formáty	formát	k1gInPc1
(	(	kIx(
<g/>
PAL	pal	k1gInSc1
<g/>
,	,	kIx,
SECAM	SECAM	kA
<g/>
,	,	kIx,
NTSC	NTSC	kA
<g/>
)	)	kIx)
a	a	k8xC
SDTV	SDTV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
HDTV	HDTV	kA
se	se	k3xPyFc4
vysílá	vysílat	k5eAaImIp3nS
digitálně	digitálně	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
může	moct	k5eAaImIp3nS
tak	tak	k6eAd1
být	být	k5eAaImF
použita	použít	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
jeden	jeden	k4xCgInSc1
z	z	k7c2
formátů	formát	k1gInPc2
digitální	digitální	k2eAgFnSc2d1
televize	televize	k1gFnSc2
DVB	DVB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
HDTV	HDTV	kA
definována	definovat	k5eAaBmNgFnS
pro	pro	k7c4
rozlišení	rozlišení	k1gNnSc4
1080	#num#	k4
prokládaných	prokládaný	k2eAgInPc2d1
(	(	kIx(
<g/>
tzv.	tzv.	kA
1080	#num#	k4
<g/>
i	i	k8xC
<g/>
)	)	kIx)
nebo	nebo	k8xC
neprokládaných	prokládaný	k2eNgFnPc2d1
(	(	kIx(
<g/>
progresivních	progresivní	k2eAgFnPc2d1
<g/>
;	;	kIx,
tzv.	tzv.	kA
1080	#num#	k4
<g/>
p	p	k?
<g/>
)	)	kIx)
řádků	řádek	k1gInPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
720	#num#	k4
neprokládaných	prokládaný	k2eNgInPc2d1
řádků	řádek	k1gInPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
720	#num#	k4
<g/>
p	p	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozměry	rozměra	k1gFnPc4
obrazu	obraz	k1gInSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
poměru	poměr	k1gInSc6
16	#num#	k4
:	:	kIx,
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Formáty	formát	k1gInPc1
</s>
<s>
Pro	pro	k7c4
označení	označení	k1gNnSc4
formátů	formát	k1gInPc2
obrazu	obraz	k1gInSc2
HDTV	HDTV	kA
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
tři	tři	k4xCgInPc1
údaje	údaj	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Počet	počet	k1gInSc1
řádků	řádek	k1gInPc2
<g/>
:	:	kIx,
720	#num#	k4
nebo	nebo	k8xC
1080	#num#	k4
</s>
<s>
Prokládání	prokládání	k1gNnSc1
<g/>
:	:	kIx,
i	i	k9
jako	jako	k9
interlaced	interlaced	k1gInSc1
(	(	kIx(
<g/>
prokládané	prokládaný	k2eAgNnSc1d1
řádkování	řádkování	k1gNnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
p	p	k?
jako	jako	k8xC,k8xS
progressive	progressiev	k1gFnSc2
(	(	kIx(
<g/>
neprokládané	prokládaný	k2eNgNnSc1d1
řádkování	řádkování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
snímků	snímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
:	:	kIx,
23,976	23,976	k4
<g/>
;	;	kIx,
24	#num#	k4
<g/>
;	;	kIx,
25	#num#	k4
<g/>
;	;	kIx,
30	#num#	k4
<g/>
;	;	kIx,
50	#num#	k4
nebo	nebo	k8xC
60	#num#	k4
</s>
<s>
První	první	k4xOgInSc1
údaj	údaj	k1gInSc1
o	o	k7c6
počtu	počet	k1gInSc6
řádků	řádek	k1gInPc2
udává	udávat	k5eAaImIp3nS
rozlišení	rozlišení	k1gNnSc1
obrazu	obraz	k1gInSc2
—	—	k?
720	#num#	k4
řádků	řádek	k1gInPc2
znamená	znamenat	k5eAaImIp3nS
rozlišení	rozlišení	k1gNnSc1
1280	#num#	k4
na	na	k7c4
720	#num#	k4
pixelů	pixel	k1gInPc2
<g/>
,	,	kIx,
1080	#num#	k4
řádků	řádek	k1gInPc2
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
1920	#num#	k4
<g/>
×	×	k?
<g/>
1080	#num#	k4
pixelů	pixel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Druhý	druhý	k4xOgInSc1
údaj	údaj	k1gInSc1
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
jestli	jestli	k8xS
má	mít	k5eAaImIp3nS
obraz	obraz	k1gInSc1
prokládané	prokládaný	k2eAgNnSc4d1
či	či	k8xC
neprokládané	prokládaný	k2eNgNnSc4d1
řádkování	řádkování	k1gNnSc4
–	–	k?
rozdíl	rozdíl	k1gInSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
neprokládané	prokládaný	k2eNgNnSc1d1
video	video	k1gNnSc1
vykreslí	vykreslit	k5eAaPmIp3nS
za	za	k7c4
sekundu	sekunda	k1gFnSc4
např.	např.	kA
25	#num#	k4
kompletních	kompletní	k2eAgInPc2d1
snímků	snímek	k1gInPc2
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
prokládané	prokládaný	k2eAgNnSc1d1
video	video	k1gNnSc1
vykreslí	vykreslit	k5eAaPmIp3nS
za	za	k7c4
sekundu	sekunda	k1gFnSc4
50	#num#	k4
půlsnímků	půlsnímek	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
každý	každý	k3xTgInSc4
jeden	jeden	k4xCgInSc4
plný	plný	k2eAgInSc4d1
snímek	snímek	k1gInSc4
se	se	k3xPyFc4
rozdělí	rozdělit	k5eAaPmIp3nS
na	na	k7c4
snímek	snímek	k1gInSc4
pouze	pouze	k6eAd1
s	s	k7c7
lichými	lichý	k2eAgInPc7d1
a	a	k8xC
na	na	k7c4
snímek	snímek	k1gInSc4
pouze	pouze	k6eAd1
se	s	k7c7
sudými	sudý	k2eAgInPc7d1
řádky	řádek	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
promítnuty	promítnut	k2eAgInPc1d1
po	po	k7c6
sobě	sebe	k3xPyFc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divák	divák	k1gMnSc1
tak	tak	k9
uvidí	uvidět	k5eAaPmIp3nS
zhruba	zhruba	k6eAd1
stejné	stejný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
obrazové	obrazový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
díval	dívat	k5eAaImAgMnS
na	na	k7c4
neprokládané	prokládaný	k2eNgNnSc4d1
video	video	k1gNnSc4
s	s	k7c7
25	#num#	k4
snímky	snímek	k1gInPc7
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
rychlejším	rychlý	k2eAgInSc6d2
sledu	sled	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
prokládání	prokládání	k1gNnSc1
umožnilo	umožnit	k5eAaPmAgNnS
zvýšit	zvýšit	k5eAaPmF
u	u	k7c2
vakuových	vakuový	k2eAgFnPc2d1
obrazovek	obrazovka	k1gFnPc2
snímkovou	snímkový	k2eAgFnSc4d1
frekvenci	frekvence	k1gFnSc4
z	z	k7c2
25	#num#	k4
snímků	snímek	k1gInPc2
na	na	k7c4
50	#num#	k4
půlsn	půlsna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
,	,	kIx,
takže	takže	k8xS
poblikávání	poblikávání	k1gNnSc1
obrazu	obraz	k1gInSc2
nebylo	být	k5eNaImAgNnS
tak	tak	k9
nepříjemné	příjemný	k2eNgNnSc1d1
(	(	kIx(
<g/>
pravda	pravda	k1gFnSc1
je	být	k5eAaImIp3nS
ale	ale	k8xC
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
že	že	k8xS
původně	původně	k6eAd1
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
ke	k	k7c3
snímkové	snímkový	k2eAgFnSc3d1
synchronizaci	synchronizace	k1gFnSc3
použito	použít	k5eAaPmNgNnS
kmitočtu	kmitočet	k1gInSc3
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
také	také	k9
ten	ten	k3xDgInSc1
posun	posun	k1gInSc1
z	z	k7c2
24	#num#	k4
políček	políček	k1gInSc4
filmu	film	k1gInSc2
na	na	k7c4
25	#num#	k4
snímků	snímek	k1gInPc2
televize	televize	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
obraz	obraz	k1gInSc1
s	s	k7c7
50	#num#	k4
půlsn	půlsno	k1gNnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
s	s	k7c7
dává	dávat	k5eAaImIp3nS
pocit	pocit	k1gInSc4
plynulejšího	plynulý	k2eAgInSc2d2
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dobách	doba	k1gFnPc6
analogové	analogový	k2eAgFnSc2d1
televize	televize	k1gFnSc2
bylo	být	k5eAaImAgNnS
prokládání	prokládání	k1gNnSc1
přínosné	přínosný	k2eAgNnSc1d1
pro	pro	k7c4
subjektivní	subjektivní	k2eAgNnSc4d1
zlepšení	zlepšení	k1gNnSc4
obrazu	obraz	k1gInSc2
při	při	k7c6
stejném	stejný	k2eAgInSc6d1
objemu	objem	k1gInSc6
obrazové	obrazový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1
zobrazovače	zobrazovač	k1gInPc1
problémy	problém	k1gInPc4
s	s	k7c7
poblikáváním	poblikávání	k1gNnSc7
v	v	k7c6
rytmu	rytmus	k1gInSc6
obrazové	obrazový	k2eAgFnSc2d1
frekvence	frekvence	k1gFnSc2
nemají	mít	k5eNaImIp3nP
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
od	od	k7c2
kompromisního	kompromisní	k2eAgNnSc2d1
prokládaní	prokládaný	k2eAgMnPc1d1
upouští	upouštět	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
normě	norma	k1gFnSc6
HDTV	HDTV	kA
se	se	k3xPyFc4
dnes	dnes	k6eAd1
vysílá	vysílat	k5eAaImIp3nS
prokládaně	prokládaně	k6eAd1
jen	jen	k9
u	u	k7c2
nejvyššího	vysoký	k2eAgNnSc2d3
rozlišení	rozlišení	k1gNnSc2
1920	#num#	k4
<g/>
×	×	k?
<g/>
1080	#num#	k4
pixelů	pixel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
již	již	k6eAd1
v	v	k7c6
nové	nový	k2eAgFnSc6d1
generaci	generace	k1gFnSc6
nosičů	nosič	k1gInPc2
Blu-ray	Blu-raa	k1gFnSc2
se	se	k3xPyFc4
prosadilo	prosadit	k5eAaPmAgNnS
neprokládané	prokládaný	k2eNgNnSc1d1
řádkování	řádkování	k1gNnSc1
i	i	k9
u	u	k7c2
rozlišení	rozlišení	k1gNnSc2
1920	#num#	k4
<g/>
×	×	k?
<g/>
1080	#num#	k4
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
dočkáme	dočkat	k5eAaPmIp1nP
této	tento	k3xDgFnSc2
špičkové	špičkový	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
obrazu	obraz	k1gInSc2
i	i	k8xC
u	u	k7c2
HDTV	HDTV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technika	technika	k1gFnSc1
neprokládaného	prokládaný	k2eNgNnSc2d1
snímání	snímání	k1gNnSc2
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
hlavně	hlavně	k9
při	při	k7c6
digitalizaci	digitalizace	k1gFnSc6
filmového	filmový	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
do	do	k7c2
HD	HD	kA
kvality	kvalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgInSc1
údaj	údaj	k1gInSc1
udává	udávat	k5eAaImIp3nS
počet	počet	k1gInSc4
snímků	snímek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
vykreslí	vykreslit	k5eAaPmIp3nP
za	za	k7c4
jednu	jeden	k4xCgFnSc4
sekundu	sekunda	k1gFnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
prokládaný	prokládaný	k2eAgInSc4d1
obraz	obraz	k1gInSc4
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
jen	jen	k9
frekvence	frekvence	k1gFnSc2
50	#num#	k4
a	a	k8xC
60	#num#	k4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
označují	označovat	k5eAaImIp3nP
počet	počet	k1gInSc4
půlsnímků	půlsnímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
<g/>
:	:	kIx,
Označení	označení	k1gNnSc1
720	#num#	k4
<g/>
p	p	k?
<g/>
23.976	23.976	k4
se	se	k3xPyFc4
použije	použít	k5eAaPmIp3nS
pro	pro	k7c4
neprokládaný	prokládaný	k2eNgInSc4d1
obraz	obraz	k1gInSc4
s	s	k7c7
rozlišením	rozlišení	k1gNnSc7
1280	#num#	k4
×	×	k?
720	#num#	k4
bodů	bod	k1gInPc2
a	a	k8xC
23,976	23,976	k4
snímky	snímek	k1gInPc7
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
1080	#num#	k4
<g/>
i	i	k9
<g/>
50	#num#	k4
znamená	znamenat	k5eAaImIp3nS
rozlišení	rozlišení	k1gNnSc4
prokládaného	prokládaný	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
1920	#num#	k4
×	×	k?
1080	#num#	k4
bodů	bod	k1gInPc2
s	s	k7c7
50	#num#	k4
půlsnímky	půlsnímek	k1gInPc7
(	(	kIx(
<g/>
tj.	tj.	kA
25	#num#	k4
celosnímky	celosnímka	k1gFnPc1
<g/>
)	)	kIx)
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
tento	tento	k3xDgInSc1
formát	formát	k1gInSc1
HDTV	HDTV	kA
označuje	označovat	k5eAaImIp3nS
také	také	k9
jako	jako	k9
1080	#num#	k4
<g/>
i	i	k9
<g/>
25	#num#	k4
<g/>
,	,	kIx,
protože	protože	k8xS
označování	označování	k1gNnSc1
formátů	formát	k1gInPc2
s	s	k7c7
prokládanými	prokládaný	k2eAgInPc7d1
řádky	řádek	k1gInPc7
není	být	k5eNaImIp3nS
jednotné	jednotný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údaj	údaj	k1gInSc1
o	o	k7c6
počtu	počet	k1gInSc6
snímků	snímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
se	se	k3xPyFc4
často	často	k6eAd1
vynechává	vynechávat	k5eAaImIp3nS
a	a	k8xC
většinou	většinou	k6eAd1
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
hodnota	hodnota	k1gFnSc1
50	#num#	k4
u	u	k7c2
prokládaného	prokládaný	k2eAgNnSc2d1
videa	video	k1gNnSc2
a	a	k8xC
hodnota	hodnota	k1gFnSc1
23,976	23,976	k4
u	u	k7c2
videa	video	k1gNnSc2
neprokládaného	prokládaný	k2eNgNnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Volba	volba	k1gFnSc1
formátu	formát	k1gInSc2
</s>
<s>
Vhodný	vhodný	k2eAgInSc1d1
formát	formát	k1gInSc1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
typu	typ	k1gInSc6
zdrojového	zdrojový	k2eAgNnSc2d1
média	médium	k1gNnSc2
a	a	k8xC
také	také	k9
na	na	k7c6
druhu	druh	k1gInSc6
obsahu	obsah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišení	rozlišení	k1gNnPc1
a	a	k8xC
počet	počet	k1gInSc1
snímků	snímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
voleny	volit	k5eAaImNgInP
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
rozlišení	rozlišení	k1gNnSc4
a	a	k8xC
počet	počet	k1gInSc4
snímků	snímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
zdroje	zdroj	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vhodně	vhodně	k6eAd1
využít	využít	k5eAaPmF
transformací	transformace	k1gFnSc7
těchto	tento	k3xDgInPc2
parametrů	parametr	k1gInPc2
přímo	přímo	k6eAd1
v	v	k7c4
set-top	set-top	k1gInSc4
boxech	box	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoké	vysoká	k1gFnSc2
rozlišení	rozlišení	k1gNnPc2
vyžaduje	vyžadovat	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
datový	datový	k2eAgInSc1d1
tok	tok	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
ztrátová	ztrátový	k2eAgFnSc1d1
komprese	komprese	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zmenší	zmenšit	k5eAaPmIp3nS
nároky	nárok	k1gInPc4
na	na	k7c4
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
zhorší	zhoršit	k5eAaPmIp3nS
kvalitu	kvalita	k1gFnSc4
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
nekomprimovaným	komprimovaný	k2eNgInSc7d1
obrazem	obraz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
35	#num#	k4
<g/>
mm	mm	kA
film	film	k1gInSc1
používaný	používaný	k2eAgInSc1d1
v	v	k7c6
kinematografii	kinematografie	k1gFnSc6
má	mít	k5eAaImIp3nS
rozlišení	rozlišení	k1gNnSc1
srovnatelné	srovnatelný	k2eAgNnSc1d1
s	s	k7c7
formáty	formát	k1gInPc1
Full	Fulla	k1gFnPc2
HDTV	HDTV	kA
nebo	nebo	k8xC
větší	veliký	k2eAgFnSc4d2
a	a	k8xC
obrazovou	obrazový	k2eAgFnSc4d1
frekvenci	frekvence	k1gFnSc4
24	#num#	k4
sn	sn	k?
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Pro	pro	k7c4
naskenování	naskenování	k1gNnSc4
takového	takový	k3xDgInSc2
filmu	film	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
možnostech	možnost	k1gFnPc6
rychlosti	rychlost	k1gFnSc2
datového	datový	k2eAgInSc2d1
přenosu	přenos	k1gInSc2
a	a	k8xC
požadovaném	požadovaný	k2eAgNnSc6d1
množství	množství	k1gNnSc6
detailů	detail	k1gInPc2
v	v	k7c6
obraze	obraz	k1gInSc6
jako	jako	k8xS,k8xC
ideální	ideální	k2eAgMnSc1d1
jeví	jevit	k5eAaImIp3nP
formáty	formát	k1gInPc1
720	#num#	k4
<g/>
p	p	k?
<g/>
24	#num#	k4
a	a	k8xC
1080	#num#	k4
<g/>
p	p	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
takový	takový	k3xDgInSc1
film	film	k1gInSc1
vysílá	vysílat	k5eAaImIp3nS
ve	v	k7c6
standardu	standard	k1gInSc6
PAL	pal	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
ho	on	k3xPp3gMnSc4
převést	převést	k5eAaPmF
na	na	k7c4
25	#num#	k4
snímků	snímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
zrychlením	zrychlení	k1gNnSc7
o	o	k7c4
4,1	4,1	k4
procenta	procento	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
vysílání	vysílání	k1gNnSc4
ve	v	k7c6
standardu	standard	k1gInSc6
NTSC	NTSC	kA
(	(	kIx(
<g/>
30	#num#	k4
snímků	snímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
)	)	kIx)
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
převod	převod	k1gInSc1
„	„	k?
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pulldown	pulldown	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Nahrávky	nahrávka	k1gFnPc1
určené	určený	k2eAgFnPc1d1
přímo	přímo	k6eAd1
pro	pro	k7c4
HDTV	HDTV	kA
vysílání	vysílání	k1gNnSc1
se	se	k3xPyFc4
pořizují	pořizovat	k5eAaImIp3nP
ve	v	k7c6
formátu	formát	k1gInSc6
720	#num#	k4
<g/>
p	p	k?
nebo	nebo	k8xC
1080	#num#	k4
<g/>
i.	i.	k?
Použitý	použitý	k2eAgInSc1d1
typ	typ	k1gInSc1
závisí	záviset	k5eAaImIp3nS
na	na	k7c4
televizní	televizní	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
formát	formát	k1gInSc1
720	#num#	k4
<g/>
p	p	k?
se	se	k3xPyFc4
více	hodně	k6eAd2
hodí	hodit	k5eAaPmIp3nS,k5eAaImIp3nS
pro	pro	k7c4
záznamy	záznam	k1gInPc4
obsahující	obsahující	k2eAgInSc4d1
rychlý	rychlý	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
(	(	kIx(
<g/>
např.	např.	kA
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1080	#num#	k4
<g/>
i	i	k8xC
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
nabídne	nabídnout	k5eAaPmIp3nS
více	hodně	k6eAd2
detailů	detail	k1gInPc2
při	při	k7c6
statických	statický	k2eAgFnPc6d1
nebo	nebo	k8xC
málo	málo	k6eAd1
pohyblivých	pohyblivý	k2eAgInPc6d1
záběrech	záběr	k1gInPc6
<g/>
.	.	kIx.
720	#num#	k4
<g/>
p	p	k?
se	se	k3xPyFc4
také	také	k9
hodně	hodně	k6eAd1
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
distribuci	distribuce	k1gFnSc4
HD	HD	kA
videa	video	k1gNnSc2
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
počítačové	počítačový	k2eAgInPc1d1
monitory	monitor	k1gInPc1
nepoužívají	používat	k5eNaImIp3nP
prokládání	prokládání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
USA	USA	kA
používají	používat	k5eAaImIp3nP
formát	formát	k1gInSc1
720	#num#	k4
<g/>
p	p	k?
stanice	stanice	k1gFnSc1
Fox	fox	k1gInSc1
<g/>
,	,	kIx,
ABC	ABC	kA
a	a	k8xC
ESPN	ESPN	kA
<g/>
,	,	kIx,
ve	v	k7c6
formátu	formát	k1gInSc6
1080	#num#	k4
<g/>
i	i	k8xC
vysílají	vysílat	k5eAaImIp3nP
NBC	NBC	kA
<g/>
,	,	kIx,
Universal	Universal	k1gFnSc1
HD	HD	kA
<g/>
,	,	kIx,
CBS	CBS	kA
<g/>
,	,	kIx,
HBO	HBO	kA
HD	HD	kA
<g/>
,	,	kIx,
INHD	INHD	kA
<g/>
,	,	kIx,
HDNet	HDNet	k1gInSc1
a	a	k8xC
TNT	TNT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropské	evropský	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
s	s	k7c7
vysokým	vysoký	k2eAgNnSc7d1
rozlišením	rozlišení	k1gNnSc7
Sky	Sky	k1gFnSc2
Deutschland	Deutschlanda	k1gFnPc2
<g/>
,	,	kIx,
Sky	Sky	k1gFnPc2
<g/>
,	,	kIx,
Anixe	Anixe	k1gFnSc2
HD	HD	kA
<g/>
,	,	kIx,
Sat	Sat	k1gFnSc1
<g/>
1	#num#	k4
a	a	k8xC
ProSieben	ProSiebna	k1gFnPc2
používají	používat	k5eAaImIp3nP
formát	formát	k1gInSc1
1080	#num#	k4
<g/>
i.	i.	k?
</s>
<s>
pozn	pozn	kA
<g/>
.	.	kIx.
Z	z	k7c2
družice	družice	k1gFnSc2
Astra	astra	k1gFnSc1
19,2	19,2	k4
<g/>
°	°	k?
<g/>
E	E	kA
se	se	k3xPyFc4
volné	volný	k2eAgNnSc4d1
HD	HD	kA
stanice	stanice	k1gFnSc2
vysílají	vysílat	k5eAaImIp3nP
většinou	většina	k1gFnSc7
ve	v	k7c6
720	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
ORF	ORF	kA
<g/>
,	,	kIx,
3	#num#	k4
<g/>
sat	sat	k?
<g/>
,	,	kIx,
HD	HD	kA
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přenos	přenos	k1gInSc1
obrazu	obraz	k1gInSc2
a	a	k8xC
zvuku	zvuk	k1gInSc2
</s>
<s>
Pro	pro	k7c4
kompresi	komprese	k1gFnSc4
obrazu	obraz	k1gInSc2
HDTV	HDTV	kA
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
kodek	kodek	k1gInSc1
MPEG-4	MPEG-4	k1gFnSc2
AVC	AVC	kA
(	(	kIx(
<g/>
H	H	kA
<g/>
.264	.264	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
díky	díky	k7c3
vyšší	vysoký	k2eAgFnSc3d2
kompresi	komprese	k1gFnSc3
využít	využít	k5eAaPmF
stejný	stejný	k2eAgInSc4d1
datový	datový	k2eAgInSc4d1
tok	tok	k1gInSc4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
vyššího	vysoký	k2eAgInSc2d2
počtu	počet	k1gInSc2
pixelů	pixel	k1gInPc2
beze	beze	k7c2
ztráty	ztráta	k1gFnSc2
kvality	kvalita	k1gFnSc2
obrazu	obraz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kodek	kodek	k1gInSc1
MPEG-2	MPEG-2	k1gFnSc2
Video	video	k1gNnSc1
využívaný	využívaný	k2eAgInSc4d1
u	u	k7c2
digitální	digitální	k2eAgFnSc2d1
televize	televize	k1gFnSc2
standarního	standarní	k2eAgNnSc2d1
rozlišení	rozlišení	k1gNnSc2
by	by	kYmCp3nS
při	při	k7c6
zachování	zachování	k1gNnSc6
stejné	stejný	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
obrazu	obraz	k1gInSc2
vyžadoval	vyžadovat	k5eAaImAgMnS
pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
počtu	počet	k1gInSc2
pixelů	pixel	k1gInPc2
mnohem	mnohem	k6eAd1
vyšší	vysoký	k2eAgInSc4d2
datový	datový	k2eAgInSc4d1
tok	tok	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
zbytečně	zbytečně	k6eAd1
ubíral	ubírat	k5eAaImAgMnS
prostor	prostor	k1gInSc4
v	v	k7c6
kanálovém	kanálový	k2eAgInSc6d1
multiplexu	multiplex	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgFnPc3d3
problém	problém	k1gInSc1
při	při	k7c6
zavádění	zavádění	k1gNnSc6
MPEG-4	MPEG-4	k1gFnPc2
je	být	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
zákazníci	zákazník	k1gMnPc1
potřebují	potřebovat	k5eAaImIp3nP
přijímač	přijímač	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
tento	tento	k3xDgInSc1
kodek	kodek	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
znamená	znamenat	k5eAaImIp3nS
nákup	nákup	k1gInSc4
nového	nový	k2eAgInSc2d1
přístroje	přístroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
některé	některý	k3yIgInPc1
státy	stát	k1gInPc1
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
nechtějí	chtít	k5eNaImIp3nP
dělat	dělat	k5eAaImF
digitalizaci	digitalizace	k1gFnSc4
nadvakrát	nadvakrát	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
rovnou	rovnou	k6eAd1
zavést	zavést	k5eAaPmF
formát	formát	k1gInSc4
MPEG-	MPEG-	k1gFnSc2
<g/>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
DVB	DVB	kA
(	(	kIx(
<g/>
Digital	Digital	kA
Video	video	k1gNnSc1
Broadcasting	Broadcasting	k1gInSc1
<g/>
,	,	kIx,
DVB-S	DVB-S	k1gMnSc1
<g/>
,	,	kIx,
DVB-T	DVB-T	k1gMnSc1
<g/>
,	,	kIx,
DVB-C	DVB-C	k1gMnSc1
<g/>
)	)	kIx)
SDTV	SDTV	kA
i	i	k8xC
HDTV	HDTV	kA
podporuje	podporovat	k5eAaImIp3nS
přenos	přenos	k1gInSc1
prostorového	prostorový	k2eAgInSc2d1
zvuku	zvuk	k1gInSc2
ve	v	k7c6
formátu	formát	k1gInSc6
DD	DD	kA
Dolby	Dolba	k1gFnSc2
Digital	Digital	kA
(	(	kIx(
<g/>
kodek	kodek	k1gInSc1
AC	AC	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
5.1	5.1	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
8.12	8.12	k4
<g/>
.2008	.2008	k4
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
doplnila	doplnit	k5eAaPmAgFnS
DVB-S	DVB-S	k1gFnSc4
(	(	kIx(
<g/>
S	s	k7c7
-	-	kIx~
satelitní	satelitní	k2eAgFnSc7d1
<g/>
,	,	kIx,
na	na	k7c4
pozici	pozice	k1gFnSc4
23	#num#	k4
<g/>
°	°	k?
<g/>
E	E	kA
<g/>
)	)	kIx)
vysílání	vysílání	k1gNnSc1
o	o	k7c4
volitelnou	volitelný	k2eAgFnSc4d1
AC3	AC3	k1gFnSc4
stopou	stopa	k1gFnSc7
-	-	kIx~
DD	DD	kA
<g/>
2.0	2.0	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
příslibem	příslib	k1gInSc7
filmů	film	k1gInPc2
v	v	k7c6
AC3	AC3	k1gFnSc6
formátu	formát	k1gInSc2
DD	DD	kA
<g/>
5.1	5.1	k4
(	(	kIx(
<g/>
tedy	tedy	k8xC
6	#num#	k4
kanálový	kanálový	k2eAgInSc4d1
zvuk	zvuk	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Potřebné	potřebný	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
</s>
<s>
Ke	k	k7c3
sledování	sledování	k1gNnSc3
HDTV	HDTV	kA
je	on	k3xPp3gInPc4
třeba	třeba	k6eAd1
mít	mít	k5eAaImF
zařízení	zařízení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
schopné	schopný	k2eAgNnSc1d1
pracovat	pracovat	k5eAaImF
s	s	k7c7
vyšším	vysoký	k2eAgNnSc7d2
rozlišením	rozlišení	k1gNnSc7
normy	norma	k1gFnSc2
(	(	kIx(
<g/>
výrobci	výrobce	k1gMnPc1
ho	on	k3xPp3gMnSc4
označují	označovat	k5eAaImIp3nP
jako	jako	k8xS,k8xC
„	„	k?
<g/>
HD	HD	kA
ready	ready	k0
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
HD	HD	kA
Ready	ready	k0
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
označována	označován	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
dokáží	dokázat	k5eAaPmIp3nP
zpracovat	zpracovat	k5eAaPmF
obraz	obraz	k1gInSc4
ve	v	k7c6
standardech	standard	k1gInPc6
720	#num#	k4
<g/>
p	p	k?
a	a	k8xC
1080	#num#	k4
<g/>
i.	i.	k?
HD	HD	kA
Ready	ready	k0
neoznačuje	označovat	k5eNaImIp3nS
plné	plný	k2eAgFnSc3d1
HD	HD	kA
rozlišení	rozlišení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
„	„	k?
<g/>
Full	Fulla	k1gFnPc2
HD	HD	kA
<g/>
“	“	k?
a	a	k8xC
je	být	k5eAaImIp3nS
definováno	definovat	k5eAaBmNgNnS
rozlišením	rozlišení	k1gNnSc7
1920	#num#	k4
x	x	k?
1080	#num#	k4
neprokládaně	prokládaně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sledováním	sledování	k1gNnSc7
HDTV	HDTV	kA
na	na	k7c6
klasické	klasický	k2eAgFnSc6d1
televizi	televize	k1gFnSc6
s	s	k7c7
PAL	pal	k1gInSc4
(	(	kIx(
<g/>
pomocí	pomocí	k7c2
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
dokáže	dokázat	k5eAaPmIp3nS
převést	převést	k5eAaPmF
HDTV	HDTV	kA
na	na	k7c4
PAL	pal	k1gInSc4
<g/>
)	)	kIx)
dosáhnete	dosáhnout	k5eAaPmIp2nP
jen	jen	k9
o	o	k7c4
něco	něco	k3yInSc4
málo	málo	k6eAd1
lepší	dobrý	k2eAgFnSc2d2
kvality	kvalita	k1gFnSc2
oproti	oproti	k7c3
analogovému	analogový	k2eAgNnSc3d1
vysílání	vysílání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
specifikace	specifikace	k1gFnSc1
takto	takto	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Full	Full	k1gInSc1
HD	HD	kA
<g/>
/	/	kIx~
<g/>
1080	#num#	k4
<g/>
p	p	k?
<g/>
“	“	k?
apod.	apod.	kA
</s>
<s>
Výhody	výhoda	k1gFnPc1
HDTV	HDTV	kA
</s>
<s>
HDTV	HDTV	kA
se	se	k3xPyFc4
vysílá	vysílat	k5eAaImIp3nS
digitálně	digitálně	k6eAd1
—	—	k?
obraz	obraz	k1gInSc1
tak	tak	k9
bude	být	k5eAaImBp3nS
většinou	většinou	k6eAd1
bez	bez	k7c2
chyby	chyba	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
pokud	pokud	k8xS
nebude	být	k5eNaImBp3nS
čistý	čistý	k2eAgInSc1d1
<g/>
,	,	kIx,
zobrazí	zobrazit	k5eAaPmIp3nS
se	se	k3xPyFc4
kostičkovaný	kostičkovaný	k2eAgMnSc1d1
<g/>
,	,	kIx,
trhaný	trhaný	k2eAgMnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
nezobrazí	zobrazit	k5eNaPmIp3nP
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdy	nikdy	k6eAd1
ale	ale	k8xC
nebude	být	k5eNaImBp3nS
zašuměný	zašuměný	k2eAgMnSc1d1
<g/>
,	,	kIx,
vybledlý	vybledlý	k2eAgMnSc1d1
<g/>
,	,	kIx,
různě	různě	k6eAd1
posunutý	posunutý	k2eAgMnSc1d1
nebo	nebo	k8xC
s	s	k7c7
tzv.	tzv.	kA
"	"	kIx"
<g/>
duchy	duch	k1gMnPc7
<g/>
"	"	kIx"
jako	jako	k8xS,k8xC
v	v	k7c6
případě	případ	k1gInSc6
zastaralých	zastaralý	k2eAgInPc2d1
analogových	analogový	k2eAgInPc2d1
televizních	televizní	k2eAgInPc2d1
signálů	signál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Obraz	obraz	k1gInSc1
je	být	k5eAaImIp3nS
vysílán	vysílat	k5eAaImNgInS
v	v	k7c6
širokoúhlém	širokoúhlý	k2eAgInSc6d1
formátu	formát	k1gInSc6
16	#num#	k4
:	:	kIx,
9	#num#	k4
(	(	kIx(
<g/>
filmy	film	k1gInPc1
v	v	k7c6
ještě	ještě	k6eAd1
širších	široký	k2eAgInPc6d2
formátech	formát	k1gInPc6
jako	jako	k8xC,k8xS
např.	např.	kA
2,35	2,35	k4
:	:	kIx,
1	#num#	k4
(	(	kIx(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS
poměru	poměra	k1gFnSc4
stran	strana	k1gFnPc2
16	#num#	k4
:	:	kIx,
7	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
vysílají	vysílat	k5eAaImIp3nP
s	s	k7c7
pruhy	pruh	k1gInPc7
nahoře	nahoře	k6eAd1
a	a	k8xC
dole	dole	k6eAd1
—	—	k?
tzv.	tzv.	kA
„	„	k?
<g/>
letterbox	letterbox	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starý	k2eAgInPc1d2
filmy	film	k1gInPc1
a	a	k8xC
jiné	jiný	k2eAgInPc1d1
televizní	televizní	k2eAgInPc1d1
programy	program	k1gInPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
zachovat	zachovat	k5eAaPmF
poměr	poměr	k1gInSc4
4	#num#	k4
:	:	kIx,
3	#num#	k4
(	(	kIx(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS
poměru	poměra	k1gFnSc4
stran	strana	k1gFnPc2
16	#num#	k4
:	:	kIx,
12	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
vysílají	vysílat	k5eAaImIp3nP
se	s	k7c7
svislými	svislý	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
po	po	k7c6
stranách	strana	k1gFnPc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
„	„	k?
<g/>
pillar	pillar	k1gInSc1
box	box	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevují	objevovat	k5eAaImIp3nP
se	se	k3xPyFc4
však	však	k9
už	už	k6eAd1
i	i	k9
první	první	k4xOgFnSc1
televize	televize	k1gFnSc1
s	s	k7c7
poměrem	poměr	k1gInSc7
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
blízký	blízký	k2eAgInSc1d1
2,35	2,35	k4
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Barvy	barva	k1gFnPc1
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
přechody	přechod	k1gInPc1
vypadají	vypadat	k5eAaPmIp3nP,k5eAaImIp3nP
realističtěji	realisticky	k6eAd2
díky	díky	k7c3
jejich	jejich	k3xOp3gInSc3
většímu	veliký	k2eAgInSc3d2
použitelnému	použitelný	k2eAgInSc3d1
rozsahu	rozsah	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Vizuální	vizuální	k2eAgFnSc1d1
informace	informace	k1gFnSc1
je	být	k5eAaImIp3nS
celkově	celkově	k6eAd1
2	#num#	k4
<g/>
×	×	k?
až	až	k9
5	#num#	k4
<g/>
×	×	k?
podrobnější	podrobný	k2eAgFnSc2d2
<g/>
,	,	kIx,
mezery	mezera	k1gFnPc4
mezi	mezi	k7c7
řádky	řádek	k1gInPc7
jsou	být	k5eAaImIp3nP
menší	malý	k2eAgInPc1d2
nebo	nebo	k8xC
nepostřehnutelné	postřehnutelný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgFnSc1d2
podrobnost	podrobnost	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
pohodlné	pohodlný	k2eAgNnSc4d1
sledování	sledování	k1gNnSc4
na	na	k7c6
větších	veliký	k2eAgFnPc6d2
úhlopříčkách	úhlopříčka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgNnPc1d1
média	médium	k1gNnPc1
Blu-ray	Blu-raa	k1gFnSc2
(	(	kIx(
<g/>
i	i	k9
dnes	dnes	k6eAd1
již	již	k6eAd1
nevyvíjená	vyvíjený	k2eNgNnPc1d1
a	a	k8xC
neprodávaná	prodávaný	k2eNgNnPc1d1
média	médium	k1gNnPc1
HD	HD	kA
DVD	DVD	kA
<g/>
)	)	kIx)
podporují	podporovat	k5eAaImIp3nP
formáty	formát	k1gInPc1
HDTV	HDTV	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Slovensko	Slovensko	k1gNnSc1
se	se	k3xPyFc4
přiklání	přiklánět	k5eAaImIp3nS
k	k	k7c3
DVB-T	DVB-T	k1gFnSc3
v	v	k7c6
MPEG-	MPEG-	k1gFnSc6
<g/>
4	#num#	k4
<g/>
,	,	kIx,
parabola	parabola	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Philips	Philips	kA
TV	TV	kA
v	v	k7c4
kino	kino	k1gNnSc4
formátu	formát	k1gInSc2
21	#num#	k4
<g/>
:	:	kIx,
<g/>
9	#num#	k4
<g/>
,	,	kIx,
hdworld	hdworld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
AudioVideo	AudioVideo	k1gMnSc1
<g/>
,	,	kIx,
19.01	19.01	k4
<g/>
.2009	.2009	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
High-definition	High-definition	k1gInSc1
television	television	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
HD	HD	kA
formát	formát	k1gInSc1
<g/>
,	,	kIx,
www.elektrotechnika.web2001.cz	www.elektrotechnika.web2001.cz	k1gInSc1
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
skrývá	skrývat	k5eAaImIp3nS
za	za	k7c7
písmeny	písmeno	k1gNnPc7
HDTV	HDTV	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
radiotv	radiotv	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Výhody	výhoda	k1gFnPc1
HDTV	HDTV	kA
<g/>
,	,	kIx,
hdtvblog	hdtvblog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Novinky	novinka	k1gFnPc1
ze	z	k7c2
světa	svět	k1gInSc2
HDTV	HDTV	kA
<g/>
,	,	kIx,
hdtvblog	hdtvblog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Novinky	novinka	k1gFnPc1
ze	z	k7c2
světa	svět	k1gInSc2
HDTV	HDTV	kA
a	a	k8xC
Blu-ray	Blu-raa	k1gFnPc1
<g/>
,	,	kIx,
hdworld	hdworld	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Srovnání	srovnání	k1gNnSc1
HDTV	HDTV	kA
a	a	k8xC
SDTV	SDTV	kA
<g/>
,	,	kIx,
videotech	videot	k1gInPc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4191818-6	4191818-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
88001765	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
88001765	#num#	k4
</s>
