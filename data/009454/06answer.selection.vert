<s>
Trinitární	trinitární	k2eAgFnSc1d1	trinitární
teologie	teologie	k1gFnSc1	teologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
trinitas	trinitas	k1gInSc1	trinitas
trojice	trojice	k1gFnSc1	trojice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odvětvím	odvětví	k1gNnSc7	odvětví
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
dogmatem	dogma	k1gNnSc7	dogma
o	o	k7c6	o
Nejsvětější	nejsvětější	k2eAgFnSc6d1	nejsvětější
Trojici	trojice	k1gFnSc6	trojice
<g/>
.	.	kIx.	.
</s>
