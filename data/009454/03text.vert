<p>
<s>
Trinitární	trinitární	k2eAgFnSc1d1	trinitární
teologie	teologie	k1gFnSc1	teologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
trinitas	trinitas	k1gInSc1	trinitas
trojice	trojice	k1gFnSc1	trojice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odvětvím	odvětví	k1gNnSc7	odvětví
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
dogmatem	dogma	k1gNnSc7	dogma
o	o	k7c6	o
Nejsvětější	nejsvětější	k2eAgFnSc6d1	nejsvětější
Trojici	trojice	k1gFnSc6	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Trinitární	trinitární	k2eAgFnSc1d1	trinitární
teologie	teologie	k1gFnSc1	teologie
náleží	náležet	k5eAaImIp3nS	náležet
vedle	vedle	k7c2	vedle
christologie	christologie	k1gFnSc2	christologie
k	k	k7c3	k
základním	základní	k2eAgNnPc3d1	základní
teologickým	teologický	k2eAgNnPc3d1	teologické
odvětvím	odvětví	k1gNnPc3	odvětví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Učení	učení	k1gNnSc1	učení
o	o	k7c6	o
Nejsvětější	nejsvětější	k2eAgFnSc6d1	nejsvětější
Trojici	trojice	k1gFnSc6	trojice
formulovala	formulovat	k5eAaImAgFnS	formulovat
církev	církev	k1gFnSc1	církev
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
čtyřech	čtyři	k4xCgInPc6	čtyři
ekumenických	ekumenický	k2eAgInPc6d1	ekumenický
koncilech	koncil	k1gInPc6	koncil
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zahrnuto	zahrnout	k5eAaPmNgNnS	zahrnout
v	v	k7c4	v
křesťany	křesťan	k1gMnPc4	křesťan
obecně	obecně	k6eAd1	obecně
uznávaných	uznávaný	k2eAgNnPc6d1	uznávané
vyznáních	vyznání	k1gNnPc6	vyznání
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
nicejsko-konstantinopolském	nicejskoonstantinopolský	k2eAgNnSc6d1	nicejsko-konstantinopolské
vyznání	vyznání	k1gNnSc6	vyznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
trinitární	trinitární	k2eAgInPc4d1	trinitární
spisy	spis	k1gInPc4	spis
patří	patřit	k5eAaImIp3nP	patřit
De	De	k?	De
trinitate	trinitat	k1gInSc5	trinitat
Hilaria	Hilarium	k1gNnPc4	Hilarium
z	z	k7c2	z
Poitiers	Poitiersa	k1gFnPc2	Poitiersa
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
De	De	k?	De
trinitate	trinitat	k1gInSc5	trinitat
Aurelia	Aurelius	k1gMnSc2	Aurelius
Augustina	Augustin	k1gMnSc4	Augustin
z	z	k7c2	z
první	první	k4xOgFnSc2	první
třetiny	třetina	k1gFnSc2	třetina
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Augustinově	Augustinův	k2eAgFnSc6d1	Augustinova
trinitární	trinitární	k2eAgFnSc6d1	trinitární
teologii	teologie	k1gFnSc6	teologie
hraje	hrát	k5eAaImIp3nS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
úlohu	úloha	k1gFnSc4	úloha
jeho	jeho	k3xOp3gFnSc2	jeho
teorie	teorie	k1gFnSc2	teorie
vnitrobožských	vnitrobožský	k2eAgInPc2d1	vnitrobožský
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Boží	boží	k2eAgFnSc1d1	boží
podstata	podstata	k1gFnSc1	podstata
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
nepodléhající	podléhající	k2eNgFnSc4d1	nepodléhající
žádné	žádný	k3yNgFnSc3	žádný
změně	změna	k1gFnSc3	změna
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
případkové	případek	k1gMnPc1	případek
bytí	bytí	k1gNnSc2	bytí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Augustin	Augustin	k1gMnSc1	Augustin
hledal	hledat	k5eAaImAgMnS	hledat
rozlišení	rozlišení	k1gNnSc4	rozlišení
Otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Syna	syn	k1gMnSc2	syn
a	a	k8xC	a
Ducha	duch	k1gMnSc2	duch
Svatého	svatý	k2eAgMnSc2d1	svatý
pomocí	pomocí	k7c2	pomocí
kategorie	kategorie	k1gFnSc2	kategorie
vztahu	vztah	k1gInSc2	vztah
(	(	kIx(	(
<g/>
nebyl	být	k5eNaImAgInS	být
prvním	první	k4xOgMnPc3	první
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
počátek	počátek	k1gInSc4	počátek
v	v	k7c6	v
teologii	teologie	k1gFnSc6	teologie
sv.	sv.	kA	sv.
Řehoře	Řehoř	k1gMnSc2	Řehoř
Naziánského	Naziánský	k2eAgMnSc2d1	Naziánský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bohu	bůh	k1gMnSc3	bůh
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
subsistující	subsistující	k2eAgInPc4d1	subsistující
vztahy	vztah	k1gInPc4	vztah
otcovství	otcovství	k1gNnSc2	otcovství
<g/>
,	,	kIx,	,
synovství	synovství	k1gNnSc2	synovství
a	a	k8xC	a
pasivního	pasivní	k2eAgNnSc2d1	pasivní
darování	darování	k1gNnSc2	darování
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
věčně	věčně	k6eAd1	věčně
plodí	plodit	k5eAaImIp3nS	plodit
Syna	syn	k1gMnSc4	syn
a	a	k8xC	a
Syn	syn	k1gMnSc1	syn
"	"	kIx"	"
<g/>
vydechuje	vydechovat	k5eAaImIp3nS	vydechovat
<g/>
"	"	kIx"	"
Ducha	duch	k1gMnSc4	duch
Svatého	svatý	k2eAgMnSc4d1	svatý
<g/>
,	,	kIx,	,
Syn	syn	k1gMnSc1	syn
je	být	k5eAaImIp3nS	být
věčně	věčně	k6eAd1	věčně
plozen	plodit	k5eAaImNgInS	plodit
a	a	k8xC	a
"	"	kIx"	"
<g/>
vydechuje	vydechovat	k5eAaImIp3nS	vydechovat
<g/>
"	"	kIx"	"
Ducha	duch	k1gMnSc4	duch
Svatého	svatý	k2eAgMnSc4d1	svatý
<g/>
,	,	kIx,	,
Duch	duch	k1gMnSc1	duch
Svatý	svatý	k1gMnSc1	svatý
je	být	k5eAaImIp3nS	být
věčně	věčně	k6eAd1	věčně
"	"	kIx"	"
<g/>
vydechován	vydechován	k2eAgMnSc1d1	vydechován
<g/>
"	"	kIx"	"
Otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
Synem	syn	k1gMnSc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
společný	společný	k2eAgInSc4d1	společný
<g/>
,	,	kIx,	,
věčný	věčný	k2eAgInSc4d1	věčný
a	a	k8xC	a
naprosto	naprosto	k6eAd1	naprosto
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
úkon	úkon	k1gInSc4	úkon
vědění	vědění	k1gNnSc2	vědění
<g/>
,	,	kIx,	,
chtění	chtění	k1gNnSc2	chtění
a	a	k8xC	a
bytí	bytí	k1gNnSc2	bytí
<g/>
.	.	kIx.	.
</s>
<s>
Jednají	jednat	k5eAaImIp3nP	jednat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
jediný	jediný	k2eAgInSc1d1	jediný
princip	princip	k1gInSc1	princip
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Augustinus	Augustinus	k1gMnSc1	Augustinus
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
,	,	kIx,	,
De	De	k?	De
Trinitate	Trinitat	k1gMnSc5	Trinitat
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
W.	W.	kA	W.
Haddan	Haddan	k1gMnSc1	Haddan
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Z.	Z.	kA	Z.
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Trojiční	trojiční	k2eAgFnSc1d1	trojiční
teologie	teologie	k1gFnSc1	teologie
<g/>
:	:	kIx,	:
základ	základ	k1gInSc1	základ
teologie	teologie	k1gFnSc2	teologie
ve	v	k7c6	v
zjevení	zjevení	k1gNnSc6	zjevení
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Marek	Marek	k1gMnSc1	Marek
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
J.	J.	kA	J.
<g/>
-	-	kIx~	-
<g/>
H.	H.	kA	H.
Nicolas	Nicolas	k1gInSc1	Nicolas
<g/>
,	,	kIx,	,
Syntéza	syntéza	k1gFnSc1	syntéza
dogmatické	dogmatický	k2eAgFnSc2d1	dogmatická
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
<g/>
,	,	kIx,	,
Bůh	bůh	k1gMnSc1	bůh
v	v	k7c6	v
Trojici	trojice	k1gFnSc6	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Krystal	krystal	k1gInSc1	krystal
OP	op	k1gMnSc1	op
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
C.	C.	kA	C.
V.	V.	kA	V.
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
-	-	kIx~	-
Pravda	pravda	k1gFnSc1	pravda
dějin	dějiny	k1gFnPc2	dějiny
<g/>
:	:	kIx,	:
trojiční	trojiční	k2eAgFnSc1d1	trojiční
a	a	k8xC	a
christocentrická	christocentrický	k2eAgFnSc1d1	christocentrická
teologie	teologie	k1gFnSc1	teologie
dějin	dějiny	k1gFnPc2	dějiny
Kostelní	kostelní	k2eAgInSc1d1	kostelní
Vydří	vydří	k2eAgInSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
C.	C.	kA	C.
V.	V.	kA	V.
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Jako	jako	k8xS	jako
v	v	k7c6	v
nebi	nebe	k1gNnSc6	nebe
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
:	:	kIx,	:
náčrt	náčrt	k1gInSc1	náčrt
trinitární	trinitární	k2eAgFnSc2d1	trinitární
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Krystal	krystal	k1gInSc1	krystal
OP	op	k1gMnSc1	op
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
Řehoř	Řehoř	k1gMnSc1	Řehoř
z	z	k7c2	z
Nyssy	Nyssa	k1gFnSc2	Nyssa
<g/>
,	,	kIx,	,
Proč	proč	k6eAd1	proč
neříkáme	říkat	k5eNaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgMnPc1	tři
bohové	bůh	k1gMnPc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
a	a	k8xC	a
úvod	úvod	k1gInSc1	úvod
L.	L.	kA	L.
Karfíková	Karfíková	k1gFnSc1	Karfíková
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Oikúmené	Oikúmený	k2eAgNnSc1d1	Oikúmené
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
T.	T.	kA	T.
Špidlík	Špidlík	k1gMnSc1	Špidlík
<g/>
,	,	kIx,	,
My	my	k3xPp1nPc1	my
v	v	k7c6	v
Trojici	trojice	k1gFnSc6	trojice
<g/>
:	:	kIx,	:
krátká	krátký	k2eAgFnSc1d1	krátká
trinitární	trinitární	k2eAgFnSc1d1	trinitární
esej	esej	k1gFnSc1	esej
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Refugium	refugium	k1gNnSc1	refugium
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1	nejsvětější
Trojice	trojice	k1gFnSc1	trojice
</s>
</p>
<p>
<s>
Trojjedinost	trojjedinost	k1gFnSc1	trojjedinost
</s>
</p>
<p>
<s>
Teologie	teologie	k1gFnSc1	teologie
</s>
</p>
<p>
<s>
Christologie	christologie	k1gFnSc1	christologie
</s>
</p>
<p>
<s>
Filioque	Filioque	k6eAd1	Filioque
</s>
</p>
