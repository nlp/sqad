<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
teologii	teologie	k1gFnSc3	teologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
dogmatem	dogma	k1gNnSc7	dogma
o	o	k7c6	o
Nejsvětější	nejsvětější	k2eAgFnSc6d1	nejsvětější
Trojici	trojice	k1gFnSc6	trojice
<g/>
?	?	kIx.	?
</s>
