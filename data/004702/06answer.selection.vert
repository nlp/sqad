<s>
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
(	(	kIx(	(
[	[	kIx(	[
<g/>
roʊ	roʊ	k?	roʊ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
and	and	k?	and
Providence	providence	k1gFnSc2	providence
Plantations	Plantationsa	k1gFnPc2	Plantationsa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
regionu	region	k1gInSc2	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
