<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
krátce	krátce	k6eAd1	krátce
stal	stát	k5eAaPmAgInS	stát
nejvýdělečnejším	výdělečný	k2eAgInSc7d3	nejvýdělečnější
filmem	film	k1gInSc7	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
překonán	překonat	k5eAaPmNgInS	překonat
Titanikem	Titanic	k1gInSc7	Titanic
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
ale	ale	k9	ale
opět	opět	k6eAd1	opět
překonal	překonat	k5eAaPmAgMnS	překonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
jiný	jiný	k2eAgInSc4d1	jiný
Mijazakiho	Mijazaki	k1gMnSc2	Mijazaki
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
