<p>
<s>
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
je	být	k5eAaImIp3nS	být
každé	každý	k3xTgNnSc1	každý
celé	celý	k2eAgNnSc1d1	celé
číslo	číslo	k1gNnSc1	číslo
buď	buď	k8xC	buď
sudé	sudý	k2eAgNnSc1d1	sudé
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
liché	lichý	k2eAgFnPc1d1	lichá
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
násobkem	násobek	k1gInSc7	násobek
dvou	dva	k4xCgInPc2	dva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sudé	sudý	k2eAgNnSc1d1	sudé
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
liché	lichý	k2eAgNnSc1d1	liché
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Sudá	sudý	k2eAgNnPc1d1	sudé
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
např.	např.	kA	např.
−	−	k?	−
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
76	[number]	k4	76
<g/>
;	;	kIx,	;
lichá	lichý	k2eAgNnPc1d1	liché
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
−	−	k?	−
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
nula	nula	k1gFnSc1	nula
je	být	k5eAaImIp3nS	být
sudé	sudý	k2eAgNnSc1d1	sudé
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
rovno	roven	k2eAgNnSc1d1	rovno
nula-krát	nularát	k6eAd1	nula-krát
dvěma	dva	k4xCgFnPc7	dva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlastnost	vlastnost	k1gFnSc1	vlastnost
čísla	číslo	k1gNnSc2	číslo
být	být	k5eAaImF	být
sudým	sudý	k2eAgInSc7d1	sudý
anebo	anebo	k8xC	anebo
lichým	lichý	k2eAgInSc7d1	lichý
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
parita	parita	k1gFnSc1	parita
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
chápat	chápat	k5eAaImF	chápat
i	i	k9	i
jako	jako	k9	jako
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
zbytku	zbytek	k1gInSc2	zbytek
po	po	k7c6	po
dělení	dělení	k1gNnSc6	dělení
čísla	číslo	k1gNnSc2	číslo
dvěma	dva	k4xCgFnPc7	dva
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
parita	parita	k1gFnSc1	parita
sudého	sudý	k2eAgNnSc2d1	sudé
čísla	číslo	k1gNnSc2	číslo
je	být	k5eAaImIp3nS	být
nula	nula	k1gFnSc1	nula
<g/>
,	,	kIx,	,
parita	parita	k1gFnSc1	parita
lichého	lichý	k2eAgNnSc2d1	liché
čísla	číslo	k1gNnSc2	číslo
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgNnPc2	všecek
sudých	sudý	k2eAgNnPc2d1	sudé
čísel	číslo	k1gNnPc2	číslo
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xS	jako
</s>
</p>
<p>
<s>
Sudá	sudý	k2eAgFnSc1d1	sudá
=	=	kIx~	=
2Z	[number]	k4	2Z
=	=	kIx~	=
{	{	kIx(	{
...	...	k?	...
<g/>
,	,	kIx,	,
−	−	k?	−
<g/>
,	,	kIx,	,
−	−	k?	−
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
...	...	k?	...
}	}	kIx)	}
<g/>
Množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgNnPc2	všecek
lichých	lichý	k2eAgNnPc2d1	liché
čísel	číslo	k1gNnPc2	číslo
pak	pak	k6eAd1	pak
jako	jako	k8xC	jako
</s>
</p>
<p>
<s>
Lichá	lichý	k2eAgFnSc1d1	lichá
=	=	kIx~	=
2Z	[number]	k4	2Z
+	+	kIx~	+
1	[number]	k4	1
=	=	kIx~	=
{	{	kIx(	{
...	...	k?	...
<g/>
,	,	kIx,	,
−	−	k?	−
<g/>
,	,	kIx,	,
−	−	k?	−
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
...	...	k?	...
}	}	kIx)	}
<g/>
Je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
libovolné	libovolný	k2eAgNnSc1d1	libovolné
sudé	sudý	k2eAgNnSc1d1	sudé
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
2	[number]	k4	2
<g/>
k	k	k7c3	k
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
∈	∈	k?	∈
Z	Z	kA	Z
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
libovolné	libovolný	k2eAgNnSc1d1	libovolné
liché	lichý	k2eAgNnSc1d1	liché
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xC	jako
2	[number]	k4	2
<g/>
k	k	k7c3	k
+	+	kIx~	+
1	[number]	k4	1
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
pro	pro	k7c4	pro
k	k	k7c3	k
∈	∈	k?	∈
Z.	Z.	kA	Z.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
sudé	sudý	k2eAgNnSc1d1	sudé
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
kongruentní	kongruentní	k2eAgFnSc4d1	kongruentní
s	s	k7c7	s
nulou	nula	k1gFnSc7	nula
modulo	modout	k5eAaPmAgNnS	modout
dvěma	dva	k4xCgFnPc7	dva
<g/>
,	,	kIx,	,
liché	lichý	k2eAgFnPc1d1	lichá
právě	právě	k6eAd1	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
kongruentní	kongruentní	k2eAgFnSc4d1	kongruentní
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
modulo	modula	k1gFnSc5	modula
dvěma	dva	k4xCgFnPc7	dva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
zapsané	zapsaný	k2eAgNnSc1d1	zapsané
v	v	k7c6	v
desítkové	desítkový	k2eAgFnSc6d1	desítková
soustavě	soustava	k1gFnSc6	soustava
je	být	k5eAaImIp3nS	být
sudé	sudý	k2eAgNnSc1d1	sudé
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
sudá	sudý	k2eAgFnSc1d1	sudá
jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
číslice	číslice	k1gFnSc1	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tedy	tedy	k9	tedy
číslo	číslo	k1gNnSc1	číslo
končí	končit	k5eAaImIp3nS	končit
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
číslic	číslice	k1gFnPc2	číslice
0	[number]	k4	0
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
sudé	sudý	k2eAgNnSc1d1	sudé
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
liché	lichý	k2eAgNnSc1d1	liché
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
v	v	k7c6	v
libovolné	libovolný	k2eAgFnSc6d1	libovolná
jiné	jiný	k2eAgFnSc6d1	jiná
k-adické	kdický	k2eAgFnSc6d1	k-adický
soustavě	soustava	k1gFnSc6	soustava
se	s	k7c7	s
sudou	sudý	k2eAgFnSc7d1	sudá
bází	báze	k1gFnSc7	báze
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
binární	binární	k2eAgFnSc6d1	binární
soustavě	soustava	k1gFnSc6	soustava
končí	končit	k5eAaImIp3nS	končit
libovolné	libovolný	k2eAgNnSc1d1	libovolné
sudé	sudý	k2eAgNnSc1d1	sudé
číslo	číslo	k1gNnSc1	číslo
nulou	nula	k1gFnSc7	nula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
s	s	k7c7	s
lichou	lichý	k2eAgFnSc7d1	lichá
bází	báze	k1gFnSc7	báze
má	mít	k5eAaImIp3nS	mít
sudé	sudý	k2eAgNnSc4d1	sudé
číslo	číslo	k1gNnSc4	číslo
sudý	sudý	k2eAgInSc1d1	sudý
ciferný	ciferný	k2eAgInSc1d1	ciferný
součet	součet	k1gInSc1	součet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
sudým	sudý	k2eAgNnSc7d1	sudé
prvočíslem	prvočíslo	k1gNnSc7	prvočíslo
<g/>
,	,	kIx,	,
všechna	všechen	k3xTgNnPc1	všechen
ostatní	ostatní	k2eAgNnPc1d1	ostatní
prvočísla	prvočíslo	k1gNnPc1	prvočíslo
jsou	být	k5eAaImIp3nP	být
lichá	lichý	k2eAgNnPc1d1	liché
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Goldbachovy	Goldbachův	k2eAgFnSc2d1	Goldbachova
hypotézy	hypotéza	k1gFnSc2	hypotéza
lze	lze	k6eAd1	lze
každé	každý	k3xTgNnSc4	každý
sudé	sudý	k2eAgNnSc4d1	sudé
číslo	číslo	k1gNnSc4	číslo
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
2	[number]	k4	2
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xC	jako
součet	součet	k1gInSc1	součet
dvou	dva	k4xCgNnPc2	dva
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
byla	být	k5eAaImAgFnS	být
pomocí	pomocí	k7c2	pomocí
počítačů	počítač	k1gMnPc2	počítač
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
čísla	číslo	k1gNnPc4	číslo
až	až	k6eAd1	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
1014	[number]	k4	1014
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
dosud	dosud	k6eAd1	dosud
neexistuje	existovat	k5eNaImIp3nS	existovat
matematický	matematický	k2eAgInSc1d1	matematický
důkaz	důkaz	k1gInSc1	důkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sudá	sudý	k2eAgNnPc1d1	sudé
čísla	číslo	k1gNnPc1	číslo
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
ideál	ideál	k1gInSc1	ideál
<g/>
,	,	kIx,	,
lichá	lichý	k2eAgNnPc1d1	liché
čísla	číslo	k1gNnPc1	číslo
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Množina	množina	k1gFnSc1	množina
sudých	sudý	k2eAgNnPc2d1	sudé
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
množina	množina	k1gFnSc1	množina
lichých	lichý	k2eAgNnPc2d1	liché
čísel	číslo	k1gNnPc2	číslo
spočetně	spočetně	k6eAd1	spočetně
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
mohutnost	mohutnost	k1gFnSc4	mohutnost
jako	jako	k8xC	jako
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgNnPc2	všecek
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Aritmetické	aritmetický	k2eAgFnPc4d1	aritmetická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
některých	některý	k3yIgFnPc2	některý
základních	základní	k2eAgFnPc2d1	základní
aritmetických	aritmetický	k2eAgFnPc2d1	aritmetická
operací	operace	k1gFnPc2	operace
můžeme	moct	k5eAaImIp1nP	moct
paritu	parita	k1gFnSc4	parita
výsledku	výsledek	k1gInSc2	výsledek
poznat	poznat	k5eAaPmF	poznat
podle	podle	k7c2	podle
parity	parita	k1gFnSc2	parita
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
operandů	operand	k1gInPc2	operand
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Sčítání	sčítání	k1gNnPc2	sčítání
a	a	k8xC	a
odčítání	odčítání	k1gNnPc2	odčítání
====	====	k?	====
</s>
</p>
<p>
<s>
sudé	sudý	k2eAgFnPc1d1	sudá
±	±	k?	±
sudé	sudý	k2eAgFnPc1d1	sudá
=	=	kIx~	=
sudé	sudý	k2eAgFnPc1d1	sudá
</s>
</p>
<p>
<s>
sudé	sudý	k2eAgFnPc1d1	sudá
±	±	k?	±
liché	lichý	k2eAgFnPc1d1	lichá
=	=	kIx~	=
liché	lichý	k2eAgFnPc1d1	lichá
</s>
</p>
<p>
<s>
liché	lichý	k2eAgFnPc1d1	lichá
±	±	k?	±
sudé	sudý	k2eAgFnPc1d1	sudá
=	=	kIx~	=
liché	lichý	k2eAgFnPc1d1	lichá
</s>
</p>
<p>
<s>
liché	lichý	k2eAgFnPc1d1	lichá
±	±	k?	±
liché	lichý	k2eAgFnPc1d1	lichá
=	=	kIx~	=
sudé	sudý	k2eAgFnPc1d1	sudá
</s>
</p>
<p>
<s>
====	====	k?	====
Násobení	násobení	k1gNnSc2	násobení
====	====	k?	====
</s>
</p>
<p>
<s>
sudé	sudý	k2eAgInPc4d1	sudý
×	×	k?	×
sudé	sudý	k2eAgInPc4d1	sudý
=	=	kIx~	=
sudé	sudý	k2eAgInPc4d1	sudý
</s>
</p>
<p>
<s>
sudé	sudý	k2eAgInPc4d1	sudý
×	×	k?	×
liché	lichý	k2eAgInPc4d1	lichý
=	=	kIx~	=
sudé	sudý	k2eAgInPc4d1	sudý
</s>
</p>
<p>
<s>
liché	lichý	k2eAgInPc4d1	lichý
×	×	k?	×
sudé	sudý	k2eAgInPc4d1	sudý
=	=	kIx~	=
sudé	sudý	k2eAgInPc4d1	sudý
</s>
</p>
<p>
<s>
liché	lichý	k2eAgInPc4d1	lichý
×	×	k?	×
liché	lichý	k2eAgInPc4d1	lichý
=	=	kIx~	=
liché	lichý	k2eAgInPc4d1	lichý
</s>
</p>
<p>
<s>
====	====	k?	====
Dělení	dělení	k1gNnSc1	dělení
====	====	k?	====
</s>
</p>
<p>
<s>
Dělení	dělení	k1gNnSc2	dělení
dvou	dva	k4xCgNnPc2	dva
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
jako	jako	k8xC	jako
výsledek	výsledek	k1gInSc4	výsledek
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
není	být	k5eNaImIp3nS	být
celé	celý	k2eAgNnSc1d1	celé
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
nelze	lze	k6eNd1	lze
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c4	o
sudosti	sudost	k1gFnPc4	sudost
<g/>
/	/	kIx~	/
<g/>
lichosti	lichost	k1gFnPc4	lichost
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
však	však	k9	však
podíl	podíl	k1gInSc1	podíl
dvou	dva	k4xCgNnPc2	dva
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
také	také	k6eAd1	také
číslo	číslo	k1gNnSc4	číslo
celé	celý	k2eAgNnSc4d1	celé
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Liché	lichý	k2eAgNnSc1d1	liché
/	/	kIx~	/
liché	lichý	k2eAgNnSc1d1	liché
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
sudé	sudý	k2eAgNnSc1d1	sudé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sudé	sudý	k2eAgNnSc1d1	sudé
/	/	kIx~	/
liché	lichý	k2eAgNnSc1d1	liché
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
liché	lichý	k2eAgNnSc1d1	liché
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Liché	lichý	k2eAgNnSc1d1	liché
/	/	kIx~	/
sudé	sudý	k2eAgNnSc1d1	sudé
nikdy	nikdy	k6eAd1	nikdy
není	být	k5eNaImIp3nS	být
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
sudé	sudý	k2eAgFnSc2d1	sudá
/	/	kIx~	/
sudé	sudý	k2eAgNnSc1d1	sudé
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
konkrétních	konkrétní	k2eAgNnPc6d1	konkrétní
číslech	číslo	k1gNnPc6	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Parita	parita	k1gFnSc1	parita
</s>
</p>
<p>
<s>
Sudé	sudý	k2eAgFnPc1d1	sudá
a	a	k8xC	a
liché	lichý	k2eAgFnPc1d1	lichá
funkce	funkce	k1gFnPc1	funkce
</s>
</p>
