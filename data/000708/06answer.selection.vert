<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Daevid	Daevida	k1gFnPc2	Daevida
Allen	Allen	k1gMnSc1	Allen
Trio	trio	k1gNnSc1	trio
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gNnSc2	on
působili	působit	k5eAaImAgMnP	působit
ještě	ještě	k6eAd1	ještě
Robert	Robert	k1gMnSc1	Robert
Wyatt	Wyatt	k1gMnSc1	Wyatt
a	a	k8xC	a
Daevid	Daevida	k1gFnPc2	Daevida
Allen	Allen	k1gMnSc1	Allen
<g/>
.	.	kIx.	.
</s>
