<s>
Hugh	Hugh	k1gMnSc1	Hugh
Hopper	Hopper	k1gMnSc1	Hopper
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Canterbury	Canterbura	k1gFnSc2	Canterbura
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
baskytarista	baskytarista	k1gMnSc1	baskytarista
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Daevid	Daevida	k1gFnPc2	Daevida
Allen	Allen	k1gMnSc1	Allen
Trio	trio	k1gNnSc1	trio
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gNnSc2	on
působili	působit	k5eAaImAgMnP	působit
ještě	ještě	k6eAd1	ještě
Robert	Robert	k1gMnSc1	Robert
Wyatt	Wyatt	k1gMnSc1	Wyatt
a	a	k8xC	a
Daevid	Daevida	k1gFnPc2	Daevida
Allen	Allen	k1gMnSc1	Allen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Wyattem	Wyatt	k1gMnSc7	Wyatt
<g/>
,	,	kIx,	,
Kevinem	Kevin	k1gMnSc7	Kevin
Ayersem	Ayers	k1gMnSc7	Ayers
<g/>
,	,	kIx,	,
Richardem	Richard	k1gMnSc7	Richard
Sinclairem	Sinclair	k1gMnSc7	Sinclair
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Brianem	Brian	k1gMnSc7	Brian
Hopperem	Hopper	k1gMnSc7	Hopper
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gMnSc1	The
Wilde	Wild	k1gInSc5	Wild
Flowers	Flowers	k1gInSc1	Flowers
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
pak	pak	k6eAd1	pak
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
obnovené	obnovený	k2eAgFnSc3d1	obnovená
skupině	skupina	k1gFnSc3	skupina
Soft	Soft	k?	Soft
Machine	Machin	k1gMnSc5	Machin
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
působil	působit	k5eAaImAgInS	působit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
skupinami	skupina	k1gFnPc7	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Gilgamesh	Gilgamesh	k1gMnSc1	Gilgamesh
a	a	k8xC	a
Soft	Soft	k?	Soft
Heap	Heap	k1gInSc1	Heap
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
sólovém	sólový	k2eAgNnSc6d1	sólové
albu	album	k1gNnSc6	album
Kevina	Kevin	k2eAgFnSc1d1	Kevina
Ayerse	Ayerse	k1gFnSc1	Ayerse
s	s	k7c7	s
názvem	název	k1gInSc7	název
Joy	Joy	k1gFnSc4	Joy
of	of	k?	of
a	a	k8xC	a
Toy	Toy	k1gFnSc2	Toy
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
sólovém	sólový	k2eAgNnSc6d1	sólové
albu	album	k1gNnSc6	album
bývalého	bývalý	k2eAgMnSc2d1	bývalý
člena	člen	k1gMnSc2	člen
skupiny	skupina	k1gFnSc2	skupina
Pink	pink	k6eAd1	pink
Floyd	Floyd	k1gMnSc1	Floyd
Syda	Syda	k1gMnSc1	Syda
Barretta	Barretta	k1gMnSc1	Barretta
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
The	The	k1gMnPc2	The
Madcap	Madcap	k1gInSc4	Madcap
Laughs	Laughsa	k1gFnPc2	Laughsa
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgMnPc7d1	další
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgInPc4	který
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
Lindsay	Lindsa	k2eAgInPc1d1	Lindsa
Cooper	Cooper	k1gInSc1	Cooper
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc1	Chris
Cutler	Cutler	k1gMnSc1	Cutler
nebo	nebo	k8xC	nebo
Pip	pipa	k1gFnPc2	pipa
Pyle	pyl	k1gInSc5	pyl
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vydal	vydat	k5eAaPmAgInS	vydat
několik	několik	k4yIc4	několik
alb	alba	k1gFnPc2	alba
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2008	[number]	k4	2008
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
leukemie	leukemie	k1gFnSc1	leukemie
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
čtyřiašedesáti	čtyřiašedesát	k4xCc2	čtyřiašedesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Christine	Christin	k1gInSc5	Christin
<g/>
.	.	kIx.	.
</s>
