<s>
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
této	tento	k3xDgFnSc2	tento
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bývala	bývat	k5eAaImAgFnS	bývat
pravidelně	pravidelně	k6eAd1	pravidelně
hrána	hrát	k5eAaImNgFnS	hrát
na	na	k7c6	na
letní	letní	k2eAgFnSc6d1	letní
scéně	scéna	k1gFnSc6	scéna
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
hradu	hrad	k1gInSc2	hrad
Karlštejna	Karlštejn	k1gInSc2	Karlštejn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
často	často	k6eAd1	často
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
herci	herec	k1gMnPc1	herec
z	z	k7c2	z
pražského	pražský	k2eAgNnSc2d1	Pražské
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
provozována	provozovat	k5eAaImNgFnS	provozovat
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
natočena	natočit	k5eAaBmNgFnS	natočit
i	i	k9	i
v	v	k7c6	v
Československém	československý	k2eAgInSc6d1	československý
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
i	i	k9	i
na	na	k7c6	na
gramofonových	gramofonový	k2eAgFnPc6d1	gramofonová
deskách	deska	k1gFnPc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Režii	režie	k1gFnSc4	režie
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
hry	hra	k1gFnSc2	hra
provedl	provést	k5eAaPmAgMnS	provést
Jiří	Jiří	k1gMnSc1	Jiří
Horčička	Horčička	k1gMnSc1	Horčička
<g/>
.	.	kIx.	.
</s>
<s>
Císaře	Císař	k1gMnSc4	Císař
a	a	k8xC	a
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc4	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zde	zde	k6eAd1	zde
hrál	hrát	k5eAaImAgMnS	hrát
Otomar	Otomar	k1gMnSc1	Otomar
Korbelář	korbelář	k1gMnSc1	korbelář
<g/>
,	,	kIx,	,
Alžbětu	Alžběta	k1gFnSc4	Alžběta
jeho	jeho	k3xOp3gFnSc4	jeho
manželku	manželka	k1gFnSc4	manželka
Jiřina	Jiřina	k1gFnSc1	Jiřina
Petrovická	Petrovický	k2eAgFnSc1d1	Petrovická
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnSc1d2	novější
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
krále	král	k1gMnSc2	král
hrál	hrát	k5eAaImAgMnS	hrát
Luděk	Luděk	k1gMnSc1	Luděk
Munzar	Munzar	k1gMnSc1	Munzar
<g/>
,	,	kIx,	,
královnu	královna	k1gFnSc4	královna
a	a	k8xC	a
císařovnu	císařovna	k1gFnSc4	císařovna
Alžbětu	Alžběta	k1gFnSc4	Alžběta
pak	pak	k6eAd1	pak
Jana	Jan	k1gMnSc4	Jan
Drbohlavová	Drbohlavový	k2eAgFnSc1d1	Drbohlavová
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
námětu	námět	k1gInSc2	námět
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
filmový	filmový	k2eAgInSc1d1	filmový
muzikál	muzikál	k1gInSc1	muzikál
režiséra	režisér	k1gMnSc2	režisér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Podskalského	podskalský	k2eAgMnSc2d1	podskalský
s	s	k7c7	s
písněmi	píseň	k1gFnPc7	píseň
Karla	Karel	k1gMnSc2	Karel
Svobody	Svoboda	k1gMnSc2	Svoboda
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc2	Jiří
Štaidla	Štaidlo	k1gNnSc2	Štaidlo
<g/>
.	.	kIx.	.
</s>
