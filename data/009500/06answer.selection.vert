<s>
První	první	k4xOgFnSc1	první
člověkem	člověk	k1gMnSc7	člověk
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
Měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
automatická	automatický	k2eAgFnSc1d1	automatická
sovětská	sovětský	k2eAgFnSc1d1	sovětská
sonda	sonda	k1gFnSc1	sonda
Luna	luna	k1gFnSc1	luna
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1959	[number]	k4	1959
ve	v	k7c6	v
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
24	[number]	k4	24
Z.	Z.	kA	Z.
</s>
