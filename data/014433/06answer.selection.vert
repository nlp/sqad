<s>
Útvar	útvar	k1gInSc1
odhalování	odhalování	k1gNnSc2
korupce	korupce	k1gFnSc2
a	a	k8xC
finanční	finanční	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
(	(	kIx(
<g/>
ÚOKFK	ÚOKFK	kA
<g/>
)	)	kIx)
či	či	k8xC
Útvar	útvar	k1gInSc1
odhalování	odhalování	k1gNnSc2
korupce	korupce	k1gFnSc2
a	a	k8xC
finanční	finanční	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
služby	služba	k1gFnSc2
kriminální	kriminální	k2eAgFnSc2d1
policie	policie	k1gFnSc2
a	a	k8xC
vyšetřování	vyšetřování	k1gNnSc2
(	(	kIx(
<g/>
ÚOKFK	ÚOKFK	kA
SKPV	SKPV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
též	též	k9
jako	jako	k9
protikorupční	protikorupční	k2eAgInSc1d1
útvar	útvar	k1gInSc1
či	či	k8xC
protikorupční	protikorupční	k2eAgFnSc1d1
policie	policie	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgInS
útvar	útvar	k1gInSc1
Policie	policie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
s	s	k7c7
celorepublikovou	celorepublikový	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
bojem	boj	k1gInSc7
s	s	k7c7
nejzávažnější	závažný	k2eAgFnSc7d3
hospodářskou	hospodářský	k2eAgFnSc7d1
kriminalitou	kriminalita	k1gFnSc7
a	a	k8xC
korupcí	korupce	k1gFnSc7
<g/>
.	.	kIx.
</s>