<s>
Útvar	útvar	k1gInSc1
odhalování	odhalování	k1gNnSc2
korupce	korupce	k1gFnSc2
a	a	k8xC
finanční	finanční	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
</s>
<s>
Znak	znak	k1gInSc1
ÚOKFK	ÚOKFK	kA
</s>
<s>
Útvar	útvar	k1gInSc1
odhalování	odhalování	k1gNnSc2
korupce	korupce	k1gFnSc2
a	a	k8xC
finanční	finanční	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
(	(	kIx(
<g/>
ÚOKFK	ÚOKFK	kA
<g/>
)	)	kIx)
či	či	k8xC
Útvar	útvar	k1gInSc1
odhalování	odhalování	k1gNnSc2
korupce	korupce	k1gFnSc2
a	a	k8xC
finanční	finanční	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
služby	služba	k1gFnSc2
kriminální	kriminální	k2eAgFnSc2d1
policie	policie	k1gFnSc2
a	a	k8xC
vyšetřování	vyšetřování	k1gNnSc2
(	(	kIx(
<g/>
ÚOKFK	ÚOKFK	kA
SKPV	SKPV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
též	též	k9
jako	jako	k9
protikorupční	protikorupční	k2eAgInSc1d1
útvar	útvar	k1gInSc1
či	či	k8xC
protikorupční	protikorupční	k2eAgFnSc1d1
policie	policie	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgInS
útvar	útvar	k1gInSc1
Policie	policie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
s	s	k7c7
celorepublikovou	celorepublikový	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
bojem	boj	k1gInSc7
s	s	k7c7
nejzávažnější	závažný	k2eAgFnSc7d3
hospodářskou	hospodářský	k2eAgFnSc7d1
kriminalitou	kriminalita	k1gFnSc7
a	a	k8xC
korupcí	korupce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Činnost	činnost	k1gFnSc1
</s>
<s>
Těžiště	těžiště	k1gNnSc1
činnosti	činnost	k1gFnSc2
útvaru	útvar	k1gInSc2
leželo	ležet	k5eAaImAgNnS
v	v	k7c6
odhalování	odhalování	k1gNnSc6
<g/>
,	,	kIx,
prověřování	prověřování	k1gNnSc6
a	a	k8xC
vyšetřování	vyšetřování	k1gNnSc6
nejzávažnější	závažný	k2eAgFnSc2d3
hospodářské	hospodářský	k2eAgFnSc2d1
trestné	trestný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
vykonává	vykonávat	k5eAaImIp3nS
dozor	dozor	k1gInSc1
nejčastěji	často	k6eAd3
vrchní	vrchní	k2eAgNnSc4d1
státní	státní	k2eAgNnSc4d1
zastupitelství	zastupitelství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
zpracovávané	zpracovávaný	k2eAgFnSc2d1
problematiky	problematika	k1gFnSc2
se	se	k3xPyFc4
týkala	týkat	k5eAaImAgFnS
daňové	daňový	k2eAgFnPc4d1
trestné	trestný	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
<g/>
,	,	kIx,
veřejných	veřejný	k2eAgFnPc2d1
zakázek	zakázka	k1gFnPc2
a	a	k8xC
nejzávažnějších	závažný	k2eAgFnPc2d3
forem	forma	k1gFnPc2
korupčního	korupční	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
praní	praní	k1gNnSc1
špinavých	špinavý	k2eAgInPc2d1
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ÚOKFK	ÚOKFK	kA
zároveň	zároveň	k6eAd1
plnil	plnit	k5eAaImAgInS
za	za	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
funkci	funkce	k1gFnSc4
úřadu	úřad	k1gInSc2
pro	pro	k7c4
dohledávání	dohledávání	k1gNnSc4
majetku	majetek	k1gInSc2
pocházejícího	pocházející	k2eAgInSc2d1
z	z	k7c2
trestné	trestný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
(	(	kIx(
<g/>
Asset	Asset	k1gInSc1
Recovery	Recovera	k1gFnSc2
Office	Office	kA
<g/>
,	,	kIx,
tzv.	tzv.	kA
ARO	ara	k1gMnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
ÚOKFK	ÚOKFK	kA
byl	být	k5eAaImAgInS
v	v	k7c6
rámci	rámec	k1gInSc6
Policie	policie	k1gFnSc2
ČR	ČR	kA
metodickým	metodický	k2eAgNnSc7d1
pracovištěm	pracoviště	k1gNnSc7
pro	pro	k7c4
boj	boj	k1gInSc4
s	s	k7c7
daňovou	daňový	k2eAgFnSc7d1
kriminalitou	kriminalita	k1gFnSc7
<g/>
,	,	kIx,
kriminalitou	kriminalita	k1gFnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
veřejných	veřejný	k2eAgFnPc2d1
zakázek	zakázka	k1gFnPc2
<g/>
,	,	kIx,
korupcí	korupce	k1gFnPc2
a	a	k8xC
zajišťování	zajišťování	k1gNnSc6
výnosů	výnos	k1gInPc2
z	z	k7c2
trestné	trestný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
odborů	odbor	k1gInPc2
umístěných	umístěný	k2eAgInPc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
měl	mít	k5eAaImAgInS
útvar	útvar	k1gInSc1
expozitury	expozitura	k1gFnSc2
v	v	k7c6
šesti	šest	k4xCc6
krajských	krajský	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Útvar	útvar	k1gInSc1
odhalování	odhalování	k1gNnSc2
korupce	korupce	k1gFnSc2
a	a	k8xC
finanční	finanční	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
vytvořil	vytvořit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
Generálním	generální	k2eAgNnSc7d1
finančním	finanční	k2eAgNnSc7d1
ředitelstvím	ředitelství	k1gNnSc7
a	a	k8xC
Generálním	generální	k2eAgNnSc7d1
ředitelstvím	ředitelství	k1gNnSc7
cel	clo	k1gNnPc2
meziresortní	meziresortní	k2eAgInSc1d1
tým	tým	k1gInSc1
Kobra	kobra	k1gFnSc1
(	(	kIx(
<g/>
Daňová	daňový	k2eAgFnSc1d1
Kobra	kobra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
vysoce	vysoce	k6eAd1
efektivním	efektivní	k2eAgInSc7d1
nástrojem	nástroj	k1gInSc7
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
daňové	daňový	k2eAgFnSc3d1
kriminalitě	kriminalita	k1gFnSc3
(	(	kIx(
<g/>
resp.	resp.	kA
zajišťování	zajišťování	k1gNnSc1
výnosů	výnos	k1gInPc2
z	z	k7c2
trestné	trestný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
metodickým	metodický	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
ÚOKFK	ÚOKFK	kA
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
zřízeny	zřízen	k2eAgInPc1d1
týmy	tým	k1gInPc1
Kobra	kobra	k1gFnSc1
i	i	k9
v	v	k7c6
rámci	rámec	k1gInSc6
jednotlivých	jednotlivý	k2eAgNnPc2d1
krajských	krajský	k2eAgNnPc2d1
ředitelství	ředitelství	k1gNnPc2
policie	policie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
získal	získat	k5eAaPmAgInS
ÚOKFK	ÚOKFK	kA
Národní	národní	k2eAgInSc1d1
cenu	cena	k1gFnSc4
kvality	kvalita	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
prestižní	prestižní	k2eAgNnSc1d1
ocenění	ocenění	k1gNnSc1
Excelentní	excelentní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
veřejného	veřejný	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
dle	dle	k7c2
modelu	model	k1gInSc2
EFQM	EFQM	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
2012	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
a	a	k8xC
2014	#num#	k4
byl	být	k5eAaImAgInS
vyhlášen	vyhlásit	k5eAaPmNgInS
„	„	k?
<g/>
Policistou	policista	k1gMnSc7
roku	rok	k1gInSc2
<g/>
“	“	k?
příslušník	příslušník	k1gMnSc1
ÚOKFK	ÚOKFK	kA
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
Policie	policie	k1gFnSc2
ČR	ČR	kA
samostatná	samostatný	k2eAgFnSc1d1
služba	služba	k1gFnSc1
s	s	k7c7
názvem	název	k1gInSc7
Služba	služba	k1gFnSc1
na	na	k7c4
ochranu	ochrana	k1gFnSc4
ekonomických	ekonomický	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
(	(	kIx(
<g/>
SOEZ	SOEZ	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
ředitelem	ředitel	k1gMnSc7
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
JUDr.	JUDr.	kA
Jan	Jan	k1gMnSc1
Šula	Šula	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
1992	#num#	k4
je	být	k5eAaImIp3nS
faktickým	faktický	k2eAgInSc7d1
začátkem	začátek	k1gInSc7
práce	práce	k1gFnSc2
SOEZ	SOEZ	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
fungoval	fungovat	k5eAaImAgInS
v	v	k7c6
podstatě	podstata	k1gFnSc6
jako	jako	k8xS,k8xC
zpravodajská	zpravodajský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravodajskou	zpravodajský	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
na	na	k7c6
úseku	úsek	k1gInSc6
ochrany	ochrana	k1gFnSc2
ekonomických	ekonomický	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
státu	stát	k1gInSc2
však	však	k9
převzala	převzít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
BIS	BIS	kA
a	a	k8xC
ze	z	k7c2
SOEZ	SOEZ	kA
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Služba	služba	k1gFnSc1
policie	policie	k1gFnSc2
pro	pro	k7c4
odhalování	odhalování	k1gNnSc4
korupce	korupce	k1gFnSc2
a	a	k8xC
hospodářské	hospodářský	k2eAgFnSc2d1
trestné	trestný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
(	(	kIx(
<g/>
SPOK	SPOK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
Služba	služba	k1gFnSc1
policie	policie	k1gFnSc2
pro	pro	k7c4
odhalování	odhalování	k1gNnSc4
korupce	korupce	k1gFnSc2
a	a	k8xC
závažné	závažný	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
trestné	trestný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
(	(	kIx(
<g/>
SPOK	SPOK	kA
ZHTČ	ZHTČ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
služba	služba	k1gFnSc1
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
zabývat	zabývat	k5eAaImF
nejzávažnějšími	závažný	k2eAgInPc7d3
případy	případ	k1gInPc7
na	na	k7c6
úseku	úsek	k1gInSc6
ekonomické	ekonomický	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
a	a	k8xC
jejím	její	k3xOp3gInSc7
logickým	logický	k2eAgInSc7d1
protějškem	protějšek	k1gInSc7
na	na	k7c6
úseku	úsek	k1gInSc6
vyšetřování	vyšetřování	k1gNnSc2
byly	být	k5eAaImAgFnP
až	až	k9
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2001	#num#	k4
především	především	k9
Úřad	úřad	k1gInSc1
vyšetřování	vyšetřování	k1gNnSc2
pro	pro	k7c4
ČR	ČR	kA
a	a	k8xC
krajské	krajský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
vyšetřování	vyšetřování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
byly	být	k5eAaImAgFnP
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2002	#num#	k4
úřady	úřad	k1gInPc1
vyšetřování	vyšetřování	k1gNnSc2
zrušeny	zrušit	k5eAaPmNgInP
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
Služba	služba	k1gFnSc1
kriminální	kriminální	k2eAgFnSc2d1
policie	policie	k1gFnSc2
a	a	k8xC
vyšetřování	vyšetřování	k1gNnSc2
(	(	kIx(
<g/>
SKPV	SKPV	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
byly	být	k5eAaImAgInP
zřízeny	zřízen	k2eAgInPc1d1
nové	nový	k2eAgInPc1d1
specializované	specializovaný	k2eAgInPc1d1
útvary	útvar	k1gInPc1
policie	policie	k1gFnSc2
a	a	k8xC
mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
také	také	k9
Útvar	útvar	k1gInSc4
pro	pro	k7c4
odhalování	odhalování	k1gNnSc4
korupce	korupce	k1gFnSc2
a	a	k8xC
závažné	závažný	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
trestné	trestný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
Služby	služba	k1gFnSc2
kriminální	kriminální	k2eAgFnSc2d1
policie	policie	k1gFnSc2
a	a	k8xC
vyšetřování	vyšetřování	k1gNnSc2
(	(	kIx(
<g/>
UOK	UOK	kA
ZHTČ	ZHTČ	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
nástupce	nástupce	k1gMnSc4
bývalého	bývalý	k2eAgInSc2d1
SPOK	SPOK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšetřovatelé	vyšetřovatel	k1gMnPc1
z	z	k7c2
partnerského	partnerský	k2eAgInSc2d1
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
vyšetřování	vyšetřování	k1gNnSc4
ČR	ČR	kA
<g/>
,	,	kIx,
Odboru	odbor	k1gInSc2
pro	pro	k7c4
vyšetřování	vyšetřování	k1gNnSc4
korupce	korupce	k1gFnSc2
a	a	k8xC
závažné	závažný	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
trestné	trestný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
odbor	odbor	k1gInSc1
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
vyčleněni	vyčleněn	k2eAgMnPc1d1
do	do	k7c2
samostatného	samostatný	k2eAgInSc2d1
útvaru	útvar	k1gInSc2
nazvaného	nazvaný	k2eAgInSc2d1
Úřad	úřad	k1gInSc1
finanční	finanční	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
státu	stát	k1gInSc2
(	(	kIx(
<g/>
UFKOS	UFKOS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2003	#num#	k4
se	se	k3xPyFc4
UOK	UOK	kA
ZHTČ	ZHTČ	kA
sloučil	sloučit	k5eAaPmAgInS
s	s	k7c7
Úřadem	úřad	k1gInSc7
finanční	finanční	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
státu	stát	k1gInSc2
(	(	kIx(
<g/>
UFKOS	UFKOS	kA
<g/>
)	)	kIx)
a	a	k8xC
tak	tak	k6eAd1
vznikl	vzniknout	k5eAaPmAgInS
současný	současný	k2eAgInSc1d1
Útvar	útvar	k1gInSc1
odhalování	odhalování	k1gNnSc2
korupce	korupce	k1gFnSc2
a	a	k8xC
finanční	finanční	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
po	po	k7c6
stránce	stránka	k1gFnSc6
vyšetřování	vyšetřování	k1gNnSc2
soběstačným	soběstačný	k2eAgMnPc3d1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2004	#num#	k4
byl	být	k5eAaImAgInS
z	z	k7c2
útvaru	útvar	k1gInSc2
vydělen	vydělen	k2eAgInSc1d1
útvar	útvar	k1gInSc1
specializovaný	specializovaný	k2eAgInSc1d1
na	na	k7c4
závažnou	závažný	k2eAgFnSc4d1
daňovou	daňový	k2eAgFnSc4d1
kriminalitu	kriminalita	k1gFnSc4
a	a	k8xC
zajišťování	zajišťování	k1gNnSc4
výnosů	výnos	k1gInPc2
z	z	k7c2
trestné	trestný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
„	„	k?
<g/>
finanční	finanční	k2eAgFnSc2d1
policie	policie	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
FIPO	FIPO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
formálně	formálně	k6eAd1
Útvar	útvar	k1gInSc1
odhalování	odhalování	k1gNnSc2
nelegálních	legální	k2eNgInPc2d1
výnosů	výnos	k1gInPc2
a	a	k8xC
daňové	daňový	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
(	(	kIx(
<g/>
ÚONVDK	ÚONVDK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
personálně	personálně	k6eAd1
doplněný	doplněný	k2eAgInSc1d1
o	o	k7c4
příslušníky	příslušník	k1gMnPc4
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
poněkud	poněkud	k6eAd1
krátký	krátký	k2eAgInSc4d1
život	život	k1gInSc4
ukončil	ukončit	k5eAaPmAgMnS
ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
Ivan	Ivan	k1gMnSc1
Langer	Langer	k1gMnSc1
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc3
2006	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
ÚONVDK	ÚONVDK	kA
opět	opět	k6eAd1
fakticky	fakticky	k6eAd1
včlenil	včlenit	k5eAaPmAgMnS
do	do	k7c2
struktury	struktura	k1gFnSc2
ÚOKFK	ÚOKFK	kA
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
část	část	k1gFnSc1
policistů	policista	k1gMnPc2
odešla	odejít	k5eAaPmAgFnS
zpět	zpět	k6eAd1
na	na	k7c4
krajská	krajský	k2eAgNnPc4d1
policejní	policejní	k2eAgNnPc4d1
ředitelství	ředitelství	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
srpnu	srpen	k1gInSc3
2016	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
sloučení	sloučení	k1gNnSc3
ÚOKFK	ÚOKFK	kA
(	(	kIx(
<g/>
o	o	k7c4
412	#num#	k4
zaměstnancích	zaměstnanec	k1gMnPc6
<g/>
)	)	kIx)
s	s	k7c7
ÚOOZ	ÚOOZ	kA
(	(	kIx(
<g/>
o	o	k7c4
474	#num#	k4
zaměstnancích	zaměstnanec	k1gMnPc6
<g/>
)	)	kIx)
v	v	k7c4
jeden	jeden	k4xCgInSc4
celek	celek	k1gInSc4
(	(	kIx(
<g/>
NCOZ	NCOZ	kA
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
900	#num#	k4
zaměstnanci	zaměstnanec	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosavadní	dosavadní	k2eAgMnSc1d1
šéf	šéf	k1gMnSc1
ÚOKFK	ÚOKFK	kA
Jaroslav	Jaroslav	k1gMnSc1
Vild	Vild	k1gMnSc1
se	se	k3xPyFc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2016	#num#	k4
stal	stát	k5eAaPmAgInS
náměstkem	náměstek	k1gMnSc7
tohoto	tento	k3xDgInSc2
útvaru	útvar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
V	v	k7c6
čele	čelo	k1gNnSc6
tohoto	tento	k3xDgInSc2
útvaru	útvar	k1gInSc2
stál	stát	k5eAaImAgMnS
ředitel	ředitel	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2003	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
plk.	plk.	kA
Jiří	Jiří	k1gMnSc1
Pálka	Pálka	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
plk.	plk.	kA
Milan	Milan	k1gMnSc1
Šiška	Šiška	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
plk.	plk.	kA
Miloslav	Miloslav	k1gMnSc1
Brych	Brych	k1gMnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
plk.	plk.	kA
Renata	Renata	k1gFnSc1
Strnadová	Strnadová	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
plk.	plk.	kA
Jiří	Jiří	k1gMnSc1
Novák	Novák	k1gMnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
plk.	plk.	kA
Libor	Libor	k1gMnSc1
Vrba	Vrba	k1gMnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
plk.	plk.	kA
Tomáš	Tomáš	k1gMnSc1
Martinec	Martinec	k1gMnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
plk.	plk.	kA
Milan	Milan	k1gMnSc1
Komárek	Komárek	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
plk.	plk.	kA
Jaroslav	Jaroslav	k1gMnSc1
Vild	Vild	k1gMnSc1
</s>
<s>
Ředitel	ředitel	k1gMnSc1
spadal	spadat	k5eAaPmAgMnS,k5eAaImAgMnS
pod	pod	k7c4
náměstka	náměstek	k1gMnSc4
policejního	policejní	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
pro	pro	k7c4
SKPV	SKPV	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jiný	jiný	k2eAgInSc4d1
hlas	hlas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šéf	šéf	k1gMnSc1
protikorupční	protikorupční	k2eAgFnSc2d1
policie	policie	k1gFnSc2
s	s	k7c7
reorganizací	reorganizace	k1gFnSc7
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Šlachty	šlachta	k1gFnSc2
souhlasí	souhlasit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-06-17	2016-06-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.danovakobra.cz	http://www.danovakobra.cz	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
centrála	centrála	k1gFnSc1
proti	proti	k7c3
organizovanému	organizovaný	k2eAgInSc3d1
zločinu	zločin	k1gInSc3
</s>
<s>
Útvar	útvar	k1gInSc1
pro	pro	k7c4
odhalování	odhalování	k1gNnSc4
organizovaného	organizovaný	k2eAgInSc2d1
zločinu	zločin	k1gInSc2
</s>
<s>
Útvar	útvar	k1gInSc1
odhalování	odhalování	k1gNnSc2
nelegálních	legální	k2eNgInPc2d1
výnosů	výnos	k1gInPc2
a	a	k8xC
daňové	daňový	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
útvarů	útvar	k1gInPc2
s	s	k7c7
celostátní	celostátní	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
Policie	policie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
</s>
