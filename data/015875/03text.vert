<s>
Cory	Cora	k1gFnPc1
Booker	Bookra	k1gFnPc2
</s>
<s>
Cory	Cora	k1gFnPc1
Booker	Bookra	k1gFnPc2
</s>
<s>
Senátor	senátor	k1gMnSc1
Senátu	senát	k1gInSc2
USAza	USAza	k1gFnSc1
stát	stát	k5eAaPmF,k5eAaImF
New	New	k1gFnSc4
Jersey	Jersea	k1gFnSc2
Úřadující	úřadující	k2eAgFnSc2d1
</s>
<s>
Ve	v	k7c4
funkci	funkce	k1gFnSc4
od	od	k7c2
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Jeffrey	Jeffrey	k1gInPc1
Chiesa	Chiesa	k1gFnSc1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1969	#num#	k4
(	(	kIx(
<g/>
51	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Afroameričané	Afroameričan	k1gMnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
Stanfordova	Stanfordův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
Stanfordova	Stanfordův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
The	The	k1gFnSc1
Queen	Quena	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
College	College	k1gNnSc7
(	(	kIx(
<g/>
do	do	k7c2
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
Yale	Yal	k1gInSc2
Law	Law	k1gFnSc2
School	Schoola	k1gFnPc2
(	(	kIx(
<g/>
do	do	k7c2
1997	#num#	k4
<g/>
)	)	kIx)
Profese	profese	k1gFnSc1
</s>
<s>
politik	politik	k1gMnSc1
a	a	k8xC
advokát	advokát	k1gMnSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
Baptisté	baptista	k1gMnPc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Rhodes	Rhodes	k1gMnSc1
Scholarship	Scholarship	k1gMnSc1
Podpis	podpis	k1gInSc4
</s>
<s>
Webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
www.booker.senate.gov	www.booker.senate.gov	k1gInSc1
Commons	Commonsa	k1gFnPc2
</s>
<s>
Cory	Cora	k1gFnPc1
Booker	Bookra	k1gFnPc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cory	Cor	k2eAgInPc1d1
Anthony	Anthony	k1gMnSc1
Booker	Booker	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1969	#num#	k4
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
senátor	senátor	k1gMnSc1
za	za	k7c4
New	New	k1gFnSc4
Jersey	Jersea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
na	na	k7c6
Stanfordově	Stanfordův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
následně	následně	k6eAd1
díky	díky	k7c3
Rhodesovu	Rhodesův	k2eAgNnSc3d1
stipendiu	stipendium	k1gNnSc3
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Anglie	Anglie	k1gFnSc2
za	za	k7c7
studiem	studio	k1gNnSc7
na	na	k7c6
Oxfordské	oxfordský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
docházel	docházet	k5eAaImAgInS
na	na	k7c4
právní	právní	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Yaleovy	Yaleův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Politická	politický	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
neúspěšně	úspěšně	k6eNd1
kandidoval	kandidovat	k5eAaImAgMnS
na	na	k7c4
post	post	k1gInSc4
starosty	starosta	k1gMnSc2
newjersejského	wjersejský	k2eNgNnSc2d1
města	město	k1gNnSc2
Newark	Newark	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
byl	být	k5eAaImAgInS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
druhou	druhý	k4xOgFnSc7
kandidaturou	kandidatura	k1gFnSc7
úspěšný	úspěšný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
října	říjen	k1gInSc2
2013	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
funkci	funkce	k1gFnSc4
starosty	starosta	k1gMnSc2
opustil	opustit	k5eAaPmAgMnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
senátorem	senátor	k1gMnSc7
za	za	k7c4
New	New	k1gFnSc4
Jersey	Jersea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvního	první	k4xOgInSc2
února	únor	k1gInSc2
2019	#num#	k4
oznámil	oznámit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
ucházet	ucházet	k5eAaImF
o	o	k7c4
nominaci	nominace	k1gFnSc4
Demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
na	na	k7c4
kandidáta	kandidát	k1gMnSc4
pro	pro	k7c4
volbu	volba	k1gFnSc4
prezidenta	prezident	k1gMnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
nedostatku	nedostatek	k1gInSc3
finančních	finanční	k2eAgInPc2d1
darů	dar	k1gInPc2
v	v	k7c6
pozdní	pozdní	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
kampaně	kampaň	k1gFnSc2
a	a	k8xC
nízkým	nízký	k2eAgInPc3d1
výsledkům	výsledek	k1gInPc3
ve	v	k7c6
volebních	volební	k2eAgInPc6d1
průzkumech	průzkum	k1gInPc6
se	se	k3xPyFc4
nekvalifikoval	kvalifikovat	k5eNaBmAgInS
na	na	k7c4
poslední	poslední	k2eAgFnPc4d1
dvě	dva	k4xCgFnPc4
oficiální	oficiální	k2eAgFnPc4d1
televizní	televizní	k2eAgFnPc4d1
debaty	debata	k1gFnPc4
a	a	k8xC
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
<g/>
,	,	kIx,
necelé	celý	k2eNgInPc4d1
tři	tři	k4xCgInPc4
týdny	týden	k1gInPc4
před	před	k7c7
primárkami	primárky	k1gFnPc7
v	v	k7c6
Iowě	Iowa	k1gFnSc6
<g/>
,	,	kIx,
svou	svůj	k3xOyFgFnSc4
kandidaturu	kandidatura	k1gFnSc4
stáhl	stáhnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
listopadu	listopad	k1gInSc6
2020	#num#	k4
se	se	k3xPyFc4
proto	proto	k8xC
uchází	ucházet	k5eAaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c4
znovuzvolení	znovuzvolení	k1gNnSc4
do	do	k7c2
pozice	pozice	k1gFnSc2
senátora	senátor	k1gMnSc2
státu	stát	k1gInSc2
New	New	k1gMnSc2
Jersey	Jersea	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Počínaje	počínaje	k7c7
rokem	rok	k1gInSc7
1992	#num#	k4
byl	být	k5eAaImAgInS
vegetariánem	vegetarián	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
po	po	k7c4
dobu	doba	k1gFnSc4
jednoho	jeden	k4xCgInSc2
roku	rok	k1gInSc2
bude	být	k5eAaImBp3nS
stravovat	stravovat	k5eAaImF
jako	jako	k9
vegan	vegan	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
JAFFE	JAFFE	kA
<g/>
,	,	kIx,
Alexandra	Alexandra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Booker	Booker	k1gInSc1
ends	ends	k6eAd1
presidential	presidentiat	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
bid	bid	k?
after	after	k1gInSc1
polling	polling	k1gInSc1
<g/>
,	,	kIx,
money	money	k1gInPc1
struggles	strugglesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Associated	Associated	k1gInSc1
Press	Press	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-01-14	2020-01-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AUSTIN	AUSTIN	kA
<g/>
,	,	kIx,
Shelbi	Shelbi	k1gNnSc1
<g/>
.	.	kIx.
10	#num#	k4
Things	Thingsa	k1gFnPc2
You	You	k1gMnSc1
Didn	Didn	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Know	Know	k1gMnSc4
About	About	k1gMnSc1
Cory	Cora	k1gFnSc2
Booker	Booker	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
2017-03-16	2017-03-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LEHMKUHL	LEHMKUHL	kA
<g/>
,	,	kIx,
Vance	Vance	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cory	Cory	k1gInPc7
Booker	Booker	k1gMnSc1
going	going	k1gMnSc1
vegan	vegan	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
'	'	kIx"
<g/>
I	i	k9
wasn	wasn	k1gInSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
living	living	k1gInSc1
my	my	k3xPp1nPc1
truth	truth	k1gInSc4
<g/>
'	'	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philly	Philla	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2014-12-11	2014-12-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LORIA	LORIA	kA
<g/>
,	,	kIx,
Joe	Joe	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cory	Cory	k1gInPc7
Booker	Booker	k1gMnSc1
Fires	Fires	k1gMnSc1
Back	Back	k1gMnSc1
at	at	k?
Ridiculous	Ridiculous	k1gMnSc1
Claim	Claim	k1gMnSc1
That	That	k1gMnSc1
a	a	k8xC
Vegan	Vegan	k1gMnSc1
Can	Can	k1gFnSc2
<g/>
’	’	k?
<g/>
t	t	k?
Be	Be	k1gMnSc7
POTUS	POTUS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mercy	Merca	k1gFnPc4
for	forum	k1gNnPc2
Animals	Animalsa	k1gFnPc2
<g/>
,	,	kIx,
2016-08-04	2016-08-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Cory	Cora	k1gFnSc2
Booker	Booker	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Senátoři	senátor	k1gMnPc1
117	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kongresu	kongres	k1gInSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
Předsedkyně	předsedkyně	k1gFnSc1
Senátu	senát	k1gInSc2
<g/>
:	:	kIx,
Kamala	Kamala	k1gFnSc1
Harrisová	Harrisový	k2eAgFnSc1d1
•	•	k?
Předseda	předseda	k1gMnSc1
pro	pro	k7c4
tempore	tempor	k1gInSc5
<g/>
:	:	kIx,
Patrick	Patrick	k1gMnSc1
Leahy	Leaha	k1gFnSc2
•	•	k?
Vůdce	vůdce	k1gMnSc1
většiny	většina	k1gFnSc2
<g/>
:	:	kIx,
Chuck	Chuck	k1gMnSc1
Schumer	Schumer	k1gMnSc1
•	•	k?
Vůdce	vůdce	k1gMnSc1
menšiny	menšina	k1gFnSc2
<g/>
:	:	kIx,
Mitch	Mitch	k1gMnSc1
McConnell	McConnell	k1gMnSc1
</s>
<s>
AL	ala	k1gFnPc2
</s>
<s>
AK	AK	kA
</s>
<s>
AZ	AZ	kA
</s>
<s>
AR	ar	k1gInSc1
</s>
<s>
CA	ca	kA
</s>
<s>
CO	co	k3yInSc1,k3yRnSc1,k3yQnSc1
</s>
<s>
CT	CT	kA
</s>
<s>
DE	DE	k?
</s>
<s>
FL	FL	kA
</s>
<s>
GA	GA	kA
</s>
<s>
HI	hi	k0
</s>
<s>
ID	Ida	k1gFnPc2
</s>
<s>
IL	IL	kA
</s>
<s>
Shelby	Shelba	k1gFnPc1
</s>
<s>
Murkowská	Murkowskat	k5eAaPmIp3nS
</s>
<s>
Sinemová	Sinemová	k1gFnSc1
</s>
<s>
Boozman	Boozman	k1gMnSc1
</s>
<s>
Feinsteinová	Feinsteinová	k1gFnSc1
</s>
<s>
Bennet	Bennet	k1gMnSc1
</s>
<s>
Blumenthal	Blumenthal	k1gMnSc1
</s>
<s>
Carper	Carper	k1gMnSc1
</s>
<s>
Scott	Scott	k1gMnSc1
</s>
<s>
Warnock	Warnock	k6eAd1
</s>
<s>
Schatz	Schatz	k1gMnSc1
</s>
<s>
Crapo	Crapa	k1gFnSc5
</s>
<s>
Durbin	Durbin	k2eAgInSc1d1
</s>
<s>
Tuberville	Tuberville	k6eAd1
</s>
<s>
Sullivan	Sullivan	k1gMnSc1
</s>
<s>
Kelly	Kella	k1gFnPc1
</s>
<s>
Cotton	Cotton	k1gInSc1
</s>
<s>
Padilla	Padilla	k6eAd1
</s>
<s>
Hickenlooper	Hickenlooper	k1gMnSc1
</s>
<s>
Murphy	Murpha	k1gFnPc1
</s>
<s>
Coons	Coons	k6eAd1
</s>
<s>
Rubio	Rubio	k6eAd1
</s>
<s>
Ossoff	Ossoff	k1gMnSc1
</s>
<s>
Hironová	Hironová	k1gFnSc1
</s>
<s>
Risch	Risch	k1gMnSc1
</s>
<s>
Duckworthová	Duckworthovat	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
IN	IN	kA
</s>
<s>
IA	ia	k0
</s>
<s>
KA	KA	kA
</s>
<s>
KY	KY	kA
</s>
<s>
LA	la	k1gNnSc1
</s>
<s>
ME	ME	kA
</s>
<s>
MD	MD	kA
</s>
<s>
MA	MA	kA
</s>
<s>
MI	já	k3xPp1nSc3
</s>
<s>
MN	MN	kA
</s>
<s>
MS	MS	kA
</s>
<s>
MO	MO	kA
</s>
<s>
Young	Young	k1gMnSc1
</s>
<s>
Grassley	Grasslea	k1gFnPc1
</s>
<s>
Marshall	Marshall	k1gMnSc1
</s>
<s>
McConnell	McConnell	k1gMnSc1
</s>
<s>
Kennedy	Kenned	k1gMnPc4
</s>
<s>
Collinsová	Collinsová	k1gFnSc1
</s>
<s>
Van	van	k1gInSc1
Hollen	Hollna	k1gFnPc2
</s>
<s>
Warrenová	Warrenová	k1gFnSc1
</s>
<s>
Stabenowová	Stabenowovat	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
Klobucharová	Klobucharová	k1gFnSc1
</s>
<s>
Wicker	Wicker	k1gMnSc1
</s>
<s>
Hawley	Hawlea	k1gFnPc1
</s>
<s>
Braun	Braun	k1gMnSc1
</s>
<s>
Ernstová	Ernstová	k1gFnSc1
</s>
<s>
Moran	Morana	k1gFnPc2
</s>
<s>
Paul	Paul	k1gMnSc1
</s>
<s>
Cassidy	Cassida	k1gFnPc1
</s>
<s>
King	King	k1gMnSc1
</s>
<s>
Cardin	Cardin	k2eAgInSc1d1
</s>
<s>
Markey	Markea	k1gFnPc1
</s>
<s>
Peters	Peters	k6eAd1
</s>
<s>
Smithová	Smithová	k1gFnSc1
</s>
<s>
Hydeová-Smithová	Hydeová-Smithovat	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Blunt	Blunt	k1gMnSc1
</s>
<s>
MT	MT	kA
</s>
<s>
NE	ne	k9
</s>
<s>
NV	NV	kA
</s>
<s>
NH	NH	kA
</s>
<s>
NJ	NJ	kA
</s>
<s>
NM	NM	kA
</s>
<s>
NY	NY	kA
</s>
<s>
NC	NC	kA
</s>
<s>
ND	ND	kA
</s>
<s>
OH	OH	kA
</s>
<s>
OK	oko	k1gNnPc2
</s>
<s>
OR	OR	kA
</s>
<s>
Tester	tester	k1gInSc1
</s>
<s>
Fischerová	Fischerová	k1gFnSc1
</s>
<s>
Cortezová	Cortezový	k2eAgFnSc1d1
Mastová	Mastová	k1gFnSc1
</s>
<s>
Shaheenová	Shaheenová	k1gFnSc1
</s>
<s>
Menendes	Menendes	k1gMnSc1
</s>
<s>
Luján	Luján	k1gMnSc1
</s>
<s>
Schumer	Schumer	k1gMnSc1
</s>
<s>
Burr	Burr	k1gMnSc1
</s>
<s>
Hoeven	Hoeven	k2eAgInSc1d1
</s>
<s>
Brown	Brown	k1gMnSc1
</s>
<s>
Inhofe	Inhof	k1gMnSc5
</s>
<s>
Wyden	Wyden	k1gInSc1
</s>
<s>
Daines	Daines	k1gMnSc1
</s>
<s>
Sasse	Sasse	k6eAd1
</s>
<s>
Rosenová	Rosenová	k1gFnSc1
</s>
<s>
Hassanová	Hassanová	k1gFnSc1
</s>
<s>
Booker	Booker	k1gMnSc1
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
</s>
<s>
Gillibrandová	Gillibrandový	k2eAgFnSc1d1
</s>
<s>
Tillis	Tillis	k1gFnSc1
</s>
<s>
Cramer	Cramer	k1gMnSc1
</s>
<s>
Portman	Portman	k1gMnSc1
</s>
<s>
Langford	Langford	k6eAd1
</s>
<s>
Merkley	Merklea	k1gFnPc1
</s>
<s>
PA	Pa	kA
</s>
<s>
RI	RI	kA
</s>
<s>
SC	SC	kA
</s>
<s>
SD	SD	kA
</s>
<s>
TN	TN	kA
</s>
<s>
TX	TX	kA
</s>
<s>
UT	UT	kA
</s>
<s>
VT	VT	kA
</s>
<s>
VA	va	k0wR
</s>
<s>
WA	WA	kA
</s>
<s>
WV	WV	kA
</s>
<s>
WI	WI	kA
</s>
<s>
WY	WY	kA
</s>
<s>
Casey	Casea	k1gFnPc1
</s>
<s>
Reed	Reed	k6eAd1
</s>
<s>
Graham	Graham	k1gMnSc1
</s>
<s>
Thune	Thunout	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
Hagerty	Hagert	k1gInPc1
</s>
<s>
Cruz	Cruz	k1gMnSc1
</s>
<s>
Romney	Romnea	k1gFnPc1
</s>
<s>
Leahy	Leaha	k1gFnPc1
</s>
<s>
Warner	Warner	k1gMnSc1
</s>
<s>
Murrayová	Murrayovat	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Manchin	Manchin	k1gMnSc1
</s>
<s>
Johnson	Johnson	k1gMnSc1
</s>
<s>
Lummisová	Lummisový	k2eAgFnSc1d1
</s>
<s>
Toomey	Toomea	k1gFnPc1
</s>
<s>
Whitehouse	Whitehouse	k6eAd1
</s>
<s>
Scott	Scott	k1gMnSc1
</s>
<s>
Rounds	Rounds	k6eAd1
</s>
<s>
Blackburnová	Blackburnová	k1gFnSc1
</s>
<s>
Cornyn	Cornyn	k1gMnSc1
</s>
<s>
Lee	Lea	k1gFnSc3
</s>
<s>
Sanders	Sanders	k6eAd1
</s>
<s>
Kaine	Kain	k1gMnSc5
</s>
<s>
Kantwellová	Kantwellová	k1gFnSc1
</s>
<s>
Mooreová	Mooreový	k2eAgFnSc1d1
Capitová	Capitový	k2eAgFnSc1d1
</s>
<s>
Baldwin	Baldwin	k1gMnSc1
</s>
<s>
Barrasso	Barrassa	k1gFnSc5
</s>
<s>
Republikáni	republikán	k1gMnPc1
(	(	kIx(
<g/>
50	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Demokraté	demokrat	k1gMnPc1
(	(	kIx(
<g/>
48	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Senátoři	senátor	k1gMnPc1
116	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kongresu	kongres	k1gInSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
Předseda	předseda	k1gMnSc1
Senátu	senát	k1gInSc2
<g/>
:	:	kIx,
Mike	Mike	k1gInSc1
Pence	pence	k1gFnSc2
•	•	k?
Předseda	předseda	k1gMnSc1
pro	pro	k7c4
tempore	tempor	k1gInSc5
<g/>
:	:	kIx,
Chuck	Chuck	k1gInSc1
Grassley	Grasslea	k1gFnSc2
•	•	k?
Vůdce	vůdce	k1gMnSc1
většiny	většina	k1gFnSc2
<g/>
:	:	kIx,
Mitch	Mitch	k1gMnSc1
McConnell	McConnell	k1gMnSc1
•	•	k?
Vůdce	vůdce	k1gMnSc1
menšiny	menšina	k1gFnSc2
<g/>
:	:	kIx,
Charles	Charles	k1gMnSc1
Schumer	Schumer	k1gMnSc1
</s>
<s>
AL	ala	k1gFnPc2
</s>
<s>
AK	AK	kA
</s>
<s>
AZ	AZ	kA
</s>
<s>
AR	ar	k1gInSc1
</s>
<s>
CA	ca	kA
</s>
<s>
CO	co	k3yRnSc1,k3yQnSc1,k3yInSc1
</s>
<s>
CT	CT	kA
</s>
<s>
DE	DE	k?
</s>
<s>
FL	FL	kA
</s>
<s>
GA	GA	kA
</s>
<s>
HI	hi	k0
</s>
<s>
ID	Ida	k1gFnPc2
</s>
<s>
IL	IL	kA
</s>
<s>
Shelby	Shelba	k1gFnPc1
</s>
<s>
Murkowská	Murkowskat	k5eAaPmIp3nS
</s>
<s>
Sinemová	Sinemová	k1gFnSc1
</s>
<s>
Boozman	Boozman	k1gMnSc1
</s>
<s>
Feinsteinová	Feinsteinová	k1gFnSc1
</s>
<s>
Bennet	Bennet	k1gMnSc1
</s>
<s>
Blumenthal	Blumenthal	k1gMnSc1
</s>
<s>
Carper	Carper	k1gMnSc1
</s>
<s>
Scott	Scott	k1gMnSc1
</s>
<s>
Loefflerová	Loefflerová	k1gFnSc1
</s>
<s>
Schatz	Schatz	k1gMnSc1
</s>
<s>
Crapo	Crapa	k1gFnSc5
</s>
<s>
Durbin	Durbin	k2eAgInSc1d1
</s>
<s>
Jones	Jones	k1gMnSc1
</s>
<s>
Sullivan	Sullivan	k1gMnSc1
</s>
<s>
McSallyová	McSallyovat	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Cotton	Cotton	k1gInSc1
</s>
<s>
Harrisová	Harrisový	k2eAgFnSc1d1
</s>
<s>
Gardner	Gardner	k1gMnSc1
</s>
<s>
Murphy	Murpha	k1gFnPc1
</s>
<s>
Coons	Coons	k6eAd1
</s>
<s>
Rubio	Rubio	k6eAd1
</s>
<s>
Perdue	Perdue	k6eAd1
</s>
<s>
Hironová	Hironová	k1gFnSc1
</s>
<s>
Risch	Risch	k1gMnSc1
</s>
<s>
Duckworth	Duckworth	k1gMnSc1
</s>
<s>
IN	IN	kA
</s>
<s>
IA	ia	k0
</s>
<s>
KA	KA	kA
</s>
<s>
KY	KY	kA
</s>
<s>
LA	la	k1gNnSc1
</s>
<s>
ME	ME	kA
</s>
<s>
MD	MD	kA
</s>
<s>
MA	MA	kA
</s>
<s>
MI	já	k3xPp1nSc3
</s>
<s>
MN	MN	kA
</s>
<s>
MS	MS	kA
</s>
<s>
MO	MO	kA
</s>
<s>
Young	Young	k1gMnSc1
</s>
<s>
Grassley	Grasslea	k1gFnPc1
</s>
<s>
Roberts	Roberts	k6eAd1
</s>
<s>
McConnell	McConnell	k1gMnSc1
</s>
<s>
Kennedy	Kenned	k1gMnPc4
</s>
<s>
Collinsová	Collinsová	k1gFnSc1
</s>
<s>
Van	van	k1gInSc1
Hollen	Hollna	k1gFnPc2
</s>
<s>
Warrenová	Warrenová	k1gFnSc1
</s>
<s>
Stabenow	Stabenow	k?
</s>
<s>
Klobucharová	Klobucharová	k1gFnSc1
</s>
<s>
Wicker	Wicker	k1gMnSc1
</s>
<s>
Hawley	Hawlea	k1gFnPc1
</s>
<s>
Braun	Braun	k1gMnSc1
</s>
<s>
Ernstová	Ernstová	k1gFnSc1
</s>
<s>
Moran	Morana	k1gFnPc2
</s>
<s>
Paul	Paul	k1gMnSc1
</s>
<s>
Cassidy	Cassida	k1gFnPc1
</s>
<s>
King	King	k1gMnSc1
</s>
<s>
Cardin	Cardin	k2eAgInSc1d1
</s>
<s>
Markey	Markea	k1gFnPc1
</s>
<s>
Peters	Peters	k6eAd1
</s>
<s>
Smith	Smith	k1gMnSc1
</s>
<s>
Hydeová-Smithová	Hydeová-Smithovat	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Blunt	Blunt	k1gMnSc1
</s>
<s>
MT	MT	kA
</s>
<s>
NE	ne	k9
</s>
<s>
NV	NV	kA
</s>
<s>
NH	NH	kA
</s>
<s>
NJ	NJ	kA
</s>
<s>
NM	NM	kA
</s>
<s>
NY	NY	kA
</s>
<s>
NC	NC	kA
</s>
<s>
ND	ND	kA
</s>
<s>
OH	OH	kA
</s>
<s>
OK	oko	k1gNnPc2
</s>
<s>
OR	OR	kA
</s>
<s>
Tester	tester	k1gInSc1
</s>
<s>
Fischer	Fischer	k1gMnSc1
</s>
<s>
Cortez	Cortez	k1gInSc1
Masto	Masto	k1gNnSc1
</s>
<s>
Shaheenová	Shaheenová	k1gFnSc1
</s>
<s>
Menendes	Menendes	k1gMnSc1
</s>
<s>
Udall	Udall	k1gMnSc1
</s>
<s>
Schumer	Schumer	k1gMnSc1
</s>
<s>
Burr	Burr	k1gMnSc1
</s>
<s>
Hoeven	Hoeven	k2eAgInSc1d1
</s>
<s>
Brown	Brown	k1gMnSc1
</s>
<s>
Inhofe	Inhof	k1gMnSc5
</s>
<s>
Wyden	Wyden	k1gInSc1
</s>
<s>
Daines	Daines	k1gMnSc1
</s>
<s>
Sasse	Sasse	k6eAd1
</s>
<s>
Rosen	rosen	k2eAgInSc1d1
</s>
<s>
Hassan	Hassan	k1gMnSc1
</s>
<s>
Booker	Booker	k1gMnSc1
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
</s>
<s>
Gillibrandová	Gillibrandový	k2eAgFnSc1d1
</s>
<s>
Tillis	Tillis	k1gFnSc1
</s>
<s>
Cramer	Cramer	k1gMnSc1
</s>
<s>
Portman	Portman	k1gMnSc1
</s>
<s>
Langford	Langford	k6eAd1
</s>
<s>
Merkley	Merklea	k1gFnPc1
</s>
<s>
PA	Pa	kA
</s>
<s>
RI	RI	kA
</s>
<s>
SC	SC	kA
</s>
<s>
SD	SD	kA
</s>
<s>
TN	TN	kA
</s>
<s>
TX	TX	kA
</s>
<s>
UT	UT	kA
</s>
<s>
VT	VT	kA
</s>
<s>
VA	va	k0wR
</s>
<s>
WA	WA	kA
</s>
<s>
WV	WV	kA
</s>
<s>
WI	WI	kA
</s>
<s>
WY	WY	kA
</s>
<s>
Casey	Casea	k1gFnPc1
</s>
<s>
Reed	Reed	k6eAd1
</s>
<s>
Graham	Graham	k1gMnSc1
</s>
<s>
Thune	Thunout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Alexander	Alexandra	k1gFnPc2
</s>
<s>
Cruz	Cruz	k1gMnSc1
</s>
<s>
Romney	Romnea	k1gFnPc1
</s>
<s>
Leahy	Leaha	k1gFnPc1
</s>
<s>
Warner	Warner	k1gMnSc1
</s>
<s>
Murray	Murraa	k1gFnPc1
</s>
<s>
Manchin	Manchin	k1gMnSc1
</s>
<s>
Johnson	Johnson	k1gMnSc1
</s>
<s>
Enzi	Enzi	k6eAd1
</s>
<s>
Toomey	Toomea	k1gFnPc1
</s>
<s>
Whitehouse	Whitehouse	k6eAd1
</s>
<s>
Scott	Scott	k1gMnSc1
</s>
<s>
Rounds	Rounds	k6eAd1
</s>
<s>
Blackburnová	Blackburnová	k1gFnSc1
</s>
<s>
Cornyn	Cornyn	k1gMnSc1
</s>
<s>
Lee	Lea	k1gFnSc3
</s>
<s>
Sanders	Sanders	k6eAd1
</s>
<s>
Kaine	Kain	k1gMnSc5
</s>
<s>
Kantwell	Kantwell	k1gMnSc1
</s>
<s>
Moore	Moor	k1gMnSc5
Capito	Capita	k1gMnSc5
</s>
<s>
Baldwin	Baldwin	k1gMnSc1
</s>
<s>
Barrasso	Barrassa	k1gFnSc5
</s>
<s>
Republikáni	republikán	k1gMnPc1
(	(	kIx(
<g/>
53	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Demokraté	demokrat	k1gMnPc1
(	(	kIx(
<g/>
45	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2016934740	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2006012700	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
38149106191268491526	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2006012700	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
