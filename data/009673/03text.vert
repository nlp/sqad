<p>
<s>
Clearwater	Clearwater	k1gMnSc1	Clearwater
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Pinellas	Pinellas	k1gInSc1	Pinellas
County	Counta	k1gFnSc2	Counta
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Florida	Florida	k1gFnSc1	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
97,7	[number]	k4	97,7
km2	km2	k4	km2
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
107	[number]	k4	107
685	[number]	k4	685
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hlavní	hlavní	k2eAgNnSc1d1	hlavní
sídlo	sídlo	k1gNnSc1	sídlo
scientologické	scientologický	k2eAgFnSc2d1	Scientologická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
zde	zde	k6eAd1	zde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
slovenský	slovenský	k2eAgMnSc1d1	slovenský
státník	státník	k1gMnSc1	státník
Milan	Milan	k1gMnSc1	Milan
Hodža	Hodža	k1gMnSc1	Hodža
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seserská	Seserský	k2eAgNnPc1d1	Seserský
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Nagano	Nagano	k1gNnSc1	Nagano
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
</s>
</p>
<p>
<s>
Wyong	Wyong	k1gInSc1	Wyong
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Clearwater	Clearwatra	k1gFnPc2	Clearwatra
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
