<s>
Procházková	procházkový	k2eAgFnSc1d1
tůň	tůň	k1gFnSc1
je	být	k5eAaImIp3nS
staré	starý	k2eAgNnSc4d1
říční	říční	k2eAgNnSc4d1
rameno	rameno	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
místě	místo	k1gNnSc6
dřívějšího	dřívější	k2eAgInSc2d1
toku	tok	k1gInSc2
řeky	řeka	k1gFnSc2
Labe	Labe	k1gNnSc2
oddělením	oddělení	k1gNnSc7
od	od	k7c2
tůně	tůně	k1gFnSc2
Hrad	hrad	k1gInSc1
<g/>
.	.	kIx.
</s>