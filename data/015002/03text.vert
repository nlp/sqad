<s>
Procházková	procházkový	k2eAgFnSc1d1
tůň	tůň	k1gFnSc1
</s>
<s>
Procházková	procházkový	k2eAgFnSc1d1
tůň	tůň	k1gFnSc1
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc4
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
Okresy	okres	k1gInPc1
</s>
<s>
Praha-východ	Praha-východ	k1gInSc1
<g/>
,	,	kIx,
Nymburk	Nymburk	k1gInSc1
Obce	obec	k1gFnSc2
</s>
<s>
Čelákovice	Čelákovice	k1gFnPc1
<g/>
,	,	kIx,
Přerov	Přerov	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Katastrální	katastrální	k2eAgFnSc2d1
území	území	k1gNnSc6
</s>
<s>
Sedlčánky	Sedlčánka	k1gFnPc1
<g/>
,	,	kIx,
Přerov	Přerov	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
8	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Rozměry	rozměra	k1gFnSc2
Rozloha	rozloha	k1gFnSc1
</s>
<s>
0,301	0,301	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Délka	délka	k1gFnSc1
</s>
<s>
80	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Šířka	šířka	k1gFnSc1
</s>
<s>
50	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ostatní	ostatní	k2eAgInSc1d1
Typ	typ	k1gInSc1
</s>
<s>
tůň	tůň	k1gFnSc1
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
172	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
m	m	kA
n.	n.	k?
m.	m.	k?
Přítok	přítok	k1gInSc4
vody	voda	k1gFnSc2
</s>
<s>
Zámecký	zámecký	k2eAgInSc1d1
potok	potok	k1gInSc1
Odtok	odtok	k1gInSc4
vody	voda	k1gFnSc2
</s>
<s>
Zámecký	zámecký	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Procházková	procházkový	k2eAgFnSc1d1
tůň	tůň	k1gFnSc1
je	být	k5eAaImIp3nS
staré	starý	k2eAgNnSc4d1
říční	říční	k2eAgNnSc4d1
rameno	rameno	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
místě	místo	k1gNnSc6
dřívějšího	dřívější	k2eAgInSc2d1
toku	tok	k1gInSc2
řeky	řeka	k1gFnSc2
Labe	Labe	k1gNnSc2
oddělením	oddělení	k1gNnSc7
od	od	k7c2
tůně	tůně	k1gFnSc2
Hrad	hrad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
Sedlčánek	Sedlčánka	k1gFnPc2
na	na	k7c6
hranici	hranice	k1gFnSc6
okresů	okres	k1gInPc2
Praha-východ	Praha-východ	k1gInSc1
a	a	k8xC
Nymburk	Nymburk	k1gInSc1
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
0,301	0,301	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
80	#num#	k4
m	m	kA
dlouhé	dlouhý	k2eAgFnSc2d1
a	a	k8xC
50	#num#	k4
m	m	kA
široké	široký	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
172	#num#	k4
m.	m.	k?
Patří	patřit	k5eAaImIp3nS
do	do	k7c2
skupiny	skupina	k1gFnSc2
Hrbáčkových	Hrbáčkových	k2eAgFnPc2d1
tůní	tůně	k1gFnPc2
avšak	avšak	k8xC
není	být	k5eNaImIp3nS
zahrnuto	zahrnout	k5eAaPmNgNnS
do	do	k7c2
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Káraný	káraný	k2eAgMnSc1d1
-	-	kIx~
Hrbáčkovy	Hrbáčkův	k2eAgFnPc1d1
tůně	tůně	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Okolí	okolí	k1gNnSc1
</s>
<s>
V	v	k7c6
okolí	okolí	k1gNnSc6
tůně	tůně	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
jen	jen	k9
několik	několik	k4yIc4
stromů	strom	k1gInPc2
u	u	k7c2
jižního	jižní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
ke	k	k7c3
břehům	břeh	k1gInPc3
dosahují	dosahovat	k5eAaImIp3nP
pole	pole	k1gNnPc4
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
severu	sever	k1gInSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
východní	východní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
je	být	k5eAaImIp3nS
porostlý	porostlý	k2eAgInSc4d1
keři	keř	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
navazuje	navazovat	k5eAaImIp3nS
tůň	tůň	k1gFnSc1
Hrad	hrad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Jezerem	jezero	k1gNnSc7
protéká	protékat	k5eAaImIp3nS
Zámecký	zámecký	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitéká	přitékat	k5eAaImIp3nS
na	na	k7c6
východě	východ	k1gInSc6
od	od	k7c2
tůně	tůně	k1gFnSc2
Poltruba	poltruba	k1gFnSc1
a	a	k8xC
odtéká	odtékat	k5eAaImIp3nS
na	na	k7c6
západě	západ	k1gInSc6
do	do	k7c2
navazující	navazující	k2eAgFnSc2d1
tůně	tůně	k1gFnSc2
Hrad	hrad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náleží	náležet	k5eAaImIp3nS
k	k	k7c3
povodí	povodí	k1gNnSc6
Výmoly	výmol	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Přístup	přístup	k1gInSc1
</s>
<s>
Přístup	přístup	k1gInSc1
je	být	k5eAaImIp3nS
možný	možný	k2eAgInSc1d1
přes	přes	k7c4
pole	pole	k1gNnSc4
od	od	k7c2
levého	levý	k2eAgInSc2d1
břehu	břeh	k1gInSc2
Labe	Labe	k1gNnSc2
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yRgNnSc6,k3yQgNnSc6,k3yIgNnSc6
vedou	vést	k5eAaImIp3nP
ze	z	k7c2
Sedlčánek	Sedlčánka	k1gFnPc2
do	do	k7c2
Přerova	Přerov	k1gInSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
:	:	kIx,
</s>
<s>
cyklostezka	cyklostezka	k1gFnSc1
č.	č.	k?
0	#num#	k4
<g/>
0	#num#	k4
<g/>
19	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
turistická	turistický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
č.	č.	k?
3113	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Na	na	k7c6
jezeře	jezero	k1gNnSc6
rostou	růst	k5eAaImIp3nP
stulíky	stulík	k1gInPc1
žluté	žlutý	k2eAgInPc1d1
a	a	k8xC
v	v	k7c6
okolí	okolí	k1gNnSc6
zpívají	zpívat	k5eAaImIp3nP
skřivani	skřivan	k1gMnPc1
polní	polní	k2eAgMnSc1d1
a	a	k8xC
slavíci	slavík	k1gMnPc1
obecní	obecnět	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ryb	ryba	k1gFnPc2
jsou	být	k5eAaImIp3nP
zastoupeni	zastoupit	k5eAaPmNgMnP
karas	karas	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
kapr	kapr	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
lín	lín	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
štika	štika	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
,	,	kIx,
sumec	sumec	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Odhad	odhad	k1gInSc1
podle	podle	k7c2
ikatastr	ikatastr	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Odhad	odhad	k1gInSc1
podle	podle	k7c2
mapy	mapa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Procházková	procházkový	k2eAgFnSc1d1
tůň	tůň	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Turistický	turistický	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
po	po	k7c6
čelákovském	čelákovský	k2eAgInSc6d1
rybářském	rybářský	k2eAgInSc6d1
revíru	revír	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Procházková	Procházková	k1gFnSc1
-	-	kIx~
foto	foto	k1gNnSc1
revíru	revír	k1gInSc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Tůň	tůň	k1gFnSc1
Procházková	Procházková	k1gFnSc1
–	–	k?
0,3	0,3	k4
ha	ha	kA
</s>
