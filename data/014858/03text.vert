<s>
LGBT	LGBT	kA
práva	práv	k2eAgFnSc1d1
v	v	k7c6
Kazachstánu	Kazachstán	k1gInSc6
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Kazachstánu	Kazachstán	k1gInSc2
</s>
<s>
Lesby	lesba	k1gFnPc1
<g/>
,	,	kIx,
gayové	gay	k1gMnPc1
<g/>
,	,	kIx,
bisexuálové	bisexuál	k1gMnPc1
a	a	k8xC
transsexuálové	transsexuál	k1gMnPc1
(	(	kIx(
<g/>
LGBT	LGBT	kA
<g/>
)	)	kIx)
v	v	k7c6
Kazachstánu	Kazachstán	k1gInSc6
čelí	čelit	k5eAaImIp3nS
právním	právní	k2eAgInPc3d1
problémům	problém	k1gInPc3
a	a	k8xC
diskriminaci	diskriminace	k1gFnSc6
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yQgInPc7,k3yRgInPc7,k3yIgInPc7
se	se	k3xPyFc4
většinová	většinový	k2eAgFnSc1d1
populace	populace	k1gFnSc1
nepotýká	potýkat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužský	mužský	k2eAgInSc4d1
i	i	k8xC
ženský	ženský	k2eAgInSc4d1
stejnopohlavní	stejnopohlavní	k2eAgInSc4d1
styk	styk	k1gInSc4
je	být	k5eAaImIp3nS
v	v	k7c6
Kazachstánu	Kazachstán	k1gInSc6
legální	legální	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
stejnopohlavní	stejnopohlavní	k2eAgInPc1d1
páry	pár	k1gInPc1
žijící	žijící	k2eAgInPc1d1
ve	v	k7c6
společné	společný	k2eAgFnSc6d1
domácnosti	domácnost	k1gFnSc6
nemají	mít	k5eNaImIp3nP
stejnou	stejný	k2eAgFnSc4d1
právní	právní	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
<g/>
,	,	kIx,
jaké	jaký	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
těší	těšit	k5eAaImIp3nP
různopohlavní	různopohlavní	k2eAgInPc1d1
páry	pár	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
r.	r.	kA
2009	#num#	k4
Kazachstán	Kazachstán	k1gInSc1
spolu-podpořil	spolu-podpořit	k5eAaPmAgInS,k5eAaImAgInS
opoziční	opoziční	k2eAgNnSc4d1
stanovisko	stanovisko	k1gNnSc4
k	k	k7c3
Deklaraci	deklarace	k1gFnSc3
Spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
a	a	k8xC
genderové	genderový	k2eAgFnSc2d1
identity	identita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Zákony	zákon	k1gInPc1
týkající	týkající	k2eAgFnSc2d1
se	se	k3xPyFc4
stejnopohlavní	stejnopohlavní	k2eAgFnSc2d1
sexuální	sexuální	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
</s>
<s>
Mužská	mužský	k2eAgFnSc1d1
i	i	k8xC
ženská	ženský	k2eAgFnSc1d1
sexuální	sexuální	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Kazachstánu	Kazachstán	k1gInSc6
legální	legální	k2eAgFnSc1d1
od	od	k7c2
r.	r.	kA
1998	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Věk	věk	k1gInSc1
způsobilosti	způsobilost	k1gFnSc2
k	k	k7c3
pohlavnímu	pohlavní	k2eAgInSc3d1
styku	styk	k1gInSc3
je	být	k5eAaImIp3nS
stanoven	stanovit	k5eAaPmNgInS
na	na	k7c4
16	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
touto	tento	k3xDgFnSc7
novelizací	novelizace	k1gFnSc7
kazašský	kazašský	k2eAgInSc1d1
trestní	trestní	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
trestal	trestat	k5eAaImAgInS
dle	dle	k7c2
§	§	k?
104	#num#	k4
anální	anální	k2eAgInSc4d1
sex	sex	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
legislativa	legislativa	k1gFnSc1
vycházela	vycházet	k5eAaImAgFnS
ze	z	k7c2
sekce	sekce	k1gFnSc2
121	#num#	k4
Trestního	trestní	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
kriminalizoval	kriminalizovat	k5eAaImAgInS
anální	anální	k2eAgInSc4d1
pohlavní	pohlavní	k2eAgInSc4d1
styk	styk	k1gInSc4
mezi	mezi	k7c7
muži	muž	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
LGBT	LGBT	kA
osoby	osoba	k1gFnSc2
v	v	k7c6
Kazachstánu	Kazachstán	k1gInSc6
čelí	čelit	k5eAaImIp3nS
diskriminaci	diskriminace	k1gFnSc4
a	a	k8xC
předsudkům	předsudek	k1gInPc3
na	na	k7c6
základě	základ	k1gInSc6
jejich	jejich	k3xOp3gFnSc2
sexuální	sexuální	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
nebo	nebo	k8xC
genderové	genderový	k2eAgFnSc2d1
identity	identita	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
běžného	běžný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Negativní	negativní	k2eAgInPc1d1
postoje	postoj	k1gInPc1
vůči	vůči	k7c3
LGBT	LGBT	kA
osobám	osoba	k1gFnPc3
jako	jako	k9
je	být	k5eAaImIp3nS
ostrakizace	ostrakizace	k1gFnPc4
<g/>
,	,	kIx,
posměch	posměch	k1gInSc4
a	a	k8xC
násilí	násilí	k1gNnSc4
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
často	často	k6eAd1
špatný	špatný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
psychické	psychický	k2eAgNnSc4d1
a	a	k8xC
duševní	duševní	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
strachu	strach	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
vyhnuli	vyhnout	k5eAaPmAgMnP
nebezpečí	nebezpeč	k1gFnPc2wB
ze	z	k7c2
strany	strana	k1gFnSc2
společnosti	společnost	k1gFnSc2
neschvalující	schvalující	k2eNgInSc4d1
jejich	jejich	k3xOp3gInSc4
odlišný	odlišný	k2eAgInSc4d1
životní	životní	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
,	,	kIx,
mnoho	mnoho	k6eAd1
LGBT	LGBT	kA
lidí	člověk	k1gMnPc2
drží	držet	k5eAaImIp3nS
svojí	svojit	k5eAaImIp3nS
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
nebo	nebo	k8xC
genderovou	genderový	k2eAgFnSc4d1
identitu	identita	k1gFnSc4
v	v	k7c6
tajnosti	tajnost	k1gFnSc6
před	před	k7c7
vesměs	vesměs	k6eAd1
všemi	všecek	k3xTgMnPc7
lidmi	člověk	k1gMnPc7
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
nezbytné	zbytný	k2eNgNnSc4d1,k2eAgNnSc4d1
tajit	tajit	k5eAaImF
jejich	jejich	k3xOp3gFnSc4
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
nebo	nebo	k8xC
genderovou	genderový	k2eAgFnSc4d1
identitu	identita	k1gFnSc4
zejména	zejména	k9
na	na	k7c6
pracovišti	pracoviště	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
neztratili	ztratit	k5eNaPmAgMnP
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
případně	případně	k6eAd1
neztratili	ztratit	k5eNaPmAgMnP
dobré	dobrý	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
se	s	k7c7
zaměstnavateli	zaměstnavatel	k1gMnPc7
a	a	k8xC
kolegy	kolega	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snahy	snaha	k1gFnPc4
o	o	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
publikaci	publikace	k1gFnSc4
homofobního	homofobní	k2eAgNnSc2d1
a	a	k8xC
transfobního	transfobní	k2eAgNnSc2d1
násilí	násilí	k1gNnSc2
se	se	k3xPyFc4
často	často	k6eAd1
setkávají	setkávat	k5eAaImIp3nP
s	s	k7c7
odporem	odpor	k1gInSc7
a	a	k8xC
nepřátelství	nepřátelství	k1gNnSc1
ze	z	k7c2
strany	strana	k1gFnSc2
úřadů	úřad	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
studie	studie	k1gFnSc1
Chicagské	chicagský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
došla	dojít	k5eAaPmAgFnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
akceptace	akceptace	k1gFnSc1
LGBT	LGBT	kA
osob	osoba	k1gFnPc2
je	být	k5eAaImIp3nS
buď	buď	k8xC
pomalejší	pomalý	k2eAgMnSc1d2
nebo	nebo	k8xC
mnohem	mnohem	k6eAd1
odlišnější	odlišný	k2eAgFnPc4d2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
a	a	k8xC
dalších	další	k2eAgFnPc2d1
postsovětských	postsovětský	k2eAgFnPc2d1
republik	republika	k1gFnPc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
se	s	k7c7
světem	svět	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Životní	životní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
</s>
<s>
Duhová	duhový	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
Kazachstánu	Kazachstán	k1gInSc2
</s>
<s>
Legální	legální	k2eAgInSc1d1
stejnopohlavní	stejnopohlavní	k2eAgInSc1d1
styk	styk	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stejný	stejný	k2eAgInSc1d1
věk	věk	k1gInSc1
legální	legální	k2eAgFnSc2d1
způsobilosti	způsobilost	k1gFnSc2
k	k	k7c3
pohlavnímu	pohlavní	k2eAgInSc3d1
styku	styk	k1gInSc3
pro	pro	k7c4
obě	dva	k4xCgFnPc4
orientace	orientace	k1gFnPc4
</s>
<s>
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anti-diskriminační	Anti-diskriminační	k2eAgInPc1d1
zákony	zákon	k1gInPc1
v	v	k7c6
zaměstnání	zaměstnání	k1gNnSc6
</s>
<s>
Anti-diskriminační	Anti-diskriminační	k2eAgInPc1d1
zákony	zákon	k1gInPc1
v	v	k7c6
přístupu	přístup	k1gInSc6
ke	k	k7c3
zboží	zboží	k1gNnSc3
a	a	k8xC
službám	služba	k1gFnPc3
</s>
<s>
Anti-diskriminační	Anti-diskriminační	k2eAgInPc1d1
zákony	zákon	k1gInPc1
v	v	k7c6
ostatních	ostatní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
(	(	kIx(
<g/>
homofobní	homofobnit	k5eAaPmIp3nP
urážky	urážka	k1gFnPc1
<g/>
,	,	kIx,
zločiny	zločin	k1gInPc1
z	z	k7c2
nenávisti	nenávist	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1
forma	forma	k1gFnSc1
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1
soužití	soužití	k1gNnSc2
</s>
<s>
Adopce	adopce	k1gFnPc1
dítěte	dítě	k1gNnSc2
partnera	partner	k1gMnSc2
</s>
<s>
Společná	společný	k2eAgFnSc1d1
adopce	adopce	k1gFnSc1
stejnopohlavními	stejnopohlavní	k2eAgMnPc7d1
páry	pár	k1gInPc1
</s>
<s>
Gayové	gay	k1gMnPc1
a	a	k8xC
lesby	lesba	k1gFnPc1
můžou	můžou	k?
otevřeně	otevřeně	k6eAd1
sloužit	sloužit	k5eAaImF
v	v	k7c6
armádě	armáda	k1gFnSc6
</s>
<s>
Možnost	možnost	k1gFnSc1
změny	změna	k1gFnSc2
pohlaví	pohlaví	k1gNnSc2
</s>
<s>
Přístup	přístup	k1gInSc1
k	k	k7c3
umělému	umělý	k2eAgNnSc3d1
oplodnění	oplodnění	k1gNnSc3
pro	pro	k7c4
lesbické	lesbický	k2eAgFnPc4d1
ženy	žena	k1gFnPc4
</s>
<s>
Náhradní	náhradní	k2eAgNnSc1d1
mateřství	mateřství	k1gNnSc1
pro	pro	k7c4
gay	gay	k1gMnSc1
páry	pár	k1gInPc1
</s>
<s>
Související	související	k2eAgFnSc1d1
</s>
<s>
LGBT	LGBT	kA
práva	práv	k2eAgFnSc1d1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
LGBT	LGBT	kA
práva	práv	k2eAgFnSc1d1
v	v	k7c6
Asii	Asie	k1gFnSc6
</s>
<s>
Kazašská	kazašský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
LGBT	LGBT	kA
rights	rights	k6eAd1
in	in	k?
Kazakhstan	Kazakhstan	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
old	old	k?
<g/>
.	.	kIx.
<g/>
ilga	ilga	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
old	old	k?
<g/>
.	.	kIx.
<g/>
ilga	ilga	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
National	National	k1gFnSc1
Laws	Laws	k1gInSc1
Legislation	Legislation	k1gInSc1
of	of	k?
Interpol	interpol	k1gInSc1
–	–	k?
member	member	k1gInSc1
states	states	k1gInSc4
on	on	k3xPp3gMnSc1
sexual	sexual	k1gMnSc1
offences	offences	k1gMnSc1
against	against	k1gMnSc1
children	childrna	k1gFnPc2
–	–	k?
Kazakhstan	Kazakhstan	k1gInSc1
<g/>
.	.	kIx.
www.interpol.int	www.interpol.int	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KAZAKHSTAN	KAZAKHSTAN	kA
<g/>
.	.	kIx.
ilga	ilga	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Unacknowledged	Unacknowledged	k1gMnSc1
and	and	k?
Unprotected	Unprotected	k1gMnSc1
<g/>
:	:	kIx,
Lesbian	Lesbian	k1gMnSc1
<g/>
,	,	kIx,
Gay	gay	k1gMnSc1
<g/>
,	,	kIx,
Bisexual	Bisexual	k1gMnSc1
and	and	k?
Transgender	Transgender	k1gMnSc1
People	People	k1gMnSc1
in	in	k?
Kazakhstan	Kazakhstan	k1gInSc1
by	by	kYmCp3nS
Soros	Soros	k1gInSc4
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
www.soros.kz	www.soros.kz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Cross-national	Cross-national	k1gMnSc1
Differences	Differences	k1gMnSc1
in	in	k?
Attitudes	Attitudes	k1gMnSc1
towards	towards	k6eAd1
Homosexuality	homosexualita	k1gFnSc2
by	by	kYmCp3nS
NORC	NORC	kA
<g/>
/	/	kIx~
<g/>
University	universita	k1gFnPc1
of	of	k?
Chicago	Chicago	k1gNnSc1
<g/>
.	.	kIx.
www	www	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
law	law	k?
<g/>
.	.	kIx.
<g/>
ucla	ucla	k1gMnSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
LGBT	LGBT	kA
práva	práv	k2eAgFnSc1d1
v	v	k7c6
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
Země	zem	k1gFnSc2
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
Andorra	Andorra	k1gFnSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Monako	Monako	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
San	San	k1gMnSc1
Marino	Marina	k1gFnSc5
•	•	k?
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Srbsko	Srbsko	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Guernsey	Guernsea	k1gFnSc2
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Jersey	Jersea	k1gFnSc2
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
<g/>
Území	území	k1gNnSc4
se	s	k7c7
sporným	sporný	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
<g/>
:	:	kIx,
Abcházie	Abcházie	k1gFnSc2
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
•	•	k?
Kosovo	Kosův	k2eAgNnSc1d1
•	•	k?
Podněstří	Podněstří	k1gMnPc7
</s>
<s>
LGBT	LGBT	kA
práva	práv	k2eAgFnSc1d1
v	v	k7c6
asijských	asijský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
Myanmar	Myanmar	k1gMnSc1
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Palestina	Palestina	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Sýrie	Sýrie	k1gFnSc1
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
•	•	k?
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
</s>
