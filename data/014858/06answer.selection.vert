<s desamb="1">
Mužský	mužský	k2eAgInSc4d1
i	i	k8xC
ženský	ženský	k2eAgInSc4d1
stejnopohlavní	stejnopohlavní	k2eAgInSc4d1
styk	styk	k1gInSc4
je	být	k5eAaImIp3nS
v	v	k7c6
Kazachstánu	Kazachstán	k1gInSc6
legální	legální	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
stejnopohlavní	stejnopohlavní	k2eAgInPc1d1
páry	pár	k1gInPc1
žijící	žijící	k2eAgInPc1d1
ve	v	k7c6
společné	společný	k2eAgFnSc6d1
domácnosti	domácnost	k1gFnSc6
nemají	mít	k5eNaImIp3nP
stejnou	stejný	k2eAgFnSc4d1
právní	právní	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
<g/>
,	,	kIx,
jaké	jaký	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
těší	těšit	k5eAaImIp3nP
různopohlavní	různopohlavní	k2eAgInPc1d1
páry	pár	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>