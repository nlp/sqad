<p>
<s>
Borderlands	Borderlands	k6eAd1	Borderlands
je	být	k5eAaImIp3nS	být
RPG	RPG	kA	RPG
střílečka	střílečka	k1gFnSc1	střílečka
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
první	první	k4xOgFnSc2	první
osoby	osoba	k1gFnSc2	osoba
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
firmou	firma	k1gFnSc7	firma
Gearbox	Gearbox	k1gInSc4	Gearbox
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
pokračování	pokračování	k1gNnSc4	pokračování
Borderlands	Borderlandsa	k1gFnPc2	Borderlandsa
2	[number]	k4	2
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbenost	oblíbenost	k1gFnSc1	oblíbenost
získala	získat	k5eAaPmAgFnS	získat
také	také	k9	také
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
kooperativní	kooperativní	k2eAgInSc4d1	kooperativní
multiplayer	multiplayer	k1gInSc4	multiplayer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gameplay	Gamepla	k2eAgMnPc4d1	Gamepla
==	==	k?	==
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
akční	akční	k2eAgFnSc1d1	akční
střílečka	střílečka	k1gFnSc1	střílečka
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
pohledu	pohled	k1gInSc2	pohled
s	s	k7c7	s
výraznými	výrazný	k2eAgInPc7d1	výrazný
RPG	RPG	kA	RPG
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
vylepšovat	vylepšovat	k5eAaImF	vylepšovat
postavu	postava	k1gFnSc4	postava
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
vlastního	vlastní	k2eAgInSc2d1	vlastní
herního	herní	k2eAgInSc2d1	herní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
odlehčená	odlehčený	k2eAgFnSc1d1	odlehčená
a	a	k8xC	a
humorná	humorný	k2eAgFnSc1d1	humorná
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
přispívá	přispívat	k5eAaImIp3nS	přispívat
a	a	k8xC	a
komiksová	komiksový	k2eAgFnSc1d1	komiksová
stylizace	stylizace	k1gFnSc1	stylizace
<g/>
,	,	kIx,	,
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
NPC	NPC	kA	NPC
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
úkoly	úkol	k1gInPc1	úkol
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Pandora	Pandora	k1gFnSc1	Pandora
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
zcela	zcela	k6eAd1	zcela
svobodně	svobodně	k6eAd1	svobodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Postavy	postava	k1gFnPc1	postava
===	===	k?	===
</s>
</p>
<p>
<s>
Mordecai	Mordecai	k6eAd1	Mordecai
vyniká	vynikat	k5eAaImIp3nS	vynikat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
boji	boj	k1gInSc6	boj
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
zbraně	zbraň	k1gFnPc4	zbraň
patří	patřit	k5eAaImIp3nS	patřit
odstřelovací	odstřelovací	k2eAgFnSc1d1	odstřelovací
puška	puška	k1gFnSc1	puška
nebo	nebo	k8xC	nebo
revolver	revolver	k1gInSc1	revolver
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc7d1	speciální
schopností	schopnost	k1gFnSc7	schopnost
tohoto	tento	k3xDgMnSc2	tento
ostrostřelce	ostrostřelec	k1gMnSc2	ostrostřelec
je	být	k5eAaImIp3nS	být
vyvolání	vyvolání	k1gNnSc1	vyvolání
Bloodwinga	Bloodwing	k1gMnSc2	Bloodwing
(	(	kIx(	(
<g/>
agresivního	agresivní	k2eAgMnSc2d1	agresivní
<g/>
,	,	kIx,	,
na	na	k7c4	na
nepřátele	nepřítel	k1gMnPc4	nepřítel
útočícího	útočící	k2eAgMnSc4d1	útočící
ptáka	pták	k1gMnSc4	pták
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lilith	Lilith	k1gInSc1	Lilith
hodně	hodně	k6eAd1	hodně
těží	těžet	k5eAaImIp3nS	těžet
ze	z	k7c2	z
sil	síla	k1gFnPc2	síla
elementů	element	k1gInPc2	element
<g/>
.	.	kIx.	.
</s>
<s>
Nejenže	nejenže	k6eAd1	nejenže
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
zvyšující	zvyšující	k2eAgFnSc4d1	zvyšující
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
vyvolání	vyvolání	k1gNnSc4	vyvolání
takového	takový	k3xDgInSc2	takový
efektu	efekt	k1gInSc2	efekt
u	u	k7c2	u
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
další	další	k2eAgMnPc1d1	další
ze	z	k7c2	z
skillů	skill	k1gMnPc2	skill
(	(	kIx(	(
<g/>
dovedností	dovednost	k1gFnPc2	dovednost
<g/>
)	)	kIx)	)
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
její	její	k3xOp3gFnSc4	její
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
těmto	tento	k3xDgFnPc3	tento
silám	síla	k1gFnPc3	síla
<g/>
.	.	kIx.	.
</s>
<s>
Unikátní	unikátní	k2eAgFnSc4d1	unikátní
schopnost	schopnost	k1gFnSc4	schopnost
téhle	tenhle	k3xDgFnSc2	tenhle
postavy	postava	k1gFnSc2	postava
jí	jíst	k5eAaImIp3nS	jíst
dává	dávat	k5eAaImIp3nS	dávat
moc	moc	k6eAd1	moc
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
neviditelnou	viditelný	k2eNgFnSc7d1	neviditelná
vůči	vůči	k7c3	vůči
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brick	Brick	k6eAd1	Brick
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
na	na	k7c4	na
blízko	blízko	k6eAd1	blízko
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
méně	málo	k6eAd2	málo
schopný	schopný	k2eAgMnSc1d1	schopný
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
.	.	kIx.	.
</s>
<s>
Odolný	odolný	k2eAgMnSc1d1	odolný
silák	silák	k1gMnSc1	silák
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
speciální	speciální	k2eAgFnSc1d1	speciální
schopnost	schopnost	k1gFnSc1	schopnost
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
nazvat	nazvat	k5eAaBmF	nazvat
stavem	stav	k1gInSc7	stav
nepříčetnosti	nepříčetnost	k1gFnSc2	nepříčetnost
<g/>
.	.	kIx.	.
</s>
<s>
Obrazovka	obrazovka	k1gFnSc1	obrazovka
zčervená	zčervenat	k5eAaPmIp3nS	zčervenat
a	a	k8xC	a
Brick	Brick	k1gInSc1	Brick
vyrazí	vyrazit	k5eAaPmIp3nS	vyrazit
vstříc	vstříc	k6eAd1	vstříc
protivníkům	protivník	k1gMnPc3	protivník
s	s	k7c7	s
pěstmi	pěst	k1gFnPc7	pěst
nahoře	nahoře	k6eAd1	nahoře
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
vydrží	vydržet	k5eAaPmIp3nS	vydržet
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roland	Roland	k1gInSc1	Roland
si	se	k3xPyFc3	se
nejvíce	nejvíce	k6eAd1	nejvíce
rozumí	rozumět	k5eAaImIp3nS	rozumět
s	s	k7c7	s
brokovnicí	brokovnice	k1gFnSc7	brokovnice
nebo	nebo	k8xC	nebo
útočnou	útočný	k2eAgFnSc7d1	útočná
puškou	puška	k1gFnSc7	puška
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
skilly	skilla	k1gFnPc1	skilla
zvyšující	zvyšující	k2eAgFnSc4d1	zvyšující
účinnost	účinnost	k1gFnSc4	účinnost
zbraní	zbraň	k1gFnPc2	zbraň
nebo	nebo	k8xC	nebo
automatické	automatický	k2eAgNnSc1d1	automatické
doplňování	doplňování	k1gNnSc1	doplňování
nábojů	náboj	k1gInPc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
charakter	charakter	k1gInSc4	charakter
je	být	k5eAaImIp3nS	být
vyvolávání	vyvolávání	k1gNnSc1	vyvolávání
samostatně	samostatně	k6eAd1	samostatně
fungujícího	fungující	k2eAgInSc2d1	fungující
kulometného	kulometný	k2eAgInSc2d1	kulometný
turretu	turret	k1gInSc2	turret
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
disponuje	disponovat	k5eAaBmIp3nS	disponovat
štítem	štít	k1gInSc7	štít
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
léčí	léčit	k5eAaImIp3nP	léčit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dodatečně	dodatečně	k6eAd1	dodatečně
vydaný	vydaný	k2eAgInSc1d1	vydaný
obsah	obsah	k1gInSc1	obsah
(	(	kIx(	(
<g/>
DLC	DLC	kA	DLC
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
zombie	zombie	k1gFnSc1	zombie
Island	Island	k1gInSc1	Island
of	of	k?	of
Doctor	Doctor	k1gMnSc1	Doctor
Ned	Ned	k1gMnSc1	Ned
</s>
</p>
<p>
<s>
Mad	Mad	k?	Mad
Moxxi	Moxxe	k1gFnSc4	Moxxe
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Underdome	Underdom	k1gInSc5	Underdom
Riot	Riot	k2eAgMnSc1d1	Riot
</s>
</p>
<p>
<s>
The	The	k?	The
Secret	Secret	k1gMnSc1	Secret
Armory	Armora	k1gFnSc2	Armora
of	of	k?	of
General	General	k1gMnSc4	General
Knoxx	Knoxx	k1gInSc1	Knoxx
</s>
</p>
<p>
<s>
Claptrap	Claptrap	k1gInSc1	Claptrap
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
New	New	k1gMnSc7	New
Robot	robota	k1gFnPc2	robota
Revolution	Revolution	k1gInSc1	Revolution
</s>
</p>
<p>
<s>
==	==	k?	==
Pokračování	pokračování	k1gNnSc1	pokračování
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Borderlands	Borderlands	k1gInSc1	Borderlands
2	[number]	k4	2
===	===	k?	===
</s>
</p>
<p>
<s>
Borderlands	Borderlandsa	k1gFnPc2	Borderlandsa
2	[number]	k4	2
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2012	[number]	k4	2012
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zavedené	zavedený	k2eAgInPc1d1	zavedený
herní	herní	k2eAgInPc1d1	herní
mechanismy	mechanismus	k1gInPc1	mechanismus
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
opět	opět	k6eAd1	opět
zavítají	zavítat	k5eAaPmIp3nP	zavítat
na	na	k7c4	na
Pandoru	Pandora	k1gFnSc4	Pandora
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
grafické	grafický	k2eAgNnSc1d1	grafické
zpracování	zpracování	k1gNnSc1	zpracování
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nezměnilo	změnit	k5eNaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Pokračování	pokračování	k1gNnSc1	pokračování
nabízí	nabízet	k5eAaImIp3nS	nabízet
hlavně	hlavně	k9	hlavně
lepší	dobrý	k2eAgInSc1d2	lepší
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgFnSc4d2	delší
herní	herní	k2eAgFnSc4d1	herní
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
DLC	DLC	kA	DLC
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
na	na	k7c4	na
Borderlands	Borderlands	k1gInSc4	Borderlands
na	na	k7c6	na
Bonusweb	Bonuswba	k1gFnPc2	Bonuswba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Borderlands	Borderlands	k6eAd1	Borderlands
na	na	k7c6	na
hodnoceniher	hodnocenihra	k1gFnPc2	hodnocenihra
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Neoficiální	neoficiální	k2eAgFnSc1d1	neoficiální
čeština	čeština	k1gFnSc1	čeština
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
Borderlands	Borderlandsa	k1gFnPc2	Borderlandsa
</s>
</p>
