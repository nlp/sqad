<s>
Spiral	Spirat	k5eAaPmAgMnS	Spirat
Tribe	Trib	k1gInSc5	Trib
byl	být	k5eAaImAgInS	být
freetekno	freetekna	k1gFnSc5	freetekna
soundsystém	soundsystý	k2eAgInSc6d1	soundsystý
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
existující	existující	k2eAgFnPc1d1	existující
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
-	-	kIx~	-
1992	[number]	k4	1992
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
mnoho	mnoho	k4c1	mnoho
free	free	k1gNnPc2	free
parties	partiesa	k1gFnPc2	partiesa
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
volné	volný	k2eAgNnSc1d1	volné
sdružení	sdružení	k1gNnSc1	sdružení
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
největší	veliký	k2eAgFnSc2d3	veliký
slávy	sláva	k1gFnSc2	sláva
rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
až	až	k6eAd1	až
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
23	[number]	k4	23
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
o	o	k7c6	o
anglické	anglický	k2eAgFnSc6d1	anglická
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
francouzské	francouzský	k2eAgMnPc4d1	francouzský
hudebníky	hudebník	k1gMnPc4	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
legislativa	legislativa	k1gFnSc1	legislativa
ale	ale	k8xC	ale
nebyla	být	k5eNaImAgFnS	být
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
těmto	tento	k3xDgMnPc3	tento
free	free	k1gNnSc1	free
parties	partiesa	k1gFnPc2	partiesa
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
zákony	zákon	k1gInPc1	zákon
znemožňující	znemožňující	k2eAgFnSc2d1	znemožňující
volné	volný	k2eAgFnSc2d1	volná
(	(	kIx(	(
<g/>
neorganizované	organizovaný	k2eNgFnSc2d1	neorganizovaná
<g/>
)	)	kIx)	)
party	parta	k1gFnSc2	parta
o	o	k7c6	o
větším	veliký	k2eAgInSc6d2	veliký
počtu	počet	k1gInSc6	počet
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zatčení	zatčení	k1gNnSc6	zatčení
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
propuštění	propuštění	k1gNnSc6	propuštění
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
legendárním	legendární	k2eAgMnSc7d1	legendární
Castlemorton	Castlemorton	k1gInSc1	Castlemorton
Common	Common	k1gInSc1	Common
Festivalem	festival	k1gInSc7	festival
se	se	k3xPyFc4	se
soundsystém	soundsystý	k2eAgInSc6d1	soundsystý
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
teknivalů	teknival	k1gInPc2	teknival
<g/>
.	.	kIx.	.
</s>
<s>
Spiral	Spirat	k5eAaPmAgMnS	Spirat
Tribe	Trib	k1gInSc5	Trib
iniciovali	iniciovat	k5eAaBmAgMnP	iniciovat
i	i	k9	i
Teknival	Teknival	k1gMnPc4	Teknival
Hostomice	Hostomika	k1gFnSc6	Hostomika
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
přispěli	přispět	k5eAaPmAgMnP	přispět
ke	k	k7c3	k
zrodu	zrod	k1gInSc3	zrod
freetekno	freetekno	k1gNnSc1	freetekno
scény	scéna	k1gFnSc2	scéna
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgMnS	být
o	o	k7c6	o
Spiral	Spiral	k1gFnSc6	Spiral
Tribe	Trib	k1gInSc5	Trib
natočen	natočit	k5eAaBmNgInS	natočit
dokument	dokument	k1gInSc1	dokument
"	"	kIx"	"
<g/>
23	[number]	k4	23
Minute	Minut	k1gInSc5	Minut
Warning	Warning	k1gInSc1	Warning
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
kolekce	kolekce	k1gFnSc2	kolekce
"	"	kIx"	"
<g/>
World	World	k1gMnSc1	World
Traveller	Traveller	k1gMnSc1	Traveller
Adventures	Adventures	k1gMnSc1	Adventures
<g/>
"	"	kIx"	"
představující	představující	k2eAgInPc1d1	představující
novodobé	novodobý	k2eAgInPc1d1	novodobý
travellery	traveller	k1gInPc1	traveller
<g/>
.	.	kIx.	.
</s>
<s>
Sebastian	Sebastian	k1gMnSc1	Sebastian
(	(	kIx(	(
<g/>
69	[number]	k4	69
<g/>
db	db	kA	db
<g/>
)	)	kIx)	)
Mark	Mark	k1gMnSc1	Mark
Stormcore	Stormcor	k1gInSc5	Stormcor
Lol	Lol	k1gMnSc1	Lol
Hammond	Hammonda	k1gFnPc2	Hammonda
Zander	Zander	k1gMnSc1	Zander
Simon	Simon	k1gMnSc1	Simon
(	(	kIx(	(
<g/>
Crystal	Crystal	k1gFnSc1	Crystal
Distortion	Distortion	k1gInSc1	Distortion
<g/>
)	)	kIx)	)
Jeff	Jeff	k1gInSc1	Jeff
23	[number]	k4	23
(	(	kIx(	(
<g/>
DJ	DJ	kA	DJ
Tal	Tal	k1gFnSc1	Tal
<g/>
)	)	kIx)	)
Ixindamix	Ixindamix	k1gInSc1	Ixindamix
Curley	Curlea	k1gFnSc2	Curlea
MeltDown	MeltDowna	k1gFnPc2	MeltDowna
Mickey	Mickea	k1gFnSc2	Mickea
kaos	kaos	k1gInSc1	kaos
MC	MC	kA	MC
Skallywag	Skallywag	k1gInSc1	Skallywag
Debbie	Debbie	k1gFnSc1	Debbie
(	(	kIx(	(
<g/>
Pheen	Pheen	k1gInSc1	Pheen
X	X	kA	X
<g/>
)	)	kIx)	)
Sally	Salla	k1gMnSc2	Salla
Alex	Alex	k1gMnSc1	Alex
65	[number]	k4	65
Steve	Steve	k1gMnSc1	Steve
Bedlam	Bedlam	k1gInSc4	Bedlam
James	James	k1gMnSc1	James
alias	alias	k9	alias
Jack	Jack	k1gMnSc1	Jack
<g />
.	.	kIx.	.
</s>
<s>
Acid	Acid	k1gMnSc1	Acid
Little	Little	k1gFnSc2	Little
Ez	Ez	k1gMnSc1	Ez
Stefnie	Stefnie	k1gFnSc2	Stefnie
Nigel	Nigel	k1gMnSc1	Nigel
(	(	kIx(	(
<g/>
Edge	Edge	k1gFnSc1	Edge
<g/>
)	)	kIx)	)
DJ	DJ	kA	DJ
Aztek	Aztek	k1gMnSc1	Aztek
DJ	DJ	kA	DJ
Manic	Manic	k1gMnSc1	Manic
josh	josh	k1gMnSc1	josh
DJ	DJ	kA	DJ
Charlie	Charlie	k1gMnSc1	Charlie
Hall	Hall	k1gMnSc1	Hall
Hamish	Hamish	k1gMnSc1	Hamish
Darren	Darrna	k1gFnPc2	Darrna
Doug	Doug	k1gMnSc1	Doug
Sim	sima	k1gFnPc2	sima
Simmer	Simmer	k1gMnSc1	Simmer
Joe	Joe	k1gMnSc1	Joe
Tim	Tim	k?	Tim
Sancha	Sancha	k1gMnSc1	Sancha
Dom	Dom	k?	Dom
The	The	k1gMnSc1	The
EP	EP	kA	EP
U	u	k7c2	u
Make	Make	k1gNnSc2	Make
Me	Me	k1gFnSc2	Me
Feel	Feel	k1gMnSc1	Feel
So	So	kA	So
Good	Good	k1gMnSc1	Good
Breach	Breach	k1gMnSc1	Breach
The	The	k1gMnSc1	The
Peace	Peace	k1gMnSc1	Peace
Forward	Forward	k1gMnSc1	Forward
The	The	k1gFnPc2	The
Revolution	Revolution	k1gInSc1	Revolution
Forward	Forward	k1gMnSc1	Forward
The	The	k1gFnPc2	The
Revolution	Revolution	k1gInSc1	Revolution
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Youth	Youth	k1gMnSc1	Youth
Remix	Remix	k1gInSc1	Remix
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Spiral	Spiral	k1gMnSc5	Spiral
Tribe	Trib	k1gMnSc5	Trib
EP	EP	kA	EP
Sirius	Sirius	k1gInSc4	Sirius
23	[number]	k4	23
Spiral	Spiral	k1gFnPc2	Spiral
Tribe	Trib	k1gInSc5	Trib
Sound	Sounda	k1gFnPc2	Sounda
System	Syst	k1gInSc7	Syst
(	(	kIx(	(
<g/>
The	The	k1gMnPc1	The
Album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
Tecno	Tecno	k6eAd1	Tecno
Terra	Terra	k1gMnSc1	Terra
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Take	Take	k1gFnSc1	Take
The	The	k1gFnSc2	The
Piss	Pissa	k1gFnPc2	Pissa
Definitely	Definitela	k1gFnSc2	Definitela
Taking	Taking	k1gInSc1	Taking
Drugs	Drugs	k1gInSc1	Drugs
Expekt	Expekt	k1gInSc1	Expekt
The	The	k1gFnSc1	The
Unxpekted	Unxpekted	k1gMnSc1	Unxpekted
SP	SP	kA	SP
23	[number]	k4	23
Panasonic	Panasonic	kA	Panasonic
Power	Power	k1gInSc1	Power
House	house	k1gNnSc1	house
Power	Powra	k1gFnPc2	Powra
House	house	k1gNnSc1	house
02	[number]	k4	02
Probably	Probably	k1gMnPc2	Probably
Taking	Taking	k1gInSc4	Taking
Drugs	Drugsa	k1gFnPc2	Drugsa
Spiral	Spiral	k1gMnSc1	Spiral
Tribe	Trib	k1gInSc5	Trib
1	[number]	k4	1
Spiral	Spiral	k1gMnSc1	Spiral
Tribe	Trib	k1gInSc5	Trib
2	[number]	k4	2
Spiral	Spiral	k1gMnSc1	Spiral
Tribe	Trib	k1gInSc5	Trib
3	[number]	k4	3
Spiral	Spiral	k1gMnSc1	Spiral
Tribe	Trib	k1gInSc5	Trib
4	[number]	k4	4
Spiral	Spiral	k1gMnSc1	Spiral
Tribe	Trib	k1gInSc5	Trib
5	[number]	k4	5
Full	Full	k1gMnSc1	Full
Fill	Fill	k1gMnSc1	Fill
Fromage	Fromage	k1gInSc4	Fromage
Strange	Strang	k1gInSc2	Strang
Breaks	Breaks	k1gInSc1	Breaks
Fac	Fac	k1gFnSc1	Fac
<g/>
'	'	kIx"	'
<g/>
em	em	k?	em
If	If	k1gMnPc4	If
They	Thea	k1gFnSc2	Thea
Can	Can	k1gFnSc2	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Tek	teka	k1gFnPc2	teka
A	a	k8xC	a
Joke	Joke	k1gFnPc2	Joke
Freetekno	Freetekno	k1gNnSc1	Freetekno
Teknival	Teknival	k1gMnSc1	Teknival
CzechTek	CzechTek	k1gMnSc1	CzechTek
Sound	Sound	k1gMnSc1	Sound
system	syst	k1gMnSc7	syst
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
Spiral	Spiral	k1gMnSc5	Spiral
Tribe	Trib	k1gMnSc5	Trib
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Manifest	manifest	k1gInSc1	manifest
</s>
