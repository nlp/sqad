<p>
<s>
Přirozeným	přirozený	k2eAgNnSc7d1	přirozené
číslem	číslo	k1gNnSc7	číslo
(	(	kIx(	(
<g/>
číslem	číslo	k1gNnSc7	číslo
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
obvykle	obvykle	k6eAd1	obvykle
rozumí	rozumět	k5eAaImIp3nS	rozumět
nezáporné	záporný	k2eNgNnSc4d1	nezáporné
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
mohutnosti	mohutnost	k1gFnSc2	mohutnost
(	(	kIx(	(
<g/>
konečné	konečný	k2eAgFnSc2d1	konečná
<g/>
)	)	kIx)	)
množiny	množina	k1gFnSc2	množina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
kardinální	kardinální	k2eAgNnSc4d1	kardinální
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
počtu	počet	k1gInSc2	počet
nějakých	nějaký	k3yIgInPc2	nějaký
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
nula	nula	k1gFnSc1	nula
mezi	mezi	k7c4	mezi
přirozená	přirozený	k2eAgNnPc4d1	přirozené
čísla	číslo	k1gNnPc4	číslo
nepočítala	počítat	k5eNaImAgFnS	počítat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
použití	použití	k1gNnSc2	použití
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
pro	pro	k7c4	pro
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
pořadí	pořadí	k1gNnSc2	pořadí
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
ordinální	ordinální	k2eAgNnSc4d1	ordinální
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přirozená	přirozený	k2eAgNnPc1d1	přirozené
čísla	číslo	k1gNnPc1	číslo
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgInPc4d1	základní
matematické	matematický	k2eAgInPc4d1	matematický
koncepty	koncept	k1gInPc4	koncept
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
nejjednodušší	jednoduchý	k2eAgInSc4d3	nejjednodušší
na	na	k7c4	na
pochopení	pochopení	k1gNnSc4	pochopení
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
výuka	výuka	k1gFnSc1	výuka
matematiky	matematika	k1gFnSc2	matematika
obvykle	obvykle	k6eAd1	obvykle
od	od	k7c2	od
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Značení	značení	k1gNnSc2	značení
==	==	k?	==
</s>
</p>
<p>
<s>
Množina	množina	k1gFnSc1	množina
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
N	N	kA	N
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
zdvojeným	zdvojený	k2eAgNnSc7d1	zdvojené
písmenem	písmeno	k1gNnSc7	písmeno
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
touto	tento	k3xDgFnSc7	tento
značkou	značka	k1gFnSc7	značka
označují	označovat	k5eAaImIp3nP	označovat
kladná	kladný	k2eAgNnPc1d1	kladné
celá	celý	k2eAgNnPc1d1	celé
čísla	číslo	k1gNnPc1	číslo
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
nezáporná	záporný	k2eNgNnPc1d1	nezáporné
celá	celý	k2eAgNnPc1d1	celé
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
také	také	k9	také
značení	značení	k1gNnSc1	značení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tuto	tento	k3xDgFnSc4	tento
nejednoznačnost	nejednoznačnost	k1gFnSc4	nejednoznačnost
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
nezáporná	záporný	k2eNgNnPc4d1	nezáporné
celá	celý	k2eAgNnPc4d1	celé
čísla	číslo	k1gNnPc4	číslo
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
nuly	nula	k1gFnSc2	nula
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
N	N	kA	N
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
případně	případně	k6eAd1	případně
N	N	kA	N
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
Z	z	k7c2	z
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
kladná	kladný	k2eAgNnPc4d1	kladné
celá	celý	k2eAgNnPc4d1	celé
čísla	číslo	k1gNnPc4	číslo
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
nuly	nula	k1gFnSc2	nula
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
N	N	kA	N
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
Z	z	k7c2	z
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Formální	formální	k2eAgFnPc1d1	formální
definice	definice	k1gFnPc1	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Exaktní	exaktní	k2eAgFnPc1d1	exaktní
matematické	matematický	k2eAgFnPc1d1	matematická
definice	definice	k1gFnPc1	definice
množiny	množina	k1gFnSc2	množina
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
následujících	následující	k2eAgInPc6d1	následující
axiomech	axiom	k1gInPc6	axiom
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Peanova	Peanův	k2eAgFnSc1d1	Peanova
aritmetika	aritmetika	k1gFnSc1	aritmetika
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
číslo	číslo	k1gNnSc1	číslo
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každé	každý	k3xTgNnSc4	každý
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
následníka	následník	k1gMnSc2	následník
<g/>
,	,	kIx,	,
označeného	označený	k2eAgInSc2d1	označený
jako	jako	k8xS	jako
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
následníkem	následník	k1gMnSc7	následník
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Různá	různý	k2eAgNnPc1d1	různé
přirozená	přirozený	k2eAgNnPc1d1	přirozené
čísla	číslo	k1gNnPc1	číslo
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgMnPc4d1	různý
následníky	následník	k1gMnPc4	následník
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
a	a	k8xC	a
≠	≠	k?	≠
b	b	k?	b
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
≠	≠	k?	≠
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
nějakou	nějaký	k3yIgFnSc4	nějaký
vlastnost	vlastnost	k1gFnSc4	vlastnost
splňuje	splňovat	k5eAaImIp3nS	splňovat
jak	jak	k8xC	jak
číslo	číslo	k1gNnSc1	číslo
0	[number]	k4	0
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
každé	každý	k3xTgNnSc4	každý
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
následníkem	následník	k1gMnSc7	následník
nějakého	nějaký	k3yIgNnSc2	nějaký
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
splňuje	splňovat	k5eAaImIp3nS	splňovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
splňují	splňovat	k5eAaImIp3nP	splňovat
všechna	všechen	k3xTgNnPc1	všechen
přirozená	přirozený	k2eAgNnPc1d1	přirozené
čísla	číslo	k1gNnPc1	číslo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tento	tento	k3xDgInSc1	tento
axiom	axiom	k1gInSc1	axiom
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
platnost	platnost	k1gFnSc4	platnost
důkazů	důkaz	k1gInPc2	důkaz
technikou	technika	k1gFnSc7	technika
matematické	matematický	k2eAgFnSc2d1	matematická
indukce	indukce	k1gFnSc2	indukce
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Číslo	číslo	k1gNnSc1	číslo
0	[number]	k4	0
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
postulátech	postulát	k1gInPc6	postulát
nemusí	muset	k5eNaImIp3nS	muset
odpovídat	odpovídat	k5eAaImF	odpovídat
běžnému	běžný	k2eAgInSc3d1	běžný
výkladu	výklad	k1gInSc3	výklad
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
čísla	číslo	k1gNnSc2	číslo
nula	nula	k1gFnSc1	nula
<g/>
.	.	kIx.	.
0	[number]	k4	0
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
formální	formální	k2eAgFnSc6d1	formální
definici	definice	k1gFnSc6	definice
znamená	znamenat	k5eAaImIp3nS	znamenat
pouze	pouze	k6eAd1	pouze
nějaký	nějaký	k3yIgInSc4	nějaký
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
následnosti	následnost	k1gFnSc2	následnost
splňuje	splňovat	k5eAaImIp3nS	splňovat
Peanovy	Peanův	k2eAgInPc4d1	Peanův
axiomy	axiom	k1gInPc4	axiom
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
konstrukcí	konstrukce	k1gFnSc7	konstrukce
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
v	v	k7c6	v
axiomatické	axiomatický	k2eAgFnSc6d1	axiomatická
teorii	teorie	k1gFnSc6	teorie
množin	množina	k1gFnPc2	množina
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
postup	postup	k1gInSc1	postup
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Definujeme	definovat	k5eAaBmIp1nP	definovat
0	[number]	k4	0
=	=	kIx~	=
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Definujeme	definovat	k5eAaBmIp1nP	definovat
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
=	=	kIx~	=
a	a	k8xC	a
∪	∪	k?	∪
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
a.	a.	k?	a.
</s>
</p>
<p>
<s>
Množinu	množina	k1gFnSc4	množina
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
pak	pak	k6eAd1	pak
definujeme	definovat	k5eAaBmIp1nP	definovat
jako	jako	k8xS	jako
průnik	průnik	k1gInSc1	průnik
všech	všecek	k3xTgFnPc2	všecek
množin	množina	k1gFnPc2	množina
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
0	[number]	k4	0
a	a	k8xC	a
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
vůči	vůči	k7c3	vůči
funkci	funkce	k1gFnSc3	funkce
následnosti	následnost	k1gFnSc2	následnost
<g/>
.	.	kIx.	.
<g/>
Pomocí	pomocí	k7c2	pomocí
axiomu	axiom	k1gInSc2	axiom
nekonečna	nekonečno	k1gNnSc2	nekonečno
lze	lze	k6eAd1	lze
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
splňuje	splňovat	k5eAaImIp3nS	splňovat
Peanovy	Peanův	k2eAgInPc4d1	Peanův
axiomy	axiom	k1gInPc4	axiom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
definici	definice	k1gFnSc6	definice
je	být	k5eAaImIp3nS	být
každé	každý	k3xTgNnSc1	každý
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
množinou	množina	k1gFnSc7	množina
čísel	číslo	k1gNnPc2	číslo
menších	malý	k2eAgFnPc2d2	menší
než	než	k8xS	než
ono	onen	k3xDgNnSc1	onen
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
0	[number]	k4	0
=	=	kIx~	=
{	{	kIx(	{
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
1	[number]	k4	1
=	=	kIx~	=
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
=	=	kIx~	=
{{	{{	k?	{{
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
2	[number]	k4	2
=	=	kIx~	=
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
}	}	kIx)	}
=	=	kIx~	=
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
=	=	kIx~	=
{{	{{	k?	{{
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
{{	{{	k?	{{
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
3	[number]	k4	3
=	=	kIx~	=
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
}	}	kIx)	}
=	=	kIx~	=
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
=	=	kIx~	=
{{	{{	k?	{{
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
{{	{{	k?	{{
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
{{	{{	k?	{{
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
{{	{{	k?	{{
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
...	...	k?	...
<g/>
atd.	atd.	kA	atd.
<g/>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnPc1	definice
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
s	s	k7c7	s
intuitivním	intuitivní	k2eAgNnSc7d1	intuitivní
pojetím	pojetí	k1gNnSc7	pojetí
<g/>
,	,	kIx,	,
že	že	k8xS	že
každé	každý	k3xTgNnSc1	každý
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
n	n	k0	n
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
mohutnost	mohutnost	k1gFnSc4	mohutnost
množiny	množina	k1gFnSc2	množina
o	o	k7c4	o
právě	právě	k6eAd1	právě
n	n	k0	n
prvcích	prvek	k1gInPc6	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Množina	množina	k1gFnSc1	množina
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
(	(	kIx(	(
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c4	mnoho
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
spočetná	spočetný	k2eAgFnSc1d1	spočetná
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přirozených	přirozený	k2eAgNnPc6d1	přirozené
číslech	číslo	k1gNnPc6	číslo
můžeme	moct	k5eAaImIp1nP	moct
definovat	definovat	k5eAaBmF	definovat
operaci	operace	k1gFnSc4	operace
sčítání	sčítání	k1gNnSc2	sčítání
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
a	a	k8xC	a
+	+	kIx~	+
0	[number]	k4	0
=	=	kIx~	=
a	a	k8xC	a
<g/>
,	,	kIx,	,
a	a	k8xC	a
+	+	kIx~	+
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
=	=	kIx~	=
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
a	a	k8xC	a
<g/>
,	,	kIx,	,
b.	b.	k?	b.
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
(	(	kIx(	(
<g/>
N	n	k0	n
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
)	)	kIx)	)
komutativním	komutativní	k2eAgInSc7d1	komutativní
monoidem	monoid	k1gInSc7	monoid
s	s	k7c7	s
neutrálním	neutrální	k2eAgInSc7d1	neutrální
prvkem	prvek	k1gInSc7	prvek
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
definujeme	definovat	k5eAaBmIp1nP	definovat
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
=	=	kIx~	=
1	[number]	k4	1
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
=	=	kIx~	=
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
+	+	kIx~	+
0	[number]	k4	0
<g/>
)	)	kIx)	)
=	=	kIx~	=
a	a	k8xC	a
+	+	kIx~	+
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
=	=	kIx~	=
a	a	k8xC	a
+	+	kIx~	+
1	[number]	k4	1
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
následníkem	následník	k1gMnSc7	následník
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
+	+	kIx~	+
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
monoid	monoid	k1gInSc1	monoid
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vnořit	vnořit	k5eAaPmF	vnořit
do	do	k7c2	do
grupy	grupa	k1gFnSc2	grupa
<g/>
;	;	kIx,	;
nejmenší	malý	k2eAgFnSc7d3	nejmenší
grupou	grupa	k1gFnSc7	grupa
obsahující	obsahující	k2eAgNnPc1d1	obsahující
přirozená	přirozený	k2eAgNnPc1d1	přirozené
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
celá	celý	k2eAgNnPc1d1	celé
čísla	číslo	k1gNnPc1	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdobně	obdobně	k6eAd1	obdobně
můžeme	moct	k5eAaImIp1nP	moct
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
operace	operace	k1gFnSc2	operace
sčítání	sčítání	k1gNnSc2	sčítání
definovat	definovat	k5eAaBmF	definovat
operaci	operace	k1gFnSc4	operace
násobení	násobení	k1gNnSc2	násobení
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
a	a	k8xC	a
*	*	kIx~	*
0	[number]	k4	0
=	=	kIx~	=
0	[number]	k4	0
<g/>
,	,	kIx,	,
a	a	k8xC	a
*	*	kIx~	*
(	(	kIx(	(
<g/>
b	b	k?	b
+	+	kIx~	+
1	[number]	k4	1
<g/>
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
<g/>
a	a	k8xC	a
*	*	kIx~	*
b	b	k?	b
<g/>
)	)	kIx)	)
+	+	kIx~	+
a.	a.	k?	a.
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
(	(	kIx(	(
<g/>
N	n	k0	n
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
)	)	kIx)	)
komutativním	komutativní	k2eAgInSc7d1	komutativní
monoidem	monoid	k1gInSc7	monoid
s	s	k7c7	s
neutrálním	neutrální	k2eAgInSc7d1	neutrální
prvkem	prvek	k1gInSc7	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
a	a	k8xC	a
násobení	násobení	k1gNnSc1	násobení
splňují	splňovat	k5eAaImIp3nP	splňovat
distributivní	distributivní	k2eAgInSc4d1	distributivní
zákon	zákon	k1gInSc4	zákon
<g/>
:	:	kIx,	:
a	a	k8xC	a
*	*	kIx~	*
(	(	kIx(	(
<g/>
b	b	k?	b
+	+	kIx~	+
c	c	k0	c
<g/>
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
<g/>
a	a	k8xC	a
*	*	kIx~	*
b	b	k?	b
<g/>
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
<g/>
a	a	k8xC	a
*	*	kIx~	*
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
komutativním	komutativní	k2eAgInSc7d1	komutativní
polookruhem	polookruh	k1gInSc7	polookruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přirozených	přirozený	k2eAgNnPc6d1	přirozené
číslech	číslo	k1gNnPc6	číslo
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
úplné	úplný	k2eAgNnSc4d1	úplné
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
≤	≤	k?	≤
b	b	k?	b
právě	právě	k6eAd1	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
existuje	existovat	k5eAaImIp3nS	existovat
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
c	c	k0	c
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
a	a	k8xC	a
+	+	kIx~	+
c	c	k0	c
=	=	kIx~	=
b.	b.	k?	b.
Přirozená	přirozený	k2eAgNnPc1d1	přirozené
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
každá	každý	k3xTgFnSc1	každý
neprázdná	prázdný	k2eNgFnSc1d1	neprázdná
množina	množina	k1gFnSc1	množina
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
má	mít	k5eAaImIp3nS	mít
nejmenší	malý	k2eAgInSc4d3	nejmenší
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přirozených	přirozený	k2eAgNnPc6d1	přirozené
číslech	číslo	k1gNnPc6	číslo
neexistuje	existovat	k5eNaImIp3nS	existovat
operace	operace	k1gFnSc1	operace
dělení	dělení	k1gNnSc2	dělení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podíl	podíl	k1gInSc1	podíl
dvou	dva	k4xCgNnPc2	dva
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
obecně	obecně	k6eAd1	obecně
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Alternativou	alternativa	k1gFnSc7	alternativa
je	být	k5eAaImIp3nS	být
tady	tady	k6eAd1	tady
dělení	dělení	k1gNnSc1	dělení
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
libovolná	libovolný	k2eAgNnPc4d1	libovolné
dvě	dva	k4xCgNnPc4	dva
přirozená	přirozený	k2eAgNnPc4d1	přirozené
čísla	číslo	k1gNnPc4	číslo
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
b	b	k?	b
≠	≠	k?	≠
0	[number]	k4	0
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
taková	takový	k3xDgNnPc1	takový
přirozená	přirozený	k2eAgNnPc1d1	přirozené
čísla	číslo	k1gNnPc1	číslo
r	r	kA	r
a	a	k8xC	a
q	q	k?	q
<g/>
,	,	kIx,	,
že	že	k8xS	že
platí	platit	k5eAaImIp3nS	platit
a	a	k8xC	a
=	=	kIx~	=
bq	bq	k?	bq
+	+	kIx~	+
r	r	kA	r
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
r	r	kA	r
<	<	kIx(	<
b.	b.	k?	b.
Číslu	číslo	k1gNnSc3	číslo
r	r	kA	r
pak	pak	k8xC	pak
říkáme	říkat	k5eAaImIp1nP	říkat
zbytek	zbytek	k1gInSc4	zbytek
po	po	k7c4	po
dělení	dělení	k1gNnSc4	dělení
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
číslem	číslo	k1gNnSc7	číslo
b	b	k?	b
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
q	q	k?	q
je	být	k5eAaImIp3nS	být
celočíselný	celočíselný	k2eAgInSc4d1	celočíselný
podíl	podíl	k1gInSc4	podíl
a	a	k8xC	a
a	a	k8xC	a
b.	b.	k?	b.
Tato	tento	k3xDgFnSc1	tento
operace	operace	k1gFnSc1	operace
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
mnoha	mnoho	k4c2	mnoho
vlastností	vlastnost	k1gFnPc2	vlastnost
(	(	kIx(	(
<g/>
dělitelnost	dělitelnost	k1gFnSc1	dělitelnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postupů	postup	k1gInPc2	postup
(	(	kIx(	(
<g/>
Euklidův	Euklidův	k2eAgInSc1d1	Euklidův
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
)	)	kIx)	)
a	a	k8xC	a
idejí	idea	k1gFnPc2	idea
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
existenci	existence	k1gFnSc6	existence
a	a	k8xC	a
vlastnostech	vlastnost	k1gFnPc6	vlastnost
zbytků	zbytek	k1gInPc2	zbytek
po	po	k7c4	po
dělení	dělení	k1gNnSc4	dělení
v	v	k7c6	v
přirozených	přirozený	k2eAgNnPc6d1	přirozené
číslech	číslo	k1gNnPc6	číslo
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
jedna	jeden	k4xCgFnSc1	jeden
část	část	k1gFnSc1	část
kryptografie	kryptografie	k1gFnSc2	kryptografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
přirozené	přirozený	k2eAgFnSc2d1	přirozená
číslo	číslo	k1gNnSc4	číslo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
http://www.stetson.edu/~efriedma/numbers.html	[url]	k1gMnSc1	http://www.stetson.edu/~efriedma/numbers.html
</s>
</p>
