<s>
Přirozená	přirozený	k2eAgNnPc1d1	přirozené
čísla	číslo	k1gNnPc1	číslo
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgInPc4d1	základní
matematické	matematický	k2eAgInPc4d1	matematický
koncepty	koncept	k1gInPc4	koncept
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
nejjednodušší	jednoduchý	k2eAgInSc4d3	nejjednodušší
na	na	k7c4	na
pochopení	pochopení	k1gNnSc4	pochopení
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
výuka	výuka	k1gFnSc1	výuka
matematiky	matematika	k1gFnSc2	matematika
obvykle	obvykle	k6eAd1	obvykle
od	od	k7c2	od
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
