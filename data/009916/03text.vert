<p>
<s>
Orientační	orientační	k2eAgNnSc1d1	orientační
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
č.	č.	k?	č.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
č.	č.	k?	č.
or	or	k?	or
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
čo	čo	k?	čo
<g/>
.	.	kIx.	.
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
nové	nový	k2eAgNnSc1d1	nové
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
republice	republika	k1gFnSc6	republika
doplňkové	doplňkový	k2eAgNnSc4d1	doplňkové
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
obcích	obec	k1gFnPc6	obec
s	s	k7c7	s
pojmenovanými	pojmenovaný	k2eAgFnPc7d1	pojmenovaná
ulicemi	ulice	k1gFnPc7	ulice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yQgNnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
adresu	adresa	k1gFnSc4	adresa
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
alternativou	alternativa	k1gFnSc7	alternativa
k	k	k7c3	k
domovnímu	domovní	k2eAgNnSc3d1	domovní
číslu	číslo	k1gNnSc3	číslo
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
číslu	číslo	k1gNnSc3	číslo
popisnému	popisný	k2eAgNnSc3d1	popisné
nebo	nebo	k8xC	nebo
evidenčnímu	evidenční	k2eAgNnSc3d1	evidenční
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvojité	dvojitý	k2eAgNnSc1d1	dvojité
číslování	číslování	k1gNnSc1	číslování
domů	dům	k1gInPc2	dům
je	být	k5eAaImIp3nS	být
československým	československý	k2eAgNnSc7d1	Československé
specifikem	specifikon	k1gNnSc7	specifikon
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
mají	mít	k5eAaImIp3nP	mít
domy	dům	k1gInPc1	dům
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
číslování	číslování	k1gNnSc1	číslování
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
obdobné	obdobný	k2eAgFnPc4d1	obdobná
českým	český	k2eAgFnPc3d1	Česká
orientačním	orientační	k2eAgNnPc3d1	orientační
číslům	číslo	k1gNnPc3	číslo
<g/>
,	,	kIx,	,
a	a	k8xC	a
souběžné	souběžný	k2eAgNnSc4d1	souběžné
dvojí	dvojí	k4xRgNnSc4	dvojí
číslování	číslování	k1gNnSc4	číslování
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
rakouských	rakouský	k2eAgNnPc6d1	rakouské
městech	město	k1gNnPc6	město
pouze	pouze	k6eAd1	pouze
přechodnou	přechodný	k2eAgFnSc7d1	přechodná
fází	fáze	k1gFnSc7	fáze
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
číslování	číslování	k1gNnSc2	číslování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Orientační	orientační	k2eAgFnSc4d1	orientační
roli	role	k1gFnSc4	role
zpočátku	zpočátku	k6eAd1	zpočátku
plnila	plnit	k5eAaImAgNnP	plnit
popisná	popisný	k2eAgNnPc1d1	popisné
čísla	číslo	k1gNnPc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
městech	město	k1gNnPc6	město
domy	dům	k1gInPc4	dům
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
přečíslovány	přečíslován	k2eAgFnPc1d1	přečíslována
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
císařského	císařský	k2eAgInSc2d1	císařský
výnosu	výnos	k1gInSc2	výnos
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1857	[number]	k4	1857
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zaveden	zavést	k5eAaPmNgInS	zavést
systém	systém	k1gInSc1	systém
číslování	číslování	k1gNnSc2	číslování
podle	podle	k7c2	podle
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
dnešním	dnešní	k2eAgNnPc3d1	dnešní
orientačním	orientační	k2eAgNnPc3d1	orientační
číslům	číslo	k1gNnPc3	číslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
i	i	k8xC	i
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byl	být	k5eAaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
asi	asi	k9	asi
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Zavedením	zavedení	k1gNnSc7	zavedení
pozemkových	pozemkový	k2eAgFnPc2d1	pozemková
knih	kniha	k1gFnPc2	kniha
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
se	se	k3xPyFc4	se
popisná	popisný	k2eAgNnPc1d1	popisné
čísla	číslo	k1gNnPc1	číslo
stabilizovala	stabilizovat	k5eAaBmAgNnP	stabilizovat
a	a	k8xC	a
orientační	orientační	k2eAgFnSc1d1	orientační
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
ve	v	k7c6	v
městech	město	k1gNnPc6	město
souběžně	souběžně	k6eAd1	souběžně
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
266	[number]	k4	266
<g/>
/	/	kIx~	/
<g/>
1920	[number]	k4	1920
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
názvech	název	k1gInPc6	název
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
osad	osada	k1gFnPc2	osada
a	a	k8xC	a
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
označování	označování	k1gNnSc1	označování
obcí	obec	k1gFnPc2	obec
místními	místní	k2eAgFnPc7d1	místní
tabulkami	tabulka	k1gFnPc7	tabulka
a	a	k8xC	a
číslování	číslování	k1gNnSc1	číslování
domů	dům	k1gInPc2	dům
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
případné	případný	k2eAgFnSc6d1	případná
nutnosti	nutnost	k1gFnSc6	nutnost
přečíslování	přečíslování	k1gNnSc2	přečíslování
celého	celý	k2eAgNnSc2d1	celé
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
uvádí	uvádět	k5eAaImIp3nS	uvádět
"	"	kIx"	"
<g/>
V	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
v	v	k7c6	v
rozsáhlejších	rozsáhlý	k2eAgFnPc6d2	rozsáhlejší
obcích	obec	k1gFnPc6	obec
může	moct	k5eAaImIp3nS	moct
býti	být	k5eAaImF	být
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
číslováno	číslován	k2eAgNnSc1d1	číslováno
podle	podle	k7c2	podle
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
číslování	číslování	k1gNnSc2	číslování
nebyly	být	k5eNaImAgInP	být
žádnými	žádný	k3yNgFnPc7	žádný
termíny	termín	k1gInPc1	termín
odlišeny	odlišit	k5eAaPmNgInP	odlišit
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
orientačním	orientační	k2eAgNnPc3d1	orientační
číslům	číslo	k1gNnPc3	číslo
někdy	někdy	k6eAd1	někdy
říkalo	říkat	k5eAaImAgNnS	říkat
nová	nový	k2eAgNnPc1d1	nové
čísla	číslo	k1gNnPc1	číslo
<g/>
.	.	kIx.	.
<g/>
Toto	tento	k3xDgNnSc1	tento
zdvojené	zdvojený	k2eAgNnSc1d1	zdvojené
číslování	číslování	k1gNnSc1	číslování
domů	dům	k1gInPc2	dům
je	být	k5eAaImIp3nS	být
specifikem	specifikon	k1gNnSc7	specifikon
některých	některý	k3yIgFnPc2	některý
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgNnPc2d1	slovenské
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Evropy	Evropa	k1gFnSc2	Evropa
mají	mít	k5eAaImIp3nP	mít
domy	dům	k1gInPc1	dům
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
číslování	číslování	k1gNnSc1	číslování
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
obdobné	obdobný	k2eAgFnPc4d1	obdobná
českým	český	k2eAgFnPc3d1	Česká
orientačním	orientační	k2eAgNnPc3d1	orientační
číslům	číslo	k1gNnPc3	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Tabulky	tabulka	k1gFnPc1	tabulka
s	s	k7c7	s
dvojím	dvojí	k4xRgNnSc7	dvojí
číslováním	číslování	k1gNnSc7	číslování
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
rakouských	rakouský	k2eAgNnPc6d1	rakouské
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tam	tam	k6eAd1	tam
šlo	jít	k5eAaImAgNnS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
přechodné	přechodný	k2eAgNnSc4d1	přechodné
značení	značení	k1gNnSc4	značení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
změny	změna	k1gFnSc2	změna
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
číslování	číslování	k1gNnSc2	číslování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celostátní	celostátní	k2eAgNnPc1d1	celostátní
pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
pojmenovávání	pojmenovávání	k1gNnSc4	pojmenovávání
ulic	ulice	k1gFnPc2	ulice
i	i	k9	i
číslování	číslování	k1gNnSc1	číslování
domů	dům	k1gInPc2	dům
stanoví	stanovit	k5eAaPmIp3nS	stanovit
vyhláška	vyhláška	k1gFnSc1	vyhláška
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
č.	č.	k?	č.
326	[number]	k4	326
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
prováděcí	prováděcí	k2eAgFnSc1d1	prováděcí
vyhláška	vyhláška	k1gFnSc1	vyhláška
k	k	k7c3	k
zákonu	zákon	k1gInSc3	zákon
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
a	a	k8xC	a
zákonu	zákon	k1gInSc6	zákon
o	o	k7c6	o
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgFnSc7d1	předchozí
vyhláškou	vyhláška	k1gFnSc7	vyhláška
upravující	upravující	k2eAgFnSc4d1	upravující
tuto	tento	k3xDgFnSc4	tento
tematiku	tematika	k1gFnSc4	tematika
byla	být	k5eAaImAgFnS	být
vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
97	[number]	k4	97
<g/>
/	/	kIx~	/
<g/>
1961	[number]	k4	1961
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
názvech	název	k1gInPc6	název
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
označování	označování	k1gNnSc1	označování
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
číslování	číslování	k1gNnSc1	číslování
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Systémy	systém	k1gInPc1	systém
číslování	číslování	k1gNnSc1	číslování
==	==	k?	==
</s>
</p>
<p>
<s>
Orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
ani	ani	k8xC	ani
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
republice	republika	k1gFnSc6	republika
celostátně	celostátně	k6eAd1	celostátně
povinná	povinný	k2eAgFnSc1d1	povinná
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
označovat	označovat	k5eAaImF	označovat
pořadí	pořadí	k1gNnSc4	pořadí
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ulice	ulice	k1gFnSc2	ulice
nebo	nebo	k8xC	nebo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
použijí	použít	k5eAaPmIp3nP	použít
v	v	k7c6	v
části	část	k1gFnSc6	část
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
plně	plně	k6eAd1	plně
zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
<g/>
-li	i	k?	-li
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
již	již	k9	již
očíslovanými	očíslovaný	k2eAgInPc7d1	očíslovaný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozlišení	rozlišení	k1gNnSc3	rozlišení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
číslo	číslo	k1gNnSc1	číslo
doplněno	doplnit	k5eAaPmNgNnS	doplnit
jedním	jeden	k4xCgInSc7	jeden
písmenem	písmeno	k1gNnSc7	písmeno
české	český	k2eAgFnSc2d1	Česká
abecedy	abeceda	k1gFnSc2	abeceda
bez	bez	k7c2	bez
diakritických	diakritický	k2eAgNnPc2d1	diakritické
znamének	znaménko	k1gNnPc2	znaménko
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
obcích	obec	k1gFnPc6	obec
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
malé	malý	k2eAgNnSc1d1	malé
písmeno	písmeno	k1gNnSc1	písmeno
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
velké	velký	k2eAgNnSc1d1	velké
písmeno	písmeno	k1gNnSc1	písmeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
od	od	k7c2	od
středu	střed	k1gInSc2	střed
obce	obec	k1gFnSc2	obec
k	k	k7c3	k
okrajům	okraj	k1gInPc3	okraj
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
ulice	ulice	k1gFnSc2	ulice
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
konci	konec	k1gInSc3	konec
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
<g/>
-li	i	k?	-li
takovéto	takovýto	k3xDgInPc4	takovýto
směry	směr	k1gInPc4	směr
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
lichá	lichý	k2eAgFnSc1d1	lichá
vlevo	vlevo	k6eAd1	vlevo
a	a	k8xC	a
sudá	sudý	k2eAgNnPc4d1	sudé
vpravo	vpravo	k6eAd1	vpravo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
klasického	klasický	k2eAgInSc2d1	klasický
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
i	i	k8xC	i
na	na	k7c6	na
nábřežích	nábřeží	k1gNnPc6	nábřeží
čísla	číslo	k1gNnSc2	číslo
následují	následovat	k5eAaImIp3nP	následovat
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
<g/>
,	,	kIx,	,
sudá	sudý	k2eAgFnSc1d1	sudá
i	i	k8xC	i
lichá	lichý	k2eAgFnSc1d1	lichá
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
přidělovat	přidělovat	k5eAaImF	přidělovat
i	i	k9	i
nárožním	nárožní	k2eAgInPc3d1	nárožní
domům	dům	k1gInPc3	dům
nebo	nebo	k8xC	nebo
domům	dům	k1gInPc3	dům
s	s	k7c7	s
ulicemi	ulice	k1gFnPc7	ulice
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc4	jeden
orientační	orientační	k2eAgNnSc4d1	orientační
číslo	číslo	k1gNnSc4	číslo
podle	podle	k7c2	podle
hlavního	hlavní	k2eAgInSc2d1	hlavní
vchodu	vchod	k1gInSc2	vchod
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
mívá	mívat	k5eAaImIp3nS	mívat
jeden	jeden	k4xCgInSc4	jeden
dům	dům	k1gInSc4	dům
často	často	k6eAd1	často
k	k	k7c3	k
jednomu	jeden	k4xCgNnSc3	jeden
popisnému	popisný	k2eAgNnSc3d1	popisné
číslu	číslo	k1gNnSc3	číslo
orientační	orientační	k2eAgFnPc1d1	orientační
čísla	číslo	k1gNnSc2	číslo
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
z	z	k7c2	z
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
ulic	ulice	k1gFnPc2	ulice
či	či	k8xC	či
náměstí	náměstí	k1gNnSc2	náměstí
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnSc1d2	veliký
budova	budova	k1gFnSc1	budova
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
číslo	číslo	k1gNnSc1	číslo
popisné	popisný	k2eAgInPc1d1	popisný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
vchody	vchod	k1gInPc1	vchod
či	či	k8xC	či
sekce	sekce	k1gFnPc1	sekce
budovy	budova	k1gFnSc2	budova
mají	mít	k5eAaImIp3nP	mít
samostatná	samostatný	k2eAgNnPc4d1	samostatné
čísla	číslo	k1gNnPc4	číslo
orientační	orientační	k2eAgFnPc4d1	orientační
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
dříve	dříve	k6eAd2	dříve
používala	používat	k5eAaImAgNnP	používat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
tabulky	tabulka	k1gFnPc1	tabulka
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
domech	dům	k1gInPc6	dům
fyzicky	fyzicky	k6eAd1	fyzicky
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
nesystematicky	systematicky	k6eNd1	systematicky
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
starší	starý	k2eAgInPc1d2	starší
domy	dům	k1gInPc1	dům
je	on	k3xPp3gMnPc4	on
mají	mít	k5eAaImIp3nP	mít
přiděleny	přidělen	k2eAgFnPc1d1	přidělena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
novější	nový	k2eAgInSc4d2	novější
už	už	k6eAd1	už
ne	ne	k9	ne
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
přidělena	přidělit	k5eAaPmNgNnP	přidělit
jen	jen	k9	jen
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
ulice	ulice	k1gFnPc4	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
směr	směr	k1gInSc4	směr
číslování	číslování	k1gNnPc2	číslování
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
směry	směr	k1gInPc1	směr
určené	určený	k2eAgNnSc1d1	určené
proudem	proud	k1gInSc7	proud
Vltavy	Vltava	k1gFnSc2	Vltava
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
)	)	kIx)	)
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Vltavy	Vltava	k1gFnSc2	Vltava
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgFnPc1	tento
zásady	zásada	k1gFnPc1	zásada
nebyly	být	k5eNaImAgFnP	být
uplatňovány	uplatňovat	k5eAaImNgFnP	uplatňovat
všude	všude	k6eAd1	všude
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
číslování	číslování	k1gNnPc2	číslování
mají	mít	k5eAaImIp3nP	mít
lichá	lichý	k2eAgNnPc1d1	liché
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
sudá	sudý	k2eAgFnSc1d1	sudá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
klasického	klasický	k2eAgInSc2d1	klasický
tvaru	tvar	k1gInSc2	tvar
se	se	k3xPyFc4	se
čísla	číslo	k1gNnPc1	číslo
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
obvykle	obvykle	k6eAd1	obvykle
postupně	postupně	k6eAd1	postupně
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
náměstí	náměstí	k1gNnSc2	náměstí
po	po	k7c6	po
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
Dotýká	dotýkat	k5eAaImIp3nS	dotýkat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
pozemek	pozemek	k1gInSc1	pozemek
budovy	budova	k1gFnSc2	budova
více	hodně	k6eAd2	hodně
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
přiděluje	přidělovat	k5eAaImIp3nS	přidělovat
se	se	k3xPyFc4	se
budově	budova	k1gFnSc3	budova
obvykle	obvykle	k6eAd1	obvykle
orientační	orientační	k2eAgNnSc4d1	orientační
číslo	číslo	k1gNnSc4	číslo
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
pouze	pouze	k6eAd1	pouze
číslo	číslo	k1gNnSc1	číslo
příslušející	příslušející	k2eAgNnSc1d1	příslušející
k	k	k7c3	k
té	ten	k3xDgFnSc3	ten
ulici	ulice	k1gFnSc3	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
budova	budova	k1gFnSc1	budova
hlavní	hlavní	k2eAgFnSc1d1	hlavní
vchod	vchod	k1gInSc1	vchod
<g/>
.	.	kIx.	.
</s>
<s>
Orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
podle	podle	k7c2	podle
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
ulice	ulice	k1gFnSc2	ulice
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
i	i	k9	i
těm	ten	k3xDgFnPc3	ten
budovám	budova	k1gFnPc3	budova
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nestojí	stát	k5eNaImIp3nP	stát
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
vede	vést	k5eAaImIp3nS	vést
přístup	přístup	k1gInSc1	přístup
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
dvorem	dvůr	k1gInSc7	dvůr
<g/>
,	,	kIx,	,
bezejmennou	bezejmenný	k2eAgFnSc7d1	bezejmenná
účelovou	účelový	k2eAgFnSc7d1	účelová
komunikací	komunikace	k1gFnSc7	komunikace
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišovací	rozlišovací	k2eAgNnPc4d1	rozlišovací
písmena	písmeno	k1gNnPc4	písmeno
k	k	k7c3	k
číslu	číslo	k1gNnSc3	číslo
orientačnímu	orientační	k2eAgInSc3d1	orientační
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
takřka	takřka	k6eAd1	takřka
výhradně	výhradně	k6eAd1	výhradně
k	k	k7c3	k
očíslování	očíslování	k1gNnSc3	očíslování
nových	nový	k2eAgFnPc2d1	nová
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
dosavadních	dosavadní	k2eAgFnPc6d1	dosavadní
prolukách	proluka	k1gFnPc6	proluka
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
nově	nově	k6eAd1	nově
vestavěných	vestavěný	k2eAgFnPc2d1	vestavěná
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
budov	budova	k1gFnPc2	budova
dodatečně	dodatečně	k6eAd1	dodatečně
rozdělených	rozdělený	k2eAgInPc2d1	rozdělený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
okrajových	okrajový	k2eAgFnPc6d1	okrajová
čtvrtích	čtvrt	k1gFnPc6	čtvrt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Horních	horní	k2eAgInPc6d1	horní
Měcholupech	Měcholup	k1gInPc6	Měcholup
<g/>
,	,	kIx,	,
Šeberově	Šeberův	k2eAgFnSc3d1	Šeberova
a	a	k8xC	a
Hrnčířích	Hrnčíř	k1gMnPc6	Hrnčíř
nebo	nebo	k8xC	nebo
Letňanech	Letňan	k1gMnPc6	Letňan
<g/>
,	,	kIx,	,
orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
přidělena	přidělen	k2eAgNnPc1d1	přiděleno
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
používala	používat	k5eAaImAgFnS	používat
a	a	k8xC	a
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
domech	dům	k1gInPc6	dům
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
vyznačena	vyznačit	k5eAaPmNgNnP	vyznačit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nově	nově	k6eAd1	nově
přidělována	přidělován	k2eAgNnPc1d1	přidělováno
nejsou	být	k5eNaImIp3nP	být
a	a	k8xC	a
neuvádějí	uvádět	k5eNaImIp3nP	uvádět
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
v	v	k7c6	v
databázích	databáze	k1gFnPc6	databáze
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Brno	Brno	k1gNnSc1	Brno
===	===	k?	===
</s>
</p>
<p>
<s>
Zásady	zásada	k1gFnPc1	zásada
pro	pro	k7c4	pro
číslování	číslování	k1gNnSc4	číslování
brněnských	brněnský	k2eAgInPc2d1	brněnský
domů	dům	k1gInPc2	dům
podle	podle	k7c2	podle
ulic	ulice	k1gFnPc2	ulice
stanovil	stanovit	k5eAaPmAgInS	stanovit
již	již	k6eAd1	již
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1867	[number]	k4	1867
výnos	výnos	k1gInSc1	výnos
moravského	moravský	k2eAgNnSc2d1	Moravské
místodržitelství	místodržitelství	k1gNnSc2	místodržitelství
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
ulice	ulice	k1gFnSc1	ulice
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
číslování	číslování	k1gNnSc4	číslování
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
liché	lichý	k2eAgNnSc1d1	liché
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
sudé	sudý	k2eAgFnSc3d1	sudá
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středem	středem	k7c2	středem
této	tento	k3xDgFnSc2	tento
orientace	orientace	k1gFnSc2	orientace
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
stává	stávat	k5eAaImIp3nS	stávat
toliko	toliko	k6eAd1	toliko
Velké	velký	k2eAgNnSc4d1	velké
náměstí	náměstí	k1gNnSc4	náměstí
a	a	k8xC	a
začátky	začátek	k1gInPc4	začátek
ulic	ulice	k1gFnPc2	ulice
(	(	kIx(	(
<g/>
nutné	nutný	k2eAgNnSc1d1	nutné
i	i	k9	i
ke	k	k7c3	k
stanovení	stanovení	k1gNnSc3	stanovení
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
pomyslně	pomyslně	k6eAd1	pomyslně
<g/>
,	,	kIx,	,
či	či	k8xC	či
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
čarou	čára	k1gFnSc7	čára
<g/>
)	)	kIx)	)
blíže	blíž	k1gFnSc2	blíž
onomu	onen	k3xDgInSc3	onen
stanovenému	stanovený	k2eAgInSc3d1	stanovený
středu	střed	k1gInSc3	střed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslování	číslování	k1gNnSc1	číslování
náměstí	náměstí	k1gNnSc2	náměstí
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
postupuje	postupovat	k5eAaImIp3nS	postupovat
dokola	dokola	k6eAd1	dokola
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
v	v	k7c6	v
souvislé	souvislý	k2eAgFnSc6d1	souvislá
číselné	číselný	k2eAgFnSc6d1	číselná
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rohové	rohový	k2eAgFnPc1d1	rohová
budovy	budova	k1gFnPc1	budova
nesou	nést	k5eAaImIp3nP	nést
příslušné	příslušný	k2eAgNnSc4d1	příslušné
číslo	číslo	k1gNnSc4	číslo
domu	dům	k1gInSc2	dům
ulic	ulice	k1gFnPc2	ulice
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Tábor	Tábor	k1gInSc4	Tábor
stavěny	stavěn	k2eAgInPc4d1	stavěn
obytné	obytný	k2eAgInPc4d1	obytný
bloky	blok	k1gInPc4	blok
kolmé	kolmý	k2eAgInPc4d1	kolmý
k	k	k7c3	k
ulicím	ulice	k1gFnPc3	ulice
<g/>
,	,	kIx,	,
používala	používat	k5eAaImAgFnS	používat
se	se	k3xPyFc4	se
rozlišovací	rozlišovací	k2eAgNnPc1d1	rozlišovací
písmena	písmeno	k1gNnPc1	písmeno
za	za	k7c7	za
číslem	číslo	k1gNnSc7	číslo
orientačním	orientační	k2eAgNnSc7d1	orientační
k	k	k7c3	k
odlišení	odlišení	k1gNnSc3	odlišení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
vchodů	vchod	k1gInPc2	vchod
<g/>
,	,	kIx,	,
sekcí	sekce	k1gFnPc2	sekce
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
setrvala	setrvat	k5eAaPmAgFnS	setrvat
i	i	k9	i
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnPc4d1	další
města	město	k1gNnPc4	město
a	a	k8xC	a
vesnice	vesnice	k1gFnPc4	vesnice
===	===	k?	===
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
českých	český	k2eAgInPc6d1	český
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
menších	malý	k2eAgInPc2d2	menší
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
,	,	kIx,	,
zavedly	zavést	k5eAaPmAgInP	zavést
je	být	k5eAaImIp3nS	být
leckteré	leckterý	k3yIgFnSc2	leckterý
venkovské	venkovský	k2eAgFnSc2d1	venkovská
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
byla	být	k5eAaImAgNnP	být
orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
s	s	k7c7	s
pojmenováním	pojmenování	k1gNnSc7	pojmenování
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
zavedena	zaveden	k2eAgFnSc1d1	zavedena
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
v	v	k7c6	v
Úněticích	Únětice	k1gFnPc6	Únětice
<g/>
.	.	kIx.	.
<g/>
Orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
se	se	k3xPyFc4	se
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
například	například	k6eAd1	například
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
městech	město	k1gNnPc6	město
a	a	k8xC	a
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
:	:	kIx,	:
Aš	Aš	k1gFnSc1	Aš
<g/>
,	,	kIx,	,
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
Březová	březový	k2eAgFnSc1d1	Březová
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
Lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
,	,	kIx,	,
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
Františkovy	Františkův	k2eAgFnPc1d1	Františkova
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
Havířov	Havířov	k1gInSc1	Havířov
<g/>
,	,	kIx,	,
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
Horní	horní	k2eAgInSc1d1	horní
Slavkov	Slavkov	k1gInSc1	Slavkov
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
Kraslice	kraslice	k1gFnSc1	kraslice
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Krnov	Krnov	k1gInSc1	Krnov
<g/>
,	,	kIx,	,
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
,	,	kIx,	,
Kynšperk	Kynšperk	k1gInSc1	Kynšperk
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
Loket	loket	k1gInSc1	loket
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
<g/>
,	,	kIx,	,
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
,	,	kIx,	,
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
,	,	kIx,	,
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
Únětice	Únětice	k1gFnSc1	Únětice
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Valašské	valašský	k2eAgNnSc1d1	Valašské
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
<g/>
,	,	kIx,	,
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
,	,	kIx,	,
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
nepřidělují	přidělovat	k5eNaImIp3nP	přidělovat
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
tisících	tisící	k4xOgInPc6	tisící
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
91	[number]	k4	91
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zlín	Zlín	k1gInSc1	Zlín
(	(	kIx(	(
<g/>
75	[number]	k4	75
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kladno	Kladno	k1gNnSc1	Kladno
(	(	kIx(	(
<g/>
72	[number]	k4	72
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zde	zde	k6eAd1	zde
však	však	k9	však
orientační	orientační	k2eAgNnSc1d1	orientační
číslování	číslování	k1gNnSc1	číslování
bylo	být	k5eAaImAgNnS	být
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Třinec	Třinec	k1gInSc1	Třinec
(	(	kIx(	(
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Příbram	Příbram	k1gFnSc1	Příbram
(	(	kIx(	(
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Orlová	Orlová	k1gFnSc1	Orlová
(	(	kIx(	(
<g/>
33	[number]	k4	33
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Vsetín	Vsetín	k1gInSc1	Vsetín
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Litvínov	Litvínov	k1gInSc1	Litvínov
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sokolov	Sokolov	k1gInSc1	Sokolov
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
domy	dům	k1gInPc1	dům
však	však	k8xC	však
orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
mají	mít	k5eAaImIp3nP	mít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chrudim	Chrudim	k1gFnSc1	Chrudim
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strakonice	Strakonice	k1gFnPc1	Strakonice
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zlín	Zlín	k1gInSc1	Zlín
označil	označit	k5eAaPmAgInS	označit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
ředitel	ředitel	k1gMnSc1	ředitel
komunikací	komunikace	k1gFnPc2	komunikace
České	český	k2eAgFnSc2d1	Česká
pošty	pošta	k1gFnSc2	pošta
Ivo	Ivo	k1gMnSc1	Ivo
Mravinac	Mravinac	k1gFnSc1	Mravinac
za	za	k7c4	za
celorepublikový	celorepublikový	k2eAgInSc4d1	celorepublikový
unikát	unikát	k1gInSc4	unikát
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
Zlín	Zlín	k1gInSc4	Zlín
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vymyslet	vymyslet	k5eAaPmF	vymyslet
jedinečný	jedinečný	k2eAgInSc4d1	jedinečný
systém	systém	k1gInSc4	systém
doručování	doručování	k1gNnSc2	doručování
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
ulice	ulice	k1gFnPc1	ulice
procházejí	procházet	k5eAaImIp3nP	procházet
i	i	k9	i
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
částí	část	k1gFnPc2	část
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
některá	některý	k3yIgNnPc1	některý
čísla	číslo	k1gNnPc1	číslo
popisná	popisný	k2eAgNnPc1d1	popisné
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgInSc3	ten
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
páteřní	páteřní	k2eAgFnSc6d1	páteřní
třídě	třída	k1gFnSc6	třída
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
hned	hned	k6eAd1	hned
několikrát	několikrát	k6eAd1	několikrát
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
číslo	číslo	k1gNnSc4	číslo
202	[number]	k4	202
dokonce	dokonce	k9	dokonce
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Doručovatelé	doručovatel	k1gMnPc1	doručovatel
a	a	k8xC	a
třídiči	třídič	k1gMnPc1	třídič
zásilek	zásilka	k1gFnPc2	zásilka
musejí	muset	k5eAaImIp3nP	muset
používat	používat	k5eAaImF	používat
pomocné	pomocný	k2eAgFnPc4d1	pomocná
mapy	mapa	k1gFnPc4	mapa
a	a	k8xC	a
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
místní	místní	k2eAgFnPc4d1	místní
znalosti	znalost	k1gFnPc4	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
magistrátu	magistrát	k1gInSc2	magistrát
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Dvořák	Dvořák	k1gMnSc1	Dvořák
však	však	k9	však
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stížnost	stížnost	k1gFnSc1	stížnost
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
České	český	k2eAgFnSc2d1	Česká
pošty	pošta	k1gFnSc2	pošta
k	k	k7c3	k
číslování	číslování	k1gNnSc3	číslování
domů	domů	k6eAd1	domů
zatím	zatím	k6eAd1	zatím
zlínský	zlínský	k2eAgInSc1d1	zlínský
magistrát	magistrát	k1gInSc1	magistrát
nezaznamenal	zaznamenat	k5eNaPmAgInS	zaznamenat
a	a	k8xC	a
proto	proto	k8xC	proto
ani	ani	k8xC	ani
zatím	zatím	k6eAd1	zatím
neplánuje	plánovat	k5eNaImIp3nS	plánovat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
nějaká	nějaký	k3yIgNnPc4	nějaký
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
Česká	český	k2eAgFnSc1d1	Česká
pošta	pošta	k1gFnSc1	pošta
obrátí	obrátit	k5eAaPmIp3nS	obrátit
<g/>
,	,	kIx,	,
určitě	určitě	k6eAd1	určitě
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
řešit	řešit	k5eAaImF	řešit
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
už	už	k6eAd1	už
se	se	k3xPyFc4	se
magistrát	magistrát	k1gInSc1	magistrát
záměrem	záměr	k1gInSc7	záměr
zavést	zavést	k5eAaPmF	zavést
orientační	orientační	k2eAgNnPc4d1	orientační
čísla	číslo	k1gNnPc4	číslo
zabýval	zabývat	k5eAaImAgMnS	zabývat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
námět	námět	k1gInSc4	námět
odložil	odložit	k5eAaPmAgMnS	odložit
kvůli	kvůli	k7c3	kvůli
několika	několik	k4yIc3	několik
akcím	akce	k1gFnPc3	akce
souvisejícím	související	k2eAgFnPc3d1	související
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
centrálních	centrální	k2eAgInPc2d1	centrální
registrů	registr	k1gInPc2	registr
<g/>
:	:	kIx,	:
odstraňování	odstraňování	k1gNnSc4	odstraňování
nesouladů	nesoulad	k1gInPc2	nesoulad
čísel	číslo	k1gNnPc2	číslo
popisných	popisný	k2eAgFnPc2d1	popisná
a	a	k8xC	a
evidenčních	evidenční	k2eAgFnPc2d1	evidenční
<g/>
,	,	kIx,	,
přečíslování	přečíslování	k1gNnSc1	přečíslování
půldomků	půldomek	k1gMnPc2	půldomek
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
a	a	k8xC	a
přejmenování	přejmenování	k1gNnSc2	přejmenování
několika	několik	k4yIc2	několik
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
sídel	sídlo	k1gNnPc2	sídlo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Černými	černý	k2eAgInPc7d1	černý
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Kouřim	Kouřim	k1gFnSc1	Kouřim
či	či	k8xC	či
pražská	pražský	k2eAgFnSc1d1	Pražská
čtvrť	čtvrť	k1gFnSc1	čtvrť
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
orientační	orientační	k2eAgFnSc1d1	orientační
čísla	číslo	k1gNnSc2	číslo
používala	používat	k5eAaImAgFnS	používat
a	a	k8xC	a
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
domech	dům	k1gInPc6	dům
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
vyznačena	vyznačit	k5eAaPmNgNnP	vyznačit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nově	nově	k6eAd1	nově
přidělována	přidělován	k2eAgNnPc1d1	přidělováno
nejsou	být	k5eNaImIp3nP	být
a	a	k8xC	a
neuvádějí	uvádět	k5eNaImIp3nP	uvádět
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
v	v	k7c6	v
databázích	databáze	k1gFnPc6	databáze
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
vyznačování	vyznačování	k1gNnSc1	vyznačování
==	==	k?	==
</s>
</p>
<p>
<s>
Celostátně	celostátně	k6eAd1	celostátně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vyhláškou	vyhláška	k1gFnSc7	vyhláška
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
č.	č.	k?	č.
326	[number]	k4	326
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
že	že	k8xS	že
tabulky	tabulka	k1gFnPc1	tabulka
s	s	k7c7	s
popisnými	popisný	k2eAgNnPc7d1	popisné
i	i	k8xC	i
orientačními	orientační	k2eAgNnPc7d1	orientační
čísly	číslo	k1gNnPc7	číslo
se	se	k3xPyFc4	se
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
viditelně	viditelně	k6eAd1	viditelně
<g/>
,	,	kIx,	,
že	že	k8xS	že
číslo	číslo	k1gNnSc1	číslo
orientační	orientační	k2eAgNnSc1d1	orientační
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
umístěno	umístit	k5eAaPmNgNnS	umístit
vždy	vždy	k6eAd1	vždy
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
popisným	popisný	k2eAgNnSc7d1	popisné
a	a	k8xC	a
že	že	k8xS	že
označení	označení	k1gNnSc1	označení
popisných	popisný	k2eAgNnPc2d1	popisné
<g/>
,	,	kIx,	,
orientačních	orientační	k2eAgNnPc2d1	orientační
a	a	k8xC	a
evidenčních	evidenční	k2eAgNnPc2d1	evidenční
čísel	číslo	k1gNnPc2	číslo
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
barevně	barevně	k6eAd1	barevně
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Celostátně	celostátně	k6eAd1	celostátně
nejsou	být	k5eNaImIp3nP	být
barvy	barva	k1gFnPc1	barva
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
<g/>
,	,	kIx,	,
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
podobu	podoba	k1gFnSc4	podoba
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
mají	mít	k5eAaImIp3nP	mít
podle	podle	k7c2	podle
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
(	(	kIx(	(
<g/>
§	§	k?	§
32	[number]	k4	32
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
stanovit	stanovit	k5eAaPmF	stanovit
obecní	obecní	k2eAgInPc4d1	obecní
úřady	úřad	k1gInPc4	úřad
(	(	kIx(	(
<g/>
v	v	k7c6	v
přenesené	přenesený	k2eAgFnSc6d1	přenesená
působnosti	působnost	k1gFnSc6	působnost
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nařízením	nařízení	k1gNnSc7	nařízení
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
vyhláškou	vyhláška	k1gFnSc7	vyhláška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
není	být	k5eNaImIp3nS	být
provedení	provedení	k1gNnSc1	provedení
tabulek	tabulka	k1gFnPc2	tabulka
předepsáno	předepsat	k5eAaPmNgNnS	předepsat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
popisná	popisný	k2eAgNnPc1d1	popisné
čísla	číslo	k1gNnPc1	číslo
označují	označovat	k5eAaImIp3nP	označovat
bílými	bílý	k2eAgFnPc7d1	bílá
číslicemi	číslice	k1gFnPc7	číslice
na	na	k7c6	na
červené	červený	k2eAgFnSc6d1	červená
tabulce	tabulka	k1gFnSc6	tabulka
a	a	k8xC	a
orientační	orientační	k2eAgMnPc1d1	orientační
bílými	bílý	k2eAgInPc7d1	bílý
na	na	k7c4	na
modré	modrý	k2eAgInPc4d1	modrý
<g/>
;	;	kIx,	;
evidenční	evidenční	k2eAgNnPc1d1	evidenční
čísla	číslo	k1gNnPc1	číslo
rekreačních	rekreační	k2eAgFnPc2d1	rekreační
staveb	stavba	k1gFnPc2	stavba
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
černá	černý	k2eAgFnSc1d1	černá
na	na	k7c6	na
žluté	žlutý	k2eAgFnSc6d1	žlutá
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
,	,	kIx,	,
nouzových	nouzový	k2eAgFnPc2d1	nouzová
staveb	stavba	k1gFnPc2	stavba
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1960	[number]	k4	1960
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
HMP	HMP	kA	HMP
<g/>
,	,	kIx,	,
Řád	řád	k1gInSc1	řád
pro	pro	k7c4	pro
jednotné	jednotný	k2eAgNnSc4d1	jednotné
označování	označování	k1gNnSc4	označování
veřejných	veřejný	k2eAgNnPc2d1	veřejné
prostranství	prostranství	k1gNnPc2	prostranství
a	a	k8xC	a
číslování	číslování	k1gNnSc1	číslování
domů	dům	k1gInPc2	dům
na	na	k7c6	na
území	území	k1gNnSc6	území
Ústředního	ústřední	k2eAgInSc2d1	ústřední
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
ze	z	k7c2	z
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
předepisovala	předepisovat	k5eAaImAgFnS	předepisovat
barvu	barva	k1gFnSc4	barva
i	i	k8xC	i
velikost	velikost	k1gFnSc4	velikost
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc4	jejich
textový	textový	k2eAgInSc4d1	textový
<g />
.	.	kIx.	.
</s>
<s>
obsah	obsah	k1gInSc4	obsah
a	a	k8xC	a
smaltované	smaltovaný	k2eAgNnSc4d1	smaltované
provedení	provedení	k1gNnSc4	provedení
<g/>
;	;	kIx,	;
zrušila	zrušit	k5eAaPmAgFnS	zrušit
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc6	červenec
1999	[number]	k4	1999
vyhláška	vyhláška	k1gFnSc1	vyhláška
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
14	[number]	k4	14
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
devíti	devět	k4xCc7	devět
dalšími	další	k2eAgFnPc7d1	další
podobnými	podobný	k2eAgFnPc7d1	podobná
předrevolučními	předrevoluční	k2eAgFnPc7d1	předrevoluční
vyhláškami	vyhláška	k1gFnPc7	vyhláška
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
Statutu	statut	k1gInSc2	statut
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
podle	podle	k7c2	podle
porevolučního	porevoluční	k2eAgInSc2d1	porevoluční
zákona	zákon	k1gInSc2	zákon
ČNR	ČNR	kA	ČNR
o	o	k7c6	o
hl.	hl.	k?	hl.
m.	m.	k?	m.
Praze	Praha	k1gFnSc6	Praha
418	[number]	k4	418
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Dnem	dnem	k7c2	dnem
prvních	první	k4xOgInPc2	první
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
krajských	krajský	k2eAgNnPc2d1	krajské
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2000	[number]	k4	2000
nabyl	nabýt	k5eAaPmAgInS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Praze	Praha	k1gFnSc6	Praha
č.	č.	k?	č.
131	[number]	k4	131
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
§	§	k?	§
14	[number]	k4	14
o	o	k7c6	o
číslování	číslování	k1gNnSc6	číslování
budov	budova	k1gFnPc2	budova
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
analogického	analogický	k2eAgInSc2d1	analogický
§	§	k?	§
32	[number]	k4	32
souběžně	souběžně	k6eAd1	souběžně
vydaného	vydaný	k2eAgInSc2d1	vydaný
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
nedává	dávat	k5eNaImIp3nS	dávat
Praze	Praha	k1gFnSc3	Praha
kompetenci	kompetence	k1gFnSc4	kompetence
určovat	určovat	k5eAaImF	určovat
"	"	kIx"	"
<g/>
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
provedení	provedení	k1gNnSc4	provedení
čísel	číslo	k1gNnPc2	číslo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
zákony	zákon	k1gInPc1	zákon
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
způsob	způsob	k1gInSc1	způsob
použití	použití	k1gNnSc1	použití
a	a	k8xC	a
umístění	umístění	k1gNnSc1	umístění
čísel	číslo	k1gNnPc2	číslo
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
budov	budova	k1gFnPc2	budova
<g/>
"	"	kIx"	"
stanoví	stanovit	k5eAaPmIp3nS	stanovit
prováděcí	prováděcí	k2eAgInSc1d1	prováděcí
předpis	předpis	k1gInSc1	předpis
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
společná	společný	k2eAgFnSc1d1	společná
vyhláška	vyhláška	k1gFnSc1	vyhláška
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
č.	č.	k?	č.
326	[number]	k4	326
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
specifikující	specifikující	k2eAgFnPc1d1	specifikující
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
popisná	popisný	k2eAgNnPc1d1	popisné
<g/>
,	,	kIx,	,
evidenční	evidenční	k2eAgNnPc1d1	evidenční
a	a	k8xC	a
orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
se	se	k3xPyFc4	se
barevně	barevně	k6eAd1	barevně
liší	lišit	k5eAaImIp3nS	lišit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
§	§	k?	§
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Provedení	provedení	k1gNnSc1	provedení
tabulky	tabulka	k1gFnSc2	tabulka
a	a	k8xC	a
eventuální	eventuální	k2eAgNnSc4d1	eventuální
uvedení	uvedení	k1gNnSc4	uvedení
dalších	další	k2eAgInPc2d1	další
údajů	údaj	k1gInPc2	údaj
tak	tak	k6eAd1	tak
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
libovůli	libovůle	k1gFnSc4	libovůle
majitelů	majitel	k1gMnPc2	majitel
domů	domů	k6eAd1	domů
a	a	k8xC	a
místních	místní	k2eAgFnPc6d1	místní
zvyklostech	zvyklost	k1gFnPc6	zvyklost
a	a	k8xC	a
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
obcí	obec	k1gFnSc7	obec
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
zákonné	zákonný	k2eAgNnSc4d1	zákonné
zmocnění	zmocnění	k1gNnSc4	zmocnění
jejich	jejich	k3xOp3gNnSc4	jejich
provedení	provedení	k1gNnSc4	provedení
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
Expertní	expertní	k2eAgFnSc2d1	expertní
skupiny	skupina	k1gFnSc2	skupina
primátora	primátor	k1gMnSc2	primátor
pro	pro	k7c4	pro
vizuální	vizuální	k2eAgFnSc4d1	vizuální
podobu	podoba	k1gFnSc4	podoba
města	město	k1gNnSc2	město
Rada	rada	k1gFnSc1	rada
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
Usnesením	usnesení	k1gNnSc7	usnesení
č.	č.	k?	č.
1267	[number]	k4	1267
z	z	k7c2	z
28	[number]	k4	28
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
schválila	schválit	k5eAaPmAgFnS	schválit
jednotný	jednotný	k2eAgInSc4d1	jednotný
vzhled	vzhled	k1gInSc4	vzhled
uličních	uliční	k2eAgFnPc2d1	uliční
tabulí	tabule	k1gFnPc2	tabule
podle	podle	k7c2	podle
manuálu	manuál	k1gInSc2	manuál
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
studenti	student	k1gMnPc1	student
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Filip	Filip	k1gMnSc1	Filip
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kříbek	Kříbek	k?	Kříbek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
a	a	k8xC	a
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Říha	Říha	k1gMnSc1	Říha
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Mgr.	Mgr.	kA	Mgr.
Filipa	Filip	k1gMnSc4	Filip
Blažka	Blažek	k1gMnSc4	Blažek
<g/>
.	.	kIx.	.
</s>
<s>
Manuál	manuál	k1gInSc1	manuál
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
nezávazné	závazný	k2eNgNnSc4d1	nezávazné
doporučení	doporučení	k1gNnSc4	doporučení
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
podoby	podoba	k1gFnSc2	podoba
tabulek	tabulka	k1gFnPc2	tabulka
s	s	k7c7	s
domovními	domovní	k2eAgNnPc7d1	domovní
čísly	číslo	k1gNnPc7	číslo
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
text	text	k1gInSc1	text
usnesení	usnesení	k1gNnSc2	usnesení
rady	rada	k1gFnSc2	rada
vůbec	vůbec	k9	vůbec
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
důvodová	důvodový	k2eAgFnSc1d1	Důvodová
zpráva	zpráva	k1gFnSc1	zpráva
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Prahy	Praha	k1gFnSc2	Praha
vlastníkům	vlastník	k1gMnPc3	vlastník
budov	budova	k1gFnPc2	budova
neukládá	ukládat	k5eNaImIp3nS	ukládat
povinnost	povinnost	k1gFnSc1	povinnost
respektovat	respektovat	k5eAaImF	respektovat
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
úpravu	úprava	k1gFnSc4	úprava
tabulek	tabulka	k1gFnPc2	tabulka
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
popisnými	popisný	k2eAgNnPc7d1	popisné
a	a	k8xC	a
</s>
</p>
<p>
<s>
orientačními	orientační	k2eAgInPc7d1	orientační
<g/>
.	.	kIx.	.
</s>
<s>
Manuál	manuál	k1gInSc1	manuál
respektuje	respektovat	k5eAaImIp3nS	respektovat
tradiční	tradiční	k2eAgMnSc1d1	tradiční
barevné	barevný	k2eAgNnSc4d1	barevné
<g/>
,	,	kIx,	,
grafické	grafický	k2eAgNnSc4d1	grafické
<g/>
,	,	kIx,	,
rozměrové	rozměrový	k2eAgNnSc4d1	rozměrové
i	i	k8xC	i
obsahové	obsahový	k2eAgNnSc4d1	obsahové
provedení	provedení	k1gNnSc4	provedení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
definuje	definovat	k5eAaBmIp3nS	definovat
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
písmo	písmo	k1gNnSc1	písmo
"	"	kIx"	"
<g/>
SMALT	smalt	k1gInSc1	smalt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
autoři	autor	k1gMnPc1	autor
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
městu	město	k1gNnSc3	město
Praze	Praha	k1gFnSc3	Praha
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
třem	tři	k4xCgFnPc3	tři
smaltovnám	smaltovna	k1gFnPc3	smaltovna
licenci	licence	k1gFnSc4	licence
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
bezplatnému	bezplatný	k2eAgNnSc3d1	bezplatné
užití	užití	k1gNnSc3	užití
<g/>
)	)	kIx)	)
a	a	k8xC	a
stanoví	stanovit	k5eAaPmIp3nS	stanovit
jeho	jeho	k3xOp3gFnSc1	jeho
proporce	proporce	k1gFnSc1	proporce
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnSc1d1	základní
výška	výška	k1gFnSc1	výška
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
vzory	vzor	k1gInPc1	vzor
znaků	znak	k1gInPc2	znak
v	v	k7c6	v
několika	několik	k4yIc6	několik
šířkových	šířkový	k2eAgFnPc6d1	šířková
variantách	varianta	k1gFnPc6	varianta
<g/>
,	,	kIx,	,
prostrkání	prostrkání	k1gNnSc6	prostrkání
<g/>
,	,	kIx,	,
umístění	umístění	k1gNnSc6	umístění
řádků	řádek	k1gInPc2	řádek
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
šířka	šířka	k1gFnSc1	šířka
textu	text	k1gInSc2	text
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
barvy	barva	k1gFnPc1	barva
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
RAL	RAL	kA	RAL
3000	[number]	k4	3000
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
RAL	RAL	kA	RAL
5002	[number]	k4	5002
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
RAL	RAL	kA	RAL
9003	[number]	k4	9003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
podoba	podoba	k1gFnSc1	podoba
tabulek	tabulka	k1gFnPc2	tabulka
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
uvádí	uvádět	k5eAaImIp3nP	uvádět
pouze	pouze	k6eAd1	pouze
vzor	vzor	k1gInSc4	vzor
popisného	popisný	k2eAgNnSc2d1	popisné
a	a	k8xC	a
orientačního	orientační	k2eAgNnSc2d1	orientační
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
vzor	vzor	k1gInSc1	vzor
evidenčního	evidenční	k2eAgNnSc2d1	evidenční
čísla	číslo	k1gNnSc2	číslo
není	být	k5eNaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
tabulce	tabulka	k1gFnSc6	tabulka
s	s	k7c7	s
popisným	popisný	k2eAgNnSc7d1	popisné
číslem	číslo	k1gNnSc7	číslo
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
tradiční	tradiční	k2eAgNnSc4d1	tradiční
uvádění	uvádění	k1gNnSc4	uvádění
názvu	název	k1gInSc2	název
čtvrti	čtvrt	k1gFnSc2	čtvrt
a	a	k8xC	a
názvu	název	k1gInSc2	název
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
ani	ani	k8xC	ani
správního	správní	k2eAgInSc2d1	správní
obvodu	obvod	k1gInSc2	obvod
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
nadále	nadále	k6eAd1	nadále
nemá	mít	k5eNaImIp3nS	mít
uvádět	uvádět	k5eAaImF	uvádět
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzor	vzor	k1gInSc1	vzor
tabulky	tabulka	k1gFnSc2	tabulka
s	s	k7c7	s
orientačním	orientační	k2eAgNnSc7d1	orientační
číslem	číslo	k1gNnSc7	číslo
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
dalšího	další	k2eAgInSc2d1	další
údaje	údaj	k1gInSc2	údaj
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
názvu	název	k1gInSc2	název
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
manuálu	manuál	k1gInSc2	manuál
navíc	navíc	k6eAd1	navíc
zaměnili	zaměnit	k5eAaPmAgMnP	zaměnit
název	název	k1gInSc4	název
čísla	číslo	k1gNnSc2	číslo
popisného	popisný	k2eAgNnSc2d1	popisné
s	s	k7c7	s
názvem	název	k1gInSc7	název
čísla	číslo	k1gNnSc2	číslo
orientačního	orientační	k2eAgNnSc2d1	orientační
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
popisným	popisný	k2eAgInSc7d1	popisný
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
rozměry	rozměr	k1gInPc4	rozměr
320	[number]	k4	320
x	x	k?	x
250	[number]	k4	250
mm	mm	kA	mm
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
číslic	číslice	k1gFnPc2	číslice
90	[number]	k4	90
mm	mm	kA	mm
a	a	k8xC	a
výškou	výška	k1gFnSc7	výška
písma	písmo	k1gNnSc2	písmo
doplňujících	doplňující	k2eAgInPc2d1	doplňující
údajů	údaj	k1gInPc2	údaj
27	[number]	k4	27
mm	mm	kA	mm
<g/>
,	,	kIx,	,
tabulka	tabulka	k1gFnSc1	tabulka
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
orientačním	orientační	k2eAgInSc7d1	orientační
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
rozměry	rozměr	k1gInPc4	rozměr
250	[number]	k4	250
x	x	k?	x
200	[number]	k4	200
mm	mm	kA	mm
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
číslic	číslice	k1gFnPc2	číslice
96	[number]	k4	96
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Usnesení	usnesení	k1gNnSc1	usnesení
rady	rada	k1gFnSc2	rada
ukládání	ukládání	k1gNnSc2	ukládání
magistrátnímu	magistrátní	k2eAgInSc3d1	magistrátní
odboru	odbor	k1gInSc3	odbor
ZIO	ZIO	kA	ZIO
prověřit	prověřit	k5eAaPmF	prověřit
možnosti	možnost	k1gFnPc4	možnost
právní	právní	k2eAgFnSc2d1	právní
ochrany	ochrana	k1gFnSc2	ochrana
vzhledu	vzhled	k1gInSc2	vzhled
tabulí	tabule	k1gFnPc2	tabule
například	například	k6eAd1	například
formou	forma	k1gFnSc7	forma
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
vzoru	vzor	k1gInSc2	vzor
a	a	k8xC	a
zpracovat	zpracovat	k5eAaPmF	zpracovat
návrh	návrh	k1gInSc4	návrh
jednotného	jednotný	k2eAgNnSc2d1	jednotné
rozmístění	rozmístění	k1gNnSc2	rozmístění
uličních	uliční	k2eAgFnPc2d1	uliční
tabulí	tabule	k1gFnPc2	tabule
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
fázi	fáze	k1gFnSc3	fáze
záměru	záměr	k1gInSc2	záměr
kompletního	kompletní	k2eAgInSc2d1	kompletní
manuálu	manuál	k1gInSc2	manuál
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
tvorbu	tvorba	k1gFnSc4	tvorba
a	a	k8xC	a
umisťování	umisťování	k1gNnSc4	umisťování
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
rozpracovány	rozpracován	k2eAgFnPc4d1	rozpracována
či	či	k8xC	či
závazně	závazně	k6eAd1	závazně
stanoveny	stanoven	k2eAgInPc4d1	stanoven
požadavky	požadavek	k1gInPc4	požadavek
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
tabulek	tabulka	k1gFnPc2	tabulka
s	s	k7c7	s
domovními	domovní	k2eAgNnPc7d1	domovní
čísly	číslo	k1gNnPc7	číslo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
text	text	k1gInSc1	text
usnesení	usnesení	k1gNnSc2	usnesení
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Brno	Brno	k1gNnSc1	Brno
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
24	[number]	k4	24
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc3	květen
1867	[number]	k4	1867
byly	být	k5eAaImAgFnP	být
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
ulice	ulice	k1gFnPc1	ulice
a	a	k8xC	a
zavedeno	zaveden	k2eAgNnSc1d1	zavedeno
nové	nový	k2eAgNnSc1d1	nové
číslování	číslování	k1gNnSc1	číslování
domů	dům	k1gInPc2	dům
po	po	k7c6	po
ulicích	ulice	k1gFnPc6	ulice
podle	podle	k7c2	podle
císařského	císařský	k2eAgInSc2d1	císařský
výnosu	výnos	k1gInSc2	výnos
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Číslování	číslování	k1gNnSc1	číslování
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
bylo	být	k5eAaImAgNnS	být
ponecháno	ponechat	k5eAaPmNgNnS	ponechat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
čísla	číslo	k1gNnPc1	číslo
popisná	popisný	k2eAgNnPc1d1	popisné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
jsou	být	k5eAaImIp3nP	být
popisná	popisný	k2eAgNnPc1d1	popisné
čísla	číslo	k1gNnPc1	číslo
bílá	bílý	k2eAgNnPc1d1	bílé
na	na	k7c6	na
černém	černý	k2eAgInSc6d1	černý
podkladě	podklad	k1gInSc6	podklad
<g/>
,	,	kIx,	,
orientační	orientační	k2eAgFnPc1d1	orientační
jsou	být	k5eAaImIp3nP	být
červená	červený	k2eAgFnSc1d1	červená
nebo	nebo	k8xC	nebo
modrá	modrý	k2eAgFnSc1d1	modrá
na	na	k7c6	na
bílém	bílý	k2eAgInSc6d1	bílý
podkladě	podklad	k1gInSc6	podklad
<g/>
,	,	kIx,	,
evidenční	evidenční	k2eAgNnPc4d1	evidenční
zelená	zelené	k1gNnPc4	zelené
na	na	k7c6	na
bílém	bílý	k2eAgInSc6d1	bílý
podkladě	podklad	k1gInSc6	podklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
s	s	k7c7	s
popisným	popisný	k2eAgNnSc7d1	popisné
nebo	nebo	k8xC	nebo
evidenčním	evidenční	k2eAgNnSc7d1	evidenční
číslem	číslo	k1gNnSc7	číslo
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vždy	vždy	k6eAd1	vždy
uveden	uvést	k5eAaPmNgInS	uvést
i	i	k9	i
název	název	k1gInSc1	název
příslušného	příslušný	k2eAgNnSc2d1	příslušné
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
s	s	k7c7	s
orientačním	orientační	k2eAgNnSc7d1	orientační
číslem	číslo	k1gNnSc7	číslo
název	název	k1gInSc4	název
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostrava	Ostrava	k1gFnSc1	Ostrava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
jsou	být	k5eAaImIp3nP	být
čísla	číslo	k1gNnPc1	číslo
orientační	orientační	k2eAgNnPc1d1	orientační
bílá	bílé	k1gNnPc1	bílé
na	na	k7c6	na
modrém	modrý	k2eAgInSc6d1	modrý
podkladě	podklad	k1gInSc6	podklad
<g/>
,	,	kIx,	,
evidenční	evidenční	k2eAgNnPc4d1	evidenční
bílá	bílé	k1gNnPc4	bílé
na	na	k7c6	na
červeném	červený	k2eAgInSc6d1	červený
podkladě	podklad	k1gInSc6	podklad
a	a	k8xC	a
popisná	popisný	k2eAgNnPc1d1	popisné
červená	červené	k1gNnPc1	červené
na	na	k7c6	na
bílém	bílý	k2eAgInSc6d1	bílý
podkladě	podklad	k1gInSc6	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
městský	městský	k2eAgInSc1d1	městský
obvod	obvod	k1gInSc1	obvod
Poruba	Poruba	k1gFnSc1	Poruba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
čísla	číslo	k1gNnPc1	číslo
popisná	popisný	k2eAgNnPc1d1	popisné
bílá	bílé	k1gNnPc1	bílé
na	na	k7c6	na
modrém	modrý	k2eAgInSc6d1	modrý
podkladě	podklad	k1gInSc6	podklad
a	a	k8xC	a
orientační	orientační	k2eAgFnSc1d1	orientační
červená	červená	k1gFnSc1	červená
na	na	k7c6	na
bílém	bílý	k2eAgInSc6d1	bílý
podkladě	podklad	k1gInSc6	podklad
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
opačně	opačně	k6eAd1	opačně
než	než	k8xS	než
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
domy	dům	k1gInPc1	dům
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
současně	současně	k6eAd1	současně
číslem	číslo	k1gNnSc7	číslo
popisným	popisný	k2eAgMnPc3d1	popisný
i	i	k8xC	i
orientačním	orientační	k2eAgMnSc7d1	orientační
v	v	k7c6	v
jednobarevném	jednobarevný	k2eAgNnSc6d1	jednobarevné
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
===	===	k?	===
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
o	o	k7c6	o
označování	označování	k1gNnSc6	označování
domů	dům	k1gInPc2	dům
orientačními	orientační	k2eAgNnPc7d1	orientační
čísly	číslo	k1gNnPc7	číslo
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
vyhláškou	vyhláška	k1gFnSc7	vyhláška
města	město	k1gNnSc2	město
č.	č.	k?	č.
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
bez	bez	k7c2	bez
náhrady	náhrada	k1gFnSc2	náhrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
v	v	k7c6	v
adrese	adresa	k1gFnSc6	adresa
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
adrese	adresa	k1gFnSc6	adresa
uvádí	uvádět	k5eAaImIp3nS	uvádět
zároveň	zároveň	k6eAd1	zároveň
popisné	popisný	k2eAgNnSc1d1	popisné
i	i	k8xC	i
orientační	orientační	k2eAgNnSc1d1	orientační
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
zvykem	zvyk	k1gInSc7	zvyk
uvádět	uvádět	k5eAaImF	uvádět
je	být	k5eAaImIp3nS	být
oddělená	oddělený	k2eAgFnSc1d1	oddělená
lomítkem	lomítko	k1gNnSc7	lomítko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
popisné	popisný	k2eAgNnSc1d1	popisné
nebo	nebo	k8xC	nebo
evidenční	evidenční	k2eAgNnSc1d1	evidenční
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
mnoho	mnoho	k4c4	mnoho
způsobů	způsob	k1gInPc2	způsob
psaní	psaní	k1gNnSc2	psaní
adresy	adresa	k1gFnSc2	adresa
<g/>
,	,	kIx,	,
logické	logický	k2eAgNnSc1d1	logické
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
uvádět	uvádět	k5eAaImF	uvádět
orientační	orientační	k2eAgNnSc4d1	orientační
číslo	číslo	k1gNnSc4	číslo
u	u	k7c2	u
názvu	název	k1gInSc2	název
ulice	ulice	k1gFnSc2	ulice
či	či	k8xC	či
náměstí	náměstí	k1gNnSc1	náměstí
a	a	k8xC	a
popisné	popisný	k2eAgNnSc1d1	popisné
u	u	k7c2	u
názvu	název	k1gInSc2	název
části	část	k1gFnSc2	část
obce	obec	k1gFnSc2	obec
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
u	u	k7c2	u
názvu	název	k1gInSc2	název
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
římským	římský	k2eAgNnSc7d1	římské
číslem	číslo	k1gNnSc7	číslo
označujícím	označující	k2eAgInSc7d1	označující
část	část	k1gFnSc4	část
obce	obec	k1gFnSc2	obec
<g/>
;	;	kIx,	;
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
je	být	k5eAaImIp3nS	být
také	také	k9	také
uvádět	uvádět	k5eAaImF	uvádět
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
lomítkovým	lomítkův	k2eAgInSc7d1	lomítkův
formátem	formát	k1gInSc7	formát
v	v	k7c6	v
opačném	opačný	k2eAgNnSc6d1	opačné
pořadí	pořadí	k1gNnSc6	pořadí
orientační	orientační	k2eAgMnSc1d1	orientační
<g/>
/	/	kIx~	/
<g/>
popisné	popisný	k2eAgNnSc1d1	popisné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Označování	označování	k1gNnSc1	označování
domů	dům	k1gInPc2	dům
</s>
</p>
<p>
<s>
Označování	označování	k1gNnSc1	označování
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
veřejných	veřejný	k2eAgFnPc2d1	veřejná
prostranství	prostranství	k1gNnSc4	prostranství
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Orientační	orientační	k2eAgFnSc2d1	orientační
čísla	číslo	k1gNnPc4	číslo
domů	dům	k1gInPc2	dům
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Orientation	Orientation	k1gInSc1	Orientation
house	house	k1gNnSc1	house
numbers	numbers	k1gInSc1	numbers
in	in	k?	in
the	the	k?	the
Czech	Czech	k1gInSc1	Czech
Republic	Republice	k1gFnPc2	Republice
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Orientation	Orientation	k1gInSc1	Orientation
house	house	k1gNnSc1	house
numbers	numbersa	k1gFnPc2	numbersa
</s>
</p>
<p>
<s>
Vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
veřejný	veřejný	k2eAgInSc4d1	veřejný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
registru	registrum	k1gNnSc3	registrum
územní	územní	k2eAgFnSc2d1	územní
identifikace	identifikace	k1gFnSc2	identifikace
<g/>
,	,	kIx,	,
adres	adresa	k1gFnPc2	adresa
a	a	k8xC	a
nemovitostí	nemovitost	k1gFnPc2	nemovitost
(	(	kIx(	(
<g/>
RÚIAN	RÚIAN	kA	RÚIAN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
<p>
<s>
Adresy	adresa	k1gFnPc1	adresa
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
na	na	k7c6	na
webu	web	k1gInSc6	web
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
</s>
</p>
<p>
<s>
Vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
Katastr	katastr	k1gInSc4	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
,	,	kIx,	,
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Skála	Skála	k1gMnSc1	Skála
<g/>
:	:	kIx,	:
Čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
orientujeme	orientovat	k5eAaBmIp1nP	orientovat
(	(	kIx(	(
<g/>
Zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
9	[number]	k4	9
<g/>
+	+	kIx~	+
<g/>
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
