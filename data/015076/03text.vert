<s>
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1
fakultaUniverzita	fakultaUniverzita	k1gFnSc1
KarlovaFacultas	KarlovaFacultasa	k1gFnPc2
Philosophica	Philosophica	k1gFnSc1
Faculty	Facult	k1gInPc1
of	of	k?
Arts	Artsa	k1gFnPc2
Hlavní	hlavní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
na	na	k7c4
náměstí	náměstí	k1gNnSc4
Jana	Jan	k1gMnSc2
PalachaVedení	PalachaVedení	k1gNnSc2
fakulty	fakulta	k1gFnSc2
Děkan	děkan	k1gMnSc1
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
doc.	doc.	kA
PhDr.	PhDr.	kA
Michal	Michal	k1gMnSc1
Pullmann	Pullmann	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Proděkanka	proděkanka	k1gFnSc1
</s>
<s>
prof.	prof.	kA
PhDr.	PhDr.	kA
Markéta	Markéta	k1gFnSc1
Křížová	Křížová	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Proděkan	proděkan	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Daniel	Daniel	k1gMnSc1
Soukup	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Proděkan	proděkan	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Sládek	Sládek	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Proděkanka	proděkanka	k1gFnSc1
</s>
<s>
PhDr.	PhDr.	kA
Hana	Hana	k1gFnSc1
Pazlarová	Pazlarová	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Proděkan	proděkan	k1gMnSc1
</s>
<s>
doc.	doc.	kA
PhDr.	PhDr.	kA
Ladislav	Ladislav	k1gMnSc1
Stančo	Stančo	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Proděkanka	proděkanka	k1gFnSc1
</s>
<s>
Mgr.	Mgr.	kA
Zdeňka	Zdeňka	k1gFnSc1
Filipová	Filipová	k1gFnSc1
Tajemník	tajemník	k1gInSc4
</s>
<s>
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Šebek	Šebek	k1gMnSc1
Statistické	statistický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Zaměstnanci	zaměstnanec	k1gMnPc1
</s>
<s>
700	#num#	k4
Studenti	student	k1gMnPc1
</s>
<s>
8000	#num#	k4
Studijní	studijní	k2eAgInSc1d1
program	program	k1gInSc1
Bakalářský	bakalářský	k2eAgInSc1d1
</s>
<s>
3900	#num#	k4
Magisterský	magisterský	k2eAgInSc1d1
</s>
<s>
2300	#num#	k4
Doktorský	doktorský	k2eAgInSc1d1
</s>
<s>
2000	#num#	k4
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Datum	datum	k1gInSc4
založení	založení	k1gNnSc1
</s>
<s>
1348	#num#	k4
Status	status	k1gInSc1
</s>
<s>
veřejná	veřejný	k2eAgFnSc1d1
Kontaktní	kontaktní	k2eAgFnSc1d1
údaje	údaj	k1gInPc4
Adresa	adresa	k1gFnSc1
</s>
<s>
nám.	nám.	k?
Jana	Jana	k1gFnSc1
Palacha	Palacha	k1gFnSc1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Josefov	Josefov	k1gInSc1
<g/>
116	#num#	k4
38	#num#	k4
Praha	Praha	k1gFnSc1
1	#num#	k4
Telefon	telefon	k1gInSc1
</s>
<s>
+420	+420	k4
221	#num#	k4
619	#num#	k4
111	#num#	k4
DIČ	DIČ	kA
</s>
<s>
CZ00216208	CZ00216208	k4
www.ff.cuni.cz	www.ff.cuni.cz	k1gInSc1
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
(	(	kIx(
<g/>
FF	ff	kA
UK	UK	kA
<g/>
)	)	kIx)
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
původních	původní	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
pražské	pražský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1348	#num#	k4
(	(	kIx(
<g/>
zpočátku	zpočátku	k6eAd1
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
fakulta	fakulta	k1gFnSc1
svobodných	svobodný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
neboli	neboli	k8xC
artistická	artistický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tradičním	tradiční	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
české	český	k2eAgFnSc2d1
vzdělanosti	vzdělanost	k1gFnSc2
a	a	k8xC
z	z	k7c2
hlediska	hledisko	k1gNnSc2
vědeckého	vědecký	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
nejvýznamnější	významný	k2eAgFnSc7d3
humanitní	humanitní	k2eAgFnSc7d1
institucí	instituce	k1gFnSc7
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
více	hodně	k6eAd2
než	než	k8xS
sedmdesáti	sedmdesát	k4xCc6
oborech	obor	k1gInPc6
druhé	druhý	k4xOgFnSc2
největší	veliký	k2eAgFnSc2d3
české	český	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
studuje	studovat	k5eAaImIp3nS
přes	přes	k7c4
8000	#num#	k4
studentů	student	k1gMnPc2
vyučovaných	vyučovaný	k2eAgMnPc2d1
700	#num#	k4
pedagogy	pedagog	k1gMnPc7
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgFnPc4
patří	patřit	k5eAaImIp3nP
světové	světový	k2eAgFnPc4d1
osobnosti	osobnost	k1gFnPc4
jako	jako	k8xS,k8xC
překladatel	překladatel	k1gMnSc1
Martin	Martin	k1gMnSc1
Hilský	Hilský	k1gMnSc1
<g/>
,	,	kIx,
teolog	teolog	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Halík	Halík	k1gMnSc1
a	a	k8xC
egyptolog	egyptolog	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
Verner	Verner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
zde	zde	k6eAd1
působil	působit	k5eAaImAgMnS
například	například	k6eAd1
Jan	Jan	k1gMnSc1
Hus	Hus	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Garrigue	Garrigu	k1gFnSc2
Masaryk	Masaryk	k1gMnSc1
<g/>
,	,	kIx,
Edvard	Edvard	k1gMnSc1
Beneš	Beneš	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Čapek	Čapek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Patočka	Patočka	k1gMnSc1
<g/>
,	,	kIx,
Ernst	Ernst	k1gMnSc1
Mach	Mach	k1gMnSc1
či	či	k8xC
Albert	Albert	k1gMnSc1
Einstein	Einstein	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1
filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
jednou	jeden	k4xCgFnSc7
z	z	k7c2
mála	málo	k4c2
fakult	fakulta	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gNnSc6
vedení	vedení	k1gNnSc6
se	se	k3xPyFc4
výrazně	výrazně	k6eAd1
podílejí	podílet	k5eAaImIp3nP
i	i	k9
studenti	student	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
tvoří	tvořit	k5eAaImIp3nS
téměř	téměř	k6eAd1
polovinu	polovina	k1gFnSc4
volených	volený	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
Akademického	akademický	k2eAgInSc2d1
senátu	senát	k1gInSc2
FF	ff	kA
UK	UK	kA
a	a	k8xC
sdružují	sdružovat	k5eAaImIp3nP
se	se	k3xPyFc4
do	do	k7c2
více	hodně	k6eAd2
než	než	k8xS
třiceti	třicet	k4xCc2
aktivních	aktivní	k2eAgMnPc2d1
studentských	studentský	k2eAgMnPc2d1
spolků	spolek	k1gInPc2
a	a	k8xC
iniciativ	iniciativa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc1
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Pod	pod	k7c7
názvem	název	k1gInSc7
fakulta	fakulta	k1gFnSc1
svobodných	svobodný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
Karlem	Karel	k1gMnSc7
IV	IV	kA
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1348	#num#	k4
jako	jako	k9
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
původních	původní	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc6
„	„	k?
<g/>
filozofická	filozofický	k2eAgFnSc1d1
<g/>
“	“	k?
v	v	k7c6
názvu	název	k1gInSc6
souvisí	souviset	k5eAaImIp3nS
se	se	k3xPyFc4
skutečností	skutečnost	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
minulosti	minulost	k1gFnSc6
byla	být	k5eAaImAgFnS
filosofie	filosofie	k1gFnSc1
chápána	chápat	k5eAaImNgFnS
šířeji	šířej	k1gMnPc7
než	než	k8xS
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ve	v	k7c6
smyslu	smysl	k1gInSc6
věda	věda	k1gFnSc1
nebo	nebo	k8xC
láska	láska	k1gFnSc1
k	k	k7c3
moudrosti	moudrost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tehdejší	tehdejší	k2eAgFnSc6d1
době	doba	k1gFnSc6
bylo	být	k5eAaImAgNnS
zvykem	zvyk	k1gInSc7
absolvovat	absolvovat	k5eAaPmF
nejprve	nejprve	k6eAd1
fakultu	fakulta	k1gFnSc4
svobodných	svobodný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
studenti	student	k1gMnPc1
vzdělávali	vzdělávat	k5eAaImAgMnP
především	především	k6eAd1
v	v	k7c6
rétorice	rétorika	k1gFnSc6
a	a	k8xC
filosofii	filosofie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
fakulta	fakulta	k1gFnSc1
záhy	záhy	k6eAd1
stala	stát	k5eAaPmAgFnS
nejpočetnější	početní	k2eAgFnSc7d3
součástí	součást	k1gFnSc7
univerzity	univerzita	k1gFnSc2
a	a	k8xC
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1366	#num#	k4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
mistrům	mistr	k1gMnPc3
svobodných	svobodný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
první	první	k4xOgFnSc4
pražskou	pražský	k2eAgFnSc4d1
kolej	kolej	k1gFnSc4
–	–	k?
Karolinum	Karolinum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
husitských	husitský	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
fakulta	fakulta	k1gFnSc1
svobodných	svobodný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
na	na	k7c4
dvě	dva	k4xCgNnPc4
století	století	k1gNnSc2
jádrem	jádro	k1gNnSc7
celé	celá	k1gFnSc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
oficiálně	oficiálně	k6eAd1
nazývat	nazývat	k5eAaImF
Filozofickou	filozofický	k2eAgFnSc7d1
fakultou	fakulta	k1gFnSc7
a	a	k8xC
až	až	k6eAd1
do	do	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
poskytovala	poskytovat	k5eAaImAgFnS
průpravné	průpravný	k2eAgNnSc4d1
vyšší	vysoký	k2eAgNnSc4d2
vzdělání	vzdělání	k1gNnSc4
pro	pro	k7c4
budoucí	budoucí	k2eAgMnPc4d1
studenty	student	k1gMnPc4
jiných	jiný	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zde	zde	k6eAd1
možno	možno	k6eAd1
studovat	studovat	k5eAaImF
kromě	kromě	k7c2
filozofie	filozofie	k1gFnSc2
také	také	k9
estetické	estetický	k2eAgFnSc2d1
nauky	nauka	k1gFnSc2
<g/>
,	,	kIx,
pedagogiku	pedagogika	k1gFnSc4
<g/>
,	,	kIx,
matematiku	matematika	k1gFnSc4
<g/>
,	,	kIx,
astronomii	astronomie	k1gFnSc4
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
inženýrské	inženýrský	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
ekonomii	ekonomie	k1gFnSc4
a	a	k8xC
historii	historie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
kromě	kromě	k7c2
orientalistiky	orientalistika	k1gFnSc2
<g/>
,	,	kIx,
archeologie	archeologie	k1gFnSc2
a	a	k8xC
religionistiky	religionistika	k1gFnSc2
významně	významně	k6eAd1
rozvíjet	rozvíjet	k5eAaImF
i	i	k9
velké	velký	k2eAgInPc4d1
filologické	filologický	k2eAgInPc4d1
obory	obor	k1gInPc4
jako	jako	k8xC,k8xS
český	český	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
italština	italština	k1gFnSc1
<g/>
,	,	kIx,
francouzština	francouzština	k1gFnSc1
<g/>
,	,	kIx,
angličtina	angličtina	k1gFnSc1
a	a	k8xC
hebrejština	hebrejština	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
liberalizaci	liberalizace	k1gFnSc6
výuky	výuka	k1gFnSc2
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
otevřít	otevřít	k5eAaPmF
studijní	studijní	k2eAgInPc4d1
obory	obor	k1gInPc4
filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
i	i	k8xC
ženám	žena	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
přes	přes	k7c4
rozdělení	rozdělení	k1gNnSc4
univerzity	univerzita	k1gFnSc2
na	na	k7c4
českou	český	k2eAgFnSc4d1
a	a	k8xC
německou	německý	k2eAgFnSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1882	#num#	k4
a	a	k8xC
osamostatnění	osamostatnění	k1gNnSc4
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
hrála	hrát	k5eAaImAgFnS
fakulta	fakulta	k1gFnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
celé	celý	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
klíčovou	klíčový	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
uzavření	uzavření	k1gNnSc2
školy	škola	k1gFnSc2
nacistickými	nacistický	k2eAgMnPc7d1
okupanty	okupant	k1gMnPc7
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
bylo	být	k5eAaImAgNnS
spojeno	spojit	k5eAaPmNgNnS
s	s	k7c7
perzekucí	perzekuce	k1gFnSc7
učitelů	učitel	k1gMnPc2
a	a	k8xC
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
osvobození	osvobození	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
zažila	zažít	k5eAaPmAgFnS
FF	ff	k0
UK	UK	kA
několik	několik	k4yIc4
plodných	plodný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
byla	být	k5eAaImAgNnP
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
násilně	násilně	k6eAd1
ukončena	ukončit	k5eAaPmNgFnS
nástupem	nástup	k1gInSc7
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
,	,	kIx,
po	po	k7c6
němž	jenž	k3xRgInSc6
prestiž	prestiž	k1gFnSc4
školy	škola	k1gFnSc2
kvůli	kvůli	k7c3
nucenému	nucený	k2eAgInSc3d1
odchodu	odchod	k1gInSc3
desítek	desítka	k1gFnPc2
vynikajících	vynikající	k2eAgMnPc2d1
pedagogů	pedagog	k1gMnPc2
a	a	k8xC
zavedení	zavedení	k1gNnSc4
marxisticko-leninských	marxisticko-leninský	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
poklesla	poklesnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
fakulta	fakulta	k1gFnSc1
pomalu	pomalu	k6eAd1
začala	začít	k5eAaPmAgFnS
otvírat	otvírat	k5eAaImF
význačným	význačný	k2eAgFnPc3d1
osobnostem	osobnost	k1gFnPc3
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
reformní	reformní	k2eAgInSc4d1
proces	proces	k1gInSc4
zastavila	zastavit	k5eAaPmAgFnS
sovětská	sovětský	k2eAgFnSc1d1
okupace	okupace	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
a	a	k8xC
následná	následný	k2eAgFnSc1d1
normalizace	normalizace	k1gFnSc1
československé	československý	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
pasivitě	pasivita	k1gFnSc3
a	a	k8xC
konformitě	konformita	k1gFnSc3
většiny	většina	k1gFnSc2
českého	český	k2eAgInSc2d1
národa	národ	k1gInSc2
protestoval	protestovat	k5eAaBmAgMnS
sebeupálením	sebeupálení	k1gNnPc3
student	student	k1gMnSc1
této	tento	k3xDgFnSc2
fakulty	fakulta	k1gFnSc2
Jan	Jan	k1gMnSc1
Palach	palach	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Až	až	k6eAd1
po	po	k7c6
porážce	porážka	k1gFnSc6
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
a	a	k8xC
odchodu	odchod	k1gInSc2
jeho	jeho	k3xOp3gInPc2
kompromitovaných	kompromitovaný	k2eAgInPc2d1
exponentů	exponent	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
se	se	k3xPyFc4
fakulta	fakulta	k1gFnSc1
znovu	znovu	k6eAd1
etablovala	etablovat	k5eAaBmAgFnS
jako	jako	k9
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejprestižnějších	prestižní	k2eAgInPc2d3
vysokoškolských	vysokoškolský	k2eAgInPc2d1
ústavů	ústav	k1gInPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
téměř	téměř	k6eAd1
sedmisetleté	sedmisetletý	k2eAgFnSc3d1
tradici	tradice	k1gFnSc3
<g/>
,	,	kIx,
úspěšné	úspěšný	k2eAgFnSc6d1
vědecké	vědecký	k2eAgFnSc3d1
a	a	k8xC
pedagogické	pedagogický	k2eAgFnSc3d1
činnosti	činnost	k1gFnSc3
a	a	k8xC
šíři	šíř	k1gFnSc3
studijních	studijní	k2eAgInPc2d1
oborů	obor	k1gInPc2
má	mít	k5eAaImIp3nS
FF	ff	kA
UK	UK	kA
vynikající	vynikající	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
i	i	k9
v	v	k7c6
mezinárodním	mezinárodní	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
Sv.	sv.	kA
Kateřina	Kateřina	k1gFnSc1
jako	jako	k8xS,k8xC
patronka	patronka	k1gFnSc1
pražské	pražský	k2eAgFnSc2d1
filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
<g/>
,	,	kIx,
obraz	obraz	k1gInSc4
Karla	Karel	k1gMnSc2
Škréty	Škréta	k1gMnSc2
v	v	k7c6
Karolinu	Karolinum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
sedm	sedm	k4xCc4
století	století	k1gNnPc2
existence	existence	k1gFnSc2
FF	ff	kA
UK	UK	kA
prošly	projít	k5eAaPmAgFnP
jejími	její	k3xOp3gInPc7
přednáškovými	přednáškový	k2eAgInPc7d1
sály	sál	k1gInPc7
desítky	desítka	k1gFnSc2
výrazných	výrazný	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
již	již	k6eAd1
v	v	k7c6
roli	role	k1gFnSc6
děkanů	děkan	k1gMnPc2
<g/>
,	,	kIx,
pedagogů	pedagog	k1gMnPc2
či	či	k8xC
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Hus	Hus	k1gMnSc1
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
její	její	k3xOp3gMnSc1
děkan	děkan	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
právník	právník	k1gMnSc1
Viktorin	Viktorin	k1gInSc4
Kornel	Kornel	k1gMnSc1
ze	z	k7c2
Všehrd	Všehrda	k1gFnPc2
</s>
<s>
filozofové	filozof	k1gMnPc1
Bernard	Bernard	k1gMnSc1
Bolzano	Bolzana	k1gFnSc5
<g/>
,	,	kIx,
Ernst	Ernst	k1gMnSc1
Mach	Mach	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Patočka	Patočka	k1gMnSc1
</s>
<s>
archeolog	archeolog	k1gMnSc1
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
</s>
<s>
historik	historik	k1gMnSc1
Josef	Josef	k1gMnSc1
Pekař	Pekař	k1gMnSc1
</s>
<s>
státníci	státník	k1gMnPc1
Tomáš	Tomáš	k1gMnSc1
Garrigue	Garrigue	k1gNnSc1
Masaryk	Masaryk	k1gMnSc1
a	a	k8xC
Edvard	Edvard	k1gMnSc1
Beneš	Beneš	k1gMnSc1
</s>
<s>
chemik	chemik	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Heyrovský	Heyrovský	k2eAgMnSc1d1
</s>
<s>
literární	literární	k2eAgMnSc1d1
vědci	vědec	k1gMnPc1
F.	F.	kA
X.	X.	kA
Šalda	Šalda	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Mukařovský	Mukařovský	k1gMnSc1
<g/>
,	,	kIx,
René	René	k1gMnSc1
Wellek	Wellek	k1gMnSc1
</s>
<s>
básník	básník	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Vrchlický	Vrchlický	k1gMnSc1
</s>
<s>
fyzik	fyzik	k1gMnSc1
Albert	Albert	k1gMnSc1
Einstein	Einstein	k1gMnSc1
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
Karel	Karel	k1gMnSc1
Čapek	Čapek	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Palach	palach	k1gInSc4
</s>
<s>
Ze	z	k7c2
současných	současný	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
české	český	k2eAgFnSc2d1
kulturní	kulturní	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
zde	zde	k6eAd1
studovali	studovat	k5eAaImAgMnP
spisovatelé	spisovatel	k1gMnPc1
Vít	Vít	k1gMnSc1
Kremlička	Kremlička	k1gFnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
Viewegh	Viewegh	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
Urban	Urban	k1gMnSc1
<g/>
,	,	kIx,
Radka	Radka	k1gFnSc1
Denemarková	Denemarková	k1gFnSc1
<g/>
,	,	kIx,
Petra	Petra	k1gFnSc1
Hůlová	Hůlová	k1gFnSc1
či	či	k8xC
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
Srnka	srnka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FF	ff	kA
UK	UK	kA
absolvovala	absolvovat	k5eAaPmAgFnS
i	i	k9
ombudsmanka	ombudsmanka	k1gFnSc1
Anna	Anna	k1gFnSc1
Šabatová	Šabatová	k1gFnSc1
a	a	k8xC
předsedkyně	předsedkyně	k1gFnSc1
Akreditační	akreditační	k2eAgFnSc2d1
komise	komise	k1gFnSc2
ČR	ČR	kA
politoložka	politoložka	k1gFnSc1
Vladimíra	Vladimíra	k1gFnSc1
Dvořáková	Dvořáková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Studium	studium	k1gNnSc1
</s>
<s>
Fakultní	fakultní	k2eAgFnSc1d1
pečeť	pečeť	k1gFnSc1
z	z	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
z	z	k7c2
let	léto	k1gNnPc2
1924-1930	1924-1930	k4
podle	podle	k7c2
návrhu	návrh	k1gInSc2
architekta	architekt	k1gMnSc2
Josefa	Josef	k1gMnSc2
Sakaře	sakař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Interiér	interiér	k1gInSc1
hlavní	hlavní	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
na	na	k7c6
Palachově	palachově	k6eAd1
náměstí	náměstí	k1gNnSc6
</s>
<s>
Historický	historický	k2eAgInSc1d1
kabinet	kabinet	k1gInSc1
s	s	k7c7
původním	původní	k2eAgNnSc7d1
vybavením	vybavení	k1gNnSc7
z	z	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Velikostí	velikost	k1gFnSc7
své	svůj	k3xOyFgFnSc2
akademické	akademický	k2eAgFnSc2d1
obce	obec	k1gFnSc2
fakulta	fakulta	k1gFnSc1
předčí	předčit	k5eAaBmIp3nS,k5eAaPmIp3nS
i	i	k9
většinu	většina	k1gFnSc4
českých	český	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drží	držet	k5eAaImIp3nS
primát	primát	k1gInSc4
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
do	do	k7c2
počtu	počet	k1gInSc2
studijních	studijní	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
však	však	k9
většinu	většina	k1gFnSc4
studuje	studovat	k5eAaImIp3nS
relativně	relativně	k6eAd1
malý	malý	k2eAgInSc1d1
počet	počet	k1gInSc1
posluchačů	posluchač	k1gMnPc2
v	v	k7c6
řádu	řád	k1gInSc6
desítek	desítka	k1gFnPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
i	i	k9
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
malé	malý	k2eAgInPc4d1
obory	obor	k1gInPc4
jako	jako	k8xS,k8xC
arabistika	arabistika	k1gFnSc1
<g/>
,	,	kIx,
íránistika	íránistika	k1gFnSc1
<g/>
,	,	kIx,
turkologie	turkologie	k1gFnSc1
<g/>
,	,	kIx,
koreanistika	koreanistika	k1gFnSc1
<g/>
,	,	kIx,
indonesistika	indonesistika	k1gFnSc1
<g/>
,	,	kIx,
logika	logika	k1gFnSc1
či	či	k8xC
estetika	estetika	k1gFnSc1
jsou	být	k5eAaImIp3nP
vyučovány	vyučovat	k5eAaImNgInP
na	na	k7c6
specializovaných	specializovaný	k2eAgNnPc6d1
pracovištích	pracoviště	k1gNnPc6
špičkové	špičkový	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hloubková	hloubkový	k2eAgFnSc1d1
výuka	výuka	k1gFnSc1
tak	tak	k8xC,k8xS
pestré	pestrý	k2eAgFnPc1d1
palety	paleta	k1gFnPc1
malých	malý	k2eAgInPc2d1
oborů	obor	k1gInPc2
nemá	mít	k5eNaImIp3nS
na	na	k7c6
jiných	jiný	k2eAgFnPc6d1
fakultách	fakulta	k1gFnPc6
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
obdobu	obdoba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
FF	ff	kA
UK	UK	kA
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
jedinou	jediný	k2eAgFnSc7d1
fakultou	fakulta	k1gFnSc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
učí	učit	k5eAaImIp3nP
všechny	všechen	k3xTgInPc1
jazyky	jazyk	k1gInPc1
<g/>
,	,	kIx,
kterými	který	k3yIgInPc7,k3yRgInPc7,k3yQgInPc7
se	se	k3xPyFc4
mluví	mluvit	k5eAaImIp3nS
v	v	k7c6
členských	členský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
obecně	obecně	k6eAd1
nejrozšířenějších	rozšířený	k2eAgMnPc2d3
germánských	germánský	k2eAgMnPc2d1
<g/>
,	,	kIx,
románských	románský	k2eAgInPc2d1
a	a	k8xC
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
zde	zde	k6eAd1
lze	lze	k6eAd1
studovat	studovat	k5eAaImF
africké	africký	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
,	,	kIx,
albánštinu	albánština	k1gFnSc4
<g/>
,	,	kIx,
bengálštinu	bengálština	k1gFnSc4
<g/>
,	,	kIx,
hebrejštinu	hebrejština	k1gFnSc4
<g/>
,	,	kIx,
hindštinu	hindština	k1gFnSc4
<g/>
,	,	kIx,
indonéštinu	indonéština	k1gFnSc4
<g/>
,	,	kIx,
japonštinu	japonština	k1gFnSc4
<g/>
,	,	kIx,
mongolštinu	mongolština	k1gFnSc4
<g/>
,	,	kIx,
vietnamštinu	vietnamština	k1gFnSc4
či	či	k8xC
sanskrt	sanskrt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolventi	absolvent	k1gMnPc1
těchto	tento	k3xDgInPc2
oborů	obor	k1gInPc2
se	se	k3xPyFc4
uplatňují	uplatňovat	k5eAaImIp3nP
v	v	k7c6
diplomatických	diplomatický	k2eAgFnPc6d1
a	a	k8xC
tlumočnických	tlumočnický	k2eAgFnPc6d1
službách	služba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgNnPc4d1
a	a	k8xC
podle	podle	k7c2
počtu	počet	k1gInSc2
studentů	student	k1gMnPc2
největší	veliký	k2eAgFnSc2d3
obory	obora	k1gFnSc2
patří	patřit	k5eAaImIp3nP
velké	velký	k2eAgFnPc1d1
filologie	filologie	k1gFnPc1
(	(	kIx(
<g/>
bohemistika	bohemistika	k1gFnSc1
<g/>
,	,	kIx,
anglistika	anglistika	k1gFnSc1
<g/>
,	,	kIx,
germanistika	germanistika	k1gFnSc1
<g/>
,	,	kIx,
hispanistika	hispanistika	k1gFnSc1
<g/>
,	,	kIx,
romanistika	romanistika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
historie	historie	k1gFnSc2
a	a	k8xC
filozofie	filozofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
toho	ten	k3xDgInSc2
nabízí	nabízet	k5eAaImIp3nP
FF	ff	kA
UK	UK	kA
široké	široký	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
disciplín	disciplína	k1gFnPc2
od	od	k7c2
tradiční	tradiční	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
<g/>
,	,	kIx,
religionistiky	religionistika	k1gFnSc2
<g/>
,	,	kIx,
logiky	logika	k1gFnSc2
a	a	k8xC
estetiky	estetika	k1gFnSc2
po	po	k7c4
pedagogiku	pedagogika	k1gFnSc4
<g/>
,	,	kIx,
psychologii	psychologie	k1gFnSc4
<g/>
,	,	kIx,
sociologii	sociologie	k1gFnSc4
<g/>
,	,	kIx,
politologii	politologie	k1gFnSc4
<g/>
,	,	kIx,
andragogiku	andragogika	k1gFnSc4
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgFnSc4d1
a	a	k8xC
filmovou	filmový	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
až	až	k9
po	po	k7c4
progresivní	progresivní	k2eAgNnPc4d1
studia	studio	k1gNnPc4
nových	nový	k2eAgNnPc2d1
médií	médium	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Díky	díky	k7c3
četným	četný	k2eAgInPc3d1
mezinárodním	mezinárodní	k2eAgInPc3d1
výměnným	výměnný	k2eAgInPc3d1
programům	program	k1gInPc3
tráví	trávit	k5eAaImIp3nS
řada	řada	k1gFnSc1
studentů	student	k1gMnPc2
část	část	k1gFnSc1
svého	svůj	k3xOyFgNnSc2
studia	studio	k1gNnSc2
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
přijíždí	přijíždět	k5eAaImIp3nS
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
několik	několik	k4yIc1
stovek	stovka	k1gFnPc2
zahraničních	zahraniční	k2eAgMnPc2d1
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
tyto	tento	k3xDgInPc4
programy	program	k1gInPc4
patří	patřit	k5eAaImIp3nS
např.	např.	kA
Erasmus	Erasmus	k1gInSc1
<g/>
,	,	kIx,
CEEPUS	CEEPUS	kA
<g/>
,	,	kIx,
Visegrádský	visegrádský	k2eAgInSc1d1
fond	fond	k1gInSc1
<g/>
,	,	kIx,
DAAD	DAAD	kA
<g/>
,	,	kIx,
Aktion	Aktion	k1gInSc1
<g/>
,	,	kIx,
vládní	vládní	k2eAgNnPc4d1
stipendia	stipendium	k1gNnPc4
či	či	k8xC
řada	řada	k1gFnSc1
bilaterálních	bilaterální	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Studijní	studijní	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
</s>
<s>
Studijní	studijní	k2eAgInPc1d1
obory	obor	k1gInPc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Archeologie	archeologie	k1gFnSc1
pravěku	pravěk	k1gInSc2
a	a	k8xC
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
Egyptologie	egyptologie	k1gFnSc1
<g/>
,	,	kIx,
Archivnictví	archivnictví	k1gNnSc1
a	a	k8xC
pomocné	pomocný	k2eAgFnPc1d1
vědy	věda	k1gFnPc1
historické	historický	k2eAgFnPc1d1
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
a	a	k8xC
kultura	kultura	k1gFnSc1
islámských	islámský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
a	a	k8xC
kultura	kultura	k1gFnSc1
východního	východní	k2eAgNnSc2d1
Středomoří	středomoří	k1gNnSc2
ve	v	k7c6
starověku	starověk	k1gInSc6
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc4
antické	antický	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
<g/>
,	,	kIx,
Historie	historie	k1gFnSc1
<g/>
,	,	kIx,
Hospodářské	hospodářský	k2eAgFnPc1d1
a	a	k8xC
sociální	sociální	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
<g/>
,	,	kIx,
České	český	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
v	v	k7c6
evropském	evropský	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
<g/>
,	,	kIx,
Židovská	židovská	k1gFnSc1
studia	studio	k1gNnSc2
aj.	aj.	kA
</s>
<s>
Jazyk	jazyk	k1gInSc1
a	a	k8xC
literatura	literatura	k1gFnSc1
</s>
<s>
Anglistika	anglistika	k1gFnSc1
–	–	k?
amerikanistika	amerikanistika	k1gFnSc1
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
a	a	k8xC
literatura	literatura	k1gFnSc1
<g/>
,	,	kIx,
Finská	finský	k2eAgNnPc1d1
studia	studio	k1gNnPc1
<g/>
,	,	kIx,
Fonetika	fonetika	k1gFnSc1
<g/>
,	,	kIx,
Francouzská	francouzský	k2eAgFnSc1d1
filologie	filologie	k1gFnSc1
<g/>
,	,	kIx,
Hispanistika	hispanistika	k1gFnSc1
<g/>
,	,	kIx,
Indologie	indologie	k1gFnSc1
<g/>
,	,	kIx,
Indonesistika	Indonesistika	k1gFnSc1
<g/>
,	,	kIx,
Íránistika	íránistika	k1gFnSc1
<g/>
,	,	kIx,
Italianistika	Italianistika	k1gFnSc1
<g/>
,	,	kIx,
Japonská	japonský	k2eAgNnPc1d1
studia	studio	k1gNnPc1
<g/>
,	,	kIx,
Jazyky	jazyk	k1gInPc1
a	a	k8xC
komunikace	komunikace	k1gFnSc1
neslyšících	slyšící	k2eNgMnPc2d1
<g/>
,	,	kIx,
Koreanistika	Koreanistika	k1gFnSc1
<g/>
,	,	kIx,
Mongolistika	Mongolistika	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Německý	německý	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
a	a	k8xC
literatura	literatura	k1gFnSc1
<g/>
,	,	kIx,
Nizozemský	nizozemský	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
a	a	k8xC
literatura	literatura	k1gFnSc1
<g/>
,	,	kIx,
Obecná	obecný	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
<g/>
,	,	kIx,
Portugalistika	Portugalistika	k1gFnSc1
<g/>
,	,	kIx,
Romistika	romistika	k1gFnSc1
<g/>
,	,	kIx,
Rusistika	rusistika	k1gFnSc1
<g/>
,	,	kIx,
Sinologie	sinologie	k1gFnSc1
<g/>
,	,	kIx,
Švédská	švédský	k2eAgNnPc1d1
studia	studio	k1gNnPc1
<g/>
,	,	kIx,
Překladatelství	překladatelství	k1gNnSc1
a	a	k8xC
tlumočnictví	tlumočnictví	k1gNnSc1
(	(	kIx(
<g/>
Angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
Francouzština	francouzština	k1gFnSc1
<g/>
,	,	kIx,
Němčina	němčina	k1gFnSc1
<g/>
,	,	kIx,
Ruština	ruština	k1gFnSc1
<g/>
,	,	kIx,
Španělština	španělština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Člověk	člověk	k1gMnSc1
a	a	k8xC
společnost	společnost	k1gFnSc1
</s>
<s>
Psychologie	psychologie	k1gFnPc1
<g/>
,	,	kIx,
Sociologie	sociologie	k1gFnPc1
<g/>
,	,	kIx,
Sociologicko-ekonomická	sociologicko-ekonomický	k2eAgNnPc1d1
studia	studio	k1gNnPc1
<g/>
,	,	kIx,
Politologie	politologie	k1gFnPc1
<g/>
,	,	kIx,
Politické	politický	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
<g/>
,	,	kIx,
Filozofie	filozofie	k1gFnSc1
<g/>
,	,	kIx,
Religionistika	religionistika	k1gFnSc1
<g/>
,	,	kIx,
Andragogika	andragogika	k1gFnSc1
a	a	k8xC
personální	personální	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
<g/>
,	,	kIx,
Pedagogika	pedagogika	k1gFnSc1
<g/>
,	,	kIx,
Sociální	sociální	k2eAgFnSc1d1
pedagogika	pedagogika	k1gFnSc1
<g/>
,	,	kIx,
Studia	studio	k1gNnSc2
nových	nový	k2eAgNnPc2d1
médií	médium	k1gNnPc2
<g/>
,	,	kIx,
Informační	informační	k2eAgNnPc1d1
studia	studio	k1gNnPc1
a	a	k8xC
knihovnictví	knihovnictví	k1gNnPc1
</s>
<s>
Umění	umění	k1gNnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
Divadelní	divadelní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
<g/>
,	,	kIx,
Estetika	estetika	k1gFnSc1
<g/>
,	,	kIx,
Filmová	filmový	k2eAgNnPc1d1
studia	studio	k1gNnPc1
<g/>
,	,	kIx,
Hudební	hudební	k2eAgFnSc1d1
věda	věda	k1gFnSc1
</s>
<s>
Učitelství	učitelství	k1gNnSc1
</s>
<s>
Učitelství	učitelství	k1gNnSc1
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
pro	pro	k7c4
střední	střední	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
,	,	kIx,
Učitelství	učitelství	k1gNnSc4
češtiny	čeština	k1gFnSc2
jako	jako	k8xS,k8xC
cizího	cizí	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
Učitelství	učitelství	k1gNnSc4
latinského	latinský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
pro	pro	k7c4
střední	střední	k2eAgFnPc4d1
školy	škola	k1gFnPc4
</s>
<s>
Akce	akce	k1gFnSc1
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
tzv.	tzv.	kA
třetí	třetí	k4xOgFnSc7
rolí	role	k1gFnSc7
univerzity	univerzita	k1gFnSc2
považuje	považovat	k5eAaImIp3nS
FF	ff	kA
UK	UK	kA
za	za	k7c4
svoji	svůj	k3xOyFgFnSc4
společenskou	společenský	k2eAgFnSc4d1
roli	role	k1gFnSc4
kromě	kromě	k7c2
výuky	výuka	k1gFnSc2
a	a	k8xC
výzkumu	výzkum	k1gInSc2
také	také	k9
přispívání	přispívání	k1gNnSc4
do	do	k7c2
aktuální	aktuální	k2eAgFnSc2d1
veřejné	veřejný	k2eAgFnSc2d1
debaty	debata	k1gFnSc2
svými	svůj	k3xOyFgMnPc7
odborníky	odborník	k1gMnPc7
a	a	k8xC
výzkumem	výzkum	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořádá	pořádat	k5eAaImIp3nS
proto	proto	k8xC
řadu	řada	k1gFnSc4
workshopů	workshop	k1gInPc2
<g/>
,	,	kIx,
přednášek	přednáška	k1gFnPc2
a	a	k8xC
festivalů	festival	k1gInPc2
pro	pro	k7c4
širokou	široký	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
<g/>
,	,	kIx,
žáky	žák	k1gMnPc4
základních	základní	k2eAgFnPc2d1
a	a	k8xC
středních	střední	k2eAgFnPc2d1
škol	škola	k1gFnPc2
či	či	k8xC
seniory	senior	k1gMnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
zájmové	zájmový	k2eAgNnSc1d1
studium	studium	k1gNnSc1
pro	pro	k7c4
seniory	senior	k1gMnPc4
Univerzita	univerzita	k1gFnSc1
třetího	třetí	k4xOgInSc2
věku	věk	k1gInSc2
</s>
<s>
workshopy	workshop	k1gInPc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
9	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
ZŠ	ZŠ	kA
Dětská	dětský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
</s>
<s>
festival	festival	k1gInSc1
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
Open	Open	k1gNnSc4
Square	square	k1gInSc1
</s>
<s>
festival	festival	k1gInSc1
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
Týden	týden	k1gInSc1
diverzity	diverzita	k1gFnPc1
</s>
<s>
popularizační	popularizační	k2eAgFnSc1d1
akce	akce	k1gFnSc1
Den	den	k1gInSc4
vědy	věda	k1gFnSc2
</s>
<s>
Noc	noc	k1gFnSc1
filosofie	filosofie	k1gFnSc1
</s>
<s>
S	s	k7c7
veřejností	veřejnost	k1gFnSc7
na	na	k7c6
její	její	k3xOp3gFnSc6
půdě	půda	k1gFnSc6
debatovali	debatovat	k5eAaImAgMnP
osobnosti	osobnost	k1gFnPc4
jako	jako	k8xC,k8xS
dalajláma	dalajláma	k1gMnSc1
<g/>
,	,	kIx,
Madeleine	Madeleine	k1gFnSc1
Albrightová	Albrightová	k1gFnSc1
<g/>
,	,	kIx,
čínský	čínský	k2eAgMnSc1d1
disident	disident	k1gMnSc1
Aj	aj	kA
Wej-Wej	Wej-Wej	k1gInSc1
či	či	k8xC
ruský	ruský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Zubov	Zubovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Studentský	studentský	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Studentské	studentský	k2eAgInPc1d1
spolky	spolek	k1gInPc1
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
UK	UK	kA
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
FF	ff	kA
UK	UK	kA
působí	působit	k5eAaImIp3nS
kolem	kolem	k7c2
třiceti	třicet	k4xCc2
studentských	studentský	k2eAgInPc2d1
spolků	spolek	k1gInPc2
a	a	k8xC
iniciativ	iniciativa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studentský	studentský	k2eAgInSc1d1
život	život	k1gInSc1
zde	zde	k6eAd1
má	mít	k5eAaImIp3nS
silnou	silný	k2eAgFnSc4d1
a	a	k8xC
dlouhou	dlouhý	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Studentská	studentský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Přímo	přímo	k6eAd1
na	na	k7c6
fakultní	fakultní	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
působí	působit	k5eAaImIp3nS
Studentská	studentský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
výkonným	výkonný	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
občanského	občanský	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
Studentský	studentský	k2eAgInSc1d1
fond	fond	k1gInSc1
FF	ff	kA
UK	UK	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
založili	založit	k5eAaPmAgMnP
studentští	studentský	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
revolučních	revoluční	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
sdružení	sdružení	k1gNnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
o	o	k7c4
zpestření	zpestření	k1gNnSc4
studentského	studentský	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
často	často	k6eAd1
zastupuje	zastupovat	k5eAaImIp3nS
studenty	student	k1gMnPc4
fakulty	fakulta	k1gFnSc2
při	při	k7c6
jednání	jednání	k1gNnSc6
s	s	k7c7
jejím	její	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
<g/>
,	,	kIx,
popř.	popř.	kA
s	s	k7c7
univerzitou	univerzita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radní	radní	k2eAgFnSc1d1
pravidelně	pravidelně	k6eAd1
finančně	finančně	k6eAd1
podporují	podporovat	k5eAaImIp3nP
vybrané	vybraný	k2eAgInPc4d1
studentské	studentský	k2eAgInPc4d1
projekty	projekt	k1gInPc4
jako	jako	k8xC,k8xS
časopisy	časopis	k1gInPc4
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgInPc4d1
spolky	spolek	k1gInPc4
<g/>
,	,	kIx,
festivaly	festival	k1gInPc4
a	a	k8xC
veřejné	veřejný	k2eAgInPc4d1
přednáškové	přednáškový	k2eAgInPc4d1
cykly	cyklus	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
vydávají	vydávat	k5eAaPmIp3nP,k5eAaImIp3nP
brožuru	brožura	k1gFnSc4
OFFŠEM	OFFŠEM	kA
pro	pro	k7c4
studenty	student	k1gMnPc4
prvních	první	k4xOgNnPc2
ročníků	ročník	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k6eAd1
se	se	k3xPyFc4
zorientovat	zorientovat	k5eAaPmF
na	na	k7c6
fakultě	fakulta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
připravují	připravovat	k5eAaImIp3nP
lekci	lekce	k1gFnSc4
FAUST	FAUST	kA
(	(	kIx(
<g/>
Fakultní	fakultní	k2eAgNnSc1d1
uvítání	uvítání	k1gNnSc1
studentů	student	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gInSc6
konci	konec	k1gInSc6
jsou	být	k5eAaImIp3nP
prváci	prvák	k1gMnPc1
svérázným	svérázný	k2eAgNnPc3d1
způsobem	způsob	k1gInSc7
přijati	přijmout	k5eAaPmNgMnP
na	na	k7c4
akademickou	akademický	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
FF	ff	kA
UK	UK	kA
za	za	k7c4
rok	rok	k1gInSc4
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6	#num#	k4
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
↑	↑	k?
Akademický	akademický	k2eAgInSc1d1
senát	senát	k1gInSc1
FF	ff	kA
UK	UK	kA
online	onlinout	k5eAaPmIp3nS
a	a	k8xC
studentské	studentský	k2eAgInPc1d1
spolky	spolek	k1gInPc1
FF	ff	kA
UK	UK	kA
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Multimediální	multimediální	k2eAgInSc4d1
projekt	projekt	k1gInSc4
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
Web	web	k1gInSc1
Jan	Jan	k1gMnSc1
Palach	palach	k1gInSc1
online	onlinout	k5eAaPmIp3nS
Archivováno	archivován	k2eAgNnSc4d1
22	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Stručný	stručný	k2eAgInSc4d1
přehled	přehled	k1gInSc4
dějin	dějiny	k1gFnPc2
FF	ff	kA
UK	UK	kA
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Seznam	seznam	k1gInSc1
studijních	studijní	k2eAgInPc2d1
oborů	obor	k1gInPc2
FF	ff	kA
UK	UK	kA
otevíraných	otevíraný	k2eAgInPc2d1
pro	pro	k7c4
akademický	akademický	k2eAgInSc4d1
rok	rok	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
↑	↑	k?
Studium	studium	k1gNnSc1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Akce	akce	k1gFnSc1
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
třetího	třetí	k4xOgInSc2
věku	věk	k1gInSc2
<g/>
,	,	kIx,
Dětská	dětský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Open	Open	k1gInSc1
Square	square	k1gInSc1
<g/>
,	,	kIx,
Týden	týden	k1gInSc1
diverzity	diverzita	k1gFnSc2
<g/>
,	,	kIx,
Den	den	k1gInSc4
vědy	věda	k1gFnSc2
<g/>
↑	↑	k?
Studentská	studentská	k1gFnSc1
rada	rada	k1gMnSc1
FF	ff	kA
UK	UK	kA
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Historie	historie	k1gFnSc1
Studentské	studentský	k2eAgFnPc1d1
rady	rada	k1gFnPc1
FF	ff	kA
UK	UK	kA
online	onlinout	k5eAaPmIp3nS
Archivováno	archivován	k2eAgNnSc4d1
15	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Petráň	Petráň	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Nástin	nástin	k1gInSc1
dějin	dějiny	k1gFnPc2
filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Svatoš	Svatoš	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
(	(	kIx(
<g/>
red	red	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc4
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
I.	I.	kA
(	(	kIx(
<g/>
1347	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
–	–	k?
<g/>
1622	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Čornejová	Čornejový	k2eAgFnSc1d1
<g/>
,	,	kIx,
I.	I.	kA
(	(	kIx(
<g/>
red	red	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc4
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1622	#num#	k4
<g/>
–	–	k?
<g/>
1802	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Černý	Černý	k1gMnSc1
<g/>
,	,	kIx,
Fr.	Fr.	k1gMnSc1
<g/>
:	:	kIx,
Normalizace	normalizace	k1gFnSc1
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzpomínky	vzpomínka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Holý	Holý	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
Volná	volný	k2eAgFnSc1d1
<g/>
,	,	kIx,
K.	K.	kA
(	(	kIx(
<g/>
ed	ed	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
fakulta	fakulta	k1gFnSc1
bude	být	k5eAaImBp3nS
rudá	rudý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hlaváček	Hlaváček	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
:	:	kIx,
Otevřená	otevřený	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
a	a	k8xC
její	její	k3xOp3gInPc1
cizinci	cizinec	k1gMnSc3
1918	#num#	k4
<g/>
-	-	kIx~
<g/>
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děkanů	děkan	k1gMnPc2
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
</s>
<s>
Absolventi	absolvent	k1gMnPc1
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
</s>
<s>
Knihovna	knihovna	k1gFnSc1
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
</s>
<s>
Vydavatelství	vydavatelství	k1gNnSc1
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
</s>
<s>
Časopisy	časopis	k1gInPc1
vydávané	vydávaný	k2eAgInPc1d1
Filozofickou	filozofický	k2eAgFnSc7d1
fakultou	fakulta	k1gFnSc7
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
YouTube	YouTubat	k5eAaPmIp3nS
kanál	kanál	k1gInSc4
fakulty	fakulta	k1gFnSc2
</s>
<s>
Stránky	stránka	k1gFnPc1
Studentské	studentský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Prověřená	prověřený	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
stránky	stránka	k1gFnPc1
věnované	věnovaný	k2eAgFnPc1d1
výzkumu	výzkum	k1gInSc2
o	o	k7c6
dějinách	dějiny	k1gFnPc6
FF	ff	kA
UK	UK	kA
1969	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
Samosprávné	samosprávný	k2eAgInPc4d1
orgány	orgán	k1gInPc4
</s>
<s>
Rektor	rektor	k1gMnSc1
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Akademický	akademický	k2eAgInSc1d1
senát	senát	k1gInSc1
Fakulty	fakulta	k1gFnSc2
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1
teologická	teologický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Evangelická	evangelický	k2eAgFnSc1d1
teologická	teologický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Husitská	husitský	k2eAgFnSc1d1
teologická	teologický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Právnická	právnický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
2	#num#	k4
<g/>
.	.	kIx.
lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
3	#num#	k4
<g/>
.	.	kIx.
lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
•	•	k?
Lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
•	•	k?
Farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
•	•	k?
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Matematicko-fyzikální	matematicko-fyzikální	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Fakulta	fakulta	k1gFnSc1
sociálních	sociální	k2eAgFnPc2d1
věd	věda	k1gFnPc2
•	•	k?
Fakulta	fakulta	k1gFnSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
•	•	k?
Fakulta	fakulta	k1gFnSc1
humanitních	humanitní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
Vysokoškolské	vysokoškolský	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
</s>
<s>
Ústav	ústav	k1gInSc1
dějin	dějiny	k1gFnPc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
a	a	k8xC
archiv	archiv	k1gInSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
•	•	k?
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
teoretická	teoretický	k2eAgNnPc4d1
studia	studio	k1gNnPc4
•	•	k?
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
otázky	otázka	k1gFnPc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
•	•	k?
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
ekonomický	ekonomický	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
a	a	k8xC
doktorské	doktorský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
(	(	kIx(
<g/>
CERGE-EI	CERGE-EI	k1gFnSc2
<g/>
)	)	kIx)
Knihovny	knihovna	k1gFnSc2
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
•	•	k?
Knihovna	knihovna	k1gFnSc1
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
•	•	k?
Knihovna	knihovna	k1gFnSc1
Evangelické	evangelický	k2eAgFnSc2d1
teologické	teologický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Jiná	jiný	k2eAgNnPc1d1
pracoviště	pracoviště	k1gNnPc1
</s>
<s>
Ústav	ústav	k1gInSc1
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
•	•	k?
Ústav	ústav	k1gInSc1
jazykové	jazykový	k2eAgFnSc2d1
a	a	k8xC
odborné	odborný	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
•	•	k?
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
přenos	přenos	k1gInSc4
poznatků	poznatek	k1gInPc2
a	a	k8xC
technologií	technologie	k1gFnSc7
•	•	k?
Agentura	agentura	k1gFnSc1
Rady	rada	k1gFnSc2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
Účelová	účelový	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Koleje	kolej	k1gFnPc1
a	a	k8xC
menzy	menza	k1gFnPc1
•	•	k?
Arcibiskupský	arcibiskupský	k2eAgInSc1d1
seminář	seminář	k1gInSc1
•	•	k?
Nakladatelství	nakladatelství	k1gNnSc6
Karolinum	Karolinum	k1gNnSc4
•	•	k?
Správa	správa	k1gFnSc1
budov	budova	k1gFnPc2
a	a	k8xC
zařízení	zařízení	k1gNnPc2
•	•	k?
Sportovní	sportovní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Jednotky	jednotka	k1gFnSc2
fakult	fakulta	k1gFnPc2
</s>
<s>
Botanická	botanický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
(	(	kIx(
<g/>
PřF	PřF	k1gFnSc1
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Astronomický	astronomický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
(	(	kIx(
<g/>
MFF	MFF	kA
UK	UK	kA
<g/>
)	)	kIx)
Dějiny	dějiny	k1gFnPc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
</s>
<s>
1347	#num#	k4
<g/>
–	–	k?
<g/>
1740	#num#	k4
•	•	k?
1740	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
•	•	k?
1918	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
•	•	k?
od	od	k7c2
1945	#num#	k4
•	•	k?
Dekret	dekret	k1gInSc1
kutnohorský	kutnohorský	k2eAgInSc1d1
•	•	k?
Karolinum	Karolinum	k1gNnSc1
•	•	k?
Klementinum	Klementinum	k1gNnSc1
•	•	k?
Německá	německý	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Insigniáda	Insigniáda	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Kultura	kultura	k1gFnSc1
|	|	kIx~
Praha	Praha	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1957	#num#	k4
3595	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
78091141	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
169095543	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
78091141	#num#	k4
</s>
