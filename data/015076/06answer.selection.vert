<s>
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
(	(	kIx(
<g/>
FF	ff	kA
UK	UK	kA
<g/>
)	)	kIx)
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
původních	původní	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
pražské	pražský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1348	#num#	k4
(	(	kIx(
<g/>
zpočátku	zpočátku	k6eAd1
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
fakulta	fakulta	k1gFnSc1
svobodných	svobodný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
neboli	neboli	k8xC
artistická	artistický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tradičním	tradiční	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
české	český	k2eAgFnSc2d1
vzdělanosti	vzdělanost	k1gFnSc2
a	a	k8xC
z	z	k7c2
hlediska	hledisko	k1gNnSc2
vědeckého	vědecký	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
<g />
.	.	kIx.
</s>