<s>
Chová	chovat	k5eAaImIp3nS	chovat
přes	přes	k7c4	přes
600	[number]	k4	600
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
typický	typický	k2eAgMnSc1d1	typický
je	být	k5eAaImIp3nS	být
kůň	kůň	k1gMnSc1	kůň
Převalského	převalský	k2eAgMnSc2d1	převalský
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
má	můj	k3xOp1gFnSc1	můj
zahrada	zahrada	k1gFnSc1	zahrada
i	i	k9	i
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc4	jehož
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
plemennou	plemenný	k2eAgFnSc4d1	plemenná
knihu	kniha	k1gFnSc4	kniha
vede	vést	k5eAaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
