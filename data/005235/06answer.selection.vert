<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
Pico	Pica	k1gMnSc5	Pica
Duarte	Duart	k1gMnSc5	Duart
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysoká	vysoká	k1gFnSc1	vysoká
3087	[number]	k4	3087
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejníže	nízce	k6eAd3	nízce
položeným	položený	k2eAgInSc7d1	položený
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
Enriquillo	Enriquillo	k1gNnSc1	Enriquillo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
