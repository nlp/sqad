<s>
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
Karibském	karibský	k2eAgNnSc6d1	Karibské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
Hispaniola	Hispaniola	k1gFnSc1	Hispaniola
v	v	k7c6	v
souostroví	souostroví	k1gNnSc6	souostroví
Velké	velký	k2eAgFnSc2d1	velká
Antily	Antily	k1gFnPc1	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
sousedním	sousední	k2eAgInSc7d1	sousední
státem	stát	k1gInSc7	stát
je	být	k5eAaImIp3nS	být
Haiti	Haiti	k1gNnSc1	Haiti
<g/>
,	,	kIx,	,
společná	společný	k2eAgFnSc1d1	společná
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
360	[number]	k4	360
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ostrova	ostrov	k1gInSc2	ostrov
Hispaniola	Hispaniola	k1gFnSc1	Hispaniola
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
horská	horský	k2eAgNnPc4d1	horské
pásma	pásmo	k1gNnPc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
Pico	Pica	k1gMnSc5	Pica
Duarte	Duart	k1gMnSc5	Duart
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysoká	vysoká	k1gFnSc1	vysoká
3087	[number]	k4	3087
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejníže	nízce	k6eAd3	nízce
položeným	položený	k2eAgInSc7d1	položený
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
Enriquillo	Enriquillo	k1gNnSc1	Enriquillo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nížiny	nížina	k1gFnPc1	nížina
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
vlhké	vlhký	k2eAgNnSc4d1	vlhké
tropické	tropický	k2eAgNnSc4d1	tropické
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
16	[number]	k4	16
do	do	k7c2	do
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
1000	[number]	k4	1000
do	do	k7c2	do
2000	[number]	k4	2000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
často	často	k6eAd1	často
řádí	řádit	k5eAaImIp3nP	řádit
hurikány	hurikán	k1gInPc1	hurikán
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
40	[number]	k4	40
<g/>
%	%	kIx~	%
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
lesními	lesní	k2eAgInPc7d1	lesní
porosty	porost	k1gInPc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Evropany	Evropan	k1gMnPc4	Evropan
ostrov	ostrov	k1gInSc1	ostrov
objevil	objevit	k5eAaPmAgInS	objevit
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1492	[number]	k4	1492
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
a	a	k8xC	a
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1496	[number]	k4	1496
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
město	město	k1gNnSc1	město
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k1gNnSc1	Domingo
<g/>
.	.	kIx.	.
</s>
<s>
Kolombus	Kolombus	k1gMnSc1	Kolombus
nazval	nazvat	k5eAaPmAgMnS	nazvat
ostrov	ostrov	k1gInSc4	ostrov
"	"	kIx"	"
<g/>
rájem	ráj	k1gInSc7	ráj
na	na	k7c4	na
zemi	zem	k1gFnSc4	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
první	první	k4xOgFnSc1	první
španělská	španělský	k2eAgFnSc1d1	španělská
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
univerzita	univerzita	k1gFnSc1	univerzita
i	i	k8xC	i
nemocnice	nemocnice	k1gFnSc1	nemocnice
v	v	k7c6	v
"	"	kIx"	"
<g/>
Novém	nový	k2eAgInSc6d1	nový
Světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Taínové	Taínová	k1gFnSc2	Taínová
byli	být	k5eAaImAgMnP	být
vesměs	vesměs	k6eAd1	vesměs
vyhubeni	vyhubit	k5eAaPmNgMnP	vyhubit
nebo	nebo	k8xC	nebo
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
na	na	k7c4	na
zavlečené	zavlečený	k2eAgFnPc4d1	zavlečená
nemoci	nemoc	k1gFnPc4	nemoc
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Španělští	španělský	k2eAgMnPc1d1	španělský
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
dovážet	dovážet	k5eAaImF	dovážet
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
a	a	k8xC	a
v	v	k7c6	v
dolech	dol	k1gInPc6	dol
africké	africký	k2eAgFnSc2d1	africká
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
otec	otec	k1gMnSc1	otec
Bartolomé	Bartolomý	k2eAgFnSc2d1	Bartolomý
de	de	k?	de
Las	laso	k1gNnPc2	laso
Casas	Casas	k1gInSc1	Casas
<g/>
,	,	kIx,	,
misionář	misionář	k1gMnSc1	misionář
a	a	k8xC	a
bojovník	bojovník	k1gMnSc1	bojovník
za	za	k7c2	za
práva	právo	k1gNnSc2	právo
indiánů	indián	k1gMnPc2	indián
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
předznamenal	předznamenat	k5eAaPmAgInS	předznamenat
moderní	moderní	k2eAgFnSc4d1	moderní
koncepci	koncepce	k1gFnSc4	koncepce
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1697	[number]	k4	1697
byla	být	k5eAaImAgFnS	být
[	[	kIx(	[
<g/>
Hispaniola	Hispaniola	k1gFnSc1	Hispaniola
<g/>
]]	]]	k?	]]
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
podepsáním	podepsání	k1gNnSc7	podepsání
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
Rijswijku	Rijswijka	k1gFnSc4	Rijswijka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1790	[number]	k4	1790
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
kolonii	kolonie	k1gFnSc6	kolonie
Santo	Sant	k2eAgNnSc4d1	Santo
Domingo	Domingo	k1gNnSc4	Domingo
40	[number]	k4	40
000	[number]	k4	000
bělochů	běloch	k1gMnPc2	běloch
<g/>
,	,	kIx,	,
25	[number]	k4	25
000	[number]	k4	000
svobodných	svobodný	k2eAgMnPc2d1	svobodný
černochů	černoch	k1gMnPc2	černoch
a	a	k8xC	a
mulatů	mulat	k1gMnPc2	mulat
a	a	k8xC	a
60	[number]	k4	60
000	[number]	k4	000
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1791	[number]	k4	1791
-	-	kIx~	-
1803	[number]	k4	1803
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
na	na	k7c6	na
sousedním	sousední	k2eAgNnSc6d1	sousední
Haiti	Haiti	k1gNnSc6	Haiti
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
povstání	povstání	k1gNnSc1	povstání
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
kolonie	kolonie	k1gFnSc1	kolonie
Santo	Sant	k2eAgNnSc4d1	Santo
Domingo	Domingo	k1gNnSc4	Domingo
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
byla	být	k5eAaImAgNnP	být
obsazena	obsadit	k5eAaPmNgNnP	obsadit
vojsky	vojsky	k6eAd1	vojsky
sousedního	sousední	k2eAgInSc2d1	sousední
státu	stát	k1gInSc2	stát
Haiti	Haiti	k1gNnSc2	Haiti
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gFnSc7	jehož
vládou	vláda	k1gFnSc7	vláda
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
ostrov	ostrov	k1gInSc1	ostrov
sjednocen	sjednocen	k2eAgInSc1d1	sjednocen
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
proti	proti	k7c3	proti
haitské	haitský	k2eAgFnSc3d1	Haitská
okupaci	okupace	k1gFnSc3	okupace
rostl	růst	k5eAaImAgMnS	růst
a	a	k8xC	a
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
ozbrojeném	ozbrojený	k2eAgNnSc6d1	ozbrojené
povstání	povstání	k1gNnSc6	povstání
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Pedro	Pedro	k1gNnSc4	Pedro
Santana	Santan	k1gMnSc2	Santan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
republika	republika	k1gFnSc1	republika
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
opět	opět	k6eAd1	opět
stala	stát	k5eAaPmAgFnS	stát
španělskou	španělský	k2eAgFnSc7d1	španělská
kolonií	kolonie	k1gFnSc7	kolonie
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
britským	britský	k2eAgInPc3d1	britský
pokusům	pokus	k1gInPc3	pokus
zmocnit	zmocnit	k5eAaPmF	zmocnit
se	se	k3xPyFc4	se
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
diktátorský	diktátorský	k2eAgMnSc1d1	diktátorský
vůdce	vůdce	k1gMnSc1	vůdce
Ulises	Ulisesa	k1gFnPc2	Ulisesa
Heureaux	Heureaux	k1gInSc1	Heureaux
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Haiti	Haiti	k1gNnSc4	Haiti
byla	být	k5eAaImAgFnS	být
i	i	k9	i
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1916-1924	[number]	k4	1916-1924
obsazena	obsadit	k5eAaPmNgFnS	obsadit
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
si	se	k3xPyFc3	se
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
cly	clo	k1gNnPc7	clo
podržely	podržet	k5eAaPmAgFnP	podržet
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
generál	generál	k1gMnSc1	generál
Rafael	Rafael	k1gMnSc1	Rafael
Trujillo	Trujillo	k1gNnSc4	Trujillo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
většinou	většinou	k6eAd1	většinou
jako	jako	k8xS	jako
velmi	velmi	k6eAd1	velmi
krutý	krutý	k2eAgMnSc1d1	krutý
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1961	[number]	k4	1961
v	v	k7c4	v
Santo	Sant	k2eAgNnSc4d1	Santo
Domingo	Domingo	k1gNnSc4	Domingo
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Trujillo	Trujillo	k1gNnSc4	Trujillo
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
právníka	právník	k1gMnSc2	právník
a	a	k8xC	a
úspěšného	úspěšný	k2eAgMnSc2d1	úspěšný
diplomata	diplomat	k1gMnSc2	diplomat
Joaquína	Joaquín	k1gMnSc2	Joaquín
Balaguera	Balaguer	k1gMnSc2	Balaguer
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
svým	svůj	k3xOyFgMnSc7	svůj
zástupcem	zástupce	k1gMnSc7	zástupce
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
presidentem	president	k1gMnSc7	president
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
presidentem	president	k1gMnSc7	president
levicový	levicový	k2eAgMnSc1d1	levicový
kandidát	kandidát	k1gMnSc1	kandidát
Juan	Juan	k1gMnSc1	Juan
Bosch	Bosch	kA	Bosch
a	a	k8xC	a
po	po	k7c6	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
svržen	svrhnout	k5eAaPmNgInS	svrhnout
vojenským	vojenský	k2eAgInSc7d1	vojenský
převratem	převrat	k1gInSc7	převrat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k9	už
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
proti	proti	k7c3	proti
vojenskému	vojenský	k2eAgInSc3d1	vojenský
režimu	režim	k1gInSc3	režim
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagoval	reagovat	k5eAaBmAgMnS	reagovat
americký	americký	k2eAgMnSc1d1	americký
president	president	k1gMnSc1	president
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gInSc1	Johnson
vojenským	vojenský	k2eAgNnSc7d1	vojenské
obsazením	obsazení	k1gNnSc7	obsazení
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
oddíl	oddíl	k1gInSc4	oddíl
vojáků	voják	k1gMnPc2	voják
Organizace	organizace	k1gFnSc2	organizace
amerických	americký	k2eAgMnPc2d1	americký
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
připravila	připravit	k5eAaPmAgFnS	připravit
nové	nový	k2eAgFnPc4d1	nová
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
Balaguer	Balaguer	k1gMnSc1	Balaguer
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
ostrov	ostrov	k1gInSc4	ostrov
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Balaguer	Balaguer	k1gInSc1	Balaguer
vládl	vládnout	k5eAaImAgInS	vládnout
12	[number]	k4	12
let	léto	k1gNnPc2	léto
jako	jako	k8xS	jako
bezohledný	bezohledný	k2eAgMnSc1d1	bezohledný
diktátor	diktátor	k1gMnSc1	diktátor
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
o	o	k7c4	o
život	život	k1gInSc4	život
asi	asi	k9	asi
11	[number]	k4	11
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
mírnější	mírný	k2eAgFnSc1d2	mírnější
než	než	k8xS	než
Trujillova	Trujillův	k2eAgFnSc1d1	Trujillova
a	a	k8xC	a
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
i	i	k9	i
jistých	jistý	k2eAgInPc2d1	jistý
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
budování	budování	k1gNnSc6	budování
veřejných	veřejný	k2eAgFnPc2d1	veřejná
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
době	doba	k1gFnSc6	doba
světové	světový	k2eAgFnSc2d1	světová
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
zadluženosti	zadluženost	k1gFnSc2	zadluženost
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Dominikánskou	dominikánský	k2eAgFnSc4d1	Dominikánská
republiku	republika	k1gFnSc4	republika
kritické	kritický	k2eAgFnPc4d1	kritická
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc1d1	měnový
fond	fond	k1gInSc1	fond
připravoval	připravovat	k5eAaImAgInS	připravovat
řešení	řešení	k1gNnSc4	řešení
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ovšem	ovšem	k9	ovšem
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
jen	jen	k9	jen
větší	veliký	k2eAgInPc4d2	veliký
nepokoje	nepokoj	k1gInPc4	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1978	[number]	k4	1978
-	-	kIx~	-
1986	[number]	k4	1986
vládli	vládnout	k5eAaImAgMnP	vládnout
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
demokratičtější	demokratický	k2eAgMnSc1d2	demokratičtější
presidenti	president	k1gMnPc1	president
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
zklidnila	zklidnit	k5eAaPmAgFnS	zklidnit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
opět	opět	k6eAd1	opět
Balaguer	Balaguer	k1gInSc1	Balaguer
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
80	[number]	k4	80
let	léto	k1gNnPc2	léto
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
nevidomý	vidomý	k2eNgMnSc1d1	nevidomý
<g/>
,	,	kIx,	,
a	a	k8xC	a
udržel	udržet	k5eAaPmAgInS	udržet
se	se	k3xPyFc4	se
u	u	k7c2	u
vlády	vláda	k1gFnSc2	vláda
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
porazil	porazit	k5eAaPmAgMnS	porazit
Leonel	Leonel	k1gMnSc1	Leonel
Fernández	Fernández	k1gMnSc1	Fernández
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
7,7	[number]	k4	7,7
%	%	kIx~	%
při	při	k7c6	při
nízké	nízký	k2eAgFnSc6d1	nízká
inflaci	inflace	k1gFnSc6	inflace
i	i	k8xC	i
nezaměstnanosti	nezaměstnanost	k1gFnSc6	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc4	volba
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
přesto	přesto	k8xC	přesto
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
levicový	levicový	k2eAgMnSc1d1	levicový
kandidát	kandidát	k1gMnSc1	kandidát
Hipólito	Hipólit	k2eAgNnSc4d1	Hipólito
Mejía	Mejíum	k1gNnSc2	Mejíum
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
kritická	kritický	k2eAgFnSc1d1	kritická
etapa	etapa	k1gFnSc1	etapa
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
politiiky	politiika	k1gFnSc2	politiika
Balaguerovou	Balaguerová	k1gFnSc7	Balaguerová
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
<g/>
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
opět	opět	k6eAd1	opět
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
středový	středový	k2eAgMnSc1d1	středový
kandidát	kandidát	k1gMnSc1	kandidát
Fernandez	Fernandez	k1gMnSc1	Fernandez
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
nahradil	nahradit	k5eAaPmAgInS	nahradit
Danilo	danit	k5eAaImAgNnS	danit
Medina	Medina	k1gFnSc1	Medina
z	z	k7c2	z
téže	tenže	k3xDgFnSc2	tenže
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
přes	přes	k7c4	přes
7	[number]	k4	7
%	%	kIx~	%
a	a	k8xC	a
nízká	nízký	k2eAgFnSc1d1	nízká
inflace	inflace	k1gFnSc1	inflace
přivedly	přivést	k5eAaPmAgFnP	přivést
Dominikánskou	dominikánský	k2eAgFnSc4d1	Dominikánská
republiku	republika	k1gFnSc4	republika
mezi	mezi	k7c4	mezi
nejúspěšnější	úspěšný	k2eAgFnPc4d3	nejúspěšnější
latinskoamerické	latinskoamerický	k2eAgFnPc4d1	latinskoamerická
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
patrně	patrně	k6eAd1	patrně
trpí	trpět	k5eAaImIp3nS	trpět
zvětšováním	zvětšování	k1gNnSc7	zvětšování
sociálních	sociální	k2eAgInPc2d1	sociální
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
,	,	kIx,	,
divokou	divoký	k2eAgFnSc7d1	divoká
imigrací	imigrace	k1gFnSc7	imigrace
ze	z	k7c2	z
sousedního	sousední	k2eAgNnSc2d1	sousední
Haiti	Haiti	k1gNnSc2	Haiti
a	a	k8xC	a
všudypřítomnou	všudypřítomný	k2eAgFnSc7d1	všudypřítomná
korupcí	korupce	k1gFnSc7	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Míšenci	míšenec	k1gMnPc1	míšenec
tvoří	tvořit	k5eAaImIp3nP	tvořit
73	[number]	k4	73
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
běloši	běloch	k1gMnPc1	běloch
16	[number]	k4	16
%	%	kIx~	%
a	a	k8xC	a
černoši	černoch	k1gMnPc1	černoch
11	[number]	k4	11
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
66	[number]	k4	66
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
69,4	[number]	k4	69,4
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žije	žít	k5eAaImIp3nS	žít
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
tisíc	tisíc	k4xCgInSc4	tisíc
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
Haiti	Haiti	k1gNnSc2	Haiti
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
méně	málo	k6eAd2	málo
kvalifikované	kvalifikovaný	k2eAgFnSc2d1	kvalifikovaná
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
čelí	čelit	k5eAaImIp3nS	čelit
diskriminaci	diskriminace	k1gFnSc4	diskriminace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Dominikánců	Dominikánec	k1gMnPc2	Dominikánec
<g/>
.	.	kIx.	.
</s>
<s>
Dominikánský	dominikánský	k2eAgInSc1d1	dominikánský
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
rozhodnul	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
děti	dítě	k1gFnPc1	dítě
haitských	haitský	k2eAgMnPc2d1	haitský
imigrantů	imigrant	k1gMnPc2	imigrant
narozené	narozený	k2eAgInPc1d1	narozený
v	v	k7c6	v
Dominikánské	dominikánský	k2eAgFnSc6d1	Dominikánská
republice	republika	k1gFnSc6	republika
nemají	mít	k5eNaImIp3nP	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
dominikánské	dominikánský	k2eAgNnSc4d1	Dominikánské
občanství	občanství	k1gNnSc4	občanství
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
až	až	k9	až
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
obyvatelům	obyvatel	k1gMnPc3	obyvatel
území	území	k1gNnSc4	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
vnucen	vnucen	k2eAgInSc1d1	vnucen
jazyk	jazyk	k1gInSc1	jazyk
kmene	kmen	k1gInSc2	kmen
Taínů	Taín	k1gInPc2	Taín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
objevil	objevit	k5eAaPmAgMnS	objevit
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
šířit	šířit	k5eAaImF	šířit
španělština	španělština	k1gFnSc1	španělština
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
španělština	španělština	k1gFnSc1	španělština
mateřským	mateřský	k2eAgMnSc7d1	mateřský
jazykem	jazyk	k1gMnSc7	jazyk
98	[number]	k4	98
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
menšinu	menšina	k1gFnSc4	menšina
tvoří	tvořit	k5eAaImIp3nP	tvořit
diskriminovaní	diskriminovaný	k2eAgMnPc1d1	diskriminovaný
Haiťané	Haiťan	k1gMnPc1	Haiťan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mluví	mluvit	k5eAaImIp3nS	mluvit
specifickým	specifický	k2eAgMnSc7d1	specifický
kreolem	kreol	k1gMnSc7	kreol
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Dominikánské	dominikánský	k2eAgFnSc6d1	Dominikánská
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k1gNnSc1	Domingo
–	–	k?	–
2,9	[number]	k4	2,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
)	)	kIx)	)
Santiago	Santiago	k1gNnSc4	Santiago
de	de	k?	de
los	los	k1gInSc1	los
Caballeros	Caballerosa	k1gFnPc2	Caballerosa
–	–	k?	–
506	[number]	k4	506
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
La	la	k1gNnSc2	la
Romana	Roman	k1gMnSc2	Roman
–	–	k?	–
172	[number]	k4	172
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
San	San	k1gMnSc1	San
Pedro	Pedro	k1gNnSc1	Pedro
de	de	k?	de
Macorís	Macorísa	k1gFnPc2	Macorísa
–	–	k?	–
153	[number]	k4	153
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
země	zem	k1gFnSc2	zem
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
plodinou	plodina	k1gFnSc7	plodina
je	být	k5eAaImIp3nS	být
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc1	kakao
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
maniok	maniok	k1gInSc1	maniok
<g/>
,	,	kIx,	,
podzemnice	podzemnice	k1gFnSc1	podzemnice
olejná	olejný	k2eAgFnSc1d1	olejná
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc1	rýže
a	a	k8xC	a
tabák	tabák	k1gInSc1	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
a	a	k8xC	a
kozy	koza	k1gFnPc1	koza
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
:	:	kIx,	:
azbestu	azbest	k1gInSc2	azbest
<g/>
,	,	kIx,	,
bauxitu	bauxit	k1gInSc2	bauxit
<g/>
,	,	kIx,	,
chromu	chrom	k1gInSc2	chrom
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
platiny	platina	k1gFnSc2	platina
<g/>
,	,	kIx,	,
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgNnSc7d1	významné
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
zpracovatelský	zpracovatelský	k2eAgInSc1d1	zpracovatelský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
jej	on	k3xPp3gInSc4	on
cukrovary	cukrovar	k1gInPc4	cukrovar
<g/>
,	,	kIx,	,
palírny	palírna	k1gFnPc4	palírna
rumu	rum	k1gInSc2	rum
a	a	k8xC	a
závody	závod	k1gInPc1	závod
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
tabáku	tabák	k1gInSc2	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
výroba	výroba	k1gFnSc1	výroba
stavebních	stavební	k2eAgInPc2d1	stavební
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc3	zpracování
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc1d1	textilní
a	a	k8xC	a
oděvní	oděvní	k2eAgInSc1d1	oděvní
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
petrochemie	petrochemie	k1gFnSc1	petrochemie
<g/>
.	.	kIx.	.
</s>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
zóny	zóna	k1gFnSc2	zóna
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
několika	několik	k4yIc7	několik
státy	stát	k1gInPc7	stát
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
-	-	kIx~	-
DR-CAFTA	DR-CAFTA	k1gMnSc1	DR-CAFTA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
hrubý	hrubý	k2eAgInSc4d1	hrubý
domácí	domácí	k2eAgInSc4d1	domácí
produkt	produkt	k1gInSc4	produkt
přepočtený	přepočtený	k2eAgInSc4d1	přepočtený
na	na	k7c4	na
1	[number]	k4	1
obyvatele	obyvatel	k1gMnSc4	obyvatel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
paritu	parita	k1gFnSc4	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
hodnoty	hodnota	k1gFnSc2	hodnota
5	[number]	k4	5
733	[number]	k4	733
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
složky	složka	k1gFnPc4	složka
příjmů	příjem	k1gInPc2	příjem
do	do	k7c2	do
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
turismu	turismus	k1gInSc2	turismus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
představovaly	představovat	k5eAaImAgInP	představovat
téměř	téměř	k6eAd1	téměř
9,2	[number]	k4	9,2
<g/>
%	%	kIx~	%
celkového	celkový	k2eAgNnSc2d1	celkové
státního	státní	k2eAgNnSc2d1	státní
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vyhledávaným	vyhledávaný	k2eAgInPc3d1	vyhledávaný
cílem	cíl	k1gInSc7	cíl
náročných	náročný	k2eAgMnPc2d1	náročný
západních	západní	k2eAgMnPc2d1	západní
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Provincie	provincie	k1gFnSc2	provincie
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
31	[number]	k4	31
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
Distrito	Distrita	k1gFnSc5	Distrita
Nacional	Nacional	k1gMnPc7	Nacional
-	-	kIx~	-
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Santo	Sant	k2eAgNnSc4d1	Santo
Domingo	Domingo	k1gNnSc4	Domingo
<g/>
.	.	kIx.	.
</s>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
zapojená	zapojený	k2eAgFnSc1d1	zapojená
do	do	k7c2	do
několika	několik	k4yIc2	několik
projektů	projekt	k1gInPc2	projekt
a	a	k8xC	a
organizací	organizace	k1gFnPc2	organizace
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
latinskoamerické	latinskoamerický	k2eAgFnSc2d1	latinskoamerická
integrace	integrace	k1gFnSc2	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
např.	např.	kA	např.
organizací	organizace	k1gFnSc7	organizace
Společenství	společenství	k1gNnSc1	společenství
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
a	a	k8xC	a
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Latinskoamerický	latinskoamerický	k2eAgInSc1d1	latinskoamerický
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Středoamerický	středoamerický	k2eAgInSc1d1	středoamerický
integrační	integrační	k2eAgInSc1d1	integrační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Petrocaribe	Petrocarib	k1gInSc5	Petrocarib
<g/>
.	.	kIx.	.
</s>
