<p>
<s>
Nile	Nil	k1gInSc5	Nil
Rodgers	Rodgers	k1gInSc4	Rodgers
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1952	[number]	k4	1952
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Chic	Chic	k1gInSc4	Chic
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
několika	několik	k4yIc6	několik
albech	album	k1gNnPc6	album
zpěváka	zpěvák	k1gMnSc2	zpěvák
Davida	David	k1gMnSc4	David
Bowieho	Bowie	k1gMnSc4	Bowie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Let	let	k1gInSc1	let
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dance	Danka	k1gFnSc6	Danka
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
Tie	Tie	k1gMnSc1	Tie
White	Whit	k1gMnSc5	Whit
Noise	Nois	k1gMnSc5	Nois
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
také	také	k9	také
s	s	k7c7	s
Peterem	Peter	k1gMnSc7	Peter
Gabrielem	Gabriel	k1gMnSc7	Gabriel
<g/>
,	,	kIx,	,
Jeffem	Jeff	k1gMnSc7	Jeff
Beckem	Becek	k1gMnSc7	Becek
nebo	nebo	k8xC	nebo
Madonnou	Madonný	k2eAgFnSc7d1	Madonný
<g/>
.	.	kIx.	.
</s>
<s>
Složil	složit	k5eAaPmAgMnS	složit
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmům	film	k1gInPc3	film
Pozemšťanky	pozemšťanka	k1gFnSc2	pozemšťanka
jsou	být	k5eAaImIp3nP	být
lehce	lehko	k6eAd1	lehko
k	k	k7c3	k
mání	mání	k1gNnSc3	mání
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalším	další	k2eAgMnSc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
francouzským	francouzský	k2eAgNnSc7d1	francouzské
duem	duo	k1gNnSc7	duo
Daft	Daft	k2eAgInSc4d1	Daft
Punk	punk	k1gInSc4	punk
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
albu	album	k1gNnSc6	album
Random	Random	k1gInSc4	Random
Access	Access	k1gInSc4	Access
Memories	Memoriesa	k1gFnPc2	Memoriesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nile	Nil	k1gInSc5	Nil
Rodgers	Rodgers	k1gInSc4	Rodgers
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nile	Nil	k1gInSc5	Nil
Rodgers	Rodgers	k1gInSc4	Rodgers
na	na	k7c4	na
Allmusic	Allmusice	k1gFnPc2	Allmusice
</s>
</p>
<p>
<s>
Nile	Nil	k1gInSc5	Nil
Rodgers	Rodgersa	k1gFnPc2	Rodgersa
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
