<s>
Jojo	jojo	k1gNnSc1	jojo
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
opětovné	opětovný	k2eAgNnSc4d1	opětovné
zvýšení	zvýšení	k1gNnSc4	zvýšení
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
předchozí	předchozí	k2eAgFnSc6d1	předchozí
cílené	cílený	k2eAgFnSc6d1	cílená
redukci	redukce	k1gFnSc6	redukce
<g/>
.	.	kIx.	.
</s>
<s>
Opětovné	opětovný	k2eAgNnSc1d1	opětovné
přibrání	přibrání	k1gNnSc1	přibrání
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
původní	původní	k2eAgFnSc1d1	původní
redukce	redukce	k1gFnSc1	redukce
<g/>
.	.	kIx.	.
</s>
<s>
K	K	kA	K
jojo	jojo	k1gNnSc1	jojo
efektu	efekt	k1gInSc2	efekt
dochází	docházet	k5eAaImIp3nS	docházet
zpravidla	zpravidla	k6eAd1	zpravidla
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
po	po	k7c6	po
prodělané	prodělaný	k2eAgFnSc6d1	prodělaná
redukci	redukce	k1gFnSc6	redukce
hmotnosti	hmotnost	k1gFnSc2	hmotnost
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
vrátí	vrátit	k5eAaPmIp3nS	vrátit
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
<g/>
,	,	kIx,	,
nezdravému	zdravý	k2eNgInSc3d1	nezdravý
způsobu	způsob	k1gInSc3	způsob
stravování	stravování	k1gNnSc2	stravování
<g/>
.	.	kIx.	.
</s>
<s>
Nezdravým	zdravý	k2eNgInSc7d1	nezdravý
způsobem	způsob	k1gInSc7	způsob
stravování	stravování	k1gNnSc2	stravování
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
myšleno	myslet	k5eAaImNgNnS	myslet
nejen	nejen	k6eAd1	nejen
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kvalita	kvalita	k1gFnSc1	kvalita
a	a	k8xC	a
skladba	skladba	k1gFnSc1	skladba
stravy	strava	k1gFnSc2	strava
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
zvyklé	zvyklý	k2eAgNnSc1d1	zvyklé
dělat	dělat	k5eAaImF	dělat
si	se	k3xPyFc3	se
zásoby	zásoba	k1gFnPc4	zásoba
tzv.	tzv.	kA	tzv.
na	na	k7c4	na
horší	zlý	k2eAgInPc4d2	horší
časy	čas	k1gInPc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tedy	tedy	k9	tedy
člověk	člověk	k1gMnSc1	člověk
drží	držet	k5eAaImIp3nS	držet
dietu	dieta	k1gFnSc4	dieta
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
omezuje	omezovat	k5eAaImIp3nS	omezovat
příjem	příjem	k1gInSc4	příjem
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
tělo	tělo	k1gNnSc4	tělo
považuje	považovat	k5eAaImIp3nS	považovat
takové	takový	k3xDgNnSc4	takový
období	období	k1gNnSc4	období
za	za	k7c4	za
"	"	kIx"	"
<g/>
období	období	k1gNnSc4	období
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
srovnatelné	srovnatelný	k2eAgNnSc1d1	srovnatelné
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
se	s	k7c7	s
zimním	zimní	k2eAgNnSc7d1	zimní
obdobím	období	k1gNnSc7	období
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
<g/>
-li	i	k?	-li
se	s	k7c7	s
člověk	člověk	k1gMnSc1	člověk
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
redukci	redukce	k1gFnSc6	redukce
hmotnosti	hmotnost	k1gFnSc2	hmotnost
stravovat	stravovat	k5eAaImF	stravovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
stravoval	stravovat	k5eAaImAgMnS	stravovat
<g/>
,	,	kIx,	,
než	než	k8xS	než
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
dietu	dieta	k1gFnSc4	dieta
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
tělo	tělo	k1gNnSc4	tělo
si	se	k3xPyFc3	se
neprodleně	prodleně	k6eNd1	prodleně
začne	začít	k5eAaPmIp3nS	začít
tvořit	tvořit	k5eAaImF	tvořit
zásoby	zásoba	k1gFnPc4	zásoba
na	na	k7c4	na
příští	příští	k2eAgNnSc4d1	příští
"	"	kIx"	"
<g/>
období	období	k1gNnSc4	období
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
těle	tělo	k1gNnSc6	tělo
geneticky	geneticky	k6eAd1	geneticky
zakódovaný	zakódovaný	k2eAgMnSc1d1	zakódovaný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
teplejších	teplý	k2eAgInPc6d2	teplejší
krajích	kraj	k1gInPc6	kraj
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
obezitou	obezita	k1gFnSc7	obezita
<g/>
;	;	kIx,	;
částečně	částečně	k6eAd1	částečně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
celoroční	celoroční	k2eAgFnSc1d1	celoroční
úroda	úroda	k1gFnSc1	úroda
-	-	kIx~	-
hojnost	hojnost	k1gFnSc1	hojnost
-	-	kIx~	-
<g/>
,	,	kIx,	,
a	a	k8xC	a
tělo	tělo	k1gNnSc1	tělo
tudíž	tudíž	k8xC	tudíž
nemělo	mít	k5eNaImAgNnS	mít
důvod	důvod	k1gInSc4	důvod
vytvářet	vytvářet	k5eAaImF	vytvářet
si	se	k3xPyFc3	se
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
příjem	příjem	k1gInSc1	příjem
potravin	potravina	k1gFnPc2	potravina
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
schéma	schéma	k1gNnSc4	schéma
diety	dieta	k1gFnSc2	dieta
opouštět	opouštět	k5eAaImF	opouštět
<g/>
,	,	kIx,	,
pozvolna	pozvolna	k6eAd1	pozvolna
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
tělo	tělo	k1gNnSc1	tělo
lépe	dobře	k6eAd2	dobře
adaptuje	adaptovat	k5eAaBmIp3nS	adaptovat
<g/>
,	,	kIx,	,
signál	signál	k1gInSc1	signál
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
zásob	zásoba	k1gFnPc2	zásoba
nedostane	dostat	k5eNaPmIp3nS	dostat
<g/>
.	.	kIx.	.
</s>
