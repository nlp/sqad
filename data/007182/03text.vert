<s>
Počítadlo	počítadlo	k1gNnSc1	počítadlo
či	či	k8xC	či
abakus	abakus	k1gInSc1	abakus
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
mechanická	mechanický	k2eAgFnSc1d1	mechanická
pomůcka	pomůcka	k1gFnSc1	pomůcka
usnadňující	usnadňující	k2eAgFnSc1d1	usnadňující
výpočty	výpočet	k1gInPc7	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
nejdříve	dříve	k6eAd3	dříve
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
počtářskými	počtářský	k2eAgInPc7d1	počtářský
kaménky	kamének	k1gInPc7	kamének
a	a	k8xC	a
vyznačenými	vyznačený	k2eAgInPc7d1	vyznačený
sloupci	sloupec	k1gInPc7	sloupec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
destička	destička	k1gFnSc1	destička
se	s	k7c7	s
žlábky	žlábek	k1gInPc7	žlábek
nebo	nebo	k8xC	nebo
rámeček	rámeček	k1gInSc1	rámeček
s	s	k7c7	s
kuličkami	kulička	k1gFnPc7	kulička
na	na	k7c6	na
tyčkách	tyčka	k1gFnPc6	tyčka
<g/>
.	.	kIx.	.
</s>
<s>
Destička	destička	k1gFnSc1	destička
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
také	také	k9	také
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
pouhými	pouhý	k2eAgFnPc7d1	pouhá
čarami	čára	k1gFnPc7	čára
vyznačenými	vyznačený	k2eAgFnPc7d1	vyznačená
v	v	k7c6	v
písku	písek	k1gInSc6	písek
<g/>
.	.	kIx.	.
</s>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
přesouvání	přesouvání	k1gNnSc6	přesouvání
počtářských	počtářský	k2eAgInPc2d1	počtářský
kaménků	kamének	k1gInPc2	kamének
určitého	určitý	k2eAgInSc2d1	určitý
řádu	řád	k1gInSc2	řád
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
abaku	abak	k1gInSc2	abak
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Abakus	abakus	k1gInSc1	abakus
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
v	v	k7c6	v
Babylóně	Babylón	k1gInSc6	Babylón
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Kuličkové	kuličkový	k2eAgNnSc1d1	kuličkové
počitadlo	počitadlo	k1gNnSc1	počitadlo
používané	používaný	k2eAgNnSc1d1	používané
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
ročnících	ročník	k1gInPc6	ročník
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
podob	podoba	k1gFnPc2	podoba
abaku	abak	k1gInSc2	abak
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
abaku	abak	k1gInSc2	abak
a	a	k8xC	a
výpočtů	výpočet	k1gInPc2	výpočet
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
sestavil	sestavit	k5eAaPmAgInS	sestavit
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
matematik	matematik	k1gMnSc1	matematik
Bernelius	Bernelius	k1gMnSc1	Bernelius
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
vydal	vydat	k5eAaPmAgMnS	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Liber	libra	k1gFnPc2	libra
abaci	abace	k1gFnSc3	abace
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
abakus	abakus	k1gInSc4	abakus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
žlábky	žlábek	k1gInPc1	žlábek
jsou	být	k5eAaImIp3nP	být
stejných	stejný	k2eAgInPc2d1	stejný
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
římské	římský	k2eAgFnPc4d1	římská
číslice	číslice	k1gFnPc4	číslice
(	(	kIx(	(
<g/>
I	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
<g/>
,	,	kIx,	,
V	V	kA	V
=	=	kIx~	=
5	[number]	k4	5
<g/>
,	,	kIx,	,
X	X	kA	X
=	=	kIx~	=
10	[number]	k4	10
<g/>
,	,	kIx,	,
L	L	kA	L
=	=	kIx~	=
50	[number]	k4	50
<g/>
,	,	kIx,	,
C	C	kA	C
=	=	kIx~	=
100	[number]	k4	100
<g/>
,	,	kIx,	,
D	D	kA	D
=	=	kIx~	=
500	[number]	k4	500
<g/>
,	,	kIx,	,
M	M	kA	M
=	=	kIx~	=
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zákazník	zákazník	k1gMnSc1	zákazník
si	se	k3xPyFc3	se
kupuje	kupovat	k5eAaImIp3nS	kupovat
olej	olej	k1gInSc1	olej
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chléb	chléb	k1gInSc1	chléb
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pergamen	pergamen	k1gInSc1	pergamen
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
<g/>
)	)	kIx)	)
a	a	k8xC	a
voskové	voskový	k2eAgFnPc4d1	vosková
svíčky	svíčka	k1gFnPc4	svíčka
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
51	[number]	k4	51
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obchodník	obchodník	k1gMnSc1	obchodník
při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
výsledné	výsledný	k2eAgFnSc2d1	výsledná
ceny	cena	k1gFnSc2	cena
postupně	postupně	k6eAd1	postupně
přesune	přesunout	k5eAaPmIp3nS	přesunout
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
<g/>
:	:	kIx,	:
1	[number]	k4	1
kuličku	kulička	k1gFnSc4	kulička
ve	v	k7c6	v
žlábku	žlábek	k1gInSc6	žlábek
řádu	řád	k1gInSc2	řád
X	X	kA	X
(	(	kIx(	(
<g/>
za	za	k7c4	za
olej	olej	k1gInSc4	olej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
kuličku	kulička	k1gFnSc4	kulička
ve	v	k7c6	v
žlábku	žlábek	k1gInSc6	žlábek
řádu	řád	k1gInSc2	řád
V	V	kA	V
(	(	kIx(	(
<g/>
chleba	chléb	k1gInSc2	chléb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
1	[number]	k4	1
kuličce	kulička	k1gFnSc6	kulička
ve	v	k7c6	v
žlábcích	žlábek	k1gInPc6	žlábek
řádu	řád	k1gInSc2	řád
X	X	kA	X
a	a	k8xC	a
řádu	řád	k1gInSc3	řád
<g />
.	.	kIx.	.
</s>
<s>
V	V	kA	V
a	a	k8xC	a
3	[number]	k4	3
kuličky	kulička	k1gFnSc2	kulička
ve	v	k7c6	v
žlábku	žlábek	k1gInSc6	žlábek
řádu	řád	k1gInSc2	řád
I	i	k9	i
(	(	kIx(	(
<g/>
pergamen	pergamen	k1gInSc1	pergamen
<g/>
)	)	kIx)	)
1	[number]	k4	1
kuličku	kulička	k1gFnSc4	kulička
ve	v	k7c6	v
žlábku	žlábek	k1gInSc6	žlábek
řádu	řád	k1gInSc2	řád
L	L	kA	L
a	a	k8xC	a
1	[number]	k4	1
kuličku	kulička	k1gFnSc4	kulička
ve	v	k7c6	v
žlábku	žlábek	k1gInSc6	žlábek
řádu	řád	k1gInSc2	řád
I.	I.	kA	I.
Konečný	konečný	k2eAgInSc4d1	konečný
účet	účet	k1gInSc4	účet
zákazníka	zákazník	k1gMnSc2	zákazník
tedy	tedy	k8xC	tedy
bude	být	k5eAaImBp3nS	být
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1	[number]	k4	1
×	×	k?	×
50	[number]	k4	50
<g/>
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
<g/>
2	[number]	k4	2
×	×	k?	×
10	[number]	k4	10
<g/>
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
<g/>
2	[number]	k4	2
×	×	k?	×
5	[number]	k4	5
<g/>
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
<g/>
4	[number]	k4	4
×	×	k?	×
1	[number]	k4	1
<g/>
)	)	kIx)	)
=	=	kIx~	=
84	[number]	k4	84
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
hliněnou	hliněný	k2eAgFnSc4d1	hliněná
destičku	destička	k1gFnSc4	destička
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
žlábky	žlábek	k1gInPc7	žlábek
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
žlábek	žlábek	k1gInSc1	žlábek
představoval	představovat	k5eAaImAgInS	představovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
římských	římský	k2eAgFnPc2d1	římská
číslic	číslice	k1gFnPc2	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Počítalo	počítat	k5eAaImAgNnS	počítat
se	se	k3xPyFc4	se
přesouváním	přesouvání	k1gNnSc7	přesouvání
kuliček	kulička	k1gFnPc2	kulička
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
žlábcích	žlábek	k1gInPc6	žlábek
<g/>
.	.	kIx.	.
</s>
<s>
Používala	používat	k5eAaImAgFnS	používat
se	se	k3xPyFc4	se
hladká	hladký	k2eAgFnSc1d1	hladká
deska	deska	k1gFnSc1	deska
posypaná	posypaný	k2eAgFnSc1d1	posypaná
pískem	písek	k1gInSc7	písek
rozdělená	rozdělená	k1gFnSc1	rozdělená
do	do	k7c2	do
třiceti	třicet	k4xCc2	třicet
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
sloupců	sloupec	k1gInPc2	sloupec
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Sloupce	sloupec	k1gInPc1	sloupec
byly	být	k5eAaImAgInP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
skupina	skupina	k1gFnSc1	skupina
tvořila	tvořit	k5eAaImAgFnS	tvořit
jeden	jeden	k4xCgInSc4	jeden
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
tři	tři	k4xCgInPc1	tři
sloupce	sloupec	k1gInPc1	sloupec
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
zlomků	zlomek	k1gInPc2	zlomek
<g/>
.	.	kIx.	.
</s>
<s>
Nahoře	nahoře	k6eAd1	nahoře
byly	být	k5eAaImAgInP	být
sloupce	sloupec	k1gInPc1	sloupec
ukončeny	ukončit	k5eAaPmNgInP	ukončit
oblouky	oblouk	k1gInPc7	oblouk
nazývanými	nazývaný	k2eAgInPc7d1	nazývaný
"	"	kIx"	"
<g/>
arcus	arcus	k1gInSc1	arcus
Pythagorei	Pythagore	k1gFnSc2	Pythagore
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
objev	objev	k1gInSc1	objev
abaku	abak	k1gInSc2	abak
připisoval	připisovat	k5eAaImAgInS	připisovat
Pythagorovi	Pythagoras	k1gMnSc3	Pythagoras
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sloupců	sloupec	k1gInPc2	sloupec
se	se	k3xPyFc4	se
kladly	klást	k5eAaImAgInP	klást
početní	početní	k2eAgInPc1d1	početní
kaménky	kamének	k1gInPc1	kamének
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
starověkých	starověký	k2eAgFnPc2d1	starověká
verzí	verze	k1gFnPc2	verze
abaku	abak	k1gInSc2	abak
se	se	k3xPyFc4	se
jednotky	jednotka	k1gFnPc1	jednotka
nevyjadřovaly	vyjadřovat	k5eNaImAgFnP	vyjadřovat
pomocí	pomocí	k7c2	pomocí
několika	několik	k4yIc2	několik
kaménků	kamének	k1gInPc2	kamének
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomocí	pomocí	k7c2	pomocí
destiček	destička	k1gFnPc2	destička
s	s	k7c7	s
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
příslušných	příslušný	k2eAgFnPc2d1	příslušná
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc4	ten
byly	být	k5eAaImAgInP	být
nazývány	nazýván	k2eAgInPc1d1	nazýván
"	"	kIx"	"
<g/>
apex	apex	k1gInSc1	apex
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
plurál	plurál	k1gInSc1	plurál
"	"	kIx"	"
<g/>
apices	apices	k1gInSc1	apices
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
destičky	destička	k1gFnPc4	destička
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
používány	používat	k5eAaImNgFnP	používat
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
čísel	číslo	k1gNnPc2	číslo
indo-arabské	indorabský	k2eAgFnSc2d1	indo-arabský
číslice	číslice	k1gFnSc2	číslice
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
tak	tak	k9	tak
postupně	postupně	k6eAd1	postupně
vytlačily	vytlačit	k5eAaPmAgFnP	vytlačit
římské	římský	k2eAgFnPc4d1	římská
číslice	číslice	k1gFnPc4	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Japonský	japonský	k2eAgInSc1d1	japonský
abakus	abakus	k1gInSc1	abakus
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Soroban	Soroban	k1gInSc1	Soroban
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
desítkové	desítkový	k2eAgFnPc1d1	desítková
číslice	číslice	k1gFnPc1	číslice
jsou	být	k5eAaImIp3nP	být
reprezentovány	reprezentován	k2eAgInPc4d1	reprezentován
4	[number]	k4	4
+	+	kIx~	+
1	[number]	k4	1
kameny	kámen	k1gInPc7	kámen
–	–	k?	–
oddělený	oddělený	k2eAgInSc4d1	oddělený
jeden	jeden	k4xCgInSc4	jeden
kámen	kámen	k1gInSc4	kámen
označuje	označovat	k5eAaImIp3nS	označovat
číslo	číslo	k1gNnSc1	číslo
5	[number]	k4	5
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc1	čtyři
kameny	kámen	k1gInPc1	kámen
se	se	k3xPyFc4	se
přičítají	přičítat	k5eAaImIp3nP	přičítat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
široce	široko	k6eAd1	široko
využíván	využívat	k5eAaPmNgInS	využívat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
existovaly	existovat	k5eAaImAgFnP	existovat
dokonce	dokonce	k9	dokonce
jeho	jeho	k3xOp3gFnPc1	jeho
kombinace	kombinace	k1gFnPc1	kombinace
s	s	k7c7	s
elektronickými	elektronický	k2eAgFnPc7d1	elektronická
kalkulačkami	kalkulačka	k1gFnPc7	kalkulačka
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
počítadlo	počítadlo	k1gNnSc1	počítadlo
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgNnSc1d1	zvané
sčot	sčot	k1gInSc4	sčot
nebo	nebo	k8xC	nebo
sčoty	sčot	k1gInPc4	sčot
(	(	kIx(	(
<g/>
с	с	k?	с
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
rámu	rám	k1gInSc2	rám
s	s	k7c7	s
nejčastěji	často	k6eAd3	často
deseti	deset	k4xCc7	deset
tyčkami	tyčka	k1gFnPc7	tyčka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
všechny	všechen	k3xTgFnPc1	všechen
mají	mít	k5eAaImIp3nP	mít
10	[number]	k4	10
kuliček	kulička	k1gFnPc2	kulička
kromě	kromě	k7c2	kromě
jedné	jeden	k4xCgFnSc2	jeden
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
kuličky	kulička	k1gFnSc2	kulička
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
výpočty	výpočet	k1gInPc4	výpočet
s	s	k7c7	s
čtvrtrubly	čtvrtrubnout	k5eAaPmAgInP	čtvrtrubnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
se	se	k3xPyFc4	se
sčot	sčot	k1gInSc1	sčot
používal	používat	k5eAaImAgInS	používat
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
až	až	k9	až
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
elektronickými	elektronický	k2eAgInPc7d1	elektronický
kalkulátory	kalkulátor	k1gInPc7	kalkulátor
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
počítadlo	počítadlo	k1gNnSc4	počítadlo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
