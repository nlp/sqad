<p>
<s>
Mozilla	Mozilla	k6eAd1	Mozilla
Messaging	Messaging	k1gInSc1	Messaging
je	být	k5eAaImIp3nS	být
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
organizace	organizace	k1gFnSc1	organizace
Mozilla	Mozilla	k1gMnSc1	Mozilla
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
vývoj	vývoj	k1gInSc4	vývoj
poštovního	poštovní	k1gMnSc2	poštovní
klienta	klient	k1gMnSc2	klient
Mozilla	Mozilla	k1gMnSc1	Mozilla
Thunderbird	Thunderbird	k1gMnSc1	Thunderbird
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
vyvíjen	vyvíjen	k2eAgMnSc1d1	vyvíjen
Mozilla	Mozilla	k1gMnSc1	Mozilla
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
zaměřit	zaměřit	k5eAaPmF	zaměřit
primárně	primárně	k6eAd1	primárně
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
webového	webový	k2eAgMnSc2d1	webový
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
Mozilla	Mozilla	k1gFnSc1	Mozilla
Firefox	Firefox	k1gInSc1	Firefox
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
veřejné	veřejný	k2eAgFnSc6d1	veřejná
diskusi	diskuse	k1gFnSc6	diskuse
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
převedení	převedení	k1gNnSc6	převedení
Mozilla	Mozillo	k1gNnSc2	Mozillo
Thunderbirdu	Thunderbird	k1gInSc2	Thunderbird
pod	pod	k7c4	pod
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
po	po	k7c6	po
pracovním	pracovní	k2eAgInSc6d1	pracovní
názvu	název	k1gInSc6	název
MailCo	MailCo	k6eAd1	MailCo
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
Mozilla	Mozilla	k1gFnSc1	Mozilla
Messaging	Messaging	k1gInSc1	Messaging
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
David	David	k1gMnSc1	David
Ascher	Aschra	k1gFnPc2	Aschra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Mozilla	Mozilla	k1gFnSc1	Mozilla
Messaging	Messaging	k1gInSc1	Messaging
sloučí	sloučit	k5eAaPmIp3nS	sloučit
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Mozilla	Mozill	k1gMnSc2	Mozill
Labs	Labsa	k1gFnPc2	Labsa
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
vývoj	vývoj	k1gInSc1	vývoj
Thunderbirdu	Thunderbird	k1gInSc2	Thunderbird
předán	předat	k5eAaPmNgInS	předat
komunitě	komunita	k1gFnSc3	komunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Mozilla	Mozilla	k1gFnSc1	Mozilla
-	-	kIx~	-
co	co	k3yRnSc4	co
více	hodně	k6eAd2	hodně
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
pojem	pojem	k1gInSc4	pojem
Mozilla	Mozillo	k1gNnSc2	Mozillo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
Mozilla	Mozilla	k1gMnSc1	Mozilla
Messaging	Messaging	k1gInSc1	Messaging
</s>
</p>
