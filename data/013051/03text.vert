<p>
<s>
Furnas	Furnas	k1gInSc1	Furnas
je	být	k5eAaImIp3nS	být
neaktivní	aktivní	k2eNgInSc1d1	neaktivní
stratovulkán	stratovulkán	k1gInSc1	stratovulkán
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
konci	konec	k1gInSc6	konec
ostrova	ostrov	k1gInSc2	ostrov
Sã	Sã	k1gMnSc1	Sã
Miguel	Miguel	k1gMnSc1	Miguel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořený	tvořený	k2eAgInSc1d1	tvořený
dvěma	dva	k4xCgFnPc7	dva
kalderami	kaldera	k1gFnPc7	kaldera
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc1d2	mladší
–	–	k?	–
6	[number]	k4	6
km	km	kA	km
široká	široký	k2eAgFnSc1d1	široká
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
stáří	stář	k1gFnSc7	stář
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
roků	rok	k1gInPc2	rok
a	a	k8xC	a
starší	starý	k2eAgInSc4d2	starší
30	[number]	k4	30
000	[number]	k4	000
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInPc1d1	východní
stěny	stěn	k1gInPc1	stěn
překrývají	překrývat	k5eAaImIp3nP	překrývat
starší	starý	k2eAgInPc1d2	starší
pleistocénny	pleistocénn	k1gInPc1	pleistocénn
štítový	štítový	k2eAgInSc1d1	štítový
vulkán	vulkán	k1gInSc4	vulkán
Nordeste	Nordest	k1gInSc5	Nordest
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc3	jeho
kalderu	kalder	k1gInSc3	kalder
Povoaçao	Povoaçao	k1gNnSc1	Povoaçao
<g/>
.	.	kIx.	.
</s>
<s>
Sopka	sopka	k1gFnSc1	sopka
je	být	k5eAaImIp3nS	být
známa	známo	k1gNnPc4	známo
díky	díky	k7c3	díky
početným	početný	k2eAgInPc3d1	početný
horoucím	horoucí	k2eAgInPc3d1	horoucí
pramenům	pramen	k1gInPc3	pramen
a	a	k8xC	a
gejzírům	gejzír	k1gInPc3	gejzír
<g/>
,	,	kIx,	,
vyskytujícím	vyskytující	k2eAgInPc3d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c4	v
jej	on	k3xPp3gNnSc4	on
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postkladerové	Postkladerový	k2eAgNnSc1d1	Postkladerový
stádium	stádium	k1gNnSc1	stádium
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
činnosti	činnost	k1gFnSc2	činnost
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
několik	několik	k4yIc1	několik
lávových	lávový	k2eAgInPc2d1	lávový
dómů	dóm	k1gInPc2	dóm
(	(	kIx(	(
<g/>
Pico	Pico	k6eAd1	Pico
da	da	k?	da
Areia	Areia	k1gFnSc1	Areia
<g/>
,	,	kIx,	,
Pico	Pico	k6eAd1	Pico
do	do	k7c2	do
Buraco	Buraco	k6eAd1	Buraco
<g/>
,	,	kIx,	,
Pico	Pico	k6eAd1	Pico
do	do	k7c2	do
Gaspar	Gaspara	k1gFnPc2	Gaspara
<g/>
,	,	kIx,	,
Pico	Pico	k6eAd1	Pico
do	do	k7c2	do
Ferro	Ferro	k1gNnSc2	Ferro
<g/>
,	,	kIx,	,
Pica	Pica	k1gMnSc1	Pica
das	das	k?	das
Marcondas	Marcondas	k1gMnSc1	Marcondas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
troskových	troskův	k2eAgInPc2d1	troskův
kuželů	kužel	k1gInPc2	kužel
(	(	kIx(	(
<g/>
Pico	Pico	k6eAd1	Pico
da	da	k?	da
Azeitona	Azeitona	k1gFnSc1	Azeitona
<g/>
,	,	kIx,	,
Pico	Pico	k1gMnSc1	Pico
de	de	k?	de
Canario	Canario	k1gMnSc1	Canario
<g/>
,	,	kIx,	,
Pica	Pica	k1gMnSc1	Pica
da	da	k?	da
Senhora	Senhora	k1gFnSc1	Senhora
<g/>
)	)	kIx)	)
a	a	k8xC	a
několik	několik	k4yIc1	několik
vrstev	vrstva	k1gFnPc2	vrstva
pemzy	pemza	k1gFnSc2	pemza
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgNnSc1d2	menší
pole	pole	k1gNnSc1	pole
troskových	troskův	k2eAgInPc2d1	troskův
kuželů	kužel	k1gInPc2	kužel
a	a	k8xC	a
dómů	dóm	k1gInPc2	dóm
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
i	i	k9	i
mezi	mezi	k7c7	mezi
sopkami	sopka	k1gFnPc7	sopka
Furnas	Furnasa	k1gFnPc2	Furnasa
a	a	k8xC	a
Água	Águ	k2eAgFnSc1d1	Águ
de	de	k?	de
Pau	Pau	k1gFnSc1	Pau
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historicky	historicky	k6eAd1	historicky
doložené	doložený	k2eAgFnPc1d1	doložená
erupce	erupce	k1gFnPc1	erupce
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
roku	rok	k1gInSc3	rok
1439	[number]	k4	1439
<g/>
,	,	kIx,	,
1443	[number]	k4	1443
a	a	k8xC	a
1630	[number]	k4	1630
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
erupce	erupce	k1gFnSc1	erupce
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
nejkatastrofičtější	katastrofický	k2eAgFnSc1d3	nejkatastrofičtější
erupce	erupce	k1gFnSc1	erupce
na	na	k7c6	na
Azorském	azorský	k2eAgNnSc6d1	Azorské
souostroví	souostroví	k1gNnSc6	souostroví
<g/>
;	;	kIx,	;
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
si	se	k3xPyFc3	se
asi	asi	k9	asi
200	[number]	k4	200
obětí	oběť	k1gFnPc2	oběť
na	na	k7c6	na
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Furnas	Furnasa	k1gFnPc2	Furnasa
(	(	kIx(	(
<g/>
sopka	sopka	k1gFnSc1	sopka
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Furnas	Furnasa	k1gFnPc2	Furnasa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.volcano.si.edu	www.volcano.si.et	k5eAaPmIp1nS	www.volcano.si.et
-	-	kIx~	-
Furnas	Furnas	k1gInSc4	Furnas
na	na	k7c4	na
Global	globat	k5eAaImAgInS	globat
Volcanism	Volcanism	k1gInSc1	Volcanism
Program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
