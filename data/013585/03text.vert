<s>
NGC	NGC	kA
672	#num#	k4
</s>
<s>
NGC	NGC	kA
672	#num#	k4
NGC	NGC	kA
672	#num#	k4
<g/>
,	,	kIx,
IC	IC	kA
1727	#num#	k4
a	a	k8xC
LEDA	leda	k6eAd1
1803573	#num#	k4
<g/>
Pozorovací	pozorovací	k2eAgInPc1d1
údaje	údaj	k1gInPc1
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000,0	2000,0	k4
<g/>
)	)	kIx)
Typ	typ	k1gInSc1
</s>
<s>
galaxie	galaxie	k1gFnSc1
Třída	třída	k1gFnSc1
</s>
<s>
SB	sb	kA
<g/>
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
cd	cd	kA
/	/	kIx~
HII	HII	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Objevitel	objevitel	k1gMnSc1
</s>
<s>
William	William	k1gInSc1
Herschel	Herschela	k1gFnPc2
Datum	datum	k1gInSc1
objevu	objev	k1gInSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1786	#num#	k4
Rektascenze	rektascenze	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
h	h	k?
47	#num#	k4
<g/>
m	m	kA
54,06	54,06	k4
<g/>
s	s	k7c7
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Deklinace	deklinace	k1gFnSc1
</s>
<s>
+27	+27	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
55,8	55,8	k4
<g/>
″	″	k?
Souhvězdí	souhvězdí	k1gNnSc2
</s>
<s>
Trojúhelník	trojúhelník	k1gInSc1
(	(	kIx(
<g/>
lat.	lat.	k?
Triangulum	Triangulum	k1gInSc1
<g/>
)	)	kIx)
Zdánlivá	zdánlivý	k2eAgFnSc1d1
magnituda	magnituda	k1gFnSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
10,7	10,7	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Úhlová	úhlový	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
6,0	6,0	k4
<g/>
′	′	k?
×	×	k?
2,4	2,4	k4
<g/>
′	′	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
25	#num#	k4
M	M	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
ly	ly	k?
Plošná	plošný	k2eAgFnSc1d1
jasnost	jasnost	k1gFnSc1
</s>
<s>
13,4	13,4	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Poziční	poziční	k2eAgInSc1d1
úhel	úhel	k1gInSc1
</s>
<s>
65	#num#	k4
<g/>
°	°	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Rudý	rudý	k2eAgInSc1d1
posuv	posuv	k1gInSc1
</s>
<s>
0,001408	0,001408	k4
±	±	k?
0,000007	0,000007	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Označení	označení	k1gNnSc1
v	v	k7c6
katalozích	katalog	k1gInPc6
New	New	k1gMnSc2
General	General	k1gMnSc2
Catalogue	Catalogu	k1gMnSc2
</s>
<s>
NGC	NGC	kA
672	#num#	k4
IRAS	IRAS	kA
</s>
<s>
IRAS	IRAS	kA
0	#num#	k4
<g/>
1450	#num#	k4
<g/>
+	+	kIx~
<g/>
2710	#num#	k4
a	a	k8xC
IRAS	IRAS	kA
F	F	kA
<g/>
0	#num#	k4
<g/>
1450	#num#	k4
<g/>
+	+	kIx~
<g/>
2710	#num#	k4
Uppsala	Uppsala	k1gFnSc1
General	General	k1gFnSc1
Catalogue	Catalogue	k1gFnSc1
</s>
<s>
UGC	UGC	kA
1256	#num#	k4
Principal	Principal	k1gFnSc2
Galaxies	Galaxiesa	k1gFnPc2
Catalogue	Catalogu	k1gFnSc2
</s>
<s>
PGC	PGC	kA
6595	#num#	k4
Jiná	jiná	k1gFnSc1
označení	označení	k1gNnSc2
</s>
<s>
NGC	NGC	kA
672	#num#	k4
<g/>
,	,	kIx,
PGC	PGC	kA
6595	#num#	k4
<g/>
,	,	kIx,
UGC	UGC	kA
1256	#num#	k4
<g/>
,	,	kIx,
MCG	MCG	kA
+4	+4	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
GC	GC	kA
396	#num#	k4
<g/>
,	,	kIx,
H	H	kA
I	i	k9
157	#num#	k4
<g/>
,	,	kIx,
H	H	kA
150	#num#	k4
<g/>
,	,	kIx,
CGCG	CGCG	kA
482	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
16	#num#	k4
<g/>
,	,	kIx,
IRAS	IRAS	kA
0	#num#	k4
<g/>
1450	#num#	k4
<g/>
+	+	kIx~
<g/>
2710	#num#	k4
<g/>
,	,	kIx,
VV	VV	kA
338	#num#	k4
<g/>
,	,	kIx,
<g/>
NVSS	NVSS	kA
J	J	kA
<g/>
0	#num#	k4
<g/>
14756	#num#	k4
<g/>
+	+	kIx~
<g/>
272617	#num#	k4
•	•	k?
HOLM	HOLM	kA
046A	046A	k4
</s>
<s>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
–	–	k?
měření	měření	k1gNnSc2
provedena	proveden	k2eAgFnSc1d1
ve	v	k7c6
viditelném	viditelný	k2eAgNnSc6d1
světle	světlo	k1gNnSc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
NGC	NGC	kA
672	#num#	k4
je	být	k5eAaImIp3nS
spirální	spirální	k2eAgFnSc1d1
galaxie	galaxie	k1gFnSc1
s	s	k7c7
příčkou	příčka	k1gFnSc7
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Trojúhelníku	trojúhelník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
zdánlivá	zdánlivý	k2eAgFnSc1d1
jasnost	jasnost	k1gFnSc1
je	být	k5eAaImIp3nS
10,7	10,7	k4
<g/>
m	m	kA
a	a	k8xC
úhlová	úhlový	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
6,0	6,0	k4
<g/>
′	′	k?
×	×	k?
2,4	2,4	k4
<g/>
′	′	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
vzdálená	vzdálený	k2eAgFnSc1d1
25	#num#	k4
milionů	milion	k4xCgInPc2
světelných	světelný	k2eAgFnPc2d1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
průměr	průměr	k1gInSc1
má	mít	k5eAaImIp3nS
35	#num#	k4
000	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Galaxii	galaxie	k1gFnSc6
objevil	objevit	k5eAaPmAgMnS
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1786	#num#	k4
William	William	k1gInSc1
Herschel	Herschel	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galaxie	galaxie	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
s	s	k7c7
jí	on	k3xPp3gFnSc7
blízkou	blízký	k2eAgFnSc7d1
galaxií	galaxie	k1gFnSc7
IC	IC	kA
1727	#num#	k4
gravitačně	gravitačně	k6eAd1
vázanou	vázaný	k2eAgFnSc4d1
dvojici	dvojice	k1gFnSc4
Holm	Holm	k1gInSc1
46	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdálenost	vzdálenost	k1gFnSc1
mezi	mezi	k7c7
galaxiemi	galaxie	k1gFnPc7
dvojice	dvojice	k1gFnSc1
je	být	k5eAaImIp3nS
90	#num#	k4
000	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
NASA	NASA	kA
<g/>
/	/	kIx~
<g/>
IPAC	IPAC	kA
Extragalactic	Extragalactice	k1gFnPc2
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Results	Resultsa	k1gFnPc2
for	forum	k1gNnPc2
NGC	NGC	kA
672	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
FROMMERT	FROMMERT	kA
<g/>
,	,	kIx,
Hartmut	Hartmut	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revised	Revised	k1gInSc1
NGC	NGC	kA
Data	datum	k1gNnPc1
for	forum	k1gNnPc2
NGC	NGC	kA
672	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SEDS	SEDS	kA
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SIMBAD	SIMBAD	kA
Astronomical	Astronomical	k1gFnSc3
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Results	Resultsa	k1gFnPc2
for	forum	k1gNnPc2
NGC	NGC	kA
672	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
SELIGMAN	SELIGMAN	kA
<g/>
,	,	kIx,
Courtney	Courtney	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celestial	Celestial	k1gMnSc1
Atlas	Atlas	k1gMnSc1
<g/>
:	:	kIx,
NGC	NGC	kA
672	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
NGC	NGC	kA
672	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
NASA	NASA	kA
<g/>
/	/	kIx~
<g/>
IPAC	IPAC	kA
Extragalactic	Extragalactice	k1gFnPc2
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Results	Resultsa	k1gFnPc2
for	forum	k1gNnPc2
NGC	NGC	kA
672	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
FROMMERT	FROMMERT	kA
<g/>
,	,	kIx,
Hartmut	Hartmut	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revised	Revised	k1gInSc1
NGC	NGC	kA
Data	datum	k1gNnPc1
for	forum	k1gNnPc2
NGC	NGC	kA
672	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SEDS	SEDS	kA
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SIMBAD	SIMBAD	kA
Astronomical	Astronomical	k1gFnSc1
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Results	Resultsa	k1gFnPc2
for	forum	k1gNnPc2
NGC	NGC	kA
672	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SELIGMAN	SELIGMAN	kA
<g/>
,	,	kIx,
Courtney	Courtney	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celestial	Celestial	k1gMnSc1
Atlas	Atlas	k1gMnSc1
<g/>
:	:	kIx,
NGC	NGC	kA
672	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Katalog	katalog	k1gInSc1
NGC	NGC	kA
672	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gMnSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
url	url	k?
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
//	//	k?
<g/>
upload	upload	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikimedia	wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
org	org	k?
<g/>
/	/	kIx~
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
commons	commons	k6eAd1
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
/	/	kIx~
<g/>
Skymap	Skymap	k1gInSc1
<g/>
.	.	kIx.
<g/>
png	png	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
right	right	k2eAgInSc1d1
no-repeat	no-repeat	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
vertical-align	vertical-align	k1gNnSc1
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
{	{	kIx(
<g/>
font-family	font-famit	k5eAaPmAgFnP
<g/>
:	:	kIx,
<g/>
monospace	monospace	k1gFnSc1
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
85	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
