<s>
Repulsion	Repulsion	k1gInSc1
</s>
<s>
Repulsion	Repulsion	k1gInSc1
Koncert	koncert	k1gInSc1
skupiny	skupina	k1gFnSc2
Repulsion	Repulsion	k1gInSc1
<g/>
.	.	kIx.
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Původ	původ	k1gInSc1
</s>
<s>
Flint	flint	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
Žánry	žánr	k1gInPc7
</s>
<s>
grindcore	grindcor	k1gMnSc5
<g/>
,	,	kIx,
death	death	k1gMnSc1
metal	metat	k5eAaImAgMnS
Aktivní	aktivní	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
1986	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
–	–	k?
Vydavatelé	vydavatel	k1gMnPc5
</s>
<s>
Necrosis	Necrosis	k1gFnSc1
Records	Recordsa	k1gFnPc2
<g/>
,	,	kIx,
Relapse	relaps	k1gInSc5
Records	Records	k1gInSc4
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Death	Death	k1gMnSc1
<g/>
,	,	kIx,
Současní	současný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Col	cola	k1gFnPc2
Jones	Jones	k1gMnSc1
<g/>
,	,	kIx,
Matt	Matt	k1gMnSc1
Olivo	oliva	k1gFnSc5
<g/>
,	,	kIx,
Scott	Scott	k1gMnSc1
Carlson	Carlsona	k1gFnPc2
Dřívější	dřívější	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Matt	Matt	k1gMnSc1
Harvey	Harvea	k1gFnSc2
<g/>
,	,	kIx,
Mike	Mike	k1gFnSc1
Beams	Beams	k1gInSc1
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
"	"	kIx"
<g/>
Fish	Fish	k1gInSc1
<g/>
"	"	kIx"
Perro	Perro	k1gNnSc1
<g/>
,	,	kIx,
Dave	Dav	k1gInSc5
"	"	kIx"
<g/>
Grave	grave	k1gNnPc6
<g/>
"	"	kIx"
Hollingshead	Hollingshead	k1gInSc1
<g/>
,	,	kIx,
Aaron	Aaron	k1gMnSc1
Freeman	Freeman	k1gMnSc1
<g/>
,	,	kIx,
Marissa	Marissa	k1gFnSc1
Martinez	Martineza	k1gFnPc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Repulsion	Repulsion	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
znamená	znamenat	k5eAaImIp3nS
odpor	odpor	k1gInSc1
<g/>
,	,	kIx,
averze	averze	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
legendární	legendární	k2eAgFnSc1d1
americká	americký	k2eAgFnSc1d1
metalová	metalový	k2eAgFnSc1d1
kapela	kapela	k1gFnSc1
založená	založený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
ve	v	k7c6
Flintu	flint	k1gInSc6
v	v	k7c6
Michiganu	Michigan	k1gInSc6
(	(	kIx(
<g/>
předtím	předtím	k6eAd1
její	její	k3xOp3gMnPc1
členové	člen	k1gMnPc1
hráli	hrát	k5eAaImAgMnP
ve	v	k7c6
skupině	skupina	k1gFnSc6
Genocide	Genocid	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrála	hrát	k5eAaImAgFnS
extrémní	extrémní	k2eAgInSc4d1
death	death	k1gInSc4
metal	metat	k5eAaImAgInS
a	a	k8xC
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
jednou	jeden	k4xCgFnSc7
z	z	k7c2
několika	několik	k4yIc2
kapel	kapela	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
stály	stát	k5eAaImAgFnP
u	u	k7c2
zrodu	zrod	k1gInSc2
žánru	žánr	k1gInSc2
grindcore	grindcor	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
ovlivněná	ovlivněný	k2eAgFnSc1d1
metalovými	metalový	k2eAgNnPc7d1
tělesy	těleso	k1gNnPc7
jako	jako	k9
Celtic	Celtice	k1gFnPc2
Frost	Frost	k1gMnSc1
<g/>
,	,	kIx,
Hellhammer	Hellhammer	k1gMnSc1
<g/>
,	,	kIx,
Slayer	Slayer	k1gMnSc1
<g/>
,	,	kIx,
Slaughter	Slaughter	k1gMnSc1
a	a	k8xC
Discharge	Discharge	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
kapely	kapela	k1gFnSc2
chvíli	chvíle	k1gFnSc4
spolupracovali	spolupracovat	k5eAaImAgMnP
s	s	k7c7
Chuckem	Chuck	k1gInSc7
Schuldinerem	Schuldiner	k1gMnSc7
<g/>
,	,	kIx,
frontmanem	frontman	k1gMnSc7
kultovní	kultovní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Death	Death	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Skladby	skladba	k1gFnPc1
od	od	k7c2
Repulsion	Repulsion	k1gInSc1
hrálo	hrát	k5eAaImAgNnS
nemálo	málo	k6eNd1
kapel	kapela	k1gFnPc2
jako	jako	k8xC,k8xS
coververze	coververze	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
Entombed	Entombed	k1gInSc1
<g/>
,	,	kIx,
Napalm	napalm	k1gInSc1
Death	Death	k1gInSc1
<g/>
,	,	kIx,
Impaled	Impaled	k1gMnSc1
<g/>
,	,	kIx,
Pig	Pig	k1gMnSc1
Destroyer	Destroyer	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
bylo	být	k5eAaImAgNnS
nahráno	nahrát	k5eAaBmNgNnS,k5eAaPmNgNnS
demo	demo	k2eAgFnSc1d1
Slaughter	Slaughter	k1gInSc1
of	of	k?
the	the	k?
Innocent	Innocent	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
vydáno	vydat	k5eAaPmNgNnS
jako	jako	k9
album	album	k1gNnSc1
s	s	k7c7
názvem	název	k1gInSc7
Horrified	Horrified	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kapela	kapela	k1gFnSc1
Repulsion	Repulsion	k1gInSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
v	v	k7c6
americkém	americký	k2eAgNnSc6d1
městě	město	k1gNnSc6
Flint	flinta	k1gFnPc2
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
hráli	hrát	k5eAaImAgMnP
její	její	k3xOp3gMnPc1
členové	člen	k1gMnPc1
pod	pod	k7c7
názvem	název	k1gInSc7
Genocide	Genocid	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
sestava	sestava	k1gFnSc1
zněla	znět	k5eAaImAgFnS
<g/>
:	:	kIx,
Scott	Scott	k1gMnSc1
Carlson	Carlson	k1gMnSc1
(	(	kIx(
<g/>
vokály	vokál	k1gInPc1
<g/>
,	,	kIx,
baskytara	baskytara	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Aaron	Aaron	k1gMnSc1
Freeman	Freeman	k1gMnSc1
(	(	kIx(
<g/>
kytara	kytara	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Matt	Matt	k1gMnSc1
Olivo	Oliva	k1gMnSc5
(	(	kIx(
<g/>
kytara	kytara	k1gFnSc1
<g/>
)	)	kIx)
and	and	k?
Dave	Dav	k1gInSc5
Grave	grave	k1gNnPc3
(	(	kIx(
<g/>
bicí	bicí	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
vyšlo	vyjít	k5eAaPmAgNnS
ještě	ještě	k6eAd1
pod	pod	k7c7
názvem	název	k1gInSc7
Genocide	Genocid	k1gInSc5
demo	demo	k2eAgFnSc1d1
Stench	Stench	k1gInSc1
of	of	k?
Burning	Burning	k1gInSc1
Death	Death	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bestiální	bestiální	k2eAgInSc1d1
zvuk	zvuk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
produkovali	produkovat	k5eAaImAgMnP
na	na	k7c6
tomto	tento	k3xDgInSc6
demu	demus	k1gInSc6
a	a	k8xC
také	také	k9
v	v	k7c6
klubech	klub	k1gInPc6
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
předběhl	předběhnout	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
i	i	k9
mimo	mimo	k7c4
rámec	rámec	k1gInSc4
rodícího	rodící	k2eAgInSc2d1
se	se	k3xPyFc4
death	death	k1gInSc1
metalu	metal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
neslýchaná	slýchaný	k2eNgFnSc1d1
ďábelská	ďábelský	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
s	s	k7c7
ultrarychlými	ultrarychlý	k2eAgInPc7d1
bicími	bicí	k2eAgInPc7d1
<g/>
,	,	kIx,
hororové	hororový	k2eAgInPc1d1
texty	text	k1gInPc1
-	-	kIx~
tím	ten	k3xDgNnSc7
vším	všecek	k3xTgNnSc7
se	se	k3xPyFc4
kapela	kapela	k1gFnSc1
prezentovala	prezentovat	k5eAaBmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demo	demo	k2eAgMnPc2d1
bylo	být	k5eAaImAgNnS
nadšeně	nadšeně	k6eAd1
přijato	přijmout	k5eAaPmNgNnS
v	v	k7c6
undergroundu	underground	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začínal	začínat	k5eAaImAgMnS
se	se	k3xPyFc4
formovat	formovat	k5eAaImF
styl	styl	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
nazývá	nazývat	k5eAaImIp3nS
grindcore	grindcor	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frontman	Frontman	k1gMnSc1
skupiny	skupina	k1gFnSc2
Death	Death	k1gMnSc1
Chuck	Chuck	k1gMnSc1
Schuldiner	Schuldiner	k1gMnSc1
(	(	kIx(
<g/>
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
právě	právě	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
z	z	k7c2
Kanady	Kanada	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
pokoušel	pokoušet	k5eAaImAgMnS
založit	založit	k5eAaPmF
spolupráci	spolupráce	k1gFnSc4
se	s	k7c7
Slaughter	Slaughtra	k1gFnPc2
<g/>
)	)	kIx)
požádal	požádat	k5eAaPmAgMnS
Scotta	Scotta	k1gMnSc1
a	a	k8xC
Matta	Matta	k1gMnSc1
<g/>
,	,	kIx,
zda	zda	k8xS
by	by	kYmCp3nP
nepřijeli	přijet	k5eNaPmAgMnP
na	na	k7c4
Floridu	Florida	k1gFnSc4
a	a	k8xC
nestali	stát	k5eNaPmAgMnP
se	s	k7c7
členy	člen	k1gInPc7
jeho	jeho	k3xOp3gFnSc2
kapely	kapela	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
souhlasili	souhlasit	k5eAaImAgMnP
a	a	k8xC
Repulsion	Repulsion	k1gInSc4
tak	tak	k6eAd1
šel	jít	k5eAaImAgMnS
na	na	k7c6
chvíli	chvíle	k1gFnSc6
k	k	k7c3
ledu	led	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
Michiganu	Michigan	k1gInSc2
(	(	kIx(
<g/>
zatímco	zatímco	k8xS
Chuck	Chuck	k1gMnSc1
odcestoval	odcestovat	k5eAaPmAgMnS
do	do	k7c2
San	San	k1gMnSc2
Francisca	Franciscus	k1gMnSc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
opět	opět	k6eAd1
pokračovali	pokračovat	k5eAaImAgMnP
s	s	k7c7
Repulsion	Repulsion	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1986	#num#	k4
bylo	být	k5eAaImAgNnS
nahráno	nahrát	k5eAaPmNgNnS,k5eAaBmNgNnS
demo	demo	k2eAgFnSc1d1
Slaughter	Slaughter	k1gInSc1
of	of	k?
the	the	k?
Innocent	Innocent	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
pro	pro	k7c4
rostoucí	rostoucí	k2eAgFnSc4d1
popularitu	popularita	k1gFnSc4
přikročila	přikročit	k5eAaPmAgFnS
ke	k	k7c3
změně	změna	k1gFnSc3
názvu	název	k1gInSc2
na	na	k7c4
Repulsion	Repulsion	k1gInSc4
(	(	kIx(
<g/>
aby	aby	kYmCp3nS
nedocházelo	docházet	k5eNaImAgNnS
k	k	k7c3
záměně	záměna	k1gFnSc3
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
stejnojmennými	stejnojmenný	k2eAgFnPc7d1
kapelami	kapela	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahrávka	nahrávka	k1gFnSc1
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
jako	jako	k8xS,k8xC
album	album	k1gNnSc1
s	s	k7c7
názvem	název	k1gInSc7
Horrified	Horrified	k1gMnSc1
firmičkou	firmička	k1gFnSc7
Necrosis	Necrosis	k1gFnPc2
Records	Recordsa	k1gFnPc2
čerstvě	čerstvě	k6eAd1
založenou	založený	k2eAgFnSc7d1
Billem	Bill	k1gMnSc7
Steerem	Steero	k1gNnSc7
a	a	k8xC
Jeffrey	Jeffre	k1gMnPc4
Walkerem	Walkero	k1gNnSc7
<g/>
,	,	kIx,
členy	člen	k1gInPc7
britské	britský	k2eAgFnSc2d1
kapely	kapela	k1gFnSc2
Carcass	Carcassa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
LP	LP	kA
dosáhlo	dosáhnout	k5eAaPmAgNnS
vysoké	vysoký	k2eAgFnPc4d1
popularity	popularita	k1gFnPc4
a	a	k8xC
kapela	kapela	k1gFnSc1
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
tvorbě	tvorba	k1gFnSc6
a	a	k8xC
příležitostném	příležitostný	k2eAgNnSc6d1
koncertování	koncertování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
se	se	k3xPyFc4
skupina	skupina	k1gFnSc1
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Genocide	Genocid	k1gMnSc5
</s>
<s>
Demo	demo	k2eAgFnPc1d1
nahrávky	nahrávka	k1gFnPc1
</s>
<s>
Toxic	Toxic	k1gMnSc1
Metal	metat	k5eAaImAgMnS
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Violent	Violent	k1gMnSc1
Death	Death	k1gMnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Stench	Stench	k1gInSc1
of	of	k?
Burning	Burning	k1gInSc1
Death	Death	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Repulsion	Repulsion	k1gInSc1
</s>
<s>
Demo	demo	k2eAgFnPc1d1
nahrávky	nahrávka	k1gFnPc1
</s>
<s>
Slaughter	Slaughter	k1gInSc1
of	of	k?
the	the	k?
Innocent	Innocent	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rebirth	Rebirth	k1gInSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Final	Final	k1gMnSc1
Demo	demo	k2eAgMnSc1d1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alba	alba	k1gFnSc1
</s>
<s>
Horrified	Horrified	k1gInSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Singly	singl	k1gInPc1
</s>
<s>
Excruciation	Excruciation	k1gInSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Martin	Martin	k1gMnSc1
„	„	k?
<g/>
CYKLO	CYKLO	kA
<g/>
“	“	k?
Cvilink	cvilink	k1gInSc1
<g/>
:	:	kIx,
Morbidious	Morbidious	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
Death	Death	k1gInSc1
aneb	aneb	k?
dějiny	dějiny	k1gFnPc4
smrti	smrt	k1gFnSc2
(	(	kIx(
<g/>
Part	part	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Spark	Spark	k1gInSc1
<g/>
,	,	kIx,
No	no	k9
<g/>
.8	.8	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
46	#num#	k4
<g/>
↑	↑	k?
Repulsion	Repulsion	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
9	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
relapse	relaps	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Repulsion	Repulsion	k1gInSc1
-	-	kIx~
biography	biographa	k1gFnPc1
<g/>
,	,	kIx,
Allmusic	Allmusic	k1gMnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Repulsion	Repulsion	k1gInSc1
<g/>
,	,	kIx,
Discogs	Discogs	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Repulsion	Repulsion	k1gInSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Encyclopaedia	Encyclopaedium	k1gNnSc2
Metallum	Metallum	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Repulsion	Repulsion	k1gInSc1
<g/>
,	,	kIx,
Myspace	Myspace	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Repulsion	Repulsion	k1gInSc1
<g/>
,	,	kIx,
Last	Last	k1gInSc1
<g/>
.	.	kIx.
<g/>
fm	fm	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
10333190-6	10333190-6	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
9252	#num#	k4
301X	301X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
99041842	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
135665985	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
99041842	#num#	k4
</s>
