<p>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Lingvo	Lingvo	k1gNnSc1	Lingvo
Internacia	Internacium	k1gNnSc2	Internacium
–	–	k?	–
"	"	kIx"	"
<g/>
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
plánovým	plánový	k2eAgInSc7d1	plánový
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
ze	z	k7c2	z
pseudonymu	pseudonym	k1gInSc2	pseudonym
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgInSc7	jenž
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
polský	polský	k2eAgMnSc1d1	polský
židovský	židovský	k2eAgMnSc1d1	židovský
lékař	lékař	k1gMnSc1	lékař
Ludvík	Ludvík	k1gMnSc1	Ludvík
Lazar	Lazar	k1gMnSc1	Lazar
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
základy	základ	k1gInPc1	základ
této	tento	k3xDgFnSc2	tento
řeči	řeč	k1gFnSc2	řeč
publikoval	publikovat	k5eAaBmAgMnS	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
Záměrem	záměr	k1gInSc7	záměr
tvůrce	tvůrce	k1gMnSc2	tvůrce
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
snadno	snadno	k6eAd1	snadno
naučitelný	naučitelný	k2eAgInSc4d1	naučitelný
neutrální	neutrální	k2eAgInSc4d1	neutrální
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
vhodný	vhodný	k2eAgInSc4d1	vhodný
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
nahradit	nahradit	k5eAaPmF	nahradit
ostatní	ostatní	k1gNnSc4	ostatní
národní	národní	k2eAgFnSc2d1	národní
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
žádná	žádný	k3yNgFnSc1	žádný
země	země	k1gFnSc1	země
nepřijala	přijmout	k5eNaPmAgFnS	přijmout
esperanto	esperanto	k1gNnSc4	esperanto
jako	jako	k8xS	jako
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
komunitou	komunita	k1gFnSc7	komunita
o	o	k7c6	o
odhadovaném	odhadovaný	k2eAgInSc6d1	odhadovaný
počtu	počet	k1gInSc6	počet
100	[number]	k4	100
000	[number]	k4	000
až	až	k9	až
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
asi	asi	k9	asi
tisícovku	tisícovka	k1gFnSc4	tisícovka
tvoří	tvořit	k5eAaImIp3nP	tvořit
mluvčí	mluvčí	k1gMnPc1	mluvčí
rodilí	rodilý	k2eAgMnPc1d1	rodilý
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
také	také	k9	také
některých	některý	k3yIgFnPc2	některý
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
uznání	uznání	k1gNnPc2	uznání
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
rezoluce	rezoluce	k1gFnPc1	rezoluce
UNESCO	UNESCO	kA	UNESCO
či	či	k8xC	či
podpora	podpora	k1gFnSc1	podpora
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
esperanto	esperanto	k1gNnSc1	esperanto
využíváno	využíván	k2eAgNnSc1d1	využíváno
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
cestování	cestování	k1gNnSc2	cestování
<g/>
,	,	kIx,	,
dopisování	dopisování	k1gNnPc2	dopisování
<g/>
,	,	kIx,	,
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
setkání	setkání	k1gNnPc2	setkání
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
výměn	výměna	k1gFnPc2	výměna
<g/>
,	,	kIx,	,
kongresů	kongres	k1gInPc2	kongres
<g/>
,	,	kIx,	,
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
diskuzí	diskuze	k1gFnPc2	diskuze
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc2d1	původní
i	i	k8xC	i
překladové	překladový	k2eAgFnSc2d1	překladová
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
kina	kino	k1gNnSc2	kino
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
tištěného	tištěný	k2eAgNnSc2d1	tištěné
i	i	k8xC	i
internetového	internetový	k2eAgNnSc2d1	internetové
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
<g/>
,	,	kIx,	,
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
a	a	k8xC	a
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
esperanta	esperanto	k1gNnSc2	esperanto
pochází	pocházet	k5eAaImIp3nS	pocházet
především	především	k9	především
ze	z	k7c2	z
západoevropských	západoevropský	k2eAgMnPc2d1	západoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gFnSc1	jeho
skladba	skladba	k1gFnSc1	skladba
a	a	k8xC	a
tvarosloví	tvarosloví	k1gNnSc4	tvarosloví
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
silný	silný	k2eAgInSc4d1	silný
slovanský	slovanský	k2eAgInSc4d1	slovanský
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Morfémy	morfém	k1gInPc1	morfém
jsou	být	k5eAaImIp3nP	být
neměnné	měnný	k2eNgNnSc4d1	neměnné
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
kombinovat	kombinovat	k5eAaImF	kombinovat
do	do	k7c2	do
rozmanitých	rozmanitý	k2eAgNnPc2d1	rozmanité
slov	slovo	k1gNnPc2	slovo
<g/>
;	;	kIx,	;
esperanto	esperanto	k1gNnSc1	esperanto
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
mnoho	mnoho	k4c4	mnoho
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
izolujícími	izolující	k2eAgMnPc7d1	izolující
jazyky	jazyk	k1gMnPc7	jazyk
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
struktura	struktura	k1gFnSc1	struktura
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
připomíná	připomínat	k5eAaImIp3nS	připomínat
jazyky	jazyk	k1gInPc1	jazyk
aglutinační	aglutinační	k2eAgInPc1d1	aglutinační
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
japonština	japonština	k1gFnSc1	japonština
<g/>
,	,	kIx,	,
svahilština	svahilština	k1gFnSc1	svahilština
nebo	nebo	k8xC	nebo
turečtina	turečtina	k1gFnSc1	turečtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zamenhofovo	Zamenhofův	k2eAgNnSc1d1	Zamenhofův
dětství	dětství	k1gNnSc1	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
esperanta	esperanto	k1gNnSc2	esperanto
stál	stát	k5eAaImAgMnS	stát
Ludvík	Ludvík	k1gMnSc1	Ludvík
Lazar	Lazar	k1gMnSc1	Lazar
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
mnohojazyčném	mnohojazyčný	k2eAgInSc6d1	mnohojazyčný
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
k	k	k7c3	k
Ruskému	ruský	k2eAgNnSc3d1	ruské
impériu	impérium	k1gNnSc3	impérium
náležejícím	náležející	k2eAgInSc6d1	náležející
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
polském	polský	k2eAgNnSc6d1	polské
městě	město	k1gNnSc6	město
Bělostoku	Bělostok	k1gInSc2	Bělostok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
svědkem	svědek	k1gMnSc7	svědek
častých	častý	k2eAgFnPc2d1	častá
rozepří	rozepře	k1gFnPc2	rozepře
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
etniky	etnikum	k1gNnPc7	etnikum
(	(	kIx(	(
<g/>
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Židé	Žid	k1gMnPc1	Žid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
příčin	příčina	k1gFnPc2	příčina
těchto	tento	k3xDgInPc2	tento
konfliktů	konflikt	k1gInPc2	konflikt
považoval	považovat	k5eAaImAgInS	považovat
neexistenci	neexistence	k1gFnSc4	neexistence
společného	společný	k2eAgInSc2d1	společný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
již	již	k6eAd1	již
jako	jako	k9	jako
školák	školák	k1gMnSc1	školák
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
nové	nový	k2eAgFnSc2d1	nová
vhodné	vhodný	k2eAgFnSc2d1	vhodná
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
mohla	moct	k5eAaImAgFnS	moct
plnit	plnit	k5eAaImF	plnit
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
–	–	k?	–
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
–	–	k?	–
neutrální	neutrální	k2eAgInPc1d1	neutrální
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
naučitelná	naučitelný	k2eAgFnSc1d1	naučitelná
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
také	také	k9	také
přijatelná	přijatelný	k2eAgFnSc1d1	přijatelná
jako	jako	k8xC	jako
druhý	druhý	k4xOgInSc1	druhý
jazyk	jazyk	k1gInSc1	jazyk
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc4	jazyk
vyučovaný	vyučovaný	k2eAgInSc4d1	vyučovaný
společně	společně	k6eAd1	společně
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
národními	národní	k2eAgMnPc7d1	národní
a	a	k8xC	a
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
vyžadujících	vyžadující	k2eAgFnPc6d1	vyžadující
dorozumění	dorozumění	k1gNnSc1	dorozumění
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
===	===	k?	===
</s>
</p>
<p>
<s>
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
nejdříve	dříve	k6eAd3	dříve
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c4	o
oživení	oživení	k1gNnSc4	oživení
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
učil	učít	k5eAaPmAgMnS	učít
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
dorozumívání	dorozumívání	k1gNnSc4	dorozumívání
zbytečně	zbytečně	k6eAd1	zbytečně
složitá	složitý	k2eAgNnPc1d1	složité
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
studoval	studovat	k5eAaImAgMnS	studovat
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
povšiml	povšimnout	k5eAaPmAgInS	povšimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
časování	časování	k1gNnSc4	časování
sloves	sloveso	k1gNnPc2	sloveso
podle	podle	k7c2	podle
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
čísla	číslo	k1gNnSc2	číslo
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
a	a	k8xC	a
že	že	k8xS	že
gramatický	gramatický	k2eAgInSc1d1	gramatický
systém	systém	k1gInSc1	systém
jazyka	jazyk	k1gInSc2	jazyk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
mnohem	mnohem	k6eAd1	mnohem
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
předtím	předtím	k6eAd1	předtím
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
však	však	k9	však
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
překážka	překážka	k1gFnSc1	překážka
v	v	k7c6	v
učení	učení	k1gNnSc6	učení
se	se	k3xPyFc4	se
velkému	velký	k2eAgInSc3d1	velký
množství	množství	k1gNnSc1	množství
slovíček	slovíčko	k1gNnPc2	slovíčko
nazpaměť	nazpaměť	k6eAd1	nazpaměť
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
Zamenhofa	Zamenhof	k1gMnSc2	Zamenhof
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
dva	dva	k4xCgInPc1	dva
ruské	ruský	k2eAgInPc1d1	ruský
nápisy	nápis	k1gInPc1	nápis
<g/>
:	:	kIx,	:
ш	ш	k?	ш
[	[	kIx(	[
<g/>
švejcarskaja	švejcarskaja	k1gMnSc1	švejcarskaja
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
vrátnice	vrátnice	k1gFnSc1	vrátnice
<g/>
,	,	kIx,	,
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
ш	ш	k?	ш
[	[	kIx(	[
<g/>
švejcar	švejcar	k?	švejcar
<g/>
]	]	kIx)	]
–	–	k?	–
vrátný	vrátný	k1gMnSc1	vrátný
<g/>
)	)	kIx)	)
a	a	k8xC	a
к	к	k?	к
[	[	kIx(	[
<g/>
konditěrskaja	konditěrskaja	k1gMnSc1	konditěrskaja
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
cukrárna	cukrárna	k1gFnSc1	cukrárna
<g/>
,	,	kIx,	,
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
к	к	k?	к
[	[	kIx(	[
<g/>
konditěr	konditěr	k1gMnSc1	konditěr
<g/>
]	]	kIx)	]
–	–	k?	–
cukrář	cukrář	k1gMnSc1	cukrář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
stejného	stejný	k2eAgNnSc2d1	stejné
zakončení	zakončení	k1gNnSc2	zakončení
mu	on	k3xPp3gMnSc3	on
vnukla	vnuknout	k5eAaPmAgFnS	vnuknout
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
užívání	užívání	k1gNnSc1	užívání
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
předpon	předpona	k1gFnPc2	předpona
a	a	k8xC	a
přípon	přípona	k1gFnPc2	přípona
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
významně	významně	k6eAd1	významně
snížit	snížit	k5eAaPmF	snížit
množství	množství	k1gNnSc4	množství
slovních	slovní	k2eAgInPc2d1	slovní
kořenů	kořen	k1gInPc2	kořen
nutných	nutný	k2eAgInPc2d1	nutný
k	k	k7c3	k
dorozumění	dorozumění	k1gNnSc3	dorozumění
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
kořeny	kořen	k1gInPc4	kořen
co	co	k9	co
nejmezinárodnější	mezinárodní	k2eAgMnSc1d3	mezinárodní
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
převzít	převzít	k5eAaPmF	převzít
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
především	především	k9	především
z	z	k7c2	z
románských	románský	k2eAgInPc2d1	románský
a	a	k8xC	a
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
tehdy	tehdy	k6eAd1	tehdy
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
vyučovány	vyučovat	k5eAaImNgFnP	vyučovat
nejčastěji	často	k6eAd3	často
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
konečné	konečný	k2eAgFnSc2d1	konečná
verze	verze	k1gFnSc2	verze
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
Zamenhofův	Zamenhofův	k2eAgInSc1d1	Zamenhofův
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Lingwe	Lingwe	k1gInSc1	Lingwe
Uniwersala	Uniwersala	k1gFnSc2	Uniwersala
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
víceméně	víceméně	k9	víceméně
hotov	hotov	k2eAgInSc1d1	hotov
již	již	k9	již
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
autorův	autorův	k2eAgMnSc1d1	autorův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgMnS	považovat
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
za	za	k7c4	za
marnou	marný	k2eAgFnSc4d1	marná
a	a	k8xC	a
utopistickou	utopistický	k2eAgFnSc4d1	utopistická
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
zřejmě	zřejmě	k6eAd1	zřejmě
rukopis	rukopis	k1gInSc4	rukopis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
svěřen	svěřit	k5eAaPmNgMnS	svěřit
<g/>
,	,	kIx,	,
zničil	zničit	k5eAaPmAgMnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1885	[number]	k4	1885
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
studoval	studovat	k5eAaImAgInS	studovat
lékařství	lékařství	k1gNnSc4	lékařství
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
začal	začít	k5eAaPmAgInS	začít
opět	opět	k6eAd1	opět
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc6	první
obnovené	obnovený	k2eAgFnSc6d1	obnovená
verzi	verze	k1gFnSc6	verze
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
již	již	k6eAd1	již
překládal	překládat	k5eAaImAgMnS	překládat
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jazyk	jazyk	k1gInSc1	jazyk
co	co	k3yQnSc4	co
nejvíce	hodně	k6eAd3	hodně
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
autor	autor	k1gMnSc1	autor
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
jsem	být	k5eAaImIp1nS	být
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
zdokonalování	zdokonalování	k1gNnSc4	zdokonalování
a	a	k8xC	a
zkoušení	zkoušení	k1gNnSc4	zkoušení
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
úplně	úplně	k6eAd1	úplně
hotov	hotov	k2eAgInSc1d1	hotov
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Proto-esperanto	Protosperanta	k1gFnSc5	Proto-esperanta
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
svůj	svůj	k3xOyFgInSc4	svůj
koncept	koncept	k1gInSc4	koncept
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
,	,	kIx,	,
carští	carský	k2eAgMnPc1d1	carský
cenzoři	cenzor	k1gMnPc1	cenzor
to	ten	k3xDgNnSc4	ten
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
povolit	povolit	k5eAaPmF	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
zklamán	zklamat	k5eAaPmNgMnS	zklamat
a	a	k8xC	a
volný	volný	k2eAgInSc1d1	volný
čas	čas	k1gInSc1	čas
trávil	trávit	k5eAaImAgInS	trávit
překládáním	překládání	k1gNnSc7	překládání
děl	dělo	k1gNnPc2	dělo
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
či	či	k8xC	či
některé	některý	k3yIgInPc1	některý
kusy	kus	k1gInPc1	kus
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nucené	nucený	k2eAgNnSc1d1	nucené
zpoždění	zpoždění	k1gNnSc1	zpoždění
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
významnému	významný	k2eAgNnSc3d1	významné
vylepšení	vylepšení	k1gNnSc3	vylepšení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
konečně	konečně	k6eAd1	konečně
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgFnSc4	první
učebnici	učebnice	k1gFnSc4	učebnice
"	"	kIx"	"
<g/>
М	М	k?	М
я	я	k?	я
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
jazyk	jazyk	k1gInSc1	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
tentýž	týž	k3xTgInSc4	týž
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Zamenhofova	Zamenhofův	k2eAgInSc2d1	Zamenhofův
pseudonymu	pseudonym	k1gInSc2	pseudonym
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgInSc7	jenž
knihu	kniha	k1gFnSc4	kniha
vydal	vydat	k5eAaPmAgMnS	vydat
<g/>
,	,	kIx,	,
Doktoro	Doktora	k1gFnSc5	Doktora
Esperanto	esperanto	k1gNnSc1	esperanto
(	(	kIx(	(
<g/>
esperanto	esperanto	k1gNnSc1	esperanto
–	–	k?	–
doufající	doufající	k2eAgFnPc1d1	doufající
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
řeč	řeč	k1gFnSc1	řeč
brzy	brzy	k6eAd1	brzy
začala	začít	k5eAaPmAgFnS	začít
označovat	označovat	k5eAaImF	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Jazyk	jazyk	k1gInSc1	jazyk
doktora	doktor	k1gMnSc2	doktor
Esperanta	esperanto	k1gNnSc2	esperanto
<g/>
"	"	kIx"	"
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zkráceně	zkráceně	k6eAd1	zkráceně
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
esperanto	esperanto	k1gNnSc1	esperanto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
změny	změna	k1gFnPc4	změna
===	===	k?	===
</s>
</p>
<p>
<s>
Zamenhofovi	Zamenhof	k1gMnSc3	Zamenhof
přicházelo	přicházet	k5eAaImAgNnS	přicházet
veliké	veliký	k2eAgNnSc1d1	veliké
množství	množství	k1gNnSc1	množství
nadšených	nadšený	k2eAgInPc2d1	nadšený
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
často	často	k6eAd1	často
přinášely	přinášet	k5eAaImAgInP	přinášet
také	také	k9	také
návrhy	návrh	k1gInPc1	návrh
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
úprav	úprava	k1gFnPc2	úprava
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
podněty	podnět	k1gInPc4	podnět
zaznamenával	zaznamenávat	k5eAaImAgInS	zaznamenávat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
začal	začít	k5eAaPmAgMnS	začít
i	i	k9	i
uveřejňovat	uveřejňovat	k5eAaImF	uveřejňovat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Esperantisto	esperantista	k1gMnSc5	esperantista
<g/>
,	,	kIx,	,
vycházejícím	vycházející	k2eAgNnSc7d1	vycházející
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgNnSc6	týž
periodiku	periodikum	k1gNnSc6	periodikum
nechal	nechat	k5eAaPmAgMnS	nechat
o	o	k7c6	o
úpravách	úprava	k1gFnPc6	úprava
také	také	k9	také
dvakrát	dvakrát	k6eAd1	dvakrát
hlasovat	hlasovat	k5eAaImF	hlasovat
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
čtenářů	čtenář	k1gMnPc2	čtenář
však	však	k9	však
se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgNnPc6	tento
hlasováních	hlasování	k1gNnPc6	hlasování
na	na	k7c4	na
jistý	jistý	k2eAgInSc4d1	jistý
čas	čas	k1gInSc4	čas
ztichly	ztichnout	k5eAaPmAgInP	ztichnout
hlasy	hlas	k1gInPc1	hlas
volající	volající	k2eAgInPc1d1	volající
po	po	k7c6	po
reformě	reforma	k1gFnSc6	reforma
a	a	k8xC	a
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
odběratelů	odběratel	k1gMnPc2	odběratel
měl	mít	k5eAaImAgInS	mít
časopis	časopis	k1gInSc1	časopis
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
ranou	rána	k1gFnSc7	rána
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
tedy	tedy	k9	tedy
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
carská	carský	k2eAgFnSc1d1	carská
cenzura	cenzura	k1gFnSc1	cenzura
jeho	jeho	k3xOp3gNnSc2	jeho
šíření	šíření	k1gNnSc2	šíření
zakázala	zakázat	k5eAaPmAgFnS	zakázat
kvůli	kvůli	k7c3	kvůli
článku	článek	k1gInSc3	článek
Lva	Lev	k1gMnSc2	Lev
Nikolajeviče	Nikolajevič	k1gMnSc2	Nikolajevič
Tolstého	Tolstý	k2eAgMnSc2d1	Tolstý
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgMnSc3	ten
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zanedlouho	zanedlouho	k6eAd1	zanedlouho
byl	být	k5eAaImAgInS	být
vystřídán	vystřídat	k5eAaPmNgInS	vystřídat
novým	nový	k2eAgInSc7d1	nový
<g/>
,	,	kIx,	,
nazvaným	nazvaný	k2eAgInSc7d1	nazvaný
Lingvo	Lingvo	k1gNnSc4	Lingvo
Internacia	Internacium	k1gNnSc2	Internacium
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
redigován	redigovat	k5eAaImNgInS	redigovat
ve	v	k7c6	v
švédské	švédský	k2eAgFnSc6d1	švédská
Uppsale	Uppsala	k1gFnSc6	Uppsala
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gNnSc4	jeho
vydávání	vydávání	k1gNnSc4	vydávání
zastavila	zastavit	k5eAaPmAgFnS	zastavit
až	až	k9	až
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozvoj	rozvoj	k1gInSc1	rozvoj
jazykové	jazykový	k2eAgFnSc2d1	jazyková
komunity	komunita	k1gFnSc2	komunita
===	===	k?	===
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
jazyk	jazyk	k1gInSc1	jazyk
začali	začít	k5eAaPmAgMnP	začít
brzy	brzy	k6eAd1	brzy
jeho	jeho	k3xOp3gMnPc1	jeho
uživatelé	uživatel	k1gMnPc1	uživatel
používat	používat	k5eAaImF	používat
také	také	k9	také
k	k	k7c3	k
organizaci	organizace	k1gFnSc3	organizace
odborné	odborný	k2eAgFnSc3d1	odborná
a	a	k8xC	a
zájmové	zájmový	k2eAgFnSc3d1	zájmová
činnosti	činnost	k1gFnSc3	činnost
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
desetiletích	desetiletí	k1gNnPc6	desetiletí
probíhala	probíhat	k5eAaImAgFnS	probíhat
komunikace	komunikace	k1gFnSc1	komunikace
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
písemnou	písemný	k2eAgFnSc7d1	písemná
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
nečekaně	nečekaně	k6eAd1	nečekaně
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
prvním	první	k4xOgMnSc6	první
Světovém	světový	k2eAgInSc6d1	světový
kongresu	kongres	k1gInSc6	kongres
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
,	,	kIx,	,
uspořádaném	uspořádaný	k2eAgInSc6d1	uspořádaný
roku	rok	k1gInSc6	rok
1905	[number]	k4	1905
ve	v	k7c4	v
francouzské	francouzský	k2eAgInPc4d1	francouzský
Boulogne-sur-Mer	Boulogneur-Mer	k1gInSc4	Boulogne-sur-Mer
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
ověřily	ověřit	k5eAaPmAgFnP	ověřit
možnosti	možnost	k1gFnPc1	možnost
používání	používání	k1gNnSc2	používání
této	tento	k3xDgFnSc2	tento
řeči	řeč	k1gFnSc2	řeč
v	v	k7c6	v
mluvené	mluvený	k2eAgFnSc6d1	mluvená
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
nabývat	nabývat	k5eAaImF	nabývat
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
i	i	k9	i
osobní	osobní	k2eAgInPc4d1	osobní
kontakty	kontakt	k1gInPc4	kontakt
a	a	k8xC	a
setkávání	setkávání	k1gNnSc4	setkávání
mezi	mezi	k7c7	mezi
jejími	její	k3xOp3gMnPc7	její
mluvčími	mluvčí	k1gMnPc7	mluvčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
Zamenhofova	Zamenhofův	k2eAgInSc2d1	Zamenhofův
projevu	projev	k1gInSc2	projev
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
světovém	světový	k2eAgInSc6d1	světový
kongresu	kongres	k1gInSc6	kongres
esperanta	esperanto	k1gNnSc2	esperanto
ve	v	k7c4	v
francouzské	francouzský	k2eAgInPc4d1	francouzský
Boulogne-sur-Mer	Boulogneur-Mer	k1gInSc4	Boulogne-sur-Mer
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Esperanto	esperanto	k1gNnSc4	esperanto
brzy	brzy	k6eAd1	brzy
začaly	začít	k5eAaPmAgFnP	začít
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
používat	používat	k5eAaImF	používat
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
organizace	organizace	k1gFnPc4	organizace
a	a	k8xC	a
hnutí	hnutí	k1gNnPc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
několik	několik	k4yIc1	několik
setkání	setkání	k1gNnPc2	setkání
přítomných	přítomný	k2eAgMnPc2d1	přítomný
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
uspořádat	uspořádat	k5eAaPmF	uspořádat
v	v	k7c6	v
nadcházejícím	nadcházející	k2eAgInSc6d1	nadcházející
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
kongres	kongres	k1gInSc1	kongres
katolických	katolický	k2eAgMnPc2d1	katolický
esperantistů	esperantista	k1gMnPc2	esperantista
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
něj	on	k3xPp3gInSc2	on
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
sdružení	sdružení	k1gNnSc1	sdružení
katolických	katolický	k2eAgMnPc2d1	katolický
esperantistů	esperantista	k1gMnPc2	esperantista
(	(	kIx(	(
<g/>
IKUE	IKUE	kA	IKUE
–	–	k?	–
Internacia	Internacius	k1gMnSc2	Internacius
Katolika	Katolik	k1gMnSc2	Katolik
Unuiĝ	Unuiĝ	k1gMnSc1	Unuiĝ
Esperantista	esperantista	k1gMnSc1	esperantista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Espero	Espero	k1gNnSc1	Espero
Katolika	Katolik	k1gMnSc2	Katolik
(	(	kIx(	(
<g/>
Katolická	katolický	k2eAgFnSc1d1	katolická
naděje	naděje	k1gFnSc1	naděje
<g/>
)	)	kIx)	)
však	však	k9	však
vycházel	vycházet	k5eAaImAgInS	vycházet
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
a	a	k8xC	a
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
lety	let	k1gInPc4	let
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejdéle	dlouho	k6eAd3	dlouho
vycházejícím	vycházející	k2eAgNnSc7d1	vycházející
esperantským	esperantský	k2eAgNnSc7d1	esperantské
periodikem	periodikum	k1gNnSc7	periodikum
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
při	při	k7c6	při
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
projevu	projev	k1gInSc6	projev
během	během	k7c2	během
osmého	osmý	k4xOgInSc2	osmý
Světového	světový	k2eAgInSc2d1	světový
kongresu	kongres	k1gInSc2	kongres
esperanta	esperanto	k1gNnSc2	esperanto
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
vzdal	vzdát	k5eAaPmAgInS	vzdát
své	svůj	k3xOyFgFnPc4	svůj
oficiální	oficiální	k2eAgFnPc4d1	oficiální
role	role	k1gFnPc4	role
ve	v	k7c6	v
hnutí	hnutí	k1gNnSc6	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Desátý	desátý	k4xOgInSc4	desátý
kongres	kongres	k1gInSc4	kongres
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
konat	konat	k5eAaImF	konat
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
téměř	téměř	k6eAd1	téměř
4000	[number]	k4	4000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začínající	začínající	k2eAgFnSc1d1	začínající
válka	válka	k1gFnSc1	válka
jej	on	k3xPp3gInSc4	on
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
a	a	k8xC	a
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
jen	jen	k9	jen
přes	přes	k7c4	přes
skandinávské	skandinávský	k2eAgInPc4d1	skandinávský
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
světových	světový	k2eAgFnPc2d1	světová
válek	válka	k1gFnPc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
esperantský	esperantský	k2eAgInSc1d1	esperantský
svaz	svaz	k1gInSc1	svaz
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
v	v	k7c6	v
neutrálním	neutrální	k2eAgNnSc6d1	neutrální
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
skupina	skupina	k1gFnSc1	skupina
esperantistů	esperantista	k1gMnPc2	esperantista
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Hectora	Hector	k1gMnSc2	Hector
Hodlera	Hodler	k1gMnSc2	Hodler
a	a	k8xC	a
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
Romaina	Romain	k1gMnSc2	Romain
Rollanda	Rolland	k1gMnSc2	Rolland
zprostředkovávala	zprostředkovávat	k5eAaImAgFnS	zprostředkovávat
poštovní	poštovní	k2eAgInSc4d1	poštovní
styk	styk	k1gInSc4	styk
mezi	mezi	k7c7	mezi
znepřátelenými	znepřátelený	k2eAgFnPc7d1	znepřátelená
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
přeposílali	přeposílat	k5eAaImAgMnP	přeposílat
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
barikády	barikáda	k1gFnPc4	barikáda
dvě	dva	k4xCgFnPc4	dva
stě	sto	k4xCgFnPc1	sto
až	až	k9	až
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
dopisů	dopis	k1gInPc2	dopis
osobního	osobní	k2eAgInSc2d1	osobní
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
kvůli	kvůli	k7c3	kvůli
cenzuře	cenzura	k1gFnSc3	cenzura
příslušně	příslušně	k6eAd1	příslušně
přeložených	přeložený	k2eAgInPc2d1	přeložený
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
takto	takto	k6eAd1	takto
esperantisté	esperantista	k1gMnPc1	esperantista
pomohli	pomoct	k5eAaPmAgMnP	pomoct
v	v	k7c4	v
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
tisících	tisící	k4xOgInPc2	tisící
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
uděleno	udělen	k2eAgNnSc1d1	uděleno
čestné	čestný	k2eAgNnSc1d1	čestné
uznání	uznání	k1gNnSc1	uznání
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
touha	touha	k1gFnSc1	touha
po	po	k7c4	po
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
harmonii	harmonie	k1gFnSc4	harmonie
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
nové	nový	k2eAgFnPc4d1	nová
naděje	naděje	k1gFnPc4	naděje
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
esperanto	esperanto	k1gNnSc1	esperanto
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
šířilo	šířit	k5eAaImAgNnS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
poválečný	poválečný	k2eAgInSc1d1	poválečný
kongres	kongres	k1gInSc1	kongres
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
světový	světový	k2eAgInSc1d1	světový
kongres	kongres	k1gInSc1	kongres
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
Hofburgu	Hofburg	k1gInSc6	Hofburg
otevřeno	otevřen	k2eAgNnSc1d1	otevřeno
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
esperantské	esperantský	k2eAgNnSc1d1	esperantské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
připojené	připojený	k2eAgInPc1d1	připojený
k	k	k7c3	k
Rakouské	rakouský	k2eAgFnSc3d1	rakouská
národní	národní	k2eAgFnSc3d1	národní
knihovně	knihovna	k1gFnSc3	knihovna
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
budově	budova	k1gFnSc6	budova
<g/>
.	.	kIx.	.
<g/>
Slibný	slibný	k2eAgInSc4d1	slibný
vývoj	vývoj	k1gInSc4	vývoj
však	však	k9	však
přerušila	přerušit	k5eAaPmAgFnS	přerušit
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
:	:	kIx,	:
Mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
bylo	být	k5eAaImAgNnS	být
vyvoláváno	vyvoláván	k2eAgNnSc1d1	vyvoláváno
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
<g/>
,	,	kIx,	,
esperantisté	esperantista	k1gMnPc1	esperantista
byli	být	k5eAaImAgMnP	být
povoláváni	povolávat	k5eAaImNgMnP	povolávat
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
zde	zde	k6eAd1	zde
padli	padnout	k5eAaImAgMnP	padnout
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
židovských	židovský	k2eAgMnPc2d1	židovský
esperantistů	esperantista	k1gMnPc2	esperantista
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
a	a	k8xC	a
gulazích	gulag	k1gInPc6	gulag
(	(	kIx(	(
<g/>
mnohdy	mnohdy	k6eAd1	mnohdy
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
vztahem	vztah	k1gInSc7	vztah
k	k	k7c3	k
esperantu	esperanto	k1gNnSc3	esperanto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
holokaustu	holokaust	k1gInSc6	holokaust
zahynuly	zahynout	k5eAaPmAgFnP	zahynout
i	i	k9	i
všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
děti	dítě	k1gFnPc1	dítě
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Zamenhofa	Zamenhof	k1gMnSc2	Zamenhof
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
Louis-Christophe	Louis-Christophe	k1gFnPc2	Louis-Christophe
Zaleski-Zamenhof	Zaleski-Zamenhof	k1gMnSc1	Zaleski-Zamenhof
unikl	uniknout	k5eAaPmAgMnS	uniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
z	z	k7c2	z
varšavského	varšavský	k2eAgNnSc2d1	Varšavské
ghetta	ghetto	k1gNnSc2	ghetto
<g/>
,	,	kIx,	,
skrýval	skrývat	k5eAaImAgInS	skrývat
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
falešným	falešný	k2eAgNnSc7d1	falešné
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
poté	poté	k6eAd1	poté
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
prosazení	prosazení	k1gNnSc4	prosazení
esperanta	esperanto	k1gNnSc2	esperanto
jako	jako	k8xS	jako
univerzálního	univerzální	k2eAgInSc2d1	univerzální
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
setkávaly	setkávat	k5eAaImAgInP	setkávat
s	s	k7c7	s
pozitivní	pozitivní	k2eAgFnSc7d1	pozitivní
odezvou	odezva	k1gFnSc7	odezva
i	i	k9	i
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
:	:	kIx,	:
Petice	petice	k1gFnSc1	petice
v	v	k7c4	v
jeho	on	k3xPp3gInSc4	on
prospěch	prospěch	k1gInSc4	prospěch
adresované	adresovaný	k2eAgFnSc2d1	adresovaná
Spojeným	spojený	k2eAgInPc3d1	spojený
národům	národ	k1gInPc3	národ
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
přes	přes	k7c4	přes
80	[number]	k4	80
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
například	například	k6eAd1	například
prof.	prof.	kA	prof.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
<g/>
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
UNESCO	UNESCO	kA	UNESCO
přijalo	přijmout	k5eAaPmAgNnS	přijmout
podobné	podobný	k2eAgFnPc4d1	podobná
rezoluce	rezoluce	k1gFnPc4	rezoluce
v	v	k7c6	v
Montevideu	Montevideo	k1gNnSc6	Montevideo
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1954	[number]	k4	1954
a	a	k8xC	a
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Vzalo	vzít	k5eAaPmAgNnS	vzít
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
na	na	k7c6	na
vědomí	vědomí	k1gNnSc6	vědomí
výsledky	výsledek	k1gInPc4	výsledek
dosažené	dosažený	k2eAgFnPc1d1	dosažená
esperantem	esperanto	k1gNnSc7	esperanto
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
duchovní	duchovní	k2eAgFnSc2d1	duchovní
výměny	výměna	k1gFnSc2	výměna
i	i	k9	i
pro	pro	k7c4	pro
sblížení	sblížení	k1gNnSc4	sblížení
národů	národ	k1gInPc2	národ
světa	svět	k1gInSc2	svět
a	a	k8xC	a
vyzvalo	vyzvat	k5eAaPmAgNnS	vyzvat
členské	členský	k2eAgInPc4d1	členský
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
chopily	chopit	k5eAaPmAgInP	chopit
iniciativy	iniciativa	k1gFnSc2	iniciativa
při	při	k7c6	při
zavádění	zavádění	k1gNnSc6	zavádění
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
o	o	k7c6	o
jazykovém	jazykový	k2eAgInSc6d1	jazykový
problému	problém	k1gInSc6	problém
a	a	k8xC	a
esperantu	esperanto	k1gNnSc6	esperanto
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
institucích	instituce	k1gFnPc6	instituce
vyššího	vysoký	k2eAgNnSc2d2	vyšší
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
esperantu	esperanto	k1gNnSc3	esperanto
se	se	k3xPyFc4	se
hlásila	hlásit	k5eAaImAgFnS	hlásit
i	i	k9	i
rada	rada	k1gFnSc1	rada
předsedů	předseda	k1gMnPc2	předseda
Polské	polský	k2eAgFnSc2d1	polská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Jubilejního	jubilejní	k2eAgNnSc2d1	jubilejní
72	[number]	k4	72
<g/>
.	.	kIx.	.
</s>
<s>
Světového	světový	k2eAgInSc2d1	světový
kongresu	kongres	k1gInSc2	kongres
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
(	(	kIx(	(
<g/>
výročí	výročí	k1gNnSc2	výročí
100	[number]	k4	100
let	léto	k1gNnPc2	léto
od	od	k7c2	od
uveřejnění	uveřejnění	k1gNnSc2	uveřejnění
jazyka	jazyk	k1gInSc2	jazyk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
téměř	téměř	k6eAd1	téměř
šest	šest	k4xCc1	šest
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
60	[number]	k4	60
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokroků	pokrok	k1gInPc2	pokrok
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
také	také	k9	také
katoličtí	katolický	k2eAgMnPc1d1	katolický
esperantisté	esperantista	k1gMnPc1	esperantista
–	–	k?	–
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
dokument	dokument	k1gInSc1	dokument
Norme	Norm	k1gInSc5	Norm
per	pero	k1gNnPc2	pero
la	la	k0	la
celebrazione	celebrazion	k1gInSc5	celebrazion
della	della	k6eAd1	della
Messa	Mess	k1gMnSc2	Mess
in	in	k?	in
esperanto	esperanto	k1gNnSc1	esperanto
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
Svatý	svatý	k1gMnSc1	svatý
stolec	stolec	k1gInSc4	stolec
povoluje	povolovat	k5eAaImIp3nS	povolovat
vysluhovat	vysluhovat	k5eAaImF	vysluhovat
mši	mše	k1gFnSc4	mše
svatou	svatý	k2eAgFnSc7d1	svatá
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
řeči	řeč	k1gFnSc6	řeč
bez	bez	k7c2	bez
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
povolení	povolení	k1gNnSc2	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
schváleným	schválený	k2eAgInSc7d1	schválený
umělým	umělý	k2eAgInSc7d1	umělý
liturgickým	liturgický	k2eAgInSc7d1	liturgický
jazykem	jazyk	k1gInSc7	jazyk
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
<g/>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pozdních	pozdní	k2eAgMnPc2d1	pozdní
konkurentů	konkurent	k1gMnPc2	konkurent
esperanta	esperanto	k1gNnSc2	esperanto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
projekt	projekt	k1gInSc1	projekt
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
angličtiny	angličtina	k1gFnSc2	angličtina
C.	C.	kA	C.
K.	K.	kA	K.
Ogdena	Ogden	k1gMnSc2	Ogden
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
našel	najít	k5eAaPmAgMnS	najít
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
podporu	podpora	k1gFnSc4	podpora
u	u	k7c2	u
Winstona	Winston	k1gMnSc2	Winston
Churchilla	Churchill	k1gMnSc2	Churchill
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
a	a	k8xC	a
politické	politický	k2eAgFnPc4d1	politická
výhody	výhoda	k1gFnPc4	výhoda
šíření	šíření	k1gNnSc2	šíření
angličtiny	angličtina	k1gFnSc2	angličtina
pro	pro	k7c4	pro
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
přinese	přinést	k5eAaPmIp3nS	přinést
těmto	tento	k3xDgFnPc3	tento
zemím	zem	k1gFnPc3	zem
"	"	kIx"	"
<g/>
mnohem	mnohem	k6eAd1	mnohem
trvalejší	trvalý	k2eAgInSc4d2	trvalejší
a	a	k8xC	a
větší	veliký	k2eAgInSc4d2	veliký
zisk	zisk	k1gInSc4	zisk
než	než	k8xS	než
zabírání	zabírání	k1gNnSc4	zabírání
velkých	velký	k2eAgNnPc2d1	velké
území	území	k1gNnPc2	území
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ustavení	ustavení	k1gNnSc1	ustavení
angličtiny	angličtina	k1gFnSc2	angličtina
jako	jako	k8xS	jako
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
jazyka	jazyk	k1gInSc2	jazyk
"	"	kIx"	"
<g/>
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
podpory	podpora	k1gFnSc2	podpora
a	a	k8xC	a
udržování	udržování	k1gNnSc4	udržování
západních	západní	k2eAgInPc2d1	západní
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
zájmů	zájem	k1gInPc2	zájem
<g/>
"	"	kIx"	"
dostala	dostat	k5eAaPmAgFnS	dostat
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
státem	stát	k1gInSc7	stát
zřízená	zřízený	k2eAgFnSc1d1	zřízená
Britská	britský	k2eAgFnSc1d1	britská
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
British	British	k1gMnSc1	British
Council	Council	k1gMnSc1	Council
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímž	jejíž	k3xOyRp3gFnPc3	jejíž
snahám	snaha	k1gFnPc3	snaha
se	se	k3xPyFc4	se
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
připojily	připojit	k5eAaPmAgInP	připojit
i	i	k9	i
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohé	mnohé	k1gNnSc1	mnohé
z	z	k7c2	z
cílů	cíl	k1gInPc2	cíl
esperantského	esperantský	k2eAgNnSc2d1	esperantské
hnutí	hnutí	k1gNnSc2	hnutí
dosud	dosud	k6eAd1	dosud
nedošly	dojít	k5eNaPmAgFnP	dojít
naplnění	naplnění	k1gNnSc4	naplnění
a	a	k8xC	a
že	že	k8xS	že
jako	jako	k9	jako
dorozumívací	dorozumívací	k2eAgInSc4d1	dorozumívací
jazyk	jazyk	k1gInSc4	jazyk
většiny	většina	k1gFnSc2	většina
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
aktivit	aktivita	k1gFnPc2	aktivita
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
přisuzována	přisuzovat	k5eAaImNgFnS	přisuzovat
právě	právě	k6eAd1	právě
technologické	technologický	k2eAgFnSc3d1	technologická
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc3d1	kulturní
dominanci	dominance	k1gFnSc3	dominance
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
USA	USA	kA	USA
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
esperanta	esperanto	k1gNnSc2	esperanto
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Historie	historie	k1gFnSc1	historie
esperanta	esperanto	k1gNnSc2	esperanto
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
====	====	k?	====
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
českou	český	k2eAgFnSc4d1	Česká
učebnici	učebnice	k1gFnSc4	učebnice
<g/>
,	,	kIx,	,
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
"	"	kIx"	"
<g/>
Úplná	úplný	k2eAgFnSc1d1	úplná
učebnice	učebnice	k1gFnSc1	učebnice
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
řeči	řeč	k1gFnSc2	řeč
dra	dřít	k5eAaImSgMnS	dřít
<g/>
.	.	kIx.	.
</s>
<s>
Esperanta	esperanto	k1gNnPc1	esperanto
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
řeči	řeč	k1gFnSc2	řeč
Zamenhofem	Zamenhof	k1gMnSc7	Zamenhof
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Vladimír	Vladimír	k1gMnSc1	Vladimír
Lorenc	Lorenc	k1gMnSc1	Lorenc
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
esperantský	esperantský	k2eAgInSc1d1	esperantský
klub	klub	k1gInSc1	klub
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zakladatele	zakladatel	k1gMnPc4	zakladatel
patřili	patřit	k5eAaImAgMnP	patřit
například	například	k6eAd1	například
dělnický	dělnický	k2eAgMnSc1d1	dělnický
poslanec	poslanec	k1gMnSc1	poslanec
Josef	Josef	k1gMnSc1	Josef
Hybeš	Hybeš	k1gMnSc1	Hybeš
a	a	k8xC	a
redaktor	redaktor	k1gMnSc1	redaktor
Dělnických	dělnický	k2eAgInPc2d1	dělnický
listů	list	k1gInPc2	list
Karel	Karel	k1gMnSc1	Karel
Pelant	Pelant	k1gMnSc1	Pelant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
klub	klub	k1gInSc1	klub
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
byl	být	k5eAaImAgMnS	být
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kostka	Kostka	k1gMnSc1	Kostka
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dva	dva	k4xCgInPc1	dva
svazy	svaz	k1gInPc1	svaz
–	–	k?	–
Bohema	Bohem	k1gMnSc4	Bohem
Unio	Unio	k1gMnSc1	Unio
Esperantista	esperantista	k1gMnSc1	esperantista
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
přijímal	přijímat	k5eAaImAgMnS	přijímat
pouze	pouze	k6eAd1	pouze
kluby	klub	k1gInPc4	klub
a	a	k8xC	a
spolky	spolek	k1gInPc4	spolek
<g/>
,	,	kIx,	,
a	a	k8xC	a
Bohema	Bohema	k1gNnSc1	Bohema
Asocio	Asocio	k1gMnSc1	Asocio
Esperantista	esperantista	k1gMnSc1	esperantista
<g/>
,	,	kIx,	,
určený	určený	k2eAgMnSc1d1	určený
pro	pro	k7c4	pro
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
vydávaly	vydávat	k5eAaPmAgFnP	vydávat
vlastní	vlastní	k2eAgInSc4d1	vlastní
časopis	časopis	k1gInSc4	časopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Historie	historie	k1gFnSc1	historie
esperanta	esperanto	k1gNnSc2	esperanto
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
mezi	mezi	k7c7	mezi
válkami	válka	k1gFnPc7	válka
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
konkurenční	konkurenční	k2eAgInPc1d1	konkurenční
svazy	svaz	k1gInPc1	svaz
spojily	spojit	k5eAaPmAgInP	spojit
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Ĉ	Ĉ	k?	Ĉ
Asocio	Asocio	k1gMnSc1	Asocio
Esperantista	esperantista	k1gMnSc1	esperantista
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
bylo	být	k5eAaImAgNnS	být
sdružení	sdružení	k1gNnSc1	sdružení
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Esperanto-Asocio	Esperanto-Asocio	k1gMnSc1	Esperanto-Asocio
en	en	k?	en
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pravidelně	pravidelně	k6eAd1	pravidelně
vycházel	vycházet	k5eAaImAgInS	vycházet
časopis	časopis	k1gInSc1	časopis
La	la	k1gNnSc2	la
Progreso	Progresa	k1gFnSc5	Progresa
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
Ligilo	Ligila	k1gFnSc5	Ligila
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
byl	být	k5eAaImAgInS	být
svaz	svaz	k1gInSc1	svaz
zlikvidován	zlikvidovat	k5eAaPmNgInS	zlikvidovat
gestapem	gestapo	k1gNnSc7	gestapo
a	a	k8xC	a
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Spolek	spolek	k1gInSc1	spolek
tehdy	tehdy	k6eAd1	tehdy
vydával	vydávat	k5eAaImAgInS	vydávat
časopisy	časopis	k1gInPc7	časopis
Esperantista	esperantista	k1gMnSc1	esperantista
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Esperantisto	esperantista	k1gMnSc5	esperantista
Slovaka	Slovak	k1gMnSc4	Slovak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
pořádány	pořádán	k2eAgFnPc1d1	pořádána
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
letní	letní	k2eAgFnPc1d1	letní
školy	škola	k1gFnPc1	škola
esperanta	esperanto	k1gNnSc2	esperanto
v	v	k7c6	v
Doksech	Doksy	k1gInPc6	Doksy
a	a	k8xC	a
semináře	seminář	k1gInPc4	seminář
v	v	k7c6	v
Dudincích	Dudinec	k1gInPc6	Dudinec
<g/>
,	,	kIx,	,
Herborticích	Herbortice	k1gFnPc6	Herbortice
<g/>
,	,	kIx,	,
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
,	,	kIx,	,
Opátce	Opátec	k1gInPc1	Opátec
u	u	k7c2	u
Košic	Košice	k1gInPc2	Košice
<g/>
,	,	kIx,	,
Rožnově	Rožnov	k1gInSc6	Rožnov
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
<g/>
,	,	kIx,	,
v	v	k7c6	v
Popradu	Poprad	k1gInSc6	Poprad
a	a	k8xC	a
Svitu	svit	k1gInSc6	svit
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vedeny	vést	k5eAaImNgFnP	vést
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
tři	tři	k4xCgInPc4	tři
rozhlasové	rozhlasový	k2eAgInPc4d1	rozhlasový
kurzy	kurz	k1gInPc4	kurz
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kurzů	kurz	k1gInPc2	kurz
a	a	k8xC	a
přednášek	přednáška	k1gFnPc2	přednáška
byly	být	k5eAaImAgFnP	být
odtud	odtud	k6eAd1	odtud
vysílány	vysílat	k5eAaImNgInP	vysílat
také	také	k9	také
umělecky	umělecky	k6eAd1	umělecky
hodnotné	hodnotný	k2eAgInPc4d1	hodnotný
činoherní	činoherní	k2eAgInPc4d1	činoherní
<g/>
,	,	kIx,	,
operní	operní	k2eAgInPc4d1	operní
<g/>
,	,	kIx,	,
operetní	operetní	k2eAgInPc4d1	operetní
a	a	k8xC	a
kabaretní	kabaretní	k2eAgInPc4d1	kabaretní
pořady	pořad	k1gInPc4	pořad
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
účinkovali	účinkovat	k5eAaImAgMnP	účinkovat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Karel	Karel	k1gMnSc1	Karel
Höger	Höger	k1gMnSc1	Höger
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kožík	Kožík	k1gMnSc1	Kožík
<g/>
,	,	kIx,	,
Zdeňka	Zdeňka	k1gFnSc1	Zdeňka
Švabíková	Švabíková	k1gFnSc1	Švabíková
či	či	k8xC	či
Vladimír	Vladimír	k1gMnSc1	Vladimír
Leraus	Leraus	k1gMnSc1	Leraus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vysílala	vysílat	k5eAaImAgFnS	vysílat
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
stanice	stanice	k1gFnSc1	stanice
Praha	Praha	k1gFnSc1	Praha
třikrát	třikrát	k6eAd1	třikrát
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
pořady	pořad	k1gInPc1	pořad
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jazyce	jazyk	k1gInSc6	jazyk
nechyběly	chybět	k5eNaImAgFnP	chybět
ani	ani	k8xC	ani
v	v	k7c6	v
relacích	relace	k1gFnPc6	relace
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
a	a	k8xC	a
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Historie	historie	k1gFnSc1	historie
esperanta	esperanto	k1gNnSc2	esperanto
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
====	====	k?	====
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vyhlazovacích	vyhlazovací	k2eAgInPc6d1	vyhlazovací
táborech	tábor	k1gInPc6	tábor
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
za	za	k7c2	za
války	válka	k1gFnSc2	válka
množství	množství	k1gNnSc1	množství
esperantistů	esperantista	k1gMnPc2	esperantista
<g/>
,	,	kIx,	,
především	především	k9	především
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patřil	patřit	k5eAaImAgMnS	patřit
např.	např.	kA	např.
Petr	Petr	k1gMnSc1	Petr
Ginz	Ginz	k1gMnSc1	Ginz
–	–	k?	–
syn	syn	k1gMnSc1	syn
česko-židovského	česko-židovský	k2eAgInSc2d1	česko-židovský
páru	pár	k1gInSc2	pár
esperantistů	esperantista	k1gMnPc2	esperantista
později	pozdě	k6eAd2	pozdě
proslavený	proslavený	k2eAgInSc1d1	proslavený
knihou	kniha	k1gFnSc7	kniha
Deník	deník	k1gInSc4	deník
mého	můj	k3xOp1gMnSc2	můj
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Německy	německy	k6eAd1	německy
mluvící	mluvící	k2eAgMnPc1d1	mluvící
esperantisté	esperantista	k1gMnPc1	esperantista
byli	být	k5eAaImAgMnP	být
zase	zase	k9	zase
odsunuti	odsunut	k2eAgMnPc1d1	odsunut
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následkem	následkem	k7c2	následkem
politických	politický	k2eAgFnPc2d1	politická
změn	změna	k1gFnPc2	změna
byl	být	k5eAaImAgInS	být
opětovný	opětovný	k2eAgInSc1d1	opětovný
rozvoj	rozvoj	k1gInSc1	rozvoj
esperantského	esperantský	k2eAgNnSc2d1	esperantské
hnutí	hnutí	k1gNnSc2	hnutí
přerušen	přerušit	k5eAaPmNgInS	přerušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
esperantskému	esperantský	k2eAgInSc3d1	esperantský
svazu	svaz	k1gInSc3	svaz
i	i	k8xC	i
všem	všecek	k3xTgFnPc3	všecek
esperantským	esperantský	k2eAgFnPc3d1	esperantská
odbočkám	odbočka	k1gFnPc3	odbočka
závodních	závodní	k2eAgInPc2d1	závodní
klubů	klub	k1gInPc2	klub
"	"	kIx"	"
<g/>
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
rozpustily	rozpustit	k5eAaPmAgFnP	rozpustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
soudruh	soudruh	k1gMnSc1	soudruh
Lenin	Lenin	k1gMnSc1	Lenin
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
význam	význam	k1gInSc4	význam
esperanta	esperanto	k1gNnSc2	esperanto
pro	pro	k7c4	pro
dělnickou	dělnický	k2eAgFnSc4d1	Dělnická
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k6eAd1	mnoho
času	čas	k1gInSc2	čas
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
<g/>
...	...	k?	...
statisíce	statisíce	k1gInPc4	statisíce
dělníků	dělník	k1gMnPc2	dělník
<g/>
...	...	k?	...
učí	učit	k5eAaImIp3nS	učit
se	se	k3xPyFc4	se
mateřštině	mateřština	k1gFnSc3	mateřština
průkopníků	průkopník	k1gMnPc2	průkopník
socialismu	socialismus	k1gInSc2	socialismus
–	–	k?	–
ruštině	ruština	k1gFnSc6	ruština
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Esperantské	esperantský	k2eAgInPc1d1	esperantský
časopisy	časopis	k1gInPc1	časopis
musely	muset	k5eAaImAgInP	muset
přestat	přestat	k5eAaPmF	přestat
vycházet	vycházet	k5eAaImF	vycházet
<g/>
,	,	kIx,	,
kluby	klub	k1gInPc1	klub
mohly	moct	k5eAaImAgInP	moct
existovat	existovat	k5eAaImF	existovat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
osvětových	osvětový	k2eAgFnPc2d1	osvětová
besed	beseda	k1gFnPc2	beseda
nebo	nebo	k8xC	nebo
při	při	k7c6	při
Parcích	park	k1gInPc6	park
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
oddechu	oddech	k1gInSc2	oddech
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
činnost	činnost	k1gFnSc4	činnost
řídil	řídit	k5eAaImAgInS	řídit
Celostátní	celostátní	k2eAgInSc1d1	celostátní
poradní	poradní	k2eAgInSc1d1	poradní
esperantský	esperantský	k2eAgInSc1d1	esperantský
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Československý	československý	k2eAgInSc1d1	československý
esperantský	esperantský	k2eAgInSc1d1	esperantský
výbor	výbor	k1gInSc1	výbor
u	u	k7c2	u
Osvětového	osvětový	k2eAgInSc2d1	osvětový
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
bylo	být	k5eAaImAgNnS	být
uspořádáno	uspořádat	k5eAaPmNgNnS	uspořádat
osm	osm	k4xCc1	osm
Celostátních	celostátní	k2eAgInPc2d1	celostátní
československých	československý	k2eAgInPc2d1	československý
esperantských	esperantský	k2eAgInPc2d1	esperantský
sjezdů	sjezd	k1gInPc2	sjezd
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tři	tři	k4xCgInPc4	tři
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1969	[number]	k4	1969
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
založen	založit	k5eAaPmNgInS	založit
Český	český	k2eAgInSc1d1	český
esperantský	esperantský	k2eAgInSc1d1	esperantský
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
ČES	ČES	kA	ČES
–	–	k?	–
Ĉ	Ĉ	k?	Ĉ
Esperanto-Asocio	Esperanto-Asocio	k1gMnSc1	Esperanto-Asocio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
znovuobnovení	znovuobnovení	k1gNnSc6	znovuobnovení
vydělila	vydělit	k5eAaPmAgFnS	vydělit
sekce	sekce	k1gFnSc1	sekce
mládeže	mládež	k1gFnSc2	mládež
jako	jako	k8xC	jako
Česká	český	k2eAgFnSc1d1	Česká
esperantská	esperantský	k2eAgFnSc1d1	esperantská
mládež	mládež	k1gFnSc1	mládež
(	(	kIx(	(
<g/>
Ĉ	Ĉ	k?	Ĉ
–	–	k?	–
Ĉ	Ĉ	k?	Ĉ
Esperanto-Junularo	Esperanto-Junulara	k1gFnSc5	Esperanto-Junulara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
se	se	k3xPyFc4	se
81	[number]	k4	81
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
kongres	kongres	k1gInSc1	kongres
esperanta	esperanto	k1gNnSc2	esperanto
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
a	a	k8xC	a
65	[number]	k4	65
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
kongres	kongres	k1gInSc1	kongres
esperantské	esperantský	k2eAgFnSc2d1	esperantská
mládeže	mládež	k1gFnSc2	mládež
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Esperantská	esperantský	k2eAgFnSc1d1	esperantská
současnost	současnost	k1gFnSc1	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stále	stále	k6eAd1	stále
jasněji	jasně	k6eAd2	jasně
vnímá	vnímat	k5eAaImIp3nS	vnímat
práva	právo	k1gNnPc4	právo
menšin	menšina	k1gFnPc2	menšina
i	i	k8xC	i
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc4d1	kulturní
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
jazyk	jazyk	k1gInSc1	jazyk
esperanto	esperanto	k1gNnSc1	esperanto
opět	opět	k6eAd1	opět
pozornost	pozornost	k1gFnSc4	pozornost
rozhodujících	rozhodující	k2eAgMnPc2d1	rozhodující
činitelů	činitel	k1gMnPc2	činitel
<g/>
.	.	kIx.	.
</s>
<s>
Nevládní	vládní	k2eNgFnPc1d1	nevládní
organizace	organizace	k1gFnPc1	organizace
a	a	k8xC	a
sdružení	sdružení	k1gNnPc1	sdružení
naléhají	naléhat	k5eAaBmIp3nP	naléhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nastolila	nastolit	k5eAaPmAgFnS	nastolit
otázka	otázka	k1gFnSc1	otázka
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
řeči	řeč	k1gFnSc2	řeč
na	na	k7c4	na
pořad	pořad	k1gInSc4	pořad
diskuze	diskuze	k1gFnSc2	diskuze
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1996	[number]	k4	1996
Sympozium	sympozium	k1gNnSc1	sympozium
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Inazo	Inaza	k1gFnSc5	Inaza
Nitobeho	Nitobe	k1gMnSc2	Nitobe
svolalo	svolat	k5eAaPmAgNnS	svolat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
skupinu	skupina	k1gFnSc4	skupina
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
expertů	expert	k1gMnPc2	expert
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prověřovali	prověřovat	k5eAaImAgMnP	prověřovat
tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
postavení	postavení	k1gNnSc4	postavení
esperanta	esperanto	k1gNnSc2	esperanto
a	a	k8xC	a
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
jeho	jeho	k3xOp3gNnSc4	jeho
začlenění	začlenění	k1gNnSc4	začlenění
do	do	k7c2	do
aktuálních	aktuální	k2eAgInPc2d1	aktuální
rozhovorů	rozhovor	k1gInPc2	rozhovor
o	o	k7c6	o
jazykových	jazykový	k2eAgNnPc6d1	jazykové
právech	právo	k1gNnPc6	právo
a	a	k8xC	a
jazykové	jazykový	k2eAgFnSc6d1	jazyková
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
manifest	manifest	k1gInSc1	manifest
(	(	kIx(	(
<g/>
přijatý	přijatý	k2eAgInSc1d1	přijatý
na	na	k7c6	na
Světovém	světový	k2eAgInSc6d1	světový
esperantském	esperantský	k2eAgInSc6d1	esperantský
kongresu	kongres	k1gInSc6	kongres
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
deklarace	deklarace	k1gFnSc1	deklarace
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
cílů	cíl	k1gInPc2	cíl
motivujících	motivující	k2eAgInPc2d1	motivující
esperantské	esperantský	k2eAgNnSc4d1	esperantské
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
demokracii	demokracie	k1gFnSc4	demokracie
a	a	k8xC	a
zachování	zachování	k1gNnSc4	zachování
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
známými	známý	k2eAgFnPc7d1	známá
osobnostmi	osobnost	k1gFnPc7	osobnost
používajícími	používající	k2eAgFnPc7d1	používající
esperanto	esperanto	k1gNnSc4	esperanto
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Reinhard	Reinhard	k1gMnSc1	Reinhard
Selten	Selten	k2eAgMnSc1d1	Selten
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
Judit	Judit	k1gFnSc2	Judit
Polgárová	Polgárový	k2eAgNnPc1d1	Polgárový
a	a	k8xC	a
Tivadar	Tivadar	k1gMnSc1	Tivadar
Soros	Soros	k1gMnSc1	Soros
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
finančníka	finančník	k1gMnSc2	finančník
George	Georg	k1gMnSc2	Georg
Sorose	Sorosa	k1gFnSc6	Sorosa
<g/>
.	.	kIx.	.
</s>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
komunikační	komunikační	k2eAgInSc1d1	komunikační
prostředek	prostředek	k1gInSc1	prostředek
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
"	"	kIx"	"
<g/>
Dialogy	dialog	k1gInPc7	dialog
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
programu	program	k1gInSc2	program
na	na	k7c4	na
posilnění	posilnění	k1gNnSc4	posilnění
výměny	výměna	k1gFnSc2	výměna
zkušeností	zkušenost	k1gFnPc2	zkušenost
mezi	mezi	k7c7	mezi
domorodými	domorodý	k2eAgInPc7d1	domorodý
národy	národ	k1gInPc7	národ
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odmítá	odmítat	k5eAaImIp3nS	odmítat
bývalé	bývalý	k2eAgInPc4d1	bývalý
koloniální	koloniální	k2eAgInPc4d1	koloniální
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvůrce	tvůrce	k1gMnSc1	tvůrce
esperanta	esperanto	k1gNnSc2	esperanto
Ludvík	Ludvík	k1gMnSc1	Ludvík
Lazar	Lazar	k1gMnSc1	Lazar
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
kolektivním	kolektivní	k2eAgNnSc7d1	kolektivní
používáním	používání	k1gNnSc7	používání
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
omezil	omezit	k5eAaPmAgMnS	omezit
svůj	svůj	k3xOyFgInSc4	svůj
prvotní	prvotní	k2eAgInSc4d1	prvotní
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
minimální	minimální	k2eAgFnSc4d1	minimální
gramatiku	gramatika	k1gFnSc4	gramatika
a	a	k8xC	a
malou	malý	k2eAgFnSc4d1	malá
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
však	však	k9	však
esperanto	esperanto	k1gNnSc1	esperanto
plně	plně	k6eAd1	plně
rozvinutým	rozvinutý	k2eAgInSc7d1	rozvinutý
jazykem	jazyk	k1gInSc7	jazyk
s	s	k7c7	s
celosvětovou	celosvětový	k2eAgFnSc7d1	celosvětová
komunitou	komunita	k1gFnSc7	komunita
mluvčích	mluvčí	k1gMnPc2	mluvčí
a	a	k8xC	a
kompletními	kompletní	k2eAgInPc7d1	kompletní
vyjadřovacími	vyjadřovací	k2eAgInPc7d1	vyjadřovací
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
ze	z	k7c2	z
Zamenhofových	Zamenhofův	k2eAgFnPc2d1	Zamenhofova
idejí	idea	k1gFnPc2	idea
předešly	předejít	k5eAaPmAgFnP	předejít
myšlenky	myšlenka	k1gFnPc1	myšlenka
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
lingvistiky	lingvistika	k1gFnSc2	lingvistika
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
bratr	bratr	k1gMnSc1	bratr
René	René	k1gMnSc1	René
byl	být	k5eAaImAgMnS	být
esperantistou	esperantista	k1gMnSc7	esperantista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stav	stav	k1gInSc1	stav
vývoje	vývoj	k1gInSc2	vývoj
jazyka	jazyk	k1gInSc2	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
jazyk	jazyk	k1gInSc1	jazyk
esperanto	esperanto	k1gNnSc4	esperanto
sestával	sestávat	k5eAaImAgInS	sestávat
z	z	k7c2	z
asi	asi	k9	asi
tisíce	tisíc	k4xCgInPc1	tisíc
slovních	slovní	k2eAgInPc2d1	slovní
kořenů	kořen	k1gInPc2	kořen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
vytvořit	vytvořit	k5eAaPmF	vytvořit
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
tisíc	tisíc	k4xCgInPc2	tisíc
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInPc1d1	dnešní
esperantské	esperantský	k2eAgInPc1d1	esperantský
slovníky	slovník	k1gInPc1	slovník
často	často	k6eAd1	často
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
kořenů	kořen	k1gInPc2	kořen
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
lze	lze	k6eAd1	lze
již	již	k6eAd1	již
vytvářet	vytvářet	k5eAaImF	vytvářet
slov	slovo	k1gNnPc2	slovo
statisíce	statisíce	k1gInPc1	statisíce
<g/>
;	;	kIx,	;
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
stále	stále	k6eAd1	stále
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgFnSc1d1	aktuální
tendence	tendence	k1gFnSc1	tendence
sleduje	sledovat	k5eAaImIp3nS	sledovat
a	a	k8xC	a
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
Akademie	akademie	k1gFnSc1	akademie
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
esperanta	esperanto	k1gNnPc1	esperanto
využíváno	využívat	k5eAaImNgNnS	využívat
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
dosažení	dosažení	k1gNnSc2	dosažení
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgInPc2	všecek
představitelných	představitelný	k2eAgInPc2d1	představitelný
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byly	být	k5eAaImAgFnP	být
problematické	problematický	k2eAgInPc1d1	problematický
či	či	k8xC	či
vyvolávající	vyvolávající	k2eAgFnSc4d1	vyvolávající
polemiku	polemika	k1gFnSc4	polemika
<g/>
.	.	kIx.	.
</s>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
uživatelé	uživatel	k1gMnPc1	uživatel
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
jak	jak	k6eAd1	jak
za	za	k7c2	za
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
řeč	řeč	k1gFnSc1	řeč
kosmopolitů	kosmopolit	k1gMnPc2	kosmopolit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tak	tak	k9	tak
za	za	k7c2	za
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Mein	Mein	k1gMnSc1	Mein
Kampf	Kampf	k1gMnSc1	Kampf
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
řeč	řeč	k1gFnSc1	řeč
Židů	Žid	k1gMnPc2	Žid
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Žid	Žid	k1gMnSc1	Žid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Nynější	nynější	k2eAgNnSc1d1	nynější
esperanto	esperanto	k1gNnSc1	esperanto
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
práce	práce	k1gFnSc2	práce
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
filologů	filolog	k1gMnPc2	filolog
<g/>
,	,	kIx,	,
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
tří	tři	k4xCgFnPc2	tři
generací	generace	k1gFnPc2	generace
jeho	jeho	k3xOp3gMnPc2	jeho
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
mnoho	mnoho	k4c1	mnoho
tisíc	tisíc	k4xCgInPc2	tisíc
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
slovních	slovní	k2eAgInPc2d1	slovní
obratů	obrat	k1gInPc2	obrat
<g/>
,	,	kIx,	,
metafor	metafora	k1gFnPc2	metafora
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
také	také	k6eAd1	také
vypracovány	vypracovat	k5eAaPmNgFnP	vypracovat
desítky	desítka	k1gFnPc1	desítka
odborných	odborný	k2eAgInPc2d1	odborný
slovníků	slovník	k1gInPc2	slovník
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
vědní	vědní	k2eAgFnPc4d1	vědní
obory	obora	k1gFnPc4	obora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uživatelé	uživatel	k1gMnPc1	uživatel
jazyka	jazyk	k1gInSc2	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
esperantský	esperantský	k2eAgInSc1d1	esperantský
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
UEA	UEA	kA	UEA
–	–	k?	–
Universala	Universala	k1gMnSc1	Universala
Esperanto-Asocio	Esperanto-Asocio	k1gMnSc1	Esperanto-Asocio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
členové	člen	k1gMnPc1	člen
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejaktivnější	aktivní	k2eAgFnSc4d3	nejaktivnější
část	část	k1gFnSc4	část
esperantské	esperantský	k2eAgFnSc2d1	esperantská
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
své	svůj	k3xOyFgInPc4	svůj
národní	národní	k2eAgInPc4d1	národní
svazy	svaz	k1gInPc4	svaz
v	v	k7c6	v
62	[number]	k4	62
státech	stát	k1gInPc6	stát
a	a	k8xC	a
individuální	individuální	k2eAgInPc1d1	individuální
členy	člen	k1gInPc1	člen
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
dvojnásobném	dvojnásobný	k2eAgInSc6d1	dvojnásobný
počtu	počet	k1gInSc6	počet
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
prodaných	prodaný	k2eAgFnPc2d1	prodaná
učebnic	učebnice	k1gFnPc2	učebnice
a	a	k8xC	a
členské	členský	k2eAgFnSc2d1	členská
statistiky	statistika	k1gFnSc2	statistika
místních	místní	k2eAgFnPc2d1	místní
skupin	skupina	k1gFnPc2	skupina
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
minimální	minimální	k2eAgFnSc7d1	minimální
znalostí	znalost	k1gFnSc7	znalost
jazyka	jazyk	k1gInSc2	jazyk
jsou	být	k5eAaImIp3nP	být
statisíce	statisíce	k1gInPc1	statisíce
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
miliony	milion	k4xCgInPc1	milion
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gFnSc1	mluvčí
esperanta	esperanto	k1gNnSc2	esperanto
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
koncentrací	koncentrace	k1gFnSc7	koncentrace
v	v	k7c6	v
tak	tak	k6eAd1	tak
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
či	či	k8xC	či
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
projektu	projekt	k1gInSc2	projekt
Ethnologue	Ethnologue	k1gFnSc4	Ethnologue
je	být	k5eAaImIp3nS	být
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
esperanta	esperanto	k1gNnSc2	esperanto
odhadován	odhadován	k2eAgInSc1d1	odhadován
na	na	k7c4	na
dva	dva	k4xCgMnPc4	dva
miliony	milion	k4xCgInPc4	milion
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gNnSc3	jeho
používání	používání	k1gNnSc3	používání
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgNnPc6d1	mezinárodní
manželstvích	manželství	k1gNnPc6	manželství
<g/>
,	,	kIx,	,
esperanto	esperanto	k1gNnSc1	esperanto
rodným	rodný	k2eAgInSc7d1	rodný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
a	a	k8xC	a
esperantista	esperantista	k1gMnSc1	esperantista
Ziko	Zika	k1gMnSc5	Zika
Marcus	Marcus	k1gMnSc1	Marcus
Sikosek	Sikoska	k1gFnPc2	Sikoska
tento	tento	k3xDgInSc4	tento
odhad	odhad	k1gInSc4	odhad
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
nadsazený	nadsazený	k2eAgInSc4d1	nadsazený
<g/>
,	,	kIx,	,
vycházeje	vycházet	k5eAaImSgMnS	vycházet
z	z	k7c2	z
porovnání	porovnání	k1gNnSc2	porovnání
s	s	k7c7	s
reálnými	reálný	k2eAgInPc7d1	reálný
počty	počet	k1gInPc7	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
a	a	k8xC	a
počty	počet	k1gInPc1	počet
členů	člen	k1gMnPc2	člen
esperantských	esperantský	k2eAgFnPc2d1	esperantská
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
odhady	odhad	k1gInPc1	odhad
udávají	udávat	k5eAaImIp3nP	udávat
skromnější	skromný	k2eAgNnPc4d2	skromnější
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
mezi	mezi	k7c7	mezi
statisícem	statisíec	k1gInSc7	statisíec
a	a	k8xC	a
miliónem	milión	k4xCgInSc7	milión
<g/>
.	.	kIx.	.
</s>
<s>
Nepřesnosti	nepřesnost	k1gFnPc1	nepřesnost
v	v	k7c6	v
údajích	údaj	k1gInPc6	údaj
plynou	plynout	k5eAaImIp3nP	plynout
především	především	k9	především
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc4	počet
uživatelů	uživatel	k1gMnPc2	uživatel
kteréhokoli	kterýkoli	k3yIgInSc2	kterýkoli
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
pomocného	pomocný	k2eAgInSc2d1	pomocný
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
například	například	k6eAd1	například
sčítáním	sčítání	k1gNnSc7	sčítání
lidu	lid	k1gInSc2	lid
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
pouze	pouze	k6eAd1	pouze
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jejich	jejich	k3xOp3gFnPc4	jejich
další	další	k2eAgFnPc4d1	další
jazykové	jazykový	k2eAgFnPc4d1	jazyková
znalosti	znalost	k1gFnPc4	znalost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyučování	vyučování	k1gNnSc1	vyučování
esperanta	esperanto	k1gNnSc2	esperanto
===	===	k?	===
</s>
</p>
<p>
<s>
Dorozumívat	dorozumívat	k5eAaImF	dorozumívat
se	se	k3xPyFc4	se
esperantem	esperanto	k1gNnSc7	esperanto
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
začít	začít	k5eAaPmF	začít
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
ideální	ideální	k2eAgInSc4d1	ideální
úvod	úvod	k1gInSc4	úvod
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
za	za	k7c4	za
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
studenti	student	k1gMnPc1	student
začít	začít	k5eAaPmF	začít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
řeči	řeč	k1gFnSc6	řeč
dopisovat	dopisovat	k5eAaImF	dopisovat
a	a	k8xC	a
za	za	k7c4	za
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svých	svůj	k3xOyFgFnPc2	svůj
škol	škola	k1gFnPc2	škola
či	či	k8xC	či
soukromě	soukromě	k6eAd1	soukromě
cestovat	cestovat	k5eAaImF	cestovat
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Experimentální	experimentální	k2eAgNnPc1d1	experimentální
a	a	k8xC	a
neformální	formální	k2eNgNnPc1d1	neformální
pozorování	pozorování	k1gNnPc1	pozorování
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
předcházející	předcházející	k2eAgNnSc1d1	předcházející
učení	učení	k1gNnSc1	učení
se	se	k3xPyFc4	se
esperantu	esperanto	k1gNnSc6	esperanto
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
následné	následný	k2eAgNnSc4d1	následné
studium	studium	k1gNnSc4	studium
jiných	jiný	k2eAgInPc2d1	jiný
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
takzvaném	takzvaný	k2eAgInSc6d1	takzvaný
propedeutickém	propedeutický	k2eAgInSc6d1	propedeutický
efektu	efekt	k1gInSc6	efekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
prováděny	provádět	k5eAaImNgInP	provádět
i	i	k9	i
některé	některý	k3yIgInPc4	některý
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
formální	formální	k2eAgInSc4d1	formální
výzkum	výzkum	k1gInSc4	výzkum
tohoto	tento	k3xDgInSc2	tento
fenoménu	fenomén	k1gInSc2	fenomén
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
skupina	skupina	k1gFnSc1	skupina
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
učila	učit	k5eAaImAgFnS	učit
rok	rok	k1gInSc4	rok
esperanto	esperanto	k1gNnSc1	esperanto
a	a	k8xC	a
poté	poté	k6eAd1	poté
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
lepší	dobrý	k2eAgFnSc1d2	lepší
výsledky	výsledek	k1gInPc7	výsledek
než	než	k8xS	než
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
učila	učít	k5eAaPmAgFnS	učít
francouzštinu	francouzština	k1gFnSc4	francouzština
celé	celá	k1gFnSc2	celá
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgInP	být
získány	získán	k2eAgInPc1d1	získán
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
druhým	druhý	k4xOgNnSc7	druhý
jazykem	jazyk	k1gInSc7	jazyk
japonština	japonština	k1gFnSc1	japonština
nebo	nebo	k8xC	nebo
při	při	k7c6	při
zkrácení	zkrácení	k1gNnSc6	zkrácení
doby	doba	k1gFnSc2	doba
experimentu	experiment	k1gInSc2	experiment
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
studie	studie	k1gFnSc1	studie
však	však	k9	však
již	již	k6eAd1	již
neuvádí	uvádět	k5eNaImIp3nS	uvádět
<g/>
,	,	kIx,	,
jakých	jaký	k3yRgInPc2	jaký
výsledků	výsledek	k1gInPc2	výsledek
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
učí	učit	k5eAaImIp3nP	učit
jiný	jiný	k2eAgInSc4d1	jiný
cizí	cizí	k2eAgInSc4d1	cizí
jazyk	jazyk	k1gInSc4	jazyk
než	než	k8xS	než
esperanto	esperanto	k1gNnSc4	esperanto
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
latinu	latina	k1gFnSc4	latina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
esperanto	esperanto	k1gNnSc1	esperanto
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
školách	škola	k1gFnPc6	škola
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
dnes	dnes	k6eAd1	dnes
například	například	k6eAd1	například
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Univerzitě	univerzita	k1gFnSc6	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
lidé	člověk	k1gMnPc1	člověk
učí	učit	k5eAaImIp3nP	učit
jako	jako	k9	jako
samouci	samouk	k1gMnPc1	samouk
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
korespondenčních	korespondenční	k2eAgInPc2d1	korespondenční
či	či	k8xC	či
multimediálních	multimediální	k2eAgInPc2d1	multimediální
kurzů	kurz	k1gInPc2	kurz
(	(	kIx(	(
<g/>
papírovou	papírový	k2eAgFnSc7d1	papírová
i	i	k8xC	i
elektronickou	elektronický	k2eAgFnSc7d1	elektronická
poštou	pošta	k1gFnSc7	pošta
nebo	nebo	k8xC	nebo
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
webů	web	k1gInPc2	web
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
lernu	lerna	k1gFnSc4	lerna
<g/>
!	!	kIx.	!
</s>
<s>
nebo	nebo	k8xC	nebo
Duolingo	Duolingo	k6eAd1	Duolingo
<g/>
)	)	kIx)	)
či	či	k8xC	či
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
esperantských	esperantský	k2eAgInPc6d1	esperantský
klubech	klub	k1gInPc6	klub
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
esperantským	esperantský	k2eAgNnSc7d1	esperantské
setkáním	setkání	k1gNnSc7	setkání
speciálně	speciálně	k6eAd1	speciálně
zaměřeným	zaměřený	k2eAgNnSc7d1	zaměřené
na	na	k7c4	na
začátečníky	začátečník	k1gMnPc4	začátečník
je	být	k5eAaImIp3nS	být
Letní	letní	k2eAgFnSc1d1	letní
škola	škola	k1gFnSc1	škola
esperanta	esperanto	k1gNnSc2	esperanto
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
učebnice	učebnice	k1gFnPc4	učebnice
a	a	k8xC	a
pomůcky	pomůcka	k1gFnPc4	pomůcka
pro	pro	k7c4	pro
samouky	samouk	k1gMnPc4	samouk
ve	v	k7c4	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oficiální	oficiální	k2eAgInSc4d1	oficiální
status	status	k1gInSc4	status
a	a	k8xC	a
uznání	uznání	k1gNnSc4	uznání
===	===	k?	===
</s>
</p>
<p>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
není	být	k5eNaImIp3nS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
žádné	žádný	k3yNgFnSc2	žádný
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
existovaly	existovat	k5eAaImAgFnP	existovat
plány	plán	k1gInPc4	plán
na	na	k7c4	na
ustanovení	ustanovení	k1gNnSc4	ustanovení
Neutrálního	neutrální	k2eAgInSc2d1	neutrální
Moresnetu	Moresnet	k1gInSc2	Moresnet
jako	jako	k8xC	jako
vůbec	vůbec	k9	vůbec
prvního	první	k4xOgInSc2	první
esperantského	esperantský	k2eAgInSc2d1	esperantský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
na	na	k7c6	na
krátce	krátce	k6eAd1	krátce
existujícím	existující	k2eAgInSc6d1	existující
umělém	umělý	k2eAgInSc6d1	umělý
Růžovém	růžový	k2eAgInSc6d1	růžový
ostrově	ostrov	k1gInSc6	ostrov
existoval	existovat	k5eAaImAgInS	existovat
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
mikronárod	mikronároda	k1gFnPc2	mikronároda
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
používal	používat	k5eAaImAgMnS	používat
esperanto	esperanto	k1gNnSc4	esperanto
jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
oficiální	oficiální	k2eAgInSc4d1	oficiální
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
řeč	řeč	k1gFnSc1	řeč
nicméně	nicméně	k8xC	nicméně
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
oficiální	oficiální	k2eAgInSc1d1	oficiální
pracovní	pracovní	k2eAgInSc1d1	pracovní
jazyk	jazyk	k1gInSc1	jazyk
některých	některý	k3yIgFnPc2	některý
neziskových	ziskový	k2eNgFnPc2d1	nezisková
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
esperantského	esperantský	k2eAgNnSc2d1	esperantské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
je	být	k5eAaImIp3nS	být
také	také	k9	také
jediným	jediný	k2eAgInSc7d1	jediný
schváleným	schválený	k2eAgInSc7d1	schválený
umělým	umělý	k2eAgInSc7d1	umělý
liturgickým	liturgický	k2eAgInSc7d1	liturgický
jazykem	jazyk	k1gInSc7	jazyk
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Překlady	překlad	k1gInPc4	překlad
do	do	k7c2	do
a	a	k8xC	a
z	z	k7c2	z
esperanta	esperanto	k1gNnSc2	esperanto
nabízejí	nabízet	k5eAaImIp3nP	nabízet
komerční	komerční	k2eAgFnPc1d1	komerční
překladatelské	překladatelský	k2eAgFnPc1d1	překladatelská
agentury	agentura	k1gFnPc1	agentura
a	a	k8xC	a
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k6eAd1	rovněž
jmenováno	jmenovat	k5eAaImNgNnS	jmenovat
několik	několik	k4yIc1	několik
soudních	soudní	k2eAgMnPc2d1	soudní
tlumočníků	tlumočník	k1gMnPc2	tlumočník
pro	pro	k7c4	pro
esperanto	esperanto	k1gNnSc4	esperanto
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
UNESCO	UNESCO	kA	UNESCO
uznalo	uznat	k5eAaPmAgNnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledky	výsledek	k1gInPc1	výsledek
dosažené	dosažený	k2eAgInPc1d1	dosažený
esperantem	esperanto	k1gNnSc7	esperanto
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
cíli	cíl	k1gInPc7	cíl
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgMnSc4	ten
byly	být	k5eAaImAgInP	být
ustanoveny	ustanoven	k2eAgInPc1d1	ustanoven
oficiální	oficiální	k2eAgInPc1d1	oficiální
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
UNESCO	UNESCO	kA	UNESCO
a	a	k8xC	a
Světovým	světový	k2eAgInSc7d1	světový
esperantským	esperantský	k2eAgInSc7d1	esperantský
svazem	svaz	k1gInSc7	svaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nestátní	státní	k2eNgFnSc7d1	nestátní
členskou	členský	k2eAgFnSc7d1	členská
organizací	organizace	k1gFnSc7	organizace
kategorie	kategorie	k1gFnSc2	kategorie
B.	B.	kA	B.
Spolupráce	spolupráce	k1gFnSc1	spolupráce
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
:	:	kIx,	:
Roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
UNESCO	UNESCO	kA	UNESCO
Amadou-Mahtar	Amadou-Mahtar	k1gMnSc1	Amadou-Mahtar
M	M	kA	M
<g/>
'	'	kIx"	'
<g/>
Bow	Bow	k1gMnSc1	Bow
s	s	k7c7	s
projevem	projev	k1gInSc7	projev
na	na	k7c4	na
62	[number]	k4	62
<g/>
.	.	kIx.	.
</s>
<s>
Světovém	světový	k2eAgInSc6d1	světový
esperantském	esperantský	k2eAgInSc6d1	esperantský
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
islandském	islandský	k2eAgInSc6d1	islandský
Reykjavíku	Reykjavík	k1gInSc6	Reykjavík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vyzvalo	vyzvat	k5eAaPmAgNnS	vyzvat
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
UNESCO	UNESCO	kA	UNESCO
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
jak	jak	k8xC	jak
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
o	o	k7c6	o
jazykovém	jazykový	k2eAgInSc6d1	jazykový
problému	problém	k1gInSc6	problém
a	a	k8xC	a
esperantu	esperanto	k1gNnSc6	esperanto
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
k	k	k7c3	k
prostudování	prostudování	k1gNnSc3	prostudování
možnosti	možnost	k1gFnSc2	možnost
využití	využití	k1gNnSc2	využití
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
esperantský	esperantský	k2eAgInSc1d1	esperantský
svaz	svaz	k1gInSc1	svaz
má	mít	k5eAaImIp3nS	mít
poradní	poradní	k2eAgInPc4d1	poradní
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Organizací	organizace	k1gFnSc7	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
UNICEF	UNICEF	kA	UNICEF
<g/>
,	,	kIx,	,
Radou	rada	k1gFnSc7	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Organizací	organizace	k1gFnSc7	organizace
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
organizací	organizace	k1gFnSc7	organizace
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
(	(	kIx(	(
<g/>
ISO	ISO	kA	ISO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
ekonom	ekonom	k1gMnSc1	ekonom
François	François	k1gFnPc2	François
Grin	Grin	k1gMnSc1	Grin
spočítal	spočítat	k5eAaPmAgMnS	spočítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijetí	přijetí	k1gNnSc1	přijetí
esperanta	esperanto	k1gNnSc2	esperanto
<g />
.	.	kIx.	.
</s>
<s>
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
by	by	kYmCp3nS	by
přineslo	přinést	k5eAaPmAgNnS	přinést
úsporu	úspora	k1gFnSc4	úspora
25	[number]	k4	25
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
by	by	kYmCp3nS	by
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
profitovalo	profitovat	k5eAaBmAgNnS	profitovat
85	[number]	k4	85
%	%	kIx~	%
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nynější	nynější	k2eAgFnSc1d1	nynější
dominance	dominance	k1gFnSc1	dominance
angličtiny	angličtina	k1gFnSc2	angličtina
znamená	znamenat	k5eAaImIp3nS	znamenat
zisk	zisk	k1gInSc1	zisk
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
ročně	ročně	k6eAd1	ročně
pro	pro	k7c4	pro
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
výhody	výhoda	k1gFnPc4	výhoda
plynoucí	plynoucí	k2eAgFnPc4d1	plynoucí
z	z	k7c2	z
nadřazeného	nadřazený	k2eAgNnSc2d1	nadřazené
postavení	postavení	k1gNnSc2	postavení
rodilých	rodilý	k2eAgFnPc2d1	rodilá
mluvčí	mluvčí	k1gFnPc4	mluvčí
v	v	k7c6	v
jednáních	jednání	k1gNnPc6	jednání
a	a	k8xC	a
sporech	spor	k1gInPc6	spor
vedených	vedený	k2eAgFnPc2d1	vedená
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
<g/>
Evropská	evropský	k2eAgFnSc1d1	Evropská
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
Evropa	Evropa	k1gFnSc1	Evropa
–	–	k?	–
Demokracie	demokracie	k1gFnSc1	demokracie
–	–	k?	–
Esperanto	esperanto	k1gNnSc1	esperanto
získala	získat	k5eAaPmAgFnS	získat
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
25	[number]	k4	25
259	[number]	k4	259
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
28	[number]	k4	28
944	[number]	k4	944
hlasů	hlas	k1gInPc2	hlas
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
11	[number]	k4	11
722	[number]	k4	722
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
Evropská	evropský	k2eAgFnSc1d1	Evropská
esperantská	esperantský	k2eAgFnSc1d1	esperantská
unie	unie	k1gFnSc1	unie
předložila	předložit	k5eAaPmAgFnS	předložit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
Evropské	evropský	k2eAgFnSc3d1	Evropská
komisi	komise	k1gFnSc3	komise
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
evropských	evropský	k2eAgFnPc2d1	Evropská
občanských	občanský	k2eAgFnPc2d1	občanská
iniciativ	iniciativa	k1gFnPc2	iniciativa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
doplnit	doplnit	k5eAaPmF	doplnit
evropskou	evropský	k2eAgFnSc4d1	Evropská
hymnu	hymna	k1gFnSc4	hymna
slovy	slovo	k1gNnPc7	slovo
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
tak	tak	k9	tak
občanům	občan	k1gMnPc3	občan
zpívat	zpívat	k5eAaImF	zpívat
ji	on	k3xPp3gFnSc4	on
společně	společně	k6eAd1	společně
a	a	k8xC	a
posílit	posílit	k5eAaPmF	posílit
tím	ten	k3xDgNnSc7	ten
evropskou	evropský	k2eAgFnSc4d1	Evropská
identitu	identita	k1gFnSc4	identita
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
identit	identita	k1gFnPc2	identita
národních	národní	k2eAgFnPc2d1	národní
<g/>
.	.	kIx.	.
<g/>
Usnesením	usnesení	k1gNnSc7	usnesení
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
nese	nést	k5eAaImIp3nS	nést
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
německé	německý	k2eAgNnSc4d1	německé
město	město	k1gNnSc4	město
Herzberg	Herzberg	k1gMnSc1	Herzberg
am	am	k?	am
Harz	Harz	k1gMnSc1	Harz
oficiální	oficiální	k2eAgNnSc4d1	oficiální
označení	označení	k1gNnSc4	označení
esperantské	esperantský	k2eAgNnSc1d1	esperantské
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Herzberg	Herzberg	k1gInSc1	Herzberg
am	am	k?	am
Harz	Harz	k1gInSc1	Harz
–	–	k?	–
die	die	k?	die
Esperanto-Stadt	Esperanto-Stadt	k1gInSc1	Esperanto-Stadt
<g/>
/	/	kIx~	/
<g/>
La	la	k0	la
Esperanto-urbo	Esperantorba	k1gMnSc5	Esperanto-urba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
pořádání	pořádání	k1gNnSc1	pořádání
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
aktivit	aktivita	k1gFnPc2	aktivita
souvisejících	související	k2eAgFnPc2d1	související
s	s	k7c7	s
esperantem	esperanto	k1gNnSc7	esperanto
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
partnerství	partnerství	k1gNnSc1	partnerství
s	s	k7c7	s
polským	polský	k2eAgNnSc7d1	polské
městem	město	k1gNnSc7	město
Góra	Gór	k1gInSc2	Gór
realizované	realizovaný	k2eAgNnSc1d1	realizované
na	na	k7c6	na
základě	základ	k1gInSc6	základ
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Využití	využití	k1gNnSc1	využití
esperanta	esperanto	k1gNnSc2	esperanto
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Schůze	schůze	k1gFnSc1	schůze
a	a	k8xC	a
cestování	cestování	k1gNnSc1	cestování
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
konají	konat	k5eAaImIp3nP	konat
stovky	stovka	k1gFnPc1	stovka
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
konferencí	konference	k1gFnPc2	konference
a	a	k8xC	a
setkání	setkání	k1gNnSc2	setkání
–	–	k?	–
bez	bez	k7c2	bez
tlumočníků	tlumočník	k1gMnPc2	tlumočník
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
jsou	být	k5eAaImIp3nP	být
Světové	světový	k2eAgInPc4d1	světový
esperantské	esperantský	k2eAgInPc4d1	esperantský
kongresy	kongres	k1gInPc4	kongres
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
pořádané	pořádaný	k2eAgFnPc1d1	pořádaná
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Reykjavík	Reykjavík	k1gInSc4	Reykjavík
(	(	kIx(	(
<g/>
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lille	Lille	k1gFnSc1	Lille
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nitra	Nitra	k1gFnSc1	Nitra
(	(	kIx(	(
<g/>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
a	a	k8xC	a
Soul	Soul	k1gInSc1	Soul
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
konat	konat	k5eAaImF	konat
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Lisabon	Lisabon	k1gInSc4	Lisabon
(	(	kIx(	(
<g/>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lahti	Lahti	k1gNnSc1	Lahti
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
a	a	k8xC	a
Montréal	Montréal	k1gInSc1	Montréal
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
2020	[number]	k4	2020
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1921	[number]	k4	1921
a	a	k8xC	a
1996	[number]	k4	1996
hostila	hostit	k5eAaImAgFnS	hostit
Světový	světový	k2eAgInSc4d1	světový
kongres	kongres	k1gInSc4	kongres
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
také	také	k9	také
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
kongres	kongres	k1gInSc1	kongres
esperantské	esperantský	k2eAgFnSc2d1	esperantská
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
ve	v	k7c6	v
španělském	španělský	k2eAgNnSc6d1	španělské
městě	město	k1gNnSc6	město
Badajoz	Badajoza	k1gFnPc2	Badajoza
<g/>
;	;	kIx,	;
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
takový	takový	k3xDgInSc1	takový
kongres	kongres	k1gInSc4	kongres
pořádala	pořádat	k5eAaImAgFnS	pořádat
Česká	český	k2eAgFnSc1d1	Česká
esperantská	esperantský	k2eAgFnSc1d1	esperantská
mládež	mládež	k1gFnSc1	mládež
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
343	[number]	k4	343
osob	osoba	k1gFnPc2	osoba
ze	z	k7c2	z
41	[number]	k4	41
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Šesté	šestý	k4xOgNnSc1	šestý
setkání	setkání	k1gNnSc1	setkání
esperantistů	esperantista	k1gMnPc2	esperantista
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
Jerevanu	Jerevan	k1gMnSc6	Jerevan
<g/>
,	,	kIx,	,
osmý	osmý	k4xOgInSc4	osmý
Celoamerický	celoamerický	k2eAgInSc4d1	celoamerický
kongres	kongres	k1gInSc4	kongres
byl	být	k5eAaImAgInS	být
uspořádán	uspořádán	k2eAgInSc1d1	uspořádán
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
v	v	k7c6	v
Sã	Sã	k1gFnSc6	Sã
Paulu	Paula	k1gFnSc4	Paula
a	a	k8xC	a
sedmý	sedmý	k4xOgInSc1	sedmý
Asijský	asijský	k2eAgInSc1d1	asijský
kongres	kongres	k1gInSc1	kongres
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
<g/>
Pasporta	Pasporta	k1gFnSc1	Pasporta
Servo	Servo	k1gNnSc4	Servo
je	být	k5eAaImIp3nS	být
služba	služba	k1gFnSc1	služba
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
využití	využití	k1gNnSc1	využití
esperanta	esperanto	k1gNnSc2	esperanto
v	v	k7c6	v
cestování	cestování	k1gNnSc6	cestování
<g/>
.	.	kIx.	.
</s>
<s>
Zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
ji	on	k3xPp3gFnSc4	on
sekce	sekce	k1gFnSc1	sekce
mládeže	mládež	k1gFnSc2	mládež
Světového	světový	k2eAgInSc2d1	světový
esperantského	esperantský	k2eAgInSc2d1	esperantský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
vydávaná	vydávaný	k2eAgFnSc1d1	vydávaná
publikace	publikace	k1gFnSc1	publikace
obsahující	obsahující	k2eAgFnSc2d1	obsahující
adresy	adresa	k1gFnSc2	adresa
1087	[number]	k4	1087
hostitelů	hostitel	k1gMnPc2	hostitel
v	v	k7c6	v
672	[number]	k4	672
městech	město	k1gNnPc6	město
90	[number]	k4	90
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
vydání	vydání	k1gNnSc1	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poskytujících	poskytující	k2eAgFnPc2d1	poskytující
bezplatně	bezplatně	k6eAd1	bezplatně
ubytování	ubytování	k1gNnSc4	ubytování
cestujícím	cestující	k2eAgNnSc7d1	cestující
hovořícím	hovořící	k2eAgNnSc7d1	hovořící
esperantem	esperanto	k1gNnSc7	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
je	být	k5eAaImIp3nS	být
služba	služba	k1gFnSc1	služba
zabezpečována	zabezpečován	k2eAgFnSc1d1	zabezpečována
primárně	primárně	k6eAd1	primárně
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
,	,	kIx,	,
tištěný	tištěný	k2eAgInSc1d1	tištěný
seznam	seznam	k1gInSc1	seznam
hositelů	hositel	k1gMnPc2	hositel
však	však	k9	však
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dostupný	dostupný	k2eAgInSc1d1	dostupný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Výzkum	výzkum	k1gInSc1	výzkum
a	a	k8xC	a
knihovny	knihovna	k1gFnPc1	knihovna
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
univerzitách	univerzita	k1gFnPc6	univerzita
je	být	k5eAaImIp3nS	být
esperanto	esperanto	k1gNnSc1	esperanto
součástí	součást	k1gFnPc2	součást
lingvistických	lingvistický	k2eAgInPc2d1	lingvistický
kurzů	kurz	k1gInPc2	kurz
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
ho	on	k3xPp3gInSc4	on
nabízejí	nabízet	k5eAaImIp3nP	nabízet
jako	jako	k9	jako
nezávislý	závislý	k2eNgInSc1d1	nezávislý
studijní	studijní	k2eAgInSc1d1	studijní
obor	obor	k1gInSc1	obor
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
pozoruhodné	pozoruhodný	k2eAgFnPc1d1	pozoruhodná
jsou	být	k5eAaImIp3nP	být
Univerzita	univerzita	k1gFnSc1	univerzita
Eötvöse	Eötvöse	k1gFnSc1	Eötvöse
Loránda	Loránda	k1gFnSc1	Loránda
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
s	s	k7c7	s
oddělením	oddělení	k1gNnSc7	oddělení
esperantologie	esperantologie	k1gFnSc2	esperantologie
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
a	a	k8xC	a
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
jazykovědy	jazykověda	k1gFnSc2	jazykověda
a	a	k8xC	a
Univerzita	univerzita	k1gFnSc1	univerzita
Adama	Adam	k1gMnSc2	Adam
Mickiewicze	Mickiewicze	k1gFnSc2	Mickiewicze
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
Poznani	Poznaň	k1gFnSc6	Poznaň
s	s	k7c7	s
diplomovým	diplomový	k2eAgInSc7d1	diplomový
programem	program	k1gInSc7	program
z	z	k7c2	z
interlingvistiky	interlingvistika	k1gFnSc2	interlingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Bibliografie	bibliografie	k1gFnSc1	bibliografie
amerického	americký	k2eAgNnSc2d1	americké
Sdružení	sdružení	k1gNnSc2	sdružení
moderních	moderní	k2eAgInPc2d1	moderní
jazyků	jazyk	k1gInPc2	jazyk
registruje	registrovat	k5eAaBmIp3nS	registrovat
každoročně	každoročně	k6eAd1	každoročně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
odborných	odborný	k2eAgFnPc2d1	odborná
publikací	publikace	k1gFnPc2	publikace
o	o	k7c6	o
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
Esperantského	esperantský	k2eAgInSc2d1	esperantský
svazu	svaz	k1gInSc2	svaz
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
velké	velký	k2eAgFnPc4d1	velká
knihovny	knihovna	k1gFnPc4	knihovna
patří	patřit	k5eAaImIp3nS	patřit
Hodlerova	Hodlerův	k2eAgFnSc1d1	Hodlerův
knihovna	knihovna	k1gFnSc1	knihovna
v	v	k7c6	v
rotterdamském	rotterdamský	k2eAgNnSc6d1	rotterdamské
ústředí	ústředí	k1gNnSc6	ústředí
Světového	světový	k2eAgInSc2d1	světový
esperantského	esperantský	k2eAgInSc2d1	esperantský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
Esperantská	esperantský	k2eAgFnSc1d1	esperantská
knihovna	knihovna	k1gFnSc1	knihovna
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Aalenu	Aalen	k1gInSc6	Aalen
nebo	nebo	k8xC	nebo
knihovna	knihovna	k1gFnSc1	knihovna
Mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
esperantského	esperantský	k2eAgNnSc2d1	esperantské
muzea	muzeum	k1gNnSc2	muzeum
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Knihovny	knihovna	k1gFnPc1	knihovna
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
Aalenu	Aaleno	k1gNnSc6	Aaleno
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
sbírkách	sbírka	k1gFnPc6	sbírka
také	také	k6eAd1	také
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
internetu	internet	k1gInSc2	internet
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
přístupné	přístupný	k2eAgInPc1d1	přístupný
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
meziknihovní	meziknihovní	k2eAgFnSc2d1	meziknihovní
výpůjční	výpůjční	k2eAgFnSc2d1	výpůjční
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
Muzeum	muzeum	k1gNnSc1	muzeum
esperanta	esperanto	k1gNnSc2	esperanto
ve	v	k7c6	v
Svitavách	Svitava	k1gFnPc6	Svitava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Profesní	profesní	k2eAgInPc4d1	profesní
kontakty	kontakt	k1gInPc4	kontakt
a	a	k8xC	a
odborné	odborný	k2eAgInPc4d1	odborný
zájmy	zájem	k1gInPc4	zájem
====	====	k?	====
</s>
</p>
<p>
<s>
Esperantisté	esperantista	k1gMnPc1	esperantista
jsou	být	k5eAaImIp3nP	být
sdruženi	sdružit	k5eAaPmNgMnP	sdružit
i	i	k9	i
v	v	k7c6	v
rozmanitých	rozmanitý	k2eAgInPc6d1	rozmanitý
odborných	odborný	k2eAgInPc6d1	odborný
svazech	svaz	k1gInPc6	svaz
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
,	,	kIx,	,
železničáři	železničář	k1gMnPc1	železničář
<g/>
,	,	kIx,	,
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
hudebníci	hudebník	k1gMnPc1	hudebník
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
jiní	jiný	k1gMnPc1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
sdružení	sdružení	k1gNnPc1	sdružení
často	často	k6eAd1	často
vydávají	vydávat	k5eAaPmIp3nP	vydávat
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
časopisy	časopis	k1gInPc4	časopis
<g/>
,	,	kIx,	,
organizují	organizovat	k5eAaBmIp3nP	organizovat
konference	konference	k1gFnPc1	konference
a	a	k8xC	a
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
profesionální	profesionální	k2eAgNnSc4d1	profesionální
a	a	k8xC	a
speciální	speciální	k2eAgNnSc4d1	speciální
využívání	využívání	k1gNnSc4	využívání
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
San	San	k1gFnSc6	San
Marinu	Marina	k1gFnSc4	Marina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zařadila	zařadit	k5eAaPmAgFnS	zařadit
esperanto	esperanto	k1gNnSc4	esperanto
mezi	mezi	k7c7	mezi
svých	svůj	k3xOyFgInPc2	svůj
pět	pět	k4xCc4	pět
pracovních	pracovní	k2eAgInPc2d1	pracovní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
spolupráci	spolupráce	k1gFnSc4	spolupráce
na	na	k7c6	na
univerzitní	univerzitní	k2eAgFnSc6d1	univerzitní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
konference	konference	k1gFnPc1	konference
o	o	k7c4	o
využití	využití	k1gNnSc4	využití
esperanta	esperanto	k1gNnSc2	esperanto
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
a	a	k8xC	a
technice	technika	k1gFnSc6	technika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
KAEST	KAEST	kA	KAEST
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
a	a	k8xC	a
přeložené	přeložený	k2eAgFnPc1d1	přeložená
publikace	publikace	k1gFnPc1	publikace
pravidelně	pravidelně	k6eAd1	pravidelně
vycházejí	vycházet	k5eAaImIp3nP	vycházet
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
informatika	informatika	k1gFnSc1	informatika
<g/>
,	,	kIx,	,
botanika	botanika	k1gFnSc1	botanika
<g/>
,	,	kIx,	,
entomologie	entomologie	k1gFnSc1	entomologie
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc1	právo
a	a	k8xC	a
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Esperantská	esperantský	k2eAgFnSc1d1	esperantská
podoba	podoba	k1gFnSc1	podoba
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gFnPc2	její
prvních	první	k4xOgFnPc2	první
jazykových	jazykový	k2eAgFnPc2d1	jazyková
verzí	verze	k1gFnPc2	verze
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
verzi	verze	k1gFnSc3	verze
a	a	k8xC	a
českému	český	k2eAgMnSc3d1	český
esperantistovi	esperantista	k1gMnSc3	esperantista
Miroslavu	Miroslav	k1gMnSc3	Miroslav
Malovcovi	Malovec	k1gMnSc3	Malovec
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
česká	český	k2eAgFnSc1d1	Česká
mutace	mutace	k1gFnSc1	mutace
této	tento	k3xDgFnSc2	tento
internetové	internetový	k2eAgFnSc2d1	internetová
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zájmové	zájmový	k2eAgFnPc1d1	zájmová
organizace	organizace	k1gFnPc1	organizace
esperantistů	esperantista	k1gMnPc2	esperantista
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
skauti	skaut	k1gMnPc1	skaut
<g/>
,	,	kIx,	,
zrakově	zrakově	k6eAd1	zrakově
postižení	postižený	k1gMnPc1	postižený
<g/>
,	,	kIx,	,
šachisté	šachista	k1gMnPc1	šachista
<g/>
,	,	kIx,	,
hráči	hráč	k1gMnPc1	hráč
Go	Go	k1gFnSc2	Go
(	(	kIx(	(
<g/>
japonská	japonský	k2eAgFnSc1d1	japonská
národní	národní	k2eAgFnSc1d1	národní
hra	hra	k1gFnSc1	hra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mládežnická	mládežnický	k2eAgFnSc1d1	mládežnická
sekce	sekce	k1gFnSc1	sekce
Světového	světový	k2eAgInSc2d1	světový
esperantského	esperantský	k2eAgInSc2d1	esperantský
svazu	svaz	k1gInSc2	svaz
–	–	k?	–
Světová	světový	k2eAgFnSc1d1	světová
esperantská	esperantský	k2eAgFnSc1d1	esperantská
mládež	mládež	k1gFnSc1	mládež
(	(	kIx(	(
<g/>
TEJO	TEJO	kA	TEJO
<g/>
)	)	kIx)	)
organizuje	organizovat	k5eAaBmIp3nS	organizovat
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
setkání	setkání	k1gNnSc4	setkání
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
svá	svůj	k3xOyFgNnPc4	svůj
vlastní	vlastní	k2eAgNnPc4d1	vlastní
periodika	periodikum	k1gNnPc4	periodikum
<g/>
.	.	kIx.	.
</s>
<s>
Buddhisté	buddhista	k1gMnPc1	buddhista
<g/>
,	,	kIx,	,
šintoisté	šintoista	k1gMnPc1	šintoista
<g/>
,	,	kIx,	,
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
kvakeři	kvaker	k1gMnPc1	kvaker
<g/>
,	,	kIx,	,
protestanti	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
mormoni	mormon	k1gMnPc1	mormon
a	a	k8xC	a
bahaisté	bahaista	k1gMnPc1	bahaista
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgFnPc4d1	vlastní
esperantské	esperantský	k2eAgFnPc4d1	esperantská
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
další	další	k2eAgFnPc1d1	další
společenské	společenský	k2eAgFnPc1d1	společenská
organizace	organizace	k1gFnPc1	organizace
používají	používat	k5eAaImIp3nP	používat
esperanto	esperanto	k1gNnSc4	esperanto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Literatura	literatura	k1gFnSc1	literatura
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
existuje	existovat	k5eAaImIp3nS	existovat
jak	jak	k6eAd1	jak
původní	původní	k2eAgFnSc1d1	původní
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
přeložená	přeložený	k2eAgFnSc1d1	přeložená
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
čítající	čítající	k2eAgFnSc1d1	čítající
asi	asi	k9	asi
50	[number]	k4	50
000	[number]	k4	000
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Bohatou	bohatý	k2eAgFnSc4d1	bohatá
literární	literární	k2eAgFnSc4d1	literární
tradici	tradice	k1gFnSc4	tradice
esperanta	esperanto	k1gNnSc2	esperanto
uznává	uznávat	k5eAaImIp3nS	uznávat
i	i	k9	i
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
PEN	PEN	kA	PEN
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
esperantská	esperantský	k2eAgFnSc1d1	esperantská
sekce	sekce	k1gFnSc1	sekce
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
během	během	k7c2	během
60	[number]	k4	60
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
září	září	k1gNnSc6	září
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
význačné	význačný	k2eAgMnPc4d1	význačný
současné	současný	k2eAgMnPc4d1	současný
spisovatele	spisovatel	k1gMnPc4	spisovatel
píšící	píšící	k2eAgMnPc4d1	píšící
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Trevor	Trevor	k1gInSc1	Trevor
Steele	Steel	k1gInSc2	Steel
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
István	István	k2eAgInSc1d1	István
Nemere	Nemer	k1gInSc5	Nemer
(	(	kIx(	(
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spomenka	Spomenka	k1gFnSc1	Spomenka
Štimec	Štimec	k1gMnSc1	Štimec
(	(	kIx(	(
<g/>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
básníci	básník	k1gMnPc1	básník
Mauro	Maura	k1gFnSc5	Maura
Nervi	rvát	k5eNaImRp2nS	rvát
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mao	Mao	k1gFnSc1	Mao
Zifu	Zifus	k1gInSc2	Zifus
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
a	a	k8xC	a
Abel	Abel	k1gMnSc1	Abel
Montagut	Montagut	k1gMnSc1	Montagut
(	(	kIx(	(
<g/>
Katalánsko	Katalánsko	k1gNnSc1	Katalánsko
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
esejisté	esejista	k1gMnPc1	esejista
a	a	k8xC	a
překladatelé	překladatel	k1gMnPc1	překladatel
Probal	Probal	k1gInSc1	Probal
Dašgupta	Dašgupta	k1gFnSc1	Dašgupta
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Humphrey	Humphrey	k1gInPc1	Humphrey
Tonkin	Tonkin	k1gInSc1	Tonkin
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kurisu	Kuris	k1gInSc2	Kuris
Kei	Kei	k1gFnSc2	Kei
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skotský	skotský	k2eAgMnSc1d1	skotský
básník	básník	k1gMnSc1	básník
William	William	k1gInSc4	William
Auld	Auld	k1gInSc1	Auld
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
poetickou	poetický	k2eAgFnSc4d1	poetická
tvorbu	tvorba	k1gFnSc4	tvorba
několikrát	několikrát	k6eAd1	několikrát
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
a	a	k8xC	a
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
navržen	navrhnout	k5eAaPmNgInS	navrhnout
na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Esperantský	esperantský	k2eAgInSc4d1	esperantský
překlad	překlad	k1gInSc4	překlad
Starého	Starého	k2eAgInSc2d1	Starého
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
vyšel	vyjít	k5eAaPmAgMnS	vyjít
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
ekumenická	ekumenický	k2eAgFnSc1d1	ekumenická
verze	verze	k1gFnSc1	verze
včetně	včetně	k7c2	včetně
deuterokanonických	deuterokanonický	k2eAgFnPc2d1	deuterokanonická
knih	kniha	k1gFnPc2	kniha
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známá	známý	k2eAgNnPc4d1	známé
literární	literární	k2eAgNnPc4d1	literární
díla	dílo	k1gNnPc4	dílo
přeložená	přeložený	k2eAgNnPc4d1	přeložené
do	do	k7c2	do
esperanta	esperanto	k1gNnSc2	esperanto
a	a	k8xC	a
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
například	například	k6eAd1	například
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
od	od	k7c2	od
Hemingwaye	Hemingway	k1gFnSc2	Hemingway
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
od	od	k7c2	od
Tolkiena	Tolkieno	k1gNnSc2	Tolkieno
<g/>
,	,	kIx,	,
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
od	od	k7c2	od
García	Garcíum	k1gNnSc2	Garcíum
Márqueze	Márqueze	k1gFnSc2	Márqueze
<g/>
,	,	kIx,	,
Čtyřverší	čtyřverší	k1gNnSc4	čtyřverší
Omara	Omar	k1gMnSc2	Omar
Chajjáma	Chajjám	k1gMnSc2	Chajjám
<g/>
,	,	kIx,	,
Plechový	plechový	k2eAgInSc1d1	plechový
bubínek	bubínek	k1gInSc1	bubínek
Güntera	Günter	k1gMnSc2	Günter
Grasse	Grass	k1gMnSc2	Grass
<g/>
,	,	kIx,	,
Milion	milion	k4xCgInSc1	milion
Marca	Marcum	k1gNnPc1	Marcum
Pola	pola	k1gFnSc1	pola
<g/>
,	,	kIx,	,
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Haška	Hašek	k1gMnSc4	Hašek
či	či	k8xC	či
velká	velký	k2eAgFnSc1d1	velká
rodinná	rodinný	k2eAgFnSc1d1	rodinná
sága	sága	k1gFnSc1	sága
Cchao	Cchao	k1gMnSc1	Cchao
Süe-čchina	Süe-čchina	k1gMnSc1	Süe-čchina
Sen	sen	k1gInSc1	sen
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
Asterixe	Asterixe	k1gFnSc2	Asterixe
<g/>
,	,	kIx,	,
Medvídka	medvídek	k1gMnSc2	medvídek
Pú	Pú	k1gMnSc2	Pú
a	a	k8xC	a
Tintina	Tintina	k1gFnSc1	Tintina
přeložena	přeložit	k5eAaPmNgFnS	přeložit
například	například	k6eAd1	například
i	i	k8xC	i
Pipi	Pipi	k1gNnSc6	Pipi
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Punčocha	punčocha	k1gFnSc1	punčocha
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
knihy	kniha	k1gFnPc1	kniha
Muminků	Mumink	k1gInPc2	Mumink
finské	finský	k2eAgFnSc2d1	finská
autorky	autorka	k1gFnSc2	autorka
Tove	Tov	k1gFnSc2	Tov
Janssonové	Janssonová	k1gFnSc2	Janssonová
<g/>
;	;	kIx,	;
texty	text	k1gInPc7	text
některých	některý	k3yIgInPc2	některý
překladů	překlad	k1gInPc2	překlad
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
z	z	k7c2	z
esperanta	esperanto	k1gNnSc2	esperanto
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
slovenštiny	slovenština	k1gFnSc2	slovenština
přeložena	přeložen	k2eAgFnSc1d1	přeložena
Maškaráda	maškaráda	k1gFnSc1	maškaráda
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
Tivadara	Tivadara	k1gFnSc1	Tivadara
Sorose	Sorosa	k1gFnSc3	Sorosa
vydaná	vydaný	k2eAgFnSc1d1	vydaná
esperantem	esperanto	k1gNnSc7	esperanto
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
:	:	kIx,	:
autor	autor	k1gMnSc1	autor
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
popisuje	popisovat	k5eAaImIp3nS	popisovat
život	život	k1gInSc1	život
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
během	během	k7c2	během
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hudba	hudba	k1gFnSc1	hudba
====	====	k?	====
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgInPc4d1	hudební
žánry	žánr	k1gInPc4	žánr
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
lidové	lidový	k2eAgFnPc4d1	lidová
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
rockovou	rockový	k2eAgFnSc4d1	rocková
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
kabarety	kabaret	k1gInPc4	kabaret
<g/>
,	,	kIx,	,
písně	píseň	k1gFnPc4	píseň
pro	pro	k7c4	pro
sólisty	sólista	k1gMnPc4	sólista
a	a	k8xC	a
pro	pro	k7c4	pro
sbory	sbor	k1gInPc4	sbor
i	i	k8xC	i
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
aktivní	aktivní	k2eAgMnPc4d1	aktivní
esperantské	esperantský	k2eAgMnPc4d1	esperantský
hudebníky	hudebník	k1gMnPc4	hudebník
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
např.	např.	kA	např.
švédská	švédský	k2eAgFnSc1d1	švédská
společenskokritická	společenskokritický	k2eAgFnSc1d1	společenskokritická
skupina	skupina	k1gFnSc1	skupina
La	la	k1gNnSc2	la
Perdita	Perdita	k1gFnSc1	Perdita
Generacio	Generacio	k1gMnSc1	Generacio
<g/>
,	,	kIx,	,
okcitánský	okcitánský	k2eAgMnSc1d1	okcitánský
zpěvák	zpěvák	k1gMnSc1	zpěvák
JoMo	JoMo	k1gMnSc1	JoMo
<g/>
,	,	kIx,	,
finská	finský	k2eAgFnSc1d1	finská
skupina	skupina	k1gFnSc1	skupina
Dolchamar	Dolchamar	k1gInSc1	Dolchamar
<g/>
,	,	kIx,	,
brazilská	brazilský	k2eAgFnSc1d1	brazilská
Supernova	supernova	k1gFnSc1	supernova
<g/>
,	,	kIx,	,
fríské	fríský	k2eAgNnSc1d1	fríské
uskupení	uskupení	k1gNnSc1	uskupení
Kajto	Kajt	k2eAgNnSc1d1	Kajt
či	či	k8xC	či
polský	polský	k2eAgMnSc1d1	polský
bard	bard	k1gMnSc1	bard
a	a	k8xC	a
přítel	přítel	k1gMnSc1	přítel
Jarka	Jarek	k1gMnSc2	Jarek
Nohavici	nohavice	k1gFnSc4	nohavice
Georgo	Georgo	k6eAd1	Georgo
Handzlik	Handzlika	k1gFnPc2	Handzlika
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
někteří	některý	k3yIgMnPc1	některý
populární	populární	k2eAgMnSc1d1	populární
skladatelé	skladatel	k1gMnPc1	skladatel
a	a	k8xC	a
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
britského	britský	k2eAgMnSc2d1	britský
Elvise	Elvis	k1gMnSc2	Elvis
Costella	Costell	k1gMnSc2	Costell
a	a	k8xC	a
amerického	americký	k2eAgMnSc2d1	americký
Michaela	Michael	k1gMnSc2	Michael
Jacksona	Jackson	k1gMnSc2	Jackson
<g/>
,	,	kIx,	,
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
písně	píseň	k1gFnPc4	píseň
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
,	,	kIx,	,
komponovali	komponovat	k5eAaImAgMnP	komponovat
skladby	skladba	k1gFnPc4	skladba
inspirované	inspirovaný	k2eAgFnSc2d1	inspirovaná
tímto	tento	k3xDgInSc7	tento
jazykem	jazyk	k1gInSc7	jazyk
nebo	nebo	k8xC	nebo
jej	on	k3xPp3gMnSc4	on
používali	používat	k5eAaImAgMnP	používat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
propagačních	propagační	k2eAgInPc6d1	propagační
materiálech	materiál	k1gInPc6	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
československá	československý	k2eAgFnSc1d1	Československá
pop-rocková	popockový	k2eAgFnSc1d1	pop-rocková
skupina	skupina	k1gFnSc1	skupina
Team	team	k1gInSc1	team
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
úspěchu	úspěch	k1gInSc6	úspěch
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
Team	team	k1gInSc1	team
nahrála	nahrát	k5eAaPmAgFnS	nahrát
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
i	i	k9	i
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
a	a	k8xC	a
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
mohla	moct	k5eAaImAgFnS	moct
vystoupit	vystoupit	k5eAaPmF	vystoupit
na	na	k7c6	na
několika	několik	k4yIc6	několik
zahraničních	zahraniční	k2eAgNnPc6d1	zahraniční
esperantských	esperantský	k2eAgNnPc6d1	esperantské
setkáních	setkání	k1gNnPc6	setkání
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Dočekal	Dočekal	k1gMnSc1	Dočekal
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
s	s	k7c7	s
esperantskou	esperantský	k2eAgFnSc7d1	esperantská
verzí	verze	k1gFnSc7	verze
písně	píseň	k1gFnSc2	píseň
Drahá	drahá	k1gFnSc1	drahá
<g/>
,	,	kIx,	,
vráť	vráť	k1gFnSc1	vráť
mi	já	k3xPp1nSc3	já
hlavu	hlava	k1gFnSc4	hlava
představil	představit	k5eAaPmAgInS	představit
i	i	k9	i
na	na	k7c6	na
výběrovém	výběrový	k2eAgNnSc6d1	výběrové
albu	album	k1gNnSc6	album
Vinilkosmo	Vinilkosma	k1gFnSc5	Vinilkosma
kompil	kompit	k5eAaPmAgInS	kompit
<g/>
'	'	kIx"	'
Volumo	Voluma	k1gFnSc5	Voluma
2	[number]	k4	2
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
vydal	vydat	k5eAaPmAgInS	vydat
i	i	k9	i
vlastní	vlastní	k2eAgNnSc4d1	vlastní
album	album	k1gNnSc4	album
s	s	k7c7	s
několika	několik	k4yIc2	několik
dalšími	další	k2eAgFnPc7d1	další
písněmi	píseň	k1gFnPc7	píseň
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
písničky	písnička	k1gFnPc1	písnička
z	z	k7c2	z
alba	album	k1gNnSc2	album
Esperanto	esperanto	k1gNnSc1	esperanto
od	od	k7c2	od
Warner	Warnra	k1gFnPc2	Warnra
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
–	–	k?	–
celé	celý	k2eAgNnSc4d1	celé
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
–	–	k?	–
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
vysokého	vysoký	k2eAgNnSc2d1	vysoké
umístění	umístění	k1gNnSc2	umístění
ve	v	k7c6	v
španělských	španělský	k2eAgFnPc6d1	španělská
hitparádách	hitparáda	k1gFnPc6	hitparáda
<g/>
;	;	kIx,	;
obdobně	obdobně	k6eAd1	obdobně
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
proslavila	proslavit	k5eAaPmAgFnS	proslavit
se	s	k7c7	s
singlem	singl	k1gInSc7	singl
Esperanto	esperanto	k1gNnSc4	esperanto
hiphopová	hiphopový	k2eAgFnSc1d1	hiphopová
skupina	skupina	k1gFnSc1	skupina
Freundeskreis	Freundeskreis	k1gFnSc2	Freundeskreis
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgNnPc1d1	klasické
díla	dílo	k1gNnPc1	dílo
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
a	a	k8xC	a
sbor	sbor	k1gInSc4	sbor
s	s	k7c7	s
esperantskými	esperantský	k2eAgInPc7d1	esperantský
texty	text	k1gInPc7	text
jsou	být	k5eAaImIp3nP	být
Sútra	sútra	k1gFnSc1	sútra
srdce	srdce	k1gNnSc2	srdce
od	od	k7c2	od
Lou	Lou	k1gFnSc2	Lou
Harrisona	Harrison	k1gMnSc2	Harrison
a	a	k8xC	a
První	první	k4xOgFnSc1	první
symfonie	symfonie	k1gFnSc1	symfonie
od	od	k7c2	od
Davida	David	k1gMnSc2	David
Gainese	Gainese	k1gFnSc2	Gainese
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
z	z	k7c2	z
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Toulouse	Toulouse	k1gInSc6	Toulouse
existuje	existovat	k5eAaImIp3nS	existovat
hudební	hudební	k2eAgNnSc4d1	hudební
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Vinilkosmo	Vinilkosma	k1gFnSc5	Vinilkosma
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
esperantské	esperantský	k2eAgFnSc2d1	esperantská
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
internetový	internetový	k2eAgInSc1d1	internetový
esperantský	esperantský	k2eAgInSc1d1	esperantský
zpěvník	zpěvník	k1gInSc1	zpěvník
KantarViki	KantarVik	k1gFnSc2	KantarVik
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
překonal	překonat	k5eAaPmAgMnS	překonat
hranici	hranice	k1gFnSc4	hranice
3	[number]	k4	3
000	[number]	k4	000
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xC	jak
původních	původní	k2eAgInPc2d1	původní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
přeložených	přeložený	k2eAgFnPc2d1	přeložená
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
existuje	existovat	k5eAaImIp3nS	existovat
rovněž	rovněž	k9	rovněž
Esperantsko-slovenský	esperantskolovenský	k2eAgInSc1d1	esperantsko-slovenský
spevník	spevník	k1gInSc1	spevník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
kino	kino	k1gNnSc1	kino
====	====	k?	====
</s>
</p>
<p>
<s>
Divadelní	divadelní	k2eAgFnPc1d1	divadelní
hry	hra	k1gFnPc1	hra
od	od	k7c2	od
dramatiků	dramatik	k1gMnPc2	dramatik
tak	tak	k8xS	tak
různorodých	různorodý	k2eAgMnPc2d1	různorodý
jako	jako	k8xS	jako
Goldoni	Goldon	k1gMnPc1	Goldon
<g/>
,	,	kIx,	,
Ionesco	Ionesco	k1gMnSc1	Ionesco
<g/>
,	,	kIx,	,
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
a	a	k8xC	a
Alan	Alan	k1gMnSc1	Alan
Ayckbourg	Ayckbourg	k1gMnSc1	Ayckbourg
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k8xC	i
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
esperanto	esperanto	k1gNnSc1	esperanto
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
filmech	film	k1gInPc6	film
jako	jako	k8xS	jako
kulisa	kulisa	k1gFnSc1	kulisa
–	–	k?	–
např.	např.	kA	např.
v	v	k7c4	v
Chaplinově	Chaplinově	k1gMnPc4	Chaplinově
Diktátorovi	diktátorův	k2eAgMnPc1d1	diktátorův
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
jsou	být	k5eAaImIp3nP	být
napsány	napsán	k2eAgInPc4d1	napsán
nápisy	nápis	k1gInPc4	nápis
a	a	k8xC	a
plakáty	plakát	k1gInPc4	plakát
–	–	k?	–
jindy	jindy	k6eAd1	jindy
zase	zase	k9	zase
k	k	k7c3	k
vykreslení	vykreslení	k1gNnSc3	vykreslení
budoucnosti	budoucnost	k1gFnSc2	budoucnost
–	–	k?	–
např.	např.	kA	např.
v	v	k7c6	v
akčním	akční	k2eAgInSc6d1	akční
filmu	film	k1gInSc6	film
Blade	Blad	k1gInSc5	Blad
<g/>
:	:	kIx,	:
Trinity	Trinita	k1gFnPc4	Trinita
nebo	nebo	k8xC	nebo
v	v	k7c6	v
komediálním	komediální	k2eAgInSc6d1	komediální
vědecko-fantastickém	vědeckoantastický	k2eAgInSc6d1	vědecko-fantastický
seriálu	seriál	k1gInSc6	seriál
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Celovečerní	celovečerní	k2eAgInPc1d1	celovečerní
filmy	film	k1gInPc1	film
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
méně	málo	k6eAd2	málo
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
existuje	existovat	k5eAaImIp3nS	existovat
asi	asi	k9	asi
15	[number]	k4	15
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
tématu	téma	k1gNnSc3	téma
esperanta	esperanto	k1gNnPc1	esperanto
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodnou	pozoruhodný	k2eAgFnSc7d1	pozoruhodná
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
kultovní	kultovní	k2eAgInSc1d1	kultovní
film	film	k1gInSc1	film
Incubus	Incubus	k1gInSc1	Incubus
(	(	kIx(	(
<g/>
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
William	William	k1gInSc1	William
Shatner	Shatnra	k1gFnPc2	Shatnra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
dialogy	dialog	k1gInPc1	dialog
jsou	být	k5eAaImIp3nP	být
výlučně	výlučně	k6eAd1	výlučně
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
esperanta	esperanto	k1gNnSc2	esperanto
bývají	bývat	k5eAaImIp3nP	bývat
překládány	překládat	k5eAaImNgInP	překládat
také	také	k9	také
titulky	titulek	k1gInPc4	titulek
k	k	k7c3	k
filmům	film	k1gInPc3	film
<g/>
;	;	kIx,	;
organizaci	organizace	k1gFnSc3	organizace
jejich	jejich	k3xOp3gNnSc2	jejich
překládání	překládání	k1gNnSc2	překládání
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
shromažďování	shromažďování	k1gNnSc1	shromažďování
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
Verda	Verda	k1gMnSc1	Verda
Filmejo	Filmejo	k1gMnSc1	Filmejo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Verda	Verdo	k1gNnSc2	Verdo
Filmejo	Filmejo	k6eAd1	Filmejo
se	se	k3xPyFc4	se
vyčlenila	vyčlenit	k5eAaPmAgFnS	vyčlenit
tvůrčí	tvůrčí	k2eAgFnSc1d1	tvůrčí
skupina	skupina	k1gFnSc1	skupina
Filmoj	Filmoj	k1gInSc4	Filmoj
sen	sen	k1gInSc1	sen
Limoj	Limoj	k1gInSc1	Limoj
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Filmy	film	k1gInPc7	film
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
připravovat	připravovat	k5eAaImF	připravovat
esperantské	esperantský	k2eAgNnSc1d1	esperantské
znění	znění	k1gNnSc1	znění
k	k	k7c3	k
filmům	film	k1gInPc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
v	v	k7c6	v
Sã	Sã	k1gFnSc6	Sã
Paulu	Paula	k1gFnSc4	Paula
první	první	k4xOgInSc1	první
esperantský	esperantský	k2eAgInSc1d1	esperantský
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
i	i	k9	i
soutěž	soutěž	k1gFnSc1	soutěž
velmi	velmi	k6eAd1	velmi
krátkých	krátký	k2eAgInPc2d1	krátký
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
také	také	k9	také
několik	několik	k4yIc1	několik
amatérských	amatérský	k2eAgInPc2d1	amatérský
esperantských	esperantský	k2eAgInPc2d1	esperantský
filmových	filmový	k2eAgInPc2d1	filmový
projektů	projekt	k1gInPc2	projekt
a	a	k8xC	a
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
krátkometrážní	krátkometrážní	k2eAgInPc4d1	krátkometrážní
snímky	snímek	k1gInPc4	snímek
plně	plně	k6eAd1	plně
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Periodika	periodikum	k1gNnSc2	periodikum
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
vydává	vydávat	k5eAaPmIp3nS	vydávat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
revuí	revue	k1gFnPc2	revue
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zpravodajského	zpravodajský	k2eAgInSc2d1	zpravodajský
měsíčníku	měsíčník	k1gInSc2	měsíčník
"	"	kIx"	"
<g/>
Monato	Monat	k2eAgNnSc1d1	Monato
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc2d1	literární
revue	revue	k1gFnSc2	revue
"	"	kIx"	"
<g/>
Beletra	Beletra	k1gFnSc1	Beletra
almanako	almanako	k6eAd1	almanako
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Beletristický	beletristický	k2eAgInSc1d1	beletristický
almanach	almanach	k1gInSc1	almanach
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
revue	revue	k1gFnSc2	revue
"	"	kIx"	"
<g/>
Esperanto	esperanto	k1gNnSc4	esperanto
<g/>
"	"	kIx"	"
Světového	světový	k2eAgInSc2d1	světový
esperantského	esperantský	k2eAgInSc2d1	esperantský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Dvojtýdeník	Dvojtýdeník	k1gInSc1	Dvojtýdeník
aktualit	aktualita	k1gFnPc2	aktualita
"	"	kIx"	"
<g/>
Eventoj	Eventoj	k1gInSc1	Eventoj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Události	událost	k1gFnPc1	událost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nabízen	nabízet	k5eAaImNgInS	nabízet
i	i	k9	i
v	v	k7c6	v
bezplatné	bezplatný	k2eAgFnSc6d1	bezplatná
elektronické	elektronický	k2eAgFnSc6d1	elektronická
verzi	verze	k1gFnSc6	verze
a	a	k8xC	a
svým	svůj	k3xOyFgMnPc3	svůj
předplatitelům	předplatitel	k1gMnPc3	předplatitel
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
dostupné	dostupný	k2eAgFnSc6d1	dostupná
i	i	k8xC	i
ostatní	ostatní	k2eAgInPc4d1	ostatní
zmiňované	zmiňovaný	k2eAgInPc4d1	zmiňovaný
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
významné	významný	k2eAgInPc4d1	významný
časopisy	časopis	k1gInPc4	časopis
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
internetového	internetový	k2eAgInSc2d1	internetový
archivu	archiv	k1gInSc2	archiv
starších	starý	k2eAgNnPc2d2	starší
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vycházejí	vycházet	k5eAaImIp3nP	vycházet
také	také	k9	také
periodika	periodikum	k1gNnSc2	periodikum
o	o	k7c6	o
medicíně	medicína	k1gFnSc6	medicína
<g/>
,	,	kIx,	,
přírodních	přírodní	k2eAgFnPc6d1	přírodní
vědách	věda	k1gFnPc6	věda
<g/>
,	,	kIx,	,
časopisy	časopis	k1gInPc4	časopis
s	s	k7c7	s
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
pedagogy	pedagog	k1gMnPc4	pedagog
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
literární	literární	k2eAgFnPc1d1	literární
revue	revue	k1gFnPc1	revue
či	či	k8xC	či
různé	různý	k2eAgInPc1d1	různý
speciálně	speciálně	k6eAd1	speciálně
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
časopisy	časopis	k1gInPc4	časopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rozhlas	rozhlas	k1gInSc1	rozhlas
a	a	k8xC	a
televize	televize	k1gFnSc1	televize
====	====	k?	====
</s>
</p>
<p>
<s>
Rozhlasové	rozhlasový	k2eAgFnPc1d1	rozhlasová
stanice	stanice	k1gFnPc1	stanice
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Kubě	Kuba	k1gFnSc6	Kuba
či	či	k8xC	či
Vatikánu	Vatikán	k1gInSc6	Vatikán
vysílají	vysílat	k5eAaImIp3nP	vysílat
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
relace	relace	k1gFnSc2	relace
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
rozhlasové	rozhlasový	k2eAgInPc1d1	rozhlasový
programy	program	k1gInPc1	program
lze	lze	k6eAd1	lze
poslouchat	poslouchat	k5eAaImF	poslouchat
také	také	k9	také
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
stanice	stanice	k1gFnPc1	stanice
(	(	kIx(	(
<g/>
profesionální	profesionální	k2eAgFnPc1d1	profesionální
i	i	k8xC	i
amatérské	amatérský	k2eAgFnPc1d1	amatérská
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
primárně	primárně	k6eAd1	primárně
<g/>
.	.	kIx.	.
</s>
<s>
Internetové	internetový	k2eAgNnSc1d1	internetové
rádio	rádio	k1gNnSc1	rádio
Muzaiko	Muzaika	k1gFnSc5	Muzaika
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
vysílá	vysílat	k5eAaImIp3nS	vysílat
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgInPc1d1	televizní
kanály	kanál	k1gInPc1	kanál
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
vysílají	vysílat	k5eAaImIp3nP	vysílat
kurzy	kurz	k1gInPc1	kurz
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nejnovější	nový	k2eAgFnSc2d3	nejnovější
16	[number]	k4	16
<g/>
-dílné	-dílný	k2eAgFnSc2d1	-dílný
adaptace	adaptace	k1gFnSc2	adaptace
kurzu	kurz	k1gInSc2	kurz
BBC	BBC	kA	BBC
"	"	kIx"	"
<g/>
Mazi	Mazi	k1gNnPc1	Mazi
en	en	k?	en
Gondolando	Gondolanda	k1gFnSc5	Gondolanda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mazi	Mazi	k1gNnSc1	Mazi
v	v	k7c6	v
Gondolandu	Gondoland	k1gInSc6	Gondoland
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
odvysílal	odvysílat	k5eAaPmAgInS	odvysílat
televizní	televizní	k2eAgInSc1d1	televizní
kanál	kanál	k1gInSc1	kanál
TVP	TVP	kA	TVP
1	[number]	k4	1
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokumentárního	dokumentární	k2eAgInSc2d1	dokumentární
filmu	film	k1gInSc2	film
"	"	kIx"	"
<g/>
Esperanto	esperanto	k1gNnSc1	esperanto
je	být	k5eAaImIp3nS	být
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
také	také	k9	také
projekt	projekt	k1gInSc4	projekt
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
internetové	internetový	k2eAgFnSc2d1	internetová
esperantské	esperantský	k2eAgFnSc2d1	esperantská
televize	televize	k1gFnSc2	televize
"	"	kIx"	"
<g/>
Internacia	Internacia	k1gFnSc1	Internacia
Televido	Televida	k1gFnSc5	Televida
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Internet	Internet	k1gInSc1	Internet
====	====	k?	====
</s>
</p>
<p>
<s>
Elektronické	elektronický	k2eAgFnPc1d1	elektronická
sítě	síť	k1gFnPc1	síť
a	a	k8xC	a
především	především	k9	především
internet	internet	k1gInSc4	internet
jsou	být	k5eAaImIp3nP	být
nejrychleji	rychle	k6eAd3	rychle
rostoucími	rostoucí	k2eAgInPc7d1	rostoucí
prostředky	prostředek	k1gInPc7	prostředek
komunikace	komunikace	k1gFnSc2	komunikace
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c4	mnoho
stovek	stovka	k1gFnPc2	stovka
diskuzních	diskuzní	k2eAgFnPc2d1	diskuzní
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
rozmanitými	rozmanitý	k2eAgNnPc7d1	rozmanité
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
od	od	k7c2	od
používání	používání	k1gNnSc2	používání
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
až	až	k9	až
po	po	k7c4	po
obecnou	obecný	k2eAgFnSc4d1	obecná
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
na	na	k7c4	na
statisíce	statisíce	k1gInPc4	statisíce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
České	český	k2eAgFnSc2d1	Česká
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
překladem	překlad	k1gInSc7	překlad
z	z	k7c2	z
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
,	,	kIx,	,
esperantského	esperantský	k2eAgInSc2d1	esperantský
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
i	i	k9	i
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
wikipedista	wikipedista	k1gMnSc1	wikipedista
<g/>
"	"	kIx"	"
a	a	k8xC	a
specificky	specificky	k6eAd1	specificky
česky	česky	k6eAd1	česky
zakončený	zakončený	k2eAgInSc1d1	zakončený
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Překladač	překladač	k1gInSc1	překladač
Google	Google	k1gFnSc2	Google
Translate	Translat	k1gInSc5	Translat
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
podporuje	podporovat	k5eAaImIp3nS	podporovat
esperanto	esperanto	k1gNnSc4	esperanto
jako	jako	k8xS	jako
svůj	svůj	k3xOyFgMnSc1	svůj
64	[number]	k4	64
<g/>
.	.	kIx.	.
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
největší	veliký	k2eAgInSc1d3	veliký
výkladový	výkladový	k2eAgInSc1d1	výkladový
slovník	slovník	k1gInSc1	slovník
esperanta	esperanto	k1gNnSc2	esperanto
Plena	plena	k1gFnSc1	plena
Ilustrita	Ilustrita	k1gFnSc1	Ilustrita
Vortaro	Vortara	k1gFnSc5	Vortara
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
50.000	[number]	k4	50.000
lexikálních	lexikální	k2eAgFnPc2d1	lexikální
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
dostupný	dostupný	k2eAgInSc1d1	dostupný
zdarma	zdarma	k6eAd1	zdarma
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
v	v	k7c6	v
komunikačních	komunikační	k2eAgInPc6d1	komunikační
chatových	chatový	k2eAgInPc6d1	chatový
protokolech	protokol	k1gInPc6	protokol
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
ICQ	ICQ	kA	ICQ
<g/>
,	,	kIx,	,
IRC	IRC	kA	IRC
<g/>
,	,	kIx,	,
Skype	Skyp	k1gMnSc5	Skyp
<g/>
,	,	kIx,	,
Jabber	Jabbero	k1gNnPc2	Jabbero
či	či	k8xC	či
Paltalk	Paltalka	k1gFnPc2	Paltalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Služby	služba	k1gFnSc2	služba
UEA	UEA	kA	UEA
====	====	k?	====
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
esperantský	esperantský	k2eAgInSc1d1	esperantský
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
UEA	UEA	kA	UEA
<g/>
)	)	kIx)	)
vydává	vydávat	k5eAaPmIp3nS	vydávat
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
revue	revue	k1gFnPc4	revue
a	a	k8xC	a
ročenku	ročenka	k1gFnSc4	ročenka
se	s	k7c7	s
seznamem	seznam	k1gInSc7	seznam
esperantských	esperantský	k2eAgFnPc2d1	esperantská
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
místních	místní	k2eAgMnPc2d1	místní
reprezentantů	reprezentant	k1gMnPc2	reprezentant
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
publikace	publikace	k1gFnPc4	publikace
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
o	o	k7c6	o
nosičích	nosič	k1gInPc6	nosič
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
kazetách	kazeta	k1gFnPc6	kazeta
atd.	atd.	kA	atd.
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
od	od	k7c2	od
UEA	UEA	kA	UEA
vyžádat	vyžádat	k5eAaPmF	vyžádat
písemně	písemně	k6eAd1	písemně
nebo	nebo	k8xC	nebo
konzultovat	konzultovat	k5eAaImF	konzultovat
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
UEA	UEA	kA	UEA
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
zásobě	zásoba	k1gFnSc6	zásoba
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3500	[number]	k4	3500
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
"	"	kIx"	"
<g/>
Esperantské	esperantský	k2eAgInPc1d1	esperantský
dokumenty	dokument	k1gInPc1	dokument
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
UEA	UEA	kA	UEA
vydává	vydávat	k5eAaImIp3nS	vydávat
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
,	,	kIx,	,
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
studie	studie	k1gFnSc1	studie
a	a	k8xC	a
referáty	referát	k1gInPc1	referát
o	o	k7c4	o
aktuální	aktuální	k2eAgFnSc4d1	aktuální
situaci	situace	k1gFnSc4	situace
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
publikace	publikace	k1gFnPc4	publikace
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
obstarat	obstarat	k5eAaPmF	obstarat
v	v	k7c6	v
ústředí	ústředí	k1gNnSc6	ústředí
UEA	UEA	kA	UEA
v	v	k7c6	v
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Esperantské	esperantský	k2eAgNnSc1d1	esperantské
hnutí	hnutí	k1gNnSc1	hnutí
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgInPc7d1	hlavní
reprezentanty	reprezentant	k1gInPc7	reprezentant
esperanta	esperanto	k1gNnSc2	esperanto
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Český	český	k2eAgInSc1d1	český
esperantský	esperantský	k2eAgInSc1d1	esperantský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
čítá	čítat	k5eAaImIp3nS	čítat
necelých	celý	k2eNgInPc2d1	necelý
900	[number]	k4	900
zaregistrovaných	zaregistrovaný	k2eAgInPc2d1	zaregistrovaný
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
esperantská	esperantský	k2eAgFnSc1d1	esperantská
mládež	mládež	k1gFnSc1	mládež
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
znovuobnovení	znovuobnovení	k1gNnSc2	znovuobnovení
jako	jako	k9	jako
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
mladé	mladý	k2eAgMnPc4d1	mladý
esperantisty	esperantista	k1gMnPc4	esperantista
do	do	k7c2	do
věku	věk	k1gInSc2	věk
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
esperantský	esperantský	k2eAgInSc1d1	esperantský
svaz	svaz	k1gInSc1	svaz
hostil	hostit	k5eAaImAgInS	hostit
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
81	[number]	k4	81
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
kongres	kongres	k1gInSc1	kongres
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
esperantská	esperantský	k2eAgFnSc1d1	esperantská
mládež	mládež	k1gFnSc1	mládež
pořádala	pořádat	k5eAaImAgFnS	pořádat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2009	[number]	k4	2009
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
organizacemi	organizace	k1gFnPc7	organizace
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
65	[number]	k4	65
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
kongres	kongres	k1gInSc1	kongres
esperantské	esperantský	k2eAgFnSc2d1	esperantská
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
činnost	činnost	k1gFnSc1	činnost
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
klubech	klub	k1gInPc6	klub
esperantistů	esperantista	k1gMnPc2	esperantista
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Kluby	klub	k1gInPc1	klub
vydávají	vydávat	k5eAaImIp3nP	vydávat
časopisy	časopis	k1gInPc4	časopis
<g/>
,	,	kIx,	,
pořádají	pořádat	k5eAaImIp3nP	pořádat
kongresy	kongres	k1gInPc4	kongres
<g/>
,	,	kIx,	,
letní	letní	k2eAgInPc4d1	letní
tábory	tábor	k1gInPc4	tábor
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgFnPc4d1	další
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
také	také	k9	také
další	další	k2eAgFnSc1d1	další
mluvčí	mluvčí	k1gFnSc1	mluvčí
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc4	počet
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejsou	být	k5eNaImIp3nP	být
zapojeni	zapojit	k5eAaPmNgMnP	zapojit
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
žádné	žádný	k3yNgFnSc2	žádný
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
činnost	činnost	k1gFnSc4	činnost
"	"	kIx"	"
<g/>
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
<g/>
"	"	kIx"	"
či	či	k8xC	či
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
pasivními	pasivní	k2eAgMnPc7d1	pasivní
uživateli	uživatel	k1gMnPc7	uživatel
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgMnPc4d1	známý
uživatele	uživatel	k1gMnPc4	uživatel
esperanta	esperanto	k1gNnSc2	esperanto
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
psychiatr	psychiatr	k1gMnSc1	psychiatr
Max	Max	k1gMnSc1	Max
Kašparů	Kašpar	k1gMnPc2	Kašpar
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
kardinál	kardinál	k1gMnSc1	kardinál
Vlk	Vlk	k1gMnSc1	Vlk
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
Pavel	Pavel	k1gMnSc1	Pavel
Rak	rak	k1gMnSc1	rak
či	či	k8xC	či
nakladatelé	nakladatel	k1gMnPc1	nakladatel
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Chrdle	Chrdle	k1gMnSc1	Chrdle
(	(	kIx(	(
<g/>
KAVA-PECH	KAVA-PECH	k1gMnSc1	KAVA-PECH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
českým	český	k2eAgMnSc7d1	český
členem	člen	k1gMnSc7	člen
Akademie	akademie	k1gFnSc2	akademie
esperanta	esperanto	k1gNnSc2	esperanto
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
informatik	informatik	k1gMnSc1	informatik
Marek	Marek	k1gMnSc1	Marek
Blahuš	Blahuš	k1gMnSc1	Blahuš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
jazyka	jazyk	k1gInSc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
je	být	k5eAaImIp3nS	být
plánový	plánový	k2eAgInSc4d1	plánový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
mluvené	mluvený	k2eAgFnSc6d1	mluvená
i	i	k8xC	i
psané	psaný	k2eAgFnSc6d1	psaná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
gramatice	gramatika	k1gFnSc6	gramatika
převažují	převažovat	k5eAaImIp3nP	převažovat
apriorní	apriorní	k2eAgInPc1d1	apriorní
rysy	rys	k1gInPc1	rys
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
pravidelnost	pravidelnost	k1gFnSc4	pravidelnost
a	a	k8xC	a
snadnou	snadný	k2eAgFnSc4d1	snadná
naučitelnost	naučitelnost	k1gFnSc4	naučitelnost
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
velmi	velmi	k6eAd1	velmi
přesné	přesný	k2eAgNnSc1d1	přesné
vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
pochází	pocházet	k5eAaImIp3nS	pocházet
především	především	k9	především
ze	z	k7c2	z
západních	západní	k2eAgInPc2d1	západní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gFnSc1	jeho
syntax	syntax	k1gFnSc1	syntax
(	(	kIx(	(
<g/>
větná	větný	k2eAgFnSc1d1	větná
skladba	skladba	k1gFnSc1	skladba
<g/>
)	)	kIx)	)
a	a	k8xC	a
morfologie	morfologie	k1gFnSc1	morfologie
(	(	kIx(	(
<g/>
tvarosloví	tvarosloví	k1gNnSc1	tvarosloví
<g/>
)	)	kIx)	)
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
silný	silný	k2eAgInSc4d1	silný
slovanský	slovanský	k2eAgInSc4d1	slovanský
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Esperantské	esperantský	k2eAgInPc4d1	esperantský
morfémy	morfém	k1gInPc4	morfém
(	(	kIx(	(
<g/>
nejmenší	malý	k2eAgInSc4d3	nejmenší
význam	význam	k1gInSc4	význam
nesoucí	nesoucí	k2eAgInPc4d1	nesoucí
stavební	stavební	k2eAgInPc4d1	stavební
prvky	prvek	k1gInPc4	prvek
jazyka	jazyk	k1gInSc2	jazyk
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
neměnné	měnný	k2eNgFnPc1d1	neměnná
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
kombinovat	kombinovat	k5eAaImF	kombinovat
do	do	k7c2	do
rozmanitých	rozmanitý	k2eAgNnPc2d1	rozmanité
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
mnoho	mnoho	k6eAd1	mnoho
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
izolujícími	izolující	k2eAgMnPc7d1	izolující
jazyky	jazyk	k1gMnPc7	jazyk
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
struktura	struktura	k1gFnSc1	struktura
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
připomíná	připomínat	k5eAaImIp3nS	připomínat
aglutinační	aglutinační	k2eAgInPc4d1	aglutinační
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
turečtina	turečtina	k1gFnSc1	turečtina
<g/>
,	,	kIx,	,
svahilština	svahilština	k1gFnSc1	svahilština
a	a	k8xC	a
japonština	japonština	k1gFnSc1	japonština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
93	[number]	k4	93
%	%	kIx~	%
slov	slovo	k1gNnPc2	slovo
esperanta	esperanto	k1gNnSc2	esperanto
je	on	k3xPp3gMnPc4	on
srozumitelných	srozumitelný	k2eAgFnPc2d1	srozumitelná
se	se	k3xPyFc4	se
znalostí	znalost	k1gFnPc2	znalost
románských	románský	k2eAgMnPc2d1	románský
<g/>
,	,	kIx,	,
90	[number]	k4	90
%	%	kIx~	%
se	se	k3xPyFc4	se
znalostí	znalost	k1gFnSc7	znalost
germánských	germánský	k2eAgMnPc2d1	germánský
a	a	k8xC	a
47	[number]	k4	47
%	%	kIx~	%
se	s	k7c7	s
znalostí	znalost	k1gFnSc7	znalost
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Úplná	úplný	k2eAgFnSc1d1	úplná
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
čítá	čítat	k5eAaImIp3nS	čítat
asi	asi	k9	asi
350	[number]	k4	350
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
však	však	k9	však
snadno	snadno	k6eAd1	snadno
odvoditelná	odvoditelný	k2eAgFnSc1d1	odvoditelná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
znalosti	znalost	k1gFnSc2	znalost
kořene	kořen	k1gInSc2	kořen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Abeceda	abeceda	k1gFnSc1	abeceda
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
28	[number]	k4	28
písmen	písmeno	k1gNnPc2	písmeno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
písmen	písmeno	k1gNnPc2	písmeno
se	se	k3xPyFc4	se
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
píše	psát	k5eAaImIp3nS	psát
a	a	k8xC	a
hlásky	hlásek	k1gInPc1	hlásek
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
však	však	k9	však
i	i	k9	i
drobné	drobný	k2eAgFnPc4d1	drobná
odlišnosti	odlišnost	k1gFnPc4	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
ĝ	ĝ	k?	ĝ
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
dž	dž	k?	dž
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
džem	džem	k1gInSc4	džem
<g/>
;	;	kIx,	;
také	také	k9	také
užívání	užívání	k1gNnSc1	užívání
polohlásky	polohláska	k1gFnSc2	polohláska
ŭ	ŭ	k?	ŭ
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
:	:	kIx,	:
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
dvojhláskách	dvojhláska	k1gFnPc6	dvojhláska
aŭ	aŭ	k?	aŭ
<g/>
,	,	kIx,	,
eŭ	eŭ	k?	eŭ
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
dvojhlásky	dvojhláska	k1gFnSc2	dvojhláska
au	au	k0	au
<g/>
,	,	kIx,	,
eu	eu	k?	eu
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
automobil	automobil	k1gInSc1	automobil
či	či	k8xC	či
leukocyt	leukocyt	k1gInSc1	leukocyt
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
au	au	k0	au
<g/>
,	,	kIx,	,
eu	eu	k?	eu
se	se	k3xPyFc4	se
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
jako	jako	k9	jako
dvě	dva	k4xCgFnPc4	dva
samostatné	samostatný	k2eAgFnPc4d1	samostatná
hlásky	hláska	k1gFnPc4	hláska
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
obdobu	obdoba	k1gFnSc4	obdoba
například	například	k6eAd1	například
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
naučit	naučit	k5eAaPmF	naučit
či	či	k8xC	či
neumět	umět	k5eNaImF	umět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
jako	jako	k9	jako
na-učit	načit	k5eAaPmF	na-učit
<g/>
,	,	kIx,	,
ne-umět	mět	k5eNaPmF	-umět
<g/>
.	.	kIx.	.
</s>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
přesnější	přesný	k2eAgMnSc1d2	přesnější
než	než	k8xS	než
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
nepřepisuje	přepisovat	k5eNaImIp3nS	přepisovat
dokonale	dokonale	k6eAd1	dokonale
foneticky	foneticky	k6eAd1	foneticky
<g/>
,	,	kIx,	,
nerozlišujíc	rozlišovat	k5eNaImSgFnS	rozlišovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
o	o	k7c4	o
dvojhlásku	dvojhláska	k1gFnSc4	dvojhláska
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
,	,	kIx,	,
či	či	k8xC	či
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
<s>
Slabiky	slabika	k1gFnPc1	slabika
di	di	k?	di
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
nezměkčeně	změkčeně	k6eNd1	změkčeně
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
,	,	kIx,	,
tinktura	tinktura	k1gFnSc1	tinktura
nebo	nebo	k8xC	nebo
nikotin	nikotin	k1gInSc1	nikotin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
===	===	k?	===
</s>
</p>
<p>
<s>
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
nová	nový	k2eAgNnPc1d1	nové
slova	slovo	k1gNnPc1	slovo
i	i	k9	i
z	z	k7c2	z
neevropských	evropský	k2eNgInPc2d1	neevropský
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
japonštiny	japonština	k1gFnSc2	japonština
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
kmeny	kmen	k1gInPc4	kmen
mezinárodně	mezinárodně	k6eAd1	mezinárodně
známé	známý	k2eAgInPc1d1	známý
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
nicméně	nicméně	k8xC	nicméně
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
evropských	evropský	k2eAgFnPc2d1	Evropská
řečí	řeč	k1gFnPc2	řeč
–	–	k?	–
především	především	k9	především
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
španělštiny	španělština	k1gFnSc2	španělština
<g/>
,	,	kIx,	,
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
značné	značný	k2eAgFnSc3d1	značná
shodě	shoda	k1gFnSc3	shoda
kmenů	kmen	k1gInPc2	kmen
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
esperantských	esperantský	k2eAgInPc2d1	esperantský
kmenů	kmen	k1gInPc2	kmen
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
národních	národní	k2eAgFnPc6d1	národní
řečech	řeč	k1gFnPc6	řeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
předpon	předpona	k1gFnPc2	předpona
a	a	k8xC	a
přípon	přípona	k1gFnPc2	přípona
významně	významně	k6eAd1	významně
redukuje	redukovat	k5eAaBmIp3nS	redukovat
počet	počet	k1gInSc1	počet
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
se	se	k3xPyFc4	se
učit	učit	k5eAaImF	učit
a	a	k8xC	a
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
naučitelnost	naučitelnost	k1gFnSc1	naučitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Přípony	přípona	k1gFnPc1	přípona
a	a	k8xC	a
předpony	předpona	k1gFnPc1	předpona
však	však	k9	však
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
zároveň	zároveň	k6eAd1	zároveň
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
běžná	běžný	k2eAgNnPc1d1	běžné
slova	slovo	k1gNnPc1	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Principy	princip	k1gInPc1	princip
výběru	výběr	k1gInSc2	výběr
slovních	slovní	k2eAgInPc2d1	slovní
kmenů	kmen	k1gInPc2	kmen
===	===	k?	===
</s>
</p>
<p>
<s>
Pojmenování	pojmenování	k1gNnSc4	pojmenování
dnů	den	k1gInPc2	den
týdne	týden	k1gInSc2	týden
byla	být	k5eAaImAgFnS	být
převzata	převzít	k5eAaPmNgFnS	převzít
z	z	k7c2	z
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
francouzských	francouzský	k2eAgInPc2d1	francouzský
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
dimanĉ	dimanĉ	k?	dimanĉ
<g/>
,	,	kIx,	,
lundo	lundo	k?	lundo
<g/>
,	,	kIx,	,
mardo	mardo	k1gNnSc1	mardo
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
názvy	název	k1gInPc1	název
mnoha	mnoho	k4c2	mnoho
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
či	či	k8xC	či
řečtiny	řečtina	k1gFnSc2	řečtina
(	(	kIx(	(
<g/>
hepato	hepato	k6eAd1	hepato
<g/>
,	,	kIx,	,
okulo	okout	k5eAaPmAgNnS	okout
<g/>
,	,	kIx,	,
brako	braka	k1gFnSc5	braka
<g/>
,	,	kIx,	,
koro	kora	k1gFnSc5	kora
<g/>
,	,	kIx,	,
reno	reno	k1gMnSc1	reno
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
označení	označení	k1gNnSc1	označení
časových	časový	k2eAgFnPc2d1	časová
jednotek	jednotka	k1gFnPc2	jednotka
z	z	k7c2	z
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
německých	německý	k2eAgInPc2d1	německý
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
monato	monato	k6eAd1	monato
<g/>
,	,	kIx,	,
tago	tago	k6eAd1	tago
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
pocházejí	pocházet	k5eAaImIp3nP	pocházet
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
latinských	latinský	k2eAgNnPc2d1	latinské
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
kořeny	kořen	k1gInPc1	kořen
jsou	být	k5eAaImIp3nP	být
srozumitelné	srozumitelný	k2eAgNnSc4d1	srozumitelné
mluvčím	mluvčí	k1gMnSc7	mluvčí
několika	několik	k4yIc2	několik
odlišných	odlišný	k2eAgInPc2d1	odlišný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
abdiki	abdiki	k6eAd1	abdiki
–	–	k?	–
mluvčím	mluvčí	k1gMnSc7	mluvčí
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
italštiny	italština	k1gFnSc2	italština
a	a	k8xC	a
latiny	latina	k1gFnSc2	latina
</s>
</p>
<p>
<s>
abituriento	abituriento	k1gNnSc1	abituriento
–	–	k?	–
mluvčím	mluvčit	k5eAaImIp1nS	mluvčit
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
ruštiny	ruština	k1gFnSc2	ruština
</s>
</p>
<p>
<s>
ablativo	ablativa	k1gFnSc5	ablativa
–	–	k?	–
mluvčím	mluvčí	k1gMnSc7	mluvčí
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
italštiny	italština	k1gFnSc2	italština
<g/>
,	,	kIx,	,
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
španělštiny	španělština	k1gFnSc2	španělština
</s>
</p>
<p>
<s>
funto	funto	k1gNnSc1	funto
–	–	k?	–
mluvčím	mluvčit	k5eAaImIp1nS	mluvčit
jidiš	jidiš	k1gNnSc2	jidiš
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
polštiny	polština	k1gFnSc2	polština
a	a	k8xC	a
ruštiny	ruština	k1gFnSc2	ruština
</s>
</p>
<p>
<s>
ŝ	ŝ	k?	ŝ
–	–	k?	–
mluvčím	mluvčí	k1gMnSc7	mluvčí
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
polštiny	polština	k1gFnSc2	polština
</s>
</p>
<p>
<s>
===	===	k?	===
Ukázky	ukázka	k1gFnPc4	ukázka
slov	slovo	k1gNnPc2	slovo
===	===	k?	===
</s>
</p>
<p>
<s>
z	z	k7c2	z
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
:	:	kIx,	:
abio	abio	k1gNnSc1	abio
(	(	kIx(	(
<g/>
jedle	jedle	k1gFnPc1	jedle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sed	sed	k1gInSc1	sed
(	(	kIx(	(
<g/>
ale	ale	k9	ale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tamen	tamen	k1gInSc1	tamen
(	(	kIx(	(
<g/>
přece	přece	k9	přece
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okulo	okout	k5eAaPmAgNnS	okout
(	(	kIx(	(
<g/>
oko	oko	k1gNnSc1	oko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akvo	akvo	k1gMnSc1	akvo
(	(	kIx(	(
<g/>
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
:	:	kIx,	:
dimanĉ	dimanĉ	k?	dimanĉ
(	(	kIx(	(
<g/>
neděle	neděle	k1gFnSc2	neděle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fermi	fer	k1gFnPc7	fer
(	(	kIx(	(
<g/>
zavřít	zavřít	k5eAaPmF	zavřít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ĉ	ĉ	k?	ĉ
(	(	kIx(	(
<g/>
u	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
frapi	frapi	k6eAd1	frapi
(	(	kIx(	(
<g/>
klepat	klepat	k5eAaImF	klepat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ĉ	ĉ	k?	ĉ
(	(	kIx(	(
<g/>
kůň	kůň	k1gMnSc1	kůň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
italštiny	italština	k1gFnSc2	italština
<g/>
:	:	kIx,	:
ĉ	ĉ	k?	ĉ
(	(	kIx(	(
<g/>
nebe	nebe	k1gNnSc2	nebe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fari	fari	k6eAd1	fari
(	(	kIx(	(
<g/>
dělat	dělat	k5eAaImF	dělat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
voĉ	voĉ	k?	voĉ
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
více	hodně	k6eAd2	hodně
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
facila	facila	k1gFnSc1	facila
(	(	kIx(	(
<g/>
snadný	snadný	k2eAgInSc1d1	snadný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fero	fero	k1gNnSc1	fero
(	(	kIx(	(
<g/>
železo	železo	k1gNnSc1	železo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tra	tra	k0	tra
(	(	kIx(	(
<g/>
skrz	skrz	k6eAd1	skrz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
verda	verda	k1gFnSc1	verda
(	(	kIx(	(
<g/>
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
<g/>
:	:	kIx,	:
baldaŭ	baldaŭ	k?	baldaŭ
(	(	kIx(	(
<g/>
brzy	brzy	k6eAd1	brzy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bedaŭ	bedaŭ	k?	bedaŭ
(	(	kIx(	(
<g/>
litovat	litovat	k5eAaImF	litovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
haŭ	haŭ	k?	haŭ
(	(	kIx(	(
<g/>
kůže	kůže	k1gFnSc2	kůže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jaro	jaro	k1gNnSc1	jaro
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nur	nur	k?	nur
(	(	kIx(	(
<g/>
jen	jen	k6eAd1	jen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hundo	hundo	k1gNnSc1	hundo
(	(	kIx(	(
<g/>
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bildo	bildo	k1gNnSc1	bildo
(	(	kIx(	(
<g/>
obraz	obraz	k1gInSc1	obraz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
:	:	kIx,	:
birdo	birdo	k1gNnSc1	birdo
(	(	kIx(	(
<g/>
pták	pták	k1gMnSc1	pták
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mitingo	mitingo	k1gNnSc1	mitingo
(	(	kIx(	(
<g/>
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spite	spite	k5eAaPmIp2nP	spite
(	(	kIx(	(
<g/>
navzdory	navzdory	k7c3	navzdory
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
suno	suna	k1gFnSc5	suna
(	(	kIx(	(
<g/>
slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ŝ	ŝ	k?	ŝ
(	(	kIx(	(
<g/>
žralok	žralok	k1gMnSc1	žralok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jes	jes	k?	jes
(	(	kIx(	(
<g/>
ano	ano	k9	ano
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
více	hodně	k6eAd2	hodně
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
fiŝ	fiŝ	k?	fiŝ
(	(	kIx(	(
<g/>
ryba	ryba	k1gFnSc1	ryba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fremda	fremda	k1gMnSc1	fremda
(	(	kIx(	(
<g/>
cizí	cizí	k2eAgMnSc1d1	cizí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
grundo	grundo	k1gNnSc1	grundo
(	(	kIx(	(
<g/>
půda	půda	k1gFnSc1	půda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ofta	ofta	k1gFnSc1	ofta
(	(	kIx(	(
<g/>
častý	častý	k2eAgInSc1d1	častý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
somero	somero	k1gNnSc1	somero
(	(	kIx(	(
<g/>
léto	léto	k1gNnSc1	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ŝ	ŝ	k?	ŝ
(	(	kIx(	(
<g/>
loď	loď	k1gFnSc1	loď
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ze	z	k7c2	z
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
z	z	k7c2	z
polštiny	polština	k1gFnSc2	polština
<g/>
:	:	kIx,	:
ĉ	ĉ	k?	ĉ
(	(	kIx(	(
<g/>
tázací	tázací	k2eAgFnPc1d1	tázací
částice	částice	k1gFnPc1	částice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krado	krado	k1gNnSc1	krado
(	(	kIx(	(
<g/>
mříž	mříž	k1gFnSc1	mříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
luti	luti	k6eAd1	luti
(	(	kIx(	(
<g/>
pájet	pájet	k5eAaImF	pájet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moŝ	moŝ	k?	moŝ
(	(	kIx(	(
<g/>
výsost	výsost	k1gFnSc1	výsost
–	–	k?	–
zdvořilostní	zdvořilostní	k2eAgInSc4d1	zdvořilostní
titul	titul	k1gInSc4	titul
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
ruštiny	ruština	k1gFnSc2	ruština
<g/>
:	:	kIx,	:
barakti	barakt	k5eAaBmF	barakt
(	(	kIx(	(
<g/>
bojovat	bojovat	k5eAaImF	bojovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vosto	vosto	k1gNnSc1	vosto
(	(	kIx(	(
<g/>
ocas	ocas	k1gInSc1	ocas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
češtiny	čeština	k1gFnSc2	čeština
<g/>
:	:	kIx,	:
ne	ne	k9	ne
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roboto	robota	k1gFnSc5	robota
(	(	kIx(	(
<g/>
robot	robot	k1gMnSc1	robot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
knedliko	knedlika	k1gFnSc5	knedlika
(	(	kIx(	(
<g/>
knedlík	knedlík	k1gInSc1	knedlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ĉ	ĉ	k?	ĉ
(	(	kIx(	(
<g/>
čerpat	čerpat	k5eAaImF	čerpat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
více	hodně	k6eAd2	hodně
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
klopodi	klopodit	k5eAaPmRp2nS	klopodit
(	(	kIx(	(
<g/>
usilovat	usilovat	k5eAaImF	usilovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prava	prava	k1gFnSc1	prava
(	(	kIx(	(
<g/>
správný	správný	k2eAgInSc1d1	správný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celo	cela	k1gFnSc5	cela
(	(	kIx(	(
<g/>
cíl	cíl	k1gInSc4	cíl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
serpo	serpa	k1gFnSc5	serpa
(	(	kIx(	(
<g/>
srp	srp	k1gInSc1	srp
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
:	:	kIx,	:
hepato	hepato	k6eAd1	hepato
(	(	kIx(	(
<g/>
játra	játra	k1gNnPc1	játra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kaj	kát	k5eAaImRp2nS	kát
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biologio	biologio	k1gMnSc1	biologio
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politiko	politika	k1gFnSc5	politika
(	(	kIx(	(
<g/>
politika	politika	k1gFnSc1	politika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
litevštiny	litevština	k1gFnSc2	litevština
<g/>
:	:	kIx,	:
du	du	k?	du
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ju	ju	k0	ju
(	(	kIx(	(
<g/>
čím	čí	k3xOyQgNnSc7	čí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tuj	tuj	k?	tuj
(	(	kIx(	(
<g/>
hned	hned	k6eAd1	hned
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ze	z	k7c2	z
sanskrtu	sanskrt	k1gInSc2	sanskrt
<g/>
:	:	kIx,	:
budho	budze	k6eAd1	budze
(	(	kIx(	(
<g/>
budha	budha	k1gFnSc1	budha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nirvano	nirvana	k1gFnSc5	nirvana
(	(	kIx(	(
<g/>
nirvána	nirvána	k1gFnSc1	nirvána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pado	pado	k1gMnSc1	pado
(	(	kIx(	(
<g/>
stezka	stezka	k1gFnSc1	stezka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
ugrofinské	ugrofinský	k2eAgFnSc2d1	ugrofinská
skupiny	skupina	k1gFnSc2	skupina
uralských	uralský	k2eAgInPc2d1	uralský
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
ze	z	k7c2	z
sámštiny	sámština	k1gFnSc2	sámština
<g/>
:	:	kIx,	:
boaco	boaco	k1gMnSc1	boaco
(	(	kIx(	(
<g/>
sob	sob	k1gMnSc1	sob
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jojko	jojko	k1gNnSc1	jojko
(	(	kIx(	(
<g/>
tradiční	tradiční	k2eAgInSc1d1	tradiční
laponský	laponský	k2eAgInSc1d1	laponský
hrdelní	hrdelní	k2eAgInSc1d1	hrdelní
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
finštiny	finština	k1gFnSc2	finština
<g/>
:	:	kIx,	:
lirli	lirnout	k5eAaPmAgMnP	lirnout
(	(	kIx(	(
<g/>
zurčet	zurčet	k5eAaImF	zurčet
<g/>
,	,	kIx,	,
klokotat	klokotat	k5eAaImF	klokotat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
saŭ	saŭ	k?	saŭ
(	(	kIx(	(
<g/>
sauna	sauna	k1gFnSc1	sauna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
maďarštiny	maďarština	k1gFnSc2	maďarština
<g/>
:	:	kIx,	:
ĉ	ĉ	k?	ĉ
(	(	kIx(	(
<g/>
čepice	čepice	k1gFnSc1	čepice
ke	k	k7c3	k
stejnokroji	stejnokroj	k1gInSc3	stejnokroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ĉ	ĉ	k?	ĉ
(	(	kIx(	(
<g/>
čardáš	čardáš	k1gInSc1	čardáš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ĉ	ĉ	k?	ĉ
(	(	kIx(	(
<g/>
lidské	lidský	k2eAgNnSc4d1	lidské
semeno	semeno	k1gNnSc4	semeno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ze	z	k7c2	z
semitských	semitský	k2eAgInPc2d1	semitský
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
z	z	k7c2	z
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
:	:	kIx,	:
kabalo	kabala	k1gFnSc5	kabala
(	(	kIx(	(
<g/>
kabala	kabala	k1gFnSc1	kabala
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
arabštiny	arabština	k1gFnSc2	arabština
<g/>
:	:	kIx,	:
kadio	kadio	k1gMnSc1	kadio
(	(	kIx(	(
<g/>
kádí	kádí	k1gMnSc1	kádí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aŭ	aŭ	k?	aŭ
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
z	z	k7c2	z
japonštiny	japonština	k1gFnSc2	japonština
<g/>
:	:	kIx,	:
cunamo	cunamo	k6eAd1	cunamo
(	(	kIx(	(
<g/>
tsunami	tsunami	k1gNnSc7	tsunami
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hajko	hajka	k1gMnSc5	hajka
(	(	kIx(	(
<g/>
hajku	hajek	k1gMnSc6	hajek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zeno	zeno	k1gMnSc1	zeno
(	(	kIx(	(
<g/>
zen	zen	k2eAgMnSc1d1	zen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
čínštiny	čínština	k1gFnSc2	čínština
<g/>
:	:	kIx,	:
tofuo	tofuo	k1gNnSc1	tofuo
(	(	kIx(	(
<g/>
tofu	tofu	k1gNnSc1	tofu
<g/>
)	)	kIx)	)
<g/>
Zamenhof	Zamenhof	k1gInSc1	Zamenhof
dále	daleko	k6eAd2	daleko
také	také	k9	také
pečlivě	pečlivě	k6eAd1	pečlivě
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
původních	původní	k2eAgInPc2d1	původní
slovních	slovní	k2eAgInPc2d1	slovní
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
především	především	k6eAd1	především
předpon	předpona	k1gFnPc2	předpona
a	a	k8xC	a
přípon	přípona	k1gFnPc2	přípona
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
valná	valný	k2eAgFnSc1d1	valná
část	část	k1gFnSc1	část
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
plynně	plynně	k6eAd1	plynně
již	již	k6eAd1	již
po	po	k7c6	po
naučení	naučení	k1gNnSc6	naučení
nevelkého	velký	k2eNgInSc2d1	nevelký
počtu	počet	k1gInSc2	počet
kořenů	kořen	k1gInPc2	kořen
(	(	kIx(	(
<g/>
asi	asi	k9	asi
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
předpon	předpona	k1gFnPc2	předpona
a	a	k8xC	a
přípon	přípona	k1gFnPc2	přípona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
snažil	snažit	k5eAaImAgMnS	snažit
esperanto	esperanto	k1gNnSc4	esperanto
internacionalizovat	internacionalizovat	k5eAaBmF	internacionalizovat
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
slovní	slovní	k2eAgFnSc3d1	slovní
zásobě	zásoba	k1gFnSc3	zásoba
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
evropské	evropský	k2eAgFnSc2d1	Evropská
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rys	rys	k1gInSc1	rys
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
výlučně	výlučně	k6eAd1	výlučně
u	u	k7c2	u
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
:	:	kIx,	:
většina	většina	k1gFnSc1	většina
projektů	projekt	k1gInPc2	projekt
umělých	umělý	k2eAgFnPc2d1	umělá
řečí	řeč	k1gFnPc2	řeč
užívá	užívat	k5eAaImIp3nS	užívat
společné	společný	k2eAgInPc4d1	společný
evropské	evropský	k2eAgInPc4d1	evropský
slovní	slovní	k2eAgInPc4d1	slovní
kmeny	kmen	k1gInPc4	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
rozdílem	rozdíl	k1gInSc7	rozdíl
mezi	mezi	k7c7	mezi
esperantem	esperanto	k1gNnSc7	esperanto
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
plánovanými	plánovaný	k2eAgInPc7d1	plánovaný
jazyky	jazyk	k1gInPc7	jazyk
je	být	k5eAaImIp3nS	být
neevropskost	neevropskost	k1gFnSc4	neevropskost
jeho	jeho	k3xOp3gFnSc2	jeho
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
snažil	snažit	k5eAaImAgMnS	snažit
především	především	k6eAd1	především
<g/>
.	.	kIx.	.
</s>
<s>
Gramatická	gramatický	k2eAgNnPc1d1	gramatické
slovíčka	slovíčko	k1gNnPc1	slovíčko
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
přípony	přípona	k1gFnPc4	přípona
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
uzpůsobena	uzpůsobit	k5eAaPmNgFnS	uzpůsobit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
Evropané	Evropan	k1gMnPc1	Evropan
cizost	cizost	k1gFnSc4	cizost
gramatiky	gramatika	k1gFnSc2	gramatika
ani	ani	k8xC	ani
neuvědomují	uvědomovat	k5eNaImIp3nP	uvědomovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gramatika	gramatika	k1gFnSc1	gramatika
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
====	====	k?	====
</s>
</p>
<p>
<s>
Podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
koncovku	koncovka	k1gFnSc4	koncovka
-o	-o	k?	-o
<g/>
,	,	kIx,	,
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
-	-	kIx~	-
<g/>
j.	j.	k?	j.
Esperanto	esperanto	k1gNnSc1	esperanto
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc1	dva
mluvnické	mluvnický	k2eAgInPc1d1	mluvnický
pády	pád	k1gInPc1	pád
<g/>
:	:	kIx,	:
nominativ	nominativ	k1gInSc1	nominativ
(	(	kIx(	(
<g/>
nominativo	nominativa	k1gFnSc5	nominativa
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
českému	český	k2eAgMnSc3d1	český
1	[number]	k4	1
<g/>
.	.	kIx.	.
pádu	pád	k1gInSc2	pád
<g/>
)	)	kIx)	)
a	a	k8xC	a
akuzativ	akuzativ	k1gInSc1	akuzativ
(	(	kIx(	(
<g/>
akuzativo	akuzativa	k1gFnSc5	akuzativa
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
českému	český	k2eAgMnSc3d1	český
4	[number]	k4	4
<g/>
.	.	kIx.	.
pádu	pád	k1gInSc2	pád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Akuzativ	akuzativ	k1gInSc1	akuzativ
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
připojením	připojení	k1gNnSc7	připojení
koncovky	koncovka	k1gFnSc2	koncovka
-n	-n	k?	-n
<g/>
:	:	kIx,	:
la	la	k1gNnSc6	la
patro	patro	k1gNnSc4	patro
–	–	k?	–
la	la	k1gNnSc2	la
patron	patrona	k1gFnPc2	patrona
<g/>
,	,	kIx,	,
la	la	k1gNnSc1	la
patroj	patroj	k1gInSc1	patroj
–	–	k?	–
la	la	k1gNnSc2	la
patrojn	patrojna	k1gFnPc2	patrojna
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
mluvnické	mluvnický	k2eAgInPc1d1	mluvnický
pády	pád	k1gInPc1	pád
se	se	k3xPyFc4	se
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
pomocí	pomocí	k7c2	pomocí
předložek	předložka	k1gFnPc2	předložka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
kun	kuna	k1gFnPc2	kuna
la	la	k1gNnSc6	la
patro	patro	k1gNnSc1	patro
–	–	k?	–
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
al	ala	k1gFnPc2	ala
la	la	k1gNnSc6	la
patro	patro	k1gNnSc1	patro
–	–	k?	–
k	k	k7c3	k
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
de	de	k?	de
la	la	k1gNnSc6	la
patro	patro	k1gNnSc1	patro
–	–	k?	–
od	od	k7c2	od
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
====	====	k?	====
</s>
</p>
<p>
<s>
Přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
koncovku	koncovka	k1gFnSc4	koncovka
-a	-a	k?	-a
a	a	k8xC	a
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
stupeň	stupeň	k1gInSc1	stupeň
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
pomocí	pomocí	k7c2	pomocí
příslovce	příslovce	k1gNnSc2	příslovce
pli	pli	k?	pli
–	–	k?	–
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
pomocí	pomocí	k7c2	pomocí
příslovce	příslovce	k1gNnSc2	příslovce
plej	plít	k5eAaImRp2nS	plít
–	–	k?	–
nejvíce	nejvíce	k6eAd1	nejvíce
<g/>
;	;	kIx,	;
slovo	slovo	k1gNnSc4	slovo
než	než	k8xS	než
se	se	k3xPyFc4	se
překládá	překládat	k5eAaImIp3nS	překládat
jako	jako	k9	jako
ol	ol	k?	ol
<g/>
:	:	kIx,	:
např.	např.	kA	např.
pli	pli	k?	pli
blanka	blanka	k1gFnSc1	blanka
ol	ol	k?	ol
neĝ	neĝ	k?	neĝ
–	–	k?	–
bělejší	bílý	k2eAgMnSc1d2	bělejší
než	než	k8xS	než
sníh	sníh	k1gInSc1	sníh
<g/>
;	;	kIx,	;
la	la	k1gNnSc1	la
plej	plít	k5eAaImRp2nS	plít
blanka	blanka	k1gFnSc1	blanka
–	–	k?	–
nejbělejší	bílý	k2eAgFnSc1d3	nejbělejší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zájmena	zájmeno	k1gNnSc2	zájmeno
====	====	k?	====
</s>
</p>
<p>
<s>
Osobní	osobní	k2eAgNnPc1d1	osobní
zájmena	zájmeno	k1gNnPc1	zájmeno
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
mi	já	k3xPp1nSc3	já
–	–	k?	–
já	já	k3xPp1nSc1	já
<g/>
;	;	kIx,	;
vi	vi	k?	vi
–	–	k?	–
ty	ty	k3xPp2nSc5	ty
<g/>
,	,	kIx,	,
vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
Vy	vy	k3xPp2nPc1	vy
<g/>
;	;	kIx,	;
li	li	k8xS	li
–	–	k?	–
on	on	k3xPp3gMnSc1	on
<g/>
;	;	kIx,	;
ŝ	ŝ	k?	ŝ
–	–	k?	–
ona	onen	k3xDgFnSc1	onen
<g/>
;	;	kIx,	;
ĝ	ĝ	k?	ĝ
–	–	k?	–
ono	onen	k3xDgNnSc4	onen
<g/>
;	;	kIx,	;
si	se	k3xPyFc3	se
–	–	k?	–
se	s	k7c7	s
<g/>
;	;	kIx,	;
ni	on	k3xPp3gFnSc4	on
–	–	k?	–
my	my	k3xPp1nPc1	my
<g/>
;	;	kIx,	;
ili	ili	k?	ili
–	–	k?	–
oni	onen	k3xDgMnPc1	onen
<g/>
,	,	kIx,	,
ony	onen	k3xDgFnPc1	onen
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	-li	k?	-li
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
osobě	osoba	k1gFnSc6	osoba
třeba	třeba	k6eAd1	třeba
rozlišit	rozlišit	k5eAaPmF	rozlišit
jednotné	jednotný	k2eAgNnSc4d1	jednotné
a	a	k8xC	a
množné	množný	k2eAgNnSc4d1	množné
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
pro	pro	k7c4	pro
jednotné	jednotný	k2eAgNnSc4d1	jednotné
užít	užít	k5eAaPmF	užít
zájmeno	zájmeno	k1gNnSc4	zájmeno
ci	ci	k0	ci
–	–	k?	–
ty	ten	k3xDgInPc4	ten
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
však	však	k9	však
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Zájmeno	zájmeno	k1gNnSc1	zájmeno
oni	onen	k3xDgMnPc1	onen
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
neurčitého	určitý	k2eNgInSc2d1	neurčitý
podmětu	podmět	k1gInSc2	podmět
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
německé	německý	k2eAgFnSc2d1	německá
man	mana	k1gFnPc2	mana
či	či	k8xC	či
francouzské	francouzský	k2eAgFnSc2d1	francouzská
on	on	k3xPp3gMnSc1	on
<g/>
:	:	kIx,	:
oni	onen	k3xDgMnPc1	onen
diras	diras	k1gInSc1	diras
–	–	k?	–
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přivlastňovací	přivlastňovací	k2eAgNnPc1d1	přivlastňovací
zájmena	zájmeno	k1gNnPc1	zájmeno
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
koncovkou	koncovka	k1gFnSc7	koncovka
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
-a	-a	k?	-a
<g/>
:	:	kIx,	:
mia	mia	k?	mia
–	–	k?	–
můj	můj	k1gMnSc1	můj
<g/>
.	.	kIx.	.
</s>
<s>
Zájmena	zájmeno	k1gNnPc1	zájmeno
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
<g/>
:	:	kIx,	:
min	mina	k1gFnPc2	mina
–	–	k?	–
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
mne	já	k3xPp1nSc4	já
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc4d1	další
druhy	druh	k1gInPc4	druh
zájmen	zájmeno	k1gNnPc2	zájmeno
jsou	být	k5eAaImIp3nP	být
realizovány	realizovat	k5eAaBmNgFnP	realizovat
jako	jako	k9	jako
souvztažná	souvztažný	k2eAgNnPc4d1	souvztažné
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Číslovky	číslovka	k1gFnSc2	číslovka
====	====	k?	====
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnPc1d1	základní
číslovky	číslovka	k1gFnPc1	číslovka
jsou	být	k5eAaImIp3nP	být
nesklonné	sklonný	k2eNgNnSc1d1	nesklonné
<g/>
:	:	kIx,	:
1	[number]	k4	1
–	–	k?	–
unu	unu	k?	unu
<g/>
,	,	kIx,	,
2	[number]	k4	2
–	–	k?	–
du	du	k?	du
<g/>
,	,	kIx,	,
3	[number]	k4	3
–	–	k?	–
tri	tri	k?	tri
<g/>
,	,	kIx,	,
4	[number]	k4	4
–	–	k?	–
kvar	kvar	k1gMnSc1	kvar
<g/>
,	,	kIx,	,
5	[number]	k4	5
–	–	k?	–
kvin	kvin	k1gMnSc1	kvin
<g/>
,	,	kIx,	,
6	[number]	k4	6
–	–	k?	–
ses	ses	k?	ses
<g/>
,	,	kIx,	,
7	[number]	k4	7
–	–	k?	–
sep	sep	k?	sep
<g/>
,	,	kIx,	,
8	[number]	k4	8
–	–	k?	–
ok	oka	k1gFnPc2	oka
<g/>
,	,	kIx,	,
9	[number]	k4	9
–	–	k?	–
naŭ	naŭ	k?	naŭ
<g/>
,	,	kIx,	,
10	[number]	k4	10
–	–	k?	–
dek	deka	k1gFnPc2	deka
<g/>
,	,	kIx,	,
100	[number]	k4	100
–	–	k?	–
cent	cent	k1gInSc1	cent
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
–	–	k?	–
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnSc2	číslo
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
prostým	prostý	k2eAgNnSc7d1	prosté
řazením	řazení	k1gNnSc7	řazení
číslovek	číslovka	k1gFnPc2	číslovka
<g/>
:	:	kIx,	:
5432	[number]	k4	5432
–	–	k?	–
kvin	kvin	k1gInSc1	kvin
mil	míle	k1gFnPc2	míle
kvarcent	kvarcent	k1gMnSc1	kvarcent
tridek	tridek	k1gMnSc1	tridek
du	du	k?	du
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řadové	řadový	k2eAgFnPc1d1	řadová
číslovky	číslovka	k1gFnPc1	číslovka
nesou	nést	k5eAaImIp3nP	nést
koncovku	koncovka	k1gFnSc4	koncovka
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
kvara	kvara	k1gFnSc1	kvara
–	–	k?	–
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zlomky	zlomek	k1gInPc1	zlomek
mají	mít	k5eAaImIp3nP	mít
příponu	přípona	k1gFnSc4	přípona
-on-	-on-	k?	-on-
<g/>
:	:	kIx,	:
kvarono	kvarona	k1gFnSc5	kvarona
–	–	k?	–
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souborové	souborový	k2eAgFnPc1d1	souborová
číslovky	číslovka	k1gFnPc1	číslovka
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
příponou	přípona	k1gFnSc7	přípona
-op-	-op-	k?	-op-
<g/>
:	:	kIx,	:
kvaropo	kvaropa	k1gFnSc5	kvaropa
–	–	k?	–
čtveřice	čtveřice	k1gFnSc1	čtveřice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Násobné	násobný	k2eAgFnPc4d1	násobná
číslovky	číslovka	k1gFnPc4	číslovka
označuje	označovat	k5eAaImIp3nS	označovat
přípona	přípona	k1gFnSc1	přípona
-obl-	-obl-	k?	-obl-
<g/>
:	:	kIx,	:
kvarobla	kvarobnout	k5eAaPmAgFnS	kvarobnout
–	–	k?	–
čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1	čtyřnásobný
<g/>
,	,	kIx,	,
kvaroble	kvaroble	k6eAd1	kvaroble
–	–	k?	–
čtyřnásobně	čtyřnásobně	k6eAd1	čtyřnásobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podílné	podílný	k2eAgFnPc1d1	podílná
číslovky	číslovka	k1gFnPc1	číslovka
následují	následovat	k5eAaImIp3nP	následovat
po	po	k7c6	po
předložce	předložka	k1gFnSc6	předložka
po	po	k7c4	po
<g/>
:	:	kIx,	:
po	po	k7c4	po
kvar	kvar	k1gInSc4	kvar
–	–	k?	–
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslovky	číslovka	k1gFnPc1	číslovka
mohou	moct	k5eAaImIp3nP	moct
též	též	k9	též
nést	nést	k5eAaImF	nést
koncovku	koncovka	k1gFnSc4	koncovka
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
či	či	k8xC	či
příslovce	příslovce	k1gNnSc2	příslovce
<g/>
:	:	kIx,	:
kvaro	kvaro	k6eAd1	kvaro
–	–	k?	–
čtyřka	čtyřka	k1gFnSc1	čtyřka
<g/>
,	,	kIx,	,
kvare	kvar	k1gInSc5	kvar
–	–	k?	–
za	za	k7c4	za
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Slovesa	sloveso	k1gNnSc2	sloveso
====	====	k?	====
</s>
</p>
<p>
<s>
Sloveso	sloveso	k1gNnSc1	sloveso
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
ani	ani	k8xC	ani
v	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
ani	ani	k8xC	ani
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
<g/>
:	:	kIx,	:
mi	já	k3xPp1nSc3	já
faras	faras	k1gInSc1	faras
–	–	k?	–
dělám	dělat	k5eAaImIp1nS	dělat
<g/>
,	,	kIx,	,
la	la	k1gNnSc1	la
patro	patro	k1gNnSc1	patro
faras	faras	k1gMnSc1	faras
–	–	k?	–
otec	otec	k1gMnSc1	otec
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
,	,	kIx,	,
ili	ili	k?	ili
faras	faras	k1gInSc1	faras
–	–	k?	–
oni	onen	k3xDgMnPc1	onen
<g/>
/	/	kIx~	/
<g/>
ony	onen	k3xDgFnPc1	onen
<g/>
/	/	kIx~	/
<g/>
ona	onen	k3xDgNnPc4	onen
dělají	dělat	k5eAaImIp3nP	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
má	mít	k5eAaImIp3nS	mít
koncovku	koncovka	k1gFnSc4	koncovka
-as	-as	k?	-as
<g/>
,	,	kIx,	,
minulý	minulý	k2eAgInSc4d1	minulý
-is	-iss	k?	-is
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
-os	-os	k?	-os
<g/>
,	,	kIx,	,
podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
způsob	způsob	k1gInSc1	způsob
-us	-us	k?	-us
<g/>
,	,	kIx,	,
rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
způsob	způsob	k1gInSc1	způsob
-u	-u	k?	-u
<g/>
,	,	kIx,	,
neurčitý	určitý	k2eNgInSc1d1	neurčitý
způsob	způsob	k1gInSc1	způsob
-	-	kIx~	-
<g/>
i.	i.	k?	i.
</s>
</p>
<p>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
a	a	k8xC	a
přechodníky	přechodník	k1gInPc1	přechodník
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
větší	veliký	k2eAgFnSc4d2	veliký
rozlišovací	rozlišovací	k2eAgFnSc4d1	rozlišovací
schopnost	schopnost	k1gFnSc4	schopnost
než	než	k8xS	než
v	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
šest	šest	k4xCc4	šest
příčestí	příčestí	k1gNnPc2	příčestí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
přítomné	přítomný	k2eAgInPc1d1	přítomný
činné	činný	k2eAgInPc1d1	činný
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
příponou	přípona	k1gFnSc7	přípona
-ant-	-ant-	k?	-ant-
<g/>
:	:	kIx,	:
faranta	farant	k1gMnSc4	farant
–	–	k?	–
dělající	dělající	k2eAgNnSc1d1	dělající
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
minulé	minulý	k2eAgInPc1d1	minulý
činné	činný	k2eAgInPc1d1	činný
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
příponou	přípona	k1gFnSc7	přípona
-int-	-int-	k?	-int-
<g/>
:	:	kIx,	:
farinta	farint	k1gMnSc4	farint
–	–	k?	–
dělavší	dělavší	k2eAgNnSc1d1	dělavší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
budoucí	budoucí	k2eAgInPc1d1	budoucí
činné	činný	k2eAgInPc1d1	činný
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
příponou	přípona	k1gFnSc7	přípona
-ont-	-ont-	k?	-ont-
<g/>
:	:	kIx,	:
faronta	faront	k1gMnSc4	faront
–	–	k?	–
hodlající	hodlající	k2eAgNnSc1d1	hodlající
dělat	dělat	k5eAaImF	dělat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
přítomné	přítomný	k2eAgInPc1d1	přítomný
trpné	trpný	k2eAgInPc1d1	trpný
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
příponou	přípona	k1gFnSc7	přípona
-at-	-at-	k?	-at-
<g/>
:	:	kIx,	:
farata	farata	k1gFnSc1	farata
–	–	k?	–
dělán	dělán	k2eAgInSc1d1	dělán
<g/>
,	,	kIx,	,
dělaný	dělaný	k2eAgInSc1d1	dělaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
minulé	minulý	k2eAgFnSc2d1	minulá
trpné	trpný	k2eAgFnSc2d1	trpná
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
příponou	přípona	k1gFnSc7	přípona
-it-	-it-	k?	-it-
<g/>
:	:	kIx,	:
farita	farita	k1gMnSc1	farita
–	–	k?	–
udělán	udělán	k2eAgMnSc1d1	udělán
<g/>
,	,	kIx,	,
udělaný	udělaný	k2eAgInSc1d1	udělaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
budoucí	budoucí	k2eAgFnSc2d1	budoucí
trpné	trpný	k2eAgFnSc2d1	trpná
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
příponou	přípona	k1gFnSc7	přípona
-ot-	-ot-	k?	-ot-
<g/>
:	:	kIx,	:
farota	farot	k1gMnSc4	farot
–	–	k?	–
mající	mající	k2eAgNnSc1d1	mající
být	být	k5eAaImF	být
dělán	dělán	k2eAgMnSc1d1	dělán
<g/>
.	.	kIx.	.
<g/>
Přechodníky	přechodník	k1gInPc1	přechodník
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
použitím	použití	k1gNnSc7	použití
příslovečné	příslovečný	k2eAgFnSc2d1	příslovečná
koncovky	koncovka	k1gFnSc2	koncovka
-e	-e	k?	-e
za	za	k7c7	za
tvarem	tvar	k1gInSc7	tvar
příčestí	příčestí	k1gNnSc2	příčestí
<g/>
:	:	kIx,	:
farante	farant	k1gMnSc5	farant
–	–	k?	–
dělaje	dělat	k5eAaImSgInS	dělat
<g/>
,	,	kIx,	,
farinte	farint	k1gInSc5	farint
–	–	k?	–
udělav	udělat	k5eAaPmDgInS	udělat
<g/>
,	,	kIx,	,
faronte	faront	k1gInSc5	faront
–	–	k?	–
hodlaje	hodlat	k5eAaImSgInS	hodlat
dělat	dělat	k5eAaImF	dělat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tvary	tvar	k1gInPc1	tvar
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
pomocí	pomocí	k7c2	pomocí
příslušného	příslušný	k2eAgInSc2d1	příslušný
tvaru	tvar	k1gInSc2	tvar
pomocného	pomocný	k2eAgNnSc2d1	pomocné
slovesa	sloveso	k1gNnSc2	sloveso
esti	est	k1gFnSc2	est
–	–	k?	–
být	být	k5eAaImF	být
a	a	k8xC	a
trpného	trpný	k2eAgNnSc2d1	trpné
příčestí	příčestí	k1gNnSc2	příčestí
dotyčného	dotyčný	k2eAgNnSc2d1	dotyčné
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
,	,	kIx,	,
při	při	k7c6	při
čemž	což	k3yQnSc6	což
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
konatele	konatel	k1gMnSc4	konatel
používá	používat	k5eAaImIp3nS	používat
předložky	předložka	k1gFnPc4	předložka
de	de	k?	de
<g/>
:	:	kIx,	:
ŝ	ŝ	k?	ŝ
estas	estas	k1gMnSc1	estas
amata	amata	k1gMnSc1	amata
de	de	k?	de
ĉ	ĉ	k?	ĉ
–	–	k?	–
(	(	kIx(	(
<g/>
ona	onen	k3xDgFnSc1	onen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
milována	milován	k2eAgFnSc1d1	milována
všemi	všecek	k3xTgFnPc7	všecek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Souvztažná	souvztažný	k2eAgNnPc1d1	souvztažné
slova	slovo	k1gNnPc1	slovo
====	====	k?	====
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
souvztažná	souvztažný	k2eAgNnPc4d1	souvztažné
slova	slovo	k1gNnPc4	slovo
patří	patřit	k5eAaImIp3nS	patřit
korelativní	korelativní	k2eAgInSc1d1	korelativní
zájmena	zájmeno	k1gNnSc2	zájmeno
a	a	k8xC	a
zájmenná	zájmenný	k2eAgNnPc1d1	zájmenné
příslovce	příslovce	k1gNnPc1	příslovce
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
jejich	jejich	k3xOp3gNnSc2	jejich
odvozování	odvozování	k1gNnSc2	odvozování
je	být	k5eAaImIp3nS	být
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
a	a	k8xC	a
přehledně	přehledně	k6eAd1	přehledně
jej	on	k3xPp3gMnSc4	on
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Ostatní	ostatní	k2eAgInPc1d1	ostatní
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
a	a	k8xC	a
gramatická	gramatický	k2eAgNnPc1d1	gramatické
pravidla	pravidlo	k1gNnPc1	pravidlo
====	====	k?	====
</s>
</p>
<p>
<s>
Příslovce	příslovce	k1gNnPc1	příslovce
odvozená	odvozený	k2eAgNnPc1d1	odvozené
od	od	k7c2	od
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
mají	mít	k5eAaImIp3nP	mít
koncovku	koncovka	k1gFnSc4	koncovka
-e	-e	k?	-e
<g/>
,	,	kIx,	,
stupňují	stupňovat	k5eAaImIp3nP	stupňovat
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
<g/>
:	:	kIx,	:
laŭ	laŭ	k?	laŭ
–	–	k?	–
hlasitý	hlasitý	k2eAgInSc1d1	hlasitý
<g/>
,	,	kIx,	,
laŭ	laŭ	k?	laŭ
–	–	k?	–
hlasitě	hlasitě	k6eAd1	hlasitě
<g/>
;	;	kIx,	;
mia	mia	k?	mia
frato	frato	k1gNnSc1	frato
kantas	kantas	k1gMnSc1	kantas
pli	pli	k?	pli
bone	bon	k1gInSc5	bon
ol	ol	k?	ol
mi	já	k3xPp1nSc3	já
–	–	k?	–
můj	můj	k1gMnSc1	můj
bratr	bratr	k1gMnSc1	bratr
zpívá	zpívat	k5eAaImIp3nS	zpívat
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
já	já	k3xPp1nSc1	já
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předložky	předložka	k1gFnPc1	předložka
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
pojí	pojit	k5eAaImIp3nP	pojit
s	s	k7c7	s
nominativem	nominativ	k1gInSc7	nominativ
<g/>
;	;	kIx,	;
některé	některý	k3yIgFnSc2	některý
(	(	kIx(	(
<g/>
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
směru	směr	k1gInSc2	směr
<g/>
)	)	kIx)	)
také	také	k9	také
s	s	k7c7	s
akuzativem	akuzativ	k1gInSc7	akuzativ
<g/>
:	:	kIx,	:
en	en	k?	en
la	la	k1gNnSc1	la
domo	domo	k1gMnSc1	domo
–	–	k?	–
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
en	en	k?	en
la	la	k1gNnSc1	la
domon	domon	k1gMnSc1	domon
–	–	k?	–
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odpovědi	odpověď	k1gFnSc6	odpověď
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
"	"	kIx"	"
<g/>
kam	kam	k6eAd1	kam
<g/>
"	"	kIx"	"
lze	lze	k6eAd1	lze
díky	díky	k7c3	díky
použití	použití	k1gNnSc3	použití
akuzativu	akuzativ	k1gInSc3	akuzativ
předložku	předložka	k1gFnSc4	předložka
i	i	k9	i
úplně	úplně	k6eAd1	úplně
vynechat	vynechat	k5eAaPmF	vynechat
<g/>
:	:	kIx,	:
hejmen	hejmen	k1gInSc1	hejmen
–	–	k?	–
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
Pragon	Pragona	k1gFnPc2	Pragona
–	–	k?	–
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
předložka	předložka	k1gFnSc1	předložka
má	mít	k5eAaImIp3nS	mít
určitý	určitý	k2eAgInSc4d1	určitý
a	a	k8xC	a
stálý	stálý	k2eAgInSc4d1	stálý
význam	význam	k1gInSc4	význam
<g/>
;	;	kIx,	;
v	v	k7c6	v
nerozhodném	rozhodný	k2eNgInSc6d1	nerozhodný
případě	případ	k1gInSc6	případ
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
neurčitou	určitý	k2eNgFnSc4d1	neurčitá
předložku	předložka	k1gFnSc4	předložka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
překládána	překládat	k5eAaImNgFnS	překládat
dle	dle	k7c2	dle
kontextu	kontext	k1gInSc2	kontext
různě	různě	k6eAd1	různě
<g/>
:	:	kIx,	:
sopiro	sopiro	k6eAd1	sopiro
je	on	k3xPp3gNnPc4	on
la	la	k1gNnPc4	la
patrujo	patrujo	k6eAd1	patrujo
–	–	k?	–
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
vlasti	vlast	k1gFnSc6	vlast
<g/>
,	,	kIx,	,
Je	být	k5eAaImIp3nS	být
kioma	kioma	k1gFnSc1	kioma
horo	hora	k1gFnSc5	hora
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
V	v	k7c4	v
kolik	kolik	k4yIc4	kolik
hodin	hodina	k1gFnPc2	hodina
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
</p>
<p>
<s>
Spojky	spojka	k1gFnPc1	spojka
jsou	být	k5eAaImIp3nP	být
užívány	užívat	k5eAaImNgFnP	užívat
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ve	v	k7c6	v
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Člen	člen	k1gInSc1	člen
určitý	určitý	k2eAgInSc1d1	určitý
(	(	kIx(	(
<g/>
la	la	k1gNnSc1	la
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
rody	rod	k1gInPc4	rod
a	a	k8xC	a
pády	pád	k1gInPc4	pád
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
jednotném	jednotný	k2eAgNnSc6d1	jednotné
i	i	k8xC	i
množném	množný	k2eAgNnSc6d1	množné
<g/>
.	.	kIx.	.
</s>
<s>
Neurčitý	určitý	k2eNgMnSc1d1	neurčitý
člen	člen	k1gMnSc1	člen
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
fonetická	fonetický	k2eAgFnSc1d1	fonetická
–	–	k?	–
každé	každý	k3xTgNnSc1	každý
slovo	slovo	k1gNnSc1	slovo
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
psáno	psán	k2eAgNnSc1d1	psáno
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
sekci	sekce	k1gFnSc4	sekce
Abeceda	abeceda	k1gFnSc1	abeceda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
víceslabičných	víceslabičný	k2eAgNnPc2d1	víceslabičné
slov	slovo	k1gNnPc2	slovo
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
předposlední	předposlední	k2eAgFnSc6d1	předposlední
slabice	slabika	k1gFnSc6	slabika
<g/>
,	,	kIx,	,
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
:	:	kIx,	:
Mi	já	k3xPp1nSc3	já
ĉ	ĉ	k?	ĉ
povas	povas	k1gMnSc1	povas
fari	far	k1gFnSc2	far
en	en	k?	en
tiu	tiu	k?	tiu
<g/>
,	,	kIx,	,
kiu	kiu	k?	kiu
min	mina	k1gFnPc2	mina
fortikigas	fortikigas	k1gMnSc1	fortikigas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Složená	složený	k2eAgNnPc4d1	složené
slova	slovo	k1gNnPc4	slovo
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
prostým	prostý	k2eAgNnSc7d1	prosté
spojením	spojení	k1gNnSc7	spojení
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
lze	lze	k6eAd1	lze
odvrhnout	odvrhnout	k5eAaPmF	odvrhnout
koncovku	koncovka	k1gFnSc4	koncovka
<g/>
:	:	kIx,	:
skrib-maŝ	skribaŝ	k?	skrib-maŝ
–	–	k?	–
psací	psací	k2eAgInSc1d1	psací
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
maŝ	maŝ	k?	maŝ
–	–	k?	–
strojové	strojový	k2eAgNnSc4d1	strojové
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
písmo	písmo	k1gNnSc4	písmo
psané	psaný	k2eAgFnSc2d1	psaná
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zápor	zápor	k1gInSc1	zápor
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgMnSc7	jeden
záporným	záporný	k2eAgMnSc7d1	záporný
slovem	slovo	k1gNnSc7	slovo
(	(	kIx(	(
<g/>
záporkou	záporka	k1gFnSc7	záporka
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Nikdo	nikdo	k3yNnSc1	nikdo
to	ten	k3xDgNnSc4	ten
neviděl	vidět	k5eNaImAgMnS	vidět
–	–	k?	–
Neniu	Nenius	k1gMnSc6	Nenius
tion	tion	k1gMnSc1	tion
vidis	vidis	k1gFnSc2	vidis
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nesprávně	správně	k6eNd1	správně
<g/>
:	:	kIx,	:
Neniu	Nenius	k1gMnSc6	Nenius
tion	tion	k1gNnSc1	tion
ne	ne	k9	ne
vidis	vidis	k1gInSc1	vidis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
nikdy	nikdy	k6eAd1	nikdy
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgMnSc4	takový
neviděl	vidět	k5eNaImAgMnS	vidět
–	–	k?	–
Neniu	Nenium	k1gNnSc3	Nenium
el	ela	k1gFnPc2	ela
ni	on	k3xPp3gFnSc4	on
iam	iam	k?	iam
ion	ion	k1gInSc1	ion
tian	tian	k1gMnSc1	tian
vidis	vidis	k1gInSc1	vidis
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nesprávně	správně	k6eNd1	správně
<g/>
:	:	kIx,	:
Neniu	Nenium	k1gNnSc3	Nenium
el	ela	k1gFnPc2	ela
ni	on	k3xPp3gFnSc4	on
neniam	niam	k6eNd1	niam
nenion	nenion	k1gInSc1	nenion
tian	tiana	k1gFnPc2	tiana
ne	ne	k9	ne
vidis	vidis	k1gFnSc7	vidis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Takzvaná	takzvaný	k2eAgNnPc1d1	takzvané
cizí	cizí	k2eAgNnPc1d1	cizí
slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
přejímají	přejímat	k5eAaImIp3nP	přejímat
obvykle	obvykle	k6eAd1	obvykle
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
píší	psát	k5eAaImIp3nP	psát
se	se	k3xPyFc4	se
však	však	k9	však
foneticky	foneticky	k6eAd1	foneticky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Srovnání	srovnání	k1gNnSc1	srovnání
esperantského	esperantský	k2eAgInSc2d1	esperantský
a	a	k8xC	a
českého	český	k2eAgInSc2d1	český
textu	text	k1gInSc2	text
==	==	k?	==
</s>
</p>
<p>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
modlitby	modlitba	k1gFnSc2	modlitba
Otče	otec	k1gMnSc5	otec
náš	náš	k3xOp1gMnSc1	náš
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
</s>
</p>
<p>
<s>
==	==	k?	==
Užitečné	užitečný	k2eAgFnPc1d1	užitečná
fráze	fráze	k1gFnPc1	fráze
==	==	k?	==
</s>
</p>
<p>
<s>
Několik	několik	k4yIc1	několik
užitečných	užitečný	k2eAgFnPc2d1	užitečná
frází	fráze	k1gFnPc2	fráze
<g/>
,	,	kIx,	,
s	s	k7c7	s
fonetickou	fonetický	k2eAgFnSc7d1	fonetická
transkripcí	transkripce	k1gFnSc7	transkripce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
slovníkové	slovníkový	k2eAgFnPc1d1	slovníková
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
na	na	k7c6	na
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
esperantu	esperanto	k1gNnSc3	esperanto
a	a	k8xC	a
jiným	jiný	k2eAgInPc3d1	jiný
plánovým	plánový	k2eAgInPc3d1	plánový
jazykům	jazyk	k1gInPc3	jazyk
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
mnoho	mnoho	k4c1	mnoho
argumentů	argument	k1gInPc2	argument
pro	pro	k7c4	pro
i	i	k8xC	i
proti	proti	k7c3	proti
<g/>
.	.	kIx.	.
</s>
<s>
Obecnější	obecní	k2eAgFnSc1d2	obecní
kritika	kritika	k1gFnSc1	kritika
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většinou	k6eAd1	většinou
směrována	směrovat	k5eAaImNgFnS	směrovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
na	na	k7c4	na
nejazykové	jazykový	k2eNgInPc4d1	nejazykový
aspekty	aspekt	k1gInPc4	aspekt
<g/>
:	:	kIx,	:
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dominanci	dominance	k1gFnSc3	dominance
angličtiny	angličtina	k1gFnSc2	angličtina
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
)	)	kIx)	)
nemá	mít	k5eNaImIp3nS	mít
prý	prý	k9	prý
nová	nový	k2eAgFnSc1d1	nová
řeč	řeč	k1gFnSc1	řeč
šanci	šance	k1gFnSc4	šance
tuto	tento	k3xDgFnSc4	tento
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
nadvládu	nadvláda	k1gFnSc4	nadvláda
rozbít	rozbít	k5eAaPmF	rozbít
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
převzít	převzít	k5eAaPmF	převzít
<g/>
;	;	kIx,	;
celosvětové	celosvětový	k2eAgNnSc4d1	celosvětové
zavedení	zavedení	k1gNnSc4	zavedení
esperanta	esperanto	k1gNnSc2	esperanto
by	by	kYmCp3nP	by
také	také	k6eAd1	také
údajně	údajně	k6eAd1	údajně
nebylo	být	k5eNaImAgNnS	být
vůči	vůči	k7c3	vůči
jiným	jiný	k2eAgMnPc3d1	jiný
jazykům	jazyk	k1gMnPc3	jazyk
nijak	nijak	k6eAd1	nijak
přátelštější	přátelský	k2eAgMnSc1d2	přátelštější
<g/>
.	.	kIx.	.
</s>
<s>
Jazykově	jazykově	k6eAd1	jazykově
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
kritika	kritika	k1gFnSc1	kritika
zase	zase	k9	zase
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
uměle	uměle	k6eAd1	uměle
<g/>
"	"	kIx"	"
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
jazyk	jazyk	k1gInSc1	jazyk
není	být	k5eNaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
použití	použití	k1gNnSc4	použití
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
prý	prý	k9	prý
ani	ani	k9	ani
možné	možný	k2eAgNnSc1d1	možné
psát	psát	k5eAaImF	psát
krásnou	krásný	k2eAgFnSc4d1	krásná
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
vůči	vůči	k7c3	vůči
esperantu	esperanto	k1gNnSc3	esperanto
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
namítáno	namítán	k2eAgNnSc1d1	namítáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
natolik	natolik	k6eAd1	natolik
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
plánový	plánový	k2eAgInSc1d1	plánový
jazyk	jazyk	k1gInSc1	jazyk
teoreticky	teoreticky	k6eAd1	teoreticky
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
evropské	evropský	k2eAgNnSc1d1	Evropské
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
řidčeji	řídce	k6eAd2	řídce
také	také	k9	také
nedostatečně	dostatečně	k6eNd1	dostatečně
evropské	evropský	k2eAgFnPc1d1	Evropská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
bývají	bývat	k5eAaImIp3nP	bývat
rovněž	rovněž	k9	rovněž
některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
jeho	jeho	k3xOp3gFnSc2	jeho
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
písmena	písmeno	k1gNnPc4	písmeno
s	s	k7c7	s
diakritickými	diakritický	k2eAgNnPc7d1	diakritické
znaménky	znaménko	k1gNnPc7	znaménko
nebo	nebo	k8xC	nebo
akuzativ	akuzativ	k1gInSc4	akuzativ
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Metaforické	metaforický	k2eAgNnSc1d1	metaforické
použití	použití	k1gNnSc1	použití
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
esperanto	esperanto	k1gNnSc1	esperanto
<g/>
"	"	kIx"	"
==	==	k?	==
</s>
</p>
<p>
<s>
Metaforicky	metaforicky	k6eAd1	metaforicky
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
esperanto	esperanto	k1gNnSc1	esperanto
občas	občas	k6eAd1	občas
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
něčeho	něco	k3yInSc2	něco
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
<g/>
,	,	kIx,	,
zprostředkujícího	zprostředkující	k2eAgMnSc2d1	zprostředkující
<g/>
,	,	kIx,	,
smíšeného	smíšený	k2eAgMnSc2d1	smíšený
<g/>
,	,	kIx,	,
sjednocujícího	sjednocující	k2eAgNnSc2d1	sjednocující
či	či	k8xC	či
neutrálního	neutrální	k2eAgNnSc2d1	neutrální
–	–	k?	–
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Java	Java	k1gFnSc1	Java
označována	označován	k2eAgFnSc1d1	označována
za	za	k7c4	za
"	"	kIx"	"
<g/>
esperanto	esperanto	k1gNnSc4	esperanto
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
Linux	Linux	kA	Linux
za	za	k7c4	za
"	"	kIx"	"
<g/>
esperanto	esperanto	k1gNnSc4	esperanto
světa	svět	k1gInSc2	svět
počítačů	počítač	k1gMnPc2	počítač
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
šiřitelné	šiřitelný	k2eAgNnSc4d1	šiřitelné
písmo	písmo	k1gNnSc4	písmo
Noto	nota	k1gFnSc5	nota
dávající	dávající	k2eAgInSc4d1	dávající
jednotný	jednotný	k2eAgInSc4d1	jednotný
vzhled	vzhled	k1gInSc4	vzhled
textům	text	k1gInPc3	text
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
esperantem	esperanto	k1gNnSc7	esperanto
písem	písmo	k1gNnPc2	písmo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
"	"	kIx"	"
<g/>
daňovém	daňový	k2eAgNnSc6d1	daňové
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
"	"	kIx"	"
spojujícím	spojující	k2eAgMnPc3d1	spojující
nejosvědčenější	osvědčený	k2eAgInPc1d3	nejosvědčenější
prvky	prvek	k1gInPc7	prvek
daňových	daňový	k2eAgFnPc2d1	daňová
soustav	soustava	k1gFnPc2	soustava
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
společnosti	společnost	k1gFnSc2	společnost
Google	Google	k1gNnSc2	Google
přestat	přestat	k5eAaPmF	přestat
v	v	k7c6	v
prohlížeči	prohlížeč	k1gInSc6	prohlížeč
Chrome	chromat	k5eAaImIp3nS	chromat
podporovat	podporovat	k5eAaImF	podporovat
patentovaně	patentovaně	k6eAd1	patentovaně
chráněný	chráněný	k2eAgInSc1d1	chráněný
video	video	k1gNnSc4	video
kodek	kodek	k1gInSc4	kodek
H	H	kA	H
<g/>
.264	.264	k4	.264
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
otevřeného	otevřený	k2eAgInSc2d1	otevřený
formátu	formát	k1gInSc2	formát
WebM	WebM	k1gMnPc2	WebM
bylo	být	k5eAaImAgNnS	být
kritiky	kritik	k1gMnPc4	kritik
z	z	k7c2	z
Microsoftu	Microsoft	k1gInSc2	Microsoft
přirovnáno	přirovnán	k2eAgNnSc1d1	přirovnáno
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
esperanta	esperanto	k1gNnSc2	esperanto
jako	jako	k8xS	jako
úředního	úřední	k2eAgInSc2d1	úřední
jazyka	jazyk	k1gInSc2	jazyk
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
<g/>
Takové	takový	k3xDgFnPc1	takový
analogie	analogie	k1gFnPc1	analogie
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
často	často	k6eAd1	často
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc3	označení
esperantská	esperantský	k2eAgFnSc1d1	esperantská
měna	měna	k1gFnSc1	měna
použité	použitý	k2eAgFnSc2d1	použitá
pro	pro	k7c4	pro
euro	euro	k1gNnSc4	euro
<g/>
:	:	kIx,	:
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
ostatní	ostatní	k2eAgFnPc4d1	ostatní
měny	měna	k1gFnPc4	měna
–	–	k?	–
esperantisté	esperantista	k1gMnPc1	esperantista
však	však	k9	však
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
neusilují	usilovat	k5eNaImIp3nP	usilovat
o	o	k7c6	o
odstranění	odstranění	k1gNnSc6	odstranění
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
esperantem	esperanto	k1gNnSc7	esperanto
však	však	k9	však
euro	euro	k1gNnSc1	euro
sdílí	sdílet	k5eAaImIp3nS	sdílet
svou	svůj	k3xOyFgFnSc4	svůj
neutralitu	neutralita	k1gFnSc4	neutralita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
v	v	k7c6	v
sobě	se	k3xPyFc3	se
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
informace	informace	k1gFnPc4	informace
čerpané	čerpaný	k2eAgFnPc4d1	čerpaná
z	z	k7c2	z
dokumentu	dokument	k1gInSc2	dokument
"	"	kIx"	"
<g/>
Ĝ	Ĝ	k?	Ĝ
pri	pri	k?	pri
Esperanto	esperanto	k1gNnSc4	esperanto
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Aktuálně	aktuálně	k6eAd1	aktuálně
o	o	k7c6	o
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
svolením	svolení	k1gNnSc7	svolení
jeho	jeho	k3xOp3gMnSc2	jeho
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
,	,	kIx,	,
Světového	světový	k2eAgInSc2d1	světový
esperantského	esperantský	k2eAgInSc2d1	esperantský
svazu	svaz	k1gInSc2	svaz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Universala	Universat	k5eAaPmAgFnS	Universat
Esperanto-Asocio	Esperanto-Asocio	k6eAd1	Esperanto-Asocio
<g/>
.	.	kIx.	.
Ĝ	Ĝ	k?	Ĝ
pri	pri	k?	pri
Esperanto	esperanto	k1gNnSc4	esperanto
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Universala	Universat	k5eAaImAgFnS	Universat
Esperanto-Asocio	Esperanto-Asocio	k6eAd1	Esperanto-Asocio
<g/>
,	,	kIx,	,
2002-12-03	[number]	k4	2002-12-03
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Lazar	Lazar	k1gMnSc1	Lazar
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
</s>
</p>
<p>
<s>
lernu	lernout	k5eAaPmIp1nS	lernout
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
esperantský	esperantský	k2eAgInSc1d1	esperantský
svaz	svaz	k1gInSc1	svaz
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
esperantská	esperantský	k2eAgFnSc1d1	esperantská
mládež	mládež	k1gFnSc1	mládež
</s>
</p>
<p>
<s>
Pasporta	Pasporta	k1gFnSc1	Pasporta
Servo	Servo	k1gNnSc1	Servo
</s>
</p>
<p>
<s>
Esperantská	esperantský	k2eAgFnSc1d1	esperantská
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
esperanto	esperanto	k1gNnSc4	esperanto
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
Úplná	úplný	k2eAgFnSc1d1	úplná
učebnice	učebnice	k1gFnSc1	učebnice
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
řeči	řeč	k1gFnSc2	řeč
dra	dřít	k5eAaImSgInS	dřít
<g/>
.	.	kIx.	.
</s>
<s>
Esperanta	esperanto	k1gNnPc1	esperanto
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
esperanto	esperanto	k1gNnSc4	esperanto
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Esperanto	esperanto	k1gNnSc4	esperanto
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Esperanto	esperanto	k1gNnSc1	esperanto
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Esperanto	esperanto	k1gNnSc1	esperanto
je	být	k5eAaImIp3nS	být
<g/>
...	...	k?	...
–	–	k?	–
sedm	sedm	k4xCc4	sedm
krátkých	krátká	k1gFnPc2	krátká
videí	video	k1gNnPc2	video
o	o	k7c6	o
různých	různý	k2eAgInPc6d1	různý
rysech	rys	k1gInPc6	rys
esperanta	esperanto	k1gNnSc2	esperanto
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
lernu	lernout	k5eAaImIp1nS	lernout
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
web	web	k1gInSc1	web
pro	pro	k7c4	pro
snadné	snadný	k2eAgNnSc4d1	snadné
a	a	k8xC	a
bezplatné	bezplatný	k2eAgNnSc4d1	bezplatné
studium	studium	k1gNnSc4	studium
esperanta	esperanto	k1gNnSc2	esperanto
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Český	český	k2eAgInSc1d1	český
esperantský	esperantský	k2eAgInSc1d1	esperantský
svaz	svaz	k1gInSc1	svaz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
esperantská	esperantský	k2eAgFnSc1d1	esperantská
mládež	mládež	k1gFnSc1	mládež
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
v	v	k7c6	v
esperantu	esperanto	k1gNnSc6	esperanto
<g/>
)	)	kIx)	)
Pasporta	Pasporta	k1gFnSc1	Pasporta
Servo	Servo	k1gNnSc1	Servo
–	–	k?	–
bezplatná	bezplatný	k2eAgFnSc1d1	bezplatná
hostitelská	hostitelský	k2eAgFnSc1d1	hostitelská
síť	síť	k1gFnSc1	síť
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
esperanta	esperanto	k1gNnSc2	esperanto
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Učebnice	učebnice	k1gFnSc1	učebnice
esperanta	esperanto	k1gNnSc2	esperanto
(	(	kIx(	(
<g/>
lernolibro	lernolibro	k6eAd1	lernolibro
de	de	k?	de
esperanto	esperanto	k1gNnSc4	esperanto
<g/>
)	)	kIx)	)
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
učebnice	učebnice	k1gFnSc1	učebnice
esperanta	esperanto	k1gNnSc2	esperanto
pro	pro	k7c4	pro
samouky	samouk	k1gMnPc4	samouk
i	i	k8xC	i
kluby	klub	k1gInPc4	klub
a	a	k8xC	a
tábory	tábor	k1gInPc4	tábor
od	od	k7c2	od
Theodora	Theodor	k1gMnSc2	Theodor
Kiliána	Kilián	k1gMnSc2	Kilián
</s>
</p>
