<s>
Želvy	želva	k1gFnPc1	želva
(	(	kIx(	(
<g/>
Testudines	Testudines	k1gInSc1	Testudines
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
plazů	plaz	k1gMnPc2	plaz
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
především	především	k9	především
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
obvykle	obvykle	k6eAd1	obvykle
chráněno	chránit	k5eAaImNgNnS	chránit
kostěným	kostěný	k2eAgInSc7d1	kostěný
krunýřem	krunýř	k1gInSc7	krunýř
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
hřbetní	hřbetní	k2eAgInSc4d1	hřbetní
(	(	kIx(	(
<g/>
karapax	karapax	k1gInSc4	karapax
<g/>
)	)	kIx)	)
a	a	k8xC	a
břišní	břišní	k2eAgInSc1d1	břišní
(	(	kIx(	(
<g/>
plastron	plastron	k1gInSc1	plastron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
jejich	jejich	k3xOp3gNnPc2	jejich
žeber	žebro	k1gNnPc2	žebro
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
několik	několik	k4yIc1	několik
dnes	dnes	k6eAd1	dnes
již	již	k9	již
vyhynulých	vyhynulý	k2eAgInPc2d1	vyhynulý
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
druhy	druh	k1gInPc1	druh
želv	želva	k1gFnPc2	želva
žily	žít	k5eAaImAgFnP	žít
před	před	k7c4	před
zhruba	zhruba	k6eAd1	zhruba
220	[number]	k4	220
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
v	v	k7c6	v
období	období	k1gNnSc6	období
triasu	trias	k1gInSc2	trias
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Odontochelys	Odontochelysa	k1gFnPc2	Odontochelysa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dělá	dělat	k5eAaImIp3nS	dělat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dosud	dosud	k6eAd1	dosud
žijících	žijící	k2eAgFnPc2d1	žijící
skupin	skupina	k1gFnPc2	skupina
plazů	plaz	k1gMnPc2	plaz
(	(	kIx(	(
<g/>
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ještěři	ještěr	k1gMnPc1	ještěr
nebo	nebo	k8xC	nebo
hadi	had	k1gMnPc1	had
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
asi	asi	k9	asi
300	[number]	k4	300
druhů	druh	k1gInPc2	druh
želv	želva	k1gFnPc2	želva
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
ohrožené	ohrožený	k2eAgFnPc1d1	ohrožená
<g/>
.	.	kIx.	.
</s>
<s>
Želvy	želva	k1gFnPc4	želva
jsou	být	k5eAaImIp3nP	být
studenokrevní	studenokrevní	k2eAgMnPc1d1	studenokrevní
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
typicky	typicky	k6eAd1	typicky
dlouhověké	dlouhověký	k2eAgMnPc4d1	dlouhověký
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
dožili	dožít	k5eAaPmAgMnP	dožít
prokazatelně	prokazatelně	k6eAd1	prokazatelně
i	i	k8xC	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
180	[number]	k4	180
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
např.	např.	kA	např.
želva	želva	k1gFnSc1	želva
sloní	sloní	k2eAgFnSc1d1	sloní
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
plazů	plaz	k1gMnPc2	plaz
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
přibližně	přibližně	k6eAd1	přibližně
osm	osm	k4xCc4	osm
druhů	druh	k1gInPc2	druh
želv	želva	k1gFnPc2	želva
<g/>
.	.	kIx.	.
</s>
<s>
Nejhojnějším	hojný	k2eAgInSc7d3	nejhojnější
druhem	druh	k1gInSc7	druh
želv	želva	k1gFnPc2	želva
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
nepůvodní	původní	k2eNgFnSc1d1	nepůvodní
želva	želva	k1gFnSc1	želva
nádherná	nádherný	k2eAgFnSc1d1	nádherná
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
původní	původní	k2eAgFnSc7d1	původní
(	(	kIx(	(
<g/>
a	a	k8xC	a
i	i	k9	i
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
údajně	údajně	k6eAd1	údajně
vysazená	vysazený	k2eAgFnSc1d1	vysazená
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
želva	želva	k1gFnSc1	želva
bahenní	bahenní	k2eAgFnSc1d1	bahenní
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
želv	želva	k1gFnPc2	želva
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
různí	různit	k5eAaImIp3nP	různit
<g/>
.	.	kIx.	.
</s>
<s>
Mořské	mořský	k2eAgInPc1d1	mořský
druhy	druh	k1gInPc1	druh
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
obrovských	obrovský	k2eAgInPc2d1	obrovský
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sladkovodní	sladkovodní	k2eAgFnPc1d1	sladkovodní
želvy	želva	k1gFnPc1	želva
jsou	být	k5eAaImIp3nP	být
typicky	typicky	k6eAd1	typicky
daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgFnSc1d2	menší
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
zaznamenáni	zaznamenán	k2eAgMnPc1d1	zaznamenán
i	i	k8xC	i
jedinci	jedinec	k1gMnSc3	jedinec
2	[number]	k4	2
m	m	kA	m
dlouzí	dlouhý	k2eAgMnPc1d1	dlouhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Suchozemské	suchozemský	k2eAgFnPc1d1	suchozemská
želvy	želva	k1gFnPc1	želva
mohou	moct	k5eAaImIp3nP	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
až	až	k9	až
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
želv	želva	k1gFnPc2	želva
mořských	mořský	k2eAgFnPc2d1	mořská
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
želvou	želva	k1gFnSc7	želva
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
kožatka	kožatka	k1gFnSc1	kožatka
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
mořská	mořský	k2eAgFnSc1d1	mořská
želva	želva	k1gFnSc1	želva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
váží	vážit	k5eAaImIp3nS	vážit
i	i	k9	i
přes	přes	k7c4	přes
900	[number]	k4	900
kg	kg	kA	kg
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
krunýř	krunýř	k1gInSc1	krunýř
je	být	k5eAaImIp3nS	být
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgFnSc7d3	nejmenší
želvou	želva	k1gFnSc7	želva
je	být	k5eAaImIp3nS	být
Homopus	Homopus	k1gMnSc1	Homopus
signatus	signatus	k1gMnSc1	signatus
signatus	signatus	k1gMnSc1	signatus
(	(	kIx(	(
<g/>
poddruh	poddruh	k1gInSc1	poddruh
želvy	želva	k1gFnSc2	želva
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měří	měřit	k5eAaImIp3nS	měřit
necelých	celý	k2eNgInPc2d1	necelý
8	[number]	k4	8
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
pouhých	pouhý	k2eAgInPc2d1	pouhý
140	[number]	k4	140
g.	g.	k?	g.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
prehistorických	prehistorický	k2eAgFnPc6d1	prehistorická
dobách	doba	k1gFnPc6	doba
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
hojně	hojně	k6eAd1	hojně
rozšířeny	rozšířen	k2eAgFnPc1d1	rozšířena
želvy	želva	k1gFnPc1	želva
sloní	slonit	k5eAaImIp3nP	slonit
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
avšak	avšak	k8xC	avšak
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
člověka	člověk	k1gMnSc2	člověk
tento	tento	k3xDgMnSc1	tento
druh	druh	k1gMnSc1	druh
téměř	téměř	k6eAd1	téměř
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
lidé	člověk	k1gMnPc1	člověk
lovili	lovit	k5eAaImAgMnP	lovit
jako	jako	k9	jako
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
můžeme	moct	k5eAaImIp1nP	moct
želvy	želva	k1gFnPc1	želva
sloní	sloní	k2eAgFnPc1d1	sloní
najít	najít	k5eAaPmF	najít
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Seychelách	Seychely	k1gFnPc6	Seychely
a	a	k8xC	a
Galapágách	Galapágy	k1gFnPc6	Galapágy
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
přes	přes	k7c4	přes
130	[number]	k4	130
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
okolo	okolo	k7c2	okolo
300	[number]	k4	300
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
želvy	želva	k1gFnPc4	želva
celé	celý	k2eAgFnSc2d1	celá
geologické	geologický	k2eAgFnSc2d1	geologická
historie	historie	k1gFnSc2	historie
patřil	patřit	k5eAaImAgInS	patřit
obří	obří	k2eAgInSc1d1	obří
svrchnokřídový	svrchnokřídový	k2eAgInSc1d1	svrchnokřídový
rod	rod	k1gInSc1	rod
Archelon	Archelon	k1gInSc1	Archelon
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
těla	tělo	k1gNnSc2	tělo
až	až	k9	až
4	[number]	k4	4
metry	metro	k1gNnPc7	metro
a	a	k8xC	a
několikatunovou	několikatunový	k2eAgFnSc7d1	několikatunová
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tito	tento	k3xDgMnPc1	tento
obři	obr	k1gMnPc1	obr
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
mořích	moře	k1gNnPc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Želvy	želva	k1gFnPc1	želva
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
podřádů	podřád	k1gInPc2	podřád
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
vtahují	vtahovat	k5eAaImIp3nP	vtahovat
krk	krk	k1gInSc4	krk
do	do	k7c2	do
krunýře	krunýř	k1gInSc2	krunýř
<g/>
:	:	kIx,	:
želvy	želva	k1gFnSc2	želva
skupiny	skupina	k1gFnSc2	skupina
Cryptodira	Cryptodir	k1gInSc2	Cryptodir
(	(	kIx(	(
<g/>
skrytohrdlí	skrytohrdlí	k1gNnSc2	skrytohrdlí
<g/>
)	)	kIx)	)
dokážou	dokázat	k5eAaPmIp3nP	dokázat
zatáhnout	zatáhnout	k5eAaPmF	zatáhnout
krk	krk	k1gInSc4	krk
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
pod	pod	k7c4	pod
páteř	páteř	k1gFnSc4	páteř
<g/>
,	,	kIx,	,
želvy	želva	k1gFnPc4	želva
skupiny	skupina	k1gFnSc2	skupina
Pleurodira	Pleurodir	k1gInSc2	Pleurodir
(	(	kIx(	(
<g/>
skrytohlaví	skrytohlavý	k2eAgMnPc1d1	skrytohlavý
<g/>
)	)	kIx)	)
ji	on	k3xPp3gFnSc4	on
schovávají	schovávat	k5eAaImIp3nP	schovávat
do	do	k7c2	do
krunýře	krunýř	k1gInSc2	krunýř
na	na	k7c4	na
levou	levý	k2eAgFnSc4d1	levá
nebo	nebo	k8xC	nebo
pravou	pravý	k2eAgFnSc4d1	pravá
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
tlustý	tlustý	k2eAgInSc1d1	tlustý
a	a	k8xC	a
nevysunutelný	vysunutelný	k2eNgInSc1d1	vysunutelný
<g/>
.	.	kIx.	.
</s>
<s>
Komorové	komorový	k2eAgNnSc1d1	komorové
oči	oko	k1gNnPc1	oko
většiny	většina	k1gFnSc2	většina
suchozemských	suchozemský	k2eAgFnPc2d1	suchozemská
želv	želva	k1gFnPc2	želva
směřují	směřovat	k5eAaImIp3nP	směřovat
dolů	dolů	k6eAd1	dolů
na	na	k7c4	na
předměty	předmět	k1gInPc4	předmět
před	před	k7c7	před
nimi	on	k3xPp3gFnPc7	on
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
vodních	vodní	k2eAgFnPc2d1	vodní
želv	želva	k1gFnPc2	želva
se	se	k3xPyFc4	se
oči	oko	k1gNnPc1	oko
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
vrchní	vrchní	k2eAgFnSc6d1	vrchní
části	část	k1gFnSc6	část
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
dokážou	dokázat	k5eAaPmIp3nP	dokázat
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
vodách	voda	k1gFnPc6	voda
skrýt	skrýt	k5eAaPmF	skrýt
před	před	k7c7	před
predátory	predátor	k1gMnPc7	predátor
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
ponořeny	ponořit	k5eAaPmNgInP	ponořit
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
vyjma	vyjma	k7c2	vyjma
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
nozder	nozdra	k1gFnPc2	nozdra
<g/>
.	.	kIx.	.
</s>
<s>
Mořské	mořský	k2eAgFnPc1d1	mořská
želvy	želva	k1gFnPc1	želva
mají	mít	k5eAaImIp3nP	mít
poblíž	poblíž	k7c2	poblíž
očí	oko	k1gNnPc2	oko
žlázky	žlázka	k1gFnSc2	žlázka
produkující	produkující	k2eAgFnSc2d1	produkující
slané	slaný	k2eAgFnSc2d1	slaná
slzy	slza	k1gFnSc2	slza
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zbavují	zbavovat	k5eAaImIp3nP	zbavovat
nadbytečné	nadbytečný	k2eAgFnSc2d1	nadbytečná
soli	sůl	k1gFnSc2	sůl
získané	získaný	k2eAgFnSc2d1	získaná
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pijí	pít	k5eAaImIp3nP	pít
<g/>
.	.	kIx.	.
</s>
<s>
Želvy	želva	k1gFnPc1	želva
vidí	vidět	k5eAaImIp3nP	vidět
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
neobvykle	obvykle	k6eNd1	obvykle
vysokému	vysoký	k2eAgInSc3d1	vysoký
počtu	počet	k1gInSc3	počet
tyčinek	tyčinka	k1gFnPc2	tyčinka
v	v	k7c6	v
sítnici	sítnice	k1gFnSc6	sítnice
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Navíc	navíc	k6eAd1	navíc
nedokážou	dokázat	k5eNaPmIp3nP	dokázat
plynule	plynule	k6eAd1	plynule
sledovat	sledovat	k5eAaImF	sledovat
pohybující	pohybující	k2eAgFnSc1d1	pohybující
se	se	k3xPyFc4	se
kořist	kořist	k1gFnSc1	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
je	být	k5eAaImIp3nS	být
obyčejně	obyčejně	k6eAd1	obyčejně
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
pouze	pouze	k6eAd1	pouze
predátorům	predátor	k1gMnPc3	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Masožravé	masožravý	k2eAgFnPc1d1	masožravá
želvy	želva	k1gFnPc1	želva
ovšem	ovšem	k9	ovšem
dokážou	dokázat	k5eAaPmIp3nP	dokázat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
pohnout	pohnout	k5eAaPmF	pohnout
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
doslova	doslova	k6eAd1	doslova
chňapnout	chňapnout	k5eAaPmF	chňapnout
po	po	k7c6	po
kořisti	kořist	k1gFnSc6	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Vidí	vidět	k5eAaImIp3nS	vidět
barevně	barevně	k6eAd1	barevně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Želvy	želva	k1gFnPc1	želva
mají	mít	k5eAaImIp3nP	mít
zkostnatělý	zkostnatělý	k2eAgInSc4d1	zkostnatělý
zobák	zobák	k1gInSc4	zobák
<g/>
,	,	kIx,	,
čelisti	čelist	k1gFnPc1	čelist
používají	používat	k5eAaImIp3nP	používat
ke	k	k7c3	k
kousání	kousání	k1gNnSc3	kousání
a	a	k8xC	a
žvýkání	žvýkání	k1gNnSc3	žvýkání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
zubů	zub	k1gInPc2	zub
je	být	k5eAaImIp3nS	být
horní	horní	k2eAgFnSc1d1	horní
i	i	k8xC	i
dolní	dolní	k2eAgFnSc1d1	dolní
čelist	čelist	k1gFnSc1	čelist
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
rohovitými	rohovitý	k2eAgInPc7d1	rohovitý
výčnělky	výčnělek	k1gInPc7	výčnělek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
masožravých	masožravý	k2eAgFnPc2d1	masožravá
želv	želva	k1gFnPc2	želva
jsou	být	k5eAaImIp3nP	být
kvůli	kvůli	k7c3	kvůli
chytání	chytání	k1gNnSc3	chytání
potravy	potrava	k1gFnSc2	potrava
obvykle	obvykle	k6eAd1	obvykle
ostré	ostrý	k2eAgInPc1d1	ostrý
<g/>
,	,	kIx,	,
u	u	k7c2	u
býložravých	býložravý	k2eAgFnPc2d1	býložravá
želv	želva	k1gFnPc2	želva
bývají	bývat	k5eAaImIp3nP	bývat
vroubkované	vroubkovaný	k2eAgFnPc1d1	vroubkovaná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
žvýkaní	žvýkaný	k2eAgMnPc1d1	žvýkaný
velmi	velmi	k6eAd1	velmi
tuhých	tuhý	k2eAgFnPc2d1	tuhá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
polykání	polykání	k1gNnSc6	polykání
potravy	potrava	k1gFnSc2	potrava
používají	používat	k5eAaImIp3nP	používat
želvy	želva	k1gFnPc1	želva
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
plazů	plaz	k1gMnPc2	plaz
ho	on	k3xPp3gMnSc4	on
nemohou	moct	k5eNaImIp3nP	moct
vystrčit	vystrčit	k5eAaPmF	vystrčit
a	a	k8xC	a
chytat	chytat	k5eAaImF	chytat
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Krunýř	krunýř	k1gInSc1	krunýř
sloužil	sloužit	k5eAaImAgInS	sloužit
původně	původně	k6eAd1	původně
u	u	k7c2	u
želv	želva	k1gFnPc2	želva
při	při	k7c6	při
hrabání	hrabání	k1gNnSc6	hrabání
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
krunýře	krunýř	k1gInSc2	krunýř
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
karapax	karapax	k1gInSc1	karapax
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
pak	pak	k6eAd1	pak
plastron	plastron	k1gInSc4	plastron
a	a	k8xC	a
dohromady	dohromady	k6eAd1	dohromady
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
tzv.	tzv.	kA	tzv.
mosty	most	k1gInPc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
karapax	karapax	k1gInSc1	karapax
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
5	[number]	k4	5
hřbetních	hřbetní	k2eAgMnPc2d1	hřbetní
<g/>
,	,	kIx,	,
8	[number]	k4	8
žeberních	žeberní	k2eAgFnPc2d1	žeberní
a	a	k8xC	a
24	[number]	k4	24
postranních	postranní	k2eAgFnPc2d1	postranní
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Právě	právě	k9	právě
počet	počet	k1gInSc1	počet
těchto	tento	k3xDgFnPc2	tento
desek	deska	k1gFnPc2	deska
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tvarem	tvar	k1gInSc7	tvar
plastronu	plastron	k1gInSc2	plastron
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
rozlišovacích	rozlišovací	k2eAgInPc2d1	rozlišovací
znaků	znak	k1gInPc2	znak
pro	pro	k7c4	pro
podobné	podobný	k2eAgInPc4d1	podobný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
krunýře	krunýř	k1gInSc2	krunýř
tvoří	tvořit	k5eAaImIp3nS	tvořit
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
S	s	k7c7	s
krunýřem	krunýř	k1gInSc7	krunýř
je	být	k5eAaImIp3nS	být
tělo	tělo	k1gNnSc1	tělo
želvy	želva	k1gFnSc2	želva
pevně	pevně	k6eAd1	pevně
spojeno	spojit	k5eAaPmNgNnS	spojit
páteřními	páteřní	k2eAgInPc7d1	páteřní
a	a	k8xC	a
žeberními	žeberní	k2eAgFnPc7d1	žeberní
kostmi	kost	k1gFnPc7	kost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
želva	želva	k1gFnSc1	želva
nemůže	moct	k5eNaImIp3nS	moct
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
krunýře	krunýř	k1gInSc2	krunýř
vylézt	vylézt	k5eAaPmF	vylézt
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
část	část	k1gFnSc1	část
krunýře	krunýř	k1gInSc2	krunýř
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
štítovými	štítový	k2eAgFnPc7d1	štítová
destičkami	destička	k1gFnPc7	destička
z	z	k7c2	z
keratinu	keratin	k1gInSc2	keratin
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
krunýř	krunýř	k1gInSc1	krunýř
krytý	krytý	k2eAgInSc1d1	krytý
jen	jen	k6eAd1	jen
silnou	silný	k2eAgFnSc7d1	silná
kůží	kůže	k1gFnSc7	kůže
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tvar	tvar	k1gInSc1	tvar
krunýře	krunýř	k1gInSc2	krunýř
nám	my	k3xPp1nPc3	my
velmi	velmi	k6eAd1	velmi
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
při	při	k7c6	při
zjišťování	zjišťování	k1gNnSc6	zjišťování
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
želvy	želva	k1gFnSc2	želva
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
suchozemských	suchozemský	k2eAgFnPc2d1	suchozemská
želv	želva	k1gFnPc2	želva
má	mít	k5eAaImIp3nS	mít
veliký	veliký	k2eAgInSc4d1	veliký
a	a	k8xC	a
těžký	těžký	k2eAgInSc4d1	těžký
krunýř	krunýř	k1gInSc4	krunýř
kupolovitého	kupolovitý	k2eAgInSc2d1	kupolovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
predátorům	predátor	k1gMnPc3	predátor
uchopit	uchopit	k5eAaPmF	uchopit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
čelistí	čelist	k1gFnPc2	čelist
a	a	k8xC	a
rozdrtit	rozdrtit	k5eAaPmF	rozdrtit
<g/>
.	.	kIx.	.
</s>
<s>
Želva	želva	k1gFnSc1	želva
skalní	skalní	k2eAgFnSc1d1	skalní
má	mít	k5eAaImIp3nS	mít
naopak	naopak	k6eAd1	naopak
plochý	plochý	k2eAgInSc4d1	plochý
a	a	k8xC	a
ohebný	ohebný	k2eAgInSc4d1	ohebný
krunýř	krunýř	k1gInSc4	krunýř
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
skryje	skrýt	k5eAaPmIp3nS	skrýt
ve	v	k7c6	v
skalních	skalní	k2eAgFnPc6d1	skalní
trhlinách	trhlina	k1gFnPc6	trhlina
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Většina	většina	k1gFnSc1	většina
vodních	vodní	k2eAgFnPc2d1	vodní
želv	želva	k1gFnPc2	želva
má	mít	k5eAaImIp3nS	mít
krunýř	krunýř	k1gInSc1	krunýř
plochý	plochý	k2eAgInSc1d1	plochý
<g/>
,	,	kIx,	,
hydrodynamického	hydrodynamický	k2eAgInSc2d1	hydrodynamický
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
snadno	snadno	k6eAd1	snadno
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
plavat	plavat	k5eAaImF	plavat
a	a	k8xC	a
potápět	potápět	k5eAaImF	potápět
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Krunýř	krunýř	k1gInSc1	krunýř
vodních	vodní	k2eAgFnPc2d1	vodní
želv	želva	k1gFnPc2	želva
je	být	k5eAaImIp3nS	být
také	také	k9	také
daleko	daleko	k6eAd1	daleko
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
u	u	k7c2	u
suchozemských	suchozemský	k2eAgInPc2d1	suchozemský
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
kostmi	kost	k1gFnPc7	kost
velké	velký	k2eAgFnSc2d1	velká
mezery	mezera	k1gFnSc2	mezera
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
fontanely	fontanela	k1gFnSc2	fontanela
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
plastronu	plastron	k1gInSc2	plastron
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
želvy	želva	k1gFnSc2	želva
dá	dát	k5eAaPmIp3nS	dát
poznat	poznat	k5eAaPmF	poznat
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Sameček	sameček	k1gMnSc1	sameček
má	mít	k5eAaImIp3nS	mít
plastron	plastron	k1gInSc4	plastron
vypouklý	vypouklý	k2eAgInSc4d1	vypouklý
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
páření	páření	k1gNnSc6	páření
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Barva	barva	k1gFnSc1	barva
krunýře	krunýř	k1gInSc2	krunýř
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
různí	různit	k5eAaImIp3nP	různit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
černá	černá	k1gFnSc1	černá
<g/>
,	,	kIx,	,
hnědá	hnědý	k2eAgFnSc1d1	hnědá
a	a	k8xC	a
olivově	olivově	k6eAd1	olivově
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
krunýři	krunýř	k1gInSc6	krunýř
i	i	k8xC	i
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
oranžové	oranžový	k2eAgFnPc1d1	oranžová
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnPc1d1	žlutá
nebo	nebo	k8xC	nebo
šedé	šedý	k2eAgFnPc1d1	šedá
tečky	tečka	k1gFnPc1	tečka
<g/>
,	,	kIx,	,
linky	linka	k1gFnPc1	linka
nebo	nebo	k8xC	nebo
nepravidelné	pravidelný	k2eNgFnPc1d1	nepravidelná
skvrnky	skvrnka	k1gFnPc1	skvrnka
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejkrásněji	krásně	k6eAd3	krásně
zbarvených	zbarvený	k2eAgFnPc2d1	zbarvená
želv	želva	k1gFnPc2	želva
je	být	k5eAaImIp3nS	být
želva	želva	k1gFnSc1	želva
ozdobná	ozdobný	k2eAgFnSc1d1	ozdobná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
žlutý	žlutý	k2eAgInSc4d1	žlutý
plastron	plastron	k1gInSc4	plastron
a	a	k8xC	a
černý	černý	k2eAgInSc4d1	černý
nebo	nebo	k8xC	nebo
olivově	olivově	k6eAd1	olivově
zelený	zelený	k2eAgInSc1d1	zelený
karpax	karpax	k1gInSc1	karpax
s	s	k7c7	s
červenými	červený	k2eAgFnPc7d1	červená
značkami	značka	k1gFnPc7	značka
okolo	okolo	k7c2	okolo
jeho	on	k3xPp3gInSc2	on
okraje	okraj	k1gInSc2	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
strana	strana	k1gFnSc1	strana
krunýře	krunýř	k1gInSc2	krunýř
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každá	každý	k3xTgFnSc1	každý
štítová	štítový	k2eAgFnSc1d1	štítová
destička	destička	k1gFnSc1	destička
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jedné	jeden	k4xCgFnSc6	jeden
modifikované	modifikovaný	k2eAgFnSc6d1	modifikovaná
šupině	šupina	k1gFnSc6	šupina
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
kůže	kůže	k1gFnSc2	kůže
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
mnohem	mnohem	k6eAd1	mnohem
menšími	malý	k2eAgFnPc7d2	menší
šupinkami	šupinka	k1gFnPc7	šupinka
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
plazů	plaz	k1gMnPc2	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
želvy	želva	k1gFnPc1	želva
nesvlékají	svlékat	k5eNaImIp3nP	svlékat
kůži	kůže	k1gFnSc4	kůže
naráz	naráz	k6eAd1	naráz
jako	jako	k8xS	jako
hadi	had	k1gMnPc1	had
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neustále	neustále	k6eAd1	neustále
po	po	k7c6	po
malých	malý	k2eAgInPc6d1	malý
kouskách	kousek	k1gInPc6	kousek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
chováte	chovat	k5eAaImIp2nP	chovat
želvu	želva	k1gFnSc4	želva
v	v	k7c6	v
akváriu	akvárium	k1gNnSc6	akvárium
<g/>
,	,	kIx,	,
můžete	moct	k5eAaImIp2nP	moct
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
najít	najít	k5eAaPmF	najít
částečky	částečka	k1gFnPc4	částečka
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Suchozemské	suchozemský	k2eAgFnPc1d1	suchozemská
želvy	želva	k1gFnPc1	želva
také	také	k9	také
svlékají	svlékat	k5eAaImIp3nP	svlékat
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
vodním	vodní	k2eAgFnPc3d1	vodní
želvám	želva	k1gFnPc3	želva
nikdy	nikdy	k6eAd1	nikdy
neodlupuje	odlupovat	k5eNaImIp3nS	odlupovat
a	a	k8xC	a
vrství	vrstvit	k5eAaImIp3nS	vrstvit
se	se	k3xPyFc4	se
do	do	k7c2	do
tlustých	tlustý	k2eAgInPc2d1	tlustý
'	'	kIx"	'
<g/>
kopečků	kopeček	k1gInPc2	kopeček
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
podobu	podoba	k1gFnSc4	podoba
letokruhů	letokruh	k1gInPc2	letokruh
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
panuje	panovat	k5eAaImIp3nS	panovat
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
spočítáním	spočítání	k1gNnSc7	spočítání
těchto	tento	k3xDgInPc2	tento
letokruhů	letokruh	k1gInPc2	letokruh
dostaneme	dostat	k5eAaPmIp1nP	dostat
věk	věk	k1gInSc4	věk
želvy	želva	k1gFnSc2	želva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
přesná	přesný	k2eAgFnSc1d1	přesná
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
růstu	růst	k1gInSc2	růst
nových	nový	k2eAgFnPc2d1	nová
destiček	destička	k1gFnPc2	destička
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
(	(	kIx(	(
<g/>
za	za	k7c4	za
rok	rok	k1gInSc4	rok
jich	on	k3xPp3gFnPc2	on
při	při	k7c6	při
příznivých	příznivý	k2eAgFnPc6d1	příznivá
podmínkách	podmínka	k1gFnPc6	podmínka
může	moct	k5eAaImIp3nS	moct
narůst	narůst	k5eAaPmF	narůst
i	i	k9	i
několik	několik	k4yIc4	několik
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
destičky	destička	k1gFnPc4	destička
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
krunýře	krunýř	k1gInSc2	krunýř
odpadnou	odpadnout	k5eAaPmIp3nP	odpadnout
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Suchozemské	suchozemský	k2eAgFnPc1d1	suchozemská
želvy	želva	k1gFnPc1	želva
jsou	být	k5eAaImIp3nP	být
proslulé	proslulý	k2eAgFnPc1d1	proslulá
svým	svůj	k3xOyFgNnSc7	svůj
pomalým	pomalý	k2eAgNnSc7d1	pomalé
tempem	tempo	k1gNnSc7	tempo
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
zapříčiněn	zapříčinit	k5eAaPmNgMnS	zapříčinit
jednak	jednak	k8xC	jednak
jejich	jejich	k3xOp3gInSc7	jejich
těžkým	těžký	k2eAgInSc7d1	těžký
a	a	k8xC	a
velkým	velký	k2eAgInSc7d1	velký
krunýřem	krunýř	k1gInSc7	krunýř
a	a	k8xC	a
jednak	jednak	k8xC	jednak
relativně	relativně	k6eAd1	relativně
neefektivním	efektivní	k2eNgInSc7d1	neefektivní
způsobem	způsob	k1gInSc7	způsob
postavení	postavení	k1gNnSc4	postavení
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
roztažené	roztažený	k2eAgFnPc1d1	roztažená
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
jako	jako	k8xC	jako
u	u	k7c2	u
ještěrek	ještěrka	k1gFnPc2	ještěrka
<g/>
.	.	kIx.	.
</s>
<s>
Sladkovodní	sladkovodní	k2eAgFnPc1d1	sladkovodní
želvy	želva	k1gFnPc1	želva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
žijí	žít	k5eAaImIp3nP	žít
částečně	částečně	k6eAd1	částečně
i	i	k9	i
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
plovací	plovací	k2eAgFnSc2d1	plovací
blány	blána	k1gFnSc2	blána
a	a	k8xC	a
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
drápy	dráp	k1gInPc4	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
těchto	tento	k3xDgInPc2	tento
drápů	dráp	k1gInPc2	dráp
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyšplhat	vyšplhat	k5eAaPmF	vyšplhat
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
či	či	k8xC	či
plovoucí	plovoucí	k2eAgFnPc4d1	plovoucí
klády	kláda	k1gFnPc4	kláda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rády	rád	k2eAgFnPc1d1	ráda
vyhřívají	vyhřívat	k5eAaImIp3nP	vyhřívat
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mívají	mívat	k5eAaImIp3nP	mívat
drápy	dráp	k1gInPc1	dráp
delší	dlouhý	k2eAgInPc1d2	delší
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
je	on	k3xPp3gNnSc4	on
ke	k	k7c3	k
stimulaci	stimulace	k1gFnSc3	stimulace
samice	samice	k1gFnSc2	samice
při	při	k7c6	při
páření	páření	k1gNnSc6	páření
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
plavání	plavání	k1gNnSc2	plavání
sladkovodních	sladkovodní	k2eAgFnPc2d1	sladkovodní
želv	želva	k1gFnPc2	želva
připomíná	připomínat	k5eAaImIp3nS	připomínat
psa	pes	k1gMnSc4	pes
(	(	kIx(	(
<g/>
jakoby	jakoby	k8xS	jakoby
běh	běh	k1gInSc4	běh
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
končetiny	končetina	k1gFnPc4	končetina
ale	ale	k8xC	ale
míří	mířit	k5eAaImIp3nP	mířit
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
druhy	druh	k1gInPc1	druh
želv	želva	k1gFnPc2	želva
plavou	plavat	k5eAaImIp3nP	plavat
spíše	spíše	k9	spíše
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
největší	veliký	k2eAgFnPc1d3	veliký
už	už	k6eAd1	už
často	často	k6eAd1	často
jen	jen	k9	jen
chodí	chodit	k5eAaImIp3nP	chodit
po	po	k7c6	po
dně	dno	k1gNnSc6	dno
řek	řeka	k1gFnPc2	řeka
či	či	k8xC	či
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Plovací	plovací	k2eAgFnPc1d1	plovací
blány	blána	k1gFnPc1	blána
mají	mít	k5eAaImIp3nP	mít
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
sladkovodních	sladkovodní	k2eAgFnPc2d1	sladkovodní
želv	želva	k1gFnPc2	želva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
karetka	karetka	k1gFnSc1	karetka
novoguinejská	novoguinejský	k2eAgFnSc1d1	novoguinejská
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
místo	místo	k7c2	místo
končetin	končetina	k1gFnPc2	končetina
ploutve	ploutev	k1gFnSc2	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
želv	želva	k1gFnPc2	želva
pak	pak	k6eAd1	pak
plavou	plavat	k5eAaImIp3nP	plavat
stejným	stejný	k2eAgInSc7d1	stejný
stylem	styl	k1gInSc7	styl
jako	jako	k8xC	jako
mořské	mořský	k2eAgFnSc2d1	mořská
želvy	želva	k1gFnSc2	želva
<g/>
.	.	kIx.	.
</s>
<s>
Mořské	mořský	k2eAgFnPc1d1	mořská
želvy	želva	k1gFnPc1	želva
žijí	žít	k5eAaImIp3nP	žít
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
místo	místo	k7c2	místo
nohou	noha	k1gFnPc2	noha
ploutve	ploutev	k1gFnSc2	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
pak	pak	k6eAd1	pak
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
vznášely	vznášet	k5eAaImAgInP	vznášet
či	či	k8xC	či
létaly	létat	k5eAaImAgInP	létat
<g/>
.	.	kIx.	.
</s>
<s>
Předními	přední	k2eAgFnPc7d1	přední
končetinami	končetina	k1gFnPc7	končetina
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnPc1d1	zadní
končetiny	končetina	k1gFnPc1	končetina
nejsou	být	k5eNaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
ke	k	k7c3	k
kormidlování	kormidlování	k1gNnSc3	kormidlování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
souši	souš	k1gFnSc6	souš
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
omezenou	omezený	k2eAgFnSc4d1	omezená
pohyblivost	pohyblivost	k1gFnSc4	pohyblivost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c4	na
souš	souš	k1gFnSc4	souš
vylézají	vylézat	k5eAaImIp3nP	vylézat
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nakladly	naklást	k5eAaPmAgFnP	naklást
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
namáhavě	namáhavě	k6eAd1	namáhavě
se	se	k3xPyFc4	se
postrkují	postrkovat	k5eAaImIp3nP	postrkovat
dopředu	dopředu	k6eAd1	dopředu
pomocí	pomoc	k1gFnSc7	pomoc
ploutví	ploutev	k1gFnSc7	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Zadními	zadní	k2eAgFnPc7d1	zadní
končetinami	končetina	k1gFnPc7	končetina
vyhrabou	vyhrabat	k5eAaPmIp3nP	vyhrabat
samice	samice	k1gFnSc1	samice
díru	díra	k1gFnSc4	díra
<g/>
,	,	kIx,	,
nakladou	naklást	k5eAaPmIp3nP	naklást
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
vejce	vejce	k1gNnSc1	vejce
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	on	k3xPp3gMnPc4	on
zase	zase	k9	zase
zahrabou	zahrabat	k5eAaPmIp3nP	zahrabat
pískem	písek	k1gInSc7	písek
<g/>
..	..	k?	..
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
stráví	strávit	k5eAaPmIp3nP	strávit
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
života	život	k1gInSc2	život
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
dýchat	dýchat	k5eAaImF	dýchat
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
tedy	tedy	k9	tedy
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
intervalech	interval	k1gInPc6	interval
vyplouvat	vyplouvat	k5eAaImF	vyplouvat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
doplnily	doplnit	k5eAaPmAgFnP	doplnit
plíce	plíce	k1gFnPc1	plíce
čerstvým	čerstvý	k2eAgInSc7d1	čerstvý
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
života	život	k1gInSc2	život
stráví	strávit	k5eAaPmIp3nS	strávit
i	i	k9	i
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
<s>
Mořské	mořský	k2eAgFnPc1d1	mořská
želvy	želva	k1gFnPc1	želva
kladou	klást	k5eAaImIp3nP	klást
vejce	vejce	k1gNnSc4	vejce
na	na	k7c6	na
suchých	suchý	k2eAgFnPc6d1	suchá
písečných	písečný	k2eAgFnPc6d1	písečná
plážích	pláž	k1gFnPc6	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yRnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
také	také	k9	také
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
schopnost	schopnost	k1gFnSc4	schopnost
některých	některý	k3yIgFnPc2	některý
australských	australský	k2eAgFnPc2d1	australská
sladkovodních	sladkovodní	k2eAgFnPc2d1	sladkovodní
želv	želva	k1gFnPc2	želva
dýchat	dýchat	k5eAaImF	dýchat
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
prostornou	prostorný	k2eAgFnSc4d1	prostorná
kloaku	kloaka	k1gFnSc4	kloaka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
sliznice	sliznice	k1gFnSc2	sliznice
tvoří	tvořit	k5eAaImIp3nP	tvořit
prstovité	prstovitý	k2eAgInPc1d1	prstovitý
výběžky	výběžek	k1gInPc1	výběžek
<g/>
,	,	kIx,	,
papily	papila	k1gFnPc1	papila
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
bohaté	bohatý	k2eAgNnSc4d1	bohaté
krevní	krevní	k2eAgNnSc4d1	krevní
zásobení	zásobení	k1gNnSc4	zásobení
a	a	k8xC	a
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
povrch	povrch	k1gInSc4	povrch
kloaky	kloaka	k1gFnSc2	kloaka
<g/>
.	.	kIx.	.
</s>
<s>
Želvy	želva	k1gFnPc1	želva
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
přijímat	přijímat	k5eAaImF	přijímat
kyslík	kyslík	k1gInSc4	kyslík
rozpuštěný	rozpuštěný	k2eAgInSc4d1	rozpuštěný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
papily	papila	k1gFnPc1	papila
fungují	fungovat	k5eAaImIp3nP	fungovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
rybí	rybí	k2eAgFnPc4d1	rybí
žábry	žábry	k1gFnPc4	žábry
<g/>
.	.	kIx.	.
</s>
<s>
Třída	třída	k1gFnSc1	třída
plazů	plaz	k1gMnPc2	plaz
je	být	k5eAaImIp3nS	být
parafyletický	parafyletický	k2eAgInSc4d1	parafyletický
taxon	taxon	k1gInSc4	taxon
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
nejsou	být	k5eNaImIp3nP	být
zahrnuti	zahrnut	k2eAgMnPc1d1	zahrnut
i	i	k8xC	i
ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
při	při	k7c6	při
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
plazů	plaz	k1gMnPc2	plaz
také	také	k9	také
savci	savec	k1gMnPc1	savec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
najdete	najít	k5eAaPmIp2nP	najít
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
věnované	věnovaný	k2eAgFnSc6d1	věnovaná
plazům	plaz	k1gInPc3	plaz
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
želvy	želva	k1gFnPc4	želva
se	se	k3xPyFc4	se
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
objevily	objevit	k5eAaPmAgFnP	objevit
už	už	k6eAd1	už
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
před	před	k7c7	před
200	[number]	k4	200
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žijících	žijící	k2eAgMnPc2d1	žijící
plazů	plaz	k1gMnPc2	plaz
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
známe	znát	k5eAaImIp1nP	znát
již	již	k6eAd1	již
z	z	k7c2	z
triasu	trias	k1gInSc2	trias
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jedinou	jediný	k2eAgFnSc7d1	jediná
přeživší	přeživší	k2eAgFnSc7d1	přeživší
větví	větev	k1gFnSc7	větev
prastarého	prastarý	k2eAgInSc2d1	prastarý
kladistického	kladistický	k2eAgInSc2d1	kladistický
podstromu	podstrom	k1gInSc2	podstrom
anapsid	anapsid	k1gInSc1	anapsid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xC	jako
Millerettidae	Millerettidae	k1gNnSc2	Millerettidae
<g/>
,	,	kIx,	,
Procolophonoidea	Procolophonoideum	k1gNnSc2	Procolophonoideum
a	a	k8xC	a
Pareiasauria	Pareiasaurium	k1gNnSc2	Pareiasaurium
<g/>
.	.	kIx.	.
</s>
<s>
Lebka	lebka	k1gFnSc1	lebka
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
anapsid	anapsida	k1gFnPc2	anapsida
postrádá	postrádat	k5eAaImIp3nS	postrádat
otvory	otvor	k1gInPc4	otvor
ve	v	k7c6	v
spánkové	spánkový	k2eAgFnSc6d1	spánková
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
žijící	žijící	k2eAgInPc1d1	žijící
druhy	druh	k1gInPc1	druh
amniot	amniota	k1gFnPc2	amniota
mají	mít	k5eAaImIp3nP	mít
právě	právě	k9	právě
tyto	tento	k3xDgInPc1	tento
lebeční	lebeční	k2eAgInPc1d1	lebeční
otvory	otvor	k1gInPc1	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
anapsid	anapsida	k1gFnPc2	anapsida
vyhynula	vyhynout	k5eAaPmAgFnS	vyhynout
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
permu	perm	k1gInSc6	perm
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
skupiny	skupina	k1gFnSc2	skupina
Procolophonoidea	Procolophonoideus	k1gMnSc2	Procolophonoideus
a	a	k8xC	a
předchůdců	předchůdce	k1gMnPc2	předchůdce
želv	želva	k1gFnPc2	želva
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
nedávno	nedávno	k6eAd1	nedávno
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
typická	typický	k2eAgFnSc1d1	typická
vlastnost	vlastnost	k1gFnSc1	vlastnost
anapsid	anapsida	k1gFnPc2	anapsida
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
převzala	převzít	k5eAaPmAgFnS	převzít
i	i	k9	i
želví	želví	k2eAgFnSc1d1	želví
lebka	lebka	k1gFnSc1	lebka
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
znak	znak	k1gInSc1	znak
určující	určující	k2eAgInSc1d1	určující
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
želvy	želva	k1gFnPc4	želva
potomci	potomek	k1gMnPc1	potomek
anapsid	anapsid	k1gInSc4	anapsid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
případ	případ	k1gInSc4	případ
konvergentní	konvergentní	k2eAgFnSc2d1	konvergentní
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnPc1d3	nejnovější
fylogenetické	fylogenetický	k2eAgFnPc1d1	fylogenetická
studie	studie	k1gFnPc1	studie
proto	proto	k8xC	proto
zařadily	zařadit	k5eAaPmAgFnP	zařadit
želvy	želva	k1gFnPc1	želva
mezi	mezi	k7c4	mezi
Diapsida	Diapsid	k1gMnSc4	Diapsid
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
šupinatým	šupinatý	k2eAgInPc3d1	šupinatý
než	než	k8xS	než
k	k	k7c3	k
archosaurům	archosaur	k1gMnPc3	archosaur
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
nové	nový	k2eAgFnPc1d1	nová
studie	studie	k1gFnPc1	studie
podporují	podporovat	k5eAaImIp3nP	podporovat
nové	nový	k2eAgNnSc4d1	nové
zařazení	zařazení	k1gNnSc4	zařazení
želv	želva	k1gFnPc2	želva
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
některé	některý	k3yIgInPc4	některý
přiřazují	přiřazovat	k5eAaImIp3nP	přiřazovat
želvy	želva	k1gFnPc4	želva
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
archosaurům	archosaur	k1gMnPc3	archosaur
<g/>
.	.	kIx.	.
</s>
<s>
Přezkoumáním	přezkoumání	k1gNnSc7	přezkoumání
předchozích	předchozí	k2eAgInPc2d1	předchozí
fylogenetických	fylogenetický	k2eAgInPc2d1	fylogenetický
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dokazovaly	dokazovat	k5eAaImAgFnP	dokazovat
příbuznost	příbuznost	k1gFnSc4	příbuznost
želv	želva	k1gFnPc2	želva
s	s	k7c7	s
anapsida	anapsid	k1gMnSc2	anapsid
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mylné	mylný	k2eAgNnSc1d1	mylné
zařazení	zařazení	k1gNnSc1	zařazení
želv	želva	k1gFnPc2	želva
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
jak	jak	k6eAd1	jak
počátečním	počáteční	k2eAgInSc7d1	počáteční
předpokladem	předpoklad	k1gInSc7	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
želvy	želva	k1gFnPc1	želva
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
Anapsida	Anapsid	k1gMnSc4	Anapsid
(	(	kIx(	(
<g/>
a	a	k8xC	a
spíše	spíše	k9	spíše
zkoumaly	zkoumat	k5eAaImAgInP	zkoumat
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
typ	typ	k1gInSc1	typ
anapsid	anapsid	k1gInSc4	anapsid
želvy	želva	k1gFnSc2	želva
jsou	být	k5eAaImIp3nP	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
nedostatečně	dostatečně	k6eNd1	dostatečně
širokým	široký	k2eAgInSc7d1	široký
vzorkem	vzorek	k1gInSc7	vzorek
fosilních	fosilní	k2eAgInPc2d1	fosilní
a	a	k8xC	a
recentních	recentní	k2eAgInPc2d1	recentní
taxonů	taxon	k1gInPc2	taxon
nutných	nutný	k2eAgInPc2d1	nutný
pro	pro	k7c4	pro
sestrojení	sestrojení	k1gNnSc4	sestrojení
kladogramu	kladogram	k1gInSc2	kladogram
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
věc	věc	k1gFnSc1	věc
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
přiklání	přiklánět	k5eAaImIp3nS	přiklánět
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
želvy	želva	k1gFnPc1	želva
jsou	být	k5eAaImIp3nP	být
diapsidní	diapsidní	k2eAgFnPc1d1	diapsidní
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnPc4d3	nejstarší
želvy	želva	k1gFnPc4	želva
patří	patřit	k5eAaImIp3nP	patřit
rody	rod	k1gInPc1	rod
Proganochelys	Proganochelysa	k1gFnPc2	Proganochelysa
a	a	k8xC	a
Australochelys	Australochelysa	k1gFnPc2	Australochelysa
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc4	jejich
zástupci	zástupce	k1gMnPc1	zástupce
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
žili	žít	k5eAaImAgMnP	žít
ještě	ještě	k9	ještě
před	před	k7c7	před
oddělením	oddělení	k1gNnSc7	oddělení
vznikem	vznik	k1gInSc7	vznik
sesterských	sesterský	k2eAgFnPc2d1	sesterská
linií	linie	k1gFnPc2	linie
–	–	k?	–
skrytohlavých	skrytohlavý	k2eAgMnPc2d1	skrytohlavý
a	a	k8xC	a
skrytohrdlých	skrytohrdlý	k2eAgMnPc2d1	skrytohrdlý
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Australochelys	Australochelysa	k1gFnPc2	Australochelysa
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
želvami	želva	k1gFnPc7	želva
spojován	spojovat	k5eAaImNgInS	spojovat
do	do	k7c2	do
společné	společný	k2eAgFnSc2d1	společná
skupiny	skupina	k1gFnSc2	skupina
Rhaptochelydia	Rhaptochelydium	k1gNnSc2	Rhaptochelydium
<g/>
.	.	kIx.	.
</s>
