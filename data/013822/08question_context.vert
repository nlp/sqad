<s>
Jeremy	Jeremy	k1gInSc1
Bentham	Bentham	k1gInSc1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1748	#num#	k4
Londýn	Londýn	k1gInSc1
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1832	#num#	k4
tamtéž	tamtéž	k6eAd1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
britský	britský	k2eAgMnSc1d1
právní	právní	k2eAgMnSc1d1
teoretik	teoretik	k1gMnSc1
<g/>
,	,	kIx,
osvícenský	osvícenský	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
a	a	k8xC
radikální	radikální	k2eAgMnSc1d1
společenský	společenský	k2eAgMnSc1d1
reformátor	reformátor	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
utilitarismu	utilitarismus	k1gInSc2
a	a	k8xC
kritik	kritika	k1gFnPc2
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
</s>