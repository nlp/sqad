<s>
Jeremy	Jerema	k1gFnPc1
Bentham	Bentham	k1gInSc1
</s>
<s>
Jeremy	Jerema	k1gFnPc4
Bentham	Bentham	k1gInSc4
Narození	narození	k1gNnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1748	#num#	k4
<g/>
Londýn	Londýn	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1832	#num#	k4
<g/>
Londýn	Londýn	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Westminster	Westminster	k1gInSc1
School	School	k1gInSc1
(	(	kIx(
<g/>
1755	#num#	k4
<g/>
–	–	k?
<g/>
1760	#num#	k4
<g/>
)	)	kIx)
<g/>
The	The	k1gFnSc1
Queen	Quena	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
College	College	k1gNnSc7
(	(	kIx(
<g/>
od	od	k7c2
1760	#num#	k4
<g/>
)	)	kIx)
Povolání	povolání	k1gNnSc2
</s>
<s>
filozof	filozof	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
obchodník	obchodník	k1gMnSc1
<g/>
,	,	kIx,
advokát	advokát	k1gMnSc1
<g/>
,	,	kIx,
politolog	politolog	k1gMnSc1
<g/>
,	,	kIx,
sufražet	sufražeta	k1gFnPc2
<g/>
,	,	kIx,
lidskoprávní	lidskoprávní	k2eAgMnSc1d1
aktivista	aktivista	k1gMnSc1
a	a	k8xC
právník	právník	k1gMnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
deismus	deismus	k1gInSc4
Rodiče	rodič	k1gMnPc1
</s>
<s>
Jeremiah	Jeremiah	k1gInSc1
Bentham	Bentham	k1gInSc1
a	a	k8xC
Alicia	Alicia	k1gFnSc1
Woodward	Woodwarda	k1gFnPc2
Grove	Groev	k1gFnSc2
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Samuel	Samuel	k1gMnSc1
Bentham	Bentham	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jeremy	Jeremy	k1gInSc1
Bentham	Bentham	k1gInSc1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1748	#num#	k4
Londýn	Londýn	k1gInSc1
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1832	#num#	k4
tamtéž	tamtéž	k6eAd1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
britský	britský	k2eAgMnSc1d1
právní	právní	k2eAgMnSc1d1
teoretik	teoretik	k1gMnSc1
<g/>
,	,	kIx,
osvícenský	osvícenský	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
a	a	k8xC
radikální	radikální	k2eAgMnSc1d1
společenský	společenský	k2eAgMnSc1d1
reformátor	reformátor	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
utilitarismu	utilitarismus	k1gInSc2
a	a	k8xC
kritik	kritika	k1gFnPc2
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
bohaté	bohatý	k2eAgFnSc6d1
a	a	k8xC
konzervativní	konzervativní	k2eAgFnSc6d1
rodině	rodina	k1gFnSc6
advokáta	advokát	k1gMnSc2
a	a	k8xC
jako	jako	k9
zázračné	zázračný	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
se	se	k3xPyFc4
už	už	k6eAd1
v	v	k7c6
dětství	dětství	k1gNnSc6
naučil	naučit	k5eAaPmAgInS
latinsky	latinsky	k6eAd1
a	a	k8xC
francouzsky	francouzsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
dvanácti	dvanáct	k4xCc2
se	se	k3xPyFc4
zapsal	zapsat	k5eAaPmAgMnS
na	na	k7c4
Queen	Queen	k1gInSc4
<g/>
´	´	k?
<g/>
s	s	k7c7
College	College	k1gNnSc7
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
roku	rok	k1gInSc2
1763	#num#	k4
titul	titul	k1gInSc4
bakaláře	bakalář	k1gMnSc2
a	a	k8xC
1766	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
osmnácti	osmnáct	k4xCc6
letech	léto	k1gNnPc6
titul	titul	k1gInSc4
magistra	magistr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působil	působit	k5eAaImAgMnS
jako	jako	k9
advokát	advokát	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
protivila	protivit	k5eAaImAgFnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
spletitost	spletitost	k1gFnSc1
britského	britský	k2eAgMnSc2d1
Common	Commona	k1gFnPc2
law	law	k?
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
věnovat	věnovat	k5eAaImF,k5eAaPmF
jeho	jeho	k3xOp3gFnSc3
reformě	reforma	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
vlivem	vliv	k1gInSc7
Th	Th	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hobbese	Hobbese	k1gFnSc1
<g/>
,	,	kIx,
D.	D.	kA
Huma	Humum	k1gNnSc2
a	a	k8xC
francouzského	francouzský	k2eAgMnSc2d1
osvícence	osvícenec	k1gMnSc2
Helvétia	Helvétius	k1gMnSc2
uznával	uznávat	k5eAaImAgInS
jako	jako	k9
jediný	jediný	k2eAgInSc1d1
základ	základ	k1gInSc1
práva	právo	k1gNnSc2
i	i	k8xC
státu	stát	k1gInSc2
jejich	jejich	k3xOp3gFnSc4
užitečnost	užitečnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1785-1788	1785-1788	k4
procestoval	procestovat	k5eAaPmAgMnS
Evropu	Evropa	k1gFnSc4
od	od	k7c2
Francie	Francie	k1gFnSc2
až	až	k9
po	po	k7c4
Rusko	Rusko	k1gNnSc4
a	a	k8xC
setkal	setkat	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
řadou	řada	k1gFnSc7
významných	významný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
s	s	k7c7
d	d	k?
<g/>
'	'	kIx"
<g/>
Alembertem	Alembert	k1gMnSc7
<g/>
,	,	kIx,
s	s	k7c7
J.	J.	kA
<g/>
-	-	kIx~
<g/>
B.	B.	kA
Sayem	Sayem	k1gInSc1
a	a	k8xC
dalšími	další	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příznivě	příznivě	k6eAd1
přijal	přijmout	k5eAaPmAgMnS
Francouzskou	francouzský	k2eAgFnSc4d1
revoluci	revoluce	k1gFnSc4
a	a	k8xC
1792	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
čestným	čestný	k2eAgMnSc7d1
občanem	občan	k1gMnSc7
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
náboženství	náboženství	k1gNnSc3
zastával	zastávat	k5eAaImAgMnS
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
přinejmenším	přinejmenším	k6eAd1
skeptické	skeptický	k2eAgFnPc1d1
<g/>
,	,	kIx,
spíše	spíše	k9
nepřátelské	přátelský	k2eNgNnSc1d1
stanovisko	stanovisko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Benthamova	Benthamův	k2eAgFnSc1d1
mumie	mumie	k1gFnSc1
v	v	k7c4
University	universita	k1gFnPc4
College	College	k1gFnPc2
London	London	k1gMnSc1
</s>
<s>
Když	když	k8xS
po	po	k7c6
smrti	smrt	k1gFnSc6
otce	otec	k1gMnSc2
1792	#num#	k4
zdědil	zdědit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gNnSc4
jmění	jmění	k1gNnSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k6eAd1
nezávislým	závislý	k2eNgInSc7d1
<g/>
,	,	kIx,
usadil	usadit	k5eAaPmAgInS
se	se	k3xPyFc4
ve	v	k7c6
Westminsteru	Westminster	k1gInSc6
jako	jako	k8xS,k8xC
zámožný	zámožný	k2eAgMnSc1d1
gentleman	gentleman	k1gMnSc1
a	a	k8xC
čtyřicet	čtyřicet	k4xCc4
let	léto	k1gNnPc2
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
psaní	psaní	k1gNnSc2
o	o	k7c6
reformách	reforma	k1gFnPc6
práva	právo	k1gNnSc2
a	a	k8xC
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
velmi	velmi	k6eAd1
pracovitý	pracovitý	k2eAgMnSc1d1
a	a	k8xC
denně	denně	k6eAd1
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
10	#num#	k4
až	až	k9
20	#num#	k4
stránek	stránka	k1gFnPc2
rukopisu	rukopis	k1gInSc2
<g/>
;	;	kIx,
jen	jen	k9
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
však	však	k9
vyšla	vyjít	k5eAaPmAgFnS
za	za	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
života	život	k1gInSc2
<g/>
;	;	kIx,
řada	řada	k1gFnSc1
spisů	spis	k1gInPc2
vyšla	vyjít	k5eAaPmAgFnS
nejprve	nejprve	k6eAd1
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
teprve	teprve	k6eAd1
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1823	#num#	k4
spoluzakládal	spoluzakládat	k5eAaImAgInS
Bentham	Bentham	k1gInSc1
Westminster	Westminster	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
,	,	kIx,
vlivný	vlivný	k2eAgInSc1d1
časopis	časopis	k1gInSc1
"	"	kIx"
<g/>
filosofických	filosofický	k2eAgInPc2d1
radikálů	radikál	k1gInPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gMnPc4
žáky	žák	k1gMnPc4
patřil	patřit	k5eAaImAgMnS
John	John	k1gMnSc1
Stuart	Stuarta	k1gFnPc2
Mill	Mill	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Benthamova	Benthamův	k2eAgMnSc2d1
přítele	přítel	k1gMnSc2
<g/>
,	,	kIx,
spisovatele	spisovatel	k1gMnSc2
a	a	k8xC
filosofa	filosof	k1gMnSc2
J.	J.	kA
Milla	Mill	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bentham	Bentham	k1gInSc1
zemřel	zemřít	k5eAaPmAgInS
jako	jako	k9
starý	starý	k2eAgMnSc1d1
mládenec	mládenec	k1gMnSc1
a	a	k8xC
do	do	k7c2
vysokého	vysoký	k2eAgInSc2d1
věku	věk	k1gInSc2
prý	prý	k9
slzel	slzet	k5eAaImAgInS
<g/>
,	,	kIx,
kdykoli	kdykoli	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
připomněly	připomnět	k5eAaPmAgFnP
jeho	jeho	k3xOp3gFnPc4
marné	marný	k2eAgFnPc4d1
námluvy	námluva	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
vůli	vůle	k1gFnSc6
Bentham	Bentham	k1gInSc1
stanovil	stanovit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
mrtvola	mrtvola	k1gFnSc1
má	mít	k5eAaImIp3nS
vypreparovat	vypreparovat	k5eAaPmF
a	a	k8xC
vystavit	vystavit	k5eAaPmF
v	v	k7c4
University	universita	k1gFnPc4
College	College	k1gFnPc2
London	London	k1gMnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
pomáhal	pomáhat	k5eAaImAgInS
založit	založit	k5eAaPmF
a	a	k8xC
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
vystavena	vystavit	k5eAaPmNgFnS
<g/>
,	,	kIx,
ovšem	ovšem	k9
s	s	k7c7
voskovou	voskový	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
její	její	k3xOp3gFnSc1
mumifikace	mumifikace	k1gFnSc1
se	se	k3xPyFc4
nepodařila	podařit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Reforma	reforma	k1gFnSc1
společnosti	společnost	k1gFnSc2
</s>
<s>
Benthamův	Benthamův	k2eAgInSc1d1
návrh	návrh	k1gInSc1
věznice	věznice	k1gFnSc2
(	(	kIx(
<g/>
Panopticon	Panopticon	k1gMnSc1
<g/>
,	,	kIx,
1791	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bentham	Bentham	k1gInSc1
byl	být	k5eAaImAgInS
zastánce	zastánce	k1gMnSc4
asociační	asociační	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
myšlení	myšlení	k1gNnSc2
<g/>
,	,	kIx,
liberalismu	liberalismus	k1gInSc2
a	a	k8xC
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
a	a	k8xC
první	první	k4xOgMnSc1
myslitel	myslitel	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
vyslovil	vyslovit	k5eAaPmAgMnS
pro	pro	k7c4
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
(	(	kIx(
<g/>
vzdělaných	vzdělaný	k2eAgFnPc2d1
<g/>
)	)	kIx)
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východiskem	východisko	k1gNnSc7
jeho	jeho	k3xOp3gFnPc2
reforem	reforma	k1gFnPc2
byla	být	k5eAaImAgFnS
představa	představa	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
cílem	cíl	k1gInSc7
společnosti	společnost	k1gFnSc2
je	být	k5eAaImIp3nS
rozmnožovat	rozmnožovat	k5eAaImF
lidské	lidský	k2eAgNnSc4d1
štěstí	štěstí	k1gNnSc4
a	a	k8xC
odstraňovat	odstraňovat	k5eAaImF
nebo	nebo	k8xC
aspoň	aspoň	k9
omezovat	omezovat	k5eAaImF
každé	každý	k3xTgNnSc4
utrpení	utrpení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
roku	rok	k1gInSc2
1768	#num#	k4
narazil	narazit	k5eAaPmAgMnS
při	při	k7c6
četbě	četba	k1gFnSc6
na	na	k7c4
Hutchesonův	Hutchesonův	k2eAgInSc4d1
princip	princip	k1gInSc4
„	„	k?
<g/>
největšího	veliký	k2eAgNnSc2d3
štěstí	štěstí	k1gNnSc2
pro	pro	k7c4
největší	veliký	k2eAgInSc4d3
počet	počet	k1gInSc4
<g/>
“	“	k?
a	a	k8xC
rozvinul	rozvinout	k5eAaPmAgInS
jej	on	k3xPp3gMnSc4
do	do	k7c2
systému	systém	k1gInSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
se	se	k3xPyFc4
podle	podle	k7c2
něho	on	k3xPp3gNnSc2
mají	mít	k5eAaImIp3nP
řídit	řídit	k5eAaImF
lidské	lidský	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
i	i	k8xC
jejich	jejich	k3xOp3gNnPc2
zákonodárství	zákonodárství	k1gNnPc2
<g/>
;	;	kIx,
tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
nazval	nazvat	k5eAaPmAgInS,k5eAaBmAgInS
utilitarismus	utilitarismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Proto	proto	k8xC
také	také	k9
chtěl	chtít	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
některé	některý	k3yIgInPc1
činy	čin	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
podle	podle	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
názoru	názor	k1gInSc2
nikomu	nikdo	k3yNnSc3
neškodí	škodit	k5eNaImIp3nS
<g/>
,	,	kIx,
netrestaly	trestat	k5eNaImAgFnP
(	(	kIx(
<g/>
včetně	včetně	k7c2
homosexuality	homosexualita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zastával	zastávat	k5eAaImAgMnS
osvobození	osvobození	k1gNnSc4
otroků	otrok	k1gMnPc2
i	i	k8xC
kolonií	kolonie	k1gFnPc2
<g/>
,	,	kIx,
právo	právo	k1gNnSc4
na	na	k7c4
rozvod	rozvod	k1gInSc4
a	a	k8xC
odluku	odluka	k1gFnSc4
církve	církev	k1gFnSc2
od	od	k7c2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
vůči	vůči	k7c3
zločincům	zločinec	k1gMnPc3
zastával	zastávat	k5eAaImAgMnS
tvrdé	tvrdý	k2eAgNnSc4d1
stanovisko	stanovisko	k1gNnSc4
<g/>
,	,	kIx,
připouštěl	připouštět	k5eAaImAgInS
i	i	k9
tělesné	tělesný	k2eAgInPc4d1
tresty	trest	k1gInPc4
a	a	k8xC
pro	pro	k7c4
odsouzené	odsouzená	k1gFnPc4
navrhl	navrhnout	k5eAaPmAgMnS
do	do	k7c2
kruhu	kruh	k1gInSc2
uspořádané	uspořádaný	k2eAgNnSc1d1
vězení	vězení	k1gNnSc1
(	(	kIx(
<g/>
Panoptikon	Panoptikon	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
může	moct	k5eAaImIp3nS
dozorce	dozorce	k1gMnSc1
sledovat	sledovat	k5eAaImF
úplně	úplně	k6eAd1
všechno	všechen	k3xTgNnSc4
<g/>
;	;	kIx,
podobné	podobný	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
navrhoval	navrhovat	k5eAaImAgMnS
i	i	k9
pro	pro	k7c4
továrny	továrna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Utilitaristická	utilitaristický	k2eAgFnSc1d1
etika	etika	k1gFnSc1
</s>
<s>
Benthamova	Benthamův	k2eAgFnSc1d1
racionalistická	racionalistický	k2eAgFnSc1d1
etika	etika	k1gFnSc1
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
třech	tři	k4xCgInPc6
sloupech	sloup	k1gInPc6
<g/>
:	:	kIx,
prozíravosti	prozíravost	k1gFnPc1
(	(	kIx(
<g/>
prudence	prudence	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poctivosti	poctivost	k1gFnPc4
(	(	kIx(
<g/>
probity	probit	k2eAgMnPc4d1
<g/>
)	)	kIx)
a	a	k8xC
dobročinnosti	dobročinnost	k1gFnSc2
(	(	kIx(
<g/>
beneficience	beneficience	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
cílem	cíl	k1gInSc7
každého	každý	k3xTgNnSc2
jednání	jednání	k1gNnSc2
je	být	k5eAaImIp3nS
zvětšovat	zvětšovat	k5eAaImF
štěstí	štěstí	k1gNnSc4
a	a	k8xC
omezovat	omezovat	k5eAaImF
strasti	strast	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
protože	protože	k8xS
míru	míra	k1gFnSc4
štěstí	štěstí	k1gNnSc2
lze	lze	k6eAd1
podle	podle	k7c2
něho	on	k3xPp3gMnSc2
dokonce	dokonce	k9
změřit	změřit	k5eAaPmF
<g/>
,	,	kIx,
lze	lze	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
„	„	k?
<g/>
počtu	počet	k1gInSc2
obšťastnění	obšťastnění	k1gNnSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
felicific	felicific	k1gMnSc1
calculus	calculus	k1gMnSc1
<g/>
)	)	kIx)
kvalitu	kvalita	k1gFnSc4
každého	každý	k3xTgNnSc2
jednání	jednání	k1gNnSc2
přesně	přesně	k6eAd1
a	a	k8xC
objektivně	objektivně	k6eAd1
určit	určit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodující	rozhodující	k2eAgFnSc1d1
je	on	k3xPp3gNnSc4
trvání	trvání	k1gNnSc4
<g/>
,	,	kIx,
intenzita	intenzita	k1gFnSc1
<g/>
,	,	kIx,
blízkost	blízkost	k1gFnSc1
a	a	k8xC
jistota	jistota	k1gFnSc1
pocitu	pocit	k1gInSc2
blaha	blaho	k1gNnSc2
<g/>
,	,	kIx,
počet	počet	k1gInSc1
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
je	být	k5eAaImIp3nS
zakusí	zakusit	k5eAaPmIp3nS
<g/>
,	,	kIx,
a	a	k8xC
vyhlídka	vyhlídka	k1gFnSc1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
něho	on	k3xPp3gInSc2
vzniknou	vzniknout	k5eAaPmIp3nP
další	další	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
minimalizaci	minimalizace	k1gFnSc4
utrpení	utrpení	k1gNnSc2
<g/>
,	,	kIx,
zaslouží	zasloužit	k5eAaPmIp3nS
si	se	k3xPyFc3
podle	podle	k7c2
Benthama	Bentham	k1gMnSc2
ochranu	ochrana	k1gFnSc4
všichni	všechen	k3xTgMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
mohou	moct	k5eAaImIp3nP
trpět	trpět	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
nejen	nejen	k6eAd1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
zvířata	zvíře	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Defence	Defence	k1gFnSc1
of	of	k?
usury	usura	k1gFnSc2
<g/>
,	,	kIx,
1788	#num#	k4
</s>
<s>
Bentham	Bentham	k6eAd1
se	se	k3xPyFc4
počítá	počítat	k5eAaImIp3nS
mezi	mezi	k7c4
zakladatele	zakladatel	k1gMnSc4
ekonomického	ekonomický	k2eAgInSc2d1
liberalismu	liberalismus	k1gInSc2
a	a	k8xC
proti	proti	k7c3
svému	svůj	k3xOyFgMnSc3
příteli	přítel	k1gMnSc3
Adamu	Adam	k1gMnSc3
Smithovi	Smith	k1gMnSc3
skutečně	skutečně	k6eAd1
hájil	hájit	k5eAaImAgMnS
„	„	k?
<g/>
lichvu	lichva	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
tj.	tj.	kA
volné	volný	k2eAgFnSc2d1
úrokové	úrokový	k2eAgFnSc2d1
sazby	sazba	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
však	však	k9
zastával	zastávat	k5eAaImAgMnS
myšlenku	myšlenka	k1gFnSc4
státem	stát	k1gInSc7
zaručené	zaručený	k2eAgFnPc1d1
minimální	minimální	k2eAgFnPc1d1
mzdy	mzda	k1gFnPc1
pro	pro	k7c4
každého	každý	k3xTgMnSc4
<g/>
,	,	kIx,
povinného	povinný	k2eAgNnSc2d1
zdravotního	zdravotní	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
a	a	k8xC
státní	státní	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
růstu	růst	k1gInSc2
hospodářství	hospodářství	k1gNnSc2
i	i	k8xC
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgMnPc1d1
ekonomičtí	ekonomický	k2eAgMnPc1d1
liberálové	liberál	k1gMnPc1
(	(	kIx(
<g/>
L.	L.	kA
Mises	Mises	k1gInSc1
<g/>
,	,	kIx,
F.	F.	kA
von	von	k1gInSc1
Hayek	Hayek	k1gInSc1
aj.	aj.	kA
<g/>
)	)	kIx)
jej	on	k3xPp3gMnSc4
proto	proto	k6eAd1
kritizují	kritizovat	k5eAaImIp3nP
a	a	k8xC
pokládají	pokládat	k5eAaImIp3nP
naopak	naopak	k6eAd1
za	za	k7c2
nebezpečného	bezpečný	k2eNgMnSc2d1
odpůrce	odpůrce	k1gMnSc2
principů	princip	k1gInPc2
liberálního	liberální	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Lidská	lidský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
</s>
<s>
Jako	jako	k9
právník	právník	k1gMnSc1
a	a	k8xC
člověk	člověk	k1gMnSc1
přesvědčený	přesvědčený	k2eAgMnSc1d1
o	o	k7c6
nezbytnosti	nezbytnost	k1gFnSc6
práva	právo	k1gNnSc2
byl	být	k5eAaImAgMnS
Bentham	Bentham	k1gInSc4
sice	sice	k8xC
příznivcem	příznivec	k1gMnSc7
Francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
velmi	velmi	k6eAd1
ostrým	ostrý	k2eAgMnSc7d1
kritikem	kritik	k1gMnSc7
představy	představa	k1gFnSc2
přirozených	přirozený	k2eAgNnPc2d1
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
proti	proti	k7c3
nimž	jenž	k3xRgFnPc3
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
plamenný	plamenný	k2eAgInSc4d1
pamflet	pamflet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
něho	on	k3xPp3gMnSc2
-	-	kIx~
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
pro	pro	k7c4
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
současníka	současník	k1gMnSc4
Hegela	Hegel	k1gMnSc4
–	–	k?
možné	možný	k2eAgNnSc1d1
jen	jen	k9
v	v	k7c6
organizovaném	organizovaný	k2eAgInSc6d1
státu	stát	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
není	být	k5eNaImIp3nS
založen	založit	k5eAaPmNgMnS
na	na	k7c6
žádné	žádný	k3yNgFnSc6
společenské	společenský	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
legitimuje	legitimovat	k5eAaBmIp3nS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
svojí	svůj	k3xOyFgFnSc7
prospěšností	prospěšnost	k1gFnSc7
a	a	k8xC
užitkem	užitek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představa	představa	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
člověk	člověk	k1gMnSc1
mohl	moct	k5eAaImAgMnS
mít	mít	k5eAaImF
nějaká	nějaký	k3yIgNnPc4
práva	právo	k1gNnPc4
„	„	k?
<g/>
od	od	k7c2
narození	narození	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
„	„	k?
<g/>
rodil	rodit	k5eAaImAgMnS
svobodný	svobodný	k2eAgMnSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
tvrdily	tvrdit	k5eAaImAgFnP
listiny	listina	k1gFnPc1
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
podle	podle	k7c2
něho	on	k3xPp3gMnSc2
„	„	k?
<g/>
anarchistický	anarchistický	k2eAgInSc1d1
klam	klam	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
nebezpečný	bezpečný	k2eNgInSc1d1
nesmysl	nesmysl	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Pamflet	pamflet	k1gInSc1
ovšem	ovšem	k9
vyšel	vyjít	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
až	až	k8xS
1816	#num#	k4
ve	v	k7c6
Francii	Francie	k1gFnSc6
za	za	k7c2
obnoveného	obnovený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Benthamův	Benthamův	k2eAgInSc1d1
vliv	vliv	k1gInSc1
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2
svých	svůj	k3xOyFgMnPc2
vlivných	vlivný	k2eAgMnPc2d1
přátel	přítel	k1gMnPc2
a	a	k8xC
Westminster	Westminstra	k1gFnPc2
Revue	revue	k1gFnPc3
měl	mít	k5eAaImAgInS
Bentham	Bentham	k1gInSc1
nesmírný	smírný	k2eNgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
politické	politický	k2eAgNnSc4d1
a	a	k8xC
právní	právní	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
a	a	k8xC
dodnes	dodnes	k6eAd1
patrně	patrně	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
nejvlivnějším	vlivný	k2eAgMnSc7d3
teoretikem	teoretik	k1gMnSc7
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
myšlenky	myšlenka	k1gFnSc2
převzali	převzít	k5eAaPmAgMnP
a	a	k8xC
šířili	šířit	k5eAaImAgMnP
lidé	člověk	k1gMnPc1
jako	jako	k8xC,k8xS
John	John	k1gMnSc1
Stuart	Stuarta	k1gFnPc2
Mill	Mill	k1gMnSc1
<g/>
,	,	kIx,
Herbert	Herbert	k1gMnSc1
Spencer	Spencer	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Austin	Austin	k1gMnSc1
a	a	k8xC
mnoho	mnoho	k4c4
dalších	další	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utilitarismus	utilitarismus	k1gInSc1
je	být	k5eAaImIp3nS
i	i	k9
dnes	dnes	k6eAd1
významný	významný	k2eAgInSc4d1
směr	směr	k1gInSc4
zejména	zejména	k9
v	v	k7c6
anglosaském	anglosaský	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
češtiny	čeština	k1gFnSc2
nebyl	být	k5eNaImAgInS
přeložen	přeložit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Citáty	citát	k1gInPc1
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
štěstí	štěstí	k1gNnSc1
největšího	veliký	k2eAgInSc2d3
počtu	počet	k1gInSc2
je	být	k5eAaImIp3nS
základ	základ	k1gInSc1
morálky	morálka	k1gFnSc2
i	i	k8xC
zákonodárství	zákonodárství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Příroda	příroda	k1gFnSc1
určila	určit	k5eAaPmAgFnS
lidstvu	lidstvo	k1gNnSc3
dva	dva	k4xCgInPc4
suverénní	suverénní	k2eAgFnPc4d1
pány	pan	k1gMnPc7
<g/>
,	,	kIx,
utrpení	utrpení	k1gNnSc4
a	a	k8xC
štěstí	štěstí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
stanoví	stanovit	k5eAaPmIp3nP
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
máme	mít	k5eAaImIp1nP
dělat	dělat	k5eAaImF
a	a	k8xC
určují	určovat	k5eAaImIp3nP
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
budeme	být	k5eAaImBp1nP
dělat	dělat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
jsou	být	k5eAaImIp3nP
motivovány	motivován	k2eAgFnPc4d1
pouze	pouze	k6eAd1
touhou	touha	k1gFnSc7
získat	získat	k5eAaPmF
blaho	blaho	k1gNnSc4
a	a	k8xC
vyhnout	vyhnout	k5eAaPmF
se	se	k3xPyFc4
utrpení	utrpení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Právníci	právník	k1gMnPc1
jsou	být	k5eAaImIp3nP
jediní	jediný	k2eAgMnPc1d1
<g/>
,	,	kIx,
u	u	k7c2
koho	kdo	k3yInSc2,k3yRnSc2,k3yQnSc2
se	se	k3xPyFc4
neznalost	neznalost	k1gFnSc1
zákonů	zákon	k1gInPc2
netrestá	trestat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Na	na	k7c4
téma	téma	k1gNnSc4
práv	právo	k1gNnPc2
zvířat	zvíře	k1gNnPc2
<g/>
)	)	kIx)
Otázkou	otázka	k1gFnSc7
není	být	k5eNaImIp3nS
<g/>
,	,	kIx,
zda	zda	k8xS
dokáží	dokázat	k5eAaPmIp3nP
myslet	myslet	k5eAaImF
<g/>
,	,	kIx,
ani	ani	k8xC
zda	zda	k8xS
dokáží	dokázat	k5eAaPmIp3nP
mluvit	mluvit	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
schopna	schopen	k2eAgNnPc1d1
trpět	trpět	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Jeremy	Jerema	k1gFnPc4
Bentham	Bentham	k1gInSc1
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
několik	několik	k4yIc4
desítek	desítka	k1gFnPc2
spisů	spis	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
uvádíme	uvádět	k5eAaImIp1nP
alespoň	alespoň	k9
následující	následující	k2eAgFnPc1d1
<g/>
:	:	kIx,
</s>
<s>
A	a	k9
Fragment	fragment	k1gInSc4
on	on	k3xPp3gMnSc1
Government	Government	k1gMnSc1
[	[	kIx(
<g/>
Fragment	fragment	k1gInSc1
o	o	k7c6
vládě	vláda	k1gFnSc6
<g/>
]	]	kIx)
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
r.	r.	kA
1776	#num#	k4
</s>
<s>
Defence	Defence	k1gFnSc1
of	of	k?
Usury	usura	k1gFnSc2
[	[	kIx(
<g/>
Obrana	obrana	k1gFnSc1
lichvářství	lichvářství	k1gNnSc2
<g/>
]	]	kIx)
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
r.	r.	kA
1787	#num#	k4
</s>
<s>
An	An	k?
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Principles	Principles	k1gInSc1
of	of	k?
Morals	Morals	k1gInSc1
and	and	k?
Legislation	Legislation	k1gInSc1
[	[	kIx(
<g/>
Úvod	úvod	k1gInSc1
do	do	k7c2
principů	princip	k1gInPc2
mravnosti	mravnost	k1gFnSc2
a	a	k8xC
zákonodárství	zákonodárství	k1gNnSc2
<g/>
]	]	kIx)
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
r.	r.	kA
1789	#num#	k4
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Traité	Traita	k1gMnPc1
de	de	k?
législation	législation	k1gInSc1
civile	civil	k1gMnSc5
et	et	k?
pénale	pénala	k1gFnSc6
[	[	kIx(
<g/>
Pojednání	pojednání	k1gNnSc1
o	o	k7c6
civilním	civilní	k2eAgInSc6d1
a	a	k8xC
trestním	trestní	k2eAgNnSc6d1
zákonodárství	zákonodárství	k1gNnSc6
<g/>
]	]	kIx)
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
francouzsky	francouzsky	k6eAd1
r.	r.	kA
1802	#num#	k4
</s>
<s>
Theorie	theorie	k1gFnSc1
des	des	k1gNnSc2
peines	peinesa	k1gFnPc2
et	et	k?
des	des	k1gNnSc1
recompenses	recompenses	k1gMnSc1
[	[	kIx(
<g/>
Teorie	teorie	k1gFnSc1
trestů	trest	k1gInPc2
a	a	k8xC
odměn	odměna	k1gFnPc2
<g/>
]	]	kIx)
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
francouzsky	francouzsky	k6eAd1
r.	r.	kA
1811	#num#	k4
</s>
<s>
Deontology	Deontolog	k1gMnPc4
<g/>
,	,	kIx,
or	or	k?
the	the	k?
science	science	k1gFnSc2
of	of	k?
morality	moralita	k1gFnSc2
[	[	kIx(
<g/>
Deontologie	Deontologie	k1gFnSc1
neboli	neboli	k8xC
věda	věda	k1gFnSc1
o	o	k7c6
mravnosti	mravnost	k1gFnSc6
<g/>
]	]	kIx)
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
posmrtně	posmrtně	k6eAd1
r.	r.	kA
1834	#num#	k4
</s>
<s>
Čtyři	čtyři	k4xCgInPc4
spisy	spis	k1gInPc4
zařadila	zařadit	k5eAaPmAgFnS
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
na	na	k7c4
Index	index	k1gInSc4
zakázaných	zakázaný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Traité	Traitý	k2eAgFnSc2d1
de	de	k?
législation	législation	k1gInSc1
civile	civil	k1gMnSc5
et	et	k?
pénale	pénala	k1gFnSc6
<g/>
"	"	kIx"
(	(	kIx(
<g/>
vydáno	vydat	k5eAaPmNgNnS
r.	r.	kA
1802	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
Indexu	index	k1gInSc6
od	od	k7c2
r.	r.	kA
1819	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
„	„	k?
<g/>
Three	Three	k1gFnSc1
tracts	tracts	k6eAd1
relative	relativ	k1gInSc5
to	ten	k3xDgNnSc4
spanish	spanish	k1gMnSc1
and	and	k?
portuguese	portuguese	k1gFnSc2
affairs	affairsa	k1gFnPc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
vydáno	vydat	k5eAaPmNgNnS
r.	r.	kA
1821	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
Indexu	index	k1gInSc6
od	od	k7c2
r.	r.	kA
1826	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
„	„	k?
<g/>
Traité	Traitý	k2eAgNnSc4d1
des	des	k1gNnSc4
preuves	preuves	k1gMnSc1
judiciaires	judiciaires	k1gMnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
vydáno	vydat	k5eAaPmNgNnS
r.	r.	kA
1824	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
Indexu	index	k1gInSc6
od	od	k7c2
r.	r.	kA
1828	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
Deontology	Deontolog	k1gMnPc7
<g/>
,	,	kIx,
or	or	k?
the	the	k?
science	science	k1gFnSc2
of	of	k?
morality	moralita	k1gFnSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
vydáno	vydat	k5eAaPmNgNnS
r.	r.	kA
1834	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
Indexu	index	k1gInSc6
od	od	k7c2
r.	r.	kA
1835	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Jeremy	Jerema	k1gFnSc2
Bentham	Bentham	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Kindred	Kindred	k1gMnSc1
Britain	Britain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
M.	M.	kA
St.	st.	kA
John	John	k1gMnSc1
Packe	Pack	k1gMnSc2
<g/>
,	,	kIx,
The	The	k1gMnSc2
Life	Lif	k1gMnSc2
of	of	k?
John	John	k1gMnSc1
Stuart	Stuarta	k1gFnPc2
Mill	Mill	k1gMnSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
J.	J.	kA
Bentham	Bentham	k1gInSc1
<g/>
,	,	kIx,
Anarchical	Anarchical	k1gMnSc1
fallacies	fallacies	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jeremy	Jerema	k1gFnSc2
Bentham	Bentham	k1gInSc1
<g/>
,	,	kIx,
An	An	k1gFnSc1
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Principles	Principles	k1gInSc1
of	of	k?
Morals	Morals	k1gInSc1
and	and	k?
Legislation	Legislation	k1gInSc1
(	(	kIx(
<g/>
1789	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Index	index	k1gInSc1
librorum	librorum	k1gInSc1
prohibitorum	prohibitorum	k1gInSc1
/	/	kIx~
Leonis	Leonis	k1gInSc1
XIII	XIII	kA
Summi	Sum	k1gFnPc7
Pontificis	Pontificis	k1gInSc1
auctoritate	auctoritat	k1gInSc5
recognitus	recognitus	k1gInSc1
SSmi	SSm	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
D.	D.	kA
N.	N.	kA
Pii	Pius	k1gMnPc7
pp	pp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
XI	XI	kA
iussu	iussa	k1gFnSc4
editus	editus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Romae	Romae	k1gNnSc1
:	:	kIx,
Typis	Typis	k1gFnSc1
polyglottis	polyglottis	k1gFnSc1
Vaticanis	Vaticanis	k1gFnSc1
<g/>
,	,	kIx,
1924	#num#	k4
<g/>
.	.	kIx.
292	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
JOHNSON	JOHNSON	kA
<g/>
,	,	kIx,
Oliver	Oliver	k1gMnSc1
A.	A.	kA
Jeremy	Jerema	k1gFnPc1
Bentham	Bentham	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
McGREAL	McGREAL	k1gMnSc1
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
Philip	Philip	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgInPc1d1
postavy	postav	k1gInPc1
západního	západní	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
:	:	kIx,
slovník	slovník	k1gInSc1
myslitelů	myslitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Martin	Martin	k1gMnSc1
Pokorný	Pokorný	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Prostor	prostor	k1gInSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
369	#num#	k4
<g/>
–	–	k?
<g/>
372	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85190	#num#	k4
<g/>
-	-	kIx~
<g/>
61	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bentham	Bentham	k1gInSc1
<g/>
,	,	kIx,
Jeremy	Jerem	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
:	:	kIx,
illustrovaná	illustrovaný	k2eAgFnSc1d1
encyklopaedie	encyklopaedie	k1gFnSc1
obecných	obecný	k2eAgFnPc2d1
vědomostí	vědomost	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1890	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
768	#num#	k4
<g/>
–	–	k?
<g/>
769	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bentham	Bentham	k1gInSc1
<g/>
,	,	kIx,
Jeremy	Jerem	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
©	©	k?
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
https://www.biolib.cz/cz/glossaryterm/id4685/	https://www.biolib.cz/cz/glossaryterm/id4685/	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Liberalismus	liberalismus	k1gInSc1
</s>
<s>
Osvícenství	osvícenství	k1gNnSc1
</s>
<s>
Utilitarismus	utilitarismus	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jeremy	Jerema	k1gFnSc2
Bentham	Bentham	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Jeremy	Jerema	k1gFnSc2
Bentham	Bentham	k1gInSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jeremy	Jerema	k1gFnSc2
Bentham	Bentham	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS
Library	Librara	k1gFnPc4
of	of	k?
Liberty	Libert	k1gMnPc7
-	-	kIx~
Jeremy	Jerem	k1gMnPc7
Bentham	Bentham	k1gInSc4
<g/>
,	,	kIx,
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Jeremy	Jerema	k1gFnPc1
Bentham	Bentham	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Critique	Critique	k1gFnSc1
of	of	k?
the	the	k?
Doctrine	Doctrin	k1gInSc5
of	of	k?
Inalienable	Inalienable	k1gMnSc5
<g/>
,	,	kIx,
Natural	Natural	k?
Rights	Rights	k1gInSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
in	in	k?
Anarchical	Anarchical	k1gMnSc1
Fallacies	Fallacies	k1gMnSc1
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
of	of	k?
Bowring	Bowring	k1gInSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Works	Works	kA
<g/>
,	,	kIx,
1843	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
International	Internationat	k5eAaImAgMnS,k5eAaPmAgMnS
Website	Websit	k1gInSc5
for	forum	k1gNnPc2
Utilitarian	Utilitarian	k1gMnSc1
Philosophy	Philosopha	k1gFnSc2
</s>
<s>
Bentham	Bentham	k6eAd1
Index	index	k1gInSc1
<g/>
,	,	kIx,
bibliografie	bibliografie	k1gFnSc1
</s>
<s>
Kolekce	kolekce	k1gFnSc1
linků	link	k1gInPc2
a	a	k8xC
textů	text	k1gInPc2
o	o	k7c6
Benthamovi	Bentham	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Jeremy	Jerema	k1gFnPc1
Bentham	Bentham	k1gInSc1
<g/>
,	,	kIx,
categorized	categorized	k1gInSc1
links	links	k6eAd1
</s>
<s>
Jeremy	Jerema	k1gFnPc1
Bentham	Bentham	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Life	Life	k1gInSc1
and	and	k?
Impact	Impact	k1gInSc1
</s>
<s>
Heslo	heslo	k1gNnSc1
Benthamism	Benthamisma	k1gFnPc2
v	v	k7c6
Catholic	Catholice	k1gFnPc2
Encyclopedia	Encyclopedium	k1gNnPc5
</s>
<s>
Heslo	heslo	k1gNnSc4
Bentham	Bentham	k1gInSc1
a	a	k8xC
bibliografie	bibliografie	k1gFnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
encyclopedia	encyclopedium	k1gNnSc2
of	of	k?
philosophy	philosopha	k1gFnSc2
</s>
<s>
Utilitarianism	Utilitarianism	k6eAd1
as	as	k9
Secondary	Secondar	k1gInPc1
Ethic	Ethice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehled	přehled	k1gInSc4
utilitarismu	utilitarismus	k1gInSc2
včetně	včetně	k7c2
jeho	jeho	k3xOp3gFnPc2
kritik	kritika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
J.	J.	kA
Bentham	Bentham	k1gInSc1
<g/>
,	,	kIx,
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Principles	Principles	k1gInSc1
of	of	k?
Morals	Morals	k1gInSc1
and	and	k?
Legislation	Legislation	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700153	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118509187	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2280	#num#	k4
5406	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79040051	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
59078842	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79040051	#num#	k4
</s>
