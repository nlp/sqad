<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
virem	vir	k1gInSc7	vir
Bluetongue	Bluetongue	k1gFnPc2	Bluetongue
virus	virus	k1gInSc4	virus
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Reoviridae	Reovirida	k1gFnSc2	Reovirida
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přenášeno	přenášet	k5eAaImNgNnS	přenášet
krevsajícím	krevsající	k2eAgInSc7d1	krevsající
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
tiplíky	tiplík	k1gMnPc7	tiplík
(	(	kIx(	(
<g/>
Culicoides	Culicoidesa	k1gFnPc2	Culicoidesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
