<s>
Monroeova	Monroeův	k2eAgFnSc1d1
doktrína	doktrína	k1gFnSc1
</s>
<s>
Monroeova	Monroeův	k2eAgFnSc1d1
doktrína	doktrína	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Monroe	Monroe	k1gFnSc1
Doctrine	Doctrin	k1gInSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zahraničně	zahraničně	k6eAd1
politická	politický	k2eAgFnSc1d1
doktrína	doktrína	k1gFnSc1
<g/>
,	,	kIx,
již	již	k6eAd1
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1823	#num#	k4
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
poselství	poselství	k1gNnSc6
Kongresu	kongres	k1gInSc2
vyhlásil	vyhlásit	k5eAaPmAgMnS
prezident	prezident	k1gMnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgFnPc2d1
James	Jamesa	k1gFnPc2
Monroe	Monroe	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropské	evropský	k2eAgFnSc6d1
mocnosti	mocnost	k1gFnSc6
podle	podle	k7c2
této	tento	k3xDgFnSc2
doktríny	doktrína	k1gFnSc2
nemají	mít	k5eNaImIp3nP
právo	právo	k1gNnSc4
zasahovat	zasahovat	k5eAaImF
do	do	k7c2
záležitostí	záležitost	k1gFnPc2
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
na	na	k7c6
území	území	k1gNnSc6
amerického	americký	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
a	a	k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
budou	být	k5eAaImBp3nP
jejich	jejich	k3xOp3gFnSc4
případné	případný	k2eAgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
akce	akce	k1gFnPc1
na	na	k7c6
území	území	k1gNnSc6
světadílu	světadíl	k1gInSc2
považovat	považovat	k5eAaImF
za	za	k7c4
„	„	k?
<g/>
ohrožení	ohrožení	k1gNnSc4
svého	svůj	k3xOyFgInSc2
míru	mír	k1gInSc2
a	a	k8xC
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
manifestaci	manifestace	k1gFnSc4
nepřátelského	přátelský	k2eNgInSc2d1
postoje	postoj	k1gInSc2
vůči	vůči	k7c3
USA	USA	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
vyjadřují	vyjadřovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
a	a	k8xC
nikdy	nikdy	k6eAd1
nebylo	být	k5eNaImAgNnS
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
zájmu	zájem	k1gInSc6
nějak	nějak	k6eAd1
zasahovat	zasahovat	k5eAaImF
do	do	k7c2
válek	válka	k1gFnPc2
mocností	mocnost	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
vážně	vážně	k6eAd1
ohrožena	ohrožen	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SCHÄFER	SCHÄFER	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezidenti	prezident	k1gMnPc1
USA	USA	kA
<g/>
:	:	kIx,
od	od	k7c2
George	Georg	k1gMnSc2
Washingtona	Washington	k1gMnSc2
po	po	k7c4
Billa	Bill	k1gMnSc4
Clintona	Clinton	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
538	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
499	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
72	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Monroeova	Monroeův	k2eAgFnSc1d1
doktrína	doktrína	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Our	Our	k?
Documents	Documents	k1gInSc1
-	-	kIx~
přepis	přepis	k1gInSc1
Monroeovy	Monroeův	k2eAgFnSc2d1
doktríny	doktrína	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4170489-7	4170489-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85086988	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85086988	#num#	k4
</s>
