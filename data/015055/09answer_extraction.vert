nemají	mít	k5eNaImIp3nP
právo	právo	k1gNnSc4
zasahovat	zasahovat	k5eAaImF
do	do	k7c2
záležitostí	záležitost	k1gFnPc2
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
na	na	k7c6
území	území	k1gNnSc6
amerického	americký	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
a	a	k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
budou	být	k5eAaImBp3nP
jejich	jejich	k3xOp3gFnSc4
případné	případný	k2eAgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
akce	akce	k1gFnPc1
na	na	k7c6
území	území	k1gNnSc6
světadílu	světadíl	k1gInSc2
považovat	považovat	k5eAaImF
za	za	k7c4
„	„	k?
<g/>
ohrožení	ohrožení	k1gNnSc4
svého	svůj	k3xOyFgInSc2
míru	mír	k1gInSc2
a	a	k8xC
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
manifestaci	manifestace	k1gFnSc4
nepřátelského	přátelský	k2eNgInSc2d1
postoje	postoj	k1gInSc2
vůči	vůči	k7c3
USA	USA	kA
<g/>
“	“	k?
