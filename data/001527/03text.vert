<s>
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc4	William
Henry	henry	k1gInSc2	henry
Bragg	Bragga	k1gFnPc2	Bragga
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
Westward	Westward	k1gInSc1	Westward
u	u	k7c2	u
Wigtonu	Wigton	k1gInSc2	Wigton
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnSc2	hrabství
Cumbria	Cumbrium	k1gNnSc2	Cumbrium
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Williama	William	k1gMnSc2	William
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
Bragga	Bragg	k1gMnSc2	Bragg
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
strukturní	strukturní	k2eAgFnSc2d1	strukturní
analýzy	analýza	k1gFnSc2	analýza
a	a	k8xC	a
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
-	-	kIx~	-
1940	[number]	k4	1940
byl	být	k5eAaImAgInS	být
prezidentem	prezident	k1gMnSc7	prezident
Royal	Royal	k1gInSc4	Royal
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
pohlcováním	pohlcování	k1gNnSc7	pohlcování
a	a	k8xC	a
ionizací	ionizace	k1gFnSc7	ionizace
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
od	od	k7c2	od
vzdalenosti	vzdalenost	k1gFnSc2	vzdalenost
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
schopností	schopnost	k1gFnSc7	schopnost
materiálů	materiál	k1gInPc2	materiál
brzdit	brzdit	k5eAaImF	brzdit
elektrony	elektron	k1gInPc4	elektron
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
od	od	k7c2	od
atomového	atomový	k2eAgNnSc2d1	atomové
čísla	číslo	k1gNnSc2	číslo
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Braggův-Grayův	Braggův-Grayův	k2eAgInSc1d1	Braggův-Grayův
vztah	vztah	k1gInSc1	vztah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rentgenovým	rentgenový	k2eAgNnSc7d1	rentgenové
zářením	záření	k1gNnSc7	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
dlouho	dlouho	k6eAd1	dlouho
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
proud	proud	k1gInSc4	proud
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
v	v	k7c6	v
roku	rok	k1gInSc6	rok
1913	[number]	k4	1913
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
Braggovu	Braggův	k2eAgFnSc4d1	Braggova
metodu	metoda	k1gFnSc4	metoda
(	(	kIx(	(
<g/>
metodu	metoda	k1gFnSc4	metoda
otočného	otočný	k2eAgInSc2d1	otočný
krystalu	krystal	k1gInSc2	krystal
<g/>
)	)	kIx)	)
na	na	k7c4	na
určovaní	určovaný	k2eAgMnPc1d1	určovaný
krystalové	krystalový	k2eAgNnSc1d1	krystalové
struktury	struktura	k1gFnPc4	struktura
pomocí	pomocí	k7c2	pomocí
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
umožnila	umožnit	k5eAaPmAgFnS	umožnit
zjistit	zjistit	k5eAaPmF	zjistit
krystalovou	krystalový	k2eAgFnSc4d1	krystalová
strukturu	struktura	k1gFnSc4	struktura
mnohých	mnohý	k2eAgFnPc2d1	mnohá
anorganických	anorganický	k2eAgFnPc2d1	anorganická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
diamantu	diamant	k1gInSc2	diamant
či	či	k8xC	či
kamenné	kamenný	k2eAgFnSc2d1	kamenná
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
dostali	dostat	k5eAaPmAgMnP	dostat
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
například	například	k6eAd1	například
Studies	Studies	k1gInSc1	Studies
in	in	k?	in
radioactivity	radioactivita	k1gFnPc1	radioactivita
<g/>
,	,	kIx,	,
X-rays	Xays	k1gInSc1	X-rays
and	and	k?	and
crystal	crystat	k5eAaImAgInS	crystat
structure	structur	k1gMnSc5	structur
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
world	world	k1gMnSc1	world
of	of	k?	of
sound	sound	k1gInSc1	sound
<g/>
,	,	kIx,	,
Concerning	Concerning	k1gInSc1	Concerning
the	the	k?	the
nature	natur	k1gMnSc5	natur
of	of	k?	of
things	thingsa	k1gFnPc2	thingsa
<g/>
,	,	kIx,	,
Old	Olda	k1gFnPc2	Olda
trades	tradesa	k1gFnPc2	tradesa
and	and	k?	and
new	new	k?	new
knowledge	knowledge	k1gInSc1	knowledge
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
universe	universe	k1gFnSc2	universe
of	of	k?	of
light	light	k1gMnSc1	light
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Z	z	k7c2	z
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
dvěma	dva	k4xCgInPc7	dva
přírodními	přírodní	k2eAgInPc7d1	přírodní
jevy	jev	k1gInPc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Přitažlivostí	přitažlivost	k1gFnSc7	přitažlivost
a	a	k8xC	a
třením	tření	k1gNnSc7	tření
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
William	William	k1gInSc4	William
Henry	henry	k1gInSc4	henry
Bragg	Bragga	k1gFnPc2	Bragga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
