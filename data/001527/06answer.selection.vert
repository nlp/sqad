<s>
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc4	William
Henry	henry	k1gInSc2	henry
Bragg	Bragga	k1gFnPc2	Bragga
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
Westward	Westward	k1gInSc1	Westward
u	u	k7c2	u
Wigtonu	Wigton	k1gInSc2	Wigton
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnSc2	hrabství
Cumbria	Cumbrium	k1gNnSc2	Cumbrium
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Williama	William	k1gMnSc2	William
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
Bragga	Bragg	k1gMnSc2	Bragg
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
strukturní	strukturní	k2eAgFnSc2d1	strukturní
analýzy	analýza	k1gFnSc2	analýza
a	a	k8xC	a
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
<g/>
.	.	kIx.	.
</s>
