<s>
Všichni	všechen	k3xTgMnPc1	všechen
žáci	žák	k1gMnPc1	žák
Zmijozelu	Zmijozela	k1gFnSc4	Zmijozela
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
alespoň	alespoň	k9	alespoň
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
potomky	potomek	k1gMnPc7	potomek
čistokrevných	čistokrevný	k2eAgFnPc2d1	čistokrevná
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
není	být	k5eNaImIp3nS	být
často	často	k6eAd1	často
dodržováno	dodržován	k2eAgNnSc1d1	dodržováno
<g/>
.	.	kIx.	.
</s>
