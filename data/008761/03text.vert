<p>
<s>
Poledník	poledník	k1gInSc1	poledník
je	být	k5eAaImIp3nS	být
pomyslná	pomyslný	k2eAgFnSc1d1	pomyslná
čára	čára	k1gFnSc1	čára
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
průnikem	průnik	k1gInSc7	průnik
nějaké	nějaký	k3yIgFnSc2	nějaký
poloroviny	polorovina	k1gFnSc2	polorovina
<g/>
,	,	kIx,	,
určené	určený	k2eAgNnSc1d1	určené
zemskou	zemský	k2eAgFnSc7d1	zemská
osou	osa	k1gFnSc7	osa
<g/>
,	,	kIx,	,
a	a	k8xC	a
povrchu	povrch	k1gInSc3	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Poledníky	poledník	k1gInPc1	poledník
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
nejkratšími	krátký	k2eAgFnPc7d3	nejkratší
spojnicemi	spojnice	k1gFnPc7	spojnice
severního	severní	k2eAgInSc2d1	severní
a	a	k8xC	a
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
vedoucími	vedoucí	k2eAgInPc7d1	vedoucí
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
poledník	poledník	k1gInSc1	poledník
definovat	definovat	k5eAaBmF	definovat
i	i	k9	i
pro	pro	k7c4	pro
jiná	jiný	k2eAgNnPc4d1	jiné
vesmírná	vesmírný	k2eAgNnPc4d1	vesmírné
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
základní	základní	k2eAgInSc4d1	základní
poledník	poledník	k1gInSc4	poledník
(	(	kIx(	(
<g/>
též	též	k9	též
nultý	nultý	k4xOgInSc1	nultý
poledník	poledník	k1gInSc1	poledník
<g/>
)	)	kIx)	)
zvolen	zvolit	k5eAaPmNgInS	zvolit
poledník	poledník	k1gInSc1	poledník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
astronomickou	astronomický	k2eAgFnSc7d1	astronomická
observatoří	observatoř	k1gFnSc7	observatoř
v	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
poledníky	poledník	k1gInPc1	poledník
jsou	být	k5eAaImIp3nP	být
identifikovány	identifikovat	k5eAaBmNgInP	identifikovat
pomocí	pomocí	k7c2	pomocí
úhlu	úhel	k1gInSc2	úhel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
svírají	svírat	k5eAaImIp3nP	svírat
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
základním	základní	k2eAgInSc7d1	základní
poledníkem	poledník	k1gInSc7	poledník
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
úhlu	úhel	k1gInSc2	úhel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jejich	jejich	k3xOp3gFnSc1	jejich
poloroviny	polorovina	k1gFnPc1	polorovina
svírají	svírat	k5eAaImIp3nP	svírat
s	s	k7c7	s
polorovinou	polorovina	k1gFnSc7	polorovina
nultého	nultý	k4xOgInSc2	nultý
poledníku	poledník	k1gInSc2	poledník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
údaj	údaj	k1gInSc1	údaj
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
Poledníky	poledník	k1gInPc1	poledník
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Greenwiche	Greenwich	k1gInSc2	Greenwich
mají	mít	k5eAaImIp3nP	mít
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
0	[number]	k4	0
až	až	k9	až
180	[number]	k4	180
stupňů	stupeň	k1gInPc2	stupeň
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
poledníky	poledník	k1gInPc4	poledník
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
0	[number]	k4	0
až	až	k9	až
180	[number]	k4	180
stupňů	stupeň	k1gInPc2	stupeň
západní	západní	k2eAgFnSc2d1	západní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
vesmírných	vesmírný	k2eAgNnPc6d1	vesmírné
tělesech	těleso	k1gNnPc6	těleso
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
stejný	stejný	k2eAgInSc4d1	stejný
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zvolit	zvolit	k5eAaPmF	zvolit
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
počátek	počátek	k1gInSc4	počátek
(	(	kIx(	(
<g/>
nultý	nultý	k4xOgMnSc1	nultý
poledník	poledník	k1gMnSc1	poledník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
prochází	procházet	k5eAaImIp3nS	procházet
nultý	nultý	k4xOgMnSc1	nultý
poledník	poledník	k1gMnSc1	poledník
středem	středem	k7c2	středem
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
pólu	pól	k1gInSc2	pól
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
rovníku	rovník	k1gInSc3	rovník
se	se	k3xPyFc4	se
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
poledníky	poledník	k1gMnPc7	poledník
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
svého	svůj	k3xOyFgNnSc2	svůj
maxima	maximum	k1gNnSc2	maximum
111,324	[number]	k4	111,324
km	km	kA	km
<g/>
;	;	kIx,	;
na	na	k7c4	na
50	[number]	k4	50
<g/>
.	.	kIx.	.
stupni	stupeň	k1gInPc7	stupeň
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
jen	jen	k9	jen
71,556	[number]	k4	71,556
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
d	d	k?	d
dvou	dva	k4xCgInPc2	dva
poledníků	poledník	k1gInPc2	poledník
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
vzorce	vzorec	k1gInPc4	vzorec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
180	[number]	k4	180
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
180	[number]	k4	180
<g/>
}}	}}	k?	}}
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
phi	phi	k?	phi
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
pi	pi	k0	pi
;	;	kIx,	;
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
pí	pí	k1gNnSc2	pí
<g/>
,	,	kIx,	,
rz	rz	k?	rz
poloměr	poloměr	k1gInSc4	poloměr
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
phi	phi	k?	phi
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
počítáme	počítat	k5eAaImIp1nP	počítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
všech	všecek	k3xTgInPc2	všecek
poledníků	poledník	k1gInPc2	poledník
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
a	a	k8xC	a
činí	činit	k5eAaImIp3nS	činit
od	od	k7c2	od
pólu	pól	k1gInSc2	pól
k	k	k7c3	k
pólu	pól	k1gInSc3	pól
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
4,576	[number]	k4	4,576
<g/>
5	[number]	k4	5
km	km	kA	km
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
obvod	obvod	k1gInSc1	obvod
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
měřený	měřený	k2eAgInSc1d1	měřený
přes	přes	k7c4	přes
poledníky	poledník	k1gInPc4	poledník
činí	činit	k5eAaImIp3nS	činit
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
9,153	[number]	k4	9,153
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označováno	označovat	k5eAaImNgNnS	označovat
(	(	kIx(	(
<g/>
poněkud	poněkud	k6eAd1	poněkud
nepřesně	přesně	k6eNd1	přesně
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
poledníková	poledníkový	k2eAgFnSc1d1	Poledníková
kružnice	kružnice	k1gFnSc1	kružnice
(	(	kIx(	(
<g/>
správněji	správně	k6eAd2	správně
elipsa	elipsa	k1gFnSc1	elipsa
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
pólem	pól	k1gInSc7	pól
a	a	k8xC	a
rovníkem	rovník	k1gInSc7	rovník
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
kvadrant	kvadrant	k1gInSc1	kvadrant
(	(	kIx(	(
<g/>
čtvrtkruh	čtvrtkruh	k1gInSc1	čtvrtkruh
<g/>
,	,	kIx,	,
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
2,288	[number]	k4	2,288
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
:	:	kIx,	:
délka	délka	k1gFnSc1	délka
rovníku	rovník	k1gInSc2	rovník
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
40	[number]	k4	40
075	[number]	k4	075
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
poledníků	poledník	k1gInPc2	poledník
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k8xS	jako
výchozí	výchozí	k2eAgFnSc1d1	výchozí
veličina	veličina	k1gFnSc1	veličina
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
délky	délka	k1gFnSc2	délka
metru	metr	k1gInSc2	metr
jako	jako	k8xC	jako
základní	základní	k2eAgFnSc2d1	základní
jednotky	jednotka	k1gFnSc2	jednotka
metrického	metrický	k2eAgInSc2d1	metrický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rotace	rotace	k1gFnPc1	rotace
země	zem	k1gFnPc1	zem
slouží	sloužit	k5eAaImIp3nP	sloužit
poledníky	poledník	k1gMnPc4	poledník
i	i	k9	i
k	k	k7c3	k
dělení	dělení	k1gNnSc3	dělení
Země	zem	k1gFnSc2	zem
do	do	k7c2	do
časových	časový	k2eAgNnPc2d1	časové
pásem	pásmo	k1gNnPc2	pásmo
<g/>
:	:	kIx,	:
časový	časový	k2eAgInSc1d1	časový
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
jedním	jeden	k4xCgInSc7	jeden
stupněm	stupeň	k1gInSc7	stupeň
činí	činit	k5eAaImIp3nS	činit
4	[number]	k4	4
minuty	minuta	k1gFnSc2	minuta
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
15	[number]	k4	15
stupni	stupeň	k1gInPc7	stupeň
pak	pak	k6eAd1	pak
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
při	při	k7c6	při
360	[number]	k4	360
stupních	stupeň	k1gInPc6	stupeň
znamená	znamenat	k5eAaImIp3nS	znamenat
1440	[number]	k4	1440
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
poledník	poledník	k1gInSc1	poledník
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
meridies	meridiesa	k1gFnPc2	meridiesa
<g/>
,	,	kIx,	,
poledne	poledne	k1gNnSc4	poledne
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
používán	používat	k5eAaImNgInS	používat
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
meridián	meridián	k1gInSc1	meridián
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgNnSc4d1	sluneční
poledne	poledne	k1gNnSc4	poledne
nastává	nastávat	k5eAaImIp3nS	nastávat
nad	nad	k7c7	nad
daným	daný	k2eAgInSc7d1	daný
poledníkem	poledník	k1gInSc7	poledník
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
okamžik	okamžik	k1gInSc4	okamžik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
</s>
</p>
<p>
<s>
datová	datový	k2eAgFnSc1d1	datová
hranice	hranice	k1gFnSc1	hranice
</s>
</p>
<p>
<s>
Degree	Degree	k1gFnSc1	Degree
Confluence	Confluence	k1gFnSc2	Confluence
Project	Projecta	k1gFnPc2	Projecta
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
poledník	poledník	k1gInSc1	poledník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
poledník	poledník	k1gInSc1	poledník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
