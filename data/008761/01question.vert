<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
pomyslná	pomyslný	k2eAgFnSc1d1	pomyslná
čára	čára	k1gFnSc1	čára
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
průnikem	průnik	k1gInSc7	průnik
nějaké	nějaký	k3yIgFnSc2	nějaký
poloroviny	polorovina	k1gFnSc2	polorovina
<g/>
,	,	kIx,	,
určené	určený	k2eAgNnSc1d1	určené
zemskou	zemský	k2eAgFnSc7d1	zemská
osou	osa	k1gFnSc7	osa
<g/>
,	,	kIx,	,
a	a	k8xC	a
povrchu	povrch	k1gInSc3	povrch
Země	zem	k1gFnSc2	zem
<g/>
?	?	kIx.	?
</s>
