<s>
Tykev	tykev	k1gFnSc1	tykev
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
tykvovité	tykvovitý	k2eAgFnSc2d1	tykvovitá
(	(	kIx(	(
<g/>
Cucurbitaceae	Cucurbitacea	k1gFnSc2	Cucurbitacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
druhy	druh	k1gInPc1	druh
a	a	k8xC	a
variety	varieta	k1gFnPc1	varieta
tykví	tykev	k1gFnPc2	tykev
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
dýně	dýně	k1gFnSc1	dýně
<g/>
,	,	kIx,	,
cuketa	cuketa	k1gFnSc1	cuketa
nebo	nebo	k8xC	nebo
patizon	patizon	k1gNnSc1	patizon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
plody	plod	k1gInPc1	plod
jako	jako	k8xS	jako
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
