<s>
Tykev	tykev	k1gFnSc1	tykev
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
tykvovité	tykvovitý	k2eAgFnSc2d1	tykvovitá
(	(	kIx(	(
<g/>
Cucurbitaceae	Cucurbitacea	k1gFnSc2	Cucurbitacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
druhy	druh	k1gInPc1	druh
a	a	k8xC	a
variety	varieta	k1gFnPc1	varieta
tykví	tykev	k1gFnPc2	tykev
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
dýně	dýně	k1gFnSc1	dýně
<g/>
,	,	kIx,	,
cuketa	cuketa	k1gFnSc1	cuketa
nebo	nebo	k8xC	nebo
patizon	patizon	k1gNnSc1	patizon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
plody	plod	k1gInPc1	plod
jako	jako	k8xC	jako
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoletou	jednoletý	k2eAgFnSc4d1	jednoletá
popínavou	popínavý	k2eAgFnSc4d1	popínavá
rostlinu	rostlina	k1gFnSc4	rostlina
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
koncem	koncem	k7c2	koncem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
španělskými	španělský	k2eAgFnPc7d1	španělská
kolumbovskými	kolumbovský	k2eAgFnPc7d1	kolumbovská
výpravami	výprava	k1gFnPc7	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Dýně	dýně	k1gFnSc1	dýně
je	být	k5eAaImIp3nS	být
lidový	lidový	k2eAgInSc4d1	lidový
název	název	k1gInSc4	název
nejčastěji	často	k6eAd3	často
pro	pro	k7c4	pro
tykev	tykev	k1gFnSc4	tykev
obecnou	obecný	k2eAgFnSc4d1	obecná
(	(	kIx(	(
<g/>
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
také	také	k6eAd1	také
tykev	tykev	k1gFnSc4	tykev
turek	turek	k1gInSc1	turek
nebo	nebo	k8xC	nebo
dýně	dýně	k1gFnSc1	dýně
obecná	obecná	k1gFnSc1	obecná
<g/>
.	.	kIx.	.
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Cucurbita	Cucurbita	k1gMnSc1	Cucurbita
pepo	pepo	k1gMnSc1	pepo
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
varieta	varieta	k1gFnSc1	varieta
pepo	pepo	k1gMnSc1	pepo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tykev	tykev	k1gFnSc4	tykev
velkoplodou	velkoplodý	k2eAgFnSc4d1	velkoplodá
(	(	kIx(	(
<g/>
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
také	také	k9	také
dýně	dýně	k1gFnSc1	dýně
velkoplodá	velkoplodý	k2eAgFnSc1d1	velkoplodá
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Cucurbita	Cucurbit	k2eAgFnSc1d1	Cucurbita
maxima	maxima	k1gFnSc1	maxima
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
