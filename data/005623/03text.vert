<s>
Sršeň	sršeň	k1gFnSc1	sršeň
(	(	kIx(	(
<g/>
Vespa	Vespa	k1gFnSc1	Vespa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
blanokřídlého	blanokřídlý	k2eAgInSc2d1	blanokřídlý
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
největší	veliký	k2eAgFnPc4d3	veliký
sociální	sociální	k2eAgFnPc4d1	sociální
vosy	vosa	k1gFnPc4	vosa
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
sršně	sršeň	k1gFnPc1	sršeň
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
až	až	k9	až
45	[number]	k4	45
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
žije	žít	k5eAaImIp3nS	žít
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
–	–	k?	–
sršeň	sršeň	k1gFnSc1	sršeň
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Vespa	Vespa	k1gFnSc1	Vespa
crabro	crabro	k6eAd1	crabro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spisovně	spisovně	k6eAd1	spisovně
česky	česky	k6eAd1	česky
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc4	ten
i	i	k9	i
ta	ten	k3xDgFnSc1	ten
sršeň	sršeň	k1gFnSc1	sršeň
(	(	kIx(	(
<g/>
Příruční	příruční	k2eAgInSc1d1	příruční
slovník	slovník	k1gInSc1	slovník
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
Internetová	internetový	k2eAgFnSc1d1	internetová
jazyková	jazykový	k2eAgFnSc1d1	jazyková
příručka	příručka	k1gFnSc1	příručka
ÚJČ	ÚJČ	kA	ÚJČ
uvádějí	uvádět	k5eAaImIp3nP	uvádět
ženský	ženský	k2eAgInSc4d1	ženský
rod	rod	k1gInSc4	rod
jako	jako	k8xS	jako
méně	málo	k6eAd2	málo
častý	častý	k2eAgInSc1d1	častý
<g/>
,	,	kIx,	,
v	v	k7c6	v
zoologii	zoologie	k1gFnSc6	zoologie
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
preferovaný	preferovaný	k2eAgMnSc1d1	preferovaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
i	i	k8xC	i
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
)	)	kIx)	)
sršán	sršán	k1gMnSc1	sršán
<g/>
,	,	kIx,	,
sršáň	sršáň	k1gMnSc1	sršáň
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgInPc4d1	vhodný
oba	dva	k4xCgInPc4	dva
možné	možný	k2eAgInPc4d1	možný
rody	rod	k1gInPc4	rod
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
článku	článek	k1gInSc6	článek
kombinovat	kombinovat	k5eAaImF	kombinovat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
sršní	sršeň	k1gFnPc2	sršeň
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
subtropech	subtropy	k1gInPc6	subtropy
žije	žít	k5eAaImIp3nS	žít
V.	V.	kA	V.
orientalis	orientalis	k1gInSc1	orientalis
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
areál	areál	k1gInSc1	areál
leží	ležet	k5eAaImIp3nS	ležet
kromě	kromě	k7c2	kromě
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
i	i	k9	i
na	na	k7c6	na
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
pásmu	pásmo	k1gNnSc6	pásmo
žije	žít	k5eAaImIp3nS	žít
V.	V.	kA	V.
simillima	simillima	k1gFnSc1	simillima
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k6eAd1	již
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
evropská	evropský	k2eAgFnSc1d1	Evropská
sršeň	sršeň	k1gFnSc1	sršeň
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
introdukována	introdukovat	k5eAaImNgFnS	introdukovat
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Sršně	sršně	k6eAd1	sršně
jsou	být	k5eAaImIp3nP	být
dravci	dravec	k1gMnPc7	dravec
<g/>
,	,	kIx,	,
živí	živit	k5eAaImIp3nS	živit
se	s	k7c7	s
především	především	k6eAd1	především
jinými	jiný	k2eAgMnPc7d1	jiný
bezobratlými	bezobratlí	k1gMnPc7	bezobratlí
nebo	nebo	k8xC	nebo
sbírají	sbírat	k5eAaImIp3nP	sbírat
sladké	sladký	k2eAgFnPc4d1	sladká
ovocné	ovocný	k2eAgFnPc4d1	ovocná
šťávy	šťáva	k1gFnPc4	šťáva
nebo	nebo	k8xC	nebo
med	med	k1gInSc4	med
z	z	k7c2	z
vyloupených	vyloupený	k2eAgNnPc2d1	vyloupené
včelích	včelí	k2eAgNnPc2d1	včelí
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
sršeň	sršeň	k1gFnSc1	sršeň
dokáže	dokázat	k5eAaPmIp3nS	dokázat
denně	denně	k6eAd1	denně
ulovit	ulovit	k5eAaPmF	ulovit
až	až	k9	až
pět	pět	k4xCc4	pět
kusů	kus	k1gInPc2	kus
much	moucha	k1gFnPc2	moucha
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
rozšířené	rozšířený	k2eAgFnSc3d1	rozšířená
představě	představa	k1gFnSc3	představa
o	o	k7c6	o
smrtelně	smrtelně	k6eAd1	smrtelně
nebezpečném	bezpečný	k2eNgNnSc6d1	nebezpečné
tvorovi	tvor	k1gMnSc3	tvor
je	být	k5eAaImIp3nS	být
sršeň	sršeň	k1gFnSc1	sršeň
obecná	obecná	k1gFnSc1	obecná
mírumilovná	mírumilovný	k2eAgFnSc1d1	mírumilovná
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
neškodná	škodný	k2eNgFnSc1d1	neškodná
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obav	obava	k1gFnPc2	obava
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
velikosti	velikost	k1gFnSc2	velikost
sršně	sršeň	k1gFnSc2	sršeň
a	a	k8xC	a
jejího	její	k3xOp3gNnSc2	její
hlasitého	hlasitý	k2eAgNnSc2d1	hlasité
bzučení	bzučení	k1gNnSc2	bzučení
<g/>
.	.	kIx.	.
</s>
<s>
Varovné	varovný	k2eAgNnSc4d1	varovné
bzučení	bzučení	k1gNnSc4	bzučení
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
na	na	k7c4	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
asi	asi	k9	asi
50	[number]	k4	50
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Sršeň	sršeň	k1gFnSc1	sršeň
také	také	k9	také
reaguje	reagovat	k5eAaBmIp3nS	reagovat
podrážděně	podrážděně	k6eAd1	podrážděně
při	při	k7c6	při
prudkých	prudký	k2eAgInPc6d1	prudký
otřesech	otřes	k1gInPc6	otřes
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
svého	svůj	k3xOyFgNnSc2	svůj
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
manipulaci	manipulace	k1gFnSc4	manipulace
u	u	k7c2	u
výletového	výletový	k2eAgInSc2d1	výletový
otvoru	otvor	k1gInSc2	otvor
či	či	k8xC	či
přehrazení	přehrazení	k1gNnSc2	přehrazení
příletové	příletový	k2eAgFnSc2d1	příletová
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Sršně	sršně	k6eAd1	sršně
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
základy	základ	k1gInPc1	základ
sama	sám	k3xTgFnSc1	sám
vybuduje	vybudovat	k5eAaPmIp3nS	vybudovat
královna-zakladatelka	královnaakladatelka	k1gFnSc1	královna-zakladatelka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
kusadly	kusadla	k1gNnPc7	kusadla
nastrouhá	nastrouhat	k5eAaPmIp3nS	nastrouhat
kousky	kousek	k1gInPc4	kousek
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
smíchá	smíchat	k5eAaPmIp3nS	smíchat
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
slinami	slina	k1gFnPc7	slina
a	a	k8xC	a
ze	z	k7c2	z
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
papírové	papírový	k2eAgFnSc2d1	papírová
hmoty	hmota	k1gFnSc2	hmota
buduje	budovat	k5eAaImIp3nS	budovat
odshora	odshora	k6eAd1	odshora
dolů	dolů	k6eAd1	dolů
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc4	první
potomky	potomek	k1gMnPc4	potomek
vychovává	vychovávat	k5eAaImIp3nS	vychovávat
sama	sám	k3xTgFnSc1	sám
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
dělnice	dělnice	k1gFnPc1	dělnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
převezmou	převzít	k5eAaPmIp3nP	převzít
veškerou	veškerý	k3xTgFnSc4	veškerý
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Těch	ten	k3xDgFnPc2	ten
bývá	bývat	k5eAaImIp3nS	bývat
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
<g/>
,	,	kIx,	,
shánět	shánět	k5eAaImF	shánět
potravu	potrava	k1gFnSc4	potrava
však	však	k9	však
vylétá	vylétat	k5eAaPmIp3nS	vylétat
jen	jen	k9	jen
asi	asi	k9	asi
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
pak	pak	k6eAd1	pak
z	z	k7c2	z
hnízda	hnízdo	k1gNnSc2	hnízdo
vylétají	vylétat	k5eAaPmIp3nP	vylétat
oplodněné	oplodněný	k2eAgFnPc1d1	oplodněná
samičky	samička	k1gFnPc1	samička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
založí	založit	k5eAaPmIp3nP	založit
nová	nový	k2eAgNnPc1d1	nové
hnízda	hnízdo	k1gNnPc1	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Společenství	společenství	k1gNnSc1	společenství
sršní	sršeň	k1gFnPc2	sršeň
totiž	totiž	k9	totiž
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
včel	včela	k1gFnPc2	včela
nepřežívá	přežívat	k5eNaImIp3nS	přežívat
zimu	zima	k1gFnSc4	zima
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
již	již	k6eAd1	již
zmiňovaných	zmiňovaný	k2eAgFnPc2d1	zmiňovaná
budoucích	budoucí	k2eAgFnPc2d1	budoucí
královen	královna	k1gFnPc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
usedne	usednout	k5eAaPmIp3nS	usednout
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
není	být	k5eNaImIp3nS	být
potravou	potrava	k1gFnSc7	potrava
a	a	k8xC	a
během	během	k7c2	během
pár	pár	k4xCyI	pár
sekund	sekunda	k1gFnPc2	sekunda
odletí	odletět	k5eAaPmIp3nS	odletět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
nutné	nutný	k2eAgNnSc1d1	nutné
se	se	k3xPyFc4	se
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
prudkých	prudký	k2eAgInPc2d1	prudký
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
mohly	moct	k5eAaImAgInP	moct
rozrušit	rozrušit	k5eAaPmF	rozrušit
<g/>
.	.	kIx.	.
</s>
<s>
Sršeň	sršeň	k1gFnSc1	sršeň
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
–	–	k?	–
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
<g/>
,	,	kIx,	,
chceme	chtít	k5eAaImIp1nP	chtít
<g/>
-li	i	k?	-li
ji	on	k3xPp3gFnSc4	on
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgNnSc2	který
vlétla	vlétnout	k5eAaPmAgFnS	vlétnout
<g/>
.	.	kIx.	.
</s>
<s>
Sršně	sršeň	k1gFnPc1	sršeň
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
žihadlem	žihadlo	k1gNnSc7	žihadlo
spojeným	spojený	k2eAgNnSc7d1	spojené
s	s	k7c7	s
jedovou	jedový	k2eAgFnSc7d1	jedová
žlázou	žláza	k1gFnSc7	žláza
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
žahadlovití	žahadlovitý	k2eAgMnPc1d1	žahadlovitý
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
žihadlo	žihadlo	k1gNnSc1	žihadlo
a	a	k8xC	a
jed	jed	k1gInSc1	jed
jim	on	k3xPp3gMnPc3	on
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k6eAd1	především
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
když	když	k8xS	když
některé	některý	k3yIgInPc1	některý
tropické	tropický	k2eAgInPc1d1	tropický
druhy	druh	k1gInPc1	druh
sršní	sršeň	k1gFnPc2	sršeň
jsou	být	k5eAaImIp3nP	být
útočné	útočný	k2eAgFnPc1d1	útočná
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
jed	jed	k1gInSc1	jed
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
jed	jed	k1gInSc1	jed
sršně	sršeň	k1gFnSc2	sršeň
obecné	obecný	k2eAgFnSc2d1	obecná
je	být	k5eAaImIp3nS	být
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
méně	málo	k6eAd2	málo
toxický	toxický	k2eAgInSc1d1	toxický
než	než	k8xS	než
jed	jed	k1gInSc1	jed
včely	včela	k1gFnSc2	včela
medonosné	medonosný	k2eAgFnSc2d1	medonosná
<g/>
.	.	kIx.	.
</s>
<s>
Bodnutí	bodnutí	k1gNnSc1	bodnutí
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
otoky	otok	k1gInPc7	otok
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jiné	jiný	k2eAgFnPc4d1	jiná
obtíže	obtíž	k1gFnPc4	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
-li	i	k?	-li
k	k	k7c3	k
zasažení	zasažení	k1gNnSc3	zasažení
jazyka	jazyk	k1gInSc2	jazyk
či	či	k8xC	či
jícnu	jícen	k1gInSc2	jícen
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
bodnutí	bodnutí	k1gNnSc1	bodnutí
(	(	kIx(	(
<g/>
ani	ani	k8xC	ani
několikanásobné	několikanásobný	k2eAgNnSc1d1	několikanásobné
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
zdravého	zdravý	k2eAgMnSc4d1	zdravý
člověka	člověk	k1gMnSc4	člověk
nikterak	nikterak	k6eAd1	nikterak
život	život	k1gInSc1	život
ohrožující	ohrožující	k2eAgInSc1d1	ohrožující
<g/>
.	.	kIx.	.
</s>
<s>
Bodnutí	bodnutí	k1gNnSc1	bodnutí
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
pro	pro	k7c4	pro
přecitlivělé	přecitlivělý	k2eAgFnPc4d1	přecitlivělá
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgMnPc2	který
hrozí	hrozit	k5eAaImIp3nS	hrozit
rozvoj	rozvoj	k1gInSc4	rozvoj
anafylaktického	anafylaktický	k2eAgInSc2d1	anafylaktický
šoku	šok	k1gInSc2	šok
<g/>
.	.	kIx.	.
</s>
<s>
Sršně	sršně	k6eAd1	sršně
navíc	navíc	k6eAd1	navíc
mají	mít	k5eAaImIp3nP	mít
jednu	jeden	k4xCgFnSc4	jeden
speciální	speciální	k2eAgFnSc4d1	speciální
vlastnost	vlastnost	k1gFnSc4	vlastnost
–	–	k?	–
dokáží	dokázat	k5eAaPmIp3nP	dokázat
jed	jed	k1gInSc4	jed
vystřikovat	vystřikovat	k5eAaImF	vystřikovat
až	až	k9	až
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
40	[number]	k4	40
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
<g/>
-li	i	k?	-li
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
pepřový	pepřový	k2eAgInSc1d1	pepřový
sprej	sprej	k1gInSc1	sprej
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
může	moct	k5eAaImIp3nS	moct
zasažení	zasažení	k1gNnSc4	zasažení
vyvolat	vyvolat	k5eAaPmF	vyvolat
zimnici	zimnice	k1gFnSc4	zimnice
či	či	k8xC	či
třesavku	třesavka	k1gFnSc4	třesavka
<g/>
,	,	kIx,	,
ty	ten	k3xDgMnPc4	ten
však	však	k9	však
do	do	k7c2	do
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
samy	sám	k3xTgFnPc4	sám
odezní	odeznět	k5eAaPmIp3nS	odeznět
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sršeň	sršeň	k1gFnSc1	sršeň
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sršeň	sršeň	k1gFnSc1	sršeň
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc4	taxon
Vespa	Vesp	k1gMnSc4	Vesp
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
Žihadlo	žihadlo	k1gNnSc1	žihadlo
sršně	sršně	k6eAd1	sršně
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
méně	málo	k6eAd2	málo
jedu	jet	k5eAaImIp1nS	jet
než	než	k8xS	než
žihadlo	žihadlo	k1gNnSc4	žihadlo
včely	včela	k1gFnSc2	včela
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Ekolist	Ekolist	k1gInSc1	Ekolist
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Sršeň	sršeň	k1gMnSc1	sršeň
-	-	kIx~	-
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
příjemný	příjemný	k2eAgMnSc1d1	příjemný
soused	soused	k1gMnSc1	soused
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zbavit	zbavit	k5eAaPmF	zbavit
sršního	sršní	k2eAgNnSc2d1	sršní
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
?	?	kIx.	?
</s>
