<s>
Nálezy	nález	k1gInPc1	nález
mumifikovaných	mumifikovaný	k2eAgNnPc2d1	mumifikované
kočičích	kočičí	k2eAgNnPc2d1	kočičí
těl	tělo	k1gNnPc2	tělo
a	a	k8xC	a
kočičích	kočičí	k2eAgInPc2d1	kočičí
amuletů	amulet	k1gInPc2	amulet
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
i	i	k9	i
v	v	k7c6	v
hrobech	hrob	k1gInPc6	hrob
faraónů	faraón	k1gMnPc2	faraón
dokládají	dokládat	k5eAaImIp3nP	dokládat
kultovní	kultovní	k2eAgNnSc4d1	kultovní
uctívání	uctívání	k1gNnSc4	uctívání
koček	kočka	k1gFnPc2	kočka
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měly	mít	k5eAaImAgFnP	mít
i	i	k9	i
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
bohyni	bohyně	k1gFnSc4	bohyně
-	-	kIx~	-
Bastet	Bastet	k1gInSc1	Bastet
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zobrazována	zobrazovat	k5eAaImNgFnS	zobrazovat
jako	jako	k8xS	jako
malá	malý	k2eAgFnSc1d1	malá
kočka	kočka	k1gFnSc1	kočka
se	s	k7c7	s
lví	lví	k2eAgFnSc7d1	lví
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
žena	žena	k1gFnSc1	žena
s	s	k7c7	s
kočičí	kočičí	k2eAgFnSc7d1	kočičí
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
