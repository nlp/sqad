<s>
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
(	(	kIx(	(
<g/>
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
Red	Red	k1gMnSc1	Red
Dwarf	Dwarf	k1gMnSc1	Dwarf
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgInSc1d1	britský
sitcom	sitcom	k1gInSc1	sitcom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
natáčený	natáčený	k2eAgInSc1d1	natáčený
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
a	a	k8xC	a
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgMnS	řadit
do	do	k7c2	do
žánru	žánr	k1gInSc2	žánr
sci-fi	scii	k1gFnSc2	sci-fi
coby	coby	k?	coby
parodie	parodie	k1gFnSc2	parodie
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
děj	děj	k1gInSc4	děj
tohoto	tento	k3xDgInSc2	tento
seriálu	seriál	k1gInSc2	seriál
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgInPc1	tři
milióny	milión	k4xCgInPc1	milión
let	léto	k1gNnPc2	léto
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
se	se	k3xPyFc4	se
také	také	k9	také
stal	stát	k5eAaPmAgInS	stát
kultovním	kultovní	k2eAgNnSc7d1	kultovní
dílem	dílo	k1gNnSc7	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
šest	šest	k4xCc1	šest
sezón	sezóna	k1gFnPc2	sezóna
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
po	po	k7c6	po
šesti	šest	k4xCc2	šest
epizodách	epizoda	k1gFnPc6	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přestávce	přestávka	k1gFnSc6	přestávka
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc4	dva
série	série	k1gFnPc4	série
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
měla	mít	k5eAaImAgFnS	mít
osm	osm	k4xCc4	osm
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
na	na	k7c4	na
52	[number]	k4	52
epizod	epizoda	k1gFnPc2	epizoda
(	(	kIx(	(
<g/>
vysíláno	vysílat	k5eAaImNgNnS	vysílat
na	na	k7c4	na
BBC	BBC	kA	BBC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
pauza	pauza	k1gFnSc1	pauza
<g/>
,	,	kIx,	,
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
byl	být	k5eAaImAgMnS	být
obnoven	obnovit	k5eAaPmNgMnS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
odvysílána	odvysílán	k2eAgFnSc1d1	odvysílána
třídílná	třídílný	k2eAgFnSc1d1	třídílná
minisérie	minisérie	k1gFnSc1	minisérie
"	"	kIx"	"
<g/>
Zpátky	zpátky	k6eAd1	zpátky
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
seriál	seriál	k1gInSc1	seriál
přesunul	přesunout	k5eAaPmAgInS	přesunout
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
Dave	Dav	k1gInSc5	Dav
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
diváckému	divácký	k2eAgInSc3d1	divácký
úspěchu	úspěch	k1gInSc3	úspěch
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
později	pozdě	k6eAd2	pozdě
natočena	natočit	k5eAaBmNgFnS	natočit
i	i	k9	i
desátá	desátý	k4xOgFnSc1	desátý
sezóna	sezóna	k1gFnSc1	sezóna
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
6	[number]	k4	6
dílů	díl	k1gInPc2	díl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
conu	conus	k1gInSc6	conus
Dimension	Dimension	k1gInSc1	Dimension
Jump	Jump	k1gInSc1	Jump
pořádaném	pořádaný	k2eAgInSc6d1	pořádaný
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
nemohl	moct	k5eNaImAgMnS	moct
autor	autor	k1gMnSc1	autor
Doug	Doug	k1gMnSc1	Doug
Naylor	Naylor	k1gMnSc1	Naylor
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
11	[number]	k4	11
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
rád	rád	k6eAd1	rád
natáčel	natáčet	k5eAaImAgMnS	natáčet
již	již	k6eAd1	již
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
možná	možná	k9	možná
vrátí	vrátit	k5eAaPmIp3nS	vrátit
postava	postava	k1gFnSc1	postava
počítače	počítač	k1gInSc2	počítač
Holly	Holla	k1gFnSc2	Holla
<g/>
.	.	kIx.	.
</s>
<s>
Herec	herec	k1gMnSc1	herec
Craig	Craig	k1gMnSc1	Craig
Charles	Charles	k1gMnSc1	Charles
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2013	[number]	k4	2013
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
chystá	chystat	k5eAaImIp3nS	chystat
natáčení	natáčení	k1gNnSc4	natáčení
nových	nový	k2eAgInPc2d1	nový
dílů	díl	k1gInPc2	díl
Červeného	Červený	k1gMnSc2	Červený
trpaslíka	trpaslík	k1gMnSc2	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Danny	Danna	k1gFnPc1	Danna
John-Jules	John-Julesa	k1gFnPc2	John-Julesa
později	pozdě	k6eAd2	pozdě
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
natáčení	natáčení	k1gNnSc1	natáčení
začne	začít	k5eAaPmIp3nS	začít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
odvysílání	odvysílání	k1gNnSc4	odvysílání
11	[number]	k4	11
<g/>
.	.	kIx.	.
řady	řada	k1gFnSc2	řada
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
Dave	Dav	k1gInSc5	Dav
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
oficiálně	oficiálně	k6eAd1	oficiálně
oznámila	oznámit	k5eAaPmAgFnS	oznámit
začátek	začátek	k1gInSc4	začátek
natáčení	natáčení	k1gNnSc2	natáčení
XI	XI	kA	XI
<g/>
.	.	kIx.	.
a	a	k8xC	a
XII	XII	kA	XII
<g/>
.	.	kIx.	.
série	série	k1gFnSc1	série
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
byla	být	k5eAaImAgFnS	být
odvysílána	odvysílán	k2eAgFnSc1d1	odvysílána
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
následovat	následovat	k5eAaImF	následovat
12	[number]	k4	12
<g/>
.	.	kIx.	.
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
postav	postava	k1gFnPc2	postava
seriálu	seriál	k1gInSc2	seriál
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dave	Dav	k1gInSc5	Dav
Lister	Lister	k1gMnSc1	Lister
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
letý	letý	k2eAgMnSc1d1	letý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
vzhled	vzhled	k1gInSc1	vzhled
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
pět	pět	k4xCc4	pět
dredů	dred	k1gInPc2	dred
vzadu	vzadu	k6eAd1	vzadu
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
pojídá	pojídat	k5eAaImIp3nS	pojídat
ostré	ostrý	k2eAgInPc4d1	ostrý
indické	indický	k2eAgInPc4d1	indický
pokrmy	pokrm	k1gInPc4	pokrm
(	(	kIx(	(
<g/>
kari	kari	k1gNnSc2	kari
a	a	k8xC	a
indické	indický	k2eAgFnSc2d1	indická
placky	placka	k1gFnSc2	placka
<g/>
)	)	kIx)	)
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
kytaru	kytara	k1gFnSc4	kytara
s	s	k7c7	s
pěti	pět	k4xCc7	pět
strunami	struna	k1gFnPc7	struna
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
tři	tři	k4xCgInPc1	tři
jsou	být	k5eAaImIp3nP	být
G.	G.	kA	G.
Listerovým	Listerův	k2eAgInSc7d1	Listerův
snem	sen	k1gInSc7	sen
je	být	k5eAaImIp3nS	být
odjet	odjet	k5eAaPmF	odjet
na	na	k7c6	na
Fidži	Fidž	k1gFnSc6	Fidž
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
láskou	láska	k1gFnSc7	láska
Kristinou	Kristin	k2eAgFnSc7d1	Kristina
Kochanskou	Kochanský	k2eAgFnSc7d1	Kochanská
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tam	tam	k6eAd1	tam
mohl	moct	k5eAaImAgInS	moct
chovat	chovat	k5eAaImF	chovat
ovce	ovce	k1gFnPc4	ovce
a	a	k8xC	a
krávy	kráva	k1gFnPc4	kráva
a	a	k8xC	a
křížit	křížit	k5eAaImF	křížit
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
událost	událost	k1gFnSc4	událost
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgMnSc7	svůj
vlastním	vlastní	k2eAgMnSc7d1	vlastní
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
Kristina	Kristina	k1gFnSc1	Kristina
Kochanská	Kochanský	k2eAgFnSc1d1	Kochanská
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kocour	kocour	k1gMnSc1	kocour
(	(	kIx(	(
<g/>
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Lister	Lister	k1gInSc1	Lister
byl	být	k5eAaImAgInS	být
zmrazen	zmrazit	k5eAaPmNgInS	zmrazit
ve	v	k7c6	v
stázovém	stázový	k2eAgNnSc6d1	stázový
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jeho	jeho	k3xOp3gNnSc1	jeho
kočka	kočka	k1gFnSc1	kočka
Frankenstein	Frankensteina	k1gFnPc2	Frankensteina
bezpečně	bezpečně	k6eAd1	bezpečně
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
v	v	k7c6	v
lodním	lodní	k2eAgNnSc6d1	lodní
skladišti	skladiště	k1gNnSc6	skladiště
a	a	k8xC	a
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
její	její	k3xOp3gMnPc1	její
potomci	potomek	k1gMnPc1	potomek
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
v	v	k7c4	v
humanoidní	humanoidní	k2eAgFnSc4d1	humanoidní
formu	forma	k1gFnSc4	forma
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
zachovala	zachovat	k5eAaPmAgFnS	zachovat
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
povahové	povahový	k2eAgInPc4d1	povahový
rysy	rys	k1gInPc4	rys
podobné	podobný	k2eAgInPc4d1	podobný
kočkám	kočka	k1gFnPc3	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rasa	rasa	k1gFnSc1	rasa
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Felis	Felis	k1gFnSc1	Felis
sapiens	sapiensa	k1gFnPc2	sapiensa
(	(	kIx(	(
<g/>
nesprávně	správně	k6eNd1	správně
Felix	Felix	k1gMnSc1	Felix
sapiens	sapiensa	k1gFnPc2	sapiensa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
vlastní	vlastní	k2eAgFnSc3d1	vlastní
civilizaci	civilizace	k1gFnSc3	civilizace
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnPc1	její
zástupci	zástupce	k1gMnPc1	zástupce
do	do	k7c2	do
knih	kniha	k1gFnPc2	kniha
místo	místo	k7c2	místo
písmen	písmeno	k1gNnPc2	písmeno
píší	psát	k5eAaImIp3nP	psát
pachem	pach	k1gInSc7	pach
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
teritorium	teritorium	k1gNnSc4	teritorium
si	se	k3xPyFc3	se
označují	označovat	k5eAaImIp3nP	označovat
voňavkou	voňavka	k1gFnSc7	voňavka
<g/>
.	.	kIx.	.
</s>
<s>
Kocour	kocour	k1gMnSc1	kocour
je	být	k5eAaImIp3nS	být
posledním	poslední	k2eAgMnSc7d1	poslední
žijícím	žijící	k2eAgMnSc7d1	žijící
zástupcem	zástupce	k1gMnSc7	zástupce
této	tento	k3xDgFnSc2	tento
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
jeho	on	k3xPp3gInSc2	on
druhu	druh	k1gInSc2	druh
buď	buď	k8xC	buď
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
hledat	hledat	k5eAaImF	hledat
svého	svůj	k3xOyFgMnSc4	svůj
boha	bůh	k1gMnSc4	bůh
Cloistera	Cloister	k1gMnSc4	Cloister
<g/>
.	.	kIx.	.
</s>
<s>
Kocourovy	kocourův	k2eAgInPc4d1	kocourův
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
orgány	orgán	k1gInPc4	orgán
včetně	včetně	k7c2	včetně
stěn	stěna	k1gFnPc2	stěna
žaludku	žaludek	k1gInSc2	žaludek
jsou	být	k5eAaImIp3nP	být
barevně	barevně	k6eAd1	barevně
sladěny	sladěn	k2eAgInPc1d1	sladěn
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
rytmičtější	rytmický	k2eAgInSc1d2	rytmičtější
<g/>
"	"	kIx"	"
tep	tep	k1gInSc1	tep
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
času	čas	k1gInSc2	čas
stráví	strávit	k5eAaPmIp3nP	strávit
paráděním	parádění	k1gNnSc7	parádění
se	se	k3xPyFc4	se
a	a	k8xC	a
obhlížením	obhlížení	k1gNnPc3	obhlížení
se	se	k3xPyFc4	se
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
dle	dle	k7c2	dle
kočičí	kočičí	k2eAgFnSc2d1	kočičí
Bible	bible	k1gFnSc2	bible
byla	být	k5eAaImAgFnS	být
Matka	matka	k1gFnSc1	matka
boží	boží	k2eAgFnSc1d1	boží
(	(	kIx(	(
<g/>
Frankenstein	Frankenstein	k1gMnSc1	Frankenstein
<g/>
)	)	kIx)	)
zachráněna	zachráněn	k2eAgFnSc1d1	zachráněna
Cloisterem	Cloister	k1gMnSc7	Cloister
Hlupákem	hlupák	k1gMnSc7	hlupák
(	(	kIx(	(
<g/>
Listerem	Lister	k1gInSc7	Lister
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
hibernovat	hibernovat	k5eAaPmF	hibernovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
zachránil	zachránit	k5eAaPmAgMnS	zachránit
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kočičí	kočičí	k2eAgInSc1d1	kočičí
lid	lid	k1gInSc1	lid
odvedl	odvést	k5eAaPmAgInS	odvést
na	na	k7c4	na
Fušál	Fušál	k1gInSc4	Fušál
(	(	kIx(	(
<g/>
Fidži	Fidž	k1gFnSc6	Fidž
<g/>
)	)	kIx)	)
do	do	k7c2	do
země	zem	k1gFnSc2	zem
zaslíbené	zaslíbený	k2eAgFnSc2d1	zaslíbená
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
zdá	zdát	k5eAaImIp3nS	zdát
absurdní	absurdní	k2eAgMnSc1d1	absurdní
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
Lister	Lister	k1gInSc1	Lister
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
civilizaci	civilizace	k1gFnSc4	civilizace
bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Arnold	Arnold	k1gMnSc1	Arnold
Rimmer	Rimmer	k1gMnSc1	Rimmer
<g/>
.	.	kIx.	.
</s>
<s>
Hologram	hologram	k1gInSc1	hologram
30	[number]	k4	30
<g/>
letého	letý	k2eAgMnSc4d1	letý
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
před	před	k7c7	před
výbuchem	výbuch	k1gInSc7	výbuch
tepelného	tepelný	k2eAgInSc2d1	tepelný
štítu	štít	k1gInSc2	štít
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
opravit	opravit	k5eAaPmF	opravit
<g/>
,	,	kIx,	,
zastával	zastávat	k5eAaImAgMnS	zastávat
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
pozici	pozice	k1gFnSc4	pozice
druhého	druhý	k4xOgMnSc2	druhý
technika	technik	k1gMnSc2	technik
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
náplní	náplň	k1gFnSc7	náplň
práce	práce	k1gFnSc2	práce
bylo	být	k5eAaImAgNnS	být
opravovat	opravovat	k5eAaImF	opravovat
automaty	automat	k1gInPc4	automat
na	na	k7c4	na
polévku	polévka	k1gFnSc4	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
byl	být	k5eAaImAgInS	být
stvořen	stvořit	k5eAaPmNgInS	stvořit
lodním	lodní	k2eAgInSc7d1	lodní
počítačem	počítač	k1gInSc7	počítač
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
staral	starat	k5eAaImAgInS	starat
o	o	k7c4	o
Listerovo	Listerův	k2eAgNnSc4d1	Listerovo
duševní	duševní	k2eAgNnSc4d1	duševní
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Rimmer	Rimmer	k1gMnSc1	Rimmer
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgMnSc1d1	absolutní
břídil	břídil	k1gMnSc1	břídil
trpící	trpící	k2eAgMnPc1d1	trpící
neurózami	neuróza	k1gFnPc7	neuróza
a	a	k8xC	a
napoleonským	napoleonský	k2eAgInSc7d1	napoleonský
komplexem	komplex	k1gInSc7	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
postavit	postavit	k5eAaPmF	postavit
čemukoliv	cokoliv	k3yInSc3	cokoliv
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
před	před	k7c7	před
každým	každý	k3xTgInSc7	každý
problémem	problém	k1gInSc7	problém
zbaběle	zbaběle	k6eAd1	zbaběle
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
posedlý	posedlý	k2eAgInSc1d1	posedlý
touhou	touha	k1gFnSc7	touha
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
důstojníkem	důstojník	k1gMnSc7	důstojník
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
neustále	neustále	k6eAd1	neustále
propadá	propadat	k5eAaPmIp3nS	propadat
u	u	k7c2	u
důstojnických	důstojnický	k2eAgFnPc2d1	důstojnická
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hologram	hologram	k1gInSc1	hologram
se	se	k3xPyFc4	se
Rimmer	Rimmra	k1gFnPc2	Rimmra
nemůže	moct	k5eNaImIp3nS	moct
čehokoliv	cokoliv	k3yInSc2	cokoliv
dotýkat	dotýkat	k5eAaImF	dotýkat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jen	jen	k9	jen
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
bytostí	bytost	k1gFnSc7	bytost
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Legie	legie	k1gFnSc2	legie
převeden	převést	k5eAaPmNgInS	převést
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
tvrdé	tvrdé	k1gNnSc1	tvrdé
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
to	ten	k3xDgNnSc4	ten
Rimmer	Rimmer	k1gMnSc1	Rimmer
mohl	moct	k5eAaImAgMnS	moct
dotáhnout	dotáhnout	k5eAaPmF	dotáhnout
je	on	k3xPp3gNnSc4	on
Eso	eso	k1gNnSc4	eso
Rimmer	Rimmer	k1gInSc4	Rimmer
<g/>
,	,	kIx,	,
Rimmerovo	Rimmerův	k2eAgNnSc4d1	Rimmerovo
jiné	jiný	k2eAgNnSc4d1	jiné
já	já	k3xPp1nSc1	já
z	z	k7c2	z
alternativní	alternativní	k2eAgFnSc2d1	alternativní
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
téměř	téměř	k6eAd1	téměř
za	za	k7c4	za
superhrdinu	superhrdina	k1gFnSc4	superhrdina
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kristina	Kristin	k2eAgFnSc1d1	Kristina
Kochanská	Kochanský	k2eAgFnSc1d1	Kochanská
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
letá	letý	k2eAgFnSc1d1	letá
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
Lister	Lister	k1gMnSc1	Lister
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
havárií	havárie	k1gFnSc7	havárie
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
bláznivě	bláznivě	k6eAd1	bláznivě
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
a	a	k8xC	a
asi	asi	k9	asi
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
chodil	chodit	k5eAaImAgMnS	chodit
<g/>
.	.	kIx.	.
</s>
<s>
Kristina	Kristina	k1gFnSc1	Kristina
je	být	k5eAaImIp3nS	být
chytrá	chytrý	k2eAgFnSc1d1	chytrá
<g/>
,	,	kIx,	,
vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
a	a	k8xC	a
vyzná	vyznat	k5eAaBmIp3nS	vyznat
se	se	k3xPyFc4	se
v	v	k7c6	v
sýrech	sýr	k1gInPc6	sýr
(	(	kIx(	(
<g/>
nejraději	rád	k6eAd3	rád
má	mít	k5eAaImIp3nS	mít
kozí	kozí	k2eAgInSc1d1	kozí
sýr	sýr	k1gInSc1	sýr
s	s	k7c7	s
kousky	kousek	k1gInPc7	kousek
ananasu	ananas	k1gInSc2	ananas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Listerem	Lister	k1gInSc7	Lister
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
setkají	setkat	k5eAaPmIp3nP	setkat
<g/>
,	,	kIx,	,
když	když	k8xS	když
výsadková	výsadkový	k2eAgFnSc1d1	výsadková
loď	loď	k1gFnSc1	loď
Červeného	Červený	k1gMnSc2	Červený
trpaslíka	trpaslík	k1gMnSc2	trpaslík
(	(	kIx(	(
<g/>
Kosmik	Kosmik	k1gMnSc1	Kosmik
<g/>
)	)	kIx)	)
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
tunel	tunel	k1gInSc4	tunel
vedoucí	vedoucí	k1gMnSc1	vedoucí
z	z	k7c2	z
alternativní	alternativní	k2eAgFnSc2d1	alternativní
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kryton	Kryton	k1gInSc1	Kryton
<g/>
.	.	kIx.	.
</s>
<s>
Kryton	Kryton	k1gInSc1	Kryton
je	být	k5eAaImIp3nS	být
android	android	k1gInSc4	android
série	série	k1gFnSc1	série
4000	[number]	k4	4000
<g/>
m	m	kA	m
který	který	k3yIgInSc4	který
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
společností	společnost	k1gFnSc7	společnost
Divadroid	Divadroid	k1gInSc1	Divadroid
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hranatou	hranatý	k2eAgFnSc4d1	hranatá
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
hranaté	hranatý	k2eAgNnSc4d1	hranaté
i	i	k8xC	i
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
celé	celý	k2eAgNnSc1d1	celé
jméno	jméno	k1gNnSc1	jméno
zní	znět	k5eAaImIp3nS	znět
Kryton	Kryton	k1gInSc4	Kryton
2	[number]	k4	2
<g/>
X	X	kA	X
<g/>
4	[number]	k4	4
<g/>
B-	B-	k1gFnSc1	B-
<g/>
523	[number]	k4	523
<g/>
P.	P.	kA	P.
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
oslovuje	oslovovat	k5eAaImIp3nS	oslovovat
všechny	všechen	k3xTgInPc4	všechen
"	"	kIx"	"
<g/>
pane	pan	k1gMnSc5	pan
<g/>
"	"	kIx"	"
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
největší	veliký	k2eAgFnSc7d3	veliký
zálibou	záliba	k1gFnSc7	záliba
je	být	k5eAaImIp3nS	být
žehlení	žehlení	k1gNnSc1	žehlení
a	a	k8xC	a
praní	praní	k1gNnSc1	praní
prádla	prádlo	k1gNnSc2	prádlo
<g/>
.	.	kIx.	.
</s>
<s>
Krytonův	Krytonův	k2eAgInSc1d1	Krytonův
největší	veliký	k2eAgInSc1d3	veliký
sen	sen	k1gInSc1	sen
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
ho	on	k3xPp3gInSc4	on
Lister	Lister	k1gInSc4	Lister
naučí	naučit	k5eAaPmIp3nS	naučit
i	i	k9	i
lhaní	lhaní	k1gNnPc2	lhaní
<g/>
,	,	kIx,	,
drzosti	drzost	k1gFnSc3	drzost
a	a	k8xC	a
jiným	jiný	k2eAgInPc3d1	jiný
negativním	negativní	k2eAgInPc3d1	negativní
projevům	projev	k1gInPc3	projev
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
"	"	kIx"	"
<g/>
lidštějším	lidský	k2eAgInSc7d2	lidštější
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
loď	loď	k1gFnSc4	loď
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
Lister	Lister	k1gInSc4	Lister
zachránil	zachránit	k5eAaPmAgMnS	zachránit
z	z	k7c2	z
havarované	havarovaný	k2eAgFnSc2d1	havarovaná
lodi	loď	k1gFnSc2	loď
Nova	nova	k1gFnSc1	nova
5	[number]	k4	5
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
posádku	posádka	k1gFnSc4	posádka
Kryton	Kryton	k1gInSc1	Kryton
nedopatřením	nedopatření	k1gNnSc7	nedopatření
zabil	zabít	k5eAaPmAgInS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
cituje	citovat	k5eAaBmIp3nS	citovat
přesné	přesný	k2eAgNnSc4d1	přesné
znění	znění	k1gNnSc4	znění
direktiv	direktiva	k1gFnPc2	direktiva
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
jako	jako	k9	jako
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
Rimmerovy	Rimmerův	k2eAgFnPc4d1	Rimmerova
vlastní	vlastní	k2eAgFnPc4d1	vlastní
citace	citace	k1gFnPc4	citace
a	a	k8xC	a
interpretace	interpretace	k1gFnPc4	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
androidi	android	k1gMnPc1	android
a	a	k8xC	a
stroje	stroj	k1gInPc1	stroj
věří	věřit	k5eAaImIp3nP	věřit
v	v	k7c4	v
křemíkové	křemíkový	k2eAgNnSc4d1	křemíkové
nebe	nebe	k1gNnSc4	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Holly	Holla	k1gFnSc2	Holla
(	(	kIx(	(
<g/>
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Holly	Holla	k1gFnPc4	Holla
je	být	k5eAaImIp3nS	být
centrální	centrální	k2eAgInSc1d1	centrální
počítač	počítač	k1gInSc1	počítač
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
IQ	iq	kA	iq
původně	původně	k6eAd1	původně
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
hodnoty	hodnota	k1gFnPc4	hodnota
6	[number]	k4	6
000	[number]	k4	000
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
během	během	k7c2	během
3	[number]	k4	3
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Hollyho	Holly	k1gMnSc2	Holly
inteligence	inteligence	k1gFnSc2	inteligence
značně	značně	k6eAd1	značně
snížila	snížit	k5eAaPmAgFnS	snížit
kvůli	kvůli	k7c3	kvůli
počítačové	počítačový	k2eAgFnSc3d1	počítačová
senilitě	senilita	k1gFnSc3	senilita
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
zmatkářský	zmatkářský	k2eAgInSc4d1	zmatkářský
počítač	počítač	k1gInSc4	počítač
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
neví	vědět	k5eNaImIp3nS	vědět
rady	rada	k1gFnPc4	rada
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vždy	vždy	k6eAd1	vždy
najde	najít	k5eAaPmIp3nS	najít
řešení	řešení	k1gNnSc4	řešení
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
posádka	posádka	k1gFnSc1	posádka
lodi	loď	k1gFnSc2	loď
narazí	narazit	k5eAaPmIp3nS	narazit
<g/>
.	.	kIx.	.
</s>
<s>
Nejdřív	dříve	k6eAd3	dříve
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
asi	asi	k9	asi
padesátiletého	padesátiletý	k2eAgNnSc2d1	padesátileté
bílého	bílé	k1gNnSc2	bílé
plešatého	plešatý	k2eAgMnSc2d1	plešatý
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
z	z	k7c2	z
milionu	milion	k4xCgInSc2	milion
různých	různý	k2eAgFnPc2d1	různá
možností	možnost	k1gFnSc7	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
paralelního	paralelní	k2eAgInSc2d1	paralelní
vesmíru	vesmír	k1gInSc2	vesmír
se	se	k3xPyFc4	se
bláznivě	bláznivě	k6eAd1	bláznivě
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
lodního	lodní	k2eAgInSc2d1	lodní
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
Hilly	Hilla	k1gFnSc2	Hilla
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivněn	ovlivněn	k2eAgInSc1d1	ovlivněn
tímto	tento	k3xDgNnSc7	tento
milostným	milostný	k2eAgNnSc7d1	milostné
vzplanutím	vzplanutí	k1gNnSc7	vzplanutí
na	na	k7c6	na
sobě	se	k3xPyFc3	se
Holly	Holl	k1gInPc7	Holl
provede	provést	k5eAaPmIp3nS	provést
změnu	změna	k1gFnSc4	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
35	[number]	k4	35
<g/>
letá	letý	k2eAgFnSc1d1	letá
žena	žena	k1gFnSc1	žena
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
blonďatými	blonďatý	k2eAgInPc7d1	blonďatý
vlasy	vlas	k1gInPc7	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Replikant	Replikant	k1gInSc1	Replikant
je	být	k5eAaImIp3nS	být
uměle	uměle	k6eAd1	uměle
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
bytost	bytost	k1gFnSc1	bytost
humanoidního	humanoidní	k2eAgInSc2d1	humanoidní
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pohrdá	pohrdat	k5eAaImIp3nS	pohrdat
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
vším	všecek	k3xTgNnSc7	všecek
co	co	k9	co
je	on	k3xPp3gNnSc4	on
připomíná	připomínat	k5eAaImIp3nS	připomínat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
největší	veliký	k2eAgFnSc1d3	veliký
zábava	zábava	k1gFnSc1	zábava
je	být	k5eAaImIp3nS	být
zabíjení	zabíjení	k1gNnSc4	zabíjení
a	a	k8xC	a
mučení	mučení	k1gNnSc4	mučení
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
toto	tento	k3xDgNnSc4	tento
mučení	mučení	k1gNnSc4	mučení
prodlužují	prodlužovat	k5eAaImIp3nP	prodlužovat
na	na	k7c4	na
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
možnou	možný	k2eAgFnSc4d1	možná
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
i	i	k9	i
celá	celý	k2eAgNnPc4d1	celé
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Krytona	Kryton	k1gMnSc2	Kryton
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
rozdílem	rozdíl	k1gInSc7	rozdíl
mezi	mezi	k7c4	mezi
androidy	android	k1gInPc4	android
a	a	k8xC	a
replikanty	replikant	k1gInPc4	replikant
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Android	android	k1gInSc1	android
by	by	kYmCp3nS	by
nikdy	nikdy	k6eAd1	nikdy
člověku	člověk	k1gMnSc3	člověk
neurval	urvat	k5eNaPmAgMnS	urvat
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
nenaplival	naplivat	k5eNaPmAgMnS	naplivat
mu	on	k3xPp3gMnSc3	on
do	do	k7c2	do
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Replikanti	Replikant	k1gMnPc1	Replikant
jsou	být	k5eAaImIp3nP	být
skoro	skoro	k6eAd1	skoro
nezničitelní	zničitelný	k2eNgMnPc1d1	nezničitelný
a	a	k8xC	a
vydrží	vydržet	k5eAaPmIp3nP	vydržet
palbu	palba	k1gFnSc4	palba
z	z	k7c2	z
bazukoidů	bazukoid	k1gInPc2	bazukoid
na	na	k7c4	na
blízkou	blízký	k2eAgFnSc4d1	blízká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
jen	jen	k9	jen
s	s	k7c7	s
minimálním	minimální	k2eAgNnSc7d1	minimální
poškozením	poškození	k1gNnSc7	poškození
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
několik	několik	k4yIc1	několik
replikantů	replikant	k1gInPc2	replikant
<g/>
,	,	kIx,	,
např.	např.	kA	např.
replikant	replikant	k1gInSc1	replikant
z	z	k7c2	z
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Kentaur	kentaur	k1gMnSc1	kentaur
a	a	k8xC	a
replikant	replikant	k1gMnSc1	replikant
z	z	k7c2	z
trestanecké	trestanecký	k2eAgFnSc2d1	trestanecká
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pistolníci	pistolník	k1gMnPc1	pistolník
z	z	k7c2	z
Apokalypsy	apokalypsa	k1gFnSc2	apokalypsa
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
posádka	posádka	k1gFnSc1	posádka
Kosmiku	Kosmika	k1gFnSc4	Kosmika
střetne	střetnout	k5eAaPmIp3nS	střetnout
s	s	k7c7	s
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
lodí	loď	k1gFnSc7	loď
pilotovanou	pilotovaný	k2eAgFnSc4d1	pilotovaná
replikanty	replikanta	k1gFnPc4	replikanta
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
je	být	k5eAaImIp3nS	být
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
patřící	patřící	k2eAgFnSc2d1	patřící
Jupiterské	jupiterský	k2eAgFnSc2d1	Jupiterská
důlní	důlní	k2eAgFnSc2d1	důlní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
6	[number]	k4	6
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
cca	cca	kA	cca
10	[number]	k4	10
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysoká	vysoká	k1gFnSc1	vysoká
4	[number]	k4	4
míle	míle	k1gFnSc1	míle
(	(	kIx(	(
<g/>
cca	cca	kA	cca
6,5	[number]	k4	6,5
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
široká	široký	k2eAgFnSc1d1	široká
3	[number]	k4	3
míle	míle	k1gFnPc1	míle
(	(	kIx(	(
<g/>
cca	cca	kA	cca
5	[number]	k4	5
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
šestibokého	šestiboký	k2eAgInSc2d1	šestiboký
hranolu	hranol	k1gInSc2	hranol
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
natřená	natřený	k2eAgFnSc1d1	natřená
na	na	k7c4	na
červeno	červeno	k1gNnSc4	červeno
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
000	[number]	k4	000
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
Vpředu	vpředu	k6eAd1	vpředu
je	být	k5eAaImIp3nS	být
lapač	lapač	k1gInSc1	lapač
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
z	z	k7c2	z
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
zdrojů	zdroj	k1gInPc2	zdroj
zachytává	zachytávat	k5eAaImIp3nS	zachytávat
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
palivo	palivo	k1gNnSc4	palivo
pro	pro	k7c4	pro
motory	motor	k1gInPc4	motor
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
teoreticky	teoreticky	k6eAd1	teoreticky
plout	plout	k5eAaImF	plout
do	do	k7c2	do
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
představuje	představovat	k5eAaImIp3nS	představovat
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
malé	malý	k2eAgNnSc4d1	malé
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
nemocnice	nemocnice	k1gFnPc1	nemocnice
<g/>
,	,	kIx,	,
bary	bar	k1gInPc1	bar
<g/>
,	,	kIx,	,
diskotéky	diskotéka	k1gFnPc1	diskotéka
<g/>
,	,	kIx,	,
celnice	celnice	k1gFnPc1	celnice
i	i	k8xC	i
vězení	vězení	k1gNnPc1	vězení
(	(	kIx(	(
<g/>
věznice	věznice	k1gFnSc1	věznice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
tajném	tajný	k2eAgNnSc6d1	tajné
13	[number]	k4	13
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
přezdívaném	přezdívaný	k2eAgNnSc6d1	přezdívané
"	"	kIx"	"
<g/>
Tank	tank	k1gInSc4	tank
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
misi	mise	k1gFnSc6	mise
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
posádka	posádka	k1gFnSc1	posádka
tvořena	tvořit	k5eAaImNgFnS	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
1	[number]	k4	1
hologramem	hologram	k1gInSc7	hologram
napájeným	napájený	k2eAgInSc7d1	napájený
z	z	k7c2	z
centrální	centrální	k2eAgFnSc2d1	centrální
jednotky	jednotka	k1gFnSc2	jednotka
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
také	také	k9	také
vybavena	vybavit	k5eAaPmNgFnS	vybavit
menšími	malý	k2eAgFnPc7d2	menší
výsadkovými	výsadkový	k2eAgFnPc7d1	výsadková
plavidly	plavidlo	k1gNnPc7	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
plavidla	plavidlo	k1gNnPc1	plavidlo
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
typu	typ	k1gInSc3	typ
Kosmik	Kosmika	k1gFnPc2	Kosmika
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
typu	typ	k1gInSc3	typ
Modrý	modrý	k2eAgMnSc1d1	modrý
skrček	skrček	k1gMnSc1	skrček
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
těchto	tento	k3xDgNnPc2	tento
plavidel	plavidlo	k1gNnPc2	plavidlo
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
desítky	desítka	k1gFnSc2	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
vyšel	vyjít	k5eAaPmAgMnS	vyjít
také	také	k9	také
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
verzi	verze	k1gFnSc6	verze
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šenkyřík	Šenkyřík	k1gMnSc1	Šenkyřík
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Argo	Argo	k1gNnSc1	Argo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
odlišná	odlišný	k2eAgFnSc1d1	odlišná
a	a	k8xC	a
televizního	televizní	k2eAgInSc2d1	televizní
příběhu	příběh	k1gInSc2	příběh
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
jen	jen	k9	jen
rámcově	rámcově	k6eAd1	rámcově
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
1	[number]	k4	1
-	-	kIx~	-
Nekonečno	nekonečno	k1gNnSc1	nekonečno
vítá	vítat	k5eAaImIp3nS	vítat
ohleduplné	ohleduplný	k2eAgMnPc4d1	ohleduplný
řidiče	řidič	k1gMnPc4	řidič
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
2	[number]	k4	2
-	-	kIx~	-
Lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
život	život	k1gInSc1	život
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
3	[number]	k4	3
-	-	kIx~	-
Poslední	poslední	k2eAgMnSc1d1	poslední
člověk	člověk	k1gMnSc1	člověk
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
4	[number]	k4	4
-	-	kIx~	-
Pozpátku	pozpátku	k6eAd1	pozpátku
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
fanoušci	fanoušek	k1gMnPc1	fanoušek
seriálu	seriál	k1gInSc2	seriál
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
pořádají	pořádat	k5eAaImIp3nP	pořádat
pravidelně	pravidelně	k6eAd1	pravidelně
setkání	setkání	k1gNnSc1	setkání
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Trpaslicon	Trpaslicona	k1gFnPc2	Trpaslicona
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
setkání	setkání	k1gNnSc1	setkání
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Trpasliconu	Trpaslicon	k1gInSc6	Trpaslicon
se	se	k3xPyFc4	se
promítají	promítat	k5eAaImIp3nP	promítat
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
díly	díl	k1gInPc1	díl
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
společenské	společenský	k2eAgFnPc4d1	společenská
hry	hra	k1gFnPc4	hra
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Red	Red	k1gMnSc1	Red
Dwarf	Dwarf	k1gMnSc1	Dwarf
a	a	k8xC	a
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
zde	zde	k6eAd1	zde
pozvaní	pozvaný	k2eAgMnPc1d1	pozvaný
hosté	host	k1gMnPc1	host
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
dabéři	dabér	k1gMnPc1	dabér
Červeného	Červeného	k2eAgMnSc2d1	Červeného
trpaslíka	trpaslík	k1gMnSc2	trpaslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
všechny	všechen	k3xTgInPc1	všechen
Trpaslicony	Trpaslicon	k1gInPc1	Trpaslicon
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
množství	množství	k1gNnSc1	množství
parodií	parodie	k1gFnPc2	parodie
na	na	k7c4	na
významná	významný	k2eAgNnPc4d1	významné
díla	dílo	k1gNnPc4	dílo
především	především	k6eAd1	především
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
sci-fi	scii	k1gFnSc2	sci-fi
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
VO	VO	k?	VO
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
ST	St	kA	St
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgNnSc4d1	původní
dílo	dílo	k1gNnSc4	dílo
dokonce	dokonce	k9	dokonce
vyjmenováno	vyjmenován	k2eAgNnSc1d1	vyjmenováno
<g/>
.	.	kIx.	.
</s>
<s>
Dave	Dav	k1gInSc5	Dav
Lister	Lister	k1gMnSc1	Lister
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgMnSc1d1	poslední
žijící	žijící	k2eAgMnSc1d1	žijící
člověk	člověk	k1gMnSc1	člověk
na	na	k7c6	na
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
lodi	loď	k1gFnSc6	loď
(	(	kIx(	(
<g/>
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
posádka	posádka	k1gFnSc1	posádka
zahynula	zahynout	k5eAaPmAgFnS	zahynout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
David	David	k1gMnSc1	David
Bowman	Bowman	k1gMnSc1	Bowman
v	v	k7c6	v
VO	VO	k?	VO
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gInSc1	počítač
Holly	Holla	k1gFnSc2	Holla
se	se	k3xPyFc4	se
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
podobá	podobat	k5eAaImIp3nS	podobat
počítači	počítač	k1gInSc3	počítač
HAL	halo	k1gNnPc2	halo
9000	[number]	k4	9000
z	z	k7c2	z
VO	VO	k?	VO
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
úvodní	úvodní	k2eAgFnSc2d1	úvodní
znělky	znělka	k1gFnSc2	znělka
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Howard	Howard	k1gMnSc1	Howard
Goodall	Goodall	k1gMnSc1	Goodall
<g/>
)	)	kIx)	)
v	v	k7c6	v
začátku	začátek	k1gInSc6	začátek
parafrázuje	parafrázovat	k5eAaBmIp3nS	parafrázovat
úvod	úvod	k1gInSc4	úvod
skladby	skladba	k1gFnSc2	skladba
Tak	tak	k9	tak
pravil	pravit	k5eAaBmAgMnS	pravit
Zarathustra	Zarathustr	k1gMnSc4	Zarathustr
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Richard	Richard	k1gMnSc1	Richard
Strauss	Straussa	k1gFnPc2	Straussa
<g/>
)	)	kIx)	)
z	z	k7c2	z
úvodu	úvod	k1gInSc2	úvod
VO	VO	k?	VO
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Krytona	Krytona	k1gFnSc1	Krytona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
postavu	postava	k1gFnSc4	postava
androida	android	k1gMnSc2	android
Data	datum	k1gNnSc2	datum
z	z	k7c2	z
ST	St	kA	St
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
poznávat	poznávat	k5eAaImF	poznávat
lidské	lidský	k2eAgNnSc4d1	lidské
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
city	cit	k1gInPc4	cit
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
emoce	emoce	k1gFnPc4	emoce
a	a	k8xC	a
příčiny	příčina	k1gFnPc4	příčina
lidského	lidský	k2eAgNnSc2d1	lidské
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Polymorf	Polymorf	k1gInSc1	Polymorf
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
Vetřelce	vetřelec	k1gMnPc4	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Epizoda	epizoda	k1gFnSc1	epizoda
Kamila	Kamila	k1gFnSc1	Kamila
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
film	film	k1gInSc4	film
Casablanca	Casablanca	k1gFnSc1	Casablanca
<g/>
.	.	kIx.	.
</s>
<s>
Epizoda	epizoda	k1gFnSc1	epizoda
Roztavení	roztavení	k1gNnPc2	roztavení
je	být	k5eAaImIp3nS	být
parafrází	parafráze	k1gFnSc7	parafráze
filmu	film	k1gInSc2	film
Westworld	Westworld	k1gInSc1	Westworld
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
série	série	k1gFnSc2	série
Zpátky	zpátky	k6eAd1	zpátky
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
filmem	film	k1gInSc7	film
Blade	Blad	k1gInSc5	Blad
Runner	Runner	k1gMnSc1	Runner
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
společným	společný	k2eAgInSc7d1	společný
rysem	rys	k1gInSc7	rys
je	být	k5eAaImIp3nS	být
hledání	hledání	k1gNnSc4	hledání
vlastního	vlastní	k2eAgMnSc2d1	vlastní
tvůrce	tvůrce	k1gMnSc2	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
slova	slovo	k1gNnPc1	slovo
Rimmera	Rimmer	k1gMnSc2	Rimmer
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
polévka	polévka	k1gFnSc1	polévka
gazpacho	gazpacho	k6eAd1	gazpacho
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
parodií	parodie	k1gFnSc7	parodie
na	na	k7c4	na
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
Občana	občan	k1gMnSc4	občan
Kane	kanout	k5eAaImIp3nS	kanout
"	"	kIx"	"
<g/>
rosebud	rosebud	k6eAd1	rosebud
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
poupě	poupě	k1gNnSc1	poupě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
Já	já	k3xPp1nSc1	já
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
Občana	občan	k1gMnSc2	občan
Kane	kanout	k5eAaImIp3nS	kanout
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
hledání	hledání	k1gNnSc2	hledání
významu	význam	k1gInSc2	význam
těchto	tento	k3xDgNnPc2	tento
posledních	poslední	k2eAgNnPc2d1	poslední
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
situací	situace	k1gFnPc2	situace
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
Biblí	bible	k1gFnSc7	bible
či	či	k8xC	či
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
epizody	epizoda	k1gFnSc2	epizoda
Citróny	citrón	k1gInPc1	citrón
nebo	nebo	k8xC	nebo
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
Boha	bůh	k1gMnSc4	bůh
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
God	God	k1gFnSc2	God
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
parafrází	parafráze	k1gFnSc7	parafráze
Čekání	čekání	k1gNnSc2	čekání
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
Godot	Godot	k1gInSc1	Godot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
