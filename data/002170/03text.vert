<s>
Little	Little	k6eAd1	Little
Rock	rock	k1gInSc1	rock
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Arkansas	Arkansas	k1gInSc1	Arkansas
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sídlo	sídlo	k1gNnSc1	sídlo
okresu	okres	k1gInSc2	okres
Pulaski	Pulask	k1gFnSc2	Pulask
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
téměř	téměř	k6eAd1	téměř
193	[number]	k4	193
524	[number]	k4	524
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
města	město	k1gNnSc2	město
a	a	k8xC	a
městečka	městečko	k1gNnSc2	městečko
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
699	[number]	k4	699
757	[number]	k4	757
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Little	Little	k6eAd1	Little
Rock	rock	k1gInSc1	rock
leží	ležet	k5eAaImIp3nS	ležet
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
geografickém	geografický	k2eAgNnSc6d1	geografické
centru	centrum	k1gNnSc6	centrum
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
malého	malý	k2eAgInSc2d1	malý
skalního	skalní	k2eAgInSc2d1	skalní
útvaru	útvar	k1gInSc2	útvar
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Arkansas	Arkansas	k1gInSc1	Arkansas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
Petite	petit	k1gInSc5	petit
Roche	Roche	k1gNnPc7	Roche
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc1	Little
Rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
193	[number]	k4	193
524	[number]	k4	524
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
48,9	[number]	k4	48,9
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
42,3	[number]	k4	42,3
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
2,7	[number]	k4	2,7
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
3,9	[number]	k4	3,9
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
1,7	[number]	k4	1,7
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
6,8	[number]	k4	6,8
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Douglas	Douglas	k1gMnSc1	Douglas
MacArthur	MacArthur	k1gMnSc1	MacArthur
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
-	-	kIx~	-
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
Broncho	Broncha	k1gFnSc5	Broncha
Billy	Bill	k1gMnPc7	Bill
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
-	-	kIx~	-
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Roy	Roy	k1gMnSc1	Roy
Scheider	Scheider	k1gMnSc1	Scheider
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
-	-	kIx~	-
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
televizní	televizní	k2eAgMnSc1d1	televizní
herec	herec	k1gMnSc1	herec
Leon	Leona	k1gFnPc2	Leona
<g />
.	.	kIx.	.
</s>
<s>
Russom	Russom	k1gInSc1	Russom
(	(	kIx(	(
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Carlos	Carlos	k1gMnSc1	Carlos
N.	N.	kA	N.
Hathcock	Hathcock	k1gMnSc1	Hathcock
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
-	-	kIx~	-
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
odstřelovač	odstřelovač	k1gMnSc1	odstřelovač
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
války	válka	k1gFnSc2	válka
Corin	Corin	k1gMnSc1	Corin
Nemec	Nemec	k1gMnSc1	Nemec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Jason	Jason	k1gMnSc1	Jason
White	Whit	k1gInSc5	Whit
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
Město	město	k1gNnSc1	město
Little	Little	k1gFnPc4	Little
Rock	rock	k1gInSc4	rock
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
lidí	člověk	k1gMnPc2	člověk
dodnes	dodnes	k6eAd1	dodnes
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
představu	představa	k1gFnSc4	představa
42	[number]	k4	42
<g/>
.	.	kIx.	.
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
mnoho	mnoho	k4c1	mnoho
turistických	turistický	k2eAgFnPc2d1	turistická
zajímavostí	zajímavost	k1gFnPc2	zajímavost
Little	Little	k1gFnSc2	Little
Rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc4	sídlo
guvernéra	guvernér	k1gMnSc2	guvernér
Arkansasu	Arkansas	k1gInSc2	Arkansas
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
Clintonovým	Clintonův	k2eAgNnSc7d1	Clintonovo
každodenním	každodenní	k2eAgNnSc7d1	každodenní
pracovištěm	pracoviště	k1gNnSc7	pracoviště
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
věrnou	věrný	k2eAgFnSc7d1	věrná
<g/>
,	,	kIx,	,
zmenšenou	zmenšený	k2eAgFnSc7d1	zmenšená
kopií	kopie	k1gFnSc7	kopie
Kapitolu	Kapitol	k1gInSc2	Kapitol
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
Clinton	Clinton	k1gMnSc1	Clinton
osm	osm	k4xCc1	osm
let	léto	k1gNnPc2	léto
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Osobnost	osobnost	k1gFnSc1	osobnost
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
připomíná	připomínat	k5eAaImIp3nS	připomínat
muzeum	muzeum	k1gNnSc1	muzeum
s	s	k7c7	s
názvem	název	k1gInSc7	název
William	William	k1gInSc1	William
J.	J.	kA	J.
Clinton	Clinton	k1gMnSc1	Clinton
Presidential	Presidential	k1gMnSc1	Presidential
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
tohoto	tento	k3xDgMnSc2	tento
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozlehlé	rozlehlý	k2eAgFnSc6d1	rozlehlá
stavbě	stavba	k1gFnSc6	stavba
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
několika	několik	k4yIc6	několik
patrech	patro	k1gNnPc6	patro
shromážděny	shromážděn	k2eAgFnPc1d1	shromážděna
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
díle	díl	k1gInSc6	díl
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
<g/>
.	.	kIx.	.
</s>
<s>
Čchang-čchun	Čchang-čchun	k1gNnSc1	Čchang-čchun
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Hanam	Hanam	k1gInSc1	Hanam
City	City	k1gFnSc1	City
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
Kao-siung	Kaoiung	k1gInSc1	Kao-siung
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
La	la	k0	la
Petite-Pierre	Petite-Pierr	k1gMnSc5	Petite-Pierr
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Mons	Mons	k1gInSc1	Mons
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
Newcastle	Newcastle	k1gFnSc1	Newcastle
upon	upon	k1gInSc4	upon
Tyne	Tyn	k1gFnSc2	Tyn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Pachuca	Pachuc	k1gInSc2	Pachuc
de	de	k?	de
Soto	Soto	k1gNnSc1	Soto
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
Ragusa	Ragus	k1gMnSc2	Ragus
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
Samsun	Samsuno	k1gNnPc2	Samsuno
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
</s>
