<s>
Kostrč	kostrč	k1gFnSc1	kostrč
či	či	k8xC	či
kost	kost	k1gFnSc1	kost
kostrční	kostrční	k2eAgFnSc1d1	kostrční
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
coccygis	coccygis	k1gInSc1	coccygis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
kost	kost	k1gFnSc1	kost
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
srůstem	srůst	k1gInSc7	srůst
posledních	poslední	k2eAgInPc2d1	poslední
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
zakrnělého	zakrnělý	k2eAgInSc2d1	zakrnělý
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
