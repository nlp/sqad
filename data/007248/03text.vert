<s>
Kostrč	kostrč	k1gFnSc1	kostrč
či	či	k8xC	či
kost	kost	k1gFnSc1	kost
kostrční	kostrční	k2eAgFnSc1d1	kostrční
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
coccygis	coccygis	k1gInSc1	coccygis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
kost	kost	k1gFnSc1	kost
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
srůstem	srůst	k1gInSc7	srůst
posledních	poslední	k2eAgInPc2d1	poslední
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
zakrnělého	zakrnělý	k2eAgInSc2d1	zakrnělý
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
srůstem	srůst	k1gInSc7	srůst
tří	tři	k4xCgNnPc2	tři
až	až	k9	až
pěti	pět	k4xCc2	pět
posledních	poslední	k2eAgInPc2d1	poslední
obratlů	obratel	k1gInPc2	obratel
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
trojúhelníkovitý	trojúhelníkovitý	k2eAgInSc4d1	trojúhelníkovitý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Srostlé	srostlý	k2eAgInPc1d1	srostlý
obratle	obratel	k1gInPc1	obratel
se	se	k3xPyFc4	se
ale	ale	k9	ale
původním	původní	k2eAgInPc3d1	původní
obratlům	obratel	k1gInPc3	obratel
podobají	podobat	k5eAaImIp3nP	podobat
jen	jen	k9	jen
velice	velice	k6eAd1	velice
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
původního	původní	k2eAgInSc2d1	původní
tvaru	tvar	k1gInSc2	tvar
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
obratlové	obratlový	k2eAgNnSc4d1	obratlové
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Kostrč	kostrč	k1gFnSc1	kostrč
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
křížová	křížový	k2eAgFnSc1d1	křížová
kost	kost	k1gFnSc1	kost
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
chrupavkou	chrupavka	k1gFnSc7	chrupavka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
synchondróza	synchondróza	k1gFnSc1	synchondróza
<g/>
)	)	kIx)	)
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
báze	báze	k1gFnSc2	báze
kostrče	kostrč	k1gFnSc2	kostrč
a	a	k8xC	a
směřující	směřující	k2eAgFnSc2d1	směřující
k	k	k7c3	k
hrotu	hrot	k1gInSc3	hrot
křížové	křížový	k2eAgFnSc2d1	křížová
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
chrupavčitému	chrupavčitý	k2eAgNnSc3d1	chrupavčité
spojení	spojení	k1gNnSc3	spojení
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
kostmi	kost	k1gFnPc7	kost
lze	lze	k6eAd1	lze
docílit	docílit	k5eAaPmF	docílit
určité	určitý	k2eAgFnPc4d1	určitá
mobility	mobilita	k1gFnPc4	mobilita
spoje	spoj	k1gInSc2	spoj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kývavý	kývavý	k2eAgInSc1d1	kývavý
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
větší	veliký	k2eAgFnSc2d2	veliký
absorpce	absorpce	k1gFnSc2	absorpce
vody	voda	k1gFnSc2	voda
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
kostrč	kostrč	k1gFnSc1	kostrč
změkčit	změkčit	k5eAaPmF	změkčit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
umožní	umožnit	k5eAaPmIp3nS	umožnit
snazší	snadný	k2eAgFnSc1d2	snazší
porod	porod	k1gInSc1	porod
<g/>
.	.	kIx.	.
</s>
<s>
Kostrč	kostrč	k1gFnSc1	kostrč
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
rudimentárních	rudimentární	k2eAgInPc2d1	rudimentární
obratlů	obratel	k1gInPc2	obratel
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
<g/>
)	)	kIx)	)
Co	co	k9	co
<g/>
1	[number]	k4	1
až	až	k9	až
Co	co	k9	co
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
k	k	k7c3	k
sobě	se	k3xPyFc3	se
pevně	pevně	k6eAd1	pevně
srůstají	srůstat	k5eAaImIp3nP	srůstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
atypických	atypický	k2eAgInPc6d1	atypický
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kostrč	kostrč	k1gFnSc1	kostrč
tvořena	tvořen	k2eAgFnSc1d1	tvořena
až	až	k9	až
ze	z	k7c2	z
6	[number]	k4	6
či	či	k8xC	či
7	[number]	k4	7
obratlů	obratel	k1gInPc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Obratle	obratel	k1gInPc1	obratel
nemají	mít	k5eNaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
typický	typický	k2eAgInSc4d1	typický
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
deformované	deformovaný	k2eAgFnPc1d1	deformovaná
a	a	k8xC	a
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
původní	původní	k2eAgFnSc2d1	původní
podoby	podoba	k1gFnSc2	podoba
je	on	k3xPp3gNnSc4	on
stejné	stejný	k2eAgNnSc4d1	stejné
pouze	pouze	k6eAd1	pouze
tělo	tělo	k1gNnSc4	tělo
obratle	obratel	k1gInSc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zakrnělých	zakrnělý	k2eAgInPc2d1	zakrnělý
oblouků	oblouk	k1gInPc2	oblouk
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
jen	jen	k9	jen
zakrnělé	zakrnělý	k2eAgInPc1d1	zakrnělý
výběžky	výběžek	k1gInPc1	výběžek
cornua	cornua	k6eAd1	cornua
coccygea	coccygea	k6eAd1	coccygea
směřující	směřující	k2eAgNnSc1d1	směřující
proti	proti	k7c3	proti
cornua	cornu	k1gInSc2	cornu
sacralia	sacralium	k1gNnSc2	sacralium
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
prvního	první	k4xOgInSc2	první
obratle	obratel	k1gInSc2	obratel
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ještě	ještě	k6eAd1	ještě
pozorovat	pozorovat	k5eAaImF	pozorovat
zakončení	zakončení	k1gNnSc4	zakončení
páteřního	páteřní	k2eAgInSc2d1	páteřní
kanálu	kanál	k1gInSc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
kostrče	kostrč	k1gFnSc2	kostrč
je	být	k5eAaImIp3nS	být
volný	volný	k2eAgInSc1d1	volný
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
koncem	konec	k1gInSc7	konec
křížové	křížový	k2eAgFnSc2d1	křížová
kosti	kost	k1gFnSc2	kost
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
definování	definování	k1gNnSc4	definování
pánevní	pánevní	k2eAgFnSc2d1	pánevní
roviny	rovina	k1gFnSc2	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Člověku	člověk	k1gMnSc3	člověk
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
–	–	k?	–
díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
dlouho	dlouho	k6eAd1	dlouho
sedět	sedět	k5eAaImF	sedět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
časté	častý	k2eAgInPc4d1	častý
úrazy	úraz	k1gInPc4	úraz
kostrče	kostrč	k1gFnSc2	kostrč
patří	patřit	k5eAaImIp3nP	patřit
její	její	k3xOp3gNnSc4	její
naražení	naražení	k1gNnSc4	naražení
vlivem	vliv	k1gInSc7	vliv
tvrdého	tvrdé	k1gNnSc2	tvrdé
nárazu	náraz	k1gInSc2	náraz
či	či	k8xC	či
dopadu	dopad	k1gInSc2	dopad
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bolestivé	bolestivý	k2eAgNnSc4d1	bolestivé
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
časem	časem	k6eAd1	časem
přejde	přejít	k5eAaPmIp3nS	přejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
extrémním	extrémní	k2eAgInSc6d1	extrémní
případě	případ	k1gInSc6	případ
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
zlomení	zlomení	k1gNnSc3	zlomení
kostrče	kostrč	k1gFnSc2	kostrč
<g/>
.	.	kIx.	.
</s>
