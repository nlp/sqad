<s>
Světová	světový	k2eAgFnSc1d1	světová
poštovní	poštovní	k2eAgFnSc1d1	poštovní
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
Union	union	k1gInSc1	union
Postale	Postal	k1gMnSc5	Postal
Universelle	Universell	k1gMnSc5	Universell
<g/>
,	,	kIx,	,
Universal	Universal	k1gFnSc1	Universal
postal	postal	k1gMnSc1	postal
union	union	k1gInSc1	union
-	-	kIx~	-
UPU	UPU	kA	UPU
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1874	[number]	k4	1874
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
Generální	generální	k2eAgMnSc1d1	generální
poštovní	poštovní	k1gMnSc1	poštovní
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
rozvoj	rozvoj	k1gInSc4	rozvoj
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
poštovního	poštovní	k2eAgNnSc2d1	poštovní
spojení	spojení	k1gNnSc2	spojení
a	a	k8xC	a
sjednocovat	sjednocovat	k5eAaImF	sjednocovat
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
poštovní	poštovní	k2eAgFnSc4d1	poštovní
přepravu	přeprava	k1gFnSc4	přeprava
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgInPc7d1	základní
smluvními	smluvní	k2eAgInPc7d1	smluvní
dokumenty	dokument	k1gInPc7	dokument
UPU	UPU	kA	UPU
jsou	být	k5eAaImIp3nP	být
Akta	akta	k1gNnPc4	akta
obsahující	obsahující	k2eAgNnPc4d1	obsahující
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
poštovní	poštovní	k2eAgFnPc4d1	poštovní
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1874	[number]	k4	1874
podepsali	podepsat	k5eAaPmAgMnP	podepsat
představitelé	představitel	k1gMnPc1	představitel
22	[number]	k4	22
poštovních	poštovní	k2eAgFnPc2d1	poštovní
správ	správa	k1gFnPc2	správa
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
tzv.	tzv.	kA	tzv.
Bernskou	bernský	k2eAgFnSc4d1	Bernská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
Generální	generální	k2eAgFnSc4d1	generální
poštovní	poštovní	k2eAgFnSc4d1	poštovní
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
současný	současný	k2eAgInSc4d1	současný
název	název	k1gInSc4	název
získala	získat	k5eAaPmAgFnS	získat
UPU	UPU	kA	UPU
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1948	[number]	k4	1948
se	se	k3xPyFc4	se
UPU	UPU	kA	UPU
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
orgánů	orgán	k1gInPc2	orgán
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k6eAd1	Rakousko-Uhersko
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
UPU	UPU	kA	UPU
stalo	stát	k5eAaPmAgNnS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1875	[number]	k4	1875
Československo	Československo	k1gNnSc1	Československo
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
UPU	UPU	kA	UPU
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
Česko	Česko	k1gNnSc1	Česko
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
UPU	UPU	kA	UPU
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
Adresa	adresa	k1gFnSc1	adresa
UPU	UPU	kA	UPU
<g/>
:	:	kIx,	:
Weltpoststrasse	Weltpoststrasse	k1gFnSc1	Weltpoststrasse
4	[number]	k4	4
<g/>
,	,	kIx,	,
Case	Casus	k1gMnSc5	Casus
Postale	Postal	k1gMnSc5	Postal
3000	[number]	k4	3000
<g/>
,	,	kIx,	,
Bern	Bern	k1gInSc1	Bern
15	[number]	k4	15
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
zajištění	zajištění	k1gNnSc2	zajištění
volného	volný	k2eAgInSc2d1	volný
toku	tok	k1gInSc2	tok
poštovních	poštovní	k2eAgFnPc2d1	poštovní
zásilek	zásilka	k1gFnPc2	zásilka
na	na	k7c6	na
jednotném	jednotný	k2eAgNnSc6d1	jednotné
poštovním	poštovní	k2eAgNnSc6d1	poštovní
území	území	k1gNnSc6	území
tvořeném	tvořený	k2eAgNnSc6d1	tvořené
propojenými	propojený	k2eAgFnPc7d1	propojená
sítěmi	síť	k1gFnPc7	síť
<g/>
;	;	kIx,	;
podpora	podpora	k1gFnSc1	podpora
přijímání	přijímání	k1gNnSc2	přijímání
přiměřených	přiměřený	k2eAgFnPc2d1	přiměřená
společných	společný	k2eAgFnPc2d1	společná
norem	norma	k1gFnPc2	norma
a	a	k8xC	a
používání	používání	k1gNnSc2	používání
technologie	technologie	k1gFnSc2	technologie
<g/>
;	;	kIx,	;
zajištění	zajištění	k1gNnSc3	zajištění
spolupráce	spolupráce	k1gFnSc2	spolupráce
a	a	k8xC	a
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
součinnosti	součinnost	k1gFnSc2	součinnost
mezi	mezi	k7c7	mezi
zainteresovanými	zainteresovaný	k2eAgFnPc7d1	zainteresovaná
stranami	strana	k1gFnPc7	strana
<g/>
;	;	kIx,	;
pomoc	pomoc	k1gFnSc1	pomoc
efektivní	efektivní	k2eAgFnSc1d1	efektivní
technické	technický	k2eAgFnSc3d1	technická
spolupráci	spolupráce	k1gFnSc3	spolupráce
<g/>
;	;	kIx,	;
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
uspokojování	uspokojování	k1gNnSc4	uspokojování
měnících	měnící	k2eAgFnPc2d1	měnící
se	se	k3xPyFc4	se
potřeb	potřeba	k1gFnPc2	potřeba
zákazníků	zákazník	k1gMnPc2	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
UPU	UPU	kA	UPU
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
organizaci	organizace	k1gFnSc4	organizace
a	a	k8xC	a
zdokonalování	zdokonalování	k1gNnSc4	zdokonalování
poštovních	poštovní	k2eAgFnPc2d1	poštovní
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
podporovat	podporovat	k5eAaImF	podporovat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
rozvoj	rozvoj	k1gInSc4	rozvoj
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
UPU	UPU	kA	UPU
je	být	k5eAaImIp3nS	být
Světový	světový	k2eAgInSc4d1	světový
poštovní	poštovní	k2eAgInSc4d1	poštovní
kongres	kongres	k1gInSc4	kongres
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
minimálně	minimálně	k6eAd1	minimálně
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
provádí	provádět	k5eAaImIp3nS	provádět
změny	změna	k1gFnPc4	změna
Akt	akta	k1gNnPc2	akta
UPU	UPU	kA	UPU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
kongresy	kongres	k1gInPc7	kongres
řídí	řídit	k5eAaImIp3nP	řídit
UPU	UPU	kA	UPU
Administrativní	administrativní	k2eAgFnSc1d1	administrativní
rada	rada	k1gFnSc1	rada
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
41	[number]	k4	41
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
poštovního	poštovní	k2eAgInSc2d1	poštovní
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
Poradního	poradní	k2eAgInSc2d1	poradní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Sekretariát	sekretariát	k1gInSc1	sekretariát
UPU	UPU	kA	UPU
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nP	stát
volený	volený	k2eAgMnSc1d1	volený
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Bishar	Bishar	k1gMnSc1	Bishar
Abdirahman	Abdirahman	k1gMnSc1	Abdirahman
Hussein	Hussein	k1gMnSc1	Hussein
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2016	[number]	k4	2016
znovuzvolen	znovuzvolit	k5eAaPmNgMnS	znovuzvolit
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Světovým	světový	k2eAgInSc7d1	světový
poštovním	poštovní	k2eAgInSc7d1	poštovní
kongresem	kongres	k1gInSc7	kongres
na	na	k7c6	na
období	období	k1gNnSc6	období
dalších	další	k2eAgNnPc2d1	další
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
UPU	UPU	kA	UPU
je	být	k5eAaImIp3nS	být
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
angličtina	angličtina	k1gFnSc1	angličtina
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pracovním	pracovní	k2eAgInSc7d1	pracovní
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
pracovních	pracovní	k2eAgNnPc6d1	pracovní
jednáních	jednání	k1gNnPc6	jednání
převažuje	převažovat	k5eAaImIp3nS	převažovat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dokumentů	dokument	k1gInPc2	dokument
je	být	k5eAaImIp3nS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
oficiálních	oficiální	k2eAgInPc6d1	oficiální
jazycích	jazyk	k1gInPc6	jazyk
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
angličtinou	angličtina	k1gFnSc7	angličtina
a	a	k8xC	a
francouzštinou	francouzština	k1gFnSc7	francouzština
dále	daleko	k6eAd2	daleko
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
španělština	španělština	k1gFnSc1	španělština
a	a	k8xC	a
portugalština	portugalština	k1gFnSc1	portugalština
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
jednání	jednání	k1gNnPc1	jednání
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
jednání	jednání	k1gNnPc1	jednání
Kongresů	kongres	k1gInPc2	kongres
UPU	UPU	kA	UPU
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
simultánně	simultánně	k6eAd1	simultánně
tlumočena	tlumočit	k5eAaImNgFnS	tlumočit
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
Bern	Bern	k1gInSc1	Bern
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
Lisabon	Lisabon	k1gInSc1	Lisabon
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
D.C.	D.C.	k1gFnSc2	D.C.
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Stockholm	Stockholm	k1gInSc1	Stockholm
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Káhira	Káhira	k1gFnSc1	Káhira
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Buenos	Buenos	k1gInSc1	Buenos
Aires	Aires	k1gInSc1	Aires
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
Brusel	Brusel	k1gInSc1	Brusel
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Ottawa	Ottawa	k1gFnSc1	Ottawa
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc1	Tokio
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
Lausanne	Lausanne	k1gNnSc7	Lausanne
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Rio	Rio	k1gFnSc1	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Hamburg	Hamburg	k1gInSc1	Hamburg
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
D.C.	D.C.	k1gFnSc2	D.C.
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
Soul	Soul	k1gInSc1	Soul
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Peking	Peking	k1gInSc1	Peking
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Bukurešť	Bukurešť	k1gFnSc1	Bukurešť
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Ženeva	Ženeva	k1gFnSc1	Ženeva
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Dauhá	Dauhý	k2eAgFnSc1d1	Dauhá
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
poštovní	poštovní	k2eAgInSc1d1	poštovní
kongres	kongres	k1gInSc1	kongres
<g/>
:	:	kIx,	:
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
Istanbul	Istanbul	k1gInSc1	Istanbul
Příští	příští	k2eAgInSc1d1	příští
Kongres	kongres	k1gInSc1	kongres
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
konat	konat	k5eAaImF	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
v	v	k7c6	v
Abidjanu	Abidjan	k1gInSc6	Abidjan
<g/>
.	.	kIx.	.
</s>
<s>
UPU	UPU	kA	UPU
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
192	[number]	k4	192
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
působnosti	působnost	k1gFnSc2	působnost
patří	patřit	k5eAaImIp3nS	patřit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
pracovníků	pracovník	k1gMnPc2	pracovník
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
660	[number]	k4	660
tisících	tisící	k4xOgFnPc6	tisící
poštovních	poštovní	k2eAgFnPc6d1	poštovní
provozovnách	provozovna	k1gFnPc6	provozovna
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
Kongresů	kongres	k1gInPc2	kongres
UPU	UPU	kA	UPU
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Kataru	katar	k1gInSc2	katar
<g/>
,	,	kIx,	,
Dauhá	Dauhý	k2eAgFnSc1d1	Dauhá
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
centrálu	centrála	k1gFnSc4	centrála
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
vydává	vydávat	k5eAaPmIp3nS	vydávat
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
poštovní	poštovní	k2eAgFnSc1d1	poštovní
správa	správa	k1gFnSc1	správa
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
emise	emise	k1gFnSc2	emise
poštovních	poštovní	k2eAgFnPc2d1	poštovní
známek	známka	k1gFnPc2	známka
ve	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
měně	měna	k1gFnSc6	měna
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
nápisem	nápis	k1gInSc7	nápis
UNION	union	k1gInSc4	union
POSTALE	POSTALE	kA	POSTALE
UNIVERSELLE	UNIVERSELLE	kA	UNIVERSELLE
a	a	k8xC	a
nadtitulkem	nadtitulko	k1gNnSc7	nadtitulko
HELVETIA	Helvetia	k1gFnSc1	Helvetia
<g/>
.	.	kIx.	.
</s>
