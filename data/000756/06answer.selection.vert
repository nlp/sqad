<s>
Světová	světový	k2eAgFnSc1d1	světová
poštovní	poštovní	k2eAgFnSc1d1	poštovní
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
Union	union	k1gInSc1	union
Postale	Postal	k1gMnSc5	Postal
Universelle	Universell	k1gMnSc5	Universell
<g/>
,	,	kIx,	,
Universal	Universal	k1gFnSc1	Universal
postal	postal	k1gMnSc1	postal
union	union	k1gInSc1	union
-	-	kIx~	-
UPU	UPU	kA	UPU
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1874	[number]	k4	1874
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
Generální	generální	k2eAgMnSc1d1	generální
poštovní	poštovní	k1gMnSc1	poštovní
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
