<s>
Černá	černý	k2eAgFnSc1d1
díra	díra	k1gFnSc1
byla	být	k5eAaImAgFnS
teoreticky	teoreticky	k6eAd1
předpovězena	předpovědit	k5eAaPmNgFnS
v	v	k7c6
obecné	obecný	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
relativity	relativita	k1gFnSc2
publikované	publikovaný	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
1915	[number]	k4
Albertem	Albert	k1gMnSc7
Einsteinem	Einstein	k1gMnSc7
<g/>
.	.	kIx.
</s>