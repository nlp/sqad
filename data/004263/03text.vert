<s>
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgInSc4d1	československý
film	film	k1gInSc4	film
režiséra	režisér	k1gMnSc2	režisér
Juraje	Juraj	k1gInSc2	Juraj
Herze	Herze	k1gFnSc2	Herze
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc4d1	natočený
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
novely	novela	k1gFnSc2	novela
Ladislava	Ladislav	k1gMnSc2	Ladislav
Fukse	Fuks	k1gMnSc2	Fuks
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hororovou	hororový	k2eAgFnSc4d1	hororová
černou	černý	k2eAgFnSc4d1	černá
komedii	komedie	k1gFnSc4	komedie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgNnPc3	svůj
vyzněním	vyznění	k1gNnPc3	vyznění
řazená	řazený	k2eAgFnSc1d1	řazená
k	k	k7c3	k
následovníků	následovník	k1gMnPc2	následovník
filmů	film	k1gInPc2	film
německého	německý	k2eAgInSc2d1	německý
expresionismu	expresionismus	k1gInSc2	expresionismus
a	a	k8xC	a
k	k	k7c3	k
filmům	film	k1gInPc3	film
české	český	k2eAgFnSc2d1	Česká
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
Československo	Československo	k1gNnSc4	Československo
navržen	navržen	k2eAgMnSc1d1	navržen
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
do	do	k7c2	do
nominačního	nominační	k2eAgInSc2d1	nominační
výběru	výběr	k1gInSc2	výběr
se	se	k3xPyFc4	se
ale	ale	k9	ale
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
katalánském	katalánský	k2eAgInSc6d1	katalánský
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
festivalu	festival	k1gInSc6	festival
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
hlavní	hlavní	k2eAgFnSc2d1	hlavní
role	role	k1gFnSc2	role
oceněn	oceněn	k2eAgMnSc1d1	oceněn
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
starší	starší	k1gMnSc1	starší
a	a	k8xC	a
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
kameru	kamera	k1gFnSc4	kamera
Stanislav	Stanislava	k1gFnPc2	Stanislava
Milota	milota	k1gFnSc1	milota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
serveru	server	k1gInSc6	server
ČSFD	ČSFD	kA	ČSFD
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
únoru	únor	k1gInSc3	únor
2017	[number]	k4	2017
hodnocen	hodnocen	k2eAgInSc4d1	hodnocen
jako	jako	k8xS	jako
šestý	šestý	k4xOgInSc4	šestý
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
československý	československý	k2eAgInSc4d1	československý
film	film	k1gInSc4	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
filmu	film	k1gInSc3	film
Karel	Karel	k1gMnSc1	Karel
Kopfrkingl	Kopfrkingl	k1gMnSc1	Kopfrkingl
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
st.	st.	kA	st.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
krematoria	krematorium	k1gNnSc2	krematorium
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
opravdu	opravdu	k6eAd1	opravdu
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
i	i	k9	i
do	do	k7c2	do
vztahů	vztah	k1gInPc2	vztah
k	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
blízkým	blízký	k2eAgFnPc3d1	blízká
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
za	za	k7c2	za
nastupující	nastupující	k2eAgFnSc2d1	nastupující
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
neradostná	radostný	k2eNgFnSc1d1	neradostná
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
sleduje	sledovat	k5eAaImIp3nS	sledovat
psychologickou	psychologický	k2eAgFnSc4d1	psychologická
proměnu	proměna	k1gFnSc4	proměna
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
točen	točit	k5eAaImNgInS	točit
na	na	k7c6	na
super	super	k1gInSc6	super
16	[number]	k4	16
<g/>
mm	mm	kA	mm
film	film	k1gInSc1	film
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1,66	[number]	k4	1,66
černobíle	černobíle	k6eAd1	černobíle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
byl	být	k5eAaImAgInS	být
uložen	uložit	k5eAaPmNgInS	uložit
do	do	k7c2	do
trezoru	trezor	k1gInSc2	trezor
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
politických	politický	k2eAgFnPc2d1	politická
změn	změna	k1gFnPc2	změna
sklonku	sklonek	k1gInSc2	sklonek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
děj	děj	k1gInSc4	děj
se	se	k3xPyFc4	se
též	též	k9	též
stala	stát	k5eAaPmAgFnS	stát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
smrt	smrt	k1gFnSc4	smrt
dalajlámy	dalajláma	k1gMnSc2	dalajláma
Tubtäna	Tubtän	k1gMnSc2	Tubtän
Gjamccha	Gjamcch	k1gMnSc2	Gjamcch
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
psychologii	psychologie	k1gFnSc4	psychologie
spořádaného	spořádaný	k2eAgMnSc2d1	spořádaný
otce	otec	k1gMnSc2	otec
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
vzorného	vzorný	k2eAgMnSc2d1	vzorný
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
krematoria	krematorium	k1gNnSc2	krematorium
Karla	Karel	k1gMnSc2	Karel
Kopfrkingla	Kopfrkingl	k1gMnSc2	Kopfrkingl
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pádem	pád	k1gInSc7	pád
zlaté	zlatý	k2eAgFnSc2d1	zlatá
éry	éra	k1gFnSc2	éra
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
mění	měnit	k5eAaImIp3nS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
charakter	charakter	k1gInSc4	charakter
až	až	k9	až
do	do	k7c2	do
patologické	patologický	k2eAgFnSc2d1	patologická
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
protektorátu	protektorát	k1gInSc2	protektorát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
z	z	k7c2	z
Kopfrkingla	Kopfrkinglo	k1gNnSc2	Kopfrkinglo
vrah	vrah	k1gMnSc1	vrah
své	svůj	k3xOyFgFnSc2	svůj
milované	milovaný	k2eAgFnSc2d1	milovaná
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
příslušník	příslušník	k1gMnSc1	příslušník
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
NSDAP	NSDAP	kA	NSDAP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
šéfováním	šéfování	k1gNnSc7	šéfování
nových	nový	k2eAgNnPc2d1	nové
plynových	plynový	k2eAgNnPc2d1	plynové
žárovišť	žároviště	k1gNnPc2	žároviště
<g/>
.	.	kIx.	.
</s>
<s>
Deformace	deformace	k1gFnSc1	deformace
jeho	jeho	k3xOp3gFnSc2	jeho
psychiky	psychika	k1gFnSc2	psychika
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
umocněna	umocněn	k2eAgFnSc1d1	umocněna
návštěvami	návštěva	k1gFnPc7	návštěva
snového	snový	k2eAgInSc2d1	snový
"	"	kIx"	"
<g/>
tibetského	tibetský	k2eAgMnSc2d1	tibetský
vyslance	vyslanec	k1gMnSc2	vyslanec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
nazývá	nazývat	k5eAaImIp3nS	nazývat
novým	nový	k2eAgMnSc7d1	nový
dalajlámou	dalajláma	k1gMnSc7	dalajláma
a	a	k8xC	a
buddhou	buddha	k1gMnSc7	buddha
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílnost	rozdílnost	k1gFnSc1	rozdílnost
filmu	film	k1gInSc2	film
a	a	k8xC	a
knižní	knižní	k2eAgFnSc2d1	knižní
předlohy	předloha	k1gFnSc2	předloha
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgMnSc1d1	minimální
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
úprava	úprava	k1gFnSc1	úprava
scény	scéna	k1gFnSc2	scéna
v	v	k7c6	v
panoptiku	panoptikum	k1gNnSc6	panoptikum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
filmu	film	k1gInSc6	film
vylíčena	vylíčen	k2eAgFnSc1d1	vylíčena
daleko	daleko	k6eAd1	daleko
tvrdším	tvrdý	k2eAgInSc7d2	tvrdší
a	a	k8xC	a
morbidnějším	morbidní	k2eAgInSc7d2	morbidnější
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
časté	častý	k2eAgNnSc1d1	časté
<g/>
,	,	kIx,	,
že	že	k8xS	že
dialogy	dialog	k1gInPc1	dialog
a	a	k8xC	a
monology	monolog	k1gInPc1	monolog
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zaměňují	zaměňovat	k5eAaImIp3nP	zaměňovat
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
na	na	k7c6	na
rozdílných	rozdílný	k2eAgNnPc6d1	rozdílné
místech	místo	k1gNnPc6	místo
než	než	k8xS	než
v	v	k7c6	v
předloze	předloha	k1gFnSc6	předloha
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
předloze	předloha	k1gFnSc6	předloha
se	se	k3xPyFc4	se
u	u	k7c2	u
postav	postava	k1gFnPc2	postava
objevuje	objevovat	k5eAaImIp3nS	objevovat
hovorová	hovorový	k2eAgFnSc1d1	hovorová
řeč	řeč	k1gFnSc1	řeč
i	i	k8xC	i
podobné	podobný	k2eAgNnSc1d1	podobné
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
totožné	totožný	k2eAgFnPc1d1	totožná
větné	větný	k2eAgFnPc1d1	větná
konstrukce	konstrukce	k1gFnPc1	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
záměna	záměna	k1gFnSc1	záměna
kapitol	kapitola	k1gFnPc2	kapitola
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
časová	časový	k2eAgFnSc1d1	časová
posloupnost	posloupnost	k1gFnSc1	posloupnost
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
ve	v	k7c6	v
filmu	film	k1gInSc6	film
podaná	podaný	k2eAgNnPc4d1	podané
rozdílně	rozdílně	k6eAd1	rozdílně
<g/>
.	.	kIx.	.
</s>
<s>
Občasné	občasný	k2eAgNnSc1d1	občasné
je	být	k5eAaImIp3nS	být
i	i	k9	i
vynechání	vynechání	k1gNnSc1	vynechání
detailů	detail	k1gInPc2	detail
a	a	k8xC	a
vysvětlujících	vysvětlující	k2eAgInPc2d1	vysvětlující
rozhovorů	rozhovor	k1gInPc2	rozhovor
a	a	k8xC	a
dějů	děj	k1gInPc2	děj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
také	také	k9	také
některé	některý	k3yIgFnPc1	některý
události	událost	k1gFnPc1	událost
úplně	úplně	k6eAd1	úplně
mizí	mizet	k5eAaImIp3nP	mizet
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
jsou	být	k5eAaImIp3nP	být
interpretovány	interpretovat	k5eAaBmNgInP	interpretovat
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc1d1	zásadní
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
Kopfrkingl	Kopfrkingla	k1gFnPc2	Kopfrkingla
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
židovského	židovský	k2eAgNnSc2d1	Židovské
pohřebního	pohřební	k2eAgNnSc2d1	pohřební
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
–	–	k?	–
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
za	za	k7c4	za
žebráka	žebrák	k1gMnSc4	žebrák
a	a	k8xC	a
ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
nechá	nechat	k5eAaPmIp3nS	nechat
pozvat	pozvat	k5eAaPmF	pozvat
svým	svůj	k3xOyFgMnSc7	svůj
lékařem	lékař	k1gMnSc7	lékař
Bettleheimem	Bettleheim	k1gMnSc7	Bettleheim
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
některé	některý	k3yIgFnPc4	některý
scény	scéna	k1gFnPc4	scéna
navíc	navíc	k6eAd1	navíc
jako	jako	k8xC	jako
návštěva	návštěva	k1gFnSc1	návštěva
Kopfrkingla	Kopfrkinglo	k1gNnSc2	Kopfrkinglo
v	v	k7c6	v
"	"	kIx"	"
<g/>
masérském	masérský	k2eAgInSc6d1	masérský
salónu	salón	k1gInSc6	salón
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
je	být	k5eAaImIp3nS	být
též	též	k9	též
postava	postava	k1gFnSc1	postava
bledé	bledý	k2eAgFnSc2d1	bledá
mlčící	mlčící	k2eAgFnSc2d1	mlčící
dívky	dívka	k1gFnSc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
novely	novela	k1gFnSc2	novela
Kopfrkingla	Kopfrkinglo	k1gNnSc2	Kopfrkinglo
odváží	odvážet	k5eAaImIp3nS	odvážet
sanitka	sanitka	k1gFnSc1	sanitka
(	(	kIx(	(
<g/>
snad	snad	k9	snad
do	do	k7c2	do
blázince	blázinec	k1gInSc2	blázinec
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
poslední	poslední	k2eAgFnSc1d1	poslední
scéna	scéna	k1gFnSc1	scéna
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
v	v	k7c6	v
sanitním	sanitní	k2eAgInSc6d1	sanitní
vlaku	vlak	k1gInSc6	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
scény	scéna	k1gFnPc1	scéna
ve	v	k7c6	v
filmu	film	k1gInSc6	film
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
režisér	režisér	k1gMnSc1	režisér
Herz	Herz	k1gMnSc1	Herz
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
psychologickými	psychologický	k2eAgInPc7d1	psychologický
filmy	film	k1gInPc7	film
Alfreda	Alfred	k1gMnSc2	Alfred
Hitchcocka	Hitchcocka	k1gFnSc1	Hitchcocka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
filmy	film	k1gInPc4	film
Victora	Victor	k1gMnSc2	Victor
Sjöströma	Sjöström	k1gMnSc2	Sjöström
<g/>
,	,	kIx,	,
Ingmara	Ingmar	k1gMnSc2	Ingmar
Bergmana	Bergman	k1gMnSc2	Bergman
<g/>
,	,	kIx,	,
či	či	k8xC	či
Luise	Luisa	k1gFnSc3	Luisa
Buñ	Buñ	k1gFnPc2	Buñ
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ve	v	k7c6	v
filmu	film	k1gInSc6	film
jistý	jistý	k2eAgInSc4d1	jistý
vliv	vliv	k1gInSc4	vliv
jejich	jejich	k3xOp3gFnSc2	jejich
tvorby	tvorba	k1gFnSc2	tvorba
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
režisér	režisér	k1gMnSc1	režisér
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
cílené	cílený	k2eAgNnSc4d1	cílené
napodobování	napodobování	k1gNnSc4	napodobování
<g/>
.	.	kIx.	.
</s>
<s>
Zřetelný	zřetelný	k2eAgInSc1d1	zřetelný
je	být	k5eAaImIp3nS	být
vliv	vliv	k1gInSc1	vliv
surrealistické	surrealistický	k2eAgFnSc2d1	surrealistická
tvorby	tvorba	k1gFnSc2	tvorba
autorova	autorův	k2eAgMnSc2d1	autorův
generačního	generační	k2eAgMnSc2d1	generační
současníka	současník	k1gMnSc2	současník
a	a	k8xC	a
přítele	přítel	k1gMnSc2	přítel
Jana	Jan	k1gMnSc2	Jan
Švankmajera	Švankmajer	k1gMnSc2	Švankmajer
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
scéna	scéna	k1gFnSc1	scéna
v	v	k7c6	v
zoologické	zoologický	k2eAgFnSc6d1	zoologická
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
precizní	precizní	k2eAgFnSc2d1	precizní
režie	režie	k1gFnSc2	režie
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
expresivní	expresivní	k2eAgInSc4d1	expresivní
nádech	nádech	k1gInSc4	nádech
filmu	film	k1gInSc2	film
kamera	kamera	k1gFnSc1	kamera
Stanislava	Stanislava	k1gFnSc1	Stanislava
Miloty	milota	k1gFnSc2	milota
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
natáčel	natáčet	k5eAaImAgMnS	natáčet
Rudolfa	Rudolf	k1gMnSc4	Rudolf
Hrušínského	Hrušínský	k2eAgMnSc4d1	Hrušínský
z	z	k7c2	z
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
úhlů	úhel	k1gInPc2	úhel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
detailech	detail	k1gInPc6	detail
a	a	k8xC	a
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
speciálních	speciální	k2eAgFnPc2d1	speciální
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
bylo	být	k5eAaImAgNnS	být
–	–	k?	–
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
téměř	téměř	k6eAd1	téměř
neznámé	neznámá	k1gFnSc2	neznámá
–	–	k?	–
rybí	rybí	k2eAgNnSc1d1	rybí
oko	oko	k1gNnSc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféru	atmosféra	k1gFnSc4	atmosféra
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
výsledný	výsledný	k2eAgInSc1d1	výsledný
rychlý	rychlý	k2eAgInSc1d1	rychlý
střih	střih	k1gInSc1	střih
a	a	k8xC	a
prostřihy	prostřih	k1gInPc1	prostřih
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Janáčka	Janáček	k1gMnSc2	Janáček
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Lišky	Liška	k1gMnSc2	Liška
<g/>
.	.	kIx.	.
</s>
<s>
Notně	notně	k6eAd1	notně
se	se	k3xPyFc4	se
na	na	k7c6	na
vyznění	vyznění	k1gNnSc6	vyznění
díla	dílo	k1gNnSc2	dílo
podílí	podílet	k5eAaImIp3nS	podílet
vysoce	vysoce	k6eAd1	vysoce
ceněné	ceněný	k2eAgNnSc1d1	ceněné
herectví	herectví	k1gNnSc1	herectví
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Hrušínského	Hrušínský	k2eAgMnSc2d1	Hrušínský
v	v	k7c6	v
roli	role	k1gFnSc6	role
Kopfrkingla	Kopfrkinglo	k1gNnSc2	Kopfrkinglo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
monology	monolog	k1gInPc1	monolog
zabírají	zabírat	k5eAaImIp3nP	zabírat
až	až	k9	až
60	[number]	k4	60
%	%	kIx~	%
filmového	filmový	k2eAgInSc2d1	filmový
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Filmové	filmový	k2eAgInPc1d1	filmový
prostředky	prostředek	k1gInPc1	prostředek
též	též	k9	též
umožnily	umožnit	k5eAaPmAgInP	umožnit
zajímavě	zajímavě	k6eAd1	zajímavě
zpracovat	zpracovat	k5eAaPmF	zpracovat
specifický	specifický	k2eAgInSc1d1	specifický
vztah	vztah	k1gInSc1	vztah
Karla	Karel	k1gMnSc2	Karel
Kopfrkingla	Kopfrkingl	k1gMnSc2	Kopfrkingl
k	k	k7c3	k
obrazům	obraz	k1gInPc3	obraz
či	či	k8xC	či
fotografiím	fotografia	k1gFnPc3	fotografia
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
definoval	definovat	k5eAaBmAgMnS	definovat
už	už	k6eAd1	už
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
novele	novela	k1gFnSc6	novela
<g/>
.	.	kIx.	.
</s>
<s>
Záběry	záběr	k1gInPc1	záběr
vybraných	vybraný	k2eAgInPc2d1	vybraný
obrazů	obraz	k1gInPc2	obraz
divákovi	divák	k1gMnSc3	divák
představuje	představovat	k5eAaImIp3nS	představovat
psychiku	psychika	k1gFnSc4	psychika
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vidno	vidno	k1gNnSc1	vidno
např	např	kA	např
ve	v	k7c6	v
scéně	scéna	k1gFnSc6	scéna
u	u	k7c2	u
rámaře	rámař	k1gMnSc2	rámař
Holého	Holý	k1gMnSc2	Holý
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
filmu	film	k1gInSc6	film
také	také	k6eAd1	také
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
podtržení	podtržení	k1gNnSc3	podtržení
výhradně	výhradně	k6eAd1	výhradně
hudebních	hudební	k2eAgFnPc2d1	hudební
nebo	nebo	k8xC	nebo
mluvených	mluvený	k2eAgFnPc2d1	mluvená
pasáží	pasáž	k1gFnPc2	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
Nejpatrněji	patrně	k6eAd3	patrně
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
vidět	vidět	k5eAaImF	vidět
ve	v	k7c6	v
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Kopfrkingl	Kopfrkingl	k1gMnSc1	Kopfrkingl
představuje	představovat	k5eAaImIp3nS	představovat
svoji	svůj	k3xOyFgFnSc4	svůj
koncepci	koncepce	k1gFnSc4	koncepce
velkých	velký	k2eAgFnPc2d1	velká
pecí	pec	k1gFnPc2	pec
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
s	s	k7c7	s
pravým	pravý	k2eAgNnSc7d1	pravé
křídlem	křídlo	k1gNnSc7	křídlo
triptychu	triptych	k1gInSc2	triptych
Zahrada	zahrada	k1gFnSc1	zahrada
pozemských	pozemský	k2eAgFnPc2d1	pozemská
rozkoší	rozkoš	k1gFnPc2	rozkoš
od	od	k7c2	od
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Bosche	Bosch	k1gMnSc2	Bosch
<g/>
:	:	kIx,	:
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
projev	projev	k1gInSc1	projev
stává	stávat	k5eAaImIp3nS	stávat
intenzivnější	intenzivní	k2eAgFnSc1d2	intenzivnější
začne	začít	k5eAaPmIp3nS	začít
kamera	kamera	k1gFnSc1	kamera
bloudivě	bloudivě	k6eAd1	bloudivě
zabírat	zabírat	k5eAaImF	zabírat
detaily	detail	k1gInPc4	detail
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
i	i	k9	i
záběry	záběr	k1gInPc1	záběr
z	z	k7c2	z
obrazů	obraz	k1gInPc2	obraz
jako	jako	k8xC	jako
Nesení	nesení	k1gNnSc2	nesení
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
Let	léto	k1gNnPc2	léto
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
Sedm	sedm	k4xCc1	sedm
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
hříchů	hřích	k1gInPc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
ve	v	k7c6	v
výsledné	výsledný	k2eAgFnSc6d1	výsledná
expresivitě	expresivita	k1gFnSc6	expresivita
filmu	film	k1gInSc2	film
hrají	hrát	k5eAaImIp3nP	hrát
též	též	k9	též
exteriéry	exteriér	k1gInPc1	exteriér
a	a	k8xC	a
interiéry	interiér	k1gInPc1	interiér
krematorií	krematorium	k1gNnPc2	krematorium
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	on	k3xPp3gNnSc4	on
pardubické	pardubický	k2eAgNnSc4d1	pardubické
krematorium	krematorium	k1gNnSc4	krematorium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
objevily	objevit	k5eAaPmAgInP	objevit
záběry	záběr	k1gInPc1	záběr
urnového	urnový	k2eAgInSc2d1	urnový
háje	háj	k1gInSc2	háj
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
či	či	k8xC	či
krematoria	krematorium	k1gNnSc2	krematorium
v	v	k7c6	v
Praze-Strašnicích	Praze-Strašnice	k1gFnPc6	Praze-Strašnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
krematoriu	krematorium	k1gNnSc6	krematorium
se	se	k3xPyFc4	se
natáčelo	natáčet	k5eAaImAgNnS	natáčet
spalování	spalování	k1gNnSc1	spalování
<g/>
,	,	kIx,	,
záležitosti	záležitost	k1gFnPc1	záležitost
s	s	k7c7	s
pecemi	pec	k1gFnPc7	pec
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
novele	novela	k1gFnSc6	novela
Ladislava	Ladislav	k1gMnSc2	Ladislav
Fukse	Fuks	k1gMnSc2	Fuks
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Režiséru	režisér	k1gMnSc3	režisér
Juraji	Juraj	k1gMnSc3	Juraj
Herzovi	Herz	k1gMnSc3	Herz
se	se	k3xPyFc4	se
zamlouval	zamlouvat	k5eAaImAgMnS	zamlouvat
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
způsobil	způsobit	k5eAaPmAgMnS	způsobit
první	první	k4xOgInSc4	první
zájem	zájem	k1gInSc4	zájem
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přečtení	přečtení	k1gNnSc6	přečtení
ale	ale	k8xC	ale
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
děj	děj	k1gInSc1	děj
příliš	příliš	k6eAd1	příliš
nezamlouvá	zamlouvat	k5eNaImIp3nS	zamlouvat
pro	pro	k7c4	pro
natočení	natočení	k1gNnSc4	natočení
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
k	k	k7c3	k
úpravám	úprava	k1gFnPc3	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
vznikal	vznikat	k5eAaImAgInS	vznikat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
autorem	autor	k1gMnSc7	autor
knihy	kniha	k1gFnSc2	kniha
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
a	a	k8xC	a
Fuks	Fuks	k1gMnSc1	Fuks
nechal	nechat	k5eAaPmAgMnS	nechat
Herzovi	Herz	k1gMnSc3	Herz
téměř	téměř	k6eAd1	téměř
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
tvůrčí	tvůrčí	k2eAgFnSc1d1	tvůrčí
skupina	skupina	k1gFnSc1	skupina
Jiřího	Jiří	k1gMnSc2	Jiří
Šebora	Šebor	k1gMnSc2	Šebor
a	a	k8xC	a
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Bora	Borus	k1gMnSc2	Borus
<g/>
.	.	kIx.	.
</s>
<s>
Juraj	Juraj	k1gInSc1	Juraj
Herz	Herz	k1gInSc1	Herz
si	se	k3xPyFc3	se
našel	najít	k5eAaPmAgInS	najít
kameramana	kameraman	k1gMnSc4	kameraman
Stanislava	Stanislav	k1gMnSc4	Stanislav
Milotu	milota	k1gFnSc4	milota
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
napsal	napsat	k5eAaBmAgInS	napsat
technický	technický	k2eAgInSc4d1	technický
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
popsali	popsat	k5eAaPmAgMnP	popsat
každý	každý	k3xTgInSc4	každý
záběr	záběr	k1gInSc4	záběr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
tohoto	tento	k3xDgInSc2	tento
scénáře	scénář	k1gInSc2	scénář
striktně	striktně	k6eAd1	striktně
drželi	držet	k5eAaImAgMnP	držet
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
měl	mít	k5eAaImAgInS	mít
Herz	Herz	k1gInSc1	Herz
vyhlédnutého	vyhlédnutý	k2eAgMnSc2d1	vyhlédnutý
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Hrušínského	Hrušínský	k2eAgMnSc2d1	Hrušínský
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
technický	technický	k2eAgInSc1d1	technický
scénář	scénář	k1gInSc1	scénář
příliš	příliš	k6eAd1	příliš
nezamlouval	zamlouvat	k5eNaImAgInS	zamlouvat
(	(	kIx(	(
<g/>
málo	málo	k6eAd1	málo
hereckého	herecký	k2eAgInSc2d1	herecký
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
úvodních	úvodní	k2eAgInPc6d1	úvodní
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
natáčení	natáčení	k1gNnSc1	natáčení
začalo	začít	k5eAaPmAgNnS	začít
líbit	líbit	k5eAaImF	líbit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
poté	poté	k6eAd1	poté
výborná	výborný	k2eAgFnSc1d1	výborná
spolupráce	spolupráce	k1gFnSc1	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
v	v	k7c6	v
krematoriích	krematorium	k1gNnPc6	krematorium
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
rychlé	rychlý	k2eAgNnSc1d1	rychlé
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
štábu	štáb	k1gInSc2	štáb
je	být	k5eAaImIp3nS	být
chtěl	chtít	k5eAaImAgMnS	chtít
mít	mít	k5eAaImF	mít
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Rakve	rakev	k1gFnPc1	rakev
se	s	k7c7	s
skutečnými	skutečný	k2eAgFnPc7d1	skutečná
mrtvolami	mrtvola	k1gFnPc7	mrtvola
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
místa	místo	k1gNnSc2	místo
<g/>
)	)	kIx)	)
ležely	ležet	k5eAaImAgFnP	ležet
venku	venku	k6eAd1	venku
a	a	k8xC	a
místnostmi	místnost	k1gFnPc7	místnost
se	se	k3xPyFc4	se
šířil	šířit	k5eAaImAgInS	šířit
zápach	zápach	k1gInSc1	zápach
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
byly	být	k5eAaImAgFnP	být
letní	letní	k2eAgInPc4d1	letní
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc4	červenec
a	a	k8xC	a
srpen	srpen	k1gInSc4	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Produkční	produkční	k2eAgMnSc1d1	produkční
rozléval	rozlévat	k5eAaImAgMnS	rozlévat
po	po	k7c6	po
krematoriu	krematorium	k1gNnSc6	krematorium
vodu	voda	k1gFnSc4	voda
s	s	k7c7	s
aromatem	aroma	k1gNnSc7	aroma
lesní	lesní	k2eAgFnSc2d1	lesní
vůně	vůně	k1gFnSc2	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Juraj	Juraj	k1gMnSc1	Juraj
Herz	Herz	k1gMnSc1	Herz
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdykoli	kdykoli	k6eAd1	kdykoli
ucítil	ucítit	k5eAaPmAgMnS	ucítit
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
biografu	biograf	k1gInSc2	biograf
lesní	lesní	k2eAgFnSc4d1	lesní
vůni	vůně	k1gFnSc4	vůně
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
rozstřikovala	rozstřikovat	k5eAaImAgFnS	rozstřikovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
udělalo	udělat	k5eAaPmAgNnS	udělat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
<s>
Poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
film	film	k1gInSc1	film
zařadit	zařadit	k5eAaPmF	zařadit
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
to	ten	k3xDgNnSc1	ten
spíše	spíše	k9	spíše
na	na	k7c6	na
kulturním	kulturní	k2eAgNnSc6d1	kulturní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
byl	být	k5eAaImAgInS	být
snímek	snímek	k1gInSc1	snímek
promítán	promítat	k5eAaImNgInS	promítat
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pojat	pojmout	k5eAaPmNgInS	pojmout
jako	jako	k9	jako
horor	horor	k1gInSc1	horor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lidé	člověk	k1gMnPc1	člověk
vycházeli	vycházet	k5eAaImAgMnP	vycházet
z	z	k7c2	z
kin	kino	k1gNnPc2	kino
šokovaní	šokovaný	k2eAgMnPc1d1	šokovaný
(	(	kIx(	(
<g/>
pohřeb	pohřebit	k5eAaPmRp2nS	pohřebit
žehem	žeh	k1gInSc7	žeh
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
neprovozoval	provozovat	k5eNaImAgMnS	provozovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
během	během	k7c2	během
filmu	film	k1gInSc2	film
dokázali	dokázat	k5eAaPmAgMnP	dokázat
pobavit	pobavit	k5eAaPmF	pobavit
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
režisér	režisér	k1gMnSc1	režisér
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
<g/>
,	,	kIx,	,
točil	točit	k5eAaImAgMnS	točit
jej	on	k3xPp3gNnSc4	on
jako	jako	k9	jako
film	film	k1gInSc1	film
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
černé	černý	k2eAgFnSc2d1	černá
komedie	komedie	k1gFnSc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
lidé	člověk	k1gMnPc1	člověk
film	film	k1gInSc4	film
přijali	přijmout	k5eAaPmAgMnP	přijmout
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
téma	téma	k1gNnSc1	téma
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
dotýkalo	dotýkat	k5eAaImAgNnS	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
občas	občas	k6eAd1	občas
bylo	být	k5eAaImAgNnS	být
sladit	sladit	k5eAaImF	sladit
výkony	výkon	k1gInPc4	výkon
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Hrušínského	Hrušínský	k2eAgMnSc2d1	Hrušínský
a	a	k8xC	a
Vlasty	Vlasta	k1gMnSc2	Vlasta
Chramostové	Chramostová	k1gFnSc2	Chramostová
<g/>
.	.	kIx.	.
</s>
<s>
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
zahrál	zahrát	k5eAaPmAgMnS	zahrát
nejlépe	dobře	k6eAd3	dobře
na	na	k7c4	na
první	první	k4xOgInSc4	první
pokus	pokus	k1gInSc4	pokus
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
se	se	k3xPyFc4	se
zhoršoval	zhoršovat	k5eAaImAgMnS	zhoršovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
s	s	k7c7	s
Vlastou	Vlasta	k1gFnSc7	Vlasta
Chramostovou	Chramostová	k1gFnSc7	Chramostová
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
si	se	k3xPyFc3	se
scénu	scéna	k1gFnSc4	scéna
nacvičit	nacvičit	k5eAaBmF	nacvičit
a	a	k8xC	a
s	s	k7c7	s
každým	každý	k3xTgNnSc7	každý
opakováním	opakování	k1gNnSc7	opakování
se	se	k3xPyFc4	se
lepšila	lepšit	k5eAaImAgFnS	lepšit
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
s	s	k7c7	s
natáčením	natáčení	k1gNnSc7	natáčení
v	v	k7c6	v
krematoriu	krematorium	k1gNnSc6	krematorium
neměli	mít	k5eNaImAgMnP	mít
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
prostředí	prostředí	k1gNnSc1	prostředí
nesvědčilo	svědčit	k5eNaImAgNnS	svědčit
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Herz	Herz	k1gMnSc1	Herz
se	se	k3xPyFc4	se
k	k	k7c3	k
filmu	film	k1gInSc3	film
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jediný	jediný	k2eAgInSc1d1	jediný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gInSc4	on
chtěl	chtít	k5eAaImAgMnS	chtít
je	on	k3xPp3gNnPc4	on
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jedné	jeden	k4xCgFnSc2	jeden
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tato	tento	k3xDgFnSc1	tento
scéna	scéna	k1gFnSc1	scéna
byla	být	k5eAaImAgFnS	být
dotočena	dotočen	k2eAgFnSc1d1	dotočena
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jistá	jistý	k2eAgFnSc1d1	jistá
variace	variace	k1gFnSc1	variace
původního	původní	k2eAgInSc2d1	původní
Fuksova	Fuksův	k2eAgInSc2d1	Fuksův
dovětku	dovětek	k1gInSc2	dovětek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pan	pan	k1gMnSc1	pan
Kopfrkingl	Kopfrkingl	k1gMnSc1	Kopfrkingl
sleduje	sledovat	k5eAaImIp3nS	sledovat
zbídačené	zbídačený	k2eAgMnPc4d1	zbídačený
lidi	člověk	k1gMnPc4	člověk
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
:	:	kIx,	:
dva	dva	k4xCgMnPc1	dva
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
krematoria	krematorium	k1gNnSc2	krematorium
sedí	sedit	k5eAaImIp3nP	sedit
v	v	k7c6	v
kavárně	kavárna	k1gFnSc6	kavárna
v	v	k7c6	v
Reprezentačním	reprezentační	k2eAgInSc6d1	reprezentační
domě	dům	k1gInSc6	dům
a	a	k8xC	a
za	za	k7c7	za
oknem	okno	k1gNnSc7	okno
projíždějí	projíždět	k5eAaImIp3nP	projíždět
ruské	ruský	k2eAgInPc1d1	ruský
okupační	okupační	k2eAgInPc1d1	okupační
tanky	tank	k1gInPc1	tank
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c6	o
panu	pan	k1gMnSc6	pan
Kopfrkinglovi	Kopfrkingl	k1gMnSc6	Kopfrkingl
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
milý	milý	k2eAgMnSc1d1	milý
pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
stalo	stát	k5eAaPmAgNnS	stát
<g/>
?	?	kIx.	?
</s>
<s>
Další	další	k2eAgInSc1d1	další
záběr	záběr	k1gInSc1	záběr
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
rozbité	rozbitý	k2eAgNnSc4d1	rozbité
Muzeum	muzeum	k1gNnSc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
záběr	záběr	k1gInSc1	záběr
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
naštvané	naštvaný	k2eAgFnPc4d1	naštvaná
tváře	tvář	k1gFnPc4	tvář
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
se	se	k3xPyFc4	se
najednou	najednou	k6eAd1	najednou
vyloupne	vyloupnout	k5eAaPmIp3nS	vyloupnout
usmívající	usmívající	k2eAgNnSc1d1	usmívající
se	se	k3xPyFc4	se
pan	pan	k1gMnSc1	pan
Kopfrkingl	Kopfrkingl	k1gMnSc1	Kopfrkingl
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
velké	velká	k1gFnSc3	velká
provokativnosti	provokativnost	k1gFnSc2	provokativnost
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
scéna	scéna	k1gFnSc1	scéna
vystřižena	vystřižen	k2eAgFnSc1d1	vystřižena
<g/>
.	.	kIx.	.
</s>
<s>
Kameraman	kameraman	k1gMnSc1	kameraman
Stanislav	Stanislav	k1gMnSc1	Stanislav
Milota	milota	k1gFnSc1	milota
natáčení	natáčení	k1gNnSc4	natáčení
filmu	film	k1gInSc2	film
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
životní	životní	k2eAgInSc4d1	životní
zážitek	zážitek	k1gInSc4	zážitek
díky	díky	k7c3	díky
silnému	silný	k2eAgInSc3d1	silný
příběhu	příběh	k1gInSc3	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
bude	být	k5eAaImBp3nS	být
snímat	snímat	k5eAaImF	snímat
a	a	k8xC	a
vzpomněl	vzpomnít	k5eAaPmAgMnS	vzpomnít
si	se	k3xPyFc3	se
na	na	k7c4	na
druh	druh	k1gInSc4	druh
širokoúhlého	širokoúhlý	k2eAgInSc2d1	širokoúhlý
objektivu	objektiv	k1gInSc2	objektiv
zvaného	zvaný	k2eAgInSc2d1	zvaný
rybí	rybí	k2eAgNnSc4d1	rybí
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
na	na	k7c4	na
stěžejní	stěžejní	k2eAgInPc4d1	stěžejní
úseky	úsek	k1gInPc4	úsek
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Prosadil	prosadit	k5eAaPmAgMnS	prosadit
si	se	k3xPyFc3	se
na	na	k7c4	na
režisérovi	režisér	k1gMnSc3	režisér
černobílý	černobílý	k2eAgInSc4d1	černobílý
snímek	snímek	k1gInSc4	snímek
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
Herz	Herz	k1gMnSc1	Herz
chtěl	chtít	k5eAaImAgMnS	chtít
mít	mít	k5eAaImF	mít
film	film	k1gInSc4	film
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Natáčelo	natáčet	k5eAaImAgNnS	natáčet
se	se	k3xPyFc4	se
kamerou	kamera	k1gFnSc7	kamera
Arriflex	Arriflex	k1gInSc1	Arriflex
(	(	kIx(	(
<g/>
kamera	kamera	k1gFnSc1	kamera
bez	bez	k7c2	bez
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
přidán	přidat	k5eAaPmNgInS	přidat
postsynchronem	postsynchron	k1gInSc7	postsynchron
<g/>
)	)	kIx)	)
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
snímků	snímek	k1gInPc2	snímek
natáčel	natáčet	k5eAaImAgMnS	natáčet
Milota	milota	k1gFnSc1	milota
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zdařilý	zdařilý	k2eAgInSc4d1	zdařilý
a	a	k8xC	a
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
taková	takový	k3xDgFnSc1	takový
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
tragickou	tragický	k2eAgFnSc4d1	tragická
rovinu	rovina	k1gFnSc4	rovina
-	-	kIx~	-
"	"	kIx"	"
<g/>
Kopfrkinglové	Kopfrkingl	k1gMnPc1	Kopfrkingl
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
mezi	mezi	k7c7	mezi
námi	my	k3xPp1nPc7	my
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Herečka	herečka	k1gFnSc1	herečka
Vlasta	Vlasta	k1gFnSc1	Vlasta
Chramostová	Chramostová	k1gFnSc1	Chramostová
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
Stanislavem	Stanislav	k1gMnSc7	Stanislav
Milotou	milota	k1gFnSc7	milota
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
začal	začít	k5eAaPmAgInS	začít
točit	točit	k5eAaImF	točit
v	v	k7c6	v
uvolněné	uvolněný	k2eAgFnSc6d1	uvolněná
předsrpnové	předsrpnový	k2eAgFnSc6d1	předsrpnový
atmosféře	atmosféra	k1gFnSc6	atmosféra
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
nemohl	moct	k5eNaImAgMnS	moct
tušit	tušit	k5eAaImF	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
stane	stanout	k5eAaPmIp3nS	stanout
oceňovaný	oceňovaný	k2eAgInSc1d1	oceňovaný
kultovní	kultovní	k2eAgInSc1d1	kultovní
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dotočení	dotočení	k1gNnSc6	dotočení
a	a	k8xC	a
premiéře	premiéra	k1gFnSc6	premiéra
film	film	k1gInSc1	film
putoval	putovat	k5eAaImAgInS	putovat
do	do	k7c2	do
trezoru	trezor	k1gInSc2	trezor
<g/>
.	.	kIx.	.
</s>
<s>
Přidává	přidávat	k5eAaImIp3nS	přidávat
pikantní	pikantní	k2eAgFnSc4d1	pikantní
historku	historka	k1gFnSc4	historka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Stanislav	Stanislav	k1gMnSc1	Stanislav
Milota	milota	k1gFnSc1	milota
(	(	kIx(	(
<g/>
kameraman	kameraman	k1gMnSc1	kameraman
filmu	film	k1gInSc2	film
<g/>
)	)	kIx)	)
prorocky	prorocky	k6eAd1	prorocky
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
chtějí	chtít	k5eAaImIp3nP	chtít
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
točit	točit	k5eAaImF	točit
scénu	scéna	k1gFnSc4	scéna
s	s	k7c7	s
oběšením	oběšení	k1gNnSc7	oběšení
představitelky	představitelka	k1gFnSc2	představitelka
její	její	k3xOp3gFnSc2	její
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Milota	milota	k1gFnSc1	milota
se	s	k7c7	s
sarkasmem	sarkasmus	k1gInSc7	sarkasmus
hodným	hodný	k2eAgInSc7d1	hodný
Spalovače	spalovač	k1gMnPc4	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
odvětil	odvětit	k5eAaPmAgInS	odvětit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Přece	přece	k9	přece
jako	jako	k9	jako
tvůj	tvůj	k3xOp2gInSc4	tvůj
poslední	poslední	k2eAgInSc4d1	poslední
záběr	záběr	k1gInSc4	záběr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nemohl	moct	k5eNaImAgMnS	moct
tušit	tušit	k5eAaImF	tušit
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
vyřkl	vyřknout	k5eAaPmAgMnS	vyřknout
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Chramostová	Chramostová	k1gFnSc1	Chramostová
po	po	k7c6	po
srpnu	srpen	k1gInSc6	srpen
1968	[number]	k4	1968
nesměla	smět	k5eNaImAgFnS	smět
točit	točit	k5eAaImF	točit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
filmu	film	k1gInSc6	film
skončil	skončit	k5eAaPmAgMnS	skončit
i	i	k8xC	i
Stanislav	Stanislav	k1gMnSc1	Stanislav
Milota	milota	k1gFnSc1	milota
<g/>
.	.	kIx.	.
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
S	s	k7c7	s
výsledným	výsledný	k2eAgInSc7d1	výsledný
filmem	film	k1gInSc7	film
byl	být	k5eAaImAgMnS	být
autor	autor	k1gMnSc1	autor
námětu	námět	k1gInSc2	námět
Fuks	Fuks	k1gMnSc1	Fuks
spokojen	spokojen	k2eAgMnSc1d1	spokojen
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
ho	on	k3xPp3gNnSc4	on
také	také	k9	také
přijímá	přijímat	k5eAaImIp3nS	přijímat
vcelku	vcelku	k6eAd1	vcelku
kladně	kladně	k6eAd1	kladně
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
scénu	scéna	k1gFnSc4	scéna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
z	z	k7c2	z
ideologických	ideologický	k2eAgInPc2d1	ideologický
důvodů	důvod	k1gInPc2	důvod
vystřižena	vystřižen	k2eAgFnSc1d1	vystřižena
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
režisér	režisér	k1gMnSc1	režisér
viděl	vidět	k5eAaImAgMnS	vidět
ve	v	k7c6	v
filmu	film	k1gInSc6	film
"	"	kIx"	"
<g/>
černou	černý	k2eAgFnSc4d1	černá
komedii	komedie	k1gFnSc4	komedie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
přijímán	přijímat	k5eAaImNgInS	přijímat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
promítán	promítat	k5eAaImNgInS	promítat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c4	po
invaze	invaze	k1gFnPc4	invaze
armád	armáda	k1gFnPc2	armáda
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
proměna	proměna	k1gFnSc1	proměna
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
v	v	k7c4	v
nacistu	nacista	k1gMnSc4	nacista
chápána	chápat	k5eAaImNgFnS	chápat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
cokoli	cokoli	k3yInSc4	cokoli
jiného	jiný	k2eAgNnSc2d1	jiné
jako	jako	k8xC	jako
alegorie	alegorie	k1gFnSc1	alegorie
nastupující	nastupující	k2eAgFnSc2d1	nastupující
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
pro	pro	k7c4	pro
přílišnou	přílišný	k2eAgFnSc4d1	přílišná
morbidnost	morbidnost	k1gFnSc4	morbidnost
stažen	stáhnout	k5eAaPmNgMnS	stáhnout
z	z	k7c2	z
distribuce	distribuce	k1gFnSc2	distribuce
a	a	k8xC	a
uložen	uložit	k5eAaPmNgMnS	uložit
do	do	k7c2	do
trezoru	trezor	k1gInSc2	trezor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
vyzněl	vyznít	k5eAaPmAgInS	vyznít
film	film	k1gInSc1	film
jako	jako	k8xC	jako
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
jako	jako	k9	jako
horor	horor	k1gInSc1	horor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
kremace	kremace	k1gFnSc1	kremace
problematické	problematický	k2eAgNnSc1d1	problematické
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
široké	široký	k2eAgNnSc4d1	široké
publikum	publikum	k1gNnSc4	publikum
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
promítán	promítán	k2eAgInSc1d1	promítán
kině	kino	k1gNnSc6	kino
Accatone	Accaton	k1gInSc5	Accaton
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
nejsledovanějším	sledovaný	k2eAgInSc7d3	nejsledovanější
filmem	film	k1gInSc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
měl	mít	k5eAaImAgMnS	mít
také	také	k6eAd1	také
majitel	majitel	k1gMnSc1	majitel
kina	kino	k1gNnSc2	kino
Kazik	kazik	k1gMnSc1	kazik
Hentchel	Hentchel	k1gMnSc1	Hentchel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stylově	stylově	k6eAd1	stylově
kino	kino	k1gNnSc4	kino
vyzdobil	vyzdobit	k5eAaPmAgMnS	vyzdobit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgMnS	být
navržen	navrhnout	k5eAaPmNgMnS	navrhnout
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1969	[number]	k4	1969
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedostal	dostat	k5eNaPmAgMnS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
pětice	pětice	k1gFnSc2	pětice
nominovaných	nominovaný	k2eAgMnPc2d1	nominovaný
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Menzelovými	Menzelová	k1gFnPc7	Menzelová
Ostře	ostro	k6eAd1	ostro
sledovanými	sledovaný	k2eAgInPc7d1	sledovaný
vlaky	vlak	k1gInPc7	vlak
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c6	na
osmém	osmý	k4xOgNnSc6	osmý
místě	místo	k1gNnSc6	místo
ze	z	k7c2	z
sta	sto	k4xCgNnPc4	sto
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
pořádané	pořádaný	k2eAgFnSc6d1	pořádaná
Projektem	projekt	k1gInSc7	projekt
100	[number]	k4	100
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
sta	sto	k4xCgNnPc4	sto
let	léto	k1gNnPc2	léto
československé	československý	k2eAgFnSc2d1	Československá
kinematografie	kinematografie	k1gFnSc2	kinematografie
(	(	kIx(	(
<g/>
první	první	k4xOgMnSc1	první
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
anketě	anketa	k1gFnSc6	anketa
umístila	umístit	k5eAaPmAgFnS	umístit
Vláčilova	Vláčilův	k2eAgFnSc1d1	Vláčilova
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Asociace	asociace	k1gFnSc2	asociace
českých	český	k2eAgInPc2d1	český
filmových	filmový	k2eAgInPc2d1	filmový
klubů	klub	k1gInPc2	klub
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
NFA	NFA	kA	NFA
Český	český	k2eAgInSc1d1	český
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
1898	[number]	k4	1898
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databas	k1gInSc5	Databas
Spielberg	Spielberg	k1gMnSc1	Spielberg
mi	já	k3xPp1nSc3	já
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
scénu	scéna	k1gFnSc4	scéna
ze	z	k7c2	z
Spalovače	spalovač	k1gMnSc2	spalovač
–	–	k?	–
článek	článek	k1gInSc1	článek
pro	pro	k7c4	pro
Literární	literární	k2eAgFnPc4d1	literární
noviny	novina	k1gFnPc4	novina
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
</s>
