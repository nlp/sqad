<p>
<s>
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Л	Л	k?	Л
Н	Н	k?	Н
Т	Т	k?	Т
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpnajul	srpnajout	k5eAaPmAgInS	srpnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1828	[number]	k4	1828
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
</s>
<s>
Jasná	jasný	k2eAgFnSc1d1	jasná
Poljana	Poljana	k1gFnSc1	Poljana
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadujul	listopadujout	k5eAaPmAgInS	listopadujout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1910	[number]	k4	1910
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
</s>
<s>
Astapovo	Astapův	k2eAgNnSc4d1	Astapovo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
naturální	naturální	k2eAgFnSc7d1	naturální
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
však	však	k9	však
její	její	k3xOp3gInSc1	její
rámec	rámec	k1gInSc1	rámec
přerostl	přerůst	k5eAaPmAgInS	přerůst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
v	v	k7c6	v
Tulské	tulský	k2eAgFnSc6d1	Tulská
gubernii	gubernie	k1gFnSc6	gubernie
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Jasnaja	Jasnajus	k1gMnSc4	Jasnajus
Poljana	Poljan	k1gMnSc4	Poljan
jako	jako	k8xC	jako
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
potomek	potomek	k1gMnSc1	potomek
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
Iljiče	Iljič	k1gInSc2	Iljič
Tolstého	Tolstý	k2eAgInSc2d1	Tolstý
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
patřil	patřit	k5eAaImAgMnS	patřit
do	do	k7c2	do
starého	starý	k2eAgInSc2d1	starý
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
rozená	rozený	k2eAgFnSc1d1	rozená
kněžna	kněžna	k1gFnSc1	kněžna
Volkonská	Volkonský	k2eAgFnSc1d1	Volkonská
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
starší	starý	k2eAgMnPc4d2	starší
bratry	bratr	k1gMnPc4	bratr
Nikolaje	Nikolaj	k1gMnSc4	Nikolaj
(	(	kIx(	(
<g/>
1823	[number]	k4	1823
<g/>
–	–	k?	–
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sergeje	Sergej	k1gMnSc4	Sergej
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dimitrije	Dimitrije	k1gMnSc1	Dimitrije
(	(	kIx(	(
<g/>
1827	[number]	k4	1827
<g/>
–	–	k?	–
<g/>
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
ještě	ještě	k9	ještě
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgInP	být
L.	L.	kA	L.
N.	N.	kA	N.
Tolstému	Tolstý	k2eAgInSc3d1	Tolstý
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
mu	on	k3xPp3gMnSc3	on
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
starší	starý	k2eAgMnSc1d2	starší
syn	syn	k1gMnSc1	syn
mohl	moct	k5eAaImAgMnS	moct
připravit	připravit	k5eAaPmF	připravit
na	na	k7c4	na
přijímací	přijímací	k2eAgFnPc4d1	přijímací
zkoušky	zkouška	k1gFnPc4	zkouška
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Lvu	lev	k1gInSc3	lev
Tolstému	Tolstý	k2eAgInSc3d1	Tolstý
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Osiřelí	osiřelý	k2eAgMnPc1d1	osiřelý
sourozenci	sourozenec	k1gMnPc1	sourozenec
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
Jasné	jasný	k2eAgFnSc6d1	jasná
Poljaně	Poljana	k1gFnSc6	Poljana
u	u	k7c2	u
otcovy	otcův	k2eAgFnSc2d1	otcova
tety	teta	k1gFnSc2	teta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstali	zůstat	k5eAaPmAgMnP	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
teta	teta	k1gFnSc1	teta
zemřela	zemřít	k5eAaPmAgFnS	zemřít
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
přesunuly	přesunout	k5eAaPmAgFnP	přesunout
k	k	k7c3	k
otcově	otcův	k2eAgFnSc3d1	otcova
sestře	sestra	k1gFnSc3	sestra
do	do	k7c2	do
Kazaně	Kazaň	k1gFnSc2	Kazaň
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Kazaňské	kazaňský	k2eAgFnSc6d1	Kazaňská
univerzitě	univerzita	k1gFnSc6	univerzita
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
filologii	filologie	k1gFnSc4	filologie
<g/>
.	.	kIx.	.
</s>
<s>
Učitelé	učitel	k1gMnPc1	učitel
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
říkali	říkat	k5eAaImAgMnP	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
neschopný	schopný	k2eNgMnSc1d1	neschopný
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
je	být	k5eAaImIp3nS	být
neochotný	ochotný	k2eNgMnSc1d1	neochotný
se	se	k3xPyFc4	se
učit	učit	k5eAaImF	učit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
uprostřed	uprostřed	k7c2	uprostřed
svých	svůj	k3xOyFgFnPc2	svůj
studií	studie	k1gFnPc2	studie
do	do	k7c2	do
Jasné	jasný	k2eAgFnSc2d1	jasná
Poljany	Poljana	k1gFnSc2	Poljana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
věnovat	věnovat	k5eAaPmF	věnovat
sebevzdělávání	sebevzdělávání	k1gNnSc4	sebevzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
deníku	deník	k1gInSc2	deník
si	se	k3xPyFc3	se
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
soubor	soubor	k1gInSc1	soubor
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
dvouletý	dvouletý	k2eAgInSc1d1	dvouletý
učební	učební	k2eAgInSc1d1	učební
plán	plán	k1gInSc1	plán
však	však	k9	však
nesplnil	splnit	k5eNaPmAgInS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1848-1849	[number]	k4	1848-1849
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
trávil	trávit	k5eAaImAgMnS	trávit
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
mezi	mezi	k7c7	mezi
aristokratickou	aristokratický	k2eAgFnSc7d1	aristokratická
mládeží	mládež	k1gFnSc7	mládež
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
karetním	karetní	k2eAgFnPc3d1	karetní
hrám	hra	k1gFnPc3	hra
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
těžké	těžký	k2eAgInPc4d1	těžký
dluhy	dluh	k1gInPc4	dluh
z	z	k7c2	z
hazardu	hazard	k1gInSc2	hazard
<g/>
,	,	kIx,	,
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
v	v	k7c6	v
r.	r.	kA	r.
1851	[number]	k4	1851
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
na	na	k7c4	na
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
a	a	k8xC	a
v	v	k7c6	v
r.	r.	kA	r.
1853	[number]	k4	1853
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
ruské	ruský	k2eAgFnSc2d1	ruská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
se	se	k3xPyFc4	se
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaPmF	věnovat
literární	literární	k2eAgFnSc3d1	literární
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1852	[number]	k4	1852
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Sovremennik	Sovremennik	k1gInSc4	Sovremennik
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
próza	próza	k1gFnSc1	próza
Dětství	dětství	k1gNnSc2	dětství
-	-	kIx~	-
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
budoucí	budoucí	k2eAgFnSc2d1	budoucí
autobiografické	autobiografický	k2eAgFnSc2d1	autobiografická
trilogie	trilogie	k1gFnSc2	trilogie
Dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
Chlapectví	chlapectví	k1gNnSc2	chlapectví
<g/>
,	,	kIx,	,
Jinošství	jinošství	k1gNnSc2	jinošství
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
u	u	k7c2	u
čtenářů	čtenář	k1gMnPc2	čtenář
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
příznivě	příznivě	k6eAd1	příznivě
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
i	i	k9	i
odbornou	odborný	k2eAgFnSc7d1	odborná
kritikou	kritika	k1gFnSc7	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
podporučík	podporučík	k1gMnSc1	podporučík
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c4	o
Sevastopol	Sevastopol	k1gInSc4	Sevastopol
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
několik	několik	k4yIc4	několik
vojenských	vojenský	k2eAgNnPc2d1	vojenské
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
za	za	k7c4	za
hrdinství	hrdinství	k1gNnSc4	hrdinství
<g/>
.	.	kIx.	.
</s>
<s>
Dojmy	dojem	k1gInPc4	dojem
a	a	k8xC	a
realistické	realistický	k2eAgInPc4d1	realistický
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
krymské	krymský	k2eAgFnSc2d1	Krymská
války	válka	k1gFnSc2	válka
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
Sevastopolských	sevastopolský	k2eAgFnPc6d1	Sevastopolská
povídkách	povídka	k1gFnPc6	povídka
<g/>
,	,	kIx,	,
vydaných	vydaný	k2eAgInPc2d1	vydaný
v	v	k7c6	v
r.	r.	kA	r.
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Posílily	posílit	k5eAaPmAgFnP	posílit
jeho	jeho	k3xOp3gFnSc4	jeho
prestiž	prestiž	k1gFnSc4	prestiž
jako	jako	k8xC	jako
představitele	představitel	k1gMnPc4	představitel
nastupující	nastupující	k2eAgFnSc2d1	nastupující
generace	generace	k1gFnSc2	generace
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1856	[number]	k4	1856
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
poručíka	poručík	k1gMnSc2	poručík
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
zklamání	zklamání	k1gNnSc1	zklamání
nad	nad	k7c7	nad
poměry	poměr	k1gInPc7	poměr
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
světa	svět	k1gInSc2	svět
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Luzern	Luzerna	k1gFnPc2	Luzerna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
napsal	napsat	k5eAaBmAgInS	napsat
další	další	k2eAgFnPc4d1	další
povídky	povídka	k1gFnPc4	povídka
<g/>
:	:	kIx,	:
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
Trojí	trojit	k5eAaImIp3nS	trojit
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
Rodinné	rodinný	k2eAgNnSc4d1	rodinné
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
cesty	cesta	k1gFnSc2	cesta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
Jasné	jasný	k2eAgFnSc6d1	jasná
Poljaně	Poljana	k1gFnSc6	Poljana
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
si	se	k3xPyFc3	se
zoufalý	zoufalý	k2eAgInSc4d1	zoufalý
stav	stav	k1gInSc4	stav
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
kontrasty	kontrast	k1gInPc4	kontrast
mezi	mezi	k7c7	mezi
životem	život	k1gInSc7	život
bohatých	bohatý	k2eAgMnPc2d1	bohatý
a	a	k8xC	a
chudých	chudý	k2eAgMnPc2d1	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
vyřeší	vyřešit	k5eAaPmIp3nS	vyřešit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
mužikům	mužik	k1gMnPc3	mužik
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Zanechal	zanechat	k5eAaPmAgMnS	zanechat
literární	literární	k2eAgFnSc3d1	literární
činnosti	činnost	k1gFnSc3	činnost
a	a	k8xC	a
otevřel	otevřít	k5eAaPmAgMnS	otevřít
školu	škola	k1gFnSc4	škola
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
mužiky	mužik	k1gMnPc4	mužik
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
sám	sám	k3xTgMnSc1	sám
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1861-1862	[number]	k4	1861-1862
vydával	vydávat	k5eAaImAgInS	vydávat
pedagogický	pedagogický	k2eAgInSc1d1	pedagogický
časopis	časopis	k1gInSc1	časopis
Jasná	jasný	k2eAgFnSc1d1	jasná
Poljana	Poljana	k1gFnSc1	Poljana
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
teoretických	teoretický	k2eAgInPc2d1	teoretický
článků	článek	k1gInPc2	článek
napsal	napsat	k5eAaBmAgInS	napsat
povídky	povídka	k1gFnPc4	povídka
<g/>
,	,	kIx,	,
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
překlady	překlad	k1gInPc4	překlad
přizpůsobené	přizpůsobený	k2eAgInPc4d1	přizpůsobený
frekventantům	frekventant	k1gMnPc3	frekventant
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Mužici	mužik	k1gMnPc1	mužik
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
neviděli	vidět	k5eNaImAgMnP	vidět
žádný	žádný	k3yNgInSc4	žádný
smysl	smysl	k1gInSc4	smysl
a	a	k8xC	a
považovali	považovat	k5eAaImAgMnP	považovat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
přítěž	přítěž	k1gFnSc4	přítěž
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
statkáři	statkář	k1gMnPc1	statkář
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
přestali	přestat	k5eAaPmAgMnP	přestat
stýkat	stýkat	k5eAaImF	stýkat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
v	v	k7c6	v
lepším	dobrý	k2eAgInSc6d2	lepší
případě	případ	k1gInSc6	případ
za	za	k7c4	za
podivína	podivín	k1gMnSc4	podivín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
jej	on	k3xPp3gInSc4	on
velmi	velmi	k6eAd1	velmi
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
smrt	smrt	k1gFnSc1	smrt
milovaného	milovaný	k2eAgMnSc2d1	milovaný
bratra	bratr	k1gMnSc2	bratr
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1862	[number]	k4	1862
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
mladou	mladý	k2eAgFnSc7d1	mladá
šlechtičnou	šlechtična	k1gFnSc7	šlechtična
Sofií	Sofia	k1gFnPc2	Sofia
Andrejevnou	Andrejevný	k2eAgFnSc7d1	Andrejevný
Bersovou	Bersová	k1gFnSc7	Bersová
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodilo	narodit	k5eAaPmAgNnS	narodit
třináct	třináct	k4xCc1	třináct
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
dospělého	dospělý	k2eAgInSc2d1	dospělý
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
dožilo	dožít	k5eAaPmAgNnS	dožít
osm	osm	k4xCc1	osm
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
prožíval	prožívat	k5eAaImAgMnS	prožívat
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
šťastné	šťastný	k2eAgNnSc4d1	šťastné
období	období	k1gNnSc4	období
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
svá	svůj	k3xOyFgNnPc4	svůj
nejlepší	dobrý	k2eAgNnPc4d3	nejlepší
díla	dílo	k1gNnPc4	dílo
<g/>
:	:	kIx,	:
romány	román	k1gInPc1	román
Vojna	vojna	k1gFnSc1	vojna
a	a	k8xC	a
mír	mír	k1gInSc1	mír
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
Karenina	Karenina	k1gFnSc1	Karenina
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
až	až	k6eAd1	až
1872	[number]	k4	1872
strávil	strávit	k5eAaPmAgMnS	strávit
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
nad	nad	k7c7	nad
vznikem	vznik	k1gInSc7	vznik
Slabikáře	slabikář	k1gInSc2	slabikář
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
obnovil	obnovit	k5eAaPmAgInS	obnovit
svoji	svůj	k3xOyFgFnSc4	svůj
praktickou	praktický	k2eAgFnSc4d1	praktická
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
literárním	literární	k2eAgNnSc6d1	literární
i	i	k8xC	i
společenském	společenský	k2eAgNnSc6d1	společenské
vědomí	vědomí	k1gNnSc6	vědomí
<g/>
,	,	kIx,	,
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
jej	on	k3xPp3gNnSc4	on
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
duševní	duševní	k2eAgFnSc1d1	duševní
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
promítla	promítnout	k5eAaPmAgFnS	promítnout
i	i	k9	i
do	do	k7c2	do
rodinných	rodinný	k2eAgInPc2d1	rodinný
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Východisko	východisko	k1gNnSc4	východisko
viděl	vidět	k5eAaImAgMnS	vidět
ve	v	k7c6	v
sblížení	sblížení	k1gNnSc6	sblížení
s	s	k7c7	s
prostým	prostý	k2eAgInSc7d1	prostý
lidem	lid	k1gInSc7	lid
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
rozdělením	rozdělení	k1gNnSc7	rozdělení
svého	svůj	k3xOyFgInSc2	svůj
majetku	majetek	k1gInSc2	majetek
mezi	mezi	k7c7	mezi
mužiky	mužik	k1gMnPc7	mužik
a	a	k8xC	a
sdílením	sdílení	k1gNnSc7	sdílení
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
na	na	k7c4	na
nespravedlivé	spravedlivý	k2eNgNnSc4d1	nespravedlivé
uspořádání	uspořádání	k1gNnSc4	uspořádání
společnosti	společnost	k1gFnSc2	společnost
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
traktátů	traktát	k1gInPc2	traktát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Zpověď	zpověď	k1gFnSc1	zpověď
<g/>
)	)	kIx)	)
,	,	kIx,	,
filozofických	filozofický	k2eAgFnPc2d1	filozofická
úvah	úvaha	k1gFnPc2	úvaha
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
další	další	k2eAgInSc1d1	další
díla	dílo	k1gNnSc2	dílo
s	s	k7c7	s
kritickým	kritický	k2eAgInSc7d1	kritický
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
ruskou	ruský	k2eAgFnSc4d1	ruská
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Dokončil	dokončit	k5eAaPmAgMnS	dokončit
svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
román	román	k1gInSc4	román
Vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slavné	slavný	k2eAgFnSc2d1	slavná
novely	novela	k1gFnSc2	novela
Kreutzerova	Kreutzerův	k2eAgFnSc1d1	Kreutzerova
sonáta	sonáta	k1gFnSc1	sonáta
a	a	k8xC	a
Smrt	smrt	k1gFnSc1	smrt
Ivana	Ivan	k1gMnSc2	Ivan
Iljiče	Iljič	k1gMnSc2	Iljič
<g/>
,	,	kIx,	,
povídku	povídka	k1gFnSc4	povídka
Hadži-Murat	Hadži-Murat	k1gMnSc1	Hadži-Murat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
r.	r.	kA	r.
1905	[number]	k4	1905
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
obžalobu	obžaloba	k1gFnSc4	obžaloba
panování	panování	k1gNnSc2	panování
romanovské	romanovský	k2eAgFnSc2d1	romanovská
dynastie	dynastie	k1gFnSc2	dynastie
ve	v	k7c6	v
stati	stať	k1gFnSc6	stať
Nemohu	moct	k5eNaImIp1nS	moct
mlčet	mlčet	k5eAaImF	mlčet
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
vycestoval	vycestovat	k5eAaPmAgMnS	vycestovat
pozdější	pozdní	k2eAgMnSc1d2	pozdější
první	první	k4xOgMnSc1	první
československý	československý	k2eAgMnSc1d1	československý
prezident	prezident	k1gMnSc1	prezident
Tomáš	Tomáš	k1gMnSc1	Tomáš
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
jej	on	k3xPp3gMnSc4	on
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
Jasné	jasný	k2eAgFnSc6d1	jasná
Poljaně	Poljana	k1gFnSc6	Poljana
také	také	k9	také
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
nazval	nazvat	k5eAaPmAgMnS	nazvat
"	"	kIx"	"
<g/>
úžasně	úžasně	k6eAd1	úžasně
srdečným	srdečný	k2eAgMnSc7d1	srdečný
<g/>
,	,	kIx,	,
milým	milý	k2eAgMnSc7d1	milý
člověkem	člověk	k1gMnSc7	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Car	car	k1gMnSc1	car
ho	on	k3xPp3gMnSc4	on
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
revolucionáře	revolucionář	k1gMnPc4	revolucionář
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
však	však	k9	však
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
života	život	k1gInSc2	život
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
jako	jako	k8xS	jako
kacíř	kacíř	k1gMnSc1	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
odmítání	odmítání	k1gNnSc1	odmítání
dogmatu	dogma	k1gNnSc2	dogma
o	o	k7c6	o
trojjedinosti	trojjedinost	k1gFnSc6	trojjedinost
Boží	boží	k2eAgFnSc1d1	boží
a	a	k8xC	a
ostrá	ostrý	k2eAgFnSc1d1	ostrá
kritika	kritika	k1gFnSc1	kritika
militaristické	militaristický	k2eAgFnSc2d1	militaristická
role	role	k1gFnSc2	role
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
žehnání	žehnání	k1gNnSc1	žehnání
zbraním	zbraň	k1gFnPc3	zbraň
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
válkám	válka	k1gFnPc3	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neshodách	neshoda	k1gFnPc6	neshoda
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
ji	on	k3xPp3gFnSc4	on
opustil	opustit	k5eAaPmAgInS	opustit
a	a	k8xC	a
bez	bez	k7c2	bez
rozloučení	rozloučení	k1gNnSc2	rozloučení
odjel	odjet	k5eAaPmAgMnS	odjet
jen	jen	k9	jen
s	s	k7c7	s
několika	několik	k4yIc7	několik
přáteli	přítel	k1gMnPc7	přítel
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1910	[number]	k4	1910
vlakem	vlak	k1gInSc7	vlak
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
nachladil	nachladit	k5eAaPmAgInS	nachladit
–	–	k?	–
dojel	dojet	k5eAaPmAgMnS	dojet
do	do	k7c2	do
Astapova	Astapův	k2eAgNnSc2d1	Astapovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
deseti	deset	k4xCc6	deset
dnech	den	k1gInPc6	den
zemřel	zemřít	k5eAaPmAgInS	zemřít
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgInPc1d1	politický
názory	názor	k1gInPc1	názor
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
Tolstého	Tolstý	k2eAgInSc2d1	Tolstý
tezí	teze	k1gFnSc7	teze
bylo	být	k5eAaImAgNnS	být
neprotivení	neprotivení	k1gNnSc1	neprotivení
se	se	k3xPyFc4	se
zlu	zlo	k1gNnSc3	zlo
násilím	násilí	k1gNnSc7	násilí
a	a	k8xC	a
celkové	celkový	k2eAgNnSc4d1	celkové
nenásilí	nenásilí	k1gNnSc4	nenásilí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
se	se	k3xPyFc4	se
prolíná	prolínat	k5eAaImIp3nS	prolínat
celým	celý	k2eAgInSc7d1	celý
jeho	jeho	k3xOp3gInSc7	jeho
dílem	díl	k1gInSc7	díl
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
některé	některý	k3yIgMnPc4	některý
pacifisty	pacifista	k1gMnPc4	pacifista
a	a	k8xC	a
odpírače	odpírač	k1gMnPc4	odpírač
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
názorů	názor	k1gInPc2	názor
lze	lze	k6eAd1	lze
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
především	především	k9	především
tyto	tento	k3xDgFnPc4	tento
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Vše	všechen	k3xTgNnSc4	všechen
lze	lze	k6eAd1	lze
řešit	řešit	k5eAaImF	řešit
po	po	k7c6	po
dobrém	dobrý	k2eAgNnSc6d1	dobré
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
žena	žena	k1gFnSc1	žena
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
primárně	primárně	k6eAd1	primárně
starat	starat	k5eAaImF	starat
o	o	k7c4	o
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odmítal	odmítat	k5eAaImAgMnS	odmítat
měšťanské	měšťanský	k2eAgFnPc4d1	měšťanská
konvence	konvence	k1gFnPc4	konvence
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
názorů	názor	k1gInPc2	názor
vycházel	vycházet	k5eAaImAgMnS	vycházet
jak	jak	k6eAd1	jak
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
při	při	k7c6	při
psaní	psaní	k1gNnSc3	psaní
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Význam	význam	k1gInSc1	význam
L.	L.	kA	L.
N.	N.	kA	N.
Tolstého	Tolstý	k2eAgMnSc2d1	Tolstý
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
nejen	nejen	k6eAd1	nejen
ruské	ruský	k2eAgFnSc2d1	ruská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
obrovský	obrovský	k2eAgInSc1d1	obrovský
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
autorů	autor	k1gMnPc2	autor
pokusil	pokusit	k5eAaPmAgMnS	pokusit
ruskou	ruský	k2eAgFnSc4d1	ruská
literaturu	literatura	k1gFnSc4	literatura
přiblížit	přiblížit	k5eAaPmF	přiblížit
západní	západní	k2eAgFnSc4d1	západní
civilizaci	civilizace	k1gFnSc4	civilizace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
*	*	kIx~	*
Slabikář	slabikář	k1gInSc1	slabikář
–	–	k?	–
napsáno	napsat	k5eAaBmNgNnS	napsat
pro	pro	k7c4	pro
základní	základní	k2eAgFnSc4d1	základní
výuku	výuka	k1gFnSc4	výuka
</s>
</p>
<p>
<s>
===	===	k?	===
Romány	román	k1gInPc4	román
===	===	k?	===
</s>
</p>
<p>
<s>
Vojna	vojna	k1gFnSc1	vojna
a	a	k8xC	a
mír	mír	k1gInSc1	mír
–	–	k?	–
popis	popis	k1gInSc1	popis
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
ruského	ruský	k2eAgNnSc2d1	ruské
tažení	tažení	k1gNnSc2	tažení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
rodinu	rodina	k1gFnSc4	rodina
šlechtice	šlechtic	k1gMnSc2	šlechtic
Andreje	Andrej	k1gMnSc2	Andrej
Bolkonského	Bolkonský	k2eAgMnSc2d1	Bolkonský
<g/>
.	.	kIx.	.
</s>
<s>
Bolkonskému	Bolkonský	k2eAgMnSc3d1	Bolkonský
zemře	zemřít	k5eAaPmIp3nS	zemřít
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
proslaví	proslavit	k5eAaPmIp3nS	proslavit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Nebojí	bát	k5eNaImIp3nP	bát
se	se	k3xPyFc4	se
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
nezáleží	záležet	k5eNaImIp3nS	záležet
mu	on	k3xPp3gNnSc3	on
na	na	k7c6	na
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dovolené	dovolená	k1gFnSc6	dovolená
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
mladou	mladý	k2eAgFnSc7d1	mladá
Natašou	Nataša	k1gFnSc7	Nataša
Rostovou	Rostový	k2eAgFnSc7d1	Rostový
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	se	k3xPyFc2	se
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
Andrej	Andrej	k1gMnSc1	Andrej
získá	získat	k5eAaPmIp3nS	získat
znovu	znovu	k6eAd1	znovu
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Pierre	Pierr	k1gInSc5	Pierr
Bezuchov	Bezuchov	k1gInSc4	Bezuchov
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
Tolstého	Tolstý	k2eAgNnSc2d1	Tolstý
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
dění	dění	k1gNnSc1	dění
kolem	kolem	k7c2	kolem
Napoleona	Napoleon	k1gMnSc2	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Bezuchov	Bezuchov	k1gInSc1	Bezuchov
šel	jít	k5eAaImAgInS	jít
do	do	k7c2	do
války	válka	k1gFnSc2	válka
ze	z	k7c2	z
zvědavosti	zvědavost	k1gFnSc2	zvědavost
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zrodí	zrodit	k5eAaPmIp3nP	zrodit
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
násilí	násilí	k1gNnSc3	násilí
a	a	k8xC	a
prochází	procházet	k5eAaImIp3nS	procházet
válkou	válka	k1gFnSc7	válka
jako	jako	k9	jako
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
Napoleona	Napoleon	k1gMnSc2	Napoleon
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
ho	on	k3xPp3gMnSc4	on
zastřelit	zastřelit	k5eAaPmF	zastřelit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neudělá	udělat	k5eNaPmIp3nS	udělat
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dostává	dostávat	k5eAaImIp3nS	dostávat
těžce	těžce	k6eAd1	těžce
raněný	raněný	k2eAgInSc1d1	raněný
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gInSc4	on
ošetřuje	ošetřovat	k5eAaImIp3nS	ošetřovat
Nataša	Nataša	k1gFnSc1	Nataša
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zemře	zemřít	k5eAaPmIp3nS	zemřít
v	v	k7c6	v
náručí	náručí	k1gNnSc6	náručí
<g/>
.	.	kIx.	.
</s>
<s>
Nataša	Nataša	k1gFnSc1	Nataša
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
vdává	vdávat	k5eAaImIp3nS	vdávat
za	za	k7c4	za
Bezuchova	Bezuchův	k2eAgMnSc4d1	Bezuchův
<g/>
.	.	kIx.	.
</s>
<s>
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
odsuzuje	odsuzovat	k5eAaImIp3nS	odsuzovat
války	válka	k1gFnPc1	válka
a	a	k8xC	a
násilí	násilí	k1gNnSc1	násilí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
obranné	obranný	k2eAgNnSc1d1	obranné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
náměty	námět	k1gInPc4	námět
románu	román	k1gInSc2	román
Vojna	vojna	k1gFnSc1	vojna
a	a	k8xC	a
mír	mír	k1gInSc1	mír
složil	složit	k5eAaPmAgInS	složit
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
Sergej	Sergej	k1gMnSc1	Sergej
Prokofjev	Prokofjev	k1gMnSc1	Prokofjev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Karenina	Karenina	k1gFnSc1	Karenina
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vydáno	vydat	k5eAaPmNgNnS	vydat
také	také	k9	také
jako	jako	k9	jako
Anna	Anna	k1gFnSc1	Anna
Kareninová	Kareninový	k2eAgFnSc1d1	Kareninová
<g/>
)	)	kIx)	)
–	–	k?	–
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hrdinkou	hrdinka	k1gFnSc7	hrdinka
je	být	k5eAaImIp3nS	být
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
,	,	kIx,	,
krásná	krásný	k2eAgFnSc1d1	krásná
a	a	k8xC	a
inteligentní	inteligentní	k2eAgFnSc1d1	inteligentní
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgNnSc4d1	dobré
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
nemá	mít	k5eNaImIp3nS	mít
důvod	důvod	k1gInSc4	důvod
si	se	k3xPyFc3	se
stěžovat	stěžovat	k5eAaImF	stěžovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
nespokojená	spokojený	k2eNgFnSc1d1	nespokojená
<g/>
,	,	kIx,	,
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	se	k3xPyFc4	se
s	s	k7c7	s
mladým	mladý	k2eAgMnSc7d1	mladý
důstojníkem	důstojník	k1gMnSc7	důstojník
Vronským	Vronský	k1gMnSc7	Vronský
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
odchází	odcházet	k5eAaImIp3nS	odcházet
od	od	k7c2	od
muže	muž	k1gMnSc2	muž
za	za	k7c7	za
Vronským	Vronský	k1gMnSc7	Vronský
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
jí	on	k3xPp3gFnSc7	on
odmítá	odmítat	k5eAaImIp3nS	odmítat
vydat	vydat	k5eAaPmF	vydat
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
ztrátou	ztráta	k1gFnSc7	ztráta
syna	syn	k1gMnSc2	syn
trpí	trpět	k5eAaImIp3nS	trpět
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
ji	on	k3xPp3gFnSc4	on
postupně	postupně	k6eAd1	postupně
odsoudí	odsoudit	k5eAaPmIp3nS	odsoudit
celá	celý	k2eAgFnSc1d1	celá
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
Vronskij	Vronskij	k1gFnSc4	Vronskij
přestává	přestávat	k5eAaImIp3nS	přestávat
milovat	milovat	k5eAaImF	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
vše	všechen	k3xTgNnSc4	všechen
řeší	řešit	k5eAaImIp3nP	řešit
skokem	skok	k1gInSc7	skok
pod	pod	k7c4	pod
vlak	vlak	k1gInSc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Přičemž	přičemž	k6eAd1	přičemž
už	už	k6eAd1	už
s	s	k7c7	s
Vronským	Vronský	k1gMnSc7	Vronský
měla	mít	k5eAaImAgFnS	mít
dceru	dcera	k1gFnSc4	dcera
Annu	Anna	k1gFnSc4	Anna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
–	–	k?	–
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
jsou	být	k5eAaImIp3nP	být
šlechtic	šlechtic	k1gMnSc1	šlechtic
Něchljudov	Něchljudov	k1gInSc1	Něchljudov
(	(	kIx(	(
<g/>
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
knihy	kniha	k1gFnSc2	kniha
student	student	k1gMnSc1	student
<g/>
)	)	kIx)	)
a	a	k8xC	a
služebná	služebná	k1gFnSc1	služebná
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
prostitutka	prostitutka	k1gFnSc1	prostitutka
Maslovová	Maslovová	k1gFnSc1	Maslovová
<g/>
.	.	kIx.	.
</s>
<s>
Seznámili	seznámit	k5eAaPmAgMnP	seznámit
se	se	k3xPyFc4	se
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
u	u	k7c2	u
Něchljudovových	Něchljudovův	k2eAgFnPc2d1	Něchljudovův
tet	teta	k1gFnPc2	teta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Maslovová	Maslovová	k1gFnSc1	Maslovová
sloužila	sloužit	k5eAaImAgFnS	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
slaví	slavit	k5eAaImIp3nS	slavit
Vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
páně	páně	k2eAgNnSc1d1	páně
<g/>
,	,	kIx,	,
Maslovová	Maslovová	k1gFnSc1	Maslovová
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
Něchljudovovi	Něchljudova	k1gMnSc3	Něchljudova
a	a	k8xC	a
nechtěně	chtěně	k6eNd1	chtěně
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
otěhotní	otěhotnět	k5eAaPmIp3nS	otěhotnět
<g/>
.	.	kIx.	.
</s>
<s>
Těhotnou	těhotná	k1gFnSc4	těhotná
ji	on	k3xPp3gFnSc4	on
tety	teta	k1gFnPc4	teta
vykáží	vykázat	k5eAaPmIp3nP	vykázat
ze	z	k7c2	z
statku	statek	k1gInSc2	statek
a	a	k8xC	a
Maslovová	Maslovová	k1gFnSc1	Maslovová
skončí	skončit	k5eAaPmIp3nS	skončit
jako	jako	k9	jako
prostitutka	prostitutka	k1gFnSc1	prostitutka
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
jí	on	k3xPp3gFnSc3	on
klient	klient	k1gMnSc1	klient
pošle	poslat	k5eAaPmIp3nS	poslat
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
pokoje	pokoj	k1gInSc2	pokoj
pro	pro	k7c4	pro
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Maslovová	Maslovová	k1gFnSc1	Maslovová
si	se	k3xPyFc3	se
jako	jako	k9	jako
svědky	svědek	k1gMnPc4	svědek
bere	brát	k5eAaImIp3nS	brát
pokojskou	pokojská	k1gFnSc4	pokojská
a	a	k8xC	a
portýra	portýr	k1gMnSc4	portýr
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
je	být	k5eAaImIp3nS	být
náročný	náročný	k2eAgMnSc1d1	náročný
a	a	k8xC	a
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
Maslovová	Maslovový	k2eAgFnSc1d1	Maslovový
uspat	uspat	k5eAaPmF	uspat
<g/>
,	,	kIx,	,
portýr	portýr	k1gMnSc1	portýr
ji	on	k3xPp3gFnSc4	on
však	však	k8xC	však
místo	místo	k7c2	místo
uspávacího	uspávací	k2eAgInSc2d1	uspávací
prostředku	prostředek	k1gInSc2	prostředek
podstrčí	podstrčit	k5eAaPmIp3nS	podstrčit
jed	jed	k1gInSc1	jed
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pokojskou	pokojská	k1gFnSc7	pokojská
pak	pak	k6eAd1	pak
klienta	klient	k1gMnSc4	klient
okrade	okrást	k5eAaPmIp3nS	okrást
<g/>
.	.	kIx.	.
</s>
<s>
Maslovová	Maslovová	k1gFnSc1	Maslovová
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
vrazi	vrah	k1gMnPc1	vrah
jsou	být	k5eAaImIp3nP	být
posláni	poslat	k5eAaPmNgMnP	poslat
před	před	k7c4	před
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
porotců	porotce	k1gMnPc2	porotce
zasedá	zasedat	k5eAaImIp3nS	zasedat
i	i	k9	i
Něchljudov	Něchljudov	k1gInSc1	Něchljudov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
let	léto	k1gNnPc2	léto
ze	z	k7c2	z
studenta	student	k1gMnSc2	student
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c4	v
otylého	otylý	k2eAgMnSc4d1	otylý
šlechtice	šlechtic	k1gMnSc4	šlechtic
žijícího	žijící	k2eAgInSc2d1	žijící
bujarým	bujarý	k2eAgInSc7d1	bujarý
nočním	noční	k2eAgInSc7d1	noční
životem	život	k1gInSc7	život
městské	městský	k2eAgFnSc2d1	městská
smetánky	smetánka	k1gFnSc2	smetánka
<g/>
.	.	kIx.	.
</s>
<s>
Něchljudov	Něchljudov	k1gInSc1	Něchljudov
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
rozpozná	rozpoznat	k5eAaPmIp3nS	rozpoznat
Maslovovou	Maslovová	k1gFnSc4	Maslovová
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
jeho	jeho	k3xOp3gNnSc6	jeho
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Nechá	nechat	k5eAaPmIp3nS	nechat
si	se	k3xPyFc3	se
převyprávět	převyprávět	k5eAaPmF	převyprávět
její	její	k3xOp3gInSc4	její
příběh	příběh	k1gInSc4	příběh
a	a	k8xC	a
cítí	cítit	k5eAaImIp3nP	cítit
podíl	podíl	k1gInSc4	podíl
viny	vina	k1gFnSc2	vina
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
osudu	osud	k1gInSc6	osud
<g/>
.	.	kIx.	.
</s>
<s>
Maslovová	Maslovová	k1gFnSc1	Maslovová
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
chybě	chyba	k1gFnSc3	chyba
poroty	porota	k1gFnSc2	porota
odsouzena	odsoudit	k5eAaPmNgFnS	odsoudit
k	k	k7c3	k
nuceným	nucený	k2eAgFnPc3d1	nucená
pracím	práce	k1gFnPc3	práce
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Něchljudov	Něchljudov	k1gInSc1	Něchljudov
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
snaží	snažit	k5eAaImIp3nS	snažit
pomoci	pomoct	k5eAaPmF	pomoct
a	a	k8xC	a
odvrátit	odvrátit	k5eAaPmF	odvrátit
její	její	k3xOp3gInSc4	její
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
odvolání	odvolání	k1gNnSc4	odvolání
k	k	k7c3	k
senátu	senát	k1gInSc3	senát
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
je	být	k5eAaImIp3nS	být
neúspěšné	úspěšný	k2eNgNnSc4d1	neúspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestování	cestování	k1gNnSc6	cestování
po	po	k7c6	po
věznicích	věznice	k1gFnPc6	věznice
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
Maslovová	Maslovový	k2eAgFnSc1d1	Maslovový
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zhnusen	zhnusen	k2eAgInSc1d1	zhnusen
přístupem	přístup	k1gInSc7	přístup
státu	stát	k1gInSc2	stát
k	k	k7c3	k
prostým	prostý	k2eAgMnPc3d1	prostý
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
osloven	osloven	k2eAgMnSc1d1	osloven
jistým	jistý	k2eAgMnSc7d1	jistý
anglickým	anglický	k2eAgMnSc7d1	anglický
filozofem	filozof	k1gMnSc7	filozof
odmítajícím	odmítající	k2eAgFnPc3d1	odmítající
soukromé	soukromý	k2eAgFnPc1d1	soukromá
vlastnícví	vlastnícví	k1gNnSc4	vlastnícví
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
mužikům	mužik	k1gMnPc3	mužik
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
statcích	statek	k1gInPc6	statek
předat	předat	k5eAaPmF	předat
půdu	půda	k1gFnSc4	půda
do	do	k7c2	do
jejich	jejich	k3xOp3gNnSc2	jejich
užívání	užívání	k1gNnSc2	užívání
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
vlivných	vlivný	k2eAgMnPc2d1	vlivný
známých	známý	k1gMnPc2	známý
přimlouvá	přimlouvat	k5eAaImIp3nS	přimlouvat
za	za	k7c4	za
Maslovovou	Maslovová	k1gFnSc4	Maslovová
a	a	k8xC	a
posílá	posílat	k5eAaImIp3nS	posílat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
carovi	car	k1gMnSc3	car
<g/>
.	.	kIx.	.
</s>
<s>
Maslovové	Maslovové	k2eAgInSc1d1	Maslovové
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
sňatek	sňatek	k1gInSc1	sňatek
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
ho	on	k3xPp3gMnSc4	on
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hrdá	hrdý	k2eAgFnSc1d1	hrdá
<g/>
,	,	kIx,	,
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
však	však	k9	však
Něchljudova	Něchljudův	k2eAgNnSc2d1	Něchljudův
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
Něchljudov	Něchljudov	k1gInSc1	Něchljudov
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
odradit	odradit	k5eAaPmF	odradit
a	a	k8xC	a
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Zařídí	zařídit	k5eAaPmIp3nS	zařídit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
přeřazena	přeřadit	k5eAaPmNgFnS	přeřadit
mezi	mezi	k7c4	mezi
politické	politický	k2eAgMnPc4d1	politický
vězně	vězeň	k1gMnPc4	vězeň
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgFnPc7	který
je	být	k5eAaImIp3nS	být
lépe	dobře	k6eAd2	dobře
zacházeno	zacházen	k2eAgNnSc1d1	zacházeno
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
morálnější	morální	k2eAgNnSc4d2	morálnější
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Maslovová	Maslovová	k1gFnSc1	Maslovová
se	se	k3xPyFc4	se
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
revolucionáři	revolucionář	k1gMnPc7	revolucionář
a	a	k8xC	a
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
vyslyší	vyslyšet	k5eAaPmIp3nS	vyslyšet
dovolání	dovolání	k1gNnSc4	dovolání
(	(	kIx(	(
<g/>
i	i	k9	i
díky	díky	k7c3	díky
Něchljudovovým	Něchljudovův	k2eAgFnPc3d1	Něchljudovův
známým	známá	k1gFnPc3	známá
<g/>
)	)	kIx)	)
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
trest	trest	k1gInSc4	trest
z	z	k7c2	z
nucených	nucený	k2eAgFnPc2d1	nucená
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
.	.	kIx.	.
<g/>
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
v	v	k7c6	v
románu	román	k1gInSc6	román
velmi	velmi	k6eAd1	velmi
obrazně	obrazně	k6eAd1	obrazně
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
všednost	všednost	k1gFnSc1	všednost
a	a	k8xC	a
beznaděj	beznaděj	k1gFnSc1	beznaděj
chudiny	chudina	k1gFnSc2	chudina
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
neskutečné	skutečný	k2eNgInPc4d1	neskutečný
sociální	sociální	k2eAgInPc4d1	sociální
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
,	,	kIx,	,
kontrast	kontrast	k1gInSc4	kontrast
přepychu	přepych	k1gInSc2	přepych
a	a	k8xC	a
hýření	hýření	k1gNnSc2	hýření
s	s	k7c7	s
bojem	boj	k1gInSc7	boj
o	o	k7c4	o
život	život	k1gInSc4	život
spodiny	spodina	k1gFnSc2	spodina
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
proti	proti	k7c3	proti
pokrytectví	pokrytectví	k1gNnSc3	pokrytectví
v	v	k7c6	v
pravoslavné	pravoslavný	k2eAgFnSc6d1	pravoslavná
církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
popisován	popisován	k2eAgInSc1d1	popisován
ruský	ruský	k2eAgInSc1d1	ruský
vězeňský	vězeňský	k2eAgInSc1d1	vězeňský
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yRgNnSc6	který
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
obyčejného	obyčejný	k2eAgMnSc4d1	obyčejný
člověka	člověk	k1gMnSc4	člověk
brán	brán	k2eAgInSc1d1	brán
ohled	ohled	k1gInSc1	ohled
a	a	k8xC	a
v	v	k7c6	v
kterém	který	k3yRgInSc6	který
pozbývá	pozbývat	k5eAaImIp3nS	pozbývat
vězeň	vězeň	k1gMnSc1	vězeň
veškerých	veškerý	k3xTgNnPc2	veškerý
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
státní	státní	k2eAgFnSc4d1	státní
byrokracii	byrokracie	k1gFnSc4	byrokracie
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
morální	morální	k2eAgNnSc4d1	morální
právo	právo	k1gNnSc4	právo
věznit	věznit	k5eAaImF	věznit
jiného	jiný	k2eAgMnSc4d1	jiný
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
sami	sám	k3xTgMnPc1	sám
nejsou	být	k5eNaImIp3nP	být
bez	bez	k7c2	bez
viny	vina	k1gFnSc2	vina
<g/>
.	.	kIx.	.
</s>
<s>
Něchljudov	Něchljudov	k1gInSc1	Něchljudov
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
vězněna	věznit	k5eAaImNgFnS	věznit
neprávem	neprávo	k1gNnSc7	neprávo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gInSc1	jejich
vývoj	vývoj	k1gInSc1	vývoj
byl	být	k5eAaImAgInS	být
determinován	determinovat	k5eAaBmNgInS	determinovat
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jim	on	k3xPp3gMnPc3	on
nedalo	dát	k5eNaPmAgNnS	dát
jinou	jiný	k2eAgFnSc4d1	jiná
šanci	šance	k1gFnSc4	šance
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
náboženský	náboženský	k2eAgInSc4d1	náboženský
akcent	akcent	k1gInSc4	akcent
bylo	být	k5eAaImAgNnS	být
jakožto	jakožto	k8xS	jakožto
"	"	kIx"	"
<g/>
antiburžoazní	antiburžoazní	k2eAgInSc4d1	antiburžoazní
<g/>
"	"	kIx"	"
román	román	k1gInSc4	román
Vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
i	i	k9	i
v	v	k7c6	v
komunistické	komunistický	k2eAgFnSc6d1	komunistická
ČSSR	ČSSR	kA	ČSSR
vydáváno	vydávat	k5eAaImNgNnS	vydávat
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
hrál	hrát	k5eAaImAgMnS	hrát
i	i	k9	i
obdiv	obdiv	k1gInSc4	obdiv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
k	k	k7c3	k
Tolstému	Tolstý	k2eAgInSc3d1	Tolstý
celoživotně	celoživotně	k6eAd1	celoživotně
choval	chovat	k5eAaImAgMnS	chovat
Vladimir	Vladimir	k1gMnSc1	Vladimir
Iljič	Iljič	k1gMnSc1	Iljič
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
.	.	kIx.	.
<g/>
Ďábel	ďábel	k1gMnSc1	ďábel
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
Ivana	Ivan	k1gMnSc2	Ivan
Iljiče	Iljič	k1gMnSc2	Iljič
–	–	k?	–
Úředník	úředník	k1gMnSc1	úředník
Ivan	Ivan	k1gMnSc1	Ivan
Iljič	Iljič	k1gMnSc1	Iljič
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Bolesti	bolest	k1gFnPc1	bolest
se	se	k3xPyFc4	se
horší	zlý	k2eAgMnSc1d2	horší
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
Iljič	Iljič	k1gMnSc1	Iljič
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
zatrpklejším	zatrpklý	k2eAgInSc7d2	zatrpklý
a	a	k8xC	a
nenávistnějším	návistný	k2eNgInSc7d2	návistný
<g/>
.	.	kIx.	.
</s>
<s>
Nerozumí	rozumět	k5eNaImIp3nS	rozumět
proč	proč	k6eAd1	proč
ho	on	k3xPp3gInSc4	on
osud	osud	k1gInSc4	osud
trestá	trestat	k5eAaImIp3nS	trestat
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
až	až	k9	až
na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
posteli	postel	k1gFnSc6	postel
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
bolestech	bolest	k1gFnPc6	bolest
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nežil	žít	k5eNaImAgMnS	žít
opravdově	opravdově	k6eAd1	opravdově
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obyčejně	obyčejně	k6eAd1	obyčejně
poživačně	poživačně	k6eAd1	poživačně
<g/>
.	.	kIx.	.
</s>
<s>
Špatně	špatně	k6eAd1	špatně
žil	žít	k5eAaImAgMnS	žít
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
špatně	špatně	k6eAd1	špatně
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
hodinách	hodina	k1gFnPc6	hodina
odpouští	odpouštět	k5eAaImIp3nS	odpouštět
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
se	se	k3xPyFc4	se
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kreutzerova	Kreutzerův	k2eAgFnSc1d1	Kreutzerova
sonáta	sonáta	k1gFnSc1	sonáta
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
tmy	tma	k1gFnSc2	tma
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
-	-	kIx~	-
Realistické	realistický	k2eAgNnSc1d1	realistické
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
pojednávající	pojednávající	k2eAgNnSc1d1	pojednávající
o	o	k7c6	o
morálním	morální	k2eAgInSc6d1	morální
úpadku	úpadek	k1gInSc6	úpadek
ruského	ruský	k2eAgInSc2d1	ruský
venkova	venkov	k1gInSc2	venkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živá	živý	k2eAgFnSc1d1	živá
voda	voda	k1gFnSc1	voda
</s>
</p>
<p>
<s>
Otec	otec	k1gMnSc1	otec
Sergej	Sergej	k1gMnSc1	Sergej
</s>
</p>
<p>
<s>
Nemohu	moct	k5eNaImIp1nS	moct
mlčetNovelyKozáci	mlčetNovelyKozák	k1gMnPc1	mlčetNovelyKozák
</s>
</p>
<p>
<s>
Hadži-Murat	Hadži-Murat	k1gInSc1	Hadži-Murat
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
-	-	kIx~	-
obávaný	obávaný	k2eAgMnSc1d1	obávaný
protivník	protivník	k1gMnSc1	protivník
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
Hadži-Murat	Hadži-Murat	k1gInSc1	Hadži-Murat
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
vydá	vydat	k5eAaPmIp3nS	vydat
Rusům	Rus	k1gMnPc3	Rus
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
bude	být	k5eAaImBp3nS	být
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
je	být	k5eAaImIp3nS	být
vylíčen	vylíčen	k2eAgInSc4d1	vylíčen
celý	celý	k2eAgInSc4d1	celý
jeho	jeho	k3xOp3gInSc4	jeho
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
jedinou	jediný	k2eAgFnSc4d1	jediná
podmínku	podmínka	k1gFnSc4	podmínka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
začal	začít	k5eAaPmAgInS	začít
pro	pro	k7c4	pro
Rusy	Rus	k1gMnPc4	Rus
pracovat	pracovat	k5eAaImF	pracovat
-	-	kIx~	-
zachránit	zachránit	k5eAaPmF	zachránit
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
držena	držet	k5eAaImNgFnS	držet
jako	jako	k8xC	jako
rukojmí	rukojmí	k1gMnPc1	rukojmí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
nic	nic	k3yNnSc1	nic
neděje	dít	k5eNaImIp3nS	dít
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
řešit	řešit	k5eAaImF	řešit
situaci	situace	k1gFnSc4	situace
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
-	-	kIx~	-
a	a	k8xC	a
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kavkazský	kavkazský	k2eAgMnSc1d1	kavkazský
zajatec	zajatec	k1gMnSc1	zajatec
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
-	-	kIx~	-
dva	dva	k4xCgMnPc1	dva
Rusové	Rus	k1gMnPc1	Rus
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
zajatci	zajatec	k1gMnPc1	zajatec
Čečenů	Čečen	k1gInPc2	Čečen
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
chtěli	chtít	k5eAaImAgMnP	chtít
získat	získat	k5eAaPmF	získat
výkupné	výkupné	k1gNnSc4	výkupné
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
útěk	útěk	k1gInSc4	útěk
se	se	k3xPyFc4	se
nezdařil	zdařit	k5eNaPmAgMnS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
útěku	útěk	k1gInSc3	útěk
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
napomohla	napomoct	k5eAaPmAgFnS	napomoct
čečenská	čečenský	k2eAgFnSc1d1	čečenská
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Útěk	útěk	k1gInSc1	útěk
se	se	k3xPyFc4	se
zdařil	zdařit	k5eAaPmAgInS	zdařit
a	a	k8xC	a
za	za	k7c4	za
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vykoupit	vykoupit	k5eAaPmF	vykoupit
i	i	k9	i
druhého	druhý	k4xOgMnSc4	druhý
zajatce	zajatec	k1gMnSc4	zajatec
(	(	kIx(	(
<g/>
zfilmováno	zfilmován	k2eAgNnSc4d1	zfilmováno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válečná	válečný	k2eAgNnPc1d1	válečné
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Vpád	vpád	k1gInSc1	vpád
</s>
</p>
<p>
<s>
Kácení	kácení	k1gNnSc1	kácení
lesa	les	k1gInSc2	les
</s>
</p>
<p>
<s>
Sevastopolské	sevastopolský	k2eAgFnPc1d1	Sevastopolská
povídky	povídka	k1gFnPc1	povídka
</s>
</p>
<p>
<s>
===	===	k?	===
Dramata	drama	k1gNnPc4	drama
===	===	k?	===
</s>
</p>
<p>
<s>
Plody	plod	k1gInPc1	plod
osvěty	osvěta	k1gFnSc2	osvěta
</s>
</p>
<p>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
povídky	povídka	k1gFnPc1	povídka
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
tmy	tma	k1gFnSc2	tma
–	–	k?	–
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
naturalismem	naturalismus	k1gInSc7	naturalismus
</s>
</p>
<p>
<s>
Živá	živý	k2eAgFnSc1d1	živá
mrtvola	mrtvola	k1gFnSc1	mrtvola
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnPc1d1	další
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
Kolik	kolika	k1gFnPc2	kolika
země	zem	k1gFnSc2	zem
člověk	člověk	k1gMnSc1	člověk
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
ji	on	k3xPp3gFnSc4	on
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
povídek	povídka	k1gFnPc2	povídka
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
trilogie	trilogie	k1gFnSc1	trilogie
</s>
</p>
<p>
<s>
Dětství	dětství	k1gNnSc1	dětství
</s>
</p>
<p>
<s>
Chlapectví	chlapectví	k1gNnSc1	chlapectví
</s>
</p>
<p>
<s>
Jinošství	jinošství	k1gNnSc1	jinošství
–	–	k?	–
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Dětstvím	dětství	k1gNnSc7	dětství
a	a	k8xC	a
Chlapectvím	chlapectví	k1gNnSc7	chlapectví
tvoří	tvořit	k5eAaImIp3nS	tvořit
trilogii	trilogie	k1gFnSc4	trilogie
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
malém	malý	k1gMnSc6	malý
Nikolajovi	Nikolaj	k1gMnSc6	Nikolaj
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
vztah	vztah	k1gInSc1	vztah
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
záhy	záhy	k6eAd1	záhy
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
výchovy	výchova	k1gFnSc2	výchova
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
ujímá	ujímat	k5eAaImIp3nS	ujímat
babička	babička	k1gFnSc1	babička
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bratrů	bratr	k1gMnPc2	bratr
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
sobci	sobec	k1gMnPc1	sobec
<g/>
.	.	kIx.	.
</s>
<s>
Nikolajův	Nikolajův	k2eAgMnSc1d1	Nikolajův
bratr	bratr	k1gMnSc1	bratr
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
universitu	universita	k1gFnSc4	universita
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
také	také	k9	také
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
propadá	propadat	k5eAaPmIp3nS	propadat
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
trilogii	trilogie	k1gFnSc4	trilogie
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
autobiografickou	autobiografický	k2eAgFnSc4d1	autobiografická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
trilogie	trilogie	k1gFnSc1	trilogie
</s>
</p>
<p>
<s>
Markérovy	markérův	k2eAgInPc1d1	markérův
zápisky	zápisek	k1gInPc1	zápisek
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
</s>
</p>
<p>
<s>
Statkářovo	statkářův	k2eAgNnSc1d1	statkářův
jitro	jitro	k1gNnSc1	jitro
–	–	k?	–
s	s	k7c7	s
Albertem	Albert	k1gMnSc7	Albert
a	a	k8xC	a
Markérovými	markérův	k2eAgInPc7d1	markérův
zápisky	zápisek	k1gInPc7	zápisek
tvoří	tvořit	k5eAaImIp3nS	tvořit
trilogii	trilogie	k1gFnSc4	trilogie
</s>
</p>
<p>
<s>
Vzpamatujte	vzpamatovat	k5eAaPmRp2nP	vzpamatovat
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
o	o	k7c6	o
Ivanu	Ivan	k1gMnSc6	Ivan
Hlupákovi	hlupák	k1gMnSc6	hlupák
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
bratřích	bratr	k1gMnPc6	bratr
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
Asarchadon	Asarchadon	k1gMnSc1	Asarchadon
</s>
</p>
<p>
<s>
O	o	k7c6	o
bratru	bratr	k1gMnSc6	bratr
Palečkovi	Paleček	k1gMnSc6	Paleček
</s>
</p>
<p>
<s>
Zpověď	zpověď	k1gFnSc1	zpověď
–	–	k?	–
soubor	soubor	k1gInSc4	soubor
traktátů	traktát	k1gInPc2	traktát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
objasňuje	objasňovat	k5eAaImIp3nS	objasňovat
svoje	svůj	k3xOyFgFnPc4	svůj
myšlenky	myšlenka	k1gFnPc4	myšlenka
</s>
</p>
<p>
<s>
Co	co	k3yQnSc4	co
máme	mít	k5eAaImIp1nP	mít
kdy	kdy	k6eAd1	kdy
dělati	dělat	k5eAaImF	dělat
</s>
</p>
<p>
<s>
Království	království	k1gNnSc1	království
Boží	božit	k5eAaImIp3nS	božit
ve	v	k7c4	v
vás	vy	k3xPp2nPc4	vy
</s>
</p>
<p>
<s>
Sevastopolské	sevastopolský	k2eAgFnPc1d1	Sevastopolská
povídky	povídka	k1gFnPc1	povídka
</s>
</p>
<p>
<s>
Evangelium	evangelium	k1gNnSc1	evangelium
ve	v	k7c6	v
stručnosti	stručnost	k1gFnSc6	stručnost
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
násilí	násilí	k1gNnSc2	násilí
a	a	k8xC	a
Zákon	zákon	k1gInSc1	zákon
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
ALMI	ALMI	kA	ALMI
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
98	[number]	k4	98
S.	S.	kA	S.
Překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Kraupner	Kraupner	k1gMnSc1	Kraupner
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
L.	L.	kA	L.
N.	N.	kA	N.
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Karenina	Karenina	k1gFnSc1	Karenina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rozmluvy	rozmluva	k1gFnSc2	rozmluva
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
L.	L.	kA	L.
N.	N.	kA	N.
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Dvě	dva	k4xCgFnPc1	dva
smrti	smrt	k1gFnPc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Havran	Havran	k1gMnSc1	Havran
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
L.	L.	kA	L.
N.	N.	kA	N.
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgInPc1d1	Malé
romány	román	k1gInPc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	svoboda	k1gFnSc1	svoboda
1990	[number]	k4	1990
</s>
</p>
<p>
<s>
L.	L.	kA	L.
N.	N.	kA	N.
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Nemohu	moct	k5eNaImIp1nS	moct
mlčet	mlčet	k5eAaImF	mlčet
–	–	k?	–
jasnopoljanské	jasnopoljanský	k2eAgFnPc4d1	jasnopoljanský
epištoly	epištola	k1gFnPc4	epištola
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1990	[number]	k4	1990
</s>
</p>
<p>
<s>
L.	L.	kA	L.
N.	N.	kA	N.
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Tři	tři	k4xCgFnPc1	tři
podoby	podoba	k1gFnPc1	podoba
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
L.	L.	kA	L.
N.	N.	kA	N.
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Vláda	vláda	k1gFnSc1	vláda
tmy	tma	k1gFnSc2	tma
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
dramata	drama	k1gNnPc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
L.	L.	kA	L.
N.	N.	kA	N.
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Vojna	vojna	k1gFnSc1	vojna
a	a	k8xC	a
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
L.	L.	kA	L.
N.	N.	kA	N.
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Vzkříšení	vzkříšený	k2eAgMnPc1d1	vzkříšený
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ikar	Ikar	k1gInSc1	Ikar
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
T.	T.	kA	T.
A.	A.	kA	A.
Kuzminská	Kuzminský	k2eAgFnSc1d1	Kuzminský
<g/>
,	,	kIx,	,
Mé	můj	k3xOp1gInPc4	můj
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
L.	L.	kA	L.
N.	N.	kA	N.
Tolstého	Tolstý	k2eAgInSc2d1	Tolstý
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Kallinikov	Kallinikov	k1gInSc1	Kallinikov
<g/>
,	,	kIx,	,
Lev	Lev	k1gMnSc1	Lev
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
Tragedie	tragedie	k1gFnSc2	tragedie
sexuální	sexuální	k2eAgFnSc4d1	sexuální
<g/>
,	,	kIx,	,
Symposion	symposion	k1gNnSc4	symposion
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Velemínský	Velemínský	k2eAgMnSc1d1	Velemínský
–	–	k?	–
U	u	k7c2	u
Tolstého	Tolstý	k2eAgNnSc2d1	Tolstý
<g/>
.	.	kIx.	.
</s>
<s>
Nákladem	náklad	k1gInSc7	náklad
Tiskařského	tiskařský	k2eAgNnSc2d1	tiskařské
a	a	k8xC	a
nakladatelského	nakladatelský	k2eAgNnSc2d1	nakladatelské
družstva	družstvo	k1gNnSc2	družstvo
"	"	kIx"	"
<g/>
Pokrok	pokrok	k1gInSc1	pokrok
<g/>
"	"	kIx"	"
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1908	[number]	k4	1908
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RYČLOVÁ	RYČLOVÁ	kA	RYČLOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
Dilema	dilema	k1gNnSc1	dilema
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
CDK	CDK	kA	CDK
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
63	[number]	k4	63
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Konec	konec	k1gInSc1	konec
mýtu	mýtus	k1gInSc2	mýtus
o	o	k7c6	o
Lvu	lev	k1gInSc6	lev
Tolstém	Tolstý	k2eAgInSc6d1	Tolstý
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
s.	s.	k?	s.
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOLSTOJ	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
<g/>
,	,	kIx,	,
Vláda	vláda	k1gFnSc1	vláda
tmy	tma	k1gFnSc2	tma
<g/>
,	,	kIx,	,
Hospodář	hospodář	k1gMnSc1	hospodář
a	a	k8xC	a
čeledín	čeledín	k1gMnSc1	čeledín
<g/>
,	,	kIx,	,
O	o	k7c6	o
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1895	[number]	k4	1895
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
355	[number]	k4	355
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOLSTOJ	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
<g/>
.	.	kIx.	.
</s>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
výklad	výklad	k1gInSc1	výklad
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Fr.	Fr.	k1gMnSc1	Fr.
Bačkovský	Bačkovský	k2eAgMnSc1d1	Bačkovský
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1895	[number]	k4	1895
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
175	[number]	k4	175
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ruských	ruský	k2eAgMnPc2d1	ruský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lev	lev	k1gInSc1	lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
<g/>
Heslo	heslo	k1gNnSc4	heslo
'	'	kIx"	'
<g/>
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
'	'	kIx"	'
na	na	k7c6	na
Lib	Liba	k1gFnPc2	Liba
<g/>
.	.	kIx.	.
<g/>
ru	ru	k?	ru
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
L.	L.	kA	L.
N.	N.	kA	N.
Tolstého	Tolstý	k2eAgNnSc2d1	Tolstý
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
</s>
</p>
<p>
<s>
Genealogie	genealogie	k1gFnPc1	genealogie
L.	L.	kA	L.
N.	N.	kA	N.
Tolstého	Tolstý	k2eAgNnSc2d1	Tolstý
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Ilustrovaný	ilustrovaný	k2eAgInSc1d1	ilustrovaný
životopis	životopis	k1gInSc1	životopis
–	–	k?	–
Univ	Univ	k1gInSc1	Univ
<g/>
.	.	kIx.	.
of	of	k?	of
Virginia	Virginium	k1gNnSc2	Virginium
</s>
</p>
<p>
<s>
Tolstoy	Tolstoa	k1gFnPc1	Tolstoa
and	and	k?	and
Popular	Popular	k1gMnSc1	Popular
Literature	Literatur	k1gMnSc5	Literatur
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
prací	práce	k1gFnPc2	práce
University	universita	k1gFnSc2	universita
of	of	k?	of
Minnesota	Minnesota	k1gFnSc1	Minnesota
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc4	životopis
a	a	k8xC	a
stránky	stránka	k1gFnPc4	stránka
o	o	k7c6	o
Anně	Anna	k1gFnSc6	Anna
Karenině	Karenin	k2eAgFnSc6d1	Karenina
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
dny	den	k1gInPc1	den
L.	L.	kA	L.
N.	N.	kA	N.
Tolstého	Tolstý	k2eAgMnSc4d1	Tolstý
</s>
</p>
<p>
<s>
Aleksandra	Aleksandra	k1gFnSc1	Aleksandra
Tolstaja	Tolstaja	k1gFnSc1	Tolstaja
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tragedy	Trageda	k1gMnSc2	Trageda
of	of	k?	of
Tolstoy	Tolstoa	k1gMnSc2	Tolstoa
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
E.	E.	kA	E.
Lindgren	Lindgrna	k1gFnPc2	Lindgrna
<g/>
,	,	kIx,	,
Leo	Leo	k1gMnSc1	Leo
Tolstoy	Tolstoa	k1gFnSc2	Tolstoa
Chronicle	Chronicle	k1gFnSc2	Chronicle
</s>
</p>
<p>
<s>
G.	G.	kA	G.
Orwell	Orwell	k1gMnSc1	Orwell
<g/>
,	,	kIx,	,
Lear	Lear	k1gMnSc1	Lear
<g/>
,	,	kIx,	,
Tolstoy	Tolstoa	k1gFnPc1	Tolstoa
and	and	k?	and
the	the	k?	the
Fool	Fool	k1gInSc1	Fool
</s>
</p>
<p>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
i	i	k8xC	i
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
</s>
</p>
