<s>
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
v	v	k7c6	v
Tulské	tulský	k2eAgFnSc6d1	Tulská
gubernii	gubernie	k1gFnSc6	gubernie
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Jasnaja	Jasnajus	k1gMnSc4	Jasnajus
Poljana	Poljan	k1gMnSc4	Poljan
jako	jako	k8xC	jako
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
potomek	potomek	k1gMnSc1	potomek
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
Iljiče	Iljič	k1gInSc2	Iljič
Tolstého	Tolstý	k2eAgInSc2d1	Tolstý
<g/>
.	.	kIx.	.
</s>
