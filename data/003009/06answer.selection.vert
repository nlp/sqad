<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
vědních	vědní	k2eAgInPc2d1	vědní
oborů	obor	k1gInPc2	obor
se	se	k3xPyFc4	se
v	v	k7c6	v
anatomii	anatomie	k1gFnSc6	anatomie
důsledně	důsledně	k6eAd1	důsledně
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
terminologie	terminologie	k1gFnSc1	terminologie
(	(	kIx(	(
<g/>
odborné	odborný	k2eAgNnSc1d1	odborné
názvosloví	názvosloví	k1gNnSc1	názvosloví
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
rámci	rámec	k1gInSc6	rámec
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
neměnná	měnný	k2eNgFnSc1d1	neměnná
anatomická	anatomický	k2eAgFnSc1d1	anatomická
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
v	v	k7c6	v
latinském	latinský	k2eAgInSc6d1	latinský
jazyce	jazyk	k1gInSc6	jazyk
je	být	k5eAaImIp3nS	být
závazná	závazný	k2eAgNnPc1d1	závazné
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
