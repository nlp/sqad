<s>
Anatomie	anatomie	k1gFnSc1	anatomie
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
biologie	biologie	k1gFnSc2	biologie
nebo	nebo	k8xC	nebo
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
makroskopickou	makroskopický	k2eAgFnSc7d1	makroskopická
a	a	k8xC	a
mikroskopickou	mikroskopický	k2eAgFnSc7d1	mikroskopická
stavbou	stavba	k1gFnSc7	stavba
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
anatomii	anatomie	k1gFnSc4	anatomie
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
anatomii	anatomie	k1gFnSc4	anatomie
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samostatný	samostatný	k2eAgInSc1d1	samostatný
název	název	k1gInSc1	název
anatomie	anatomie	k1gFnSc2	anatomie
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
synonymum	synonymum	k1gNnSc1	synonymum
anatomie	anatomie	k1gFnSc2	anatomie
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Slovo	slovo	k1gNnSc1	slovo
anatomie	anatomie	k1gFnSc2	anatomie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
anatemnō	anatemnō	k?	anatemnō
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
rozříznout	rozříznout	k5eAaPmF	rozříznout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
struktur	struktura	k1gFnPc2	struktura
organismů	organismus	k1gInPc2	organismus
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gFnPc2	jejich
soustav	soustava	k1gFnPc2	soustava
<g/>
,	,	kIx,	,
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
umístění	umístění	k1gNnSc4	umístění
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
složení	složení	k1gNnSc4	složení
a	a	k8xC	a
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
orgány	orgán	k1gInPc7	orgán
či	či	k8xC	či
orgánovými	orgánový	k2eAgFnPc7d1	orgánová
soustavami	soustava	k1gFnPc7	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
se	se	k3xPyFc4	se
od	od	k7c2	od
fyziologie	fyziologie	k1gFnSc2	fyziologie
a	a	k8xC	a
biochemie	biochemie	k1gFnSc2	biochemie
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
disciplíny	disciplína	k1gFnPc1	disciplína
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
zejména	zejména	k9	zejména
funkcemi	funkce	k1gFnPc7	funkce
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
chemickými	chemický	k2eAgInPc7d1	chemický
procesy	proces	k1gInPc7	proces
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
tyto	tento	k3xDgFnPc4	tento
části	část	k1gFnPc1	část
procházejí	procházet	k5eAaImIp3nP	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Anatom	anatom	k1gMnSc1	anatom
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c4	o
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
umístění	umístění	k1gNnSc4	umístění
<g/>
,	,	kIx,	,
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
přísun	přísun	k1gInSc4	přísun
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
inervaci	inervace	k1gFnSc4	inervace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
fyziolog	fyziolog	k1gMnSc1	fyziolog
se	se	k3xPyFc4	se
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c4	o
produkci	produkce	k1gFnSc4	produkce
žluči	žluč	k1gFnSc2	žluč
<g/>
,	,	kIx,	,
roli	role	k1gFnSc4	role
jater	játra	k1gNnPc2	játra
ve	v	k7c6	v
výživě	výživa	k1gFnSc6	výživa
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
o	o	k7c4	o
regulaci	regulace	k1gFnSc4	regulace
tělesných	tělesný	k2eAgFnPc2d1	tělesná
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
určitého	určitý	k2eAgNnSc2d1	určité
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
anatomie	anatomie	k1gFnSc1	anatomie
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
s	s	k7c7	s
embryologií	embryologie	k1gFnSc7	embryologie
a	a	k8xC	a
komparativní	komparativní	k2eAgFnSc7d1	komparativní
anatomií	anatomie	k1gFnSc7	anatomie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
evoluční	evoluční	k2eAgFnSc3d1	evoluční
biologii	biologie	k1gFnSc3	biologie
a	a	k8xC	a
fylogenezi	fylogeneze	k1gFnSc3	fylogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
medicínských	medicínský	k2eAgFnPc2d1	medicínská
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
vědních	vědní	k2eAgInPc2d1	vědní
oborů	obor	k1gInPc2	obor
se	se	k3xPyFc4	se
v	v	k7c6	v
anatomii	anatomie	k1gFnSc6	anatomie
důsledně	důsledně	k6eAd1	důsledně
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
terminologie	terminologie	k1gFnSc1	terminologie
(	(	kIx(	(
<g/>
odborné	odborný	k2eAgNnSc1d1	odborné
názvosloví	názvosloví	k1gNnSc1	názvosloví
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
rámci	rámec	k1gInSc6	rámec
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
neměnná	měnný	k2eNgFnSc1d1	neměnná
anatomická	anatomický	k2eAgFnSc1d1	anatomická
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
v	v	k7c6	v
latinském	latinský	k2eAgInSc6d1	latinský
jazyce	jazyk	k1gInSc6	jazyk
je	být	k5eAaImIp3nS	být
závazná	závazný	k2eAgNnPc1d1	závazné
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Anatomii	anatomie	k1gFnSc4	anatomie
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
na	na	k7c4	na
makroskopickou	makroskopický	k2eAgFnSc4d1	makroskopická
a	a	k8xC	a
mikroskopickou	mikroskopický	k2eAgFnSc4d1	mikroskopická
<g/>
.	.	kIx.	.
</s>
<s>
Makroskopická	makroskopický	k2eAgFnSc1d1	makroskopická
anatomie	anatomie	k1gFnSc1	anatomie
<g/>
,	,	kIx,	,
též	též	k9	též
nazývána	nazýván	k2eAgFnSc1d1	nazývána
topografická	topografický	k2eAgFnSc1d1	topografická
anatomie	anatomie	k1gFnSc1	anatomie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
zkoumáním	zkoumání	k1gNnSc7	zkoumání
částí	část	k1gFnPc2	část
těl	tělo	k1gNnPc2	tělo
živočichů	živočich	k1gMnPc2	živočich
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Topografická	topografický	k2eAgFnSc1d1	topografická
anatomie	anatomie	k1gFnSc1	anatomie
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
anatomii	anatomie	k1gFnSc4	anatomie
<g/>
.	.	kIx.	.
</s>
<s>
Mikroskopická	mikroskopický	k2eAgFnSc1d1	mikroskopická
anatomie	anatomie	k1gFnSc1	anatomie
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
výzkumu	výzkum	k1gInSc2	výzkum
optické	optický	k2eAgInPc4d1	optický
přístroje	přístroj	k1gInPc4	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Mikroskopická	mikroskopický	k2eAgFnSc1d1	mikroskopická
anatomie	anatomie	k1gFnSc1	anatomie
studuje	studovat	k5eAaImIp3nS	studovat
tkáně	tkáň	k1gFnPc4	tkáň
různých	různý	k2eAgFnPc2d1	různá
struktur	struktura	k1gFnPc2	struktura
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
věda	věda	k1gFnSc1	věda
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
histologie	histologie	k1gFnSc1	histologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
i	i	k9	i
stavbou	stavba	k1gFnSc7	stavba
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
medicíny	medicína	k1gFnSc2	medicína
z	z	k7c2	z
obecného	obecný	k2eAgNnSc2d1	obecné
hlediska	hledisko	k1gNnSc2	hledisko
a	a	k8xC	a
morfologických	morfologický	k2eAgInPc2d1	morfologický
oborů	obor	k1gInPc2	obor
zvláště	zvláště	k6eAd1	zvláště
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
anatomie	anatomie	k1gFnSc2	anatomie
úzce	úzko	k6eAd1	úzko
spjaty	spjat	k2eAgFnPc1d1	spjata
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
pitvy	pitva	k1gFnPc1	pitva
byly	být	k5eAaImAgFnP	být
prováděny	provádět	k5eAaImNgFnP	provádět
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
přípravy	příprava	k1gFnSc2	příprava
těl	tělo	k1gNnPc2	tělo
zemřelých	zemřelý	k1gMnPc2	zemřelý
pro	pro	k7c4	pro
mumifikaci	mumifikace	k1gFnSc4	mumifikace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zároveň	zároveň	k6eAd1	zároveň
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
anatomii	anatomie	k1gFnSc6	anatomie
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
lékař	lékař	k1gMnSc1	lékař
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgMnSc1d1	označován
Imhotep	Imhotep	k1gMnSc1	Imhotep
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
době	doba	k1gFnSc6	doba
faraóna	faraón	k1gMnSc2	faraón
Džosera	Džoser	k1gMnSc2	Džoser
z	z	k7c2	z
III	III	kA	III
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2600	[number]	k4	2600
př.n.l.	př.n.l.	k?	př.n.l.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
anatomie	anatomie	k1gFnSc2	anatomie
jako	jako	k8xC	jako
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
starověcí	starověký	k2eAgMnPc1d1	starověký
lékaři	lékař	k1gMnPc1	lékař
Herophilos	Herophilos	k1gMnSc1	Herophilos
a	a	k8xC	a
Erasistrakos	Erasistrakos	k1gMnSc1	Erasistrakos
<g/>
,	,	kIx,	,
představitelé	představitel	k1gMnPc1	představitel
alexandrijské	alexandrijský	k2eAgFnSc2d1	Alexandrijská
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
270	[number]	k4	270
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
kromě	kromě	k7c2	kromě
mrtvol	mrtvola	k1gFnPc2	mrtvola
pitvali	pitvat	k5eAaImAgMnP	pitvat
údajně	údajně	k6eAd1	údajně
i	i	k8xC	i
odsouzence	odsouzenec	k1gMnPc4	odsouzenec
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
alexandrijskou	alexandrijský	k2eAgFnSc4d1	Alexandrijská
školu	škola	k1gFnSc4	škola
přímo	přímo	k6eAd1	přímo
navázal	navázat	k5eAaPmAgInS	navázat
Galén	Galén	k1gInSc1	Galén
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
prováděl	provádět	k5eAaImAgMnS	provádět
hlavně	hlavně	k6eAd1	hlavně
pitvy	pitva	k1gFnPc4	pitva
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Galén	Galén	k1gInSc1	Galén
shrnul	shrnout	k5eAaPmAgInS	shrnout
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
anatomické	anatomický	k2eAgFnPc4d1	anatomická
znalosti	znalost	k1gFnPc4	znalost
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
autorita	autorita	k1gFnSc1	autorita
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
dominovala	dominovat	k5eAaImAgFnS	dominovat
medicíně	medicína	k1gFnSc3	medicína
až	až	k9	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
anatomické	anatomický	k2eAgInPc4d1	anatomický
poznatky	poznatek	k1gInPc4	poznatek
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
ztraceny	ztratit	k5eAaPmNgInP	ztratit
<g/>
,	,	kIx,	,
zachovaly	zachovat	k5eAaPmAgInP	zachovat
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
latiny	latina	k1gFnSc2	latina
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1235	[number]	k4	1235
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
Salernu	Salerna	k1gFnSc4	Salerna
založena	založen	k2eAgFnSc1d1	založena
lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
středověká	středověký	k2eAgFnSc1d1	středověká
pitva	pitva	k1gFnSc1	pitva
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
roku	rok	k1gInSc2	rok
1286	[number]	k4	1286
v	v	k7c6	v
Cremoně	Cremona	k1gFnSc6	Cremona
<g/>
.	.	kIx.	.
</s>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
i	i	k9	i
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Mondino	Mondin	k2eAgNnSc4d1	Mondino
de	de	k?	de
<g/>
'	'	kIx"	'
<g/>
Liuzzi	Liuzze	k1gFnSc4	Liuzze
organizoval	organizovat	k5eAaBmAgMnS	organizovat
veřejné	veřejný	k2eAgFnSc2d1	veřejná
pitvy	pitva	k1gFnSc2	pitva
<g/>
,	,	kIx,	,
de	de	k?	de
<g/>
'	'	kIx"	'
<g/>
Liuzzi	Liuzh	k1gMnPc1	Liuzh
též	též	k9	též
roku	rok	k1gInSc2	rok
1316	[number]	k4	1316
sepsal	sepsat	k5eAaPmAgMnS	sepsat
učebnici	učebnice	k1gFnSc4	učebnice
"	"	kIx"	"
<g/>
Anatomia	Anatomia	k1gFnSc1	Anatomia
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pitvou	pitva	k1gFnSc7	pitva
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
také	také	k9	také
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnPc7	Vinca
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1543	[number]	k4	1543
vychází	vycházet	k5eAaImIp3nS	vycházet
De	De	k?	De
Humani	Humaň	k1gFnSc3	Humaň
Corporis	Corporis	k1gFnPc2	Corporis
Fabrica	Fabricum	k1gNnSc2	Fabricum
<g/>
,	,	kIx,	,
ilustrovaná	ilustrovaný	k2eAgFnSc1d1	ilustrovaná
učebnice	učebnice	k1gFnSc1	učebnice
anatomie	anatomie	k1gFnSc2	anatomie
italského	italský	k2eAgMnSc2d1	italský
lékaře	lékař	k1gMnSc2	lékař
Andrea	Andrea	k1gFnSc1	Andrea
Vesalia	Vesalia	k1gFnSc1	Vesalia
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc4	základ
moderní	moderní	k2eAgFnSc2d1	moderní
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
pitvu	pitva	k1gFnSc4	pitva
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
Jan	Jan	k1gMnSc1	Jan
Jessenius	Jessenius	k1gMnSc1	Jessenius
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
racionalismu	racionalismus	k1gInSc2	racionalismus
a	a	k8xC	a
vynálezem	vynález	k1gInSc7	vynález
mikroskopu	mikroskop	k1gInSc2	mikroskop
se	se	k3xPyFc4	se
anatomie	anatomie	k1gFnSc1	anatomie
začala	začít	k5eAaPmAgFnS	začít
rychle	rychle	k6eAd1	rychle
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgInPc1	první
anatomické	anatomický	k2eAgInPc1d1	anatomický
preparáty	preparát	k1gInPc1	preparát
a	a	k8xC	a
muzea	muzeum	k1gNnPc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
centrum	centrum	k1gNnSc1	centrum
anatomického	anatomický	k2eAgNnSc2d1	anatomické
bádání	bádání	k1gNnSc2	bádání
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
výzkumu	výzkum	k1gInSc2	výzkum
anatomie	anatomie	k1gFnSc1	anatomie
je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
stále	stále	k6eAd1	stále
hlubším	hluboký	k2eAgNnSc7d2	hlubší
porozuměním	porozumění	k1gNnSc7	porozumění
funkcí	funkce	k1gFnPc2	funkce
orgánů	orgán	k1gMnPc2	orgán
a	a	k8xC	a
struktur	struktura	k1gFnPc2	struktura
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
zkoumání	zkoumání	k1gNnSc2	zkoumání
se	se	k3xPyFc4	se
také	také	k9	také
značně	značně	k6eAd1	značně
zdokonalily	zdokonalit	k5eAaPmAgFnP	zdokonalit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
zkoumání	zkoumání	k1gNnSc2	zkoumání
anatomie	anatomie	k1gFnSc2	anatomie
živočichů	živočich	k1gMnPc2	živočich
pomocí	pomocí	k7c2	pomocí
anatomických	anatomický	k2eAgFnPc2d1	anatomická
pitev	pitva	k1gFnPc2	pitva
uhynulých	uhynulý	k2eAgNnPc2d1	uhynulé
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
mrtvých	mrtvý	k1gMnPc2	mrtvý
lidských	lidský	k2eAgNnPc2d1	lidské
těl	tělo	k1gNnPc2	tělo
se	se	k3xPyFc4	se
metody	metoda	k1gFnPc1	metoda
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přesunuly	přesunout	k5eAaPmAgInP	přesunout
k	k	k7c3	k
vyspělým	vyspělý	k2eAgFnPc3d1	vyspělá
zobrazovacím	zobrazovací	k2eAgFnPc3d1	zobrazovací
metodám	metoda	k1gFnPc3	metoda
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
rentgen	rentgen	k1gInSc4	rentgen
<g/>
,	,	kIx,	,
ultrazvukové	ultrazvukový	k2eAgFnPc1d1	ultrazvuková
vlny	vlna	k1gFnPc1	vlna
a	a	k8xC	a
magnetická	magnetický	k2eAgFnSc1d1	magnetická
rezonance	rezonance	k1gFnSc1	rezonance
<g/>
.	.	kIx.	.
</s>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
a	a	k8xC	a
fyziologie	fyziologie	k1gFnSc1	fyziologie
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
funkci	funkce	k1gFnSc4	funkce
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
dvojici	dvojice	k1gFnSc4	dvojice
vzájemně	vzájemně	k6eAd1	vzájemně
propojených	propojený	k2eAgFnPc2d1	propojená
vědních	vědní	k2eAgFnPc2d1	vědní
disciplín	disciplína	k1gFnPc2	disciplína
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tudíž	tudíž	k8xC	tudíž
většinou	většinou	k6eAd1	většinou
studovány	studovat	k5eAaImNgInP	studovat
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Obor	obor	k1gInSc4	obor
anatomie	anatomie	k1gFnSc2	anatomie
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
množství	množství	k1gNnSc2	množství
podoborů	podobor	k1gInPc2	podobor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
makroskopickou	makroskopický	k2eAgFnSc4d1	makroskopická
a	a	k8xC	a
mikroskopickou	mikroskopický	k2eAgFnSc4d1	mikroskopická
anatomii	anatomie	k1gFnSc4	anatomie
<g/>
.	.	kIx.	.
</s>
<s>
Makroskopická	makroskopický	k2eAgFnSc1d1	makroskopická
anatomie	anatomie	k1gFnSc1	anatomie
studuje	studovat	k5eAaImIp3nS	studovat
struktury	struktura	k1gFnSc2	struktura
viditelné	viditelný	k2eAgFnSc2d1	viditelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Mikroskopická	mikroskopický	k2eAgFnSc1d1	mikroskopická
anatomie	anatomie	k1gFnSc1	anatomie
studuje	studovat	k5eAaImIp3nS	studovat
struktury	struktura	k1gFnPc4	struktura
na	na	k7c6	na
mikroskopické	mikroskopický	k2eAgFnSc6d1	mikroskopická
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
histologii	histologie	k1gFnSc4	histologie
(	(	kIx(	(
<g/>
studium	studium	k1gNnSc4	studium
tkání	tkáň	k1gFnPc2	tkáň
<g/>
)	)	kIx)	)
a	a	k8xC	a
embryologii	embryologie	k1gFnSc4	embryologie
(	(	kIx(	(
<g/>
studium	studium	k1gNnSc4	studium
organismů	organismus	k1gInPc2	organismus
v	v	k7c6	v
nevyvinutém	vyvinutý	k2eNgNnSc6d1	nevyvinuté
stádiu	stádium	k1gNnSc6	stádium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
těla	tělo	k1gNnSc2	tělo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zkoumána	zkoumat	k5eAaImNgFnS	zkoumat
jak	jak	k8xC	jak
pomocí	pomocí	k7c2	pomocí
invazivních	invazivní	k2eAgNnPc2d1	invazivní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
neinvazivních	invazivní	k2eNgFnPc2d1	neinvazivní
vyšetřovacích	vyšetřovací	k2eAgFnPc2d1	vyšetřovací
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
studie	studie	k1gFnSc2	studie
je	být	k5eAaImIp3nS	být
získání	získání	k1gNnSc1	získání
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc6	uspořádání
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
orgánových	orgánový	k2eAgFnPc2d1	orgánová
soustav	soustava	k1gFnPc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
zkoumání	zkoumání	k1gNnSc2	zkoumání
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
pitvu	pitva	k1gFnSc4	pitva
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
tělo	tělo	k1gNnSc4	tělo
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
a	a	k8xC	a
studují	studovat	k5eAaImIp3nP	studovat
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
a	a	k8xC	a
endoskopii	endoskopie	k1gFnSc4	endoskopie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zákrok	zákrok	k1gInSc4	zákrok
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
endoskop	endoskop	k1gInSc1	endoskop
(	(	kIx(	(
<g/>
nástroj	nástroj	k1gInSc1	nástroj
vybavený	vybavený	k2eAgInSc1d1	vybavený
kamerou	kamera	k1gFnSc7	kamera
<g/>
)	)	kIx)	)
zavede	zavést	k5eAaPmIp3nS	zavést
do	do	k7c2	do
malého	malý	k2eAgInSc2d1	malý
otvoru	otvor	k1gInSc2	otvor
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
umožní	umožnit	k5eAaPmIp3nS	umožnit
tak	tak	k6eAd1	tak
zkoumat	zkoumat	k5eAaImF	zkoumat
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
orgány	orgán	k1gInPc4	orgán
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
struktury	struktura	k1gFnPc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zobrazení	zobrazení	k1gNnSc3	zobrazení
krevního	krevní	k2eAgNnSc2d1	krevní
řečiště	řečiště	k1gNnSc2	řečiště
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
angiografie	angiografie	k1gFnSc1	angiografie
(	(	kIx(	(
<g/>
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
metoda	metoda	k1gFnSc1	metoda
užívající	užívající	k2eAgFnSc1d1	užívající
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
či	či	k8xC	či
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
rezonanci	rezonance	k1gFnSc4	rezonance
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
anatomie	anatomie	k1gFnSc2	anatomie
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
užívá	užívat	k5eAaImIp3nS	užívat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
anatomii	anatomie	k1gFnSc3	anatomie
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
stejných	stejný	k2eAgFnPc2d1	stejná
či	či	k8xC	či
podobných	podobný	k2eAgFnPc2d1	podobná
struktur	struktura	k1gFnPc2	struktura
a	a	k8xC	a
tkání	tkáň	k1gFnPc2	tkáň
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
živočišné	živočišný	k2eAgFnSc6d1	živočišná
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
pojem	pojem	k1gInSc1	pojem
anatomie	anatomie	k1gFnSc2	anatomie
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
též	též	k6eAd1	též
anatomii	anatomie	k1gFnSc4	anatomie
jiných	jiný	k2eAgMnPc2d1	jiný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
označení	označení	k1gNnSc3	označení
vědy	věda	k1gFnSc2	věda
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
anatomií	anatomie	k1gFnSc7	anatomie
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
užívá	užívat	k5eAaImIp3nS	užívat
slovo	slovo	k1gNnSc4	slovo
zootomie	zootomie	k1gFnSc2	zootomie
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
a	a	k8xC	a
tkáně	tkáň	k1gFnPc1	tkáň
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
povahy	povaha	k1gFnPc1	povaha
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
jsou	být	k5eAaImIp3nP	být
studovány	studovat	k5eAaImNgInP	studovat
v	v	k7c6	v
anatomii	anatomie	k1gFnSc6	anatomie
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Živočišná	živočišný	k2eAgFnSc1d1	živočišná
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
Metazoa	Metazoa	k1gFnSc1	Metazoa
<g/>
)	)	kIx)	)
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
heterotrofní	heterotrofní	k2eAgInPc4d1	heterotrofní
motilní	motilní	k2eAgInPc4d1	motilní
(	(	kIx(	(
<g/>
pohyblivé	pohyblivý	k2eAgInPc4d1	pohyblivý
<g/>
)	)	kIx)	)
mnohobuněčné	mnohobuněčný	k2eAgInPc4d1	mnohobuněčný
organismy	organismus	k1gInPc4	organismus
(	(	kIx(	(
<g/>
některé	některý	k3yIgNnSc1	některý
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
evoluce	evoluce	k1gFnSc2	evoluce
adoptovaly	adoptovat	k5eAaPmAgInP	adoptovat
sesilní	sesilní	k2eAgInPc1d1	sesilní
-	-	kIx~	-
přisedlý	přisedlý	k2eAgInSc1d1	přisedlý
-	-	kIx~	-
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
živočichů	živočich	k1gMnPc2	živočich
má	mít	k5eAaImIp3nS	mít
těla	tělo	k1gNnSc2	tělo
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Takoví	takový	k3xDgMnPc1	takový
živočichové	živočich	k1gMnPc1	živočich
jsou	být	k5eAaImIp3nP	být
označováni	označován	k2eAgMnPc1d1	označován
jako	jako	k8xS	jako
eumetazoa	eumetazoa	k6eAd1	eumetazoa
(	(	kIx(	(
<g/>
praví	pravit	k5eAaImIp3nS	pravit
mnohobuněční	mnohobuněční	k2eAgMnPc1d1	mnohobuněční
živočichové	živočich	k1gMnPc1	živočich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
trávicí	trávicí	k2eAgFnSc4d1	trávicí
dutinu	dutina	k1gFnSc4	dutina
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgInPc7	dva
otvory	otvor	k1gInPc7	otvor
<g/>
,	,	kIx,	,
pohlavní	pohlavní	k2eAgFnPc4d1	pohlavní
buňky	buňka	k1gFnPc4	buňka
(	(	kIx(	(
<g/>
gamety	gameta	k1gFnPc4	gameta
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
produkovány	produkovat	k5eAaImNgFnP	produkovat
v	v	k7c6	v
mnohobuněčných	mnohobuněčný	k2eAgInPc6d1	mnohobuněčný
pohlavních	pohlavní	k2eAgInPc6d1	pohlavní
orgánech	orgán	k1gInPc6	orgán
a	a	k8xC	a
v	v	k7c6	v
embryonálním	embryonální	k2eAgInSc6d1	embryonální
vývoji	vývoj	k1gInSc6	vývoj
zygoty	zygota	k1gFnSc2	zygota
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
stádium	stádium	k1gNnSc4	stádium
blastuly	blastula	k1gFnSc2	blastula
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
říše	říš	k1gFnSc2	říš
metazoa	metazoa	k6eAd1	metazoa
nejsou	být	k5eNaImIp3nP	být
zahrnuty	zahrnut	k2eAgFnPc4d1	zahrnuta
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
nediferencované	diferencovaný	k2eNgFnPc4d1	nediferencovaná
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
rostlinným	rostlinný	k2eAgFnPc3d1	rostlinná
buňkám	buňka	k1gFnPc3	buňka
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
živočišné	živočišný	k2eAgFnPc1d1	živočišná
buňky	buňka	k1gFnPc1	buňka
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
stěnu	stěna	k1gFnSc4	stěna
ani	ani	k8xC	ani
chloroplasty	chloroplast	k1gInPc4	chloroplast
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
živočišné	živočišný	k2eAgFnSc6d1	živočišná
buňce	buňka	k1gFnSc6	buňka
přítomny	přítomen	k2eAgFnPc1d1	přítomna
vakuoly	vakuola	k1gFnPc1	vakuola
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gInPc2	on
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
vakuoly	vakuola	k1gFnPc1	vakuola
v	v	k7c6	v
rostlinných	rostlinný	k2eAgFnPc6d1	rostlinná
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Tělní	tělní	k2eAgFnPc1d1	tělní
tkáně	tkáň	k1gFnPc1	tkáň
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
svalovými	svalový	k2eAgFnPc7d1	svalová
<g/>
,	,	kIx,	,
nervovými	nervový	k2eAgFnPc7d1	nervová
či	či	k8xC	či
kožními	kožní	k2eAgFnPc7d1	kožní
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
buňka	buňka	k1gFnSc1	buňka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
buněčné	buněčný	k2eAgFnSc2d1	buněčná
membrány	membrána	k1gFnSc2	membrána
tvořené	tvořený	k2eAgFnSc2d1	tvořená
fosfolipidy	fosfolipida	k1gFnSc2	fosfolipida
<g/>
,	,	kIx,	,
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
a	a	k8xC	a
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduší	jednoduchý	k2eAgMnPc1d1	jednoduchý
bezobratlí	bezobratlý	k2eAgMnPc1d1	bezobratlý
živočichové	živočich	k1gMnPc1	živočich
složení	složení	k1gNnSc2	složení
z	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
zárodečných	zárodečný	k2eAgInPc2d1	zárodečný
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
entodermu	entoderm	k1gInSc2	entoderm
a	a	k8xC	a
ektodermu	ektoderm	k1gInSc2	ektoderm
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
dvojlistí	dvojlistý	k2eAgMnPc1d1	dvojlistý
(	(	kIx(	(
<g/>
láčkovci	láčkovec	k1gMnPc1	láčkovec
<g/>
,	,	kIx,	,
Diblastica	Diblastica	k1gMnSc1	Diblastica
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyvinutější	vyvinutý	k2eAgMnPc1d2	vyvinutější
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
orgány	orgán	k1gInPc1	orgán
a	a	k8xC	a
tkáně	tkáň	k1gFnPc1	tkáň
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
třemi	tři	k4xCgInPc7	tři
zárodečnými	zárodečný	k2eAgInPc7d1	zárodečný
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
trojlistí	trojlistý	k2eAgMnPc1d1	trojlistý
(	(	kIx(	(
<g/>
Triblastica	Triblastica	k1gMnSc1	Triblastica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tkáně	tkáň	k1gFnPc1	tkáň
a	a	k8xC	a
orgány	orgán	k1gInPc1	orgán
živočichů	živočich	k1gMnPc2	živočich
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
trojlistých	trojlistý	k2eAgFnPc2d1	trojlistá
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
třemi	tři	k4xCgNnPc7	tři
vrstvami	vrstva	k1gFnPc7	vrstva
zárodečných	zárodečný	k2eAgInPc2d1	zárodečný
listů	list	k1gInPc2	list
-	-	kIx~	-
ektodermu	ektoderm	k1gInSc2	ektoderm
<g/>
,	,	kIx,	,
mezodermu	mezoderm	k1gInSc2	mezoderm
a	a	k8xC	a
entodermu	entoderm	k1gInSc2	entoderm
<g/>
.	.	kIx.	.
</s>
<s>
Živočišné	živočišný	k2eAgFnPc1d1	živočišná
tkáně	tkáň	k1gFnPc1	tkáň
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
pojivová	pojivový	k2eAgFnSc1d1	pojivová
<g/>
,	,	kIx,	,
epitelová	epitelový	k2eAgFnSc1d1	epitelová
<g/>
,	,	kIx,	,
svalová	svalový	k2eAgFnSc1d1	svalová
a	a	k8xC	a
nervová	nervový	k2eAgFnSc1d1	nervová
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Pojivové	pojivový	k2eAgFnPc1d1	pojivová
tkáně	tkáň	k1gFnPc1	tkáň
jsou	být	k5eAaImIp3nP	být
vláknité	vláknitý	k2eAgFnPc1d1	vláknitá
tkáně	tkáň	k1gFnPc1	tkáň
tvořené	tvořený	k2eAgFnPc1d1	tvořená
buňkami	buňka	k1gFnPc7	buňka
rozptýlenými	rozptýlený	k2eAgFnPc7d1	rozptýlená
v	v	k7c6	v
anorganickém	anorganický	k2eAgInSc6d1	anorganický
materiálu	materiál	k1gInSc6	materiál
zvaném	zvaný	k2eAgInSc6d1	zvaný
extracelulární	extracelulární	k2eAgFnSc1d1	extracelulární
matrix	matrix	k1gInSc1	matrix
(	(	kIx(	(
<g/>
či	či	k8xC	či
mezibuněčná	mezibuněčný	k2eAgFnSc1d1	mezibuněčná
hmota	hmota	k1gFnSc1	hmota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojivová	pojivový	k2eAgFnSc1d1	pojivová
tkáň	tkáň	k1gFnSc1	tkáň
určuje	určovat	k5eAaImIp3nS	určovat
tvar	tvar	k1gInSc4	tvar
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
je	on	k3xPp3gMnPc4	on
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
typy	typ	k1gInPc7	typ
pojivové	pojivový	k2eAgFnSc2d1	pojivová
tkáně	tkáň	k1gFnSc2	tkáň
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
řídké	řídký	k2eAgNnSc1d1	řídké
vazivo	vazivo	k1gNnSc1	vazivo
<g/>
,	,	kIx,	,
tuková	tukový	k2eAgFnSc1d1	tuková
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
,	,	kIx,	,
vazivová	vazivový	k2eAgFnSc1d1	vazivová
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
,	,	kIx,	,
chrupavka	chrupavka	k1gFnSc1	chrupavka
a	a	k8xC	a
kost	kost	k1gFnSc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Extracelulární	Extracelulární	k2eAgInSc1d1	Extracelulární
matrix	matrix	k1gInSc1	matrix
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
proteiny	protein	k1gInPc7	protein
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
kolagen	kolagen	k1gInSc1	kolagen
<g/>
.	.	kIx.	.
</s>
<s>
Kolagen	kolagen	k1gInSc1	kolagen
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
organizování	organizování	k1gNnSc6	organizování
a	a	k8xC	a
podpoře	podpora	k1gFnSc3	podpora
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Matrix	Matrix	k1gInSc1	Matrix
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přeměněn	přeměnit	k5eAaPmNgInS	přeměnit
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
kostry	kostra	k1gFnSc2	kostra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
vnější	vnější	k2eAgFnSc1d1	vnější
kostra	kostra	k1gFnSc1	kostra
(	(	kIx(	(
<g/>
exoskelet	exoskelet	k1gInSc1	exoskelet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
kostra	kostra	k1gFnSc1	kostra
(	(	kIx(	(
<g/>
endoskelet	endoskelet	k1gInSc1	endoskelet
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
opěrná	opěrný	k2eAgFnSc1d1	opěrná
soustava	soustava	k1gFnSc1	soustava
jak	jak	k8xC	jak
u	u	k7c2	u
vyšších	vysoký	k2eAgInPc2d2	vyšší
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
nižších	nízký	k2eAgMnPc2d2	nižší
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Epitelová	epitelový	k2eAgFnSc1d1	epitelová
tkáň	tkáň	k1gFnSc1	tkáň
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
těsně	těsně	k6eAd1	těsně
přiléhajícími	přiléhající	k2eAgFnPc7d1	přiléhající
buňkami	buňka	k1gFnPc7	buňka
spojenými	spojený	k2eAgFnPc7d1	spojená
pomocí	pomocí	k7c2	pomocí
adhezních	adhezní	k2eAgFnPc2d1	adhezní
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc4d1	malý
mezibuněčný	mezibuněčný	k2eAgInSc4d1	mezibuněčný
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Epiteliální	epiteliální	k2eAgFnPc4d1	epiteliální
buňky	buňka	k1gFnPc4	buňka
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
na	na	k7c6	na
ploché	plochý	k2eAgFnSc6d1	plochá
<g/>
,	,	kIx,	,
kubické	kubický	k2eAgFnSc6d1	kubická
a	a	k8xC	a
cylindrické	cylindrický	k2eAgFnSc6d1	cylindrická
<g/>
.	.	kIx.	.
</s>
<s>
Nasedají	nasedat	k5eAaImIp3nP	nasedat
na	na	k7c4	na
bazální	bazální	k2eAgFnSc4d1	bazální
membránu	membrána	k1gFnSc4	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Epitelová	epitelový	k2eAgFnSc1d1	epitelová
tkáň	tkáň	k1gFnSc1	tkáň
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
rozdělena	rozdělen	k2eAgFnSc1d1	rozdělena
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dýchacím	dýchací	k2eAgInSc6d1	dýchací
traktu	trakt	k1gInSc6	trakt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řasnatý	řasnatý	k2eAgInSc1d1	řasnatý
epitel	epitel	k1gInSc1	epitel
a	a	k8xC	a
v	v	k7c6	v
tenkém	tenký	k2eAgNnSc6d1	tenké
střevě	střevo	k1gNnSc6	střevo
mikroklky	mikroklka	k1gFnSc2	mikroklka
<g/>
.	.	kIx.	.
</s>
<s>
Pokožka	pokožka	k1gFnSc1	pokožka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
vrstvy	vrstva	k1gFnSc2	vrstva
zrohovatělého	zrohovatělý	k2eAgInSc2d1	zrohovatělý
epitelu	epitel	k1gInSc2	epitel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Keratinocyty	Keratinocyt	k1gInPc1	Keratinocyt
(	(	kIx(	(
<g/>
pokožkové	pokožkový	k2eAgFnPc1d1	pokožková
buňky	buňka	k1gFnPc1	buňka
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
až	až	k9	až
95	[number]	k4	95
%	%	kIx~	%
kožních	kožní	k2eAgFnPc2d1	kožní
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Epiteliální	epiteliální	k2eAgFnPc1d1	epiteliální
buňky	buňka	k1gFnPc1	buňka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
těla	tělo	k1gNnSc2	tělo
obvykle	obvykle	k6eAd1	obvykle
produkují	produkovat	k5eAaImIp3nP	produkovat
extracelulární	extracelulární	k2eAgInSc4d1	extracelulární
matrix	matrix	k1gInSc4	matrix
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
kutikuly	kutikula	k1gFnSc2	kutikula
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vyšších	vysoký	k2eAgMnPc2d2	vyšší
živočichů	živočich	k1gMnPc2	živočich
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
epiteliálních	epiteliální	k2eAgFnPc2d1	epiteliální
buněk	buňka	k1gFnPc2	buňka
tvořeny	tvořit	k5eAaImNgFnP	tvořit
mnohé	mnohý	k2eAgFnPc1d1	mnohá
žlázy	žláza	k1gFnPc1	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Svalové	svalový	k2eAgFnPc1d1	svalová
buňky	buňka	k1gFnPc1	buňka
(	(	kIx(	(
<g/>
myocyty	myocyt	k1gInPc1	myocyt
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
aktivní	aktivní	k2eAgFnSc4d1	aktivní
stažlivou	stažlivý	k2eAgFnSc4d1	stažlivý
tkáň	tkáň	k1gFnSc4	tkáň
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Svalová	svalový	k2eAgFnSc1d1	svalová
tkáň	tkáň	k1gFnSc1	tkáň
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vyprodukovat	vyprodukovat	k5eAaPmF	vyprodukovat
sílu	síla	k1gFnSc4	síla
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
jak	jak	k6eAd1	jak
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Sval	sval	k1gInSc1	sval
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
stažlivými	stažlivý	k2eAgNnPc7d1	stažlivý
vlákny	vlákno	k1gNnPc7	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
tři	tři	k4xCgInPc4	tři
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
svaloviny	svalovina	k1gFnSc2	svalovina
<g/>
:	:	kIx,	:
hladkou	hladký	k2eAgFnSc4d1	hladká
<g/>
,	,	kIx,	,
kosterní	kosterní	k2eAgFnSc4d1	kosterní
(	(	kIx(	(
<g/>
příčně	příčně	k6eAd1	příčně
pruhovanou	pruhovaný	k2eAgFnSc4d1	pruhovaná
<g/>
)	)	kIx)	)
a	a	k8xC	a
srdeční	srdeční	k2eAgInPc4d1	srdeční
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hladké	hladký	k2eAgFnSc6d1	hladká
svalovině	svalovina	k1gFnSc6	svalovina
není	být	k5eNaImIp3nS	být
pod	pod	k7c7	pod
mikroskopem	mikroskop	k1gInSc7	mikroskop
patrné	patrný	k2eAgNnSc1d1	patrné
příčné	příčný	k2eAgNnSc1d1	příčné
žíhání	žíhání	k1gNnSc1	žíhání
a	a	k8xC	a
stahuje	stahovat	k5eAaImIp3nS	stahovat
se	se	k3xPyFc4	se
pomaleji	pomale	k6eAd2	pomale
než	než	k8xS	než
svalovina	svalovina	k1gFnSc1	svalovina
kosterní	kosterní	k2eAgFnSc1d1	kosterní
<g/>
.	.	kIx.	.
</s>
<s>
Hladkou	hladký	k2eAgFnSc4d1	hladká
svalovinu	svalovina	k1gFnSc4	svalovina
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
v	v	k7c6	v
chapadlech	chapadlo	k1gNnPc6	chapadlo
mořských	mořský	k2eAgFnPc2d1	mořská
sasanek	sasanka	k1gFnPc2	sasanka
či	či	k8xC	či
na	na	k7c6	na
tělech	tělo	k1gNnPc6	tělo
mořských	mořský	k2eAgFnPc2d1	mořská
okurek	okurka	k1gFnPc2	okurka
(	(	kIx(	(
<g/>
sumýšů	sumýš	k1gMnPc2	sumýš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
vyšších	vysoký	k2eAgMnPc2d2	vyšší
živočichů	živočich	k1gMnPc2	živočich
najdeme	najít	k5eAaPmIp1nP	najít
hladkou	hladký	k2eAgFnSc4d1	hladká
svalovinu	svalovina	k1gFnSc4	svalovina
ve	v	k7c6	v
stěnách	stěna	k1gFnPc6	stěna
dělohy	děloha	k1gFnSc2	děloha
<g/>
,	,	kIx,	,
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
<g/>
,	,	kIx,	,
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
jícnu	jícen	k1gInSc2	jícen
<g/>
,	,	kIx,	,
dýchacího	dýchací	k2eAgInSc2d1	dýchací
traktu	trakt	k1gInSc2	trakt
a	a	k8xC	a
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Kosterní	kosterní	k2eAgFnSc1d1	kosterní
svalovina	svalovina	k1gFnSc1	svalovina
se	se	k3xPyFc4	se
stahuje	stahovat	k5eAaImIp3nS	stahovat
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gNnSc1	její
natažení	natažení	k1gNnSc1	natažení
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
omezené	omezený	k2eAgNnSc1d1	omezené
<g/>
.	.	kIx.	.
</s>
<s>
Kosterní	kosterní	k2eAgFnSc4d1	kosterní
svalovinu	svalovina	k1gFnSc4	svalovina
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
v	v	k7c6	v
končetinách	končetina	k1gFnPc6	končetina
a	a	k8xC	a
chapadlech	chapadlo	k1gNnPc6	chapadlo
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgMnSc1d2	vyšší
živočichové	živočich	k1gMnPc1	živočich
mají	mít	k5eAaImIp3nP	mít
kosterní	kosterní	k2eAgFnSc4d1	kosterní
svalovinu	svalovina	k1gFnSc4	svalovina
připojenou	připojený	k2eAgFnSc4d1	připojená
ke	k	k7c3	k
kostem	kost	k1gFnPc3	kost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
svalovina	svalovina	k1gFnSc1	svalovina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kontrakci	kontrakce	k1gFnSc4	kontrakce
a	a	k8xC	a
pumpuje	pumpovat	k5eAaImIp3nS	pumpovat
tak	tak	k6eAd1	tak
krev	krev	k1gFnSc4	krev
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nervová	nervový	k2eAgFnSc1d1	nervová
tkáň	tkáň	k1gFnSc1	tkáň
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
-	-	kIx~	-
neuronů	neuron	k1gInPc2	neuron
-	-	kIx~	-
které	který	k3yRgInPc1	který
přenášejí	přenášet	k5eAaImIp3nP	přenášet
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
radiálně	radiálně	k6eAd1	radiálně
symetrických	symetrický	k2eAgMnPc2d1	symetrický
mořských	mořský	k2eAgMnPc2d1	mořský
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
žebernatek	žebernatka	k1gFnPc2	žebernatka
nebo	nebo	k8xC	nebo
žahavců	žahavec	k1gInPc2	žahavec
(	(	kIx(	(
<g/>
mořské	mořský	k2eAgFnPc1d1	mořská
sasanky	sasanka	k1gFnPc1	sasanka
<g/>
,	,	kIx,	,
medúzy	medúza	k1gFnPc1	medúza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
nervy	nerv	k1gInPc4	nerv
nervovou	nervový	k2eAgFnSc4d1	nervová
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
živočichů	živočich	k1gMnPc2	živočich
jsou	být	k5eAaImIp3nP	být
nervová	nervový	k2eAgNnPc1d1	nervové
vlákna	vlákno	k1gNnPc1	vlákno
uspořádána	uspořádán	k2eAgNnPc1d1	uspořádáno
do	do	k7c2	do
podélných	podélný	k2eAgMnPc2d1	podélný
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Nervové	nervový	k2eAgInPc4d1	nervový
receptory	receptor	k1gInPc4	receptor
nižších	nízký	k2eAgMnPc2d2	nižší
živočichů	živočich	k1gMnPc2	živočich
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
podněty	podnět	k1gInPc4	podnět
pouze	pouze	k6eAd1	pouze
lokálně	lokálně	k6eAd1	lokálně
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
podnětu	podnět	k1gInSc2	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgMnPc1d2	vyšší
živočichové	živočich	k1gMnPc1	živočich
mají	mít	k5eAaImIp3nP	mít
speciální	speciální	k2eAgInPc4d1	speciální
receptory	receptor	k1gInPc4	receptor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
chemoreceptory	chemoreceptor	k1gInPc1	chemoreceptor
či	či	k8xC	či
fotoreceptory	fotoreceptor	k1gInPc1	fotoreceptor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vedou	vést	k5eAaImIp3nP	vést
signál	signál	k1gInSc4	signál
nervovým	nervový	k2eAgInSc7d1	nervový
systémem	systém	k1gInSc7	systém
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Neurony	neuron	k1gInPc1	neuron
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
ganglii	ganglie	k1gFnSc3	ganglie
<g/>
.	.	kIx.	.
</s>
<s>
Nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
vyšších	vysoký	k2eAgMnPc2d2	vyšší
živočichů	živočich	k1gMnPc2	živočich
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
centrální	centrální	k2eAgFnSc4d1	centrální
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
(	(	kIx(	(
<g/>
CNS	CNS	kA	CNS
<g/>
)	)	kIx)	)
a	a	k8xC	a
periferní	periferní	k2eAgFnSc4d1	periferní
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
(	(	kIx(	(
<g/>
PNS	PNS	kA	PNS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
PNS	PNS	kA	PNS
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
senzorickými	senzorický	k2eAgInPc7d1	senzorický
nervy	nerv	k1gInPc7	nerv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vedou	vést	k5eAaImIp3nP	vést
informace	informace	k1gFnPc4	informace
ze	z	k7c2	z
smyslových	smyslový	k2eAgInPc2d1	smyslový
orgánů	orgán	k1gInPc2	orgán
do	do	k7c2	do
motorických	motorický	k2eAgInPc2d1	motorický
nervů	nerv	k1gInPc2	nerv
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
cílové	cílový	k2eAgInPc4d1	cílový
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
PNS	PNS	kA	PNS
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
somatický	somatický	k2eAgInSc4d1	somatický
nervový	nervový	k2eAgInSc4d1	nervový
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
zodpovídá	zodpovídat	k5eAaImIp3nS	zodpovídat
za	za	k7c4	za
činnost	činnost	k1gFnSc4	činnost
svalů	sval	k1gInPc2	sval
ovládaných	ovládaný	k2eAgFnPc2d1	ovládaná
vůlí	vůle	k1gFnPc2	vůle
<g/>
)	)	kIx)	)
a	a	k8xC	a
autonomní	autonomní	k2eAgInSc1d1	autonomní
nervový	nervový	k2eAgInSc1d1	nervový
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
ovládající	ovládající	k2eAgNnSc1d1	ovládající
hladké	hladký	k2eAgNnSc1d1	hladké
svalstvo	svalstvo	k1gNnSc1	svalstvo
<g/>
,	,	kIx,	,
některé	některý	k3yIgNnSc1	některý
žlázy	žláza	k1gFnPc4	žláza
a	a	k8xC	a
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
orgány	orgán	k1gInPc4	orgán
<g/>
)	)	kIx)	)
Všichni	všechen	k3xTgMnPc1	všechen
obratlovci	obratlovec	k1gMnPc1	obratlovec
mají	mít	k5eAaImIp3nP	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
stavbu	stavba	k1gFnSc4	stavba
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
stádiu	stádium	k1gNnSc6	stádium
vývoje	vývoj	k1gInSc2	vývoj
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
embryonálním	embryonální	k2eAgNnSc6d1	embryonální
stádiu	stádium	k1gNnSc6	stádium
<g/>
)	)	kIx)	)
sdílí	sdílet	k5eAaImIp3nS	sdílet
podobné	podobný	k2eAgFnPc4d1	podobná
charakteristiky	charakteristika	k1gFnPc4	charakteristika
<g/>
:	:	kIx,	:
hřbetní	hřbetní	k2eAgFnSc4d1	hřbetní
strunu	struna	k1gFnSc4	struna
<g/>
,	,	kIx,	,
nervovou	nervový	k2eAgFnSc4d1	nervová
trubici	trubice	k1gFnSc4	trubice
či	či	k8xC	či
žaberní	žaberní	k2eAgInPc4d1	žaberní
oblouky	oblouk	k1gInPc4	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Mícha	mícha	k1gFnSc1	mícha
obratlovců	obratlovec	k1gMnPc2	obratlovec
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
páteří	páteř	k1gFnSc7	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Nervová	nervový	k2eAgFnSc1d1	nervová
tkáň	tkáň	k1gFnSc1	tkáň
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
z	z	k7c2	z
ektodermu	ektoderm	k1gInSc2	ektoderm
<g/>
,	,	kIx,	,
pojivové	pojivový	k2eAgFnSc2d1	pojivová
tkáně	tkáň	k1gFnSc2	tkáň
tvoří	tvořit	k5eAaImIp3nS	tvořit
entoderm	entoderm	k1gInSc1	entoderm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
míchy	mícha	k1gFnSc2	mícha
a	a	k8xC	a
páteře	páteř	k1gFnSc2	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Ústní	ústní	k2eAgFnSc1d1	ústní
dutina	dutina	k1gFnSc1	dutina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c4	v
přední	přední	k2eAgFnPc4d1	přední
části	část	k1gFnPc4	část
těla	tělo	k1gNnSc2	tělo
živočicha	živočich	k1gMnSc2	živočich
a	a	k8xC	a
řitní	řitní	k2eAgInSc1d1	řitní
otvor	otvor	k1gInSc1	otvor
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
při	při	k7c6	při
bázi	báze	k1gFnSc6	báze
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
znakem	znak	k1gInSc7	znak
obratlovců	obratlovec	k1gMnPc2	obratlovec
je	být	k5eAaImIp3nS	být
páteř	páteř	k1gFnSc1	páteř
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
obratlů	obratel	k1gInPc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
embryonálního	embryonální	k2eAgInSc2d1	embryonální
vývoje	vývoj	k1gInSc2	vývoj
hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
struna	struna	k1gFnSc1	struna
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
v	v	k7c4	v
meziobratlovou	meziobratlový	k2eAgFnSc4d1	meziobratlová
ploténku	ploténka	k1gFnSc4	ploténka
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
obratlovcům	obratlovec	k1gMnPc3	obratlovec
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
)	)	kIx)	)
však	však	k9	však
hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
struna	struna	k1gFnSc1	struna
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
i	i	k9	i
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Čelistnatci	čelistnatec	k1gMnPc1	čelistnatec
jsou	být	k5eAaImIp3nP	být
charakterizováni	charakterizovat	k5eAaBmNgMnP	charakterizovat
párovými	párový	k2eAgFnPc7d1	párová
končetinami	končetina	k1gFnPc7	končetina
(	(	kIx(	(
<g/>
ploutvemi	ploutev	k1gFnPc7	ploutev
<g/>
,	,	kIx,	,
či	či	k8xC	či
nohama	noha	k1gFnPc7	noha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zakrnělé	zakrnělý	k2eAgFnPc1d1	zakrnělá
<g/>
.	.	kIx.	.
</s>
<s>
Končetiny	končetina	k1gFnPc1	končetina
obratlovců	obratlovec	k1gMnPc2	obratlovec
mají	mít	k5eAaImIp3nP	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zdědili	zdědit	k5eAaPmAgMnP	zdědit
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
posledním	poslední	k2eAgNnSc6d1	poslední
společném	společný	k2eAgNnSc6d1	společné
předkovi	předek	k1gMnSc3	předek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
argument	argument	k1gInSc4	argument
předložil	předložit	k5eAaPmAgMnS	předložit
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
k	k	k7c3	k
podložení	podložení	k1gNnSc3	podložení
své	svůj	k3xOyFgFnSc2	svůj
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
ryby	ryba	k1gFnSc2	ryba
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
trupu	trup	k1gInSc2	trup
a	a	k8xC	a
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
nejsou	být	k5eNaImIp3nP	být
někdy	někdy	k6eAd1	někdy
přechody	přechod	k1gInPc4	přechod
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
třemi	tři	k4xCgFnPc7	tři
částmi	část	k1gFnPc7	část
jasně	jasně	k6eAd1	jasně
zřetelné	zřetelný	k2eAgFnPc1d1	zřetelná
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
opěrnou	opěrný	k2eAgFnSc4d1	opěrná
strukturu	struktura	k1gFnSc4	struktura
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
ryby	ryba	k1gFnSc2	ryba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
buď	buď	k8xC	buď
chrupavkou	chrupavka	k1gFnSc7	chrupavka
(	(	kIx(	(
<g/>
chrupavčití	chrupavčitý	k2eAgMnPc1d1	chrupavčitý
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kostí	kost	k1gFnPc2	kost
(	(	kIx(	(
<g/>
kostnatí	kostnatět	k5eAaImIp3nS	kostnatět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
částí	část	k1gFnSc7	část
kostry	kostra	k1gFnSc2	kostra
je	být	k5eAaImIp3nS	být
páteř	páteř	k1gFnSc1	páteř
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
lehkých	lehký	k2eAgFnPc2d1	lehká
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
velmi	velmi	k6eAd1	velmi
pevných	pevný	k2eAgInPc2d1	pevný
obratlů	obratel	k1gInPc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Žebra	žebro	k1gNnPc1	žebro
jsou	být	k5eAaImIp3nP	být
připevněna	připevněn	k2eAgNnPc1d1	připevněno
k	k	k7c3	k
páteři	páteř	k1gFnSc3	páteř
a	a	k8xC	a
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
nejsou	být	k5eNaImIp3nP	být
patrné	patrný	k2eAgFnPc1d1	patrná
žádné	žádný	k3yNgFnPc1	žádný
končetiny	končetina	k1gFnPc1	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
vnější	vnější	k2eAgInPc1d1	vnější
znaky	znak	k1gInPc1	znak
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
ploutve	ploutev	k1gFnSc2	ploutev
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
kostěnými	kostěný	k2eAgInPc7d1	kostěný
nebo	nebo	k8xC	nebo
měkkými	měkký	k2eAgInPc7d1	měkký
paprsky	paprsek	k1gInPc7	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
ocasní	ocasní	k2eAgFnSc2d1	ocasní
ploutve	ploutev	k1gFnSc2	ploutev
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
napojeny	napojit	k5eAaPmNgInP	napojit
na	na	k7c4	na
páteř	páteř	k1gFnSc4	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vyztuženy	vyztužit	k5eAaPmNgInP	vyztužit
svaly	sval	k1gInPc7	sval
tvořícími	tvořící	k2eAgInPc7d1	tvořící
většinu	většina	k1gFnSc4	většina
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
a	a	k8xC	a
pumpuje	pumpovat	k5eAaImIp3nS	pumpovat
krev	krev	k1gFnSc4	krev
přes	přes	k7c4	přes
žaberní	žaberní	k2eAgFnPc4d1	žaberní
štěrbiny	štěrbina	k1gFnPc4	štěrbina
celým	celý	k2eAgInSc7d1	celý
tělem	tělo	k1gNnSc7	tělo
ryby	ryba	k1gFnSc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
přizpůsobeny	přizpůsobit	k5eAaPmNgFnP	přizpůsobit
k	k	k7c3	k
podvodnímu	podvodní	k2eAgNnSc3d1	podvodní
vidění	vidění	k1gNnSc3	vidění
<g/>
,	,	kIx,	,
dohlednost	dohlednost	k1gFnSc1	dohlednost
ryb	ryba	k1gFnPc2	ryba
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
značně	značně	k6eAd1	značně
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
mají	mít	k5eAaImIp3nP	mít
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
ucho	ucho	k1gNnSc4	ucho
<g/>
,	,	kIx,	,
postrádají	postrádat	k5eAaImIp3nP	postrádat
však	však	k9	však
vnější	vnější	k2eAgNnSc4d1	vnější
i	i	k8xC	i
střední	střední	k2eAgNnSc4d1	střední
ucho	ucho	k1gNnSc4	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Postranní	postranní	k2eAgFnSc1d1	postranní
čára	čára	k1gFnSc1	čára
(	(	kIx(	(
<g/>
smyslový	smyslový	k2eAgInSc1d1	smyslový
orgán	orgán	k1gInSc1	orgán
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
trupu	trup	k1gInSc2	trup
ryb	ryba	k1gFnPc2	ryba
<g/>
)	)	kIx)	)
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
nízkofrekvenční	nízkofrekvenční	k2eAgFnPc4d1	nízkofrekvenční
vibrace	vibrace	k1gFnPc4	vibrace
a	a	k8xC	a
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
pohyby	pohyb	k1gInPc4	pohyb
či	či	k8xC	či
změny	změna	k1gFnPc4	změna
tlaku	tlak	k1gInSc2	tlak
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Obojživelníci	obojživelník	k1gMnPc1	obojživelník
jsou	být	k5eAaImIp3nP	být
třídou	třída	k1gFnSc7	třída
živočichů	živočich	k1gMnPc2	živočich
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
žáby	žába	k1gFnSc2	žába
<g/>
,	,	kIx,	,
mloky	mlok	k1gMnPc4	mlok
a	a	k8xC	a
červory	červor	k1gMnPc4	červor
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
mají	mít	k5eAaImIp3nP	mít
čtyři	čtyři	k4xCgFnPc1	čtyři
končetiny	končetina	k1gFnPc1	končetina
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
červoři	červor	k1gMnPc1	červor
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mloků	mlok	k1gMnPc2	mlok
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgFnPc4	žádný
končetiny	končetina	k1gFnPc4	končetina
či	či	k8xC	či
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gFnPc4	jejich
končetiny	končetina	k1gFnPc4	končetina
zakrnělé	zakrnělý	k2eAgFnPc4d1	zakrnělá
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
obojživelníků	obojživelník	k1gMnPc2	obojživelník
jsou	být	k5eAaImIp3nP	být
duté	dutý	k2eAgFnPc1d1	dutá
a	a	k8xC	a
lehké	lehký	k2eAgFnPc1d1	lehká
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
zkostnatělé	zkostnatělý	k2eAgFnPc1d1	zkostnatělá
<g/>
.	.	kIx.	.
</s>
<s>
Obratle	obratel	k1gInPc1	obratel
těchto	tento	k3xDgMnPc2	tento
živočichů	živočich	k1gMnPc2	živočich
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgInPc1d1	spojený
klouby	kloub	k1gInPc1	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Žebra	žebro	k1gNnPc1	žebro
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
krátká	krátký	k2eAgNnPc1d1	krátké
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
páteří	páteř	k1gFnSc7	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Lebky	lebka	k1gFnPc1	lebka
obojživelníků	obojživelník	k1gMnPc2	obojživelník
jsou	být	k5eAaImIp3nP	být
široké	široký	k2eAgNnSc4d1	široké
a	a	k8xC	a
krátké	krátký	k2eAgNnSc4d1	krátké
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
zkostnatělé	zkostnatělý	k2eAgNnSc1d1	zkostnatělé
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
kůže	kůže	k1gFnSc1	kůže
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jen	jen	k9	jen
málo	málo	k1gNnSc1	málo
keratinu	keratin	k1gInSc2	keratin
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
šupinatá	šupinatý	k2eAgFnSc1d1	šupinatá
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
však	však	k9	však
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
slizových	slizový	k2eAgFnPc2d1	slizová
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
dokonce	dokonce	k9	dokonce
žlázy	žláza	k1gFnPc1	žláza
jedové	jedový	k2eAgFnPc1d1	jedová
<g/>
.	.	kIx.	.
</s>
<s>
Srdce	srdce	k1gNnSc1	srdce
obojživelníků	obojživelník	k1gMnPc2	obojživelník
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
komor	komora	k1gFnPc2	komora
-	-	kIx~	-
dvou	dva	k4xCgInPc6	dva
síní	síň	k1gFnPc2	síň
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
močový	močový	k2eAgInSc4d1	močový
měchýř	měchýř	k1gInSc4	měchýř
a	a	k8xC	a
dusíkaté	dusíkatý	k2eAgInPc1d1	dusíkatý
odpady	odpad	k1gInPc1	odpad
jsou	být	k5eAaImIp3nP	být
vylučovány	vylučován	k2eAgMnPc4d1	vylučován
především	především	k6eAd1	především
močí	močit	k5eAaImIp3nP	močit
<g/>
.	.	kIx.	.
</s>
<s>
Obojživelníci	obojživelník	k1gMnPc1	obojživelník
dýchají	dýchat	k5eAaImIp3nP	dýchat
vzduch	vzduch	k1gInSc4	vzduch
pumpováním	pumpování	k1gNnSc7	pumpování
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
-	-	kIx~	-
vzduch	vzduch	k1gInSc1	vzduch
je	být	k5eAaImIp3nS	být
nejprve	nejprve	k6eAd1	nejprve
veden	vést	k5eAaImNgInS	vést
přes	přes	k7c4	přes
nozdry	nozdra	k1gFnPc4	nozdra
do	do	k7c2	do
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
<g/>
,	,	kIx,	,
nozdry	nozdra	k1gFnPc1	nozdra
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
uzavřou	uzavřít	k5eAaPmIp3nP	uzavřít
a	a	k8xC	a
vzduch	vzduch	k1gInSc1	vzduch
je	být	k5eAaImIp3nS	být
stažením	stažení	k1gNnSc7	stažení
krku	krk	k1gInSc2	krk
hnán	hnát	k5eAaImNgInS	hnát
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Plicní	plicní	k2eAgNnSc1d1	plicní
dýchání	dýchání	k1gNnSc1	dýchání
je	být	k5eAaImIp3nS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
dýcháním	dýchání	k1gNnSc7	dýchání
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
obojživelníků	obojživelník	k1gMnPc2	obojživelník
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
udržována	udržovat	k5eAaImNgFnS	udržovat
vlhká	vlhký	k2eAgFnSc1d1	vlhká
<g/>
.	.	kIx.	.
</s>
<s>
Plazi	plaz	k1gMnPc1	plaz
jsou	být	k5eAaImIp3nP	být
živočišnou	živočišný	k2eAgFnSc7d1	živočišná
třídou	třída	k1gFnSc7	třída
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
želvy	želva	k1gFnSc2	želva
<g/>
,	,	kIx,	,
hatérie	hatérie	k1gFnSc2	hatérie
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc4	ještěrka
<g/>
,	,	kIx,	,
hady	had	k1gMnPc4	had
a	a	k8xC	a
krokodýly	krokodýl	k1gMnPc4	krokodýl
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
čtyři	čtyři	k4xCgFnPc1	čtyři
končetiny	končetina	k1gFnPc1	končetina
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
hadi	had	k1gMnPc1	had
a	a	k8xC	a
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
ještěrek	ještěrka	k1gFnPc2	ještěrka
nemá	mít	k5eNaImIp3nS	mít
nohy	noha	k1gFnPc4	noha
žádné	žádný	k3yNgInPc4	žádný
či	či	k8xC	či
zakrnělé	zakrnělý	k2eAgInPc4d1	zakrnělý
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
plazů	plaz	k1gMnPc2	plaz
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
zkostnatělé	zkostnatělý	k2eAgFnPc1d1	zkostnatělá
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
pevnější	pevný	k2eAgFnPc1d2	pevnější
než	než	k8xS	než
kosti	kost	k1gFnPc1	kost
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
plazů	plaz	k1gMnPc2	plaz
mají	mít	k5eAaImIp3nP	mít
kuželovitý	kuželovitý	k2eAgInSc4d1	kuželovitý
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgNnSc1d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Povrchové	povrchový	k2eAgFnPc1d1	povrchová
buňky	buňka	k1gFnPc1	buňka
pokožky	pokožka	k1gFnSc2	pokožka
jsou	být	k5eAaImIp3nP	být
přeměněny	přeměnit	k5eAaPmNgFnP	přeměnit
ve	v	k7c4	v
zrohovatělé	zrohovatělý	k2eAgFnPc4d1	zrohovatělá
šupiny	šupina	k1gFnPc4	šupina
tvořící	tvořící	k2eAgFnSc4d1	tvořící
voděodolnou	voděodolný	k2eAgFnSc4d1	voděodolná
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
,	,	kIx,	,
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
dýchat	dýchat	k5eAaImF	dýchat
pokožkou	pokožka	k1gFnSc7	pokožka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
dýchací	dýchací	k2eAgFnSc1d1	dýchací
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
efektivnější	efektivní	k2eAgNnSc1d2	efektivnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vhání	vhánět	k5eAaImIp3nS	vhánět
kyslík	kyslík	k1gInSc4	kyslík
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
pomocí	pomocí	k7c2	pomocí
roztahování	roztahování	k1gNnSc2	roztahování
hrudní	hrudní	k2eAgFnSc2d1	hrudní
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Srdce	srdce	k1gNnSc1	srdce
plazů	plaz	k1gMnPc2	plaz
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
srdci	srdce	k1gNnSc3	srdce
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ovšem	ovšem	k9	ovšem
přepážku	přepážka	k1gFnSc4	přepážka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mnohem	mnohem	k6eAd1	mnohem
dokonaleji	dokonale	k6eAd2	dokonale
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
okysličenou	okysličený	k2eAgFnSc4d1	okysličená
a	a	k8xC	a
neokysličenou	okysličený	k2eNgFnSc4d1	neokysličená
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožovací	rozmnožovací	k2eAgFnSc1d1	rozmnožovací
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
uzpůsobena	uzpůsobit	k5eAaPmNgFnS	uzpůsobit
vnitřnímu	vnitřní	k2eAgNnSc3d1	vnitřní
oplození	oplození	k1gNnSc3	oplození
<g/>
,	,	kIx,	,
sestává	sestávat	k5eAaImIp3nS	sestávat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
z	z	k7c2	z
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
orgánů	orgán	k1gInPc2	orgán
přítomných	přítomný	k2eAgInPc2d1	přítomný
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
jsou	být	k5eAaImIp3nP	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
blánou	blána	k1gFnSc7	blána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	on	k3xPp3gFnPc4	on
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
vysušením	vysušení	k1gNnSc7	vysušení
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
kladena	kladen	k2eAgNnPc4d1	kladeno
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Močový	močový	k2eAgInSc1d1	močový
měchýř	měchýř	k1gInSc1	měchýř
plazů	plaz	k1gMnPc2	plaz
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
a	a	k8xC	a
dusíkaté	dusíkatý	k2eAgFnPc1d1	dusíkatá
odpadní	odpadní	k2eAgFnPc1d1	odpadní
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
odváděny	odváděn	k2eAgInPc4d1	odváděn
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
<g/>
.	.	kIx.	.
</s>
<s>
Želvy	želva	k1gFnPc1	želva
jsou	být	k5eAaImIp3nP	být
význačné	význačný	k2eAgInPc1d1	význačný
svou	svůj	k3xOyFgFnSc7	svůj
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
schránkou	schránka	k1gFnSc7	schránka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
neohebný	ohebný	k2eNgInSc1d1	neohebný
trup	trup	k1gInSc1	trup
je	být	k5eAaImIp3nS	být
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
v	v	k7c6	v
rohovinovém	rohovinový	k2eAgInSc6d1	rohovinový
krunýři	krunýř	k1gInSc6	krunýř
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
kostěnými	kostěný	k2eAgInPc7d1	kostěný
pláty	plát	k1gInPc7	plát
usazenými	usazený	k2eAgInPc7d1	usazený
v	v	k7c6	v
kožní	kožní	k2eAgFnSc6d1	kožní
vrstvě	vrstva	k1gFnSc6	vrstva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
vrstvou	vrstva	k1gFnSc7	vrstva
zrohovatělou	zrohovatělý	k2eAgFnSc7d1	zrohovatělá
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
se	s	k7c7	s
žebry	žebr	k1gInPc7	žebr
a	a	k8xC	a
páteří	páteř	k1gFnSc7	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
želv	želva	k1gFnPc2	želva
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
ohebný	ohebný	k2eAgInSc1d1	ohebný
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
končetiny	končetina	k1gFnPc1	končetina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zatáhnuty	zatáhnuty	k?	zatáhnuty
dovnitř	dovnitř	k7c2	dovnitř
krunýře	krunýř	k1gInSc2	krunýř
<g/>
.	.	kIx.	.
</s>
<s>
Želvy	želva	k1gFnPc1	želva
jsou	být	k5eAaImIp3nP	být
býložravci	býložravec	k1gMnSc3	býložravec
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
zuby	zub	k1gInPc1	zub
byly	být	k5eAaImAgInP	být
tudíž	tudíž	k8xC	tudíž
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
ostrými	ostrý	k2eAgInPc7d1	ostrý
rohovitými	rohovitý	k2eAgInPc7d1	rohovitý
výčnělky	výčnělek	k1gInPc7	výčnělek
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnPc1d1	přední
končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
druhů	druh	k1gInPc2	druh
želv	želva	k1gFnPc2	želva
přeměněny	přeměněn	k2eAgInPc4d1	přeměněn
na	na	k7c4	na
ploutve	ploutev	k1gFnPc4	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Ještěrky	ještěrka	k1gFnPc1	ještěrka
mají	mít	k5eAaImIp3nP	mít
lebku	lebka	k1gFnSc4	lebka
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
otvorem	otvor	k1gInSc7	otvor
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
široce	široko	k6eAd1	široko
rozevřít	rozevřít	k5eAaPmF	rozevřít
ústa	ústa	k1gNnPc4	ústa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
čelisti	čelist	k1gFnPc1	čelist
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
mnohem	mnohem	k6eAd1	mnohem
volněji	volně	k6eAd2	volně
<g/>
.	.	kIx.	.
</s>
<s>
Ještěrky	ještěrka	k1gFnPc1	ještěrka
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
čtyři	čtyři	k4xCgFnPc1	čtyři
krátké	krátký	k2eAgFnPc1d1	krátká
postranní	postranní	k2eAgFnPc1d1	postranní
končetiny	končetina	k1gFnPc1	končetina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
drží	držet	k5eAaImIp3nP	držet
tělo	tělo	k1gNnSc4	tělo
mírně	mírně	k6eAd1	mírně
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
ovšem	ovšem	k9	ovšem
nohy	noha	k1gFnPc4	noha
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
hadům	had	k1gMnPc3	had
<g/>
.	.	kIx.	.
</s>
<s>
Ještěrky	ještěrka	k1gFnPc1	ještěrka
mají	mít	k5eAaImIp3nP	mít
pohyblivá	pohyblivý	k2eAgNnPc4d1	pohyblivé
víčka	víčko	k1gNnPc4	víčko
<g/>
,	,	kIx,	,
ušní	ušní	k2eAgInPc4d1	ušní
bubínky	bubínek	k1gInPc4	bubínek
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
ještěrek	ještěrka	k1gFnPc2	ještěrka
mají	mít	k5eAaImIp3nP	mít
dokonce	dokonce	k9	dokonce
parietální	parietální	k2eAgNnSc4d1	parietální
oko	oko	k1gNnSc4	oko
(	(	kIx(	(
<g/>
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
třetí	třetí	k4xOgNnSc4	třetí
oko	oko	k1gNnSc4	oko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hadi	had	k1gMnPc1	had
jsou	být	k5eAaImIp3nP	být
blízkými	blízký	k2eAgMnPc7d1	blízký
příbuznými	příbuzný	k1gMnPc7	příbuzný
ještěrek	ještěrka	k1gFnPc2	ještěrka
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
oddělili	oddělit	k5eAaPmAgMnP	oddělit
ze	z	k7c2	z
společné	společný	k2eAgFnSc2d1	společná
rodové	rodový	k2eAgFnSc2d1	rodová
linie	linie	k1gFnSc2	linie
v	v	k7c6	v
období	období	k1gNnSc6	období
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
</s>
<s>
Sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
ještěrkami	ještěrka	k1gFnPc7	ještěrka
mnoho	mnoho	k6eAd1	mnoho
společných	společný	k2eAgInPc2d1	společný
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
jazylky	jazylka	k1gFnSc2	jazylka
<g/>
,	,	kIx,	,
páteře	páteř	k1gFnSc2	páteř
a	a	k8xC	a
žeber	žebro	k1gNnPc2	žebro
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
si	se	k3xPyFc3	se
také	také	k6eAd1	také
ponechaly	ponechat	k5eAaPmAgFnP	ponechat
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
pánve	pánev	k1gFnSc2	pánev
a	a	k8xC	a
zadních	zadní	k2eAgFnPc2d1	zadní
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
jako	jako	k8xC	jako
ještěrky	ještěrka	k1gFnSc2	ještěrka
mohou	moct	k5eAaImIp3nP	moct
hadi	had	k1gMnPc1	had
rozevřít	rozevřít	k5eAaPmF	rozevřít
ústa	ústa	k1gNnPc4	ústa
tak	tak	k6eAd1	tak
doširoka	doširoka	k6eAd1	doširoka
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáží	dokázat	k5eAaPmIp3nP	dokázat
spolknout	spolknout	k5eAaPmF	spolknout
celou	celý	k2eAgFnSc4d1	celá
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Hadi	had	k1gMnPc1	had
mají	mít	k5eAaImIp3nP	mít
pohyblivá	pohyblivý	k2eAgNnPc1d1	pohyblivé
víčka	víčko	k1gNnPc1	víčko
<g/>
,	,	kIx,	,
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
průhlednými	průhledný	k2eAgFnPc7d1	průhledná
šupinami	šupina	k1gFnPc7	šupina
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
ušní	ušní	k2eAgInPc4d1	ušní
bubínky	bubínek	k1gInPc4	bubínek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kostí	kost	k1gFnPc2	kost
lebky	lebka	k1gFnSc2	lebka
dokáží	dokázat	k5eAaPmIp3nP	dokázat
detekovat	detekovat	k5eAaImF	detekovat
vibrace	vibrace	k1gFnPc4	vibrace
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
rozeklaný	rozeklaný	k2eAgInSc1d1	rozeklaný
jazyk	jazyk	k1gInSc1	jazyk
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
rozpoznání	rozpoznání	k1gNnSc3	rozpoznání
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
pachu	pach	k1gInSc2	pach
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
hadi	had	k1gMnPc1	had
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
senzorickou	senzorický	k2eAgFnSc4d1	senzorická
dutinu	dutina	k1gFnSc4	dutina
umožňující	umožňující	k2eAgMnSc1d1	umožňující
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
teplokrevnou	teplokrevný	k2eAgFnSc4d1	teplokrevná
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Krokodýli	krokodýl	k1gMnPc1	krokodýl
jsou	být	k5eAaImIp3nP	být
mohutní	mohutný	k2eAgMnPc1d1	mohutný
vodní	vodní	k2eAgMnPc1d1	vodní
plazi	plaz	k1gMnPc1	plaz
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
čenichem	čenich	k1gInSc7	čenich
a	a	k8xC	a
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
tělo	tělo	k1gNnSc1	tělo
jsou	být	k5eAaImIp3nP	být
dorzoventrálně	dorzoventrálně	k6eAd1	dorzoventrálně
zploštělé	zploštělý	k2eAgInPc1d1	zploštělý
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
též	též	k9	též
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
zploštělý	zploštělý	k2eAgInSc1d1	zploštělý
<g/>
.	.	kIx.	.
</s>
<s>
Krokodýli	krokodýl	k1gMnPc1	krokodýl
jím	on	k3xPp3gMnSc7	on
vlní	vlnit	k5eAaImIp3nS	vlnit
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pohyb	pohyb	k1gInSc1	pohyb
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tuhé	tuhý	k2eAgFnPc1d1	tuhá
zrohovatělé	zrohovatělý	k2eAgFnPc1d1	zrohovatělá
šupiny	šupina	k1gFnPc1	šupina
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tělesné	tělesný	k2eAgNnSc4d1	tělesné
brnění	brnění	k1gNnSc4	brnění
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
šupiny	šupina	k1gFnPc1	šupina
jsou	být	k5eAaImIp3nP	být
připevněny	připevněn	k2eAgFnPc1d1	připevněna
k	k	k7c3	k
lebce	lebka	k1gFnSc3	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Nozdry	nozdra	k1gFnPc1	nozdra
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
a	a	k8xC	a
uši	ucho	k1gNnPc1	ucho
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
nad	nad	k7c4	nad
povrch	povrch	k1gInSc4	povrch
ploché	plochý	k2eAgFnSc2d1	plochá
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
když	když	k8xS	když
krokodýl	krokodýl	k1gMnSc1	krokodýl
plave	plavat	k5eAaImIp3nS	plavat
<g/>
.	.	kIx.	.
</s>
<s>
Nozdry	nozdra	k1gFnPc1	nozdra
a	a	k8xC	a
uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
proti	proti	k7c3	proti
vniknutí	vniknutí	k1gNnSc3	vniknutí
vody	voda	k1gFnSc2	voda
chráněné	chráněný	k2eAgFnSc2d1	chráněná
záklopkami	záklopka	k1gFnPc7	záklopka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
plazů	plaz	k1gMnPc2	plaz
mají	mít	k5eAaImIp3nP	mít
krokodýli	krokodýl	k1gMnPc1	krokodýl
čtyřkomorová	čtyřkomorový	k2eAgNnPc4d1	čtyřkomorové
srdce	srdce	k1gNnPc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
komory	komora	k1gFnPc1	komora
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
dokonalé	dokonalý	k2eAgNnSc4d1	dokonalé
oddělení	oddělení	k1gNnSc4	oddělení
okysličené	okysličený	k2eAgFnSc2d1	okysličená
a	a	k8xC	a
neokysličené	okysličený	k2eNgFnSc2d1	neokysličená
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
čtyřnozí	čtyřnohý	k2eAgMnPc1d1	čtyřnohý
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
zadní	zadní	k2eAgFnPc1d1	zadní
končetiny	končetina	k1gFnPc1	končetina
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
chůzi	chůze	k1gFnSc3	chůze
či	či	k8xC	či
poskakování	poskakování	k1gNnSc3	poskakování
a	a	k8xC	a
přední	přední	k2eAgFnSc2d1	přední
končetiny	končetina	k1gFnSc2	končetina
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
v	v	k7c4	v
křídla	křídlo	k1gNnPc4	křídlo
pokrytá	pokrytý	k2eAgNnPc4d1	pokryté
peřím	peřit	k5eAaImIp1nS	peřit
a	a	k8xC	a
uzpůsobená	uzpůsobený	k2eAgFnSc1d1	uzpůsobená
k	k	k7c3	k
létání	létání	k1gNnSc3	létání
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
teplokrevní	teplokrevný	k2eAgMnPc1d1	teplokrevný
živočichové	živočich	k1gMnPc1	živočich
s	s	k7c7	s
rychlým	rychlý	k2eAgInSc7d1	rychlý
metabolismem	metabolismus	k1gInSc7	metabolismus
<g/>
,	,	kIx,	,
lehkou	lehký	k2eAgFnSc7d1	lehká
kostrou	kostra	k1gFnSc7	kostra
a	a	k8xC	a
silnými	silný	k2eAgInPc7d1	silný
svaly	sval	k1gInPc7	sval
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
kosti	kost	k1gFnPc1	kost
mají	mít	k5eAaImIp3nP	mít
tenké	tenký	k2eAgInPc1d1	tenký
<g/>
,	,	kIx,	,
duté	dutý	k2eAgInPc1d1	dutý
a	a	k8xC	a
velice	velice	k6eAd1	velice
lehké	lehký	k2eAgNnSc1d1	lehké
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
kostí	kost	k1gFnPc2	kost
vedou	vést	k5eAaImIp3nP	vést
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
vaky	vak	k1gInPc1	vak
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
plícemi	plíce	k1gFnPc7	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgInPc4	žádný
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
úzké	úzký	k2eAgFnPc1d1	úzká
čelisti	čelist	k1gFnPc1	čelist
se	se	k3xPyFc4	se
přeměnily	přeměnit	k5eAaPmAgFnP	přeměnit
v	v	k7c4	v
zobák	zobák	k1gInSc4	zobák
pokrytý	pokrytý	k2eAgInSc4d1	pokrytý
vrstvou	vrstva	k1gFnSc7	vrstva
rohoviny	rohovina	k1gFnSc2	rohovina
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
nočních	noční	k2eAgInPc2d1	noční
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
sovy	sova	k1gFnPc4	sova
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
dravých	dravý	k2eAgMnPc2d1	dravý
ptáků	pták	k1gMnPc2	pták
směřují	směřovat	k5eAaImIp3nP	směřovat
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
kachen	kachna	k1gFnPc2	kachna
směřují	směřovat	k5eAaImIp3nP	směřovat
do	do	k7c2	do
boku	bok	k1gInSc2	bok
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
epidermální	epidermální	k2eAgInSc1d1	epidermální
kožní	kožní	k2eAgInSc1d1	kožní
porost	porost	k1gInSc1	porost
vyrůstající	vyrůstající	k2eAgInSc1d1	vyrůstající
z	z	k7c2	z
pokožky	pokožka	k1gFnSc2	pokožka
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
pera	pero	k1gNnPc1	pero
sloužící	sloužící	k2eAgNnPc1d1	sloužící
k	k	k7c3	k
létání	létání	k1gNnSc3	létání
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
a	a	k8xC	a
ocase	ocas	k1gInSc6	ocas
<g/>
,	,	kIx,	,
konturové	konturový	k2eAgNnSc1d1	konturové
peří	peří	k1gNnSc1	peří
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
povrch	povrch	k1gInSc4	povrch
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
jemné	jemný	k2eAgNnSc4d1	jemné
prachové	prachový	k2eAgNnSc4d1	prachové
peří	peří	k1gNnSc4	peří
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
na	na	k7c6	na
mláďatech	mládě	k1gNnPc6	mládě
a	a	k8xC	a
pod	pod	k7c7	pod
konturovým	konturový	k2eAgNnSc7d1	konturové
peřím	peří	k1gNnSc7	peří
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
kožní	kožní	k2eAgFnSc7d1	kožní
žlázou	žláza	k1gFnSc7	žláza
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
kostrční	kostrční	k2eAgFnSc1d1	kostrční
žláza	žláza	k1gFnSc1	žláza
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
u	u	k7c2	u
báze	báze	k1gFnSc2	báze
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Produkuje	produkovat	k5eAaImIp3nS	produkovat
mazovou	mazový	k2eAgFnSc4d1	mazová
sekreci	sekrece	k1gFnSc4	sekrece
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
peří	peří	k1gNnSc4	peří
proti	proti	k7c3	proti
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
končetinách	končetina	k1gFnPc6	končetina
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
šupinky	šupinka	k1gFnPc1	šupinka
<g/>
,	,	kIx,	,
prsty	prst	k1gInPc1	prst
jsou	být	k5eAaImIp3nP	být
zakončeny	zakončen	k2eAgInPc1d1	zakončen
drápy	dráp	k1gInPc1	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
jsou	být	k5eAaImIp3nP	být
rozmanitou	rozmanitý	k2eAgFnSc7d1	rozmanitá
skupinou	skupina	k1gFnSc7	skupina
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
suchozemská	suchozemský	k2eAgFnSc1d1	suchozemská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
létají	létat	k5eAaImIp3nP	létat
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
čtyřnohé	čtyřnohý	k2eAgMnPc4d1	čtyřnohý
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
mořské	mořský	k2eAgInPc1d1	mořský
druhy	druh	k1gInPc1	druh
nemají	mít	k5eNaImIp3nP	mít
končetiny	končetina	k1gFnPc4	končetina
žádné	žádný	k3yNgFnPc4	žádný
či	či	k8xC	či
modifikované	modifikovaný	k2eAgFnPc4d1	modifikovaná
do	do	k7c2	do
ploutví	ploutev	k1gFnPc2	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnPc1d1	přední
končetiny	končetina	k1gFnPc1	končetina
netopýrů	netopýr	k1gMnPc2	netopýr
se	se	k3xPyFc4	se
přeměnily	přeměnit	k5eAaPmAgFnP	přeměnit
v	v	k7c4	v
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
většiny	většina	k1gFnSc2	většina
savců	savec	k1gMnPc2	savec
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
pod	pod	k7c7	pod
trupem	trup	k1gInSc7	trup
posazeným	posazený	k2eAgInSc7d1	posazený
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
savců	savec	k1gMnPc2	savec
jsou	být	k5eAaImIp3nP	být
zkostnatělé	zkostnatělý	k2eAgInPc1d1	zkostnatělý
a	a	k8xC	a
zuby	zub	k1gInPc1	zub
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
diferencované	diferencovaný	k2eAgNnSc1d1	diferencované
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
pokryty	pokryt	k2eAgFnPc1d1	pokryta
vrstvou	vrstva	k1gFnSc7	vrstva
zubní	zubní	k2eAgFnPc1d1	zubní
skloviny	sklovina	k1gFnPc1	sklovina
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
život	život	k1gInSc4	život
savců	savec	k1gMnPc2	savec
vymění	vyměnit	k5eAaPmIp3nS	vyměnit
(	(	kIx(	(
<g/>
mléčné	mléčný	k2eAgInPc1d1	mléčný
zuby	zub	k1gInPc1	zub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kytovci	kytovec	k1gMnPc1	kytovec
nemění	měnit	k5eNaImIp3nP	měnit
svoje	svůj	k3xOyFgInPc4	svůj
zuby	zub	k1gInPc4	zub
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
uchu	ucho	k1gNnSc6	ucho
tři	tři	k4xCgFnPc1	tři
kůstky	kůstka	k1gFnPc1	kůstka
a	a	k8xC	a
hlemýždě	hlemýžď	k1gMnPc4	hlemýžď
ve	v	k7c6	v
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
uchu	ucho	k1gNnSc6	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Těla	tělo	k1gNnPc1	tělo
savců	savec	k1gMnPc2	savec
jsou	být	k5eAaImIp3nP	být
pokryta	pokryt	k2eAgFnSc1d1	pokryta
chlupy	chlup	k1gInPc7	chlup
a	a	k8xC	a
pokožka	pokožka	k1gFnSc1	pokožka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
žlázy	žláza	k1gFnPc4	žláza
produkující	produkující	k2eAgFnSc1d1	produkující
pot	pot	k1gInSc1	pot
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
žlázy	žláza	k1gFnPc1	žláza
jsou	být	k5eAaImIp3nP	být
speciální	speciální	k2eAgFnPc1d1	speciální
-	-	kIx~	-
prsní	prsní	k2eAgFnPc1d1	prsní
žlázy	žláza	k1gFnPc1	žláza
produkující	produkující	k2eAgFnSc4d1	produkující
mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
samice	samice	k1gFnSc1	samice
krmí	krmit	k5eAaImIp3nS	krmit
svá	svůj	k3xOyFgNnPc4	svůj
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
dýchají	dýchat	k5eAaImIp3nP	dýchat
pomocí	pomocí	k7c2	pomocí
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
hrudní	hrudní	k2eAgInSc1d1	hrudní
koš	koš	k1gInSc1	koš
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
břišní	břišní	k2eAgFnSc2d1	břišní
dutiny	dutina	k1gFnSc2	dutina
oddělen	oddělit	k5eAaPmNgInS	oddělit
svalovitou	svalovitý	k2eAgFnSc7d1	svalovitá
membránou	membrána	k1gFnSc7	membrána
-	-	kIx~	-
bránicí	bránice	k1gFnSc7	bránice
-	-	kIx~	-
hlavním	hlavní	k2eAgInSc7d1	hlavní
dýchacím	dýchací	k2eAgInSc7d1	dýchací
svalem	sval	k1gInSc7	sval
<g/>
.	.	kIx.	.
</s>
<s>
Srdce	srdce	k1gNnSc1	srdce
savců	savec	k1gMnPc2	savec
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
komory	komora	k1gFnPc4	komora
<g/>
,	,	kIx,	,
okysličená	okysličený	k2eAgFnSc1d1	okysličená
a	a	k8xC	a
neokysličená	okysličený	k2eNgFnSc1d1	neokysličená
krev	krev	k1gFnSc1	krev
jsou	být	k5eAaImIp3nP	být
dokonale	dokonale	k6eAd1	dokonale
odděleny	oddělit	k5eAaPmNgInP	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Dusíkatý	dusíkatý	k2eAgInSc1d1	dusíkatý
odpad	odpad	k1gInSc1	odpad
je	být	k5eAaImIp3nS	být
vylučován	vylučovat	k5eAaImNgInS	vylučovat
močí	moč	k1gFnSc7	moč
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
savců	savec	k1gMnPc2	savec
rodí	rodit	k5eAaImIp3nS	rodit
živá	živý	k2eAgNnPc4d1	živé
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
zástupci	zástupce	k1gMnPc7	zástupce
řádu	řád	k1gInSc2	řád
ptakořitních	ptakořitní	k2eAgMnPc2d1	ptakořitní
-	-	kIx~	-
ptakopysk	ptakopysk	k1gMnSc1	ptakopysk
a	a	k8xC	a
ježura	ježura	k1gFnSc1	ježura
australská	australský	k2eAgFnSc1d1	australská
-	-	kIx~	-
kteří	který	k3yIgMnPc1	který
kladou	klást	k5eAaImIp3nP	klást
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
savci	savec	k1gMnPc1	savec
mají	mít	k5eAaImIp3nP	mít
placentu	placenta	k1gFnSc4	placenta
<g/>
,	,	kIx,	,
skrz	skrz	k7c4	skrz
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
plod	plod	k1gInSc1	plod
vyživován	vyživován	k2eAgInSc1d1	vyživován
<g/>
.	.	kIx.	.
</s>
<s>
Vačnatci	vačnatec	k1gMnPc1	vačnatec
ovšem	ovšem	k9	ovšem
mají	mít	k5eAaImIp3nP	mít
fetální	fetální	k2eAgNnSc4d1	fetální
stádium	stádium	k1gNnSc4	stádium
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnPc1d1	krátká
a	a	k8xC	a
rodí	rodit	k5eAaImIp3nP	rodit
nevyvinutá	vyvinutý	k2eNgNnPc1d1	nevyvinuté
mláďata	mládě	k1gNnPc1	mládě
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
hledají	hledat	k5eAaImIp3nP	hledat
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
matčina	matčin	k2eAgInSc2d1	matčin
vaku	vak	k1gInSc2	vak
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přisají	přisát	k5eAaPmIp3nP	přisát
k	k	k7c3	k
bradavce	bradavka	k1gFnSc3	bradavka
a	a	k8xC	a
tam	tam	k6eAd1	tam
dokončí	dokončit	k5eAaPmIp3nP	dokončit
svůj	svůj	k3xOyFgInSc4	svůj
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
stavbu	stavba	k1gFnSc4	stavba
těla	tělo	k1gNnSc2	tělo
shodnou	shodnout	k5eAaPmIp3nP	shodnout
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
ostatních	ostatní	k2eAgInPc2d1	ostatní
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
trup	trup	k1gInSc4	trup
(	(	kIx(	(
<g/>
obsahující	obsahující	k2eAgFnSc4d1	obsahující
dutinu	dutina	k1gFnSc4	dutina
hrudní	hrudní	k2eAgFnSc4d1	hrudní
a	a	k8xC	a
dutinu	dutina	k1gFnSc4	dutina
břišní	břišní	k2eAgFnSc4d1	břišní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
paže	paže	k1gFnPc1	paže
s	s	k7c7	s
rukama	ruka	k1gFnPc7	ruka
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
biologických	biologický	k2eAgFnPc2d1	biologická
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
zdravotníci	zdravotník	k1gMnPc1	zdravotník
<g/>
,	,	kIx,	,
ortopedové	ortoped	k1gMnPc1	ortoped
<g/>
,	,	kIx,	,
fyzioterapeuti	fyzioterapeut	k1gMnPc1	fyzioterapeut
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
sestry	sestra	k1gFnPc1	sestra
a	a	k8xC	a
studenti	student	k1gMnPc1	student
medicíny	medicína	k1gFnSc2	medicína
učí	učit	k5eAaImIp3nP	učit
makroskopickou	makroskopický	k2eAgFnSc4d1	makroskopická
a	a	k8xC	a
mikroskopickou	mikroskopický	k2eAgFnSc4d1	mikroskopická
anatomii	anatomie	k1gFnSc4	anatomie
na	na	k7c6	na
anatomických	anatomický	k2eAgInPc6d1	anatomický
modelech	model	k1gInPc6	model
<g/>
,	,	kIx,	,
kostrách	kostra	k1gFnPc6	kostra
<g/>
,	,	kIx,	,
diagramech	diagram	k1gInPc6	diagram
<g/>
,	,	kIx,	,
fotografiích	fotografia	k1gFnPc6	fotografia
<g/>
,	,	kIx,	,
učebnicích	učebnice	k1gFnPc6	učebnice
<g/>
,	,	kIx,	,
na	na	k7c6	na
přednáškách	přednáška	k1gFnPc6	přednáška
a	a	k8xC	a
praktických	praktický	k2eAgNnPc6d1	praktické
cvičeních	cvičení	k1gNnPc6	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
medicínských	medicínský	k2eAgInPc2d1	medicínský
oborů	obor	k1gInPc2	obor
učí	učit	k5eAaImIp3nP	učit
makroskopickou	makroskopický	k2eAgFnSc4d1	makroskopická
anatomii	anatomie	k1gFnSc4	anatomie
skrz	skrz	k7c4	skrz
praktickou	praktický	k2eAgFnSc4d1	praktická
zkušenost	zkušenost	k1gFnSc4	zkušenost
-	-	kIx~	-
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
pitev	pitva	k1gFnPc2	pitva
a	a	k8xC	a
zkoumáním	zkoumání	k1gNnSc7	zkoumání
mrtvých	mrtvý	k2eAgNnPc2d1	mrtvé
těl	tělo	k1gNnPc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Praktické	praktický	k2eAgNnSc1d1	praktické
studium	studium	k1gNnSc1	studium
mikroskopické	mikroskopický	k2eAgFnSc2d1	mikroskopická
anatomie	anatomie	k1gFnSc2	anatomie
(	(	kIx(	(
<g/>
histologie	histologie	k1gFnSc1	histologie
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
zkoumání	zkoumání	k1gNnSc2	zkoumání
histologických	histologický	k2eAgInPc2d1	histologický
preparátů	preparát	k1gInPc2	preparát
pod	pod	k7c7	pod
mikroskopem	mikroskop	k1gInSc7	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
fyziologie	fyziologie	k1gFnSc2	fyziologie
a	a	k8xC	a
biochemie	biochemie	k1gFnSc2	biochemie
jsou	být	k5eAaImIp3nP	být
pomocné	pomocný	k2eAgFnPc1d1	pomocná
vědy	věda	k1gFnPc1	věda
základní	základní	k2eAgFnSc2d1	základní
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
vyučovány	vyučovat	k5eAaImNgFnP	vyučovat
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Anatomii	anatomie	k1gFnSc4	anatomie
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
lze	lze	k6eAd1	lze
studovat	studovat	k5eAaImF	studovat
systematicky	systematicky	k6eAd1	systematicky
nebo	nebo	k8xC	nebo
topograficky	topograficky	k6eAd1	topograficky
<g/>
.	.	kIx.	.
</s>
<s>
Topografické	topografický	k2eAgNnSc1d1	topografické
studium	studium	k1gNnSc1	studium
lidské	lidský	k2eAgFnSc2d1	lidská
anatomie	anatomie	k1gFnSc2	anatomie
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
zkoumání	zkoumání	k1gNnSc3	zkoumání
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hlavy	hlava	k1gFnSc2	hlava
nebo	nebo	k8xC	nebo
hrudi	hruď	k1gFnSc2	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Systematická	systematický	k2eAgFnSc1d1	systematická
anatomie	anatomie	k1gFnSc1	anatomie
studuje	studovat	k5eAaImIp3nS	studovat
orgánové	orgánový	k2eAgFnSc2d1	orgánová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nervovou	nervový	k2eAgFnSc4d1	nervová
či	či	k8xC	či
dýchací	dýchací	k2eAgFnSc4d1	dýchací
<g/>
.	.	kIx.	.
</s>
<s>
Dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
znalost	znalost	k1gFnSc4	znalost
lidské	lidský	k2eAgFnSc2d1	lidská
anatomie	anatomie	k1gFnSc2	anatomie
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
obory	obor	k1gInPc4	obor
lékařské	lékařský	k2eAgInPc4d1	lékařský
<g/>
,	,	kIx,	,
chirurgické	chirurgický	k2eAgInPc4d1	chirurgický
a	a	k8xC	a
diagnostické	diagnostický	k2eAgInPc4d1	diagnostický
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
histopatologie	histopatologie	k1gFnSc1	histopatologie
či	či	k8xC	či
radiologie	radiologie	k1gFnSc1	radiologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Akademičtí	akademický	k2eAgMnPc1d1	akademický
anatomové	anatom	k1gMnPc1	anatom
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
zaměstnáváni	zaměstnávat	k5eAaImNgMnP	zaměstnávat
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
a	a	k8xC	a
zdravotních	zdravotní	k2eAgFnPc6d1	zdravotní
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vyučují	vyučovat	k5eAaImIp3nP	vyučovat
anatomii	anatomie	k1gFnSc4	anatomie
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
orgánových	orgánový	k2eAgInPc2d1	orgánový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
anatomie	anatomie	k1gFnSc1	anatomie
Anatomie	anatomie	k1gFnSc2	anatomie
rostlin	rostlina	k1gFnPc2	rostlina
Anatomie	anatomie	k1gFnSc2	anatomie
živočichů	živočich	k1gMnPc2	živočich
Anatomie	anatomie	k1gFnSc2	anatomie
měkkýšů	měkkýš	k1gMnPc2	měkkýš
Anatomie	anatomie	k1gFnSc2	anatomie
obratlovců	obratlovec	k1gMnPc2	obratlovec
Anatomie	anatomie	k1gFnSc2	anatomie
kočky	kočka	k1gFnSc2	kočka
domácí	domácí	k2eAgFnSc2d1	domácí
Anatomie	anatomie	k1gFnSc2	anatomie
psa	pes	k1gMnSc2	pes
domácího	domácí	k1gMnSc2	domácí
Anatomie	anatomie	k1gFnSc2	anatomie
ptáků	pták	k1gMnPc2	pták
Anatomie	anatomie	k1gFnSc2	anatomie
člověka	člověk	k1gMnSc2	člověk
Patologická	patologický	k2eAgFnSc1d1	patologická
anatomie	anatomie	k1gFnSc1	anatomie
</s>
