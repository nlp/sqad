<s>
Malé	Malé	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
používá	používat	k5eAaImIp3nS
k	k	k7c3
popisu	popis	k1gInSc3
tahů	tah	k1gInPc2
šachovou	šachový	k2eAgFnSc4d1
notaci	notace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Malé	Malé	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
je	být	k5eAaImIp3nS
šachový	šachový	k2eAgInSc4d1
pojem	pojem	k1gInSc4
souhrnně	souhrnně	k6eAd1
označující	označující	k2eAgNnPc1d1
pole	pole	k1gNnPc1
e	e	k0
<g/>
4	#num#	k4
<g/>
,	,	kIx,
e	e	k0
<g/>
5	#num#	k4
<g/>
,	,	kIx,
d	d	k?
<g/>
4	#num#	k4
a	a	k8xC
d	d	k?
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
zaměňuje	zaměňovat	k5eAaImIp3nS
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
se	s	k7c7
samotným	samotný	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechna	všechen	k3xTgNnPc1
pole	pole	k1gNnPc1
malého	malý	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
obsahuje	obsahovat	k5eAaImIp3nS
i	i	k9
velké	velký	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jsou	být	k5eAaImIp3nP
všechna	všechen	k3xTgNnPc1
pole	pole	k1gNnPc1
malého	malý	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
a	a	k8xC
pole	pole	k1gFnSc2
k	k	k7c3
nim	on	k3xPp3gInPc3
přiléhající	přiléhající	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Strategie	strategie	k1gFnSc1
šachu	šach	k1gInSc2
Základní	základní	k2eAgInPc1d1
pojmy	pojem	k1gInPc1
</s>
<s>
Centrum	centrum	k1gNnSc1
•	•	k?
Hodnota	hodnota	k1gFnSc1
šachových	šachový	k2eAgInPc2d1
kamenů	kámen	k1gInPc2
•	•	k?
Hypermodernismus	hypermodernismus	k1gInSc1
•	•	k?
Malé	Malé	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
•	•	k?
Opozice	opozice	k1gFnSc1
•	•	k?
Plán	plán	k1gInSc1
hry	hra	k1gFnSc2
•	•	k?
Velké	velký	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Fáze	fáze	k1gFnSc1
partie	partie	k1gFnSc1
</s>
<s>
Zahájení	zahájení	k1gNnSc1
•	•	k?
Střední	střední	k2eAgFnSc1d1
hra	hra	k1gFnSc1
•	•	k?
Koncovka	koncovka	k1gFnSc1
Faktory	faktor	k1gInPc4
pozice	pozice	k1gFnSc2
</s>
<s>
Dvojice	dvojice	k1gFnSc1
střelců	střelec	k1gMnPc2
•	•	k?
Opěrný	opěrný	k2eAgInSc4d1
bod	bod	k1gInSc4
•	•	k?
Pěšcová	pěšcový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
•	•	k?
Postavení	postavení	k1gNnSc2
králů	král	k1gMnPc2
•	•	k?
Prostor	prostor	k1gInSc1
•	•	k?
Volný	volný	k2eAgInSc1d1
sloupec	sloupec	k1gInSc1
•	•	k?
Vývin	vývin	k1gInSc1
figur	figura	k1gFnPc2
Strategické	strategický	k2eAgInPc1d1
postupy	postup	k1gInPc1
</s>
<s>
Blokáda	blokáda	k1gFnSc1
•	•	k?
Lavírování	lavírování	k1gNnPc2
•	•	k?
Likvidace	likvidace	k1gFnSc2
slabin	slabina	k1gFnPc2
•	•	k?
Manévrování	manévrování	k1gNnSc6
•	•	k?
Minoritní	minoritní	k2eAgInSc1d1
útok	útok	k1gInSc1
•	•	k?
Oběť	oběť	k1gFnSc1
•	•	k?
Opačné	opačný	k2eAgFnPc4d1
rošády	rošáda	k1gFnPc4
•	•	k?
Profylaxe	profylaxe	k1gFnSc1
•	•	k?
Překrytí	překrytí	k1gNnSc2
•	•	k?
Útok	útok	k1gInSc1
na	na	k7c4
krále	král	k1gMnPc4
•	•	k?
Výměna	výměna	k1gFnSc1
•	•	k?
Využití	využití	k1gNnSc2
slabé	slabý	k2eAgFnSc2d1
sedmé	sedmý	k4xOgFnSc2
řady	řada	k1gFnSc2
•	•	k?
Zjednodušení	zjednodušení	k1gNnSc1
pozice	pozice	k1gFnSc1
Velcí	velký	k2eAgMnPc1d1
stratégové	stratég	k1gMnPc1
</s>
<s>
Aaron	Aaron	k1gMnSc1
Nimcovič	Nimcovič	k1gMnSc1
•	•	k?
Savielly	Saviella	k1gFnSc2
Tartakower	Tartakower	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Réti	Rét	k1gFnSc2
•	•	k?
José	Josá	k1gFnSc2
Raúl	Raúl	k1gMnSc1
Capablanca	Capablanca	k1gMnSc1
•	•	k?
Vasilij	Vasilij	k1gFnSc1
Smyslov	Smyslov	k1gInSc1
•	•	k?
Anatolij	Anatolij	k1gFnSc1
Karpov	Karpovo	k1gNnPc2
•	•	k?
Luděk	Luděk	k1gMnSc1
Pachman	Pachman	k1gMnSc1
•	•	k?
Vlastimil	Vlastimil	k1gMnSc1
Hort	Hort	k1gMnSc1
</s>
