<p>
<s>
Matrix	Matrix	k1gInSc1	Matrix
je	být	k5eAaImIp3nS	být
kultovní	kultovní	k2eAgFnSc4d1	kultovní
sci-fi	scii	k1gFnSc4	sci-fi
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
ze	z	k7c2	z
série	série	k1gFnSc2	série
tří	tři	k4xCgInPc2	tři
kultovních	kultovní	k2eAgInPc2d1	kultovní
filmů	film	k1gInPc2	film
série	série	k1gFnSc1	série
Matrix	Matrix	k1gInSc1	Matrix
<g/>
,	,	kIx,	,
napsaných	napsaný	k2eAgNnPc2d1	napsané
a	a	k8xC	a
režírovaných	režírovaný	k2eAgNnPc2d1	režírované
sourozenci	sourozenec	k1gMnSc3	sourozenec
Larrym	Larrym	k1gInSc4	Larrym
(	(	kIx(	(
<g/>
Lanou	laný	k2eAgFnSc4d1	laná
<g/>
)	)	kIx)	)
a	a	k8xC	a
Andym	Andym	k1gInSc1	Andym
(	(	kIx(	(
<g/>
Lilly	Lilla	k1gFnPc1	Lilla
<g/>
)	)	kIx)	)
Wachovskými	Wachovský	k2eAgMnPc7d1	Wachovský
<g/>
.	.	kIx.	.
</s>
<s>
Zahráli	zahrát	k5eAaPmAgMnP	zahrát
si	se	k3xPyFc3	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
například	například	k6eAd1	například
Keanu	Kean	k1gMnSc3	Kean
Reeves	Reevesa	k1gFnPc2	Reevesa
<g/>
,	,	kIx,	,
Laurence	Laurence	k1gFnSc1	Laurence
Fishburne	Fishburn	k1gInSc5	Fishburn
<g/>
,	,	kIx,	,
Carrie-Anne	Carrie-Ann	k1gInSc5	Carrie-Ann
Moss	Moss	k1gInSc1	Moss
nebo	nebo	k8xC	nebo
Hugo	Hugo	k1gMnSc1	Hugo
Weaving	Weaving	k1gInSc1	Weaving
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
popisuje	popisovat	k5eAaImIp3nS	popisovat
svět	svět	k1gInSc4	svět
v	v	k7c6	v
Matrixu	Matrix	k1gInSc6	Matrix
<g/>
,	,	kIx,	,
rozsáhlém	rozsáhlý	k2eAgInSc6d1	rozsáhlý
počítačovém	počítačový	k2eAgInSc6d1	počítačový
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
jsou	být	k5eAaImIp3nP	být
připojeni	připojen	k2eAgMnPc1d1	připojen
lidé	člověk	k1gMnPc1	člověk
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
svůj	svůj	k3xOyFgInSc4	svůj
virtuální	virtuální	k2eAgInSc4d1	virtuální
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
mozek	mozek	k1gInSc1	mozek
je	být	k5eAaImIp3nS	být
napojen	napojit	k5eAaPmNgInS	napojit
do	do	k7c2	do
Matrixu	Matrix	k1gInSc2	Matrix
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
neuvědomují	uvědomovat	k5eNaImIp3nP	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nežijí	žít	k5eNaImIp3nP	žít
skutečný	skutečný	k2eAgInSc4d1	skutečný
život	život	k1gInSc4	život
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gInPc3	on
pouštěna	pouštět	k5eAaImNgFnS	pouštět
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
virtuální	virtuální	k2eAgFnSc1d1	virtuální
realita	realita	k1gFnSc1	realita
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
takto	takto	k6eAd1	takto
nevědomky	nevědomky	k6eAd1	nevědomky
využíváni	využívat	k5eAaImNgMnP	využívat
stroji	stroj	k1gInPc7	stroj
s	s	k7c7	s
umělou	umělý	k2eAgFnSc7d1	umělá
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
převzaly	převzít	k5eAaPmAgFnP	převzít
dominanci	dominance	k1gFnSc4	dominance
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
efektivně	efektivně	k6eAd1	efektivně
chovají	chovat	k5eAaImIp3nP	chovat
a	a	k8xC	a
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
lidstvo	lidstvo	k1gNnSc4	lidstvo
jen	jen	k9	jen
pro	pro	k7c4	pro
energii	energie	k1gFnSc4	energie
z	z	k7c2	z
lidských	lidský	k2eAgNnPc2d1	lidské
těl	tělo	k1gNnPc2	tělo
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
fungují	fungovat	k5eAaImIp3nP	fungovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
mnoho	mnoho	k4c4	mnoho
odkazů	odkaz	k1gInPc2	odkaz
jak	jak	k6eAd1	jak
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
filozofické	filozofický	k2eAgFnPc4d1	filozofická
a	a	k8xC	a
náboženské	náboženský	k2eAgFnPc4d1	náboženská
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c4	na
hackerskou	hackerský	k2eAgFnSc4d1	hackerská
a	a	k8xC	a
kyberpunkovou	kyberpunkový	k2eAgFnSc4d1	kyberpunková
subkulturu	subkultura	k1gFnSc4	subkultura
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
proslul	proslout	k5eAaPmAgInS	proslout
také	také	k9	také
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
vynikajícímu	vynikající	k2eAgNnSc3d1	vynikající
zpracování	zpracování	k1gNnSc3	zpracování
soubojů	souboj	k1gInPc2	souboj
v	v	k7c6	v
hongkongském	hongkongský	k2eAgInSc6d1	hongkongský
stylu	styl	k1gInSc6	styl
a	a	k8xC	a
inspirací	inspirace	k1gFnSc7	inspirace
v	v	k7c6	v
japonském	japonský	k2eAgNnSc6d1	Japonské
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
studií	studie	k1gFnPc2	studie
Warner	Warnra	k1gFnPc2	Warnra
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
a	a	k8xC	a
australských	australský	k2eAgFnPc2d1	australská
Village	Village	k1gFnPc2	Village
Roadshow	Roadshow	k1gMnSc1	Roadshow
Pictures	Pictures	k1gMnSc1	Pictures
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
zejména	zejména	k9	zejména
v	v	k7c6	v
australském	australský	k2eAgNnSc6d1	Australské
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Série	série	k1gFnSc1	série
Matrix	Matrix	k1gInSc1	Matrix
==	==	k?	==
</s>
</p>
<p>
<s>
Matrix	Matrix	k1gInSc1	Matrix
vydělal	vydělat	k5eAaPmAgInS	vydělat
171	[number]	k4	171
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
jen	jen	k9	jen
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
460	[number]	k4	460
milionů	milion	k4xCgInPc2	milion
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
.	.	kIx.	.
</s>
<s>
Příliš	příliš	k6eAd1	příliš
se	se	k3xPyFc4	se
neočekávalo	očekávat	k5eNaImAgNnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
takto	takto	k6eAd1	takto
nevšední	všední	k2eNgInSc4d1	nevšední
film	film	k1gInSc4	film
získá	získat	k5eAaPmIp3nS	získat
davy	dav	k1gInPc4	dav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nečekaný	čekaný	k2eNgInSc1d1	nečekaný
úspěch	úspěch	k1gInSc1	úspěch
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
natočení	natočení	k1gNnSc3	natočení
dalších	další	k2eAgInPc2d1	další
dvou	dva	k4xCgInPc2	dva
dílů	díl	k1gInPc2	díl
<g/>
:	:	kIx,	:
Matrix	Matrix	k1gInSc1	Matrix
Reloaded	Reloaded	k1gInSc1	Reloaded
a	a	k8xC	a
Matrix	Matrix	k1gInSc1	Matrix
Revolutions	Revolutionsa	k1gFnPc2	Revolutionsa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Wachowských	Wachowská	k1gFnPc2	Wachowská
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
trilogie	trilogie	k1gFnSc1	trilogie
zamýšlena	zamýšlet	k5eAaImNgFnS	zamýšlet
již	již	k6eAd1	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Trvalo	trvat	k5eAaImAgNnS	trvat
mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
přepisoval	přepisovat	k5eAaImAgInS	přepisovat
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
filmy	film	k1gInPc1	film
začaly	začít	k5eAaPmAgInP	začít
současně	současně	k6eAd1	současně
točit	točit	k5eAaImF	točit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
daly	dát	k5eAaPmAgFnP	dát
dohromady	dohromady	k6eAd1	dohromady
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jednolitý	jednolitý	k2eAgInSc1d1	jednolitý
příběh	příběh	k1gInSc1	příběh
uměle	uměle	k6eAd1	uměle
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
premiéru	premiéra	k1gFnSc4	premiéra
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokračování	pokračování	k1gNnSc1	pokračování
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
kvalit	kvalita	k1gFnPc2	kvalita
ani	ani	k8xC	ani
koncepční	koncepční	k2eAgFnSc2d1	koncepční
hloubky	hloubka	k1gFnSc2	hloubka
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
stále	stále	k6eAd1	stále
vedou	vést	k5eAaImIp3nP	vést
debaty	debata	k1gFnPc4	debata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vydán	vydat	k5eAaPmNgInS	vydat
byl	být	k5eAaImAgInS	být
také	také	k9	také
Animatrix	Animatrix	k1gInSc1	Animatrix
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
devíti	devět	k4xCc2	devět
animovaných	animovaný	k2eAgFnPc2d1	animovaná
povídek	povídka	k1gFnPc2	povídka
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
kreslířském	kreslířský	k2eAgInSc6d1	kreslířský
stylu	styl	k1gInSc6	styl
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
autory	autor	k1gMnPc4	autor
filmové	filmový	k2eAgFnSc2d1	filmová
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
.	.	kIx.	.
</s>
<s>
Sourozenci	sourozenec	k1gMnPc1	sourozenec
Wachowští	Wachowský	k2eAgMnPc1d1	Wachowský
sice	sice	k8xC	sice
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
projekt	projekt	k1gInSc4	projekt
dohlíželi	dohlížet	k5eAaImAgMnP	dohlížet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
napsali	napsat	k5eAaPmAgMnP	napsat
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgMnPc4	čtyři
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
žádný	žádný	k3yNgInSc4	žádný
nerežírovali	režírovat	k5eNaImAgMnP	režírovat
<g/>
,	,	kIx,	,
sezvali	sezvat	k5eAaPmAgMnP	sezvat
totiž	totiž	k9	totiž
významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
kreslené	kreslený	k2eAgFnSc2d1	kreslená
animace	animace	k1gFnSc2	animace
a	a	k8xC	a
nechali	nechat	k5eAaPmAgMnP	nechat
jim	on	k3xPp3gMnPc3	on
relativně	relativně	k6eAd1	relativně
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přenesli	přenést	k5eAaPmAgMnP	přenést
své	svůj	k3xOyFgFnPc4	svůj
vize	vize	k1gFnPc4	vize
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
nakreslen	nakreslit	k5eAaPmNgInS	nakreslit
zcela	zcela	k6eAd1	zcela
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
něčem	něco	k3yInSc6	něco
jiném	jiný	k2eAgNnSc6d1	jiné
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
ale	ale	k8xC	ale
dokreslují	dokreslovat	k5eAaImIp3nP	dokreslovat
příběh	příběh	k1gInSc4	příběh
Matrixu	Matrix	k1gInSc2	Matrix
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
svět	svět	k1gInSc1	svět
jako	jako	k8xS	jako
takový	takový	k3xDgInSc4	takový
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
části	část	k1gFnPc1	část
byly	být	k5eAaImAgFnP	být
vydané	vydaný	k2eAgInPc1d1	vydaný
na	na	k7c6	na
oficiální	oficiální	k2eAgFnSc6d1	oficiální
stránce	stránka	k1gFnSc6	stránka
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
promítán	promítat	k5eAaImNgInS	promítat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
filmem	film	k1gInSc7	film
od	od	k7c2	od
Warner	Warnero	k1gNnPc2	Warnero
Bros	Brosa	k1gFnPc2	Brosa
Dreamcatcher	Dreamcatchra	k1gFnPc2	Dreamcatchra
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
až	až	k6eAd1	až
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
samostatného	samostatný	k2eAgInSc2d1	samostatný
DVD	DVD	kA	DVD
Animatrix	Animatrix	k1gInSc1	Animatrix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
sérii	série	k1gFnSc3	série
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
tři	tři	k4xCgFnPc4	tři
počítačové	počítačový	k2eAgFnPc4d1	počítačová
hry	hra	k1gFnPc4	hra
<g/>
:	:	kIx,	:
Enter	Enter	k1gInSc1	Enter
the	the	k?	the
Matrix	Matrix	k1gInSc1	Matrix
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zobrazovala	zobrazovat	k5eAaImAgFnS	zobrazovat
události	událost	k1gFnPc4	událost
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
filmem	film	k1gInSc7	film
a	a	k8xC	a
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
navíc	navíc	k6eAd1	navíc
krátké	krátký	k2eAgInPc4d1	krátký
hrané	hraný	k2eAgInPc4d1	hraný
filmečky	filmeček	k1gInPc4	filmeček
natočené	natočený	k2eAgInPc4d1	natočený
přímo	přímo	k6eAd1	přímo
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
<g/>
;	;	kIx,	;
The	The	k1gFnSc1	The
Matrix	Matrix	k1gInSc1	Matrix
Online	Onlin	k1gInSc5	Onlin
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MMORPG	MMORPG	kA	MMORPG
ve	v	k7c6	v
které	který	k3yIgFnPc1	který
příběh	příběh	k1gInSc1	příběh
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
po	po	k7c4	po
Matrix	Matrix	k1gInSc4	Matrix
Revolutions	Revolutions	k1gInSc4	Revolutions
<g/>
;	;	kIx,	;
a	a	k8xC	a
The	The	k1gFnSc1	The
Matrix	Matrix	k1gInSc1	Matrix
<g/>
:	:	kIx,	:
Path	Path	k1gMnSc1	Path
of	of	k?	of
Neo	Neo	k1gMnSc1	Neo
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
Vyvoleného	vyvolený	k1gMnSc4	vyvolený
a	a	k8xC	a
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
ocitá	ocitat	k5eAaImIp3nS	ocitat
během	během	k7c2	během
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
komiksů	komiks	k1gInPc2	komiks
odehrávajících	odehrávající	k2eAgFnPc2d1	odehrávající
se	se	k3xPyFc4	se
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Matrixu	Matrix	k1gInSc2	Matrix
nakreslených	nakreslený	k2eAgFnPc2d1	nakreslená
různými	různý	k2eAgFnPc7d1	různá
osobnostmi	osobnost	k1gFnPc7	osobnost
z	z	k7c2	z
komiksového	komiksový	k2eAgInSc2d1	komiksový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
také	také	k6eAd1	také
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
tištěných	tištěný	k2eAgNnPc2d1	tištěné
vydání	vydání	k1gNnPc2	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
kultovní	kultovní	k2eAgInSc1d1	kultovní
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
během	běh	k1gInSc7	běh
let	léto	k1gNnPc2	léto
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
nejen	nejen	k6eAd1	nejen
do	do	k7c2	do
filmové	filmový	k2eAgFnSc2d1	filmová
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
využil	využít	k5eAaPmAgMnS	využít
i	i	k9	i
různých	různý	k2eAgNnPc2d1	různé
jiných	jiný	k2eAgNnPc2d1	jiné
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
forem	forma	k1gFnPc2	forma
vyjádření	vyjádření	k1gNnSc2	vyjádření
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
velkého	velký	k2eAgInSc2d1	velký
a	a	k8xC	a
provázaného	provázaný	k2eAgInSc2d1	provázaný
světa	svět	k1gInSc2	svět
Matrixu	Matrix	k1gInSc2	Matrix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
filmu	film	k1gInSc2	film
==	==	k?	==
</s>
</p>
<p>
<s>
Programátor	programátor	k1gMnSc1	programátor
jménem	jméno	k1gNnSc7	jméno
Thomas	Thomas	k1gMnSc1	Thomas
A.	A.	kA	A.
Anderson	Anderson	k1gMnSc1	Anderson
(	(	kIx(	(
<g/>
Keanu	Kean	k1gInSc2	Kean
Reeves	Reeves	k1gInSc1	Reeves
<g/>
)	)	kIx)	)
vede	vést	k5eAaImIp3nS	vést
tajně	tajně	k6eAd1	tajně
druhý	druhý	k4xOgInSc4	druhý
život	život	k1gInSc4	život
jako	jako	k8xC	jako
obávaný	obávaný	k2eAgMnSc1d1	obávaný
hacker	hacker	k1gMnSc1	hacker
Neo	Neo	k1gMnSc1	Neo
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
podivných	podivný	k2eAgFnPc2d1	podivná
událostí	událost	k1gFnPc2	událost
jej	on	k3xPp3gInSc4	on
přivádí	přivádět	k5eAaImIp3nS	přivádět
až	až	k6eAd1	až
k	k	k7c3	k
setkání	setkání	k1gNnSc3	setkání
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
lidí	člověk	k1gMnPc2	člověk
vedenou	vedený	k2eAgFnSc4d1	vedená
Morpheem	Morpheus	k1gMnSc7	Morpheus
(	(	kIx(	(
<g/>
Laurence	Laurence	k1gFnSc1	Laurence
Fishburne	Fishburn	k1gInSc5	Fishburn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
nabízí	nabízet	k5eAaImIp3nS	nabízet
možnost	možnost	k1gFnSc4	možnost
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
skutečně	skutečně	k6eAd1	skutečně
je	být	k5eAaImIp3nS	být
onen	onen	k3xDgInSc1	onen
Matrix	Matrix	k1gInSc1	Matrix
<g/>
.	.	kIx.	.
</s>
<s>
Neo	Neo	k?	Neo
je	být	k5eAaImIp3nS	být
vytažen	vytáhnout	k5eAaPmNgInS	vytáhnout
z	z	k7c2	z
falešné	falešný	k2eAgFnSc2d1	falešná
reality	realita	k1gFnSc2	realita
do	do	k7c2	do
reálného	reálný	k2eAgInSc2d1	reálný
zničeného	zničený	k2eAgInSc2d1	zničený
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nepíše	psát	k5eNaImIp3nS	psát
rok	rok	k1gInSc1	rok
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
umělém	umělý	k2eAgInSc6d1	umělý
Matrixu	Matrix	k1gInSc6	Matrix
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
2199	[number]	k4	2199
(	(	kIx(	(
<g/>
Morpheus	Morpheus	k1gMnSc1	Morpheus
si	se	k3xPyFc3	se
tímto	tento	k3xDgNnSc7	tento
není	být	k5eNaImIp3nS	být
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
;	;	kIx,	;
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
třetího	třetí	k4xOgInSc2	třetí
dílu	díl	k1gInSc2	díl
toto	tento	k3xDgNnSc4	tento
nepřímo	přímo	k6eNd1	přímo
vyvracejí	vyvracet	k5eAaImIp3nP	vyvracet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidstvo	lidstvo	k1gNnSc1	lidstvo
vede	vést	k5eAaImIp3nS	vést
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
inteligentními	inteligentní	k2eAgInPc7d1	inteligentní
stroji	stroj	k1gInPc7	stroj
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
samo	sám	k3xTgNnSc1	sám
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
bojích	boj	k1gInPc6	boj
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vypálit	vypálit	k5eAaPmF	vypálit
oblohu	obloha	k1gFnSc4	obloha
a	a	k8xC	a
odříznout	odříznout	k5eAaPmF	odříznout
tak	tak	k9	tak
stroje	stroj	k1gInPc4	stroj
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc1	stroj
reagovaly	reagovat	k5eAaBmAgInP	reagovat
zotročením	zotročení	k1gNnSc7	zotročení
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
je	on	k3xPp3gFnPc4	on
uzavírat	uzavírat	k5eAaPmF	uzavírat
do	do	k7c2	do
schránek	schránka	k1gFnPc2	schránka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jim	on	k3xPp3gMnPc3	on
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
byla	být	k5eAaImAgFnS	být
pouštěna	pouštěn	k2eAgFnSc1d1	pouštěna
umělá	umělý	k2eAgFnSc1d1	umělá
realita	realita	k1gFnSc1	realita
–	–	k?	–
Matrix	Matrix	k1gInSc1	Matrix
–	–	k?	–
iluzorní	iluzorní	k2eAgInSc4d1	iluzorní
svět	svět	k1gInSc4	svět
zobrazující	zobrazující	k2eAgInSc4d1	zobrazující
lidskou	lidský	k2eAgFnSc4d1	lidská
společnost	společnost	k1gFnSc4	společnost
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
lidé	člověk	k1gMnPc1	člověk
zdánlivě	zdánlivě	k6eAd1	zdánlivě
žijí	žít	k5eAaImIp3nP	žít
své	svůj	k3xOyFgInPc4	svůj
životy	život	k1gInPc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
drženi	držet	k5eAaImNgMnP	držet
na	na	k7c6	na
umělé	umělý	k2eAgFnSc6d1	umělá
výživě	výživa	k1gFnSc6	výživa
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
kinetická	kinetický	k2eAgFnSc1d1	kinetická
<g/>
,	,	kIx,	,
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
a	a	k8xC	a
tepelná	tepelný	k2eAgFnSc1d1	tepelná
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
pro	pro	k7c4	pro
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Pěstováním	pěstování	k1gNnSc7	pěstování
lidí	člověk	k1gMnPc2	člověk
stroje	stroj	k1gInPc1	stroj
získaly	získat	k5eAaPmAgFnP	získat
nevyčerpatelný	vyčerpatelný	k2eNgMnSc1d1	nevyčerpatelný
nekonečně	konečně	k6eNd1	konečně
se	se	k3xPyFc4	se
množící	množící	k2eAgInSc1d1	množící
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Morpheus	Morpheus	k1gInSc1	Morpheus
s	s	k7c7	s
organizovanou	organizovaný	k2eAgFnSc7d1	organizovaná
skupinou	skupina	k1gFnSc7	skupina
lidí	člověk	k1gMnPc2	člověk
probouzí	probouzet	k5eAaImIp3nP	probouzet
lidi	člověk	k1gMnPc4	člověk
stále	stále	k6eAd1	stále
připojené	připojený	k2eAgInPc1d1	připojený
na	na	k7c4	na
Matrix	Matrix	k1gInSc4	Matrix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Morpheus	Morpheus	k1gMnSc1	Morpheus
odpojil	odpojit	k5eAaPmAgMnS	odpojit
Nea	Nea	k1gFnSc4	Nea
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pevně	pevně	k6eAd1	pevně
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
Vyvolený	vyvolený	k2eAgInSc1d1	vyvolený
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
příchod	příchod	k1gInSc1	příchod
předpovězený	předpovězený	k2eAgInSc1d1	předpovězený
Vědmou	vědma	k1gFnSc7	vědma
má	mít	k5eAaImIp3nS	mít
přinést	přinést	k5eAaPmF	přinést
zkázu	zkáza	k1gFnSc4	zkáza
Matrixu	Matrix	k1gInSc2	Matrix
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
pro	pro	k7c4	pro
lidstvo	lidstvo	k1gNnSc4	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Vyvolený	vyvolený	k1gMnSc1	vyvolený
prý	prý	k9	prý
dokáže	dokázat	k5eAaPmIp3nS	dokázat
Matrix	Matrix	k1gInSc4	Matrix
ovládat	ovládat	k5eAaImF	ovládat
a	a	k8xC	a
podřídit	podřídit	k5eAaPmF	podřídit
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Neo	Neo	k?	Neo
samotný	samotný	k2eAgInSc1d1	samotný
je	být	k5eAaImIp3nS	být
zpočátku	zpočátku	k6eAd1	zpočátku
skeptický	skeptický	k2eAgMnSc1d1	skeptický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Morpheus	Morpheus	k1gMnSc1	Morpheus
jej	on	k3xPp3gInSc4	on
s	s	k7c7	s
neotřesitelným	otřesitelný	k2eNgNnSc7d1	neotřesitelné
přesvědčením	přesvědčení	k1gNnSc7	přesvědčení
učí	učit	k5eAaImIp3nS	učit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
přisvojit	přisvojit	k5eAaPmF	přisvojit
a	a	k8xC	a
překročit	překročit	k5eAaPmF	překročit
pravidla	pravidlo	k1gNnPc4	pravidlo
umělého	umělý	k2eAgInSc2d1	umělý
světa	svět	k1gInSc2	svět
–	–	k?	–
porušovat	porušovat	k5eAaImF	porušovat
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Matrix	Matrix	k1gInSc4	Matrix
zevnitř	zevnitř	k6eAd1	zevnitř
hlídají	hlídat	k5eAaImIp3nP	hlídat
programy	program	k1gInPc1	program
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
agenti	agent	k1gMnPc1	agent
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
neskutečně	skutečně	k6eNd1	skutečně
silní	silný	k2eAgMnPc1d1	silný
<g/>
,	,	kIx,	,
ovládají	ovládat	k5eAaImIp3nP	ovládat
bojová	bojový	k2eAgNnPc1d1	bojové
umění	umění	k1gNnPc1	umění
a	a	k8xC	a
superschopnosti	superschopnost	k1gFnPc1	superschopnost
<g/>
.	.	kIx.	.
</s>
<s>
Neo	Neo	k?	Neo
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
sbližuje	sbližovat	k5eAaImIp3nS	sbližovat
a	a	k8xC	a
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Trinity	Trinit	k1gInPc7	Trinit
(	(	kIx(	(
<g/>
Carrie-Anne	Carrie-Ann	k1gInSc5	Carrie-Ann
Moss	Mossa	k1gFnPc2	Mossa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neo	Neo	k?	Neo
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Vědmou	vědma	k1gFnSc7	vědma
(	(	kIx(	(
<g/>
Gloria	Gloria	k1gFnSc1	Gloria
Foster	Foster	k1gMnSc1	Foster
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dostává	dostávat	k5eAaImIp3nS	dostávat
svému	svůj	k3xOyFgNnSc3	svůj
jménu	jméno	k1gNnSc3	jméno
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
mu	on	k3xPp3gMnSc3	on
jeho	jeho	k3xOp3gInSc2	jeho
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
skupinu	skupina	k1gFnSc4	skupina
zradí	zradit	k5eAaPmIp3nS	zradit
Cypher	Cyphra	k1gFnPc2	Cyphra
(	(	kIx(	(
<g/>
Joe	Joe	k1gFnSc1	Joe
Pantoliano	Pantoliana	k1gFnSc5	Pantoliana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
pohodlnému	pohodlný	k2eAgInSc3d1	pohodlný
nevědomému	vědomý	k2eNgInSc3d1	nevědomý
životu	život	k1gInSc3	život
zpátky	zpátky	k6eAd1	zpátky
v	v	k7c6	v
Matrixu	Matrix	k1gInSc6	Matrix
<g/>
.	.	kIx.	.
</s>
<s>
Morpheus	Morpheus	k1gMnSc1	Morpheus
je	být	k5eAaImIp3nS	být
chycen	chytit	k5eAaPmNgMnS	chytit
agenty	agent	k1gMnPc7	agent
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vymýváním	vymývání	k1gNnSc7	vymývání
mozku	mozek	k1gInSc2	mozek
snaží	snažit	k5eAaImIp3nP	snažit
dostat	dostat	k5eAaPmF	dostat
přístupové	přístupový	k2eAgInPc4d1	přístupový
kódy	kód	k1gInPc4	kód
k	k	k7c3	k
Sionu	Siono	k1gNnSc3	Siono
<g/>
,	,	kIx,	,
poslednímu	poslední	k2eAgNnSc3d1	poslední
lidskému	lidský	k2eAgNnSc3d1	lidské
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
předpovědi	předpověď	k1gFnSc2	předpověď
se	se	k3xPyFc4	se
Neo	Neo	k1gFnSc1	Neo
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
Morphea	Morphe	k2eAgFnSc1d1	Morphe
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Trinity	Trinit	k1gInPc7	Trinit
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Matrixu	Matrix	k1gInSc2	Matrix
a	a	k8xC	a
vysvobodí	vysvobodit	k5eAaPmIp3nS	vysvobodit
Morphea	Morphea	k1gFnSc1	Morphea
z	z	k7c2	z
držení	držení	k1gNnPc2	držení
agentů	agent	k1gMnPc2	agent
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Morpheovi	Morpheův	k2eAgMnPc1d1	Morpheův
a	a	k8xC	a
Trinity	Trinita	k1gFnPc1	Trinita
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nP	podařit
uniknout	uniknout	k5eAaPmF	uniknout
"	"	kIx"	"
<g/>
východem	východ	k1gInSc7	východ
<g/>
"	"	kIx"	"
–	–	k?	–
k	k	k7c3	k
odpojení	odpojení	k1gNnSc3	odpojení
z	z	k7c2	z
Matrixu	Matrix	k1gInSc2	Matrix
používají	používat	k5eAaImIp3nP	používat
telefony	telefon	k1gInPc1	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Neo	Neo	k?	Neo
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
donucen	donutit	k5eAaPmNgMnS	donutit
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
Smithovi	Smith	k1gMnSc3	Smith
<g/>
,	,	kIx,	,
vedoucímu	vedoucí	k1gMnSc3	vedoucí
agentovi	agent	k1gMnSc3	agent
<g/>
.	.	kIx.	.
</s>
<s>
Porazí	porazit	k5eAaPmIp3nS	porazit
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
agenti	agent	k1gMnPc1	agent
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
znovu	znovu	k6eAd1	znovu
vtělit	vtělit	k5eAaPmF	vtělit
do	do	k7c2	do
kterékoli	kterýkoli	k3yIgFnSc2	kterýkoli
osoby	osoba	k1gFnSc2	osoba
v	v	k7c6	v
Matrixu	Matrix	k1gInSc6	Matrix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neo	Neo	k?	Neo
tedy	tedy	k9	tedy
utíká	utíkat	k5eAaImIp3nS	utíkat
pronásledován	pronásledovat	k5eAaImNgInS	pronásledovat
agenty	agens	k1gInPc7	agens
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
najít	najít	k5eAaPmF	najít
jiný	jiný	k2eAgInSc4d1	jiný
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
dostižen	dostihnout	k5eAaPmNgMnS	dostihnout
Smithem	Smith	k1gInSc7	Smith
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
střelen	střelit	k5eAaPmNgMnS	střelit
do	do	k7c2	do
hrudi	hruď	k1gFnSc2	hruď
a	a	k8xC	a
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
ale	ale	k8xC	ale
Trinity	Trinita	k1gFnPc4	Trinita
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc4d1	stojící
u	u	k7c2	u
jeho	on	k3xPp3gNnSc2	on
reálného	reálný	k2eAgNnSc2d1	reálné
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
neschopná	schopný	k2eNgFnSc1d1	neschopná
připustit	připustit	k5eAaPmF	připustit
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
políbí	políbit	k5eAaPmIp3nP	políbit
jeho	jeho	k3xOp3gInPc4	jeho
rty	ret	k1gInPc4	ret
a	a	k8xC	a
šeptá	šeptat	k5eAaImIp3nS	šeptat
mu	on	k3xPp3gMnSc3	on
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
víře	víra	k1gFnSc6	víra
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
moc	moc	k6eAd1	moc
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
jí	on	k3xPp3gFnSc7	on
předpověděla	předpovědět	k5eAaPmAgFnS	předpovědět
vědma	vědma	k1gFnSc1	vědma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
smrti	smrt	k1gFnSc2	smrt
si	se	k3xPyFc3	se
Neo	Neo	k1gMnSc1	Neo
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
mozek	mozek	k1gInSc1	mozek
rozpoznává	rozpoznávat	k5eAaImIp3nS	rozpoznávat
nereálnou	reálný	k2eNgFnSc4d1	nereálná
a	a	k8xC	a
jen	jen	k9	jen
umělou	umělý	k2eAgFnSc4d1	umělá
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
pochopí	pochopit	k5eAaPmIp3nP	pochopit
pravidla	pravidlo	k1gNnPc1	pravidlo
Matrixu	Matrix	k1gInSc2	Matrix
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
ožívá	ožívat	k5eAaImIp3nS	ožívat
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
jako	jako	k9	jako
Vyvolený	vyvolený	k1gMnSc1	vyvolený
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
námahy	námaha	k1gFnSc2	námaha
zničí	zničit	k5eAaPmIp3nS	zničit
agenty	agent	k1gMnPc4	agent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
epilogu	epilog	k1gInSc6	epilog
jim	on	k3xPp3gInPc3	on
slibuje	slibovat	k5eAaImIp3nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povede	vést	k5eAaImIp3nS	vést
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
za	za	k7c2	za
osvobození	osvobození	k1gNnSc2	osvobození
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Matrix	Matrix	k1gInSc1	Matrix
Reloaded	Reloaded	k1gInSc1	Reloaded
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Svět	svět	k1gInSc1	svět
Matrixu	Matrix	k1gInSc2	Matrix
a	a	k8xC	a
skryté	skrytý	k2eAgInPc4d1	skrytý
významy	význam	k1gInPc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
početné	početný	k2eAgNnSc1d1	početné
množství	množství	k1gNnSc1	množství
odkazů	odkaz	k1gInPc2	odkaz
na	na	k7c4	na
historická	historický	k2eAgNnPc4d1	historické
a	a	k8xC	a
literární	literární	k2eAgNnPc4d1	literární
díla	dílo	k1gNnPc4	dílo
včetně	včetně	k7c2	včetně
Platónova	Platónův	k2eAgInSc2d1	Platónův
dialogu	dialog	k1gInSc2	dialog
Ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
Alenky	Alenka	k1gFnPc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
Orwellův	Orwellův	k2eAgInSc4d1	Orwellův
román	román	k1gInSc4	román
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
židovsko-křesťanskou	židovskořesťanský	k2eAgFnSc4d1	židovsko-křesťanská
představu	představa	k1gFnSc4	představa
o	o	k7c4	o
mesiášství	mesiášství	k1gNnSc4	mesiášství
<g/>
,	,	kIx,	,
indickou	indický	k2eAgFnSc4d1	indická
filosofii	filosofie	k1gFnSc4	filosofie
(	(	kIx(	(
<g/>
Matrix	Matrix	k1gInSc1	Matrix
<g/>
:	:	kIx,	:
májá	májá	k1gFnSc1	májá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
gnosticismus	gnosticismus	k1gInSc1	gnosticismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
třeba	třeba	k6eAd1	třeba
díla	dílo	k1gNnSc2	dílo
jako	jako	k8xC	jako
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
od	od	k7c2	od
japonského	japonský	k2eAgMnSc2d1	japonský
režiséra	režisér	k1gMnSc2	režisér
Mamoru	Mamor	k1gInSc2	Mamor
Ošiiho	Ošii	k1gMnSc2	Ošii
<g/>
,	,	kIx,	,
Naušika	Naušika	k1gFnSc1	Naušika
z	z	k7c2	z
Větrného	větrný	k2eAgNnSc2d1	větrné
údolí	údolí	k1gNnSc2	údolí
od	od	k7c2	od
Hayao	Hayao	k6eAd1	Hayao
Miyazakiho	Miyazaki	k1gMnSc4	Miyazaki
nebo	nebo	k8xC	nebo
příběhy	příběh	k1gInPc4	příběh
od	od	k7c2	od
Williama	William	k1gMnSc2	William
Forda	ford	k1gMnSc2	ford
Gibsona	Gibson	k1gMnSc2	Gibson
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Neuromancer	Neuromancer	k1gMnSc1	Neuromancer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komiksová	komiksový	k2eAgFnSc1d1	komiksová
série	série	k1gFnSc1	série
The	The	k1gMnSc1	The
Invisibles	Invisibles	k1gMnSc1	Invisibles
od	od	k7c2	od
Granta	Grant	k1gInSc2	Grant
Morrisona	Morrison	k1gMnSc2	Morrison
zcela	zcela	k6eAd1	zcela
jistě	jistě	k6eAd1	jistě
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
trilogii	trilogie	k1gFnSc4	trilogie
Matrix	Matrix	k1gInSc1	Matrix
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
tematické	tematický	k2eAgFnSc2d1	tematická
a	a	k8xC	a
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
podobnosti	podobnost	k1gFnSc2	podobnost
<g/>
.	.	kIx.	.
</s>
<s>
Morrison	Morrison	k1gMnSc1	Morrison
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wachowští	Wachowský	k1gMnPc1	Wachowský
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
okopírovali	okopírovat	k5eAaPmAgMnP	okopírovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
článku	článek	k1gInSc2	článek
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
popis	popis	k1gInSc1	popis
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
postapokalyptické	postapokalyptický	k2eAgFnSc2d1	postapokalyptická
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
sestrojena	sestrojit	k5eAaPmNgFnS	sestrojit
první	první	k4xOgFnSc1	první
umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
žili	žít	k5eAaImAgMnP	žít
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
roboty	robot	k1gInPc7	robot
v	v	k7c6	v
míru	mír	k1gInSc6	mír
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jak	jak	k6eAd1	jak
začala	začít	k5eAaPmAgFnS	začít
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2090	[number]	k4	2090
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
lidé	člověk	k1gMnPc1	člověk
nechali	nechat	k5eAaPmAgMnP	nechat
zčernat	zčernat	k5eAaPmF	zčernat
nebe	nebe	k1gNnSc4	nebe
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
roboti	robot	k1gMnPc1	robot
neměli	mít	k5eNaImAgMnP	mít
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
však	však	k9	však
zdroj	zdroj	k1gInSc4	zdroj
našli	najít	k5eAaPmAgMnP	najít
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
transportu	transport	k1gInSc2	transport
biologické	biologický	k2eAgFnSc2d1	biologická
elektřiny	elektřina	k1gFnSc2	elektřina
z	z	k7c2	z
živých	živý	k2eAgMnPc2d1	živý
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
baterií	baterie	k1gFnPc2	baterie
a	a	k8xC	a
Města	město	k1gNnSc2	město
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
roboti	robot	k1gMnPc1	robot
si	se	k3xPyFc3	se
doslova	doslova	k6eAd1	doslova
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zastírání	zastírání	k1gNnSc3	zastírání
skutečnosti	skutečnost	k1gFnSc2	skutečnost
slouží	sloužit	k5eAaImIp3nS	sloužit
program	program	k1gInSc1	program
virtuální	virtuální	k2eAgFnSc2d1	virtuální
reality	realita	k1gFnSc2	realita
Matrix	Matrix	k1gInSc1	Matrix
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
vymyšlen	vymyslet	k5eAaPmNgInS	vymyslet
jako	jako	k9	jako
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
nikom	nikdo	k3yNnSc6	nikdo
nebylo	být	k5eNaImAgNnS	být
pácháno	páchán	k2eAgNnSc4d1	pácháno
příkoří	příkoří	k1gNnSc4	příkoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údajném	údajný	k2eAgInSc6d1	údajný
roce	rok	k1gInSc6	rok
2199	[number]	k4	2199
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
už	už	k9	už
jeho	jeho	k3xOp3gFnSc1	jeho
šestá	šestý	k4xOgFnSc1	šestý
verze	verze	k1gFnSc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
zbytky	zbytek	k1gInPc1	zbytek
svobodných	svobodný	k2eAgMnPc2d1	svobodný
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
usadily	usadit	k5eAaPmAgInP	usadit
v	v	k7c6	v
městě	město	k1gNnSc6	město
Siónu	Sión	k1gInSc2	Sión
u	u	k7c2	u
zemského	zemský	k2eAgNnSc2d1	zemské
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skutečný	skutečný	k2eAgInSc1d1	skutečný
svět	svět	k1gInSc1	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
lidé	člověk	k1gMnPc1	člověk
nechali	nechat	k5eAaPmAgMnP	nechat
zčernat	zčernat	k5eAaPmF	zčernat
nebe	nebe	k1gNnSc4	nebe
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
znemožnili	znemožnit	k5eAaPmAgMnP	znemožnit
sluneční	sluneční	k2eAgFnPc4d1	sluneční
reakce	reakce	k1gFnPc4	reakce
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
roboti	robot	k1gMnPc1	robot
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
člověka	člověk	k1gMnSc4	člověk
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
zemského	zemský	k2eAgInSc2d1	zemský
a	a	k8xC	a
bývalá	bývalý	k2eAgNnPc1d1	bývalé
velkoměsta	velkoměsto	k1gNnPc1	velkoměsto
začala	začít	k5eAaPmAgNnP	začít
chátrat	chátrat	k5eAaImF	chátrat
<g/>
.	.	kIx.	.
</s>
<s>
Roboti	robot	k1gMnPc1	robot
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
pány	pan	k1gMnPc7	pan
tvorstva	tvorstvo	k1gNnSc2	tvorstvo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
hodně	hodně	k6eAd1	hodně
scén	scéna	k1gFnPc2	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznášedlo	vznášedlo	k1gNnSc1	vznášedlo
letí	letět	k5eAaImIp3nS	letět
starým	starý	k2eAgInSc7d1	starý
kanalizačním	kanalizační	k2eAgInSc7d1	kanalizační
systémem	systém	k1gInSc7	systém
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
patnáct	patnáct	k4xCc4	patnáct
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
stoky	stoka	k1gFnPc1	stoka
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
vodorovně	vodorovně	k6eAd1	vodorovně
i	i	k9	i
svisle	svisel	k1gInPc1	svisel
jsou	být	k5eAaImIp3nP	být
spojnicemi	spojnice	k1gFnPc7	spojnice
mezi	mezi	k7c7	mezi
nehostinným	hostinný	k2eNgInSc7d1	nehostinný
<g/>
,	,	kIx,	,
temně	temně	k6eAd1	temně
pustým	pustý	k2eAgInSc7d1	pustý
zemským	zemský	k2eAgInSc7d1	zemský
povrchem	povrch	k1gInSc7	povrch
(	(	kIx(	(
<g/>
s	s	k7c7	s
Městem	město	k1gNnSc7	město
strojů	stroj	k1gInPc2	stroj
<g/>
)	)	kIx)	)
a	a	k8xC	a
pohostinnou	pohostinný	k2eAgFnSc7d1	pohostinná
poslední	poslední	k2eAgFnSc7d1	poslední
základnou	základna	k1gFnSc7	základna
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
městem	město	k1gNnSc7	město
Siónem	Sión	k1gInSc7	Sión
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sión	Sión	k1gInSc4	Sión
====	====	k?	====
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
podzemní	podzemní	k2eAgNnSc1d1	podzemní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
lidským	lidský	k2eAgNnSc7d1	lidské
městem	město	k1gNnSc7	město
a	a	k8xC	a
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
poslední	poslední	k2eAgInPc1d1	poslední
zbytky	zbytek	k1gInPc1	zbytek
kdysi	kdysi	k6eAd1	kdysi
většinou	většina	k1gFnSc7	většina
svobodného	svobodný	k2eAgNnSc2d1	svobodné
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vykutáno	vykutat	k5eAaPmNgNnS	vykutat
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
stále	stále	k6eAd1	stále
drží	držet	k5eAaImIp3nS	držet
teplo	teplo	k1gNnSc4	teplo
od	od	k7c2	od
zemského	zemský	k2eAgNnSc2d1	zemské
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
Doků	dok	k1gInPc2	dok
<g/>
,	,	kIx,	,
velitelství	velitelství	k1gNnSc2	velitelství
<g/>
,	,	kIx,	,
poradního	poradní	k2eAgInSc2d1	poradní
sálu	sál	k1gInSc2	sál
<g/>
,	,	kIx,	,
několika	několik	k4yIc7	několik
moderních	moderní	k2eAgInPc6d1	moderní
samostřílen	samostřílet	k5eAaPmNgMnS	samostřílet
a	a	k8xC	a
také	také	k9	také
Technického	technický	k2eAgNnSc2d1	technické
podlaží	podlaží	k1gNnSc2	podlaží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
lidí	člověk	k1gMnPc2	člověk
byl	být	k5eAaImAgInS	být
Sion	Sion	k1gInSc1	Sion
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
propuknutí	propuknutí	k1gNnSc6	propuknutí
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
stroje	stroj	k1gInPc1	stroj
razantně	razantně	k6eAd1	razantně
vyhrávaly	vyhrávat	k5eAaImAgInP	vyhrávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k8xC	jako
prostředek	prostředek	k1gInSc1	prostředek
poslední	poslední	k2eAgFnSc2d1	poslední
záchrany	záchrana	k1gFnSc2	záchrana
civilizace	civilizace	k1gFnSc2	civilizace
před	před	k7c7	před
mechanizovanou	mechanizovaný	k2eAgFnSc7d1	mechanizovaná
hrozbou	hrozba	k1gFnSc7	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
dle	dle	k7c2	dle
Architekta	architekt	k1gMnSc2	architekt
z	z	k7c2	z
druhého	druhý	k4xOgInSc2	druhý
dílu	díl	k1gInSc2	díl
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
trochu	trochu	k6eAd1	trochu
komplikovanější	komplikovaný	k2eAgMnSc1d2	komplikovanější
<g/>
;	;	kIx,	;
Sion	Sion	k1gInSc1	Sion
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
součástí	součást	k1gFnSc7	součást
Primárního	primární	k2eAgInSc2d1	primární
programu	program	k1gInSc2	program
–	–	k?	–
programu	program	k1gInSc2	program
důležitého	důležitý	k2eAgInSc2d1	důležitý
pro	pro	k7c4	pro
samotný	samotný	k2eAgInSc4d1	samotný
chod	chod	k1gInSc4	chod
Matrixu	Matrix	k1gInSc2	Matrix
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
opakování	opakování	k1gNnSc3	opakování
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
cyklu	cyklus	k1gInSc2	cyklus
–	–	k?	–
stroje	stroj	k1gInSc2	stroj
zcela	zcela	k6eAd1	zcela
vyhladí	vyhladit	k5eAaPmIp3nS	vyhladit
celý	celý	k2eAgInSc4d1	celý
Sion	Sion	k1gInSc4	Sion
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Vyvolený	vyvolený	k2eAgInSc1d1	vyvolený
(	(	kIx(	(
<g/>
anomální	anomální	k2eAgInSc1d1	anomální
program	program	k1gInSc1	program
na	na	k7c6	na
základě	základ	k1gInSc6	základ
neodstranitelné	odstranitelný	k2eNgFnSc2d1	neodstranitelná
nerovnice	nerovnice	k1gFnSc2	nerovnice
<g/>
)	)	kIx)	)
později	pozdě	k6eAd2	pozdě
odpojí	odpojit	k5eAaPmIp3nS	odpojit
několik	několik	k4yIc1	několik
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
16	[number]	k4	16
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
7	[number]	k4	7
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
z	z	k7c2	z
Matrixu	Matrix	k1gInSc2	Matrix
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
Sion	Sion	k1gInSc4	Sion
opět	opět	k6eAd1	opět
obnoví	obnovit	k5eAaPmIp3nS	obnovit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
jakkoliv	jakkoliv	k6eAd1	jakkoliv
věděli	vědět	k5eAaImAgMnP	vědět
o	o	k7c6	o
předchozích	předchozí	k2eAgInPc6d1	předchozí
vyhlazovacích	vyhlazovací	k2eAgInPc6d1	vyhlazovací
cyklech	cyklus	k1gInPc6	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
cyklus	cyklus	k1gInSc1	cyklus
pravidelně	pravidelně	k6eAd1	pravidelně
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nějž	jenž	k3xRgInSc2	jenž
by	by	kYmCp3nS	by
Matrix	Matrix	k1gInSc1	Matrix
nefungoval	fungovat	k5eNaImAgInS	fungovat
(	(	kIx(	(
<g/>
Sion	Sion	k1gInSc1	Sion
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
útočiště	útočiště	k1gNnSc4	útočiště
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc7	jejichž
mozky	mozek	k1gInPc7	mozek
se	se	k3xPyFc4	se
napojení	napojení	k1gNnSc1	napojení
na	na	k7c4	na
Matrix	Matrix	k1gInSc4	Matrix
brání	bránit	k5eAaImIp3nS	bránit
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hrozba	hrozba	k1gFnSc1	hrozba
pro	pro	k7c4	pro
samotný	samotný	k2eAgInSc4d1	samotný
chod	chod	k1gInSc4	chod
Matrixu	Matrix	k1gInSc2	Matrix
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
tvoří	tvořit	k5eAaImIp3nP	tvořit
jen	jen	k9	jen
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
pro	pro	k7c4	pro
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
propracovaný	propracovaný	k2eAgInSc1d1	propracovaný
systém	systém	k1gInSc1	systém
tvoří	tvořit	k5eAaImIp3nS	tvořit
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Chrám	chrám	k1gInSc4	chrám
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velká	velký	k2eAgFnSc1d1	velká
klenutá	klenutý	k2eAgFnSc1d1	klenutá
krápníková	krápníkový	k2eAgFnSc1d1	krápníková
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
žhnou	žhnout	k5eAaImIp3nP	žhnout
nepřetržitě	přetržitě	k6eNd1	přetržitě
pochodně	pochodně	k6eAd1	pochodně
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ji	on	k3xPp3gFnSc4	on
zahřívají	zahřívat	k5eAaImIp3nP	zahřívat
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
Chrámu	chrám	k1gInSc2	chrám
vypadá	vypadat	k5eAaImIp3nS	vypadat
poněkud	poněkud	k6eAd1	poněkud
goticky	goticky	k6eAd1	goticky
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
tuto	tento	k3xDgFnSc4	tento
gotiku	gotika	k1gFnSc4	gotika
hyzdí	hyzdit	k5eAaImIp3nP	hyzdit
kabely	kabel	k1gInPc1	kabel
(	(	kIx(	(
<g/>
všudypřítomné	všudypřítomný	k2eAgNnSc1d1	všudypřítomné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
prováděny	prováděn	k2eAgFnPc1d1	prováděna
modlitby	modlitba	k1gFnPc1	modlitba
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
zde	zde	k6eAd1	zde
taneční	taneční	k2eAgInPc1d1	taneční
zábavy	zábav	k1gInPc1	zábav
a	a	k8xC	a
lid	lid	k1gInSc1	lid
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
schovává	schovávat	k5eAaImIp3nS	schovávat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
i	i	k9	i
jindy	jindy	k6eAd1	jindy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Doky	dok	k1gInPc4	dok
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
parkují	parkovat	k5eAaImIp3nP	parkovat
lodě	loď	k1gFnPc1	loď
(	(	kIx(	(
<g/>
elektrovznášedla	elektrovznášedlo	k1gNnPc1	elektrovznášedlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vjíždí	vjíždět	k5eAaImIp3nS	vjíždět
se	s	k7c7	s
sem	sem	k6eAd1	sem
mohutnými	mohutný	k2eAgFnPc7d1	mohutná
bránami	brána	k1gFnPc7	brána
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tady	tady	k6eAd1	tady
také	také	k9	také
řídící	řídící	k2eAgFnSc2d1	řídící
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
virtuální	virtuální	k2eAgInPc4d1	virtuální
dispečinky	dispečink	k1gInPc4	dispečink
a	a	k8xC	a
automatické	automatický	k2eAgFnPc4d1	automatická
samopalné	samopalný	k2eAgFnPc4d1	samopalná
věže	věž	k1gFnPc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
toho	ten	k3xDgMnSc4	ten
nebylo	být	k5eNaImAgNnS	být
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
některá	některý	k3yIgNnPc1	některý
velitelství	velitelství	k1gNnPc1	velitelství
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
zde	zde	k6eAd1	zde
mechové	mechový	k2eAgFnPc1d1	mechová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
docích	dok	k1gInPc6	dok
je	být	k5eAaImIp3nS	být
také	také	k9	také
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Poradní	poradní	k2eAgInSc1d1	poradní
sál	sál	k1gInSc1	sál
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
místnost	místnost	k1gFnSc1	místnost
zde	zde	k6eAd1	zde
nemá	mít	k5eNaImIp3nS	mít
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgInPc4	žádný
kabely	kabel	k1gInPc4	kabel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
klenutý	klenutý	k2eAgInSc1d1	klenutý
dóm	dóm	k1gInSc1	dóm
<g/>
,	,	kIx,	,
něco	něco	k3yInSc1	něco
mezi	mezi	k7c7	mezi
americkým	americký	k2eAgInSc7d1	americký
Kapitolem	Kapitol	k1gInSc7	Kapitol
<g/>
,	,	kIx,	,
barokními	barokní	k2eAgFnPc7d1	barokní
katedrálami	katedrála	k1gFnPc7	katedrála
a	a	k8xC	a
klasicistními	klasicistní	k2eAgFnPc7d1	klasicistní
budovami	budova	k1gFnPc7	budova
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
notně	notně	k6eAd1	notně
šedivější	šedivý	k2eAgFnSc1d2	šedivější
<g/>
.	.	kIx.	.
</s>
<s>
Zasedá	zasedat	k5eAaImIp3nS	zasedat
zde	zde	k6eAd1	zde
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
také	také	k9	také
zástupci	zástupce	k1gMnPc1	zástupce
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Svislá	svislý	k2eAgFnSc1d1	svislá
promenáda	promenáda	k1gFnSc1	promenáda
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
soubor	soubor	k1gInSc1	soubor
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
kruhovou	kruhový	k2eAgFnSc7d1	kruhová
dírou	díra	k1gFnSc7	díra
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
patrech	patro	k1gNnPc6	patro
jsou	být	k5eAaImIp3nP	být
byty	byt	k1gInPc7	byt
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
zase	zase	k9	zase
výroba	výroba	k1gFnSc1	výroba
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Osvěcují	osvěcovat	k5eAaImIp3nP	osvěcovat
je	on	k3xPp3gFnPc4	on
staré	starý	k2eAgFnPc4d1	stará
velké	velký	k2eAgFnPc4d1	velká
trojžárovky	trojžárovka	k1gFnPc4	trojžárovka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
zhasínají	zhasínat	k5eAaImIp3nP	zhasínat
a	a	k8xC	a
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
úsporná	úsporný	k2eAgNnPc4d1	úsporné
modrá	modrý	k2eAgNnPc4d1	modré
světla	světlo	k1gNnPc4	světlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
dělají	dělat	k5eAaImIp3nP	dělat
3D	[number]	k4	3D
efekt	efekt	k1gInSc4	efekt
noční	noční	k2eAgFnSc2d1	noční
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
však	však	k9	však
mohli	moct	k5eAaImAgMnP	moct
lidé	člověk	k1gMnPc1	člověk
dopravit	dopravit	k5eAaPmF	dopravit
mezi	mezi	k7c7	mezi
podlažími	podlaží	k1gNnPc7	podlaží
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jim	on	k3xPp3gMnPc3	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
několik	několik	k4yIc4	několik
výtahů	výtah	k1gInPc2	výtah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Technické	technický	k2eAgNnSc1d1	technické
podlaží	podlaží	k1gNnSc1	podlaží
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Technické	technický	k2eAgNnSc1d1	technické
podlaží	podlaží	k1gNnSc1	podlaží
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
nejznámější	známý	k2eAgNnPc1d3	nejznámější
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
zařízení	zařízení	k1gNnSc4	zařízení
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
k	k	k7c3	k
recyklaci	recyklace	k1gFnSc3	recyklace
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
výrobě	výroba	k1gFnSc6	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
z	z	k7c2	z
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
víceméně	víceméně	k9	víceméně
bez	bez	k7c2	bez
dozoru	dozor	k1gInSc2	dozor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyhledávací	vyhledávací	k2eAgInSc1d1	vyhledávací
program	program	k1gInSc1	program
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
scéně	scéna	k1gFnSc6	scéna
vidíme	vidět	k5eAaImIp1nP	vidět
na	na	k7c6	na
černé	černý	k2eAgFnSc6d1	černá
obrazovce	obrazovka	k1gFnSc6	obrazovka
zelený	zelený	k2eAgInSc4d1	zelený
blikající	blikající	k2eAgInSc4d1	blikající
kurzor	kurzor	k1gInSc4	kurzor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
první	první	k4xOgFnSc6	první
metafoře	metafora	k1gFnSc6	metafora
je	být	k5eAaImIp3nS	být
skryt	skryt	k2eAgInSc4d1	skryt
nejhlubší	hluboký	k2eAgInSc4d3	nejhlubší
význam	význam	k1gInSc4	význam
celého	celý	k2eAgInSc2d1	celý
filmu	film	k1gInSc2	film
–	–	k?	–
živoucí	živoucí	k2eAgInSc1d1	živoucí
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
dualita	dualita	k1gFnSc1	dualita
mezi	mezi	k7c7	mezi
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
umělou	umělý	k2eAgFnSc7d1	umělá
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
scénáři	scénář	k1gInSc6	scénář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
přirovnávají	přirovnávat	k5eAaImIp3nP	přirovnávat
Wachowští	Wachowský	k1gMnPc1	Wachowský
onen	onen	k3xDgInSc1	onen
blikající	blikající	k2eAgInSc1d1	blikající
kurzor	kurzor	k1gInSc1	kurzor
k	k	k7c3	k
bijícímu	bijící	k2eAgNnSc3d1	bijící
srdci	srdce	k1gNnSc3	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zrcadla	zrcadlo	k1gNnPc1	zrcadlo
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Zrcadla	zrcadlo	k1gNnPc1	zrcadlo
se	se	k3xPyFc4	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
objevují	objevovat	k5eAaImIp3nP	objevovat
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
<g/>
:	:	kIx,	:
odlesk	odlesk	k1gInSc1	odlesk
červené	červený	k2eAgFnSc2d1	červená
a	a	k8xC	a
modré	modrý	k2eAgFnSc2d1	modrá
pilulky	pilulka	k1gFnSc2	pilulka
v	v	k7c6	v
Morpheových	Morpheův	k2eAgFnPc6d1	Morpheův
brýlích	brýle	k1gFnPc6	brýle
<g/>
,	,	kIx,	,
chycení	chycení	k1gNnSc6	chycení
Nea	Nea	k1gFnPc2	Nea
je	být	k5eAaImIp3nS	být
sledováno	sledovat	k5eAaImNgNnS	sledovat
zpětným	zpětný	k2eAgNnSc7d1	zpětné
zrcátkem	zrcátko	k1gNnSc7	zrcátko
na	na	k7c6	na
Trinitině	Trinitin	k2eAgFnSc6d1	Trinitin
motorce	motorka	k1gFnSc6	motorka
<g/>
,	,	kIx,	,
odraz	odraz	k1gInSc1	odraz
zakolísá	zakolísat	k5eAaPmIp3nS	zakolísat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
lžíce	lžíce	k1gFnSc1	lžíce
ohýbána	ohýbán	k2eAgFnSc1d1	ohýbána
<g/>
,	,	kIx,	,
odraz	odraz	k1gInSc1	odraz
prolétající	prolétající	k2eAgFnSc2d1	prolétající
helikoptéry	helikoptéra	k1gFnSc2	helikoptéra
na	na	k7c6	na
mrakodrapu	mrakodrap	k1gInSc6	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
také	také	k9	také
často	často	k6eAd1	často
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Alenku	Alenka	k1gFnSc4	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
pokračování	pokračování	k1gNnSc1	pokračování
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
za	za	k7c7	za
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
(	(	kIx(	(
<g/>
Through	Through	k1gInSc1	Through
the	the	k?	the
Looking-Glass	Looking-Glass	k1gInSc1	Looking-Glass
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
text	text	k1gInSc1	text
Alenky	Alenka	k1gFnSc2	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
začíná	začínat	k5eAaImIp3nS	začínat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Alenka	Alenka	k1gFnSc1	Alenka
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
bílého	bílý	k2eAgMnSc4d1	bílý
králíka	králík	k1gMnSc4	králík
a	a	k8xC	a
spadne	spadnout	k5eAaPmIp3nS	spadnout
do	do	k7c2	do
králičí	králičí	k2eAgFnSc2d1	králičí
nory	nora	k1gFnSc2	nora
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
úvodních	úvodní	k2eAgFnPc2d1	úvodní
scén	scéna	k1gFnPc2	scéna
Matrixu	Matrix	k1gInSc2	Matrix
je	být	k5eAaImIp3nS	být
Neo	Neo	k1gMnSc1	Neo
vyzván	vyzvat	k5eAaPmNgMnS	vyzvat
nápisem	nápis	k1gInSc7	nápis
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
následoval	následovat	k5eAaImAgInS	následovat
bílého	bílý	k2eAgMnSc4d1	bílý
králíka	králík	k1gMnSc4	králík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Don	Don	k1gMnSc1	Don
Davis	Davis	k1gFnSc2	Davis
se	se	k3xPyFc4	se
také	také	k9	také
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
motiv	motiv	k1gInSc4	motiv
reflexe	reflexe	k1gFnSc2	reflexe
<g/>
,	,	kIx,	,
když	když	k8xS	když
tvořil	tvořit	k5eAaImAgInS	tvořit
soundtrack	soundtrack	k1gInSc1	soundtrack
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
se	se	k3xPyFc4	se
sekcemi	sekce	k1gFnPc7	sekce
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
zakomponovat	zakomponovat	k5eAaPmF	zakomponovat
protichůdné	protichůdný	k2eAgInPc4d1	protichůdný
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Použil	použít	k5eAaPmAgMnS	použít
orchestr	orchestr	k1gInSc4	orchestr
a	a	k8xC	a
symfonii	symfonie	k1gFnSc4	symfonie
k	k	k7c3	k
reprezentaci	reprezentace	k1gFnSc3	reprezentace
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
syntetizéry	syntetizér	k1gInPc1	syntetizér
pro	pro	k7c4	pro
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jména	jméno	k1gNnSc2	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sérii	série	k1gFnSc6	série
Matrix	Matrix	k1gInSc4	Matrix
jména	jméno	k1gNnSc2	jméno
často	často	k6eAd1	často
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
významem	význam	k1gInSc7	význam
postav	postava	k1gFnPc2	postava
v	v	k7c6	v
ději	děj	k1gInSc6	děj
</s>
</p>
<p>
<s>
Neo	Neo	k?	Neo
znamená	znamenat	k5eAaImIp3nS	znamenat
nový	nový	k2eAgMnSc1d1	nový
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
anagram	anagram	k1gInSc1	anagram
od	od	k7c2	od
One	One	k1gFnSc2	One
čili	čili	k8xC	čili
Vyvolený	vyvolený	k1gMnSc1	vyvolený
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
celé	celý	k2eAgNnSc4d1	celé
jméno	jméno	k1gNnSc4	jméno
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Trojka	trojka	k1gFnSc1	trojka
nese	nést	k5eAaImIp3nS	nést
plno	plno	k6eAd1	plno
různých	různý	k2eAgInPc2d1	různý
významů	význam	k1gInPc2	význam
skrz	skrz	k7c4	skrz
celý	celý	k2eAgInSc4d1	celý
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
dělená	dělený	k2eAgFnSc1d1	dělená
třemi	tři	k4xCgNnPc7	tři
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
0,33	[number]	k4	0,33
až	až	k9	až
do	do	k7c2	do
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
operace	operace	k1gFnSc1	operace
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
udělána	udělán	k2eAgFnSc1d1	udělána
stroji	stroj	k1gInPc7	stroj
bez	bez	k7c2	bez
umělého	umělý	k2eAgNnSc2d1	umělé
zastavení	zastavení	k1gNnSc2	zastavení
nebo	nebo	k8xC	nebo
zaokrouhlení	zaokrouhlení	k1gNnSc2	zaokrouhlení
<g/>
.	.	kIx.	.
</s>
<s>
Poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
Neo	Neo	k1gFnSc1	Neo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zkáza	zkáza	k1gFnSc1	zkáza
Matrixu	Matrix	k1gInSc2	Matrix
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
celé	celý	k2eAgNnSc4d1	celé
jméno	jméno	k1gNnSc4	jméno
Thomas	Thomas	k1gMnSc1	Thomas
A.	A.	kA	A.
Anderson	Anderson	k1gMnSc1	Anderson
má	mít	k5eAaImIp3nS	mít
spousty	spousta	k1gFnPc4	spousta
dalších	další	k2eAgMnPc2d1	další
náboženských	náboženský	k2eAgMnPc2d1	náboženský
<g/>
,	,	kIx,	,
filozofických	filozofický	k2eAgMnPc2d1	filozofický
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
významů	význam	k1gInPc2	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trinity	Trinita	k1gFnPc4	Trinita
znamená	znamenat	k5eAaImIp3nS	znamenat
svatá	svatý	k2eAgFnSc1d1	svatá
trojice	trojice	k1gFnSc1	trojice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Morpheus	Morpheus	k1gInSc1	Morpheus
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
znamená	znamenat	k5eAaImIp3nS	znamenat
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
hýbe	hýbat	k5eAaImIp3nS	hýbat
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
řecký	řecký	k2eAgMnSc1d1	řecký
bůh	bůh	k1gMnSc1	bůh
snění	snění	k1gNnSc2	snění
a	a	k8xC	a
snů	sen	k1gInPc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
on	on	k3xPp3gMnSc1	on
Nea	Nea	k1gMnSc1	Nea
probudí	probudit	k5eAaPmIp3nS	probudit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cypher	Cyphra	k1gFnPc2	Cyphra
se	se	k3xPyFc4	se
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
posádky	posádka	k1gFnSc2	posádka
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
několika	několik	k4yIc6	několik
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Morpheus	Morpheus	k1gMnSc1	Morpheus
představoval	představovat	k5eAaImAgMnS	představovat
nového	nový	k2eAgMnSc4d1	nový
společníka	společník	k1gMnSc4	společník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
promluvil	promluvit	k5eAaPmAgMnS	promluvit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
Lucifer	Lucifer	k1gMnSc1	Lucifer
<g/>
,	,	kIx,	,
Satan	Satan	k1gMnSc1	Satan
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
také	také	k9	také
znamená	znamenat	k5eAaImIp3nS	znamenat
Jidáš	jidáš	k1gInSc1	jidáš
<g/>
.	.	kIx.	.
</s>
<s>
Zradí	zradit	k5eAaPmIp3nS	zradit
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
cypher	cyphra	k1gFnPc2	cyphra
rovněž	rovněž	k9	rovněž
označuje	označovat	k5eAaImIp3nS	označovat
"	"	kIx"	"
<g/>
šifra	šifra	k1gFnSc1	šifra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Cypher	Cyphra	k1gFnPc2	Cyphra
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
příběhu	příběh	k1gInSc2	příběh
skrývá	skrývat	k5eAaImIp3nS	skrývat
svůj	svůj	k3xOyFgInSc4	svůj
skutečný	skutečný	k2eAgInSc4d1	skutečný
záměr	záměr	k1gInSc4	záměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Jones	Jones	k1gMnSc1	Jones
a	a	k8xC	a
Brown	Brown	k1gMnSc1	Brown
jsou	být	k5eAaImIp3nP	být
nejčastější	častý	k2eAgFnPc4d3	nejčastější
americká	americký	k2eAgNnPc4d1	americké
příjmení	příjmení	k1gNnPc4	příjmení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
bezidentickou	bezidentický	k2eAgFnSc4d1	bezidentický
povahu	povaha	k1gFnSc4	povaha
agentů	agens	k1gInPc2	agens
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
těmto	tento	k3xDgMnPc3	tento
i	i	k9	i
k	k	k7c3	k
mnohým	mnohý	k2eAgNnPc3d1	mnohé
dalším	další	k2eAgNnPc3d1	další
jménům	jméno	k1gNnPc3	jméno
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
výkladů	výklad	k1gInPc2	výklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Barvy	barva	k1gFnSc2	barva
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
budování	budování	k1gNnSc6	budování
kulis	kulisa	k1gFnPc2	kulisa
pro	pro	k7c4	pro
scény	scéna	k1gFnPc4	scéna
uvnitř	uvnitř	k7c2	uvnitř
Matrixu	Matrix	k1gInSc2	Matrix
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
určitých	určitý	k2eAgInPc2d1	určitý
vzorců	vzorec	k1gInPc2	vzorec
a	a	k8xC	a
osnov	osnova	k1gFnPc2	osnova
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
navozena	navozen	k2eAgFnSc1d1	navozena
atmosféra	atmosféra	k1gFnSc1	atmosféra
odtažité	odtažitý	k2eAgFnSc2d1	odtažitá
a	a	k8xC	a
chladné	chladný	k2eAgFnSc2d1	chladná
vypočítavosti	vypočítavost	k1gFnSc2	vypočítavost
tohoto	tento	k3xDgInSc2	tento
umělého	umělý	k2eAgInSc2d1	umělý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
scénách	scéna	k1gFnPc6	scéna
reálného	reálný	k2eAgInSc2d1	reálný
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
modré	modrý	k2eAgInPc4d1	modrý
odstíny	odstín	k1gInPc4	odstín
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
Matrixu	Matrix	k1gInSc6	Matrix
na	na	k7c4	na
zelenou	zelená	k1gFnSc4	zelená
<g/>
,	,	kIx,	,
barvu	barva	k1gFnSc4	barva
stékajícího	stékající	k2eAgInSc2d1	stékající
kódu	kód	k1gInSc2	kód
Matrixu	Matrix	k1gInSc2	Matrix
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	Zelená	k1gFnSc1	Zelená
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
záměrně	záměrně	k6eAd1	záměrně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podtrhovala	podtrhovat	k5eAaImAgFnS	podtrhovat
spojitost	spojitost	k1gFnSc1	spojitost
této	tento	k3xDgFnSc2	tento
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
starým	starý	k2eAgInPc3d1	starý
monochromatickým	monochromatický	k2eAgInPc3d1	monochromatický
monitorům	monitor	k1gInPc3	monitor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
efektů	efekt	k1gInPc2	efekt
se	se	k3xPyFc4	se
scény	scéna	k1gFnPc1	scéna
z	z	k7c2	z
reálného	reálný	k2eAgInSc2d1	reálný
světa	svět	k1gInSc2	svět
točily	točit	k5eAaImAgFnP	točit
na	na	k7c6	na
modrém	modrý	k2eAgNnSc6d1	modré
pozadí	pozadí	k1gNnSc6	pozadí
a	a	k8xC	a
ty	ty	k3xPp2nSc1	ty
z	z	k7c2	z
Matrixu	Matrix	k1gInSc2	Matrix
na	na	k7c6	na
zeleném	zelené	k1gNnSc6	zelené
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kabeláž	kabeláž	k1gFnSc1	kabeláž
v	v	k7c6	v
Nabuchadnezzaru	Nabuchadnezzar	k1gInSc6	Nabuchadnezzar
má	mít	k5eAaImIp3nS	mít
červenou	červený	k2eAgFnSc4d1	červená
a	a	k8xC	a
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připomínala	připomínat	k5eAaImAgFnS	připomínat
schematické	schematický	k2eAgNnSc4d1	schematické
zobrazení	zobrazení	k1gNnSc4	zobrazení
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
se	se	k3xPyFc4	se
tak	tak	k9	tak
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
žijící	žijící	k2eAgFnSc1d1	žijící
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dává	dávat	k5eAaImIp3nS	dávat
posádce	posádka	k1gFnSc3	posádka
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
ale	ale	k9	ale
také	také	k9	také
milující	milující	k2eAgFnSc4d1	milující
péči	péče	k1gFnSc4	péče
od	od	k7c2	od
všech	všecek	k3xTgMnPc2	všecek
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ji	on	k3xPp3gFnSc4	on
neustále	neustále	k6eAd1	neustále
opravují	opravovat	k5eAaImIp3nP	opravovat
<g/>
,	,	kIx,	,
čistí	čistit	k5eAaImIp3nP	čistit
a	a	k8xC	a
starají	starat	k5eAaImIp3nP	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
připomíná	připomínat	k5eAaImIp3nS	připomínat
symbiotický	symbiotický	k2eAgInSc1d1	symbiotický
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
entitami	entita	k1gFnPc7	entita
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
nemůže	moct	k5eNaImIp3nS	moct
žít	žít	k5eAaImF	žít
bez	bez	k7c2	bez
druhé	druhý	k4xOgFnSc2	druhý
<g/>
,	,	kIx,	,
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
Matrix	Matrix	k1gInSc4	Matrix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
Matrix	Matrix	k1gInSc1	Matrix
získal	získat	k5eAaPmAgInS	získat
Oscary	Oscar	k1gInPc4	Oscar
za	za	k7c4	za
střih	střih	k1gInSc4	střih
<g/>
,	,	kIx,	,
zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
<g/>
,	,	kIx,	,
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
film	film	k1gInSc4	film
získal	získat	k5eAaPmAgMnS	získat
tato	tento	k3xDgNnPc4	tento
ocenění	ocenění	k1gNnPc4	ocenění
v	v	k7c4	v
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měly	mít	k5eAaImAgFnP	mít
premiéru	premiéra	k1gFnSc4	premiéra
Star	star	k1gFnSc1	star
Wars	Warsa	k1gFnPc2	Warsa
<g/>
:	:	kIx,	:
Epizoda	epizoda	k1gFnSc1	epizoda
I	i	k9	i
-	-	kIx~	-
Skrytá	skrytý	k2eAgFnSc1d1	skrytá
hrozba	hrozba	k1gFnSc1	hrozba
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
stal	stát	k5eAaPmAgMnS	stát
první	první	k4xOgInSc4	první
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
speciální	speciální	k2eAgInPc4d1	speciální
efekty	efekt	k1gInPc4	efekt
v	v	k7c6	v
soupeření	soupeření	k1gNnSc6	soupeření
s	s	k7c7	s
kterýmkoli	kterýkoli	k3yIgInSc7	kterýkoli
filmem	film	k1gInSc7	film
ze	z	k7c2	z
série	série	k1gFnSc2	série
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Filosofie	filosofie	k1gFnSc1	filosofie
</s>
</p>
<p>
<s>
Platón	Platón	k1gMnSc1	Platón
</s>
</p>
<p>
<s>
Jean	Jean	k1gMnSc1	Jean
Baudrillard	Baudrillard	k1gMnSc1	Baudrillard
</s>
</p>
<p>
<s>
Indická	indický	k2eAgFnSc1d1	indická
filosofie	filosofie	k1gFnSc1	filosofie
</s>
</p>
<p>
<s>
Kyberpunk	Kyberpunk	k6eAd1	Kyberpunk
</s>
</p>
<p>
<s>
Neuromancer	Neuromancer	k1gMnSc1	Neuromancer
</s>
</p>
<p>
<s>
Anime	Animat	k5eAaPmIp3nS	Animat
</s>
</p>
<p>
<s>
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
</s>
</p>
<p>
<s>
Kung-fu	Kung	k1gMnSc3	Kung-f
</s>
</p>
<p>
<s>
Matrix	Matrix	k1gInSc1	Matrix
recesed	recesed	k1gInSc1	recesed
–	–	k?	–
knižní	knižní	k2eAgFnSc2d1	knižní
parodie	parodie	k1gFnSc2	parodie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Matrix	Matrix	k1gInSc1	Matrix
-	-	kIx~	-
Mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c6	o
naší	náš	k3xOp1gFnSc6	náš
době	doba	k1gFnSc6	doba
-	-	kIx~	-
Úděl	úděl	k1gInSc1	úděl
vyvoleného	vyvolený	k1gMnSc2	vyvolený
–	–	k?	–
filosofický	filosofický	k2eAgMnSc1d1	filosofický
průvodce	průvodce	k1gMnSc1	průvodce
Matrixem	Matrix	k1gInSc7	Matrix
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
předpokladu	předpoklad	k1gInSc6	předpoklad
<g/>
:	:	kIx,	:
co	co	k3yRnSc1	co
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
film	film	k1gInSc1	film
naprosto	naprosto	k6eAd1	naprosto
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
<g/>
?	?	kIx.	?
</s>
<s>
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Matrix	Matrix	k1gInSc1	Matrix
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Matrix	Matrix	k1gInSc1	Matrix
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Česky	Česko	k1gNnPc7	Česko
====	====	k?	====
</s>
</p>
<p>
<s>
Matrix	Matrix	k1gInSc1	Matrix
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Matrix	Matrix	k1gInSc1	Matrix
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Matrix	Matrix	k1gInSc1	Matrix
na	na	k7c4	na
SMS	SMS	kA	SMS
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
====	====	k?	====
Anglicky	Anglicko	k1gNnPc7	Anglicko
====	====	k?	====
</s>
</p>
<p>
<s>
Neoficiální	neoficiální	k2eAgInSc1d1	neoficiální
scénář	scénář	k1gInSc1	scénář
</s>
</p>
<p>
<s>
Matrix	Matrix	k1gInSc1	Matrix
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
