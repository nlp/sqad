<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
poznala	poznat	k5eAaPmAgFnS	poznat
svého	svůj	k3xOyFgMnSc4	svůj
budoucího	budoucí	k2eAgMnSc4d1	budoucí
manžela	manžel	k1gMnSc4	manžel
Pierra	Pierr	k1gMnSc4	Pierr
Curie	Curie	k1gMnSc4	Curie
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
doktorandem	doktorand	k1gMnSc7	doktorand
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
Henri	Henri	k1gNnPc2	Henri
Becquerela	Becquerel	k1gMnSc2	Becquerel
<g/>
.	.	kIx.	.
</s>
