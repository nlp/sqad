<s>
Whippet	Whippet	k1gMnSc1	Whippet
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velké	velký	k2eAgNnSc1d1	velké
psí	psí	k2eAgNnSc1d1	psí
plemeno	plemeno	k1gNnSc1	plemeno
<g/>
,	,	kIx,	,
typově	typově	k6eAd1	typově
podobné	podobný	k2eAgInPc1d1	podobný
většímu	veliký	k2eAgMnSc3d2	veliký
greyhoundovi	greyhound	k1gMnSc3	greyhound
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
pes	pes	k1gMnSc1	pes
začal	začít	k5eAaPmAgMnS	začít
vznikat	vznikat	k5eAaImF	vznikat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1835	[number]	k4	1835
až	až	k9	až
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
záměrně	záměrně	k6eAd1	záměrně
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
šlechtit	šlechtit	k5eAaImF	šlechtit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
křížením	křížení	k1gNnSc7	křížení
malých	malý	k2eAgInPc2d1	malý
greyhoundů	greyhound	k1gInPc2	greyhound
s	s	k7c7	s
teriéry	teriér	k1gMnPc7	teriér
(	(	kIx(	(
<g/>
Old	Olda	k1gFnPc2	Olda
English	English	k1gInSc1	English
Black	Black	k1gMnSc1	Black
and	and	k?	and
Tan	Tan	k1gMnSc1	Tan
Terrier	Terrier	k1gMnSc1	Terrier
<g/>
)	)	kIx)	)
a	a	k8xC	a
italským	italský	k2eAgMnSc7d1	italský
chrtíkem	chrtík	k1gMnSc7	chrtík
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
vyšlechtit	vyšlechtit	k5eAaPmF	vyšlechtit
psa	pes	k1gMnSc4	pes
menšího	malý	k2eAgMnSc4d2	menší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
greyhound	greyhound	k1gInSc4	greyhound
a	a	k8xC	a
přesto	přesto	k8xC	přesto
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
vzhledem	vzhled	k1gInSc7	vzhled
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Anglii	Anglie	k1gFnSc6	Anglie
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
populární	populární	k2eAgMnSc1d1	populární
ve	v	k7c6	v
štvanici	štvanice	k1gFnSc6	štvanice
na	na	k7c4	na
králíky	králík	k1gMnPc4	králík
,	,	kIx,	,
Odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Snap	Snap	k1gInSc1	Snap
Dog	doga	k1gFnPc2	doga
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
první	první	k4xOgFnSc2	první
výstavy	výstava	k1gFnSc2	výstava
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
harmonicky	harmonicky	k6eAd1	harmonicky
stavěný	stavěný	k2eAgInSc1d1	stavěný
<g/>
,	,	kIx,	,
elegantní	elegantní	k2eAgInSc1d1	elegantní
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
<g/>
,	,	kIx,	,
suchá	suchý	k2eAgFnSc1d1	suchá
s	s	k7c7	s
plochým	plochý	k2eAgNnSc7d1	ploché
čelem	čelo	k1gNnSc7	čelo
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
k	k	k7c3	k
čumáku	čumák	k1gInSc3	čumák
<g/>
.	.	kIx.	.
</s>
<s>
Čenich	čenich	k1gInSc1	čenich
nosů	nos	k1gInPc2	nos
má	mít	k5eAaImIp3nS	mít
barvu	barva	k1gFnSc4	barva
černou	černý	k2eAgFnSc4d1	černá
<g/>
,	,	kIx,	,
modrou	modrý	k2eAgFnSc4d1	modrá
nebo	nebo	k8xC	nebo
hnědou	hnědý	k2eAgFnSc4d1	hnědá
dle	dle	k7c2	dle
zbarvení	zbarvení	k1gNnSc2	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgFnPc1d1	velká
<g/>
,	,	kIx,	,
jasné	jasný	k2eAgFnPc1d1	jasná
a	a	k8xC	a
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
chytrý	chytrý	k2eAgInSc4d1	chytrý
a	a	k8xC	a
přitažlivý	přitažlivý	k2eAgInSc4d1	přitažlivý
výraz	výraz	k1gInSc4	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
růžového	růžový	k2eAgInSc2d1	růžový
lístku	lístek	k1gInSc2	lístek
složené	složený	k2eAgNnSc1d1	složené
dozadu	dozadu	k6eAd1	dozadu
<g/>
,	,	kIx,	,
porostlé	porostlý	k2eAgNnSc1d1	porostlé
hedvábnou	hedvábný	k2eAgFnSc7d1	hedvábná
srstí	srst	k1gFnSc7	srst
<g/>
.	.	kIx.	.
</s>
<s>
Hrudník	hrudník	k1gInSc1	hrudník
je	být	k5eAaImIp3nS	být
hluboký	hluboký	k2eAgInSc1d1	hluboký
<g/>
,	,	kIx,	,
nohy	noha	k1gFnPc4	noha
se	s	k7c7	s
silnými	silný	k2eAgFnPc7d1	silná
kostmi	kost	k1gFnPc7	kost
dobře	dobře	k6eAd1	dobře
osvalené	osvalený	k2eAgNnSc1d1	osvalené
<g/>
,	,	kIx,	,
pokryté	pokrytý	k2eAgNnSc1d1	pokryté
velmi	velmi	k6eAd1	velmi
tenkou	tenký	k2eAgFnSc7d1	tenká
kůží	kůže	k1gFnSc7	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
má	mít	k5eAaImIp3nS	mít
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
šavlovitý	šavlovitý	k2eAgInSc1d1	šavlovitý
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
závěsu	závěs	k1gInSc2	závěs
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
měkkou	měkký	k2eAgFnSc4d1	měkká
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
krátkou	krátký	k2eAgFnSc4d1	krátká
a	a	k8xC	a
jemnou	jemný	k2eAgFnSc4d1	jemná
srst	srst	k1gFnSc4	srst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
druhy	druh	k1gInPc7	druh
chrtů	chrt	k1gMnPc2	chrt
je	být	k5eAaImIp3nS	být
přátelštější	přátelský	k2eAgFnPc4d2	přátelštější
a	a	k8xC	a
otevřenější	otevřený	k2eAgFnPc4d2	otevřenější
povahy	povaha	k1gFnPc4	povaha
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
přizpůsobí	přizpůsobit	k5eAaPmIp3nS	přizpůsobit
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
hravou	hravý	k2eAgFnSc4d1	hravá
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
přednosti	přednost	k1gFnPc4	přednost
patří	patřit	k5eAaImIp3nS	patřit
rychlá	rychlý	k2eAgFnSc1d1	rychlá
reakce	reakce	k1gFnSc1	reakce
a	a	k8xC	a
obratnost	obratnost	k1gFnSc1	obratnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
počáteční	počáteční	k2eAgFnSc7d1	počáteční
rychlostí	rychlost	k1gFnSc7	rychlost
tvoří	tvořit	k5eAaImIp3nP	tvořit
vhodné	vhodný	k2eAgInPc1d1	vhodný
předpoklady	předpoklad	k1gInPc1	předpoklad
pro	pro	k7c4	pro
lovce	lovec	k1gMnSc4	lovec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
lovu	lov	k1gInSc2	lov
mu	on	k3xPp3gMnSc3	on
nechybí	chybit	k5eNaPmIp3nS	chybit
odvaha	odvaha	k1gFnSc1	odvaha
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
taktizovat	taktizovat	k5eAaImF	taktizovat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
citlivá	citlivý	k2eAgNnPc1d1	citlivé
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
nikdy	nikdy	k6eAd1	nikdy
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
vychovávaná	vychovávaný	k2eAgNnPc4d1	vychovávané
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
přírodnímu	přírodní	k2eAgInSc3d1	přírodní
charakteru	charakter	k1gInSc3	charakter
se	se	k3xPyFc4	se
whippeti	whippet	k1gMnPc1	whippet
dožívají	dožívat	k5eAaImIp3nP	dožívat
poměrně	poměrně	k6eAd1	poměrně
vysokého	vysoký	k2eAgInSc2d1	vysoký
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Whippeti	Whippet	k1gMnPc1	Whippet
netrpí	trpět	k5eNaImIp3nP	trpět
vrozenými	vrozený	k2eAgFnPc7d1	vrozená
vadami	vada	k1gFnPc7	vada
a	a	k8xC	a
neobjevují	objevovat	k5eNaImIp3nP	objevovat
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
záněty	zánět	k1gInPc4	zánět
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
minimum	minimum	k1gNnSc4	minimum
pachových	pachový	k2eAgFnPc2d1	pachová
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvoňavější	voňavý	k2eAgMnPc4d3	nejvoňavější
psy	pes	k1gMnPc4	pes
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgInSc1d1	důležitý
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejrychlejší	rychlý	k2eAgMnPc4d3	nejrychlejší
psy	pes	k1gMnPc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
kůže	kůže	k1gFnSc1	kůže
je	být	k5eAaImIp3nS	být
náchylná	náchylný	k2eAgFnSc1d1	náchylná
na	na	k7c4	na
oděrky	oděrka	k1gFnPc4	oděrka
a	a	k8xC	a
trhliny	trhlina	k1gFnPc4	trhlina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
výborné	výborný	k2eAgFnPc4d1	výborná
regenerační	regenerační	k2eAgFnPc4d1	regenerační
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Drápy	dráp	k1gInPc4	dráp
pátých	pátá	k1gFnPc2	pátá
zakrnělých	zakrnělý	k2eAgInPc2d1	zakrnělý
prstů	prst	k1gInPc2	prst
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
už	už	k6eAd1	už
u	u	k7c2	u
štěňat	štěně	k1gNnPc2	štěně
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
předchází	předcházet	k5eAaImIp3nS	předcházet
odtrhnutím	odtrhnutí	k1gNnSc7	odtrhnutí
a	a	k8xC	a
bolestivým	bolestivý	k2eAgNnSc7d1	bolestivé
zraněním	zranění	k1gNnSc7	zranění
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
whippetů	whippet	k1gMnPc2	whippet
(	(	kIx(	(
<g/>
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
plemen	plemeno	k1gNnPc2	plemeno
chrtů	chrt	k1gMnPc2	chrt
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
savců	savec	k1gMnPc2	savec
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
výjimečně	výjimečně	k6eAd1	výjimečně
objevuje	objevovat	k5eAaImIp3nS	objevovat
mutace	mutace	k1gFnSc1	mutace
v	v	k7c6	v
genu	gen	k1gInSc6	gen
pro	pro	k7c4	pro
myostatin	myostatin	k1gInSc4	myostatin
(	(	kIx(	(
<g/>
myostatin	myostatin	k1gInSc1	myostatin
defficiency	defficienca	k1gFnSc2	defficienca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mutace	mutace	k1gFnSc1	mutace
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
u	u	k7c2	u
whippetů	whippet	k1gInPc2	whippet
dvojité	dvojitý	k2eAgNnSc4d1	dvojité
osvalení	osvalení	k1gNnSc4	osvalení
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
nesoucí	nesoucí	k2eAgFnSc4d1	nesoucí
jednu	jeden	k4xCgFnSc4	jeden
mutaci	mutace	k1gFnSc4	mutace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
heterozygoté	heterozygotý	k2eAgInPc1d1	heterozygotý
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
díky	díky	k7c3	díky
více	hodně	k6eAd2	hodně
vyvinutému	vyvinutý	k2eAgNnSc3d1	vyvinuté
osvalení	osvalení	k1gNnSc3	osvalení
větší	veliký	k2eAgInSc4d2	veliký
potenciál	potenciál	k1gInSc4	potenciál
při	při	k7c6	při
dostizích	dostih	k1gInPc6	dostih
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
nesoucí	nesoucí	k2eAgFnPc1d1	nesoucí
dvě	dva	k4xCgFnPc1	dva
mutace	mutace	k1gFnPc1	mutace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mutovaní	mutovaný	k2eAgMnPc1d1	mutovaný
homozygoté	homozygota	k1gMnPc1	homozygota
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
pak	pak	k6eAd1	pak
dvojité	dvojitý	k2eAgNnSc4d1	dvojité
osvalení	osvalení	k1gNnSc4	osvalení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
občasných	občasný	k2eAgFnPc2d1	občasná
svalových	svalový	k2eAgFnPc2d1	svalová
křečí	křeč	k1gFnPc2	křeč
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
jinak	jinak	k6eAd1	jinak
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
prožít	prožít	k5eAaPmF	prožít
plnohodnotný	plnohodnotný	k2eAgInSc4d1	plnohodnotný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Dostihů	dostih	k1gInPc2	dostih
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
účastnit	účastnit	k5eAaImF	účastnit
nemohou	moct	k5eNaImIp3nP	moct
<g/>
.	.	kIx.	.
</s>
<s>
Předcházet	předcházet	k5eAaImF	předcházet
mutacím	mutace	k1gFnPc3	mutace
v	v	k7c6	v
chovu	chov	k1gInSc6	chov
jde	jít	k5eAaImIp3nS	jít
včasným	včasný	k2eAgNnSc7d1	včasné
testováním	testování	k1gNnSc7	testování
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
již	již	k6eAd1	již
celkem	celkem	k6eAd1	celkem
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
)	)	kIx)	)
a	a	k8xC	a
křížením	křížení	k1gNnSc7	křížení
vhodných	vhodný	k2eAgMnPc2d1	vhodný
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
je	být	k5eAaImIp3nS	být
vlídný	vlídný	k2eAgMnSc1d1	vlídný
a	a	k8xC	a
přítulný	přítulný	k2eAgMnSc1d1	přítulný
<g/>
.	.	kIx.	.
</s>
<s>
Veselý	veselý	k2eAgMnSc1d1	veselý
<g/>
,	,	kIx,	,
energetický	energetický	k2eAgInSc1d1	energetický
<g/>
,	,	kIx,	,
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
klidný	klidný	k2eAgMnSc1d1	klidný
<g/>
,	,	kIx,	,
oddaný	oddaný	k2eAgMnSc1d1	oddaný
<g/>
,	,	kIx,	,
laskavý	laskavý	k2eAgMnSc1d1	laskavý
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
běhu	běh	k1gInSc6	běh
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
rychlosti	rychlost	k1gFnPc4	rychlost
do	do	k7c2	do
65	[number]	k4	65
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
kamarádský	kamarádský	k2eAgInSc1d1	kamarádský
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
snáší	snášet	k5eAaImIp3nS	snášet
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
silně	silně	k6eAd1	silně
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
lovecký	lovecký	k2eAgInSc1d1	lovecký
pud	pud	k1gInSc1	pud
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nevhodný	vhodný	k2eNgInSc1d1	nevhodný
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
venku	venku	k6eAd1	venku
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
špatně	špatně	k6eAd1	špatně
snáší	snášet	k5eAaImIp3nS	snášet
zimu	zima	k1gFnSc4	zima
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chován	chovat	k5eAaImNgInS	chovat
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
procházky	procházka	k1gFnPc4	procházka
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
příležitostí	příležitost	k1gFnPc2	příležitost
k	k	k7c3	k
běhu	běh	k1gInSc3	běh
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Whippet	Whippeta	k1gFnPc2	Whippeta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Whippet	Whippeta	k1gFnPc2	Whippeta
-	-	kIx~	-
Psí	psí	k2eAgFnSc2d1	psí
rasy	rasa	k1gFnSc2	rasa
</s>
