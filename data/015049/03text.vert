<s>
Želva	želva	k1gFnSc1
egyptská	egyptský	k2eAgFnSc1d1
</s>
<s>
Želva	želva	k1gFnSc1
egyptská	egyptský	k2eAgFnSc1d1
Stupeň	stupeň	k1gInSc4
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
kriticky	kriticky	k6eAd1
ohrožený	ohrožený	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
plazi	plaz	k1gMnPc1
(	(	kIx(
<g/>
Reptilia	Reptilia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
želvy	želva	k1gFnPc1
(	(	kIx(
<g/>
Testudines	Testudines	k1gInSc1
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
skrytohrdlí	skrytohrdlí	k1gNnSc1
(	(	kIx(
<g/>
Cryptodira	Cryptodira	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
želvovití	želvovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Testudinidae	Testudinidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
želva	želva	k1gFnSc1
(	(	kIx(
<g/>
Testudo	Testudo	k1gNnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Testudo	Testudo	k1gNnSc1
kleinmanniL	kleinmanniL	k?
<g/>
.	.	kIx.
<g/>
Lortet	Lortet	k1gMnSc1
<g/>
;	;	kIx,
1883	#num#	k4
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Synonyma	synonymum	k1gNnPc1
</s>
<s>
želva	želva	k1gFnSc1
Kleinmannova	Kleinmannov	k1gInSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Želva	želva	k1gFnSc1
egyptská	egyptský	k2eAgFnSc1d1
(	(	kIx(
<g/>
též	též	k9
želva	želva	k1gFnSc1
Kleinmannova	Kleinmannův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Testudo	Testuda	k1gMnSc5
kleinmanni	kleinmanň	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
želv	želva	k1gFnPc2
z	z	k7c2
rodu	rod	k1gInSc2
Testudo	Testudo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
byla	být	k5eAaImAgFnS
řazena	řadit	k5eAaImNgFnS
do	do	k7c2
vlastního	vlastní	k2eAgInSc2d1
rodu	rod	k1gInSc2
Pseudotestudo	Pseudotestudo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Obývá	obývat	k5eAaImIp3nS
severní	severní	k2eAgInPc1d1
okraje	okraj	k1gInPc1
pouštních	pouštní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Libye	Libye	k1gFnSc2
a	a	k8xC
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Izraelská	izraelský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
z	z	k7c2
negevské	gevský	k2eNgFnSc2d1
poušti	poušť	k1gFnSc6
byla	být	k5eAaImAgFnS
oddělena	oddělit	k5eAaPmNgFnS
do	do	k7c2
samostatného	samostatný	k2eAgInSc2d1
druhu	druh	k1gInSc2
želva	želva	k1gFnSc1
Wernerova	Wernerův	k2eAgNnSc2d1
(	(	kIx(
<g/>
Testudo	Testudo	k1gNnSc1
werneri	werner	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
řazena	řadit	k5eAaImNgFnS
mezi	mezi	k7c4
25	#num#	k4
nejohroženějších	ohrožený	k2eAgFnPc2d3
želv	želva	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jde	jít	k5eAaImIp3nS
o	o	k7c4
nejmenší	malý	k2eAgInSc4d3
druh	druh	k1gInSc4
želv	želva	k1gFnPc2
ze	z	k7c2
severní	severní	k2eAgFnSc2d1
polokoule	polokoule	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
a	a	k8xC
chování	chování	k1gNnSc1
</s>
<s>
Porovnání	porovnání	k1gNnSc1
želvy	želva	k1gFnSc2
egyptské	egyptský	k2eAgFnSc2d1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Wernerovy	Wernerův	k2eAgFnPc1d1
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Krunýř	krunýř	k1gInSc1
je	být	k5eAaImIp3nS
vysoce	vysoce	k6eAd1
vyklenutý	vyklenutý	k2eAgInSc1d1
<g/>
,	,	kIx,
dlouhý	dlouhý	k2eAgInSc1d1
až	až	k9
13,5	13,5	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
štítky	štítek	k1gInPc1
jsou	být	k5eAaImIp3nP
pískové	pískový	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
,	,	kIx,
bez	bez	k7c2
kresby	kresba	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
s	s	k7c7
výrazným	výrazný	k2eAgNnSc7d1
tmavým	tmavý	k2eAgNnSc7d1
lemováním	lemování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tělo	tělo	k1gNnSc1
je	být	k5eAaImIp3nS
světle	světle	k6eAd1
hnědé	hnědý	k2eAgNnSc1d1
až	až	k8xS
narůžovělé	narůžovělý	k2eAgNnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
nápadnými	nápadný	k2eAgFnPc7d1
šupinami	šupina	k1gFnPc7
na	na	k7c6
předních	přední	k2eAgFnPc6d1
nohou	noha	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plastron	plastron	k1gInSc1
žlutý	žlutý	k2eAgInSc1d1
<g/>
,	,	kIx,
krom	krom	k7c2
dvou	dva	k4xCgFnPc2
tmavých	tmavý	k2eAgFnPc2d1
skvrn	skvrna	k1gFnPc2
jednobarevný	jednobarevný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
dalších	další	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
rodu	rod	k1gInSc2
Testudo	Testudo	k1gNnSc1
má	mít	k5eAaImIp3nS
zadní	zadní	k2eAgFnSc4d1
část	část	k1gFnSc4
plastronu	plastron	k1gInSc2
zčásti	zčásti	k6eAd1
pohyblivou	pohyblivý	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Želva	želva	k1gFnSc1
egyptská	egyptský	k2eAgFnSc1d1
tráví	trávit	k5eAaImIp3nS
sedm	sedm	k4xCc1
až	až	k9
osm	osm	k4xCc1
měsíců	měsíc	k1gInPc2
roku	rok	k1gInSc2
schovaná	schovaná	k1gFnSc1
před	před	k7c7
letními	letní	k2eAgNnPc7d1
vedry	vedro	k1gNnPc7
v	v	k7c6
úkrytu	úkryt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
a	a	k8xC
v	v	k7c6
zimě	zima	k1gFnSc6
se	se	k3xPyFc4
rychle	rychle	k6eAd1
vykrmí	vykrmit	k5eAaPmIp3nP
spásáním	spásání	k1gNnSc7
částí	část	k1gFnSc7
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
absolvuje	absolvovat	k5eAaPmIp3nS
námluvy	námluva	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
naklade	naklást	k5eAaPmIp3nS
2-3	2-3	k4
snůšky	snůška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klade	klást	k5eAaImIp3nS
1-5	1-5	k4
vajec	vejce	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
těžké	těžký	k2eAgNnSc1d1
napodobit	napodobit	k5eAaPmF
její	její	k3xOp3gNnSc4
prostředí	prostředí	k1gNnSc4
a	a	k8xC
způsob	způsob	k1gInSc4
života	život	k1gInSc2
v	v	k7c6
zajetí	zajetí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Chov	chov	k1gInSc1
v	v	k7c6
zoo	zoo	k1gFnSc6
</s>
<s>
V	v	k7c6
Evropě	Evropa	k1gFnSc6
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
chován	chovat	k5eAaImNgInS
v	v	k7c4
přibližně	přibližně	k6eAd1
45	#num#	k4
zoo	zoo	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Největší	veliký	k2eAgNnSc1d3
zastoupení	zastoupení	k1gNnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
k	k	k7c3
vidění	vidění	k1gNnSc3
v	v	k7c4
11	#num#	k4
zoo	zoo	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
želvy	želva	k1gFnSc2
egyptské	egyptský	k2eAgFnSc2d1
chová	chovat	k5eAaImIp3nS
Zoo	zoo	k1gFnPc7
Plzeň	Plzeň	k1gFnSc1
a	a	k8xC
Zoo	zoo	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chov	chov	k1gInSc1
v	v	k7c6
Zoo	zoo	k1gFnSc6
Praha	Praha	k1gFnSc1
</s>
<s>
V	v	k7c6
Zoo	zoo	k1gFnSc6
Praha	Praha	k1gFnSc1
je	být	k5eAaImIp3nS
chován	chovat	k5eAaImNgInS
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
první	první	k4xOgInSc1
odchov	odchov	k1gInSc1
se	se	k3xPyFc4
podařil	podařit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
vidění	vidění	k1gNnSc3
je	být	k5eAaImIp3nS
v	v	k7c6
pavilonu	pavilon	k1gInSc6
velkých	velký	k2eAgFnPc2d1
želv	želva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
2017	#num#	k4
bylo	být	k5eAaImAgNnS
chováno	chovat	k5eAaImNgNnS
devět	devět	k4xCc1
jedinců	jedinec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Reference	reference	k1gFnSc1
platí	platit	k5eAaImIp3nS
pro	pro	k7c4
celý	celý	k2eAgInSc4d1
odstavec	odstavec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Kleinmann	Kleinmanna	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
tortoise	tortoise	k1gFnPc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
ZYCH	ZYCH	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Želvy	želva	k1gFnSc2
v	v	k7c6
přírodě	příroda	k1gFnSc6
a	a	k8xC
v	v	k7c6
péči	péče	k1gFnSc6
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Brázda	brázda	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
204	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
209	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
342	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Testudo	Testuda	k1gMnSc5
kleinmanni	kleinmanň	k1gMnSc5
<g/>
,	,	kIx,
s.	s.	k?
88	#num#	k4
<g/>
–	–	k?
<g/>
89	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
VELENSKÝ	VELENSKÝ	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Želva	želva	k1gFnSc1
egyptská	egyptský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trojský	trojský	k2eAgInSc4d1
koník	koník	k1gMnSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Želva	želva	k1gFnSc1
egyptská	egyptský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Testudo	Testuda	k1gMnSc5
kleinmanni	kleinmanň	k1gMnSc5
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
www.Zootierliste.de	www.Zootierliste.de	k6eAd1
<g/>
.	.	kIx.
zootierliste	zootierlist	k1gMnSc5
<g/>
.	.	kIx.
<g/>
de	de	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ročenka	ročenka	k1gFnSc1
Unie	unie	k1gFnSc2
českých	český	k2eAgFnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgFnPc2d1
zoologických	zoologický	k2eAgFnPc2d1
zahrad	zahrada	k1gFnPc2
2017	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
želva	želva	k1gFnSc1
egyptská	egyptský	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Testudo	Testudo	k1gNnSc1
werneri	werneri	k6eAd1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
V	v	k7c6
pražské	pražský	k2eAgFnSc6d1
zoo	zoo	k1gFnSc6
se	se	k3xPyFc4
vylíhla	vylíhnout	k5eAaPmAgFnS
vzácná	vzácný	k2eAgFnSc1d1
želva	želva	k1gFnSc1
egyptská	egyptský	k2eAgFnSc1d1
<g/>
,	,	kIx,
měří	měřit	k5eAaImIp3nS
jen	jen	k9
2,5	2,5	k4
centimetru	centimetr	k1gInSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
