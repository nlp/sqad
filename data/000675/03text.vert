<s>
Prima	prima	k1gFnSc1	prima
Cool	Coola	k1gFnPc2	Coola
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
kanál	kanál	k1gInSc4	kanál
stanice	stanice	k1gFnSc2	stanice
Prima	prima	k2eAgFnSc2d1	prima
TV	TV	kA	TV
<g/>
.	.	kIx.	.
</s>
<s>
Vysílání	vysílání	k1gNnSc1	vysílání
je	být	k5eAaImIp3nS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
mladou	mladý	k2eAgFnSc4d1	mladá
populaci	populace	k1gFnSc4	populace
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
až	až	k9	až
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
vysílá	vysílat	k5eAaImIp3nS	vysílat
v	v	k7c6	v
digitálním	digitální	k2eAgNnSc6d1	digitální
pozemním	pozemní	k2eAgNnSc6d1	pozemní
vysílání	vysílání	k1gNnSc6	vysílání
v	v	k7c6	v
multiplexu	multiplex	k1gInSc6	multiplex
2	[number]	k4	2
Českých	český	k2eAgFnPc2d1	Česká
radiokomunikací	radiokomunikace	k1gFnPc2	radiokomunikace
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
naladit	naladit	k5eAaPmF	naladit
také	také	k9	také
pomocí	pomocí	k7c2	pomocí
satelitního	satelitní	k2eAgInSc2d1	satelitní
nebo	nebo	k8xC	nebo
kabelového	kabelový	k2eAgInSc2d1	kabelový
příjmu	příjem	k1gInSc2	příjem
na	na	k7c6	na
části	část	k1gFnSc6	část
území	území	k1gNnSc2	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
spuštění	spuštění	k1gNnSc2	spuštění
se	se	k3xPyFc4	se
stanice	stanice	k1gFnSc1	stanice
dělila	dělit	k5eAaImAgFnS	dělit
o	o	k7c6	o
vysílání	vysílání	k1gNnSc6	vysílání
se	s	k7c7	s
stanicí	stanice	k1gFnSc7	stanice
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
představuje	představovat	k5eAaImIp3nS	představovat
regionální	regionální	k2eAgNnSc4d1	regionální
vysílání	vysílání	k1gNnSc4	vysílání
TV	TV	kA	TV
Prima	prima	k6eAd1	prima
<g/>
.	.	kIx.	.
</s>
<s>
R1	R1	k4	R1
vysílala	vysílat	k5eAaImAgFnS	vysílat
v	v	k7c6	v
dopoledních	dopolední	k2eAgFnPc6d1	dopolední
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
Prima	prima	k2eAgInSc1d1	prima
Cool	Cool	k1gInSc1	Cool
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
a	a	k8xC	a
večerních	večerní	k2eAgFnPc6d1	večerní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
již	již	k6eAd1	již
Prima	prima	k2eAgInSc1d1	prima
Cool	Cool	k1gInSc1	Cool
vysílá	vysílat	k5eAaImIp3nS	vysílat
stále	stále	k6eAd1	stále
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
především	především	k9	především
na	na	k7c4	na
premiéry	premiér	k1gMnPc4	premiér
amerických	americký	k2eAgInPc2d1	americký
seriálů	seriál	k1gInPc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Vysílané	vysílaný	k2eAgInPc1d1	vysílaný
filmy	film	k1gInPc1	film
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
seriálů	seriál	k1gInPc2	seriál
často	často	k6eAd1	často
starší	starý	k2eAgMnSc1d2	starší
a	a	k8xC	a
méně	málo	k6eAd2	málo
známé	známý	k2eAgFnPc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Vysílá	vysílat	k5eAaImIp3nS	vysílat
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
reality	realita	k1gFnPc4	realita
show	show	k1gFnSc1	show
(	(	kIx(	(
<g/>
Faktor	faktor	k1gInSc1	faktor
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
Ninja	Ninja	k1gMnSc1	Ninja
faktor	faktor	k1gMnSc1	faktor
<g/>
,	,	kIx,	,
Mistři	mistr	k1gMnPc1	mistr
demolice	demolice	k1gFnSc2	demolice
<g/>
,	,	kIx,	,
Kdo	kdo	k3yInSc1	kdo
přežije	přežít	k5eAaPmIp3nS	přežít
nebo	nebo	k8xC	nebo
Bořiči	bořič	k1gMnPc1	bořič
mýtů	mýtus	k1gInPc2	mýtus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Prima	prima	k2eAgInSc4d1	prima
Cool	Cool	k1gInSc4	Cool
mohou	moct	k5eAaImIp3nP	moct
diváci	divák	k1gMnPc1	divák
sledovat	sledovat	k5eAaImF	sledovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
Ligu	liga	k1gFnSc4	liga
Mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Prima	prima	k2eAgInSc1d1	prima
Cool	Cool	k1gInSc1	Cool
nabízí	nabízet	k5eAaImIp3nS	nabízet
také	také	k9	také
původní	původní	k2eAgInPc4d1	původní
vlastní	vlastní	k2eAgInPc4d1	vlastní
pořady	pořad	k1gInPc4	pořad
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
např.	např.	kA	např.
RE-PLAY	RE-PLAY	k1gMnPc2	RE-PLAY
(	(	kIx(	(
<g/>
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
novinky	novinka	k1gFnPc4	novinka
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Pekelná	pekelný	k2eAgFnSc1d1	pekelná
výzva	výzva	k1gFnSc1	výzva
(	(	kIx(	(
<g/>
moderuje	moderovat	k5eAaBmIp3nS	moderovat
Iva	Iva	k1gFnSc1	Iva
Pazderková	Pazderková	k1gFnSc1	Pazderková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Agresivní	agresivní	k2eAgInSc1d1	agresivní
virus	virus	k1gInSc1	virus
Akta	akta	k1gNnPc1	akta
X	X	kA	X
American	American	k1gInSc1	American
Horror	horror	k1gInSc1	horror
Story	story	k1gFnSc2	story
Americká	americký	k2eAgFnSc1d1	americká
odysea	odysea	k1gFnSc1	odysea
Americký	americký	k2eAgInSc1d1	americký
chopper	chopper	k1gInSc4	chopper
Bobovy	Bobův	k2eAgFnSc2d1	Bobova
burgery	burgera	k1gFnSc2	burgera
Bořiči	bořič	k1gMnPc7	bořič
mýtů	mýtus	k1gInPc2	mýtus
Buffy	buffa	k1gFnSc2	buffa
<g/>
,	,	kIx,	,
přemožitelka	přemožitelka	k1gFnSc1	přemožitelka
upírů	upír	k1gMnPc2	upír
Bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
Criss	Criss	k1gInSc4	Criss
Angel	angel	k1gMnSc1	angel
<g/>
:	:	kIx,	:
Extrémní	extrémní	k2eAgFnSc1d1	extrémní
magie	magie	k1gFnSc1	magie
Cleveland	Clevelando	k1gNnPc2	Clevelando
show	show	k1gFnSc2	show
Dexter	Dextrum	k1gNnPc2	Dextrum
Faktor	faktor	k1gMnSc1	faktor
strachu	strach	k1gInSc2	strach
Fifth	Fifth	k1gMnSc1	Fifth
Gear	Gear	k1gMnSc1	Gear
Frasier	Frasier	k1gMnSc1	Frasier
Futurama	Futurama	k?	Futurama
Griffinovi	Griffinův	k2eAgMnPc1d1	Griffinův
Grimm	Grimm	k1gInSc1	Grimm
Haló	haló	k0	haló
<g/>
,	,	kIx,	,
tady	tady	k6eAd1	tady
Indie	Indie	k1gFnSc1	Indie
Hračky	hračka	k1gMnSc2	hračka
Jamese	Jamese	k1gFnSc2	Jamese
Maye	May	k1gMnSc2	May
Hustej	Hustej	k?	Hustej
<g />
.	.	kIx.	.
</s>
<s>
nářez	nářez	k1gInSc1	nářez
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
Brána	brána	k1gFnSc1	brána
Chuck	Chucka	k1gFnPc2	Chucka
JAG	Jaga	k1gFnPc2	Jaga
Jak	jak	k6eAd1	jak
jsem	být	k5eAaImIp1nS	být
poznal	poznat	k5eAaPmAgInS	poznat
vaši	váš	k3xOp2gFnSc4	váš
matku	matka	k1gFnSc4	matka
(	(	kIx(	(
<g/>
i	i	k9	i
EN	EN	kA	EN
s	s	k7c7	s
českými	český	k2eAgInPc7d1	český
titulky	titulek	k1gInPc7	titulek
<g/>
)	)	kIx)	)
Jednotka	jednotka	k1gFnSc1	jednotka
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
určení	určení	k1gNnSc2	určení
Jmenuju	jmenovat	k5eAaBmIp1nS	jmenovat
se	se	k3xPyFc4	se
Earl	earl	k1gMnSc1	earl
Kdo	kdo	k3yInSc1	kdo
přežije	přežít	k5eAaPmIp3nS	přežít
Krajní	krajní	k2eAgFnPc4d1	krajní
meze	mez	k1gFnPc4	mez
Kurýr	kurýr	k1gMnSc1	kurýr
Laboratoř	laboratoř	k1gFnSc1	laboratoř
pro	pro	k7c4	pro
chlapy	chlap	k1gMnPc4	chlap
Jamese	Jamese	k1gFnSc2	Jamese
Maye	May	k1gMnPc4	May
Město	město	k1gNnSc1	město
ztracených	ztracený	k2eAgFnPc2d1	ztracená
Námořní	námořní	k2eAgFnSc1d1	námořní
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
služba	služba	k1gFnSc1	služba
L.	L.	kA	L.
A.	A.	kA	A.
Nultá	nultý	k4xOgFnSc1	nultý
hodina	hodina	k1gFnSc1	hodina
Ninja	Ninj	k1gInSc2	Ninj
faktor	faktor	k1gInSc1	faktor
Pevnost	pevnost	k1gFnSc1	pevnost
Boyard	Boyard	k1gMnSc1	Boyard
Pobřežní	pobřežní	k1gMnSc1	pobřežní
lýtka	lýtko	k1gNnSc2	lýtko
Polda	Polda	k1gFnSc1	Polda
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
Poručík	poručík	k1gMnSc1	poručík
Backstrom	Backstrom	k1gInSc4	Backstrom
Poslední	poslední	k2eAgMnSc1d1	poslední
chlap	chlap	k1gMnSc1	chlap
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
Procitnutí	procitnutí	k1gNnSc4	procitnutí
Přežít	přežít	k5eAaPmF	přežít
<g/>
!	!	kIx.	!
</s>
<s>
Řekni	říct	k5eAaPmRp2nS	říct
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
tě	ty	k3xPp2nSc4	ty
zabil	zabít	k5eAaPmAgMnS	zabít
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
Skladiště	skladiště	k1gNnSc4	skladiště
13	[number]	k4	13
Sladký	sladký	k2eAgInSc1d1	sladký
život	život	k1gInSc1	život
Zacka	Zacek	k1gMnSc2	Zacek
a	a	k8xC	a
Codyho	Cody	k1gMnSc2	Cody
Scrubs	Scrubsa	k1gFnPc2	Scrubsa
<g/>
:	:	kIx,	:
Doktůrci	doktůrek	k1gMnPc1	doktůrek
Spartakus	Spartakus	k1gMnSc1	Spartakus
<g/>
:	:	kIx,	:
Pomsta	pomsta	k1gFnSc1	pomsta
Star	star	k1gInSc1	star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Enterprise	Enterprise	k1gFnSc2	Enterprise
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
Studio	studio	k1gNnSc1	studio
30	[number]	k4	30
Rock	rock	k1gInSc1	rock
Špinavá	špinavý	k2eAgFnSc1d1	špinavá
práce	práce	k1gFnSc1	práce
Takešiho	Takeši	k1gMnSc2	Takeši
hrad	hrad	k1gInSc4	hrad
Taková	takový	k3xDgFnSc1	takový
moderní	moderní	k2eAgFnSc1d1	moderní
rodinka	rodinka	k1gFnSc1	rodinka
Teorie	teorie	k1gFnSc1	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
(	(	kIx(	(
<g/>
i	i	k9	i
EN	EN	kA	EN
s	s	k7c7	s
českými	český	k2eAgInPc7d1	český
titulky	titulek	k1gInPc7	titulek
<g/>
)	)	kIx)	)
Terra	Terra	k1gFnSc1	Terra
Nova	novum	k1gNnSc2	novum
Těžká	těžkat	k5eAaImIp3nS	těžkat
dřina	dřina	k1gFnSc1	dřina
Top	topit	k5eAaImRp2nS	topit
Gear	Geara	k1gFnPc2	Geara
Vítejte	vítat	k5eAaImRp2nP	vítat
doma	doma	k6eAd1	doma
<g/>
!	!	kIx.	!
</s>
<s>
Wilfred	Wilfred	k1gMnSc1	Wilfred
Xena	Xen	k1gInSc2	Xen
Zákon	zákon	k1gInSc1	zákon
gangu	gang	k1gInSc2	gang
Ztraceni	ztracen	k2eAgMnPc1d1	ztracen
Živí	živý	k1gMnPc1	živý
mrtví	mrtvit	k5eAaImIp3nP	mrtvit
Aliho	Ali	k1gMnSc4	Ali
parťáci	parťák	k1gMnPc1	parťák
Applikace	Applikace	k1gFnSc1	Applikace
Cinema	Cinema	k1gFnSc1	Cinema
Tipy	tip	k1gInPc1	tip
Elitní	elitní	k2eAgNnSc4d1	elitní
komanda	komando	k1gNnPc4	komando
zblízka	zblízka	k6eAd1	zblízka
Freezone	Freezon	k1gInSc5	Freezon
Hrajeme	hrát	k5eAaImIp1nP	hrát
s	s	k7c7	s
Alim	Ali	k1gNnSc7	Ali
Lovci	lovec	k1gMnPc1	lovec
zážitků	zážitek	k1gInPc2	zážitek
Moto	moto	k1gNnSc1	moto
cestou	cesta	k1gFnSc7	cesta
necestou	necesta	k1gFnSc7	necesta
Motorsport	Motorsport	k1gInSc1	Motorsport
Partička	partička	k1gFnSc1	partička
Parťáci	parťák	k1gMnPc1	parťák
Pekelná	pekelný	k2eAgFnSc1d1	pekelná
výzva	výzva	k1gFnSc1	výzva
Re-play	Relaa	k1gFnSc2	Re-plaa
Shorts	Shortsa	k1gFnPc2	Shortsa
Za	za	k7c4	za
kačku	kačka	k1gFnSc4	kačka
svlíkačku	svlíkačka	k1gFnSc4	svlíkačka
</s>
