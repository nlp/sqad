<s>
Luis	Luisa	k1gFnPc2	Luisa
Walter	Walter	k1gMnSc1	Walter
Alvarez	Alvarez	k1gMnSc1	Alvarez
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
Berkeley	Berkelea	k1gFnPc1	Berkelea
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
španělského	španělský	k2eAgInSc2d1	španělský
původu	původ	k1gInSc2	původ
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
zejména	zejména	k9	zejména
hypotézou	hypotéza	k1gFnSc7	hypotéza
o	o	k7c6	o
vymření	vymření	k1gNnSc6	vymření
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
vlivem	vlivem	k7c2	vlivem
impaktu	impakt	k1gInSc2	impakt
mimozemského	mimozemský	k2eAgNnSc2d1	mimozemské
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
výzkumem	výzkum	k1gInSc7	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Alvarez	Alvarez	k1gMnSc1	Alvarez
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1911	[number]	k4	1911
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Francisca	k1gFnSc4	Francisca
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
nejstarší	starý	k2eAgMnSc1d3	nejstarší
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
a	a	k8xC	a
bratra	bratr	k1gMnSc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
i	i	k8xC	i
dědeček	dědeček	k1gMnSc1	dědeček
byli	být	k5eAaImAgMnP	být
významnými	významný	k2eAgMnPc7d1	významný
lékaři	lékař	k1gMnPc7	lékař
<g/>
,	,	kIx,	,
teta	teta	k1gFnSc1	teta
byla	být	k5eAaImAgFnS	být
malířkou	malířka	k1gFnSc7	malířka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
až	až	k9	až
1924	[number]	k4	1924
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Madison	Madison	k1gInSc1	Madison
School	School	k1gInSc1	School
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
Polytechnic	Polytechnice	k1gFnPc2	Polytechnice
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Rochesteru	Rochester	k1gInSc2	Rochester
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poté	poté	k6eAd1	poté
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Rochester	Rochester	k1gMnSc1	Rochester
High	High	k1gMnSc1	High
School	School	k1gInSc4	School
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
Chicagskou	chicagský	k2eAgFnSc4d1	Chicagská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
získal	získat	k5eAaPmAgInS	získat
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
titul	titul	k1gInSc1	titul
magistrský	magistrský	k2eAgInSc1d1	magistrský
a	a	k8xC	a
o	o	k7c4	o
2	[number]	k4	2
roky	rok	k1gInPc7	rok
později	pozdě	k6eAd2	pozdě
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Fyziku	fyzika	k1gFnSc4	fyzika
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
bakalářského	bakalářský	k2eAgNnSc2d1	bakalářské
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
příležitosti	příležitost	k1gFnSc3	příležitost
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c4	na
zařízení	zařízení	k1gNnSc4	zařízení
prvního	první	k4xOgMnSc2	první
amerického	americký	k2eAgMnSc2d1	americký
nositele	nositel	k1gMnSc2	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
Alberta	Albert	k1gMnSc4	Albert
Abrahama	Abraham	k1gMnSc4	Abraham
Michelsona	Michelson	k1gMnSc4	Michelson
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
vedením	vedení	k1gNnSc7	vedení
významného	významný	k2eAgMnSc2d1	významný
fyzika	fyzik	k1gMnSc2	fyzik
jímž	jenž	k3xRgNnSc7	jenž
byl	být	k5eAaImAgMnS	být
Arthur	Arthur	k1gMnSc1	Arthur
Holly	Holla	k1gFnSc2	Holla
Compton	Compton	k1gInSc1	Compton
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
výzkumem	výzkum	k1gInSc7	výzkum
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
za	za	k7c4	za
pomoci	pomoc	k1gFnPc4	pomoc
vlastně	vlastně	k9	vlastně
sestrojeného	sestrojený	k2eAgInSc2d1	sestrojený
Geigerova-Müllerova	Geigerova-Müllerův	k2eAgInSc2d1	Geigerova-Müllerův
počítače	počítač	k1gInSc2	počítač
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
více	hodně	k6eAd2	hodně
záření	záření	k1gNnSc1	záření
přichází	přicházet	k5eAaImIp3nS	přicházet
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosmické	kosmický	k2eAgNnSc1d1	kosmické
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
pozitivně	pozitivně	k6eAd1	pozitivně
nabité	nabitý	k2eAgNnSc1d1	nabité
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
přijat	přijmout	k5eAaPmNgMnS	přijmout
na	na	k7c4	na
Kalifornskou	kalifornský	k2eAgFnSc4d1	kalifornská
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Berkeley	Berkeleum	k1gNnPc7	Berkeleum
k	k	k7c3	k
uznávanému	uznávaný	k2eAgMnSc3d1	uznávaný
fyzikovi	fyzik	k1gMnSc3	fyzik
Ernestu	Ernest	k1gMnSc3	Ernest
Lawrencemu	Lawrencem	k1gMnSc3	Lawrencem
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
ovšem	ovšem	k9	ovšem
i	i	k9	i
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
teoretiků	teoretik	k1gMnPc2	teoretik
již	již	k6eAd1	již
vedl	vést	k5eAaImAgMnS	vést
Robert	Robert	k1gMnSc1	Robert
Oppenheimer	Oppenheimer	k1gMnSc1	Oppenheimer
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
věnoval	věnovat	k5eAaImAgMnS	věnovat
pozorování	pozorování	k1gNnSc4	pozorování
radioaktivní	radioaktivní	k2eAgNnSc1d1	radioaktivní
beta	beta	k1gNnSc1	beta
přeměny	přeměna	k1gFnSc2	přeměna
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
Alvarez	Alvarez	k1gMnSc1	Alvarez
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
řady	řada	k1gFnSc2	řada
experimentů	experiment	k1gInPc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
výzkumu	výzkum	k1gInSc3	výzkum
izotopů	izotop	k1gInPc2	izotop
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
helia	helium	k1gNnSc2	helium
<g/>
,	,	kIx,	,
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
helium	helium	k1gNnSc1	helium
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tritium	tritium	k1gNnSc1	tritium
nestabilní	stabilní	k2eNgNnSc1d1	nestabilní
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Felixem	Felix	k1gMnSc7	Felix
Blochem	Bloch	k1gMnSc7	Bloch
změřil	změřit	k5eAaPmAgInS	změřit
magnetický	magnetický	k2eAgInSc1d1	magnetický
moment	moment	k1gInSc1	moment
neutronu	neutron	k1gInSc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začala	začít	k5eAaPmAgFnS	začít
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Massachusettském	massachusettský	k2eAgInSc6d1	massachusettský
technologickém	technologický	k2eAgInSc6d1	technologický
institutu	institut	k1gInSc6	institut
založena	založit	k5eAaPmNgFnS	založit
vojenská	vojenský	k2eAgFnSc1d1	vojenská
laboratoř	laboratoř	k1gFnSc1	laboratoř
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
radarů	radar	k1gInPc2	radar
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
přijati	přijmout	k5eAaPmNgMnP	přijmout
Alvarez	Alvarez	k1gMnSc1	Alvarez
i	i	k8xC	i
Lawrence	Lawrence	k1gFnSc1	Lawrence
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
radary	radar	k1gInPc4	radar
na	na	k7c4	na
hledání	hledání	k1gNnSc4	hledání
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
však	však	k9	však
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
mikrovlnného	mikrovlnný	k2eAgInSc2d1	mikrovlnný
radaru	radar	k1gInSc2	radar
a	a	k8xC	a
současně	současně	k6eAd1	současně
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
antény	anténa	k1gFnSc2	anténa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vynález	vynález	k1gInSc1	vynález
umožnil	umožnit	k5eAaPmAgInS	umožnit
američanům	američan	k1gMnPc3	američan
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
bombardovat	bombardovat	k5eAaImF	bombardovat
území	území	k1gNnSc4	území
nepřítele	nepřítel	k1gMnSc2	nepřítel
i	i	k8xC	i
ve	v	k7c6	v
špatném	špatný	k2eAgNnSc6d1	špatné
počasí	počasí	k1gNnSc6	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
jako	jako	k8xS	jako
radar	radar	k1gInSc4	radar
umožňující	umožňující	k2eAgNnSc4d1	umožňující
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
přistávání	přistávání	k1gNnSc4	přistávání
i	i	k9	i
za	za	k7c2	za
zhoršených	zhoršený	k2eAgFnPc2d1	zhoršená
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
Alvarez	Alvarez	k1gMnSc1	Alvarez
osobně	osobně	k6eAd1	osobně
testoval	testovat	k5eAaImAgMnS	testovat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Arthurem	Arthur	k1gMnSc7	Arthur
C.	C.	kA	C.
Clarkem	Clarek	k1gMnSc7	Clarek
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stali	stát	k5eAaPmAgMnP	stát
dobrými	dobrý	k2eAgMnPc7d1	dobrý
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
pozval	pozvat	k5eAaPmAgMnS	pozvat
Oppenheimer	Oppenheimer	k1gMnSc1	Oppenheimer
Alvareze	Alvareze	k1gFnSc2	Alvareze
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Alvarez	Alvarez	k1gInSc1	Alvarez
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
stráví	strávit	k5eAaPmIp3nS	strávit
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
tamní	tamní	k2eAgInSc4d1	tamní
tým	tým	k1gInSc4	tým
vedl	vést	k5eAaImAgMnS	vést
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
<g/>
.	.	kIx.	.
</s>
<s>
Alvarez	Alvarez	k1gMnSc1	Alvarez
byl	být	k5eAaImAgMnS	být
vojenským	vojenský	k2eAgMnSc7d1	vojenský
vedoucím	vedoucí	k1gMnSc7	vedoucí
projektu	projekt	k1gInSc2	projekt
Leslie	Leslie	k1gFnSc1	Leslie
Grovesem	Groves	k1gInSc7	Groves
požádán	požádat	k5eAaPmNgMnS	požádat
o	o	k7c4	o
zjištění	zjištění	k1gNnSc4	zjištění
způsobu	způsob	k1gInSc2	způsob
detekce	detekce	k1gFnSc2	detekce
potenciálních	potenciální	k2eAgFnPc2d1	potenciální
německých	německý	k2eAgFnPc2d1	německá
jaderných	jaderný	k2eAgFnPc2d1	jaderná
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Alvarez	Alvarez	k1gMnSc1	Alvarez
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
využití	využití	k1gNnSc4	využití
detekčního	detekční	k2eAgInSc2d1	detekční
letounu	letoun	k1gInSc2	letoun
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
by	by	kYmCp3nS	by
hledal	hledat	k5eAaImAgMnS	hledat
stopy	stop	k1gInPc4	stop
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
xenonu	xenon	k1gInSc2	xenon
133	[number]	k4	133
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
bylo	být	k5eAaImAgNnS	být
uskutečněno	uskutečněn	k2eAgNnSc1d1	uskutečněno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádné	žádný	k3yNgFnPc1	žádný
stopy	stopa	k1gFnPc1	stopa
xenonu	xenon	k1gInSc2	xenon
nebyly	být	k5eNaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Los	los	k1gInSc4	los
Alamos	Alamosa	k1gFnPc2	Alamosa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
plutoniové	plutoniový	k2eAgFnSc6d1	plutoniová
bombě	bomba	k1gFnSc6	bomba
Fat	fatum	k1gNnPc2	fatum
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovšem	ovšem	k9	ovšem
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c6	na
jiném	jiný	k2eAgInSc6d1	jiný
principu	princip	k1gInSc6	princip
než	než	k8xS	než
déle	dlouho	k6eAd2	dlouho
vyvíjená	vyvíjený	k2eAgFnSc1d1	vyvíjená
uranová	uranový	k2eAgFnSc1d1	uranová
bomba	bomba	k1gFnSc1	bomba
Little	Little	k1gFnSc2	Little
Boy	boa	k1gFnSc2	boa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
plutoniovou	plutoniový	k2eAgFnSc4d1	plutoniová
bombu	bomba	k1gFnSc4	bomba
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
téměř	téměř	k6eAd1	téměř
kritické	kritický	k2eAgNnSc4d1	kritické
množství	množství	k1gNnSc4	množství
plutonia	plutonium	k1gNnSc2	plutonium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stlačovalo	stlačovat	k5eAaImAgNnS	stlačovat
konvenčním	konvenční	k2eAgInSc7d1	konvenční
výbuchem	výbuch	k1gInSc7	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Alvarez	Alvarez	k1gMnSc1	Alvarez
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
řešil	řešit	k5eAaImAgMnS	řešit
technicky	technicky	k6eAd1	technicky
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
rozbušky	rozbuška	k1gFnPc4	rozbuška
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
soubor	soubor	k1gInSc1	soubor
kalibrovaných	kalibrovaný	k2eAgInPc2d1	kalibrovaný
mirkofonů	mirkofon	k1gInPc2	mirkofon
a	a	k8xC	a
vysílačů	vysílač	k1gInPc2	vysílač
a	a	k8xC	a
měřil	měřit	k5eAaImAgInS	měřit
jimi	on	k3xPp3gInPc7	on
síly	síl	k1gInPc7	síl
jaderných	jaderný	k2eAgInPc2d1	jaderný
výbuchů	výbuch	k1gInPc2	výbuch
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
explozí	exploze	k1gFnPc2	exploze
v	v	k7c6	v
Hirošimě	Hirošima	k1gFnSc6	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc6	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Berkeley	Berkelea	k1gFnSc2	Berkelea
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
nápady	nápad	k1gInPc7	nápad
jak	jak	k8xC	jak
válečné	válečný	k2eAgInPc4d1	válečný
vynálezy	vynález	k1gInPc4	vynález
využít	využít	k5eAaPmF	využít
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
urychlovačů	urychlovač	k1gInPc2	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
přelom	přelom	k1gInSc4	přelom
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
Edwin	Edwin	k1gMnSc1	Edwin
McMillan	McMillan	k1gMnSc1	McMillan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
první	první	k4xOgInSc4	první
synchrocyklotron	synchrocyklotron	k1gInSc4	synchrocyklotron
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
pak	pak	k6eAd1	pak
zkonstruovali	zkonstruovat	k5eAaPmAgMnP	zkonstruovat
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
největší	veliký	k2eAgInSc4d3	veliký
urychlovač	urychlovač	k1gInSc4	urychlovač
částic	částice	k1gFnPc2	částice
světa	svět	k1gInSc2	svět
Bevatron	Bevatron	k1gInSc4	Bevatron
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
obtížné	obtížný	k2eAgNnSc1d1	obtížné
provézt	provézt	k5eAaPmF	provézt
analýzu	analýza	k1gFnSc4	analýza
výsledků	výsledek	k1gInPc2	výsledek
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
chytil	chytit	k5eAaPmAgMnS	chytit
nového	nový	k2eAgInSc2d1	nový
vynálezu	vynález	k1gInSc2	vynález
bublinkové	bublinkový	k2eAgFnSc2d1	bublinková
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
přišel	přijít	k5eAaPmAgMnS	přijít
Donald	Donald	k1gMnSc1	Donald
Arthur	Arthur	k1gMnSc1	Arthur
Glaser	Glaser	k1gMnSc1	Glaser
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Glaserově	Glaserův	k2eAgInSc6d1	Glaserův
detektoru	detektor	k1gInSc6	detektor
byl	být	k5eAaImAgMnS	být
jako	jako	k8xS	jako
kapalina	kapalina	k1gFnSc1	kapalina
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
stopy	stopa	k1gFnPc1	stopa
částic	částice	k1gFnPc2	částice
ether	ether	k1gInSc1	ether
<g/>
.	.	kIx.	.
</s>
<s>
Alvarez	Alvarez	k1gInSc1	Alvarez
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
použil	použít	k5eAaPmAgInS	použít
kapalný	kapalný	k2eAgInSc1d1	kapalný
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
stopy	stopa	k1gFnPc4	stopa
fotografovat	fotografovat	k5eAaImF	fotografovat
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
komora	komora	k1gFnSc1	komora
velká	velký	k2eAgFnSc1d1	velká
2	[number]	k4	2
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
milionům	milion	k4xCgInPc3	milion
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Alvarez	Alvarez	k1gMnSc1	Alvarez
tak	tak	k6eAd1	tak
objevil	objevit	k5eAaPmAgMnS	objevit
řadu	řada	k1gFnSc4	řada
rezonančních	rezonanční	k2eAgInPc2d1	rezonanční
stavů	stav	k1gInPc2	stav
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
odměněn	odměnit	k5eAaPmNgInS	odměnit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Vyninul	Vyninout	k5eAaPmAgInS	Vyninout
také	také	k9	také
nové	nový	k2eAgInPc4d1	nový
systémy	systém	k1gInPc4	systém
pro	pro	k7c4	pro
detekci	detekce	k1gFnSc4	detekce
a	a	k8xC	a
analýzu	analýza	k1gFnSc4	analýza
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
balónový	balónový	k2eAgInSc1d1	balónový
experiment	experiment	k1gInSc1	experiment
HAPPE	HAPPE	kA	HAPPE
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
vysokoenergetického	vysokoenergetický	k2eAgNnSc2d1	vysokoenergetické
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
srážek	srážka	k1gFnPc2	srážka
se	s	k7c7	s
zemskou	zemský	k2eAgFnSc7d1	zemská
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
změnil	změnit	k5eAaPmAgMnS	změnit
zaměření	zaměření	k1gNnSc3	zaměření
experimentu	experiment	k1gInSc2	experiment
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
kosmologie	kosmologie	k1gFnSc2	kosmologie
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
částic	částice	k1gFnPc2	částice
z	z	k7c2	z
raného	raný	k2eAgInSc2d1	raný
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
hledání	hledání	k1gNnSc4	hledání
neznámých	známý	k2eNgFnPc2d1	neznámá
prostor	prostora	k1gFnPc2	prostora
v	v	k7c6	v
egyptských	egyptský	k2eAgFnPc6d1	egyptská
pyramidách	pyramida	k1gFnPc6	pyramida
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
jeho	jeho	k3xOp3gFnSc2	jeho
rychlosti	rychlost	k1gFnSc2	rychlost
a	a	k8xC	a
směru	směr	k1gInSc6	směr
částic	částice	k1gFnPc2	částice
tohoto	tento	k3xDgInSc2	tento
záření	záření	k1gNnSc1	záření
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
odhalit	odhalit	k5eAaPmF	odhalit
potenciální	potenciální	k2eAgFnPc4d1	potenciální
komory	komora	k1gFnPc4	komora
v	v	k7c6	v
pyramidách	pyramida	k1gFnPc6	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
prozkoumáno	prozkoumat	k5eAaPmNgNnS	prozkoumat
20	[number]	k4	20
<g/>
%	%	kIx~	%
velké	velký	k2eAgFnPc1d1	velká
pyramidy	pyramida	k1gFnPc1	pyramida
a	a	k8xC	a
žádné	žádný	k3yNgFnPc1	žádný
neznámé	známý	k2eNgFnPc1d1	neznámá
komory	komora	k1gFnPc1	komora
nebyly	být	k5eNaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
Alvarezův	Alvarezův	k2eAgMnSc1d1	Alvarezův
syn	syn	k1gMnSc1	syn
Walter	Walter	k1gMnSc1	Walter
Alvarez	Alvarez	k1gMnSc1	Alvarez
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
geologem	geolog	k1gMnSc7	geolog
<g/>
,	,	kIx,	,
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
vrstvy	vrstva	k1gFnSc2	vrstva
mezi	mezi	k7c7	mezi
křídou	křída	k1gFnSc7	křída
a	a	k8xC	a
třetihorami	třetihory	k1gFnPc7	třetihory
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Berkeley	Berkelea	k1gFnSc2	Berkelea
oznámil	oznámit	k5eAaPmAgInS	oznámit
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
z	z	k7c2	z
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
vymírání	vymírání	k1gNnSc3	vymírání
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
rád	rád	k6eAd1	rád
tuto	tento	k3xDgFnSc4	tento
záhadu	záhada	k1gFnSc4	záhada
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Alvarez	Alvarez	k1gMnSc1	Alvarez
starší	starší	k1gMnSc1	starší
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
otázce	otázka	k1gFnSc6	otázka
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
chemiky	chemik	k1gMnPc7	chemik
z	z	k7c2	z
Lawrence	Lawrenec	k1gInSc2	Lawrenec
Berkeley	Berkelea	k1gFnSc2	Berkelea
National	National	k1gFnSc2	National
Laboratory	Laborator	k1gMnPc4	Laborator
a	a	k8xC	a
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
vymírání	vymírání	k1gNnSc1	vymírání
má	mít	k5eAaImIp3nS	mít
mimozemský	mimozemský	k2eAgInSc4d1	mimozemský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozboru	rozbor	k1gInSc2	rozbor
jílu	jíl	k1gInSc2	jíl
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
saze	saze	k1gFnPc1	saze
<g/>
,	,	kIx,	,
šokové	šokový	k2eAgInPc1d1	šokový
krystaly	krystal	k1gInPc1	krystal
křemene	křemen	k1gInSc2	křemen
<g/>
,	,	kIx,	,
mikroskopické	mikroskopický	k2eAgInPc1d1	mikroskopický
diamanty	diamant	k1gInPc1	diamant
a	a	k8xC	a
vzácné	vzácný	k2eAgInPc1d1	vzácný
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
tlaků	tlak	k1gInPc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
zjištění	zjištění	k1gNnSc4	zjištění
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
a	a	k8xC	a
setkalo	setkat	k5eAaPmAgNnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
ohlasem	ohlas	k1gInSc7	ohlas
v	v	k7c6	v
geologické	geologický	k2eAgFnSc6d1	geologická
komunitě	komunita	k1gFnSc6	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Objevu	objev	k1gInSc3	objev
kráteru	kráter	k1gInSc2	kráter
Chicxulub	Chicxuluba	k1gFnPc2	Chicxuluba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
Alvarez	Alvarez	k1gMnSc1	Alvarez
starší	starší	k1gMnSc1	starší
již	již	k6eAd1	již
nedožil	dožít	k5eNaPmAgMnS	dožít
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
obvykle	obvykle	k6eAd1	obvykle
udávaným	udávaný	k2eAgNnSc7d1	udávané
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
vymírání	vymírání	k1gNnSc2	vymírání
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhohor	druhohory	k1gFnPc2	druhohory
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
přišli	přijít	k5eAaPmAgMnP	přijít
paleontologové	paleontolog	k1gMnPc1	paleontolog
Raup	Raup	k1gMnSc1	Raup
a	a	k8xC	a
Sepkoski	Sepkosk	k1gMnPc1	Sepkosk
na	na	k7c6	na
základě	základ	k1gInSc6	základ
domnělé	domnělý	k2eAgFnSc2d1	domnělá
periodicity	periodicita	k1gFnSc2	periodicita
velkých	velký	k2eAgNnPc2d1	velké
vymírání	vymírání	k1gNnPc2	vymírání
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
hvězda	hvězda	k1gFnSc1	hvězda
Nemesis	Nemesis	k1gFnSc1	Nemesis
obíhající	obíhající	k2eAgFnSc1d1	obíhající
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Alvarez	Alvarez	k1gMnSc1	Alvarez
spočítal	spočítat	k5eAaPmAgMnS	spočítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
muselo	muset	k5eAaImAgNnS	muset
jít	jít	k5eAaImF	jít
o	o	k7c4	o
hnědého	hnědý	k2eAgMnSc4d1	hnědý
nebo	nebo	k8xC	nebo
červeného	červený	k2eAgMnSc4d1	červený
trpaslíka	trpaslík	k1gMnSc4	trpaslík
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1,5	[number]	k4	1,5
-	-	kIx~	-
3	[number]	k4	3
světelné	světelný	k2eAgInPc1d1	světelný
roky	rok	k1gInPc1	rok
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
ovšem	ovšem	k9	ovšem
žádná	žádný	k3yNgFnSc1	žádný
taková	takový	k3xDgFnSc1	takový
hvězda	hvězda	k1gFnSc1	hvězda
nalezena	naleznout	k5eAaPmNgFnS	naleznout
nebyla	být	k5eNaImAgFnS	být
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
astronomů	astronom	k1gMnPc2	astronom
často	často	k6eAd1	často
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
zpochybňována	zpochybňovat	k5eAaImNgFnS	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
domnělá	domnělý	k2eAgFnSc1d1	domnělá
periodicita	periodicita	k1gFnSc1	periodicita
vymírání	vymírání	k1gNnSc2	vymírání
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
jen	jen	k9	jen
důsledkem	důsledek	k1gInSc7	důsledek
statistické	statistický	k2eAgFnSc2d1	statistická
chyby	chyba	k1gFnSc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vědy	věda	k1gFnSc2	věda
se	se	k3xPyFc4	se
Alvarez	Alvarez	k1gMnSc1	Alvarez
věnoval	věnovat	k5eAaPmAgMnS	věnovat
i	i	k9	i
letectví	letectví	k1gNnSc4	letectví
<g/>
.	.	kIx.	.
</s>
<s>
Létat	létat	k5eAaImF	létat
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
nalétáno	nalétán	k2eAgNnSc4d1	nalétáno
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
často	často	k6eAd1	často
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
velícího	velící	k2eAgMnSc4d1	velící
pilota	pilot	k1gMnSc4	pilot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letectví	letectví	k1gNnSc6	letectví
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
autorem	autor	k1gMnSc7	autor
řady	řada	k1gFnSc2	řada
vynálezů	vynález	k1gInPc2	vynález
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
člen	člen	k1gInSc1	člen
mnoha	mnoho	k4c2	mnoho
výborů	výbor	k1gInPc2	výbor
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgNnSc4d1	vojenské
i	i	k8xC	i
civilní	civilní	k2eAgNnSc4d1	civilní
letectví	letectví	k1gNnSc4	letectví
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
působil	působit	k5eAaImAgMnS	působit
u	u	k7c2	u
Federal	Federal	k1gFnSc2	Federal
Aviation	Aviation	k1gInSc1	Aviation
Administration	Administration	k1gInSc1	Administration
a	a	k8xC	a
President	president	k1gMnSc1	president
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Science	Science	k1gFnSc1	Science
Advisory	Advisor	k1gInPc4	Advisor
Committee	Committe	k1gFnSc2	Committe
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
i	i	k9	i
mnoha	mnoho	k4c2	mnoho
letů	let	k1gInPc2	let
ve	v	k7c6	v
vojenských	vojenský	k2eAgMnPc6d1	vojenský
letounech	letoun	k1gMnPc6	letoun
<g/>
,	,	kIx,	,
proletěl	proletět	k5eAaPmAgMnS	proletět
se	se	k3xPyFc4	se
v	v	k7c4	v
Boeing	boeing	k1gInSc4	boeing
B-29	B-29	k1gFnSc2	B-29
Superfortress	Superfortress	k1gInSc1	Superfortress
nebo	nebo	k8xC	nebo
Lockheed	Lockheed	k1gMnSc1	Lockheed
F-104	F-104	k1gMnSc1	F-104
Starfighter	Starfighter	k1gMnSc1	Starfighter
<g/>
.	.	kIx.	.
</s>
<s>
Luis	Luisa	k1gFnPc2	Luisa
Alvarez	Alvarez	k1gMnSc1	Alvarez
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
manželku	manželka	k1gFnSc4	manželka
Geraldine	Geraldin	k1gInSc5	Geraldin
Smithwick	Smithwick	k1gMnSc1	Smithwick
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
rozvedeni	rozvést	k5eAaPmNgMnP	rozvést
byli	být	k5eAaImAgMnP	být
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
manželku	manželka	k1gFnSc4	manželka
Janet	Janeta	k1gFnPc2	Janeta
Landis	Landis	k1gInSc4	Landis
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
rovněž	rovněž	k9	rovněž
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Walter	Walter	k1gMnSc1	Walter
Alvarez	Alvarez	k1gMnSc1	Alvarez
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgMnSc7d1	významný
geologem	geolog	k1gMnSc7	geolog
<g/>
.	.	kIx.	.
</s>
