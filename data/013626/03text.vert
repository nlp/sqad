<s>
Mistresses	Mistresses	k1gMnSc1
</s>
<s>
Mistresses	Mistresses	k1gInSc1
Logo	logo	k1gNnSc1
seriáluZákladní	seriáluZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Žánr	žánr	k1gInSc4
</s>
<s>
drama	drama	k1gNnSc1
Formát	formát	k1gInSc1
</s>
<s>
seriál	seriál	k1gInSc1
Dramaturgie	dramaturgie	k1gFnSc2
</s>
<s>
K.	K.	kA
J.	J.	kA
Steinberg	Steinberg	k1gInSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Lowri	Lowri	k1gNnSc1
GlainS	GlainS	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Clarkson	Clarksona	k1gFnPc2
(	(	kIx(
<g/>
původní	původní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
Hrají	hrát	k5eAaImIp3nP
</s>
<s>
Alyssa	Alyssa	k1gFnSc1
MilanoRochelle	MilanoRochelle	k1gFnSc2
AytesYunjin	AytesYunjin	k1gMnSc1
KimJes	KimJes	k1gMnSc1
MacallanJason	MacallanJason	k1gMnSc1
GeorgeBrett	GeorgeBrett	k1gMnSc1
TuckerErik	TuckerErik	k1gMnSc1
StocklinJennifer	StocklinJennifer	k1gMnSc1
EspositoRob	EspositoRoba	k1gFnPc2
Mayes	Mayes	k1gMnSc1
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
USA	USA	kA
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Počet	počet	k1gInSc1
řad	řad	k1gInSc1
</s>
<s>
4	#num#	k4
Počet	počet	k1gInSc4
dílů	díl	k1gInPc2
</s>
<s>
52	#num#	k4
(	(	kIx(
<g/>
seznam	seznam	k1gInSc4
dílů	díl	k1gInPc2
<g/>
)	)	kIx)
Obvyklá	obvyklý	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
43	#num#	k4
minut	minuta	k1gFnPc2
Produkce	produkce	k1gFnSc1
a	a	k8xC
štáb	štáb	k1gInSc1
Výkonnýproducent	Výkonnýproducent	k1gInSc1
</s>
<s>
Rima	Rima	k1gMnSc1
MimounPoulgas	MimounPoulgas	k1gMnSc1
RaeGrant	RaeGrant	k1gMnSc1
ScharboRobert	ScharboRobert	k1gMnSc1
M.	M.	kA
SertnerK	SertnerK	k1gMnSc1
<g/>
.	.	kIx.
<g/>
J.	J.	kA
Steinberg	Steinberg	k1gInSc1
Lokace	lokace	k1gFnSc1
</s>
<s>
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Vancouver	Vancouver	k1gInSc1
<g/>
,	,	kIx,
Britská	britský	k2eAgFnSc1d1
Kolumbie	Kolumbie	k1gFnSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc1
<g/>
)	)	kIx)
Produkčníspolečnost	Produkčníspolečnost	k1gFnSc1
</s>
<s>
ABC	ABC	kA
Stuidos	Stuidos	k1gMnSc1
Ecosse	Ecosse	k1gFnSc2
FilmsBob	FilmsBoba	k1gFnPc2
Sertner	Sertnra	k1gFnPc2
ProductionsGood	ProductionsGood	k1gInSc1
Talk	Talk	k1gMnSc1
Productions	Productionsa	k1gFnPc2
Distributor	distributor	k1gMnSc1
</s>
<s>
Disney	Disney	k1gInPc1
<g/>
–	–	k?
<g/>
ABC	ABC	kA
Domestic	Domestice	k1gFnPc2
TelevisionHulu	TelevisionHul	k1gInSc3
Premiérové	premiérový	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
Stanice	stanice	k1gFnSc1
</s>
<s>
ABC	ABC	kA
Formát	formát	k1gInSc1
obrazu	obraz	k1gInSc2
</s>
<s>
480	#num#	k4
<g/>
i	i	k8xC
(	(	kIx(
<g/>
SDTV	SDTV	kA
<g/>
)	)	kIx)
<g/>
720	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
HDTV	HDTV	kA
<g/>
)	)	kIx)
Vysíláno	vysílán	k2eAgNnSc1d1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2016	#num#	k4
Oficiální	oficiální	k2eAgInPc4d1
webové	webový	k2eAgInPc4d1
stránkyMistresses	stránkyMistresses	k1gInSc4
na	na	k7c4
ČSFDNěkterá	ČSFDNěkterý	k2eAgNnPc4d1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mistresses	Mistress	k1gFnPc1
je	být	k5eAaImIp3nS
americký	americký	k2eAgInSc1d1
dramatický	dramatický	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
inspirovaným	inspirovaný	k2eAgInSc7d1
britským	britský	k2eAgInSc7d1
stejnojmenným	stejnojmenný	k2eAgInSc7d1
seriálem	seriál	k1gInSc7
z	z	k7c2
let	léto	k1gNnPc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
sleduje	sledovat	k5eAaImIp3nS
život	život	k1gInSc4
čtyř	čtyři	k4xCgInPc2
kamarádek	kamarádka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgFnPc7d1
hvězdami	hvězda	k1gFnPc7
jsou	být	k5eAaImIp3nP
Alyssa	Alyssa	k1gFnSc1
Milano	Milana	k1gFnSc5
<g/>
,	,	kIx,
Rochelle	Rochelle	k1gNnPc7
Aytes	Aytes	k1gInSc1
<g/>
,	,	kIx,
Yunjin	Yunjin	k1gMnSc1
Kim	Kim	k1gMnSc1
a	a	k8xC
Jes	Jes	k1gMnSc1
Macallan	Macallan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvůrcem	tvůrce	k1gMnSc7
seriálu	seriál	k1gInSc2
je	být	k5eAaImIp3nS
K.	K.	kA
J.	J.	kA
Steinberg	Steinberg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seriál	seriál	k1gInSc1
měl	mít	k5eAaImAgInS
mít	mít	k5eAaImF
původně	původně	k6eAd1
premiéru	premiéra	k1gFnSc4
na	na	k7c6
stanici	stanice	k1gFnSc6
ABC	ABC	kA
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
premiéra	premiéra	k1gFnSc1
byla	být	k5eAaImAgFnS
posunuta	posunout	k5eAaPmNgFnS
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2014	#num#	k4
byl	být	k5eAaImAgInS
seriál	seriál	k1gInSc1
prodloužen	prodloužit	k5eAaPmNgInS
o	o	k7c4
třetí	třetí	k4xOgFnSc4
řadu	řada	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
premiéru	premiéra	k1gFnSc4
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílání	vysílání	k1gNnSc3
čtvrté	čtvrtá	k1gFnSc2
řady	řada	k1gFnSc2
bylo	být	k5eAaImAgNnS
zahájeno	zahájit	k5eAaPmNgNnS
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seriál	seriál	k1gInSc1
byl	být	k5eAaImAgInS
ukončen	ukončit	k5eAaPmNgInS
po	po	k7c6
čtvrté	čtvrtý	k4xOgFnSc6
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
poslední	poslední	k2eAgInSc1d1
díl	díl	k1gInSc1
byl	být	k5eAaImAgInS
odvysílán	odvysílat	k5eAaPmNgInS
6	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Alyssa	Alyssa	k1gFnSc1
Milano	Milana	k1gFnSc5
jako	jako	k9
Savannah	Savannah	k1gInSc1
"	"	kIx"
<g/>
Savi	Savi	k1gNnSc1
<g/>
"	"	kIx"
Davis	Davis	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
právnička	právnička	k1gFnSc1
<g/>
,	,	kIx,
Henryho	Henry	k1gMnSc2
bývalá	bývalý	k2eAgFnSc1d1
žena	žena	k1gFnSc1
<g/>
,	,	kIx,
starší	starý	k2eAgFnSc1d2
sestra	sestra	k1gFnSc1
Joss	Jossa	k1gFnPc2
</s>
<s>
Rochelle	Rochelle	k1gFnSc1
Aytes	Aytesa	k1gFnPc2
jako	jako	k8xC,k8xS
April	Aprila	k1gFnPc2
Mallowy	Mallowa	k1gFnSc2
<g/>
,	,	kIx,
vdova	vdova	k1gFnSc1
a	a	k8xC
svobodná	svobodný	k2eAgFnSc1d1
matka	matka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vlastní	vlastní	k2eAgInSc4d1
obchod	obchod	k1gInSc4
s	s	k7c7
nábytkem	nábytek	k1gInSc7
a	a	k8xC
kavárnu	kavárna	k1gFnSc4
</s>
<s>
Yunjin	Yunjin	k1gMnSc1
Kim	Kim	k1gMnSc1
jako	jako	k8xC,k8xS
doktorka	doktorka	k1gFnSc1
Karen	Karen	k1gInSc1
Kim	Kim	k1gFnSc1
<g/>
;	;	kIx,
psychiatrička	psychiatrička	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
zapletla	zaplést	k5eAaPmAgFnS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
pacientem	pacient	k1gMnSc7
</s>
<s>
Jes	Jes	k?
Macallan	Macallan	k1gInSc1
jako	jako	k8xC,k8xS
Josslyn	Josslyn	k1gInSc1
"	"	kIx"
<g/>
Joss	Joss	k1gInSc1
<g/>
"	"	kIx"
Carver	Carver	k1gInSc1
<g/>
;	;	kIx,
realitní	realitní	k2eAgFnSc1d1
makléřka	makléřka	k1gFnSc1
<g/>
,	,	kIx,
mladší	mladý	k2eAgFnSc1d2
sestra	sestra	k1gFnSc1
Savannah	Savannaha	k1gFnPc2
</s>
<s>
Jason	Jason	k1gNnSc1
George	George	k1gFnPc2
jako	jako	k8xC,k8xS
Dominic	Dominice	k1gFnPc2
Taylor	Taylor	k1gMnSc1
<g/>
;	;	kIx,
právník	právník	k1gMnSc1
<g/>
,	,	kIx,
spolupracovník	spolupracovník	k1gMnSc1
Savannah	Savannah	k1gMnSc1
</s>
<s>
Brett	Brett	k1gMnSc1
Tucker	Tucker	k1gMnSc1
jako	jako	k8xS,k8xC
Henry	Henry	k1gMnSc1
Davis	Davis	k1gFnSc2
<g/>
;	;	kIx,
šéfkuchař	šéfkuchař	k1gMnSc1
a	a	k8xC
vlastník	vlastník	k1gMnSc1
restaurace	restaurace	k1gFnSc2
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
manžel	manžel	k1gMnSc1
Savannah	Savannah	k1gMnSc1
</s>
<s>
Erik	Erik	k1gMnSc1
Stocklin	Stocklin	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Sam	Sam	k1gMnSc1
Grey	Grea	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
syn	syn	k1gMnSc1
Karenina	Karenina	k1gFnSc1
milence	milenec	k1gMnSc2
</s>
<s>
Jennifer	Jennifer	k1gMnSc1
Esposito	Esposita	k1gFnSc5
jako	jako	k8xS,k8xC
Calista	Calista	k1gMnSc1
Raines	Raines	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
kamarádka	kamarádka	k1gFnSc1
Joss	Jossa	k1gFnPc2
</s>
<s>
Rob	roba	k1gFnPc2
Mayes	Mayes	k1gMnSc1
jako	jako	k8xC,k8xS
Mark	Mark	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Harryho	Harry	k1gMnSc2
kolega	kolega	k1gMnSc1
</s>
<s>
Vedlejší	vedlejší	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Corinne	Corinnout	k5eAaPmIp3nS,k5eAaImIp3nS
Massiah	Massiah	k1gInSc4
jako	jako	k8xC,k8xS
Lucy	Luc	k2eAgFnPc4d1
Malloy	Malloa	k1gFnPc4
</s>
<s>
Matthew	Matthew	k?
Del	Del	k1gFnSc1
Negro	Negro	k1gNnSc1
jako	jako	k8xS,k8xC
Jacob	Jacoba	k1gFnPc2
</s>
<s>
Justin	Justin	k1gMnSc1
Hartley	Hartlea	k1gFnSc2
jako	jako	k8xS,k8xC
Scott	Scott	k1gMnSc1
</s>
<s>
Penelope	Penelop	k1gMnSc5
Ann	Ann	k1gMnSc1
Miller	Miller	k1gMnSc1
jako	jako	k8xS,k8xC
Elizabeth	Elizabeth	k1gFnSc1
Grey	Grea	k1gFnSc2
</s>
<s>
Cameron	Cameron	k1gMnSc1
Bender	Bender	k1gMnSc1
jako	jako	k8xC,k8xS
Richard	Richard	k1gMnSc1
</s>
<s>
Ricky	Ricky	k6eAd1
Whittle	Whittle	k1gFnSc1
jako	jako	k8xS,k8xC
Daniel	Daniel	k1gMnSc1
Zamora	Zamor	k1gMnSc2
</s>
<s>
Catherine	Catherin	k1gInSc5
Haena	Haen	k1gMnSc2
Kim	Kim	k1gFnSc1
jako	jako	k8xC,k8xS
Anna	Anna	k1gFnSc1
Choi	Cho	k1gFnSc2
</s>
<s>
Dondre	Dondr	k1gInSc5
Whitfield	Whitfield	k1gMnSc1
jako	jako	k8xS,k8xC
Paul	Paul	k1gMnSc1
Malloy	Malloa	k1gFnSc2
</s>
<s>
Shannyn	Shannyn	k1gMnSc1
Sossamon	Sossamon	k1gMnSc1
jako	jako	k8xC,k8xS
Alex	Alex	k1gMnSc1
</s>
<s>
Rebeka	Rebeka	k1gFnSc1
Montoya	Montoyus	k1gMnSc2
jako	jako	k8xC,k8xS
Antonia	Antonio	k1gMnSc2
Ruiz	Ruiza	k1gFnPc2
</s>
<s>
Mike	Mike	k6eAd1
Dopud	Dopud	k1gMnSc1
jako	jako	k8xC,k8xS
Olivier	Olivier	k1gMnSc1
Dubois	Dubois	k1gFnSc2
</s>
<s>
Kate	kat	k1gMnSc5
Beahan	Beahan	k1gMnSc1
jako	jako	k8xC,k8xS
Miranda	Miranda	k1gFnSc1
Nickleby	Nickleba	k1gFnSc2
</s>
<s>
Jason	Jason	k1gMnSc1
Gerhardt	Gerhardt	k1gMnSc1
jako	jako	k8xC,k8xS
Zack	Zack	k1gMnSc1
Kilmer	Kilmer	k1gMnSc1
</s>
<s>
Epizody	epizoda	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
dílů	díl	k1gInPc2
seriálu	seriál	k1gInSc2
Mistresses	Mistressesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
ŘadaDílyPremiéra	ŘadaDílyPremiéra	k1gFnSc1
v	v	k7c6
USA	USA	kA
</s>
<s>
První	první	k4xOgMnPc1
dílPoslední	dílPosledný	k2eAgMnPc1d1
díl	díl	k1gInSc4
</s>
<s>
1	#num#	k4
</s>
<s>
1320130603	#num#	k4
<g/>
a	a	k8xC
<g/>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
201320130909	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1320140602	#num#	k4
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
201420140901	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1320150618	#num#	k4
<g/>
a	a	k8xC
<g/>
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
201520150903	#num#	k4
<g/>
a	a	k8xC
<g/>
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
1320160530	#num#	k4
<g/>
a	a	k8xC
<g/>
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
201620160906	#num#	k4
<g/>
a	a	k8xC
<g/>
6	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2016	#num#	k4
</s>
<s>
Produkce	produkce	k1gFnSc1
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2015	#num#	k4
stanice	stanice	k1gFnSc2
ABC	ABC	kA
oficiálně	oficiálně	k6eAd1
vybrala	vybrat	k5eAaPmAgFnS
seriál	seriál	k1gInSc4
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
letního	letní	k2eAgNnSc2d1
vysílání	vysílání	k1gNnSc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
objednáno	objednat	k5eAaPmNgNnS
13	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tohle	tenhle	k3xDgNnSc1
je	být	k5eAaImIp3nS
druhý	druhý	k4xOgInSc1
pokus	pokus	k1gInSc1
o	o	k7c4
importování	importování	k1gNnSc4
britského	britský	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
do	do	k7c2
amerických	americký	k2eAgFnPc2d1
televiz	televiza	k1gFnPc2
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
stanice	stanice	k1gFnSc2
Lifetime	Lifetim	k1gInSc5
vyvíjela	vyvíjet	k5eAaImAgFnS
seriál	seriál	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
seriál	seriál	k1gInSc1
na	na	k7c4
obrazovky	obrazovka	k1gFnPc4
nedostal	dostat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lifetime	Lifetim	k1gInSc5
verzi	verze	k1gFnSc6
měla	mít	k5eAaImAgFnS
hrát	hrát	k5eAaImF
Holly	Holla	k1gFnPc4
Marie	Maria	k1gFnSc2
Combs	Combsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2013	#num#	k4
byl	být	k5eAaImAgInS
seriál	seriál	k1gInSc1
prodloužen	prodloužit	k5eAaPmNgInS
o	o	k7c4
druhou	druhý	k4xOgFnSc4
sérii	série	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
premiéru	premiéra	k1gFnSc4
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
Alyssa	Alyssa	k1gFnSc1
Milano	Milana	k1gFnSc5
se	se	k3xPyFc4
již	již	k6eAd1
do	do	k7c2
seriálu	seriál	k1gInSc2
nevrátí	vrátit	k5eNaPmIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
produkce	produkce	k1gFnSc1
seriálu	seriál	k1gInSc2
přesunula	přesunout	k5eAaPmAgFnS
z	z	k7c2
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
do	do	k7c2
Vancouveru	Vancouver	k1gInSc2
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
herečka	herečka	k1gFnSc1
Jennifer	Jennifra	k1gFnPc2
Esposito	Esposita	k1gFnSc5
se	se	k3xPyFc4
připojí	připojit	k5eAaPmIp3nS
k	k	k7c3
seriálu	seriál	k1gInSc2
jako	jako	k8xS,k8xC
Calista	Calista	k1gMnSc1
Raines	Raines	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vysílání	vysílání	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
měl	mít	k5eAaImAgInS
seriál	seriál	k1gInSc1
premiéru	premiéra	k1gFnSc4
na	na	k7c6
kanadské	kanadský	k2eAgFnSc6d1
stanici	stanice	k1gFnSc6
CTV	CTV	kA
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2013	#num#	k4
seriál	seriál	k1gInSc1
začala	začít	k5eAaPmAgFnS
vysílat	vysílat	k5eAaImF
stanice	stanice	k1gFnSc1
TLC	TLC	kA
v	v	k7c6
Britském	britský	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Austrálii	Austrálie	k1gFnSc6
měl	mít	k5eAaImAgInS
seriál	seriál	k1gInSc1
premiéru	premiéra	k1gFnSc4
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2013	#num#	k4
na	na	k7c6
stanici	stanice	k1gFnSc6
Seven	Seven	k2eAgInSc1d1
Network	network	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Mistresses	Mistressesa	k1gFnPc2
(	(	kIx(
<g/>
U.	U.	kA
<g/>
S.	S.	kA
TV	TV	kA
series	series	k1gMnSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
