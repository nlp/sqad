<p>
<s>
Aden	Aden	k1gInSc1	Aden
Abdullah	Abdullah	k1gMnSc1	Abdullah
Osman	Osman	k1gMnSc1	Osman
Daar	Daar	k1gMnSc1	Daar
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Aden	Aden	k1gInSc4	Aden
Adde	Add	k1gInSc2	Add
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
Beledweyne	Beledweyn	k1gInSc5	Beledweyn
<g/>
,	,	kIx,	,
Italské	italský	k2eAgNnSc1d1	italské
Somálsko	Somálsko	k1gNnSc1	Somálsko
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
Nairobi	Nairobi	k1gNnSc2	Nairobi
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
somálský	somálský	k2eAgMnSc1d1	somálský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Osman	Osman	k1gMnSc1	Osman
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
v	v	k7c6	v
Beledweyne	Beledweyn	k1gInSc5	Beledweyn
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
vzdělání	vzdělání	k1gNnSc4	vzdělání
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Italského	italský	k2eAgNnSc2d1	italské
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
zastával	zastávat	k5eAaImAgInS	zastávat
různá	různý	k2eAgNnPc4d1	různé
úřednická	úřednický	k2eAgNnPc4d1	úřednické
místa	místo	k1gNnPc4	místo
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
vlastním	vlastní	k2eAgInPc3d1	vlastní
obchodům	obchod	k1gInPc3	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1944	[number]	k4	1944
založil	založit	k5eAaPmAgInS	založit
Somálský	somálský	k2eAgInSc1d1	somálský
klub	klub	k1gInSc1	klub
mladých	mladý	k2eAgMnPc2d1	mladý
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Somálskou	somálský	k2eAgFnSc4d1	Somálská
ligu	liga	k1gFnSc4	liga
mladých	mladý	k2eAgMnPc2d1	mladý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gInSc7	člen
jeho	on	k3xPp3gNnSc2	on
vedení	vedení	k1gNnSc2	vedení
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
tajemníkem	tajemník	k1gMnSc7	tajemník
sekce	sekce	k1gFnSc1	sekce
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Belet	Belet	k1gInSc1	Belet
Weyne	Weyn	k1gMnSc5	Weyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
zástupce	zástupce	k1gMnSc1	zástupce
Somálského	somálský	k2eAgInSc2d1	somálský
klubu	klub	k1gInSc2	klub
mladých	mladý	k1gMnPc2	mladý
(	(	kIx(	(
<g/>
SYL	SYL	kA	SYL
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
přerušovaně	přerušovaně	k6eAd1	přerušovaně
v	v	k7c6	v
Teritoriální	teritoriální	k2eAgFnSc6d1	teritoriální
radě	rada	k1gFnSc6	rada
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
této	tento	k3xDgFnSc2	tento
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
SYL	SYL	kA	SYL
<g/>
,	,	kIx,	,
úřad	úřad	k1gInSc4	úřad
zastával	zastávat	k5eAaImAgMnS	zastávat
do	do	k7c2	do
r.	r.	kA	r.
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
r.	r.	kA	r.
1956	[number]	k4	1956
Teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
rada	rada	k1gFnSc1	rada
změněna	změnit	k5eAaPmNgFnS	změnit
v	v	k7c4	v
Zákonodárné	zákonodárný	k2eAgNnSc4d1	zákonodárné
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
zvolen	zvolit	k5eAaPmNgMnS	zvolit
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
za	za	k7c4	za
okres	okres	k1gInSc4	okres
Belet	Beleta	k1gFnPc2	Beleta
Weyne	Weyn	k1gInSc5	Weyn
a	a	k8xC	a
shromáždění	shromáždění	k1gNnSc2	shromáždění
ho	on	k3xPp3gMnSc4	on
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
svým	svůj	k3xOyFgMnSc7	svůj
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1958	[number]	k4	1958
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnSc2	předseda
SYL	SYL	kA	SYL
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
úřad	úřad	k1gInSc1	úřad
zastával	zastávat	k5eAaImAgInS	zastávat
společně	společně	k6eAd1	společně
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
předsedy	předseda	k1gMnSc2	předseda
Zákonodárného	zákonodárný	k2eAgNnSc2d1	zákonodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgMnS	udržet
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
Zákonodárné	zákonodárný	k2eAgNnSc1d1	zákonodárné
shromáždění	shromáždění	k1gNnSc1	shromáždění
změnilo	změnit	k5eAaPmAgNnS	změnit
ve	v	k7c6	v
shromáždění	shromáždění	k1gNnSc6	shromáždění
ústavodárné	ústavodárný	k2eAgNnSc1d1	Ústavodárné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vrchol	vrchol	k1gInSc1	vrchol
politické	politický	k2eAgFnSc2d1	politická
kariéry	kariéra	k1gFnSc2	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
předseda	předseda	k1gMnSc1	předseda
tohoto	tento	k3xDgNnSc2	tento
shromáždění	shromáždění	k1gNnSc2	shromáždění
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Osman	Osman	k1gMnSc1	Osman
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
nezávislost	nezávislost	k1gFnSc1	nezávislost
Somálska	Somálsko	k1gNnSc2	Somálsko
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
sjednocení	sjednocení	k1gNnSc4	sjednocení
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
Italského	italský	k2eAgNnSc2d1	italské
a	a	k8xC	a
Britského	britský	k2eAgNnSc2d1	Britské
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
Národním	národní	k2eAgNnSc7d1	národní
shromážděním	shromáždění	k1gNnSc7	shromáždění
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prozatímním	prozatímní	k2eAgMnSc7d1	prozatímní
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
samou	samý	k3xTgFnSc4	samý
funkci	funkce	k1gFnSc6	funkce
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
zájmy	zájem	k1gInPc4	zájem
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
legislativu	legislativa	k1gFnSc4	legislativa
<g/>
,	,	kIx,	,
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
sociální	sociální	k2eAgFnSc4d1	sociální
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Mluvil	mluvit	k5eAaImAgMnS	mluvit
italsky	italsky	k6eAd1	italsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
arabsky	arabsky	k6eAd1	arabsky
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgInS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
i	i	k8xC	i
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osman	Osman	k1gMnSc1	Osman
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
r.	r.	kA	r.
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
mladých	mladý	k2eAgFnPc2d1	mladá
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaPmAgMnS	učinit
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
posledním	poslední	k2eAgNnSc6d1	poslední
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
uchopil	uchopit	k5eAaPmAgInS	uchopit
moc	moc	k6eAd1	moc
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
Mohamed	Mohamed	k1gMnSc1	Mohamed
Siad	Siad	k1gMnSc1	Siad
Barre	Barr	k1gInSc5	Barr
a	a	k8xC	a
udržel	udržet	k5eAaPmAgMnS	udržet
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
až	až	k9	až
do	do	k7c2	do
r.	r.	kA	r.
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gNnSc4	on
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
předznamenalo	předznamenat	k5eAaPmAgNnS	předznamenat
čtrnáctileté	čtrnáctiletý	k2eAgNnSc1d1	čtrnáctileté
období	období	k1gNnSc1	období
anarchie	anarchie	k1gFnSc2	anarchie
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
<g/>
Aden	Aden	k1gInSc1	Aden
Abdullah	Abdullah	k1gMnSc1	Abdullah
Osman	Osman	k1gMnSc1	Osman
zemřel	zemřít	k5eAaPmAgMnS	zemřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
99	[number]	k4	99
let	léto	k1gNnPc2	léto
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
keňském	keňský	k2eAgNnSc6d1	keňské
Nairobi	Nairobi	k1gNnSc6	Nairobi
<g/>
.	.	kIx.	.
</s>
<s>
Zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
osm	osm	k4xCc4	osm
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
přejmenováno	přejmenován	k2eAgNnSc1d1	přejmenováno
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Mogadišu	Mogadišo	k1gNnSc3	Mogadišo
na	na	k7c4	na
"	"	kIx"	"
<g/>
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Adena	Aden	k1gMnSc2	Aden
Abdullaha	Abdullah	k1gMnSc2	Abdullah
Osmana	Osman	k1gMnSc2	Osman
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
