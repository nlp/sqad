<p>
<s>
Ruslan	Ruslan	k1gInSc1	Ruslan
Gasymov	Gasymov	k1gInSc1	Gasymov
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ruský	ruský	k2eAgMnSc1d1	ruský
zápasník	zápasník	k1gMnSc1	zápasník
<g/>
–	–	k?	–
<g/>
sambista	sambista	k1gMnSc1	sambista
a	a	k8xC	a
judista	judista	k1gMnSc1	judista
azerského	azerský	k2eAgInSc2d1	azerský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
úpolovými	úpolův	k2eAgInPc7d1	úpolův
sporty	sport	k1gInPc7	sport
začínal	začínat	k5eAaImAgMnS	začínat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
sambistického	sambistický	k2eAgMnSc2d1	sambistický
trenéra	trenér	k1gMnSc2	trenér
Alexandra	Alexandr	k1gMnSc2	Alexandr
Jerjomina	Jerjomin	k2eAgMnSc2d1	Jerjomin
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc3	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
sambistický	sambistický	k2eAgMnSc1d1	sambistický
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
poprvé	poprvé	k6eAd1	poprvé
výrazně	výrazně	k6eAd1	výrazně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
ruské	ruský	k2eAgFnSc6d1	ruská
judistické	judistický	k2eAgFnSc6d1	judistická
reprezentaci	reprezentace	k1gFnSc6	reprezentace
v	v	k7c6	v
polotěžké	polotěžký	k2eAgFnSc6d1	polotěžká
váze	váha	k1gFnSc6	váha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
nekvalifikoval	kvalifikovat	k5eNaBmAgMnS	kvalifikovat
kvůli	kvůli	k7c3	kvůli
<g/>
,	,	kIx,	,
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
poměry	poměra	k1gFnPc4	poměra
nevýrazným	výrazný	k2eNgInPc3d1	nevýrazný
výsledkům	výsledek	k1gInPc3	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Kvalifikační	kvalifikační	k2eAgFnSc4d1	kvalifikační
kvótu	kvóta	k1gFnSc4	kvóta
získal	získat	k5eAaPmAgMnS	získat
pro	pro	k7c4	pro
Rusko	Rusko	k1gNnSc4	Rusko
v	v	k7c6	v
olympijském	olympijský	k2eAgInSc6d1	olympijský
roce	rok	k1gInSc6	rok
Aslan	Aslana	k1gFnPc2	Aslana
Unašchotlov	Unašchotlov	k1gInSc4	Unašchotlov
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
však	však	k9	však
při	při	k7c6	při
interní	interní	k2eAgFnSc6d1	interní
nominaci	nominace	k1gFnSc6	nominace
porazil	porazit	k5eAaPmAgMnS	porazit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nominovám	nominovat	k5eAaImIp1nS	nominovat
do	do	k7c2	do
Pekingu	Peking	k1gInSc2	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
nepřešel	přejít	k5eNaPmAgMnS	přejít
přes	přes	k7c4	přes
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
minutách	minuta	k1gFnPc6	minuta
boje	boj	k1gInSc2	boj
hodil	hodit	k5eAaPmAgMnS	hodit
na	na	k7c4	na
ippon	ippon	k1gNnSc4	ippon
technikou	technika	k1gFnSc7	technika
seoi-nage	seoiag	k1gInSc2	seoi-nag
Němec	Němec	k1gMnSc1	Němec
Benjamin	Benjamin	k1gMnSc1	Benjamin
Behrla	Behrla	k1gMnSc1	Behrla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
reprezentaci	reprezentace	k1gFnSc6	reprezentace
ukončil	ukončit	k5eAaPmAgMnS	ukončit
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruslan	Ruslan	k1gInSc1	Ruslan
Gasymov	Gasymov	k1gInSc1	Gasymov
byl	být	k5eAaImAgInS	být
judistka	judistka	k1gFnSc1	judistka
s	s	k7c7	s
nestandartním	standartní	k2eNgInSc7d1	nestandartní
levým	levý	k2eAgInSc7d1	levý
úchopem	úchop	k1gInSc7	úchop
a	a	k8xC	a
s	s	k7c7	s
osobní	osobní	k2eAgFnSc7d1	osobní
technikou	technika	k1gFnSc7	technika
kata-guruma	kataurumum	k1gNnSc2	kata-gurumum
a	a	k8xC	a
kučiki-taoši	kučikiaoš	k1gMnPc1	kučiki-taoš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Judo	judo	k1gNnSc1	judo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zápas	zápas	k1gInSc1	zápas
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
novinky	novinka	k1gFnPc1	novinka
Ruslana	Ruslan	k1gMnSc2	Ruslan
Gasymova	Gasymův	k2eAgMnSc2d1	Gasymův
na	na	k7c4	na
judoinside	judoinsid	k1gInSc5	judoinsid
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
