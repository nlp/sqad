<s>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
(	(	kIx(	(
<g/>
srbsky	srbsky	k6eAd1	srbsky
Б	Б	k?	Б
/	/	kIx~	/
Beograd	Beograd	k1gInSc1	Beograd
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
a	a	k8xC	a
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Království	království	k1gNnSc2	království
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
1	[number]	k4	1
160	[number]	k4	160
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
aglomerace	aglomerace	k1gFnSc1	aglomerace
čítá	čítat	k5eAaImIp3nS	čítat
přes	přes	k7c4	přes
1,65	[number]	k4	1,65
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řek	k1gMnSc1	řek
Dunaj	Dunaj	k1gInSc1	Dunaj
a	a	k8xC	a
Sáva	Sáva	k1gFnSc1	Sáva
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
strategickém	strategický	k2eAgInSc6d1	strategický
soutoku	soutok	k1gInSc6	soutok
dvou	dva	k4xCgFnPc2	dva
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
starým	starý	k2eAgNnPc3d1	staré
městům	město	k1gNnPc3	město
a	a	k8xC	a
místům	místo	k1gNnPc3	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kontinuální	kontinuální	k2eAgNnPc4d1	kontinuální
osídlení	osídlení	k1gNnPc4	osídlení
trvá	trvat	k5eAaImIp3nS	trvat
již	již	k9	již
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
Keltů	Kelt	k1gMnPc2	Kelt
zde	zde	k6eAd1	zde
existovala	existovat	k5eAaImAgFnS	existovat
osada	osada	k1gFnSc1	osada
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
později	pozdě	k6eAd2	pozdě
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
město	město	k1gNnSc4	město
nazývali	nazývat	k5eAaImAgMnP	nazývat
Singidunum	Singidunum	k1gInSc4	Singidunum
<g/>
,	,	kIx,	,
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
a	a	k8xC	a
přebudovali	přebudovat	k5eAaPmAgMnP	přebudovat
i	i	k9	i
přístav	přístav	k1gInSc4	přístav
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
říční	říční	k2eAgFnSc4d1	říční
flotilu	flotila	k1gFnSc4	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
město	město	k1gNnSc4	město
patřilo	patřit	k5eAaImAgNnS	patřit
Byzantské	byzantský	k2eAgFnSc3d1	byzantská
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
po	po	k7c4	po
stěhování	stěhování	k1gNnSc4	stěhování
národů	národ	k1gInPc2	národ
a	a	k8xC	a
úpadku	úpadek	k1gInSc2	úpadek
Cařihradu	Cařihrad	k1gInSc2	Cařihrad
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
nastěhovali	nastěhovat	k5eAaPmAgMnP	nastěhovat
Slované	Slovan	k1gMnPc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
ale	ale	k9	ale
trpělo	trpět	k5eAaImAgNnS	trpět
pod	pod	k7c7	pod
náporem	nápor	k1gInSc7	nápor
jak	jak	k6eAd1	jak
uherských	uherský	k2eAgInPc2d1	uherský
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
benátských	benátský	k2eAgInPc2d1	benátský
útoků	útok	k1gInPc2	útok
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
získat	získat	k5eAaPmF	získat
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
pod	pod	k7c4	pod
svoji	svůj	k3xOyFgFnSc4	svůj
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1284	[number]	k4	1284
se	se	k3xPyFc4	se
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
stal	stát	k5eAaPmAgInS	stát
poprvé	poprvé	k6eAd1	poprvé
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1316	[number]	k4	1316
<g/>
-	-	kIx~	-
<g/>
1402	[number]	k4	1402
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
také	také	k9	také
součástí	součást	k1gFnSc7	součást
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1402	[number]	k4	1402
se	se	k3xPyFc4	se
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
posledního	poslední	k2eAgInSc2d1	poslední
srbského	srbský	k2eAgInSc2d1	srbský
státu	stát	k1gInSc2	stát
v	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
-	-	kIx~	-
Srbského	srbský	k2eAgInSc2d1	srbský
despotátu	despotát	k1gInSc2	despotát
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
tak	tak	k6eAd1	tak
nachází	nacházet	k5eAaImIp3nS	nacházet
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rozkvětu	rozkvět	k1gInSc3	rozkvět
města	město	k1gNnSc2	město
a	a	k8xC	a
i	i	k9	i
srbského	srbský	k2eAgInSc2d1	srbský
státu	stát	k1gInSc2	stát
jako	jako	k8xC	jako
takového	takový	k3xDgNnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
úpadku	úpadek	k1gInSc2	úpadek
těžby	těžba	k1gFnSc2	těžba
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patřily	patřit	k5eAaImAgFnP	patřit
k	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
vývozcům	vývozce	k1gMnPc3	vývozce
tohoto	tento	k3xDgInSc2	tento
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Srbsko	Srbsko	k1gNnSc1	Srbsko
stalo	stát	k5eAaPmAgNnS	stát
státem	stát	k1gInSc7	stát
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
těžbou	těžba	k1gFnSc7	těžba
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
srbskému	srbský	k2eAgMnSc3d1	srbský
králi	král	k1gMnSc3	král
Štěpánu	Štěpán	k1gMnSc3	Štěpán
Lazarevičovi	Lazarevič	k1gMnSc3	Lazarevič
udělat	udělat	k5eAaPmF	udělat
z	z	k7c2	z
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
kulturních	kulturní	k2eAgNnPc2d1	kulturní
a	a	k8xC	a
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
měst	město	k1gNnPc2	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Turecké	turecký	k2eAgNnSc1d1	turecké
obléhání	obléhání	k1gNnSc1	obléhání
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
mezi	mezi	k7c4	mezi
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
červencem	červenec	k1gInSc7	červenec
roku	rok	k1gInSc2	rok
1456	[number]	k4	1456
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
hrdinně	hrdinně	k6eAd1	hrdinně
bránilo	bránit	k5eAaImAgNnS	bránit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
počet	počet	k1gInSc1	počet
výpadů	výpad	k1gInPc2	výpad
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
stále	stále	k6eAd1	stále
agresivnějšího	agresivní	k2eAgMnSc2d2	agresivnější
islámského	islámský	k2eAgMnSc2d1	islámský
souseda	soused	k1gMnSc2	soused
se	se	k3xPyFc4	se
zvyšoval	zvyšovat	k5eAaImAgMnS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tureckých	turecký	k2eAgInPc2d1	turecký
nájezdů	nájezd	k1gInPc2	nájezd
řada	řada	k1gFnSc1	řada
vysoce	vysoce	k6eAd1	vysoce
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
,	,	kIx,	,
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
Makedonie	Makedonie	k1gFnSc1	Makedonie
utekla	utéct	k5eAaPmAgFnS	utéct
do	do	k7c2	do
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
důsledku	důsledek	k1gInSc6	důsledek
se	se	k3xPyFc4	se
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
stal	stát	k5eAaPmAgInS	stát
jednotným	jednotný	k2eAgNnSc7d1	jednotné
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
<g/>
,	,	kIx,	,
ne	ne	k9	ne
jen	jen	k9	jen
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
ale	ale	k8xC	ale
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
významných	významný	k2eAgMnPc2d1	významný
lidí	člověk	k1gMnPc2	člověk
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
Bulhar	Bulhar	k1gMnSc1	Bulhar
Konstantin	Konstantin	k1gMnSc1	Konstantin
Filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
osobním	osobní	k2eAgMnSc7d1	osobní
životopiscem	životopisec	k1gMnSc7	životopisec
krále	král	k1gMnSc2	král
Štěpána	Štěpán	k1gMnSc2	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tu	tu	k6eAd1	tu
byla	být	k5eAaImAgFnS	být
pohraniční	pohraniční	k2eAgFnSc4d1	pohraniční
pevnost	pevnost	k1gFnSc4	pevnost
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
však	však	k9	však
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
po	po	k7c6	po
mnoha	mnoho	k4c2	mnoho
neúspěšných	úspěšný	k2eNgFnPc6d1	neúspěšná
pokusech	pokus	k1gInPc6	pokus
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1688	[number]	k4	1688
<g/>
-	-	kIx~	-
<g/>
1690	[number]	k4	1690
<g/>
,	,	kIx,	,
1717	[number]	k4	1717
<g/>
-	-	kIx~	-
<g/>
1739	[number]	k4	1739
a	a	k8xC	a
1789	[number]	k4	1789
<g/>
-	-	kIx~	-
<g/>
1791	[number]	k4	1791
ho	on	k3xPp3gMnSc4	on
drželi	držet	k5eAaImAgMnP	držet
Rakušané	Rakušan	k1gMnPc1	Rakušan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
je	být	k5eAaImIp3nS	být
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
stát	stát	k1gInSc4	stát
autonomní	autonomní	k2eAgFnSc1d1	autonomní
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
tu	tu	k6eAd1	tu
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
přítomna	přítomen	k2eAgFnSc1d1	přítomna
turecká	turecký	k2eAgFnSc1d1	turecká
vojenská	vojenský	k2eAgFnSc1d1	vojenská
posádka	posádka	k1gFnSc1	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
rychlého	rychlý	k2eAgInSc2d1	rychlý
rozvoje	rozvoj	k1gInSc2	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
budovy	budova	k1gFnPc1	budova
jsou	být	k5eAaImIp3nP	být
likvidovány	likvidován	k2eAgFnPc1d1	likvidována
a	a	k8xC	a
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
je	on	k3xPp3gFnPc4	on
moderní	moderní	k2eAgFnPc4d1	moderní
stavby	stavba	k1gFnPc4	stavba
<g/>
,	,	kIx,	,
budované	budovaný	k2eAgInPc4d1	budovaný
v	v	k7c6	v
tehdejších	tehdejší	k2eAgInPc6d1	tehdejší
populárních	populární	k2eAgInPc6d1	populární
slozích	sloh	k1gInPc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Království	království	k1gNnSc1	království
SHS	SHS	kA	SHS
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
neslo	nést	k5eAaImAgNnS	nést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
název	název	k1gInSc4	název
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
následuje	následovat	k5eAaImIp3nS	následovat
další	další	k2eAgFnPc4d1	další
evropské	evropský	k2eAgFnPc4d1	Evropská
metropole	metropol	k1gFnPc4	metropol
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
moderní	moderní	k2eAgFnSc2d1	moderní
dopravních	dopravní	k2eAgNnPc2d1	dopravní
a	a	k8xC	a
komunikační	komunikační	k2eAgFnSc2d1	komunikační
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
je	být	k5eAaImIp3nS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
letiště	letiště	k1gNnSc1	letiště
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
začíná	začínat	k5eAaImIp3nS	začínat
vysílat	vysílat	k5eAaImF	vysílat
Radio	radio	k1gNnSc1	radio
Beograd	Beograd	k1gInSc4	Beograd
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
obsazen	obsazen	k2eAgInSc1d1	obsazen
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1944	[number]	k4	1944
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
obsadila	obsadit	k5eAaPmAgFnS	obsadit
národněosvobozenecká	národněosvobozenecký	k2eAgFnSc1d1	národněosvobozenecký
armáda	armáda	k1gFnSc1	armáda
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
a	a	k8xC	a
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
konaly	konat	k5eAaImAgInP	konat
procesy	proces	k1gInPc1	proces
s	s	k7c7	s
válečnými	válečný	k2eAgMnPc7d1	válečný
zločinci	zločinec	k1gMnPc7	zločinec
<g/>
,	,	kIx,	,
četníky	četník	k1gMnPc7	četník
a	a	k8xC	a
protikomunistickými	protikomunistický	k2eAgMnPc7d1	protikomunistický
aktivisty	aktivista	k1gMnPc7	aktivista
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
boje	boj	k1gInPc1	boj
skončily	skončit	k5eAaPmAgInP	skončit
a	a	k8xC	a
moci	moc	k1gFnSc2	moc
se	se	k3xPyFc4	se
chopili	chopit	k5eAaPmAgMnP	chopit
komunisté	komunista	k1gMnPc1	komunista
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
jako	jako	k8xS	jako
metropole	metropol	k1gFnSc2	metropol
rozšiřován	rozšiřován	k2eAgInSc4d1	rozšiřován
a	a	k8xC	a
modernizován	modernizován	k2eAgInSc4d1	modernizován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
první	první	k4xOgFnSc2	první
pětiletky	pětiletka	k1gFnSc2	pětiletka
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
západně	západně	k6eAd1	západně
od	od	k7c2	od
historického	historický	k2eAgNnSc2d1	historické
města	město	k1gNnSc2	město
na	na	k7c6	na
zelené	zelený	k2eAgFnSc6d1	zelená
louce	louka	k1gFnSc6	louka
stavět	stavět	k5eAaImF	stavět
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
sídliště	sídliště	k1gNnSc4	sídliště
Nový	nový	k2eAgInSc1d1	nový
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
budování	budování	k1gNnSc3	budování
byly	být	k5eAaImAgFnP	být
nasazeny	nasazen	k2eAgFnPc1d1	nasazena
mládežnické	mládežnický	k2eAgFnPc1d1	mládežnická
brigády	brigáda	k1gFnPc1	brigáda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zapojily	zapojit	k5eAaPmAgInP	zapojit
do	do	k7c2	do
mnohých	mnohý	k2eAgFnPc2d1	mnohá
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
přelomu	přelom	k1gInSc2	přelom
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
masové	masový	k2eAgFnSc2d1	masová
výstavby	výstavba	k1gFnSc2	výstavba
nových	nový	k2eAgNnPc2d1	nové
sídlišť	sídliště	k1gNnPc2	sídliště
se	se	k3xPyFc4	se
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
stal	stát	k5eAaPmAgInS	stát
také	také	k6eAd1	také
místem	místo	k1gNnSc7	místo
realizace	realizace	k1gFnSc2	realizace
moderní	moderní	k2eAgFnSc2d1	moderní
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
srbské	srbský	k2eAgFnSc2d1	Srbská
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
vznikaly	vznikat	k5eAaImAgFnP	vznikat
výstavní	výstavní	k2eAgFnPc1d1	výstavní
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
potřebovalo	potřebovat	k5eAaImAgNnS	potřebovat
také	také	k9	také
mnohé	mnohý	k2eAgFnPc4d1	mnohá
kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
společenské	společenský	k2eAgFnPc4d1	společenská
budovy	budova	k1gFnPc4	budova
vystavěné	vystavěný	k2eAgNnSc1d1	vystavěné
s	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
reprezentující	reprezentující	k2eAgFnSc4d1	reprezentující
celou	celý	k2eAgFnSc4d1	celá
federaci	federace	k1gFnSc4	federace
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
velkolepě	velkolepě	k6eAd1	velkolepě
obnovovat	obnovovat	k5eAaImF	obnovovat
chrám	chrám	k1gInSc4	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Sávy	Sáva	k1gMnSc2	Sáva
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
výstavba	výstavba	k1gFnSc1	výstavba
doteď	doteď	k?	doteď
probíhala	probíhat	k5eAaImAgFnS	probíhat
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc4	Dunaj
překročily	překročit	k5eAaPmAgInP	překročit
nové	nový	k2eAgInPc1d1	nový
silniční	silniční	k2eAgInPc1d1	silniční
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
umožnily	umožnit	k5eAaPmAgInP	umožnit
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
severním	severní	k2eAgNnSc7d1	severní
okolím	okolí	k1gNnSc7	okolí
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vůdčí	vůdčí	k2eAgInSc1d1	vůdčí
stát	stát	k1gInSc1	stát
Hnutí	hnutí	k1gNnSc2	hnutí
nezúčastněných	zúčastněný	k2eNgFnPc2d1	nezúčastněná
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
neutrálního	neutrální	k2eAgInSc2d1	neutrální
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
stal	stát	k5eAaPmAgInS	stát
místem	místo	k1gNnSc7	místo
mnohých	mnohý	k2eAgFnPc2d1	mnohá
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
konferencí	konference	k1gFnPc2	konference
a	a	k8xC	a
častých	častý	k2eAgFnPc2d1	častá
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
návštěv	návštěva	k1gFnPc2	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
a	a	k8xC	a
problémy	problém	k1gInPc4	problém
pocítili	pocítit	k5eAaPmAgMnP	pocítit
i	i	k9	i
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
politická	politický	k2eAgFnSc1d1	politická
garnitura	garnitura	k1gFnSc1	garnitura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
hlavně	hlavně	k9	hlavně
srbské	srbský	k2eAgInPc4d1	srbský
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
získávala	získávat	k5eAaImAgFnS	získávat
rychle	rychle	k6eAd1	rychle
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
mítinky	mítink	k1gInPc1	mítink
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Ušće	Ušć	k1gFnSc2	Ušć
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Sávy	Sáva	k1gFnSc2	Sáva
a	a	k8xC	a
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
rozpadu	rozpad	k1gInSc2	rozpad
jugoslávského	jugoslávský	k2eAgInSc2d1	jugoslávský
státu	stát	k1gInSc2	stát
dějištěm	dějiště	k1gNnSc7	dějiště
politických	politický	k2eAgNnPc2d1	politické
shromáždění	shromáždění	k1gNnPc2	shromáždění
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xS	jak
vládních	vládní	k2eAgInPc2d1	vládní
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
opozičních	opoziční	k2eAgFnPc2d1	opoziční
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
Slobodana	Slobodan	k1gMnSc4	Slobodan
Miloševiće	Milošević	k1gFnSc2	Milošević
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
mezinárodním	mezinárodní	k2eAgFnPc3d1	mezinárodní
sankcím	sankce	k1gFnPc3	sankce
a	a	k8xC	a
vyústil	vyústit	k5eAaPmAgMnS	vyústit
až	až	k6eAd1	až
v	v	k7c6	v
bombardování	bombardování	k1gNnSc6	bombardování
města	město	k1gNnSc2	město
vojsky	vojsky	k6eAd1	vojsky
NATO	NATO	kA	NATO
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
těžce	těžce	k6eAd1	těžce
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
významné	významný	k2eAgFnPc1d1	významná
budovy	budova	k1gFnPc1	budova
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
mosty	most	k1gInPc4	most
přes	přes	k7c4	přes
Dunaj	Dunaj	k1gInSc4	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Miloševićův	Miloševićův	k2eAgInSc1d1	Miloševićův
režim	režim	k1gInSc1	režim
byl	být	k5eAaImAgInS	být
svržen	svrhnout	k5eAaPmNgInS	svrhnout
demonstrací	demonstrace	k1gFnSc7	demonstrace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
byl	být	k5eAaImAgMnS	být
obsazen	obsazen	k2eAgMnSc1d1	obsazen
a	a	k8xC	a
vyrabován	vyrabován	k2eAgInSc1d1	vyrabován
jugoslávský	jugoslávský	k2eAgInSc1d1	jugoslávský
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
leží	ležet	k5eAaImIp3nS	ležet
116,75	[number]	k4	116,75
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
při	při	k7c6	při
soutoku	soutok	k1gInSc6	soutok
Sávy	Sáva	k1gFnSc2	Sáva
s	s	k7c7	s
Dunajem	Dunaj	k1gInSc7	Dunaj
na	na	k7c6	na
souřadnicích	souřadnice	k1gFnPc6	souřadnice
44	[number]	k4	44
<g/>
°	°	k?	°
49	[number]	k4	49
<g/>
'	'	kIx"	'
14	[number]	k4	14
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
a	a	k8xC	a
20	[number]	k4	20
<g/>
°	°	k?	°
27	[number]	k4	27
<g/>
'	'	kIx"	'
44	[number]	k4	44
v.	v.	k?	v.
z.	z.	k?	z.
d.	d.	k?	d.
Původní	původní	k2eAgNnSc1d1	původní
Bělehradské	bělehradský	k2eAgNnSc1d1	Bělehradské
historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
pevnosti	pevnost	k1gFnSc2	pevnost
Kalemegdan	Kalemegdana	k1gFnPc2	Kalemegdana
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
rozšiřováno	rozšiřovat	k5eAaImNgNnS	rozšiřovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Sávy	Sáva	k1gFnSc2	Sáva
nové	nový	k2eAgNnSc4d1	nové
město	město	k1gNnSc4	město
v	v	k7c6	v
socialistickém	socialistický	k2eAgInSc6d1	socialistický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
městem	město	k1gNnSc7	město
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
menší	malý	k2eAgInPc1d2	menší
komunity	komunita	k1gFnPc4	komunita
Zemun	Zemuno	k1gNnPc2	Zemuno
<g/>
,	,	kIx,	,
Krnjača	Krnjačum	k1gNnSc2	Krnjačum
a	a	k8xC	a
Ovča	Ovčum	k1gNnSc2	Ovčum
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
360	[number]	k4	360
km	km	kA	km
<g/>
2	[number]	k4	2
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
rozloha	rozloha	k1gFnSc1	rozloha
s	s	k7c7	s
metropolitní	metropolitní	k2eAgFnSc7d1	metropolitní
oblastí	oblast	k1gFnSc7	oblast
činí	činit	k5eAaImIp3nS	činit
3	[number]	k4	3
223	[number]	k4	223
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
tvořil	tvořit	k5eAaImAgInS	tvořit
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
důležitou	důležitý	k2eAgFnSc4d1	důležitá
křižovatku	křižovatka	k1gFnSc4	křižovatka
mezi	mezi	k7c7	mezi
západem	západ	k1gInSc7	západ
a	a	k8xC	a
Orientem	Orient	k1gInSc7	Orient
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Sávy	Sáva	k1gFnSc2	Sáva
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc4	město
kopcovitý	kopcovitý	k2eAgInSc1d1	kopcovitý
terén	terén	k1gInSc1	terén
<g/>
,	,	kIx,	,
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Torlak	Torlak	k1gInSc1	Torlak
kopec	kopec	k1gInSc1	kopec
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
303	[number]	k4	303
m.	m.	k?	m.
</s>
<s>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
má	mít	k5eAaImIp3nS	mít
mírné	mírný	k2eAgNnSc1d1	mírné
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
čtyři	čtyři	k4xCgNnPc4	čtyři
roční	roční	k2eAgNnPc4d1	roční
období	období	k1gNnPc4	období
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozloženy	rozložit	k5eAaPmNgFnP	rozložit
srážky	srážka	k1gFnPc1	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
<g/>
,	,	kIx,	,
7	[number]	k4	7
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nejteplejším	teplý	k2eAgInSc7d3	nejteplejší
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
červenec	červenec	k1gInSc1	červenec
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
teplotou	teplota	k1gFnSc7	teplota
22	[number]	k4	22
<g/>
,	,	kIx,	,
1	[number]	k4	1
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
činí	činit	k5eAaImIp3nS	činit
zhruba	zhruba	k6eAd1	zhruba
700	[number]	k4	700
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
2	[number]	k4	2
096	[number]	k4	096
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
<g/>
Nejslunnější	slunný	k2eAgInPc1d3	nejslunnější
měsíce	měsíc	k1gInPc1	měsíc
jsou	být	k5eAaImIp3nP	být
červenec	červenec	k1gInSc4	červenec
a	a	k8xC	a
srpen	srpen	k1gInSc4	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
oficiálně	oficiálně	k6eAd1	oficiálně
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
byla	být	k5eAaImAgFnS	být
43	[number]	k4	43
<g/>
,	,	kIx,	,
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgMnSc1d3	nejnižší
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1893	[number]	k4	1893
činila	činit	k5eAaImAgFnS	činit
-	-	kIx~	-
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
<s>
Jako	jako	k8xS	jako
jediné	jediný	k2eAgNnSc1d1	jediné
město	město	k1gNnSc1	město
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nad	nad	k7c4	nad
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
má	mít	k5eAaImIp3nS	mít
Bělehrad	Bělehrad	k1gInSc4	Bělehrad
dominantní	dominantní	k2eAgNnSc1d1	dominantní
postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
<g/>
,	,	kIx,	,
politickým	politický	k2eAgNnSc7d1	politické
i	i	k8xC	i
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
je	být	k5eAaImIp3nS	být
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejrozvinutější	rozvinutý	k2eAgFnSc7d3	nejrozvinutější
oblastí	oblast	k1gFnSc7	oblast
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
%	%	kIx~	%
HDP	HDP	kA	HDP
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
právě	právě	k9	právě
v	v	k7c6	v
metropoli	metropol	k1gFnSc6	metropol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
30	[number]	k4	30
%	%	kIx~	%
veškeré	veškerý	k3xTgFnSc2	veškerý
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
Air	Air	k1gFnSc1	Air
Serbia	Serbia	k1gFnSc1	Serbia
<g/>
,	,	kIx,	,
Telekom	Telekom	k1gInSc1	Telekom
Srbija	Srbija	k1gFnSc1	Srbija
<g/>
,	,	kIx,	,
Telenor	Telenor	k1gMnSc1	Telenor
Srbija	Srbija	k1gMnSc1	Srbija
<g/>
,	,	kIx,	,
Delta	delta	k1gFnSc1	delta
Holding	holding	k1gInSc1	holding
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc1d1	významné
také	také	k9	také
z	z	k7c2	z
dopravního	dopravní	k2eAgNnSc2d1	dopravní
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Křižují	křižovat	k5eAaImIp3nP	křižovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
panevropské	panevropský	k2eAgInPc1d1	panevropský
koridory	koridor	k1gInPc1	koridor
<g/>
,	,	kIx,	,
ústí	ústit	k5eAaImIp3nP	ústit
sem	sem	k6eAd1	sem
mnohé	mnohý	k2eAgFnPc1d1	mnohá
železniční	železniční	k2eAgFnPc1d1	železniční
tratě	trať	k1gFnPc1	trať
<g/>
,	,	kIx,	,
poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
dvou	dva	k4xCgFnPc2	dva
řek	řeka	k1gFnPc2	řeka
je	být	k5eAaImIp3nS	být
klíčová	klíčový	k2eAgFnSc1d1	klíčová
pro	pro	k7c4	pro
vnitrozemskou	vnitrozemský	k2eAgFnSc4d1	vnitrozemská
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Surčinu	Surčina	k1gFnSc4	Surčina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Nikoly	Nikola	k1gMnSc2	Nikola
Tesly	Tesla	k1gFnSc2	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
Metropole	metropole	k1gFnSc1	metropole
má	mít	k5eAaImIp3nS	mít
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
systém	systém	k1gInSc4	systém
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
síť	síť	k1gFnSc1	síť
<g/>
;	;	kIx,	;
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
trvají	trvat	k5eAaImIp3nP	trvat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
vedou	vést	k5eAaImIp3nP	vést
dálnice	dálnice	k1gFnPc4	dálnice
do	do	k7c2	do
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
<g/>
,	,	kIx,	,
Subotice	Subotice	k1gFnSc2	Subotice
<g/>
,	,	kIx,	,
Noviho	Novi	k1gMnSc2	Novi
Sadu	sad	k1gInSc2	sad
<g/>
,	,	kIx,	,
Podgorice	Podgorika	k1gFnSc6	Podgorika
<g/>
,	,	kIx,	,
Prištiny	Priština	k1gFnPc1	Priština
a	a	k8xC	a
do	do	k7c2	do
Skopje	Skopje	k1gFnSc2	Skopje
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bělehradských	bělehradský	k2eAgNnPc6d1	Bělehradské
předměstích	předměstí	k1gNnPc6	předměstí
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
četné	četný	k2eAgInPc1d1	četný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
většinou	většinou	k6eAd1	většinou
nevelké	velký	k2eNgInPc4d1	nevelký
strojírenské	strojírenský	k2eAgInPc4d1	strojírenský
závody	závod	k1gInPc4	závod
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
obráběcích	obráběcí	k2eAgInPc2d1	obráběcí
a	a	k8xC	a
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
strojů	stroj	k1gInPc2	stroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
podniky	podnik	k1gInPc4	podnik
textilní	textilní	k2eAgInPc4d1	textilní
<g/>
,	,	kIx,	,
kožedělné	kožedělný	k2eAgInPc4d1	kožedělný
<g/>
,	,	kIx,	,
dřevozpracující	dřevozpracující	k2eAgInPc4d1	dřevozpracující
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgInPc4d1	chemický
a	a	k8xC	a
potravinářské	potravinářský	k2eAgInPc4d1	potravinářský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
Smedereva	Smederevo	k1gNnSc2	Smederevo
a	a	k8xC	a
Pančeva	Pančevo	k1gNnSc2	Pančevo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
1	[number]	k4	1
166	[number]	k4	166
763	[number]	k4	763
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
1	[number]	k4	1
659	[number]	k4	659
440	[number]	k4	440
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
tvoří	tvořit	k5eAaImIp3nP	tvořit
Srbové	Srb	k1gMnPc1	Srb
(	(	kIx(	(
<g/>
1	[number]	k4	1
505	[number]	k4	505
448	[number]	k4	448
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Romové	Rom	k1gMnPc1	Rom
(	(	kIx(	(
<g/>
27	[number]	k4	27
325	[number]	k4	325
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Černohorci	Černohorec	k1gMnPc1	Černohorec
(	(	kIx(	(
<g/>
9	[number]	k4	9
902	[number]	k4	902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jugoslávci	Jugoslávec	k1gMnPc1	Jugoslávec
(	(	kIx(	(
<g/>
8	[number]	k4	8
0	[number]	k4	0
<g/>
61	[number]	k4	61
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chorvaté	Chorvat	k1gMnPc1	Chorvat
(	(	kIx(	(
<g/>
7	[number]	k4	7
752	[number]	k4	752
<g/>
)	)	kIx)	)
a	a	k8xC	a
Makedonci	Makedonec	k1gMnSc3	Makedonec
(	(	kIx(	(
<g/>
6	[number]	k4	6
970	[number]	k4	970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
sem	sem	k6eAd1	sem
přišlo	přijít	k5eAaPmAgNnS	přijít
z	z	k7c2	z
ekonomických	ekonomický	k2eAgMnPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
z	z	k7c2	z
menších	malý	k2eAgNnPc2d2	menší
měst	město	k1gNnPc2	město
a	a	k8xC	a
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
stovky	stovka	k1gFnPc1	stovka
tisíc	tisíc	k4xCgInSc4	tisíc
jich	on	k3xPp3gMnPc2	on
sem	sem	k6eAd1	sem
přišly	přijít	k5eAaPmAgInP	přijít
jako	jako	k8xC	jako
uprchlíci	uprchlík	k1gMnPc1	uprchlík
z	z	k7c2	z
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
a	a	k8xC	a
Kosova	Kosův	k2eAgInSc2d1	Kosův
následkem	následkem	k7c2	následkem
jugoslávských	jugoslávský	k2eAgFnPc2d1	jugoslávská
válek	válka	k1gFnPc2	válka
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
začali	začít	k5eAaPmAgMnP	začít
stěhovat	stěhovat	k5eAaImF	stěhovat
i	i	k9	i
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
dnes	dnes	k6eAd1	dnes
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
mezi	mezi	k7c7	mezi
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
v	v	k7c6	v
Novém	Nový	k1gMnSc6	Nový
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
Blok	blok	k1gInSc4	blok
70	[number]	k4	70
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hovorově	hovorově	k6eAd1	hovorově
říká	říkat	k5eAaImIp3nS	říkat
čínská	čínský	k2eAgFnSc1d1	čínská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studie	k1gFnPc6	studie
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
žít	žít	k5eAaImF	žít
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc2	Írán
<g/>
,	,	kIx,	,
Jordánska	Jordánsko	k1gNnSc2	Jordánsko
a	a	k8xC	a
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
v	v	k7c6	v
Novém	Nový	k1gMnSc6	Nový
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
usadilo	usadit	k5eAaPmAgNnS	usadit
několik	několik	k4yIc1	několik
kurdských	kurdský	k2eAgFnPc2d1	kurdská
rodin	rodina	k1gFnPc2	rodina
z	z	k7c2	z
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
1	[number]	k4	1
429	[number]	k4	429
170	[number]	k4	170
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
pravoslaví	pravoslaví	k1gNnSc3	pravoslaví
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
16	[number]	k4	16
305	[number]	k4	305
k	k	k7c3	k
Římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
3	[number]	k4	3
796	[number]	k4	796
k	k	k7c3	k
protestantismu	protestantismus	k1gInSc3	protestantismus
a	a	k8xC	a
3	[number]	k4	3
996	[number]	k4	996
je	být	k5eAaImIp3nS	být
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
zde	zde	k6eAd1	zde
měla	mít	k5eAaImAgFnS	mít
až	až	k9	až
10	[number]	k4	10
000	[number]	k4	000
příslušníků	příslušník	k1gMnPc2	příslušník
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
vlně	vlna	k1gFnSc6	vlna
emigrace	emigrace	k1gFnSc2	emigrace
jich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
asi	asi	k9	asi
2	[number]	k4	2
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
každoročně	každoročně	k6eAd1	každoročně
hostí	hostit	k5eAaImIp3nS	hostit
několik	několik	k4yIc1	několik
významných	významný	k2eAgFnPc2d1	významná
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
festivalů	festival	k1gInPc2	festival
(	(	kIx(	(
<g/>
FEST	fest	k6eAd1	fest
-	-	kIx~	-
Bělehradský	bělehradský	k2eAgInSc1d1	bělehradský
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
BITEF	BITEF	kA	BITEF
-	-	kIx~	-
Bělehradský	bělehradský	k2eAgInSc1d1	bělehradský
divadelní	divadelní	k2eAgInSc1d1	divadelní
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
BEMUS	BEMUS	kA	BEMUS
-	-	kIx~	-
Bělehradský	bělehradský	k2eAgInSc1d1	bělehradský
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
veletrhy	veletrh	k1gInPc1	veletrh
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
..	..	k?	..
Ivo	Ivo	k1gMnSc1	Ivo
Andrić	Andrić	k1gMnSc1	Andrić
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
zde	zde	k6eAd1	zde
napsal	napsat	k5eAaPmAgMnS	napsat
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
děl	dělo	k1gNnPc2	dělo
-	-	kIx~	-
Most	most	k1gInSc1	most
na	na	k7c4	na
Drině	Drině	k1gNnSc4	Drině
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
velkým	velký	k2eAgMnPc3d1	velký
bělehradským	bělehradský	k2eAgMnPc3d1	bělehradský
spisovatelům	spisovatel	k1gMnPc3	spisovatel
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Branislav	Branislav	k1gMnSc1	Branislav
Nušić	Nušić	k1gMnSc1	Nušić
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Crnjanski	Crnjansk	k1gFnSc2	Crnjansk
<g/>
,	,	kIx,	,
Meša	Meša	k1gMnSc1	Meša
Selimović	Selimović	k1gMnSc1	Selimović
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
srbského	srbský	k2eAgInSc2d1	srbský
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
je	být	k5eAaImIp3nS	být
film	film	k1gInSc4	film
oceněný	oceněný	k2eAgInSc4d1	oceněný
v	v	k7c6	v
roku	rok	k1gInSc6	rok
1995	[number]	k4	1995
na	na	k7c6	na
cenách	cena	k1gFnPc6	cena
Palme	pálit	k5eAaImRp1nP	pálit
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Or	Or	k1gMnSc1	Or
s	s	k7c7	s
názvem	název	k1gInSc7	název
Underground	underground	k1gInSc1	underground
(	(	kIx(	(
<g/>
Podzemí	podzemí	k1gNnSc1	podzemí
<g/>
)	)	kIx)	)
od	od	k7c2	od
režiséra	režisér	k1gMnSc2	režisér
Emira	Emir	k1gMnSc2	Emir
Kusturicy	Kusturica	k1gMnSc2	Kusturica
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
význačným	význačný	k2eAgFnPc3d1	význačná
rockovým	rockový	k2eAgFnPc3d1	rocková
skupinám	skupina	k1gFnPc3	skupina
patří	patřit	k5eAaImIp3nS	patřit
Riblja	Riblj	k2eAgFnSc1d1	Riblj
Čorba	Čorba	k1gFnSc1	Čorba
<g/>
,	,	kIx,	,
Bajaga	Bajaga	k1gFnSc1	Bajaga
i	i	k8xC	i
Instruktori	Instruktor	k1gFnPc1	Instruktor
aj.	aj.	kA	aj.
Během	během	k7c2	během
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
hlavním	hlavní	k2eAgInSc7d1	hlavní
centrem	centr	k1gInSc7	centr
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
turbofolk	turbofolka	k1gFnPc2	turbofolka
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
k	k	k7c3	k
jehož	jehož	k3xOyRp3gInPc3	jehož
nejznámějším	známý	k2eAgInPc3d3	nejznámější
představitelům	představitel	k1gMnPc3	představitel
patřila	patřit	k5eAaImAgFnS	patřit
Ceca	Ceca	k1gFnSc1	Ceca
Ražnatovićová	Ražnatovićová	k1gFnSc1	Ražnatovićová
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
centrem	centr	k1gInSc7	centr
srbské	srbský	k2eAgFnSc2d1	Srbská
hip-hopové	hipopový	k2eAgFnSc2d1	hip-hopová
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nP	působit
zde	zde	k6eAd1	zde
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xC	jako
Beogradski	Beogradsk	k1gMnPc1	Beogradsk
Sindikat	Sindikat	k1gFnSc2	Sindikat
<g/>
,	,	kIx,	,
Škabo	Škaba	k1gFnSc5	Škaba
<g/>
,	,	kIx,	,
Marčelo	Marčelo	k1gFnSc5	Marčelo
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
přední	přední	k2eAgNnPc1d1	přední
místa	místo	k1gNnPc1	místo
mají	mít	k5eAaImIp3nP	mít
Srbské	srbský	k2eAgNnSc4d1	srbské
národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
Jugoslávské	jugoslávský	k2eAgNnSc4d1	jugoslávské
dramatické	dramatický	k2eAgNnSc4d1	dramatické
divadlo	divadlo	k1gNnSc4	divadlo
nebo	nebo	k8xC	nebo
Ateliér	ateliér	k1gInSc4	ateliér
212	[number]	k4	212
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bělehrade	Bělehrad	k1gInSc5	Bělehrad
sídli	sídlet	k5eAaImRp2nS	sídlet
Srbská	srbský	k2eAgFnSc1d1	Srbská
akademie	akademie	k1gFnSc2	akademie
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
nebo	nebo	k8xC	nebo
Srbská	srbský	k2eAgFnSc1d1	Srbská
národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Srbské	srbský	k2eAgNnSc1d1	srbské
národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
a	a	k8xC	a
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
nabízí	nabízet	k5eAaImIp3nS	nabízet
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
000	[number]	k4	000
exponátů	exponát	k1gInPc2	exponát
<g/>
.	.	kIx.	.
</s>
<s>
Mj.	mj.	kA	mj.
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
rukopis	rukopis	k1gInSc1	rukopis
Miroslavljevo	Miroslavljevo	k1gNnSc1	Miroslavljevo
Jevanđelje	Jevanđelj	k1gFnSc2	Jevanđelj
(	(	kIx(	(
<g/>
Miroslavovo	Miroslavův	k2eAgNnSc1d1	Miroslavovo
evangelium	evangelium	k1gNnSc1	evangelium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
muzeum	muzeum	k1gNnSc1	muzeum
nadchne	nadchnout	k5eAaPmIp3nS	nadchnout
především	především	k9	především
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
turisty	turist	k1gMnPc4	turist
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
exponátům	exponát	k1gInPc3	exponát
muzea	muzeum	k1gNnSc2	muzeum
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
letecká	letecký	k2eAgFnSc1d1	letecká
stíhačka	stíhačka	k1gFnSc1	stíhačka
Lockheed	Lockheed	k1gMnSc1	Lockheed
F-117	F-117	k1gMnSc1	F-117
Nighthawk	Nighthawk	k1gMnSc1	Nighthawk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
sestřelená	sestřelený	k2eAgFnSc1d1	sestřelená
jugoslávskou	jugoslávský	k2eAgFnSc7d1	jugoslávská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
nachází	nacházet	k5eAaImIp3nS	nacházet
přes	přes	k7c4	přes
25	[number]	k4	25
000	[number]	k4	000
exponátů	exponát	k1gInPc2	exponát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
Doby	doba	k1gFnSc2	doba
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Jugoslávské	jugoslávský	k2eAgNnSc1d1	jugoslávské
letecké	letecký	k2eAgNnSc1d1	letecké
muzeum	muzeum	k1gNnSc1	muzeum
nabízí	nabízet	k5eAaImIp3nS	nabízet
přes	přes	k7c4	přes
200	[number]	k4	200
letounů	letoun	k1gInPc2	letoun
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k8xC	i
takové	takový	k3xDgInPc1	takový
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
na	na	k7c6	na
světě	svět	k1gInSc6	svět
-	-	kIx~	-
např.	např.	kA	např.
Fiat	fiat	k1gInSc1	fiat
G	G	kA	G
<g/>
.50	.50	k4	.50
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
letouny	letoun	k1gInPc1	letoun
USA	USA	kA	USA
a	a	k8xC	a
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
sestřelené	sestřelený	k2eAgFnSc2d1	sestřelená
během	během	k7c2	během
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Etnografické	etnografický	k2eAgNnSc1d1	etnografické
muzeum	muzeum	k1gNnSc1	muzeum
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
více	hodně	k6eAd2	hodně
než	než	k8xS	než
150	[number]	k4	150
000	[number]	k4	000
exponátů	exponát	k1gInPc2	exponát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
vesnickou	vesnický	k2eAgFnSc4d1	vesnická
a	a	k8xC	a
městskou	městský	k2eAgFnSc4d1	městská
kulturu	kultura	k1gFnSc4	kultura
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
,	,	kIx,	,
především	především	k9	především
však	však	k9	však
zemí	zem	k1gFnPc2	zem
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
i	i	k9	i
Muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nyní	nyní	k6eAd1	nyní
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
přes	přes	k7c4	přes
8540	[number]	k4	8540
děl	dělo	k1gNnPc2	dělo
srbských	srbský	k2eAgMnPc2d1	srbský
i	i	k8xC	i
světových	světový	k2eAgMnPc2d1	světový
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Nikoly	Nikola	k1gMnSc2	Nikola
Tesly	Tesla	k1gFnSc2	Tesla
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
a	a	k8xC	a
hostí	hostit	k5eAaImIp3nS	hostit
osobní	osobní	k2eAgInPc4d1	osobní
předměty	předmět	k1gInPc4	předmět
vynálezce	vynálezce	k1gMnSc2	vynálezce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
leží	ležet	k5eAaImIp3nS	ležet
přes	přes	k7c4	přes
160	[number]	k4	160
000	[number]	k4	000
originálních	originální	k2eAgInPc2d1	originální
dokumentů	dokument	k1gInPc2	dokument
a	a	k8xC	a
okolo	okolo	k7c2	okolo
5	[number]	k4	5
700	[number]	k4	700
jiných	jiný	k2eAgInPc2d1	jiný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
muzeum	muzeum	k1gNnSc4	muzeum
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Muzeum	muzeum	k1gNnSc1	muzeum
Vuk	Vuk	k1gMnSc1	Vuk
a	a	k8xC	a
Dositej	Dositej	k1gFnSc1	Dositej
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
letectví	letectví	k1gNnSc2	letectví
nebo	nebo	k8xC	nebo
velice	velice	k6eAd1	velice
známé	známý	k2eAgNnSc1d1	známé
Muzeum	muzeum	k1gNnSc1	muzeum
Afrického	africký	k2eAgNnSc2d1	africké
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvě	dva	k4xCgFnPc4	dva
státní	státní	k2eAgFnPc4d1	státní
a	a	k8xC	a
více	hodně	k6eAd2	hodně
soukromých	soukromý	k2eAgFnPc2d1	soukromá
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Bělehradská	bělehradský	k2eAgFnSc1d1	Bělehradská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
У	У	k?	У
у	у	k?	у
Б	Б	k?	Б
<g/>
,	,	kIx,	,
Univerzitet	Univerzitet	k1gInSc1	Univerzitet
u	u	k7c2	u
Beogradu	Beograd	k1gInSc2	Beograd
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
jako	jako	k8xC	jako
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
akademie	akademie	k1gFnSc1	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
vzdělávacím	vzdělávací	k2eAgFnPc3d1	vzdělávací
institucím	instituce	k1gFnPc3	instituce
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
studuje	studovat	k5eAaImIp3nS	studovat
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
přibližně	přibližně	k6eAd1	přibližně
90	[number]	k4	90
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
195	[number]	k4	195
základních	základní	k2eAgNnPc2d1	základní
a	a	k8xC	a
85	[number]	k4	85
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
14	[number]	k4	14
speciálních	speciální	k2eAgMnPc2d1	speciální
<g/>
,	,	kIx,	,
15	[number]	k4	15
hudebních	hudební	k2eAgFnPc2d1	hudební
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
4	[number]	k4	4
školy	škola	k1gFnPc4	škola
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Dedinje	Dedinj	k1gFnSc2	Dedinj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
komplex	komplex	k1gInSc1	komplex
královských	královský	k2eAgFnPc2d1	královská
rezidencí	rezidence	k1gFnPc2	rezidence
<g/>
,	,	kIx,	,
dělících	dělící	k2eAgFnPc2d1	dělící
se	se	k3xPyFc4	se
na	na	k7c4	na
Královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
a	a	k8xC	a
Bílý	bílý	k2eAgInSc1d1	bílý
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
sídlem	sídlo	k1gNnSc7	sídlo
pro	pro	k7c4	pro
současnou	současný	k2eAgFnSc4d1	současná
srbskou	srbský	k2eAgFnSc4d1	Srbská
královskou	královský	k2eAgFnSc4d1	královská
rodinu	rodina	k1gFnSc4	rodina
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
Alexandra	Alexandr	k1gMnSc2	Alexandr
III	III	kA	III
<g/>
..	..	k?	..
Chrám	chrám	k1gInSc1	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Marka	Marek	k1gMnSc2	Marek
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
pravoslavných	pravoslavný	k2eAgInPc2d1	pravoslavný
chrámů	chrám	k1gInPc2	chrám
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
Srbského	srbský	k2eAgInSc2d1	srbský
parlamentu	parlament	k1gInSc2	parlament
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
třídě	třída	k1gFnSc6	třída
Bulvár	bulvár	k1gInSc4	bulvár
krále	král	k1gMnSc2	král
Alexandra	Alexandr	k1gMnSc2	Alexandr
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Sávy	Sáva	k1gMnSc2	Sáva
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Avalská	Avalský	k2eAgFnSc1d1	Avalský
televizní	televizní	k2eAgFnSc1d1	televizní
věž	věž	k1gFnSc1	věž
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
kopci	kopec	k1gInSc6	kopec
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
204,5	[number]	k4	204,5
m.	m.	k?	m.
Stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původní	původní	k2eAgFnSc2d1	původní
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
zničené	zničený	k2eAgNnSc4d1	zničené
bombardováním	bombardování	k1gNnSc7	bombardování
NATO	NATO	kA	NATO
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
kapitána	kapitán	k1gMnSc2	kapitán
Miši	Miš	k1gFnSc2	Miš
dnes	dnes	k6eAd1	dnes
hostí	hostit	k5eAaImIp3nS	hostit
rektorát	rektorát	k1gInSc1	rektorát
Bělehradské	bělehradský	k2eAgFnSc2d1	Bělehradská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Genex	Genex	k1gInSc1	Genex
Tower	Tower	k1gInSc1	Tower
je	být	k5eAaImIp3nS	být
bělehradský	bělehradský	k2eAgInSc1d1	bělehradský
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
<g/>
,	,	kIx,	,
měřící	měřící	k2eAgFnSc1d1	měřící
115	[number]	k4	115
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
třetí	třetí	k4xOgFnSc1	třetí
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
Srbska	Srbsko	k1gNnSc2	Srbsko
je	být	k5eAaImIp3nS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
vládní	vládní	k2eAgFnSc1d1	vládní
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
hostící	hostící	k2eAgMnSc1d1	hostící
6	[number]	k4	6
vládních	vládní	k2eAgNnPc2d1	vládní
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
palác	palác	k1gInSc1	palác
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
ulic	ulice	k1gFnPc2	ulice
Krále	Král	k1gMnSc2	Král
Milana	Milan	k1gMnSc2	Milan
a	a	k8xC	a
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Jovanoviće	Jovanović	k1gMnSc2	Jovanović
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
a	a	k8xC	a
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
dynastie	dynastie	k1gFnSc2	dynastie
Obrenovićů	Obrenović	k1gInPc2	Obrenović
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Bělehradská	bělehradský	k2eAgFnSc1d1	Bělehradská
aréna	aréna	k1gFnSc1	aréna
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2004	[number]	k4	2004
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
výstavby	výstavba	k1gFnSc2	výstavba
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
EUR	euro	k1gNnPc2	euro
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
ji	on	k3xPp3gFnSc4	on
Vlada	Vlada	k1gMnSc1	Vlada
Slavica	slavicum	k1gNnSc2	slavicum
<g/>
.	.	kIx.	.
</s>
<s>
Beogradska	Beogradska	k1gFnSc1	Beogradska
zadruga	zadruga	k1gFnSc1	zadruga
<g/>
,	,	kIx,	,
secesní	secesní	k2eAgFnSc1d1	secesní
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
nejstarší	starý	k2eAgFnSc1d3	nejstarší
banka	banka	k1gFnSc1	banka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
mnoho	mnoho	k4c1	mnoho
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
mešita	mešita	k1gFnSc1	mešita
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
menších	malý	k2eAgInPc2d2	menší
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
,	,	kIx,	,
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
akademií	akademie	k1gFnPc2	akademie
<g/>
,	,	kIx,	,
vládních	vládní	k2eAgFnPc2d1	vládní
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
<s>
Podgorica	Podgorica	k1gFnSc1	Podgorica
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
Skopje	Skopje	k1gFnSc1	Skopje
<g/>
,	,	kIx,	,
Makedonie	Makedonie	k1gFnSc1	Makedonie
Lublaň	Lublaň	k1gFnSc1	Lublaň
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
City	City	k1gFnSc1	City
<g/>
,	,	kIx,	,
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
Láhaur	Láhaur	k1gInSc1	Láhaur
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
Havana	Havana	k1gFnSc1	Havana
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
Coventry	Coventr	k1gInPc4	Coventr
<g/>
,	,	kIx,	,
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc2	Británie
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
