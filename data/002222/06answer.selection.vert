<s>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
strategickém	strategický	k2eAgInSc6d1	strategický
soutoku	soutok	k1gInSc6	soutok
dvou	dva	k4xCgFnPc2	dva
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
starým	starý	k2eAgNnPc3d1	staré
městům	město	k1gNnPc3	město
a	a	k8xC	a
místům	místo	k1gNnPc3	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kontinuální	kontinuální	k2eAgNnPc4d1	kontinuální
osídlení	osídlení	k1gNnPc4	osídlení
trvá	trvat	k5eAaImIp3nS	trvat
již	již	k9	již
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
