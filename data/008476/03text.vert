<p>
<s>
Scottie	Scottie	k1gFnSc2	Scottie
Pippen	Pippen	k2eAgMnSc1d1	Pippen
(	(	kIx(	(
<g/>
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Scott	Scott	k1gMnSc1	Scott
Maurice	Maurika	k1gFnSc3	Maurika
Pippen	Pippen	k2eAgInSc1d1	Pippen
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
americký	americký	k2eAgMnSc1d1	americký
profesionální	profesionální	k2eAgMnSc1d1	profesionální
basketbalista	basketbalista	k1gMnSc1	basketbalista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pippen	Pippen	k2eAgMnSc1d1	Pippen
proslul	proslout	k5eAaPmAgMnS	proslout
především	především	k6eAd1	především
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
hvězda	hvězda	k1gFnSc1	hvězda
číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
<g/>
"	"	kIx"	"
týmu	tým	k1gInSc2	tým
Chicago	Chicago	k1gNnSc1	Chicago
Bulls	Bulls	k1gInSc1	Bulls
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Michaela	Michael	k1gMnSc2	Michael
Jordana	Jordan	k1gMnSc2	Jordan
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
a	a	k8xC	a
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Bulls	Bulls	k1gInSc4	Bulls
získali	získat	k5eAaPmAgMnP	získat
šest	šest	k4xCc4	šest
titulů	titul	k1gInPc2	titul
vítěze	vítěz	k1gMnSc2	vítěz
NBA	NBA	kA	NBA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
NBA	NBA	kA	NBA
hrál	hrát	k5eAaImAgMnS	hrát
postupně	postupně	k6eAd1	postupně
za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
týmy	tým	k1gInPc4	tým
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
Bulls	Bullsa	k1gFnPc2	Bullsa
<g/>
:	:	kIx,	:
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Houston	Houston	k1gInSc1	Houston
Rockets	Rockets	k1gInSc1	Rockets
<g/>
:	:	kIx,	:
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Portland	Portland	k1gInSc1	Portland
Trail	Traila	k1gFnPc2	Traila
Blazers	Blazersa	k1gFnPc2	Blazersa
<g/>
:	:	kIx,	:
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
znovu	znovu	k6eAd1	znovu
Chicago	Chicago	k1gNnSc1	Chicago
Bulls	Bullsa	k1gFnPc2	Bullsa
<g/>
:	:	kIx,	:
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
</s>
<s>
Scottie	Scottie	k1gFnSc2	Scottie
Pippen	Pippen	k2eAgMnSc1d1	Pippen
získal	získat	k5eAaPmAgMnS	získat
s	s	k7c7	s
reprezentačním	reprezentační	k2eAgInSc7d1	reprezentační
týmem	tým	k1gInSc7	tým
USA	USA	kA	USA
dvě	dva	k4xCgFnPc4	dva
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
-	-	kIx~	-
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
a	a	k8xC	a
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
kariéry	kariéra	k1gFnSc2	kariéra
profesionálního	profesionální	k2eAgMnSc2d1	profesionální
hráče	hráč	k1gMnSc2	hráč
pracoval	pracovat	k5eAaImAgMnS	pracovat
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
analytik	analytik	k1gMnSc1	analytik
pro	pro	k7c4	pro
Bulls	Bulls	k1gInSc4	Bulls
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
asistent	asistent	k1gMnSc1	asistent
trenéra	trenér	k1gMnSc2	trenér
pro	pro	k7c4	pro
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
Lakers	Lakersa	k1gFnPc2	Lakersa
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
komentátorem	komentátor	k1gMnSc7	komentátor
basketbalových	basketbalový	k2eAgInPc2d1	basketbalový
přenosů	přenos	k1gInPc2	přenos
v	v	k7c6	v
ABC	ABC	kA	ABC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Scottie	Scottie	k1gFnSc2	Scottie
Pippen	Pippen	k2eAgMnSc1d1	Pippen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
statistiky	statistika	k1gFnPc1	statistika
na	na	k7c6	na
basketball-reference	basketballeferenka	k1gFnSc6	basketball-referenka
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
