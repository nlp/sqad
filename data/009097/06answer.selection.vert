<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
trojhvězd	trojhvězda	k1gFnPc2	trojhvězda
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
kolem	kolem	k7c2	kolem
hvězdy	hvězda	k1gFnSc2	hvězda
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnSc3d1	známá
i	i	k9	i
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Toliman	Toliman	k1gMnSc1	Toliman
<g/>
.	.	kIx.	.
</s>
