<s>
Igor	Igor	k1gMnSc1	Igor
Jevgeněvič	Jevgeněvič	k1gMnSc1	Jevgeněvič
Tamm	Tamm	k1gMnSc1	Tamm
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
И	И	k?	И
<g/>
́	́	k?	́
<g/>
г	г	k?	г
Е	Е	k?	Е
<g/>
́	́	k?	́
<g/>
н	н	k?	н
Т	Т	k?	Т
<g/>
,	,	kIx,	,
Igor	Igor	k1gMnSc1	Igor
Jevgeňjevič	Jevgeňjevič	k1gMnSc1	Jevgeňjevič
Tamm	Tamm	k1gMnSc1	Tamm
<g/>
;	;	kIx,	;
26	[number]	k4	26
<g/>
.	.	kIx.	.
červnajul	červnajout	k5eAaPmAgInS	červnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1895	[number]	k4	1895
<g/>
greg	grega	k1gFnPc2	grega
Vladivostok	Vladivostok	k1gInSc1	Vladivostok
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1971	[number]	k4	1971
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sovětský	sovětský	k2eAgMnSc1d1	sovětský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
sovětským	sovětský	k2eAgMnSc7d1	sovětský
fyzikem	fyzik	k1gMnSc7	fyzik
Iľjou	Iľja	k1gMnSc7	Iľja
Michajlovičem	Michajlovič	k1gMnSc7	Michajlovič
Frankem	Frank	k1gMnSc7	Frank
na	na	k7c6	na
základě	základ	k1gInSc6	základ
klasické	klasický	k2eAgFnSc2d1	klasická
elektrodynamiky	elektrodynamika	k1gFnSc2	elektrodynamika
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
přesnou	přesný	k2eAgFnSc4d1	přesná
teorii	teorie	k1gFnSc4	teorie
vzniku	vznik	k1gInSc2	vznik
Čerenkovova	Čerenkovův	k2eAgNnSc2d1	Čerenkovovo
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
Alexejevičem	Alexejevič	k1gMnSc7	Alexejevič
Čerenkovem	Čerenkov	k1gInSc7	Čerenkov
a	a	k8xC	a
Iľjou	Iľja	k1gMnSc7	Iľja
Frankem	Frank	k1gMnSc7	Frank
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
studoval	studovat	k5eAaImAgMnS	studovat
fyziku	fyzika	k1gFnSc4	fyzika
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
dokončil	dokončit	k5eAaPmAgInS	dokončit
své	svůj	k3xOyFgNnSc4	svůj
studium	studium	k1gNnSc4	studium
v	v	k7c6	v
r.	r.	kA	r.
1918	[number]	k4	1918
na	na	k7c6	na
Moskevské	moskevský	k2eAgFnSc6d1	Moskevská
státní	státní	k2eAgFnSc6d1	státní
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
akademické	akademický	k2eAgFnSc6d1	akademická
a	a	k8xC	a
pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
kariéře	kariéra	k1gFnSc6	kariéra
na	na	k7c6	na
sovětských	sovětský	k2eAgFnPc6d1	sovětská
univerzitách	univerzita	k1gFnPc6	univerzita
a	a	k8xC	a
v	v	k7c6	v
Akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
akademik	akademik	k1gMnSc1	akademik
od	od	k7c2	od
r.	r.	kA	r.
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
prof.	prof.	kA	prof.
Mandelštamem	Mandelštam	k1gInSc7	Mandelštam
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
1934	[number]	k4	1934
byl	být	k5eAaImAgInS	být
šéfem	šéf	k1gMnSc7	šéf
teoretického	teoretický	k2eAgNnSc2d1	teoretické
oddělení	oddělení	k1gNnSc2	oddělení
v	v	k7c6	v
Ústavu	ústav	k1gInSc6	ústav
L.	L.	kA	L.
N.	N.	kA	N.
Lebedova	Lebedův	k2eAgMnSc4d1	Lebedův
AV	AV	kA	AV
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
pracích	prak	k1gInPc6	prak
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
mnoha	mnoho	k4c7	mnoho
teoretickými	teoretický	k2eAgInPc7d1	teoretický
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
i	i	k8xC	i
teorií	teorie	k1gFnSc7	teorie
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
otázkami	otázka	k1gFnPc7	otázka
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
kvantové	kvantový	k2eAgFnSc2d1	kvantová
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
fyzikou	fyzika	k1gFnSc7	fyzika
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
na	na	k7c4	na
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
vzniku	vznik	k1gInSc2	vznik
Čerenkovova	Čerenkovův	k2eAgNnSc2d1	Čerenkovovo
záření	záření	k1gNnSc2	záření
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jeho	jeho	k3xOp3gFnPc4	jeho
práce	práce	k1gFnPc4	práce
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
řízené	řízený	k2eAgFnSc2d1	řízená
termonukleární	termonukleární	k2eAgFnSc2d1	termonukleární
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
A.	A.	kA	A.
D.	D.	kA	D.
Sacharovem	Sacharov	k1gInSc7	Sacharov
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
reaktor	reaktor	k1gInSc4	reaktor
s	s	k7c7	s
řízenou	řízený	k2eAgFnSc7d1	řízená
termonukleární	termonukleární	k2eAgFnSc7d1	termonukleární
fúzí	fúze	k1gFnSc7	fúze
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k9	jako
"	"	kIx"	"
<g/>
TOKAMAK	TOKAMAK	kA	TOKAMAK
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečných	poválečný	k2eAgNnPc6d1	poválečné
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
také	také	k9	také
velmi	velmi	k6eAd1	velmi
významně	významně	k6eAd1	významně
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
termojaderné	termojaderný	k2eAgFnSc2d1	termojaderná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Igor	Igor	k1gMnSc1	Igor
Jevgeněvič	Jevgeněvič	k1gMnSc1	Jevgeněvič
Tamm	Tamm	k1gMnSc1	Tamm
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Igor	Igor	k1gMnSc1	Igor
Jevgeněvič	Jevgeněvič	k1gMnSc1	Jevgeněvič
Tamm	Tamm	k1gMnSc1	Tamm
</s>
